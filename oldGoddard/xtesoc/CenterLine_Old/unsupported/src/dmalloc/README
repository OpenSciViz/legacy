
		    Debugging Malloc, Version 2.2

			     By Jim Frost
			    August 5, 1991

WHAT IS IT?

Dmalloc is a short series of very portable malloc wrapper routines
which can be used to help track down allocation problems which are
normally very difficult to find using standard debugging environments.

Dmalloc is designed to detect malloc overruns and invalid frees as
early as possible, and to try to detect where the problem came from.
It has an allocation dumping function which can be used to find memory
leaks.

HOW SHOULD IT BE USED?

1.  Build dmalloc.a.  A Makefile is supplied to do this.  You can
    either type "make dmalloc.a" or "make".  The latter will also
    build a test program, discussed later.

2.  Add `#include "dmalloc.h"' to the top of each of your source
    files.  Files which do not include this file will use the system
    malloc routines instead of those of the debugging malloc.  See
    tester.c for an example.

3.  Compile your program and link with dmalloc.a.

4.  Set the MALLOC_DEBUG environment variable.  If set to a null
    value, debugging output will be sent to stderr, allocation checks
    will be done on every invocation of an allocation function, and
    both leading- and trailing-fence checking will be done.

    The MALLOC_DEBUG environment variable can also be set to a value
    which indicates options using the syntax:

      setenv MALLOC_DEBUG "option1,option2,option3"

    or (if you use the Bourne shell or Korn shell):

      set MALLOC_DEBUG="option1,option2,option3"
      export MALLOC_DEBUG

    The options are:

      abort	- Do not continue execution after the first error.  If
		  you are outside of CodeCenter, this will also generate
		  a core.
      fast	- Avoid searching the entire allocation list for
		  errors at every memory function call.  This can
		  significantly improve performance.  In "small" mode
		  allocations are sped up but frees are not.  To do a
		  complete error check, you can call malloc_verify()
		  at any point.
      file=filename
		- Dump output to a file rather than stderr.
      small	- Disable leading fence detection.  This cuts down the
		  overhead on each allocation by 8 bytes.  If in
		  "fast" mode it also slows down frees significantly.

    If the MALLOC_DEBUG environment variable is not set, dmalloc will
    do no checking and will behave exactly as the standard allocation
    routines.  This is done so that no recompilation is needed to
    activate or deactivate error checking.

5.  Run your program and look for errors.  Be aware that performance
    will drop depending on the number of active allocations your
    program has and what mode you are in, and that overall memory use
    may increase significantly.

6.  Unless in "fast" mode, the entire allocation list is verified each
    time an allocation or free function is called.  Any errors are
    reported at that time.  You may additionally insert calls to
    malloc_verify() in your code to force checking at specific points
    to help pinpoint problems.

7.  To help track down memory leaks, malloc_dump() is provided.  This
    dumps out all allocations in the allocation list in a sorted
    manner.  A warning is printed if malloc_dump() is called without
    MALLOC_DEBUG set.

USING DMALLOC WITHIN THE CODECENTER/SABER-C ENVIRONMENT

Dmalloc was written with and has been used within CodeCenter (which
used to be called Saber-C).  When used with CodeCenter, it provides a
number of enhancements over the standard CodeCenter environment:

  * Non-strcpy/bcopy allocation overruns are found in object as well
    as source.  Unfortunately overruns which take place in object code
    are not detected until after the damage is done, unlike
    CodeCenter's built-in checking.  When running in source,
    CodeCenter's built-in checking will not always find overruns
    immediately due to the overhead involved with leading- and
    trailing-fences.
  * Allocation history is given when a malloc problem is found.
  * Action points can call malloc_dump or malloc_verify explicitly
    without code alteration.

To use dmalloc with CodeCenter:

1.  Alter your source files to include dmalloc.h just as you would
    without CodeCenter.

2.  Type the following into the workspace:
      1-> load ccenter_dmalloc.a
      2-> source dmalloc.h
      3-> setenv MALLOC_DEBUG

When using ccenter_dmalloc.a, a CodeCenter breaklevel is generated
whenever a malloc problem is found.  You can call malloc_verify and
malloc_dump within action points for additional debugging information
without code alteration.

Note that when MALLOC_DEBUG is enabled, CodeCenter's normal malloc
overrun detection will not work in source code unless the overrun was
more than one byte.

TEST PROGRAM

The included program "tester.c" does some stupid allocation operations
to show what output is likely to look like.  I recommend running it at
least once to get an idea of what output will look like.

When run with the MALLOC_DEBUG environment variable set, this program
will report some problems.  It may also hang or dump core when
running.  If this happens, the hang/core dump should be within the
system library "free" function and there should be a problem report
right before the hang or core dump.  If it doesn't die then it's
likely that the library "free" function can handle double frees; the
Sun free can, for instance, while the DEC free cannot.  (There are a
couple of bugs in SunView that cause double frees, which is probably
why Sun's free has this behavior :-).

When run without MALLOC_DEBUG set the program should not report a
problem but is likely to dump core or exhibit other odd behavior due
to the abuse of the malloc routines.  Your mileage may vary depending
on your system.

I recommend trying out the different options with "tester" to see how
they affect performance and output.

IMPLEMENTATION DESCRIPTION

Three techniques are used to try to find allocation errors, an
allocation list, a leading fence (or allocation header) and a trailing
fence (or trailing mark byte).  In "small" mode the leading fence is
omitted.

Each allocation is added to a list of active allocations (kept in
allocation "buckets") so that they can be paired with corresponding
calls to free().  If a call to free() does not match an allocation in
the list, an error is reported.

Allocation buckets are allocated in large chunks to help minimize the
likelihood that a stray pointer will smash them.  They are never
freed, but rather placed on a free list for future allocations to use.

When a block of memory is freed, its bucket is moved to the free list
and the point at which it is freed is noted.  When a free mismatch is
noted, the free bucket list is searched for a corresponding bucket.
If found, the location at which the bucket was freed is printed.  In
this way double frees can usually be tracked down quickly.

Unless in fast mode, whenever an allocation or free is done, the
allocation list is scanned to search for corrupted fences.  If any are
found, they are reported and the size field in the allocation bucket
is changed to a negative value so that the corruption will not be
reported again (reducing noise considerably).

Additionally, the allocation lists and free lists are checked for
consistency.  If the lists become abbreviated or corrupted, either an
error is reported or the program dumps core while scanning the list.
This checking is done to cut down on the chances that a stray pointer
will destroy the internal lists.

A short list of recent allocations and frees is kept and is printed
whenever an error is found.  This list is for information only and is
not used for anything internally.

Unless in "small" mode, each allocation has a header attached to it
which contains a magic number (used for leading-fence corruption
detection) and a pointer to the bucket which owns the allocation.
When in "fast" mode this bucket is used to deallocate the allocation's
bucket without searching the whole allocation list.  This improves
performance to nearly that of bare "malloc".  If the leading- or
trailing-fence bytes are seen to be corrupted, a full check of the
allocation list is done and any errors are reported then.

MISCELLANEOUS NOTES

Allocation performance when MALLOC_DEBUG is enabled is slowed in
direct proportion to the number of allocations currently in the
allocation list.  It's fast enough for pretty large applications but
it can get pretty bad.  If in "fast" mode with leading-fences enabled
performance is nearly that of bare "malloc".

There is always a 25-byte minimum overhead with each allocation (for
the allocation bucket and trailing fence).  If not in "small" mode
there is an additional 8-byte overhead used for the leading fence.

If you get a core dump from within the checking function, it's
probably the result of an overrun or stray pointer which severely
damaged a block of allocation structures.  Unfortunately it may be
difficult to tell much more about the problem.

Since this uses the preprocessor to change malloc (etc) calls to use
dmalloc functions, these functions will not catch allocation problems
where allocations or frees are made by routines compiled without the
dmalloc.h header (this includes system library calls).  If an
allocation is made in code compiled without dmalloc.h and freed by
code compiled with it, spurious errors will be reported.  Likewise if
the allocation is made using dmalloc and freed without it.  These
errors are supposed to be recoverable.

POTENTIAL FUTURE ENHANCEMENTS

A "nearby" function would be useful, so that you could get an idea of
malloc areas that are just before or just after a particular
allocation area in memory.

Most memory-setting functions (such as bzero, memset, strcpy, etc)
could be wrapped to look for overrun behavior.

WARRANTY

T'ain't one.  This is distributed without warranty or guarantee of
support.  Use it if you want to, and I'll try to help if I can, but
this is provided as-is.

COPYRIGHT

None.  Do whatever you want with it, it's in the public domain.

HISTORY

Version	Notes
-------	-----
1.0	Original release.
1.1	Malloc_dump function and explicit Saber-C support added.
1.2	Changed to detect errors earlier in Saber-C.
2.0	Added leading-fence checking.
	Added "fast" mode with minimal automatic checking.
	Changed to co-exist with AIX 3.1 programs more easily.
	Removed some 1.2 additions that didn't work as intended.
2.1	Put in more changes to speed up calloc while in "fast" mode.
	Eliminated some warnings when not in debugging mode.
2.2	Fixed strdup() macro's call to dmalloc().
2.3	Altered to conform with Saber-C -> CodeCenter name change.

HOW TO FIND THE AUTHOR

Bug fixes, enhancements, gripes, praise, money, etc. can be directed
to the author at the address below.

jim frost
centerline software
10 fawcett street
cambridge, ma  02138
617 498 3000
jimf@centerline.com

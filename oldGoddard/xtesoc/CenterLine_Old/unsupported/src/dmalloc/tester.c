/* test.c:
 *
 * simple program to test dmalloc.  this allocates a bunch of randomly sized
 * blocks and does some random stupid things.
 *
 * this is likely to coredump when freeing stuff , but it should report
 * an "unknown address error" error before doing so, since the dump
 * will be free() dying.
 *
 * jim frost 06.21.90
 */

#include "dmalloc.h"
#include <stdio.h>

long time(); /* know C */

#define SIZE 1000

char *malloc1()
{
  return(malloc(1));
}

char *malloc2()
{
  return(malloc(2));
}

char *malloc3()
{
  return(malloc(3));
}

char *malloc4()
{
  return(malloc(4));
}

char *malloc5()
{
  return(malloc(5));
}

main()
{ char *allocs[SIZE];
  int   index, count, misses, size, smashes, frees, bogus;

  smashes= frees= bogus= 0;
  srandom((int)time(NULL));
  for (count= 0; count < SIZE; count++)
    allocs[count]= NULL;

  fprintf(stderr, "SAMPLE: show malloc_dump output for a memory leak\n");
  malloc(15);
  for (count= 1; count < 5; count++)
    for (index= 0; index < count; index++)
      malloc(count);
  malloc_dump();

  fprintf(stderr, "TEST: Early free and NULL free\n");
  free(NULL);

  fprintf(stderr, "TEST: Allocating with random goof-ups\n");
  for (count= 0; count < SIZE; count++) {
    for (misses= 0; allocs[index= (int)random() % SIZE]; misses++)

      /* too many misses, just go find one
       */

      if (misses > 100) {
	for (misses= 0; (misses < SIZE) && allocs[misses]; misses++)
	  /* EMPTY */;
	if (misses >= SIZE) {
	  fprintf(stderr, "TEST: failed to find open allocation\n");
	  index= 0; /* whatever */
	}
	else
	  index= misses;
	break;
      }

    switch (size= ((int)random() % 5) + 1) {
    case 1:
      allocs[index]= malloc1();
      break;
    case 2:
      allocs[index]= malloc2();
      break;
    case 3:
      allocs[index]= malloc3();
      break;
    case 4:
      allocs[index]= malloc4();
      break;
    case 5:
      allocs[index]= malloc5();
      break;
    }

    /* smash 1% of allocations, but not more than five times
     */

    if ((smashes < 5) && ((int)random() % 100) == 0) {
      if (((int)random() % 2) == 0) {
	fprintf(stderr, "TEST: Smashing beyond a malloc area\n");
	smashes++;
	*(allocs[index] + size)= '\0';
      }
      else {
	fprintf(stderr, "TEST: Smashing before a malloc area\n");
	smashes++;
	*(allocs[index] - 1)= '\0';
      }
    }

    /* free about 1% of allocations, but not more than five times.
     */

    if ((frees < 5) && (random() % 100) == 0) {
      for (misses= 0; (misses < 100) && !allocs[index= ((int)random() % SIZE)];
	   misses++)
	/* EMPTY */;
      if (misses < 100) {
	fprintf(stderr, "TEST: Freeing an area early\n");
	frees++;
	free(allocs[index]);
      }
    }

    /* totally bogus free 1% of the time, but not more than twice
     */

    else if ((bogus < 2) && (random() % 100) == 0) {
      fprintf(stderr, "TEST: Totally bogus free\n");
      bogus++;
      free((char *)&count);
    }

  }

  fprintf(stderr, "TEST: Freeing everything\n");
  for (count= 0; count < SIZE; count++)
    free(allocs[count]);
}

/* macro wrappers for dmalloc functions
 *
 * jim frost 06.07.90
 */

#ifdef _AIX
/* this takes care of problems if dmalloc.h is included before stdlib.h in
 * the user's source file. */
#include <stdlib.h>
#endif

#undef A
#ifdef __STDC__
#define A(ARGS) ARGS
#else
#define A(ARGS) ()
#endif

char *dmalloc A((int size, char *file, int line));
char *dcalloc A((int nelem, int size, char *file, int line));
char *drealloc A((char *ptr, int size, char *file, int line));
void  dfree A((char *ptr));
void  checkMallocBuckets A((char *file, int line, char *addr));
void  dumpMallocBuckets A((char *file, int line));

#define malloc(size)        dmalloc((size), __FILE__, __LINE__)
#define calloc(nelem, size) dcalloc((nelem), (size), __FILE__, __LINE__)
#define realloc(ptr, size)  drealloc((ptr), (size), __FILE__, __LINE__)

#define free(ptr)           dfree(ptr, __FILE__, __LINE__)
#define malloc_verify()     checkMallocBuckets(__FILE__, __LINE__, NULL)
#define malloc_dump()       dumpMallocBuckets(__FILE__, __LINE__)

#undef strdup
#define strdup(str) strcpy(dmalloc(strlen(str) + 1, __FILE__, __LINE__), (str))

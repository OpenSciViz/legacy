#!/bin/sh
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
#  C++ Examples Setup Script for CenterLine-C++
########
#@d.19940722 # This is an age stamp, do not remove.

# Get path to this script.  dir=`dirname $0`, but portable:
dir=`expr \( x"$0" : x'\(/\)$' \) \| \( x"$0" : x'\(.*\)/[^/]*$' \) \| .`
DIR=`cd $dir/.. ; pwd`
# Determine if default automounter mount path can be "ignored".
case $DIR in
 /tmp_mnt/* ) alt_DIR=`echo "$DIR" | sed 's/^\/tmp_mnt\//\//'`
              if [ -f ${alt_DIR}/admin/RUN_ME ]; then DIR=$alt_DIR ; fi ;;
esac

PATH=/bin:/usr/bin:/etc:$PATH:/usr/ucb ; export PATH

# set ARCH_OS (architecture and os type that we are running on)
ARCH_OS=`${DIR}/admin/platform 2> /dev/null`
status="$?"

#set  TUTOR_DIR
TUTOR_DIR="${DIR}/clc++/tutorial"

# check for correctness of determined location (i.e. are the files available?)
if [ ! -f ${TUTOR_DIR}/Makefile ]; then
   >&2 echo "ERROR: Unable to determine the location of the tutorial files to copy."
   >&2 echo "       The files should be in ${TUTOR_DIR}."
   >&2 echo
   exit 1
fi


# Set up the bounce tutorial
echo
echo "          CenterLine-C++ Examples Setup Program"
echo

WORKING_DIR=`pwd`

if [ -d c++examples_dir ];  then
    echo "Directory 'c++examples_dir' exists ...not creating"
    echo 
    ( cd c++examples_dir ; /bin/rm -rf `find . -type f -print -depth` 2> /dev/null )
    if [ "$?" != "0" ]; then
       >&2 echo "ERROR: You are unable to write in the existing c++examples_dir directory."
       >&2 echo "       Aborting tutorial setup."
       >&2 echo
       exit 2
    fi
else
    echo "Creating directory: c++examples_dir"
    echo
    /bin/mkdir c++examples_dir
    if [ $? != 0 ]; then
       >&2 echo "ERROR: Unable create directory: c++examples_dir"
       >&2 echo "       Aborting tutorial setup."
       >&2 echo
       exit 2
    fi
fi

echo "Copying tutorial files into 'c++examples_dir'"
echo

/bin/cp  ${TUTOR_DIR}/* ${WORKING_DIR}/c++examples_dir/.
if [ $? != 0 ]; then
   >&2 echo "ERROR: The process of copying the contents of ${TUTOR_DIR}"
   >&2 echo "       in to c++examples_dir failed. Perhaps you are out of space?"
   >&2 echo "       Please try again."
   >&2 echo
   exit 2
fi

#######
# massage the Makefile to pickup CenterLine CC script
QUOTE=`echo '"'`
BACKSLASH='\'
SLASH='/'

fixed_DIR=`echo "${DIR}" | eval "/bin/sed 's${SLASH}${BACKSLASH}${SLASH}${SLASH}${BACKSLASH}${BACKSLASH}${BACKSLASH}${SLASH}${SLASH}g'`

/bin/rm -f ${WORKING_DIR}/c++examples_dir/Makefile

# clcc is not available for all supported platforms, see if it is available.
if [ -f ${DIR}/clcc/${ARCH_OS}/bin/clcc ]; then
  if [ "${ARCH_OS}" = "pa-hpux8" ]; then
    eval "/bin/cat ${TUTOR_DIR}/Makefile | /bin/sed 's/^CXXLINE/CXX = ${fixed_DIR}\/bin\/CC/' | /bin/sed 's/^CCLINE/CC = ${fixed_DIR}\/bin\/clcc/' | /bin/sed 's/ -DHPUX_NEED_Z_FLAG/ -z/' > ${WORKING_DIR}/c++examples_dir/Makefile" 
  else
    eval "/bin/cat ${TUTOR_DIR}/Makefile | /bin/sed 's/^CXXLINE/CXX = ${fixed_DIR}\/bin\/CC/' | /bin/sed 's/^CCLINE/CC = ${fixed_DIR}\/bin\/clcc/' | /bin/sed 's/ -DHPUX_NEED_Z_FLAG//' > ${WORKING_DIR}/c++examples_dir/Makefile" 
  fi
else
  if [ "${ARCH_OS}" = "pa-hpux8" ]; then
    eval "/bin/cat ${TUTOR_DIR}/Makefile | /bin/sed 's/^CXXLINE/CXX = ${fixed_DIR}\/bin\/CC/' | /bin/sed 's/^CCLINE/CC = \/bin\/cc/' | /bin/sed 's/ -DHPUX_NEED_Z_FLAG/ -z/' > ${WORKING_DIR}/c++examples_dir/Makefile"
  else
    eval "/bin/cat ${TUTOR_DIR}/Makefile | /bin/sed 's/^CXXLINE/CXX = ${fixed_DIR}\/bin\/CC/' | /bin/sed 's/^CCLINE/CC = \/bin\/cc/' | /bin/sed 's/ -DHPUX_NEED_Z_FLAG//' > ${WORKING_DIR}/c++examples_dir/Makefile"
  fi
fi

########
# make certain the user has no problems using the files.
chmod ugo+w c++examples_dir/*

echo 
echo "Listing of files:"
/bin/ls -C c++examples_dir

echo
echo "To change to the tutorial directory, issue the following command:"
echo
echo "     cd c++examples_dir"
echo

exit 0

#c++examples

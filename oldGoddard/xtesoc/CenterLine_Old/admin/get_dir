#! /bin/sh
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
set -u

# NAME
#	get_dir -- prompt for a pathname
#
  SYNOPSIS='
	get_dir prompt [ default ]'
#
# DESCRIPTION
#	Always echoes a valid path to CenterLine
#
#	Never echoes a trailing '/' except in the case of the root directory.
#
#	If -d is specified and the user types a directory that doesn't exist,
#	the script prompts to create it.
#
# BUGS
#	Only one test(1) option can be specified.
#	The options are limited by the least common denominator, in this
#	case Ultrix.

regexp='.*'
type_opt=
what=""
allow_none=

case $# in
[12] )	;;
* )	>&2 echo Usage: $SYNOPSIS
	exit 2
	;;
esac
prompt="$1"
default="${2-}"

maybe_reenter=false
while ans=`ask_simple "$regexp" "$what" "$prompt" "$default"`
do
    case "$ans" in
    *[[~*?$]* )
	# Contains metacharacters; pass through csh.
	new=`csh -f -c "echo $ans"`
	if [ x"$new" != x"$ans" ]; then
		>&2 echo "$new"
	fi
	ans="$new"
	;;
    esac

    # Remove any number of final '/' characters.  Leave "/"
    # (root directory) alone.  (Fails for "//", no big deal.)
    case "$ans" in
    ?*/ ) ans=`expr x"$ans" : x'\(.*[^/]\)//*'` ;;
    esac
	
    case $ans in
    /* )    ;;
    * )     out "
Please type a pathname that begins with '/'."
            continue
            ;;
    esac

    case $ans in
    /export/* )
        out "
Warning: '$ans'
	begins with '/export', which is usually
	a directory on the file server but NOT a directory on its client hosts.
	The pathname should be valid on the client hosts."
        maybe_reenter=true
        ;;
    /tmp_mnt/* )
        out "
Warning: '$ans'
	begins with '/tmp_mnt', which is peculiar to the Sun automounter.
	Deleting '/tmp_mnt' from the pathname is usually a good idea."
        maybe_reenter=true
        ;;
    esac

    case $ans in
        */CenterLine ) ;;
        * ) out "
Warning: '$ans'
	does not end in 'CenterLine'.  It should."
            maybe_reenter=true
            ;;
    esac

    if $maybe_reenter ; then
         out "
Do you want to reenter it?  Type 'y' to reenter the pathname, 'n' to accept it."
        if ask_yn "
Reenter pathname" y; then
	    maybe_reenter=false
            continue
        fi
    else
	echo $ans
	break
    fi
done

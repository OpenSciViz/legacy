#! /bin/sh
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
set -u

# NAME
#	ask_path -- prompt for a pathname
#
  SYNOPSIS='
	ask_path [ -d|-f|-r|-w|-s|-t ] [ -none ] prompt [ default ]'
#
# DESCRIPTION
#	Always echoes a pathname that exists.  Options to restrict the kind
#	of file allowed are as in test(1).
#
#	Never echoes a trailing '/' except in the case of the root directory.
#
#	If -d is specified and the user types a directory that doesn't exist,
#	the script prompts to create it.
#
# BUGS
#	Only one test(1) option can be specified.
#	The options are limited by the least common denominator, in this
#	case Ultrix.

regexp='.*'
type_opt=
what="an existing pathname"
expl="does not exist"
allow_none=
while [ $# -gt 0 ]
do
	case $1 in
	-[dfrwst] )
		type_opt=$1
		case $1 in
		-d )	what="a directory"
			expl="is not a directory"
			;;
		-f )	what="a file that is not a directory"
			expl="does not exist or is a directory"
			;;
		-r )	what="a readable file"
			expl="is not readable"
			;;
		-w )	what="a writable file"
			expl="is not writable"
			;;
		-s )	what="a file with nonzero size"
			expl="does not exist or has zero size"
			;;
		-t )	what="a terminal device"
			expl="is not a terminal device"
			;;
		esac
		;;
	-none )	allow_none=yes
		;;
	-* )	>&2 echo Usage: $SYNOPSIS
		exit 2
		;;
	* )	break
		;;
	esac

	shift
done

case $# in
[12] )	;;
* )	>&2 echo Usage: $SYNOPSIS
	exit 2
	;;
esac
prompt="$1"
default="${2-}"

while ans=`ask_simple "$regexp" "$what" "$prompt" "$default"`
do
	if [ -n "$allow_none" -a x"$ans" = x"none" ]; then
		echo none
		exit 0
	fi

	case "$ans" in
	*[[~*?$]* )
		# Contains metacharacters; pass through csh.
		new=`csh -f -c "echo $ans"`
		if [ x"$new" != x"$ans" ]; then
			>&2 echo "$new"
		fi
		ans="$new"
		;;
	esac

	# Remove any number of final '/' characters.  Leave "/"
	# (root directory) alone.  (Fails for "//", no big deal.)
	case "$ans" in
	?*/ )	ans=`expr x"$ans" : x'\(.*[^/]\)//*'`
		;;
	esac

	if case $type_opt in
	   '' )	[ -f "$ans" -o -d "$ans" ] ;;
	   -* )	[ $type_opt "$ans" ] ;;
	   esac
	then
		echo "$ans"
		exit 0
	fi

	if [ x$type_opt = x"-d" -a ! -f "$ans" ] &&
	   ask_yn \
"
Directory '$ans' does not exist.
Create it"	  y
	then
                dirlist=""
                newdir=$ans
                while [ "$newdir" != "/" ]
                        do
                        dirlist="$newdir $dirlist"
                        newdir=`dirname $newdir`
                        if [ -d $newdir ] ; then
                                break
                        fi
                done

		if mkdir $dirlist; then
			echo "$ans"
			exit 0
		else
			>&2 echo "Could not create '$ans'.
"
			continue
		fi
	fi

	>&2 echo "'$ans' must be $what.
"
done

# Exit with bad status.
exit $?

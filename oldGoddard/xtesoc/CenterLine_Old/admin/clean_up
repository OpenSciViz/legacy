#!/bin/sh
# Copyright (c) 1986, 1994 by CenterLine Software, Inc.
set -u

  NAME='
	clean_up' # Clean up after installation
#
  SYNOPSIS='
	clean_up'
#

case ${INSTALL_CENTERLINE-} in
'' )	>&2 echo Error: $NAME "must be run by 'RUN_ME', not independently."
	exit 2
	;;
esac
if [ $# -ne 0 ]; then
	>&2 echo Usage: $SYNOPSIS
	exit 2
fi

for PROD_VERS in `find * \( -name 'vcpro' -o -name 'vctrm' -o -name 'testcenter' -o -name 'c_*' -o -name 'c++_*' -o -name 'clc_*' -o -name 'clcc' -o -name 'vcr_*' -o -name 'vcr_*/lib' -o -name 'clc++' -o -name 'oc_*' \) -type d -print 2> /dev/null | sed '/\//d' | sort -ru`
do
    vers=""
    version=""
    case $PROD_VERS in
     oc_2* )
      vers="NA"
      name="ObjectCenter"
      ;;
     c++_1* )
      name="ObjectCenter"
      ;;
     c_4*         )
      vers="NA"
      name="CodeCenter" 
      ;;
     c_3*          )
      name="CodeCenter" 
      ;;
     vcr_*        )
      name="ViewCenter for OI"
      ;;
     vctrm        )
      vers="NA"
      name="ViewCenter for Motif"
      ;;
     vcpro     )
      vers="NA"
      name="ViewCenter Pro"
      ;;
     testcenter   )
      vers="NA"
      name="TestCenter" 
      ;;
     clc++        )
      vers="NA"
      name="CenterLine-C++"
      ;;
     clcc         )
      vers="NA"
      name="CenterLine-C"
      ;;
     clc_*        )
      name="CenterLine-C"
      ;;
     *_*          )
      name="$PROD_VERS"
      ;;
    esac

    case ${vers-unset} in
     unset | '' )
       vers=`echo "$PROD_VERS" | awk -F'_' '{ print $2 }'`
       subversa=`echo "${vers}-" | awk -F'-' '{ print $1 }'`
       subversb=`echo "$vers" | tr -d '[a-z]' | awk -F'-' '{ print $2 }'`
       ;;
     *          )
       subversa=""
       subversb=""
       ;;
    esac

    # What type of release
    case $vers in
    *a* )	version="Release $subversa, Alpha version $subversb";;
    *b* )	version="Release $subversa, Beta version $subversb";;
    *s* )	version="Release $subversa, Special version $subversb";;
    NA  )       ;;
    * )		version="Release $subversa";;
    esac

    # Prompt for which directories to remove.
    cd $TOP/CenterLine/$PROD_VERS
    echo
    echo "
For the product/release:  $name, $version
the following architecture subdirectories are on disk:

     Size
     (KB)  	Description
     ------	---------------------------------------------"
    dirs_on_disk=""
    dirs_on_disk=`/bin/ls | egrep -v ".contents|Contents|common|configs|docs|install|include|lib|tutorial|integration"`

    found_any=0
    for dir in $dirs_on_disk
    do
        case $dir in
         .*19?????? ) continue ;;
         *.19?????? ) continue ;;
        esac
        if [ -f $dir ]; then continue ; fi

	arch=$dir
        found_any=`expr $found_any + 1`

        size=`size_on_disk $TOP/CenterLine/$PROD_VERS/$arch`
        case $size in
         ? | ?? | ??? ) echo-n "     $size	" ;;
         *            ) echo-n "     $size"       ;;
        esac
        case $arch in
          pa-hpux8       )
              echo "	HP9000s700(PA series) HP-UX 8"
            ;;
          sparc-solaris2 )
            echo "	Sun-4/SPARC running Solaris 2 ( SunOS 5)"
            ;;
          sparc-sunos4   )
            echo "	Sun-4/SPARC running SunOS 4.1 or greater (Solaris 1)"
            ;;
          mips*3x        )
            echo "	DECstation running Ultrix 3.x"
            ;;
          mips*4x        )
            echo "	DECstation running Ultrix 4.x"
            ;;
          mips*-1        )
            echo "	SUMIstation running SEI 3.2"
            echo "	 and MIPSstation running UMIPS 4_51 or greater"
            ;;
          sun3-4*        )
            echo "	Sun-3 running SunOS 4"
            ;;
          sun3-3*        )
            echo "	Sun-3 running SunOS 3"
            ;;
          sun4-41        )
            echo "	Sun-4/SPARC running SunOS 4.1 or greater"
            ;;
          sun4-40        )
            echo "	Sun-4/SPARC running SunOS 4"
            ;;
          sun4-3*        )
            echo "	Sun-4/SPARC running SunOS 3"
            ;;
          hp9000s300*    )
            case $subversa in
             3.1.2* )
              echo "	HP9000s3*/400(MC68030) HP-UX 7 or 8"
              ;;
             *        )
              echo "	HP9000s3*/400(MC68030) HP-UX 7"
              ;;
            esac
            ;;
          vax*3*         )
            echo "	VAX running Ultrix 3"
            ;;
          vax*4*         )
            echo "	VAX running Ultrix 4"
            ;;
        esac
    done

    for dir in $dirs_on_disk
    do
        case $dir in
         .*19?????? ) continue ;;
         *.19?????? ) continue ;;
        esac
        if [ -f $dir ]; then continue ; fi

	arch=$dir
        echo
        if ask_yn "Do you want to remove '$arch'" n &&
           ask_yn "Are you sure"
        then
            #
            # checking to remove unresolved links to the $PROD_VERS directory
            # in the CenterLine/include and CenterLine/lib directories
            #
            if [ "$name" = "ViewCenter for OI" ] ; then

               if [ -d $TOP/CenterLine/lib ]; then
                  if [ `/bin/ls $TOP/CenterLine/lib | grep $arch` = $arch ]; then
                     libdir=`( cd $TOP/CenterLine/lib/$arch/../.. ; pwd )`
                     lib_abrv=`basename $libdir | awk -F'_' '{ print $1 }'`
                     lib_prod_rel=`basename $libdir | awk -F'_' '{ print $2 }' | awk -F'-' '{ print $1 }'`
                     if [ $lib_abrv = OI ] ; then
                        if [ `echo "$lib_prod_rel" | tr -d \. ` = `echo "$subversa" | tr -d \. ` ] ; then
                           # execute a rm -f to insure that only a link
                           # will be removed.
                           /bin/rm -f $TOP/CenterLine/lib/$arch
                           archtest=`/bin/ls $TOP/CenterLine/lib | grep $arch`
                           if [ "x$archtest" = "x$arch" ]; then
                              echo "Warning: could not remove '$TOP/CenterLine/lib/$arch'."
                           fi
                        fi
                     fi
                  fi
               fi

               if [ -d $TOP/CenterLine/include ]; then
                  if [ `/bin/ls $TOP/CenterLine/include | grep OI` = OI ]; then
                     incdir=`( cd $TOP/CenterLine/include/OI/../.. ; pwd )`
                     inc_abrv=`basename $incdir | awk -F'_' '{ print $1 }'`
                     inc_prod_rel=`basename $incdir | awk -F'_' '{ print $2 }' | awk -F'-' '{ print $1 }'`
                     if [ $inc_abrv = OI ] ; then
                        if [ `echo "$inc_prod_rel" | tr -d \. ` = `echo "$subversa" | tr -d \. ` ] ; then
                           # execute a rm -f to insure that only a link
                           # will be removed.
                           /bin/rm -f $TOP/CenterLine/include/OI
                           OItest=`/bin/ls $TOP/CenterLine/include | grep OI`
                           if [ "xOItest" = "xOI" ]; then
                              echo "Warning: could not remove '$TOP/CenterLine/OI'."
                           fi
                        fi
                     fi
                  fi
               fi

             fi
               #
               # Now that any links into the arch directory have been removed
               # (if the product is ViewCenter for OI, then the specific architecture
               # can be removed.
               #
               /bin/rm -rf $arch
               if [ -d $arch ]; then
                   echo "Warning: could not remove '$arch'."
               else
                   echo "'$arch' removed."
                   found_any=`expr $found_any - 1 `
                   echo $found_any >> /tmp/saber.in
               fi
        fi
    done

#
# checking to remove empty $PROD_VERS directory.
#

    cd $TOP/CenterLine
    if [ $found_any -lt 1 ]; then
        echo "         0	No installed architectures"
        echo "
There are no architectures left in the $PROD_VERS directory."
        if ask_yn "Do you want to remove the $PROD_VERS directory" n &&
            ask_yn "Are you sure"
        then
            /bin/rm -rf $PROD_VERS/*/* $PROD_VERS/*/.??* $PROD_VERS/* $PROD_VERS

            if [ -d $PROD_VERS ]; then
                 echo "Warning: could not remove '$PROD'."
            else
                echo "'$PROD_VERS' removed."
	    fi
        fi
    fi
done

ACT=QUIT
export ACT

exec $TOP/CenterLine/admin/cladmin

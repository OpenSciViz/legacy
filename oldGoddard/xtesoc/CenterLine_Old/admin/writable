#! /bin/sh
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
set -u
#
#  NAME='
#	writable' # ensure that files and directories are writable
#
#  SYNOPSIS='
#	writable file|directory ... || exit'
#
# DESCRIPTION
#	If all files or directories are writable, exits with status 0.
#	If a file or directory is not writable, tells the user and asks
#	if he wants to continue.  If the user says yes, this script
#	exits with status 0.  If the user says no (the default), this
#	script exits with nonzero status.
#
# BUGS
#	Should explain about the possible causes of not being able to write.
#	Perhaps use [ -w $f ] as a test to help diagnose the problem -- if it
#	says yes, the problem is a read only filesystem or other NFS permission
#	problem.
#
# NOTE
#	Cannot use [ -w ... ] on directories -- it fails for NFS-mounted dir-
#	ectories at MIT Project Athena, maybe on all 4.3bsd.\

if   [ -f ${1} ]; then
     /bin/rm -f ${1} 2> /dev/null
     /bin/cp ${CENTERLINE_TOP}/admin/README ${1} 2> /dev/null
     if [ $? = 0 ]; then
        /bin/rm -f ${1} 2> /dev/null
        exit 0
     fi
     verb_filetype="write to the file"
elif [ -d ${1} ]; then
     /bin/cp ${CENTERLINE_TOP}/admin/README ${1}/.try_to_write 2>/dev/null
     if [ $? = 0 ]; then
        /bin/rm -f ${1}/.try_to_write 2> /dev/null
        exit 0
     fi
     verb_filetype="write to the directory"
else
     /bin/cp ${CENTERLINE_TOP}/admin/README ${1} 2> /dev/null
     if [ $? = 0 ]; then
        /bin/rm -f ${1} 2> /dev/null
        exit 0
     fi
     verb_filetype="create the file"
fi


>&2 echo
>&2 echo " ERROR:  You cannot $verb_filetype"
>&2 echo
>&2 echo "           '$1'."
>&2 echo
>&2 echo "         This will lead to failures later in this script."
>&2 echo

ask_yn "         Do you want to continue" n || exit 1
exit 0

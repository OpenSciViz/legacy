#!/bin/sh 
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.

set -u

HOST=`hostname 2> /dev/null`
if [ $? != 0 -o "x$HOST" = "x" ]; then HOST=`uname -n 2> /dev/null` ; fi
    
. ${CENTERLINE_TOP}/admin/this_cpu

cd ${CENTERLINE_TOP}

default_name=""
default_invoke=""
default_version=""
default_scandir=""
default_desc=""

echo
echo
echo 
echo "    The following is based on the 'CenterLine' directory structure."
echo "    The following products are installed in the following directory:"
echo
echo "    $HOST:$TOP/CenterLine:"
   
 
centerline_cxx=false
objectcenter=false
# Process for latest generation of CenterLine products
for scandir in `find * \( -name 'vcpro' -o -name 'vctrm' -o -name 'c_4*' -o -name 'oc_2*' -o -name 'testcenter' -o -name 'clcc*' -o -name 'clc++' \) -type d -print 2> /dev/null | sed '/\//d' | sort -ru`
do
    # What type of product
    case $scandir in
     c_4*  )
             name="CodeCenter"
             invoke="codecenter"
             ;;
     oc_2* )
             name="ObjectCenter"
             invoke="objectcenter"
             objectcenter=true
             ;;
     clcc )
             name="CenterLine-C"
             invoke="clcc"
             ;;
     clc++ ) if [ -f ${CENTERLINE_TOP}/clc++/tutorial/Makefile ]; then
                  centerline_cxx=true
             fi
             ;;
     testcenter )
             name="TestCenter"
             invoke="testcenter"
             ;;
     vctrm )
             name="ViewCenter for Motif"
             invoke="vctrm"
             ;;
     vcpro )
             name="ViewCenter Pro"
             invoke="vcpro"
             ;;
    esac
             

    # Print out the product and release
    prev_rel="0.0.0"
    case $scandir in
     c_4* | oc_2* | clcc | testcenter | vctrm | vcpro )
      for archos in `ls ${CENTERLINE_TOP}/${scandir}/*/.relnum 2> /dev/null | sed 's/\/\.relnum$//' | awk -F'/' '{ print $NF }'`
      do
          rel=`sed '1q' ${CENTERLINE_TOP}/${scandir}/${archos}/.relnum`
          if [ "$rel" != "$prev_rel" ]; then
              echo " "
              echo "    $name Release $rel"
              echo "                 Invocation Command:  $invoke"
              echo "            Installed architectures:"
          fi
          case $archos in
            m88k-svr4       )
              echo "                         Motorola Mc88000 running UNIX SVR4"
              ;;
            pa-hpux8       )
              case $scandir in
               vc?r? ) echo "                         HP9000s700(PA series) HP-UX 9," 
                       echo "                                               X11R5 and motif 1.2.2" ;;
               *     ) echo "                         HP9000s700(PA series) HP-UX 8 and 9" ;;
              esac
              ;;
            sparc-solaris2 )
              case $scandir in
               vc?r? ) echo "                         Sun-4/SPARC running Solaris 2.2 ( SunOS 5.2),"
                       echo "                                             X11R5 and motif 1.2.2" ;;
               *     ) echo "                         Sun-4/SPARC running Solaris 2.1 or greater ( SunOS 5)" ;;
              esac
              ;;
            sparc-sunos4   )
              case $scandir in
               vc?r? ) echo "                         Sun-4/SPARC running SunOS 4.1.3 (Solaris 1.1),"
                       echo "                                             X11R5 and motif 1.2.2" ;;
              *      ) echo "                         Sun-4/SPARC running SunOS 4.1.1 or greater (Solaris 1)" ;;
              esac
              ;;
          esac
          prev_rel=$rel
      done
      ;;
     clc++ )
       # both CenterLine-C++ with UI and pdm and the cfront shipped
       # with ObjectCenter 2.x use this dierctory, messy...
       if $centerline_cxx ; then
         name="CenterLine-C++"
         if [ `wc -l ${CENTERLINE_TOP}/clc++/tutorial/Makefile | awk '{ print $1 }'` = 1 ]; then
             invoke=" "
         else
             invoke="centerline-c++"
         fi
         for archos in `ls ${CENTERLINE_TOP}/clc++/*/bin/.relnum 2> /dev/null | sed 's/\/bin\/\.relnum$//' | awk -F'/' '{ print $NF }'`
         do
             rel=`tail -1 ${CENTERLINE_TOP}/clc++/${archos}/bin/.relnum`
             if [ "$rel" != "$prev_rel" ]; then
                 echo " "
                 echo "    $name Release $rel"
                 echo "                 Invocation Command:  $invoke"
                 echo "            Installed architectures:"
             fi
             case $archos in
               m88k-svr4       )
                 echo "                         Motorola Mc88000 running UNIX SVR4"
                 ;;
               pa-hpux8       )
                 echo "                         HP9000s700(PA series) HP-UX 8.x and 9.x"
                 ;;
               sparc-solaris2 )
                 echo "                         Sun-4/SPARC running Solaris 2 ( SunOS 5)"
                 ;;
               sparc-sunos4   )
                 echo "                         Sun-4/SPARC running SunOS 4.1 or greater (Solaris 1)"
                 ;;
             esac
             prev_rel=$rel
         done
       fi

      echo " "
      echo "    CLC++ (CenterLine-C++)"
      echo "                 Invocation Command:  CC"
      echo "            Installed architectures:"
      for archos in `ls ${CENTERLINE_TOP}/clc++/*/.relnum 2> /dev/null | sed 's/\/\.relnum$//' | awk -F'/' '{ print $NF }'`
      do
          if [ -f ${CENTERLINE_TOP}/clc++/${archos}/.cxx_deliverable ]; then
              cxx_delivered=`/bin/sed '1q' ${CENTERLINE_TOP}/clc++/${archos}/.cxx_deliverable`
              if $cxx_delivered ; then
                 wr_check=`/bin/sed -n '/WindRiver/p' ${CENTERLINE_TOP}/clc++/${archos}/.relnum | /bin/sed '1q'`
                 case ${wr_check-0} in
                  / | '' ) echo "                       As shipped with CenterLine-C++ for" ;;
                  *      ) echo "                       As shipped with the version ${wr_check}" ;;
                 esac
              else
                 echo "                       As shipped with ObjectCenter for"
              fi
          else
              echo "                       As shipped with ObjectCenter for "
          fi
          case $archos in
            m88k-svr4       )
              echo "                         Motorola Mc88000 running UNIX SVR4"
              ;;
            pa-hpux8       )
              echo "                         HP9000s700(PA series) HP-UX 8.x and 9.x"
              ;;
            sparc-solaris2 )
              echo "                         Sun-4/SPARC running Solaris 2 ( SunOS 5)"
              ;;
            sparc-sunos4   )
              echo "                         Sun-4/SPARC running SunOS 4.1 or greater (Solaris 1)"
              ;;
          esac
      done
      ;;
    esac
done


# Process for older generation of CenterLine products
for scandir in `find * \( -name 'c_3*' -o -name 'c++_1*' -o -name 'vcr_1*' -o -name 'vcr_1*/lib' \) -type d -print 2> /dev/null | sed '/\//d' | sort -ru`
do
    # break up scandir name into prod, vers, subvers
    prod=`echo $scandir | awk -F'_' '{ print $1 }'`
    vers=`echo $scandir | awk -F'_' '{ print $2 }'`
     
    # This section is for backwards compatability
    prod=`echo $scandir | awk -F'_' '{ print $1 }'`
    
    # What type of product
    case $scandir in
     c++_1* )
             name="ObjectCenter"
             invoke="objectcenter1, xobjectcenter1,
                                      or objecttool1"
             subversa=`echo $vers | awk -F'-' '{ print $1 }'`
             subversb=`echo $vers | tr -d '[a-z]' | awk -F'-' '{ print $2 }'`
             ;;
     c_3*  ) name="CodeCenter"
             invoke="codecenter3, xcodecenter3, or codetool3"
             subversa=`echo $vers | awk -F'-' '{ print $1 }'`
             subversb=`echo $vers | tr -d '[a-z]' | awk -F'-' '{ print $2 }'`
             ;;
     vcr_*/lib )
             case $scandir in
              *1.0.0* ) name="OI 3.2 Libraries" ;;
              *1.0.1* ) name="OI 3.3 Libraries" ;;
              *       ) continue                ;;
             esac
             scandir=`echo $scandir | awk -F'/' '{ print $1 }'`
             invoke=""
             subversa=""
             subversb=""
             ;;
     vcr_1* )
             name="ViewCenter"
             invoke="viewcenter"
             subversa=`echo $vers | awk -F'-' '{ print $1 }'`
             subversb=`echo $vers | tr -d '[a-z]' | awk -F'-' '{ print $2 }'`
             ;;
     *     ) name="$prod"
             invoke="unknown"
             subversa=`echo $vers | awk -F'-' '{ print $1 }'`
             subversb=`echo $vers | tr -d '[a-z]' | awk -F'-' '{ print $2 }'`
             ;;
    esac
      
    # What type of release
    case $vers in
     *a* ) version="Release $subversa, Alpha version $subversb"   ;;
     *b* ) version="Release $subversa, Beta version $subversb"    ;;
     *s* ) version="Release $subversa, Special version $subversb" ;;
     NA  ) version="Release $rel"                                 ;;
     *   ) version="Release $vers"                                ;;
    esac
 
                 
    # Print out the product and release
    prod_rel_desc="    $name $version"
    echo
    echo "$prod_rel_desc"
    case $vers in
     *-r ) vers=`echo $vers | awk -F'-' '{ print $1 }'`   ;;
     *   )                                                ;;
    esac
    echo "                 Invocation Command:  $invoke"
    echo "            Installed architectures:"

 
    # look for specific architectures dirs under the product directory
    for archs in `cd $scandir; find * -type d -print 2> /dev/null | sed '/\//d'`
    do
         # skip over common, general, doc
         case $archs in
          common | configs | general | docs | install | *.19* )
            continue
            ;;
          pa-hpux8       )
            echo "                         HP9000s700(PA series) HP-UX 8"
            ;;
          sparc-solaris2 )
            echo "                         Sun-4/SPARC running Solaris 2 ( SunOS 5)"
            ;;
          sparc-sunos4   )
            echo "                         Sun-4/SPARC running SunOS 4.1 or greater (Solaris 1)"
            ;;
          mips*3x        )
            echo "                         DECstation running Ultrix 3.x"
            ;;
          mips*4x        )
            echo "                         DECstation running Ultrix 4.x"
            ;;
          mips*-1        )
            echo "                         SUMIstation running SEI 3.2"
            echo "                         MIPSstation running UMIPS 4_51 or greater"
            ;;
          sun3-4*        )
            echo "                         Sun-3 running SunOS 4"
            ;;
          sun3-3*        )
            echo "                         Sun-3 running SunOS 3"
            ;;
          sun4-41        )
            echo "                         Sun-4/SPARC running SunOS 4.1 or greater"
            ;;
          sun4-40        )
            echo "                         Sun-4/SPARC running SunOS 4"
            ;;
          sun4-3*        )
            echo "                         Sun-4/SPARC running SunOS 3"
            ;;
          hp9000s300*    )
            case $scandir in
             c_3.1.2* )
              echo "                        HP9000s3*/400(MC68030) HP-UX 7 or 8"
              ;;
             *        )
              echo "                        HP9000s3*/400(MC68030) HP-UX 7"
              ;;
            esac
            ;;
          vax*3*         )
            echo "                         VAX running Ultrix 3"
            ;;
          vax*4*         )
            echo "                         VAX running Ultrix 4"
            ;;
         esac

    done # Done looking for archs

done # Done looking for release directories

exit 0
####
    

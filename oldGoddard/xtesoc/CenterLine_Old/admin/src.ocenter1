#! /bin/sh
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
#    Driver script for ObjectCenter 1.1
#
#@d.19921106 # This is an age stamp, do not remove.
#
set -u
# These parameters are hard coded
# All O* parameters are 'Old' parameters set for backward compatability
# Shipped with these three

#####
# Get path to this startup script.  dir=`dirname $0`, but portable:

dir=`expr \( x"$0" : x'\(/\)$' \) \| \( x"$0" : x'\(.*\)/[^/]*$' \) \| .`
#
#####

#####
# Get path to this startup script.  dir=`dirname $0`, but portable:

CENTERLINE_ANCHOR=`cd $dir ; pwd` ; export CENTERLINE_ANCHOR
#
#####



#####
# If -wgid is used as a command line argument, then print the workgroup ID
# and quit.
#
case "$#" in
0 );;
* )
    for arg in ${@}
    do
        case "${arg}" in
        "-wgid" ) 
	    CENTERLINE_PW="$dir/../configs/Password"
            if [ ! -f $CENTERLINE_PW ]; then
                >&2 echo "
$0: Error:
Your Password file <$CENTERLINE_PW>
does not exist.
Contact your system administrator or CenterLine customer support.
"
                exit 1
            else
	        CENTERLINE_WGID=`grep Serial_number $CENTERLINE_PW | sed -e 's/Serial_number[ 	]*\([^ 	]*\)$/\1/'`
                case "$CENTERLINE_WGID" in
                "")     
		    >&2 echo "
$0: Error:
Your Password file <$CENTERLINE_PW>
does not have a Workgroup ID.
Contact your system administrator or CenterLine customer support.
"
		    exit 1
		    ;;
	        *)      
		    echo "Workgroup ID      $CENTERLINE_WGID"
                    exit 0
                    ;;
                esac
            fi
            ;;
	    *) ;;
        esac
    done
    ;;
esac
#
#####
# core binaries
EXECUTIVE=executive
OEXECUTIVE=saberc++_exec
CENTERLINE_TTY="BSD"
RESTART_MANAGER=manager
UI=ascii_ui
OUI=saberc++mgr
CENTERLINE_PROD=c++
ORESTART_MANAGER=saberrmgr
OBASE_NAME=saberc++

# These names are to facilitate the name change
# Old file names
OXRC=.xsaberrc++
OUIL=.saberc++uil
OTOOLINIT=.saberc++tool
OINIT=.saberc++init

# New init filenames
NXRC=.ocenterrc
NUIL=.ocenteruil
NTOOLINIT=.ocentertool
NINIT=.ocenterinit
#
#####

#####
# This is set at installation time
#
BASE_NAME=objectcenter
#
#####

#^L

#####
# Facilitate name change (if script exists)
CENTERLINE_MESSAGES=0
verbose=1
case ${CENTERLINE_SKIP_NAME_CHANGE-unset} in
unset )	if [ -f $dir/.name_change/checkenv ]; then
	     . $dir/.name_change/checkenv
	fi
	;;
* ) ;;
esac
#
###########

###########
# Handle user predifined path environment variables
#

# Set CENTERLINE_DIR if it is not already set in the environment.
case ${CENTERLINE_DIR-unset} in
unset|"" )  
    CENTERLINE_DIR_SET= 
    CENTERLINE_DIR=$dir/.. ; export CENTERLINE_DIR ;;
* ) CENTERLINE_DIR_SET=1 ;;
esac

#############################
# Check to se of ObjectCenter 1.x is actually installed.
#############################
for initfileck in `/bin/ls ${CENTERLINE_DIR}/c++_1*-*/configs/ocenterinit 2> /dev/null`
do
     case ${initfileck-unset} in
      unset | '' ) ;;
      * ) exists=$initfileck ;;
     esac
done
case ${exists-unset} in
 unset | '' )
     echo
     echo "ERROR: ObjectCenter Release 1 is not installed in this CenterLine tree."
     echo "       Exiting..."
     echo
     exit 2
     ;;
 * ) ;;
esac

#######


####
# This section of code is borrowed from 'install/this_cpu'.  When changing
# or adding architectures, make sure to edit 'this_cpu' and 'Architecture' file.
# Find the Architecture and Operating system for this machine.
#

# Find CENTERLINE_ARCH_OS

case ${CENTERLINE_ARCH_OS-unset} in

unset|"" ) # If unset, or null, then find it.
    CENTERLINE_ARCH_OS_SET=
    CENTERLINE_ARCH_OS="unknown" ; export CENTERLINE_ARCH_OS
    CENTERLINE_ARCH="unknown"
    CENTERLINE_OS="unknown"
   
    DEC='mips mips-ultrix4x' # Supported DEC arch(vax obsolete) 
    SUN='sun3 sun4'                      # Supported sun arch ( sun386 obsolete)
    ALL_ARCH="$SUN $DEC"      # Supported archs
    
    ##### First, find CENTERLINE_ARCH
    # /bin/arch is a program that exists only on Suns.
    if [ -f /bin/arch ]; then CENTERLINE_ARCH=`/bin/arch` ; export CENTERLINE_ARCH
    # /bin/machine is used by DECs
    elif [ -f /bin/machine ]; then CENTERLINE_ARCH=`/bin/machine` ; export CENTERLINE_ARCH
    # /bin/uname is used on a variety of other machines.
    elif [ -f /bin/uname ]; then CENTERLINE_ARCH=`/bin/uname -m`; export CENTERLINE_ARCH
    # Else unknown architecture
    else
        >&2 echo "Unsupported cpu type.  Supported cpu types are:"
        >&2 echo "    "$ALL_ARCH
        exit 2
    fi
    #
    #####
    # 
    ##### Find CENTERLINE_ARCH_OS
    #  Based on what the architecture is, find out the os type
    case $CENTERLINE_ARCH in
    

        sun3|sun4) 
            if [ -f /usr/lib/ld.so ]; then 
                CENTERLINE_ARCH_OS="${CENTERLINE_ARCH}-40" ; export CENTERLINE_ARCH_OS
            else
                if [ -r /vmunix ]; then
                    >&2 echo "Unsupported OS type:"
                    >&2 strings /vmunix | grep ' Release' | awk '{ print $3 }' \
                        | tr -d '.' | tail -1 | sed 's/\(..\).*/\1/'
                else
                    >&2 echo "Unsupported OS type."
                fi
                >&2 echo "Supported OS types for this architecture <$CENTERLINE_ARCH> are:"
                >&2 echo '    SunOS 4.x'
                exit 2
            fi
            ;;

       mips|mips-ultrix)
            if [ -f /vmb.exe -o -f /ultrixboot ]; then
                if [ -f /etc/svc.conf ]; then
                        case $CENTERLINE_ARCH in
                            mips ) CENTERLINE_ARCH="mips-ultrix";;
                        esac
                        ultrixvers=4x
                        CENTERLINE_ARCH_OS=$CENTERLINE_ARCH$ultrixvers ; export CENTERLINE_ARCH_OS
                else
                     if [ -r /vmunix ]; then
                          >&2 echo "Unsupported OS type:"
                          >&2 strings /vmunix | grep "^Ultrix " | tail -1
                     else
                          >&2 echo "Unsupported OS type."
                     fi
                     >&2 echo "Supported OS types for this architecture <$CENTERLINE_ARCH> is Ultrix 4.x"
                     exit 2

                fi
            else
                if [ -r /vmunix ]; then
                    >&2 echo "Unsupported OS type:"
                    >&2 strings /vmunix | grep "^Ultrix " | tail -1
                else
                    >&2 echo "Unsupported OS type."
                fi
                >&2 echo "Supported OS types for this architecture <$CENTERLINE_ARCH> is Ultrix 4.x"
                exit 2
            fi
            ;;
    
        * )
            >&2 echo "Unsupported cpu type <$CENTERLINE_ARCH>. Supported cpu types are:"
            >&2 echo "	"$ALL_ARCH
            exit 2
            ;;
    esac
    ;;

#
#####
#  Find CENTERLINE_ARCH
# If CENTERLINE_ARCH_OS is set, then extrapolate the CENTERLINE_ARCH.

* ) CENTERLINE_ARCH_OS_SET=1
    case ${CENTERLINE_ARCH_OS} in
         sun4*) CENTERLINE_ARCH=sun4 ;;
         sun3*) CENTERLINE_ARCH=sun3 ;;
         mips-ultrix4x) CENTERLINE_ARCH=mips-ultrix4x ;;
    esac
    ;;
esac

#####
# Find OBJECTCENTER_VERSION
# 
CENTERLINE_PROD_VERS=""
case ${OBJECTCENTER_VERSION-unset} in
    #####
    # This section handles default invocation
    unset|"" ) 
        OBJECTCENTER_VERSION=
        OBJECTCENTER_VERSION_SET=
        # The following 'for' loop does a sort (via the shell wildcard) to find
        # the highest release of a product.

        # fixes bug in bourne shell
        SABER_TMP=0

        case ${CENTERLINE_DIR}/${CENTERLINE_PROD}_*/$CENTERLINE_ARCH_OS in
                "" ) ;;
                * )
                    for prod in ${CENTERLINE_DIR}/${CENTERLINE_PROD}_*/${CENTERLINE_ARCH_OS}
                    do  
                        i=i
                    done

                    if [ -d $prod ] ; then
                        # latest=`dirname $prod`, but portable:
                        latest=`expr \( x"$prod" : x'\(/\)$' \) \| \( x"$prod" : x'\(.*\)/[^/]*$' \) \| .`
                        CENTERLINE_PROD_VERS=`basename $latest`
                        export CENTERLINE_PROD_VERS
                    fi
                    ;;
        esac
        ;;

    #####
    # If OBJECTCENTER_VERSION is set, then export it.
    * ) OBJECTCENTER_VERSION_SET=1
        CENTERLINE_PROD_VERS=${CENTERLINE_PROD}_${OBJECTCENTER_VERSION}
        export CENTERLINE_PROD_VERS
        ;;
esac
#
##########
#
##########
# Select init files and setup WIN environment variables for the tutorial
#
old_init=0 ; new_init=0
case $BASE_NAME in
*objecttool )
    INITFILE=".ocenterinit"
    case ${NEWSSERVER-UNSET} in
    UNSET ) ;;  # no NeWS server in evidence, proceed.
    * ) >&2 echo "$0 warning: For best results, use xobjectcenter with Open Windows.";;
    esac
    ;;
*xobjectcenter )
    # These parameters are set by the installation procedure.
    # Set WININCS/LIBS if they are not already set in the environment.
    # The environment variable OPENWINHOME for open windows
    case ${OPENWINHOME-unset} in
    unset ) # If OPENWINHOME is not set then we are not using open windows
        WININCS= ; export WININCS ; WINLIBS= ; export WINLIBS ;;
    * )     # If set to something, then set/export WININCS and WINLIBS
        WININCS="-I$OPENWINHOME/include" ;    export WININCS
        WINLIBS="-L$OPENWINHOME/lib"     ;    export WINLIBS ;;
    esac
    INITFILE=".ocenterrc"
    ;;
esac
#
##########

##########
# Facilitate name change (if script exists)
case ${CENTERLINE_SKIP_NAME_CHANGE-unset} in
unset )	if [ -f $CENTERLINE_DIR/bin/.name_change/name_change ]; then
	   . $CENTERLINE_DIR/bin/.name_change/name_change
	fi
	;;
* ) ;;
esac
if [ $CENTERLINE_MESSAGES != 0 ]; then
        echo "*** In the future...
*** If you wish to suppress any name-change related messages, then set the
*** environment variable  <CENTERLINE_SILENT_NAME_CHANGE> to 1.
*** If you wish to skip the translation process, all together, then set the
*** environment variable  <CENTERLINE_SKIP_NAME_CHANGE>  to  1.
"
fi
#
##############
#^L
##############
# Set up correct path and binaries to invoke
#
D=$CENTERLINE_DIR/$CENTERLINE_PROD_VERS/$CENTERLINE_ARCH_OS
EXEC_CMD=exec        # always exec the restart manager

#####
# Are we using the old Saber-style core binaries, or the new ones
if [ -f $D/bin/$OEXECUTIVE ] ; then
        # we are using the old binary names, convert back to old names
        EXECUTIVE=$OEXECUTIVE
        UI=$OUI
        RESTART_MANAGER=$ORESTART_MANAGER
fi
#
#####

#####
# Now you're allowed to run saber.
if [ -d $D -a -d $D/bin -a -f $D/bin/$UI -a -f $D/bin/$EXECUTIVE -a -f $D/bin/$RESTART_MANAGER ]; then

    # Set up UI TERM conventions for objecttool and xobjectcenter
    if [ $UI = objectcenterxui -o $UI = objecttool ]; then
	# TERMCAP/INFO values must be specified as absolute pathnames, hence pwd
	T=`cd $D ; pwd`
        TERMCAP=$T/emulator/termcap ;   export TERMCAP
        TERMINFO=$T/emulator ;          export TERMINFO
        TERM=saber-term ;               export TERM
    fi
    # Do an eval `resize` for the ascii manager, fixes term problem on HP
    case $CENTERLINE_TTY in
        HP )
            case $UI in
                ascii_ui|saberc++mgr )
                    # inititalize $resize
                    resize=""
                    # find out if there is a resize that we can use
                    for which in /usr/bin/which /usr/ucb/which
                    do
                        if [ -x $which ]; then
                            resize=`$which resize | awk '{ print $1 }'`
                            break
                        fi
                    done
                    # If resize doesn't exist, then look for the resize 
                    # command and execute it.
                    if [ ! -n "$resize" -o x"$resize" = x"no" -o ! -x $resize ]; then
                        for rzpath in /usr/bin /usr/local /usr/local/bin /usr/bin/X11 /usr/local/X11 /usr/local/bin/X11
                        do
                            if [ -x $rzpath/resize ]; then
                    	    	# resize acts differently, depending on what the
                    	    	# SHELL variable is set to.
				SAVE_SHELL=${SHELL=''}
                    	    	SHELL=/bin/sh ; eval `$rzpath/resize`
				SHELL=$SAVE_SHELL
                                break
                            fi
                        done
                    else
                    	# resize acts differently, depending on what the
                       	# SHELL variable is set to.
			SAVE_SHELL=${SHELL=''}
                       	SHELL=/bin/sh ; eval `$resize`
			SHELL=$SAVE_SHELL
                    fi
                    ;;
            esac
            ;;
    esac

    case $# in
    0 )   $EXEC_CMD $D/bin/$RESTART_MANAGER $D/bin/$UI $D/bin/$EXECUTIVE -h$CENTERLINE_DIR ;;
    * )   $EXEC_CMD $D/bin/$RESTART_MANAGER $D/bin/$UI $D/bin/$EXECUTIVE -h$CENTERLINE_DIR "$@" ;;
    esac

    # Exit with status returned from the restart manager.
    exit
fi
##########

#
##########
# Test error conditions.

>&2 echo \
"
*
* Failure in startup script '$0'.
*
* NOTE:  There may be several probable reasons and solutions for this error.
*        You may wish to make a hard copy of the following errors/solutions.
*        In order to resolve your problem as quickly as possible, please try to
*        act on the solutions in the order that they are listed.

"
>&2 echo "Please hit return for more information: " ; read dummy

#####
# initialize some common messages
#####
message0="# A probable reason for this failure:
# The file you are trying to execute is a symbolic link, or copy of the
# installed startup script $BASE_NAME.
# You cannot invoke Saber with links or copies of Saber startup scripts.
# You must execute the file that was installed in the 'Saber/bin' directory.
#        
# The 'host:path' where this startup script was originally installed is:
#             ${PATH_TO_CENTERLINE}/bin
# Solutions:
# Include the path to the 'Saber/bin' in your PATH variable.
# Ask your administrator to execute the 'global_path' script, located in the
# ${PATH_TO_CENTERLINE}/${CENTERLINE_PROD}_*/install directory.
"

message1="# A probable reason for this failure:
# The variables 'CENTERLINE_DIR', 'OBJECTCENTER_VERSION' and 'CENTERLINE_ARCH_OS' in the
# startup script '$0' are set to:
#       '$CENTERLINE_DIR'
#       '$OBJECTCENTER_VERSION'
# and
#       '$CENTERLINE_ARCH_OS'
# respectively.
# The derived directory '$D'
# does not exist.
# This machines architecture (determined by this script) is: $CENTERLINE_ARCH_OS
# The binaries for this machine's architecture '$CENTERLINE_ARCH_OS' may not
# be installed, or the startup script '$0'
# may have been incorrectly installed.
# Solutions:
# Your system administator should make sure that the binaries for this machine's
# architecture '$CENTERLINE_ARCH_OS' are installed.
# Your system administator should reinstall this startup script by rexecuting
# the installation script 'RUN_ME' and selecting the menu item (1):
#    \"Extract files from distribution media and install commands\"
# Please try to execute '$0'
# after reinstalling."

message2="# A probable reason for this failure:
# Following, are environment variables used in the script '$BASE_NAME':
#
# Variable          Where Set            Currently Set To
# ----------------- -------------------- --------------------------------------"

message3="#
# The path to the $BASE_NAME binaries is composed of the following variables:
#                         CENTERLINE_DIR/${CENTERLINE_PROD}_OBJECTCENTER_VERSION/CENTERLINE_ARCH_OS
# The derived directory:
#                         '$D'
# does not exist.
#
# You may have incorrectly set a variable in your environment.
# The 'host:path' where the Saber directory was originally installed is:
#                         ${PATH_TO_CENTERLINE}
#
# Solution:
# Please respecify or unset any variables that were set in your environment 
# and try again."

message4="# A probable reason for this failure:
# The directory '$D' exists,
# but is not set up correctly.
#
# It should contain 'bin/$UI', 'bin/$RESTART_MANAGER', and 'bin/$EXECUTIVE',
# but it doesn't.
#
# It may have been incorrectly installed.
#
# Solution:
# Your system administator should reinstall this directory by rexecuting
# the installation script 'RUN_ME' and selecting the menu item (1):
#
#    \"Extract files from media distribution and install command\"
#
# Please try to execute '$0'
# after reinstalling."

#
# A more portable way to check to see if the file is a link
# test -h does not work on decstations
/bin/ls -l $0 | egrep "^l|^L" 2> /dev/null
status=$?
if [ x"$status" = x"0" ]; then
   >&2 echo "$message0"
   exit 2
fi

case $0 in
  */$BASE_NAME* ) ;;
  */bin/$BASE_NAME* ) ;;
  *Saber/bin/$BASE_NAME* ) ;;
  *) >&2 echo "$message0"
     >&2 echo "Please hit return for more information: " ; read dummy ;;
esac
 
if [ ! -d $D ]; then
    # if any variables are set by the user, then ask the user to retry.

    case "${CENTERLINE_DIR_SET}${OBJECTCENTER_VERSION_SET}${CENTERLINE_ARCH_OS_SET}" in

    "" ) >&2 echo "$message1" ;;
    * )  >&2 echo "$message2"
        case $CENTERLINE_DIR_SET in
        "") >&2 echo "# CENTERLINE_DIR         at run time          $CENTERLINE_DIR";;
        *)  >&2 echo "# CENTERLINE_DIR         in your environment  $CENTERLINE_DIR";;
        esac
        case $OBJECTCENTER_VERSION_SET in
        "") >&2 echo "# OBJECTCENTER_VERSION   at run time          $OBJECTCENTER_VERSION";;
        *)  >&2 echo "# OBJECTCENTER_VERSION   in your environment  $OBJECTCENTER_VERSION";;
        esac
        case $CENTERLINE_ARCH_OS_SET in
        "") >&2 echo "# CENTERLINE_ARCH_OS     at run time          $CENTERLINE_ARCH_OS";;
        *)  >&2 echo "# CENTERLINE_ARCH_OS     in your environment  $CENTERLINE_ARCH_OS";;
        esac 
        >&2 echo "$message3"
        ;;
    esac
else
    #####
    # install error
    #####
    >&2 echo "$message4"
fi
exit 2

#objectcenter1

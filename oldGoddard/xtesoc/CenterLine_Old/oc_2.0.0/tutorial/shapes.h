#ifndef __shapes_h__
#define __shapes_h__


    /*--------------------------------------------------*
     *                                                  *
     *        Definition of a Point class               *
     *                                                  *
     *--------------------------------------------------*/
class Point {
private:
  int x; // X-coordinate of point
  int y; // Y-coordinate of point
public:
  Point(int X, int Y);   // Point constructor
  Point(Point &anotherPoint);
  int   getX();          // Get the X-coordinate
  int   getY();          // Get the Y-coordinate
  void  setX(int newX);  // Set the X-coordinate
  void  setY(int newY);  // Set the Y-coordinate
};

    /*--------------------------------------------------*
     *                                                  *
     *        Definition of a drawable shape            *
     *                                                  *
     *--------------------------------------------------*/

#define LENGTH  500      /* Height of window */
#define HEIGHT  600      /* Height of window */
#define WIDTH   600      /* Width of window */
#define MARGIN  0        /* Border around window */
#define IWIDTH  64       /* Icon height */
#define IHEIGHT 20       /* Icon width */
#define INDEX(x) ((x)+50)
#define DECR (50)

class Drawable {
public:
  virtual void doDraw()=0;
};

class Shape {
public:
    virtual void setPercentage(float percentage) {percentage = .1;};
};

class DrawableShape: public Drawable, public Shape
{
protected:
  Point origin;           // Top-left of shape
  int   filled;           // Object is filled.
private: 
  short *row;      // Rows where shape should be drawn
  short *col;      // Columns where shape should be drawn
public: 
  DrawableShape(Point &topLeft);  // Drawable shape constructor
  DrawableShape(DrawableShape& d);
  ~DrawableShape(); 		 // Drawable shape destructor
  virtual void doDraw();       // Filler
  virtual void draw()=0;          // Draw the shape. Subclass responsibility
          void drawMove(int count);
          void move(Point &to);   // Move the shape to a new location
          void bounce();          // Visually bounce the shape on the screen;
	  void setFilled();       // Set the 'fill' indicator on the object
private: // Private member functions  
          void wait();            // Wait a brief time for the shape to be
                                  // displayed
          void createTable();     // Create the row column table
};

#endif
 

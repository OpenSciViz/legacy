#ifndef __LINK_H__
#define __LINK_H__

#include <stdio.h>

class Link {
private:
  Link *next;
public:
  Link(Link *nextLink=NULL);
  Link *nextLink();
  void setNext(Link *newLink);
};  

#endif

#include "rect.h"
#include "x.h"

Rectangle::Rectangle(Point &origin, Point &theExtent):
           DrawableShape(origin), extent(theExtent)
{
}

void Rectangle::draw()
{
  drawRect(origin.getX(), origin.getY(), extent.getX(), extent.getY(), filled);
}

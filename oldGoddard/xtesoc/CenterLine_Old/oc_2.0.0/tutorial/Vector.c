template <class T> Vector<T>::Vector() 
{
		// start off with 3 elements
		size = 3; 
		data = new T[size]; 
} 
template <class T> T& Vector<T>::operator[](int n) 
{ 
		int os; 
		int i; 
		T* newdata;
		// grow if have to 
		if (n >= size) 
		{
			os = size; 
			while (size <= n) 
			  size *= 2; 
			newdata = new T[size]; 
			for (i = 0; i < os; i++) 
			  newdata[i] = data[i];
			delete [] data; 
			data = newdata;
		}
		// return reference to data slot
		return data[n]; 
} 



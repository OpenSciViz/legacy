If you are unable to set a breakpoint, check the following.

o       Does the line on which you want to set the breakpoint
        have executable code?

        A breakpoint must be on a line with executable code.
        Attempting to set a breakpoint on a line without
        executable code results in the breakpoint being set
        on the next line with executable code.

        Here are some examples:

        int fn()        /* cannot stop here */
        {               /* cannot stop here */
            int a = 3;  /* can stop here, first executable code after function prolog */
            int b;      /* cannot stop here */
                        /* cannot stop here */
            b = a * a;  /* can stop here */
        #if 0           /* cannot stop here */
            b++;        /* cannot stop here */
        #endif          /* cannot stop here */
            return b;   /* can stop here */
        }               /* can stop here, prior to execution of function epilog */


o       If the file is an object file, does it have debugging
        information?

        Make sure you have compiled the file with -g (the CC
        switch that produces additional symbol table information).
        ObjectCenter ignores -g if you use it as a switch
        to its load command.

See Also
----------
CC	for information about CC switches
stop	for information about setting a breakpoint


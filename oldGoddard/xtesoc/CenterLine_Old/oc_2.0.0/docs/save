NAME	
     save

     saves the current session in a project file

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |      no    |
     -------------------------


SYNOPSIS
     save
     save file
     save project
     save project file
     
DESCRIPTION
     << none >>		Saves a project file with the
     			default name ocenter.proj.   
     
     file		Saves a project file with the
     			specified filename.          
     
     project		Same as << none >>. Saves a       
     			project file with the default name
     			ocenter.proj.                     
     
     project file	Same as file. Saves a project file
     			with the specified filename.      
USAGE
     Use the save command to save the current session of
     ObjectCenter so that it can be restored later.
     
Project files
     A project file is a text script file that contains the
     information that ObjectCenter needs to rebuild your project
     across sessions. It records the following:
     
     o	The files that make up the project and in which form they
	are loaded (as C++ or C files)
     
     o	Which warnings have been suppressed
     
     o	The values of the ObjectCenter options
     
     o	The signals that are caught and ignored
     
     o	The debugging items that have been set (such as
	breakpoints and actions)

     A project file does not specify dynamic run-time
     information, such as variable values or break-level
     location, or information about your environment, such as the
     version of ObjectCenter you invoked or the type of
     workstation or terminal you are using.
     
     NOTE	A project file does not save variables or
		functions defined in the Workspace.
     
Loading project files
     To load a saved project file, use the load command and
     supply the name of the file. You can also include the name
     of a project file on the command line when starting
     ObjectCenter.
     
RESTRICTIONS
     Information about open files is not saved with the session.
     Thus, saving a session while files are open may create
     problems when the session is reloaded, unless the exact same
     files have been opened again. Also, open files that are not
     open in the reloaded session might be closed improperly and
     data could be lost.
     
     The state of the terminal is not saved in a project file.
     
SEE ALSO
     load


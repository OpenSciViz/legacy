To find bugs in your program, you set breakpoints and actions,
run the program, examine the program at the breakpoint, and control
further execution as needed.

Setting breakpoints in templates
----------------------------------
You can set breakpoints and actions in several ways.  One way of
setting breakpoints, however, is unavailable in template functions
or function members of template classes.  That method is clicking
on a line number in code displayed in the Source Panel.  If you try
to set a breakpoint by clicking on a line number in code displayed
in the Source Panel in a template function or a function member of
template classes, this error results:

        Invalid breakpoint/action setting

To set a breakpoint in a template function, use the stop command in
the Workspace and specify the complete function name, including
the class name and parameter type.

This is the syntax:

        C++ -> stop in template_class<param_type>::func_name

Here is an example:

        C++  -> stop in List<int>::insert

The example sets a breakpoint in the insert() member function
of the List template class, but only for the "int" instantiation.
No method exists for setting a breakpoint for all instantiated
types.  You must explicitly set the breakpoint for each.

Debugging templates
--------------------
Keep the following in mind when you are debugging templates.

o       To step into a template function, you must load the
        instantiated template code as source or as object code
        with debug information (compiled with -g).

o       ObjectCenter prompts you for the correct function if the
        function name is overloaded.

o       You need to specify the location of the template source
        files with the tmpl_instantiate_flg option and possibly
        the path option.

See Also
----------
options		for more information about options
step		for information about executing by statement, entering functions
stop		for information about setting breakpoints
templates	for information about templates


NAME
     whereami

     displays the current break and scope locations

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------



SYNOPSIS
     whereami

DESCRIPTION
     << none >> 
     Displays the current break and scope locations.

USAGE
     Use the whereami command to list the current break  location
     and  the current scope location. This is particularly useful
     for finding where you are once you have moved up or down the
     execution stack while at a break level.

  Break location

     The break location is the point at which  execution  stopped
     when the break level was entered.

  Scope location

     The scope location is the point to  which  variables,  func-
     tions,  and types are scoped. When a break level is entered,
     it is set to the break location. It can be changed  to  dif-
     ferent locations on the execution stack with the up and down
     commands.

  Display of locations

     If you have not moved up or  down  in  the  execution  stack
     while  at  a  break  level, the scope location and the break
     location are the same.  The whereami command  displays  that
     location in the Source area, scrolling the display if neces-
     sary.

     If you have moved up or down in  the  execution  stack,  the
     scope location is displayed in the Source area and the break
     location is shown in the Workspace.

     NOTE    If the whereami command appears not to
             respond as you expect, keep the following in
             mind:

             o    The break location is only displayed in the
                  Workspace when the break location is
                  different from the scope location.

             o    The Source area will only change if the
                  current scope location is not already
                  displayed there.


  Display of locations in Ascii
  ObjectCenter

     In Ascii ObjectCenter, if you have not moved up or down in the
     execution  stack  while at a break level, the break location
     and the scope location are the same. In this case,  whereami
     gives  a  single  listing  for  both the break and the scope
     locations.

  Errors and warnings

     If an error or a warning caused  the  current  break  level,
     whereami  displays the error or warning number. If execution
     can be continued from the break level, whereami displays the
     arguments that can be passed to the cont command.

SEE ALSO
     cont, down, proto, up, where

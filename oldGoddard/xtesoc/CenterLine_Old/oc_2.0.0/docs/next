NAME
     next

     executes source code by line; does not enter functions

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------


SYNOPSIS
     next
     next number

DESCRIPTION
     << none >> 
     Executes an entire line, regardless of the number
     of statements on the line, and then stops execution.

     Motif and OPEN LOOK: Displays a solid arrow
     pointing to the current execution line in the
     Source area.

     number 
     Executes the specified number of  lines,  and  then
     stops execution.

OPTIONS
     The following ObjectCenter option affects the next command:

     cxx_suffixes              Specifies file extensions to search 
                               for when ObjectCenter needs to find a 
                               C++ source file that corresponds to a 
                               given object file.

     src_step                  Specifies number of lines of source
     (Ascii ObjectCenter only) code to be displayed after
                               execution of a statement.


     See the options reference page for more details  about  each
     option.  ObjectCenter  does not support these options in process
     debugging mode (pdm).


USAGE

     Use the next command to execute your code line by line without
     going into functions that are called.

  Automatic mode switching

     When using next to step through code, ObjectCenter automatically
     matches the Workspace mode to the language type of the module
     you are currently in. For example, if the current mode is C++ and
     you use next to move into a C module, then ObjectCenter
     automatically changes to C mode.

  Specifying file extensions for C++ source code modules

     When you use next to move into object code, ObjectCenter needs to
     find the corresponding source file so that it can display the source
     code in the Source area. See the language selection entry
     for information about how to set the extensions correctly for
     C++ modules

     The next command does not stop inside object code functions that
     do not have debugging information (functions either compiled
     without the -g switch or loaded with the -G switch).

SEE ALSO

     step, stepout

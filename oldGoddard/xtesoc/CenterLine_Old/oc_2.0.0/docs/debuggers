You'll find that you're already familiar with many ObjectCenter
commands if you've used other debuggers. You can also use the 
commands of other debuggers or abbreviations of commands in the
Workspace by creating aliases to ObjectCenter commands.

ObjectCenter has two debugging modes: component debugging
mode (cdm) and process debugging mode (pdm). In component
debugging mode, you can create aliases in your ObjectCenter
startup file, .ocenterinit. In process debugging mode, you 
can create aliases in your .pdminit startup file. 

For example:

	alias s step
	alias n next
	alias break stop

You can also use gdb commands in process debugging mode. In 
the Workspace, enter gdb before you enter the command, or 
change ObjectCenter to gdb mode with the gdb_mode command.  
We provide no technical support for gdb.

See Also
---------
commands 	for a complete list of ObjectCenter commands
gdb 		for information about executing a gdb command
gdb_mode 	for information about changing to gdb mode
overview 	for a description of cdm and pdm
pdm 		for information about debugging an executable
objectcenter 	for information about .ocenterinit and .pdminit



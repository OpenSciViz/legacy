If you are new to ObjectCenter, select one of the first two categories
in the panel on the left to help you get started. The topics available
in that category are listed in the panel on the right. Click on any
topic to display it here.

The "About This Release" and "Platform" categories contain information
about new features, limitations, manuals, and platform-specific
requirements and anomalies. 

If you're having a specific problem, try the troubleshooting
information in the next six categories.

The remainder of the categories contain an up-to-date version of the
ObjectCenter Reference, as well as extracts from cfront documentation.
Use the scrollbar in the categories panel below to see all the types
of information available.

Moving to other topics
======================
To move to related topics, place the mouse pointer in the top
panel of the Manual Browser and press the Right mouse button, or
pull down the See Also menu. All the related topics mentioned in
each document are on the See Also menu.

To move directly to any topic in the Manual Browser, enter its name in
the New Topic field under this panel, or enter "man" and the topic name in
the Workspace at the bottom of the Main Window, for example:

C++ 1-> man support

Searching and printing
======================
There is no search or print facility in the Manual Browser, but you
can use a plain text search of the directory containing the Manual
Browser files and use your system command to print topics.  For
example, to find out which topics discuss memory leak detection and
print a relevant topic:

% grep -l "memory leak detection" <path>/CenterLine/oc_2.0.0/docs/*
% lpr <path>/CenterLine/oc_2.0.0/docs/memory_leak_detection



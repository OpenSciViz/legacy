POTENTIAL HP ANOMALIES
  
  In most cases, your programs will run the same within the
  ObjectCenter environment as they do outside the environment.
  However, there are some platform-specific features that
  ObjectCenter may not support fully, so you may see unexpected
  behavior. This section attempts to call your attention to
  these potential anomalies.
  
Default parser configuration  
  
  The default C parser configuration for the ObjectCenter
  interpreter is K&R.
  
  Invoke the config_c_parser command to display the current
  setting.  For more information about specifying a different
  parser configuration, see the config_c_parser entry in the
  ObjectCenter Reference.

Supported cc and c89 switches

  ObjectCenter's load command supports the following switches 
  to the HP c89(1) and cc(1) commands:

  -Dname=def	   to define a preprocessor symbol
  or -Dname 

  -lx	           to cause the linker to search the library 
		   libx.a or libx.sl, where x is one or more 
		   characters, in an attempt to resolve 
		   currently unresolved global references.

  -Ldir		   to tell the linker to search in dir for 
		   libx.a or libx.sl before searching in the 
		   default locations.

  -Uname	   to remove any initial definition of name in 
		   the preprocessor.

  -w		   to suppress load-time warning messages.

Supported ld switches

  ObjectCenter's load command supports the following switches 
  to the HP ld(1) command:

  -a		   to specify whether shared or archived 
		   libraries are searched with the -l switch.

  -lx		   to cause the linker to search the library 
		   libx.a or libx.sl, where x is one or more 
		   characters, in an attempt to resolve 
		   currently unresolved global references.

  -Ldir		   to tell the linker to search in dir for 
		   libx.a or libx.sl before searching in the 
		   default locations.

  All other switches are ignored.

Different behavior of the load command

  ObjectCenter processes all -L switches before processing any
  -l switches. Thus, the command:
  
  -> load -lm -L./subdir
  
  loads the library ./subdir/libm.a instead of /lib/libm.a
  (presuming both libraries exist). HP's ld(1) command processes
  all command-line switches in left-to-right order, so this
  behavior is unexpected.
  
Unsupported environment variables
  
  Currently, ObjectCenter's load command ignores these HP-UX
  environment variables:
  
  CCOPTS
  
  FLOW_DATA_DIR
  
  LANG
  
  LPATH
  
  LD_PXDB
  
No PIC support
  
  The ObjectCenter dynamic linker/loader does not support object
  modules containing Position Independent Code (PIC) on the HP
  platform.
  
No support for cache hint bits
  
  The ObjectCenter dynamic linker/loader does not detect and
  eliminate uses of cache hint bits in object modules in
  situations where 64-byte stack pointer alignment cannot be
  guaranteed.
  
Void types may be misinterpreted
   
  Due to an HP compiler bug, objects of base type void can
  sometimes appear to have the base type of int when their
  definitions are loaded in object form.
  
No support for long pointers
  
  The ObjectCenter interpreter does not support long pointer
  declaration syntax (for example, "int ^x;").
  
No support for argument values in intrinsics

  The ObjectCenter interpreter does not support default argument
  values in calls to intrinsics.
  
ObjectCenter replaces fewer header files

  We provide fewer header files on the HP platform than we do
  for some other platforms because more HP header files comply
  with the ANSI standard.
  
Changes in signal handling

  If your program makes use of either signal() in libBSD.a or
  sigpause() in libV3.a, load the appropriate library before the
  program is linked or run for the first time in a session;
  otherwise, the libc.a version will be used.
  
  After a program is linked or run for the first time in a
  session, you cannot subsequently change the version of
  signal() or sigpause() that your program uses. Therefore, if
  you link or run without first loading the appropriate library,
  you must exit ObjectCenter and restart it.
  
  To avoid this problem, add one of the following lines to your
  personal .ocenterinit file or to the system-wide ocenterinit
  file:
  
  for libBSD.a add
  
  load -lBSD
  
  for libV3.a add
  
  load -lV3
  
sigvec() and sigvector() flags

  The SV_ONSTACK and SV_NOCLDSTOP flags to sigvec() and
  sigvector() are ignored. (This behavior with SV_ONSTACK is the
  same as on other architectures supported by ObjectCenter;
  however, the behavior with SV_NOCLDSTOP is specific to HP-UX.)
  
  The SV_BSDSIG flag used in sigvector() has no effect (that is,
  the sigvector() function always acts as if the SV_BSDSIG flag
  is set).  The SV_BSDSIG flag is not returned in the old sigvec
  structure, even if it was set by the caller.
  
sigaction() flags ignored
  
  The SA_ONSTACK and SA_NOCLDSTOP flags to sigaction() are
  ignored.
  
No sigspace() and no sigstack()
  
  The sigspace() and sigstack() functions are not supported.
  
_longjmp() and _setjmp()
  
  The _longjmp() and _setjmp() functions behave like longjmp()
  and setjmp().
  
Enhanced sigset() and sigvector()
  
  Signal handler maintenance functions which are incompatible
  under HP-UX (such as sigset() and sigvector()) work correctly
  with each other under ObjectCenter.
  
mmap() and syscall()
  
  The mmap() and syscall() functions are unsupported in HP-UX
  8.07.  The function syscall() is unsupported in HP-UX 9.xx.
  Calling these unsupported functions can result in unexpected
  behavior.
  
Calls to shl_*(3X) functions
  
  This section documents the behavior of shl_*(3X) functions
  when you call them within ObjectCenter. These functions are
  available only on the HP 9000 Series 700 and Series 800
  platforms.
  
  shl_definesym(3X) always returns -1 and sets errno to EINVAL.
  ObjectCenter is unable to accurately reproduce the semantics
  of shl_definesym(3X), so it is not supported.
  
  shl_load(3X) and cxxshl_load(3X) always interpret the
  BIND_IMMEDIATE modifier flag as if it were BIND_DEFERRED. It
  is impossible to obtain BIND_IMMEDIATE semantics from
  shl_load(3X) and cxxshl_load(3X) within the ObjectCenter
  environment.

  Loading a library with the BIND_FIRST modifier flag to
  shl_load(3X) or cxxshl_load(3X) will cause that library to be
  assigned an index of 1 (see the shl_get(3X) manual page for a
  description of a library's index). By contrast, loading a
  library outside of the ObjectCenter environment will cause
  that library to be assigned an index of 0.  Therefore, an
  index of 0 always represents the user's main program within
  the ObjectCenter environment.
  
  When the DYNAMIC_PATH flag is passed to shl_load(3X) or
  cxxshl_load(3X), those functions use only the path list stored
  in the SHLIB_PATH environment variable to find the library.
  This is because there is no way to emulate the +s and +b
  options to ld(1) within the ObjectCenter environment.
  
  ObjectCenter unloads all dynamically loaded libraries when a
  reset to top level occurs.
  
  shl_unload(3X) and cxxshl_unload(3X) do not actually unload
  the library, and thus do not unbind any symbols that have been
  bound from that library. Unloading and reloading a library
  with shl_*(3X) functions will not cause them to return an
  error status, but the original image of the library is the
  only one that is ever loaded during a single run of the user
  program.
  
  The value of the HP-UX reserved variable __dld_loc is always
  0x0 within ObjectCenter. It should not be used by a user
  program.
  
  C++ static constructors and destructors in shared libraries
  are always invoked, even if the user program does not use the
  C++-aware dynamic loading functions, cxxshl_load(3X) and
  cxxshl_unload(3X).
  
  ObjectCenter does not call initializer functions in a shared
  library at any time. Note that initializer functions in HP-UX
  shared libraries are not C++ static initializers, which
  ObjectCenter supports. Initializer functions are
  language-independent functions defined at
  shared-library-creation time with the +I option to ld(1).
  
  The initializer members of struct shl_descriptor objects
  generated by shl_get(3X) and shl_gethandle(3X) always have the
  following value: NO_INITIALIZER.
  
  shl_get(3X) will return -1 (with errno == EINVAL) if it is
  passed an index with the value -1 or 0. This is because there
  is no distinct dynamic linker (index -1) or main program
  executable (index 0) within the ObjectCenter environment.
  
  shl_gethandle(3X) will return -1 (with errno == EINVAL) if it
  is passed the handle PROG_HANDLE. This is because there is no
  distinct main program executable (index 0) within the
  ObjectCenter environment.
  
  shl_getsymbols(3X) behaves as if every defining instance of a
  symbol with external linkage in the user's program is exported
  from the main program. These symbols are accessed by passing
  PROG_HANDLE as the first argument to shl_getsymbols(3X).
  Outside of the ObjectCenter environment, this behavior is
  achieved by using the -E switch to ld(1) when creating the
  main program.
  
  The GLOBAL_VALUES and IMPORT_SYMBOLS modifier flags are not
  supported for calls to shl_getsymbols(3X). Calls to
  shl_getsymbols(3X) will return -1 with errno set to EINVAL if
  either of these flags is passed in.
  
ioctl() support
  
  ObjectCenter supports user programs that make IOMAPMAP and
  IOMAPUNMAP ioctl() requests. There is one restriction.
  
  The restriction is that ObjectCenter considers a memory area
  invalid if your program maps it with IOMAPMAP and unmaps it
  with close() or ioctl(). This is the case even if close() or
  ioctl() did not actually unmap the area because other
  processes on the system still had it mapped. A subsequent
  IOMAPMAP ioctl() request for that memory area causes
  ObjectCenter to again consider it valid.
  
Static symbol not in symtab

   On the HP platform, pdm may issue the following warning:

   Internal: static symbol `symname' found in filename psymtab but not in symtab

   If you receive this message, issue the following command:

	whatis <symname>

   and then reissue the command that caused the warning.
    

Attaching to a running process may fail on HP-UX
  
  Attaching to a running process sometimes fails on HP-UX in
  ObjectCenter's process debugging mode (pdm) if the pdm binary
  you are using is installed on a remote partition.
  
  The failure looks like a ptrace failure, such as one you get
  if you request an illegal process number or if you attempt to
  attach a file you do not own.
  
  pdm -> debug a.out 
  process_id debug: ptrace: Permission denied.  
  pdm ->
  
  Before implementing the workaround we provide below, eliminate
  any possibility that the failure is the result of a ptrace
  error. See the ptrace man page for information about ptrace
  failure errors.
  
  This is the workaround:

  o	Copy the pdm binary to a disk that is local to the 
	HP-UX machine. 

	You must copy it on a disk local to each HP-UX 
	machine on which you want pdm to run. Do not install 
	it on a cluster partition. 

  o	Create a .clpm.conf file in your home directory to 
	override the AS PDM directive in the system clpm.conf. 

	If you have multiple home directories, you have to 
	create multiple ~/.clpm.conf files so that the 
	workaround affects each machine on which you want 
	pdm to run. 

  The clpm.conf file should contain the following lines.

  AS PDM {
   BINARY: /tmp/pdm;
   BINARY_ENV: CLDB;
   ENV: 
   LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$(centerline)/$(arch)/lib;
   ARGS: -connect;
   RESTART_ARGS: -connect -restart;
   ARG_TMPL: ^[^-].*:0;
  }

  With the workaround, attempts to attach to a running process 
  will now succeed:

  pdm 1 -> debug a.out pid
  Debugging program `/net/pickup/tmp/loop'
  Resetting to top level.
  warning: reading register r4: I/O error
  0x2560 in sigpause ()
  pdm (break 1) 2 -> where
  #0 0x2560 in sigpause ()
  #1 0x21cc in _sleep ()
  #2 0x1fec in main () at loop.c:16
  
  For more information about attaching to a running process, 
  see the debug entry in the Manual Browser or ObjectCenter 
  Reference.
  
clcc command-line switches 
  Many of the switches used with the HP C compiler are also used with
  clcc, the CenterLine-C compiler. However, some switches you use with
  cc must be replaced by a corresponding clcc switch, and other cc
  switches have no equivalent in clcc. 

  The following table shows some common cc switches and their clcc
  equivalents.

  ______________________________________________________________________ 

  HP cc switch	  clcc switch	   Description
  ______________________________________________________________________ 
  -Aa		  -ansi		   Enables strict ANSI compliance

  -Ac		  -traditional,	   Disables strict ANSI C compliance.
		  -Xa, or -Xt	   Please refer to the descriptions of
				   these switches in the CenterLine-C
				   Programmer's Guide or the clcc manual
				   page for more information

  -G		  -pg		   Inserts information required by the
				   gprof profiler in the object file.

  -Wx,arg1[,arg2, -Hcppopt=string  Pass argument string to the
   ...,argn]	  -Hldopt=string   preprocessor (-Hcppopt) or the linker
				   (-Hldopt). string can contain
				   multiple arguments separated by commas.

  +L		  -Hlist	   Generates a source listing on
				   standard output.

  +On		  -On		   Sets the optimization level to n
				   where n is 1 to 7. If n is not
				   specified, then a level of 2 is used.

  +wn		  -wn		   Suppress warning messages at level
				   n and higher where n is 1 to 4.

  +z		  -pic		   Produces position-independent code.
				   The memory allocated to static
				   variables cannot exceed 4K.

  +Z		  -PIC		   Like -pic, but allows the global
				   offset table to span the range of
				   32-bit addresses when there are too
				   many global objects for -pic.
  ______________________________________________________________________ 



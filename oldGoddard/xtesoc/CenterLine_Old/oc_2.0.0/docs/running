You can run your program

o       In source and object form, for which ObjectCenter
        must be in component debugging mode

o       As an executable, for which ObjectCenter must be
        in process debugging mode.


Running the program in source and object form
------------------------------------------------
Before running the program in source and object form in
component debugging mode, you must load your program.  See 
"cdm loading".

To run your program, issue the run command in the Workspace.

If ObjectCenter finds a run-time error or warning, it
automatically generates a break level and opens the Error
Browser displaying information about the error or warning.

To edit the code that caused the run-time error and run the
program again, see "cdm loading".

Running the program as an executable
---------------------------------------
Before running the program as an executable, ObjectCenter
must be in process debugging mode, and you must create the
executable and issue the debug command to load the executable.
See "pdm debugging".

To run your program, issue the run command in the Workspace.


See Also
---------
cdm loading	for information about debugging source and object files
debug		for information about loading an executable
load		for information about loading source and object code
pdm		for information about process debugging mode
pdm debugging	for information about debugging an executable
run		for information about running a program





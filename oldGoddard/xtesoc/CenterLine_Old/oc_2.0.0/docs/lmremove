
NAME
     lmremove - remove a user's license for a product

SYNOPSIS
     Usage is:
     lmremove [-c license_file_path] feature user host[display]

     where:

      license_file_path
          is the full pathname to the license file. If this path-
          name  is not specified, lmgrd looks for the environment
          variable LM_LICENSE_FILE. If that environment  variable
          is    not    set,    lmgrd    looks   for   the   file,
          /usr/local/flexlm/licenses/license.dat.

      feature
          is the name of the product feature as specified in  the
          license.dat file.

      user
          is the name of the user that you  are  preventing  from
          using this feature.

      host
          is the name of the system that you want to remove  from
          using this feature.

      display
          is the name of the display.

     NOTE:  You should protect the execution of lmremove
            since removing a user's license can be
            disruptive.

DESCRIPTION
     The lmremove utility allows you to remove  a  single  user's
     license  for  a specified feature. This could be required in
     the case where the licensed user was running the software on
     a  node  that subsequently crashed. This situation sometimes
     causes the license to remain unusable.  lmremove allows  the
     license to return to the pool of available licenses.

SEE ALSO
     lmstat(1),lmreread(1)




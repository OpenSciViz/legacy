This entry has solutions to problems with debugging the following
preprocessed code:

1.  I'm using an Informix SQL preprocessor (.ec extension)

2.  I'm using an Ingres SQL preprocessor


1.  I'm using an Informix SQL preprocessor (.ec extension)
------------------------------------------------------------
The Informix preprocessor operates on source files with the .ec
extension.

To use Informix SQL preprocessor code:

        1.  Preprocess the .ec files outside of ObjectCenter

        2.  Load the .c files into ObjectCenter

Considerations when using the Informix preprocessor:

o       The Informix preprocessor cannot handle class definitions.
        It gives the error:

          esqlc: file "yourfile.ec", line 35: right curly brace found
          with no matching left curly brace

        To eliminate the error, avoid loading class definitions
        in the file you source into esql.

o       Informix, as well as Oracle and Ingres, supply no
        prototypes for the functions inserted into the code
        by the preprocessor. Thus, when you attempt to compile,
        CC issues error messages on function calls without
        prototypes.  To use CC, you have to supply your own
        extern C prototypes.

o       The Informix preprocessor may fail to initialize.  If
        this happens, unset the win_fork option.

2.  I'm using an Ingres SQL preprocessor
-------------------------------------------
Using an Ingres SQL preprocessor is different from using other
ESQL preprocessors.

ESQLC from Ingres does not support an option to send its output
(the preprocessed code) to stdout.  The ObjectCenter option
"setopt preprocessor esqlc..." does not work.  Ingres has one
option that allows the output to be sent to the current terminal
with paging using more.

To use Ingres SQL preprocessed code,

        1.  Preprocess the .ec files outside of ObjectCenter

        2.  Load the .c files into ObjectCenter

Do not use ESQL statements in the workspace

By default, the Ingres preprocessor generates # line directives
in comments:

        /* # line 23 "foobar.sc" 1 */

To generate # line directives without the comments, you must
invoke the preprocessor with the -# or -p option.  For example:

        esqlc -p foobar.sc

See Also
----------
options		   for information about ObjectCenter options
preprocessed code  for information about using preprocessed code


NAME
     edit

     invokes your editor at a specified location

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------


SYNOPSIS
     edit
     edit identifier
     edit file
     edit " file " :line
     edit function
     edit line number

DESCRIPTION
     << none >> 
     Loads the current file into  your  editor,  posi-
     tioned at the current list location.

     identifier 
     Loads the file containing the  defining  instance
     for the identifier into your editor, positioned at the loca-
     tion of the definition. (The identifier can be  a  variable,
     typedef,   macro,  or  class/struct/union  tag.  If  the  defining
     instance is ambiguous, nothing is loaded into the editor.)

     file 
     Loads the specified file into your  editor,  positioned
     at the top of the file.

     "file":line 
     Loads the specified file into your editor, posi-
     tioned at the specified line in the file.

     function 
     Loads the file containing  the  specified  function
     definition  into your editor, positioned at the start of the
     function.

     line number 
     Loads the file specified  by  the  current  list
     location  into your editor, positioned at the specified line
     number.

OPTIONS

     The following ObjectCenter options affect the edit command:

     cxx_suffixes
     Specifies file extensions to search for when ObjectCenter needs 
     to find a C++ source file that corresponds to a given object file.

     editor (Ascii ObjectCenter only)
     By default this option is unset. Set it only if there is no edit 
     server in your environment. Possible values are vi and emacs.

     path
     Specifies the search path for editing files.

     See the options entry for more details about each option.
     ObjectCenter does not support these options in process debugging
     mode (pdm).

     See the options reference page for more details  about  each
     option.  ObjectCenter  does not support these options in process
     debugging mode (pdm).

USAGE
     Use the edit command to facilitate quick debug-edit-run tur-
     naround times by invoking your editor (specified by the edi-
     tor option) to edit a file at a specified location.  In  all
     cases, once the editor is invoked, the current list location
     is set to the file and line number edited.

SEE ALSO
     edit server

NAME
     objectcenter

     shell command to invoke the ObjectCenter 
     programming environment

SYNOPSIS
     objectcenter [ switches ] 
     objectcenter [ switches ] project_file

DESCRIPTION
     [switches]		      Use the -ascii, -motif or -openlook   
     			      switches for the Ascii, Motif or OPEN 
     			      LOOK versions, respectively.          
     			                                            
     			      By default, if you do not specify an  
     			      interface with any of these three     
     			      switches, ObjectCenter starts as      
     			      either Motif or OPEN LOOK, depending  
     			      on your platform. For example, on     
     			      Hewlett-Packard workstations, the     
     			      default GUI is Motif, and for Sun     
     			      workstations it is OPEN LOOK.         
     			                                            
     			      By default, ObjectCenter starts in    
     			      component debugging mode; to start in 
     			      process debugging mode, use the -pdm  
     			      (process debugging mode) switch.      
     			                                            
     			      In component debugging mode, you load 
     			      all the parts, or components, of your 
     			      program, and link and execute them    
     			      within ObjectCenter. In contrast, in  
     			      process debugging mode, you load your 
     			      program as a fully linked executable, 
     			      and you have the choice of debugging  
     			      it along with a corefile, or          
     			      attaching to another process. See the 
     			      pdm entry.                            
     			                                            
     			      See Table 20 for a complete listing   
     			      of command-line switches you can use  
     			      with ObjectCenter.                    
			  	
     
     [switches] project_file  Start ObjectCenter in component      
			      debugging mode and load project_file.
     
USAGE
     The ObjectCenter startup commands are installed in a
     CenterLine/bin directory, which could be installed anywhere
     on your system. You can start ObjectCenter either by typing
     the absolute pathname of the ObjectCenter startup command or
     by putting CenterLine/bin on your path and just typing the
     command name:
     
     $ objectcenter
     
     See your system administrator if you don't know where
     CenterLine/bin is on your system.

  Startup files 
     When you start ObjectCenter, by default it reads commands
     from the system-wide startup file, named ocenterinit, and
     from the local startup file, named .ocenterinit, which is in
     your home or current directory.
     
     If you start in process debugging mode, ObjectCenter reads
     the .pdminit file only.
     
     You can use the -s[startup_file] and the -S[startup_file]
     switches to tell ObjectCenter to read startup_file instead.

  Local startup file
     The .ocenterinit file is a text file that can contain any
     input that is accepted in ObjectCenter's Workspace.
     Typically, you use .ocenterinit to set the values of
     ObjectCenter options and define aliases that are used across
     ObjectCenter sessions. Since this startup file is read
     directly into ObjectCenter's Workspace, it should contain
     only ObjectCenter commands and code that does not need to be
     debugged or reloaded.
     
     Because ObjectCenter first looks in the current working
     directory for .ocenterinit, you can have different
     .ocenterinit files for use with different projects, as long
     as you work in different directories.

  System-wide startup file
     The system-wide ocenterinit file is in the
     CenterLine/configs directory.
     
     Typically, system-wide attributes (such as the directories
     that ObjectCenter searches for libraries, header files, and
     so on) are set in ocenterinit. If you use different
     directories than the standard defaults, you need to change
     the specifications.
     
     You can also use the ocenterinit file to set options and
     aliases for every user at your site.

     NOTE	ObjectCenter reads the system-wide 
		ocenterinit file before the local .ocenterinit
		file, so any specifications in the local file
		override corresponding specifications in the 	
		system-wide file.

  Using 8-bit character sets
     In component debugging mode (not process debugging mode)
     ObjectCenter supports 8-bit character sets. Add the
     following two lines to your local .ocenterinit file so you
     can use the Meta key to get the extended character set:
     
     setopt eight_bit unsetopt line_meta
     
     To turn on this feature for all users at your site, ask your
     system administrator to add these two lines to the global
     ocenterinit file.

  Libraries loaded when starting
     When starting (in component debugging mode but not process
     debugging mode), ObjectCenter automatically loads the
     standard C++ library, libC.a, and the standard C library,
     libc.a. On some workstations, ObjectCenter might load the
     shared versions of these libraries. See the ObjectCenter
     Platform Guide for your platform for any further information
     about shared libraries.

SWITCHES     
     This section describes the ObjectCenter command-line
     switches.
     
     ObjectCenter processes command-line switches in the order in
     which they are specified.
     
     You can use most of the switches with any version of
     ObjectCenter, although a couple are specific to Ascii
     ObjectCenter. See Table 20 for an alphabetical listing and
     description of all the switches.
     
     NOTE	In addition to command-line switches,
		ObjectCenter supports many options that you can
		use to control its features and commands.  See
		the options reference page for more information.
			       
     Table 20 Command-Line Switches Supported by ObjectCenter
-------------------------------------------------------------------  
Name of Switch  What the Switch Does		       Restrictions
-------------------------------------------------------------------
-ascii	       Start ObjectCenter with the	        None
	       character-based rather than a
	       graphical user interface. Do
	       not use this switch with
	       -openlook or -motif.

               (contents command) In Motif and OPEN 
               LOOK, displays the output of the 
               contents command in ASCII format in the 
               Workspace.  Without this switch, the 
               contents command in Motif and OPEN LOOK 
               invokes the Project Browser.  

               You can use this switch in combination 
               with other contents arguments.

-class_as_struct (debug command) Disables maximum       No effect 
               processing of classes to improve         in cdm
               performance.

-d (Ascii      (load switch). Turns off  	        Ascii only 
ObjectCenter   terminal-dependent output; as a	result, No effect
only)	       raw mode input will be disabled so that	in pdm
	       pressing the Return key is required to 
	       respond to a prompt.                   
 
-Dname[=def]   (load switch). Causes name to become 	No effect
	       defined as if a #define directive	in pdm
	       had occurred. If definition is not   
	       supplied, then 1 is used. For more   
	       information, see the load entry.                   

-f log_name    Saves a copy of all input typed in   	None 
	       the Workspace in a permanent file    
	       called log_name. All input is        
	       usually saved in a temporary logfile 
	       that is deleted when you quit        
	       ObjectCenter. Note that a space is   
	       required between the switch and the  
	       argument for the switch.             

-full_symbols  (debug command). Forces the reading      No effect
               of the full symbol table for maximum     in cdm
               information immediately.

-G	       (load switch). Ignores debugging	    	No effect  
	       information, produced by the -g		in pdm
	       switch of the compiler, when loading
	       compiled files. For more            
	       information, see the load entry.           

-Iheader_path  (load switch). Adds directory_name	No effect  
	       to the list of directories to search	in pdm
	       for files specified by the #include  
	       preprocessor directive. For more     
	       information, see the load entry.     

-i input_file  Specifies that ObjectCenter's	    	No effect  
	       command input should be read from	in pdm  
               the input_file, rather than from        	    
	       standard input. Note that a space is
	       required between the switch and the 
	       argument for the switch.            

-list          (link command) Echos the library list    No effect 
               order to the Workspace.  This switch     in pdm 
               is useful for diagnosing link-order 
               related problems in the interpreter. 
               The link command makes no links.

-Llibrary_path (load switch). Adds library_path to  	No effect 
	       the list of directories to search    	in pdm    
	       for libraries. For more information, 
	       see the load entry.                  

-llib_name     (load switch). When a library is     	No effect 
	       loaded using the -l x format, then a	in pdm
	       file called libx.a is sought first    
	       in the directories specified by -L    
	       options, then in the standard         
	       directories /lib, /usr/lib, and       
	       /usr/local/lib. For more              
	       information, see the load entry.      

-m target      Indicates that ObjectCenter should    	No effect  
	       perform a make on target when		in pdm    
	       starting up. This is equivalent to   
	       entering make target as the first    
	       statement in the Workspace. Note     
	       that a space is required between the 
	       switch and the argument for the      
	       switch.                              

-motif	       Start ObjectCenter with the Motif    	None   
	       graphical user interface. Do not use 
	       this switch with -openlook or        
	       -ascii.                              

-no_fork       Create a separate Run Window but avoid   None
               returning immediate control to the 
               shell.  With -no_fork, control returns 
               when you enter ^Z in the shell or exit 
               ObjectCenter.  Without -no_fork, the shell 
               prompt comes back immediately.

-no_run_window Avoids creating the separate Run Window  None
               and avoids returning control to the 
               shell.  Your program's output goes to 
               the shell in which you invoked 
               ObjectCenter. Using the -no_run_window 
               switch means you are unable to interrupt 
               ObjectCenter and unable to place it in 
               the background. This option is intended 
               for debugging applications that need 
               specific terminal support rather than a 
               generic terminal such as xterm.

-o output_file Specifies that ObjectCenter's            No effect 
	       command output should be written to      in pdm    
	       output_file, rather than to standard 
	       output. Note that a space is         
	       required between the switch and the  
	       argument for the switch.             

-openlook      Start ObjectCenter with the OPEN	    	None 
	       LOOK graphical user interface. Do  
	       not use this switch with -motif or 
	       -ascii.                            

-pdm	       Start ObjectCenter in process	  	None 
	       debugging mode. See the pdm entry 
	       for more information.             

-r number      Specifies the size of the run-time       No effect
	       stack as number nested function          in pdm   
	       calls. The default size is           
	       approximately 1000 nested function   
	       calls. The default size may need to  
	       be increased when executing highly   
	       recursive programs. Note that a      
	       space is required between the switch 
	       and the argument for the switch.     

-S [startup_file]  If startup_file is supplied, it  	None
   	       is read at startup instead of the        
	       default system startup file. If a    
	       file name is not specified, the      
	       system startup file is ignored. Use  
	       a dash ( - ) to indicate that there  
	       is no startup file. Note that a      
	       space is required between the switch 
	       and the argument for the switch.     

-s [startup_file]   If startup_file is supplied, it 	None
	       is read at startup instead of the   
	       .ocenterinit file, which is the     
	       default startup file. If a file name
	       is not specified, the default       
	       startup file is ignored. Use a dash 
	       ( - ) to indicate that there is no  
	       startup file. Note that a space is  
	       required between the switch and the 
	       argument for the switch.            

-Umacro_name   (load switch). Causes the predefined     No effect 
	       macro_name to become undefined as if     in pdm    
	       a #undef directive had occurred.     

-usage	       Displays a table of switch	    	None
	       abbreviations and arguments.

-w	       (load switch). Suppresses reporting  	No effect 
	       of warnings; errors are always       	in pdm    
	       reported. For more information, see  
	       the load entry.                      
 -------------------------------------------------------------------
     NOTE	ObjectCenter no longer supports the -p switch.
		By default, all file descriptors that are open
		when you start ObjectCenter remain open during
		your session.
		          
     See Table 21 for a list of switches that you can use with
     the objectcenter command along with the -motif or -openlook
     switches. These command-line switches allow you to fine tune
     your windowing environment without having to edit any files
     specifying X resources.
     
  Table 21 Switches to Specify Graphical User Interface 
			from Command Line
-------------------------------------------------------------------
Name of Switch and 	    What the Switch Does       		
Arguments, if any	                                      
-------------------------------------------------------------------
-background color	    Specifies background color.         
-bg color		                                        
			                                        
-config pathname	    Uses the X resource specifications  
			    in pathname instead of the defaults.
			    See the X resources entry for more  
			    information.                        
	
-debug			    Enables protocol error handler. If  
			    this switch is specified, any X     
			    protocol error or fatal OITM error  
			    causes an error message to be       
			    printed on stderr followed by a core
			    dump. Note that running ObjectCenter
			    with a command line of -debug is    
			    different than compiling with a flag
			    of -debug. If the command-line      
			    argument -debug is not specified,   
			    the error messages still print when 
			    these errors occur, but a core dump 
			    is not produced.                    

-display host:dpy.scn	     Specifies X server to connect to. If  
			     -display host:dpy.scn is specified,   
			     the program's display is targeted     
			     for machine host on the network, on   
			     display and screen dpy.scn. If this   
			     argument is not specified, the        
			     display is taken from the             
			     environment variable DISPLAY, if it   
			     exists; otherwise, the display is     
			     targeted for the originating host,    
			     display and screen using unix:0.0.    

-fastdraw		     Tells ObjectCenter to sacrifice     
			     appearance for faster drawing. If   
			     -fastdraw is specified, the         
			     appearance of objects drawn on the  
			     screen will be compromised for      
			     faster drawing. This is useful if   
			     your program is displaying on an X  
			     terminal over an RS232 line.        

-font font_name		     Specifies default text font for 
			     all objects in the GUI.         

-foreground fgcolor	     Specifies foreground color.
-fgcolor

-iconic			     Tells ObjectCenter to start in 	
			     iconic state.                  

-name name_string	     Specifies name_string as the name    
			     for this instance of program. If      
			     -name name_string is specified,       
			     name_string will be the value of the  
			     instance portion of the WM_CLASS      
			     property for this instance of the     
			     execution of the program. If -title   
			     is not also specified, name_string    
			     will be the value of the WM_NAME      
			     property, and will be displayed in    
			     the title bar of the main             
			     application window (assuming the      
			     window manager uses WM_NAME).         

-ol			     Tells ObjectCenter to use the most   
			     appropriate OPEN LOOK model. If the  
			     monitor is monochrome, the 2-D model 
			     is used; if the monitor is color,    
			     the 3-D model is used.               

-ol2d -openlook_2d	     Tells ObjectCenter to use the 2-D
			     OPEN LOOK model.                 

-ol3d -openlook_3d	     Tells ObjectCenter to use the 3-D    
			     OPEN LOOK model.                      

-reverse -rv		     Tells ObjectCenter to reverse    	
			     foreground and background colors.

-xrm`resource_string:value'  Sets the X resource resource_string 
			     in the X resource database to the   
			     value string.                       
--------------------------------------------------------------------
     NOTE	See the X resources reference page for more 
		information about setting up your X 
		environment.

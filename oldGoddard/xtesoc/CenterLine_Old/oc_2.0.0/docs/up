NAME
     up

     moves up the execution stack

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------



SYNOPSIS
     up
     up number

DESCRIPTION
     << none  >>	Moves the current scope location up
 			one level on the execution stack.  
     
     			Motif and OPENLOOK: Source panel shows 
 			file scoped to location and highlights 
 			it with an arrow.                      

     number		Moves the current scope location 
 			the specified number of levels up
     			the execution stack.             
     
USAGE
     Use the up command to move the current scope location up the
     execution  stack,  toward the top level of the Workspace and
     away from the current break level.

     The scope location is the  point  at  which  all  variables,
     types,  and  macros  are  scoped. When a break level is gen-
     erated, the scope location is set to the point at which exe-
     cution was interrupted.

     When at a break level, use the where command to display  the
     execution  stack.  Use  the  whereami command to display the
     break location and the current scope location.

     The cont command can be used to continue execution, and  the
     reset  command  can  be  used  to return to a previous break
     level or to the top level of the Workspace without  continu-
     ing execution.

SEE ALSO
     cont, down, reset, where, whereami

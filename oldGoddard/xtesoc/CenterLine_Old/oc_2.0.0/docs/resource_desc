NAME

  Resource Descriptions

     This section describes the most frequently used X resources and 
     possible values.

     ObjectCenter*Color*OI_scroll_text.@text.Background  
        Sets color resources for scrolling text objects.             

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*Color*Workspace.@text.Background  
        Sets the workspace colors.  

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*ConfirmSelnUse
	By default, when you select a command on a menu, such as
	the Examine menu, ObjectCenter uses the current text
	selection as input to the command. Alternatively, the GUI 
	can present a dialog box showing the current selection, 
	which you can edit before applying it as input to the 
	command. If you want this alternative behavior, set the 
	value of this resource to True.

	Values: True, False (default).

     ObjectCenter*DefaultKey
	Defines the accelerator key used to activate the default 
	cell in a menu. When a cell is found with an accelerator that
	matches this key, the cell is made the default, and no 
	accelerator label is displayed for it.

	Values: Any valid key.

     ObjectCenter*dimButtonsWhenDebuggerBusy  
        Specifies the length of time that the debugger must be
        busy for the control buttons on the GUI to dim.  The 
	default value is 1.15.  The value of this resource can 
	be:
        
        o  The string Always if you want the buttons to dim as soon 
	   as the debugger is busy.

        o  The string Never if you never want the buttons to dim.

        o  Any positive floating-point number, to indicate the number 
	   of seconds you want to elapse before the buttons start 
	   dimming.

	Values: Always, Never, <number> (default 1.15).

		   ObjectCenter*FixedWidthBoldFont
        Changes the font of the top-level entries.

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*FocusPolicy
	Specifies the focus policy to use. By default, the Motif GUI
	uses click_to_type, and OPEN LOOK uses follows_pointer. See
	the "Setting the keyboard input-focus" section for more 
	information.

	Values: click_to_type, follows_pointer.

     ObjectCenter*Font
	Changes fonts globally. See the "Examples of changing fonts
	in particular components" section.

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*HelpTranslations
	Specifies the set of keys eligible for use as the help key.
	See the "Changing the help key" section for more information.

	Values: Any valid sequence of keys and action functions.
 
     ObjectCenter*MainWindow*ExamineMenuUsesPopups
        Specifies whether or not the Print, Whatis, and Whereis
        menu items on the Examine Menu display their output in
        the Workspace (default) or in a special popup window.

        Values: True, False (default).
	
     ObjectCenter*Model
	Specifies the interaction model to use. The default setting
	for this resource is platform-specific. For instance, 
	Hewlett-Packard workstations use motif, and Sun workstations
	use openlook.

	Values: motif, openlook, openlook2d, openlook3d.

     ObjectCenter*MotifPushpin
	Uses a `screw' to simulate OPEN LOOK pushpins. If you want 
	a pushpin on dialogs in Motif, set this resource to True.
	See the "Adding pushpins to the Motif GUI" section for more 
	information. 

	Values: True, False (default).

     ObjectCenter*OI_entry_field.Font
	Overrides the fonts for all single-line text fields.

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*OI_multi_text.Font
	Overrides the fonts for all multiple-line text fields.

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*OI_scroll_text.Font
	Overrides the fonts for all scrollable text boxes.

	Values: As specified by XLFD (X11 Logical Font Description).

     ObjectCenter*usePanner 
	The Inheritance Browser, Data Browser, and Cross-Reference 
	Browser use a scrollbar by default. To make them use a 
	panner instead, set this resource to True.

	Values: True, False (default).

     ObjectCenter*WMIgnoresPPosition
	Notifies ObjectCenter that the window manager used does not
	properly interpret the PPosition bit in the WM_NORMAL_HINTS
	property. Set this resource if you are using mwm as a window
	manager.

	Values: True, False (default).

     ObjectCenter*workspaceTranscriptSize
	Specifies the maximum number of lines available for the
	Workspace. The default setting is 2000. Setting the value to 0
	means there is no maximum; that is, the number of lines in the
	Workspace can grow indefinitely.

	Values: Any integer greater than or equal to 0.

     ObjectCenter*XrefBrowser*showReturnType 
        Specifies whether or not the Cross-Reference Browser shows the 
	return type for functions.

	Values: True, False (default)
                                          
SEE ALSO
     resource examples, X resources




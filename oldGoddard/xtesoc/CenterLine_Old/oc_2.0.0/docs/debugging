
NAME
     debugging

     balancing speed and other trade-offs with various ObjectCenter
     debugging capabilities

     ObjectCenter allows you to  perform  the  following  kinds  of
     debugging activities:

     o    Load-time error checking of source code

     o    Run-time error checking of source and object code

     o    Interactive source-level debugging of  source,  object,
          library, and a.out files

     o    Code  visualization  through  examination   of   cross-
          references and data variables

     Different kinds of debugging help you find  different  kinds
     of  errors.   Also,  there  are trade-offs with each kind of
     debugging; for instance, you get  the  most  thorough  error
     checking  when  you load source code, but source code is the
     slowest to execute in ObjectCenter.

     We assume that you will use the various kinds  of  debugging
     at various stages of the development process. In the rest of
     this entry, we describe scenarios that illustrate  different
     ways  to  balance  speed  of  execution  with run-time error
     checking and debugging capabilities. We also provide a brief
     overview of all the debugging activities listed above, along
     with  performance  considerations   associated   with   each
     activity.

Loading source versus object code versus executables

     See Table 10 for brief information about  several  scenarios
     for  loading  your  code;  each scenario balances speed with
     run-time error checking and debugging in a different way. We
     discuss  each  scenario  in more detail in the next few 
     sections.

     NOTE	For the best performance in most debugging 
		scenarios, we recommend that you compile object
		files 	 with the -dd=on compile switch.  For
		browsing and developing new code, on the other 
		hand, you might want to compile with -dd=off. 
		See the demand-driven code generation entry
		for more information about this	feature.

    	 Table 10 Six Debugging Scenarios Showing Trade-Offs for Loading Source vs. Object Code
     --------------------------------------------------------------------------------------------
     Files Used in this Scenario                 Debugging Considerations with this Scenario
     --------------------------------------------------------------------------------------------
                                                 Run-Time Error   Speed of              Debugging
                                                 Checking         Execution             Available
                                                 ------------------------------------------------
     #1  Load all modules as source files        excellent        slowest               excellent

     #2  Load 1 or 2 as source files; load the   very good        somewhat less         excellent
         rest as instrumented object code (a)                     than full speed

     #3  Load all files as instrumented object   good             somewhat less         good
         code                                                     than full speed

     #4  Load all files as regular object code   minimal          full speed            good
         with debugging information
         (compiled with -g switch)

     #5  Load all files as regular object code   none             full speed            minimal
         without debugging information
         (compiled without -g, or loaded
         with -G)

     #6  Load the a.out file in pdm              none             full speed            good
     -------------------------------------------------------------------------------------------
     (a)     See the "Run-time error checking" section
             for a definition of instrumented object
             code.

  Scenario #1: Maximizing number of errors reported

     Suppose your goal is to catch as  many  run-time  errors  as
     possible,  no  matter  what the cost in processing speed. In
     this case, you could load your program  entirely  as  source
     code,  correcting  or  suppressing  load-time warnings, then
     build and run your program.

     This scenario costs the most execution time, but  it  yields
     the most complete set of run-time errors.This scenario is
     really useful only with small projects. Some projects may be
     so large that this scenario is not possible.


  #2: Getting fewer run-time errors but increasing speed

     The second scenario is much more typical for a large C++
     program; we recommend either this scenario or the third
     scenario for most applications.

     Let us assume that you cannot afford the execution time to
     load and run your program entirely as source code, but you
     suspect that one or two source modules are likely to have
     errors. In this scenario, you load the suspect modules as
     source and the other modules as instrumented object code.
     Then, when you link and run your program, you will
     probably catch many, if not all, run-time errors, and your
     program's execution time will not be nearly as great as in
     the first scenario.

  #3: Increasing speed even more

     To improve speed even more than the  second  scenario,  load
     your program entirely as instrumented object code. Your pro-
     gram will run only slightly slower than the  full  speed  of
     the  machine,  and you will get much, though not all, of the
     run-time error checking  and  debugging  capabilities  Object-
     Center  provides.  This scenario may be the most typical and
     effective way to use ObjectCenter for many users.

  #4 and #5: Maximizing speed of execution

     To maximize speed, load your code as  regular  object  code,
     with  or without debugging information. You will not be able
     to do any run-time error  checking,  but  you  can  do  some
     source-level  debugging--the  amount  depends on whether you
     load debugging information or not.

  #6: Fastest startup

     When startup and execution time are the most important  con-
     siderations and run-time error checking and incremental tur-
     naround times for changes are not important, start your pro-
     ject  using  pdm  and  load your code as an a.out file. This
     results in reasonably good debugging facilities and  maximum
     speed of execution. Using pdm is especially useful for find-
     ing bugs quickly in existing code, rather than for debugging
     new code as you develop it.

Kinds of debugging supported

     See Table 11 for a summary of the various kinds of debugging
     supported by ObjectCenter, the types of files you need to
     load for each kind, and the benefits provided by
     ObjectCenter's various debugging tools.


      Table 11 Kinds of Debugging Supported by ObjectCenter
     -------------------------------------------------------------------------------------------
     Debugging Activity             Kinds of Files            What ObjectCenter Does
     -------------------------------------------------------------------------------------------

     Load-time error checking       source code               Issues errors or warnings for the
                                                              following conditions: (a b)

                                                              I/O errors
                                                              Illegal characters
                                                              Illegal constant formats
                                                              Illegal escape sequences
                                                              Lexical constant overflow
                                                              Improper comments
                                                              Preprocessing violations
                                                              Macro expansion violations
                                                              Syntax errors
                                                              Illegal statements
                                                              Illegal expressions
                                                              Undefined identifiers
                                                              Unused variables
                                                              Improper type specifiers
                                                              Illegal bitfield declarations
                                                              Declaration violations
                                                              Illegal parameter declarations
                                                              Initialization violations
                                                              Redefinition violations
                                                              Linking violations
                                                              Variable warnings
     -------------------------------------------------------------------------------------------

     Run-time error checking        source code              Issues errors or warnings for the following
                                                             conditions: (a)

                                                             Undefined/questionable arithmetic operations
                                                             Undefined/illegal pointer operations
                                                             Enumerator warnings
                                                             Losing information during
                                                             conversions/assignments
                                                             Function warnings
                                                             Storage warnings

     Run-time error checking        source code              Issues errors or warnings for the following
                                    regular object code      conditions: (a)
                                    instrumented object
                                    code (c)                 Memory allocation warnings
                                                             Miscellaneous warnings (bad arguments to
                                                             strcpy, strcmp, bzero, longjmp)

     Run-time error checking        source code              Issues errors or warnings for the following
                                    instrumented object      conditions: (a)
                                    code (c)
                                                             Using memory that has not been set
                                                             Addressing errors (pointer dereference,
                                                             alignment, array index errors)
     ---------------------------------------------------------------------------------------------------------
     Interactive source-level       source code              Allows you to do all of the following:
     debugging                      object code
                                    a.out (with core         o       Start your program under varying
                                    or process)                      conditions that might affect its behavior
                                                             o       Stop your program on specified conditions
                                                             o       See what has happened when your program
                                                                     has stopped
                                                             o       Change your program, so you can try out
                                                                     solutions to problems you discover

     Code visualization             source code              Displays static cross-references for functions or
     (Cross-Reference Browser)      object code              variables in source and object files.

     Code visualization             source                   Allows you to examine values of data variables,
     (Data Browser)                 object                   including complex pointer structures.
                                    a.out
     ---------------------------------------------------------------------------------------------------------
     (a)     Use the Man Browser to see a list of messages in each
             category; issue the man command in the Work space and
             select the "violations" topic.
     (b)     Use the config_c_parser command to set the compiler
             configuration to be used for error checking.
     (c)     See the instrument and uninstrument commands for more
             information.

Performance considerations
     In this section we describe the various kinds  of  debugging
     activities shown in Table 11 in terms of performance.

  Load-time error checking
     Load-time error checking allows you to discover compile-time
     errors in your code as well as hazardous but legal usages of
     the C language.

     If you want to use the load-time error checking features  of
     ObjectCenter,  you  must load your code as source, even though
     this means your code  will  execute  slowly.  For  load-time
     error  checking,  we  recommend that you load in source form
     only those modules you are checking specifically  for  load-
     time errors; load as much of the rest of your code as possi-
     ble in object form.

  Run-time error checking
     Run-time error checking allows you to  discover  errors  and
     warnings  related  to memory allocation and usage as well as
     miscellaneous problems with enumerators, conversions,  func-
     tion calls, and storage.

     As shown in Table 11,  the  specific  run-time  errors  that
     ObjectCenter  can find in your code depend on whether the code
     is loaded as source, regular object, or instrumented  object
     code.  By instrumented object code, we mean object code that
     has been modified so that it supports run-time error  check-
     ing;  regular  object code has not been so modified. See the
     instrument entry  for  more  information  about  the  errors
     reported for instrumented object code.

     Regular object code that is linked and executed within Object-
     Center runs at nearly the full speed of the machine. Instru-
     mented object code runs somewhat more  slowly  than  regular
     object  code,  and  source  code  runs much more slowly than
     either regular or instrumented object code.

     So, for maximum  speed  with  run-time  error  checking,  we
     recommend  that  you  load  in source or instrumented object
     form only those modules you are  checking  specifically  for
     run-time  errors;  load  as much of the rest of your code as
     possible in regular object form.

  Source-level debugging
     Source-level debugging allows you to examine what is going
     on in a program while it executes. This kind of debugging is
     available in either of ObjectCenter's two modes, which are
     component debugging mode and process debugging mode.  In
     component debugging mode, you load a program into
     ObjectCenter as any combination of source code, object code
     (instrumented or regular), and library files that you link
     and execute within ObjectCenter.  In process debugging
     mode, you load your program as a fully linked executable.

     For maximum speed of source-level debugging, load your files
     as regular object code in component debugging mode or as an
     a.out file in process debugging mode. In either case, your
     program will run at or near the full speed of the machine.

Differences in source-level debugging 
     The debugging features available in ObjectCenter vary
     somewhat according to whether you have loaded source,
     object, or a.out files.

  Source-level debugging of a.out files
     As previously mentioned, to perform source-level debugging
     with a.out files, you must use ObjectCenter's process debug-
     ging mode (pdm). Most ObjectCenter commands are exactly the
     same, whether or not you are in process debugging mode, but
     there are some differences. See the pdm entry for more
     information.

     Also, the debugging information in the symbol table in an
     a.out file varies according to whether or not you used the
     -g switch when you compiled the object modules that you
     linked to create it. See the debug entry for more informa-
     tion about loading an a.out file.

  Maximizing debugging capability with object code
     You can load object code with and without debugging informa-
     tion.   To  get  maximum  debugging  capability, load object
     files that have been compiled with the -g compiler switch.

  Source-level debugging of source code vs. object code
     ObjectCenter  provides  very  similar  source-level  debugging
     capability  for  object  code  as  for code loaded in source
     form, as long as the object code is  loaded  with  debugging
     information. There are, however, a few differences.

     If you load object files with debugging information:

     o    You cannot use the stepout command when you are stopped
          in object code.

     o    You cannot trace execution through object code  as  you
          can in source code.

     o    Some forms of the action command have  no  effect  with
          code loaded in object form.

     In addition, if  you  load  object  code  without  debugging
     information:

     o    You can stop on or set an action on  a  function  name,
          but you cannot stop on or set an action on a particular
          line.

     o    You cannot step through the object code.

     To examine some variables in object code, you must load the
     header file that defines them. To work around the following
     limitations, load a header file with the appropriate
     declarations, or swap one of the object files to source:
     
     o You cannot get class and function template information
       from code loaded in object form.
     
     o References are treated as if they are pointers.
     
Special considerations with C++ object files
     Because of the way that C++ object files are generated,
     there are special considerations when examining some objects
     that are loaded in object form.
     
   o Setting breakpoints in inline functions
     
     You can set breakpoints in inline functions loaded in object
     form only if the file was compiled using CC's +d switch. The
     +d switch tells CC not to expand inline functions. (You can
     always set breakpoints in inline functions loaded in source
     form.)
     
   o Functions taking variable arguments
     
     When cfront generates C files from C++ files for
     compilation, it discards ellipses in function definitions
     because some C compilers don't support them. Ellipses
     indicate that a function takes a variable number of
     arguments.
     
     This means that if you call from source code such a function
     that is loaded in object form (with debugging information),
     ObjectCenter will issue an erroneous warning because the
     number of arguments does not match the number specified in
     the object file.
     
     Because the violation is a warning (not an error), you can
     simply suppress it and continue.
     
   o Constants
     
     When the C++ translator compiles a const object, it puts the
     compile-time value, not the identifier, in the object code.
     That means that you cannot examine const objects that are
     loaded in object form.
     
     For example, say that the file const.C has the following
     definition:
     
     const int i;
     
     If that code is loaded in object form, you will get an error
     if you try to examine i:

     -> load const.o
     Loading: const.o
     -> whatis i
     'i' is undefined.

     This is true even if the file is loaded with debugging
     information.
     
   o Enumerated types
     
     If enumeration data types are loaded in object code that
     contains debugging information, you can examine the data
     type and variables of that type.
     
     For example, say that enum.C has the following definition:
     
     enum colors {red, green, blue} mycolor;
     
     If that code is loaded in object form with debugging
     information, you can examine colors:

     -> load enum.o
     -> whatis colors
     enum colors {
       red=0,
       green=1;
       blue=2
     };

     When you examine a variable of an enumeration type,
     ObjectCenter tells you that it is an integer and returns the
     integer value of the enumeration:

     -> mycolor;
     (int) 0

     You can specify the enumerators (members) if the object file
     is loaded with debugging information:
     
     -> mycolor=blue;
     (int) 2
     
     However, if the object file is loaded without debugging 
     information, you cannot examine the enumerated data type:
     
     -> load -G enum.o
     Loading: -G enum.o
     -> whatis colors
     'colors' is undefined.
     
     You can examine variables of the enumerated type when they
     are in object code without debugging information, but you
     can only assign integral values to them; you cannot specify
     the enumerators:
     
     -> mycolor;
     (int) 0
     -> mycolor=2;
     (int) 2
     -> mycolor=blue;
     Error #718: blue undefined.
     
   o Functions that return void
     
     When the C++ translator compiles functions that are defined
     as returning void, it changes their definition to functions
     returning char (it does this because not all C compilers
     accept void as a return type).
     
     For example, say you have defined a function foo() in enum.C
     as follows:
     
     void foo() { /* body */ }
     
     If the file is loaded in object form, ObjectCenter will tell
     you the function returns char:
     
     -> load enum.o
     Loading: enum.o
     -> whatis foo
     
     -> whatis foo
     char foo() /* defined */

Using object code without debugging information
     When a call to an object code function without debugging
     information is displayed, the formal parameters for the
     function are not listed because they are not known by
     ObjectCenter. You can display the parameters by specifying a
     prototype for the object code function.

Minimizing load time and memory needed for object code
     Object code with debugging information  requires  much  more
     memory  and  loads  more  slowly,  so if memory or speed are
     issues, load object code without debugging information.  You
     can do any of the following:

     o    Load an object file that was not compiled with  the  -g
          switch.

     o    Specify the -G switch with the load command to load the
          object  files  even  faster.  The  -G  switch makes the
          loader skip the -g debugging information in the  object
          file.  However,  the only debugging you will be able to
          do on the resulting object code is to  set  breakpoints
          and actions on a particular function.

  Consolidate object files
     Another way to speed up  the  load  process  is  to  combine
     object  files  that  are  not changing often into one larger
     object file. To do this, invoke the UNIX linker as follows:

     % ld -r -o all.o file1.o file2.o file3.o ...

     This creates one large object file  called  all.o  from  the
     several  smaller  files  named  file1.o, file2.o, and so on.
     Loading all.o into ObjectCenter is  faster  than  loading  the
     individual object files.

  Turn on the save_memory option
     When the save_memory option is set, ObjectCenter does not  use
     extra  memory  for  run-time  type checking when the program
     allocates memory. As  a  result,  your  code  requires  less
     memory,  but  you  will lose some run-time warnings. If you
     use this option, set it before loading your program.

  Set instrument_space option to 0
     The instrument_space option specifies the  amount  of  space
     reserved  for  instrumenta tion of object files. The default
     value of this option is 2, which corresponds to an amount of
     space  approximately half the size of the text space of your
     application. Setting this option  to  0  saves  memory,  but
     removes the ability to instrument the object file.

Debugging multiple processes
     You can debug multiple processes in ObjectCenter. If your
     program calls fork(), the child process appears in a
     separate window and shares a Run window with the parent
     process. You can set breakpoints in the parent and child
     independently.  However, in the child process, your code
     must be loaded as source to set breakpoints; the parent
     process can be loaded as source or object code to set
     breakpoints. 	

  If the child process does an exec
     If the child process does an exec and you want to debug the
     child program, you must modify the call to exec...() so that
     objectcenter is executed instead of the child program. Here
     is an example of a modified exec:

     if (fork() == 0)
     {
     printf( " In the child\n " );
     #ifdef __OBJECTCENTER__
     execlp( " objectcenter", " objectcenter", " -motif",0);
     #else
     execlp( " child_prog ", " child_prog ",0);
     #endif
     }

     After this code is executed, you can move the mouse  to  the
     new  Workspace  window,  load  the source code for the child
     program you want to debug, then run it.

     NOTE:   You cannot use object code debugging for
             the child program in programs that fork;
             you can set breakpoints and step through
             code  only  in  source code in the child
             process.


  If your program fails to fork
     In order to fork another ObjectCenter window, the win_fork 
     option must be set to TRUE (the default). To check if win_fork
     is set, enter this command in the Workspace:

     -> printopt win_fork

     If win_fork is set to FALSE, enter this command to set it to 
     true:

     -> setopt win_fork

     Failure to fork can also occur because there is not enough 
     swap space for two copies of ObjectCenter.

     If you are using fork() followed by exec...(), using vfork()
     instead will help. Here is a modification of the above code,
     which invokes Ascii ObjectCenter instead of the Motif
     version, as in the previous example :

     if (fork() == 0)
     {
     printf("In the child\n " );
     #ifdef __OBJECTCENTER__
     execlp( " objectcenter", " objectcenter", "-ascii",
     " -i/dev/ttyp9 ", " -o/dev/ttyp9 ",0);
     #else
     execlp( " child_prog ", " child_prog ",0);
     #endif
     }

     The arguments to objectcenter are different because a new
     window is not being created automatically. The arguments -i
     device_name and -o device_name name the devices that
     ObjectCenter will use for its input and output,
     respectively.  Here, another terminal window was named to
     act as the console.  Before doing this, you must get the
     correct name of the terminal window, then put it to sleep by
     issuing the following command:

     % sleep 1000 0


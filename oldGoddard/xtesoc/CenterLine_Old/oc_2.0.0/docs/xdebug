Anyone who has written even a trivial X11 application knows that
debugging an X11 application is difficult.  A few simple techniques,
however, can simplify the task dramatically.

Some of these techniques follow.  You may also want to consider
reading the comp.windows.x newsgroup, which gives you access to
thousands of X11 programmers, including many who worked on the
original design and implementation of X.  This newsgroup is a
source of tricks, utilities, and ideas.

Turning off asynchronicity
----------------------------
The X11 client/server model adds unexpected complexity to every
application.  To achieve high performance over a network connection,
X11 buffers, or synchronizes, its requests.  Rather than sending
a request and waiting for its status, X11 holds requests and sends
them as a group.  Because the error status returns long after X11
issued the request, tracking the source of an error is difficult.

Xlib provides two ways to turn off asynchronicity.

o       XSynchronize()

        Inside an X11 application you can turn buffering on or off
        with the XSynchronize() function.  Many applications supply
        a flag to set synchronous mode or set it when you compile
        the program for debugging.   XSynchronize() requires a
        Display argument.

o       _Xdebug in an ObjectCenter action

        If a Display argument is unavailable, use the global
        variable _Xdebug.  _Xdebug controls synchronicity for
        all connections simultaneously and is easy to set within
        a debugger.

        To use _Xdebug, set the _Xdebug variable to a nonzero
        value early in an X11 debugging session after loading
        and linking.  For example, inside ObjectCenter,
        enter

           -> action in main
           Setting action at "xprog.c":10, main().
           Enter body of action.  Use braces when entering
           multiple statements.
           action -> _Xdebug = 1;
           action (1) set.
           ->


        A ObjectCenter action is a piece of C++ code that
        ObjectCenter executes in your program at the point you
        have designated.

        In the example above, the action sets _Xdebug to 1 whenever
        main() executes, such as following a run command, and causes
        X11 to use synchronous communication between the application
        and the X server.

Setting a breakpoint in the Xlib error handler
------------------------------------------------
The fact that many X11 commands occur before the statement with
the error makes finding an X11 error more difficult.

Most applications use the default Xlib error rather than their own
error handler.  If your application uses the default Xlib error
handler, the following is the quickest way to find an error in a
test case.

1.      Set a breakpoint on exit() in the Xlib error handler.

        (The Xlib error handler calls exit() after printing an
        error message.)

2.      Rerun the test case.

        When the error occurs, the Xlib error handler calls exit()
        and ObjectCenter produces a break level.  The
        offending function is in the stack backtrace.  You can
        easily determine where the program went wrong.

Using X11 protocol debugging tools
-----------------------------------
Because even small X11 protocol errors generate errors in an X
program, watching protocol requests on the network can be useful.
Two tools enable you to watch X11 requests, replies, and events.
One is xscope, and the other is the combination of xmonui and
xmond.

o       xscope

        xscope sets itself up as another server on your system
        (usually as display "hostname:1").  It passes requests
        and replies between your program and the server and prints
        them as it passes them.  Although xscope is useful, it
        generates so much output that finding the problem in all
        the traffic may be difficult.

o       xmonui and xmond

        xmonui and xmond perform like xscope, but they allow for
        sophisticated filtering.  To use xmonui and xmond

        1.      Enter this at the shell.

                        % xmonui | xmond

                A window appears with a number of toggles.  You can
                choose the classes of events to show, the replies
                you are interested in, and the level of detail you
                want.

        2.      In a session, start up the xmon tools.

                By default, xmon tools have output turned off
                except for error messages.

        3.      Run your application up to some point before the
                error occurs and turn on printing of the particular
                requests that are causing problems.

                When you restart your program, you see only the
                traffic which interests you.

Both xscope and the xmon tools are part of the contrib section of
the MIT X11 releases.  You can get them by anonymous ftp from
export.lcs.mit.edu in /pub/R5untarred/contrib/{xscope,xmon}.
We highly recommend them to serious X11 programmers.

If you use tools such as xscope and xmonui and xmond regularly,
consider looking into Volume 0 of the O'Reilley X Window System
Series.  This volume covers the X protocol in detail and contains
the complete list of reasons for failure, whereas other books
(including the otherwise excellent Volume 2 of the O'Reilley
series) contain only the most common ones.

Working around the Server/Debugger-Deadlock Problem
-----------------------------------------------------------
Many developers avoid dealing with the raw Xlib routines, preferring
higher-level abstractions such as Xt and toolkits such as Motif.
These toolkits have their own foibles.  Although applications using
higher-level libraries hide details from the user, implementation
details can become painfully obvious when debugging.

One such painful detail of some applications like Motif is grabbing
the server during some operations.  For example, if you generate a
break level inside a Motif operation that has grabbed the server,
the server postpones interacting with any other applications until
the Motif application ungrabs the server.

If you're using a windowed debugger or dbx running inside an xterm,
a classic deadlock condition occurs:  you cannot continue execution
of the Motif program without giving commands to your debugger, and
your debugger is unable to talk to you while the Motif program has
the server grabbed.

To work around the server/debugger-deadlock problem, try one
of the following:

o       Run the debugger on one machine and point your application
        to a different display (via "run -display ...").  The
        server Motif grabs is not the one displaying
        ObjectCenter.

o       Run Ascii ObjectCenter on a separate ASCII terminal
        plugged into a serial port on the workstation.

o       Set a breakpoint in ObjectCenter.

        Some debugging tools like ObjectCenter are powerful
        enough to execute commands automatically at a break level.
        ObjectCenter interprets an arbitrary chunk of C
        code if you make it available to the display command(1).
        By setting up the proper function you can automatically
        release any grabs made by your program or your program's
        toolkit.  For example:

                /* this function forcibly ungrabs the server */
                #include <Xlib.h>
                extern Display *Dpy; /* global Display variable */
                int ungrab_display()
                {
                   if (Dpy)
                      XUngrabServer(Dpy);
                   return(0);
                }

        If this function is loaded, you can type the following
        into the Workspace:

                -> display ungrab_display();

        When an error or breakpoint occurs, ungrab_display()
        executes, Motif ungrabs the server, and you can continue
        debugging.

        This technique takes advantage of the way display operates.
        Usually, you use display to print the current value of a
        variable.  In this case, you are more interested in the
        function it executes than the value it returns.


        Ungrabbing the server may change the behavior of an
        application, since server grabs have side effects on how
        X11 distributes events to windows at its lowest levels.


Ungrabbing the keyboard or mouse
----------------------------------
To find out how to ungrab the keyboard or mouse, refer to the
article "Using Saber C with X Clients that Perform Grabs" from the
Fall 1990 ASAP issue.  For information about how to get a copy,
see "support".

The last example in the article (using XUngrabPointer) has an error.
It should have included the word "display":

        display (XUngrabPointer (dpy, 0), XFlush(dpy));

See Also
---------
display		for information about displaying values
stop		for information about setting breakpoints


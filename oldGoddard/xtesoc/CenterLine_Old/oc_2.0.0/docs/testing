In component debugging mode, you can load a unit of your program
in source or object form and test it in isolation from the rest
of the program. You do not have to load main().

To test a unit of your program,

1.      Load the unit of the program you want to test.

        If the unit is a source file and an object file, enter

                C++ -> load source.C object.o

2.      Link your program.  Enter

                C++ -> link

        If you have unresolved references, you can resolve them
        by loading other pieces of your program or declaring the
        unresolved symbols in the Workspace.  For unresolved
        function calls, you could declare function stubs in the
        Workspace.

3.      Test your program by calling its functions in the
        Workspace.  For example, to test the function foo(),
        enter

                C++ -> foo();

Chapter 5 (Component Debugging) of the hardcopy ObjectCenter
User's Guide discusses interactive unit testing.

See Also
---------
link	    for more information about linking
load	    for information about loading
Workspace   for information about using the Workspace




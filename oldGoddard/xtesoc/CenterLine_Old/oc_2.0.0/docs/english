NAME
     english

     describes a C type in English

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |      no    |
     -------------------------


SYNOPSIS
     english type_expression
     english identifier

DESCRIPTION
     type_expression 
     Displays a prose description for the type of
     the specified type expression.

     identifier 
     Displays a prose description for the type of  the specified  
     identifier (variable, function, class/struct/union tag, or typedef).

USAGE
     Use the english command to clarify a C type expression or to
     display a prose description of the type of an identifier.

EXAMPLE
     The following example indicates both  ways  english  can  be
     used to describe a type:

     -> char *(*func)();
     -> english func
     pointer to function returning pointer to char.
     -> english char *(*)()
     pointer to function returning pointer to char.

RESTRICTIONS

     The english command is available only in C mode.

SEE ALSO
     help, man

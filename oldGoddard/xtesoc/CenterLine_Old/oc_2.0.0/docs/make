NAME

     make

     invokes the UNIX make command to handle CenterLine (CL) 
     targets


     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------




SYNOPSIS
     make
     make target ...

DESCRIPTION
     << none >>		Calls the UNIX make command using the default 
			target. Motif and OPEN LOOK: Shows load-time 
			errors in the Error Browser.                 



     target		Calls the UNIX make command using the target
			argument as its target. Motif and OPEN LOOK:
			Shows loadtime errors in the Error Browser. 



     Note:    Using the make command while you are in
              process debugging mode has the same effect as
              using the make command in the shell; it does
              not recognize any CL target rules. The
              following description of the make command
              applies to component debugging mode only.

OPTIONS
     The following ObjectCenter options affect the make command:

     cc_prog        Specifies the name  of  the  C  compiler
                    that ObjectCenter invokes.

     ccargs         Specifies arguments passed  to  cc  when
                    invoked from ObjectCenter.

     cxx_prog       Specifies the name of the C++ translator to be
                    invoked by ObjectCenter when compiling a
                    C++ file.

     cxxargs        Specifies default arguments passed to the C++
                    translator when invoked by ObjectCenter.

     make_hfiles    Checks header files to  find  out  if  a
                    file should be reloaded.

     make_args      Specifies  the  command-line   arguments
                    passed  to  the  UNIX make by ObjectCenter
                    make command.

     make_offset    Specifies the number  of  characters  to
                    skip  when  reading  shell commands from
                    the make program.

     make_prog      Specifies the program invoked to make an
                    target (the default is make ).

     make_symbol    Specifies the string used  to  denote  a
                    ObjectCenter  command  line in a makefile.
                    By default, the string is the #  charac-
                    ter.

     subshell       Specifies the shell used to invoke the C++
                    translator or C compiler.



     See the options entry in the Man Browser for more details
     about each option.  ObjectCenter does not support any
     options in process debugging mode (pdm).

USAGE
     Use the ObjectCenter make command to load files into Object-
     Center using makefiles containing CL target rules in addi-
     tion to the standard target rules. See the UNIX manpage for
     make for a list of the switches you can use.

     ObjectCenter's EZSTART utility provides a shortcut for
     creating CL targets in makefiles; see the clezstart entry
     for more information.

  What is a CL target rule?
     A standard UNIX make target rule contains shell lines, which
     are  lines containing shell commands. The syntax for a shell
     line is as follows:

     <tab> shell command [; shell command ]

     For example, the following is a shell  line  in  a  standard
     UNIX makefile target:

     <tab>echo "starting a standard target"

     A CL target rule is just like a standard target rule  except
     that  it  contains  one  or  more  CL  lines; a CL line is a
     makefile command line preceded by <tab> #.  ObjectCenter  han-
     dles  CL  lines  as ObjectCenter commands. It passes all other
     lines to the Bourne shell for execution, just as  when  make
     is used outside of ObjectCenter.

     The syntax for a CL line in a CL target rule is as follows:

     <tab># ObjectCenter command

     For example, the following is a CL line in a CL target rule:

     <tab>#load a.o b.o

     The preceding example has the same effect as  the  following
     command issued in the Workspace:

     -> load a.o b.o

     NOTE:   A # character in the first column  in  a
             line  causes  ObjectCenter  to  treat that
             line as a comment, so  you  must  indent
             the # to indicate that a ObjectCenter com-
             mand  follows.   Use  the  Tab  key  to
             indent.


     Here is  another  example  of  a  standard  target  and  the
     corresponding CL target:

     a_standard_target: a.o b.o
        echo "starting a standard target"
        $(CC) $(CFLAGS) a.o b.o

     a_cl_target: a.o b.o
        echo "starting a cl target"
        #load $(CFLAGS) a.o b.o

  Designing a CL target
     To design a CL target that you can add to a makefile,  think
     of  the  ObjectCenter  commands that you want your makefile to
     automate.  For example, if your standard target is the  fol-
     lowing:

     prog: a.o b.o
        echo "starting a standard target"
        $(CC) $(CFLAGS) -o my_program a.o b.o -lm

     then the equivalent CL target would be the following:

     cl_obj: a.o b.o
        echo "starting a cl target"
        #load $(CFLAGS) a.o b.o -lm
        #link
        #setopt program_name my_program

     Note:     See the templates entry for more
               information about templates and how to use
               make with template object modules.

EXAMPLE

     The following is an excerpt from a typical makefile that
     includes two standard targets used directly by CC (.C.o and
     all) and two that are specific to ObjectCenter (ocenter_src
     and ocenter_obj):

     # This is a comment
     # a.C, b.C, and c.C are C++ files

     SRCS = a.C b.C c.C
     OBJS = a.o b.o c.o
     FLAGS = -g -DDEBUG
     .SUFFIXES: .C .o

     # The following is an implicit target that specifies
     # how to convert a .C file to a .o file. In this case
     # CC is called with the switches +d, -g, and -c
     .C.o:
       CC +d -g -c $<

     # The next target creates an executable named all
     # from the three files a.o, b.o, and c.o. If any of
     # the .o files are missing or out of date, they will
     # be compiled, using the implicit target .C.o

     all: $(OBJS)
     CC +d -g -o all $(OBJS)

     # targets specific to ObjectCenter ...
     # note the indented # character

     ocenter_src: $(SRCS)
       #load $(FLAGS) $(SRCS)
   
     # the following loads object files into ObjectCenter,
     # using the implicit target to convert .C to .o

     ocenter_obj: $(OBJS)
       #load $(FLAGS) $(OBJS)

     If you issue the make command within ObjectCenter and provide
     the name of a standard target, ObjectCenter passes the command
     line to the Bourne shell for execution:

     -> make all
     sh CC +d -g -c a.C
     CC  +d a.C:
     cc  -c  -g a.c
     sh CC +d -g -c b.C
     CC  +d b.C:
     cc  -c  -g b.c
     sh CC +d -g -c c.C
     CC  +d c.C:
     cc  -c  -g c.c
     sh CC +d -g -o all a.o b.o c.o
     cc  -L/usr/local/lib  -o all  -g a.o b.o c.o -lC
     ->

     In this example, the .o files did not exist, so the
     .C files had to be compiled. After ObjectCenter
     passes the invocation of CC to the shell, CC
     processes the files, calling cc to compile and link
     them.

  Target rules that call cc, CC, or ld
     If an explicit target rule or an implicit suffix rule causes
     a call to cc, CC, or ld, the corresponding CL target rule
     should issue the load command on the same source or object
     files. Also, you need to supply the same switches with #load
     that you would use with cc, CC, or ld - for instance, the -D
     switch.


  Target rules that call cc or ld
     If an explicit target rule or an implicit suffix rule causes
     a  call  to  cc or ld, the corresponding CL target rule load
     command on the same source or object files. Also,  you  need
     to  supply  the  same switches with #load that you would use
     with cc or ld--for instance, the -D switch.

  CL suffix rules for loading individual files
     You can add implicit rules specific to ObjectCenter for  load-
     ing  individual  files.  For example, consider the following
     makefile fragment:

     FLAGS = -g -DDEBUG
     .SUFFIXES: .c .o .src .obj
     .c.src:
         #load $(FLAGS) $<
     .o.obj:
         #load $(FLAGS) $<

     The first rule specifies that to make a file ending in .src,
     load  a  source file ending in .c. The second rule indicates
     that to make a file ending in .obj, load an object file end-
     ing with .o.

     If you set up your makefile with these implicit  rules,  you
     can  load  individual source or object files by specifying a
     file with a .src or .obj suffix as a target:

     -> make a.src
     load -g -DDEBUG a.c
     Loading: -DDEBUG a.c
     -> make b.obj
     load -g -DDEBUG b.o
     Loading: b.o

  Meta-character
     Before ObjectCenter executes rules that begin  with  a  #,  it
     passes  them  first  through  the Bourne shell, just as make
     does. The subshell interprets all meta-characters and  sends
     the output back to ObjectCenter.

     To avoid the delay when spawning  the  shell,  or  to  avoid
     improper  meta-character expansion by the Bourne shell, pre-
     face the command with two #  characters.  For  example,  the
     following  rule  uses  ##  to  prevent the Bourne shell from
     interpreting  the  left  and  right  parentheses  as   meta-
     characters.

     start:
        #load $(FLAGS) $(SOURCES)
        ##printf("All done\n");

  Other characters
     See Table 18 for a description of the meaning and  usage  of
     various characters in CL targets.

     Table 18 Meaning of Special Characters in CL Targets
     -----------------------------------------------------------------------------------------------------------------------
     Character                             Meaning and Usage
     -----------------------------------------------------------------------------------------------------------------------
     \ character                           On CL lines and shell lines in CL targets, use the backslash to escape EOL in the
                                           same way as you do for shell lines in standard targets.
     (backslash)
                                           Note that a backslash does not escape a space character on a CL line for the load
                                           command.

     @ character                           Execute but do not echo the current line; this does not apply to nmake.

                                           Beginning a shell line in a CL target with an @ character does not interfere with
                                           the ObjectCenter make command's implicit use of the -n option. For example:

                                           any_cl-specific_target: $(X_OBJ)
                                              echo "next line not echoed by UNIX make"
                                              @$(CC) $(CFLAGS) -DX -o xcompile bounce.c\
                                              $(XOBJ) $(XLIBS)

     \" characters                         Use the double CL target symbol (##) to keep escaped quotation marks (\"
                                           being stripped.
     (escaped quotation marks)
                                           For example:

                                           ##load -DTIME=\"three_bells\" new.c

                                           As shown in the example above, even with escaped quotation marks, you cannot
                                           pass a space character on a CL line for the load command. For more information,
                                           see the entry for "space character" next in this table.

     space character                       On a CL line, a space character cannot be passed in an argument for the load
                                           command. For example, there is no exact CL line equivalent of the following
                                           standard shell line:

                                           $(CC) -DTIME=\"three\ bells\" foo.c.

                                           One workaround is to eliminate the space in the macro definition in the
                                           following way:

                                           ##load -DTIME=\"three_bells \" foo.c

                                           This limitation does not, however, apply to passing a space character on CL lines
                                           with ObjectCenter commands other than load. For example, the following CL line
                                           is valid:

                                           #setenv TIME three bell
     -----------------------------------------------------------------------------------------------------------------------

  CL lines that change directories
     If you are designing a CL target that  changes  the  current
     directory,  keep  in  mind  that  the  ObjectCenter cd command
     affects subsequent commands in  the  CL  target  differently
     than  the  cd command affects subsequent commands in a stan-
     dard target.

     In a standard target, since each rule  line  invokes  a  new
     subshell,  a  cd  shell command affects only subsequent com-
     mands on the same line. For example, in the following  stan-
     dard  target,  the CC in the second line of the rule will be
     invoked from the new_dir directory, while the  pwd  in  both
     the  first  and  third  lines  will  be issued in the parent
     directory of new_dir:

     standard_subs:
        pwd
        cd new_dir; $(CC) -c $(CFLAGS) a.c
        pwd

     Since each CL line can have only a single CL command, the CL
     target equivalent for this standard target is the following:

     cl_subs:
        #pwd
        #cd new_dir
        #load $(CFLAGS) a.o
        #cd ..
        #pwd

     The cd new_dir command sets the current  directory  for  the
     Workspace  until the Workspace directory is explicitly reset
     by a new cd command. Therefore, the cd  ..  command  returns
     the  Workspace  to  the original directory so that the first
     and second pwd commands display the same directory.

  CL targets that invoke make
     To invoke make from a CL target, use a shell line and imple-
     ment  the  call  using $(MAKE) -$(MAKEFLAGS). The MAKE macro
     causes the make utility to be executed immediately  so  that
     each  lower-level makefile unwinds in the correct order. The
     MAKEFLAGS macro ensures that the proper switches are  passed
     down from ObjectCenter.

     NOTE:   For recursive invocations of make in
             CL targets, invoke make only from
             shell lines.  That is, avoid the following
             constructions:  #make,  ##make,  #$(MAKE ),
             and ##$(MAKE). Using  these  CL  line  constructions
             to   invoke   make   may   cause  incorrect   recursion
             and   will   give unpredictable results.


     If both the recursive call and the  new  target  being  gen-
     erated  are  in  the same directory, then designing these CL
     targets is straightforward. For example:

     cl_recursive:
        $(MAKE) -$(MAKEFLAGS) CFLAGS=-DFOO stopper
     stopper:
        #load $(CFLAGS) a.o

     However, if the call and the target are in different  direc-
     tories,  you  first  use  a shell line that both changes the
     directory and invokes make. For example, where cl_switch  is
     in  the  makefile in /dir1 and cl_sub2 is in the makefile in
     /dir1/dir2:

     cl_switch:
        cd dir2; $(MAKE) -$(MAKEFLAGS) \
        DIR=dir2 cl_sub2

     Also, in the CL target for the makefile in  the  lower-level
     directory  (here  /dir1/dir2  ),  you need to keep the Object-
     Center  Workspace  synchronized  with  the  current  working
     directory  of  the  shell  from which the recursive make was
     invoked. Synchronize the Workspace by using  a  pair  of  CL
     lines  that  issue  the  cd  command.  For example, with the
     recursive call to make in the target cl_switch shown  above,
     the target in the new directory would use cd commands in the
     following way:

     cl_sub2:
        #cd $(DIR)
        #load $(CFLAGS) a.o
        #cd ..

  Debugging CL targets
     Debug a CL target by using the -n switch as an  argument  to
     the  ObjectCenter make command issued on the CL target you are
     testing.

     When the -n switch is used as an  argument,  the  ObjectCenter
     make command echoes but does not execute the rule.

     Using make -n to debug a CL target  from  the  Workspace  is
     similar  to debugging a standard target from the shell using
     -n with  the  UNIX  make.  After  issuing  make  -n  in  the
     Workspace  on  a  CL  target,  check the listing of commands
     displayed in the Workspace to see if  this  is  exactly  the
     series of commands you want executed by ObjectCenter.

  Loading changes into ObjectCenter
     Use the make command to load your files into  ObjectCenter  at
     the  start  of  a session. If you make changes to files that
     are loaded, use the build command to check the  dependencies
     and  reload  all files that are affected, with the following
     exceptions:

     o    If you make changes that affect only a few  files,  and
          you know which files these are, the fastest way to load
          the changes is with the load command.

     o    If you change a makefile in a way that affects  any  CL
          targets used to load files currently in your ObjectCenter
          session, use the make command.

Compatibility
     This section describes differences between ObjectCenter's make
     and other implementations.

  SHELL makefile variable
     With the UNIX make, the SHELL variable specifies which  sub-
     shell  is  invoked by a standard target. The ObjectCenter make
     command ignores makefile SHELL variable definitions, such as

     SHELL = /bin/csh

     To have the ObjectCenter make command use a shell  other  than
     /bin/sh, set the shell option and redefine the ObjectCenter sh
     command in the following way:

     -> setopt shell /bin/csh
     -> rename centerline_sh centerline_binsh
     'centerline_sh' renamed to 'centerline_binsh'   
     -> int centerline_sh(a) char *a; {
     +> centerline_shell(a);
     +> }

  nmake compatibility
     Using the ObjectCenter make command with the AT&T  version  of
     the  UNIX  make utility ( nmake ) requires the adaptation of
     both CL targets and the ObjectCenter  environment  in  several
     ways.  For  a  detailed discussion of how to implement these
     adaptations for nmake, contact CenterLine Software Technical
     Support  and  request the Support Note Using AT&T nmake with
     CodeCenter and ObjectCenter.

  gmake compatibility
     Unlike many other versions of the  UNIX  make  utility,  GNU
     make  (  gmake  ) filters out and ignores all lines starting
     with a TAB#. To use gmake with the ObjectCenter make  command,
     set  the  make_prog  option to gmake and set the make_symbol
     option to something other than the # character; for example,
     set  make_symbol to the ! character. Then construct CL lines
     using this alternative CL target symbol.

     For example, assume that you make the following settings  in
     the Workspace or in your startup file:

     -> setopt make_prog gmake
     -> setopt make_symbol !

     Then the CL lines in your CL targets would need to look like
     the following:

     a_cl_target:
        !load $(CFLAGS) new1.o new2.o

Errors
     Error messages related to the ObjectCenter  make  command  are
     displayed either in the Workspace or the Error Browser.

RESTRICTIONS
     The makefile option called .SILENT does not work  well  with
     ObjectCenter's make.

     ObjectCenter does not support the use of CL  target  rules  in
     pdm.

     In  most  cases,  you  can  use  the  same   switches   with
     ObjectCenter's  make  command that you would use with the com-
     mand outside the environment, with the following exceptions:

     o    The -D switch is not  compatible  with  the  ObjectCenter
          make command. Do not use make -D in the Workspace.

     o    The -s switch is not  compatible  with  the  ObjectCenter
          make command. Do not use make -s in the Workspace.

SEE ALSO
     build, clezstart, contents, load, source

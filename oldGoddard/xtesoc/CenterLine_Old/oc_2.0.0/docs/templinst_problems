Template instantiation fails when either standalone CC or
ObjectCenter is unable to find template definition files at
link time.  You may get one of the following error messages:

        ptlink:"fatal error: template definition file cannot be found"
        ptlink:"fatal error: template symbols cannot be resolved"
        ptlink:"template definition file not found"
        CX[ptlink] warning: template definition file <name> not found
        E#156 Calling undefined function

If you get any of these messages, consider this information
and the cases that follow.

o       For a link to work, CC or ObjectCenter must be able to
        locate the following two types of files at link time.

        -  Template declaration files (usually .h or .H)
        -  Template definition files (usually .c or .C)

o       Template declaration files can contain only these
        types of things:

        - template class declarations
        - forward references to template functions
        - definitions of inline functions

o       Never include a template declaration file (foo.h) in the
        like-named definition file (foo.c).

o       Never compile (with CC) or load (with ObjectCenter)
        template declaration files directly.  The template
        instantiation system occurs at link-time.

        ObjectCenter expects the definition files to be in the
        same directory as the declaration files.  If the
        definition files are in a different directory, you must
        create an nmap file to let ObjectCenter know where the
        definition files are (see "templates").

o       Set the tmpl_instantiate_flg option to contain the
        -I flags for the link command:

            -> setopt tmpl_instantiate_flg <required -I flags>

Here are some cases to consider.

Case 1 (with ObjectCenter)
--------------------------
When you call CC to compile and link a program, it knows what -I
directories to use because you explicitly state the -I flags for the
compile.

In ObjectCenter, you load (the equivalent of compile) the files first.
ObjectCenter instantiates templates after you issue the link
command.  Since the link command has no -I switch, ObjectCenter is
unable to find #include files and displays one of the following
errors:

        ptlink:"fatal error: template definition file cannot be found"
        ptlink:"fatal error: template symbols cannot be resolved"
        ptlink:"template definition file not found"
        CX[ptlink] warning: template definition file <name> not found
        E#156 Calling undefined function

The tmpl_instantiate_flg option is designed to solve this problem.
Set the tmpl_instantiate_flg option to contain the -I flags:

        -> setopt tmpl_instantiate_flg <required -I flags>

Case 2 (with CC)
------------------
You may not have included the necessary compilation switches with
the link.

Before templates, you could pass compile related switches (-D, -I)
during CC -c and then pass link related switches (-L, -l) on the
link command line.  Templates, however, have changed this compile-link
distinction.

If main() uses a template that relies on the X11 library, you must
use the -I switch to link the X11 library on the link line.

Case 3 (with ObjectCenter and CC)
------------------------------------
You may be using your own file naming scheme.

ObjectCenter and CC use an automatic template instantiation system,
which means that ptlink attempts to locate the template files by
itself using a defined naming scheme.

If you do not follow the defined naming scheme, the link could
fail. You could rename the files to conform to the naming scheme
or create an nmap file to override the default system (see "Coding
Conventions" and "Map files" in templates).


Case 4: (with ObjectCenter and CC)
-------------------------------------
The template definition and declaration files may be in different
directories.

If they are, the -I switch must know about both directories to pass
the information to CC.

For example, suppose the definition files (.c files) are in dirA,
and the declaration files (.h files) are in dirB. Let's also suppose
the application files are in a third directory. The compilation takes
place in the directory that contains the application sources, so the
compile line should look like this:

     CC -IdirA -IdirB <application sources>

Case 5 (with ObjectCenter and CC)
------------------------------------
You may have changed the default setting of the cxx_suffixes option.

The default value of this option is set to "C c CC cc CXX cxx cpp
H h HH hh HXX hxx hpp".  If you have, reset cxx_suffixes to the
default value or add your template definition file extensions to
the default value.

See Also
---------
templates	for information about templates



If ObjectCenter crashes before reaching the first executable line of
your program, check if your program has nonlocal data requiring
static initialization, for example, an instance of a class that
requires calling the constructor.

Static initialization in C++ means initialization to possibly
nonconstant values, therefore requiring a function call rather than
the assignment of a value to a memory location.  (For information
about static initialization, see the Annotated C++ Reference Manual,
Ellis and Stroustrup.)

Compilers based on cfront, such as clc++, insert _main() as the
first executable statement in main() in order to perform static
initialization.  ObjectCenter may have crashed because it
encountered a bug while attempting a static initialization.

To set a breakpoint at the first executable line of code, set
the breakpoint at _main() instead of main().  For example,

        C++ ->  stop at _main()



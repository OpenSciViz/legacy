NAME
     unsetenv

     removes a variable from the program environment

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------



SYNOPSIS
     unsetenv variable

DESCRIPTION
     variable 
     Removes the definition of variable from the  system
     environment.

USAGE
     Use the unsetenv command  to  remove  a  variable  from  the
     program's system environment. The unsetenv command is analo-
     gous to the similarly named shell command.

     The unsetenv command affects only your program's environment
     variables. It does not affect the environment variables used
     by ObjectCenter to control its own operations.

     The environment is an array of strings that is  made  avail-
     able  to the program through the global environ variable and
     the envp parameter, which is passed as the third argument to
     the main() function. By convention, each string has the for-
     mat name=value, where the value part is optional.

WARNINGS
     If unsetenv is called from a break level, it will alter  the
     value  of  the  global  environ  variable,  but not the envp
     parameter passed to main(). This problem  also  occurs  with
     the putenv() function.

     Changing the EDITOR or DISPLAY shell variables with unsetenv
     does  not  affect  which editor or display screen ObjectCenter
     uses.

SEE ALSO
     printenv, setenv

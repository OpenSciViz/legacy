OBJECTCENTER DOCUMENTATION
     This section describes the documentation for ObjectCenter. 
     ObjectCenter Version 2.1 comes with hardcopy documentation and 
     online documentation. In addition, we provide USL  C++ Language 
     System documentation. 
     
   Online documentation
     This section describes the online documentation for ObjectCenter. 
     Online documentation is available both within the environment and 
     outside of it.
     
     Within the environment, context-sensitive help, the Help menu, 
     Workspace help, and the Manual Browser are available as follows.

     o	Access context-sensitive help in the GUI version of ObjectCenter 
	by moving the cursor over the item you want information about 
	and pressing F1 or the Help key if your keyboard has one.  A 
	Help window appears describing that item.

	If you have bound the F1 key to a window manager operation, 
	you are unable to access context-sensitive help with the F1 key.

     o	Access information on a variety of topics from the Help menu, 
	which appears on every primary window.

     o	Access information about a command by typing help in the 
	Workspace followed by the name of the command.

     o	Access any entry in the ObjectCenter Reference from any primary 
	window by selecting Manual Browser from the Windows menu.

	Where there are differences between the ObjectCenter Reference 
	and the Manual Browser, the information in the Manual Browser is 
	more up-to-date.

     The online documentation available outside the ObjectCenter 
     environment is in this directory:

	path/CenterLine/oc_2.0.0/<arch>/docs

     The word path represents the path to the CenterLine directory,
     and <arch> is a platform-specific directory, for example sparc-sunos4,
     sparc-solaris2, pa-hpux8, i486-svr4, or m88k-svr4.

     The online directory contains a file called README, which describes 
     the files in this directory. Among the files are 

     o	bugs.open, which describes the known bugs, limitations, and 
	workarounds for ObjectCenter. 

     o	bugs.fixed, which describes bugs fixed since the most recent
	version of ObjectCenter


   Hardcopy documentation
     This is the hardcopy documentation that comes with ObjectCenter:   

	ObjectCenter Read Me First Release Bulletin
	The latest hardcopy information, containing any updates necessary 
	to other hardcopy documentation.

	Installing and Managing CenterLine Products
	How to install ObjectCenter and administer it, including how to 
	troubleshoot licensing problems.

	ObjectCenter Tutorial 
	A step-by-step introduction to ObjectCenter features.

	ObjectCenter User's Guide
	A task-based description of ObjectCenter, explaining how to use the 
	graphical user interface to load, manage, run, and debug programs 
	within ObjectCenter.

	ObjectCenter Reference
	A complete reference for ObjectCenter, containing an alphabetical 
	listing and description of ObjectCenter commands, functions, and 
	informational topics.

	What's New in ObjectCenter and CodeCenter
	An overview of new features in this release.

	CenterLine-C Programmer's Guide 
	Information about the CenterLine-C compiler.

	Tools.h++ - Introduction and Reference Manual
	Information about the Tools.h++ foundation class library from
	Rogue Wave.

   USL documentation
     ObjectCenter also comes with USL  C++ Language System 
     documentation. We have incorporated the C++ information in our 
     ObjectCenter documentation set as follows: 

     AT&T  C++ Language System Product Reference Manual
	We ship this complete manual with ObjectCenter. This manual provides 
	a complete definition of the C++ language supported by Release 3.0 of 
	the C++ Language System.

     AT&T  C++ Language System Library Manual
	We ship this complete manual with ObjectCenter. This manual describes 
	class libraries shipped with Release 3.0: the iostream library, 
	complex	library, and task library. (We do not, however, support or 
	supply the task library libtask.a itself.)

     AT&T  C++ Language System Release Notes for Release 3.0
	The online directory contains files with relevant sections of this 
	document: known problems and implementation-specific behavior. 
	(The path to the online directory is specified in "Online 
	documentation" above.) The ObjectCenter Reference Appendix C
	is a reprint of sections of Chapter 4 of this document.


     AT&T Selected Readings
	The ObjectCenter Reference Appendix B is a full reprint of Chapter 7 
	of this document. The ObjectCenter Manual Browser entry called
	templates also provides other information on templates you may
	find useful.





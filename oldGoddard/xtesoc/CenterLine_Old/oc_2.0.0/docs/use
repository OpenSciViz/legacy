NAME
     use

     displays or sets the directory search path

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------



SYNOPSIS
     use
     use pathname

DESCRIPTION
     << none  >> 
     Displays the current directory search path.

     pathname ...  
     Sets the list of directories to be searched to the specified
     pathname. If more than one pathname is listed, they must be
     separated by spaces or colons.  The directories can be
     specified as absolute or relative pathnames.

OPTIONS
     The following ObjectCenter options affect the use command:

     cxx_suffixes     Specifies file extensions to search for when
                      ObjectCenter needs to find a C++ source file
                      that corresponds to a given object file.

     path             Specifies the search path for loading
                      source and object files (not for #include
                      files). You must also set the
                      swap_uses_path option for the path
                      option to affect the swap command.

     swap_uses_path   Determines whether the swap command
                      uses the path, which can be set by the use
                      command or by the path option.

     See the options entry for more details about each option.
     ObjectCenter does not support these options in process debugging
     mode (pdm).

USAGE
     Use the use command to set the list  of  directories  to  be
     searched  when  a filename is given to the edit, list, load,
     or swap commands.  The swap command uses the path set by use
     only if the swap_uses_path option is set.

     The use command sets and displays the current value  of  the
     path  option,  which  can also be set and displayed with the
     setopt and printopt commands, respectively.

RESTRICTIONS
     The use command does not provide a search path  for  loading
     #include files, only for loading source and object files. To
     give the search path for #include directories,  use  the  -I
     switch with the load command according to the following for-
     mat:

     load -I include_dir1 [-I include_dir2 ] file.c

SEE ALSO
     cd, edit, list, load, printopt, setopt, swap

This entry has solutions for the following problems loading
source code:

1.  I can't find some function declarations

2.  I'm getting message E#1079: "inconsistent linkage specifications"

3.  I'm getting message E#656 about qsort() with CC but not clcc


1.  I can't find some function declarations
---------------------------------------------
To find declarations for functions such as open(), close(), fork(),
pipe(), and dup(), look in the following places:

For CC: clc++/<arch-os>/incl/sysent.h

        The cfront based CenterLine CC defines these and other
        UNIX I/O related functions in sysent.h rather than 
        fcntl.h.  If you need to use these functions you must
        include sysent.h in addition to fcntl.h.

For clcc: /usr/include/unistd.h

        For clcc, these functions are declared in the location
        POSIX specifies, unistd.h.

These files can be included in your source code with
#include <sysent.h> or	#include <unistd.h>. ObjectCenter will then be
able to locate the files when loading your code.


2.  I'm getting message E#1079: "inconsistent linkage specifications"
----------------------------------------------------------------------
This error occurs in a module with two function declarations that
are inconsistent.  For example:

        extern void foo();
        extern "C" { void foo (...); }
        // Note: this error won't occur if you switch the two lines

Inconsistent definitions are harder to detect if one or both of the
declarations are from included header files.

For example, this problem can occur using the XVT library for UI
programming.  The file xvt_cc.h includes files in an extern "C"
scope.

To eliminate this message, do one of the following:

o       Add the header files that are causing a problem before
        extern "C" so that they will not be included again.

               #include <stdio.h>
               #include <unistd.h>
               #include <stdlib.h>

                extern "C" {
               #include <X11/Xos.h>
                }
                main()
                    {
                    }

o       Edit the appropriate C++ header file and add an extern
        "C++" linkage specifier to the WHOLE header file.
        That is:

        -  Add the following to the beginning of the header file
           after the first #ifndef, #define pair:

                extern "C++" {

        -  Add a closing brace to the end of the header file before
           the last #endif.

        For example (unistd.h):

               #ifndef __UNISTD_H
               #define __UNISTD_H

                extern "C++" {

                ...the_rest_of_the_header_file...

                }

               #endif

        To find the appropriate header files, grep through the
        header files for the symbol causing the error.  For
        example, if ______ctermid is the symbol causing the
        problem,

                % grep ______ctermid *.h
                stdio.h: #define ctermid ______ctermid
                unistd.h: #define ctermid ______ctermid

        Since stdio.h and unistd.h define ______ctermid, these
        would be the header files to change.

        Another way to find the appropriate header files is to use
        the following:

                % CC -E program.C | uniq | grep function_name

        The -E flag tells CC to generate only C preprocessor output,
        uniq eliminates duplicate lines, and grep searches for the
        function name you specify.

        To get a listing of the full pathnames of included files
        on stderr, you could use the following if cpp is in your
        path:

               % cpp -H program.C > /dev/null

3.  I'm getting message E#656 about qsort() with CC but not clcc
-------------------------------------------------------------------
C++ is stricter about type matching than C, and attempts to maintain
type safety in pointers to functions.  If qsort() gives you an error
message with CC and not with clcc, consider that the implicit
conversion of a pointer to something to a void* does not take place
for argument types of a pointer to function used as an argument.
This means that the following is not a suitable argument for qsort():

        int bad_compare_function(const item *item1, const item *item2)

Accepting 'bad_compare_function' as an argument to qsort() would
violate the guarantee that 'bad_compare_function' will be called with
arguments of type 'item*'.  If you want to violate that guarantee
you'll have to use explicit type conversion, as illustrated by the
following example, qsort.c.

/* ----- qsort.c ----- */

#include <stdlib.h>
#include <string.h>

/*
 * This example is taken from
 *      "CenterLine-C Programmer's Guide and Reference"
 *
 * See "The C++ Programming Language", 2nd Edition,  page 138
 * for reasons why it needed to be modified for C++
 *
 */

/* from stdlib.h:
 *       void qsort(void*, size_t, size_t, int(*)(const void*, const void*));
 */

struct info
    {
    char name[8];
    int  age;
    char phone[9];
    };

     /* Unsorted info array: */
struct info data[] =
    {
    { "Anne",  33, "555-5552" },
    { "Fred",  27, "555-1221" },
    { "Sonya", 36, "555-1976" },
    { "Frank", 30, "555-1965" },
    { "Carol", 32, "555-4299" },
    { "Alice", 19, "555-7979" },
    { "Larry", 33, "555-8235" },
    };

int compare_function(const void *item1, const void *item2)
    {
    if (((info*)item1)->age < ((info*)item2)->age)  /* Compare by age.*/
        return (-1);
    else if (((info*)item1)->age > ((info*)item2)->age)
        return (1);
    else
        /* then compare by name */
        return (strcmp(((info*)item1)->name, ((info*)item1)->name));
    }m

/* NOTE: to avoid the explicit cast of item1 and item2 to info*
*  inside the compare_function, you could
*  use the following typedef and make a cast in the argument list for qsort()
*
*  typedef int (*fp) (const void*, const void*); // typedef for explicit cast */

main()
    {
    int i;

    qsort(data, sizeof(data)/sizeof(*data), sizeof(*data), compare_function);
/*  Make a cast to the typedef above to avoid casting inside the comare_function
 */
/*  qsort(data, sizeof(data)/sizeof(*data), sizeof(*data), (fp) compare_function
); *
/


    for (i = 0; i < sizeof(data)/sizeof(*data); i++)
        printf("%-8s %d, %s\n", data[i].name, data[i].age, data[i].phone);
    }



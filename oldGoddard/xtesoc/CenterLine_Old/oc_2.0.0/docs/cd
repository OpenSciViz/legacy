NAME
     cd

     changes the current working directory

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------


SYNOPSIS
     cd
     cd pathname

DESCRIPTION
     << none >>
     Changes the working directory for  ObjectCenter  to
     your home directory.

     pathname
     Changes the working directory for ObjectCenter to the
     designated pathname. UNIX wildcards are allowed.

OPTIONS
     The following ObjectCenter option affects the cd command:

     path   Specifies the search  path  for  loading
            source   and   object   files  (not  for
            #include files) and for a matching path-
            name with the cd command.

     See the options reference page for more details  about  each
     option.  ObjectCenter  does not support these options in process
     debugging mode (pdm).

USAGE
     To facilitate loading and saving files, use the  cd  command
     to change the current working directory for ObjectCenter.

     ObjectCenter searches the directories specified  by  the  path
     option  for subdirectories that match the pathname specified
     with the cd command.

     Here is an example of the use of cd in connection  with  the
     path option:

     -> pwd
     /my_home_directory
     -> printopt path
     path (unset)
     string - list of directories to search for  source,  object,
     and library files
     -> cd temp3
     cd: cannot change to directory 'temp3'.
     -> setopt path ~/temp1/temp2
     -> cd temp3
     wd now: '/my_home_directory/temp1/temp2/temp3'

SEE ALSO
     use


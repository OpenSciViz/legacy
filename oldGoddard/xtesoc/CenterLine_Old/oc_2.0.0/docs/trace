NAME
     trace

     traces program execution

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     no     |
     -------------------------

SYNOPSIS
     trace
     trace function

DESCRIPTION
     << none  >>     Displays each line of source code as it is 
		     being executed.

     function	     Displays each line of source code in the  
		     specified function as it is being executed.

USAGE
     Use the trace command to display each line of source code as
     it is being executed. If a function is specified, tracing is
     limited to that function.

     Statements executed within an action are not traced.

     To turn off tracing, use the delete command.

RESTRICTIONS
     You cannot use trace within object code in component debugging
     mode.

SEE ALSO
     delete, next, step, stop, status

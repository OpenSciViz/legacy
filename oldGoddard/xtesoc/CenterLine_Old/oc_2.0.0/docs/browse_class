
NAME	
     browse_class

     displays base and derived classes and members in the 
     Workspace

    ----------------    
    |  cdm  | pdm  |
    +-------+------+
    |  yes  | no   |
    ----------------  

SYNOPSIS
     browse_class class_name

DESCRIPTION
    class_name		Ascii ObjectCenter: Lists the base
			classes and derived classes for 
    			class_name in the Workspace.    
    			                                
    			Displays data members and member
    			functions that are accessible to
    			class_name.                     
    			                                
    			Lists the members' access level 
    			(public, protected, or private).
    
SWITCHES
     -l 		Ascii ObjectCenter: Limits the  
     			display of members to those defined
     			in the specified class, not listing
     			inherited members.                 

OPTIONS
     The following ObjectCenter option affects the browse_class
     command:
     
     show_inheritance	Displays class members with the full 
     			inheritance path showing how members
     			are inherited. If unset, shows a    
     			truncated inheritance path giving   
     			only the defining class.            
          
     See the options entry for more details about each option.
     ObjectCenter does not support this option in process
     debugging mode (pdm).

USAGE
     Use the browse_class command to list in the Workspace the
     full range of inheritance information for a particular
     class. The information is as follows: base classes, derived
     classes, data members, and member functions.
     
     For example, the following output from browse_class shows
     that the class Land_vehicle publicly derives from a base
     class Vehicle and in turn serves as a base class for the
     publicly derived classes Car and Truck.
     
     The data members doors, mileage, and model and the functions
     calcMileage(), setModel(), show(), and setMileage(),
     together with two constructors and one destructor, are all
     local to the class Car.  The data member tires and two
     constructor functions are local to Land_vehicle, while the
     other data members and member functions derive from the
     class Vehicle.

     -> browse_class Car
     Base Classes:
     Land_vehicle <Public>
     
     Derived Classes:
     (nothing)
     Data Member Interface:
     <Protected> int doors
     <Protected> float mileage
     <Protected> char *model
     <Protected> int Land_vehicle::tires
     <Protected> char *Land_vehicle:Vehicle::serial_num
     <Protected> char *Land_vehicle:Vehicle::owner
     <Protected> char *Land_vehicle:Vehicle::city
     <Protected> int Land_vehicle:Vehicle::passengers
     Member Function Interface:
     <Public> Car::Car(char *sn = "XXXXX", char *own = 
     "Unknown", int dr = 4)
     <Public> inline Car::Car(const class Car &)
     <Public> Car::~Car()
     <Public> float Car::calcMileage()
     <Public> void Car::setModel(char *mdl)
     <Public> virtual void Car::show(ostream &strm = 
     <const value>)
     <Protected> inline void Car::setMileage(float mpg)
     <Public> Land_vehicle::Land_vehicle(char *sn = 
     "XXXXX", char *own = "Unknown", int tr = 4)
     <Public> inline Land_vehicle::Land_vehicle(const 
     class Land_vehicle &)
     <Public> Land_vehicle:Vehicle::Vehicle(char *sn = 
     "XXXXX", char *own = "Unknown")
     <Public> inline Land_vehicle:Vehicle::Vehicle(const 
     class Vehicle &)
     <Public> Land_vehicle:Vehicle::~Vehicle()
     <Public> void Land_vehicle:Vehicle::setSerial(char *sn)
     <Public> void Land_vehicle:Vehicle::setOwner(char *own)
     <Public> void 
     Land_vehicle:Vehicle::setPassengers(int p)
     <Public> virtual void 
     Land_vehicle:Vehicle::move(char *cty)
     <Public> virtual void 
     Land_vehicle:Vehicle::show(ostream &strm = <const value>)

  Indicating inheritance
     When ObjectCenter shows the inheritance path, a single colon
     ( : ) indicates inheritance and the scoping operator ( :: )
     indicates the class that actually defines a member. The way
     that ObjectCenter shows inheritance is affected by the
     current value of the show_inheritance option.
     
     In the previous example, to indicate that setOwner() is a
     member function that the class Land_vehicle inherits
     publicly from its base class Vehicle, the browse_class
     command displays the following line:

     <Public> void Land_vehicle:Vehicle::setOwner(char *own)

SEE ALSO
     browse_base, browse_data_members, browse_derived, 
     browse_friends, browse_member_functions, list, list_classes



SUN SHARED LIBRARIES

     This section describes the support for shared libraries within the 
     ObjectCenter environment. For general information about Sun shared 
     libraries, see the Sun manual Programming Utilities & Libraries  
     on Solaris 1 and Linker and Libraries Manual SunOS 5.0 on Solaris 2.
     
     Reading debugging information on shared libraries is not supported
     in component debugging mode. Without debugging information on a file, 
     you are unable to perform certain debugging activities, such as 
     stepping through functions. For information about what debugging 
     techniques are possible on code without debugging information, see 
     the debugging entry in the Man Browser or ObjectCenter Reference 
     Manual.

     ObjectCenter supports full source-level debugging of shared libraries
     in process debugging mode. For example, you can step into functions
     in shared libraries that were compiled with -g.  

     Once you load a shared library into ObjectCenter, its functions and 
     any of its data that you have exported are available to your program.
     ObjectCenter mimics the behavior of the system's link editors, ld
     and ld.so, with regard to loading shared libraries and with regard
     to binding functions and data.

   Search rules for loading libraries
     When you load a library using the load command's -l switch, 
     ObjectCenter searches for libraries in its search path in the same 
     way that the Sun ld command does. ObjectCenter stops searching as 
     soon as it finds either the shared or static version of the library. 
     If it finds both versions in the same directory, ObjectCenter uses the 
     shared version (.so file) by default. You can, however, override this 
     default behavior by specifying the binding mode:

     On Solaris 1:

	-> load -lX11 
	Attaching: /s/apps/openwin3.0/lib/libX11.sa.4.3 
	Attaching: /s/apps/openwin3.0/lib/libX11.so.4.3 
	-> unload -lX11 
	Detaching: /s/apps/openwin3.0/lib/libX11.sa.4.3 
	Detaching: /s/apps/openwin3.0/lib/libX11.so.4.3 
	-> load -Bstatic -lX11 
	Attaching: /s/apps/openwin3.0/lib/libX11.a 
	-> 

     On Solaris 2:

       -> load -lX11
       Attaching: /usr/openwin/lib/libX11.so.4
       Attaching: /usr/lib/libsocket.so.1
       Attaching: /usr/lib/libnsl.so.1
       Attaching: /usr/lib/libintl.so.1
       Attaching: /usr/lib/libw.so.1
       -> unload -X11
       Detaching: /usr/openwin/lib/libX11.so.4
       -> load -Bstatic -lX11
       Attaching: /usr/openwin/lib/libX11.a
       ->

     Note that ObjectCenter may load more libraries than just the X11
     library. When you load a library into ObjectCenter, ObjectCenter also
     loads any other library referenced by the library you loaded
     explicitly.  

     ObjectCenter unloads only the X11 library as a result of unload -X11. 
     The unload command unloads only those libraries that you explicitly 
     unload, regardless of whether ObjectCenter loaded them as a result 
     of a reference or an explicit load command.  

     ObjectCenter allows you to load both the static and shared versions 
     of a library, but we do not recommend that you do so.

     On Solaris 1, when ObjectCenter loads a shared object (an .so
     file), it looks in the same directory for a data interface
     description file (an .sa file) with the same root name and version
     number. If it finds such an .sa file, it loads it so it can
     generate the necessary data references.  ObjectCenter does not
     report an error if it fails to find such an .sa file.

     NOTE	ObjectCenter does not load an .sa file with the 
		same root name but different version number 
		as an .so file. This situation is treated the same 
		as a missing .sa file.


   Using environment variables to modify loading libraries
     One way to affect how ObjectCenter loads shared libraries is by 
     setting environment variables before starting ObjectCenter. Like ld, 
     ObjectCenter takes into account the following environment 
     variables:

     LD_LIBRARY_PATH	A colon-separated list of directories to 
			search for libraries specified with the -l 
			switch. Like ld, ObjectCenter looks in 
			directories specified in this environment 
			variable after looking in libraries specified 
			with the -L switch on the command line.

     LD_OPTIONS		A default set of options to pass to ld. The 
			options specified in LD_OPTIONS are 
			passed to load just as if they were entered 
			first on the command line.

			ObjectCenter ignores other environment variables 
			used by ld.

    NOTE	You must set environment variables before you 
		start ObjectCenter.


   Using switches to modify the loading of libraries
     Another way to affect how ObjectCenter loads shared libraries is to 
     use ld's command-line switches with ObjectCenter's load command. 
     The following command-line switches affect how ObjectCenter 
     loads libraries:

     -Bdynamic,	Specifies binding mode. -Bdynamic is the 
     -Bstatic	default.


 		-Bdynamic enables dynamic binding; that 
		is, it uses the shared version of a library if 
		one exists.

 		-Bstatic forces static binding; that is, it loads 
		the static version of the library.

     -lx[.v]	Loads a library with the name libx.so or 
		libx.a. If -Bdynamic is in effect at that point 
		on the command line, loads the latest 
		version of the shared library in the first 
		directory found that contains the library. If 
		no shared version is found, loads the static 
		version.

		If you supply a .v suffix, only the version 
		specified will be loaded. If that version is not 
		found, ObjectCenter reports an error. If you 
		specify a .v suffix to -l when -Bstatic is in 
		effect, ObjectCenter reports a load-time 
		error.

     -Ldir	Adds dir to the directories searched for 
		libraries. 

     ObjectCenter ignores other ld command-line switches.

   Specifying the binding mode
     If you want to load the static version of a library instead of the 
     shared version, you can specify the binding mode with the load 
     command.
     
     Just as with ld, you specify the binding mode using the -B switch. The 
     binding mode you specify is in effect until the end of the command 
     line or until you specify another binding mode. Because you can 
     specify binding mode more than once with the load command, you 
     can use the shared version of some libraries and the static version of 
     others.
     
     In the following example, the static library One (libOne.a) is loaded, 
     and the shared library Two (libTwo.so) is loaded:

	-> load -Bstatic -lOne -Bdynamic -lTwo

   Loading the static version of the C and C++ libraries
     ObjectCenter automatically loads the C and C++ libraries when 
     starting. By default, it loads the shared versions. You can force 
     ObjectCenter to load the static version (libc.a and libC.a) by setting 
     the environment variable LD_OPTIONS to the value -Bstatic before 
     starting ObjectCenter. That causes ObjectCenter to load the static 
     versions of all libraries by default. Alternatively, you can unload the 
     shared libraries (using unload), then explicitly load the static 
     versions by specifying their pathnames with the load command.

     When you load a library, ObjectCenter automatically loads any
     other libraries that the library references. Unloading the first
     library unloads only that first library, not any of the libraries
     that were referenced by it. To unload the other libraries, you
     must unload them explicitly.
  
   Setting breakpoints in shared libraries while in pdm
     While you are in pdm, you can set a breakpoint in a C library 
     function, such as printf(). To do so, you first set a breakpoint in 
     main(), issue the run command, set the breakpoint in printf(), and 
     issue the cont command: 

     pdm -> stop in printf
     Function "printf" not defined.
     pdm -> stop in main
     stop (1) set at "main.c":8, main().
     pdm -> run
     Resetting to top level.
     Executing: /test_dir/a.out

     Breakpoint 1, main (argc=1, argv=0xf7fff824) at main.c:8
     pdm (break 1) -> stop in printf
     stop (2) set at 0xf76ed904, printf().
     pdm (break 1) -> cont

     Breakpoint 2, 0xf76ed904 in printf ()
     pdm (break 1) ->

     You have to set the breakpoint in this fashion only once; 
     subsequent runs of the program retain the breakpoint in printf():

     pdm 14 -> run
     Resetting to top level.
     Executing: /test_dir/a.out

     Breakpoint 1, main (argc=1, argv=0xf7fff824) at main.c:8
     pdm (break 1) 15 -> cont
     Breakpoint 2, 0xf76ed904 in printf ()

  Binding of functions and data
     Like the Sun dynamic linker/loader, ld.so, ObjectCenter binds 
     functions from shared libraries at run time when the functions are 
     called.
     
     Note that, if you have loaded the definition of a function in your own 
     source or object file, it takes precedence over a definition of that 
     function in a shared library. 

     On Solaris 1, ObjectCenter binds a library's exported initialized data 
     (usually found in the .sa file) at link time, not at run time (similar 
     to ld's statically linking an .sa file at link time).

   Unloading a specific function
     If you specify a function in a shared library with unload, 
     ObjectCenter unloads the entire .so file.

	-> unload printf 
	Detaching: /usr/lib/libc.so.1

    On Solaris 1, ObjectCenter does not unload the .sa file.

   Other Solaris 1 differences

     NOTE: The rest of the section on shared libraries applies only
	   to Solaris 1.
    
   Unloading shared objects
     You can unload and unlink an entire shared library at once. 

	-> unload -lX11	 
	Detaching: /usr/openwin/lib/libX11.sa.4.3 
	Detaching: /usr/openwin/lib/libX11.so.4.3 

   Unloading a specific module
     You can also unload specific modules in a data interface description 
     file (.sa file) by specifying the module in parentheses following the 
     library name:

	-> unload /lib/libc.sa.1.6(errlst.o)
	Unloading: /lib/libc.sa.1.6(errlst.o)


   Using initialized global data
     If you declare initialized global data in a program using Solaris 1
     shared libraries, you should include its initialization in your .sa 
     archive, as well as in your .so file. If you don't, your program might 
     not use the correct initialization values.
     
     The problem results from the behavior of Sun's linker, ld, which can 
     only find initializations of data in the user's program or in an archive 
     (.a or .sa file)-not in .so files. When ld can't find the initialization 
     for a variable, the system initializes the variable to 0 (zero).
     
     This issue can come up if you use common variables in C programs. 
     Common variables are global variables that are defined in more than 
     one module without using the extern qualifier. Consider this simple C 
     example with two files, a.c and b.c:

     a.c					 b.c
     int Dogs;					 int Dogs = 12;
     main() 
     {printf("Dogs is %d\n", Dogs);}

     In this example, Dogs is a common variable. It is declared in two 
     modules, namely a.c and b.c, without using the extern qualifier.

     Here's what happens when we compile the example statically:

	% cc -c a.c 
	% cc -c -pic b.c 
	% cc -o static a.o b.o 
	% static 
	Dogs is 12

     The program runs fine. But suppose the code from b.c is in a shared 
     library without an .sa archive. When we link the code, it runs 
     incorrectly:

	% ld -o libb.so.1.0 b.o -assert pure-text 
	% cc -o shared1 a.o -L. -lb 
	% shared1 
	Dogs is 0

     The linker erroneously initialized the variable Dogs to 0 (zero).

     The solution is to create an .sa file containing the static data in b.c:

	% ar r libb.sa.1.0 b.o 
	ar: creating libb.sa.1.0 
	% ranlib libb.sa.1.0 
	% cc -o shared2 a.o -L. -lb 
	% shared2 
	Dogs is 12

     Now the program runs correctly. Notice that in this situation you must 
     initialize data in the .sa file; otherwise the statically linked program 
     runs differently from the dynamically linked one.
     
     As good practice, you should probably always include initializations 
     in your .sa files, even if the static variable is initialized to zero. 
     This results in better shared-library performance. 
     

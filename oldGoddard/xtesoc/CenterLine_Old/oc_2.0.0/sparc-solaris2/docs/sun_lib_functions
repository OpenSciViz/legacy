SUN C LIBRARY FUNCTIONS REPLACED

     To do its run-time error checking and to make its environment behave 
     like a standard UNIX process, ObjectCenter replaces many C library 
     functions and system calls with its own version of them. For some of 
     these functions, you can substitute your own version. However, 
     ObjectCenter cannot provide run-time error checking on your 
     substituted function.
     
     To use your own version of a function, load the function in a source or 
     object file before linking your program. If your program has already 
     been linked, you must quit, then start a new ObjectCenter session to 
     substitute your function for one of the ObjectCenter replacements.
     
     The following three tables list functions that ObjectCenter replaces. 
     The three tables apply to

     o	Solaris 1 

     o	Solaris 2 in libc 

     o	Solaris 2 in libucb 


     Table 1 ObjectCenter replaces these C Library functions on Solaris 1    


     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     __builtin_alloc()       no              shmdt()         no           
                                                                          
     _exit()                 no              shmget()        no           
                                                                          
     _longjmp()              no              shmctl()        no           
                                                                          
     _setjmp()               no              sigaction()     no           
                                                                          
     alloca()                no              sigaddset()     yes          
                                                                          
     bcopy()                 yes             sigblock()      no           
                                                                          
     brk()                   no              sigdelset()     yes          
                                                                          
     bzero()                 yes             sigemptyset()   yes          
                                                                          
     close()                 no              sigfillset()    no           
                                                                          
     dup2()                  no              sigismember()   yes          
                                                                          
     fork()                  no              siglongjmp()    no           
                                                                          
     free()                  yes             signal()        no           
                                                                          
     getdtablesize()         no              sigpause()      no           
                                                                          
     getrlimit()             no              sigpending()    no           
                                                                          
     longjmp()               no              sigprocmask()   no           
                                                                          
     malloc()                yes             sigsetjmp()     no           
                                                                          
     mallopt()               yes             sigsetmask()    no           
                                                                          
     mallinfo()              yes             sigstack()      no           
                                                                          
     memalign()              yes             sigsuspend()    no           
                                                                          
     memcpy()                yes             sigvec()        no           
                                                                          
     memccpy()               yes             strcat()        yes          
                                                                          
     memset()                yes             strcmp()        yes          
                                                                          
     mmap()                  no              strcpy()        yes          
                                                                          
     munmap()                no              strncat()       yes          
                                                                          
     pause()                 no              strncmp()       yes          
                                                                          
     realloc()               yes             strncpy         yes          
                                                                          
     sbrk()                  no              syscall()       no           
                                                                          
     setjmp()                no              system()        yes          
                                                                          
     setrlimit()             no              valloc()        yes          
                                                                          
     shmat()                 no              vfork()         no           
                                                                          
                                                                          
                                                                          
     Table 2 ObjectCenter replaces these C Library functions on 
     Solaris 2 in libc
                                                                          
                                                                          
     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     __builtin_alloc()       no              shmget()        no           
                                                                          
     _exit()                 no              shmctl()        no           
                                                                          
     alloca()                no              sigaction()     no           
                                                                          
     atexit()                no              sigaddset()     yes          
                                                                          
     brk()                   no              sigdelset()     yes          
                                                                          
     close()                 no              sigemptyset()   yes          
                                                                          
     dup2()                  no              sigfillset()    no           
                                                                          
     exit()                  no              sigismember()   yes          
                                                                          
     fork()                  no              siglongjmp()    no           
                                                                          
     free()                  yes             signal()        no           
                                                                          
     getrlimit()             no              sigpause()      no           
                                                                          
     longjmp()               no              sigpending()    no           
                                                                          
     malloc()                yes             sigprocmask()   no           
                                                                          
     memalign()              yes             sigsetjmp()     no           
                                                                          
     memcpy()                yes             sigsuspend()    no           
                                                                          
     memccpy()               yes             strcat()        yes          
                                                                          
     memove()                no              strcmp()        yes          
                                                                          
     memset()                yes             strcpy()        yes          
                                                                          
     mmap()                  no              strncat()       yes          
                                                                          
     munmap()                no              strncmp()       yes          
                                                                          
     pause()                 no              strncpy()       yes          
                                                                          
     realloc()               yes             syscall()       no           
                                                                          
     sbrk()                  no              sysconf()       no           
                                                                          
     setjmp()                no              system()        yes          
                                                                          
     setrlimit()             no              valloc()        yes          
                                                                          
     shmat()                 no              vfork()         no           
                                                                          
     shmdt()                 no                                           
                                                                          
                                                                          
                                                                          
     Table 3 ObjectCenter replaces these C Library functions on 
     Solaris 2 in libucb
                                                                          
                                                                          
     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     _longjmp()              no              signal()        no           
                                                                          
     _setjmp()               no              sigpause()      no           
                                                                          
     bcopy()                 yes             sigsetmask()    no           
                                                                          
     bzero()                 yes             sigstack()      no           
                                                                          
     getdtablesize()         no              sigvec()        no           
                                                                          
     getrlimit()             no              syscall()       no           
                                                                          
     longjmp()               no              ucbsignal()     no           
                                                                          
     setjmp()                no              ucbsigpause()   no           
                                                                          
     sigblock()              no              ucbsigblock()   no           
                                                                          
                                                                          
                                                                          

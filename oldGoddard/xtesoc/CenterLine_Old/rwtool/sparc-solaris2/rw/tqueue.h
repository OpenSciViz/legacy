#ifndef __RWTQUEUE_H__
#define __RWTQUEUE_H__

/*
 * Parameterized queue of T's, implemented using class C.
 *
 * $Id: tqueue.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tqueue.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.5  1993/12/31  00:56:30  jims
 * ObjectStore version: add get_os_typespec() static member function
 *
 * Revision 2.4  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.2  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 *    Rev 1.3   25 May 1992 15:55:56   KEFFER
 * No longer uses access adjustment
 * 
 *    Rev 1.2   15 Mar 1992 12:47:32   KEFFER
 * 
 *    Rev 1.0   02 Mar 1992 16:10:52   KEFFER
 * Initial revision.
 */

#ifndef __RWDEFS_H__
#  include "rw/defs.h"
#endif

#ifdef RW_BROKEN_TEMPLATES

template <class T, class C> class RWExport RWTQueue
{

public:

  void		clear()		{container_.clear();}
  size_t	entries() const	{return container_.entries();}
  T		first() const	{return container_.first();}
  T		get()		{return container_.removeFirst();}
  RWBoolean	isEmpty() const	{return container_.isEmpty();}
  void		insert(T a)	{container_.append(a);}
  T		last() const	{return container_.last();}

protected:

  C		container_;

};

#else	/* !RW_BROKEN_TEMPLATES */

template <class T, class C> class RWExport RWTQueue : private C
{

public:

  C::clear;
  C::entries;
  C::first;
  T		get()		{return C::removeFirst();}
  void		insert(T a)	{C::append(a);}
  C::isEmpty;
  C::last;

};

#endif	/* RW_BROKEN_TEMPLATES */

#endif	/* __RWTQUEUE_H__ */

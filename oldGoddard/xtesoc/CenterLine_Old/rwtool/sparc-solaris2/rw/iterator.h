#ifndef __RWITERATOR_H__
#define __RWITERATOR_H__

/*
 * Abstract base class for Iterator classes.
 *
 * $Id: iterator.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: iterator.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   18 Feb 1992 09:54:24   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:18   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:15:34   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/defs.h"

class RWExport RWCollectable;

class RWExport RWIterator {
public:
  virtual RWCollectable*	findNext(const RWCollectable*) = 0; // Find next matching item
  virtual RWCollectable*	key() const = 0;		  // Return current key
  virtual RWCollectable*	operator()() = 0;		  // Advance iterator
  virtual void		reset() = 0;
};	  

#endif /* __RWITERATOR_H__ */

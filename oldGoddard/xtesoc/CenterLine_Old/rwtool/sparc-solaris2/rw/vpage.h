#ifndef __RWVPAGE_H__
#define __RWVPAGE_H__

/*
 * RWVirtualPageHeap: Abstraction of a page heap, swapped to unknown parts
 *
 * $Id: vpage.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: vpage.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.0   11 Mar 1992 14:10:00   KEFFER
 * Initial revision.
 */

#include "rw/tooldefs.h"

typedef unsigned RWHandle;

class RWExport RWVirtualPageHeap {
protected:
  unsigned		pageSize_;
public:
  RWVirtualPageHeap(unsigned pgsize) : pageSize_(pgsize) { }
  virtual		~RWVirtualPageHeap() { }

  unsigned		pageSize() const {return pageSize_;}

  virtual RWHandle	allocate()           = 0;	// Allocate a page
  virtual void		deallocate(RWHandle) = 0;	// Deallocate it
  virtual void		dirty(RWHandle)      = 0;	// Declare page as dirty
  virtual void*		lock(RWHandle)       = 0;	// Lock a page
  virtual void		unlock(RWHandle)     = 0;	// Unlock a page
};

#endif	/* __RWVPAGE_H__ */

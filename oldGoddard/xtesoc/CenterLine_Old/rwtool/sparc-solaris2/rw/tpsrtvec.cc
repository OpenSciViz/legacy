/*
 * Template definitions for RWTPtrSortedVector<TP>
 *
 * $Id: tpsrtvec.cc,v 1.1 1994/05/26 17:46:33 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tpsrtvec.cc,v $
// Revision 1.1  1994/05/26  17:46:33  sridhar
// Initial revision
//
 * Revision 1.6  1994/01/12  03:09:17  jims
 * Add constness to T* parameters where appropriate
 *
 * Revision 1.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.4  1993/07/08  03:42:18  keffer
 * Corrected logic error in RWDEBUG version of bsearch().
 *
 * Revision 1.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.1  1993/01/29  03:06:18  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */


template <class TP> size_t
RWTPtrSortedVector<TP>::index(const TP* item) const
{
  size_t idx;
  if(! bsearch(item,idx))  // Return RW_NPOS if item is not in collection:
    return RW_NPOS;

  // The item is in the collection.
  // Search downwards looking for the first instance:

  while (idx && *(*this)(idx-1) == *item)
    --idx;

  RWPOSTCONDITION(*(*this)(idx) == *item);
  RWPOSTCONDITION(idx>=0 && idx<nitems_);

  return idx;
}

template <class TP> void
RWTPtrSortedVector<TP>::insert(TP* p)
{
  // This algorithm was written such that only
  // the equality and less-than operator are used.

#ifdef RWDEBUG
  size_t count = occurrencesOf(p);
#endif

  size_t idx;
  if (bsearch(p,idx))
  {

    // A matching item was found.  Insert after the
    // last equal item.
    while (idx<nitems_ && *(*this)(idx) == *p)
      ++idx;
  }
  else
  {
    // No matching item found.  Search upward
    // for the first item greater than the value
    // and insert before it.
    while (idx<nitems_ && *(*this)(idx) < *p)
      ++idx;
  }

  insertAt(idx, p);

  RWPOSTCONDITION(isSorted() && occurrencesOf(p) == count+1);
}

template <class TP> size_t
RWTPtrSortedVector<TP>::occurrencesOf(const TP* p) const
{
  size_t iend;
  size_t istart = indexSpan(p, iend);
  return istart == RW_NPOS ? 0 : iend-istart+1;
}

/*
 * Remove and return the first occurrence of an object with the
 * same value as the object pointed to by "p".
 */
template <class TP> TP*
RWTPtrSortedVector<TP>::remove(const TP* p)
{
  size_t idx = index(p);
  return idx == RW_NPOS ? rwnil : removeAt(idx);
}

template <class TP> size_t
RWTPtrSortedVector<TP>::removeAll(const TP* p)
{
  size_t iend;
  size_t istart = indexSpan(p, iend);

  if (istart == RW_NPOS) return 0;
  iend++;
  size_t nremoved = iend-istart;

  // Do a "solid body" slide left of the remaining items in the collection:
  while (iend<nitems_)
    (*this)(istart++) = (*this)(iend++);

  nitems_ -= nremoved;
  RWPOSTCONDITION(!contains(p));
  return nremoved;
}

/****************************************************************
 *								*
 *	protected members of RWTPtrSortedVector<TP>		*
 *								*
 ****************************************************************/

template <class TP> RWBoolean
RWTPtrSortedVector<TP>::bsearch(const TP* key, size_t& idx) const
{
  idx = 0;
  if (entries())
  {
    size_t top = entries() - 1;
    size_t bottom = 0;

    while (top>bottom)
    {
      idx = (top+bottom) >> 1;
      // It is important that we use only the equality and less-than
      // operators:
      if (*key == *(*this)(idx))
        return TRUE;
      else if(*key < *(*this)(idx))
        top    = idx ? idx - 1 : 0u;
      else
        bottom = idx + 1;
    }
    return *key == *(*this)(idx=bottom);
  }
  // Not found:
  return FALSE;
}

template <class TP> size_t
RWTPtrSortedVector<TP>::indexSpan(const TP* key, size_t& end) const
{
  // Do a binary search to find the first match:
  size_t istart = index(key);

  if (istart != RW_NPOS)
  {
    // Found one.  Do a linear search, looking for the last match:
    end = istart;
    while ( end+1 < entries() && *((*this)(end+1)) == *key)
     ++end;
  }

  return istart;
}

#ifdef RWDEBUG
template <class TP> RWBoolean
RWTPtrSortedVector<TP>::isSorted() const
{
  for(size_t idx=0; idx<nitems_-1; idx++)
  {
    // Not sorted if the item at this index is not less
    // than and not equal to the item at the next index
    // (i.e., it is greater than).
    if(!(*(*this)(idx) < *(*this)(idx+1)) && !(*(*this)(idx) == *(*this)(idx+1)))
      return FALSE;
  }
  return TRUE;
}
#endif

#ifndef __RWCOREERR_H__
#define __RWCOREERR_H__

/*
 * Error messages for Core.h++
 *
 * $Id: coreerr.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 *  Rogue Wave Software, Inc.
 *  P.O. Box 2328
 *  Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: coreerr.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.2  1993/05/18  21:48:25  keffer
 * Added RCS keywords
 *
 */

#include "rw/message.h"

extern const RWMsgId RWCORE_EOF;
extern const RWMsgId RWCORE_GENERIC;
extern const RWMsgId RWCORE_INVADDR;
extern const RWMsgId RWCORE_LOCK;
extern const RWMsgId RWCORE_NOINIT;
extern const RWMsgId RWCORE_NOMEM;
extern const RWMsgId RWCORE_OPERR;
extern const RWMsgId RWCORE_OUTALLOC;
extern const RWMsgId RWCORE_OVFLOW;
extern const RWMsgId RWCORE_STREAM;
extern const RWMsgId RWCORE_SYNSTREAM;

#endif /* __RWCOREERR_H__ */

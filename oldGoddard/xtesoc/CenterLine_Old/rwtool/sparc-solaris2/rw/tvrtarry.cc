
/*
 * Template definitions for RWTValVirtualArray<T>
 *
 * $Id: tvrtarry.cc,v 1.1 1994/05/26 17:46:33 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tvrtarry.cc,v $
// Revision 1.1  1994/05/26  17:46:33  sridhar
// Initial revision
//
 * Revision 1.5  1993/11/08  20:32:10  jims
 * Port to ObjectStore
 *
 * Revision 1.4  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.3  1993/02/17  18:32:03  keffer
 * Now passes T's by const reference, rather than by value
 *
 * Revision 1.2  1993/02/06  02:05:47  keffer
 * Added copyright notice.
 *
 *
 ***************************************************************************
 */

#include "rw/vpage.h"

template <class T>
RWTValVirtualArray<T>::RWTValVirtualArray(long size, RWVirtualPageHeap* heap)
{
  vref_ = new RWTVirtualRef<T>(size, heap);
}

template <class T>
RWTValVirtualArray<T>::~RWTValVirtualArray()
{
  if (vref_->removeReference() == 0) delete vref_;
}

template <class T>
RWTValVirtualArray<T>::RWTValVirtualArray(const RWTValVirtualArray<T>& v)
{
  vref_ = v.vref_;
  vref_->addReference();
}

template <class T>
RWTValVirtualArray<T>::RWTValVirtualArray(const RWTVirtualSlice<T>& sl)
{
  vref_ = new RWTVirtualRef<T>(sl.extent_, sl.varray_->heap());
  RWTVirtualRef<T>* vr = (RWTVirtualRef<T>*)sl.varray_->vref_;
  vref_->conformalCopy(0, *vr, sl.start_, sl.extent_);
}

template <class T> RWTValVirtualArray<T>&
RWTValVirtualArray<T>::operator=(const RWTValVirtualArray<T>& v)
{
  v.vref_->addReference();
  if (vref_->removeReference() == 0) delete vref_;
  vref_ = v.vref_;
  return *this;
}

template <class T> void
RWTValVirtualArray<T>::operator=(const RWTVirtualSlice<T>& sl)
{
  RWTValVirtualArray<T>* v2 = (RWTValVirtualArray<T>*)sl.varray_;
  RWTVirtualRef<T>* newvref = new RWTVirtualRef<T>(sl.extent_, v2->heap());
  newvref->conformalCopy(0, *v2->vref_, sl.start_, sl.extent_);
  if (vref_->removeReference() == 0) delete vref_;
  vref_ = newvref;
}

template <class T> T
RWTValVirtualArray<T>::operator=(const T& val)
{
  slice(0, length()-1) = val;		// Take a slice of self
  return val;
}

template <class T> void
RWTValVirtualArray<T>::cow()
{
  if (vref_->references()>1) {
    vref_->removeReference();
    vref_ = new RWTVirtualRef<T>(*vref_);
  }
}

/****************************************************************
 ****************************************************************
 *								*
 *			RWTVirtualSlice<T>			*
 *			Definitions				*
 *								*
 ****************************************************************
 ****************************************************************/

template <class T> T
RWTVirtualSlice<T>::operator=(const T& newVal)
{
  varray_->cow();
  varray_->vref_->set(start_, extent_, newVal);
  return newVal;
}

template <class T> void
RWTVirtualSlice<T>::operator=(const RWTVirtualSlice<T>& sl)
{
  RWVirtualRef& vr = (RWVirtualRef&)sl.varray_->vref_;

  varray_->cow();
  varray_->vref_->setSlice(start_, extent_, vr, sl.start_, sl.extent_);
}

template <class T> void
RWTVirtualSlice<T>::operator=(const RWTValVirtualArray<T>& v)
{
  RWTValVirtualArray<T>& va = (RWTValVirtualArray<T>&)v;
  varray_->cow();
  varray_->vref_->setSlice(start_, extent_, *(va.vref_), 0, v.length());
}


#ifndef  __RWIDENDICT_H__
#define  __RWIDENDICT_H__

/*
 * Declarations for RWIdentityDictionary (Hashed Identity Dictionary)
 *
 * $Id: idendict.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of 
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: idendict.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.4  1994/03/04  01:04:16  jims
 * Override isEqual member function from RWCollectable to return
 * TRUE or FALSE based on operator==
 *
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.2  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   25 May 1992 15:50:38   KEFFER
 * Optimized, reducing size.
 * 
 *    Rev 1.4   22 May 1992 17:04:12   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   29 Apr 1992 14:51:18   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 *    Rev 1.1   28 Oct 1991 09:08:18   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:15:20   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/hashdict.h"

/****************************************************************
 *								*
 *			RWIdentityDictionary			*
 *								*
 ****************************************************************/

/*
 * For the storage and retrieval of (key, value) pairs, using the identity
 * (i.e., the address) of the key.
 */

class RWExport RWIdentityDictionary : public RWHashDictionary {

  RWDECLARE_COLLECTABLE(RWIdentityDictionary)

public:

  RWIdentityDictionary(size_t N = RWCollection::DEFAULT_CAPACITY);

  virtual RWBoolean		isEqual(const RWCollectable*) const;

protected:

  virtual RWCollectableAssociation*
  				findAssociation(const RWCollectable* key) const;
  virtual RWCollectableAssociation*	
  				newAssociation(RWCollectable*, RWCollectable*) const;
  virtual RWCollectableAssociation*
  				removeAssociation(const RWCollectable* key);

};

#endif /* __RWIDENDICT_H__ */

#ifndef __RWSTRING_H__
#define __RWSTRING_H__

/*
 * RWString: Backwards compatibility declaration
 *
 * $Id: rwstring.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * This file is provided for backwards compatibility.  Note, however, that
 * the copy constructor and assignment operator of RWString used reference
 * semantics while the new RWCString uses copy semantics.
 *
 ***************************************************************************
 *
 * $Log: rwstring.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.8   01 Mar 1992 15:43:34   KEFFER
 * Now just a typedef to RWCString.
 */

#include "rw/cstring.h"

typedef RWCString RWString;

#endif	/* __RWSTRING_H__ */

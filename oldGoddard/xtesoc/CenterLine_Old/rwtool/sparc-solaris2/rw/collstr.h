#ifndef __RWCOLLSTR_H__
#define __RWCOLLSTR_H__

/*
 * RWCollectableString --- collectable strings
 *
 * $Id: collstr.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: collstr.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.4  1993/07/28  22:41:54  keffer
 * Changed to reflect new RWCString interface.
 *
 * Revision 2.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.2  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   22 May 1992 17:04:10   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.4   23 Apr 1992 08:42:40   KEFFER
 * Added RWCollectableString(const char*, unsigned) constructor.
 * 
 *    Rev 1.3   01 Mar 1992 15:46:06   KEFFER
 * Now uses RWCString instead of RWString.
 * 
 *    Rev 1.1   28 Oct 1991 09:08:12   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:13:56   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/cstring.h"
#include "rw/collect.h"

/****************************************************************
 *								*
 *			RWCollectableString			*
 *								*
 ****************************************************************/

class RWExport RWCollectableString : public RWCollectable, public RWCString {

  RWDECLARE_COLLECTABLE(RWCollectableString)

public:

  RWCollectableString();
  RWCollectableString(const char* a) : 		RWCString(a)   { }
  RWCollectableString(const char* a, size_t N) : RWCString(a,N) { }
  RWCollectableString(char c, size_t N) :	RWCString(c,N) { }
  RWCollectableString(const RWCString& s) :	RWCString(s)   { }
  RWCollectableString(const RWCSubString& s) :	RWCString(s)   { }

  /* Virtual functions inherited from RWCollectable */
  virtual RWspace		binaryStoreSize() const {return RWCString::binaryStoreSize();}
  virtual int			compareTo(const RWCollectable*) const;
  virtual unsigned		hash() const;
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;
};

#endif /* __RWCOLLSTR_H__ */

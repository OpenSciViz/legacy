#ifndef __RWCOLLDATE_H__
#define __RWCOLLDATE_H__

/*
 * RWCollectableDate --- RWCollectable Dates.
 *
 * $Id: colldate.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: colldate.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.9  1993/11/08  07:50:15  jims
 * Port to ObjectStore
 *
 * Revision 2.8  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.7  1993/04/07  02:55:10  myersn
 * eliminate member operator==.
 *
 * Revision 2.6  1993/03/31  02:54:58  myersn
 * add constructor from RWCString and RWLocale.
 *
 * Revision 2.5  1993/03/31  02:14:50  myersn
 * typo
 *
 * Revision 2.4  1993/03/31  01:53:08  myersn
 * fold in RWZone & RWLocale arguments supported by base classes RWDate & RWTime
 *
 * Revision 2.3  1993/03/26  21:16:25  myersn
 * replace dayTy etc. with unsigned.
 *
 * Revision 2.2  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   22 May 1992 17:04:10   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   18 Feb 1992 09:54:14   KEFFER
 * 
 *    Rev 1.2   28 Oct 1991 09:08:10   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.1   09 Oct 1991 18:32:56   keffer
 * Added RWCollectableDate(const RWDate&) constructor.
 * 
 *    Rev 1.0   28 Jul 1991 08:13:32   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/rwdate.h"
#include "rw/collect.h"
#include "rw/locale.h"

/****************************************************************
 *								*
 *			RWCollectableDate			*
 *								*
 ****************************************************************/

class RWExport RWCollectableDate : public RWCollectable, public RWDate {

  RWDECLARE_COLLECTABLE(RWCollectableDate)

public:
  RWCollectableDate();
  RWCollectableDate(unsigned d, unsigned y)	: RWDate(d, y)        { }
  RWCollectableDate(unsigned d, const char* month, unsigned y,
		    const RWLocale& loc = RWLocale::global())
						: RWDate(d, month, y, loc) { }
  RWCollectableDate(unsigned d, unsigned m, unsigned y)
						: RWDate(d, m, y)     { }
  RWCollectableDate(istream& s, const RWLocale& locale = RWLocale::global())
						: RWDate(s, locale)   { }
  RWCollectableDate(const RWCString& str,
                    const RWLocale& locale = RWLocale::global())
                                                : RWDate(str, locale) { }
  RWCollectableDate(const RWTime& t, const RWZone& zone = RWZone::local())
						: RWDate(t, zone)     { }
  RWCollectableDate(const RWDate& d)            : RWDate(d)           { }
  RWCollectableDate(const struct tm* tmb)	: RWDate(tmb)         { }

  /* Virtual functions inherited from RWCollectable */
  virtual RWspace		binaryStoreSize() const {return RWDate::binaryStoreSize();}
  virtual int			compareTo(const RWCollectable*) const;
  virtual unsigned		hash() const;
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;
};	  

#endif /* __RWCOLLECTABLE_DATE__ */

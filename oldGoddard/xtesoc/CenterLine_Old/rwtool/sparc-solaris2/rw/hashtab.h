#ifndef  __RWHASHTAB_H__
#define  __RWHASHTAB_H__

/*
 * Declarations for RWHashTable --- hash table lookup.
 *
 * $Id: hashtab.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of 
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Hash table look up with chaining
 * Duplicates are kept as distinct entries.
 * 
 ***************************************************************************
 *
 * $Log: hashtab.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.10  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.9  1993/07/19  20:45:26  keffer
 * friend classes now use elaborated-type-specifier (ARM Sec. 11.4)
 *
 * Revision 2.8  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.7  1993/04/01  00:32:13  myersn
 * add RW prefix to GVector mention.
 *
 * Revision 2.6  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.5  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.4  1993/02/07  21:31:32  keffer
 * Provided copy constructor for iterator
 *
 * Revision 2.3  1993/01/29  21:52:56  keffer
 * *** empty log message ***
 *
 * Revision 2.2  1993/01/28  22:40:22  keffer
 * Optimized insertions slightly.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   04 Aug 1992 18:59:52   KEFFER
 * Added keyword "virtual" to destructor for documentation purposes.
 * 
 *    Rev 1.4   25 May 1992 15:50:36   KEFFER
 * Optimized, reducing size.
 * 
 *    Rev 1.3   22 May 1992 17:02:32   KEFFER
 * Simplified algorithms
 * 
 *    Rev 1.2   22 May 1992 10:41:04   KEFFER
 * Uses RWDECLARE_COLLECTABLE.
 * 
 *    Rev 1.1   29 Apr 1992 15:51:26   KEFFER
 * Removed default argument from copyOld()
 * 
 *    Rev 1.0   29 Apr 1992 14:51:18   KEFFER
 * Hashing now uses chaining to resolve collisions
 */

#include "rw/colclass.h"
#include "rw/iterator.h"
#include "rw/gvector.h"

class RWExport RWSlistCollectables;
class RWExport RWSlistCollectablesIterator;
class RWExport RWHashTableIterator;
// Declare a vector of pointers to RWSlistCollectables:
typedef RWSlistCollectables* RWSlistCollectablesP;
declare(RWGVector,RWSlistCollectablesP)

/****************************************************************
 *								*
 *			RWHashTable				*
 *								*
 ****************************************************************/

class RWExport RWHashTable : public RWCollection {

  friend class RWExport RWHashTableIterator;
  RWDECLARE_COLLECTABLE(RWHashTable)

public:

  RWHashTable(size_t N = RWCollection::DEFAULT_CAPACITY);
  RWHashTable (const RWHashTable&);
  virtual ~RWHashTable();

  /******************** Member operators ****************************/
  RWHashTable&			operator=(const RWHashTable&);
  RWBoolean 			operator<=(const RWHashTable&) const;
  RWBoolean 			operator==(const RWHashTable&) const;
  RWBoolean 			operator!=(const RWHashTable&) const;

  /****************** Virtual member functions *******************/
  virtual void			apply(RWapplyCollectable, void*);
//virtual RWspace		binaryStoreSize() const;
  virtual void			clear();
//virtual void			clearAndDestroy();
//virtual int			compareTo(const RWCollectable*) const;
//virtual RWBoolean		contains(const RWCollectable*) const;
  virtual size_t		entries() const		{return nitems_;}
  virtual RWCollectable*	find(const RWCollectable*) const;
//virtual unsigned		hash() const;
  virtual RWCollectable*	insert(RWCollectable*);
  virtual RWBoolean		isEmpty() const		{return nitems_==0;}
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual size_t		occurrencesOf(const RWCollectable*) const;
  virtual RWCollectable*	remove(const RWCollectable*);
//virtual void			removeAndDestroy(const RWCollectable*); 
//virtual void			restoreGuts(RWvistream&);
//virtual void			restoreGuts(RWFile&);
//virtual void			saveGuts(RWvostream&) const;
//virtual void			saveGuts(RWFile&) const;

/********************** Special functions **********************************/
  virtual void			resize(size_t n = 0);

protected:

  RWGVector(RWSlistCollectablesP) table_;  // Table of pointers to chains.
  size_t			nitems_;   // Total number of stored objects.

protected:

  size_t			buckets() const {return table_.length();}
  size_t			hashIndex(const RWCollectable* p) const
     { return (size_t)p->hash() % buckets(); }
  RWCollectable*		insertIndex(size_t, RWCollectable*);
};



/****************************************************************
 *								*
 *		RWHashTableIterator				*
 *								*
 ****************************************************************/

class RWExport RWHashTableIterator : public RWIterator {

public:

  RWHashTableIterator(RWHashTable& h);
  RWHashTableIterator(const RWHashTableIterator&);
  virtual ~RWHashTableIterator();

  RWHashTableIterator&	operator=(const RWHashTableIterator&);

/*********** Virtual functions inherited from class RWIterator ***********/
  virtual RWCollectable*	findNext(const RWCollectable*);	// Find next matching item
  virtual RWCollectable*	key() const;			// Return current item
  virtual RWCollectable*	operator()();			// Advance iterator
  virtual void			reset();
/******************* Special iterator functions *******************************/
  RWCollectable*		remove();			// Remove current item
  RWCollectable*   		removeNext(const RWCollectable*);	// Remove next matching item

protected:

  void				nextIterator();

private:

  RWHashTable*			myHash_;
  size_t			idx_;		// which Slist is current
  RWSlistCollectablesIterator*	iterator_;	// an iterator for that Slist

};


#endif /* __RWHASHTAB_H__ */


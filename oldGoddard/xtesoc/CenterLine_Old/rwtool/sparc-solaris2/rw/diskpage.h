#ifndef __RWDISKPAGE_H__
#define __RWDISKPAGE_H__

/*
 * RWDiskPageHeap: Specializing buffered page heap that swaps pages out to disk.
 *
 * $Id: diskpage.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: diskpage.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.0   11 Mar 1992 14:09:58   KEFFER
 * Initial revision.
 */

#include "rw/bufpage.h"
#include "rw/bitvec.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

class RWExport RWDiskPageHeap : public RWBufferedPageHeap {

public:

  RWDiskPageHeap(const char* filename=0, unsigned nbufs=10, unsigned pgsize=512);
  virtual		~RWDiskPageHeap();

  RWBoolean		isValid() const {return tempfp_!=0;}

  // Inherited from RWPageBuffer:
  virtual RWHandle	allocate();
  virtual void		deallocate(RWHandle);

protected:

  enum HandleStatus { NotUsed, NoSwapSpace, HasSwapSpace };

  RWBoolean		allocateDiskPage(RWHandle);
  RWBoolean		handleValid(RWHandle);
  RWoffset		offsetOfHandle(RWHandle);
  void			resize(unsigned);

  // Inherited from RWBufferedPageHeap:
  virtual RWBoolean	swapIn(RWHandle, void*);
  virtual RWBoolean	swapOut(RWHandle, void*);

private:

  static const unsigned		initialPages_;
  static const unsigned		initialHandles_;
  static const unsigned		pageIncrement_;
  static const unsigned		handleIncrement_;
  RWBitVec			freePageMap_;	// Bit flags for free disk pages
  unsigned*			handleMap_;	// Array that maps from handle number to disk page
  HandleStatus*			handleStatus_;	// Status of each slot in handleMap_
  unsigned			nHandles_;	// Length of handleMap_ and handleStatus_
  FILE*				tempfp_;

};

#endif	/* __RWDISKPAGE_H__ */

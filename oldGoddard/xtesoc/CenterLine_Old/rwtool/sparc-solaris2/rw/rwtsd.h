#ifndef __RWTSD_H__
#define __RWTSD_H__

/*
 * Declarations for Task Specific Data calls to rwtsd.dll
 *
 * $Id: rwtsd.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwtsd.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 1.9  1993/11/22  17:47:04  jims
 * Restore file to sane state (same as rev 1.6)
 *
 * Revision 1.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/08/05  11:40:24  jims
 * Remove exitProc and GlobalRelease... function
 *
 * Revision 1.4  1993/02/10  02:45:35  jims
 * Now compiles under STRICT; RWTSDKEY typedef changed to unsigned long
 *
 * Revision 1.3  1993/02/05  07:55:14  jims
 * Cosmetic changes
 *
 * Revision 1.2  1993/02/03  20:15:55  jims
 * Added test to see if header file already included.
 * Added header and log information
 *
 * 
 */
#include <windows.h>

typedef unsigned long RWTSDKEY;

RWTSDKEY FAR PASCAL 
RWGetTaskSpecificKey();

int FAR PASCAL 
RWSetTaskSpecificData(RWTSDKEY hKey, void FAR*);

void FAR* FAR PASCAL 
RWGetTaskSpecificData(RWTSDKEY hKey);

void FAR* FAR PASCAL 
RWReleaseTaskSpecificData(RWTSDKEY hKey);

#endif  /* __RWTSD_H__ */

/*
 * RWTValDlist<T> example, from the manual.
 *
 * $Id: tvdlist.cpp,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1992 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tvdlist.cpp,v $
# Revision 1.1  1994/05/26  17:48:47  sridhar
# Initial revision
#
 * Revision 2.3  1993/06/16  01:11:04  keffer
 * Added RCS keywords.
 *
 *
 */

#include <rw/tvdlist.h>
#include <rw/rstream.h>
#include <string.h>

class Dog {
  char* name;
public:
  Dog(){name=0;}
  Dog( const char* c) {
    name = new char[strlen(c)+1];
    strcpy(name, c);
  }

  ~Dog() { delete name; }

  // Define a copy constructor:
  Dog(const Dog& dog) {
    name = new char[strlen(dog.name)+1];
    strcpy(name, dog.name);
  }

  // Define an assignment operator:
  void operator=(const Dog& dog) {
    if (this!=&dog) {
      delete name;
      name = new char[strlen(dog.name)+1];
      strcpy(name, dog.name);
    }
  }

  // Define an equality test operator:
  int operator==(const Dog& dog) const {
    return strcmp(name, dog.name)==0;
  }

  // Define a less-than operator
  int operator<(const Dog& dog) const {
	return strcmp(name, dog.name) < 0;
  }

  friend ostream& operator<<(ostream& str, const Dog& dog){
    str << dog.name;
    return str;
  }
};

main()
{
  RWTValDlist<Dog> terriers;
  terriers.insert("Cairn Terrier");	// NB: type conversion occurs
  terriers.insert("Irish Terrier");
  terriers.insert("Schnauzer");

  cout << "The list " <<
    (terriers.contains("Schnauzer") ? "does " : "does not ") <<
    "contain a Schnauzer\n";

  terriers.insertAt(
      terriers.index("Irish Terrier"),
      "Fox Terrier"
    );

  while (!terriers.isEmpty())
    cout << terriers.get() << endl;

  return 0;
}

#ifndef __BUS_H__
#define __BUS_H__

/*
 * Declarations used for the Bus example in the Tools.h++ manual
 *
 * $Id: bus.h,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: bus.h,v $
 * Revision 1.1  1994/05/26  17:48:47  sridhar
 * Initial revision
 *
 * Revision 2.5  1993/06/21  18:00:55  keffer
 * Added RCS keywords
 *
 *
 */

#include "rw/rwset.h"
#include "rw/cstring.h"
#include "rw/collstr.h"

class Bus : public RWCollectable
{

	RWDECLARE_COLLECTABLE(Bus)

public:

	Bus();
	Bus(int busno, const RWCString& driver);
	~Bus();

	// Inherited from class "RWCollectable":
	RWspace		binaryStoreSize() const;
	int		compareTo(const RWCollectable*) const;
	RWBoolean	isEqual(const RWCollectable*) const;
	unsigned	hash() const;
	void		restoreGuts(RWFile&);
	void		restoreGuts(RWvistream&);
	void		saveGuts(RWFile&) const;
	void		saveGuts(RWvostream&) const;

	void		addPassenger(const char* name);
	void		addCustomer(const char* name);
	size_t		customers() const;
	size_t		passengers() const;
	RWCString	driver() const		{return driver_;}
	int		number() const		{return busNumber_;}

private:

	RWSet		customers_;
	RWSet*		passengers_;
	int		busNumber_;
	RWCString	driver_;
};

#endif


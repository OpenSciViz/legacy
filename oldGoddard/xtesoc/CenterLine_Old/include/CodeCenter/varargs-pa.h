/* $Id: varargs-pa.h,v 1.4 1993/02/26 12:59:58 sudduth Exp $ */
#if defined(__CENTERLINE__)
#if !defined(_VARARGS_INCLUDED)
#define _VARARGS_INCLUDED

#if !defined(_SYS_STDSYMS_INCLUDED)
#include <sys/stdsyms.h>
#endif /* !_SYS_STDSYMS_INCLUDED */

#if !defined(_VA_LIST)
#define _VA_LIST

typedef double *va_list;

#endif /* !_VA_LIST */

#define va_end(__list)

#define __WORD_MASK 0xFFFFFFFC
#define __DW_MASK   0xFFFFFFF8

#if defined(_STDARG_INCLUDED)

#define va_start(a,b) ((a)=(va_list)&(b))

#    define va_arg(__list,__mode)					\
	(sizeof(__mode) > 8 ?						\
	  ((__list = (va_list) ((char *)__list - sizeof (int))),	\
	   (*((__mode *) (*((int *) (__list)))))) :			\
	  ((__list =							\
	      (va_list) ((long)((char *)__list - sizeof (__mode))	\
	      & (sizeof(__mode) > 4 ? __DW_MASK : __WORD_MASK))),	\
	   (*((__mode *) ((char *)__list +				\
		((8 - sizeof(__mode)) % 4))))))

#else /* !_STDARG_INCLUDED */

#define va_dcl long va_alist;

#define va_start(a) ((a)=(char *)&va_alist+4)

#    define va_arg(__list,__mode)					\
	(sizeof(__mode) > 8 ?						\
	  ((__list = (va_list) ((char *)__list - sizeof (int))),	\
	   (*((__mode *) (*((int *) (__list)))))) :			\
	  ((__list =							\
	      (va_list) ((long)((char *)__list - sizeof (__mode))	\
	      & (sizeof(__mode) > 4 ? __DW_MASK : __WORD_MASK))),	\
	   (*((__mode *) ((char *)__list +				\
		((8 - sizeof(__mode)) % 4))))))

#endif /* !_STDARG_INCLUDED */
#endif /* !_VARARGS_INCLUDED */
#else /* !__CENTERLINE__ */
/*
 * Do not include this version of varargs-pa.h unless you loading your
 * code into Codecenter/Objectcenter as source. 
 */
@@@ The Centerline version of varargs-pa.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

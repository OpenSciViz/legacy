/* Special va-m88k.h for use in Codecenter/Objectcenter on the Motorola platform */
#ifndef _CL_VA_M88K_H
#define _CL_VA_M88K_H

#ifdef __CENTERLINE__

#ifndef __CL_VA_LIST
#define  __CL_VA_LIST
typedef struct {
	int  __va_arg;		/* argument number */
	int *__va_stk;		/* start of args passed on stack */
	int *__va_reg;		/* start of args passed in regs */
} va_list, __gnuc_va_list;
#endif /*  __CL_VA_LIST */

#define	__va_size(__t) ((sizeof(__t) + 3) / 4)

#define __va_align(__p,__t) \
(__alignof__ (*(__t *)0) >= 8 ? ((__p).__va_arg & 1) : 0)

#define __va_ptr(__p,__t)    (__p).__va_stk

#define va_end(__p)

#define va_arg(__p,__t) \
   (((__p).__va_arg += __va_align(__p,__t) + __va_size(__t)),    (* ((__t *) (__va_ptr (__p, __t) + ((__p).__va_arg - __va_size (__t))))))

#if defined(__cplusplus) && !defined(__GNUG__)
extern "C" {
           int  __alignof__(...);
           }
#endif /* __cplusplus && !__GNUG */
#else

    @@@ The Centerline version of cl_va-m88k.h should only be used
         within CODECENTER or OBJECTCENTER. @@@

#endif /* !__CENTERLINE__ */
#endif /* _CL_VA_M88K_H */

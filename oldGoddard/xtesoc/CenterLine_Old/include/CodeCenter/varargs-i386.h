/* Special varargs.h for use within Codecenter on the intel platform */

#ifdef __CENTERLINE__
/* just include the system version */
#include "/usr/include/varargs.h"
#else

    @@@ The Centerline version of varargs.h should only be used
         within CODECENTER or OBJECTCENTER. @@@

#endif /* !__CENTERLINE__ */

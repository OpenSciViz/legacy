#if defined(__CENTERLINE__)
#ifndef _CENTERLINE_VARARGS_INCLUDED
#define _CENTERLINE_VARARGS_INCLUDED

#ifdef sparc
#include "varargs-sparc.h"
#endif /* sparc */

#ifdef __hp9000s700
#include "varargs-pa.h"
#endif /* __hp9000s700 */

#ifdef __m88k__
#include "varargs-m88k.h"
#endif /* __m88k__ */

#ifdef _I386
#include "varargs-i386.h"
#endif /* _I386 */

#endif /* _CENTERLINE_VARARGS_INCLUDED */
#else
/*
 * Do not include this version of varargs.h unless you loading your
 * code into Codecenter/Objectcenter as source.
 */
@@@ The Centerline version of varargs.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

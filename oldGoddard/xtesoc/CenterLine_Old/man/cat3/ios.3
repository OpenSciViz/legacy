


IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



NAME
     ios - input/output formatting

SYNOPSIS
     #include <iostream.h>

     class ios {
     public:
               enum      io_state { goodbit=0, eofbit, failbit, badbit };
               enum      open_mode { in, out, ate, app, trunc, nocreate, noreplace };
               enum      seek_dir { beg, cur, end };
               /* flags for controlling format */
               enum      { skipws=01,
                           left=02,  right=04, internal=010,
                           dec=020, oct=040, hex=0100,
                           showbase=0200, showpoint=0400, uppercase=01000, showpos=02000,
                           scientific=04000, fixed=010000,
                           unitbuf=020000, stdio=040000 };
               static const long basefield;
                         /* dec|oct|hex */
               static const long adjustfield;
                         /* left|right|internal */
               static const long floatfield;
                         /* scientific|fixed */
     public:
                         ios(streambuf*);

               int       bad() const;
               static longbitalloc();
               void      clear(int state =0);
               int       eof() const;
               int       fail() const;
               char      fill() const;
               char      fill(char);
               long      flags() const;
               long      flags(long);
               int       good() const;
               long&     iword(int);
               int       operator!() const;
                         operator void*();
                         operator const void*() const;
               int       precision() const;
               int       precision(int);
               streambuf*rdbuf();
               void* &   pword(int);
               int       rdstate() const;
               long      setf(long setbits, long field);
               long      setf(long);
               static voidsync_with_stdio();
               ostream*  tie();
               ostream*  tie(ostream*);
               long      unsetf(long);



                 Last change: C++ Stream Library                1






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



               int       width() const;
               int       width(int);
               static intxalloc();
     protected:
                         ios();
                         init(streambuf*);
     private:
                         ios(ios&);
               void      operator=(ios&);
     };

               /* Manipulators */
     ios&      dec(ios&) ;
     ios&      hex(ios&) ;
     ios&      oct(ios&) ;
     ostream&  endl(ostream& i) ;
     ostream&  ends(ostream& i) ;
     ostream&  flush(ostream&) ;
     istream&  ws(istream&) ;

DESCRIPTION
     The stream classes derived from class  ios  provide  a  high
     level  interface  that  supports  transferring formatted and
     unformatted information into and out  of  streambufs.   This
     manual  page  describes  the operations common to both input
     and output.

     Several enumerations are declared in class  ios,  open_mode,
     io_state, seek_dir, and format flags, to avoid polluting the
     global name space.  The  io_states  are  described  on  this
     manual  page  under  "Error  States."  The format fields are
     also  described  on  this  page,  under  "Formatting."   The
     open_modes  are  described  in detail in _f_s_t_r_e_a_m(_3_C++) under
     open().  The seek_dirs are described in _s_b_u_f._p_u_b(_3_C++) under
     seekoff().

     In the following descriptions assume:
          - s and s2 are ioss.
          - sr is an ios&.
          - sp is a ios*.
          - i, oi j, and n are ints.
          - l, f, and b are longs.
          - c and oc are chars.
          - osp and oosp are ostream*s.
          - sb is a streambuf*.
          - pos is a streampos.
          - off is a streamoff.
          - dir is a seek_dir.
          - mode is an int representing an open_mode.
          - fct is a function with type ios& (*)(ios&).
          - vp is a void*&.




                 Last change: C++ Stream Library                2






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



  Constructors and assignment:
          ios(sb)
               The streambuf denoted by sb becomes the  streambuf
               associated  with  the  constructed  ios.  If sb is
               null, the effect is undefined.

          ios(sr)
          s2=s
               Copying of ioss is not  well-defined  in  general,
               therefore the constructor and assignment operators
               are private so that  the  compiler  will  complain
               about  attempts  to  copy  ios  objects.   Copying
               pointers to iostreams is usually what is desired.

          ios()
          init(sb)
               Because class ios is now inherited  as  a  virtual
               base  class,  a constructor with no arguments must
               be used.  This constructor is declared  protected.
               Therefore  ios::init(streambuf*)  is declared pro-
               tected and must  be  used  for  initialization  of
               derived classes.

  Error States
     An ios has an internal _e_r_r_o_r _s_t_a_t_e (which is a collection of
     the  bits  declared  as  io_states).  Members related to the
     error state are:

          i=s.rdstate()
               Returns the current error state.

          s.clear(i)
               Stores i as the error state.  If i is  zero,  this
               clears  all  bits.   To set a bit without clearing
               previously  set  bits  requires   something   like
               s.clear(ios::badbit|s.rdstate()).

          i=s.good()
               Returns non-zero if the error state  has  no  bits
               set, zero otherwise.

          i=s.eof()
               Returns non-zero if eofbit is  set  in  the  error
               state,  zero  otherwise.  Normally this bit is set
               when an end-of-file has been encountered during an
               extraction.

          i=s.fail()
               Returns non-zero if either badbit  or  failbit  is
               set  in the error state, zero otherwise.  Normally
               this indicates that some extraction or  conversion
               has  failed, but the stream is still usable.  That



                 Last change: C++ Stream Library                3






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



               is, once the failbit is cleared, I/O on s can usu-
               ally continue.

          i=s.bad()
               Returns non-zero if badbit is  set  in  the  error
               state,  zero  otherwise.   This  usually indicates
               that some operation on  s.rdbuf()  has  failed,  a
               severe  error,  from  which  recovery  is probably
               impossible.  That is, it will probably be impossi-
               ble to continue I/O operations on s.

  Operators
     Three operators are defined to allow convenient checking  of
     the  error  state  of  an  ios:  operator!(), operator const
     void*(), and operator void*().   The  latter  two  functions
     convert  an  ios  to a pointer so that it can be compared to
     zero.  The conversion will return 0 if failbit or badbit  is
     set in the error state, and will return a pointer value oth-
     erwise.  This pointer is not meant to be used.  This  allows
     one to write expressions such as:

          if ( cin ) ...

          if ( cin >> x ) ...


     The ! operator returns non-zero if failbit or badbit is  set
     in  the  error state, which allows expressions like the fol-
     lowing to be used:

          if ( !cout ) ...


  Formatting
     An ios has a _f_o_r_m_a_t _s_t_a_t_e that is used by input  and  output
     operations  to control the details of formatting operations.
     For other operations the  format  state  has  no  particular
     effect  and  its  components  may  be set and examined arbi-
     trarily by user code.   Most  formatting  details  are  con-
     trolled by using the flags(), setf(), and unsetf() functions
     to set  the  following  flags,  which  are  declared  in  an
     enumeration  in  class  ios.   Three other components of the
     format state are controlled separately  with  the  functions
     fill(), width(), and precision().

          skipws
               If skipws is set, whitespace will  be  skipped  on
               input.   This applies to scalar extractions.  When
               skipws is  not  set,  whitespace  is  not  skipped
               before the extractor begins conversion.  As a pre-
               caution against looping,  zero  width  fields  are
               considered  a  bad format by the extractors, so if



                 Last change: C++ Stream Library                4






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



               the next character  is  whitespace  and  the  skip
               variable  is  not  set,  the arithmetic extractors
               will signal an error.

          left
          right
          internal
               These flags control the padding of a value.   When
               left  is set, the value is left-adjusted, that is,
               the fill character is added after the value.  When
               right  is  set,  the value is right-adjusted, that
               is, the fill character is added before the  value.
               When  internal is set, the fill character is added
               after any leading sign  or  base  indication,  but
               before the value.  Right-adjustment is the default
               if none of these flags is set.  These  fields  are
               collectively  identified  by  the  static  member,
               ios::adjustfield.   The  fill  character  is  con-
               trolled  by  the fill() function, and the width of
               padding is controlled by the width() function.

          dec
          oct
          hex
               These flags  control  the  conversion  base  of  a
               value.  The conversion base is 10 (decimal) if dec
               is set, but if oct or hex is set, conversions  are
               done  in  octal  or hexidecimal, respectively.  If
               none of these is set, insertions are  in  decimal,
               but  extractions  are interpreted according to the
               C++ lexical conventions  for  integral  constants.
               These  fields  are  collectively identified by the
               static member, ios::basefield.  The  flag  dec  is
               set  by  default  in  ios::init(streambuf*), and a
               manipulator must be used to change the  conversion
               base flags to another value; see _m_a_n_i_p(_3_C++).  The
               manipulators hex, dec, and oct can also be used to
               set  the  conversion base; see "Built-in Manipula-
               tors" below.

          showbase
               If showbase is set, insertions will  be  converted
               to  an external form that can be read according to
               the C++  lexical  conventions  for  integral  con-
               stants.  showbase is unset by default.

          showpos
               If showpos is set, then a  "+"  will  be  inserted
               into  a  decimal  conversion of a postive integral
               value.

          uppercase



                 Last change: C++ Stream Library                5






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



               If uppercase is set, then an uppercase "X" will be
               used  for  hexadecimal conversion when showbase is
               set, or an uppercase "E" will  be  used  to  print
               floating point numbers in scientific notation.

          showpoint
               If showpoint is set, trailing  zeros  and  decimal
               points  appear  in  the result of a floating point
               conversion.

          scientific
          fixed
               These flags control the format to which a floating
               point  value  is  converted  for  insertion into a
               stream.  If scientific is set, the value  is  con-
               verted  using  scientific notation, where there is
               one digit before the decimal point and the  number
               of  digits after it is equal to the _p_r_e_c_i_s_i_o_n (see
               below), which is six by default.  An uppercase "E"
               will introduce the exponent if uppercase _i_s _s_e_t, _a
               _l_o_w_e_r_c_a_s_e "_e" _w_i_l_l _a_p_p_e_a_r _o_t_h_e_r_w_i_s_e.  _I_f _f_i_x_e_d  is
               set,  the  value  is converted to decimal notation
               with _p_r_e_c_i_s_i_o_n digits after the decimal point,  or
               six  by  default.  If neither scientific _n_o_r _f_i_x_e_d
               is set, then the value  will  be  converted  using
               either  notation,  depending on the value;  scien-
               tific notation will be used only if  the  exponent
               resulting  from  the conversion is less than -4 or
               greater than the precision.  If showpoint  _i_s  _n_o_t
               _s_e_t,  _t_r_a_i_l_i_n_g  _z_e_r_o_e_s _a_r_e _r_e_m_o_v_e_d _f_r_o_m _t_h_e _r_e_s_u_l_t
               _a_n_d _a _d_e_c_i_m_a_l _p_o_i_n_t _a_p_p_e_a_r_s _o_n_l_y _i_f _i_t _i_s _f_o_l_l_o_w_e_d
               _b_y _a _d_i_g_i_t.  _s_c_i_e_n_t_i_f_i_c and fixed _a_r_e _c_o_l_l_e_c_t_i_v_e_l_y
               _i_d_e_n_t_i_f_i_e_d _b_y _t_h_e _s_t_a_t_i_c _m_e_m_b_e_r _i_o_s::_f_l_o_a_t_f_i_e_l_d.

          unitbuf
               When set, a flush is performed by  ostream::osfx()
               after  each  insertion.  Unit buffering provides a
               compromise between buffered output and  unbuffered
               output.   Performance is better under unit buffer-
               ing than unbuffered output, which makes  a  system
               call  for  each  character output.  Unit buffering
               makes a system call for each insertion  operation,
               and    doesn't    require   the   user   to   call
               ostream::flush().

          stdio
               When  set,  stdout  and  stderr  are  flushed   by
               ostream::osfx() after each insertion.

     The following functions use and set  the  format  flags  and
     variables.




                 Last change: C++ Stream Library                6






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



          oc=s.fill(c)
               Sets the "fill character" format state variable to
               c  and returns the previous value.  c will be used
               as the padding character, if one is necessary (see
               width(),  below).   The  default  fill  or padding
               character is a space.  The positioning of the fill
               character  is  determined  by the right, left, and
               internal flags; see above.  A parameterized  mani-
               pulator,  setfill,  is  also available for setting
               the fill character; see _m_a_n_i_p(_3_C++).

          c=s.fill()
               Returns the "fill character"  format  state  vari-
               able.

          l=s.flags()
               Returns the current format flags.

          l=s.flags(f)
               Resets all the format flags to those specified  in
               f and returns the previous settings.

          oi=s.precision(i)
               Sets the "precision" format state  variable  to  i
               and  returns  the  previous  value.  This variable
               controls the number of significant digits inserted
               by the floating point inserter.  The default is 6.
               A parameterized manipulator, setprecision, is also
               available   for   setting   the   precision;   see
               _m_a_n_i_p(_3_C++).

          i=s.precision()
               Returns the "precision" format state variable.

          l=s.setf(b)
               Turns on in s the format flags  marked  in  b  and
               returns  the  previous  settings.  A parameterized
               manipulator, setiosflags, performs the same  func-
               tion; see _m_a_n_i_p(_3_C++).

          l=s.setf(b,f)
               Resets in s only the format flags specified  by  f
               to  the settings marked in b, and returns the pre-
               vious settings.  That is, the format flags  speci-
               fied by f are cleared in s, then reset to be those
               marked in b.  For example, to change  the  conver-
               sion  base  in  s  to  be  hex,  one  could write:
               s.setf(ios::hex,ios::basefield)  .  ios::basefield
               specifies  the  conversion base bits as candidates
               for change, and ios::hex specifies the new  value.
               s.setf(0,f)  will  clear all the bits specified by
               f,   as   will   a   parameterized    manipulator,



                 Last change: C++ Stream Library                7






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



               resetiosflags; see _m_a_n_i_p(_3_C++).

          l=s.unsetf(b)
               Unsets in s the bits set in b and returns the pre-
               vious settings.

          oi=s.width(i)
               Sets the "field width" format variable  to  i  and
               returns  the previous value.  When the field width
               is zero (the default), inserters will insert  only
               as  many  characters as necessary to represent the
               value being inserted.  When  the  field  width  is
               non-zero,  the inserters will insert at least that
               many characters, using the fill character  to  pad
               the  value,  if  the value being inserted requires
               fewer   than   field-width   characters   to    be
               represented.  However, the numeric inserters never
               truncate values, so if the  value  being  inserted
               will  not fit in field-width characters, more than
               field-width characters will be output.  The  field
               width is always interpreted as a mininum number of
               characters; there is no direct way  to  specify  a
               maximum  number  of  characters.   The field width
               format variable is reset  to  the  default  (zero)
               after  each  insertion  or extraction, and in this
               sense it behaves as a parameter for insertions and
               extractions.   A  parameterized manipulator, setw,
               is also  available  for  setting  the  width,  see
               _m_a_n_i_p(_3_C++).

          i=s.width()
               Returns the "field width" format variable.

  User-defined Format Flags
     Class ios can be used as a base class  for  derived  classes
     that require additional format flags or variables.  The ios-
     tream library provides several functions to  do  this.   The
     two  static  member functions ios::xalloc and ios::bitalloc,
     allow several such  classes  to  be  used  together  without
     interference.

          b=ios::bitalloc()
               Returns a long with a single,  previously  unallo-
               cated,  bit  set.   This  allows users who need an
               additional flag to acquire one, and pass it as  an
               argument to ios::setf(), for example.

          i=ios::xalloc()
               Returns a previously unused index into an array of
               words  available for use as format state variables
               by derived classes.




                 Last change: C++ Stream Library                8






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



          l=s.iword(i)
               When i  is  an  index  allocated  by  ios::xalloc,
               iword()  returns  a  reference  to  the  ith user-
               defined word.

          vp=s.pword(i)
               When i  is  an  index  allocated  by  ios::xalloc,
               pword()  returns  a  reference  to  the  ith user-
               defined word.  pword() is the same as iword except
               that it is typed differently.

  Other members:
          sb=s.rdbuf()
               Returns a pointer to the streambuf associated with
               s when s was constructed.

          ios::sync_with_stdio()
               Solves problems that arise when mixing  stdio  and
               iostreams.   The  first  time it is called it will
               reset the standard  iostreams  (cin,  cout,  cerr,
               clog) to be streams using  stdiobufs.  After that,
               input and output using these streams may be  mixed
               with  input  and  output  using  the corresponding
               FILEs (stdin, stdout,  and  stderr)  and  will  be
               properly  synchronized.   sync_with_stdio()  makes
               cout and cerr unit buffered (see ios::unitbuf  and
               ios::stdio   above).   Invoking  sync_with_stdio()
               degrades performance a variable amount,  depending
               on  the  length  of  the  strings  being  inserted
               (shorter strings incur a larger performance hit).

          oosp=s.tie(osp)
               Sets the "tie" variable to osp,  and  returns  its
               previous  value.  This variable supports automatic
               "flushing" of ioss.  If the tie variable  is  non-
               null and an ios needs more characters or has char-
               acters to be consumed, the ios pointed at  by  the
               tie  variable is flushed.  By default, cin is tied
               initially to cout so that  attempts  to  get  more
               characters  from standard input result in flushing
               standard output.  Additionally, cerr and clog  are
               tied  to cout by default.  For other ioss, the tie
               variable is set to zero by default.

          osp=s.tie()
               Returns the "tie" variable.

  Built-in Manipulators:
     Some convenient manipulators (functions that take  an  ios&,
     an  istream&,  or an ostream& and return their argument, see
     _m_a_n_i_p(_3_C++)) are:
     sr<<dec



                 Last change: C++ Stream Library                9






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



     sr>>dec
          These set the conversion base format flag to 10.

     sr<<hex
     sr>>hex
          These set the conversion base format flag to 16.

     sr<<oct
     sr>>oct
          These set the conversion base format flag to 8.

          sr>>ws
               Extracts     whitespace      characters.       See
               _i_s_t_r_e_a_m(_3_C++).

          sr<<endl
               Ends a line by inserting a newline  character  and
               flushing.  See _o_s_t_r_e_a_m(_3_C++).

          sr<<ends
               Ends a string by inserting a null  (0)  character.
               See _o_s_t_r_e_a_m(_3_C++).

          sr<<flush
               Flushes outs.  See _o_s_t_r_e_a_m(_3_C++).

     Several  parameterized  manipulators  that  operate  on  ios
     objects  are  described in _m_a_n_i_p(_3_C++):  setw, setfill, set-
     precision, setiosflags, and resetiosflags.

     The streambuf associated with an ios may be  manipulated  by
     other methods than through the ios.  For example, characters
     may be stored in a queuelike streambuf  through  an  ostream
     while  they  are  being  fetched through an istream.  Or for
     efficiency some part of a program may choose to do streambuf
     operations  directly  rather  than through the ios.  In most
     cases the program does not have to worry about  this  possi-
     bility,  because  an  ios  never saves information about the
     internal state of a streambuf.  For example, if the  stream-
     buf   is  repositioned  between  extraction  operations  the
     extraction (input) will proceed normally.

CAVEATS
     The need for sync_with_stdio is  a  wart.   The  old  stream
     package  did  this as a default, but in the iostream package
     unbuffered stdiobufs are too inefficient to be the default.

     The stream package had a constructor that took a FILE* argu-
     ment.   This  is  now  replaced  by  stdiostream.  It is not
     declared even as an obsolete form to avoid having iostream.h
     depend on stdio.h.




                 Last change: C++ Stream Library               10






IOS(3C++)         Misc. Reference Manual Pages          IOS(3C++)



     The old stream package allowed copying of streams.  This  is
     disallowed  by  the  iostream  package.  However, objects of
     type     istream_withassign,     ostream_withassign,     and
     iostream_withassign  can  be  assigned  to.   Old code using
     copying can usually be rewritten to use  pointers  or  these
     classes.   (The  standard  streams cin, cout, cerr, and clog
     are members of "withassign" classes, so they can be assigned
     to, as in cin = inputfstream.)

SEE ALSO
     IOS.INTRO(3C++),       streambuf(3C++),       istream(3C++),
     ostream(3C++), manip(3C++).











































                 Last change: C++ Stream Library               11




.TH CLEZSTART 1 "July 1993" "{Code,Object}Center" "{Code,Object}Center Manual Pages"
.SH NAME
clezstart \- build a makefile to use in loading a project into {Code,Object}Center
.SH SYNOPSIS
.B clezstart
[\c
.I clezstart_switches\c
\&...] [\c
.I make_arguments\c
\&...]
.SH DESCRIPTION
{Code,Object}Center offers many features
not found in other debugging tools.
In order to provide these features, {Code,Object}Center
needs to load all of the libraries and object files,
and optionally,
the source files,
which make up the executable you want to debug.
For simple programs consisting of a few source files
and libraries,
it is relatively easy to manually load all libraries
and source or object files into {Code,Object}Center,
using the Project Browser or the \fBload\fR command.
Larger programs present more of a problem.
It becomes increasingly difficult to manually load
all files needed for typical large projects.
.PP
The {Code,Object}Center
.I Reference Manual
shows how to modify the makefiles used to build your executable
so that they contain targets which can be used to load
all the files required (see the entry on
.B make\c
).
Once the load targets are added to your makefiles,
typing a command of the form:
.sp
.B make
.I program\c
.B _src
.sp
(assuming
.I program\c
.B _src
represents one of the load targets for the program
.I program)
from within {Code,Object}Center
would cause all files used in the build of the
program to be loaded into {Code,Object}Center.
.PP
Performing the modifications to the makefiles to
add the load targets could be a lengthy and
error-prone process,
depending of course on the complexity of the makefiles.
\fBclezstart\fR is a tool which can be used to automatically
generate load targets for your program.
Using your makefile as input,
it builds an additional makefile, which contains load
targets you can use to load the component files
of your program into {Code,Object}Center. 
Due to the almost infinite number of combinations
of tools and parameters used in makefiles,
it is not always possible for \fBclezstart\fR to produce
load targets,
but it works perfectly in almost all cases,
and provides a good basis for the more manual method
described above even when it cannot fully understand
a make.
If you use make to build your program,
\fBclezstart\fR can help get your program loaded.
.SH HOW IT WORKS
\fBclezstart\fR runs make,
just as if you ran it from the command line.
However,
before it actually starts the make,
it sets up to monitor all of the actions taken
during the make.
As the make proceeds,
\fBclezstart\fR builds a list of each of the actions
taken by the make,
and the directories in which those actions
took place.
After the make is complete,
\fBclezstart\fR analyzes the list,
and builds another makefile,
named
.B Makefile.cline,
which contains load targets for the program
built during the make.
Two load targets are built.
One,
.I program\c
.B _src,
(where
.I program
is the name of the executable)
contains {Code,Object}Center \fBload\fR commands for all of
the source files and all of the libraries used to build
the executable.
The other,
.I program\c
.B _obj,
contains the \fBload\fR commands for all of the object files
and libraries used to build the executable.
.PP
Once generated,
the \fBMakefile.cline\fR can be used directly to load up
{Code,Object}Center, with a workspace command
of this form:
.sp
.B make -f Makefile.cline
.I program\c
.B _src
.sp
.SH SWITCHES
An invocation of \fBclezstart\fR contains any of the following
optional clezstart switches,
followed by the arguments (switches and targets)
you ordinarily use with
.BR make :
.TP
.B \-relative
Generate relative pathnames for the
.B -I
and
.B -L
switches for compilers and linkers.
By default,
absolute pathnames are generated
for these switches.
Note that if you set this switch and do not also set the \fB-path\fR 
switch, the {Code,Object}Center 
.B swap
command may not work correctly.
.TP
.B \-libobjs
The load targets will contain a \fBload\fR command
for each individual object file (or its corresponding source 
file) in each library
built by the makefile.
This allows you to swap
individual library modules from object to source
while debugging a library.
If this switch is not specified,
libraries built by the make that 
are components of an executable will be loaded
by the load target for the executable.
.TP
.B \-lddashr
By default,
each time a partially linked object (\c
.B ld -r\c
) file is encountered,
the file is expanded so that each of its component
files is loaded.
If this flag is present on the command line,
partially linked object files are not expanded
but are loaded "whole."
.TP
.B \-stat
Suppresses checking for the existence of files to be loaded.
By default, 
.B clezstart
checks whether files to be loaded
really exist and issues a message in the output makefile
.B Makefile.cline
if they do not exist.
Note that,
unless this switch is specified,
you could get such a message about a missing file
if a file is moved or removed
during the build process.
.TP
.B \-G
Generates a third target named
.I program\c
.B _obj_nodebug,
which corresponds to the
.I program\c
.B _obj
target,
except the
.B nodebug
version is generated with the
.B -G
switch,
excluding debugging information.
.TP
.B \-path
If this switch is specified,
each load target in the output makefile
will contain both a {Code,Object}Center
.B setopt path
and
.B setopt swap_uses_path
command.
The former command sets up {Code,Object}Center's
path option to contain all of the directories that
\fBclezstart\fR encountered in the process of building the
load targets.
This information helps {Code,Object}Center find source files
when its 
.B swap
command is executed.
.TP
.B \-ptr
If this switch is specified,
each load target in the output makefile
will contain a {Code,Object}Center
.B setopt tmpl_instantiate_flg
command,
containing a 
.B -ptr
switch for each directory
\fBclezstart\fR encountered in the process of building the
load targets.
This information helps {Code,Object}Center locate repositories
for C++ templates.
.TP
.B \-force
Overwrite the output makefile,
\fBMakefile.cline\fR,
if it exists.
By default,
\fBclezstart\fR refuses to overwrite an existing \fBMakefile.cline\fR.
.TP
.B \-ccpath \fIpathname\fR
.TP
.B \-ccmacro \fImacroname\fR
Specifies the full pathname of the C compiler used in
the makefile,
and the name of the macro used to specify it.
If your makefile invokes the C compiler via a relative path,
you do not have to set these.
So,
if the makefile has C compile commands of the form:
.sp
.B cc -c file.c
.sp
or of the form:
.sp
.B $(CC) -c file.c
.sp
where the CC macro is defined as path relative,
such as:
.sp
.B CC=gcc
.sp
Then you do 
.I not
need to set these switches.
If the makefile uses a macro to define the C compiler,
and the definition is an absolute path,
such as:
.sp
.B CCOMP=/opt/SUNWspro/bin/cc
.sp
then you need to use these switches to let \fBclezstart\fR
know the pathname of the C compiler
and the name of the macro.
In the last example,
your \fBclezstart\fR invocation would contain the following arguments:
.sp
.B clezstart -ccpath /opt/SUNWspro/bin/cc -ccmacro CCOMP
.sp
Note that \fBclezstart\fR cannot be used if your makefile
invokes the C compiler or any other executable by
absolute path,
and the invocation is not done through a macro.
.TP
.B \-CCpath \fIpathname\fR
.TP
.B \-CCmacro \fImacroname\fR
Specifies the full pathname of the C++ compiler used in
the makefile,
and the name of the macro used to specify it.
If your makefile invokes the C++ compiler via a relative path,
you do not have to set these.
.TP
.B \-ldpath \fIpathname\fR
.TP
.B \-ldmacro \fImacroname\fR
Specifies the full pathname of the linker used in
the makefile,
and the name of the macro used to specify it.
If your makefile invokes the linker via a relative path,
you do not have to set these.
.TP
.B \-arpath \fIpathname\fR
.TP
.B \-armacro \fImacroname\fR
Specifies the full pathname of the archive (library) builder used in
the makefile,
and the name of the macro used to specify it.
If your makefile invokes the archive builder via a relative path,
you do not have to set these.
.TP
.B \-cext \fIextension\fR
Adds a new extension to the list of extensions \fBclezstart\fR
recongnizes as C source files.
More than one
.B -cext
switch can appear on the command line.
The extension should be specified without the dot (.) character.
For example,
to get \fBclezstart\fR to recognize the strings
.B a.csource
and
.B b.c_source
as C source files,
you could use an invocation of the form:
.sp
.B clezstart -cext csource -cext c_source
.sp
By default,
\fBclezstart\fR recognizes the filenames containing
the following extensions as C source files:
.sp
.B c
.TP
.B \-cxxext \fIextension\fR
Adds a new extension to the list of extensions \fBclezstart\fR
recognizes as C++ source files.
More than one
.B -cxxext
switch can appear on the command line.
The extension should be specified without the dot (.) character.
For example,
to get \fBclezstart\fR to recognize the strings
.B a.cxxsource
and
.B b.C_source
as C++ source files,
you could use an invocation of the form:
.sp
.B clezstart -cxxext cxxsource -cxxext C_source
.sp
By default,
\fBclezstart\fR recognizes the filenames containing
the following extensions as C++ source files:
.sp
.B c C cc cpp cxx i
.TP
.B \-oext \fIextension\fR
Replaces the filename extension \fBclezstart\fR
uses to recongnize object files.
The extension should be specified without the dot (.) character.
For example,
to get \fBclezstart\fR to recognize the string
.B a.obj
as an object file,
you could use an invocation of the form:
.sp
.B clezstart -oext obj
.sp
By default,
\fBclezstart\fR recognizes filenames containing
the following extensions as object files:
.sp
.B o
.SH USING CLEZSTART
This section is organized as a sequence of things to try.
If the initial prerequisites for using \fBclezstart\fR are met,
the program is almost always successful at generating a
makefile with load targets that can be used to load the
object files and libraries of an executable into
{Code,Object}Center.
This section starts with this operation.
Some folks will not need to read any further.
.sp
.B Prerequisites
.PP
In order to use \fBclezstart\fR,
the following prerequisites must be met:
.TP
\(bu
You must use make to build your program
.TP
\(bu
Your makefile must not use absolute pathnames
to invoke the major program-building executables,
i.e.,
the compiler,
linker,
and library builder,
unless they are invoked through macros.
See the \fB-ccpath\fR switch in the previous section
for more on this.
.sp
.PP
.B Perusing Your Makefile
.PP
Before you start,
you should take a careful look at your makefile(s)
to determine what switches you'll need to set on the
\fBclezstart\fR command line.
The most important thing is to check to see if
the makefile invokes the compiler,
linker,
or archiver by absolute pathname.
If it does (and the majority of makefiles do),
you'll have to set the appropriate path and macro
switches on the \fBclezstart\fR command line.
It is a good idea to write down the names of the
macros and the pathnames of all such tools used,
for quick reference.
When perusing the makefile(s),
be sure to follow all recursive makes and
the files named in all makefile
.B include
directives.
.sp
.B Generating an Object Load Target
.PP
This is by far the simplest approach to using \fBclezstart\fR.
It involves the following steps.
.TP
.B 1.  Collect pathnames and macro names
This procedure is described above in
.B Perusing Your 
.BR Makefile .
.TP
.B 2.  Make the executable
Do a make of the executable you want to load.
This guarantees that all files are up-to-date,
and it also verifies that the makefile actually works.
If we assume that 
.I program
stands for the name of your executable, 
for example,
typing:
.sp
.B rm
.I program
.sp
followed by:
.sp
.B make
.I program
.sp
should (at the least) link a new executable.
.TP
.B 3. Run \fBclezstart\fR
To generate a \fBMakefile.cline\fR containing a load target,
type:
.sp
.B rm
.I program
.sp
followed by:
.sp
.B clezstart 
.I clezstart_switches program
.sp
This command line performs the same actions as the make above,
but should also generate a \fBMakefile.cline\fR file
containing a load target.
If it does not build the file,
it is likely that you did not specify
all of the full pathnames and macros used for the
executables in the makefile(s).
These should be checked before trying again.
.TP
.B 4. Check the Load Targets
If \fBclezstart\fR successfully generated the \fBMakefile.cline\fR file,
display the file in an editor to see if it
contains a valid {Code,Object}Center load target.
If we assume 
.I program
stands for the name of your executable,
the file should contain a target of the form:
.sp
.I program\c
.B _obj:
.sp
It contains at least one other target as well,
but this can be ignored here.
The actions associated with the target are all 
.B #load
commands for {Code,Object}Center,
each one loading a single object file or library.
A visual check to be sure a \fBload\fR command exists
for each required object file and library would
be a good idea at this time.
.TP
.B 5. Load your Program Into {Code,Object}Center
Once you have sucessfully created a \fBMakefile.cline\fR
file containing a load target,
you can use it to load your program into
{Code,Object}Center as object files
and libraries.
To do this,
you can type:
.sp
.B \-> make -f Makefile.cline 
.I program\c
.B _obj
.sp
from within the {Code,Object}Center workspace,
substituting the actual name of your program for
.IR program .
The appropriate object files are loaded into 
{Code,Object}Center.
What this command does is invoke make to "build"
the load target.
All of the actions associated with the load target
look like comments to make,
but since the invocation is done from within {Code,Object}Center,
they are interpretted as {Code,Object}Center \fBload\fR commands.
These commands load each of the component object files and
libraries into {Code,Object}Center.
Once this is done,
you can run and/or debug the program from {Code,Object}Center.
.PP
Many {Code,Object}Center features can be used on
a program loaded in object form,
including most of the "standard" debugging features
as well as object code instrumentation.
To take full advantage of the power of {Code,Object}Center,
it is generally a good idea to load your program in source form.
Loading an entire program in source, however, may be impractical if the
program is very large,
so most folks load large programs into {Code,Object}Center
as a mixture of object files and source files.
The parts of the program that are already known to work can be
loaded in object form,
while those parts which are actively being debugged can be
loaded in source form. You can use
\fBclezstart\fR to generate load targets which can
help you load your program into {Code,Object}Center either
entirely as source,
or as a mix of object and source files.
.sp
.B Generating Source and Object Load Targets
.PP
To load your program into {Code,Object}Center as source files,
it is convenient to use a source load target generated
by \fBclezstart\fR.
A source load target itself is not usually used
when loading a program in as a mix of object and source files.
The usual method for doing this is to load in all files
in object form,
and then swap those which you are currently debugging from
object to source form. However, 
\fBclezstart\fR can be made to generate some additional information
while it is generating both source and object load targets.
This additional information can 
make it easier to swap in selected source files.
Generating complete object and source load targets involve
the steps listed below.
This process could be a little more complicated,
depending on the complexities of your makefile(s),
but in most cases it is no more difficult than generating
a complete object load target.
.TP
.B 1. Collect pathnames and macro names
This procedure is described above in
.B Perusing Your 
.BR Makefile .
.TP
.B 2. Make the executable from scratch
You need to guarantee that all of the component
parts of the executable are buildable from scratch.
The best way to do this is to delete the executable,
all of its component object files,
and all of its component libraries that are built,
along with their component object files.
Many makefiles follow the convention of providing
a "clean" target which can be used to set up a build
area in a state where everything will be built from scratch.
If such a target exists,
typing a line of the form:
.sp
.B make clean
.sp
should set things up.
Now build the program,
using make in the familiar manner:
.sp
.B make program
.sp
This should cause every component part of
the executable to be rebuilt.
The main reason for performing this rebuild is
to make sure that the makefile actually works.
At this point it is also a good idea to verify that
all components were actually rebuilt.
\fBclezstart\fR gets the information about which files
it needs to include in a load operation from observing
a make in process.
If a file is not rebuilt by the make,
\fBclezstart\fR will not know about it.
.TP
.B 3. Run clezstart
To generate a \fBMakefile.cline\fR containing the load targets,
again, 
you'll have to
.sp
.B make clean
.sp
or manually remove all of the component objects and built
libraries of the executable,
so the run of \fBclezstart\fR will know about
all of the files.
The invocation of \fBclezstart\fR will look familiar:
.sp
.B clezstart
.I clezstart_switches program
.sp
This command line performs the same actions as the make above,
but should also generate a \fBMakefile.cline\fR file
containing the load targets.
.sp
If it does not build the file,
it is likely that you did not specify
all of the full pathnames and macros used for the
executables in the makefile(s).
These should be checked before trying again.
.TP
.B 4. Check the Load Targets
If \fBclezstart\fR successfully generated the \fBMakefile.cline\fR file,
you should display the file in an editor to see if it
contains valid {Code,Object}Center load targets.
If we assume
.I program
stands for the name of your executable,
the file should contain a target of the form:
.sp
.I program\c
.B _obj:
.sp
which should have a \fBload\fR command for each component
object file associated with it.
The \fBMakefile.cline\fR should also contain a target of the form:
.sp
.I program\c
.B _src:
.sp
which should have a \fBload\fR command for each corresponding
source file component of the executable,
as well as a \fBload\fR command for each of the "standard"
libraries specified on its link line.
.SH CLEZSTART_INIT FILE
\fBclezstart\fR provides two methods of parameterizing
its operation.
The command line switches described above can
be used to modify the behavior of \fBclezstart\fR on a
per-invocation basis.
For more permanent parameterization,
\fBclezstart\fR provides an init file which contains
.B clezstart 
definitions that can be edited.
Each time \fBclezstart\fR runs,
the first thing it does is to check to see
if a file named
.B clezstart_init
exists in the
current directory.
If it does,
the file is read for initialization arguments.
If the file is not found in the current directory,
the \fBclezstart\fR distribution directory
/usr/local/CenterLine/\c
.I arch-os\c
/EZ is checked.
(The letters
.I arch-os
designate the CPU and operating system of your particular platform,
such as sparc-solaris2, sparc-sunos4, or pa-hpux8.) 
Any switches given on the command line override those
in the
.B clezstart_init
file.
.PP
The init file is a Bourne shell sh(1) source file,
so read its modification instructions carefully
before modifying it.
The best approach is to copy the file in the
\fBclezstart\fR distribution directory into the current directory,
and then to make modifications to the local file.
.PP
The following chart shows the relationship between
the command line switches and the variables in the
.B clezstart_init
file:
.TP
.B -relative
cl_ez_path=r
.TP
.B -libobjs
cl_ez_ar=a
.TP
.B -lddashr
cl_ez_ld_dash_r
.TP
.B -stat
cl_ez_fstat=s
.TP
.B -G
cl_nodebug_target=yes
.TP
.B -path
cl_set_path=yes
.TP
.B -ptr
cl_set_ptr=yes
.TP
.B -ccpath
cl_ezcc
.TP
.B -ccmacro
cl_ezcc_macro
.TP
.B -CCpath
cl_ezCC
.TP
.B -CCmacro
cl_ezCC_macro
.TP
.B -ldpath
cl_ezld
.TP
.B -ldmacro
cl_ezld_macro
.TP
.B -arpath
cl_ezar
.TP
.B -armacro
cl_ezar_macro
.TP
.B -cext
cl_ez_csource_ext_additions
.TP
.B -cxxext
cl_ez_cxxsource_ext_additions
.TP
.B -oext
cl_ez_obj_ext
.SH MESSAGES
.PP
After you run
.B clezstart\c
,
you should examine the
.B Makefile.cline
to see if there's a message section with messages placed as comments.
These messages indicate possible problems that may be encountered when using
.BR Makefile.cline .
Here's a list of EZSTART messages and their meanings.
.TP
# In target \fItarget\fR: non-existent file \-\- \fIfile\fR
A file (\c
.I file) 
was used during the build that does not exist after the build
has completed.
The most common reason for this message is that one of the actions
of the make deleted the file.
.TP
# In target \fItarget\fR: Unable to determine source(s) for \fIfile\fR
An object file was used in the build but no source file could 
be determined.
You get this message if you do not do a complete
.B make clean
before you load the object files,
or if the source for the object file is an
assembler source
.RB ( \.s
or
.B \.S
suffix) file.
.TP
# The following command was captured, but no action taken: #\fIcommand\fI
EZSTART encountered a command that it cannot handle.
.TP
# In target \fItarget\fR (_obj & _src): Load not done, unknown file type for \fIfile\fR # \fIcommand\fR
EZSTART could not determine the type for
.IR command ,
which uses
.I file
as an input, and did not generate a load in either the 
.B _obj
or the
.B _src
target.
If the file named is known to be a C or C++ source file,
the most likely cause of this complaint is that \fBclezstart\fR
does not recognize its filename extension.
You can add extensions to the list that \fBclezstart\fR recognizes,
as described 
in 
.B SWITCHES
and 
.B CLEZSTART_INIT FILE
above.
If this does not turn out to be the problem,
review the command to see if it represents a valid load
and insert the 
.B load
commands manually into the \fBMakefile.cline\fR file if necessary.
.TP
# In target \fItarget\fR (_obj & _src): No action taken on the following command: #\fIcommand\fR
Although EZSTART captured and identified \fIcommand\fR
for the specified target, EZSTART could not determine what action to take.
This usually only happens for custom scripts that \fBclezstart\fR
doesn't know about.
If the command is used to generate a C or C++ source file,
this warning can be ignored as {Code,Object}Center
can only load C and C++ source files,
object files,
and libraries anyway.
If your makefile includes commands used to preprocess
source files this message could be generated.
In this case,
it indicates a real problem for \fBclezstart\fR.
.TP
# Encountered both -Bstatic and -Bdynamic linking
Both
.B -Bstatic
and
.B -Bdynamic
switches were specified in compilation or link commands.
Check to make sure that
they are properly used in
.BR Makefile.cline .
See the section below called
.B Using both -Bstatic and 
.BR -Bdynamic .
.TP
# \fIfile\fR was created multiple times \-\- check usage!
EZSTART created \fIfile\fR more than once.
It may be that a source file was compiled
more than one time,
with different settings or
.B ar
was used many times to update a library. Look for occurrences of \fIfile\fR in
.B Makefile.cline
to verify that they are correct.
This almost always indicates a problem that \fBclezstart\fR
cannot deal with.
In almost all cases the makefile can be made more structured
to avoid these messages.
This structuring not only makes \fBclezstart\fR more useful,
but it is likely to eliminate potential makefile bugs as well.
.SH FILENAME SUFFIXES
The
.B clezstart
utility interprets filename suffixes as follows:
.TP
.B \.c
C or C++ source
.TP
.B \.cc
C or C++ source
.TP
.B \.C
C or C++ source
.TP
.B \.cxx
C or C++ source
.TP
.B \.cpp
C or C++ source
.TP
.B \.i
C or C++ source
.TP
.B \.o
Object file
.TP
.B \.a
library
.TP
.B \.so
library
.TP
.B \.so\.*
library
.TP
.B \.s
Assembler source
.TP
.B \.S
Assembler source
.TP
all others
Executable (or unknown)
.SH SCENARIOS
.PP
In this section we describe the following uses of
.B clezstart:
.TP
\(bu
Using
.B clezstart
with non-standard tools
.TP
\(bu
Using absolute pathnames
.TP
\(bu
Building libraries
.TP
\(bu
Using different make systems
.TP
\(bu
Using recursive makes
.TP
\(bu
Adding commands
.SS If you use additional tools
.PP
\fBclezstart\fR recognizes the following tools by default:
.BR cc ,
.BR CC ,
.BR gcc ,
.BR acc ,
.BR ld ,
.BR make ,
.BR ar ,
.BR mv ,
and
.BR cp .
If you use other tools, and if you do not invoke them by absolute pathname, you
must use a few additional techniques to be successful with \fBclezstart\fR.
.PP
Suppose you use a compiler other than
.BR cc ,
.BR CC ,
.BR gcc ,
or
.BR acc .
You'll need to create a link in the
.B EZ/tools
directory for that compiler. Alternatively, you can create the link in the
directory from which you plan to invoke
.BR clezstart .
.PP
For example, if the C compiler you use is called
.BR xcc ,
do the following:
.TP
\(bu
. B cd
to the following directory:
.br 2
.B /usr/local/CenterLine/\c
.I arch-os\c
/EZ
.br 2
where
.I arch-os
designates the CPU and operating system of your particular platform,
such as sparc-solaris2, sparc-sunos4, or pa-hpux8.
.TP
\(bu
Create the symbolic link:
.B ln \-s \./ezcc xcc
.TP
\(bu
Run \fBclezstart\fR as described above.
.PP
When you use non-standard tools, create the symbolic link as follows:
.TP
C compiler
Gets linked to
.BR ezcc ,
for example:
.br 2
.B ln -s ezcc xcc
.TP
C++ compiler
Gets linked to
.BR ezCC ,
for example:
.br 2
.B ln -s ezCC xCC
.TP
linker
Gets linked to
.BR ezld ,
for example:
.br 2
.B ln -s ezld xld
.TP
archiver
Gets linked to 
.BR ezar ,
for example:
.br 2
.B ln -s ezar xar
.SS Using absolute pathnames for tools
.PP
If you invoke tools by specifying an absolute pathname, you must also do the 
following in order for \fBclezstart\fR to work correctly:
.TP
\(bu
Use macros to represent the tool in your makefiles.
.TP
\(bu
Be consistent with the macro names. For instance, you cannot use
.B CC
for your C compiler in one makefile and
.B MYCC
in another makefile used during the same build.
.TP
\(bu
Use only one of each type of tool during the build. For instance, do not use
.B /bin/cc
and
.B /usr/local/bin/gcc
during the same build.
.TP
\(bu
Use the
.B -ccpath,
.B -ccmacro,
.B -CCpath,
.B -CCmacro,
.B -ldpath,
.B -ldmacro,
.B -arpath,
and
.B -armacro
command line switches to indicate the correct names
for the tools.
You can also edit the file
.B clezstart_init
to indicate the correct names for the tools. For example, if you invoke your C
compiler as
.B /usr/local/gnu/bin/gcc
and do this through the macro
.BR CC ,
edit
.B clezstart_init
in the following way. If your previous version of
.B clezstart_init
had
.BR cl_ezcc= ,
then the revised version of
.B clezstart_init
should have
.BR cl_ezcc=/usr/local/gnu/bin/gcc .
If your previous version of
.B clezstart_init
had
.BR cl_ezcc_macro= ,
then the revised version of
.B clezstart_init
should have
.BR cl_ezcc_macro=CC .
.br
.br
After you edit
.BR clezstart_init ,
you can execute
.B clezstart
as in the previous scenarios.
.TP
\(bu
Make sure that you use command line switches or edit lines in
.B clezstart_init
for all of the commands you invoke by absolute path.
.SS Making libraries
.PP
Often libraries, or archives, are created as part of a build. The default behavior
for \fBclezstart\fR is to load the library when it is encountered by name in a command line\-\-\c
that is, as 
.BR myLib.a ,
not
.BR \-lmyLib .
.PP
If, however, you are debugging the libraries themselves,
you probably want to have the
individual component files of the library loaded instead of the library itself.
This allows you to swap individual modules from
object to source and back while you're
debugging.
.PP
If you are sure that all archive commands which are executed during your build
create libraries (not just add to them), you may want to edit
.B clezstart_init
to cause the component files from the libraries to be loaded
into the {Code,Object}Center
environment rather than having the library loaded.
In order to do this,
set the
.B cl_ez_ar
option to 
.BR a :
.RS 5
.B cl_ez_ar=a
.RE
You can also use the
.B -libobjs
command line switch for the same purpose.
.SS Using different make systems
.PP
If you use a program building system other than
.BR make ,
you may still be able to use \fBclezstart\fR.
You need to edit
.BR clezstart_init ,
changing the following line
.RS 5
.B clezstart=
.RE
.PP
to
.RS 5
.B clezstart=\c
.I your make program
.RE
.PP
You must also make a link in the
.B EZ/tools
directory to
.BR ezmake ,
naming the link with the name of your
.B make
program.
.PP
For example, if you use a
.B make
program called
.BR xmake ,
do the following from within the
.B EZ/tools
directory:
.RS 5
.B ln \-s \./ezmake xmake
.RE
.TP
.B Note:
If you have recursive invocations of your
.B make
program, and do it by absolute path, you can not use \fBclezstart\fR.
.SS Using clezstart with recursive makes
.PP
If you have a project that requires many invocations of
.B make
on different makefiles,
there are at least two ways you can use \fBclezstart\fR.
.PP
In the first scenario, you can do a
.B make clean
for the whole system, then invoke
.B clezstart
at the top level. This produces one
.B Makefile.cline
that loads all of the modules for the targets. This means that if you build
an object file in one directory from many smaller
.B \.o
files in that directory (using the
.B \-r
switch to
.BR ld ),
the individual
.B \.o
files and corresponding source files are loaded.
This is the most complete and
the most time consuming way of getting
.BR Makefile.cline .
.PP
A second scenario, 
if you are building libraries in many of the subdirectories,
is to go to each of those and do a
.B make
.BR clean ,
then invoke
.B clezstart
from that directory. This creates a
.B Makefile.cline
to load the individual components of the library. Next go to the top level and invoke
.BR clezstart ;
do not do a
.B make clean
in the directories where the libraries were created. Now the libraries are 
loaded as archives, and when you are in {Code,Object}Center and you want to 
debug the files
from the individual libraries, you can:
.TP
\(bu
Unload the library
.TP
\(bu
.B
cd
to the directory from which the library is made
.TP
\(bu
Enter the following:
.br
.br
.B \-> make -f Makefile.cline \fIlibrary\fI \fItarget\fI
.br
.br
to load the individual components into the environment.
.SS Getting additional information placed in Makefile.cline
.PP
If you use commands such as
.B yacc
or
.B lex
in your builds,
you may be able to capture the command line and have it placed in
.B Makefile.cline
as a comment in the messages area.
.PP
To do this,
you must invoke the tool by name without using an absolute path \-\-
for instance,
.B yacc
and not
.BR /bin/yacc .
To cause the command line from the tool to be captured:
.TP
\(bu
.B cd
to the EZ/tools directory
.TP
\(bu
.B \- ln \-s \./cl_eztool
.I toolname
.PP
For each tool or command linked in this way, a message is placed in
.B Makefile.cline
every time it is invoked,
along with the text of the command line. You can then edit
.B Makefile.cline
to supply additional information.
.SH RESTRICTIONS
This section lists the known limitations of
.BR clezstart .
.SS Changing search path during build
.PP
If the search path is changed during the build process,
\fBclezstart\fR may not work properly.
This is because the scripts that are called reset the path
to the value it had before
.B clezstart
was invoked and then call the real tool.
.SS Some switches may not translate correctly
.PP
If you use a tool other than one of the default tools
recognized by \fBclezstart\fR that has
different switches on its command line,
\fBclezstart\fR may not translate the command properly.
.PP
In most cases,
\fBclezstart\fR ignores switches that it does not know about.
However,
if the
switch takes a value that is separated from the switch by spaces,
\fBclezstart\fR will
probably interpret the value as an input file to the operation.
As long as the
value does not have the form of a valid input file,
such as
.B foo.c
or
.BR bar.o ,
\fBclezstart\fR does not generate a
.B load
command and emits a message at the end of
.B Makefile.cline
informing you of the situation.
.SS Some ar switches not recognized
.PP
It is possible that some switches
are not recognized correctly by \fBclezstart\fR,
depending on how you use
.BR ar ,
which is the UNIX command for maintaining groups of files
into a single archive file.
\fBclezstart\fR tries to recognize when an archive is being updated or created.
If you
extract a file from an archive to be used in your build,
the command is not recognized.
.SS Missing object files
.PP
If you produce an executable file by using a C compiler
and include source files
on the command line,
\fBclezstart\fR attempts to load object
.RB ( \.o )
files for the
.B _obj
target.
Most compilers remove these files automatically in this case. For example,
if you have the following generated by your make:
.RS 5
.B cc \-o test test1\.c test2\.c \-lm
.RE
.PP
.B Makefile.cline
has the following for the target
.BR test_obj :
.RS 5
test_obj:
.br 2
#load test1.o
.br
#load test2.o
.br
#load -lm
#setopt program_name test
.RE
.PP
.B Makefile.cline
also has two messages indicating that
.B test1.o
and
.B test2.o
do not exist, unless you have suppressed these messages
with the
.B cl_ez_fstat
option.
.PP
In this case, when you try to make the
.B test_obj
target from within {Code,Object}Center, it fails.
You can fix this problem by changing the way
the executable is produced.
Use the following two commands:
.RS 5
.B cc \-c test1\.c test2\.c
.br
.B ld \-o test test1\.o test2\.o \-lm
.SS Requires Bourne shell
.PP
\fBclezstart\fR requires
.B /bin/sh
to be the Bourne shell.
If you have changed it to be some other shell, \fBclezstart\fR will
probably not work.
If you have the Bourne shell in some other location, you can get
\fBclezstart\fR to work by editing all of the shell scripts
in \fBclezstart\fR and changing the
first line of each to invoke the Bourne shell.
.SS Using both -Bstatic and -Bdynamic
.PP
If you use both 
.B \-Bstatic
and
.B \-Bdynamic
on a command line to produce an executable file, {Code,Object}Center
uses the last one specified.
This might not be what you want,
and it is different from the behavior of the
.B ld
command.
.PP
For example, if you have the following:
.RS 5
.B cc \-o prog prog\.o \-Bstatic \-llib1 \-Bdynamic \-llib2
.RE
.PP
{Code,Object}Center generates the following load lines for the libraries:
.RS 5
#load \-Bstatic \-Bdynamic \-llib1
.br
#load \-Bstatic \-Bdynamic \-llib2
.RE
.PP
You might need to edit the
.B Makefile.cline
to correct this to the following,
if it's what you intended:
.RS 5
#load \-Bstatic \-llib1
.br
#load \-Bdynamic \-llib2
.RE
.PP
If you use both the 
.B \-Bstatic
and
.B \-Bdynamic
switches during the build,
a message is placed in the message file indicating that
you should check the uses.
There are cases where the results are not correct\-\-\c
for instance, the same library name is used in two different links, one static
and one dynamic.
.SS Creating a file more than once
.PP
If you create a file more than once during the build (perhaps with different
switch settings for a compilation), only the first one is captured.
.SS Updating a library more than once
.PP
If you update an archive more than one time during a build, the
results will not be correct. For
.B clezstart
to work properly 
in this case, do all updating of the archive with one command.
.SH SEE ALSO
.B codecenter(1)
or
.B objectcenter(1)
.br
.I CodeCenter Reference Manual
or
.I ObjectCenter Reference Manual
.SH AUTHOR
CenterLine Software, Inc.

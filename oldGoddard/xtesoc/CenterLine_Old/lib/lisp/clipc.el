;; $Id: clipc.el,v 1.50 1994/07/19 18:17:38 sudduth Exp $
;; emacs lisp interface to clipc and the message server
;;

(defconst centerline-emacs19
  (eq 0 (string-match "^19\\." emacs-version))
  "t if running Emacs 19, nil otherwise.")

(require 'clipc-core)
(if centerline-emacs19
    (require 'gud-centerline))

(defun cl-next () "" (interactive) (send-clipc-cmd "next"))
(defun cl-run () ""(interactive) (send-clipc-cmd "run"))
(defun cl-build () "" (interactive)(send-clipc-cmd "build"))

(defun cl-setup ()
  "For CenterLine, set up handlers and listeners for clipc requests and
notifications, respectively."
  (interactive)
  (setq clipc-handler-callback-list nil)
  (setq clipc-listener-callback-list nil)
  (add-clipc-handler "cx_edit_location"  'cl-edit-location-callback)
  (add-clipc-handler "show_file_lines"   'show-file-lines-callback)
  (add-clipc-listener "event_chdir"      'cl-event-chdir-callback)
  (add-clipc-listener "event_status"     'cl-event-status-callback)
  (add-clipc-listener "cxcmd_whatis"     'cl-whatis-callback)
  (add-clipc-listener "cxcmd_print"      'cl-print-callback)
  (add-clipc-listener "cx_list_location" 'cx-list-callback)
  (add-clipc-listener "mgr_list"         'cx-list-callback)
  (add-clipc-listener "compile_error"    'compile-error-callback)
  (add-clipc-listener "cxcmd_xref"       'cxcmd-xref-callback)
  (add-clipc-listener "event_stop_loc"   'stop-loc-callback)
  (add-clipc-listener "event_reset"      'event-reset-callback)
  (add-clipc-listener "event_run_end"    'event-run-end-callback))

(defun cl-event-chdir-callback (msg-name msg)
  "Listener for chdir messages from clms."
  (let ((directory  (clipc-msg-getval msg "directory")))
    (save-excursion
      (set-buffer cl-current-workspace-buffer)
      (cd directory))))

;;; added cx-list-callback to take care of the clipc message
;;; cx_list_location.
;;; --Sridhar, 03/10/94

(defun cx-list-callback (msg-name msg)
  (let* ((path (clipc-msg-getval msg "path"))
	 (file (clipc-dict-getval path "visiblePath"))
	 (line (clipc-msg-getval msg "linenum")))
    (or (string= file "workspace") (cx-display-line file line))))

;; ----------------------------------------------------------------------
;; This section is for cl-print-callback.
;; ----------------------------------------------------------------------

;;; The following comes through clms after a cxcmd_print is sent from the ui for
;;; a structure.

;;;("cxcmd_print"
;;;  (
;;;    ("flags" 80)
;;;    ("msg_id" 849)
;;;    ("mgr_name" "MGR11-7")
;;;    ("ref_id" 847)
;;;    ("type" 1))
;;;  (
;;;    ("output"
;;;      (
;;;        ("exprType"
;;;          (
;;;            ("typeInfo"
;;;              (
;;;                ("structType"
;;;                  (
;;;                    ("structFields"
;;;                      (
;;;                        (
;;;                          ("offsetOrAddress" 0)
;;;                          ("fieldClass" 0)
;;;                          ("bitSize" 0)
;;;                          ("fieldType"
;;;                            (
;;;                              ("typeInfo"
;;;                                (
;;;                                  ("simpleType"
;;;                                    (
;;;                                      ("typeName" "int")))
;;;                                  ("typeInfoKey" "simpleType")))
;;;                              ("typeAfter" "")
;;;                              ("typeBefore" "int")
;;;                              ("typeSize" 4)
;;;                              ("typeNumber" 2)))
;;;                          ("memberName"
;;;                            (
;;;                              ("internalName" "a__1A")
;;;                              ("visibleName" "A::a"))))))
;;;                    ("structTag" "A")))
;;;                ("typeInfoKey" "structType")))
;;;            ("typeAfter" "")
;;;            ("typeBefore" "struct A")
;;;            ("typeSize" 4)
;;;            ("typeNumber" 120)))
;;;        ("exprName"
;;;          (
;;;            ("simpleName" "*a_ptr")))))))

(defun cl-print-callback (msg-name msg)
  "Listener for print messages from clms."
  (let ((expr  (clipc-msg-getval msg "expr"))
        string-to-display)
    (if (eq nil expr)
        ;; must be structure.
        (let* ((output (clipc-msg-getval msg "output"))
               (expr-name (clipc-dict-getval output "exprName"))
               (string-to-display (concat "Print " expr-name "\n"))
               (expr-type (clipc-dict-getval output "exprType"))
               (type-info (clipc-dict-getval expr-type "typeInfo"))
               (type-info-key (clipc-dict-getval type-info "typeInfoKey")))
          (if (equal "structType" type-info-key)
              (let* ((struct-type (clipc-dict-getval type-info "structType"))
                     (struct-fields (clipc-dict-getval struct-type
                                                       "structFields")))
                )))
      ;; not a structure.
      (let ((rvalue-string  (clipc-msg-getval msg "rvalueString")))
        (setq string-to-display (concat "Print " expr "\n"))
        (setq string-to-display (concat string-to-display rvalue-string))))
    ;; pop-up a buffer to display info.
    (cl-temp-display string-to-display " *ObjectCenter Print*")))

;; ----------------------------------------------------------------------
;; This section is for cl-whatis-callback.
;; ----------------------------------------------------------------------

;; This section is based on the idea that a cxcmd-whatis notification looks like
;; the following:

;; For a structure:

;; (M_DCT cxcmd_whatis
;;   (body
;;     (T_DCT
;;       (symbol
;;         (T_DCT
;;           (visibleName (T_STR 1 A))
;;           (internalName (T_STR 1 A))))
;;       (useList
;;         (T_ARR
;;           (T_DCT
;;             (symbol
;;               (T_DCT
;;                 (visibleName (T_STR 1 A))
;;                 (internalName (T_STR 1 A))))
;;             (symbolType
;;               (T_DCT
;;                 (typeNumber (T_INT 124))
;;                 (typeSize (T_INT 4))
;;                 (typeBefore (T_STR 8 struct A))
;;                 (typeAfter (T_STR 0 ))
;;                 (typeInfo
;;                   (T_DCT
;;                     (typeInfoKey (T_STR 10 structType))
;;                     (structType
;;                       (T_DCT
;;                         (structTag (T_STR 1 A))
;;                         (structFields
;;                           (T_ARR
;;                             (T_DCT
;;                               (memberName
;;                                 (T_DCT
;;                                   (visibleName (T_STR 4 A::a))
;;                                   (internalName (T_STR 5 a__1A))))
;;                               (fieldType
;;                                 (T_DCT
;;                                   (typeNumber (T_INT 2))
;;                                   (typeSize (T_INT 4))
;;                                   (typeBefore (T_STR 3 int))
;;                                   (typeAfter (T_STR 0 ))
;;                                   (typeInfo
;;                                     (T_DCT
;;                                       (typeInfoKey (T_STR 10 simpleType))
;;                                       (simpleType
;;                                         (T_DCT
;;                                           (typeName (T_STR 3 int))))))))
;;                               (bitSize (T_INT 0))
;;                               (fieldClass (T_INT 0))
;;                               (offsetOrAddress (T_INT 0))))))))))))))
;;       (status (T_INT 0)))))

;; For pointer:

;;  (body
;;    (T_DCT
;;      (symbol
;;        (T_DCT
;;          (visibleName (T_STR 1 a))
;;          (internalName (T_STR 1 a))))
;;      (useList
;;        (T_ARR
;;          (T_DCT
;;            (symbol
;;              (T_DCT
;;                (visibleName (T_STR 1 a))
;;                (internalName (T_STR 4 __1a))))
;;            (symbolStorage (T_STR 4 auto))
;;            (symbolType
;;              (T_DCT
;;                (typeNumber (T_INT 125))
;;                (typeSize (T_INT 4))
;;                (typeBefore (T_STR 10 struct A *))
;;                (typeAfter (T_STR 0 ))
;;                (typeInfo
;;                  (T_DCT
;;                    (typeInfoKey (T_STR 11 pointerType))
;;                    (pointerType
;;                      (T_DCT
;;                        (pointedToType
;;                          (T_DCT
;;                            (typeNumber (T_INT 124))
;;                            (typeSize (T_INT 4))
;;                            (typeBefore (T_STR 8 struct A))
;;                            (typeAfter (T_STR 0 )))))))))))))
;;      (status (T_INT 0)))))

(defun cl-whatis-callback (msg-name msg)
  "Listener for whatis messages from clms."
  (let* ((symbol  (clipc-msg-getval msg "symbol"))
         (name (clipc-dict-getval symbol "visibleName"))
         ;; for useList, take the car since it is a T_ARR and we are interested
         ;; only in the first element.
         (use-list (car (clipc-msg-getval msg "useList")))
         (error-msg (clipc-msg-getval msg "errorMsg"))
         (symbol-storage (clipc-dict-getval use-list "symbolStorage"))
         (symbol-type (clipc-dict-getval use-list "symbolType"))
         (type-before (clipc-dict-getval symbol-type "typeBefore"))
         (type-after (clipc-dict-getval symbol-type "typeAfter"))
         (type-info (clipc-dict-getval symbol-type "typeInfo"))
         (type-info-key (clipc-dict-getval type-info "typeInfoKey"))
         (string-to-display (concat "Whatis " name "\n"))
         (is-keyword (clipc-dict-getval use-list "isKeyword"))
         (symbol-enum-val (clipc-dict-getval use-list "symbolEnumVal"))
         (symbol-def (clipc-dict-getval use-list "symbolDef")))
    (cond (error-msg
           (setq string-to-display
                 (concat string-to-display error-msg)))
          (symbol-def
           ;; symbol - function or macro.
           (setq string-to-display
                 (concat string-to-display symbol-def)))
          (symbol-enum-val
           ;; enum value.
           (setq string-to-display
                 (concat string-to-display
                         "Enumerator value: < " symbol-enum-val " >")))
          (is-keyword
           (cond ((= 10 is-keyword)
                  ;; language keyword.
                  (setq string-to-display
                        (concat string-to-display "Language Keyword")))
                 ((= 9 is-keyword)
                  ;; preprocessor keyword.
                  (setq string-to-display
                        (concat string-to-display "Preprocessor Keyword")))))
          (t
           (if (and symbol-storage (not (equal "" symbol-storage)))
               (setq symbol-storage (concat symbol-storage " ")))
           (if (not (equal "" type-before))
               (setq type-before (concat type-before " ")))
           (if (not (equal "" type-after))
               (setq type-after (concat " " type-after)))
           (setq string-to-display
                 (concat string-to-display symbol-storage
                         type-before name type-after))
           (cond ((equal "pointerType" type-info-key))
                 ((equal "structType" type-info-key)
                  (let* ((struct-type (clipc-dict-getval type-info "structType"))
                         (struct-fields (clipc-dict-getval struct-type
                                                           "structFields"))
                         (temp-fields  struct-fields)
                         field member-name field-type)
                    (setq string-to-display
                          (concat string-to-display "\n{\n"))
                    (while temp-fields
                      (setq field (car temp-fields))
                      (setq temp-fields (cdr temp-fields))
                      (setq member-name (clipc-dict-getval field "memberName"))
                      (setq name (clipc-dict-getval member-name "visibleName"))
                      (setq field-type (clipc-dict-getval field "fieldType"))
                      (setq type-before (clipc-dict-getval field-type
                                                           "typeBefore"))
                      (setq type-after (clipc-dict-getval field-type
                                                          "typeAfter"))
                      (if (not (equal "" type-before))
                          (setq type-before (concat type-before " ")))
                      (if (not (equal "" type-after))
                          (setq type-after (concat " " type-after)))
                      (setq string-to-display
                            (concat string-to-display " "
                                    type-before name type-after "\n")))
                    (setq string-to-display
                          (concat string-to-display "};"))
                    )))))
    (cl-temp-display string-to-display " *ObjectCenter Whatis*")))

(defvar cl-temp-string ""
  "String to display in temp buffer for objectcenter.")

(defun cl-insert-temp-string ()
  (insert cl-temp-string)
  (save-excursion
   (goto-char (point-min))
   (end-of-line)
   (let ((o1 (make-overlay (point-min) (point))))
     (overlay-put o1 'face 'underline))
   )
  (goto-char (point-min))
  nil)  ;; return nil so that pop-up window is shrunk to fit buffer.

(defun cl-temp-display-hook ()
  (shrink-window-if-larger-than-buffer (selected-window))
  (local-set-key [?\C- ] 'set-mark-command)
  (local-set-key [?\M-<] 'beginning-of-buffer)
  (local-set-key [?\M->] 'end-of-buffer)
  (local-set-key "\M-w"  'kill-ring-save)
  (local-set-key "\C-b"  'backward-char)
  (local-set-key "\M-f"  'forward-word)
  (local-set-key "\M-b"  'backward-word)
  (local-set-key "\C-f"  'forward-char)
  (local-set-key "\C-a"  'beginning-of-line)
  (local-set-key "\C-e"  'end-of-line)
  (local-set-key "\M-a"  'backward-sentence)
  (local-set-key "\M-e"  'forward-sentence)
  (local-set-key "\C-n"  'next-line)
  (local-set-key "\C-p"  'previous-line)
  (local-set-key "\C-v"  'scroll-up)
  (local-set-key "\M-v"  'scroll-down))

;; Let's add the function above to electric-help-mode-hook.
;; That will affect both cl-temp-display and electric-help.

(if centerline-emacs19
    (add-hook 'electric-help-mode-hook 'cl-temp-display-hook))

(defun cl-with-electric-help (thunk &optional buffer window noerase)
  "For CenterLine, run a modified version of with-electric-help (running THUNK
as in with-electric-help) that brings up BUFFER in a sub-window of WINDOW."
  (let ((one (one-window-p t))
	(config (current-window-configuration))
        (bury nil))
    (unwind-protect
         (save-excursion
           ;; Split window and use the lower one - which is returned by
           ;; split-window to display buffer.
           (setq window (split-window window))
           (set-window-buffer window buffer)
           (select-window window)
           (save-excursion
             (set-buffer buffer)
             (electric-help-mode)
             (setq buffer-read-only nil)
             (or noerase (erase-buffer)))
           (let ((standard-output buffer))
             (if (not (funcall thunk))
                 (progn
                   (set-buffer buffer)
                   (set-buffer-modified-p nil)
                   (goto-char (point-min))
                   (if one (shrink-window-if-larger-than-buffer
                            (selected-window))))))
           (set-buffer buffer)
           (run-hooks 'electric-help-mode-hook)
           (if (eq (car-safe (electric-help-command-loop))
                   'retain)
               (setq config (current-window-configuration))
               (setq bury t)))
      (message "")
      (set-buffer buffer)
      (setq buffer-read-only nil)
      (condition-case ()
          (funcall (or default-major-mode 'fundamental-mode))
        (error nil))
      (set-window-configuration config)
      (if bury
          (progn
            ;;>> Perhaps this shouldn't be done.
            ;; so that when we say "Press space to bury" we mean it
            (replace-buffer-in-windows buffer)
            ;; must do this outside of save-window-excursion
            (bury-buffer buffer))))))

(defun cl-temp-display (string &optional buffer-name)
  "Display STRING in temporary buffer for perusal with electric-help bindings.
Optional arg BUFFER-NAME determines the name of the buffer."
  (interactive)
  (let (window)
    (save-excursion
      (if (equal nil buffer-name)
          (setq buffer-name " *Temp CenterLine*"))
      (erase-buffer (set-buffer (get-buffer-create buffer-name)))
      (setq cl-temp-string string)
      (setq window (get-buffer-window cl-current-workspace-buffer))
      (if (eq nil window)
          (setq window (selected-window)))
      (cl-with-electric-help 'cl-insert-temp-string (current-buffer)
                             window))))

;; ----------------------------------------------------------------------
;; This section is all for cl-event-status-callback.
;; ----------------------------------------------------------------------

(defvar cl-status-record nil
  "For objectcenter, a record of all breaks and actions.")

;; Here are some functions that get various types of info from an entry in
;; cl-status-record.

(defun cl-status-file (status-item)
  "Given the statusItem LIST of a clipc event_status, return the filename, if
any."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (file-location (clipc-dict-getval item-location "fileLocation"))
         (path (clipc-dict-getval file-location "path"))
         (file (clipc-dict-getval path "visiblePath")))
    file))

(defun cl-status-line (status-item)
  "Given the statusItem LIST of a clipc event_status, return the line number, if
any."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (file-location (clipc-dict-getval item-location "fileLocation"))
         (line (clipc-dict-getval file-location "lineNumber")))
    line))

(defun cl-status-start-addr (status-item)
  "Given the statusItem LIST of a clipc event_status, return the start addr, if
any."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (memory-location (clipc-dict-getval item-location "memoryLocation"))
         (start-addr (clipc-dict-getval memory-location "startAddr")))
    start-addr))

(defun cl-status-end-addr (status-item)
  "Given the statusItem LIST of a clipc event_status, return the end addr, if
any."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (memory-location (clipc-dict-getval item-location "memoryLocation"))
         (end-addr (clipc-dict-getval memory-location "endAddr")))
    end-addr))

(defun cl-status-func (status-item)
  "Given the statusItem LIST of a clipc event_status, return the function, if
any."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (file-location (clipc-dict-getval item-location "fileLocation"))
         (func (clipc-dict-getval file-location "func"))
         (func-name (clipc-dict-getval func "visibleName")))
    func-name))

(defun cl-status-loc-type (status-item)
  "Given the statusItem LIST of a clipc event_status, return the location type."
  (let* ((item-location (clipc-dict-getval status-item "itemLocation"))
         (loc-type (clipc-dict-getval item-location "locationKey")))
    loc-type))

(defun cl-status-type (status-item)
  "Given the statusItem LIST of a clipc event_status, return the type."
  (clipc-dict-getval status-item "itemType"))

(defun cl-status-item-number (status-item)
  "Given the statusItem LIST of a clipc event_status, return the item number."
  (clipc-dict-getval status-item "itemNumber"))

;; cl-last-action-body is a kludge to get around the fact that clipc does not
;; make this information available to us currently.  In contexts where we can
;; capture this information in emacs (like when an action is set using the
;; menus), we keep track of the body in this variable so that when we get an
;; event_status message we can add it in.  This is set in cl-set-action.

(defvar cl-last-action-body nil
  "For objectcenter, body of last action not yet recorded.")

(defun cl-status-record-add (status-item)
  "Add a breakpoint or action to the status record."
  (save-excursion
    ;; We set the buffer to make sure we get the correct buffer-local values for
    ;; cl variables.
    (set-buffer cl-current-workspace-buffer)
    (let ((item-number (cl-status-item-number status-item))
          (type (cl-status-type status-item)))
      (if (equal "action" type)
          ;; See if there is an action body to add.
          ;; If so, add new dictionary item.
          (if cl-last-action-body
              (progn
                (setq status-item
                      (append status-item
                              (list (list "body" cl-last-action-body))))
                (setq cl-last-action-body nil))))
      ;; Find place to store the status-item.
      (let ((temp-record cl-status-record)
            new-record)
        (while
            (and temp-record
                 (< (cl-status-item-number (car temp-record))
                    item-number))
          (setq new-record (append new-record (list (car temp-record))))
          (setq temp-record (cdr temp-record)))
        ;; Add status-item in here.
        (setq cl-status-record (append new-record
                                       (list status-item)
                                       temp-record))))))

(defun cl-status-record-delete (item-number)
  "Delete a breakpoint or action from the status record."
  (save-excursion
    ;; We set the buffer to make sure we get the correct buffer-local values for
    ;; cl variables.
    (set-buffer cl-current-workspace-buffer)
    (let ((temp-record cl-status-record)
          new-record)
      (while
          (and temp-record
               (< (cl-status-item-number (car temp-record))
                  item-number))
        (setq new-record (append new-record (list (car temp-record))))
        (setq temp-record (cdr temp-record)))
      ;; The item should be next one, if any.
      (if (and temp-record (= item-number
             (cl-status-item-number (car temp-record))))
          (setq cl-status-record (append new-record
                                         (cdr temp-record)))))))

;; The next two functions dynamically edit the menu that the "delete" menuitem
;; of the "debug" menu pops up.

(defun cl-remove-from-delete-menuitem (status-item)
  "Given a statusItem LIST indicating an item to remove from the status list,
modify the delete menuitem to reflect the removal."
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    (let* ((item (cl-status-item-number status-item))
           ;; Get the keymap for the delete menuitem.  It defines a menu.
           (delete-keymap (local-key-binding [menu-bar cl-debug delete]))
           (index 0)
           ;; Initialize a new keymap which eventually replace the current one.
           (new-keymap (list (car delete-keymap)))
           ;; Store the list of menuitems in the keymap in another var.
           (temp-keymap (cdr delete-keymap))
           (length temp-keymap)
           key stop)
      ;; search for "item" in the keymap.
      (while temp-keymap
        (setq key (car temp-keymap))
        (if (listp key)
            ;; This is a menuitem.
            (if (not (equal item (car key)))
                ;; This is not "item".
                (progn
                  (if (and (not cl-status-record)
                           (equal 'none (car key)))
                      ;; There are no status items.
                      ;; "key" is the "none" menuitem, which had been set so
                      ;; that it would not show.
                      ;; Add a menuitem to new-keymap that shows this item.
                      (define-key new-keymap [none]
                        '("No Debugging Items" .
                          cl-debug-delete-none))
                    ;; If there are status items and "key" is not the "all"
                    ;; menuitem, add it to the new keymap.
                    (if (or cl-status-record
                            ;; Don't keep binding for "all".
                            (not (equal 'all (car key))))
                        (setq new-keymap (append new-keymap (list key)))))))
          ;; Must be string name of map.  Add this to the new keymap.
          (setq new-keymap (append new-keymap (list key))))
        ;; Increment to the next item.
        (setq temp-keymap (cdr temp-keymap)))
      ;; Set the new local definition for "delete".
      (local-set-key (vector 'menu-bar 'cl-debug 'delete)
                     (cons "Delete..." new-keymap)))))

(defun cl-add-to-delete-menuitem (status-item)
  "Given a new statusItem LIST, modify the delete menuitem to reflect the
additional item."
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    (let ((delete-keymap (local-key-binding [menu-bar cl-debug delete])))
      (if (> 2 (length cl-status-record))
          ;; There are no existing items.  Remove the existing one (for "none")
          ;; and add new one in for deleting all debugging items.
          (progn
            ;; Setting menuitem to nil will not remove item from keymap, but
            ;; will keep it from being displayed.
            (local-set-key [menu-bar cl-debug delete none] nil)
            (local-set-key [menu-bar cl-debug delete all]
                           '("Delete All Debugging Items" .
                             cl-debug-delete-all))))
      ;; Add in the new menuitem for status-item.
      (let ((type (cl-status-type status-item))
            (item (cl-status-item-number status-item))
            (file (cl-status-file status-item))
            (line (cl-status-line status-item))
            (func (cl-status-func status-item))
            menu-text
            menu-binding
            last-menu-item
            key)
        ;; Make the menuitem's visible text.
        (setq menu-text type)
        (if file
            (setq menu-text
                  (concat "(" item ") " menu-text " at " file ": "
                          line " /* " func " */")))
        ;; Create the binding for the new menuitem.
        (setq menu-binding
              (cons menu-text
                    (funcall 'list 'lambda nil '(interactive)
                             (list 'cl-debug-delete-breakpoint item))))
        ;; Get the last menuitem in the delete-keymap.  We need this in order to
        ;; get define-key-after to allow us to place the new menuitem at the end
        ;; of the menu.
        ;; Note: one of the last two elements in the keymap is not a menuitem.
        (setq key (nth (- (length delete-keymap) 2) delete-keymap))
        (if (listp key)
            ;; "key" is the last menuitem.
            (setq last-menu-item (car key))
          ;; "key" does not represent a menuitem.  So the correct element in the
          ;; keymap is the last one.
          (setq last-menu-item
                (car (nth (- (length delete-keymap) 1) delete-keymap))))
        ;; Put the new menuitem after the last one - so it appears at the end of
        ;; the menu.
        (define-key-after delete-keymap
          (vector item) menu-binding (vector last-menu-item))))))

(defun cl-debug-delete-all ()
  "For CenterLine, delete all status items."
  ;; This is used as the callback for menuitem of delete menuitem menu that
  ;; deletes all breakpoints.
  (interactive)
  (let ((temp cl-status-record)
        status-record-copy)
    ;; Make a copy of cl-status-record.
    ;; We do this because the callback for clipc event_status messages modify
    ;; cl-status-record.
    (while temp
      (setq status-record-copy (append status-record-copy (list (car temp))))
      (setq temp (cdr temp)))
    ;; Send a clipc message for each item saying to delete it.
    (while status-record-copy
      (let (status-item)
        (setq status-item (car status-record-copy))
        (cl-debug-delete-breakpoint (cl-status-item-number status-item))
        (setq status-record-copy (cdr status-record-copy))))))

(defun cl-contains-parens (string)
  "Returns t if STRING contains start and end parentheses; nil otherwise."
  (interactive "s")
  ;; Just insert the string in a temporary buffer, search for the start
  ;; parenthese, then search for the end parenthese.
  (let (flag)
    (save-excursion
      (set-buffer (get-buffer-create " *temp-centerline*"))
      (erase-buffer)
      (insert string)
      (goto-char (point-min))
      (if (search-forward "(" nil t)
          (if (search-forward ")" nil t)
              (setq flag t)))
      (kill-buffer nil))
    flag))

;; The next two functions deal with echo'ing to the user a change to a status
;; item (either setting a new one or deleting an existing one).
;; The message is put in the workspace buffer.

(defvar cl-overlay-list nil
  "List of entries, one for each overlay in a visited file, that were created by
CenterLine.  Each entry consists of 2 or more elements - a string indicating the
reason for the overlay, the overlay, and any info specific to the particular
use.  There is a local version of this variable for each CenterLine workspace
buffer.")

;; This creates cl-breakpoint-face, which is the face given to breakpoint lines
;; in CenterLine.  The values given to foreground and background can be changed
;; by the user.

(if (and (fboundp 'make-face)
	 (fboundp 'set-face-foreground)
	 (fboundp 'set-face-background))
    (progn
      (make-face 'cl-breakpoint-face)
      (set-face-foreground 'cl-breakpoint-face "white")
      (set-face-background 'cl-breakpoint-face "black")))

(defun cl-add-to-overlay-list (type overlay line)
  "Add a new entry to the overlay list for the current CenterLine workspace."
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    (setq cl-overlay-list
          (append cl-overlay-list
                  (list (list type overlay line))))))

(defun cl-highlight-breakpoint (line)
  "For CenterLine, highlight breakpoint in current buffer at line NUMBER."
  (save-excursion
    (goto-line line)
    (let ((start-point (point))
          o1)
      (end-of-line)
      (setq o1 (make-overlay start-point (point)))
      (overlay-put o1 'face 'cl-breakpoint-face)
      (cl-add-to-overlay-list "stop" o1 line))))

(defun cl-highlight-breakpoint-everywhere (file line)
  "For CenterLine, highlight breakpoint in all buffers containing FILE at line
NUMBER."
  (let* ((truename (abbreviate-file-name (file-truename file)))
         (number (nthcdr 10 (file-attributes truename))))
    ;; Cycle through buffer-list searching for a match.
    ;; This code was hacked from part of find-file-noselect.
    (let ((list (buffer-list)))
      (while list
        (save-excursion
          (set-buffer (car list))
          (if (or (and buffer-file-name
                       (string= buffer-file-truename truename))
                  (and (equal buffer-file-number number)
                       ;; Verify this buffer's file number
                       ;; still belongs to its file.
                       (file-exists-p buffer-file-name)
                       (equal (nthcdr 10 (file-attributes
                                          buffer-file-name)) number)))
              ;; This matches.
              (cl-highlight-breakpoint line)))
        (setq list (cdr list))))))

(defun cl-highlight-file-breakpoints ()
  "For CenterLine, this is a hook for find-file-noselect.  It highlights any
lines that are breakpoints."
  ;; Cycle through the status-record list.
  (if (and (boundp 'cl-current-workspace-buffer)
	   cl-current-workspace-buffer
	   (memq cl-current-workspace-buffer (buffer-list)))
      (let (temp-list status-item type file line)
	(save-excursion
	  (set-buffer cl-current-workspace-buffer)
	  (setq temp-list cl-status-record))
	(while temp-list
	  (setq status-item (car temp-list))
	  (setq temp-list (cdr temp-list))
	  (setq type (cl-status-type status-item))
	  (setq file (cl-status-file status-item))
	  (setq line (cl-status-line status-item))
	  (if (equal "stop" type)
	      (if (equal (abbreviate-file-name (file-truename file))
			 buffer-file-truename)
		  (cl-highlight-breakpoint line)))))))

(if centerline-emacs19
    (add-hook 'find-file-hooks 'cl-highlight-file-breakpoints t))

(defun cl-echo-set-status-to-user (status-item)
  "For CenterLine, give user feedback about status item."
  (let ((item-number (cl-status-item-number status-item))
        (file (cl-status-file status-item))
        (line (cl-status-line status-item))
        (func (cl-status-func status-item))
        (type (cl-status-type status-item))
        at-point-max window msg)
    ;; Make a message.  This code should work when the item is a stop or action
    ;; at a particular line of a file.  Otherwise, not much info will be
    ;; given to the user.
    (setq msg (concat type " (" item-number ") set"))
    (if file
        (progn
          (setq msg (concat msg " at \"" file "\":" line))
          (if func
              (progn
                (setq msg (concat msg ", " func))
                (if (cl-contains-parens func)
                    (setq msg (concat msg "."))
                  (setq msg (concat msg "().")))))))
    ;; Insert the message in the mini-buffer.
    (message nil msg)
    ;; If this is a breakpoint at a particular line, highlight the line.
    (if (and file line (equal type "stop"))
        (cl-highlight-breakpoint-everywhere file line))))

(defun cl-dehighlight-breakpoint-everywhere (file line)
  "For CenterLine, dehighlight breakpoint in FILE at LINE."
  (let (temp-list item new-list)
    (save-excursion
      (set-buffer cl-current-workspace-buffer)
      (setq temp-list cl-overlay-list))
    (while temp-list
      (setq item (car temp-list))
      (setq temp-list (cdr temp-list))
      (if (and (equal "stop" (nth 0 item))
               (equal line (nth 2 item))
               (overlayp (nth 1 item))
               (bufferp (overlay-buffer (nth 1 item)))
               (file-exists-p (buffer-file-name (overlay-buffer (nth 1 item))))
               (equal (abbreviate-file-name (file-truename
                                             (buffer-file-name (overlay-buffer
                                                                (nth 1 item)))))
                     (abbreviate-file-name (file-truename file))))
          (delete-overlay (nth 1 item))
        (setq new-list (append new-list (list item)))))))

(defun cl-dehighlight-all-breakpoints ()
  "For CenterLine, dehighlight all breakpoints."
  (let (temp-list item new-list)
    (save-excursion
      (set-buffer cl-current-workspace-buffer)
      (setq temp-list cl-overlay-list))
    (while temp-list
      (setq item (car temp-list))
      (setq temp-list (cdr temp-list))
      (if (overlayp (nth 1 item))
          (delete-overlay (nth 1 item))))))

(defun cl-echo-delete-status-to-user (status-item item-number)
  "For CenterLine, give user feedback about status item."
  (let ((file (cl-status-file status-item))
        (line (cl-status-line status-item))
        (type (cl-status-type status-item))
        at-point-max window msg)
    ;; Make a message.
    (setq msg (concat type " (" item-number ") deleted."))
    ;; Insert the message in the mini-buffer.
    (message nil msg)
    ;; If this is a breakpoint at a particular line, dehighlight the line.
    (if (and file line (equal type "stop"))
        (cl-dehighlight-breakpoint-everywhere file line))))

(defun cl-event-status-callback (msg-name msg)
  "Listener for status messages from clms - so that emacs can keep track of
breakpoints and actions."
  (let ((operation  (clipc-msg-getval msg "operation"))
        (status-item (clipc-msg-getval msg "statusItem"))
        item-number)
    ;; Make sure we change cl-status-record before calling the functions which
    ;; modify the delete menuitem.
    (cond ((equal "set" operation)
           (cl-status-record-add status-item)
           (cl-add-to-delete-menuitem status-item)
           (cl-echo-set-status-to-user status-item))
          ((equal "delete" operation)
           (setq item-number (cl-status-item-number status-item))
           (let ((item (cl-get-status-record-item item-number)))
             (cl-echo-delete-status-to-user item item-number))
           (cl-status-record-delete item-number)
           (cl-remove-from-delete-menuitem status-item)
           ))))

(defun cl-get-status-record-item (item-number)
  "Return item in cl-status-record with item-number equal to NUMBER."
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    ;; Cycle through cl-status-record until we find item with item-number.
    (let ((temp-list cl-status-record) item found)
      (while temp-list
        (setq item (car temp-list))
        (setq temp-list (cdr temp-list))
        (if (= item-number
               (cl-status-item-number item))
            (progn
              (setq temp-list nil)
              (setq found item))))
      found)))

;; ----------------------------------------------------------------------
;; End of cl-event-status-callback code.
;; ----------------------------------------------------------------------

(defun cl-edit-location-callback (msg-name msg)
  "Listener for edit location messages from clms."
  (let ((filename (clipc-msg-getval msg "filename"))
        (linenum  (clipc-msg-getval msg "linenum"))
	(reply-envlp (make-reply-envlp msg))
	(reply-args nil))
    (setq reply-args (list '("status" 1)
			   (list "linenum" linenum)
			   (list "filename" filename)))
    (clipc-reply-send msg-name reply-args reply-envlp)
    (if (not (or (equal "" filename)
                 (eq nil filename)
                 (equal "" linenum)
                 (eq nil linenum)))
        (if (file-readable-p filename)
            (let ((buffer (find-file-noselect filename)))
              ;; Put this buffer in a window other than the workspace one.
              ;; Keep the workspace below the one for buffer.
              (if (one-window-p t)
                  ;; only one window.
                  (let* ((cur-buffer (window-buffer (selected-window))))
                    (set-window-buffer (selected-window) buffer)
                    (display-buffer cur-buffer))
                ;; more than one window
                (let* ((workspace-window
                        (get-buffer-window cl-current-workspace-buffer))
                       (other-win (next-window workspace-window 1)))
                  (set-window-buffer other-win buffer)
                  (select-window other-win)))
              (goto-line linenum))))))
    
(defun cl-gen-cmd (cmd)
  "prompt for and send a generic CX command"
  (interactive "sCX command: ")
  (send-clipc-cmd cmd))

(defun cl-stop-loc ()
  "Set CX breakpoint at this source line."
  (interactive)
  (let ((file-name buffer-file-name)
	(line (save-restriction
		(widen)
		(1+ (count-lines 1 (point))))))
    (clipc-request-send "cxcmd_stop" (list (list "lineNumber" line)
					   (list "pathName" file-name)))))

(defun cl-display-point ()
  "Set CX display on variable at point"
  (interactive)
  (let ((disp (cl-find-tag-tag)))
    (send-clipc-cmd (format "display %s" disp))))

(defun cl-load-file ()
  "Load the current file into CenterLine"
  (interactive)
  (let ((file-name buffer-file-name))
    (send-clipc-cmd (format "load %s" file-name))))


(defun event-run-end-callback (msg-name msg) 
  "called when CX finished running program"
  (cl-clear-arrow))

(defun event-reset-callback (msg-name msg) 
  "called when CX resets"
  (cl-clear-arrow))

;;
;; error browser hacks
;;

(defun compile-error-callback (msg-name msg)
  (let ((buffer (get-buffer-create "*cx-error*"))
        window)
    (if (one-window-p t)
        ;; only one window.
        (let*
            ((cur-buffer (window-buffer (selected-window))))
          (setq window (set-window-buffer (selected-window) buffer))
          (select-window (display-buffer cur-buffer)))
      (setq window (display-buffer buffer)))
    (let* ((compSrc (clipc-msg-getval msg "compilationSrc"))
           (file (clipc-dict-getval compSrc "visiblePath")) 
           (line (clipc-msg-getval msg "fileLine"))
           (text (clipc-msg-getval msg "msgText"))
           (pos))
      (save-excursion
        (set-buffer buffer )
        (goto-char (point-max))
        (insert (format "%s:%d: %s\n" file line text))))))

(defun stop-loc-callback (msg-name msg)
  (let* ((path (clipc-msg-getval msg "filePath")) 
	 (file (clipc-dict-getval path "visiblePath"))
	 (line (clipc-msg-getval msg "lineNumber"))
	 (func (clipc-msg-getval msg "funcName")))
    (or (string= file "workspace") (cx-display-line file line))))

(defun show-file-lines-callback (msg-name msg)
  (let ((file (clipc-msg-getval msg "fileName")) 
	(line (clipc-msg-getval msg "lineNumber")))
    (or (string= file "workspace") (cx-display-line file line))))

(defun cxcmd-xref-callback (msg-name msg)
  ""
  (let* ((buffer (get-buffer-create "*cx-xref*"))
	 (window (display-buffer buffer nil))
	 (symbol (clipc-msg-getval msg "referenceSymbol"))
	 (to-list (clipc-msg-getval msg "referencesTo"))
	 (from-list (clipc-msg-getval msg "referencesFrom")))
    (save-excursion
      (set-buffer buffer )
      (goto-char (point-max))
      (insert (format "Symbol %s:\n%s\n%s\n"
		      symbol
		      (print-ref-list from-list "Referenced by:\n")
		      (print-ref-list to-list "References to:\n"))))))

(defun print-ref-list (ref-list string) ""
  (if (null ref-list)
      string
    (concat string (print-ref (car ref-list)) 
	    (print-ref-list (cdr ref-list) ""))))
  
(defun print-ref (ref) ""
  (let* ((type (clipc-dict-getval ref "symbolType"))
	 (name (clipc-dict-getval ref "symbolName"))
	 (unDef (clipc-dict-getval ref "isUndef"))
	 (unRes (clipc-dict-getval ref "isUnres"))
	 (type-before (clipc-dict-getval type "typeBefore"))
	 (type-after (clipc-dict-getval type "typeAfter")))
    (if (and (stringp type-before)
	     (not (string= "*" (substring type-before -1 nil))))
	(setq type-before (concat type-before " ")))
    (concat "\t" (if (null type-before) "<extern>" type-before)
	    name
	    (if (null type-after) "" type-after)
	    (if (not (null unDef)) " (undefined)")
	    (if (not (null unRes)) " (unresolved)")
	    "\n")))

;; send a command to cx or pdm.  If prompt is not given, default to
;; echoing in workspace

(defun send-clipc-cmd (cmd &optional echo) ""
  (clipc-request-send "cxcmd_exec" 
		      (list (list "commandLine" cmd)
			    (list "cxcmdEchoCommand" 
				  (if (null echo) 1 echo)))))

(defvar cl-source-window-buffer nil
  "For CenterLine, the current source window buffer.  This is local to each
workspace buffer.")

(defvar cl-source-window-keymap-copy nil
  "For CenterLine, a copy of the local keymap of the current source window
buffer as it was before it was modified for CenterLine.  This is local to each
workspace buffer.")

;; from gdb.el - modified to reverse windows.
(defun cx-display-line (true-file line)
  (let* ((buffer (and true-file (find-file-noselect true-file)))
	 (pos) window local-map)
    (if buffer
        (progn
          (if (one-window-p t)
              ;; only one window.
              (let*
                  ((temp (selected-window))
                   (cur-buffer (window-buffer temp)))
                (setq window temp)
                (set-window-buffer temp buffer)
                (select-window (display-buffer cur-buffer)))
            (setq window (display-buffer buffer)))
          (save-excursion
            (if cl-current-workspace-buffer
                (progn
                  (set-buffer cl-current-workspace-buffer)
                  ;; Restore the local bindings for the current source window
                  ;; buffer.
                  (if (and cl-source-window-buffer
                           (not (eq nil (buffer-name cl-source-window-buffer))))
                      ;; Buffer still exists.
                      (progn
                        (set-buffer cl-source-window-buffer)
                        (use-local-map cl-source-window-keymap-copy)))
                  ;; Set the current-workspace local variable -
                  ;; cl-source-window-buffer.
                  (setq cl-source-window-buffer buffer)))
            (set-buffer buffer)
            (setq local-map (current-local-map))
            (save-excursion
              (set-buffer cl-current-workspace-buffer)
              ;; Set local copy of var.
              (setq cl-source-window-keymap-copy
                    (copy-keymap (current-local-map))))
            (run-hooks 'cl-source-window-hooks)
            (save-restriction
              (widen)
              (goto-line line)
              (setq pos (point))
              (setq overlay-arrow-string "=>")
              (or overlay-arrow-position
                  (setq overlay-arrow-position (make-marker)))
              (set-marker overlay-arrow-position (point) (current-buffer)))
            (cond ((or (< pos (point-min)) (> pos (point-max)))
                   (widen)
                   (goto-char pos))))
          (set-window-point window overlay-arrow-position)
          (raise-frame (window-frame
                        (get-buffer-window
                         cl-current-workspace-buffer)))))))

;; clear out the arrow
(defun cl-clear-arrow () "clear overlay arrow when execution completes"
  (setq overlay-arrow-string nil))

;; Return a default tag to search for, based on the text at point.
(defun cl-find-tag-default ()
  (save-excursion
    (while (looking-at "\\sw\\|\\s_")
      (forward-char 1))
    (if (re-search-backward "\\sw\\|\\s_" nil t)
	(progn (forward-char 1)
	       (buffer-substring (point)
				 (progn (forward-sexp -1)
					(while (looking-at "\\s'")
					  (forward-char 1))
					(point))))
      nil)))

(defun cl-find-tag-tag ()
  ""
  (let* ((default (cl-find-tag-default))
	 (spec (read-string
		(format "display (default %s) " default))))
    (if (equal spec "")
	default
      spec)))


;; parse a hex string
(defun string-to-hexint (string) ""
  (let ((char)
	(zero (string-to-char "0"))
	(nine (string-to-char "9"))
	(small-a (string-to-char "a"))
	(small-f (string-to-char "f"))
	(big-a (string-to-char "A"))
	(big-f (string-to-char "F"))
	(rval 0))
    (catch 'non-digit
      (while (and (not (null string)) (not (string= string "")))
	(setq char (string-to-char string))
	(setq string (substring string 1 nil))
	(setq rval (+ (* 16 rval)
		      (cond ((and (>= char zero) (<= char nine))
			     (- char zero))
			    ((and (>= char small-a) (<= char small-f))
			     (+ 10 (- char small-a)))
			    ((and (>= char big-a) (<= char big-f))
			     (+ 10 (- char big-a)))
			    (t (throw 'non-digit rval))))))
      rval)))

(provide 'clipc)

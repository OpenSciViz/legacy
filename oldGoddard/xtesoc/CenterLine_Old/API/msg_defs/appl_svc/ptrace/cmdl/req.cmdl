//-SECTION
//    *.+ *Center Ptracer Specific Request Messages
//
//-DESCRIPTION
// Request messages providing interfaces to functions provided only
// by the *Center Ptrace  application service are described in this section.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// CX asks for a process to set breakpoints in object code segments of
// a project.  This capability may go away if we implement single-process
// object code debugging in the interpreter.  Until it does, the
// interpreter sends out a request with its process id, the address and size
// of a memory segment within its address space that should be shared with
// the breakpoint setting process, and the address of a function that will
// call _kill() to deliver IPC signals to the interpreter.
//
// Once a ptrace(2) connection is made between the ptracer and the
// interpreter, a side channel taking advantage of ptrace shared memory 
// and signal behavior is used for all requests to the ptracer.
// Processes that want to handle this request 
// without understanding the ptrace IPC mechanism used should respond
// with a status other than 'okay', and object code breakpoints will
// not be enabled in the corresponding interpreter session.

mgr_obj_breakpoints
 -request (
// The process id of the CX interpreter.
	pid: INT,  

// The start of the region in CX's address space that should be used
// for shared memory IPC.
	packetaddr: INT,       

// The end of CX's text space.
	etext: INT,         

// The start of the function that CX will use to deliver signals to itself.
	funcaddr: INT,         

// The size of the signal delivering function.
	funcsize: INT
 )         

 -notify  ( 
// The status reply should be 'okay' if the ptracer process has established
// a ptrace connection and can read from and write to the shared memory 
// segment.  Otherwise, the status should be some other string.
	status: STRING
 )

//-END MESSAGE DEFINITION mgr_obj_breakpoints

//-END SECTION *Center Ptracer Specific Request Messages

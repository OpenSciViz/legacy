//-SECTION
//    *.+ CX Specific Definitions
//
//-DESCRIPTION
// Definitions used by the CX specific request and notification message
// definitions.




//-DEFINITION
//
//-DESCRIPTION
//
// UnresolvedSymbol describes the unresolved symbols emitted as part of
// the reply to a link or unres request.

UnresolvedSymbol: (T_DCT
// The name of the unresolved symbol.
	name: IdentifierName,

// The type of the symbol, to the extent it is known.
	type: ExprType
)

//-END DEFINITION UnresolvedSymbol




//-DEFINITION
//
//-DESCRIPTION
// Describes a change in the suppression state of an error or warning.

SuppItem: (T_DCT
// The warning or error number to be suppressed or unsuppressed.
	errNumber: INT,

// The abbreviated human-readable description of the warning or error.
	errAbbrev: STRING,

// The action on the suppress: add, remove, or just list.
	suppAction: (T_ENU REMOVE=0, ADD=1, LIST=2),

// The actual scope.  Redundant with the existence of the other fields, but
// more stable with them.
	suppScope: (T_ENU GLOB=0, FILE=1, LINE=2, PROC=3, NAME=4),

// The identifier name, if a warning is to be suppressed on a name.
// This, and the following two optional fields are mutually exclusive-
// only one of these fields will be present in the message.
	& suppressOnName: IdentifierName,

// The function name for procedure scope.
	& suppressInFunc: IdentifierName,

// The location for file or line scope.
	& suppressInFile: FileName,
        & fileLine: INT
)

//-END DEFINITION SuppItem




//-DEFINITION
//
//-DESCRIPTION
//    An array of full pathnames of the files which represent a
//    loaded item.  In the current implementation, the array will
//    always be of length 1.

ActualFilenames: (T_ARY FileName)

//-END DEFINITION ActualFilenames




//-DEFINITION
//
//-DESCRIPTION
//    Each command response notification includes this to
//    indicate the execution status of the command.

CxcmdLoadResponseStatus: (T_ENU
// Should never happen.
		UNKNOWN = 0,

// No arguments were given to the command.
// No part of the command was executed.
		NO_ARGUMENTS = 1,

// An argument error exists for one of the items.
// No part of the command was executed.
		ARGUMENT_ERROR = 2,

// Command execution was asynchronously interrupted.
// Some operations might have completed successfully.
                INTERRUPT = 3,

// There was some problem with one of more of the argument
// items- see the item error array for specifics.
// Some operations might have completed successfully.
		ITEM_ERROR = 4,

// Executed successfully on all items.
                OK = 5
)

//-END DEFINITION CxcmdLoadResponseStatus




//-DEFINITION
//
//-DESCRIPTION
//    Used by all command request messages which take
//    arguments which specify a module to load, unload or swap.
//    Also used to return info on the load in various
//    event_*_* messages.

LoadItemSpec: (T_DCT
// Name of item to load, unload, reload, or swap.
	itemName: LoadItemName,

// Type of the item.
	& loadAs: LoadItemType,		

// Further specifies item to load.  Also specifies language-
// specific compilation parameters.
	& language: (T_ENU		
// Figure it out from the filename extension.
		DETERMINE_FROM_FILENAME = 0,

// Language is C.
		C = 1,

// Language is C++.
		CXX = 2,

// Language is ANSI C.  This is currently not supported and it is not
// likely to ever be supported as the parser really needs to know a lot
// more information about the source language than just that it is
// supposed to be ANSI.
		ANSI_C = 3
	),

// -I, -L, -D, -U flags to the load.
	& loadArgs: STRING,		

// Lazy generation level for ObjectCenter.
	& cxxRememberDecls: (T_ENU
// Should never happen.
		DECL_UNKNOWN = 0,

// Remember all declarations seen; corresponds to the -z0
// option on the load command.  This is the default.
		DECL_REMEMBER_ALL = 1,

// Remember all declarations except for unused class 
// declarations; this is what USL cfront 3.0 does by
// default; corresponds to the -z1 option on the load
// command.
		DECL_REMEMBER_MOST = 2,

// Intermediate level elision of unneeded declarations;
// this value is not used in the current implementation,
// but is instead treated as DECL_REMEMBER_MINIMAL;
// corresponds to the -z2 option on the load command.
		DECL_REMEMBER_SOME = 3,

// Most aggressive elision of unneeded declarations; 
// remembers only those declarations needed by global
// non-inline functions and by global instances of
// objects; corresponds to the -z3 option on the load
// command.
		DECL_REMEMBER_MINIMAL = 4
	),

// Specifies the directory from which relative
// filenames are based.
	& workingDir: STRING,		

// Used for loads and swaps only.  When set, specifies
// that warnings should be ignored on loads.
	& isIgnoreWarnings: INT,	

// Used for loads and swaps only.  Specifies when and
// if debugging symbols should be included when
// object files are loaded.
	& includeDebugInfo: (T_ENU
// Include debugging symbols for this load at load time.
		YES = 0,

// Don't include debugging symbols with this load, but a reference
// to this info should cause a load with debegging symbols at
// that time (on-demand loading of debugging info).  Currently
// unsupported.
		ON_DEMAND = 1,

// Don't include debugging symbols with the load, and never load
// them on-demand, either.
		NEVER = 2
	),

// When set, an object file includes run time instrumentation checks.
	& isInstrumented: INT,

// When set, a source file will be loaded using the
// current global parser configuration.  If this is the
// first time the file is loaded, this flag is moot.
// Subsequent reloads or swaps of the file will reuse
// the parser configuration used when the file was first
// loaded, unless overridden with this flag.
 	& useGlobalParserConfig: INT,

// When set, load behaves as if auto_replace is set. Set
// this for reissues of load commands that failed due
// to replace confirmation needed.
	& isForcedReplace: INT,			

// When set, load behaves as if auto_compile is set.  Set
// this for reissues of load commands that failed due
// to compile confirmation needed.
	& isForcedCompile: INT,

// Used for loads and swaps only.  When set, specifies
// that load should be deferred.  Currently unsupported.
	& isDeferred: INT
)

//-END DEFINITION LoadItemSpec




//-DEFINITION
//
//-DESCRIPTION
//    An array of these is returned with each command reply.
//    Each element of the array indicates the status of the
//    attempt to load (unload) the corresponding element of
//    the original ItemSpec array in the request.

LoadItemResponse: (T_DCT
// Original name requested in the load command.
	itemName: LoadItemName,

// Status of the load/unload for this item.
	status: ItemStatus,

// Human readable string indicating why file cannot be loaded,
// unloaded, or swapped.  Does not appear unless status != OK.
	& errorMsg: STRING
)

//-END DEFINITION LoadItemResponse




//-DEFINITION
//
//-DESCRIPTION
//    Contains the status of the operation and information on
//    the current state of the load item.

LoadItemStatus: (T_DCT
// Name used to specify file or module in original command request.
	itemName: LoadItemName,

// CX module list number for the item.  If the item already exists,
// this is its module number.  If it doesn't, this is the number
// newly assigned for it.  This field is undefined for failed
// loads that would create a new module in the module list.
	moduleNumber: INT,

// Module list number of the parent library, if the module
// is an object module loaded from a library, or MOD_ERROR
// if not.
	parentLibModNumber: INT,

// Full pathname of files as actually found.  Undefined for
// failed loads.
	actualFileNames: ActualFilenames,

// Full pathname of the real or likely source file for this
// item if itemType == OBJECT, omitted otherwise.  Appears
// only for load items, not unload items.  Undefined for failed
// loads.
	& sourceFilePath: FileName,

// Status of the load/unload for this item.
	status: ItemStatus,

// Human readable string indicating why file cannot be loaded,
// unloaded, or swapped.  Does not appear unless status != OK.
	& errorMsg: STRING,

// Type of the item as loaded.
	itemType: LoadItemType,	

// Set only if a module is allocated in the module list for
// this item.  The load may or may not have been
// successful, but if this is set, a module has been allocated
// for the item in the module list.  For unloaded items, indicates
// if the item (still) has a place in the module list.
	hasModule: Bool,

// Set if this file has just been created as a consequence of the load.
// Does not appear for unloaded items.
	& wasCreated: Bool,			
	
// Set if this file has just been reloaded as a consequence of
// the load.  Does not appear for unloaded items.
	& wasReloaded: Bool,			

// Set if this file has just been recompiled as a consequence of
// the load.  Does not appear for unloaded items.
	& wasRecompiled: Bool,			

// Set if this file replaced a file of the "other" form as a
// consequence of the load.  Does not appear for unloaded items.
	& wasReplaced: Bool
)

//-END DEFINITION LoadItemStatus




//-DEFINITION
//
//-DESCRIPTION
//    Item names are used to indicate an item for load, unload, or swap.
//    Since we provide support for directly naming items (at present,
//    only files) and for indirectly naming items (as in, "the file
//    containing the def for this function), an LoadItemName cannot be
//    represented as a simple string.

LoadItemName: (T_DCT
// Name of the file or module (or name of a function
// defined in a file) or library member to load, unload, or swap.
	name: STRING,				
	
// Name of the library from which the above specified
// member should be loaded.
	& lib: STRING,				

// What does the name represent?
	nameIs: (T_ENU
// When set, indicates that the name argument
// contains the name of a file.
		FILE = 0,

// When set, indicates that the name argument
// contains the name of an identifier who's def is
// in the file, rather than the name of the file or module.
		IDENTIFIER = 1,

// When set, indicates that the name argument
// contains the name of a library member found in
// the library named by lib argument.
		LIBRARY_MEMBER = 2,

// When set, indicates that the name argument
// contains arbitrary text that should be resolved
// to an item to load/unload considering the command
// context.  This is used to provide arbitrary user-
// supplied text arguments to commands, and is never
// returned by the CX.
		FROM_CONTEXT = 3
	)
)

//-END DEFINITION LoadItemName




//-DEFINITION
//
//-DESCRIPTION
//    The form of an item.  For loads and swaps, this specifies
//    the form to use to load the object in.  This is also used
//    in status indications of the current form of an item.

LoadItemType: (T_ENU
// Shouldn't happen.
	UNKNOWN = 0,

// Determine the form of the item from its name.
	DETERMINE_FROM_FILENAME = 1,

// When set, load the item as source (or for swaps,
// swap in as source).
	SOURCE = 2,

// When set, load the item as object (or for swaps,
// swap in as object).
	OBJECT = 3,

// Indicates the file is (to be) loaded as a library.
// Use this rather than the more specific values below to
// indicate that the file should be loaded as whatever the
// default form of libraries is in the environment.
	LIBRARY = 4,

// On systems which provide a shared library type, indicates
// that the file is loaded as a static library.
	STATIC_LIBRARY = 5,

// On systems which provide a shared library type, indicates
// that the file is loaded as a shared library.
	SHARED_LIBRARY = 6
)

//-END DEFINITION LoadItemType




//-DEFINITION
//
//-DESCRIPTION
//    An enumeration of status values for the load (unload)
//    of a single load item.
//    An array of these is returned with each command reply.
//    Each element of the array indicates the status of the
//    attempt to load (unload) the corresponding element of
//    the original ItemSpec array in the request.

ItemStatus: (T_ENU
// Should never happen.
	UNKNOWN = 0,

// For loads, file was not found.
// For unloads, item to unload was not found
// to be loaded.
	NOT_FOUND = 1,

// The request asked that this module be loaded in
// source form, but the source file is not available.
	CANT_LOAD_AS_SOURCE = 2,

// The request asked that this module be loaded in
// object form, but the object file is not available.
	CANT_LOAD_AS_OBJECT = 3,

// The request asked that this file load be deferred,
// but the module has already been loaded for real.
// Currently unsupported.
	CANT_DEFER_LOAD = 4,

// Auto replace is not on, so the user must verify the replace.
	REPLACE_VERIFICATION_REQUIRED = 5,
						
// Auto compile is not on, so the user must verify the compile.
	COMPILE_VERIFICATION_REQUIRED = 6,
						
// For unloads, can't unload because it is not loaded.
	NOT_LOADED = 7,

// File(s) found but unreadable.
	PERMISSIONS_ERROR = 8,

// The load invoked a make, and it failed.
	MAKE_FAILED = 9,

// The load invoked a compile instead of a make, and it failed.
	COMPILE_FAILED = 10,

// The load failed for some other reason.
	LOAD_FAILED = 11,

// A command interrupt was received before this load was done.
// Item not loaded.
	INTERRUPT = 12,

// Warnings were emitted for the load and were ignored.
	LOADED_WITH_WARNINGS = 13,

// Everything (un)loaded fine.		       	
	OK = 14,

// The file didn't exist and an attempt was made to run the
// create file and it failed to create the file.
	CREATE_ATTEMPT_FAILED = 15,

// The file didn't exist and an attempt was made
// to run the create file but it has a format error.
	BAD_CREATE_FILE = 16,

// Could not get cd to the passed in working directory,
// or could not find the current directory in the process of trying.
	CANT_CD_TO_WORKING_DIR = 17
)

//-END DEFINITION ItemStatus




//-DEFINITION
//
//-DESCRIPTION
//    Name and/or value of an option (flag).

OptNameValue: (T_DCT
// The name of the option.
	optName: STRING,

// The Category of the option.
// One of the following values: "misc", "reload", "load",
// "run-time", "listing", "memory", "lint_load", "output",
//  "make", "paging", "general", "window".
	optCategory: STRING,

// The type of the changed option.
	optType: (T_ENU bool=1, int=2, string=3),

// The value of the changed option.
	optValue: STRING,

// A human-readable description of the option.
	optDesc: STRING,

// Boolean: Is this option read only (not settable by the user)?
	optReadOnly: INT
)		

//-END DEFINITION OptNameValue




//-DEFINITION
//
//-DESCRIPTION
//    Tells how project files are loaded/saved.

ProjectFileFormat: (T_ENU
// Saved as a file which when loaded will recreate state by
// executing CX commands.
	project = 3,

// Saved as a memory image file.  These are huge and take a long time
// to load and save.
	image = 2
)

//-END DEFINITION ProjectFileFormat

//-END SECTION CX Specific Definitions


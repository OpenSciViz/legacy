//-SECTION
//    *.+ Generic Debugger Notification Messages
//
//-DESCRIPTION
// General notifications of significant events (state changes) within
// the debugger are made via the following notification messages.  In general,
// notifications are made only of changes of internal state that are of
// interest to other members of the session than the requestor of the
// state change.  Changes of state that are only of concern to an
// application service requesting it are usually made known to the
// requestor in the reply message.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Sent when execution of the debugged program continues, usually
// from a breakpoint.

event_cont
 -notify ()

//-END MESSAGE DEFINITION event_cont




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Notification that the scope has changed, down the stack.

event_down
 -notify (
// How many stack frames has the scope moved down?
	frames: INT
 )

//-END MESSAGE DEFINITION event_down




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Sent when the current breaklevel of the debugger it reset.

event_reset
 -notify (
// Breaklevel reset to here.
	breakLevel: INT
)

//-END MESSAGE DEFINITION event_reset




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Sent when execution of a user's program begins.

event_run_start
 -notify (
// The arguments to the debugged program.
	args: STRING
)

//-END MESSAGE DEFINITION event_run_start




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Notifies of a change in the current dynamic (stack) scope.
// This message is emitted as a side effect of the UP and DOWN commands.
// Specifies the name of the function scoped to.

event_scop_hdr
 -notify (
// This string specifies the new scope in human-readable form.
// If this is a function's scope, the string is of the form:
//
// Scoping to <function>() at "<source file name>":<line number>
	message: STRING
  )

//-END MESSAGE DEFINITION event_scop_hdr




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Not determined at time of publication.

event_scop_src
 -notify (
// Not determined at time of publication.
	source: STRING
  )

//-END MESSAGE DEFINITION event_scop_src




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Notifies of a change in the current dynamic (stack) scope.
// This message is emitted as a side effect of the UP and DOWN commands.

event_scop_loc
 -notify (
// The line number (in the file specified by filePath) where
// the debugger is stopped, scoped to or tracing,
// if it is available.
	& lineNumber: INT,

// Optionally, the path name of the file.
// If this doesn't appear, the lineNumber refers to the "current" file.
	& filePath: FileName,

// Optionally, the function name location.
	& funcName: IdentifierName
  )

//-END MESSAGE DEFINITION event_scop_loc




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// When the status of a debugging item (breakpoint, watchpoint,
// display, etc.) changes, the debugger will emit an
// event_status notification containing the item description and
// a description of the state change.

event_status
 -notify (
// How did the state change?  If this is "report", then there is
// no state change; the message is issued as a result of a request
// for the current state of the item.
	operation: (T_ENU set, delete, report),

// The debugging item reported on.
	statusItem: StatusItem
 )

//-END MESSAGE DEFINITION event_status




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Whenever the debugger stops within a program it notifies the session
// where it is with this message.

event_stop_loc
 -notify (
// The line number (in the file specified by filePath) where
// the debugger is stopped, scoped to or tracing,
// if it is available.
	& lineNumber: INT,

// Optionally, the path name of the file.
// If this doesn't appear, the lineNumber refers to the "current" file.
	& filePath: FileName,

// Optionally, the function name location.
	& funcName: IdentifierName
  )

//-END MESSAGE DEFINITION event_stop_loc




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// This message is emitted whenever the debugger enters
// the scope of a traced function.

event_trac_loc
 -notify (
// The line number (in the file specified by filePath) where
// the debugger is stopped, scoped to or tracing,
// if it is available.
	& lineNumber: INT,

// Optionally, the path name of the file.
// If this doesn't appear, the lineNumber refers to the "current" file.
	& filePath: FileName,

// Optionally, the function name location.
	& funcName: IdentifierName
  )

//-END MESSAGE DEFINITION event_trac_loc




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Notification that the scope has changed, up the stack.

event_up
 -notify (
// How many stack frames has the scope moved up?
	frames: INT
 )

//-END MESSAGE DEFINITION event_up

//-END SECTION Generic Debugger Notification Messages



//-SECTION
//    *.+ Standard Application Service Request Messages
//
//-DESCRIPTION
// Request messages providing interfaces to functions provided by all
// application services are described in this section.




//-SECTION
//    .+ Process ID Functions
//
//-DESCRIPTION
// Messages requesting various Process ID functions are found
// in this section.  In general, session members should not know
// or care about the process IDs of other memebers- if they need
// to get a message to a specific member they should use application
// service names to specify the destination.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Command to restart a process (Maybe just the debugger).

cxcmd_restart
 -request (
// Current directory.
	directory: STRING,

// Arguments to the command.
	argString: STRING,

// The run time engine to start up.  1 = pdm.
 	whichRTE: INT,

// Boolean: echo command to workspace?
	cxcmdEchoCommand: INT
  )

 -notify (
// An error occurred starting up the process.
	isError: Bool
  )

//-END MESSAGE DEFINITION cxcmd_restart

//-END SECTION Process ID Functions




//-SECTION
//    + Standard Functions
//
//-DESCRIPTION
// Generic messages that every application service should be able to handle.
// Apserv_* are sent to application services (processes).
// Process_* are emitted by processes (application services).
// ( I would be happy to find another pair of prefixes )
// Currently, the generic messages are:
//   iconify,
//   deiconify,
//   raise,
//   lower,
//   map,
//   unmap,
//   configure,
//   quit.
//
// Until the message server handles directed message delivery, the
// protocol for handling these messages is that each process registers as
// a listener for the generic messages.  When it recieves one of these
// notifications it looks in the pidList for its process id.  If its process
// id is in the pidList, the process should execute its implementation of
// the generic command.  If the pidList is empty, the process should assume
// that the message was directed to it.
// 
// Only the configure message seems poorly defined.  For now it can either
// be ignored, or if the process has a Preferences dialog, that can be
// shown to the user.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell an application service to iconify itself.

apserv_iconify
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_iconify




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to deiconify itself.

apserv_deiconify
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_deiconify




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to raise its window.

apserv_raise
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_raise




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to lower its window.

apserv_lower
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_lower




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to map its window.

apserv_map
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_map




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to unmap its window.

apserv_unmap
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_unmap




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to configure itself.

apserv_configure
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION apserv_configure




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell a process to quit.

process_quit
 -notify (
	pidList: (T_ARY INT)
  )

//-END MESSAGE DEFINITION process_quit

//-END SECTION Standard Functions

//-END SECTION Standard Application Service Request Messages

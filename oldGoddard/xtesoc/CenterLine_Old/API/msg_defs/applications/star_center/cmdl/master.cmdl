//-APPLICATION SERVICE
//	2 The CenterLine Executive
//
//-DESCRIPTION
//	The executive is the interpretive engine which provides
//	standard debugging features, standard static program analysis
//	features, and program interpretation features to an application.
//
//	The CX implements the Generic Debugger and Generic Static Analysis
//	Tool application services, as well as the Standard application
//	service.  All messages defined for those virtual application
//	services are also supported by the CX.




//-SECTION
//	2.1 Request Message Definitions
//
//-DESCRIPTION
//	The CX provides a message-based programmatic interface with
//	subprogram-call semantics.  Generally, request messages and
//	their replies have function semantics, with the return value(s)
//	returned in the reply message.  However, this is not always the
//	case.  Often a request is made for the side effects only.
//	These cases of messages implementing procedure-call semantics
//	are outlined in detail in the PROTOCOL sections of the pages on
//	these messages.  If you don't see documentation speciifically
//	outlining the side effects of a request, it is most often the
//	case that the request provides simple function-call semantics.

INCLUDE ../../appl_svc/cx/cmdl/req.cmdl

//-END SECTION Request Message Definitions




//-SECTION
//	2.2 Notification Message Definitions
//
//-DESCRIPTION
//	Side effect messages emitted during CX execution are documented
//	here.  These messages identify state transitions occurring
//	within the CX, and are often used to notify requestors of the
//	effects of their (or others) requests.

INCLUDE ../../appl_svc/cx/cmdl/not.cmdl

//-END SECTION Notification Message Definitions




//-SECTION
//	2.3 Definitions
//
//-DESCRIPTION
//	The following definitions are used in the previous message
//	definitions.

INCLUDE ../../appl_svc/cx/cmdl/def.cmdl

//-END SECTION Definitions

//-END APPLICATION SERVICE The CenterLine Executive




//-APPLICATION SERVICE
//	3 The Process Debugging Mode Debugger
//
//-DESCRIPTION
// Standard debugging engine used to debug linked executables
// (processes).
//
// The PDM implements the Generic Debugger application services,
// as well as the Standard application service.  All messages
// defined for those virtual application services are also
// supported by the PDM.




//-SECTION
//	3.1 Notification Message Definitions
//
//-DESCRIPTION
//	Side effect messages emitted during PDM execution are documented
//	here.  These messages identify state transitions occurring
//	within the PDM, and are often used to notify requestors of the
//	effects of their (or others) requests.

INCLUDE ../../appl_svc/pdm/cmdl/not.cmdl

//-END SECTION Notification Message Definitions

//-END APPLICATION SERVICE The Process Debugging Mode Debugger




//-APPLICATION SERVICE
//	4 The Data Browser
//
//-DESCRIPTION
//	This application service provides a general user interface
//	display for information queries on program data entities.
//
//	The Data Browser implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Data Browser.




//-SECTION
//	4.1 Request Message Definitions
//
//-DESCRIPTION
//	Request messages supported by this application service are
//	found in this section.

INCLUDE ../../appl_svc/datab/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The Data Browser




//-APPLICATION SERVICE
//	5 The Email Server
//
//-DESCRIPTION
//	This application service provides general electronic mail
//	services to the application.  It is used for tech support
//	and complaint mail.
//
//	The Email Server implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Email Server.




//-SECTION
//	5.1 Request Message Definitions
//
//-DESCRIPTION
//	Request messages supported by this application service are
//	found in this section.

INCLUDE ../../appl_svc/email/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The Email Server




//-APPLICATION SERVICE
//	6 The Error Browser
//
//-DESCRIPTION
//	Manages the display of errors and warnings from compilers
//	and the CX engine.
//
//	The Error Browser implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Error Browser.




//-SECTION
//	6.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/errorb/cmdl/req.cmdl

//-END SECTION Request Message Definitions




//-SECTION
//	6.2 Notification Message Definitions
//
//-DESCRIPTION
//	Notifications made by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/errorb/cmdl/not.cmdl

//-END SECTION Notification Message Definitions

//-END APPLICATION SERVICE Error Browser




//-APPLICATION SERVICE
//	7 The Make Server
//
//-DESCRIPTION
//	Manages compilations and "makes" on behalf of other application
//	member application services.
//
//	The Make Server also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Make Server.




//-SECTION
//	7.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/make/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Make Server




//-APPLICATION SERVICE
//	8 The Reference Manual Browser
//
//-DESCRIPTION
//	The man browser provides a user interface to a library
//	of man pages.
//
//	The Man Browser also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Man Browser.




//-SECTION
//	8.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/manb/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Reference Manual Browser




//-APPLICATION SERVICE
//	9 The Reference Manual Server
//
//-DESCRIPTION
//	Provides a CLIPC message interface to a library of
//	reference manual pages.
//
//	The Reference Manual Server implements the Standard application
//	service and the Generic Reference Manual Server application
//	service.  All messages defined for these virtual application
//	services are supported by the Reference Manual Server- in fact,
//	the Reference Manual Server is simply an real implementation
//	of the Generic Reference Manual Server application service.

//-END APPLICATION SERVICE Reference Manual Server




//-APPLICATION SERVICE
//	10 The Options Browser
//
//-DESCRIPTION
//	Manages the display and setting of options for the CX engine.
//
//	The Options Browser also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Options Browser.




//-SECTION
//	10.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/optionsb/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Options Browser




//-APPLICATION SERVICE
//	11 The Cross Reference Browser
//
//-DESCRIPTION
//	Manages the display of static cross reference relationships.
//
//	The Cross Reference Browser also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Cross Reference Browser.




//-SECTION
//	11.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/xrefb/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Cross Reference Browser




//-APPLICATION SERVICE
//	12 The Project Browser
//
//-DESCRIPTION
//	Manages loading and saving of files and options associated with
//	a program debugging session.
//
//	The Project Browser also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Project Browser.
//
//-MAKES REQUESTS
;; ???? these need to be enumerated // centerline_XXX_browser_winctl
// compose_mail
// cx_edit_location
// cx_list_location
// cxcmd_action
// cxcmd_chdir
// cxcmd_cwd
;; ???? no def for this? // cxcmd_edit
// cxcmd_env_contents
// cxcmd_exec
;; ???? no def for this? // cxcmd_list
// cxcmd_load
// cxcmd_load_project
// cxcmd_mod_contents
// cxcmd_printopt
// cxcmd_reload
// cxcmd_restart
// cxcmd_save_project
// cxcmd_setopt
// cxcmd_stop
// cxcmd_swap
// cxcmd_unload
// cxcmd_watchpoint
// cxcmd_xref
//
//-LISTENS FOR
// event_build_start
// event_build_end
// event_load_start
// event_load_end
// event_reload_start
// event_reload_end
// event_swap_start
// event_swap_end
// event_unload_start
// event_unload_end
// event_load_project
// event_save_project
// event_option_changed
// event_chdir
// cxcmd_mod_contents
// event_run_start
// event_rte_restart
// process_quit





//-SECTION
//	12.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/projectb/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The Project Browser




//-APPLICATION SERVICE
//	13 The PTracer
//
//-DESCRIPTION
//	Breakpoint ptrace handler.
//
//	The PTracer also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the PTracer.




//-SECTION
//	13.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/ptrace/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The PTracer




//-APPLICATION SERVICE
//	14 The Interactive Workspace
//
//-DESCRIPTION
//	The *Center applicatons provide an interactive workspace for
//	command interaction with the engines.  Messages which set up
//	that workspace are defined here.
//
//	The Interactive Workspace also implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Interactive Workspace.




//-SECTION
//	14.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests handled by this application service are specified in
//	this section.

INCLUDE ../../appl_svc/workspace/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The Interactive Workspace




//-APPLICATION SERVICE
//	15 The Emacs Edit Server
//
//-DESCRIPTION
//	Provides a CLIPC message interface to the Emacs text editor.
//
//	The Emacs Edit Server implements the Standard application
//	service and the Generic Text Editor Server application
//	service.  All messages defined for these virtual application
//	services are supported by the Emacs Edit Server- in fact,
//	the Emacs Edit Server is simply a real implementation
//	of the Generic Text Editor Server application service.
//	???? Do we really have this?

//-END APPLICATION SERVICE The Emacs Edit Server




//-APPLICATION SERVICE
//	16 The TextEdit Edit Server
//
//-DESCRIPTION
//	Provides a CLIPC message interface to the TextEdit text editor.
//
//	The TextEdit Edit Server implements the Standard application
//	service and the Generic Text Editor Server application
//	service.  All messages defined for these virtual application
//	services are supported by the TextEdit Edit Server- in fact,
//	the TextEdit Edit Server is simply a real implementation
//	of the Generic Text Editor Server application service.
//	???? Do we really have this?

//-END APPLICATION SERVICE The TextEdit Edit Server




//-APPLICATION SERVICE
//	17 The Vi Edit Server
//
//-DESCRIPTION
//	Provides a CLIPC message interface to the Vi text editor.
//
//	The Vi Edit Server implements the Standard application
//	service and the Generic Text Editor Server application
//	service.  All messages defined for these virtual application
//	services are supported by the Vi Edit Server- in fact,
//	the Vi Edit Server is simply a real implementation
//	of the Generic Text Editor Server application service.
//	???? Do we really have this?

//-END APPLICATION SERVICE The Vi Edit Server




//-APPLICATION SERVICE
//	18 Generic Debugger Application Service
//
//-DESCRIPTION
//	The minimum interface which must be supported by any
//	program debugger application service is described in
//	the following sections.




//-SECTION
//	18.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests which must be supported are described here.

INCLUDE ../../appl_svc/dbg_gen/cmdl/req.cmdl

//-END SECTION Request Message Definitions




//-SECTION
//	18.2 Notification Message Definitions
//
//-DESCRIPTION
//	Side effect messages emitted during debugger execution are documented
//	here.  These messages identify state transitions occurring
//	within the debugger, and are often used to notify requestors of the
//	effects of their (or others) requests.

INCLUDE ../../appl_svc/dbg_gen/cmdl/not.cmdl

//-END SECTION Notification Message Definitions




//-SECTION
//	18.3 Definitions
//
//-DESCRIPTION
//	The following definitions are used in the previous message
//	definitions.

INCLUDE ../../appl_svc/dbg_gen/cmdl/def.cmdl

//-END SECTION Definitions

//-END APPLICATION SERVICE Generic Debugger Application Service




//-APPLICATION SERVICE
//	19 Generic Reference Manual Server Application Service
//
//-DESCRIPTION
//	The minimum interface which must be supported by any
//	reference manual ("man") application service is described in
//	the following sections.




//-SECTION
//	19.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests which must be supported are described here.

INCLUDE ../../appl_svc/man_gen/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Generic Reference Manual Server Application Service




//-APPLICATION SERVICE
//	20 Generic Static Analysis Tool Application Service
//
//-DESCRIPTION
//	The minimum interface which must be supported by any
//	static analysis tool application service is described in
//	the following sections.




//-SECTION
//	20.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests which must be supported are described here.

INCLUDE ../../appl_svc/sanal_gen/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Generic Static Analysis Tool Application Service




//-APPLICATION SERVICE
//	21 Generic Text Editor Application Service
//
//-DESCRIPTION
//	The minimum interface which must be supported by any
//	text editor application service is described in
//	the following sections.




//-SECTION
//	21.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests which must be supported are described here.

INCLUDE ../../appl_svc/tedit_gen/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE Generic Text Editor Application Service




//-APPLICATION SERVICE
//	22 The Run Window
//
//-DESCRIPTION
//	This application service provides a window for the
//	user's program to run in.
//
//	The Run Window implements the Standard application
//	service.  All messages defined for this virtual application
//	service are also supported by the Run Window.




//-SECTION
//	22.1 Request Message Definitions
//
//-DESCRIPTION
//	Request messages supported by this application service are
//	found in this section.

INCLUDE ../../appl_svc/run_win/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The Run Window




//-APPLICATION SERVICE
//	23 The License Manager
//
//-DESCRIPTION
//	This application service provides product licensing
//	for an application.




//-SECTION
//	23.1 Request Message Definitions
//
//-DESCRIPTION
//	Request messages supported by this application service are
//	found in this section.

INCLUDE ../../appl_svc/lic_server/cmdl/req.cmdl

//-END SECTION Request Message Definitions

//-END APPLICATION SERVICE The License Manager




//-APPLICATION SERVICE
//	24 The CLIPC Message Server
//
//-DESCRIPTION
//	The message server itself issues messages which indicate
//	the state of the application to all application services.




//-SECTION
//	24.1 Notification Message Definitions
//
//-DESCRIPTION
//	Request messages supported by this application service are
//	found in this section.

INCLUDE ../../appl_svc/msg_server/cmdl/not.cmdl

//-END SECTION Notification Message Definitions

//-END APPLICATION SERVICE The CLIPC Message Server




//-APPLICATION SERVICE
//	25 Standard Application Service
//
//-DESCRIPTION
//	This section describes the interface which must be provided
//	by any application service.  Any application service which
//	connects to an application session must register for all
//	of the request messages, although only those which appropriate
//	to the application service need to be handled with more than
//	a simple reply.  All application services should send any of
//	the notifications listed here when appropriate.
//
//	The definitions listed in this section are used in the definition
//	of the messages listed above, as well as in other definitions
//	in other application service documents.




//-SECTION
//	25.1 Request Message Definitions
//
//-DESCRIPTION
//	Requests which must be supported by all application services
//	are listed here.

INCLUDE ../../appl_svc/std/cmdl/req.cmdl

//-END SECTION Request Message Definitions




//-SECTION
//	25.2 Notification Message Definitions
//
//-DESCRIPTION
//	Side effect messages emitted during execution are documented
//	here.  These messages identify state transitions occurring
//	within an application service, and are often used to notify
//	requestors of the effects of their (or others) requests.

INCLUDE ../../appl_svc/std/cmdl/not.cmdl

//-END SECTION Notification Message Definitions




//-SECTION
//	25.3 Definitions
//
//-DESCRIPTION
//	The following definitions are used in the previous message
//	definitions, and/or in definitions in at least one other
//	application service document.

INCLUDE ../../appl_svc/std/cmdl/def.cmdl

//-END SECTION Definitions

//-END APPLICATION SERVICE Standard Application Service






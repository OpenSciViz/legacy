// CLIPC <-> ToolTalk Gateway Translation Verification Testing Rules
//
// This file contains rules used for the stimulation (and, ultimately,
// for the verification) part of verification tests of the gateway.
// The gateway, when invoked with the -i command line argument, will
// listen on its standard input for pseudotargets.  These are just
// names typed to its stdin.  When set in this mode, the gateway will
// attempt to match a line typed with the target of one of its
// translation rules.  Test fixturing for the gateway is composed of
// the following components:
//
//    The gateway under test, with its translation rules;
//
//    The stimulation gateway, with its stimulation rules;
//
// The rules in the stimulation gateway are resposible for sending
// the request messages which stimulate translation by the gateway
// under test, and receiving the replies to those messages.
// The actions associated with the replies can either report returned
// values to the human user, for manual verification, or can attempt
// the verify the output in an automated fashion.




// Startup Actions

START, NOTIFICATION, Startup:
    STRING $DEBUGGED_PROGRAM = " ";
    STRING $GATEWAY_CAT_SEPARATOR = "";
    STRING $CURRENT_LOC = "<workspace>";




// ToolTalk Do_Command request transaction
STDIN, NOTIFICATION, Do_Command_Transaction_Type:
    echo("Starting Test: Do_Command_Transaction_Type");
    alloc(MSG_TT_REQUEST, Do_Command);
    STRING MSG_TT_REQUEST.0 = "list";
    STRING MSG_TT_REQUEST.1 = "DUMMY";	// Must "allocate" all TT fields.
    send(MSG_TT_REQUEST);

TT, REPLY, Do_Command:
    echo("Return Value (): ", MSG_TARGET.1);
    echo("Ending Test: Do_Command_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Do_Command:
    echo("Reply returned with error.");
    echo("Ending Test: Do_Command_Transaction_Type");
    echo("");




// ToolTalk Quit request transaction
STDIN, NOTIFICATION, Quit_Transaction_Type:
    echo("Starting Test: Quit_Transaction_Type");
    alloc(MSG_TT_REQUEST, Quit);
    send(MSG_TT_REQUEST);

// Won't get back a reply with a valid transaction id.




// ToolTalk Debug request transaction
STDIN, NOTIFICATION, Debug_Transaction_Type:
    echo("Starting Test: Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, Debug);
    STRING MSG_TT_REQUEST.0 = "dummy_program_name";
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Debugged:
    echo("Got Debugged notification");
    echo("Program Name (dummy_program_name): ", MSG_TT_NOTIFICATION.0);

TT, REPLY, Debug:
    echo("Ending Test: Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Debug:
    echo("Reply returned with error.");
    echo("Ending Test: Debug_Transaction_Type");
    echo("");




// ToolTalk Unload_Debug request transaction
STDIN, NOTIFICATION, Unload_Debug_Transaction_Type:
    echo("Starting Test: Unload_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, Unload_Debug);
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Debug_Unloaded:
    echo("Got Debug_Unloaded notification");
    echo("Program Name (): ", MSG_TT_NOTIFICATION.0);

TT, REPLY, Unload_Debug:
    echo("Ending Test: Unload_Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Unload_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: Unload_Debug_Transaction_Type");
    echo("");




// ToolTalk Set_Trap request transaction
STDIN, NOTIFICATION, Set_Trap_Transaction_Type:
    echo("Starting Test: Set_Trap_Transaction_Type");
    alloc(MSG_TT_REQUEST, Set_Trap);
    STRING $TRAP_ACTION = "";
    STRING MSG_TT_REQUEST.2 = $TRAP_ACTION;
					// Default action.
    INTEGER MSG_TT_REQUEST.3 = 0;	// Dummy value for OUT parameter.
    STRING $TRAP_SPEC = "subprogram";
    STRING MSG_TT_REQUEST.4 = $TRAP_SPEC;
					// Stop in function "subprogram".
    STRING $TRAP_CONDITION = "";
    STRING MSG_TT_REQUEST.5 = $TRAP_CONDITION;
					// Default condition.
    STRING $TRAP_ID = "1";
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Trap_Set:
    echo("Got Trap_Set notification");
    echo("Program Name (dummy_program_name): ", MSG_TT_NOTIFICATION.0);
    echo("Action (", $TRAP_ACTION, "):", MSG_TT_NOTIFICATION.2);
    echo("Trap Spec (", $TRAP_SPEC, "): ", MSG_TT_NOTIFICATION.3);
    echo("Condition (", $TRAP_CONDITION, "): ", MSG_TT_NOTIFICATION.4);
    echo("Trap ID (", $TRAP_ID, "): ", MSG_TT_NOTIFICATION.5);

TT, REPLY, Set_Trap:
    echo("Got Set_Trap reply");
    echo("Trap ID (1): ", MSG_TT_REQUEST.3);
    STRING $TEST_TRAP_ID = MSG_TT_REQUEST.3;
    echo("Ending Test: Set_Trap_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Set_Trap:
    echo("Reply returned with error.");
    echo("Ending Test: Set_Trap_Transaction_Type");
    echo("");




// ToolTalk Delete_Trap request transaction
STDIN, NOTIFICATION, Delete_Trap_Transaction_Type:
    echo("Starting Test: Delete_Trap_Transaction_Type");
    alloc(MSG_TT_REQUEST, Delete_Trap);
    INTEGER MSG_TT_REQUEST.5 = $TEST_TRAP_ID;
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Trap_Delete:
    echo("Got Trap_Delete notification");
    echo("Program Name (dummy_program_name): ", MSG_TT_NOTIFICATION.0);
    echo("Trap ID (", $TEST_TRAP_ID, "): ", MSG_TT_NOTIFICATION.5);

TT, REPLY, Delete_Trap:
    echo("Got Delete_Trap reply");
    echo("Trap ID (", $TEST_TRAP_ID, "): ", MSG_TT_REQUEST.5);
    echo("Ending Test: Delete_Trap_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Delete_Trap:
    echo("Reply returned with error.");
    echo("Ending Test: Delete_Trap_Transaction_Type");
    echo("");




// ToolTalk Execute_Debug request transaction
STDIN, NOTIFICATION, Execute_Debug_Transaction_Type:
    echo("Starting Test: Execute_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, Execute_Debug);
    STRING MSG_TT_REQUEST.5 = "";
    STRING $CURRENT_LOC = "<workspace>";
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Debug_Executing:
    echo("Got Debug_Executing notification.");

TT, NOTIFICATION, Trap_Hit:
    echo("Got Trap_Hit notification.");
    echo("Trap ID: ", MSG_TARGET.5);
    STRING $CURRENT_LOC = MSG_TARGET.3;
    echo("At: ", $CURRENT_LOC);

TT, REPLY, Execute_Debug:
    echo("Got Execute_Debug reply.");
    echo("Ending Test: Execute_Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Execute_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: Execute_Debug_Transaction_Type");
    echo("");




// ToolTalk Stop_Debug request transaction
STDIN, NOTIFICATION, Stop_Debug_Transaction_Type:
    echo("Starting Test: Stop_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, Stop_Debug);
    STRING $TRAP_ACTION = "";
    STRING $TRAP_SPEC = "testprog.c:11";
    STRING MSG_TT_REQUEST.2 = $TRAP_SPEC;	// Stop in testprog.c, line #11
    INTEGER MSG_TT_REQUEST.3 = 0;		// Dummy value for OUT param.
    send(MSG_TT_REQUEST);
    STRING $TRAP_CONDITION = "";
    STRING $TRAP_ID = "2";

TT, REPLY, Stop_Debug:
    echo("Got Stop_Debug reply");
    echo("Trap ID (", $TRAP_ID, "): ", MSG_TT_REQUEST.3);
    STRING $TEST_STOP_ID = MSG_TT_REQUEST.3;
    echo("Ending Test: Stop_Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Stop_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: Stop_Debug_Transaction_Type");
    echo("");




// ToolTalk Resume_Debug request transaction
STDIN, NOTIFICATION, Resume_Debug_Transaction_Type:
    echo("Starting Test: Resume_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, Resume_Debug);
    STRING $RESUMED_FROM = $CURRENT_LOC;
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, Debug_Resumed:
    echo("Got Debug_Resumed notification.");
    echo("Program Name (dummy_program_name): ", MSG_TARGET.0);
    echo("Resuming from (", $RESUMED_FROM, "): ", MSG_TARGET.2);

TT, REPLY, Resume_Debug:
    echo("Ending Test: Resume_Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Resume_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: Resume_Debug_Transaction_Type");
    echo("");
    



// ToolTalk RunUntil_Debug request transaction
STDIN, NOTIFICATION, RunUntil_Debug_Transaction_Type:
    echo("Starting Test: RunUntil_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, RunUntil_Debug);
    STRING $RESUMED_FROM = $CURRENT_LOC;
    STRING $TRAP_SPEC = "testprog.c:21";
    STRING MSG_TT_REQUEST.2 = $TRAP_SPEC;	// Stop in testprog.c, line #21
    INTEGER MSG_TT_REQUEST.3 = 0;		// Dummy value for OUT param.
    STRING $TRAP_ID = "3";
    send(MSG_TT_REQUEST);

TT, REPLY, RunUntil_Debug:
    echo("Got RunUntil_Debug reply.");
    echo("Temporary Trap ID (", $TRAP_ID, "): ", MSG_TT_REQUEST.3);
    echo("Ending Test: RunUbtil_Transaction_Type");
    echo("");

TT, ERROR_REPLY, RunUntil_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: RunUntil_Debug_Transaction_Type");
    echo("");




// ToolTalk StepInto_Debug request transaction
STDIN, NOTIFICATION, StepInto_Debug_Transaction_Type:
    echo("Starting Test: StepInto_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, StepInto_Debug);
    STRING MSG_TT_REQUEST.2 = " ";		// Dummy value for OUT param.
    echo("Currently at: ", $CURRENT_LOC);
    send(MSG_TT_REQUEST);

TT, REPLY, StepInto_Debug:
    echo("Got StepInto_Debug reply.");
    echo("Stopping at: ", MSG_TT_REQUEST.2);
    STRING $CURRENT_LOC = MSG_TT_REQUEST.2;
    echo("Ending Test: StepInto_Transaction_Type");
    echo("");

TT, ERROR_REPLY, StepInto_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: StepInto_Debug_Transaction_Type");
    echo("");

    



// ToolTalk StepOut_Debug request transaction
STDIN, NOTIFICATION, StepOut_Debug_Transaction_Type:
    echo("Starting Test: StepOut_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, StepOut_Debug);
    STRING MSG_TT_REQUEST.2 = " ";		// Dummy value for OUT param.
    echo("Currently at: ", $CURRENT_LOC);
    send(MSG_TT_REQUEST);

TT, REPLY, StepOut_Debug:
    echo("Got StepOut_Debug reply.");
    echo("Stopping at: ", MSG_TT_REQUEST.2);
    STRING $CURRENT_LOC = MSG_TT_REQUEST.2;
    echo("Ending Test: StepOut_Transaction_Type");
    echo("");

TT, ERROR_REPLY, StepOut_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: StepOut_Debug_Transaction_Type");
    echo("");
    



// ToolTalk StepOver_Debug request transaction
STDIN, NOTIFICATION, StepOver_Debug_Transaction_Type:
    echo("Starting Test: StepOver_Debug_Transaction_Type");
    alloc(MSG_TT_REQUEST, StepOver_Debug);
    STRING MSG_TT_REQUEST.2 = " ";
    echo("Currently at: ", $CURRENT_LOC);
    send(MSG_TT_REQUEST);

TT, REPLY, StepOver_Debug:
    echo("Got StepOver_Debug reply.");
    echo("Stopping at: ", MSG_TT_REQUEST.2);
    STRING $CURRENT_LOC = MSG_TT_REQUEST.2;
    echo("Ending Test: StepOver_Debug_Transaction_Type");
    echo("");

TT, ERROR_REPLY, StepOver_Debug:
    echo("Reply returned with error.");
    echo("Ending Test: StepOver_Debug_Transaction_Type");
    echo("");
    



// ToolTalk Describe_DebugSymbol request transaction
STDIN, NOTIFICATION, Describe_DebugSymbol_Transaction_Type:
    echo("Starting Test: Describe_DebugSymbol_Transaction_Type");
    alloc(MSG_TT_REQUEST, Describe_DebugSymbol);
    STRING MSG_TT_REQUEST.2 = "a";
    STRING MSG_TT_REQUEST.3 = " ";
    send(MSG_TT_REQUEST);

TT, REPLY, Describe_DebugSymbol:
    echo("Got Describe_DebugSymbol reply");
    echo("Description of a (): ", MSG_TT_REQUEST.3);
    echo("Ending Test: Describe_DebugSymbol_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Describe_DebugSymbol:
    echo("Reply returned with error.");
    echo("Ending Test: Describe_DebugSymbol_Transaction_Type");
    echo("");
    



// ToolTalk Assign_DebugExpression request transaction
STDIN, NOTIFICATION, Assign_DebugExpression_Transaction_Type:
    echo("Starting Test: Assign_DebugExpression_Transaction_Type");
    alloc(MSG_TT_REQUEST, Assign_DebugExpression);
    STRING MSG_TT_REQUEST.2 = "a";
    STRING MSG_TT_REQUEST.3 = "100";
    send(MSG_TT_REQUEST);

TT, REPLY, Assign_DebugExpression:
    echo("Got Assign_DebugExpression reply");
    echo("Ending Test: Assign_DebugExpression_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Assign_DebugExpression:
    echo("Reply returned with error.");
    echo("Ending Test: Assign_DebugExpression_Transaction_Type");
    echo("");




// ToolTalk Eval_DebugExpression request transaction
STDIN, NOTIFICATION, Eval_DebugExpression_Transaction_Type:
    echo("Starting Test: Eval_DebugExpression_Transaction_Type");
    alloc(MSG_TT_REQUEST, Eval_DebugExpression);
    STRING MSG_TT_REQUEST.2 = "a + a";
    STRING MSG_TT_REQUEST.3 = " ";
    send(MSG_TT_REQUEST);

TT, REPLY, Eval_DebugExpression:
    echo("Got Eval_DebugExpression reply");
    echo("Value ((int) 200): ", MSG_TT_REQUEST.3);
    echo("Ending Test: Eval_DebugExpression_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Eval_DebugExpression:
    echo("Reply returned with error.");
    echo("Ending Test: Eval_DebugExpression_Transaction_Type");
    echo("");
    



// ToolTalk Get_DebugContext request transaction
STDIN, NOTIFICATION, Get_DebugContext_Transaction_Type:
    echo("Starting Test: Get_DebugContext_Transaction_Type");
    alloc(MSG_TT_REQUEST, Get_DebugContext);
    STRING MSG_TT_REQUEST.2 = " ";
    INTEGER MSG_TT_REQUEST.3 = 0;
    send(MSG_TT_REQUEST);

TT, REPLY, Get_DebugContext:
    echo("Got Get_DebugContext reply.");
    echo("Frames: ", MSG_TT_REQUEST.2);
    echo("Current frame: ", MSG_TT_REQUEST.3);
    STRING $CURRENT_FRAME = MSG_TT_REQUEST.3;
    echo("Ending Test: Get_DebugContext_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Get_DebugContext:
    echo("Reply returned with error.");
    echo("Ending Test: Get_DebugContext_Transaction_Type");
    echo("");
    



// ToolTalk Set_DebugContext request transaction
STDIN, NOTIFICATION, Set_DebugContext_Transaction_Type:
    echo("Starting Test: Set_DebugContext_Transaction_Type");
    alloc(MSG_TT_REQUEST, Set_DebugContext);
    STRING $CURRENT_FRAME = "0";
    INTEGER MSG_TT_REQUEST.2 = $CURRENT_FRAME;
    send(MSG_TT_REQUEST);

TT, NOTIFICATION, DebugContext_Set:
    echo("Got DebugContext_Set notification.");
    echo("Frames: ", MSG_TT_NOTIFICATION.2);
    echo("Current frame (", $CURRENT_FRAME, "): ", MSG_TT_NOTIFICATION.3);

TT, REPLY, Set_DebugContext:
    echo("Got Set_DebugContext reply.");
    echo("Ending Test: Set_DebugContext_Transaction_Type");
    echo("");

TT, ERROR_REPLY, Set_DebugContext:
    echo("Reply returned with error.");
    echo("Ending Test: Set_DebugContext_Transaction_Type");
    echo("");

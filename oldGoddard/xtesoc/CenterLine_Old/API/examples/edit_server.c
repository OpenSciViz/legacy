/*
MODULE editor_server_demo IS (
    main
)

OVERVIEW
    This is a CLIPC demonstartion program, used to show how a
    typical server for a CodeCenter request is built.  CodeCenter
    often needs to edit a file.  Since it maintains no native
    editing facilities itself, it sends out a request message
    (cx_edit_location) with the name of the file it needs edited.
    Normally, the vi or emacs edit server that comes with CodeCenter
    has already registered to handle these requests.  When this demo
    is run, it will register to handle the request.  Each time a
    cx_edit_location message is received, it will invoke an editor
    for the argument file.

    How to Build This Program

    Change the definition of EDITOR_INVOCATION_LINE to invoke the
    editor of your choice.  The code will append the name of the
    argument file to be edited to this string and use it as the
    argument to the system(3) function to invoke the editor.
    After this definition is changed, compile and link the program.

    How to Run This Program

    Bring up a CodeCenter or ObjectCenter session.  Once it is up,
    run this program in a separate window.  You may also want to
    bring up clms_monitor (also in a separate window) so you can
    watch the CLIPC message traffic.  When this program is run,
    it will register as a handler for cx_edit_location messages,
    pushing any esiting handlers down on the handler stack for
    this message type.  Now attempt to edit a file from within
    CodeCenter.  Using the File->Edit <listed file> menu selection
    will do nicely.  When this is done, CodeCenter will send out
    a cx_edit_location request message.  This program will handle it
    by formatting and returning a reply to CodeCenter, and invoking
    an editor on the file specified in the message.

    This program is very simple so, it will not handle another request
    until the editor is exited and control returns here.
*/


/*
 * Copyright (c) 1992, 1993 by CenterLine Software, Inc., Cambridge, MA.  All
 * rights reserved.   Use, duplication, or disclosure is subject to
 * restrictions described by license agreements with CenterLine Software, Inc.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "clipc.h"
#include "clipc-machdep.h"


/* Local Definitions */
#define EDITOR_INVOCATION_LINE		"vi"


/* Global Variables */
int registered = 0;
CLIPC_CONN clipc_conn = 0;
char *saber_cwd = 0;


/* Local Function Declarations */
void do_sig();


/* External Function Implementations */

main()
{
    CLIPC_MSG msg;
    char *msg_name;
    CLIPC_STATUS status;
    extern errno;
    int errcount = 0;

    /* Cnnect to the default CLIPC session. */
    clipc_conn = clipc_connection_init(0, 0, 0);
    if (clipc_conn == 0) {
	/* There is no session so ... */
	printf("couldn't get CLIPC connection\n");
	exit(1);
    }

    /* Register as a handler for the edit request message. */
    printf(" registering to handle cx_edit_location\n");
    status = clipc_register_handler(clipc_conn, "cx_edit_location");
    if (status != CLIPC_STATUS_OK) {
	fprintf(stderr, "clipc_register_handler: failed, status %d\n", status);
	exit(2);
    }

    /*
     * We're registered.  Now make sure that when we die,
     * we unregister for this message, to pop the previously
     * registered handler, if there is one.
     */
    registered = 1;
    signal(1, do_sig);
    signal(2, do_sig);
    signal(3, do_sig);    
    
    /* Request handling loop. */
    while (1) {
	msg = clipc_read_msg(clipc_conn, &status);
	/*
	 * We should only get here after a message has been
	 * sent to us, and since we've only registered for
	 * the cx_edit_location message, that should be the
	 * message we have.
	 */
	if ((msg == 0) || (status != CLIPC_STATUS_OK)) {
	    fprintf(stderr,
		    "clipc_read_msg returns 0x%x, status %d, errno %d\n",
		    msg, status, errno);
	    if (errcount++ > 5) {
		/* Don't try forever. */
		continue;
	    }
	    clipc_connection_close(clipc_conn);
	    exit(3);
	}

	msg_name = clipc_msg_getname(msg, &status);
	if ((msg_name == 0) || (status != CLIPC_STATUS_OK)) {
	    fprintf(stderr, "clipc_msg_getname returns 0x%x, status %d\n",
		    msg_name, status);
	    close_clipc_connection(clipc_conn);
	    exit(4);
	}
	errcount = 0;

	if (!strcmp(msg_name, "cx_edit_location")) {
	    CLIPC_MSG repl;
	    char *filename;
	    int linenum;
	    char command[1024];
	    
	    /* Build the command. */
	    filename = clipc_msg_getstring(msg, "filename", 0);
	    sprintf(command, "%s %s\n", EDITOR_INVOCATION_LINE, filename);
	    
	    /* Build ans send the reply message. */
	    repl = clipc_reply_alloc(msg, 0);
	    clipc_msg_addstring(repl, "status", "done");
	    clipc_msg_addstring(repl, "filename", filename);
	    /*
	     * We ignored the line number field in the request
	     * so set the line number field in the reply to the
	     * first line in the file.
	     */
	    clipc_msg_addint(repl, "linenum", 1);
	    clipc_write_msg(clipc_conn, repl);
	    clipc_msg_free(repl);

	    /*
	     * Now invoke the editor on the file.
	     */
	    system(command);
	}
	else {
	    printf("got message: '%s'\n", msg_name);
	}
	clipc_msg_free(msg);
    } /* if message is of type cx_edit_location */

} /* END main */


void do_sig(int signum)
{
    printf("do_sig: got signal %d", signum);
    
    if (clipc_conn == 0) {
	printf(" no CLIPC connection, exiting.\n");
	exit(1);
    }
    
    if (registered) {
	printf(" unregistering to handle cx_edit_location\n");
	clipc_unregister_handler(clipc_conn, "cx_edit_location");
	registered = 0;
    }

    exit(0);
}


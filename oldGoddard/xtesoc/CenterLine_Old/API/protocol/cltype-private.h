#if !defined (__CLTYPE_PRIVATE_H)
#define __CLTYPE_PRIVATE_H 1
#include "clipc.h"
#include "cltype.h"

struct cltype_msgdef {
    char *msg_name;
    int refcount;
    CLTYPE_MTYPENT mtypent;
    char *description;
};

struct cltype_typdef {
    char *typ_name;
    int refcount;
    CLTYPE_TYPDEF_TYPE type;
    union {
	CLIPC_TYPE      basic_type;
	CLTYPE_TYPDEF   array_elmt;
	CLTYPE_DICTENT  dict_fields;
	CLTYPE_ENUMENT  enum_fields;
	struct cltype_disc {
	    char *disc_name;
	    CLTYPE_DICTENT disc_fields;
	} disc;
    } un;
    char *description;    
};

struct cltype_mtypent {
    char *mtyp_name;
    CLTYPE_TYPDEF mtyp_typdef;
    CLTYPE_MTYPENT next;
};

struct cltype_dictent {
    char *key_name;
    int is_opt;
    char *description;
    CLTYPE_TYPDEF key_typdef;
    CLTYPE_DICTENT next;
};

struct cltype_enument {
    char *enum_name;
    int enum_value;
    int is_explicit;
    char *description;
    CLTYPE_ENUMENT next;
};

struct cltype_msgdef_list {
    CLTYPE_MSGDEF msgdef;
    CLTYPE_MSGDEF_LIST next;
};

struct cltype_typdef_list {
    CLTYPE_TYPDEF typdef;
    CLTYPE_TYPDEF_LIST next;
};


static struct basic_type {
    char *name;
    CLIPC_TYPE type;
} basic_types[] = {
    {"STRING", CLIPC_TYPE_STRING},
    {"MEM", CLIPC_TYPE_MEM},
    {"CHAR", CLIPC_TYPE_CHAR},
    {"INT", CLIPC_TYPE_INT},
    {"FLOAT", CLIPC_TYPE_FLOAT},
    {0, 0}
};

#endif /* !defined(__CLTYPE_PRIVATE_H) */

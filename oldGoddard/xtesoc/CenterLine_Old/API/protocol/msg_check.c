#include <stdio.h>
#include "clipc.h"
#include "cltype.h"
    


extern int cltype_parse_verbose ;
extern int cltype_lex_only ;
extern int yydebug;

int verbose = 0;
int check_all = 0;

main(int argc, char *argv[])
{
    int rval = 0;
    extern char *optarg;
    extern int optind, opterr;
    int c;
    int i;
    CLIPC_CONN conn;
    CLIPC_STATUS status;
    int errcount = 0;
    char *infile = 0;
    char *outfile = 0;    
    FILE *ofp = stdout;
    
    /*
     * open up a socket
     * print out the port number
     * wait for messages
     */

    while ((c = getopt(argc, argv, "adf:o:v")) != -1) {
	switch (c) {
	  case 'a':
	    check_all++;
	    break;
	  case 'd':
	    yydebug = 1;
	    break;
	  case 'f':
	    infile = optarg;
	    break;
	  case 'o':
	    outfile = optarg;
	    break;
	  case 'v':
	    verbose = 1;
	    break;
	}
    }
    argc -= optind;
    argv += optind;

    if (outfile) {
	ofp = fopen(outfile, "w");
	if (ofp == NULL) {
	    fprintf(stderr, "can't open '%s'\n", outfile);
	    exit(2);
	}
    }
    
    conn = init_clipc_connection(0);
    if (conn == 0) {
	fprintf(stderr, "couldn't init connection\n");
	exit(1);
    }

    if (infile == 0) {
	if (argc == 0) {
	    rval = cltype_parse_file(0);
	}
	else {
	    for (i = 0; i < argc; i++) {
		rval += cltype_parse_file(argv[i]);
	    }
	}

	if (check_all)
	  register_clipc_listener(conn, "*", CLIPC_MSGTYPE_ANY);
	else
	  cltype_register_msgdef_list(conn);
    }
    else {
	rval = cltype_parse_file(infile);
	if (check_all) {
	    register_clipc_listener(conn, "*", CLIPC_MSGTYPE_ANY);
	}
	else {
	    for (i = 0; i < argc; i++) {
		register_clipc_listener(conn, argv[i], CLIPC_MSGTYPE_ANY);
	    }
	}
    }
    
    if (0 && verbose) {
	cltype_print_typdef_list(ofp);
	cltype_print_msgdef_list(ofp);
    }

    while (1) {
	CLIPC_MSG msg;
	CLIPC_DICT envlp;
	CLTYPE_MSGDEF mdef;
	char *mgr_name;
	char *msg_name;	
	
	msg = clipc_read_msg(conn, &status);
	if (status != CLIPC_STATUS_OK) {
	    if (errcount++ > 10)
	      exit(errcount);
	    else
	      continue;
	}

	errcount = 0;
	envlp = clipc_msg_getenvlp(msg, 0);
	mgr_name = clipc_dict_getstring(envlp, "mgr_name", 0);
	msg_name = clipc_msg_getname(msg, 0);
	
	if (cltype_msg_format_ok(msg, ofp, check_all)) {
	    fprintf(ofp, "GOOD MESSAGE: '%s' from '%s'\n",
		    msg_name ? msg_name : "<NULL>",
		    mgr_name ? mgr_name : "<NULL>");
	}
	else {
	    fprintf(ofp, "BAD MESSAGE: '%s' from '%s'\n",
		    msg_name ? msg_name : "<NULL>",
		    mgr_name ? mgr_name : "<NULL>");
	    if (verbose) {
		fprint_msg(ofp, msg);
		mdef = cltype_msgdef_findbyname(msg_name);
		cltype_print_msgdef(ofp, mdef);
	    }
	}
	fflush(ofp);
    }
}


#if !defined(TRUE)
#define TRUE 1
#define FALSE 0
#endif
enum cltype_typdef_type {
    CLTYPE_TYPE_NIL,
    CLTYPE_TYPE_BASIC,    
    CLTYPE_TYPE_ARRAY,
    CLTYPE_TYPE_DICT,
    CLTYPE_TYPE_ENUM,
    CLTYPE_TYPE_DISC
};

typedef enum cltype_typdef_type CLTYPE_TYPDEF_TYPE;

typedef struct cltype_msgdef    *CLTYPE_MSGDEF;
typedef struct cltype_typdef    *CLTYPE_TYPDEF;
typedef struct cltype_mtypent   *CLTYPE_MTYPENT;
typedef struct cltype_dictent   *CLTYPE_DICTENT;
typedef struct cltype_enument   *CLTYPE_ENUMENT;
typedef struct cltype_disc      *CLTYPE_DISC;
typedef struct cltype_msgdef_list *CLTYPE_MSGDEF_LIST;
typedef struct cltype_typdef_list *CLTYPE_TYPDEF_LIST;

void cltype_msgdef_add(CLTYPE_MSGDEF msgdef);
void cltype_typdef_add(CLTYPE_TYPDEF typdef);

CLTYPE_TYPDEF  cltype_typdef_alloc(char *typ_name, CLTYPE_TYPDEF_TYPE type);
CLTYPE_MSGDEF  cltype_msgdef_alloc(char *msg_name, CLTYPE_MTYPENT mtypent,
				   char *comment);
CLTYPE_MTYPENT cltype_mtypent_alloc(char *typ_name, CLTYPE_DICTENT dictent);
CLTYPE_MTYPENT cltype_mtypent_defer_alloc(char *name, char *typename,
					  char *file, int line); 
CLTYPE_DICTENT cltype_dictent_alloc(char *name, CLTYPE_TYPDEF typdef,
				    int is_opt);
CLTYPE_ENUMENT cltype_enument_alloc(char *name, int val, int is_explicit);
CLTYPE_ENUMENT cltype_enument_add(CLTYPE_ENUMENT enument, CLTYPE_ENUMENT list);
CLTYPE_TYPDEF cltype_typdef_setname(CLTYPE_TYPDEF tdef, char *type_name,
				    char *comment);
CLTYPE_MSGDEF  cltype_msgdef_findbyname(char *msg_name);

CLTYPE_MTYPENT cltype_mtyplist_add(CLTYPE_MTYPENT mtype,
				    CLTYPE_MTYPENT list);

CLTYPE_TYPDEF cltype_typdef_findbyname(char *name, int add);


CLTYPE_TYPDEF cltype_make_dicttype(CLTYPE_DICTENT list);
CLTYPE_TYPDEF cltype_make_arraytype(CLTYPE_TYPDEF typdef);
CLTYPE_TYPDEF cltype_make_disctype(char *disc, CLTYPE_DICTENT list);
CLTYPE_TYPDEF cltype_make_enumtype(CLTYPE_ENUMENT list);
CLTYPE_DICTENT cltype_make_dictlist(CLTYPE_DICTENT dictent,
				    CLTYPE_DICTENT list, char *comment); 


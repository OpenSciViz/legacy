/*
 * Machine independent interface to data types
 */
#if defined(NeXT)
#include <sys/types.h>
#include <sys/dir.h>
typedef struct direct * CLIPC_DIR;
#define CLIPC_DIR_GETNAME(d) ((d)->d_name)

typedef union wait CLIPC_WAIT;
#define WEXITSTATUS(w) (((w).w_status>>8)&0xff)
#else
#include <dirent.h>
typedef struct dirent * CLIPC_DIR;
#define CLIPC_DIR_GETNAME(d) ((d)->d_name)
typedef int CLIPC_WAIT;
#endif

/*
 * Interface to machine dependent routines
 */

/*
 * System V has utsname rather than gethostname.
 * Return a pointer to a static buffer containing this hosts name.
 */
char *_clipc_gethostname(void);

/*
 * System V has getcwd rather than gethostname.
 * Return a pointer to a static buffer containing the process's
 * current directory.
 */
char *_clipc_getwd(void);

#ifndef SYSTIMEH
#define SYSTIMEH
extern "C++" {

/* The next two line were added by P.Coleman */
#define gettimeofday ______gettimeofday
#define settimeofday ______settimeofday

#include <cc/sys/time.h>

#undef gettimeofday
#undef settimeofday

extern "C" {
    int gettimeofday(struct timeval *);
    int settimeofday(struct timeval *);

}
}
#endif /* SYSTIMEH */

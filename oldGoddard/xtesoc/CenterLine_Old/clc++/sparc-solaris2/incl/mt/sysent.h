/* */


#ifndef __SYSENT_H
#define __SYSENT_H
extern "C++" {

#include <stdlib.h>
#include <unistd.h>

extern "C" int gethostname(const char *, int);
}

#endif

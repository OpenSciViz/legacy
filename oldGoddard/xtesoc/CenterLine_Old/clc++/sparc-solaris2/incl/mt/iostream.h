/*ident	"@(#)cls4:incl-master/const-headers/iostream.h	1.1" */
/*******************************************************************************
 
C++ source for the C++ Language System, Release 3.0.  This product
is a new release of the original cfront developed in the computer
science research center of AT&T Bell Laboratories.

Copyright (c) 1991 AT&T and UNIX System Laboratories, Inc.
Copyright (c) 1984, 1989, 1990 AT&T.  All Rights Reserved.

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of AT&T and UNIX System
Laboratories, Inc.  The copyright notice above does not evidence
any actual or intended publication of such source code.

*******************************************************************************/
#ifndef IOSTREAMH
#define IOSTREAMH
extern "C++" {

#include <rlocks.h>
#include <memory.h>
		/* Some inlines use memcpy */

/* If EOF is defined already verify that it is -1.  Otherwise
 * define it.
 */

#ifdef EOF
#	if EOF!=-1
#		define EOF (-1) 
#	endif
#else
#	define EOF (-1)
#endif

/* Don't worry about NULL not being 0 */
#ifndef NULL
#	define NULL 0
#endif

#define	zapeof(c) ((c)&0377)
	    /* extracts char from c. The critical requirement is
	     *      zapeof(EOF)!=EOF
	     * ((c)&0377) and ((unsigned char)(c)) are alternative definitions
	     * whose efficiency depends on compiler environment.
	     */

typedef long streampos ;
typedef long streamoff ;

class streambuf ;
class ostream ;
class ios ;

class unsafe_ios 
{
public: /* Some enums are declared in unsafe_ios to avoid pollution of
	 * global namespace
	 */
	enum io_state	{ goodbit=0, eofbit=1, failbit=2, badbit=4, 
				hardfail=0200};
				/* hard fail can be set and reset internally,
				 * but not via public function */
	enum open_mode	{ in=1, out=2, ate=4, app=010, trunc=020,
				nocreate=040, noreplace=0100} ;
	enum seek_dir	{ beg=0, cur=1, end=2 } ;

	/* flags for controlling format */
	enum		{ skipws=01,	
					/* skip whitespace on input */
			  left=02,  right=04, internal=010,
					/* padding location */
			  dec=020, oct=040, hex=0100, 
					/* conversion base */
			  showbase=0200, showpoint=0400, uppercase=01000,
			  showpos=02000, 
					/* modifiers */
			  scientific=04000, fixed=010000,
					/* floating point notation */
			  unitbuf=020000, stdio=040000
					/* stuff to control flushing */
			  } ;
	static const long 
			basefield ; /* dec|oct|hex */
	static const long
			adjustfield ; /* left|right|internal */
	static const long
			floatfield ; /* scientific|fixed */
public:
			unsafe_ios(streambuf*) ;
	virtual		~unsafe_ios() ;

	long		flags() const 	{ return x_flags ; }
	long		flags(long f);

	long		setf(long setbits, long field);
	long		setf(long) ;
	long		unsetf(long) ;

	int		width() const	{ return x_width ; }
	int		width(int w)
	{
			int i = x_width ; x_width = w ; return i ;
	}
		
	ostream*	tie(ostream* s); 
	ostream*	tie()		{ return x_tie ; }
	char		fill(char) ;
	char		fill() const	{ return x_fill ; }
	int		precision(int) ;
	int		precision() const	{ return x_precision ; }

	int		rdstate() const	{ return state ; }
/*
			operator void*()
				{
				if (state&(failbit|badbit|hardfail)) return 0 ;
				else return this ;
				}
			operator const void*() const
				{
				if (state&(failbit|badbit|hardfail)) return 0 ;
				else return this ;
				}
*/
	int		operator!() const
				{ return state&(failbit|badbit|hardfail); } 
	int		eof() const	{ return state&eofbit; }
	int		fail() const	{ return state&(failbit|badbit|hardfail); }
	int		bad() const	{ return state&badbit ; }
	int		good() const	{ return state==0 ; }
	void		clear(int i =0) 
				{	
				state =  (i&0377) | (state&hardfail) ;
				ispecial = (ispecial&~0377) | state ; 
				ospecial = (ospecial&~0377) | state ; 
				}
	streambuf*	rdbuf() { return bp ;} 

public: /* Members related to user allocated bits and words */
	long &		iword(int) ;
	void* &		pword(int) ;
	static long	bitalloc() ;
	static int	xalloc() ;

private: /*** privates for implemting allocated bits and words */ 
	static long	nextbit ;
	static long	nextword ;
	
	int		nuser ;
	union ios_user_union*
			x_user ;
	void	uresize(int) ;
	friend  class ios;
public: /* static member functions */
	static void	sync_with_stdio() ;
protected:
	enum 		{ skipping=01000, tied=02000 } ;
			/*** bits 0377 are reserved for userbits ***/
	streambuf*	bp;
	void		setstate(int b)
			{	state |= (b&0377) ;
				ispecial |= b&~skipping ;
				ispecial |= b ;
			}
	int		state;	
	int		ispecial;		
	int		ospecial;
	int		isfx_special;
	int		osfx_special;		
	int		delbuf;
	ostream*	x_tie;
	long 		x_flags;
	short		x_precision;
	char		x_fill;
	short 		x_width;

	static void	(*stdioflush)() ;

	void		init(streambuf*) ;
				/* Does the real work of a constructor */
			unsafe_ios() ; /* No initialization at all. Needed by
				 * multiple inheritance versions */
	int		assign_private ;
				/* needed by with_assgn classes */
private:
			unsafe_ios(unsafe_ios&) ; /* Declared but not defined */
	void		operator=(unsafe_ios&) ; /* Declared but not defined */
public:   /* old stream package compatibility */
	int		skip(int i) ; 
#if DEBUG
	friend void dump(unsafe_ios &);
#endif
};

class ios : virtual public unsafe_ios, public stream_MT 
{
public:
			ios(streambuf*) ;
	virtual		~ios() ;

        static void	ios_mutex_init();


	long		flags() const;
	long		flags(long f);

	long		setf(long setbits, long field);
	long		setf(long) ;
	long		unsetf(long) ;

	int		width() const;
	int		width(int w);
		
	ostream*	tie(ostream* s); 
	ostream*	tie();
	char		fill(char) ;
	char		fill() const;
	int		precision(int) ;
	int		precision() const;

	int		rdstate() const;
			operator void*();
			operator const void*() const;

	int		operator!() const; 
	int		eof() const;
	int		fail() const;
	int		bad() const;
	int		good() const;
	void		clear(int i =0);
	streambuf*	rdbuf(); 

public: /* Members related to user allocated bits and words */
	long &		iword(int) ;
	void* &		pword(int) ;
	static long	bitalloc() ;
	static int	xalloc() ;

private: /*** privates for implemting allocated bits and words */ 
	void		uresize(int) ;
public: /* static member functions */
	static void	sync_with_stdio() ;
protected:
	void		setstate(int b);
	static void	(*stdioflush)() ;
	void		init(streambuf*) ;
				/* Does the real work of a constructor */
			ios() ; /* No initialization at all. Needed by
				 * multiple inheritance versions */
private:		
			ios(ios&) ; /* Declared but not defined */
	void		operator=(ios&) ; /* Declared but not defined */
public:   /* old stream package compatibility */
	int		skip(int i) ; 
protected:
      static stream_rmutex static_mutlock;
      static int mutex_init_count;
#if DEBUG
	friend void dump(ios &);
#endif
};

inline ios::ios() { } 

inline ostream *ios::tie(ostream* s)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::tie(s);
}

inline long & ios::iword(int x)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::iword(x);
}

inline void* &ios::pword(int x)
{
	STREAM_ONLY_LOCK()
	return unsafe_ios::pword(x);
}

inline void ios::uresize(int n)
{
  STREAM_ONLY_LOCK()
  unsafe_ios::uresize(n);
}

inline char ios::fill(char c)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::fill(c);
}

inline long ios::setf(long b, long f)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::setf(b,f);
}

inline long ios::setf(long b)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::setf(b);
}

inline long ios::unsetf(long b)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::unsetf(b);
}

inline long ios::flags(long f)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::flags(f);
}

inline int ios::precision(int p)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::precision(p);
}

inline long ios::flags() const 
{ 
  STREAM_ONLY_LOCK()
  return unsafe_ios::flags();
}

inline ios::ios(streambuf* b)
{ 
  STREAM_RDBUF_LOCK(unsafe_ios)
  unsafe_ios::init(b);
}

inline ios::~ios()
{
  STREAM_RDBUF_LOCK(unsafe_ios)
  unsafe_ios::~unsafe_ios();
}

inline void ios::init(streambuf* b)
{
#if _REENTRANT_STREAMS
  ios_mutex_init();
  get_lock().init();	// initialize the recursive_mutex
#endif
  STREAM_RDBUF_LOCK(unsafe_ios)
  unsafe_ios::init(b);
}

inline int ios::width() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::width();
}

inline int ios::width(int w)
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::width(w);
}

inline ostream *ios::tie()
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::tie();
}

inline char ios::fill() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::fill();
}

inline int ios::precision() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::precision();
}

inline int ios::rdstate() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::rdstate();
}


inline ios::operator void*()
{
  STREAM_ONLY_LOCK()
  return ((state&(failbit|badbit|hardfail)) ? 0 : this);
}

inline ios::operator const void*() const
{
  STREAM_ONLY_LOCK()
  return ((state&(failbit|badbit|hardfail)) ? 0 : this);
}


inline int ios::operator!() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::operator!();
}

inline int ios::eof() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::eof();
}

inline int ios::fail() const
{
  STREAM_ONLY_LOCK()
  return (int)((state&(failbit|badbit|hardfail)) ? 0 : this);
}

inline int ios::bad() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::bad();
}

inline int ios::good() const
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::good();
}

inline void ios::clear(int i) 
{
  STREAM_ONLY_LOCK()
  unsafe_ios::clear(i);
}

inline streambuf *ios::rdbuf()
{
  STREAM_ONLY_LOCK()
  return unsafe_ios::rdbuf();
}

inline void ios::setstate(int b)
{
  STREAM_ONLY_LOCK()
  unsafe_ios::setstate(b);
}

class streambuf : public stream_MT 
{
	short		alloc;	
	short		x_unbuf;
	char* 		x_base;	
	char*		x_pbase;
	char*		x_pptr;	
	char* 		x_epptr;
	char* 		x_gptr;
	char*		x_egptr;
	char*		x_eback;
	int		x_blen;	
    private:
			streambuf(streambuf&); /* Declared but not defined */
	void		operator=(streambuf&); /* Declared but not defined */
    public:
	void		dbp();
    protected:
	// The unsafe version of the member functions
	char*		base_unlocked()	{ return x_base ; }
	char*		pbase_unlocked(){ return x_pbase ; }
	char*		pptr_unlocked() { return x_pptr ; }
	char*		epptr_unlocked(){ return x_epptr ; }
	char*		gptr_unlocked() { return x_gptr ; }
	char*		egptr_unlocked(){ return x_egptr ; }
	char*		eback_unlocked(){ return x_eback ; }
	char* 		ebuf_unlocked()	{ return x_base+x_blen ; }
	int		blen_unlocked() const	{ return x_blen; }
	void		setp_unlocked(char*  p, char*  ep)
	{
		x_pbase=x_pptr=p ; x_epptr=ep ;
	}
	void		setg_unlocked(char*  eb,char*  g, char*  eg)
	{
		x_eback=eb; x_gptr=g ; x_egptr=eg ;
	}
	void		pbump_unlocked(int n) 
	{ 
		x_pptr+=n ;
	}

	void		gbump_unlocked(int n) 
	{ 
		x_gptr+=n ;
		}

	void		setb_unlocked(char* b, char* eb, int a = 0 )
	{
		if ( alloc && x_base ) delete x_base ;
		x_base = b ;
		x_blen= (eb>b) ? (eb-b) : 0 ;
		alloc = a ;
		}
	int		unbuffered_unlocked() const  { return x_unbuf; }
	void		unbuffered_unlocked(int unb) { x_unbuf = (unb!=0)  ; }
	int		allocate_unlocked()
	{
		if ( x_base== 0 && !unbuffered_unlocked() ) return doallocate() ;
		else			  	 return 0 ;
	}

	// The safe versions of the member functions
	char*		base();
	char*		pbase();
	char*		pptr();
	char*		epptr();
	char*		gptr();
	char*		egptr();
	char*		eback();
	char* 		ebuf();
	int		blen() const;
	void		setp(char*  p, char*  ep);
	void		setg(char*  eb,char*  g, char*  eg);
	void		pbump(int n);
	void		gbump(int n);
	void		setb(char* b, char* eb, int a = 0 );
	int		unbuffered() const;
	void		unbuffered(int unb);
	int		allocate();
	virtual int 	doallocate();
    public : 
        // The MT-unsafe unlocked versions of the member functions
	int		in_avail_unlocked()
	{
		return x_gptr<x_egptr ? x_egptr-x_gptr : 0 ;
	}

	int		out_waiting_unlocked() 
	{	
		if ( x_pptr ) return x_pptr-x_pbase ;
		else	      return 0 ; 
	}

	int		sgetc_unlocked()
	{
		/***WARNING: sgetc does not bump the pointer ***/
		return (x_gptr>=x_egptr) ? underflow() : zapeof(*x_gptr);
	}
	int		snextc_unlocked()
	{
		return (++x_gptr>=x_egptr)
				? x_snextc()
				: zapeof(*x_gptr);
	}
	int		sbumpc_unlocked()
	{
		return  ( x_gptr>=x_egptr && underflow()==EOF ) 
				? EOF 
				: zapeof(*x_gptr++) ;
	}
	int		optim_in_avail_unlocked()
	{
		return x_gptr<x_egptr ;
	}
	int		optim_sbumpc_unlocked()
	{
		return  zapeof(*x_gptr++) ; 
	}
	void		stossc_unlocked()
	{
		if ( x_gptr >= x_egptr ) underflow() ;
		else x_gptr++ ;
	}

	int		sputbackc_unlocked(char c)
	{
		if (x_gptr > x_eback ) {
			if ( *--x_gptr == c ) return zapeof(c) ;
			else 		      return zapeof(*x_gptr=c) ;
		} else {
			return pbackfail(c) ;
		}
	}

	int		sputc_unlocked(int c)
	{
		return (x_pptr>=x_epptr) ? overflow(zapeof(c))
				      : zapeof(*x_pptr++=c);
	}
	int		sputn_unlocked(const char*  s,int n)
	 {
		if ( n <= (x_epptr-x_pptr) ) {
			memcpy(x_pptr,s,n) ;
			pbump_unlocked(n);
			return n ;
		} else {
			return xsputn(s,n) ;
		}
	}
	int		sgetn_unlocked(char*  s,int n)
	{
		if ( n <= (x_egptr-x_gptr) ) {
			memcpy(s,x_gptr,n) ;
			gbump_unlocked(n);
			return n ;
		} else {
			return xsgetn(s,n) ;
		}
	}
   	streambuf*	setbuf_unlocked(unsigned char*  p, int len) ;

	streambuf*	setbuf_unlocked(char*  p, int len, int count) ;

	// The mt-safe versions of the member functions

	virtual int	overflow(int c=EOF);
	virtual int	underflow();
	virtual int	pbackfail(int c);
	virtual int	sync();
	virtual streampos
			seekoff(streamoff,unsafe_ios::seek_dir,int =unsafe_ios::in|unsafe_ios::out);
	virtual streampos
			seekpos(streampos, int =unsafe_ios::in|unsafe_ios::out) ;
	virtual int	xsputn(const char*  s,int n);
	virtual int	xsgetn(char*  s,int n);

	int		in_avail();
	int		out_waiting() ;
	int		sgetc();
	int		snextc();
	int		sbumpc();
	int		optim_in_avail();
	int		optim_sbumpc();
	void		stossc();
	int		sputbackc(char c);
	int		sputc(int c);
	int		sputn(const char*  s,int n);
	int		sgetn(char*  s,int n);
	virtual streambuf*
			setbuf(char*  p, int len) ;
   	streambuf*	setbuf(unsigned char*  p, int len) ;

	streambuf*	setbuf(char*  p, int len, int count) ;
				/* obsolete third argument */
  			/*** Constructors -- should be protected ***/
			streambuf() ;
			streambuf(char*  p, int l) ;

			streambuf(char*  p, int l,int c) ;
			/* 3 argument form is obsolete.
			 * Use strstreambuf.
			 */
	virtual		~streambuf() ;
private:
	int		x_snextc() ;
#if DEBUG
	friend void dump(streambuf *);
#endif
};

// Bodies for the safe versions of the member functions
// Note the classes of member functions are not found in unsafe versions:
//    those declared only, those which are virtual, and constructors

inline char* streambuf::base()
{
  STREAM_ONLY_LOCK()
  return base_unlocked();
}

inline char* streambuf::pbase()
{
  STREAM_ONLY_LOCK()
  return pbase_unlocked();
}

inline char* streambuf::pptr()
{
  STREAM_ONLY_LOCK()
  return pptr_unlocked();
}

inline char* streambuf::epptr()
{
  STREAM_ONLY_LOCK()
  return epptr_unlocked();
}

inline char* streambuf::gptr()
{
  STREAM_ONLY_LOCK()
  return gptr_unlocked();
}

inline char* streambuf::egptr()
{
  STREAM_ONLY_LOCK()
  return egptr_unlocked();
}

inline char* streambuf::eback()
{
  STREAM_ONLY_LOCK()
  return eback_unlocked();
}

inline char*  streambuf::ebuf()
{
  STREAM_ONLY_LOCK()
  return ebuf_unlocked();
}

inline int streambuf::blen() const
{
  STREAM_ONLY_LOCK()
  return blen_unlocked();
}

inline void streambuf::setp(char*  p, char*  ep) 
{ 
  STREAM_ONLY_LOCK()
  setp_unlocked(p, ep);
}

inline void streambuf::setg(char*  eb,char*  g, char*  eg)
{ 
  STREAM_ONLY_LOCK()
  setg_unlocked(eb, g, eg);
}

inline void streambuf::pbump(int n)
{
  STREAM_ONLY_LOCK()
  pbump_unlocked(n);
}

inline void streambuf::gbump(int n)
{
  STREAM_ONLY_LOCK()
  gbump_unlocked(n);
}

inline void streambuf::setb(char* b, char* eb, int a)
{ 
  STREAM_ONLY_LOCK()
  setb_unlocked(b, eb, a);
}

inline int streambuf::unbuffered() const
{
  STREAM_ONLY_LOCK()
  return unbuffered_unlocked();
}

inline void streambuf::unbuffered(int unb)
{
  STREAM_ONLY_LOCK()
  unbuffered_unlocked(unb);
}

inline int streambuf::allocate()
{
  STREAM_ONLY_LOCK()
  return allocate_unlocked();
}

inline int streambuf::in_avail()
{
  STREAM_ONLY_LOCK()
  return in_avail_unlocked();
}

inline int streambuf::out_waiting() 
{
  STREAM_ONLY_LOCK()
  return out_waiting_unlocked();
}

inline int streambuf::sgetc()
{
  STREAM_ONLY_LOCK()
  return sgetc_unlocked();
}

inline int streambuf::snextc()
{
  STREAM_ONLY_LOCK()
  return snextc_unlocked();
}

inline int streambuf::sbumpc()
{
  STREAM_ONLY_LOCK()
  return sbumpc_unlocked();
}

inline int streambuf::optim_in_avail()
{
  STREAM_ONLY_LOCK()
  return optim_in_avail_unlocked();
}

inline int streambuf::optim_sbumpc()
{
  STREAM_ONLY_LOCK()
  return optim_sbumpc_unlocked();
}

inline void streambuf::stossc()
{
  STREAM_ONLY_LOCK()
  stossc_unlocked();
}

inline int streambuf::sputbackc(char c)
{
  STREAM_ONLY_LOCK()
  return sputbackc_unlocked(c);
}

inline int streambuf::sputc(int c)
{
  STREAM_ONLY_LOCK()
  return sputc_unlocked(c);
}

inline int streambuf::sputn(const char*  s,int n)
{
  STREAM_ONLY_LOCK()
  return sputn_unlocked(s,n);
}

inline int streambuf::sgetn(char*  s,int n)
{
  STREAM_ONLY_LOCK()
  return sgetn_unlocked(s,n);
}


class unsafe_istream : virtual public unsafe_ios
{
public: /* Constructor */
			unsafe_istream(streambuf*) ;
	virtual		~unsafe_istream() ;
public:	
	int		ipfx(int noskipws=0)
			{	if ( noskipws?(ispecial&~skipping):ispecial) {
					return do_ipfx(noskipws) ;
				} else return 1 ;
			}
	// ipfx0 and ipfx1 were added to match the solaris public interface
	int ipfx0() { return ispecial ? do_ipfx(0) : 1; }		// same as ipfx(0)
	int ipfx1() { return (ispecial&~skipping) ? do_ipfx(1) : 1; }	// same as ipfx(1)
	void		isfx() { }  
	unsafe_istream&	seekg(streampos p) ;
	unsafe_istream&	seekg(streamoff o, unsafe_ios::seek_dir d) ;
   	streampos	tellg() ; 
	unsafe_istream&	operator>> (unsafe_istream& (*f)(unsafe_istream&))
			{	return (*f)(*this) ; }
	unsafe_istream&	operator>> (unsafe_ios& (*f)(unsafe_ios&) ) ;
	unsafe_istream&	operator>>(char*);
	unsafe_istream&	operator>>(unsigned char*);
	unsafe_istream&	operator>>(unsigned char& c)
			{	if ( !ispecial && bp->optim_in_avail() ) {
					c = bp->optim_sbumpc() ;
					return *this;
				}
				else {
					return (rs_complicated(c));
				}
			}
	unsafe_istream&	operator>>(char& c)
			{	if ( !ispecial && bp->optim_in_avail() ) {
					c = bp->optim_sbumpc() ;
					return *this;
				}
				else {
					return (rs_complicated(c));
				}
			}
	unsafe_istream&	rs_complicated(unsigned char& c);
	unsafe_istream&	rs_complicated(char& c);
	unsafe_istream&	operator>>(short&);
	unsafe_istream&	operator>>(int&);
	unsafe_istream&	operator>>(long&);
	unsafe_istream&	operator>>(unsigned short&);
	unsafe_istream&	operator>>(unsigned int&);
	unsafe_istream&	operator>>(unsigned long&);
	unsafe_istream&	operator>>(float&);
	unsafe_istream&	operator>>(double&);
	unsafe_istream&	operator>>(streambuf*);
	unsafe_istream&	get(char* , int lim, char delim='\n');
	unsafe_istream&	get(unsigned char* b,int lim, char delim='\n');
	unsafe_istream&	getline(char* b, int lim, char delim='\n');
	unsafe_istream&	getline(unsigned char* b, int lim, char delim='\n');
	unsafe_istream&	get(streambuf& sb, char delim ='\n');
	unsafe_istream&	get_complicated(unsigned char& c);
	unsafe_istream&	get_complicated(char& c);
	unsafe_istream&	get(unsigned char& c)
			{
				if ( !(ispecial & ~skipping) && bp->optim_in_avail() ) {
					x_gcount = 1 ;
					c = bp->sbumpc() ;
					return *this;
				} else {
					return (get_complicated(c));
				}
			}
	unsafe_istream&	get(char& c)
			{
				if ( !(ispecial & ~skipping) && bp->optim_in_avail() ) {
					x_gcount = 1 ;
					c = bp->sbumpc() ;
					return *this;
				} else {
					return (get_complicated(c));
				}
			}
	int 		get()
			{
				int c ;
				if ( !ipfx(1) ) return EOF ;
				else {
					c = bp->sbumpc() ;
					if ( c == EOF ) setstate(eofbit) ;
					return c ;
					}
			}
	int		peek() 
			{
				if ( ipfx(-1) ) return bp->sgetc() ;
				else		return EOF ;

			}
	unsafe_istream&	ignore(int n=1,int delim=EOF) ;
	unsafe_istream&	read(char*  s,int n);
	unsafe_istream&	read(unsigned char* s,int n) 
			{
				return read((char*)s,n) ;
			}
	int		gcount() ;
	unsafe_istream&	putback(char c);
	int		sync()	{ return bp->sync() ; }
protected:  
	int		do_ipfx(int noskipws) ;
	void		eatwhite() ;
			unsafe_istream() ;
private: 
	int		x_gcount ;
	void 		xget(char*  c) ;
public: /*** Obsolete constructors, carried over from stream package ***/
			unsafe_istream(streambuf*, int sk, ostream* t=0) ;
				/* obsolete, set sk and tie
				 * via format state variables */
			unsafe_istream(int size ,char*,int sk=1) ;
				/* obsolete, use strstream */
			unsafe_istream(int fd,int sk=1, ostream* t=0) ;
				/* obsolete use fstream */
};

class istream : virtual public ios, public unsafe_istream
{
public: /* Constructor */
			istream(streambuf*) ;
	virtual		~istream() ;
public:	
	int		ipfx(int noskipws=0);			
	int		ipfx0();	// to match the solaris public interface
	int		ipfx1();	// to match the solaris public interface
	void		isfx() { }
	istream&	seekg(streampos p) ;
	istream&	seekg(streamoff o, unsafe_ios::seek_dir d) ;
   	streampos	tellg() ; 
	istream&	operator>> (istream& (*f)(istream&));
	istream&	operator>> (ios& (*f)(ios&) ) ;
	istream&	operator>>(char*);
	istream&	operator>>(unsigned char*);
	istream&	operator>>(unsigned char& c);
	istream&	operator>>(char& c);
	istream&	rs_complicated(unsigned char& c);
	istream&	rs_complicated(char& c);
	istream&	operator>>(short&);
	istream&	operator>>(int&);
	istream&	operator>>(long&);
	istream&	operator>>(unsigned short&);
	istream&	operator>>(unsigned int&);
	istream&	operator>>(unsigned long&);
	istream&	operator>>(float&);
	istream&	operator>>(double&);
	istream&	operator>>(streambuf*);
	istream&	get(char* , int lim, char delim='\n');
	istream&	get(unsigned char* b,int lim, char delim='\n');
	istream&	getline(char* b, int lim, char delim='\n');
	istream&	getline(unsigned char* b, int lim, char delim='\n');
	istream&	get(streambuf& sb, char delim ='\n');
	istream&	get_complicated(unsigned char& c);
	istream&	get_complicated(char& c);
	istream&	get(unsigned char& c);
	istream&	get(char& c);
	int 		get();
	int		peek();
	istream&	ignore(int n=1,int delim=EOF) ;
	istream&	read(char*  s,int n);
	istream&	read(unsigned char* s,int n);
	int		gcount() ;
	istream&	putback(char c);
	int		sync()	;
protected:  
	int		do_ipfx(int noskipws) ;
	void		eatwhite() ;
			istream() ;
public: /*** Obsolete constructors, carried over from stream package ***/
			istream(streambuf*, int sk, ostream* t=0) ;
				/* obsolete, set sk and tie
				 * via format state variables */
			istream(int size ,char*,int sk=1) ;
				/* obsolete, use strstream */
			istream(int fd,int sk=1, ostream* t=0) ;
				/* obsolete use fstream */
};

inline istream::istream(streambuf* sb) 
{
  STREAM_ONLY_LOCK()
  RDBUF_ONLY_LOCK()
  init(sb) ; 
}

inline int istream::ipfx(int noskipws)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  return unsafe_istream::ipfx(noskipws);
}

inline int istream::ipfx0()
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  return unsafe_istream::ipfx0();
}

inline int istream::do_ipfx(int noskipws)
{
	STREAM_RDBUF_LOCK(unsafe_istream) 
	return unsafe_istream::do_ipfx(noskipws);
}

inline int istream::ipfx1()
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  return unsafe_istream::ipfx1();
}

inline void istream::eatwhite()
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::eatwhite ();
}

inline istream& istream::operator>>(char *c)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::operator>>(c);
  return *this;
}

inline istream& istream::operator>>(unsigned char* s)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::operator>>(s);
  return *this;
}

inline istream& istream::putback(register char c) 
{ 
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::putback(c);
  return *this;
}

inline istream&	istream::operator>> (istream& (*f)(istream&))
{
  STREAM_ONLY_LOCK()
  streambuf* sb = unsafe_istream::rdbuf();
  (*f)(*this);
  return *this;
}

inline istream& istream::operator>>(unsigned char& c)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::operator>>(c);
  return *this;
}

inline istream& istream::operator>>(char& c)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::operator>>(c);
  return *this;
}

inline istream& istream::rs_complicated(unsigned char& c) 
{ 
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::rs_complicated(c);
  return *this;
}

inline istream& istream::rs_complicated(char& c)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::rs_complicated(c);
  return *this;
}

inline istream& istream::get_complicated(unsigned char& c)
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::get_complicated(c);
  return *this;
}

inline istream& istream::get_complicated(char& c)
{ 
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::get_complicated(c);
  return *this;
}

inline istream& istream::get(unsigned char& c)
{ 
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::get(c);
  return *this;
}

inline istream&  istream::get(char& c)
{ 
  STREAM_RDBUF_LOCK(unsafe_istream)
  unsafe_istream::get(c);
  return *this;
}

inline int istream::get()
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  return unsafe_istream::get();
}

inline int istream::peek() 
{
  STREAM_RDBUF_LOCK(unsafe_istream)
  return unsafe_istream::peek();
}

inline int istream::sync()
{
  STREAM_ONLY_LOCK()
  return unsafe_istream::sync();
}

inline istream& istream::seekg(streamoff o, unsafe_ios::seek_dir d)
{
	STREAM_RDBUF_LOCK(unsafe_istream) 
	unsafe_istream::seekg(o,d);
	return *this;
}

inline istream& istream::seekg(streampos p)
{
	STREAM_RDBUF_LOCK(unsafe_istream) 
	unsafe_istream::seekg(p);
	return *this;
}

class unsafe_ostream : virtual public unsafe_ios 
{
public: /* Constructor */
			unsafe_ostream(streambuf*) ;
	virtual		~unsafe_ostream();
public:	
	int		opfx()	/* Output prefix */
			{	if ( ospecial )	return do_opfx() ;
				else		return 1 ;
			}
	void		osfx() 
			{	if ( osfx_special ) do_osfx() ; }

	unsafe_ostream&	flush() ;
	unsafe_ostream&	seekp(streampos p) ;
	unsafe_ostream&	seekp(streamoff o, unsafe_ios::seek_dir d) ;
 	streampos	tellp() ; 
	unsafe_ostream&	put(char c)
	{
		if (ospecial || osfx_special) {
			return complicated_put(c);
		}
		else {
			if (  bp->sputc(c) == EOF )  {
				setstate(eofbit|failbit) ;
			}
			return *this ;
		}
	}
	// Put here for Solaris compatibility
        unsafe_ostream& put(unsigned char x)
			{ return put((char)x); } 
	unsafe_ostream&	complicated_put(char c);
	unsafe_ostream&	operator<<(char c)
	{
		if (ospecial || osfx_special) {
			return ls_complicated(c);
		}
		else {
			if (  bp->sputc(c) == EOF )  {
				setstate(eofbit|failbit) ;
			}
			return *this ;
		}
	}

	unsafe_ostream&	operator<<(unsigned char c) 
	{
		if (ospecial || osfx_special) {
			return ls_complicated(c);
		}
		else {
			if (  bp->sputc(c) == EOF )  {
				setstate(eofbit|failbit) ;
			}
			return *this ;
		}
	}
	unsafe_ostream& 	ls_complicated(char);
	unsafe_ostream& 	ls_complicated(unsigned char);

	unsafe_ostream&	operator<<(const char*);
	unsafe_ostream&	operator<<(int a); 
	unsafe_ostream&	operator<<(long);	
	unsafe_ostream&	operator<<(double);
	unsafe_ostream&	operator<<(float);
	unsafe_ostream&	operator<<(unsigned int a);
	unsafe_ostream&	operator<<(unsigned long);
	unsafe_ostream&	operator<<(void*);
/*	unsafe_ostream&	operator<<(const void*);   add this later */
	unsafe_ostream&	operator<<(streambuf*);
	unsafe_ostream&	operator<<(short i) { return *this << (int)i ; }
	unsafe_ostream&	operator<<(unsigned short i) 
			{ return *this << (int)i  ; }

	unsafe_ostream&	operator<< (unsafe_ostream& (*f)(unsafe_ostream&))
			{ return (*f)(*this) ; }
	unsafe_ostream&	operator<< (unsafe_ios& (*f)(unsafe_ios&) ) ;

	unsafe_ostream&	write(const char*  s,int n)	
	{
		if ( !state ) {
			if ( bp->sputn(s,n) != n ) setstate(eofbit|failbit);
			}
		return *this ;
	}
	unsafe_ostream&	write(const unsigned char* s, int n)
	{
		return write((const char*)s,n);
	}
protected: /* More unsafe_ostream members */
	int		do_opfx() ;
	void		do_osfx() ;
			unsafe_ostream() ;

public: /*** Obsolete constructors, carried over from stream package ***/
			unsafe_ostream(int fd) ;
				/* obsolete use fstream */
			unsafe_ostream(int size ,char*) ;
				/* obsolete, use strstream */
} ;

class ostream : virtual public ios, public unsafe_ostream
{
public: /* Constructor */
			ostream(streambuf*) ;
	virtual		~ostream();
public:	
	int		opfx();	/* Output prefix */
	void		osfx();
	ostream&	flush() ;
	ostream&	seekp(streampos p) ;
	ostream&	seekp(streamoff o, unsafe_ios::seek_dir d) ;
 	streampos	tellp() ; 
	ostream&	put(char c) {
			   STREAM_RDBUF_LOCK(unsafe_ostream)
			   unsafe_ostream::put(c);
			   return *this;  
			}
	// Solaris compatibility
	ostream&	put(unsigned char c) { return put((char)c); }

	ostream&	operator<<(char c);
	ostream&	operator<<(unsigned char c) ;
	ostream&	operator<<(const char*);
	ostream&	operator<<(int a); 
	ostream&	operator<<(long);	
	ostream&	operator<<(double);
	ostream&	operator<<(float);
	ostream&	operator<<(unsigned int a);
	ostream&	operator<<(unsigned long);
	ostream&	operator<<(void*);
/*	ostream&	operator<<(const void*);   add this later */
	ostream&	operator<<(streambuf*);
	ostream&	operator<<(short i) ;
	ostream&	operator<<(unsigned short i) ;
	ostream&	operator<< (ostream& (*f)(ostream&));
	ostream&	operator<< (ios& (*f)(ios&) ) ;

	ostream&	write(const char*  s,int n) ;
	ostream&	write(const unsigned char* s, int n) ;
protected: /* More ostream members */
	int		do_opfx() ;
	void		do_osfx() ;
			ostream() ;

public: /*** Obsolete constructors, carried over from stream package ***/
			ostream(int fd) ;	   /* obsolete use fstream */
			ostream(int size ,char*) ; /* obsolete, use strstream */
} ;

inline ostream::ostream(streambuf* sb)
{ 
  STREAM_ONLY_LOCK()
  RDBUF_ONLY_LOCK()
  unsafe_ios::init(sb) ;
}

inline ostream& ostream::flush()
{
  STREAM_RDBUF_LOCK(unsafe_ostream) 
  unsafe_ostream::flush();
  return *this;
}

inline streampos ostream::tellp()
{
	STREAM_RDBUF_LOCK(unsafe_ostream) 
	return unsafe_ostream::tellp();
}

inline ostream& ostream::seekp(streampos p)
{
	STREAM_RDBUF_LOCK(unsafe_ostream) 
	return (ostream &)unsafe_ostream::seekp(p);
}

inline int ostream::do_opfx()
{
	STREAM_RDBUF_LOCK(unsafe_ostream) 
	return unsafe_ostream::do_opfx();
}

inline void ostream::do_osfx()
{
	STREAM_RDBUF_LOCK(unsafe_ostream) 
	unsafe_ostream::do_osfx();
}

inline ostream& ostream::seekp(streamoff o, unsafe_ios::seek_dir d)
{
	STREAM_RDBUF_LOCK(unsafe_ostream) 
	unsafe_ostream::seekp(o,d);
	return *this;
}

inline int ostream::opfx()
{
  STREAM_ONLY_LOCK()
  return unsafe_ostream::opfx();
}

inline void ostream::osfx()
{
  STREAM_ONLY_LOCK()
  unsafe_ostream::osfx();
}

inline ostream& ostream::operator<<(char c)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<(c);
  return *this;
}

inline ostream&	ostream::operator<<(unsigned char c)
{
  return *this << (char)c;
}

inline ostream& ostream::operator<<(const char*s)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<(s);
  return *this;
}

/*	ostream&	ostream::operator<<(const void*);   add this later */

inline ostream&	ostream::operator<<(short i)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<((long) i);
  return *this;
}

inline ostream&	ostream::operator<<(unsigned short i)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<((unsigned long) i);
  return *this;
}

inline ostream& ostream::operator<<(long l)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<(l);
  return *this;
}

inline ostream& ostream::operator<<(unsigned long l)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<(l);
  return *this;
}
	
inline ostream& ostream::operator<<(int x)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<((long) x);
  return *this;
}

inline ostream& ostream::operator<<(unsigned int x)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<((unsigned long) x);
  return *this;
}

inline ostream& ostream::operator<<(void* v)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::operator<<(v);
  return *this;
}

inline ostream&	ostream::operator<< (ostream& (*f)(ostream&))
{
  STREAM_ONLY_LOCK()
  (*f)(*this);
  return *this;
}

inline ostream&	ostream::write(const char*  s,int n)
{
  STREAM_RDBUF_LOCK(unsafe_ostream)
  unsafe_ostream::write(s, n);
  return *this;
}

inline ostream&	ostream::write(const unsigned char* s, int n)
{
  return write((const char*) s, n);
}


class unsafe_iostream : public unsafe_istream, public unsafe_ostream {
public:
			unsafe_iostream(streambuf*) ;
	virtual		~unsafe_iostream() ;
protected:
			unsafe_iostream() ;
	} ;


class iostream : public istream, public ostream {
public:
			iostream(streambuf*) ;
	virtual		~iostream() ;
protected:
			iostream() ;
	} ;

inline iostream::iostream(streambuf* sb) {
  STREAM_ONLY_LOCK()
  RDBUF_ONLY_LOCK()
  init(sb) ; 
}

class istream_withassign : public istream {
public:
			istream_withassign() ;
	virtual		~istream_withassign() ;
	istream_withassign&	operator=(istream&) ;
	istream_withassign&	operator=(streambuf*) ;
} ;

inline istream_withassign::istream_withassign() 
{
	STREAM_ONLY_LOCK()
	// In order for the standard streams to be properly initialized
	// it is essential that nothing is done by the combination
	// of this constructor and unsafe_ios::unsafe_ios().  So we undo the effect of
	// unsafe_ios::unsafe_ios() 
	state = assign_private ;
}

inline istream_withassign& istream_withassign::operator=(istream& s)
{
	init(s.rdbuf()) ;
	return *this ;
}

inline istream_withassign& istream_withassign::operator=(streambuf* sb)
{
	init(sb) ;
	return *this ;
}

class ostream_withassign : public ostream {
public:
			ostream_withassign() ;
	virtual		~ostream_withassign() ;
	ostream_withassign&	operator=(ostream&) ;
	ostream_withassign&	operator=(streambuf*) ;
} ;

class iostream_withassign : public iostream {
public:
			iostream_withassign() ;
	virtual		~iostream_withassign() ;
	iostream_withassign&	operator=(unsafe_ios&) ;
	iostream_withassign&	operator=(streambuf*) ;
} ;

inline ostream_withassign::ostream_withassign() 
{
	// In order for the standard streams to be properly initialized
	// it is essential that nothing is done by the combination
	// of this constructor and unsafe_ios::unsafe_ios().  So we undo the effect of
	// unsafe_ios::unsafe_ios() 
	STREAM_ONLY_LOCK()
	state = assign_private ;
}

inline ostream_withassign& ostream_withassign::operator=(ostream& s)
{
	init(s.rdbuf()) ;
	return *this ;
}

inline ostream_withassign& ostream_withassign::operator=(streambuf* sb)
{
	init(sb) ;
	return *this ;
	}

inline iostream_withassign::iostream_withassign() 
{
	// In order for the standard streams to be properly initialized
	// it is essential that nothing is done by the combination
	// of this constructor and ios::ios().  So we undo the effect of
	// ios::ios() 
	STREAM_ONLY_LOCK()
	state = assign_private ;
}

inline iostream_withassign& iostream_withassign::operator=(unsafe_ios& s)
{
	init(s.rdbuf()) ;
	return *this ;
}

inline iostream_withassign& iostream_withassign::operator=(streambuf* sb)
{
	init(sb) ;
	return *this ;
}

extern istream_withassign cin ;
extern ostream_withassign cout ;
extern ostream_withassign cerr ;
extern ostream_withassign clog ;

unsafe_ios&	dec(unsafe_ios&) ; 
ios&		dec(ios&) ; 
unsafe_ostream&	endl(unsafe_ostream& i) ;
unsafe_ostream&	ends(unsafe_ostream& i) ;
unsafe_ostream&	flush(unsafe_ostream&) ;
ostream&	endl(ostream& i) ;
ostream&	ends(ostream& i) ;
ostream&	flush(ostream&) ;
unsafe_ios&	hex(unsafe_ios&) ;
unsafe_ios&	oct(unsafe_ios&) ; 
ios&		hex(ios&) ;
ios&		oct(ios&) ; 
unsafe_istream&	ws(unsafe_istream&) ;
istream&	ws(istream&) ;

static class Iostream_init {
	static int	stdstatus ; /* see cstreams.c */
	static int	initcount ;
	friend		ios ;
	friend		unsafe_ios ;
public:
	Iostream_init() ; 
	~Iostream_init() ; 
} iostream_init ;	

}
#endif


#ifndef SYSTIMEH
#define SYSTIMEH
extern "C++" {
#include <cc/sys/time.h>

extern "C" {
    int gettimeofday(struct timeval *);
    int settimeofday(struct timeval *);

}
}
#endif /* SYSTIMEH */

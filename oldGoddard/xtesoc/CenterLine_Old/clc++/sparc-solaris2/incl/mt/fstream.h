/*ident	"@(#)cls4:incl-master/const-headers/fstream.h	1.3" */
/*******************************************************************************
 
C++ source for the C++ Language System, Release 3.0.  This product
is a new release of the original cfront developed in the computer
science research center of AT&T Bell Laboratories.

Copyright (c) 1991, 1992 AT&T and UNIX System Laboratories, Inc.
Copyright (c) 1984, 1989, 1990 AT&T.  All Rights Reserved.

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of AT&T and UNIX System
Laboratories, Inc.  The copyright notice above does not evidence
any actual or intended publication of such source code.

*******************************************************************************/
#ifndef FSTREAMH
#define FSTREAMH
extern "C++" {

#include <iostream.h>

class  filebuf : public streambuf {	/* a stream buffer for files */
public:
	static const int openprot ; /* default protection for open */
public:
			filebuf() ;
			filebuf(int fd);
			filebuf(int fd, char*  p, int l) ;
			~filebuf() ;

	int		fd();
	// The safe version of the member functions
	int		is_open();
	filebuf*	open(const char *name, int om, int prot=openprot);
	filebuf*	attach(int fd) ;
	int		detach() ;
	filebuf* 	close() ;
	// The unsafe verison of the member functions
 	int 		is_open_unlocked() { return opened ;} 	
	filebuf*	open_unlocked(const char* name,int om,
					int prot=openprot);	
	filebuf* 	attach_unlocked(int);
	int		detach_unlocked();
  	filebuf*	close_unlocked();
	// end 
	
public: /* virtuals */
	virtual int	overflow(int=EOF);
	virtual int	underflow();
	virtual int	sync() ;
	virtual streampos
			seekoff(streamoff,ios::seek_dir,int) ;
	virtual streambuf*
			setbuf(char*  p, int len) ;
protected:
	int		xfd;	
	int		mode ;
	char		opened;
	streampos	last_seek ;
	char* 		in_start;
	int		last_op();
	char		lahead[2] ;
};

inline int filebuf::is_open() {
#if _REENTRANT_STREAMS
  	stream_locker  strmlock(this, stream_locker::lock_defer);
  	if (test_safe_flag()) strmlock.lock();
#endif
	return opened;	
}

inline streambuf* filebuf::setbuf(char* p , int len)
{
	STREAM_ONLY_LOCK()
	streambuf *retstream;
	if ( is_open() && base_unlocked() ) {
	  retstream = 0 ;
	} else {
	  // Note the special case of allowing buffering to be turned
	    // on even for an already opened filebuf.
	      setb(0,0) ;
	  retstream = streambuf::setbuf(p,len) ;
	}
	return retstream;
}

inline int filebuf::fd() {
	STREAM_ONLY_LOCK()
	return xfd;
}
	
inline filebuf* filebuf::close() {
        STREAM_ONLY_LOCK()
        return filebuf::close_unlocked();
}
 
inline filebuf* filebuf::open(const char* name, int om, int prot) {
        STREAM_ONLY_LOCK()
        return filebuf::open_unlocked(name, om, prot);
}
 
inline filebuf* filebuf::attach(int p) {
        STREAM_ONLY_LOCK()
        return filebuf::attach_unlocked(p);
}
 
inline int filebuf::detach() {
        STREAM_ONLY_LOCK()
        return filebuf::detach_unlocked();
}

					// changes for MT-safe
class unsafe_fstreambase : virtual public unsafe_ios 
{ 
public:
			unsafe_fstreambase() ;
	
			unsafe_fstreambase(const char* name, 
					int mode,
					int prot=filebuf::openprot) ;
			unsafe_fstreambase(int fd) ;
			unsafe_fstreambase(int fd, char*  p, int l) ;
			~unsafe_fstreambase() ;
	void		open(const char* name, int mode, 
					int prot=filebuf::openprot) ;
	void		attach(int fd);
	int		detach();
	void		close() ;
	void		setbuf(char*  p, int len) ;
	filebuf*	rdbuf() { return &buf ; }
private:
	filebuf		buf ;
	friend class fstreambase;
protected:
	void		verify(int) ;   // not implemented
} ;
					// end of changes for MT-safe

class fstreambase : virtual public ios, public unsafe_fstreambase 
{ 
public:
			fstreambase() ;
			fstreambase(const char* name, 
					int mode,
					int prot=filebuf::openprot) ;
			fstreambase(int fd) ;
			fstreambase(int fd, char*  p, int l) ;
			~fstreambase() ;

	void		open(const char* name, int mode, 
					int prot=filebuf::openprot) ;
	void		attach(int fd);
	int		detach();
	void		close() ;
	void		setbuf(char*  p, int l) ;
	filebuf*	rdbuf(); 
};
 
inline void fstreambase::open(const char* name, int mode, int prot) {
        STREAM_ONLY_LOCK()
        unsafe_fstreambase::open(name, mode, prot);
}
 
inline void fstreambase::attach(int fd) {
        STREAM_ONLY_LOCK()
        unsafe_fstreambase::attach(fd);
}
 
inline void fstreambase::close() {
        STREAM_ONLY_LOCK()
        unsafe_fstreambase::close();
}


inline void fstreambase::setbuf(char* p, int len) {
        STREAM_ONLY_LOCK()
        unsafe_fstreambase::setbuf(p,len);
}
 

inline filebuf* fstreambase::rdbuf() {
#if _REENTRANT_STREAMS
  	stream_locker  strmlock(this, stream_locker::lock_defer);
  	if (test_safe_flag()) strmlock.lock();
#endif
	return unsafe_fstreambase::rdbuf();
}
	
class ifstream : public fstreambase, public istream {
public:
			ifstream() ;
			ifstream(const char* name, 
					int mode=ios::in,
					int prot=filebuf::openprot) ;
			ifstream(int fd) ;
			ifstream(int fd, char*  p, int l) ;
			~ifstream() ;

	filebuf*	rdbuf() { return fstreambase::rdbuf(); }
	void		open(const char* name, int mode=ios::in, 
					int prot=filebuf::openprot) ;
} ;

class ofstream : public fstreambase, public ostream 
{
public:
			ofstream() ;
			ofstream(const char* name, 
					int mode=ios::out,
					int prot=filebuf::openprot) ;
			ofstream(int fd) ;
			ofstream(int fd, char*  p, int l) ;
			~ofstream() ;

	filebuf*	rdbuf() { return fstreambase::rdbuf(); }
	void		open(const char* name, int mode=ios::out, 
					int prot=filebuf::openprot) ;
} ;


class fstream : public fstreambase, public iostream 
{
public:
			fstream() ;
	
			fstream(const char* name, 
					int mode,
					int prot=filebuf::openprot) ;
			fstream(int fd) ;
			fstream(int fd, char*  p, int l) ;
			~fstream() ;
	filebuf*	rdbuf() { return fstreambase::rdbuf(); }
	void		open(const char* name, int mode, 
				int prot=filebuf::openprot) ;
} ;

}
#endif

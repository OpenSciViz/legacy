/*ident	"@(#)cls4:incl-master/const-headers/strstream.h	1.1" */
/*******************************************************************************
 
C++ source for the C++ Language System, Release 3.0.  This product
is a new release of the original cfront developed in the computer
science research center of AT&T Bell Laboratories.

Copyright (c) 1991 AT&T and UNIX System Laboratories, Inc.
Copyright (c) 1984, 1989, 1990 AT&T.  All Rights Reserved.

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of AT&T and UNIX System
Laboratories, Inc.  The copyright notice above does not evidence
any actual or intended publication of such source code.

*******************************************************************************/
#ifndef STRSTREAMH
#define STRSTREAMH
extern "C++" {

#ifndef IOSTREAMH
#include <iostream.h>
#endif

class strstreambuf : public streambuf
{
public: 
			strstreambuf() ;
			strstreambuf(int) ;
			strstreambuf(void* (*a)(long), void (*f)(void*)) ;
			strstreambuf(char* b, int size, char* pstart = 0 ) ;
			strstreambuf(unsigned char* b, int size, unsigned char* pstart = 0 ) ;
	int		pcount_unlocked();
	int		pcount();
	void 		freeze_unlocked(int=1);
	void		freeze(int n=1) ;
	int 		isfrozen_unlocked();
	int 		isfrozen();
	char*		str_unlocked();
	char*		str() ;
			~strstreambuf() ;

public: /* virtuals  */
	virtual int	doallocate() ;
	virtual int	overflow(int) ;
	virtual int	underflow() ;
	virtual streambuf*
			setbuf(char*  p, int l) ;
	virtual streampos
			seekoff(streamoff,ios::seek_dir,int) ;

private:
	void		init(char*,int,char*) ;

	void*		(*afct)(long) ;
	void		(*ffct)(void*) ;
	int		ignore_oflow ;
	int		froozen ;
	int		auto_extend ;
} ;

inline strstreambuf::isfrozen_unlocked() { return froozen ; }

inline strstreambuf::isfrozen()
{
#if _REENTRANT_STREAMS
  stream_locker  strmlock(this, stream_locker::lock_defer);
  if (test_safe_flag()) strmlock.lock();
#endif
  return isfrozen_unlocked() ; 
}

inline int strstreambuf::pcount(){
	STREAM_ONLY_LOCK()
	return pcount_unlocked();
}

class unsafe_strstreambase : public virtual unsafe_ios { 
public: 
	strstreambuf* rdbuf();
protected:
	unsafe_strstreambase(char*, int, char*);
	unsafe_strstreambase();
	~unsafe_strstreambase();
private:
	strstreambuf buf;
};

class strstreambase : virtual public ios, public unsafe_strstreambase {
public:
	strstreambuf*	rdbuf() ;
protected:	
			strstreambase(char*, int, char*) ;
			strstreambase() ;
			~strstreambase() ;
} ;

class istrstream : public strstreambase, public istream {
public:
			istrstream(char* str);
			istrstream(char* str, int size ) ;
			istrstream(const char* str);
			istrstream(const char* str, int size);
			~istrstream() ;
} ;

inline strstreambuf* strstreambase::rdbuf() 
{ 
  STREAM_ONLY_LOCK()
  return unsafe_strstreambase::rdbuf() ; 
}

class ostrstream : public strstreambase, public ostream {
public:
			ostrstream(char* str, int size, int=ios::out) ;
			ostrstream() ;
			~ostrstream() ;
	char*		str() ;
	int		pcount() ;
} ;

inline ostrstream::~ostrstream()
{
	STREAM_RDBUF_LOCK(strstreambase)
	if (!unsafe_strstreambase::rdbuf()->isfrozen())
		unsafe_ios::rdbuf()->sputc(0) ;
}


class strstream : public strstreambase, public iostream {
public:
			strstream() ;
			strstream(char* str, int size, int mode) ;
			~strstream() ;
	char*		str() ;
} ;

inline int ostrstream::pcount() {
	STREAM_RDBUF_LOCK(strstreambase)
	return unsafe_strstreambase::rdbuf()->pcount();
}

}
#endif


						January 10, 1996


cfront Patch 3.0 Information/Installation Note (P3_SOL_CFRONT_211.README)
   FOR ObjectCenter Version 2.1.1

Note:	Install this patch only if you have already installed
	ObjectCenter Version 2.1.1


What the Patch Fixes:
--------------------

  o Bug number 15003 -  vec_new/vec_delete use too much memory

    Note: set the CENTERLINE_OLD_VEC_NEW environment variable when
          running to get the old vec_new/vec_delete behavior.

*** The following were fixed in Patch 2 and are also included in Patch 3 ***

  o Bug number 07846 -  Second -ptr repository is not read-only

  o Bug number 13434 -  Ptlink does not work correctly with nmake

  o Bug number 13983 -  Explicit call to destructor requires an object

*** The following was fixed in Patch 1 and is also included in Patch 2 ***

  o Bug number 13260 -  CC[ptlink] fatal error:
                        popstring() called without pushstring()


How to get the Patch:
--------------------

The patch is available from CenterLine via ftp:

o	Get the compressed tar file from ftp.centerline.com:

	% ftp ftp.centerline.com
	    cd patch/cfront
	    get P3_SOL_CFRONT_211.README (this is the file you are reading)
	    binary
	    get P3_SOL_CFRONT_211.tar.Z
	    quit

o	Uncompress the compressed tar file:

	% uncompress P3_SOL_CFRONT_211.tar.Z


Contents of the Patch:
---------------------

This patch contains the following files:

	clc++/sparc-solaris2/.relnum
	clc++/sparc-solaris2/cfront
	clc++/sparc-solaris2/cfront.orig
	clc++/sparc-solaris2/ptlink
	clc++/sparc-solaris2/ptlink.orig

	sparc-solaris2/lib/a0/libC.a
	sparc-solaris2/lib/a0/libC.a.orig
	sparc-solaris2/lib/a0/libC.so
	sparc-solaris2/lib/a0/libC.so.orig
	sparc-solaris2/lib/a0/libC.so.3.2
	sparc-solaris2/lib/a0/libC_mt.so
	sparc-solaris2/lib/a0/libC_mt.so.orig
	sparc-solaris2/lib/a0/libC_mt.so.3.2
	sparc-solaris2/lib/a0/libC_p.a
	sparc-solaris2/lib/a0/libC_p.a.orig
	sparc-solaris2/lib/a0/libCdynamic.so
	sparc-solaris2/lib/a0/libCdynamic.so.orig
	sparc-solaris2/lib/a0/libCdynamic.so.3.2

	sparc-solaris2/lib/a1/libC.a
	sparc-solaris2/lib/a1/libC.a.orig
	sparc-solaris2/lib/a1/libC.so
	sparc-solaris2/lib/a1/libC.so.orig
	sparc-solaris2/lib/a1/libC.so.3.2
	sparc-solaris2/lib/a1/libC_mt.so
	sparc-solaris2/lib/a1/libC_mt.so.orig
	sparc-solaris2/lib/a1/libC_mt.so.3.2
	sparc-solaris2/lib/a1/libC_p.a
	sparc-solaris2/lib/a1/libC_p.a.orig
	sparc-solaris2/lib/a1/libCdynamic.so
	sparc-solaris2/lib/a1/libCdynamic.so.orig
	sparc-solaris2/lib/a1/libCdynamic.so.3.2

The files with the ".orig" extensions are copies of the files that
had been installed originally.  If you have problems with the patch
binaries, move these files back to their original names (see
"Deinstalling the patch" below).


Installing the Patch:
--------------------

To install this patch, do the following:

1       Have all ObjectCenter Version 2.1.1 avoid using the C++
	compiler so that the patch may be installed.

2       Login or su as the user who had previously installed ObjectCenter
        Version 2.1.1.

3       Change directories to where ObjectCenter Version 2.1.1 had been
        previously installed:

                % cd /PATH-TO/CenterLine

        (where PATH-TO is the path to your CenterLine directory)

4        Extract the files from the tar file:

                % tar xvpf /path-to-tar-file/P2_SOL_CFRONT_211.tar

5        The patch is now installed.


Deinstalling the Patch:
----------------------

If there is a problem with the patch and you need to revert back to
the previous version, do the following:

1       Have all ObjectCenter Version 2.1.1 avoid using the C++
	compiler so that the patch may be deinstalled.

2       Login or su as the user who had previously installed the patch.

3       Change directories to where patch had been installed:

                % cd /PATH-TO/CenterLine

        (where PATH-TO is the path to your CenterLine directory)

4	remove all the libraries with the "3.2" extension

	rm sparc-solaris2/lib/a0/libC.so.3.2
	rm sparc-solaris2/lib/a0/libC_mt.so.3.2
	rm sparc-solaris2/lib/a0/libCdynamic.so.3.2
	rm sparc-solaris2/lib/a1/libC.so.3.2
	rm sparc-solaris2/lib/a1/libC_mt.so.3.2
	rm sparc-solaris2/lib/a1/libCdynamic.so.3.2


5	Move the files with the ".orig" extension to the files without
	the '.orig' extension:

	mv clc++/sparc-solaris2/cfront.orig clc++/sparc-solaris2/cfront
	mv clc++/sparc-solaris2/ptlink.orig clc++/sparc-solaris2/ptlink

	mv sparc-solaris2/lib/a0/libC.a.orig sparc-solaris2/lib/a0/libC.a
	mv sparc-solaris2/lib/a0/libC.so.orig sparc-solaris2/lib/a0/libC.so
	mv sparc-solaris2/lib/a0/libC_mt.so.orig sparc-solaris2/lib/a0/libC_mt.so
	mv sparc-solaris2/lib/a0/libC_p.a.orig sparc-solaris2/lib/a0/libC_p.a
	mv sparc-solaris2/lib/a0/libCdynamic.so.orig sparc-solaris2/lib/a0/libCdynamic.so

	mv sparc-solaris2/lib/a1/libC.a.orig sparc-solaris2/lib/a1/libC.a
	mv sparc-solaris2/lib/a1/libC.so.orig sparc-solaris2/lib/a1/libC.so
	mv sparc-solaris2/lib/a1/libC_mt.so.orig sparc-solaris2/lib/a1/libC_mt.so
	mv sparc-solaris2/lib/a1/libC_p.a.orig sparc-solaris2/lib/a1/libC_p.a
	mv sparc-solaris2/lib/a1/libCdynamic.so.orig sparc-solaris2/lib/a1/libCdynamic.so

6	Please report the problem to CenterLine Technical Support


NOTES:
-----

o	To save space once you have verified that the patch is working
	correctly for your group, you can remove the ".orig" files from
	your installation:

	rm clc++/sparc-solaris2/cfront.orig
	rm clc++/sparc-solaris2/ptlink.orig

	rm sparc-solaris2/lib/a0/libC.a.orig
	rm sparc-solaris2/lib/a0/libC.so.orig
	rm sparc-solaris2/lib/a0/libC_mt.so.orig
	rm sparc-solaris2/lib/a0/libC_p.a.orig
	rm sparc-solaris2/lib/a0/libCdynamic.so.orig

	rm sparc-solaris2/lib/a1/libC.a.orig
	rm sparc-solaris2/lib/a1/libC.so.orig
	rm sparc-solaris2/lib/a1/libC_mt.so.orig
	rm sparc-solaris2/lib/a1/libC_p.a.orig
	rm sparc-solaris2/lib/a1/libCdynamic.so.orig

	where all paths are relative to the CenterLine directory.


#
# Use this license.opts file as a template to activate (reserve) 
# the names of your named-user licenses with the RESERVE keyword.
#
# The syntax of the RESERVE command line is as follows:
#
#     RESERVE number product platform USER username
#
# where:
#
# number        is the number of licenses reserved for the specified user.
#
# product       is the product that you are reserving. It can be one 
#		of the following: 
#
#               CodeCenter 	for CodeCenter
#		ObjectCenter 	for ObjectCenter 
#		CenterLine-C++  for CenterLine-C++
#		CLCC 		for CenterLine-C compiler
#		CLC++ 		for CenterLine-C++ compiler
#		TestCenter	for TestCenter
#		VCTRM       	for ViewCenter
#
# platform	is the platform on which this licensed software will run. 
#		It can be one of the following:
#
#		+i486-svr4 	for NCR systems  (NCR 3000 series UNIX SVR4)
#		+m88k_u		for M88k systems (Motorola SVR4)
#		+pa_u 		for HP systems   (HP9000s700)
#		+sparc_u 	for Sun systems  (SPARCstations)
#
# name          is the UNIX username of the user who owns this license.
#
#
# For example, the following "commented-out" RESERVE lines reserve:
#
#  o  One CodeCenter and CenterLine-C compiler SPARC-user license for
#       the user, smythe.
#  o  One CodeCenter HP-user license for the user, mueller.
#  o  Two ObjectCenter and CenterLine-C++ compiler HP-user licenses for
#       the user, lewis.
#  o  One ObjectCenter, CenterLine-C++ compiler and CenterLine-C compiler
#       SPARC-user license for the user, jones.
#  o  One CodeCenter M88k-user license for the user, barker.
#  o  One CodeCenter NCR-user license for the user, burnett.
#  o  One ObjectCenter NCR-user license for the user, burnett.
#  o  One ObjectCenter and CenterLine-C++ M88k-user licenses for
#       the user, scott.
#  o  One CenterLine-C++ and CenterLine-C++ compiler SPARC-user license for
#       the user, moe.
#  o  Two ViewCenter for Motif SPARC-user license for the user, larry.
#  o  One ViewCenter for Motif HP-user license for the user, curley.
#  o  One ViewCenterPro SPARC-user license for the user, moe.
#  o  One ViewCenterPro HP-user license for the user, shemp.
#  o  One TestCenter SPARC-user license for the user, moe.
#  o  One TestCenter HP-user license for the user, shemp.
#
# RESERVE 1 CodeCenter+sparc_u USER smythe
# RESERVE 1 CLCC+sparc_u USER smythe
#
# RESERVE 1 CodeCenter+pa_u USER mueller
#
# RESERVE 2 ObjectCenter+pa_u USER lewis
# RESERVE 2 CLC+++pa_u USER lewis
#
# RESERVE 1 ObjectCenter+sparc_u USER jones
# RESERVE 1 CLC+++sparc_u USER jones
# RESERVE 1 CLCC+sparc_u USER jones
#
# RESERVE 1 CodeCenter+m88k_u USER barker
#
# RESERVE 1 CodeCenter+ncr3000_u USER burnett
# RESERVE 1 ObjectCenter+ncr3000_u USER burnett
#
# RESERVE 1 ObjectCenter+m88k_u USER scott
# RESERVE 1 CLC+++m88k_u USER scott
#
# RESERVE 1 CenterLine-C+++sparc_u USER moe
# RESERVE 1 CLC+++sparc_u USER moe
#
# RESERVE 2 VCTRM+sparc_u USER larry
# RESERVE 1 VCTRM+pa_u USER curley
#
# RESERVE 1 VCPRO+sparc_u USER moe
# RESERVE 1 VCPRO+pa_u USER shemp

# RESERVE 1 TestCenter+sparc_u USER moe
# RESERVE 1 TestCenter+pa_u USER shemp
#
# Commented lines begin with #'s. Enter your names using the examples
# above as a template. You must provide a RESERVE line for each named-user
# license you have; otherwise, the license server daemon cannot grant a 
# license.
#
# See the manpage, 'CenterLine/man/man5/license.opt.5' for additional ways
# to use this file.
#
#

.TH CLMS_GATEWAY 1 "May 1993" "CenterLine" "CenterLine Manual Pages"
.SH NAME
clms_gateway, clms_ttgateway, clms_sbgateway \-
.br
CLIPC <-> {ToolTalk,SoftBench} message gateway
.SH SYNOPSIS
.B clms_gateway\ [-p]\ [-v]\ [-i]\ [-c\fIclipc_session\fR]
.br
.B [-t\fItooltalk_session\fR]
.B [-s\fItoolclass\fR]\ [\fIdefinition_file\fR]
.SH IMPLEMENTATION STATE
Translations have been built and tested for a sunos4 ToolTalk
version of the gateway, as well as an hpux SoftBench version.
See the
.B DEMONSTRATIONS
section for info on running these demos.
.PP
The gateway has only been tested for these demos.
It is not likely to be able to support all permutations
of translations for all messaging domains.
.PP
The parser for the tdl language is not of production quality.  It should
catch all syntax errors, but not necessarily with a very specific
message.  This limitation is consistent with the fact that the language
will probably only ever be used to write a handful of translation
definitions.
.SH DESCRIPTION
Provides a message gateway between a clms message server and a ToolTalk
message server.
The gateway may be used to provide access to CLIPC-based
products such as CodeCenter and ObjectCenter from the ToolTalk
or SoftBench messaging domains.
Translations are specified by translation rules,
written in Translation Definition Language (TDL).
Clms_gateway starts up, reads in the argument file
specifying message translations, joins a clms session and joins a
ToolTalk and/or SoftBench session.
It registers to handle and listen to specified messages in the domains
gatewayed.  Upon receipt of any
of these messages, a list of actions associated with the
received message is executed.
.PP
Optional invocation flags include:
.TP
.B \-p
Parse translation specification file only.  The argument file
contains a translation specification written in the translation
specification language described below.  This flag tells the
executable to just parse the specification, but not to actually
begin doing the translations.  It is used while debugging a
translation specification.
.TP
.B \-v
Be verbose about the state of the gateway.  Status messages are
sent to
.B stdout
indicating the current state of the gateway.
An indication is sent each time a message is read or written.
.TP
.B \-i
Look at standard input for special targets.  Normal operation
of the gateway is to perform actions specified for a target
message when the message is received by the gateway.  If this
flag is set, the gateway also looks at the standard input for
target words as well, and executes the actions specified for
those targets when they are received.  This is useful
in automating testing of a gateway.
.TP
.B \-c\fIclipc_session\fR
Specifies the CLIPC session to join.  If not
specified, the default session will be joined.  See documentation
on the clms and clms_query commands for more info on the default
session and the format of the string.
.TP
.B \-t\fItooltalk_session\fR
Specifies the ToolTalk session to join.  If not
specified, the default session will be joined.  See "ToolTalk
Programmer's Guide" for info on the default session and the
format of the string.
.TP
.B \-s\fItool_class\fR
Specifies the SoftBench default tool class value.  This is pretty
much required for all SoftBench gateways, as this value finds its
way into the patterns specifying messages the gateway will receive.
See "SoftBench Encapsulator: Programmer's Guide"
for info on the tool class.
.PP
The gateway begins execution by reading in the argument translation
specification file.  If no file is specified on the command line, the
file "xlate.tdl" will be read by default.  This file is organized as a
series of rules.  Each rule consists of a target, which specifies a
message (and, implicitly, some indication of its error state) and a
list of actions.  Execution continues with the gateway translating
messages as they are received according to the rules in this file. In
the simplest case (where there is no transaction type specified) each
time a message matching a target is received, the executable statements
associated with the target are executed.  For example, the following
TDL rule will be invoked each time the
gateway receives a ToolTalk message of the type ToolTalk_Test_Message:
.PP
.nf
    // ToolTalk ToolTalk_Test_Message notification
    //
    // NOTIFICATION ToolTalk_Test_Message IS (
    //    IN string user_message;
    // )

    TT, NOTIFICATION, ToolTalk_Test_Message:
        alloc(MSG_CLIPC_NOTIFICATION, clipc_test_msg);
        STRING MSG_CLIPC_NOTIFICATION.user_msg = MSG_TARGET.0;
        send(MSG_CLIPC_NOTIFICATION);
.fi
.PP
The target line of the rule specifies the message domain (TT, for
"ToolTalk"), the delivery class of the message (NOTIFICATION), and the
message type name (ToolTalk_Test_Message).  The actions associated with
the target in the rule will be executed each time a message which
matches the target specification is received by the gateway.  In this
example, a CLIPC message will be allocated and a field named "user_msg"
in the CLIPC message will be assigned the value of the 0th field of the
ToolTalk target message, and the CLIPC message will be sent.
.PP
.B Message Transactions
.PP
The translation specification facility maintains the concept of a
message transaction,
the scope of which may include the receiving and sending of an
arbitrary number of messages.  A transaction is bounded by the receipt
of a target message which has been specified as the initial target for a
transaction, and a target message specified as the terminal target of
the transaction.  Consider the following sample translation
specification:
.PP
.nf
    // ToolTalk Do_Command request transaction
    //
    // REQUEST Do_Command IS (
    //    IN string command_to_execute;
    //    OUT string result;     Always empty!
    // )

    TT, REQUEST, Do_Command, Do_Cmd_Transaction_Type,
    INITIAL:
        alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
        STRING MSG_CLIPC_REQUEST.commandLine = MSG_TARGET.0;
        INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = TRUE;
        INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = TRUE;
        send(MSG_CLIPC_REQUEST);

    CLIPC, NOTIFICATION, event_ready, Do_Cmd_Transaction_Type,
    TERMINAL:
        STRING MSG_TT_REQUEST.1 = "";
        send(MSG_TT_REQUEST);
.fi
.PP
The transaction type specified here is named
"Do_Cmd_Transaction_Type".  The ToolTalk request message type
"Do_Command" is the initial target message type of the transaction.
When a message of the type "Do_Command" is received, and it does not
carry a transaction ID (indicating that it is not already a part of an
existing transaction), the gateway initiates a new transaction,
initializes the new transaction state, and adds the transaction to its
list of active transactions.  In the example above, the actions
associated with this target message build and send a CLIPC request
message.  Since the message is sent in the context of a transaction, its
envelope is tagged with the transaction ID of the transaction.  It is
assumed that all CLIPC message clients will propagate this ID to all
messages related to the request.  This is a safe assumption for the
*Center executives, which are currently the only CLIPC message clients
which handle CLIPC requests.  Transaction IDs found in request messages
will be propagated to both the reply messages of the requests and to the
event_ready notification which signals the executive is done with
certain commands.  Outgoing ToolTalk
and SoftBench messages associated with a
transaction are currently NOT tagged with the transaction ID as the
ToolTalk and SoftBench
interface libraries have no provisions for envelope manipulation,
and the body fields are sequential (rather than named like CLIPC message
fields) so attempts to add additional fields may not be safe.  This
effectively reduces the gateway to serving requests made in the ToolTalk
or SoftBench message domain using services accessed in the CLIPC domain
if transaction scope must be maintained for the translation.
Atomic translations,
where a message in one domain is simply converted to a message
in another domain,
can be handled for any domain.
.PP
The next rule in the example is for the reply to the sent
CLIPC request message.  When a message of the named type is received and
its envelope contains a transaction ID, its transaction ID is checked
against the gateway's list of active transactions.  If a match is found,
and the active transaction matched is of the transaction type named in
this target, then the target is considered to be matched and its actions
are executed.  Note that the target in this rule is specified as the
TERMINAL rule in the transaction.  After the actions in the rule are
executed the transaction's state is deleted, its transaction ID is
retired, and it is removed from the gateway's active transaction list.
.PP
.B Transaction State
.PP
Each active transaction maintains a transaction state.  This includes
a set of the following message variables:
.TP
.B MSG_TT_REQUEST
Used to hold a ToolTalk request/reply message.
.TP
.B MSG_TT_NOTIFICATION
Used to hold a ToolTalk notification message.
.TP
.B MSG_SB_REQUEST
Used to hold a SoftBench request message.
.TP
.B MSG_SB_FAILURE
Used to hold a SoftBench failure message.
.TP
.B MSG_SB_NOTIFICATION
Used to hold a SoftBench notification message.
.TP
.B MSG_CLIPC_REQUEST
Used to hold a CLIPC request message.
.TP
.B MSG_CLIPC_REPLY
Used to hold a CLIPC reply message.
.TP
.B MSG_CLIPC_NOTIFICATION
Used to hold a CLIPC notification message.
.PP
In addition, the alias
.B MSG_TARGET
indicates the message variable which contains the target message
for the current rule.
.PP
Each time a target message is matched, it is copied into the appropriate
message variable before any actions are executed.  Likewise each time an
action explicitly allocates a message (using the
.B alloc()
procedure) the
newly allocated and empty message is copied into the named message
variable.
In both of these cases any message in the variable before the
copy is freed up and therefore lost.
.PP
.B Targets
.PP
There are a number of classes of target specifications.  Those that name
a transaction type are considered matched only if the message contains
the transaction ID of an active transaction, as described above.  Of
these, there are three subclasses- targets marked as the initial target
of a transaction type; targets marked as the terminal target of a
transaction type; and (unmarked) member targets of a transaction type.
The former two have been discussed above.  A member target is simply a
target which names a transaction type but is not an initial or terminal
target for the transaction.
.PP
It is also possible to specify a target without a transaction type.
Rules associated with these targets are executed for matching messages
which are received and do not have transaction IDs in their envelopes.
Targets of this form are generally used to handle notification messages
which may be emitted in the context of any number of transactions, or
not associated with any transaction, but which are handled the same in
all cases.
.PP
Target specifications take as their second field a specification of
the delivery class of the message.  These are:
.TP
.B REQUEST
Specifies only a request message should be
matched.  In the ToolTalk domain, this further
indicates the message must be in state TT_SENT.
.TP
.B REPLY
Specifies only a reply message should be
matched.  In the ToolTalk domain, this specifies
a request message in state TT_HANDLED.
There are no reply message semantics supported in
the SoftBench domain.
.TP
.B NOTIFICATION
Specifies only a notification message should
be matched.
.TP
.B ERROR_REPLY
Specifies only the reply to a failed request
message should be matched.  In the CLIPC domain,
this means a reply message with a non-zero
integer
.B status
field in its body.
In the ToolTalk
domain, this means a request message in state
TT_FAILED or TT_REJECTED.
There are no error reply message semantics supported
in the SoftBench domain.
.TP
.B FAILURE
Specifies only a SoftBench failure message
should be matched.
.PP
.B Special Targets
.PP
There are two types of "special", non-message targets.  These are
specified by substituting the message domain specification part of the
target specification for either
.B START
or
.B STDIN.
The first case
specifies a startup rule, which will be executed once, when the gateway
is first invoked.  There should be only one such rule in a translation
definition file.  The second case specifies a rule which is useful for
debugging.  Such rules are only ever matched if the
.B \-i
flag is given on
the gateway invocation command line.  If that flag is present, the
gateway will not only read messages from the CLIPC,
SoftBench,  and ToolTalk
messaging domains,
but will also look at standard input for words which
match the name appearing in the message name field of the target
specification.  For example, if the rule:
.PP
.nf
    // ToolTalk Do_Command request transaction
    STDIN, NOTIFICATION, Do_Command_Transaction_Type:
        echo("Starting Test: Do_Command_Transaction_Type");
        alloc(MSG_TT_REQUEST, Do_Command);
        STRING MSG_TT_REQUEST.0 = "list";
        STRING MSG_TT_REQUEST.1 = "";
        send(MSG_TT_REQUEST);
.fi
.PP
appears in the translation definitions and the
.B \-i
flag is given upon
gateway invocation, typing:
.PP
.nf
    Do_Command_Transaction_Type
.fi
.PP
on the standard input of the gateway would cause the actions for the
rule to be executed.
.PP
.B Actions
.PP
Actions consist of a number of simple operations used to allocate, send,
and receive messages, and to assign values to their fields.  Actions
fall into three major classes- procedures, functions, and assignment
operations.
.PP
.B Procedures
.PP
Procedures are executed for their side effects, and include:
.TP
.B alloc(\fImessage_variable, name\fR)
Allocate storage for a new message of the named type in the
specified message variable.
Delete any message currently in that variable.
.TP
.B alloc(MSG_CLIPC_REPLY)
Allocate storage for a clipc reply message for the request message
currently in
.B MSG_CLIPC_REQUEST,
and store it in the
.B MSG_CLIPC_REPLY
message variable.
Delete any message currently in that variable.
.TP
.B alloc(\fIsoftbench_msg_variable\fR)
Allocates storage for a SoftBench message in the specified
message variable.
SoftBench doesn't have the same level of message semantics
as does CLIPC or ToolTalk.
It maintains no notion of an envelope and a body part
of a message.
The message type name (command) field of the message
is assigned just like any other field.
.TP
.B send(\fImessage_variable\fR)
Send the message in a message variable.
The message is sent out in the appropriate domain,
and is delivered according to the values of its
envelope fields.
.TP
.B error_send(\fImessage_variable\fR)
Sends the message in the message variable
(CLIPC or ToolTalk messages only),
but first add the
status values contained in the $GATEWAY_MSG_ERROR_STRING and
$GATEWAY_MSG_STATUS variables to the message.
Note that these variables are automatically set from
the fields of CLIPC or ToolTalk error messages.
.TP
.B echo(\fIstrings\fR)
Concatenate and echo the argument strings to the standard output.
.PP
.B Functions
.PP
There is currently only one function available.
The
.B shell(\fIstrings\fR)
function concatenates its arguments and passes them to a shell for
execution, using the
.B system(3)
function.  The standard output from the
invocation is captured and returned as the result of the function.  If
the shell returns failure execution status, the invocation of the
gateway fails too, so it is generally wise to be sure execution will be
successful in all but the most dire cases.
.PP
.B Assignment Operations
.PP
The only operation currently supported is integer or string assignment.
An assignment statement is of the form:
.PP
.nf
    type lvalue = rvalue;
.fi
.PP
Types available are
.B INTEGER
and
.B STRING
(For assignment to SoftBench message fields,
only the
.B STRING
type is available).
Lvalues can be message fields
or string variables,
and rvalues can be lvalues or string, integer, or
boolean constants.
.PP
.B String Variables
.PP
String variables have global scope and can be assigned to.  a statement
of the form:
.PP
.nf
    $PROGRAM_NAME = "a.out";
.fi
.PP
Assigns the value of the string constant "a.out" to the string variable
$PROGRAM_NAME.  As in most shells and the BASIC programming language,
string variable names must begin with the dollar sign ('$') character.
The value returned when a string variable is referenced depends on the
following algorithm.  If the string variable exists, its value is
returned.  If it does not exist, the environment is searched for an
environment variable of that name.  If found, the value of the
environment variable is returned.  If it is not found, a new string
variable with the referenced name is created and set to an empty string
value, and that value is returned.
.PP
As in awk, strings and integers are used pretty interchangeably in tdl.
An empty string returns an integer value of zero (0).
.PP
There are a number of special string variables which are set or
referenced as a consequence of procedure invocations.  There usage is
outlined in a separate section.
.PP
.B String Concatenation
.PP
Any place a string rvalue can be used, a comma separated series of string
values may be used instead.  The strings are concatenated together,
separated by the value of the
.B $GATEWAY_CAT_SEPARATOR
string variable.
.PP
Field Specification
.PP
Standard structure "dot" notation is used to specify fields of messages.
Since CLIPC message fields are named, field specification includes the
field names, i.e.:
.PP
.nf
    MSG_CLIPC_REPLY.function.visibleName
.fi
.PP
Which specifies the field "visibleName" of the field "function" of the
message currently contained in the MSG_CLIPC_REPLY message variable.
.PP
CLIPC array field specification reflects the strange semantics of CLIPC
arrays.  On field references an array element is indicated with a string
of the form "name[]."  On field assignments an array element can be of
the form "name[\fIinteger_constant\fR]" or "name[*]" or "name[@]."  The first
example specifies an array element, while the second and third select
all elements of the array, in forward and reverse order respectively.
.PP
Dot notation is also used to specify fields of ToolTalk messages, but
since they are position-dependent, an ordinal number is used to
represent them, i.e., TT_REQUEST.1.
.PP
SoftBench message fields are also specified using dot notation,
but the name following the dot character must be one of the
following:
.TP
.B FIELD_SB_ID
Specifies the message's ID field.
If not set, SoftBench will provide an appropriate ID.
.TP
.B FIELD_SB_TOOL_CLASS
Specifies the tool class of the message.
If not, set, the default tool class will be substituted
by SoftBench.
Note that for messages which you send out in the SoftBench domain,
the default tool class is usually NOT what you want.
.TP
.B FIELD_SB_COMMAND
Specifies the message's command field.
This field is analogous to the message type name
of CLIPC messages.
It is not set at message allocation time,
so must be set explicitly.
.TP
.B FIELD_SB_HOST
This field specifies the host component of
the message's context.
.TP
.B FIELD_SB_DIRECTORY
This field specifies the directory component of
the message's context.
.TP
.B FIELD_SB_FILE
This field specifies the file component of
the message's context.
.TP
.B FIELD_SB_DATA
This field specifies the message's data.
It may not be used alone in a field specification,
but must include a sub-field number.
Thus, the field specification "MSG_SB_REQUEST.FIELD_SB_DATA.2"
specifies the second data sub-field.
A field specification of the form "MSG_SB_REQUEST.FIELD_SB_DATA.0"
specifies the entire data field.
.PP
.B String Variables
.PP
The following string variables are special in the gateway:
.TP
.B $GATEWAY_CAT_SEPARATOR
The value of this variable is used
to separate all string concatenations.
.TP
.B $GATEWAY_MSG_ERROR_STRING
Actions for
.B ERROR_REPLY
targets can
use the value of this variable, which
contains the human-readable error
extracted from the reply message.
The
.B error_send(\fImessage_variable\fR)
function adds this value to the argument
message before it is sent.
.TP
.B $GATEWAY_MSG_STATUS
Similar to above, but contains the
integer status value from the reply
message.
.TP
.B $GATEWAY_MSG_FIELD_DEFAULT
This value is substituted for the
value of a specified message field
if the specified field cannot be
found in the message.
.SH DEMONSTRATIONS
There are two translation demonstrations currently
implemented with the gateway.
.PP
.B ToolTalk Demo
.PP
The first is a ToolTalk demonstration,
which implements and exercises the CASE Interoperability "Debug"
messages (Release 1.0).
To my knowledge,
there are no commercial products which actually use
these messages,
so the demo includes a script which actually sends them.
To run this demo,
first bring up a ToolTalk session
on a Sparc sunos4 machine.
Bring up a CodeCenter session in the directory
containing the demo files:
.PP
.nf
    cd /usr/local/CenterLine/lib/general
    codecenter
.fi
.PP
Start up the gateway with the TDL file which specifies
the CASE message translations:
.PP
.nf
    cd /usr/local/CenterLine/lib/general
    /usr/local/CenterLine/<arch-os>/bin/clms_ttgateway -v ttxlate.tdl
.fi
.PP
Then in a separate window,
start up the driver script:
.PP
.nf
    cd /usr/local/CenterLine/lib/general
    tttest.csh | /usr/local/CenterLine/<arch-os>/bin/clms_ttgateway -i -v tttest.tdl
.fi
.PP
Each time you hit the return key in the window in which
the script is running,
a ToolTalk message will be broadcast,
which will in turn be received by the gateway.
The gateway will translate this message into
an appropriate action,
resulting in one or more CLIPC messages being sent to
the CodeCenter session.
The reply and notification messages from the CodeCenter
session are also received by the gateway and translated
into appropriate ToolTalk messages.
.PP
.B SoftBench Demo
.PP
The SoftBench demo actually installs CodeCenter as an
available SoftBench Tool Manager tool.
In addition,
CodeCenter requests for file editing services will be vectored
to SoftBench.
To run this demo,
first bring up SoftBench and CodeCenter on an
HP machine.
Then bring up the gateway as follows:
.PP
.nf
    clms_sbgateway -sCodeCenter /usr/local/CenterLine/lib/general/sbxlate.tdl
.fi
.PP 
.SH FILES
.TP
.B /usr/local/CenterLine/lib/general/ttxlate.tdl
Translation rules for CASE Interoperability "Debug"
messages (Release 1.0).
This also serves as an example of tdl
syntax, lacking a more formal description in this
man page.
.TP
.B /usr/local/CenterLine/lib/general/tttest.tdl
Some automated testing rules for the translations in
ttxlate.tdl.
.TP
.B /usr/local/CenterLine/lib/general/tttest.csh
A script which drives tttest.tdl.
.TP
.B /usr/local/CenterLine/lib/general/testprog.c
A sample program for the ToolTalk demo.
.TP
.B /usr/local/CenterLine/lib/general/Makefile.cline
A sample loading make file for the ToolTalk demo.
.TP
.B /usr/local/CenterLine/lib/general/sbxlate.tdl
Translation rules for SoftBench demo.
This also serves as an example of tdl
syntax, lacking a more formal description in this
man page.
.SH SEE ALSO
.B CLIPC User's Guide
.br
.B ToolTalk Programmer's Guide
.br
.B SoftBench Encapsulator: Programmer's Guide
.br
.B SoftBench Encapsulator: Programmer's Reference
.SH BUGS
It is not likely that the tdl language is powerful enough to
deal with all possible translations.
Its use should be limited to reasonably simple translations.
It may be enhanced in the future given a high enough
demand for the product.
.PP
No provisions exist for directed signaling.
.PP
Resorting to the shell for most conversions is likely to be
slow.  If the gateway is ever intended to be of
production quality then it will have to be enhanced in this
area or the CX's message structure will have to be enhanced.
.SH AUTHOR
CenterLine Software, Inc.

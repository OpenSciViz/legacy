.TH OBJECTCENTER 1 "November 1993" "ObjectCenter" "ObjectCenter Manual Pages"
.SH NAME
objectcenter \- Start the ObjectCenter Programming Environment
.SH SYNOPSIS
.B objectcenter
.RI [ "switches" ]
.RI [ project_file ]
.SH DESCRIPTION
ObjectCenter is an integrated programming environment for the C++ language.
It consists of a C++ translating system (CC), a C++ interpreter, a source-level debugger for source,
object, library, and
.B a\.out
files, a load-time error checker for source code, a run-time error checker 
for source and object code, and an incremental linker/loader.
.SS User Interfaces
.PP
You can run ObjectCenter with three different user interfaces:
.TP
\(bu 
Motif graphical user interface
.RB ( \-motif
switch)
.TP
\(bu 
OPEN LOOK graphical user interface
.RB ( \-openlook
switch)
.TP
\(bu 
Ascii character-based interface
.RB ( \-ascii
switch)
.PP
The Motif and OPEN LOOK interfaces provide a multiple-window development environment.
You can use the mouse to set breakpoints, examine the values of variables, control 
program execution, examine source files, select commands, and so on.
.PP
The Ascii interface, known as 
.I Ascii ObjectCenter,
can run in any shell, so it can be used on terminals that don't support 
graphics. It consists of only the ObjectCenter workspace.
.SS Debugging Modes
.PP
You can run ObjectCenter with two different debugging modes:
.TP
\(bu 
Component debugging mode - 
You load all the parts, or components, of your program, and then link and execute them
within ObjectCenter. Program components include source code, object code, and library files.
.TP
\(bu 
Process debugging mode - 
You load your program as a fully-linked executable
.RB ( a\.out
file), and you have the choice of debugging it along with a core file or
attaching to another process.
.PP
When you start ObjectCenter, it automatically loads the standard C++ and C  libraries.
On some workstations, ObjectCenter might load the shared version of these libraries.
.SS Eight-bit Character Sets
.PP
ObjectCenter supports eight-bit character sets in component debugging mode. Add the following two lines to
your local
.B .ocenterinit
file so you can use the Meta key to get the extended character set:
.RS 5
setopt eight_bit
.br
unsetopt line_meta
.RE
.PP
To turn on this feature for all users at your site, ask your system administrator
to add these two lines to the system-wide
.B ocenterinit
file.
.SS Arguments
.PP
The
.B objectcenter
command is installed in the following directory:
.RS 5
.IB install_dir /CenterLine/bin/objectcenter
.RE
.PP
You can start ObjectCenter by typing the absolute pathname of the
.B objectcenter
command, by putting
.B CenterLine/bin
in your path, or by just typing the command name. See your system administrator if
you don't know where 
.B CenterLine/bin
is on your system.
.TP
.I switches
Command-line options. Since ObjectCenter supports a large number of command-line
options as well as workspace options, the term "switches" refers to command-line
options, whereas the term "options" refers to workspace options. Some switches
apply only to a particular debugging mode or user interface, while others apply
to all modes and interfaces.
.TP
.I project_file
Starts ObjectCenter in component debugging mode and loads a project file named
.IR project_file .
This is equivalent to entering
.B load
.I project_file
as the first statement in the ObjectCenter workspace.
.SH GENERAL SWITCHES
.PP
The following switches select the user interface and debugging model. 
The default user interface depends on your platform. The default debugging
mode is component debugging mode.
.TP
.B \-ascii
Starts ObjectCenter with the character-based interface (Ascii ObjectCenter).
.TP
.B \-motif
Starts ObjectCenter with the Motif user interface.
.TP
.B \-openlook
Starts ObjectCenter with the OPEN LOOK user interface.
.TP
.B \-pdm
Starts ObjectCenter in process debugging mode.
.PP
The following switches apply to both component debugging mode and process debugging mode.
.TP
.BI \-config \ pathname
Uses the X resources in
.I pathname
instead of the defaults.
.TP
.BI \-f  \ log_name
Saves a copy of all input typed in the ObjectCenter workspace in a permanent
file called
.I log_name.
All input is usually saved in a temporary log file that is deleted when you
quit ObjectCenter.
.TP
.B \-no_fork
Create a separate Run Window but avoid returning immediate control to the shell.
 With -no_fork, control returns when you enter ^Z in the shell or exit ObjectCenter.  
Without -no_fork, the shell prompt comes back immediately.
.TP
.B \-no_run_window 
Avoids creating the separate Run Window and avoids returning control to the shell.  
Your program's output goes to the shell in which you invoked ObjectCenter. Using
the -no_run_window switch means you are unable to interrupt ObjectCenter
and unable to place it in the background. This option is intended for debugging
applications that need specific terminal support rather than a generic terminal
such as xterm.
.TP
.BI \-s \ [startup_file]
If
.I startup_file
is supplied, ObjectCenter reads it at startup instead of the 
.B $HOME\(sl\.ocenterinit
file, which is the default startup file. If a filename is not specified,
ObjectCenter ignores the 
.B $HOME\(sl\.ocenterinit
startup file, which is the default startup file.
.TP
.BI \-S \ [startup_file]
If 
.I startup_file
is supplied, ObjectCenter reads it at startup instead of the
.IB install_dir \(slCenterLine\(slconfigs\(slocenterinit
startup file, which is the default system startup file. If a filename is not specified,
ObjectCenter ignores the
.IB install_dir \(slCenterLine\(slconfigs\(slocenterinit
system startup file.
.TP
.B \-usage
Displays a summary of switch abbreviations and arguments.
.SH CDM-SPECIFIC SWITCHES
.PP
The following switches apply only to 
.I component debugging mode.
.TP
.BI \-D name[\(eqdefinition]
Passes this load switch to the 
.B load
workspace command when starting ObjectCenter. This switch causes
.I name
to become defined as if a
.B #define
directive had occurred. If
.I definition
is not supplied, then the value 1 is used.
.TP
.B \-G
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. This switch ignores debugging 
information, produced by the
.B \-g
switch of the compiler, when loading compiled files. This allows you to
load compiled files for which ObjectCenter has trouble reading the debugging
information. If this switch is used when a library is loaded, debugging 
information will be ignored when linking from the library.
.TP
.BI \-I header_path
Passes this load switch to the
.B load 
workspace command when starting ObjectCenter. This switch adds
.I header_path
to the list of directories to search for files specified by the
.B #include
preprocessor directive. When the name of a file is surrounded by double
quotes, it is first sought in the directory of the file being read, then in
directories specified by
.B \-I
switches, and finally in the
.B \(slusr\(slinclude
directory. If a filename is quoted with angle brackets, it is only sought
in directories specified by the
.B \-I
switches and the
.B \(slusr\(slinclude
directory.
.TP
.BI \-i  \ input_stream
Specifies that ObjectCenter's command input should be read from
.I input_stream
(a file or device name) rather than from standard input.
.TP
.BI \-l lib_name
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. When a library is loaded using the
.BI \-l x
format, (where
.I x
is the name of the library) then a file called 
.BI lib x
.B \.a
is sought first in the directories specified by
.B \-L
switches, then in the standard directories
.BR \(sllib ,
.BR \(slusr\(sllib ,
and
.BR \(slusr\(sllocal\(sllib .
.TP
.BI \-L library_path
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. This switch adds
.I library_path
to the list of directories to search for libraries.
.TP
.BI \-m  \ target
Indicates that ObjectCenter should perform a
.B make
on
.I target
when starting up. This is equivalent to entering
.B make
.I target
as the first statement in the ObjectCenter workspace.
.TP
.BI \-o  \ output_stream
Specifies that ObjectCenter's command output should be written to
.I output_stream
(a file or device name) rather than to standard output.
.TP
.BI \-r \ number
Sets the size of the run-time stack to
.I number
nested function calls. The default size is approximately 1000 nested function
calls. The default size may need to be increased when executing highly recursive
programs.
.TP
.BI \-U macro_name
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. This switch causes the predefined
.I macro_name
to become undefined as if a
.B #undef
directive had occurred.
.TP
.B \-w
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. This switch suppresses reporting
of warnings; errors are always reported. If this switch is used when a library
is loaded, warnings will be suppressed when modules are linked from the
library.
.SH PDM-SPECIFIC SWITCHES
.PP
The following switches apply only to
.I process debugging mode.
.TP
.B \-class_as_struct 
Disables maximum processing of classes to improve performance.
.TP
.B \-full_symbols
Forces the reading of the full symbol table for maximum information immediately.
.SH ASCII-SPECIFIC SWITCH
.PP
The following switch applies only to Ascii ObjectCenter.
.TP
.B \-d
Passes this load switch to the
.B load
workspace command when starting ObjectCenter. This switch turns off terminal-dependent
output; as a result, raw mode input will be disabled so that pressing the Return key
is required to respond to a prompt.
.SH FILES
.PP
In pathnames,
.I install_dir
is the name of the directory where ObjectCenter was installed.
.TP
.IB install_dir /CenterLine/configs/ocenterinit
System-wide ObjectCenter startup file.
.TP
.B $HOME\(sl\.ocenterinit
Personal ObjectCenter startup file.
.TP
.B $HOME\(sl\.pdminit
Personal pdm-specific startup file.
.TP
.B gdbinit
System-wide 
.B gdb
startup file.
.TP
.B $HOME\(sl\.gdbinit
Personal 
.B gdb
startup file.
.SH "SEE ALSO"
.PP
.BR clezstart(1) ,
.BR c++tutor(1) ,
.BR clms(1) ,
.BR clms_registry(1) ,
.BR clms_monitor(1) ,
.BR clms_query(1) ,
.BR killms(1)
.PP
.I Installing and Managing CenterLine Products
.br
.I ObjectCenter Platform Guide
.br
.I ObjectCenter Tutorial
.br
.I ObjectCenter User's Guide
.br
.I ObjectCenter Reference
.br
Context-sensitive help (via Help or F1 key)
.br
ObjectCenter Man browser
.SH AUTHOR
CenterLine Software, Inc.




SBUF.PUB(3C++)    Misc. Reference Manual Pages     SBUF.PUB(3C++)



NAME
     streambuf - public interface of character buffering class

SYNOPSIS
     #include <iostream.h>

     typedef long streamoff, streampos;
     class ios {
     public:
               enum      seek_dir { beg, cur, end };
               enum      open_mode { in, out, ate, app, trunc, nocreate, noreplace } ;
               // _a_n_d _l_o_t_s _o_f _o_t_h_e_r _s_t_u_f_f ... _S_e_e _i_o_s(_3_C++)
     } ;

     class streambuf {
     public :

               int       in_avail();
               int       out_waiting();
               int       sbumpc();
               streambuf*setbuf(char* ptr, int len);
               streampos seekpos(streampos, int =ios::in|ios::out);
               streampos seekoff(streamoff, seek_dir, int =ios::in|ios::out);
               int       sgetc();
               int       sgetn(char* ptr, int n);
               int       snextc();
               int       sputbackc(char);
               int       sputc(int c);
               int       sputn(const char* s, int n);
               void      stossc();
               virtual intsync();
     };

DESCRIPTION
     The streambuf class supports buffers into  which  characters
     can  be  inserted  (put)  or  from  which  characters can be
     fetched (gotten).  Abstractly, such a buffer is  a  sequence
     of  characters  together  with  one  or  two pointers (a get
     and/or a put pointer) that  define  the  location  at  which
     characters  are  to  be  inserted  or fetched.  The pointers
     should be thought of as pointing between  characters  rather
     than  at them.  This makes it easier to understand the boun-
     dary conditions (a pointer before  the  first  character  or
     after the last).  Some of the effects of getting and putting
     are defined by this class but most of the details  are  left
     to   specialized   classes  derived  from  streambuf.   (See
     _f_i_l_e_b_u_f(_3_C++), _s_s_b_u_f(_3_C++), and _s_t_d_i_o_b_u_f(_3_C++).)

     Classes derived from streambuf vary in their  treatments  of
     the  get  and put pointers.  The simplest are unidirectional
     buffers which permit only gets or only puts.   Such  classes
     serve  as  pure  sources (producers) or sinks (consumers) of



                 Last change: C++ Stream Library                1






SBUF.PUB(3C++)    Misc. Reference Manual Pages     SBUF.PUB(3C++)



     characters.  Queuelike buffers  (e.g.,  see  strstream(3C++)
     and  ssbuf(3C++))  have  a  put and a get pointer which move
     independently of each other.   In  such  buffers  characters
     that are stored are held (i.e., queued) until they are later
     fetched.    Filelike    buffers    (e.g.,    filebuf,    see
     _f_i_l_e_b_u_f(_3_C++))  permit  both  gets  and puts but have only a
     single pointer.  (An alternative description is that the get
     and put pointers are tied together so that when one moves so
     does the other.)

     Most streambuf  member  functions  are  organized  into  two
     phases.  As far as possible, operations are performed inline
     by storing into or fetching from arrays (the  _g_e_t  _a_r_e_a  and
     the  _p_u_t  _a_r_e_a,  which  together  form  the _r_e_s_e_r_v_e _a_r_e_a, or
     _b_u_f_f_e_r).  From time to time, virtual functions are called to
     deal  with  collections  of  characters  in  the get and put
     areas.  That is, the virtual functions are called  to  fetch
     more  characters  from  the  ultimate producer or to flush a
     collection of characters to  the  ultimate  consumer.   Gen-
     erally  the  user  of a streambuf does not have to know any-
     thing about these details, but some of  the  public  members
     pass back information about the state of the areas.  Further
     detail about these areas  is  provided  in  _s_b_u_f._p_r_o_t(_3_C++),
     which describes the protected interface.

     The public member  functions  of  the  streambuf  class  are
     described below.  In the following descriptions assume:
     - i, n, and len are ints.
     - c is an int. It always holds a "character" value  or  EOF.
     A  "character"  value  is  always positive even when char is
     normally sign extended.
     - sb and sb1 are streambuf*s.
     - ptr is a char*.
     - off is a streamoff.
     - pos is a streampos.
     - dir is a seek_dir.
     - mode is an int representing an open_mode.

     Public member functions:

     i=sb->in_avail()
          Returns the number of characters that  are  immediately
          available  in  the get area for fetching.  i characters
          may be fetched with a guarantee that no errors will  be
          reported.

     i=sb->out_waiting()
          Returns the number of characters in the put  area  that
          have not been consumed (by the ultimate consumer).

     c=sb->sbumpc()
          Moves the get pointer forward one character and returns



                 Last change: C++ Stream Library                2






SBUF.PUB(3C++)    Misc. Reference Manual Pages     SBUF.PUB(3C++)



          the  character  it  moved past.  Returns EOF if the get
          pointer is currently at the end of the sequence.

     pos=sb->seekoff(off, dir, mode)
          Repositions the get and/or put pointers.   mode  speci-
          fies  whether the put pointer (ios::out bit set) or the
          get pointer (ios::in bit set) is to be modified.   Both
          bits  may  be set in which case both pointers should be
          affected.   off  is  interpreted  as  a  byte   offset.
          (Notice that it is a signed quantity.)  The meanings of
          possible values of dir are

          ios::beg
               The beginning of the stream.

          ios::cur
               The current position.

          ios::end
               The end of the stream (end of file.)
     Not all classes derived from streambuf  support  reposition-
     ing.   seekoff()  will return EOF if the class does not sup-
     port repositioning.  If the class does  support  reposition-
     ing, seekoff() will return the new position or EOF on error.

     pos=sb->seekpos(pos, mode)
          Repositions the streambuf get  and/or  put  pointer  to
          pos.  mode specifies which pointers are affected as for
          seekoff().  Returns pos (the argument) or  EOF  if  the
          class  does  not  support  repositioning  or  an  error
          occurs.  In general a streampos should be treated as  a
          "magic cookie" and no arithmetic should be performed on
          it.  Two particular values have special meaning:

          streampos(0)
               The beginning of the file.

          streampos(EOF)
               Used as an error indication.

     c=sb->sgetc()
          Returns the character after the get pointer.   Contrary
          to  what  most  people expect from the name _I_T _D_O_E_S _N_O_T
          _M_O_V_E _T_H_E _G_E_T _P_O_I_N_T_E_R.  Returns EOF if there is no char-
          acter available.

     sb1=sb->setbuf(ptr, len, i)
          Offers the len bytes starting at  ptr  as  the  reserve
          area.   If  ptr is null or len is zero or less, then an
          unbuffered state is  requested.   Whether  the  offered
          area  is  used,  or  a  request for unbuffered state is
          honored  depends  on  details  of  the  derived  class.



                 Last change: C++ Stream Library                3






SBUF.PUB(3C++)    Misc. Reference Manual Pages     SBUF.PUB(3C++)



          setbuf() normally returns sb, but if it does not accept
          the offer or honor the request, it returns 0.

     i=sb->sgetn(ptr, n)
          Fetches the n characters following the get pointer  and
          copies  them  to  the area starting at ptr.  When there
          are fewer than n characters left before the end of  the
          sequence  sgetn()  fetches  whatever characters remain.
          sgetn()  repositions  the  get  pointer  following  the
          fetched characters and returns the number of characters
          fetched.

     c=sb->snextc()
          Moves the get pointer forward one character and returns
          the  character  following the new position.  It returns
          EOF if the pointer is  currently  at  the  end  of  the
          sequence  or is at the end of the sequence after moving
          forward.

     i=sb->sputbackc(c)
          Moves the get pointer back one character.   c  must  be
          the current content of the sequence just before the get
          pointer.  The underlying mechanism may simply  back  up
          the  get  pointer  or  may  rearrange its internal data
          structures so the c is saved.  Thus the effect of sput-
          backc()  is  undefined if c is not the character before
          the get  pointer.   sputbackc()  returns  EOF  when  it
          fails.   The  conditions under which it can fail depend
          on the details of the derived class.

     i=sb->sputc(c)
          Stores c after the  put  pointer,  and  moves  the  put
          pointer past the stored character; usually this extends
          the sequence.  It returns EOF  when  an  error  occurs.
          The  conditions  that  can  cause  errors depend on the
          derived class.

     i=sb->sputn(ptr, n)
          Stores the n characters starting at ptr after  the  put
          pointer  and  moves the put pointer past them.  sputn()
          returns i, the number  of  characters  stored  success-
          fully.  Normally i is n, but it may be less when errors
          occur.

     sb->stossc()
          Moves the get pointer forward one  character.   If  the
          pointer  started  at the end of the sequence this func-
          tion has no effect.

     i=sb->sync()
          Establishes  consistency  between  the  internal   data
          structures  and  the  external  source  or  sink.   The



                 Last change: C++ Stream Library                4






SBUF.PUB(3C++)    Misc. Reference Manual Pages     SBUF.PUB(3C++)



          details of this function depend on the  derived  class.
          Usually  this  "flushes"  any characters that have been
          stored but not yet consumed, and "gives back" any char-
          acters that may have been produced but not yet fetched.
          sync() returns EOF to indicate errors.

CAVEATS
     setbuf does not really belong in the public interface. It is
     there for compatibility with the stream package.

     Requiring the program  to  provide  the  previously  fetched
     character to sputback is probably a botch.

SEE ALSO
     ios(3C++), istream(3C++), ostream(3C++), sbuf.prot(3C++)








































                 Last change: C++ Stream Library                5




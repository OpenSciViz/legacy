.TH FPRINTF 3 "June 1993" "CenterLine-C" "Reference Pages"
.SH NAME
fprintf
.LP
ANSI           Print to a file.
.SH SYNOPSIS
.LP
.B #include <stdio.h>     /* Required. */
.br
\fBint fprintf(FILE *\fIstream\fB,const char *\fIformat\fB, ...);\fR
.SH DESCRIPTION
.LP
Writes to the file associated with
.I stream
according to the format string
.IR format .
.LP
.I format
contains conversion specifications that specify how to
represent subsequent arguments in print. Any characters that are
not elements of conversion specifications are simply printed out
unchanged. The representation of each argument following
.I format
is printed out at the point in the string where its conversion
specification appears. The conversion specifications act as place
holders for a printable representation of the corresponding argument.
.I format
is printed with each specification replaced by the
representation of its argument.
.LP
If the number of actual arguments is fewer than the number of
arguments specified by format,
.B fprintf
traipses merrily through
memory printing whatever it finds in its presumed argument list
according to the specification in the format string. If the number of
arguments passed in is greater than the number specified by format,
the excess arguments are evaluated by the standard function call
mechanism, but otherwise are ignored.
.LP
Conversion specifications are of the following form:
.LP
.RS
 '%' Flag* Field_width? Precision? Size? C_char
.RE
.LP
where:
.LP
.RS
Flag        -> '-' | '+' | ' ' | '#' | '0';
.br
Field_width -> ((Digit - '0') Digits) | '*';
.br
Precision   -> '.' (Digits | '*');
.br
Size        -> 'h' | 'l' | 'L';
.br
C_char      -> 'd' | 'i' | 'o' | 'u' | 'x' | 'X' |
.br
		'f' | 'e' | 'E' | 'g' | 'G' | 'c' |
.br
		's' | 'p' | 'n' | '%';
.RE
.LP
The asterisk (*) indicates that a conversion specification can contain
zero or more
.IR Flags .
The question mark (?) after the
.IR Field_width ,
.IR Precision ,
and
.I Size
specifications indicates that zero or one of each of them can appear.
For more information about this notation, refer to the "Regular Expressions"
section in the CenterLine-C Programmer's Guide and Reference.
.LP
 '%' sets off a conversion specification from ordinary characters.
.LP
Each conversion takes place within a field of characters. The
minimum size of the field can be specified by
.B Field_width
If no
.B Field_width
appears, the field is the size of the result of the
conversion. If the result of the conversion contains fewer characters than the
.B Field_width
specifies, the field is padded with spaces or zeros. If
.B Field_width
is not an integer but is '*', the value of
.B Field_width
is to be taken from an
.B int
argument that precedes the
argument to be converted. If there are more characters resulting
from a conversion than specified by
.B Field_width
the field is expanded so that they all get printed;
.B Field_width
never causes
truncation.
.LP
The meaning of
.B Precision
varies from one conversion type to the
next. Usually it specifies a maximum or minimum number of
significant characters in the result.
.B Precision
differs from
.B Field_width
in that
.B Field_width
can cause padding, but can never affect the "value" of the result, while
.B Precision
affects the characters produced by converting an argument. For example,
.B Precision
can cause string truncation, or affect the number of characters that
appear after the decimal point in a
.B double
conversion. If
.B Precision
is not an integer but is '*', the value of
.B Precision
is to be taken from an
.B int
argument that precedes the argument to be converted. If both
.B Field_width
and
.B Precision
are specified by asterisks, the
.B Field_width
argument comes first, then the
.B Precision
argument, then the argument to be converted. A negative
.B Precision
is taken as if
.B Precision
were missing.
.LP
.B Size
specifies the size of an argument whose type comes in more
than one size, for example,
.B int
versus
.B long int
The
.B Size
specification is mentioned below in the description of each
conversion that it can affect. If
.B Size
is specified for a conversion that it cannot affect, it is ignored.
.LP
.B C_char
specifies the type of conversion: both the type of the
argument to be converted and the format of the converted result (modified by
.BR Flags ,
.BR Field_width ,
.BR Precision ,
and
.BR Size ).
.LP
See the "Regular Expressions" section
in the CenterLine-C Programmer's Guide and Reference for an explanation
of the notation used to describe conversion characters.
.LP
The meanings of the flag characters are:
.TP
 '-'
The result of the conversion is left justified within
the field. Any padding appears on the right. If '-' does
not appear, the result is right justified.
.TP
 '+'
The result of a signed conversion begins with a plus or
minus sign. Negative values are printed beginning with a
minus sign, and positive values are printed beginning with
a plus sign. If neither '+' nor ' ' appear, negative values
begin with a minus sign, and positive values begin with the
first digit of the result.
.TP
 ' '
The result of a signed conversion begins with a space or
minus sign. Negative values are printed beginning with a
minus sign, and positive values are printed beginning with a
space. If both the ' ' flag and the '+' flag appear, the
 ' ' flag is ignored. If neither '+' nor ' ' appears, negative
values begin with a minus sign, and positive values begin
with the first digit of the result.
.TP
 '#'
The result is to be converted to an alternate form, specified
below in the description of each conversion character. If no
alternate form is mentioned in such a description, the flag has
no effect on that conversion.
.TP
 '0'
Padding is by zeros. If the '0' flag does not appear, padding
is by spaces. If padding is by zeros, the sign (or space, if
the ' ' flag appears), if any, precedes the zeros. If padding
is by spaces, the spaces precede the sign. If the result is left
justified ('-' appears), padding is by spaces on the right and         
the '0' flag has no effect.
.LP
The meanings of the size characters are:
.TP
 'h'
The argument A is a short int.
.TP
 'l'
The argument A is a long int.
.TP
 'L'
The argument A is a long double.
.LP
The meanings of the conversion characters are:
.TP
 'd', 'i'
The argument A is an int that is printed out as a signed
decimal number. Precision specifies the minimum number of
digits to appear. If the value can be represented in fewer than
Precision digits, it is expanded with leading zeros. The
default precision is one. If Precision is zero and A is zero, the
converted value consists of no characters. (This is
independent of any padding specified by Field_width.)
.TP
 'o'
The argument A is an int that is printed out as an unsigned
octal number. Precision specifies the minimum number of
digits to appear. If the value can be represented in fewer than
Precision digits, it is padded with leading zeros. The default
precision is one. If Precision is zero and A is zero, the
converted value consists of no characters. (This is
independent of any padding specified by Field_width.) If the
 '#' flag appears, '0' is prepended to the result if it is not zero.
.TP
 'u'
The argument A is an int that is printed out as an unsigned
decimal number. Precision specifies the minimum number
of  digits to appear. If the value can be represented in fewer
than Precision digits, it is padded with leading zeros. The
default precision is one. If Precision is zero and A is zero, the
converted value consists of no characters. (This is
independent of any padding specified by Field_width).
.TP
 'x', 'X'
The argument A is an int that is printed out as an unsigned
hexadecimal number. The letters abcdef are used for 'x'
conversion, while ABCDEF are used for 'X' conversion.
Precision specifies the minimum number of digits to appear.
If the value can be represented in fewer than Precision digits,
it is padded with leading zeros. The default precision is one. If
Precision is zero and A is zero, the converted value consists of
no characters. (This is independent of any padding specified
by Field_width.) If the '#' flag appears, 0x is prepended to the
result (0X in the 'X' case).
.TP
 'f'
The argument A is a double (or a float, which is converted to
double when passed as a parameter) that is printed out in
decimal notation. If A is negative there is a leading minus. The
integral portion of the number appears, then a decimal point,
then Precision digits after the decimal point. If Precision is
not specified, it defaults to six. If Precision is explicitly zero,
no decimal point appears. If the number has no integral
portion and Precision is not zero, a '0' is printed out before the
decimal point. If the '#' flag appears, a decimal point is always
printed, even if it is not followed by any digits. If Precision is
zero and '#' appears, a decimal point is printed.
.TP
 'e', 'E'
The argument A is a double (or a float, which is converted to
double when passed as a parameter) that is printed out in
decimal notation. If A is negative there is a leading minus.
One digit appears before the decimal point, Precision digits
after the decimal point, then an e (or an E if the conversion
character is 'E'), followed by the sign of the exponent,
followed by the exponent. At least two digits are printed for
the exponent. If Precision is not specified, it defaults to six. If
Precision is explicitly zero, no decimal point appears. If the '#'
flag appears, a decimal point is always printed, even if it is not
followed by any digits. If Precision is zero and '#' appears, a
decimal point is printed.
.TP
 'g', 'G'
The argument A is a double (or a float, which is converted to
double when passed as a parameter) that is printed out in
style 'f', 'e', or 'E' with Precision specifying the number of
significant digits. Note that if Precision is zero, the converted
value consists of no characters. (This is independent of any
padding specified by Field_width.)
.br
.br
Style 'f' is used unless the exponent to be printed is less than 4
or greater than Precision. In that case, style 'e' is used for 'g'
and style 'E' is used for 'G'. Trailing zeros are removed from
the result. A decimal point appears only if it is followed by a
digit. If the '#' flag appears, a decimal point is always printed,
even if it is not followed by any digits, and trailing zeros are
not removed.
.TP
 'c'
The argument is an int, the least significant character of which is printed.
.TP
 'p'
The argument is taken to be a pointer to an object.
.TP
 's'
The argument is a (char *) string. Characters from the string
are printed until either a NUL is encountered (which is not
printed), or Precision characters have been printed. If
Precision is not explicitly set, characters are printed until a
NUL is found.
.TP
 'n'
The argument is a pointer to an integer into which is written
the number of characters printed thus far by the current call to fprintf.
.TP
 '%'
A % is printed. There is no argument.
.LP
If a conversion specification is not valid, that is, if the conversion
character is not one of those listed above,
.B errno
is set and that conversion is ignored. The argument that would have been
converted is converted instead by the next conversion specification.
.LP
If the arguments do not match those specified by format, the behavior of
.B fprintf
is undefined.
.LP
.B fprintf
returns the number of characters printed, or a negative
number if a write error occurs.
.SH CAUTIONS
.LP
The number of arguments passed to
.B fprintf
must equal or exceed the number specified by
.BR format ,
or garbage may be printed.
.LP
The types of the arguments to
.B fprintf
must match the types specified by
.BR format ,
or garbage may be printed.
.LP
In most systems, for a '%p' conversion specification, the argument is
taken to be a pointer to an object. The value of the pointer is printed
in hexadecimal.
.SH EXAMPLE
.LP
.RS
.nf
#include <stdio.h>
.br
#define FAILURE  (-1)
.br
.br
main() {
.br
FILE *FP;
.br
char s[14] = "like this one", c = '*';
.br
int i = 10, x = 255, o = 45;
.br
double d = 3.1415927, nf = -3.449123;
.br
if ((FP = fopen("example", "w")) == NULL) {
.br
	perror("Cannot open example");
.br
	return FAILURE;
.br
	}
.br
fprintf(FP,"fprintf prints strings (%s),\\n", s);
.br
fprintf(FP,"decimal numbers(%d),", i);
.br
fprintf(FP," hex numbers(%04X),\\n", x);
.br
fprintf(FP,"floating-point numbers (%+.4e)\\n", d);
.br
fprintf(FP,"percent signs(%%),\\n");
.br
fprintf(FP,"characters (%-5c),\\n", c);
.br
fprintf(FP,"negative floating-points (% 010.2e),
.br
	\\n",nf);
.br
fprintf(FP,"and even octal numbers (%-+#5o).", o);
.br
}
.fi
.RE
.LP
This prints to file
.BR example :
.LP
.RS
.nf
fprintf prints strings (like this one),
.br
decimal numbers (10), hex numbers (00FF),
.br
floating point numbers (+3.1416e+00),
.br
percent signs (%),
.br
characters (*    ),
.br
negative floating-points (-03.45e+00),
.fi
.RE
.SH SEE ALSO
.LP
.BR fscanf(3) ,
.BR printf(3) ,
.BR scanf(3) ,
.BR sprintf(3) ,
.BR sscanf(3) ,
.BR vfprintf(3) ,
.BR vprintf(3) ,
.B vsprintf(3)



#if defined(__CENTERLINE__)
#ifndef _CENTERLINE_STDARG_INCLUDED
#define _CENTERLINE_STDARG_INCLUDED

#ifdef sparc
#include "stdarg-sparc.h"
#endif /* sparc */

#ifdef __hp9000s700
#include "stdarg-pa.h"
#endif /* __hp9000s700 */

#ifdef __m88k__
#include "stdarg-m88k.h"
#endif /* __m88k__ */

#ifdef _I386
#include "stdarg-i386.h"
#endif /* _I386 */
#endif /* _CENTERLINE_STDARG_INCLUDED */
#else
/*
 * Do not include this version of stdarg.h unless you loading your
 * code into Codecenter/Objectcenter as source.
 */
@@@ The Centerline version of stdarg.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

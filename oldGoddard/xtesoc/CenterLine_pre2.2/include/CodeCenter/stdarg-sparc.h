#if defined(__CENTERLINE__)
#ifndef _CENTERLINE_SPARC_STDARG
#define _CENTERLINE_SPARC_STDARG

#include <varargs.h>

#endif /* _CENTERLINE_STDARG */
#else
/*
 * Do not include this version of stdarg-sparc.h unless you loading your
 * code into Codecenter/Objectcenter as source.
 */
@@@ The Centerline version of stdarg-sparc.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

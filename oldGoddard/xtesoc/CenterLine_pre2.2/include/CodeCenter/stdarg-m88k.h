/* Special stdarg.h for use in Codecenter/Objectcenter on 88k platforms */

#ifndef _STDARG_H
#define _STDARG_H

#ifdef __CENTERLINE__

#include "cl_va-m88k.h"

#ifndef _VARARGS_H
#define va_start(__p,__parmN) 	(((__p).__va_stk = (int *) &__parmN), ((__p).__va_arg =  __va_size(__parmN)))
#endif /* _VARARGS_H */

#else

    @@@ The CENTERLINE version of stdarg.h should only be used within @@@
    @@@ CODECENTER or OBJECTCENTER. @@@

#endif /* __CENTERLINE__ */
#endif  /* #ifndef _STDARG_H */

#if defined(__CENTERLINE__)
#ifndef _CENTERLINE_PA_STDARG
#define _CENTERLINE_PA_STDARG

/* LEAVE PA define */
#define _STDARG_INCLUDED

#ifndef _SYS_STDSYMS_INCLUDED
#  include <sys/stdsyms.h>
#endif /* _SYS_STDSYMS_INCLUDED */
#include <varargs.h>

#endif /* _CENTERLINE_PA_STDARG */
#else
/*
 * Do not include this version of varargs.h unless you loading your
 * code into Codecenter/Objectcenter as source.
 */
@@@ The Centerline version of stdarg.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

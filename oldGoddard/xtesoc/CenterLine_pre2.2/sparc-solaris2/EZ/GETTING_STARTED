
		Getting Started with the EZSTART Tools
		======================================


1. Who Should Use EZSTART
=========================

EZSTART is designed to help people who have been working on a software
project outside of the CenterLine environment and want to begin working
in the CenterLine environment as quickly as possible.  If you have been
working with CenterLine environments and have already modified your
make files to load the proper component files, then there are fewer
reasons for you to use EZSTART than for those people just getting
started with the CenterLine products.

EZSTART is most useful for customers who are evaluating the CenterLine
environment and do not wish to take the time to manually modify their
existing make files.


2. Overview
===========

The EZSTART software is a set of shell scripts and executable programs
that will help you load files from existing software development
projects into a CenterLine programming environment (either CodeCenter or
ObjectCenter).  CenterLine programming environments need to know which
files (both source and object) are part of a project and what command
line switches are passed to the compiler and linker.  EZSTART works with
your existing make file to find out what those files and command line
switches are.  It does so by intercepting compiles, links, and other
commands and recording what the command line arguments are then it
executes the command as it normally would be executed.

When done it writes out a make file (called Makefile.cline) that 
contains exactly the commands needed to load files into a CenterLine 
programming environment.

Since EZSTART uses your existing make file, it will only see those
commands that make generates.  To get started quickly you will probably
choose to merely recompile a few files and then relink.  EZSTART will
know about all of the object files in your system (from the link) and
will additionally know how to load the source for those files that were
recompiled.  Given this information, you can immediately start using a
CenterLine programming environment.

Once you have experimented a bit, you can use EZSTART to discover more
and more of the components used to build your project.  As with most tools,
you need to be a little more clever when you use the tool in more 
complicated situations.  Techniques for using EZSTART to discover all
of the components used to build your project are documented separately,
in the file named, "REFERENCE_GUIDE".


3. Installation
===============

a.) Create a directory called EZ.  (The name can be anything you want, but
    the documentation assumes that you call it EZ).

b.) cd to the EZ directory.

c.) Make sure you have access to the distribution.  The software is
    delivered as a tar file.  Extract all files from the tar file into
    the EZ directory.  For example, if the distribution is a cartridge
    tape, do the following from the EZ directory:
    
	  tar xvf /dev/rst8 .

    All of the files will be extracted into the EZ directory.

    There are now five files in the EZ directory:
      - README (an introduction)
      - GETTING_STARTED (this document)
      - REFERENCE_GUIDE (more detailed documentation)
      - clezstart (the script run to execute the EZSTART system)
      - clezstart_init (a template used with more advanced features)

    There is also a directory called "tools" which contains the reset of
    the EZSTART software.

    You may put the EZ directory in your search path, but you must NOT
    put the tools directory in the search path.  For instance, in the 
    C-shell, you could set your search path to include EZ by adding
    the following line to the end of your .cshrc file (assuming you installed
    EZSTART in the directory "usr/local/EZ"):

	set path = ( $path /usr/local/EZ )


4. Running EZSTART
==================

This document describes an approach that depends on certain assumptions
being true.  If any of these assumptions are false, then you will need
to consult the REFERENCE_GUIDE document to determine how to proceed.  You
can think of this as similar to filing your taxes - if you satisfy a certain
profile you can use the short form; otherwise you need to use the long
form, and in some cases you have to get an accountant (your UNIX make
guru) to help you.  The assumptions are:

	+ Your make file does not use absolute path names to specify
	  the compiler (cc or CC), linker (ld), archiver (ar), or
	  make tool (make).  Typically a make file contains a line
	  similar to:
			CC = cc
	  or:
			CC = /bin/cc

	  The former works just fine; the latter requires you to 
	  read the REFERENCE_GUIDE document.

	+ Your make file uses standard names for the compiler (cc, gcc,
	  acc, CC), linker (ld), archiver (ar), and make tool (make).

If your make file satisfies the above assumptions, then to create a 
Makefile.cline file for your project, do the following:

   1.) Go to a directory from which you normally do a make for one or
       more targets.

   2.) Remove the targets you are making as well as object files for
       files that you want to be able to load as source into the
       CenterLine programming environment.  Note that loading a file as
       source allows the CenterLine programming environment to perform
       many more compile-time and run-time checks and is one of the
       primary benefits of using the environment.

       We suggest that you first remove only a limited number of 
       object files (perhaps you will just remove the targets).  Later
       when you have gained experience with both EZSTART and the
       CenterLine programming environment, you will want to rerun
       EZSTART after removing all of your object files so that the
       Makefile.cline will have complete information.
   
   3.) Run clezstart with the same arguments you give to make.  If clezstart
       is not in your search path, you will have to invoke it by
       absolute path name.

When clezstart finishes you should have a Makefile.cline in the current
directory.  This file contains the commands to load the appropriate
files into the CenterLine programming environment.

There may be a section at the end of the file labeled "Messages" 
indicating conditions which EZSTART considered problematic.  In general, 
these messages can be safely ignored - they indicate cases when 
EZSTART was unable to extract complete information.  As discussed in
the README file, the lack of complete information is not a problem - you
will still be able to load your application into the CenterLine
environment.  However, when you decide that you do want complete 
information, if you have "Messages" then you should consult the
REFERENCE_GUIDE document.


5. How to use Makefile.cline
============================

Now that you have created a Makefile.cline, all you need to do is to
start the CenterLine programming environment and issue the appropriate
load commands.  For instance, if your application was called "program"
and it was built from three files, "foo.c", "bar.c" and "blatz.c", then
to load "program" in object form, you would type into the CenterLine
environment's workspace:

	make -f Makefile.cline program_obj

To load "program" in source form, you would type:

	make -f Makefile.cline program_src

The Makefile.cline file contains two pseudo make targets for each top
level component built by your existing make file: one contains
CenterLine commands to load the component in object file format and the
other contains CenterLine commands to load the component in source file
format.  A "top level" component is a component which is not
subsequently used to build yet another component.  For instance if your
make file builds two executables (say one for testing, called "test,"
and one that is the real application, "doit"), then there will be four
pseudo make targets in Makefile.cline: "test_obj", "test_src",
"doit_obj", and "doit_src".

NOTE: There is actually a third target which can be created for each top
level component.  This is named <target>_obj_nodebug.  See the
REFERENCE_GUIDE document for information on how to get this target.

The most efficient use of the CenterLine environment is to load files in
object form and keep only those you are working on directly in source form.
The way to do this is to first load everything as object, and to then
load individual files as source using the CenterLine "swap" command.
Using our example from above, if you wanted to only load the module "foo"
as source then you would type:

	make -f Makefile.cline program_obj
	swap foo.o

That's all there is to it.  Enjoy your CenterLine environment.  If you have
questions please send e-mail to:

		support@centerline.com

or call:
		(800)669-9165 or (617) 498-3100

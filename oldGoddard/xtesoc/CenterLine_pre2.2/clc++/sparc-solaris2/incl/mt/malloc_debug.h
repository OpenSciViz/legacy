/*ident	"@(#)cls4:incl-master/proto-headers/malloc_debug.h	1.1" */

#ifndef __MALLOC_DEBUG_H
#define __MALLOC_DEBUG_H
extern "C++" {

extern "C" {
	int malloc_debug(int);
	int malloc_verify();
}

}
#endif

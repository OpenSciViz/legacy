#ifndef _RLOCKS_H_
#define _RLOCKS_H_
extern "C++" {

//
//
//     recursive_mutex   The recursive mutex class
//     stream_rmutex     iostreams FOR INTERNAL USE ONLY subclass
//     			 of recursive_mutex.
//     stream_MT         The base class for any iostream class which
//     			 supports locking
//     stream_locker	 Cooperating class which does the locking
//
//

#include <thread.h>

#if _REENTRANT_STREAMS
#define STREAM_ONLY_LOCK()						\
  stream_locker strmlock((stream_MT *)this, stream_locker::lock_defer);	\
  if (test_safe_flag()) strmlock.lock();
#else
#define STREAM_ONLY_LOCK()
#endif

#if _REENTRANT_STREAMS
#define RDBUF_ONLY_LOCK()					\
  stream_locker  strmlock2(sb, stream_locker::lock_defer);	\
  if (test_safe_flag()) strmlock2.lock() ;
#else
#define RDBUF_ONLY_LOCK()
#endif

#if _REENTRANT_STREAMS
#define STREAM_RDBUF_LOCK(str_type)				\
  STREAM_ONLY_LOCK()						\
  streambuf* sb = str_type::rdbuf();				\
  RDBUF_ONLY_LOCK()
#else
#define STREAM_RDBUF_LOCK(str_type)
#endif

// this class supports recursive mutex. 
// It is used as a base class for stream_rec_mutex. 
#pragma disable_warn
class recursive_mutex 
{
   private:
	mutex_t user_mutex;	// The user provided mutex
	unsigned int thread_id; // Owning thread
  	int ref_count;		// Reference count for recursive calls
   public:
        recursive_mutex() {
	    thread_id = 0;
	    ref_count = 0;
	}
	void init();	 	// works as a constructor - cannot statically
	  			// initialize this class in the Solaris library.
 	void destroy();		// works as a destructor
	void lock();
	void unlock();
	~recursive_mutex();	// there is a destructor but no constructor
#if DEBUG
	friend void dump(recursive_mutex *);
#endif
};
#pragma enable_warn

//
// Not for public use. This is for iostreams only.
// Users may use/inherit from recursive_mutex directly if they
// need a recursive mutex locking class.
//
class stream_rmutex : private recursive_mutex 
{
  // The list of cooperating classes which use stream_rmutex:
  friend class ios;
  friend class streambuf;
  friend class stream_MT;
  friend class stream_locker;
  friend class Iostream_init;
  
};

typedef char stream_bool_t;

//
// Only turn this on if _REENTRANT is defined 
// See SunOS5.2 Guide to Multi-Thread Programming
// See use below in class stream_MT
//
#ifdef _REENTRANT
#define _REENTRANT_STREAMS 1
#else
#define _REENTRANT_STREAMS 0
#endif

//
// base class for MT-safe classes
//
class stream_MT 
{
private:
    stream_rmutex   lock;
    stream_bool_t   safe_flag;  // 1: safe object, 0: unsafe object
protected:
    stream_rmutex&  get_lock() { return lock; }
public:

    // safety turns on/off MT-safety at runtime.
    // A class inheriting from stream_MT can be MT-unsafe.
    enum { unsafe_object=0, safe_object=1 };
    stream_MT(stream_bool_t flag = safe_object) { safe_flag = flag; }
    stream_bool_t test_safe_flag() const { return safe_flag; }

    void set_safe_flag(stream_bool_t flag) { 
#	ifdef _REENTRANT
            lock.lock();
#	endif

        safe_flag = flag;
 
#	ifdef _REENTRANT
            lock.unlock();
#	endif
    }

    friend class stream_locker;
    friend class Iostream_init;
#if DEBUG
    friend void dump(stream_MT &);
#endif
};

//
// Class stream_locker for locking operations on an IOstream
//
class stream_locker 
{
public:
    enum lock_choice { lock_defer=0, lock_now=1 };
private:
    stream_rmutex *lockptr;
    int local_lock_ref_count;
    void init(stream_rmutex *lptr, const lock_choice fl) {
        lockptr = lptr;
	local_lock_ref_count = 0;
	if(fl == lock_now) lock();
    }
    void destroy_locker() {
        while(local_lock_ref_count--)
        lockptr->unlock(); 
    }
public:
    stream_locker(stream_MT& s, const lock_choice lock_flag=lock_now) { 
        init(&(s.lock),lock_flag);
    }
    stream_locker(stream_MT *s, const lock_choice lock_flag=lock_now) { 
        init(&(s->lock),lock_flag);
    }
    stream_locker(stream_rmutex&  l, const lock_choice lock_flag=lock_now) { 
        init(&l,lock_flag);
    }
    stream_locker(stream_rmutex  *l, const lock_choice lock_flag=lock_now) { 
        init(l,lock_flag);
    }
    ~stream_locker();
    void lock()   { 
      		    local_lock_ref_count++;
		    lockptr->lock();   
		  }
    void unlock() { 
		    local_lock_ref_count--;
		    lockptr->unlock(); 
		  }
};


}
#endif

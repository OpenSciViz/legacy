// Message Translation Specification for ToolTalk <-> CLIPC Gateway

// Each section of this file describes the translations necessary
// to do a single translation transaction.  This file specifies
// translations to support the the "Debugging Notification and
// Request Messages" as defined in "The CASE Interoperability
// Message Sets", Release 1.0, with the following general exceptions:
//
// 1.  The argument field debugClientID is never set or consumed.
//     I've got no idea what this is intended to do.
//
// 2.  Although CodeCenter has most of the semantics of a generic
//     debugger, there is a pretty major diversion when it comes
//     to loading files.  A generic debugger takes a load command
//     specifying the name of a single executable file.  This file
//     is accessed directly by the debugger, which also accesses the
//     source files used to create it.  In CodeCenter there is no
//     executable file, so appropriate semantic modifications have
//     been made to deal with a (generic debugger semantics) load.




// Startup Actions

START, NOTIFICATION, Startup:
    STRING $DEBUGGED_PROGRAM = " ";
    STRING $GATEWAY_CAT_SEPARATOR = "";
    STRING $CURRENT_SCOPE_LOCATION = "0";
    STRING $TRAP_SPEC = "<environment>";




// ToolTalk Do_Command request transaction
//
// REQUEST Do_Command IS (
//    IN string command_to_execute;
//    OUT string result;     Always empty!
// )

TT, REQUEST, Do_Command, Do_Command_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = MSG_TARGET.0;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = TRUE;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = TRUE;
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, Do_Command_Transaction_Type, TERMINAL:
    STRING MSG_TT_REQUEST.1 = "";
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, Do_Command_Transaction_Type, TERMINAL:
    STRING MSG_TT_REQUEST.1 = "";
    error_send(MSG_TT_REQUEST);




// ToolTalk Quit request transaction
//
// REQUEST Quit IS ()

TT, REQUEST, Quit, Quit_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_quit);
    send(MSG_CLIPC_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_quit, Quit_Transaction_Type, TERMINAL:
    // cxcmd_quit always returns an error in the reply, since
    // the executable dies before it can send a reply, and the
    // message server notices this and sends off an error reply.
    send(MSG_TT_REQUEST);




// ToolTalk Debug request transaction
//
// REQUEST Debug IS (
//    IN string file;
//		This argument to the request is really meaningless
//		to CodeCenter, but we keep the string and pass it back in
//		various notifications.
//    IN opaque debugClientID;
//		Ignored by the gateway and CodeCenter.
// )
//
// NOTIFICATION Debugged IS (
//    IN string file;
//		The requestor's file argument is simply returned.
// )

TT, REQUEST, Debug, Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "make -f ", "Makefile.cline";
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = TRUE;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = TRUE;
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, Debug_Transaction_Type, TERMINAL:
    alloc(MSG_TT_NOTIFICATION, Debugged);
    STRING MSG_TT_NOTIFICATION.0 = MSG_TT_REQUEST.0;
    // Keep the caller's name around for future use.
    STRING $DEBUGGED_PROGRAM = MSG_TT_REQUEST.0;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Unload_Debug request transaction
//
// REQUEST Unload_Debug IS ()
//
// NOTIFICATION Debug_Unloaded IS (
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
// )

TT, REQUEST, Unload_Debug, Unload_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "unload user";
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = TRUE;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = TRUE;
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, Unload_Debug_Transaction_Type, TERMINAL:
    alloc(MSG_TT_NOTIFICATION, Debug_Unloaded);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING $DEBUGGED_PROGRAM = "";
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, Unload_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Set_Trap request transaction
//
// REQUEST Set_Trap IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string action;
//		Action to take when the trap occurs.  String is of
//		the form do:<action>.  If string is empty, the line
//		"saber_stop(\"\")" is substituted.
//    OUT int trapID;
//		Breakpoint number assigned to this trap.  Valid values
//		range from 1 to n.  Values of 0 or -1 indicate an
//		error in setting the trap.
//    IN string trapSpec;
//		Specifies where the trap is to take place.  String is
//		either a function name or a filename line number pair
//		of the form: filename:linenumber.  If this is empty,
//		a trap will be set "here."
//    IN string condition;
//		Conditional expression.  This is evaluated at run time
//		at the trap location.  Iff it is TRUE, the trap action
//		is executed.  If this expression is empty, "TRUE" is
//		substituted.
// )
//
// NOTIFICATION Trap_Set IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string action;
//		Echo of request argument of the same name.
//    IN string trapSpec;
//		Echo of request argument of the same name.
//    IN string condition;
//		Echo of request argument of the same name.
//    IN int trapID;
//		Breakpoint number assigned to this trap.  Valid values
//		range from 1 to n.  Values of 0 or -1 indicate an
//		error in setting the trap.
// )
	
TT, REQUEST, Set_Trap, Set_Trap_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_action);
    STRING MSG_CLIPC_REQUEST.where.fileLoc.pathName =
        shell("echo ", MSG_TARGET.4, " | sed -e '/:/!d' -e '/:/s/:.*//'");
    INTEGER MSG_CLIPC_REQUEST.where.fileLoc.lineNumber =
	shell("echo ", MSG_TARGET.4, " | sed -e '/:/!d' -e '/:/s/.*://'");
    STRING MSG_CLIPC_REQUEST.where.funcName =
	shell("echo ", MSG_TARGET.4, " | sed -e '/:/d'");
    STRING MSG_CLIPC_REQUEST.where.locType =
	shell("echo funcName\:", MSG_CLIPC_REQUEST.where.funcName,
	      "\:fileLoc | sed -e '/::/s/funcName:://' ",
	      "-e '/:/s/:.*//'");
    INTEGER MSG_CLIPC_REQUEST.isAction = TRUE;
    STRING MSG_CLIPC_REQUEST.actionText =
	"{if (", shell("echo 1'", MSG_TARGET.5, "' |sed -e '/^1?/s/1//'"),
        ") {", shell("echo centerline_stop\(\"\"\)\;", MSG_TARGET.2,
	" | sed -e '/do:/s/.*do://'"), "}}";
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_action, Set_Trap_Transaction_Type, TERMINAL:
    INTEGER MSG_TT_REQUEST.3 = MSG_TARGET.breakNumber;
    alloc(MSG_TT_NOTIFICATION, Trap_Set);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = MSG_TT_REQUEST.2;
    STRING MSG_TT_NOTIFICATION.3 = MSG_TT_REQUEST.4;
    STRING MSG_TT_NOTIFICATION.4 = MSG_TT_REQUEST.5;
    STRING MSG_TT_NOTIFICATION.5 = MSG_TARGET.breakNumber;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_action, Set_Trap_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Delete_Trap request transaction
//
// REQUEST Delete_Trap IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string action;
//		Ignored.
//    IN string trapSpec;
//		Ignored.
//    IN string condition;
//		Ignored.
//    INOUT int trapID;
//		Breakpoint number of the trap to delete.  Valid values
//		range from 1 to n.  Returned values of 0 or -1 indicate an
//		error in setting the trap.
// )
//
// NOTIFICATION Trap_Deleted IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string action;
//		Not Set.
//    IN string trapSpec;
//		Not Set.
//    IN string condition;
//		Not Set.
//    IN int trapID;
//		Breakpoint number of the deleted trap.
// )
	
TT, REQUEST, Delete_Trap, Delete_Trap_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_delete);
    INTEGER MSG_CLIPC_REQUEST.itemNumber = MSG_TARGET.5;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_delete, Delete_Trap_Transaction_Type, TERMINAL:
    alloc(MSG_TT_NOTIFICATION, Trap_Deleted);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.5 = MSG_TT_REQUEST.5;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_delete, Delete_Trap_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Execute_Debug request transaction
//
// REQUEST Execute_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string stdin;
//		Ignored.
//    IN string stdout;
//		Ignored.
//    IN string stderr;
//		Ignored.
//    IN string args;
//		Arguments for the run.
// )
//
// NOTIFICATION Debug_Executing IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string stdin;
//		Not Set.
//    IN string stdout;
//		Not Set.
//    IN string stderr;
//		Not Set.
//    IN string args;
//		Arguments for the run.
// )
	
TT, REQUEST, Execute_Debug, Execute_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "run ", MSG_TARGET.5;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = TRUE;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = TRUE;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_exec, Execute_Debug_Transaction_Type, TERMINAL:
    alloc(MSG_TT_NOTIFICATION, Debug_Executing);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.5 = MSG_TT_REQUEST.5;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, Execute_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Stop_Debug request transaction
//
// The CASEI message spec on this gives no clue what this message
// is for.  Since there is also a Signal_Debug request, I assume this
// request is to set a breakpoint, and the document has some of the
// descriptions of the parameters wrong.
//
// REQUEST Stop_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string trapSpec;
//		Specifies where the trap is to take place.  String is
//		either a function name or a filename line number pair
//		of the form: filename:linenumber.  If this is empty,
//		a trap will be set "here."
//    OUT int trapID;
//		This doesn't appear in the CASEI Messages for some
//		reason.
// )
//
// NOTIFICATION Trap_Set IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string action;
//		Empty, indicating a breakpoint.
//    IN string trapSpec;
//		Echo of request argument of the same name.
//    IN string condition;
//		Empty.
//    IN int trapID;
//		Breakpoint number assigned to this trap.  Valid values
//		range from 1 to n.  Values of 0 or -1 indicate an
//		error in setting the trap.
// )
	
TT, REQUEST, Stop_Debug, Stop_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_action);
    STRING MSG_CLIPC_REQUEST.where.fileLoc.pathName =
        shell("echo ", MSG_TARGET.2, " | sed -e '/:/!d' -e '/:/s/:.*//'");
    INTEGER MSG_CLIPC_REQUEST.where.fileLoc.lineNumber =
	shell("echo ", MSG_TARGET.2, " | sed -e '/:/!d' -e '/:/s/.*://'");
    STRING MSG_CLIPC_REQUEST.where.funcName =
	shell("echo ", MSG_TARGET.2, " | sed -e '/:/d'");
    STRING MSG_CLIPC_REQUEST.where.locType =
	shell("echo funcName\:", MSG_CLIPC_REQUEST.where.funcName,
	      "\:fileLoc | sed -e '/::/s/funcName:://' ",
	      "-e '/:/s/:.*//'");
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_action, Stop_Debug_Transaction_Type, TERMINAL:
    INTEGER MSG_TT_REQUEST.3 = MSG_TARGET.breakNumber;
    alloc(MSG_TT_NOTIFICATION, Trap_Set);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = "";
    STRING MSG_TT_NOTIFICATION.3 = MSG_TT_REQUEST.2;
    STRING MSG_TT_NOTIFICATION.4 = "";
    STRING MSG_TT_NOTIFICATION.5 = MSG_TARGET.breakNumber;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_action, Stop_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Resume_Debug request transaction
//
// REQUEST Resume_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    OPTIONAL IN string trapSpec;
//		Ignored.
// )
//
// NOTIFICATION Debug_Resumed IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string trapSpec;
//		Location stopped at, and resuming from.
// )
	
TT, REQUEST, Resume_Debug, Resume_Debug_Transaction_Type, INITIAL:
    alloc(MSG_TT_NOTIFICATION, Debug_Resumed);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = $TRAP_SPEC;
    alloc(MSG_CLIPC_REQUEST, cxcmd_continue);
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, Resume_Debug_Transaction_Type, TERMINAL:
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_continue, Resume_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);
    



// ToolTalk RunUntil_Debug request transaction
//
// REQUEST RunUntil_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string trapSpec;
//		Specifies where to stop.
//    OUT int trapID;
//		This doesn't appear in the CASEI Messages.
// )
//
// This translation sets a breakpoint at the argument location.
// If that operation failed, it returns an error.  If it was
// successful, it then sets a variable indicating the number
// of the RunUntil breakpoint, and runs the program.  The temporary
// breakpoint is deleted once it is hit.
	
TT, REQUEST, RunUntil_Debug, RunUntil_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_action);
    STRING MSG_CLIPC_REQUEST.where.fileLoc.pathName =
        shell("echo ", MSG_TARGET.2, " | sed -e '/:/!d' -e '/:/s/:.*//'");
    INTEGER MSG_CLIPC_REQUEST.where.fileLoc.lineNumber =
	shell("echo ", MSG_TARGET.2, " | sed -e '/:/!d' -e '/:/s/.*://'");
    STRING MSG_CLIPC_REQUEST.where.funcName =
	shell("echo ", MSG_TARGET.2, " | sed -e '/:/d'");
    STRING MSG_CLIPC_REQUEST.where.locType =
	shell("echo funcName\:", MSG_CLIPC_REQUEST.where.funcName,
	      "\:fileLoc | sed -e '/::/s/funcName:://' ",
	      "-e '/:/s/:.*//'");
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_action, RunUntil_Debug_Transaction_Type:
    // Set the reply field.
    INTEGER MSG_TT_REQUEST.3 = MSG_TARGET.breakNumber;

    // Keep the ID around so we can delete the breakpoint after it's hit.
    STRING $RUNUNTIL_BREAK_ID = MSG_TARGET.breakNumber;

    // Send Trap_Set notification, too.
    alloc(MSG_TT_NOTIFICATION, Trap_Set);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = "";
    STRING MSG_TT_NOTIFICATION.3 = MSG_TT_REQUEST.2;
    STRING MSG_TT_NOTIFICATION.4 = "";
    STRING MSG_TT_NOTIFICATION.5 = MSG_TARGET.breakNumber;
    send(MSG_TT_NOTIFICATION);

    // Start the run.
    alloc(MSG_CLIPC_REQUEST, cxcmd_continue);
    send(MSG_CLIPC_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_action, RunUntil_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);


CLIPC, REPLY, cxcmd_continue, RunUntil_Debug_Transaction_Type:
    alloc(MSG_TT_NOTIFICATION, Debug_Resumed);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = $TRAP_SPEC;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

    // The CX is now running.  Queue up the request to delete the
    // temporary breakpoint.
    alloc(MSG_CLIPC_REQUEST, cxcmd_delete);
    INTEGER MSG_CLIPC_REQUEST.itemNumber = $RUNUNTIL_BREAK_ID;
    send(MSG_CLIPC_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_continue, RunUntil_Debug_Transaction_Type, TERMINAL:
    error_send(MSG_TT_REQUEST);


CLIPC, REPLY, cxcmd_delete, RunUntil_Debug_Transaction_Type, TERMINAL:
    // The CX stopped and the temporary breakpoint was deleted.
    alloc(MSG_TT_NOTIFICATION, Trap_Deleted);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.5 = $RUNUNTIL_BREAK_ID;
    send(MSG_TT_NOTIFICATION);

CLIPC, ERROR_REPLY, cxcmd_delete, RunUntil_Debug_Transaction_Type, TERMINAL:
    // We don't care about the error- something might have beat us to it.
    echo("Warning: RunUntil breakpoint ", $RUNUNTIL_BREAK_ID,
	 " already deleted. ");

    


// ToolTalk Signal_Debug request transaction
//
// The spec for this message is unintelligable.  It looks like it
// may be intended as an asynchronous stop of an executing program.
// We currently have no CLIPC message means to do this.




// ToolTalk StepInto_Debug request transaction
//
// REQUEST StepInto_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    OUT string trapSpec;
//		Specifies where stopped.
// )

TT, REQUEST, StepInto_Debug, StepInto_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "step";
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, StepInto_Debug_Transaction_Type:
    STRING MSG_TT_REQUEST.2 = $TRAP_SPEC;
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, StepInto_Debug_Transaction_Type:
    error_send(MSG_TT_REQUEST);

    


// ToolTalk StepOut_Debug request transaction
//
// REQUEST StepOut_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    OUT string trapSpec;
//		Specifies where stopped.
// )

TT, REQUEST, StepOut_Debug, StepOut_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "stepout";
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, StepOut_Debug_Transaction_Type:
    STRING MSG_TT_REQUEST.2 = $TRAP_SPEC;
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, StepOut_Debug_Transaction_Type:
    error_send(MSG_TT_REQUEST);




// ToolTalk StepOver_Debug request transaction
//
// REQUEST StepOver_Debug IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    OUT string trapSpec;
//		Specifies where stopped.
// )

TT, REQUEST, StepOver_Debug, StepOver_Debug_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "next";
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, StepOver_Debug_Transaction_Type:
    STRING MSG_TT_REQUEST.2 = $TRAP_SPEC;
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec_cmd, StepOver_Debug_Transaction_Type:
    error_send(MSG_TT_REQUEST);
    



// ToolTalk Describe_DebugSymbol request transaction
//
// REQUEST Describe_DebugSymbol IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string symbol;
//		The symbol to describe.
//    OUT string description;
//		Description.
// )

TT, REQUEST, Describe_DebugSymbol, Describe_DebugSymbol_Transaction_Type,
INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_whatis);
    STRING MSG_CLIPC_REQUEST.idName = MSG_TARGET.2;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_whatis, Describe_DebugSymbol_Transaction_Type, TERMINAL:
    // First build the command to be passed to the shell in a
    // string variable.
    STRING $COMMAND = "echo ";
    STRING $GATEWAY_CAT_SEPARATOR = "'\\\n%'1: ";
    STRING $COMMAND = $COMMAND, "'1: ";
    STRING $COMMAND = $COMMAND,
	MSG_TARGET.useList[*]symbolStorage, "'\\\n";
    STRING $GATEWAY_CAT_SEPARATOR = "'\\\n%'2: ";
    STRING $COMMAND = $COMMAND, "%'2: ";
    STRING $COMMAND = $COMMAND,
	MSG_TARGET.useList[*]symbolType.typeBefore, "'\\\n";
    STRING $GATEWAY_CAT_SEPARATOR = "'\\\n%'3: ";
    STRING $COMMAND = $COMMAND, "%'3: ";
    STRING $COMMAND = $COMMAND,
	MSG_TARGET.useList[*]symbol.visibleName, "'\\\n";
    STRING $GATEWAY_CAT_SEPARATOR = "'\\\n%'4: ";
    STRING $COMMAND = $COMMAND, "%'4: ";
    STRING $COMMAND = $COMMAND,
	MSG_TARGET.useList[*]symbolType.typeAfter, "'\\\n";
    STRING $GATEWAY_CAT_SEPARATOR = "";
    STRING $COMMAND = $COMMAND,
	" | sed -e '1,$y/%/\\n/' | awk \\\n",
	"'BEGIN {i1 = 0; i2 = 0; i3 = 0; i4 = 0;}'\\\n",
	"'/1: / {a1[i1] = substr($0, 4, length($0) - 3); i1++;}'\\\n",
	"'/2: / {a2[i2] = substr($0, 4, length($0) - 3); i2++;}'\\\n",
	"'/3: / {a3[i3] = substr($0, 4, length($0) - 3); i3++;}'\\\n",
	"'/4: / {a4[i4] = substr($0, 4, length($0) - 3); i4++;}'\\\n",
	"'END {for (n = 0; n < i2; n++) {print a1[n], a2[n], a3[n], a4[n];}}'";
    STRING MSG_TT_REQUEST.3 = shell($COMMAND);

    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_whatis, Describe_DebugSymbol_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);
    



// ToolTalk Assign_DebugExpression request transaction
//
// REQUEST Assign_DebugExpression IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string lhs;
//		The lvalue to assign to.
//    IN string expression;
//		Evaluate this expression and assign it to the lhs.
// )

TT, REQUEST, Assign_DebugExpression, Assign_DebugExpression_Transaction_Type,
INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_exec);
    STRING MSG_CLIPC_REQUEST.commandLine = "assign ", MSG_TARGET.2, " = ",
					   MSG_TARGET.3;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoCommand = FALSE;
    INTEGER MSG_CLIPC_REQUEST.cxcmdEchoOutput = FALSE;
    send(MSG_CLIPC_REQUEST);

CLIPC, NOTIFICATION, event_ready, Assign_DebugExpression_Transaction_Type,
TERMINAL:
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_exec, Assign_DebugExpression_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);




// ToolTalk Eval_DebugExpression request transaction
//
// REQUEST Eval_DebugExpression IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN string expression;
//		Evaluate this expression and assign it to the lhs.
//    OUT string result;
//		Result of the evaluation.
// )

TT, REQUEST, Eval_DebugExpression, Eval_DebugExpression_Transaction_Type,
INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_print);
    INTEGER MSG_CLIPC_REQUEST.typeDepth = 1;
    STRING MSG_CLIPC_REQUEST.expr = MSG_TARGET.2;
    INTEGER MSG_CLIPC_REQUEST.echoCommand = FALSE;
    INTEGER MSG_CLIPC_REQUEST.echoOutput = FALSE;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_print, Eval_DebugExpression_Transaction_Type, TERMINAL:
    STRING MSG_TT_REQUEST.3 = MSG_TARGET.rvalueString;
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_print, Eval_DebugExpression_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);
    



// ToolTalk Get_DebugContext request transaction
//
// REQUEST Get_DebugContext IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    OUT string frames;
//		Comma-separated list of stack frames.
//    OUT int current;
//		The current stack frame.
// )

TT, REQUEST, Get_DebugContext, Get_DebugContext_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_whereami);
    INTEGER MSG_CLIPC_REQUEST.numLevels = 1000;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_whereami, Get_DebugContext_Transaction_Type, TERMINAL:
    STRING $GATEWAY_CAT_SEPARATOR = "(), ";
    STRING $CURRENT_FRAME_LIST =
      MSG_TARGET.execLocation[@]function.visibleName;
    STRING $CURRENT_FRAME_LIST = $CURRENT_FRAME_LIST, "()";
    STRING $GATEWAY_CAT_SEPARATOR = "";
    STRING MSG_TT_REQUEST.2 = $CURRENT_FRAME_LIST;
    // Result of expr is piped through expand because expr has wierd
    // status values.  Expand is just invoked for its status-setting
    // side effects.
    STRING MSG_TT_REQUEST.3 = shell("expr ", 
                                    MSG_TARGET.numScopeFrames,
				    " - 1 | expand");
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_whereami, Get_DebugContext_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);
    



// ToolTalk Set_DebugContext request transaction
//
// REQUEST Set_DebugContext IS (
//    IN string file;
//		Ignored.
//    IN opaque debugClientID;
//		Ignored.
//    IN int current;
//		The desired stack frame.
// )

TT, REQUEST, Set_DebugContext, Set_DebugContext_Transaction_Type, INITIAL:
    alloc(MSG_CLIPC_REQUEST, cxcmd_whereami);
    INTEGER MSG_CLIPC_REQUEST.numLevels = 1000;
    send(MSG_CLIPC_REQUEST);

CLIPC, REPLY, cxcmd_whereami, Set_DebugContext_Transaction_Type:
    alloc(MSG_CLIPC_REQUEST, cxcmd_setframe);
    STRING $GATEWAY_CAT_SEPARATOR = "(), ";
    STRING $CURRENT_FRAME_LIST =
	MSG_TARGET.execLocation[@]function.visibleName;
    STRING $CURRENT_FRAME_LIST = $CURRENT_FRAME_LIST, "()";
    STRING $GATEWAY_CAT_SEPARATOR = "";
    STRING $CURRENT_SCOPE_LOCATION =
	shell("expr ", MSG_CLIPC_REPLY.numScopeFrames, " - 1 | expand");
    INTEGER MSG_CLIPC_REQUEST.numLevels =
	shell("expr ", MSG_TT_REQUEST.2, " - ", $CURRENT_SCOPE_LOCATION,
	      " | expand");
    send(MSG_CLIPC_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_whereami, Set_DebugContext_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);
    
CLIPC, REPLY, cxcmd_setframe, Set_DebugContext_Transaction_Type, TERMINAL:
    alloc(MSG_TT_NOTIFICATION, DebugContext_Set);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = $CURRENT_FRAME_LIST;
    // Note that event_up and event_down rules are going to
    // modify $CURRENT_SCOPE_LOCATION appropriately.
    INTEGER MSG_TT_NOTIFICATION.3 = $CURRENT_SCOPE_LOCATION;
    send(MSG_TT_NOTIFICATION);
    send(MSG_TT_REQUEST);

CLIPC, ERROR_REPLY, cxcmd_setframe, Set_DebugContext_Transaction_Type,
TERMINAL:
    error_send(MSG_TT_REQUEST);
    



// Notifications

// NOTIFICATION Debug_Stopped IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string trapSpec;
//		Function name, or source file:line number of
//		this trap.
// )
	
CLIPC, NOTIFICATION, event_stop_loc:
    STRING $TRAP_SPEC = shell("echo ", MSG_TARGET.funcName.visibleName,
			      "\(\) at ",
	                      MSG_TARGET.filePath.visiblePath, ":",
                              MSG_TARGET.lineNumber,
                              " | sed -e '/^()/s/() at //' ");

    // Send Debug_Stopped notification
    alloc(MSG_TT_NOTIFICATION, Debug_Stopped);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.2 = $TRAP_SPEC;
    send(MSG_TT_NOTIFICATION);




// NOTIFICATION Trap_Hit IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN string action;
//		Not Set.
//    IN string trapSpec;
//		Function name, or source file:line number of
//		this trap.
//    IN string condition;
//		Not Set.
//    IN int trapID;
//		Break number of the trap hit.
// )

CLIPC, NOTIFICATION, event_break_loc:
    // Send Trap_Hit notification
    alloc(MSG_TT_NOTIFICATION, Trap_Hit);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    STRING MSG_TT_NOTIFICATION.3 = $TRAP_SPEC;
    INTEGER MSG_TT_NOTIFICATION.5 = MSG_TARGET.breakNumber;
    send(MSG_TT_NOTIFICATION);




// NOTIFICATION Debug_Exited IS {
//    IN string file;
//		The name given for this "program" when its load
//		was requested is returned here.
//    IN opaque debugClientID;
//		Not Set.
//    IN int exitStatus;
//		Return value of main.
// )
//
CLIPC, NOTIFICATION, event_run_end:
    alloc(MSG_TT_NOTIFICATION, Debug_Exited);
    STRING MSG_TT_NOTIFICATION.0 = $DEBUGGED_PROGRAM;
    INTEGER MSG_TT_NOTIFICATION.2 = MSG_TARGET.returnValue;
    send(MSG_TT_NOTIFICATION);
    STRING $TRAP_SPEC = "<environment>";




CLIPC, NOTIFICATION, event_up:
    STRING $CURRENT_SCOPE_LOCATION = 
	shell("expr ", $CURRENT_SCOPE_LOCATION, " - ", MSG_TARGET.frames,
              " | expand");




CLIPC, NOTIFICATION, event_down:
    STRING $CURRENT_SCOPE_LOCATION = 
	shell("expr ", $CURRENT_SCOPE_LOCATION, " + ", MSG_TARGET.frames,
              " | expand");


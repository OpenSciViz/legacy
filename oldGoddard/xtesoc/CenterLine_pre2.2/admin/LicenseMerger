#!/bin/sh
# Copyright (c) 1986, 1994 by CenterLine Software, Inc.

# Check first argument 
case ${1-unset} in
 unset | '' ) # unset
  >&2 echo
  >&2 echo "ERROR: The first FLEXlm license file must be specified."
  >&2 echo
  >&2 echo "       USAGE: LicenseMerger license_file1 license_file2"
  >&2 echo
  exit 1
  ;;
 /*         ) # correct format
  LICENSE_ONE=${1}
  if [ ! -f ${LICENSE_ONE} ] ; then
     >&2 echo
     >&2 echo "ERROR: The first FLEXlm license file specified,"
     >&2 echo "         ${LICENSE_ONE}"
     >&2 echo "       does not exist."
     >&2 echo
     exit 1
  fi
  ;;
 *          ) # not an absolute path
  if [ ! -f ${LICENSE_ONE} ] ; then
     >&2 echo
     >&2 echo "ERROR: The first FLEXlm license file must be specified by an"
     >&2 echo "       absolute path name, such as /foo/bar/license.dat."
     >&2 echo
     exit 1
  fi
  ;;
esac

# Check second argument 
case ${2-unset} in
 unset | '' ) # unset
  >&2 echo
  >&2 echo "ERROR: The second FLEXlm license file must be specified."
  >&2 echo
  >&2 echo "       USAGE: LicenseMerger license_file1 license_file2"
  >&2 echo
  exit 2
  ;;
 /*         ) # correct format, but does the file exist?
  LICENSE_TWO=${2}
  if [ ! -f ${LICENSE_TWO} ] ; then
     >&2 echo
     >&2 echo "ERROR: The second FLEXlm license file specified,"
     >&2 echo "         ${LICENSE_TWO}"
     >&2 echo "       does not exist."
     >&2 echo
     exit 2
  fi
  ;;
 *          ) # not an absolute path
  if [ ! -f ${LICENSE_TWO} ] ; then
     >&2 echo
     >&2 echo "ERROR: The second FLEXlm license file must be specified by an"
     >&2 echo "       absolute path name, such as /foo/bar/license.dat."
     >&2 echo
     exit 3
  fi
  ;;
esac

# Make certain that the path makes sense.
case ${PATH-unset} in
 unset | '' ) PATH=/bin:/etc:/usr/bin:/usr/etc:/usr/ucb       ;;
 *          ) PATH=/bin:/etc:/usr/bin:/usr/etc:/usr/ucb:$PATH ;;
esac
export PATH

# Make certain both sets of licenses use the same server(s).
SERVERS_ONE=`/bin/sed -n '/SERVER/p' ${LICENSE_ONE}  \
             | /bin/awk '{ print $2$3 }' | /bin/sort \
             | tr "\012" " " | tr "\011" " "`
SERVERS_TWO=`/bin/sed -n '/SERVER/p' ${LICENSE_TWO}  \
             | /bin/awk '{ print $2$3 }' | /bin/sort \
             | tr "\012" " " | tr "\011" " "`
if [ "$SERVERS_ONE" != "$SERVERS_TWO" ]; then
   >&2 echo
   >&2 echo "ERROR: The two license files' SERVER line(s) do not specify"
   >&2 echo "       the exact same servers. These two license files "
   >&2 echo "       can NOT be merged."
   >&2 echo 
   exit 4
fi

# Ok, so display the new merged license files contents.
/bin/sed -n '/SERVER/p' ${LICENSE_ONE}
echo 
/bin/sed '/^SERVER/d' ${LICENSE_ONE}
echo 
/bin/sed '/^SERVER/d' ${LICENSE_TWO}
 
exit 0

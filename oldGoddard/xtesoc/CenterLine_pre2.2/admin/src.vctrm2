#!/bin/sh
##########################################################################
#  Copyright 1993, 1994 Visual Edge Software Ltd. and CenterLine Software, Inc.
#
# ALL RIGHTS RESERVED.  This notice is  intended  as  a  precaution
# against  inadvertent publication, and shall not be deemed to con-
# stitute an acknowledgment that publication has  occurred  nor  to
# imply  any  waiver  of confidentiality.  The year included in the
# notice is the year of the creation of the work.
##########################################################################
##########
#@d.19940729 # This is an age stamp, do not remove.

######
# Get path to this startup script
dir=`/bin/expr \( x"$0" : x'\(/\)$' \) \| \( x"$0" : x'\(.*\)/[^/]*$' \) \| .`
VIEWCENTER_TOP=`cd $dir/.. ; /bin/pwd`
######

######
# Determine the invoking platform type.
ARCH_OS=`${VIEWCENTER_TOP}/admin/platform`

# Check to see if invocation is possible on this platform.
if [ ! -f ${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/bin/vctrm ] ; then
   # this means that the invoking platform type is not installed!
   >&2 echo " "
   >&2 echo "ERROR: ViewCenter for Motif has not been installed to run on this type"
   >&2 echo "       of machine/operating system: `/bin/uname -mrs`."
   >&2 echo " "
   >&2 echo "       Aborting..."
   >&2 echo " "
   exit 1
fi
######


######
# is VCTRM_SILENT set? ...then do not check for appropriate environment.
case ${VCTRM_SILENT-/} in
 / )
  ######
  # Check for OS version
  OS_warn=true
  case ${ARCH_OS} in
   sparc-sunos4   ) case `/bin/uname -r` in
                     4.1.3* ) OS_warn=false ;;
                    esac
                    CENTERLINE_ECHO_N=BSD
                    ;;
   sparc-solaris2 ) case `/bin/uname -r` in
                     5.2 ) OS_warn=false ;;
                    esac
                    CENTERLINE_ECHO_N=SYSV
                    ;;
   m88k-svr4      ) CENTERLINE_ECHO_N=SYSV 
                    OS_warn=false
                    ;;
   pa-hpux8       ) case `/bin/uname -r | /usr/bin/tr -d "[A-Z][a-z]" | /bin/sed 's/\.//g' | /bin/sed 's/^0//'` in
                     9* ) OS_warn=false
                          CENTERLINE_ECHO_N=SYSV ;;
                     8* ) >&2 echo "ERROR: ViewCenter for Motif is supported only for"
                          >&2 echo "       HP-UX 9, this system is running HP-UX 8."
                          >&2 echo " "
                          >&2 echo "       Aborting..."
                          >&2 echo " "
                          exit 1
                    esac
                    ;;
  esac
  export CENTERLINE_ECHO_N
  if $OS_warn ; then
     OS_running=`/bin/uname -rs`
     case ${ARCH_OS} in
      sparc-sunos4   ) echo "WARNING: ViewCenter for Motif is supported only for"
                       echo "         SunOS 4.1.3 (Solaris 1.2), this system is running ${OS_running} ."
                       ;;
      sparc-solaris2 ) echo "WARNING: ViewCenter for Motif is supported only for"
                       echo "         SunOS 5.2 (Solaris 2.2), this system is running ${OS_running} ."
                       ;;
      pa-hpux8       ) echo "WARNING: ViewCenter for Motif is supported only for"
                       echo "         HP-UX 9, this system is running ${OS_running} ."
                       ;;
     esac
     echo
     ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
     read continue
     case ${continue-n} in
      y* | Y* ) echo
                echo " Continuing at user request..."
                echo
                echo " Set the environment variable VCTRM_SILENT to turn off"
                echo " dependency checking and this warning message."
                echo
                ;;
      *       ) echo
                echo " Aborting at user request..."
                echo
                exit 0
                ;;
     esac
  fi

  ######
  # Check X11 and Motif are available...
  check_version=true
  if [ -f ${VIEWCENTER_TOP}/configs/vcenterinit ]; then
      for depend in X11INCDIR X11LIBDIR XMINCDIR XMLIBDIR
      do
          DEPEND_DIR=`eval "/bin/sed -n '/${ARCH_OS}:${depend}/p' ${VIEWCENTER_TOP}/configs/vcenterinit | /bin/sed 's/${ARCH_OS}:${depend}//'"`
          case $depend in
           X11INCDIR ) X11INCDIR=${DEPEND_DIR} ;;
           X11LIBDIR ) X11LIBDIR=${DEPEND_DIR} ;;
           XMINCDIR  ) XMINCDIR=${DEPEND_DIR}  ;;
           XMLIBDIR  ) XMLIBDIR=${DEPEND_DIR}  ;;
          esac
          if [ ! -r ${DEPEND_DIR} ]; then
             echo "WARNING: The directory ${DEPEND_DIR}"
             echo "         is not readable or does not exist."
             echo
             echo "         Elements with this directory are needed to run."
             echo
             ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
             read continue
             case ${continue-n} in
              y* | Y* ) echo 
                        echo " Continuing at user request..."
                        echo
                        echo " Set the environment variable VCTRM_SILENT to turn off"
                        echo " dependency checking and this warning message."
                        echo
                        check_version=false
                        ;;
              *       ) echo
                        echo " Aborting at user request..."
                        echo
                        exit 0
                        ;;
             esac
          fi
      done
  else
      >&2 echo
      >&2 echo "ERROR: This installation of ViewCenter for Motif is corrupted."
      >&2 echo
      >&2 echo "       Aborting..."
      >&2 echo
      exit 1
  fi
  ######

  ######
  # Check that it is X11R5 that is available...
  if $check_version ; then
     unset VCTRM_LIB_CHECK
     case ${ARCH_OS} in
      sparc-solaris2 | m88k-svr4 )
       VCTRM_LIB_CHECK=`/usr/ccs/bin/nm ${X11LIBDIR}/libX11.a | /bin/sed -n '/CIE/p' | /bin/sed '1q'`
       ;;
      sparc-sunos4 | pa-hpux8 )
       VCTRM_LIB_CHECK=`/bin/nm ${X11LIBDIR}/libX11.a | /bin/sed -n '/CIE/p' | /bin/sed '1q'`
       ;;
     esac
     case ${VCTRM_LIB_CHECK-/} in
      / | '' ) VCTRM_LIB_CHECK=true  ;;
      *      ) VCTRM_LIB_CHECK=false ;;
     esac
     
     if [ -f ${X11INCDIR}/X11/Xcms.h ]; then
          VCTRM_INC_CHECK=false
     else
          VCTRM_INC_CHECK=true
     fi
    
     if $VCTRM_LIB_CHECK ; then
        echo "WARNING: The directory ${X11LIBDIR}"
        echo  "        does not appear to contain X11R5 libraries."
        echo
        echo "         Motif 1.2 and ViewCenter for Motif require X11R5."
        echo
        ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
        read continue
        case ${continue-n} in
         y* | Y* ) echo 
                   echo " Continuing at user request..."
                   echo
                   echo " Set the environment variable VCTRM_SILENT to turn off"
                   echo " dependency checking and this warning message."
                   echo
                   check_version=false
                   ;;
         *       ) echo
                   echo " Aborting at user request..."
                   echo
                   exit 0
                   ;;
        esac
     fi
     if $VCTRM_INC_CHECK ; then
        echo "WARNING: The directory ${X11INCDIR}"
        echo "         does not appear to contain X11R5 header files."
        echo
        echo "         Motif 1.2 and ViewCenter for Motif require X11R5."
        echo
        ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
        read continue
        case ${continue-n} in
         y* | Y* ) echo 
                   echo " Continuing at user request..."
                   echo
                   echo " Set the environment variable VCTRM_SILENT to turn off"
                   echo " dependency checking and this warning message."
                   echo
                   check_version=false
                   ;;
         *       ) echo
                   echo " Aborting at user request..."
                   echo
                   exit 0
                   ;;
        esac
     fi
  fi
  ######

  ######
  # Check that it is Motif 1.2.2 that is available...
  if $check_version ; then
     unset VCTRM_LIB_CHECK
     unset VCTRM_INC_CHECK
     case ${ARCH_OS} in
       m88k-svr4 )
       MOTIF_VERSION="1.2.2"
       VCTRM_LIB_CHECK=`/usr/ccs/bin/nm ${XMLIBDIR}/libXm.a | /bin/sed -n '/_XmStrings/p' | /bin/sed '1q'`
       VCTRM_INC_CHECK="Indeterminable at this time."
       ;;
      sparc-solaris2 )
       MOTIF_VERSION="1.2.2"
       VCTRM_LIB_CHECK=`/usr/ccs/bin/nm ${XMLIBDIR}/libXm.a | /bin/sed -n '/_XmStrings/p' | /bin/sed '1q'`
       VCTRM_INC_CHECK=`/bin/sed -n '/XmGetDefaultTime/p' ${XMINCDIR}/Xm/XmP.h | /bin/sed '1q'`
       ;;
      sparc-sunos4   )
       MOTIF_VERSION="1.2.2"
       VCTRM_LIB_CHECK=`/bin/nm ${XMLIBDIR}/libXm.a | /bin/sed -n '/_XmStrings/p' | /bin/sed '1q'`
       VCTRM_INC_CHECK=`/bin/sed -n '/XmGetDefaultTime/p' ${XMINCDIR}/Xm/XmP.h | /bin/sed '1q'`
       ;;
      pa-hpux8       )
       MOTIF_VERSION="1.2"
       VCTRM_LIB_CHECK=`/bin/ls ${XMLIBDIR}/libXm.a |/bin/sed '1q'`
       VCTRM_INC_CHECK=`/bin/ls ${XMINCDIR}/Xm/XmP.h | /bin/sed '1q'`
       ;;
     esac

     case ${VCTRM_LIB_CHECK-/} in
      / | '' ) VCTRM_LIB_CHECK=true  ;;
      *      ) VCTRM_LIB_CHECK=false ;;
     esac

     case ${VCTRM_INC_CHECK-/} in
      / | '' ) VCTRM_INC_CHECK=true  ;;
      *      ) VCTRM_INC_CHECK=false ;;
     esac

     if $VCTRM_LIB_CHECK ; then
        echo "WARNING: The directory ${XMLIBDIR}"
        echo "         does not appear to contain Motif ${MOTIF_VERSION} libraries."
        echo
        echo "         ViewCenter for Motif requires Motif ${MOTIF_VERSION} ."
        echo
        ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
        read continue
        case ${continue-n} in
         y* | Y* ) echo 
                   echo " Continuing at user request..."
                   echo
                   echo " Set the environment variable VCTRM_SILENT to turn off"
                   echo " dependency checking and this warning message."
                   echo
                   check_version=false
                   ;;
         *       ) echo
                   echo " Aborting at user request..."
                   echo
                   exit 0
                   ;;
        esac
     fi
     if $VCTRM_INC_CHECK ; then
        echo "WARNING: The directory ${X11INCDIR}"
        echo "         does not appear to contain Motif ${MOTIF_VERSION} header files."
        echo
        echo "         ViewCenter for Motif requires Motif ${MOTIF_VERSION} ."
        echo
        ${VIEWCENTER_TOP}/admin/echo-n "         Continue with invocation of ViewCenter for Motif? [n] "
        read continue
        case ${continue-n} in
         y* | Y* ) echo 
                   echo " Continuing at user request..."
                   echo
                   echo " Set the environment variable VCTRM_SILENT to turn off"
                   echo " dependency checking and this warning message."
                   echo
                   check_version=false
                   ;;
         *       ) echo
                   echo " Aborting at user request..."
                   echo
                   exit 0
                   ;;
        esac
     fi
  fi
  ######
  ;;
 * ) ;;
esac
######

######
# Determine the license file. Remove this section if feasible.
IS_LIC_FILE=true
if [ -f ${VIEWCENTER_TOP}/configs/license.dat ]; then
    LM_LICENSE_FILE="${VIEWCENTER_TOP}/configs/license.dat"
    export LM_LICENSE_FILE
else
    case ${LM_LICENSE_FILE-/} in
     / | ''     ) error=true ;;
     /*         ) if [ ! -f ${LM_LICENSE_FILE} ]; then
                      error=true
                  else
                      error=false
                  fi         ;;
     *@*        ) IS_LIC_FILE=false
                  lic_type_check=`echo "$LM_LICENSE_FILE" | awk -F'@' '{ print $1 }' | /bin/sed -e "s+0++g" -e "s+1++g" -e "s+2++g" -e "s+3++g" -e "s+4++g" -e "s+5++g" -e "s+6++g" -e "s+7++g" -e "s+8++g" -e "s+9++g"`
                  case ${lic_type_check-0} in
                   0 | '' ) error=false ;;
                   * ) error=true ;;
                  esac       ;;
     *          ) error=true ;;
    esac
    if $error ; then
       >&2 echo
       >&2 echo "ERROR: No license file detected."
       >&2 echo
       >&2 echo "       There must be a valid license file named"
       >&2 echo
       >&2 echo "          ${VIEWCENTER_TOP}/configs/license.dat"
       >&2 echo
       >&2 echo "       or the environmental variable LM_LICENSE_FILE must"
       >&2 echo "       set to the absolute pathname of a valid license file."
       >&2 echo
       case ${LM_LICENSE_FILE-/} in
        / | '' )
         >&2 echo
         >&2 echo "       The environmental variable LM_LICENSE_FILE is unset"
         >&2 echo "       in your environment."
         >&2 echo
         ;;
        * )
         >&2 echo
         >&2 echo "       The environmental variable LM_LICENSE_FILE is set to"
         >&2 echo
         >&2 echo "          ${LM_LICENSE_FILE}"
         >&2 echo
         >&2 echo "       in your environment."
         >&2 echo
         ;;
       esac
       exit 2
    fi
fi
case ${LANG-/} in
 / | '' | english ) ;; # Do nothing!
 * ) if $IS_LIC_FILE ; then
       cp ${LM_LICENSE_FILE} /tmp/.cl_vctrm_lic.$$
       for feature in `/bin/sed -n '/^FEATURE VC.* centerline/p'  \
                       /tmp/.cl_vctrm_lic.$$ | awk '{ print $2 }'`
       do
          sed -e                                                              \
          "s/^FEATURE $feature centerline 2./FEATURE $feature centerline 2,/" \
          /tmp/.cl_vctrm_lic.$$ > /tmp/.cl_vctrm_lic1.$$
          /bin/mv /tmp/.cl_vctrm_lic1.$$ /tmp/.cl_vctrm_lic.$$
       done
       LM_LICENSE_FILE="/tmp/.cl_vctrm_lic.$$"
       export LM_LICENSE_FILE
       TEMP_LIC_FILE="$LM_LICENSE_FILE"
     fi
     ;;
esac
######

######
# ViewCenter for Motif explicit environmental variables
VCTRMDIR="${VIEWCENTER_TOP}/vctrm/${ARCH_OS}"

NLSPATH="${VCTRMDIR}/msg/C/%N"

if [ ${VCTRM_INTEGRATION_DIR:-/} = "/" ]; then
    XAPPLRESDIR="${VCTRMDIR}/newconfig/app-defaults"
elif [ -f ${VCTRM_INTEGRATION_DIR}/Vctrm ]; then
    XAPPLRESDIR="${VCTRM_INTEGRATION_DIR}"
else
    XAPPLRESDIR="${VCTRMDIR}/newconfig/app-defaults"
fi

######


#########################################################################
#                                                                       #
#    HACK          HACK             HACK          HACK        HACK      #
#                                                                       #
######                                                                  #
# Modify the users path to include the viewcenter specific directory.   #
#########################################################################
PATH="${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/bin:${PATH}"
export PATH
######

######
# Invoke a clms_registry daemon.
${VIEWCENTER_TOP}/${ARCH_OS}/bin/clms_registry &
######

######
# If CLMS_SESSION not set, start up a CLMS session. This variable is set if
#  viewcenterm was called from ObjectCenter or CodeCenter.
CLMS_SESSION=${CLMS_SESSION-`${VIEWCENTER_TOP}/${ARCH_OS}/bin/clms -v`}
######

######
# Determine if called from Objectcenter or CodeCenter.
#  Need to unset CENTERLINE_CORE, otherwise cannot start vctrm from *Center.
case ${CENTERLINE_CORE-/} in
 / | '' )                       ;;  # do nothing.
 *      ) unset CENTERLINE_CORE ;;
esac
######

######
# Export set environment before invoking
export VCTRMDIR XAPPLRESDIR NLSPATH CLMS_SESSION 
######

######
# Display the WGID and RELEASE No. before invoking.
case ${VCTRM_SILENT-/} in
 / ) if [ -f ${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/.relnum ]; then
         RELEASE=`/bin/sed '1q' ${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/.relnum`
     else
         RELEASE="Undetermined (is this install corrupted?)"
     fi
     if [ -f ${VIEWCENTER_TOP}/configs/support_defs ]; then
         WGID=`/bin/sed -n '/Workgroup_Id/p' ${VIEWCENTER_TOP}/configs/support_defs | /bin/sed 's/Workgroup_Id/Workgroup Id:/'`
         PHONE=`/bin/sed -n '/vctrm-Support_phone/p' ${VIEWCENTER_TOP}/configs/support_defs | /bin/sed 's/vctrm-Support_phone/Support Telephone:/'`
         EMAIL=`/bin/sed -n '/vctrm-Email_address/p' ${VIEWCENTER_TOP}/configs/support_defs | /bin/sed 's/vctrm-Email_address/Support EMAIL:/'`
     else
         WGID="Undetermined (is this install corrupted?)"
         PHONE="Undetermined (is this install corrupted?)"
         EMAIL="Undetermined (is this install corrupted?)"
     fi
     echo
     echo "Starting ViewCenter for Motif."
     echo
     echo "         ${WGID}"
     echo "              Release:	$RELEASE"
     echo "    ${PHONE}"
     echo "        ${EMAIL}"
     echo
     ;;
esac
######

######
# Invoke vctrm and determine the exit status.
case "$#" in
 0 ) ${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/bin/vctrm       ;;
 * ) ${VIEWCENTER_TOP}/vctrm/${ARCH_OS}/bin/vctrm "$@"  ;;
esac
EXIT_STATUS="$?"
######

######
# cleanup temporary license file.
case ${TEMP_LIC_FILE-0} in
 /tmp* ) /bin/rm -f ${TEMP_LIC_FILE} 2> /dev/null ;;
esac
######

######
# kill any "orphaned" clms binaries.
${VIEWCENTER_TOP}/${ARCH_OS}/bin/killms -n 2> /dev/null
######

######
# Exit with the status of vctrm.
exit "${EXIT_STATUS}"
######

# vctrm

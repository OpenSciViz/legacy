(let (core (or (getenv "CENTERLINE_CORE")
	       ("/usr/local/CenterLine/c_4.0.0-b1.0/sun4-41")))
  (setq load-path (cons (concat core "/misc/lisp") load-path)))
(load "clipc-core.el")
(load "clipc.el")
(cl-edit)

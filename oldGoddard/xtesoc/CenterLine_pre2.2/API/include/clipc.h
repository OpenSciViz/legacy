#if !defined(__CLIPC_H)
#define __CLIPC_H 1

#if defined(__cplusplus)
extern "C" {
#endif
#if defined(SABER) && !defined(__STDC__) && !defined(__cplusplus)
#include <varargs.h>
#endif
    
#if defined(__STDC__) || defined(__cplusplus)
#include <stdarg.h>
#endif
#if !defined(A)
#define A_DEFINED_IN_FILE 1
#if defined(__STDC__) || defined(__cplusplus)    
#define A(arglist) arglist
#else
#define A(arglist) ()
#endif
#endif /* !defined(A) */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
    
#define __CLIPC_VERSION_MAJOR 1
#define __CLIPC_VERSION_MINOR 28

/*
 * message types and flags
 */
enum clipc_msg_delclass {    
    CLIPC_DELCLASS_NONE     = 0x00,
    CLIPC_DELCLASS_ANY      = 0xff,
    CLIPC_DELCLASS_NOTIFY   = 0x01,
    CLIPC_DELCLASS_REQUEST  = 0x02,
    CLIPC_DELCLASS_CMSCPREQ = 0x04,
    CLIPC_DELCLASS_CMSCPRPL = 0x08,
    CLIPC_DELCLASS_QUERY    = 0x10,
    CLIPC_DELCLASS_PING     = 0x20
};

enum clipc_msgflag {
    CLIPC_MSGFLAG_ACKREQ = 0x01,
    CLIPC_MSGFLAG_QUEUE = 0x02,
    CLIPC_MSGFLAG_MEONLY = 0x04,
    CLIPC_MSGFLAG_SYNC = 0x08,
    CLIPC_MSGFLAG_REPLY = 0x10,
    CLIPC_MSGFLAG_INFO = 0x20,
    CLIPC_MSGFLAG_REPLY_TO_ME = 0x40,
    CLIPC_MSGFLAG_ERROR_IF_BUSY = 0x80       
};

/*
 * CLIPC process types
 */
#define CLIPC_PTYPE_HANDLER  1
#define CLIPC_PTYPE_LISTENER 2

/*
 * CLIPC message ids
 */
#define CLIPC_MSGID_ANY   0  /* matches any message id */
#define CLIPC_MSGID_NONE -1  /* signifies no message ids */

enum clipc_status {
    CLIPC_STATUS_OK = 0,
    CLIPC_STATUS_ERR = -1,
    CLIPC_STATUS_UNIMP = -2,
    CLIPC_STATUS_WOULDBLOCK = -3,
    CLIPC_STATUS_BADREAD = -4,    
    CLIPC_STATUS_BADFORMAT = -5 
};

enum clipc_type {
    CLIPC_TYPE_NONE = 0,
    CLIPC_TYPE_STATUS,
    CLIPC_TYPE_TYPE,
    CLIPC_TYPE_MSG,
    CLIPC_TYPE_VAL,
    CLIPC_TYPE_DICT,
    CLIPC_TYPE_ARRAY,
    CLIPC_TYPE_MEM,    
    CLIPC_TYPE_STRING,
    CLIPC_TYPE_INT,
    CLIPC_TYPE_FLOAT,
    CLIPC_TYPE_CHAR,
    CLIPC_TYPE_UNK,
    CLIPC_TYPE_NIL          
};

typedef enum clipc_status CLIPC_STATUS;
typedef enum clipc_type   CLIPC_TYPE;
typedef enum clipc_msg_delclass CLIPC_MSG_DELCLASS;
typedef enum clipc_msgflag CLIPC_MSGFLAG;
typedef struct clipc_dict *CLIPC_DICT;
typedef struct clipc_array *CLIPC_ARRAY;
typedef struct clipc_mem *CLIPC_MEM;
typedef struct clipc_string *CLIPC_STRING;
typedef struct clipc_int *CLIPC_INT;
typedef struct clipc_char *CLIPC_CHAR;
typedef struct clipc_float *CLIPC_FLOAT;
typedef struct clipc_msg *CLIPC_MSG;
typedef union clipc_value *CLIPC_VALUE;
typedef struct clipc_conn *CLIPC_CONN;
typedef struct clipc_msgqueue *CLIPC_MSGQUEUE;

/*
 * Memory management routines Section 2 of CLIPC Library Reference
 */

char *clipc_malloc A((CLIPC_MSG msg, int size));
char *clipc_strdup A((CLIPC_MSG msg, char *str));

CLIPC_STATUS clipc_free A((char *cp));
     
/*
 * Making Connections (Section 3)
 * connect.c
 */
CLIPC_ARRAY clipc_find_proc A((char *machine, char *uname, int pid));

CLIPC_STATUS clipc_udpsend_msg A((CLIPC_MSG msg, long ipaddr, short port,
			       int *fdp));
CLIPC_MSG clipc_udpsend_query A((CLIPC_MSG msg, long addr, short port,
			      int msec, CLIPC_STATUS *sp));
CLIPC_MSG clipc_udpread_msg A((int fd, struct sockaddr_in *addrp,
			    int msec, CLIPC_STATUS *sp));

CLIPC_DICT  clipc_server_query A((int wgid, int msec, CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_client_query A((char *machine, char *user, int pid, int msec,
			       CLIPC_STATUS *sp));
long get_broadcast_addr();

#define CLIPC_REGISTRY_PORT 18765
#define MSEC_FOREVER (int)~0

/*
 * Output Routines (Section 4)
 */

CLIPC_STATUS clipc_write_msg A((CLIPC_CONN conn, CLIPC_MSG msg));

CLIPC_MSG clipc_request_send A((CLIPC_CONN conn, CLIPC_MSG msg,
				CLIPC_STATUS *statusp));

CLIPC_STATUS clipc_notify_send A((CLIPC_CONN conn, CLIPC_MSG msg));
CLIPC_STATUS clipc_syncnotify_send A((CLIPC_CONN conn, CLIPC_MSG msg));

/*
 * Input Routines (Section 5)
 */

CLIPC_MSG clipc_read_msg A((CLIPC_CONN conn, CLIPC_STATUS *sp));
CLIPC_MSG  clipc_readnb_msg A((CLIPC_CONN conn, CLIPC_STATUS *sp));
CLIPC_STATUS clipc_unread_msg A((CLIPC_CONN conn, CLIPC_MSG msg));

/*
 * Messages, Dictionaries, Arrays, Values X
 *  alloc
 *  free
 *  add
 *  get
 */

/*
 * Messages
 */

CLIPC_MSG clipc_msg_alloc A((char *name, CLIPC_STATUS *sp));
CLIPC_MSG clipc_msg_dup A((CLIPC_MSG msg));
CLIPC_MSG clipc_msg_ref A((CLIPC_MSG msg));
CLIPC_MSG clipc_msg_alloc_vinit A((char *msg_name, va_list pvar));

CLIPC_MSG clipc_notify_alloc A((char *name, CLIPC_STATUS *sp));
CLIPC_MSG clipc_request_alloc A((char *name, int flags, CLIPC_STATUS *sp));
CLIPC_MSG clipc_syncnotify_alloc A((char *name, CLIPC_STATUS *sp));
CLIPC_MSG clipc_reply_alloc A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_STATUS clipc_msg_free A((CLIPC_MSG msg));


CLIPC_STATUS clipc_msg_addval A((CLIPC_MSG msg, char *name, CLIPC_VALUE val));
CLIPC_STATUS clipc_msg_addstring A((CLIPC_MSG msg, char *name, char *str));
CLIPC_STATUS clipc_msg_addmem A((CLIPC_MSG msg, char *name, int len,
				  char *mem));
CLIPC_STATUS clipc_msg_addchar A((CLIPC_MSG msg, char *name, char c)); 
CLIPC_STATUS clipc_msg_addint A((CLIPC_MSG msg, char *name, int num));
CLIPC_STATUS clipc_msg_addfloat A((CLIPC_MSG msg, char *name, double num));
CLIPC_STATUS clipc_msg_adddict A((CLIPC_MSG msg, char *name, CLIPC_DICT dict));
CLIPC_STATUS clipc_msg_addarray A((CLIPC_MSG msg, char *name,
				   CLIPC_ARRAY array));
CLIPC_STATUS clipc_msg_genadd A((CLIPC_MSG msg, ...));
CLIPC_STATUS clipc_msg_vgenadd A((CLIPC_MSG msg, va_list pvar));

CLIPC_MSG_DELCLASS clipc_msg_getdelclass A((CLIPC_MSG msg, CLIPC_STATUS *sp));
char *clipc_msg_getname A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_DICT clipc_msg_getenvlp A((CLIPC_MSG msg, CLIPC_STATUS *sp));
int clipc_msg_getid A((CLIPC_MSG msg, CLIPC_STATUS *sp));
int clipc_msg_getflags A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_STATUS clipc_msg_setflags A((CLIPC_MSG msg, CLIPC_MSGFLAG flags));
CLIPC_STATUS clipc_msg_setdelclass A((CLIPC_MSG msg, CLIPC_MSG_DELCLASS type));
CLIPC_DICT clipc_msg_getbody A((CLIPC_MSG msg, CLIPC_STATUS *sp));


CLIPC_VALUE clipc_msg_getval A((CLIPC_MSG msg, char *name, CLIPC_STATUS *sp));
char *clipc_msg_getstring A((CLIPC_MSG msg, char *name, CLIPC_STATUS *sp));
char *clipc_msg_getmem A((CLIPC_MSG msg, char *name, int *lenp,
			  CLIPC_STATUS *sp));
char clipc_msg_getchar A((CLIPC_MSG msg, char *name, CLIPC_STATUS *sp));     
long clipc_msg_getint A((CLIPC_MSG msg, char *name, CLIPC_STATUS *sp));
double clipc_msg_getfloat A((CLIPC_MSG msg, char *name, CLIPC_STATUS *sp));
CLIPC_DICT clipc_msg_getdict A((CLIPC_MSG msg, char *name,
				CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_msg_getarray A((CLIPC_MSG msg, char *name,
				  CLIPC_STATUS *sp));


/*
 * Dictionaries
 */

CLIPC_DICT clipc_dict_alloc A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_DICT clipc_dict_dup A((CLIPC_MSG msg, CLIPC_DICT dict, CLIPC_STATUS *sp));
CLIPC_STATUS clipc_dict_free A((CLIPC_DICT dict));


CLIPC_STATUS clipc_dict_addval A((CLIPC_DICT dict, char *name,CLIPC_VALUE val));
CLIPC_STATUS clipc_dict_rmval A((CLIPC_DICT dict, char *name));
CLIPC_STATUS clipc_dict_addstring A((CLIPC_DICT dict, char *name, char *str));
CLIPC_STATUS clipc_dict_addmem A((CLIPC_DICT dict, char *name, int len,
				  char *mem));
CLIPC_STATUS clipc_dict_addchar A((CLIPC_DICT dict, char *name, char c)); 
CLIPC_STATUS clipc_dict_addint A((CLIPC_DICT dict, char *name, int num));
CLIPC_STATUS clipc_dict_addfloat A((CLIPC_DICT dict, char *name, double num));
CLIPC_STATUS clipc_dict_adddict A((CLIPC_DICT dict, char *name,
				    CLIPC_DICT val)); 
CLIPC_STATUS clipc_dict_addarray A((CLIPC_DICT dict, char *name,
				    CLIPC_ARRAY val)); 
CLIPC_STATUS clipc_dict_genadd A((CLIPC_DICT dict, ...));
CLIPC_STATUS clipc_dict_vgenadd A((CLIPC_DICT dict, va_list pvar));

CLIPC_VALUE clipc_dict_getval A((CLIPC_DICT dict, char *name,
				 CLIPC_STATUS *sp));
char *clipc_dict_getstring A((CLIPC_DICT dict, char *name, CLIPC_STATUS *sp));
char *clipc_dict_getmem A((CLIPC_DICT dict, char *name, int *lenp,
			   CLIPC_STATUS *sp));
char clipc_dict_getchar A((CLIPC_DICT dict, char *name, CLIPC_STATUS *sp));
long clipc_dict_getint A((CLIPC_DICT dict, char *name, CLIPC_STATUS *sp));
double clipc_dict_getfloat A((CLIPC_DICT dict, char *name, CLIPC_STATUS *sp));
CLIPC_DICT clipc_dict_getdict A((CLIPC_DICT dict, char *name,
				 CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_dict_getarray A((CLIPC_DICT dict, char *name,
				 CLIPC_STATUS *sp));

CLIPC_ARRAY clipc_dict_getfieldnames A((CLIPC_DICT dict, CLIPC_STATUS *sp));

/*
 * Arrays
 */
CLIPC_ARRAY clipc_array_alloc A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_array_dup A((CLIPC_MSG msg, CLIPC_ARRAY array, CLIPC_STATUS *sp)); 
CLIPC_STATUS clipc_array_free A((CLIPC_ARRAY array));

CLIPC_STATUS clipc_array_rmval A((CLIPC_ARRAY array, int elem));
CLIPC_STATUS clipc_array_addval A((CLIPC_ARRAY array, CLIPC_VALUE val));
CLIPC_STATUS clipc_array_addstring A((CLIPC_ARRAY array, char *string));
CLIPC_STATUS clipc_array_addmem A((CLIPC_ARRAY array, int len, char *mem));
CLIPC_STATUS clipc_array_addchar A((CLIPC_ARRAY array, char c)); 
CLIPC_STATUS clipc_array_addint A((CLIPC_ARRAY array, int num));
CLIPC_STATUS clipc_array_addfloat A((CLIPC_ARRAY array, double num));
CLIPC_STATUS clipc_array_adddict A((CLIPC_ARRAY array, CLIPC_DICT val));
CLIPC_STATUS clipc_array_addarray A((CLIPC_ARRAY array, CLIPC_ARRAY val));
CLIPC_STATUS clipc_array_genadd A((CLIPC_ARRAY array, ...));
CLIPC_STATUS clipc_array_vgenadd A((CLIPC_ARRAY array, va_list pvar));

int clipc_array_size A((CLIPC_ARRAY array));
CLIPC_VALUE clipc_array_getval A((CLIPC_ARRAY array, int elem,
				 CLIPC_STATUS *sp));
char *clipc_array_getstring A((CLIPC_ARRAY array, int elem, CLIPC_STATUS *sp));
char *clipc_array_getmem A((CLIPC_ARRAY array, int elem, int *lenp,
			    CLIPC_STATUS *sp));
char clipc_array_getchar A((CLIPC_ARRAY array, int elem, CLIPC_STATUS *sp));
long clipc_array_getint A((CLIPC_ARRAY array, int elem, CLIPC_STATUS *sp));
double clipc_array_getfloat A((CLIPC_ARRAY array, int elem, CLIPC_STATUS *sp));
CLIPC_DICT clipc_array_getdict A((CLIPC_ARRAY array, int elem,
				 CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_array_getarray A((CLIPC_ARRAY array, int elem,
				 CLIPC_STATUS *sp));

/*
 * Values
 */
CLIPC_VALUE clipc_new_stringval A((CLIPC_MSG msg, char *str));
CLIPC_VALUE clipc_new_memval A((CLIPC_MSG msg, int len, char *mem));
CLIPC_VALUE clipc_new_charval A((CLIPC_MSG msg, char num));
CLIPC_VALUE clipc_new_intval A((CLIPC_MSG msg, int num));
CLIPC_VALUE clipc_new_floatval A((CLIPC_MSG msg, double num));
CLIPC_VALUE clipc_new_arrayval A((CLIPC_MSG msg, CLIPC_ARRAY array));
CLIPC_VALUE clipc_new_dictval A((CLIPC_MSG msg, CLIPC_DICT dict));
CLIPC_STATUS clipc_value_free A((CLIPC_VALUE value));
CLIPC_VALUE clipc_value_dup A((CLIPC_MSG msg, CLIPC_VALUE value));
CLIPC_VALUE clipc_new_value A((CLIPC_MSG msg, CLIPC_TYPE type));

CLIPC_TYPE clipc_val_gettype A((CLIPC_VALUE value, CLIPC_STATUS *sp));
char *clipc_val_getstring A((CLIPC_VALUE value, CLIPC_STATUS *sp));
char *clipc_val_getmem A((CLIPC_VALUE value, int *lenp, CLIPC_STATUS *sp));
int clipc_val_getmemlen A((CLIPC_VALUE value, CLIPC_STATUS *sp));
char *clipc_val_getmemval A((CLIPC_VALUE value, CLIPC_STATUS *sp));
char clipc_val_getchar A((CLIPC_VALUE value, CLIPC_STATUS *sp));
long clipc_val_getint A((CLIPC_VALUE value, CLIPC_STATUS *sp));
double clipc_val_getfloat A((CLIPC_VALUE value, CLIPC_STATUS *sp));
CLIPC_DICT clipc_val_getdict A((CLIPC_VALUE value, CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_val_getarray A((CLIPC_VALUE value, CLIPC_STATUS *sp));

/* don't document these */
CLIPC_STRING clipc_string_dup A((CLIPC_MSG msg, CLIPC_STRING string));
CLIPC_CHAR clipc_char_dup A((CLIPC_MSG msg, CLIPC_CHAR c));
CLIPC_INT clipc_int_dup A((CLIPC_MSG msg, CLIPC_INT num));
CLIPC_FLOAT clipc_float_dup A((CLIPC_MSG msg, CLIPC_FLOAT num)); 

/*
 * Message server
 */


CLIPC_CONN clipc_connection_init A((char *host, int port, CLIPC_STATUS *sp));
CLIPC_STATUS clipc_connection_close A((CLIPC_CONN conn));
CLIPC_STATUS clipc_set_connection_type A((CLIPC_CONN conn, CLIPC_TYPE type));
CLIPC_STATUS clipc_listen_all A((CLIPC_CONN conn));
CLIPC_STATUS clipc_register_handler A((CLIPC_CONN conn, char *name));
CLIPC_STATUS clipc_register_listener A((CLIPC_CONN conn, char *name,
					int flags));
CLIPC_STATUS clipc_register_synclistener A((CLIPC_CONN conn, char *name));
int clipc_check_for_handler A((CLIPC_CONN conn, char *name, int msg_id,
			       CLIPC_STATUS *sp));
CLIPC_STATUS clipc_unregister_handler A((CLIPC_CONN conn, char *name));
CLIPC_STATUS clipc_unregister_listener A((CLIPC_CONN conn, char *name));
CLIPC_STATUS clipc_unregister_synclistener A((CLIPC_CONN conn, char *name));

/*
 * CLIPC connection 
 */
CLIPC_CONN clipc_conn_alloc A((int fd, CLIPC_STATUS *sp));
void clipc_conn_free A((CLIPC_CONN conn, int free_fd));
void clipc_conn_setfd A((CLIPC_CONN conn, int new_fd));
int clipc_conn_getfd A((CLIPC_CONN conn));
char *clipc_conn_getobuf A((CLIPC_CONN conn)); 
CLIPC_CONN clipc_conn_findbyfd A((int fd, CLIPC_STATUS *sp));

/*
 * QUEUE support
 */
/*
 * QUEUE system
 */

CLIPC_STATUS clipc_msgqueue_enq A((CLIPC_MSGQUEUE *qp, CLIPC_MSG value,
				   int key)); 
CLIPC_STATUS clipc_msgqueue_pushq A((CLIPC_MSGQUEUE *qp, CLIPC_MSG value,
				     int key)); 
CLIPC_MSG clipc_msgqueue_deq A((CLIPC_MSGQUEUE *qp, CLIPC_MSG value,
				int key));
CLIPC_MSGQUEUE clipc_msgqueue_concat A((CLIPC_MSGQUEUE *qp,
				       CLIPC_MSGQUEUE tail));
CLIPC_MSGQUEUE clipc_msgqueue_copy A((CLIPC_MSGQUEUE qp));

void clipc_msgqueue_free A((CLIPC_MSGQUEUE *qp));
void clipc_msgqueue_rmmsg A((CLIPC_MSGQUEUE *qp, CLIPC_MSG value, int key));

CLIPC_MSGQUEUE clipc_msgqueue_next A((CLIPC_MSGQUEUE qp));
CLIPC_MSG clipc_msgqueue_getmsg A((CLIPC_MSGQUEUE qp));

/*
 * misc functions (should be private, but for now, they are useful)
 */
int unimp A((char *str, ...));
void clipc_error A((char *str, ...)); 
void clipc_msg_print A((CLIPC_MSG msg));
void clipc_val_print A((CLIPC_VALUE val, char *indent));


#if defined(__cplusplus)
}      /* matches extern "C" { at start of file */
#endif

#if defined(CLIPC_OLD_INTERFACE)
#if defined(__cplusplus)
extern "C" {
#endif

/* old function names, will go away in the next install of libclipc */

typedef enum clipc_msg_delclass CLIPC_MSGTYPE;
#define CLIPC_MSGTYPE_NONE     CLIPC_DELCLASS_NONE
#define CLIPC_MSGTYPE_ANY      CLIPC_DELCLASS_ANY
#define CLIPC_MSGTYPE_NOTIFY   CLIPC_DELCLASS_NOTIFY
#define CLIPC_MSGTYPE_REQUEST  CLIPC_DELCLASS_REQUEST
#define CLIPC_MSGTYPE_CMSCPREQ CLIPC_DELCLASS_CMSCPREQ
#define CLIPC_MSGTYPE_CMSCPRPL CLIPC_DELCLASS_CMSCPRPL
#define CLIPC_MSGTYPE_QUERY    CLIPC_DELCLASS_QUERY
#define CLIPC_MSGTYPE_PING     CLIPC_DELCLASS_PING

CLIPC_CONN init_clipc_connection A((CLIPC_STATUS *sp));
CLIPC_STATUS close_clipc_connection A((CLIPC_CONN));
CLIPC_STATUS register_clipc_handler A((CLIPC_CONN, char *));
CLIPC_STATUS unregister_clipc_handler A((CLIPC_CONN, char *));
CLIPC_STATUS register_clipc_listener A((CLIPC_CONN, char *, int));
CLIPC_STATUS unregister_clipc_listener A((CLIPC_CONN, char *));
CLIPC_STATUS register_clipc_synclistener A((CLIPC_CONN, char *));
CLIPC_STATUS unregister_clipc_synclistener A((CLIPC_CONN, char *));
void print_msg A((CLIPC_MSG));
void print_val A((CLIPC_VALUE, char *));

CLIPC_STATUS clipc_msg_settype A((CLIPC_MSG msg, CLIPC_MSGTYPE type));
CLIPC_DICT clipc_msg_getargs A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_MSGTYPE clipc_msg_gettype A((CLIPC_MSG msg, CLIPC_STATUS *sp));
CLIPC_ARRAY clipc_dict_getkeys A((CLIPC_DICT dict, CLIPC_STATUS *sp));


CLIPC_STATUS clipc_session_addint A((CLIPC_CONN conn, char *name, int val));
CLIPC_STATUS clipc_session_addstring A((CLIPC_CONN conn, char *name, char *val));

#if defined(__cplusplus)
}      /* matches extern "C" { at start of file */
#endif
#endif  /* !defined(CLIPC_NEW_INTERFACE) */

#if defined(A_DEFINED_IN_FILE)
#undef A
#undef A_DEFINED_IN_FILE
#endif /* defined(A_DEFINED_IN_FILE) */
#endif /* defined (__CLIPC_H) */






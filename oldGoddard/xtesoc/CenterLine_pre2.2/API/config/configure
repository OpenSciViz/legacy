#!/usr/local/bin/perl

$interactive = 0;

if (-d 'config') {
    chdir 'config';
}
elsif (-d '../config') {
    chdir ('../config');
}
else {
    die("can't find config directory");
}

while ($_ = $ARGV[0],/^-/) {
    shift;			
    last if /^--$/;
    
    if (/^-i/) {
	$interactive = 1;
    }
    elsif (/^-v/ || /-verbose/) {
	do print_config(0, "currently");
	exit(0);
    }
    elsif (/^-r/ || /-reconfig/ || /-reconfigure/) {
	$current_config = do print_config(0, "currently");
	if ($current_config) {
	    print "Reconfiguring...";
	    $rval = do config_target($current_config);
	    print "Done\n";
	    exit ($rval);
	}
	else {
	    print "can't reconfigure; not currently configured\n";
	    exit(1);
	}
    }
    else {
	/^-help/ || print "unknown option '$_'\n" ;
	do usage("usage: configure [options] target");	
	exit(1);
    }
}

while ($interactive) {
    do print_config(1);
    print "target? ";
    $line = <>;
    exit(1) if ($line eq "");
    chop($line);

    next if ($line eq "");    
    
    $rval = do config_target($line);
    if ($rval == 0) {
	exit(0);
    }
}

if ($#ARGV == -1 && $interactive == 0) {
    do usage("usage: configure [options] target");
    do print_config(1);
    exit(2);
}

do print_config(0, "currently");

if (do config_target($ARGV[0])) {
    do print_config(1);
    exit(1);
}

do print_config(0, "now");

sub config_target {
    local($target) = @_;
    local($rval);
    local($chip) = $target =~ /^([^-]*)-/;
    local($chipos) = $target =~ /^([^-]*-[^-]*)-/;
    local($product) = $target =~ /^[^-]*-[^-]*-([^-]*)-/;

    # configure the Make.include symlink
    if ( -e "Make.$target") {
	unlink('Make.include');
	$rval = system ("ln -s Make.$target Make.include");
	if ($rval) {
	    print "problems with 'ln -s Make.$target Make.include'\n";
	}
    }
    else {
	print "No Make include file ('Make.$target') for '$target'.\n";
	$rval = 1;
    }
   
    return $rval;
}


sub print_config {
    local($targets, $when) = @_;
    local($dest, $line, $arch, $file);
    local (@file_list);

    if ( -e 'Make.include') {
	$line = `ls -l Make.include`;
	($dest) = $line =~/lrwx.*include -> Make\.(.*)/;
	print "$when configured for '$dest'\n";
	return $dest;
    }

    if ($targets) {
	@file_list = <Make.*>;
	if ($#file_list >= 0) {
	    print "possible targets are:\n";
	    
	    foreach $file (@file_list) {
		($arch) = $file =~ /Make.(.*)/;
		next if ($arch =~ /~$/);
		next if ($arch =~ /.save[ed]*$/);
		next if ($arch eq "include"); 
		print "\t$arch\n";
	    }
	}
    }
    return 0;
}

sub usage {
    local($output) = @_;

    print "$output\n" if ($output);
    
    print "options are:\n";
    print "\t-i        interactive [$interactive]\n";
    print "\t-v        describe the current configuration\n";
    print "\t-r        reconfigure the current configuration\n";
    print "\n";
}

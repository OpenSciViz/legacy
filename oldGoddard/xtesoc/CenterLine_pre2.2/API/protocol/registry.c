#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include "clipc.h"
#include "../lib/clipc-private.h"

CLIPC_MSG cl_parse_dict_msg(CLFP ifp);

#define SABER_PORT 12345

main()
{
    int sock_fd;
    
    sock_fd = init_server_socket();

    while (1) {
	process_input(sock_fd);
    }
}


/*
 * open up a socket on the Well Known port
 */

init_server_socket()
{
    int fd;
    struct sockaddr_in my_addr;

    fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (fd == -1) {
	perror("socket");
	return -1;
    }

    memset((char *)&my_addr, 0, sizeof my_addr);

    my_addr.sin_family = AF_INET;    
    my_addr.sin_port = SABER_PORT;
    my_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(fd, (struct sockaddr *)&my_addr, sizeof(my_addr)) == -1) {
	perror("bind");
	close (fd);
	return -1;
    }
    return fd;
}

/*
 * block reading on a socket, handle the message and return a status
 */

#define INPUT_BUFSIZ 8096
process_input(int fd)
{
    CLIPC_MSG msg;
    CLFP ifp;
    struct sockaddr_in client_addr;
    int addrlen = sizeof(client_addr);
    char inbuf[INPUT_BUFSIZ];
    char *cp, *end;
    int cc;

    cc = recvfrom(fd, inbuf, INPUT_BUFSIZ, 0,
		  (struct sockaddr *)&client_addr, &addrlen);
    if (cc == -1)
      return -1;

    end = inbuf+cc;
    for (cp = inbuf; cp < end && (isdigit(*cp) || isspace(*cp)); cp++, cc--)
      continue;

    ifp = clfp_init(cp, cc);
    
    msg = cl_parse_dict_msg(ifp);
    print_msg(msg);
    clipc_msg_free(msg);
    return 0;
}
     
    
      

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <memory.h>
#include "clipc.h"
#include "../lib/clipc-private.h"
#include "typedef.h"

CLIPC_TYPEDEF clipc_typedef_alloc(char *name, CLIPC_TYPEDEF_TYPE type);
CLIPC_TYPEDEF clipc_typedef_findbyname(char *name, int add);
CLIPC_TYPEDEF parse_typeref(FILE *ifp, char *name);

CLIPC_MSGDEF clipc_msgdef_findbyname(char *name);

struct clipc_dict_fldlist *parse_dict_fldlist(FILE *ifp);
struct clipc_enum_list *parse_enum_list(FILE *ifp);

static CLIPC_TYPEDEF typedef_list = 0;
static CLIPC_MSGDEF msgdef_list = 0;

CLIPC_MSG build_test_msg()
{
    CLIPC_MSG msg;
    msg = clipc_notify_alloc("cxcmd_printopt");
    clipc_msg_genadd
      (msg,
       "optName",  CLIPC_TYPE_STRING, "foo",
       "optType",  CLIPC_TYPE_STRING, "bool",
       "optValue", CLIPC_TYPE_STRING, "foo",
       "optDesc", CLIPC_TYPE_STRING, "TRUE",
       0);
    return msg;
}    

char *getword();
main(int argc, char *argv[])
{
    int i;
    CLIPC_MSG msg;

    init_basic_types();

    for (i = 1; i < argc; i++) 
      clipc_typedef_readfile(argv[i]);
    
    print_typedef_list();
    print_msgdef_list();
    msg = build_test_msg();
    msg = msg;
}
/*
 * Return TRUE if msg is valid, FALSE if it doesn't match the known
 * template.
 */
clipc_typedef_msgcheck(CLIPC_MSG msg)
{
    CLIPC_MSGDEF msg_def;
    CLIPC_DICT args;
    CLIPC_MSGTYPE msg_type;
    
    char *msg_name;
    if (msg == 0)
      return TRUE;
    
    msg_name = clipc_msg_getname(msg, 0);
    msg_type = clipc_msg_gettype(msg, 0);
    
    msg_def = clipc_msgdef_findbyname(msg_name);
    if (msg_def == 0)
      return TRUE;

    args = clipc_msg_getargs(msg, 0);
    if (args == 0)
      return FALSE;

    if (msg_type == CLIPC_MSGTYPE_REQUEST) {
	return clipc_typedef_match((CLIPC_VALUE)args, msg_def->request_def);
    }
    else {
	return clipc_typedef_match((CLIPC_VALUE)args, msg_def->notify_def);
    }
}

clipc_typedef_match(CLIPC_VALUE value, CLIPC_TYPEDEF def)
{
    CLIPC_TYPE valtype;
    CLIPC_ARRAY array;
    CLIPC_DICT dict;
    CLIPC_VALUE dvalue;
    struct clipc_dict_fldlist *tdfld;
    int i;

    if (def == 0 || def->typedef_type == CLIPC_TYPEDEF_NIL)
      return TRUE;
    
    valtype = clipc_val_gettype(value, 0);
    switch (valtype) {
      case CLIPC_TYPE_DICT:
	dict = clipc_val_getdict(value, 0);
	/*
	 * simple check for fields.  Don't yet worry about mutex 
	 */
	if (def->typedef_type == CLIPC_TYPEDEF_DICT) {
	    for (tdfld = def->un.dict_fields; tdfld; tdfld = tdfld->next) {
		dvalue = clipc_dict_getval(dict, tdfld->keyname, 0);
		if (dvalue == 0) {
		    if (tdfld->is_opt == 0)
		      return FALSE;
		    else
		      continue;
		}
		
		if (!clipc_typedef_match(dvalue, tdfld->def)) {
		    return FALSE;
		}
	    }
	    return TRUE;
	}
	
	if (def->typedef_type == CLIPC_TYPEDEF_DISC) {
	    unimp("checking discriminated unions");
	    return TRUE;
	}
	return FALSE;

      case CLIPC_TYPE_ARRAY:
	if (def->typedef_type != CLIPC_TYPEDEF_ARRAY)
	  return FALSE;

	array = clipc_val_getarray(value, 0);
	for (i = 0; i < clipc_array_size(array); i++) {
	    if (!clipc_typedef_match(clipc_array_getval(array, i, 0),
				     def->un.array_elmt))
	    {
		return FALSE;
	    }
	}
	return TRUE;

      case CLIPC_TYPE_STRING:
      case CLIPC_TYPE_INT:	
	if (def->typedef_type == CLIPC_TYPEDEF_ENUM) {
	    struct clipc_enum_list *efld;
	    int intval;
	    char *strval;
	    CLIPC_STATUS status;
	    
	    if (valtype == CLIPC_TYPE_INT) {
		intval = clipc_val_getint(value, &status);
		if (status != CLIPC_STATUS_OK)
		  return FALSE;
		
		for (efld = def->un.enum_list; efld; efld = efld->next) {
		    if (efld->value == intval)
		      return TRUE;
		}
		return FALSE;
	    }
	    else if (valtype == CLIPC_TYPE_STRING) {
		strval = clipc_val_getstring(value, &status);
		if (status != CLIPC_STATUS_OK || strval == 0)
		  return FALSE;
		
		for (efld = def->un.enum_list; efld; efld = efld->next) {
		    if (!strcmp(efld->name, strval))
		      return TRUE;
		}
		return FALSE;		
	    }
	    else {
		clipc_error("neither INT nor STRING");
		return FALSE;
	    }
	}
	/* FALLTHRU */
	
      default:
	return (def->typedef_type == CLIPC_TYPEDEF_BASIC
		&& valtype == def->un.basic_type);
    }
}

print_clipc_typedef(FILE *ofp, CLIPC_TYPEDEF def)
{
    char newindent[1024];    
    fprintf(ofp, "%s: ",  def->typedef_name);
    sprintf(newindent, "%*c  ", strlen(def->typedef_name), ' ');
    print_typeref(ofp, def, newindent, 0);
    fprintf(ofp, "\n");
}

print_typeref(FILE *ofp, CLIPC_TYPEDEF def, char *indent, int indent_first)
{
    char newindent[1024];
    CLIPC_TYPEDEF array_elmt;
    struct clipc_enum_list *efld;

    switch (def->typedef_type) {
      case CLIPC_TYPEDEF_DICT:
	fprintf(ofp, "%s(T_DCT\n", indent_first ? indent : "");
	print_dict_fields(ofp, def->un.dict_fields, indent);
	fprintf(ofp, ")");
	break;

      case CLIPC_TYPEDEF_DISC:
	fprintf(ofp, "(T_DSC %s\n", def->un.disc.disc_name);
	print_dict_fields(ofp, def->un.disc.disc_fields, indent);
	fprintf(ofp, ")");
	break;
	
      case CLIPC_TYPEDEF_ARRAY:
	fprintf(ofp, "(T_ARY ");
	array_elmt = def->un.array_elmt;
	if (array_elmt) {
	    sprintf(newindent, "%s      ", indent);
	    if (array_elmt->typedef_name && *array_elmt->typedef_name) {
		fprintf(ofp, "%s", array_elmt->typedef_name);
	    }
	    else {
		fprintf(ofp, "\n");
		print_typeref(ofp, array_elmt, newindent, 1);
	    }
	}
	else {
	    fprintf(ofp, "NIL");
	}
	fprintf(ofp, ")");
	break;

      case CLIPC_TYPEDEF_ENUM:
	fprintf(ofp, "%s(T_ENU\n", indent_first ? indent: "");
	for (efld = def->un.enum_list; efld; efld = efld->next) {
	    fprintf(ofp, "%s     %s", indent, efld->name);
	    if (efld->is_explicit)
	      fprintf(ofp, " = %d", efld->value);
	    if (efld->next)
	      fprintf(ofp, ",\n");
	}
	fprintf(ofp, "%s)", indent);
	break;
      default:
	fprintf(ofp, "%s", def->typedef_name);
	break;
    }
}

print_dict_fields(FILE *ofp, struct clipc_dict_fldlist *typfld, char *indent)
{
    char newindent[1024];
    
    for ( ; typfld; typfld = typfld->next) {
	fprintf(ofp, "%s    %c%s: ", indent,
		typfld->is_opt ? '&' : ' ', typfld->keyname);
	
	if (typfld->def) {
	    sprintf(newindent, "%s     %*c  ", indent,
		    strlen(typfld->keyname), ' ');
	    
	    if (typfld->def->typedef_name && *typfld->def->typedef_name)
	      fprintf(ofp, "%s", typfld->def->typedef_name);
	    else
	      print_typeref(ofp, typfld->def, newindent, 0);
	}
	else {
	    fprintf(ofp, "NIL");
	}
	if (typfld->next)
	  fprintf(ofp, ",\n");
    }
}      


CLIPC_TYPEDEF clipc_typedef_alloc(char *name, CLIPC_TYPEDEF_TYPE type)
{
    CLIPC_TYPEDEF rval;
    rval = (CLIPC_TYPEDEF)malloc(sizeof *rval);
    rval->typedef_name = clipc_strdup(0, name);
    rval->typedef_type = type;
    memset((char *)&rval->un, 0, sizeof(rval->un));
    rval->refcount = 1;
    rval->next = 0; 
    return rval;
}

clipc_typedef_free(CLIPC_TYPEDEF def)
{
    struct clipc_dict_fldlist *typfld;
    struct clipc_enum_list *efld;        

    if (def == 0)
      return;
    
    def->refcount--;
    if (def->refcount <= 0) {
	free(def->typedef_name);
	def->next = 0;
	switch (def->typedef_type) {
	  case CLIPC_TYPEDEF_ARRAY:
	    clipc_typedef_free(def->un.array_elmt);
	    break;
	    
	  case CLIPC_TYPEDEF_DICT:
	    for (typfld = def->un.dict_fields; typfld; ) {
		struct clipc_dict_fldlist *tmp;
		tmp = typfld->next;
		free(typfld->keyname);
		clipc_typedef_free(typfld->def);
		free((char *)typfld);
		typfld = tmp;
	    }
	    def->un.dict_fields = 0;
	    break;
	  case CLIPC_TYPEDEF_DISC:
	    for (typfld = def->un.disc.disc_fields; typfld; ) {
		struct clipc_dict_fldlist *tmp;
		tmp = typfld->next;
		free(typfld->keyname);
		clipc_typedef_free(typfld->def);
		free((char *)typfld);
		typfld = tmp;
	    }
	    def->un.disc.disc_fields = 0;
	    free(def->un.disc.disc_name);
	    break;

	  case CLIPC_TYPEDEF_ENUM:
	    for (efld = def->un.enum_list; efld; ) {
		struct clipc_enum_list *tmp;
		tmp = efld->next;
		free(efld->name);
		free((char *)efld);
		efld = tmp;
	    }
	    def->un.enum_list = 0;
	    break;
	    
	  default:
	    break;
	}
	free((char *)def);
    }
}

clipc_typedef_adddictfld(struct clipc_dict_fldlist **tpp, char *name,
			 CLIPC_TYPEDEF elmt, int is_opt)
{
    struct clipc_dict_fldlist *typfld;

    typfld = (struct clipc_dict_fldlist *)malloc(sizeof *typfld);
    typfld->is_opt = is_opt;
    typfld->keyname = clipc_strdup(0, name);
    typfld->next = 0;
    if (elmt)
      elmt->refcount++;
    typfld->def = elmt;
    for ( ; *tpp; tpp = &(*tpp)->next)
      continue;

    *tpp = typfld;
}

clipc_typedef_addarrayelmt(CLIPC_TYPEDEF def, CLIPC_TYPEDEF elmt)
{
    if (elmt)
      elmt->refcount++;
    if (def->un.array_elmt)
      clipc_error("replacing array element for typedef '%s'",
		  def->typedef_name);
    def->un.array_elmt = elmt;
}
 

clipc_typedef_add(CLIPC_TYPEDEF def)
{
    CLIPC_TYPEDEF tmp, *tdp;

    for (tmp = typedef_list; tmp; tmp = tmp->next) {
	if (strcmp(tmp->typedef_name, def->typedef_name) == 0)
	  return;
    }

    def->refcount++;
    for (tdp = &typedef_list; *tdp; tdp = &(*tdp)->next)
      continue;
    *tdp = def;
}

clipc_msgdef_add(CLIPC_MSGDEF msg_def)
{
    CLIPC_MSGDEF tmp, *tdp;
    
    for (tmp = msgdef_list; tmp; tmp = tmp->next) {
	if (strcmp(tmp->msg_name, msg_def->msg_name) == 0)
	  return;
    }

    msg_def->refcount++;
    for (tdp = &msgdef_list; *tdp; tdp = &(*tdp)->next)
      continue;
    *tdp = msg_def;
}

CLIPC_TYPEDEF clipc_typedef_findbyname(char *name, int add)
{
    CLIPC_TYPEDEF rval;

    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    if (name == 0 || *name == 0) {
	if (add)
	  return clipc_typedef_alloc(name, CLIPC_TYPEDEF_NIL);
	else
	  return 0;
    }
    
    for (rval = typedef_list; rval; rval = rval->next) {
	if (strcmp(rval->typedef_name, name) == 0)
	  return rval;
    }
    
    if (add) {
	rval = clipc_typedef_alloc(name, CLIPC_TYPEDEF_NIL);
	clipc_typedef_add(rval);
	return rval;
    }
    return 0;
}

CLIPC_MSGDEF clipc_msgdef_findbyname(char *name)
{
    CLIPC_MSGDEF rval;

    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    if (name == 0 || *name == 0) {
	return 0;
    }
    
    for (rval = msgdef_list; rval; rval = rval->next) {
	if (strcmp(rval->msg_name, name) == 0)
	  return rval;
    }
    
    return 0;
}

print_typedef_list()
{
    CLIPC_TYPEDEF tdp;

    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    for (tdp = typedef_list; tdp; tdp = tdp->next) {
	if (tdp->typedef_type == CLIPC_TYPEDEF_BASIC)
	  continue;
	
	print_clipc_typedef(stdout, tdp);
    }
}

print_msgdef_list()
{
    CLIPC_MSGDEF mdp;

    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    for (mdp = msgdef_list; mdp; mdp = mdp->next) {
	print_clipc_msgdef(stdout, mdp);
    }
    printf("\n");
}

print_clipc_msgdef(FILE *ofp, CLIPC_MSGDEF mdp)
{
    printf("%s\n", mdp->msg_name);

    if (mdp->request_def) {
	fprintf(ofp, " -request: ");
	print_typeref(ofp, mdp->request_def, "          ", 0);
	fprintf(ofp, "\n");
    }
    
    if (mdp->notify_def) {
	fprintf(ofp, " -notify: ");
	print_typeref(ofp, mdp->notify_def, "          ", 0);
	fprintf(ofp, "\n");	
    }
}


/*
 * parsing code for typedefs:
 *
 *  refSym: (T_DCT symbolName : STRING, symbolType : ExprTypeName,
 *                 file : STRING, linenumber: INT)
 */

CLIPC_TYPEDEF parse_typedef(FILE *ifp, char *typedef_name)
{
    CLIPC_TYPEDEF old_def;
    CLIPC_TYPEDEF def;    
    
    /*
     * parse typeRef, given def
     *   See if it is a dict or an array typeref
     *   If it is T_DCT, parse the field list and eat the ')'
     *   If it is array, get the elmt type and eat the ')'
     */

    old_def = clipc_typedef_findbyname(typedef_name, 0);
    if (old_def && old_def->typedef_type != CLIPC_TYPEDEF_NIL) {
	clipc_error("redefining type '%s'", typedef_name);
	free(typedef_name);	
	return 0;
    }

    def = parse_typeref(ifp, typedef_name);
    free(typedef_name);
    return def;
}

CLIPC_TYPEDEF parse_typeref(FILE *ifp, char *name)
{
    char *type_start = 0;
    CLIPC_TYPEDEF def;

    type_start = getword(ifp, "), \t\n");
    if (type_start == 0) {
	clipc_error("parse_tdef: couldn't get start of def for '%s'",
		    name);		    
	return 0;
    }
    
    if (*type_start != '(') {
	def = clipc_typedef_findbyname(type_start, 1);
	free(type_start);
	return def;
    }
    
    if (strcmp(type_start, "(T_DCT") == 0) {
	char *tmp = 0;
	
	def = clipc_typedef_findbyname(name, 1);
	if (def->typedef_type == CLIPC_TYPEDEF_NIL) {
	    def->typedef_type = CLIPC_TYPEDEF_DICT;
	} else {
	    clipc_error("trying to redefine typedef '%s'", name);
	}
	
	def->un.dict_fields = parse_dict_fldlist(ifp);
	tmp = getword(ifp, ")");
	getc(ifp);
	if (tmp)
	  free(tmp);
	if (type_start)
	  free(type_start);	
	return def;
    }

    if (strcmp(type_start, "(T_DSC") == 0) {
	char *tmp;
	char *disc_name = 0;
	disc_name = getword(ifp, " \t\n");
	
	def = clipc_typedef_findbyname(name, 1);
	if (def->typedef_type == CLIPC_TYPEDEF_NIL) {
	    def->typedef_type = CLIPC_TYPEDEF_DISC;
	} else {
	    clipc_error("trying to redefine typedef '%s'", name);
	}
	
	def->un.disc.disc_fields = parse_dict_fldlist(ifp);
	def->un.disc.disc_name = clipc_strdup(0, disc_name);
	if (disc_name)
	  free(disc_name);
	
	tmp = getword(ifp, ")");
	getc(ifp);
	if (tmp)
	  free(tmp);
	if (type_start)
	  free(type_start);	
	return def;
    }
    
    if (strcmp(type_start, "(T_ARY") == 0) {
	CLIPC_TYPEDEF elmt_def;
	char *tmp;
	
	def = clipc_typedef_findbyname(name, 1);
	if (def->typedef_type == CLIPC_TYPEDEF_NIL) {
	    def->typedef_type = CLIPC_TYPEDEF_ARRAY;
	}
	else {
	    clipc_error("trying to redefine typedef '%s'", name);
	}
	elmt_def = parse_typeref(ifp, "");
	clipc_typedef_addarrayelmt(def, elmt_def);
	tmp = getword(ifp, ")");
	getc(ifp);
	if (tmp)
	  free(tmp);
	if (type_start)
	  free(type_start);		
	return def;
    }
    if (strcmp(type_start, "(T_ENU") == 0) {
	char *tmp;
	def = clipc_typedef_findbyname(name, 1);
	if (def->typedef_type == CLIPC_TYPEDEF_NIL) {
	    def->typedef_type = CLIPC_TYPEDEF_ENUM;
	}
	else {
	    clipc_error("trying to redefine typedef '%s'", name);
	}
	def->un.enum_list = parse_enum_list(ifp);
	tmp = getword(ifp, ")");
	getc(ifp);
	if (tmp)
	  free(tmp);
	if (type_start)
	  free(type_start);		
	return def;
    }
    
    free(type_start); 
    return 0;
}

struct clipc_dict_fldlist *
parse_dict_fldlist(FILE *ifp)
{
    int c;
    char *fld_name;
    char *type_name;
    CLIPC_TYPEDEF type_elmt;
    struct clipc_dict_fldlist *rval = 0;
    
    /*
     * loop:
     * find name : typename
     *  - add name, typename to the dictionary fields
     * look for , or ')'
     *  - if ')' unget char and return.
     *  if ',' loop
     */
    
    while (1) {
	int is_opt = 0;
	fld_name = getword(ifp, "), \t\n:");
	if (fld_name == 0 || *fld_name == 0) {
	    c = getc(ifp);
	    if (c != ')')
	      clipc_error("parse_tdef_fldlist: EOF looking for fld_name.");
	    else
	      ungetc(c, ifp);
	    return 0;
	}
	if (*fld_name == '&') {
	    is_opt = 1;
	    if (*(fld_name+1) == 0 || isspace(*(fld_name+1))) {
		free(fld_name);
		fld_name = getword(ifp, "), \t\n:");
	    }
	    else {
		char *tmpname;
		tmpname = clipc_strdup(0, fld_name+1);
		free(fld_name);
		fld_name = tmpname;
	    }
	}
	else {
	    is_opt = 0;
	}
	    
	while ((c=getc(ifp))!=EOF && c != ':')
	  continue;
	
	if (c == EOF) {
	    clipc_error("parse_tdef_fldlist: EOF looking for ':' after '%s'",
			fld_name);
	    return 0;
	}

	c = skipspace(ifp);
	if (c == '(') {
	    type_elmt = parse_typeref(ifp, "");
	    clipc_typedef_adddictfld(&rval, fld_name, type_elmt, is_opt);
	}
	else {
	    type_name = getword(ifp, "), \t\n");
	    if (type_name == 0) {
		clipc_error("parse_tdef_fldlist: EOF looking for type_name.");
		return 0;
	    }
	    
	    type_elmt = clipc_typedef_findbyname(type_name, 1);
	    clipc_typedef_adddictfld(&rval, fld_name, type_elmt, is_opt);
	    free(type_name);
	}

	free(fld_name);
	c = skipspace(ifp);
	if (c == EOF) {
	    clipc_error("parse_tdef_fldlist: EOF looking for EO list.");
	    return 0;
	}
	c = getc(ifp);
	
	if (c == ')') {
	    ungetc(c, ifp);
	    break;
	}
	if (c != ',') {
	    clipc_error("parse_tdef_fldlist: EOF looking for comma.");
	    return 0;
	}
    }
    return rval;
}

struct clipc_enum_list *parse_enum_list(FILE *ifp)
{
    char *enum_name;
    int is_explicit;
    int cur_value = 0;
    int enum_value;
    int c;
    struct clipc_enum_list *tmp, *rval = 0;
    struct clipc_enum_list *last = 0;

    while (1) {
	is_explicit = 0;
	enum_name = getword(ifp, " \t\n,=)");
	if (enum_name == 0 || *enum_name == 0) {
	    clipc_error("parse_enum_list: empty name");
	    break;
	}
	enum_value = 0;
	c = skipspace(ifp);
	if (c == '=') {
	    getc(ifp);
	    c = skipspace(ifp);
	    if (isdigit(c)) {
		enum_value = 0;
		c = getc(ifp);
		while (isdigit(c)) {
		    enum_value = 10*enum_value + (c - '0');
		    c = getc(ifp);
		}
		if (c == EOF)
		  break;
		ungetc(c, ifp);
		cur_value = enum_value;
		is_explicit = 1;
	    }
	    c = skipspace(ifp);	    
	}
	tmp = (struct clipc_enum_list *)malloc(sizeof *tmp);
	tmp->next = 0;
	tmp->name = enum_name;
	tmp->value = cur_value++;
	tmp->is_explicit = is_explicit;
	if (rval == 0) {
	    rval = tmp;
	}
	else {
	    if (last)
	      last->next = tmp;
	}
	
	last = tmp;

	if (c == ')') 
	  break;
	
	if (c != ',') {
	    clipc_error("parse_enum_list: expected ',' after '%s', got '%c'",
			enum_name, c);
	}
	getc(ifp);
    }
    return rval;
}

clipc_typedef_readfile(char *name)
{
    FILE *ifp;
    CLIPC_TYPEDEF def;
    CLIPC_MSGDEF msg_def;    
    int def_count = 0;
    char *msg_name;
    int c;
    
    ifp = fopen(name, "r");
    if (ifp == 0)
      return -1;

    while (!feof(ifp)) {
	c = skipspace(ifp);
	msg_name = getword(ifp, " \t\n:(");
	if (msg_name == 0)
	  break;
	
	c = skipspace(ifp);
	if (c == EOF)
	  break;
	
	if (c == ':') {
	    c = getc(ifp);
	    def = parse_typedef(ifp, msg_name);
	    if (def == 0)
	      break;
	    clipc_typedef_add(def);
	    def_count++;
	}
	else {
	    char *type;
	    if (c != '-') {
		clipc_error("expected '-' after message name '%s', got '%c'",
			    msg_name, c);
	    }
	    msg_def = clipc_msgdef_findbyname(msg_name);
	    if (msg_def) {
		clipc_error("redefining message '%s'", msg_name);
	    }
	      
	    /*
	     * get the request and notify args if they are there
	     */
	    msg_def = (CLIPC_MSGDEF)malloc(sizeof *msg_def);
	    msg_def->msg_name = clipc_strdup(0, msg_name);
	    msg_def->next = 0;
	    msg_def->refcount = 1; 
	    msg_def->request_def = 0;
	    msg_def->notify_def = 0;
	    
	    while (1) {
		c = skipspace(ifp);
		if (c != '-')
		  break;

		c = getc(ifp);
		type = getword(ifp, " \t\n:");
		c = skipspace(ifp);
		if (c != ':') {
		    clipc_error("got '%c', not ':' after '%s'", c, type);
		    break;
		}
		else {
		    c = getc(ifp);
		}
		
		if (strcmp(type, "request") == 0) {
		    msg_def->request_def = parse_typeref(ifp, "");
		}
		else if (strcmp(type, "notify") == 0) {
		    msg_def->notify_def = parse_typeref(ifp, ""); 
		}
		else {
		    clipc_error("didn't get request or notify, but '%s'",
				type);
		}
	    }
	    clipc_msgdef_add(msg_def);
	}
    }
    fclose(ifp);
    return def_count;
}

skipspace(FILE *ifp)
{
    int c;
    while ((c = getc(ifp)) != EOF) {
	if (c == ';' || c == '/') {
	    while ((c = getc(ifp)) != EOF && c != '\n')
	      continue;
	    if (c == EOF)
	      break;
	}
	if (!isspace(c))
	  break;
    }
    
    if (c != EOF)
      ungetc(c, ifp);
    return c;
}

char *getword(FILE *ifp, char *endstr)
{
    char rbuf[1024];
    int i;
    int c;
    
    i = 0;
    c = skipspace(ifp);
    if (c == EOF)
      return 0;
    
    while ((c = getc(ifp)) != EOF && !strchr(endstr, c)) {
	if (c == ';' || c == '/') {
	    while ((c = getc(ifp)) != EOF && c != '\n')
	      continue;
	    break;
	}
	rbuf[i++] = c;
	continue;
    }
    rbuf[i] = 0;
    if (c != EOF)
      ungetc(c, ifp);
    return clipc_strdup(0, rbuf);
}

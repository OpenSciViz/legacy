#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/socket.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include "clipc.h"
#include "../lib/clipc-private.h"

#define SABER_PORT 12345

CLIPC_STATUS udp_send_msg(int fd, CLIPC_MSG msg);

main()
{
    int rval;
    rval = client_register(0xbfbf);
    printf("client_register returns %d\n", rval);
}

client_register(int port)
{
    CLIPC_MSG msg;
    int fd;
    struct sockaddr_in my_addr;
    char *outbuf;
    int bcast = 0;
    int rval;
    int len = sizeof(bcast);
    char hostname[256];
    int uid;
    struct passwd *pw;
    
    fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if (fd == -1) {
	perror("socket");
	return -1;
    }

    bcast = 1;
    rval = setsockopt(fd, SOL_SOCKET, SO_DEBUG, (char *)&bcast, len);
    if (rval == -1) {
	perror("setsockopt");
	return -1;
    }
    
    memset((char *)&my_addr, 0, sizeof my_addr);

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = 0;
    my_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(fd, (struct sockaddr *)&my_addr, sizeof(my_addr)) == -1) {
	perror("bind");
	close (fd);
	return -1;
    }
    
    msg = clipc_msg_alloc("test_register");
    gethostname(hostname, 255);
    uid = getuid();
    pw = getpwuid(uid);
    
    clipc_msg_addstring(msg, "hostName", hostname);
    clipc_msg_addstring(msg, "userName", pw->pw_name);
    clipc_msg_addint(msg, "userId", uid);
    clipc_msg_addint(msg, "listenPort", port);
    
    udp_send_msg(fd, msg);
    close(fd);
    clipc_msg_free(msg);
}

CLIPC_STATUS udp_send_msg(int fd, CLIPC_MSG msg)
{
    char *obuf;
    int msg_size;
    struct sockaddr_in dest_addr;
    int rval;
    int cc;
    static long broadcast_addr = 0;
    if (broadcast_addr == 0)
      broadcast_addr = get_broadcast_addr();
    
    dest_addr.sin_family = AF_INET;    
    dest_addr.sin_port = SABER_PORT;
    dest_addr.sin_addr.s_addr = broadcast_addr;

    msg_size = cli_find_msg_size(msg);
    obuf = malloc(msg_size + 11);
    if (obuf == 0)
      return CLIPC_STATUS_ERR;

    rval = cl_sprint_dict_msg(obuf+10, msg);
    
    if (rval != CLIPC_STATUS_OK) {
	free(obuf);
	return rval;
    }
    
    msg_size = strlen(obuf+10);
    sprintf(obuf, "%09d", msg_size);
    obuf[9] = ' ';

    cc = sendto(fd, obuf, msg_size+10, 0,
		(struct sockaddr *)&dest_addr, sizeof(dest_addr));
    if (cc == -1) {
	perror("sendto");
    }
    return cc;
}

long get_broadcast_addr()
{
    unimp("real get_broadcast_addr()");
    return 0x8cef01ff;
}

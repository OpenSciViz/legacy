%{
#include <stdio.h>
#include <stdlib.h>    
#include "cltype.h"

char *clipc_strdup();    
    
#define YYDEBUG 1
#if defined(NeXT)
#define strdup(s) clipc_strdup(0, (s))
#endif
extern int cltype_parse_verbose;

static char *input_filename;
static int input_linenum;

%}

%union {
    int intval;
    char *strval;
    CLTYPE_MSGDEF    msgdef;
    CLTYPE_TYPDEF    typdef;
    CLTYPE_MTYPENT   mtypent;
    CLTYPE_DICTENT   dictent;
    CLTYPE_ENUMENT   enument;
}

%token MSGNAME NAME INT T_DCT T_ARY T_ENU T_DSC DESC
%%
def_list : msg_desc    { cltype_init_basic_types(); }
         | mdef def_list  { cltype_msgdef_add($<msgdef>1); }
         | tdef def_list  { cltype_typdef_add($<typdef>1); }
         | error def_list 
         ;
  
mdef   : msg_desc MSGNAME msgtype_list
        {
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", $<strval>2);
	    $<msgdef>$ = cltype_msgdef_alloc($<strval>2, $<mtypent>3,
					     $<strval>1);
	}
       | msg_desc NAME msgtype_list    
        {
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", $<strval>2);
	    $<msgdef>$ = cltype_msgdef_alloc($<strval>2, $<mtypent>3,
					     $<strval>1); 
	}
       ;

msg_desc : /* empty */ {$<strval>$ = 0;}
         | msg_desc DESC
            {
		/* not the best string handling routine around, but the
		 * inputs will be relatively short.
		 */
		if ($<strval>1 == 0) {
		    $<strval>$ = $<strval>2;
		}
		else {
		    char *tmp;
		    tmp = malloc(strlen($<strval>1) + strlen($<strval>2) + 4);
		    strcpy(tmp, $<strval>1);
		    strcat(tmp, $<strval>2);
		    free($<strval>1);
		    free($<strval>2);
		    $<strval>$ = tmp;
		}
	    }
         ;

tdef   : msg_desc NAME ':' typedef
        {
	    if (cltype_parse_verbose)
	      printf("read typedef '%s'\n", $<strval>2);
	    $<typdef>$ = cltype_typdef_setname($<typdef>4, $<strval>2,
					       $<strval>1);
	}	    
       | msg_desc NAME ':' error
           {
	       printf("error in tdef '%s'\n", $<strval>2); 
	       $<typdef>$ = 0;
	   }    
       ;

msgtype_list : msgtype
              {
		  $<mtypent>$ = cltype_mtyplist_add($<mtypent>1, 0);
	      }
             | msgtype msgtype_list
              {
		  $<mtypent>$ = cltype_mtyplist_add($<mtypent>1,
						       $<mtypent>2);
	      }
             ;

msgtype :  '-' NAME ':' field_list
         {
	     fprintf(stderr, "%s:%d '':' after '%s' is obsolete'\n",
		     input_filename, input_linenum, $<strval>2);
		     
	     $<mtypent>$ = cltype_mtypent_alloc($<strval>2, $<dictent>4);
	 }
        |  '-' NAME field_list
         {
	     $<mtypent>$ = cltype_mtypent_alloc($<strval>2, $<dictent>3);
	 }
        |  '-' NAME ':' NAME
         {
	     $<mtypent>$ = cltype_mtypent_defer_alloc($<strval>2, $<strval>4,
						      input_filename,
						      input_linenum);
	 }
        |  '-' NAME NAME
         {
	     $<mtypent>$ = cltype_mtypent_defer_alloc($<strval>2, $<strval>3,
						      input_filename,
						      input_linenum);
	 }
        |  '-' NAME error
          {
	      printf("error in parsing msgtype '%s'\n", $<strval>1);
	      $<mtypent>$ = 0;	      
	  }
        |  '-' NAME ':' error
          {
	      printf("error in parsing msgtype '%s'\n", $<strval>1);
	      $<mtypent>$ = 0;
	  }
        ;

field_list : '(' dict_list ')'
                { $<dictent>$ = $<dictent>2 }
           | '(' T_DCT dict_list ')'
                {
		    $<dictent>$ = $<dictent>3;
		    yyerror("message field list shouldn't have T_DCT");
		}
           ;
     
typeref : NAME
         {
	     $<typdef>$ = cltype_typdef_findbyname($<strval>1, TRUE);
	 }
        | typedef
        ;

typedef : '(' T_DCT dict_list ')'
         {
	     $<typdef>$ = cltype_make_dicttype($<dictent>3);
	 }
        | '(' T_ARY typeref ')'
         {
	     $<typdef>$ = cltype_make_arraytype($<typdef>3);
	 }
        | '(' T_DSC NAME dict_list ')'
         {
	     $<typdef>$ = cltype_make_disctype($<strval>3, $<dictent>4);
	 }
        | '(' T_ENU enum_list ')'
         {
	     $<typdef>$ = cltype_make_enumtype($<enument>3);
	     cltype_reset_enumcount();
	 }
        ;

dict_list : msg_desc
           {
	       $<dictent>$ = 0;
	   }
          | msg_desc dict_entry
           {
	       $<dictent>$ = cltype_make_dictlist($<dictent>2, 0, $<strval>1);
	   }
          | msg_desc dict_entry ',' dict_list
           {
	       $<dictent>$ = cltype_make_dictlist($<dictent>2, $<dictent>4,
						  $<strval>1);
	   }
          ;

dict_entry : NAME ':' typeref
            {
		$<dictent>$ = cltype_dictent_alloc($<strval>1, $<typdef>3, 0);
	    }
           | NAME ':' error
             {
		 printf("error in parsing dict_entry '%s'\n", $<strval>1);
		 $<dictent>$ = 0;
	     }
           | '&' NAME ':' typeref
            {
		$<dictent>$ = cltype_dictent_alloc($<strval>2, $<typdef>4, 1);
	    }
  
           | '&' NAME ':' error
             {
		 printf("error in parsing dict_entry '%s'\n", $<strval>2);
		 $<dictent>$ = 0;		 
	     }  
           ;

enum_list : /* empty */     
          | enum_entry
           {
	       $<enument>$ = cltype_enument_add($<enument>1, 0);
	   }
          | enum_entry ',' enum_list
           {
	       $<enument>$ = cltype_enument_add($<enument>1, $<enument>3);
	   }
          ;

					     
enum_entry : MSGNAME
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, 0, 0);
	    }
           | NAME
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, 0, 0);
	    }

           | MSGNAME '=' INT
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, $<intval>3, 1);
	    }

           | NAME '=' INT
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, $<intval>3, 1);
	    }
           | DESC enum_entry
             {
		 $<enument>$ = cltype_enument_adddesc($<enument>2,
						      $<strval>1);
	     }

           ;

%%
     
#include <ctype.h>

yyerror(s)
     char *s;
{
    fprintf(stderr, "%s:%d '%s'\n", input_filename, input_linenum, s);
}

static FILE *ifp = stdin;

yysetup(fp, name)
     FILE *fp;
     char *name;
{
    if (fp) {
	ifp = fp;
    }
    if (input_filename) {
	clipc_free(input_filename);
    }
    if (name == 0) {
	input_filename = clipc_strdup(0, "stdin");
    }
    else {
	input_filename = clipc_strdup(0, name);
    }
    input_linenum = 1;
}

#define CLTYPE_ISNAMECHAR(c) (isalpha(c) || (c) == '_' || isdigit(c))

char *lex_name(FILE *ifp, int *is_msgname)
{
    static char rbuf[1024];
    char *cp;
    int c;
    
    c = getc(ifp);
    for (cp = rbuf; CLTYPE_ISNAMECHAR(c); c = getc(ifp)) {
	if (is_msgname && (!isalpha(c) || isdigit(c)))
	  *is_msgname = 1;

	*cp++ = c;
    }
    *cp = 0;
    
    if (c != EOF)
      ungetc(c, ifp);
    return rbuf;
}

yylex ()
{
    int c;
    char *name;
    static char *comment_buf;
    static comment_buflen;
    char *comment_cp = comment_buf;

  rescan:
    /* skip white space  */
    while ((c = getc(ifp)) == ' ' || c == '\t' || c == '\n')
      if (c == '\n')
	input_linenum++;
    
    switch (c) {
      case ';':
	while ((c = getc(ifp)) != EOF && c != '\n')
	  continue;
	if (c == EOF)
	  return 0;
	input_linenum++;
	goto rescan;
	
      case '/':
	c = getc(ifp);
	if (c != '/') {
	    ungetc (c, ifp);
	    c = ';';
	    break;
	}
	c = getc(ifp);
	if (c != ' ')
	  ungetc (c, ifp);

	while ((c = getc(ifp)) != EOF && c != '\n') {
	    if (comment_cp+4 >= comment_buf + comment_buflen) {
		char *tmp;
		tmp = malloc(comment_buflen+1024);

		if (comment_buf)
		  free(comment_buf);
		comment_buf = tmp;
		comment_cp = comment_buf + comment_buflen;
		comment_buflen += 1024;
	    }
	    *comment_cp++ = c;
	}
	if (c == EOF)
	  return 0;
	input_linenum++;
	if (comment_buf) {
	    *comment_cp++ = '\n';
	    *comment_cp = 0;
	}
	    
	yylval.strval = comment_buf ? strdup(comment_buf) : 0;
	return DESC;

      case '(':
      case ')':
      case ',':
      case ':':
      case '-':
      case '&':
      case '=':			
	return c;
      case EOF:
	return 0;
	
      default:
	break;
    }
    
    if (isdigit(c)) {
	int rval = 0;
	
	while (isdigit(c)) {
	    rval = 10 * rval + (c - '0');
	    c = getc(ifp);
	}
	if (c != EOF)
	  ungetc(c, ifp);
	yylval.intval = rval;
	return INT;
    }

    if (CLTYPE_ISNAMECHAR(c)) {
	int is_msgname = 0;
	ungetc(c, ifp);
	name = lex_name(ifp, &is_msgname);
	if (name == 0)
	  return 0;
	if (name[0] == 'T' && name[1] == '_') {
	    if (strcmp(name, "T_DCT") == 0) 
	      return T_DCT;
	    
	    if (strcmp(name, "T_DSC") == 0) 
	      return T_DSC;
	    
	    if (strcmp(name, "T_ARY") == 0) 
	      return T_ARY;
	    
	    if (strcmp(name, "T_ENU") == 0) 
	      return T_ENU;
	}

	yylval.strval = clipc_strdup(0, name);
	if (is_msgname)
	  return MSGNAME;
	else
	  return NAME;
    }
    goto rescan;
}

char * token_name(t)
     int t;
{
    switch (t) {
      case NAME:
	return "NAME";

      case INT:
	return "INT";

      case MSGNAME:
	return "MSGNAME";

      case T_DCT:
	return "T_DCT";
	
      case T_ARY:
	return "T_ARY";
	
      case T_DSC:
	return "T_DSC";
	
      case T_ENU:
	return "T_ENU";

      case '(':
	return "(";
	
      case ')':
	return ")";
	
      case ':':
	return ":";
	
      case '&':
	return "&";
      case '=':
	return "=";
	
      case ',':
	return ",";
	
      case '-':
	return "-";
	
      case 0:
	return "EOF";
    }	
    return "UNKNOWN";
}

print_lex_value(int rval)
{
    switch (rval) {
      case NAME:
      case MSGNAME:
	printf("(%s)\n", yylval.strval);
	break;

      case DESC:
	printf("DESCRIPTION: '%s'\n", yylval.strval);
	break;

      case INT:
	printf("(%d)\n", yylval.intval);	
	break;

      case T_DCT:
	printf("(T_DCT)\n");
	break;

      case T_ARY:
	printf("(T_ARY)\n");
	break;

      case T_ENU:
	printf("(T_ENU)\n");
	break;

      case T_DSC:
	printf("(T_DSC)\n");
	break;

      case '(': case ')':
      case ',': case ':':
      case '-': case '&': case '=':			
	printf("('%c')\n", rval);
	break;
	
      case 0:
	return 0;
	
      default:
	printf("%d: (%d)\n", rval, yylval.intval);
	break;
    }
}


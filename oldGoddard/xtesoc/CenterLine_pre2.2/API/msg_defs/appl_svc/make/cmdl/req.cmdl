//-SECTION
//    *.+ Make Server Request Messages
//
//-DESCRIPTION
// Messages which provide an interface to the Make/Compile server
// are defined in this section.


//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request a make or a compile with captured error messages.

compile_serve
 -request (
// The current directory to use for the command shell.
	directory: STRING,

// The command to execute, usually a make or compile, which is
// supplied to "/bin/sh -c".  This can optionally be replaced with
// "argc" and "argv".
	&command: STRING,

// Argv-style command description of command to execute if "command"
// is not supplied.
	&argv : (T_ARY STRING),	// array of strings containing command and its
				// arguments.

// The filename to use as the standard input of the shell.
// If this is not provided the workspace is used and the process
// is placed in the foreground of the workspace tty.
	&stdin: STRING,

// The filename to use as the standard output of the shell.
// If this is not provided the workspace is used.
	&stdout: STRING,

// The filename to use as the standard error  of the shell.
// If this is not provided the workspace is used.
	&stderr: STRING
 )

 -notify (
// The exit code from the requested command.  Command-dependent
// so not definable here.
	exitCode: INT
 )

//-END MESSAGE DEFINITION compile_serve

//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request the execution of a program.

exec_serve
 -request (
// The current directory to use for the command shell.
	directory: STRING,

// The command to execute, which is supplied to "/bin/sh -c".
// This can optionally be replaced with "argc" and "argv".
	&command: STRING,

// Argv-style command description of command to execute if "command"
// is not supplied.
	&argv : (T_ARY STRING),	// array of strings containing command and its
				// arguments.

// The filename to use as the standard input of the shell.
// If this is not provided the workspace is used and the process
// is placed in the foreground of the workspace tty.
	&stdin: STRING,

// The filename to use as the standard output of the shell.
// If this is not provided the workspace is used.
	&stdout: STRING,

// The filename to use as the standard error  of the shell.
// If this is not provided the workspace is used.
	&stderr: STRING
 )

 -notify (
// The exit code from the requested command.  Command-dependent
// so not definable here.
	exitCode: INT,

// Optional text describing why execution failed.
	&errorText : STRING
 )

//-END MESSAGE DEFINITION exec_serve

//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request a make or a compile.  This is provided for backwards
// compatibility and is superceded by compile_serve.

make_serve
 -request (
// The current directory to use for the command shell.
	directory: STRING,

// The command to execute, usually a make or compile.
	command: STRING,

// The filename to use as the standard input of the shell.
// If this is not provided the workspace is used and the process
// is placed in the foreground of the workspace tty.
	&stdin: STRING,

// The filename to use as the standard output of the shell.
// If this is not provided the workspace is used.
	&stdout: STRING,

// The filename to use as the standard error  of the shell.
// If this is not provided the workspace is used.
	&stderr: STRING
 )

 -notify (
// The exit code from the requested command.  Command-dependent
// so not definable here.
	exitCode: INT
 )

//-END MESSAGE DEFINITION make_serve

//-END SECTION Exec Server Request Messages

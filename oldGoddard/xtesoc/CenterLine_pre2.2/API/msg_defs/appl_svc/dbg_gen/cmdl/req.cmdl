//-SECTION
//    *.+ Generic Debugger Request Messages
//
//-DESCRIPTION
// Request messages providing interfaces to functions provided by all
// standard debuggers are described in this section.




//-SECTION
//    .+ Misc Functions
//
//-DESCRIPTION
// Messages requesting various misc functions which don't fit anywhere
// else are found in this section.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Change the debugger's notion of what its current directory is.

cxcmd_chdir
 -request (
// Directory to change to.
	directory: STRING
 )

 -notify (
// Boolean status value.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcnd_chdir




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// A request to find out what the current directory is for the debugger.

cxcmd_cwd
 -request ()

 -notify (

// The host that the directory resides on.
	hostName: STRING,

// The current directory.
    	currentDir: STRING,

// Inidication of success of the request: 0 = no error, 1 = error.
// If field does not appear, this also indicates no error.
    	& isError: INT,

// Human-readable string indicating error status.  Field appears
// only if isError == 1.
    	& errorString: STRING
  )

//-END MESSAGE DEFINITION cxcmd_cwd




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to execute an arbitrary command, as if typed at the workspace
// command line prompt.
// This request should be used only for special cases.  If it
// is needed for a regular feature in the CenterLine environment,
// a specific request message should be defined.

cxcmd_exec
 -request (
// Text of command to execute.
	commandLine: STRING,

// Echo command to the workspace?
	& cxcmdEchoCommand: Bool,

// Echo output of command to the workspace?
	& excmdEchoOutput: Bool		;probably not a good idea.
  )

 -notify (
// Boolean status value.  Note that this is NOT the status of
// the executed command, just the status of whether or not the
// command was accepted as if typed in the workspace.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_exec




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request the facility which displays source (the source panel)
// to display a section of a source file.

show_file_lines
 -request (
// Path for source file.
	fileName: STRING,

// Line number to begin display.
	lineNumber: INT,

// Number of lines to display.
	nlines: INT
  )

 -notify (
// Status value.  0 = OK.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION show_file_lines

//-END SECTION Misc Functions




//-SECTION
//    + Breakpoint and Watchpoint Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// breakpoint, watchpoint, and other program execution and data
// monitoring functions.  Breakpoints are maintained in a numbered
// list, each containing the type of the breakpoint.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Asks CX to delete a breakpoint, watchpoint or display item.
// The notification may be sent without a CLIPC request, in response
// to a workspace command, or the unloading of a file containing the
// location.

cxcmd_delete
 -request (
// The number of the item to delete.
    	itemNumber: INT
 )

 -notify (

// The number of the item that was deleted.
    	itemNumber: INT,

// The type of the deleted item.
// Values not determined at time of publication.
    	itemType   : STRING,

// The status of the item that was deleted.
// Values not determined at time of publication.
    	itemStatus: STRING,

// Inidication of success of the request: 0 = no error, 1 = error.
// If field does not appear, this also indicates no error.
    	& isError: INT,

// Human-readable string indicating error status.  Field appears
// only if isError == 1.
    	& errorString: STRING
 )

//-END MESSAGE DEFINITION cxcmd_delete




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Asks CX to display an expression at each breaklevel (and maybe at 
// each prompt).
//
// Warning-  The reply message is not well implemented here.  Either it is an
// error, with a non-zero status field, or it is a valid reply with
// no status field.  This will be fixed for the next Beta release.

cxcmd_display 
 -request (
// The text of the expression to display.
	& disptext: STRING,

// The number to reuse if you want to do a delete and display in an
// atomic operation.
	& dispNumber: INT,

// Conditional: if this is NOT true anymore (should be true at first) CX should
// remove this display item.  This condition should be checked before
// the display is updated.
        & condition: STRING,
 )

  -notify (
// The number of the display item.
	& dispNumber: INT,	

// The actual Expression that was displayed, with a name, type and value.
	& dispExpr: Expression,

// Is the expression in scope?
	& isInScope: Bool,

// Boolean status value.
	& status: INT
 ) 

//-END MESSAGE DEFINITION cxcmd_display




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Turn off display of displays if they are currently turned on.

cxcmd_disable_display 
 -request (
 )

  -notify (
// Boolean status value.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
 ) 

//-END MESSAGE DEFINITION cxcmd_disable_display




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Turn on display of all displays if at least one is currently set
// and displays have been turned off.

cxcmd_enable_display 
 -request (
  )

  -notify (
// Boolean status value.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
 ) 

//-END MESSAGE DEFINITION cxcmd_enable_display




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Message sent when another process wants to query CX for the
// Status Items that are currently active.  The reply has three
// lists, organized by triggering mechanism for each of the actions.
//
// The displayList field contains displays, which are evaluated and printed
// at each breakpoint.
//
// The anchorList field contains actions which are evaluated at specified
// program locations.
//
// The watchpointList field contains actions which are evaluated when memory
// changes value.

cxcmd_status
 -request ()  ;; a cxcmd_status request is not yet specified
              ;; ???? Is this true?  What causes the notification
	      ;; to be sent then?  Need a PROTOCOL section?

 -notify  (
// The list of display items (expressions which are
// evaluated at each breaklevel).
	displayList: (T_ARY StatusItem),

// The list of anchor items (code that is evaluated at a 
// given program location).
	anchorList: (T_ARY StatusItem),

// The list of watchpoints (code that is evaluated when a memory location is
// modified)
	watchpointList: (T_ARY StatusItem)
 )

//-END MESSAGE DEFINITION cxcmd_status




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to set a breakpint at a given source location.
// This is not used by the product itself, which uses cxcmd_action
// instead.
;; ???? why can't we specify a function?

cxcmd_stop
 -request ( 
// The line number to stop at.
	lineNumber: INT,

// The path name of the file to set the breakpoint in.
	pathName: STRING
 )

 -notify  ( 
;; ???? need better response?? 
// Indication of success of the request: 0 = no error, 1 = error.
// If field does not appear, this also indicates no error.
;; ???? should be Bool?
    	& isError: INT,

// Human-readable string indicating error status.  Field appears
// only if isError == 1.
    	& errorString: STRING
 )

//-END MESSAGE DEFINITION cxcmd_stop

//-END SECTION Breakpoint and Watchpoint Functions




//-SECTION
//    + Program Stack Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// operations on the program stack, including displays and traversals.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to move the scope location down a given number of stack frames.

cxcmd_down
 -request (
// How many stack frames to move down.  Values out of range
// of the current stack will just set scope location to
// the bottom of the stack.  If this field is not included,
// a value of 1 is assumed.
	& numLevels: INT
 )

 -notify  (
// Boolean status value.  Will always be set to 0.
	status: INT
 )

//-END MESSAGE DEFINITION cxcmd_down




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to dump the current stack frame, or the frame of a named function.

cxcmd_dump
 -request (
// Optionally, the function whose frame should be dumped.
	& func: STRING,			

// Not determined at time of publication.
	& text: STRING
 )			

 -notify (
;; ???? Should be tied to StackFrame pseudo structure?
// Array of values of formal parameters in the selected stack frame.
	& formalParams: (T_ARY VarDump),

// Array of values of automatic (local) variables in the selected stack frame.
	& autoParams: (T_ARY VarDump),

// Boolean status value.
	status: INT,

// Human-readable string indicating error status.
	& errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcmd_dump




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Tell CX to go up a stack frame.  The default number of frames is 1
// If the optional numLevels contains an int, go up that many levels.
// If it is impossible to go up the desired number of levels, leave the
// scope location where it is, and return an error and error message.

cxcmd_up
 -request (
// Optional number of stack frames to go up.  Defaults to 1.
// If the value is greater than the number of frames on the stack,
// the top frame will be selected.
	&numLevels: INT
 )

 -notify (
// Boolean status value.  Will always be set to 0.
	status: INT
 )

//-END MESSAGE DEFINITION cxcmd_up




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX about the current scope location.

cxcmd_where
 -request (
// Optionally, restrict the number of stack frames described.  If no
// numLevels is specified, all current stack frames will be described.  If
// a positive integer is given, that many stack frames, starting from the
// current scope location will be described.
	& numLevels: INT
 )

 -notify  (
// The list of stack frames.
	frameList: (T_ARY StackFrame)
 )

//-END MESSAGE DEFINITION cxcmd_where




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX about scope and execution locations.  The two locations can be
// different if the scope location is moved with up and down commands
// (and other scope location browsing commands).

cxcmd_whereami
 -request ()

 -notify  (
// The scope location.
	scopeLocation: StackFrame,

// The execution location.  
	execLocation: StackFrame
 )

//-END MESSAGE DEFINITION cxcmd_whereami

//-END SECTION Program Stack Functions




//-SECTION
//    + Expression Display and Info Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// operations on (possibly simple) expressions, including queries
// for the values of variables, expressions, and addresses.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX for name, type, and value info about a variable, address
// or expression.
cxcmd_info
 -request (
// The varible to describe.
// Set this to an empty string if you want info on an address
// or an expression.
	var: STRING,	

// The address to describe.
// Set this to 0 if you want info on a variable
// or an expression.
	addr: INT,

// An expression to describe.
// Set this to an empty string if you want info on an address
// or a variable.
	expr: STRING
 )

 -notify (
// The complete expression containing name, type and value.  
	type: ExprType
 )

//-END MESSAGE DEFINITION cxcmd_info




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to print the value of an expression.
cxcmd_print
 -request (
// Give CX an indication of how much type information is required.
	typeDepth: INT,

// The actual expression to evaluate and 'print' by returning its value.
	expr: STRING,

// Boolean: Should the command be echoed to the workspace?
	& echoCommand: INT,

// Boolean: Should the output be echoed to the workspace?
	& echoOutput: INT 
 )	

 -notify (
// The expression that was evaluated.
	expr: STRING,		

// The value of the expression.
	output: Expression
 )

//-END MESSAGE DEFINITION cxcmd_print




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to set a value.

cxcmd_setval
 -request (
// The address of the value that needs to be set.
	valAddr: INT,

// Offset of the data to set (in bits).  
//      valOffset: INT,

// Size of data in bits.  This and valOffset allow setting bitfields.
// If set to zero (0), valOffset and valSize are ignored.
        valSize: INT,

// The type of the value that needs to be set.
	valType: STRING,

// The expression that describes the new value.
	valExpr: STRING
 )

 -notify ()		

//-END MESSAGE DEFINITION cxcmd_setval




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX about an identifier.  Returns info on all references of
// the specified name.

cxcmd_whatis
 -request (
// The name of the identifier about which information is being requested.
	idName: STRING,

// Boolean: Should the command be echoed to the workspace?
	& echoCommand: Bool,

// Boolean: Should the output be echoed to the workspace?
	& echoOutput: Bool
 )

 -notify  (
// The name of the identifier about which information is being requested,
// corresponding to idName in the request.  (The name is different in the
// reply for historical reasons.)
	symbol: IdentifierName,

// The list of references of the requested name.
	useList: (T_ARY RefSym),

// Status indication for the command.
	status: (T_ENU OK = 0, NOT_FOUND = -1),

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcmd_whatis




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX where a given identifier is defined.  Reports on the source
// file(s) and line number(s) of the all definitions of the identifier.
cxcmd_whereis
 -request (
// The name of the identifier about which information is being requested.
	symbolName: STRING,

// Boolean: Should the command be echoed to the workspace?
	& echoCommand: INT,

// Boolean: Should the output be echoed to the workspace?
	& echoOutput: INT 
)

 -notify  (
// The name of the identifier about which information is being requested,
// corresponding to idName in the request.  (The name is different in the
// reply for historical reasons.)
	symbol: IdentifierName,

// A list of locations where the symbol is defined.
	locationList: (T_ARY RefSym),

// Indication of success of the request.
// If field does not appear, this also indicates no error.
    	& isError: Bool,

// Human-readable string indicating error status.  Field appears
// only if isError == TRUE.
    	& errorString: STRING
 )

//-END MESSAGE DEFINITION cxcmd_whereis

//-END SECTION Expression Display and Info Functions




//-SECTION
//    + Program Execution Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// debugged program execution.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Continues program execution from the current breakpoint,
// until the next breakpoint or end of program.
//
//-SIDE EFFECT MESSAGES
//    event_cont
//    event_notready
//    event_ready
//    event_run_end
//    event_stop_loc

cxcmd_continue
  -request ()

  -notify (
// Command status enumeration.
	status: (T_ENU OK = 0, ERROR = -1),

// Human-readable status if status is !OK.
// Not returned if status = OK
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_continue




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Continues execution of the debugged program for a single source
// statement.  Does not step into called functions.
//
//-SIDE EFFECT MESSAGES
//    event_cont
//    event_notready
//    event_ready
//    event_run_end
//    event_stop_loc

cxcmd_next
  -request ()

  -notify (
// Command status enumeration.
	status: (T_ENU OK = 0, ERROR = -1),

// Human-readable status if status is !OK.
// Not returned if status = OK
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_next




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Continues execution of the debugged program for a single source
// statement.  Steps into called functions.
//
//-SIDE EFFECT MESSAGES
//    event_cont
//    event_notready
//    event_ready
//    event_run_end
//    event_stop_loc

cxcmd_step
  -request ()

  -notify (
// Command status enumeration.
	status: (T_ENU OK = 0, ERROR = -1),

// Human-readable status if status is !OK.
// Not returned if status = OK
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_step




//-END SECTION Program Execution Functions

//-END SECTION Generic Debugger Request Messages



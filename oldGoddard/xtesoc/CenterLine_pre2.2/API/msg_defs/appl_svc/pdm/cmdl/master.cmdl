//-APPLICATION SERVICE
//	2 The Process Debugging Mode Debugger
//
//-DESCRIPTION
// Standard debugging engine used to debug linked executables
// (processes).  Provides all standard generic debugger facilities.




//-SECTION
//	2.1 Request Message Definitions
//
//-DESCRIPTION
//	The PDM provides a message-based programmatic interface with
//	subprogram-call semantics.  Generally, request messages and
//	their replies have function semantics, with the return value(s)
//	returned in the reply message.  However, this is not always the
//	case.  Often a request is made for the side effects only.
//	These cases of messages implementing procedure-call semantics
//	are outlined in detail in the PROTOCOL sections of the pages on
//	these messages.  If you don't see documentation speciifically
//	outlining the side effects of a request, it is most often the
//	case that the request provides simple function-call semantics.

INCLUDE ../std/cmdl/req.cmdl
INCLUDE ../dbg_gen/cmdl/req.cmdl

//-END SECTION Request Message Definitions




//-SECTION
//	2.2 Notificaton Message Definitions
//
//-DESCRIPTION
//	Side effect messages emitted during PDM execution are documented
//	here.  These messages identify state transitions occurring
//	within the PDM, and are often used to notify requestors of the
//	effects of their (or others) requests.

INCLUDE ../std/cmdl/not.cmdl
INCLUDE ../dbg_gen/cmdl/not.cmdl
INCLUDE ../pdm/cmdl/not.cmdl

//-END SECTION Notification Message Definitions




//-SECTION
//	2.3 Definitions
//
//-DESCRIPTION
//	The following definitions are used in the previous message
//	definitions.

INCLUDE ../std/cmdl/def.cmdl
INCLUDE ../dbg_gen/cmdl/def.cmdl

//-END SECTION Definitions

//-END APPLICATION SERVICE The Process Debugging Mode Debugger



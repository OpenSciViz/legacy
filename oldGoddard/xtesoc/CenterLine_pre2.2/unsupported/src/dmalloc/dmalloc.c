/* dmalloc.c:
 *
 * debugging malloc wrapper routines.
 *
 * this is in the public domain.
 *
 * see the associated README file for an implementation description.
 * queries and enhancements should be directed to jimf@centerline.com.
 *
 * written by jim frost 06.07.90
 */

#include <stdio.h>

#ifdef malloc
#undef malloc
#endif
#ifdef calloc
#undef calloc
#endif
#ifdef realloc
#undef realloc
#endif
#ifdef free
#undef free
#endif

#ifdef __CODECENTER__
extern FILE *centerline_stderr;
#else /* !__CODECENTER__ */
#ifdef __SABER__
extern FILE *saber_stderr;
#endif /* __SABER__ */
#endif /* !__CODECENTER__ */

char *getenv();
char *malloc();
char *calloc();
char *realloc();
void  free();

#define ABS(A) ((A) < 0 ? -(A) : (A))

#define MALLOC_MARK_BYTE (char)0xbf /* end of malloc area mark byte */

/* malloc information bucket.
 */

struct mallocbucket {
  char *addr; /* address of malloc */
  int   len;  /* length of malloc not including overwrite byte */
  char *file; /* where malloc was done */
  int   line;
  struct mallocbucket *prev, *next; /* links to malloc list */
};

/* malloc header.  this is 8-byte so it won't affect alignment.
 */

struct mallocheader {
  struct mallocbucket *bucket; /* pointer to bucket which owns this malloc */
  unsigned int         magic;  /* magic number used as leading fence */
};

#define MALLOC_MAGIC 0x4d414cbf /* "MAL" 0xbf */
#define MALLOC_FREE  0x465245bf /* "FRE" 0xbf; not really used for anything
				 * in particular */

static struct mallocbucket *MallocAllocList= NULL;     /* list of all allocs */
static struct mallocbucket *MallocFreeListHead= NULL;  /* free buckets */
static struct mallocbucket *MallocFreeListTail= NULL;

static int   MallocInit= 0;        /* true if already initialized */
static int   MallocDebug= 0;       /* true if debugging enabled */
static int   MallocAbort;          /* abort if malloc error detected */
static int   MallocFast;           /* fast malloc; minimal checking */
static int   MallocSmall;          /* disable leading-fence checking */
static int   MallocBucketCount= 0; /* # of buckets allocated */
static FILE *MallocFile;           /* output file for errors */

/* this is used to keep a history of malloc's, etc. that were done recently
 */

#define MALLOC_HISTORY_SIZE 10
static unsigned int MallocHistoryPtr= 0;

static struct mallochistory {
  char *op;   /* allocation operation */
  char *addr; /* allocation address */
  char *file; /* file operation took place in */
  int   line; /* line operation tool place at */
} MallocHistory[MALLOC_HISTORY_SIZE];

/* ARGSUSED */
static void mallocProblem(file, line, str, a, b, c, d, e, f, g)
     char *file;
     int   line;
     char *str;
     int   a, b, c, d, e, f, g;
{ int i;

  if (!MallocDebug) /* silence! */
    return;
  fprintf(MallocFile, str, a, b, c, d, e, f, g);
  fprintf(MallocFile, ", detected at %s:%d\n", file, line);
  fflush(MallocFile);
  fprintf(MallocFile, " Malloc history (oldest to newest):\n");
  for (i= 0; (i < MALLOC_HISTORY_SIZE) && MallocHistory[i].file; i++)
    fprintf(MallocFile, "  %d. %s 0x%08x at %s:%d\n", i + 1,
	    MallocHistory[i].op,
	    (unsigned long)MallocHistory[i].addr,
	    MallocHistory[i].file,
	    MallocHistory[i].line);
#ifdef __CODECENTER__
  fprintf(centerline_stderr, "Generating CodeCenter breaklevel....\n\007");
  fflush(centerline_stderr);
  centerline_stop(""); /* generate CodeCenter breaklevel on problem */
#else /* !__CODECENTER__ */
#ifdef __SABER__
  fprintf(saber_stderr, "Generating Saber-C breaklevel....\n\007");
  fflush(saber_stderr);
  saber_stop(""); /* generate Saber-C breaklevel on problem */
#else /* !__SABER__ */
  fflush(MallocFile);
  if (MallocAbort) { /* abort on problem */
    fprintf(stderr, "dmalloc: Aborting due to malloc error...\n\007");
    abort();
  }
#endif /* !__SABER__ */
#endif /* !__CODECENTER__ */
}

static void freeMallocBucket(file, line, bucket)
     char *file;
     int   line;
     struct mallocbucket *bucket;
{
  struct mallocheader *header;

  /* smash malloc header if there is one
   */

  if (!MallocSmall) {
    header= (struct mallocheader *)(bucket->addr - sizeof(struct mallocheader));
    header->bucket= NULL;
    header->magic= MALLOC_FREE;
  }

  /* unlink from alloc list
   */

  if (bucket->prev)
    bucket->prev->next= bucket->next;
  else
    MallocAllocList= bucket->next;
  if (bucket->next)
    bucket->next->prev= bucket->prev;

  /* add bucket to free list
   */

  bucket->file= file; /* set its location to the free location */
  bucket->line= line;
  bucket->prev= MallocFreeListTail;
  if (MallocFreeListTail)
    bucket->prev->next= bucket;
  else
    MallocFreeListHead= bucket;
  MallocFreeListTail= bucket;
  bucket->next= NULL;
}

/* this verifies the integrity of malloc'ed areas and frees a bucket if
 * requested.
 */

void checkMallocBuckets(file, line, addr)
     char *file;
     int   line;
     char *addr;
{
  struct mallocheader *header;
  struct mallocbucket *bucket, *freebucket;
  int bucketcount= 0;

  /* if we're in "fast" mode and we don't have anything to free,
   * skip the check.
   */

  if (MallocFast && !addr)
    return;

  /* if we're in fast mode and have headers and want to delete something,
   * this does it quickly unless its header or trailer is smashed in some way.
   */

  if (MallocFast && !MallocSmall &&
      (addr > (char *)sizeof(struct mallocheader))) {
    header= (struct mallocheader *)(addr - sizeof(struct mallocheader));
    if ((header->magic == MALLOC_MAGIC) &&
	(*(addr + header->bucket->len) == MALLOC_MARK_BYTE)) {
      freeMallocBucket(file, line, header->bucket);
      return;
    }
  }

  /* verify free list
   */

  if (MallocFreeListHead && (MallocFreeListHead->prev))
    mallocProblem(file, line, "Malloc free bucket list (head) smashed");
  if (MallocFreeListTail && (MallocFreeListTail->next))
    mallocProblem(file, line, "Malloc free bucket list (tail) smashed");
  for (bucket= MallocFreeListHead; bucket; bucket= bucket->next) {
    bucketcount++;
    if (bucket->next && (bucket->next->prev != bucket))
      mallocProblem(file, line, "Malloc free bucket list (internal) smashed");
    }

  /* verify alloc list and find alloc bucket if necessary
   */

  if (MallocAllocList && (MallocAllocList->prev))
    mallocProblem(file, line, "Malloc allocation list (head) smashed");

  freebucket= NULL;
  for (bucket= MallocAllocList; bucket; bucket= bucket->next) {
    bucketcount++;
    if (addr && (bucket->addr == addr))
      freebucket= bucket;
    if ((bucket->len > 0) && !MallocSmall) {
      header= (struct mallocheader *)(bucket->addr - sizeof(struct mallocheader));
      if (header->magic != MALLOC_MAGIC) {
	mallocProblem(file, line, "Malloc header destroyed at %s:%d",
		      bucket->file, bucket->line);
	bucket->len= -bucket->len;
      }
    }
    if ((bucket->len > 0) &&
	(*(bucket->addr + bucket->len) != MALLOC_MARK_BYTE)) {
	mallocProblem(file, line, "Overrun of malloc at %s:%d",
		      bucket->file, bucket->line);
	bucket->len= -bucket->len; /* mark this as having been smashed */
      }
    if (bucket->next && (bucket->next->prev != bucket))
      mallocProblem(file, line, "Malloc allocation list (internal) smashed");
  }

  if (bucketcount != MallocBucketCount)
    mallocProblem(file, line,
		  "Malloc bucket count doesn't match allocated buckets");

  /* if we wanted to free a bucket, do it now
   */

  if (addr) {
    if (freebucket)
      freeMallocBucket(file, line, freebucket);
    else {

      /* user tried to free something we don't know about.  see if
       * it's on our free list
       */

      for (bucket= MallocFreeListTail; bucket; bucket= bucket->prev)
	if (bucket->addr == addr)
	  break;
      if (bucket) {
	if (!bucket->file)
	  mallocProblem(file, line, "\
Unknown address 0x%x to free/realloc possibly freed previously by an external \
free", addr);
	else
	  mallocProblem(file, line, "\
Unknown address 0x%x to free/realloc, probably freed previously at %s:%d",
			addr, bucket->file, bucket->line);
      }
      else
	mallocProblem(file, line, "\
Unknown address 0x%x to free/realloc, possibly freed previously or allocated \
by an external malloc",
		      addr);
    }
  }
}

static void addMallocHistory(op, addr, file, line)
     char *op;
     char *addr;
     char *file;
     int   line;
{ int a;

  if (MallocHistoryPtr >= MALLOC_HISTORY_SIZE)
    for (a= 0; a < MALLOC_HISTORY_SIZE - 1; a++)
      MallocHistory[a]= MallocHistory[a + 1];
  else
    a= MallocHistoryPtr++;
  MallocHistory[a].op= op;
  MallocHistory[a].addr= addr;
  MallocHistory[a].file= file;
  MallocHistory[a].line= line;
}

static void newMallocBucket(addr, len, file, line)
     char *addr;   /* address that was malloc'ed */
     int   len;    /* length of malloc'ed address excluding mark byte */
     char *file;   /* file in which malloc was done */
     int   line;   /* line at which malloc was done */
{
  int a;
  struct mallocbucket *bucket;
  struct mallocheader *header;

  if (!addr) /* fatal, caught by malloc routines later */
    return;
  if (!len) /* this is really just a warning */
    mallocProblem(file, line, "Malloc is allocating zero length area\n");

  *(addr + len) = MALLOC_MARK_BYTE;

  /* fill in malloc header if necessary.  we can't look for memory
   * conflicts because the malloc address we got doesn't match with
   * the malloc address the system has.
   */
  if (MallocSmall && !MallocFast) {

    /* look for allocation conflicts; these often mean something freed
     * one of our memory blocks without telling us.
     */

    for (bucket= MallocAllocList; bucket; bucket= bucket->next)
      if ((addr + len >= bucket->addr) &&
	  (addr <= bucket->addr + bucket->len)) { /* <= because of mark */
	mallocProblem(file, line, "\
Malloc address conflicts with %s:%d, possibly freed previously with an \
external free",
		      bucket->file, bucket->line);
	if (bucket->prev)
	  bucket->prev->next= bucket->next;
	else
	  MallocAllocList= bucket->next;
	if (bucket->next)
	  bucket->next->prev= bucket->prev;
	bucket->file= NULL;
	bucket->line= 0;
	bucket->prev= MallocFreeListTail;
	if (MallocFreeListTail)
	  bucket->prev->next= bucket;
	else
	  MallocFreeListHead= bucket;
	bucket->next= NULL;
      }
  }

  /* if we have no more entries on our free list, alloc another block
   */

  if (!MallocFreeListHead) {
    MallocBucketCount += BUFSIZ;
    MallocFreeListHead= bucket=
      (struct mallocbucket *)malloc(sizeof(struct mallocbucket) *
				    BUFSIZ);
    for (a= 0; a < BUFSIZ; a++, bucket++) {
      bucket->addr= NULL;
      bucket->len= 0;
      bucket->file= NULL;
      bucket->line= 0;
      if (a == 0)
	bucket->prev= NULL;
      else
	bucket->prev= bucket - 1;
      if (a == BUFSIZ - 1) {
	bucket->next= NULL;
	MallocFreeListTail= bucket;
      }
      else
	bucket->next= bucket + 1;
    }
  }

  bucket= MallocFreeListHead;
  MallocFreeListHead= MallocFreeListHead->next;
  if (MallocFreeListHead)
    MallocFreeListHead->prev= NULL;
  else
    MallocFreeListTail= NULL;
  bucket->addr= addr;
  bucket->len= len;
  bucket->file= file;
  bucket->line= line;
  bucket->prev= NULL;
  bucket->next= MallocAllocList;
  if (bucket->next)
    bucket->next->prev= bucket;
  MallocAllocList= bucket;

  if (!MallocSmall) {
    header = (struct mallocheader *)(addr - sizeof(struct mallocheader));
    header->bucket= bucket;
    header->magic= MALLOC_MAGIC;
  }
}

static void mallocInit()
{ char *s, *p;
  char  option[BUFSIZ];

  MallocInit= 1;

  /* set up defaults
   */

#ifdef __CODECENTER__
  MallocFile= centerline_stderr;
#else /* !__CODECENTER__ */
#ifdef __SABER__
  MallocFile= saber_stderr;
#else /* !__SABER__ */
  MallocFile= stderr;
#endif /* !__SABER__ */
#endif /* !__CODECENTER__ */
  MallocAbort= 0; /* do not abort */
  MallocFast= 0;  /* full checking */
  MallocSmall= 0; /* leading fence checking */

  if (s= getenv("MALLOC_DEBUG")) {
    MallocDebug= 1;

    /* parse options
     */

    while (*s) {
      if (! (p= (char *)index(s, ',')))
	p= s + strlen(s);
      strncpy(option, s, p - s);
      option[p - s]= '\0';
      s = p + (*p ? 1 : 0);

      if (!strcmp(option, "abort"))
	MallocAbort= 1;
      else if (!strcmp(option, "fast"))
	MallocFast = 1;
      else if (!strncmp(option, "file=", 5) && option[5]) {
	if (! (MallocFile= fopen(option + 5, "w"))) {
	  fprintf(stderr, "dmalloc: can't open %s as logfile (using stderr)\n",
		  option + 5);
	  MallocFile= stderr;
	}
      }
      else if (!strcmp(option, "small"))
	MallocSmall= 1;
      else
	fprintf(stderr, "dmalloc: Unrecognized MALLOC_DEBUG option: %s\n",
		option);
    }
  }
}

char *dmalloc(size, file, line)
     int   size;
     char *file;
     int   line;
{ char *ptr;
    
  if (!MallocInit)
    mallocInit();

  if (size < 0) {
    mallocProblem(file, line, "Malloc passed a negative size");
    return(NULL);
  }
  if (MallocDebug) {
    checkMallocBuckets(file, line, NULL);
    if (MallocSmall)
      ptr= malloc(size + 1);
    else {
      ptr= malloc(size + sizeof(struct mallocheader) + 1);
      ptr += sizeof(struct mallocheader);
    }
    addMallocHistory("malloc", ptr, file, line);
    newMallocBucket(ptr, size, file, line);
  }
  else
    ptr= malloc(size);
  
  if (!ptr)
    mallocProblem(file, line, "Malloc failed");
  return(ptr);
}

char *dcalloc(nelem, size, file, line)
     int   nelem, size;
     char *file;
     int   line;
{ char *ptr;
    
  if (!MallocInit)
    mallocInit();

  if (size < 0) {
    mallocProblem(file, line, "Calloc passed a negative size");
    return(NULL);
  }
  if (nelem < 0) {
    mallocProblem(file, line, "Calloc passed a negative element count");
    return(NULL);
  }

  if (MallocDebug) {
    checkMallocBuckets(file, line, NULL);
    size *= nelem;
    if (MallocSmall) 
      ptr= malloc(size + 1);
    else {
      ptr= malloc(size + sizeof(struct mallocheader) + 1);
      ptr += sizeof(struct mallocheader);
    }
    bzero(ptr, size);
    addMallocHistory("malloc", ptr, file, line);
    newMallocBucket(ptr, size, file, line);
  }
  else
    ptr= calloc(nelem, size);
  
  if (!ptr)
    mallocProblem(file, line, "Calloc failed");
  return(ptr);
}

char *drealloc(ptr, size, file, line)
     char *ptr;
     int   size;
     char *file;
     int   line;
{
  if (!MallocInit) {
    mallocInit();
    mallocProblem(file, line, "Realloc called before malloc");
  }
  if (size < 0) {
    mallocProblem(file, line, "Malloc passed a negative size");
    return(NULL);
  }

  if (MallocDebug) {
    checkMallocBuckets(file, line, ptr);
    if (MallocSmall)
      ptr= realloc(ptr, size + 1);
    else {
      ptr= realloc(ptr, size + sizeof(struct mallocheader) + 1);
      ptr += sizeof(struct mallocheader); /* skip header */
    }
    addMallocHistory("realloc", ptr, file, line);
    newMallocBucket(ptr, size, file, line);
  }
  else
    ptr= realloc(ptr, size);

  if (!ptr)
    mallocProblem(file, line, "Realloc failed");

  return(ptr);
}

void dfree(ptr, file, line)
     char *ptr;
     char *file;
     int line;
{

  if (!MallocInit) {
    mallocInit();
    mallocProblem(file, line, "Free called before malloc");
  }

  if (MallocDebug) {
    checkMallocBuckets(file, line, ptr);
    addMallocHistory("free", ptr, file, line);
    if (!ptr) {
      mallocProblem(file, line, "Free called with null argument");
      return;
    }
    if (!MallocSmall)
      ptr -= sizeof(struct mallocheader);
  }

  free(ptr);
}

/* predicate function for qsort.  this sorts by file then line then size.
 */

static int sortPredicate(b1, b2)
     struct mallocbucket **b1, **b2;
{ int ret;

  if (ret= strcmp((*b1)->file, (*b2)->file))
    return(ret);
  if ((*b1)->line != (*b2)->line)
    return((*b1)->line < (*b2)->line ? -1 : 1);
  return(ABS((*b1)->len) > ABS((*b2)->len) ? -1 : 1);
}

/* dump out current allocations to a file.
 */

void dumpMallocBuckets(file, line)
     char *file;
     int   line;
{ unsigned int          a, count, sizecount;
  struct mallocbucket **allocs, *bucket, *lastnode;

  if (!MallocInit)
    mallocInit();
  if (!MallocDebug) {
    fprintf(stderr, "malloc_dump: called without MALLOC_DEBUG enabled\n");
    return;
  }

  /* build array to pass to qsort
   */

  for (count= 0, bucket= MallocAllocList; bucket; bucket= bucket->next)
    count++;
  if (!count) {
    fprintf(MallocFile,
	    "===== No Unfreed Allocations At %s:%d =====\n", file, line);
    fflush(MallocFile);
    return;
  }
  allocs= (struct mallocbucket **)
    malloc(sizeof(struct mallocbucket *) * count);
  for (a= 0, bucket= MallocAllocList; bucket; bucket= bucket->next)
    allocs[a++]= bucket;
  qsort(allocs, count, sizeof(struct mallocbucket *), sortPredicate);
  fprintf(MallocFile,
	  "===== Unfreed Allocations At %s:%d =====\n", file, line);

  /* dump out allocations
   */

  lastnode= allocs[0];
  sizecount= 0;
  for (a= 0; a <= count; a++) {
    if ((a == count) ||                              /* last allocation */
	(strcmp(lastnode->file, allocs[a]->file)) || /* new file */
	(lastnode->line != allocs[a]->line) ||       /* new line in file */
	(lastnode->len != allocs[a]->len)) {         /* alloc size changed */
      fprintf(MallocFile, "%5d allocation%s of %5d byte%s, %s:%d\n",
	      sizecount, (sizecount == 1 ? " " : "s"),
	      ABS(lastnode->len), (ABS(lastnode->len) == 1 ? " " : "s"),
	      lastnode->file, lastnode->line);
      sizecount= 0;
    }
    sizecount++;
    lastnode= allocs[a];
  }
  free(allocs);
  fprintf(MallocFile, "\n");
  fflush(MallocFile);
}

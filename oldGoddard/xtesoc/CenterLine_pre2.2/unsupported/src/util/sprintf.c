
#include <stdarg.h>
#include <malloc.h>

#define SPRINTF_RETURNS_INT 1

#if defined(SPRINTF_RETURNS_INT)
int
#else  
char *
#endif  
sprintf (char *s, char *format, ...)
{
    struct _iobuf *ofp, *tmpfile();
    va_list pvar;
    int size;
    char *nbuf;

    va_start(pvar, format);
    /* use a tmpfile since vfprintf won't overflow any buffers */
    ofp = tmpfile();
    vfprintf(ofp, format, pvar);

    /* find out the size */
    size = ftell(ofp);

    /* allocate a temp buffer */
    nbuf = malloc(size+1);

    /* read the contents of the temp file */
    rewind(ofp);
    fread(nbuf, 1, size, ofp);
    nbuf[size] = 0;

    /* close the FILE now since we may longjmp from an error in bcopy */
    fclose(ofp);
    
    strncpy( s, nbuf,size+1);
    free(nbuf);
#if defined(SPRINTF_RETURNS_INT)
    return size;
#else  
    return s;
#endif  
}


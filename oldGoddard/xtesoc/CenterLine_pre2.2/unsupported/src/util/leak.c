
/* Copyright (c) 1991 by Centerline Software, Inc., Cambridge, MA.  All rights */
/* reserved.   Use of this source code must contain this copyright banner.  */


/* 
   This has only been tested for the sparc architecture.

    Please note:  THIS BARELY WORKS.  It is placed here because
    someone, somewhere, might be interested in it as a sample
    CodeCenter IPC program.  

   Enjoy, and please send constructive modifications back to me,
   and I'll try to incorporate them into future versions.

   Shiva Kumar 
   Centerline Software, Inc.
   shiva@centerline.com
*/
#include <stdio.h>
#include <ctype.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "clipc.h"

CLIPC_CONN clipc_conn = 0;


main(argc, argv)
char **argv;
{
    CLIPC_MSG msg;
    CLIPC_MSG reply_msg;
    char *msg_name;
    CLIPC_STATUS status;
    CLIPC_MSG request;
    extern errno;
    char *filename;
    int errcount = 0;
    char pathname[1024];
    char buf[1024];

    clipc_conn = init_clipc_connection(0);
    if (clipc_conn == 0) {
        fprintf(stderr, "init_clipc_connection failed.\n");
        exit(1);
    }


    request = clipc_request_alloc("cx_alloc_start_monitor", 0, 0);
    clipc_msg_addstring(request, "filename", "/tmp/leak.temp");
    clipc_msg_addint(request, "traceLevel", 3);
    reply_msg = clipc_request_send(clipc_conn, request, 0);
    clipc_msg_free(reply_msg);

    printf("Press return when ready to stop collecting leaks");
    gets(buf);
    request = clipc_request_alloc("cx_alloc_stop_monitor", 0, 0);
    reply_msg = clipc_request_send(clipc_conn, request, 0);
    filename = clipc_msg_getstring(reply_msg, "filename", 0);

    printf("Leaks stored in '%s'\n", filename);
    sprintf(buf, "more %s", filename);

    system(buf);
}

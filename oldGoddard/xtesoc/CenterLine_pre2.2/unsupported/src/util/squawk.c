
/* Copyright (c) 1991 by Centerline Software, Inc., Cambridge, MA.  All rights */
/* reserved.   Use of this source code must contain this copyright banner.  */


/* 
   This has only been tested for the sparc architecture.

    Please note:  THIS BARELY WORKS.  It is placed here because
    someone, somewhere, might be interested in it as a sample
    CodeCenter IPC program.  

   Enjoy, and please send constructive modifications back to me,
   and I'll try to incorporate them into future versions.

   Shiva Kumar 
   Centerline Software, Inc.
   shiva@centerline.com
*/

#include <clipc.h>
#include <stdio.h>

main()
{
CLIPC_CONN recv_conn;
CLIPC_MSG recv_msg;
CLIPC_STATUS status;

  recv_conn = init_clipc_connection(&status);
    if (status != CLIPC_STATUS_OK) {
        printf("error return %d from init_clipc_connection\n",
               status);
        exit(2);
    }

  register_clipc_listener(recv_conn,"event_compile_error");
  register_clipc_listener(recv_conn,"event_run_start");
  register_clipc_listener(recv_conn,"event_run_end");

  while(1) {

	char *msg_name;
	char buf[100];
	recv_msg = clipc_read_msg(recv_conn, 0);
	msg_name = clipc_msg_getname(recv_msg, 0);

	if (strcmp(msg_name,"event_compile_error") == 0) {
		sprintf(buf,"cat /s/users/kaufer/sound/mindgoing.au >> /dev/audio");
		system(buf);
	}

	if (strcmp(msg_name,"event_run_start") == 0) {
		sprintf(buf,"cat /s/users/kaufer/sound/ring.au >> /dev/audio");
		system(buf);
	}

	if (strcmp(msg_name,"event_run_end") == 0) {
		sprintf(buf,"cat /s/users/kaufer/sound/ring.au >> /dev/audio");
		system(buf);
	}

  }
  }


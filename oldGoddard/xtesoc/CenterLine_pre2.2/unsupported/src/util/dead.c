/* Copyright (c) 1991 by Centerline Software, Inc., Cambridge, MA.  All rights */
/* reserved.   Use of this source code must contain this copyright banner.  */


/* 
   This has only been tested for the sparc architecture.

    Please note:  THIS BARELY WORKS.  It is placed here because
    someone, somewhere, might be interested in it as a sample
    CodeCenter IPC program.  

   Enjoy, and please send constructive modifications back to me,
   and I'll try to incorporate them into future versions.

   Shiva Kumar 
   Centerline Software, Inc.
   shiva@centerline.com
*/

#include <stdio.h>
#include <clipc.h>

struct SymNode {
	char *SymbolName;
	char *FileName;
	char *SymbolKind; /* only for funcs for now */
	char *Description;
	struct SymNode *next;
} *symlist ;
int num_symbols;
main()
{
	CLIPC_CONN recv_conn;
	CLIPC_MSG recv_msg;
	CLIPC_MSG reply;
	CLIPC_STATUS status;
	CLIPC_ARRAY arr;
	int size;
	int i;
	struct SymNode *next;

	symlist = NULL;
	num_symbols = 0;

	recv_conn = init_clipc_connection(&status);

	if (status != CLIPC_STATUS_OK) {
		fprintf(stderr,"clipc connection:error bad return status \n");
		exit(2);
	}

	recv_msg = clipc_request_alloc("cxsymbol_dump",0,(CLIPC_STATUS *)0);
	reply = clipc_request_send(recv_conn,recv_msg,&status);

	arr = clipc_msg_getarray(reply,"Symbols",&status);

	if (status != CLIPC_STATUS_OK) {
		exit (25);
	}
	size = clipc_array_size(arr);
	
	for ( i = 0; i < size; i++) {
		char *symbol;
		char *file;
		char *line;
		char *description;
		char *type;

		CLIPC_DICT symdict;      

		symdict = clipc_array_getdict(arr,i,0);

		symbol = clipc_dict_getstring(symdict,"SymbolName", 0);
		type = clipc_dict_getstring(symdict,"SymbolKind",0);
		description = clipc_dict_getstring(symdict,"Description",0);
		file = clipc_dict_getstring(symdict,"FileName",0);
		line = clipc_dict_getstring(symdict,"Line",0);

		if ((strcmp("func",type) == 0) && (strncmp(symbol,"centerline_",10))) {

			if ((strcmp(symbol,"__stopup__") == 0) ||
			    (strcmp(symbol,"$noerr") == 0))
				continue;

			struct SymNode *new_node = (struct SymNode *) 
						malloc(sizeof(struct SymNode));

			new_node->SymbolName = strdup(symbol);
			new_node->SymbolKind = strdup(type);
			new_node->FileName = strdup(file);
			new_node->Description = strdup(description);
		
			new_node->next = symlist;
			symlist = new_node;
			num_symbols++;
		}	
	} 	/* Built the function list */

	/* Request xref info for each element in the list */
	clipc_msg_free(recv_msg); 
	next = symlist; 
	for (i = 0; i < num_symbols; i++) {
		CLIPC_ARRAY to_refs, from_refs;

		assert(next);
		recv_msg = clipc_request_alloc("cxcmd_xref",0,&status);
		if (status != CLIPC_STATUS_OK)
			exit(200);

		clipc_msg_addstring(recv_msg,"symbolName",next->SymbolName);

	/* Send Xref request */

		reply = clipc_request_send(recv_conn,recv_msg,&status);
		to_refs = clipc_msg_getarray(reply,"referencesTo",&status);
		from_refs = clipc_msg_getarray(reply,"referencesFrom",&status);

		if ((clipc_array_size(to_refs) == 0) && 
			(clipc_array_size(from_refs) == 0)) {
			printf("**********************************\n");
			printf("%s\n",next->SymbolName);
			printf("%s\n",next->FileName);
			printf("%s\n",next->Description);
		}
		next = next->next;
	}	
}
		

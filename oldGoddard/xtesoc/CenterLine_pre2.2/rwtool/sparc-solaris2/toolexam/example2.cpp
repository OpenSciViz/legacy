/*
 * Example 2: Using the RWCString class
 *
 * $Id: example2.cpp,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example2.cpp,v $
# Revision 1.1  1994/05/26  17:48:47  sridhar
# Initial revision
#
 * Revision 2.3  1993/09/16  06:20:07  randall
 * port to xlC
 *
 * Revision 2.2  1993/08/07  17:33:01  jims
 * Flush cout before requesting input from cin (MS C8 bug: streams not tied)
 *
 * Revision 2.1  1992/11/27  22:24:47  myersn
 * eliminate NL
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:19:42   KEFFER
 * Tools.h++ V5.1
 * 
 */


// Include the header file for the class RWCString:
#include <rw/cstring.h>
#include <rw/rstream.h>

void exerciseSubString();	// Defined below

main()
{
  // Construct an empty string:
  RWCString  x;

  // Read a word from the terminal:
  cout << "Type a word: " << flush;
  cin >> x;
  cout << "You typed: " << x << endl;
  
  // Print the number of characters:
  cout << "The length of the word is:\t" << x.length() << endl;

  // Assignment to a char*: 
  RWCString a;
  a = "test string";
  cout << a << endl;
  
  // Access to elements with bounds checking:
#if defined(_MSC_VER) || defined(__xlC__) || defined(__IBMCPP__) && (__IBMCPP__ <= 200)
  // can't resolve operator[] on RWCStrings; claims ambiguity.
  cout << "a(3): " << a(3) << endl;
#else
  cout << "a[3]: " << a[3] << endl;
#endif

  // Construct two RWCStrings:
  RWCString one = "one";
  RWCString two = "two";

  // Concatenate them:
  cout << "one + two :\t" << one + two << endl;
  // Append a char* to one:
  one += "plus";
  cout << "one += plus :\t" << one << endl;

  // Convert a RWCString to upper case:;
  RWCString Case("Test Of toUpper() And toLower()");
  cout << "Original string:   " << Case << endl;
  cout << "toUpper() version: " << toUpper(Case) <<endl;
  cout << "toLower() version: " << toLower(Case) <<endl;

  // Illustration of pattern search:
  RWCString Pattern = "And";
  cout << "Search for pattern using an RWCString containing \"" << Pattern << "\"\n";
  cout << " Index of first match : " 
       << Case.index(Pattern) << "\n\n";

  // Find a char* pattern:
  char* charPattern = "to";
  cout << "Search for pattern using a char* containing \"" << charPattern << "\"\n";
  int iMatch = Case.index(charPattern);
  cout << "Index of first match  : " << iMatch << endl;
  cout << "Index of second match : " 
       << Case.index(charPattern, iMatch+1) << endl;

  exerciseSubString();

  // The following line should not be necessary, but a bug in Sun C++
  // requires it:
  cout.flush();

  return 0;
}

/*
 * Exercise the RWSubString class.
 */

void
exerciseSubString()
{
  // Construct a RWCString and print it out:
  RWCString Y("History is bunk.");
  cout << "\nTest of Substrings.\nOriginal string \"Y\":\n" << Y << endl;

  // Now print out a substring of Y, using a conversion to a RWCString:
  cout << "Y(8,2): " << RWCString(Y(8,2)) << endl;

  // Here is an example of using a RWSubString as an lvalue:
  // This changes the RWCString Y.
  Y(8,2) = "was";
  cout << "Use a substring as an l-value:\n";
  cout << "Y(8,2) = \"was\":\n" << Y << endl;
}

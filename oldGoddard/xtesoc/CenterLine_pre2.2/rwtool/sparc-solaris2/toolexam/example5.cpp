/* 
 * Example 5: Hashed dictionaries
 * This program uses RWCollectableStrings as keys and RWCollectableInts as values.
 *
 * $Id: example5.cpp,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example5.cpp,v $
# Revision 1.1  1994/05/26  17:48:47  sridhar
# Initial revision
#
 * Revision 2.2  1993/08/07  17:33:01  jims
 * Flush cout before requesting input from cin (MS C8 bug: streams not tied)
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:10   KEFFER
 * Tools.h++ V5.1
 * 
 */


/*
 * Declarations for class RWCollectableString:  
 * This class inherits class RWCString and class RWCollectable.  
 * Class RWCollectable has virtual functions hash() and matches() that are 
 * redefined by class RWCollectableString.  These functions are used to 
 * insert and retrieve RWCollectableStrings in the hash table.
 */
#include <rw/collstr.h>

// Declarations for class RWCollectableInt:
#include <rw/collint.h>

// Declarations for RWHashDictionary:
#include <rw/hashdict.h>
#include <rw/rstream.h>

STARTWRAP
#include <stdlib.h>
ENDWRAP

// The following can easily be changed to store and retrieve 
// keys and values of other types:
typedef RWCollectableString KeyType;	
typedef RWCollectableInt    ValueType;

// The following can be changed to use a different container class; 
// for example, a B-Tree:
typedef RWHashDictionary        DictionaryType;

/* Global function to print keys and values.  This function is handed to  
 * RWHashDictionary::apply().
 */
void
printKeyAndValue( RWCollectable* ky, RWCollectable* val, void*)
{
  /* Do the necessary typecast, then print 'em out: */
  cout << *(const KeyType*) ky << "\t" <<  *(const ValueType*)val << endl;
}  

main()
{
  cout << "***************Example of Hash Dictionary *****************\n\n";

  // Construct a dictionary with no entries:
  DictionaryType dictionary;

  // Loop to do various things to the Dictionary:
  // Since the Dictionary stores pointers to keys and values, storage will be 
  // provided from the heap for the actual keys and values.

  KeyType*      theKey;      // pointer to key.
  ValueType*    theValue;    // pointer to value.
  KeyType       aKey;        // Test key.
  char          option;      // Options for switch.

  do {
    cout << dictionary.entries() << " entries in dictionary.\n";

    cout << "(i)nsert (f)ind (d)elete (l)ist (c)lear e(x)it:\t";

    // Borland bug necessitates explicit test for .good():

    if ( !(cin >> option).good() ) break; // Check for EOF
    
    switch ( option ) {
    case 'i':         // Insert a key-value pair.
    case 'I':
      cout << "Enter word to act as key:\t" << flush;
      theKey = new KeyType;   // Allocate from the heap.
      cin >> *theKey;
      cout << "\nEnter int to act as value:\t" << flush;
      theValue = new ValueType;  // Allocate from the heap.
      cin >> *theValue;
      dictionary.insertKeyAndValue(theKey, theValue);
      cout << "Key " << *theKey << " with value " << *theValue << " entered.\n";
      break;
    case 'f':        // Find a value, given a test key.
    case 'F':
      cout << "Enter word to find:\t" << flush;
      cin >> aKey;
      theValue = (ValueType*)dictionary.findValue(&aKey);
      if ( theValue )
	cout << aKey << " is a member of the collection with value " << *theValue << endl;
      else
	cout << aKey << " is not a member of the collection.\n";
      break;
    case 'd':       // Delete a key.
    case 'D':
      cout << "Enter word to be deleted:\t" << flush;
      cin >> aKey;
      RWCollectable* temp;	// Temporary necessary to calm cfront down
      theKey = (KeyType*)dictionary.removeKeyAndValue(&aKey, temp);
      if(theKey){
	theValue = (ValueType*)temp;
	cout << "Key " << *theKey << " with value " << *theValue << " removed.\n";
	delete theKey;         
	delete theValue;
      }
      else
	cout << "Key " << aKey << " not found.\n";
      break;
    case 'l':    // List contents of Dictionary.
    case 'L':
      dictionary.applyToKeyAndValue(printKeyAndValue, rwnil); // Uses global function defined above.
      break;
    case 'c':   // Clear the Dictionary
    case 'C':
      dictionary.clearAndDestroy();
      break;
    case 'x':
    case 'X':
      break;
    default:
      cerr << "Not recognized.\n";
    }	// end switch
  } while (option != 'x' && option != 'X') ;

  return 0;
}


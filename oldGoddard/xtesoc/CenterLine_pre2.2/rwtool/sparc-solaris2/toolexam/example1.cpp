/*
 * Example 1: classes RWTime and RWDate 
 *
 * $Id: example1.cpp,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example1.cpp,v $
# Revision 1.1  1994/05/26  17:48:47  sridhar
# Initial revision
#
 * Revision 2.8  1993/09/13  01:43:53  keffer
 * No longer uses sleep() --- to unportable.
 *
 * Revision 2.7  1993/08/13  16:59:48  dealys
 * ported to MPW C++ 3.3
 *
 * Revision 2.6  1993/08/07  17:33:01  jims
 * Flush cout before requesting input from cin (MS C8 bug: streams not tied)
 *
 * Revision 2.5  1993/08/06  19:44:44  jims
 * No longer calls sleep from Win16 programs
 *
 * Revision 2.4  1993/04/10  22:25:38  keffer
 * nameOfMonth() changed to monthName().
 *
 * Revision 2.3  1993/03/06  03:32:35  keffer
 * Added <stdlib.h> for Centerline.
 *
 * Revision 2.2  1993/02/09  18:21:43  keffer
 * New <rw/compiler.h> macro names
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:08   KEFFER
 * Tools.h++ V5.1
 * 
 */

// Include the header files for the classes RWDate and RWTime:
#include <rw/rwdate.h>
#include <rw/rwtime.h>
#include <rw/rstream.h>

STARTWRAP
#include <time.h>		/* looking for time() */
ENDWRAP

const int sleepTime = 5;

main()
{

  /**************************************************************/
  /*                       RWDate                               */
  /**************************************************************/
  
  // Construct a RWDate with the current date.
  RWDate d1;

  cout << "Print the date.\n";
  cout << d1 << "\n\n\n";

  cout << "Now print some information about the date:\n";
  cout << "Day of the year:   " << d1.day()        << endl;
  cout << "Day of the month:  " << d1.dayOfMonth() << endl;
  cout << "Name of the month: " << d1.monthName()  << endl;
  cout << "The year:          " << d1.year()       << endl;
  
  cout << "Construct the date 4/12/1842...";
  RWDate d2(12, "April", 1842);
  cout << "done.  Print it out:\n";
  cout << d2 << endl;

  // Was this year a leap year?
  if ( d2.leap() ) 
        cout << "A leap year!\n";
  else
        cout << "Not a leap year.\n";
  
  // Add some days to a RWDate:
  cout << "Add 37 days to this date, then print again:\n";
  d2 += 37;
  cout << d2 << endl;
   
  RWDate inputDate;

  while(1) {
    cout << "Enter a date in one of the following formats:\n";
    cout << "  dd-mmm-yy    (e.g. 10-MAR-86);\n";
    cout << "  mm/dd/yy     (e.g. 3/10/86);\n";
    cout << "  mmm dd, yyyy (e.g. March 10, 1986).\n";
    cout << "Invalid dates will be flagged.\nUse EOF to terminate (^Z on the PC; usually ^D on Unix).\n" << flush;

    cin.clear();		// Clear any corrupt dates
    cin >> inputDate;		// Get the date

    if( cin.eof() ) break;	// Check for EOF

    if( inputDate.isValid() )
      cout << "You entered:\t" << inputDate << endl;
    else {
      cout << "Not a valid date.\n";
    }
  }

  /**************************************************************/
  /*                       RWTime                               */
  /**************************************************************/

  cout << "\nWell, that was fun.  Now let's exercise RWTime.\n\n";
  cout << "Construct a RWTime with the current time...";
  RWTime t1;

  cout << "done.  Now print it out.\n";
  cout << t1 << endl;

  cout << "Convert to a date, then print it out:\n";
  cout << RWDate(t1) << endl;

  cout << "Return some information about the time:\n";
  cout << "The hour is       " << t1.hour()    << endl;
  cout << "The hour (GMT) is " << t1.hourGMT() << endl;
  cout << "The minute is:    " << t1.minute()  << endl;
  cout << "The second is:    " << t1.second()  << endl;

  cout << "Hang on while I go to sleep for a few seconds...";
  cout.flush();

  time_t start, end;
  time(&start);
  do {
    time(&end);
  } while ( end - start < (time_t)sleepTime ) ;

  cout << "back again.\n\nNow construct a new time.\n";

  // Construct new RWTime with current time:
  RWTime t2;

  // Print it out:
  cout << t2 << endl;

  // The following line should not be necessary, but a bug in Sun C++
  // requires it:
  cout.flush();
  return 0;
}

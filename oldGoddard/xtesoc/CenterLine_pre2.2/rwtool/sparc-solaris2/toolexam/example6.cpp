/*
 * Example 6: BTree-on-disk.  Retrieves birthdays, given a name
 *
 * $Id: example6.cpp,v 1.1 1994/05/26 17:48:47 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991, 1992, 1993. 
 * This software is subject to copyright protection under the laws 
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example6.cpp,v $
# Revision 1.1  1994/05/26  17:48:47  sridhar
# Initial revision
#
 * Revision 2.6  1993/08/07  21:40:21  keffer
 * Simplified further.
 *
 * Revision 2.5  1993/08/07  17:33:01  jims
 * Flush cout before requesting input from cin (MS C8 bug: streams not tied)
 *
 * Revision 2.4  1993/07/08  19:46:04  griswolf
 * Simplified by removing backward-compatible code
 *
 * Revision 2.3  1993/05/25  22:42:32  griswolf
 * updated to deal with the latest disktree version
 *
 * Revision 2.2  1993/02/24  22:42:06  griswolf
 * Update to fit version 0x200 RWBTreeOnDisk
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:10   KEFFER
 * Tools.h++ V5.1
 * 
 */

#include <rw/disktree.h>
#include <rw/filemgr.h>
#include <rw/rwdate.h>
#include <rw/cstring.h>
#include <rw/rstream.h>


// example of using a struct for the void* in applyToKeyAndValue:
struct printStruct {
  RWFileManager*	filemgr;
  unsigned	 	keyLen;
};

// Function to print a key and value:
void 
printKey(const char* a, RWstoredValue sv, void* x)
{
  printStruct *ps = (struct printStruct *)x;
  unsigned len = ps->keyLen;
  RWCString key(a,len);		// don\'t count on trailing \0 in key
  RWDate bday;
  RWFileManager* fm = ps->filemgr;
  fm->SeekTo(sv);
  bday.restoreFrom(*fm);
  cout << "Name: " << key << " birthday: " << bday << endl;
}

main()
{
  RWCString	filename;
  RWDate	birthday;
  RWoffset	loc;

  cout << "       RWBTreeOnDisk sample program.\n";
  cout << "       Stores names and birthdays in a BTree.\n";

  cout << "Enter either the name of a new file or of a pre-existing file: " << flush;
  filename.readLine(cin);

  RWFileManager fmgr(filename.data());
  RWBTreeOnDisk bt(fmgr);	// Construct a B Tree with allocations managed by fmgr

  unsigned keyLen = bt.keyLength();

  // display a list of this tree's parameters
  cout << "The RWBTreeOnDisk being managed in file " << filename;
  cout << ", has: " << endl;
  cout << "\tCache blocks:\t"     << bt.cacheCount() << endl;
  cout << "\tVersion:\t0x" << hex << bt.version() << dec << endl;
  cout << "\tKey Length:\t"       << bt.keyLength() << endl;
  cout << "\tOrder:\t\t"          << bt.order() << endl;
  cout << "\tMinimum Order:\t"    << bt.minOrder() << endl;
  cout << "\tHeight:\t\t"         << bt.height() << endl;
  cout << "\tEntries:\t"          << bt.entries() << endl;
  cout << endl;
  
  // Loop to do various things to the tree:

  while (1) {
    cout << "(i)ns (n)umber of entries (f)ind (d)el (l)ist (c)lear e(x)it: ";

    /*
     * Read the character; check for EOF.
     */
    char response;
    if ( !(cin >> response).good() ) break;

    if ( response == 'x' || response == 'X' ) break; // Check for exit

    RWCString	name;
    RWCString	retKey;
    switch ( response ) {

    // Insert a key-value pair.
    case 'i':
    case 'I':

      cout << "Person's name: " << flush; // Prompt
      cin >> ws;			  // Skip any leading whitespace
      name.readLine(cin);		  // Get the person's name.
      name.resize(bt.keyLength());	  // Trim to the size of the B-Tree key.
      cout << "Birthday: " << flush;	  // Prompt for the person's birthday.
      cin >> birthday;			  // Read it in
      if( birthday.isValid() )		  // Check validity
      {
        loc = fmgr.allocate(birthday.binaryStoreSize());
        fmgr.SeekTo(loc);
        birthday.saveOn(fmgr);
        bt.insertKeyAndValue(name.data(), loc);	// Remember the location.
      }
      else
      {
        cin.clear();		// Clear state
        cout << "Bad date.\n";
      }
      break;

    // Find a person's birthday:
    case 'f':
    case 'F':

      cout << "Person's name: " << flush; // Prompt
      cin >> ws;			  // Skip leading whitespace
      name.readLine(cin);		  // Get the person's name, to use as the key.
      name.resize(bt.keyLength());	  // Trim to the size of the B-Tree key.
      // Go find the birthday location.
      if(! bt.findKeyAndValue(name.data(), retKey, loc))
	cout << name << " not found.\n";
      else
      {
	fmgr.SeekTo(loc);
	birthday.restoreFrom(fmgr); // Retrieve the birthday.
	cout << "Birthday is " << birthday << endl;
      }
      break;

    // Delete a person:
    case 'd':
    case 'D':

      cout << "Name of person to delete: " << flush;
      cin >> ws;
      name.readLine(cin);
      name.resize(bt.keyLength());
      if(! bt.removeKeyAndValue(name.data(),loc))
	cout << "Can't find " << name << " to delete" << endl;
      else
	fmgr.deallocate(loc);
      
      break;

    // List the number of entries:
    case 'n':
    case 'N':

      cout << "Number of items in tree: " << bt.entries() << endl;
      break;

    // Clear the tree:
    case 'c':
    case 'C':
      cout << "Clearing..." << flush;
      bt.clear();		// Remove all keys.
      cout << "done.\n" << flush;
      break;

    // Print all entries:
    case 'l':

      struct printStruct ps;
      ps.filemgr = &fmgr;
      ps.keyLen = bt.keyLength();
      
      bt.applyToKeyAndValue(printKey, &ps);
      break;

    default:

      cerr << "Input not recognized...try again\n";

    }  // end switch
  }
  return 0;
}

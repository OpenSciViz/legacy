/*
 * Template definitions for RWTIsvSlist<TL> and RWTIsvSlistIterator<TL>
 *
 * $Id: tislist.cc,v 1.1 1994/05/26 17:46:33 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tislist.cc,v $
// Revision 1.1  1994/05/26  17:46:33  sridhar
// Initial revision
//
 * Revision 1.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.4  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.3  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.2  1993/02/05  23:09:47  keffer
 * Corrected problem in RWTIsvSlist<TL>::remove(RWBoolean (*testFun)(const TL*, void*), void* d);
 *
 * Revision 1.1  1993/01/27  21:38:32  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */

template <class TL> void
RWTIsvSlist<TL>::apply(void (*applyFun)(TL*, void*), void* d)
{
  TL* link = firstLink();
  while (link != tailLink())
  {
    applyFun(link, d);		// Apply the function
    link = (TL*)link->next();	// Advance
  }
}

template <class TL> void
RWTIsvSlist<TL>::clearAndDestroy()
{
  TL* n;
  TL* link = firstLink();
  while (link != tailLink())
  {
    n = (TL*)link->next();
    delete link;
    link = n;
  }
  init();
}
  
template <class TL> TL*
RWTIsvSlist<TL>::find(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      return link;
    link = (TL*)link->next();
  }
  return rwnil;
}

template <class TL> size_t
RWTIsvSlist<TL>::index(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  size_t count = 0;
  const TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      return count;
    link = (const TL*)link->next();
    ++count;
  }
  return RW_NPOS;
}

template <class TL> size_t
RWTIsvSlist<TL>::occurrencesOf(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  size_t count = 0;
  const TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      ++count;
    link = (const TL*)link->next();
  }
  return count;
}

/*
 * Remove and return the first link for which the tester function
 * returns TRUE.
 */
template <class TL> TL*
RWTIsvSlist<TL>::remove(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  RWPRECONDITION(testFun!=rwnil);

  TL* link = headLink();
  while (link != lastLink())
  {
    if (testFun((const TL*)link->next(), d))
      return removeRight(link);
    link = (TL*)link->next();
  }
  return rwnil;
}

/****************************************************************
 *								*
 *	Definitions for RWTIsvSlistIterator<TL>			*
 *								*
 ****************************************************************/

/*
 * Return first occurrence where the tester returns true.
 */
template <class TL> TL*
RWTIsvSlistIterator<TL>::findNext(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  TL* p;
  while ( (p= (*this)()) != 0 )
  {
    if (testFun(p, d))
      return p;
  }
  return rwnil;
}

/*
 * Remove first occurrence where the tester returns true.
 * To get any decent efficiency out of this function with
 * a singly-linked list, we must remember the previous link.
 */
template <class TL> TL*
RWTIsvSlistIterator<TL>::removeNext(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  while (cursor()->next() != container()->tailLink())
  {
    if (testFun((const TL*)cursor()->next(), d))
    {
      return container()->removeRight(cursor());
    }
    advance();
  }
  return rwnil;
}


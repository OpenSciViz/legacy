#ifndef __RWTVASSLNK_H__
#define __RWTVASSLNK_H__

/*
 * RWTValAssocLink: Key / Value association link using values
 * RWTPtrAssocLink: Key / Value association link using pointers
 *
 * $Id: tasslnk.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Class RWTValAssocLink<K,V> defines an association between a key of
 * type K, and a value of type V in a singly-linked link, using value
 * semantics.
 *
 * It has a single value constructor that takes just the key.
 * This means that the value will be constructed using the default
 * constructor for type V.  Usually this works just fine.  However, if the
 * value (type V) is a builtin, then its value will be left undefined.
 * Usually this also works fine.  However, if this is unsatisfactory,
 * then you can supply your own definition that overrides the template-
 * generated definition.  For an explanation of user-specified overrides
 * of template-generated definitions, see Stroustrup II, sec. 8.4.1.
 *
 * Example:
 *
 *   RWTValAssocLink<int,double>::RWTValAssocLink(int i) :
 *     key_(i)
 *    {
 *      value_ = 0.0;	// Explicitly set the value to zero.
 *    }
 *
 * Class RWTPtrAssocLink<K,V> defines an association between a pointer to
 * a key of type K and a pointer to a value of type V in a singly-linked
 * link.
 *
 * It also has a single value constructor which takes just a pointer to
 * a key.  It always sets the value pointer to nil:
 *
 *   RWTPtrAssocLink<int,double>::RWTPtrAssocLink(int* i) :
 *     key_(i),
 *     value_(rwnil)
 *    {
 *    }
 *
 ***************************************************************************
 *
 * $Log: tasslnk.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.7  1993/12/31  00:02:53  jims
 * Left justify preprocessor directives
 *
 * Revision 2.6  1993/11/04  14:06:32  jims
 * Port to ObjectStore
 *
 * Revision 2.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.4  1993/02/17  20:26:07  keffer
 * Added class RWTPtrAssocLink<KP,VP>
 *
 * Revision 2.3  1993/02/17  18:32:03  keffer
 * Now passes T's by const reference, rather than by value
 *
 * Revision 2.2  1993/01/28  21:11:49  keffer
 * Ported to cfront V3.0
 *
 *    Rev 1.0   02 Mar 1992 16:10:52   KEFFER
 * Initial revision.
 */

#ifndef __RWISLIST_H__
# include "rw/islist.h"
#endif

template <class K, class V> struct RWExport RWTValAssocLink : public RWIsvSlink
{
  K		key_;
  V		value_;
  RWTValAssocLink(const K& key);
  RWTValAssocLink(const K& key, const V& value) : key_(key), value_(value) { }

  RWTValAssocLink<K,V>* next() const { return (RWTValAssocLink<K,V>*)next_;}

};

template <class K, class V> struct RWExport RWTPtrAssocLink : public RWIsvSlink
{
  K*		key_;
  V*		value_;
  RWTPtrAssocLink(K* key) : key_(key), value_(rwnil) {;}
  RWTPtrAssocLink(K* key, V* value) : key_(key), value_(value) {;}

  RWTPtrAssocLink<K,V>* next() const { return (RWTPtrAssocLink<K,V>*)next_;}

};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tasslnk.cc"
#endif

#endif	/* __RWTVASSLNK_H__ */

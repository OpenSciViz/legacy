/*
 * Template definitions for RWTValHashSet<T>
 *
 * $Id: tvhset.cc,v 1.1 1994/05/26 17:46:33 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tvhset.cc,v $
// Revision 1.1  1994/05/26  17:46:33  sridhar
// Initial revision
//
 * Revision 1.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.1  1993/06/03  20:52:23  griswolf
 * Initial revision
 *
 *
 */

template <class T> void
RWTValHashSet<T>::insert(const T& val)
{
  insertOnce(val);
}

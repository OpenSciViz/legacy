#ifndef __RWBTRDICT_H__
#define __RWBTRDICT_H__

/*
 * RWBTreeDictionary --- Dictionary implemented as an in memory B-Tree
 *
 * $Id: btrdict.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $#
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 * $Log: btrdict.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.3  1994/03/04  01:04:16  jims
 * Override isEqual member function from RWCollectable to return
 * TRUE or FALSE based on operator==
 *
 * Revision 2.2  1993/09/09  02:38:31  keffer
 * Added copy constructor and assignment operator.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   04 Aug 1992 18:55:48   KEFFER
 * Added "virtual" keyword to destructor for documentation purposes.
 * 
 *    Rev 1.3   22 May 1992 17:04:08   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.2   18 Feb 1992 09:54:08   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:08   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:12:36   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

/*
 * For the storage and retrieval of (key, value) pairs.
 */

#include "rw/btree.h"
#include "rw/colclass.h"

/****************************************************************
 *								*
 *			RWBTreeDictionary			*
 *								*
 ****************************************************************/

class RWExport RWBTreeDictionary : public RWBTree {

  RWDECLARE_COLLECTABLE(RWBTreeDictionary)

public:

  RWBTreeDictionary();
  RWBTreeDictionary(const RWBTreeDictionary&);
  void			operator=(const RWBTreeDictionary&);

  virtual ~RWBTreeDictionary();

  void				applyToKeyAndValue(RWapplyKeyAndValue, void*);
  virtual void			clear();
  virtual void			clearAndDestroy();
  virtual RWCollectable*	find(const RWCollectable* key) const;  // Returns value 
  RWCollectable*		findKeyAndValue(const RWCollectable* key, RWCollectable*& value) const;
  RWCollectable*		findValue(const RWCollectable*) const;	// Returns value
  RWCollectable*		findValue(const RWCollectable*, RWCollectable*) const; // Replace value.
  RWCollectable*		insertKeyAndValue(RWCollectable* key, RWCollectable* val);
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual RWCollectable*	remove(const RWCollectable* key); // Returns value
  virtual void			removeAndDestroy(const RWCollectable*); 
  RWCollectable*		removeKeyAndValue(const RWCollectable* key, RWCollectable*& value);
};

#endif /* __RWBTRDICT_H__ */

#ifndef __RWTIMER_H__
#define __RWTIMER_H__

/*
 * RWTimer: measures elapsed CPU or user time.
 *
 * $Id: timer.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: timer.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.1  1992/10/31  02:20:23  keffer
 * Clarified header comments.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 *    Rev 1.0   11 Mar 1992 14:10:46   KEFFER
 * Initial revision.
 */

#ifndef __RWDEFS_H__
#  include "rw/defs.h"
#endif

class RWExport RWTimer {

  double	startTime_;
  double	stopTime_;
  RWBoolean	isStopped_;

  static double	absoluteTime();

public:
  RWTimer();

  double	elapsedTime() const;
  void		reset();
  void		start();
  void		stop();
};

#endif	/* __RWTIMER_H__ */

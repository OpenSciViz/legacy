#ifndef __RWMODEL_H__
#define __RWMODEL_H__

/*
 * RWModel --- maintains a list of dependent clients
 *
 * $Id: model.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: model.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.1  1992/11/16  04:24:16  keffer
 * Broke out RWModel functionality into RWModel and RWModelClient
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   18 Feb 1992 09:55:34   KEFFER
 * 
 *    Rev 1.0   12 Nov 1991 13:13:18   keffer
 * Initial revision.
 */

#include "rw/ordcltn.h"

class RWExport RWModelClient;

class RWExport RWModel
{
public:
  RWModel();
  void			addDependent(RWModelClient*);
  void			removeDependent(RWModelClient*);
  // Allows peek at dependent list:
  const RWOrdered*	dependents() {return &dependList;}
  virtual void		changed(void* pData = 0);	// Notify all dependents
private:
  RWOrdered		dependList;
};

class RWExport RWModelClient : public RWCollectable
{
public:
  virtual void		updateFrom(RWModel* model, void* pData) = 0;
};

#endif	/* __RWMODEL_H__ */

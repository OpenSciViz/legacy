#ifndef __RWCOLLTIME_H__
#define __RWCOLLTIME_H__

/*
 * RWCollectableTime --- collectable times.
 *
 * $Id: colltime.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: colltime.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.7  1993/11/08  07:53:42  jims
 * Port to ObjectStore
 *
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/03/31  02:54:58  myersn
 * add constructor from RWCString and RWLocale.
 *
 * Revision 2.4  1993/03/31  01:53:08  myersn
 * fold in RWZone & RWLocale arguments supported by base classes RWDate & RWTime
 *
 * Revision 2.3  1993/03/25  05:32:01  myersn
 * eliminate hourTy, monthTy, etc. references.
 *
 * Revision 2.2  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   22 May 1992 17:04:10   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   18 Feb 1992 09:54:16   KEFFER
 * 
 *    Rev 1.2   28 Oct 1991 09:08:12   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.1   09 Oct 1991 18:28:56   keffer
 * Provided RWCollectableTime(const RWTime&) constructor.
 * 
 *    Rev 1.0   28 Jul 1991 08:14:00   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/rwtime.h"
#include "rw/collect.h"
#include "rw/zone.h"

/****************************************************************
 *								*
 *			RWCollectableTime			*
 *								*
 ****************************************************************/

class RWExport RWCollectableTime : public RWCollectable, public RWTime {

  RWDECLARE_COLLECTABLE(RWCollectableTime)

public:

  RWCollectableTime();
  RWCollectableTime(unsigned long s)       : RWTime(s) { }
  RWCollectableTime(const RWTime& t) : RWTime(t) { }
  RWCollectableTime(unsigned h, unsigned m, unsigned s = 0,
		    const RWZone& zone = RWZone::local())
    : RWTime(h, m, s, zone) { }
  RWCollectableTime(const RWDate& d, unsigned h=0, unsigned m=0, unsigned s=0,
		    const RWZone& zone = RWZone::local())
    : RWTime(d, h, m, s, zone)   { }
  RWCollectableTime(const RWDate& d, const RWCString& str,
                    const RWZone& z = RWZone::local(),
                    const RWLocale& l = RWLocale::global())
    : RWTime(d, str, z, l) {}
  RWCollectableTime(const struct tm* tmb, const RWZone& zone = RWZone::local())
    : RWTime(tmb, zone) { }

  /* Virtual functions inherited from RWCollectable */
  virtual RWspace		binaryStoreSize() const {return RWTime::binaryStoreSize();}
  virtual int			compareTo(const RWCollectable*) const;
  virtual unsigned		hash() const;
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;
};

#endif /* __RWCOLLTIME_H__ */

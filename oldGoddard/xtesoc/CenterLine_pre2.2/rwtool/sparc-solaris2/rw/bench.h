#ifndef __RWBENCH_H__
#define __RWBENCH_H__

/*
 * RWBench:  A class to help in the running of benchmarks
 *
 * $Id: bench.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * To use, derive a class from RWBench including a doLoop(unsigned long) 
 * function, and a what() function (if you plan to use report()).  
 * The doLoop(unsigned long N) function should perform N operations of
 * the type you are trying to benchmark.  RWBench will call this function
 * over and over again until "duration" time has elapsed.  Then it will sum
 * the total number of operations performed.
 *
 * To run, construct an object then call go().  Then call report() to get
 * a summary.  You can call ops() outerLoops(), etc. for more detail.
 *
 * If you wish to correct for overhead, then provide an idleLoop() function
 * which does non-benchmark related calculations.
 *
 ***************************************************************************
 *
 * $Log: bench.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   17 Mar 1992 11:31:22   KEFFER
 * 
 *    Rev 1.0   10 Feb 1992 18:54:06   KEFFER
 * Initial revision.
 */

#include "rw/timer.h"

class RWExport RWBench {

public:

  // Default: 5 second test, one inner loop op, no machine name.
  RWBench(double duration = 5, unsigned long ILO=1000, const char* machine = 0);
  virtual void   parse(int argc, char* argv[]);
  virtual void   go();                      // Run the benchmark
  virtual void   doLoop(unsigned long n)=0; // User supplied: should execute the inner loop n times
  virtual void   idleLoop(unsigned long n); // To calculate looping overhead
  virtual void   what(ostream&) const;      // Print out what we're doing --- used by report()
  void           where(ostream&) const;     // Print out the machine type and the compiler
  virtual void   report(ostream&) const;    // Print out #reps and speed
  double         setDuration(double);       // Set test duration to something else
  unsigned long  setInnerLoops(unsigned long);// Set # inner loops to something else
  double         duration() const           {return timeToTest_;}
  unsigned long  innerLoops() const         {return innerLoops_;}
  double         time() const;              // time to execute in seconds
  unsigned long  outerLoops() const;        // Number of times the inner loop was executed
  double         ops() const;               // Number of operations performed
  double         opsRate() const;           // Number of operations per second
  double         kiloOpsRate() const;       // Number of thousands of ops per second
  double         megaOpsRate() const;       // Number of millions of ops per second

private:

  const char*    machine_;       // What machine we're running on
  double         timeToTest_;    // How long should the test take
  unsigned long  innerLoops_;    // Number of inner loop operations to be done
  unsigned long  outerLoops_;    // Number of outer loops actually executed
  double         delta_;         // Actual time (corrected for overhead)
};

#endif	/* __RWBENCH_H__ */

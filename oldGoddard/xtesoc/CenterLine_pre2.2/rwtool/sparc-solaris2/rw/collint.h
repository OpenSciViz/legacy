#ifndef __RWCOLLINT_H__
#define __RWCOLLINT_H__

/*
 * RWCollectableInt --- RWCollectable Integers.
 *
 * $Id: collint.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: collint.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.2  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   22 May 1992 17:04:10   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.2   18 Feb 1992 09:54:14   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:10   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:13:50   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/rwint.h"
#include "rw/collect.h"

/****************************************************************
 *								*
 *			RWCollectableInt			*
 *								*
 ****************************************************************/

class RWExport RWCollectableInt : public RWCollectable, public RWInteger {

  RWDECLARE_COLLECTABLE(RWCollectableInt)

public:

  RWCollectableInt();
  RWCollectableInt(int j) : 	RWInteger(j) { }
  RWCollectableInt(const RWCollectableInt& ci) : RWInteger(ci.value()) { }

  RWBoolean			operator==(const RWCollectableInt& c) const
    {return value()==c.value();}

  virtual RWspace		binaryStoreSize() const {return RWInteger::binaryStoreSize();}
  virtual int			compareTo(const RWCollectable*) const;
  virtual unsigned		hash() const;
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;
};	  

#endif /* __RWCOLLINT_H__ */

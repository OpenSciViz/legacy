#ifndef __RWCOLLASS_H__
#define __RWCOLLASS_H__

/*
 * Declarations for RWCollectable Associations, used in dictionaries.
 *
 * $Id: collass.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: collass.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.4  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.3  1993/07/03  23:55:20  keffer
 * Removed the declaration for the no-longer-needed function rwDestroyAssociation()
 *
 * Revision 2.2  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   25 May 1992 15:47:12   KEFFER
 * 
 *    Rev 1.4   22 May 1992 17:04:08   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   29 Apr 1992 14:51:18   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 *    Rev 1.2   18 Feb 1992 09:54:12   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:10   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:13:26   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

/*
 * Binds a "key" and a "value" together.  The key and value must be RWCollectable.
 */

#include "rw/collect.h"

/****************************************************************
 *								*
 *			RWCollectableAssociation		*
 *								*
 ****************************************************************/

class RWExport RWCollectableAssociation : public RWCollectable {

  RWDECLARE_COLLECTABLE(RWCollectableAssociation)

public:

  RWCollectableAssociation();
  RWCollectableAssociation(RWCollectable* k, RWCollectable* v){ky=k; val=v;}

  RWCollectable*		key()   const			{return ky;}
  RWCollectable*		value() const			{return val;}
  RWCollectable*		value(RWCollectable*);
  void				setValue(RWCollectable* v)	{val=v;}

  /* Inherited from class RWCollectable: */
  virtual RWspace		binaryStoreSize() const;
  virtual int			compareTo(const RWCollectable*) const;
  virtual unsigned		hash() const;
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;

protected:

  RWCollectable*		ky;
  RWCollectable*		val;
};


/****************************************************************
 *								*
 *			RWCollectableIDAssociation		*
 *								*
 ****************************************************************/

class RWExport RWCollectableIDAssociation : public RWCollectableAssociation {

  RWDECLARE_COLLECTABLE(RWCollectableIDAssociation)

public:
   RWCollectableIDAssociation();
   RWCollectableIDAssociation(RWCollectable *k, RWCollectable*v) :
      RWCollectableAssociation(k,v) { }

   /*** Override these methods in order to use addresses, not values ***/

   virtual unsigned		hash() const;
   virtual int			compareTo(const RWCollectable*) const;
   virtual RWBoolean		isEqual(const RWCollectable* c) const;
};
      
#endif /* __RWCOLLASS_H__ */

#ifndef __RWSEQCLTN_H__
#define __RWSEQCLTN_H__

/*
 * Abstract base class for sequenceable collections.
 *
 * $Id: seqcltn.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This is an abstract base class, inherited by collections whose
 * elements are ordered and accessible by an index.
 *
 * $Log: seqcltn.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.4  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.3  1993/01/25  18:12:13  keffer
 * RW_NO_CONST_OVERLOADS->RW_NO_CONST_OVERLOAD
 *
 * Revision 2.1  1992/11/19  05:45:01  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   18 Feb 1992 09:54:44   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:22   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:16:54   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colclass.h"

class RWExport RWSequenceable : public RWCollection {

public:

  /************ Virtual functions inherited from RWCollection *************/
  virtual void			apply(RWapplyCollectable, void*) = 0;
  virtual void			clear() = 0;
//virtual void			clearAndDestroy();
//virtual RWBoolean		contains(const RWCollectable*) const;
  virtual size_t		entries() const = 0;	// Total entries
  virtual RWCollectable*	find(const RWCollectable*) const = 0;		// First occurrence
  virtual RWCollectable*	insert(RWCollectable*) = 0;
  virtual RWBoolean		isEmpty() const = 0;
  virtual size_t		occurrencesOf(const RWCollectable*) const = 0;
  virtual RWCollectable*	remove(const RWCollectable*) = 0;	// Remove first occurrence
//virtual void			removeAndDestroy(const RWCollectable*); 

public:

  /****************  Added virtual functions for RWSequenceables ****************/
  virtual RWCollectable*	append(RWCollectable*) = 0;
  virtual RWCollectable*&	at(size_t) = 0; // Some collections can use as lvalue
#ifndef RW_NO_CONST_OVERLOAD
  virtual const RWCollectable*	at(size_t) const = 0; // Cannot use as lvalue
#endif
  virtual RWCollectable*	first() const = 0;
  virtual size_t		index(const RWCollectable*) const = 0;
  virtual RWCollectable*	insertAt(size_t, RWCollectable*) = 0;
  virtual RWCollectable*	last() const = 0;
  virtual RWCollectable*	prepend(RWCollectable*) = 0;

  // For backwards compatibility:
  virtual RWCollectable*	insertAfter(int, RWCollectable*) = 0;
};

#endif /* __RWSEQCLTN_H__ */

#ifndef __RWINSTMGR_H__
#define __RWINSTMGR_H__

/*
 * Declarations for RWInstanceManager --- manages multi-thread and per-process
 * instance data.
 *
 * $Id: instmgr.h,v 1.1 1994/05/26 17:45:26 sridhar Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1990, 1991, 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: instmgr.h,v $
 * Revision 1.1  1994/05/26  17:45:26  sridhar
 * Initial revision
 *
 * Revision 1.15  1993/11/22  18:21:44  jims
 * Move OS/2 section up where it belongs -- before the class definition
 *
 * Revision 1.14  1993/11/18  01:20:44  jims
 * Move support for OS/2 MT from rwtsd to instmgr
 *
 * Revision 1.13  1993/11/16  08:42:38  myersn
 * add OS/2 support
 *
 * Revision 1.12  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.11  1993/08/05  11:40:24  jims
 * Remove exitProc and GlobalRelease... function
 *
 * Revision 1.10  1993/07/29  06:42:17  jims
 * Change RW_WIN32_API to __WIN32__
 *
 * Revision 1.9  1993/07/09  07:58:05  jims
 * Port to Windows NT
 *
 * Revision 1.8  1993/07/08  00:10:55  jims
 * use rwfar from defs.h instead of #defining FAR
 *
 * Revision 1.7  1993/07/01  18:12:36  jims
 * Remove #include directive for rwwind.h
 *
 * Revision 1.6  1993/06/24  01:39:17  keffer
 * Now includes <rw/defs.h> *before* checking for RW_MULTI_THREAD
 *
 * Revision 1.5  1993/04/12  12:03:21  jims
 * Now supports thread-specific data for multiple threads within on process,
 * continues to support per-task data for Windows 3.x DLLs
 *
 * Revision 1.4  1993/03/25  01:17:42  keffer
 * Now includes "rw/defs.h"
 *
 * Revision 1.3  1993/02/06  02:34:31  keffer
 * Added copyright notice.
 *
 */

#include "rw/defs.h"

#if (defined(__DLL__) && defined(__WIN16__)) || defined(RW_MULTI_THREAD)

#if defined(__DLL__) && defined(__WIN16__)
#  include "rw/rwtsd.h"

#elif defined(sun)		// MT & Sun -- assuming Solaris 2.x/Cafe
#  include <thread.h>
   typedef thread_key_t RWTSDKEY;

#elif defined(__WIN32__)
#  include <windows.h>
   typedef DWORD RWTSDKEY;

#elif defined(__OS2__)
#  if defined(__IBMCPP__)
#    define RWTHREADID *_threadid
#  elif defined(__TURBOC__)
#    define RWTHREADID _threadid
#  endif
#  define INCLDOSPROCESS
#  include <os2.h>
#  define rwfar
#  define rwpascal
   typedef unsigned long RWTSDKEY;

   RWTSDKEY rwfar rwpascal 
   RWGetTaskSpecificKey();

   int rwfar rwpascal 
   RWSetTaskSpecificData(RWTSDKEY hKey, void rwfar*);

   void rwfar* rwfar rwpascal 
   RWGetTaskSpecificData(RWTSDKEY hKey);

   void rwfar* rwfar rwpascal 
   RWReleaseTaskSpecificData(RWTSDKEY hKey);

#else
#  error RW_MULTI_THREAD not supported in this environment
#endif

class RWExport RWInstanceManager
{
public:
  void rwfar*		addValue();       // set instance specific data
  void rwfar*		currentValue();	  // get instance specific data
  void 	                freeValue();      // release key and discard data

  // Values to be provided and discarded by the specializing class:
  virtual void rwfar*	newValue() = 0;
  virtual void          deleteValue(void rwfar*) = 0;

  RWInstanceManager();

private:
  RWTSDKEY              tsd_key;
};

#else	/* neither 16-bit Windodws DLL nor MultiThread */

#error RWInstanceManager class not meant to be used outside DLL or MT situation

#endif

#endif	/* __RWINSTMGR_H__ */

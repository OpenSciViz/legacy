@Topic@Default
@Objects@Application Window@OI_app_window
@Objects@Box@OI_box
@Objects@Dialog Box@OI_dialog_box
@Objects@Entry Field@OI_entry_field
@Objects@Gauge@OI_gauge
@Objects@Glyph@OI_glyph
@Objects@Menu@OI_menu
@Objects@Panner@OI_panner
@Objects@Scroll Bar@OI_scroll_bar
@Objects@Scrollable Objects@scroll_obj
@Objects@Slider@OI_slider
@Objects@Text Objects@text_obj
@Miscellaneous@Mouse Operation@mouse_ops
@Miscellaneous@Cut and Paste@cut_paste
@Miscellaneous@Keyboard Input@kb_input
             Object Interface Operation

The "Objects" menu above lists the types of
objects which may be used to make up an
application.  To find out more about how to
manipulate a particular object, press the left
mouse button on the Objects menu cell, position
the pointer over the desired object name in the
menu, and release the mouse button.
@Topic@OI_app_window
@Components@Title Bar@title_bar
@Components@Interior Box@OI_box
@Components@Help Line@help_line
@Related Objects@Box@OI_box
An application window is used as the enclosure 
for an application.  The appearance of an 
application window may vary, depending on the 
window manager you are using.  If you use the 
Solbourne window manager (swm) in Motif mode, 
an application window looks like this:

@OI_Bitmap@$OI_LIB/OI/Mappwin.bm

An application window consists of the following
elements:
  Title Bar (supplied by the window manager)
  Interior Box
  Help Line
To learn how a particular component can be
manipulated, select the item from the "Components"
menu above.
@Topic@OI_entry_field
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Entry Field Input Bindings@entry_field_input
@Related Objects@Sequenced Entry Field@OI_seq_entry_field
An entry field is a space for accepting user
keyboard input.  It looks like this:

@OI_Compound_Object@$OI_LIB/OI/entfld.cf
When the focus is in an entry field, your keyboard
input will show up in the entry field.  If your
focus model is click to type you must position
the mouse pointer within the entry filed and click
the left mouse button to get focus.

Entry fields support cut and paste operations.

Entry field objects can support input operations
by clicking a mouse button once, twice, or three
times with the pointer positioned over them.  A
single click will always reposition the text entry
point at the cursor location.
By default, no other action will happen when this
is done.  However, a particular application may
choose to perform some action.
@Topic@OI_seq_entry_field
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Seq Entry Field Input Bindings@seq_entry_field_input
@Related Objects@Entry Field@OI_entry_field
A sequenced entry field is an entry field with two
additional buttons to the right of the data entry
line.  It looks like this:

@OI_Compound_Object@$OI_LIB/OI/sqentfld.cf
The buttons are used to step the value by
increasing or decreasing.  The sequenced item need
not be numeric; the default sequenced entry field
uses integers, but an application may supply
functions to step through any quantity, not just
numeric ones.

@Topic@OI_scroll_bar
@Sub Topics@Scroll Bar Input Bindings@scroll_bar_input
@Related Objects@Panner@OI_panner
@Related Objects@Scrollable Objects@scroll_obj
A scroll_bar is an object used to manipulate the
view of another object.  Scroll bars  are 
typically used to scroll text in a text window or
change the portion of a drawing which is visible 
in a window.  A scroll_bar looks like this:

@OI_Compound_Object@$OI_LIB/OI/scrlbar.cf
A scroll_bar is manipulated using the left and
middle mouse button; in addition accelerator keys
may be used as described in the "Accelerators"
section.

A scroll_bar consists of the following parts:
  Begin/End Arrow Buttons
    One at each end of the scroll bar.  Used
    to move the controlled object by units
    of one.
  Channel
    Represents the total span of the controlled 
    object.
  Handle
    Rectangle within the channel, its size
    represents the portion of the controlled
    object spanned by the viewport.

Clicking the left or middle mouse button when
the pointer is over an arrow button moves the
handle, and the object it controls, one unit in
the direction the arrow points.

Clicking the left mouse button when the pointer 
is over the channel moves the handle, and the 
object it controls, one viewport towards the 
pointer.

Pressing the left or middle mouse button on the
handle allows one to drag it to any location on
the channel. The object in the viewport also
moves.

Pressing the middle mouse button when the pointer
is over the channel positions the handle at the
location of the mouse.

@Topic@OI_slider
@Sub Topics@Slider Input Bindings@slider_input
@Related Objects@Gauge@OI_gauge
A slider is a long narrow object used adjust some
quantity.  It looks like this:

@OI_Compound_Object@$OI_LIB/OI/slider.cf
A slider consists of the following parts:
    Channel
      Area where the handle moves.
    Handle
      Rectangle on top of bar, its location
      represents the current value.

Pressing the left mouse button when the pointer 
is over the handle allows one to drag the handle 
to any desired position, until the button is 
released.

Clicking the left mouse button when the pointer 
is on the channel will move the handle one unit in
the direction of the mouse pointer.

Clicking the right mouse button when the pointer
is on the channel moves the handle to the pointer
location.
@Topic@OI_gauge
@Related Objects@Slider@OI_slider
A gauge is a long narrow object used to display
some quantity as a fraction of another quantity.
It looks like this:

@OI_Compound_Object@$OI_LIB/OI/gauge.cf
A gauge is an output only object; you cannot
change it.
@Topic@OI_glyph
A glyph is a bitmap graphic which may be displayed
anywhere.  It is normally for display purposes
only, but a particular application may be
sensitive to button presses/clicks when the
pointer is over the glyph.  A sample glyph is
shown below:

@OI_Compound_Object@$OI_LIB/OI/glyph.cf
Some of the pictures of objects in this help text
are glyphs.
@Topic@OI_box
@Related Objects@Application Window@OI_app_window
@Related Objects@Dialog Box@OI_dialog_box
@Related Objects@Scroll Box@OI_scroll_box
A box is simply a rectangle in which other objects
may be placed, or where graphics may be drawn by
an application.  The basic rectangle of an
application window is a box.
A box looks like this:
@OI_Compound_Object@$OI_LIB/OI/box.cf

What happens in a box is dependent on the
particular application.  By default, a box ignores
all forms of input.
@Topic@OI_dialog_box
@Components@Button Menu@OI_button_menu
@Components@Box@OI_box
@Components@Pushpin@Pushpin
@Related Objects@Command Dialog Box@OI_command_dialog_box
@Related Objects@File Dialog Box@OI_file_dialog_box
@Related Objects@Message Dialog Box@OI_ms_dialog_box
@Related Objects@Prompt Dialog Box@OI_prompt_dialog_box
@Related Objects@Select Dialog Box@OI_select_dialog_box
A dialog box is a simple box with a button menu
centered at the bottom.  It is usually used to
enclose a set of data entry fields and menus for
you to fill in, or to display some information.
In normal usage, when you are finished with it 
you click the left mouse button on one of 
the buttons at the bottom.

The farthest right button in the button menu at
the bottom may look like this:

@OI_Compound_Object@$OI_LIB/OI/Mpin_out.cf

This is a pushpin button.
For more information on pushpin operation,
select "Pushpin" from the "Components" menu.

A sample dialog box appears below:

@OI_Compound_Object@$OI_LIB/OI/dlgbox.cf
@Topic@Pushpin
@Related Objects@Dialog Box@OI_dialog_box
A dialog box may have an additional button at
the far right in the button menu at the bottom
of the dialog box.  If so, it will look like
this:

@OI_Compound_Object@$OI_LIB/OI/Mpin_out.cf

This is a "pushpin" button.  Clicking on this
button effectively "pins up" the dialog box so
you may use it more than once without having 
it appear and disappear from the screen.  When
this is done, the button image will change to
this:

@OI_Compound_Object@$OI_LIB/OI/Mpin_in.cf

When you are through with the dialog box, clicking
on the button again will unpin the dialog box.

@Topic@Prompt Dialog Box@OI_prompt_dialog_box
@Components@Entry Field@OI_entry_field
@Components@Button Menu@OI_button_menu
@Components@Box@OI_box
@Related Objects@Dialog Box@OI_dialog_box
A prompt dialog box is a dialog box with an entry
field. A prompt dialog box looks like this:
@OI_Compound_Object@$OI_LIB/OI/pmptdlg.cf
You can type a response in this entry field
and select one of the buttons on the button menu
so the program can act on the text string entered.
On clicking mouse button on any dialog box buttons
(except the default Help cell) the dialog box
would come down.
@Topic@Select Dialog Box@OI_select_dialog_box
@Components@Scroll Menu@OI_scroll_menu
@Components@Entry Field@OI_entry_field
@Components@Button Menu@OI_button_menu
@Components@Box@OI_box
@Related Objects@Dialog Box@OI_dialog_box
A select dialog box is a dialog box containing a
scroll menu and an entry field. A select dialog
box looks like this:
@OI_Compound_Object@$OI_LIB/OI/seldlg.cf
You can type a selection into the entry field or
make a selection by clicking on a menu cell in the
scroll menu. Double clicking on a menu cell is
equivalent to clicking on the cell then selecting
the "OK" button of the dialog box.
@Topic@Command Dialog Box@OI_command_dialog_box
@Components@Scroll Menu@OI_scroll_menu
@Components@Entry Field@OI_entry_field
@Components@Box@OI_box
@Related Objects@Dialog Box@OI_dialog_box
A command dialog box is a dialog box with a scroll
menu and an entry field. A command dialog box
looks like this:
@OI_Compound_Object@$OI_LIB/OI/cmddlg.cf
The scroll menu is a history log of each command
that is entered on the "Command Line".  A previous
command can be invoked by double clicking the left
mouse button on a command in the scroll menu.  A
new command can be executed by typing the command
text into the entry field and hitting the Return
key.
@Topic@File Dialog Box@OI_file_dialog_box
@Components@Scroll Menu@OI_scroll_menu
@Components@Entry Field@OI_entry_field
@Components@Button Menu@OI_button_menu
@Components@Box@OI_box
@Related Objects@Dialog Box@OI_dialog_box
A file dialog box is a dialog box with two scroll
menus and two entry fields. It allows the user 
to browse through the file system and select a 
particular file.  
!A file dialog box looks like this:
!@OI_Compound_Object@$OI_LIB/OI/filedlg.cf
The top entry field shows the file filter used 
for the search criteria. It accepts C-Shell type 
regular expressions.  Hitting the return key in
this entry field is equivalent to pressing the
"Filter" button in the button menu.  Pressing the 
"Filter" button applies the "File Filter" value 
and displays the files matching the filter.

The scroll menu on the left shows sub-directories 
under the current directory.  Double clicking on 
a directory in this menu searchs for files in this
directory and updates both scroll menus with new 
files and directories found.

The menu on the right shows the files found in the
selected directory. A single click selects the 
file and a double click selects and makes the 
callback associated with "OK" button. 


@Topic@Message Dialog Box@OI_ms_dialog_box
@Components@Glyph@OI_glyph
@Components@Button Menu@OI_button_menu
@Components@Box@OI_box
@Related Objects@Dialog Box@OI_dialog_box
There are six different types of message dialog
boxes. Each is a dialog box containing a message; 
some have a glyph indicating the type of message.

Following are different message dialog boxes with
their default glyphs:

1. Error dialog box. An error dialog box looks
like this:
@OI_Compound_Object@$OI_LIB/OI/errdlg.cf

2. Info dialog box. An info dialog box looks
like this:
@OI_Compound_Object@$OI_LIB/OI/infodlg.cf

3. Message dialog box. A message dialog box
looks like this:
@OI_Compound_Object@$OI_LIB/OI/msgdlg.cf

4. Question dialog box. A question dialog box
looks like this:
@OI_Compound_Object@$OI_LIB/OI/quesdlg.cf

5. Warning dialog box. A warning dialog box
looks like this:
@OI_Compound_Object@$OI_LIB/OI/warndlg.cf

6. Working dialog box. A working dialog box
looks like this:
@OI_Compound_Object@$OI_LIB/OI/workdlg.cf

@Topic@Text Objects@text_obj
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Input Bindings@kb_input
@Related Objects@Static Text@OI_static_text
@Related Objects@Multi Text@OI_multi_text
@Related Objects@Scroll Text@OI_scroll_text
The Solbourne object interface supports several
different kinds of text objects.  These are:

Static Text
  Text which may be displayed only; it may only
  occupy a single line.

Multi Text
  Text consisting of more than one line.  The
  entire text may be visible at once, or only a
  portion, controlled by the application.

Scroll Text
  A multi text object with attached controllers to
  allow the user to scroll through the text.

To find out more about a specific text object, 
select the object from the "Related Objects" menu.
@Topic@OI_static_text
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Static Text Input Bindings@static_text_input
@Related Objects@Multi Text@OI_multi_text
@Related Objects@Scroll Text@OI_scroll_text
A static text object is a short text string
displayable on one line. Static text looks like
this:
@OI_Compound_Object@$OI_LIB/OI/stattxt.cf

Static text objects are output only as far as text
is concerned; the value displayed may be changed
only by the underlying application.  You may copy
the text from a static text object using the mouse
copy operation.

Static text objects can support input operations
by clicking a mouse button once, twice, or three
times with the pointer positioned over them.  By
default, no action will happen when this is done.
However, a particular application may choose to
perform some action.
@Topic@OI_multi_text
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Multi Text Input Bindings@multi_text_input
@Related Objects@Scroll Text@OI_scroll_text
@Related Objects@Static Text@OI_static_text
A multi text object supports display and entry of
text extending over more than one line.  A multi
text looks like this:

@OI_Compound_Object@$OI_LIB/OI/mlttxt.cf

Unless disabled by the application, one may type
into a multi text object.

The text from a multi text object may be
manipulated using the mouse cut and paste
operations as well.

Multi text objects can support input operations by
clicking a mouse button once, twice, or three
times with the pointer positioned over them.  By
default, no action will happen when this is done.
However, a particular application may choose to
perform some action.
@Topic@OI_scroll_text
@Components@Scroll Bar@OI_scroll_bar
@Components@Panner@OI_panner
@Components@Multi Text@OI_multi_text
@Sub Topics@Cut and Paste@cut_paste
@Sub Topics@Scroll Text Input Bindings@scroll_text_input
@Related Objects@Multi Text@OI_multi_text
@Related Objects@Static Text@OI_static_text
@Related Objects@Scrollable Objects@scroll_obj
A scroll text object is a multi text object
with scroll bars or a panner attached to it.
A scroll text looks like this:
@OI_Compound_Object@$OI_LIB/OI/scrltxt.cf

Scroll bars may be either horizontal, vertical,
or both; and they may be present at either side
(but not both) or above or below (but not both)
the text.  The text to be viewed typically cannot
all be seen at once; the scroll bars or panner
allow you to position the text in the viewport.
For example, help text longer than 10 lines
appears in a scroll text object with a scroll bar
on the left.
@Topic@OI_scroll_box
@Components@Scroll Bar@OI_scroll_bar
@Components@Panner@OI_panner
@Components@Box@OI_box
@Related Objects@Scrollable Objects@scroll_obj
A scroll box object consists of a rectangular box,
known as the viewport, with scroll bars or a
panner attached to it.
A scroll box looks like this:
@OI_Compound_Object@$OI_LIB/OI/scrlbox.cf
The viewport is a window into an underlying box,
which is normally larger than the viewport.
The scroll bars or panner allow you to position
the underlying box in the viewport.  Select
"Panner" or "Scroll Bar" from the "Components"
menu above for information on manipulating the
scroll box controllers.
@Topic@OI_menu
@Related Objects@Button Menu@OI_button_menu
@Related Objects@Exclusive Menu@OI_excl_menu
@Related Objects@Non-Exclusive Menu@OI_poly_menu
@Related Objects@Pop-up Menu@popup_menu
@Related Objects@Sub Menu@sub_menu
@Related Objects@Abbreviated Menu@OI_abbr_menu
@Related Objects@Scroll Menu@OI_scroll_menu
Menus come in several forms.  They may be titled
or untitled, horizontal or vertical.

Menus are of three basic types:
  Button
  Exclusive
  Non-Exclusive
Each of these types is described in greater
detail in its own subsection, selectable from
the "Related Objects" menu above.

If the menu is visible you choose something from
it as follows:
  Position pointer over the menu cell of interest
  Press the left mouse button
  The menu cell will highlight
  You may move the pointer to a different cell,
    changing the choice
  Release the mouse button
The action associated with the menu cell will be
triggered when the button is released.  Moving the
pointer completely off the menu before releasing 
the button will ensure that no cell is selected.

Pop-up menus are invisible.  They are used 
the same way as regular menus, except they are 
triggered with the right mouse button, and do not
appear until the right mouse button is pressed.
@Topic@OI_button_menu
@Related Objects@Pop-up Menu@popup_menu
@Related Objects@Sub Menu@sub_menu
@Related Objects@Abbreviated Menu@OI_abbr_menu
@Related Objects@Scroll Menu@OI_scroll_menu
Button menus look like this:

@OI_Compound_Object@$OI_LIB/OI/btnmnu.cf
Button menus allow only one item to be chosen at a
time, and the choice is not remembered once the
menu cell has completed its task.

You choose something from it as follows:
  Position pointer over the menu cell of interest
  Press the left mouse button
  The menu cell will highlight
  You may move the pointer to a different cell,
    changing the choice
  Release the mouse button

The action associated with the menu cell will be
triggered when the mouse button is released.
Moving the pointer completely off the menu before
releasing the button will ensure nothing is 
chosen.
@Topic@OI_excl_menu
@Related Objects@Pop-up Menu@popup_menu
@Related Objects@Sub Menu@sub_menu
@Related Objects@Scroll Menu@OI_scroll_menu
Exclusive Menus look like this:

@OI_Compound_Object@$OI_LIB/OI/exchkmnu.cf

Exclusive Menus allow only one item to be chosen
at a time, the choice is remembered until
another item is selected or the current one is
deselected.

You choose something from it as follows:
  Position pointer over the menu cell of interest
  Press the left mouse button
  The menu cell will highlight
  You may move the pointer to a different cell,
    changing the choice
  Release the mouse button

If any action is associated with the menu cell, it
will be triggered both when the item is selected
and deselected.  Moving the pointer completely off
the menu before releasing the button will ensure
the selection is not changed and no action is
triggered.  However, no action is normally 
associated with the cells of a Exclusive Menu.
Instead, the menu is queried at a later point in 
time to determine which cell is set.
@Topic@OI_poly_menu
@Related Objects@Pop-up Menu@popup_menu
@Related Objects@Sub Menu@sub_menu
@Related Objects@Scroll Menu@OI_scroll_menu
Non-Exclusive Menus look like this:

@OI_Compound_Object@$OI_LIB/OI/pyrctmnu.cf

Non-Exclusive Menus allow more than one item to be
chosen at the same time; the choices are
remembered until the menu is changed.

You select or deselect an item as follows:
  Position pointer over the menu cell of interest
  Press the left mouse button
  The menu cell will highlight or unhighlight
  You may move the pointer to a different cell,
    changing the choice
  Release the mouse button

If an action is associated with the menu cell, it
will be triggered both when an item is selected
and deselected.  Moving the pointer completely off
the menu before releasing the button will ensure 
the menu state is unchanged.  However, no action 
is normally associated with the cells of a 
Non-Exclusive Menu; the menu is usually queried 
at a later time to see what values are set.
@Topic@Pop-up Menu@popup_menu
@Components@Button Menu@OI_button_menu
@Components@Exclusive Menu@OI_excl_menu
@Components@Non-Exclusive Menu@OI_poly_menu
@Related Objects@Menu@OI_menu
A pop-up menu is normally not visible.  It is
active only in an application defined region, such
as inside an application window or inside a glyph.

Pop-up menus are really just one of the other 
menu forms with a slightly different means of
activating the menu.  There is a pop-up menu in 
the box below.  Press the right mouse button
to activate it.

@OI_Compound_Object@$OI_LIB/OI/popup.cf
You choose something from it as follows:
  Position pointer over the object where the 
    pop-up is active
  Press the right mouse button
  The menu will appear
  Select your choice(s)
  Release the mouse button
  The menu will disappear

Selecting from a pop-up menu depends on the 
underlying menu type.  Select one of the "menu" 
buttons from the "Components" menu above to get 
help on menu types.  
Moving the pointer completely off the menu before 
releasing the button will ensure that no cell is 
selected.
@Topic@Sub Menu@sub_menu
@Components@Button Menu@OI_button_menu
@Components@Exclusive Menu@OI_excl_menu
@Components@Non-Exclusive Menu@OI_poly_menu
@Related Objects@Menu@OI_menu
Sub Menus look like this:

@OI_Compound_Object@$OI_LIB/OI/submnu.cf
Sub Menus are a means of implementing a hierarchy
of menus.  Each cell of a menu may have another 
menu associated with it.  When you press the right
mouse button over the menu cell and move the 
pointer to the right, the menu associated with 
that cell appears, allowing you to choose 
something from the additional menu.  Menus may be
cascaded like this to an arbitrary depth.

You choose something from a sub menu as follows:
  Position pointer over the menu cell of interest
  Press the left mouse button
  The menu cell will highlight and the sub menu 
    will appear
  Move the pointer into the sub menu
  Select your choice(s)
  Release the mouse button

Selecting from a sub menu depends on the 
underlying menu type.  Select one of the "menu" 
buttons from the "Components" menu above to get 
help on menu types.  
Moving the pointer completely off the menu before 
releasing the button will ensure that no cell is 
selected.
@Topic@OI_abbr_menu
@Components@Button Menu@OI_button_menu
@Components@Exclusive Menu@OI_excl_menu
@Related Objects@Menu@OI_menu
An abbreviated menu is a compact form for another
menu.  They look like this:

@OI_Compound_Object@$OI_LIB/OI/abrmnu.cf
Instead of displaying all possible choices for the
menu, a "Default" choice is displayed as a button
with a smaller button to the right of the choice.
If the menu has a title it is displayed to the 
left of the button. 

To activate a cell on the menu press the left 
mouse button; the full menu will display as a 
pop-up.
You can then select any cell by placing the 
pointer on the cell and releasing the mouse
button.  This selection becomes the default
selection.  To keep the default selection
simply release the left mouse button without
moving the pointer.

Moving the pointer completely off the menu before
releasing the mouse button ensures no action is 
taken.
@Topic@OI_scroll_menu
@Components@Scroll Bar@OI_scroll_bar
@Components@Button Menu@OI_button_menu
@Components@Exclusive Menu@OI_excl_menu
@Components@Non-Exclusive Menu@OI_poly_menu
@Related Objects@Menu@OI_menu
@Related Objects@Scrollable Objects@scroll_obj
A scroll menu consists of a menu, partially
visible through a rectangular viewport, with a
scroll bar attached to it. A sample of an
exclusive scroll menu appears below:

@OI_Compound_Object@$OI_LIB/OI/scrlmnu.cf

The scroll bar allows you to position the
underlying menu in the viewport. The underlying
menu may be any menu type.

Selecting from a scroll menu depends on the 
underlying menu type.  Select one of the "menu" 
buttons from the "Components" menu above to get 
help on menu types.  
@Topic@OI_panner
@Related Objects@Scroll Bar@OI_scroll_bar
@Related Objects@Scrollable Objects@scroll_obj
A panner is a two dimensional controller used by
scrollable objects.  It consists of a rectangle
representing the entire object being scrolled,
with an interior rectangle representing the
viewport through which the object is being viewed.
A panner looks like this:
@OI_Compound_Object@$OI_LIB/OI/panner.cf

To move the viewport:
  Position the pointer in the interior rectangle
  Press the left mouse button
  Drag the panner viewport 
    The related object tracks the relative
    position of the panner viewport
  Release the mouse button
@Topic@Scrollable Objects@scroll_obj
@Components@Scroll Bar@OI_scroll_bar
@Related Objects@Scroll Box@OI_scroll_box
@Related Objects@Scroll Menu@OI_scroll_menu
@Related Objects@Scroll Text@OI_scroll_text
@Related Objects@Panner@OI_panner
The Solbourne object interface supports several
different kinds of scrollable objects.  These are:

Scroll Box
  A box with scroll bars.

Scroll Menu
  A menu with scroll bars.
	
Scroll Text
  A multi text object with attached controllers to
  allow the user to scroll through the text.

To find out more about a particular type of 
scroll object, select the object from the 
"Related Objects" menu.
@Topic@Mouse Operation@mouse_ops
@Sub Topics@Cut and Paste@cut_paste
@Related Objects@Text Objects@text_obj
@Related Objects@Pop-up Menu@popup_menu
Applications built using the Solbourne Object
Interface and running under the OpenLook 
interaction model should function in a similar
manner as far as the mouse is concerned.  Most
operations are performed using the left mouse
button.  Exceptions are text pasting, which uses
the middle button, and activating pop-up menus,
which uses the right mouse button.  Throughout
this text, wherever "Left Mouse Button" is
mentioned, one should assume the appropriate
translation if the button assignments have been
changed.

At present, it is not possible to change the
button assignment; this feature will be available
in the future.
@Topic@Cut and Paste@cut_paste
@Sub Topics@Mouse Operation@mouse_ops
@Related Objects@Entry Field@OI_entry_field
@Related Objects@Text Objects@text_obj
The Solbourne Object Interface supports cut and
paste operations using the mouse.  "Cut" is really
a misnomer; it is really a non-destructive copy
operation.

To save a particular piece of text:
  Position the pointer over the first character
    you wish to save
  Press the left mouse button
  Drag the mouse over the text you wish to save
  Release the mouse button

The text being saved will be highlighted while you
are moving the mouse.

Objects which support text copy are:
  Entry Field
  Sequenced Entry Field
  Multi Text
  Scroll Text
  Static Text

To insert the saved text, simply position the
pointer in the object you wish to insert the text
into, and click the middle mouse button.  The text
will be inserted at the current entry position for
that object.

Objects which accept text input are:
    Entry Field
    Sequenced Entry Field
    Multi Text
    Scroll Text
@Topic@Keyboard Input@kb_input
@Related Objects@Entry Field@OI_entry_field
@Related Objects@Text Objects@text_obj
Any of the OI objects which support keyboard input
use a set of keyboard mappings. Look under
appropriate text object's input bindings to find
out what are the key mappings for that object.

@Topic@Title Bar@title_bar
@Related Objects@Application Window@OI_app_window
The title bar, if present, is supplied by the
window manager.  It usually contains a menu box, a
title, and some means of resizing and moving the
window.  It may contain other items as well.  The
Solbourne Window Manager title bar looks like this
when running in Motif mode:

@OI_Bitmap@$OI_LIB/OI/Mttlbar.bm
The angular brackets in the upper left and upper
right corners are resize corners.  Pressing the
left mouse button in a resize corner and 
dragging the mouse will allow you to resize the
application.

In some cases, the application may restrict the
allowable sizes for its window; the final size you
get may be different from the one outlined when
you release the mouse button.

The box on the left is a marker for a pulldown
button menu.  Pressing the left mouse button on 
it will bring up a button menu from which you may 
choose one of the following operations:

  Restore
    Restore the application to the original size. 
  Move
    Move the application.
  Size
    Change the application size.
  Minimize
    Collapse the application into an icon; the
    application may be deiconified using a popup
    menu over the icon.
  Maximize
    Enlarge the application to take up the entire 
    display.
  Lower
    Move the application to the back of the
    display.
  Close
    Exit the application.

The box in the right corner of the title bar is
a maximize button.  Select this glyph with the
left mouse button to toggle the application
between the full size of the display and the
original size.

The box just to the left of the maximize button is
the minimize button.  The application is iconified
by pressing the left mouse button on this box.

Pressing the left mouse button anywhere else will
allow you to drag the entire application to a new
location on the display; release the mouse button
when it is where you want it.
@Topic@Help Line@help_line
@Components@Button Menu@OI_button_menu
@Related Objects@Application Window@OI_app_window
The bottom of an application window contains a
single line which is used to display error
messages and other short helpful information.  If
the application window does not have a main menu
then on the right side of this line is a small
button labeled "help".  As you have already
discovered, this is a menu button to activate
help! If there is a main menu then the help
button is to the extreme right side of the
main menu and not on this line.

If you are using the Solbourne Window Manager, the
lower left and right corners contain resize
corners.  Pressing the left mouse button in these
allows you to resize the window by dragging the
mouse.

In some cases, the application may restrict the
allowable sizes for its window; the final size you
get may be different from the one outlined when
you release the mouse button.

@Topic@Entry Field Input Bindings@entry_field_input
@Related Objects@Entry Field@OI_entry_field
@Related Objects@Text Objects@text_obj
Entry field supports following key and mouse
bindings.
Abbreviations used:
  S  => Shift
  C  => Control
  M  => Meta

         <Key>Up:	previous_object
         <Key>Down:	next_object
         <Key>Linefeed:	replace_with_default
       M <Key>asciitilde:toggle_mode
       M <Key>I:	insert_mode
       M <Key>R:	replace_mode
    C    <Key>BackSpace:
		delete_to_beginning_of_line
         <Key>BackSpace: delete_previous_character
 S     M <Key>Delete:	cut_primary
 S    ~M <Key>Delete:	cut_clipboard
    C    <Key>Delete:	delete_to_end_of_line
         <Key>Delete:	delete_next_character
 S  C    <Key>Left:	backward_word
    C    <Key>Left:	backward_word
 S       <Key>Left:	backward_character
         <Key>Left:	backward_character
 S  C    <Key>Right:	forward_word
    C    <Key>Right:	forward_word
 S       <Key>Right:	forward_character
         <Key>Right:	forward_character
 S       <Key>R7:	beginning_of_line
         <Key>R7:	beginning_of_line
 S       <Key>R13:	end_of_line
         <Key>R13:	end_of_line
    C    <Key>R9:	backward_view
    C    <Key>R15:	forward_view
 S  C    <Key>Tab:	previous_tab_group
 S       <Key>Tab:	previous_tab_group
    C    <Key>Tab:	next_tab_group
         <Key>Tab:	next_tab_group
         <Key>F4:	activate_popup
         <Key>F10:	activate_main_menu
       M <Key>L6:	copy_primary
         <Key>L6:	copy_clipboard
         <Key>L8:	paste_clipboard
       M <Key>L10:	cut_primary
         <Key>L10:	cut_clipboard
    C  M <Key>Insert:	copy_primary
    C    <Key>Insert:	copy_clipboard
 S       <Key>Insert:	paste_clipboard
    C  M <Key>KP_0:	copy_primary
    C    <Key>KP_0:	copy_clipboard
 S       <Key>KP_0:	paste_clipboard
    C    <Key>slash:	select_all
    C    <Key>backslash: unselect_all
   !C    <Key>space:	set_anchor
!S       <Key>space:	key_select
         <Key>Escape:	cancel_select
         <Key>Return:	newline
   !C    <Btn1Down>:	move_insertion
!S       <Btn1Down>:	extend_start
~S ~C  M <Btn2Down>:	secondary_start
~S       <Btn2Down>:	secondary_start
~S ~C  M <Btn2Motion>:	secondary_adjust
~S       <Btn2Motion>:	secondary_adjust
~S ~C  M <Btn2Up>:	move_selection
~S       <Btn2Up>:	secondary_end
         <Btn3Down>:	click_down
         <Btn3Up>:	click_up
~S ~C ~M <Btn1Down>:	take_focus
   ~C    <Btn1Motion>:	select_adjust
         <Btn1Up>:	select_end
         <Key>:		input_character
@Topic@Multi Text Input Bindings@multi_text_input
@Related Objects@Multi Text@OI_multi_text
@Related Objects@Text Objects@text_obj
Multi text supports following key and mouse
bindings.
Abbreviations used:
  S  => Shift
  C  => Control
  M  => Meta

       M <Key>asciitilde:toggle_mode
       M <Key>I:	insert_mode
       M <Key>R:	replace_mode
       M <Key>D:	delete_this_line
 S     M <Key>O:	open_previous_line
       M <Key>O:	open_next_line
    C ~M <Key>BackSpace:
		delete_to_beginning_of_line
      ~M <Key>BackSpace: delete_previous_character
 S     M <Key>Delete:	cut_primary
 S    ~M <Key>Delete:	cut_clipboard
    C ~M <Key>Delete:	delete_to_end_of_line
      ~M <Key>Delete:	delete_next_character
 S  C    <Key>Left:	backward_word(extend)
    C    <Key>Left:	backward_word
 S       <Key>Left:	backward_character(extend)
         <Key>Left:	backward_character
 S  C    <Key>Right:	forward_word(extend)
    C    <Key>Right:	forward_word
 S       <Key>Right:	forward_character(extend)
         <Key>Right:	forward_character
 S  C    <Key>Down:	forward_paragraph(extend)
    C    <Key>Down:	forward_paragraph
 S       <Key>Down:	next_line(extend)
         <Key>Down:	next_line
 S  C    <Key>Up:	backward_paragraph(extend)
    C    <Key>Up:	backward_paragraph
 S       <Key>Up:	previous_line(extend)
         <Key>Up:	previous_line
 S  C    <Key>R7:	beginning_of_file(extend)
    C    <Key>R7:	beginning_of_file
 S       <Key>R7:	beginning_of_line(extend)
         <Key>R7:	beginning_of_line
 S  C    <Key>R13:	end_of_file(extend)
    C    <Key>R13:	end_of_file
 S       <Key>R13:	end_of_line(extend)
         <Key>R13:	end_of_line
    C    <Key>R9:	scroll_left
 S       <Key>R9:	scroll_up(extend)
         <Key>R9:	scroll_up
    C    <Key>R15:	scroll_right
 S       <Key>R15:	scroll_down(extend)
         <Key>R15:	scroll_down
         <Key>Return:	newline
         <Key>Linefeed:	newline
 S  C    <Key>Tab:	previous_tab_group
 S       <Key>Tab:	previous_tab_group
    C    <Key>Tab:	next_tab_group
         <Key>Tab:	input_character
         <Key>F10:	activate_main_menu
         <Key>F4:	activate_popup
       M <Key>L6:	copy_primary
         <Key>L6:	copy_clipboard
         <Key>L8:	paste_clipboard
       M <Key>L10:	cut_primary
         <Key>L10:	cut_clipboard
    C  M <Key>Insert:	copy_primary
    C    <Key>Insert:	copy_clipboard
 S       <Key>Insert:	paste_clipboard
    C  M <Key>KP_0:	copy_primary
    C    <Key>KP_0:	copy_clipboard
 S       <Key>KP_0:	paste_clipboard
    C    <Key>slash:	select_all
    C    <Key>backslash:	unselect_all
   !C    <Key>space:	set_anchor
!S       <Key>space:	key_select
         <Key>Escape:	cancel_select
   !C    <Btn1Down>:	move_insertion
!S       <Btn1Down>:	extend_start
~S ~C  M <Btn2Down>:	secondary_start(SECONDARY)
~S       <Btn2Down>:	secondary_start(SECONDARY)
~S ~C  M <Btn2Motion>: secondary_adjust(SECONDARY)
~S       <Btn2Motion>: secondary_adjust(SECONDARY)
~S ~C  M <Btn2Up>:	move_selection
~S       <Btn2Up>:	secondary_end(SECONDARY)
         <Btn3Down>:	click_down
         <Btn3Up>:	click_up
         <Btn1Down>:	take_focus
         <Btn1Motion>:	select_adjust
         <Btn1Up>:	select_end
         <Key>:		input_character
@Topic@Seq Entry Field Input Bindings@seq_entry_field_input
@Related Objects@Seq. Entry Field@OI_seq_entry_field
@Related Objects@Entry Field Input@entry_field_input
@Related Objects@Text Objects@text_obj
Sequential Entry field supports all the key and
mouse bindings of an entry field. In additions it
also supports following key bindings.

         <Key>Up:        increment
         <Key>Down:      decrement
@Topic@Scroll Text Input Bindings@scroll_text_input
@Related Objects@Multi Text@OI_multi_text
@Related Objects@Multi Text Input@multi_text_input
@Related Objects@Text Objects@text_obj
Scroll text supports all the key and mouse
bindings of a multi text object.

@Topic@Scroll Bar Input Bindings@scroll_bar_input
@Related Objects@Scroll Bar@OI_scroll_bar
Scroll bar supports following key and mouse
bindings.
Abbreviations used:
  S  => Shift
  C  => Control
  M  => Meta

~S ~C ~M <Key>Up:	increment_up
~S ~C ~M <Key>Down:	increment_down
~S ~C ~M <Key>Left:	increment_left
~S ~C ~M <Key>Right:	increment_right
~S  C ~M <Key>Up:	page_up
~S  C ~M <Key>Down:	page_down
~S  C ~M <Key>Left:	page_left
~S  C ~M <Key>Right:	page_right
~S ~C ~M <Key>R9:	page_up
~S  C ~M <Key>R9:	page_left
~S ~C ~M <Key>R15:	page_down
~S  C ~M <Key>R15:	page_right
~S ~C ~M <Key>R7:	top_or_bottom(LEFT)
~S  C ~M <Key>R7:	top_or_bottom(LEFT)
~S ~C ~M <Key>Home:	top_or_bottom(LEFT)
~S  C ~M <Key>Home:	top_or_bottom(LEFT)

~S ~C ~M <Key>R13:	top_or_bottom(RIGHT)
~S  C ~M <Key>R13:	top_or_bottom(RIGHT)
~S ~C ~M <Key>End:	top_or_bottom(RIGHT)
~S  C ~M <Key>End:	top_or_bottom(RIGHT)
~S ~C ~M <Btn1Down>:	select
~S  C ~M <Btn1Down>:	top_or_bottom
~S ~C ~M <Btn1Motion>:	moved
~S ~C ~M <Btn1Up>:	release
~S  C ~M <Btn1Up>:	release
~S ~C ~M <Btn2Down>:	select
~S ~C ~M <Btn2Motion>:	moved
~S ~C ~M <Btn2Up>:	release

@Topic@Slider Input Bindings@slider_input
@Related Objects@Slider@OI_slider
Slider supports following key and mouse
bindings.
Abbreviations used:
  S  => Shift
  C  => Control
  M  => Meta

~S  C ~M <Btn1Down>:	top_or_bottom
~S  C ~M <Btn1Up>:	release
~S  C ~M <Key>Up:	increment_up(4)
~S  C ~M <Key>Down:	increment_down(4)
~S  C ~M <Key>Left:	increment_left(4)
~S  C ~M <Key>Right:	increment_right(4)
~S ~C ~M <Key>R7:	top_or_bottom(LEFT)
~S  C ~M <Key>R7:	top_or_bottom(LEFT)
~S ~C ~M <Key>Home:	top_or_bottom(LEFT)
~S  C ~M <Key>Home:	top_or_bottom(LEFT)
~S ~C ~M <Key>R13:	top_or_bottom(RIGHT)
~S  C ~M <Key>R13:	top_or_bottom(RIGHT)
~S ~C ~M <Key>End:	top_or_bottom(RIGHT)
~S  C ~M <Key>End:	top_or_bottom(RIGHT)
~S ~C ~M <Btn1Down>:	select
~S ~C ~M <Btn1Up>:	release
~S ~C ~M <Btn1Motion>:	moved
~S ~C ~M <Btn2Down>:	select
~S ~C ~M <Btn2Up>:	release
~S ~C ~M <Btn2Motion>:	moved
~S ~C ~M <Key>Up:	increment_up
~S ~C ~M <Key>Down:	increment_down
~S ~C ~M <Key>Left:	increment_left
~S ~C ~M <Key>Right:	increment_right

@Topic@Static Text Input Bindings@static_text_input
@Related Objects@Static Text@OI_static_text
Static text supports following key and mouse
bindings.
Abbreviations used:
  S  => Shift
  C  => Control
  M  => Meta

   <Btn1Down>:	select_start
   <Btn1Motion>:select_adjust
   <Btn1Up>:	select_end
   <Key>Escape:	cancel_select
!S <Btn1Down>:	extend_start
~S <Btn2Down>:	secondary_start(SECONDARY)
~S <Btn2Motion>:secondary_adjust(SECONDARY)
~S <Btn2Up>:	secondary_end(SECONDARY)
   <Btn3Down>:	click_down
   <Btn3Up>:	click_up

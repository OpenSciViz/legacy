SUN SYSTEM REQUIREMENTS

     For ObjectCenter's memory, swap space, and disk space requirements, 
     please see the Release Bulletin that accompanies this release.
     
   Supported platforms
     ObjectCenter Version 2.1 supports  
     
     o	Sun-4/SPARCstations running SunOS 4.1.1, 4.1.2 or 4.1.3 
     	(Solaris 1.0) or Solaris 2.2 or 2.3.
	
     o	SPARC 10 workstations running SunOS 4.1.3 or Solaris 2.2 or 2.3.
	
     The process debugging mode (pdm) used in this version of ObjectCenter 
     is based on GNU gdb, Version 4.12. 
	
     To license its software, this version of ObjectCenter uses FLEXlm, 
     Version 2.40c.
	
  Supported compilers 
     ObjectCenter supports the following compilers on Solaris 1 and Solaris 2:

     o	CenterLine-C compiler (clcc) 

     o	Sun K&R C compiler, Version 1.0 and 2.0.1 (cc)

     o	Sun ANSI C compiler, Version 1.0 and 2.0.1 (acc)

     o	ObjectCenter's C++ compiler (CC)

     ObjectCenter supports the following compilers but with limitations to 
     browsing and source level debugging. CenterLine-C and 
     ObjectCenter's C++ compiler are link compatible with them. 

     o	GNU C compiler, Version 2.3.3 (gcc) 

     o	FORTRAN

     o	Sun C++ compiler, Version 3.0.1 (CC)

     This version of ObjectCenter supports Release 3.0.2.15 of the USL C++ 
     Language System, which is backward compatible with Release 2.x and 
     3.0.x. We support everything in the  USL  C++ release, except that we 
     do not support or supply the task library (libtask.a). 
     
     In most cases, code that compiled without warning in USL  C++ 
     Release 2.x or 3.0.x will compile correctly in Release 3.0.2.15. 
     However, you may run across some cases that cause problems. For 
     instance, you may find that some cases of switch statements fail to 
     compile, complaining about jumping past an initializer; the fix here 
     is to supply curly braces ({}) around the body of the case statement. 
     For best results, put the break statement, if any, outside the closing 
     curly brace. 
     
     You may also encounter an "unresolved symbol" problem at link time 
     if you have link symbols that contain nested types, including nested 
     enums. USL  C++ Release 2.x, 3.0.x, and 3.0.2.15 encode the names of 
     such symbols differently. The fix is to recompile the problem modules 
     with cfront 3.0.2.15. Fortunately, this situation is uncommon.
     

  Supported windowing systems 
     ObjectCenter supports both the Motif and OPEN LOOK windowing systems. 
     OPEN LOOK is the default on the Sun platform. You can choose Motif at 
     startup with the -motif switch on the objectcenter command line.



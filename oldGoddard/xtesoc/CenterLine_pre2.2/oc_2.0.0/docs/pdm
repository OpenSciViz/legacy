NAME
     pdm

     process debugging mode; used for debugging an executable file,  
     a corefile, or a running process

Purpose of pdm
     Using ObjectCenter's pdm, or process  debugging  mode,  allows
     you  to  examine what is going on in a program while it exe-
     cutes. You can use process mode to debug an executable  file
     (a.out) along with a corefile or a running process.  A core-
     file contains a literal copy of the contents  of  memory  at
     the time that the operating system aborted a program.

     You can use pdm to do the following:

     o    Start your program under varying conditions that  might
          affect its behavior

     o    Stop your program on specified conditions

     o    See what has happened when your program has stopped

     o    Try out solutions to problems you discover

     Note that you cannot use  pdm  for  automatic  load-time  or
     run-time  error  checking;  see  the  debugging entry for an
     overview of these other  forms  of  debugging  supported  by
     ObjectCenter.

Invoking pdm
     There are several different ways to invoke ObjectCenter's pro-
     cess  debugging  mode,  depending  on whether or not you are
     already in the ObjectCenter environment.

  Outside the ObjectCenter environment
     If you are not already in the  ObjectCenter  environment,  you
     can  start  ObjectCenter  in  process  mode  by using the -pdm
     switch on the shell command line:

     $ objectcenter -pdm

     Alternatively, if you want to use the Ascii version of ObjectCenter
     and start in process debugging mode, you can use the following
     command:

     $ pdm

     Once you are in process debugging mode, you can  invoke  the  debugger
     using the debug command:

     (pdm) 1 -> debug my.a.out

     See the debug entry for more information.

  Within the ObjectCenter environment
     If you are already in an ObjectCenter session using the Motif or
     OPEN LOOK version, you can switch to process debugging mode
     by selecting the Restart Session menu choice on the ObjectCenter
     pulldown menu. A dialog box allows you to restart the environment
     in either component debugging mode or process debugging mode.

     Whenever you switch to process debugging mode from within the
     ObjectCenter environment, ObjectCenter initializes an ObjectCenter
     session using the standard startup file (.pdminit); it does not
     transfer any information to the new ObjectCenter session from the
     previous one. For instance, you lose all loaded files, linked libraries,
     and so on.

     As previously mentioned, once you are in process debugging mode,
     you can invoke the debugger using the debug command:
  
     (pdm) 1 -> debug my.a.out

     NOTE    If you are using Ascii ObjectCenter and you
             wish to switch to process debugging mode,
             you must start a new session from outside the
             environment.


Using pdm vs. cdm
     You can use most ObjectCenter commands the same  way,  whether
     or  not you are in debugging process mode, but there are a few differ-
     ences.

     NOTE:   When you are in process debugging mode , you  can-
             not  use  the  Project  Browser, the Class
             Examiner, the Inheritance Browser,  or  the
             Cross-Reference Browser. Also,  in  pdm,
             ObjectCenter does not support most options, so the
             Options Browser is not available.


  Commands

     See Table 21 for a list of ObjectCenter commands supported by
     process debugging mode along with a description of any differences
     between the way each command works in component or process
     debugging mode. The shaded areas of the table indicate commands
     that are not available in process debugging mode. See the reference
     page for each command for more details about the command.



     Table 21 Differences in ObjectCenter Commands by Mode
     ---------------------------------------------------

     ObjectCenter    Yes if Available   Differences between
     Command         in Process Mode    Component Mode (cdm)
                                        and Process Mode (pdm)
                                        Use of Command


     action                             Not implemented in pdm. Use when.

     alias           Yes                You can use alias [name[ text] ] in
                                        pdm, but you cannot use the following
                                        form in pdm:
                                        alias name text alias_args.

     assign          Yes                No difference.

     attach          Yes                Available in pdm only.

     browse_base                        Not implemented in pdm.

     browse_class                       Not implemented in pdm.

     browse_data_members                Not implemented in pdm.

     browse_derived                     Not implemented in pdm.

     browse_friends                     Not implemented in pdm.

     browse_member_functions            Not implemented in pdm.

     build           Yes                In pdm, ObjectCenter reloads a.out if
                                        a.out is newer than the current a.out.

     catch           Yes                No difference.

     cd              Yes                No difference.

     classinfo                          Not implemented in pdm.

     config_c_parser                    Does not apply to pdm.

     construct                          Not implemented in pdm.

     cont            Yes                The pdm , but not cdm, version of the
                                        cont command supports the following
                                        syntax:

                                        cont at line     Continue at location
                                                         specified by line

                                        cont at line sig signum
                                                         Continue with last
                                                         signal encountered

                                        cont sig signum  Continue with signal
                                                         specified by signum

                                        cont skip count  Continue, ignoring
                                                         breakpoint for count
                                                         iterations

                                        The pdm version of the cont command
                                        does not support the following syntax:

                                        cont continuation_value

     contents        Yes                The pdm version of the contents command
                                        returns the pathname of the a.out file
                                        currently loaded. The contents filename
                                        variation may return only a partial list
                                        of objects declared or defined in
                                        filename.

     cxxmode                            Not implemented in pdm.

     debug           Yes                Available in pdm only.

     delete          Yes                Available in pdm:     delete n
                                                              delete all

                                        Not available in pdm: delete
                                                              delete file:line

     destruct                           Not implemented in pdm.

     detach          Yes                Available in pdm only.

     display         Yes                No difference.

     down            Yes                No difference.

     dump            Yes                No difference.

     edit            Yes                No difference.

     email           Yes                No difference.

     english                            Not implemented in pdm.

     expand                             Not implemented in pdm.

     fg                                 Not implemented in pdm.

     file            Yes                No difference.

     gdb             Yes                Available in pdm only.

     gdb_mode        Yes                Available in pdm only.

     help            Yes                No difference.

     history                            Not implemented in pdm.

     ignore          Yes                No difference.

     info                               Not implemented in pdm.

     instrument                         Does not apply to pdm.

     keybind                            Not implemented in pdm.

     link                               Does not apply to pdm.

     list            Yes                No difference.

     list_classes                       Not implemented in pdm.

     listi           Yes                Available in pdm only.

     load                               Does not apply to pdm. Use debug.

     make            Yes                Using the ObjectCenter syntax in
                                        makefiles has no effect in pdm.

     man             Yes                No difference.

     next            Yes                No difference.

     nexti           Yes                Available in pdm only.

     print           Yes                ObjectCenter uses different formats in
                                        cdm and pdm for displaying the value of
                                        an expression or variable.

     printenv        Yes                No difference.

     printopt                           Not implemented in pdm.

     proto                              Does not apply to pdm.

     quit            Yes                In pdm you do not have the choice of
                                        saving to a project file.

     reinit                             Does not apply to pdm.

     rename                             Does not apply to pdm.

     rerun           Yes                No difference.

     reset           Yes                No difference.

     run             Yes                No difference.

     save                               Does not apply to pdm.

     set             Yes                No difference.

     setenv          Yes                No difference.

     setopt                             Not implemented in pdm.

     sh              Yes                No difference.

     shell           Yes                No difference.

     source          Yes                No difference.

     start                              Does not apply to pdm

     status          Yes                No difference.

     step            Yes                No difference.

     stepi           Yes                Available in pdm.

     stepout         Yes                No difference.

     stop            Yes                See the ObjectCenter Platform Guide for
                                        information about setting breakpoints
                                        in shared libraries while in pdm.

     stopi           Yes                Available in pdm only.

     suppress                           Does not apply to pdm.

     suspend                            Does not apply to pdm.

     swap                               Does not apply to pdm.

     touch                              Does not apply to pdm.

     trace                              Not implemented in pdm.

     unalias         Yes                No difference.

     uninstrument                       Does not apply to pdm.

     unload                             Does not apply to pdm.

     unres                              Does not apply to pdm.

     unsetenv        Yes                No difference.

     unsetopt                           Not implemented in pdm.

     unsuppress                         Does not apply to pdm.

     up              Yes                No difference.

     use             Yes                No difference.

     whatis          Yes                No difference.

     when            Yes                Available in pdm only.

     where           Yes                No difference.

     whereami        Yes                No difference.

     whereis         Yes                No difference.

     xref                               Not implemented in pdm.


  Using gdb in process debugging mode

     As a convenience, ObjectCenter allows you to use gdb commands in
     the Workspace when you are in process debugging mode; gdb is the
     GNU source-level debugger provided by the Free Software
     Foundation.

     If you wish to use gdb, you can do any of the following:
  
     o  Invoke ObjectCenter at the shell prompt with the -gdb
        command-line switch in addition to the pdm switch:

        $ objectcenter -pdm arg1 ... -gdb argn ...

        If you do so, all command-line arguments after the -gdb are
        taken to be gdb command-line switches, and any switches
        before the -gdb are taken to be pdm switches.

     o  Issue the gdb command in the Workspace while you are in
        process debugging mode; the gdb command takes as its
        argument any gdb command:

        (pdm) 1 -> gdb break 20

     o  Issue the gdb_mode command in the ObjectCenter Workspace
        while you are in process debugging mode:
  
        (pdm) 1 -> gdb_mode

        Once you are in gdb mode, you can use only the gdb command
        set. You can get back to process debugging mode by typing the
        following command:

        (gdb) pdm

        NOTE    Although we provide access to native gdb
        commands as a convenience, we do not
        provide any technical support for gdb.

SEE ALSO

       attach, commands, debug, detach

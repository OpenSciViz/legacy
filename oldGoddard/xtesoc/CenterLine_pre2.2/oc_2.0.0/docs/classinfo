NAME	
     classinfo

     displays information about classes

    ----------------    
    |  cdm  | pdm  |
    +-------+------+
    |  yes  | no   |
    ----------------  

SYNOPSIS
     classinfo
     classinfo class_name

DESCRIPTION
     << none >>		Motif and OPEN LOOK: Displays         
     			the Class Examiner.                  
     			                                     
     			Ascii ObjectCenter: Sends information
     			about all loaded classes to the      
     			Workspace.                           
     
     class_name 	Motif and OPEN LOOK: Displays        
     			the Class Examiner with the specified
     			class.                               
     			                                     
     			Ascii ObjectCenter: Sends information
     			about the specified class to the     
     			Workspace.                           
USAGE
     Use classinfo to graphically display information about a
     class in the Class Examiner, when you are using the Motif or
     OPEN LOOK version of ObjectCenter. In Ascii ObjectCenter,
     classinfo sends class information to the Workspace.

OPTIONS
     The following option affects the classinfo command:
     
     show_inheritance	Displays class members with the full    
     			inheritance path showing how members are
     			inherited. If unset, shows a truncated  
     			inheritance path giving only the        
     			defining class.                         
     
     See the options entry for more details about each option.
     ObjectCenter does not support these options in process
     debugging mode (pdm).

SEE ALSO
     browse_base, browse_class, browse_data_members,
     browse_derived, browse_friends, browse_member_functions,
     list_classes

You can debug an executable (by itself, with a corefile, or a
running process) or debug source and object code.

With ObjectCenter, you can set breakpoints or conditional
breakpoints (actions) before or during runs of your program.

When execution stops (because you entered Control-C, or
ObjectCenter encountered a breakpoint, a run-time error,
or a signal), you are at a break level, and you can step through
your code entering functions, execute the next line without
entering functions, or continue execution.

From a break level, you can launch the Data Browser for a
graphical representation of data structures and values, including
complex data structures such as arrays.  Some of the other
information about the program available from a break level are:
the value of a variable or expression, locations of a name
declaration, the uses of a name, and a list of all local variables.

For all the information available at a break level and for more
information about debugging, see the hardcopy ObjectCenter
User's Guide:

o   Chapter 5 - Component debugging, for information about
    debugging source and object code

o   Chapter 6 - Process debugging, for information about
    debugging executables

See Also
--------
debug	    for information about debugging executables
debugging   for general information about debugging
dump	    for information about displaying local variables
run	    for information about executing main()
stop	    for information about setting a breakpoint
where	    for information about displaying the execution stack


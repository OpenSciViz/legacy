In component debugging mode, you can enter any C or C++statement
in the Workspace and ObjectCenter executes it and checks
it for run-time errors. You can use this feature to test code
fragments, data structures, incomplete programs, or individual
functions, and then integrate the new code into your program.

For example, to define a function in the Workspace that adds
two integers, enter

        C++ -> int add (int x, int y)
        C++ +> { return x + y; }
        C++ -> add (3, 4);
        (int) 7

You must terminate C++ and C statements with a semicolon
in the Workspace just as you would in a file.

The C++ and C statements in the Workspace can span several
lines. To continue the statement on the next line, press Return
at an appropriate place in the statement and continue entering
the rest of the statement.

When the Workspace expects additional input for the statement,
the input prompt changes from -> to +>.

Chapter 5 (Component Debugging) of the hardcopy ObjectCenter
User's Guide discusses interactive prototyping.

See Also
---------
Workspace	for information about using the Workspace


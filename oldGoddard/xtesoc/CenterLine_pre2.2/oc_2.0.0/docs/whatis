NAME
     whatis

     lists all uses of a name

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------



SYNOPSIS
     whatis name

DESCRIPTION
     name 
     Displays all uses of the specified name as a  function,
     variable, class/struct/union  tag  name, enumerator, type defini-
     tion, or macro definition.

USAGE

     Use the whatis command to display all uses of an identifier name.
     An identifier name is a name for a function, variable, enumerator,
     class/struct/union tag name, type definition, or macro definition.

     ObjectCenter first displays all uses of the name within scope at the
     current scope location, followed by all uses of the name not within
     scope. The order of the listing represents the order in which the
     specified name is resolved when it is used.

EXAMPLE

     In the following example, the name test is used as both a variable
     and a macro.

      -> int test;
      -> #define test 100
      -> whatis test
      #define test 100
      extern int test; /* initialized */
      -> int test2=2*test;
      -> test2;
      (int) 200

     NOTE    Because you cannot declare or define variables
             in the Workspace in pdm, the preceding
             example works only in cdm.

SEE ALSO
     dump, display, english,  expand,  help,  list,  man,  print,
     whereis, xref

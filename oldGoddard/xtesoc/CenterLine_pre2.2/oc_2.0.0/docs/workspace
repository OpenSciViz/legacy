NAME
     Workspace

     All versions of ObjectCenter provide an interactive work area,
     called the Workspace, that handles ObjectCenter commands and C++
     statements. See the User's Guide for basic information about
     the Workspace.

     Here we describe more advanced ObjectCenter features that help
     you  enter  input into the Workspace. We cover the following
     topics:

     o     Saving a transcript of your session

     o     Displaying your input history

     o     Saving your input history

     o     Repeating previous input

     o     Expanding variables in the Workspace

     o     Using shell meta-characters and operators

     o     Line editing

     o     Using name completion

     o     Redirecting output

     o     Specifying a variable's location

     o     Changing and listing directories

     o     Entering C++ code in the Workspace

     o     Unloading the Workspace scratchpad

     o     Clearing the Workspace

     o	   Using the edit workspace command 

Saving a transcript of your session
     At any point during a ObjectCenter session,  you  can  save  a
     transcript  of  your  Workspace actions in a file. If you do
     so, the transcript contains all of your input as well as  of
     ObjectCenter's output.

     To save a transcript, open the Workspace  pop-up  menu,  and
     select Save to.  You can then either select the default name,
     which is ~/ocenter.script or specify a different  name  by  
     selecting Other file.

Displaying your input history
     Use ObjectCenter's history command to display previous input:

     -> int i;
     -> double d;
     -> char c;
     -> history
     1: int i;
     2: double d;
     3: char c;
     4: history

     If you are using the Motif or OPEN  LOOK  version,  you  can
     also  display  your previous input in the Workspace by using
     the mouse-based scrollbar on the left side of the  Workspace
     panel.

     The Workspace features a history mechanism modeled after the
     csh  and  tcsh  shells.  Similar to the tcsh shell, previous
     lines of input can be scanned one line at a time by entering
     Control-p to scan backward and Control-n to scan forward.

     -> int i;
     -> double d;
     -> char c;
     -> <Ctrl-p>< Ctrl-p> expands to...
     -> double d;

     You can also type a letter or letters, then press  Control-p
     or  Control-n  to scan through command lines that began with
     those  letters.  For  example,  typing  load  then  pressing
     Control-p  repeatedly  scans  all  the  previous  lines that
     loaded files.

Saving your input history
     ObjectCenter saves all Workspace input in a temporary  logfile
     that it deletes at the end of a session. You can specify the
     name of the logfile with the logfile option.

     You can tell ObjectCenter to keep a permanent logfile by using
     the  -f  command-line  switch  when starting ObjectCenter. For
     more information, see the "Switches" section  of  the  object-
     center entry.

     If you did not use the -f switch when  starting  ObjectCenter,
     you  can still save the contents of the logfile at any point
     by redirecting the output of the history command:

     -> history #> ocenter_log_name

     The  logfile  records  input  only;   it   does   not   show
     ObjectCenter's  output.  If  you are using Motif or OPEN LOOK,
     you can record output in a file  as  well,  by  opening  the
     Workspace pop-up menu and selecting Save to.

     You should never edit or modify the logfile  during  a  ses-
     sion;  ObjectCenter uses it just for recording input. Changing
     the logfile does not affect the state of the  Workspace.  If
     you  want to modify the Workspace along with the logfile, it
     is best to copy the logfile to another file, edit it, unload
     the  Workspace, and load the edited copy of the logfile with
     the source command. See the source entry for  more  informa-
     tion.

Repeating previous input
     As in the csh shell, you can execute previous lines of input
     using   a   history   character  followed  by  an  argument.
     ObjectCenter's history character is # (the csh shell's history
     character is !).

  Repeating the most recent line of input
     You can repeat the most recent line of input by entering  ##
     <return  >.   Alternatively,  you  can enter ## < space > to
     edit the line before it is re- entered:

     -> (123 + 456);
     (int) 579
     -> ## <space> expands to...
     -> (123 + 456);
     (int) 579

  Revising the most recent line of input
     Entering #^ old_string^new_string  redoes  the  most  recent
     line  of  input,  with the string new_string substituted for
     the string old_string:

     -> (123 + 456);
     (int) 579
     -> #^123^333 < space > expands to...
     -> (333 + 456);
     (int) 789
     ->

  Repeating a particular line of input
     You can repeat any line of input using the notation #  text,
     where  text  matches  the  beginning  of  a previous line of
     input. You can also repeat a previous line  of  input  using
     the notation #n, where n is the number of the input line to
     be redone, or #-n, where n is the nth previous command.

     For example:

     -> (123 + 456);
     (int) 579
     -> (321 + 654);
     (int) 975
     ->
     -> #( < space > expands to...
     -> (321 + 654);
     (int) 975
     ->
     -> #1 < space > expands to...
     -> (123 + 456);
     (int) 579
     ->
     -> #-3 < space > expands to...
     -> (321 + 654);
     (int) 975
      ->

  Expanding particular tokens of previous input
     You can expand selected  tokens  of  the  previous  line  of
     input, similar to using the csh shell's history commands !$,
     !*, and !:.  See Table 30 for the syntax.

     Table 30 Syntax for Expansion of Tokens in Workspace Input
     --------------------------------------------------------------------------
     Symbol               Expansion
     --------------------------------------------------------------------------
     #$<space>            Last token of the previous line of input

     #*<space>            All but the first token of the previous line of input

     #: number<space>     The number token on the previous line (tokens are
                          numbered starting at 0)
     --------------------------------------------------------------------------



     Here is an example:

     -> int i, j, k;
     -> i = 123 + 456;
     (int) 579
     -> j #* < space > expands to...
     -> j = 123 + 456;
     (int) 579
     -> k = #:4 < space > expands to...
     -> k = 456 ;
     (int) 456

Expanding variables in the Workspace
     You can expand the value  of  any  environment  variable  or
     ObjectCenter  option  in any Workspace command by using the #$
     syntax shown in Table 31.

     Table 31 Syntax for Expansion of Environment Variables and
     Options in Workspace Commands
     ------------------------------------------------------------------------------
     Symbol               Expansion
     ------------------------------------------------------------------------------
     #$  identifier       Substitutes the value of the ObjectCenter option, if one
                          exists, named identifier; otherwise, substitutes the value
                          of the named environment variable. For example,
                          #$path substitutes the value of the ObjectCenter path
                          option, if it is set; #$HOME substitutes the current
                          value of the HOME environment variable.

     #$(environ_var)      Substitutes the value of the named environment
                          variable. For example, including #$(HOME) substitutes
                          the current value of the HOME environment variable.
                          Note that text must be enclosed in parentheses ().

     #${option}           Substitutes the named ObjectCenter option value. For
                          example, #${load_flags} substitutes the loading flags
                          that you have set in ObjectCenter. Note that text must be
                          enclosed in braces {}.
     ------------------------------------------------------------------------------



     In the following examples, #$HOME is expanded to  the  value
     of the HOME environment variable.

     -> printenv HOME
     HOME=/s/users/jk
     -> load #$HOME/sample
     Loading: /s/users/jk/sample

     In  the  following  example,  a  directory   is   added   to
     ObjectCenter's  search  path  by  expanding  #${path}  to  the
     current value of ObjectCenter's path option,  then  specifying
     the directory to add to the path.

     -> printopt path
     path /s3/bobh/src
     -> setopt path #${path} /s3/bobh/obj
     -> printopt path
     path /s3/bobh/src /s3/bobh/obj

  Viewing the expansion before the command is executed
     You can see what the arguments expand to before executing  a
     command by pressing the Spacebar instead of pressing Return.
     Using shell meta-characte rs and operators

     The following  commands  support  the  shell  operators  and
     meta-character expansion supported by /bin/sh:

     o    cd

     o    ls

     o    load

     o    make

     o    sh

     o    shell

     All commands except the unload command attempt to match  the
     expanded  wildcard  name  to  the list of files on the disk.
     The  unload  command  tries  to  match  against  the  loaded
     filenames.

     For example, you can issue  the  following  command  in  the
     Workspace:

     -> load *.c
     Loading: bad_ptr.c
     Loading: clean_att.c
     Loading: cleanfile.c
     Loading: copyfile.c

Line editing
     The Workspace supports line editing of input similar to  the
     line  editing  available  in the emacs editor.  See Table 32
     for the most frequently used  line-editing  commands.   Note
     that not all keyboards have arrow keys.

     Table 32 Frequently Used Line-Editing Commands in ObjectCenter
     ---------------------------------------------------------------
     Key               Action
     ---------------------------------------------------------------
     Control-a         Moves the cursor to the beginning of the line

     Control-e         Moves the cursor to the end of the line

     Control-f         Moves the cursor forward one character

     Control-b         Moves the cursor backward one character

     Control-d         Deletes the character under the cursor

     Up arrow          Scrolls backward through input

     Down arrow        Scrolls forward through input

     Right arrow       Moves cursor forward one character

     Left arrow        Moves cursor backward one character
     ---------------------------------------------------------------

     For a complete list of the  default  keyboard  bindings  for
     ObjectCenter, see the keybind entry .

Using name completion
     The Workspace provides name completion for commands,  names,
     and filename patterns.

     You complete commands and names by pressing the  Escape  key
     twice  without  entering text.  ObjectCenter handles name com-
     pletion as follows:

     o If ObjectCenter cannot complete  the  name,  it  sounds  the
       bell.

     o If the completion is ambiguous, the unambiguous portion is
       completed  and all possible matches are listed.  For exam-
       ple:

     -> int ABC, VAR_1, VAR_2;
     ->
     -> A<ESC><ESC> completes to...
     -> ABC
     +>
      ;
     (int) 0
     -> V<ESC><ESC> ambiguous, completes to...
     VAR_1 VAR_2
     -> VAR_

     You complete filename  patterns  by  entering  the  sequence
     Esc-x.  This  sequence  echoes the current input line to the
     shell specified by ObjectCenter's subshell option; the default
     shell  is  /bin/sh. The subshell echoes the line, performing
     all filename pattern expansions in the process.   For  exam-
     ple:

     -> ls
     a.C b.C c.C
     -> load *.C<ESC>x expands to...
     -> load a.C b.C c.C

Redirecting output
     Just as you can redirect output of commands  at  the  shell,
     you  can  redirect  the  output of most ObjectCenter Workspace
     commands with the following symbols:.

     ------------------------------------------------------------------------------
     Symbol     Result
     ------------------------------------------------------------------------------
     #> file    Redirects the command output to file. Overwrites file if it exists.

     #>> file   Appends the command output to the contents of file.
     ------------------------------------------------------------------------------

     Note that the ObjectCenter redirection symbols start  with  #.
     For example:

     -> printenv #> my_vars

     saves the current environment variables in the file my_vars.

     To redirect the ouput of shell commands, such as ls, use the
     usual shell syntax for redirection, for example:

     -> ls >  listing.output


     NOTE:   You cannot redirect output for the  fol-
             lowing  commands: run, step, next, cont,
             reset.



Specifying a variable's location
     In certain situations, you must specify the  location  of  a
     variable  to  avoid  ambiguity. This is usually required for
     symbols of the same name that  are  defined  differently  in
     different files. The location of a variable can be specified
     in one of four ways:

     o `file`function`variable

     o `file`line_number`variable

     o `file`variable

     o function`variable

     For example, assume file i1.C contains  the  following  top-
     level declaration:

     int i = 3;

     and that i2.C contains the following top-level declaration:

     static int i = 1;

     With  these  files  loaded,  ObjectCenter  reports   on   both
     instances of i:

     -> whatis i
     static int i;
     extern int i; /* initialized */

     Because there are two variables named i in your program, you
     specify a location when printing either value:

     -> ` i1.C`i;
     (int) 3
     -> `i2.C`i;
     (int) 1

Changing and listing directories
     The cd command changes  the  current  working  directory  in
     ObjectCenter  in  the  same  manner  as  the cd command in the
     shell.

     Also, you can list the current working  directory  and  list
     files in the directory by using pwd and ls, two aliases that
     ObjectCenter defines  automatically  to  execute  Bourne  sub-
     shells.  For example:    

     ->alias
     ls           sh ls
     pwd          sh pwd
     -> pwd
     /usr/fenway
     -> cd demo
     wd now: /usr/fenway/demo
     -> ls
     Makefile   display.C   main.C    sort.C     sort.h
     ->

Entering C++ code in the Workspace
     When you enter C++ code at the Workspace prompt, the Workspace
     becomes  a  direct connection to the ObjectCenter interpreter.
     This means that C++ statements that you type in the  Workspace
     are immediately interpreted, and expressions are immediately
     evaluated.  ObjectCenter displays the result immediately after
     you type the input.

     ObjectCenter maintains a Workspace scratchpad containing C++
     definitions, which you can reference throughout a session.

     Although you can define functions, variables, and  types  in
     the Workspace, it is not easy to edit and modify the defini-
     tions. So it is better to put code that needs to be modified
     or debugged in a source file and then load the source file.

  Using multiple-line statements
     C statements you type in  the  Workspace  can  span  several
     lines.  To  continue  the statement on the next line, simply
     press Return at an appropriate place in the  statement.  The
     input prompt changes from
     - > to + >, indicating that the Workspace is expecting addi-
     tional input to complete the statement or expression:

     -> 123 +
     +> 456 +
     +> 789;
     (int) 1368
     -> NOTE:   The +>  prompt  only  appears  when  the
             Workspace is waiting for additional C++
             code.  This often happens when you  for-
             get  to  type a semicolon (;) at the end
             of a C++ statement or expression. To  com-
             plete  the input, type a semicolon, then
             press Return.

  Defining variables and types
     You can define and use variables and types directly  in  the
     Workspace  at  any  time. To display the type and value of a
     variable, enter the name of the variable followed by a semi-
     colon.  (This is a shortcut to using ObjectCenter's print com-
     mand.)

     ObjectCenter evaluates the input  and  returns  its  type  and
     value. For statements, the type void is displayed:

     -> int i;
     -> i = 16;
     (int) 16
     -> while (i<100)i++;
     (void)
     -> i;
     (int) 100
     -> int j;
     -> j= i+10;
     (int) 110

     For data structures, ObjectCenter displays all members:

     -> struct mystruct {int i; float f;};
     -> struct mystruct struct1;
     -> struct1;
     (struct mystruct) =
     {
     int i = 0;
     float f = 0.000000e+00;
     }

     For pointers, ObjectCenter displays the kind of  pointer,  the
     address being pointed to, and the data being pointed to:

     -> char *msg = "hello there";
     -> msg;
     (char *) 0x173ea8 "hello there"

  Using the delete operator in the Workspace
     The C++ delete keyword conflicts with the ObjectCenter delete
     command. To deallocate memory using the delete operator in the
     Workspace, delimit the delete statement with parentheses, such
     as:

     -> int *iptr = new int[4];
     -> (delete iptr);

  Defining functions
     When defining a function in the Workspace, you must  specify
     its  return  type  and  you  should specify the types of the
     parameters when you declare the parameters. For example,  to
     define a function that adds two integers, you could enter:

     -> int add(int x,int y)
     +> { return x+y; }
     -> add(3,4);
     (int) 7

  Calling library functions
     You can execute library functions after the library has been
     attached.  For instance:

     Attaching: /usr/lib/libc.a
     -> strlen("hi");
     Linking from '/usr/lib/libc.a' ... Linking completed.
     (int) 2

  Declaring types in the Workspace
     If an object code  file  without  debugging  information  is
     loaded, no information about the types of variables or func-
     tions is available.

     If the type for a variable defined in compiled code is  una-
     vailable,  ObjectCenter assigns the generic type <data> to the
     variable. Before variables of type <data> can be used in the
     Workspace,  you must declare a type in the Workspace or in a
     source file.

     Consider the file xyz

     int i=4;
     int test()
     {
     return i;
     }

     If this file is loaded into ObjectCenter  as  an  object  file
     compiled  with  debugging  information,  you  do not need to
     declare the type of a variable or function that  is  defined
     in that file before using it; for example:

     -> load xyz.o
     -> i;
     (int) 4

     However, if this file is compiled without debugging informa-
     tion and loaded into ObjectCenter, this happens:

     -> load xyz.o
     -> i;
     Error #739: Variable 'i' has undefined type (<data>).
     ->
     -> extern int i;
     -> i;
     (int) 4

     If the type for a function defined in compiled code is  una-
     vailable, ObjectCenter assigns the generic type < text> to the
     function.

     If the function is called in the Workspace before  its  type
     is declared, ObjectCenter gives it the return type int:

     -> load xyz.o
     -> test;
     Error #739: Function 'test' has undefined type (<text>).
     -> test();
     (int) 4
     -> test;
     (int ()) 0xdc976 < 'test' module "xyz.o" >

  Responding to errors
     ObjectCenter flags errors you make in the Workspace and sets a
     breakpoint:

     -> extern int j;
     -> int i;
     -> i = j;
     Error #155: Undefined variable: 'j'.
     (break 1) ->

     If you want to return to the top  level  in  the  Workspace,
     issue the reset command.

  Using blocks
     Blocks are useful for ensuring that operations performed  in
     the  Workspace  do  not produce adverse side effects or con-
     flict  with  global  variables.  All   automatic   variables
     declared  within  the block are local to the block; that is,
     they cease to exist at the end of the  block.  You  can  use
     these  variables  for storing values and performing calcula-
     tions without affecting global variables, even of  the  same
     name. 

     For example:

     -> int i = -1;
     -> {
     +> int i = 0;
     +> while( i <= 10 ) i++;
     +> }
     (void)
     -> i;
     (int) -1

     ObjectCenter  does  not  execute  expressions  and  statements
     placed  within a block until the block is ended with a clos-
     ing brace. Values produced by the expressions and statements
     within the block are not displayed. Only the void value pro-
     duced at the end of the block is displayed.

Unloading the Workspace scratchpad
     During a ObjectCenter session, all C++ definitions you enter
     from  the  Workspace are stored in the Workspace scratchpad.
     You can undefine all C++ definitions stored in the Workspace
     scratchpad by using the unload workspace command in the
     Workspace. For example:

     -> int add(int x,int y)
     +> { return x+y; }
     -> add(3,4);
     (int) 7
     -> unload workspace
     Unloading: workspace
     -> add(5,6);
     Error #733: 'add' is undefined.

     Unloading the  Workspace  scratchpad  does  not  affect  any
     loaded  files or attached libraries. Workspace input history
     is also unaffected by unloading the Workspace scratchpad.

Clearing the Workspace
     If you are using the Motif or OPEN LOOK  versions,  you  can
     clear  the  Workspace  pane  by opening the Workspace pop-up
     menu and selecting Clear.

Saving code with edit workspace
      You can save code you define in the Workspace with the edit
      workspace command.

      During an ObjectCenter session, all C++ definitions you enter
      are stored in a Workspace scratchpad. The edit workspace command
      lets you save the scratchpad to a file, by default workspace.C,
      and then edit the file.

      For example, suppose you create a class in the Workspace. You
      can create stubs for other classes and external functions called by
      the class, and then invoke the methods in the class to test them.
      
      After testing your class, you can use edit workspace to create a
      file containing the code you defined in the Workspace. You can
      enter your own name for the file or accept the default,
      workspace.C.
      
      -> edit workspace
      Appending all workspace definitions to a file.
      Default filename is "workspace.C" in the current directory.
      Please specify a filename, press Return to accept default,
      or <CTRL-D> to abort:

      If you want to test a particular set of definitions, edit the
      file so that it contains the definitions you want to test. Then
      use the unload workspace command to unload all the definitions
      and objects you created in the Workspace, and use the source
      command to load the definitions in your saved file back into the
      Workspace. Note that the source command will report errors if
      you've unloaded any definitions that the saved file depends on.

      If you want to use the new file as source code, add any #include
      lines you need and remove any extraneous lines. For example,
      some ObjectCenter commands, such as "whatis", will appear in the
      file. 

      NOTE: When using code developed in the Workspace, remember that
	    static functions and variables and private and protected
	    member functions are visible at global scope in the
	    Workspace. As a result, you may have to add friend
	    functions or classes or make static functions externally
	    visible to use the Workspace sources as a separate file.


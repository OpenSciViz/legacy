  Thread support on Solaris 2.3

     We've added support for threaded applications on the Solaris 2.3
     platform in process debugging mode (pdm), with the ability to
     debug threads in executables and a graphical Thread Browser to
     show the status of all the threads in your program. We have also
     added a thread-safe libC (the C++ library). 


     In process debugging mode, the Thread Browser gives you 
     information about the threads and lightweight processes in your
     program. This information includes a list of all threads, and the
     state of each thread. The state information includes the function
     the thread is executing, the execution state (for example,
     running, sleeping) of the thread, and the start function for the
     thread.  

     At any given time, the Thread Browser focuses on a single thread or
     light-weight process (LWP), known as the "current active entity."
     You control execution of the current thread with the cont, next,
     nexti, step, and stepi commands. You can display a traceback of
     the thread execution stack with the where command. You can also
     perform these operations on another thread at the break level by
     making it the current active entity. To make another thread the
     current active thread, you use the thread command with the new
     thread number as an argument.

     For more information about debugging threaded applications, see
     the thread and threads entries in the Manual Browser.




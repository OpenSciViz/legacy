NAME    window managers

     In general, we recommend that you use mwm, olwm, or olvwm as
     your window manager when you are running ObjectCenter.
     Although you may be able to use other ICCCM-compliant window
     managers with some success, ObjectCenter does not support other
     window managers explicitly.

Actions used to "Quit"

     Some window managers provide two different functions for getting
     rid of a window: f.delete and f.destroy. We recommend that you be
     careful about distinguishing them.

     The f.delete function sends an ICCCM WM_DELETE_WINDOW
     message to the selected window, which causes the window to
     disappear. The f.destroy function tells the X11 server to sever the
     connection to the client owning the selected window, killing that
     client process as a result.

     Be careful about binding f.destroy to a menu item like "Quit". If you
     do so, and your application is a multi-window application, the
     whole application will die. This problem can also result from
     binding f.destroy to a "Quit" item in a dialog box or "pinned"
     menu. In this case, you should probably use f.delete instead of
     f.destroy.

Bringing transient windows to the front

     All of ObjectCenter's non-top-level windows, such as the dialog
     boxes and pinnable property sheets, are transient for the top-level
     window to which they belong.

     As of OpenWindows 3.0, by default, olwm forces all transients for a
     given window to appear stacked above that window. If you try to
     raise the "parent" window, all the transients are raised too.

     This means, for example, that if you request the Contents window
     from the Project Browser window, you cannot bring the Project
     Browser window to the front.

     If you want to change this behavior, put the following line in your
     .Xdefaults file:

     OpenWindows.KeepTransientsAbove: False


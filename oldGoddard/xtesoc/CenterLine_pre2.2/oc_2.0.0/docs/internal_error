This entry has solutions to the following internal error problems.

1.  I'm getting parsing errors

2.  I'm getting errors using centerline_* functions

3.  I want to continue even with unresolved references


1.  I'm getting parsing errors
---------------------------
You may be getting parsing errors on object code compiled with the
-g switch of the compiler (see CC).  The -g switch produces symbol
table information for debugging.

To eliminate these parsing errors, load the files into
ObjectCenter with the -G switch.  This makes
ObjectCenter ignore debugging information produced by the
-g switch of the compiler).

Loading files with the -G switch in this way allows you to load
compiled files for which ObjectCenter has trouble reading
the debugging information.

2.  I'm getting errors using centerline_* functions
---------------------------------------------------
You may be getting an internal evaluation error if you are using
a centerline_* function without an argument.  A centerline_*
function requires a string as an argument the first time you use
it in a session.  Omitting the argument the first time you use
it can cause an internal evaluation error.

To eliminate this error with a centerline_* function, use ""
as an argument the first time you use a centerline_* function.
For example,

        C++ -> centerline_where("");
        centerline_where((char *) 0x30e480 ""), builtin centerline function
        (int) 0

3.  I want to continue even with unresolved references
----------------------------------------------------
You may want to continue executing your program in spite of a
break level with an unresolved reference. The cont command allows
you to do this.

To continue execution beyond an unresolved reference, issue the cont
command with an argument identifying the return value you want
ObjectCenter to use as it continues execution.

For example, for functions returning

o       ints, enter

                -> cont 10

o       floats, enter

                -> cont 3.14

o       chars, enter a char cast

                -> cont (char) 'a'

        Note, however, that functions returning a 'char *' cause
        ObjectCenter to produce an  internal error if you
        try something like:  cont (char *) "Hello World"

o       voids

                -> cont (void) 0

See Also
----------
built-in functions   for more information about CenterLine functions
CC		     for information about the C++ translator
cont		     for information about continuing execution from a break level



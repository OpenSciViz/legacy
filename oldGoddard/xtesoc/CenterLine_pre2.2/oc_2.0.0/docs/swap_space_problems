This entry addresses the following space problems:

1.  fork() fails inside ObjectCenter due to low swap

2.  Out of virtual memory with swap space left

3.  ObjectCenter is slow, crashes, or has unexpected behavior

4.  In ObjectCenter alloca() exhausts virtual memory

5.  mem_trace uses up swap space



1.  fork() fails inside ObjectCenter due to low swap
-----------------------------------------------------------
For information about fork() failing inside ObjectCenter due
to low swap, see "forking".

2.  Out of virtual memory with swap space left
--------------------------------------------------
ObjectCenter may give an "out of virtual memory" error
message while your system shows that plenty of swap space is left.
If so, consider the following.

A.      The maximum data size may be set too low.

        On some workstations, the default maximum data size is set
        too low for ObjectCenter.  To prevent the error, change
        the maximum data size.

        These are examples for SunOS4.

        To find the maximum data size for the current process,
        enter
                % limit datasize

        To increase the maximum data size, enter

                % limit datasize value-in-kbytes

        To make the maximum data size unlimited, enter

                % unlimit datasize

B.      The error may be bogus if your are debugging an X11
        application.

        You may receive a bogus "out of virtual memory" message
        when debugging an X application if you continue past a
        run-time warning about an invalid cast.  For example,
        you passed a pointer of type foo to a routine that was
        expecting a parameter of type foo.

3. ObjectCenter is slow, crashes, or has unexpected behavior
------------------------------------------------------------------
Having less than the recommended RAM can cause ObjectCenter
to be slow, crash, or have unexpected behavior, including
internal evaluation error messages and messages that say you are
out of virtual memory.  Low swap space may be causing these
problems, and you may have to increase swap space to solve them.

To find the ObjectCenter RAM and swap space requirements,
see the hardcopy Release Bulletin.

When evaluating RAM and swap space requirement figures, keep the
following in mind.

o       The figures apply only to supported platforms.

o       The GUI version of ObjectCenter uses more swap
        space than Ascii ObjectCenter.

o       The figures assume that the entire amount is available
        to ObjectCenter.  If you are using other
        applications at the same time, you must factor in the
        requirements of the other applications.

o       The amount of memory your application uses in
        ObjectCenter under our environment depends on a
        number of factors, including:

        -  how much data your program allocates
        -  whether you have debug info on your libraries,
        -  how much code you load as source rather than object
        -  what options you have used.

4.  In ObjectCenter alloca() exhausts virtual memory
-------------------------------------------------------
If your program makes extensive use of alloca(), it may be
causing out-of-virtual-memory messages in ObjectCenter
but not outside it.  The ObjectCenter implementation
of alloca() uses an algorithm different from the system
alloca().

o       The system alloca() allocates size bytes of space in
        the stack frame of the caller, and returns a pointer
        to the allocated block.  Since alloca() allocates
        memory on the stack, the memory is freed on the return
        from foo().

o       The ObjectCenter alloca() is a wrapper to an
        actual malloc call.  It keeps the memory allocated
        until the end of the program rather than until the
        function return.

The following program has a leak that exhausts virtual
memory:

        #include <malloc.h>
        #include <alloca.h>
        main()
        {
             int i = 0;
             while (i < 1000)
             {
                 printf("%d\n", ++i);
                 a();
             };
        }

        a() { alloca(1000000); }

You can replace the CenterLine alloca() with the system alloca() in
libc.a. However if the system alloca() is called from source code it
causes execution errors because the system alloca() is based on stack
manipulation to which CodeCenter is very sensitive. The above program
will run with the libc.a alloca() only when it is compiled. 

To extract the system alloca() from libc.a, enter

        % ar x /usr/lib/libc.a alloca.o


5.  mem_trace uses up swap space
-----------------------------------
You may have run out of swap space if you used ObjectCenter
memory leak detection and specified four or more stack trace levels.
Memory leak detection identifies potential memory leaks by
reporting on the memory that the program allocates while running
and fails to free before exiting (see "memory leak detection").

To prevent ObjectCenter from running out of swap space,
set the number of stack trace levels to less than four.  In the
Workpace enter

        C++ -> setopt mem_trace number_less_than_4

See Also
----------
forking			for information about problems with forked processes
memory leak detection	for information about finding memory leaks


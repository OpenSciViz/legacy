
NAME	
     load

     loads source, object, library, and project files

    ----------------    
    |  cdm  | pdm  |
    +-------+------+
    |  yes  | no   |
    ----------------   
	
     NOTE	To load an a.out file, use the debug command in
		pdm.  See the debug reference page for more
		information.

SYNOPSIS
     load [ switches ] file ...

DESCRIPTION
     [ switches ] file ...	Loads specified files into 
     				ObjectCenter.  If the specified files
				are already loaded, reloads files    
				that have been modified since they   
				were last loaded.                    
	                  
     Files can be source, object, library, and project files, or
     template instantiation modules; see the "Files" section for
     more details.
     
     See Table 17 for a description of the switches supported by
     load.

SWITCHES
     The load command accepts all switches used with the C++
     translator or the C compiler, but it acts upon only the
     switches listed in Table 17.
     
	      Table 17 Switches to the load Command
---------------------------------------------------------------
Name of Switch	       What the Switch Tells ObjectCenter To Do
---------------------------------------------------------------
+k[=filename] | -k     When loading an object file that requires  
		       compilation outside the environment: save  
		       and restore header files from a            
		       repository (+k); if filename is provided,  
		       use it to determine which header files to  
		       skip. The default setting is -k, which     
		       says do not save and restore header files  
		       from the repository. See the precompiled   
		       header files entry for more information.  

-C		       Cause ObjectCenter to parse the specified  
		       file as C code, rather than C++ code. See  
		       the language selection entry for details   
		       about configuring the language used by     
		       ObjectCenter.                              

-CXX		       Cause ObjectCenter to parse the specified  
		       file as C++ code, rather than C code. See  
		       the language selection entry for details   
		       about configuring the language used by     
		       ObjectCenter.                              

-dd=off		       Specify whether ObjectCenter uses demand-
-dd=on		       driven code generation; see the demand-
		       driven code entry for more information.
		       (-dd=on is the default)	

-Dname[=definition]    Define name as if with a #define         
		       directive. If definition is not supplied,
		       then define name as 1.                   

-G		       When loading compiled files, ignore       
		       debugging information produced by the -g  
		       switch of the compiler. This allows you   
		       to load compiled files for which          
		       ObjectCenter has trouble reading the      
		       debugging information. Also, you can save 
		       memory by loading libraries that have     
		       been debugged with -G; if you use -G when 
		       loading a library, ObjectCenter ignores   
		       debugging information when linking from   
		       the library.                              

-hdrepos=directory     When loading an object file that requires
		       compilation outside the environment, look
		       in directory for the filename            
		       (precompiled header information file)    
		       used with +kfilename. See the precompiled
		       header files entry for more information. 

-I directory_name      Add directory_name to the list of       
		       directories to search for files specified
		       by the #include preprocessor directive. 
		                                               
		       When the name of a file is surrounded by
		       double quotes (" "), the search path is 
		       as follows: first, the directory of the 
		       file being read, then in directories    
		       specified by -I, and finally in the     
		       /usr/include directory.                 
		                                               
		       When a filename is surrounded by angle  
		       brackets (< >), the search path is as   
		       follows: first in directories specified 
		       by -I and then in the /usr/include      
		       directory.                              

-Ldir		       Add dir to the list of directories to
		       search for libraries.

-lx		       Search for and load a library named       
		       libx.a, where x is a library name suffix. 
		       If shared libraries are supported by      
		       ObjectCenter on your platform, see the    
		       ObjectCenter Platform Guide for           
		       information about loading them.           

-U macro_name	       Cause the predefined macro_name to become 
		       undefined as if by an #undef directive.   

-w		       Suppress warnings, but report errors.      
		                                                  
		       If you use -w when loading a library,      
		       warnings are suppressed when modules are   
		       linked from the library.                   
		                                                  
		       This switch is useful when you are using
		       a graphical interface to ObjectCenter, 
		       since only Ascii ObjectCenter supports 
		       batch loading of files. Use the Workspace 
		       redirection capabilities to get 
		       ObjectCenter to save suppressed warnings 
		       in a file.             
		                                                  
		       For instance, the following command loads  
		       the files foo.c and fum.c, placing all     
		       warning messages in the file load.errors   
		       instead of sending them as output to the   
		       screen.                                    
		                                                  
		       load foo.c fum.c #> load.errors            
		                                                  
		       Then you can examine the file load.errors  
		       for problems.                              
---------------------------------------------------------------
  Using system-wide loading switches
     When loading files, ObjectCenter always uses command-line
     switches specified by one of two options: either
     sys_load_cflags for (C files) or sys_load_cxxflags (for C++
     files). The sys_load_cflags and sys_load_cxxflags options
     specify the directories to search for libraries and system
     header files as well as some macros.
     
     ObjectCenter's default values for sys_load_cflags and
     sys_load_cxxflags are specified in the system-wide
     ocenterinit file.  The exact values depend on the type of
     workstation you are using.  To see the values on your
     system, enter this command:
     
     -> printopt sys_load_cxxflags
     
     and
     
     -> printopt sys_load_cflags
     
     If you have a different library or #include path for either
     CC or cc than that specified by the sys_load_cxxflags or
     sys_load_cflags option, you should change the value of the
     option either in your personal .ocenterinit file or in the
     system-wide ocenterinit file.

  Specifying your own loading switches
     ObjectCenter always uses all switches specified with
     sys_load_cxxflags and sys_load_cflags. In addition,
     ObjectCenter also uses any switches specified with the
     load_flags option.
     
     Typically, you use the load_flags option to specify any
     switches specific to your own work. For example, the
     following commands show the use of a macro name, BETA,
     specific to a project:
     
     -> setopt load_flags -DBETA 
     -> load xyz.c 
     Loading(C++): -DBETA xyz.c 
     ->
     
     With one exception, any switches you explicitly enter when
     you issue load replace all switches you may have specified
     with load_flags. The exception is the -L switch in
     load_flags; ObjectCenter always uses -L switches specified
     by load_flags.
     
     Here is an example:
     
     -> setopt load_flags -DBETA -w 
     -> load sample.c 
     Loading: -DBETA -w sample.c 
     -> unload sample.c 
     Unloading: sample.c 
     -> load sample.c -DDEBUG 
     Loading: -DDEBUG sample.c
     
     In this example, when we explicitly specify the -DDEBUG
     switch when loading the file sample.c, ObjectCenter uses
     -DDEBUG instead of the -DBETA and -w switches specified by
     load_flags.
   TIP:  When does the load_flags option have precedence?

     If you are loading a file for the first time, and if you do
     not specify any switches with the load command, ObjectCenter
     uses the switches specified by the load_flags option.
     
     However, if you are loading a file for the first time and
     you do specify any switches with the load command,
     ObjectCenter uses the switches you specify with load instead
     of the switches in the load_flags option.  After the first
     time you load a file, ObjectCenter reuses the switches it
     used the first time it loaded the file whenever it attempts
     to load that file. For instance, when you reload a file by
     issuing load in the Workspace without any switches,
     ObjectCenter reuses the switches from the first time you
     loaded the file. Similarly, if you reload the file by
     issuing a build command, ObjectCenter reuses the switches
     from the first time it loaded the file.
     
     Once you have loaded a file, changing the value of
     load_flags has no effect on subsequent loads of that file,
     even if the load_flags option was applied the first time you
     loaded it.
     
     If you want to change the switches that ObjectCenter uses
     when loading a file that has already been loaded, you must
     do one of the following:
     
     o Issue the load command in the Workspace using the new
       switches. Then ObjectCenter will use the new switches every
       time it attempts to load the file.
     
     o Change the value of load_flags to specify the new
       switches, issue an unload command for the file, and then
       issue a load command for the file without specifying any
       switches. In this case, ObjectCenter uses the switches
       specified in load_flags.
     
     o Use the file's property sheet in the Project Browser to
       change the options used to load the file.

  Files
     Use the load command to load the following kinds of files:
     
     o Source, including preprocessed source files and #include
       files
     
     o Object
     
     o Library files, including prototype files

     o Project

     o Template instantiation modules

     We describe loading each kind of file in the next few
     sections. See the debugging reference page for a
     discussion of trade-offs in the way you load your program.

  Loading source files
     If you issue load with the name of a source code file when
     the corresponding object code file is already loaded,
     ObjectCenter unloads the object file before it loads the
     source file.

  Loading C++ source files
     By default, source files are loaded as C++ modules. C++
     source files are loaded and translated into an intermediate
     code that is used when you execute the code.
     
     The translation process is compatible with the C++
     translator and Release 3.0 of the AT&T C++ Language System
     defined in the AT&T C++ Language System Release 3.0 Product
     Reference Manual supplied with ObjectCenter. For information
     about C++, see that manual.
     
     For more information about ObjectCenter's compatibility with
     other C++ translators and with Release 3.0 of the AT&T C++
     Language System, see the CC entry.

  Using the cxx_suffixes option to specify extensions 
     The cxx_suffixes option specifies file extensions to search
     for when ObjectCenter needs to find a C++ source file that
     corresponds to a given object file. When loading files, this
     condition arises if you issue the load command and specify
     an object file. In this case, ObjectCenter needs to check
     the corresponding source file to see if the object file is
     up-to-date. If not, ObjectCenter compiles a new object file
     before proceeding.

     By default, the cxx_suffixes option contains the .C and .c
     suffixes. If you are in C++ mode, which is also the default,
     this setting for cxx_suffixes means that ObjectCenter first
     searches for a corresponding C++ source file using a .C
     extension. If the search with this extension fails, then
     ObjectCenter searches using a .c extension.
     
     If you have specified other, or additional, file extensions
     with the cxx_suffixes option, ObjectCenter first searches
     for a corresponding C++ source file using the extensions in
     the order they are specified, left to right. If searches
     with all specified extensions fail, then ObjectCenter
     searches using a .C extension and then a .c extension.
     
     For example, if you set the cxx_suffixes option as follows:
     
     -> setopt cxx_suffixes cxx cpp
     
     and then issue the following command:
     
     -> load bar.o
     
     ObjectCenter searches for a C++ source file corresponding to
     bar.o in the following order: bar.cxx, bar.cpp, bar.C, and
     bar.c.
     
     NOTE	See the language selection entry for
		more information about setting the language
                ObjectCenter uses.

  Loading C source files
     To load a source file as a C module, use the -C switch.By
     default, ObjectCenter loads C source files following the
     same rules that the cc command follows on your system.  The
     source files are translated into an intermediate code that
     is used when you execute the code.  The translation process
     is compatible with the C compiler and the ANSI standard for
     the C language.  Use the config_c_parser command to change
     the compiler configuration used by ObjectCenter, and use the
     ansi option to make sure that the C preprocessor is ANSI
     compliant.
       
  Loading and using preprocessor files
     ObjectCenter uses #line directives to map certain kinds of
     preprocessed code to the unpreprocessed code. It therefore
     allows you to work directly with input files that are run
     through preprocessors that generate C or C++ files with
     #line directives pointing back to the input file. Such
     preprocessors include yacc and certain SQL preprocessors.
     ObjectCenter uses the #line directives to associate lines in
     the generated C or C++ file with lines in the input file
     that you wrote.
     
     Thus, ObjectCenter helps you debug preprocessed code by
     allowing you to examine the input to a preprocessor rather
     than just the output from it; the input is typically much
     easier to read than the output. To work with preprocessor
     files:
     
     1 Load a file containing #line directives.
     
     2 Work with the input file in your ObjectCenter session.
     
     NOTE	See the preprocessed code reference page for
		more information about debugging code generated by
		preprocessors such as yacc.
     
  Search path for #include files
     To give the search path for #include directories, use the -I
     switch with the load command according to the following
     format:
     
     load -Iinclude_dir1 [-Iinclude_dir2 ...] file
     
     NOTE	The path option does not provide a search path
		for loading #include files, only for loading 
		source and object files.

  Loading object files
     You can load object code files that have been compiled with
     or without the -g compiler switch that adds debugging
     information.  However, to have the greatest debugging
     functionality in ObjectCenter, load object code files
     compiled with debugging information whenever possible.
     
     NOTE	When loading object code into ObjectCenter, make
		sure that the object code was compiled with the
		same release of the operating system that you are
		using to run ObjectCenter.
			       
     Because object files do not retain information about
     classes, ObjectCenter does not provide full source-level
     debugging, class browsing, and Workspace interaction for
     classes defined in C++ code loaded in object form. See the
     "Special considerations with C++ object files" section 
     for more information.
     
     Also, see "Appendix C Compatibility with Release 2.1" 
     for information about compatibility of object files
     created from an earlier version of the C++ translator.
     
     To load an object file as a C module, use the -C switch.
     
     If you issue load with the name of an object code file when
     the corresponding source code file is already loaded,
     ObjectCenter unloads the source file before it loads the
     object file.

     If an object code file specified with load does not exist
     and the directory that contains the source file contains a
     makefile, ObjectCenter does a make of the object file.
     Otherwise, if the source is available, ObjectCenter creates
     an object file by calling the C++ translator or the C
     compiler.
     
     ObjectCenter supports the loading of CenterLine-C object
     files as well as those generated by your platform's native C
     compiler. See the ObjectCenter Platform Guide for your
     particular platform for information about any additional
     object files that ObjectCenter may support.

  Specifying a different compiler
     When ObjectCenter needs to compile a C file, it invokes the
     compiler defined by its cc_prog option. If this option is
     unset (the default), ObjectCenter invokes cc.

  If ObjectCenter can't find the file
     If you specify a file with load that does not exist,
     ObjectCenter looks at the setting of its create_file option.
     If create_file describes how to create the specified file,
     ObjectCenter uses those instructions to create the file,
     then loads it. For example:
     
     -> ls *.C 
     backup.C 
     -> load a.C 
     Cannot open '/net/fenway/u1/bobh/c++/a.C'.  
     -> setopt create_file @a.C@cp backup.C a.C 
     -> load a.C 
     Cannot open '/net/fenway/u1/bobh/c++/a.C'.  
     Executing: cp backup.C a.C
     Loading (C++): a.C
     
     For more information about create_file, see the options
     entry.

  Loading libraries
     Loading a library makes the contents of the library
     available to ObjectCenter. You can load a library by:
     
     o Specifying the full pathname of the library with the load
       command
     
     o Using the -l switch.
     
     This is similar to using the -l switch to cc. See Table 18
     for a listing of the order in which ObjectCenter searches
     directories for the library.

	  Table 18 ObjectCenter's Search Path for Libraries
-----------------------------------------------------------------
  Order	   Search Path
----------------------------------------------------------------
   1	   Directories specified on the command line by -Ldir in 
	   the order specified

   2	   Directories specified by -L in ObjectCenter's 
	   load_flags option 

   3	   Default system directory specified by the 
	   sys_load_cxxflags and sys_load_cflags option
----------------------------------------------------------------
  TIP:  Specifying the search path for loading libraries and 
  #include files

     If you are using an ANSI compiler, make sure that the path
     for the libraries and #include files required for ANSI are
     specified either on the load command line or by setting the
     load_flags and/or sys_load_cflags options. The directories
     required by ANSI must be searched before the default system
     directory specified by sys_load_cflags. You must also
     explicitly load the C library.
     
     Similarly, if you use a compiler like clcc that has header
     files and libraries in "non-standard" locations, be sure to
     set the switches to the load command to specify the correct
     location to search before the default system directory
     specified by sys_load_cflags. You must also explicitly load
     the C library.

     For instance, if you are using clcc as your C compiler and
     using -ansi as a compilation mode, you should make the
     following specifications:
     
     o	Issue the setopt ansi command.
     
     o	Set the sys_load_cflags option to contain the following
	as the first -L specification:
      
      	-L/usr/local/CenterLine/clcc/arch_os/lib
      
      	where arch_os is the name of your architecture and
	operating system.
      
     o	Set the sys_load_cflags option to contain the following
	-I specification before the specification of
	-I/usr/include:
      
       	-I/usr/local/CenterLine/clcc/arch_os/inc
       
       	where arch_os is the name of your architecture and
	operating system.
       
     o 	Issue the following command:
       
       	->load /usr/local/CenterLine/clcc/arch_os/lib/libc.a
       
        
	
     NOTE	If shared libraries are supported by ObjectCenter
		on your platform, see the ObjectCenter Platform 
		Guide for information about loading them.
	
     Some operating systems provide a -u symname option to ld,
     which allows you to enter symname as an undefined symbol in
     the symbol table. The -u option is typically used to load
     entirely from a library, since initially the symbol table is
     empty and an unresolved reference is needed to force the
     loading of the first routine.
     
     ObjectCenter does not provide a -u switch for the load
     command.  Nonetheless, you can force the loading of a first
     function from a library in ObjectCenter by defining the
     function as external and making a reference to it.  Here is
     an example:
     	
	1 -> extern void main (); 2 -> main;

     NOTE	For some of the C library functions, you can
		substitute your own version. See your
		ObjectCenter Platform Guide for a list of the C
		library functions replaced by ObjectCenter, and
		the ones that you can replace.
			  
                To use your own version of a function, load the
		function in a source or object file before
		linking your program. If your program has already
		been linked, you must quit, then start a new
		ObjectCenter session to substitute your function
		for one of the ObjectCenter replacements.

  Loading function prototype files
     When working with C files, loading function prototypes
     allows ObjectCenter to check the number and type of
     arguments for calls to functions. Prototype files
     conventionally end in .proto. If a filename ends with
     .proto, load first looks for the file in the current
     directory, then looks in the list of directories specified
     by the proto_path option.
     
     For information about creating your own prototype files, see
     the reference page for the proto command.

  Loading project files
     If the first line of the file you specify with load is as
     follows:
     
     /* ObjectCenter Project File */
     
     ObjectCenter will invoke source instead of load to retrieve
     the file's contents. This is the way in which ObjectCenter
     loads project files.  When you load a project file,
     ObjectCenter reloads the most recent version of the source
     and object files in your project.
     
     NOTE	Loading a project file does not unload any
		modules that were already loaded. To unload 
		modules before loading a project file, use the 
		unload command first.
     
OPTIONS
     
     NOTE	Whenever you issue the load command with a
		particular file, ObjectCenter uses the option 
		values that were in effect the first time the 
		file was loaded. If you change the value of an 
		option after loading a file, and you want that 
		option to affect the file, you must explicitly
		issue an unload command for the file and then 
		reload it. This is true even if the file failed 
		to load when you issued the load command.
     
     The following ObjectCenter options affect the load command:
     
     ansi	         Performs preprocessing in strict 
		         conformance with the ANSI C Standard.

     auto_compile	 Automatically compiles missing or 
			 outdated object files.

     batch_load		 Suppresses prompts to the user during 
     (Ascii		 loading.
     ObjectCenter only)

     cc_prog	  	 Specifies the name of the C compiler that 
			 ObjectCenter invokes.

     ccargs		 Specifies arguments passed to the C 
			 compiler when invoked from 
			 ObjectCenter.

     create_file	 Specifies commands to create a new file 
			 when loading.

     cxx_prog		 Specifies the name of the C++ translator 
			 to be invoked by ObjectCenter when 
			 compiling a C++ file. 

     cxx_suffixes	 Specifies file extensions to search for 
			 when ObjectCenter needs to find a C++ 
			 source file that corresponds to a given 
			 object file.

     cxxargs		 Specifies default arguments passed to the 
			 C++ translator when invoked by 
			 ObjectCenter. 

     echo		 Echoes the input stream after 
			 preprocessing (similar to the -E compiler 
			 switch).

     ignore_sharp_lines	 Causes ObjectCenter to ignore #line 
			 directives generated by preprocessors. 

     instrument_all	 Automatically instruments files as they 
			 are loaded. See the instrument entry 
			 for more information.

     load_flags		 Specifies the default switches to use if 
			 load is called without any switches. 
			 ObjectCenter always uses any -L 
			 switches specified in load_flags.

     page_load (Ascii	 Sets the number of lines of error reports
     ObjectCenter only)	 to display before prompting the user for 
			 more.                                    
 
     path		 Specifies the search path for loading     
			 source and object files (not for #include 
			 files).                                   

     preprocessor	 Specifies a command to execute in a 
			 subshell before the file is loaded. 

     program_name	 Specifies the value of the first argument, 
			 argv[0], to main().                        

     proto_path		 Specifies search path for prototype files. 

     src_err  (Ascii	 Specifies the number of source lines to be 
     ObjectCenter only)	 listed for errors and warnings.            

     subshell		 Specifies the shell used to invoke the C++ 
			 translator or C compiler.

     sys_load_cflags	 Specifies switches that establish the   
			 search path for system libraries and    
			 #include files when loading source      
			 files.   (files loaded with -C).        

     sys_load_cxxflags	 Specifies the switches that establish the
			 search path for system libraries and     
 			 #include files when loading a C++ file.  

     See the options entry for more details about each option.
     ObjectCenter does not support these options in process
     debugging mode (pdm).

  Disabling load-time error checking with comments
     You can suppress certain kinds of error checking by using
     predefined comments in your source code. See the built-in
     comments entry for more information.

RESTRICTIONS
     Loading an object file without debugging information may
     cause spurious warnings since initialized variables can be
     grouped together without correct type or size information.
     
     If load rejects an object file that was compiled with
     debugging information (for example, due to a type
     redeclaration), try loading the file with the -G switch.
     
     Occasionally, linking libraries may produce spurious
     warnings about size or type redeclarations.
     
     Trying to reload a file in the Workspace by using load with
     different switches does not necessarily cause ObjectCenter
     to reload the file.  Unless the file itself has been
     modified, ObjectCenter considers it up-to-date and will not
     reload it. You can work around the problem by using the
     unload command and then load with the desired switches.

USAGE
     Use the load command to load files into ObjectCenter or
     reload files that have been modified since they were loaded.

  Using shell wildcards
     The load command takes shell wildcards so you can load
     groups of files with one command. For example:
     
     -> ls *.c 
     abc.c xyz.c 
     -> load *.c 
     Loading: abc.c 
     Loading: xyz.c
     
     You can also use Esc-x at the end of a command line to
     expand wildcards. The escape sequence echoes the command
     line to a subshell that expands any wildcards. ObjectCenter
     pauses after displaying the expanded command line, allowing
     you to edit the command line before executing it.
     
     For example:
     
     -> load str*.C 
     Loading (C++): str_1.C 
     Loading (C++): str_2.C
     Loading (C++): str_3.C

  Using wildcard expansion
     If you use shell wildcards with load, you can also use Esc-x
     at the end of a command line to expand these wildcards. The
     escape sequence echoes the command line to a subshell that
     expands any wildcards:
     
     -> load *.c f?.o<Esc-x> 
     -> load abc.c xyz.c f1.o f2.o
     Loading: abc.C 
     Loading: xyz.C 
     Loading: f1.o 
     Loading: f2.o ->
     
     Here's an example:
     
     -> load *.C f?.o<Esc-x> 
     -> load abc.C xyz.C f1.o f2.o<Return> 
     Loading C++: abc.C 
     Loading C++: xyz.C 
     Loading C++: f1.o 
     Loading C++: f2.o 
     ->
     
     ObjectCenter pauses after displaying the expanded command
     line, allowing you to edit the command line before executing
     it.
     
     The sequence Esc-x is one of the key bindings supported by
     the Workspace. See the keybind entry for more information.

SEE ALSO
     build, built-in macros, config_c_parser, contents, debugging,
     make, save, swap, unload



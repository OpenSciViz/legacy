NAME
     reinit

     initializes all global variables

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |      no    |
     -------------------------


SYNOPSIS
     reinit
     reinit variable

DESCRIPTION
     << none >>		Sets all global variables to their     
     			initial values, frees allocated      
     			memory, closes open files, and sets  
     			all signal handlers to their initial 
     			values.  Freed memory is not returned
     			to the system; it is marked as free  
     			within ObjectCenter, to be reused the
     			next time you run your program.      
     
     variable		Sets the specified variable to its initial
     			value.                                    

USAGE
     Use the reinit command to reinitialize either a specific
     variable or your entire program. By using the reinit com-
     mand, you either set a particular variable to its initial
     value or set all global variables to their initial values
     and also free allocated memory, close open files, and set
     all signal handlers to their initial values.

     If reinit is called from a break level, errors may be
     intro- duced when global variables are reinitialized to
     their ini- tial values. In addition, reinit will free
     allocated memory that might still be used.

     To remove definitions of static structures from the
     Workspace, use unload workspace instead of reinit.

SEE ALSO
    construct, rerun, run, start


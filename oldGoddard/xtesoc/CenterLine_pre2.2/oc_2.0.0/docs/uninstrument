NAME
     uninstrument

     disables run-time error checking for an object file

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |      no    |
     -------------------------



SYNOPSIS
     uninstrument
     uninstrument file...
     uninstrument all

DESCRIPTION
     < none > 
     Prompts you to remove instrument  information  from
     files one at a time.

     file ...  
     Removes instrumentation information from file.

     all 
     Removes instrumentation information from all files.

USAGE
     Use the instrument and uninstrument commands to  enable  and
     disable run-time error checking of loaded object code.  Ena-
     bling the run-time error checking of loaded object  code  is
     called instrumenting the file.

  Performance considerations

     When you disable run-time error checking  for  a  particular
     module,  that  module  runs  somewhat  faster than an object
     module with run-time error checking enabled. See  the  "Run-
     time  error checking in source code vs. object code" section
     of the "instrument" entry and "Loading source versus  object
     code  versus  executables" section of the "debugging" entry
     for more information about the performance trade-offs.

     See the instrument entry for more information about  instru-
     menting and uninstrumenting.

SEE ALSO
     debugging, instrument


NAME
     source

     reads ObjectCenter commands from a file

     -------------------------
     |   cdm    |     pdm    |
     +----------+------------+
     |   yes    |     yes    |
     -------------------------

SYNOPSIS
     source file

DESCRIPTION
     file 		Reads ObjectCenter commands from the 
			specified file.

USAGE
     Use the source command to read ObjectCenter  commands  from  a
     file.

     NOTE    Any change in the break level causes the source command    
             to terminate, therefore ObjectCenter will stop executing        
             commands in the file if it encounters a breakpoint or a    
             run-time violation. In addition, because the step command  
             causes execution to go up a break level and then back to   
             the original break level, ObjectCenter will stop reading   
             commands from a source file after the first step command.  

     ObjectCenter uses source to read the system-wide startup  file
     and either the .ocenterinit or .pdminit file in your home or
     current directory when you start ObjectCenter.

EXAMPLE
     The following example indicates how to  use  source  with  a
     file containing aliases:

     % cat aliases
     alias p print
     alias s step
     alias n next
     alias ls sh ls
     % objectcenter
	.
	.
	.
     -> source aliases
     -> p 123+456
     (int) 579
     ->

SEE ALSO
     load


NAME
     ANSI C

     ObjectCenter supports both Kernighan and Ritchie (K&R) C
     and the ANSI standard C language. The default setting
     depends on the underlying compiler in use, which is
     different for each platform.  The default setting is likely
     to be one that you are accustomed to on your platform. See
     the ObjectCenter Platform Guide for details.

     See the config_c_parser entry for more information about
     configuring ObjectCenter to emulate a particular C
     compiler.

     In the rest of this reference page, we describe the follow-
     ing topics:

     o Using the ANSI mode of ObjectCenter

     o ANSI conventions always in effect

     o Known incompatibilities and bugs in ObjectCenter's ANSI
       support

     o Using function prototypes, including generating them with
       the proto command and loading them from libraries

     NOTE	The information in this entry about ANSI C
		applies when you set the ansi option and also 
		do any of the following:
     
     		o	Use C mode
     
     		o	Load files with the -C switch
     
     		o	Set the primary_language option to C
     
     		See the language selection entry for additional
		information.


Using ANSI
     To work with ANSI C code using ObjectCenter's C mode, use
     the setopt command to set ObjectCenter's primary_language
     and ansi options:

	setopt primary_language C
        setopt ansi

     With these options set, ObjectCenter loads and runs C code
     strictly according to the ANSI standard.

     If you wish to use ANSI C rules while you are in C++ mode,
     then leave the primary_language option set to the default,
     which is C++ for ObjectCenter, and just use the ansi
     option. In C++ mode, the ansi option affects only the
     preprocessor.

     As shown in the following example, ObjectCenter with ansi
     set accepts constructs not found in K&R C, but found in
     ANSI C.

     C++ 3 -> cmode 
     C Workspace Enabled. 
     C 4 -> const int j=5; 
     Error #733: 'const' is undefined. 
     C 5 -> setopt ansi
     C 6 -> const int j=5; 

     Note:     If you are using ANSI, be sure to read the
               "Specifying  the search path for loading libraries
               and #include files" TIP in the "load" entry.

  TIP:  When does the ansi option take effect?

     If you forget to set the ansi option when you load an ANSI
     C source file, you'll get errors for code that is
     ANSI-compliant and not K&R C. To fix this problem, you must
     not only set the ansi option and load the file, you must
     also explicitly unload the file with the unload command
     before you load the file.
     
     Here's an example.
     
     Suppose your source file named ansi.c contains the
     following code:
     
     main() 
     { 
     const int i =4; 
     }
     
     Attempting to load this file generates an error:
     
     C++ 26 -> load -C ansi.c 
     Loading (C): ansi.c 
     Unloading: ansi.c 
     Warning: 1 module currently not loaded.
     
     The error message is as follows:
     
     Line: 3 E#733 `const' is undefined
     
     Now you realize you forgot to set the ansi option, which
     you do, but instead of unloading and loading, you simply
     load; as a result you get the same error again:

     C++ 27 -> setopt ansi 
     C++ 28 -> load -C ansi.c 
     Loading (C): ansi.c 
     Unloading: ansi.c 
     Warning: 1 module currently not loaded. 

     The correct way to cause the ansi option to take effect is
     to explicitly unload and then load:
     
     C++ 29 -> unload ansi.c 
     C++ 30 -> load -C ansi.c 
     Loading (C): ansi.c
     
     The ansi option works in a way that's similar to the way
     the load_flags option works; see the "When does the
     load_flags option have precedence?" TIP for more 
     information.

 ANSI conventions always in effect

     The following ANSI features are always in effect in
     ObjectCenter in C mode, even if ansi is not set:
     
     o  ANSI C function prototypes are always accepted by 	
	ObjectCenter; however, in K&R mode they do not force
	type coercion. Compare the following results involving
	the coercion of an int to a double:

     -> C++ 10 
     -> cmode 
     C Workspace Enabled. 
     C 11 -> unsetopt ansi 
     C 12 -> load -lm 
     Attaching: /usr/lib/libm.a 
     C 13 -> double sqrt(double); 
     C 14 -> sqrt(3); 
     Warning #69: Serious type mismatch in call to
     function 'sqrt': 
     Argument #1 has type (int) but type (double) was expected. 
     Defined/declared in "workspace":13 
     Linking from '/usr/lib/libm.a' .... Linking completed. 
     Linking from '/usr/lib/libc.sa.1.6' ... Linking completed. 
     (double) 2.523368e-157 
     C 15 -> setopt ansi 
     C 16 -> sqrt(3); 
     (double) 1.732051e+00 

     o In ObjectCenter , preprocessor directives do not have to
       start  with  the first character of a line. They can begin
       anywhere on a line, but the # character  that  begins  the
       directive must be the first non-whitespace character.

     o ObjectCenter ignores #pragma directives in K&R mode, as 
       well as in ANSI mode.

     o The unsigned-suffix is allowed even in K&R mode:

       -> unsetopt ansi
       -> unsigned int u = 5u;
       -> u;
       (unsigned int) 0x5

     o In accordance with the ANSI standard, ObjectCenter 
       concatenates adjacent string literals.

     o ObjectCenter places labels and variables into separate  
       name spaces.  This  means that a label and a variable 
       with the same name can be visible at the same time.

     o Union initialization lists are always allowed.

Known ANSI incompatibilities and bugs
     This section lists the known incompatibilities and bugs in
     ObjectCenter's support of ANSI C. If you discover other
     problems with the ANSI support, please contact CenterLine
     Software (email address:
      objectcenter-support@centerline.com)

     In preparing this list, we assume that all language-related
     ObjectCenter options are set to C and that the ansi option
     is set.

  Libraries and header files
     o ObjectCenter loads whatever libraries and #include files
       you indicate for it to load -- whether or not they are
       ANSI- compliant and whether or not you are in ansi mode.

       Note: If you are using an ANSI C compiler, see the
	     "Specifying the search path for loading libraries 
	     and #include files" TIP in the "load" entry.
  Function prototypes
     o In function prototypes with multiple sets of  parentheses,
       only  one set can contain parameter types if you are using
       the void keyword. For example, the following function pro-
       totypes should work in ObjectCenter but they do not:

       -> int (*g(void)) (int);
       Error #905: The function parameter list has an illegal 
       format.
       -> int (*g(void)) (int k);
       Error #905: The function parameter list has an illegal
       format.

      The workaround is to use one of the following forms:

      -> int (*g()) (int k);
      -> int (*g(void)) ();

  Scoping rules
     o ObjectCenter gives a within-block extern declaration file
       scope. ANSI gives it block scope.   

  Constant  static pointers to characters
     o ObjectCenter fails to accept the following construct in 
       ANSI mode:

       const static char *foo = "hello";

      It does, however, accept the following:

      static const char *foo = "hello";

  International features
     o ObjectCenter recognizes wide character constants and wide
       string literals, but it treats them as normal character
       and string constants. Trigraphs are not implemented.

Using function prototypes
     One of the big changes in ANSI C is the addition of function
     prototypes,  which  you can use to ensure that functions are
     being called with  the  proper  arguments  and  that  return
     values are being used properly.

Generating function prototypes
     ObjectCenter can automatically generate function prototypes
     for your loaded functions, which can be helpful when you are
     migrating K&R C applications to ANSI C. To  generate  proto-
     types,  load  your code in source form, then issue the proto
     command:

     -> load -C const.c
     -> proto const.c
     Writing prototypes to a file. Output file name?
     const.proto

     You can load prototype files just like C  source  files.  To
     avoid  redefinition errors, load them before the correspond-
     ing source files.

  Type coercion
     You can load function prototypes in K&R  mode  and  in  ANSI
     mode.   The  only difference concerns type coercion of func-
     tion arguments.

     In K&R mode,  arguments  are  not  coerced,  they  are  only
     checked; this means that warning messages might be generated
     upon a type mismatch. Prototypes in K&R mode do  not  affect
     the  meaning of your program; they only provide extra check-
     ing.

     In ANSI mode, arguments are coerced; however, following ANSI
     specifications,  a  prototype  loaded in one module does not
     cause argument coercion in another module. This is a natural
     consequence  of  the  C  language's  "separate  compilation"
     model.

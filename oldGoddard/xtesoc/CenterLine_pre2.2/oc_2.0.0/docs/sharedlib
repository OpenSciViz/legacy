
NAME	
     shared libraries
      
     A shared library is a shared object file that is used as a
     library. At run time, a shared object can be linked to more
     than one executing program; all executing programs share
     access to a single copy of the object. Thus, using shared
     libraries can represent a significant savings in storage,
     but it may also reduce speed of processing.
     
     ObjectCenter supports shared libraries on all systems that
     provide them. Shared libraries typically link more quickly
     than static libraries in the ObjectCenter environment.
     
  Setting breakpoints in shared library functions 
     Keep in mind that when you are in component debugging mode,
     functions in shared libraries are not defined until your
     program references them. This means, for instance, that you
     cannot set breakpoints on a library function until you have
     linked or run your program.  In the meantime, you may get
     messages indicating that the function is undefined. Here is
     an example:
          
     Attaching: /usr/5lib/libc.sa.2.6 
     Attaching: /usr/5lib/libc.so.2.6 
     -> load ~/c_programs/sample.c 
     Loading: /s/users/jk/c_programs/sample.c 
     -> stop in printf 
     Cannot set stop or action on an undefined symbol: 'printf'. 
     -> link 
     Linking from '/usr/5lib/libc.sa.2.6' ... Linking completed. 
     -> stop in printf 
     stop (1) set at "/usr/5lib/libc.so.2.6", function printf(). 

     See your ObjectCenter Platform Guide for information about
     setting breakpoints in shared library functions while you
     are in process debugging mode.

RESTRICTIONS
     Using xref to cross-reference symbols in shared libraries
     is unreliable and changes depending on the execution state
     of your program. This is due to the way that symbols are
     linked from shared libraries. If you need accurate
     cross-reference information, load and link static
     libraries.

     ObjectCenter does not read any debugging information on
     shared libraries. Without debugging information on a file, 
     you are unable to perform certain debugging activities, 
     such as stepping through functions. For information about 
     what debugging techniques are possible on code without 
     debugging information, see the debugging entry.
     
     NOTE	See your ObjectCenter Platform Guide for 
		more information about the use of shared 
		libraries on your particular platform.


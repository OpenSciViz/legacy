NAME

 User-defined commands

     ObjectCenter provides two ways to define your own commands: 

     o  Using the User Defined dialog box from the ObjectCenter menu
	on the Main Window.

     o  Using X11 resources to add commands to the User Defined menu
	in the Project Browser.

     The commands that you define using X resources are different from 
     the commands that you define with the User Defined dialog box. In 
     general, we recommend that you use the User Defined dialog box
     to create commands, rather than using X resources. Commands created 
     using the GUI are stored in the .octruscmd file. 

     This entry describes how to use X11 resources to define your own
     commands. See the user buttons entry in the Man Browser or the 
     ObjectCenter User's Guide for information about the User Defined 
     dialog box.

     There are 20 commands available in the Project Browser; their 
     internal names are UserCmd1 through UserCmd20. ObjectCenter uses 
     the internal name to look up the resources that describe each 
     command. You can name the commands whatever you want to appear 
     on the pulldown menu.

  Examples of user-defined commands 
     To specify a user-defined command, you generally have to write
     only two lines per command, one for the label and another for the
     command.

     For instance, the following two lines in an ObjectCenter
     app-defaults file will create a user-defined button labeled "Find
     Locked Files" that runs listlocks:

     *ProjectBrowser.UserCmd1.label: Find Locked Files
     *ProjectBrowser.UserCmd1.command: listlocks

     If you do not have an ObjectCenter app-defaults file and you
     want to put this in your .Xdefaults file instead, start each
     line with "ObjectCenter". For instance:

     ObjectCenter*ProjectBrowser.UserCmd1...

     The next two lines will create a second button labeled "List
     Files" that will run ls -lg on all of the selected files:

     *ProjectBrowser.UserCmd2.label: List Files
     *ProjectBrowser.UserCmd2.command: ls -lg $files

     And here is one that will run ls -lg on all of the selected
     sources instead:

     *ProjectBrowser.UserCmd3.label: List Sources
     *ProjectBrowser.UserCmd3.command: ls -lg $sources

     The next example runs emacs on all of the selected sources.
     Note that you can use the useTerminalEmulator resource to
     avoid running an extra xterm:

     *ProjectBrowser.UserCmd4.label: Edit Sources
     *ProjectBrowser.UserCmd4.command: emacs $sources
     *ProjectBrowser.UserCmd4.useTerminalEmulator: False

     The following example runs listlocks to find all the locked
     files, and waits until it is done before allowing the
     Project Browser to go on.  The example also puts a menu
     separator bar just before this item.

     *ProjectBrowser.UserCmd5.label: List Locked Files
     *ProjectBrowser.UserCmd5.command: listlocks
     *ProjectBrowser.UserCmd5.waitUntilDone: True
     *ProjectBrowser.UserCmd5.addSeparator: True

  X resources for user-defined commands
     See the following list for a description of the resources to use
     when defining your own commands

     Resource Class:	Label                    
     Resource Name:	label                        
     Type:		String                             

	This is the string shown in the pulldown menu item for this
	command. You should specify this item for each command, but if
	you do not, it defaults to the value of the command line
	itself.

     Resource Class:	Command                  
     Resource Name:	command                      
     Type:		String             

        This is the command line you want to execute when the menu item
        is selected. The text you supply can include any of the special
        words listed in the following table.

     Resource Class:	WaitUntilDone            
     Resource Name:	waitUntilDone                
     Type:		Boolean            
     
        A value of True means the Project Browser should wait until the
        command has finished before continuing. A value of False means
        the Project Browser will spawn the specified command and then
        continue immediately. The default value is False, so you only
        need to specify this resource if
	you want the Project Browser to wait.

     Resource Class:	UseTerminalEmulator      
     Resource Name:	useTerminalEmulator          
     Type:		Boolean                 

        A value of True means the command must be run in a terminal
        emulator window, while a value of False means the command
        either does not require a window, or runs in its own window.
        The default value is True, so you only need to specify this
        item if you want to run a program like emacs that
	opens its own window.

     Resource Class:	TerminalEmulator         
     Resource Name:	terminalEmulator             
     Type:		String 

        This string identifies the terminal emulator to use when
        running the program. The terminal emulator specification may 
	include the following special words:

        $program  

        Replaced with the pathname of a shell script that is to be 
	executed. This script contains the full text of the expanded 
	command line.

        $command 

        Replaced with the text of the command label as shown in 
	the menu item. This might be used to set the window title  
	of the terminal emulator, for example.                          

        If the $program string is not found in the terminal 
	emulator specification, then the Project Browser automatically 
	appends the name of the shell script to the end of the string.    

        If you do not specify a value for TerminalEmulator, a
        suitable default value will be used, so you only need to
        specify this item if you want a custom terminal emulator
        applied to a particular command. The default value for the
        terminal emulator is:

        clxterm -T $command -n $command -sb -sl 1000 -e 

	You can change the default value by setting the 
        following resource:   
	                                                       
	ObjectCenter*ProjectBrowser.DefaultTerminalEmulator

     Resource Class:	AddSeparator          
     Resource Name:	addSeparator                  
     Type:		Boolean              	

        A value of True means insert a menu separator immediately
        before this item in the pulldown menu. A value of False means
        do not insert a separator. This is provided so you can build
        menus that are divided into categories of related commands,
        with separators between them. The default value is False, so 
	you only need to specify this item if you want a separator.

 The following table presents a list of the special words you can use 
 in a command Resource.


     Special Words Used in the command Resource
     -------------------------------------------------------------
     Special Word        What Replaces the Special Word           
     -------------------------------------------------------------
     Affects:	Project Browser
     -------------------------------------------------------------
     $pwd                ObjectCenter's current working directory.

     $files              A space-separated list of the full pathnames 
			 for all the files that are currently selected 
			 in the Project Browser.  

     $sources            Like $files, except that the names of  object 
			 files are replaced with the full pathnames of 
			 the corresponding source files, if the names 
			 are known. If an object  file is selected for 
			 which the source file is unknown, nothing is 
			 generated for that file. 

     $libraries          Replaced with a space-separated list of the 
			 full pathnames for all the libraries that 
			 are selected in the Project Browser.

     $command            Replaced with the text of the command label 
			 as shown in the menu item. When you execute a
			 user-defined command, the command string is
			 expanded into a shell script, which is then 
			 run using execvp, so your PATH and environment 
			 variable settings are  available from within 
			 user-defined commands.
     -----------------------------------------------------------------
     Affects:	Main Window 
     -----------------------------------------------------------------
     $filename           The filename of the file in the Source area, 
			 relative to ObjectCenter's current working 
			 directory.

     $filepath           The absolute filename of the file in the 
			 Source area.

     $first_selected_char  
			 The position of the first character selected on
                         $first_selected_line.  Character positions are 
			 numbered beginning with 1, and tabs are 
			 considered to be a single character. If no text 
			 is selected in the Source area, this keyword 
			 returns 0.

     $first_selected_line 
			 Starting line number of the Source area's 
			 current text selection. Lines are numbered 
			 beginning with 1. If no text is selected in the 
			 Source area, this keyword returns 0.

     $last_selected_char The position of the last character selected on
                         $last_selected_line.  Character positions are 
			 numbered beginning with 1, and tabs are 
			 considered to be a single character. If no text 
			 is selected in the Source area, this keyword 
			 returns 0.

     $last_selected_line Ending line number Source area's current text
                         selection. Lines are numbered beginning with 1. 
			 If no text is selected in the Source area, this 
			 keyword returns 0.
     ------------------------------------------------------------------
     Affects:	Selection
     -------------------------------------------------------------------
     $selection          Replaced with the current contents of the X11
                         PRIMARY selection, interpreted as a string. If 
			 the current selection is not available or is 
			 empty, $selection is replaced with an empty 
			 string.
     -------------------------------------------------------------------
     Affects:	Clipboard  
     -------------------------------------------------------------------
     $clipboard		 Replaced with the current contents of the 
			 CLIPBOARD selection. If the current selection 
			 is not available or is empty, $clipboard is 
			 replaced with an empty string. 
     -------------------------------------------------------------------

SEE ALSO
     rcss, user buttons, X resources











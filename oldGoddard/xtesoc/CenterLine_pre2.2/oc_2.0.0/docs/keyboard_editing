NAME

 Keyboard editing

     You can modify actions and translations to change the default 
     settings for keyboard editing. In this section we describe:

     o	Default keybindings

     o	Changing Motif defaults for keyboard editing

     o	OI_entry_field translations

     o	OI_multi_text translations

     An OI_entry_field object is a region for entering or displaying a
     single line of text. Visually, it consists of an optional label
     followed by the text entry area. 

     An OI_multi_text object is a viewport onto an underlying text
     structure consisting of zero or more lines. You can control the
     size of the viewport and manipulate the text displayed in the
     text object.

 Default keybindings
      By default, these shell-like and Emacs-like keybindings are 
      available in ObjectCenter:

        control-a               beginning of line
        control-e               end of line
        control-b               backward character
        control-f               forward character
        meta-b                  backward word
        meta-f                  forward word

        control-n               next line
        control-p               previous line

        control-d               delete next character
        control-u               delete to beginning of line
        control-k               delete to end of line
        control-w               delete previous word

      In Motif, some windows may use Meta-B and Meta-F as menu
      mnemonics, rendering them unavailable in text objects.

 Changing Motif defaults for keyboard editing
     If you are a Motif user and you want to change the default
     Motif settings for keyboard editing, you can use the actions
     and translations for the OI_entry_field and OI_multi_text
     objects listed in this section.

     For example, to add more emacs keyboard shortcuts, set the 
     translations for the underlying objects in your X resources file:

ObjectCenter*OI_entry_field.Translations: #override              \n\  
    ~Shift ~Meta ~Ctrl  <Key>Delete:    delete_previous_character()\n\    
    ~Shift ~Meta ~Ctrl  <Key>BackSpace: delete_previous_character()\n\    
            Meta        <Key>D:         delete_next_word()         \n\    

ObjectCenter*OI_multi_text.Translations:  #override              \n\  
    ~Shift ~Meta ~Ctrl  <Key>Delete:    delete_previous_character()\n\    
    ~Shift ~Meta ~Ctrl  <Key>BackSpace: delete_previous_character()\n\    
            Meta        <Key>D:         delete_next_word()         \n\    

     NOTE: In the following descriptions, selection means characters 
	   that have been highlighted and are in the X window 
	   selection property, and start-of-selection means the 
	   character position in the OI_entry_field object in which 
	   the selection starts. PRIMARY, SECONDARY and CLIPBOARD 
	   selections are the selections stored in the X properties 
	   of the same names.

  OI_entry_field Translation Functions

	backward_character( )
		Moves the cursor left one character.

	backward_view( )
		Moves the cursor one viewport to the left (if the 
		field is scrolled).

	backward_word( )
		Moves the cursor left one word. A word is 
		delineated by whitespace.

	beginning_of_line( )
		Moves the cursor to the beginning of the entry.

	cancel_select( )
		If a selection is in progress using the mouse,
		deselects all the currently selected characters
		and terminates the selection process.

	click_down( )
		Processes button down event in case click
		callback is set.  Does not do any selection
		processing (see select_start).

	click_up( )
		Processes button up event and dispatches to click
		callback if one is set. Does not do any text
		selection (see select_end).

	copy_clipboard( )
		Copies the currently selected text to the
		CLIPBOARD selection.

	copy_primary( )
		Inserts the PRIMARY selection at the current
		insertion point.

	cut_clipboard( )
		Copies the currently selected text to the
		CLIPBOARD selection and deletes the PRIMARY
		selection from its original source, if possible.

	cut_primary( )
		Inserts the PRIMARY selection at the current
		insertion point and deletes the PRIMARY selection
		from its original source, if possible.

	delete_all_characters( )
		Deletes all characters in the entry.

	delete_next_character( )
		Deletes the character to the right of the cursor.

	delete_next_word( )
		Deletes from the current insertion point through
		the whitespace at the end of the text containing
		the insertion point.

	delete_previous_character( )
		Deletes the character to the left of the cursor.

	delete_previous_word( )
		Deletes from the current insertion point up to,
		but not including, the whitespace at the
		beginning of the text containing the insertion
		point.

	delete_to_beginning_of_line( )
		Deletes all the characters to the left of the cursor.

	delete_to_end_of_line( )
		Deletes all the characters to the right of the cursor.

	end_of_line( )
		Moves the cursor to the end of the entry.

	extend_start( )
		Moves start-of-selection to the character under
		the mouse pointer.

	focus_in( )
		Sets input-focus to the object.

	focus_out( )
		Gives up input-focus.

	forward_character( )
		Moves the cursor right one character.

	forward_view( )
		Moves the cursor one viewport to the right (if
		the field can be scrolled).

	forward_word( )
		Moves the cursor right one word. A word is
		delineated by whitespace.

	input_character( )
		Inserts character at the cursor position.

	insert_mode( )
		Sets the character entry mode to OI_EF_INSERT.

	insert_selection( )
		Pastes text from the PRIMARY selection at the
		cursor position.

	key_select( )
		Marks the text from the anchor point to the
		cursor as the PRIMARY selection.

	move_insertion( )
		Sets the insertion point to the mouse pointer
		position; does not clear the current selection if
		it is in the same object.

	move_selection( )
		If the SECONDARY selection is active for this
		object, then copies the SECONDARY selection text
		to the cursor location and deletes the original
		selected text. Otherwise, if the PRIMARY
		selection is active for this object and was set
		using the mouse, copies the PRIMARY selection
		text to the cursor location and deletes the
		original selected text.

	newline( )
		End of entry. This causes the end-of-entry
		validation function to be called. If it returns
		OI_EF_ENTRY_CHK_OK, and an object has been
		registered via the set_next function, the new
		object obtains input-focus. Otherwise, the
		OI_entry_field object retains the input-focus.

	next_object( )
		Same as newline( ).

	next_tab_group( )
		Transfers the input-focus to the next tab group.

	paste_clipboard( ) 
		Deletes any currently selected text. The contents
		of the CLIPBOARD are then inserted in its place.
		If no text is currently selected, the contents of
		the CLIPBOARD are inserted at the insertion
		point.

	previous_object( )
		Completes the entry as if the Return key had been
		pressed and goes to the previous object, if one
		exists.

	previous_tab_group( )
		Transfers the input-focus to the previous tab group.

	replace_mode( ) 
		Sets the character entry mode to OI_EF_REPLACE.

	replace_with_default( )
		Replaces the current entry with the default entry.

	scroll_left( )
		Scrolls the text one full viewport to the left.

	scroll_left_edge( )
		Scrolls the text so the extreme left edge is visible.

	scroll_right( )
		Scrolls the text one full viewport to the right.

	scroll_right_edge( )
		Scrolls the text so the extreme right edge is visible.

	secondary_adjust( )
		Extends the selection to the new mouse pointer
		position. If Motif is being used, underlines the
		selection.

	secondary_end( )
		Completes the selection process and saves the
		selection in the SECONDARY selection.

	secondary_start( )
		Begins selecting text for inclusion in the
		SECONDARY selection.

	select_adjust( )
		Extends the selection to the mouse pointer location.

	select_all( )
		Marks the entire text as the PRIMARY selection.

	select_end( )
		Completes the selection process and saves the
		selection as the PRIMARY selection. Also, if the
		time between press and release is less than 500
		milliseconds, calls click callback if one is
		registered.

	select_start( )
		Begins selecting text for inclusion in the
		PRIMARY selection at the mouse pointer location.
		Moves the cursor to the pointer position. Saves
		the time to determine if a button click occurred.

	set_anchor( )
		Sets the anchor for selection at the current
		insertion point.

	take_focus( )
		Sets the focus to the OI_entry_field object.

	toggle_mode( )
		Toggles the character entry mode between
		OI_EF_INSERT and OI_EF_REPLACE. The initial
		character entry mode is OI_EF_INSERT.

	unselect_all( )
		Deselects any currently selected text. Clears the
		PRIMARY selection if it is owned by the process.


     OI_multi_text Translation Functions

	backward_character( )
		Moves the cursor backwards one character. Wraps
		to the previous line if necessary. Repositions
		the text in the viewport if necessary.

	backward_paragraph( )
		Moves the cursor backwards one paragraph.
		Positions the cursor at the beginning of the
		paragraph. Repositions the text in the viewport
		if necessary.

	backward_word( )
		Moves the cursor backwards one word. Positions
		the cursor at the beginning of the word.
		Repositions the text in the viewport if
		necessary.

	backward_view( )
		Moves the cursor to the left edge of the
		viewport. If the cursor is already at the left
		edge of the viewport, shifts the text to display
		the next view to the left and positions the
		cursor at the left edge of the viewport.

	beginning_of_file( )
		Moves the cursor to the beginning of the text.
		Repositions the text in the viewport if
		necessary.

	beginning_of_line( )
		Moves the cursor to the beginning of the current
		line.  Repositions the text in the viewport if
		necessary.

	beginning_of_pane( )
		Moves the cursor to the position in front of the
		first visible character in the first line
		currently visible in the viewport.

	cancel_select( )
		Cancels any selection that is in progress.
		Applies only while the mouse button is down.

	click_down( )
		Starts timing for a mouse button click.

	click_up( )
		Ends mouse button click timing and makes click
		callback.

	copy_clipboard( )
		Copies the PRIMARY selection (the currently
		selected text) to the CLIPBOARD selection.

	copy_primary( )
		Inserts contents of the PRIMARY selection at the
		cursor location.

	cut_clipboard( )
		Copies the PRIMARY selection to the CLIPBOARD
		selection, then deletes the PRIMARY selection.

	cut_primary( )
		Inserts the contents of the PRIMARY selection at
		the cursor location, then deletes the PRIMARY
		selection.

	delete_all_characters( )
		Deletes all the characters on the current line
		(the one the cursor is in), but leaves an empty
		line. Positions the cursor at the beginning of
		the line.

	delete_next_character( )
		If the cursor is at the end of the line, joins
		the following line with the current line;
		otherwise, deletes the character to the right of
		the cursor and closes up the space.

	delete_next_word( )
		If the cursor is at the end of the line, joins
		the following line with the current line;
		otherwise, deletes the word to the right of the
		cursor and closes up the space.

	delete_previous_character( )
		If the cursor is at the beginning of the line,
		joins the current line with the previous line;
		otherwise, deletes the character to the left of
		the cursor and closes up the space.

	delete_previous_word( )
		If the cursor is at the beginning of the line,
		joins the current line with the previous line;
		otherwise, deletes the word to the left of the
		cursor and closes up the space.

	delete_this_line( )
		Deletes the line the cursor is in and closes up
		the space.

	delete_to_beginning_of_line( )
		Deletes from the beginning of the current line to
		the cursor position and closes up the space.

	delete_to_end_of_line( )
		Deletes from the cursor position to the end of
		the current line and closes up the space.

	end_of_file( )
		Moves the cursor to the end of the text.
		Repositions the text in the viewport if
		necessary.

	end_of_line( )
		Moves the cursor to the end of the current line.
		Repositions the text in the viewport if
		necessary.

	end_of_pane( )
		Moves the cursor to the position after the last
		character on the last line currently visible.

	extend_start( )
		Begins a PRIMARY selection as for select_start,
		but initially includes all text from the mouse
		pointer to the cursor.

	focus_in( )
		Paints focus indicators to indicate that the 
		object has theinput-focus.

	focus_out( )
		Paints focus indicators to indicate that the 
		object does not	have the input-focus.

	forward_character( )
		Moves the cursor forward one character. Wraps to 
		the next line if necessary. Repositions the text 
		in the viewport	if necessary.

	forward_paragraph( )
		Moves the cursor forward one paragraph. Positions 
		the cursor at the beginning of the paragraph. 
		Repositions the	text in the viewport if necessary.

	forward_word( )
		Moves the cursor forward one word. Positions the 
		cursor at the beginning of the word. Repositions 
		the text in the	viewport if necessary.

	forward_view( )
		Moves the cursor to the right edge of the viewport. 
		If the cursor is already at the right edge of the 
		viewport, shifts the text to display the next view 
		to the right and position the cursor at the right 
		edge of the viewport.

	input_character( )
		Inserts the character at the current cursor position. 
		Works only for key events.

	input_convert( )
		Works only for key events. Passes the key event to 
		the language server for conversion. When the server 
		is done	converting (this may take several key events), 
		inserts the result at the cursor position.

	insert_mode( )
		Puts the object in insert mode. This means that any
		characters inserted are placed at the current cursor
		location and are inserted between the two adjacent
		characters.
	
	insert_selection(arg)
		Inserts text from an X selection at the cursor 
		position. If no	arguments are present, the PRIMARY 
		selection is used. Otherwise, arg is used as the name 
		of the selection to use.

	insert_string(arg)
		Inserts the string arg at the cursor location. Use 
		"\n" to	indicate newlines and the Tab key (not "\t") 
		to indicate tabs when specifying the string arg.

	key_select( )
		Puts all the text between the point marked by
		set_anchor to the current cursor location in the
		PRIMARY selection.

	move_insertion( )
		Moves the cursor to the location under the mouse
		pointer without clearing the PRIMARY selection.

	move_selection( )
		If the SECONDARY selection is active for this
		object, then copies the SECONDARY selection text
		to the cursor location and deletes the original
		selected text. Otherwise, if the PRIMARY
		selection is active for this object and was set
		using the mouse, copies the PRIMARY selection
		text to the cursor location and deletes the
		original selected text.

	newline( )   
		If an end-of-entry callback is registered, calls
		it. If no end-of-entry callback exists or if the
		end-of-entry callback returned
		OI_mt_entry_chk_ok, then inserts a new line after
		the line the cursor is in, and positions the
		cursor at the beginning of the new line.

	next_line( )
		Moves the cursor to the next line. Repositions the 
		text in	the viewport if necessary.

	next_object( )
		Sets the input-focus to the next object in the 
		focus chain (if	any).

	next_tab_group( )
		Sets the input-focus to the focus object in the 
		next tab group (if any).

	next_page( )                                 
		Scrolls the text so that the next page towards 
		the end of the text becomes visible.

	open_next_line( )                            
		Inserts a blank line following the line containing 
		the cursor. Rearranges the text accordingly.

	open_previous_line( )                        
		Inserts a blank line previous to the line containing 
		the cursor. Rearranges the text accordingly.

	paste_clipboard( )                           
		Inserts the text from the CLIPBOARD selection at the
		cursor location.

	previous_line( )                             
		Moves the cursor to the previous line. Repositions the 
		text in the viewport if necessary.

	previous_object( )                           
		Sets the input-focus to the previous object in the 
		focus chain.

	previous_tab_group( )                        
		Sets the input-focus to the focus object in the 
		previous tab group, if any.

	previous_page( )                             
		Scrolls the text so that the previous page towards 
		the beginning of the text becomes visible.

	process_return( )                            
		If an end-of-entry callback is registered, calls
		it. If no end-of-entry callback exists or if the
		end-of-entry callback returned
		OI_mt_entry_chk_ok, then positions the cursor at
		the beginning of the next line. If there is no
		next line (that is, the cursor is in the last
		line of text), inserts a new line at the end of
		the text and positions the cursor there.

	replace_mode( )                              
		Puts the object in replace mode. This means that
		any characters input replace the character to the
		right of the current cursor location. Any
		characters input at the end of the line are
		appended to the line.

	scroll_bottom( )                             
		Scrolls to the last line of the text.

	scroll_down( )                               
		Scrolls one full viewport towards the end of the text.

	scroll_left( )                               
		Scrolls one full viewport towards the left of the text.

	scroll_left_edge( )                          
		Scrolls to the first character in the line.

	scroll_right( )                              
		Scrolls one full viewport towards the right of the text.

                                                  
	scroll_right_edge( )                        
		Scrolls to the last character position in the
		object. This may be well past the last character
		in the current line.

	scroll_top( )                                
		Scrolls to the first line of the text.

	scroll_up( )                                 
		Scrolls one full viewport towards the beginning
		of the text.

	secondary_adjust( )                          
		Adjusts the SECONDARY selection to include all
		text from the secondary_start position to the
		position under the mouse pointer.

	secondary_end( )                             
		Completes the selection process and saves the
		selection as the SECONDARY selection. Also
		processes button-up event and calls click
		callback if one is registered.

	secondary_start( )                           
		Begins selecting text at the current location
		under the pointer for the SECONDARY selection.
		Underlines the selected text.

	select_adjust( )                             
		Adjusts the PRIMARY selection to include all text
		from the select_start position to the position
		under the pointer.

	select_all( )                                
		Puts the entire text in the PRIMARY selection.
		Highlights the selection.

	select_end( )                                
		Completes the selection process and saves the
		selection as the PRIMARY selection. Also
		processes button-up event and calls click
		callback if one is registered.

	select_line( )                               
		Selects the entire current line as the PRIMARY
		selection.

	select_start( )                              
		Begins selecting text at the mouse pointer
		location for the PRIMARY selection. Highlights
		the selection. Also processes button-down event
		in case click callbacks are registered.

	set_anchor( )                                
		Marks the current cursor location as the start
		for a keyboard-defined selection.

	start_input_conversion( )                    
		Sends all subsequent characters to the input
		server for conversion.

	stop_input_conversion( )                     
		Treats subsequent characters normally (quits
		sending characters to the input server).

	toggle_mode( )                               
		If the current mode is insert mode, changes it to
		replace mode. If the current mode is replace
		mode, changes it to insert mode.

	take_focus( )                                
		Sets the focus to the OI_multi_text object.

	unselect_all( )                              
		Unselects any currently selected text.


SEE ALSO
     keybind, X resources


















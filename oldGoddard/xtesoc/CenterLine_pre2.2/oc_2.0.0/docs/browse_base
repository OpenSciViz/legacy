 
NAME	
     browse_base

     displays base classes in the Workspace

    ----------------    
    |  cdm  | pdm  |
    +-------+------+
    |  yes  | no   |
    ----------------  

SYNOPSIS
     browse_base class_name

DESCRIPTION
     class_name		Ascii ObjectCenter: Displays all base
     			classes for class_name. Shows whether
			class_name was derived publicly or   
			privately.                           

SWITCHES
     -a			Ascii ObjectCenter: Displays the 
     			complete inheritance hierarchy for
    			the specified class item.        
     
USAGE
     Use the browse_base command to examine information about
     base classes or about the complete inheritance hierarchy for
     a given class.
     
     When displaying inherited base classes in the Workspace,
     browse_base uses indentation to indicate inheritance levels.
     For example, the following output from browse_base with the
     -a switch shows that the class Car is derived publicly from
     the class Land_vehicle, which in turn is derived publicly
     from the class Vehicle.
     
     -> browse_base -a Car 
     Base Classes: Land_vehicle <Public>
       Vehicle <Public>
		
SEE ALSO
     browse_class, browse_data_members, browse_derived,
     browse_friends, browse_member_functions, classinfo,
     list_classes

In component debugging mode, ObjectCenter provides both static
(compile-time) and run-time error checking. Static error-checking 
only occurs when you load source code.

Loading source files into the ObjectCenter environment (using
the load command) is the equivalent of compiling source files
outside the environment (using CC).  (Component debugging mode
is the ObjectCenter mode for debugging source and object files.
For information, see "overview".)

When the source code you are trying to load has compile
violations, the Error Browser button on the Main Window notifies
you by displaying "new errors".  Clicking on the Error Browser
button launches the Error Browser, a tool for displaying errors
and warnings. You can also invoke your editor to fix bugs and
reload and build your code directly from the Error Browser. You
can choose to suppress warnings (see "suppress").

ObjectCenter finds the following types of load-time errors:

o  I/O errors                   o  Illegal characters
o  Illegal constant formats     o  Illegal escape sequences
o  Lexical constant overflow    o  Improper comments

o  Preprocessing violations     o  Macro expansion violations
o  Syntax errors                o  Illegal statements
o  Illegal expressions          o  Undefined identifiers

o  Unused variables             o  Improper type specifiers
o  Declaration violations       o  Initialization violations
o  Redefinition violations      o  Linking violations

o  C++ ambiguity errors         o  Class-specific errors
o  Operator errors              o  Virtual function errors

Once you've loaded source and object files in component debugging
mode, you can issue the run command.  As ObjectCenter runs
the program, it automatically checks for the following run-time
errors and warnings.

o  Losing information during data conversions/assignments

o  Calling functions with the wrong number of arguments

o  Returning a pointer to an automatic or formal variable

o  Trying to free unallocated memory

o  Dereferencing a pointer that is out of bounds

o  Using a pointer that points to freed memory

o  Illegal index into an array

o  Division by zero

o  Long jump error

o  Signals

o  C++ casting errors

If ObjectCenter finds a run-time error or warning, it
automatically generates a break level and opens the Error Browser
displaying information about the error or warning.  To fix the
error or warning, you can automatically invoke your editor on the
file.

For information about managing errors, see the hardcopy
ObjectCenter User's Guide:

o  Chapter 4 - Fixing static errors, for load-time errors

o  Chapter 5 - Component debugging, for run-time errors

See Also
--------
load		for information about loading source and object code
overview	for an overview of component and process debugging modes
run		for information about executing main()
suppress	for information about suppressing warnings


#ifndef __table_h__
#define __table_h__

#ifdef __cplusplus
extern "C" {
#endif
    void makeTable(
#if defined(__STDC__) || defined(__cplusplus)
		   short *rows,
		   short *cols,
		   int   length,
		   short width,
		   short height,
		   short iwidth,
		   short iheight,
		   short margin
#endif
		   );
#if defined(__cplusplus)
};
#endif
#endif

#ifndef __x_h__
#define __x_h__

    /*---------------------------------------------*
     *                                             *
     *    Definitions for X support                *
     *                                             *
     *---------------------------------------------*/

    void openWindow(
#if defined(__STDC__) || defined(__cplusplus)
		    int windowX,
		    int windowY,
		    int width,
		    int height
#endif
		    );
    void closeWindow();
    void drawRect(
#if defined(__STDC__) || defined(__cplusplus)
		  int xOrigin,
		  int yOrigin,
		  int xExtent,
		  int yExtent,
		  int filled
#endif
		  );

    void drawCircle(
#if defined(__STDC__) || defined(__cplusplus)
                    short xOrigin,
		    short yOrigin,
		    short radius,
		    int   filled
#endif
		    );
    void drawCenterLine();
    void makeFilled();
#endif





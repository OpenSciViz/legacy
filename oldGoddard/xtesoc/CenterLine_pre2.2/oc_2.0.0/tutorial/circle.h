#ifndef __CIRCLE_H__
#define __CIRCLE_H__

#include "shapes.h"

extern Point DefaultPt;

class Circle: public DrawableShape {
private:
  int radius;
public:
  Circle(Point &origin=DefaultPt, int itsRadius=30);
  Circle(Circle&);
  virtual void draw();
};
#endif

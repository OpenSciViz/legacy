#ifndef __RECT_H__
#define __RECT_H__

#include "shapes.h"

class Rectangle : public DrawableShape {
private:
  Point extent;
public:
  Rectangle(Point &origin, Point &theExtent);
  virtual void draw();
};

#endif

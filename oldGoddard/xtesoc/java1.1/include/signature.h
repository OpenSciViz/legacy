/*
 * @(#)signature.h	1.10 96/11/23
 * 
 * Copyright (c) 1995, 1996 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of Sun
 * Microsystems, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Sun.
 * 
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 * 
 * CopyrightVersion 1.1_beta
 * 
 */

/*
 * The keyletters used in type signatures
 */

#ifndef _SIGNATURE_H_
#define _SIGNATURE_H_

#define SIGNATURE_ANY		'A'
#define SIGNATURE_ARRAY		'['
#define SIGNATURE_BYTE		'B'
#define SIGNATURE_CHAR		'C'
#define SIGNATURE_CLASS		'L'
#define SIGNATURE_ENDCLASS	';'
#define SIGNATURE_ENUM		'E'
#define SIGNATURE_FLOAT		'F'
#define SIGNATURE_DOUBLE        'D'
#define SIGNATURE_FUNC		'('
#define SIGNATURE_ENDFUNC	')'
#define SIGNATURE_INT		'I'
#define SIGNATURE_LONG		'J'
#define SIGNATURE_SHORT		'S'
#define SIGNATURE_VOID		'V'
#define SIGNATURE_BOOLEAN	'Z'

#define SIGNATURE_ANY_STRING		"A"
#define SIGNATURE_ARRAY_STRING		"["
#define SIGNATURE_BYTE_STRING		"B"
#define SIGNATURE_CHAR_STRING		"C"
#define SIGNATURE_CLASS_STRING		"L"
#define SIGNATURE_ENDCLASS_STRING	";"
#define SIGNATURE_ENUM_STRING		"E"
#define SIGNATURE_FLOAT_STRING		"F"
#define SIGNATURE_DOUBLE_STRING       	"D"
#define SIGNATURE_FUNC_STRING		"("
#define SIGNATURE_ENDFUNC_STRING	")"
#define SIGNATURE_INT_STRING		"I"
#define SIGNATURE_LONG_STRING		"J"
#define SIGNATURE_SHORT_STRING		"S"
#define SIGNATURE_VOID_STRING		"V"
#define SIGNATURE_BOOLEAN_STRING	"Z"



#endif /* !_SIGNATURE_H_ */

/*
 * @(#)mutex_md.h	1.6 96/11/23
 * 
 * Copyright (c) 1995, 1996 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of Sun
 * Microsystems, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Sun.
 * 
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 * 
 * CopyrightVersion 1.1_beta
 * 
 */

/*
 * Interface to mutex HPI implementation for Solaris
 */

#ifndef _MUTEX_MD_H_
#define _MUTEX_MD_H_

#include <synch.h>

/*
 * Generally, we would typedef mutex_t to be whatever the system
 * supplies.  But Solaris gives us mutex_t directly.
 */

int    mutexInit(mutex_t *);
int    mutexDestroy(mutex_t *);
int    mutexLock(mutex_t *);
bool_t mutexLocked(mutex_t *);
int    mutexUnlock(mutex_t *);

#endif /* !_MUTEX_MD_H_ */

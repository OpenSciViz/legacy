void calback(double JD, int* id, int* mm, int* iyyy, float* UT);
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#define IGREG 2299161

void
main(int argc, char* argv[])
{
   double mjd, jd;
   
   mjd = atof(argv[1]);
   
   // -28980 = JD 1900.0 -2444000.0 (TJD)
   jd = mjd + 2444000.0;

   int day, month, year; 
   float UT;
   
   calback(jd, &day, &month, &year, &UT);
   
   int  hours, mins, secs;
   hours = (int)(UT);
   mins  = (int)(UT*60.0 - hours*60.0);
   secs  = (int)(UT*60.0*60.0 - hours*60.0*60.0 - mins*60.0);
   printf("%02d %02d %04d %02d %02d %02d\n", month, day, year, hours, mins, secs);

}

void calback(double JD, int* id, int* mm, int* iyyy, float* UT)
{
  long julian;
  long ja,jalpha,jb,jc,jd,je;
  float utcalc;

  utcalc = (float) (JD+0.5 - (int)(JD+0.5));
  *UT = utcalc*24.0;

  julian = (long) (JD+0.5); /* ignore UT effect right now */
  printf("%d\n", julian);

  if (julian >= IGREG)
    {
      jalpha = (long)(((float) (julian-1867216)-0.25)/36524.25);
      ja = julian + 1 + jalpha - (long) (0.25*jalpha);
    }
  else
    ja = julian;
  printf("%d\n", ja);
  jb = ja+1524;
  jc = (long)(6680.0+((float) (jb-2439870)-122.1)/365.25);
  jd = (long)(365*jc+(0.25*jc));
  je = (long)((jb-jd)/30.6001);
  *id = jb-jd-(long) (30.6001*je);
  *mm=je-1;
  printf("%d, %d, %d, %d, %d, %d\n", jb, jc, jd, je, *id, *mm);
  if (*mm > 12) *mm-=12;
  *iyyy=jc-4715;
  if (*mm > 2) --(*iyyy);
  if (*iyyy <= 0)--(*iyyy);
}



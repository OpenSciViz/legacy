# pocessEventFile.awk
#/ROSTER_START/ { gsub(/\)/,"", $2); print "echo start_schedule\ntjd_utc ", $2; };
#/ROSTER_END/   { gsub(/\)/,"", $2); print "echo end_schedule\ntjd_utc ", $2; };
#/\(EVENT /     { gsub(/\)/,"", $4); print "echo ", $2, " ", $4, "  \ntjd_utc ", $3; }
gawk -f processEventFile.awk sim3-remote-sts.events >! x
csh x >! xx
gawk '/-00/ { printf("%s ", $0); } /1995/ { print $0; }' xx >! xxx
gawk 'NF>=7 { day = $4+31; printf("%s:%s:%s:%s:%s %s %s\n", $5, day, $6, $7, $8, $1, $2); }' xxx | sort >! xxxx
# BEGIN { eventName["Begin_slew"] = "in_slew"; eventName["End_slew"] = "out_slew";
#         eventName["Enter_SAA"]  = "in_saa";  eventName["Exit_SAA"] = "out_saa";
#         eventName["Enter_occultation"] = "in_occult";eventName["Exit_occultation"] = "out_occult" }
# { print $1, eventName[$2], $3; }
gawk -f renameEvents.awk xxxx >! xxxxx

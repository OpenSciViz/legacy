BEGIN { numObs = 1; lastOb = ""; }
{
   if ($2 ~ /Enter_SAA/) {
      x = $3 - 0.0001;
      printf(".graph element append SAA { %f 0 }\n", x);
      printf(".graph element append SAA { %s %d }\n", $3, numObs);
   } 
   if ($2 ~ /Exit_SAA/) {
      x = $3 - 0.0001;
      printf(".graph element append SAA { %f %d }\n", x, numObs);
      printf(".graph element append SAA { %s 0 }\n", $3);
   }
   if ($2 ~ /Enter_occultation/) {
      x = $3 - 0.0001;
      printf(".graph element append OCCULT { %f 0 }\n", x);
      printf(".graph element append OCCULT { %s %d }\n", $3, numObs);
   }
   if ($2 ~ /Exit_occultation/) {
      x = $3 - 0.0001;
      printf(".graph element append OCCULT { %f %d }\n", x, numObs);
      printf(".graph element append OCCULT { %s 0 }\n", $3);
   }
   if ($2 ~ /Begin_slew/) {
      x = $3 - 0.0001;
      printf(".graph element append SLEW { %f 0 }\n", x);
      printf(".graph element append SLEW { %s %d }\n", $3, (numObs+1));
   }
   if ($2 ~ /End_slew/) {
      x = $3 - 0.0001;
      printf(".graph element append SLEW { %f %d }\n", x, numObs);
      printf(".graph element append SLEW { %s 0 }\n", $3);
    }
    if ($1 ~ /\(EVENT/) {
       if ($4 != lastOb) {
          numObs++; lastOb = $4;
	  gsub(/\)/,"", $4);
	  printf(".graph element create OBS%s -symbol circle -bg powderblue -fg red -linewidth 2\n", $4);
	  printf(".graph element append OBS%s { %f %d }\n", $4, $3, numObs);
	  printf(".graph element append OBS { %f %d }\n",  $3, numObs);
       }
    }
}

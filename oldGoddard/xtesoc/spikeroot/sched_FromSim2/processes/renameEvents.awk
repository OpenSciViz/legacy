 BEGIN { eventName["Begin_slew"] = "in_slew"; eventName["End_slew"] = "out_slew";
         eventName["Enter_SAA"]  = "in_saa";  eventName["Exit_SAA"] = "out_saa";
         eventName["Enter_occultation"] = "in_occult"; eventName["Exit_occultation"] = "out_occult" }
 { print $1, eventName[$2], $3; }

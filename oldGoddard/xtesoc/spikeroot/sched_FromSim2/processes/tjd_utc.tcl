proc tjd_utc {tjd} {
   set jd [expr $tjd + 2444000.0]
   #puts "$jd" 
  
   set utcalc [expr (($tjd + .5) - floor($tjd + .5))]
   set UT     [expr $utcalc*24.0]
   #puts "$utcalc $UT"
    
   set julian [expr $jd+.5]
   #puts "$julian"
   
   set IGREG 2299161
   
   if {$julian >= $IGREG} {
      set jalpha [expr floor((($julian-1867216)-.25)/36524.25)]
      set ja     [expr $julian + 1 + $jalpha - floor(.25*$jalpha)]
   } else {
      set ja     $julian
   }
   #puts "$ja"
   set jb  [expr floor($ja + 1524)]
   set jc  [expr floor(6680.0+((($jb-2439870)-122.1)/365.25))]
   set jd  [expr floor(365*$jc+(.25*$jc))]
   set je  [expr floor(($jb-$jd)/30.6001)]
   set id  [expr $jb - $jd - floor(30.6001*$je)]
   set mm  [expr $je - 1.0]
   #puts "$jb $jc $jd $je $id $mm - $jb2 $jd2"
   if {$mm > 12.0} { set mm [expr $mm - 12.0] }
   set iyyy [expr $jc - 4715]
   if {$mm > 2} { set iyyy [expr $iyyy - 1] }
   if {$iyyy <= 0} { set iyyy [expr $iyyy - 1] }

   set hours [expr floor($UT)]
   set mins  [expr floor($UT*60.0 - $hours*60.0)]
   set secs  [expr floor($UT*60.0*60.0 - $hours*60.0*60.0 - $mins*60.0)]
  
   list $mm $id $iyyy $hours $mins $secs
}

proc lisp_to_tcl {fileName} {
   exec gema -f lisp_tcl.gema $fileName $fileName.tcl
   source $fileName.tcl
}

proc gather_events {x} {
   set events {};
   foreach y $x {
      if {[lindex $y 0] == "EVENT"} {
	 lappend events $y
      }
   }
   return $events
}

proc gather_slew_events {x} {
   set events {}
   foreach y $x {
      if {[lindex $y 1] == "Begin_slew" ||
	  [lindex $y 1] == "End_slew"} {
          lappend events $y
      }
    }
    return $events
}

proc gather_occult_events {x} {
   set events {}
   foreach y $x {
      if {[lindex $y 1] == "Enter_occultation" ||
	  [lindex $y 1] == "Exit_occultation"} {
          lappend events $y
      }
   }
   return $events
}

proc gather_saa_events {x} {
   set events {}
   foreach y $x {
      if {[lindex $y 1] == "Enter_SAA" ||
	  [lindex $y 1] == "Exit_SAA"} {
	  lappend events $y
      }
   }
   return $events
}

proc sort_events {x} {
   
}
			       
#proc tcl_to_lisp {fileName source} {
#    if {[catch {open $fileName w} fileId]} {
#       error "Cannot open $fileName"
#    } else {
#       set str [join $source]
#       while {[gets $fileId line] >= 0} {
#	  # replace # -> ;
#	  # replace lisp { -> {
#	  # replace {quote -> '{
#	  # replace { -> (
#	  # replace } -> )
#       }
#       close $fileId
#    }
#}

cp auto.sched temp
gawk    '/constraint-files/,/locked-vars/ 
         { 
	   gsub(/11/," xt-11"); 
	   gsub(/[0-9]xt-11/, "\x22 \x22xt-11"); 
	   gsub(/   xt-11/,"   \x22xt-11");
	   $0 = $0 "\x22";
	  }


/*  This is a julian date routine, converting any calender date to
    Julian.  This is the core algorithm, see jmain.c for more */
/* it is taken from numerical recipes */
#include <stdlib.h>
#include <stdio.h>

#include <math.h>
#define IGREG (15+31L*(10+12L*1582))
float julian(int id, int mm, int iyyy, float UT);

inline double floor(double x) { int y = (int)x; return((double)x); }


void
main(int argc, char* argv[])
{
   int    day, month, year, hour, min, sec;
   float  UT;
   
   month = atol(argv[1]);
   day   = atol(argv[2]);
   year  = atol(argv[3]);
   hour  = atol(argv[4]);
   min   = atol(argv[5]);
   sec   = atol(argv[6]);
   UT = sec + min*60.0 + hour*60.0*60.0;
   printf("%d %d %d %d %d %d %f\n", month, day, year, hour, min, sec, UT);
   printf("%f\n", (julian(day, month, year, UT)-2444000.0));
}

float julian(int id, int mm, int iyyy, float UT)
{
  long jul;
  int ja, jy=iyyy, jm;
  float JD;
//  double floor();

  if (jy == 0)
    {
      printf("error-- there is no year zero, adjusting to year 1\n");
      jy = 1;
    }
  if (jy < 0) ++jy;
  if (mm > 2)
    jm = mm + 1;
  else
    {
      --jy;
      jm=mm+13;
    }
  jul = (long) (floor(365.25*jy)+floor(30.6001*jm)+id+1720995);
  if (id+31L*(mm+12L*iyyy) >= IGREG)
    {
      ja = (int)(0.01*jy);
      jul += 2-ja+(int) (0.25*ja);
    }
  
  JD = (float)jul - 0.5 +  UT/24.0;
  
  return(JD);
  
}							/* end main */


BEGIN { feb011995 = 2449749 - 2400000.5; print feb011995;
        eventTrans["Enter_SAA"] = "in_saa";
	eventTrans["Exit_SAA"]  = "out_saa";
	eventTrans["Enter_occultation"] = "in_occult";
	eventTrans["Exit_occultation"] = "out_occult";
	eventTrans["Begin_slew"] = "start_slew";
	eventTrans["End_slew"] = "end_slew";
	eventTrans["Begin_ASM_dwell_seq"] = "dwell";
}
$1 ~ /EVENT/ {
  type = eventTrans[$2];
  mjd  = $3;
  mID  = $4;
  gsub(/\)/, "", mID);
  s = "day  = int(mjd - feb011995);"
  s = " fracDay = (mjd - feb11994) - day;"
  s = "fracDay *= 25;"
  s = sprintf("tjd_utc %s", mjd);
  s  | getline out; close(s);
  print type, out, mID; 
}

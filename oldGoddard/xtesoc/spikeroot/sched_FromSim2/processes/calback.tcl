set tcl_precision 17

proc calback_tjd { JD } {
   global IGREG
   set IGREG 2299161
   set JD [expr $JD + 2444000.0]
   set utcalc [expr ($JD+0.5) - int($JD+0.5)]
   set UT     [expr $utcalc*24.0]
   
   set julian [expr int($JD+0.5)]
   
   if { $julian >= $IGREG } {
      set jalpha [expr int(((($julian-1867216)-0.25)/36524.25))]
      set ja     [expr int($julian + 1 + $jalpha - int(0.25*$jalpha))]
   } else {
      set ja     $julian
   }
   set jb     [expr $ja + 1524]
   set jc     [expr int((6680.0+(($jb-2439870)-122.1)/365.25))]
   set jd     [expr int(365*$jc+(0.25*$jc))]
   set je     [expr int(($jb-$jd)/30.6001)]
   set id     [expr $jb-$jd-int(30.6001*$je)]
   set mm     [expr int($je - 1)]
   if {$mm > 12} { set mm [expr int($mm - 12)] }
   set iyyy   [expr int($jc - 4715)]
   if {$mm > 2}  { set iyyy [expr int($iyyy - 1)] }
   if {$iyyy <= 0} { set iyyy [expr int($iyyy - 1)] }
   set hours  [expr int($UT)]
   set mins   [expr int($UT*60.0 - $hours*60.0)]
   set secs   [expr int($UT*60*60 - $hours*60.0*60.0 - $mins*60.0)]
   lappend ret $iyyy; lappend ret $mm; lappend ret $id; lappend ret $hours; lappend ret $mins; lappend ret $secs;
}

{
  if ($6 ~ /observation/) { continue; }
  if ($6 ~ /macros/)      { continue; }
  if ($6 ~ /EDS/)         { continue; }
  if ($6 ~ /eds/)         { continue; }
  if ($6 ~ /ACS/)         { continue; }
  
   base = $1;
   ut   = $3/24.0 + $4/(24.0*60.0) + $5/(24.0*60.0*60.0);
   base += ut;
   
   printf("  (EVENT %s %s %s)\n", $6, base, $7);
 
}

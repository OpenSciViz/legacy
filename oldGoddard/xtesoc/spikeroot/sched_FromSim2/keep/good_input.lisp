(setf *XTE-OVERSUBSCRIPTION-FACTOR* 0.5)
(setf *XTE-TARG-FILES* '("xt-12345-01-00-00" "xt-12345-01-01-00"
                         "xt-12345-01-58-00" "xt-12345-01-59-00" ) )
(setf *XTE-CONSTRAINT-FILES* '("xt-12345-01-56-00" "xt-12345-01-49-00"
                               "xt-12345-01-00-00" ) )
(DEFUN RECREATE-XTE-SCHEDULE (&key
    (name             "lts-fri-morn" )
    (targ-files       '("xt-12345-01-00-00" "xt-12345-01-01-00"
                        "xt-12345-01-58-00" "xt-12345-01-59-00" ) )
    (constraint-files '("xt-12345-01-56-00" "xt-12345-01-49-00"
                        "xt-12345-01-00-00" ) )
    (locked-vars      'NIL)
    (ignored-vars     'NIL)
    (assigned-vars    '((12345-01-59-00 0) (12345-01-58-00 7)
                        (12345-01-01-00 14) (12345-01-00-00 21) ) )
    (commit-times '(
          (12345-01-59-00 6083.54 NIL)
          (12345-01-00-00 6104.54 NIL)
      ))
    (locked-bins       'NIL)
    )
  (xte-init-prep-and-load
    targ-files
    :constraint-files constraint-files
    :name name)
  (dolist (v locked-vars)
    (lock-var (var-named (obs-csp *xte-long-term-schedule*) (first v)) (second v) ) )
  (dolist (v ignored-vars)
    (ignore-var (var-named (obs-csp *xte-long-term-schedule*) v) :permanent t))
  (dolist (v assigned-vars)
     (assign-value (var-named (obs-csp *xte-long-term-schedule*) (first v)) (second v) ) )
  (dolist (bin locked-bins)
     (lock-bin *xte-long-term-schedule* bin))
  (dolist (s commit-times)
    (set-long-term-commitment-time (first s) (second s))
    (set-short-term-commitment-time (first s) (third s) ) )
 ) 

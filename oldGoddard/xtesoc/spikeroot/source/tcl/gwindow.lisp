;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; gwindow.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: gwindow.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.3  1992/03/20  12:39:22  johnston
;;; fixed plot-interval-list to default on val if not present
;;;
;;; Revision 1.2  1991/12/01  00:21:18  johnston
;;; minor fixes
;;;
;;; Revision 1.1  1991/11/26  15:40:11  johnston
;;; Initial revision
;;;
;;;
;;; generalized graphics windows, for xlib, mac, and postscript
;;;
;;; MDJ 11Nov91
;;;
;;; This file contains general classes and methods.  Others specialize for
;;; specific window types.

;;; TODO
;;; - ?? write xy page translate/inverter for use in change-class
;;;   i.e. to transform history list 
;;; - rounding is NOT done by gwindow base class:  needs to be overridden
;;;   for window types that require it.  See mac-gwindow for an example.
;;;   Also required for X-windows.
;;; - AUTOSCALE!!


;;; ======================================================================
;;;                                                                GLOBALS

;;; plotting symbols:  list of (dx1 dy1 dx2 dy2) for line segments relative
;;; to central point.

(defvar *plus-symbol*  '((-1 0 1 0)(0 1 0 -1)))
(defvar *cross-symbol* '((-1 -1 1 1)(-1 1 1 -1)))
(defvar *star-symbol*  '((-1 -1 1 1)(-1 1 1 -1)(-1 0 1 0)(0 1 0 -1)))

;;; ======================================================================
;;;                                                        CLASS:  GWINDOW

(defclass GWINDOW ()
  (
   ;; --------------------------------------------------

   ;; remember last x and y page positions for moveto, lineto commands
   ;; NOTE that these fields are only meaningful during recording, not
   ;; playback!
   (last-x :initform nil)
   (last-y :initform nil)

   (font-name :initform "Helvetica")
   (font-size :initform 9)
   
   ;; list of forms specifying drawing: all coordinates here are
   ;; page coordinates.  forms are:
   ;; (fcn-name arg1...)
   ;; to do drawing, each form is applied as follows:
   ;; (apply fcn-name (cons drawing-destination (rest form)))
   (history :initform nil)

   ;; where to playback the drawing:  for a postscript window this is
   ;; a stream; for a mac or xlib view it is the view object
   (drawing-destination :initform nil :initarg :drawing-destination)

   ;; --------------------------------------------------
   (xpage-min)
   (xpage-max)
   (xpage-range)
   (ypage-min)
   (ypage-max)
   (ypage-range)
   
   ;; where within the drawing rectangle the window is located
   ;; coordinates run 0 to 1 in x and y
   (xmin)
   (xmax)
   (xrange)
   (ymin)
   (ymax)
   (yrange)
   
   ;; values corresponding to xmin, xmax
   (tmin)
   (tmax)
   (trange)
   ;; values corresponding to ymin, ymax
   (vmin)
   (vmax)
   (vrange)
   
   (current-page-number)
   ))

;;; ======================================================================
;;;                                                  CLASS:  IMMED-GWINDOW
;;;
;;; subtype of gwindow, only difference is that, instead of buffering
;;; graphics calls to a history and then replaying it, they are executed
;;; immediately
;;; Useful for postscript, xlib types

(defclass IMMED-GWINDOW (GWINDOW)
  ())


;;; ----------------------------------------------------------------------
;;; initialization

(defmethod INITIALIZE-INSTANCE ((self gwindow)
                                &rest initargs)
  (call-next-method)
  ;; standard initialization
  (with-slots
    (xpage-min xpage-max xpage-range ypage-min ypage-max ypage-range) self
    (setf xpage-min 0)
    (setf xpage-max 600)
    (setf xpage-range (- xpage-max xpage-min))
    (setf ypage-min 0)
    (setf ypage-max 200)
    (setf ypage-range (- ypage-max ypage-min)))
  (with-slots
    (xmin xmax xrange ymin ymax yrange) self
    (setf xmin 0.2)
    (setf xmax 0.95)
    (setf xrange (- xmax xmin))
    (setf ymin 0.2)
    (setf ymax 0.95)
    (setf yrange (- ymax ymin)))
  (with-slots
    (tmin tmax trange vmin vmax vrange current-page-number) self
    (setf tmin 0)
    (setf tmax 100)
    (setf trange 100)
    (setf vmin 0)
    (setf vmax 100)
    (setf vrange 100)
    (setf current-page-number 1))
  (setf (slot-value self 'drawing-destination)
        (getf initargs :drawing-destination nil))
  )


;;; ----------------------------------------------------------------------
;;; coordinate setup and conversion

(defmethod t-min ((g gwindow)) 
  (slot-value g 'tmin))

(defmethod t-max ((g gwindow)) 
  (slot-value g 'tmax))

(defmethod set-page-loc-of-drawing-rect ((g gwindow) x1 x2 y1 y2)
  (with-slots
    (xpage-min xpage-max xpage-range ypage-min ypage-max ypage-range) g
    
    (setf xpage-min  x1
          xpage-max  x2
          ypage-min  y1
          ypage-max  y2)
    (setf xpage-range (- xpage-max xpage-min))
    (setf ypage-range (- ypage-max ypage-min))))

(defmethod set-loc-of-drawing-window ((g gwindow) x1 x2 y1 y2)
  (with-slots
    (xmin xmax xrange ymin ymax yrange) g
    (setf xmin x1 xmax x2 ymin y1 ymax y2)
    (setf xrange (- x2 x1))
    (setf yrange (- y2 y1))))
  
(defmethod set-x-window-scale ((g gwindow) t1 t2)
  (with-slots
    (tmin tmax trange) g
    (setf tmin t1 tmax t2)
    (setf trange (- t2 t1))))

(defmethod set-y-window-scale ((g gwindow) v1 v2)
  (with-slots
    (vmin vmax vrange) g
    (setf vmin v1 vmax v2)
    (setf vrange (- v2 v1))))
  
(defmethod current-page-number ((g gwindow))
  (slot-value g 'current-page-number))

(defmethod increment-page-number ((g gwindow))
  (setf (slot-value g 'current-page-number)
        (1+ (slot-value g 'current-page-number))))

(defmethod reset-page-number ((g gwindow))
  (setf (slot-value g 'current-page-number) 1))

;; convert from t to x w.r.t draw rect
(defmethod t-to-x ((g gwindow) t1)
  (with-slots (xmin xrange tmin trange) g
    (+ xmin (* xrange (/ (- t1 tmin) trange)))))

;; convert from v to y w.r.t draw rect
(defmethod v-to-y ((g gwindow) v1)
  (with-slots (ymin yrange vmin vrange) g
    (+ ymin (* yrange (/ (- v1 vmin) vrange)))))

;; convert from x to page horiz
(defmethod x-to-page-horiz ((g gwindow) x)
  (with-slots (xpage-min xmin xpage-range xrange) g
    (+ xpage-min 
       (* xmin xpage-range)
       (* (* xrange xpage-range)
          (/ (- x xmin) xrange)))))

;; convert from y to page vert
(defmethod y-to-page-vert ((g gwindow) y)
  (with-slots (ypage-min ymin ypage-range yrange) g
    (+ ypage-min
       (* ymin ypage-range)
       (* (* yrange ypage-range)
          (/ (- y ymin) yrange)))))
  
  ;; convert from t to page x
(defmethod t-to-page-x ((g gwindow) t1)
  (x-to-page-horiz g (t-to-x g t1)))
  
  ;; convert from v to page y
(defmethod v-to-page-y ((g gwindow) v1)
  (y-to-page-vert g (v-to-y g v1)))

;; return user (page locations) of rectangle boundaries
(defmethod y-rect-top ((g gwindow))
  (slot-value g 'ypage-max))

(defmethod y-rect-bottom ((g gwindow))
  (slot-value g 'ypage-min))

(defmethod x-rect-left ((g gwindow))
  (slot-value g 'xpage-min))

(defmethod x-rect-right ((g gwindow))
  (slot-value g 'xpage-max))

;; return user (page locations) of window boundaries
(defmethod y-window-top ((g gwindow))
  (y-to-page-vert g (slot-value g 'ymax)))

(defmethod y-window-bottom ((g gwindow))
  (y-to-page-vert g (slot-value g 'ymin)))

(defmethod x-window-left ((g gwindow))
  (x-to-page-horiz g (slot-value g 'xmin)))

(defmethod x-window-right ((g gwindow))
  (x-to-page-horiz g (slot-value g 'xmax)))


;; debugging: show values of current parameters
(defmethod show-rect-params ((g gwindow))
  (with-slots
    (xpage-min xpage-max ypage-min ypage-max
               xmin xmax ymin ymax
               tmin tmax vmin vmax) g
  (format t "~%on page (LRTB): ~a ~a ~a ~a" 
          xpage-min xpage-max ypage-min ypage-max)
  (format t "~% window (LRTB): ~a ~a ~a ~a" xmin xmax ymin ymax)
  (format t "~% t values (min max): ~a ~a" tmin tmax)
  (format t "~% v values (min max): ~a ~a" vmin vmax)))

;;; ----------------------------------------------------------------------
;;; history management methods

(defmethod clear-gwindow-history ((g gwindow))
  (setf (slot-value g 'history) nil)
  (setf (slot-value g 'last-x) nil)
  (setf (slot-value g 'last-y) nil))

(defmethod play-gwindow-history ((g gwindow))
  ;; need to reverse
  (with-slots (history) g
    (dolist (history-form (reverse history))
      (apply (first history-form)
             (cons g (rest history-form))))))

;;(defmethod transform-gwindow-history ((g gwindow)...)  )


;;; ----------------------------------------------------------------------
;;; drawing primitive methods for base class: these must be specifically
;;; implemented for each subclass of the base class
;;; these methods actually DO the drawing during history playback
;;; note:  all x-y positions are page coordinates

(defmethod gwindow-prolog ((g gwindow))
  )

(defmethod gwindow-wrapup ((g gwindow))
  )

(defmethod gwindow-nextpage ((g gwindow))
  )

(defmethod gwindow-moveto ((g gwindow) x y)
  (declare (ignore x y)))

(defmethod gwindow-lineto ((g gwindow) x y linewidth dash1 dash2)
  (declare (ignore x y linewidth dash1 dash2)))

(defmethod gwindow-line ((g gwindow) x1 y1 x2 y2 linewidth dash1 dash2)
  (declare (ignore x1 y1 x2 y2 linewidth dash1 dash2)))

(defmethod gwindow-polyline ((g gwindow) xy-list 
                             linewidth dash1 dash2
                             symbol symbol-scale)
  (declare (ignore xy-list linewidth dash1 dash2
                   symbol symbol-scale)))

(defmethod gwindow-polygon ((g gwindow) xy-list 
                             frame fill linewidth dash1 dash2)
  (declare (ignore xy-list frame fill linewidth dash1 dash2)))

(defmethod gwindow-polyseg ((g gwindow) seg-list 
                             linewidth dash1 dash2)
  (declare (ignore seg-list linewidth dash1 dash2)))

(defmethod gwindow-font ((g gwindow) font-name font-size)
  (declare (ignore font-name font-size)))

(defmethod gwindow-leftshow ((g gwindow) x y string)
  (declare (ignore x y string)))

(defmethod gwindow-rightshow ((g gwindow) x y string)
  (declare (ignore x y string)))

;;; ----------------------------------------------------------------------
;;; methods for recording drawing commands:  they don't actually do the
;;; drawing, this is done during history playback

(defmethod record-prolog ((g gwindow))
  (push (list 'gwindow-prolog) (slot-value g 'history))
  nil)

(defmethod record-prolog ((g immed-gwindow))
  (gwindow-prolog g))


(defmethod record-wrapup ((g gwindow))
  (push (list 'gwindow-wrapup) (slot-value g 'history))
  nil)

(defmethod record-wrapup ((g immed-gwindow))
  (gwindow-wrapup g))


(defmethod record-nextpage ((g gwindow))
  (push (list 'gwindow-nextpage) (slot-value g 'history))
  nil)

(defmethod record-nextpage ((g immed-gwindow))
  (gwindow-nextpage g))


(defmethod record-moveto ((g gwindow) x y)
  ;; x y are page coordinates
  (with-slots (last-x last-y history) g
    (setf last-x x last-y y)
    (push (list 'gwindow-moveto x y) history)
    nil))

(defmethod record-moveto ((g immed-gwindow) x y)
  ;; x y are page coordinates
  (with-slots (last-x last-y) g
    (setf last-x x last-y y)
    (gwindow-moveto g x y)))


(defmethod record-lineto ((g gwindow) x y
                          &key (linewidth 1) (dash1 nil) (dash2 nil))
  ;; x y are page coordinates
  (with-slots (last-x last-y history) g
    (setf last-x x last-y y)
    (push (list 'gwindow-lineto x y linewidth dash1 dash2) history)
    nil))

(defmethod record-lineto ((g immed-gwindow) x y
                          &key (linewidth 1) (dash1 nil) (dash2 nil))
  ;; x y are page coordinates
  (with-slots (last-x last-y) g
    (setf last-x x last-y y)
    (gwindow-lineto g x y linewidth dash1 dash2)))


(defmethod record-line ((g gwindow) x1 y1 x2 y2 
                        &key (linewidth 1) (dash1 nil) (dash2 nil))
  (with-slots (last-x last-y history) g
    (setf last-x x2 last-y y2)
    (push (list 'gwindow-line x1 y1 x2 y2 linewidth dash1 dash2) history)
    nil))

(defmethod record-line ((g immed-gwindow) x1 y1 x2 y2 
                        &key (linewidth 1) (dash1 nil) (dash2 nil))
  (with-slots (last-x last-y) g
    (setf last-x x2 last-y y2)
    (gwindow-line g x1 y1 x2 y2 linewidth dash1 dash2)))


(defmethod record-polyline ((g gwindow) xy-list 
                            &key 
                            (linewidth 1) (dash1 nil) (dash2 nil)
                            (symbol nil) (symbol-scale 1))
  ;; xy-list is (x1 y1 x2 y2 x3 y3 ...)
  ;; symbol is list of segments to offset from points, 
  ;; in the form ((dx1 dy1 dx2 dy2) ...) 
  ;;  e.g. a plus sign of size 1 could be:
  ;;   '((-1 0 +1 0) (0 +1 0 -1))
  ;; symbol scale is factor to multiply dx dy by in symbol segment list
  ;; e.g. 2 will double the size
  (with-slots (last-x last-y history) g
    (setf last-x nil last-y nil)
    (push (list 'gwindow-polyline
                xy-list linewidth dash1 dash2 symbol symbol-scale) history)
    nil))

(defmethod record-polyline ((g immed-gwindow) xy-list 
                            &key 
                            (linewidth 1) (dash1 nil) (dash2 nil)
                            (symbol nil) (symbol-scale 1))
  (with-slots (last-x last-y) g
    (setf last-x nil last-y nil)
    (gwindow-polyline
     g xy-list linewidth dash1 dash2 symbol symbol-scale)))


(defmethod record-polygon ((g gwindow) xy-list 
                            &key 
                            (frame t) (fill nil) (linewidth 1)
                            (dash1 nil) (dash2 nil))
  ;; frame can be t or nil
  ;; default fill is nil, i.e. no fill
  ;; value can be in range 0 to 1, where 0=black, 1=white
  ;; xy points must form a closed list
  (with-slots (last-x last-y history) g
    (setf last-x nil last-y nil)
    (push (list 'gwindow-polygon 
                xy-list frame fill linewidth dash1 dash2) history)
    nil))

(defmethod record-polygon ((g immed-gwindow) xy-list 
                           &key 
                           (frame t) (fill nil) (linewidth 1)
                           (dash1 nil) (dash2 nil))
  (with-slots (last-x last-y) g
    (setf last-x nil last-y nil)
    (gwindow-polygon 
     g xy-list frame fill linewidth dash1 dash2)))


(defmethod record-polyseg ((g gwindow) seg-list 
                            &key (linewidth 1) (dash1 nil) (dash2 nil))
  ;; seg-list is list of (x1 y1 x2 y2)
  ;; segments need not be connected
  (with-slots (last-x last-y history) g
    (setf last-x nil last-y nil)
    (push (list 'gwindow-polyseg 
                seg-list linewidth dash1 dash2) history)
    nil))

(defmethod record-polyseg ((g immed-gwindow) seg-list 
                           &key (linewidth 1) (dash1 nil) (dash2 nil))
  (with-slots (last-x last-y) g
    (setf last-x nil last-y nil)
    (gwindow-polyseg 
     g seg-list linewidth dash1 dash2)))


(defmethod record-font ((g gwindow) font size)
  (with-slots (font-name font-size history) g
    (setf font-name font
          font-size size)
    (push (list 'gwindow-font font size) history)
    nil))

(defmethod record-font ((g immed-gwindow) font size)
  (with-slots (font-name font-size) g
    (setf font-name font
          font-size size)
    (gwindow-font g font size)))


(defmethod record-leftshow ((g gwindow) x y string)
  (with-slots (history) g
    (push (list 'gwindow-leftshow x y string) history)
    nil))

(defmethod record-leftshow ((g immed-gwindow) x y string)
  (gwindow-leftshow g x y string))


(defmethod record-rightshow ((g gwindow) x y string)
  (with-slots (history) g
    (push (list 'gwindow-rightshow x y string) history)
    nil))

(defmethod record-rightshow ((g immed-gwindow) x y string)
  (gwindow-rightshow g x y string))

;;; ----------------------------------------------------------------------
;;; higher level drawing methods: these don't use the primitives directly

(defmethod BOX-RECT ((g gwindow) &key (linewidth 0.25))
  "draw a box around the drawing rectangle"
  (with-slots (xpage-min xpage-max ypage-min ypage-max) g
    (let ((xy-list nil))
      (push (list xpage-min ypage-min) xy-list)
      (push (list xpage-min ypage-max) xy-list)
      (push (list xpage-max ypage-max) xy-list)
      (push (list xpage-max ypage-min) xy-list)
      (push (list xpage-min ypage-min) xy-list)
      (record-polygon g xy-list :frame t :linewidth linewidth))))


(defmethod BOX-WINDOW ((g gwindow)  &key (linewidth 0.25))
  "draw a box around the drawing window"
  (with-slots (xmin xmax ymin ymax) g
    (let ((x1 (x-to-page-horiz g xmin))
          (x2 (x-to-page-horiz g xmax))
          (y1 (y-to-page-vert g ymin))
          (y2 (y-to-page-vert g ymax)))
      (let ((xy-list nil))
        (push (list x1 y1) xy-list)
        (push (list x1 y2) xy-list)
        (push (list x2 y2) xy-list)
        (push (list x2 y1) xy-list)
        (push (list x1 y1) xy-list)
        (record-polygon g xy-list :frame t :linewidth linewidth)))))


(defmethod PLOT-SYMBOL-AT-POINT ((g gwindow) time value
                                 &key 
                                 (symbol nil)
                                 (symbol-scale nil)
                                 (string nil))
  (let ((x (t-to-page-x g time))
        (y (v-to-page-y g value)))
    (record-polyline g (list (list x y)) :symbol symbol :symbol-scale symbol-scale)
    (when string
      (record-leftshow g (+ x symbol-scale) y string))))

  
(defmethod STRING-AT-LINE-IN-WINDOW ((g gwindow)
                                     &key (line-number 1)
                                     (left-offset 2)
                                     (font-size 12)
                                     (string "foo"))
  (let* (;;
         ;; need to detect whether y runs up (postscript,mac) or down the page (xlib)
         (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
         )
    (record-leftshow g
                     (+ (round left-offset) (x-window-left g)) 
                     (- (y-window-top g)
                        (* line-number y-page-sign font-size))
                     string)))


(defmethod STRING-AT-LINE-IN-RECT ((g gwindow)
                                     &key (line-number 1)
                                     (left-offset 2)
                                     (font-size 12)
                                     (string "foo"))
  (let* (;;
         ;; need to detect whether y runs up (postscript,mac) or down the page (xlib)
         (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
         )
    (record-leftshow g
                     (+ (round left-offset) (x-rect-left g)) 
                     (- (y-rect-top g)
                        (* line-number y-page-sign font-size))
                     string)))


(defmethod STRING-AT-RECT-LEFT-WINDOW-BOTTOM
  ((g gwindow)
   &key (left-offset 2)
   (string "foo"))
  
  "Put string at left edge of the current rectangle, offset by left-offset.
    Y is at bottom of current window"
  
  (record-leftshow g
                   (+ left-offset (x-rect-left g))
                   (y-window-bottom g)
                   string))


(defmethod PLOT-CURVE ((g gwindow) 
                       point-list 
                       &key
                       (linewidth 0.25) 
                       (dash1 nil)
                       (dash2 nil)
                       (wrap-fraction 0.2)
                       (symbol nil)
                       (symbol-scale 1))
  
  "Plot a curve on the drawing window.  The points in the curve are specified 
    as a list of conses:  ((t1 . v1)(t2 . v2)...)
    or list of lists ((t1 v1) (t2 v2)...)
    linewidth is the line width; dash1 and dash2 are the parameters specifying 
    dashed lines.  wrap-fraction breaks the curve when the v values change 
    from within wrap fraction of the top of the window to within wrap-fraction 
    of the bottom between two points. Set wrap-fraction to zero to 
    prevent breaking"
  
  ;; (clip-to-window)

  (when point-list
    (let* (x y prev-x prev-y
             (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
             (bottom-axis-y-page (y-window-bottom g))
             (top-axis-y-page (y-window-top g))
             (range (if (= y-page-sign -1)
                      (- top-axis-y-page bottom-axis-y-page)
                      (- bottom-axis-y-page top-axis-y-page)))
             (fcn (if (numberp (rest (first point-list)))
                    'rest 'second))
             (xy-list nil))
      (setf x (t-to-page-x g (first (first point-list))))
      (setf y (v-to-page-y g (funcall fcn (first point-list))))
      (setf prev-x x prev-y y)
      (push (list x y) xy-list)
      (dolist (point point-list)
        (setf x (t-to-page-x g (first point)))
        (setf y (v-to-page-y g (funcall fcn point)))
        (let ((prev-y-near-top    (< (/ (abs (- top-axis-y-page    prev-y)) range) wrap-fraction))
              (y-near-top         (< (/ (abs (- top-axis-y-page    y))      range) wrap-fraction))
              (prev-y-near-bottom (< (/ (abs (- bottom-axis-y-page prev-y)) range) wrap-fraction))
              (y-near-bottom      (< (/ (abs (- bottom-axis-y-page y))      range) wrap-fraction)))
          (cond ((not (or (and prev-y-near-top y-near-bottom)
                          (and prev-y-near-bottom y-near-top)))
                 ;; flush current points, start new list
                 (when xy-list
                   (record-polyline g xy-list 
                                    :linewidth linewidth :dash1 dash1 :dash2 dash2
                                    :symbol symbol :symbol-scale symbol-scale)
                   (setf xy-list nil))
                 (push (list x y) xy-list)
                 )
                (t (push (list x y) xy-list))))
        (setf prev-x x prev-y y))
      ;; output current points
      (when xy-list
        (record-polyline g xy-list :linewidth linewidth :dash1 dash1 :dash2 dash2
                         :symbol symbol :symbol-scale symbol-scale))
      )))


(defmethod PLOT-POLY ((g gwindow) 
                      point-list 
                      &key
                      (linewidth 0.25) 
                      (dash1 nil)
                      (dash2 nil)
                      (frame t)
                      (fill nil))
  
  "Plot a polygon on the drawing window.  The points in the curve are specified 
    as a list of conses:  ((t1 . v1)(t2 . v2)...)
    or list of lists ((t1 v1) (t2 v2)...).  The curve will be closed
    automatically, i.e. the first and last points need not be the same.
    linewidth is the line width; dash1 and dash2 are the parameters specifying 
    dashed lines.  frame is t or nil to frame or not.
    fill is nil for none, 0 for black, 1 for white."
  
  ;; (clip-to-window)
  
  (when point-list
    (let* (x y 
             (fcn (if (numberp (rest (first point-list)))
                    'rest 'second))
             (xy-list nil))
      (setf x (t-to-page-x g (first (first point-list))))
      (setf y (v-to-page-y g (funcall fcn (first point-list))))
      (push (list x y) xy-list)
      (dolist (point point-list)
        (setf x (t-to-page-x g (first point)))
        (setf y (v-to-page-y g (funcall fcn point)))
        (push (list x y) xy-list)
        )
      ;; output points
      (when xy-list
        (record-polygon g xy-list :linewidth linewidth :dash1 dash1 :dash2 dash2
                        :frame frame :fill fill))
      )))


(defmethod LABEL-Y-AXIS ((g gwindow) start end 
                         &key
                         (tic-value 1)
                         (tic-size 1) 
                         (linewidth 0.25)
                         (label t)
                         (label-format "~a")
                         (font "Helvetica")
                         (font-size 5)
			 (grid nil)
			 (grid-width 0.1)
			 (grid-dash1 1)
			 (grid-dash2 0))
  
  "label v axis every tic-value on both left and right sides.
    tic-size - tic size in points
    linewidth - width in points
    if label is t (the default), then a label will be printed to the left
    of the tics, using the specified label-format"
  
  (if (<= tic-value 0) (error "tic-value must be > 0 ~a" tic-value))
  (when (> end start)
    (let* ((left-axis-x-page (x-window-left g))
           (right-axis-x-page (x-window-right g))
           (down (round font-size 2))
           (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
           (y start)
           (the-tics nil)
	   (the-grid nil)
           )
      (if label (record-font g font font-size))
      (record-line 
       g left-axis-x-page (v-to-page-y g start)
       left-axis-x-page (v-to-page-y g end) :linewidth linewidth)
      (record-line 
       g right-axis-x-page (v-to-page-y g start)
       right-axis-x-page (v-to-page-y g end) :linewidth linewidth)
      
      (loop
        (let* ((vert (v-to-page-y g y)))
          (push (list (- left-axis-x-page tic-size) vert
                      left-axis-x-page vert)
                the-tics)
          (push (list right-axis-x-page vert
                      (+ right-axis-x-page tic-size) vert)
                the-tics)
	  (when grid
	    (push (list left-axis-x-page vert right-axis-x-page vert)
		  the-grid))
          (when label
            (record-rightshow g
                              (- left-axis-x-page tic-size) 
                              (- vert (* y-page-sign down))
                              (format nil label-format y)))
          (setf y (+ y tic-value))
          (if (> y end) (return nil))))
      
      (when the-tics
        (record-polyseg g the-tics :linewidth linewidth))
      (when the-grid
	(record-polyseg g the-grid :linewidth grid-width
			:dash1 grid-dash1 :dash2 grid-dash2))
      )))


(defmethod LABEL-X-AXIS ((g gwindow) start end 
                         &key
                         (tic-value 1)
                         (tic-size 1) 
                         (linewidth 0.25)
                         (label t)
                         (label-format "~a")
                         (font "Helvetica")
                         (font-size 5)
			 (grid nil)
			 (grid-width 0.1)
			 (grid-dash1 1)
			 (grid-dash2 0))
  
  "label t axis every tic-value on both top and bottom.
    tic-size - tic size in points
    linewidth - line width in points
    if label is t (the default), then a label will be printed under
    of the tics, using the specified label-format"
  
  (if (<= tic-value 0) (error "tic-value must be > 0 ~a" tic-value))
  (when (> end start)
    (let* ((top-axis-y-page (y-window-top g))
           (bottom-axis-y-page (y-window-bottom g))
           (down (+ font-size tic-size))
           (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
           (x start)
           (the-tics nil)
	   (the-grid nil)
           )
      (if label (record-font g font font-size))
      (record-line 
       g (t-to-page-x g start) top-axis-y-page
       (t-to-page-x g end) top-axis-y-page :linewidth linewidth)
      (record-line 
       g (t-to-page-x g start) bottom-axis-y-page
       (t-to-page-x g end) bottom-axis-y-page :linewidth linewidth)
      
      (loop
        (let* ((h (t-to-page-x g x)))
          (push (list h (- top-axis-y-page tic-size)
                      h top-axis-y-page)
                the-tics)
          (push (list h bottom-axis-y-page
                      h (+ bottom-axis-y-page tic-size))
                the-tics)
	  (when grid
	    (push (list h top-axis-y-page h bottom-axis-y-page)
		  the-grid))
          (when label
            (record-leftshow g
                             h
                             (- bottom-axis-y-page (* y-page-sign down))
                             (format nil label-format x)))
          (setf x (+ x tic-value))
          (if (> x end) (return nil))))
      
      (when the-tics
        (record-polyseg g the-tics :linewidth linewidth))
      (when the-grid
	(record-polyseg g the-grid :linewidth grid-width
			:dash1 grid-dash1 :dash2 grid-dash2))
      )))


(defmethod PLOT-INTERVAL-BARS ((g gwindow) bar-list vmin vmax
                               &key (frame t) (fill nil)
                               (linewidth 0.25) (dash1 nil) (dash2 nil))
  
  "Plot intervals as bars.  bar-list can be:
    a list of start/end times (start1 end1 start2 end2...)
    or a list of lists of start end times:  ((start1 end1) (start2 end2)...).
    bars extend from vmin to vmax (window coordinates)"
  
  ;; each bar is plotted as a polygon
  (let ((y1 (v-to-page-y g vmin))
        (y2 (v-to-page-y g vmax)))
    (cond ((listp (first bar-list))
           ;; format is ((t1 t2)(t3 t4)...)
           (dolist (bar bar-list)
             (let ((x1 (t-to-page-x g (first bar)))
                   (x2 (t-to-page-x g (second bar))))
               (record-polygon
                g (list (list x1 y1) (list x2 y1) (list x2 y2) 
                        (list x1 y2) (list x1 y1))
                :frame frame :fill fill :linewidth linewidth 
                :dash1 dash1 :dash2 dash2))))
          (t ;; format is (t1 t2 t3 t4...)
           (do* ((todo bar-list (cddr todo)))
                ((null todo) nil)
             (if todo
               (let ((x1 (t-to-page-x g (first todo)))
                     (x2 (t-to-page-x g (second todo))))
                 (record-polygon
                  g (list (list x1 y1) (list x2 y1) (list x2 y2) 
                          (list x1 y2) (list x1 y1))
                  :frame frame :fill fill :linewidth linewidth 
                  :dash1 dash1 :dash2 dash2))))))))


(defmethod PLOT-INTERVAL-LIST ((g gwindow) input-int-list 
                               &key (fill nil) (linewidth 0.25)
                               (dash1 nil) (dash2 nil))
  
  "input interval list ((t1 t2 val)...) e.g. from pcf-to-interval-list.
    val is optional, if missing is assumed to be 1.
    fill is nil or 0 to 1 (nil is no fill, 1 is white, 0 is black).
    Draw pcf, outline, and fill.  Also draw baseline.
    Draws in the current drawing window at whatever scale is set."
  
  ;; draw as polygon
  ;; ensure that fits in window
  (let* ((int-list 
          (pcf-to-interval-list 
           (pcf-from-interval-list input-int-list)
           (t-min g) (t-max g)))
         (xy-list nil)
         (first-int (first int-list))
         x y xstart
         last-x
         last-y)
    
    ;; first interval
    (push (list (t-to-page-x g (first first-int)) (v-to-page-y g 0)) xy-list)
    (setf last-y (v-to-page-y g (or (third first-int) 1)))
    (push (list (t-to-page-x g (first first-int)) last-y) xy-list)
    (setf last-x (t-to-page-x g (second first-int)))
    (push (list last-x last-y) xy-list)
    
    ;; remaining intervals
    (dolist (int (rest int-list))
      (setf xstart (t-to-page-x g (first int)))
      (setf x (t-to-page-x g (second int)))
      (setf y (v-to-page-y g (or (third int) 1)))
      (cond ((= xstart last-x)
             (push (list xstart y) xy-list)
             (push (list x y) xy-list))
            (t
             (push (list last-x (v-to-page-y g 0)) xy-list)
             (push (list xstart (v-to-page-y g 0)) xy-list)
             (push (list xstart y) xy-list)
             (push (list x y) xy-list)))
      (setf last-x x last-y y)
      )
    
    ;; line down to baseline
    (push (list last-x (v-to-page-y g 0)) xy-list)
    
    (record-polygon g xy-list :frame t :fill fill
                    :linewidth linewidth :dash1 dash1 :dash2 dash2)
    
    ;; base (zero) line
    (record-line
     g (x-window-left g) (v-to-page-y g 0) (x-window-right g) (v-to-page-y g 0)
     :linewidth linewidth :dash1 dash1 :dash2 dash2)
    ))


(defmethod PLOT-PCF ((g gwindow) pcf &key (fill nil) (linewidth 0.25)
                     (dash1 nil) (dash2 nil))
  (plot-interval-list g
   (pcf-to-interval-list pcf (t-min g) (t-max g)) 
   :fill fill :linewidth linewidth :dash1 dash1 :dash2 dash2))


(defmethod TIME-VALUE-PLOT
  ((g gwindow)
   &key 
   start-time end-time
   page-left page-right page-bottom page-top
   window-left window-right window-bottom window-top
   curve pcf
   rect-title (rect-font "Helvetica") (rect-font-size 9)
   window-title (window-font "Helvetica")(window-font-size 6)
   (linewidth 0.25) (dash1 nil) (dash2 nil)
   y-scale-min			; required
   y-scale-max			; required
   (time-axis nil)
   (time-label nil)
   (time-axis-font "Helvetica")
   (time-axis-font-size 6)
   (box-rect nil)
   (box-window nil))
  
  "This is a generic curve or PCF postscript plotter.
    Arguments:
     start-time end-time - for x axis, in TJD
     page-left page-right page-bottom page-top - location of outer rectangle
      in page coordinates (points, 72/inch, origin is page lower left)
     window-left window-right window-bottom window-top - location of drawing
      window in rectangle.  Coordinates are given as fraction of the window
      width and height, i.e. they range from 0 to 1
     curve - if present, must be list of conses ((time1 . value1) ...)
     pcf - if present, must be PCF to be plotted.
     rect-title - title string for upper left of rectangle, or nil if none
     rect-font - postscript font for rect-title, default Helvetica
     rect-font-size - postscript font size for rect-title, default 9
     window-title - title string for upper left of window, or nil if none
     window-font - font for window-title, default Helvetica
     window-font-size - font size for window-title, default 6
     linewidth - curve or PCF width, default 1
     dash1, dash2 - dash parameters, both nil for solid curve, dash1=1,
      dash2=0 for dotted, dash1=3,dash2=0 for long dash, etc.
     y-scale-min y-scale-max - min and max scales for y axis
     time-axis - t if plot time axis, nil otherwise
     time-label - t if plot time label on axis, nil if just tics
     box-rect - t if draw box around rect
     box-window - t if draw box around window
     "
  
  (set-page-loc-of-drawing-rect 
   g page-left page-right page-bottom page-top) ;LRBT
  (set-loc-of-drawing-window
   g window-left window-right window-bottom window-top) ;LRBT
  (when rect-title
    (record-font g rect-font rect-font-size)
    (string-at-line-in-rect
     g :line-number 1 :font-size rect-font-size :string rect-title))
  (when window-title
    (record-font g window-font window-font-size)
    (string-at-line-in-window
     g :line-number 1 :font-size window-font-size :string window-title))
  (set-x-window-scale g start-time end-time)
  (if time-axis 
    (time-label g start-time end-time :label time-label
                :font time-axis-font :font-size time-axis-font-size))
  (set-y-window-scale g y-scale-min y-scale-max)
  (unless (= y-scale-min y-scale-max)
    (if curve (plot-curve g curve :linewidth linewidth :dash1 dash1 :dash2 dash2
                          :wrap-fraction 0))
    (if pcf (plot-pcf g pcf :linewidth linewidth :dash1 dash1 :dash2 dash2)))
  (if box-rect (box-rect g))
  (if box-window (box-window g))
  )


(defmethod TIME-LABEL ((g gwindow) start-date end-date
                       &key (label nil) 
                       (font "Helvetica")
                       (font-size 6) 
                       (linewidth 0.25)
                       (min-tic-size 2) 
                       (tic-increment 2)
                       (min-pixels-for-tics 10)
                       (min-pixels-for-labels 50)
                       (tics-at '((:second 10)
                                  (:minute)
                                  (:minute 5)
                                  (:minute 30)
                                  (:hour)
                                  (:hour 6)
                                  (:day)
                                  (:day 7)
                                  (:day 30))))
  ;; :day 30 means month start
  ;; :day 7 means week start
  ;; :day 14 means every two weeks
  "Draw time labels on the t axis between start and end dates, given as TJD.
    Labels drawn only if keyword arg label is t."
  
  (let* ((time-span (- end-date start-date))
         (x-span (- (x-window-right g) (x-window-left g)))
         (y-page-sign (if (< (y-window-bottom g) (y-window-top g)) +1 -1))
         (bottom-axis-y-page (y-window-bottom g))
         (top-axis-y-page (y-window-top g))
         (tic-size min-tic-size)
         (labeled-x-values nil)
         (labeled-x-values-units nil)
         (label-delta (round (* 1.2 font-size)))
         (label-offset 6)
         (label-offset-units nil)
         (showed-date nil)
         )
    
    (record-line
     g (t-to-page-x g start-date) bottom-axis-y-page 
     (t-to-page-x g end-date) bottom-axis-y-page :linewidth linewidth)
    (record-line
     g (t-to-page-x g start-date) top-axis-y-page
     (t-to-page-x g end-date) top-axis-y-page :linewidth linewidth)
    (record-font g font font-size)
    
    (dolist (tic-at tics-at)
      (let* ((unit (first tic-at))
             (scale (max 1 (or (second tic-at) 1)))
             (tic-interval-in-days 
              (* scale (cond ((eq unit :second) (/ 1.0 86400.0))
                             ((eq unit :minute) (/ 1.0 1440.0))
                             ((eq unit :hour  ) (/ 1.0 24.0))
                             ((eq unit :day   ) 1.0)
                             (t (error "invalid units ~a" unit)))))
             (number-of-tics (/ time-span tic-interval-in-days))
             (tic-page-x (/ x-span number-of-tics))
             (draw-tics (> tic-page-x min-pixels-for-tics))
             (draw-labels (and label (> tic-page-x min-pixels-for-labels)))
             )
        
        (when draw-tics
          
          (when (not (eq labeled-x-values-units unit))
            (setf labeled-x-values nil
                  labeled-x-values-units unit))
          (when (and draw-labels (not (eq label-offset-units unit)))
            (setf label-offset (+ label-offset label-delta)
                  label-offset-units unit))
          
          (cond 
           
           ((eq unit :day)
            (loop-over-time-in-range 
             :TJD-start start-date :TJD-end end-date
             :day-action 
             (let* ((doy (day-month-year-to-day-of-year 
                          day-of-month month year))
                    (tx (t-to-page-x g tjd)))
               (when (or (and (= 30 scale) (= 1 day-of-month))
			 ;; 0=Monday, this will put tic and label at Sunday 2400h = Monday 0h
                         (and (= 7 scale) (= 0 (day-of-week (+ 0.01 tjd))))
                         (and (= 14 scale) (= 0 (day-of-week (+ 0.01 tjd)))
			      (oddp (week-of-year tjd)))
                         (and (/= 30 scale) (/= 7 scale) (/= 14 scale) (= 0 (mod doy scale))))
                 (record-line g tx (- bottom-axis-y-page (* y-page-sign tic-size))
                              tx bottom-axis-y-page :linewidth linewidth)
                 (record-line g tx top-axis-y-page
                              tx (+ top-axis-y-page (* y-page-sign tic-size))
			      :linewidth linewidth)
                 (when (and draw-labels 
                            (not (member tx labeled-x-values))
                            (not showed-date))
                   (pushnew tx labeled-x-values)
                   ;; label days
                   (record-leftshow 
                    g 
                    (- tx 2) 
                    (- bottom-axis-y-page 
                       (* y-page-sign label-offset))
                    (format nil "~2d.~3,'0d" (mod year 100) (round doy)))
                   (record-leftshow 
                    g
                    (- tx 2) 
                    (- bottom-axis-y-page
                       (* y-page-sign (+ label-offset label-delta)))
                    (format nil "~2d/~d" month day-of-month))))))
            (if draw-labels (setf showed-date t))
            )
           ((eq unit :hour)
            (loop-over-time-in-range 
             :TJD-start start-date :TJD-end end-date
             :hour-action 
             (let* ((tx (t-to-page-x g tjd)))
               (when (= 0 (mod hour scale))
                 (record-line g tx (- bottom-axis-y-page (* y-page-sign tic-size))
                              tx bottom-axis-y-page :linewidth linewidth)
                 (record-line g tx top-axis-y-page
                              tx (+ top-axis-y-page (* y-page-sign tic-size))
			       :linewidth linewidth)
                 (when (and draw-labels (not (member tx labeled-x-values)))
                   (pushnew tx labeled-x-values)
                   (record-leftshow 
                    g 
                    (- tx 2) 
                    (- bottom-axis-y-page 
                       (* y-page-sign label-offset))
                    (format nil "~2,'0dh" hour))
                   ))))
            )
           ((eq unit :minute)
            (loop-over-time-in-range 
             :TJD-start start-date :TJD-end end-date
             :minute-action 
             (let* ((tx (t-to-page-x g tjd)))
               (when (= 0 (mod minute scale))
                 (record-line g tx (- bottom-axis-y-page (* y-page-sign tic-size))
                              tx bottom-axis-y-page :linewidth linewidth)
                 (record-line g tx top-axis-y-page
                              tx (+ top-axis-y-page (* y-page-sign tic-size))
			       :linewidth linewidth)
                 (when (and draw-labels (not (member tx labeled-x-values)))
                   (pushnew tx labeled-x-values)
                   (record-leftshow 
                    g 
                    (- tx 2) 
                    (- bottom-axis-y-page 
                       (* y-page-sign label-offset))
                    (format nil "~2,'0dm" minute))
                   ))))
            )
           ((eq unit :second)
            (loop-over-time-in-range 
             :TJD-start start-date :TJD-end end-date
             :second-action 
             (let* ((tx (t-to-page-x g tjd)))
               (when (= 0 (mod second scale))
                 (record-line g tx (- bottom-axis-y-page (* y-page-sign tic-size))
                              tx bottom-axis-y-page :linewidth linewidth)
                 (record-line g tx top-axis-y-page
                              tx (+ top-axis-y-page (* y-page-sign tic-size))
			       :linewidth linewidth)
                 (when (and draw-labels (not (member tx labeled-x-values)))
                   (pushnew tx labeled-x-values)
                   (record-leftshow 
                    g 
                    (- tx 2) 
                    (- bottom-axis-y-page 
                       (* y-page-sign label-offset))
                    (format nil "~2,'0ds" second))
                   ))))
            ))
          
          (setf tic-size (+ tic-size tic-increment))
          )                             ; end when tics
        ))                              ; end dolist over tics
    ;; put out date label if not done already
    (when (and label (not showed-date))
      (record-leftshow 
       g 
       (- (t-to-page-x g start-date) 2) 
       (- bottom-axis-y-page (* y-page-sign label-offset))
       (format-abs-time start-date))
      )
    ))



#|  NOT IMPLEMENTED:

  ;; use between gsave grestore to get back unclipped region
  (defmethod clip-to-window ((g gwindow))
    "clip to the window location"
    (let ((x1 (x-to-page-horiz g xmin))
          (x2 (x-to-page-horiz g xmax))
          (y1 (y-to-page-vert g ymin))
          (y2 (y-to-page-vert g ymax)))
      (ps-clip-to-rect g x1 x2 y1 y2)))

|#
  


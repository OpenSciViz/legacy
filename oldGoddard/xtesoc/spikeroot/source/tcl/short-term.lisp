;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; short-term.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: short-term.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.4  1992/04/22  16:54:05  johnston
;;; new routines for interruption returns
;;;
;;; Revision 1.3  1991/12/01  00:27:07  johnston
;;; faster version of function to average over fdsia intervals
;;;
;;; Revision 1.2  1991/11/26  15:45:53  johnston
;;; added averaging over intervals for mapping to suitability
;;;
;;;

#|
Externally callable routines:


MAKE-FDSIA [function]
   (&KEY INTERVAL-LIST INTERVAL-TYPE (MIN-DURATION NIL) (MAX-DURATION NIL) (SPECIFIED-DURATION NIL))
   make an FDSIA = array of FDSI.
    :interval-list - list of (t1 t2 [dur])
      where t1 is early start of interval, t2 is latest start or end 
      (depending on the keyword arg :interval-type)
      and dur is duration if scheduled in the interval
      All must be fixnums.  Duration may be omitted and will be ignored
      if :specified-duration argument is present.
      Intervals must be increasing time order by t1
     :interval-type - must be one of :START-START or :START-END
      if :start-start then t1 and t2 are interpreted as early and late starts
      if :start-end then t1 is early start and t2 is late end
      no default - this must be specified.
     :min-duration - nil or number s.t. intervals where the duration
      is < min-duration are excluded from the FDSIA
     :max-duration - nil or number s.t. intervals where the duration is
      > max-duration are excluded from the FDSIA
     :specified-duration - if a number, this overrides any dur in the 
      interval list.  Default is nil and the duration must be present
      in the intervals.
     Note that duration for min-duration and max-duration is the dur value
     in the interval (t1 t2 dur), not (- t2 t1) or some other quantity.
     returned value is FDSIA corresponding to input interval list,
     or nil if no viable intervals exist.

 boundary values & times

FDSIA-EARLIEST-START [function]
   (FDSIA)
   return earliest start time of any interval in FDSIA

FDSIA-EARLIEST-FINISH [function]
   (FDSIA)
   return earliest finish time of any interval in FDSIA

FDSIA-LATEST-START [function]
   (FDSIA)
   return latest start time of any interval in FDSIA

FDSIA-LATEST-FINISH [function]
   (FDSIA)
   return latest finish time of any interval in FDSIA

FDSIA-FIRST-VAL [function]
   (FDSIA)
   return value corresponding to earliest start in earliest interval of FDSIA

FDSIA-LAST-VAL [function]
   (FDSIA)
   return value corresponding to latest start in latest interval of FDSIA

FDSIA-VAL-COUNT [function]
   (FDSIA)
   return number of values (start times) in FDSIA

 conversion between times and vals

FDSIA-TIME-TO-VAL [function]
   (FDSIA TIME)
   Return multiple values:  
    first is val corresponding to start time, nil if no correspondence
    second is FDSI in which start time falls

FDSIA-TIME-TO-NEXT-VAL [function]
   (FDSIA TIME)
   Given an early-start time, return one of the following:
    If time corresponds to a specific val, return it
    Otherwise return the next val after time if there is one; if not return nil

FDSIA-TIME-TO-PREV-VAL [function]
   (FDSIA TIME)
   Given an early-start time, return one of the following:
    If time corresponds to a specific val, return it
    Otherwise return the first val before time if there is one; if not return nil

FDSIA-TIME-RANGE-TO-VAL-RANGE [function]
   (FDSIA TIME-RANGE)
   Input is a start time range (t1 t2), return value range (v1 v2) 
    s.t. all values corresponding to times between t1 and t2 inclusive
    are between v1 and v2 inclusive.  Return nil if the times are out of
    range of the FDSIA.

FDSIA-TIME-RANGE-LIST-TO-VAL-RANGE-LIST [function]
   (FDSIA TIME-RANGE-LIST)
   Input is a start time range list ((t1 t2)(t3 t4)...), 
    return corresponding value range list ((v1 v2) (v3 v4)...)
    s.t. all values corresponding to times between any t(i) and t(i+1) inclusive
    are between some pair of values inclusive.  
    Return nil if the times are out of range of the FDSIA.

FDSIA-VAL-TO-TIME [function]
   (FDSIA VAL)
   return multiple values start-time end-time corresponding to
    val, nil if no correspondence

 some general properties

FDSIA-MIN-DURATION [function]
   (FDSIA)
   return min duration for all FDSIs in FDSIA

FDSIA-MAX-DURATION [function]
   (FDSIA)
   return max duration for all FDSIs in FDSIA

FDSIA-DURATION-GIVEN-START-AT [function]
   (FDSIA TIME)
   Return duration if started at time, nil if time is not a valid start

FDSIA-DURATION-GIVEN-END-AT [function]
   (FDSIA TIME)
   Return duration if ends at time, nil if time is not a valid end

FDSIA-START-GIVEN-END-AT [function]
   (FDSIA TIME)
   return end time if started at time, or nil if time is not a valid end

FDSIA-END-GIVEN-START-AT [function]
   (FDSIA TIME)
   return start time if ended at time, or nil if time is not a valid start

 overlap exclusion calculation

FDSI-START-RANGE-EXCLUDED-BY-OVERLAP [function]
   (FDSI T1 T2)
   return multiple values start-val end-val excluded by overlap, nil for
    both if no overlap

FDSIA-OVERLAP-EXCLUSION [function]
   (FDSIA T1 T2)
   Return list of value ranges for which start of end of a task can
    overlap the interval t1 through t2

DO-FOR-EACH-TIME-IN-FDSIA [macro]
   (FDSIA &KEY TIME VALUE FDSI BODY)
   Execute body once for each discrete start time in FDSIA.
    :time and :value are symbols bound to corresponding time and value
     Both are optional: only those referenced in body need be supplied.
    :FDSI is optional: if present, it is a symbol bound to the current FDSI
    :body is the form to execute

DO-FOR-EACH-VALUE-IN-FDSIA [macro]
   (FDSIA &KEY VALUE START-TIME END-TIME DURATION FDSI BODY)
   Execute body once for each discrete value in FDSIA.
    :value is symbol bound to value
    :start-time and :end-time are symbols bound to times corresponding to value
    :duration is symbol is bound to duration
    :FDSI is a symbol bound to the current FDSI
    All are optional: only those required in body need be supplied.
    :body is the form to execute

 NOTE: this interprets suitabilities as at start: might need to average
 over duration?

FDSIA-MAP-TO-INTERVAL-LIST [function]
   (FDSIA INTERVAL-LIST)
   Input is an FDSIA and an interval-list ((t1 t2 suit)..) such as
    generated by ipcf-to-interval-list.
    Two values are returned:  
     - a list of suit values corresponding to each start time
       in the FDSIA.  This can be used to set preferences in the CSP.
     - an interval list covering the times and values of the list above.
       Only legal start times are included in the intervals returned.
    If there are insufficient intervals in the interval-list, nil is
    returned and a warning is emitted.

ARRAY-FIND-FIRST-GREATER-THAN-OR-EQUAL [function]
   (ARRAY VALUE KEY)
   input array and key function which can be applied to an array
    element and which returns a numeric value.  Array must be
    in STRICTLY ascending order by value returned by key.  
    Find first element (i.e. with smallest index) with 
     (funcall key element) >= value.
    Return multiple values:  array-element and index, or both 
    nil if not in range

ARRAY-FIND-LAST-LESS-THAN-OR-EQUAL [function]
   (ARRAY VALUE KEY)
   input array and key function which can be applied to an array
    element and which returns a numeric value.  Array must be
    in STRICTLY ascending order by value returned by key.  
    Find last element (i.e. with largest index) with 
     (funcall key element) <= value.
    Return multiple values:  array-element and index, or both 
    nil if not in range

LAYDOWN [function]
   (TASK FORWARD REMAINING-DUR T0 I-LIST MIN-SEG-DUR MAX-SEG-SEP SEG-OVERHEAD REVERSE-DONE-SEGS REVERSE-I-LIST CUM-OVERHEAD)
   

LAYDOWN-ALL [function]
   (TASK DUR INTERVALS MIN-SEG-DUR REL-DUR-ADJUST ABS-DUR-ADJUST)
   Return two values:
    dur-list - list of (t1 t2 d) s.t. task has duration d if 
     started and ended between t1 and t2 inclusive (this is suitable 
     for the interval-list for MAKE-FDSIA of type :start-end)
    eff-list - list of (t1 t2 eff) s.t. task has efficiency eff if
     started and ended between t1 and t2 inclusive

   Args:
    dur - nominal duration (required)
    intervals - list of (start end) intervals within which task must
     be scheduled
    min-seg-dur - minimum duration of any segment
    rel-dur-adjust - fraction of dur which may be added or subtracted
     to/from nominal duration, nil if not applicable
    abs-dur-adjust - time which may be added/subtracted to/from nominal
     duration, nil if not applicable
     If both rel and abs adjustments are provided, then whichever gives
     the largest potential adjustment is used.
    
    All times are integral and in the same units.  Adjustment times are
     calculated then rounded.
    Duration will be extended or shortened within limits above to improve
     efficiency.

|#

;; (quick-doc "xanadu:may91:short-term.lisp" "xanadu:may91:temp.tmp")


;;; FDSI = fixed duration scheduling interval
;;; implementation:  array of:
;;;  early-start, early-finish, late-start, late-finish, duration, val-es, val-ef
;;; all are fixnums!


;;; accessors for FDSI fields:

(defun FDSI-early-start (FDSI)
  (aref FDSI 0))
(defun set-FDSI-early-start (FDSI value)
  (setf (aref FDSI 0) value))
(defsetf FDSI-early-start set-FDSI-early-start)

(defun FDSI-early-finish (FDSI)
  (aref FDSI 1))
(defun set-FDSI-early-finish (FDSI value)
  (setf (aref FDSI 1) value))
(defsetf FDSI-early-finish set-FDSI-early-finish)

(defun FDSI-late-start (FDSI)
  (aref FDSI 2))
(defun set-FDSI-late-start (FDSI value)
  (setf (aref FDSI 2) value))
(defsetf FDSI-late-start set-FDSI-late-start)

(defun FDSI-late-finish (FDSI)
  (aref FDSI 3))
(defun set-FDSI-late-finish (FDSI value)
  (setf (aref FDSI 3) value))
(defsetf FDSI-late-finish set-FDSI-late-finish)

(defun FDSI-duration (FDSI)
  (aref FDSI 4))
(defun set-FDSI-duration (FDSI value)
  (setf (aref FDSI 4) value))
(defsetf FDSI-duration set-FDSI-duration)

(defun FDSI-val-es (FDSI)
  (aref FDSI 5))
(defun set-FDSI-val-es (FDSI value)
  (setf (aref FDSI 5) value))
(defsetf FDSI-val-es set-FDSI-val-es)

(defun FDSI-val-ls (FDSI)
  (aref FDSI 6))
(defun set-FDSI-val-ls (FDSI value)
  (setf (aref FDSI 6) value))
(defsetf FDSI-val-ls set-FDSI-val-ls)

(defun make-FDSI (&key early-start early-finish late-start late-finish duration
                       val-es val-ls)

  "Make an FDSI object with specified field values. Returned value is FDSI."

  (let ((FDSI (make-array 7 :initial-element -1 :element-type 'fixnum)))
    (if early-start (setf (FDSI-early-start FDSI) early-start))
    (if early-finish (setf (FDSI-early-finish FDSI) early-finish))
    (if late-start (setf (FDSI-late-start FDSI) late-start))
    (if late-finish (setf (FDSI-late-finish FDSI) late-finish))
    (if duration (setf (FDSI-duration FDSI) duration))
    (if val-es (setf (FDSI-val-es FDSI) val-es))
    (if val-ls (setf (FDSI-val-ls FDSI) val-ls))
    FDSI))
    
;;(setf foo (make-FDSI :early-start 0 :late-start 2))

#| Test construction of FDSIA:

;; next two give same result: on is start-start, other is start-end
(setf foo (make-FDSIA :interval-list 
                      '((0 5 2)(4 11 4)(8 13 2))
                      :interval-type :start-end))
(setf foo (make-FDSIA :interval-list 
                      '((0 3 2)(4 7 4)(8 11 2))
                      :interval-type :start-start))
;; merge if contiguous and same duration
(setf foo (make-FDSIA :interval-list 
                      '((0 3 2)(4 7 2)(8 11 2))
                      :interval-type :start-start))
;; specified duration
(setf foo (make-FDSIA :interval-list 
                      '((0 5)(4 11)(8 13))
                      :interval-type :start-end :specified-duration 2))

;; test interval exclusion
(setf foo (make-FDSIA :interval-list 
                      '((0 5 2)(4 11 4)(8 13 2))
                      :interval-type :start-end
                      :max-duration 3))
(setf foo (make-FDSIA :interval-list 
                      '((0 5 2)(4 11 4)(8 13 2))
                      :interval-type :start-end
                      :min-duration 3))
;; no intervals provided
(setf foo (make-FDSIA :interval-list 
                      nil
                      :interval-type :start-start))
;; no intervals feasible
(setf foo (make-FDSIA :interval-list 
                      '((0 -5 2))
                      :interval-type :start-start))
|#

(defun make-FDSIA (&key interval-list 
                        interval-type
                        (min-duration nil) 
                        (max-duration nil)
                        (specified-duration nil))

  "make an FDSIA = array of FDSI.
    :interval-list - list of (t1 t2 [dur])
      where t1 is early start of interval, t2 is latest start or end 
      (depending on the keyword arg :interval-type)
      and dur is duration if scheduled in the interval
      All must be fixnums.  Duration may be omitted and will be ignored
      if :specified-duration argument is present.
      Intervals must be increasing time order by t1
     :interval-type - must be one of :START-START or :START-END
      if :start-start then t1 and t2 are interpreted as early and late starts
      if :start-end then t1 is early start and t2 is late end
      no default - this must be specified.
     :min-duration - nil or number s.t. intervals where the duration
      is < min-duration are excluded from the FDSIA
     :max-duration - nil or number s.t. intervals where the duration is
      > max-duration are excluded from the FDSIA
     :specified-duration - if a number, this overrides any dur in the 
      interval list.  Default is nil and the duration must be present
      in the intervals.
     Note that duration for min-duration and max-duration is the dur value
     in the interval (t1 t2 dur), not (- t2 t1) or some other quantity.
     returned value is FDSIA corresponding to input interval list,
     or nil if no viable intervals exist."

  ;; error check
  (unless (or (eq interval-type :start-start)
              (eq interval-type :start-end))
    (error "unrecognized interval-type: ~a" interval-type))

  (let ((current-FDSI nil)
        (reverse-accum-FDSI nil)
        (FDSIA nil))

    (dolist (interval interval-list)
      (let ((t1 (first interval))
            (t2 (second interval))
            (dur (or specified-duration (third interval))))
        (if (not dur)
          (error "No duration for interval ~a" interval))
        ;; make new FDSI if there's enough time
        (unless (or (and (eq interval-type :start-end) (> dur (- t2 t1)))
                    (and min-duration (< dur min-duration))
                    (and max-duration (> dur max-duration)))
          (setf current-FDSI (make-FDSI))
          (setf (FDSI-early-start  current-FDSI) t1
                (FDSI-late-finish  current-FDSI) (if (eq interval-type :start-end)
                                                   t2 (+ t2 dur))
                (FDSI-late-start   current-FDSI) (if (eq interval-type :start-end)
                                                   (- t2 dur) t2)
                (FDSI-early-finish current-FDSI) (+ t1 dur) 
                (FDSI-duration     current-FDSI) dur)
          ;; check earlier intervals: are there any whose late finish
          ;; is >= the early finish of this one? if so set back the 
          ;; late start/finish accordingly
          (dolist (prev-FDSI reverse-accum-FDSI)
            (cond ((< (FDSI-late-finish  prev-FDSI) 
                      (FDSI-early-finish current-FDSI))
                   ;; done: no more possible
                   (return nil))
                  (t (let ((delta (1+ (- (FDSI-late-finish  prev-FDSI)
                                         (FDSI-early-finish current-FDSI)))))
                       ;;(format t "~%modifying ~a by ~a" prev-FDSI delta)
                       (decf (FDSI-late-finish prev-FDSI) delta)
                       (decf (FDSI-late-start  prev-FDSI) delta)
                       ;;(format t "~%       to ~a" prev-FDSI)
                       ))))
          ;; if contiguous, modify previous FDSI, else save this one
          (cond ((and reverse-accum-FDSI
                      (= (FDSI-duration current-FDSI)
                         (FDSI-duration (first reverse-accum-FDSI)))
                      (= (FDSI-early-start current-FDSI)
                         (1+ (FDSI-late-start (first reverse-accum-FDSI)))))
                 ;;(format t "~%contiguous ~a with ~a" (first reverse-accum-FDSI)
                 ;;       current-FDSI)
                 (setf (FDSI-late-start (first reverse-accum-FDSI))
                       (FDSI-late-start current-FDSI))
                 (setf (FDSI-late-finish (first reverse-accum-FDSI))
                       (FDSI-late-finish current-FDSI)))
                (t ;; save current FDSI
                 (push current-FDSI reverse-accum-FDSI)))
          ))) ;end dolist over interval list
    
    ;; convert to array
    (let ((accum-FDSI nil)
          (count 0))
      (dolist (FDSI reverse-accum-FDSI)
        ;; drop those where late-start < early-start
        (when (>= (FDSI-late-start FDSI) (FDSI-early-start FDSI))
          (incf count)
          (push FDSI accum-FDSI)))

      (unless (= 0 count)
        ;; only make array if there are elements
        (setf FDSIA (make-array count :initial-contents accum-FDSI))
        ;; compute vals
        (let ((first-FDSI (aref FDSIA 0)))
          (setf (FDSI-val-es first-FDSI) 0)
          (setf (FDSI-val-ls first-FDSI) (- (FDSI-late-start  first-FDSI)
                                            (FDSI-early-start first-FDSI))))
        (dotimes (i (1- count))
          (let* ((prev-FDSI (aref FDSIA i))
                 (first-val (1+ (FDSI-val-ls prev-FDSI)))
                 (FDSI (aref FDSIA (1+ i))))
            (setf (FDSI-val-es FDSI) first-val
                  (FDSI-val-ls FDSI) (+ first-val
                                        (- (FDSI-late-start  FDSI)
                                           (FDSI-early-start FDSI)))))))

      )

    ;; returned value
    FDSIA))

;;; boundary values & times

(defun FDSIA-earliest-start (FDSIA)
  "return earliest start time of any interval in FDSIA"
  (if FDSIA (FDSI-early-start (aref FDSIA 0))))

(defun FDSIA-earliest-finish (FDSIA)
  "return earliest finish time of any interval in FDSIA"
  (if FDSIA (FDSI-early-finish (aref FDSIA 0))))

(defun FDSIA-latest-start (FDSIA)
  "return latest start time of any interval in FDSIA"
  (if FDSIA (FDSI-late-start (aref FDSIA (1- (array-dimension FDSIA 0))))))

(defun FDSIA-latest-finish (FDSIA)
  "return latest finish time of any interval in FDSIA"
  (if FDSIA (FDSI-late-finish (aref FDSIA (1- (array-dimension FDSIA 0))))))

(defun FDSIA-first-val (FDSIA)
  "return value corresponding to earliest start in earliest interval of FDSIA"
  (if FDSIA (FDSI-val-es (aref FDSIA 0))))

(defun FDSIA-last-val (FDSIA)
  "return value corresponding to latest start in latest interval of FDSIA"
  (if FDSIA (FDSI-val-ls (aref FDSIA (1- (array-dimension FDSIA 0))))))

(defun FDSIA-val-count (FDSIA)
  "return number of values (start times) in FDSIA"
  (if FDSIA 
    (1+ (- (FDSIA-last-val FDSIA)
           (FDSIA-first-val FDSIA)))
    0))

;;; conversion between times and vals

(defun FDSIA-time-to-val (FDSIA time)
  "Return multiple values:  
    first is val corresponding to start time, nil if no correspondence
    second is FDSI in which start time falls"
  ;; (FDSIA-time-to-val foo 4)
  ;; (FDSIA-time-to-val foo 7)
  
  (if FDSIA
    (let ((FDSI (array-find-last-less-than-or-equal FDSIA time #'FDSI-early-start)))
      (cond (FDSI
             (let* ((early-start (FDSI-early-start FDSI))
                    (late-start (FDSI-late-start FDSI))
                    (first-val (FDSI-val-es FDSI)))
               (values (if (<= time late-start)
                         (+ first-val (- time early-start))
                         nil)
                       FDSI)))
            (t (values nil nil))))))


(defun FDSIA-time-to-next-val (FDSIA time)
  "Given an early-start time, return one of the following:
    If time corresponds to a specific val, return it
    Otherwise return the next val after time if there is one; if not return nil"
  ;; (FDSIA-time-to-next-val foo 4)
  ;; (FDSIA-time-to-next-val foo 7)
  
  (if FDSIA
    (multiple-value-bind
      (FDSI index)
      (array-find-last-less-than-or-equal FDSIA time #'FDSI-early-start)
      (cond (FDSI
             (let* ((early-start (FDSI-early-start FDSI))
                    (late-start (FDSI-late-start FDSI))
                    (first-val (FDSI-val-es FDSI)))
               (cond ((<= time late-start)
                      ;; exact correspondence to val: return it
                      (+ first-val (- time early-start)))
                     (t ; find next FDSI if any
                      (let ((next-FDSI (if (< index (1- (array-dimension FDSIA 0)))
                                         (aref FDSIA (1+ index)))))
                        (cond (next-FDSI
                               (FDSI-val-es next-FDSI))
                              (t nil)))))))
            (t ; time is before first interval: return ES of first interval
             (FDSI-val-es (aref FDSIA 0)))))))


(defun FDSIA-time-to-prev-val (FDSIA time)
  "Given an early-start time, return one of the following:
    If time corresponds to a specific val, return it
    Otherwise return the first val before time if there is one; if not return nil"
  ;; (FDSIA-time-to-prev-val foo 4)
  ;; (FDSIA-time-to-prev-val foo 7)
  
  (if FDSIA
    (multiple-value-bind
      (FDSI index)
      (array-find-last-less-than-or-equal FDSIA time #'FDSI-early-start)
      (declare (ignore index))
      (cond (FDSI
             (let* ((early-start (FDSI-early-start FDSI))
                    (late-start (FDSI-late-start FDSI))
                    (first-val (FDSI-val-es FDSI)))
               (cond ((<= time late-start)
                      ;; exact correspondence to val: return it
                      (+ first-val (- time early-start)))
                     (t ; return last val in interval
                      (FDSI-val-ls FDSI)))))
            (t ; time is before first interval: return nil
             nil)))))

#| test time range stuff
(setf foo (make-FDSIA :interval-list 
                      '((1 3 2)(4 7 4)(8 11 2))
                      :interval-type :start-start))
(loop-over-value-range
 i -2 12
 (format t "~% t=~3d  val: ~3a  next val: ~3a prev val: ~3a"
         i (FDSIA-time-to-val foo i)
         (FDSIA-time-to-next-val foo i)
         (FDSIA-time-to-prev-val foo i)))
(FDSIA-time-range-to-val-range foo '(6 15))
(FDSIA-time-range-to-val-range foo '(5 8))
(FDSIA-time-range-list-to-val-range-list foo '((-1 1)(5 8)(20 30)))
|#

(defun FDSIA-time-range-to-val-range (FDSIA time-range)
  "Input is a start time range (t1 t2), return value range (v1 v2) 
    s.t. all values corresponding to times between t1 and t2 inclusive
    are between v1 and v2 inclusive.  Return nil if the times are out of
    range of the FDSIA."
  ;; (FDSIA-time-range-to-val-range foo '(11 50))
  (if FDSIA
    (let ((next-after-t1 (FDSIA-time-to-next-val FDSIA (first time-range)))
          (prev-before-t2 (FDSIA-time-to-prev-val FDSIA (second time-range))))
      (cond ((and next-after-t1
                  prev-before-t2
                  (<= next-after-t1 prev-before-t2))
             (list next-after-t1 prev-before-t2))
            (t ;out of range
             nil)))))


(defun FDSIA-time-range-list-to-val-range-list (FDSIA time-range-list)
  "Input is a start time range list ((t1 t2)(t3 t4)...), 
    return corresponding value range list ((v1 v2) (v3 v4)...)
    s.t. all values corresponding to times between any t(i) and t(i+1) inclusive
    are between some pair of values inclusive.  
    Return nil if the times are out of range of the FDSIA."
  (if FDSIA
    (let ((result nil))
      (dolist (time-range time-range-list)
        (let ((value-range (FDSIA-time-range-to-val-range FDSIA time-range)))
          (if value-range (push value-range result))))
      (nreverse result))))


(defun FDSIA-val-to-time (FDSIA val)
  "return multiple values start-time end-time corresponding to
    val, nil if no correspondence"
  (if FDSIA
    (let ((FDSI (array-find-last-less-than-or-equal FDSIA val #'FDSI-val-es)))
      (cond (FDSI
             (let* ((early-start (FDSI-early-start FDSI))
                    (duration (FDSI-duration FDSI))
                    (val-es (FDSI-val-es FDSI))
                    (val-ls (FDSI-val-ls FDSI))
                    (start (+ early-start (- val val-es)))
                    (end (+ start duration)))
               (if (<= val val-ls) (values start end) (values nil nil))))
            (t (values nil nil))))))

;;; some general properties

(defun FDSIA-min-duration (FDSIA)
  "return min duration for all FDSIs in FDSIA"
  ;; (FDSIA-min-duration foo)
  (if FDSIA
    (let ((result (FDSI-duration (aref FDSIA 0))))
      (loop-over-value-range
       i 1 (1- (array-dimension FDSIA 0))
       (setf result (min result (FDSI-duration (aref FDSIA i)))))
      result)))

(defun FDSIA-max-duration (FDSIA)
  "return max duration for all FDSIs in FDSIA"
  ;; (FDSIA-max-duration foo)
  (if FDSIA
    (let ((result (FDSI-duration (aref FDSIA 0))))
      (loop-over-value-range
       i 1 (1- (array-dimension FDSIA 0))
       (setf result (max result (FDSI-duration (aref FDSIA i)))))
      result)))

(defun FDSIA-duration-given-start-at (FDSIA time)
  "Return duration if started at time, nil if time is not a valid start"
  ;; (dotimes (i 13) (format t "~%t=~d dur ~a" i (FDSIA-duration-given-start-at foo i)))
  (if FDSIA
    (multiple-value-bind
      (val FDSI)
      (FDSIA-time-to-val FDSIA time)
      (cond ((and val FDSI) (FDSI-duration FDSI))
            (t nil)))))

(defun FDSIA-duration-given-end-at (FDSIA time)
  "Return duration if ends at time, nil if time is not a valid end"
  ;; (FDSIA-duration-given-end-at foo 4)
  ;; (dotimes (i 15) (format t "~%t=~d dur ~a" i (FDSIA-duration-given-end-at foo i)))
  (if FDSIA
    (let ((FDSI (array-find-last-less-than-or-equal FDSIA time #'FDSI-early-finish)))
      (cond (FDSI
             (let* ((early-finish (FDSI-early-finish FDSI))
                    (late-finish (FDSI-late-finish FDSI))
                    )
               (if (and (>= time early-finish)
                        (<= time late-finish))
                 (FDSI-duration FDSI)
                 nil)))
            (t nil)))))

(defun FDSIA-start-given-end-at (FDSIA time)
  "return end time if started at time, or nil if time is not a valid end"
  (if FDSIA
    (let ((dur (FDSIA-duration-given-end-at FDSIA time)))
      (if dur (- time dur) nil))))

(defun FDSIA-end-given-start-at (FDSIA time)
  "return start time if ended at time, or nil if time is not a valid start"
  (if FDSIA
    (let ((dur (FDSIA-duration-given-start-at FDSIA time)))
      (if dur (+ time dur) nil))))

#| test:
(loop-over-value-range
 i -1 15
 (let*  ((end (FDSIA-end-given-start-at foo i))
         (recomp-start (if end (FDSIA-start-given-end-at foo end)))
         (recomp-end (if recomp-start (FDSIA-end-given-start-at foo recomp-start))))
   (format t "~%i=~3d  end: ~3a (~3a)  recomputed start: ~3a"
           i end recomp-end recomp-start)))

|#
;;; overlap exclusion calculation

(defun FDSI-start-range-excluded-by-overlap (FDSI t1 t2)

  "return multiple values start-val end-val excluded by overlap, nil for
    both if no overlap"

  (let* ((ES (FDSI-early-start FDSI))
         (LF (FDSI-late-finish FDSI)))
    ;; cases
    (cond 
     ((or (> t1 LF) (< t2 ES)) 
      ;; no overlap:
      ;;            t1         t2
      ;;            |----------|
      ;;                        ES |====| LF
      ;; ES |====| LF
      (values nil nil))
     ((and (< t1 ES) (> t2 LF))
      ;; whole interval excluded:
      ;;          t1         t2
      ;;          |----------|
      ;;         ES |======| LF
      (values (FDSI-val-es FDSI) (FDSI-val-ls FDSI)))
     (t ;; some overlap:
      (let* ((duration (FDSI-duration FDSI))
             ;; times excluded: start & end
             (start-exclude (1+ (- t1 duration)))
             (end-exclude (1- t2)))
        (cond
         ((< end-exclude start-exclude)
          ;; no times excluded
          (values nil nil))
         (t ;; find values excluded, then limit to range
          (let* ((val-es (FDSI-val-es FDSI))
                 (val-ls (FDSI-val-ls FDSI))
                 (first-val-excluded (+ val-es (- start-exclude ES)))
                 (last-val-excluded  (+ val-es (- end-exclude   ES))))
            ;; limit vals to range of FDSI
            (cond ((or (< last-val-excluded val-es)
                       (> first-val-excluded val-ls))
                   ;; no values in intersection:
                   ;; in diagrams, v1=first-val-excluded, v2=last-val-excluded
                   ;;      val-es |------------| val-ls
                   ;;   v1|===|v2
                   ;;      val-es |------------| val-ls
                   ;;                              v1|===|v2
                   (values nil nil))
                  ((<= first-val-excluded val-es)
                   (cond ((<= last-val-excluded val-ls)
                          ;;      val-es |------------| val-ls
                          ;;    v1|===========|v2
                          (values val-es last-val-excluded))
                         (t 
                          ;;      val-es |------------| val-ls
                          ;;    v1|======================|v2
                          (values val-es val-ls))))
                  ((<= first-val-excluded val-ls)
                   (cond ((<= last-val-excluded val-ls)
                          ;;      val-es |------------| val-ls
                          ;;              v1|===|v2
                          (values first-val-excluded last-val-excluded))
                         (t 
                          ;;      val-es |------------| val-ls
                          ;;              v1|============|v2
                          (values first-val-excluded val-ls))))
                  (t (error "drop through interval overlap")))))))))))

#| ;; test overlap on single FDSI
(setf f (make-FDSI :early-start 0 :early-finish 2 :late-start 3 :late-finish 5
                   :duration 2 :val-es 0 :val-ls 3))
;; no overlap no exclusion: all return nil nil
(FDSI-start-range-excluded-by-overlap f -5 -2)
(FDSI-start-range-excluded-by-overlap f -5 0)
(FDSI-start-range-excluded-by-overlap f 5 10)
(FDSI-start-range-excluded-by-overlap f 6 10)
;; fully contained all excluded: all return 0 3
(FDSI-start-range-excluded-by-overlap f 0 5)
(FDSI-start-range-excluded-by-overlap f -1 5)
(FDSI-start-range-excluded-by-overlap f 0 6)
(FDSI-start-range-excluded-by-overlap f -1 6)
;; start is excluded
(FDSI-start-range-excluded-by-overlap f -1 0) -> nil nil
(FDSI-start-range-excluded-by-overlap f -1 1) -> 0 0
(FDSI-start-range-excluded-by-overlap f -1 2) -> 0 1
(FDSI-start-range-excluded-by-overlap f -1 3) -> 0 2
(FDSI-start-range-excluded-by-overlap f -1 4) -> 0 3
(FDSI-start-range-excluded-by-overlap f -1 5) -> 0 3
(FDSI-start-range-excluded-by-overlap f -1 6) -> 0 3
(FDSI-start-range-excluded-by-overlap f -1 7) -> 0 3
;; end is excluded
(FDSI-start-range-excluded-by-overlap f 5 10) -> nil nil
(FDSI-start-range-excluded-by-overlap f 4 10) -> 3 3
(FDSI-start-range-excluded-by-overlap f 3 10) -> 2 3
(FDSI-start-range-excluded-by-overlap f 2 10) -> 1 3
(FDSI-start-range-excluded-by-overlap f 1 10) -> 0 3
(FDSI-start-range-excluded-by-overlap f 0 10) -> 0 3
;; middle is excluded
(FDSI-start-range-excluded-by-overlap f 0 0) - nil nil
(FDSI-start-range-excluded-by-overlap f 1 1) -> 0 0
(FDSI-start-range-excluded-by-overlap f 1 2) -> 0 1
(FDSI-start-range-excluded-by-overlap f 1 3) -> 0 2
(FDSI-start-range-excluded-by-overlap f 1 4) -> 0 3
(FDSI-start-range-excluded-by-overlap f 2 2) -> 1 1
(FDSI-start-range-excluded-by-overlap f 2 3) -> 1 2
(FDSI-start-range-excluded-by-overlap f 2 4) -> 1 3
(FDSI-start-range-excluded-by-overlap f 2 5) -> 1 3
(FDSI-start-range-excluded-by-overlap f 3 3) -> 2 2
(FDSI-start-range-excluded-by-overlap f 3 4) -> 2 3
(FDSI-start-range-excluded-by-overlap f 3 5) -> 2 3
(FDSI-start-range-excluded-by-overlap f 3 6) -> 2 3
(FDSI-start-range-excluded-by-overlap f 4 4) -> 3 3
(FDSI-start-range-excluded-by-overlap f 4 5) -> 3 3

;; zero duration FDSI
(setf f (make-FDSI :early-start 0 :early-finish 0 :late-start 2 :late-finish 2
                   :duration 0 :val-es 0 :val-ls 2))
;; unit duration FDSI
(setf f (make-FDSI :early-start 0 :early-finish 1 :late-start 2 :late-finish 3
                   :duration 1 :val-es 0 :val-ls 2))

(loop-over-value-range
 t1 -1 3
 (loop-over-value-range
  t2 -1 3
  (unless (< t2 t1)
    (format t "~% ~3d ~3d excludes ~a" 
            t1 t2
            (multiple-value-list 
             (FDSI-start-range-excluded-by-overlap f t1 t2))))))

;; results for zero-duration fdsi:
  -1  -1 excludes (NIL NIL)
  -1   0 excludes (NIL NIL)
  -1   1 excludes (0 0)
  -1   2 excludes (0 1)
  -1   3 excludes (0 2)
   0   0 excludes (NIL NIL)
   0   1 excludes (NIL NIL)
   0   2 excludes (1 1)
   0   3 excludes (1 2)
   1   1 excludes (NIL NIL)
   1   2 excludes (NIL NIL)
   1   3 excludes (2 2)
   2   2 excludes (NIL NIL)
   2   3 excludes (NIL NIL)
   3   3 excludes (NIL NIL)
;; results for unit duration fdsi
  -1  -1 excludes (NIL NIL)
  -1   0 excludes (NIL NIL)
  -1   1 excludes (0 0)
  -1   2 excludes (0 1)
  -1   3 excludes (0 2)
   0   0 excludes (NIL NIL)
   0   1 excludes (0 0)
   0   2 excludes (0 1)
   0   3 excludes (0 2)
   1   1 excludes (NIL NIL)
   1   2 excludes (1 1)
   1   3 excludes (1 2)
   2   2 excludes (NIL NIL)
   2   3 excludes (2 2)
   3   3 excludes (NIL NIL)

|#


(defun FDSIA-overlap-exclusion (FDSIA t1 t2)

  "Return list of value ranges for which start of end of a task can
    overlap the interval t1 through t2"

  (when FDSIA
    (unless (>= t2 t1) (error "t1 ~a must be >= t2 ~a" t1 t2))
    (let* ((max-index (1- (array-dimension FDSIA 0))))
      (multiple-value-bind
        (FDSI-1 index-1)
        (array-find-last-less-than-or-equal FDSIA t1 #'FDSI-late-finish)
        (declare (ignore FDSI-1))
        (multiple-value-bind
          (FDSI-2 index-2)
          (array-find-first-greater-than-or-equal FDSIA t2 #'FDSI-early-start)
          (declare (ignore FDSI-2))
          ;; cases
          ;;(format t "~%last LF <= ~3d: ~3a, first ES >= ~3d: ~3a"
          ;;        t1 index-1 t2 index-2)
          (cond 
           
           ((and index-2 (= index-2 0))
            ;; t1 t2 is before any sched interval
            ;;(format t "   before all")
            nil)
           ((and index-1 (= index-1 max-index))
            ;; t1 t2 is after any sched interval
            ;;(format t "   after all")
            nil)
           (t
            (let* ((lower-index (if index-1 (min (1+ index-1) max-index) 0))
                   (upper-index (if index-2 (max 0 (1- index-2)) max-index))
                   (result nil))
              ;;(format t "  index range: ~3d-~3d" lower-index upper-index)
              (loop-over-value-range 
               i lower-index upper-index
               (multiple-value-bind
                 (start-excluded-val end-excluded-val)
                 (FDSI-start-range-excluded-by-overlap
                  (aref FDSIA i) t1 t2)
                 ;;(format t "~%  excluding ~3a ~3a" start-excluded-val end-excluded-val)
                 (unless (or (null start-excluded-val) (null end-excluded-val))
                   (cond 
                    ((and result
                          (= start-excluded-val (1+ (second (first result)))))
                     ;; contiguous with previous range: just extend upper bound
                     (setf (second (first result)) end-excluded-val))
                    (t ;; not contiguous, add new range
                     (push (list start-excluded-val end-excluded-val) result))))
                 ;;(format t " ~a" result)
                 ))
              (nreverse result)
              ))))))))


(defmacro DO-FOR-EACH-TIME-IN-FDSIA (FDSIA &key time value duration FDSI body)

  "Execute body once for each discrete start time in FDSIA.
     :time, :value, and :duration are symbols bound to corresponding time, value,
       and duration if started at time.  They are optional: only those 
       referenced in body need be supplied.
     :FDSI is optional: if present, it is a symbol bound to the current FDSI
     :body is the form to execute"

  (let ((FDSI-index-sym    (gensym "FDSI-INDEX"))
        (FDSI-sym          (or FDSI (gensym "FDSI")))
        (val-es-sym        (gensym "VAL-ES"))
        (early-start-sym   (gensym "EARLY-START"))
        (late-start-sym    (gensym "LATE-START"))
        (time-sym          (or time (gensym "TIME")))
        (value-sym         (or value (gensym "VALUE")))
        (duration-sym      (or duration (gensym "DURATION")))
        )
    
    `(when ,FDSIA
       (dotimes (,FDSI-index-sym (array-dimension ,FDSIA 0))
         (let* ((,FDSI-sym (aref ,FDSIA ,FDSI-index-sym))
                (,val-es-sym (FDSI-val-es ,FDSI-sym))
                (,early-start-sym (FDSI-early-start ,FDSI-sym))
                (,late-start-sym (FDSI-late-start ,FDSI-sym))
                (,duration-sym (FDSI-duration ,FDSI-sym))
                (,value-sym nil)
                )
           (loop-over-value-range 
            ,time-sym ,early-start-sym ,late-start-sym
            (setf ,value-sym (+ ,val-es-sym (- ,time-sym ,early-start-sym)))
            ,body))))))


(defmacro DO-FOR-EACH-VALUE-IN-FDSIA (FDSIA &key value 
                                            start-time end-time duration
                                            FDSI body)
  "Execute body once for each discrete value in FDSIA.
    :value is symbol bound to value
    :start-time and :end-time are symbols bound to times corresponding to value
    :duration is symbol is bound to duration
    :FDSI is a symbol bound to the current FDSI
    All are optional: only those required in body need be supplied.
    :body is the form to execute"
  (let ((FDSI-index-sym     (gensym "FDSI-INDEX"))
        (FDSI-sym           (or FDSI (gensym "FDSI")))
        (val-es-sym         (gensym "VAL-ES"))
        (early-start-sym    (gensym "EARLY-START"))
        (late-start-sym     (gensym "LATE-START"))
        (duration-sym       (or duration (gensym "DURATION")))
        (start-time-sym     (or start-time (gensym "START-TIME")))
        (end-time-sym       (or end-time (gensym "END-TIME")))
        (value-sym          (or value (gensym "VALUE"))))
    `(when ,FDSIA
       (dotimes (,FDSI-index-sym (array-dimension ,FDSIA 0))
         (let* ((,FDSI-sym (aref ,FDSIA ,FDSI-index-sym))
                (,val-es-sym (FDSI-val-es ,FDSI-sym))
                (,early-start-sym (FDSI-early-start ,FDSI-sym))
                (,late-start-sym (FDSI-late-start ,FDSI-sym))
                (,duration-sym (FDSI-duration ,FDSI-sym))
                (,value-sym nil)
                (,end-time-sym nil))
           (loop-over-value-range 
            ,start-time-sym ,early-start-sym ,late-start-sym
            (setf ,value-sym (+ ,val-es-sym (- ,start-time-sym ,early-start-sym)))
            (setf ,end-time-sym (+ ,start-time-sym ,duration-sym))
            ,body))))))


(defun FDSIA-map-to-interval-list (FDSIA interval-list)
  
  "Input is an FDSIA and an interval-list ((t1 t2 suit)..) such as
    generated by ipcf-to-interval-list.
    Two values are returned:  
     - a list of suit values corresponding to each start time
       in the FDSIA.  This can be used to set preferences in the CSP.
     - an interval list covering the times and values of the list above.
       Only legal start times are included in the intervals returned.
    If there are insufficient intervals in the interval-list, nil is
    returned and a warning is emitted."
  
  ;; (ipcf-to-interval-list (ipcf-from-interval-list '((0 5 0.2)(6 10 0.5)(11 15 0.1))))

  (let ((remaining-intervals (rest interval-list))
        (current-interval (first interval-list))
        (result nil) ;; list of values at start times
        (mapped-interval-list nil))
    (do-for-each-value-in-FDSIA 
     FDSIA :value value :start-time start-time
     :body
     (loop
       ;; exit from loop if time in current interval
       (when (and (>= start-time (first current-interval))
                  (<= start-time (second current-interval)))
         (push (third current-interval) result)
         (cond ((and (first mapped-interval-list)
                     (= start-time (1+ (second (first mapped-interval-list))))
                     (= (third current-interval) (third (first mapped-interval-list))))
                ;; time follows previous interval, value is the same:
                ;; just update end time of previous interval
                (setf (second (first mapped-interval-list)) start-time))
               (t
                ;; no previous interval or interval can't be extended:
                ;; add new interval
                (push (list start-time start-time (third current-interval))
                      mapped-interval-list)))
         (return nil))
       ;; else go on to next interval
       (setf current-interval (first remaining-intervals)
             remaining-intervals (rest remaining-intervals))
       ;; if we run out of intervals, warn then exit returning nil
       (when (null current-interval)
         (warn "no intervals left in interval list")
         (return-from FDSIA-map-to-interval-list nil))))
    ;; returned values
    (values (nreverse result)
            (nreverse mapped-interval-list))))


(defun FDSIA-map-average-to-interval-list (FDSIA interval-list)
  
  "Input is an FDSIA and an interval-list ((t1 t2 suit)..) such as
    generated by ipcf-to-interval-list.
    Two values are returned:  
     - a list of suit values corresponding to the AVERAGE suitability
       for each start time in the FDSIA.  This can be used to set preferences 
       in the CSP.
     - an interval list covering the times and values of the list above.
       Only legal start times are included in the intervals returned.
    If there are insufficient intervals in the interval-list, nil is
    returned and a warning is emitted."
  
  ;; This is an extension of FDSIA-map-to-interval-list which calculates
  ;; average suitability instead of just taking the value at the start 
  ;; time.

  (let (;;(suit-ipcf (ipcf-from-interval-list interval-list))
        (remaining-intervals (rest interval-list))
        (current-interval (first interval-list))
        (result nil) ;; list of values at start times
        (mapped-interval-list nil))
    (do-for-each-value-in-FDSIA 
     FDSIA :value value :start-time start-time :end-time end-time
     :body
     (loop
       ;; exit from loop if time in current interval
       (when (and (>= start-time (first current-interval))
                  (<= start-time (second current-interval)))
         ;; averaging occurs as follows:
         ;;     0     1     2     3     4
         ;;   --+-----+-----+-----+-----+---  (+=quantized time points)
         ;;    start  |-----------------| end, dur = 3 in this example
         ;; average over values a t=1,2,3 (i.e. don't include 4)
         ;; thus, from start to (end-1)
         ;; if duration is zero, then start = end so use value at start
         ;; (i.e. same as averaging with duration 1)
         (let ((ave-value
                ;; slow, excruciatingly slow...
                ;;(ipcf-average-over-range 
                ;; suit-ipcf start-time (max start-time (1- end-time)))
                ;; replaced by:
                (average-over-current-and-future-intervals
                 start-time (max start-time (1- end-time))
                 current-interval remaining-intervals)
                ))
           (push ave-value result)
           (cond ((and (first mapped-interval-list)
                       (= start-time (1+ (second (first mapped-interval-list))))
                       (= ave-value (third (first mapped-interval-list))))
                  ;; time follows previous interval, value is the same:
                  ;; just update end time of previous interval
                  (setf (second (first mapped-interval-list)) start-time))
                 (t
                  ;; no previous interval or interval can't be extended:
                  ;; add new interval
                  (push (list start-time start-time ave-value)
                        mapped-interval-list)))
           (return nil)))
       ;; else go on to next interval
       (setf current-interval (first remaining-intervals)
             remaining-intervals (rest remaining-intervals))
       ;; if we run out of intervals, warn then exit returning nil
       (when (null current-interval)
         (warn "no intervals left in interval list")
         (return-from FDSIA-map-average-to-interval-list nil))))   ; end do-for-each-val..

    ;; returned values
    (values (nreverse result)
            (nreverse mapped-interval-list))))

;; aux function: average over current and future intervals
;; for FDSIA-map-average-to-interval-list

(defun AVERAGE-OVER-CURRENT-AND-FUTURE-INTERVALS
       (start end current-interval remaining-intervals)
  
  "Intervals (current and list of remaining intervals)
    are of form (t1 t2 value), asc order and non-overlapping.  
    Get average value over the period start to end inclusive.  
    start is in the range t1 <= start <= end.  Second value
    returned is number of values in intervals that were averaged."
  
  (let* (sum count)
    
    ;; first interval
    (let ((int-end (second current-interval))
          (int-value (third current-interval)))
      (setf count (if (> end int-end)
                    (1+ (- int-end start))
                    (1+ (- end start))))
      (setf sum (* count int-value)))
    
    ;; go though remaining intervals if necessary
    (dolist (int remaining-intervals)
      (if (< end (first int)) (return nil)) ;done
      (let* ((int-start (first int))
             (int-end (second int))
             (int-value (third int))
             (delta-count (if (> end int-end) 
                            (1+ (- int-end int-start))
                            (1+ (- end int-start))))
             )
        (incf count delta-count)
        (incf sum (* delta-count int-value))))
    (/ sum count)))




#| FDSIA testing

(setf foo (make-FDSIA :interval-list 
                      '((0 3 2)(4 7 4)(8 11 2)(12 15 1))
                      :interval-type :start-start))
(setf foo nil)

(do-for-each-time-in-FDSIA
 foo :time time :value value :duration dur :FDSI f
 :body (format t "~%time: ~3a  value: ~3a  dur: ~3a   FDSI ~a"
               time value dur f))
(do-for-each-value-in-FDSIA
 foo :value value :start-time start :end-time end :duration d :FDSI f
 :body (format t "~%value: ~3a  start: ~3a end: ~3a dur: ~3a FDSI ~a" 
               value start end d f))

;; OK
(FDSIA-map-to-interval-list 
 foo '((0 5 0.2)(6 10 0.5)(11 15 0.1)))
(FDSIA-map-average-to-interval-list 
 foo '((0 5 0.2)(6 10 0.5)(11 15 0.1)))
(FDSIA-map-to-interval-list 
 foo '((-10 2 0.2)(3 3 0.15)(4 5 0.16)(6 10 0.5)(11 15 0.1)))
(FDSIA-map-average-to-interval-list 
 foo '((-10 2 0.2)(3 3 0.15)(4 5 0.16)(6 10 0.5)(11 15 0.1)))
;; warn and return nil
(FDSIA-map-to-interval-list 
 foo '((5 5 0.2)(6 10 0.5)(11 15 0.1)))
(FDSIA-map-to-interval-list 
 foo '((-10 5 0.2)(6 10 0.5)))

(loop-over-value-range
 i 1 15
 (format t "~%end=~a, ave=~,4f" i
         (average-over-current-and-future-intervals
          1 i '(0 2 1) '((3 4 2)(5 6 3)(7 8 4)(9 100 0)))))

(defun CF-MAP&AVERAGE (fdsia intervals)
  (multiple-value-bind 
    (vals1 ints1) (fdsia-map-to-interval-list fdsia intervals)
    (multiple-value-bind
      (vals2 ints2) (fdsia-map-average-to-interval-list fdsia intervals)
      
      (let ((val 0))
        (loop
          (if (not vals1) (return nil))
          (format t "~%val ~3d time: ~3d dur: ~3d map: ~6,3f ave: ~6,3f"
                  val (FDSIA-val-to-time FDSIA val)
                  (FDSIA-duration-given-start-at 
                   FDSIA (FDSIA-val-to-time FDSIA val))
                  (first vals1) (first vals2))
          (incf val)
          (pop vals1)
          (pop vals2)))
      
      (loop
        (if (and (null ints1) (null ints2)) (return nil))
        (format t "~%map: ~a    ave: ~a"
                (if (first ints1)
                  (format nil "~3d ~3d ~6,3f"
                          (first (first ints1)) (second (first ints1))
                          (third (first ints1)))
                  "--- --- ------")
                (if (first ints2)
                  (format nil "~3d ~3d ~6,3f"
                          (first (first ints2)) (second (first ints2))
                          (third (first ints2)))
                  "--- --- ------"))
        (pop ints1)
        (pop ints2))
      )))

(setf foo (make-FDSIA :interval-list 
                      '((0 3 2)(4 7 4)(8 11 2)(12 15 1)(16 19 0))
                      :interval-type :start-start))
(cf-map&average
 foo '((-10 2 0.2)(3 3 0.15)(4 5 0.16)(6 10 0.5)(11 19 0.1)))
(cf-map&average
 foo '((0 5 0.2)(6 10 0.5)(11 19 0.1)))

(ipcf-to-interval-list 
 (ipcf-from-interval-list '((0 5 0.2)(6 10 0.5)(11 15 0.1))))
(ipcf-to-interval-list
 (ipcf-from-interval-list '((0 5 0.2) (8 10 0.5) (11 11 0.1))))

(FDSIA-first-val foo); -> 0
(FDSIA-last-val foo); -> 9
(FDSIA-earliest-start foo); -> 0
(FDSIA-earliest-finish foo); -> 2
(FDSIA-latest-start foo); -> 11
(FDSIA-latest-finish foo); -> 13
(FDSIA-val-count foo); -> 10
(FDSIA-val-to-time foo -1); -> nil nil
(dotimes (i (1+ (FDSIA-val-count foo)))
  (multiple-value-bind (start end)
                       (FDSIA-val-to-time foo i)
    (format t "~%val: ~3d   start-end:  ~3d-~3d" i start end)))
(dotimes (i 14)
  (format t "~%time: ~3d val: ~3d" i (FDSIA-time-to-val foo i)))
(progn
  (fdsia-overlap-exclusion foo -10 -5)
  (fdsia-overlap-exclusion foo -2 0)
  (fdsia-overlap-exclusion foo -2 1)
  (fdsia-overlap-exclusion foo 1 3)
  (fdsia-overlap-exclusion foo 1 6)
  (fdsia-overlap-exclusion foo 6 10)
  (fdsia-overlap-exclusion foo 7 15)
  (fdsia-overlap-exclusion foo 9 15)
  (fdsia-overlap-exclusion foo 12 20)
  (fdsia-overlap-exclusion foo 13 20)
  )
|#


(defun ARRAY-FIND-FIRST-GREATER-THAN-OR-EQUAL (array value key)
  "input array and key function which can be applied to an array
    element and which returns a numeric value.  Array must be
    in STRICTLY ascending order by value returned by key.  
    Find first element (i.e. with smallest index) with 
     (funcall key element) >= value.
    Return multiple values:  array-element and index, or both 
    nil if not in range"
  
  (let* ((size (array-dimension array 0))
         (first-index 0)
         (first-value (funcall key (aref array first-index)))
         (last-index (1- size))
         (last-value (funcall key (aref array last-index))))
    (cond
     ;; out of bounds?
     ((> value last-value)
      (values nil nil))
     ((= value last-value)
      (values (aref array last-index) last-index))
     ((<= value first-value)
      (values (aref array first-index) first-index))
     (t ; in bounds, start search
      (loop
        ;; exit condition
        (if (or (= first-index last-index)
                (and (= last-index (1+ first-index))
                     (> value first-value)
                     (<= value last-value)))
          (return nil))
        (let* ((mid-index (+ first-index (round (- last-index first-index) 2)))
               (mid-value (funcall key (aref array mid-index))))
          (cond ((= value mid-value)
                 (setf last-index mid-index
                       last-value mid-value)
                 ;;(format t "~%range: ~d - ~d DONE" first-index last-index)
                 (return nil)
                 )
                ((> value mid-value)
                 (setf first-index mid-index
                       first-value mid-value)
                 ;;(format t "~%range: ~d - ~d" first-index last-index)
                 )
                (t (setf last-index mid-index
                         last-value mid-value)
                   ;;(format t "~%range: ~d - ~d" first-index last-index)
                   )))) ;; end loop
      (values (aref array last-index) last-index)))))


(defun ARRAY-FIND-LAST-LESS-THAN-OR-EQUAL (array value key)
  "input array and key function which can be applied to an array
    element and which returns a numeric value.  Array must be
    in STRICTLY ascending order by value returned by key.  
    Find last element (i.e. with largest index) with 
     (funcall key element) <= value.
    Return multiple values:  array-element and index, or both 
    nil if not in range"
  
  (let* ((size (array-dimension array 0))
         (first-index 0)
         (first-value (funcall key (aref array first-index)))
         (last-index (1- size))
         (last-value (funcall key (aref array last-index))))
    (cond
     ;; out of bounds?
     ((< value first-value)
      (values nil nil))
     ((= value first-value)
      (values (aref array first-index) first-index))
     ((>= value last-value)
      (values (aref array last-index) last-index))
     (t ; in bounds, start search
      (loop
        ;; exit condition
        (if (or (= first-index last-index)
                (and (= last-index (1+ first-index))
                     (>= value first-value)
                     (< value last-value)))
          (return nil))
        (let* ((mid-index (+ first-index (round (- last-index first-index) 2)))
               (mid-value (funcall key (aref array mid-index))))
          (cond ((= value mid-value)
                 (setf first-index mid-index
                       first-value mid-value)
                 ;;(format t "~%range: ~d - ~d DONE" first-index last-index)
                 (return nil)
                 )
                ((> value mid-value)
                 (setf first-index mid-index
                       first-value mid-value)
                 ;;(format t "~%range: ~d - ~d" first-index last-index)
                 )
                (t (setf last-index mid-index
                         last-value mid-value)
                   ;;(format t "~%range: ~d - ~d" first-index last-index)
                   )))) ;; end loop
      (values (aref array first-index) first-index)))))

#| Test binary array search:
(dotimes (i 10)
  (multiple-value-bind 
    (element index)
    (array-find-last-less-than-or-equal #(2 4 6 7 8) i #'identity)
    (format t "~%i: ~3d elt: ~3d  index: ~3d" i element index)))
(dotimes (i 10)
  (multiple-value-bind 
    (element index)
    (array-find-first-greater-than-or-equal #(2 4 6 7 8) i #'identity)
    (format t "~%i: ~3d elt: ~3d  index: ~3d" i element index)))
|#



#| laydown activity across multiple interrutions

(laydown t t 3 0 '((0 1)(2 3)(4 5)(6 10)) nil nil nil nil nil 0)
(laydown t t 3 5 '((0 1)(2 3)(4 5)(6 10)) nil nil nil nil nil 0)
(laydown t t 3 0 '((0 1)(2 3)(4 5)(6 10)) 2 nil nil nil nil 0)
(laydown t t 3 0 '((0 1)(2 3)(4 5)(6 10)) 1 0 nil nil nil 0)
(laydown t t 5 0 '((0 1)(2 3)(4 6)(10 20)) 1 nil nil nil nil 0)
(laydown t t 5 0 '((0 1)(2 3)(4 6)(10 20)) 2 nil nil nil nil 0)
(laydown t t 5 0 '((0 1)(2 3)(4 6)(10 20)) 3 nil nil nil nil 0)

;; backward
(laydown t nil 5 14 '((10 20)(4 6)(2 3)(0 1)) 1 nil nil nil nil 0)
(laydown t nil 5 10 '((10 20)(4 6)(2 3)(0 1)) 1 nil nil nil nil 0)

;; w/seg overhead
(laydown t t 5 0 '((0 1)(2 3)(4 6)(10 20)) 1 nil 4 nil nil 0)
|#

(defun LAYDOWN (task forward
                     remaining-dur t0 i-list 
                     min-seg-dur max-seg-sep seg-overhead
                     reverse-done-segs reverse-i-list cum-overhead)
  ;; forward is t or nil: if t, i-list must be in increasing time order
  ;; cum-overhead is total overhead in done segments
  ;; seg-overhead is overhead added per segment. nil if none applies
  ;;  any overhead in first segment should be included in remaining-dur
  ;; i-list is remaining scheduling intervals ((t1 t2) (t3 t4)...)
  ;; reverse-done-segs is reverse order list of already scheduled
  ;;  segments ((start end) ...).  Times include any seg overheads added.
  ;; max-seg-sep is max end-to-start separation of segments.  nil if 
  ;;  no separation limit to be applied
  ;; min-seg-dur is minimum segment duration. nil if no minimum applies.
  ;;  this should not include seg-overhead
  (cond 
   
   ((< remaining-dur 0) (error "remaining-dur ~a < 0" remaining-dur))

   ;; this should apply more flexible criteria on remaining duration
   ((= 0 remaining-dur)
    ;; done, fit
    ;;(format t "~%done, fit")
    (values t reverse-done-segs reverse-i-list cum-overhead))
   
   ((null i-list)
    ;; done, won't fit (no segments left)
    ;;(format t "~%done, won't fit - no segments left")
    (values nil reverse-done-segs reverse-i-list cum-overhead))
   
   ((let (;; if forward: start of next scheduling interval
          ;; if backward: end of 'previous' sched interval
          (t1 (if forward 
                (first (first i-list))
                (second (first i-list))))
          ;; if forward: end time previous scheduled segment
          ;; if backward: start time 'next' segment
          ;; may be nil if none scheduled
          (prev-seg (if forward 
                      (second (first reverse-done-segs))
                      (first (first reverse-done-segs))))
          )
      (and max-seg-sep prev-seg (> (abs (- t1 prev-seg)) max-seg-sep)))
    ;; done, violates seg separation limit
    ;;(format t "~%done, violates seg separation")
    (values nil reverse-done-segs reverse-i-list cum-overhead))
   
   (t
    (let* ((next-interval (first i-list))
           ;; start of next scheduling interval
           (t1 (first next-interval))
           ;; end of next sched interval
           (t2 (second next-interval))
           (overhead (if (and reverse-done-segs seg-overhead)
                       seg-overhead 0))
           (min-time-needed (+ (or min-seg-dur 0) overhead))
           )
      ;; move t0 if needed
      (setf t0 (if forward (max t0 t1) (min t0 t2)))
      (cond
       ;; two cases: either a piece fits, or not
       ((or (if forward (>= t0 t2) (<= t0 t1))
            (if forward 
              (< (- t2 t0) min-time-needed)
              (< (- t0 t1) min-time-needed)))
        ;; won't fit
        ;;(format t "~%trying next segment after ~a" next-interval)
        (laydown
         task forward remaining-dur t0 (rest i-list )
         min-seg-dur max-seg-sep seg-overhead
         reverse-done-segs 
         (cons next-interval reverse-i-list)
         cum-overhead))
       ;; fits
       (t
        (let ((seg-size (min remaining-dur (if forward (- t2 t0) (- t0 t1)))))
          ;;(if forward
            ;;(format t "~%fit piece forward ~a in ~a ~a" seg-size t0 t2)
            ;;(format t "~%fit piece backward ~a in ~a ~a" seg-size t1 t0))
          (laydown
           task forward
           (- remaining-dur seg-size)
           t2 (rest i-list )
           min-seg-dur max-seg-sep seg-overhead
           (cons (if forward (list t0 (+ t0 seg-size))
                     (list (- t0 seg-size) t0))
                 reverse-done-segs)
           (cons next-interval reverse-i-list)
           (+ cum-overhead overhead)))))))))



#|
(laydown-all t 2 '((0 3)(4 5)(6 7)(8 12)) 2 0 0 :details t :verbose t)
(laydown-all t 2 '((0 3)(4 5)(6 7)(8 12)) 1 0 0 :details t)
(laydown-all t 3 '((0 3)(4 5)(6 7)(8 20)) 1 0 0 :details t)
(laydown-all t 3 '((0 3)(4 5)(6 7)(8 20)) 1 0 1 :details t :verbose t)
(laydown-all t 3 '((0 3)(4 5)(6 7)(8 12)) 2 0 0 :details t)

Note the following version only attempts to lay down tasks at the
start of intervals:  if they fit in one interval, then they are allowed
to slide within the interval boundaries.  

(laydown-all-test :dur 3 :min-seg-dur 1 :rel-dur-adjust 0 :abs-dur-adjust 1
                  :intervals '((0 3)(4 5)(6 7)(8 20)) :default-dur :nominal)
(laydown-all-test :dur 3 :min-seg-dur 1 :rel-dur-adjust 0 :abs-dur-adjust 1
                  :intervals '((0 3)(4 5)(6 7)(8 20)) :default-dur :min)
(laydown-all-test :dur 20 :min-seg-dur 5 :rel-dur-adjust 0.2 :abs-dur-adjust 5
                  :intervals '((0 50)) :default-dur :nominal)

|#


(defun LAYDOWN-ALL-TEST (&key dur min-seg-dur rel-dur-adjust abs-dur-adjust
                              intervals (default-dur :nominal))
  (multiple-value-bind 
    (dur-list eff-list details)
    (laydown-all t dur intervals min-seg-dur rel-dur-adjust abs-dur-adjust
                 :details t :default-dur default-dur)
    (declare (ignore eff-list))
    ;; adjust dur-list to be (early-start late-start duration)
    (dolist (int dur-list)
      (setf (second int)
            (- (second int) (third int))))
    (format t "~%dur-list: ~a" dur-list)
    (let ((fdsia (make-fdsia :interval-list dur-list
                             :interval-type :start-start))
          (err nil))
      (do-for-each-value-in-fdsia 
       FDSIA :value value :start-time start-time :end-time end-time 
       :duration duration :FDSI FDSI :body
       (let* ((sched-ints (SCHED-INTERVALS-FROM-LAYDOWN start-time details))
              (start-time2 (first (first sched-ints)))
              (end-time2 (second (first (last sched-ints))))
              (duration2 (- end-time2 start-time2)))
         (format t "~%val ~3a ~3a-~3a dur=~3a ~3a ~a"
                 value start-time end-time duration 
                 (if (and (= start-time start-time2)
                          (= end-time end-time2)
                          (= duration duration2))
                   "OK" (progn (setf err t) "ERR"))
                 sched-ints)))
      (when err
        (error "error detected in laydown-test!!!"))
      )))

                       

(defun LAYDOWN-ALL (task dur intervals min-seg-dur
                         rel-dur-adjust
                         abs-dur-adjust
                         &key 
                         (details nil)
                         (default-dur :min)
                         (verbose nil))
  "Args:
    dur - nominal duration (required)
    intervals - list of (start end) intervals within which task must
     be scheduled
    min-seg-dur - minimum duration of any segment
    rel-dur-adjust - fraction of dur which may be added or subtracted
     to/from nominal duration, nil if not applicable
    abs-dur-adjust - time which may be added/subtracted to/from nominal
     duration, nil if not applicable
     If both rel and abs adjustments are provided, then whichever gives
     the largest potential adjustment is used.
   --keyword args--
    :details - if t, return list of scheduled intervals for each start time
     as a list of 'specifications' in the form:
      (spec1 spec2 ...)
     where a speci is (t1 t2 ((s1 e1)(s2 e2)...))
      where  t1 and t2 are early start and late start, and 
      (s e)(s e)... lists are start and end times of scheduled intervals 
      given start times at t1.  Must be adjusted for start times >t1 and <= t2.
      Default is nil (no details).
    :default-dur - which duration to use for nominal scheduling.  Legal 
      values are  :min, :nominal, or :max.
    
    All times are integral and in the same units.  Adjustment times are
     calculated then rounded.
    Duration will be extended or shortened within limits above to improve
     efficiency.

   Returned multiple values:
    dur-list - list of (t1 t2 d) s.t. task has duration d if 
     started and ended between t1 and t2 inclusive (this is suitable 
     for the interval-list for MAKE-FDSIA of type :start-end)
    eff-list - list of (t1 t2 eff) s.t. task has efficiency eff if
     started and ended between t1 and t2 inclusive
    detailed-schedule-results - see details argument above for format"
  
  ;; this version has no max-seg-sep separation overheads included
  (let* ((dur-abs-min (round (- dur (or abs-dur-adjust 0))))
         (dur-abs-max (round (+ dur (or abs-dur-adjust 0))))
         (dur-rel-min (round (* (- 1 (or rel-dur-adjust 0)) dur)))
         (dur-rel-max (round (* (+ 1 (or rel-dur-adjust 0)) dur)))
         (dur-min (max 1 (min dur-abs-min dur-rel-min)))
         (dur-max (max dur-abs-max dur-rel-max))
         (t0 nil)
         (try-dur nil)
         
         ;; for returned values:
         (dur-list nil)
         (eff-list nil)
         (detail-results nil)
         )
    (when verbose
      (format t "~%dur nominal ~a range ~a to ~a" dur dur-min dur-max))
    
    (do* ((i-list intervals (rest i-list)))
         ((null i-list) nil)
      
      
      (setf t0 (first (first i-list)))
      (setf try-dur default-dur)

      (loop
        (when verbose (format t "~%~a ~a ~a" t0 try-dur i-list))
        
        (multiple-value-bind
          (fit early-start late-end sched-duration sched-efficiency 
               rev-sched-intervals)
          (laydown-forward-adjust-for-efficiency
           :task task :dur dur :dur-min dur-min :dur-max dur-max 
           :min-seg-dur min-seg-dur :t0 t0 :i-list i-list
           :default-dur try-dur)
          
          (when (and fit
                     ;; skip cases where first schedulable time is beyond
                     ;; the current interval:  they will be picked up next
                     ;; time the interval is advanced
                     (= early-start t0))
            (push (list early-start late-end sched-duration) dur-list)
            (push (list early-start late-end sched-efficiency) eff-list)
            (if details
              (push (list early-start (- late-end sched-duration)
                          (reverse rev-sched-intervals))
                    detail-results)))

          ;; exit or adjust t0 and continue loop
          (cond ((or (not fit)
                     (>= t0 (1- (second (first i-list))))
                     )
                 (when verbose (format t "~%  -exit t0 loop"))
                 (return nil)           ; exit loop
                 )
                (t 
                 ;; set t0 to late-start+1
                 (setf t0 (1+ (- late-end sched-duration)))
                 (setf try-dur :min)
                 (when verbose (format t "~%  -t0 -> ~a" t0))
                 ))
          
          )                             ; end multiple-value-bind
        )                               ; end loop
      )                                 ; end do
      
      ;;(format t "~%dur-list: ~a" dur-list)
      ;;(format t "~%eff-list: ~a" eff-list)
      
      (values (nreverse dur-list)
              (nreverse eff-list)
              (nreverse detail-results))))


(defun SCHED-INTERVALS-FROM-LAYDOWN (start laydown-list)
  "input is the detail list returned from LAYDOWN-ALL and a start time.
    return a list of actual scheduled intervals for the specified start.
    return nil if start is not legal."
  (let ((result nil))
    (dolist (item laydown-list)
      (when (and (>= start (first item))
                 (<= start (second item)))
        ;; hit: 2 cases, single interval with possible shift, or multi-interval
        (cond ((= (first item) (second item))
               (setf result (third item)))
              ((= (first item) start)
               (setf result (third item)))
              (t (let* ((int (first (third item)))
                        (t1 (first int))
                        (t2 (second int))
                        (shift (- start (first item))))
                   (setf result (list (list (+ t1 shift) (+ t2 shift)))))))))
    result))
      


#|
(let* ((intervals '((-6 3)(4 5)(6 7)(8 15))))
  (dolist (int intervals)
    (loop-over-value-range
     t0 (first int) (second int)
     (multiple-value-bind
       (fit ES LE dur eff rev-int)
       (LAYDOWN-FORWARD-ADJUST-FOR-EFFICIENCY 
        :task t :dur 3 :dur-min 2 :dur-max 4 :min-seg-dur 1 
        :t0 t0 :i-list intervals :default-dur :max :verbose nil)
       (if (and fit (= t0 ES))
         (format t "~%t0=~3a d=~3a ES=~3a LE=~3a sched int: ~a" t0 
                 (summed-interval-duration rev-int) 
                 ES LE rev-int))))))

|#

(defun LAYDOWN-FORWARD-ADJUST-FOR-EFFICIENCY
       (&key task dur dur-min dur-max min-seg-dur t0 i-list
             (verbose nil)
             (default-dur :nominal))

  "Function to lay out a task in detail in the intervals in i-list
    and adjust the duration between dur-min and dur-max to maximize
    efficiency.  
    Arguments:
     :task - identifier only, not referenced
     :dur - nominal duration
     :dur-min - min allowed duration
     :dur-max - max allowed duration
     :min-seg-dur - min size of scheduled segment
     :t0 - start time for scheduling
     :i-list - list of intervals ((t1 t2)(t3 t4)...) within which task
      must be scheduled
     :verbose - print to standard output if t
     :default-dur can be :min, :max, or :nominal to determine which duration
     duration is used when seeing whether task fits.  If not one of the 
     above three values, :nominal is used with no warning or error."

  (let ((early-start nil)
        (late-end nil)
        (sched-dur nil)
        (sched-efficiency nil)
        (rev-sched-intervals nil))
    
    (multiple-value-bind
      (fit reverse-done-segs reverse-i-list cum-overhead)
      (laydown task t 
               (cond ((eq default-dur :min    ) dur-min)
                     ((eq default-dur :nominal) dur)
                     ((eq default-dur :max    ) dur-max)
                     (t dur))
               t0 i-list min-seg-dur nil nil nil nil 0)
      (declare (ignore cum-overhead))
      
      (cond 
       
       ;; fit in one segment
       ((and fit (= 1 (length reverse-done-segs)))
        (setf sched-dur (summed-interval-duration reverse-done-segs))
        (setf early-start (first (first reverse-done-segs)))
        (setf late-end    (second (first reverse-i-list)))
        (setf sched-efficiency 1)
        (setf rev-sched-intervals reverse-done-segs)
        (when verbose 
          (format t "~% -fit 1 seg: t0=~a, rd=~a" t0 reverse-done-segs)
          (format t "~%   ES ~3a LE ~3a dur ~3a" early-start late-end sched-dur))
        )
       
       
       ;; multi-segment, can either truncate or extend last segment,
       ;; or leave alone
       (fit
        (let* ((total-dur (summed-interval-duration reverse-done-segs))
               (task-start (first (first (last reverse-done-segs))))
               (task-end   (second (first reverse-done-segs)))
               (task-effic (float (/ total-dur
                                     (- task-end task-start))))
               (last-done-seg (first reverse-done-segs))
               (dur-last-seg (- (second last-done-seg) (first last-done-seg)))
               (last-interval (first reverse-i-list))
               (dur-last-interval (- (second last-interval) (first last-interval)))
               
               (dur-if-dropped (- total-dur dur-last-seg))
               (task-end-if-dropped (second (second reverse-done-segs)))
               (effic-if-dropped (float (/ dur-if-dropped
                                           (- task-end-if-dropped task-start))))
               
               (dur-if-extended (+ total-dur (- dur-last-interval dur-last-seg)))
               (task-end-if-extended (second last-interval))
               (effic-if-extended (float (/ dur-if-extended
                                            (- task-end-if-extended task-start))))
               
               (change nil) ; whether to change (extend or drop) or leave alone
               (extend t) ;default whether to extend or not
               )
          
          (when verbose
            (format t "~% -fit n seg: t0=~a, rd=~a" t0 reverse-done-segs)
            (format t "~%  if extended:  effic: ~a dur ~a"
                    effic-if-extended dur-if-extended)
            (format t "~%  if dropped    effic: ~a dur ~a"
                    effic-if-dropped dur-if-dropped))
          
          (cond 
           ;; if effic improved, and within dur limits, then extend
           ((and (>= effic-if-extended effic-if-dropped)
                 (>= dur-if-extended dur-min)
                 (<= dur-if-extended dur-max))
            (setf change t extend t))
           ;; if effic improved, and within dur limits, then drop
           ((and (>= effic-if-dropped effic-if-extended)
                 (>= dur-if-dropped dur-min)
                 (<= dur-if-dropped dur-max))
            (setf change t extend nil))
           ;; if neither of above applies, then choose based on whether
           ;; within duration limits
           ((and (>= dur-if-extended dur-min) (<= dur-if-extended dur-max))
            (setf change t extend t))
           ((and (>= dur-if-dropped dur-min) (<= dur-if-dropped dur-max))
            (setf change t extend nil))
           ;; if nothing still applies, then default
           (t nil))
          
          (when verbose 
            (cond (change
                   (cond (extend (format t "~%   --- EXTEND"))
                         (t (format t "~%   --- DROP"))))
                  (t (format t "~%   --- NO CHANGE"))))
          
          
          (cond ((and change extend)
                 ;;(format t "~%   t0=~a, rd=~a" t0 reverse-done-segs)
                 (setf early-start task-start)
                 (setf late-end task-end-if-extended)
                 (setf sched-dur (- task-end-if-extended task-start))
                 (setf sched-efficiency effic-if-extended)
                 (setf rev-sched-intervals 
                       (cons (list (first last-done-seg) task-end-if-extended) 
                             (rest reverse-done-segs)))
                 )
                ((and change (not extend))
                 ;;(format t "~%   t0=~a, rd=~a" t0 reverse-done-segs)
                 (setf early-start task-start)
                 (setf late-end task-end-if-dropped)
                 (setf sched-dur (- task-end-if-dropped task-start))
                 (setf sched-efficiency effic-if-dropped)
                 (setf rev-sched-intervals (rest reverse-done-segs))
                 )
                (t                    ; no change 
                 (setf early-start task-start)
                 (setf late-end task-end)
                 (setf sched-dur (- task-end task-start))
                 (setf sched-efficiency task-effic)
                 (setf rev-sched-intervals reverse-done-segs))
                ))))
      (values
       fit
       early-start
       late-end
       sched-dur
       sched-efficiency
       rev-sched-intervals))))

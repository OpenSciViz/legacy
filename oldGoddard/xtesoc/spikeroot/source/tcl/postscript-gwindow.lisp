;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; postscript-gwindow.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: postscript-gwindow.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.2  1991/12/01  00:23:57  johnston
;;; minor fixes
;;;
;;; Revision 1.1  1991/11/26  15:43:18  johnston
;;; Initial revision
;;;
;;;


;;; ======================================================================
;;;                                                     CLASS:  PS-GWINDOW

(defclass PS-GWINDOW (IMMED-GWINDOW)
  ((ps-procs 
    :initform '("/rsh"
                "{ dup stringwidth pop neg"
                "  0 rmoveto show } def"
                "/lt {lineto} def"
                "/mt {moveto} def"
                "/beg {gsave setlinewidth newpath} def"
                "/cs  {closepath stroke grestore} def"
                "/cf  {closepath gsave setgray fill grestore grestore} def"
                "/cfs {closepath gsave setgray fill grestore stroke grestore} def"
                "/sg  {stroke grestore} def")
    :reader ps-procs
    :documentation
    "list of strings to be written as part of postscript prolog, defining
      procedures")
   (ps-fonts
    :initform '("Helvetica" 
                "Helvetica-Bold" 
                "Helvetica-Oblique")
    :reader ps-fonts
    :documentation
    "list of font names to put in epilog")
   (filename
    :initform nil
    :documentation
    "name of output postscript file")
   )
  (:documentation "postscript version of gwindow objects"))


;;; ----------------------------------------------------------------------
;;; method modifications

(defmethod INITIALIZE-INSTANCE :AFTER ((self ps-gwindow)
                                       &rest initargs)
  (declare (ignore initargs))
  (with-slots (ps-fonts drawing-destination) self
    (setf ps-fonts '("Helvetica" 
                     "Helvetica-Bold" 
                     "Helvetica-Oblique"))
    ;; set the drawing destination to be t until a prolog is recorded
    (setf drawing-destination t)
    ))


;;; --- override method: no action (immediate window)

(defmethod PLAY-GWINDOW-HISTORY ((g ps-gwindow))
  nil)

;;; --- override method: just restore font list

(defmethod CLEAR-GWINDOW-HISTORY ((g ps-gwindow))
  (setf (slot-value g 'ps-fonts)
        '("Helvetica" 
          "Helvetica-Bold" 
          "Helvetica-Oblique")))


;;; --- before method:  open stream for output

(defmethod RECORD-PROLOG :BEFORE ((g ps-gwindow))
  (with-slots (filename drawing-destination) g
    ;; filename should hold either a filename or t or nil
    (cond 
     ;; no filename: destination is just t
     ((or (not filename) (eq filename t))
      (setf drawing-destination t))
     
     ;; filename: save the stream
     (t (setf drawing-destination
              (open filename
                    :direction :output
                    :if-exists :supersede
                    :if-does-not-exist :create))))))

;;; --- after method: close stream

(defmethod RECORD-WRAPUP :AFTER ((g ps-gwindow))
  (with-slots (drawing-destination) g
    (if (streamp drawing-destination) (close drawing-destination)))
  ;; re-init
  (initialize-instance g))

;;; ----------------------------------------------------------------------
;;; drawing primitives

(defmethod gwindow-prolog ((g ps-gwindow))
  (with-slots (drawing-destination ps-procs) g
    (format drawing-destination "%!PS-Adobe-1.0")
    (format drawing-destination "~%%%Creator: unknown")
    (format drawing-destination "~%%%Title: unknown")
    (format drawing-destination "~%%%CreationDate: ~a" 
            (get-formatted-universal-time))
    (format drawing-destination "~%%%Pages: (atend)")
    (format drawing-destination "~%%%DocumentFonts: (atend)")
    (format drawing-destination "~%%%BoundingBox: 0 0 612 792")
    (format drawing-destination "~%%%EndComments")
    (format drawing-destination "~%% prolog goes here")
    (dolist (proc ps-procs)
      (format drawing-destination "~%~a" proc))
    (format drawing-destination "~%%%EndProlog")
    (format drawing-destination "~%%%Page: ? 1")
    (format drawing-destination "~%initclip")
    (format drawing-destination "~%newpath")))

(defmethod gwindow-wrapup ((g ps-gwindow))
  (with-slots (ps-fonts drawing-destination) g
    (format drawing-destination "~%showpage")
    (format drawing-destination "~%%%Trailer")
    (format drawing-destination "~%%%DocumentFonts:")
    (dolist (font ps-fonts) (format drawing-destination " ~a" font))
    (format drawing-destination "~%%%Pages: ~d" (current-page-number g))
    (format drawing-destination "~%")
    (close drawing-destination)
    ;; set to t so any further output will come to screen
    (setf drawing-destination t)))

(defmethod gwindow-nextpage ((g ps-gwindow))
  (with-slots (drawing-destination) g
    (format drawing-destination "~%showpage")
    (increment-page-number g)
    (format drawing-destination "~%%%Page: ? ~d" (current-page-number g))
    ))

(defmethod gwindow-moveto ((g ps-gwindow) x y)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-moveto")
    (format drawing-destination "~%~,1f ~,1f mt" x y)
    ))

(defmethod gwindow-lineto ((g ps-gwindow) x y linewidth dash1 dash2)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-lineto")
    (format drawing-destination "~%gsave")
    (format drawing-destination "~%~,2f setlinewidth" linewidth)
    (if (and dash1 dash2)
      (format drawing-destination "~%[~d] ~d setdash" dash1 dash2))
    (format drawing-destination "~%~,1f ~,1f lt" x y)
    (format drawing-destination "~%sg")
    ))


(defmethod gwindow-line ((g ps-gwindow) x1 y1 x2 y2 linewidth dash1 dash2)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-line")
    (format drawing-destination "~%~,2f beg" linewidth)
    (if (and dash1 dash2)
      (format drawing-destination "~%[~d] ~d setdash" dash1 dash2))
    (format drawing-destination "~%~,1f ~,1f mt" x1 y1)
    (format drawing-destination "~%~,1f ~,1f lt" x2 y2)
    (format drawing-destination "~%sg")
    ))

(defmethod gwindow-polyline ((g ps-gwindow) xy-list 
                             linewidth dash1 dash2
                             symbol symbol-scale)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-polyline")
    (format drawing-destination "~%~,2f beg" linewidth)
    (if (and dash1 dash2)
	(format drawing-destination "~%[~d] ~d setdash" dash1 dash2))
    (let ((prev-x (first (first xy-list)))
          (prev-y (second (first xy-list))))
      ;; plot the curve first
      (format drawing-destination "~%~,1f ~,1f mt" prev-x prev-y)
      (dolist (xy (rest xy-list))
        (let ((x (first xy))
              (y (second xy)))
          (format drawing-destination "~%~,1f ~,1f lt" x y)
          (setf prev-x x prev-y y)
          ))				; end dolist
      (format drawing-destination "~%sg")
      ;; plot symbol
      (when symbol
	(format drawing-destination "~%~,2f beg" linewidth)
	(dolist (xy xy-list)
	  (let ((x (first xy))
		(y (second xy)))
            (dolist (sym symbol)
              (let ((dx1 (* (first sym) symbol-scale))
                    (dy1 (* (second sym) symbol-scale))
                    (dx2 (* (third sym) symbol-scale))
                    (dy2 (* (fourth sym) symbol-scale)))
                (format drawing-destination "~%~,1f ~,1f mt" 
                        (+ x dx1) (+ y dy1))
                (format drawing-destination "~%~,1f ~,1f lt" 
                        (+ x dx2) (+ y dy2)))))
          )				;end dolist
	(format drawing-destination "~%sg")
	)				; end when symbol
      )))


(defmethod gwindow-polygon ((g ps-gwindow) xy-list 
                            frame fill linewidth dash1 dash2)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-polygon")
    (format drawing-destination "~%~,2f beg" linewidth)
    (if (and dash1 dash2)
      (format drawing-destination "~%[~d] ~d setdash" dash1 dash2))
    (format drawing-destination "~%~,1f ~,1f mt" 
            (first (first xy-list)) (second (first xy-list)))
    (dolist (xy (rest xy-list))
      (format drawing-destination "~%~,1f ~,1f lt" 
              (first xy) (second xy)))
    (cond ((and fill frame)
           (format drawing-destination "~%~,2f cfs" fill))
          ((and fill (not frame))
           (format drawing-destination "~%~,2f cf" fill))
          (t (format drawing-destination "~%cs")))))


(defmethod gwindow-polyseg ((g ps-gwindow) seg-list 
                            linewidth dash1 dash2)
  (with-slots (drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-polyseg")
    (format drawing-destination "~%~,2f beg" linewidth)
    (if (and dash1 dash2)
      (format drawing-destination "~%[~d] ~d setdash" dash1 dash2))
    (dolist (seg seg-list)
      (let ((x1 (first seg)) (y1 (second seg))
            (x2 (third seg)) (y2 (fourth seg)))
        (format drawing-destination "~%~,1f ~,1f mt" x1 y1)
        (format drawing-destination " ~,1f ~,1f lt" x2 y2)))
    (format drawing-destination "~%sg")
    ))

(defmethod gwindow-font ((g ps-gwindow) font-name font-size)
  (with-slots (drawing-destination ps-fonts) g
    (format drawing-destination
            "~%/~a findfont ~d scalefont setfont" font-name font-size)
    (pushnew font-name ps-fonts :test 'string-equal)))

(defmethod gwindow-leftshow ((g ps-gwindow) x y string)
  (with-slots (drawing-destination) g
    (format drawing-destination "~%~,1f ~,1f mt" x y)
    (format drawing-destination "~%(~a) show" string)))

(defmethod gwindow-rightshow ((g ps-gwindow) x y string)
  (with-slots (drawing-destination) g
    (format drawing-destination "~%~,1f ~,1f mt" x y)
    (format drawing-destination "~%(~a) rsh" string)))

;;; ======================================================================
;;;                                                            CONSTRUCTOR

(defun MAKE-PS-GWINDOW (&key (file t))
  ;; return gwindow instance
  (let ((instance (make-instance 'ps-gwindow)))
    (with-slots (filename) instance
      (setf filename file))
    instance))

#|
(setf gp (make-ps-gwindow :file t))
(setf gp (make-ps-gwindow :file "clipper:0pstemp.ps"))
(record-prolog gp)

(record-moveto gp 200 10)
(record-lineto gp 200 50)

(record-line gp 350 50 400 100)
(record-line gp 350 60 400 110 :linewidth 2)
(record-line gp 350 70 400 120 :linewidth 1 :dash1 1 :dash2 0)
(record-line gp 350 80 400 130 :linewidth 0.25 :dash1 2 :dash2 0)
(record-line gp 350 90 400 140 :linewidth 0.25 :dash1 3 :dash2 0)

(record-polyline gp '((540 110) (540 120) (540 130) (540 140)))
(record-polyline gp '((550 110) (550 120) (550 130) (550 140))
                 :symbol '((1 1 -1 -1) (1 -1 -1 1)) :symbol-scale 3)
(record-polyline gp '((560 110) (560 120) (560 130) (560 140))
                 :symbol '((1 1 -1 -1) (1 -1 -1 1)
                           (-1 0 1 0) (0 -1 0 1)) :symbol-scale 3)

(record-polygon gp '((40 40) (70 40) (70 70) (40 70) (40 40)) :frame t)
(record-polygon gp '((50 50) (80 50) (80 80) (50 80) (50 50)) :frame t)
(record-polygon gp '((60 60) (90 60) (90 90) (60 90) (60 60)) :fill 0.25)
(record-polygon gp '((70 70) (110 70) (110 110) (70 110) (70 70)) 
                 :frame nil :fill 0.5)

(record-polyseg gp '((200 200 210 210) (205 200 215 200)))

(record-font gp "Helvetica-Bold" 9)
(record-leftshow gp 40 40 "Now is the time for all good men")
(record-rightshow gp 40 40 "foo")
(record-font gp "Helvetica" 6)
(record-leftshow gp 40 50 "Now is the time for all good men")
(record-font gp "Helvetica-Oblique" 10)
(record-leftshow gp 40 60 "Now is the time for all good men")
(record-font gp "Helvetica" 9)

(record-wrapup gp)
(play-gwindow-history gp)
(clear-gwindow-history gp)
(describe gp)

;;; =============
;;; test higher level calls

(setf gp (make-ps-gwindow :file "clipper:0pstemp.ps"))
(record-prolog gp)

(set-page-loc-of-drawing-rect gp 50 400 50 400)

(box-window gp)
(box-rect gp)

;;; bars
(set-x-window-scale gp 0 10)
(set-y-window-scale gp 0 1)
(plot-interval-bars gp '((0 1)(3 4)(6 8)) 0.8 0.9)
(plot-interval-bars gp '((0 1)(3 4)(6 8)) 0.75 0.8 :fill 0)
(plot-interval-bars gp '((0 1)(3 4)(6 8)) 0.65 0.7 :fill 0.5)
(plot-interval-bars gp '(2 3 5 6 7 8 9 10) 0.4 0.5)

(record-nextpage gp)

;; sine curve
(set-x-window-scale gp 0 6.0)
(set-y-window-scale gp -1.1 1.1)
(setf foo nil)
(do* ((x 0 (+ x 0.1))
      )
     ((> x 6.0) nil)
  (push (cons x (sin x)) foo))
(plot-curve gp foo :wrap-fraction 0 :symbol *cross-symbol*)
(plot-curve gp foo :wrap-fraction 0 :symbol nil)
(label-y-axis gp -1.0 1.0 :tic-value 0.2 :tic-size 3 :font-size 9
              :label-format "~,2f")
(label-x-axis gp 0 6.0 :tic-value 1 :tic-size 3 :font-size 9
              :label-format "~,2f")
(label-x-axis gp 0 6.0 :tic-value 0.5 :tic-size 1 :label nil)

(record-nextpage gp)

(set-x-window-scale gp -1.1 1.1)
(set-y-window-scale gp -1.1 1.1)
;; circle as region
(plot-poly gp (do* ((xy-list nil)
                    (x 0 (+ 0.1 x)))
                   ((> x 2pi) xy-list)
                (push (list (sin x) (cos x)) xy-list)))
(plot-poly gp (do* ((xy-list nil)
                    (x 0 (+ 0.1 x)))
                   ((> x 2pi) xy-list)
                (push (list (* 0.8 (sin x)) (* 0.8 (cos x))) xy-list))
           :frame nil :fill 0.5)
(plot-poly gp (do* ((xy-list nil)
                    (x 0 (+ 0.1 x)))
                   ((> x 2pi) xy-list)
                (push (list (* 0.5 (sin x)) (* 0.5 (cos x))) xy-list))
           :frame nil :fill 0)
(string-at-line-in-rect gp :line-number 1 
                        :string "foo on you")

(record-wrapup gp)
(play-gwindow-history gp)
(clear-gwindow-history gp)

(ps-fonts gp)
(ed "clipper:0pstemp.ps")

|#

BEGIN { i = 0; }
{
  if (match($0, /^\(/)) {
     $0 = "set lisp" i " "$0 ;
     i++;
  }
  gsub(/\(/,"{");
  gsub(/\)/,"}");
  gsub(/^ *\;/, "\#;");
 
  print $0;
}

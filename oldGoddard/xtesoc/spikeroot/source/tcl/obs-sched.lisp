;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; obs-sched.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: obs-sched.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.7  1992/03/11  22:22:56  johnston
;;; put reverse on phase constraint (fixed problem found by Glenn)
;;;
;;; Revision 1.6  1991/12/01  00:22:45  johnston
;;; major rework of time/value calculation for task initialization.
;;; Some minor new methods added for tasks.
;;;
;;; Revision 1.5  1991/11/26  15:42:05  johnston
;;; major improvements
;;;
;;;
;;; classes and methods for general observation scheduling:
;;;
;;; ================================================================
;;;                                                     CLASS: QTIME
;;;
;;; QTIME below refers to 'quantized time':  not (necessarily) ticks as
;;; defined by ABS-TIME & REL-TIME functions in astro-utilities, but a 
;;; linear transformation from TJD to quantized time.  Setup requires 
;;; the following:  TJD corresponding to start, end, and size of quantum
;;; in days. Methods provided to convert to/from and interval specified
;;; as TJD times into quantized time (quanta).

(defclass QTIME ()
  (
   ;; parameters specifying transformation
   (TJD-start 
    :initform nil :reader TJD-start :initarg :TJD-start
    :documentation
    "time (TJD) corresponding to qtime 0")
   (TJD-end
    :initform nil :reader TJD-end :initarg :TJD-end
    :documentation
    "time (TJD) corresponding to end of interval")
   (TJD-quantum
    :initform nil :reader TJD-quantum :initarg :TJD-quantum
    :documentation
    "size of time quantum, in days")
   
   ;; derived fields (values determined at initialization)
   (qtime-start 
    :reader qtime-start
    :documentation "time in quanta corresponding to TJD-start")
   (qtime-end
    :reader qtime-end
    :documentation "time in quanta corresponding to TJD-end")
   (qtime-quanta
    :reader qtime-quanta
    :documentation "number of time quanta from qtime-start to qtime-end")
   
   )
  
  (:documentation "The mapping from TJD to quantized time"))

;;; -----------------------------------------------------------------------
;;;                                                    QTIME INITIALIZATION

(defgeneric INIT-QTIME (qtime &key TJD-start TJD-end TJD-quantum)
  (:documentation
   "Initialize an instance of qtime class:  required arguments are
     qtime - qtime instance
     TJD-start - start time in TJD
     TJD-end - end time in TJD
     TJD-quantum - size of quantum in days"))


(defmethod INIT-QTIME ((qtime qtime) &key TJD-start TJD-end TJD-quantum)
  
  ;; error check
  (unless 
      (and  TJD-start TJD-end TJD-quantum ;; all args must be specified
	    ;; and be numeric
	    (numberp TJD-start) (numberp TJD-end) (numberp TJD-quantum)
	    ;; end must be after start
	    (> TJD-end TJD-start)
	    ;; quantum must be positive
	    (> TJD-quantum 0)
	    ;; must be at least one quantum within start & end
	    (> (- TJD-end TJD-start) TJD-quantum))
    (error "Error in QTIME specification: start ~a end ~a quantum ~a"
           TJD-start TJD-end TJD-quantum))
  
  ;; store basic slot values
  (setf (slot-value qtime 'TJD-start   ) TJD-start)
  (setf (slot-value qtime 'TJD-end     ) TJD-end)
  (setf (slot-value qtime 'TJD-quantum ) TJD-quantum)
  
  ;; derived slots
  (set-qtime-derived-slots qtime)
  ;; returned value
  qtime)


(defgeneric SET-QTIME-DERIVED-SLOTS (qtime)
  (:documentation
   "compute and fill derived slots for QTIME instance"))


(defmethod SET-QTIME-DERIVED-SLOTS ((qtime qtime))
  (with-slots 
    (TJD-start TJD-end TJD-quantum qtime-start qtime-end qtime-quanta) qtime
    (setf qtime-start 0)
    (setf qtime-end (round (/ (- TJD-end TJD-start) TJD-quantum)))
    (setf qtime-quanta (1+ (- qtime-end qtime-start)))))


;;; -----------------------------------------------------------------------
;;;                                               APPLICATION-LEVEL METHODS

;;; valid reader methods:
;;;  (TJD-START QTIME)
;;;  (TJD-END QTIME)
;;;  (TJD-QUANTUM QTIME)

(defgeneric TJD-TO-QTIME (qtime TJD)
  (:documentation "convert time in TJD to time in quanta"))

(defmethod TJD-TO-QTIME ((qtime qtime) TJD)
  ;; replaces (TJD-TO-EUVE-TIME tjd)
  (with-slots (TJD-start TJD-quantum) qtime
	      (round (/ (- TJD TJD-start) TJD-quantum))))


(defgeneric QTIME-TO-TJD (qtime q)
  (:documentation "convert time in quanta 'q to time in TJD"))

(defmethod QTIME-TO-TJD ((qtime qtime) q)
  ;; replaces (EUVE-TIME-TO-TJD time)
  (unless (and (typep qtime 'qtime) (numberp q)	    ;; 11/18/92 REB - test
	       (equalp (- (floor q) q) 0))	    ;;  was just (integerp q)
    (error "invalid arguments to qtime-to-tjd"))
  (with-slots (TJD-start TJD-quantum) qtime
	      (+ TJD-start (* q TJD-quantum))))


(defgeneric DURATION-IN-DAYS-TO-QTIME (qtime days)
  (:documentation "convert duration in days to duration in quanta"))

(defmethod DURATION-IN-DAYS-TO-QTIME ((qtime qtime) days)
  ;; replaces (DAYS-TO-EUVE-DURATION duration)
  (with-slots (TJD-quantum) qtime
	      (round (/ days TJD-quantum))))

(defmethod DURATION-IN-DAYS-TO-FRACTIONAL-QTIME ((qtime qtime) days)
  "returns fractional quantum duration"
  (with-slots (TJD-quantum) qtime
	      (/ days TJD-quantum)))


(defgeneric DURATION-IN-QTIME-TO-DAYS (qtime duration)
  (:documentation "convert duration in quanta 'duration to duration in days"))

(defmethod DURATION-IN-QTIME-TO-DAYS ((qtime qtime) duration)
  ;; replaces (EUVE-DURATION-TO-DAYS duration)
  (with-slots (TJD-quantum) qtime
	      (* duration TJD-quantum)))


(defgeneric CONVERT-TJD-INTERVAL-LIST-TO-QTIME-IPCF (qtime interval-list)
  (:documentation
   "Input is interval list ((t1 t2 val1)(t3 t4 val2)...) where t1...
    are times in TJD.  Convert to ipcf where time is QTIME quantized units"))

(defmethod CONVERT-TJD-INTERVAL-LIST-TO-QTIME-IPCF ((qtime qtime) interval-list)
  (let* ((interval-list2 
          (transform-time-in-interval-list
           interval-list 
           #'(lambda (x) 
               (cond ((= x *minus-infinity*) *minus-infinity*)
                     ((= x *plus-infinity*)  *plus-infinity*)
                     (t (tjd-to-qtime qtime x)))))))
    
    ;; adjust integral times so that end of one interval is < start of next
    ;; check for errors in input time list
    (let ((prev-int (first interval-list2)))
      (dolist (next-int (rest interval-list2))
        ;;(format t "~%~a-~a" prev-int next-int)
        ;; error if times in interval aren't ordered
        (when (< (second prev-int) (first prev-int))
          (error "times in interval list not ordered ~a" prev-int))
        ;; error if intervals out of order
        (when (< (first next-int) (first prev-int))
          (error "intervals out of order ~a ~a" prev-int next-int))
        (when (<= (first next-int) (second prev-int))
          ;;(format t "~%adjusting time: ~a ~a" prev-int next-int)
          (setf (second prev-int)
                (max (first prev-int) 
                     (- (second prev-int)
                        (1+ (- (second prev-int) (first next-int)))))))
        (setf prev-int next-int)))
    
    (ipcf-from-interval-list interval-list2)))


(defgeneric REPORT-QTIME (qtime &key stream title)
  (:documentation "Report times to output stream as lisp comments"))

(defmethod REPORT-QTIME ((qtime qtime) &key (stream t) (title "Schedule times:"))
  (format stream "~%;;; ~a" title)
  (let ((start (qtime-start qtime))
        (end   (qtime-end   qtime)))
    (format stream "~%;;;  Start: quantized time ~4d, TJD ~8,3f (~a)"
            start (qtime-to-tjd qtime start) 
            (format-abs-time (qtime-to-tjd qtime start)))
    (format stream "~%;;;  End  : quantized time ~4d, TJD ~8,3f (~a)"
            end (qtime-to-tjd qtime end)
            (format-abs-time (qtime-to-tjd qtime end)))
    (format stream "~%;;;  Number of quantized time values: ~d" 
            (qtime-quanta qtime))
    (format stream "~%;;;  One time unit (quantum) is ~f days"
            (duration-in-qtime-to-days qtime 1))))

#|  TEST
(setf qq (make-instance 'qtime))
(init-qtime qq 
            :tjd-start (dmy-to-time '1-jan-1991)
            :tjd-end (dmy-to-time '30-jan-1991)
            :tjd-quantum 0.5)
(describe qq)
(qtime-start qq)
(qtime-end qq)
(qtime-quanta qq)

(TJD-start qq)
(TJD-end qq)
(TJD-quantum qq)

(TJD-to-qtime qq (dmy-to-time '15-jan-1991))
(qtime-to-TJD qq 0)
(qtime-to-TJD qq 15)
(duration-in-days-to-qtime qq 10)
(duration-in-qtime-to-days qq 10)


(format-abs-time (qtime-to-tjd qq (qtime-start qq)))
(format-abs-time (qtime-to-tjd qq (qtime-end qq)))
(report-qtime qq :title "Fred")

|#
;;; ======================================================================
;;;                                                       CLASS:  TEST-SEG
;;;
;;; this class combines quantized time and segmentations:  they provide
;;; a way to check times and segment boundaries for consistency before
;;; creating a full OBS-CSP-CHRONIKA
;;;
;;; *** TODO: upgrade this to handle arbitrary segment lists

(defclass TEST-SEG
    (QTIME TIME-TO-SEGMENT-MAPPING)
    ((initialized :initform nil :accessor initialized))
  (:documentation "test combination of quanta and segments"))

(defvar *TEST-SEG* nil)

(defun TEST-SEG-INIT-QTIME (&key tjd-start tjd-end tjd-quantum)
  "setup test instance for quantum time and segments.  Inputs are:
    tjd-start - TJD, start of time range
    tjd-end - TJD, end of time range
    tjd-quantum - days"
  (if (null *test-seg*) (setf *test-seg* (make-instance 'test-seg)))
  (init-qtime *test-seg* :tjd-start tjd-start :tjd-end tjd-end :tjd-quantum tjd-quantum))

(defun TEST-SEG-INIT (&key tjd-start tjd-end tjd-quantum segment-start segment-duration segment-count)
  "setup test instance for quantum time and segments.  Inputs are:
    tjd-start - TJD, start of time range
    tjd-end - TJD, end of time range
    tjd-quantum - days
    segment-start - TJD, start of first segment
    segment-duration - in units of of quanta
    segment-count - # of segments"
  (if (null *test-seg*) (setf *test-seg* (make-instance 'test-seg)))
  (init-qtime *test-seg* :tjd-start tjd-start :tjd-end tjd-end :tjd-quantum tjd-quantum)
  (init-time-to-segment-mapping *test-seg*
				:segment-start (TJD-to-qtime *test-seg* segment-start)
				:segment-duration (* segment-duration (duration-in-days-to-qtime *test-seg* tjd-quantum))
				:segment-count segment-count)
  (setf (initialized *test-seg*) t))

(defun TEST-SEG-STATUS ()
  "return nil if segments ok, else error string"
  (when (and *test-seg* (initialized *test-seg*))
    (let ((q1 (qtime-start *test-seg*))
	  (q2 (qtime-end *test-seg*))
	  (s1 (start-of-first-segment *test-seg*))
	  (s2 (end-of-last-segment *test-seg*))
	  (err nil)
	  (result nil))
      (unless (= q1 s1)
	(setf err (format nil "schedule and segment start mismatch"))
	(setf result (if result (format nil "~a~%~a" result err) err)))
      (unless (= q2 s2)
	(setf err (format nil "schedule and segment end mismatch"))
	(setf result (if result (format nil "~a~%~a" result err) err)))
      result)))

(defun TEST-SEG-QTIME-DESCRIPTION ()
  (when (and *test-seg* (initialized *test-seg*))
    (with-output-to-string (stream)
      (let ((start (qtime-start *test-seg*))
	    (end   (qtime-end   *test-seg*)))
	(format stream   "Start: ~4d, TJD ~8,3f (~a)"
		start (qtime-to-tjd *test-seg* start) 
		(format-abs-time (qtime-to-tjd *test-seg* start)))
	(format stream "~%End  : ~4d, TJD ~8,3f (~a)"
		end (qtime-to-tjd *test-seg* end)
		(format-abs-time (qtime-to-tjd *test-seg* end)))
	(format stream "~%Number of (~ad) quanta: ~d" 
		(duration-in-qtime-to-days *test-seg* 1) (qtime-quanta *test-seg*))))))

(defun TEST-SEG-DESCRIPTION ()
  "string representation of segments"
  (when (and *test-seg* (initialized *test-seg*))
    (with-output-to-string (stream)
      (format stream "~d segments" (total-number-of-segments *test-seg*))
      (format stream "~%  start of first ~5d TJD ~9,4f (~a)"
	      (start-time-of-first-segment *test-seg*)
	      (qtime-to-TJD *test-seg* (start-time-of-first-segment *test-seg*))
	      (format-abs-time 
	       (qtime-to-TJD *test-seg* (start-time-of-first-segment *test-seg*))))
      (format stream "~%  end of last    ~5d TJD ~9,4f (~a)"
	      (end-time-of-last-segment *test-seg*)
	      (qtime-to-TJD *test-seg* (end-time-of-last-segment *test-seg*))
	      (format-abs-time 
	       (qtime-to-TJD *test-seg* (end-time-of-last-segment *test-seg*))))
      (format stream "~%  duration ~d quanta (days: ~,4f)"
	      (duration-of-uniform-segment *test-seg*)
	      (duration-in-qtime-to-days *test-seg*
					 (duration-of-uniform-segment *test-seg*)))

      #|=== excluded
    (dotimes (i (total-number-of-segments *test-seg*))
      (format stream "~%seg: ~3d  ~3d-~3d (TJD ~9,4f-~9,4f) ~a-~a" i 
              (start-of-segment *test-seg* i)
              (end-of-segment *test-seg* i)
              (qtime-to-TJD *test-seg*  (start-of-segment *test-seg* i))
              (qtime-to-TJD *test-seg*  (end-of-segment *test-seg* i))
              (format-abs-time 
               (qtime-to-TJD *test-seg* (start-of-segment *test-seg* i)))
              (format-abs-time 
               (qtime-to-TJD *test-seg* (end-of-segment *test-seg* i)))))
      ===|#    
      )))


#|
(test-seg-init :tjd-start 50 :tjd-end 60 :tjd-quantum 0.5
	       :segment-start 50 :segment-count 10 :segment-duration 2)
(describe *test-seg*)
(report-qtime *test-seg*)
(show-segments *test-seg*)
(format t "~%start of first: ~a end of last: ~a"
	(start-of-first-segment *test-seg*) (end-of-last-segment *test-seg*))

(test-seg-init :tjd-start (dmy-to-time '2-jan-1992) 
               :tjd-end (dmy-to-time '30-jun-1992) :tjd-quantum  1.0
               :segment-start (dmy-to-time '1-jan-1992) 
               :segment-count 26 :segment-duration 7)
(print (test-seg-status)) ;; start mismatch

(test-seg-init :tjd-start (dmy-to-time '1-jan-1992) 
               :tjd-end (dmy-to-time '15-jul-1992) :tjd-quantum  1.0
               :segment-start (dmy-to-time '1-jan-1992) 
               :segment-count 26 :segment-duration 7)
(print (test-seg-status)) ;; end mismatch
(print (test-seg-description))
|#


;;; ======================================================================
;;;                                                   CLASS:  OBS-CHRONIKA
;;; a chronika with quantized time built in
;;;

(defclass OBS-CHRONIKA (CHRONIKA QTIME)
  ((obs-csp-chronika 
    :initform nil :reader obs-csp-chronika
    :documentation "obs-csp-chronika instance to which this obs-chronika belongs"))
  (:documentation 
   "A chronika with a mapping from TJD to quantized time.
    Chronika times are in terms of quantized time."))


(defmethod PRINT-OBJECT ((chr obs-chronika) stream)
  (format stream "#<OBS-CHRONIKA ~a>" (name chr)))

;;; ----------------------------------------------------------------------
;;; OBS-CHRONIKA initialization

(defmethod INIT-CHRONIKA ((chr obs-chronika) args)

  "args required for initting qtime:
    :TJD-start, :TJD-end, :TJD-quantum, :obs-csp-chronika"

  ;; init the time quantization
  (init-qtime chr 
              :tjd-start (getf args :TJD-start)
              :tjd-end (getf args :TJD-end)
              :tjd-quantum (getf args :TJD-quantum))
  ;; record quantized start and end in chronika
  (setf (start chr) (qtime-start chr))
  (setf (end   chr) (qtime-end   chr))
  ;; record obs-csp-chronika
  (setf (slot-value chr 'obs-csp-chronika) (getf args :obs-csp-chronika))
  chr)


#| TEST
(make-chronika :name :fred
               :keep-history nil
               :chronika-type 'obs-chronika
               :tjd-start (dmy-to-time '1-jan-1991)
               :tjd-end (dmy-to-time '30-jan-1991)
               :tjd-quantum 0.5)
(describe *current-chronika*)
|#


;;; ======================================================================
;;;                                                       CLASS:  OBS-TASK
;;;
;;; a task representing an observation:  slots for name, exposure time, 
;;; exposure time, var (for CSP connection), priority, and variable duration
;;; depending on time scheduled


(defclass OBS-TASK (TASK)
  (
   (target-name
    :initform "???" :reader target-name :documentation
    "name of target, string")
   (exp-time
    :initform nil :reader exp-time :documentation
    "Requested exposure time, seconds")
   (task-var
    :initform nil :reader task-var :documentation
    "Instance of CSP var corresponding to this task")
   (priority
    :initform nil :reader priority :documentation
    "Priority value of task, lower numbers mean higher priority")
   
   (FDSIA
    :initform nil :reader FDSIA :documentation
    "Instance of Fixed Duration Scheduling Interval Array for this
      task.  This records timing data for the task.  See short-term.lisp
      for details.")
   (efficiency-ipcf
    :initform nil :reader efficiency-ipcf :documentation
    "ipcf, time in quanta, value is efficiency of observing at time.
      Used to convert exp-time to elapsed time.")
   (duration-ipcf
    :initform nil :reader duration-ipcf :documentation
    "ipcf, time in quanta, value is duration of observing if started
      at time, in quanta.  Duration may be integral quanta or not, depending
      on desired interpretation.")
   (value-preference-list
    :initform nil :reader value-preference-list :documentation
    "list of preference values for legal start times.  Computed by 
      init-task from abs suitability, used to set preferences for CSP.")
   
   )
  (:documentation "task representing an observation"))


(defmethod PRINT-OBJECT ((obj OBS-TASK) stream)
  (format stream "#<OBS-TASK ~a Prio ~d exp ~ds>" 
          (slot-value obj 'target-name)
          (slot-value obj 'priority)
          (slot-value obj 'exp-time)))

;;; ----------------------------------------------------------------------
;;; OBS-TASK initialization
;;;
;;; Note: MAKE-TASK supports the following basic keyword arguments:
;;;   (minimum-duration 0)
;;;   (maximum-duration nil)
;;;   (id nil)
;;;   (chr *current-chronika*)
;;;   (task-type 'task)
;;; These arguments, and any others, are passed to init-task.
;;; If id is nil, one is generated

(defmethod INIT-TASK :AFTER ((task obs-task) args)
  
  "Initialization method for obs-tasks.  These should be provided as arguments
    to make-task.
    :name - target name (string)
    :exp-time - initial exposure time (seconds)
    :priority - priority, number, lower numbers mean higher priority
    :available-time-intervals - interval list, time in TJD, 
      value is fraction of wall-clock time that is available for exposing
      if observation taken during interval.
      This is effectively observing efficiency.
    :abs-constraint-intervals - interval list, time in TJD, 
      value is suitability for taking data at time.  Note that this
      is NOT start time suitability.
    :abs-constraint - instance of abs constraint for task suitability
    :suitability-scale - integral value corresponding to suitability 1
    :default-priority - if priority not specified, this is value used"
  
  (let (
        ;; (chr (getf args :chr))
        (name (getf args :name))
        (exp-time (getf args :exp-time))
        (priority (getf args :priority))
        (default-priority (getf args :default-priority))
        (abs-constraint (getf args :abs-constraint))
        (suitability-scale (getf args :suitability-scale))
        
        ;; derived
        (fdsia nil))
    
    ;; error check args
    (let ((ok t))
      (when (not name) 
        (spike-diagnostic "INIT-TASK: no task name")
        (setf OK nil))
      (when (or (not exp-time) (not (numberp exp-time)))
        (spike-diagnostic "INIT-TASK: no exp-time, or not numeric")
        (setf ok nil))
      (when (not (or priority default-priority))
        (spike-diagnostic "INIT-TASK: no task priority or default")
        (setf ok nil))
      (when (not abs-constraint)
        (spike-diagnostic "INIT-TASK: no abs-constraint instance")
        (setf ok nil))
      (when (or (not suitability-scale) (not (numberp suitability-scale)))
        (spike-diagnostic "INIT-TASK: no suitability-scale, or not numeric")
        (setf ok nil))
      (when (not ok)
        (error "error in arguments: ~a" args)))
    
    (multiple-value-bind
      (efficiency-ipcf duration-ipcf duration-interval-list abs-interval-list)
      (init-task-duration-ipcf task args)
      
      (setf fdsia (make-fdsia :interval-list duration-interval-list
                              :interval-type :start-start))
      
      ;; get abs suitability and record
      (multiple-value-bind
        (suit-list suit-interval-list)
        (fdsia-map-average-to-interval-list fdsia abs-interval-list)
        
        ;; construct abs task constraint iPCF
        (add-abs-task-window 
         abs-constraint task (ipcf-from-interval-list suit-interval-list))
        
        ;; store in slots
        (setf (slot-value task 'duration-ipcf) duration-ipcf)
        (setf (slot-value task 'efficiency-ipcf) efficiency-ipcf)
        (setf (slot-value task 'fdsia) fdsia)
        (setf (slot-value task 'target-name) name)
        (setf (slot-value task 'exp-time) exp-time)
        (setf (slot-value task 'priority) (or priority default-priority))
        (setf (slot-value task 'minimum-duration) 
              (or (fdsia-min-duration fdsia) 0))
        (setf (slot-value task 'maximum-duration) (fdsia-max-duration fdsia))
        
        ;; save value preferences
        (setf (slot-value task 'value-preference-list)
              (mapcar #'(lambda (x) 
                          (round (* x suitability-scale)))
                      suit-list))
        )))
  task)


;;; --- auxiliary methods for INIT-TASK ---

(defmethod INIT-TASK-DURATION-IPCF ((task obs-task) args)
  
  "This method is called with task instance and the args plist given to
    make-task and init-task.  It requires the following indicators in
    the args plist:
     :chr - chronika
     :exp-time - exposure time, seconds
     :available-time-intervals - list of (t1 t2 efficiency) where times are TJD
     :abs-constraint-intervals - list of (t1 t2 suit) where times are TJD
    Note that times in above two interval lists are for overlapping interval,
    not starting in interval.
    This method returns multiple values:
    efficiency-ipcf - the ipcf for recording efficiency to store in the 
     task slot
    duration-ipcf - the ipcf possibly with fractional quanta duration for
     recording in the task slot.
    duration-interval-list - an interval list of the form
     ((early-start late-start duration)...) for constructing the FDSIA.  
     times and duration values must be integral.
    abs-interval-list - interval list ((start end suit)...), in qtime, 
     with suitability of overlapping interval
   All args available to init-task are available in args plist."
  
  (let* ( ;; needed args
         (chr (getf args :chr))
         (exp-time (getf args :exp-time))
         (available-time-intervals (getf args :available-time-intervals))
         (abs-constraint-intervals (getf args :abs-constraint-intervals))
         
         ;; derived quantities
         (fractional-exp-qtime (duration-in-days-to-fractional-qtime 
                                chr (/ exp-time 86400.0)))
         (efficiency-ipcf (convert-tjd-interval-list-to-qtime-ipcf
                           chr available-time-intervals))
         (abs-ipcf (convert-tjd-interval-list-to-qtime-ipcf
                    chr abs-constraint-intervals))
         (abs-interval-list (ipcf-to-interval-list 
                             abs-ipcf (qtime-start chr) (qtime-end chr)))
         
         )
    
    (multiple-value-bind
      (duration-interval-list duration-ipcf)
      (compute-duration-ipcf 
       task chr efficiency-ipcf abs-ipcf fractional-exp-qtime)
      
      ;; returned values
      (values efficiency-ipcf duration-ipcf
              duration-interval-list abs-interval-list))))


;;; The duration-ipcf is stored in the task slot and is used in the
;;; DURATION-AT-TIME and EXPOSURE-DURATION-AT-TIME methods
;;; (these are new methods for obs-tasks, not to be confused with 
;;; existing task methods duration-given-start-at etc. which use
;;; use the FDSIA to return integral time only).
;;; It is also used in the SEGMENTED-OBS-CSP class as the time
;;; resource requirement for the task 
;;; (see SEGMENTED-OBS-CSP-SET-REQ-CAPACITY-CACHE).  
;;; 
;;; The duration-interval-list is used to construct the FDSIA (in
;;; the INIT-TASK :AFTER method for OBS-TASKS.  It must represent
;;; integral (quantized) durations only.  It is calculated by
;;; INIT-TASK-DURATION-IPCF, and by default in COMPUTE-DURATION-IPCF.
;;; The meaning of each interval (t1 t2 dur) is: task can START between
;;; t1 and t2, inclusive, and if so has duration dur.  t1, t2, and dur
;;; are all integral.  dur can be legitimately zero.


(defmethod COMPUTE-DURATION-IPCF ((task obs-task)
                                  (chr obs-chronika)
                                  efficiency-ipcf
                                  abs-ipcf
                                  fractional-exp-qtime)
  
  "Supplemental method for OBS-TASK initialization.  Input is
     task - task instance
     chr - chronika instance
     efficiency-ipcf - ipcf, time in qtime, value is efficiency at qtime
     abs-ipcf - ipcf, time in qtime, value is suitability at qtime
      Note: this is not suitability of STARTING at qtime, but suitability
      of operating at time.  Here it only matters if suitability is zero or not.
     fractional-exp-qtime - exposure time, in fractional qtime
    Returns two values:
     duration-interval-list - quantized duration of task if started in
      interval, including end times
     duration-ipcf (possibly fractional) - duration in qtime of task if
      started at qtime"
  
  (duration-ipcf-and-intervals
   efficiency-ipcf abs-ipcf fractional-exp-qtime 
   (qtime-start chr) (qtime-end chr)
   :min-duration-limit 1
   :get-integral-duration-by :round
   :remove-intervals-with-zero-task-duration nil
   :zero-task-durations nil
   :duration-ipcf-is-integral t
   :duration-ipcf-max-duration 0)
  
  
  )


;; broken out as separate function for easier testing

(defun DURATION-IPCF-AND-INTERVALS 
       (efficiency-ipcf abs-ipcf min-dur qstart qend
                        &key 
                        (min-duration-limit 1)
                        (get-integral-duration-by :round)
                        (remove-intervals-with-zero-task-duration nil)
                        (zero-task-durations nil)
                        (duration-ipcf-is-integral t)
                        (duration-ipcf-max-duration 0)
                        (verbose nil)
                        )

  "The nuts and bolts of computing a task duration ipcf and interval list
    for task initialization.  
    Input --
     efficiency-ipcf - ipcf, time in qtime, value is efficiency at qtime.
       efficiency is in the range 0 to 1, where 1 means task duration will
       be equal to min-dur (see below).
     abs-ipcf - ipcf, time in qtime, value is suitability at qtime
       Note: this is not suitability of STARTING at qtime, but suitability
       of operating at time.  Here it only matters if suitability is zero or not.
     min-dur - minimum task duration, in fractional qtime, such that
       nominal task duration = min-dur/efficiency
     qstart - earliest bounding time for task
     qend - latest bounding time for task
    Returns two values --
     duration-interval-list - list of ((t1 t2 dur)...) s.t. task can start
       between t1 and t2 inclusive, and if so has duration dur.  t1, t2, and
       dur are all integral.  dur may be 0.  The returned interval list is
       appropriate as an :interval-list argument to make-fdsia for 
       interval-type :start-start.
     duration-ipcf - ipcf, time in qtime, value is duration of task if started
       at time.  May be integral or quantized.
    Keyword options --
     :min-duration-limit - quantized duration for duration-interval-list are
       not permitted to fall below this value. Default is 1, can be 0.
       Must be integer.
     :get-integral-duration-by - keyword describing how to convert min-dur/effic
       to an integral value.  Default is :round, other legal values are:
       :ceiling, :floor.
     :remove-intervals-with-zero-task-duration - if t, any intervals in
       duration-interval-list with task duration 0 are removed, otherwise they
       are left in the list.
     :zero-task-durations - if t, task durations in the duration-interval-list
       are set to zero (after removing intervals that already have zero duration;
       in fact, it doesn't make sense to do this unless 
      :remove-intervals-with-zero-task-duration is t, so a warning is put out in
      case these are inconsistent
     :duration-ipcf-is-integral - If t, then the duration-ipcf corresponds
       exactly to the duration-interval-list and has integral durations
       using whatever method was specified for doing this.  If nil, then
       the duration-ipcf is fractional qtime as given by min-dur/efficiency.
     :duration-ipcf-max-duration - only used if :duration-ipcf-is-integral is nil.
       this will be the duration value in the pcf when the efficiency is 
       zero and min-dur/efficiency would otherwise blow up.  Defaults to
       zero.
     :verbose - if t, print out the entire contents of the Library of Congress
       to standard output"

  (if (and (not :remove-intervals-with-zero-task-duration)
           :zero-task-duration)
    (warn "zeroing task duration, but not removing intervals with zero duration first"))

  (let* ((integral-duration-ipcf nil)
         (duration-ipcf nil)
         (duration-interval-list nil)
         ;; used to indicate durations which would round to zero and would
         ;; therefore not be distinguished from the mask: the exact value
         ;; doesn't matter, so long as it rounds to zero
         (epsilon 0.001))
    
    ;; construct duration ipcf:  time will be quanta, value will be duration
    ;; in integral quanta.  If efficiency is zero then duration is set to
    ;; *plus-infinity*
    (setf 
     integral-duration-ipcf
     (ipcf-transform  
      efficiency-ipcf
      #'(lambda (effic) 
          (cond ((= 0 effic)
                 ;; if efficiency or abs suit is zero, duration is infinite
                 *plus-infinity*)
                (t ;; otherwise compute duration as fractional qtime
                 (max (max min-duration-limit epsilon)
                      (case get-integral-duration-by
                        (:round   (round   (/ min-dur effic)))
                        (:ceiling (ceiling (/ min-dur effic)))
                        (:floor   (floor   (/ min-dur effic)))
                        (:otherwise 
                         (error "illegal value for get-integral-duration-by: ~a"
                                get-integral-duration-by)))))))))
    
    (if verbose
      (format t "~%initial integral-duration-ipcf (as interval list) ~a"
              (ipcf-to-interval-list integral-duration-ipcf qstart qend)))

    ;; remove impossible start times (at ends of non-zero intervals
    ;; of either efficiency or abs-ipcf)
    (let ((nzi (ipcf-non-zero-intervals 
                (ipcf-multiply abs-ipcf efficiency-ipcf) qstart qend))
          (mask-intervals nil)
          (mask-ipcf nil))
      ;; construct an ipcf to mask out times where the task would overlap
      ;; times with zero abs-ipcf values or with zero effic values
      (dolist (interval nzi)
        (cond ((= (second interval) *plus-infinity*)
               ;; intervals ending at infinity don't need to be shrunk
               (push interval mask-intervals))
              (t (push (list (first interval)
                             (- (second interval)
                                (round (ipcf-get-value 
                                        (second interval)
                                        integral-duration-ipcf))))
                       mask-intervals))))
      (if verbose
        (format t "~%mask intervals: ~a" (reverse mask-intervals)))
      (setf mask-ipcf (ipcf-from-interval-list (reverse mask-intervals)))
      ;; now mask the times
      (setf integral-duration-ipcf
            (ipcf-multiply mask-ipcf integral-duration-ipcf)))
    (if verbose
      (format t "~%masked integral-duration-ipcf (as interval list) ~a"
              (ipcf-to-interval-list integral-duration-ipcf qstart qend)))
    
    ;; convert duration ipcf to interval list
    (setf duration-interval-list 
          (ipcf-to-interval-list integral-duration-ipcf qstart qend))
    ;; remove any intervals with third value = 0: they have been masked out
    (setf duration-interval-list
          (remove-if #'(lambda (x) (= (third x) 0)) duration-interval-list))
    ;; set any epsilon durations to zero
    (dolist (int duration-interval-list)
      (if (= (third int) epsilon) (setf (third int) 0)))

    ;; remove zero task duration intervals if desired
    (when remove-intervals-with-zero-task-duration
      (setf duration-interval-list
            (remove-if #'(lambda (x) (= (third x) 0)) duration-interval-list))
      (if verbose
        (format t "~%with 0-dur intervals removed ~a"
                duration-interval-list))
      )

    ;; zero task durations if desired
    (when zero-task-durations
      (dolist (int duration-interval-list)
        (setf (third int) 0))
      (if verbose
        (format t "~%after zeroing durations ~a"
                duration-interval-list))
      )

    ;; for the default method, the duration-ipcf is the integral-duration-ipcf
    (if duration-ipcf-is-integral
      (setf duration-ipcf integral-duration-ipcf)
      (setf duration-ipcf
            (ipcf-transform
             efficiency-ipcf
             #'(lambda (x) (cond ((= x 0) duration-ipcf-max-duration)
                                 (t (/ min-dur x)))))))
 
    (if verbose
      (format t "~%duration-ipcf (as interval list) ~a"
              (ipcf-to-interval-list duration-ipcf qstart qend)))
   
    (values duration-interval-list
            duration-ipcf)))



#|======================================================================
  === old method with problems =========================================
(defmethod COMPUTE-DURATION-IPCF ((task obs-task)
                                  (chr obs-chronika)
                                  efficiency-ipcf
                                  abs-ipcf
                                  fractional-exp-qtime)

  "Supplemental method for OBS-TASK initialization.  Input is
     task - task instance
     chr - chronika instance
     efficiency-ipcf - ipcf, time in qtime, value is efficiency
     abs-ipcf - ipcf, time in qtime, value is suitability (here it only
      matters if suitability is zero or not)
     fractional-exp-qtime - exposure time, in fractional qtime
    Returns two values:
     duration-interval-list (quantized)
     duration-ipcf (possibly fractional)"
   
  (let ((duration-ipcf nil)
        (duration-interval-list nil))
    
    ;; construct duration ipcf:  time will be quanta, value will be duration in
    ;; quanta.  If efficiency is zero, or if abs suitability is zero,
    ;; then duration is set to be 0.
    (setf duration-ipcf
          (ipcf-combine 
           efficiency-ipcf
           abs-ipcf
           #'(lambda (effic abs) 
               (cond 
                ;; if efficiency or abs suit is zero, duration is 0
                ((or (= 0 effic) (= 0 abs)) 0)
                (t ;; otherwise compute duration as fractional qtime
                 (max 1 (round (/ fractional-exp-qtime effic))))))))
    
    ;; convert duration ipcf to interval list
    (setf duration-interval-list 
          (ipcf-to-interval-list 
           duration-ipcf (qtime-start chr) (qtime-end chr)))
    ;; filter intervals if needed
    
    (values duration-interval-list
            duration-ipcf)))
  ======================================================================|#


;;; ----------------------------------------------------------------------
;;; override default TASK methods for OBS-TASK

(defmethod MINIMUM-DURATION ((i obs-task))
  (or (slot-value i 'minimum-duration)
      (setf (slot-value i 'minimum-duration) (fdsia-min-duration (fdsia i)))))

(defmethod MAXIMUM-DURATION ((i obs-task))
  (or (slot-value i 'maximum-duration)
      (setf (slot-value i 'maximum-duration) (fdsia-max-duration (fdsia i)))))

;;; NOTE: these methods all return numeric values even it time is not
;;; a feasible start or end time

(defmethod DURATION-GIVEN-START-AT ((i obs-task) time)
  (or (fdsia-duration-given-start-at (fdsia i) time)
      0))

(defmethod DURATION-GIVEN-END-AT ((i obs-task) time)
  (or (fdsia-duration-given-end-at (fdsia i) time)
      0))

(defmethod END-GIVEN-START-AT ((i obs-task) time)
  "Return the end time of a task given that it starts at time"
  (or (fdsia-end-given-start-at (fdsia i) time)
      time))

(defmethod START-GIVEN-END-AT ((i obs-task) time)
  "Return the start time of a task given that it ends at time"
  (or (fdsia-start-given-end-at (fdsia i) time)
      time))

(defmethod DEFAULT-PCF ((i obs-task))
  "variant of usual method: use fdsia to get early/late start"
  (with-slots (fdsia) i
    (cond (FDSIA
           (iPCF-create-with-single-interval
            (fdsia-earliest-start fdsia)
            (fdsia-latest-start fdsia)
            1 
            0))
          (t *zero-PCF*))))

;;; ----------------------------------------------------------------------
;;; New methods for OBS-TASKS


(defmethod EFFICIENCY-AT-TIME ((i obs-task) time)
  
  "Input quantized time, return efficiency"

  (ipcf-get-value time (slot-value i 'efficiency-ipcf)))


(defmethod DURATION-AT-TIME ((i obs-task) time)

  "Input quantized time, value returned is duration in quantized time
    units (possibly fractional)"

  (ipcf-get-value time (slot-value i 'duration-ipcf)))


(defmethod EXPOSURE-DURATION-AT-TIME ((i obs-task) time)
  
  "Return exposure duration (seconds) given that task scheduled
    to start at time (quanta).  Include efficiency factor.  If time is not
    a legal start time, return 0."
  
  (let ((effic (ipcf-get-value time (slot-value i 'efficiency-ipcf)))
        (duration (ipcf-get-value time (slot-value i 'duration-ipcf)))
        (exp-duration 0))
    (when (and duration effic)
      (setf exp-duration
            ;; convert duration to days, multiply by efficiency, then
            ;; convert to seconds
            (round (* effic 
                      (duration-in-qtime-to-days (chronika i) duration)
                      86400))))
    exp-duration))


(defmethod MINIMUM-NONZERO-DURATION ((task obs-task))
  "Return minimum non-zero duration of task"
  (minimum-duration task))


(defmethod START-TIME-COUNT ((task obs-task))
  "return number of possible discrete start time possibilities"
  (fdsia-val-count (fdsia task)))


(defmethod TASK-TO-TASK-OVERHEAD ((task1 obs-task) (task2 obs-task))
  "return time in qtime between END of task1 and START of task2"
  ;; default method: no overhead
  0)


(defmethod TASK-CAN-START-AND-FIT-IN-INTERVAL ((task obs-task) start end)
  "return t if task can start at specified start time, and
    can end before the specified end time, i.e. it will fit"
  (let ((end-time (fdsia-end-given-start-at (fdsia task) start)))
    ;; end-time is nil if start is not legal
    (and end-time (<= end-time end))))

(defmethod TASK-CAN-OVERLAP-INTERVAL ((task obs-task) start end)
  "return list of value ranges (NOT time) for which task overlaps start
    and end, nil if no times allow overlap"
  (fdsia-overlap-exclusion (fdsia task) start end))    


;;; ======================================================================
;;;                                                 CLASS:  FIXED-OBS-TASK
;;;
;;; a subtype of OBS-TASK representing an observation of a fixed target:
;;; additional slots for position

(defclass FIXED-OBS-TASK (OBS-TASK)
  (   
   (target-ra
    :initform nil :reader target-ra :documentation
    "RA of target, decimal degrees")
   (target-dec
    :initform nil :reader target-dec :documentation
    "Dec of target, decimal degrees")
   (pos-vector 
    :initform nil :reader pos-vector :documentation
    "Position vector for target")
   )
  (:documentation "Type of OBS-TASK viewing a fixed astronomical object"))


(defmethod PRINT-OBJECT ((obj FIXED-OBS-TASK) stream)
  (format stream "#<FIXED-OBS-TASK ~a Prio ~d exp ~ds>" 
          (slot-value obj 'target-name)
          (slot-value obj 'priority)
          (slot-value obj 'exp-time)))

;;; ----------------------------------------------------------------------
;;; FIXED-OBS-TASK initialization

(defmethod INIT-TASK :AFTER ((task fixed-obs-task) args)
  
  "Initialization method for fixed-obs-tasks.  Required keywords in args:
    :ra - RA in degrees
    :dec - DEC in degrees"
  
  (let ((ra (getf args :ra))
        (dec (getf args :dec)))
    ;; error check
    (when (or (not ra) (not dec))
      (error "RA and DEC are required arguments"))
    
    (setf (slot-value task 'target-ra) ra)
    (setf (slot-value task 'target-dec) dec)
    (setf (slot-value task 'pos-vector)
          (3vect-spherical-to-cartesian
           (list (radians ra) (radians dec))))
    task))

;;; ----------------------------------------------------------------------
;;; New methods for FIXED-OBS-TASK

(defmethod TASK-ANGULAR-SEPARATION ((task1 fixed-obs-task) 
                                    (task2 fixed-obs-task))
  
  "return angular separation of two tasks, in DEGREES"
  
  (let* ((pos1 (pos-vector task1))
         (pos2 (pos-vector task2))
         (sep (max 0 (3vect-separation pos1 pos2 :units :degrees))))
    sep))


;;; ======================================================================
;;;                                                        CLASS:  OBS-CSP
;;;
;;; type of CSP corresponding to OBS-CHRONIKA and tasks
;;;

(defclass OBS-CSP (CAP-CSP)
  ((obs-csp-chronika 
    :initform nil :reader obs-csp-chronika
    :documentation "obs-csp-chronika instance to which this obs-csp belongs"))
  (:documentation "type of CSP corresponding to OBS-CHRONIKA"))


(defmethod PRINT-OBJECT ((obj OBS-CSP) stream)
  (format stream "#<OBS-CSP ~a>" (slot-value obj 'name)))


(defmethod INIT-CSP :AFTER ((csp OBS-CSP) args)
  
  "initialization method: required arg in plist is :obs-csp-chronika"
  
  (setf (slot-value csp 'obs-csp-chronika)
        (getf args :obs-csp-chronika)))


;;; ======================================================================
;;;                                                        CLASS:  OBS-VAR
;;;
;;; special type of CSP VAR corresponding to an OBS-TASK or subtype

(defclass OBS-VAR (VAR)
  (
   (task
    :initform nil :initarg :task :accessor task
    :documentation 
    "task instance corresponding to var")
   )
  (:documentation "var corresponding to OBS-TASK"))


(defmethod PRINT-OBJECT ((obj OBS-VAR) stream)
  (let ((name (slot-value obj 'name))
        (var-number (slot-value obj 'var-number)))
    (if name
      (format stream "#<OBS-VAR ~a #~d>" name var-number)
      (format stream "#<OBS-VAR #~d>" var-number))))

;;; ----------------------------------------------------------------------
;;; Initializtion for OBS-VAR
;;;
;;; Note:  MAKE-VAR takes the following args:
;;;  CSP 
;;;  number 
;;;  name 
;;;  (var-type 'VAR)
;;;  value-count
;;;  solution-value
;;;  (priority *default-var-priority*)
;;; These args, and any others, are in the args plist

(defmethod INIT-VAR :AFTER ((v OBS-VAR) args)
  
  "special initialization for obs-var:  args plist must contain
    :task keyword and instance"
  
  (let ((task (getf args :task)))
    (cond (task
           (setf (task v) task))
          (t 
           (error "no corresponding task for var ~v" v)))))

;;; -----------------------------------------------------------------------
;;; New methods for OBS-VAR

(defmethod ASSIGNED-TIME ((v obs-var))
  "return two values:  start and end times if assigned (quanta), else nil"
  (let ((assignment (assigned-value v))
        (task (task v)))
    (cond ((and assignment task)
           (FDSIA-val-to-time (FDSIA task) assignment))
          (t (values nil nil)))))

;;; for strategies:

(defmethod FIRST-TIME-WITH-NO-CONFLICTS ((v obs-var))
  "return first time value with min-conflicts value"
  (let ((value (first-value-lexical-order-with-no-conflicts v))
        (task (task v)))
    (FDSIA-val-to-time (FDSIA task) value)))

(defmethod PREFERENCE-ON-FIRST-TIME-WITH-NO-CONFLICTS ((v obs-var))
  "return preference value of first time with no conflicts"
  (value-preference v (first-value-lexical-order-with-no-conflicts v)))


(defmethod FIRST-TIME-WITH-MIN-CONFLICTS ((v obs-var))
  "return first time with min-conflicts value"
  (let ((value (first-value-lexical-order-with-min-conflicts v))
        (task (task v)))
    (FDSIA-val-to-time (FDSIA task) value)))

(defmethod PREFERENCE-ON-FIRST-TIME-WITH-MIN-CONFLICTS ((v obs-var))
  "return preference value of first time with number of conflicts
    equal to the current min conflicts"
  (value-preference v (first-value-lexical-order-with-min-conflicts v)))

;;; note: could define corresponding methods for LAST, but no need at
;;; present


;;; ======================================================================
;;;                   METHODS FOR OBS-CSP, OBS-VAR, OBS-CHRONIKA, OBS-TASK
;;; ======================================================================
;;;
;;; these tie together the behavior of these classes

;;; ----------------------------------------------------------------------
;;; creation of CSP, VARs from CHRONIKA, TASKs

(defmethod MAKE-CSP-FOR-CHRONIKA ((chronika OBS-CHRONIKA) 
                                  csp-type 
                                  csp-initargs 
				  var-type 
                                  var-initargs)

  "Create CSP corresponding to input chronika, including vars corresponding
    to tasks.  Input args are:
     chronika - the chronika instance
     csp-type - csp type to make
     csp-initargs - plist of args for call to make-csp
     var-type - var type
     var-initargs - plist of args for call to make-var
    Returned value is CSP instance."

  (let ((csp (apply
	      'make-csp
	      (append `(
			:name ,(name chronika)
			:vars-named-p t
			:var-init-hook nil
			:csp-type ,csp-type
			:cache-assigned-conflicts-p t)
		      csp-initargs))))
    (make-vars-for-chronika csp chronika var-type var-initargs)
    csp))


(defmethod MAKE-VARS-FOR-CHRONIKA ((csp OBS-CSP) 
                                   (chronika OBS-CHRONIKA) 
                                   var-type var-initargs)
  
  "Create variables corresponding to tasks in the chronika.  Args are the 
    CSP, the chronika, the var-type, and plist of var-initargs."
  
  ;; loop over tasks in chronika (reverse order makes vars and tasks
  ;; occur in same order)
  (let ((var-number 0))
    (dolist (task (reverse (task-list chronika)))
      (make-var-for-task csp task var-number var-type var-initargs)
      (incf var-number))))


(defmethod MAKE-VAR-FOR-TASK ((csp obs-csp) (task obs-task) 
                              var-number var-type var-initargs)
  
  "Make var instance corresponding to task. Input csp, task, var-number,
    var-type, and var-initargs.  Var is added to CSP.  Returned valus 
    is var instance."
  
  ;; first remove any old vars association, if any
  (setf (slot-value task 'task-var) nil)
  
  ;; create new var if appropriate
  ;; test for inclusion here
  (when (FDSIA task)
    
    (let ((var (apply
                'make-var 
                (append `(
                          :csp ,csp
                          :number ,var-number
                          :name ,(id task)
                          :task ,task
                          :value-count ,(start-time-count task)
                          :var-type ,var-type
                          :priority ,(priority task))
                        var-initargs))))
      
      ;; record that task corresponds to var
      (setf (slot-value task 'task-var) var)
      
      ;; remove domain values that have zero suitability
      ;; (either absolute, or because of restrictions, executed tasks, etc.)
      (let ((exclude (times-excluded-by-suitability csp task)))
        (remove-domain-values-in-range-list csp var exclude :permanent t))
      
      ;; remove domain values due to edge effects
      ;; note:  only do this if task must be scheduled inside time boundaries
      (when t				;test stubbed
        (let ((exclude (times-excluded-by-boundary csp task)))
          ;;(format t "~%var: ~a exclude: ~a" var exclude)
          (remove-domain-values-in-range-list csp var exclude :permanent t)))
      
      ;; establish preferences for remaining times
      (let ((val 0))
        (dolist (pref (value-preference-list task))
          (set-value-preference var val pref)
          (incf val)))
      
      ;; returned value is the var instance
      var)))


;;; ----------------------------------------------------------------------
;;; methods used in MAKE-VAR-FOR-TASK


(defmethod REMOVE-DOMAIN-VALUES-IN-RANGE-LIST
  ((csp OBS-CSP) (var OBS-VAR) range-list &key (permanent nil))
  
  "range-list is a list of TIME ranges ((t1 t2)(t3 t4)...)
    remove from domain all VALUES for var corresponding to task
    which are included in any of the ranges.  If the keyword
    argument :permanent is t, then the values are removed permanently
    (i.e. they will remain removed after a reset-csp)."
  
  (let* ((task (task var))
         (FDSIA (FDSIA task))
         (value-range-list (FDSIA-time-range-list-to-val-range-list
                            FDSIA range-list)))
    (dolist (value-range value-range-list)
      (loop-over-value-range
       value (first value-range) (second value-range)
       (remove-value-from-domain var value
                                 :permanent permanent)))))


(defmethod TIMES-EXCLUDED-BY-SUITABILITY ((csp OBS-CSP) (i OBS-TASK))
  
  "return list of TIME ranges excluded because suitability
    is zero.  Based on task suitability only, i.e. ignores boundary effects."
  
  (let ((chronika (chronika i)))
    ;; error check: chronika range must be 0 to (qtime-quanta chronika)
    (unless (and (= (start chronika) 0)
                 (= (end   chronika) (1- (qtime-quanta chronika))))
      (error "time range of chronika not 0 to ~a" (qtime-quanta chronika)))
    
    (invert-range-list (non-zero-intervals i) (qtime-quanta chronika))))


(defmethod TIMES-EXCLUDED-BY-BOUNDARY ((csp OBS-CSP) (i OBS-TASK))
  
  "return list of TIME ranges excluded because suitability
    is zero.  Based on task restricting each affecting task to lie 
    within time range."
  
  (let ((result nil)
        (chronika-start (start (chronika i)))
        (chronika-end   (end   (chronika i)))
        (quanta   (qtime-quanta (chronika i))))
    
    ;; check each constraint, only if schedulable:  if unschedulable,
    ;; this will not yield any information and the excluded range will 
    ;; be all times
    (when (schedulable-p i)
      (dolist (constraint (constraints i))
        ;;(format t "~%checking ~a" constraint)
        (multiple-value-bind 
          (limiting-ipcf unsched-flag unsched-components)
          (limit-based-on-boundaries constraint i chronika-start chronika-end)
          (declare (ignore unsched-components unsched-flag))
          ;;(format t "  nzi ~a" (ipcf-zero-intervals limiting-ipcf))
          (setf result
                (append result
                        (invert-range-list
                         (ipcf-non-zero-intervals limiting-ipcf chronika-start chronika-end)
                         quanta))))))
    ;;(format t "~%result: ~a" result)
    result))

;;; ----------------------------------------------------------------------
;;; OBS-VAR assignment/unassignment methods
;;;
;;; for both of these next two methods, the impact of assigning an OBS-VAR to a
;;; value is given by VAR-EFFECT method
;;;

(defmethod CSP-VAR-VALUE-ASSIGN ((csp OBS-CSP) (v OBS-var) value)
  
  ;; caching used here in the mode of recording conflicts caused by 
  ;; current assignments only
  ;; weight is 1 here
  
  (let ((effect (VAR-EFFECT csp v value)))
    (dolist (j-effect effect)
      ;; effect on single other var
      (let ((j-var (first j-effect))	;var
            (cause (second j-effect))
            (excluded-value-ranges (cddr j-effect)))
        
        (dolist (value-range excluded-value-ranges)
          
          (if (cache-assigned-conflicts-p csp)
            
            ;; caching on:  add to conflicts and cache
            (add-to-conflicts-and-cache 
             csp v value cause j-var 
             :first-value (first value-range)
             :last-value (second value-range))
            
            ;; no caching: loop over each segment and add conflicts
            (do ((val (first value-range) (1+ val)))
                ((> val (second value-range)) nil)
              (add-to-conflicts-on-value j-var val))
            
            ))))))


(defmethod CSP-VAR-VALUE-UNASSIGN ((csp OBS-CSP) (v OBS-var) value)
  
  ;; caching supported here for this constraint
  
  (if (cache-assigned-conflicts-p csp)
    
    ;; caching on: use cache and remove
    (subtract-cached-conflicts csp v value :remove-from-cache t)
    
    ;; caching not on: compute effect
    (let ((effect (var-effect csp v value)))
      (dolist (j-effect effect)
        ;; effect on single task
        (let* ((j-var (first j-effect))	;var
               (excluded-value-ranges (cddr j-effect)))
          (dolist (value-range excluded-value-ranges)
            (do ((val (first value-range) (1+ val)))
                ((> val (second value-range)) nil)
              (subtract-from-conflicts-on-value 
               j-var val))))))))


(defmethod VAR-EFFECT ((csp OBS-CSP) (v OBS-var) value)
  
  "If var v is assigned specified value return list of affected other
    vars and the values excluded, in the form:
    ((<var1> <cause1> (val11 val12)(val13 val14)...)
     (<var2> <cause2> (val21 val22)(val23 val24)...))
    This form is useful for populating the cache.
    The meaning is: v conflicts with var1 assigned to val11 through val12,
     val13 through val14, etc. and with var2 assigned to val21 through val22...
    Cause is whatever is returned by the constraint source method.
    Note that this INCLUDES the task overlap effect (cause :OVERLAP).
    Only vars corresponding to tasks in the same chronika as (task v)
    are checked."
  
  (let* ((task (task v))
         (task-FDSIA (FDSIA task))
         (task-start (FDSIA-val-to-time task-FDSIA value))
         (chronika-start (start (chronika task)))
         (chronika-end (end (chronika task)))
         (quanta (qtime-quanta (chronika task)))
         ;; start with overlap effect, then loop over other constraints below
         (result (var-overlap-effect csp v value)))
    
    ;; check each constraint
    (dolist (constraint (constraints task))
      
      ;; check each affected task 
      (dolist (affected-task (tasks-affected-via-constraint constraint task))
        
        ;; task constrains affected-task: find allowed times for affected-task
        (let* ((time-range-list (invert-range-list
                                 (ipcf-non-zero-intervals
                                  (effect-of-task-on-task-given-interval
                                   constraint task affected-task 
                                   task-start task-start)
                                  chronika-start chronika-end)
                                 quanta))
               (value-range-list (FDSIA-time-range-list-to-val-range-list
                                  (FDSIA affected-task) time-range-list)))
          (if value-range-list
            (push (cons (task-var affected-task)
                        (cons (source constraint) value-range-list))
                  result)))))           ; end loop over constraints
    
    result))


;;; TODO:  allow overheads below to depend on scheduled start/end times
;;; explicitly

(defmethod VAR-OVERLAP-EFFECT ((csp OBS-CSP) (v OBS-var) value)
  
  "If var v is assigned to value, return list of affected other vars
    and conflicting values in the form:
    ((<var1> <cause1> (val1 val2)(val3 val4)...)(<var2><cause2>...))
    This form is useful for populating the cache.
    For overlap, the cause is the keyword :OVERLAP.
    The meaning is: v conflicts with var1 assigned to val1 through val2,
     val3 through val4, etc."
  
  ;; get the task and start/end times from the value
  (let* ((task (task v))
         (FDSIA (FDSIA task))
         (result nil))
    (multiple-value-bind
      (task-start task-end)
      (FDSIA-val-to-time FDSIA value)
      
      (if (not task-start)
        (error "value ~a out of range for var ~a, task ~a" 
               value v task))
      
      ;; check all other vars in the CSP
      (do-for-all-ignored-and-unignored-vars
       :var affected-var :csp csp
       :predicate (and (not (equal affected-var v))
                       (may-not-overlap-p v affected-var))
       :form (let* ((affected-task (task affected-var))
                    (affected-task-FDSIA (FDSIA affected-task))
                    (overhead (task-to-task-overhead task affected-task))
                    (exclusion (FDSIA-overlap-exclusion
                                affected-task-FDSIA
                                (- task-start overhead)
                                (+ task-end overhead))))
               (when exclusion
                 (push (cons affected-var
                             (cons :OVERLAP exclusion))
                       result)))))
    result))


(defmethod MAY-NOT-OVERLAP-P ((var1 OBS-VAR) (var2 OBS-VAR))

  "Return t if var1 may not overlap var2, nil otherwise.  E.g. if var1 and
    var2 correspond to tasks that can occur in parallel, this method returns
    nil."

  ;; this method should not be called with the same var for var1 and var2
  ;; default method is no two tasks can overlap
  t)

;;; ----------------------------------------------------------------------
;;; gap fillers


(defmethod TASKS-FITTING-IN-INTERVAL ((chr obs-chronika) start end
                                      &key (exact nil))
  
  "Return list of tasks that can start at specified start and end at or
    before the specified end.  Both are specified as qtimes.
    If :exact is t, then only tasks that end exactly at specified end
    are returned"

  (let ((result nil))
    (dolist (task (task-list chr))
      (when (TASK-CAN-START-AND-FIT-IN-INTERVAL task start end)
        ;; it's a candidate: check exact fit
        (cond (exact
               (if (= (end-given-start-at task start) end)
                 (push task result)))
              (t (push task result)))))
    result))

;;; -----------------------------------------------------------------------
;;; methods for schedule reporting


(defmethod TOTAL-SCHEDULED-DURATION ((csp obs-CSP))
  "return time (days) scheduled"
  ;; (total-scheduled-duration (obs-csp -occ))
  (let ((total 0))
    (do-for-all-unignored-vars 
     :var var :csp csp :form
     (if (assigned-value var)
       (let* ((task (task var))
              (chronika (chronika task)))
         (incf total (duration-in-qtime-to-days
                      chronika
                      (duration-given-start-at task (assigned-time var)))))))
    total))
  

(defmethod TOTAL-MIN-DURATION ((csp obs-CSP))
  "return minimum duration total time (days) scheduled"
  ;; (total-min-duration (obs-csp -occ))
  (let ((total 0))
    (do-for-all-unignored-vars 
     :var var :csp csp :form
     (if (has-assigned-value-p var)
       (let* ((task (task var))
              (chronika (chronika task)))
         (incf total (duration-in-qtime-to-days
                      chronika
                      (minimum-nonzero-duration task))))))
    total))


(defmethod GAP-LIST ((csp obs-csp))
  "return list of gaps ((gap1-start gap1-end)...).  Times are in
    qtime."
  ;; (gap-list *euve-schedule*)
  (let* ((assigned-vars (all-assigned-vars csp))
         (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
         (chronika (chronika (task (first (all-vars csp)))))
         (overall-start (start chronika))
         (overall-end   (end chronika))
         (prev-end overall-start)
         (gaps nil)
         )
    
    (dolist (var sorted-vars)
      (when (< prev-end (assigned-time var))
        ;; gap
        (push (list prev-end (assigned-time var)) gaps)
        )
      (setf prev-end (max prev-end (end-given-start-at (task var) 
                                                       (assigned-time var)))))
    ;; gap at end?
    (when (/= prev-end overall-end)
      (push (list prev-end overall-end) gaps)
      )
    (nreverse gaps)))


(defmethod TOTAL-GAPS ((csp obs-CSP))
  "return total time (days) in gaps"
  ;; (total-gaps (obs-csp -occ))
  (let* ((assigned-vars (all-assigned-vars csp))
         (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
         (chronika (chronika (task (first (all-vars csp)))))
         (overall-start (start chronika))
         (overall-end   (end chronika))
         (prev-end overall-start)
         (total-gap 0)
         )
    
    (dolist (var sorted-vars)
      (when (< prev-end (assigned-time var))
        ;; gap
        (incf total-gap (- (assigned-time var) prev-end)))
      (setf prev-end (max prev-end (end-given-start-at (task var) 
                                                       (assigned-time var)))))
    ;; gap at end?
    (when (/= prev-end overall-end)
      (incf total-gap (- overall-end prev-end)))
    (duration-in-qtime-to-days chronika total-gap)))


(defmethod CSP-STATE-SUMMARY ((c OBS-CSP) &rest which)
  
  "Return list of state indicators.  &rest arg is list of indicators
    desired.  Returned value is lisp object of unspecified type.  
    Access to indicator values is via GET-CSP-STATE-SUMMARY-VALUE
    Legal choices for WHICH are: 
    :total-min-duration
    :total-sched-duration
    :total-gaps
    Others TBD.
    :all - all of the above
    Default is :ALL"

  ;; (csp-state-summary (obs-csp -occ))
  
  ;; build returned summary
  (let ((result (call-next-method)))
    (if (null which) (setf which '(:all)))
    
    (when (or (member :all which) (member :total-min-duration which))
      (push (total-min-duration c) result) 
      (push :total-min-duration result))
    (when (or (member :all which) (member :total-sched-duration which))
      (push (total-scheduled-duration c) result) 
      (push :total-sched-duration result))
    (when (or (member :all which) (member :total-gaps which))
      (push (total-gaps c) result) 
      (push :total-gaps result))
    
    result))


(defmethod SCHEDULE-REPORT ((csp obs-csp) &key (stream t) &allow-other-keys)

  "Report schedule status to stream"
  ;; (schedule-report (obs-csp -occ))

  ;; report all assignments and gaps
  (format stream "~%Scheduled activities:")
  (format stream
	  "~%Name            start date       start end  dur   dur    min   -exp. time---  conf suit  suit/")

  (format stream
	  "~%                                                (days)   dur    req  act/req    #        max")

  (let* ((assigned-vars (all-assigned-vars csp))
         (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
         (chronika (chronika (task (first (all-vars csp)))))
         (overall-start (start chronika))
         (overall-end   (end chronika))
         (prev-end overall-start)
         (sched-days (duration-in-qtime-to-days 
                      chronika (- overall-end overall-start)))
         (total-gap 0)
         (total-sched 0)
         (total-min 0)
         (total-exp 0)
         )
    
    ;; report unassigned vars
    (dolist (var sorted-vars)
      (when (< prev-end (assigned-time var))
	;; gap
	(format stream "~%---gap(~4d)---                  ~4d ~4d" 
		(- (assigned-time var) prev-end) prev-end (assigned-time var))
	(incf total-gap (- (assigned-time var) prev-end)))
      (incf total-sched (duration-given-start-at 
                         (task var) (assigned-time var)))
      (incf total-min (minimum-nonzero-duration (task var)))
      (incf total-exp (exposure-duration-at-time 
                       (task var) (assigned-time var)))
      (format stream "~%~a" (task-report var))
      ;; flag if actual>minimum duration
      (if (> (duration-given-start-at (task var) (assigned-time var))
	     (minimum-nonzero-duration (task var)))
	  (format stream "*"))
      (setf prev-end 
            (max prev-end 
                 (end-given-start-at (task var) (assigned-time var))))
      )                                 ; end dolist over sorted-vars

    ;; gap at end?
    (when (/= prev-end overall-end)
      (incf total-gap (- overall-end prev-end))
      (format stream "~%---gap(~4d)---                  ~4d ~4d" 
              (- overall-end prev-end) prev-end overall-end))

    ;; summary info
    (format stream "~%Schedule time span  : ~6,2f days" sched-days)
    (format stream "~%Total scheduled time: ~6,2f days (~6,2f%)" 
            (duration-in-qtime-to-days chronika total-sched)
            (* 100 (/ (duration-in-qtime-to-days chronika total-sched) 
                      sched-days)))
    (format stream "~%Total gaps          : ~6,2f days (~6,2f%)" 
	    (duration-in-qtime-to-days chronika total-gap)
	    (* 100 (/ (duration-in-qtime-to-days chronika total-gap) 
                      sched-days)))
    (when (> total-exp 0)
      (format stream "~%Total exp time      : ~6,2f days" (/ total-exp 86400.0))
      (format stream " (~d sec, ~6,2f% of schedule time span)"
              total-exp (* 100 (/ (/ total-exp 86400.0) sched-days))))
    (when (> total-min 0)
      (format stream "~%Total minimum duration: ~6,2f days"
              (duration-in-qtime-to-days chronika total-min))
      (format stream " (expansion factor actual/min: ~7,3f)"
              (/ total-sched total-min)))

    ;; unscheduled tasks
    (unless (all-vars-assigned-p csp)
      (setf total-min 0)
      (format stream "~%Unscheduled activities:")
      (dolist (var (all-unassigned-vars csp))
	(format stream "~%~a" (task-report var))
	(incf total-min (minimum-nonzero-duration (task var)))
	)
      (format stream "~%Total minimum time not scheduled:  ~6,2f days"
	      (duration-in-qtime-to-days chronika total-min)))

    ;; assignment info
    (format stream "~% ~d tasks, ~a assigned, ~a assigned with conflicts"
	    (var-count csp) (assigned-var-count csp) (conflicting-var-count csp))
    (format stream "~% mean preference  : ~8,2f  mean conflicts  : ~8,2f"
	    (mean-preference csp) (mean-conflict-count csp))
    (format stream "~% summed preference: ~8d  summed conflicts: ~8d"
	    (summed-preference csp) (summed-conflict-count csp))

    ))


(defmethod TASK-REPORT ((var OBS-VAR))
  "Return string describing scheduling status of var"
  (let* ((task (task var))
         (chronika (chronika task))
         (name (target-name task))
         (exp-time (exp-time task))
         (assigned-value (assigned-value var))
         (start-end (multiple-value-list (assigned-time var)))
         (start (first start-end))
         (end   (second start-end))
         (conf (conflicts-on-assigned-value var))
         (max-pref (max-preference var))
         (min-duration (minimum-nonzero-duration task))
         ;; only valid if start is known
         (pref (if assigned-value (value-preference var assigned-value)))
         (actual-duration (if start (duration-given-start-at task start)))
         (act-exp-time (if start (exposure-duration-at-time task start)))
         )
    (if start
      (format nil "~15a ~16a ~4d ~4d ~4d ~5,2fd ~5,2fd ~7ds ~5,2f ~4d ~5d ~5,2f"
              name 
              (task-formatted-abs-qtime task start)
              start 
              end 
              actual-duration 
              (duration-in-qtime-to-days chronika actual-duration) 
              (duration-in-qtime-to-days chronika min-duration) 
              exp-time 
              (if (/= exp-time 0) (/ act-exp-time exp-time) 0)
              conf 
              pref
              (if (/= max-pref 0) (/ pref max-pref) 0))
      ;; unassigned
      (format nil "~15a ~16a ~4a ~4a ~4a ~5a  ~5a  ~7ds ~5a ~4a ~5a ~5a"
              name "- not sched -" "" "" "" "" "" exp-time "" "" "" ""))))


(defmethod TASK-FORMATTED-ABS-QTIME ((task obs-task) qtime)

  "input task and qtime, output formatted abs time for reporting"

  (task-formatted-abs-time 
   task (qtime-to-tjd (chronika task) qtime)))


(defmethod TASK-FORMATTED-ABS-TIME ((task obs-task) TJD)
  
  "input task and time as TJD, output formatted abs time for reporting"
  
  (subseq (format-abs-time TJD) 0 15))

;;; ======================================================================
;;;                                                CLASS:  CONSTRAINT-LIST
;;;
;;; for managing list of relative and absolute constraints to be applied
;;; to tasks in OBS-CSP-CHRONIKA instances

(defclass CONSTRAINT-LIST ()
  ((constraints
    :initform nil :reader constraints
    :documentation
    "list of constraints")
   (constraint-files
    :initform nil :reader constraint-files
    :documentation
    "List of files or pathnames which contain constraints.
        Used for historical record of where constraints came from")
   )
  (:documentation "constraint file mixin")
  )


;;; ----------------------------------------------------------------------
;;; generics

(defgeneric ADD-TO-CONSTRAINT-LIST (obj constraint)
  (:documentation "add constraint to list. Syntax defined as list
     (name arg1 ...) elsewhere."))

(defgeneric CLEAR-CONSTRAINT-LIST (obj)
  (:documentation "clear all constraint files/lists"))

(defgeneric LOAD-CONSTRAINT-FILE (obj file)
  (:documentation "load a constraint file"))

(defgeneric LOAD-CONSTRAINT-FILES (obj file-list)
  (:documentation "load a list of constraint files"))

(defgeneric APPLY-CONSTRAINTS (obj chr constraint)
  (:documentation "apply constraints as applicable to constraint"))

(defgeneric VALIDATE-CONSTRAINTS (obj chr constraint &key stream)
  (:documentation "check that constraints are syntactically correct"))

(defgeneric TRANSLATE-NAME-TO-TASK (chr name)
  (:documentation "translate input name to task instance or list of instances,
     based on specified input chronika"))

(defgeneric TRANSLATE-DURATION-FOR-CONSTRAINT-LIST (chr duration)
  (:documentation "translation from input duration to units appropriate
     for the input chronika"))

(defgeneric TRANSLATE-TIME-FOR-CONSTRAINT-LIST (chr time)
  (:documentation "translation from input time (date) to units appropriate
     for the input chronika"))

;;; ----------------------------------------------------------------------
;;; methods for general objects

(defmethod ADD-TO-CONSTRAINT-LIST (obj constraint)
  (error "can't add constraint ~a to constraint-list ~a" constraint obj))

(defmethod CLEAR-CONSTRAINT-LIST ((obj t))
  nil)

(defmethod LOAD-CONSTRAINT-FILES ((obj t) file-list)
  (declare (ignore file-list))
  nil)

(defmethod LOAD-CONSTRAINT-FILE ((obj t) file)
  (declare (ignore file))
  nil)

(defmethod APPLY-CONSTRAINTS ((obj t) (chr t) (constraint t))
  nil)

(defmethod VALIDATE-CONSTRAINTS ((obj t) (chr t) (constraint t) &key stream)
  (declare (ignore stream))
  nil)

(defmethod TRANSLATE-NAME-TO-TASK ((chr t) name)
  (error "can't translate name:  chr ~a, name ~a" chr name))

(defmethod TRANSLATE-NAME-TO-TASK ((chr chronika) name)
  ;; default name translation: name is ID, use lookup-task method
  (lookup-task chr name))

(defmethod TRANSLATE-DURATION-FOR-CONSTRAINT-LIST ((chr t) duration)
  (error "can't translate duration:  chr ~a, duration ~a" chr duration))

(defmethod TRANSLATE-DURATION-FOR-CONSTRAINT-LIST ((chr chronika) duration)
  ;; default method
  (duration-in-days-to-qtime chr duration))

(defmethod TRANSLATE-TIME-FOR-CONSTRAINT-LIST ((chr t) time)
  (error "can't translate time:  chr ~a, time ~a" chr time))

(defmethod TRANSLATE-TIME-FOR-CONSTRAINT-LIST ((chr chronika) time)
  ;; default method
  (TJD-to-qtime chr time))


;;; ----------------------------------------------------------------------
;;; methods for constraint-list class
;;;
;;; task names are converted to tasks via (translate-name-to-task chr task)
;;; time durations are converted to time via 
;;;  (translate-duration-for-constraint-list chr offset)
;;;
;;; task1 before task2:
;;;  (order task1 task2)
;;;
;;; task1 before task2, minimum end-to-start sep is offset-time
;;;  (order-and-offset offset-time task1 task2)
;;;
;;; group tasks within time
;;;  (group-within group-time task1 task2 ...)
;;;
;;; minimum time separation between task1 and task2 is sep-time
;;;  (min-time-separation sep-time task1 task2)
;;;
;;; time window (early and late starts specified)
;;;  (window (1992 1 5 0 0 0) (1992 1 6 0 0 0) task1)
;;;
;;; phase constraint: start date/time, period (d), phase min/max, task1 ...
;;;  (phase (1992 1 1 0 0 0) 30 0.0 0.05 task1)


(defmethod ADD-TO-CONSTRAINT-LIST ((obj constraint-list) constraint)
  (setf (slot-value obj 'constraints)
        (cons constraint (constraints obj))))


(defmethod CLEAR-CONSTRAINT-LIST ((obj constraint-list))
  (with-slots
   (constraints constraint-files) obj
   (setf constraints nil constraint-files nil)))


(defmethod LOAD-CONSTRAINT-FILES ((obj constraint-list) file-list)
  (dolist (file file-list)
    (load-constraint-file obj file)))


(defmethod LOAD-CONSTRAINT-FILE ((obj constraint-list) file)
  (with-slots
   (constraints constraint-files) obj
   (push file constraint-files)
   (with-open-file (stream file
		    :direction :input
		    :if-does-not-exist :error)
     (loop
       (let ((form (read stream nil nil)))
	 (cond ((null form) (return nil))
	       (t (push form constraints)))))
     file)))


(defmethod APPLY-CONSTRAINTS ((obj constraint-list) (chr chronika) 
                              (constraint task-constraint))
  (dolist (form (reverse (constraints obj)))
    (multiple-value-bind (function args error-p)
	(transform-constraint chr constraint form :verbose nil :stream t)
      (when (and function (not error-p))
	(apply function args)))))


(defmethod VALIDATE-CONSTRAINTS 
  ((obj constraint-list) (chr chronika) (constraint task-constraint)
   &key (stream t))
  (dolist (form (reverse (constraints obj)))
    (transform-constraint chr constraint form :verbose t :stream stream)))


(defmethod TRANSFORM-CONSTRAINT ((chr chronika) (constraint task-constraint) form
				 &key (verbose nil) (stream t))
  "Input constraint form, returns multiple values describing how to 
    interpret.  chr is a chronika, where any tasks are looked up.
    constraint is a subtype of task-constraint.  form is the constraint form as 
    a list.  :verbose should be t for a description to be written to :stream.
    Values returned are:
      function - name of (generic) function to call, or nil if none
      arglist - arg list for function, s.t. (apply function arglist) is correct
      OK - t if form is interpretable, nil if detected error"

  (cond
   ;; must be a list
   ((not (listp form))
    (format stream "~%transform constraint:  form is not a list ~a" form)
    (values nil nil nil))
   ;; dispatch on name of form
   (t (case (first form)
      
	(order 
	 (transform-order-constraint
	  chr constraint form :verbose verbose :stream stream))
	(order-and-offset
	 (transform-order-and-offset-constraint
	  chr constraint form :verbose verbose :stream stream))     
	(group-within
	 (transform-group-within-constraint
	  chr constraint form :verbose verbose :stream stream))     
	(min-time-separation
	 (transform-min-time-separation-constraint
	  chr constraint form :verbose verbose :stream stream))     
	(window
	 (transform-window-constraint
	  chr constraint form :verbose verbose :stream stream))     
	(phase
	 (transform-phase-constraint
	  chr constraint form :verbose verbose :stream stream))     

	(otherwise
	 (format stream "~%Unrecognized constraint form: ~a" form)
	 (values nil nil nil))))))


(defmethod TRANSFORM-ORDER-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  (let* ((first-name  (second form))
	 (second-name (third form))
	 (first-task (translate-name-to-task chr first-name))
	 (second-task (translate-name-to-task chr second-name))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'binary-rel-constraint))
	   (setf applicable nil))
	  ((or (not first-task) (not second-task))
	   (format stream "~%**error: ~a ~%  tasks not found~%  ~a ~a" form first-task second-task)
	   (setf error t))
	  (t nil))
    ;; transform if applicable and no errors
    (when (and applicable (not error))
      (setf function 'precedence-constraint)
      (setf arglist (list constraint first-task second-task))
      (when verbose
	(format stream "~%~a ~a" function arglist)))
    ;; returned value
    (values function arglist error)))


(defmethod TRANSFORM-ORDER-AND-OFFSET-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  (let* ((first-name  (third form))
	 (second-name (fourth form))
	 (offset (second form))
	 (first-task (translate-name-to-task chr first-name))
	 (second-task (translate-name-to-task chr second-name))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'binary-rel-constraint))
	   (setf applicable nil))
	  ((or (not first-task) (not second-task))
	   (format stream "~%**error: ~a tasks not found~%  ~a ~a" form first-task second-task)
	   (setf error t))
	  ((not (numberp offset))
	   (format stream "~%**error: ~a offset not numeric~%  ~a" form offset)
	   (setf error t))
	  (t nil))
    ;; transform if no errors
    (when (and applicable (not error))
      (setf function 'precedence-constraint)
      (setf arglist (list constraint first-task second-task
			  :offset (translate-duration-for-constraint-list 
                                   chr offset)))
      (when verbose
	(format stream "~%~a ~a" function arglist)))
    ;; returned value
    (values function arglist error)))


(defmethod TRANSFORM-GROUP-WITHIN-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  (let* ((task-names (cddr form))
	 (tol (second form))
	 (task-list (flatten
                     (mapcar #'(lambda (x) 
                                 (translate-name-to-task chr x)) task-names)))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'binary-rel-constraint))
	   (setf applicable nil))
	  ((some 'null task-list)
	   (format stream "~%**error: ~a tasks not found~%  ~a ~a" 
                   form task-names task-list)
	   (setf error t))
	  ((not (numberp tol))
	   (format stream "~%**error: ~a tolerance not numeric ~a" form tol)
	   (setf error t))
	  (t nil))
    ;; transform if no errors
    (when (and applicable (not error))
      (setf function 'group-within)
      (setf arglist (list constraint 
                          task-list 
                          (translate-duration-for-constraint-list chr tol)))
      (when verbose
	(format stream "~%~a ~a" function arglist)))
    ;; returned value
    (values function arglist error)))


(defmethod TRANSFORM-MIN-TIME-SEPARATION-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  (let* ((first-name  (third form))
	 (second-name (fourth form))
	 (offset (second form))
	 (first-task (translate-name-to-task chr first-name))
	 (second-task (translate-name-to-task chr second-name))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'binary-rel-constraint))
	   (setf applicable nil))
	  ((or (not first-task) (not second-task))
	   (format stream "~%**error: ~a tasks not found~%  ~a ~a" 
                   form first-task second-task)
	   (setf error t))
	  ((not (numberp offset))
	   (format stream "~%**error: ~a offset not numeric~%  ~a" form offset)
	   (setf error t))
	  (t nil))
    ;; transform if no errors
    (when (and applicable (not error))
      (setf function 'min-time-separation-constraint)
      (setf arglist (list constraint first-task second-task
			  :offset (translate-duration-for-constraint-list 
                                   chr offset)))
      (when verbose
	(format stream "~%~a ~a" function arglist)))
    ;; returned value
    (values function arglist error)))


(defmethod TRANSFORM-WINDOW-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  (let* ((start-form (second form))
	 (end-form (third form))
	 (task-name (fourth form))
	 (task (translate-name-to-task chr task-name))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'abs-task-constraint))
	   (setf applicable nil))
	  ((null task)
	   (format stream "~%**error: ~a task not found~%  task name was: ~a" form task-name)
	   (setf error t))
	  ((not (and (listp start-form)          (listp end-form)
		     (>= (length start-form) 3)  (>= (length end-form) 3)
		     (every 'numberp start-form) (every 'numberp end-form)))
	   (format stream "~%**error: ~a error in start/end date forms ~%   ~a ~a"
		   form start-form end-form)
	   (setf error t))
	  (t nil))
    ;; transform if no errors
    (when (and applicable (not error))
      (setf function 'add-abs-task-window)
      (let* ((start-tjd (apply 'ymdhms-to-jd start-form))
	     (end-tjd (apply 'ymdhms-to-jd end-form))
	     (qstart (translate-time-for-constraint-list chr start-tjd))
	     (qend   (translate-time-for-constraint-list chr end-tjd)))
	(setf arglist (list constraint task
			    (ipcf-create-with-single-interval qstart qend 1 0)))
	(when verbose
	  (format stream "~%~a ~a" function arglist))))
    ;; returned value
    (values function arglist error)))


(defmethod TRANSFORM-PHASE-CONSTRAINT 
  ((chr chronika) (constraint task-constraint) form
   &key (verbose nil) (stream t))

  "For returned args see transform-constraint.  Form is known to be a list with
    correct first element."
  
  ;; start date/time, period (d), phase min/max, task1 ...
  ;; (phase (1992 1 30 0 0 0) 20 0.0 0.1 dummy-3)

  (let* ((start-form (second form))
	 (period (third form))
	 (min-phase (fourth form))
	 (max-phase (fifth form))
	 (task-name (sixth form))
	 (task (translate-name-to-task chr task-name))
	 (function nil)
	 (arglist nil)
	 (error nil)
	 (applicable t)
	 )
    ;; error detection
    (cond ((not (typep constraint 'abs-task-constraint))
           (setf applicable nil))
          ((null task)
           (format stream "~%**error: ~a task not found~%  task name was: ~a" 
                   form task-name)
           (setf error t))
          ((not (and (listp start-form)
                     (>= (length start-form) 3)
                     (every 'numberp start-form)))
           (format stream "~%**error: ~a error in start date form ~%   ~a"
                   form start-form)
           (setf error t))
          ((not (and (numberp period) (numberp min-phase)(numberp max-phase)))
           (format stream "~%**error: ~a period & min/max phase must be numeric~%   ~a ~a ~a"
                   form period min-phase max-phase)
           (setf error t))
          ;;  *** TODO: should check that period >0 and not too small 
          ;; based on time quantum
          (t nil))
    ;; transform if no errors
    (when (and applicable (not error))
      (setf function 'add-abs-task-window)
      (let* ((start-tjd (apply 'ymdhms-to-jd start-form))
	     (chr-start-tjd (qtime-to-tjd chr (start chr)))
	     (chr-end-tjd (qtime-to-tjd chr (end chr)))
	     ;; cycles numbered s.t. T = T0 + np
	     (n-min (1- (floor (/ (- chr-start-tjd start-tjd) period))))
	     (n-max (1+ (ceiling (/ (- chr-end-tjd start-tjd) period))))
	     ;; offsets in days from period start for required phase
	     (min-delta (* min-phase period))
	     (max-delta (* max-phase period))
	     phase-interval-list
	     phase-ipcf
	     )
	;;(format t "~%~% chronika start/end: ~a ~a  zero-phase start: ~a" chr-start-tjd chr-end-tjd start-tjd)
	;;(format t "~% n min ~a max ~a" n-min n-max)
	(loop-over-value-range
	 i n-min n-max
	 (push (list (+ start-tjd (* i period) min-delta)
		     (+ start-tjd (* i period) max-delta)
		     1)
	       phase-interval-list))
	(setf phase-ipcf 
              (convert-tjd-interval-list-to-qtime-ipcf 
               chr (reverse phase-interval-list)))
	(setf arglist (list constraint task phase-ipcf))
	(when verbose
	  (format stream "~%~a ~a" function arglist))))
    ;; returned value
    (values function arglist error)))


;;; ======================================================================
;;;                                                CLASS: OBS-CSP-CHRONIKA


(defclass OBS-CSP-CHRONIKA (CONSTRAINT-LIST)
  (
   (obs-csp-chronika-name
    :reader obs-csp-chronika-name :initarg :obs-csp-chronika-name
    :documentation "name (string or keyword) for identifying this instance")
   (obs-csp-chronika-directory 
    :initform nil :reader obs-csp-chronika-directory
    :documentation 
    "directory for reading/writing anything.  Should be a logical pathname.")

   ;; chronika
   (obs-chronika-type :reader obs-chronika-type :initform 'obs-chronika)
   (obs-task-type :reader obs-task-type :initform 'obs-task)
   (obs-chronika :reader obs-chronika :initform 'obs-chronika)
   (obs-abs-constraint :reader obs-abs-constraint :initform nil)
   (obs-rel-constraint :reader obs-rel-constraint :initform nil)
   (obs-chronika-TJD-start :reader obs-chronika-TJD-start)
   (obs-chronika-TJD-end :reader obs-chronika-TJD-end)
   (obs-chronika-quantum :reader obs-chronika-quantum)

   ;; csp
   (obs-csp-type :reader obs-csp-type :initform 'obs-csp)
   (obs-var-type :reader obs-var-type :initform 'obs-var)
   (obs-csp :reader obs-csp :initform nil)

   ;; parameters
   (obs-default-priority  :reader obs-default-priority :initform 100)
   (obs-suitability-scale :reader obs-suitability-scale :initform 100)

   )
  (:documentation "Combo chronika & csp"))


(defmethod PRINT-OBJECT ((obj OBS-CSP-CHRONIKA) stream)
  (format stream "#<OBS-CSP-CHRONIKA ~a>" 
          (slot-value obj 'obs-csp-chronika-name)))


;;; ----------------------------------------------------------------------
;;; globals for OBS-CSP-CHRONIKA

(defvar *CURRENT-OBS-CSP-CHRONIKA* nil
  "Most recently created instance of OBS-CSP-CHRONIKA")

(defmacro WITH-CURRENT-OBS-CSP-CHRONIKA
          (csp chronika &rest body)
  "Execute body with symbol 'csp bound to csp, 'chronika bound to chronika
    of *CURRENT-OBS-CSP-CHRONIKA*.  For debugging."
  `(let ((,(or csp (gensym)) (obs-csp *CURRENT-OBS-CSP-CHRONIKA*))
         (,(or chronika (gensym)) (obs-chronika *CURRENT-OBS-CSP-CHRONIKA*)))
     ,@body))
#|
(with-current-obs-csp-chronika
  csp nil
  (describe csp))
|#


;;; ----------------------------------------------------------------------
;;; create & initialize OBS-CSP-CHRONIKA

(defun MAKE-OBS-CSP-CHRONIKA 
       (&rest args 
              &key 
              (obs-csp-chronika-type 'obs-csp-chronika) 
              &allow-other-keys)

  "Make instance of obs-csp-chronika of type specified, call init-obs-csp-chronika
    method, and return initialized instance"

  (let ((instance (make-instance obs-csp-chronika-type)))
    (init-obs-csp-chronika instance args)
    instance))


(defmethod INIT-OBS-CSP-CHRONIKA 
  ((occ obs-csp-chronika) args)
  
  "Initialization method for obs-csp-chronika.  Required arguments in args
    plist are:
     :name - the name string or keyword
     :directory - directory as logical pathname
     :tjd-start, :tjd-end, :tjd-quantum - times for start, end, qtime mapping
     :chronika-type - the type of chronika
     :task-type - the type of task for the chronika
     :csp-type - the type of csp
     :var-type - the type of var for the csp
     :default-priority - default priority level, number (hi value = low prio)
     :suitability-scale - numeric scale for suitability e.g. if scale is 100
       then suit of 1.0 becomes pref of 100."

  (let ((name (getf args :name))
        (directory (getf args :directory))
        (tjd-start (getf args :tjd-start))
        (tjd-end (getf args :tjd-end))
        (tjd-quantum (getf args :tjd-quantum))
        
        (chronika-type (getf args :chronika-type))
        (task-type (getf args :task-type))
        
        (default-priority (getf args :default-priority))
        (suitability-scale (getf args :suitability-scale))
        
        (csp-type (getf args :csp-type))
        (var-type (getf args :var-type))
        
        )
    
    ;; other slots
    (with-slots (obs-csp-chronika-name
                 obs-csp-chronika-directory
                 obs-default-priority 
                 obs-suitability-scale
                 obs-chronika-type
                 obs-task-type 
                 obs-csp-type 
                 obs-var-type
                 obs-chronika-TJD-start
                 obs-chronika-TJD-end
                 obs-chronika-quantum) occ
      (setf obs-csp-chronika-name name)
      (setf obs-csp-chronika-directory directory)
      (setf obs-default-priority default-priority)
      (setf obs-suitability-scale suitability-scale)
      (setf obs-chronika-type chronika-type)
      (setf obs-task-type task-type)
      (setf obs-csp-type csp-type)
      (setf obs-var-type var-type)
      (setf obs-chronika-TJD-start TJD-start)
      (setf obs-chronika-TJD-end TJD-end)
      (setf obs-chronika-quantum TJD-quantum))
    
    (setf *CURRENT-OBS-CSP-CHRONIKA* occ)
    occ))

;;; ----------------------------------------------------------------------
;;; initargs methods for chronika, csp, task, var

(defmethod CHRONIKA-INITARGS ((occ OBS-CSP-CHRONIKA))
  `(
    :obs-csp-chronika ,occ))


(defmethod TASK-INITARGS ((occ OBS-CSP-CHRONIKA))
  "Collect parameters from OBS-CSP-CHRONIKA that are arguments for 
    MAKE-TASK/INIT-TASK"
  `(
    :chr ,(obs-chronika occ)
    :task-type ,(obs-task-type occ)
    :abs-constraint ,(obs-abs-constraint occ)
    :suitability-scale ,(obs-suitability-scale occ)
    :default-priority ,(obs-default-priority occ)))


(defmethod CSP-INITARGS ((occ OBS-CSP-CHRONIKA))
  `(
    :obs-csp-chronika ,occ))


(defmethod VAR-INITARGS ((occ OBS-CSP-CHRONIKA))
  nil)


;;; ----------------------------------------------------------------------
;;; functions for setup, adding tasks, closing chronika

(defun MAKE-OBS-TASK (&rest 
                      args 
                      &key (obs-csp-chronika *CURRENT-OBS-CSP-CHRONIKA*)
                      &allow-other-keys)
  (let ((args (append args (task-initargs obs-csp-chronika))))
    (apply 'make-task args)))
                            

(defmethod OPEN-CHRONIKA ((OCC obs-csp-chronika))
  ;; set up chronika and abs & rel constraint
  (with-slots
    (obs-chronika 
     obs-abs-constraint 
     obs-rel-constraint)
    OCC

    (setf obs-chronika
          (apply 
           'make-chronika 
           (append `(
                     :name           ,(obs-csp-chronika-name occ)
                     :keep-history    nil
                     :chronika-type  ,(obs-chronika-type occ)
                     :tjd-start      ,(obs-chronika-tjd-start occ)
                     :tjd-end        ,(obs-chronika-tjd-end occ)
                     :tjd-quantum    ,(obs-chronika-quantum occ))
                   (chronika-initargs occ))))
    (setf obs-abs-constraint 
          (make-abs-task-constraint 
           (intern (string-upcase 
                    (format nil "ABS-~a" (obs-csp-chronika-name occ))))))
    (setf obs-rel-constraint 
          (make-binary-rel-constraint 
           (intern (string-upcase 
                    (format nil "REL-~a" (obs-csp-chronika-name occ))))))
    ;; returned value is chronika
    obs-chronika))

(defmethod CLOSE-CHRONIKA ((occ obs-csp-chronika))
  (with-slots (obs-abs-constraint obs-rel-constraint obs-chronika) occ
    (apply-constraints occ obs-chronika obs-abs-constraint)
    (apply-constraints occ obs-chronika obs-rel-constraint)
    (activate-constraint obs-abs-constraint)
    (make-path-consistent obs-rel-constraint)
    (activate-constraint obs-rel-constraint)
    (check-consistency obs-chronika)
    (print-task-diagnostics obs-chronika)))

(defmethod MAKE-CSP-FOR-OBS-CSP-CHRONIKA ((occ obs-csp-chronika))
  (with-slots
    (obs-chronika obs-csp-type obs-var-type obs-csp)
    occ
    (setf obs-csp 
          (make-csp-for-chronika obs-chronika 
                                 obs-csp-type (csp-initargs occ)
                                 obs-var-type (var-initargs occ)
                                 ))))

(defmethod CLOSE-OBS-CSP-CHRONIKA ((occ obs-csp-chronika))
  (close-chronika occ)
  (make-csp-for-obs-csp-chronika occ))

;;; ----------------------------------------------------------------------
;;; misc functions

(defmethod GAP-LIST ((occ obs-csp-chronika))
  "return list of gaps ((start end)...) in qtime"
  (gap-list (obs-csp occ)))


(defmethod BEST-GAP-FILLERS ((occ obs-csp-chronika) start end
                             &key (exact nil))
  "Return ordered list of tasks that can start at specified start
     and end at or before specified end.  Ordering is by
     suitability at start.  If :exact is t, only tasks which
     exactly fit the interval are returned"
  (let* ((candidates (tasks-fitting-in-interval (obs-chronika occ) start end
                                                :exact exact))
         (sorted-candidates nil))
    (setf sorted-candidates
          (sort candidates
                #'(lambda (x y)
                    (> (ipcf-get-value start (suitability x))
                       (ipcf-get-value start (suitability y))))))
    sorted-candidates))

#| from this, it would not be difficult to make a gap filler routine
    to fill holes in the schedule, or identify backup observations
(let ((start 53) (dur 2))
  (dolist (task (best-gap-fillers 
                 *euve-schedule* start (+ start dur) :exact t))
    (format t "~%~20a dur: ~3d  suit: ~,4f"
            task (duration-given-start-at task start)
            (ipcf-get-value start (suitability task)))))
(loop-over-value-range
 i (qtime-start (obs-chronika *euve-schedule*))
 (qtime-end (obs-chronika *euve-schedule*))
 (format t "~%t=~3d" i)
 (dolist (task (task-list (obs-chronika *euve-schedule*)))
   (format t "   ~a ~3d ~,3f"
            (id task) (duration-given-start-at task i)
            (ipcf-get-value i (suitability task)))))

;; see whether gaps can be filled by ANY task at all
(dolist (gap (gap-list *EUVE-SCHEDULE*))
  (let* ((start (first gap)) 
	 (end (second gap))
	 (dur (- end start)))
    (format t "~%Gap from ~a to ~a" (first gap) (second gap))
    (dolist (task (task-list (obs-chronika *EUVE-SCHEDULE*)))
      (let ((val-ranges (task-can-overlap-interval task start end)))
	(when val-ranges
	  (format t "~%   ~20a assigned to: ~4a  value ranges: ~a, times"
		  (id task) 
		  (assignment (task-var task))
		  val-ranges)
	  (dolist (val-range val-ranges)
	    (loop-over-value-range 
	     i (first val-range) (second val-range)
	     (multiple-value-bind
		 (t1 t2)
		 (fdsia-val-to-time (fdsia task) i)
	     (format t " (~a,~a)" t1 t2)
	     (if (and (= t1 start) (= t2 end))
		 (format t "<--+ "))))))))))

|#


;;; ----------------------------------------------------------------------
;;; Scheduling search methods

(defmethod EARLY-GREEDY-SCHEDULE ((occ obs-csp-chronika) 
                                  &key (only-vars nil))
  (let ((csp (obs-csp occ)))
    (if only-vars
      (unmake-assignments csp :to-vars only-vars)
      (reset-csp csp))
    (make-assignments 
     csp
     :to-vars                   :all-unassigned-with-no-conflict-values 
     :limited-to-vars           only-vars
     :strategy '(;; var
                 ((first-time-with-no-conflicts :min)
                  (priority :max)
                  (preference-on-first-time-with-no-conflicts :max)
                  :first)
                 ;; value
                 ((conflicts-on-value :min)
		  :first))
     :verbose nil)
    ))


(defmethod ELMINC-SCHEDULE ((occ obs-csp-chronika)
			    &key (only-vars nil))
  (let ((csp (obs-csp occ)))

    (if only-vars
      (unmake-assignments csp :to-vars only-vars)
      (reset-csp csp))
    
    ;; initial guess elminc
    (make-assignments 
     csp
     :to-vars                    :all-unassigned
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((first-time-with-min-conflicts :min)
                  (values-with-min-conflicts-count :min))
                 ;; value
                 ((conflicts-on-value :min)
                  ;; (value-preference :max)
                  :first))
     :verbose nil)
    
    ;; repair (same for elminc and maxpref)
    (make-assignments 
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   (* 2 (var-count csp))
     :strategy '(;; var
                 ((conflicts-on-assigned-value :max))
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)))
     :verbose nil)
    
    ;; unassign conflicts: max conflicts & min preference
    (unmake-assignments
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   nil
     :strategy '((conflicts-on-assigned-value :max)
                 (assigned-value-preference :min)))
    
    ;; fill in with greedy anytime max pref
    (make-assignments 
     csp 
     :to-vars                    :all-unassigned-with-no-conflict-values 
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((max-preference-on-min-conflict-values :max) 
                  :random)
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)
                  :first))
     :verbose nil)
    (format t "~a~%" (csp-state-summary csp))
  ))


(defmethod MAX-PREF-SCHEDULE ((occ obs-csp-chronika) &key (only-vars nil))
  (let ((csp (obs-csp occ)))

    (if only-vars
	(unmake-assignments csp :to-vars only-vars)
	(reset-csp csp))
    
    ;; initial guess: max-pref min-conflicts
    (make-assignments 
     csp
     :to-vars                    :all-unassigned
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((max-preference-on-min-conflict-values :max))
                 ;; value
                 ((value-preference :max)
                  (conflicts-on-value :min)))
     :verbose nil)

    ;; repair (same for elminc and maxpref)
    (make-assignments 
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   (* 2 (var-count csp))
     :strategy '(;; var
                 ((conflicts-on-assigned-value :max))
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)))
     :verbose nil)
    
    ;; unassign conflicts: max conflicts & min preference
    (unmake-assignments
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   nil
     :strategy '((conflicts-on-assigned-value :max)
                 (assigned-value-preference :min)))
    
    ;; fill in with greedy anytime max pref
    (make-assignments 
     csp 
     :to-vars                    :all-unassigned-with-no-conflict-values 
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((max-preference-on-min-conflict-values :max) 
                  :random)
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)
                  :first))
     :verbose nil)
    (format t "~a~%" (csp-state-summary csp))

  ))


(defmethod HI-PRIO-MAX-PREF-SCHEDULE ((occ obs-csp-chronika) &key (only-vars nil))
  (let ((csp (obs-csp occ)))

    (if only-vars
	(unmake-assignments csp :to-vars only-vars)
	(reset-csp csp))
    
    ;; initial guess: priority max-pref min-conflicts
    (make-assignments 
     csp
     :to-vars                    :all-unassigned
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((priority :min)
                  (max-preference-on-min-conflict-values :max))
                 ;; value
                 ((value-preference :max)
                  (conflicts-on-value :min)))
     :verbose nil)
    
    ;; repair (same for elminc and maxpref)
    (make-assignments 
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   (* 2 (var-count csp))
     :strategy '(;; var
                 ((conflicts-on-assigned-value :max))
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)))
     :verbose nil)
    
    ;; unassign conflicts: max conflicts & min preference
    (unmake-assignments
     csp
     :to-vars                    :all-with-conflicts
     :limited-to-vars            only-vars
     :how-many                   nil
     :strategy '((priority :max)
                 (conflicts-on-assigned-value :max)
                 (assigned-value-preference :min)))
    
    ;; fill in with greedy anytime max pref
    (make-assignments 
     csp 
     :to-vars                    :all-unassigned-with-no-conflict-values 
     :limited-to-vars            only-vars
     :strategy '(;; var
                 ((priority :min)
                  (max-preference-on-min-conflict-values :max) 
                  :random)
                 ;; value
                 ((conflicts-on-value :min)
                  (value-preference :max)
                  :first))
     :verbose nil)
    (format t "~a~%" (csp-state-summary csp))

  ))

         
#|
(setf -occ
      (make-obs-csp-chronika 
       :obs-csp-chronika-type 'obs-csp-chronika
       :name :fred 
       :directory "spike:obs;"
       :tjd-start 4500.0 
       :tjd-end 4510.0 
       :tjd-quantum 1.0
       :chronika-type 'obs-chronika
       :task-type     'fixed-obs-task
       :csp-type      'obs-csp
       :var-type      'obs-var
       :default-priority 100
       :suitability-scale 100))
(open-chronika -occ)
(describe -occ)
(describe (obs-chronika -occ))
(task-initargs -occ)

(make-obs-task
 :id :foo1
 :name "Foo X-1"
 :exp-time 86400
 :priority 3
 :ra 12.34
 :dec -15.112
 :available-time-intervals '((4500.0 4510.0 1))
 :abs-constraint-intervals '((4500.0 4505.0 1)(4505.0 4511.0 0.5))
 )
(make-obs-task
 :id :foo2
 :name "Foo X-2"
 :exp-time (* 2 86400)
 :priority 47
 :ra 12.34
 :dec -16.112
 :available-time-intervals '((4500.0 4510.0 1))
 :abs-constraint-intervals '((4500.0 4505.0 1)(4505.0 4511.0 0.5))
 )
(setf foo1 (translate-name-to-task (obs-chronika -occ) :foo1))
(setf foo2 (translate-name-to-task (obs-chronika -occ) :foo2))
(task-angular-separation foo1 foo2)
(precedence-constraint (obs-rel-constraint -occ) foo1 foo2 :offset 2)
(exposure-duration-at-time foo1 2)
(exposure-duration-at-time foo2 2)
(describe foo1)
(describe foo2)

(close-chronika -occ)
(make-csp-for-obs-csp-chronika -occ)

(close-obs-csp-chronika -occ)


(describe (obs-csp -occ))
(setf v0 (var-named (obs-csp -occ) :foo1))
(setf v1 (var-named (obs-csp -occ) :foo2))
(describe v0)
(describe v1)
(var-overlap-effect (obs-csp -occ) v0 5)
(var-overlap-effect (obs-csp -occ) v1 5)
(var-effect (obs-csp -occ) v1 3)
(show-csp-state (obs-csp -occ) :preferences t)
(assign-value v0 5)
(assigned-time v0)
(unassign-value v0)
(assign-value v1 8)
(assigned-time v1)
(task-report v0)
(schedule-report (obs-csp -occ))
(with-current-obs-csp-chronika
  csp nil
  (describe csp))

;; make-task supports the following basic keyword arguments:
;;   (id nil)
    :name - target name (string)
    :exp-time - initial exposure time (seconds)
    :priority - priority, number, lower numbers mean higher priority
    :available-time-intervals - interval list, time in TJD, 
      value is fraction of wall-clock time that is available for exposing.
      This is effectively observing efficiency.
    :abs-constraint-intervals - interval list, time in TJD, 
      value is suitability for taking data at time.  Note that this
      is NOT start time suitability.
    :abs-constraint - instance of abs constraint for task suitability
    :suitability-scale - integral value corresponding to suitability 1
    :default-priority - if priority not specified, this is value used"

|#

;;; ======================================================================
;;;                                               CLASS: SEGMENTED-OBS-CSP
;;; 
;;; subtype of OSB-CSP, CAP-CSP, and TIME-TO-SEGMENT-MAPPING, i.e. the
;;; CSP instance specifies the mapping from time to segments
;;;

(defclass SEGMENTED-OBS-CSP (OBS-CSP 
                             GENERAL-TIME-TO-SEGMENT-MAPPING)
  (
   (oversubscription-factor :reader oversubscription-factor :initform nil)
   (capacity-unit-in-quanta :reader capacity-unit-in-quanta :initform nil)
   )
  (:documentation "Subtype of OBS-CSP w/segments for capacity constraints"))


(defmethod PRINT-OBJECT ((obj SEGMENTED-OBS-CSP) stream)
  (format stream "#<SEGMENTED-OBS-CSP ~a>" (slot-value obj 'name)))


(defmethod INIT-CSP :AFTER ((csp SEGMENTED-OBS-CSP) args)
  (with-slots
   (oversubscription-factor capacity-unit-in-quanta)
   csp
   
   (setf oversubscription-factor (getf args :oversubscription-factor))
   (setf capacity-unit-in-quanta (getf args :capacity-unit-in-quanta))))


;;; ======================================================================
;;;                                               CLASS: SEGMENTED-OBS-VAR
;;; var belonging to a SEGMENTED-OBS-CSP
;;;

(defclass SEGMENTED-OBS-VAR (OBS-VAR)
  ((bin-to-values-cache 
    :initform nil :reader bin-to-values-cache
    :documentation
    "Cache for the transformation from bins to values for capacity
      constraints.  Array of length (total-number-of-segments CSP)
      such that (aref bin-to-values-cache bin) => list of values in bin.
      Filled when bins referenced.")
   (value-to-bin-cache :initform nil :reader value-to-bin-cache)
   (required-capacity-cache :initform nil :reader required-capacity-cache)
   (max-required-capacity-amount :initform nil 
                                 :reader max-required-capacity-amount)
   )
  (:documentation "var belonging to a segmented-obs-csp"))


(defmethod SCHEDULABLE-IN-DATE-RANGE ((var SEGMENTED-OBS-VAR) min-date max-date)
  "var is variable with corresponding task.
    Return t if var is schedulable after min-date and before max-date.
    Dates are TJD.
    If min-date is nil, no test is made and t is returned.
    If max-date is nil, no test is made and t is returned."
  
  (let* ((task (task var))
         (FDSIA (FDSIA task))
         (chronika (chronika task))
         (min-qtime (if min-date (tjd-to-qtime chronika min-date)))
         (max-qtime (if max-date (tjd-to-qtime chronika max-date))))
    
    (cond ((and min-date (not max-date))
           ;;(format t "~%min qtime: ~a" min-qtime)
           (loop-over-value-range
            val 0 (1- (value-count var))
            ;;(format t "~%val: ~a qtime ~a" val (FDSIA-val-to-time FDSIA val))
            (when (>= (FDSIA-val-to-time FDSIA val) min-qtime)
              ;;(format t "~%-- min OK!!")
              (return t))))
          
          ((and (not min-date) max-date)
           ;;(format t "~%max qtime: ~a" max-qtime)
           (loop-over-value-range
            val 0 (1- (value-count var))
            ;;(format t "~%val: ~a qtime ~a" val (FDSIA-val-to-time FDSIA val))
            (when (<= (FDSIA-val-to-time FDSIA val) max-qtime)
              ;;(format t "~%-- max OK!!")
              (return t))))
          
          ((and min-date max-date)
           (loop-over-value-range
            val 0 (1- (value-count var))
            ;;(format t "~%val: ~a qtime ~a" val (FDSIA-val-to-time FDSIA val))
            (when (and (<= (FDSIA-val-to-time FDSIA val) max-qtime)
                       (>= (FDSIA-val-to-time FDSIA val) min-qtime))
              ;;(format t "~%-- both min & max OK!!")
              (return t))))
          
          (t t))))

;;; ======================================================================
;;;                                    CLASS: SEGMENTED-OBS-CAP-CONSTRAINT

(defclass SEGMENTED-OBS-CAP-CONSTRAINT (CAPACITY-CONSTRAINT)
  ()
  (:documentation "type of capacity constraint for segmented-obs-csp's"))

;;; -----------------------------------------------------------------------
;;; methods for SEGMENTED-OBS-CAP-CONSTRAINT

(defmethod VALUE-TO-BIN 
  ((c segmented-obs-csp) (capcon segmented-obs-cap-constraint) 
   (v segmented-obs-var) value)
  
  ;; map value to time, then time to segment
  ;; cache stores bin number if there is one, else -1, in which case this
  ;; function returns nil
  
  (let ((cache (value-to-bin-cache v)))
    
    ;; store the cache unless it's already there
    (unless cache
      (let ((count (value-count v))
            (fdsia (fdsia (task v))))
        (setf cache (make-array count :element-type 'fixnum :initial-element -1))
        (dotimes (i count)
          (setf (aref cache i)
                (or (time-to-segment c (fdsia-val-to-time fdsia i))
                    -1))))
      (setf (slot-value v 'value-to-bin-cache) cache))
    
    ;; return the bin corresponding to the input value, nil if no correspondence
    (let ((cache-contents (aref cache value)))
      (if (= cache-contents -1) nil cache-contents))))

#|
(time-to-segment (obs-csp -occ) 8)
(describe (obs-csp -occ))
(setf -capcon (make-instance 'capacity-constraint))
(setf (slot-value -capcon 'bin-count) 2)
(describe -capcon)
(describe v0)
(dotimes (i (value-count v0))
  (format t "~%~a: ~a" i 
          (value-to-bin (obs-csp -occ) -capcon v0 i)))
(bin-to-values (obs-csp -occ) -capcon v0 0)
(bin-to-values (obs-csp -occ) -capcon v0 1)
(setf (slot-value v0 'bin-to-values-cache) nil)
(setf (slot-value v0 'value-to-bin-cache) nil)
(print (bin-to-values-cache v0))
(print (value-to-bin-cache v0))
|#

(defmethod BIN-TO-VALUES 
  ((c segmented-obs-csp) (capcon segmented-obs-cap-constraint) 
   (v segmented-obs-var) bin)
  
  ;; get start/end times of bin, then convert to values
  ;; set up cache if not already done
  (unless (bin-to-values-cache v)
    ;; no, set up cache
    (setf (slot-value v 'bin-to-values-cache)
          (make-array (total-number-of-segments c) :initial-element nil))
    (dotimes (b (total-number-of-segments c))
      (let* ((start (start-of-segment c b))
             (end (end-of-segment c b))
             (val-range (FDSIA-time-range-to-val-range (FDSIA (task v)) 
                                                       (list start end)))
             (result nil))
        (when val-range
          (loop-over-value-range
           i (first val-range) (second val-range)
           (push i result)))
        (setf (aref (bin-to-values-cache v) b) (nreverse result)))))

  ;; returned value
  (aref (bin-to-values-cache v) bin))


(defmethod SEGMENTED-OBS-CSP-SET-REQ-CAPACITY-CACHE 
  ((c segmented-obs-csp) (capcon segmented-obs-cap-constraint) 
   (v segmented-obs-var))

  "fill the required-capacity-cache and max-required-capacity-amount slot"

  (let* ((capacity-unit (capacity-unit-in-quanta c))
         (task (task v))
         cache)
    (setf cache
          (map 'vector
               #'(lambda (x) (round (/ x capacity-unit)))
               ;; this gives duration in qtime quanta: convert to capacity quanta
               (fdsia-map-to-interval-list 
                (fdsia task)
                (ipcf-to-interval-list (duration-ipcf task)))))
    (setf (slot-value v 'required-capacity-cache) cache)
    (setf (slot-value v 'max-required-capacity-amount) (vector-max cache))
    ;; returned value is cache
    cache))


(defmethod REQUIRED-CAPACITY 
  ((c segmented-obs-csp) (capcon segmented-obs-cap-constraint) 
   (v segmented-obs-var) bin value)
  (declare (ignore bin))
  (let ((cache (required-capacity-cache v)))
    (unless cache
      (setf cache 
            (segmented-obs-csp-set-req-capacity-cache c capcon v)))
    (aref cache value)))


(defmethod MAX-REQUIRED-CAPACITY 
  ((c segmented-obs-csp) (capcon segmented-obs-cap-constraint) 
   (v segmented-obs-var))
  
  (let ((amount (max-required-capacity-amount v)))
    (when (null amount)
      (segmented-obs-csp-set-req-capacity-cache c capcon v)
      (setf amount (max-required-capacity-amount v)))
    amount))


;;; CONSUMED-CAPACITY defaults to usual method, i.e. to REQUIRED-CAPACITY


(defmethod CHECK-BIN-TO-VALUE-MAPPING ((OCC obs-csp-chronika)
                                       cap-constraint-name
                                       &key (verbose nil))
  
  "Look for any errors in value-to-bin and bin-to-value mapping.  Input
    is obs-csp-chronika and capacity constraint name (used in 
    find-capacity-constraint.  Prints mismatches."
  
  (let* ((csp (obs-csp OCC))
         (cap (find-capacity-constraint (obs-csp OCC) cap-constraint-name))
         ;;(v (var (obs-csp -occ) 0))
         ;;(bin-count (bin-count cap))
         )
    (do-for-all-vars
     :csp csp :var v
     :form
     (progn
       (format t "~%var: ~a ~a" (var-number v) (name v))
       (dotimes (i (value-count v))
         (let* ((bin (value-to-bin csp cap v i))
                (vals (if bin (bin-to-values csp cap v bin)))
                (err (and bin (not (member i vals)))))
           (if verbose
             (format t "~%  ~a val: ~a bin: ~a ~a" 
                     (if err "***" "   ") i bin vals)
             (if err
               (format t "~%   *** error on val ~a: bin ~a vals: ~a"
                       i bin vals)))))))))


;;; -----------------------------------------------------------------------
;;; reporting SEGMENTED-OBS-CSPs

;;; Notes:
;;; Use of task slots:
;;;  efficiency-ipcf:  time in qtime, value is fraction of elapsed wall-clock
;;;   time available for exposure
;;;  duration-ipcf:  time in qtime, value is wall-clock time required to obtain
;;;   desired exposure time in fractional qtime
;;;  FDSIA uses 0 durations since only allocation to bins is relevant
;;;  duration-given-start-at, etc.: these methods use FDSIA, so durations
;;;   are always 0.

;; (schedule-report (obs-csp *astrod-long-term-schedule*))


(defmethod SCHEDULE-REPORT ((csp segmented-obs-csp) &key (stream t) &allow-other-keys)
  
  "Report schedule status to stream"
  ;; (schedule-report (obs-csp -occ))
  ;; (obs-cap-constraint (obs-csp-chronika (obs-csp -occ)))
    
  (let* ((assigned-vars (all-assigned-vars csp))
         (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
         (chronika (chronika (task (first (all-vars csp)))))
         (cap-con (obs-cap-constraint (obs-csp-chronika csp)))
         (cap-unit-quanta (capacity-unit-in-quanta csp))
         (cap-unit-days (duration-in-qtime-to-days chronika cap-unit-quanta))
         (oversub (oversubscription-factor csp))
         (overall-start (start chronika))
         (overall-end   (end chronika))
         (sched-days (duration-in-qtime-to-days 
                      chronika (- overall-end overall-start)))
         (total-sched 0) ;qtime
         total-sched-days ;days
         (total-exp 0) ;seconds
         total-exp-days ;days
         (prev-bin nil)
         (current-bin nil)
         )
    
    ;; report all assignments and gaps
    (format stream "~%Scheduled activities (oversubscription limit ~,1f%)"
            (* 100 oversub))
    
    ;; report assigned vars
    (dolist (var sorted-vars)
      (setf current-bin (value-to-bin csp cap-con var (assigned-value var)))
      (when (or (null prev-bin)
                (/= prev-bin current-bin))
        (setf prev-bin current-bin)
        (let ((limit (aref (bin-limit cap-con) current-bin))
              (usage (aref (bin-usage cap-con) current-bin)))
          (format stream "~%~%Segment: ~3d  limit: ~6,2fd, used ~6,2fd =~6,2f%   ~a to ~a"
                  current-bin
                  (* limit cap-unit-days)
                  (* usage cap-unit-days)
                  (* 100 (/ usage limit))
                  (task-formatted-abs-qtime 
                   (task var) (start-of-segment csp current-bin))
                  (task-formatted-abs-qtime 
                   (task var) (+ (start-of-segment csp current-bin)
                                 (duration-of-segment csp current-bin)))
                  ))
        (format stream
                "~%Name            Start                   dur   efficiency    exp time (s)   conf pref  pref/")
        (format stream
                "~%                                        (d)   act.   max   actual  request             max")
        (format stream
                "~%--------------- --------------- ----- ------ ----- ----- -------- -------- ---- ----- -----")
        
        )
      (incf total-sched 
            (ipcf-get-value (assigned-time var) (duration-ipcf (task var))))
      (incf total-exp (exposure-duration-at-time (task var) (assigned-time var)))
      (format stream "~%~a" (task-report var))
      )
    
    (setf total-sched-days (duration-in-qtime-to-days chronika total-sched))
    (setf total-exp-days (/ total-exp 86400.0))
    (format stream "~%~%Schedule time span  : ~6,2f days" sched-days)
    (format stream "~%Total scheduled time: ~6,2f days (~6,2f%)" 
            total-sched-days
            (* 100 (/ total-sched-days sched-days)))
    (when (> total-exp 0)
      (format stream "~%Total exp time      : ~6,2f days (~d sec, ~6,2f% of schedule time span)"
              total-exp-days total-exp 
              (* 100 (/ (/ total-exp 86400.0) sched-days)))
      (format stream "~% exposure efficiency: ~6,2f %    (~d sec exposure time in ~6,2fd)"
              (* 100 (/ total-exp-days total-sched-days))
              total-exp total-sched-days))
    
    (unless (all-vars-assigned-p csp)
      (setf total-exp 0)
      (format stream "~%~%Unscheduled activities:")
      (format stream
              "~%Name            Start                   dur   efficiency    exp time (s)   conf pref  pref/")
      (format stream
              "~%                                        (d)   act.   max   actual  request             max")
      (format stream
              "~%--------------- --------------- ----- ------ ----- ----- -------- -------- ---- ----- -----")
      (dolist (var (all-unassigned-vars csp))
        (incf total-exp (exp-time (task var)))
        (format stream "~%~a" (task-report var))
        )
      (setf total-exp-days (/ total-exp 86400.0))
      (format stream "~%Total exp time      : ~6,2f days (~d sec)"
              total-exp-days total-exp))
    
    
    (format stream "~% ~d tasks, ~a assigned, ~a assigned with conflicts"
            (var-count csp) (assigned-var-count csp) (conflicting-var-count csp))
    (format stream "~% mean preference  : ~8,2f  mean conflicts  : ~8,2f"
            (mean-preference csp) (mean-conflict-count csp))
    (format stream "~% summed preference: ~8d  summed conflicts: ~8d"
            (summed-preference csp) (summed-conflict-count csp))
    
    ))

(defmethod TASK-REPORT ((var segmented-obs-var))
  "Return string describing scheduling status of var"
  (let* ((task (task var))
         (chronika (chronika task))
         (name (target-name task))
         (exp-time (exp-time task))
         (assigned-value (assigned-value var))
         (start-end (multiple-value-list (assigned-time var)))
         (start (first start-end))
         ;; (end   (second start-end))
         (conf (conflicts-on-assigned-value var))
         (max-pref (max-preference var))
         ;; (min-duration (minimum-nonzero-duration task))
         (max-effic (ipcf-max (efficiency-ipcf task)))
         ;; only valid if start is known
         assigned-pref assigned-duration assigned-exp-time assigned-effic
         )
    (when start ;if there is an assignment
      (setf 
       assigned-pref (value-preference var assigned-value)
       assigned-duration (duration-in-qtime-to-days 
                          chronika (ipcf-get-value start (duration-ipcf task)))
       assigned-exp-time (exposure-duration-at-time task start)
       assigned-effic (ipcf-get-value start (efficiency-ipcf task))))
    
    (if start
      (format nil "~15a ~16a ~4d ~5,2fd ~5,2f ~5,2f ~7ds ~7ds ~4d ~5d ~5,2f"
              name 
              (task-formatted-abs-qtime task start)
              start 
              assigned-duration
              assigned-effic
              max-effic
              assigned-exp-time
              exp-time
              conf 
              assigned-pref
              (if (/= max-pref 0) (/ assigned-pref max-pref) 0))
      ;; unassigned
      (format nil "~15a ~16a ~4a ~5a  ~5a ~5,2f ~7a  ~7ds"
              name 
              ""
              "" 
              ""
              ""
              max-effic
              ""
              exp-time))))

;;; ======================================================================
;;;                                      CLASS: SEGMENTED-OBS-CSP-CHRONIKA

(defclass SEGMENTED-OBS-CSP-CHRONIKA (OBS-CSP-CHRONIKA)
  (
   ;; these are in days, will be rounded to quanta
   ;; -- slots for uniform segmentation
   (obs-segment-start :reader obs-segment-start) ;TJD
   (obs-segment-duration :reader obs-segment-duration) ;days
   (obs-segment-count :reader obs-segment-count)
   ;; -- slots for arbitrary segmentation
   (obs-segment-list 
    :reader obs-segment-list :initform nil
    :documentation
    "List of segment boundary times (t1 t2 t3 ...)")

   (obs-segment-capacity-unit-in-days :reader obs-segment-capacity-unit-in-days) ;days
   ;; fraction, 0 means no oversubscription, 0.2 means 20%, etc.
   (obs-segment-oversubscription :reader obs-segment-oversubscription) 

   ;; this is the special cap constraint corresponding to time
   (obs-cap-constraint-type :reader obs-cap-constraint-type :initform nil)
   (obs-cap-constraint :reader obs-cap-constraint :initform nil)
   )
  (:documentation "obs-csp-chronika type with segmented capacity constraint"))


(defmethod PRINT-OBJECT ((obj segmented-obs-csp-chronika) stream)
  (format stream "#<SEGMENTED-OBS-CSP-CHRONIKA ~a>"
          (slot-value obj 'obs-csp-chronika-name)))

;;; ----------------------------------------------------------------------
;;; initialization and setup

(defmethod INIT-OBS-CSP-CHRONIKA :AFTER ((SOCC segmented-obs-csp-chronika) args)
  
  (let ((segment-start (getf args :segment-start))
        (segment-duration (getf args :segment-duration))
        (segment-count (getf args :segment-count))
        (segment-capacity-unit-in-days (getf args :segment-capacity-unit-in-days))
        (segment-oversubscription (getf args :segment-oversubscription))
        (cap-constraint-type (getf args :capacity-constraint-type))
        (segment-list (getf args :segment-list)))
    
    (with-slots
      (obs-segment-start 
       obs-segment-duration 
       obs-segment-count
       obs-segment-list
       obs-segment-capacity-unit-in-days 
       obs-segment-oversubscription
       obs-cap-constraint-type)
      SOCC
      
      (setf obs-segment-start segment-start)
      (setf obs-segment-duration segment-duration)
      (setf obs-segment-count segment-count)
      (setf obs-segment-list segment-list)
      (setf obs-segment-capacity-unit-in-days segment-capacity-unit-in-days)
      (setf obs-segment-oversubscription segment-oversubscription)
      (setf obs-cap-constraint-type cap-constraint-type))))
                       

(defmethod CSP-INITARGS ((SOCC SEGMENTED-OBS-CSP-CHRONIKA))
  (append (call-next-method SOCC)
	  `(
	    :oversubscription-factor ,(obs-segment-oversubscription SOCC)
	    :capacity-unit-in-quanta 
            ,(duration-in-days-to-fractional-qtime
              (obs-chronika SOCC) (obs-segment-capacity-unit-in-days SOCC)))))


(defmethod MAKE-CSP-FOR-OBS-CSP-CHRONIKA :AFTER 
  ((SOCC segmented-obs-csp-chronika))
  
  (with-slots
    (obs-chronika 
     obs-csp 
     obs-segment-start 
     obs-segment-duration
     obs-segment-count
     obs-segment-list
     obs-cap-constraint
     obs-cap-constraint-type
     obs-segment-oversubscription
     obs-segment-capacity-unit-in-days)
    SOCC
    
    ;; set up segmentation: either uniform, or specified segments
    (cond ((and obs-segment-start obs-segment-count obs-segment-duration)
           (init-time-to-segment-mapping
            obs-csp
            :segment-start (TJD-to-qtime obs-chronika obs-segment-start)
            :segment-duration (duration-in-days-to-qtime 
                               obs-chronika obs-segment-duration)
            :segment-count obs-segment-count))
          (obs-segment-list
           ;; arbitrary segments
           (let ((segs nil) 
                 (first-time (TJD-to-qtime obs-chronika (first obs-segment-list))))
             (dolist (time (rest obs-segment-list))
               (push (list first-time 
                           (1- (TJD-to-qtime obs-chronika time)))
                     segs)
               (setf first-time (TJD-to-qtime obs-chronika time)))
             (init-time-to-segment-mapping
              obs-csp :segments (reverse segs))))
          (t (error "can't set up time-to-segment-mapping")))
    
    ;; make capacity constraint
    (let ((limit (make-array 
                  (total-number-of-segments obs-csp)
                  :element-type 'fixnum)))
      ;; build limit array
      (dotimes (i (total-number-of-segments obs-csp))
        (setf (aref limit i)
              (round (* (+ 1 obs-segment-oversubscription)
                        (/ (duration-in-qtime-to-days
                            obs-chronika (duration-of-segment obs-csp i))
                           obs-segment-capacity-unit-in-days)))))
      
      (setf obs-cap-constraint
            (make-capacity-constraint 
             obs-csp
             :capcon-type obs-cap-constraint-type
             :name (intern (string-upcase (format nil "CAP-~a" 
                                                  (obs-csp-chronika-name SOCC))))
             :limit-array limit
             :remove-values-if-capacity-exceeded :permanent
             :remove-values-if-not-in-any-bin    :permanent
             :weight 1)))
    
    ))


(defmethod BIN-LIST ((SOCC segmented-obs-csp-chronika))
  
  "List bin info"
    
  (let* ((chronika (obs-chronika SOCC))
	 (csp (obs-csp SOCC))
         (cap-con (obs-cap-constraint SOCC)))

    (cond 
     
     ;; cps or capacity constraint not present - return nil (no bins defined)
     ((or (not csp) (not cap-con))
      nil)
     
     (t ; get bin info
      (let* ((bin-count (bin-count cap-con))
             (cap-unit-quanta (capacity-unit-in-quanta csp))
             (cap-unit-days (duration-in-qtime-to-days chronika cap-unit-quanta))
             ;; (oversub (oversubscription-factor csp))
             ;; (overall-start (start chronika))
             ;; (overall-end   (end chronika))
             ;; (sched-days (duration-in-qtime-to-days chronika (- overall-end overall-start)))
             (result nil))
        
        ;; report assigned vars
        (dotimes (current-bin bin-count)
          
          (let ((limit (aref (bin-limit cap-con) current-bin))
                (usage (aref (bin-usage cap-con) current-bin)))
            #| print for testing
	(format t "~%Segment: ~3d  limit: ~6,2fd, used ~6,2fd =~6,2f%   ~a to ~a"
		current-bin
		(* limit cap-unit-days)
		(* usage cap-unit-days)
		(* 100 (/ usage limit))
		(qtime-to-tjd chronika (start-of-segment csp current-bin))
		(qtime-to-tjd chronika (+ (start-of-segment csp current-bin)
					  (duration-of-segment csp current-bin)))
		)
        |#
            (push (list current-bin 
                        (qtime-to-tjd chronika (start-of-segment csp current-bin))
                        (qtime-to-tjd chronika (+ (start-of-segment csp current-bin)
                                                  (duration-of-segment csp current-bin)))
                        (* limit cap-unit-days)
                        (* usage cap-unit-days)
                        (coerce (* 100 (/ usage limit)) 'float)
                        )
                  result)))
        ;; returned value
        (nreverse result))))))

#|
(setf -occ
      (make-obs-csp-chronika
       :obs-csp-chronika-type 'segmented-obs-csp-chronika
       :name :fred 
       :directory "xanadu:may91:"
       :tjd-start 4500.0 
       :tjd-end 4510.0 
       :tjd-quantum 1.0
       :chronika-type 'obs-chronika
       :task-type     'fixed-obs-task
       :csp-type      'segmented-obs-csp
       :var-type      'segmented-obs-var
       :default-priority 100
       :suitability-scale 100
       ;; new for segmented
       :capacity-constraint-type 'segmented-obs-cap-constraint
       :segment-start 4500.0
       :segment-duration 5.0
       :segment-count 2
       :segment-capacity-unit-in-days 0.01
       :segment-oversubscription 0))
;; non-uniform segments
(setf -occ
      (make-obs-csp-chronika
       :obs-csp-chronika-type 'segmented-obs-csp-chronika
       :name :fred 
       :directory "xanadu:may91:"
       :tjd-start 4500.0 
       :tjd-end   4510.0 
       :tjd-quantum 1.0
       :chronika-type 'obs-chronika
       :task-type     'fixed-obs-task
       :csp-type      'segmented-obs-csp
       :var-type      'segmented-obs-var
       :default-priority 100
       :suitability-scale 100
       ;; new for segmented
       :capacity-constraint-type 'segmented-obs-cap-constraint
       :segment-list '(4501.0 4503.0 4505.0 4509.0 4515.0)
       :segment-capacity-unit-in-days 0.01
       :segment-oversubscription 0))
(describe -occ)
(bin-list -occ)
(open-chronika -occ)
(describe -occ)
(describe (obs-rel-constraint -occ))
(describe (obs-chronika -occ))
(print (task-initargs -occ))
(print (csp-initargs -occ))
(print (var-initargs -occ))
(print (chronika-initargs -occ))

(make-obs-task
 :id :foo1
 :name "Foo X-1"
 :exp-time 86400
 :priority 3
 :ra 12.34
 :dec -15.112
 :available-time-intervals '((4500.0 4510.0 1))
 :abs-constraint-intervals '((4500.0 4505.0 1)(4505.0 4511.0 0.5))
 )
(make-obs-task
 :id :foo2
 :name "Foo X-2"
 :exp-time (* 2 86400)
 :priority 47
 :ra 12.34
 :dec -16.112
 :available-time-intervals '((4500.0 4510.0 1))
 :abs-constraint-intervals '((4500.0 4505.0 1)(4505.0 4511.0 0.5))
 )
(make-obs-task
 :id :foo3
 :name "Foo X-3"
 :exp-time (* 4 86400)
 :priority 48
 :ra 12.34
 :dec -16.112
 :available-time-intervals '((4500.0 4510.0 1))
 :abs-constraint-intervals '((4500.0 4505.0 1)(4505.0 4511.0 0.5))
 )
(setf foo1 (lookup-task (obs-chronika -occ) :foo1))
(setf foo2 (lookup-task (obs-chronika -occ) :foo2))
(task-angular-separation foo1 foo2)
(precedence-constraint (obs-rel-constraint -occ)
                       foo1 foo2 :offset 2)
(exposure-duration-at-time foo1 2)
(exposure-duration-at-time foo2 2)

(describe foo1)
(describe foo2)
(print (csp-initargs -occ))

(make-csp-for-obs-csp-chronika -occ)
(describe (obs-csp -occ))
(describe (first (capacity-constraints (obs-csp -occ))))
(obs-cap-constraint -occ)
(setf v0 (var-named (obs-csp -occ) :foo1))
(setf v1 (var-named (obs-csp -occ) :foo2))
(setf v2 (var-named (obs-csp -occ) :foo3))
(describe v0)
(describe v1)
(describe v2)
(var-overlap-effect (obs-csp -occ) v0 5)
(var-overlap-effect (obs-csp -occ) v1 5)
(var-effect (obs-csp -occ) v1 3)
(show-csp-state (obs-csp -occ) :preferences t)
(assign-value v0 5)
(assigned-time v0)
(unassign-value v0)
(assign-value v1 8)
(assign-value v1 4)
(assigned-time v1)
(assign-value v2 6)
(reset-csp (obs-csp -occ))
(task-report v1)
(schedule-report (obs-csp -occ))
(elminc-schedule -occ)

;;; ******** telnet TEST THAT HAVING PARENT OBS-CSP and CAP-CSP works
(print (why-conflicts (obs-csp -occ) v1 6))

|#

#|
EXAMPLE OF MULTIPLE CAPACITY CONSTRAINTS:

;;; create new class, e.g.:

(defclass TEST-CAP (CAPACITY-CONSTRAINT
                    GENERAL-TIME-TO-SEGMENT-MAPPING)
  ()
  (:documentation "test"))


;;; define the init method. args will be passed from make-capacity-constraint.
;;; here there are two args:  chronika and a segment list if the form
;;; of ((t1 t2)(t3 t4)...) where times are TJD.
;;; capacity is set to 1.0, but could be argument also

(defmethod INIT-CAPACITY-CONSTRAINT ((cap test-cap) (csp csp) args)
  (let ((chr (getf args :chronika))
        (segs (getf args :segments))
        (transformed-segs nil))
    
    ;; convert segments to qtime
    (dolist (seg segs)
      (push (list (TJD-to-qtime chr (first seg))
                  (TJD-to-qtime chr (second seg)))
            transformed-segs))
    (setf transformed-segs (nreverse transformed-segs))
    
    (init-time-to-segment-mapping cap :segments transformed-segs)

    ;; set up limit array
    (let* ((count (total-number-of-segments cap))
           (lim (make-array count))
           (used (make-array count :initial-element 0)))
      ;; fill limit array
      (dotimes (i count)
        (setf (aref lim i)
              ;; here, for example, set the bin capacity to 1.0
              ;; could be changed by passing in additional args to make-cap...
              1.0))
      ;; store in slots
      (setf (slot-value cap 'bin-limit) lim)
      (setf (slot-value cap 'bin-usage) used)
      (setf (slot-value cap 'bin-count) count)
      )
    cap))

;;; define basic methods (note bin-to-values not cached here; this could
;;; be done in the cap constraint instance)

(defmethod VALUE-TO-BIN 
  ((c obs-csp) (capcon test-cap)  (v obs-var) value)
  
  ;; map value to time, then time to segment
  (let ((fdsia (fdsia (task v)))
        (bin nil))
    (multiple-value-bind
      (start-time end-time)
      (fdsia-val-to-time fdsia value)

      (when (and start-time end-time)
        (let ((seg1 (time-to-segment capcon start-time))
              (seg2 (time-to-segment capcon end-time)))
          ;; if both start and end within segment, then value maps to bin
          ;; this could be different for other types of capacities.
          (if (and seg1 seg2 (= seg1 seg2))
            (setf bin seg1)))))
    bin))
 

(defmethod BIN-TO-VALUES 
  ((c obs-csp) (capcon test-cap) (v obs-var) bin)
  
  ;; get start/end times of bin, then convert to values
  
  (let* ((start (start-of-segment capcon bin))
         (end (end-of-segment capcon bin))
         (FDSIA (FDSIA (task v)))
         (val-range (FDSIA-time-range-to-val-range 
                     FDSIA (list start end)))
         (result nil))
    (when val-range
      (loop-over-value-range
       i (first val-range) (second val-range)
       (let ((test-bin (value-to-bin c capcon v i)))
         (if (and test-bin (= test-bin bin))
           (push i result)))))
    result))

;;; constant required capacity here, could be different

(defmethod REQUIRED-CAPACITY 
  ((c obs-csp) (capcon test-cap) 
   (v obs-var) bin value)
  (declare (ignore bin))
  0.6)


(defmethod MAX-REQUIRED-CAPACITY 
  ((c obs-csp) (capcon test-cap) 
   (v obs-var))
  0.6)

;;; create new instance:

(make-capacity-constraint
 (obs-csp -occ)
 ;; --- standard args
 :capcon-type 'test-cap
 :name 'test-cap-1
 :limit-array nil
 :remove-values-if-capacity-exceeded t
 :remove-values-if-not-in-any-bin t
 :weight 1
 ;; --- additional args for INIT-...
 :chronika (obs-chronika -occ)
 :segments '((4501.0 4504.0)(4504.0 4506.0)(4506.0 4520.0))
 )

|#

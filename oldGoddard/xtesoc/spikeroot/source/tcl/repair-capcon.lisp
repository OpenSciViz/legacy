;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; repair-capcon.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: repair-capcon.lisp,v $
;;; Revision 3.1  1994/11/28  22:54:27  buehler
;;; Incorporates telemetry constraint additions by mark johnston.
;;;
;;; Revision 1.14  1992/10/04  13:42:59  johnston
;;; fixed erroneous (declare (ignore ...)) in value-to-bin method
;;;
;;; Revision 1.13  1992/10/04  13:10:50  johnston
;;; added function to remove capacity constraints from CSP
;;; see REMOVE-CAPACITY-CONSTRAINT
;;;
;;; Revision 1.12  1992/04/22  16:45:27  johnston
;;; added multi-bin capacity constraint (where one value assignments
;;; can affect more than one bin)
;;;
;;; Revision 1.11  1991/12/01  00:25:49  johnston
;;; clean up compiler warnings
;;;
;;; Revision 1.10  1991/11/26  15:43:56  johnston
;;; rework methods for required/consumed capacity constraints
;;; see MDJ memo 25Nov91
;;;
;;; Revision 1.9  1991/10/03  14:25:16  krueger
;;; STIF3 - divorce introduced errors by changing a slot "number" to
;;; "variable-number".  Updated code to call an abstractor function "var-number"
;;;
;;; Revision 1.8  1991/06/05  22:51:46  johnston
;;; significant changes:
;;; - changed required/consumed capacity functions to take both bin and value
;;;   as arguments
;;; - added capability to use max-capacity function to reduce checking
;;;   after assignments/unassignments
;;;
;;; Revision 1.7  91/04/29  11:35:42  sponsler
;;; Method SHOW-CSP-STATE :AFTER demon - &key added to argument list
;;; 
;;; Revision 1.6  91/03/27  08:46:52  krueger
;;; Updated Make-Capacity-Constraint to call an initialization method.
;;; Added a default initialization method, Init-Capacity-Constraint, which
;;; is called when a capacity constraint is made.
;;; 
;;; Revision 1.5  91/03/25  13:16:52  johnston
;;; removed defdoc
;;; 
;;; Revision 1.4  91/02/19  12:47:55  miller
;;; rewrote bin-pack example and moved to repair-examples
;;; 
;;; Revision 1.3  91/02/18  15:16:39  miller
;;; modified show-constraint to print amount of unconsumed capacity as well as
;;;  limit and what's been consumed
;;; 
;;; Revision 1.2  91/01/24  13:44:04  miller
;;; changed so that resource conflicts on ignored vars are kept up to date
;;; 
;;; Revision 1.1  90/11/28  11:05:27  johnston
;;; Initial revision
;;; 
;;; Revision 1.7  90/11/26  01:28:10  johnston
;;; minor changes, documentation
;;; 
;;; Revision 1.6  90/11/24  11:25:12  johnston
;;; added why conflicts code
;;; 
;;; Revision 1.4  90/10/29  10:37:50  johnston
;;; fixes after testing
;;; 
;;; Revision 1.3  90/10/25  17:55:43  johnston
;;; development
;;; 
;;; Revision 1.2  90/10/25  09:51:43  johnston
;;; development
;;; 
;;; Revision 1.1  90/10/25  09:39:56  johnston
;;; Initial revision
;;; 
;;; creation 24 Oct 90 MDJ
;;;


;;; -----------------------------------------------------------------------
;;; REPAIR-CAPCON.LISP:  extensions to repair.lisp for capacity constraints
;;; -----------------------------------------------------------------------



;;; -----------------------------------------------------------------------
;;; CLASS:  CAP-CSP
;;;
;;; CSPs with capacity constraints
;;; -----------------------------------------------------------------------

(defclass CAP-CSP (CSP)
  ((capacity-constraints
    :initform nil :accessor capacity-constraints
    :documentation 
    "list of capacity constraint instances")))

;;; -----------------------------------------------------------------------
;;; method variations for CSPs with capacity constraints
;;; -----------------------------------------------------------------------

(defmethod RESET-CSP :BEFORE ((c CAP-CSP))
  (dolist (cap-con (capacity-constraints c))
    (reset-constraint cap-con)))

(defmethod RESET-CSP :AFTER ((c CAP-CSP))
  nil)

(defmethod INIT-BEFORE-LOCKING-VARS ((c CAP-CSP))
  ;; when called, it is assumed that all assignments have been unmade
  (dolist (cap-con (capacity-constraints c))
    (init-capacity-constraint-conflicts cap-con)
    (init-capacity-constraint-domains cap-con)))

(defmethod SHOW-CSP-STATE :AFTER ((c CAP-CSP) &key)
  (let ((*print-array* t))
    (dolist (cap-con (capacity-constraints c))
      (show-constraint cap-con t))))

(defmethod CHECK-CSP-STATE :AFTER ((c CAP-CSP))
  (dolist (cap-con (capacity-constraints c))
    (check-capacity-constraint cap-con)))

(defmethod WHY-CONFLICTS ((csp cap-csp) var value)

  "Extension to why-conflicts for capacity constraints"

  (let ((list-from-cache (call-next-method csp var value)))
    (dolist (cap-con (capacity-constraints csp))
      (let ((conflicts (conflicting-assignments cap-con var value))
            (name cap-con))
        (dolist (conflict conflicts)
          (push (list conflict (assigned-value conflict) name)
                list-from-cache))))
    list-from-cache))

;; Note: can't use caching for capacity constraints since conflicts depend
;; on more than one var/value assignment (in general)

(defmethod CSP-VAR-VALUE-ASSIGN :AFTER ((c CAP-CSP) (v var) value)
  "var v has its assignment made to value: update state"
  (dolist (cap-con (capacity-constraints c))
    (capacity-constraint-var-value-assign cap-con v value)))

(defmethod CSP-VAR-VALUE-UNASSIGN :AFTER ((c CAP-CSP) (v var) value)
  "var v has its assignment unmade from value: update state"
  (dolist (cap-con (capacity-constraints c))
    (capacity-constraint-var-value-unassign cap-con v value)))

(defmethod ASSIGNMENTS-WOULD-CONFLICT-P ((c CAP-CSP) (v1 var) value1 (v2 var))
  "return t if v2 has an assigned value that would conflict with assigning
    val1 to v1, nil otherwise
    NOT IMPLEMENTED FOR CAP-CSP"
  (declare (ignore value1))
  (error "not implemented"))


;;; -----------------------------------------------------------------------
;;; Additional methods for CSPs with capacity constraints
;;; -----------------------------------------------------------------------


(defgeneric FIND-CAPACITY-CONSTRAINT (cap name)
  (:documentation  
   "Find capacity constraint instance by name.  Return instance if found,
    nil if not"))

(defmethod FIND-CAPACITY-CONSTRAINT ((c CSP) name)
  ;; default method for CSPs: return nil
  (declare (ignore name))
  nil)

(defmethod FIND-CAPACITY-CONSTRAINT ((c CAP-CSP) name)
  (dolist (capcon (capacity-constraints c))
    (if (equal name (name capcon))
      (return capcon))))


;;; -----------------------------------------------------------------------
;;; CLASS:  CAPACITY-CONSTRAINTS
;;; -----------------------------------------------------------------------

(defclass CAPACITY-CONSTRAINT ()
  ((csp
    :initform nil :accessor csp :initarg :csp
    :documentation 
    "csp instance to which this constraint attached")
   (name
    :initform "ANON-CAP" :accessor name :initarg :name
    :documentation "print name of this constraint")

   (bin-count
    :initform nil :accessor bin-count :initarg :bin-count
    :documentation 
    "number of bins")
   (bin-limit
    :initform nil :accessor bin-limit :initarg :bin-limit
    :documentation 
    "array length bin-count of limits on bin usage")
   (bin-usage
    :initform nil :accessor bin-usage :initarg :bin-usage
    :documentation 
    "array length bin-count of current bin usage")

   (remove-values-from-domain-if-exceed-capacity-p
    :initform t :accessor remove-values-from-domain-if-exceed-capacity-p
    :initarg :remove-values-from-domain-if-exceed-capacity-p
    :documentation
    "what to do on initialization when values require more capacity than 
      is available in a bin:  
      If t, then values are removed from the domain temporarily
      If :permanent then values are removed from domain permanently
      If nil, the conflict count is increased")

   (remove-values-from-domain-if-not-in-any-bin-p
    :initform nil :accessor remove-values-from-domain-if-not-in-any-bin-p
    :initarg :remove-values-from-domain-if-not-in-any-bin-p
    :documentation
    "what to do on initialization with values that do not belong to any
      bin:  choices are:
      nil - no action
      t - values are removed from domain temporarily
      :permanent - values are removed from domain permanently")

   (cap-constraint-weight
    :initform 1 :accessor cap-constraint-weight :initarg :cap-constraint-weight
    :documentation
    "weight of constraint for conflict count changes: default 1")

   ))

(defclass GENERAL-CAPACITY-CONSTRAINT (CAPACITY-CONSTRAINT)
  ()
  (:documentation
   "Generalization of capacity constraints to handle multiple bins for values
     as well as multiple values for bins"))


;;; -----------------------------------------------------------------------
;;; CONSTRUCTOR FOR CAPACITY CONSTRAINTS
;;; 
;;; Notes:
;;; 
;;; - error checking:  only attach constraint if no vars assigned
;;; 
;;; - upon attachment, or upon reset, values that require more capacity than
;;; available in bin have conflict count incremented, or have values removed
;;; from domain (controlled by flag in constraint instance)
;;; 
;;; - can change bin limit while assignments are made
;;; 
;;; - can't change functions that return required or consumed capacities
;;; on the fly and have the conflicts remain consistent.  To do this, must
;;; reset-csp after change.
;;; -----------------------------------------------------------------------


(defun MAKE-CAPACITY-CONSTRAINT 
       (CSP
        &rest args
        &key
        (capcon-type 'capacity-constraint)
        (name "Anon")
        limit-array
        (remove-values-if-capacity-exceeded t)
        (remove-values-if-not-in-any-bin nil)
        (weight 1)
        &allow-other-keys)
  
  "Create and attach a capacity constraint.  
    CSP must be a CSP of type cap-csp with no current assignments.
    limit-array must be an array of length bin-count of bin limiting quantities.
    If limit-array is nil, then it must be set by INIT-CAPACITY-CONSTRAINT
     along with the bin-count and bin-usage slots.
    All others default as follows:
     capcon-type = capacity-constraint
     name = Anon
     remove-values-if-capacity-exceeded defaults to t
     remove-values-if-not-in-any-bin defaults to nil"
  
  ;; error check
  (unless (and (typep csp 'cap-csp)
               (= 0 (assigned-var-count csp)))
    (error "csp ~a must be of type CAP-CSP and have no assignments" csp))
  
  (let* ((number-of-bins (if limit-array (array-dimension limit-array 0) nil))
         instance)
    (setf instance
          (make-instance capcon-type
                         :csp csp
                         :name name
                         :bin-count number-of-bins
                         :bin-limit limit-array
                         :bin-usage (if limit-array
                                      (make-array 
                                       number-of-bins :initial-element 0)
                                      nil)
                         :remove-values-from-domain-if-exceed-capacity-p
                         remove-values-if-capacity-exceeded
                         :remove-values-from-domain-if-not-in-any-bin-p
                         remove-values-if-not-in-any-bin
                         :cap-constraint-weight weight
                         ))
    
    ;; initialize the capacity constraint if necessary
    (init-capacity-constraint instance csp args)
    
    ;; check that we have a bin-count, bin-limit, and bin-usage arrays
    (unless (and (bin-count instance)
                 (numberp (bin-count instance))
                 (bin-limit instance)
                 (bin-usage instance))
      (error "error in capacity constraint initialization"))

    ;; attach to csp
    (setf (capacity-constraints csp)
          (cons instance (capacity-constraints csp)))
    
    ;; remove domain values for variables whose resource usage exceeds
    ;;  any bin limits on this constaint
    (init-capacity-constraint-conflicts instance)
    (init-capacity-constraint-domains instance)

    ;; return instance
    instance))


(defmethod INIT-CAPACITY-CONSTRAINT ((capcon CAPACITY-CONSTRAINT) csp args)
  "Default method for initializing a capacity constraint.  Currently
     it does nothing for base class.  Args is the keyword/value list 
     given to the constructor make-capacity-constraint."
  (declare (ignore csp args))
  nil)


;;; ----------------------------------------------------------------------
;;; REMOVE CAPACITY CONSTRAINTS

;;; WARNING! if the capacity constraint has the remove values flags set to
;;; :permanent, then this routine will not leave the csp in a state identical
;;; one wherein the capacity constraint was never defined. 

(defmethod REMOVE-CAPACITY-CONSTRAINT ((c CAP-CSP) (cap-con CAPACITY-CONSTRAINT))
  "remove a capacity constraint.  Returned value is list of assignments
    in effect (as from get-assignments; could be input to put-assignments).
    Assignments are all unmade."
  (let ((assignments (get-assignments c)))
    (with-slots
      (capacity-constraints) c
      
      (setf capacity-constraints (remove cap-con capacity-constraints))
      (reset-csp c)

      assignments)))

;;; -----------------------------------------------------------------------
;;; GENERIC FUNCTIONS FOR CAPACITY REQUIREMENTS, VALUE-TO-BIN MAPPING

;;; --- capacity required/consumed by variables when assigned

(defgeneric REQUIRED-CAPACITY (csp cap-con var bin value)
  (:documentation
   "function called with 5 args:  csp, cap constraint, var, bin, and value.  
      Returns capacity required in order to make assignment without conflicts.
      May return nil to mean no capacity required"))

(defgeneric MAX-REQUIRED-CAPACITY (csp cap-con var)
  (:documentation
   "function called with 3 args:  csp, cap constraint, and var.  Returns 
      max capacity required in order to make assignment without conflicts
      anywhere in domain.  This function can return nil to indicate NO
      capacity required anywhere, or t to indicate that the maximum is
      not known.  This is only used to improve efficiency so t is OK as
      default."))

(defgeneric CONSUMED-CAPACITY (csp cap-con var bin value)
  (:documentation 
   "function called with 5 args:  csp, cap constraint, var, bin, and value.  
      Returns capacity consumed when var assigned to bin number.  Usually this
      is the same as required-capacity.  In all cases it must be less than or
      equal to required capacity.  May return nil to indicate no capacity
      required"))
   
;;; --- mapping bins to/from values:  in this version, the mapping from values
;;; --- to bins is many to one (or one to one)

(defgeneric VALUE-TO-BIN (csp cap-con var value)
  (:documentation
   "function which takes as args the csp, the capacity constraint instance,
      a var, and a value: returns bin number corresponding to value.
      Default is value number = bin number.  A value is not required to belong
      to a bin, i.e. this function can return nil."))

;;; function applicable to general-capacity-constraints only
(defgeneric VALUE-TO-BINS (csp cap-con var value)
  (:documentation
   "function which takes as args the csp, the capacity constraint instance,
      a var, and a value: returns list of bin numbers corresponding to value.
      Default is value number = bin number.  A value is not required to belong
      to a bin, i.e. this function can return nil."))


(defgeneric BIN-TO-VALUES (csp cap-con var bin)
  (:documentation
    "function which takes as args the csp, the capacity constraint instance,
      a var, and a bin number: returns list of values corresponding to
      bin.  Default is value number = bin number.  May return nil, i.e. a bin
      does not correspond to any values for a var."))


;;; -----------------------------------------------------------------------
;;; DEFAULT METHODS FOR CAPACITY REQUIREMENTS, VALUE-TO-BIN MAPPING
;;; -----------------------------------------------------------------------


(defmethod REQUIRED-CAPACITY ((c CSP) (capcon CAPACITY-CONSTRAINT) (v VAR)
                              bin val)
  (declare (ignore bin val))
  ;; default is 0
  0)

(defmethod MAX-REQUIRED-CAPACITY ((c CSP) (capcon CAPACITY-CONSTRAINT) (v VAR))
  ;; default is t, i.e. max is unknown
  t)

(defmethod CONSUMED-CAPACITY ((c CSP) (capcon CAPACITY-CONSTRAINT) (v VAR)
                              bin val)
  ;; default is same as required capacity
  (required-capacity c capcon v bin val))


;;; --- mapping bins to/from values:  for class CAPACITY-CONSTRAINT, 
;;; --- the mapping from values to bins is many to one (or one to one)
;;; --- default method for value-to-bin conversion: value = bin

(defmethod VALUE-TO-BIN ((c CSP) (capcon CAPACITY-CONSTRAINT) (v VAR) val)
  val)
           
(defmethod BIN-TO-VALUES ((c CSP) (capcon CAPACITY-CONSTRAINT) (v VAR) bin)
  (list bin))


;;; --- for GENERAL-CAPACITY-CONSTRAINTS the mapping from value to bin is
;;; --- possibly one to many

(defmethod VALUE-TO-BIN 
  ((c CSP) (capcon GENERAL-CAPACITY-CONSTRAINT) (v VAR) val)

  (declare (ignore val))

  (error "value-to-bin not defined for GENERAL-CAPACITY-CONSTRAINT"))


(defmethod VALUE-TO-BINS 
  ((c CSP) (capcon GENERAL-CAPACITY-CONSTRAINT) (v VAR) val)

  (list val))

;;; -----------------------------------------------------------------------
;;; METHODS FOR CLASS:  CAPACITY-CONSTRAINT
;;; -----------------------------------------------------------------------

(defmethod PRINT-OBJECT ((capcon capacity-constraint) stream)
  (format stream "<Capacity constraint ~a>" (name capcon)))

(defmethod RESET-CONSTRAINT ((capcon capacity-constraint))
  "Reset bin usage.  Note that if there are locked vars, init
    conflicts must be called to set current bin usage."
  (fill (bin-usage capcon) 0)
  )

(defmethod SHOW-CONSTRAINT ((capcon capacity-constraint) stream)
  "Print capacity constraint description to stream"
  (format stream "~%Capacity constraint ~a" (name capcon))
  (format stream "~%Limit: ~a" (bin-limit capcon))
  (format stream "~%Usage: ~a" (bin-usage capcon))
  (format stream "~%Left:  ~a"  
          (map 'array #'- (bin-limit capcon) (bin-usage capcon))))

#| test
(setf *print-array* t)
(setf c (make-csp :name "captest" 
                  :var-init-hook #'(lambda (var) (list 4 nil))
                  :csp-type 'cap-csp))
(setf v0 (make-var :csp c :number 0))

;; prefs testing
(set-value-preference v0 3 5)
(set-value-preference v0 2 4)
(set-value-preference v0 1 3)
(set-value-preference v0 0 2)
(set-value-preferences-by-generator v0 #'(lambda (var val) (+ 10 val)))
(remove-low-preference-values-from-domain v0 :min-preference-in-domain 9)
(remove-low-preference-values-from-domain v0 :max-difference-from-max-preference 5)
(restore-all-values-to-domain v0)
(conflicts v0)
(prefs v0)
(all-min-conflicts-values-ordered-by-preference v0)
(all-values-ordered-by-min-conflicts-and-max-preference v0)
(all-values-preference-order v0)
(sort-values-by-preference v0 '(1 3 2 0))
(max-preference-values-in-value-list v0 '(2 3))
(assign-min-conflicts-max-preference-value-at-random v0)
(all-values-preference-order v0)
(value-preference v0 3)

(setf v1 (make-var :csp c :number 1))
(setf v2 (make-var :csp c :number 2))
(describe c)
(describe v0)


(defclass test-cap-csp (cap-csp)())
(defclass test-cap-var (var)())
(defclass test-cap-con (capacity-constraint)())

(defmethod required-capacity ((csp test-cap-csp) 
                              (capcon test-cap-con)
                              (var test-cap-var) bin val)
  (declare (ignore csp capcon bin))
  ;; var2 in bin2 requires 10
  (cond ((and (= (var-number var) 2)
              (= val 2))
         10)
        (t 1)))

(defmethod max-required-capacity ((csp test-cap-csp) 
                                  (capcon test-cap-con)
                                  (var test-cap-var))
  (cond ((= (var-number var) 2) 10)
        (t 1)))

;; two values in one bin
(defmethod value-to-bin ((csp test-cap-csp) 
                         (capcon test-cap-con)
                         (var test-cap-var) value)
  (let ((calc (floor value 2)))
    (if (and (>= calc 0) (<= calc 1)) calc nil)))

(defmethod bin-to-values ((csp test-cap-csp) 
                          (capcon test-cap-con)
                          (var test-cap-var) bin)
  (if (and (>= bin 0) (<= bin 1))
    (list (* 2 bin) (+ (* 2 bin) 1))
    nil))

(setf c (make-csp :name "captest" 
                  :var-init-hook #'(lambda (var) 
                                     (declare (ignore var))(list 5 nil))
                  :csp-type 'test-cap-csp))
(setf cc (make-capacity-constraint 
          c :name "test"
          :limit-array   (make-array 2 :initial-element 2)
          :remove-values-if-not-in-any-bin t
          :capcon-type 'test-cap-con))
(setf v0 (make-var :csp c :number 0 :var-type 'test-cap-var))
(setf v1 (make-var :csp c :number 1 :var-type 'test-cap-var))
(setf v2 (make-var :csp c :number 2 :var-type 'test-cap-var))
(bin-to-values c cc v0 0)
(value-to-bin c cc v0 4)
(required-capacity c cc v2 2 0)

(describe cc)
(find-capacity-constraint c "test")
(show-csp-state c)
(reset-csp c)
(check-csp-state c)
(reset-and-check c)

(assign-value v0 0)
(assign-value v1 0)
(unassign-value v0)
(lock-var v0 0)
(unlock-var v0)
(restore-value-to-domain v2 2)
(remove-value-from-domain v2 2 :permanent t)
(lock-var v2 2 :restore-value t)
(lock-var v2 1 :restore-value t)
(assign-value v2 2)
(unassign-value v2)
(unlock-var v2)
(assign-value v1 2)
(why-conflicts c v1 2)
(change-bin-limit cc 0 5)
(change-bin-limit cc 0 2)
(change-bin-limit cc 0 0)
(change-bin-limit cc 1 1)
(change-bin-limit cc 1 10)
(assign-value v2 0)
(unassign-value v0)
(unassign-value v1)
(unassign-value v2)
|#

(defmethod VARS-ASSIGNED-TO-BIN ((capcon CAPACITY-CONSTRAINT) bin)
  
  "Return list of vars assigned to specified bin.  Includes both
    locked and unignored vars."
  
  (let* ((csp (csp capcon))
         (result nil))
    (do-for-all-vars
     :var v :csp csp
     :form
     (when (has-assigned-value-p v)
       (let ((value-assigned-to-bin
              (value-to-bin csp capcon v (assigned-value v))))
         (if (and value-assigned-to-bin
                  (= value-assigned-to-bin bin))
           (push v result)))))
    result))

(defmethod CONFLICTING-ASSIGNMENTS ((capcon capacity-constraint) (v var) value)

  "Return list of vars whose current assignments conflict with the assignment
    of var to value, due to this capacity constraint only"

  ;; which bin does value go into?
  (let* ((csp (csp capcon))
         (bin-to-check (value-to-bin csp capcon v value))   ; could be nil
         (assignment (assigned-value v))
         (assigned-bin (if assignment (value-to-bin csp capcon v assignment)))
         (assignment-in-bin-to-check
          (if (and bin-to-check assigned-bin) (= bin-to-check assigned-bin)))
         (required-in-bin 
          (if bin-to-check (required-capacity csp capcon v bin-to-check value)))
         (limit (if bin-to-check (aref (bin-limit capcon) bin-to-check)))
         (usage (if bin-to-check (aref (bin-usage capcon) bin-to-check)))
         )

    (cond
     
     ;; value not in any bin: no constraint
     ((null bin-to-check) nil)
     
     ;; no resources required: no constraint
     ((or (not required-in-bin)
          (= 0 required-in-bin))
      nil)
     
     ;; v assigned to different bin, or not assigned to any bin
     ((or (not assignment)
          (not assignment-in-bin-to-check))
      ;; no conflict as long as limit-usage >= required, else list of
      ;; all vars assigned to bin
      (if (>= (- limit usage) required-in-bin)
        nil ; no conflicts
        (vars-assigned-to-bin capcon bin-to-check))
      )
     
     ;; v assigned to bin to check
     (assignment-in-bin-to-check
      ;; if usage > limit, return list of all vars assigned to this bin
      ;; (including v itself)
      (if (> usage limit)
        (vars-assigned-to-bin capcon bin-to-check))
      )
     
     ;; unrecognized case
     (t (error "unrecognized case!")))))


(defmethod INIT-CAPACITY-CONSTRAINT-CONFLICTS ((capcon capacity-constraint))
  
  "Initialize conflict counts for a capacity constraint.  
    Looks at all vars (unignored, ignored, and locked).  
    Conflict counts are always updated.  Depending on flag 
    remove-values-from-domain-if-exceed-capacity-p, values are also removed
    from domain.
    THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
     (i.e. before locked vars are assigned)."
  
  ;; if there are locked vars, they do not have their assigned values removed
  ;; from the domain (this would undo the assignment); they only have their
  ;; conflict counts incremented.
  
  (let ((csp (csp capcon))
        (remove-from-domain 
         (remove-values-from-domain-if-exceed-capacity-p capcon))
        (weight (cap-constraint-weight capcon)))

    (do-for-all-vars 
     :csp csp :var v 
     :form
     ;; do for each bin
     (dotimes (i (bin-count capcon))
       (let ((limit (aref (bin-limit capcon) i)))
         ;; do for all values in bin
         (dolist (val (bin-to-values csp capcon v i))
           ;; if required capacity > limit, set conflict
           (let ((req-cap (required-capacity csp capcon v i val)))
             
             ;; remove from domain if specified
             (when (and req-cap (> req-cap limit))
               (if remove-from-domain
                 (remove-value-from-domain 
                  v val 
                  :permanent (if (eq remove-from-domain :permanent) t nil)))
               ;; always update conflicts (so that if domain value is 
               ;; restored, the conflict count will be right)
               (add-to-conflicts-on-value v val weight)))))))))
           

(defmethod INIT-CAPACITY-CONSTRAINT-DOMAINS ((capcon capacity-constraint))
  
  "Initialize conflict counts for a capacity constraint.  
    Looks at all vars (unignored, ignored, and locked).  
    Conflict counts are always updated.  Depending on flag 
    remove-values-from-domain-if-exceed-capacity-p, values are also removed
    from domain.
    THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
     (i.e. before locked vars are assigned)."
  
  ;; if there are locked vars, they do not have their assigned values removed
  ;; from the domain (this would undo the assignment); they only have their
  ;; conflict counts incremented.
  
  (let ((csp (csp capcon))
        (remove-from-domain 
         (remove-values-from-domain-if-not-in-any-bin-p capcon)))
    (when remove-from-domain
      (do-for-all-vars 
       :csp csp :var v 
       :form
       ;; do for each value
       (dotimes (val (value-count v))
         (when (not (value-to-bin csp capcon v val))
           ;; val not in any bin
           (remove-value-from-domain 
            v val 
            :permanent (if (eq remove-from-domain :permanent) t nil))))))))


(defmethod CHECK-CAPACITY-CONSTRAINT ((capcon capacity-constraint))
  
  "Check that usage matches assignments.  Print any discrepancies to standard
    output.  Debugging only."
  
  (let ((csp (csp capcon)))
    ;; do for each bin
    (dotimes (i (bin-count capcon))
      (let ((usage (aref (bin-usage capcon) i))
            (usage-check 0))
        (do-for-all-vars
         :csp csp :var v 
         :form
         (let* ((assignment (assignment v))
                (assignment-bin 
                 (if assignment 
                   (value-to-bin csp capcon v assignment)))
                (var-usage-in-bin
                 (if (and assignment-bin (= assignment-bin i))
                   (consumed-capacity csp capcon v assignment-bin assignment))))
           (if var-usage-in-bin
             (incf usage-check var-usage-in-bin))))
        ;; check that match
        (if (/= usage usage-check)
          (format t "~%~a: bin-usage ~3d is ~a, but current sum is ~a"
                  capcon i usage usage-check))))))


(defmethod CHANGE-BIN-LIMIT ((capcon capacity-constraint) bin new-limit)

  "Change usage limit on specified bin to new value. Update conflict
    counts on all variables.  new-limit can either be more or
    less than old limit.  Note:  this only checks values currently in 
    domain, and does not remove values from domain if they exceed capacity.
    To do this, change bin limits, then reset CSP, making sure that flag
     remove-values-from-domain-if-exceed-capacity-p is t"

  (let* ((csp (csp capcon))
         (limit-array (bin-limit capcon))
         (old-limit (aref limit-array bin))
         (usage (aref (bin-usage capcon) bin))
         (weight (cap-constraint-weight capcon))
         )
    ;; check each var
    (do-for-all-vars
     :csp csp :var v 
     :form
     (let* ((assignment (assignment v))
            (assignment-bin 
             (if assignment (value-to-bin csp capcon v assignment)))
            (var-usage-in-bin
             (if (and assignment-bin (= assignment-bin bin))
               (consumed-capacity csp capcon v assignment-bin assignment))))
       ;; check each value in bin
       (dolist (val (bin-to-values csp capcon v bin))
         (let ((require-cap (required-capacity csp capcon v bin val))
               limit-comparison 
               would-fit-before will-fit-now)
           (when (and require-cap (> require-cap 0))
             ;; compute comparison against limit: if <= limit then no conflict
             (setf limit-comparison (+ (- usage (or var-usage-in-bin 0))
                                       require-cap))
             (setf would-fit-before (<= limit-comparison old-limit))
             (setf will-fit-now (<= limit-comparison new-limit))
             ;; update conflict counts if needed
             (if (and would-fit-before (not will-fit-now))
               (add-to-conflicts-on-value v val weight))
             (if (and (not would-fit-before) will-fit-now)
               (subtract-from-conflicts-on-value v val weight)))))))

    ;; now update amount in limit array
    (setf (aref limit-array bin) new-limit)))

;;; -----------------------------------------------------------------------
;;; METHODS FOR CLASS:  GENERAL-CAPACITY-CONSTRAINT
;;; -----------------------------------------------------------------------

;;; --- Note: the following methods are modified from those of the same
;;; --- name for CAPACITY-CONSTRAINTS.  This is to prevent code breakage.
;;; --- at some point the CAPACITY-CONSTRAINT class could be retired, since
;;; --- all the functionality is provided in GENERAL-CAPACITY-CONSTRAINT class.

(defmethod VARS-ASSIGNED-TO-BIN ((capcon GENERAL-CAPACITY-CONSTRAINT) bin)
  
  "Return list of vars assigned to specified bin.  Includes both
    locked and unignored vars."
  
  (let* ((csp (csp capcon))
         (result nil))
    (do-for-all-vars
     :var v :csp csp
     :form
     (when (has-assigned-value-p v)
       (let ((value-assigned-to-bins
              (value-to-bins csp capcon v (assigned-value v))))
         (if (and value-assigned-to-bins
                  (member bin value-assigned-to-bins))
           (push v result)))))
    result))


;;; --- REVIEW this method for logic...

(defmethod CONFLICTING-ASSIGNMENTS 
  ((capcon GENERAL-CAPACITY-CONSTRAINT) (v var) value)

  "Return list of vars whose current assignments conflict with the assignment
    of var to value, due to this capacity constraint only"

  ;; which bin does value go into?
  (let* ((csp (csp capcon))
         (bins-to-check (value-to-bins csp capcon v value))   ; could be nil
         (assignment (assigned-value v))
         (assigned-bins (if assignment (value-to-bins csp capcon v assignment)))
         (result nil)
         )

    (dolist (bin-to-check bins-to-check)
      (let*
        ((assignment-in-bin-to-check
          (and assigned-bins
               (member bin-to-check assigned-bins)))
         (required-in-bin 
          (required-capacity csp capcon v bin-to-check value))
         (limit (if bin-to-check (aref (bin-limit capcon) bin-to-check)))
         (usage (if bin-to-check (aref (bin-usage capcon) bin-to-check)))
         )
        
        (cond
         
         ;; value not in any bin: no constraint
         ((null bin-to-check) nil)
         
         ;; no resources required: no constraint
         ((or (not required-in-bin)
              (= 0 required-in-bin))
          nil)
         
         ;; v assigned to different bin, or not assigned to any bin
         ((or (not assignment)
              (not assignment-in-bin-to-check))
          ;; no conflict as long as limit-usage >= required, else list of
          ;; all vars assigned to bin
          (unless (>= (- limit usage) required-in-bin)
            (setf result 
                  (append result (vars-assigned-to-bin capcon bin-to-check)))))
         
         ;; v assigned to bin to check
         (assignment-in-bin-to-check
          ;; if usage > limit, return list of all vars assigned to this bin
          ;; (including v itself)
          (when (> usage limit)
            (setf result
                  (append result (vars-assigned-to-bin capcon bin-to-check)))))
         
         ;; unrecognized case
         (t (error "unrecognized case!")))))

    result))


(defmethod INIT-CAPACITY-CONSTRAINT-CONFLICTS 
  ((capcon GENERAL-CAPACITY-CONSTRAINT))
  
  "Initialize conflict counts for a capacity constraint.  
    Looks at all vars (unignored, ignored, and locked).  
    Conflict counts are always updated.  Depending on flag 
    remove-values-from-domain-if-exceed-capacity-p, values are also removed
    from domain.
    THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
     (i.e. before locked vars are assigned)."
  
  ;; if there are locked vars, they do not have their assigned values removed
  ;; from the domain (this would undo the assignment); they only have their
  ;; conflict counts incremented.
  
  (let ((csp (csp capcon))
        (remove-from-domain 
         (remove-values-from-domain-if-exceed-capacity-p capcon))
        (weight (cap-constraint-weight capcon)))

    (do-for-all-vars 
     :csp csp :var v 
     :form
     ;; do for each bin
     (dotimes (i (bin-count capcon))
       (let ((limit (aref (bin-limit capcon) i)))
         ;; do for all values in bin
         (dolist (val (bin-to-values csp capcon v i))
           ;; if required capacity > limit, set conflict
           (let ((req-cap (required-capacity csp capcon v i val)))
             
             ;; remove from domain if specified
             (when (and req-cap (> req-cap limit))
               (if remove-from-domain
                 (remove-value-from-domain 
                  v val 
                  :permanent (if (eq remove-from-domain :permanent) t nil)))
               ;; always update conflicts (so that if domain value is 
               ;; restored, the conflict count will be right)
               (add-to-conflicts-on-value v val weight)))))))))
           


(defmethod INIT-CAPACITY-CONSTRAINT-DOMAINS 
  ((capcon GENERAL-CAPACITY-CONSTRAINT))
  
  "Initialize conflict counts for a capacity constraint.  
    Looks at all vars (unignored, ignored, and locked).  
    Conflict counts are always updated.  Depending on flag 
    remove-values-from-domain-if-exceed-capacity-p, values are also removed
    from domain.
    THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
     (i.e. before locked vars are assigned)."
  
  ;; if there are locked vars, they do not have their assigned values removed
  ;; from the domain (this would undo the assignment); they only have their
  ;; conflict counts incremented.
  
  (let ((csp (csp capcon))
        (remove-from-domain 
         (remove-values-from-domain-if-not-in-any-bin-p capcon)))
    (when remove-from-domain
      (do-for-all-vars 
       :csp csp :var v 
       :form
       ;; do for each value
       (dotimes (val (value-count v))
         (when (not (value-to-bins csp capcon v val))
           ;; val not in any bin
           (remove-value-from-domain 
            v val 
            :permanent (if (eq remove-from-domain :permanent) t nil))))))))


(defmethod CHECK-CAPACITY-CONSTRAINT ((capcon GENERAL-CAPACITY-CONSTRAINT))
  
  "Check that usage matches assignments.  Print any discrepancies to standard
    output.  Debugging only."
  
  (let ((csp (csp capcon)))
    ;; do for each bin
    (dotimes (i (bin-count capcon))
      (let ((usage (aref (bin-usage capcon) i))
            (usage-check 0))
        (do-for-all-vars
         :csp csp :var v 
         :form
         (let* ((assignment (assignment v))
                (assignment-bins 
                 (if assignment 
                   (value-to-bins csp capcon v assignment)))
                (var-usage-in-bin
                 (if (and assignment-bins (member i assignment-bins))
                   (consumed-capacity csp capcon v i assignment))))
           (if var-usage-in-bin
             (incf usage-check var-usage-in-bin))))
        ;; check that match
        (if (/= usage usage-check)
          (format t "~%~a: bin-usage ~3d is ~a, but current sum is ~a"
                  capcon i usage usage-check))))))


(defmethod CHANGE-BIN-LIMIT ((capcon GENERAL-CAPACITY-CONSTRAINT) bin new-limit)

  "Change usage limit on specified bin to new value. Update conflict
    counts on all variables.  new-limit can either be more or
    less than old limit.  Note:  this only checks values currently in 
    domain, and does not remove values from domain if they exceed capacity.
    To do this, change bin limits, then reset CSP, making sure that flag
     remove-values-from-domain-if-exceed-capacity-p is t"

  (let* ((csp (csp capcon))
         (limit-array (bin-limit capcon))
         (old-limit (aref limit-array bin))
         (usage (aref (bin-usage capcon) bin))
         (weight (cap-constraint-weight capcon))
         )
    ;; check each var
    (do-for-all-vars
     :csp csp :var v 
     :form
     (let* ((assignment (assignment v))
            (assignment-bins 
             (if assignment (value-to-bins csp capcon v assignment)))
            (var-usage-in-bin
             (if (and assignment-bins (member bin assignment-bins))
               (consumed-capacity csp capcon v bin assignment))))
       ;; check each value in bin
       (dolist (val (bin-to-values csp capcon v bin))
         (let ((require-cap (required-capacity csp capcon v bin val))
               limit-comparison 
               would-fit-before will-fit-now)
           (when (and require-cap (> require-cap 0))
             ;; compute comparison against limit: if <= limit then no conflict
             (setf limit-comparison (+ (- usage (or var-usage-in-bin 0))
                                       require-cap))
             (setf would-fit-before (<= limit-comparison old-limit))
             (setf will-fit-now (<= limit-comparison new-limit))
             ;; update conflict counts if needed
             (if (and would-fit-before (not will-fit-now))
               (add-to-conflicts-on-value v val weight))
             (if (and (not would-fit-before) will-fit-now)
               (subtract-from-conflicts-on-value v val weight)))))))

    ;; now update amount in limit array
    (setf (aref limit-array bin) new-limit)))


(defmethod CAPACITY-CONSTRAINT-VAR-VALUE-ASSIGN 
  ((capcon GENERAL-CAPACITY-CONSTRAINT) (v var) value)
  "var v has its assignment made to value: update state"
  
  (let* ((csp (csp capcon))
         (bins (value-to-bins csp capcon v value)))
    
    ;; if value belongs to no bin (bins is nil), do nothing
    (dolist (bin bins)
      (let* (
             (varnum (var-number v))
             (usage-array (bin-usage capcon)) ;; usage array
             (old-usage (aref usage-array bin)) ;; old usage in this bin
             (limit (aref (bin-limit capcon) bin)) ;; old limit in bin
             (consumed-cap (consumed-capacity csp capcon v bin value))
             (new-usage (+ old-usage consumed-cap))
             (weight (cap-constraint-weight capcon))
             )
        
        ;; update bin-usage
        (setf (aref usage-array bin) new-usage)
        
        ;; update conflicts
        ;; "vtc" stands for var-to-check in the following
        (do-for-all-vars 
         :var vtc :csp csp
         :form
         (let ((vtc-max-usage (max-required-capacity csp capcon vtc)))
           (cond
            
            ((or (not vtc-max-usage) ;no usage of capacity
                 (and vtc-max-usage  ;possible usage
                      (not (eq t vtc-max-usage)) ;max not unknown
                      (< (+ new-usage vtc-max-usage) limit)))
             ;; no need to check: can't possibly change conflicts
             ;;(format t "~%skipping check for ~a" vtc)
             )
            
            (t ;check each value
             (let* ((vtc-number (var-number vtc))
                    (vtc-assignment (assignment vtc))
                    (vtc-assigned-bins 
                     (if vtc-assignment 
                       (value-to-bins csp capcon vtc vtc-assignment)))
                    (vtc-assigned-to-bin-to-check
                     (if vtc-assigned-bins (member bin vtc-assigned-bins)))
                    (vtc-consumed-capacity
                     (if vtc-assigned-to-bin-to-check
                       (consumed-capacity 
                        csp capcon vtc bin vtc-assignment))))
               
               (dolist (val-to-check (bin-to-values csp capcon vtc bin))
                 (multiple-value-bind 
                   (would-fit-before will-fit-now)
                   (would-fit-before-will-fit-now old-usage new-usage limit
                                                  varnum consumed-cap
                                                  vtc-number
                                                  (required-capacity
                                                   csp capcon vtc bin val-to-check)
                                                  vtc-assigned-to-bin-to-check
                                                  vtc-consumed-capacity)
                   ;;(format t " <~d ~d>" vtc-number val-to-check)
                   (if (and would-fit-before
                            (not will-fit-now))
                     (add-to-conflicts-on-value vtc val-to-check weight)))))))))))))
 

(defmethod CAPACITY-CONSTRAINT-VAR-VALUE-UNASSIGN 
  ((capcon GENERAL-CAPACITY-CONSTRAINT) (v var) value)
  "var v has its assignment unmade from value: update state"
  
  (let* ((csp (csp capcon))
         (bins (value-to-bins csp capcon v value)))

    ;; if value belongs to no bin (bins is nil), do nothing
    (dolist (bin bins)
      (let* (         
             (varnum (var-number v))
             (usage-array (bin-usage capcon)) ;; usage array
             (old-usage (aref usage-array bin)) ;; old usage in this bin
             (limit (aref (bin-limit capcon) bin)) ;; old limit in bin
             (consumed-cap (consumed-capacity  csp capcon v bin value))
             (new-usage (- old-usage consumed-cap))
             (weight (cap-constraint-weight capcon))
             )
        
        ;; update bin-usage
        (setf (aref usage-array bin) new-usage)
        
        ;; update conflicts
        ;; "vtc" stands for var-to-check in the following
        (do-for-all-vars
         :var vtc :csp csp
         :form
         (let ((vtc-max-usage (max-required-capacity csp capcon vtc)))
           (cond
            
            ((or (not vtc-max-usage)
                 (and vtc-max-usage
                      (not (eq t vtc-max-usage))
                      (< (+ old-usage vtc-max-usage) limit)))
             ;; no need to check: can't possibly change conflicts
             ;;(format t "~%skipping check for ~a" vtc)
             )
            
            (t ;check each value
             (let* ((vtc-number (var-number vtc))
                    (vtc-assignment (assignment vtc))
                    (vtc-assigned-bins 
                     (if vtc-assignment (value-to-bins csp capcon vtc vtc-assignment)))
                    (vtc-assigned-to-bin-to-check
                     (if vtc-assigned-bins (member bin vtc-assigned-bins)))
                    (vtc-consumed-capacity
                     (if vtc-assigned-to-bin-to-check
                       (consumed-capacity 
                        csp capcon vtc bin vtc-assignment))))
               
               (dolist (val-to-check (bin-to-values csp capcon vtc bin))
                 (multiple-value-bind 
                   (would-fit-before will-fit-now)
                   (would-fit-before-will-fit-now old-usage new-usage limit
                                                  varnum consumed-cap
                                                  vtc-number
                                                  (required-capacity 
                                                   csp capcon vtc bin val-to-check)
                                                  vtc-assigned-to-bin-to-check
                                                  vtc-consumed-capacity)
                   ;;(format t " <~d ~d>" vtc-number val-to-check)
                   (if (and (not would-fit-before)
                            will-fit-now)
                     (subtract-from-conflicts-on-value vtc val-to-check weight)))))))))))))

#|
;;;
;;; TEST CASE FOR GENERAL-CAPACITY-CONSTRAINTS
;;;

(setf *print-array* t)

(defclass test-cap-csp (cap-csp)())
(defclass test-cap-var (var)())
(defclass test-cap-con (general-capacity-constraint)())

;; required=consumed
(defmethod required-capacity ((csp test-cap-csp) 
                              (capcon test-cap-con)
                              (var test-cap-var) bin val)
  (declare (ignore csp capcon))
  (cond ((and (= (var-number var) 0) (= val 4) (= bin 2)) 2)
        ((and (= (var-number var) 1) (= val 4) (= bin 2)) 2)
        (t 1)))

(defmethod max-required-capacity ((csp test-cap-csp) 
                                  (capcon test-cap-con)
                                  (var test-cap-var))
  (cond ((= (var-number var) 0) 2)
        ((= (var-number var) 1) 2)
        (t 1)))


(defmethod VALUE-TO-BINS ((csp test-cap-csp) 
                          (capcon test-cap-con)
                          (var test-cap-var) value)
  (let ((varnum (var-number var)))
    (cond ((= varnum 0)
           (cond ((= value 0) '(0))
                 ((= value 1) '(0))
                 ((= value 2) '(1))
                 ((= value 3) '(1))
                 ((= value 4) '(2))
                 (t nil)))
          ((= varnum 1)
           (case value
             (0 '(0))
             (1 '(0))
             (2 '(0 1))
             (3 '(0 1))
             (4 '(0 1 2))
             (t nil)))
          ((= varnum 2)
           (case value
             (0 '(0))
             (1 '(0))
             (2 '(0 1))
             (3 '(1))
             (4 '(1))
             (t nil)))
          (t (error "no var")))))
         

(defmethod bin-to-values ((csp test-cap-csp) 
                          (capcon test-cap-con)
                          (var test-cap-var) bin)
  (let ((varnum (var-number var)))
    (cond ((= varnum 0)
           (case bin (0 '(0 1)) (1 '(2 3)) (2 '(4))))
          ((= varnum 1)
           (case bin (0 '(0 1 2 3 4)) (1 '(2 3 4)) (2 '(4))))
          ((= varnum 2)
           (case bin (0 '(0 1 2)) (1 '(2 3 4)) (2 nil)))
          (t (error "no bin")))))


(setf c (make-csp :name "captest" 
                  :var-init-hook #'(lambda (var) 
                                     (declare (ignore var))(list 5 nil))
                  :csp-type 'test-cap-csp))
(setf cc (make-capacity-constraint 
          c :name "test"
          :limit-array   (make-array 3 :initial-element 2)
          :remove-values-if-not-in-any-bin t
          :capcon-type 'test-cap-con))
(reset-csp c)
(show-csp-state c)
(setf v0 (make-var :csp c :number 0 :var-type 'test-cap-var))
(setf v1 (make-var :csp c :number 1 :var-type 'test-cap-var))
(setf v2 (make-var :csp c :number 2 :var-type 'test-cap-var))
(bin-to-values c cc v0 0)
(value-to-bins c cc v0 4)
(required-capacity c cc v2 2 0)

(describe cc)
(find-capacity-constraint c "test")
(show-csp-state c)
(reset-csp c)
(check-csp-state c)
(reset-and-check c)

(assign-value v0 0)
(assign-value v1 0)
(unassign-value v0)
(unassign-value v1)
(assign-value v2 0)

(assign-value v0 4)
(assign-value v1 4)
(unassign-value v0)

(assign-value v1 4)
(assign-value v0 1)
(why-conflicts c v2 0)
(change-bin-limit cc 0 5)
(change-bin-limit cc 0 2)
(change-bin-limit cc 0 0)
(change-bin-limit cc 1 1)
(change-bin-limit cc 1 10)
(assign-value v2 0)
(unassign-value v0)
(unassign-value v1)
(unassign-value v2)

(dolist (val0 '(0 1 2 3 4))
  (assign-value v0 val0)
  (dolist (val1 '(0 1 2 3 4))
    (assign-value v1 val1)
    (dolist (val2 '(0 1 2 3 4))
      (assign-value v2 val2)
      
      )
    )
  )


|#

;;; -----------------------------------------------------------------------
;;; UTILITY FUNCTIONS FOR CAPACITY CONSTRAINTS (ALL TYPES)
;;; -----------------------------------------------------------------------

#|
;; no consumption, always fits
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 6 0 1   1 nil nil nil))) ;t t
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 6 0 1   1 0 nil nil))) ;t t
;; var 0 being assigned, using capacity 1; bin usage increased from 9 to 10.  Limit is 8
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 8 0 1   0 1 t 1))) ;nil nil
;; var 0 being assigned, using capacity 1; bin usage increased from 9 to 10.  Limit is 12
;; check assigned var
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   0 1 t 1))) ;t t
;; check another value assigned to same bin
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 1 t 1))) ;t t
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 4 t 1))) ;t nil
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 6 t 1))) ;nil nil
;; check another var not assigned to same bin
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 1 nil 1))) ;t t
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 3 nil 1))) ;t nil
(print (multiple-value-list (would-fit-before-will-fit-now 9 10 12 0 1   1 4 nil 1))) ;nil nil

;; var 0 being unassigned, using capacity 1; bin usage decreased from 10 to 9  Limit is 8
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 8 0 1   0 1 t 1))) ;nil nil
;; var 0 being unassigned, using capacity 1; bin usage decreased from 10 to 9  Limit is 12
;; check assigned var
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   0 1 t 1))) ;t t
;; check another value assigned to same bin
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 1 t 1))) ;t t
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 4 t 1))) ;nil t
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 6 t 1))) ;nil nil
;; check another var not assigned to same bin
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 1 nil 1))) ;t t
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 3 nil 1))) ;nil t
(print (multiple-value-list (would-fit-before-will-fit-now 10 9 12 0 1   1 4 nil 1))) ;nil nil

|#

(defun WOULD-FIT-BEFORE-WILL-FIT-NOW (old-bin-usage 
				      new-bin-usage
				      bin-limit
				      
				      var-assignment-changed
				      consumed-by-value-assignment-changed

				      var-to-check
				      required-by-value-to-check
				      var-to-check-assigned-to-bin-to-check-p
				      consumed-by-var-to-check-assigned-value
				      )

  "Return multiple values
     would-fit-before:   non-nil if value-to-check was not in conflict with a capacity constraint
                         when old-bin-usage was committed
     will-fit-now:       non-nil if value-to-check is not in conflict with capacity constraint when
                         new-bin-usage is committed
   bin-to-check refers to the bin being checked.
   var identifiers are only used to test for equality, i.e. whether var-assignment-changed
   is the same as var-to-check or not.
   The args are all numeric >= 0 unless specified:
     -- quantites related to the bin --
     old-bin-usage              : quantity of resource previously consumed by 
                                  assignments in bin-to-check. This must include 
                                  all of the capacity committed by all 
                                  assignments except the one currently being made.     
     new-bin-usage              : quantity of resource consumed after changed 
                                  assignment in bin-to-check. This must include 
                                  all of the capacity committed by all assignments
                                  made. If new-bin-usage > old-bin-usage the 
                                  var is being assigned, else it is being unassigned.
     bin-limit                  : maximum quantity of resource consumable 
                                  without conflict in bin-to-check
     -- quantities related to the variable/value currently being assigned --
     var-assignment-changed     : number of var currently being 
                                  assigned/unassigned
     consumed-by-value-assignment-changed:  capacity consumed by 
                                  var-assignment-changed/value-assignment-changed 
                                : = |new-bin-usage - old-bin-usage|
     -- quantities related to the variable/value being checked for conflicts--
     var-to-check               : number of the var being checked for conflicts.  
     required-by-value-to-check : quantity of resource required by var for 
                                  assignment to value-to-check.  May be nil if 
                                  no capacity required.
     var-to-check-assigned-to-bin-to-check-p  : t if the var-to-check is assigned 
                                  to the bin being checked, nil if not assigned 
                                  or if value-assigned does not belong to any bin
     consumed-by-var-to-check-assigned-value: quantity of resource consumed by 
                                  assigned value of var-to-check, 
                                  or nil if not assigned or if assigned value 
                                  does not require resource
     "

  (let ((would-fit-before nil)
	(will-fit-now nil)
        (contribution-to-subtract-from-old-bin-usage 0)
        (contribution-to-subtract-from-new-bin-usage 0))

    ;; basic scheme:  if the var being checked contributes C to a bin's usage, and the
    ;; var/value requires R to be committed, then there is no conflict (i.e. will fit)
    ;; if BU-C+R <= BL where BU is bin usage and BL is bin limit.

    ;; get the contributions to subtract:
    (cond
     
     ;; check the variable currently being assigned/unassigned
     ((= var-assignment-changed var-to-check)
      (cond 
       ((> new-bin-usage old-bin-usage)
        ;; var is being assigned, so contribution is to new bin usage only
        (setf contribution-to-subtract-from-new-bin-usage consumed-by-value-assignment-changed))
       (t ;; var is being unassigned, so contribution is to old bin usage only
        (setf contribution-to-subtract-from-old-bin-usage consumed-by-value-assignment-changed))))
     
     ;; check a var which is already assigned to the bin being checked
     ((and (/= var-assignment-changed var-to-check)
           var-to-check-assigned-to-bin-to-check-p)
      (setf contribution-to-subtract-from-old-bin-usage consumed-by-var-to-check-assigned-value)
      (setf contribution-to-subtract-from-new-bin-usage consumed-by-var-to-check-assigned-value))
     
     ;; check a var which is not assigned, or is assigned to a different bin
     ((and (/= var-assignment-changed var-to-check)
           (not var-to-check-assigned-to-bin-to-check-p))
      ;; no action: contributions default to zero
      )
     
     ;; dropthrough:  error condition
     (t (error "capacity constraint error")))

    ;; figure out if would fit or not
    (cond 
     
      ;; will always fit since requires none of capacity
     ((or (null required-by-value-to-check)
          (= 0 required-by-value-to-check))
      (setf would-fit-before t will-fit-now t))
     
     (t ;;otherwise compute
      (setf would-fit-before
            (<= (+ (- old-bin-usage contribution-to-subtract-from-old-bin-usage)
                   required-by-value-to-check)
                bin-limit))
      (setf will-fit-now
            (<= (+ (- new-bin-usage contribution-to-subtract-from-new-bin-usage)
                   required-by-value-to-check)
                bin-limit))))

    ;; returned values
    (values would-fit-before will-fit-now)
    ))

#|
(reset-csp c)
(show-csp-state c)
(assign-value v0 1)
(assign-value v1 2)
(unassign-value v0)
(unassign-value v1)
|#

(defmethod CAPACITY-CONSTRAINT-VAR-VALUE-ASSIGN 
  ((capcon capacity-constraint) (v var) value)
  "var v has its assignment made to value: update state"
  
  (let* ((csp (csp capcon))
         (bin (value-to-bin csp capcon v value)))
    
    ;; if value belongs to no bin (bin is nil), do nothing
    (when bin
      (let* (
             (varnum (var-number v))
             (usage-array (bin-usage capcon)) ;; usage array
             (old-usage (aref usage-array bin)) ;; old usage in this bin
             (limit (aref (bin-limit capcon) bin)) ;; old limit in bin
             (consumed-cap (consumed-capacity csp capcon v bin value))
             (new-usage (+ old-usage consumed-cap))
             (weight (cap-constraint-weight capcon))
             )
        
        ;; update bin-usage
        (setf (aref usage-array bin) new-usage)
        
        ;; update conflicts
        ;; "vtc" stands for var-to-check in the following
        (do-for-all-vars 
         :var vtc :csp csp
         :form
         (let ((vtc-max-usage (max-required-capacity csp capcon vtc)))
           (cond
            
            ((or (not vtc-max-usage) ;no usage of capacity
                 (and vtc-max-usage  ;possible usage
                      (not (eq t vtc-max-usage)) ;max not unknown
                      (< (+ new-usage vtc-max-usage) limit)))
             ;; no need to check: can't possibly change conflicts
             ;;(format t "~%skipping check for ~a" vtc)
             )
            
            (t ;check each value
             (let* ((vtc-number (var-number vtc))
                    (vtc-assignment (assignment vtc))
                    (vtc-assigned-bin 
                     (if vtc-assignment 
                       (value-to-bin csp capcon vtc vtc-assignment)))
                    (vtc-assigned-to-bin-to-check
                     (if vtc-assigned-bin (= vtc-assigned-bin bin)))
                    (vtc-consumed-capacity
                     (if vtc-assigned-to-bin-to-check
                       (consumed-capacity 
                        csp capcon vtc vtc-assigned-bin vtc-assignment))))
               
               (dolist (val-to-check (bin-to-values csp capcon vtc bin))
                 (multiple-value-bind 
                   (would-fit-before will-fit-now)
                   (would-fit-before-will-fit-now old-usage new-usage limit
                                                  varnum consumed-cap
                                                  vtc-number
                                                  (required-capacity
                                                   csp capcon vtc bin val-to-check)
                                                  vtc-assigned-to-bin-to-check
                                                  vtc-consumed-capacity)
                   ;;(format t " <~d ~d>" vtc-number val-to-check)
                   (if (and would-fit-before
                            (not will-fit-now))
                     (add-to-conflicts-on-value vtc val-to-check weight)))))))))))))
 

(defmethod CAPACITY-CONSTRAINT-VAR-VALUE-UNASSIGN 
  ((capcon capacity-constraint) (v var) value)
  "var v has its assignment unmade from value: update state"
  
  (let* ((csp (csp capcon))
         (bin (value-to-bin csp capcon v value)))

    ;; if value belongs to no bin (bin is nil), do nothing
    (when bin
      (let* (         
             (varnum (var-number v))
             (usage-array (bin-usage capcon)) ;; usage array
             (old-usage (aref usage-array bin)) ;; old usage in this bin
             (limit (aref (bin-limit capcon) bin)) ;; old limit in bin
             (consumed-cap (consumed-capacity  csp capcon v bin value))
             (new-usage (- old-usage consumed-cap))
             (weight (cap-constraint-weight capcon))
             )
        
        ;; update bin-usage
        (setf (aref usage-array bin) new-usage)
        
        ;; update conflicts
        ;; "vtc" stands for var-to-check in the following
        (do-for-all-vars
         :var vtc :csp csp
         :form
         (let ((vtc-max-usage (max-required-capacity csp capcon vtc)))
           (cond
            
            ((or (not vtc-max-usage)
                 (and vtc-max-usage
                      (not (eq t vtc-max-usage))
                      (< (+ old-usage vtc-max-usage) limit)))
             ;; no need to check: can't possibly change conflicts
             ;;(format t "~%skipping check for ~a" vtc)
             )
            
            (t ;check each value
             (let* ((vtc-number (var-number vtc))
                    (vtc-assignment (assignment vtc))
                    (vtc-assigned-bin 
                     (if vtc-assignment (value-to-bin csp capcon vtc vtc-assignment)))
                    (vtc-assigned-to-bin-to-check
                     (if vtc-assigned-bin (= vtc-assigned-bin bin)))
                    (vtc-consumed-capacity
                     (if vtc-assigned-to-bin-to-check
                       (consumed-capacity 
                        csp capcon vtc vtc-assigned-bin vtc-assignment))))
               
               (dolist (val-to-check (bin-to-values csp capcon vtc bin))
                 (multiple-value-bind 
                   (would-fit-before will-fit-now)
                   (would-fit-before-will-fit-now old-usage new-usage limit
                                                  varnum consumed-cap
                                                  vtc-number
                                                  (required-capacity 
                                                   csp capcon vtc bin val-to-check)
                                                  vtc-assigned-to-bin-to-check
                                                  vtc-consumed-capacity)
                   ;;(format t " <~d ~d>" vtc-number val-to-check)
                   (if (and (not would-fit-before)
                            will-fit-now)
                     (subtract-from-conflicts-on-value vtc val-to-check weight)))))))))))))


;;; see repair-examples.lisp for a bin packing example using capacity constraints


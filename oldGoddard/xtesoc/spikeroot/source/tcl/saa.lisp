;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; saa.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: saa.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.2  1991/12/01  00:26:50  johnston
;;; no changes
;;;
;;; Revision 1.1  1991/11/26  15:45:35  johnston
;;; Initial revision
;;;
;;;
;;; routines for SAA passage orbital events
;;;
;;; MDJ 8 Nov 1991
;;;

;;; ======================================================================
;;;                                                EARTH-POLY-REGION CLASS
;;; a polygonal region on the earth's surface

(defclass EARTH-POLY-REGION ()
  ((name :initarg :name
    :initform "Anonymous"
    :documentation "Name of this region (for printing only)")
   (point-list 
    :initform nil :initarg :point-list
    :documentation
    "list of longitude latitude points on earth surface representing
      polygonal region.  Last point will be connected with first and thus
      need not be repeated.  Units are DEGREES.")
   (vector-center
    :initform nil
    :documentation
    "geocentric unit vector to mean of region (vector average of  boundary
      points")
   (poly-triad
    :initform nil
    :documentation
    "triad (standard north-east) centered on vector center, for definition
       of tangent plane for points")
   (max-radius
    :initform nil
    :documentation
    "angle in radians from vector-center to most distant point on boundary
      of polygon")
   (point-slope-intercept-list
    :initform nil
    :documentation
    "list of boundary LINES in the form:
       (xsi1 eta1 xsi2 eta2 slope cept) where slope, cept is for line through
       xsi1 eta1 and xsi2 eta2")
   )
  (:documentation 
   "SAA regions (or other polygonal regions defined on earth surface)"))

;;; --- print function

(defmethod PRINT-OBJECT ((obj EARTH-POLY-REGION) stream)
  (format stream "#<EARTH-POLY-REGION ~a>"
          (slot-value obj 'name)))

;;; --- constructor

(defun MAKE-EARTH-POLY-REGION (&key (name "???") 
                                    point-list)
  
  "Make an instance of an EARTH-POLY-REGION class.  Input arguments are:
    :name <string> - print name
    :point-list ((long1 lat1)(long2 lat2)...) - list of longitude latitude
      pairs in decimal degrees specifying vertices of region on earth's
      surface.
    Returned value is instance of earth-poly-region"
  
  (unless point-list
    (error "Point list is required for earth-poly-region"))
  (let ((EPR (make-instance 'earth-poly-region 
                            :name name
                            :point-list point-list)))
    (set-derived-earth-poly-region-quantities EPR)
    EPR))


;;; --- initialize derived quantities at object construction
;;; (could also be called if point-list changes, but this is not implemented)

(defmethod SET-DERIVED-EARTH-POLY-REGION-QUANTITIES ((EPR EARTH-POLY-REGION))
  
  "Calculate derived quantities for earth-poly-region.  It is assumed
    that the point-list slot contains the list of vertex points."
  
  (with-slots
    (point-list vector-center poly-triad max-radius point-slope-intercept-list)
    EPR
    
    ;; compute vector list corresponding to points; accum vector for mean
    (let ((vector-list nil)
          (summed-vectors '(0 0 0)))
      (dolist (point point-list)
        ;; calculate vector corresponding to point
        (let ((vector (3vect-spherical-to-cartesian 
                       (list (radians (first point)) 
                             (radians (second point))))))
          ;;save on vector-list
          (push vector vector-list)
          ;; accumulate to get mean vector
          (setf summed-vectors (3vect-add summed-vectors vector))))   ; end dolist
      
      ;; compute mean vector
      (setf vector-center
            (3vect-normalize
             (3vect-times-scalar (/ 1.0 (length vector-list)) summed-vectors)))
      
      ;; compute triad
      (setf poly-triad (3vect-to-triad vector-center))
      
      ;; find max radius
      (setf max-radius 0)
      (dolist (vector vector-list)
        (setf max-radius (max max-radius 
                              (3vect-separation vector-center vector))))
      
      (setf point-slope-intercept-list nil)
      (let ((v1 (first (last vector-list))))
        (dolist (v2 vector-list)
          ;; v1 and v2 are two adjacent vectors to points on region boundary
          (let* ((xsi-eta1 (3vect-to-tp-coord poly-triad v1))
                 (xsi-eta2 (3vect-to-tp-coord poly-triad v2))
                 (xsi1 (first xsi-eta1)) (eta1 (second xsi-eta1))
                 (xsi2 (first xsi-eta2)) (eta2 (second xsi-eta2))
                 ;; KNOWN BUG:  will blow up if xsi1=xsi2
                 (slope (/ (- eta2 eta1) (- xsi2 xsi1)))
                 (cept (- eta1 (* slope xsi1))))
            ;;(format t "~%xsi eta 1: ~6,2f ~6,2f 2: ~6,2f ~6,2f  slope ~6,2f  cept ~6,2f"
            ;;       xsi1 eta1 xsi2 eta2 slope cept)
            (push (list xsi1 eta1 xsi2 eta2 slope cept)
                  point-slope-intercept-list)
            (setf v1 v2))))
      )))


(defmethod DESCRIBE-EARTH-POLY-REGION ((EPR EARTH-POLY-REGION) &key (stream t))
  
  "Send readable description of EARTH-POLY-REGION to an output stream.
    Default is *standard-output*."
  
  (with-slots
    (name point-list vector-center max-radius) EPR
    
    (format stream "~%Earth poly region name ~a, ~d points on boundary"
            name (length point-list))
    (let ((center-long-lat (3vect-cartesian-to-spherical vector-center)))
      (format stream "~%  center: longitude ~,2fd (~,2fd), latitude ~,2fd"
              (degrees (first center-long-lat))
              (- (degrees (first center-long-lat)) 360)
              (degrees (second center-long-lat))))
    (format stream "~%  max radius:  ~,2fd" (degrees max-radius))))


(defmethod POINT-IN-EARTH-POLY-REGION ((EPR EARTH-POLY-REGION) vect)
  
  "Return t if vect is within region.  Vect is in earth frame i.e. z along
    north pole, x towards Greenwich on equator, y makes RHS"

  (with-slots
    (vector-center poly-triad max-radius point-slope-intercept-list)
    EPR

    (cond ((> (3vect-separation vect vector-center) max-radius)
           ;; no points within radius, so not in region
           nil)
  
          (t
           (let* ((xsi-eta (3vect-to-tp-coord poly-triad vect))
                  (xsi (first xsi-eta))
                  (eta (second xsi-eta))
                  (count 0) ; number of intersections w/boundary
                  )
             (dolist (bound-line point-slope-intercept-list)
               (let ((xsi1 (first bound-line))
                     (eta1 (second bound-line))
                     (xsi2 (third bound-line))
                     (eta2 (fourth bound-line))
                     (slope (fifth bound-line))
                     (cept (sixth bound-line)))
                 (cond ((or (and (> xsi1 xsi) (> xsi2 xsi))
                            (and (< xsi1 xsi) (< xsi2 xsi))
                            (and (> eta1 eta) (> eta2 eta)))
                        ;; no intersection
                        )
                       ((<= (+ (* slope xsi) cept) eta)
                        (incf count))
                       (t nil))))
             (if (oddp count) t nil))))))
             

(defmethod OBJECT-IN-EARTH-POLY-REGION ((EPR earth-poly-region)
                                        (AO astronomical-object)
                                        jd)

  "Return t if object is in earth poly region at time, nil otherwise."
  
  (point-in-earth-poly-region 
   EPR (earth-position-below AO jd)))

;;; ----------------------------------------------------------------------
;;;                                                  SAA EARTH-POLY-REGION
;;;
;;; this is a relatively crude SAA model, but approximately representative

(defvar *SAA* nil)
(setf *SAA* (make-earth-poly-region :name "SAA" 
                                    :point-list '((30 -38)
                                                  (15 -45)
                                                  (0 -48)
                                                  (-15 -55)
                                                  (-30 -55)
                                                  (-45 -50)
                                                  (-60 -45)
                                                  (-70 -30)
                                                  (-65 -15)
                                                  (-60 -10)
                                                  (-45 -7)
                                                  (-30 -7)
                                                  (-15 -15)
                                                  (0 -22)
                                                  (15 -28)
                                                  (30 -32))))
#| Tests:
(describe *saa*)
(describe-earth-poly-region *SAA*)
(point-in-earth-poly-region 
 *SAA* (ra-dec-to-cartesian-3vect '(50 50) :units :degrees))
(point-in-earth-poly-region 
 *SAA* (ra-dec-to-cartesian-3vect '(-30 -33) :units :degrees))


(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.05 0.95 0.1 0.80)
(set-x-window-scale g -180 180)
(set-y-window-scale g -90 90)
(plot-poly g (slot-value *SAA* 'point-list) :frame t :fill 0.5)
(box-window g)
(box-rect g)
(label-y-axis g -90 90 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(label-x-axis g -180 180 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(record-wrapup g)

;; plot a ground trace from start to end time
(let* ((xy-list nil)
       (start (time-of-asc-node *ST*))
       (end (+ start (nodal-period *ST*)))
       (delta (/ 2.0 1440.0))
       (prev-long-lat nil)
       (time start))
  (loop
    (let* ((long-lat (earth-longitude-latitude-below *st* time :units :degrees))
           (long (if (< (first long-lat) 180)
                   (first long-lat) (- (first long-lat) 360)))
           (lat (second long-lat)))
      (cond ((or (null prev-long-lat)
                 (< (abs (- long (first prev-long-lat))) 180))
             (push (list long lat) xy-list)
             (setf prev-long-lat (list long lat)))
            (t ;; start new track
             (plot-curve g xy-list)
             (setf xy-list nil prev-long-lat nil))))
    (incf time delta)
    (if (> time end) (return nil)))
  (if xy-list (plot-curve g xy-list)))

(show *st*)

;; long of asc node (degrees)
(first (earth-longitude-latitude-below 
        *ST* (time-of-asc-node *ST*) :units :degrees))

;; reset *ST* orbit parameters (DRM orbit)
(update-orbit-parameters 
 *ST*
 :inclination          28.5             ;inclination, deg
 :time-asc-node      2135.520145833     ;time asc node, TJD
 :RA-asc-node          64.906           ;RA asc node, deg
 :regression-rate      -6.7046          ;nodal regression rate, deg/day
 :period               94.42896         ;period, minutes
 :semimajor-axis     6878.165           ;semimajor axis, km
 )
|#

(defmethod TABULATE-REGION-ENTRY-EXIT ((LEOS low-earth-orbit-satellite)
                                       (EPR earth-poly-region)
                                       &key (steps 360) (debug nil))

  "Create a table on *standard-output* of region (e.g. SAA) entry, 
    exit times (in days relative to ascending node), and earth longitude
    & latitude of entry/exit (in decimal degrees.  Table is keyed by longitude
    of ascending node.  Orbit model is successively modified by changing
    the time of asc node by the number of steps specified"

  ;; update orbit parameters to change time of asc node by delta
  ;; 4 min in time asc node => 1 deg longitude asc node
  (let ((saved-time-asc-node (time-of-asc-node LEOS))
        (stepsize (/ 1.0 steps)))
    (dotimes (i steps)
      (update-orbit-parameters 
       LEOS
       :inclination          (degrees (inclination LEOS))
       :time-asc-node        (- (time-of-asc-node LEOS) stepsize)
       :RA-asc-node          (degrees (RA-of-asc-node LEOS))
       :regression-rate      (degrees (nodal-regression-rate LEOS))
       :period               (* 1440.0 (nodal-period LEOS))
       :semimajor-axis       (semimajor-axis LEOS))
      (multiple-value-bind
        (long-asc-node entry-time entry-long-lat
                       exit-time exit-long-lat)
        (region-entry-exit-wrt-asc-node LEOS EPR :debug debug)
        (cond ((and entry-time exit-time)
               (format t "~%(~8,3f ~9,6f ~8,3f ~8,3f   ~9,6f ~8,3f ~8,3f)"
                       long-asc-node entry-time
                       (first entry-long-lat) (second entry-long-lat)
                       exit-time
                       (first exit-long-lat) (second exit-long-lat)))
              (t (format t "~%(~8,3f nil nil nil   nil nil nil)"
                         long-asc-node)))))
    ;; table finished, restore orbit model as it was:
    (update-orbit-parameters 
       LEOS
       :inclination          (degrees (inclination LEOS))
       :time-asc-node        saved-time-asc-node
       :RA-asc-node          (degrees (RA-of-asc-node LEOS))
       :regression-rate      (degrees (nodal-regression-rate LEOS))
       :period               (* 1440.0 (nodal-period LEOS))
       :semimajor-axis       (semimajor-axis LEOS))
    ;; return nil
    nil))


(defmethod REGION-ENTRY-EXIT-WRT-ASC-NODE ((LEOS low-earth-orbit-satellite)
                                           (EPR earth-poly-region)
                                           &key (steps-per-orbit 100)
                                           (tolerance 1.0e-5)
                                           (debug nil))

  "Compute and return times of entry/exit of low-earth-orbit-satellite in/out
    of polygonal region on earth surface.  The orbit is checked from the
    asc node for one nodal period, broken into steps-per-orbit samples.
    Five values are returned:
     long-asc-node - longitude of the orbit ascending node, degrees
     entry-time - days, from time of asc node
     entry-long-lat - list (longitude latitude) of satellite position above
      earth at time of entry.  In degrees.
     exit-time - days, from time of asc node
     exit-long-lat - list (longitude latitude) of satellite position above
      earth at time of exit.  In degrees.
    Note:  last four returned values will be NIL if there is no entry/exit in
    the orbit."

  (let* ((start-time (time-of-asc-node LEOS))
         (period (nodal-period LEOS))
         (step (/ period steps-per-orbit))
         (long-asc-node (first (earth-longitude-latitude-below 
                                LEOS start-time :units :degrees)))
         (in-prev-step (object-in-earth-poly-region EPR LEOS start-time))
         (prev-time start-time)
         (entry-time nil)
         (exit-time nil)
         )
    (dotimes (i steps-per-orbit)
      (let* ((time (+ start-time (* (1+ i) step)))
             (in-this-step (object-in-earth-poly-region EPR LEOS time)))
        (cond ((and in-prev-step (not in-this-step))
               ;; exiting
               (setf exit-time
                     (bisect-for-region-entry-exit 
                      LEOS EPR :t1 prev-time :in1 in-prev-step
                      :t2 time :in2 in-this-step :tolerance tolerance))
               )
              ((and (not in-prev-step) in-this-step)
               ;; entering
               (setf entry-time
                     (bisect-for-region-entry-exit 
                      LEOS EPR :t1 prev-time :in1 in-prev-step
                      :t2 time :in2 in-this-step :tolerance tolerance))
               )
              (t nil))
        (setf in-prev-step in-this-step
              prev-time time)))
    (when (and debug entry-time exit-time)
      (format t "~%long asc node: ~,4fdeg,  entry: ~,4fmin,  exit ~,4fmin"
              long-asc-node
              (* (- entry-time start-time) 1440.0)
              (* (- exit-time start-time) 1440.0))
      (format t "~%  tolerance ~,3fmin=~,1fsec"
              (* tolerance 1440.0) (* tolerance 86400))
      (format t " check: entry ~a/~a, exit ~a/~a"
              (object-in-earth-poly-region EPR LEOS (- entry-time tolerance))
              (object-in-earth-poly-region EPR LEOS (+ entry-time tolerance))
              (object-in-earth-poly-region EPR LEOS (- exit-time tolerance))
              (object-in-earth-poly-region EPR LEOS (+ exit-time tolerance)))
      )                                 ; end when
    ;; returned values:
    (values long-asc-node 
            (if entry-time (- entry-time start-time)  nil)
            (if entry-time (earth-longitude-latitude-below 
                            LEOS entry-time :units :degrees) nil)
            (if exit-time  (- exit-time  start-time)  nil)
            (if exit-time (earth-longitude-latitude-below 
                           LEOS exit-time :units :degrees) nil))
    ))


(defmethod BISECT-FOR-REGION-ENTRY-EXIT ((LEOS low-earth-orbit-satellite)
                                         (EPR earth-poly-region)
                                         &key t1 in1 t2 in2
                                         (tolerance 1.0e-5))
  
  "Refine estimate of polygonal region entry/exit times.  Input args
    are:
    LEOS - low-earth-orbit-satellite object instance
    EPR - earth poly region instance
    t1 and t2 are TJD times bounding entry or exit,
    and in1 and in2 are flags indicating whether LEOS is in (t) or out (nil)
     of region at t1 and t2, respectively.
    It must be known that satellite is in region at t1 and out at t2, or
    vice versa.  Further, t1 must be < t2, and in1 and in2 must be 
    either t/nil or nil/t.  Bisection method it used, to a time tolerance
    specified (in days).  Returned value is time of entry/exit, as TJD."
  
  (loop 
    (let* ((tmid (+ t1 (* 0.5 (- t2 t1))))
           (in-mid (object-in-earth-poly-region EPR LEOS tmid)))
      (cond ((or (and in1 in-mid)
                 (and (not in1) (not in-mid)))
             (setf t1 tmid in1 in-mid))
            ((or (and in-mid in2)
                 (and (not in-mid) (not in2)))
             (setf t2 tmid in2 in-mid))
            (t (error "dropped through bisection")))
      (if (< (- t2 t1) tolerance)
        (return (+ t1 (* 0.5 (- t2 t1))))))))

;;; ----------------------------------------------------------------------
;;;                                                   SAA ENTRY-EXIT TABLE
;;; 
;;; the following table came from the non-nil entries of:
;;;   (tabulate-region-entry-exit *ST* *SAA* :steps 360)

;;; NOTE: long asc node must be in INCREASING order, but wraparound 360->0
;;; is OK.

(defvar *SAA-entry-exit-table* nil)
;;; put this in a progn and return nil, to prevent having table echoed
;;; to screen while initializing
(progn
  (setf   
   *SAA-entry-exit-table*
   ;; values are:
   ;;  asc node  entry    entry     entry      exit     exit       exit
   ;; long (deg) time(d) long(deg) lat(deg)   time(d)  long(deg)  lat(deg)
   '(
     ( 342.000 nil nil nil   nil nil nil)
     ( 342.287  0.061393  299.289  -10.729    0.061762  300.992   -9.818)
     ( 343.290  0.060947  298.220  -11.815    0.061869  302.489   -9.551)
     ( 344.293  0.060511  297.181  -12.859    0.061987  304.033   -9.257)
     ( 345.296  0.060081  296.149  -13.872    0.062105  305.575   -8.962)
     ( 346.298  0.059666  295.171  -14.831    0.062228  307.140   -8.653)
     ( 347.301  0.059395  294.868  -15.448    0.062356  308.727   -8.331)
     ( 348.304  0.059154  294.705  -15.987    0.062484  310.314   -8.007)
     ( 349.307  0.058913  294.536  -16.519    0.062622  311.946   -7.657)
     ( 350.309  0.058677  294.385  -17.033    0.062755  313.553   -7.318)
     ( 351.312  0.058442  294.227  -17.538    0.062878  315.113   -7.005)
     ( 352.315  0.058206  294.063  -18.036    0.062873  316.093   -7.018)
     ( 353.317  0.057976  293.917  -18.516    0.062868  317.072   -7.031)
     ( 354.320  0.057745  293.764  -18.987    0.062863  318.052   -7.044)
     ( 355.323  0.057520  293.630  -19.440    0.062863  319.055   -7.044)
     ( 356.326  0.057294  293.489  -19.884    0.062858  320.034   -7.057)
     ( 357.328  0.057069  293.341  -20.321    0.062858  321.037   -7.057)
     ( 358.331  0.056848  293.213  -20.738    0.062858  322.040   -7.057)
     ( 359.334  0.056628  293.078  -21.148    0.062858  323.042   -7.057)
     (   0.337  0.056413  292.964  -21.539    0.062858  324.045   -7.057)
     (   1.339  0.056198  292.842  -21.922    0.062858  325.048   -7.057)
     (   2.342  0.055983  292.715  -22.296    0.062863  326.074   -7.044)
     (   3.345  0.055767  292.581  -22.661    0.062863  327.076   -7.044)
     (   4.348  0.055557  292.468  -23.008    0.062868  328.102   -7.031)
     (   5.350  0.055353  292.376  -23.338    0.062873  329.128   -7.018)
     (   6.353  0.055142  292.251  -23.668    0.062863  330.085   -7.044)
     (   7.356  0.054938  292.148  -23.980    0.062750  330.576   -7.331)
     (   8.359  0.054733  292.039  -24.283    0.062638  331.067   -7.618)
     (   9.361  0.054533  291.952  -24.570    0.062530  331.581   -7.890)
     (  10.364  0.054333  291.860  -24.848    0.062417  332.071   -8.175)
     (  11.367  0.054133  291.763  -25.116    0.062310  332.583   -8.447)
     (  12.369  0.053933  291.661  -25.376    0.062197  333.071   -8.731)
     (  13.372  0.053739  291.582  -25.619    0.062089  333.581   -9.000)
     (  14.375  0.053544  291.499  -25.854    0.061977  334.068   -9.282)
     (  15.378  0.053355  291.440  -26.074    0.061869  334.577   -9.551)
     (  16.380  0.053160  291.348  -26.290    0.061756  335.061   -9.831)
     (  17.383  0.052970  291.281  -26.491    0.061649  335.568  -10.098)
     (  18.386  0.052781  291.210  -26.684    0.061536  336.051  -10.376)
     (  19.389  0.052591  291.135  -26.867    0.061429  336.557  -10.641)
     (  20.391  0.052407  291.086  -27.037    0.061321  337.061  -10.905)
     (  21.394  0.052222  291.033  -27.197    0.061208  337.541  -11.180)
     (  22.397  0.052038  290.977  -27.349    0.061101  338.043  -11.442)
     (  23.400  0.051853  290.918  -27.491    0.060993  338.545  -11.703)
     (  24.402  0.051674  290.886  -27.621    0.060885  339.045  -11.963)
     (  25.405  0.051490  290.822  -27.745    0.060778  339.545  -12.222)
     (  26.408  0.051310  290.785  -27.857    0.060670  340.043  -12.480)
     (  27.411  0.051131  290.746  -27.960    0.060558  340.516  -12.749)
     (  28.413  0.050957  290.735  -28.052    0.060450  341.012  -13.005)
     (  29.416  0.050778  290.692  -28.138    0.060342  341.507  -13.259)
     (  30.419  0.050603  290.677  -28.212    0.060240  342.025  -13.501)
     (  31.421  0.050424  290.631  -28.280    0.060132  342.518  -13.753)
     (  32.424  0.050250  290.614  -28.337    0.060025  343.010  -14.004)
     (  33.427  0.050076  290.595  -28.386    0.059917  343.500  -14.253)
     (  34.430  0.049902  290.576  -28.426    0.059810  343.989  -14.502)
     (  35.432  0.049732  290.586  -28.457    0.059702  344.477  -14.749)
     (  36.435  0.049558  290.565  -28.480    0.059600  344.988  -14.983)
     (  37.438  0.049389  290.574  -28.494    0.059487  345.449  -15.239)
     (  38.441  0.049215  290.553  -28.500    0.059379  345.933  -15.482)
     (  39.443  0.049046  290.562  -28.497    0.059272  346.416  -15.724)
     (  40.446  0.048877  290.571  -28.487    0.059159  346.872  -15.976)
     (  41.449  0.048708  290.580  -28.468    0.059051  347.352  -16.214)
     (  42.452  0.048539  290.589  -28.441    0.058944  347.831  -16.452)
     (  43.454  0.048370  290.600  -28.406    0.058836  348.308  -16.687)
     (  44.457  0.048201  290.611  -28.363    0.058729  348.784  -16.922)
     (  45.460  0.048032  290.623  -28.311    0.058621  349.259  -17.154)
     (  46.463  0.047863  290.636  -28.252    0.058513  349.732  -17.385)
     (  47.465  0.047699  290.680  -28.187    0.058401  350.178  -17.626)
     (  48.468  0.047530  290.696  -28.112    0.058298  350.674  -17.843)
     (  49.471  0.047366  290.743  -28.031    0.058191  351.142  -18.069)
     (  50.474  0.047197  290.762  -27.940    0.058083  351.610  -18.293)
     (  51.476  0.047033  290.813  -27.845    0.057976  352.075  -18.516)
     (  52.479  0.046864  290.835  -27.738    0.057868  352.540  -18.737)
     (  53.482  0.046700  290.890  -27.628    0.057760  353.002  -18.956)
     (  54.484  0.046531  290.916  -27.506    0.057653  353.464  -19.173)
     (  55.487  0.046367  290.975  -27.381    0.057545  353.923  -19.389)
     (  56.490  0.046198  291.007  -27.244    0.057443  354.408  -19.592)
     (  57.493  0.046034  291.071  -27.105    0.057335  354.864  -19.804)
     (  58.495  0.045865  291.108  -26.953    0.057228  355.319  -20.014)
     (  59.498  0.045701  291.176  -26.799    0.057125  355.799  -20.212)
     (  60.501  0.045532  291.219  -26.633    0.057018  356.251  -20.418)
     (  61.504  0.045368  291.294  -26.465    0.056910  356.702  -20.623)
     (  62.506  0.045199  291.342  -26.284    0.056807  357.177  -20.815)
     (  63.509  0.045035  291.423  -26.103    0.056700  357.625  -21.015)
     (  64.512  0.044866  291.478  -25.908    0.056597  358.097  -21.204)
     (  65.515  0.044702  291.565  -25.713    0.056490  358.541  -21.400)
     (  66.517  0.044533  291.627  -25.505    0.056387  359.011  -21.585)
     (  67.520  0.044363  291.692  -25.290    0.056280  359.452  -21.777)
     (  68.523  0.044200  291.790  -25.076    0.056177  359.918  -21.958)
     (  69.526  0.044030  291.862  -24.848    0.056065    0.330  -22.154)
     (  70.528  0.043861  291.938  -24.613    0.055952    0.739  -22.348)
     (  71.531  0.043692  292.018  -24.372    0.055839    1.147  -22.540)
     (  72.534  0.043523  292.102  -24.125    0.055732    1.580  -22.720)
     (  73.536  0.043354  292.190  -23.872    0.055619    1.984  -22.907)
     (  74.539  0.043185  292.281  -23.612    0.055506    2.386  -23.091)
     (  75.542  0.043016  292.376  -23.346    0.055394    2.787  -23.273)
     (  76.545  0.042842  292.448  -23.066    0.055281    3.186  -23.452)
     (  77.547  0.042673  292.552  -22.789    0.055168    3.583  -23.628)
     (  78.550  0.042499  292.632  -22.497    0.055055    3.979  -23.801)
     (  79.553  0.042330  292.743  -22.207    0.054943    4.372  -23.972)
     (  80.556  0.042155  292.832  -21.904    0.054830    4.764  -24.140)
     (  81.558  0.041986  292.951  -21.603    0.054717    5.155  -24.306)
     (  82.561  0.041812  293.048  -21.288    0.054605    5.543  -24.468)
     (  83.564  0.041638  293.149  -20.968    0.054492    5.930  -24.628)
     (  84.567  0.041464  293.254  -20.642    0.054379    6.316  -24.784)
     (  85.569  0.041290  293.363  -20.311    0.054266    6.699  -24.938)
     (  86.572  0.041115  293.476  -19.974    0.054154    7.081  -25.089)
     (  87.575  0.040936  293.568  -19.623    0.054036    7.434  -25.244)
     (  88.578  0.040762  293.689  -19.276    0.053923    7.812  -25.389)
     (  89.580  0.040583  293.789  -18.914    0.053810    8.189  -25.531)
     (  90.583  0.040408  293.919  -18.558    0.053693    8.536  -25.676)
     (  91.586  0.040229  294.027  -18.186    0.053580    8.910  -25.812)
     (  92.589  0.040050  294.139  -17.810    0.053462    9.254  -25.950)
     (  93.591  0.039871  294.256  -17.429    0.053344    9.596  -26.085)
     (  94.594  0.039691  294.376  -17.044    0.053226    9.937  -26.217)
     (  95.597  0.039512  294.500  -16.654    0.053114   10.305  -26.340)
     (  96.599  0.039333  294.628  -16.260    0.052991   10.613  -26.470)
     (  97.602  0.039148  294.735  -15.850    0.052873   10.949  -26.591)
     (  98.605  0.038969  294.871  -15.448    0.052755   11.283  -26.709)
     (  99.608  0.038784  294.986  -15.030    0.052637   11.616  -26.823)
     ( 100.610  0.038641  295.300  -14.702    0.052514   11.919  -26.939)
     ( 101.613  0.038503  295.642  -14.384    0.052391   12.219  -27.050)
     ( 102.616  0.038359  295.961  -14.051    0.052268   12.519  -27.158)
     ( 103.619  0.038221  296.306  -13.729    0.052145   12.817  -27.261)
     ( 104.621  0.038077  296.629  -13.392    0.052023   13.113  -27.361)
     ( 105.624  0.037939  296.979  -13.066    0.051900   13.409  -27.456)
     ( 106.627  0.037796  297.306  -12.725    0.051771   13.673  -27.551)
     ( 107.630  0.037657  297.659  -12.394    0.051643   13.936  -27.642)
     ( 108.632  0.037514  297.990  -12.050    0.051515   14.198  -27.728)
     ( 109.635  0.037370  298.323  -11.703    0.051382   14.429  -27.813)
     ( 110.638  0.037227  298.657  -11.355    0.051254   14.688  -27.891)
     ( 111.641  0.037089  299.017  -11.018    0.051116   14.887  -27.969)
     ( 112.643  0.036945  299.355  -10.666    0.050983   15.114  -28.039)
     ( 113.646  0.036802  299.695  -10.313    0.050844   15.310  -28.107)
     ( 114.649  0.036669  300.084   -9.984    0.050706   15.506  -28.169)
     ( 115.651  0.036612  300.827   -9.844    0.050568   15.700  -28.227)
     ( 116.654  0.036561  301.594   -9.716    0.050419   15.834  -28.282)
     ( 117.657  0.036505  302.338   -9.576    0.050276   15.996  -28.329)
     ( 118.660  0.036448  303.082   -9.436    0.050127   16.128  -28.373)
     ( 119.662  0.036392  303.826   -9.295    0.049978   16.260  -28.409)
     ( 120.665  0.036336  304.570   -9.154    0.049820   16.330  -28.442)
     ( 121.668  0.036279  305.315   -9.013    0.049666   16.430  -28.467)
     ( 122.671  0.036223  306.060   -8.872    0.049502   16.469  -28.485)
     ( 123.673  0.036161  306.781   -8.718    0.049338   16.508  -28.497)
     ( 124.676  0.036105  307.527   -8.576    0.049169   16.517  -28.500)
     ( 125.679  0.036044  308.249   -8.421    0.048995   16.496  -28.495)
     ( 126.682  0.035987  308.994   -8.279    0.048821   16.475  -28.481)
     ( 127.684  0.035926  309.717   -8.124    0.048636   16.394  -28.458)
     ( 128.687  0.035864  310.440   -7.968    0.048447   16.284  -28.423)
     ( 129.690  0.035808  311.186   -7.826    0.048252   16.144  -28.377)
     ( 130.693  0.035746  311.909   -7.670    0.048047   15.946  -28.316)
     ( 131.695  0.035685  312.633   -7.513    0.047837   15.719  -28.242)
     ( 132.698  0.035623  313.357   -7.357    0.047622   15.464  -28.154)
     ( 133.701  0.035562  314.081   -7.201    0.047391   15.122  -28.044)
     ( 134.704  0.035501  314.805   -7.044    0.047156   14.754  -27.917)
     ( 135.706  0.035490  315.761   -7.018    0.046905   14.300  -27.765)
     ( 136.709  0.035495  316.787   -7.031    0.046638   13.762  -27.585)
     ( 137.712  0.035495  317.790   -7.031    0.046356   13.141  -27.373)
     ( 138.714  0.035501  318.816   -7.044    0.046059   12.439  -27.127)
     ( 139.717  0.035506  319.842   -7.057    0.045747   11.658  -26.843)
     ( 140.720  0.035506  320.844   -7.057    0.045403   10.713  -26.502)
     ( 141.723  0.035506  321.847   -7.057    0.045040    9.665  -26.108)
     ( 142.725  0.035506  322.850   -7.057    0.044645    8.462  -25.645)
     ( 143.728  0.035506  323.852   -7.057    0.044215    7.080  -25.096)
     ( 144.731  0.035506  324.855   -7.057    0.043749    5.525  -24.453)
     ( 145.734  0.035501  325.835   -7.044    0.043226    3.696  -23.675)
     ( 146.736  0.035501  326.837   -7.044    0.042652    1.632  -22.755)
     ( 147.739  0.035495  327.817   -7.031    0.041874  358.545  -21.400)
     ( 148.742  0.035490  328.797   -7.018    0.040526  352.668  -18.800)
     ( 149.745  0.035485  329.776   -7.005    0.038364  343.114  -14.063)
     ( 150.000 nil nil nil   nil nil nil)
     ))
  nil)


;; test, especially wraparound 360->0
;; (interpolate-entry-exit-table 342.286 :table *saa-entry-exit-table*)  -before table start
;; (interpolate-entry-exit-table 342.287 :table *saa-entry-exit-table*)  -at table start
;; (interpolate-entry-exit-table 359.0   :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 359.3340001 :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 359.5   :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 0       :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 0.2     :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 0.337   :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 0.3370001 :table *saa-entry-exit-table*)
;; (interpolate-entry-exit-table 149.743 :table *saa-entry-exit-table*)  -before table end
;; (interpolate-entry-exit-table 149.745 :table *saa-entry-exit-table*)  -at table end
;; (interpolate-entry-exit-table 149.746 :table *saa-entry-exit-table*)  -after table end

#|
;;; plot

;;; plot entry/exit time vs long asc node
(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.05 0.95 0.1 0.80)
(set-x-window-scale g 0 360)
(set-y-window-scale g 0 0.07)
(let ((entry-list nil)
      (exit-list nil))
  (dotimes (i 360)
    (multiple-value-bind 
      (entry-time exit-time)
      (interpolate-entry-exit-table i :table *saa-entry-exit-table*)
      (cond (entry-time (push (list i entry-time) entry-list))
            (t (when entry-list 
                 (plot-curve g entry-list :wrap-fraction 0)
                 (setf entry-list nil))))
      (cond (exit-time (push (list i exit-time) exit-list))
            (t (when exit-list
                 (plot-curve g exit-list  :wrap-fraction 0)
                 (setf exit-list nil))))))
  (if entry-list (plot-curve g entry-list :wrap-fraction 0))
  (if exit-list  (plot-curve g exit-list  :wrap-fraction 0)))
(box-window g)
(box-rect g)
(label-y-axis g 0 .07 :tic-value 0.01 :tic-size 3 :font-size 9
              :label-format "~,2f")
(label-x-axis g 0 360 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(record-wrapup g)

;;; plot intersection points w/ contour
(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.05 0.95 0.1 0.80)
(set-x-window-scale g -180 180)
(set-y-window-scale g -90 90)
;;(set-x-window-scale g -80 40)
;;(set-y-window-scale g -60 0)
(plot-poly g (slot-value *SAA* 'point-list) :frame t :fill 0.5)
(box-window g)
(box-rect g)
(label-y-axis g -90 90 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(label-x-axis g -180 180 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(dotimes (i 36)
  (multiple-value-bind
    (entry-time exit-time entry-long-lat exit-long-lat)
    (interpolate-entry-exit-table (* i 10) :table *saa-entry-exit-table*)
    (when (and entry-time exit-time)
      (plot-symbol-at-point g (if (< (first entry-long-lat) 180)
                                (first entry-long-lat)
                                (- (first entry-long-lat) 360))
                            (second entry-long-lat)
                            :symbol *cross-symbol* :symbol-scale 2)
      (plot-symbol-at-point g (if (< (first exit-long-lat) 180)
                                (first exit-long-lat)
                                (- (first exit-long-lat) 360))
                            (second exit-long-lat)
                            :symbol *plus-symbol* :symbol-scale 2)
      )))
    
(record-wrapup g)

|#


;;; ----------------------------------------------------------------------
;;;                                                      CALCULATE SAA PCF


(defmethod SAA-PCF ((LEOS low-earth-orbit-satellite) jd1 jd2
                    &key (table *SAA-entry-exit-table*))
  
  "Return PCF valued 1 when not in SAA, 0 when in SAA, over the specified
    time range.  A table for interpolation must be specified."
  
  (let ((asc-nodes (asc-node-list LEOS jd1 jd2))
        (saa-intervals nil))
    (dolist (node asc-nodes)
      (let (
            ;; long asc node, in DEGREES
            (long-asc-node
             (first (earth-longitude-latitude-below LEOS node :units :degrees)))
            )
        (multiple-value-bind 
          (entry-time exit-time entry-long-lat exit-long-lat)
          (interpolate-entry-exit-table long-asc-node
                                        :table table)
          (declare (ignore entry-long-lat exit-long-lat))
          
          (when (and entry-time exit-time)
            (push (+ node entry-time) saa-intervals)
            (push (+ node exit-time ) saa-intervals)
            )                           ; end when
          )                             ; end multiple-value-bind
        ))                              ; end dolist
    (pcf-from-start-end-list
     (nreverse saa-intervals) 0 1)
    ))

#|
(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.05 0.95 0.1 0.80)
(set-x-window-scale g *drm-start* (+ *drm-start* 7))
(set-y-window-scale g 0 1)
(box-window g)
(box-rect g)
(plot-interval-bars g
                    (pcf-non-zero-intervals
                     (saa-pcf *ST* *drm-start* (+ *drm-start* 7))
                     *drm-start* (+ *drm-start* 7))
                    0.8 0.9
                    :frame t :fill 0)
(record-wrapup g)
|#

;;; ----------------------------------------------------------------------
;;;                                                    SIMPLER SAA METHODS


(defmethod POSSIBLE-SAA-TIMES ((LEOS low-earth-orbit-satellite) jd1 jd2 latitude)
  
  "Return list of start end times when the satellite can potentially
   be in the SAA, i.e. when it's latitude is below the specified angle (radians)
   Latitude must be zero or negative, i.e. in the southern hemisphere."
  
  (let* ((nodes (asc-node-list LEOS jd1 jd2))
         (asin-offset-angle (asin (abs (/ latitude (inclination LEOS)))))
         (node-offset-to-entry
          (* (nodal-period LEOS) (/ (+ pi asin-offset-angle) 2pi)))
         (node-offset-to-exit
          (* (nodal-period LEOS) (/ (- 2pi asin-offset-angle) 2pi)))
         result)
    
    ;;(format t  ~%nodal period ~a min  (* nodal-period 1440.0))
    ;;(format t  ~%offsets to entry ~a, exit ~a min 
    ;;	    (* node-offset-to-entry 1440.0)  (* node-offset-to-exit 1440.0))
    (dolist (a nodes)
      (push (+ a node-offset-to-entry) result)
      (push (+ a node-offset-to-exit) result))
    (nreverse result)))


(defmethod PROBABILITY-NOT-IN-SAA ((LEOS low-earth-orbit-satellite) jd1 jd2)
  
  "Return PCF valued as probability that low earth orbit satellite is not 
    in the SAA"
  
  (let ((nodes (asc-node-list LEOS jd1 jd2))
        (single-orbit-probability *unity-pcf*)
        result)
    
    ;; here is the SAA probability model, expressed as PCF for one orbit
    ;; the numbers here are for 10 protons/cm2-s from DRM p. 2-67
    ;; probability is 1 for latitude > 0
    ;; probability is 0.875 for latitude between 0 and -10 deg
    ;; probability is 0.74 for latitute between -10 and -20 deg
    ;; probability is 0.69 for latitute between -20 and -30 deg
    ;; note:  values below exagerated for emphasis
    (setf single-orbit-probability (pcf-set-value 0.5 1 0.875 single-orbit-probability))
    (setf single-orbit-probability
          (pcf-set-value
           (/ (+ pi (asin (/ (radians 10.0) (inclination LEOS)))) 2pi)
           (/ (- 2pi (asin (/ (radians 10.0) (inclination LEOS)))) 2pi)
           0.74 single-orbit-probability))
    (setf single-orbit-probability
          (pcf-set-value
           (/ (+ pi (asin (/ (radians 20.0) (inclination LEOS)))) 2pi)
           (/ (- 2pi (asin (/ (radians 20.0) (inclination LEOS)))) 2pi)
           0.69 single-orbit-probability))
    
    ;(format t  ~%single orbit PCF: ~a  single-orbit-probability)
    (setf result (pcf-replicate-periodic nodes single-orbit-probability))
    
    ;;(PCF-loop-over-intervals
    ;;  result start end value left-to-do done (nil)
    ;;  (format t  ~%~9,3f ~9,3f: ~5,2f ~5,3f  start end
    ;;      (/ (- start time-of-asc-node) nodal-period) value))
    result))


(defmethod SIMPLE-SAA-PCF ((LEOS low-earth-orbit-satellite) jd1 jd2 &key (step 0.01))
  
  "Compute PCF valued 0 when satellite is in the SAA, 1 otherwise.
    Step is the fraction of the orbital period used to check for
    SAA presence.  The model is a simple circular region."
  
  (let ((saa-center (3vect-spherical-to-cartesian 
                     (list (radians -35.0) (radians -30.0))))
        (cos-saa-radius (cos (radians 30.0)))
        (step-in-days (* step (nodal-period LEOS)))
        (result nil))
    (add-interval-to-reversed-pcf-list result *minus-infinity* 1)
    (do ((time jd1 (+ time step-in-days)))
        ((> time jd2)
         (nreverse result))
      (let* ((earth-orient (earth-orientation time))
             (sat-pos (position-vector LEOS time))
             (earth-relative-vector 
              (3x3matrix-transpose-times-vector earth-orient sat-pos)))
        ;;(format t "~%~a:  ~a" time (3vect-dot-product earth-relative-vector saa-center))
        (if (> (3vect-dot-product earth-relative-vector saa-center) 
               cos-saa-radius)
          
          ;; in SAA
          (add-interval-to-reversed-pcf-list result time 0)
          
          ;; out of SAA
          (add-interval-to-reversed-pcf-list result time 1))))))

;;(send *ST* :simple-SAA-PCF (dmy-to-time '25-Mar-90)
;;      (+ (dmy-to-time '25-Mar-90) 0.1))

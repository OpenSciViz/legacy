;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; xte-event-ps-plot.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: xte-event-ps-plot.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.6  1991/12/01  00:15:52  johnston
;;; fixed for new postscript output
;;;
;;; Revision 1.5  1991/11/26  15:35:15  johnston
;;; no substantive changes
;;;
;;; Revision 1.4  1991/08/21  11:26:41  giuliano
;;; Changed interface functions for the event calculator.
;;;
;;; Revision 1.3  91/06/19  17:25:05  johnston
;;; renamed *last... conflicting with euve
;;; 
;;; Revision 1.2  91/06/17  21:39:18  johnston
;;; changed pcf plots for actual xte constraints
;;; 
;;; Revision 1.1  91/06/17  19:56:59  johnston
;;; Initial revision
;;; 
;;;
;;; xte-event-ps-plot.lisp - postscript plots of target constraints
;;;  from similar file for EUVE.
;;;


#|======================================================================
Usage:

The functions in this file generate two kinds of constraint plots
(as postscript files that need to be sent to a postscript printer):

(GENERATE-XTE-ORB-VIEWING-PLOTS
 "clipper:1xte-ev1.ps" ; "/grendel/data1/nov91/temp/temp.ps"	;the output filename
 ;; a list of targets
 :target-list '((dummy-1  208.7867 21.9992)
                (dummy-2  244.9089 19.8703)
                (dummy-3  256.1133 38.2085)
                )
 :start-date '1-jan-1992		;plot start date
 :end-date '1-mar-1992			;plot end date
 :orbit-model "XTE"			;orbit model name
 )

Or, from a .targ file:

(GENERATE-XTE-ORB-VIEWING-PLOTS
 "clipper:1xte-ev2.ps" ; "/grendel/data1/nov91/temp/temp.ps"	;the output filename
 :target-file "clipper:spike:obs:xte-test.targ" ;"/grendel/data1/nov91/obs/xte.targ"
 :start-date '1-jan-1992		;plot start date
 :end-date '1-mar-1992			;plot end date
 :orbit-model "XTE"			;orbit model name
 )

Note:  target list is of the form: '((targname ra dec exptime)...)
where targname is a symbol used for name, ra and dec are in decimal
degrees, and exptime is ignored here (as are any later elements in the
list.  Target file is .targ file with forms like 
(def-xte-exposure :name U_GEM      :ra 118.7867 :dec  21.9992 :exp-time   40000)


Plot detailed constraints for one target (full page per target):

(PLOT-XTE-TARGET-CONSTRAINTS 
 :name 'dummy-1  
 :ra 208.7867 
 :dec 21.9992
 :start-date '1-jan-1992
 :end-date '1-mar-1992
 :orbit-model "XTE"		 
 :file "clipper:1xte-ev3.ps" ; "/grendel/data1/nov91/temp/ev.ps"
 )

======================================================================|#


(defun GENERATE-XTE-ORB-VIEWING-PLOTS
    (file &key target-list target-file start-date end-date orbit-model)

  "Given file name, target-list (list of (name ra dec ...)), start-date,
    end-date, and orbit model, plot orb-viewing constraint to
    postscript file. "

  (let* ((gp (make-ps-gwindow :file file))
	 (EEC (current-XTE-event-calc))
	 ;; page loc of rect left, right: same throughout
	 (pleft 60) (pright 550)
	 ;; window loc left right: same throughout
	 (wleft 0.1) (wright 0.98)
	 ;; current page top bottom
	 (initial-page-top 700)
	 (ptop nil) 
	 (pbottom initial-page-top) ;; this is the initial value of page top
	 )
    
    ;; get target list from file if not provided
    (cond ((and target-list (not target-file))) ;ok, use the list
	  ((and (not target-list) target-file)
	   (setf target-list
	     (target-list-from-XTE-targ-file target-file))
	   (format t "~%loaded ~d targets from ~a" 
		   (length target-list) target-file))
	  (t (error "either target-list or target-file must be specified, not both")))
    
    ;; setup
    (record-prolog gp)

    ;; PLOT HEADER
    (plot-XTE-page-header 
     :where gp :page-left pleft :page-right pright
     :page-bottom initial-page-top :page-top (+ initial-page-top 50)
     :filename target-file)
    
    (dolist (target target-list)
      (let ((name (first target))
	    (ra (second target))
	    (dec (third target)))
	  
	(set-XTE-constraint-parameters
	 :name name :ra ra :dec dec 
	 :start-date start-date :end-date end-date
	 :orbit-model orbit-model)
    
	;; 
	(setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	(xte-event-calc-pcf-plot
	 EEC :where gp :pcf (get-XTE-orbital-viewing-pcf)
	 :label-string "orbital viewing"
	 :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	 :window-left wleft :window-right wright)
	  
	  
	(when (< pbottom 100)

	  ;; DRAW TIME AXIS OVER WHOLE PAGE
	  (set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
	  (set-loc-of-drawing-window gp wleft wright 0 1)
	  (time-label gp (start-date EEC) (end-date EEC) :label t
		      :min-pixels-for-tics 5
		      :min-pixels-for-labels 20)
	  
	  ;; new page
	  (record-nextpage gp)
	  ;; header on new page
	  (plot-XTE-page-header 
	   :where gp :page-left pleft :page-right pright
	   :page-bottom initial-page-top :page-top (+ initial-page-top 50))
	  
	  (setf ptop nil pbottom initial-page-top)
	  )))

    ;; DRAW TIME AXIS OVER WHOLE PAGE
    (set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
    (set-loc-of-drawing-window gp wleft wright 0 1)
    (time-label gp (start-date EEC) (end-date EEC) :label t
		      :min-pixels-for-tics 5
		      :min-pixels-for-labels 20)
      
    (record-wrapup gp)))


(defun TARGET-LIST-FROM-XTE-TARG-FILE (targ-file)

  "Input a filename for a file containing a list of def-...-exposure
    forms like:
     (def-XTE-exposure :name U_GEM :ra 118.7867 :dec  21.9992 :exp-time   40000)
    Extract :name, :ra, and :dec values and return in a list of the form:
     ((name1 ra1 dec1)(name2 ra2 dec2)...)"

  (let ((result nil))
    (with-open-file (stream targ-file
		     :direction :input
		     :if-does-not-exist :error)
      (loop
	  (let ((form (read stream nil nil)))
	    (if (not form) (return nil))
	    (let ((name (getf (rest form) :name))
		  (ra (getf (rest form) :ra))
		  (dec (getf (rest form) :dec)))
	      (cond ((and name ra dec)
		     (push (list name ra dec) result))
		    (t (spike-diagnostic
			"unrecognized target form: ~a" form))))))
      (nreverse result))))

(defun PLOT-XTE-PAGE-HEADER
    (&key where page-left page-right page-bottom page-top filename)
  ;; page-left page-right page-bottom page-top
  (set-page-loc-of-drawing-rect where page-left page-right page-bottom page-top)
  ;; window-left window-right window-bottom window-top
  (set-loc-of-drawing-window where  0 1 0 1)
  (record-font where "Helvetica-Bold" 9)
  (string-at-line-in-rect 
   where  :line-number 1 :font-size 9
   :string 
   (with-output-to-string 
       (stream)
     (format stream "XTE target suitability ~a" (get-formatted-universal-time))
     (if filename (format stream " ~a" filename))
     (format stream ", p. ~a" (current-page-number where))))
  (record-font where "Helvetica-Oblique" 7)
  (string-at-line-in-rect 
   where  :line-number 2 :font-size 9
   :string "Spike/Space Telescope Science Institute")
  
  )


(defun XTE-EVENT-CALC-PCF-PLOT (EC &key pcf where label-string				      
				       page-left page-right page-bottom page-top
				       window-left window-right)
  
  "Plot input pcf. Label with target name, etc. from EC.  Size of 50 works well."

  (event-plot
   EC :where where
   :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
   :window-left window-left :window-right window-right :window-bottom 0.07 :window-top 0.82
   :pcf pcf :width 0.25
   :rect-title (let ((astro-object (astro-object EC)))
		 (format nil "~a  RA: ~,3f (~a) Dec: ~,3f (~a)  Ecl. long: ~,3f lat: ~,3f ~a"
			 (name EC)
			 (degrees (ra astro-object))
			 (let ((hms (degrees-of-ra-to-hms (degrees (ra astro-object)))))
			   (format nil "~dh ~2,'0dm ~,1fs" (first hms) (second hms) (third hms)))
			 (degrees (dec astro-object))
			 (let ((dms (degrees-of-dec-to-dms (degrees (dec astro-object)))))
			   (format nil "~dd ~2,'0dm ~ds" (first dms) (second dms) (round (third dms))))
			 (degrees (lambda-value astro-object))
			 (degrees (beta astro-object))
			 label-string))
   :rect-font-size 6
   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
  (label-y-axis where 0 1.1 :tic-value 0.1 :tic-size 1 :label nil)
  (label-y-axis where 0 1.1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
  )


;; save last computed sample lists for debugging
(defvar *XTE-sun-non-zero-intervals* nil)
(defvar *XTE-last-orb-samples* nil)
(defvar *XTE-last-sun-samples* nil)
(defvar *XTE-last-moon-samples* nil)


(defun PLOT-XTE-TARGET-CONSTRAINTS (&key name ra dec file start-date end-date orbit-model)

  "Plot constraints, ra and dec in degrees, start-date and end-date
    as standard times.  Target name, ra, and dec, start and end times
    are reset only if these arguments are supplied here.  
    If orbit-model is provided, then the orbital viewing parameters
    are reset to defaults as well.  Otherwise they are not changed."

  (when (and ra dec)
    (set-XTE-event-calc-pointing :name name :ra ra :dec dec :units :degrees
				  :observer-name "XTE" :observer *XTE*)
    (set-XTE-event-calc-roll :nominal))
  (when (and start-date end-date)
    (set-XTE-event-calc-dates :start start-date :end end-date))
  (when orbit-model
    (set-XTE-event-calc-orbital-viewing :orbit-model orbit-model))

  (let* (
	 (sun-samples (sun-sample-list (current-XTE-event-calc)))
	 (moon-samples (moon-sample-list (current-XTE-event-calc)))
	 (sun-pcf (sun-constraint-pcf (current-XTE-event-calc)))
	 (sun-intervals 
	  (pcf-non-zero-intervals sun-pcf (start-date (current-XTE-event-calc))
				  (end-date (current-XTE-event-calc))))
	 (list-of-orb-viewing-sample-lists
	  (make-list-of-orb-viewing-sample-lists
	   (current-XTE-event-calc) :non-zero-intervals sun-intervals
	   :sensor-name-list '(:STTA :STTB)))
	 )
    (setf *XTE-last-orb-samples* list-of-orb-viewing-sample-lists
	  *XTE-sun-non-zero-intervals* sun-intervals
	  *XTE-last-sun-samples* sun-samples
	  *XTE-last-moon-samples* moon-samples)
    (generate-XTE-constraint-plot
     (or file (string-downcase (format nil "/grendel/data1/~a.ps" name)))
     :orb-samples list-of-orb-viewing-sample-lists
     :non-zero-intervals sun-intervals
     :sun-samples sun-samples
     :moon-samples moon-samples)
    ))



(defun GENERATE-XTE-CONSTRAINT-PLOT
    (file &key orb-samples non-zero-intervals sun-samples moon-samples)
  
  
  "Given file name, and sun, moon, and orbital viewing sample lists,
    plot constraints to postscript file.  Use (current-event-calc)
    for constraint definitions."

  (let* ((gp (make-ps-gwindow :file file))
	 (EEC (current-XTE-event-calc))
	 ;; page loc of rect left, right: same throughout
	 (pleft 60) (pright 550)
	 ;; window loc left right: same throughout
	 (wleft 0.1) (wright 0.98)
	 ;; current page top bottom
	 (initial-page-top 700)
	 (ptop nil) 
	 (pbottom initial-page-top) ;; this is the initial value of page top
	 )
    
    ;; setup
    (record-prolog gp)


    ;; PLOT HEADER
    (plot-target-constraints-header
     EEC :where gp
     :page-left pleft :page-right pright :page-bottom initial-page-top :page-top (+ initial-page-top 50))
    
    ;; SUN
    (setf ptop pbottom pbottom (- pbottom 80)) ;number is height of rect
    (sun-angle-and-pcf-plot 
     EEC :where gp :sample-list sun-samples
     :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
     :window-left wleft :window-right wright)

    ;; MOON
    (setf ptop pbottom pbottom (- pbottom 80)) ;number is height of rect
    (moon-angle-and-pcf-plot 
     EEC :where gp :sample-list moon-samples
     :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
     :window-left wleft :window-right wright)

    (multiple-value-bind
	(;; dark vis time in days
	 dark-time-pcf 
	 ;; dark vis time, normalized to max=1
	 norm-dark-time-pcf 
	 ;; north dark vis time in days
	 north-dark-time-pcf)
	(XTE-dark-vis-pcf
	 EEC :non-zero-intervals non-zero-intervals
	 :list-of-orb-viewing-sample-lists orb-samples)
      
      
      (multiple-value-bind
	  (;; fraction of dark vis that both trackers useable
	   both-trackers-pcf
	   ;; fraction of dark vis that either tracker useable
	   either-tracker-pcf)
	  (XTE-tracker-pcf
	   EEC :non-zero-intervals non-zero-intervals
	   :list-of-orb-viewing-sample-lists orb-samples
	   :sensor1 :STTA :sensor2 :STTB)
	
	(let* ((total-tracker-pcf
		(pcf-combine both-trackers-pcf either-tracker-pcf
			     #'XTE-tracker-vis-to-suitability))
	       (total-north-pcf
		(pcf-combine dark-time-pcf north-dark-time-pcf
			     #'XTE-north-vis-suitability))
	       (total-pcf
		(pcf-multiply total-tracker-pcf
			      (pcf-multiply norm-dark-time-pcf total-north-pcf)))
	       (efficiency-pcf
		(XTE-efficiency-pcf
		 EEC :non-zero-intervals non-zero-intervals
		 :list-of-orb-viewing-sample-lists orb-samples)))

	  ;; DARK VIS and NORTH DARK VIS
	  (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.07 :window-top 0.82
	   :pcf (pcf-scalar-multiply north-dark-time-pcf 1440.0)
	   :width 0.25 :dash1 1 :dash2 0
	   :rect-title "Dark vis (solid), north dark vis (dotted), minutes" :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.07 :window-top 0.82
	   :pcf (pcf-scalar-multiply dark-time-pcf 1440.0)
	   :width 0.25 :y-scale-min 0 :y-scale-max 100)
	  (label-y-axis gp 0 100 :tic-value 10 :tic-size 1 :label nil)
	  (label-y-axis gp 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 

	  ;; VISIBILITY PCF
	  (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.10 :window-top 0.90
	   :pcf norm-dark-time-pcf
	   :width 0.25
	   :rect-title "Vis PCF" :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (label-y-axis gp 0 1 :tic-value 0.1 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 

	  ;; NORTH PCF
	  (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.10 :window-top 0.90
	   :pcf total-north-pcf
	   :width 0.25
	   :rect-title "North PCF" :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (label-y-axis gp 0 1 :tic-value 0.1 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 

	  ;; TRACKER PCF
	  (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.10 :window-top 0.90
	   :pcf total-tracker-pcf
	   :width 0.25
	   :rect-title "Tracker PCF" :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (label-y-axis gp 0 1 :tic-value 0.1 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 

	  ;; TOTAL PCF
	  (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.10 :window-top 0.90
	   :pcf total-pcf
	   :width 0.25
	   :rect-title "TOTAL PCF" :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (label-y-axis gp 0 1 :tic-value 0.1 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 

	  ;; EFFICIENCY
	  (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.07 :window-top 0.82
	   :pcf efficiency-pcf
	   :width 0.25
	   :rect-title (format nil "Efficiency (max: ~,3f)" 
			       (pcf-max efficiency-pcf (start-date EEC) (end-date EEC)))
	   :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (label-y-axis gp 0 1 :tic-value 0.1 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value 0.2 :tic-size 3 :label t :label-format "~,1f") 

	  ;; BOTH/EITHER TRACKER
	  (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.07 :window-top 0.82
	   :pcf both-trackers-pcf
	   :width 0.25 :dash1 1 :dash2 0
	   :rect-title "Tracker availability as fraction of dark vis: either (solid), both (dotted)"
	   :rect-font-size 6
	   :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
	  (event-plot
	   EEC :where gp
	   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
	   :window-left wleft :window-right wright :window-bottom 0.07 :window-top 0.82
	   :pcf either-tracker-pcf
	   :width 0.25 :y-scale-min 0 :y-scale-max 1.1)
	  (label-y-axis gp 0 1 :tic-value .10 :tic-size 1 :label nil)
	  (label-y-axis gp 0 1 :tic-value .20 :tic-size 3 :label t :label-format "~,1f") 

	  ;; TRACKERS
	  (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	  (dolist (sample-list orb-samples)
	    (tracker-sun-moon-plot EEC :where gp :sample-list sample-list :sensor :STTA
				   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
				   :window-left wleft :window-right wright))

	  (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
	  (dolist (sample-list orb-samples)
	    (tracker-sun-moon-plot EEC :where gp :sample-list sample-list :sensor :STTB
				   :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
				   :window-left wleft :window-right wright))

	  )))

    ;; DRAW TIME AXIS OVER WHOLE PAGE
    (set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
    (set-loc-of-drawing-window gp wleft wright 0 1)
    (time-label gp (start-date EEC) (end-date EEC) :label t
		:min-pixels-for-tics 5
		:min-pixels-for-labels 20)

    (record-wrapup gp)))

;;; ----------------------------------------------------------------------
;;;                                              STAR TRACKER PLOTS (XTE)
;;; ----------------------------------------------------------------------


(defun XTE-TRACKER-DARK-PCF-PLOT (EEC &key where sample-list sensor 
					     page-left page-right page-bottom page-top
					     window-left window-right)

  (let* ((suit (XTE-tracker-avail-dark-pcf EEC :sample-list sample-list
					    :sensor sensor)))
    (event-plot
     EEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
     :pcf suit :width 0.25
     :rect-title (format nil "~a" sensor) :rect-font-size 6
     :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
    (string-at-line-in-rect where :line-number 2 :font-size 6 :string "dark")
    (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
    ))


(defun XTE-BOTH-TRACKERs-DARK-PCF-PLOT (EEC &key where sample-list sensor1 sensor2 
						   page-left page-right page-bottom page-top
						   window-left window-right)
  (let ((suit (XTE-both-trackers-avail-dark-pcf EEC :sample-list sample-list
						 :sensor1 sensor1 :sensor2 sensor2)))
    (event-plot
     EEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
     :pcf suit :width 0.25
     :rect-title "Both trackers" :rect-font-size 6
     :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)

    (string-at-line-in-rect where :line-number 2 :font-size 6 :string "dark")
    (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
    ))


(defun XTE-EITHER-TRACKER-DARK-PCF-PLOT (EEC &key where sample-list sensor1 sensor2 
						    page-left page-right page-bottom page-top
						    window-left window-right)

  (let ((suit (XTE-either-tracker-avail-dark-pcf EEC :sample-list sample-list
						  :sensor1 sensor1 :sensor2 sensor2)))
    (event-plot
     EEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
     :pcf suit :width 0.25
     :rect-title "Either tracker" :rect-font-size 6
     :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
    (string-at-line-in-rect where :line-number 2 :font-size 6 :string "dark")
    (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
    ))

;;;-*- Mode:COMMON-LISP; Package: USER; Base: 10 -*-

;;; ======================================================================
;;; PACKAGE DEFINITION

(defpackage "CLIMIF"
  (:use "COMMON-LISP" "COMMON-LISP-USER")
  ;; (:nicknames "SCUIF")
  (:export

   ;; export symbols here
   abracadabra

   ))

;;; ======================================================================
;;; ROOT

(defvar climif::*clim-root* nil)

;;; hack: the following will croak if not on either Mac or Sun
;;; hack: how to specify another internet host?
(defun MAKE-CLIM-ROOT ()
  (setf climif::*clim-root* 
    (clim:open-root-window #+:MCL :MCL
			   #+:Allegro :CLX)))

(make-clim-root)

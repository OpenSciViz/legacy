BEGIN { commentStart = 0; }
{
   # take care of comments ;;;
   if (match($0, /^ *;/)) sub(/^ *;/,"#;");
   
   # take care of quotes
   if (match($0, /^ *`\(/)) sub(/`\(/, "\(quote ");
   gsub(/\ *`\(/, "\(quote ");
   
   gsub(/\'/, "quote? ");
   
   # signal vetors
   gsub(/\#\(/, "vector?");
   
   # function quote
   gsub(/\#`/,"function? ");
   
   # : access 
   gsub(/::/, "??");
   gsub(/:initform/, "ctor?");
   gsub(/:initarg/, "initarg?");
   gsub(/:accessor/, "accessor?");
   gsub(/:documentation/, "documentation?");
   
   gsub(/:/, "\-");
   gsub(/\?\?/, "::");
   
   # bit-vector 
   gsub(/\#\*/,"bitvector? ");

   # take care of "comments"
   x = gsub(/\x22/, "\x22");
   if (x==2) {
      $0 = "# " $0;
   } else {
      if (x==1) { $0 = "#" $0; }
      if ($0 ~ /^ *\x22/) commentStart = 1;
      if (commentStart) $0 = "#" $0; 
      if ($0 ~ /\x22.*$/) commentStart = 0;
   }
  
   # decapitalize and prevent moveing paramters out
   if ($0 ~ /defun/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defun/) {
	   y = array[(x+1)];
           $0 = tolower($0);
	   sub(/\(/,"");
	   gsub(/\(/, "\{");
	   gsub(/\)/, "\}");
	   $0 = $0 " {"
	   break;
	}
      }
   }
   if ($0 ~ /defmac/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defmac/) {
	   y = array[(x+1)];
           $0 = tolower($0);
	   sub(/\(/,"");
	   gsub(/\(/, "\{");
	   gsub(/\)/, "\}");
	   $0 = $0 " {"
	   break;
	}
      }
   }
   if ($0 ~ /define-modify-macro/) {
      # not used
   }
   if ($0 ~ /define-setf-method/) {
      # not used
   }
   if ($0 ~ /defclass/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defclass/) {
	   y = array[(x+1)];
           $0 = tolower($0);
	   sub(/\(/,"");
	   gsub(/\(/, "\{");
	   gsub(/\)/, "\}");
	   $0 = $0 " {"
	   break;
	}
      }
   }
   if ($0 ~ /defgeneric/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defgeneric/) {
	   y = array[(x+1)];
           $0 = tolower($0);
	   sub(/\(/,"");
	   gsub(/\(/, "\{");
	   gsub(/\)/, "\}");
	   $0 = $0 " {"
	   break;
	}
      }
   }
   if ($0 ~ /defmethod/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defmethod/) {
	   y = array[(x+1)];
           $0 = tolower($0);
	   sub(/\(/,"");
	   gsub(/\(/, "\{");
	   gsub(/\)/, "\}");
	   $0 = $0 " {"
	   break;
	}
      }
   }
   if ($0 ~ /defsetf/) {
   }
   if ($0 ~ /deftype/) {
   }
   if ($0 ~ /define-compiler-macro/) {
      # not used
   }
   if ($0 ~ /define-condition/) {
      # not used
   }
   if ($0 ~ /define-declaration/) {
      # not used
   }
   if ($0 ~ /define-method-combination/) {
      # not used
   }
   if ($0 ~ /definevar/) {
      # not used
   }
   if ($0 ~ /defvar/) {
   }
   if ($0 ~ /defconst/) {
   }
   if ($0 ~ /defpackage/) {
   }
   if ($0 ~ /defstruct/) {
     split($0, array); 
      for (x in array) {
        if (array[x] ~ /defstruct/) {
           $0 = tolower($0);
	   break;
	}
      }
   }
   
   # take care of (func x) -> func (x)
   while (match($0, /\( *[^ ]+/) != 0) {
     # print "$0 =", $0;
     # print "RSTART = ", RSTART, "RLENGTH = ", RLENGTH;
     s = substr($0, RSTART, RLENGTH);
     # print "s = ", s;
     sub(/\(/, "", s); k = s " \{";
     # print "k = ", k;
     sub(/\( *[^ ]+/, k, $0);
     # print "$0 =", $0;
   }
   
   # take care of ()
   gsub(/\(/, "\{");
   gsub(/\)/, "\}");
   
   print $0;
}

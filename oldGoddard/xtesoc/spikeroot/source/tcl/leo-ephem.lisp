#+:Allegro
(in-package USER)

(defvar *asc-node-min-spacing* 0.05)
(defvar *XTE-EPHEMERIS-MODEL* nil)
(defvar *FDF-MODEL-START* nil)
(defvar *FDF-MODEL-END* nil)
(defvar *XTE-EPHEMERIS-FILE-NAME* nil)
(defvar *XTE-EPHEMERIS-IN-USE* nil)

;;;=========================================================
;;; Load C++ code to handle FDF ephemeris file.

(load "fdfEphem.o"  :foreign-files 
      '("libmitutil.a" "librwtool.a"
	"libC.a") :unreferenced-lib-names 
	'("___sti__rwtimeio_cpp_asString_"
	  "___sti__rwdateio_cpp_asString_"))

(setq arg-lists
  (list (list 
	 'cpp-init-stream1
	 :entry-point "___sti_____stream_filebuf_c_last_op_"
	 :arguments nil
	 :return-type :void)
	(list 'cpp-init-stream2
	      :entry-point "___sti_____stream_cstreams_c_stdstatus_"
	      :arguments nil
	      :return-type :void)
	(list ' cpp-init-stream3
		:entry-point "___sti_____stream_stream_c_stdioflush_"
		:arguments nil
		:return-type :void)
    (list 'cpp-init-fread
	  :entry-point "___sti__cstrngio_cpp_readFile_"
	  :arguments nil
	  :return-type :void)
    (list  'cpp-init-rwtime
	   :entry-point "___sti__rwtime_cpp_compareTo_"
	   :arguments nil
	   :return-type :void)
    (list  'cpp-init-rwtimeio
	   :entry-point "___sti__rwtimeio_cpp_asString_"
	   :arguments nil
	   :return-type :void)
    (list  'cpp-init-rwdateio
	   :entry-point "___sti__rwdateio_cpp_asString_"
	   :arguments nil
	   :return-type :void)
    (list 'cpp-init-fdfEphem
	  :entry-point "___sti__fdfEphem_C___pl_"
	  :arguments nil
	  :return-type :void)
    (list 'set-orbit-file
	  :entry-point "_setOrbitFileTo__FPc"
	  :arguments '(string)
	  :return-type :integer)
    (list 'cget-ephemeris-quantum
	  :entry-point "_getEphemQuantum__Fv"
	  :arguments nil
	  :return-type :integer)
    (list 'get-five
	  :entry-point "_getFive__Fv"
	  :arguments nil
	  :return-type :integer)
    (list 'cget-incl
	  :entry-point "_getInclination__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-ra-asc
	  :entry-point "_getRaAscNode__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-nodal-regression
	  :entry-point "_getNodalRegression__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-semimajor-axis
	  :entry-point "_getSemimajorAxis__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-eccentricity
	  :entry-point "_getEccentricity__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-delta-apsides
	  :entry-point "_getDeltaApsides__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-anomalistic-period
	  :entry-point "_getAnomalisticPeriod__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-mean-anomaly
	  :entry-point "_getMeanAnomaly__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-true-anomaly
	  :entry-point "_getTrueAnomaly__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-ecc-anomaly
	  :entry-point "_getEccentricAnomaly__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-angle-of-perigee
	  :entry-point "_getAngleOfPerigee__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cget-last-epoch
	  :entry-point "_getLastEpoch__Fv"
	  :arguments nil
	  :return-type :double-float)
    (list 'cload-ephem-data
	  :entry-point "_loadEphemeris__Fi"
	  :arguments '(integer)
	  :return-type :integer)
    (list 'cget-ephem-data
	  :entry-point "_loadEphemerisForLisp__FiPd"
	  :arguments '(integer (array double-float))
	  :return-type :integer)
    (list 'ephem-point-count
	  :entry-point "_ephemPointCount__Fv"
	  :arguments nil
	  :return-type :integer)))
    
(ff:defforeign-list arg-lists)

;;; Call C++ static initialization functions, if not already loaded.
(cpp-init-stream1)
(cpp-init-stream2)
(cpp-init-stream3)
(cpp-init-fread)
(cpp-init-rwtime)
(cpp-init-rwtimeio)
(cpp-init-rwdateio)
(cpp-init-fdfEphem)

;;; ======================================================================
;;;                                      CLASS:  LOW-EARTH-ORBIT SATELLITE
;;;
;;; For XTE. Assumes an elliptical, uniformly regressing orbit given
;;; once each ephemeris-quantum by an ephemeris file.
;;; Interpolation between ephemeris points assumes is linear in polar
;;; coordinates - angle swept and length of radial vector are proportional
;;; to time elapsed.
;;;

(defclass LEO-SATELLITE-FROM-EPHEMERIS (LOW-EARTH-ORBIT-SATELLITE)
  ((ephemeris-quantum
    :initform 60
    :initarg :ephemeris-quantum
    :accessor ephemeris-quantum
    :documentation "Number of seconds between ephemeris points")
   (eccentricity
    :initform 0
    :initarg :eccentricity
    :accessor eccentricity)
   (angle-of-perigee
    :initform nil
    :initarg :angle-of-perigee
    :accessor angle-of-perigee)
   (mean-anomaly
    :initform :mean-anomaly
    :accessor mean-anom
    :documentation "Mean anomaly at epoch in degrees")
   (true-anomaly
    :initform :true-anomaly
    :accessor true-anom
    :documentation "True anomaly at epoch in degrees")
   (anomalistic-period
    :initform :anomalistic-period
    :accessor anomalistic-period
    :documentation "Rate of change of angle of perigee, degrees per day")
  (time-of-elements
   :initform :time-of-elements
   :accessor time-of-elements
   :documentation "Time of Keplerian elements in FDF file, in Julian days")
  (ephemeris
   :initform :ephemeris
   :accessor ephemeris
   :documentation "FDF ephemeris data for one week from time-of-elements")
  
  (last-distance
   :initform 6900
   :accessor last-distance
   :documentation "Distance from earth center in km on last ephemeris consultation")
  (last-speed
   :initform 6900
   :accessor last-speed
   :documentation "Speed in km/s on last ephemeris consultation")

  ))

(defun MAKE-LEO-SATELLITE-FROM-EPHEMERIS (file-name jd &key (just-params nil))
    (if (null file-name)
	(progn
	  (format t "~%SPIKE Error:Julian date ~a not covered by ephemeris"
		  jd)
	  (exit)))
    (load-ephemeris '(nil nil nil) 
		    file-name
		    :params-only just-params)
  (let* ((ephem (find-if #'(lambda (item) (equalp (car item) file-name))
			*XTE-FDF-EPHEMERIDES-LOADED*))
	(params (caddr ephem))
	(instance (make-low-earth-orbit-satellite
		   "No name yet"
		   (degrees (cadr (assoc 'inclination params)))
		   (cadr (assoc 'time-asc-node params))
		   (degrees (cadr (assoc 'ra-asc-node params)))
		   (degrees (cadr (assoc 'nodal-regression-rate params)))
		   (* 24 60                      ;; days converted to minutes 
		      (cadr (assoc 'nodal-period params)))	           
		   (cadr (assoc 'semimajor-axis params))
		   :type 'leo-satellite-from-ephemeris)))
    (setf (ephemeris-quantum instance)
      (cadr (assoc 'ephemeris-quantum params)))
    (setf (angle-of-perigee instance)
      (degrees (cadr (assoc 'angle-of-perigee params))))
    (setf (eccentricity instance) (cadr (assoc 'eccentricity params)))
    (setf (mean-anom instance)
      (degrees (cadr (assoc 'mean-anomaly params))))
    (setf (true-anom instance)
      (degrees (cadr (assoc 'true-anomaly params))))
    (setf (anomalistic-period instance) 
      (cadr (assoc 'anomalistic-period params)))
    (setf (time-of-elements instance)
      (cadr (assoc 'time-of-elements params)))
    (setf (ephemeris instance) (cadr ephem))
    (setf *asc-node-min-spacing* (* 0.9 (nodal-period instance)))

    instance))

(defmethod SHOW :after ((ELEOS LEO-satellite-from-ephemeris))
  (format t "~%LEO satellite from FDF ephemeris, FDF & eccentric parameters:")
  (format t "~% Ephemeris quantum: ~d seconds" (ephemeris-quantum ELEOS))
  (format t "~% Eccentricity: ~F " (eccentricity ELEOS))
  (format t "~% Angle of perigee ~F deg" (angle-of-perigee ELEOS))
  (format t "~% Mean anomaly at epoch: ~F deg" (mean-anom ELEOS))
  (format t "~% True anomaly at epoch: ~F deg" (true-anom ELEOS))
  (format t "~% Period from perigee to perigee: ~F days" 
	  (anomalistic-period ELEOS))
  (format t "~% Time at epoch: ~F JD" (time-of-elements ELEOS))
)
	  

(defmethod POSITION-VECTOR ((ELEOS LEO-satellite-from-ephemeris) jd)
  
  "Return unit position vector for satellite in celestial coords.
   This will set time, position, velocity, distance and speed
   instance variables."

   ;; if time is the same as last call
  (cond ((and (time-of-position ELEOS)
	      (= jd (time-of-position ELEOS)))

	 ;; then return previous position
	 (last-position-vector ELEOS))
	
	;; switch fdf files if necessary
	((fdf-includes-time ELEOS jd)
	 
	 ;; else recalculate
	 (let* ((ephem (ephemeris ELEOS))
		(ix (get-fdf-handle ephem jd)))
	   (setf (time-of-position ELEOS) 
	     (fdf-time ephem ix))
	   (setf (last-position-vector ELEOS)
	     (fdf-unit-position ephem ix))
	   (setf (last-distance ELEOS) 
	     (fdf-distance ephem ix))
	   (setf (last-velocity-vector ELEOS)
	     (fdf-unit-velocity ephem ix))
	   (setf (last-speed ELEOS)
	     (fdf-speed ephem ix))))    

	(t             ;; Time not covered by an fdf file.
	 (setf (time-of-position ELEOS) jd)
	 (setf (last-position-vector ELEOS)
	   (position-vector *XTE-CIRCULAR* jd))
	 (setf (last-distance ELEOS)
	   (distance *XTE-CIRCULAR* jd))
	 (setf (last-velocity-vector ELEOS)
	   (velocity-vector *XTE-CIRCULAR* jd))
	 (setf (last-speed ELEOS)
	   (velocity *XTE-CIRCULAR* jd))))
    ;; return position
      (last-position-vector ELEOS))

(defmethod DISTANCE ((ELEOS LEO-satellite-from-ephemeris) jd
		     &optional (print-steps nil))
  "return distance in km from center of earth"
  (declare (ignore  print-steps))
  (position-vector ELEOS jd)
  (last-distance ELEOS)
  )

(defmethod VELOCITY-VECTOR ((ELEOS LEO-satellite-from-ephemeris) jd)
  "return velcoity unit vector"
  (position-vector ELEOS jd)
  (last-velocity-vector ELEOS)
  )

(defmethod VELOCITY ((ELEOS LEO-satellite-from-ephemeris) jd)
  "return speed in km/sec"
  (position-vector ELEOS jd)
  (last-speed ELEOS)
  )

(defmethod TIME-OF-NEXT-ASC-NODE ((ELEOS leo-satellite-from-ephemeris) jd)

  "return time of first ascending node crossing before input jd"
  (cond ((fdf-includes-time ELEOS jd)
	 (let ((an (FDF-NEAREST-ASC-NODE (ephemeris ELEOS) jd)))
	   (cond ((< an jd)
		  (setq an (fdf-nearest-asc-node 
			    (ephemeris ELEOS)
			    (+ an (nodal-period ELEOS))))))
	   (if (equal an 0.0)
	       (time-of-next-asc-node *XTE-CIRCULAR* jd)
	   an)))
	(t (time-of-next-asc-node *XTE-CIRCULAR* jd)))
  )

(defmethod TIME-OF-PREV-ASC-NODE ((ELEOS leo-satellite-from-ephemeris) jd)

  "return time of first ascending node crossing before input jd"

  (cond ((fdf-includes-time ELEOS jd)
	 (let ((an (FDF-NEAREST-ASC-NODE (ephemeris ELEOS) jd)))
	   (cond ((> an jd)
		  (setq an (fdf-nearest-asc-node 
			    (ephemeris ELEOS)
			    (- an (nodal-period ELEOS))))))
	   (if (equal an 0.0)
	       (time-of-next-asc-node *XTE-CIRCULAR* jd)
	     an)))
	(t (time-of-prev-asc-node *XTE-CIRCULAR* jd)))
)

(defun APPEND-WITH-MIN-GAP (favored list1 list2)
  "Append lists of times. If distance between last of list1 and first of list2
   is significantly shorter than current nodal-period, drop time at tip
   of the less favored list."
  ;; "Favored" list will be the one with the more accurate ephemeris.
  (cond ((< (- (car list2) (car (last list1)))
	    *asc-node-min-spacing*)
	 (cond ((equalp favored 1)
		(setf list2 (cdr list2)))
	       ((equalp favored 2)
		(setf list1 (butlast list1))))))
  (append list1 list2))

(defmethod GET-FDF-SUBINTERVALS ((ELEOS leo-satellite-from-ephemeris)
			     jd1 jd2)
  "Given two Julian dates, break them down into contiguous intervals,
   each interval representing a week covered by an FDF ephemeris
   file, or a period uncovered by any ephemeris."
  
  ;; Returns a list of triples: interval start, interval stop, and
  ;; t or nil depending on whether the interval is covered by an
  ;; ephemeris file.
  (let* ((jd1-aux jd1)
	 (jd2-aux jd2)
	 )
    (do ()
	((or (fdf-includes-time ELEOS 
				(+ jd1-aux (/ 1 (* 24 60 60))))

	     ;; An interval doesn't overlap an FDF file if it contains
	     ;; only its final point, so add one second to jd1-aux.
	     ;; before testing
	     (> jd1-aux jd2))
	 
	 (cond ((equal jd1-aux jd1)
		;; case 1: jd1 falls within an fdf file range
		(setq jd2-aux (fdf-end-time (ephemeris ELEOS)))
		(if (< jd2-aux jd2)
		    (cons (list jd1 jd2-aux t)
			  (get-fdf-subintervals ELEOS jd2-aux jd2))
		  (cons (list jd1 jd2 t) '())))
	       ((fdf-includes-time ELEOS 
				   (+ jd1-aux (/ 1 (* 24 60 60))))
		;; case 2: jd1 outside fdf range, but (jd1, jd2) overlaps one.
		(setq jd1-aux (fdf-start-time (ephemeris ELEOS)))
		(setq jd2-aux (fdf-end-time (ephemeris ELEOS)))
		(if (< jd2-aux jd2)
		    (cons (list jd1 jd1-aux nil)
			  (cons (list jd1-aux jd2-aux t)
			  (get-fdf-subintervals ELEOS jd2-aux jd2)))
		  (cons (list jd1 jd1-aux nil)
			(get-fdf-subintervals ELEOS jd1-aux jd2))))
	       ;; case 3: (jd1, jd2) does not overlap any fdf file range.
	       (t
		(list (list jd1 jd2 nil)))))

      (setq jd1-aux (+ jd1-aux 7)))))
		      

(defmethod ASC-NODE-LIST ((ELEOS leo-satellite-from-ephemeris) tjd1 tjd2)
  "return list of ascending nodes between specified Julian dates"
  
  (let ((intervals (get-fdf-subintervals ELEOS tjd1 tjd2))
	(sublists nil)
	(an-list nil))
    (setq sublists
    (mapcar 
     #'(lambda (times)
	 (let ((jd1 (car times))
	       (jd2 (cadr times))
	       (has-file (caddr times)))
	   (cond (has-file
		  (fdf-includes-time ELEOS jd1)
		  (cons has-file
			(asc-node-list-from-fdf ELEOS jd1 jd2)))
		 (t 
		  (cons has-file
			(asc-node-list *XTE-CIRCULAR* jd1 jd2))))))
     intervals))
    (setq an-list (cdr (car sublists)))
    (setq sublists (cdr sublists))
    (dolist (item sublists an-list)
      (cond ((car item)
	     (setq an-list
	       (append-with-min-gap 2 an-list (cdr item))))
	    (t
	     (setq an-list
	       (append-with-min-gap 1 an-list (cdr item))))))
    ))
		
(defmethod ASC-NODE-LIST-FROM-FDF ((ELEOS leo-satellite-from-ephemeris)
				   jd1 jd2)
  "Caller guarantees jd1 and jd2 are within bounds of current FDF
   ephemeris file. Return list of ascending nodes between those times."
  
(do* ((t-asc-node (time-of-next-asc-node ELEOS jd1))
      (np (nodal-period ELEOS))
      (next-guess (+ t-asc-node np))
      (node-list (list t-asc-node))
      )
    ((> next-guess jd2)
     (if (equal (car node-list) 0.0)
	 (setq node-list (cdr node-list)))
     (reverse node-list))
  (setq next-guess (+ t-asc-node np))
  (setq t-asc-node (fdf-nearest-asc-node
		    (ephemeris ELEOS) next-guess))
  (if (equal t-asc-node 0.0)
      (setq t-asc-node next-guess)
    (push t-asc-node node-list))))

;;; Find nearest ascending node from ephemeris, given an initial guess.
;;; Decrement positive y, or increment negative y, until sign of y
;;; changes. Then interpolate.
;;; If nearest asc node precedes the ephemeris, return first asc node.
;;; If nearest asc node follows the ephemeris, return 0.
-
(defun FDF-NEAREST-ASC-NODE (ephem jd)
  "Julian date of asc node within ephemeris limits nearest to jd."

  ;;; If jd is less than half a period from ends of interval, 
  ;;; advance or retard to stay within range.
#|
  (let ((jd-aux nil) 
	(semi-np (/ (nodal-period *XTE*) 2))
	(time-aux nil))
    (cond ((< jd (+ (fdf-start-time ephem) semi-np .0000001))
	   (setq jd-aux (+ jd semi-np))
	   (setq time-aux (fdf-nearest-asc-node ephem jd-aux))
	       (return-from fdf-nearest-asc-node time-aux))
	  ((> jd (- (fdf-end-time ephem) (+ semi-np .0000001)))
	   (setq jd-aux (- jd semi-np))
	   (setq time-aux (fdf-nearest-asc-node ephem jd-aux))
	       (return-from fdf-nearest-asc-node time-aux))))
	       |#
;;; If too close to either end of range to set up initial indices, 
;;; return Jdate 0.
  (let ((ix (get-fdf-handle ephem jd)))
    (if (or (and (equal (car ix) 0)
		 (> (aref ephem 3) 0))
	    (and (equal (car ix) (- (length ephem) 9))
		 (< (aref ephem (- (car ix) 6)) 0)))
	(return-from fdf-nearest-asc-node 0.0))
 
  (let* ((old-ix (+ (car ix) 3))
	 (new-ix (+ (car ix) 12))
	 (old-y (aref ephem old-ix))
	 (new-y (aref ephem new-ix))
	 (direction nil)
	 (pts 0)
	 (out-of-bounds nil))
    (cond ((< old-y 0)
	   (setq direction 1))
	  (t 
	   (setq direction -1)
	   (setq new-ix (- old-ix 9))
	   (setq new-y (aref ephem new-ix))))
    
    (do ()
	((< (* new-y old-y) 0)
	 (if out-of-bounds
	     (return-from fdf-nearest-asc-node 0.0))
	 (let ((old-t nil)
	       (new-t nil)
	       (frac nil))
	   (cond ((> new-ix old-ix)
		  (setq new-ix (- new-ix 3))
		  (setq old-ix (- old-ix 3)))
		 (t
		  (setq old-ix (- new-ix 3))
		  (setq new-ix (+ new-ix 6))))
	   (setq old-t (aref ephem old-ix))
	   (setq new-t (aref ephem new-ix))
	   (setq frac (/ (abs (aref ephem (+ old-ix 3)))
			 (+ (abs (aref ephem (+ old-ix 3)))
			    (abs (aref ephem (+ new-ix 3))))))
	   (+ old-t (* frac (- new-t old-t)))))
      (setq new-ix (+ new-ix (* 9 direction)))
      (setq old-ix (+ old-ix (* 9 direction)))
      (cond ((or (< new-ix 0) (> new-ix (length ephem)))
	     (setq out-of-bounds t)
	     (setq new-y (* -1 old-y)))
	    (t
	     (setq old-y (aref ephem old-ix))
	     (setq new-y (aref ephem new-ix))
	     (setq pts (+ 1 pts))))
      ))))
	   
		 
(defvar *XTE-FDF-EPHEMERIDES-LOADED*)
(setq *XTE-FDF-EPHEMERIDES-LOADED* nil)
(defvar *XTE-CURRENT-EPHEMERIS*)
(setq *XTE-CURRENT-EPHEMERIS* (list "fdf/X1996017EPHSH.cur" nil nil))

;;; Functions to retrieve data from an fdf ephemeris

(defun GET-FDF-HANDLE (ephem jd)
  "Return a handle to the records in the ephemeris table that describe orbit
   state at julian date jd. Handle may then be used to access specific state
   elements"
  ;;; The handle is actually a list of
  ;;;     index to julian date of preceding ephemeris point
  ;;;     index to julian date of following ephemeris point
  ;;;     ratio of (jd - preceding time) to (following time - preceding)
  ;;; When jd exactly matches an ephemeris point, the first two
  ;;;     items on the list are equal, and the ratio is 0.
  ;;; Access functions will interpolate between the two using the ratio.
  (cond 
   ((< jd (aref ephem 0))
    '(0 0 0))
   ((> jd (aref ephem (- (length ephem) 9)))
    (let ((last-t (- (length ephem) 9)))
      (list last-t last-t 0)))
   (t                             ;; jd within ephemeris bounds

  (do*
      ((row-sz 9) ;; number of data elements per ephemeris point
       (found nil)
       (low-ix 0)
       (high-ix (- (length ephem) (- row-sz 1)))
;;       (last-low (aref ephem low-ix))	 ;;; lower limit for binary search
;;       (last-high (aref ephem high-ix))
       (ix (* row-sz (floor (/ (length ephem) (* 2 row-sz)))))
       (incr 0))
	(found
	  (cond ((< jd (aref ephem ix))
		 (list (- ix row-sz) 
		       ix 
		       (/ (- jd (aref ephem (- ix row-sz)))
			  (- (aref ephem ix)
			     (aref ephem ( - ix row-sz))))))
		((eql jd (aref ephem ix))
		 (list ix ix 0))
		(t 
		 (list ix 
		       (+ ix row-sz)
		       (/ (- jd (aref ephem ix))
			  (- (aref ephem (+ ix row-sz))
			     (aref ephem ix)))))))

    (cond ((< jd (aref ephem ix))     ;; time at ix later than desired
	     (cond ((< row-sz (- ix low-ix))
		    (setq high-ix ix)
		    (setq incr
		      (* row-sz
		      (floor (/ (- ix low-ix) (* 2 row-sz)))))
		    (setq ix (- ix incr)))
		   (t (setq found t))))
	  ((eql jd (aref ephem ix))
	   (setq found t))
	  (t                         ;; time at ix is earlier than desired
	   (cond ((< row-sz (- high-ix ix))
		  (setq low-ix ix)
		  (setq incr
		    (* row-sz
		    (floor (/ (- high-ix ix) (* 2 row-sz)))))
		  (setq ix (+ ix incr)))
		 (t (setq found t)))))))
   )
)

(defun FDF-TIME (ephem ix)
  "Extract the Julian date of an FDF ephemeris point given handle"
  (let* ((lo (car ix))
	 (hi (cadr ix))
	 (low-t (aref ephem lo))
	 (hi-t (aref ephem hi))
	 (frac (caddr ix))
	 )
    (+ low-t (* frac (- hi-t low-t)))
    ))
	 
(defun FDF-DISTANCE (ephem ix)
  "Extract satellite distance from earth center from loaded FDF ephemeris"
  (let* ((lo (car ix))
	 (hi (cadr ix))
	 (low-d (aref ephem (+ lo 1)))
	 (hi-d (aref ephem (+ hi 1)))
	 (frac (caddr ix))
	 )
    (+ low-d (* frac (- hi-d low-d)))
    ))

(defun FDF-UNIT-POSITION (ephem ix)
  "Extract satellite's unit position vector from loaded FDF ephemeris
   given handle"
  (let* ((lo (car ix))
	 (hi (cadr ix))
	 (low-p  (list (aref ephem (+ lo 2))
		      (aref ephem (+ lo 3))
		      (aref ephem (+ lo 4))))
	 (hi-p  (list (aref ephem (+ hi 2))
		      (aref ephem (+ hi 3))
		      (aref ephem (+ hi 4))))
	 (frac (caddr ix))
	 (pos nil)
	 )
    (setq pos
      (mapcar #'(lambda (x y) (+ x (* frac (- y x))))
	      low-p hi-p))
    (3vect-normalize pos)
    ))

(defun FDF-SPEED (ephem ix)
  "Extract satellite's speed from loaded FDF ephemeris given handle"
  (let* ((lo (car ix))
	 (hi (cadr ix))
	 (low-s (aref ephem (+ lo 5)))
	 (hi-s (aref ephem (+ hi 5)))
	 (frac (caddr ix))
	 )
    (+ low-s (* frac (- hi-s low-s)))
  ))

(defun FDF-UNIT-VELOCITY (ephem ix)
  "Extract satellite's unit velocity from loaded FDF ephemeris given handle"  
  (let* ((lo (car ix))
	 (hi (cadr ix))
	 (low-v  (list (aref ephem (+ lo 6))
		      (aref ephem (+ lo 7))
		      (aref ephem (+ lo 8))))
	 (hi-v  (list (aref ephem (+ hi 6))
		      (aref ephem (+ hi 7))
		      (aref ephem (+ hi 8))))
	 (frac (caddr ix))
	 (vel nil)
	 )
    (setq vel
      (mapcar #'(lambda (x y) (+ x (* frac (- y x))))
	      low-v hi-v))
    (3vect-normalize vel)
))

(defun LOAD-EPHEMERIS (ephem file-name &key (params-only nil))
  (cond
   ((find-if #'(lambda (item) (equalp (car item) file-name))
	     *XTE-FDF-EPHEMERIDES-LOADED*)
       nil)
   (t     
    (set-orbit-file file-name)	;;; foreign function to set file, read headers
    (cload-ephem-data 1)	       ;;; ff to load data into C++
    (let ((point-count (ephem-point-count))
	  (params nil)
	  (delta-apsides (cget-delta-apsides))
	  (anomalistic-period (cget-anomalistic-period))
	  (time-of-elements (cget-last-epoch))
	  (eccentricity (cget-eccentricity))
	  (angle-of-perigee (cget-angle-of-perigee))
	  (semimajor-axis (cget-semimajor-axis))
			    )
	(setf (car ephem) file-name)
	(setf (cadr ephem) 
	  (make-array (* point-count 9) 
		      :element-type 'double-float
		      :initial-element 0.0d0))
	(if (not params-only)
	    (cget-ephem-data point-count (cadr ephem)))
	(setq params
	  (list
	   `(inclination ,(cget-incl))
	   `(ra-asc-node ,(cget-ra-asc))
	   `(nodal-regression-rate ,(cget-nodal-regression))
	   `(semimajor-axis ,semimajor-axis)
	   `(ephemeris-quantum ,(cget-ephemeris-quantum))
	   `(eccentricity ,eccentricity)
	   `(angle-of-perigee ,angle-of-perigee)
	   `(mean-anomaly ,(cget-mean-anomaly))
	   `(true-anomaly ,(cget-true-anomaly))
	   `(anomalistic-period ,anomalistic-period)
	   `(time-of-elements ,time-of-elements)
	   `(time-asc-node 
	     ,(time-asc-node time-of-elements 
			     eccentricity
			     (cget-ecc-anomaly)
			     angle-of-perigee
			     semimajor-axis
			     ))
	   `(nodal-period
	    ,(* anomalistic-period 
	       ( -  1 (* delta-apsides anomalistic-period 
			  (/ 0.5 (coerce pi 'double-float))))))
	   ))
	(setf (caddr ephem) params))
    (setq *XTE-FDF-EPHEMERIDES-LOADED*
      (cons ephem *XTE-FDF-EPHEMERIDES-LOADED*))
    (if params-only
	nil
      t))))

;;; To derive time of ascending node, determine time of perigee:
;;;   epoch + (E - e * sin E) * (sqrt a^3 / mu)
;;; where E is the eccentric anomaly at epoch, e the eccentricity,
;;; a the semimajor axis, and mu is G * mass-of-earth in km^3 / day^2.
;;;
;;; Then find the eccentric anomaly at the time of ascending node:
;;;   tan( Ean / 2) = (sqrt (1 - e) / (1 + e)) * tan ( Aop / 2)
;;; where Ean is to be found, and Aop is the angle of perigee at epoch.
;;;
;;; Finally the time of ascending node is:
;;;   T-0 + (Ean - e * sin Ean) * (sqrt a^3 / mu)
;;; where T-0 is the time of perigee.

(defun time-asc-node (time e ea aop a)
  (let ((time-of-perigee nil)
	(sqrt-a3-mu (sqrt (/ (* a a a) 2.9755408e+15 )))
	(ecc-factor (sqrt (/ (- 1 e) (+ 1 e))))
	(ea-time-asc-node nil)
	time-a-n
	)
    (setq time-of-perigee (+ time (* sqrt-a3-mu
				     (- ea (* e (sin ea))))))
    (setq ea-time-asc-node
      (* 2 (atan 
	    (* ecc-factor (tan (/ aop 2))))))
    (setq time-a-n
      (+ time-of-perigee (* sqrt-a3-mu
			    (- ea-time-asc-node
			       (* e ea-time-asc-node)))))
  time-a-n)
)

;;; Return T if there's an FDF file for the time in question.
;;;  If so, switch the ephemeris if necessary.
;;; Return nil if we have to rely on the circular model.

(defmethod FDF-INCLUDES-TIME ((ELEO LEO-satellite-from-ephemeris) jd)
  (let ((fn (FDF-FILE-NAME-CONTAINING-JD jd)))
    (cond ((null fn) 
	   (update-xte-model ELEO jd) nil)
	  ((in-rangep ELEO jd) t)
	  (t 
	   (update-xte-model ELEO jd)
	   t))))

(defmethod IN-RANGEP ((LEO low-earth-orbit-satellite) jd)
	   (if (or (< jd *FDF-MODEL-START*)
		   (> jd *FDF-MODEL-END*))
	       t
	     nil))

(defmethod IN-RANGEP ((ELEO  LEO-SATELLITE-FROM-EPHEMERIS) jd)
  (and (>= jd (FDF-START-TIME (ephemeris ELEO)))
       (<= jd (FDF-END-TIME (ephemeris ELEO)))))

(defun FDF-START-TIME (ephem)
  (aref ephem 0))

(defun FDF-END-TIME (ephem)
  (aref ephem (- (length ephem) 9)))

(defun FDF-FILE-NAME-CONTAINING-JD (jd)
  (if (and (>= jd *FDF-MODEL-START*)
	   (<= jd *FDF-MODEL-END*))
      (progn 
	(setf *XTE-CURRENT-EPHEMERIS* *XTE-EPHEMERIS-MODEL*)
	*XTE-EPHEMERIS-FILE-NAME*)
    (progn 
      (setf *XTE-CURRENT-EPHEMERIS* nil)
      nil)))

#|
(defun FDF-FILE-NAME-CONTAINING-JD (jd)
  "Return name of FDF ephemeris file covering Julian Date jd.
  If no such file known, return nil."

  (let* ((fname nil)
	 (dmy nil)
	 (day-of-year nil)
	 (the-day (day-of-week jd))
	 ;; NOTA BENE! CHANGE NEXT LINE IF FDF FILES DON'T START ON
	 ;; MONDAYS! (Better yet, introduce a global variable for it.)
	 (fdf-week-start (- jd 6)))
    (do ((found nil))
	((or found (< jd fdf-week-start)) fname)
      (setq dmy (jdut jd))
      (setq day-of-year 
	(floor (day-month-year-to-day-of-year
		(car dmy) (cadr dmy) (caddr dmy))))
      (setq fname
	(concatenate 'string "fdf/X" 
		     (princ-to-string (caddr dmy))
		     (cond ((> 10 day-of-year)
			    "00")
			   ((> 100 day-of-year)
			    "0")
			   (t ""))
		     (princ-to-string day-of-year)
		     "EPHSH.cur"))
      (setq jd (- jd 1))
      (if (probe-file fname)
	  (progn (setf *XTE-CURRENT-EPHEMERIS* fname)
		 (setq found t))
	(progn (setf *XTE-CURRENT-EPHEMERIS* nil)
	  (setq fname nil)))))
  )
|#

(defmethod UPDATE-XTE-MODEL ((LEO low-earth-orbit-satellite) jd)
  (cond ((in-rangep LEO jd) t)
	(t 
	 (cond ((equalp (type-of LEO) 'LEO-satellite-from-ephemeris)
		;; the current model comes from an ephemeris
		(setf *XTE* *XTE-CIRCULAR*)
		(setf *XTE-EPHEMERIS-IN-USE* nil))
	       (t
		;; the current model is a circular orbit
		(setf *XTE* *XTE-EPHEMERIS-MODEL*)
		(setf *XTE-EPHEMERIS-IN-USE* t))))
	))

(defun INITIALIZE-EPHEMERIS-MODEL (file-name)
  "Set global ephemeris model and circular model from the given ephemeris
   file. Return t on success, NIL if file not found."
  (let* ((path (concatenate 'string
		"fdf/" file-name))
	   (file-start (ephem-name-to-time path)))
    (if (probe-file path)
	(progn
	  (setf *XTE-EPHEMERIS-MODEL* 
	    (make-LEO-satellite-from-ephemeris path file-start))
	  (setf *XTE-EPHEMERIS-FILE-NAME* file-name)
	  (setf *FDF-MODEL-START* 
	    (fdf-start-time (ephemeris *XTE-EPHEMERIS-MODEL*)))
	  (setf *FDF-MODEL-END* 
	    (fdf-end-time (ephemeris *XTE-EPHEMERIS-MODEL*)))
	  (setf *XTE-EPHEMERIS-IN-USE* t)
	  (let* ((model *XTE-EPHEMERIS-MODEL*)
		 (period (nodal-period model)))
	  (setf *XTE-CIRCULAR* 
	    (make-low-earth-orbit-satellite	      
	     "No name yet"
	     (degrees (inclination model))
	     ;; back up over half a year, so all of AO period will be covered.I
	     (- (time-of-asc-node model)
		      (* period 16 200))
	     (degrees (ra-of-asc-node model))
	     (degrees (nodal-regression-rate model))
	     (* 24 60                      ;; days converted to minutes 
		period)	           
	     (semimajor-axis model)
	     :type 'low-earth-orbit-satellite)))
	  t)
      nil)))

(defun EPHEM-NAME-TO-TIME (file-name)
  "Given a file name in the form 'fdf/XyyyydddEPHSH.cur', return
   the tjd for the start of the given day of the given year."
  (let* ((year (read-from-string (subseq file-name 5 9)))
	(day (read-from-string (subseq file-name 9 12)))
	 (dmy (day-of-year-to-day-month-year day year)))
    (format t "~%Year ~a, Day ~a, Dmy ~a" year day dmy)
     (ymdhms-to-jd 
		(third dmy)
		(second dmy)
		(first dmy)
		0.0 0.0 0.0))
  )



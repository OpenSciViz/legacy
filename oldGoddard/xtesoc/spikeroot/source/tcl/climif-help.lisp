;;;-*-Mode:LISP; Syntax: Common-Lisp;Package: climif;-*-
;;;
;;; climif-help.lisp
;;;
;;; creation: 7/6/92 MDJ
;;;
;;; CLIM help browser application (simple hypertext)
;;;
#| TODO:
-- hook for arbitrary function for output in help: e.g. to draw diagrams,
   etc.
-- ability to jump to another help dialog

|#

;;; ======================================================================
;;;                                                                PACKAGE
(eval-when (compile load eval)
  (in-package climif))

;;; ======================================================================
;;;                                      C L I M I F - H E L P   C L A S S

(defclass CLIMIF-HELP ()
  ((help-title 
    :initform "???" :initarg :title
    :documentation "string describing help area")
   (help-width
    :initform 50 :initarg :width
    :documentation "width of help text when output, in characters")
   (help-table 
    :initform (make-hash-table :test #'equal)
    :documentation
    "help-table: key is string for topic, value is list of strings that are
      the help. Starter is the string 'Introduction'")
   (help-is-cross-referenced-p
    :initform nil 
    :documentation
    "t if help table is cross-referenced, nil otherwise. To cross-ref, call
      the cross-reference method.  Adding new text will reset this flag to nil")
   (all-help-topics 
    :initform nil
    :documentation 
    "list of help topics, as strings")
   ))

#|======================================================================
Input help format:

Topic-and-text consists of:
("topic" "text line 1" "text line 2" ...)
where: "text line ..." will be broken up into words at space, tab, and
return boundaries.  Instead of a string, the following are allowed:
  (:ref "string") - include a cross reference to the specified topic
  :paragraph - start a new paragraph
  "" or " " - null or blank string means paragraph break when printing help text
  
In the help-table, the input lines are broken into words for output, but
otherwise have the saem form as input.
Thus in the hash-table, the key is the topic, the value will be a list
of:
 - strings to put out as words
 - the keyword :PARAGRAPH indicating a paragraph break
 - the construct (:REF "string") where "string" is a cross-reference
   to another help topic
======================================================================|#

(defmethod ADD-HELP-TOPIC ((c CLIMIF-HELP) topic help-strings &key (verbose nil))
  ;; topic is a string, help-strings is a list of strings
  (with-slots (help-table help-is-cross-referenced-p all-help-topics) c
    (setf help-is-cross-referenced-p nil)
    (pushnew topic all-help-topics :test #'string-equal)
    (dolist (s help-strings)
      (cond ((stringp s)
             (let ((ds (help-string-to-word-list s)))
               ;;(format t "~%  appending: ~s" (or ds '(:PARAGRAPH)))
               (setf (gethash topic help-table)
                     (append (gethash topic help-table)
                             (or ds '(:PARAGRAPH))))))
            ((and (listp s) (eq (first s) :REF))
             (setf (gethash topic help-table)
                   (append (gethash topic help-table) (list s))))
            ((and (keywordp s) (eq s :PARAGRAPH))
             (setf (gethash topic help-table)
                   (append (gethash topic help-table) '(:PARAGRAPH))))
            (t 
             (if verbose
               (format t "~%Warning: can't interpret <~s> as help text" s)))))))


(defmethod RECORD-HELP ((c CLIMIF-HELP) topics-and-text &key (verbose nil))
  "record a list of topics and text, i.e.
   ((\"topic1\" \"line1\" ...)(\"topic2\" \"line2\" ...))"
  (dolist (tt topics-and-text)
    (add-help-topic c (first tt) (rest tt) :verbose verbose)))


(defmethod CROSS-REFERENCE-HELP ((c CLIMIF-HELP))
  (with-slots 
    (help-table help-is-cross-referenced-p
                all-help-topics) c
    (setf all-help-topics (sort all-help-topics #'string-lessp))
    (maphash #'(lambda (key val)
                 (let ((xref-val nil))
                   (dolist (item val)
                     (cond ((or (listp item)
                                (eq item :paragraph))
                            (push item xref-val))
                           ((gethash item help-table)
                            (push (list :ref item) xref-val))
                           (t (push item xref-val))))
                   ;;(format t "~%storing: ~a" (reverse xref-val))
                   (setf (gethash key help-table)
                         (reverse xref-val))))
             help-table)
    (setf help-is-cross-referenced-p t)
    ))



(defmethod TEST-HELP ((c CLIMIF-HELP))
  (with-slots (help-table help-title help-width) c
    (format t "~%Help for: ~a" help-title)
    (format t "~%~%Intro: ~a"
            (if (gethash "Introduction" help-table) "Yes" "*** NO ***"))
    (maphash #'(lambda (key val)
                 (format t "~%~%Topic: ~a~%" key)
                 (let ((line 0))
                   (dolist (word val)
                     (cond ((eq word :PARAGRAPH)
                            (terpri) (terpri) (setf line 0))
                           ((and (listp word) (eq (first word) :REF))
                            (format t "{~a}" (second word))
                            (incf line (+ (length word) 2)))
                           ((gethash word help-table)
                            (format t "<~a>" word)
                            (incf line (+ (length word) 2)))
                           (t (write-string word)
                              (incf line (length word))))
                     (cond ((> line help-width) 
                            (terpri)
                            (setf line 0))
                           ((not (eq word :PARAGRAPH))
                            (write-string " "))))))
             help-table)))


(defmethod CLEAR-HELP ((c CLIMIF-HELP))
  (with-slots (help-table all-help-topics help-is-cross-referenced-p) c
    (clrhash help-table)
    (setf all-help-topics nil)
    (setf help-is-cross-referenced-p nil)))

(defmethod FIND-TOPICS-MENTIONING ((c CLIMIF-HELP) string)
  (let ((result nil)
        )
    (with-slots (help-table) c
      (maphash #'(lambda (key val)
                   (if (find-if 
                        #'(lambda (x) 
                            (cond ((stringp x) (search string x 
                                                       :test #'char-equal))
                                  ((and (listp x) (eq (first x) :ref))
                                   (search string (second x)
                                           :test #'char-equal))
                                  (t nil)))
                        val)
                     (push key result)))
               help-table))
    (sort result #'string-lessp)))
    
  
#| TEST:
(setf hhh2 (make-instance 'CLIMIF-HELP :title "Demo help 2" :width 40))
(clear-help hhh2)
(record-help
 hhh2
 '(
   (
    "Introduction"
    "This is help about poetry." :paragraph
    "There was an old man from Brite"
    "Whose age was not known tonight"
    "He fell on his head" "and started writing poetry"
    )
   (
    "tree"
    "I think that I shall never see, a poem as lovely as a tree"
    "Star bright, star light, first star I see tonite"
    )
   (
    "poem"
    "This is not one.  Shall I repeat that? This is not one."
    "This is not one.  This is not one.  This is not one.  This is not one.  "
    :PARAGRAPH "This is new paragraph"
    "This is not one.  This is not one.  This is not one.  This is not one.  ")
   ("tonite"
    "That is this nite, or rather night")
   ("tonight"
    "see" (:ref "tonite"))
   ("poesie"
    "see" (:ref "poem")))
 )
(cross-reference-help hhh2)
(test-help hhh2)
(find-topics-mentioning hhh2 "lovely")
(find-topics-mentioning hhh2 "poem")
(find-topics-mentioning hhh2 "POEM")

(setf hhh (make-instance 'CLIMIF-HELP :title "Demo help" :width 30))
(clear-help hhh)
(record-help
 hhh
 '(("topic-a"
    "Now is the time for all good men topic-b to come"
    "to the aid of their topic-c"
    "and so much for this help stuff."
    "" ; para
    "Second paragraph on topic-a, " (:ref "special")
    "this is the second paragraph"
    "you know")
   ("topic-b"
    "Here's the second topic, which has references to topic-a"
    "and to topic-c in the text, and so on and so forth etc. etc.")
   ("topic-c"
    "This is actually topic-c, but contains references to topic-a"
    "and to topic-b as well as to now is the time for all good men to come"
    "to the aid of their country")
   ("Introduction"
    "This is the introduction to Help: click on any illuminated topic"
    "to get additional help on that topic. For example, topic-a has help,"
    "as does topic-b")
   )
 :verbose t)
(test-help hhh)
(setf (slot-value hhh 'help-width) 40)
(test-help hhh)
(cross-reference-help hhh)
(describe hhh)
|#

#| test:
(HELP-STRING-TO-WORD-LIST "Now is the time for all
 good men to come to the
aid of their country.")
(HELP-STRING-TO-WORD-LIST " ")
|#

#| 
----------------------------------------------------------------------
Quotify file

This will take a text file and put quotes on each line
from which it can be edited into the form of a help file.

(let ((infile "spiketest.tmp")
      (outfile "q.tmp"))
  (with-open-file (instream infile :direction :input)
    (with-open-file (outstream outfile :direction :output)
      (loop
	(let ((line (read-line instream nil nil)))
	  (if (not line) (return nil))
	  (format outstream "~%   \"~a\"" line))))))

----------------------------------------------------------------------
|#

(defun HELP-STRING-TO-WORD-LIST 
       (str &optional (delimiters '(#\space #\tab #\return)))
  
  (let ((result nil)
        (substring-chars nil))
    
    (dotimes (i (length str))
      (let ((ch (elt str i)))
        (cond ((member ch delimiters)
               (if substring-chars
                 (push (map 'string #'identity (reverse substring-chars)) result))
               (setf substring-chars nil)
               )
              (t (push ch substring-chars)))))
    (if substring-chars
      (push (map 'string #'identity (reverse substring-chars)) result))
    (reverse result)))

;;; ======================================================================
;;;                                         CLIM Definitions

(clim:define-presentation-type HELP-TOPIC ())

(clim:define-presentation-method clim:present (object (type HELP-TOPIC)
                                                      stream view &key)
  (declare (ignore view))
  (clim:with-text-face  #+(version>= 4 2) (stream :bold)
				  #-(version>= 4 2) (:bold stream)
    (write-string object stream)))
 

(clim:define-application-frame HELP-BROWSER
  ()
  ;; application state variables
  ((help-inst :initform nil)            ; instance of CLIMIF-HELP
   (current-topic :initform "Introduction")
   )
  ;; command table, definer
  
  (:panes
   ( (menu :command-menu #-(version>= 4 2) :default-text-style 
	  #+(version>= 4 2) :text-style '(:sans-serif :roman :normal))
    (show-help :application 
               :scroll-bars :both
               :display-function 'display-help
               :display-after-commands nil
               :incremental-redisplay nil
	       #-(version>= 4 2) :default-text-style 
	       #+(version>= 4 2) :text-style '(:sans-serif :roman :small))
    (show-topics :application 
                 :scroll-bars :both
                 :display-function 'display-topics
                 :display-after-commands nil
                 :incremental-redisplay nil
	       #-(version>= 4 2) :default-text-style 
	       #+(version>= 4 2) :text-style '(:sans-serif :roman :small))
    (pdoc :pointer-documentation :scroll-bars nil
	  #-(version>= 4 2) :default-text-style 
	  #+(version>= 4 2) :text-style '(:sans-serif :roman :small)) 
    ))
  
;;;  ( #-(version>= 4 2) :layout #+(version>= 4 2) :layouts spurious)
  (:layout
   ((default
      (:column 1
               (menu :compute)
               (:row :rest
                (show-help :rest)
                (show-topics 1/3))
               (pdoc :compute)))))
  )


(define-help-browser-command (com-help-redisplay
                              :menu "ReDisplay"
                              :name "ReDisplay")
                             ()
  ;; on Mac, this seems to bring back dead scroll bars...
  (clim:window-refresh 
   (clim:get-frame-pane clim::*application-frame* 'show-help))
  (clim:window-refresh 
   (clim:get-frame-pane clim::*application-frame* 'show-topics)))


(define-help-browser-command (com-help-find 
			      :menu ("Find" 
				     :documentation "Find topics mentioning a string")
			      :name "Find")
    ()
  (let ((help-inst (slot-value clim:*application-frame* 'help-inst))
        (stream (clim:get-frame-pane clim:*application-frame* 'show-help))
        (string nil)
        (choice nil)
        (topics nil))
    
    (clim:accepting-values 
     (stream :resynchronize-every-pass t :own-window t
             :label "Find...")
     (fresh-line stream)
     (setf string
           (clim:accept '(clim:null-or-type string) :default string
                        :prompt "Search string" :stream stream))
     (terpri stream)
     (setf topics 
           (if string (find-topics-mentioning help-inst string)))
     (if (and topics (= (length topics) 1))
       (setf choice (first topics)))
     (if (not (member choice topics :test #'string-equal))
       (setf choice nil))
     (cond (topics
            (setf choice
                  (clim:accept (cons 'member topics)
                               :default choice
                               :prompt "Choose topic" :stream stream)))
           (t ;; no topics
            (write-string "No help topics mention search string" stream)))
     (terpri stream))
    
    (when choice
      (setf (slot-value clim:*application-frame* 'current-topic) choice)
      (display-help clim:*application-frame*
                    (clim:get-frame-pane clim:*application-frame* 'show-help))
      )))


(define-help-browser-command (com-help-exit
			      :menu ("Exit" :documentation "Exit help now")
			      :name "Exit")
    ()
  (clim:frame-exit clim:*application-frame*))

(define-help-browser-command 
  (com-set-topic :name "Set topic")
  ((topic 'help-topic))
  (setf (slot-value clim:*application-frame* 'current-topic) topic)
  (display-help clim:*application-frame*
                (clim:get-frame-pane clim:*application-frame* 'show-help))
  )


(clim:define-presentation-to-command-translator 
  HELP-ON-TOPIC 
  (help-topic com-set-topic help-browser
           :gesture :select
           :pointer-documentation "Display help on this topic")
  (object)
  `(,object))


(defmethod DISPLAY-HELP ((f HELP-BROWSER) stream)
  (with-slots (help-inst current-topic) f
    (when help-inst
      (let* ((topic (or current-topic "Introduction"))
             (help-table (slot-value help-inst 'help-table))
             (xrefd (slot-value help-inst 'help-is-cross-referenced-p))
             (width (slot-value help-inst 'help-width))
             (current-text nil)
             (line 0))
        
        (setf current-text (gethash topic help-table))
        
	;; (clim:filling-output (stream :fill-width '(80 :character)))
        (clim:with-end-of-line-action (:allow stream)
	  
	  (terpri stream)
	  (clim:surrounding-output-with-border
	   (stream :shape :underline)
	   (clim:with-text-size 
	       (:normal stream)
	     (clim:with-text-face #+(version>= 4 2) (stream :bold)
				  #-(version>= 4 2) (:bold stream)
	       (write-string "Topic: " stream)
	       (write-string topic stream)
	       (terpri stream))))

	  (terpri stream)
	  (dolist (word current-text)
	    (cond ((eq word :PARAGRAPH)
		   (terpri stream) 
		   (unless (= line 0) (terpri stream))
		   (setf line 0))
		  ((and (listp word) (eq (first word) :REF))
		   (clim:present (second word) 'help-topic :stream stream)
		   (incf line (length (second word))))
		  ((and (not xrefd) (gethash word help-table))
		   (clim:present word 'help-topic :stream stream)
		   (incf line (length word)))
		  (t (write-string word stream)
		     (incf line (length word))))
	    (cond ((> line width)
		   (terpri stream)
		   (setf line 0))
		  ((not (eq word :PARAGRAPH))
		   (write-string " " stream)
		   ))))
	(terpri stream)       
	))))

(defmethod DISPLAY-TOPICS ((f HELP-BROWSER) stream)
  (with-slots (help-inst) f
    (when help-inst
      (let* ((all-topics (slot-value help-inst 'all-help-topics))
             )
        (clim:with-end-of-line-action (:allow stream)
	  (clim:with-text-size 
	      (:small stream)   
	    (dolist (topic all-topics)
	      (write-string "  " stream)
	      (clim:present topic 'help-topic :stream stream)
	      (terpri stream))))))))


;;; --- global for application frame

(defvar *HELP-BROWSER-FRAME* nil)


;;; --- constructor

(defun MAKE-HELP-BROWSER-FRAME (&key (root *clim-root*)
				     (force nil))
  
  ;; if force, kill the old one
  (if force (setf *HELP-BROWSER-FRAME* nil))
  ;; only make a new one if there isn't one bound to the global
  (unless *HELP-BROWSER-FRAME*
    (setf *HELP-BROWSER-FRAME* 
      (clim:make-application-frame 'HELP-BROWSER :parent root
				   :width 500 :height 600
				   :pretty-name "Help"
				   ))))


;;; --- display

(defun SHOW-HELP-BROWSER (which &key (frame *help-browser-frame*))
  (clim:run-frame-top-level frame)
  )


(defun RUN-HELP-BROWSER-FRAME (&key which
				    (root *clim-root*)
				    (force nil))
  
  (let* ((process-name "Help Browser")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal)))

    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
      (if process 
	  #+:Allegro (mp:process-kill process)
	  #-:Allegro nil
	  )
      (setf *HELP-BROWSER-FRAME* nil)
      )

    ;; create frame if it doesn't exist
    (unless *HELP-BROWSER-FRAME*
      (make-help-browser-frame :root root :force force))
    
    ;; update state
    (let ((frame *HELP-BROWSER-FRAME*))
      (cond ((eq which (slot-value frame 'help-inst)))
	    (t (clim:window-clear (clim:get-frame-pane frame 'show-help))
	       (clim:window-clear (clim:get-frame-pane frame 'show-topics))
	       (setf (slot-value frame 'help-inst) which)
	       (setf (slot-value frame 'current-topic) "Introduction")
	       (display-help
		frame
		(clim:get-frame-pane frame 'show-help))
	       (display-topics
		frame
		(clim:get-frame-pane frame 'show-topics))
	       )))


    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*HELP-BROWSER-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *HELP-BROWSER-FRAME*)
      ))))
    


#| 
(setf *help-browser-frame* nil)

(run-help-browser-frame :which hhh)
(run-help-browser-frame :which hhh2)

(describe hhh2)
(test-help hhh)
|#

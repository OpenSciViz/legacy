;;;-*-Mode:LISP; Syntax: Common-Lisp;Package: climif;-*-
;;;
;;; climif-main.lisp
;;;
;;; main CLIM spike user interface
;;;
;;; Jun/Jul92 MDJ
;;; extended from version was sent to MIT 29Jul92
;;; broke out classes for short-term displays
;;;

#| compile and load
(progn
  (compile-file "source/climif-main")
  (load "source/climif-main.fasl"))
|#

;;; ======================================================================
;;;                                         PACKAGE

(eval-when (compile load eval)
  (in-package climif))


;;; ======================================================================
;;;                                         GLOBALS
;;; this one is for testing only

(defvar *THE-GENERIC-CSP-SCHEDULE-FRAME* nil)



;;; ======================================================================
;;;                                         UTILITY FUNCTIONS


(defmacro WITH-NO-STANDARD-OUTPUT (&body body)
  (let ((sym (gensym)))
    `(let* ((,sym (make-broadcast-stream))
            (*standard-output* ,sym)
	    #+:Allegro (lisp:*load-print* nil)
	    #+:Allegro (lisp:*load-verbose* nil)
	    #+:Allegro (excl:*redefinition-warnings* nil)
	    )
       ,@body)))


(defun STRING-TRUNC (string size)
  "return string truncated to size if longer"
  (cond ((= size 0) "")
        ((<= (length string) size)
         string)
        (t (concatenate 'string 
                        (subseq string 0 (1- size))
                        "+"))))

#| test case:
(string-trunc "abcdef" 0)
(string-trunc "abcdef" 1)
(string-trunc "abcdef" 2)
(string-trunc "abcdef" 5)
(string-trunc "abcdef" 6)
(string-trunc "abcdef" 10)
|#


(defun NICE-SCALE (smin smax)
  (let* ((span (- smax smin))
         )
    (cond ((<= span 0.1)
           (list
            (list (* 0.01 (ceiling smin 0.01)) (* 0.01 (floor smax 0.01)) 0.01 "~,2f")
            (list (* 0.002 (ceiling smin 0.002)) (* 0.002 (floor smax 0.002)) 0.002 "~,2f")
            ))
          ((<= span 1)
           (list
            (list (*  0.1 (ceiling smin  0.1)) (*  0.1 (floor smax  0.1))  0.1 "~,1f")
            (list (*  0.02 (ceiling smin  0.02)) (*  0.02 (floor smax  0.02))  0.02 "~,2f")
            ))
          ((<= span 10)
           (list
            (list (*  1 (ceiling smin  1)) (*  1 (floor smax  1))  1 "~d")
            (list (*  0.2 (ceiling smin  0.2)) (*  0.2 (floor smax  0.2))  0.2 "~,1f")
            ))
          ((<= span 100)
           (list
            (list (* 10 (ceiling smin 10)) (* 10 (floor smax 10)) 10 "~d")
            (list (*  2 (ceiling smin  2)) (*  2 (floor smax  2))  2 "~d")
            ))
          ((<= span 1000)
           (list
            (list (* 100 (ceiling smin 100)) (* 100 (floor smax 100)) 100 "~d")
            (list (*  20 (ceiling smin  20)) (*  20 (floor smax  20))  20 "~d")
            ))
          ((<= span 10000)
           (list
            (list (* 1000 (ceiling smin 1000)) (* 1000 (floor smax 1000)) 1000 "~d")
            (list (* 200 (ceiling smin 200)) (* 200 (floor smax 200)) 200 "~d")
            ))
          )))
#|
(nice-scale 2.102 2.193)
(nice-scale 2.1 2.9)
(nice-scale 1.5 8.2)
(nice-scale 205 520)
|#

(defmacro DRAW-GWINDOW-RECT (gwindow x1 y1 x2 y2 
                                     &key 
                                     (frame t frame-supplied-p) 
                                     (fill nil fill-supplied-p) 
                                     (linewidth 1 linewidth-supplied-p))
  "draw a connected or filled rectangle.  
    coordinates are page coord
    Args:
    :frame can be t or nil
    :fill can be nil, t, or color (Allegro) or fill fraction (Mac monochrome)"
  ;;(draw-gwindow-rect foo 0 0 1 1)
  ;;(draw-gwindow-rect foo 0 0 1 1 :fill nil)
  ;;(draw-gwindow-rect foo 0 0 1 1 :fill nil :frame t)
  `(let ((xx1 ,x1)(xx2 ,x2)(yy1 ,y1)(yy2 ,y2))
     (user::record-polygon
      ,gwindow (list (list xx1 yy1)(list xx1 yy2)
                     (list xx2 yy2)(list xx2 yy1)
                     (list xx1 yy1))
      ,@(if fill-supplied-p (list :fill fill))
      ,@(if frame-supplied-p (list :frame frame))
      ,@(if linewidth-supplied-p (list :linewidth linewidth)))))


;;; ======================================================================
;;;                                         SORTING 

(defun ALL-VAR-SORT-CRITERIA ()
  '(("Top priority first" 
     :value :top-prio-first 
     :documentation "Higher priority = smaller priority values")
    ("Top priority last"
     :value :top-prio-last
     :documentation "Higher priority = smaller priority values")
    ("Smallest domain first"
     :value :smallest-domain-size
     :documentation "Smallest domain size = most restrictive scheduling choices")
    ("Smallest number of min-conflict values"
     :value :smallest-min-conf-domain
     :documentation "More restrictive min-conflicts choices first")
    ("Observation name"
     :value :obs-name
     :documentation "Observation name")
    ("Target RA"
     :value :ra
     :documentation "Target right ascension")
    ("Exposure time (smallest first)"
     :value :exp-time
     :documentation "Exposure time")
    ("Scheduled start time"
     :value :scheduled-start
     :documentation "Schedule start")
    
    ))

(defun SORT-VARS-BY-CRITERIA (var-list criteria)
  (case criteria
    (:top-prio-first 
     (user::safe-sort var-list #'< :key #'user::priority))
    (:top-prio-last 
     (user::safe-sort var-list #'> :key #'user::priority))
    (:smallest-domain-size
     (user::safe-sort var-list #'< :key #'user::values-in-domain-count))
    (:smallest-min-conf-domain 
     (user::safe-sort var-list #'< :key #'user::values-with-min-conflicts-count))
    (:obs-name
     (user::safe-sort var-list #'string-lessp 
		      :key #'(lambda (x) (user::string-it (user::var-name x)))))
    (:ra
     (user::safe-sort var-list #'< 
		      :key #'(lambda (x) (user::target-ra
					  (user::task x)))))
    (:exp-time
     (user::safe-sort var-list #'<
		      :key #'(lambda (x) (user::exp-time
					  (user::task x)))))
    (:scheduled-start
     (user::safe-sort var-list #'<
		      :key #'(lambda (x)
			       (or (user::assigned-time x)
				   most-positive-double-float))))

    ;; none of the above
    (t
     var-list)))
     

;;; ======================================================================
;;;                                                                  FOCUS

(defvar *FOCUS-SPECS* nil)
(setf 
 *FOCUS-SPECS*
 '(("Assignment status"
    ("All" :value :assigned-dont-care 
     :documentation "All values, assigned or unassigned" 
     :tester identity)
    ("Assigned only" :value :assigned-only 
     :documentation "Vars with assignments only"
     :tester (lambda (v) (user::has-assigned-value-p v)))
    ("Unassigned only" :value :unassigned-only 
     :documentation "Vars with no assigned values"
     :tester (lambda (v) (not (user::has-assigned-value-p v))))
    ("Assigned with conflicts" 
     :value :assigned-with-conflicts 
     :documentation "Assigned value has conflicts"
     :tester (lambda (v) (and (user::has-assigned-value-p v)
                              (user::assigned-value-has-conflicts-p v))))
    ("Assigned, no conflicts" 
     :value :assigned-with-no-conflicts 
     :documentation "Assigned value has no conflicts"
     :tester (lambda (v) (and (user::has-assigned-value-p v)
                              (not (user::assigned-value-has-conflicts-p v)))))
    )
   
   ("Lock status"
    ("All" :value :locked-dont-care 
     :documentation "All values, locked and unlocked" 
     :tester identity)
    ("Locked only" :value :locked-only 
     :documentation "Locked values only"
     :tester user::locked-p)
    ("Unlocked only" :value :unlocked-only 
     :documentation "Unlocked values only"
     :tester (lambda (v) (not (user::locked-p v))))
    )
   
   ("Ignored status"
    ("All" :value :ignored-dont-care 
     :documentation "All vars, ignored and unignored" 
     :tester identity)
    ("Ignored only" :value :ignored-only 
     :documentation "Ignored values only"
     :tester user::ignored-p)
    ("Unignored only" :value :unignored-only 
     :documentation "Unignored vars only"
     :tester (lambda (v) (not (user::ignored-p v))))
    )

   ("Priority"
    ("All" :value :priority-dont-care
     :documentation "All priorities"
     :tester identity)
    ;; Note: pre-tester is called first with csp as arg: results
    ;; are second argument to tester
    ("Highest priority" :value :highest-priority
     :documentation "Highest priority only"
     :tester (lambda (v prio-min-max)
               (= (user::priority v) (first prio-min-max)))
     ;; pre-tester returns list (min max) priority, where higher priority
     ;; is smaller value
     :pre-tester priority-pre-tester)
    ("Lowest priority" :value :lowest-priority
     :documentation "Lowest priority only"
     :tester (lambda (v prio-min-max)
               (= (user::priority v) (second prio-min-max)))
     :pre-tester priority-pre-tester)
    )
   
   (("Target Name" 
     (clim:null-or-type string)
     ;; same structure as pre-tester
     (lambda (v name) 
       (search name
	       (user::target-name (user::task v))
	       :test #'char-equal))))

   ))


;;; --- PRE-TESTERS FOR FOCUS SET

(defun PRIORITY-PRE-TESTER (c)
  "pre-tester for priority sort functions:  input csp, output
    is a list (priority-min priority-max)"
  (user::min-max-priority c))



;;; --- FOCUS FUNCTIONS

(defun CHECK-FOCUS-SPEC ()
  (dolist (topic *FOCUS-SPECS*)
    (cond ((stringp (first topic))
	   (format t "~%~a" (first topic))
	   (dolist (item (rest topic))
	     (format t "~%  ~30s ~2a ~3a ~30s" (first item) 
		     (if (getf (rest item) :tester) "Y" "N")
		     (if (getf (rest item) :pre-tester) "pre" "   ")
		     (getf (rest item) :value))))
	  (t 
	   (format t "~%~a" (first (first topic)))
	   (format t "~%  CLIM type: ~a" (second (first topic)))))))  

#| check the focus list: print out label, whether there is a tester (Y/N),
   and the keyword indicator:
   Example:
(check-focus-spec) =>
Assignment status
  "All"                          Y      :ASSIGNED-DONT-CARE           
  "Assigned only"                Y      :ASSIGNED-ONLY                
  "Unassigned only"              Y      :UNASSIGNED-ONLY              
  "Assigned with conflicts"      Y      :ASSIGNED-WITH-CONFLICTS      
  "Assigned, no conflicts"       Y      :ASSIGNED-WITH-NO-CONFLICTS   
Lock status
  "All"                          Y      :LOCKED-DONT-CARE             
  "Locked only"                  Y      :LOCKED-ONLY                  
  "Unlocked only"                Y      :UNLOCKED-ONLY                
Ignored status
  "All"                          Y      :IGNORED-DONT-CARE            
  "Ignored only"                 Y      :IGNORED-ONLY                 
  "Unignored only"               Y      :UNIGNORED-ONLY               
Priority
  "All"                          Y      :PRIORITY-DONT-CARE           
  "Highest priority"             Y  pre :HIGHEST-PRIORITY             
  "Lowest priority"              Y  pre :LOWEST-PRIORITY              
|#

(defun GET-FOCUSED-VAR-LIST (keylist csp focus-specs)
  (let ((fcn-list nil)
        (result nil)
	(specs focus-specs)
	(spec nil))
    
    ;; get predicates for testing
    (dolist (key keylist)
      (setf spec (pop specs))
      (let ((fcn nil))
	
	(cond ((listp (first spec))
	       (setf fcn `(lambda (x) 
			    (funcall #',(third (first spec)) x ,key)))
	       (push fcn fcn-list)
	       ;; (format t "~%~a: ~a <--" key fcn)
	       )
	      ((stringp (first spec))       
	       (dolist (possibles (rest spec))
		 (when (eq key (getf (rest possibles) :value))
		   (let ((pre-tester (getf (rest possibles) :pre-tester))
			 (tester (getf (rest possibles) :tester 'identity)))
		     (setf fcn (if pre-tester 
				   `(lambda (x) 
				      (funcall #',tester x ',(funcall pre-tester csp)))
				 tester))
		     ;; (format t "~%~a: ~a" key fcn)
		     (push fcn fcn-list)
		     (return nil)))))
	      (t 
	       ;; (format t "~%No match: key ~a, topic ~a" key spec)
	       nil
	       ))))
	
    ;; items in the fcn-list are a funcallable form to be called on the var
    ;; (format t "~%fcns ~a" fcn-list)
    (user::do-for-all-vars
     :var v :csp csp :form 
     (let ((include (dolist (fcn fcn-list t)
                      (if (not (funcall fcn v)) (return nil)))))
       (if include (push v result))))
    (nreverse result)))

#|
(get-focused-var-list '(:assigned-dont-care
			:locked-dont-care
			:ignored-dont-care
			:highest-priority
			"U"
			nil)
		      (user::obs-csp *xte-long-term-schedule*)
		      *XTE-FOCUS-SPECS*)
(let ((temp nil))
  (dolist (topic *FOCUS-SPECS*)
    (push (if (stringp (first topic))
	      (first topic) (first (first topic)))
	  temp))
  (reverse temp))
(position "Target Name" *focus-specs*)

|#



;;; ======================================================================
;;;                                         AUTO SCHEDULING


;;; --- all strategies and how to run them

(defvar *AUTO-STRAT* nil)
(setf   *AUTO-STRAT*
  '(("Early greedy" 
     :value user::early-greedy-schedule
     :documentation "Build schedule forward in time, best next, no search")
    ("Earliest least-min-conflicts (ELMINC)"
     :value user::elminc-schedule
     :documentation "Select var with smallest number of min-conflicts values; min-conflicts repair")
    ("Max-preference"
     :value user::max-pref-schedule
     :documentation "Highest preference value selection, min-conflicts repair")
    ("High-priority max-preference"
     :value user::hi-prio-max-pref-schedule
     :documentation "Highest priority var selection, then max preference; min-conflicts repair")
    ))


;;; --- a single auto run

(defclass 
    AUTO-RUN ()
    ((csp :initform nil :initarg :csp
	  :documentation "CSP instance")
     (state-summary
      :initform nil :initarg :state-summary
      :documentation "value of csp-state-summary function applied
       to CSP after auto run")
     (strategy 
      :initform nil :initarg :strategy 
      :documentation "description of strategy used (meaning TBD)")
     (assignments
      :initform nil :initarg :assignments
      :documentation "list of assignments in form var ID and value")
     (run-number
      :initform nil :initarg :run-number
      :documentation "number of run")
     ))

(defmethod PRINT-OBJECT ((object AUTO-RUN) stream)
  (format stream "<Run ~a ~a>"
	  (slot-value object 'run-number)
	  (slot-value object 'strategy)))


;;; --- a collection of auto runs

(defclass 
    AUTO-RUN-SET ()
    ((auto-runs :initform nil :initarg :auto-runs
		:documentation "A list of auto-run instances"))
  )


;;; --- methods for AUTO-RUN-SET

(defmethod CLEAR-AUTO-RUN-HISTORY ((ars auto-run-set))
  (with-slots (auto-runs) ars
    (setf auto-runs nil)))


(defmethod ADD-TO-AUTO-RUN-HISTORY ((ars auto-run-set) &key csp strategy)

  "add to history. csp must be in state of just completed auto run"
  
  (with-slots (auto-runs) ars
    (push (make-instance 'auto-run
	    :csp csp
	    :state-summary (user::csp-state-summary csp)
	    :strategy strategy
	    :assignments (user::list-assignments 
			  csp :var-mapping 'user::name)
	    :run-number (1+ (length auto-runs)))
	  auto-runs)))


(defmethod AUTO-RUNS-P ((ars auto-run-set) csp)

  "t if any, nil otherwise"
  
  (with-slots (auto-runs) ars
    (dolist (run auto-runs)
      (if (eq csp (slot-value run 'csp))
	  (return-from auto-runs-p t)))
    nil))


(defmethod AUTO-RUN-COUNT ((ars auto-run-set))
  (with-slots (auto-runs) ars
    (length auto-runs)))


(defmethod AUTO-RUN ((ars auto-run-set) i)
  "return ith auto run"
  (with-slots (auto-runs) ars
    (if (< i (length auto-runs))
	(elt auto-runs i))))


(defmethod MOST-RECENT-AUTO-RUN ((ars auto-run-set))
  (with-slots (auto-runs) ars
    (first auto-runs)))


(defmethod ALL-AUTO-RUNS ((ars auto-run-set))
  (with-slots (auto-runs) ars
    auto-runs))


(defmethod AUTO-RUN-KEYWORDS ((ars auto-run-set) test-csp)

  "return list of all keywords valid for input CSP"
  
  (with-slots (auto-runs) ars
    (let ((result nil))
      (dolist (run (all-auto-runs ars))
	;; (format t "~%run: ~a" run)
	(with-slots (csp state-summary) run
	  (when (equal csp test-csp)
	    (let ((ctr 0))
	      (dolist (i state-summary)
		(if (evenp ctr) (push i result))
		(incf ctr))
	      (return nil)))))
      (nreverse result))))


(defmethod AUTO-RUN-KEYWORD-RANGE ((ars auto-run-set) test-csp keyword)

  "return list (min max) value for specified keyword"
  
  (with-slots (auto-runs) ars
    (let ((found-any nil)
	(min-value 1e20)
	(max-value -1e20))
    (dolist (run (all-auto-runs ars))
      (with-slots (csp state-summary) run
	(when (equal test-csp csp)
	  (let ((val (getf state-summary keyword nil)))
	    (when val
	      (setf found-any t)
	      (setf min-value (min min-value val))
	      (setf max-value (max max-value val)))))))
    (cond (found-any (list min-value max-value))
	  (t nil)))))


(defmethod AUTO-RUN-STRATEGIES ((ars auto-run-set) test-csp)

  "return list of all strategies found"

  (with-slots (auto-runs) ars
    (let ((result nil))
      (dolist (run (all-auto-runs ars))
	(with-slots (csp strategy) run
	  (when (equal csp test-csp)
	    (pushnew strategy result :test #'equal))))
      result)))

#|
(auto-run-keywords (obs-csp *xte-long-term-schedule*))
(auto-run-keyword-range (obs-csp *xte-long-term-schedule*) :assigned-var-count)
(auto-run-strategies (obs-csp *xte-long-term-schedule*))

(let* ((frame *the-csp-schedule-frame*)
       (csp (slot-value frame 'csp))
       (all-keywords (auto-run-keywords frame csp))
       (all-runs (reverse (all-auto-runs frame)))
       )
  (dolist (run all-runs)
    (with-slots (state-summary run-number strategy) run
      (format t "~%run# ~d strat ~a" run-number strategy)
      (dolist (key all-keywords)
	(format t " ~a" (getf state-summary key)))
  )))
|#


;;; ======================================================================
;;;                                         MIXIN CLASSES FOR SCHEDULE
;;;                                         APPLICATION FRAMES
;;;

;;; --- cache bin locations for faster drawing

(defclass CSP-SCHEDULE-BIN-CACHE ()
  (
   ;; array of size bin-count for current capacity constraint
   (bin-to-x-array :initform nil)
   )
  (:documentation
   "Mixin for array mapping bins to x for faster drawing.  Note: this should
    only be mixed in with a CSP display where there is a resource-constraint
    known.  And, there must be page-left and -right, and wleft and wright
    in the parent class"))

;;; --- compute method

(defmethod CALCULATE-VIEW-CACHE-BINS ((frame CSP-SCHEDULE-BIN-CACHE))
  (with-slots
    (current-var bin-to-x-array
                 resource-constraint
		 resource-segment-definition
                 gwindow page-left page-right wleft wright)
    frame
    
    (when (and current-var resource-constraint)
      (let* (;; (csp (if current-var (user::csp current-var)))
             (task (if current-var (user::task current-var)))
             (chronika (if task (user::chronika task)))
             ;; (FDSIA (user::FDSIA task))
             (bin-count (user::bin-count resource-constraint))
             )
        
        (user::set-page-loc-of-drawing-rect
         gwindow page-left page-right 0 1)
        (user::set-loc-of-drawing-window 
         gwindow wleft wright 0 1)
        (user::set-x-window-scale 
         gwindow 
         (- (user::qtime-start chronika) 0.5) 
         (+ (user::qtime-end   chronika) 0.5))
        
        (when (or (null bin-to-x-array)
                  (> bin-count (array-dimension bin-to-x-array 0)))
          (setf bin-to-x-array (make-array bin-count)))
        (dotimes (bin bin-count)
          (let* ((qtime1 (user::start-of-segment resource-segment-definition bin))
                 (qtime2 (user::end-of-segment resource-segment-definition bin))
                 (x1 (round (user::t-to-page-x gwindow (- qtime1 0.5))))
                 (x2 (round (user::t-to-page-x gwindow (+ qtime2 0.5)))))
            (setf (aref bin-to-x-array bin) (list x1 x2))))))))



;;; --- schedule modification & saving

(defclass CSP-SCHEDULE-MOD-MANAGER ()
  (  
   ;; flag: t if modified
   (modified :initform nil)
   
   ;; filename
   (schedule-filename :initform nil)
   )
  
  (:documentation 
   "Mixin for CSP schedule class: manage setting/clearing the modified
    flag, plus saving/restoring the schedule to a file under user
    control"))

;;; --- modify

(defgeneric MARK-MODIFIED (obj)
  (:documentation
   "Set flag indicating that obj has been modified"))

(defmethod MARK-MODIFIED ((obj t)))

(defmethod MARK-MODIFIED ((frame CSP-SCHEDULE-MOD-MANAGER))
  (setf (slot-value frame 'modified) t))

;;; --- unmodify

(defgeneric MARK-UNMODIFIED (obj)
  (:documentation
   "Clear flag indicating that obj has been modified"))

(defmethod MARK-UNMODIFIED ((obj t)))

(defmethod MARK-UNMODIFIED ((frame CSP-SCHEDULE-MOD-MANAGER))
  (setf (slot-value frame 'modified) nil))

;;; --- dialog and save

(defgeneric SAVE-IF-REQUESTED (obj)
  (:documentation
   "save object if interactive dialog with user requests it"))

(defmethod SAVE-IF-REQUESTED ((obj t))
  ;; notify if called on object which doesn't know how to save
  (clim:notify-user 
   clim:*application-frame*
   "Don't know how to save this object!!"))
                              
(defmethod SAVE-IF-REQUESTED ((frame CSP-SCHEDULE-MOD-MANAGER))
  (let ((stream (clim:get-frame-pane frame 'pointer-doc)))
    (with-slots (modified schedule-filename) frame
      (when modified
        (let ((save t))
          (clim:accepting-values
           (stream :resynchronize-every-pass t :own-window t 
                   :label "Save changes")
           (clim:with-text-size (:large stream)
             (clim:with-text-family (:sans-serif stream)
               
               (setf save (clim:accept 'clim:boolean 
                                       :default save :stream stream 
                                       :prompt "Save changes"))
               
               (fresh-line stream)
               (setf schedule-filename
                     (clim:accept '(clim:null-or-type string) 
                                  :default schedule-filename
                                  :prompt "To file" :stream stream))
               
               (cond ((and save schedule-filename)
                      (cond ((probe-file schedule-filename)
                             (clim:with-text-face (:bold stream)
                               (format stream "~%File already exists:")
                               (format stream "~%it will be overwritten")))
                            (t (format stream "~%(new file)"))))
                     ((and save (not schedule-filename))
                      (clim:with-text-face (:bold stream)
                        (format stream "~%Warning: no file specified:")
                        (format stream "~%changes will NOT be saved"))))
               )))                      ; end accepting values
          
          (when (and save schedule-filename)
            ;; (save-sort-crit schedule-filename)
            (mark-unmodified frame)))))))




;;; ======================================================================
;;;                                         CLIM PRESENTATIONS

;;; --- presentation: VAR


(clim:define-presentation-type VAR ())

(clim:define-presentation-method 
  clim:present (object (type VAR) stream view &key)
  (declare (ignore view))
  (print-object object stream))


;;; --- presentation: VAR-VALUE
;;; a var-value is a cons (var . value), displayed as value ONLY!


(clim:define-presentation-type VAR-VALUE ())

(clim:define-presentation-method 
  clim:present (object (type VAR-VALUE) stream view &key)
  (declare (ignore view))
  (format stream "~d" (rest object))
  )

;;; --- presentation: VAR-PICTURE
;;; this is a drawing of a variable, from which var and value can be
;;; extracted (see the presentation-to-command translator)

(clim:define-presentation-type VAR-PICTURE ())

;;; --- presentation: AUTO-RUN

(clim:define-presentation-type AUTO-RUN ())

;;; --- presentation: VAR-PAGE-UP-BUTTON

(clim:define-presentation-type VAR-PAGE-UP-BUTTON ())

;;; --- presentation: VAR-PAGE-DOWN-BUTTON

(clim:define-presentation-type VAR-PAGE-DOWN-BUTTON ())

;;; --- presentation: VAR-JUMP-TO-PAGE-BUTTON

(clim:define-presentation-type VAR-JUMP-TO-PAGE-BUTTON ())


;;; ======================================================================
;;;                                                APPLICATION FRAME MIXIN
;;; ======================================================================
;;; Note: this class is used to define behavior that is is common to 
;;; both Long- and short-term schedules.  Specialized methoda handle the 
;;; differences  (e.g. in display functions)


(defclass CSP-SCHEDULE-FRAME-MIXIN
  
  ()                                    ; superclasses
  
  (                                     ; state variables
   ;; instance of csp
   (csp :initform nil)
   ;; list of vars currently in "focus" set
   (focus-vars :initform nil)
   ;; focus criteria name: keyword
   (focus-criteria :initform nil)
   ;; sort criteria name: keyword
   (sort-criteria :initform nil)
   ;; the currently selected var
   (current-var :initform nil)

   ;; for grouping vars in the display
   (var-group-size :initform 10)
   ;; group list: this looks like
   ;; ((0 (var1 var2 ...) (1 (var10 var11 ...)) ...))
   (var-groups :initform nil)
   ;; the current var group
   (var-group :initform nil)
   
   ;; resource constraint to draw, if any
   (resource-constraint :initform nil)
   ;; segment definition: only if there is a resource constraint
   (resource-segment-definition :initform nil)

   ;; -graphics slots-
   ;; a gwindow instance for the graphics window
   (gwindow :initform nil)
   ;; array of value to pair mapping (x1 x2), for plotting only
   ;; array of size value-count of current-var
   (val-to-x-array :initform nil)
   ;; location of drawing window:  page-left, page-right are for
   ;; pixel location of left/right side of screen
   ;; wleft, wright are fractional (range: 0-1) locations of
   ;; window left and right sizes
   (page-left :initform 50)
   (page-right :initform 500)
   (wleft :initform 0.10)
   (wright :initform 0.95)

   
   )
  
  )


;;; ----------------------------------------------------------------------
;;;                                         cache methods for graphics

;;; --- init

(defgeneric INIT-GRAPHICS-CACHE (frame)
  (:documentation "initialize graphics cache items that depend only
    on the CSP instance, not the var"))

(defmethod INIT-GRAPHICS-CACHE ((frame CSP-SCHEDULE-FRAME-MIXIN))
  ;; cache the segment boundary bins to x values:
  (calculate-view-cache-bins frame)
  )

;;; --- update

(defgeneric UPDATE-VIEW-CACHE-VALUES (frame)
  (:documentation "update the graphics cache for items that depend
    on the var or other quantities"))

(defmethod UPDATE-VIEW-CACHE-VALUES ((frame CSP-SCHEDULE-FRAME-MIXIN))
  (with-slots
   (current-var gwindow val-to-x-array page-left page-right wleft wright)
   frame

   (when current-var
     (let* ((task (if current-var (user::task current-var)))
            (chronika (if task (user::chronika task)))
            (FDSIA (user::FDSIA task))
            (value-count (user::value-count current-var))
            )
       (user::set-page-loc-of-drawing-rect
        gwindow page-left page-right 0 1)
       (user::set-loc-of-drawing-window 
        gwindow wleft wright 0 1)
       (user::set-x-window-scale 
        gwindow (- (user::qtime-start chronika) 0.5) (+ (user::qtime-end chronika) 0.5))

       (when (or (null val-to-x-array)
		 (> value-count (array-dimension val-to-x-array 0)))
	 (setf val-to-x-array (make-array value-count)))
       (dotimes (val value-count)
	 (let* ((qtime (user::FDSIA-val-to-time FDSIA val))
		(x1 (round (user::t-to-page-x gwindow (- qtime 0.5))))
		(x2 (round (user::t-to-page-x gwindow (+ qtime 0.5)))))
	   (setf (aref val-to-x-array val) (list x1 x2))))

       ))))

;;; ----------------------------------------------------------------------
;;;                                         frame mixin methods

;;; --- initialization

(defmethod INIT-CSP-SCHEDULE-FRAME-MIXIN ((frame CSP-SCHEDULE-FRAME-MIXIN) 
					  &key (pane 'schedule-picture)
					       (page-left 20)
					       (page-right 500)
					       (wleft 0.10)
					       (wright 0.95)
					       (page-height 250))
  
  "initialize CSP-SCHEDULE-FRAME-MIXIN instance. Pane is name the pane which
    will be associated with the CLIM-gwindow for graphics output
    e.g. (init-csp-schedule-frame-mixin foo 'schedule-picture)"
  
  (setf (slot-value frame 'gwindow)
    (user::make-clim-gwindow 
     :stream (clim:get-frame-pane frame pane)
     :page-height page-height))
  (setf (slot-value frame 'page-left) page-left
	(slot-value frame 'page-right) page-right
	(slot-value frame 'wleft) wleft
	(slot-value frame 'wright) wright)
  
  (init-graphics-cache frame)
  (update-view-cache-values frame)
  )


;;; --- var grouping for display

(defmethod GROUP-VARS-FOR-DISPLAY ((frame CSP-SCHEDULE-FRAME-MIXIN))
  
  "given the current value of focus-vars and current var,
    this routine sets up the var group values for display
    call this whenever the focus-vars list changes"
  
  (with-slots (var-group-size var-groups var-group
	       focus-vars current-var) frame
    
    (let ((var-counter 0)
	  (group-counter 0)
	  (current-var-group 0)
	  (group nil)
	  (all-groups nil))

      (dolist (var focus-vars)
	(cond ((> var-counter (1- var-group-size))
	       (push (list group-counter (reverse group)) all-groups)
	       (if (member current-var group) 
		   (setf current-var-group group-counter))
	       (setf var-counter 1)
	       (setf group (list var))
	       (incf group-counter))
	      (t (push var group)
		 (incf var-counter))))
      (when group 
	(push (list group-counter (reverse group)) all-groups)
	(if (member current-var group) 
	    (setf current-var-group group-counter)))
      
      (setf var-groups (reverse all-groups))
      (setf var-group (assoc current-var-group var-groups))
      ;(format t "~%var groups: ~a" var-groups)
      ;(format t "~%group: ~a (current-var = ~a)" var-group current-var)
	      
      )))


(defmethod SET-VAR-GROUP-FOR-CURRENT-VAR ((frame CSP-SCHEDULE-FRAME-MIXIN))
  
  "set the slot corresponding to the var group in which the current
    var is a member.  Call this whenever the current var changes,
    but the focus-vars doesn't"
  
  (with-slots (current-var var-group var-groups) frame
    (cond
     ;; if it's in the same group, don't do anything
     ((member current-var (second var-group))
      ;; done
      )
     (t					
      ;; otherwise look through the var-groups list
      (dolist (group var-groups)
	(when (member current-var (second group))
	  ;; put the group in the slot
	  (setf var-group group)
	  ;; break out and return
	  (return nil)))))))
      

(defmethod SET-VAR-GROUP-BY-NUMBER ((frame CSP-SCHEDULE-FRAME-MIXIN)
				    new-var-group-number)
  
  "set the slot corresponding to the var group specified
    by the new-var-group-number 1... max"

  (with-slots (current-var var-group var-groups) frame
    (cond
     ;; if in the same group, don't do anything
     ((= (1- new-var-group-number) (first var-group))
      ;; done
      )
     (t					
      ;; otherwise look through the var-groups list
      (dolist (group var-groups)
	(when (= (1- new-var-group-number) (first group))
	  ;; put the group in the slot
	  (setf var-group group)
	  ;; break out and return
	  (return nil)))))))

  
  
(defmethod NEXT-VAR-GROUP ((frame CSP-SCHEDULE-FRAME-MIXIN))
  "return next var group if there is one, nil otherwise"
  (with-slots (var-group var-groups) frame
    (when (and var-group var-groups)
      (assoc (1+ (first var-group)) var-groups))))


(defmethod PREV-VAR-GROUP ((frame CSP-SCHEDULE-FRAME-MIXIN))
  "return prev  if there is a next var group, nil otherwise"
  (with-slots (var-group var-groups) frame
    (when (and var-group var-groups)
      (assoc (1- (first var-group)) var-groups))))


(defmethod SET-NEXT-VAR-GROUP ((frame CSP-SCHEDULE-FRAME-MIXIN))
  "set next var group if there is one, else no action"
  (let ((g (next-var-group frame)))
    (if g (setf (slot-value frame 'var-group) g))))


(defmethod SET-PREV-VAR-GROUP ((frame CSP-SCHEDULE-FRAME-MIXIN))
  "set prev var group if there is one, else no action"
  (let ((g (prev-var-group frame)))
    (if g (setf (slot-value frame 'var-group) g))))



;;; ======================================================================
;;;                                            COMMAND TABLES AND COMMANDS
;;; ======================================================================


;;; ----------------------------------------------------------------------
;;;                                         file menu commands

;;; --- file command table:

(clim:define-command-table CSP-SCHEDULE-FILE-COMMANDS)


;;; --- file menu commands:

(clim:define-command (COM-SAVE :command-table CSP-SCHEDULE-FILE-COMMANDS
			       :menu "Save" :name t)
    ()
  (do-com-save clim:*application-frame*))


(defmethod DO-COM-SAVE ((frame CSP-SCHEDULE-FRAME-MIXIN))
  (clim:notify-user
   frame
   (format nil "Don't know how to do menu SAVE")))


(clim:define-command (COM-SAVE-AS :command-table CSP-SCHEDULE-FILE-COMMANDS
				  :menu "Save As..." :name t)
    ()
  (do-com-save-as clim:*application-frame*))


(defmethod DO-COM-SAVE-AS ((frame CSP-SCHEDULE-FRAME-MIXIN))
  (clim:notify-user
   frame
   (format nil "Don't know how to do menu SAVE AS")))


;;; ----------------------------------------------------------------------
;;;                                         main menu bar and commands
;;;                                         not in any menu

;;; --- command table:
 
(clim:define-command-table CSP-SCHEDULE-COMMAND-TABLE)


;;; --- commands in main menu bar:

(clim:define-command (COM-CSP-SCHEDULE-EXIT
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu "Exit"
                      :name "Exit")
  ()
  (clim:frame-exit clim:*application-frame*))


(clim:define-command (COM-CSP-SCHEDULE-HELP 
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu "Help"
                      :name "Help")
  ()
  (do-com-csp-schedule-help clim:*application-frame*))


(defmethod DO-COM-CSP-SCHEDULE-HELP ((frame CSP-SCHEDULE-FRAME-MIXIN))
  (clim:notify-user clim:*application-frame*
                    "Not implemented"
		    ))


;;; FIX ???!!! the list of panes should be in the class
;;; instance (next two commands)

(clim:define-command (COM-CSP-SCHEDULE-REDISPLAY 
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu "Redisplay"
                      :name "Redisplay")
  ()
  (let ((panes '(var-list var-descrip var-menu schedule-status
                 schedule-picture)))
    (dolist (pane panes)
      (clim:window-refresh 
       (clim:get-frame-pane clim::*application-frame* pane)))))


(clim:define-command (COM-CSP-SCHEDULE-REDRAW 
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu "Redraw"
                      :name "Redraw")
  ()
  (do-com-schedule-redraw clim:*application-frame*))


(defmethod DO-COM-SCHEDULE-REDRAW 
    ((frame CSP-SCHEDULE-FRAME-MIXIN)
     &key (panes '(;;(var-list . display-var-list)
		   (var-descrip . display-var-descrip)
		   ;;(var-menu . display-var-menu)
		   (schedule-status . display-schedule-status)
		   (schedule-picture . display-schedule-picture))))
  
  "panes is a list of conses (pane-name . pane-display-fcn)"
  
    (dolist (pane panes)
      (let* ((pane-name (first pane))
             (display-fcn (rest pane))
             (stream (clim:get-frame-pane frame pane-name)))
        (clim:window-clear stream)
        (funcall display-fcn frame stream)

	;; extra refresh only on the mac...
        #+:CCL (clim:window-refresh (clim:get-frame-pane frame pane-name))

        )))
       

;;; ----------------------------------------------------------------------
;;;                                         commands not in menu
;;;                                         presentation-to-command
;;;                                         translators


(clim:define-command (COM-RESTORE-AUTO-RUN
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu nil
                      :name "Restore assignments from an auto schedule run")
    ((run 'AUTO-RUN))
  (with-slots (csp assignments) run
    ;; swallow output
    (with-no-standard-output
	(user::make-listed-assignments
	 csp :assignment-list assignments
	 :var-mapping #'(lambda (x) (user::var-named csp x))))
    ;; update picture
    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
    ))



(clim:define-presentation-to-command-translator 
  RESTORE-AUTO-RUN 
  (AUTO-RUN COM-RESTORE-AUTO-RUN CSP-SCHEDULE-COMMAND-TABLE
            :gesture :select
            :pointer-documentation "Restore assignments from this auto schedule run")
  (object)
  `(,object))


(clim:define-command (COM-SELECT-VAR 
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu nil 
                      :name "Select a var to make current")
    ((var 'VAR))
  ;; Note: used to include :gesture in arglist, but then can't define
  ;; tester.  So removed it and made a separate 
  ;; presentation-to-command-translator below.
  ;; ((var 'var :gesture :select))
  (setf (slot-value clim:*application-frame* 'current-var) var)
  (update-view-cache-values clim:*application-frame*)
  (set-var-group-for-current-var clim:*application-frame*)
  (display-schedule-picture 
   clim:*application-frame*
   (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
  )


;;; only reason for this is to allow tester, to not select currently-selected
;;; var... 

(clim:define-presentation-to-command-translator 
  SELECT-A-VAR 
  (VAR COM-SELECT-VAR CSP-SCHEDULE-COMMAND-TABLE
       :gesture :select
       :tester ((object)
                (not (equal object 
                            (slot-value clim:*application-frame* 
                                        'current-var))))
       :pointer-documentation "Select a var to make current")
  (object)
  `(,object))


(clim:define-command (COM-ASSIGN-VAR 
                      :command-table CSP-SCHEDULE-COMMAND-TABLE
                      :menu nil 
                      :name "Assign a value to a var")
    ((vv 'VAR-VALUE :gesture :select))
  (let ((var (first vv))
        (val (rest vv)))
    (cond ((and (user::has-assigned-value-p var)
                (= (user::assigned-value var) val))
           (user::unassign-value var))
          (t (user::assign-value var val)))
    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
    ))


(clim:define-presentation-to-command-translator
  ASSIGN-A-VAR
  (VAR-PICTURE COM-ASSIGN-VAR CSP-SCHEDULE-COMMAND-TABLE
               :gesture :select
               :tester ((object)
                        (not (or (user::locked-p object)
                                 (user::ignored-p object))))
               :pointer-documentation
               "Click to assign var to value, or unassign if already assigned to value")
  (object presentation context-type frame event window x y)
  
  (with-slots (val-to-x-array current-var) frame
    (let ((closest-val nil)
          (dist-to-closest-val nil))
      (dotimes (i (user::value-count current-var))
        (let* ((x1x2 (aref val-to-x-array i))
               (x1 (first x1x2))
               (x2 (second x1x2))
               (dist (cond ((and (>= x x1)(<= x x2)) 0)
                           (t (min (abs (- x x1)) (abs (- x x2)))))))
          (when (or (not dist-to-closest-val)
                    (< dist dist-to-closest-val))
            (setf dist-to-closest-val dist)
            (setf closest-val i))))
      (list (cons object closest-val)))))


(clim:define-command (COM-VAR-LIST-PAGE-UP
		      :command-table CSP-SCHEDULE-COMMAND-TABLE
		      :menu nil
		      :name "Previous page of observations")
    ()					;no args
  (let ((frame clim:*application-frame*))
    (set-prev-var-group frame)
    (with-slots (current-var var-group) frame
      (when (and current-var var-group
		 (not (member current-var (second var-group))))
	(setf current-var (first (second var-group)))
	(update-view-cache-values clim:*application-frame*)
	))
    
    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))))



(clim:define-presentation-to-command-translator
    VAR-LIST-PAGE-UP
    (VAR-PAGE-UP-BUTTON COM-VAR-LIST-PAGE-UP CSP-SCHEDULE-COMMAND-TABLE
			:gesture :select
			:pointer-documentation 
			"Show previous page of observations")
  ()
  ())


(clim:define-command (COM-VAR-LIST-PAGE-DOWN
		      :command-table CSP-SCHEDULE-COMMAND-TABLE
		      :menu nil
		      :name "Previous page of observations")
    ()					;no args
  (let ((frame clim:*application-frame*))
    (set-next-var-group frame)
    (with-slots (current-var var-group) frame
      (when (and current-var var-group
		 (not (member current-var (second var-group))))
	(setf current-var (first (second var-group)))
	(update-view-cache-values clim:*application-frame*)
	))
        
    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))))



(clim:define-presentation-to-command-translator
    VAR-LIST-PAGE-DOWN
    (VAR-PAGE-DOWN-BUTTON COM-VAR-LIST-PAGE-DOWN CSP-SCHEDULE-COMMAND-TABLE
			  :gesture :select
			  :pointer-documentation 
			  "Show next page of observations")
  ()
  ())


(clim:define-command (COM-VAR-LIST-JUMP-TO-PAGE
		      :command-table CSP-SCHEDULE-COMMAND-TABLE
		      :menu nil
		      :name "Specified page of observations")
    ()					;no args
  (when (> (length (slot-value clim:*application-frame* 'var-groups)) 1)
    (let* ((frame clim:*application-frame*)
	   (choice
	    (catch :cancel
	      (clim:menu-choose
	       (mapcar #'(lambda (x) (1+ (first x)))
		       (slot-value frame 'var-groups))))))
      (unless (eq choice :cancel)
	(set-var-group-by-number frame choice)
	(with-slots (current-var var-group) frame
	  (when (and current-var var-group
		     (not (member current-var (second var-group))))
	    (setf current-var (first (second var-group)))
	    (update-view-cache-values clim:*application-frame*)
	    )))
    
      (display-schedule-picture 
       clim:*application-frame*
       (clim:get-frame-pane clim:*application-frame* 'schedule-picture)))))


(clim:define-presentation-to-command-translator
    VAR-LIST-JUMP-TO-PAGE
    (VAR-JUMP-TO-PAGE-BUTTON COM-VAR-LIST-JUMP-TO-PAGE CSP-SCHEDULE-COMMAND-TABLE
			     :gesture :select
			     :tester ((object)
				      (declare (ignore object))
				      (> (length 
					  (slot-value 
					   clim:*application-frame* 'var-groups)) 
					 1))
			     :pointer-documentation 
			     "Show specified page of observations")
  ()
  ())


;;; ----------------------------------------------------------------------
;;;                                         var menu

;;; --- var menu command table

(clim:define-command-table CSP-SCHEDULE-VAR-COMMANDS)


;;; --- var menu commands:


(clim:define-command (COM-FOCUS
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Focus" 
                      :name "Focus on a subset of all vars")
  ()
  (do-com-focus clim:*application-frame*
    :focus-specs *FOCUS-SPECS*))


;;; Note: use an :around method to override just the
;;; focus-spec list

(defmethod DO-COM-FOCUS ((frame CSP-SCHEDULE-FRAME-MIXIN)
			 &key (focus-specs *FOCUS-SPECS*))
  (with-slots 
    (csp focus-vars current-var focus-criteria)
    frame
    
    (flet ((default-focus-criteria ()
             (let ((temp nil))
               (dolist (topic focus-specs)
                 (push (cond ((stringp (first topic))
			      (getf (rest (first (rest topic))) :value))
			     (t nil))
		       temp))
               (reverse temp))))
      
      ;; initialize focus-criteria to list of defaults
      (unless (and (listp focus-criteria)
                   (= (length focus-criteria)
                      (length focus-specs)))
        (setf focus-criteria (default-focus-criteria)))
      
      
      (let ((stream (clim:get-frame-pane frame 'var-list))
            ;; modify a copy
            (crit (copy-list focus-criteria))
            (topic-order (let ((temp nil))
                           (dolist (topic focus-specs)
                             (push (if (stringp (first topic))
				       (first topic) (first (first topic)))
				   temp))
                           (reverse temp)))
            (new-var-list nil)
            (changed nil)
            )
        
        (clim:accepting-values
         (stream :resynchronize-every-pass t :own-window t
                 :label "Focus on vars...")
         
         (clim:with-text-family (:sans-serif stream)
           (clim:with-text-size (:small stream)
             (dolist (topic focus-specs)
               (fresh-line stream)
               (let ((counter (position (if (stringp (first topic))
					    (first topic) (first (first topic)))
					topic-order)))
                 (setf (elt crit counter)
		   (clim:accept (if (stringp (first topic))
				    (cons 'clim:member-alist (list (rest topic)))
				  (second (first topic)))
				:default (elt crit counter)
				:prompt (if (stringp (first topic))
					    (first topic) (first (first topic)))
				:stream stream))))))
         
         (fresh-line stream)
         (clim:with-text-face (:italic stream)
           (clim:accept-values-command-button 
            (stream :resynchronize t)
            "Click here to reset focus to all vars"
            (setq crit (default-focus-criteria))))
         
         (fresh-line stream)
         (clim:with-text-face (:italic stream)
           (clim:accept-values-command-button 
            (stream :resynchronize t)
            "Click here to refresh window"
            (clim:window-refresh stream)))
         
         (fresh-line stream)
         (setf new-var-list (get-focused-var-list crit csp focus-specs))
         (let ((num (length new-var-list)))
           (clim:with-text-face ((if (zerop num) :bold :roman) stream)
             (if (zerop num)
               (format stream "WARNING! "))
             (format stream "~d vars will be in focus set" num)))
         
         )                              ; end accepting-values
        
        ;; execute if not aborted
        ;; record decision
        (setf focus-criteria crit)
        
        ;; set new list
        ;; have new var list
        (when (null new-var-list)
          (clim:notify-user frame
                            "No vars selected, resetting to all...")
          (setf new-var-list (copy-list (user::all-vars csp)))
          (setf focus-criteria (default-focus-criteria)))
        
        ;; changed? 
        (setf changed
              (not (equal focus-vars new-var-list)))
        
        ;; save the new focus list
        (setf focus-vars new-var-list)
        
        ;; fix current-var
        (when (not (member current-var focus-vars))
          (setf current-var (first focus-vars))
          (update-view-cache-values frame)
          (setf changed t))
	
	;; fix var groups -- both focus-vars and current-var could
	;; have changed
	(group-vars-for-display frame)
	(display-schedule-picture 
	 frame
	 (clim:get-frame-pane frame 'schedule-picture))
	
        ))))


(clim:define-command (COM-SORT
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Sort" 
                      :name "Sort the vars by specified criteria")
  ()
  (with-slots (focus-vars) clim:*application-frame*
    (let ((choice
           (catch :cancel
             (clim:menu-choose
              (all-var-sort-criteria)))))
      (unless (or (not choice)
		  (eq choice :cancel))
        (setf focus-vars (sort-vars-by-criteria focus-vars choice))
	(group-vars-for-display clim:*application-frame*)

	(display-schedule-picture 
	 clim:*application-frame*
	 (clim:get-frame-pane clim:*application-frame* 'schedule-picture))

	))))


(clim:define-command (COM-AUTO-ASSIGN
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Auto schedule - run" 
                      :name "Run the auto scheduler")
  ()
  (do-com-auto-assign clim:*application-frame*))


(defmethod DO-COM-AUTO-ASSIGN ((frame CSP-SCHEDULE-FRAME-MIXIN)
			       &key (auto-strat *AUTO-STRAT*))
  (with-slots 
      (csp focus-vars)
      frame
    
    (let* ((stream (clim:get-frame-pane frame 'auto-status))
	   (strategy (getf (rest (first auto-strat)) :value nil))
	   (number-of-runs 1)
	   (clear-history nil)
	   (cycle-on-all-strategies nil)
	   )
      
      (clim:accepting-values
       (stream :resynchronize-every-pass t :own-window t
	       :label "Auto Schedule")
         
       (clim:with-text-family (:sans-serif stream)
	 (clim:with-text-size (:small stream)
	   (clim:with-text-face (:bold stream)
	     (format stream "~d runs of strategy ~a"
		     number-of-runs 
		     (if cycle-on-all-strategies "-each-"
		       strategy)))))

       (fresh-line stream)
       (clim:with-text-family (:sans-serif stream)
	 (clim:with-text-size (:small stream)
	   (setf strategy
	     (clim:accept (list 'clim:member-alist auto-strat)
			  :default strategy
			  :prompt "Auto strategy"
			  :stream stream))))
       (if strategy (setf cycle-on-all-strategies nil))
       
       (fresh-line stream)       
       (clim:with-text-family (:sans-serif stream)
	 (clim:with-text-size (:small stream)
	   (setf number-of-runs
	     (clim:accept '(integer 1 100)
			  :default number-of-runs
			  :prompt "Number of runs (max 100) "
			  :stream stream))))
       
       (fresh-line stream)
       (clim:with-text-family (:sans-serif stream)
	 (clim:with-text-size (:small stream)
	   (setf clear-history
	     (clim:accept 'clim:boolean
			  :default clear-history
			  :prompt "Clear previous run history?"
			  :stream stream))))
	       
       (fresh-line stream)
       (clim:with-text-face (:italic stream)
	 (clim:accept-values-command-button 
	  (stream :resynchronize t)
	  "Click here to reset"
	  (setf strategy (getf (rest (first auto-strat)) :value nil)
		number-of-runs 1
		clear-history t)))
         
       (fresh-line stream)
       (clim:with-text-face (:italic stream)
	 (clim:accept-values-command-button 
	  (stream :resynchronize t)
	  "Click here to cycle through all strategies"
	  (setf strategy nil
		cycle-on-all-strategies t)))

       (fresh-line stream)
       (clim:with-text-face (:italic stream)
	 (clim:accept-values-command-button 
	  (stream :resynchronize t)
	  "Click here to refresh window"
	  (clim:window-refresh stream)))
         
       )				; end accepting-values
        
      ;; execute if not aborted
      (when (or strategy cycle-on-all-strategies)
	
        (clim:with-end-of-line-action (:allow stream)
          (when clear-history 
            (clear-auto-run-history frame)
            (format stream "~%Clearing history..."))
	  (cond (strategy
		 (run-auto-assign-strategy
		     frame stream strategy number-of-runs))
		(cycle-on-all-strategies
		 (dolist (strat auto-strat)
		   (run-auto-assign-strategy
		       frame stream (getf (rest strat) :value)
		       number-of-runs)))
		(t nil))
	  )

	;; update the picture
	(display-schedule-picture 
	 frame (clim:get-frame-pane frame 'schedule-picture))

	;; output a table
	(clim:window-clear stream)    
	(let ((all-keywords (auto-run-keywords frame csp))
	      (all-runs (reverse (all-auto-runs frame)))
	      )

	  (fresh-line stream)
	  (clim:formatting-table
	   (stream)

	   ;; title row
	   (clim:formatting-row
	    (stream)
	    (clim:formatting-cell
	     (stream) (format stream "Run#"))
	    (clim:formatting-cell
	     (stream) (format stream "Strategy"))
	    (dolist (key all-keywords)
	      (clim:formatting-cell
	       (stream) 
	       (format stream "~a" 
		       (substitute
			#\NEWLINE #\-
			(user::string-it-downcase key))))))
	   
	   ;; other rows
	   (dolist (run all-runs)

	     (clim:with-output-as-presentation 
		 (:object run :type 'auto-run :stream stream 
			  :single-box t :allow-sensitive-inferiors nil)
	       (clim:formatting-row
		(stream)
		(with-slots (state-summary run-number strategy) run
		  ;; (format t "~%run# ~d strat ~a" run-number strategy)
		  (clim:formatting-cell
		   (stream) (format stream "~d" run-number))
		  (clim:formatting-cell
		   (stream) (format stream "~a" (string-trunc
						 (user::string-it strategy)
						 15)))
		  (dolist (key all-keywords)
		    (let ((keyval (getf state-summary key)))
		    ;; (format t " ~a" (getf state-summary key))
		    (clim:formatting-cell
		     (stream) (format stream "~a" 
				      (cond ((and (numberp keyval)
						  (not (integerp keyval)))
					     (coerce keyval 'single-float))
					    (t keyval))
				      )))))))
	     
	     )))
	
	)				;end when (or strategy)
	
      )))


(defmethod RUN-AUTO-ASSIGN-STRATEGY ((frame CSP-SCHEDULE-FRAME-MIXIN)
				     stream strategy number-of-runs)
  (with-slots (csp focus-vars) frame
    (format stream "~%Running strategy: ~a" strategy)
    (dotimes (i number-of-runs)
      (format stream "~%Run...")
      ;; swallow output
      (with-no-standard-output
	  (funcall strategy (user::obs-csp-chronika csp)
		   :only-vars focus-vars))
      (add-to-auto-run-history frame :csp csp :strategy strategy)
      (let ((run (most-recent-auto-run frame)))
	(clim:with-output-as-presentation 
	    (:object run :type 'auto-run :stream stream 
		     :single-box t :allow-sensitive-inferiors nil)
	  (format stream "~a" run)))
      (format stream "~%  ~a" (user::csp-state-summary csp))
      )))


(clim:define-command (COM-AUTO-DISPLAY
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Auto schedule - view"
                      :name "Display automatic schedule results")
    ()
  (cond
   ((not (auto-runs-p clim:*application-frame*
		      (slot-value clim:*application-frame* 'csp)))
    (clim:notify-user "No auto run data available"))
   (t 
    (let* ((frame clim:*application-frame*)
	   (stream (clim:get-frame-pane frame 'auto-status))
           (csp (slot-value frame 'csp))
           (keys (auto-run-keywords frame csp))
           (strats (cons :all (auto-run-strategies frame csp)))
           (x-key :assigned-var-count)
           (y-key :mean-preference)
           (strategy :All)
           (include-label nil)
           (x-range nil)
           (y-range nil)
           (selected-run nil)
           (gw (user::make-clim-gwindow 
                :stream stream
                :page-height 250)))
      
      (clim:accepting-values
       (stream :resynchronize-every-pass nil :own-window t
               :label "Auto schedule history")
       
       (clim:with-text-family (:sans-serif stream)
         (clim:with-text-size (:small stream)
           
           (setf x-key
	     (clim:accept (cons 'clim:member keys)
			  :default x-key
			  :prompt "X axis"
			  :stream stream))
           (fresh-line stream)
           (setf y-key
	     (clim:accept (cons 'clim:member keys)
			  :default y-key
			  :prompt "Y axis"
			  :stream stream))
           
           (fresh-line stream)
           (setf strategy
	     (clim:accept (cons 'clim:member strats)
			  :default strategy
			  :prompt "Strategy"
			  :stream stream))
           
           (fresh-line stream)
           (setf include-label
	     (clim:accept 'clim:boolean
			  :default include-label
			  :prompt "Include label on points"
			  :stream stream))
           
           (setf x-range (auto-run-keyword-range frame csp x-key))
           (setf y-range (auto-run-keyword-range frame csp y-key))
	   ;; expand range a little so points aren't lost in boundary
           (flet ((expand-range (range)
                    (let* ((min (first range)) (max (second range))
                           (span (if (= min max) 1 (- max min))))
                      (list (- min (* 0.05 span))
                            (+ max (* 0.05 span))))))
             (setf x-range (expand-range x-range))
             (setf y-range (expand-range y-range)))
           
           
           (fresh-line stream)
           (clim:with-room-for-graphics
	       (stream)
             (let* ((x-range-diff (- (second x-range) (first x-range)))
                    (y-range-diff (- (second y-range) (first y-range)))
                    )
               (setf x-range-diff (if (zerop x-range-diff) 1 x-range-diff))
               (setf y-range-diff (if (zerop y-range-diff) 1 y-range-diff))
               (user::set-drawing-destination gw stream)
               (user::set-page-loc-of-drawing-rect gw 0 250 0 250)
               (user::set-loc-of-drawing-window gw 0.1 0.9 0.1 0.9)
               (user::set-x-window-scale gw (first x-range) (+ (first x-range) x-range-diff))
               (user::set-y-window-scale gw (first y-range) (+ (First y-range) y-range-diff))
               (user::record-font gw "Helv" 6)
               (user::box-rect gw)
               (user::string-at-line-in-rect 
                gw :line-number 1 :font-size 8
                :string (format nil "~a vs. ~a" x-key y-key))
               (user::box-window gw)
	       ;; x axis label
               (let* ((scale (nice-scale (first x-range) (second x-range)))
                      (label-scale (first scale)))
                 (when label-scale
                   (user::label-x-axis gw (first label-scale) (second label-scale)
                                       :tic-value (third label-scale)
                                       :tic-size 6 :label t :label-format (fourth label-scale)))
                 (dolist (s (rest scale))
                   (user::label-x-axis gw (first s) (second s)
                                       :tic-value (third s)
                                       :tic-size 3 :label nil)))
	       ;; y axis
               (let* ((scale (nice-scale (first y-range) (second y-range)))
                      (label-scale (first scale)))
                 (when label-scale
                   (user::label-y-axis gw (first label-scale) (second label-scale)
                                       :tic-value (third label-scale)
                                       :tic-size 6 :label t :label-format (fourth label-scale)))
                 (dolist (s (rest scale))
                   (user::label-y-axis gw (first s) (second s)
                                       :tic-value (third s)
                                       :tic-size 3 :label nil)))
               
               
	       ;; (clim:draw-rectangle*  stream 0 0 x-size y-size :filled nil)
               (when (and x-range y-range)
                 (dolist (run (all-auto-runs frame))
                   (when (and (eq (slot-value run 'csp) csp)
                              (or (eq :all strategy)
                                  (equal (slot-value run 'strategy) strategy)))
                     (let* ((summ (slot-value run 'state-summary))
                            (x (getf summ x-key))
                            (y (getf summ y-key))
                            )
                       (clim:with-output-as-presentation 
			   (:object run :type 'auto-run :stream stream 
				    :single-box t :allow-sensitive-inferiors nil)
                         (user::plot-symbol-at-point
                          gw x y :symbol user::*cross-symbol*
                          :symbol-scale 2
                          :string (if include-label (format nil "~a" run) nil)
                          ))))))))

           (fresh-line stream)
           (setf selected-run
	     (clim:accept '(clim:null-or-type auto-run)
			  :default selected-run
			  :prompt "Run to restore"
			  :stream stream))
           
           (when selected-run
             (let ((s (slot-value selected-run 'state-summary)))
               (format 
		stream "(assignments: ~d, # conf vars: ~d, mean pref: ~,2f, mean conflicts: ~,2f)"
		(user::get-csp-state-summary-value s :type :assigned-var-count :default 0)
		(user::get-csp-state-summary-value s :type :conflicting-var-count :default 0)
		(user::get-csp-state-summary-value s :type :mean-preference :default 0)
		(user::get-csp-state-summary-value s :type :mean-conflicts :default 0)
		)))
           
           )))				;end clim:accepting-values
      
      (when selected-run
        (user::make-listed-assignments
         csp :assignment-list (slot-value selected-run 'assignments)
         :var-mapping #'(lambda (x) (user::var-named csp x)))
	(display-schedule-picture 
	 clim:*application-frame*
	 (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
	)
      
      ))))

   
(clim:define-command (COM-ASSIGN-BY-DIALOG
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Assign..." 
                      :name "Assign the currently selected var")
    ()
  (with-slots
      (current-var csp) clim:*application-frame*
    
    (flet ((val-string (title val)
	     (cond (val
		    (format nil "~a: ~a pref: ~3a conf: ~3a"
			    title
			    (user::task-formatted-abs-qtime
			     (user::task current-var)
			     (user::FDSIA-val-to-time 
			      (user::FDSIA (user::task current-var)) val))
			    (user::value-preference current-var val)
			    (user::conflicts-on-value current-var val)))
		   (t "~a: -none-" title))))
      
      (let ((choice
	     (catch :cancel
	       (clim:menu-choose 
		`((,(val-string "First possible time         " 
				(user::first-value-lexical-order current-var))
		   :value ,(user::first-value-lexical-order current-var))
		  (,(val-string "Last possible time          " 
				(user::last-value-lexical-order current-var))
		   :value ,(user::last-value-lexical-order current-var))
		  (,(val-string "First min-conflicts time    " 
				(first (user::all-min-conflicts-values current-var)))
		   :value ,(first (user::all-min-conflicts-values current-var)))
		  (,(val-string "Last min-conflicts time     "
				(first (last (user::all-min-conflicts-values current-var))))
		   :value ,(first (last (user::all-min-conflicts-values current-var))))
		  (,(val-string "Max preference min-conflicts"
				(first (user::all-min-conflicts-values-ordered-by-preference 
					current-var)))
		   :value ,(first (user::all-min-conflicts-values-ordered-by-preference 
				  current-var))))))))
	
	(when (and choice (not (eq choice :cancel)))
	  (user::assign-value current-var choice)
	  
	  (display-schedule-picture 
	   clim:*application-frame*
	   (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
	  
	  )))))


(clim:define-command (COM-UNASSIGN
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Unassign" 
                      :name "Unassign the currently selected var")
    ()
  (with-slots
      (current-var) clim:*application-frame*
    (when (and (user::has-assigned-value-p current-var)
               (not (user::locked-p current-var)))
      (user::unassign-value current-var)
      )

    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
    ))

(clim:define-command (COM-UNASSIGN-ALL
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Unassign All" 
                      :name "Unassign all vars")
    ()
  (with-slots (focus-vars) clim:*application-frame*
    (dolist (var focus-vars)
      (user::unassign-value var))
    
    (display-schedule-picture 
     clim:*application-frame*
     (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
    ))


(clim:define-command (COM-LOCK
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Lock" 
                      :name "Lock this var at its current assignment")
    ()
  (with-slots (current-var) clim:*application-frame*
    (if (user::has-assigned-value-p current-var)
	(user::lock-var current-var (user::assigned-value current-var)))
    
    ))


(clim:define-command (COM-LOCK-ALL
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Lock all"
                      :name "Lock all focus vars at their current assignments")
    ()
  (with-slots (focus-vars) clim:*application-frame*
    (dolist (var focus-vars)
      (when (and (user::has-assigned-value-p var)
                 (not (user::ignored-p var))
                 (not (user::locked-p var)))
        (user::lock-var var (user::assigned-value var))))
    ))


(clim:define-command (COM-UNLOCK
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Unlock" 
                      :name "Unlock this var (without changing assignment)")
    ()
  (with-slots (current-var) clim:*application-frame*
    (if (user::locked-p current-var)
	(user::unlock-var current-var :unassign nil))
    ))


(clim:define-command (COM-UNLOCK-ALL
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Unlock all"
                      :name "Unlock all focus vars (without changing current assignments)")
    ()
  (with-slots (focus-vars) clim:*application-frame*
    (dolist (var focus-vars)
      (when (and (user::locked-p var)
                 (not (user::ignored-p var)))
        (user::unlock-var var :unassign nil)))
    
    ))


(clim:define-command (COM-IGNORE
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Ignore..." 
                      :name "Ignore selected vars")
  ()
  (with-slots (current-var csp focus-vars) clim:*application-frame*
    (let ((choice (if (not (user::ignored-p current-var)) :selected :focus-set))
          (stream (clim:get-frame-pane clim:*application-frame* 'pointer-doc)))

      (clim:accepting-values
       (stream :resynchronize-every-pass nil :own-window t
               :label "Ignore vars")
       
       (setf choice
             (clim:accept (cond ((user::ignored-p current-var)
                                 '(clim:member :focus-set))
                                (t '(clim:member :selected :focus-set)))
                          :default choice
                          :prompt "Ignore"
                          :stream stream))
       (fresh-line stream)
       (clim:with-text-face ((if (eq choice :selected) :bold nil) stream)
         (format stream "Selected var is: ~a" current-var))
       (fresh-line stream)
       (clim:with-text-face ((if (eq choice :focus-set) :bold nil) stream)
         (format stream "Focus set includes ~d (of ~d total vars)"
                 (length focus-vars) (length (user::all-vars csp))
                 )))
      
      (cond ((eq choice :selected)
             (user::unlock-var current-var)
             (user::unassign-value current-var)
             (user::ignore-var current-var))
            ((eq choice :focus-set)
             (dolist (var focus-vars)
               (user::unlock-var var)
               (user::unassign-value var)
               (user::ignore-var var)))
            (t nil))
      
      (display-schedule-picture 
       clim:*application-frame*
       (clim:get-frame-pane clim:*application-frame* 'schedule-picture))

      )))


(clim:define-command (COM-UNIGNORE
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Unignore..." 
                      :name "Unignore selected vars")
  ()
  (with-slots (current-var csp focus-vars) clim:*application-frame*
    (let ((choice (if (user::ignored-p current-var) :selected :focus-set))
          (stream (clim:get-frame-pane clim:*application-frame* 'pointer-doc)))
      
      (clim:accepting-values
       (stream :resynchronize-every-pass nil :own-window t
               :label "Unignore vars")
       
       (setf choice
             (clim:accept (cond ((user::ignored-p current-var)
                                 '(clim:member :selected :focus-set))
                                (t '(clim:member :focus-set)))
                          :default choice
                          :prompt "Unignore"
                          :stream stream))
       (fresh-line stream)
       (clim:with-text-face ((if (eq choice :selected) :bold nil) stream)
         (format stream "Selected var is: ~a" current-var))
       (fresh-line stream)
       (clim:with-text-face ((if (eq choice :focus-set) :bold nil) stream)
         (format stream "Focus set includes ~d (of ~d total vars)"
                 (length focus-vars) (length (user::all-vars csp))
                 )))
      
      (cond ((and (eq choice :selected)
		  (user::ignored-p current-var))
             (user::unignore-var current-var))
            ((eq choice :focus-set)
             (dolist (var focus-vars)
	       (if (user::ignored-p var)
		   (user::unignore-var var))))
            (t nil)))))

    
(clim:define-command (COM-WHY-CONFLICTS
                      :command-table CSP-SCHEDULE-VAR-COMMANDS
                      :menu "Explain conflicts" 
                      :name "Explain why this var has conflicts on its current assignment")
    ()
  (with-slots (csp current-var) clim:*application-frame*
    (let ((stream (clim:get-frame-pane clim:*application-frame* 'pointer-doc))
          (unassign nil)
          (why (user::why-conflicts csp current-var (user::assigned-value current-var)))
          )
      (clim:accepting-values
       (stream :resynchronize-every-pass nil :own-window t
               :label "Explain conflicts")
       
       (clim:with-text-family (:sans-serif stream)
         (clim:with-text-size (:small stream)
           
	   ;; title
           (clim:updating-output
            (stream :unique-id :why-conflicts :cache-value t)
            
            (format stream "Conflicts with var ~a due to:"
                    (user::var-name current-var))
            
            (dolist (reason why)
              (format stream "~%~a [~d] ~a" 
                      (user::target-name (user::task (first reason)))
                      (second reason) (third reason)))
            
            (fresh-line stream)
            (format stream "----------------------------------------")
            (fresh-line stream)
            )

           (setf unassign
	     (clim:accept 'clim:boolean
			  :default unassign
			  :prompt "Unassign the conflicting vars listed above?"
			  :stream stream))
           )))				; end accepting values
      
      (when unassign
        (user::unassign-conflicting-vars 
	 current-var (user::assigned-value current-var))

	(display-schedule-picture 
	 clim:*application-frame*
	 (clim:get-frame-pane clim:*application-frame* 'schedule-picture))
	)
      
      )))


;;; ======================================================================
;;;                                                      DISPLAY FUNCTIONS
;;; ======================================================================


(defmethod DISPLAY-VAR-LIST-CONTROL ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)
  (with-slots
      (current-var focus-vars csp focus-criteria var-group var-groups
       var-group-size) frame
    
    (let ((group-num (if var-group (1+ (first var-group)) 0))
	  (group-count (length var-groups))
	  (focus-var-count (length focus-vars))
	  (all-vars-count (if csp (length (user::all-vars csp)) 0))
	  (next-group (next-var-group frame))
	  (prev-group (prev-var-group frame))
	  )
      
      (clim:updating-output
       (stream :unique-id (cons frame :display-var-list-control)
	       :id-test #'equal
	       :cache-value (list focus-criteria group-num 
				  next-group prev-group)
	       :cache-test #'equal)

       (clim:with-text-face (:bold stream)

	 (clim:with-output-as-presentation
	     (:object nil :type 'VAR-JUMP-TO-PAGE-BUTTON :stream stream
		      :single-box t :allow-sensitive-inferiors nil)
	   (format stream "Observations page ~d of ~d  " group-num group-count))
	 
	 (when focus-criteria
	   (format stream " (focus: ~d of ~d)  "
		   focus-var-count all-vars-count))
	 )
	 
       (when prev-group
	 (clim:with-output-as-presentation
	     (:object nil :type 'VAR-PAGE-UP-BUTTON :stream stream 
		      :single-box t :allow-sensitive-inferiors nil)
	   (clim:with-text-face (nil stream)
	     (format stream " Previous page ")
	     )))

       (when next-group
	 (clim:with-output-as-presentation
	     (:object nil :type 'VAR-PAGE-DOWN-BUTTON :stream stream 
		      :single-box t :allow-sensitive-inferiors nil)
	   (clim:with-text-face (nil stream)
	     (format stream " Next page")
	     )))
	 
       ))))



(defmethod DISPLAY-VAR-LIST ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)
  (with-slots
      (current-var focus-vars csp focus-criteria var-group var-groups
       var-group-size) frame

    (let* ((vars (second var-group))
	   (var nil)
           (lh (+ 3 (clim:stream-line-height stream)))
	   (cy 0)
	   ;; this is a list with min (highest) priority first, max (lowest)
	   ;; priority second
           (min-max-prio (if csp (user::min-max-priority csp) '(1 2)))
	   )
    
    (clim:updating-output
     (stream :unique-id (cons frame :display-var-list)
	     :id-test #'equal)
     
     (clim:formatting-table 
      (stream)
      
      ;; title row
      (clim:updating-output 
       (stream :unique-id :title :cache-value var-group
	       :cache-test #'equal)
       (clim:formatting-row
	(stream)
	(clim:formatting-cell (stream) 
				(write-string " " stream))
	(clim:formatting-cell (stream) 
			      (clim:with-text-face (:bold stream)
				(clim:with-text-size (:very-small stream)
				  (write-string "Observation" stream))))
	(clim:formatting-cell (stream) 	
			      (clim:with-text-face (:bold stream)
				(clim:with-text-size (:very-small stream)
				  (write-string "Prio." stream))))
	
	))
      
      (dotimes (counter var-group-size)
	(setf var (pop vars))
	
	(clim:formatting-row 
	 (stream)

	 (clim:formatting-cell
	  (stream)
	  (clim:updating-output
	   (stream :unique-id (cons counter :col0) :id-test #'equal
		   :cache-value (if var
				    (list var (user::ignored-p var)
					  (user::has-assigned-value-p var)
					  (user::assigned-value-has-conflicts-p var)
					  (user::locked-p var)
					  (user::priority var))
				  nil)
		   :cache-test #'equal)
	   
	   (cond
 	    ((not var)) ;draw nothing
	    ((user::ignored-p var)
	     (clim:with-drawing-options
		 (stream :ink clim:+light-gray+)
	       (draw-var-circle
		stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh) ; x y radius diagonal
		:filled nil :diagonal nil))
	     )
	    (t
	     (draw-var-circle
	      stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh) ; x y radius diagonal
	      :filled   (user::has-assigned-value-p var)
	      :diagonal (user::assigned-value-has-conflicts-p var))))
	  
	   (if (and var (user::locked-p var))
	       (draw-var-lock 
		stream (* 1.25 lh) (+ cy (* 0.75 lh)) (* 0.12 lh) (* 0.35 lh)))
	  
	   (if var
	       (draw-var-prio 
		stream (* 1.75 lh) (+ cy (* 1.0 lh)) (* 0.75 lh) (* 0.25 lh) 
		(let ((p (user::priority var)))
		  (cond ((= p (first min-max-prio)) 1)
			((= p (second min-max-prio)) 3)
			(t 2)))))
	   ))

	 
        (clim:formatting-cell
         (stream :align-x :left)
         (clim:updating-output 
          (stream :unique-id (cons counter :col1) :id-test #'equal		  
                  :cache-value (cons var (equal var current-var)) :cache-test #'equal)
	  (when var
	    ;;   (if (equal var current-var)
	    ;;	   (clim:with-text-face (:bold stream) 
	    ;;       (clim:present var 'var :stream stream))
	    ;;     (clim:present var 'var :stream stream))
	    (clim:with-output-as-presentation 
		(:object var :type 'var :stream stream 
			 :single-box t :allow-sensitive-inferiors nil)
	      (clim:with-text-face
		  ((if (equal var current-var) :bold nil) stream)
		(format stream "~a (#~a)" (user::var-name var) (user::var-number var))))
	    )))
	 
        (clim:formatting-cell
         (stream :align-x :center)
         (clim:updating-output 
          (stream :unique-id (cons counter :col2) :id-test #'equal
                  :cache-value (cons var (if var (user::priority var)))
		  :cache-test #'equal)
	  (when var
	    (if (user::priority var)
		(format stream "~d" (user::priority var))
	      (write-string "-" stream)))))        
	
        )))))))


(defun DRAW-VAR-CIRCLE (stream x y radius diag &key (filled nil) (diagonal nil))
  (clim:draw-circle* 
   stream x y radius :filled filled)
  (when diagonal
    (clim:draw-line*
     stream (- x diag) (- y diag) (+ x diag) (+ y diag)
     :line-thickness 1)
    (clim:draw-line*
     stream (- x diag) (+ y diag) (+ x diag) (- y diag)
     :line-thickness 1)))


(defun DRAW-VAR-LOCK (stream x y radius body)
  (clim:draw-circle*
   stream x y radius :filled nil)
  (let ((half-body (/ body 2)))
    (clim:draw-rectangle*
     stream (- x half-body) y (+ x half-body) (+ y (* 0.75 body)))))


(defun DRAW-VAR-PRIO (stream x y height width priority)
  ;; x, y are lower left.  Priority is 1 (highest) to 3 (lowest)
  (let* ((yinc (/ height 3.0))
	 (dy-top (* (- 4 priority) yinc))
	 (dy-bottom (* (- 3 priority) yinc)))
    (clim:draw-rectangle*
     stream x y (+ x width) (- y height) :filled nil)
    (clim:draw-rectangle*
     stream x (- y dy-bottom) (+ x width) (- y dy-top) :filled t)
    ))


(defmethod DISPLAY-VAR-DESCRIP ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)
  (with-slots
      (current-var) frame
          
    (clim:updating-output
     (stream :unique-id (cons frame :display-var-descrip)
	     :id-test #'equal
	     :cache-value (var-descrip-cache-value frame current-var)
	     :cache-test #'equal)

     (var-descrip-to-stream frame stream current-var))))


(defmethod VAR-DESCRIP-CACHE-VALUE ((frame CSP-SCHEDULE-FRAME-MIXIN) current-var)
  "return something to use as cache value for display-var-desrip"
  (list current-var
	(if current-var (user::assigned-value current-var))
	(if current-var (user::locked-p current-var))
	(if current-var (user::ignored-p current-var))
	))
  

;;; NOTE: this version is generic, see override method for Astro-D 
;;; specific method

(defmethod VAR-DESCRIP-TO-STREAM ((frame CSP-SCHEDULE-FRAME-MIXIN)
				  stream current-var)
  "write desription of current-var to stream.  Updating output
           is handled by caller"
  
  (let* ((task (user::task current-var))
	 )
  
    ;; --- title
    (clim:with-text-face (:bold stream)
      (format stream "Var: ~a (#~d), Target name: ~a"
	      (or (user::var-name current-var) "-")
	      (user::var-number current-var)
	      (user::target-name task)))
       	
    ;; --- ra & dec
    (format stream "~%RA: ~,3f (~a), Dec: ~,3f (~a)"
	    (user::target-ra task)
	    (let ((hms (user::degrees-of-ra-to-hms 
			(user::target-ra task))))
	      (format nil "~dh ~2,'0dm ~,1fs" 
		      (first hms) (second hms) (third hms)))
	    (user::target-dec task)
	    (let ((dms (user::degrees-of-dec-to-dms 
			(user::target-dec task))))
	      (format nil "~dd ~2,'0dm ~ds" 
		      (first dms) (second dms) 
		      (round (third dms)))))
         
    ;; --- exposure time
    (format stream "~%Exp. time ~a sec, Priority ~a" 
	    (user::exp-time task)
	    (user::priority current-var)
	    )
       
    ;; --- assignment
    (cond ((user::assigned-value current-var)
	   (format stream "~%Assigned to ~a (time ~a = ~a), ~d confl (min confl: ~a), pref: ~a"
		   (user::assigned-value current-var)
		   (user::assigned-time current-var)
		   (user::task-formatted-abs-qtime 
		    task (user::assigned-time current-var))
		   (user::conflicts-on-assigned-value current-var)
		   (user::min-conflicts current-var)
		   (user::assigned-value-preference current-var)))
	  (t (format stream "~%No assigned value, min confl: ~a"
		     (user::min-conflicts current-var))))
       
    ;; --- locked, ignored
    (format stream "~%~a ~a"
	    (if (user::locked-p current-var) "Locked" "Unlocked")
	    (if (user::ignored-p current-var) "Ignored" "Unignored"))
    ;; --- number of values in domain
    (format stream ", values total ~a, in domain ~a"
	    (user::value-count current-var)
	    (user::values-in-domain-count current-var))

      
    ))


(defmethod DISPLAY-VAR-MENU ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)

  (enable-disable-var-menu frame)
  
  (draw-fixed-size-command-table 
   frame stream 'CSP-SCHEDULE-VAR-COMMANDS 
   "Commands" :var-menu))


(defmethod ENABLE-DISABLE-VAR-MENU ((frame csp-schedule-frame-mixin))
  (with-slots (current-var focus-vars csp) frame
    
    ;; unconditional
    (clim:enable-command 'COM-FOCUS frame)
    (clim:enable-command 'COM-SORT frame)
    (clim:enable-command 'COM-AUTO-ASSIGN frame)
    
    ;; depend on current-var
    (when (and current-var csp)
      (if (and current-var
	       (not (user::locked-p current-var)))
	  (clim:enable-command 'COM-ASSIGN-BY-DIALOG frame)
	(clim:disable-command 'COM-ASSIGN-BY-DIALOG frame))
      (if (and current-var
               (user::has-assigned-value-p current-var)
               (not (user::locked-p current-var)))
	  (clim:enable-command  'COM-UNASSIGN frame)
        (clim:disable-command 'COM-UNASSIGN frame))
      (clim:enable-command 'COM-IGNORE frame)
      (if (and (user::has-assigned-value-p current-var)
               (not (user::locked-p current-var))
               (not (user::ignored-p current-var)))
	  (clim:enable-command  'COM-LOCK frame)
        (clim:disable-command 'COM-LOCK frame))
      (if (and (user::locked-p current-var) 
               (not (user::ignored-p current-var)))
	  (clim:enable-command  'COM-UNLOCK frame)
        (clim:disable-command 'COM-UNLOCK frame))
      (if (and (user::has-assigned-value-p current-var)
               (user::assigned-value-has-conflicts-p current-var))
	  (clim:enable-command  'COM-WHY-CONFLICTS frame)
        (clim:disable-command 'COM-WHY-CONFLICTS frame))
      )
   
    (when focus-vars

      (if (dolist (var focus-vars)
	    (if (and (not (user::locked-p var))
		     (not (user::ignored-p var))
		     (user::has-assigned-value-p var))
		(return t)))
	  (clim:enable-command  'COM-UNASSIGN-ALL frame)
        (clim:disable-command 'COM-UNASSIGN-ALL frame))

      
      (if (dolist (var focus-vars)
            (if (and (user::has-assigned-value-p var)
                     (not (user::locked-p var))
                     (not (user::ignored-p var)))
		(return t)))
	  (clim:enable-command  'COM-LOCK-ALL frame)
        (clim:disable-command 'COM-LOCK-ALL frame))
      
      (if (dolist (var focus-vars)
            (if (and (user::locked-p var) 
                     (not (user::ignored-p var)))
		(return t)))
	  (clim:enable-command  'COM-UNLOCK-ALL frame)
        (clim:disable-command 'COM-UNLOCK-ALL frame))
      )
    
    ;; depends on auto runs
    (if (auto-runs-p frame csp)
	(clim:enable-command 'COM-AUTO-DISPLAY frame)
      (clim:disable-command 'COM-AUTO-DISPLAY frame))
    
    ))


;;; --- auxiliary display: draw a command menu

;; method to draw a menu fairly generally, with incremental display and with
;; italicized menu items if disabled


(defmethod DRAW-FIXED-SIZE-COMMAND-TABLE ((frame CSP-SCHEDULE-FRAME-MIXIN) 
                                          stream table title unique-keyword)
  (let ((command-table (clim:find-command-table table))
        (lh (+ 3 (clim:stream-line-height stream)))
        (counter 0)
	(command-table-list nil)
        )

    (setf command-table-list (list command-table))
    (dolist (inh (clim:command-table-inherit-from command-table))
      (setf command-table-list
	(cons inh command-table-list)))
    
    (clim:updating-output
     (stream :unique-id unique-keyword)
     
     (clim:updating-output
      (stream :unique-id :title :cache-value t)
      (clim:draw-text* stream title 0 lh
                       :text-face :bold	; '(:bold :italic)
                       :text-family :sans-serif :text-size :normal)
      )
     
     (dolist (a-command-table command-table-list)
       (clim:map-over-command-table-menu-items 
	#'(lambda (menu-name key menu-item)
	    (declare (ignore key))
	    (incf counter)
	    (let ((enabled-p (clim:command-enabled-p 
			      (clim:command-name 
			       (clim:command-menu-item-value menu-item)) frame))
		  (cx 0)
		  (cy (* (+ 1 counter) lh)))
	      (clim:updating-output
	       (stream :unique-id counter
		       :cache-value enabled-p)
             
	       (cond (enabled-p
		      (clim:with-output-as-presentation
			  (:object (clim:command-menu-item-value menu-item)
				   :type (list 'clim:command :command-table command-table)
				   :stream stream
				   :single-box t)
			;; note: present does not italicize disabled commands, so have to
			;; do this manually.  The following does present a command, however:
			;;(clim:present (clim:command-menu-item-value menu-item)
			;;              (list 'clim:command :command-table command-table)
			;;              :stream stream)
			(clim:draw-text* stream menu-name cx cy)))
		     (t			; disabled
		      (clim:draw-text* stream menu-name cx cy :text-face :italic)
		      )))))
	a-command-table)))))


(defmethod DISPLAY-SCHEDULE-STATUS ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)
  (with-slots (csp) frame
    (let* ((state (user::csp-state-summary csp)))
      
      (clim:updating-output
       (stream :unique-id (cons frame :schedule-status)
	       :cache-value (schedule-status-cache-value 
			     frame :state state)
	       :id-test #'equal
               :cache-test #'equal)
       
       ;; (clim:window-clear stream)
       
       (clim:with-end-of-line-action
	   (:allow stream)
	 
	 (clim:with-text-face (:bold stream)
	   (clim:with-text-size (:small stream)
	     (format stream "Schedule status")))
	 (fresh-line stream)
	 
	 (schedule-status-to-stream frame stream
				    :state state))))))


(defmethod SCHEDULE-STATUS-CACHE-VALUE ((frame CSP-SCHEDULE-FRAME-MIXIN) 
					&key state)
  (with-slots (csp) frame
    
    (cons csp
	  (append (user::locked-var-list csp)
		  (user::ignored-var-list csp)
		  state))
    ))
  

(defmethod SCHEDULE-STATUS-TO-STREAM ((frame CSP-SCHEDULE-FRAME-MIXIN) stream
				      &key state)

  
  (with-slots (csp) frame
    
    (format stream "~d Observations" (length (user::all-vars csp)))    
    (fresh-line stream)
    (format stream "of which:")
    (fresh-line stream)
    (format stream "  ~d locked" (length (user::locked-var-list csp)))
    (fresh-line stream)
    (format stream "  ~d ignored" (length (user::ignored-var-list csp)))    
    (fresh-line stream)
    (format stream "leaving ~d, of which:"
	    (- (length (user::all-vars csp))
	       (+ (length (user::locked-var-list csp))
		  (length (user::ignored-var-list csp)))))
    (fresh-line stream)
    (format stream " ~d are assigned"
	    (user::get-csp-state-summary-value 
	     state :type :assigned-var-count :default 0))
    (fresh-line stream)
    
    (let ((conf (user::get-csp-state-summary-value 
		 state :type :conflicting-var-count :default 0)))
      (cond ((= 0 conf)
	     (format stream "No conflicts"))
	    (t (format stream "~d conflicts" conf))))
    (fresh-line stream)
	 

    (format stream "~8,3f mean confl"
	    (user::get-csp-state-summary-value state :type :mean-conflicts :default 0))
    (fresh-line stream)
    (format stream "~8,3f mean pref"
	    (user::get-csp-state-summary-value state :type :mean-preference :default 0))
    (format stream "~%")
    
    ))


#|
----------------------------------------------------------------------
Notes:

how to access some info about the chronika:
(chronika (user::chronika (user::task current-var)))
(qstart (user::qtime-start chronika))
(qend (user::qtime-end chronika))
(tjd-start (user::qtime-to-tjd chronika qstart))
(tjd-end (user::qtime-to-tjd chronika qend))
(value-count (user::value-count current-var))

;;; next function not needed, but this is how to do it if it ever is:
(defun REDISPLAY-SCHEDULE-PICTURE ()
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'schedule-picture)))
    (clim:window-clear stream)
    (display-schedule-picture clim::*application-frame* stream))
  )
----------------------------------------------------------------------
|#


(defgeneric DISPLAY-SCHEDULE-PICTURE (frame stream)
  (:documentation
   "Drawing function for the schedule-picture frame"))


(defmethod DISPLAY-SCHEDULE-PICTURE ((frame CSP-SCHEDULE-FRAME-MIXIN) stream)
  
  (with-slots (current-var csp gwindow) frame
          
    (when current-var

      (clim:window-clear stream)
      
      (user::record-prolog gwindow)
      
      (draw-preferences frame :stream stream
			:page-bottom 165 :page-top 235)
      
      (clim:with-output-as-presentation 
          (:object current-var
                   :type 'VAR-PICTURE
                   :stream stream
                   :single-box t)
	(user::box-window gwindow :linewidth 1)
	)
      
      (draw-efficiency frame :stream stream
		       :page-bottom 110 :page-top 145)
      
      (draw-resources frame :stream stream
		      :page-bottom 20 :page-top 80)
      (draw-conflicts frame :stream stream
		      :page-bottom 85 :page-top 105)
              
      (user::record-wrapup gwindow)
      )))


(defmethod DRAW-CONFLICTS ((frame CSP-SCHEDULE-FRAME-MIXIN) &key
                           stream page-top page-bottom 
                           )
  (declare (ignore stream))
  (with-slots 
    (current-var gwindow page-left page-right wleft wright
                 val-to-x-array)
    frame
    

    
    (when current-var
      
      (let* ((task (if current-var (user::task current-var)))
             (chronika (if task (user::chronika task)))
             (max-conf 0)
             (value-count (user::value-count current-var))
             )
        
        (user::set-page-loc-of-drawing-rect 
         gwindow page-left page-right page-bottom page-top)
        (user::set-loc-of-drawing-window 
         gwindow wleft wright 0.0 1.0) ;LRBT
        (user::set-y-window-scale gwindow 0 1)
        (user::set-x-window-scale 
         gwindow (- (user::qtime-start chronika) 0.5) 
         (+ (user::qtime-end chronika) 0.5))
        (user::box-window gwindow)
        (user::record-font gwindow "Helv" 9)
        (user::string-at-line-in-rect
         gwindow 
         :line-number 1 :font-size 9
         :string "CONFLICTS")
        
        
        (dotimes (val value-count)
          (setf max-conf (max max-conf (or (user::conflicts-on-value current-var val) 0))))
        (setf max-conf (* 1.2 max-conf))
        
        (when (> max-conf 0)
          (let* ((ybase (round (user::v-to-page-y gwindow 0)))
                 (prevy ybase)
                 (prevx nil))
            (dotimes (val value-count)
              (let* ((x1x2 (aref val-to-x-array val))
                     (x1 (first x1x2))
                     (x2 (second x1x2))
                     (conf (or (user::conflicts-on-value current-var val) 0))
                     (ytop (round (user::v-to-page-y gwindow (/ conf max-conf)))))
                (cond ((null prevx)
                       (user::record-line gwindow x1 ybase x1 ytop)
                       (user::record-line gwindow x1 ytop x2 ytop))
                      (t (cond ((= x1 prevx)
                                (user::record-line gwindow x1 prevy x1 ytop)
                                (user::record-line gwindow x1 ytop x2 ytop))
                               (t (user::record-line gwindow prevx prevy prevx ybase)
                                  (user::record-line gwindow prevx ybase x1 ybase)
                                  (user::record-line gwindow x1 ybase x1 ytop)
                                  (user::record-line gwindow x1 ytop x2 ytop)))))
                (setf prevx x2 prevy ytop)))
            (unless (= prevy ybase)
              (user::record-line gwindow prevx prevy prevx ybase))))
        

        (user::set-y-window-scale gwindow 0 max-conf)
	(cond ((<= max-conf 5)
	       (user::label-y-axis 
		gwindow 0 max-conf :tic-value 1 :tic-size 3 :label t :label-format "~d"))
	      ((<= max-conf 50)
	       (user::label-y-axis 
		gwindow 0 max-conf :tic-value 5 :tic-size 3 :label t :label-format "~d"))
	      (t
	       (user::label-y-axis 
		gwindow 0 max-conf :tic-value 20 :tic-size 3 :label t :label-format "~d")))
        
        ))))


(defmethod DRAW-RESOURCES ((frame CSP-SCHEDULE-FRAME-MIXIN) &key
                           stream page-top page-bottom 
                           )
  (declare (ignore stream))
  (with-slots (csp current-var 
                   resource-constraint
                   gwindow page-left page-right wleft wright
                   bin-to-x-array) frame
    
    (when (and current-var resource-constraint)
      
      (let* ((task (if current-var (user::task current-var)))
             (chronika (if task (user::chronika task)))
             ;; (FDSIA (user::FDSIA task))
             (bin-count (user::bin-count resource-constraint))
             (bin-limit (user::bin-limit resource-constraint))
             (bin-usage (user::bin-usage resource-constraint))
             (max-res 0)
             (scale-factor 1.2)
             ;; temporary lists to speed drawing
             (used-list nil)
             (over-list nil)
             )
        
        (user::set-page-loc-of-drawing-rect 
         gwindow page-left page-right page-bottom page-top)      
        (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
        (user::set-y-window-scale gwindow 0 1)
        (user::set-x-window-scale 
         gwindow 
         (- (user::qtime-start chronika) 0.5) 
         (+ (user::qtime-end chronika) 0.5))
        (user::box-window gwindow)
        (user::record-font gwindow "Helv" 9)
        (user::string-at-line-in-rect
         gwindow :line-number 1 :font-size 9 :string "RESOURCE")
        (user::string-at-line-in-rect
         gwindow :line-number 2 :font-size 9 :string "USAGE")
        
        (dotimes (i bin-count)
          (setf max-res (max max-res (aref bin-usage i) (aref bin-limit i))))
        (setf max-res (* scale-factor max-res))
        
        ;; draw the limit bars
        (dotimes (i bin-count)
          (let* ((x1x2 (aref bin-to-x-array i))
                 (xleft (first x1x2))
                 (xright (second x1x2))
                 ;; (xwidth (- xright xleft))
                 (limit (aref bin-limit i))
                 (used (aref bin-usage i))
                 (ybottom (round (user::v-to-page-y gwindow 0)))
                 (ylimit (round (user::v-to-page-y gwindow (/ limit max-res))))
                 (yused (round (user::v-to-page-y gwindow (/ used max-res)))))
            (draw-gwindow-rect
             gwindow xleft ybottom xright ylimit :fill nil)
            ;; save coordinates on used-list (green) and over-list (red) so don't
            ;; have to switch gcontext too much
            (when (> used 0)
              (if (> used limit)
                (push (list (1+ xleft) (- ybottom 1) (1- xright) yused) over-list)
                (push (list (1+ xleft) (- ybottom 1) (1- xright) yused) used-list)))))
        (when used-list
          ;; draw green...
          (dolist (item used-list)
            (draw-gwindow-rect gwindow (first item) (second item) (third item) (fourth item)
                               :fill #+:CCL nil #-:CCL clim:+spring-green+)))
        (when over-list
          ;; draw red... (gray for now)
          (dolist (item over-list)
            (draw-gwindow-rect gwindow (first item) (second item) (third item) (fourth item)
                               :fill #+:CCL 0.75 #-:CCL clim:+red+)))
        
        (user::set-y-window-scale gwindow 0 scale-factor)
        (user::label-y-axis
         gwindow 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
        (user::label-y-axis 
         gwindow 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f"))
      )))


(defmethod DRAW-PREFERENCES ((frame CSP-SCHEDULE-FRAME-MIXIN) 
			     &key
			     stream page-top page-bottom 
			     (suitability-scale 100))
  
  (declare (ignore stream))
  
  (with-slots (current-var gwindow page-left page-right wleft wright
	       val-to-x-array) frame
    
    (when current-var
    
      (let* ((task (user::task current-var))
	     (chronika (user::chronika task))
	     (scale-factor 1.2)
	     (scale (* scale-factor suitability-scale))
	     )
        
	;;(clear-area view page-left py-pref-top (- page-right page-left) 
	;;   (- py-pref-bottom py-pref-top))
	
	(user::set-page-loc-of-drawing-rect
	 gwindow page-left page-right page-bottom page-top)
	(user::set-loc-of-drawing-window gwindow wleft wright 0 1)
	(user::set-y-window-scale gwindow 0 scale)
	(user::set-x-window-scale 
	 gwindow 
	 (- (user::qtime-start chronika) 0.5) 
	 (+ (user::qtime-end chronika) 0.5))
	(user::box-window gwindow)
	(user::record-font gwindow "Helv" 9)
	(user::string-at-line-in-rect
	 gwindow :line-number 1 :font-size 9
	 :string "PREFERENCES")	
	(user::string-at-line-in-window
	 gwindow :line-number 1 :font-size 9
	 :string (format nil "~a" (or (user::var-name current-var)
				      (user::var-number current-var))))
        
	(let* ((ybase (round (user::v-to-page-y gwindow 0)))
	       (prevy ybase)
	       (prevx nil))
	  (dotimes (val (user::value-count current-var))
	    (let* ((x1x2 (aref val-to-x-array val))
		   (x1 (first x1x2))
		   (x2 (second x1x2))
		   (pref (if (user::value-in-domain-p current-var val)
			     (user::value-preference current-var val) 0))
		   (ytop (round (user::v-to-page-y gwindow pref))))
	      (cond ((null prevx)
		     (user::record-line gwindow x1 ybase x1 ytop)
		     (user::record-line gwindow x1 ytop x2 ytop))
		    (t (cond ((= x1 prevx)
			      (user::record-line gwindow x1 prevy x1 ytop)
			      (user::record-line gwindow x1 ytop x2 ytop))
			     (t (user::record-line gwindow prevx prevy prevx ybase)
				(user::record-line gwindow prevx ybase x1 ybase)
				(user::record-line gwindow x1 ybase x1 ytop)
				(user::record-line gwindow x1 ytop x2 ytop)))))
	      (setf prevx x2 prevy ytop)))
	  (unless (= prevy ybase)
	    (user::record-line gwindow prevx prevy prevx ybase)))
        
	(user::set-y-window-scale gwindow 0 scale-factor)
	(user::label-y-axis 
	 gwindow 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
	(user::label-y-axis 
	 gwindow 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
        
	))))


(defmethod DRAW-EFFICIENCY ((frame CSP-SCHEDULE-FRAME-MIXIN) 
			    &key
			    stream page-top page-bottom)

  (declare (ignore stream))
  
  (with-slots (current-var gwindow page-left page-right wleft wright
                           val-to-x-array) frame
    
    (when current-var
      (let* ((task (user::task current-var))
             (chronika (user::chronika task))
             (scale-factor 1.1)

             )
        
        (user::set-page-loc-of-drawing-rect
         gwindow page-left page-right page-bottom page-top)
        (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
        (user::set-y-window-scale gwindow 0 scale-factor)
        (user::set-x-window-scale 
         gwindow 
         (- (user::qtime-start chronika) 0.5) 
         (+ (user::qtime-end chronika) 0.5))
        (user::box-window gwindow)
        (user::record-font gwindow "Helv" 9)
        (user::string-at-line-in-rect
         gwindow :line-number 1 :font-size 9
	 :string (format nil "EFFICIENCY"))

	(user::plot-pcf gwindow (user::efficiency-ipcf task) :linewidth 0.25
			:fill #+:CCL 0 #-:CCL clim:+blue+)
        
        (user::label-y-axis 
         gwindow 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
        (user::label-y-axis 
         gwindow 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
        
        ))))


;;; ======================================================================
;;;                                         GENERIC CSP SCHEDULE FRAME

(clim:define-application-frame GENERIC-CSP-SCHEDULE-FRAME

    (CSP-SCHEDULE-FRAME-MIXIN
     CSP-SCHEDULE-MOD-MANAGER
     CSP-SCHEDULE-BIN-CACHE
     AUTO-RUN-SET
     CLIM:APPLICATION-FRAME)		; superclasses
  
  ()					; slots
  
  (:command-table (GENERIC-CSP-SCHEDULE-COMMAND-TABLE
                   :inherit-from (csp-schedule-command-table
				  ;; separately defined commands
				  csp-schedule-file-commands
                                  csp-schedule-var-commands)
                   :menu (("File"      :menu    csp-schedule-file-commands)
                          ("Help"      :command (com-csp-schedule-help))
                          ("Exit"      :command (com-csp-schedule-exit))
                          ("ReDisplay" :command (com-csp-schedule-redisplay))
			  ;; redraw is for debugging only
                          ("ReDraw"    :command (com-csp-schedule-redraw))
                          )))
  
  (:command-definer DEFINE-GENERIC-CSP-SCHEDULE-COMMAND)
  
  (:panes
   ((menu             :command-menu)
    
    (var-list-control :application
		      :scroll-bars              nil
		      :default-text-style       '(:sans-serif :roman :small)
		      :incremental-redisplay    t
		      :display-after-commands   t
		      :display-function         'display-var-list-control)

    (var-list         :application
                      :scroll-bars              :both
                      :default-text-style       '(:sans-serif :roman :small)
                      :incremental-redisplay    t
                      :display-after-commands   t
                      :display-function         'display-var-list
                      )
    
    (var-descrip      :application
                      :scroll-bars              :both
                      :default-text-style       '(:sans-serif :roman :small)
                      :incremental-redisplay    nil
                      :display-after-commands   t
                      :display-function         'display-var-descrip
                      )
    
    (var-menu         :application
                      :scroll-bars              :vertical
                      :default-text-style       '(:sans-serif :roman :small)
                      :incremental-redisplay    t
                      :display-after-commands   t
                      :display-function         'display-var-menu
                      )
    
    (schedule-status  :application	; text description of state
                      :scroll-bars              :both
                      :default-text-style       '(:sans-serif :roman :small)
                      :incremental-redisplay    nil
                      :display-after-commands   t
                      :display-function         'display-schedule-status
                      )
    
    (schedule-picture :application
                      :scroll-bars              :both
                      :default-text-style       '(:sans-serif :roman :small)
                      :incremental-redisplay    nil
                      :display-after-commands   nil
                      :display-function         'display-schedule-picture
                      )
    
    (auto-status      :application
                      :scroll-bars              :both
                      :default-text-style       '(:sans-serif :roman :very-small)
                      :incremental-redisplay    nil
                      :display-after-commands   nil
                      :display-function         nil
                      )
    
    (pointer-doc      :pointer-documentation 
                      :scroll-bars nil
                      :default-text-style '(:sans-serif :roman :very-small)
                      )
    
    ))
  
  (:layout
   ((default
	(:column 1
		 (menu :compute)
		 (:row 0.45
		       (:column 3/5
				(var-list-control :compute)
				(var-list 0.55)
				(var-descrip :rest))
		       (schedule-status 1/5)
		       (var-menu :rest)
		       )
		 (auto-status 1/8)
		 (schedule-picture :rest)
		 (pointer-doc :compute))) ; end default layout
       ))

  )

;;; ======================================================================
;;;                                         INITIALIZATION

(defmethod INITIALIZE-INSTANCE :AFTER 
	   ((frame GENERIC-CSP-SCHEDULE-FRAME) &rest args)
  (declare (ignore args))

  (init-csp-schedule-frame-mixin 
   frame
   :pane 'schedule-picture
   :page-left 20 
   :page-right 1100
   :wleft 0.10 
   :wright 0.95
   :page-height 250)
  )


;;; ======================================================================
;;;                                         START AND RUN THE FRAME

#|

(run-generic-csp-schedule-frame
 :csp (user::obs-csp user::*xte-long-term-schedule*))

|#

(defun MAKE-GENERIC-CSP-SCHEDULE-FRAME (&key (root *clim-root*)
					       (force nil))
  (if force (setf *THE-GENERIC-CSP-SCHEDULE-FRAME* nil))
  ;; only make new one if there isn't one bound to the global
  (unless *THE-GENERIC-CSP-SCHEDULE-FRAME*
    (setf *THE-GENERIC-CSP-SCHEDULE-FRAME*
      (clim:make-application-frame 
       'GENERIC-CSP-SCHEDULE-FRAME
       :parent root
       :width #+:Allegro 800 #-:Allegro 600 
       :height #+:Allegro 800 #-:Allegro 350
       :pretty-name "Generic CSP Schedule"))))


(defun RUN-GENERIC-CSP-SCHEDULE-FRAME (&key csp
					    (root *clim-root*)
					    (force nil))
  
  "create and start a long-term sched application frame.  
    Arguments:
     csp - the csp instance to connect to for display"

  (let* ((process-name "Generic CSP Schedule")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal))
	 )

    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
      (if process 
	  #+:Allegro (mp:process-kill process)
	  #-:Allegro nil
	  )
      (setf *THE-GENERIC-CSP-SCHEDULE-FRAME* nil)
      )

    ;; create frame if it doesn't exist
    (unless *THE-GENERIC-CSP-SCHEDULE-FRAME*
      (make-generic-csp-schedule-frame :root root :force force))
    
    ;; store the slot contents
    (let ((frame *THE-GENERIC-CSP-SCHEDULE-FRAME*))
      (setf (slot-value frame 'var-group-size) 10)    
      (setf (slot-value frame 'csp) csp)
      (setf (slot-value frame 'focus-vars)
	(copy-list (user::all-vars csp)))
      (setf (slot-value frame 'current-var) 
	(first (user::all-vars csp)))
      (group-vars-for-display frame)
      
      ;; long-term: resource constraint is time capacity
      (setf (slot-value frame 'resource-constraint)
	(user::obs-cap-constraint (user::obs-csp-chronika csp)))
      (setf (slot-value frame 'resource-segment-definition)
	csp)
      
      ;; setup cache
      (init-graphics-cache frame)
      (update-view-cache-values frame)

      (do-com-schedule-redraw frame)
      )
    
    ;; run it
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*THE-GENERIC-CSP-SCHEDULE-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *THE-GENERIC-CSP-SCHEDULE-FRAME*)
      ))))




#|
----------------------------------------------------------------------
Notes on the design for process and frame display

The design is as follows:

For frame of type fooframe, there is a global bound to the instance
 *fooframe-frame*

The functions which creates the instance is: (make-fooframe ... &key
(force nil)) which creates the instance and binds it to
*fooframe-frame*.  If for some reason this can't be done, then the
instance is not created.  If :force is t, an old instance (if any is
bound to *fooframe-frame*) is discarded first.

The function
(run-fooframe ...)
runs the application frame in a separate process (on the Sun.)
This should create the frame if it doesn't exist.
On the Sun there are two cases:
1. The process exists: do a reset
2. The process does not exist: do mp:run-process
The fun-fooframe function may take arguments to affect the behavior
of the frame.


----------------------------------------------------------------------
|#

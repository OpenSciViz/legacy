# @(#)ltags.awk	1.4 04/22/93  Written By:  J. Flanagan, WaveStar Technology
# Q&D AWK Script to generate lisp tags on stdout suitable for use with vi.

BEGIN {	OFS="," }
tolower($1" "$2" "$3) ~ /^\([ ]*(defun|defmacro|define-modify-macro|define-setf-method|defclass|defgeneric|defmethod|defsetf|deftype|define-compiler-macro|define-condition|define-declaration|define-method-combination|defvar|defparameter|defvar|defconstant|defpackage|defstruct)[ ]+/ {
	k= 2 + ($1 == "(")
	tag = $k

	if (tag ~ /^.+\(/)	# strip trailing left-parens "name("
		tag = substr(tag,1,index(tag,"(")-1)
	if (tag ~ /^[^(].+\)/)	# strip trailing right-parens "name)"
		tag = substr(tag,1,index(tag,")")-1)

	if (tag == "(") {	# if name is "( setf", use next arg
		k++; tag = $k 
	}

	if (tolower(tag) ~ /^[(]?setf/) {
		k++
		if (index($k,"(")==0) {
			tag = "(setf "$k	# append setf method name
			if (index($k,")")==0)	# maybe add right-paren
				tag=tag")"
		}
	}
	else if (tag ~ /[()]/) {	# strip leading/trailing parens
		sub("^[(]","",tag)
		sub("[)]$","",tag)
		if (tag ~ /[()]/) 	# any left? if yes, ignore tag
			tag = ""
	}

	if (tag != "")
		printf("%s\t%s\t/^%s/\n", tag, FILENAME, $0)
}

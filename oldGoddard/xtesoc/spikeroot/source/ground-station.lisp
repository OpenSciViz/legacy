;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; ground-station.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: ground-station.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.2  1991/12/01  00:21:01  johnston
;;; no changes
;;;
;;; Revision 1.1  1991/11/26  15:39:55  johnston
;;; Initial revision
;;;
;;;
;;; routines for finding ground station pass times
;;;
;;; MDJ 11Nov91
;;; Note:  ground-station class and basic methods are in astro-objects.lisp




(defmethod VISIBLE-FOR-UPLINK ((LEOS low-earth-orbit-satellite)
                               (GS ground-station)
                               TJD)
  "return t if satellite is visible for uplink from groundstation,
    nil if not"
  (>= (cos-zenith-angle-of-object GS LEOS TJD)
      (slot-value GS 'cos-uplink-zenith-angle)))


(defmethod VISIBLE-FOR-DOWNLINK ((LEOS low-earth-orbit-satellite)
                               (GS ground-station)
                               TJD)
  "return t if satellite is visible for downlink from groundstation,
    nil if not"
  (>= (cos-zenith-angle-of-object GS LEOS TJD)
      (slot-value GS 'cos-downlink-zenith-angle)))


(defmethod TABULATE-GROUND-STATION-ENTRY-EXIT ((LEOS low-earth-orbit-satellite)
                                               (GS ground-station)
                                               &key 
                                               (steps 360) 
                                               (debug nil)
                                               (type :downlink))
  
  "Create a table on *standard-output* of ground station entry, 
    exit times (in days relative to ascending node), and earth longitude
    & latitude of entry/exit (in decimal degrees.  Table is keyed by longitude
    of ascending node.  Orbit model is successively modified by changing
    the time of asc node by the number of steps specified
    :type is :uplink or :downlink"
  
  ;; update orbit parameters to change time of asc node by delta
  ;; 4 min in time asc node => 1 deg longitude asc node
  (let ((saved-time-asc-node (time-of-asc-node LEOS))
        (stepsize (/ 1.0 steps)))
    (dotimes (i steps)
      (update-orbit-parameters 
       LEOS
       :inclination          (degrees (inclination LEOS))
       :time-asc-node        (- (time-of-asc-node LEOS) stepsize)
       :RA-asc-node          (degrees (RA-of-asc-node LEOS))
       :regression-rate      (degrees (nodal-regression-rate LEOS))
       :period               (* 1440.0 (nodal-period LEOS))
       :semimajor-axis       (semimajor-axis LEOS))
      (multiple-value-bind
        (long-asc-node entry-time entry-long-lat
                       exit-time exit-long-lat)
        (ground-station-entry-exit-wrt-asc-node 
         LEOS GS :debug debug :type type)
        (cond ((and entry-time exit-time)
               (format t "~%(~8,3f ~9,6f ~8,3f ~8,3f   ~9,6f ~8,3f ~8,3f)"
                       long-asc-node entry-time
                       (first entry-long-lat) (second entry-long-lat)
                       exit-time
                       (first exit-long-lat) (second exit-long-lat)))
              (t (format t "~%(~8,3f nil nil nil   nil nil nil)"
                         long-asc-node)))))
    ;; table finished, restore orbit model as it was:
    (update-orbit-parameters 
     LEOS
     :inclination          (degrees (inclination LEOS))
     :time-asc-node        saved-time-asc-node
     :RA-asc-node          (degrees (RA-of-asc-node LEOS))
     :regression-rate      (degrees (nodal-regression-rate LEOS))
     :period               (* 1440.0 (nodal-period LEOS))
     :semimajor-axis       (semimajor-axis LEOS))
    ;; return nil
    nil))


(defmethod GROUND-STATION-ENTRY-EXIT-WRT-ASC-NODE ((LEOS low-earth-orbit-satellite)
                                                   (GS ground-station)
                                                   &key (steps-per-orbit 100)
                                                   (tolerance 1.0e-5)
                                                   (type :downlink)
                                                   (debug nil))
  
  "Compute and return times of entry/exit of low-earth-orbit-satellite in/out
    of ground station.  The orbit is checked from the
    asc node for one nodal period, broken into steps-per-orbit samples.
    :type is :uplink or :downlink
    Five values are returned:
     long-asc-node - longitude of the orbit ascending node, degrees
     entry-time - days, from time of asc node
     entry-long-lat - list (longitude latitude) of satellite position above
      earth at time of entry.  In degrees.
     exit-time - days, from time of asc node
     exit-long-lat - list (longitude latitude) of satellite position above
      earth at time of exit.  In degrees.
    Note:  last four returned values will be NIL if there is no entry/exit in
    the orbit."
  
  (let* ((start-time (time-of-asc-node LEOS))
         (period (nodal-period LEOS))
         (step (/ period steps-per-orbit))
         (long-asc-node (first (earth-longitude-latitude-below 
                                LEOS start-time :units :degrees)))
         (in-prev-step (if (eq type :downlink)
                         (visible-for-downlink LEOS GS start-time)
                         (visible-for-uplink   LEOS GS start-time)))
         (prev-time start-time)
         (entry-time nil)
         (exit-time nil)
         )
    (dotimes (i steps-per-orbit)
      (let* ((time (+ start-time (* (1+ i) step)))
             (in-this-step (if (eq type :downlink)
                             (visible-for-downlink LEOS GS time)
                             (visible-for-uplink   LEOS GS time))))
        (cond ((and in-prev-step (not in-this-step))
               ;; exiting
               (setf exit-time
                     (bisect-for-ground-station-entry-exit 
                      LEOS GS :t1 prev-time :in1 in-prev-step
                      :t2 time :in2 in-this-step :tolerance tolerance
                      :type type))
               )
              ((and (not in-prev-step) in-this-step)
               ;; entering
               (setf entry-time
                     (bisect-for-ground-station-entry-exit 
                      LEOS GS :t1 prev-time :in1 in-prev-step
                      :t2 time :in2 in-this-step :tolerance tolerance
                      :type type))
               )
              (t nil))
        (setf in-prev-step in-this-step
              prev-time time)))
    (when (and debug entry-time exit-time)
      (format t "~%long asc node: ~,4fdeg,  entry: ~,4fmin,  exit ~,4fmin"
              long-asc-node
              (* (- entry-time start-time) 1440.0)
              (* (- exit-time start-time) 1440.0))
      (format t "~%  tolerance ~,3fmin=~,1fsec"
              (* tolerance 1440.0) (* tolerance 86400))
      (format t " check: entry ~a/~a, exit ~a/~a"
              (if (eq type :downlink)
                (visible-for-downlink LEOS GS (- entry-time tolerance))
                (visible-for-uplink   LEOS GS (- entry-time tolerance)))
              (if (eq type :downlink)
                (visible-for-downlink LEOS GS (+ entry-time tolerance))
                (visible-for-uplink   LEOS GS (+ entry-time tolerance)))
              (if (eq type :downlink)
                (visible-for-downlink LEOS GS (- exit-time tolerance))
                (visible-for-uplink   LEOS GS (- exit-time tolerance)))
              (if (eq type :downlink)
                (visible-for-downlink LEOS GS (+ exit-time tolerance))
                (visible-for-uplink   LEOS GS (+ exit-time tolerance)))
              )
      )                                 ; end when
    ;; returned values:
    (values long-asc-node 
            (if entry-time (- entry-time start-time)  nil)
            (if entry-time (earth-longitude-latitude-below 
                            LEOS entry-time :units :degrees) nil)
            (if exit-time  (- exit-time  start-time)  nil)
            (if exit-time (earth-longitude-latitude-below 
                           LEOS exit-time :units :degrees) nil))
    ))


(defmethod BISECT-FOR-GROUND-STATION-ENTRY-EXIT ((LEOS low-earth-orbit-satellite)
                                                 (GS ground-station)
                                                 &key t1 in1 t2 in2
                                                 (type :downlink)
                                                 (tolerance 1.0e-5))
  
  "Refine estimate of ground-station entry/exit times.  Input args
    are:
    LEOS - low-earth-orbit-satellite object instance
    GS- ground station
    t1 and t2 are TJD times bounding entry or exit,
    and in1 and in2 are flags indicating whether LEOS is in (t) or out (nil)
     of region at t1 and t2, respectively.
    type is :uplink or :downlink
    It must be known that satellite is in region at t1 and out at t2, or
    vice versa.  Further, t1 must be < t2, and in1 and in2 must be 
    either t/nil or nil/t.  Bisection method it used, to a time tolerance
    specified (in days).  Returned value is time of entry/exit, as TJD."
  
  (loop 
    (let* ((tmid (+ t1 (* 0.5 (- t2 t1))))
           (in-mid (if (eq type :downlink)
                     (visible-for-downlink LEOS GS tmid)
                     (visible-for-uplink   LEOS GS tmid))))
      (cond ((or (and in1 in-mid)
                 (and (not in1) (not in-mid)))
             (setf t1 tmid in1 in-mid))
            ((or (and in-mid in2)
                 (and (not in-mid) (not in2)))
             (setf t2 tmid in2 in-mid))
            (t (error "dropped through bisection")))
      (if (< (- t2 t1) tolerance)
        (return (+ t1 (* 0.5 (- t2 t1))))))))


;;; ----------------------------------------------------------------------
;;;                                        GROUND-STATION ENTRY-EXIT TABLE
;;; 

;;;
;;; --- Canberra
;;;

(defvar *CANBERRA*)
(setf *CANBERRA*
     (make-ground-station "Canberra" -35.47 148.98 0
                          :uplink-horizon-angle 5.0
                          :downlink-horizon-angle 2.0))
;; (show *canberra*)


#|
;;; the following table came from (with repeated nil entries removed):
(tabulate-ground-station-entry-exit *XTE* *Canberra* :steps 36
                                    :type :downlink)
|#

;;; NOTE: long asc node must be in INCREASING order, but wraparound 360->0 is OK.

(defvar *CANBERRA-DOWNLINK-ENTRY-EXIT-TABLE* nil)
;;; put this in a progn and return nil, to prevent having table echoed
;;; to screen while initializing
(progn
  (setf   
   *CANBERRA-DOWNLINK-ENTRY-EXIT-TABLE*
   ;; values are:
   ;;  asc node  entry    entry     entry      exit     exit       exit
   ;; long (deg) time(d) long(deg) lat(deg)   time(d)  long(deg)  lat(deg)
   '(
     ( 279.671  0.042754  129.768  -21.270    0.050734  173.672  -28.474)
     ( 289.699  0.041371  132.914  -18.642    0.048963  173.546  -28.256)
     ( 299.726  0.040176  137.189  -16.146    0.047077  172.904  -27.100)
     ( 309.754  0.039270  142.960  -14.135    0.044950  171.286  -24.735)
     ( 319.781  0.039044  151.942  -13.621    0.042199  167.085  -20.252)
     ( 329.808 nil nil nil   nil nil nil)
     ;; repeated nil entries removed
     ( 190.411 nil nil nil   nil nil nil)
     ( 200.438  0.056813  128.056  -23.025    0.061581  151.413  -13.573)
     ( 210.465  0.054491  125.667  -26.160    0.060905  158.292  -15.100)
     ( 220.493  0.052511  124.635  -27.839    0.059825  163.189  -17.429)
     ( 230.520  0.050692  124.280  -28.479    0.058521  166.831  -20.034)
     ( 240.548  0.048963  124.395  -28.256    0.057075  169.523  -22.600)
     ( 250.575  0.047292  124.954  -27.278    0.055529  171.408  -24.904)
     ( 260.602  0.045667  126.007  -25.651    0.053925  172.681  -26.739)
     ( 270.630  0.044096  127.638  -23.500    0.052270  173.404  -27.974)
     ))
  nil)


;;;
;;; --- Kagoshima
;;;

(defvar *KAGOSHIMA*)
(setf   *KAGOSHIMA*
        (make-ground-station "Kagoshima" +31.25 131.079 0
                             :uplink-horizon-angle 5.0
                             :downlink-horizon-angle 2.0))
;;; (show *kagoshima*)

#|
;;; the following table came from (with repeated nil entries removed):
(tabulate-ground-station-entry-exit *XTE* *Kagoshima* :steps 36
                                    :type :downlink)
(tabulate-ground-station-entry-exit *XTE* *Kagoshima* :steps 36
                                    :type :uplink)
|#

;;; NOTE: long asc node must be in INCREASING order, but wraparound 360->0 is OK.

(defvar *KAGOSHIMA-DOWNLINK-ENTRY-EXIT-TABLE* nil)
;;; put this in a progn and return nil, to prevent having table echoed
;;; to screen while initializing
(progn
  (setf   
   *KAGOSHIMA-DOWNLINK-ENTRY-EXIT-TABLE*
   ;; values are:
   ;;  asc node  entry    entry     entry      exit     exit       exit
   ;; long (deg) time(d) long(deg) lat(deg)   time(d)  long(deg)  lat(deg)
   '(
     ( 329.808 nil nil nil   nil nil nil)
     ( 339.836  0.025426  110.644   19.184    0.029869  131.419    9.244)
     ( 349.863  0.022885  107.747   23.636    0.029340  139.069   10.528)
     ( 359.890  0.020799  106.548   26.329    0.028308  144.394   12.968)
     (   9.918  0.018913  106.024   27.875    0.027009  148.361   15.896)
     (  19.945  0.017126  105.845   28.482    0.025541  151.320   18.956)
     (  29.973  0.015387  105.901   28.240    0.023959  153.423   21.895)
     (  40.000  0.013673  106.230   27.210    0.022308  154.834   24.475)
     (  50.027  0.011976  106.899   25.460    0.020616  155.673   26.518)
     (  60.055  0.010304  108.042   23.100    0.018908  156.131   27.878)
     (  70.082  0.008685  109.811   20.291    0.017184  156.313   28.476)
     (  80.109  0.007160  112.369   17.254    0.015423  156.247   28.254)
     (  90.137  0.005777  115.858   14.230    0.013589  155.898   27.140)
     ( 100.164  0.004608  120.513   11.517    0.011609  155.057   24.994)
     ( 110.192  0.003801  126.898    9.576    0.009329  153.166   21.465)
     ( 120.219  0.004074  138.150   10.237    0.005997  146.967   14.726)
     ( 130.246 nil nil nil   nil nil nil)
     ))
  nil)


(defvar *KAGOSHIMA-UPLINK-ENTRY-EXIT-TABLE* nil)
;;; put this in a progn and return nil, to prevent having table echoed
;;; to screen while initializing
(progn
  (setf   
   *KAGOSHIMA-UPLINK-ENTRY-EXIT-TABLE*
   ;; values are:
   ;;  asc node  entry    entry     entry      exit     exit       exit
   ;; long (deg) time(d) long(deg) lat(deg)   time(d)  long(deg)  lat(deg)
   '(
     ( 329.808 nil nil nil   nil nil nil)
     ( 339.836  0.026862  117.583   16.214    0.028429  124.893   12.688)
     ( 349.863  0.023629  111.619   22.453    0.028596  135.689   12.296)
     ( 359.890  0.021412  109.902   25.638    0.027695  141.556   14.372)
     (   9.918  0.019468  109.162   27.514    0.026453  145.715   17.088)
     (  19.945  0.017661  108.910   28.391    0.025007  148.680   19.994)
     (  29.973  0.015911  108.898   28.400    0.023435  150.726   22.772)
     (  40.000  0.014192  109.144   27.603    0.021790  152.054   25.170)
     (  50.027  0.012489  109.697   26.062    0.020102  152.824   27.004)
     (  60.055  0.010823  110.761   23.895    0.018389  153.182   28.142)
     (  70.082  0.009209  112.446   21.251    0.016655  153.275   28.498)
     (  80.109  0.007705  115.005   18.378    0.014878  153.141   28.015)
     (  90.137  0.006358  118.582   15.529    0.013008  152.663   26.607)
     ( 100.164  0.005279  123.581   13.090    0.010943  151.508   24.072)
     ( 110.192  0.004739  131.137   11.827    0.008392  148.460   19.734)
     ( 120.219 nil nil nil   nil nil nil)
     ))
  nil)

;;; ----------------------------------------------------------------------
;;;                                                       CALCULATE PASSES


(defmethod GROUND-STATION-PASSES ((LEOS low-earth-orbit-satellite) jd1 jd2
                                  &key 
                                  (table *Canberra-downlink-entry-exit-table*))
  
  "Return list of ground-station passes in the specified time range.  
    A table for interpolation must be specified.  Returned list is of
    the form:  ((jd1 jd2)...) where dates are pass start/end times."
  
  (let ((asc-nodes (asc-node-list LEOS jd1 jd2))
        (intervals nil)
        (period (nodal-period LEOS)))
    (dolist (node asc-nodes)
      (let (
            ;; long asc node, in DEGREES
            (long-asc-node
             (first (earth-longitude-latitude-below LEOS node :units :degrees)))
            )
        (multiple-value-bind 
          (entry-time exit-time entry-long-lat exit-long-lat)
          (interpolate-entry-exit-table long-asc-node
                                        :table table)
          (declare (ignore entry-long-lat exit-long-lat))
          
          (when (and entry-time exit-time)
            (push (if (> exit-time entry-time)
                    (list (+ node entry-time) (+ node exit-time))
                    (list (+ node entry-time (- period))
                          (+ node exit-time)))
                  intervals)
            )                           ; end when
          )                             ; end multiple-value-bind
        ))                              ; end dolist
    (nreverse intervals)
    ))

#|
test:
(ground-station-passes 
 *xte* (dmy-to-time '1-jan-1992) (dmy-to-time '2-jan-1992)
 :table *canberra-downlink-entry-exit-table*)
|#

;;; ======================================================================
;;;                                            CLASS:  GROUND-STATION-PASS
;;;
;;; record of an individual pass
;;;

(defclass GROUND-STATION-PASS ()
  ((station-abbrev :initarg :station-abbrev
                   :documentation "abbrev for station name")
   (pass-start :initarg :pass-start :reader pass-start
               :documentation "start of pass, TJD")
   (pass-end :initarg :pass-end :reader pass-end
             :documentation "end of pass, TJD")
   (type :initarg :type 
         :documentation "keyword:  :uplink :downlink")
   (available :initarg :available :initform t
              :documentation
              "t or nil")
   (comment :initarg :comment :initarg nil
            :documentation
            "nil or string comment")
   )
  (:documentation "ground station pass availability record"))


(defmethod PRINT-OBJECT ((p ground-station-pass) stream)
  (format stream "#<~a ~a ~a ~,1f min>"
          (slot-value p 'station-abbrev)
          (slot-value p 'type)
          (format-abs-time (slot-value p 'pass-start))
          (* 1440 (- (slot-value p 'pass-end) (slot-value p 'pass-start)))))



(defun MAKE-GROUND-STATION-PASS (&key station-abbrev
                                      type pass-start pass-end
                                      (available t) comment)
  "constructor function for ground-station-pass"
  (make-instance 'ground-station-pass
                 :station-abbrev station-abbrev
                 :pass-start pass-start
                 :pass-end pass-end
                 :type type
                 :available available
                 :comment comment))


;;; ======================================================================
;;;                                       CLASS:  GROUND-STATION-PASS-LIST
;;;
;;; A collection of passes between specified start and end dates.  Here
;;; are the things you can do to pass lists:
;;;
;;; 1.  Create a new pass list over some specified time interval and save it
;;; to a file.  It becomes the current pass-list.
;;;
;;; 2.  Edit the file and reload it (or just reload it in a different session).
;;; The loaded file becomes the current pass list.
;;;

(defclass GROUND-STATION-PASS-LIST ()
  ((start-time :initarg :start 
               :documentation "start time TJD")
   (end-time :initarg :end
             :documentation "end time TJD")
   (pass-list :initarg :pass-list
              :documentation "List of ground-station-pass instances")
   (file :initarg :file
         :documentation "name of file to read/write pass records from/to")
   )
  (:documentation "list of pass records and their status"))


;;; --- the current pass list

(defvar *CURRENT-GROUND-STATION-PASS-LIST* nil)


;;; could adopt naming convention for files, based on dates

(defun MAKE-GROUND-STATION-PASS-LIST (&key start end file
                                           pass-generator)
  
  "Create a new ground-station-pass-list instance and make it current
    (by assigning it to the special var *CURRENT-GROUND-STATION-PASS-LIST*).
    Calculate pass times.  Store in specified file.
    The keywords :start and :end specify the time range in TJD.
    pass-generator is a function of two arguments, start and end time,
    which calculates and returns a list of GROUND-STATION-PASS instances.
    There is where customatization for each mission can be put.
    File should be root name only:  extension .pass is added, and file is
    put in obs logical directory."
  
  (let* ((pass-list 
          (sort (funcall pass-generator start end)
                '< :key 'pass-start))
         (logical-file (format nil "spike:obs;~a.pass" file))
         (instance (make-instance 'GROUND-STATION-PASS-LIST
                                  :start start 
                                  :end end
                                  :pass-list pass-list
                                  :file logical-file)))
    (setf *CURRENT-GROUND-STATION-PASS-LIST* instance)
    instance))


(defun XTE-PASS-GENERATOR (start end)

  "Mission specific ground station pass generator function for ASTRO-D.
    Input is start/end time for passes.  Output is list of ground-station-pass
    instances for available passes between times."

  ;; for now, record all passes and mark those that don't meet criteria

  (let ((all-passes nil))

    ;; Kagoshima
    (let ((station-abbrev "KAG"))
      ;; uplinks
      (dolist (pass-times (ground-station-passes 
                           *xte* start end
                           :table *KAGOSHIMA-UPLINK-ENTRY-EXIT-TABLE*))
        (push (make-ground-station-pass 
               :station-abbrev station-abbrev
               :type :uplink :pass-start (first pass-times) 
               :pass-end (second pass-times)
               :available t)
              all-passes))
      ;; downlinks
      (dolist (pass-times (ground-station-passes 
                           *xte* start end
                           :table *KAGOSHIMA-DOWNLINK-ENTRY-EXIT-TABLE*))
        (push (make-ground-station-pass 
               :station-abbrev station-abbrev
               :type :downlink :pass-start (first pass-times) 
               :pass-end (second pass-times)
               :available t)
              all-passes))
      )

    ;; Canberra
    (let ((station-abbrev "CAN"))
      ;; uplinks
      (dolist (pass-times (ground-station-passes 
                           *xte* start end
                           :table *CANBERRA-DOWNLINK-ENTRY-EXIT-TABLE*))
        (push (make-ground-station-pass 
               :station-abbrev station-abbrev
               :type :downlink :pass-start (first pass-times) 
               :pass-end (second pass-times)
               :available t)
              all-passes)))

    ;; returned value
    (xte-filter-passes start end all-passes)))

                   
(defun XTE-FILTER-PASSES (start end pass-list)
  
  "Input a list of ground-station-pass instances, return list of pass instances
    but with available and comment fields set to indicate excluded passes."
  
  (dolist (pass pass-list)
    
    (with-slots (station-abbrev pass-start pass-end available
                         comment) pass
      
      ;; exclude passes with start or end times out of specified range
      (when (or (< pass-start start) (> pass-end end))
        (setf available nil
              ;; preserve other comments
              comment (concatenate 'string 
                                   (if comment comment "")
                                   (if comment "; " "") 
                                   "time not in range")))
      
      ;; exclude Kagoshima passes on Sunday
      (when (and (equalp station-abbrev "KAG")
                 (or (= (day-of-week pass-start) 6) 
                     (= (day-of-week pass-end) 6)))
        (setf available nil
              ;; preserve other comments
              comment (concatenate 'string 
                                   (if comment comment "")
                                   (if comment "; " "") 
                                   "Sunday")))
      
      ))                                ; end loop over passes
  
  pass-list)
      

(defmethod REPORT-PASS-LIST ((PL GROUND-STATION-PASS-LIST)
                             &key (stream t))
  "report pass list to a stream, default *standard-output*"
  (with-slots (start-time end-time pass-list file) PL
    (format stream "~%Ground station passes for the period:")
    (format stream "~%  ~a to ~a" (format-abs-time start-time)
            (format-abs-time end-time))
    (format stream "~%  associated with file: ~a" file)
    
    (dolist (pass pass-list)
      (with-slots (pass-start pass-end station-abbrev type available comment) pass
        (let* ((pass-start-ut (jd-to-ymdhms pass-start))
               (year (first pass-start-ut))
               (month (second pass-start-ut))
               (day (third pass-start-ut))
               (hour (fourth pass-start-ut))
               (minute (fifth pass-start-ut))
               (second (sixth pass-start-ut))
               (pass-dur (- pass-end pass-start))
               (pass-dur-hms (fractional-day-to-hour-minute-second pass-dur))
               (dur-min (second pass-dur-hms))
               (dur-sec (third pass-dur-hms)))
          (format 
           stream
           "~%~3a ~2a ~1a ~4,'0d/~2,'0d/~2,'0d ~2,'0d:~2,'0d:~2,'0d  ~2,'0d:~2,'0d ~s"
           station-abbrev 
           (cond ((eq type :uplink) "UP") ((eq type :downlink) "DN")
                 (t "??"))
           (if available "Y" "N")
           year month day hour minute second
           dur-min dur-sec
           (if comment comment "")))))
    
    ))


;;; need to clean up format here

(defmethod SAVE-PASS-LIST ((PL GROUND-STATION-PASS-LIST))
  "save to file in readable form."
  
  (with-slots (start-time end-time pass-list file) PL
    
    (with-open-file (stream (translate-logical-pathname file)
                            :direction :output
                            :if-exists :supersede)
      
      (format stream "~%;; Ground station passes for the period:")
      (format stream "~%~a~%~a" (jd-to-ymdhms start-time)
              (jd-to-ymdhms end-time))
      (format stream "~%;; associated with file: ~a" file)
      (format stream "~%;; created:  ~a" (get-formatted-universal-time))
      
      (dolist (pass pass-list)
        (with-slots (pass-start pass-end station-abbrev type available comment) pass
          (let* ((pass-start-ut (jd-to-ymdhms pass-start))
                 (pass-dur (- pass-end pass-start))
                 (pass-dur-hms (fractional-day-to-hour-minute-second pass-dur))
                 )
            (format stream "~%~3a ~2a ~1a ~a ~a ~s"
                    station-abbrev 
                    (cond ((eq type :uplink) "UP") ((eq type :downlink) "DN")
                          (t "??"))
                    (if available "Y" "N")
                    pass-start-ut
                    pass-dur-hms
                    (if comment comment "")))))
      (format stream "~%")
      
      )))


(defun LOAD-PASS-LIST (&key file)
  "input root name, load pass list"
  (with-open-file (stream (translate-logical-pathname
                           (format nil "spike:obs;~a.pass" file))
                          :direction :input)
    (let ((start (apply 'ymdhms-to-jd (read stream)))
          (end (apply 'ymdhms-to-jd (read stream)))
          (passes nil))
      (loop
        (let ((station-abbrev (read stream nil nil))
              (type (read stream nil nil))
              (available (read stream nil nil))
              (pass-start (read stream nil nil))
              (pass-dur (read stream nil nil))
              (comment (read stream nil nil)))
          (if (null station-abbrev) (return nil))
          ;; make pass record
          (push (make-ground-station-pass
                 :station-abbrev (symbol-name station-abbrev)
                 :type (if (eq type 'UP) :uplink :downlink)
                 :available (if (eq available 'Y) t nil)
                 :pass-start (apply 'ymdhms-to-jd pass-start)
                 :pass-end (+ (apply 'ymdhms-to-jd pass-start)
                              (apply 'hour-minute-second-to-fractional-day
                                     pass-dur))
                 :comment comment)
                passes)))               ; end loop
      
      (setf *CURRENT-GROUND-STATION-PASS-LIST*
            (make-instance 'GROUND-STATION-PASS-LIST
                           :start start 
                           :end end
                           :pass-list (sort passes '< :key 'pass-start)
                           :file (format nil "spike:obs;~a.pass" file)))
      )))

#| test

(make-ground-station-pass-list :start 4012.0 :end 4019.0
     :file "test" :pass-generator 'xte-pass-generator)
(report-pass-list *CURRENT-GROUND-STATION-PASS-LIST*)
(save-pass-list *CURRENT-GROUND-STATION-PASS-LIST*)
(load-pass-list :file "test")

(describe *CURRENT-GROUND-STATION-PASS-LIST*)
(translate-logical-pathname "spike:obs;test.pass")
|#


;;; ======================================================================
;;; ======================================================================
;;; PLOTTING

#|
;;; plot points in table:  entry/exit long-lat
(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.05 0.95 0.1 0.80)
(set-x-window-scale g -180 180)
(set-y-window-scale g -90 90)
(box-window g)
(box-rect g)
(label-y-axis g -90 90 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(label-x-axis g -180 180 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
;; plot points in table
(dolist (rec *kagoshima-upLINK-ENTRY-EXIT-TABLE*)
  (let ((entry-time (second rec))
        (exit-time (fifth rec))
        (entry-long (third rec))
        (entry-lat (fourth rec))
        (exit-long (sixth rec))
        (exit-lat (seventh rec)))
    (when (and entry-time exit-time)
      (plot-symbol-at-point g (if (< entry-long 180) entry-long (- entry-long 360))
                            entry-lat
                            :symbol *cross-symbol* :symbol-scale 2)
      (plot-symbol-at-point g (if (< exit-long 180) exit-long (- exit-long 360))
                            exit-lat
                            :symbol *plus-symbol* :symbol-scale 2)
      )))
(record-wrapup g)


;;; plot passage times
;;; plot entry/exit time vs long asc node
(clear-gwindow-history g)
(set-page-loc-of-drawing-rect g 10 400 200 10)
(set-loc-of-drawing-window g 0.07 0.95 0.1 0.80)
(set-x-window-scale g 0 360)
(set-y-window-scale g 0 0.07)
;; plot just the entries in the table
(dolist (rec *kagoshima-upLINK-ENTRY-EXIT-TABLE*)
  (let ((long (first rec))
        (entry-time (second rec))
        (exit-time (fifth rec)))
    (when (and entry-time exit-time)
      (plot-symbol-at-point g long entry-time
                            :symbol *cross-symbol* :symbol-scale 2)
      (plot-symbol-at-point g long exit-time
                            :symbol *plus-symbol* :symbol-scale 2))
    ))
;; interpolate and plot
(dotimes (i 360)
  (multiple-value-bind 
    (entry-time exit-time)
    (interpolate-entry-exit-table i :table *kagoshima-upLINK-ENTRY-EXIT-TABLE*)
    (when (and entry-time exit-time)
      (plot-symbol-at-point g i entry-time
                            :symbol *cross-symbol* :symbol-scale 2)
      (plot-symbol-at-point g i exit-time
                            :symbol *plus-symbol* :symbol-scale 2))
    ))
    
(box-window g)
(box-rect g)
(label-y-axis g 0 .07 :tic-value 0.01 :tic-size 3 :font-size 9
              :label-format "~,2f")
(label-x-axis g 0 360 :tic-value 30 :tic-size 3 :font-size 9
              :label-format "~,0f")
(record-wrapup g)

|#

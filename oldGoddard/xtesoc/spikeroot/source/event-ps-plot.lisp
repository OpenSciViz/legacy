;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; event-ps-plot.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: event-ps-plot.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.14  1991/12/01  00:18:40  johnston
;;; fixed
;;; new postscript routines in this version
;;;
;;; Revision 1.13  1991/11/26  15:38:44  johnston
;;; minor changes, this version is still broken though
;;;
;;; Revision 1.12  1991/08/21  11:30:10  giuliano
;;; Changed interface functions for the event calculator.
;;;
;;; Revision 1.11  91/07/18  09:58:09  miller
;;; fixed some minor broken things, added some error checking,
;;; added non-interactive mode, removed obsolete code
;;; 
;;; 
;;; Revision 1.10  91/06/05  22:54:37  johnston
;;; no changes.
;;; 
;;; Revision 1.9  91/04/28  18:00:50  johnston
;;; changed to make compatible with changes to fast-event
;;; 
;;; Revision 1.8  91/04/15  12:56:22  johnston
;;; added return at end of file
;;; 
;;; Revision 1.7  91/04/05  07:57:17  johnston
;;; minor changes
;;; 
;;; Revision 1.6  91/03/25  13:22:52  johnston
;;; major rework
;;; 
;;; Revision 1.5  91/03/19  08:56:09  johnston
;;; first stage of rework for new event calculation objects
;;; 
;;; Revision 1.4  91/02/11  09:20:53  johnston
;;; added capability to specify sample interval for postscript plots
;;; for sun, moon, orbital events
;;; 
;;; Revision 1.3  90/12/12  10:19:50  krueger
;;; Updated LABEL-PS-PLOT.  The slot "lambda" on fixed-astronomical-object
;;; was renamed to "lambda-value".  PCL on the SUN was confusing the 
;;; generic function "lambda" with the Lisp "lambda" function.
;;; 
;;; Revision 1.2  90/12/07  07:25:11  krueger
;;; Converted Flavor code to CLOS.  Many changes
;;; 
;;; Revision 1.1  90/09/24  14:33:13  johnston
;;; Initial revision
;;; 
;;;
;;;

;;; ======================================================================
;;; FUNCTIONS TO PLOT TARGETS FROM A PROPOSAL, FOR USE IN CHI MENUS
;;; ======================================================================
;;;
;;; User callable functions are
;;;    plot-target-from-proposal (interactive)
;;;    plot-target-events        (non-interactive)
;;;
;;; **********************************************************************
;;; uses plot-hst-target-constraints function instead of the obsolete
;;;  target-plot-ps function
;;;
;;; still relies on Trans to load an idb file to populate cbase. Since
;;; Trans itself is not run, this is very fast. As part of final stif
;;; work should have this read stifs
;;;
;;; Also added a non-interactive interface to ease testing
;;; **********************************************************************


(defvar *multi-proposal-target-table* (make-hash-table :test #'equal)
  "key is target name + proposal ID as a string, value is a list (ra dec)")

(defvar *multi-proposal-target-list* nil
  "list of lists (targetname targetid) where name is displayed in the
    choice menu and id is the hash table key for the target ra and dec")

#|
to clear tables for testing:
 (progn (clrhash *multi-proposal-target-table*)
	(setf *multi-proposal-target-list* nil))
|#


#| Example:
(manually-select-proposals '(210p))
(proposals *default-planning-session-parameters*)
(load-multi-proposal-target-table)
(plot-target-from-proposal)
|#


(defun LOAD-MULTI-PROPOSAL-TARGET-TABLE ()
  "load the list of current proposals and extract the targets to put into
    the *multi-proposal-target-table*.  A sorted list of target names & ids
    is saved in *multi-proposal-target-list*"
  (let ((proposal-list (proposals *default-planning-session-parameters*)))
    (cond

      ;; no proposals:  note and exit
      ((null proposal-list)
	   (force-confirmation "No proposals specified")
	   (return-from load-multi-proposal-target-table))

      ;; otherwise load the proposals and save the targets
      (t ;; clear current table and proposal list
       (clrhash *multi-proposal-target-table*)
       (setf *multi-proposal-target-list* nil)
       ;; loop over proposals
       (dolist (proposal-version proposal-list)
	 (let ((proposal-id (first proposal-version))
	       (version (second proposal-version)))
	   ;; load proposal
;;;
;;; THIS IS WHERE TRANS IS INVOKED, REPLACE WITH STIF
;;;
	   (load-proposal proposal-id version)
	   ;; get targets and positions and put in table
	   (for-each :idb-target :f :do
		     (let* ((name (format nil "~a~a-~a" proposal-id version :f.targname))
			    (id (intern name)))
		       (setf (gethash id *multi-proposal-target-table*)
			     (list :f.computed-ra :f.computed-dec))
		       (push (list name id) *multi-proposal-target-list*)))))
       ;; sort the target list
       (setf *multi-proposal-target-list*
	     (sort *multi-proposal-target-list* #'string< :key #'first))))))


(defun SET-ALL-HST-EVENT-CALC-PARAMETERS 
       (&key (start (segmentation-start *default-planning-session-parameters*))
             (end (segmentation-end *default-planning-session-parameters*)))

  "function to call to set up all event calc parms
    when you don't want to call them individually. Uses current orbit model"
  
  ;; WHERE IS THE OFFICIAL PLACE TO GET THESE MAGIC NUMBERS??
  
  (set-HST-event-calc-dates :start start :end end :default-sample-interval 2)

  (warn-if-dates-outside-orbit-model start end)

  (set-HST-event-calc-sun-constraint 
    :angle-table '((0 50 0)(50 180 1)) :sample-interval 1)
  (set-HST-event-calc-moon-constraint 
    :angle-table '((0 15 0)(15 180 1)) :sample-interval 1/3)
  (set-HST-event-calc-zodiacal-light :wavelength 5000 :brightness 1e-10)
  (set-HST-event-calc-orbital-viewing
   :orbit-model (current-orbit-model)
   :bright-limb-angle 20.9 :dark-limb-angle 12 :limb-angles-in-degrees t
   :terminator-angle 104.0 :terminator-angle-in-degrees t
   :dark t :dark-sun-limb-angle -0.25 :dark-sun-limb-angle-in-degrees t
   :northern-hemisphere-only t
   :viewing-time nil
   :interruptible nil
   :min-viewing-time nil
   :view-suitability-table nil
   :high-accuracy nil
   :sample-interval 1)

  (set-HST-event-calc-sensor-limits 
    :sensor-name :FHST1
    :sun 40 :moon 17.5 :bright-limb 37 :dark-limb 21
    :angles-in-degrees t)
  (set-HST-event-calc-sensor-limits 
    :sensor-name :FHST2
    :sun 40 :moon 17.5 :bright-limb 37 :dark-limb 21
    :angles-in-degrees t)
  (set-HST-event-calc-sensor-limits 
    :sensor-name :FHST3
    :sun 40 :moon 17.5 :bright-limb 37 :dark-limb 21
    :angles-in-degrees t)

  )


(defun WARN-IF-DATES-OUTSIDE-ORBIT-MODEL (start end)
  "notifies user if the two TJD dates are not totally
   within orbit model dates"

  (let ((problem (dates-outside-orbit-model start end)))
    (when problem 
      (format t "~2%NOTICE::: start/end dates ~a ~a" start end)
      (format t "~%  not within orbit model ~a ~a"
	      *orbit-model-start-date* *orbit-model-end-date*)
      (format t "~%~a" problem))))


(defun PLOT-TARGET-FROM-PROPOSAL ()

  "Interactively let user select target from current list to plot.
    The time boundaries are those of the current segmentation.
    The orbit model is the current orbit model.
    Use chi to set these appropriately."

  ;; load proposals if needed
  (when (null *multi-proposal-target-list*)
    (format t "Loading proposals to get targets")
    (load-multi-proposal-target-table)
    (when (null *multi-proposal-target-list*)
      (force-confirmation "No targets loaded... cannot plot events")
      (return-from plot-target-from-proposal)))

  ;; get parameters
  (let* ((orbit-model (current-orbit-model))
	 (start (segmentation-start *default-planning-session-parameters*))
	 (end (segmentation-end *default-planning-session-parameters*))
	 (filename nil)
	 (target nil))

    ;; error check and exit
    (when (or (null orbit-model)
	      (null start)
	      (null end))
      (force-confirmation
       "Orbit model, segmentation start and end must be specified")
      (return-from plot-target-from-proposal))
    
    ;; select target
    (setf target
      (pop-up-menu-choose (cons '(abort nil)
				*multi-proposal-target-list*)
			  :title "Choose a target"))

    ;; get filename
    (setf filename (generate-report-get-filename (format nil "~a.ps" target)))

    ;; error check
    (when (null filename)
      (force-confirmation "No filename specified")
      (return-from plot-target-from-proposal))

    (unless (null target)
      ;; plot it
      (let ((ra-dec (gethash target *multi-proposal-target-table* )))
	(set-all-HST-event-calc-parameters :start start :end end) 
	(plot-hst-target-constraints :name target :ra (first ra-dec) :dec (second ra-dec)
				     :file filename)
	
	))))

;;; non-interactive function

(defun PLOT-TARGET-EVENTS 
       (targname ra dec filename 
                 &key 
                 (start (segmentation-start *default-planning-session-parameters*))
                 (end (segmentation-end *default-planning-session-parameters*))
                 (print-it nil) 
                 (delete nil))
  
  "generate postscript file to print a target's events. RA and Dec in DEGREES.
    output is sent to filename. if print-it is non-nil, file is sent to printer.
    If delete is non-nil, file is deleted when routine is done.
    Events are plotted from start to end, using current orbit model.
    Non-interactive analog to plot-target-from-proposal"
  
  (unless (and ra dec filename start end)
    (error "Not all required parameters specified"))
  (unless targname (setf targname "anonymous"))
  (set-all-HST-event-calc-parameters :start start :end end)
  (plot-hst-target-constraints :name targname :ra ra :dec dec :file filename)
  (if print-it (print-file filename))
  ;note user can delete without printing, heh heh
  (if delete (delete-file filename)))

#|
(initialize-event-calculator "test")
(plot-target-events "Aries" 0.0 0.0 "c:/cerb/u1/miller/aries.ps")
(plot-target-events "North Ecliptic Pole" 270 67.0 "c:/cerb/u1/miller/nep.ps")
(plot-target-events  "MK 1152"  18.457599  -14.845503 "c:/cerb/u1/miller/mk-1152.ps" :print-it t)
;; NGC188 is at 00H 46M 00S +85D 14' 30"
(plot-target-events  "NGC188"  (hms-to-ra 0 46 0)  (dms-to-dec '+ 85 14 30)  "/cerb/u1/miller/temp1.ps")
(plot-target-events  "NGC 5617"  216.37  -60.44  "/cerb/u1/miller/temp2.ps")
|#

;;; ======================================================================
;;; POSTSCRIPT PLOTS FROM TARGET COORDINATES, ETC.
;;; ======================================================================

#|
(initialize-event-calculator "test")
(set-all-HST-event-calc-parameters :start (dmy-to-time '1-jan-1992)
                                   :end (dmy-to-time '5-jan-1992))
(plot-hst-target-constraints :name "MK 1152" :ra 18.457599 :dec -14.845503
			    :file "/cerb/u1/miller/mk-1152ps")
;; NGC188 is at 00H 46M 00S +85D 14' 30"
(plot-hst-target-constraints :name "NGC188" :ra (hms-to-ra 0 46 0) :dec (dms-to-dec '+ 85 14 30)
			    :file "/cerb/u1/miller/ngc188.ps")
(plot-hst-target-constraints :name "NGC 5617" :ra 216.37 :dec -60.44
			    :file "/cerb/u1/miller/ngc5617.ps")

|#

;;; for debugging: keep the most recently computed sample lists available
(defvar *eps-orb-samples* nil)
(defvar *eps-sun-samples* nil)
(defvar *eps-moon-samples* nil)

;;; NOTE:  disabled calculation of star tracker visibility:  must
;;; add back sensor-name-list below to restore this
;;; MDJ 27Nov91

(defun PLOT-HST-TARGET-CONSTRAINTS (&key name ra dec file)
  "Plot constraints, ra and dec in degrees.  All parameters must
    already be set up."
  (set-HST-event-calc-pointing :name name :ra ra :dec dec :units :degrees
                               :observer-name "HST" :observer *ST*)
  (set-HST-event-calc-roll :nominal)
  (let ((orb-samples
         (orb-viewing-sample-list 
          (current-HST-event-calc)
          ;; :sensor-name-list '(:fhst1 :fhst2 :fhst3)
          :named-vector-list nil)
	 )
        (sun-samples (sun-sample-list (current-HST-event-calc)))
        (moon-samples (moon-sample-list (current-HST-event-calc))))
    (setf *eps-orb-samples* orb-samples
          *eps-sun-samples* sun-samples
          *eps-moon-samples* moon-samples)
    (generate-hst-constraint-plot
     file
     :orb-samples orb-samples
     :sun-samples sun-samples
     :moon-samples moon-samples)
    ))



(defun GENERATE-HST-CONSTRAINT-PLOT
    (file &key orb-samples sun-samples moon-samples)
  
  "Given file name, and sun, moon, and orbital viewing sample lists,
    plot constraints to postscript file.  Use (current-HST-event-calc)
    for constraint definitions."
  
  (let* ((gp (make-ps-gwindow :file file))
         (HEC (current-HST-event-calc))
	 ;; page loc of rect left, right: same throughout
         (pleft 60) (pright 550)
	 ;; window loc left right: same throughout
         (wleft 0.1) (wright 0.98)
	 ;; current page top bottom
         (initial-page-top 700)
         (ptop nil) 
         (pbottom initial-page-top) ;; this is the initial value of page top
	 (pbottom-limit 50) ;; minimum y on page
         )
    
    ;; setup
    (record-prolog gp)
    
    ;; PLOT HEADER
    (plot-target-constraints-header
     HEC :where gp
     :page-left pleft :page-right pright :page-bottom initial-page-top :page-top (+ initial-page-top 50))
    
    
    ;; SUN
    ;; former size: 80
    (when sun-samples
      (setf ptop pbottom pbottom (- pbottom 100)) ;number is height of rect
      (sun-angle-and-pcf-plot 
       HEC :where gp :sample-list sun-samples
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright))
    
    
    ;; MOON
    ;; former size: 80
    (when moon-samples 
      (setf ptop pbottom pbottom (- pbottom 100)) ;number is height of rect
      (moon-angle-and-pcf-plot 
       HEC :where gp :sample-list moon-samples
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright))

    (when orb-samples

      ;; VISIBILITY AND TERMINATOR
      (setf ptop pbottom pbottom (- pbottom 150)) ;number is height of rect
      (bright-dark-bright-time-plot 
       HEC :where gp :sample-list orb-samples
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright)
    
      ;; TOTAL TARGET VISIBILITY (total and dark)
      (setf ptop pbottom pbottom (- pbottom 60)) ;number is height of rect
      (visibility-time-plot
       HEC :where gp :sample-list orb-samples
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright) 
    
      ;; NORTH VISIBILITY (north and north dark)
      (setf ptop pbottom pbottom (- pbottom 60)) ;number is height of rect
      (north-visibility-time-plot
       HEC :where gp :sample-list orb-samples
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright)
    
      ;; VIS PCF
      ;; was 20
      (setf ptop pbottom pbottom (- pbottom 40)) ;number is height of rect
      (visibility-pcf-plot
       HEC :where gp :sample-list orb-samples :dark nil
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright)
    
      ;; DARK VIS PCF
      ;; was 20
      (setf ptop pbottom pbottom (- pbottom 40)) ;number is height of rect
      (visibility-pcf-plot
       HEC :where gp :sample-list orb-samples :dark t
       :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
       :window-left wleft :window-right wright)
    
      )					;end when orb-samples
    
    ;; DRAW TIME AXIS OVER WHOLE PAGE
    ;; assumes time scale correctly set from above
    (set-page-loc-of-drawing-rect gp pleft pright (+ pbottom-limit 30) initial-page-top)
    (set-loc-of-drawing-window gp wleft wright 0 1)
    ;; force labelling of week starts
    (time-label gp (start-date HEC) (end-date HEC) :label t
		:font-size 4 :min-pixels-for-tics 5 :min-pixels-for-labels 10
		:tics-at '((:day 7)(:day 14)))
    (set-page-loc-of-drawing-rect gp pleft pright (+ pbottom-limit 10) initial-page-top)
    (set-loc-of-drawing-window gp wleft wright 0 1)
    (time-label gp (start-date HEC) (end-date HEC) :label t
		:min-pixels-for-labels 20)
    
    (record-wrapup gp))
  )

;;; this was taken out of the above function:  if returned, put it after
;;; dark vis pcf plot before the end when orb-samples
#|======================================================================
  === REMOVED  MDJ 27Nov91 =============================================    
    ;;; For this to work, must have new version of orbit models.
    ;;; don't put in a test since we're supposed to convert to them...
    
    ;; TRACKER SUN/MOON ANGLES AND PCF
    (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
    (tracker-sun-moon-plot HEC :where gp :sample-list orb-samples :sensor :fhst1
                           :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                           :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
    (tracker-sun-moon-plot HEC :where gp :sample-list orb-samples :sensor :fhst2
                           :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                           :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 50)) ;number is height of rect
    (tracker-sun-moon-plot HEC :where gp :sample-list orb-samples :sensor :fhst3
                           :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                           :window-left wleft :window-right wright)
    
    ;; HST TRACKER TIME AVAIL AT END OF OCCULTATION
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (tracker-occ-avail-plot HEC :where gp :sample-list orb-samples :sensor :fhst1
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (tracker-occ-avail-plot HEC :where gp :sample-list orb-samples :sensor :fhst2
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (tracker-occ-avail-plot HEC :where gp :sample-list orb-samples :sensor :fhst3
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
    
    
    ;; HST TWO-TRACKER AVAIL AT END OF OCCULTATION
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (two-tracker-avail-plot HEC :where gp :sample-list orb-samples :sensor1 :fhst1 :sensor2 :fhst2
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (two-tracker-avail-plot HEC :where gp :sample-list orb-samples :sensor1 :fhst1 :sensor2 :fhst3
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
    (setf ptop pbottom pbottom (- pbottom 20)) ;number is height of rect
    (two-tracker-avail-plot HEC :where gp :sample-list orb-samples :sensor1 :fhst2 :sensor2 :fhst3
                            :page-left pleft :page-right pright :page-bottom pbottom :page-top ptop
                            :window-left wleft :window-right wright)
  ======================================================================
  ======================================================================|#



(defun PLOT-TARGET-CONSTRAINTS-HEADER 
       (HEC
        &key
        where page-left page-right page-bottom page-top)
  
  "Plot header with target and orbit info"
  
  ;; page-left page-right page-bottom page-top
  (set-page-loc-of-drawing-rect where page-left page-right page-bottom page-top)
  ;; window-left window-right window-bottom window-top
  (set-loc-of-drawing-window where  0 1 0 1)
  (record-font where "Helvetica-Bold" 9)
  (string-at-line-in-rect 
   where  :line-number 1 :font-size 9
   :string (format nil "Spike STScI ~a" (get-formatted-universal-time)))
  
  (string-at-line-in-rect 
   where :line-number 3 :font-size 9
   :string (format nil "Target: ~a" (string-upcase (name HEC))))
  
  (string-at-line-in-rect
   where :line-number 4 :font-size 9
   :string 
   (let ((astro-object (astro-object HEC)))
     (format nil "   RA: ~,3f (~a) Dec: ~,3f (~a)  Ecl. long: ~,3f lat: ~,3f" 
             (degrees (ra astro-object))
             (let ((hms (degrees-of-ra-to-hms (degrees (ra astro-object)))))
               (format nil "~dh ~2,'0dm ~,1fs" (first hms) (second hms) (third hms)))
             (degrees (dec astro-object))
             (let ((dms (degrees-of-dec-to-dms (degrees (dec astro-object)))))
               (format nil "~dd ~2,'0dm ~ds" (first dms) (second dms) (round (third dms))))
             (degrees (lambda-value astro-object))
             (degrees (beta astro-object)))))
  
  (record-font where "Helvetica" 6)
  (string-at-line-in-rect
   where :line-number 5 :font-size 9
   :string
   (with-output-to-string
     (stream)
     (with-slots 
       (observer observer-name) HEC
       (format stream "Orbit model: ~a" (orbit-model HEC))
       (format stream " ~a" observer-name)
       (format stream " incl: ~,2fd;" (degrees (inclination observer)))
       (format stream " time asc. node: ~a;"  (format-abs-time (time-of-asc-node observer)))
       (format stream " RA asc. node: ~,2fd;"  (degrees (ra-of-asc-node observer)))
       (format stream " regr. rate: ~,2f deg/day;" (degrees (nodal-regression-rate observer)))
       (format stream " nodal period: ~,2f min;" (* 1440 (nodal-period observer)))
       (format stream " semimaj. axis:  ~,1f km"  (semimajor-axis observer))
       )))
  )

;;; ----------------------------------------------------------------------
;;;                                             GENERAL CURVE/PCF PLOTTING
;;; ----------------------------------------------------------------------


(defun EVENT-PLOT (HEC 
                   &key
                   where
                   page-left page-right page-bottom page-top
                   window-left window-right window-bottom window-top
                   curve pcf
                   rect-title (rect-font "Helvetica") (rect-font-size 9)
                   window-title (window-font "Helvetica")(window-font-size 6)
                   (width 1) (dash1 nil) (dash2 nil)
                   y-scale-min		; required
                   y-scale-max		; required
                   (time-axis nil)
                   (time-label nil)
                   (box-rect nil)
                   (box-window nil))

  "This is a generic curve or PCF postscript plotter.
    HEC is instance fixed-target-event-calc or equivalent
    which responds to the generic functions START-DATE and END-DATE.
    Arguments:
     page-left page-right page-bottom page-top - location of outer rectangle
      in page coordinates (points, 72/inch, origin is page lower left)
     window-left window-right window-bottom window-top - location of drawing
      window in rectangle.  Coordinates are given as fraction of the window
      width and height, i.e. they range from 0 to 1
     curve - if present, must be list of conses ((time1 . value1) ...)
     pcf - if present, must be PCF to be plotted.
     rect-title - title string for upper left of rectangle, or nil if none
     rect-font - postscript font for rect-title, default Helvetica
     rect-font-size - postscript font size for rect-title, default 9
     window-title - title string for upper left of window, or nil if none
     window-font - font for window-title, default Helvetica
     window-font-size - font size for window-title, default 6
     width - curve or PCF width, default 1
     dash1, dash2 - dash parameters, both nil for solid curve, dash1=1,
      dash2=0 for dotted, dash1=3,dash2=0 for long dash, etc.
     y-scale-min y-scale-max - min and max scales for y axis
     time-axis - t if plot time axis, nil otherwise
     time-label - t if plot time label on axis, nil if just tics
     box-rect - t if draw box around rect
     box-window - t if draw box around window
     "

  (set-page-loc-of-drawing-rect
   where page-left page-right page-bottom page-top) ;LRBT
  (set-loc-of-drawing-window 
   where window-left window-right window-bottom window-top) ;LRBT
  (when rect-title
    (record-font where rect-font rect-font-size)
    (string-at-line-in-rect where :line-number 1 :font-size rect-font-size
                            :string rect-title))
  (when window-title
    (record-font where window-font window-font-size)
    (string-at-line-in-window
     where :line-number 1 :font-size window-font-size
     :string window-title))
  (set-x-window-scale where (start-date HEC) (end-date HEC))
  (if time-axis 
    (time-label 
     where (start-date HEC) (end-date HEC) :label time-label))
  (set-y-window-scale where y-scale-min y-scale-max)
  (unless (= y-scale-min y-scale-max)
    (if curve (plot-curve where curve :linewidth width :dash1 dash1 :dash2 dash2))
    (if pcf (plot-pcf where pcf :linewidth width :dash1 dash1 :dash2 dash2)))
  (if box-rect (box-rect where))
  (if box-window (box-window where))
  )


;;; ----------------------------------------------------------------------
;;;                                               PLOT ANGLES, TIMES, ETC.
;;; ----------------------------------------------------------------------


(defun SUN-ANGLE-AND-PCF-PLOT (HEC 
                               &key where sample-list
                               page-left page-right page-bottom page-top
                               window-left window-right)
  
  "Plot sun angle and sun PCF.  Size of 80 works well.  Sample-list 
    must be a sun sample list."
  
  (let ((angle-list nil)
        (suit nil))
    (with-sun-sample-list 
      sample-list
      (push (cons time (degrees angle)) angle-list)
      (if (null suit) (push (list *minus-infinity* sample-start 1) suit))
      (push (list sample-start sample-end suitability) suit))
    (setf suit (if (null suit) *unity-pcf*
                   (pcf-from-interval-list 
                    (cons (list (second (first suit)) 
                                *plus-infinity* 1) suit))))
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.2 :window-top 0.95
     :curve angle-list
     :rect-title "Sun" :rect-font-size 6
     :y-scale-min 0 :y-scale-max 180 :time-label nil :width 0.25
     :box-rect t :box-window t)
    (label-y-axis where 0 180 :tic-value 10 :tic-size 1 :label nil)
    (label-y-axis where 0 180 :tic-value 30 :tic-size 3 :label t)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.03 :window-top 0.15
     :pcf suit
     :y-scale-min 0 :y-scale-max 1.5 :width 0.25
     :box-rect nil :box-window t)
    (label-y-axis where 0 1.5 :tic-value 1.0 :label t :label-format "~,1f")
    ))
    

(defun MOON-ANGLE-AND-PCF-PLOT (HEC 
				&key where sample-list
				     page-left page-right page-bottom page-top
				     window-left window-right)
  
  "Plot sun angle and sun PCF.  Size of 80 works well.  Sample-list must 
    be a moon sample list."
  
  
  (let ((angle-list nil)
	  (suit nil))
      (with-moon-sample-list 
       sample-list
       (push (cons time (degrees angle)) angle-list)
       (if (null suit) (push (list *minus-infinity* sample-start 1) suit))
       (push (list sample-start sample-end suitability) suit))
      (setf suit (if (null suit) *unity-pcf*
		   (pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 1) suit))))
      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.2 :window-top 0.95
       :curve angle-list
       :rect-title "Moon" :rect-font-size 6
       :y-scale-min 0 :y-scale-max 180 :time-label nil :width 0.25
       :box-rect t :box-window t)
      (label-y-axis where 0 180 :tic-value 10 :tic-size 1 :label nil)
      (label-y-axis where 0 180 :tic-value 30 :tic-size 3 :label t)
      ;;(ps-time-label (start-date HEC) (end-date HEC) :label nil)
      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.03 :window-top 0.15
       :pcf suit
       :y-scale-min 0 :y-scale-max 1.5 :time-label nil :width 0.25
       :box-rect nil :box-window t)
      (label-y-axis where 0 1.5 :tic-value 1.0 :label t :label-format "~,1f")
      ))


(defun BRIGHT-DARK-BRIGHT-TIME-PLOT (HEC &key where sample-list
					page-left page-right page-bottom page-top
					window-left window-right)

  "Plot bright and dark target visibility time, as fraction of orbit.  Whether
    bright time comes at start or end of visibility period is shown.
    Size of 50 works well.  Sample-list must an orb-viewing-sample-list."

    (let ((bright1 nil) (dark1 nil) (bright2 nil)(dark2 nil))
      (with-orb-viewing-sample-list
        sample-list
        (multiple-value-bind
          (b1 d1 b2 d2)
          (bright-dark-bright-times 
           n1 n2 vis-pcf (pcf-multiply vis-pcf dark-pcf))
          (push (cons n1 (* b1 1440.0)) bright1)
          (push (cons n1 (* d1 1440.0)) dark1)
          (push (cons n1 (* b2 1440.0)) bright2)
          (push (cons n1 (* d2 1440.0)) dark2)
          ))
      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.03 :window-top 0.22
       :curve bright1 :width 0.25 :dash1 1 :dash2 0
       :rect-title "Target visibility (minutes)" :rect-font-size 6
       :window-title "Bright at start of target visibility" :window-font-size 6
       :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
      (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
      (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 
      ;;(ps-time-label (start-date HEC) (end-date HEC) :label nil)

      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.27 :window-top 0.48
       :curve dark1 :width 0.25
       :window-title "Shadow time" :window-font-size 6
       :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
      (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
      (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 

      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.52 :window-top 0.73
       :curve bright2 :dash1 2 :dash2 0 :width 0.25
       :window-title "Bright after dark" :window-font-size 6
       :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
      (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
      (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 

      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.77 :window-top 0.94
       :curve dark2 :width 0.25
       :window-title "Shadow time (second interval)" :window-font-size 6
       :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
      (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
      (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 

      ))


(defun VISIBILITY-TIME-PLOT (HEC &key where sample-list
                                 page-left page-right page-bottom page-top
                                 window-left window-right)
  
  "Plot total bright and dark target visibility time, as fraction of orbit.
    Size of 50 works well.  Sample-list must an orb-viewing-sample-list."
  
  (let ((total-vis nil)(dark-vis nil))
    (with-orb-viewing-sample-list
      sample-list
        (push (cons n1 (* (summed-interval-duration (pcf-non-zero-intervals vis-pcf)) 1440)) 
              total-vis)
        (push (cons n1 (* (summed-interval-duration 
                           (pcf-non-zero-intervals (pcf-multiply vis-pcf dark-pcf))) 1440)) 
              dark-vis)
	)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.07 :window-top 0.82
     :curve total-vis :width 0.25 :dash1 1 :dash2 0
     :rect-title "Target visibility (minutes):  total (dotted) and dark (solid)"
     :rect-font-size 6
     :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t
     ;; :time-axis t :time-label t
     )
    (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
    (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f")
    ;;(ps-time-label (start-date HEC) (end-date HEC) :label nil)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.07 :window-top 0.82
     :curve dark-vis :width 0.25
     :y-scale-min 0 :y-scale-max 100)
    ))
    

(defun NORTH-VISIBILITY-TIME-PLOT (HEC &key where sample-list
					    page-left page-right page-bottom page-top
					    window-left window-right)

  "Plot total northern-hemisphere bright and dark target visibility time, as fraction of orbit.
    Size of 50 works well.  Sample-list must an orb-viewing-sample-list."

  (let ((north-vis nil) (north-dark-vis nil))
    (with-orb-viewing-sample-list
     sample-list
     (push (cons n1 (* (summed-interval-duration
			(pcf-non-zero-intervals (pcf-multiply vis-pcf north-pcf))) 1440))
	   north-vis)
     (push (cons n1 (* (summed-interval-duration
			(pcf-non-zero-intervals
			 (pcf-multiply
			  (pcf-multiply vis-pcf north-pcf) dark-pcf)))
		       1440))
	   north-dark-vis))

    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.07 :window-top 0.82
     :curve north-vis :width 0.25 :dash1 1 :dash2 0
     :rect-title "Northern-hemisphere target visibility (minutes):  total (dotted) and dark (solid)"
     :rect-font-size 6
     :y-scale-min 0 :y-scale-max 100 :box-rect t :box-window t)
    (label-y-axis where 0 100 :tic-value 10 :tic-size 1 :label nil)
    (label-y-axis where 0 100 :tic-value 20 :tic-size 3 :label t :label-format "~,0f") 
    ;;(ps-time-label (start-date HEC) (end-date HEC) :label nil)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.07 :window-top 0.82
     :curve north-dark-vis :width 0.25
     :y-scale-min 0 :y-scale-max 100)
    ))


;;; ----------------------------------------------------------------------
;;;                                                         PLOT PCFs ETC.
;;; ----------------------------------------------------------------------

(defun VISIBILITY-PCF-PLOT (HEC &key where dark sample-list
				      page-left page-right page-bottom page-top
				      window-left window-right)
  
  "Plot total visibility PCF.  This is based here on fraction of total orbit
    period in which target is visible.
    Size of 20 works well.  Sample-list must an orb-viewing-sample-list."

    (let ((int-list nil)
          (suit nil)
	  ;; this is a HACK: need to fool compute-orbital-viewing-suitability method...
	  (saved-dark-flag (slot-value HEC 'dark)))
      (setf (slot-value HEC 'dark) dark)
      (with-orb-viewing-sample-list
       sample-list
       (push (list sample-start sample-end 
                   (compute-orbital-viewing-suitability 
                    HEC :sample orb-viewing-sample))
             int-list))
      (setf suit (pcf-from-interval-list int-list))
      (event-plot
       HEC :where where
       :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
       :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
       :pcf suit :width 0.25
       :rect-title (if dark "Dark vis PCF" "Vis PCF") :rect-font-size 6
       :y-scale-min 0 :y-scale-max 1.1 :box-rect t :box-window t)
      (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
      (label-y-axis where 0 1 :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
      (setf (slot-value HEC 'dark) saved-dark-flag)
      ))




;;; ----------------------------------------------------------------------
;;;                                               STAR TRACKER PLOTS (HST)
;;; ----------------------------------------------------------------------


(defun TRACKER-OCC-AVAIL-PLOT (HEC &key where sample-list sensor (clear-tolerance 0)
                                   page-left page-right page-bottom page-top
                                   window-left window-right)
  
  "Plot time that tracker is clear at end of target occultation.  Time is in minutes.
    clear-tolerance is time in days:  how far end of tracker visibility interval
    can be from the end of occultation."
  
  (let* ((suit (tracker-clear-end-occ-pcf HEC :sample-list sample-list :sensor sensor
                                          :clear-tolerance clear-tolerance))
         (max-suit (* 1.1 (pcf-max suit))))
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
     :pcf suit :width 0.25
     :rect-title (format nil "~a clear" sensor) :rect-font-size 6
     :y-scale-min 0 :y-scale-max max-suit :box-rect t :box-window t)
    (label-y-axis where 0 max-suit :tic-value 10 :tic-size 3 :label t :label-format "~d")
    (string-at-line-in-rect where :line-number 2 :font-size 6
                            :string "minutes")
    ))


(defun TWO-TRACKER-AVAIL-PLOT (HEC &key where sample-list sensor1 sensor2 (clear-tolerance 0)
                                   page-left page-right page-bottom page-top
                                   window-left window-right)
  
  "Return PCF whose value is time that two trackers are visible at end of target
    occultation.  Clear-tolerance is how far before occultation end that tracker visibility 
    can end, in days."    
  
  (let* ((suit (tracker-clear-end-occ-pcf 
                HEC :sample-list sample-list :sensor sensor1 :sensor2 sensor2
                :clear-tolerance clear-tolerance))
         (max-suit (* 1.1 (pcf-max suit))))
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.10 :window-top 0.90
     :pcf suit :width 0.25
     :rect-title (format nil "~a ~a" sensor1 sensor2) :rect-font-size 6
     :y-scale-min 0 :y-scale-max max-suit :box-rect t :box-window t)
    (label-y-axis where 0 max-suit :tic-value 10 :tic-size 3 :label t :label-format "~d")
    (string-at-line-in-rect where :line-number 2 :font-size 6
                            :string "avail (min)")
    ))


(defun TRACKER-SUN-MOON-PLOT (HEC &key where sample-list sensor 
                                  page-left page-right page-bottom page-top
                                  window-left window-right)
  
  "Plot tracker boresight angle to sun, moon.  Plot PCF also in separate window below."

  (let ((sun-angle nil)
        (moon-angle nil)
        (sun-moon-pcf nil))

    (with-orb-viewing-sample-list
      sample-list
      (push (cons n1 (degrees (sensor-sun-angle sensor))) sun-angle)
      (push (cons n1 (degrees (sensor-moon-angle sensor))) moon-angle)
      )

    (setf sun-moon-pcf (tracker-sun-moon-avoidance-pcf HEC :sample-list sample-list :sensor sensor))
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.2 :window-top 0.8
     :curve sun-angle
     :rect-title (format nil "~a Sun angle (solid), moon angle (dotted), avoidance PCF" sensor)
     :rect-font-size 6
     :y-scale-min 0 :y-scale-max 180 :time-label nil :width 0.25
     :box-rect t :box-window t)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.2 :window-top 0.8
     :curve moon-angle :dash1 1 :dash2 0 :width 0.25
     :y-scale-min 0 :y-scale-max 180 :time-label nil)
    ;;(ps-time-label (start-date HEC) (end-date HEC) :label nil)
    (label-y-axis where 0 180 :tic-value 10 :tic-size 1 :label nil)
    (label-y-axis where 0 180 :tic-value 30 :tic-size 3 :label t)
    (event-plot
     HEC :where where
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.03 :window-top 0.15
     :pcf sun-moon-pcf
     :y-scale-min 0 :y-scale-max 1.5 :time-label nil :width 0.25
     :box-rect nil :box-window t)
    (label-y-axis where 0 1.5 :tic-value 1.0 :label t :label-format "~,1f")
    ))



;;; Note: interval plots, which were done using the function target-plot-ps
;;;  have been deleted. Use rcs to recover this code in case you want to
;;;  do that again. Get revision 1.10


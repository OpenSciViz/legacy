{
   if ($0 ~ /\-\-\-start\-\-\-/) {
      file = $1; gsub(/\x22/,"", file); gsub(/\.lisp/,".info", file);
   } else if ($0 ~ /\-\-\-end\-\-\-/) {
      close(file);
   } else {
     print $0 > file;
   }
}

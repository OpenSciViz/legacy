;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; repair.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; creation Sep 90 MDJ
;;;
;;; $Log: repair.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.13  1992/10/04  13:13:34  johnston
;;; minor changes:
;;; - changed proclaim to declaim (consistent w/CLtL2)
;;; - made default :keep-assignment-history-p nil in make-csp
;;; - added verbose keyword arg to make-listed-assignments
;;;
;;; Revision 1.12  1992/04/22  16:44:39  johnston
;;; changed to output for make-assignments; new function
;;; last-value-lexical-order
;;;
;;; Revision 1.11  1991/12/01  00:26:27  johnston
;;; no changes
;;;
;;; Revision 1.10  1991/11/26  15:44:55  johnston
;;; minor changes: removed reference to pcl package; extended strategy
;;; for make-assignments to take arbitrary list of var-value strategies
;;;
;;; Revision 1.9  1991/09/04  18:13:48  sponsler
;;; STIF3 changes
;;;
;;; Revision 1.8  91/06/05  22:49:26  johnston
;;; minor changes to domain add/remove routines
;;; change to reset-csp to treat capacity constraints
;;; properly
;;; 
;;; Revision 1.7  91/04/28  17:54:33  johnston
;;; many changes:
;;; - made domain inclusion/exclusion into bit array
;;; - added sort predicate to csp
;;; - allow ignored, locked vars to be remembered across reset-csp
;;; 
;;; Revision 1.6  91/04/05  07:55:39  johnston
;;; minor changes, some new methods for search strategies
;;; 
;;; Revision 1.5  91/03/27  08:48:18  krueger
;;; Updated Show-Csp-State to print a header, and added
;;; Summarize-Csp-State.
;;; 
;;; Revision 1.4  91/03/26  02:14:35  johnston
;;; fixed make-array in preference functions to specify :element-type 'fixnum.
;;; Without this, type declarations of the prefs array as fixnum were
;;; returning garbage numbers under Allegro.
;;; 
;;; Revision 1.3  91/03/25  13:14:44  johnston
;;; minor changes
;;; 
;;; Revision 1.2  91/03/19  08:56:38  johnston
;;; New methods for initial guess, split off utilities.
;;; 
;;; Revision 1.1  90/11/28  09:56:10  johnston
;;; Initial revision
;;; 
;;; Revision 1.7  90/11/26  01:25:39  johnston
;;; cleaned up for explorer; added defdoc
;;; 
;;; Revision 1.6  90/11/25  11:36:45  johnston
;;; added generic assignment routines
;;; 
;;; Revision 1.5  90/11/24  11:23:11  johnston
;;; many changes: most important are documentation, 
;;; constraint cache implemented, why-conflicts
;;; 
;;; Revision 1.3  90/10/29  10:37:18  johnston
;;; preferences added and tested
;;; 
;;; Revision 1.2  90/10/25  09:38:02  johnston
;;; removed capacity constraints to repair-capcon.lisp
;;; 
;;; Revision 1.1  90/10/25  09:32:54  johnston
;;; Initial revision
;;; 
;;; 

;;; -----------------------------------------------------------------------
;;; REPAIR.LISP:  CONSTRAINT SATISFACTION TOOLKIT
;;; -----------------------------------------------------------------------
;;; Notation:
;;;   N = number of vars in CSP (vars numbered 0,...,N-1
;;;   k = number of values in domain of var (values numbered 0,...,k-1"

;;; Notes:
;;;  Changes made 2Mar91 MDJ:
;;;  - added priority slot to tasks (integer default most-positive-fixnum, 
;;;    lower numbers mean higher priority (i.e. highest priority is zero)
;;;  - history completed:  now can mark, backup, etc.
;;;  - added flag keep-assignment-history-p to control whether history is kept
;;;  - generalized initial guess methods, added new ordering rules
;;;  - added csp quality measures
;;;  Changes made 27Apr91 MDJ:
;;;  - changed representation of not-in-domain to use separate bit array,
;;;    not the conflicts array
;;;  - added ability to remove domain values permanently and ignore vars
;;;    permanently, i.e. across reset-csp calls
;;;  - added do-for-all-vars macro
;;;  - added make-listed-assignments to make assignments from a saved list
;;;    in various formats (such as returned by list-assignments)
;;;  - added sort-vars slot to csp, all-vars: now can keep var lists sorted
;;;    if desired.
;;;
;;; STILL TO DO:
;;; - add/subtract conflicts on value: permit value lists, relative values
;;; - special version which uses ipcfs to keep track of conflicts, etc.

;;; -----------------------------------------------------------------------
;;; GLOBALS
;;; -----------------------------------------------------------------------

;;; (declaim '(optimize (speed 3) (safety 0)))


(defparameter *default-var-priority* 100
  "A positive fixnum used as the default var priority value.  Lower numbers
    mean higher priority.")

;; Note: could have made default priority most-positive-fixnum, but then
;; methods which compute priority-weighted quality measures would not
;; work.  This seems a reasonable compromise.


;;; -----------------------------------------------------------------------
;;; CLASS:  VAR (a CSP variable)
;;; -----------------------------------------------------------------------

(defclass VAR ()
  ((var-number
    :initform nil :reader var-number :initarg :var-number
    :documentation 
    "var number")
   (name
    :initform nil :reader name :initarg :name
    :documentation 
    "var name may be any lisp object.  If vars have names, then different 
      vars must have names which are not equal (names are used as keys into
      a var hash-table")
   (priority
    :initform *default-var-priority* :accessor priority :initarg :priority
    :documentation
    "var priority value, fixnum.  Lower values mean higher priority")
   (csp
    :initform nil :reader csp :initarg :csp
    :documentation 
    "CSP instance to which this var belongs")
   (ignored-p
    :initform nil :reader ignored-p
    :documentation
    "flag to ignore variable: t to ignore, nil to not ignore")
   (ignored-permanently-p
    :initform nil :reader ignored-permanently-p
    :documentation
    "flag to ignore variable after a reset-csp")
   (locked-p
    :initform nil :reader locked-p
    :documentation
    "flag set to t if var is locked, nil otherwise")
   (assignment
    :initform nil :reader assignment
    :documentation 
    "current assignment (value number) or nil if not assigned")
   (solution-value
    :initform nil :accessor solution-value
    :documentation 
    "solution value (val number) or nil if not known")
   (min-conflicts
    :initform nil :reader min-conflicts
    :documentation 
    "minimum number of conflicts on any value in domain, or nil if
     needs to be computed")
   (value-count
    :initform nil :reader value-count
    :documentation 
    "number of values for variable (k in the documentation)")
   (values-in-domain-count
    :initform nil :reader values-in-domain-count
    :documentation 
    "number of values currently in domain (<= k)")
   (conflicts
    :initform nil :reader conflicts
    :documentation 
    "array length k: value is number of conflicts on ith value.")
   (prefs
    :initform nil :reader prefs
    :documentation
    "array length k: value is degree of preference on ith value as
      a fixnum.  If this slot is nil there are no preferences")
   (domain-bit-array
    :initform nil :reader domain-bit-array
    :documentation
    "this slot holds either nil (meaning no values are excluded from the domain)
      or a simple array of type bit (i.e. type is '(simple-array bit (*))
      such that when (sbit domain-bit-array val) is 1 then val is in the domain
      when 0 it is not")
   (saved-domain-bit-array
    :initform nil :reader saved-domain-bit-array
    :documentation
    "nil or bit array like domain bit array indicating values that should
      be excluded permanently, i.e. after a reset-csp")
   ))


;;; -----------------------------------------------------------------------
;;; CLASS:  CSP (a CSP problem instance)
;;; -----------------------------------------------------------------------

(defclass CSP ()
  ((name
    :initform "anon-CSP" :accessor name :initarg :name
    :documentation 
    "lisp object which names CSP instance")

   (var-init-hook
    :initform nil :accessor var-init-hook :initarg :var-init-hook
    :documentation 
    "function of one argument, the var instance, 
      which returns a list (number-of-values solution-value).  
      Used for problem initialization.")

   ;; var bookkeeping

   (all-vars
    :initform nil :reader all-vars
    :documentation
    "list of all var instances currently associated with the CSP, including
      unignored, ignored, and locked vars.  This list is read only and
      is used by reset-csp to restore the initial state.")
   (sort-vars
    :initform nil :reader sort-vars :initarg :sort-vars
    :documentation
    "nil or predicate for sorting lists of vars.  If nil, vars are considered
      unordered.  If a predicate is provided then the various lists of
      vars below are kept sorted accordingly.")
   (var-list
    :initform nil :reader var-list
    :documentation 
    "list of var instances (unordered) excluding those which are ignored
      or locked.  This is essentially the 'active' variable list.")
   (ignored-var-list
    :initform nil :reader ignored-var-list
    :documentation
    "list of var instances which are currently marked as ignored")
   (locked-var-list
    :initform nil :reader locked-var-list
    :documentation
    "list of var instances which are locked, i.e. which have assignments
      which should be preserved across resets.  Each var is in one and 
      only of the the lists:  var-list, ignored-var-list, locked-var-list")
   (locked-var-table
    :initform (make-hash-table) :accessor locked-var-table
    :documentation
    "hash table key var instance, value is value assigned to locked var")
   (var-number-table
    :initform (make-hash-table) :accessor var-number-table
    :documentation 
    "hash table, key is var number, value is var instance")
   (vars-named-p
    :initform nil :accessor vars-named-p :initarg :vars-named-p
    :documentation 
    "t if access by name is required, nil otherwise.  If t, var names are
      checked for uniqueness (by equal)")
   (var-name-table
    :initform (make-hash-table :test #'equal) :accessor var-name-table
    :documentation 
    "hash table, key var name (symbol or string), value var instance.
      Built only if vars-named-p is t.  Error signalled if duplicate
      var names encountered.")
   (var-count
    :initform 0 :accessor var-count
    :documentation 
    "total number of vars which are NOT ignored or locked")

   ;; current state tracking

   (assigned-var-count
    :initform 0 :reader assigned-var-count
    :documentation 
    "number of vars with assignments.  Note that ignored vars cannot have 
      assignments and so are NOT counted.  Locked vars are also NOT counted.")
   (conflicting-var-count
    :initform 0 :reader conflicting-var-count
    :documentation 
    "number of vars with conflicts on current assignment, or nil if not
      known.  Only valid if assigned-var-count>0.  Value is 0 in solution state.")
   (max-conflicts
    :initform nil :reader max-conflicts
    :documentation 
    "max number of conflicts on any current assignment, or nil if not known.
      Only valid if assigned-var-count>0.")

   ;; history for backing up

   (keep-assignment-history-p
    :initform t :accessor keep-assignment-history-p
    :initarg :keep-assignment-history-p
    :documentation
    "flag set to t if history should be kept, nil otherwise")
   (assignment-history
    :initform nil :reader assignment-history
    :documentation 
    "list of lists (var old-value new-value) in reverse order of assignment, and
      possibly symbols (markers).")

   ;; cache assignment conflicts
   (cache-assigned-conflicts-p
    :initform nil :accessor cache-assigned-conflicts-p
    :initarg :cache-assigned-conflicts-p
    :documentation 
    "t if current conflicts are to remembered for fast undoing.")
   (conflict-cache
    :initform (make-hash-table :test #'equal) :accessor conflict-cache
    :documentation 
    "Used only if cache-assigned-conflicts-p is non-nil.  Hash table,
      key is var, value is list of lists (var value-range1 value-range2 ...)
      of values constrained by assignment to key variable.
      A value-range is one of the following forms:
       value - a single value, weight 1
       (val1 . val2) - a range of values val1 through val2 inclusive, weight 1
       ((val1 . val2) . weight) - a range of values with specified weight")
   ))

;;; -----------------------------------------------------------------------
;;; PRINT-OBJECT METHODS
;;; -----------------------------------------------------------------------

(defmethod PRINT-OBJECT ((csp CSP) stream)
  (format stream "<~a ~a ~d vars ~d assign (~d w/conf) ~d ignored ~d locked>"
          (class-name (class-of csp))
          (slot-value csp 'name)
          (slot-value csp 'var-count)
          (slot-value csp 'assigned-var-count)
          (slot-value csp 'conflicting-var-count)
          (length (slot-value csp 'ignored-var-list))
          (length (slot-value csp 'locked-var-list))))

(defmethod PRINT-OBJECT ((v var) stream)
  (format stream "<Var ~d" (slot-value v 'var-number))
  (if (slot-value v 'name)
    (format stream " ~a" (slot-value v 'name)))
  (format stream ">"))

;;; -----------------------------------------------------------------------
;;; CONSTRUCTORS
;;; -----------------------------------------------------------------------

(defun MAKE-CSP (&rest args
                       &key 
                       (name "Anon")
                       var-init-hook 
                       (vars-named-p nil)
                       (cache-assigned-conflicts-p nil)
                       (keep-assignment-history-p nil)
                       (CSP-type 'CSP)
                       (sort-vars 'sort-vars-predicate)
                       &allow-other-keys)
  
  "Create new CSP instance and call init-CSP on it.  Instance is returned.
    Arguments (all are optional):
     name:  CSP print name (string or symbol)
     var-init-hook:  function of one arg, the var instance,
       which returns list number of values in domain, solution value or nil)
       If nil or missing, these must be provided by make-var
     vars-named-p:  flag t or nil whether vars have (unique) names
     cache-assigned-conflicts-p:  flag t to cache conflicts (note
       that methods must support caching if t
     keep-assignment-history-p:  flag t to keep history by default (can
       be turned off or on later)
     CSP-type:  default CSP, used in call to make-instance
     sort-vars: nil or a predicate function to apply to two vars to specify
      sort order
    All args passed through to init-csp."
  
  (let ((instance (make-instance 
                   CSP-type
                   :name name
                   :var-init-hook var-init-hook
                   :vars-named-p vars-named-p
                   :keep-assignment-history-p keep-assignment-history-p
                   :cache-assigned-conflicts-p  cache-assigned-conflicts-p
                   :sort-vars sort-vars)))
    (init-csp instance args)
    instance))

(defmethod INIT-CSP ((c CSP) args)
  
  "Type-specific CSP initialization.  Default does nothing.
    Called by make-csp. Args is list of keyword value arguments
    which were supplied to MAKE-CSP.  Customize this method for
    each CSP type."
  
  (declare (ignore args)))


(defun MAKE-VAR (&rest args
                       &key
                       CSP 
                       number 
                       name 
                       (var-type 'VAR)
                       value-count
                       solution-value
                       (priority *default-var-priority*)
                       &allow-other-keys)

  "Create new var instance attached to specified CSP.
    Call method INIT-VAR on new instance passing through args to 
    this function.  Arguments:
     CSP:  the CSP instance to attach to.  Required.
     number:  var number (required)
     name:  var name, required if vars-named-p is t, must be unique.
      May be nil if no names.
     var-type:  allows specification of specialization of class var, 
      which is the default.
     value-count:  if supplied, this overrides the value from var-init-hook.
      Must be supplied if var-init-hook is nil.
     solution-value:  if supplied, this overrides the value from var-init-hook.
      May be nil or omitted (defaults to nil).
     priority:  initial value for priority.  Defaults to *default-var-priority*
      if unspecified or nil.
    Var instance is returned."

  (let ((instance
         (make-instance var-type
                        :var-number number
                        :name name
                        :csp CSP
                        :priority priority)))
    (init-var instance args)
    (attach-var CSP instance :value-count value-count 
                :solution-value solution-value)
    instance))


(defmethod ATTACH-VAR ((c CSP) (v var) 
                       &key (value-count nil) (solution-value nil))
  
  "Attach var to CSP.  Var instance number, CSP, and name fields must 
    have been initialized (e.g. by make-var).  This calls var-init-hook 
    to get remaining info, unless provided in arg list.  
    This function should not be called by users:
    it is automatically called by MAKE-VAR."
  
  ;; set up var:  note that supplied args take precedence over var-init-hook
  (let* ((var-initlist (if (var-init-hook c) (funcall (var-init-hook c) v)))
         (k (or value-count (first var-initlist)))
         (solution (or solution-value (second var-initlist))))
    ;; must get value-count from somewhere...
    (if (not k)
      (error "No value-count known for var ~a" v))
    (setf (slot-value v 'assignment) nil)
    (setf (slot-value v 'ignored-p) nil)
    (setf (slot-value v 'ignored-permanently-p) nil)
    (setf (slot-value v 'locked-p) nil)
    (setf (slot-value v 'solution-value) solution)
    (setf (slot-value v 'min-conflicts) nil)
    (setf (slot-value v 'value-count) k)
    (setf (slot-value v 'values-in-domain-count) k)
    (setf (slot-value v 'conflicts)
          (make-array k :initial-element 0 :element-type 'fixnum)))
  
  ;; attach to CSP
  (with-slots (all-vars
               var-list
               var-number-table
               vars-named-p
               var-name-table
               var-count
               sort-vars) c
    
    ;; error check: numbers must be unique
    (when (gethash (var-number v) var-number-table)
      (error "var numbered ~a already defined: ~a"
             (var-number v) (gethash (var-number v) var-number-table)))
    ;; add var to all-vars, var-list and to number table
    (if sort-vars
      (setf all-vars (merge 'list all-vars (list v) sort-vars))
      (push v all-vars))
    (if sort-vars
      (setf var-list (merge 'list var-list (list v) sort-vars))
      (push v var-list))
    (setf (gethash (var-number v) var-number-table) v)
    
    ;; check that name is unique if vars are named.  If so, add to name table.
    (when vars-named-p
      (cond ((gethash (name v) var-name-table)
             (error "duplicate name for var ~a" v))
            (t (setf (gethash (name v) var-name-table) v))))
    
    ;; bump the var-count
    (incf var-count))
  
  )


(defmethod INIT-VAR ((v VAR) args)
  "Type-specific initialization.  Default does nothing.  Called
    by make-var before var is attached to CSP.  Args is list of keyword
    value arguments to MAKE-VAR"
  (declare (ignore args)))


(defmethod SORT-VARS-PREDICATE ((v1 var) (v2 var))

  "standard sorting method that can always be used:  sort vars by
    number"

  (< (var-number v1) (var-number v2)))

;;; -----------------------------------------------------------------------
;;; VAR METHODS:  USER-CALLABLE
;;; -----------------------------------------------------------------------
;;;
;;; Note:  -aux functions are not user callable (they are used to make the
;;; methods more efficient.

;;; --- General methods ---

;;; (CSP (V VAR)) is a slot reader

(defmethod VAR-NAME ((v var))
  "return var name (slot value)"
  (name v))

;;; (VAR-NUMBER (V VAR)) is a var slot reader

;;; (VALUE-COUNT (V VAR)) is a var slot reader

;;; (SOLUTION-VALUE ((V VAR))) is a slot accessor

(defmethod VALUE-NUMBER ((v var) value)
  
  "Return number of value (i.e. return value).
    Useful when ordering values lexically
    Example: (value-number v 12) -> 12"
  
  value)


;;; --- Initialization ---


(defmethod RE-INITIALIZE-VARIABLE ((v var))
  
  "Back to initial state immediately.
    Ignored variables are made unignored unless they are marked as 
      ignored-permanently-p t.
    Locked vars are unlocked.
    The domain is set back to the full set of possible values, unless 
      there are domain values marked as excluded permanently.
    The var is made unassigned.
    Preferences are NOT affected.
    Called by RESET-CSP."
  
  (setf (slot-value v 'assignment) nil)
  (setf (slot-value v 'ignored-p) (slot-value v 'ignored-permanently-p))
  (setf (slot-value v 'locked-p) nil)
  (setf (slot-value v 'min-conflicts) 0)
  (setf (slot-value v 'values-in-domain-count) (value-count v))
  (fill (conflicts v) 0)

  ;; set up the domain
  (let ((domain (domain-bit-array v))
        (saved-domain (saved-domain-bit-array v)))
    (if domain (fill (domain-bit-array v) 1))
    (when saved-domain
      (bit-and domain saved-domain t)
      (setf (slot-value v 'values-in-domain-count)
            (count 1 domain))
      ;; mark as ignored is all domain values removed
      ;; note: NOT as ignored permanently.  This follows behavior
      ;; of remove-value-from-domain, which marks as temporarily
      ;; ignored if all values removed
      (if (= 0 (slot-value v 'values-in-domain-count))
        (setf (slot-value v 'ignored-p) t))
      ))

  ;; returned value
  v)



;;; --- Information about conflicts ---

(defmethod ALL-VALUES-IN-DOMAIN-HAVE-CONFLICTS-P ((v var))
  
  "Predicate returning t if all values in domain have non-zero conflicts.
    If there are no values in the domain, this function returns nil."
  
  (let ((conflicts-array (conflicts v))
        (domain (domain-bit-array v))
        (value-count (value-count v)))
    (not
     (dotimes (i value-count)
       (if (and (in-domain-p i domain)
                (= (aref conflicts-array i) 0))
         (return t))))))
  

(defmethod CONFLICTS-ON-VALUE ((v var) val)
  
  "Return number of conflicts on specified value, nil if value not in domain"
  
  (declare (optimize (speed 3)(safety 1))
           (type fixnum val))
  (cond ((in-domain-p val (domain-bit-array v))
         (aref (conflicts v) val))
        (t nil)))


(defmethod VALUES-WITH-CONFLICTS-COUNT ((v var))
  "number of values in domain that have conflicts >0"
  (values-with-conflicts-count-aux
   (conflicts v)
   (value-count v)
   (domain-bit-array v)))


(defmethod VALUES-WITH-NO-CONFLICTS-COUNT ((v var))
  "number of values in domain that have conflicts=0"
  (- (values-in-domain-count v)
     (values-with-conflicts-count-aux
      (conflicts v)
      (value-count v)
      (domain-bit-array v))))


(defun VALUES-WITH-CONFLICTS-COUNT-AUX (conflicts value-count domain)
  
  "Return number of values with conflicts.
    conflicts is an array of fixnum of size value-count
    domain is the contents of slot domain-bit-array (nil or bit-array)"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) conflicts))
  (let ((result 0))
    (declare (type fixnum result))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (if (and (in-domain-p i domain)
               (> (aref conflicts i) 0))
        (incf result)))
    result))


(defmethod VALUES-WITH-MIN-CONFLICTS-COUNT ((v var))

  "Number of values in domain that have conflicts = min-conflicts"

  (values-with-min-conflicts-count-aux
   (conflicts v)
   (value-count v)
   (min-conflicts v)
   (domain-bit-array v)))


(defun VALUES-WITH-MIN-CONFLICTS-COUNT-AUX 
       (conflicts value-count min-conflicts domain)
  
  "Aux function for VALUES-WITH-MIN-CONFLICTS-COUNT.
    Return number of values with conflicts = min-conflicts.
    conflicts is an array of fixnum of size value-count
    domain is the contents of slot domain-bit-array (nil or bit-array)"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts))
  (let ((result 0))
    (declare (type fixnum result))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (if (and (in-domain-p i domain)
               (= (aref conflicts i) min-conflicts))
        (incf result)))
    result))


(defmethod SUMMED-CONFLICT-COUNT ((v var))
 
  "Return sum of conflict count on all values in domain."

  (summed-conflict-count-aux 
   (conflicts v)
   (value-count v)
   (domain-bit-array v)))


(defun SUMMED-CONFLICT-COUNT-AUX (conflicts value-count domain)
  
  "Return sum of conflict counts on all values"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) conflicts))
  (let ((result 0))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (when (in-domain-p i domain)
        (incf result (aref conflicts i))))
    result))


(defmethod MEAN-CONFLICT-COUNT ((v var))

  "Return mean number of conflicts on values in domain.  If there are
    no values in the domain, return 0."

  (cond ((= 0 (values-in-domain-count v))
         0)

        (t (/ (summed-conflict-count v)
              (values-in-domain-count v)))))


;;; (MIN-CONFLICTS ((V VAR))) is a var slot reader:  before method below
;;;   ensures that slot holds current value


(defmethod MIN-CONFLICTS :BEFORE ((v var))

  "Compute min-conflicts value if slot holds nil; store in slot"

  (when (null (slot-value v 'min-conflicts))
    (setf (slot-value v 'min-conflicts)
          (min-conflicts-aux 
           (conflicts v) (value-count v) (domain-bit-array v)))))


(defun MIN-CONFLICTS-AUX (conflicts value-count domain)
  
  "Find the min value in the conflicts array
    conflicts is an array of fixnum of size value-count
    domain is the contents of slot domain-bit-array (nil or bit-array)"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) conflicts))
  (let ((min-conf most-positive-fixnum))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (let ((conf (aref conflicts i)))
        (declare (type fixnum conf))
        (if (in-domain-p i domain)
          (setf min-conf (min min-conf conf)))))
    min-conf))




;;; --- methods returning values or list of values ---

(defmethod ALL-VALUES-LEXICAL-ORDER ((v var))

  "Return list of value numbers in domain, ordered lexically"

  (all-values-lexical-order-aux 
   (value-count v) (domain-bit-array v)))


(defun ALL-VALUES-LEXICAL-ORDER-AUX (value-count domain)
  
  "Aux function for ALL-VALUES-LEXICAL-ORDER.  Return list of all values
    in conflicts array which are in domain."
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count))
  (let ((result nil) (i 0))
    (declare (type fixnum i))
    ;; push in reverse order so returned list is in value order
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (if (in-domain-p i domain) (push i result)))
    result))


(defmethod FIRST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS ((v var))
  
  "Return first value in domain, by lexical ordering, which has number 
    of conflicts equal to current minimum conflicts.  May return nil if
    there are no values in domain"
  
  (first-value-lexical-order-with-min-conflicts-aux
   (conflicts v)
   (value-count v)
   (min-conflicts v)
   (domain-bit-array v)))


(defun FIRST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS-AUX 
       (conflicts value-count min-conflicts domain)

  "Aux function for method FIRST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS:
    return first value in conflicts array equal to min-conflicts"

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts))
  (dotimes (i value-count)
    (declare (type fixnum i))
    (if (and (in-domain-p i domain)
             (= (aref conflicts i) min-conflicts))
      (return i))))


(defmethod FIRST-VALUE-LEXICAL-ORDER ((v var))
  
  "Return first value in domain, by lexical ordering.  May return nil if
    there are no values in domain"
  
  (first-value-lexical-order-aux   
   (value-count v)
   (domain-bit-array v)))


(defun FIRST-VALUE-LEXICAL-ORDER-AUX 
       (value-count domain)

  "Aux function for method FIRST-VALUE-LEXICAL-ORDER:
    return first value in domain"

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count))
  (dotimes (i value-count)
    (declare (type fixnum i))
    (if (in-domain-p i domain)
      (return i))))


(defmethod FIRST-VALUE-LEXICAL-ORDER-WITH-NO-CONFLICTS ((v var))
  
  "Return first value in domain, by lexical ordering, which has no
    conflicts.  May return nil if there are no values with on conflicts."
  
  (first-value-lexical-order-with-min-conflicts-aux
   (conflicts v)
   (value-count v)
   0
   (domain-bit-array v)))


(defmethod LAST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS ((v var))
  
  "Return last value in domain, by lexical ordering, which has number 
    of conflicts equal to current minimum conflicts.  May return nil if
    there are no values in domain"
  
  (last-value-lexical-order-with-min-conflicts-aux
   (conflicts v)
   (value-count v)
   (min-conflicts v)
   (domain-bit-array v)))


(defun LAST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS-AUX 
       (conflicts value-count min-conflicts domain)

  "Aux function for method LAST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS:
    return last value in conflicts array equal to min-conflicts"

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts))
  (let ((i 0))
    (declare (type fixnum i))
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (if (and (in-domain-p i domain)
               (= (aref conflicts i) min-conflicts))
        (return i)))))


(defmethod LAST-VALUE-LEXICAL-ORDER ((v var))
  
  "Return last value in domain, by lexical ordering.  May return nil if
    there are no values in domain"
  
  (last-value-lexical-order-aux
   (value-count v)
   (domain-bit-array v)))


(defun LAST-VALUE-LEXICAL-ORDER-AUX 
       (value-count domain)

  "Aux function for method LAST-VALUE-LEXICAL-ORDER:
    return last value in domain"

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count))
  (let ((i 0))
    (declare (type fixnum i))
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (if (in-domain-p i domain)
        (return i)))))


(defmethod LAST-VALUE-LEXICAL-ORDER-WITH-NO-CONFLICTS ((v var))
  
  "Return last value in domain, by lexical ordering, which has no
    conflicts.  May return nil if there are no values with no conflicts."
  
  (last-value-lexical-order-with-min-conflicts-aux
   (conflicts v)
   (value-count v)
   0
   (domain-bit-array v)))


(defmethod ALL-MIN-CONFLICTS-VALUES ((v var))
  
  "Return list of all value numbers in domain with min conflicts"
  
  (all-min-conflicts-values-aux
   (conflicts v)
   (value-count v)
   (min-conflicts v)
   (domain-bit-array v)))


(defun ALL-MIN-CONFLICTS-VALUES-AUX (conflicts value-count min-conflicts domain)

  "Return list of all min-conflicts value numbers in domain.
    conflicts is an array of fixnum of size value-count
    min-conflicts is the minimum number of conflicts (fixnum)
    domain is the contents of slot domain-bit-array: values not in
     domain are excluded."

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts))
  (let ((result nil) (i 0))
    (declare (type fixnum i))
    ;; push in reverse order so returned list is in value order
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (if (and (in-domain-p i domain)
               (= (aref conflicts i) min-conflicts))
        (push i result)))
    result))


(defmethod A-RANDOM-VALUE-WITH-MIN-CONFLICTS ((v var))

  "Return one value with min-conflicts, selected at random"

  (a-random-element-from-list (all-min-conflicts-values v)))


(defmethod N-RANDOM-VALUES-WITH-MIN-CONFLICTS ((v var) n)

  "Return n values with min-conflicts, selected at random.  If there
    are <= n values with min conflicts, return all of them."

  (N-random-elements-from-list (all-min-conflicts-values v) n))


(defmethod ALL-VALUES-ORDERED-BY-MIN-CONFLICTS ((v var))

  "Sorted list of values in domain, min-conflicts first"

  (all-values-ordered-by-min-conflicts-aux
   (conflicts v) (value-count v) (domain-bit-array v)))


(defun ALL-VALUES-ORDERED-BY-MIN-CONFLICTS-AUX (conflicts value-count domain)

  "Aux function for ALL-VALUES-ORDERED-BY-MIN-CONFLICTS.
    Return list of all min-conflicts value numbers
    conflicts is an array of fixnum of size value-count,
    domain is the contents of slot domain-bit-array: values not in
     domain are excluded."

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) conflicts))
  (let ((value-list nil) (i 0))
    (declare (type fixnum i))
    ;; build value list unsorted in reverse order
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (if (in-domain-p i domain) (push i value-list)))
    ;; sort it
    (setf value-list
      (stable-sort value-list
		   #'(lambda (x y)
		       (declare (optimize (speed 3) (safety 1))
				(type fixnum x y))
		       (< (aref conflicts x) (aref conflicts y)))))
    value-list))


(defmethod ALL-VALUES-MIN-CONFLICTS-FIRST ((v var))
  
  "Return list of value numbers in domain, min-conflicts first, others
    unordered"
  
  (all-values-min-conflicts-first-aux
   (conflicts v) (value-count v) (min-conflicts v) (domain-bit-array v)))


(defun ALL-VALUES-MIN-CONFLICTS-FIRST-AUX 
       (conflicts value-count min-conflicts domain)
  
  "Aux function for ALL-VALUES-MIN-CONFLICTS-FIRST.
    Return list of value numbers in domain, min-conflicts first, others
    unordered.
    CONFLICTS is an array of size VALUE-COUNT, with minimum conflict value
    MIN-CONFLICTS.  Values not in domain are ignored."
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts))
  (let ((r1 nil)(r2 nil) (i 0))
    (declare (type fixnum i))
    ;; push in reverse order so returned list is in value order
    (dotimes (j value-count)
      (declare (type fixnum j))
      (setf i (- value-count j 1))
      (let ((conf (aref conflicts i)))
        (declare (type fixnum conf))
        (cond ((not (in-domain-p i domain))) ;not in domain, ignore
              ((= conf min-conflicts) ;min conflicts value
               (push i r1))
              (t ;in domain, not min
               (push i r2)))))
    (nconc r1 r2)))


;;; --- Assignments ---

(defmethod HAS-ASSIGNED-VALUE-P ((v var))

  "Return non-nil if var has an assigned value, nil otherwise"

  (assignment v))


(defmethod ASSIGNED-VALUE ((v var))

  "Return assigned value or nil if not assigned"

  (assignment v))


(defmethod ASSIGNED-VALUE-HAS-CONFLICTS-P ((v var))

  "Return t if there is an assignment on v and it has conflicts, nil otherwise"

  (let ((a (assignment v)))
    (and a (> (aref (conflicts v) a) 0))))


(defmethod CONFLICTS-ON-ASSIGNED-VALUE ((v var))

  "If there is an assignment, return the number of conflicts on it.
    return nil otherwise"

  (let ((a (assignment v)))
    (if a (aref (conflicts v) a) nil)))


(defmethod ASSIGN-VALUE ((v var) value)

  "Make the assignment.  Return new value if assignment changed, nil otherwise.
    assignment will not be made if value is not in domain or if var is ignored
    or locked.  In this case no action is taken and no error indicated."

  ;; nil is ok for value: it means to unassign
  (when (and (numberp value)
	     (>= value (value-count v)))
    (error "value ~a out of range for var ~a" value v))

  (let ((assignment (assignment v)))
    (cond 
     
     ;; var is ignored or locked
     ((or (ignored-p v)
          (locked-p v))
      ;; do nothing
      nil)

     ;; do nothing: no change in assignment
     ((or (and (null assignment) (null value))
          (and assignment value (= assignment value)))
      nil)
     
     ;; unassign value
     ((and assignment (null value))
      (unassign-value v)
      nil)
     
     ;; no assignment now, make it
     ((null assignment)
      (let ((new-assignment value)
            (new-conflicts-on-assignment (aref (conflicts v) value))
            (csp (csp v)))
        (cond 
	 ;; check that new assignment is in domain
         ((not (in-domain-p value (domain-bit-array v)))
          nil)
         (t ;; in domain, so make assignment
          (setf (slot-value v 'assignment) value)
          (update-history csp v nil value)
          (update-assignment-count csp 1)
          (update-max-conflicts csp nil new-conflicts-on-assignment)
          (if (/= 0 new-conflicts-on-assignment)
	      (update-conflicting-var-count csp 1))
          (csp-var-value-assign csp v new-assignment)
	  value))))
     
     ;; change the assignment
     (t 
      (let* ((conflicts (conflicts v))
             (old-assignment assignment)
             (old-conflicts-on-assignment (aref conflicts assignment))
             (new-assignment value)
             (new-conflicts-on-assignment (aref conflicts value))
             (csp (csp v)))
        (cond 
         ;; check that new assignment is in domain
         ((not (in-domain-p value (domain-bit-array v)))
          nil)
         (t ;; in domain, so make assignment
          (setf (slot-value v 'assignment) value)
          (update-history csp v old-assignment value)
          (update-max-conflicts 
           csp old-conflicts-on-assignment new-conflicts-on-assignment)
          ;; adjust the number of vars with conflicts
          (cond ((= new-conflicts-on-assignment 0)
                 ;; no conflicts on new assignment
                 (if (> old-conflicts-on-assignment 0)
                   (update-conflicting-var-count csp -1)))
                (t ;; conflicts on new assignment
                 (if (= old-conflicts-on-assignment 0)
                   (update-conflicting-var-count csp +1))))
          (csp-var-value-unassign csp v old-assignment)
          (csp-var-value-assign   csp v new-assignment)
          value)))))))


(defmethod UNASSIGN-VALUE ((v var))

  "Make var unassigned.  If var is ignored or locked no action is taken."

  (let ((assignment (assignment v)))
    (cond 

     ((or (ignored-p v)
          (locked-p v))
      ;; var is ignored: no action
      nil)

     ((null assignment)
      ;; already unassigned: do nothing
      nil)
     
     (t ;; unassign value
      (let ((old-assignment assignment)
            (old-conflicts-on-assignment (aref (conflicts v) assignment))
            (csp (csp v)))
        (setf (slot-value v 'assignment) nil)
	(update-history csp v old-assignment nil)
        ;; reduce number of assigned vars
        (update-assignment-count csp -1)
        ;; update max conflicts
        (update-max-conflicts csp old-conflicts-on-assignment nil)
        ;; if there were conflicts on assignment, this var would have
        ;; contributed to conflicting-var-count.  If so, subtract it.
        (if (/= 0 old-conflicts-on-assignment)
          (update-conflicting-var-count csp -1))
        (csp-var-value-unassign csp v old-assignment))
      nil))))


(defmethod ASSIGN-VALUE-AT-RANDOM ((v var))

  "Assign a value at random from all vars currently in domain."

  (assign-value v (a-random-element-from-list 
		   (all-values-lexical-order v))))


(defmethod ASSIGN-MIN-CONFLICTS-VALUE-AT-RANDOM ((v var))

  "Assign a min-conflicts value at random.  If there are no choices,
    the var is made unassigned."

  (assign-value v (a-random-value-with-min-conflicts v)))


(defmethod ASSIGN-SOLUTION-VALUE ((v var))

  "Assign solution value, if known.  Otherwise no change in assignment."

  (if (solution-value v)
    (assign-value v (solution-value v))))


(defmethod ASSIGN-NON-SOLUTION-VALUE-AT-RANDOM ((v var))
  
  "Assign a value at random excluding solution value (if known) and
    excluding values not in domain.  If there are no choices, the
    var is made unassigned."
  
  (let ((value-count (value-count v))
        (solution (solution-value v)) ;may be nil
        (pick-from nil)
        (domain (domain-bit-array v)))
    
    (declare (optimize (speed 3)(safety 1))
             (type fixnum value-count)
             (type (simple-array fixnum (*)) conflicts))
    
    (dotimes (i value-count)
      (declare (type fixnum i))
      (cond ((and solution (= solution i))
             ;; don't include
             )
            ((not (in-domain-p i domain))
             ;; not in domain: don't include
             )
            (t (push i pick-from))))
    (assign-value v (a-random-element-from-list pick-from))))


;;; --- Update conflict counts ---

(defmethod ADD-TO-CONFLICTS-ON-VALUE ((v var) value &optional (increment 1))
  
  "Add the specified increment to the conflict count for the specified
    value."
  
  (declare (optimize (speed 3)(safety 1))
           (type fixnum value increment))
  
  (let* ((conflicts (conflicts v))
         (value-in-domain-p (in-domain-p value (domain-bit-array v)))
         ;; don't trigger calculation of min-conflicts if not known
         (min-conflicts (slot-value v 'min-conflicts))
         (assignment (assignment v))
         (locked (locked-p v))
         (ignored (ignored-p v))
         (csp (csp v))
         (old-count (aref conflicts value))
         (new-count (+ old-count increment)))
    
    (declare (type fixnum old-count new-count)
             (type (simple-array fixnum (*)) conflicts))
    
    ;; need to recompute the minimum?
    (when (and min-conflicts value-in-domain-p)
      ;; if the old value was the minimum, then need to recompute
      (if (= (the fixnum min-conflicts) old-count)
        (setf (slot-value v 'min-conflicts) nil)))
    
    (when (and assignment (= assignment value)
               (not locked) (not ignored))
      ;; we are increasing the conflicts on the current assignment
      ;; if the number used to be zero, we are adding a var with conflicts
      (if (and (= old-count 0) (> new-count 0))
        (update-conflicting-var-count csp +1))
      ;; update max conflicts
      (update-max-conflicts csp old-count new-count))
    
    (setf (aref conflicts value) new-count)))


(defmethod SUBTRACT-FROM-CONFLICTS-ON-VALUE ((v var) value 
                                             &optional (decrement 1))
  
  "Subtract the specified decrement from the conflict count for the specified
    value."
  
  (declare (optimize (speed 3)(safety 1))
           (type fixnum value decrement))
  
  (let* ((conflicts (conflicts v))
         (value-in-domain-p (in-domain-p value (domain-bit-array v)))
         ;; don't trigger calculation of min-conflicts if not known
         (min-conflicts (slot-value v 'min-conflicts))
         (assignment (assignment v))
         (locked (locked-p v))
         (ignored (ignored-p v))
         (csp (csp v))
         (old-count (aref conflicts value))
         (new-count (- old-count decrement)))
    
    (declare (type fixnum old-count new-count)
             (type (simple-array fixnum (*)) conflicts))
    
    ;; sanity check:  if new-count is negative we've made a mistake 
    (if (< new-count 0) (error "conflicts < 0"))
    
    ;; need to recompute the minimum?
    (when (and min-conflicts value-in-domain-p)
      ;; if the new value is less than the minimum, then set the minimum
      (if (< new-count (the fixnum min-conflicts))
        (setf (slot-value v 'min-conflicts) new-count)))
    
    ;; check assignments
    (when (and assignment (= assignment value)
               (not locked) (not ignored))
      ;; we are decreasing the conflicts on the current assignment
      ;; if the new number is zero, we are reducing the number of vars with
      ;; conflicts
      (if (and (= new-count 0) (> old-count 0))
        (update-conflicting-var-count csp -1))
      ;; update max conflicts
      (update-max-conflicts csp old-count new-count))
    
    (setf (aref conflicts value) new-count)))


;;; --- Ignore/unignore var ---

;;; (IGNORED-P (V VAR)) is a slot reader

;;; (IGNORED-PERMANENTLY-P (V VAR)) is a slot reader

(defmethod IGNORE-VAR ((v var) &key (permanent nil))

  "Mark var as ignored.  If it currently has an assignment, first
    make it unassigned (an ignored var cannot have an assignment).
    Note that ignored vars are not counted by var-count (csp method):
    for all practical purposes they are no longer part of the csp
    problem (until they are unignored).
    If the keyword argument :permanent is non-nil, the var is marked
    as to be ignored permanently (i.e. after a reset-csp)"

  (when (has-assigned-value-p v)
    (unassign-value v))
  (setf (slot-value v 'ignored-p) t)
  (if permanent (setf (slot-value v 'ignored-permanently-p) t))
  (inform-csp-var-ignored (csp v) v))

(defmethod UNIGNORE-VAR ((v var) &key (permanent nil))

  "Mark var as not to be ignored"

  (setf (slot-value v 'ignored-p) nil)
  (if permanent (setf (slot-value v 'ignored-permanently-p) nil))
  (inform-csp-var-unignored (csp v) v))

;;; --- locking/unlocking variables ---

;;; (LOCKED-P (V VAR)) is a slot reader

(defmethod LOCK-VAR ((v var) value &key (restore-value nil)
                     (warn-if-cannot-lock t))

  "Assign value to var and mark as locked.  If var is ignored it is made
    unignored first.  If var is already locked it is made unlocked first.
    If optional argument restore-value is t, then the value is restored
    to the domain before locking, otherwise if value is not in domain 
    then no assignment is made and the var is not locked.
    In this case, a warning is emitted only if warn-if-cannot-lock is non-nil.
    Note that locked vars are not counted by var-count (csp method).  
    For all practical purposes they are no longer part of the problem."

  (if (ignored-p v) (unignore-var v))
  (if (locked-p v) (unlock-var v))
  (when restore-value
    (unless (value-in-domain-permanently-p v value)
      (restore-value-to-domain v value :permanent t))
    (unless (value-in-domain-p v value)
      (restore-value-to-domain v value)))
  (assign-value v value)
  ;; only lock if value was assigned
  (cond ((has-assigned-value-p v)
         (setf (slot-value v 'locked-p) t)
         (inform-csp-var-locked (csp v) v value))
        (t (if warn-if-cannot-lock
             (warn "Unable to lock ~a" v))))
  )


(defmethod UNLOCK-VAR ((v var) &key (unassign nil))

  "Mark var as unlocked.  If key argument :unassign is t then value is
    unassigned, otherwise var retains value it had when locked."

  (setf (slot-value v 'locked-p) nil)
  (inform-csp-var-unlocked (csp v) v)
  (if unassign
    (unassign-value v))
  )

;;; --- domain manipulation ---


(defmethod REMOVE-VALUE-FROM-DOMAIN ((v var) value
                                     &key (permanent nil))
  
  "Remove value from domain if currently in domain. If value is
    already removed, then no action is taken and no error signalled. 
    If the key argument :permanent is t, then the value is removed permanently.
    If the removed value is the currently assigned value, the var is made 
    unassigned first.  
    If the value being removed is the current assignment
    of a locked var, then a continuable error is emitted:  if continued,
    the var will be unlocked.
    If the removal of this value makes
    values-in-domain-count zero (i.e. there are no values left in the
    domain, the var is made ignored.  CAVEAT:  restoring values to the
    domain will NOT make the var unignored!"
  
  ;; if this is a permanent remove, record in saved-domain-bit-array
  (when permanent
    (let ((saved-domain (saved-domain-bit-array v)))
      ;; make permanent domain bit array if necessary
      (when (null saved-domain)
        (setf saved-domain (make-array (value-count v)
                                       :element-type 'bit
                                       :initial-element 1))
        (setf (slot-value v 'saved-domain-bit-array) saved-domain))
      ;; mark the value as permanently removed
      (setf (sbit saved-domain value) 0)))
  
  (let ((domain (domain-bit-array v)))
    ;; action taken when value is now in the domain
    (when (in-domain-p value domain)
      
      ;; unassign if assigned value is being removed from domain
      (when (and (has-assigned-value-p v)
                 (= (assigned-value v) value))
        (when (locked-p v)
          ;; removing assignment value for a locked var: error
          (cerror "var will be unlocked"
                  "removing domain value ~a assigned for locked var ~a"
                  value v)
          (unlock-var v))
        (unassign-value v))
      
      ;; if this is the first value being removed from the domain,
      ;; create the domain bit array
      (when (null domain)
        (setf domain (make-array (value-count v)
                                 :element-type 'bit
                                 :initial-element 1))
        (setf (slot-value v 'domain-bit-array) domain))
      
      ;; remove value from domain array
      (setf (sbit domain value) 0)
      
      ;; if the conflict count on the removed value is the current min,
      ;; then mark as needing it re-computed
      (let ((min-conf (slot-value v 'min-conflicts)))
        (when (and min-conf (= min-conf (aref (conflicts v) value)))
          (setf (slot-value v 'min-conflicts) nil)))
      
      ;; update the count of values currently in domain
      (setf (slot-value v 'values-in-domain-count) 
            (- (values-in-domain-count v) 1))
      
      ;; ignore if all values removed from domain
      (if (= 0 (values-in-domain-count v))
        (ignore-var v)))))


(defmethod RESTORE-VALUE-TO-DOMAIN ((v var) value &key (permanent nil))
  
  "Restore value to domain if currently not in domain.  If keyword arg
    :permanent is non-nil, then mark the var as being restored after a
    reset-csp."
  
  ;; if this is a permanent restore, record in saved-domain-bit-array
  (when permanent
    (let ((saved-domain (saved-domain-bit-array v)))
      ;; mark the value as permanently restored
      (if saved-domain (setf (sbit saved-domain value) 1))))
  
  ;; action taken when restored
  (let ((domain (domain-bit-array v)))
    (when (not (in-domain-p value domain))
      
      ;; mark the value as in domain
      (setf (sbit domain value) 1)
      
      ;; check min-conf:  if conflicts on the restored value are
      ;; < min conf, then set it
      (let ((min-conf (slot-value v 'min-conflicts))
            (current-conf (aref (conflicts v) value)))
        (when (and min-conf (< current-conf min-conf))
          (setf (slot-value v 'min-conflicts) current-conf)))
      
      (setf (slot-value v 'values-in-domain-count) 
            (+ (values-in-domain-count v) 1)))))
      

(defmethod RESTORE-ALL-VALUES-TO-DOMAIN ((v var) &key (permanent nil))
  
  "Restore all values to domain:  runs RESTORE-VALUE-TO-DOMAIN on all
    values not currently in domain.  If :permanent is non-nil, mark
    all values (whether currently removed or not) as permanently back 
    in the domain."
  
  ;; mark permanent restorals if needed
  (when permanent
    (let ((saved-domain (slot-value v 'saved-domain-bit-array)))
      ;; if the slot contains nil, then all values are already in the domain
      ;; otherwise make all array entries into 1
      (if saved-domain (fill saved-domain 1))))
  
  ;; restore any currently removed
  (let ((value-count (value-count v))
        (domain (domain-bit-array v)))
    (declare (optimize (speed 3)(safety 1))
             (type fixnum value-count))
    (dotimes (i value-count)
      (if (not (in-domain-p i domain))
        ;; no need to set permanent arg here, it was handled above
        (restore-value-to-domain v i)))))


(defmethod REMOVE-VALUES-WITH-CONFLICTS-FROM-DOMAIN ((v var) &key (permanent nil))
  
  "Remove from domain all values with conflicts.  If :permanent is non-nil
    the values are removed permanently."
  
  (let ((value-count (value-count v))
        (conflicts (conflicts v)))
    (declare (optimize (speed 3)(safety 1))
             (type fixnum value-count))
    (dotimes (i value-count)
      (if (> (aref conflicts i) 0)
        (remove-value-from-domain v i :permanent permanent)))))


;;; (VALUES-IN-DOMAIN-COUNT (V VAR)) is a var slot reader


(defmethod VALUE-IN-DOMAIN-P ((v var) value)
  
  "Return non-nil if value is currently in domain, nil otherwise"
  
  (in-domain-p value (domain-bit-array v)))


(defmethod NO-VALUES-IN-DOMAIN-P ((v var))
  
  "Return t if no values left in domain, nil otherwise"
  
  (= 0 (values-in-domain-count v)))

(defmethod VALUE-IN-DOMAIN-PERMANENTLY-P ((v var) value)
  
  "Return non-nil if value is permanently in domain, nil otherwise"
  
  (in-domain-p value (saved-domain-bit-array v)))


(defmethod NO-VALUES-IN-DOMAIN-PERMANENTLY-P ((v var))
  
  "Return t if no values left in domain after reset-csp, nil otherwise"
  
  (= 0 (count 1 (saved-domain-bit-array v))))


;;; --- Internal domain manipulation functions: not for users ---

(defun IN-DOMAIN-P (value domain-bit-array)

  "Internal function to determine whether value is in domain.  Args:
     value: value number
     domain-bit-array: contents of slot 'domain-bit-array of var (this
      can either be nil, or a simple bit array)
    Returned value is t if value is in domain, nil otherwise.
    No checking is done of domain-bit-array size."

  (declare (optimize (speed 3) (safety 1))
           (type fixnum value)
           (type (simple-array bit (*)) domain-bit-array))
  (if domain-bit-array
    ;; if there's a domain-bit-array, return t if bit value is 1
    (= (sbit domain-bit-array value) 1)
    ;; if no array, then all values are in domain: return t
    t))


;;; --- Preferences ---

(defmethod SET-VALUE-PREFERENCE ((v var) value pref-value)
  "set preference value to specified value.  This will create a prefs array
    if one does not exist."
  (let ((prefs (prefs v)))
    ;; create prefs array if not already done
    (unless prefs
      (setf prefs (make-array (value-count v) :initial-element 0 :element-type 'fixnum))
      (setf (slot-value v 'prefs) prefs))
    ;; set value
    (setf (aref prefs value) pref-value)))


(defmethod SET-VALUE-PREFERENCES-BY-GENERATOR ((v var) pref-generator)
  "set preferences values via a generator function.  Pref-generator
    is a function which takes as args the var and a value number, and
    returns the preference value (numeric).  This will create a prefs
    array if one does not exist."
  (let ((prefs (prefs v))
        (value-count (value-count v)))
    ;; create prefs array if not already done
    (unless prefs
      (setf prefs (make-array value-count :initial-element 0 :element-type 'fixnum))
      (setf (slot-value v 'prefs) prefs))
    ;; set values
    (dotimes (i value-count)
      (setf (aref prefs i)
            (funcall pref-generator v i)))))


(defmethod CLEAR-VALUE-PREFERENCES ((v var))
  
  "Remove all preferences for variable"
  
  ;; if there is already a prefs array, just fill it with 0.
  ;; otherwise no action
  (if (prefs v) (fill (prefs v) 0)))


(defmethod VALUE-PREFERENCE ((v var) value)
  
  "Return value preference.  If not defined, value preferences are
    returned as 0.  This will not create prefs array."
  
  (let ((prefs (prefs v)))
    (cond (prefs (aref prefs value))
          (t 0))))


(defmethod ASSIGNED-VALUE-PREFERENCE ((v var))

  "Return preference of assigned value, if assigned, else
    return nil"

  (if (assigned-value v) (value-preference v (assigned-value v))))


(defmethod SORT-VALUES-BY-PREFERENCE ((v var) value-list)
  
  "Sort list of values by preference.  Non-destructive:
    value-list is not modified"
  
  (sort-values-by-preference-aux
   (prefs v) (copy-list value-list)))


(defun SORT-VALUES-BY-PREFERENCE-AUX (prefs value-list)
  
  "prefs is array of preferences or nil if no prefs defined.
    value-list is a list of values. Return value-list sorted
    by preference with highest preference values first.
    Destructive - may destroy input value-list."
  
  ;; test: (sort-values-by-preference-aux #(1 1 2 3 3 4) '(0 1 2 3 4 5))
  ;; if no prefs return value-list unchanged
  (cond ((null prefs) value-list)
        ;; otherwise sort
        (t (stable-sort value-list
                        #'(lambda (x y)
                            (> (aref prefs x) (aref prefs y)))))))


(defmethod MAX-PREFERENCE-VALUES-IN-VALUE-LIST ((v var) value-list)

  "Return list of those values in value-list which have largest
    preference value.  If preferences are not defined, return
    value-list unchanged."

  (max-preference-values-in-value-list-aux 
   (prefs v) value-list))


(defun MAX-PREFERENCE-VALUES-IN-VALUE-LIST-AUX (prefs value-list)

  "Return list of those values in value-list which have the largest
    preference value.  prefs is the preference array, i.e.
    (aref prefs value) -> preference for value.  Prefs may be nil,
    in which case value-list is returned unchanged."

  ;; test: (max-preference-values-in-value-list-aux #(1 1 2 3 3 4) '(0 3 4))
  ;; returns '(3 4)
  (cond ((null prefs) value-list)
        (t 
         (let ((max-pref 0)
               (result nil))
           (dolist (value value-list)
             (setf max-pref (max max-pref (aref prefs value))))
           (dolist (value value-list)
             (if (= max-pref (aref prefs value))
               (push value result)))
           (nreverse result)))))


(defmethod MAX-PREFERENCE ((v var))
  
  "Return max preference on all values in domain, or 0 if no prefs 
    defined for var"
  
  (if (null (prefs v))
    0
    (max-preference-aux (prefs v)
                        (value-count v)
                        (domain-bit-array v))))


(defun MAX-PREFERENCE-AUX (prefs value-count domain)
  
  "Return max preference value for any value in domain.
    prefs is array of length value-count.
    domain is contents of slot domain-bit-array"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) prefs))
  (let ((result 0))
    (declare (type fixnum result))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (when (in-domain-p i domain)
        (setf result (max result (aref prefs i)))))
    result))


(defmethod MAX-PREFERENCE-ON-MIN-CONFLICT-VALUES ((v var))
  
  "Return max preference on all min-conflict values in domain, or 0 if no prefs 
    defined for var"
  
  (if (null (prefs v))
    0
    (max-preference-on-min-conflict-values-aux (conflicts v)
                                               (prefs v)
                                               (value-count v)
                                               (min-conflicts v)
                                               (domain-bit-array v))))

(defun MAX-PREFERENCE-ON-MIN-CONFLICT-VALUES-AUX 
       (conflicts prefs value-count min-conflicts domain)
  
  "Return max preference on min conflicts values in domain.
    conflicts and prefs are arrays of length value-count.
    min-conflicts is the min number of conflicts on any value.
    domain is contents of slot domain-bit-array."
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts prefs))
  (let ((result 0))
    (declare (type fixnum result))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (when (and (in-domain-p i domain)
                 (= (aref conflicts i) min-conflicts))
        (setf result (max result (aref prefs i)))))
    result))


(defmethod MEAN-PREFERENCE ((v var))
  
  "Return mean preference for all values in domain, or 0 if no prefs 
    defined for var"
  
  (if (null (prefs v))
    0
    (mean-preference-aux (prefs v)
                         (value-count v)
                         (domain-bit-array v))))


(defun MEAN-PREFERENCE-AUX (prefs value-count domain)
  
  "Return mean preference value for all values in domain.
    conflicts and prefs are arrays of length value-count.
    domain is contents of slot domain-bit-array.
    If there are no values in domain, 0 is returned"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count)
           (type (simple-array fixnum (*)) prefs))
  (let ((result 0)
        (in-domain-count 0))
    (declare (type fixnum result in-domain-count))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (when (in-domain-p i domain)
        (incf result (aref prefs i))
        (incf in-domain-count)))
    (if (= 0 in-domain-count)
      0
      (/ result in-domain-count))))


(defmethod MEAN-PREFERENCE-ON-MIN-CONFLICT-VALUES ((v var))
  
  "Return mean preference for all min-conflict values in domain, 
    or 0 if no prefs defined for var"
  
  (if (null (prefs v))
    0
    (mean-preference-on-min-conflict-values-aux (conflicts v)
                                                (prefs v)
                                                (value-count v)
                                                (min-conflicts v)
                                                (domain-bit-array v))))


(defun MEAN-PREFERENCE-ON-MIN-CONFLICT-VALUES-AUX 
       (conflicts prefs value-count min-conflicts domain)
  
  "Return mean preference value for all min conflict values in domain.
    conflicts and prefs are arrays of length value-count.
    min-conflicts is the min number of conflicts on any value.
    domain is contents of slot domain-bit-array.
    If there are no values in domain, 0 is returned"
  
  (declare (optimize (speed 3) (safety 1))
           (type fixnum value-count min-conflicts)
           (type (simple-array fixnum (*)) conflicts prefs))
  (let ((result 0)
        (count 0))
    (declare (type fixnum result count))
    (dotimes (i value-count)
      (declare (type fixnum i))
      (when (and (in-domain-p i domain)
                 (= (aref conflicts i) min-conflicts))
        (incf result (aref prefs i))
        (incf count)))
    (if (= 0 count)
      0
      (/ result count))))


(defmethod PREFERENCE-ON-FIRST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS ((v var))
  "return preference value of first value by lexical order with number of conflicts
    equal to the current min conflicts"
  (value-preference v (first-value-lexical-order-with-min-conflicts v)))

(defmethod PREFERENCE-ON-LAST-VALUE-LEXICAL-ORDER-WITH-MIN-CONFLICTS ((v var))
  "return preference value of last value by lexical order with number of conflicts
    equal to the current min conflicts"
  (value-preference v (last-value-lexical-order-with-min-conflicts v)))

(defmethod PREFERENCE-ON-FIRST-VALUE-LEXICAL-ORDER-WITH-NO-CONFLICTS ((v var))
  "return preference value of first value by lexical order with no conflicts"
  (value-preference v (first-value-lexical-order-with-no-conflicts v)))

(defmethod PREFERENCE-ON-LAST-VALUE-LEXICAL-ORDER-WITH-NO-CONFLICTS ((v var))
  "return preference value of last value by lexical order with no conflicts"
  (value-preference v (last-value-lexical-order-with-no-conflicts v)))


(defmethod SORT-VALUE-LIST-MIN-CONFLICTS-MAX-PREFERENCE ((v var) value-list)

  "Sort value list first by min-conflicts, then by max preferences.  If
    preferences are undefined, sort by min-conflicts only"

  (sort-value-list-min-conflicts-max-preference-aux
   (conflicts v) (prefs v) value-list))


(defun SORT-VALUE-LIST-MIN-CONFLICTS-MAX-PREFERENCE-AUX 
       (conflicts prefs value-list)

  "Sort value-list based on first min-conflicts, then max-preference.
   conflicts and prefs are arrays of length value-count s.t.
   (aref conflicts value) -> conflict count, and 
   (aref prefs value) -> preference.
   Destructive - value list may be destroyed"

  ;; test: (sort-value-list-min-conflicts-max-preference-aux #(2 1 0 0 1) #(0 2 4 5 6) '(0 1 2 3 4))
  ;; returns '(3 2 4 1 0)
  ;; test: (sort-value-list-min-conflicts-max-preference-aux #(2 1 0 0 1) nil '(0 1 2 3 4))
  ;; returns '(2 3 1 4 0)
  (cond ((null prefs)
         ;; no prefs: sort by conflicts only
         (stable-sort value-list
                      #'(lambda (x y)
                          (cond ((< (aref conflicts x) (aref conflicts y)) t)
                                (t nil)))))
        (t
         ;; sort by both conflicts and preferences
         (stable-sort value-list
                      #'(lambda (x y)
                          (cond ((< (aref conflicts x) (aref conflicts y)) t)
                                ((> (aref conflicts x) (aref conflicts y)) nil)
                                (t (cond ((> (aref prefs x) (aref prefs y)) t)
                                         (t nil)))))))))


(defmethod REMOVE-LOW-PREFERENCE-VALUES-FROM-DOMAIN 
  ((v var) 
   &key
   (min-preference-in-domain nil)
   (max-difference-from-max-preference nil)
   (permanent nil))
  
  "Remove low preference values from domain.
   If min-preference-in-domain is non-nil, remove from domain all values
    with preference values < min-preference-in-domain.
   If max-difference-from-max-preference is non-nil, remove from domain
    all values which differ from the max preference value by more than
    max-difference-from-max-preference.
   Both may be specified and act as above.
   If no preferences are defined, no action is taken.  If both 
    min-preference-in-domain and max-difference-from-max-preference
    are nil, no action is taken.
   If :permanent is non-nil, the removal is marked as permanent (i.e.
    preserved after a reset-csp."
  
  (let ((prefs (prefs v))
        (value-count (value-count v)))
    (cond 
     
     ((null prefs)) ;do nothing- no preferences defined
     
     (t 
      
      ;; remove based on absolute value
      (when min-preference-in-domain
        (dotimes (i value-count)
          (when (< (aref prefs i) min-preference-in-domain)
            (remove-value-from-domain v i :permanent permanent))))
      
      ;; remove based on difference from max
      (when max-difference-from-max-preference
        (let ((max-pref 0))
          (dotimes (i value-count)
            (setf max-pref (max max-pref (aref prefs i))))
          (dotimes (i value-count)
            (when (> (- max-pref (aref prefs i))
                     max-difference-from-max-preference)
              (remove-value-from-domain v i :permanent permanent)))))
      ))))


;;; Note: the following methods sort and order first on min-conflicts,
;;; then on preference.  I.e. preference is used as a tie breaker when
;;; conflict counts are the same.
  
(defmethod ALL-MIN-CONFLICTS-VALUES-ORDERED-BY-PREFERENCE ((v var))

  "Return list of all value numbers in domain with min conflicts, ordered by
    preference"

  (let ((prefs (prefs v))
        (value-list (all-min-conflicts-values-aux
                     (conflicts v)
                     (value-count v)
                     (min-conflicts v)
                     (domain-bit-array v))))
    (cond ((null prefs) value-list)
          (t (sort-values-by-preference-aux prefs value-list)))))


(defmethod A-RANDOM-VALUE-WITH-MIN-CONFLICTS-AND-MAX-PREFERENCE ((v var))

  "Return a value with min-conflicts and, of those value with min conflicts,
    has a max preference (select at random if more than one)"

  (let ((prefs (prefs v))
        (value-list (all-min-conflicts-values v)))
    (cond ((null prefs)
           (a-random-element-from-list value-list))
          (t
           (a-random-element-from-list
            (max-preference-values-in-value-list-aux prefs value-list))))))


(defmethod N-RANDOM-VALUES-WITH-MIN-CONFLICTS-AND-MAX-PREFERENCE ((v var) n)

  "Rreturn one value with min-conflicts, selected at random"

  (let ((prefs (prefs v))
        (value-list (all-min-conflicts-values v)))
    (cond ((null prefs)
           (N-random-elements-from-list value-list n))
          (t
           (N-random-elements-from-list
            (max-preference-values-in-value-list-aux prefs value-list)
            n)))))


(defmethod ALL-VALUES-ORDERED-BY-MIN-CONFLICTS-AND-MAX-PREFERENCE ((v var))

  "Sorted list of value numbers in domain, min-conflicts first.  Ties
    in min-conflicts are broken by max preference, if available."

  (let ((prefs (prefs v)))
    (cond ((null prefs) (all-values-ordered-by-min-conflicts v))
          ;; get lexical order list and sort.  This ensure that
          ;; values excluded from domain don't appear in result.
          (t (let ((value-list (all-values-lexical-order v)))
               (sort-value-list-min-conflicts-max-preference-aux
                (conflicts v) prefs value-list))))))


;;; no preference method analog for the following:
;;; ALL-VALUES-MIN-CONFLICTS-FIRST


(defmethod ALL-VALUES-PREFERENCE-ORDER ((v var))
  
  "Return list of all values in domain, sorted by preference (highest first)"
  
  (let ((unsorted-values
         (all-values-lexical-order-aux (value-count v) (domain-bit-array v)))
        (prefs (prefs v)))
    (cond (prefs
           (sort-values-by-preference-aux prefs unsorted-values))
          (t unsorted-values))))


(defmethod ASSIGN-MIN-CONFLICTS-MAX-PREFERENCE-VALUE-AT-RANDOM ((v var))

  "Assign a min-conflicts/max preference value at random.  
    If there are no choices, the var is made unassigned."

  (assign-value v (a-random-value-with-min-conflicts-and-max-preference v)))


;;; --- var priority ---

;;; (PRIORITY (V VAR)) is a slot accessor

(defmethod RESTORE-DEFAULT-PRIORITY ((v var))

  "restore var priority to default value"

  (setf (priority v) *default-var-priority*))


;;; -----------------------------------------------------------------------
;;; CSP DEVELOPMENT METHODS
;;; 
;;; These are primarily intended for development/debugging.
;;; -----------------------------------------------------------------------

(defmethod SHOW-CSP-STATE ((c csp) &key (preferences nil))
  "print CSP state description to standard output.  Useful for 
    debugging.  If preferences is non-nil then include preference data
    also."
  (with-slots 
    (assigned-var-count
     conflicting-var-count
     all-vars
     vars-named-p) c
    (format t "~%~d assignments, ~d with conflicts, max-conflicts: ~a"
            assigned-var-count conflicting-var-count
            (max-conflicts c))
    (if (ignored-var-list c)
      (format t "~%~d var(s) ignored: ~a" (length (ignored-var-list c))
              (if vars-named-p
                (mapcar #'name (ignored-var-list c))
                (mapcar #'var-number (ignored-var-list c)))))
    (if (locked-var-list c)
      (format t "~%~d var(s) locked: ~a" (length (locked-var-list c))
              (if vars-named-p
                (mapcar #'name (locked-var-list c))
                (mapcar #'var-number (locked-var-list c)))))
    (format t "~2%  Variable    Assigned       Conflicts")
    (format t "~%              Val [#conf] ")
    ;; note: print in var-list order, not reverse
    (dolist (v all-vars)
      (cond ((ignored-permanently-p v) (format t "~%I "))
            ((ignored-p v) (format t "~%i "))
            ((locked-p v) (format t "~%L "))
            (t (format t "~%  ")))
      (if vars-named-p
        (format t "~10a:" (var-name v))
        (format t "~4d:" (var-number v)))
      (format t " ~3a [~3d]"
              (assigned-value v)
              (conflicts-on-assigned-value v))
      (let ((conf (slot-value v 'conflicts))
            (domain (slot-value v 'domain-bit-array))
            (saved-domain (slot-value v 'saved-domain-bit-array)))
        (dotimes (i (value-count v))
          (format t "~3d" (aref conf i))
          (cond ((and (not (in-domain-p i domain))
                      (not (in-domain-p i saved-domain)))
                 (format t "X")) ;permanently and currently excluded
                ((and (in-domain-p i domain)
                      (not (in-domain-p i saved-domain)))
                 (format t "t")) ;temporarily in domain
                ((and (not (in-domain-p i domain))
                      (in-domain-p i saved-domain))
                 (format t "x")) ;temporarily excluded from domain
                (t (format t " "))))
          (when preferences
            (format t "~%  ~a" (if vars-named-p "           " "     "))
            (format t "    pref: ")
            (dotimes (i (value-count v))
              (format t "~3d " (value-preference v i))))

          ))))


(defmethod SUMMARIZE-CSP-STATE ((c csp))
 "print a summary of CSP state description to standard output"
  (with-slots (var-count
		assigned-var-count 
		conflicting-var-count) c
    (format t "~%~a variables, ~a assigned values, ~a conflicting (~a max conflict)"
	    var-count assigned-var-count conflicting-var-count (max-conflicts c))))

(defmethod CHECK-CSP-STATE ((c csp))
  "Look for consistency in current state and describe any inconsistencies
    to standard output.  Debugging tool."
  (with-slots (var-list
               assigned-var-count
               conflicting-var-count) c
    (let ((check-assignment-count 0)
          (check-max-conflicts nil)
          (check-conflicting-var-count 0))
      (dolist (v var-list)
        (when (assigned-value v)
          (incf check-assignment-count)
          (if (assigned-value-has-conflicts-p v)
            (incf check-conflicting-var-count))
          (if check-max-conflicts
            (setf check-max-conflicts
                  (max check-max-conflicts
                       (conflicts-on-assigned-value v)))
            (setf check-max-conflicts
                  (conflicts-on-assigned-value v)))))
      (format t "~%-- check: ")
      (if (/= assigned-var-count check-assignment-count)
        (format t " *ASSIGNMENT COUNT ERROR*"))
      (if (/= conflicting-var-count check-conflicting-var-count)
        (format t " *CONFLICTING VARS ERROR* (~a ~a)"
                conflicting-var-count check-conflicting-var-count))
      ;; go through accessor for max conflicts
      (if (not (equal (max-conflicts c) check-max-conflicts))
        (format t " *MAX CONFLICTS ERROR* (~a ~a)"
                (max-conflicts c) check-max-conflicts)))))


;;; -----------------------------------------------------------------------
;;; MACROS
;;; 
;;; These are for iteration over vars in a CSP instance.
;;; -----------------------------------------------------------------------


(defmacro DO-FOR-ALL-UNIGNORED-VARS (&key var csp (predicate t) form)
  "iterate over all unignored (and unlocked) vars in a CSP"
  `(dolist (,var (var-list ,csp))
     (when ,(if (eq t predicate) t predicate)
       ,form)))


(defmacro DO-FOR-ALL-IGNORED-VARS (&key var csp (predicate t) form)
  "iterate over all ignored vars in a CSP"
  `(dolist (,var (ignored-var-list ,csp))
     (when ,(if (eq t predicate) t predicate)
       ,form)))


(defmacro DO-FOR-ALL-IGNORED-AND-UNIGNORED-VARS 
          (&key var csp (predicate t) form)
  "iterate over all ignored and unignored vars in a CSP (exclude locked vars)"
  `(progn 
     (dolist (,var (var-list ,csp))
       (when ,(if (eq t predicate) t predicate)
         ,form))
     (dolist (,var (ignored-var-list ,csp))
       (when ,(if (eq t predicate) t predicate)
         ,form))))

(defmacro DO-FOR-ALL-LOCKED-VARS (&key var csp (predicate t) form)
  "iterate over all locked vars in a CSP"
  `(dolist (,var (locked-var-list ,csp))
     (when ,(if (eq t predicate) t predicate)
       ,form)))

(defmacro DO-FOR-ALL-VARS 
          (&key var csp (predicate t) form)
  
  "iterate over all vars (ignored, unignored, and locked) in a CSP"
  
  `(dolist (,var (all-vars ,csp))
     (when ,(if (eq t predicate) t predicate)
       ,form)))

;;; Examples:
;;; (do-for-all-unignored-vars :var v :csp *C* :form (print v))
;;; (do-for-all-unignored-vars :var v :csp *C* :form (print v) 
;;; 			   :predicate (= (var-number v) 0))")


;;; -----------------------------------------------------------------------
;;; SYSTEM-LEVEL CSP METHODS
;;; 
;;; These should never be called by users directly, but only through var 
;;; or CSP methods.  They manage the CSP state description.
;;; -----------------------------------------------------------------------

;;; --- ignored vars ---

(defmethod INFORM-CSP-VAR-IGNORED ((c csp) (v var))
  
  "Update state to reflect v is now an ignored var.  Vars being ignored
    should not have assignments (the caller should ensure this), but if
    it does have one then a continuable error will be signalled."
  
  ;; make sure var is not already in the ignored list...
  (when (member v (var-list c))
    
    ;; ignored vars shouldn't have assignments, so this should
    ;; never be executed
    (when (has-assigned-value-p v)
      (cerror "value will be unassigned"
              "attempting to ignore variable ~a with an assignment ~a"
              v (assigned-value v))
      (unassign-value v))

    ;; update var-list and ignored-var-list
    (let ((sort-vars (sort-vars c))
          (old-list (ignored-var-list c)))
      (setf (slot-value c 'ignored-var-list)
            (if sort-vars
              (merge 'list old-list (list v) sort-vars)
              (cons v old-list))))
    (setf (slot-value c 'var-list) (delete v (var-list c)))
    
    (setf (slot-value c 'var-count) (- (var-count c) 1))
    ))


(defmethod INFORM-CSP-VAR-UNIGNORED ((c csp) (v var))
  
  "Update state to reflect var v is not ignored.  Vars being unignored
    should not have assignments (the caller should ensure this), but if
    it does have one then a continuable error will be signalled."
  
  ;; ignored vars shouldn't have assignments, so this should
  ;; never be executed
  (when (has-assigned-value-p v)
    (cerror "value will be unassigned"
            "attempting to unignore variable ~a with an assignment ~a"
            v (assigned-value v))
    (unassign-value v))
  
  ;; make sure var is already in the ignored list...
  (when (member v (ignored-var-list c))
    
    (let ((sort-vars (sort-vars c))
          (old-list (var-list c)))
      (setf (slot-value c 'var-list)
            (if sort-vars
              (merge 'list old-list (list v) sort-vars)
              (cons v old-list))))
    (setf (slot-value c 'ignored-var-list)
          (delete v (ignored-var-list c)))
    
    (setf (slot-value c 'var-count) (+ (var-count c) 1))
    
    ))


;;; --- locked vars ---

(defmethod INFORM-CSP-VAR-LOCKED ((c csp) (v var) value)
  
  "Update state to reflect v is now locked with specified value"
  
  (when (member v (var-list c))
    ;; this is only executed if var is neither ignored nor locked
    
    ;; update var-list and locked-var-list
    (let ((sort-vars (sort-vars c))
          (old-list (locked-var-list c)))
      (setf (slot-value c 'locked-var-list)
            (if sort-vars
              (merge 'list old-list (list v) sort-vars)
              (cons v old-list))))
    (setf (slot-value c 'var-list) (delete v (var-list c)))
    
    ;; record the locked var's value in the locked-var-table
    (setf (gethash v (locked-var-table c)) value)
    (setf (slot-value c 'var-count) (- (var-count c) 1))
    
    ;; note that locked vars have assignments, so fix assignment count
    ;; and other state slots
    (when (has-assigned-value-p v) 
      (update-assignment-count c -1)
      (when (assigned-value-has-conflicts-p v)
        (update-max-conflicts c (conflicts-on-assigned-value v) nil)
        (update-conflicting-var-count c -1)))
      
    ))


(defmethod INFORM-CSP-VAR-UNLOCKED ((c csp) (v var))

  "Update state to reflect var v is not locked"

  (when (member v (locked-var-list c))
    ;; this is only executed if var is locked

    ;; update var-list and locked-var-list
    (let ((sort-vars (sort-vars c))
          (old-list (var-list c)))
      (setf (slot-value c 'var-list)
            (if sort-vars
              (merge 'list old-list (list v) sort-vars)
              (cons v old-list))))
    (setf (slot-value c 'locked-var-list) (delete v (locked-var-list c)))

    (setf (slot-value c 'var-count) (+ (var-count c) 1))

    ;; remove value from locked var table
    (remhash v (locked-var-table c))

    ;; if the var has an assigned value then update assignment count
    (when (has-assigned-value-p v) 
      (update-assignment-count c +1)
      (when (assigned-value-has-conflicts-p v)
        (update-max-conflicts c nil (conflicts-on-assigned-value v))
        (update-conflicting-var-count c +1)))
    ))


;;; methods invoked by vars when assignments or conflicts on
;;; assignments change:

(defmethod UPDATE-ASSIGNMENT-COUNT ((c csp) delta)
  
  "Update the assignment-count slot by the specified delta, which may
    be positive or negative"
  
  (setf (slot-value c 'assigned-var-count)
        (+ (assigned-var-count c) delta)))


(defmethod UPDATE-MAX-CONFLICTS ((c csp) old-count new-count)

  "Update the max-conflicts slot given that the number of conflicts
    on some assignment has changed from old-count to new-count.
    old-count can be nil, i.e. the var had no previous assignment, and
    new-count can be nil, i.e. the var is becoming unassigned"
  
  ;; don't trigger calculation here
  (let ((max-conflicts (slot-value c 'max-conflicts)))
    
    ;; do nothing unless there is a max-conflicts value
    (when max-conflicts
      
      (cond 
       
       ((and (null old-count) new-count)
        ;; newly assigned variable: if new count greater than 
        ;; previous max, then reset max
        (if (> new-count max-conflicts)
          (setf (slot-value c 'max-conflicts) new-count)))
       
       ((and old-count (null new-count))
        ;; newly unassigned variable:  if old-count was max, then
        ;; reset to recompute
        (if (= old-count max-conflicts)
          (setf (slot-value c 'max-conflicts) nil)))
       
       (t ;; assignment is changing
        (cond ((= old-count new-count)
               ;; no change in conflict count, do nothing
               )
              ((> new-count max-conflicts)
               ;; set new max
               (setf (slot-value c 'max-conflicts) new-count))
              ((and (= old-count max-conflicts)
                    (< new-count max-conflicts))
               ;; old value was max, new value less than max:
               ;; set to recompute
               (setf (slot-value c 'max-conflicts) nil))))))))


(defmethod UPDATE-CONFLICTING-VAR-COUNT ((c csp) delta)

  "Update the conflicting-var-count slot by the specified delta, which may
    be positive or negative"

  (setf (slot-value c 'conflicting-var-count)
        (+ (conflicting-var-count c) delta)))


(defmethod MAX-CONFLICTS :BEFORE ((c csp))

  "Update max-conflicts slot if necessary: :before method on 
    max-conflicts accessor."
  
  (let ((max-conflicts (slot-value c 'max-conflicts))
        (assigned-var-count (assigned-var-count c)))
    
    (cond ((= 0 assigned-var-count)
           (setf (slot-value c 'max-conflicts) nil))
          
          ((null max-conflicts)
           ;; value unknown: compute
           (let ((max-conf nil)
                 (conf nil))
             (dolist (v (var-list c))
               (setf conf (conflicts-on-assigned-value v))
               (if conf
                 (if max-conf
                   (setf max-conf (max max-conf conf))
                   (setf max-conf conf))))
             ;; store value in slot
             (setf (slot-value c 'max-conflicts) max-conf))))))


;;; --- Conflict Cache ---
;;; 
;;; Caveat:  the cache is only useful for constraints which associate a single
;;; assignment with one or more var/value conflicts.  Thus for binary (1-1) or
;;; 1-N constraints it is useful.  For constraints where N vars cause conflicts,
;;; the cache cannot be used.  Thus resource constraints cannot be cached.
;;; This limits the usefulness of the WHY-CONFLICTS and 
;;; UNDO-CONFLICTING-ASSIGNMENTS methods, unless they make use of more than the
;;; cached data.
;;; 
;;; Structure of conflict cache:  hash table, key is 
;;;  (var . value) where var is assigned to value
;;; value is list of lists of
;;;  (cause affected-var val-spec1 val-spec2 ...)
;;; where val-spec is:
;;;  a single fixnum specifying a value, weight 1
;;;  a cons (val1 . val2) specifying an inclusive range of values, weight 1
;;;  of the form ((val1 . val2) . weight) specifying an inclusive range
;;;   of values with specified weight
;;; if cause and affected-var are the same as the current head of the list,
;;; then the val-specs are updated; otherwise a new record is made and consed
;;; to the head of the list.  There is potentially some redundancy in the 
;;; causes and affected-var entries (though not in val-specs, or else conflict
;;; counts would become inconsistent).


(defun MAKE-VALSPEC (value first-value last-value weight)

  "Make a valspec for use in the conflict cache.  Inputs are:
   For single value:
    value, optional weight (if nil defaults to 1)
   For a value range:
    first-value, last-value, optional weight (defaults to 1)
   If both value and first-value/last-value specified, an error
   is signalled."

  ;; (make-valspec 23 nil nil nil) => 23
  ;; (make-valspec 23 nil nil 1) => 23
  ;; (make-valspec 23 nil nil 2) => ((23 . 23) . 2)
  ;; (make-valspec nil 1 5 nil) => (1 . 5)
  ;; (make-valspec nil 1 5 1) => (1 . 5)
  ;; (make-valspec nil 1 5 2) => ((1 . 5) . 2)
  ;; (make-valspec 23 1 5 2) => error
  (cond ((and value (null first-value) (null last-value))
         (if (and weight (/= weight 1))
           (cons (cons value value) weight) value))
        ((and (null value) first-value last-value)
         (if (and weight (/= weight 1))
           (cons (cons first-value last-value) weight)
           (cons first-value last-value)))
        (t
         (error "invalid valspec parameters: ~a ~a ~a ~a"
                value first-value last-value weight))))


(defun DECODE-VALSPEC (valspec)

  "input is valspec as encoded by make-valspec.  Multiple values returned:
    first-value, last-value, weight"

  ;; (decode-valspec (make-valspec 23 nil nil nil)) => 23 23 1
  ;; (decode-valspec (make-valspec 23 nil nil 1)) => 23 23 1
  ;; (decode-valspec (make-valspec 23 nil nil 2)) => 23 23 2
  ;; (decode-valspec (make-valspec nil 1 5 nil)) => 1 5 1
  ;; (decode-valspec (make-valspec nil 1 5 1)) => 1 5 1
  ;; (decode-valspec (make-valspec nil 1 5 2)) => 1 5 2

  (cond 
   ((fixnump valspec)
    ;; one value, weight 1
    (values valspec valspec 1))
   
   ((and (consp valspec) (not (consp (first valspec))))
    ;; simple value range, weight 1
    (values (first valspec) (rest valspec) 1))
   
   ((and (consp valspec) (consp (first valspec)))
    ;; value range with weight
    (values (first (first valspec)) (rest (first valspec)) (rest valspec)))
   
   (t (error "can't decode valspec ~a" valspec))))


(defmethod ADD-TO-CONFLICT-CACHE ((c CSP) 
                                  cause-var 
                                  cause-val 
                                  cause
                                  affected-var 
                                  &key
                                  (value nil)
                                  (first-value nil)
                                  (last-value nil)
                                  (weight 1))
  
  "Record a conflict in the conflict cache.  
    cause-var is a var with value being assigned to cause-val
    It affects affected-var via cause, which can be a
    constraint instance or an be a symbol naming a reason, or nil.
    Constrained values are specified in one of the following forms:
     :value - single conflicting value
    or
     :first-value, :last-value - a pair of values, s.t. all values between
       first-value and last-value inclusive are constrained
    weight defaults to 1, or may be specified as any positive fixnum.

   Note:  this does NOT change the conflict counts on any vars, it simply
   records them in the cache in a canonical format.  
   See ADD-TO-CONFLICTS-AND-CACHE."

  ;; error check
  (cond ((and value (null first-value) (null last-value)))
        ((and (null value) first-value last-value))
        (t (error 
            "either value or both of first-value last-value must be specified")))
  (unless (and (fixnump weight) (> weight 0))
    (error "weight must be positive fixnum"))

  (let* ((cache (conflict-cache c))
         (key (cons cause-var cause-val))
         (rec (gethash key cache))
         (prev-cause (first (first rec)))
         (prev-affected-var (second (first rec)))
         (new-valspec 
          (make-valspec value first-value last-value weight)))

    (cond ((and (eq prev-cause cause)
                (eq prev-affected-var affected-var))
           ;; just add valspec (destructive mod of rec)
           (setf (cddr (first rec)) (cons new-valspec (cddr (first rec)))))
          (t ; make new entry
           (setf (gethash key cache)
                 (cons
                  (list cause affected-var new-valspec)
                  rec))))
           
    ))


(defmethod CONFLICT-ALREADY-CACHED-P ((c CSP) 
                                      cause-var 
                                      cause-val
                                      &optional
                                      cause
                                      affected-var)

  "Return cache record if conflicts due to assigning cause-var to cause-val
    are recorded.
    If cause is non-nil, then a matching cause is required.
    If a cause is specified, than an affected-var can also be specified.
    If affected-var is nil, then any record with
    matching cause is returned.  If affected-var is a var instance, then
    a match on cause and affected-var is required."

  (let* ((cache (conflict-cache c))
         (key (cons cause-var cause-val))
         (rec (gethash key cache)))

    (cond (cause
           ;; there is a cache record:  see and cause and/or  affected-var occur:
           (dolist (conf-rec rec)
             (if affected-var
               ;; affected-var specified:  need match on cause and affected-var
               (if (and (eq (first conf-rec) cause)
                        (eq (second conf-rec) affected-var))
                 (return conf-rec))
               ;; just need match on cause
               (if (eq (first conf-rec) cause)
                 (return conf-rec)))))
          ;; no cause required:  return rec
          (t rec))))


;;; Next two methods are for use by CSP-VAR-VALUE-ASSIGN and
;;; CSP-VAR-VALUE-UNASSIGN, for maintaining the cache and using it if filled.

(defmethod ADD-TO-CONFLICTS-AND-CACHE ((c CSP) 
                                       cause-var 
                                       cause-val 
                                       cause
                                       affected-var 
                                       &key
                                       (value nil)
                                       (first-value nil)
                                       (last-value nil)
                                       (weight 1))
  "Add to conflicts and record in cache.  This is unconditional:  if 
    the conflicts are already recorded they will be recorded again,
    and an error will result when the cache contents are subtracted.
    Only add once.  Use CONFLICT-ALREADY-CACHED-P to tell if conflicts
    are already stored in the cache.
    Arguments:
    cause-var is a var with value being assigned to cause-val
    It affects affected-var via cause, which can be a
    constraint instance or an be a symbol naming a reason, or nil.
    Constrained values are specified in one of the following forms:
     :value - single conflicting value
    or
     :first-value, :last-value - a pair of values, s.t. all values between
       first-value and last-value inclusive are constrained
    weight defaults to 1, or may be specified as any positive fixnum."
  
  (cond (value
         (add-to-conflicts-on-value affected-var value weight))
        ((and first-value last-value)
         (loop-over-value-range
          val first-value last-value
          (add-to-conflicts-on-value affected-var val weight)))
        (t (error "invalid conflict specification")))
  (add-to-conflict-cache c cause-var cause-val cause
                         affected-var :value value
                         :first-value first-value
                         :last-value last-value
                         :weight weight))


(defmethod SUBTRACT-CACHED-CONFLICTS ((c CSP)
                                      cause-var
                                      cause-value
                                      &key
                                      (remove-from-cache t))
  "cause-var is being unassigned from cause-value.  Subtract conflicts
    as specified in the cache.  If :remove-from-cache is non-nil,
    the cached conflicts are removed; otherwise no change is made in the cache.
    This method returns the cached conflicts record if there is one,
    else nil.  Caller can use this to tell whether conflicts were in fact
    in the cache (i.e. as a consistency check).
    Note:  no check performed here that value is actually assigned: 
    or is being unassigned.  Responsibility of caller!"

  (let* ((cache (conflict-cache c))
         (key (cons cause-var cause-value))
         (rec (gethash key cache)))

    (cond ((null rec) ;; do nothing
           )
          (t ;; go through conflicts and subtract
           (dolist (conf-rec rec)
	     ;; first element of conf-rec is cause: not needed here
             (let ((affected-var (second conf-rec))
                   (valspecs (cddr conf-rec)))
               (dolist (valspec valspecs)
                 (multiple-value-bind (first-value last-value weight)
                                      (decode-valspec valspec)
                   (loop-over-value-range
                    val first-value last-value
                    (subtract-from-conflicts-on-value
                     affected-var val weight))))))))

    ;; remove from cache if desired
    (if remove-from-cache
      (remhash key cache))

    ;; returned value is rec from cache
    rec
    ))

#| testing junk - remove...
(reset-csp *C*)
(conflict-cache *C*)
(clear-conflict-cache *C*)
(show-conflict-cache *c*)
;; low level
(add-to-conflict-cache *c* *u* 0 :friday *v* :value 0)
(add-to-conflict-cache *c* *u* 0 :friday *v* :first-value 1 :last-value 2)
(add-to-conflict-cache *c* *u* 0 :thursday *v* :first-value 0 :last-value 2)
(add-to-conflict-cache *c* *v* 1 :thursday *u* :first-value 0 :last-value 2)
;; high level
(add-to-conflicts-and-cache *c* *u* 0 :friday *v* :value 0)
(add-to-conflicts-and-cache *c* *u* 0 :thursday *v* :value 1 :weight 2)
(add-to-conflicts-and-cache *c* *u* 0 :friday *v* 
                            :first-value 1 :last-value 2 :weight 1)
(show-csp-state *c*)
(conflict-already-cached-p *c* *u* 0 :friday *v*)
(conflict-already-cached-p *c* *u* 0 :friday)
(conflict-already-cached-p *c* *u* 0 :thurs *v*)
(conflict-already-cached-p *c* *u* 0)
(conflict-already-cached-p *c* *u* 1)
(subtract-cached-conflicts *c* *u* 0 :remove-from-cache t)
(subtract-cached-conflicts *c* *u* 0 :remove-from-cache nil)
(assign-value *u* 0)
(why-conflicts *c* *v* 0)
(unassign-conflicting-vars *v* 0)
|#

;;; -----------------------------------------------------------------------
;;; USER-CALLABLE CSP METHODS
;;; -----------------------------------------------------------------------
;;; 
;;; (VAR-COUNT (C CSP)) is a slot accessor
;;;   
;;; (MAX-CONFLICTS (C CSP)) is a slot reader - no special method needed
;;;   --but note before method above to compute if not known
;;;   
;;; (ASSIGNED-VAR-COUNT (C CSP)) is a slot reader
;;;   
;;; (CONFLICTING-VAR-COUNT (C CSP)) is a slot reader
;;;   
;;; (VAR-LIST (C CSP)) is a slot reader returning unignored & unlocked vars
;;;   
;;; (IGNORED-VAR-LIST (C CSP))) is a slot reader
;;;
;;; (LOCKED-VAR-LIST (C CSP)) is a slot reader
;;;   
;;; (VARS-NAMED-P (C CSP)) is a slot accessor
;;;   



(defmethod UNASSIGNED-VAR-COUNT ((c CSP))

  "number of (unignored) vars with NO assignment"

  (- (var-count c) (assigned-var-count c)))


(defmethod IGNORED-VAR-COUNT ((c CSP))

  "Return number of ignored vars"

  (length (ignored-var-list c)))


(defmethod LOCKED-VAR-COUNT ((c CSP))

  "Return number of locked vars"

  (length (locked-var-list c)))


(defmethod VAR ((c CSP) i)

  "return the ith var in the CSP"

  (gethash i (var-number-table c)))


(defmethod VAR-NAMED ((c CSP) name)

  "return var with specified name.  If vars-named-p is nil, or it the
    name is not found, return nil."

  (cond ((null (vars-named-p c)) nil) ; no names
        ((gethash name (var-name-table c))) ;name from table
        (t nil)))


(defmethod IN-SOLUTION-STATE-P ((c CSP))

  "return t if all unignored vars assigned and no conflicts"

  (and (= (var-count c) (assigned-var-count c))
       (= 0 (conflicting-var-count c))))
  

(defmethod ALL-VARS-ASSIGNED-P ((c CSP))

  "return t if all vars assigned, nil otherwise"

  (= (var-count c) (assigned-var-count c)))


(defmethod UNASSIGN-ALL-VARS ((c csp))

  "Undo all assignments, i.e. unassign-value for all vars"

  (dolist (v (var-list c))
    (unassign-value v)))


(defmethod IGNORE-ALL-VARS-WITH-EMPTY-DOMAINS ((c csp))

  "Make ignored all vars which have no domain values.  Return number
    of vars ignored."

  (let ((count 0))
    ;; copy the list because it can be destructively modified by ignoring vars
    (dolist (v (copy-list (var-list c)))
      (when (= 0 (values-in-domain-count v))
        (ignore-var v)
        (incf count)))
    count))


(defmethod UNIGNORE-ALL-VARS ((c csp))
  
  "Make unignored all vars which are currently ignored"
  
  ;; need copylist since making unignored destructively modifies
  ;; ignored-var-list slot contents
  (dolist (ignored-var (copy-list (ignored-var-list c)))
    (unignore-var ignored-var)))


(defmethod UNLOCK-ALL-VARS ((c csp))
  
  "Make unlocked all vars which are currently locked"
  
  ;; need copylist since making unignored destructively modifies
  ;; locked-var-list slot contents
  (dolist (locked-var (copy-list (locked-var-list c)))
    (unlock-var locked-var)))


(defmethod RESET-CSP ((c csp))
  
  "Reset CSP to all vars unassigned.  Make all vars unignored (except
    those marked as ignored-permanently-p) and restore all domain 
    values (except those marked as exclude permanently)
    Reinitialize vars and history. Clear assignment cache.
    Assign and lock any previously locked vars (i.e. locks are 
    preserved across reset-csp)"
  
  (let ((previously-locked-vars (locked-var-list c)))
    
    ;; start with a fresh copy of all-vars list if there are any 
    ;; locked or ignored vars (otherwise the current value is OK)
    (when (or (locked-var-list c) (ignored-var-list c))
      (setf (slot-value c 'var-list) (copy-list (all-vars c)))
      (setf (slot-value c 'ignored-var-list) nil)
      (setf (slot-value c 'locked-var-list) nil))

    ;; reset count
    (setf (slot-value c 'var-count) (length (var-list c)))

    ;; re-initialize all vars
    (dolist (v (var-list c)) (re-initialize-variable v))

    ;; clear assignment cache
    (when (cache-assigned-conflicts-p c)
      (clrhash (conflict-cache c)))

    ;; init bookkeeping
    (setf (slot-value c 'assigned-var-count) 0
          (slot-value c 'max-conflicts) nil
          (slot-value c 'conflicting-var-count) 0
          (slot-value c 'assignment-history) nil)

    ;; ignore any vars that are marked as such (these are the ones to
    ;; permanently ignore)
    (dolist (v (var-list c))
      (if (ignored-p v) (ignore-var v)))

    ;; take care of any initialization before vars are locked
    (init-before-locking-vars c)

    ;; assign locked vars and mark as locked
    (dolist (v previously-locked-vars)
      (lock-var v (gethash v (locked-var-table c))
                :restore-value t
                :warn-if-cannot-lock t))

    ;; returned value is CSP
    c))


(defmethod INIT-BEFORE-LOCKING-VARS ((c csp))

  "perform any initialization after reset but before locked vars have
    their assignments made.  This method can assume that no vars have
    assignments"
  ;; default does nothing
  nil)

(defmethod RESET-AND-CHECK ((c csp))
  "reset and check state for consistency"
  (reset-csp c)
  (check-csp-state c))


;;; --- Priority range

(defmethod MIN-MAX-PRIORITY ((c CSP))

  "input csp, output aa list (min max) priority (note higher priority
    is smaller priority number)"
  
  (let* ((all-vars (all-vars c))
	 (v1 (first all-vars))
         (prio-min (priority v1))
         (prio-max (priority v1)))
    (dolist (v (rest all-vars))
      (let ((prio (priority v)))
	(setf prio-min (min prio-min prio))
	(setf prio-max (max prio-max prio))))
    (list prio-min prio-max)))



;;; --- Methods to get vars or lists of vars meeting various conditions ---

(defmethod ALL-UNASSIGNED-VARS ((c CSP))
  
  "Return list of all (unignored) vars with no assignments"
  
  (let ((result nil))
    (dolist (var (var-list c))
      (if (null (assigned-value var))
        (push var result)))
    result))


(defmethod ALL-ASSIGNED-VARS ((c CSP))
  
  "Return list of all vars with assignments"
  
  (let ((result nil))
    (dolist (var (var-list c))
      (if (has-assigned-value-p var)
        (push var result)))
    result))


(defmethod ALL-UNASSIGNED-VARS-WITH-NO-CONFLICT-VALUES ((c CSP))
  
  "Return list of all (unignored) vars with no assignments and which
    have at least one value with no conflicts."
  
  (let ((result nil))
    (dolist (var (var-list c))
      (if (and (null (assigned-value var))
	       (> (values-with-no-conflicts-count var) 0))
        (push var result)))
    result))



(defmethod LIST-ASSIGNMENTS ((c csp) &key
                             var-mapping
                             value-mapping
                             (form :cons))
  
  "Return list of current assignments.  Options are:
    :var-mapping - function which takes a var as input and outputs the
      transformed var in the returned list. Default is nil and the
      var itself is returned.
    :value-mapping - function which takes a var and a value as input 
      and outputs the transformed value in the returned list.  Default
      is nil and the value itself is returned.
    :form - may be :cons or :list.  If :cons a list of conses
      ((var . value)...) is returned (where var and value have been
      transformed using the mapping functions.
      If form is :list then a list of lists is returned ((var value)...).
    See MAKE-LISTED-ASSIGNMENTS for the inverse function."
  
  (let ((result nil))
    (dolist (v (var-list c))
      (if (assigned-value v)
        (let ((var (if var-mapping (funcall var-mapping v) v))
              (val (if value-mapping 
                     (funcall value-mapping v (assigned-value v))
                     (assigned-value v))))
          (push (if (eq form :list)
                  (list var val)
                  (cons var val))
                result))))
    result))


(defmethod MAKE-LISTED-ASSIGNMENTS ((c csp) &key
                                    assignment-list
                                    var-mapping
                                    value-mapping
                                    (verbose nil))
  
  "Make assignments as specified in the input keyword arg :assignment-list
    which is a list such as returned by LIST-ASSIGNMENTS. The list can
    either be a list of 2-element lists 
      ((var1 val1) (var2 val2)...)  i.e. :form :list from list-assignments) 
    or a list of conses:
      ((var1 . val1)(var2 . val2)...) i.e. :form :cons from list-assignments.
    In either of these form, 'var' and 'val' can either be the lisp objects
    or some other representation which can be transformed into the lisp 
    objects based on the var-mapping and value-mapping arguments:
    :var-mapping - function which takes a 'var' as input and returns the
      var instance.  Default is nil and it is assumed that the
      var instance itself is in the assignment list.
    :value-mapping - function which takes a var instance and a 'val' as input 
      and returns the value to be assigned.  Default is nil and
      it is assumed the value number itself appears in assignment list.
    These defaults are so that the default assignment list from list-assignments
    can be used by make-listed-assignments without any transformations, e.g.
     (setf saved-assignments (list-assignments csp))
     (make-listed-assignments csp :assignment-list saved-assignments)
    The keyword argument :verbose, if t, causes each assignment to be printed
    to standard output."
  
  (dolist (var-val assignment-list)
    ;; get the var/value components
    (let* ((in-var (first var-val))
           (in-val (if (listp (rest var-val)) (second var-val) (rest var-val)))
           (var (if var-mapping (funcall var-mapping in-var) in-var))
           (val (if value-mapping (funcall value-mapping var in-val) in-val)))
      (if verbose
        (format t "~%var: ~a -> ~a, val: ~a -> ~a" in-var var in-val val))
      (assign-value var val))))


(defmethod A-RANDOM-UNASSIGNED-VAR ((c csp))

  "Return a single unassigned var, chosen at random"

  (a-random-element-from-list (all-unassigned-vars c)))


(defmethod ALL-VARS-WITH-CONFLICTS ((c CSP))

  "Return list of all (unignored) vars with conflicts on their 
    current assignments"

  (let ((result nil))
    (if (> (conflicting-var-count c) 0)
      (dolist (v (var-list c))
        (if (and (assigned-value v)
                 (assigned-value-has-conflicts-p v))
          (push v result))))
    result))


(defmethod ALL-VARS-WITH-NO-CONFLICTS ((c CSP))

  "Return list of all (unignored) vars with no conflicts on their current 
    assignments"

  (let ((result nil))
    (if (= (conflicting-var-count c) 0)
      ;; if no conflicting vars, return list of all vars
      (setf result (var-list c))
      ;; else build list of vars with no conflicts
      (dolist (v (var-list c))
        (if (and (assigned-value v)
                 (not (assigned-value-has-conflicts-p v)))
          (push v result))))
    result))


(defmethod A-RANDOM-VAR-WITH-CONFLICTS ((c CSP))

  "Return a random var with conflicts on its current assignment"

  (a-random-element-from-list (all-vars-with-conflicts c)))


(defmethod N-RANDOM-VARS-WITH-CONFLICTS ((c CSP) N)

  "Return up to N random vars with conflicts on current assignment"

  (n-random-elements-from-list (all-vars-with-conflicts c) N))


(defmethod ALL-VARS-WITH-MAX-CONFLICTS ((c CSP))

  "Return list of all vars whose conflict count on their current assignment
    is equal to the maximum"

  (let ((max-conf (max-conflicts c))
        (result nil))
    (dolist (v (var-list c))
      (if (and (assigned-value v)
               (= max-conf (conflicts-on-assigned-value v)))
        (push v result)))
    result))


(defmethod A-RANDOM-VAR-WITH-MAX-CONFLICTS ((c CSP))

  "Return a random variable which has the current maximum number of 
    conflicts on its current assignment"

  (a-random-element-from-list (all-vars-with-max-conflicts c)))


(defmethod N-RANDOM-VARS-WITH-MAX-CONFLICTS ((c CSP) n)

  "Return a list of up to N variables selected at random which have
    the current maximum number of conflicts on their current assignments"

  (n-random-elements-from-list (all-vars-with-max-conflicts c) n))


(defmethod ALL-VARS-MAX-CONFLICTS-FIRST ((c CSP))

  "Return list of all vars with current assignments, max conflicts 
    vars at front of list, order of others unspecified"

  (let ((max-conf (max-conflicts c))
        (result-max-conf nil)
        (result-other nil))
    (dolist (v (var-list c))
      (when (assigned-value v)
        (if (= max-conf (conflicts-on-assigned-value v))
          (push v result-max-conf)
          (push v result-other))))
    (nconc result-max-conf result-other)))


(defmethod ALL-VARS-ORDERED-BY-MAX-CONFLICTS ((c CSP))
  
  "Return list of all vars with current assignments, sorted in descending 
    order by conflicts on assignment"
  
  (let ((result nil))
    (dolist (v (var-list c))
      (when (assigned-value v)
        (push v result)))
    (stable-sort result #'(lambda (x y)
                            (> (conflicts-on-assigned-value x)
                               (conflicts-on-assigned-value y))))))


;;; -----------------------------------------------------------------------
;;; CSP quality measures
;;; -----------------------------------------------------------------------


(defmethod SUMMED-PREFERENCE ((c CSP))
  "Return sum of value preferences on all assignments.
    Null prefs contribute 0 to the sum"
  
  (let ((sum 0))
    (do-for-all-unignored-vars 
     :var v :csp c
     :form
     (when (assigned-value v)
       (incf sum (value-preference v (assigned-value v)))))
    sum))


(defmethod MEAN-PREFERENCE ((c CSP))
  "Return mean value preference on all assignments.
    If there are no assignments, 0 is returned."
  
  (cond ((= 0 (assigned-var-count c))
         0)
        (t (coerce (/ (summed-preference c)
                      (assigned-var-count c))
                   'float))))


(defmethod SUMMED-CONFLICT-COUNT ((c CSP))
  "Return sum of conflict counts on all assignments."

  (let ((sum 0))
    (do-for-all-unignored-vars 
     :var v :csp c
     :form
     (incf sum (or (conflicts-on-assigned-value v)
                   0)))
     sum))


(defmethod MEAN-CONFLICT-COUNT ((c CSP))
  
  "Return mean conflict count on all assignments.
    If there are no assignments, 0 is returned."
  
  (cond ((= 0 (assigned-var-count c))
         0)
        (t (coerce (/ (summed-conflict-count c)
                      (assigned-var-count c))
                   'float))))


(defmethod CSP-STATE-SUMMARY ((c CSP) &rest which)
  
  "Return list of state indicators.  &rest arg is list of indicators
    desired.  Returned value is lisp object of unspecified type.  
    Access to indicator values is via GET-CSP-STATE-SUMMARY-VALUE
    Legal choices for WHICH are: 

    :var-count - (var-count c)
    :assigned-var-count - number of assignments (assigned-var-count c)
    :conflicting-var-count - (conflicting-var-count c)
    :summed-conflicts - (summed-conflict-count c)
    :mean-conflicts - (mean-conflict-count c)
    :summed-preference - (summed-preference c)
    :mean-preference - (mean-preference c)
    Others TBD.

    :all - all of the above
    Default is :ALL

    Note:  :AROUND methods can be defined for this which get specific 
    state data for other CSP types.  The lisp object returned here is
    simply a plist, but this could be changed (change the method
    get-csp-state-summary-value to correspond)"

  ;; build returned summary
  (let ((result nil))
    (if (null which) (setf which '(:all)))

    (when (or (member :all which) (member :var-count which))
      (push (var-count c) result) 
      (push :var-count result))
    (when (or (member :all which) (member :assigned-var-count which))
      (push (assigned-var-count c) result) 
      (push :assigned-var-count result))
    (when (or (member :all which) (member :conflicting-var-count which))
      (push (conflicting-var-count c) result) 
      (push :conflicting-var-count result))
    (when (or (member :all which) (member :summed-conflicts which))
      (push (summed-conflict-count c) result) 
      (push :summed-conflicts result))
    (when (or (member :all which) (member :mean-conflicts which))
      (push (mean-conflict-count c) result) 
      (push :mean-conflicts result))
    (when (or (member :all which) (member :summed-preference which))
      (push (summed-preference c) result) 
      (push :summed-preference result))
    (when (or (member :all which) (member :mean-preference which))
      (push (mean-preference c) result) 
      (push :mean-preference result))

    result))


(defun GET-CSP-STATE-SUMMARY-VALUE (csp-state-summary &key type default)

  "Input is CSP state summary object such as returned by CSP-STATE-SUMMARY,
    and &key arg TYPE which must be one of legal values for WHICH
    supplied to CSP-STATE-SUMMARY.  Returned value is that for indicator.
    If indicator is not present, DEFAULT is returned."

  (getf csp-state-summary type default))

    
;;; -----------------------------------------------------------------------
;;; Methods applicable only to problems with known solutions
;;; -----------------------------------------------------------------------

(defmethod RESET-TO-SOLUTION ((c csp))
  "reset to solution state. Return t if all vars have solution values
    assigned, nil otherwise"
  (let ((all-defined t))
    (dolist (v (var-list c))
      (assign-solution-value v)
      (if (not (solution-value v))
        (setf all-defined nil)))
    all-defined))


(defmethod DISTANCE-FROM-SOLUTION ((c csp))
  "return number of vars with non-solution assignments, if:
    all vars assigned
    all vars have solution values recorded.
   nill otherwise"
  (let ((d 0)
        (ok t))
    (dolist (v (var-list c))
      (cond ((and (assigned-value v)
                  (solution-value v))
             (if (/= (assigned-value v) (solution-value v))
               (incf d)))
            (t (setf ok nil))))
    (if ok d nil)))


(defmethod RESET-TO-SPECIFIED-DISTANCE-FROM-SOLUTION ((c csp) d)
  "reset to specified distance d from solution"
  (let ((solution-set (reset-to-solution c)))
    (when solution-set
      (let ((random-to-change
             (n-random-elements-from-list (var-list c) d)))
        (dolist (v random-to-change)
          (assign-non-solution-value-at-random v))))))


;;; -----------------------------------------------------------------------
;;; CSP history methods
;;; -----------------------------------------------------------------------")
;;; 
;;; Using the CSP history:
;;; CSP assignment history is kept when the flag keep-assignment-history-p is
;;; non-nil.  This can be set either when the CSP is created (as an arg to
;;; make-csp) or at any time via the accessor:
;;;   (setf (keep-assignment-history-p csp) t)
;;; 
;;; The history automatically records all var assignments or unassignments
;;; in reverse chronological order, i.e. the history list could look like:
;;;   assign5 assign4 assign3 marker1 assign2 assign1
;;; assign5 is the most recent assignment and assign1 the oldest.
;;; A marker is an indicator of a particular point of interest in the history,
;;; as determined by the user. Assignments record the variable and old and new
;;; values (where nil values indicate unassignments).

;;; --- User methods for CSP history ---


(defmethod RESET-HISTORY ((c csp))

  "Clear the CSP assignment history"

  (setf (slot-value c 'assignment-history) nil))


(defmethod MARK-HISTORY ((c csp))
  
  "Mark history, return unique marker name for possible later backup.
    No action if keep-assignment-history-p is nil."
  
  (if (keep-assignment-history-p c)
    (let ((marker (gentemp "HISTORY-MARKER-")))
      (setf (slot-value c 'assignment-history)
            (cons marker (assignment-history c)))
      ;; returned value is marker symbol
      marker)))


(defmethod BACKUP-HISTORY-TO-MARKER ((c csp) marker)
  
  "Backup history to specified marker, undoing all assignments made
    since the marker.
    If keep-assignment-history-p is nil then error.
    If marker is nil, the back up to the start of the history (effectively
     doing a reset-history).
    If the marker does not exist, then take no action.
    If the marker is found, then back up to it and remove it from the
     history.  If necessary, mark the history again at that point.
    Returned value is the number of history records undone."
  
  (let ((record-count 0))
    
    (cond ((not (keep-assignment-history-p c))
           (error "CSP history not kept, no backup possible ~a" c))
          ((and marker (not (find-marker c marker)))
           ;; no action: marker does not exist
           )
          (t (let ((history (assignment-history c)))
               (loop (let ((rec (pop history)))
                       (cond ((null rec)
                              ;; done: end of history
                              (return nil))
                             ((and (symbolp rec) (eq rec marker))
                              ;; done: found marker
                              (return nil))
                             ((listp rec)
                              ;; undo assignment:  history record is
                              ;; (var old-value new-value), so here we
                              ;; just assign var to old-value (maybe nil)
                              (assign-value (first rec) (second rec))
                              (incf record-count))
                             (t (error "Unrecognized data in history: ~a"
                                       rec)))))
               ;; loop is done, save history
               (setf (slot-value c 'assignment-history) history))))
    ;; returned value
    record-count))


(defmethod GET-CSP-HISTORY ((c csp) &key (var-format :var))
  
  "Return the current csp history as a list of assignments
      ((var old-value new-value) ...)
    in FORWARD time order (i.e. first item in list was first assignment,
    etc.).  [Note that this is the reverse of the way the history is kept.]
    The optional argument var-format specifies how variables are indicated
    in the returned list:  legal values are
     :var - by var instance
     :name - by name
     :number - by number
    Old and new values are either numbers or nil (to indicate no 
     value assigned).
    No change to the internal history is made.  Markers are filtered out
    of the returned list."
  
  (let ((result nil))
    (dolist (rec (assignment-history c))
      (cond ((symbolp rec)
             ;; marker: skip
             )
            ((listp rec)
             (push (list (cond ((eq var-format :name) (var-name (first rec)))
                               ((eq var-format :number) (var-number (first rec)))
                               (t (first rec)))
                         (second rec)
                         (third rec))
                   result))
            (t (error "Bad data in history ~a" rec))))
    result))


;;; --- system methods for history management ---
;;; These are not user callable

(defmethod UPDATE-HISTORY ((c csp) (v var) old-value new-value)
  
  "Update the history to record the current var/value assignment, if
    keep-assignment-history-p is non-nil.  Values may be nil to record
    lack of an unassignment.  Not to be called by users.  This is automatically
    called whenever an assignment is changed."

  (if (keep-assignment-history-p c)
    (setf (slot-value c 'assignment-history)
          (cons (list v old-value new-value) 
                (assignment-history c)))))


(defmethod FIND-MARKER ((c csp) marker)
  
  "Return t if marker is found in history, else nil.  Aux function for
    backup-history-to-marker to ensure that marker exists."
  
  (dolist (history-record (assignment-history c))
    (if (and (symbolp history-record)
             (eq history-record marker))
      (return t))))


;;; -----------------------------------------------------------------------
;;; Save/restore assignments
;;; -----------------------------------------------------------------------

;;; See also LIST-ASSIGNMENTS, MAKE-LISTED-ASSIGNMENTS

(defmethod GET-ASSIGNMENTS ((c csp))
  "Return list of conses (var . value) for all current assignments"
  (let ((result nil))
    (dolist (v (var-list c))
      (if (assigned-value v)
        (push (cons v (assigned-value v)) result)))
    result))


(defmethod PUT-ASSIGNMENTS ((c csp) var-value-list)
  "Input is list of conses (var . value) such as that returned by 
    get-assignments. Make the specified assignments.  Vars not in the 
    list have no change made to their assignments"
  (dolist (var-value var-value-list)
    (assign-value (first var-value) (rest var-value))))


;;; -----------------------------------------------------------------------
;;; Using the conflict cache
;;; -----------------------------------------------------------------------

(defmethod CLEAR-CONFLICT-CACHE ((c CSP))

  "Clear the conflict cache"

  (clrhash (conflict-cache c)))


(defmethod SHOW-CONFLICT-CACHE ((c CSP))

  "Display the conflict cache to standard output in readable form"

  (let ((cache (conflict-cache c)))
    (maphash #'(lambda (key conf-recs)
                 (format t "~%var ~a (#~3d) value ~3d causes conflicts with: " 
                         (if (vars-named-p c) (name (first key)) "")
                         (var-number (first key))
                         (rest key))
                 (dolist (effect conf-recs)
                   (format t "~%      ~10a Var ~a (#~3d) values: ~a"
                           (first effect)  ;; cause
                           (if (vars-named-p c) (name (second effect)) "")
                           (var-number (second effect))
                           (cddr effect))))
             cache)))


(defmethod WHY-CONFLICTS ((c CSP) (v var) value)

  "Return reason for conflicts on input var and value.  Only valid if 
    cache-assigned-conflicts-p is t (returns nil if this is not set).
    Reasons are returned as a list of lists:
     ((cause-var1 cause-val1 reason1)
      (cause-var2 cause-val2 reason2)
      ...)
    where reasons are those supplied with ADD-TO-CONFLICTS-AND-CACHE.
   This has to scan the entire cache, so it won't be speedy."

  (when (cache-assigned-conflicts-p c)
    (let* ((result nil)
           (cache (conflict-cache c)))

      (maphash #'(lambda (key conf-recs)
                   (let ((cause-var (first key))
                         (cause-value (rest key)))
                     ;; see if cause var/val currently assigned
                     (when (and (has-assigned-value-p cause-var)
                                (= (assigned-value cause-var) cause-value))
                       ;; it is, see if conflict with affected var/value
                       (dolist (conf-rec conf-recs)
                         (when (eq (second conf-rec) v)
                           (dolist (valspec (cddr conf-rec))
                             (multiple-value-bind (first-val last-val weight)
                                                  (decode-valspec valspec)
			       (declare (ignore weight))
                               (when (and (>= value first-val)
                                          (<= value last-val))
                                 (pushnew (list cause-var cause-value
                                                (first conf-rec))
                                          result
                                          :test #'equal)))))))))
               cache)
      result)))


(defmethod UNASSIGN-CONFLICTING-VARS ((v var) value)
  
  "Unassign all vars whose current assignments conflict with var and value.  
    Requires caching to be on.  Returns list of vars unassigned, or nil if
    caching not on or no conflicting vars"

  (let ((conflict-causes (why-conflicts (csp v) v value)))
    (dolist (cause conflict-causes)
      ;; don't unassign v's value, only those that conflict with it
      (unless (eq v (first cause))
	(unassign-value (first cause))))))


;;; -----------------------------------------------------------------------
;;; Initial guess methods
;;; -----------------------------------------------------------------------


(defmethod INITIAL-GUESS-RCMC ((c csp))

  "Simple initial guess heuristic: select unassigned var at random,
    assign it a min-conflicts value.  Returned value is number of conflicts
    on resulting assignments"

  (reset-csp c)
  ;; initial guess
  (loop
    (if (all-vars-assigned-p c) (return nil))
    (let ((var-to-assign (a-random-unassigned-var c)))
      (assign-value var-to-assign (a-random-value-with-min-conflicts var-to-assign))))
  ;; returned value: conflict count
  (conflicting-var-count c))


(defmethod INITIAL-GUESS-MC ((c csp))

  "Simple initial guess heuristic: select unassigned var in order,
    assign it a min-conflicts value.  Returned value is number of conflicts
    on resulting assignments"

  (reset-csp c)
  ;; initial guess
  (dolist (var-to-assign (all-unassigned-vars c))
    (assign-value var-to-assign 
                  (a-random-value-with-min-conflicts var-to-assign)))
  ;; returned value: conflict count
  (conflicting-var-count c))


;;; Elaborate initial guess (or possibly even repair) methods:  these methods
;;; permit up to 3 levels of var and/or value selection criteria, and assign
;;; all or a subset of vars

(defmethod MAKE-ASSIGNMENTS
  ((csp CSP)
   &key 
   (to-vars :all-unassigned)
   (limited-to-vars nil)
   (how-many nil)

   (strategy nil)

   (primary-var-selection :random)      ; obsolete
   (secondary-var-selection :random)    ; obsolete
   (tertiary-var-selection :random)     ; obsolete
   (primary-value-selection :random)    ; obsolete
   (secondary-value-selection :random)  ; obsolete
   (tertiary-value-selection :random)   ; obsolete

   (verbose nil)
   (collector nil))
  
  "Make assignments to specified vars
    to-vars can be: :all, :all-unassigned, :all-assigned, 
     :all-with-conflicts, :all-with-no-conflicts, :all-with-max-conflicts
     :all-unassigned-with-no-conflict-values
     or a specific list of vars
    limited-to-vars is nil or a list of vars:  this acts as a secondary
     filter on :to-vars, in that any vars specified by to-vars must be
     in the limited-to-vars list to be considered
    how-many specifies how many of the vars specified by to-vars are to
     be assigned:  legal values are nil (for all of them) or a number >0
     (the maximum number to assign)

    :strategy is a two-element list specifying var and value selection
      criteria.  The first element specifies var selection, the second
      value selection.  Each element consists of a list of:
       :random, or :first, or :last, or
       a pair (fcn :min) or (fcn :max) in which case fcn is funcalled on
       var or var & value and returns nil or a numeric value, of which the
       min or max is selected.  Ties are broken by next criterion in the list.
       If there are still choices after all criteria have been applied, then
       the first var or value is chosen.

    % NOTE:  strategy replaces separate var and val selection args, but they
    % are kept for compatibility for now.
    % var selection (primary, secondary, tertiary) can be
    %  :random, or :first, or :last,
    %  or a pair (fcn :min) or (fcn :max) in which case
    %  fcn is funcalled on each var and returns nil or a numeric value,
    %  of which the min or max is selected.
    %  Ties are broken with secondary or tertiary var selection.
    %  If there are still ties after tertiary var selection, the first
    %  var in the remaining list is picked.
    % val selection is similar to var selection, except the function supplied
    %  must take two args, the var and the value.

    If verbose is t, information about each choice is printed to standard
     output.
    Returned value is list of assignments made, i.e. a list of pairs
     (var-number value)
    If collector is non-nil it must be a function of one arg, the CSP:
     it is called on the CSP after each assignment, and the results are 
     returned as the third element of the result list, i.e.
     (var-number value collector-results)"
  
  ;; first get vars to assign
  (let ((assignment-count 0)
        (specific-vars (if (listp to-vars) (copy-list to-vars) nil))
        (vars-to-assign nil)
        (var-strategy (or (first strategy)
                          (list primary-var-selection
                                secondary-var-selection
                                tertiary-var-selection)))
        (value-strategy (or (second strategy)
                            (list primary-value-selection
                                  secondary-value-selection
                                  tertiary-value-selection)))
        (result nil))
    
    (loop

      ;; get vars to consider
      (setf vars-to-assign
            ;; note these return vars in ascending order of number
            (cond ((eq to-vars :all-unassigned)
                   (all-unassigned-vars csp))
                  ((eq to-vars :all-assigned)
                   (all-assigned-vars csp))
                  ((eq to-vars :all-with-conflicts)
                   (all-vars-with-conflicts csp))
                  ((eq to-vars :all-with-no-conflicts)
                   (all-vars-with-no-conflicts csp))
                  ((eq to-vars :all-with-max-conflicts)
                   (all-vars-with-max-conflicts csp))
		  ((eq to-vars :all-unassigned-with-no-conflict-values)
		   (all-unassigned-vars-with-no-conflict-values csp))
                  (specific-vars)
                  (t (error "unrecognized form for to-vars ~a" to-vars))))

      ;; limit further if desired
      (when limited-to-vars
	(setf vars-to-assign
	  (intersection vars-to-assign limited-to-vars)))

      ;; exit if none to assign
      (if (null vars-to-assign) (return nil))
      
      (let* ((var-to-assign
              (a-random-element-from-list
               (apply-var-selection vars-to-assign var-strategy)))
             (value-choices (all-values-lexical-order var-to-assign))
             (value-to-assign
              (a-random-element-from-list
               (apply-value-selection var-to-assign value-choices
                                      value-strategy))))
        
        (when verbose 
          (format t "~%picked: ~a ~a (of ~a)" var-to-assign value-to-assign
                  (length value-choices)))
        
        (when (and var-to-assign value-to-assign)
          (assign-value var-to-assign value-to-assign)
          (incf assignment-count)
          (if collector
            (push (list (var-number var-to-assign) value-to-assign
                        (funcall collector csp)) result)
            (push (list (var-number var-to-assign) value-to-assign) result))
          (if (and how-many (>= assignment-count how-many))
            (return nil)))

        (when specific-vars
          (setf specific-vars (delete var-to-assign specific-vars))
          (if (null specific-vars) (return nil)))
        ))
    
    ;; returned value is assignments in order (+ collector if any)
    (nreverse result)))


(defmethod UNMAKE-ASSIGNMENTS
  ((csp CSP)
   &key 
   (to-vars :all-with-conflicts)
   (limited-to-vars nil)
   (how-many nil)
   (strategy nil)

   (primary-var-selection :random)      ; obsolete
   (secondary-var-selection :random)    ; obsolete
   (tertiary-var-selection :random)     ; obsolete

   (verbose nil)
   (collector nil))
  
  "Unassign specified vars
    to-vars can be: :all, :all-assigned, 
     :all-with-conflicts, :all-with-no-conflicts, :all-with-max-conflicts
     or a specific list of vars
     Default is :all-with-conflicts
    limited-to-vars is nil or a list of vars:  this acts as a secondary
     filter on :to-vars, in that any vars specified by to-vars must be
     in the limited-to-vars list to be considered
    how-many specifies how many of the vars specified by to-vars are to
     be unassigned:  legal values are nil (for all of them) or a number >0
     (the maximum number to unassign)

    :strategy specifies var selection criteria and is a list of:
       :random, or :first, or :last, or
       a pair (fcn :min) or (fcn :max) in which case fcn is funcalled on
       var and returns nil or a numeric value, of which the
       min or max is selected.  Ties are broken by next criterion in the list.
       If there are still choices after all criteria have been applied, then
       the first var is chosen.

    % NOTE:  strategy replaces separate var and val selection args, but they
    % are kept for compatibility for now.
    % var selection (primary, secondary, tertiary) can be
    %  :random, or :first, or :last,
    %  or a pair (fcn :min) or (fcn :max) in which case
    %  fcn is funcalled on each var and returns nil or a numeric value,
    %  of which the min or max is selected.
    %  Ties are broken with secondary or tertiary var selection.
    %  If there are still ties after tertiary var selection, the first
    %  var in the remaining list is picked.

    If verbose is t, information about each choice is printed to standard
     output.
    Returned value is list of unassignments made, i.e. a list of pairs
     (var-number value)
    If collector is non-nil it must be a function of one arg, the CSP:
     it is called on the CSP after each unassignment, and the results are 
     returned as the third element of the result list, i.e.
     (var-number value collector-results)"
  
  ;; first get vars to unassign
  (let ((unassignment-count 0)
        (specific-vars (if (listp to-vars) (copy-list to-vars) nil))
        (vars-to-unassign nil)
        (var-strategy (or strategy
                          (list primary-var-selection
                                secondary-var-selection
                                tertiary-var-selection)))
        (result nil))
    
    (loop

      ;; get vars to consider
      (setf vars-to-unassign
            ;; note these return vars in ascending order of number
            (cond ((eq to-vars :all-assigned)
                   (all-assigned-vars csp))
                  ((eq to-vars :all-with-conflicts)
                   (all-vars-with-conflicts csp))
                  ((eq to-vars :all-with-no-conflicts)
                   (all-vars-with-no-conflicts csp))
                  ((eq to-vars :all-with-max-conflicts)
                   (all-vars-with-max-conflicts csp))
		  (specific-vars)
                  (t (error "unrecognized form for to-vars ~a" to-vars))))

      ;; limit further if desired
      (when limited-to-vars
	(setf vars-to-unassign
	  (intersection vars-to-unassign limited-to-vars)))

      ;; exit if none to unassign
      (if (null vars-to-unassign) (return nil))
      
      (let* ((var-to-unassign
              (a-random-element-from-list
               (apply-var-selection
                vars-to-unassign var-strategy)))
             (old-value (assigned-value var-to-unassign)))
        
        (when verbose 
          (format t "~%picked: ~a (value was ~a)" var-to-unassign old-value))
        
        (when var-to-unassign
          (unassign-value var-to-unassign)
          (incf unassignment-count)
          (if collector
            (push (list (var-number var-to-unassign) old-value
                        (funcall collector csp)) result)
            (push (list (var-number var-to-unassign) old-value) result))
          (if (and how-many (>= unassignment-count how-many))
            (return nil)))

        (when specific-vars
          (setf specific-vars (delete var-to-unassign specific-vars))
          (if (null specific-vars) (return nil)))
        ))
    
    ;; returned value is assignments in order (+ collector if any)
    (nreverse result)))


(defun APPLY-VAR-SELECTION (var-list selection)
  
  "Select from vars in list according to selection criteria.
    var-list is a list of vars (may be nil, or may have only one entry)
    selection is a list of elements, each of which may be:
      :random, or :first, or :last,
      or a pair (fcn :min) or (fcn :max) in which case
        fcn is funcalled on each var and returns nil or a numeric value,
        of which the min or max is selected.
    Criteria in selection are applied consecutively until no more are left,
    or no vars satisfying the criteria are found.
    Returned value is a list of selected vars (may be nil)"
  
  (let* ((criterion (first selection))
         (trimmed-list
          (cond 
           
           ((null criterion) (error "null criterion"))
           
           ((null var-list)
            nil)
           
           ((= 1 (length var-list))
            var-list)
           
           ((eq criterion :random)
            (list (a-random-element-from-list var-list)))
           
           ((eq criterion :first)
            (list (first var-list)))
           
           ((eq criterion :last)
            (last var-list))
           
           (t (extreme-vars-in-list 
               var-list
               (first criterion)
               (second criterion))))))
    
    (cond ((null trimmed-list) nil)
          ((= (length trimmed-list) 1) trimmed-list)
          ((null (rest selection)) trimmed-list)
          (t (apply-var-selection trimmed-list (rest selection))))))

    

        
(defun APPLY-VALUE-SELECTION (var value-list selection)
  
  "Select from values in list according to selection criteria.
    var is a var instance, value-list is a list of values 
    (may be nil, or may have only one entry)
    selection is a list of criteria, of the form:
      :random, or :first, or :last,
      or a pair (fcn :min) or (fcn :max) in which case
       fcn is funcalled on each var and value and returns nil or a numeric value,
       of which the min or max is selected.
    Criteria in selection are applied consecutively until no more are left,
    or no vars satisfying the criteria are found.
    Returned value is a list of selected values (may be nil)"
  
  (let* ((criterion (first selection))
         (trimmed-list
          (cond 
           
           ((null criterion) (error "null criterion"))
           
           ((null value-list)
            nil)
           
           ((= 1 (length value-list))
            value-list)
           
           ((eq criterion :random)
            (list (a-random-element-from-list value-list)))
           
           ((eq criterion :first)
            (list (first value-list)))
           
           ((eq criterion :last)
            (last value-list))
           
           (t (extreme-values-in-list 
               var
               value-list
               (first criterion)
               (second criterion))))))
    
    (cond ((null trimmed-list) nil)
          ((= (length trimmed-list) 1) trimmed-list)
          ((null (rest selection)) trimmed-list)
          (t (apply-value-selection var trimmed-list (rest selection))))))



#| Obsolete: version only applies one selection criterion
(defun APPLY-VAR-SELECTION (var-list selection)

  "Select from vars in list according to selection criteria.
    var-list is a list of vars (may be nil, or may have only one entry)
    selection is one of:
     :random, or :first, or :last,
     or a pair (:min fcn) or (:max fcn) in which case
     fcn is funcalled on each var and returns nil or a numeric value,
     of which the min or max is selected.
    Returned value is a list of selected vars (may be nil)"

  (cond 

   ((null var-list)
    nil)

   ((= 1 (length var-list))
    var-list)
   
   ((eq selection :random)
    (list (a-random-element-from-list var-list)))
   
   ((eq selection :first)
    (list (first var-list)))
   
   ((eq selection :last)
    (last var-list))
   
   (t (extreme-vars-in-list 
       var-list
       (first selection)
       (second selection)))))

        
(defun APPLY-VALUE-SELECTION (var value-list selection)

  "Select from values in list according to selection criteria.
    var is a var instance, value-list is a list of values 
    (may be nil, or may have only one entry)
    selection is one of:
     :random, or :first, or :last,
     or a pair (:min fcn) or (:max fcn) in which case
     fcn is funcalled on each var and value and returns nil or a numeric value,
     of which the min or max is selected.
    Returned value is a list of selected values (may be nil)"

  (cond 

   ((null value-list)
    nil)

   ((= 1 (length value-list))
    value-list)
   
   ((eq selection :random)
    (list (a-random-element-from-list value-list)))
   
   ((eq selection :first)
    (list (first value-list)))
   
   ((eq selection :last)
    (last value-list))
   
   (t (extreme-values-in-list 
       var
       value-list
       (first selection)
       (second selection)))))
|#

(defun EXTREME-VARS-IN-LIST (var-list fcn min-or-max)

  "Find vars with extreme value of some property.
    Input is a list of variables, a function to apply to each var,
    and min-or-max which is either :min or :max;
    Function can return nil, or a numeric value.
    Return list of vars which have either min or max value returned
    or nil if there are none."

  (let ((extreme nil)
        (extreme-vars nil)
        (comparison (cond ((eq min-or-max :min) #'<)
                          ((eq min-or-max :max) #'>)
                          (t (error "invalid min-or-max ~a" min-or-max)))))
    (dolist (var var-list)
      (let ((property-value (funcall fcn var)))
        (cond ((null property-value)) ;do nothing
              (extreme ;already have extreme
               (cond ((funcall comparison property-value extreme)
                      ;; found new extreme value
                      (setf extreme property-value)
                      (setf extreme-vars (list var)))
                     ((= property-value extreme)
                      ;; equal to extreme value: save this var
                      (push var extreme-vars))
                     (t nil)))
              ((null extreme)
               ;; no extreme
               (setf extreme property-value)
               (setf extreme-vars (list var)))
              (t (error "drop through cases!")))))
    (nreverse extreme-vars)))


(defun EXTREME-VALUES-IN-LIST (var value-list fcn min-or-max)

  "Find values with extreme values of some property.
    Input is a var and a list of values, a function to apply to each var & value,
    and min-or-max which is either :min or :max;
    Function can return nil, or a numeric value.
    Return list of values which have either min or max value of fcn returned
    or nil if there are none."
  (let ((extreme nil)
        (extreme-values nil)
        (comparison (cond ((eq min-or-max :min) #'<)
                          ((eq min-or-max :max) #'>)
                          (t (error "invalid min-or-max ~a" min-or-max)))))
    (dolist (value value-list)
      (let ((property-value (funcall fcn var value)))
        (cond ((null property-value)) ;do nothing
              (extreme ;already have extreme
               (cond ((funcall comparison property-value extreme)
                      ;; found new extreme value
                      (setf extreme property-value)
                      (setf extreme-values (list value)))
                     ((= property-value extreme)
                      ;; equal to extreme value: save this var
                      (push value extreme-values))
                     (t nil)))
              ((null extreme)
               ;; no extreme
               (setf extreme property-value)
               (setf extreme-values (list value)))
              (t (error "drop through cases!")))))
    (nreverse extreme-values)))


;;; -----------------------------------------------------------------------
;;; Repair methods
;;; 
;;; var-selection below has one of the following values:
;;;   :RC - select variable with conflicts at random (Random Conflicts)
;;;   :MC - select variable with max-conflicts at random (Max Conflicts)
;;; value-selection is uniformly min-conflicts (select value at random
;;;   with min-conflicts).
;;; 
;;; NOTE:  preferences are ignored here!
;;; -----------------------------------------------------------------------

(defmethod REPAIR-HILL-CLIMB ((c CSP) limit var-selection)
  "repair current assignments with single step min-conflicts repair
    method.  var-selection is :rc or :mc.
    Multiple values returned:
     t if in solution, nil otherwise
     number of steps"
  (let ((steps 0)
        var)
    (loop
      (if (in-solution-state-p c) (return nil))
      (setf var 
            (cond ((eq var-selection :rc) (a-random-var-with-conflicts c))
                  ((eq var-selection :mc) (a-random-var-with-max-conflicts c))
                  (t (error "unrecognized var selection ~a" var-selection))))
      (if (null var) (return nil))
      (assign-value var (a-random-value-with-min-conflicts var))
      (incf steps)
      (if (= steps limit) (return nil)))
    (values (in-solution-state-p c)
            steps)))


(defmethod MULTISTEP-REPAIR-HILL-CLIMB ((c CSP) limit var-selection)
  "repair current assignments with multi-step min-conflicts repair
    method.  var-selection is :rc or :mc.
    Multiple values returned:
     t if in solution, nil otherwise
     number of steps"
  (let ((steps 0)
        vars)
    (loop
      (if (in-solution-state-p c) (return nil))
      (setf vars 
            (cond ((eq var-selection :rc) (all-vars-with-conflicts c))
                  ((eq var-selection :mc) (all-vars-with-max-conflicts c))
                  (t (error "unrecognized var selection ~a" var-selection))))
      (dolist (var vars)
	(assign-value var (a-random-value-with-min-conflicts var)))
      (incf steps (length vars))
      (if (>= steps limit) (return nil)))
    (values (in-solution-state-p c)
            steps)))


(let ((steps nil)
      (var-method nil)
      (value-method nil)
      (consistency-checks nil)
      (max-steps nil))
  
  (defmethod REPAIR-BACKTRACK ((c CSP) limit var-selection
                               &optional (value-selection :mc))
    "repair current assignments with informed backtracking min-conflicts repair
    method.  Limit is max number of assignments permitted before failure.
    var-selection is :rc or :mc
    value-selection is :mc or nil
    Multiple values returned:
     t if in solution, nil otherwise
     number of steps"
    (let ((vars-left (var-list c))
          (vars-done nil))
      (setf steps 0)
      (setf var-method var-selection)
      (setf value-method value-selection)
      (setf max-steps limit)
      (setf consistency-checks 0)
      (REPAIR-BACKTRACK-AUX c vars-left vars-done)
      (values (in-solution-state-p c)
              steps
              consistency-checks)))
  
  (defmethod RC-BACKTRACK-SELECT-VAR ((c csp) vars-left)
    "select a random var with conflicts from vars-left"
    (let ((candidates nil))
      (dolist (var vars-left)
        (if (assigned-value-has-conflicts-p var)
          (push var candidates)))
      (a-random-element-from-list candidates)))

  (defmethod MC-BACKTRACK-SELECT-VAR ((c csp) vars-left)
    "select a random var with max conflicts from vars-left"
    (let ((candidates nil)
          (max-conf nil))
      ;; get max conflicts count
      ;; this will break if the vars in vars-left don't have assignments
      (dolist (var vars-left)
        (if (null max-conf)
          (setf max-conf (conflicts-on-assigned-value var))
          (setf max-conf (max max-conf
                              (conflicts-on-assigned-value var)))))
      (dolist (var vars-left)
        (if (= max-conf (conflicts-on-assigned-value var))
          (push var candidates)))
      (a-random-element-from-list candidates)))

  (defmethod REPAIR-BACKTRACK-AUX ((c CSP) vars-left vars-done)
    "repair backtrack: return t if done (solution or steps exceeded),
      nil if failed"
    (cond 
     ((or (in-solution-state-p c)
          (= steps max-steps))
      t) ;done
     (t (let* ((var-to-do 
                (cond ((eq var-method :rc) (rc-backtrack-select-var c vars-left))
                      ((eq var-method :mc) (mc-backtrack-select-var c vars-left))
                      (t (error "unknown var-method ~a" var-method))))
               (ordered-values 
                (cond ((eq value-method :mc)
                       (all-values-ordered-by-min-conflicts var-to-do))
                      ((eq value-method nil)
                       (all-values-lexical-order var-to-do))
                      (t (error "unknown value-method ~a" value-method))))
               (new-vars-left (remove var-to-do vars-left))
               (new-vars-done (cons var-to-do vars-done))
               conflict-with-done)
          (dolist (value ordered-values)
            ;; if value would conflict with any assignment in vars-done, skip
            (setf conflict-with-done
                  (dolist (done vars-done)
                    (incf consistency-checks)
                    (if (assignments-would-conflict-p
                         c var-to-do value done)
                      (return t))))
            (when (not conflict-with-done)
              ;; no conflict: make assignment and continue
              (assign-value var-to-do value)
              (incf steps)
              ;; break out?
              (cond ((or (in-solution-state-p c)
                         (= steps max-steps))
                     (return t))
                    ((REPAIR-BACKTRACK-AUX c new-vars-left 
                                           new-vars-done)
                     (return t)))))))))
  )
             
;;; -----------------------------------------------------------------------
;;; CSP methods implementing constraints
;;; 
;;; These have to be customized for each problem type
;;; 
;;; NOTE:  it is up to the problem whether or not to maintain conflicts
;;; on ignored vars.  If they are maintained, the constraint methods should
;;; pay attention to ignored-var-list as well as var-list.  If not, 
;;; inconsistencies can arise if ignored vars are made unignored.  
;;; Beware!!
;;; -----------------------------------------------------------------------


(defmethod CSP-VAR-VALUE-ASSIGN ((c CSP) (v var) value)
  "var v has its assignment made to value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  (declare (ignore value))
  nil)


(defmethod CSP-VAR-VALUE-UNASSIGN ((c CSP) (v var) value)
  "var v has its assignment unmade from value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  (declare (ignore value))
  nil)


(defmethod ASSIGNMENTS-WOULD-CONFLICT-P ((c csp) (v1 var) value1 (v2 var))
  "return t if v2 has an assigned value that would conflict with assigning
    val1 to v1, nil otherwise.  This method is required only by the 
    repair backtrack methods and can be left undefined if this technique
    is not used.  Default method returns nil."
  (declare (ignore value1))
  nil)



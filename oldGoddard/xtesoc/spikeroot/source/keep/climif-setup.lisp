;;;-*- Mode:COMMON-LISP; Package: user; Base: 10 -*-

;;; load the basic system...



#+:Allegro
(progn
  (chdir "/cygx1/h1/ehm/spike/spikeroot")
  (load "source/climif-package")
;;;  (load "source/export-spike-build")
  (load "source/clim-gwindow")
  ;; (load "source/airline")
  (load "source/climif-main")
  )

;; do first, so can edit while the rest is loading...
#+:CCL 
(progn
  (load "Macintosh HD:Spike:work:climif-package.lisp")
  (load "Macintosh HD:Spike:source:export-spike-build-170")
  ;; (load "Macintosh HD:SPike:work:airline")
  (load "Macintosh HD:Spike:work:clim-gwindow")
  (load "Macintosh HD:Spike:work:climif-main")
  )

;;(load "Macintosh HD:Spike:Lib:repair-examples")

;; (ed "Macintosh HD:Spike:work:climif-main.lisp")

;;; *** NOT READY TO LOAD... ***
;;(load "Macintosh HD:Spike:work:climif-main.lisp")

;;; LOAD XTE DEMO PROBLEM

(setf *XTE-SCHEDULE-START*   (dmy-to-time '1-jan-1992))
(setf *XTE-SCHEDULE-END*     (dmy-to-time '1-jul-1992))
(setf *XTE-TIME-QUANTUM*     7)
(setf *XTE-SEGMENT-START*    (dmy-to-time '1-jan-1992))
(setf *XTE-SEGMENT-DURATION* 7)
(setf *XTE-SEGMENT-COUNT*    27)
(setf *XTE-CAPACITY-UNIT*    0.01)
(setf *XTE-OVERSUBSCRIPTION-FACTOR* 0.50)

;;; for short term scheduling use 5 min time quantization
;;; other parameters in xte-csp.lisp
(setf   *XTE-SHORT-TERM-TIME-QUANTUM* (/ 5.0 1440.0))

(xte-init-prep-and-load '("xte-test")

#|
(xte-init-prep-and-load 
 '("xte-test1")
 :constraint-files '("xte-test1"))
|#

(time (early-greedy-schedule *XTE-LONG-TERM-SCHEDULE*))
;;(time (max-pref-schedule *XTE-LONG-TERM-SCHEDULE*))
;;(time (elminc-schedule *XTE-LONG-TERM-SCHEDULE*))
;;(time (hi-prio-max-pref-schedule *XTE-LONG-TERM-SCHEDULE*))

;;; report current long-term schedule to lisp listener
(schedule-report (obs-csp *XTE-LONG-TERM-SCHEDULE*))


#| For ASTRO-D: test function calls

(csp-state-summary (obs-csp *xte-long-term-schedule*))
(vars-assigned-to-bin (obs-cap-constraint *XTE-LONG-TERM-SCHEDULE*) 0)
(show-segments *xte-long-term-schedule*)
(obs-cap-constraint *current-obs-csp-chronika*)
(print (bin-usage (obs-cap-constraint *current-obs-csp-chronika*)))
(check-capacity-constraint (obs-cap-constraint *current-obs-csp-chronika*))
(reset-csp (obs-csp *xte-long-term-schedule*))
(show-csp-state (obs-csp *xte-long-term-schedule*))
(assign-value (var-named (obs-csp *xte-long-term-schedule*) 'dummy-1) 25)
(user::var-list (obs-csp *xte-long-term-schedule*))
|#

#|
;;; Make user manual

(load "macintosh hd:lisp:cmu-util:user-manual.fasl")
(with-open-file  (stream "Macintosh HD:repair.text"
                         :direction :output
                         :if-does-not-exist :create
                         :if-exists :supersede)
  (create-user-manual "Macintosh HD:spike:source:repair.lisp"
                      :output-stream stream))

|#

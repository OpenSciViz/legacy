;;; xte-spike-build
;;;
;;; Nov-30-92 Compiles sources and builds xte version of Spike
;;;   and then exits, as called for in the XTE Spike User Manual. 

(load "source/xte-build-spike")
(exit)

;;;-*-Mode:LISP; Syntax: Common-Lisp;Package: USER;-*-
;;;
;;; xte-functions.lisp
;;;
;;; special purpose functions for XTE
;;;
;;; December 1992 - Fixed SAA-REPORT to report times as dd-mmm-yyyy and to 
;;;                 display entries into SAA before exits.  r. buehler
;;;
#|
(compile-file "source/xte-functions.lisp" :output-file "lib/xte-functions.fasl")
(load "lib/xte-functions.fasl")
|#

(eval-when (compile load eval)
  (in-package user))

(load "source/leo-ephem.lisp")
;;; ======================================================================
;;; G L O B A L S
;;; ======================================================================

(defvar *SAA-COUNT* 0)

(defvar *SAA-LIST*  (make-hash-table :size 200))

;;; Preserve circular model of satellite
(defvar *XTE-CIRCULAR* *XTE*)

;;; Attach ephemeris model of satellite
(setf *XTE* (make-leo-satellite-from-ephemeris 6100.9))



;;; ======================================================================
;;; F U N C T I O N S
;;; ======================================================================




;;;  the next function is added 6-5-92

(defun ST-CURRENT-CONSTRAINT-PLOT (value)
  (let* ((var value)
	 (task (task var))
	 (chronika (chronika task))
	 
	 (start  (qtime-to-tjd chronika (qtime-start chronika)))
	 (start (jd-to-ymdhms  start))
	 (end  (qtime-to-tjd chronika (qtime-end chronika)))
	 (end (jd-to-ymdhms end))
	 
	 (start-date (format nil "~a-~a-~a" (third start) 
			     (month-number-to-month-string (second start)) (first start)))
	 (end-date (format nil "~a-~a-~a" (third end) 
			   (month-number-to-month-string (second end)) (first end)))
	 )
    #|
    (format t "~% name: ~a" (target-name task))
    (format t "~% ra:   ~a" (target-ra task))
    (format t "~%dec:   ~a" (target-dec task))
    (format t "~%start-date: ~a" start-date)
    (format t "~%end-date  : ~a" end-date)
|#
    
    (plot-xte-target-constraints
     :name		(target-name task)
     :ra		(target-ra task)
     :dec		(target-dec task)
     :start-date	start-date
     :end-date	end-date
     :orbit-model	"xte"
     :file		"temp/st-constraint.ps")
    
;;; (format t "~%Finish Saving")
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun SAA-REPORT (start-time end-time &key (stream t))
  
  "Print SAA entry-exit table"
  
  (format stream "~%SAA Entry-Exit Table")
  (format stream "~% Starting Time: ~a   Ending Time : ~a " start-time end-time)
  (format stream "~%          Time        Lat         Lon        North      East      Down      Strength")
  (format stream "~%                                                (Normalized vector)          (gauss)")
  
  (dolist (int (pcf-zero-intervals
		(saa-pcf *xte* 
			 start-time
			 end-time
			 :table *SAA-ENTRY-EXIT-TABLE*)
		start-time
		end-time))
	  
	  (let* ((SAA-entry (first int))
		 (SAA-exit (second int))
		 (earth-radius 6378.14)
		 
		 ;;    compute altitude, year, and position (lon.lat). Then
		 ;;    find out the magnetic field vector (north, east, down), and its strength.
		 
		 (entry-alt (- (distance *xte* SAA-entry) earth-radius))
		 (entry-year (first (jd-to-ymdhms start-time)))
		 (entry-position (earth-longitude-latitude-below *xte*
								 SAA-entry :units :degrees))
		 (entry-lat (second entry-position))
		 (entry-lon (first entry-position))
;;;		This is a dummy.
		 (ra-dec (position-ra-dec (position-vector *xte* SAA-entry)))
		 (entry-ra (first ra-dec))
		 (entry-dec (second ra-dec))
		 
		 (exit-alt  (- (distance *xte*  SAA-exit) earth-radius))
		 (exit-year (first (jd-to-ymdhms end-time)))
		 (exit-position  (earth-longitude-latitude-below *xte* 
								 SAA-exit :units :degrees))
		 (exit-lat (second exit-position))
		 (exit-lon (first exit-position))
;;;		 this is also a dummy
		 (ra-dec (position-ra-dec (position-vector *xte* SAA-entry)))
		 (exit-ra (first ra-dec))
		 (exit-dec (second ra-dec))
		 )
	    (format stream "~% entry ~10,5,,f ~10,5,,f ~10,5,,f " 
		    (pretty-string-abs-time
		      (tjd-to-abs-time SAA-entry))
		    (coerce entry-lat 'single-float)
		    (coerce entry-lon 'single-float))
	    (format stream "~% exit  ~10,5,,f ~10,5,,f ~10,5,,f ~%"
		     (pretty-string-abs-time
			       (tjd-to-abs-time SAA-exit))
		     (coerce exit-lat 'single-float)
		     (coerce exit-lon 'single-float))
	    )))

(defun SORT-HASH-TABLE (table element)
  ;;  sort the entries of a hash-table by the first element of the entries. Ascending order.
  ;;   table : name of the hash-table
  ;;  element: number of the entries
  
  (cond ((>= element 0)
	 (dotimes (val element)
		  (let* ((j  (- element val))
			 (jj (- j 1)))
		    (cond ((>= jj 0)
			   (dotimes (val2 jj)
				    (cond ((> (first (gethash val2 table)) (first (gethash j table)))
					   (setf y-save (gethash j table))
					   (setf (gethash j table) (gethash val2 table))
					   (setf (gethash val2 table) y-save))
					  )		; cond
				    ))		; dotimes, condition
			  )			; cond
		    )))			; let*, dotime, contion
	))				; cond, closing





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;; The next function is added by T. I. feb-27-92

(defun PCF-zero-intervals (pcf &optional 
			       (range-start *MINUS-INFINITY*)
			       (range-end *PLUS-INFINITY*))
  "Returns a list of (start end) times when the input PCF is non-zero"
  (let ((result nil))
    (PCF-loop-over-intervals
     pcf start end value left-to-do done
     ((nreverse result))
     
     (unless (or (<= end range-start)
		 (>= start range-end))
	     (when (zerop value)
		   (if (null result)
		       ;; start the output list
		       (push (list (max-of-two-values start range-start)
				   (min-of-two-values end range-end))
			     result)
		     ;; add to output list
		     (let ((tmin (max-of-two-values start range-start))
			   (tmax (min-of-two-values end range-end))
			   (old-tmax (second (first result))))
		       ;; see whether the new interval can be merged with the previous one
		       (if (> tmin old-tmax)
			   ;; add new nonzero interval to the result
			   (push (list tmin tmax) result)
			 ;; modify the end time of the most recent interval addede
			 (setf (second (first result)) tmax)))))))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; the next function is added 6-10-92
(defun GET-ORIGINAL-TARGET-LIST ()
  
  ;;  Check unobserved targets and put them into an "unobserved-list".
  
  (if (null *original-target-list*)
      (let* ((original-list nil))
	(with-current-obs-csp-chronika
	 csp chronika
	 (do-for-all-vars
	  :var v :csp csp
	  :predicate (has-assigned-value-p v)
	  :form (let* ((name (target-name (task v)))
		       (ra   (target-ra (task v)))
		       (dec  (target-dec (task v)))
		       (exp-time (exp-time (task v)))
		       (priority (priority (task v)))
		       )
		  (push (list name ra dec exp-time priority) original-list)
		  ))
	 )
	
	(setf *original-target-list* original-list)
	)				;let*
    )					;if
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;the next function is added 6-9-92

(defun LT-UNOBSERVED-TARGETS (original-list)
  
  (let* ((observed-list nil))
    
    (do-for-all-vars
     :var v :csp (obs-csp *xte-long-term-schedule*)
     :predicate (has-assigned-value-p v)
     :form (let* ((name (target-name (task v)))
		  )
	     (push name observed-list)))
    
    (dolist (observed observed-list)
	    (dolist (check original-list)
		    (if (eq observed (first check))
			(setf original-list (remove check original-list)))))
    
    
    (with-open-file (stream "temp/long-term-unobserved-list"
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :supersede)
		    (let ((*standard-output* stream))
		      
		      (dolist (target original-list)
			      (format stream "~%(def-xte-expousre ")
			      (format stream ":name ~a " (first target))
			      (format stream ":ra    ~a " (second target))
			      (format stream ":dec   ~a " (third target))
			      (format stream ":exp-time ~a " (fourth target))
			      (format stream ":priority ~a " (sixth target))
			      (format stream "~% ")
			      )				;dolist
		      :stream stream)			;let
		    )
    )					; let*
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;the next function is added 6-8-92

(defun ST-UNOBSERVED-TARGETS ()
  
  ;; Check unobserved targets and put them into an "unobserved-list".
  
  (let* ((segment-number *short-term-segment-number*)
	 (long-term-obs-csp *XTE-LONG-TERM-SCHEDULE*)
	 (csp (obs-csp long-term-obs-csp))
	 (vars (vars-assigned-to-bin
		(obs-cap-constraint long-term-obs-csp) segment-number))
	 (original-list (mapcar #'task vars))
	 
	 (observed-list nil)
	 )
    
    (do-for-all-vars
     :var v :csp (obs-csp *xte-short-term-schedule*)
     :predicate (has-assigned-value-p v)
     :form (let* ((name (target-name (task v)))
		  )
	     (push name observed-list)))
    
    (dolist (observed observed-list)
	    (dolist (check original-list)
		    (if (eq observed (target-name check))
			(setf original-list (remove check original-list)))))
    
    
    (with-open-file (stream "temp/short-term-unobserved-list"
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :supersede)
		    (let ((*standard-output* stream))
		      
		      (dolist (target original-list)
			      (format stream "~%(def-xte-expousre ")
			      (format stream ":name ~a " (target-name target))
			      (format stream ":ra    ~a " (target-ra target))
			      (format stream ":dec   ~a " (target-dec target))
			      (format stream ":exp-time ~a " (exp-time target))
			      (format stream ":priority ~a " (priority target))
			      (format stream "~% ")
			      )				;dolist
		      :stream stream)			;let
		    )
    
    
    )					; let*
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun REST-OF-LONG-TERM-TARGETS (segment-number)
  
  ;; this function creates a file with all targets in the long-term schedule
  ;; expect those sent to the short-term schedule.
  ;; this one is called in sched-long-term.lisp, when short-term segment is 
  ;; selected.
  
  (setf rest-of-targets nil)
  
  (let* ((long-term-obs-csp *XTE-LONG-TERM-SCHEDULE*)
	 (csp (obs-csp long-term-obs-csp))
	 (bins (user::bin-list long-term-obs-csp))
	 )
    (dolist (bin bins)
	    (if (/= (first bin) segment-number)
		(let* ((vars (vars-assigned-to-bin
			      (obs-cap-constraint long-term-obs-csp) (first bin)))
		       (segment-task (mapcar #'task vars))
		       )
		  (if segment-task
		      (dolist (seg segment-task)
			      (push seg rest-of-targets)))
		  ))
	    )	   ;;;dolist
    )					; let*
  
  (with-open-file (stream "temp/rest-of-long-term-targets"
			  :direction :output
			  :if-does-not-exist :create
			  :if-exists :supersede)
		  (let ((*standard-output* stream))
		    
		    (dolist (target rest-of-targets)
			    (format stream "~%(def-xte-expousre ")
			    (format stream ":name ~a " (target-name   target))
			    (format stream ":ra    ~a " (target-ra   target))
			    (format stream ":dec   ~a " (target-dec   target))
			    (format stream ":exp-time ~a " (exp-time   target))
			    (format stream ":priority ~a " (priority  target))
			    (format stream "~% ")
			    
					;dolist
			    :stream stream)			;let
		    )
		  ))




#|
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun NEW-SCHEDULE-REPORT ((csp xte-short-term-csp) 
			    &key (stream t) (optimize t))
  
  "Report schedule status to stream"
  ;; (schedule-report (obs-csp -occ))
  
  ;; report all assignments and gaps
  (format stream "~%Scheduled activities:")
  (format stream
	  "~%Name            start date       start end  dur   dur    min   -exp. time---  conf suit  suit/")
  
  (format stream
	  "~%                                                (days)   dur    req  act/req    #        max")
  
  (let* ((assigned-vars (all-assigned-vars csp))
   	 (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
   	 (opt-table (if optimize (short-term-optimize csp)))
         (chronika (chronika (task (first (all-vars csp)))))
	 (tjd-chronika-start (qtime-to-tjd chronika (qtime-start chronika)))
	 (tjd-chronika-end (qtime-to-tjd chronika (qtime-end chronika)))
	 (tjd-chronika-dur (- tjd-chronika-end tjd-chronika-start))
         (overall-start (start chronika))
         (overall-end   (end chronika))
   	 (prev-end overall-start)
   	 (sched-days (duration-in-qtime-to-days 
                      chronika (- overall-end overall-start)))
	 (total-gap 0)
	 (total-sched 0)
	 (total-min 0)
	 (total-exp 0)
	 ;; for optimized schedule
	 (total-exp-time 0)
	 (total-overhead 0)
	 (total-efficiency 0)
	 )	      
    ;; report assigned vars
    (dolist (var sorted-vars)
	    (when (< prev-end (assigned-time var))
		  ;; gap
		  (format stream "~%---gap(~4d)---                  ~4d ~4d" 
			  (- (assigned-time var) prev-end) prev-end (assigned-time var))
		  (incf total-gap (- (assigned-time var) prev-end)))
	    (incf total-sched (duration-given-start-at (task var) (assigned-time var)))
	    (incf total-min (minimum-nonzero-duration (task var)))
	    (incf total-exp (exposure-duration-at-time (task var) (assigned-time var)))
	    (format stream "~%~a" (task-report var))
	    ;; flag if actual>minimum duration
	    (if (> (duration-given-start-at (task var) (assigned-time var))
		   (minimum-nonzero-duration (task var)))
		(format stream "*"))
	    
	    (when optimize
		  (let* ((task (task var))
			 (opt-results (gethash var opt-table))
			 (overhead (if opt-results (first opt-results)))
			 (sched-int (if opt-results (rest opt-results)))
			 (summed (if sched-int (summed-interval-duration sched-int))))
		    (dolist (interval sched-int)
			    (format stream "~%                ~a  -  ~a" (task-formatted-abs-time task (first interval))
				    (task-formatted-abs-time task (second interval)))
			    (format stream "~% ")
			    (describe-satellite-orientation *xte*  (first interval)) 
			    (format stream "~% ~% ")
			    )
		    (format stream   "~%   Total: ~,3fd (~ds)  Overhead following task: ~ds"
			    summed (round (* 86400 summed)) (round (* 86400 (or overhead 0))))))
	    
	    (setf prev-end (max prev-end (end-given-start-at (task var) (assigned-time var)))))
    
    ;; gap at end?
    (when (/= prev-end overall-end)
	  (incf total-gap (- overall-end prev-end))
	  (format stream "~%---gap(~4d)---                  ~4d ~4d" 
		  (- overall-end prev-end) prev-end overall-end))
    (format stream "~%Schedule time span  : ~6,2f days" sched-days)
    (format stream "~%Total scheduled time: ~6,2f days (~6,2f%)" 
            (duration-in-qtime-to-days chronika total-sched)
            (* 100 (/ (duration-in-qtime-to-days chronika total-sched) 
                      sched-days)))
    (format stream "~%Total gaps          : ~6,2f days (~6,2f%)" 
	    (duration-in-qtime-to-days chronika total-gap)
	    (* 100 (/ (duration-in-qtime-to-days chronika total-gap) 
                      sched-days)))
    (when (> total-exp 0)
	  (format stream "~%Total exp time      : ~6,2f days (~d sec, ~6,2f% of schedule time span)"
		  (/ total-exp 86400.0) total-exp 
		  (* 100 (/ (/ total-exp 86400.0) sched-days))))
    (if (> total-min 0)
	(format stream "~%Total minimum duration: ~6,2f days (expansion factor actual/min: ~7,3f)"
		(duration-in-qtime-to-days chronika total-min)
		(/ total-sched total-min)))
    (when opt-table
	  (maphash #'(lambda (key val)
		       (declare (ignore key))
		       (incf total-overhead (or (first val) 0))
		       (incf total-exp-time (summed-interval-duration (rest val))))
		   opt-table)
	  (setf total-efficiency (/ total-exp-time tjd-chronika-dur))
	  (format stream "~%Optimized schedule:")
	  (format stream "~%  Total exp time : ~6,2fd (~10ds)" total-exp-time (round (* 86400 total-exp-time)))
	  (format stream "~%  Total overhead : ~6,2fd (~10ds)" total-overhead (round (* 86400 total-overhead)))
	  (format stream "~%  Efficiency     : ~6,2f%" (* total-efficiency 100)))
    
    
    (unless (all-vars-assigned-p csp)
	    (setf total-min 0)
	    (format stream "~%Unscheduled activities:")
	    (dolist (var (all-unassigned-vars csp))
		    (format stream "~%~a" (task-report var))
		    (incf total-min (minimum-nonzero-duration (task var)))
		    )
	    (format stream "~%Total minimum time not scheduled:  ~6,2f days"
		    (duration-in-qtime-to-days chronika total-min)))
    
    (format stream "~% ~d tasks, ~a assigned, ~a assigned with conflicts"
	    (var-count csp) (assigned-var-count csp) (conflicting-var-count csp))
    (format stream "~% mean preference  : ~8,2f  mean conflicts  : ~8,2f"
	    (mean-preference csp) (mean-conflict-count csp))
    (format stream "~% summed preference: ~8d  summed conflicts: ~8d"
	    (summed-preference csp) (summed-conflict-count csp))
    
    ))
|#







(defun SAA-HORIZON-CHECK (time)
  (if (>= (degrees (saa-horizon-angle time)) 
	  *default-xte-dark-limb-angle*)
      t nil))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun COMPUTE-SEGMENT ()
  
  (let* ((diff (1+ (- *xte-schedule-end* *xte-schedule-start*)))
	 (diff-int (round (/ diff *xte-segment-duration*)))
	 (diff-frac (- (/ diff *xte-segment-duration*) diff-int))
	 )
    (cond ((= diff-frac 0)
	   (setf *xte-segment-count* diff-int))
	  (t
	   (setf *xte-schedule-end* (+ *xte-schedule-end*
				       (round 
					(* *xte-segment-duration*
					   (- 1.0 diff-frac)))))
	   (setf *xte-segment-count* (+ diff-int 1)))
	  )
    )
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




#| ======================================================================
   O L D   C O D E
   ======================================================================
;;;  the next function is added 5-22-92

(defun LT-CURRENT-CONSTRAINT-PLOT (value)
  
  (let* ((var value)
	 (task (task var))
	 (chronika (chronika task))
	 
	 (start  (qtime-to-tjd chronika (qtime-start chronika)))
	 (start (jd-to-ymdhms  start))
	 (end  (qtime-to-tjd chronika (qtime-end chronika)))
	 (end (jd-to-ymdhms end))
	 
	 (start-date (format nil "~a-~a-~a" (third start) 
			     (month-number-to-month-string (second start)) (first start)))
	 (end-date (format nil "~a-~a-~a" (third end) 
			   (month-number-to-month-string (second end)) (first end)))
	 )
    
    (format t "~% name: ~a" (target-name task))
    (format t "~% ra:   ~a" (target-ra task))
    (format t "~%dec:   ~a" (target-dec task))
    (format t "~%start-date: ~a" start-date)
    (format t "~%end-date  : ~a" end-date)
    
    (plot-xte-target-constraints
     :name		(target-name task)
     :ra		(target-ra task)
     :dec		(target-dec task)
     :start-date	start-date
     :end-date	end-date
     :orbit-model	"xte"
     :file		"temp/lt-constraint.ps")
    
    (format t "~%Finish Saving")
    ))


;; removed and fixed usage in climif-xte 10/10/92 MDJ
(defun REPORT-CAPACITY-USE ()
  (with-open-file (stream "temp/capacity-usage"
			  :direction :output
			  :if-does-not-exist :create
			  :if-exists :supersede)
		  (let ((*standard-output* stream))
		    
		    (bdr-capacity-list *xte-short-term-schedule*
				       :only-used-bins nil)
		    )))

;;; moved 10/10/92 to xte-csp.lisp by MDJ
;;; and renamed

(defun PRINT-SHORT-TERM-REPORT (chronika csp)
  
  (with-open-file (stream "temp/short-term-report"
			  :direction :output
			  :if-does-not-exist :create
			  :if-exists :supersede)
		  (let ((*standard-output* stream))
		    
		    
		    (setf short-tarm-target-list nil)
		    (with-current-obs-csp-chronika
		     csp chronika
		     (do-for-all-vars
		      :var v :csp csp
		      
		      :predicate (has-assigned-value-p v)
		      :form (let* ((name (target-name (task v)))
				   )
			      (push (list name )
				    short-term-target-list)))
		     
		     (format stream "~%Target Information")
		     (format stream  "~%name      ")
		     (format stream "~%")
		     
		     (dolist (target short-term-target-list)
			     (format stream "~%~15a ~13a ~13a ~13a ~13a"
				     (first target)
				     (second target)
				     (third target)
				     (fourth target)
				     (fifth target))))
		    
		    (format stream "~% ")
		    
		    (schedule-report (obs-csp *xte-short-term-schedule*)
				     :stream stream)
		    
		    )))

|#

;;;-*-Mode:LISP; Syntax: Common-Lisp;Package: climif;-*-
;;;
;;; climif-XTE.lisp
;;;
;;; XTE CLIM spike user interface
;;;
;;; Jun/Jul92 MDJ
;;; thru Oct92...
;;;
;;;
;;; ======================================================================
;;;                                         PACKAGE

(eval-when (compile load eval)
	   (in-package climif))


;;; ======================================================================
;;;                                         GLOBALS

(defvar *THE-XTE-LT-CSP-SCHEDULE-FRAME* nil)

(defvar *THE-XTE-ST-CSP-SCHEDULE-FRAME* nil)


;;; ======================================================================
;;;                                         EXTEND FOCUS CRITERIA

(defvar *XTE-FOCUS-SPECS* nil)

;;; ======================================================================
;;;                                         FOCUS ADDITIONS


(setf *XTE-FOCUS-SPECS*
      (append 
       *FOCUS-SPECS*
       '(
	 (("Assigned to bin"
	   (clim:null-or-type integer)
	   
	   (lambda (v bin)
	     ;; if no bin specified, then return t
	     (cond ((not bin) t)
		   ((not (typep v 'user::xte-var)) t)
		   (t (and (typep v 'user::xte-var)
			   (user::has-assigned-value-p v)
			   (equal bin
				  (user::value-to-bin 
				   (user::csp v)
				   (user::obs-cap-constraint 
				    (user::obs-csp-chronika (user::csp v)))
				   v 
				   (user::assigned-value v)))))))
	   ))
	 
	 )))




;;; ======================================================================
;;;                                         XTE-CSP-SCHEDULE-FRAME

;;; parent class for XTE CSP schedule frames, both long and short

(defclass XTE-CSP-SCHEDULE-FRAME 
  
  ;; inherit from:
  (CSP-SCHEDULE-FRAME-MIXIN
   CSP-SCHEDULE-MOD-MANAGER
   CSP-SCHEDULE-BIN-CACHE
   AUTO-RUN-SET
   )
  
  
  ()
  )


;;; --- methods: override for class XTE-CSP-SCHEDULE-FRAME

(defmethod DO-COM-SAVE ((frame XTE-CSP-SCHEDULE-FRAME))
  (let ((root (user::obs-csp-chronika-name user::*xte-long-term-schedule*)))
    ;; save parameters to file, then load the file
    (with-no-standard-output
     (user::save-xte-schedule-to-file :occ user::*xte-long-term-schedule*)
     (user::restore-xte-schedule-from-file root)
     )
    (clim:notify-user
     clim:*application-frame* 
     (format nil "Schedule file ~s was saved" root))     
    ))

(defmethod DO-COM-SAVE-AS ((frame XTE-CSP-SCHEDULE-FRAME))
  (let ((stream (clim:get-frame-pane frame 'pointer-doc))
	(answer nil)
	(old-files (user::xte-sched-files)))
    
    (clim:accepting-values 
     (stream :resynchronize-every-pass t :own-window t
	     :label "Save as...")
     (format stream "Current file is: ~a"
	     (slot-value user::*xte-long-term-schedule*
			 'user::obs-csp-chronika-name))
     (cond (old-files
	    (format stream "~%Other schedule files are:")
	    (dolist (old-file old-files)
		    (format stream "~%   ~a" old-file)))
	   (t (format stream "~%   -none-")))
     (format stream "~%Save schedule to a new file")
     (fresh-line stream)
     
     (setf answer
	   (clim:accept '(clim:null-or-type string) 
			:default answer
			:prompt "named" :stream stream))
     
     (when (and answer 
		(member answer old-files :test #'string-equal))
	   (clim:with-text-face (:bold stream)
				(format stream "~%File already exists, choose another name"))
	   )
     
     ;; fix the string up: no spaces, upper case, etc.
     (if answer 
	 (setf answer (fix-string-for-sched-file-name answer)))
     
     )
    
    (when (and answer
	       (not (member answer old-files :test #'string-equal)))
	  
	  ;; save schedule to file, then load the file
	  (with-no-standard-output
	   (setf (slot-value user::*xte-long-term-schedule*
			     'user::obs-csp-chronika-name)
		 answer)
	   (user::save-xte-schedule-to-file 
	    :occ user::*xte-long-term-schedule*
	    :root answer)
	   (user::restore-xte-schedule-from-file answer)
	   )
	  (clim:notify-user
	   clim:*application-frame* 
	   (format nil "Schedule file ~s was saved" answer))
	  (do-xte-save-roster clim::*application-frame* 
			      :root answer)
	  )))

;;; call the same function, but with a specific table for focus
;;; criteria:

(defmethod DO-COM-FOCUS :AROUND ((frame XTE-CSP-SCHEDULE-FRAME)
				 &key focus-specs)
  (declare (ignore focus-specs))
  (call-next-method frame :focus-specs *XTE-FOCUS-SPECS*))


;;; --- override the DISPLAY-VAR-LIST method

(defmethod DISPLAY-VAR-LIST ((frame XTE-CSP-SCHEDULE-FRAME) stream)
  (with-slots
   (current-var focus-vars csp focus-criteria var-group var-groups
		var-group-size) frame
		
		(let* ((vars (second var-group))
		       (var nil)
		       (lh (+ 3 (clim:stream-line-height stream)))
		       (cy 0)
		       ;; this is a list with min (highest) priority first, max (lowest)
		       ;; priority second
		       (min-max-prio (if csp (user::min-max-priority csp) '(1 2)))
		       )
		  
		  (clim:updating-output
		   (stream :unique-id :display-var-list)
		   
		   (clim:formatting-table 
		    (stream)
		    
		    ;; title row
		    (clim:updating-output 
		     (stream :unique-id :title :cache-value var-group
			     :cache-test #'equal)
		     (clim:formatting-row
		      (stream)
		      (clim:formatting-cell (stream) 
					    (write-string " " stream))
		      (clim:formatting-cell (stream) 
					    (clim:with-text-face (:bold stream)
								 (clim:with-text-size (:very-small stream)
										      (write-string "Observation" stream))))
		      (clim:formatting-cell (stream) 	
					    (clim:with-text-face (:bold stream)
								 (clim:with-text-size (:very-small stream)
										      (write-string "Prio." stream))))
		      ))
		    
		    (dotimes (counter var-group-size)
			     (setf var (pop vars))
			     
			     (clim:formatting-row 
			      (stream)
			      
			      (clim:formatting-cell
			       (stream)
			       (clim:updating-output
				(stream :unique-id (cons counter :col0) :id-test #'equal
					:cache-value (if var
							 (list var (user::ignored-p var)
							       (user::has-assigned-value-p var)
							       (user::assigned-value-has-conflicts-p var)
							       (user::locked-p var)
							       (user::priority var))
						       nil)
					:cache-test #'equal)
				
				
				(cond
				 ((not var)) ;draw nothing
				 ((user::ignored-p var)
				  (clim:with-drawing-options
				   (stream :ink clim:+light-gray+)
				   (draw-var-circle
				    stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh) ; x y radius diagonal
				    :filled nil :diagonal nil))
				  )
				 (t
				  (draw-var-circle
				   stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh) ; x y radius diagonal
				   :filled   (user::has-assigned-value-p var)
				   :diagonal (user::assigned-value-has-conflicts-p var))))
				
				(if (and var (user::locked-p var))
				    (draw-var-lock 
				     stream (* 1.25 lh) (+ cy (* 0.75 lh)) (* 0.12 lh) (* 0.35 lh)))
				
				(if var
				    (draw-var-prio 
				     stream (* 1.75 lh) (+ cy (* 1.0 lh)) (* 0.75 lh) (* 0.25 lh) 
				     (let ((p (user::priority var)))
				       (cond ((= p (first min-max-prio)) 1)
					     ((= p (second min-max-prio)) 3)
					     (t 2)))))
				))
			      
			      
			      (clim:formatting-cell
			       (stream :align-x :left)
			       (clim:updating-output 
				(stream :unique-id (cons counter :col1) :id-test #'equal		  
					:cache-value (cons var (equal var current-var)) :cache-test #'equal)
				(when var
				      ;;   (if (equal var current-var)
				      ;;	   (clim:with-text-face (:bold stream) 
				      ;;       (clim:present var 'var :stream stream))
				      ;;     (clim:present var 'var :stream stream))
				      (clim:with-output-as-presentation 
				       (:object var :type 'var :stream stream 
						:single-box t :allow-sensitive-inferiors nil)
				       (clim:with-text-face
					((if (equal var current-var) :bold nil) stream)
					(format stream "~a (#~a)" (user::var-name var) (user::var-number var))))
				      )))
			      
			      (clim:formatting-cell
			       (stream :align-x :center)
			       (clim:updating-output 
				(stream :unique-id (cons counter :col2) :id-test #'equal
					:cache-value (cons var (if var (user::priority var)))
					:cache-test #'equal)
				(when var
				      (if (user::priority var)
					  (format stream "~d" (user::priority var))
					(write-string "-" stream)))))
			      )))))))

;;; --- override the VAR-DESCRIP-TO-STREAM method for both short
;;; --- and long term schedules (both use the same method, specialized
;;; --- for XTE)

(defmethod VAR-DESCRIP-TO-STREAM ((frame XTE-CSP-SCHEDULE-FRAME)
				  stream current-var)
  "write desription of current-var to stream.  Updating output
           is handled by caller"
  
  (let* ((task (user::task current-var))
	 )
    ;; --- title
    (clim:with-text-face (:bold stream)
			 (format stream "Var: ~a (#~d), Target name: ~a, "
				 (or (user::var-name current-var) "-")
				 (user::var-number current-var)
				 (user::target-name task)))
    
    ;; --- ra & dec
    (format stream "~%RA: ~,3f (~a), Dec: ~,3f (~a)"
	    (user::target-ra task)
	    (let ((hms (user::degrees-of-ra-to-hms 
			(user::target-ra task))))
	      (format nil "~dh ~2,'0dm ~,1fs" 
		      (first hms) (second hms) (third hms)))
	    (user::target-dec task)
	    (let ((dms (user::degrees-of-dec-to-dms 
			(user::target-dec task))))
	      (format nil "~dd ~2,'0dm ~ds" 
		      (first dms) (second dms) 
		      (round (third dms)))))
    
    ;; --- exposure time
    (format stream "~%Exp. time ~a sec, Priority ~a, " 
	    (user::exp-time task)
	    (user::priority current-var))
    
    ;; --- assignment
    (cond ((user::assigned-value current-var)
	   (format stream "~%Assigned to ~a (time ~a = ~a), ~d confl (min confl: ~a), pref: ~a"
		   (user::assigned-value current-var)
		   (user::assigned-time current-var)
		   (user::task-formatted-abs-qtime 
		    task (user::assigned-time current-var))
		   (user::conflicts-on-assigned-value current-var)
		   (user::min-conflicts current-var)
		   (user::assigned-value-preference current-var)))
	  (t (format stream "~%No assigned value, min confl: ~a"
		     (user::min-conflicts current-var))))
    
    
    ;; --- locked, ignored
    (format stream "~%~a ~a"
	    (if (user::locked-p current-var) "Locked" "Unlocked")
	    (if (user::ignored-p current-var) "Ignored" "Unignored"))
    ;; --- number of values in domain
    (format stream ", values total ~a, in domain ~a"
	    (user::value-count current-var)
	    (user::values-in-domain-count current-var))
    ))    

;;; -- override method for XTE schedule status, both long and short term

(defmethod SCHEDULE-STATUS-TO-STREAM ((frame XTE-CSP-SCHEDULE-FRAME) stream
				      &key state)
  
  ;; :state is results of call to CSP-state-summary on the csp
  
  (with-slots (csp) frame
	      
	      (format stream "~d Observations" (length (user::all-vars csp)))    
	      (fresh-line stream)
	      (format stream "of which:")
	      (fresh-line stream)
	      (format stream "  ~d locked" (length (user::locked-var-list csp)))
	      (fresh-line stream)
	      (format stream "  ~d ignored" (length (user::ignored-var-list csp)))    
	      (fresh-line stream)
	      (format stream "leaving ~d, of which:"
		      (- (length (user::all-vars csp))
			 (+ (length (user::locked-var-list csp))
			    (length (user::ignored-var-list csp)))))
	      (fresh-line stream)
	      (format stream " ~d are assigned"
		      (user::get-csp-state-summary-value 
		       state :type :assigned-var-count :default 0))
	      (fresh-line stream)
	      
	      (let ((conf (user::get-csp-state-summary-value 
			   state :type :conflicting-var-count :default 0)))
		(cond ((= 0 conf)
		       (format stream "No conflicts"))
		      (t (format stream "~d conflicts" conf))))
	      (fresh-line stream)
	      
	      
	      (format stream "~8,3f mean conflicts"
		      (user::get-csp-state-summary-value state :type :mean-conflicts :default 0))
	      (fresh-line stream)
	      (format stream "~8,3f mean preference"
		      (user::get-csp-state-summary-value state :type :mean-preference :default 0))
	      
	      ))

(defmethod DO-COM-CSP-SCHEDULE-HELP ((frame XTE-CSP-SCHEDULE-FRAME))
  (run-help-browser-frame :which *XTE-HELP*)
  )


;;; ======================================================================
;;;                                         PRESENTATION TYPES


;;; ======================================================================
;;;                                         FRAMES

;;; LONG TERM FRAME

(clim:define-application-frame XTE-CSP-LT-SCHEDULE-FRAME
			       
			       (XTE-CSP-SCHEDULE-FRAME
				CLIM:APPLICATION-FRAME)		; superclasses
			       
			       ()					; slots
			       
			       (:command-table (XTE-CSP-LT-SCHEDULE-COMMAND-TABLE
						:inherit-from (csp-schedule-command-table
							       ;; separately defined commands
							       csp-schedule-file-commands
							       XTE-csp-lt-schedule-var-commands)
						:menu (("File"      :menu    csp-schedule-file-commands)
						       ("Help"      :command (com-csp-schedule-help))
						       ("Exit"      :command (com-csp-schedule-exit))
						       ("ReDisplay" :command (com-csp-schedule-redisplay))
						       ;; redraw is for debugging only
						       ("ReDraw"    :command (com-csp-schedule-redraw))
						       )))
			       
			       (:command-definer DEFINE-XTE-CSP-LT-SCHEDULE-COMMAND)
			       
			       (:panes
				((menu             :command-menu)
				 
				 (var-list-control :application
						   :scroll-bars              nil
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-list-control)
				 
				 (var-list         :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-list
						   )
				 
				 (var-descrip      :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   t
						   :display-function         'display-var-descrip
						   )
				 
				 (var-menu         :application
						   :scroll-bars              :vertical
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-menu
						   )
				 
				 (schedule-status  :application	; text description of state
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   t
						   :display-function         'display-schedule-status
						   )
				 
				 (schedule-picture :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   nil
						   :display-function         'display-schedule-picture
						   )
				 
				 (auto-status      :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :very-small)
						   :incremental-redisplay    nil
						   :display-after-commands   nil
						   :display-function         nil
						   )
				 
				 (pointer-doc      :pointer-documentation 
						   :scroll-bars nil
						   :default-text-style '(:sans-serif :roman :very-small)
						   )
				 
				 ))
			       
			       
			       ;; the next layout is added 9-9-92
			       (:layout
				((default
				   (:column 1
					    (menu :compute)
					    (:row 0.45
						  (:column 3/5
							   (var-list-control :compute)
							   (var-list 0.55)
							   (var-descrip :rest))
						  (schedule-status 1/5)
						  (var-menu :rest)
						  )
					    (auto-status 1/8)
					    (schedule-picture :rest)
					    (pointer-doc :compute))) ; end default layout
				 ))
			       ;;
			       
			       )

;;; SHORT TERM FRAME

(clim:define-application-frame XTE-CSP-ST-SCHEDULE-FRAME
			       
			       (XTE-CSP-SCHEDULE-FRAME 
				CLIM:APPLICATION-FRAME)		; superclasses
			       
			       ()					; slots
			       
			       (:command-table (XTE-CSP-ST-SCHEDULE-COMMAND-TABLE
						:inherit-from (csp-schedule-command-table
							       ;; separately defined commands
							       csp-schedule-file-commands
							       XTE-csp-st-schedule-var-commands)
						:menu (("File"      :menu    csp-schedule-file-commands)
						       ("Help"      :command (com-csp-schedule-help))
						       ("Exit"      :command (com-csp-schedule-exit))
						       ("ReDisplay" :command (com-csp-schedule-redisplay))
						       ;; redraw is for debugging only
						       ("ReDraw"    :command (com-csp-schedule-redraw))
						       )))
			       
			       (:command-definer DEFINE-XTE-CSP-ST-SCHEDULE-COMMAND)
			       
			       (:panes
				((menu             :command-menu)
				 
				 (var-list-control :application
						   :scroll-bars              nil
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-list-control)
				 
				 (var-list         :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-list
						   )
				 
				 (var-descrip      :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   t
						   :display-function         'display-var-descrip
						   )
				 
				 (var-menu         :application
						   :scroll-bars              :vertical
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    t
						   :display-after-commands   t
						   :display-function         'display-var-menu
						   )
				 
				 (schedule-status  :application	; text description of state
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   t
						   :display-function         'display-schedule-status
						   )
				 
				 (schedule-picture :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :small)
						   :incremental-redisplay    nil
						   :display-after-commands   nil
						   :display-function         'display-schedule-picture
						   )
				 
				 (auto-status      :application
						   :scroll-bars              :both
						   :default-text-style       '(:sans-serif :roman :very-small)
						   :incremental-redisplay    nil
						   :display-after-commands   nil
						   :display-function         nil
						   )
				 
				 (pointer-doc      :pointer-documentation 
						   :scroll-bars nil
						   :default-text-style '(:sans-serif :roman :very-small)
						   )
				 
				 ))
			       
			       ;; the next layout is added 9-9-92
			       (:layout
				((default
				   (:column 1
					    (menu :compute)
					    (:row 0.45
						  (:column 3/5
							   (var-list-control :compute)
							   (var-list 0.55)
							   (var-descrip :rest))
						  (schedule-status 1/5)
						  (var-menu :rest)
						  )
					    (auto-status 1/8)
					    (schedule-picture :rest)
					    (pointer-doc :compute))) ; end default layout
				 ))
			       
			       )

;;; ======================================================================
;;;                                         COMMAND TABLES AND COMMANDS

;;; ----------------------------------------------------------------------
;;;              Additional commands common to short and long term tables.

;; Code to install Select button in long & short term schedule windows.

(defmethod DO-COM-SELECT ((frame CSP-SCHEDULE-FRAME-MIXIN))
  " Display all .sched files, and let user select one schedule"
  (cond 
   ((null (user::xte-sched-files))
    (clim:notify-user clim:*application-frame*
		      "No schedule files exist"))
   (t
      (let ((stream (clim:get-frame-pane clim:*application-frame* 'pointer-doc))
	    (choice nil)
	    (sched-name "")
	    (old-files (user::xte-sched-files)))
	
	(clim:accepting-values 
	    (stream :resynchronize-every-pass t :own-window t)
	  (write-string "Select an existing schedule" stream)
	  (fresh-line stream)
	  
	  (setf choice
	    (clim:accept (cons 'clim:member old-files)
			 :default choice
			 :prompt "Choice"
			 :stream stream))
	  
	  )
	
	(when choice
	  ;; choice now bound to name
	  (setf sched-name
	    (subseq choice
		    0 
		    (search ".sched" choice :from-end t)))

	  ;; record selection
	  (with-open-file (ss-stream "sched/saved-scheds.current"
			   :direction :output
			   :if-exists :append
			   :if-does-not-exist :create)
	    (format ss-stream "SELECTED ~a~%" sched-name))
	  )))))

(clim:define-command (COM-SELECT
    :command-table CSP-SCHEDULE-VAR-COMMANDS
    :menu "Select schedule"
    :name "Mark one schedule as the one selected for commanding")
    ()
    (do-com-select clim:*application-frame*))


;;; ----------------------------------------------------------------------
;;;                                 Additional Long-term schedule commands

(clim:define-command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
			   :inherit-from (CSP-SCHEDULE-VAR-COMMANDS))


(clim:define-command (COM-PRINT-LT-SCHEDULE-REPORT
		      :command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
		      :menu "Schedule Report"
		      :name "Print the schedule")
		     ()
		     (with-slots (csp current-var) clim:*application-frame*
				 ;;     long-term schedule reporting	
				 (let* (
					(csp (if current-var (user::csp current-var)))
					(filename (translate-logical-pathname
						   "spike:report;long-term-report.text"))
					)
				   
				   (with-open-file (stream filename
							   :direction :output
							   :if-does-not-exist :create
							   :if-exists :supersede)
						   (user::XTE-long-term-report csp :stream stream))
				   (clim:notify-user
				    clim:*application-frame*
				    (format nil "Reported to ~a" filename)))))



(clim:define-command (COM-PLOT-XTE-LT-CSP
		      :command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
		      :menu "Schedule plot"
		      :name "Plot schedule as postscript file")
		     ()
		     (with-slots (csp) clim:*application-frame*
				 (with-no-standard-output
				  (user::generate-XTE-csp-plot
				   (translate-logical-pathname "spike:temp;XTE-long.ps")
				   :obs-csp-chronika (user::obs-csp-chronika csp)))))


(clim:define-command (COM-CONSTRAINT-LT-PRINT-OUT
		      :command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
		      :menu "Target Constraint Plot"
		      :name "Target constraint plot: interactive, or as postscript file")
		     ()
		     (with-slots (csp current-var) clim:*application-frame*
				 
				 (let* ((task (user::task current-var))
					(chronika (user::chronika task))
					(qstart (user::qtime-start chronika))
					(qend (user::qtime-end chronika))
					(tjd-start (user::qtime-to-tjd chronika qstart))
					(tjd-end (user::qtime-to-tjd chronika qend))
					(choice
					 (catch :cancel
					   (clim:menu-choose
					    '(("Interactive" :value :interactive)
					      ("Postscript file" :value :postscript)
					      ("Both" :value :both))))))
				   
				   (when (or (eq choice :both) (eq choice :interactive))
					 (run-XTE-lt-constraints-frame :task (user::id task)))
				   
				   (when (or (eq choice :both) (eq choice :postscript))
					 (let ((filename (translate-logical-pathname
							  "spike:temp;lt-constraint.ps")))
					   (user::plot-XTE-target-constraints
					    :name		(user::target-name task)
					    :ra		(user::target-ra task)
					    :dec		(user::target-dec task)
					    :start-date	tjd-start
					    :end-date	tjd-end
					    :orbit-model	"XTE"
					    :file	filename)
					   (clim:notify-user
					    clim:*application-frame*
					    (format nil "Plot is file: ~a" filename)))
					 
					 ))))


(clim:define-command (COM-MAKE-SHORT-TERM
		      :command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
		      :menu "Short-term schedule"
		      :name "Make or show short-term schedule")
		     ()
		     (cond
		      ;; existing schedule to show?
		      (user::*XTE-SHORT-TERM-SCHEDULE*
		       (let ((choice 
			      (catch :cancel
				(clim:menu-choose '(("View existing schedule" :value :old)
						    ("Create new schedule"    :value :new))))))
			 (cond ((eq choice :old)
				(do-com-show-short-term clim:*application-frame*)
				)
			       ((eq choice :new)
				(do-com-make-short-term clim:*application-frame*))
			       (t nil))))
		      ;; no short term schedule, create new one?
		      (t  (do-com-make-short-term clim:*application-frame*))))


(defmethod DO-COM-MAKE-SHORT-TERM ((frame XTE-CSP-LT-SCHEDULE-FRAME))
  (with-slots (csp) frame
	      (let* ((bin-info (user::bin-list (user::obs-csp-chronika csp)))
		     (bin-choices nil))
		(dolist (bin bin-info)
			(push (list (format nil "Bin#~3d starts: ~a filled to: ~4,1f%"
					    (first bin)
					    (subseq
					     (user::format-abs-time (second bin))
					     0 11)
					    (sixth bin))
				    :value (first bin))
			      bin-choices))
		(let ((choice
		       (catch :cancel
			 (clim:menu-choose (reverse bin-choices)))))
		  
		  (when (numberp choice)
			;; make schedule
			(with-no-standard-output
			 (user::make-XTE-short-term-schedule choice))
			;; show it
			(run-XTE-csp-st-schedule-frame 
			 :csp (user::obs-csp user::*XTE-short-term-schedule*))
			)))))


(defmethod DO-COM-SHOW-SHORT-TERM ((frame XTE-CSP-LT-SCHEDULE-FRAME))
  (cond 
   (user::*XTE-SHORT-TERM-SCHEDULE*
    (run-XTE-csp-st-schedule-frame 
     :csp (user::obs-csp user::*XTE-short-term-schedule*))
    )
   (t 
    (clim:notify-user 
     frame "No short term schedule available"))))


(clim:define-command (COM-LOCK-BIN
		      :command-table XTE-CSP-LT-SCHEDULE-VAR-COMMANDS
		      :menu "Lock/unlock bin"
		      :name "Lock or unlock contents of bin")
		     ()
		     (with-slots (csp) clim:*application-frame*
				 (let* ((occ (user::obs-csp-chronika csp))
					(locked-bins (user::locked-bins occ))
					)
				   (cond (locked-bins
					  (let ((choice 
						 (catch :cancel
						   (clim:menu-choose '(("Lock bin"   :value :lock)
								       ("Unlock bin" :value :unlock))))))
					    (cond ((eq choice :lock)
						   (do-com-lock-bin clim:*application-frame*))
						  ((eq choice :unlock)
						   (do-com-unlock-bin clim:*application-frame*))
						  (t nil))))
					 (t ; no locked bins, only choice is to lock
					  (do-com-lock-bin clim:*application-frame*))))))	     


(defmethod DO-COM-UNLOCK-BIN ((frame XTE-CSP-LT-SCHEDULE-FRAME))
  (with-slots (csp) frame
	      (let* ((occ (user::obs-csp-chronika csp))
		     (locked-bins (user::locked-bins occ))
		     (bin-info (user::bin-list (user::obs-csp-chronika csp)))
		     (bin-choices nil))
		
		(when locked-bins
		      
		      ;; build menu
		      (dolist (bin bin-info)
			      (when (member (first bin) locked-bins)
				    (push (list (format nil "Bin#~3d starts: ~a filled to: ~4,1f%"
							(first bin)
							(subseq 
							 (user::format-abs-time (second bin))
							 0 11)
							(sixth bin))
						:value (first bin))
					  bin-choices)))
		      
		      (cond (bin-choices
			     ;; get choice
			     (let ((choice
				    (catch :cancel
				      (clim:menu-choose (reverse bin-choices)))))
			       
			       (when (numberp choice)
				     ;; lock
				     (user::unlock-bin user::*xte-long-term-schedule* choice)
				     ;; update display
				     (display-schedule-picture 
				      frame
				      (clim:get-frame-pane frame 'schedule-picture))
				     (clim:notify-user
				      frame "Unlocked bin ~a" choice))))
			    (t (clim:notify-user
				frame
				"No bins remain to be unlocked")))))))


(defmethod DO-COM-LOCK-BIN ((frame XTE-CSP-LT-SCHEDULE-FRAME))
  (with-slots (csp) frame
	      (let* ((occ (user::obs-csp-chronika csp))
		     (locked-bins (user::locked-bins occ))
		     (bin-info (user::bin-list (user::obs-csp-chronika csp)))
		     (bin-choices nil))
		
		;; build menu
		(dolist (bin bin-info)
			(when (not (member (first bin) locked-bins))
			      (push (list (format nil "Bin#~3d starts: ~a filled to: ~4,1f%"
						  (first bin)
						  (subseq 
						   (user::format-abs-time (second bin))
						   0 11)
						  (sixth bin))
					  :value (first bin))
				    bin-choices)))
		
		(cond (bin-choices
		       ;; get choice
		       (let ((choice
			      (catch :cancel
				(clim:menu-choose (reverse bin-choices)))))
			 
			 (when (numberp choice)
			       ;; lock
			       (user::lock-bin user::*xte-long-term-schedule* choice)
			       ;; update display
			       (display-schedule-picture 
				frame
				(clim:get-frame-pane frame 'schedule-picture))
			       ;; inform user
			       (clim:notify-user
				frame (format nil "Locked bin ~a" choice))
			       )))
		      (t (clim:notify-user
			  frame
			  "No bins remain to be locked"))))))

;;; ----------------------------------------------------------------------
;;;                                       Short-term commands: not in menu

;;; ----------------------------------------------------------------------
;;;                         Additional short-term var commands for Xte


(clim:define-command-table XTE-CSP-ST-SCHEDULE-VAR-COMMANDS
			   :inherit-from (CSP-SCHEDULE-VAR-COMMANDS))


(clim:define-command (COM-SHORT-TERM-OPTIMIZE
		      :command-table XTE-CSP-ST-SCHEDULE-VAR-COMMANDS
		      :menu "Optimize"
		      :name "Optimize short-term schedule to fill gaps")
		     ()
		     (with-slots (csp) clim:*application-frame*
				 (let ((state (user::csp-state-summary csp)))
				   
				   (user::no-optimize csp)
				   (user::short-term-optimize csp)
				   
				   
				   (clim:notify-user
				    clim:*application-frame*
				    (with-output-to-string
				     (stream)
				     (format stream "~%Before optimization:")
				     (let* (
					    (occ (user::obs-csp-chronika csp))
					    (chronika (user::obs-chronika occ))
					    (tjd-chronika-start (user::qtime-to-tjd chronika (user::qtime-start chronika)))
					    (tjd-chronika-end (user::qtime-to-tjd chronika (user::qtime-end chronika)))
					    (tjd-chronika-dur (- tjd-chronika-end tjd-chronika-start))
					    (min-dur (user::get-csp-state-summary-value state :type :total-min-duration))
					    (sched-dur (user::get-csp-state-summary-value state :type :total-sched-duration))
					    (gaps (user::get-csp-state-summary-value state :type :total-gaps))
					    (opt-exp (user::optimized-total-exp-time csp))
					    (opt-oh  (user::optimized-total-overhead csp))
					    )
				       (format stream "~%  total min duration    : ~8,0f sec (~6,2f days) ~6,1f% of schedule"
					       (* min-dur 86400) min-dur
					       (* 100 (/ min-dur tjd-chronika-dur)))
				       (format stream "~%  total sched duration  : ~8,0f sec (~6,2f days) ~6,1f% of schedule"
					       (* sched-dur 86400) sched-dur
					       (* 100 (/ sched-dur tjd-chronika-dur)))
				       (format stream "~%  total gaps            : ~8,0f sec (~6,2f days) ~6,1f% of schedule"
					       (* gaps 86400) gaps
					       (* 100 (/ gaps tjd-chronika-dur)))
				       
				       (format stream "~%Optimized:")
				       (format stream "~%  total exp time        : ~8,0f sec (~6,2f days) ~6,1f% of schedule"
					       (* opt-exp 86400)
					       opt-exp (* 100 (/ opt-exp tjd-chronika-dur)))
				       (format stream "~%  total overhead        : ~8,0f sec (~6,2f days) ~6,1f% of schedule"
					       (* opt-oh 86400) opt-oh
					       (* 100 (/ opt-oh tjd-chronika-dur)))))))))


(clim:define-command (COM-PLOT-XTE-ST-CSP
		      :command-table XTE-CSP-ST-SCHEDULE-VAR-COMMANDS
		      :menu "Schedule Plot"
		      :name "Plot schedule as postscript file")
		     ()
		     (with-slots (csp) clim:*application-frame*
				 (with-no-standard-output
				  (user::generate-XTE-csp-plot-short-term
				   (translate-logical-pathname "spike:temp;XTE-short.ps")
				   :obs-csp-chronika (user::obs-csp-chronika csp)
				   :start-fraction 0 :end-fraction 1.0))))


(clim:define-command (COM-PRINT-ST-SCHEDULE-REPORT
		      :command-table XTE-CSP-ST-SCHEDULE-VAR-COMMANDS
		      :menu "Schedule Report"
		      :name "Print the schedule")
		     ()
		     (with-slots (csp current-var) clim:*application-frame*
				 ;;     short-term schedule reporting
				 (let* ((task (if current-var (user::task current-var)))
					(chronika (if task (user::chronika task)))
					(csp (if current-var (user::csp current-var)))
					(tjd-start (user::qtime-to-tjd chronika (user::qtime-start chronika)))
					(tjd-end   (user::qtime-to-tjd chronika (user::qtime-end chronika)))
					)
				   
				   (when (fboundp 'user::XTE-short-term-report)
					 (with-open-file (stream (translate-logical-pathname
								  "spike:report;short-term-report.text")
								 :direction :output
								 :if-does-not-exist :create
								 :if-exists :supersede)
							 (user::XTE-short-term-report csp :stream stream)))
				   
				   (when (fboundp 'user::saa-report)
					 (user::with-open-file (stream (translate-logical-pathname
									"spike:report;SAA-report.text")
								       :direction :output
								       :if-does-not-exist :create
								       :if-exists :supersede)
							       (user::saa-report tjd-start tjd-end :stream stream)))
				   
				   
				   (clim:notify-user
				    clim:*application-frame*
				    "Short-term schedule reports complete")
				   
				   )))


(clim:define-command (COM-MAKE-LONG-TERM-CONFORM
		      :command-table XTE-CSP-ST-SCHEDULE-VAR-COMMANDS
		      :menu "Fix long-term"
		      :name "Make long-term schedule conform to this short-term schedule")
		     ()
		     (with-slots (csp) clim:*application-frame*
				 (with-no-standard-output
				  (user::make-XTE-long-term-conform-to-short-term
				   :short-term-schedule (user::obs-csp-chronika csp)))
				 ;; kick the display
				 (run-XTE-csp-lt-schedule-frame 
				  :csp (user::obs-csp user::*xte-long-term-schedule*))
				 ;; notify
				 ;;(clim:notify-user 
				 ;; clim:*application-frame*
				 ;; "Made long-terms schedule conform to short-term")
				 ))






;;; ======================================================================
;;;                                         METHODS

(defmethod INITIALIZE-INSTANCE :AFTER 
  ((frame XTE-CSP-ST-SCHEDULE-FRAME) &rest args)
  (declare (ignore args))
  
  (init-csp-schedule-frame-mixin 
   frame
   :pane 'schedule-picture
   :page-left 20 :page-right 1100
   :wleft 0.10 :wright 0.95
   :page-height 250)
  )

(defmethod INITIALIZE-INSTANCE :AFTER 
  ((frame XTE-CSP-LT-SCHEDULE-FRAME) &rest args)
  (declare (ignore args))
  
  (init-csp-schedule-frame-mixin 
   frame
   :pane 'schedule-picture
   :page-left 20 :page-right 1100
   :wleft 0.10 :wright 0.95
   :page-height 250)
  
  )




;;; --- override method (short and long term) for var menu display

(defmethod ENABLE-DISABLE-VAR-MENU :AFTER ((frame XTE-CSP-LT-SCHEDULE-FRAME))
  (clim:enable-command 'COM-PRINT-LT-SCHEDULE-REPORT frame)
  (clim:enable-command 'COM-PLOT-XTE-LT-CSP frame)
  (clim:enable-command 'COM-CONSTRAINT-LT-PRINT-OUT frame)
  )

(defmethod DISPLAY-VAR-MENU ((frame XTE-CSP-LT-SCHEDULE-FRAME) stream)
  
  (enable-disable-var-menu frame)
  
  (draw-fixed-size-command-table 
   frame stream 'XTE-CSP-LT-SCHEDULE-VAR-COMMANDS 
   "Commands" :XTE-long-term-var-menu))


(defmethod ENABLE-DISABLE-VAR-MENU :AFTER ((frame XTE-CSP-ST-SCHEDULE-FRAME))
  
  (with-slots (csp) frame
	      
	      (if (user::optimizable-p csp)
		  (clim:enable-command 'COM-SHORT-TERM-OPTIMIZE frame)
		(clim:disable-command 'COM-SHORT-TERM-OPTIMIZE frame))
	      
	      (clim:enable-command 'COM-PRINT-ST-SCHEDULE-REPORT frame)
	      (clim:enable-command 'COM-PLOT-XTE-ST-CSP frame)
	      
	      ))

(defmethod DISPLAY-VAR-MENU ((frame XTE-CSP-ST-SCHEDULE-FRAME) stream)
  
  (enable-disable-var-menu frame)
  
  (draw-fixed-size-command-table 
   frame stream 'XTE-CSP-ST-SCHEDULE-VAR-COMMANDS 
   "Commands" :XTE-short-term-var-menu))


;;; May 1994 reb
;;; Generic method to write event roster, called as part of schedule save.
;;; Specialization to LTS frame does nothing, specialization to STS frame writes
;;; .events file as expected by XTE SPIKE wrapper.

(defgeneric do-xte-save-roster ((frame XTE-CSP-SCHEDULE-FRAME) &key root))

(defmethod do-xte-save-roster ((frame XTE-CSP-LT-SCHEDULE-FRAME) &key root)
  nil)

(defmethod do-xte-save-roster 
    ((frame XTE-CSP-ST-SCHEDULE-FRAME) &key root)
  (user::save-xte-roster-to-file :occ user::*XTE-SHORT-TERM-SCHEDULE* 
			   :evt-file-name root
			   :do-saa t :do-occult t :do-commits t)
  (clim:notify-user frame
		    (format nil "Roster file ~s was saved" root)))

;;; ----------------------------------------------------------------------
;;;                                         Long-term schedule picture


(defmethod DISPLAY-SCHEDULE-PICTURE ((frame XTE-CSP-LT-SCHEDULE-FRAME) stream)
  
  ;; version for long-term schedule
  (with-slots (current-var csp gwindow) frame
	      
	      (when current-var
		    
		    (clim:window-clear stream)
		    
		    (user::record-prolog gwindow)
		    
		    ;; explicitly draw scale
		    ;;(user::set-page-loc-of-drawing-rect gwindow page-left page-right 90 190)
		    ;;(user::set-loc-of-drawing-window gwindow wleft wright 0 1)
		    ;;(user::set-x-window-scale gwindow tjd-start tjd-end)
		    ;;(user::time-label gwindow tjd-start tjd-end :label t
		    ;;                  :min-pixels-for-tics 5
		    ;;                  :min-pixels-for-labels 20)
		    
		    (draw-time-scale
		     frame :stream stream :page-top 10 :page-bottom 240)
		    
		    (draw-preferences frame :stream stream
				      :page-bottom 165 :page-top 235
				      :suitability-scale user::*XTE-SUITABILITY-SCALE*)
		    
		    (clim:with-output-as-presentation 
		     (:object current-var
			      :type 'VAR-PICTURE
			      :stream stream
			      :single-box t)
		     (user::box-window gwindow :linewidth 1)
		     )
		    
		    (draw-efficiency frame :stream stream
				     :page-bottom 110 :page-top 145)
		    
		    (draw-resources frame :stream stream
				    :page-bottom 20 :page-top 80)
		    (draw-conflicts frame :stream stream
				    :page-bottom 85 :page-top 105)
		    
		    (draw-assignment frame :stream stream
				     :page-bottom 150 :page-top 160)
		    
		    (user::record-wrapup gwindow)
		    )))


;;; this differs from the standard method only by coloring the bin for
;;; locked bins

(defmethod DRAW-RESOURCES ((frame XTE-CSP-LT-SCHEDULE-FRAME) &key
			   stream page-top page-bottom 
			   )
  (declare (ignore stream))
  (with-slots (csp current-var 
		   resource-constraint
		   gwindow page-left page-right wleft wright
		   bin-to-x-array) frame
		   
		   (when (and current-var resource-constraint)
			 
			 (let* ((task (if current-var (user::task current-var)))
				(chronika (if task (user::chronika task)))
				(occ (user::obs-csp-chronika csp))
				;; (FDSIA (user::FDSIA task))
				(bin-count (user::bin-count resource-constraint))
				(bin-limit (user::bin-limit resource-constraint))
				(bin-usage (user::bin-usage resource-constraint))
				(max-res 0)
				(scale-factor 1.2)
				;; temporary lists to speed drawing
				(used-list nil)
				(over-list nil)
				)
			   
			   (user::set-page-loc-of-drawing-rect 
			    gwindow page-left page-right page-bottom page-top)      
			   (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
			   (user::set-y-window-scale gwindow 0 1)
			   (user::set-x-window-scale 
			    gwindow 
			    (- (user::qtime-start chronika) 0.5) 
			    (+ (user::qtime-end chronika) 0.5))
			   (user::box-window gwindow)
			   (user::record-font gwindow "Helv" 9)
			   (user::string-at-line-in-rect
			    gwindow :line-number 1 :font-size 9 :string "RESOURCE")
			   (user::string-at-line-in-rect
			    gwindow :line-number 2 :font-size 9 :string "USAGE")
			   
			   (dotimes (i bin-count)
				    (setf max-res (max max-res (aref bin-usage i) (aref bin-limit i))))
			   (setf max-res (* scale-factor max-res))
			   
			   ;; draw the limit bars
			   (dotimes (i bin-count)
				    (let* ((x1x2 (aref bin-to-x-array i))
					   (xleft (first x1x2))
					   (xright (second x1x2))
					   ;; (xwidth (- xright xleft))
					   (limit (aref bin-limit i))
					   (used (aref bin-usage i))
					   (ybottom (round (user::v-to-page-y gwindow 0)))
					   (ylimit (round (user::v-to-page-y gwindow (/ limit max-res))))
					   (yused (round (user::v-to-page-y gwindow (/ used max-res)))))
				      (draw-gwindow-rect
				       gwindow xleft ybottom xright ylimit :fill nil)
				      ;; save coordinates on used-list (green) and over-list (red) so don't
				      ;; have to switch gcontext too much
				      (when (> used 0)
					    (if (> used limit)
						;; add i here to keep the bin number (to tell if locked)
						(push (list i (1+ xleft) (- ybottom 1) (1- xright) yused) over-list)
					      (push (list i (1+ xleft) (- ybottom 1) (1- xright) yused) used-list)))))
			   
			   (when used-list
				 ;; draw green...
				 (dolist (item used-list)
					 (draw-gwindow-rect 
					  gwindow (second item) (third item) (fourth item) (fifth item)
					  :fill #+:CCL nil 
					  #-:CCL (if (user::bin-is-locked-p occ (first item))
						     clim:+dark-sea-green+ ; locked bin
						   clim:+spring-green+ ; unlocked bin
						   ))))
			   
			   (when over-list
				 ;; draw red... (gray for now)
				 (dolist (item over-list)
					 (draw-gwindow-rect 
					  gwindow (second item) (third item) (fourth item) (fifth item)
					  :fill #+:CCL 0.75 
					  #-:CCL (if (user::bin-is-locked-p occ (first item))
						     clim:+maroon+	; locked bin
						   clim:+red+	; unlocked bin
						   ))))
			   
			   (user::set-y-window-scale gwindow 0 scale-factor)
			   (user::label-y-axis
			    gwindow 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
			   (user::label-y-axis 
			    gwindow 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f"))
			 )))



(defmethod DRAW-ASSIGNMENT ((frame XTE-CSP-LT-SCHEDULE-FRAME) &key
			    stream page-top page-bottom 
			    )
  (declare (ignore stream))
  (with-slots 
   (current-var resource-constraint
		bin-to-x-array csp gwindow page-left page-right wleft wright)
   frame
   
   (when (and current-var resource-constraint)
	 
	 (let* ((task (user::task current-var))
		(chronika (user::chronika task))
		(assignment (user::assigned-value current-var))
		(current-bin
		 (if assignment (user::value-to-bin 
				 csp resource-constraint current-var assignment)))
		(x1x2 (if current-bin (aref bin-to-x-array current-bin)))
		)
	   
	   (user::set-page-loc-of-drawing-rect 
	    gwindow page-left page-right page-bottom page-top) ;LRBT
	   (user::set-loc-of-drawing-window 
	    gwindow wleft wright 0.0 1.0) ;LRBT
	   (user::set-x-window-scale 
	    gwindow 
	    (- (user::qtime-start chronika) 0.5) 
	    (+ (user::qtime-end   chronika) 0.5))
	   (user::set-y-window-scale gwindow 0 1)
	   (user::box-window gwindow)
	   
	   ;; draw GRAY for conflicts, black for OK???
	   (when assignment 
		 
		 (draw-gwindow-rect 
		  gwindow (first x1x2) (user::v-to-page-y gwindow 0) 
		  (second x1x2) (user::v-to-page-y gwindow 1) 
		  :frame t 
		  :fill (if (user::assigned-value-has-conflicts-p current-var) 
			    ;; different for Mac (B&W), Allegro sun (color)
			    #-:CCL clim:+red+          #+:CCL 0.75 
			    #-:CCL clim:+spring-green+ #+:CCL 0))
		 
		 ;;(user::plot-interval-list
		 ;; gwindow (list (list bin-start bin-end 1))
		 ;; :fill (if (user::assigned-value-has-conflicts-p current-var) 0.5 0))
		 
		 )))))


(defmethod DRAW-TIME-SCALE ((frame XTE-CSP-LT-SCHEDULE-FRAME) &key
			    stream page-top page-bottom)
  
  (with-slots (current-var gwindow page-left page-right wleft wright) frame
	      
	      (when current-var
		    (let* ((task (user::task current-var))
			   (chronika (user::chronika task))
			   (qstart (user::qtime-start chronika))
			   (qend (user::qtime-end chronika))
			   ;; for staggering the time labels for legibility
			   (time-label 0)
			   (time-dy 7)
			   (time-label-max 2)
			   ;; only put out ticks every so often
			   (min-x-tic-sep 20)
			   (last-x-tic nil)
			   )
		      
		      (user::set-page-loc-of-drawing-rect 
		       gwindow page-left page-right page-bottom page-top)
		      (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
		      (user::set-x-window-scale gwindow
						(- (user::qtime-start chronika) 0.5) 
						(+ (user::qtime-end chronika) 0.5))
		      (user::record-font gwindow "Helv" 9)
		      
		      (let ((xmin (round (user::t-to-page-x gwindow qstart))) ;; (round (x-to-page-horiz wleft))
			    (xmax (round (user::t-to-page-x gwindow qend)))   ;; (round (x-to-page-horiz wright))
			    )
			(user::record-line gwindow xmin page-top xmax page-top)
			(user::record-line gwindow xmin page-bottom xmax page-bottom)
			(user::loop-over-value-range
			 qtime qstart qend
			 (let ((x (round (user::t-to-page-x gwindow qtime))))
			   (user::record-line gwindow x page-top x (- page-top 2))
			   (user::record-line gwindow x page-bottom x (+ page-bottom 2))
			   
			   ;; labels
			   (when (or (null last-x-tic) (> x (+ last-x-tic min-x-tic-sep)))
				 (let* ((ut (user::jdut (user::qtime-to-tjd chronika qtime)))
					;; (doy (day-month-year-to-day-of-year (first ut) (second ut) (third ut)))
					(str (format nil "~2d/~d" (second ut) (round (first ut))))
					(str-width (clim::stream-string-width stream str)))
				   (user::record-line gwindow x page-bottom x (+ page-bottom 5))
				   (clim:draw-text* 
				    stream str
				    (- x (round str-width 2)) 
				    (+ page-bottom (+ 12 (* time-label time-dy))))
				   (incf time-label)
				   (if (> time-label time-label-max) (setf time-label 0))
				   (setf last-x-tic x)
				   ))
			   ))                         ; end loop over value range
			)))))

;;; ----------------------------------------------------------------------
;;;                                         Short-term schedule picture

(defmethod DISPLAY-SCHEDULE-PICTURE ((frame XTE-CSP-ST-SCHEDULE-FRAME) stream)
  ;; version for short-term schedule
  (with-slots (current-var csp gwindow) frame
	      
	      (when current-var
		    
		    (clim:window-clear stream)      
		    
		    (user::record-prolog gwindow)
		    
		    (draw-time-scale
		     frame :stream stream :page-top 10 :page-bottom 240)
		    
		    ;;		    (draw-saa
		    ;;		     frame :stream stream :page-top 135 :page-bottom 125)
		    
		    )
		    (draw-preferences frame :stream stream
				      :page-bottom 200 :page-top 235
				      :suitability-scale user::*XTE-SUITABILITY-SCALE*)
		    
		    (clim:with-output-as-presentation 
		     (:object current-var
			      :type 'VAR-PICTURE
			      :stream stream
			      :single-box t)
		     (user::box-window gwindow :linewidth 1)
		     )
		    (draw-conflicts frame :stream stream :page-bottom 170 :page-top 195)
		    
		    (draw-efficiency frame :stream stream
				     :page-bottom 15 :page-top 35)
		    
		    (draw-resources frame :stream stream
				    :page-bottom 38 :page-top 85)
		    
		    
		    ;; redraw if var or assignment changes
		    (draw-assignment frame :stream stream
				     :page-bottom 110 :page-top 120)
		    
		    
		    (user::record-wrapup gwindow)
		    ))
 

(defmethod DRAW-ASSIGNMENT ((frame XTE-CSP-ST-SCHEDULE-FRAME) &key
			      stream page-top page-bottom 
			      )
    (declare (ignore stream))
    (with-slots 
     (current-var csp gwindow page-left page-right wleft wright)
     frame
     
     (when current-var
	   (let* ((task (if current-var (user::task current-var)))
		  (val (user::assigned-value current-var))
		  (start-end (if (and current-var val)
				 (multiple-value-list 
				  (user::FDSIA-val-to-time 
				   (user::FDSIA task) val))))
		  (chronika (if task (user::chronika task)))
		  startx endx y1 y2)
	     
	     (user::set-page-loc-of-drawing-rect
	      gwindow page-left page-right page-bottom page-top)
	     (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
	     (user::set-y-window-scale gwindow 0 1)
	     (user::set-x-window-scale
	      gwindow (- (user::qtime-start chronika) 0.5) 
	      (+ (user::qtime-end chronika) 0.5))
	     
	     ;;   the next two lines are moved from "(when val..." loop 9-4-92
	     (setf y1 (round (user::v-to-page-y gwindow 0.25)))
	     (setf y2 (round (user::v-to-page-y gwindow 0.75)))
	     
	     (when val
		   
		   ;; bounding start/end
		   (setf startx (round (user::t-to-page-x gwindow (first  start-end))))
		   (setf endx   (round (user::t-to-page-x gwindow (second start-end))))
		   ;; color based on conflicts:
		   ;; (if (> (user::conflicts-on-value current-var val) 0) "red" "green")
		   (draw-gwindow-rect
		    gwindow startx y1 endx y2
		    :frame t 
		    :fill (if (user::assigned-value-has-conflicts-p current-var) 
			      ;; different for Mac (B&W), Allegro sun (color)
			      #-:CCL clim:+red+          #+:CCL 0.75 
			      #-:CCL clim:+spring-green+ #+:CCL 0))
		   ;; name will be plotted below
		   ;;(user::record-leftshow gwindow startx (+ y1 10) 
		   ;;			 (user::string-it (user::var-name current-var)))
		   )				;end when val
	     
	     ;; draw other vars
	     (let ((dy 7)			;y step between rows
		   (n-stagger 1)		;which row we're on
		   (stagger-max 3)		;max number of rows
		   (sorted-vars (user::safe-sort
				 (user::all-assigned-vars csp)
				 #'<
				 :key #'user::assigned-time))
		   )
	       (dolist (var sorted-vars)
		       (let ((start-end (multiple-value-list (user::assigned-time var)))
			     )
			 (setf startx (round (user::t-to-page-x gwindow (first  start-end))))
			 (setf endx   (round (user::t-to-page-x gwindow (second start-end))))
			 (draw-gwindow-rect
			  gwindow startx y1 endx y2
			  :frame t 
			  :fill (if (user::assigned-value-has-conflicts-p var) 
				    ;; different for Mac (B&W), Allegro sun (color)
				    #-:CCL clim:+red+          #+:CCL 0.75 
				    #-:CCL clim:+spring-green+ #+:CCL 0))
			 (user::record-leftshow gwindow startx (+ y1 9 (* (1- n-stagger) dy))
						(user::string-it (user::var-name var)))
			 (incf n-stagger)
			 (if (> n-stagger stagger-max) (setf n-stagger 1))
			 
			 )))
	     
	     ;; this method draws the individual orbit commitments of an activity 
	     ;; when scheduled.  It uses the detailed breakdown recorded for Xte,
	     ;; needed for the BDR constraint
	     (when (and current-var val (typep task 'user::XTE-short-term-task))
		   (setf y1 (round (user::v-to-page-y gwindow 0)))
		   (setf y2 (round (user::v-to-page-y gwindow 1)))
		   (let ((details (user::detail-intervals task (first start-end))))
		     (dolist (int details)
			     ;; int is list (start end)
			     (setf startx (round (user::t-to-page-x gwindow (first  int))))
			     (setf endx   (round (user::t-to-page-x gwindow (second int))))
			     (draw-gwindow-rect
			      gwindow startx y1 endx y2
			      :frame t 
			      :fill nil))))
	     
	     ))))

#|
(defmethod DRAW-SAA ((frame XTE-CSP-ST-SCHEDULE-FRAME) 
		     &key stream page-top page-bottom 
		     )
  ;; (declare (ignore stream))
  (with-slots 
   (current-var gwindow page-left page-right wleft wright)
   frame
   
   (when current-var
	 (let* (
		;; (csp (user::csp current-var))
		;; (occ (user::obs-csp-chronika csp))
		;; (passes (user::saa-passes occ))
		(task (if current-var (user::task current-var)))
		(chronika (if task (user::chronika task)))
		(qstart (user::qtime-start chronika))
		(qend (user::qtime-end chronika))
		(tjd-start (user::qtime-to-tjd chronika qstart))
		(tjd-end (user::qtime-to-tjd chronika qend))
		startx endx y1 y2)
	   
	   (user::set-page-loc-of-drawing-rect
	    gwindow page-left page-right page-bottom page-top)
	   (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
	   (user::set-y-window-scale gwindow 0 1)
	   (user::set-x-window-scale gwindow TJD-start TJD-end) 
	   (user::record-font gwindow "Helv" 9)
	   (clim:with-drawing-options (stream :ink clim:+magenta+)
				      (user::string-at-rect-left-window-bottom 
				       gwindow :string "SAA"))
	   
	   (dotimes (count user::*saa-count*)
		    (let* ((saa-list-check (gethash count user::*saa-list*))
			   (start (coerce (first saa-list-check) 'single-float))
			   (end   (coerce (second saa-list-check) 'single-float))
			   (saa-check (third saa-list-check))
			   )
		      ;; bounding start/end
		      (setf startx (round (user::t-to-page-x gwindow start)))
		      (setf endx   (round (user::t-to-page-x gwindow end  )))
		      (setf y1 (round (user::v-to-page-y gwindow 0)))
		      (setf y2 (round (user::v-to-page-y gwindow 1)))
		      
		      (if (eql saa-check t)
			  (draw-gwindow-rect
			   gwindow startx y1 endx y2
			   :frame nil
			   :fill #-:CCL clim:+magenta+ #+:CCL 0)
			
			(draw-gwindow-rect
			 gwindow startx y1 endx y2
			 :frame nil
			 :fill #-:CCL clim:+black+ #+:CCL 0)
			)
		      ))
	   
	   ))))
|#

(defmethod DRAW-TIME-SCALE ((frame XTE-CSP-ST-SCHEDULE-FRAME) &key
			    stream page-top page-bottom)
  
  (declare (ignore stream))
  (with-slots 
   (current-var gwindow page-left page-right wleft wright) 
   frame
   (when current-var
	 (let* ((task (user::task current-var))
		(chronika (user::chronika task))
		(qstart (user::qtime-start chronika))
		(qend (user::qtime-end chronika))
		(tjd-start (user::qtime-to-tjd chronika qstart))
		(tjd-end (user::qtime-to-tjd chronika qend))
		)
	   
	   (user::set-page-loc-of-drawing-rect 
	    gwindow page-left page-right page-bottom page-top)       
	   (user::set-loc-of-drawing-window
	    gwindow wleft wright 0 1)
	   (user::set-x-window-scale gwindow tjd-start tjd-end)
	   (user::record-font gwindow "Helv" 6)
	   
	   (let ((xmin (round (user::t-to-page-x gwindow tjd-start))) 
		 ;; (round (x-to-page-horiz wleft))
		 (xmax (round (user::t-to-page-x gwindow tjd-end  ))) 
		 ;; (round (x-to-page-horiz wright))
		 )
	     (user::record-line gwindow xmin page-top xmax page-top)
	     (user::record-line gwindow xmin page-bottom xmax page-bottom)
	     ;; draw tic marks
	     
	     (user::loop-over-time-in-range
	      :TJD-start tjd-start :TJD-end tjd-end
	      :TJD tjd :month month :day-of-month day-of-month
	      :year year :hour hour
	      :day-action
	      (let* ((x (round (user::t-to-page-x gwindow TJD)))
		     ;;(str (format nil "~2d/~2d/~2d" month day-of-month (- year 1900)))
		     ;;(str-width (clim::stream-string-width stream str))
		     )
		(user::record-line gwindow x page-top x (- page-top 6)) ;tic size 6 pixels
		(user::record-line gwindow x page-bottom x (+ page-bottom 6))
		;; (clim:draw-text* stream str (- x (round str-width 2)) (+ page-bottom 16))
		)
	      #|
           :hour-action 
           (let ((x (round (user::t-to-page-x gwindow TJD))))
             (user::record-line gwindow x page-top x (- page-top 2))
             (user::record-line gwindow x page-bottom x (+ page-bottom 2))
             (when (and (/= 0 hour) (= 0 (rem hour 6)))
               (let* ((str (format nil "~d" hour))
                      (str-width (clim::stream-string-width stream str))
                      )
                 ;;(draw-glyphs view (- x (round str-width 2)) (+ page-bottom 12) str)
                 (clim:draw-text* stream str (- x (round str-width 2)) (+ page-bottom 12))
                 ))
             )
|#	   
	      ))))))




;;; the next function is added 9-29-92

#| testing
(let*
    ((csp (user::obs-csp *XTE-short-term-schedule*))
     (var (user::var csp 0))
     (task (user::task var))
     (abs-int (user::tjd-abs-intervals task))
     (abs-pcf (user::pcf-from-interval-list abs-int))
     (start (user::tjd-start (user::obs-chronika *XTE-short-term-schedule*)))
     (end (user::tjd-end (user::obs-chronika *XTE-short-term-schedule*)))
     (asc-nodes (user::asc-node-list user::*XTE* start end))
     (effic-list (user::mean-PCF-over-partition abs-pcf asc-nodes))
     (pstart nil)
     (pend   nil)
     )
  
  (dolist (rate effic-list)
	  (setf pend (pop asc-nodes))
	  (when (and pstart
		     (>= pstart start)
		     (<= pend end))
		;; only draw if orbit is in the plot range
		(format t "~%~8,4f ~8,4f (~6,4f) effic: ~7,3f"
			pstart pend (- pend pstart) rate)
		)
	  (setf pstart pend)
	  )
  
  )
|#


(defmethod DRAW-EFFICIENCY ((frame XTE-CSP-ST-SCHEDULE-FRAME)
			    &key stream page-top page-bottom)
  
  ;; This function displays the visibility of the object specified by
  ;; current-var per orbit for the specified period
  
  (with-slots 
   (current-var gwindow page-left page-right wleft wright) frame
   
   (let* ((task     (user::task current-var))
	  (abs-int (user::tjd-abs-intervals task))
	  (abs-pcf (user::pcf-from-interval-list abs-int))
	  (start (user::tjd-start (user::obs-chronika user::*XTE-short-term-schedule*)))
	  (end (user::tjd-end (user::obs-chronika user::*XTE-short-term-schedule*)))
	  (asc-nodes (user::asc-node-list user::*XTE* start end))
	  (effic-list (user::mean-PCF-over-partition abs-pcf asc-nodes))
	  (pstart nil)               ;for orbit period times
	  (pend   nil)
	  startx endx y1 y2          ;for graphics calls
	  (scale-factor 1.2)
	  )
     
     
     ;;  Set the display space
     
     (user::set-page-loc-of-drawing-rect
      gwindow page-left page-right page-bottom page-top)
     (user::set-loc-of-drawing-window gwindow wleft wright 0 1)
     (user::set-y-window-scale gwindow 0 1)
     (user::set-x-window-scale gwindow start end)
     (user::record-font gwindow "Helv" 9)
     (clim:with-drawing-options (stream :ink clim:+blue+)
				(user::string-at-rect-left-window-bottom
				 gwindow :string "Visibility"))
     
     ;;  Start plotting
     
     (dolist (rate effic-list)
	     (setf pend (pop asc-nodes))
	     (when (and pstart
			(>= pstart start)
			(<= pend end))
		   ;; only draw if orbit is in the plot range
		   ;;(format t "~%~8,4f ~8,4f (~6,4f) ~7,3f" pstart pend (- pend pstart) rate)
		   (setf startx (round (user::t-to-page-x gwindow pstart)))
		   (setf endx   (round (user::t-to-page-x gwindow pend)))
		   (setf y1 (round (user::v-to-page-y gwindow 0)))
		   (setf y2 (round (user::v-to-page-y gwindow rate)))
		   (draw-gwindow-rect
		    gwindow startx y1 endx y2
		    :frame nil
		    :fill #-:CLL clim:+blue+ #+:CLL 0)
		   )
	     (setf pstart pend)
	     )
     
     (user::set-y-window-scale gwindow 0 scale-factor)
     (user::label-y-axis
      gwindow 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
     (user::label-y-axis
      gwindow 0 scale-factor :tic-value 0.5 :tic-size 3 
      :label t :label-format "~,1f")
     
     )					;let*
   )					;with-slots
  )


;;; ======================================================================
;;;                                         START APPLICATION

~

(defun MAKE-XTE-CSP-LT-SCHEDULE-FRAME (&key (root *clim-root*)
					    (force nil))
  (if force (setf *THE-XTE-LT-CSP-SCHEDULE-FRAME* nil))
  ;; only make new one if there isn't one bound to the global
  (unless *THE-XTE-LT-CSP-SCHEDULE-FRAME*
	  (setf *THE-XTE-LT-CSP-SCHEDULE-FRAME*
		(clim:make-application-frame 
		 'XTE-CSP-LT-SCHEDULE-FRAME
		 :parent root
		 :width #+:Allegro 800 #-:Allegro 600 
		 :height #+:Allegro 800 #-:Allegro 350
		 :pretty-name "XTE Long-Term Schedule"))))


(defun RUN-XTE-CSP-LT-SCHEDULE-FRAME (&key csp
					   (root *clim-root*)
					   (force nil))
  
  "create and start a long-term sched application frame.  
    Arguments:
     csp - the csp instance to connect to for display"
  
  (let* ((process-name "Xte Long-Term Schedule")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal))
	 )
    
    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
	  (if process 
	      #+:Allegro (mp:process-kill process)
	      #-:Allegro nil
	      )
	  (setf *THE-XTE-LT-CSP-SCHEDULE-FRAME* nil)
	  )
    
    ;; create frame if it doesn't exist
    (unless *THE-XTE-LT-CSP-SCHEDULE-FRAME*
      (make-XTE-csp-lt-schedule-frame :root root :force force))
    
    ;; clear out old saved-scheds file.
    (with-open-file (stream "sched/saved-scheds.current"
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
      )
    
    ;; store the slot contents
    (let* ((frame *THE-XTE-LT-CSP-SCHEDULE-FRAME*)
	   (changing-csp (not (equal (slot-value frame 'csp) csp))))
      (setf (slot-value frame 'var-group-size) 10)    
      (setf (slot-value frame 'csp) csp)
      (setf (slot-value frame 'focus-vars)
	    (copy-list (user::all-vars csp)))
      (setf (slot-value frame 'current-var) 
	    (first (user::all-vars csp)))
      (group-vars-for-display frame)
      
      ;; long-term: resource constraint is time capacity
      (setf (slot-value frame 'resource-constraint)
	    (user::obs-cap-constraint (user::obs-csp-chronika csp)))
      (setf (slot-value frame 'resource-segment-definition)
	    csp)
      
      ;; setup cache
      (init-graphics-cache frame)
      (update-view-cache-values frame)
      
      (do-com-schedule-redraw frame)
      
      (when changing-csp
	    (clim:window-clear (clim:get-frame-pane frame 'auto-status)))
      )
    
    ;; run it
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*THE-XTE-LT-CSP-SCHEDULE-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *THE-XTE-LT-CSP-SCHEDULE-FRAME*)
      ))))



(defun MAKE-XTE-CSP-ST-SCHEDULE-FRAME (&key (root *clim-root*)
					    (force nil))
  (if force (setf *THE-XTE-ST-CSP-SCHEDULE-FRAME* nil))
  ;; only make new one if there isn't one bound to the global
  (unless *THE-XTE-ST-CSP-SCHEDULE-FRAME*
	  (setf *THE-XTE-ST-CSP-SCHEDULE-FRAME*
		(clim:make-application-frame 
		 'XTE-CSP-ST-SCHEDULE-FRAME
		 :parent root
		 :width #+:Allegro 800 #-:Allegro 600 
		 :height #+:Allegro 800 #-:Allegro 350
		 :pretty-name "XTE Short-Term Schedule"))))


(defun RUN-XTE-CSP-ST-SCHEDULE-FRAME 
  (&key csp
	(capacity-constraint-name 'user::bdr-capacity)
	(root *clim-root*)
	(force nil))
  
  "create and start a Short-term sched application frame.  
    Arguments:
     csp - the csp instance to connect to for display"
  
  (let* ((process-name "Xte Short-Term Schedule")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal))
	 )
    
    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
	  (if process 
	      #+:Allegro (mp:process-kill process)
	      #-:Allegro nil
	      )
	  (setf *THE-XTE-ST-CSP-SCHEDULE-FRAME* nil)
	  )
    
    ;; create frame if it doesn't exist
    (unless *THE-XTE-ST-CSP-SCHEDULE-FRAME*
	    (make-XTE-csp-st-schedule-frame :root root :force force))
    
    ;; store the slot contents
    (let* ((frame *THE-XTE-ST-CSP-SCHEDULE-FRAME*)
	   (changing-csp (not (equal (slot-value frame 'csp) csp))))
      
      (setf (slot-value frame 'var-group-size) 10)    
      (setf (slot-value frame 'csp) csp)
      (setf (slot-value frame 'focus-vars)
	    (copy-list (user::all-vars csp)))
      (setf (slot-value frame 'current-var) 
	    (first (user::all-vars csp)))
      (group-vars-for-display frame)
      
      ;; Short-term: resource constraint
      (setf (slot-value frame 'resource-constraint)
	    (user::find-capacity-constraint csp capacity-constraint-name))
      (setf (slot-value frame 'resource-segment-definition)
	    (user::find-capacity-constraint csp capacity-constraint-name))
      
      ;; setup cache
      (init-graphics-cache frame)
      (update-view-cache-values frame)
      
      (do-com-schedule-redraw frame)
      
      (when changing-csp
	    (clim:window-clear (clim:get-frame-pane frame 'auto-status)))
      )
    
    ;; run it
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*THE-XTE-ST-CSP-SCHEDULE-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *THE-XTE-ST-CSP-SCHEDULE-FRAME*)
      ))))

#|
(describe *THE-XTE-lt-csp-schedule-frame*)
(inspect *THE-XTE-lt-csp-schedule-frame*)
(describe (user::obs-csp *xte-long-term-schedule*))
(describe *xte-long-term-schedule*)
(describe (user::obs-cap-constraint *xte-long-term-schedule*))
(user::obs-csp-chronika (user::obs-csp user::*xte-long-term-schedule*))
(user::reset-csp (obs-csp user::*xte-long-term-schedule*))
(user::min-max-priority  (obs-csp user::*xte-long-term-schedule*))
(user::csp-state-summary (obs-csp user::*xte-long-term-schedule*))


(run-XTE-csp-lt-schedule-frame 
 :csp (user::obs-csp user::*xte-long-term-schedule*))

(run-XTE-csp-st-schedule-frame 
 :csp (user::obs-csp user::*XTE-short-term-schedule*))



|#


;;; ======================================================================
;;;                                          TARGET CONSTRAINT WINDOW (LT)
;;; ======================================================================
;;; example of long-term target constraint plot on the screen

(clim:define-application-frame XTE-LT-CONSTRAINTS
			       
			       ()					; superclasses
			       
			       ;; state
			       ((gw :initform nil)			; gwindow instance
				(task :initform nil)
				)
			       
			       (:panes
				((menu :command-menu)
				 (status :application
					 :default-text-style '(:sans-serif :roman :large)
					 :display-function 'display-XTE-lt-constraints-status
					 :display-after-commands nil
					 :scroll-bars nil
					 )
				 (picture :application
					  :scroll-bars :both
					  :default-text-style '(:sans-serif :roman :large)
					  :display-function 'display-XTE-lt-constraints
					  :display-after-commands nil)
				 ))
			       
			       (:layout
				((default
				   (:column 1
					    (menu :compute)
					    (status :compute)
					    (picture :rest))))))


(defmethod INITIALIZE-INSTANCE :AFTER ((frame XTE-LT-CONSTRAINTS) &rest args)
  (declare (ignore args))
  (with-slots (gw) frame
	      (setf gw (user::make-clim-gwindow 
			:stream (clim:get-frame-pane frame 'picture) 
			:page-height 500))))


(defmethod DISPLAY-XTE-LT-CONSTRAINTS-STATUS 
  ((frame XTE-LT-CONSTRAINTS) stream)
  (format stream "Idle.")
  )


(defmethod DISPLAY-XTE-LT-CONSTRAINTS ((frame XTE-LT-CONSTRAINTS) stream)
  (declare (ignore stream))
  (with-slots (gw task) frame
	      
	      ;; Note: the record-... functions work in terms of device coordinates
	      ;; and so are not inverted for output.
	      ;; i.e. the origin is at window top left, and y runs down.
	      (when task
		    (let ((status-stream (clim:get-frame-pane frame 'status)))
		      (format status-stream "~%Computing...")
		      (user::stream-force-output status-stream)
		      
		      (user::PLOT-XTE-TASK-DETAILS user::*xte-long-term-schedule*
						   :id	task
						   :where    gw
						   :ptop 450 :pbottom 50
						   )
		      (format status-stream "done.")
		      (user::stream-force-output status-stream)))))


;;; --- redraw the picture

(defmethod REDISPLAY-XTE-LT-CONSTRAINTS-FRAME ((frame XTE-LT-CONSTRAINTS))
  (let ((stream (clim:get-frame-pane frame 'picture)))
    (clim:window-clear stream)
    (display-XTE-lt-constraints frame stream)))


;;; --- commands

(DEFINE-XTE-LT-CONSTRAINTS-COMMAND
  (COM-SELECT-TASK 
   :menu "Select Observation" 
   :name "Select which task to plot")
  ()
  (when (and (boundp 'user::*xte-long-term-schedule*)
	     user::*xte-long-term-schedule*)	     
	(let ((stream (clim:get-frame-pane clim::*application-frame* 'picture))
	      (choice
	       (catch :cancel
		 (clim:menu-choose
		  (mapcar #'user::id (user::task-list (user::obs-chronika 
						       user::*xte-long-term-schedule*)))))))	      
	  (unless (or (eq choice nil) (eq choice :cancel))
		  (setf (slot-value clim:*application-frame* 'task)
			choice)
		  (clim:window-clear stream)
		  (display-XTE-lt-constraints clim::*application-frame* stream)))))


(DEFINE-XTE-LT-CONSTRAINTS-COMMAND
  (COM-REFRESH 
   :menu "Redisplay" 
   :name "Redisplay")
  ()
  ;; on Mac, this seems to bring back dead scroll bars...
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'picture)))
    (clim:window-refresh stream)
    (user::stream-force-output stream)
    ))


(DEFINE-XTE-LT-CONSTRAINTS-COMMAND
  (COM-REDISPLAY
   :menu "Redraw" 
   :name "Redraw")
  ()
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'picture)))
    (clim:window-clear stream)
    (display-XTE-lt-constraints clim::*application-frame* stream)))


(DEFINE-XTE-LT-CONSTRAINTS-COMMAND 
  (COM-EXIT
   :menu "Exit" 
   :name "Exit") 
  ()
  (clim:frame-exit clim:*application-frame*))


;;; --- global frame variable

(defvar *XTE-LT-CONSTRAINTS-FRAME* nil)


;;; --- constructor and display functions

(defun MAKE-XTE-LT-CONSTRAINTS-FRAME (&key (root *clim-root*)
					   (force nil))
  
  ;; if force, kill the old one
  (if force (setf *XTE-LT-CONSTRAINTS-FRAME* nil))
  ;; only make a new one if there isn't one bound to the global
  (unless *XTE-LT-CONSTRAINTS-FRAME*
	  (setf *XTE-LT-CONSTRAINTS-FRAME* 
		(clim:make-application-frame 'XTE-LT-CONSTRAINTS :parent root
					     :width 600 :height 550
					     :pretty-name "Target Constraints"
					     ))))


(defun RUN-XTE-LT-CONSTRAINTS-FRAME (&key (task nil)
					  (root *clim-root*)
					  (force nil))
  
  (let* ((process-name "Xte Long-Term Target Constraints")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal)))
    
    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
	  (if process 
	      #+:Allegro (mp:process-kill process)
	      #-:Allegro nil
	      )
	  (setf *XTE-LT-CONSTRAINTS-FRAME* nil)
	  )
    
    ;; create frame if it doesn't exist
    (unless *XTE-LT-CONSTRAINTS-FRAME*
	    (make-XTE-lt-constraints-frame :root root :force force))
    
    ;; store task if requested
    (when task
	  (setf (slot-value *XTE-LT-CONSTRAINTS-FRAME* 'task) task)
	  (redisplay-XTE-lt-constraints-frame *XTE-LT-CONSTRAINTS-FRAME*))
    
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*XTE-LT-CONSTRAINTS-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *XTE-LT-CONSTRAINTS-FRAME*)
      ))))


#| TEST

(describe *XTE-LT-CONSTRAINTS-FRAME*)

(setf *XTE-LT-CONSTRAINTS-FRAME* nil)

;; startup, no tasks
(run-XTE-lt-constraints-frame)
;; force the display
(run-XTE-lt-constraints-frame :task 'user::u-gem)
(run-XTE-lt-constraints-frame :task 'user::tycho-num3)

(user::PLOT-XTE-TASK-DETAILS user::*xte-long-term-schedule*
			     :id	'user::u-gem
			     :file "temp/foo.ps"
			     ;; :ptop 10 :pbottom 500 ;mac
			     :ptop 500 :pbottom 50 ;mac
			     )
|#


;;; ======================================================================
;;;                                                XTE TOP-LEVEL WINDOW
;;; ======================================================================
;;; the tag "ADTL" below is "Xte Top-Level"


(clim:define-application-frame ADTL
			       
			       ()					;superclassess
			       
			       ;; application state variables
			       (;; name of sched file
				(sched-file-name :initform nil)
				)
			       
			       ;; command table, definer
			       (:command-table (ADTL-command-table
						:inherit-from (ADTL-file ADTL-schedule
									 )
						:menu (("File" :menu ADTL-file)
						       ("Schedule" :menu ADTL-schedule)
						       ("Help" :menu (com-help-ADTL))
						       ("Exit" :command (com-exit-ADTL)))))
			       
			       (:command-definer define-ADTL-command)  
			       
			       (:panes
				((menu :command-menu :default-text-style '(:sans-serif :roman :normal))
				 (interactor :application 
					     :scroll-bars :vertical
					     :default-text-style '(:sans-serif :roman :small))
				 (show-state :application 
					     :scroll-bars :vertical
					     :display-function 'display-show-state
					     :default-text-style '(:sans-serif :roman :normal))
				 (pdoc :pointer-documentation :scroll-bars nil
				       :default-text-style clim:*default-text-style*)
				 ))
			       
			       (:layout
				((default
				   (:column 1
					    (menu :compute)
					    (show-state :rest)
					    (interactor 0.2)
					    (pdoc :compute)))))
			       
			       )

;;; --- command table

(clim:define-command-table ADTL-file)


;;; --- commands

(clim:define-command (com-ADTL-new 
		      :command-table ADTL-file 
		      :menu "New"
		      :name "Create a new schedule file")
		     ()
		     (with-slots (sched-file-name) clim:*application-frame*
				 (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
				       (answer nil)
				       (old-files (user::xte-sched-files)))
				   
				   (clim:accepting-values 
				    (stream :resynchronize-every-pass t :own-window t
					    :label "New Schedule File")
				    (write-string "Create a new schedule file" stream)
				    (fresh-line stream)
				    
				    (setf answer
					  (clim:accept '(clim:null-or-type string) 
						       :default answer
						       :prompt "named" :stream stream))
				    
				    (when (member answer old-files :test #'string-equal)
					  (clim:with-text-face (:bold stream)
							       (format stream "~%File already exists, choose another name"))
					  )
				    
				    ;; fix the string up: no spaces, upper case, etc.
				    (if answer 
					(setf answer (fix-string-for-sched-file-name answer)))
				    
				    )
				   
				   (when (and answer
					      (not (member answer old-files :test #'string-equal)))
					 
					 ;; edit the parameter settings and save the file
					 (edit-xte-schedule-parameters stream answer)
					 
					 ;; save parameters to file, then load the file
					 (user::save-xte-schedule-parameters-to-file :root answer)
					 (user::restore-xte-schedule-from-file answer)
					 
					 (setf sched-file-name answer)
					 ;; edit the parameters
					 (ADTL-history "~%Created new schedule file ~s" sched-file-name)
					 
					 )
				   )))


(clim:define-command (com-ADTL-open 
		      :command-table ADTL-file 
		      :menu "Open"
		      :name "Open an existing schedule file")
		     ()
		     (cond 
		      ((null (user::xte-sched-files))
		       (clim:notify-user clim:*application-frame*
					 "No schedule files exist"))
		      (t
		       (with-slots (sched-file-name) clim:*application-frame*
				   (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
					 (choice nil)
					 (old-files (user::xte-sched-files)))
				     
				     (clim:accepting-values 
				      (stream :resynchronize-every-pass t :own-window t)
				      (write-string "Open an existing schedule file" stream)
				      (fresh-line stream)
				      
				      (setf choice
					    (clim:accept (cons 'clim:member old-files)
							 :default choice
							 :prompt "Choice"
							 :stream stream))
				      
				      )
				     
				     (when choice
					   ;; choice now bound to name
					   (setf sched-file-name choice)
					   (ADTL-history "~%Opening schedule file ~s" sched-file-name)
					   ;; load the file
					   (user::restore-xte-schedule-from-file sched-file-name)
					   ))))))


(clim:define-command (com-ADTL-copy 
		      :command-table ADTL-file 
		      :menu "Open a copy"
		      :name "Open a copy of an existing schedule file")
		     ()
		     (cond 
		      ((null (user::xte-sched-files))
		       (clim:notify-user clim:*application-frame*
					 "No schedule files exist"))
		      (t
		       (with-slots (sched-file-name) clim:*application-frame*
				   (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
					 (old-file nil)
					 (old-files (user::xte-sched-files))
					 (new-file nil))
				     
				     (clim:accepting-values 
				      (stream :resynchronize-every-pass t :own-window t
					      :label "Open a copy...")
				      
				      
				      (setf old-file
					    (clim:accept (cons 'clim:member old-files)
							 :default old-file
							 :prompt "Open the existing schedule file"
							 :stream stream))
				      (fresh-line stream)
				      (setf new-file
					    (clim:accept '(clim:null-or-type string) 
							 :default new-file
							 :prompt "under the name" 
							 :stream stream))
				      
				      (when (member new-file old-files :test #'string-equal)
					    (clim:with-text-face (:bold stream)
								 (format stream "~%File already exists, choose another name"))
					    )
				      
				      ;; fix the string up: no spaces, upper case, etc.
				      (if new-file 
					  (setf new-file (fix-string-for-sched-file-name new-file)))
				      
				      (when (or (not new-file)
						(not old-file)
						(member new-file old-files :test #'string-equal))
					    (clim:with-text-face (:bold stream)
								 (format stream "~%Both old and unique new files must be specified")))
				      
				      )
				     
				     (when (and old-file new-file)
					   
					   ;; copy the file
					   #+:ALLEGRO (excl:run-shell-command 
						       (format nil "cp ~s ~s" 
							       (namestring (user::XTE-root-to-sched old-file))
							       (namestring (user::XTE-root-to-sched new-file))))
					   
					   ;; load the file
					   (cond ((probe-file (user::XTE-root-to-sched new-file))
						  (user::restore-xte-schedule-from-file new-file)
						  (setf sched-file-name new-file)
						  (ADTL-history "~%Opening copy of schedule file ~s" old-file)
						  (ADTL-history "~%  as ~s" new-file)
						  
						  ;; edit the parameter settings and save the file
						  (edit-xte-schedule-parameters stream new-file)
						  
						  ;; save parameters to file, then load the file
						  (user::save-xte-schedule-parameters-to-file :root new-file)
						  (user::restore-xte-schedule-from-file new-file)
						  
						  (ADTL-history "~%Created new schedule file ~s" sched-file-name)
						  
						  )
						 (t
						  (clim:notify-user clim:*application-frame*
								    "Copied file ~s not found!" new-file)))
					   
					   ))))))


(clim:define-command (com-ADTL-rename 
		      :command-table ADTL-file 
		      :menu "Rename"
		      :name "Rename a schedule file")
		     ()
		     (cond 
		      ((null (user::xte-sched-files))
		       (clim:notify-user clim:*application-frame*
					 "No schedule files exist"))
		      (t    
		       (with-slots (sched-file-name) clim:*application-frame*
				   (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
					 (old-file nil)
					 (old-files (user::xte-sched-files))
					 (new-file nil))
				     
				     (clim:accepting-values 
				      (stream :resynchronize-every-pass t :own-window t
					      :label "Rename schedule file...")
				      
				      
				      (setf old-file
					    (clim:accept (cons 'clim:member old-files)
							 :default old-file
							 :prompt "Rename the schedule file"
							 :stream stream))
				      (fresh-line stream)
				      (setf new-file
					    (clim:accept '(clim:null-or-type string) 
							 :default new-file
							 :prompt "to the new name" 
							 :stream stream))
				      
				      (when (member new-file old-files :test #'string-equal)
					    (clim:with-text-face (:bold stream)
								 (format stream "~%File already exists, choose another name"))
					    )
				      
				      ;; fix the string up: no spaces, upper case, etc.
				      (if new-file 
					  (setf new-file (fix-string-for-sched-file-name new-file)))
				      
				      (when (or (not new-file)
						(not old-file)
						(member new-file old-files :test #'string-equal))
					    (clim:with-text-face (:bold stream)
								 (format stream "~%Both old and unique new files must be specified")))            
				      )
				     
				     (when (and old-file new-file)
					   ;; choice now bound to name
					   ;; rename the file
					   #+:ALLEGRO 
					   (excl:run-shell-command (format nil "mv ~s ~s" 
									   (namestring (user::XTE-root-to-sched old-file))
									   (namestring (user::XTE-root-to-sched new-file))))
					   
					   (cond ((probe-file (user::XTE-root-to-sched new-file))
						  (if (and sched-file-name (string-equal sched-file-name old-file))
						      (setf sched-file-name new-file))
						  (ADTL-history "~%Renaming schedule file ~s" old-file)
						  (ADTL-history "~%  to ~s" new-file)
						  )
						 (t
						  (clim:notify-user clim:*application-frame*
								    "Error renaming file ~s!" new-file)))
					   
					   ))))))




(clim:define-command (com-ADTL-delete 
		      :command-table ADTL-file 
		      :menu "Delete"
		      :name "Delete a schedule file")
		     ()
		     (cond 
		      ((null (user::xte-sched-files))
		       (clim:notify-user clim:*application-frame*
					 "No schedule files exist"))
		      (t
		       (with-slots (sched-file-name) clim:*application-frame*
				   (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
					 (choice nil)
					 (old-files (user::xte-sched-files)))
				     
				     (clim:accepting-values 
				      (stream :resynchronize-every-pass t :own-window t
					      :label "Delete schedule file")
				      
				      
				      (setf choice
					    (clim:accept (cons 'clim:member old-files)
							 :default choice
							 :prompt "Delete the schedule file"
							 :stream stream))
				      
				      )
				     
				     (when (and choice
						(confirm-deletion stream choice))
					   ;; choice now bound to name
					   (ADTL-history "~%Deleting schedule file ~s" choice)
					   ;; delete the file
					   (delete-file (user::XTE-root-to-sched choice))
					   (if (and sched-file-name (string-equal sched-file-name choice))
					       (setf sched-file-name nil))
					   ))))))


(defun CONFIRM-DELETION (stream file)
  (let ((answer t)
        (prompt (format nil "Delete the file ~s" file)))
    
    (clim:accepting-values
     (stream :resynchronize-every-pass t :own-window t 
             :label "Confirm deletion")
     (clim:with-text-size (:large stream)
			  (clim:with-text-family (:sans-serif stream)
						 
						 (setf answer (clim:accept 'clim:boolean 
									   :default answer :stream stream 
									   :prompt prompt)))
			  ))
    ;; end accepting values
    answer))


;; temporary, for editing date strings:

(defun XTE-DATE-STRING (tjd)
  (let ((ut (user::jdut tjd)))
    (format nil "~d-~a-~d"
	    (round (first ut))
	    (user::month-number-to-month-string (second ut))
	    (- (third ut) 1900))))


(defun EDIT-XTE-SCHEDULE-PARAMETERS (stream file)
  (let* ((title (format nil "Schedule Parameters for ~s" file))
	 (targ-files (user::XTE-targ-files))
	 (constraint-files (user::XTE-constraint-files))
	 (targ-select (list 'clim:subset-completion
			    targ-files
			    :test 'string-equal))
	 (constraint-select (list 'clim:subset-completion
				  constraint-files
				  :test 'string-equal))
	 )
    
    (let ((temp nil) (hit nil))
      (dolist (targ user::*XTE-targ-files*)
	      (setf hit (find (user::string-it targ) targ-files :test 'string-equal))
	      (if hit (push hit temp)))
      (setf user::*XTE-targ-files* (reverse temp)))
    (let ((temp nil) (hit nil))
      (dolist (constraint user::*XTE-constraint-files*)
	      (setf hit (find (user::string-it constraint) constraint-files :test 'string-equal))
	      (if hit (push hit temp)))
      (setf user::*XTE-constraint-files* (reverse temp)))
    
    (clim:accepting-values
     (stream :resynchronize-every-pass t :own-window t 
             :label title)
     (clim:with-text-size (:normal stream)
			  (clim:with-text-family (:sans-serif stream)
						 
						 (cond (targ-files
							(setf user::*XTE-targ-files*
							      (clim:accept 
							       targ-select
							       :prompt "Target files" 
							       :default  user::*XTE-targ-files*
							       :stream stream)))
						       (t (format stream "- No .targ files available -")))
						 (fresh-line stream)
						 
						 (fresh-line stream)
						 (cond (constraint-files
							(setf user::*XTE-constraint-files*
							      (clim:accept 
							       constraint-select
							       :prompt "Constraint files" 
							       :default  user::*XTE-constraint-files*
							       :stream stream)))
						       (t (format stream "- No .constr files available -")))
						 
						 
						 (fresh-line stream)
						 (format stream "Schedule start ..... ~10,4f " user::*xte-schedule-start*)
						 (setf user::*xte-schedule-start*
						       (or (user::dmy-to-time
							    (clim:accept 'string
									 :prompt nil
									 :query-identifier 'user::*xte-schedule-start*
									 :default (XTE-date-string user::*xte-schedule-start*)
									 :stream stream))
							   user::*xte-schedule-start*))
						 
						 (fresh-line stream)
						 (format stream "Schedule end ....... ~10,4f " user::*xte-schedule-end*)
						 (setf user::*xte-schedule-end*
						       (or (user::dmy-to-time
							    (clim:accept 'string
									 :prompt nil
									 :query-identifier 'user::*xte-schedule-end*
									 :default (XTE-date-string user::*xte-schedule-end*)
									 :stream stream))
							   user::*xte-schedule-end*))
						 
						 (fresh-line stream)
						 (format stream "Time quantum (d) ... ")
						 (setf user::*xte-time-quantum*
						       (clim:accept 'integer
								    :prompt nil
								    :query-identifier 'user::*xte-time-quantum*
								    :default user::*xte-time-quantum*
								    :stream stream))
						 
						 (fresh-line stream)
						 (format stream "Segment start ...... ~10,4f " user::*xte-segment-start*)
						 (setf user::*xte-segment-start*
						       (or (user::dmy-to-time
							    (clim:accept 'string
									 :prompt nil
									 :query-identifier 'user::*xte-segment-start*
									 :default (XTE-date-string user::*xte-segment-start*)
									 :stream stream))
							   user::*xte-segment-start*))
						 
						 (fresh-line stream)
						 (format stream "Seg duration ....... ")
						 (setf user::*xte-segment-duration*
						       (clim:accept 'integer
								    :prompt nil
								    :query-identifier 'user::*xte-segment-duration*
								    :default user::*xte-segment-duration*
								    :stream stream))
						 
						 (fresh-line stream)
						 (format stream "Seg count .......... ")
						 (setf user::*xte-segment-count*
						       (clim:accept 'integer
								    :prompt nil
								    :query-identifier 'user::*xte-segment-count*
								    :default user::*xte-segment-count*
								    :stream stream))
						 
						 
						 ;; print segment info
						 (cond ((< user::*xte-schedule-end*
							   (+ user::*xte-schedule-start*
							      user::*xte-time-quantum*))
							(fresh-line stream)
							(clim:with-text-face (:bold stream)
									     (write-string "Invalid dates:  end must be > start + quantum" stream)))
						       (t
							(user::test-seg-init 
							 :tjd-start         user::*xte-schedule-start*
							 :tjd-end           user::*xte-schedule-end*
							 :tjd-quantum       user::*xte-time-quantum*
							 :segment-start     user::*xte-segment-start*
							 :segment-count     user::*xte-segment-count* 
							 :segment-duration  user::*xte-segment-duration*
							 )
							(fresh-line stream)
							(clim:with-text-face (:bold stream)
									     (write-string (user::test-seg-status) stream))
							(fresh-line stream)
							(write-string (user::test-seg-description) stream)
							))
						 
						 (fresh-line stream)
						 (clim:with-text-face (:italic stream)
								      (clim:accept-values-command-button 
								       (stream :resynchronize t)
								       "Click here to refresh window"
								       (clim:window-refresh stream)))
						 
						 
						 )))))


;;; --- SCHEDULE menu command table

(clim:define-command-table ADTL-schedule)


;;; --- commands

;;; FIX?? can't get pointer documentation to work below...

(clim:define-command (com-ADTL-load 
		      :command-table ADTL-schedule 
		      :menu ("Load" 
			     :documentation "Load schedule file, create long-term schedule")
		      )
		     ()
		     (with-slots (sched-file-name) clim:*application-frame*
				 (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
				       )
				   
				   (cond ((and sched-file-name (fboundp 'user::recreate-xte-schedule))
					  (ADTL-history "~%Loading ~s" sched-file-name)
					  (user::restore-xte-schedule-from-file sched-file-name)
					  (let ((*standard-output* stream)
						(*package* (find-package 'user)))
					    (user::recreate-xte-schedule))
					  (run-XTE-csp-lt-schedule-frame 
					   :csp (user::obs-csp user::*xte-long-term-schedule*))
					  )
					 (t
					  (clim:notify-user 
					   clim:*application-frame*
					   "You must select a schedule file first"))))))


(clim:define-command (com-ADTL-show-long-term
		      :command-table ADTL-schedule
		      :menu "Display long-term schedule")
		     ()
		     (cond
		      (user::*xte-long-term-schedule*
		       (run-XTE-csp-lt-schedule-frame 
			:csp (user::obs-csp user::*xte-long-term-schedule*)))
		      (t
		       (clim:notify-user
			clim:*application-frame*
			"No long term schedule exists~%You must \"Load\" first"))))


(clim:define-command (com-ADTL-show-short-term
		      :command-table ADTL-schedule
		      :menu "Display short-term schedule")
		     ()
		     (cond
		      (user::*XTE-short-term-schedule*
		       (run-XTE-csp-st-schedule-frame 
			:csp (user::obs-csp user::*XTE-short-term-schedule*)))
		      (t
		       (clim:notify-user
			clim:*application-frame*
			(format nil "~a~%~a~%~a" 
				"No short term schedule exists"
				"You must \"Load\" first,"
				"then create a short-term schedule.")))))


(clim:define-command (com-ADTL-prep 
		      :command-table ADTL-schedule 
		      :menu ("Prep"
			     :documentation "Prep target lists if needed"))
		     ()
		     (with-slots (sched-file-name) clim:*application-frame*
				 (let (;; (stream (clim:get-frame-pane clim:*application-frame* 'interactor))
				       )
				   (cond (sched-file-name
					  (dolist (targ-file user::*XTE-targ-files*)
						  (ADTL-history "~%Prepping ~s" sched-file-name)
						  (user::XTE-prep-file targ-file)
						  )
					  (ADTL-history "~%Prepping for schedule ~s completed."
							sched-file-name)
					  )
					 (t 
					  (clim:notify-user clim:*application-frame*
							    "You must select a schedule file first"))))))




;;; --- commands in main menu

(clim:define-command (com-exit-ADTL
                      :command-table ADTL-command-table 
		      :menu "Exit"
                      :name "Exit top-level")
		     ()
		     (clim:frame-exit clim:*application-frame*)
		     )

(clim:define-command (com-help-ADTL
                      :command-table ADTL-command-table 
		      :menu "Help"
                      :name "Help")
		     ()
		     (run-help-browser-frame :which *XTE-HELP*)
		     )


;;; --- history record

(defun ADTL-history (format-string &rest args)
  (let ((stream (clim:get-frame-pane clim:*application-frame* 'interactor))
        )
    (apply #'format 
           (append (list stream format-string) args))))


;;; --- fix strings for use as file names

(defun FIX-STRING-FOR-SCHED-FILE-NAME (instring)
  (let ((ok-chars "-")
        (outstring (string-downcase (copy-seq instring))))
    (map 'string
         #'(lambda (x)
             (cond ((or (alphanumericp x)
                        (find x ok-chars))
                    x)
                   (t #\-)))
         outstring)))


;;; --- presentation types

(clim:define-presentation-type jd-date ())

(clim:define-presentation-method clim:present 
				 (object (type jd-date) stream view &key)
				 (format stream "~a ~a" object (user::jdut object)))


;;; --- Display functions

(defmethod display-show-state ((frame ADTL) stream)
  ;; (clim:window-clear stream)
  (with-slots (sched-file-name) frame
	      
	      (cond (sched-file-name
		     (format stream "Current schedule file name: ~s" sched-file-name)
		     ;; report schedule info
		     (format stream "~%Target files: ~a" user::*XTE-targ-files*)
		     (format stream "~%Constraint files: ~a" 
			     user::*XTE-constraint-files*)
		     (user::report-xte-schedule-times stream)
		     (user::report-xte-segment-times stream)
		     ;; FILL IN
		     
		     )
		    (t (format stream "No schedule file loaded")))
	      
	      
	      
	      ))


;;; --- global for frame

(defvar *ADTL* nil)


;;; --- construct and run

(defun MAKE-ADTL-FRAME (&key (root *clim-root*)
			     (force nil))
  
  ;; if force, kill the old one
  (if force (setf *ADTL* nil))
  ;; only make a new one if there isn't one bound to the global
  (unless *ADTL*
	  (setf *ADTL* 
		(clim:make-application-frame 
		 'ADTL 
		 :parent root
		 :width 450 :height 500
		 :pretty-name "Xte Spike"
		 ))))


(defun SPIKE-TOP-LEVEL (&key (root *clim-root*)
			     (force nil))
  
  (let* ((process-name "Xte Spike Top-Level Frame")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal)))
    
    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
	  (if process 
	      #+:Allegro (mp:process-kill process)
	      #-:Allegro nil
	      )
	  (setf *ADTL* nil)
	  )
    
    ;; create frame if it doesn't exist
    (unless *ADTL*
	    (make-adtl-frame :root root :force force))
    
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*ADTL*)))
      #-:Allegro  (clim:run-frame-top-level *ADTL*)
      ))))


#|
(setf *ADTL* nil)

(spike-top-level)
|#


;;; ======================================================================
;;;                                                   XTE STATUS WINDOW
;;; ======================================================================


(clim:define-application-frame XTE-STATUS
			       ()					; superclasses
			       ;; state
			       ((gw :initform nil)			; gwindow instance
				
				)
			       
			       ;; only needed because we want to update the display if started
			       ;; from another CLIM application
			       (:top-level (XTE-status-window-top-level))
			       
			       (:panes
				((status :application
					 :default-text-style '(:sans-serif :roman :normal)
					 :display-function 'display-XTE-status
					 :display-after-commands t
					 :incremental-redisplay t
					 :scroll-bars :both
					 )
				 ))
			       
			       (:layout
				((default
				   (:column 1
					    (status :rest)
					    ))))
			       )



(defmethod DISPLAY-XTE-STATUS
  ((frame XTE-status) stream)
  
  (clim:window-clear stream)
  
  (format stream "~a" (user::get-formatted-universal-time))
  
  (format stream "~%Long-term schedule:")
  (cond (user::*xte-long-term-schedule*
	 (format stream " ~a" (user::obs-csp-chronika-name 
			       user::*xte-long-term-schedule*)))
	(t (format stream " Not loaded")))
  
  (format stream "~%Short-term schedule:")
  (cond (user::*XTE-short-term-schedule*
	 (format stream " ~a" (user::obs-csp-chronika-name 
			       user::*XTE-short-term-schedule*)))
	(t (format stream " Not loaded")))
  
  (format stream "~%Event cache")
  (user::event-cache-report (user::current-XTE-event-calc)
			    :stream stream)
  
  )

;;; special top-level which does a display update first, then calls the default top
;;; level.  This allows for the fact that the slots can be modified before
;;; displaying.

(defmethod XTE-STATUS-WINDOW-TOP-LEVEL ((frame XTE-STATUS)
					&rest options)
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'status)))    
    (loop
     (display-XTE-status clim::*application-frame* stream)
     (sleep 3)
     )))


;;; --- global

(defvar *XTE-STATUS-FRAME* nil)


;;; --- constructor & display

(defun MAKE-XTE-STATUS-FRAME (&key (root *clim-root*)
				   (force nil))
  
  ;; if force, kill the old one
  (if force (setf *XTE-STATUS-FRAME* nil))
  ;; only make a new one if there isn't one bound to the global
  (unless *XTE-STATUS-FRAME*
	  (setf *XTE-status-frame*
		(clim:make-application-frame 'XTE-status :parent root
					     :width 350 :height 200
					     :pretty-name "Spike XTE Status"
					     ))))


(defun RUN-XTE-STATUS-FRAME (&key (root *clim-root*)
				  (force nil))
  
  (let* ((process-name "Xte Spike Status Frame")
	 (process
	  #-:Allegro nil
	  #+:Allegro (find process-name 
			   mp:*all-processes*
			   :key #'mp:process-name
			   :test #'string-equal)))
    
    ;; if force is t, and the process exists, then kill it
    ;; and null the global
    (when force
	  (if process 
	      #+:Allegro (mp:process-kill process)
	      #-:Allegro nil
	      )
	  (setf *XTE-STATUS-FRAME* nil)
	  )
    
    ;; create frame if it doesn't exist
    (unless *XTE-STATUS-FRAME*
	    (make-XTE-status-frame :root root :force force))
    
    ;; run
    (cond 
     ;; process exists: reset
     (process
      #+:Allegro (mp:process-reset process)
      #-:Allegro nil
      )
     ;; no process exists: run
     (t
      #+:Allegro (mp:process-run-function 
		  process-name
		  #'(lambda () (clim:run-frame-top-level 
				*XTE-STATUS-FRAME*)))
      #-:Allegro  (clim:run-frame-top-level *XTE-STATUS-FRAME*)
      ))))


#|
(run-XTE-status-frame)
|#


;;; ======================================================================
;;;                                                            XTE HELP
;;; ======================================================================


(defvar *XTE-HELP* nil)

(setf *XTE-HELP*
      (make-instance 'CLIMIF-HELP :title "Xte Help" :width 40))

(clear-help *XTE-HELP*)
(record-help
 *XTE-HELP*
 '(
   (
    "Introduction"
    "This is the introduction to Xte help.  For help on any"
    "topic, click on the bold-faced keyword.  You can also select"
    "topics from the list on the right"
    :paragraph
    "See:" (:ref "Mission Description") 
    "for an overview of the Xte mission"
    "For an overview, see" (:ref "A Road Map")
    )
   (
    "Mission Description"
    "XTE is Japan's fourth X-ray astronomy mission and the "
    "first for which the US is providing a part of the scientific payload. "
    "Scheduled to fly in February 1993, its four large-area telescopes "
    "will focus X-rays from a wide energy range onto a pair each of "
    "charge coupled devices (CCDs) and imaging gas proportional counters "
    "(GSPCs).  XTE will be the first X-ray imaging mission operating "
    "over the 0.5-12 keV band with high energy resolution.  This "
    "combination of capabilities will enable a varied and exciting "
    "program of astronomical research to be carried out. "
    ""
    "XTE will weight 420 kg and will be launched in February "
    "1993 from Kagoshima Space Center (KSC) by the ISAS into an "
    "approximately circular orbit at an altitude varying between 550-650 "
    "km.  It will be three-axis stabilized to an accuracy of one arcminute; "
    "post facto attitude reconstruction will be accurate to about 0.1 "
    "arcminute.  The mirror axis is parallel to the plane of the solar "
    "paddles, which means that the observable region of the sky is a belt "
    "centered on a great circle 90�from the sun vector.  The direction of "
    "solar power requirements limit the width of the belt to �30�. Direct "
    "contacts between the satellite and the ground stations will be "
    "possible for ten of the fifteen orbits per day; five from Japan and "
    "five from Australia.  "
    ""
    "After an initial seven-month calibration and verification"
    "phase, observing time on XTE will be open to competitive "
    "proposals.  The observing time will be apportioned as follows: "
    "60 per cent for Japanese and European proposals,"
    "15 per cent for US proposals,"
    "25 per cent for US/Japan collaborative proposals."
    "Based on the previous X-ray missions (i.e. Einstein and ROSAT), it is"
    "expected that more than 1000 proposals will be submitted just from US"
    "astronomers every half year.  As a result, XTE may need to observe"
    "5 to 20 objects per day.  It is almost impossible to schedule this"
    "many targets without a computer assistance.  Based on this, ISAS"
    "decided to use an observation planning software developed at Space"
    "Telescope Institute (STScI) called Scientific Planning Interactive"
    "Knowledge-based Environment"
    (:ref "Spike")
    )
   (
    "SPIKE"
    "SPIKE is a mission planning software developed by a team of"
    "programmers at the STScI under the direction of Dr. Mark Johnston for"
    "use with the Hubble Space Telescope (HST).  SPIKE ihas been developed"
    "for the purpose of automating observatory scheduling to increase the"
    "effective utilization and, ultimately, scientific return from orbiting"
    "telescopes.  Its developers have applied artificial intelligence (AI)"
    "techniques to the problem of scheduling astronomical observations."
    "High-level scheduling strategies using both rule-based and neural"
    "network approaches have been incorporated.  Graphical displays of"
    "activities, constraints, and schedules are an important feature of the"
    "system.  Several space missions (i.e., EUVE, XTE, and AXAF) are"
    "currently planning to use SPIKE."
    :paragraph
    "See the additional topics: "
    (:ref "Spike input")"," (:ref "Spike usage") "," (:ref "What Spike is not")
    )
   (
    "Spike input"
    "SPIKE accepts a list of targets, checks constraints (such as"
    "sun avoidance, horizon angle limits, and South Atlantic Anomaly"
    "interference) and computes a "suitability function" for each object."
    "Then, it schedules the targets during a specified period to find an"
    "optimum observation plan using a neuro-network procedure.  Since it"
    "uses AI, a SPIKE schedule is not necessarily unique and may be only"
    "one of several possible good observation plans."
    )
   ("Spike usage"
    "Although SPIKE can give excellent schedules which do not"
    "violate the constraints, users must realize that SPIKE is just a"
    "toolfor assisting in the scheduling of observations.  Planner must to"
    "decide which schedule is the best.  Further, if the planner does not"
    "want to accept the results, he can modify it before the result is sent"
    "into the Daily Planning Software."
    )
   ("What Spike is not"
    "SPIKE is not a proposal entry or evaluation system.  SPIKE can"
    "certainly be used to determine whether a proposal violates mission"
    "constraints, it may be used by proposers to evaluate the feasibility"
    "of their requests."
    ""
    "It is very important to understand what SPIKE can do and"
    "cannot.  Without understanding this, SPIKE will be less useful for the"
    "schedule process."
    )
   (
    "A Road Map"
    "XTE operational requirements are summarized below in"
    (:ref "Constraints for SPIKE")
    ""
    "If you need to compile SPIKE first time, start from sections 3, "
    "and follow the directions."
    ""
    "If you need to know how to use SPIKE, start from section 8, "
    "though you may need to refer to several previous sections for "
    "starting up."
    ""
    "If you want to know the background information about SPIKE, "
    "go to the Appendices which contains portions of two articles by "
    "Mark Johnston."
    )
   (
    "Requirements for SPIKE"
    "The following requirements for SPIKE are based on Dr. Dotani's "
    "Memos (6-27-91)."
    ""
    "SPIKE and the daily planning software will handle XTE "
    "observation scheduling and operation planning. SPIKE will make long "
    "term schedules (~6 months) and short-term schedules (~one week) "
    "based on given targets lists and constraints.  The daily planning "
    "software will accept the resulting short-term schedule, verify it "
    "and converts it into spacecraft commands. "
    )
   (
    "SPIKE Input File Construction"
    "A target list will be complied from approved proposals for a "
    "certain period (e.g.,. every half year)in a specified SPIKE input file "
    "format. Each record in this file contains the source names, celestial "
    "coordinate (in degree), length of observation, priority, and other "
    "conditions, such as (1) bit rate, (2) Background (BGD) observation "
    "requirement (not implemented in SPIKE), (3) observational mode. "
    "After completion of an the observation, the target  will be removed "
    "from the list. Currently, this part of the work is expected to be "
    "manually. "
    )
   (
    "Observation Starting Time"
    "SPIKE assists in scheduling the starting times for each object "
    "in a two-step process: a long term and a short term planning "
    "operation. It is anticipated that SPIKE will be run once a week with "
    "updated orbital elements."
    )
   (
    "Long Term Planning"
    "The long-term observation plan will be generated in intervals "
    "of one week. In other words, a SPIKE long term plan will specify "
    "which sources should be observed in which week."
    )
   (
    "Short Term Planning"
    "SPIKE will be run once per week to generate detailed schedule.  "
    "The short term plan will specify the items described below and send "
    "them to the daily planning software. "
    "1. Maneuver starting time and its satellite attitude "
    "2. Observation period and bit rate."
    )
   (
    "Command Planning (Daily Planning Software)"
    "The daily planning software will make actual commands. "
    "Although the output from the step 2 may be used as a default, the "
    "planner can, of course, change the plan, to accommodate special "
    "requirements. "
    )
   (
    "Desirable Information from SPIKE"
    "In the XTE operation, it is expected that SPIKE will make "
    "a long term general plan and a short term observation plan, the daily "
    "planning software will create actual commands. Therefore, it is "
    "essential to decide which information from SPIKE will be required "
    "by the daily planning software. "
    ""
    "[Minimum Information]"
    "The minimum information required by the daily planning "
    "software is a maneuver starting time, the celestial coordinates of "
    "the target, and a source name at the pointing between one "
    "maneuvering and the next. If SPIKE gives the BGD observation, the "
    "attitude of the satellite should be given. "
    ""
    "[Desirable Information]"
    "It is very desirable to have information related to a capacity "
    "of the BDR."
    "(1) The starting time and the ending time of data recording on the "
    "BDR. "
    "(2) A bit rate for the recording."
    "(3) The starting time and duration of downlink to the stations "
    "(Kagoshima and Canberra). "
    ""
    "The daily planning software will get the information as a "
    "default, but modify it if it is necessarily, and make a command plan. "
    ""
    "[Useful information]"
    "If the information described below is available, it will reduce "
    "the task of the daily planning software very much.  These "
    "requirements are currently not in SPIKE."
    "(1) For a given satellite attitude, ID of guiding stars tracked by the "
    "STT's."
    "(2) Information whether two STT's are tracking stars or only one of "
    "them is tracking stars."
    )
   (
    "Constraints for SPIKE"
    "The long term SPIKE planning should (or hopefully) include the "
    "constraints"
    (:ref "Tracking Station") 
    ", "
    (:ref "Working Day")
    ", "
    (:ref "Number of Contact Pass per Day")
    ", "
    (:ref "Ranging")
    ", "
    (:ref "Other Satellite Transmission")
    ", "
    (:ref "Data Transmission")
    ", "
    (:ref "Satellite Attitude")
    ", "
    (:ref "Attitude Error")
    ", "
    (:ref "Maneuverability")
    )
   (
    "Tracking Station"
    "Kagoshima Space Center (KSC: East Long, 131�.079, North Lat. "
    "31�.25) and NASA Deep Space Network at Canberra (East Long. "
    "148�.98, South Lat.  35�.47) will be used as tracking stations. The "
    "station at Canberra will be used only for a downlink. Therefore, the "
    "downlink command for the Canberra station needs to be given by KSC "
    "prior to the downlink."
    )
   (
    "Working Day"
    ""
    "KSC is closed on Sunday. The Canberra station does not have "
    "such a restriction. Therefore, we need to send two days worth of "
    "commands on Saturday. KSC will also be closed at the end of the year "
    "for about a week. The Canberra station is expected to be opened all "
    "year long."
    )
   (
    "Number of Contact Pass per Day"
    "Although the satellite circles about 15 times per day, KSC can "
    "contact the satellite only about 5 to 6 times a day. Because of a "
    "limitation on working hour of contractors the contacts to the "
    "satellite are limited to 5 times a day (8 hrs working day) at KSC. "
    "The Canberra station will be available for all contacts. "
    )
   (
    "Ranging"
    "No restriction."
    )
   (
    "Other Satellite Transmission"
    "Although it will be usually possible to contact XTE 5 "
    "times a day, ithere will be conflicts with other satellites. The "
    "satellites sharing the KSC facilities are"
    :paragraph
    "	(1) Akebono     Feb. 1989"
    :paragraph    
    "	(2) SOLAR-A     Aug. 1991"
    :paragraph
    "	(3) SFU         Winter 1994 (H-II)"
    :paragraph
    "	(4) MUSES-B     Winter 1995 (M-V)"
    :paragraph
    "	(5) GEOTAIL     Summer 1992 (NASA)"
    :paragraph
    "If the contact time is overlapped, priority will be decided by "
    "negotiation among the groups. Since the SOLAR-A uses the Canberra "
    "station, it is possible that the contact time will overlap at the "
    "Canberra station."
    )
   (
    "Data Transmission"
    "Three operations described below can be done simultaneously "
    "while in contact with KSC. "
    "(1) Command transmission "
    "(2) Real time data transmission (from the satellite)"
    "(3) BDR transcription  (from the satellite)"
    "Since XTE has two directional antennas, it is necessary "
    "to change the antenna depending on the satellite attitude viewed "
    "from the ground station (although there are three antennas for the "
    "S-band, it has not been decided whether the third antenna will be "
    "used in the normal operation). Since the antennas have low gain in "
    "the direction of the satellite Z-axis (the telescope axis direction), "
    "if the Z-axis is pointing to the station, the contact with the "
    "satellite may not be perfect. Another restriction is that the "
    "command transmission can be done only when the elevation angle of "
    "the satellite is higher than 5� as required by a Japanese law (expect "
    "in the case of emergency). Therefore, if the maximum elevation is "
    "less than 5�, we do not contact to the satellite. However, if we limit "
    "to 5 times per day, this condition will be automatically satisfied.  "
    :paragraph
    "The Canberra station can receive only the transcribed data "
    "from the BDR and cannot receive the real time data.  "
    )
   (
    "Satellite Attitude"
    "Angle to the" (:ref "Sun")
    "The angle between the satellite Y-axis (perpendicular to a "
    "solar paddle) and the line to the sun is constrained to be less than "
    "30 degrees."
    )
   (
    "Attitude Error"
    "In a normal operation, the attitude control errors are: "
    "20.7 arcsec around the X and Y axes,"
    "44.3 arcsec around the Z axis."
    "The stability is:"
    "5.1 arcsec/32 sec around the X and Y axes, "
    "5.2 arcsec/32 sec around the Z axis."
    "These values are the errors when two star trackers are used. If only "
    "one star tracker is used, the errors may be larger. "
    )
   (
    "Maneuverability"
    "The specifications for maneuvering are:"
    "Max. Speed: about  0.15 deg/sec"
    "Max Slew time (180 deg): 20 min."
    "Positional Error after a Slew: about 0.1 deg"
    "It takes a few minutes to stabilize the satellite attitude after a "
    "slew, assuming the STT's find guide stars."
    )
   
   
   ))

(cross-reference-help *XTE-HELP*)



#|

(test-help *XTE-help*)
(describe *XTE-help*)
(run-help-browser-frame :which *XTE-HELP*)

|#

;;;
;;; micro-spike-test.lisp
;;;

#|
(setf *very-verbose-constraint-prop* t)

(make-chronika :name "Fred" :start 0 :end 10)
(setf (keep-history *current-chronika*) t)
(describe *current-chronika*)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(task-list *current-chronika*)
(describe *current-chronika*)
(describe a)
(check-consistency *current-chronika*)
(describe a)
;;(backup-history *current-chronika*)
;;(describe a)
;;(check-consistency *current-chronika*)

;; bug in history keeping:  chronika consistency flag not set
;; properly; also not all task slots are properly kept:
;; check usage of (setf (slot-value...)) that bypasses history
;; method

;; test accessors
(id a)
(chronika a)
(diagnostic-messages a)
(minimum-duration a)
(maximum-duration a)
(properties a)
(constraints a)
(directly-related-tasks a)
(all-related-tasks a)
(executed a)
(restriction a)
(abs-suitability a)
(rel-suitability a)
(suitability a)
(earliest-start a)
(latest-start a)
(non-zero-intervals a)
(check-suitability a)
(properties a)
  
;; test methods
(record-diagnostic-message a "Test message 1")
(record-diagnostic-message a "Test message 2")
(diagnostic-messages a)
(clear-diagnostic-messages a)
(diagnostic-messages a)
(get-task-property a :size)
(set-task-property a :size 15)
(get-task-property a :size)
(properties a)
(set-task-property a :size nil)
(get-task-property a :size)

(schedulable-p a)
(duration-given-end-at a 5)
(duration-given-start-at a 5)
(start-given-end-at a 5)
(end-given-start-at a 5)
(earliest-end a)
(latest-end a)
(default-pcf a)
(compute-suitability a)

;; possibly destructive methods
(update-suitability a)
(open-suitability a)
(describe a)
(update-suitability a)
(suitability a)

(execution-time a)
(executed a)
(setf (execution-time a) 5)
(execution-time a)
(executed a)
(non-zero-intervals a)
(setf (execution-time a) 6)
(non-zero-intervals a)
(setf (execution-time a) nil)
(non-zero-intervals a)

(restriction-time a)
(restriction a)
(setf (restriction a) (ipcf-create-with-single-interval 2 4 1 0))
(restriction-time a)
(restriction a)
(non-zero-intervals a)
(setf (restriction-time a) 5)
(restriction-time a)
(restriction a)
(non-zero-intervals a)
(setf (restriction-time a) 6)
(restriction-time a)
(restriction a)
(non-zero-intervals a)
(setf (restriction-time a) nil)
(restriction-time a)
(restriction a)
(non-zero-intervals a)

;; abs time constraint
(setf c1 (make-abs-task-constraint :abs))
(add-abs-task-window
 c1 a (ipcf-create-with-single-interval 2 7 1 0))
(add-abs-task-window
 c1 a (ipcf-create-with-single-interval 6 9 1 0))
(describe c1)

(source c1)
(tasks c1)
(closed c1)
(constraint-abs-pcf c1 a)
(constraint-abs-pcf c1 b)
(constraint-rel-pcf c1 a)
(task-has-changed c1 a)
(tasks-affected-via-constraint c1 a)
(tasks-affecting-via-constraint c1 a)
(tasks-related-by-constraint c1)
(constraint-details c1)
(task-window-table c1)
(gethash a (task-window-table c1))

(activate-constraint c1)
(suitability a)
(non-zero-intervals a)

;; binary rel constraint
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))

(setf c2 (make-binary-rel-constraint :super))
;; explicit precedence constraint
(define-binary-constraint c2 a b 
  (ipcf-create-with-single-interval 
   (minimum-duration a) *plus-infinity* 1 0))
(constraint-details c2)
(define-binary-constraint c2 b a 
  (ipcf-create-with-single-interval 
   *minus-infinity*
   (- (minimum-duration a)) 1 0))
(constraint-details c2)
;; same precedence constraint
(mutually-constrain c2 a b :type :precedence :offset 0)
(constraint-details c2)

(describe c2)
(tasks-constrained-by-task c2 a)
(tasks-constraining-task c2 a)
(current-constraint c2 a b)
(current-constraint-intervals c2 a b)
(current-constraint-nonzero-intervals c2 a b)

(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset 0)
(mutually-constrain c2 b c :type :precedence :offset 0)
(constraint-details c2)
(induced-constraint c2 a b c)
(induced-constraint c2 c b a)
(add-forward-induced-arcs c2 a b)
(add-backward-induced-arcs c2 a b)
(add-forward-induced-arcs c2 b c)
(add-backward-induced-arcs c2 b c)
(add-forward-induced-arcs c2 b a)
(add-backward-induced-arcs c2 b a)
(constraint-details c2)

(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset 0)
(mutually-constrain c2 b c :type :precedence :offset 0)
(make-path-consistent c2)
(constraint-details c2)

(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset 0)
(mutually-constrain c2 b c :type :precedence :offset 0
                    :propagate t)
(constraint-details c2)

(describe c2)
(tasks-constrained-by-task c2 a)
(tasks-constraining-task c2 a)
(current-constraint c2 a b)
(current-constraint-intervals c2 a b)
(current-constraint-nonzero-intervals c2 a b)

;; simple test of constraint propagation
(setf *very-verbose-constraint-prop* t)
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset 0)
(mutually-constrain c2 b c :type :precedence :offset 0)
(make-path-consistent c2)
(activate-constraint c2)
(describe a)
(describe b)
(describe *current-chronika*)
(check-consistency *current-chronika*)
(non-zero-intervals a)
(non-zero-intervals b)
(non-zero-intervals c)
(setf (execution-time a) 1)
(non-zero-intervals a)
(setf (execution-time a) nil)
(non-zero-intervals a)
(setf (restriction-time a) 1)
(non-zero-intervals a)
(setf (restriction-time a) nil)
(non-zero-intervals a)

=================
(setf *very-verbose-constraint-prop* nil)
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf d (make-task :id 'd :minimum-duration 2))
(setf e (make-task :id 'e :minimum-duration 2))
(setf f (make-task :id 'f :minimum-duration 2))

(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset 0)
(mutually-constrain c2 b c :type :precedence :offset 0)
(mutually-constrain c2 c d :type :precedence :offset 0)
(no-overlap c2 (list a e f))

(make-path-consistent c2)
(constraint-details c2)
(activate-constraint c2)
(check-consistency *current-chronika*)

(restrict-to-earliest-start a)
(restrict-to-earliest-start b)
(restrict-to-earliest-start c)
(restrict-to-earliest-start d)
(restrict-to-earliest-start e)
(restrict-to-earliest-start f)
(check-consistency *current-chronika*)


;; group within & no overlap
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(no-overlap c2 (list a b c))
(group-within c2 (list a b c) 6)

(make-path-consistent c2)
(constraint-details c2)
(activate-constraint c2)
(check-consistency *current-chronika*)
(setf (restriction-time a) 5)
(restrict-to-earliest-start b)
(check-consistency *current-chronika*)
(non-zero-intervals a)
(non-zero-intervals b)
(non-zero-intervals c)


;; precedence fixed offset
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence-fixed-offset :offset 2)
(group-within c2 (list a b c) 6)
(no-overlap c2 (list a b c))
(make-path-consistent c2)
(constraint-details c2)
(activate-constraint c2)
(check-consistency *current-chronika*)
(setf (restriction-time a) 3)
(check-consistency *current-chronika*)
(restrict-to-earliest-start b)
(check-consistency *current-chronika*)
(non-zero-intervals a)
(non-zero-intervals b)
(non-zero-intervals c)

;; ordering relationships
(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf d (make-task :id 'd :minimum-duration 2))
(setf e (make-task :id 'e :minimum-duration 2))
(setf f (make-task :id 'f :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence :offset -1)
(mutually-constrain c2 c d :type :precedence)
(mutually-constrain c2 e f :type :precedence-fixed-offset :offset 5)

(order-possibilities c2 a b)
(order-possibilities c2 b a)
(order-determined c2 a b)
(order-determined c2 b a)
(order-possibilities c2 c d)
(order-possibilities c2 d c)
(order-determined c2 c d)
(order-determined c2 d c)
(min-max-separation c2 a b)
(min-max-separation c2 b a)
(min-max-separation c2 e f)
(min-max-separation c2 f e)
(task-can-immediately-follow c2 a b)
(task-can-immediately-follow c2 b a)
(task-can-immediately-follow c2 a d)
(task-can-immediately-follow c2 e f)
(task-can-immediately-follow c2 f e)
(task-can-immediately-follow c2 c d)
(task-can-immediately-follow c2 d c)

(make-chronika :name "Fred" :start 0 :end 10)
(setf a (make-task :id 'a :minimum-duration 0))
(setf b (make-task :id 'b :minimum-duration 0))
(setf c (make-task :id 'c :minimum-duration 0))
(setf c2 (make-collapsible-binary-rel-constraint :super))
(strict-order-nogap c2 (list a b c))
(order-determined c2 a b)
(order-possibilities c2 a b)
(constraint-details c2)

;; history keeping
(make-chronika :name "Fred" :start 0 :end 10 :keep-history t)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(mutually-constrain c2 a b :type :precedence-fixed-offset :offset 2)
(group-within c2 (list a b c) 6)
(no-overlap c2 (list a b c))
(make-path-consistent c2)
(constraint-details c2)
(activate-constraint c2)
(check-consistency *current-chronika*)
(clear-history *current-chronika*)
(describe *current-chronika*)

(restrict-to-earliest-start a)
(history *current-chronika*)
(check-consistency *current-chronika*)
(history-marker *current-chronika*)
(restriction a)
(non-zero-intervals a)
(non-zero-intervals b)
(backup-history *current-chronika*)
(history *current-chronika*)

;; set execution-time
(non-zero-intervals b)
(setf (execution-time b) 3)
(execution-time b)
(executed b)
(non-zero-intervals b)
;; change execution time
(setf (execution-time b) 6)
(non-zero-intervals b)
(setf (execution-time b) nil)
(non-zero-intervals b)
(backup-history *current-chronika*)
(backup-history *current-chronika*)
(backup-history *current-chronika*)
(backup-history *current-chronika*)


(setf (execution-time b) 6)
(non-zero-intervals b)
(setf *saved-marker* (history-marker *current-chronika*))
(setf (restriction-time a) 9)
(non-zero-intervals a)
(backup-history-to-marker *current-chronika* *saved-marker*)
(backup-history-to-marker *current-chronika* 'history-start)

;; attach constraint & detach with history
(setf *saved-marker* (history-marker *current-chronika*))
(setf c3 (make-binary-rel-constraint :super))
(strict-order c3 (list b a))
(activate-constraint c3)
(check-consistency *current-chronika*)
(backup-history-to-marker *current-chronika* *saved-marker*)







(defun early-start-max-suit-step ()
  (let ((earliest-start-time (end *current-chronika*))
        (max-suitability 0)
        (candidate-tasks nil)
        (changed-task nil))
    
    ;; find earliest start of any tasks
    (dolist (a (task-list *current-chronika*))
      (when (and (not (restriction-time a))
                 (schedulable-p a))
        (setf earliest-start-time 
              (min earliest-start-time (earliest-start a)))))
    
    ;; get list of tasks with min earliest start
    (dolist (a (task-list *current-chronika*))
      (when (and (not (restriction-time a))
                 (schedulable-p a)
                 (= earliest-start-time (earliest-start a)))
        (push a candidate-tasks)
        (setf max-suitability 
              (max max-suitability
                   (pcf-get-value (earliest-start a) (suitability a))))))
    
    (format t "~%candidates: ~a, t=~a"
            (mapcar #'id candidate-tasks) earliest-start-time)
    
    ;; if any have suitability at earliest start which is not max,
    ;; then restrict to max
    (let ((restricted-to-max nil))
      (dolist (a candidate-tasks)
        (when (/= (pcf-max (suitability a))
                  (pcf-get-value (earliest-start a) (suitability a)))
          (setf changed-task a)
          (restrict-to-max-suitability a)
          (format t "~%restrict to max ~a" (id a))
          (setf restricted-to-max a)))
      ;; if none restricted, then restrict one with max suitability
      (when (not restricted-to-max)
        (dolist (a candidate-tasks)
          (when (= max-suitability
                   (pcf-get-value (earliest-start a) (suitability a)))
            (restrict-to-earliest-start a)
            (setf changed-task a)
            (format t "~%restrict to ES ~a" (id a))))))
    changed-task))



(make-chronika :name "Fred" :start 0 :end 20 :keep-history t)
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf d (make-task :id 'd :minimum-duration 2))
(setf e (make-task :id 'e :minimum-duration 2))
(setf f (make-task :id 'f :minimum-duration 2))
(setf g (make-task :id 'g :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
;;(order-preference c2 (list a b c d e f))
;;(order-preference c2 (list a c e b d f g))
;;(no-overlap c2 (list a b c d e f g))
(no-overlap c2 (list a b c))
(mutually-constrain c2 a b :type :max-time-separation :offset 6)
(group-within c2 (list a b c d) 12)
(mutually-constrain c2 a b :type :precedence :offset 2)
(make-path-consistent c2)
(activate-constraint c2)
(check-consistency *current-chronika*)
(clear-history *current-chronika*)

(early-start-max-suit-step)
(loop (when (not (early-start-max-suit-step)) 
        (check-consistency *current-chronika*)
        (return)))
(check-consistency *current-chronika*)
(backup-history-to-marker *current-chronika* 'history-start)


(current-constraint-intervals c2 a b)
(all-related-tasks a)
(tasks-affected-via-constraint c2 g)
(tasks-affected-via-constraint c2 a)
(order-determined c2 a c)
(order-determined c2 a b)
(order-determined c2 b c)
(current-constraint c2 c a)

    
|#



#|  TEST COLLAPSIBLE CONSTRAINT
|#
  

#|
(make-chronika :name "Fred")
(setf a (make-task :id 'a :minimum-duration 2))
(setf b (make-task :id 'b :minimum-duration 2))
(setf c (make-task :id 'c :minimum-duration 2))
(setf d (make-task :id 'd :minimum-duration 2))
(setf c2 (make-binary-rel-constraint :super))
(strict-order c2 (list a b c d))
;;(strict-order c2 (list d a))  ; cycle
(mutually-constrain c2 b c :type :fixed-offset)
(make-path-consistent c2)
(constraint-details c2)
(current-constraint c2 a b)
(current-constraint-intervals c2 a b)
(current-constraint-intervals c2 b a)
(current-constraint-intervals c2 a c)
(current-constraint-intervals c2 b c)
(current-constraint-intervals c2 c b)
(current-constraint c2 a b)
(current-constraint c2 b a)
(current-constraint c2 a c)
(current-constraint c2 b c)
(current-constraint c2 c b)
(current-constraint-nonzero-intervals c2 a b)
(current-constraint-nonzero-intervals c2 b a)
(current-constraint-nonzero-intervals c2 a c)
(current-constraint-nonzero-intervals c2 c b)
(activate-constraint c2)
(check-consistency *current-chronika*)
(clear-history *current-chronika*)
(setf (restriction-time b) 20)
(suitability b)

(setf c3 (make-collapsible-binary-rel-constraint :collapse))
(strict-order c3 (list a b c d))
;;(strict-order c3 (list d a))  ; cycle
;;(strict-order c3 (list a d))  ; cycle
(mutually-constrain c3 b c :type :fixed-offset)
(constraint-details c3)
(current-constraint c3 a b)
(current-constraint-intervals c3 a b)
(current-constraint-intervals c3 b a)
(current-constraint-intervals c3 a c)
(current-constraint-intervals c3 b c)
(current-constraint-intervals c3 c b)
(current-constraint c3 a b)
(current-constraint c3 b a)
(current-constraint c3 a c)
(current-constraint c3 b c)
(current-constraint c3 c b)
(current-constraint-nonzero-intervals c3 a b)
(current-constraint-nonzero-intervals c3 b a)
(current-constraint-nonzero-intervals c3 c b)
(constrained-unschedulable c3 b)

(collapse-constraint c3)
(make-path-consistent c3)
(activate-constraint c3)
(check-consistency *current-chronika*)
(clear-history *current-chronika*)
(setf (restriction-time b) 20)
(suitability b)
(setf (restriction-time b) nil)

(tasks-affected-via-constraint c3 a)
(tasks-affected-via-constraint c3 b)
(tasks-affected-via-constraint c3 c)
(tasks-affected-via-constraint c3 d)

(tasks-affecting-via-constraint c3 a)
(tasks-affecting-via-constraint c3 b)
(tasks-affecting-via-constraint c3 c)
(tasks-affecting-via-constraint c3 d)

|#









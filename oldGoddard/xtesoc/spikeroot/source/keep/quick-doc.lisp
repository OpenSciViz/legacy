;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; quick-doc.lisp:  strip and format comments in lisp code,
;;;  for most common forms.
;;;
;;; Usage:  (quick-doc infile outfile)
;;; - formats all common forms (defun, defparameters, ...) in a standard
;;;   way with arg lists and doc strings [see QUICK-DOC-FORM for a list]
;;; - Formats ;;; comments
;;; - Ignores #| |# comments
;;;
;;; Known bugs:
;;; - can't handle (eval-when forms)
;;;
;;; $Log: quick-doc.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.4  1991/12/01  00:24:14  johnston
;;; no changes
;;;
;;; Revision 1.3  1991/11/26  15:43:43  johnston
;;; no changes
;;;
;;; Revision 1.2  1991/03/18  22:21:27  johnston
;;; changed to recognize ;;; comments and include them in the documentation
;;;
;;; Revision 1.1  91/02/28  14:36:26  johnston
;;; Initial revision
;;; 
;;; Revision 1.2  90/11/26  01:25:10  johnston
;;; added defgeneric, trans forms, etc.
;;; 
;;;
;;;

#|
(quick-doc "/cerb/p1/spike/develop/nets/nnif.lisp"
	   "/cerb/u1/johnston/gds/nnif.text")

(quick-doc "/cerb/u1/johnston/gds/repair.lisp" "/cerb/u1/johnston/gds/repair-doc.text")
(quick-doc "/cerb/u1/johnston/gds/repair-capcon.lisp" "/cerb/u1/johnston/gds/repair-capcon-doc.text")
(quick-doc "/cerb/u1/johnston/gds/hook-to-spike.lisp" "/cerb/u1/johnston/gds/hook-to-spike-doc.text")
(quick-doc "/cerb/u1/johnston/gds/repair-plot.lisp" "/cerb/u1/johnston/gds/repair-plot-doc.text")
(quick-doc "/cerb/u1/johnston/gds/network-files-to-csp.lisp" "/cerb/u1/johnston/gds/network-files-to-csp-doc.text")

(quick-doc "xanadu:utilities:astro-objects.lisp"
           "xanadu:mar91:temp.tmp")
(quick-doc "xanadu:gds:gds-code:repair.lisp"
           "xanadu:gds:gds-code:repair-doc.text")
(quick-doc "xanadu:gds:gds-code:hook-to-spike.lisp"
           "xanadu:gds:gds-code:hook-to-spike-doc.text")
(quick-doc "xanadu:mar91:quick-doc.lisp" "xanadu:mar91:temp.tmp")
|#

(defun QUICK-DOC (infile outfile &optional (print? nil))

  "Strip comments from lisp source file and write to another
    file.  Usage:
     (quick-doc infile outfile)
    where infile and outfile are filenames.
    A third optional argument is print? which defaults to nil:
    if t, then unrecognized forms are briefly printed."

  (with-open-file (outstream outfile
                             :direction :output
                             :if-does-not-exist :create
                             :if-exists :supersede)
    (format outstream 
            "Documentation for: ~a" infile)
    
    (with-open-file (stream infile :direction :input)

      ;; last-printed keeps track of last item printed:  may be nil (don't know)
      ;; :form, :comment.  Used to get line spacing right.
      (let ((last-printed nil))

      (loop
        
        (let ((next-char (read-char stream nil :eof)))
          (cond 
           
           ((eq :eof next-char)
            (return nil))
           
           ((char-equal #\# next-char)
            (when (char-equal #\| (read-char stream nil #\space))
              ;; found pound-bar comment: read to match
              (read-past-block-comment stream)))

           ((char-equal #\; next-char)
            (unread-char next-char stream)
            (let ((comment (read-line stream nil :eof)))
              (cond ((eq comment :eof) (return nil))
                    (t (quick-doc-comment comment outstream last-printed)
                       (setf last-printed :comment)))))
           
           ((char-equal #\( next-char)
            (unread-char next-char stream)
            (let ((form (read stream nil :eof)))
              (cond ((eq form :eof) (return nil))
                    (t (setf last-printed :form)
                       (quick-doc-form form outstream print?)))))
           
           (t nil))))))))

(defun READ-PAST-BLOCK-COMMENT (stream)
  "read past nested block comment indicators (pound-bar bar-pound)
   and return either at end of file or when nested comments end"
  (let ((prev-char #\space)
        (depth 1))
    (loop 
      (let ((char (read-char stream nil :eof)))
        (cond ((eq char :eof) ;done: end of file
               (return nil))
              ((and (char-equal #\# char)
                    (char-equal #\| prev-char))
               (decf depth)
               (if (= depth 0) (return nil) (setf prev-char char)))
              ((and (char-equal #\| char)
                    (char-equal #\# prev-char))
               (incf depth)
               (setf prev-char char))
              (t (setf prev-char char)))))))



(defun QUICK-DOC-COMMENT (string outstream last-printed)
  
  "print comment string to outstream.  Last-printed may be :comment or
   some other value.  Rules here are:
    1.  only ;;; comments are printed
    2.  if last-printed was not a comment, then a newline is emitted before the
        comment string."
  
  (when (and (>= (length string) 3)
             (char-equal #\; (elt string 0) (elt string 1) (elt string 2)))
    (unless (eq last-printed :comment) (format outstream "~%"))
    (format outstream "~%~a" (subseq string 3))))


(defun QUICK-DOC-FORM (form outstream &optional (print? t))

  "input is a form and a stream, output to the stream documentation
    about the input form if it is one of the recognized types.
    If print? is t, then print brief description of unrecognized types,
    (otherwise print nothing in this case)"

  (cond 
   ((symbolp form)
    nil)
   
   ((stringp form)
    (format outstream "~%~%~a" form))
   
   ((eq (first form) 'defdoc)
    (dolist (doc (rest form))
      (format outstream "~%~%~a" doc)))
   
   ((eq (first form) 'defvar)
    (format-defvar form outstream))
   
   ((eq (first form) 'defclass)
    (format-defclass form outstream))
   
   ((eq (first form) 'defun)
    (format-defun form outstream))
   
   ((eq (first form) 'defmethod)
    (format-defmethod form outstream))
   
   ((eq (first form) 'defparameter)
    (format-defparameter form outstream))
   
   ((eq (first form) 'defconstant)
    (format-defconstant form outstream))

   ((eq (first form) 'defmacro)
    (format-defmacro form outstream))
   
   ((eq (first form) 'let)
    (format-let form outstream))
   
   ((eq (first form) 'let)
    (format-let form outstream))
   
   ((eq (first form) 'defgeneric)
    (format-defgeneric form outstream))
   
   ((eq (first form) 'define-function)
    (format-define-function form outstream))
   
   ((eq (first form) 'define-rule)
    (format-define-rule form outstream))
   
   ((eq (first form) 'setf)
    ) ;; ignore toplevel setfs
   
   (t
    (if print?
      (format outstream "~%~%?? ~a ~a~%" (first form) (second form))))))

#|
(quick-doc-comment ";;;" *standard-output* :comment)
(format-defvar '(defvar *3col-ternary-table* (make-hash-table :test #'equal)
                  "a piece of shit"))
(format-defconstant '(defconstant RTOD (/ 180 (coerce pi 'double-float))
  "Conversion factor from radians to degrees:  180/PI"))
(format-defparameter '(defparameter *not-in-domain* -1
  "a numeric value (fixnum) which, when stored in the var conflicts 
    array, indicates that the value is not in the var's domain"
  ))
(format-defun '(defun MAKE-VAR (&rest args
                       &key
                       CSP 
                       number 
                       name 
                       (var-type 'var)
                       &allow-other-keys)
  "Create new var instance attached to specified CSP.
    Call method INIT-VAR on new instance passing through args to this fcn.
    CSP, number are required.  Name is optional (defaults to nil).
    Var instance is returned."
  (let ((instance
         (make-instance var-type
                        :number number
                        :name name
                        :csp CSP)))
    (init-var instance args)
    (attach-var CSP instance)
    instance)))
(format-defun '(defun FOO () "no args for this function" 23))
(format-defmethod '(defmethod INIT-CSP ((c CSP) args) 'foo
  "Type-specific CSP initialization.  Default does nothing.
    Called by make-csp. Args is list of keyword value arguments."
  ))
(format-defmethod '(defmethod RESET-CSP :AFTER ((c CAP-CSP))
  ;; when called, it is assumed that all assignments have been unmade, and
  ;; that all values restored to domain.
  (dolist (cap-con (capacity-constraints c))
    (reset-constraint cap-con)
    (init-capacity-constraint-conflicts cap-con))))
(format-defmacro '(defmacro DO-FOR-ALL-UNIGNORED-VARS (&key var csp (predicate t) form)
  "iterate over all vars in a CSP"
  `(dolist (,var (var-list ,csp))
     (when ,(if (eq t predicate) t predicate)
       ,form))))
(format-defclass '(defclass CAP-CSP (CSP)
  ((capacity-constraints
    :initform nil :accessor capacity-constraints
    :documentation 
    "list of capacity constraint instances"))))
(format-defgeneric '(defgeneric SAVE-ORIENT-CONSTRAINTS-TO-NETWORK (chronika stream)
  (:documentation 
    "1. Gathers up Orient From and Same Orient data and
     prints it to STREAM attached to .network file.
     The former data is used to recreate SPIKE data structures
     in SSC mode.
     2. Also save affects related to orient constraints for use
     by the NN scheduler.  This data is a table that records the
     result of simulating the commitment of a specific SU to a segment on 
     other SUs that are linked via ORIENT constraints.")))
(format-define-function '(define-function COLLECT-OBSETS ((:su :qscheduling))
  "For a specific SU, collect list of obsets with substructure too."
  (local-variables
    ((obsets nil)
     (:ob :qbs_obset))
    (assign :ob :su.first-ob-ptr)
    (iterate UNTIL (or (null :ob)
		       (and :ob.new-su?
			    (not (equal :ob :su.first-ob-ptr))))
	     DOING
	     (push (list :OBSET :ob.obset_id
			 :ALIGNMENTS (collect-alignments :ob))
		   obsets)
	     (assign :ob :ob.next-ptr))
    (nreverse obsets))))
(format-define-rule '(define-rule Set-Alignment-Target-ID
  "Set the target ID for the alignment"
  (FOR-EACH 
   :qalignment :al :do
   (local-variables
    (
     (:exp :qexposure)          ; The exposure being checked
     (true-target nil)          ; Whether a true target has been found
     (:target :idb-target)
     (pointing-target)
     )
    (assign :exp (First-PMDB-Exposure-In-Alignment :al))
    (WHILE (not (or (null :exp) true-target))
	   (cond                                ; If the target ID is not nil
	     ((and                               ; exposure is not a calibration
		(or (Populated-Qexposure :exp) (equalp :exp :al.exposure))
		:exp.pointing-coord-id :exp.pointing-ex
		(or
		  (not (equal :exp.SI_used "FGS")) ; Want only the primary target
		  (equal :exp.astrom_tgt "P")))    ; of an FGS exposre
	      (assign true-target t)
	      (assign :target :exp.pointing-ex.target)
	      (when :target
		(assign pointing-target :target.qtargets.target_id))))
	   (assign :exp (Next-PMDB-Exposure-In-Alignment :exp))
	   )
    (when (and (null pointing-target) (populated-qexposure :al.exposure)
	       (pseudo-external-exposure :al.exposure.idb-exposure))
      (assign pointing-target "NEP"))
    (when pointing-target
      (assign :al.prim_target pointing-target))))))
|#

(defun format-defvar (form &optional (stream t))
  (format stream "~%~%~a [variable]~%   ~a"
          (second form) (if (stringp (fourth form)) (fourth form) "")))

(defun format-defparameter (form &optional (stream t))
  (format stream "~%~%~a [parameter] value: ~a~%   ~a"
          (second form) (third form) 
          (if (stringp (fourth form)) (fourth form) "")))

(defun format-defconstant (form &optional (stream t))
  (format stream "~%~%~a [constant] value: ~a~%   ~a"
          (second form) (third form) 
          (if (stringp (fourth form)) (fourth form) "")))

(defun format-defun (form &optional (stream t))
  (format stream "~%~%~a [function]~%   ~a~%   ~a"
          (second form) 
	  (if (third form) (third form) "()")
          (if (stringp (fourth form)) (fourth form) "")))

(defun format-define-function (form &optional (stream t))
  (format stream "~%~%~a [TRANS function]~%   ~a~%   ~a"
          (second form)
	  (if (third form) (third form) "()")
          (if (stringp (fourth form)) (fourth form) "")))

(defun format-define-rule (form &optional (stream t))
  (format stream "~%~%~a [TRANS rule]~%   ~a"
          (second form)
          (if (stringp (third form)) (third form) "")))

(defun format-defgeneric (form &optional (stream t))
  (format stream "~%~%~a [generic function]~%   ~a~%   ~a"
          (second form)
	  (if (third form) (third form) "()")
          (if (stringp (getf (fourth form) :documentation))
	      (getf (fourth form) :documentation)
	      "")))


(defun format-defmethod (form &optional (stream t))
  (cond ((listp (third form))
         (format stream "~%~%~a [method]~%   ~a~%   ~a"
                 (second form) (third form) 
                 (if (stringp (fourth form)) (fourth form) "")))
        (t
         (format stream "~%~%~a [~a method]~%   ~a~%   ~a"
                 (second form) (third form) (fourth form) 
                 (if (stringp (fifth form)) (fifth form) "")))))

(defun format-defmacro (form &optional (stream t))
  (format stream "~%~%~a [macro]~%   ~a~%   ~a"
          (second form) (third form) 
          (if (stringp (fourth form)) (fourth form) "")))

(defun format-let (form &optional (stream t))
  (dolist (let-form (cddr form))
    (cond 
     ((eq (first let-form) 'defvar)
      (format-defvar let-form stream))
     
     ((eq (first let-form) 'defclass)
      (format-defclass let-form stream))
     
     ((eq (first let-form) 'defun)
      (format-defun let-form stream))
     
     ((eq (first let-form) 'defmethod)
      (format-defmethod let-form stream))
     
     ((eq (first let-form) 'defparameter)
      (format-defparameter let-form stream))
     
     ((eq (first let-form) 'defmacro)
      (format-defmacro let-form stream))
     
     (t
      (format stream "~%~%?? ~a~%" (first let-form) (second let-form))))))


(defun format-defclass (form &optional (stream t))
  (format stream "~%~%~a [class]~%  parent classes: ~a~%  slots:"
          (second form) (third form))
  (dolist (slot (fourth form))
    (format stream "~%   ~a accessor: ~a"
            (first slot)
            (getf (rest slot) :accessor "none"))
    (if (getf (rest slot) :reader)
      (format stream "  reader: ~a" (getf (rest slot) :reader)))
    (if (getf (rest slot) :writer)
      (format stream "  writer: ~a" (getf (rest slot) :writer)))
    (format stream "~%     ~a" (getf (rest slot) :documentation ""))))


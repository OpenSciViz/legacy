;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; time-to-segments.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; class for defining time to segment mappings for CSP
;;; split off from spike-csp.lisp 14May91 MDJ
;;;
;;; $Log: time-to-segments.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.4  1991/12/01  00:28:01  johnston
;;; no changes
;;;
;;; Revision 1.3  1991/11/26  15:47:15  johnston
;;; generalized to permit arbitrary segment sizes
;;;
;;; Revision 1.2  1991/06/11  12:12:50  johnston
;;; changed segment boundaries to include times: now all times map
;;; uniquely to segments
;;;
;;; Revision 1.1  91/06/05  23:18:38  johnston
;;; Initial revision
;;; 
;;;



;;; ================================================================
;;; Application-level methods for TIME-TO-SEGMENT-MAPPING
;;; ================================================================

(defgeneric INIT-TIME-TO-SEGMENT-MAPPING (tsm 
					   &key 
					   segment-start 
					   segment-duration 
					   segment-count)
  (:documentation
  "initialize mapping:  for uniform contiguous segments, supply the three
    arguments:
    :segment-start - start time of first segment
    :segment-duration - uniform duration of each segment
    :segment-count - number of segments
   Other segment types will need different argument lists, TBD."))



(defgeneric SHOW-SEGMENTS (tsm &optional stream)
  (:documentation
    "printed representation of segments to stream"))


(defgeneric START-OF-SEGMENT (tsm i)
  (:documentation
    "Return the start time of the ith segment - in ticks"))


(defgeneric END-OF-SEGMENT (tsm i)
  (:documentation 
    "Return the last time of the ith segment - in ticks"))


(defgeneric DURATION-OF-SEGMENT (tsm i)
  (:documentation
    "Duration of the ith segment in ticks"))


(defgeneric START-OF-FIRST-SEGMENT (tsm)
  (:documentation
    "Return start time of first segment in ticks"))


(defgeneric END-OF-LAST-SEGMENT (tsm)
  (:documentation
    "Return the end time of the last segment in ticks"))


(defgeneric TIME-TO-SEGMENT (tsm time)
  (:documentation
    "Input time in ticks, output segment number or nil if not
      within any segment"))


(defgeneric FIRST-SEGMENT-INCLUDING-TIME (tsm time)
  (:documentation
    "Return the number of the first segment which includes
      the specified time (ticks)"))


(defgeneric LAST-SEGMENT-INCLUDING-TIME (tsm time)
  (:documentation
    "Return the number of the last segment which includes
      the specified time (ticks)"))


(defgeneric TIME-INTERVAL-TO-OVERLAPPING-SEGMENT-RANGE (tsm interval)
  (:documentation
    "interval is list (start end) in ticks; return list (start-seg end-seg)
    of segments overlapping interval, or nil if none"))


(defgeneric TIME-INTERVAL-TO-INCLUDED-SEGMENT-RANGE (tsm interval)
  (:documentation
      "interval is list (start end) in ticks; return list (start-seg end-seg)
      of segments fully included in interval"))


(defgeneric SEGMENT-TO-TIME-RANGE (tsm segment)
 (:documentation
   "input segment number, output time range as list (start end) in ticks inclusive"))


(defgeneric ERROR-CHECK-SEGMENT (tsm i)
  (:documentation
    "check that segment i is legal: return nil if so, otherwise generate error"))


;;; ================================================================
;;; CLASS:  TIME-TO-SEGMENT-MAPPING
;;;
;;; In contrast to former segmentations, here segments are numbered from
;;; 0:  this makes the mapping to bins easier to manage
;;;
;;; This is a mixin - not to be instantiated directly. This class handles
;;; only uniform contiguous segments - could be easily generalized to
;;; more general segment types
;;; ================================================================

;;; Mapping of segments to discrete times looks like the following:
;;; 
;;;     time   t1  t2  t3  t4  t5  t6  t7  t8  t9
;;;     ---+---+---+---+---+---+---+---+---+---+---
;;;          |       |       |       |       |
;;;      seg #   0       1       2       3  
;;; 
;;; In this model, if e.g. t1=1 etc, then
;;;   start-time-of-first-segment = 1
;;;   duration-of-uniform-segment = 2
;;;   number-of-segments = 4
;;; Derived end-time-of-last-segment = 8
;;; All times belong to exactly ONE segment.
;;; i.e. t3 belongs to segment 1.

#|
(setf zzz (make-instance 'time-to-segment-mapping2))
(init-time-to-segment-mapping
 zzz :segment-start 1 :segment-duration 2 :segment-count 4)
(describe zzz)
(dotimes (i 4)
  (format t "~%seg ~d start ~d end ~d"
	  i (start-of-segment zzz i) (end-of-segment zzz i)))
(dotimes (i 11)
  (format t "~%time ~d time-to-segment ~d first incl ~a last incl ~a"
          i 
	  (time-to-segment zzz i)
	  (first-segment-including-time zzz i)
          (last-segment-including-time zzz i)))

|#

(defclass TIME-TO-SEGMENT-MAPPING ()
  
  ;; slots defining segmentation
  ((start-time-of-first-segment
    :initform 0 :accessor start-time-of-first-segment
    :documentation 
    "start time for first segment")
   (duration-of-uniform-segment
    :initform 1 :accessor duration-of-uniform-segment
    :documentation 
    "segment duration")
   (total-number-of-segments
    :initform 10 :accessor total-number-of-segments
    :documentation 
    "number of segments")

   ;; computed slots
   (end-time-of-last-segment
    :initform nil :accessor end-time-of-last-segment
    :documentation 
    "computed end time of last segment")
   ))


;;; ================================================================
;;; Application-level methods for TIME-TO-SEGMENT-MAPPING
;;; ================================================================

;;; NOTE:  DEFGENERICS FOR METHODS BELOW ARE ALL DEFINED ABOVE


(defmethod INIT-TIME-TO-SEGMENT-MAPPING ((tsm TIME-TO-SEGMENT-MAPPING)
					 &key 
					 (segment-start 0) 
					 (segment-duration 1) 
					 (segment-count 10)
                                         &allow-other-keys)

  ;; documentation in DEFGENERIC
    
  ;; store slot values
  ;; specified the next three fields to determine mapping segments to time
  (setf (start-time-of-first-segment tsm) segment-start)
  (setf (duration-of-uniform-segment tsm) segment-duration)
  (setf (total-number-of-segments tsm) segment-count)
  
  ;; computed slots
  (setf (end-time-of-last-segment tsm)
    (1- (+ (start-time-of-first-segment tsm)
           (* (total-number-of-segments tsm) 
	      (duration-of-uniform-segment tsm)))))
  )

(defmethod SHOW-SEGMENTS ((tsm TIME-TO-SEGMENT-MAPPING) &optional (stream t))

  ;; documentation in DEFGENERIC

  (format stream
          "~%~d segments, start of first ~d, end of last ~d, duration ~d"
          (total-number-of-segments tsm)
          (start-time-of-first-segment tsm)
          (end-time-of-last-segment tsm)
          (duration-of-uniform-segment tsm))
  (dotimes (i (total-number-of-segments tsm))
    (format t "~%seg: ~d  ~d-~d" i 
            (start-of-segment tsm i) 
            (end-of-segment tsm i)))
  )

(defmethod START-OF-SEGMENT ((tsm time-to-segment-mapping) i)

  ;; documentation in DEFGENERIC

  (error-check-segment tsm i)
  (+ (start-time-of-first-segment tsm) 
     (* i (duration-of-uniform-segment tsm))))


(defmethod END-OF-SEGMENT ((tsm time-to-segment-mapping) i)

  ;; documentation in DEFGENERIC

  (error-check-segment tsm i)
  (+ (start-of-segment tsm i) (duration-of-uniform-segment tsm) -1))


(defmethod DURATION-OF-SEGMENT ((tsm time-to-segment-mapping) i)

  ;; documentation in DEFGENERIC

  (error-check-segment tsm i)
  (duration-of-uniform-segment tsm))


(defmethod START-OF-FIRST-SEGMENT ((tsm time-to-segment-mapping))

  ;; documentation in DEFGENERIC

  (start-time-of-first-segment tsm))


(defmethod END-OF-LAST-SEGMENT ((tsm time-to-segment-mapping))

  ;; documentation in DEFGENERIC

  (end-time-of-last-segment tsm))


(defmethod TIME-TO-SEGMENT ((tsm time-to-segment-mapping) time)

  ;; documentation in DEFGENERIC

  (let ((start (start-time-of-first-segment tsm))
        (end   (end-time-of-last-segment tsm)))
    (cond ((or (< time start)
               (> time end))
           nil ;; not range
           )
          (t (floor (- time start) 
                    (duration-of-uniform-segment tsm))))))


(defmethod FIRST-SEGMENT-INCLUDING-TIME ((tsm time-to-segment-mapping) time)
  (time-to-segment tsm time))

(defmethod LAST-SEGMENT-INCLUDING-TIME ((tsm time-to-segment-mapping) time)
  (time-to-segment tsm time))

(defmethod TIME-INTERVAL-TO-OVERLAPPING-SEGMENT-RANGE 
  ((tsm time-to-segment-mapping) interval)

  ;; documentation in DEFGENERIC

  (let* ((seg-start (or (first-segment-including-time tsm (first interval))
                        (if (< (first interval) 
                               (start-time-of-first-segment tsm))
                          0)))
         (seg-end (or (last-segment-including-time tsm (second interval))
                      (if (> (second interval) 
                             (end-time-of-last-segment tsm)) 
                        (1- (total-number-of-segments tsm))))))
    (cond
     ((or (null seg-start) (null seg-end))
      nil) ;; out of range
     (t 
      (list seg-start seg-end)))))


(defmethod TIME-INTERVAL-TO-INCLUDED-SEGMENT-RANGE 
  ((tsm time-to-segment-mapping) interval)

  ;; documentation in DEFGENERIC

  (let* ((seg-start (or (first-segment-including-time tsm (first interval))
                        (if (< (first interval) 
                               (start-time-of-first-segment tsm))
                          0)))
         (seg-end (or (last-segment-including-time tsm (second interval))
                      (if (> (second interval) 
                             (end-time-of-last-segment tsm)) 
                        (1- (total-number-of-segments tsm))))))
    (cond
     ((or (null seg-start) (null seg-end))
      nil) ;; out of range
     (t 
      ;; eliminate pieces of segments
      (if (> (first interval) (start-of-segment tsm seg-start))
        (incf seg-start))
      (if (< (second interval) (end-of-segment tsm seg-end))
        (decf seg-end))
      ;; result
      (if (>= seg-end seg-start) (list seg-start seg-end))))))


(defmethod SEGMENT-TO-TIME-RANGE ((tsm time-to-segment-mapping) segment)

  ;; documentation in DEFGENERIC

  (error-check-segment tsm segment)
  (list (start-of-segment tsm segment)
        (end-of-segment tsm segment)))


(defmethod ERROR-CHECK-SEGMENT ((tsm time-to-segment-mapping) i)

  ;; documentation in DEFGENERIC

  (unless (and (integerp i)
               (>= i 0)
               (< i (total-number-of-segments tsm)))
    (error "segment ~a not legal for ~a" i tsm)))


;;; ================================================================
;;; CLASS:  GENERAL-TIME-TO-SEGMENT-MAPPING
;;;
;;; subtime of time-to-segment-mapping that takes an arbitrary list
;;; of intervals to define the segments.  Thus they may be 
;;; non-uniform.
;;; ================================================================


(defclass GENERAL-TIME-TO-SEGMENT-MAPPING (TIME-TO-SEGMENT-MAPPING)
  ((segment-list
    :initarg nil :initform nil
    :documentation
    "List of the form ((t1 t2)(t3 t4)...) defining the segments,
      where t1 = first time of first segment, t2 = last time of first segment,
      etc.")
   (segment-table
    :initarg nil :initform nil
    :documentation
    "Hash table, key is segment number, value is list (start end) where
      start is first time of segment, end is last time in segment")
   ))


(defmethod INIT-TIME-TO-SEGMENT-MAPPING ((tsm general-time-to-segment-mapping)
                                         &key
                                         segments
                                         segment-start
                                         segment-duration
                                         segment-count
                                         &allow-other-keys)
  
  "Initialize general time-to-segment-mapping.  
    Either segment-start, segment-duration, segment-count can be provided,
    in which case a uniform segmentation is generated.  Or
    segments can be provided, for general segment boundaries.
    segments must be a list of start/end of each segment."

  (cond ((and segment-start segment-duration segment-count (not segments))
         ;; uniform: construct segment list
         (dotimes (i segment-count)
           (push (list (+ segment-start (* i segment-duration))
                       (+ segment-start (* i segment-duration)
                          segment-duration -1))
                 segments))
         (setf segments (reverse segments)))
        ((and segments (not segment-start) 
              (not segment-duration) (not segment-count))
         ;; OK, use segments as supplied
         )
        (t (error 
            "either segments or segment start,duration,count required")))

  ;; store slot values
  (with-slots 
    (start-time-of-first-segment 
     duration-of-uniform-segment
     total-number-of-segments
     end-time-of-last-segment
     segment-list
     segment-table)
    tsm
    
    (setf start-time-of-first-segment (first (first segments)))
    (setf end-time-of-last-segment (second (first (last segments))))
    (setf duration-of-uniform-segment nil)
    (setf total-number-of-segments (length segments))
    (setf segment-list segments)
    (setf segment-table (make-hash-table :test #'equal))
    
    (let ((counter 0))
      (dolist (segment segments)
        (setf (gethash counter segment-table) segment)
        (incf counter)))
    
    ))


(defmethod SHOW-SEGMENTS ((tsm general-time-to-segment-mapping) 
                          &optional (stream t))

  (format stream
          "~%~d segments, start of first ~d, end of last ~d"
          (total-number-of-segments tsm)
          (start-time-of-first-segment tsm)
          (end-time-of-last-segment tsm))
  (dotimes (i (total-number-of-segments tsm))
    (format t "~%seg: ~d  ~d-~d" i 
            (start-of-segment tsm i) 
            (end-of-segment tsm i)))
  )


(defmethod START-OF-SEGMENT ((tsm general-time-to-segment-mapping) i)
  (error-check-segment tsm i)
  (first (gethash i (slot-value tsm 'segment-table))))


(defmethod END-OF-SEGMENT ((tsm general-time-to-segment-mapping) i)
  (error-check-segment tsm i)
  (second (gethash i (slot-value tsm 'segment-table))))


(defmethod DURATION-OF-SEGMENT ((tsm general-time-to-segment-mapping) i)
  (error-check-segment tsm i)
  (let ((start-end (gethash i (slot-value tsm 'segment-table))))
    (1+ (- (second start-end) (first start-end)))))


(defmethod START-OF-FIRST-SEGMENT ((tsm general-time-to-segment-mapping))
  (start-of-segment tsm 0))


(defmethod END-OF-LAST-SEGMENT ((tsm general-time-to-segment-mapping))
  (end-of-segment tsm (1- (total-number-of-segments tsm))))


(defmethod TIME-TO-SEGMENT ((tsm general-time-to-segment-mapping) time)
  (let ((start (start-time-of-first-segment tsm))
        (end   (end-time-of-last-segment tsm)))
    (cond ((or (< time start)
               (> time end))
           nil ;; not range
           )
          (t (let ((counter 0))
               (dolist (segment (slot-value tsm 'segment-list))
                 (if (and (>= time (first segment))
                          (<= time (second segment)))
                   (return counter))
                 (incf counter)))))))

;; no change to FIRST-SEGMENT-INCLUDING-TIME

;; no change to LAST-SEGMENT-INCLUDING-TIME

;; no change to TIME-INTERVAL-TO-OVERLAPPING-SEGMENT-RANGE 

;; no change to TIME-INTERVAL-TO-INCLUDED-SEGMENT-RANGE

(defmethod SEGMENT-TO-TIME-RANGE ((tsm general-time-to-segment-mapping) segment)
  ;; more efficient
  (error-check-segment tsm segment)
  (gethash segment (slot-value tsm 'segment-table)))

;; no change to ERROR-CHECK-SEGMENT

;;; -----------------------------------------------------------------------
;;; test cases

#| TEST:
(setf zzz (make-instance 'time-to-segment-mapping))
(init-time-to-segment-mapping zzz :segment-start 5 :segment-duration 2
                              :segment-count 3)
(show-segments zzz)

(setf zzz (make-instance 'general-time-to-segment-mapping))
(init-time-to-segment-mapping zzz :segment-start 5 :segment-duration 2
                              :segment-count 3)
(init-time-to-segment-mapping zzz :segments '((5 6)(7 8)(9 10)))
(init-time-to-segment-mapping zzz :segments '((1 4)(5 6)(7 7)(20 25)))
(describe zzz)

(dotimes (i (total-number-of-segments zzz))
  (format t "~%~d - ~d" (start-of-segment zzz i) (end-of-segment zzz i))
  (format t "  dur ~d" (duration-of-segment zzz i)))
(error-check-segment zzz -1)
(error-check-segment zzz 4)
(error-check-segment zzz 2)
(show-segments zzz)
(describe zzz)
(dolist (form '(
		(error-check-segment zzz 3)
		(start-of-segment zzz 2)
		(end-of-segment zzz 2)
		(duration-of-segment zzz 1)
		(start-of-first-segment zzz)
		(end-of-last-segment zzz)
		(total-number-of-segments zzz)))
  (format t "~%~a -> ~a" form (eval form)))

(dotimes (i 13)
  (format t "~%~a -> seg ~a" i (time-to-segment zzz i)))
(dotimes (i 13)
  (format t "~%time including ~a first: ~a  last: ~a" 
          i (first-segment-including-time zzz i)
          (last-segment-including-time zzz i)))
(dolist (form '(
		(time-interval-to-overlapping-segment-range zzz '(0 20))
		(time-interval-to-overlapping-segment-range zzz '(0 5))
		(time-interval-to-overlapping-segment-range zzz '(0 6))
		(time-interval-to-overlapping-segment-range zzz '(0 7))
		(time-interval-to-overlapping-segment-range zzz '(0 8))
		(time-interval-to-overlapping-segment-range zzz '(6 7))
		(time-interval-to-overlapping-segment-range zzz '(6 8))
		(time-interval-to-overlapping-segment-range zzz '(6 9))
		(time-interval-to-overlapping-segment-range zzz '(6 10))
		(time-interval-to-overlapping-segment-range zzz '(6 11))
		(time-interval-to-overlapping-segment-range zzz '(6 12))
                (time-interval-to-overlapping-segment-range zzz '(7 7))
		(time-interval-to-overlapping-segment-range zzz '(12 20))))
  (format t "~%~a -> ~a" form (eval form)))

(dolist (form '(
		(time-interval-to-included-segment-range zzz '(0 20))
		(time-interval-to-included-segment-range zzz '(0 5))
		(time-interval-to-included-segment-range zzz '(0 6))
		(time-interval-to-included-segment-range zzz '(0 7))
		(time-interval-to-included-segment-range zzz '(6 7))
		(time-interval-to-included-segment-range zzz '(6 8))
		(time-interval-to-included-segment-range zzz '(6 9))
		(time-interval-to-included-segment-range zzz '(6 10))
		(time-interval-to-included-segment-range zzz '(6 11))
		(time-interval-to-included-segment-range zzz '(6 12))
		(time-interval-to-included-segment-range zzz '(12 20))))
  (format t "~%~a -> ~a" form (eval form)))
(dotimes (i 3)
  (format t "~%~a -> ~a" i (segment-to-time-range zzz i)))
|#



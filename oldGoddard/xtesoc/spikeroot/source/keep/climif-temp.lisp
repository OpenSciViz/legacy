
(eval-when (compile load eval)
(in-package climif))

;;;;    INPUT-FRAME is the base frame to contol other frames.

(clim:define-application-frame INPUT-FRAME
   
   ()
   (
   (current-var :initform nil)

   )

   (:command-table (INPUT-COMMAND-TABLE
		    :menu (("Exit"        :command (com-csp-schedule-exit))
			   )))

   (:command-definer define-input-command)

   (:panes
     ((menu	:command-menu)
      (commands   :application
		  :scroll-bar nil
		  :default-text-style '(:sans-serif :roman :very-small)
		  :incremental-redisplay nil
		  :display-after-command nil
		  :display-function 'display-command-lists)

      (prep-file  :application
		  :scroll-bar :both
		  :default-text-style '(:sans-serif :roman :very-small)
		  :incremental-redisplay nil
		  :display-after-command nil
		  :display-function 'display-command-lists)
			
   ))

   (:layout
     ((default
	(:column 1
	     (menu :compute)
	     (commands :rest)
     ))))

)

;;;;;    TARGET-FILE-FRAME is used to display target files in 
;;;;;    spikeroot/obs, and choose files for prep and load.

(clim:define-application-frame TARGET-FILE-FRAME

  ()
  (
  (current-var :initform nil)
  )

  (:command-table (TARGET-FILE-COMMAND-TABLE
		   :menu (("Exit" :command (com-csp-schedule-exit))
		   )))

  (:command-definer define-target-file-command)
  (:panes
   ((menu         :command-menu)
    (target-list  :application
		  :scroll-bar :both
		  :default-text-style '(:sans-serif :roman :very-small)
		  :incremental-redisplay t
		  :display-after-command t
		  :display-function 'display-target-file-name)
    (input-pane   :accept-values
		  :scroll-bar :both
		  :default-text-style '(:sans-serif :roman :very-small)
		  :incremental-redisplay nil
		  :display-after-command nil
		  :display-function 'chosen-target-file)
  ))

  (:layout
    ((default 
       (:column 1
		(menu :compute)
		(target-list 2/3)
		(input-pane :rest))
	)))
)


;;;;    WEEK-SELECTION-FRAME is used to chose a segment for a
;;;;    short-term-winodw

(clim:define-application-frame WEEK-SELECTION-FRAME

   ()
   (
   (current-var :initform nil)
   )

   (:command-table (WEEK-SELECTION-COMMAND-TABLE
		    :menu (("Exit" :command (com-csp-schedule-exit))
		    )))

   (:command-definer define-week-selection-command)
   (:panes
     ((menu		:command-menu)
      (week-list        :application
			:scroll-bar :both
			:default-text-style '(:sans-serif :roman :very-small)
			:incremental-redisplay t
			:display-after-command t
			:display-function 'display-week-selection-list)
    (input-pane   :accept-values
		  :scroll-bar :both
		  :default-text-style '(:sans-serif :roman :very-small)
		  :incremental-redisplay nil
		  :display-after-command nil
		  :display-function 'chosen-week-segment)
   ))

   (:layout
     ((default
       (:column 1
		(menu :compute)
		(week-list 2/3)
		(input-pane :rest))

    )))
)


;;;;  Set starting date, ending date, duration, etc
;;;   This button must be setected before starting the long-term scheduler.

(clim:define-command (COM-PARAMETER-LIST
   		      :command-table INPUT-COMMAND-TABLE
		      :menu "Set Parameters"
		      :name "Set Parameters")
  ()

;;;; these are default values (1993 1 1) to (1993 6 24).
;;;; the number of weeks are computed later by "compute-segment"
;;;; and hence we do not need to specify.

 (setf st-start 4988.5)
 (setf st-end   5162.5)
 (setf seg-start 4988.5)
 (setf time-quantum 1.0)
 (setf duration 7.0)

 (let ((stream (clim:get-frame-pane clim:*application-frame* 'menu))
      )

(clim:accepting-values
     (stream :resynchronize-every-pass t :own-window t
	     :label "Parameters")
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (setf st-start
   	 	(clim:accept 'float
   			     :default st-start
			     :prompt "*astrod-schedule-start*     "
			     :stream stream))))
(fresh-line stream)
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (setf st-end
   	 	(clim:accept 'float
   			     :default st-end
			     :prompt "*astrod-schedule-end*       "
			     :stream stream))))
(fresh-line stream)
     (clim:with-text-family (:fix  stream)
	 (clim:with-text-size (:small stream)
	 (setf seg-start
   	 	(clim:accept 'float
   			     :default seg-start
			     :prompt "*astrod-segment-start*      "
			     :stream stream))))
(fresh-line stream)
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (setf time-quantum
   	 	(clim:accept 'float
   			     :default time-quantum
			     :prompt "*astrod-time-quantum*       "
			     :stream stream))))
(fresh-line stream)
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (setf duration
   	 	(clim:accept 'float
   			     :default duration
			     :prompt "*astrod-segment-duration*   "
			     :stream stream))))
)

(setf user::*astrod-schedule-start* st-start)
(setf user::*astrod-schedule-end* st-end)
(setf user::*astrod-segment-start* seg-start)
(setf user::*astrod-time-quantum* time-quantum)
(setf user::*astrod-segment-duration* duration)

;;;; the next function is in astrod-functions.lisp
(user::compute-segment)

)
)


;;;; selecting input files

(clim:define-command (COM-PREP-FILE
   		      :command-table INPUT-COMMAND-TABLE
		      :menu "Select Target Files"
		      :name "Select Target Files")
  ()
  (with-slots (current-var) clim:*application-frame*
  (start-target-file-selection) 
))


;;;;; starting the long-term scheduler. Prep and load are performed
;;;;  under this command

(clim:define-command (COM-LONG-TERM
   		      :command-table INPUT-COMMAND-TABLE
		      :menu "Long-Term"
		      :name "Long-Term")

  ()
  (with-slots (current-var) clim:*application-frame*
  (user::astrod-init-prep-and-load selected-file)
  (start-lt-schedule-application (user::obs-csp 
				   user::*astrod-long-term-schedule*))))


;;;   selecting a week segment

(clim:define-command (COM-SELECT-WEEK
   		      :command-table INPUT-COMMAND-TABLE
		      :menu "Select a Week"
		      :name "Select a Week")

  ()
  (with-slots (current-var) clim:*application-frame*
  (setf week-segment 0))
  (start-week-selection-list))


;;;; starting the short-term scheduler

(clim:define-command (COM-SHORT-TERM
   		      :command-table INPUT-COMMAND-TABLE
		      :menu "Short-Term"
		      :name "Short-Term")
  ()
  (with-slots (current-var) clim:*application-frame*
  
  (user::make-astrod-short-term-schedule week-segment)
  (start-st-schedule-application (user::obs-csp
				    user::*astrod-short-term-schedule*))))



;;;;;   command list display

(defmethod DISPLAY-COMMAND-LISTS ((frame INPUT-FRAME) stream)

(with-slots (current-var) frame

(clim:enable-command 'COM-PARAMETER-LIST frame)
(clim:enable-command 'COM-PREOP-FILE frame)
(clim:enable-command 'COM-LONG-TERM frame)
(clim:enable-command 'COM-SELECTE-WEEK frame)
(clim:enable-command 'COM-SHORT-TERM frame)
)
 )


;;;;this function shows a list of terget file on TARGET-FILE-FRMAE
;;;;Although it is developed for a user to select files from the list,
;;;; I could not make it work and it just displays the list of target
;;; files. The selection of files is done in CHOSEN-TARGET-FILE.

(defmethod DISPLAY-TARGET-FILE-NAME ((frame TARGET-FILE-FRAME) stream)

(with-slots (current-var) frame

(setf selected-or-not nil)
(setf file-list (user::astrod-targ-files))
(setf selected-file nil)
(let* ((counter 0)
       (lh (+ 3 (clim:stream-line-height stream)))
       (cy 0)
       )
  
  (clim:updating-output
    (stream :unique-id :display-file-name)

  (dolist (file-name file-list)
   (incf counter)
   (setf cy (* counter lh))

   (clim:updating-output
    (stream :unique-id counter
	    :cache-value file-name
	    :cache-test #'equal)

   (clim:with-output-as-presentation
       (
	:object file-name
	:type 'string
	:stream stream
	:single-box t
	:allow-sensitive-inferiors nil)
       (draw-var-circle
	stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh)
	:filled selected-or-not)

	(cond 
	 ((eq file-name current-var)
	  (clim:surrounding-output-with-border
	      (stream :shape :rectangle)
	      (clim:draw-text* stream
		(format nil "~a" file-name)
		(* 3 lh) (+ cy lh) :text-family :fix))
	  (cond ((equal selecte-or-not nil)
	           (setf selected-or-not t)
   	           (push file-name selected-file))
	        (t (setf selected-or-not nil)
	           (remove file-name selected-file)))
	    )

	 (t (clim:draw-text* stream
	       (format nil "~a" file-name)
	       (* 3 lh) (+ cy lh) :text-family :fix)
	    (setf selected-or-not nil))
	 ))))))))


;;;;   this function makes a user to type in name of a target file
;;;   on the pane. The user can choose more than one files.

(defmethod CHOSEN-TARGET-FILE ((frame TARGET-FILE-FRAME) stream)

(with-slots (current-var) frame

(clim:accepting-values
     (stream :resynchronize-every-pass t :own-window nil )
	     
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (let ((file
   	 	(clim:accept 'string
			     :prompt "file name "
			     :stream stream)))
		 (push file selected-file)	     
			     )))

;;;; somehow the file names are duplicated in the "selected-file"
;;;; they needed to be removed.

     (setf selected-file (remove 'nil selected-file))
     (setf selected-file (remove-duplicates selected-file))
  )
  )
  )


;;;;   thie function lists the week segment. As is in 
;;;;   DISPLAY-TARGET-FILE-NAME, I could not make it work. So
;;;    the selection of a segment is done separately.

(defmethod DISPLAY-WEEK-SELECTION-LIST ((frame WEEK-SELECTION-FRAME) stream)

(with-slots (current-var) frame

(setf selected-or-not nil)
(setf selected-file nil)
(let* ((counter 0)
       (lh (+ 3 (clim:stream-line-height stream)))
       (cy 0)
       (bins (user::bin-list *astrod-long-term-schedule*))
       )
  
  (clim:updating-output
    (stream :unique-id :display-file-name)

  (dolist (var bins)
   (incf counter)
   (setf cy (* counter lh))

   (clim:updating-output
    (stream :unique-id counter
	    :cache-value  var
	    :cache-test #'equal)

   (clim:with-output-as-presentation
       (
	:object var
	:type 'var
	:stream stream
	:single-box t
	:allow-sensitive-inferiors nil)
       (draw-var-circle
	stream (* 0.75 lh) (+ cy (* 0.75 lh)) (* 0.25 lh) (* 0.30 lh)
	:filled selected-or-not)

	(cond 
	 ((eq var current-var)
	  (clim:surrounding-output-with-border
	      (stream :shape :rectangle)
	      (clim:draw-text* stream
		     (format nil "~d ~a-~a ~,2f% filled"
		     (first var) 
		     (user::format-abs-time (second var))
		     (user::format-abs-time (third var))
		     (sixth var))
		(* 3 lh) (+ cy lh) :text-family :fix))
		(setf week-segment (first var))
	    )

	 (t (clim:draw-text* stream
		     (format nil "~d ~a-~a ~,2f% filled"
		     (first var) 
		     (user::format-abs-time (second var))
		     (user::format-abs-time (third var))
		     (sixth var))
	       (* 3 lh) (+ cy lh) :text-family :fix)
	    (setf selected-or-not nil))
	 ))))))))


;;;   this function makes a user to type the number of the segment
;;;   used in the short-term scheduler

(defmethod CHOSEN-WEEK-SEGMENT ((frame WEEK-SELECTION-FRAME) stream)

(with-slots (current-var) frame

(clim:accepting-values
     (stream :resynchronize-every-pass t :own-window nil )
	     
     (clim:with-text-family (:fix stream)
	 (clim:with-text-size (:small stream)
	 (setf week-segment
   	 	(clim:accept 'integer
			     :prompt "Week Segment "
			     :stream stream))))

  )
  )
  )


;;;; starting input-frame

(defun START-INPUT-FRAME (&key  (run t) (frame-type 'INPUT-FRAME))

 
 (let ((frame (clim:make-application-frame
			frame-type
			:parent *clim-root*
			:width 200 :height 250
			:pretty-name "Input Window"
			)))
      (setf *THE-INPUT-FRAME* frame)

      (when run
	 #+:CCL (clim:run-frame-top-level frame)
         #+:Allegro (mp:process-run-function
		 "Input Window"
                #'(lambda () (clim:run-frame-top-level frame)))
	 )
 ))

;;; starting the parameter-set frame

(defun START-PARAMETER-SET (&key (run t) (frame-type 'PARAMETER-SET-FRAME))

 (let ((frame (clim:make-application-frame
			frame-type
			:parent *clim-root*
			:width 300 :height 300
			:pretty-name "Setting Paramters"
			)))
      (setf *THE-PARAMETER-FRAME* frame)

      (when run
       #+:CCL (clim:run-frame-top-level frame)
       #+:Allegro (mp:process-run-function
		"Parameter List"
	      #'(lambda () (clim:run-frame-top-level frame)))
	)

))

;;;; starting the target-file selection

(defun START-TARGET-FILE-SELECTION (&key (run t) (frame-type 'TARGET-FILE-FRAME))

 (let ((frame (clim:make-application-frame
			frame-type
			:parent *clim-root*
			:width 300 :height 300
			:pretty-name "Input Target Files"
			)))
	(setf *THE-TARGET-FILE-FRAME* frame)

	(when run
	 #+:CCL (clim:run-frame-top-level frame)
         #+:Allegro (mp:process-run-function
		 "Target Files"	
                #'(lambda () (clim:run-frame-top-level frame)))
	 )
)) 


;;;; starting the week-selection frame

(defun START-WEEK-SELECTION-LIST (&key (run t) (frame-type 'WEEK-SELECTION-FRAME))

 (let ((frame (clim:make-application-frame
			frame-type
			:parent *clim-root*
			:width 500 :height 300
			:pretty-name "Week Selection"
			)))
	(setf *THE-WEEK-FRAME* frame)

	(when run
	 #+:CCL (clim:run-frame-top-level frame)
         #+:Allegro (mp:process-run-function
		 "Target Files"	
                #'(lambda () (clim:run-frame-top-level frame)))
	 )
))

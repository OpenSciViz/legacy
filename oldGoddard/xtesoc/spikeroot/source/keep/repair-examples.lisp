;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; repair-examples.lisp - examples of CSPS, for repair.lisp
;;;
;;; split from repair.lisp 28Nov90 MDJ
;;;
;;; $Log: repair-examples.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.6  1991/12/01  00:26:13  johnston
;;; no changes
;;;
;;; Revision 1.5  1991/11/26  15:44:30  johnston
;;; reworked bin-packing examples to correspond to changes to repair-capcon
;;;
;;; Revision 1.4  1991/06/05  22:50:46  johnston
;;; fixed required capacity function to take both bin and value as args
;;;
;;; Revision 1.3  91/03/25  13:17:28  johnston
;;; minor changes
;;; 
;;; Revision 1.2  91/02/19  12:54:11  miller
;;; added bin packing as an example of using a capacity constraint csp
;;; 
;;; Revision 1.1  90/11/28  09:57:10  johnston
;;; Initial revision
;;; 
;;;
;;;

;;; -----------------------------------------------------------------------
;;; SPECIALIZED CLASSES:  DENSE 3COL
;;; -----------------------------------------------------------------------

(defclass DENSE-3COL (CSP)
  ())

(defmethod INIT-CSP ((c DENSE-3COL) args)
  "Type-specific CSP initialization.  Default does nothing.
    Called by make-csp. Args is list of keyword value arguments."
  (let ((N (getf args :size 3)))
    (format t "~%Initializing Dense 3-Col, size ~a" N)
    (with-slots (var-init-hook
                 vars-named-p) c
      (setf vars-named-p nil)
      (setf var-init-hook #'(lambda (v)
                              (list 3 (mod (number v) 3)))))
    (dotimes (i N)
      (make-var :csp c :number i))))


(defmethod CSP-VAR-VALUE-ASSIGN ((c dense-3col) (v var) value)
  "var v has its assignment made to value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  
  ;; constrain all vars with solution values different from current var
  (dolist (v1 (var-list c))
    (unless (or (eq v1 v)
                (= (solution-value v1) (solution-value v)))
      (add-to-conflicts-on-value v1 value)))
  )

(defmethod CSP-VAR-VALUE-UNASSIGN ((c dense-3col) (v var) value)
  "var v has its assignment unmade from value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  
  ;; constrain all vars with solution values different from current var
  (dolist (v1 (var-list c))
    (unless (or (eq v1 v)
                (= (solution-value v1) (solution-value v)))
      (subtract-from-conflicts-on-value v1 value)))
  
  )

(defmethod ASSIGNMENTS-WOULD-CONFLICT-P ((c dense-3col) (v1 var) value1 (v2 var))
  "return t if v2 has an assigned value that would conflict with assigning
    value1 to v1, nil otherwise"
  (and (assigned-value v2)
       (solution-value v1)
       (solution-value v2)
       (/= (solution-value v1) (solution-value v2)) ;vars have constraint
       (= value1 (assigned-value v2)) ;vals conflict
       ))


;;; -----------------------------------------------------------------------
;;; SPECIALIZED CLASSES:  SPARSE 3COL
;;; -----------------------------------------------------------------------

(defclass sparse-3COL (CSP)
  ())

;;; FROM YANN4:


(defvar *3col-graph*      nil
  "adjacency matrix:  1 means connected, 0 means not connected") ; adj matrix

(defvar *3col-colors*     nil
  "a consistent 3-coloring: (aref *3col-colors* i) => color of node i")

(defvar *3col-vertex-degree*  nil
  "array of vertex degrees, created & filled by 
    3COL-COMPUTE-VERTEX-DEGREE")

(defvar *3col-adjacent-nodes* nil
  "array of list of nodes connected to node; created & filled by 
    3COL-COMPUTE-ADJACENT-NODES")


(defun 3COL-COMPUTE-VERTEX-DEGREE ()
  (setf *3col-vertex-degree* (make-array (array-dimension *3col-colors* 0)))
  ;; vertex degree count
  (dotimes (i (array-dimension *3col-colors* 0))
    (let ((degree 0))
      (dotimes (j (array-dimension *3col-colors* 0))
        (if (= 1 (aref *3col-graph* i j))
          (incf degree)))
      (setf (aref *3col-vertex-degree* i) degree))))


(defun 3COL-COMPUTE-ADJACENT-NODES ()
  (setf *3col-adjacent-nodes* (make-array (array-dimension *3col-colors* 0)))
  ;; vertex degree count
  (dotimes (i (array-dimension *3col-colors* 0))
    (dotimes (j (array-dimension *3col-colors* 0))
      (if (= 1 (aref *3col-graph* i j))
          (push j (aref *3col-adjacent-nodes* i))))))
  
;;; size should be a multiple of 3
;;; default edges is such that vertex degree is 4
;;;(init-color-graph 30)
;;;(3col-describe-graph)

;; this makes a fully connected graph

(defun INIT-COLOR-GRAPH (size &optional (edges (* 2 size)))
  (setf *3col-graph* (make-array (list size size)
                                :element-type 'bit
                                :initial-element 0))
  (setf *3col-colors* (make-array size :initial-element 0))
  (dotimes (i size)
    (setf (aref *3col-colors* i) (mod i 3))
    ) ;color 0,1,2 in sequence
  ;; make matrix of edges
  (let ((actual-edges 0))
    (loop (let ((i (random size))
                (j (random size)))
            (unless (= i j)
              ;; if nodes i & j are different colors...
              (when (and (/= (aref *3col-colors* i)
                             (aref *3col-colors* j))
                         (/= (aref *3col-graph* i j) 1))
                ;; then put an edge between the nodes
                (setf (aref *3col-graph* i j) 1)
                (setf (aref *3col-graph* j i) 1)
                (incf actual-edges))))
          (if (= actual-edges edges)
            (return))))
  (3col-compute-vertex-degree)
  (3col-compute-adjacent-nodes))

(defun 3COL-PRINT-GRAPH ()
  (dotimes (i (array-dimension *3col-graph* 0))
    (format t "~%node: ~3d  color: ~d: " i (aref *3col-colors* i))
    (dotimes (j (array-dimension *3col-graph* 1))
      (format t "~d" (aref *3col-graph* i j)))))

(defun 3COL-DESCRIBE-GRAPH ()
  (let ((N (array-dimension *3col-colors* 0))
        (col0 0)
        (col1 0)
        (col2 0)
        (ave-degree 0))
    (dotimes (i N)
      (case (aref *3col-colors* i)
        (0 (incf col0))
        (1 (incf col1))
        (2 (incf col2))
        (t (error "unknown color"))))
    (format t "~%Nodes ~d, color breakdown: ~d ~d ~d"
            N col0 col1 col2)
    ;; vertex degree count
    (dotimes (i N)
      (let ((degree 0))
        (dotimes (j N)
          (if (= 1 (aref *3col-graph* i j))
            (incf degree)))
        (format t "~%~3d color ~d degree ~d"
                i (aref *3col-colors* i) degree)
        (incf ave-degree degree)))
    (format t "~%Average degree: ~f" (/ ave-degree N))))

(defun 3COL-CHECK-CONNECTED ()
  (let* (changed
         (N (array-dimension *3col-graph* 0))
         (label (make-array N :element-type 'number
                            :initial-element 0)))
    (setf (aref label 0) 1)
    (loop
      (setf changed 0)
      (dotimes (i N)
        (when (= 1 (aref label i))
          ;;(format t "~%~d =>" i)
          (dotimes (j N)
            (when (and (= 1 (aref *3col-graph* i j))
                       (= 0 (aref label j)))
              ;;(format t " ~d" j)
              (incf changed)
              (setf (aref label j) 1)))))
      ;;(format t " changed ~d~%" changed)
      (if (= 0 changed)
        (return)))
    ;; are all nodes labelled?
    (dotimes (i N)
      (if (= 0 (aref label i))
        (return-from 3col-check-connected nil)))
    t))


;; count the edges
(defun 3COL-EDGE-COUNT ()
  (let ((sum 0))
    (dotimes (i (array-dimension *3col-adjacent-nodes* 0))
      (incf sum (list-length (aref *3col-adjacent-nodes* i))))
    (/ sum 2)))

;; remove link at random unless the graph becomes disconnected
(defun 3COL-REMOVE-RANDOM-LINK ()
  (let* ((N (array-dimension *3col-graph* 0))
         (node1 (random N))
         (number-adjacent (list-length (aref *3col-adjacent-nodes* node1)))
         (node2 (elt (aref *3col-adjacent-nodes* node1)
                     (random number-adjacent)))
         (old-adjacent-nodes *3col-adjacent-nodes*))
    ;; check that adjacent
    (unless (and (= 1 (aref *3col-graph* node1 node2))
                 (= 1 (aref *3col-graph* node2 node1)))
      (error "nodes ~d and ~d not linked" node1 node2))
    ;; remove the link
    (setf (aref *3col-graph* node1 node2) 0
          (aref *3col-graph* node2 node1) 0)
    (3col-compute-adjacent-nodes)
    (cond ((3col-check-connected)
           (3col-compute-vertex-degree)
           t)
          (t ;; restore
           (setf (aref *3col-graph* node1 node2) 1
                 (aref *3col-graph* node2 node1) 1)
           (setf *3col-adjacent-nodes* old-adjacent-nodes)
           nil))))

;; this version uses the *3col-adjacent-nodes* array
(defun 3COL-CHECK-CONNECTED ()
  (let* (changed
         (N (array-dimension *3col-graph* 0))
         (label (make-array N :element-type 'number
                            :initial-element 0)))
    (setf (aref label 0) 1)
    (loop
      (setf changed 0)
      (dotimes (i N)
        (when (= 1 (aref label i))
          ;;(format t "~%~d =>" i)
          (dolist (node (aref *3col-adjacent-nodes* i))
            (when (= 0 (aref label node))
              ;;(format t " ~d" node)
              (incf changed)
              (setf (aref label node) 1)))
          ))
      ;;(format t " changed ~d~%" changed)
      (if (= 0 changed)
        (return)))
    ;; are all nodes labelled?
    (dotimes (i N)
      (if (= 0 (aref label i))
        (return-from 3col-check-connected nil)))
    t))
;;; end stuff from YANN4

(defun INIT-HARD-COLOR-GRAPH (size)
  (loop
    (init-color-graph size)
    (format t ".")
    (if (3col-check-connected)
      (return 'connected))))

;; (init-hard-color-graph 9)
;; (3col-describe-graph)

(defmethod INIT-CSP ((c sparse-3COL) args)
  "Type-specific CSP initialization.  Default does nothing.
    Called by make-csp. Args is list of keyword value arguments."
  (let ((N (getf args :size 3)))
    (format t "~%Initializing sparse 3-Col, size ~a" N)
    (init-hard-color-graph N)
    (with-slots (var-init-hook
                 vars-named-p) c
      (setf vars-named-p nil)
      (setf var-init-hook #'(lambda (v)
                              (list 3 (aref *3col-colors* (number v)))))
    (dotimes (i N)
      (make-var :csp c :number i)))))

(defmethod CSP-VAR-VALUE-ASSIGN ((c sparse-3col) (v var) value)
  "var v has its assignment made to value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  
  (dolist (node (aref *3col-adjacent-nodes* (var-number v)))
      (add-to-conflicts-on-value (var c node) value))
  )

(defmethod CSP-VAR-VALUE-UNASSIGN ((c sparse-3col) (v var) value)
  "var v has its assignment unmade from value: update state"
  ;; default method does nothing
  ;; method for actual problem types should update conflict counts on vars, 
  ;; update cache, etc.
  
  (dolist (node (aref *3col-adjacent-nodes* (var-number v)))
    (subtract-from-conflicts-on-value (var c node) value))
  )

(defmethod ASSIGNMENTS-WOULD-CONFLICT-P ((c sparse-3col) (v1 var) value1 (v2 var))
  "return t if v2 has an assigned value that would conflict with assigning
    value1 to v1, nil otherwise"
  (and 
   ;; v2 has an assigned value
   (assigned-value v2)
   ;; the nodes are connected
   (= 1 (aref *3col-graph* (var-number v1) (var-number v2)))
   ;; the colors are the same
   (= value1 (assigned-value v2)) ;values conflict
   ))

;; a special method for sparse 3-col:  a pathological initial 
;; state, distance d from solution, consisting of a connected
;; subgraph of size d with mutually consistent assignments

(defmethod PATHOLOGICAL-SPECIFIED-DISTANCE-FROM-SOLUTION ((c sparse-3col) d)
  "reset to specified distance d from solution"
  (let ((solution-set (reset-to-solution c)))
    (when solution-set
      
      (let ((var-list nil)
	    (related-var-list nil)
	    (value nil)
	    (N (var-count c)))
	
	;; get list of variables to change
	(cond ((= d N)
	       (dotimes (i N)
		 (push i var-list)))
	      (t (loop
		   (cond ((null var-list)
			  ;; pick random first var
			  (setf var-list (list (random N))))
			 (t ;; otherwise...
			  ;; get vars related to those in list
			  (setf related-var-list nil)
			  (dolist (var var-list)
			    (dolist (linked-var (aref *3col-adjacent-nodes* var))
			      ;; note push, not pushnew:  increase likelihood
			      ;; of selecting var related to many already picked
			      (push linked-var related-var-list)))
			  ;; select at random from related var list
			  (loop
			    (let ((cand (elt related-var-list 
					     (random (length related-var-list)))))
			      (when (not (member cand var-list))
				(push cand var-list)
				(return nil))))))
		   ;; done when d selected
		   (if (= (length var-list) d) (return nil)))))
	
	;; set to shifted erroneous values
	(dolist (var var-list)
	  (setf value (mod (+ (aref *3col-colors* var) 1) 3))
	  (assign-value (var c var) value))

	))))

;;; -----------------------------------------------------------------------
;;; SPECIALIZED CLASSES:  N-QUEENS
;;; -----------------------------------------------------------------------

(defclass QUEENS (CSP)
  ())

(defmethod INIT-CSP ((c QUEENS) args)
  "Type-specific CSP initialization.  Default does nothing.
    Called by make-csp. Args is list of keyword value arguments."
  (let ((N (getf args :size 3)))
    (format t "~%Initializing N-Queens, size ~a" N)
    (with-slots (var-init-hook
                 vars-named-p) c
      (setf vars-named-p nil)
      ;; solution not known
      (setf var-init-hook #'(lambda (v)
			      (declare (ignore v))
                              (list N nil))))
    (dotimes (i N)
      (make-var :csp c :number i))))


(defmethod CSP-VAR-VALUE-ASSIGN ((c queens) (v var) value)
  "var v has its assignment made to value: update state"
  (declare (optimize (speed 3) (safety 1))
	   (type fixnum value))
  (let ((v-number (the fixnum (var-number v)))
	(n (the fixnum (var-count c))))
    (declare (type fixnum v-number n))
    (dolist (v1 (var-list c))
      (unless (eq v1 v)
	;; constrain same column
	(add-to-conflicts-on-value v1 value)
	;; constrain the diagonals
	(dolist (value1 (queens-conflict-list v-number value (var-number v1) n))
	  (add-to-conflicts-on-value v1 value1))))))


(defmethod CSP-VAR-VALUE-UNASSIGN ((c queens) (v var) value)
  "var v has its assignment unmade from value: update state"
  (declare (optimize (speed 3) (safety 1))
	   (type fixnum value))
  (let ((v-number (the fixnum (var-number v)))
	(N (the fixnum (var-count c))))
    (declare (type fixnum v-number N))
    (dolist (v1 (var-list c))
      (unless (eq v1 v)
	;; constrain same column
	(subtract-from-conflicts-on-value v1 value)
	;; constrain the diagonals
	(dolist (value1 (queens-conflict-list v-number value (var-number v1) N))
	  (subtract-from-conflicts-on-value v1 value1))))))


(defun QUEENS-CONFLICT-LIST (var1 value1 var2 N)
  "return list of diagonals from var1 value1 on var2"
  (declare (optimize (speed 3)(safety 1))
	   (type fixnum var1 value1 var2 N))
  (let ((row-diff (- var1 var2))
	(d1 0)
	(d2 0)
	(result nil))
    (declare (type fixnum row-diff d1 d2))
    (setf d1 (- value1 row-diff))
    (setf d2 (+ value1 row-diff))
    (if (and (>= d1 0) (< d1 N)) (push d1 result))
    (if (and (>= d2 0) (< d2 N)) (push d2 result))
    result))
    

(defmethod ASSIGNMENTS-WOULD-CONFLICT-P ((c queens) (v1 var) value1 (v2 var))
  "return t if v2 has an assigned value that would conflict with assigning
    value1 to v1, nil otherwise"
  (declare (optimize (speed 3) (safety 1))
	   (type fixnum value1))
  (cond 
   
   ((null (assigned-value v2)) nil)	;no assignment on v2
   
   (t ;;check
    (let ((value2 (assigned-value v2)))
      (declare (type fixnum value2))
      (or (= value1 value2)
	  (member value1 (queens-conflict-list 
			(var-number v2) value2 
			(var-number v1) (var-count c))
		  :test #'=))))))

(defmethod QUEENS-INITIAL-GUESS ((c queens))
  ;;(initial-guess-rcmc c)
  (initial-guess-mc c)
  )

(defmethod SOLVE-QUEENS ((c queens))
  (loop 
    (if (in-solution-state-p c) (return))
    ;; reset
    (format t "~%reset&guess...")
    (queens-initial-guess c)
    (format t "(~d conf)" (conflicting-var-count c))
    ;; repair
    (format t " repair...")
    (repair-hill-climb c (* 2 (var-count c)) :RC)
    (format t "(~d conf)" (conflicting-var-count c))
    ;;(show-csp-state c)
    )
  ;; we have a solution: record it
  (dolist (var (var-list c))
    (format t "~%var: ~a, assigned ~a, soln: ~a" var 
            (assigned-value var) (solution-value var))
    (setf (solution-value var) (assigned-value var)))  
  )
	
(defmethod GET-QUEENS-INITIAL-GUESS ((c queens))
  (let ((saved-guess nil))
    (dolist (var (var-list c))
      (push (cons (var-number var) (assigned-value var)) saved-guess))
    saved-guess))

(defmethod PUT-QUEENS-INITIAL-GUESS ((c queens) saved-guess)
  (dolist (var-val saved-guess)
    (assign-value (var c (first var-val)) (rest var-val))))
  

;; remember one initial guess
(let ((saved-guess nil))

  (defmethod SAVE-QUEENS-INITIAL-GUESS ((c queens))
    (setf saved-guess nil)
    (dolist (var (var-list c))
      (push (cons (var-number var) (assigned-value var)) saved-guess)))

  (defmethod RESTORE-QUEENS-INITIAL-GUESS ((c queens))
    (dolist (var-val saved-guess)
      (assign-value (var c (first var-val)) (rest var-val))))

  (defun show-saved-queens-initial-guess ()
    (format t "~%~a" saved-guess)))

#|
;;Test:
(a-random-unassigned-var *q*)
(a-random-var-with-max-conflicts *q*)
(setf *q* (make-csp :csp-type 'queens :size 32))
(setf *q* (make-csp :csp-type 'queens :size 128))
(setf *q* (make-csp :csp-type 'queens :size 256))
(setf *q* (make-csp :csp-type 'queens :size 512))

(show-csp-state *Q*)
(reset-and-check *Q*)
(time (reset-to-solution *q*))
(assign-value (var *Q* 0) 0)
(assign-value (var *q* 1) 2)
(unassign-value (var *Q* 0))
(assign-value (var *Q* 8) 3)
(unassign-value (var *Q* 8))
(time (queens-initial-guess *q*))
 ;;for mc:  156.1 on Mac for 256
 ;; 11.6s on sparc, improved to 8.6s
 ;;for rcmc:  45s on Mac for 128, 180.1 for 256
(time (reset-csp *Q*))
 ;;.75s on Mac for 128, 2.9s for 256
 ;; .31s on sparc for 256
(solve-queens *q*)

(loop
      (if (all-vars-assigned-p *q*) (return nil))
      (let ((var-to-assign (a-random-unassigned-var *q*)))
	(assign-value var-to-assign (a-random-value-with-min-conflicts var-to-assign))))
(repair-backtrack *q* 500 :rc)
(dolist (var (var-list *Q*)) (unassign-value var))
  (format t "~%var: ~a, assigned ~a, soln: ~a" var (assigned-value var) (solution-value var)))


(queens-initial-guess *q*)
(time (repair-hill-climb *q* (* 2 (var-count *q*)) :rc))
(repair-hill-climb *q* (* 2 (var-count *q*)) :mc)
(repair-backtrack *q* (* 2 (var-count *q*)) :rc)
(repair-backtrack *q* (* 2 (var-count *q*)) :mc)

(defun knight-guess ()
  (reset-csp *q*)
  (dotimes (i (var-count *q*))
    (assign-value (var *q* i)
		  (mod (+ (* 2 i) 1) (var-count *q*)))))

(defun foo ()
  (reset-csp *q*)
  (knight-guess);(queens-initial-guess *q*)
  (reset-history *q*)
  (print (repair-hill-climb *q* (* 2 (var-count *q*)) :mc))
  (dolist (a (assignment-history *q*))
    (print a)))
  

(doq 10 16)

(defun doq (trials N)
  (setf *q* (make-csp :csp-type 'queens :size N))
  (let (
	(hcrc-results nil)
	(hcmc-results nil)
	(mshcrc-results nil)
	(mshcmc-results nil)
	(btrc-results nil)
	(btmc-results nil)

	(initial-conflicts nil)
	)

    ;; column headings
    (format t "~%N")
    (format t "~aHC-RC P(conv)" #\tab)
    (format t "~aHC-RC #steps" #\tab)
    (format t "~aHC-RC steps dev" #\tab)
    (format t "~aHC-MC P(conv)" #\tab)
    (format t "~aHC-MC #steps" #\tab)
    (format t "~aHC-MC steps dev" #\tab)
    (format t "~aMSHC-RC P(conv)" #\tab)
    (format t "~aMSHC-RC #steps" #\tab)
    (format t "~aMSHC-RC steps dev" #\tab)
    (format t "~aMSHC-MC P(conv)" #\tab)
    (format t "~aMSHC-MC #steps" #\tab)
    (format t "~aMSHC-MC steps dev" #\tab)
    (format t "~aBT-RC P(conv)" #\tab)
    (format t "~aBT-RC #steps" #\tab)
    (format t "~aBT-RC steps dev" #\tab)
    (format t "~aBT-MC P(conv)" #\tab)
    (format t "~aBT-MC #steps" #\tab)
    (format t "~aBT-MC steps dev" #\tab)
    ;;  (format t "~aBT-UIF P(conv)" #\tab)
    ;;  (format t "~aBT-UIF #steps" #\tab)
    ;;  (format t "~aBT-UIF steps dev" #\tab)

    (format t "~%")
    (dotimes (i trials)
      (format t ".")
      (if (= 0 (mod i 10)) (format t "[~d]" i))
      (force-output *terminal-io*)
      
      (push (queens-initial-guess *q*) initial-conflicts)
      (save-queens-initial-guess *q*)
      
      (push (multiple-value-list
		(repair-hill-climb *q* (* 2 (var-count *q*)) :rc)) hcrc-results)
      
      (restore-queens-initial-guess *q*)
      (push (multiple-value-list
		(repair-hill-climb *q* (* 2 (var-count *q*)) :mc)) hcmc-results)
      
      (restore-queens-initial-guess *q*)
      (push (multiple-value-list
		(multistep-repair-hill-climb *q* (* 2 (var-count *q*)) :rc)) mshcrc-results)
      
      (restore-queens-initial-guess *q*)
      (push (multiple-value-list
		(multistep-repair-hill-climb *q* (* 2 (var-count *q*)) :mc)) mshcmc-results)
      
      (restore-queens-initial-guess *q*)
      (push (multiple-value-list
		(repair-backtrack *q* (* 2 (var-count *q*)) :rc)) btrc-results)
      
      (restore-queens-initial-guess *q*)
      (push (multiple-value-list
		(repair-backtrack *q* (* 2 (var-count *q*)) :mc)) btmc-results)
      )
    
    (format t "~%~d" (var-count *q*))
    (print-results hcrc-results)
    (print-results hcmc-results)
    (print-results mshcrc-results)
    (print-results mshcmc-results)
    (print-results btrc-results)
    (print-results btmc-results)
    
    (format t "~%HCRC:~%~a~%~a" initial-conflicts hcrc-results)
    (format t "~%HCMC:~%~a~%~a" initial-conflicts hcmc-results)
    (format t "~%MSHCRC:~%~a~%~a" initial-conflicts mshcrc-results)
    (format t "~%MSHCMC:~%~a~%~a" initial-conflicts mshcmc-results)
    (format t "~%BTRC:~%~a~%~a" initial-conflicts btrc-results)
    (format t "~%BTMC:~%~a~%~a" initial-conflicts btmc-results)
    
    ))

(defun PRINT-RESULTS (results)
  (let ((num-conv 0)
        (steps 0)
        (steps2 0)
        (trials (length results)))
    (dolist (result results)
      (when (first result) 
        (incf num-conv)
        (incf steps (second result))
        (incf steps2 (* (second result) (second result)))))
    ;(format t "  prob of conv: ~5,3f" (/ num-conv trials))
    (format t "~a~5,3f" #\tab (/ num-conv trials))
    (cond ((= 0 num-conv)
	   (format t "~a ~a " #\tab #\tab))
	  (t
	   (setf steps (/ steps num-conv))
	   (setf steps2 (/ steps2 num-conv))
	   ;(format t "  steps:  ~6,1f +/- ~6,1f" steps (sqrt (- steps2 (* steps steps))))
	   (format t "~a~6,1f~a~6,1f" #\tab steps #\tab (sqrt (- steps2 (* steps steps))))
	   ))))

  
|#


;;; -----------------------------------------------------------------------
;;;  BIN PACKING - AN EXAMPLE USING CAPACITY CONSTRAINTS
;;; -----------------------------------------------------------------------

(defclass BINPACK-VAR (VAR) 
  ())
(defclass BINPACK (CAP-CSP)
  ())


(defmethod INIT-CSP ((c BINPACK) args)
  "establishes number of variables by :size arg"

  (let ((N         (getf args :size)))

    (unless (and (numberp N) (> N 0))
      (error "size not positive number ~a" N))

    (format t "~%Initializing BINPACK, size ~a" N)

    ;; make vars
    (dotimes (i N)
      (make-var :csp c :number i :var-type 'binpack-var
                :value-count *bin-count*))))

#| some examples. use *bin-count* and *bin-limit* to easily parametrize
   these example calls. 

 (defvar *bin-count*)
 (defvar *bin-limit*)
 (defvar *size*)

  Simple BINPACK. everyone uses 1, limit of 2/bin, 5 bins
  so all 10 fit

 (setf *bin-count* 5)
 (setf *bin-limit* 2)
 (setf *size* 10)
 (setf *b* (make-csp :name 'simple-BINPACK 
                     :csp-type 'BINPACK
                     :size *size*))
(show-csp-state *B*)

 ; note since bins map 1-to-1 to var domain, *bin-count* is used
 ; to initialize variable's conflicts array


 ; make the constraint and attach to the csp 

(defmethod REQUIRED-CAPACITY ((csp BINPACK) (capcon capacity-constraint)
                              (var binpack-var) bin value)
  (declare (ignore bin value))
  1)

(make-capacity-constraint
 *b* :name "simple-BINPACK-capacity-constraint"
 :limit-array (make-array *bin-count* :initial-element *bin-limit*)
 )

 (reset-csp *b*)
 (show-csp-state *b*)

;;assign one var and look at state
(assign-value (var *b* 0) 0)
(show-csp-state *b*)

;; assign another
(assign-value (var *b* 1) 0)
(show-csp-state *b*)

;; remove both, back to original?
(unassign-value (var *b* 1))
(unassign-value (var *b* 0))
(show-csp-state *b*)

;; go for an initial solution
(initial-guess-rcmc *b*)
(show-csp-state *b*)

;;;
;; now an impossible one as you are one bin short
;;;

(setf *bin-count* 4)
(setf *b* (make-csp :name 'one-short-BINPACK :csp-type 'BINPACK
             :size *size* :bin-count *bin-count*))

(make-capacity-constraint
      *b* :name "simple-BINPACK-capacity-constraint"
      :limit-array (make-array *bin-count* :initial-element *bin-limit*)
      )

 (reset-csp *b*)
 (initial-guess-rcmc *b*)
 (show-csp-state *b*)
(do-for-all-vars :csp *b* :var v
                 :form (unassign-value v))


; best solution has 6 conflicts, since *all* vars in an overloaded
; bin are given conflicts. 2 bins each have 2 that fit and 1 overload,
; so 6 = 2 * 3

|#

#|
 More complex bin pack. Each var can consume a different amount.
 Still 5 bins but can use up to 6 resource units per bin.
 the 10 items consume 1-5 units (see below). there is a conflict-free solution

 (setf *bin-count* 5)
 (setf *bin-limit* 6)
 (setf *b* (make-csp :name 'BINPACK-with-variable-consumption
                     :csp-type 'BINPACK
                     :size *size* :bin-count *bin-count*))

(defclass BINPACK-VAR-CAP-CON (capacity-constraint)
  ())

(defvar *BINPACK-USAGE*)
(setf *BINPACK-usage* (make-array *size* 
   :initial-contents '(1 2 3 4 5 5 4 3 2 1)))
;; usage must be defined before constraint is created since initialization
;; removes domain values which exceed capacity

(defmethod REQUIRED-CAPACITY ((csp BINPACK) (capcon BINPACK-VAR-CAP-CON)
                              (var binpack-var) bin value)
  (declare (ignore csp capcon bin value))
  (aref  *BINPACK-USAGE* (var-number var)))


 (make-capacity-constraint
      *b* 
      :name "BINPACK-with-variable-consumption-capacity-constraint"
      :capcon-type 'BINPACK-VAR-CAP-CON
      :limit-array (make-array *bin-count* :initial-element *bin-limit*)
      )


(reset-csp *b*)
(show-csp-state *b*)
(assign-value (var *b* 0) 0)
(assign-value (var *b* 1) 0)
(assign-value (var *b* 4) 1)
(show-csp-state *b*)

(unassign-value (var *b* 1))
(unassign-value (var *b* 0))
(unassign-value (var *b* 4))

(assign-value (var *b* 0) 0)
(assign-value (var *b* 1) 1)
(assign-value (var *b* 2) 2)
(in-solution-state-p *b*)

(reset-csp *b*)
(initial-guess-rcmc *b*)
(show-csp-state *b*)
(repair-hill-climb *b* 200 :mc)
(repair-hill-climb *b* 200 :mcmc)
(show-csp-state *b*)
(csp-state-summary *b*)

;; elminc init guess
(make-assignments 
 *b*
 :to-vars                    :all-unassigned
 :strategy  '(((first-value-lexical-order-with-min-conflicts :min)
               (values-with-min-conflicts-count :min)
               :random)
              ;; value
              ((conflicts-on-value :min)
               (value-preference :max)
               :first))
 )

;; max conf repair
(make-assignments 
 *b*
 :to-vars                    :all-with-conflicts
 :how-many                   (* 2 (var-count *b*))
 :strategy '(((conflicts-on-assigned-value :max)
              :random)
             ((conflicts-on-value :min)
              (value-preference :max)
              :random))
 :verbose nil)

|#


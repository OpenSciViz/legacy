;;; -*- Mode: LISP; Syntax: Common-lisp; Package: USER; Base: 10; -*-
;;;
;;; astro-objects.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: astro-objects.lisp,v $
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.5  1992/03/12  18:25:31  johnston
;;; made slots on low-earth-orbit-satellite into double-floats, and
;;; put check in asc-node-list that no infinite loop occurs
;;;
;;; Revision 1.4  1991/12/12  18:05:46  giuliano
;;; Changed the position-vector method for moving targets.
;;; The new method allows a two day slop on either end of
;;; the ephemeris data.
;;;
;;; Revision 1.3  1991/12/01  00:11:52  johnston
;;; minor changes
;;;
;;; Revision 1.2  1991/11/26  15:32:44  johnston
;;; added terminator crossing calculations, observatories,
;;; ground stations; some efficiency improvements in occultation
;;; calculations
;;;
;;; Revision 1.1  1991/09/04  18:53:54  sponsler
;;; Initial revision
;;;
;;; Revision 1.11  91/08/21  11:21:00  giuliano
;;; Added definition and associated methods for moving-astronomical-target.
;;; 
;;; Revision 1.10  91/06/05  22:44:24  johnston
;;; trivial fixes
;;; 
;;; Revision 1.9  91/04/15  13:03:47  johnston
;;; added newline at end of file
;;; 
;;; Revision 1.8  91/03/25  13:21:15  johnston
;;; star tracker changes
;;; 
;;; Revision 1.7  91/03/18  22:19:07  johnston
;;; added sensor and named-vectors to satellite classes.  Added orientation
;;; methods to satellites.
;;;
;;; creation:  Feb 1987, MDJ
;;; modified 31-Jan-1988 MDJ
;;;  added routines to convert to/from ecliptic coordinates, and
;;;  to compute HST roll dates and angles
;;;
;;; ======================================================================
;;;                                                                GLOBALS
;;; approx value for 1980


(defvar *DEFAULT-ECLIPTIC-OBLIQUITY* (radians 23.442) 
  "obliquity of the ecliptic, radians")

;;; NOTE:  specific objects *SUN*, *MOON*, *ST*, *TDRSE*, *TDRSW*
;;; are defined at the END of this file

(defvar *SUN*) 
(defvar *MOON*) 
(defvar *ST*) 
(defvar *TDRSE*) 
(defvar *TDRSW*) 

;;; Note:  JDs are relative to *JD-Reference*.  See astro-utilities.lisp.


(defparameter *DRM-START* 2135.520145833
  "TJD, equiv. 11-Mar-1983 00:29:00.6 UT")

;;; ======================================================================
;;;                                                        SATELLITE TABLE

(defvar *SATELLITE-TABLE* (make-hash-table :test #'equal)
  "Table with key = satellite name, value = satellite object instance")

;;; --- accessors to global satellite table

;;; NOTE:  ALL instances of SATELLITE objects should be recorded in this
;;; table, in order for references to them to be updated after a new orbit
;;; model is loaded!!!  See fast-event.lisp for usage.


(defun UPDATE-SATELLITE-TABLE (name instance)
  "store correspondence between satellite name and instance in global table"
  (setf (gethash (string-it-upcase name) *satellite-table*) instance))


(defun GET-SATELLITE-BY-NAME (name)
  "return satellite instance corresponding to name (case insensitive)"
  ;; (get-satellite-by-name "HST")
  ;; (get-satellite-by-name "hst")
  (gethash (string-it-upcase name) *satellite-table*))


(defun SHOW-SATELLITE-TABLE ()
  ;; (show-satellite-table)
  (maphash #'(lambda (key value)
               (format t "~%~15a ~a" key value))
           *satellite-table*))


;;; ======================================================================
;;;                                            CLASS:  ASTRONOMICAL-OBJECT

(defclass ASTRONOMICAL-OBJECT ()
  ((name
    :initform  "???"
    :initarg  :name
    :accessor  name
    :documentation "Object name")
   ))

;;; --- general methods

(defmethod POSITION-VECTOR ((AO astronomical-object) jd)
  
  "Unit 3-vector from earth center to astronomical object"
  
  (declare (ignore jd))
  (format t "~%*** don't know how to get position-vector for ~a" AO)
  nil)


(defmethod DISTANCE ((AO astronomical-object) jd &optional (print-steps nil))
  
  "Distance in km of astronomical-object from earth center"

  (declare (ignore jd print-steps))
  (format t "~%*** don't know how to get distance for ~a" AO)
  nil) 


(defmethod VELOCITY-VECTOR ((AO astronomical-object) jd)

  "Unit geocentric velocity vector of astronomical object"

  (declare (ignore jd))
  (format t "~%*** don't know how to get velocity-vector for ~a" AO)
  nil) 


(defmethod VELOCITY ((AO astronomical-object) jd)
  
  "Speed of object in km/sec (magnitude of velocity vector)"
  
  (declare (ignore jd))
  (format t "~%*** don't know how to get speed for ~a" AO)
  nil)


;;; Methods valid for objects which respond to position-vector and 
;;; distance messages.  position-vector returns unit vector, distance 
;;; returns km (both geocentric); both messages require date (JD) as argument

(defmethod LINE-OF-SIGHT ((viewer astronomical-object) 
                          (target astronomical-object) jd)

  "Compute line-of-sight (LOS) unit vector from viewer to target
    Target may be at infinity (distance NIL)
    See astrolibe LOSVEC"

  (let ((target-distance (distance target jd)))
    (if target-distance
      
      ;; target at finite distance: compute LOS vector
      (3vect-normalize
       (3vect-subtract (3vect-times-scalar target-distance 
                        (position-vector target jd))
        (3vect-times-scalar (distance viewer jd)
         (position-vector viewer jd))))
      
      ;; else target at infinity: return position vector
      (position-vector target jd))))


(defmethod ANGLE-TO-HORIZON ((viewer astronomical-object) 
                             (target astronomical-object)
                             jd &optional (limb-angle 0))

  "Returns angle above horizon in radians that target appears to viewer 
    at input time jd.  Positive angles means target is above horizon.
    Refers to constant *earth-radius*
    limb-angle is angle above geometrical horizon where target is occulted
    (if not specified defaults to zero)
    target may be at infinity (distance NIL)
    (see astrolibe HORIZANG)"

  (let* ((los (line-of-sight viewer target jd))

	 ;; location of viewer (self)
	 (viewer-position (position-vector viewer jd))

	 ;; zenith angle of LOS vector
	 (phizenith (acos (3vect-dot-product viewer-position los)))

	 ;; angle from origin to earth limb
	 (phisurface (asin (/ *earth-radius* (distance viewer jd)))))

    ;; result: angle from horizon
    (- (- pi phizenith) (+ phisurface limb-angle))))


(defmethod COS-ANGLE-FROM-ZENITH ((viewer astronomical-object) 
                                  (target astronomical-object)
                                  jd)

  "Returns cos of angle from zenith that target appears to viewer 
    at input time jd. "

  (3vect-dot-product 
   (position-vector viewer jd) 
   (line-of-sight viewer target jd)))


(defmethod ANGLE-FROM-ZENITH ((viewer astronomical-object) 
                              (target astronomical-object)
                              jd
                              &key (units :radians))

  "Returns angle from zenith that target appears to viewer 
    at input time jd. :units may be :degrees or :radians."

  (3vect-separation 
   (position-vector viewer jd) 
   (line-of-sight viewer target jd)
   :units units))


(defmethod EARTH-POSITION-BELOW ((AO astronomical-object) jd)
  
  "return 3vector in earth coord frame (z north pole, x Greenwich longitude
    at equator, y makes RHS) of point on earth's surface below object"

  (let* ((earth-orient (earth-orientation jd))
         (pos (position-vector AO jd))
         (earth-relative-vector 
          (3x3matrix-transpose-times-vector earth-orient pos)))
    earth-relative-vector))


(defmethod EARTH-LONGITUDE-LATITUDE-BELOW ((AO astronomical-object) jd
                                           &key (units :radians))
  "Return list (longitude latitude) of point on earth's surface below 
    the input astronomical object at time jd.  JD is TJD.
    :units specify output, can be :radians (default) or :degrees.
    Note that longitude will be in the range 0-2pi or 0-360."
  
  (let ((epb (earth-position-below AO jd)))
    (position-ra-dec epb :units units)))


;;; ======================================================================
;;;                                            GEOMETRIC UTILITY FUNCTIONS
;;;
;;; Functions in this section are used in various routines below for
;;; geometry calculations.  They are specific to astro-objects, so are
;;; not part of astro-utilities.
;;;


(defun LOS-TO-HORIZON-ANGLE (sat sat-dist los earth-radius
                                 &key (limb-angle 0))

  "Returns angle above horizon in radians that target appears to viewer.  
    Positive angles means target is above horizon.
    limb-angle is angle above geometrical horizon where target is occulted
    (if not specified defaults to zero)
    (see astrolibe HORIZANG)
    Arguments:
     sat - geocentric unit vector location of satellite
     sat-distance - distance of satellite from earth center, km
     los - geocentric unit vector line-of-sight of satellite to target
     earth-radius - km
     :limb-angle - see above, radians"

  (let* (;; zenith angle of LOS vector
	 (phizenith (acos (3vect-dot-product sat los)))

	 ;; angle from origin to earth limb
	 (phisurface (asin (/ earth-radius sat-dist))))

    ;; result: angle from horizon
    (- (- pi phizenith) (+ phisurface limb-angle))))


(defun LOS-NEAREST-LIMB-VECTOR (sat sat-dist los earth-radius)
  
  "Compute geocentric unit vector to point on earth limb nearest to
    line-of-sight.
    Arguments:
     sat - geocentric unit vector location of satellite
     sat-distance - distance of satellite from earth center, km
     los - geocentric unit vector line-of-sight of satellite
     earth-radius - km"

  (let* ((horizon-angle (los-to-horizon-angle sat sat-dist los earth-radius
                                              :limb-angle 0))
         (distance-along-los
          (/ (sqrt (- (* sat-dist sat-dist) (* earth-radius earth-radius)))
             (cos horizon-angle)))
	 (normalized-limb-vector
	  (3vect-normalize
	   (3vect-linear-comb sat-dist sat distance-along-los los)))
         )
    normalized-limb-vector))


(defun LOS-INTERSECTS-EARTH-P (sat los h)
  
  "Return non-nil if line-of-sight vector intersects the earth
    Arguments:
     sat - geocentric unit vector to satellite
     los - geocentric unit vector in direction of LOS
     h - earth half angle as seen from satellite"
  ;; (los-intersects-earth-p '(0 0 1) '(0 0 1) (radians 66)) => nil
  ;; (los-intersects-earth-p '(0 0 1) '(0 0 -1) (radians 66)) => t

  (< (3vect-dot-product sat los) (cos (- pi h))))


(defun LOS-EARTH-INTERSECTION-VECTOR (sat sat-dist los earth-radius)
  
  "Calculate geocentric unit vector for intersection
    of LOS with earth surface.  Of the two possible intersections, only the
    nearer one is returned, or nil if there is no intersection at all.
    Arguments:
     sat - geocentric unit vector, satellite location
     sat-dist - distance of satellite from earth center, km
     los - line-of-sight vector from satellite to target
     earth-radius - km
    Return nil if no such vector."

  (let ((h (asin (/ earth-radius sat-dist)))
        (cos-sat-los (3vect-dot-product sat los)))
    (cond 
     ((> cos-sat-los (cos (- pi h)))
      ;; no intersection, return nil
      nil)
     ((= cos-sat-los -1)
      ;; looking straight down
      sat)
     (t 
      (let* (
             ;; basis vectors: here y is along satellite, x is in sat los plane,
             ;; and z is perp and ignored
             (y sat)
             (z (3vect-normalize (3vect-cross-product sat los)))
             (x (3vect-normalize (3vect-cross-product z y)))
             ;; components of los (only x, y needed)
             (los-x (3vect-dot-product los x))
             (los-y (3vect-dot-product los y))
             ;; slope and intercept of line
             (m (/ los-y los-x))
             (b sat-dist)
             ;; earth rad squared
             (earth-radius2 (* earth-radius earth-radius))
             ;; coeff for quadratic
             (aa (+ 1 (* m m)))
             (bb (* 2 m b))
             (cc (- (* b b) earth-radius2))
             )
        ;;(format t "~%m ~a b ~a" m b)
        ;; solve quadratic
        (multiple-value-bind
          (x1 x2 type) (solve-quadratic aa bb cc)
          ;;(format t "~%x1 x2 type ~a ~a ~a" x1 x2 type)
          (cond ((or (eq type :real) (eq type :degenerate))
                 ;; real roots, construct the two intersection vectors
                 (let* ((vec1 (3vect-normalize
                               (3vect-linear-comb
                                x1 x (sqrt (- earth-radius2 (* x1 x1))) y)))
                        (vec2 (3vect-normalize
                               (3vect-linear-comb
                                x2 x (sqrt (- earth-radius2 (* x2 x2))) y)))
                        ;; take the one with the smallest angle to sat
                        (dot1 (3vect-dot-product vec1 sat))
                        (dot2 (3vect-dot-product vec2 sat)))
                   (if (> dot1 dot2) vec1 vec2)))
                (t nil))))))))

#| test case:

(let* ((sat '(0 0 1))
       (sat-dist (+ 600 *earth-radius*))
       (h (asin (/ *earth-radius* sat-dist)))
       dec los)
  (dotimes (i 30)
    (setf dec (+ -90 (* 3 i)))
    (setf los (3vect-spherical-to-cartesian (list 0 (radians dec))))
    (format t "~%dec: ~9,4d intersect ~a: ~a  horizon ang ~9,4fd"
            dec 
            (if (los-intersects-earth-p sat los h) "YES" " NO")
            (los-earth-intersection-vector sat sat-dist
                                           los *earth-radius*)
            (degrees (los-to-horizon-angle sat sat-dist los *earth-radius*)))))
|#

#| TEST CASE:
(los-tp-intersection-vector 
 '(0 0 1) (+ *earth-radius* 600)
 (3vect-normalize '(-1 -1 -1)) *earth-radius*)
 => (-0.09324972642823696 -0.09324972642823696 0.9912663502016589)
(los-tp-intersection-vector 
 '(0 0 1) (+ *earth-radius* 600)
 (3vect-normalize '(1 1 1)) *earth-radius*) => nil
|#

#| test case:

(LOS-GC-TERMINATOR-INTERSECTION 
 '(0 0 1) 
 (3vect-spherical-to-cartesian (list 0 (radians 30)))
 (3vect-normalize '(0 1 0))
 (radians 100)
 :verbose t)
(LOS-GC-TERMINATOR-INTERSECTION 
 '(0 0 1) 
 (3vect-spherical-to-cartesian (list (radians 30) (radians 30)))
 (3vect-normalize '(0 1 0))
 (radians 100)
 :verbose t)
(LOS-GC-TERMINATOR-INTERSECTION 
 '(0 0 1) 
 (3vect-spherical-to-cartesian (list 90 (radians 0)))
 (3vect-normalize '(0 1 0))
 (radians 100)
 :verbose t)
|#


(defun LOS-GC-TERMINATOR-INTERSECTION (sat sun los term-angle
                                           &key (verbose nil))
  
  "Compute and return intersection vectors (if any) on unit sphere
    of:  great circle through subsatellite point and LOS, and small
    circle at angle term-angle from sun.  Arguments:
     sat - geocentric unit vector of satellite
     sun - geocentric unit vector of sun
     los - geocentric unit vector line-of-sight from sat to target
     term-angle - bright earth is term-angle radians from sun vector
    If no intersections, nil is returned.
    If verbose keyword is t, then test info printed to standard output."

  (let* (
         ;; reference frame:  z along sat, los in x-z plane
         (z sat) ; z along satellite
         (y (3vect-normalize (3vect-cross-product sat los)))
         (x (3vect-normalize (3vect-cross-product y sat)))
         
         (cos-term-angle (cos term-angle))
         (cos-term-angle2 (* cos-term-angle cos-term-angle))
         
         ;; some sun components needed in ref frame
         (sunx (3vect-dot-product sun x))
         (sunz (3vect-dot-product sun z))
         (sunx2 (* sunx sunx))
         (sunz2 (* sunz sunz))
         
         ;; coeff for quadratic to solve a Kz^2 + b Kz + c = 0
         (a (+ sunx2 sunz2))
         (b (* -2 cos-term-angle sunz))
         (c (- cos-term-angle2 sunx2))
         
         ;; for computed components
         Kx-1 Kx-2 Kz-1 Kz-2 
         ;; returned vectors
         (K-1 nil)
         (K-2 nil)
         )

    (cond ((and (= 0 sunx) (/= 0 sunz))
           ;; sun is in y-z plane
           (setf Kz-1 (/ cos-term-angle sunz))
           (setf Kz-2 Kz-1)
           (setf Kx-1 (sqrt (- 1 (* Kz-1 Kz-1))))
           (setf Kx-2 (- Kx-1)))
          ((and (/= 0 sunx) (= 0 sunz))
           ;; sun is in x-y plane
           (setf Kx-1 (/ cos-term-angle sunx))
           (setf Kx-2 Kx-1)
           (setf Kz-1 (sqrt (- 1 (* Kx-1 Kx-1))))
           (setf Kz-2 (- Kz-1)))
          (t ;general case
           ;; solve the quadratic first
           (multiple-value-bind 
             (root1 root2 root-type) (solve-quadratic a b c)
             ;;(format t "~% ~a ~a ~a" kz-1 kz-2 root-type)
             (when (or (eq root-type :real) (eq root-type :degenerate))
               ;; have intersection
               (setf Kz-1 root1 Kz-2 root2)
               (setf Kx-1 (/ (- cos-term-angle (* Kz-1 sunz)) sunx))
               (setf Kx-2 (/ (- cos-term-angle (* Kz-2 sunz)) sunx))))))

    ;; calculate vectors if components found
    ;; solution vectors are in original ref frame
    (when (and Kx-1 Kx-2 Kz-1 Kz-2)
      (setf K-1 (3vect-normalize
                 (3vect-linear-comb Kx-1 x Kz-1 z)))
      (setf K-2 (3vect-normalize
                 (3vect-linear-comb Kx-2 x Kz-2 z))))
    
    ;; check solution
    (when (and verbose K-1 K-2)
      ;; K should be in sat-los plane, so check perp to y ~ sat x los 
      ;; K should also be term-angle from sun
      (format t "~%K 1: ~9,6f=90 ~9,6f=~9,6f"
              (3vect-separation K-1 y :units :degrees)
              (3vect-separation K-1 sun :units :degrees)
              (degrees term-angle))
      (format t "~%K 2: ~9,6f=90 ~9,6f=~9,6f"
              (3vect-separation K-2 y :units :degrees)
              (3vect-separation K-2 sun :units :degrees)
              (degrees term-angle)))
    
    ;; returned value
    (cond ((and K-1 K-2)
           (list K-1 K-2))
          ;; no intersection, return nil
          (t nil))))


(defun LOS-VECTOR-TO-EARTH-SURFACE (sat sat-dist surface-vect earth-radius)

  "Calculate line-of-sight-vector from satellite to point on earth's surface.
    Arguments:
     sat - geocentric unit vector in direction of satellite
     sat dist - sat distance from earth center, km
     surface-vect - geocentric unit vector to point on earth's surface
     earth-radius - km"

  (3vect-normalize 
   (3vect-linear-comb earth-radius surface-vect (- sat-dist) sat)))


(defun LOS-TP-INTERSECTION-VECTOR (sat sat-dist los earth-radius)
  
  "Calculate geocentric unit vector in direction of intersection
    of LOS to subsatellite tangent plane.
    Arguments:
     sat - geocentric unit vector, satellite location
     sat-dist - distance of satellite from earth center, km
     los - line-of-sight vector from satellite to target
     earth-radius - km
    Return nil if no such vector."
  
  (let* ((cos-sat-to-los-angle (3vect-dot-product sat los))
         (alpha (if (/= 0 cos-sat-to-los-angle)
                  (/ (- earth-radius sat-dist) cos-sat-to-los-angle))))
    (cond ((or (null alpha) (<= alpha 0))
           ;; no intersection
           nil)
          (t (3vect-normalize
              (3vect-linear-comb sat-dist sat alpha los))))))


(defun EARTH-TP-INTERSECTION-VECTOR (sat sat-dist surface-vect earth-radius)
  
  "Calculate geocentric unit vector in direction of intersection
    with subsatellite tangent plane
    of vector from satellite to point on earth surface.
    Arguments:
     sat - geocentric unit vector, satellite location
     sat-dist - distance of satellite from earth center, km
     surface-vect - geocentric unit vector to point on earth's surface
     earth-radius - km
    Return nil if no such vector."
  
  (let* ((los (los-vector-to-earth-surface 
               sat sat-dist surface-vect earth-radius))
         (result (los-tp-intersection-vector sat sat-dist los earth-radius)))
    result))
    

(defun ANGLE-TO-ILLUM-EARTH (sat sat-dist sun term-angle los earth-radius)

  "Compute and return angle to illuminated earth.
   Arguments:
    sat - geocentric unit vector, satellite location
    sat-dist - distance of satellite from earth center, km
    sun - geocentric unit vector, sun location
    term-angle - angle from sun to point on earth's surface which
      defines terminator, radians
    los - line-of-sight vector from satellite to target
    earth-radius - km
    Multiple values returned:
     angle to illuminated earth, or nil if earth not illuminated
     type keyword, one of:
      :not-illum            earth not illuminated
      :direct-full-illum    looking a fully illuminated earth
      :limb-full-illum      looking off the limb of fully illuminated earth
      :direct-partial-illum looking at bright earth when partially illum
      :direct-terminator    looking at dark earth when partially illum, angle
                            is to terminator
      :limb-partial-illum   looking off bright limb of partially illum earth,
                            angle is to limb
      :indirect-terminator  looking off dark limb of partially illum earth, 
                            angle is to terminator
    "
  (let* (
         ;; h = half angle of earth as seen from satellite
         (h (asin (/ earth-radius sat-dist)))
         (pi/2-minus-h (- pi/2 h))

         ;; angle from satellite to sun
         (sat-to-sun-angle (3vect-separation sat sun))
              
         )

    (cond 

     ((>= sat-to-sun-angle (+ term-angle pi/2-minus-h))
      ;; earth not illuminated
      (values nil :not-illum))

     ((<= sat-to-sun-angle (- term-angle pi/2-minus-h))
      ;; earth fully illuminated
      (cond ((los-intersects-earth-p sat los h)
             ;; looking at bright earth
             (values 0 :direct-full-illum))
            (t ;; los does not intersect earth: angle is to bright limb
             (values (los-to-horizon-angle sat sat-dist los earth-radius
                                           :limb-angle 0)
                     :limb-full-illum))))

     (t ;; earth partially illuminated
      (angle-to-partially-illum-earth 
       sat sat-dist sun term-angle los earth-radius
       h pi/2-minus-h)))))


;;; BEWARE: when looking at partially illuminated dark earth, angle
;;; to terminator can be very wrong (since only the three points:
;;; terminator/limb intersection, and GC between target and term
;;; are looked at.  This is ONLY valid for targets off the limb!

(defun ANGLE-TO-PARTIALLY-ILLUM-EARTH (sat sat-dist sun term-angle 
                                           los earth-radius
                                           h pi/2-minus-h)

  "Compute and return angle to partially illuminated earth.
    Auxiliary function for ANGLE-TO-ILLUM-EARTH.
   Arguments:
    sat - geocentric unit vector, satellite location
    sat-dist - distance of satellite from earth center, km
    sun - geocentric unit vector, sun location
    term-angle - angle from sun to point on earth's surface which
      defines terminator, radians
    los - line-of-sight vector from satellite to target
    earth-radius - km
    h - half-angle of earth as seen from sat, radians
    pi/2-minus-h - (- pi/2 h)
   For returned value description see ANGLE-TO-ILLUM-EARTH."

  (cond ((los-intersects-earth-p sat los h)
         ;; looking at earth: get intersection point and see if bright or dark
         (let* ((earth-intersection (los-earth-intersection-vector
                                     sat sat-dist los earth-radius))
                (earth-intersection-bright
                 (if earth-intersection
                   (< (3vect-separation sun earth-intersection) term-angle))))
           (cond (earth-intersection-bright
                  ;; looking at bright earth
                  (values 0 :direct-partial-illum))
                 (t ; looking at dark earth
                  (values (minimum-angle-to-terminator 
                           sat sat-dist sun term-angle 
                           los earth-radius pi/2-minus-h)
                          :direct-terminator)))))
         
         (t ; not looking at earth: get limb vector and see if bright or dark
          (let* (         
                 ;; nearest limb vector (geocentric)
                 (earth-limb-vector 
                  (los-nearest-limb-vector sat sat-dist los earth-radius))
                 (earth-limb-bright
                  (< (3vect-separation sun earth-limb-vector) term-angle)))
            (cond (earth-limb-bright
                   (values (los-to-horizon-angle sat sat-dist los earth-radius
                                           :limb-angle 0)
                           :limb-partial-illum))
                  (t ; looking off dark earth, get angle to terminator
                   (values (minimum-angle-to-terminator 
                            sat sat-dist sun term-angle 
                            los earth-radius pi/2-minus-h)
                           :indirect-terminator)))))))


(defun TERMINATOR-VECTORS (sat sun term-angle los pi/2-minus-h)

  "Compute and return quantities describing terminator.  Assumes that
    earth is partially illuminated (THIS IS NOT CHECKED, HOWEVER).
    Arguments:
     sat - geocentric unit vector, satellite location
     sun - geocentric unit vector, sun location
     term-angle - angle from sun to point on earth's surface which
       defines terminator, radians
     los - line-of-sight vector from satellite to target
     pi/2-minus-h - (- pi/2 h) where half-angle of earth as seen from 
       sat, radians
    Returns three values:
     term2, term3:  vectors on terminator and earth limb
     k: vector in sat-los plane which intersects terminator within horizon.
        may be nil if no such intersection exists."

  (let* (
         
         ;; basis vectors for analysis
         (z sat)
         (y (3vect-normalize (3vect-cross-product sat sun)))
         (x (3vect-normalize (3vect-cross-product y sat)))
         
         (cos-pi/2-minus-h (cos pi/2-minus-h))
         
         ;; angle of sun to x (radians)        
         (cos-sun-to-x-angle (3vect-dot-product x sun))
         (sin-sun-to-x-angle (3vect-dot-product z sun))
                  
         ;; term2, term3 are vectors on earth horizon and terminator
         (wz cos-pi/2-minus-h)
         (wx (/ (- (cos term-angle) (* cos-pi/2-minus-h sin-sun-to-x-angle))
                cos-sun-to-x-angle))
         (wy (sqrt (- 1 (+ (* wx wx) (* wz wz)))))
         (term2-vect (3vect-normalize 
                      (3vect-linear-comb 
                       1 (3vect-linear-comb wx x wy y) wz z)))
         (term3-vect (3vect-normalize 
                      (3vect-linear-comb 
                       1 (3vect-linear-comb wx x (- wy) y) wz z)))
                  
         ;; k1, k2 are geocentric vectors to intersection of sat-los great
         ;; circle and terminator: only one can be within horizon
         (k1-k2 (los-gc-terminator-intersection sat sun los term-angle))
         ;; note k1-k2 can be nil if no intersection
         (k1 (first k1-k2))
         (k2 (second k1-k2))
         ;; find which k is within horizon, if any
         (k (cond ((and k1 (> (3vect-dot-product k1 sat) cos-pi/2-minus-h)) k1)
                  ((and k2 (> (3vect-dot-product k2 sat) cos-pi/2-minus-h)) k2)
                  (t nil)))
         )
    (values term2-vect term3-vect k)))


(defun MINIMUM-ANGLE-TO-TERMINATOR (sat sat-dist sun term-angle 
                                        los earth-radius
                                        pi/2-minus-h)

  "Compute and return smallest angle from line-of-sight to any of the
     three terminator vectors term2, term3, k.
    Arguments:
     sat - geocentric unit vector, satellite location
     sat-dist - distance of satellite from earth center, km
     sun - geocentric unit vector, sun location
     term-angle - angle from sun to point on earth's surface which
       defines terminator, radians
     los - line-of-sight vector from satellite to target
     earth-radius - km
     pi/2-minus-h - (- pi/2 h) where h = half-angle of earth as seen 
       from sat, radians"

  (multiple-value-bind
    (term2 term3 k)
    (terminator-vectors sat sun term-angle los pi/2-minus-h)

    (let (

          ;; get line-of-sight vectors to points on terminator
          (term2-los (los-vector-to-earth-surface
                      sat sat-dist term2 earth-radius))
          (term3-los (los-vector-to-earth-surface
                      sat sat-dist term3 earth-radius))
          (k-los (if k (los-vector-to-earth-surface
                        sat sat-dist k earth-radius)))
          result)

      (setf result
            (min (3vect-separation los term2-los)
                 (3vect-separation los term3-los)))
      (if k (setf result (min result (3vect-separation los k-los))))

      ;; returned value
      result)))


;;; ======================================================================
;;;                                                       CLASS: SUN-CLASS 


(defclass SUN-CLASS (astronomical-object)
  ((reference-epoch
       :initform  238.5
       :initarg  :reference-epoch
       :accessor  reference-epoch)		; TJD, 1980.0
   (ecc
       :initform  0.016718
       :initarg  :ecc
       :accessor  ecc)
   (ecliptic-long-at-epoch
       :initform  (radians 278.83354)
       :initarg  :ecliptic-long-at-epoch
       :accessor  ecliptic-long-at-epoch)
   (ecliptic-long-of-perigee
       :initform  (radians 282.596403)
       :initarg  :ecliptic-long-of-perigee
       :accessor  ecliptic-long-of-perigee)
   (period
       :initform  365.2422
       :initarg  :period
       :accessor  period)			; days
   (mean-radius
       :initform  1.495985e8
       :initarg  :mean-radius
       :accessor  mean-radius)			; km
   ;; for state: remember last true anomaly calculation
   (time-of-true-anomaly
       :initform  nil
       :initarg  :time-of-true-anomaly
       :accessor  time-of-true-anomaly)
   (last-true-anomaly
       :initform  nil
       :initarg  :last-true-anomaly
       :accessor  last-true-anomaly)))


(defun KEPLER-EQUATION (m ecc &key (tolerance 1.0e-6))
  
  "solve kepler equation E - ecc * sin(E) = M where ecc is eccentricity"
  
  (do* ((e m (- e (/ delta (- 1 (* ecc (cos e))))))
        (delta (- e (* ecc (sin e)) m) (- e (* ecc (sin e)) m)))
       ((<= (abs delta) tolerance)
        e)
    ;returned value
    ;; do* body is empty
    ))


(defun OBLIQUITY-OF-THE-ECLIPTIC (jd &aux (tu (/ (- jd -28980) 36525)))
  
  "return obliquity of the ecliptic in radians"
  
  ;; the formula is: 23d27'08  - 46.8 *T where T is centuries since 1900
  ;; used below:  23d27'08  = 0.409318494687159049 radians
  ;; 46.8  = 2.26892802759262845E-4 radians
  ;; Note: -28980 is JD 1900.0 - 2444000 (TJD)
  
  (- 0.40931849468715903 (* tu 2.2689280275926285e-4)))


(defmethod MEAN-ANOMALY ((sun-class sun-class) jd)
  
  "return sun mean anomaly, radians"
  
  (let* ((days-since-1980 (- jd (reference-epoch sun-class)))
         (n (mod (/ (* 2pi days-since-1980) (period sun-class)) 2pi))
         (mean-anomaly
          (mod (- (+ n (ecliptic-long-at-epoch sun-class)) 
                  (ecliptic-long-of-perigee sun-class))
               2pi)))
    mean-anomaly))


(defmethod TRUE-ANOMALY ((sun-class sun-class) jd)
  
  "return sun true anomaly, radians"
  
  (if (and (time-of-true-anomaly sun-class) 
           (= jd (time-of-true-anomaly sun-class)))
    ;; return last calculated value
    (last-true-anomaly sun-class)
    ;; calculate and store new value
    (let* ((days-since-1980 (- jd (reference-epoch sun-class)))
           (n (mod (/ (* 2pi days-since-1980) (period sun-class)) 2pi))
           (mean-anomaly
            (mod
             (- (+ n (ecliptic-long-at-epoch sun-class)) (ecliptic-long-of-perigee sun-class))
             2pi))
           (eccentric-anomaly (kepler-equation mean-anomaly (ecc sun-class)))
           (tan-nu-over-2
            (* (sqrt (/ (+ 1.0 (ecc sun-class)) (- 1.0 (ecc sun-class))))
               (tan (/ eccentric-anomaly 2.0))))
           (nu (* 2 (atan tan-nu-over-2))))
      (setf (last-true-anomaly sun-class) nu)
      (setf (time-of-true-anomaly sun-class) jd)
      nu)))


(defmethod ECLIPTIC-LONGITUDE ((sun-class sun-class) jd)
  
  "Return ecliptic longitude of sun, radians, at date"

  (let ((true-anomaly (true-anomaly sun-class jd)))
    (mod (+ true-anomaly (ecliptic-long-of-perigee sun-class)) 2pi)))


(defmethod APPROX-ECLIPTIC-LONGITUDE ((sun-class sun-class) jd)
  
  "return approx sun ecliptic longitude, radians"
  
  (let* ((days-since-1980 (- jd (reference-epoch sun-class)))
         (n (mod (/ (* 2pi days-since-1980) (period sun-class)) 2pi))
         (mean-anomaly
          (mod (- (+ n (ecliptic-long-at-epoch sun-class)) 
                  (ecliptic-long-of-perigee sun-class))
               2pi))
         (ec (* 2 (ecc sun-class) (sin mean-anomaly)))
         (long (mod (+ n ec (ecliptic-long-at-epoch sun-class)) 2pi)))
    long))


(defmethod DISTANCE ((sun-class sun-class) jd &optional (print-steps nil))
  
  (declare (ignore print-steps))
  (let ((true-anomaly (true-anomaly sun-class jd)))
    (/ (mean-radius sun-class)
       (/ (+ 1 (* (ecc sun-class) (cos true-anomaly))) 
          (- 1 (* (ecc sun-class) (ecc sun-class)))))))


(defmethod POSITION-VECTOR ((sun-class sun-class) jd)
  
  "return sun position vector (equatorial frame)"
  
  ;; x and y span the sun's orbital plane: x is along vernal equinox,
  ;; y is perpendicular and inclined by obliquity of the ecliptic
  
  (let* ((obliq (obliquity-of-the-ecliptic jd))
         (lambda (ecliptic-longitude sun-class jd))
         (x '(1 0 0))
         (y (list 0 (cos obliq) (sin obliq))))
    (3vect-linear-comb (cos lambda) x (sin lambda) y)))


(defmethod TEST-CASE ((sun-class sun-class))
  "test case from Duffett-Smith, Practical Astronomy with Your Calculator,
    1st edition, p. 53-54
    differences probably from use of 1980 as fundamental epoch in these
    routines"
  (let* ((jd (utjd 27 7 1978))			; date of test case
         (true (degrees (true-anomaly sun-class jd)))
         (lambda (degrees (ecliptic-longitude sun-class jd)))
         (ra-dec
          (mapcar #'degrees (3vect-cartesian-to-spherical 
                             (position-vector sun-class jd))))
         (dist (distance sun-class jd)))
    (format t "~%SUN test case, date 27-Jul-1978 (JD ~a)" jd)
    (format t "~%  true anomaly: ~ad (cf. -158.909842d)" true)
    (format t "~%  longitude: ~ad (cf. 123.600554d)" lambda)
    (format t "~%  distance: ~a km (cf. 1.519270e8 km)" dist)
    (format t "~%  RA,Dec: ~a ~a (cf. 8h 23m 38s, 19d 20m 38s)"
            (degrees-of-ra-to-hms (first ra-dec)) 
            (degrees-of-dec-to-dms (second ra-dec)))
    nil))


(defmethod DATE-OF-SUN-LONGITUDE ((sun-class sun-class) jd long)
  
  "return the first date after jd that sun is at ecliptic longitude long"
  
  (let* (
         ;; compute true anomaly at longitude
         (nu (- long (ecliptic-long-of-perigee sun-class)))
         ;; compute eccentric anomaly
         (eccentric-anomaly
          (* 2 (atan (/ (tan (/ nu 2)) (sqrt (/ (+ 1 (ecc sun-class)) 
                                                (- 1 (ecc sun-class))))))))
         ;; compute mean anomaly from Kepler's equation
         (mean-anomaly (- eccentric-anomaly (* (ecc sun-class) 
                                               (sin eccentric-anomaly))))
         (n (- mean-anomaly (ecliptic-long-at-epoch sun-class)
               (- (ecliptic-long-of-perigee sun-class))))	;radians
         (n-days (* (period sun-class) (/ n 2pi)))
         (i (- (floor (- jd (reference-epoch sun-class)) 
                      (period sun-class)) 1)))
    
    ;;(format t  ~%year offset: ~a result ~a  i result)
    (do ((result (+ (reference-epoch sun-class) (* i (period sun-class)) n-days)
                 (+ result (period sun-class))))
        ((>= result jd)
         result)				;returned value
      ;; loop body is empty
      )))


(defun SUN-LONGITUDE (jd)
  
  "Longitude of sun at date, radians"
  
  (first (3vect-cartesian-to-spherical (position-vector *sun* jd))))


;;; ======================================================================
;;;                                         ECLIPTIC COORDINATE CONVERSION
;;; ecliptic to/from celestial coordinates
;;;

(defun ECLIPTIC-TO-CELESTIAL (ecliptic-3vect 
                              &key (obliquity *default-ecliptic-obliquity*))
  
  "Convert 3vector in ecliptic coordinates to celestial coordinates"
  
  (let* ((sin-obliquity (sin obliquity))
         (cos-obliquity (cos obliquity)))
    (list (first ecliptic-3vect)
          (- (* cos-obliquity (second ecliptic-3vect)) 
             (* sin-obliquity (third ecliptic-3vect)))
          (+ (* sin-obliquity (second ecliptic-3vect)) 
             (* cos-obliquity (third ecliptic-3vect))))))


(defun CELESTIAL-TO-ECLIPTIC (celestial-3vect 
                              &key (obliquity *default-ecliptic-obliquity*))
  
  "Convert 3vector in celestial coordinates to ecliptic coordinates"
  
  (let* ((sin-obliquity (sin obliquity))
         (cos-obliquity (cos obliquity)))
    (list (first celestial-3vect)
          (+ (* cos-obliquity (second celestial-3vect))
             (* sin-obliquity (third celestial-3vect)))
          (- (* cos-obliquity (third celestial-3vect))
             (* sin-obliquity (second celestial-3vect))))))

#| test cases:
(mapcar #'degrees
        (3vect-cartesian-to-spherical
         (ecliptic-to-celestial
          (3vect-spherical-to-cartesian
           (mapcar #'radians '(12 13))))))
;; result should be: (5.7739 16.6770)

(mapcar #'degrees
        (3vect-cartesian-to-spherical
         (celestial-to-ecliptic
          (3vect-spherical-to-cartesian
           (mapcar #'radians '(5.7739 16.6770))))))
;; result should be (12 13)
|#

;;; ======================================================================
;;;                                                          SUN EXCLUSION
;;;
;;; sun exclusion:  compute the times that a target is within a specified
;;; angle of the sun
;;;
;;; KNOWN INEFFICIENCY:  should check that target is too far off the ecliptic
;;; to ever be within specified angle of the moon (won't cause error,
;;; just does extra work)
;;;

(defvar *SUN-LONGITUDE-TABLE* nil) 

(defstruct SUN-LONGITUDE-TABLE
  (initialized nil)
  start-date
  end-date
  number-of-dates
  date-array
  longitude-array)

;; Coerced start/end to double-float - JLS 3/89

(defun INITIALIZE-SUN-LONGITUDE-TABLE (start end &key (interval 30))
  
  "Populate a sun-longitude-table and set *sun-longitude-table* to point
    to it.  Initialize the fields of the table are initialized.
    interval (days) must be small compared to period (i.e. year)"
  
  (setf start (coerce start 'double-float)
        end (coerce end 'double-float))
  
  ;; create a table struct if it doesn't already exist
  (if (not *sun-longitude-table*)
    (setf *sun-longitude-table* (make-sun-longitude-table)))
  
  ;; make the struct and populate scalar fields
  (let ((n (1+ (ceiling (- end start) interval))))
    
    ;;(format t  ~%Number of dates: ~d  N)
    (setf (sun-longitude-table-start-date *sun-longitude-table*) start
          (sun-longitude-table-end-date *sun-longitude-table*) end
          (sun-longitude-table-number-of-dates *sun-longitude-table*) n
          (sun-longitude-table-date-array *sun-longitude-table*)
          (make-array n :element-type 'double-float)
          (sun-longitude-table-longitude-array *sun-longitude-table*)
          (make-array n :element-type 'double-float))
    
    ;; fill the arrays
    (dotimes (i n)
      (let* ((date (+ start (* i interval)))
             (longitude (ecliptic-longitude *sun* date)))
        (setf (aref (sun-longitude-table-date-array 
                     *sun-longitude-table*) i) date
              (aref (sun-longitude-table-longitude-array 
                     *sun-longitude-table*) i) longitude)
        ;;(format t  ~%~a ~a  date longitude)
        )))
  (setf (sun-longitude-table-initialized *sun-longitude-table*) t)
  *sun-longitude-table*)


(defun ADJUST-SUN-LONGITUDE-TABLE (start end &key (interval 30))
  
  "Adjust sun longitude table to new start, end, and interval"
  
  (if (not *sun-longitude-table*)
    (initialize-sun-longitude-table start end :interval interval)
    (if (or (< start (sun-longitude-table-start-date *sun-longitude-table*))
            (> end (sun-longitude-table-end-date *sun-longitude-table*)))
      (initialize-sun-longitude-table
       (min start (sun-longitude-table-start-date *sun-longitude-table*))
       (max end (sun-longitude-table-end-date 
                 *sun-longitude-table*)) :interval interval)))
  *sun-longitude-table*)


(defun DATES-OF-SAME-TARGET-AND-SUN-LONGITUDE (target-longitude start end)
  (adjust-sun-longitude-table start end)
  (let ((result (quote nil))
        (target-longitude-plus-2pi (+ 2pi target-longitude)))
    (dotimes (i (- (sun-longitude-table-number-of-dates 
                    *sun-longitude-table*) 1))
      (let ((d1 (aref (sun-longitude-table-date-array 
                       *sun-longitude-table*) i))
            (d2 (aref (sun-longitude-table-date-array 
                       *sun-longitude-table*) (1+ i)))
            (l1 (aref (sun-longitude-table-longitude-array 
                       *sun-longitude-table*) i))
            (l2 (aref (sun-longitude-table-longitude-array 
                       *sun-longitude-table*) (1+ i))))
        (if (< l2 l1)
          (incf l2 2pi))
        
        ;;(format t   i=~d  i)
        (cond
         ((and (>= target-longitude l1) (< target-longitude l2))
          (let* ((slope (/ (- l2 l1) (- d2 d1)))
                 (est (+ d1 (/ (- target-longitude l1) slope)))
                 ;;(check (send *sun* :ecliptic-longitude est))
                 )
            (push est result)
            ))
         ((and (>= target-longitude-plus-2pi l1) 
               (< target-longitude-plus-2pi l2))
          (let* ((slope (/ (- l2 l1) (- d2 d1)))
                 (est (+ d1 (/ (- target-longitude-plus-2pi l1) slope)))
                 ;;(check (send *sun* :ecliptic-longitude est))
                 )
            (push est result)
            ))
         (t nil))))
    
    (nreverse result)))


(defun TARGET-SUN-EXCLUSION (target-position-vector start end exclusion-angle)

  "Return list of times (entry1 exit1 entry2 exit2 ...) for entering into sun
    exclusion region.  Dates are extended by a year in both directions so 
    the result will include any exclusions overlapping with specified 
    start-end range.  Max exclusion angle is 90deg."

  (let* ((target-ecliptic-3vector 
          (celestial-to-ecliptic target-position-vector))
         (target-ecliptic-long-lat 
          (3vect-cartesian-to-spherical target-ecliptic-3vector))
         (target-ecliptic-longitude (first target-ecliptic-long-lat))
         (date-list
          (dates-of-same-target-and-sun-longitude 
           target-ecliptic-longitude (- start 366)
           (+ end 366)))
         (cos-exclusion-angle (cos exclusion-angle))
         start-exclusion
         end-exclusion
         (result (quote nil)))
    
    (dolist (d date-list)
      (setf start-exclusion
            (bisection-root (- d 90) d cos-exclusion-angle
                            #'(lambda (x)
                                (3vect-dot-product target-position-vector
                                 (position-vector *sun* x))))
            end-exclusion
            (bisection-root d (+ d 90) cos-exclusion-angle
                            #'(lambda (x)
                                (3vect-dot-product target-position-vector
                                 (position-vector *sun* x)))))
      (when (and start-exclusion end-exclusion)
        (push start-exclusion result)
        (push end-exclusion result))
      ;;(format t   ~a ~a  start-exclusion end-exclusion)
      )
    (nreverse result)))

#|  Test case:
(dmy-to-time '1-jan-88)
(dmy-to-time '1-jan-90)

(initialize-sun-longitude-table 3161 3892 :interval 30)
(adjust-sun-longitude-table 3500 4500 :interval 30)
(dates-of-same-target-and-sun-longitude 0 3000 4500)

(time (target-sun-exclusion '(1 0 0) 3161 3892 (radians 50)))

|#

;;; ======================================================================
;;;                                                         ZODIACAL LIGHT

;;; --- constants

(defparameter *ZL-THRESHOLD* 0.1
   "ignore effect of zodiacal light on suitability if maximum zodiacal light
   is less than this times target flux") 

(defparameter *FLUX-PER-S10* 7.715999999999999e-9
   "flux in photons s-1 cm-2 A-1 for one S10 unit") 

(defparameter *ZODIACAL-LIGHT-SAMPLING* 14
   "zodiacal light PCF will be sampled no less often than this, in days") 

;;; --- test cases

#|
;;; target on ecliptic, at atumnal equinox
(setq *mytargpos* (ra-dec-to-cartesian-3vect '(180.0 0) :units :degrees))

(zodiacal-light-suitability 
 (utjd 21 3 1988) *mytargpos* 1.3888e-6 5000) ;; at antisun,.848148
(zodiacal-light-suitability 
 (utjd 21 6 1988) *mytargpos* 1.3888e-6 5000) ;; 90 to sun,.78424
(zodiacal-light-suitability 
 (utjd 21 9 1988) *mytargpos* 1.3888e-6 5000) ;; at sun, .11338
(zodiacal-light-suitability 
 (utjd 21 12 1988) *mytargpos* 1.3888e-6 5000) ;; same as 6/21

(zodiacal-light-suitability 
 (utjd 21 12 1988) *mytargpos* 1.6e-5 5000) ;; 1, brighter than threshold

(min-zodiacal-light *mytargpos* 5000)  ;;1.072524e-6

(zodiacal-light-brightness 
 (utjd 21 3 1988) *mytargpos* 5000)     ;; at antisun, 1.38888e-6
(zodiacal-light-brightness 
 (utjd 21 6 1988) *mytargpos* 5000)     ;; 90 to sun, 1.558632e-6
(zodiacal-light-brightness 
 (utjd 21 9 1988) *mytargpos* 5000)     ;; at sun, 1.489e-5
(zodiacal-light-brightness 
 (utjd 21 12 1988) *mytargpos* 5000)    ;; same as 6/21

(interpolate-zodiacal-light-table 
 (3vect-to-helioecliptic *mytargpos* (utjd 21 3 1988)))  ;;180
(interpolate-zodiacal-light-table 
 (3vect-to-helioecliptic *mytargpos* (utjd 21 6 1988)))  ;;202
(interpolate-zodiacal-light-table 
 (3vect-to-helioecliptic *mytargpos* (utjd 21 9 1988)))  ;;1930
|#

#|
;;; target at south ecliptic pole
(setq *mytargpos* (ra-dec-to-cartesian-3vect '(90.0 -65.0) :units :degrees))

(3vect-cartesian-to-spherical 
 (celestial-to-ecliptic (ra-dec-to-cartesian-3vect '(90.0 -65.0) :units :degrees)))

;; suitability is 1.0 everywhere because there is no variation in ZL 
;; at the ecliptic pole
(zodiacal-light-suitability 
 (utjd 21 3 1988) *mytargpos* 1.3888e-6 5000) ;; at antisun,
(zodiacal-light-suitability 
 (utjd 21 6 1988) *mytargpos* 1.3888e-6 5000) ;; 90 to sun,
(zodiacal-light-suitability 
 (utjd 21 9 1988) *mytargpos* 1.3888e-6 5000) ;; at sun,
(zodiacal-light-suitability 
 (utjd 21 12 1988) *mytargpos* 1.3888e-6 5000) ;; same as 6/21

(min-zodiacal-light *mytargpos* 5000) ;;4.86e-7

(zodiacal-light-brightness 
 (utjd 21 3 1988) *mytargpos* 5000)     ;; at antisun,same as min above
(zodiacal-light-brightness 
 (utjd 21 6 1988) *mytargpos* 5000)     ;; 90 to sun,ditto
(zodiacal-light-brightness 
 (utjd 21 9 1988) *mytargpos* 5000)     ;; at sun,ditto
(zodiacal-light-brightness 
 (utjd 21 12 1988) *mytargpos* 5000)    ;; same as 6/21

(interpolate-zodiacal-light-table 
 (3vect-to-helioecliptic *mytargpos* (utjd 21 3 1988))) ;63 any date

|#

;;; --- zodiacal light suitability

(defun CALCULATE-ZODIACAL-LIGHT-PCF (start end astro-object brightness wavelength)
  
  "Return a PCF for zodiacal light covering the start-end time interval for 
    the astronomical object with brightness in wavelength region"
  
  ;; calculate a time step which is the largest number of intervals
  ;; which still has resolution smaller than *zodiacal-light-step*
  ;; start from end time and proceed backwards so that resulting list
  ;; is ordered in increasing time
  (declare (special *zodiacal-light-sampling*))
  (let* ((range (- end start))
         (intervals (ceiling range *zodiacal-light-sampling*))
         (step (/ range intervals))
         (result `(,(+ end step) 1)))
    (do ((time end (- time step)))
        ((< time start)
         (progn
           ;; Get suitability for start of interval.
           ;; this ensures we cover the entire interval.
           (push (zodiacal-light-suitability 
                  start (position-vector astro-object start)
                  brightness wavelength) result)
           (push start result)))
      (push (zodiacal-light-suitability 
             time (position-vector astro-object time)
             brightness wavelength) result)
      (push time result))
    (unless (<= (first result) *minus-infinity*)
      (push 1 result)
      (push *minus-infinity* result))
    result))


(defun ZODIACAL-LIGHT-SUITABILITY (time position brightness wavelength)
  
  "Return the suitability for Zodiacal Light background at an instant of time.
    position is 3vector position of target, brightness is target flux at 
    given wavelength"
  
  ;; suitability is square of signal to noise at time t to maximum signal 
  ;; to noise.  If test speeds calculation when target isn't affected by 
  ;; zodiacal light
  (let ((zl (zodiacal-light-brightness time position wavelength))
        (min-zl (min-zodiacal-light position wavelength)))
    (if (> zl (* brightness *zl-threshold*))
      (/ (+ brightness (* 2 min-zl)) (+ brightness (* 2 zl)))
      1)))


;;; --- zodiacal light calculation

(defun ZODIACAL-LIGHT-BRIGHTNESS (time position wavelength)
  
  "return zodiacal light brightness seen at position and time for
   that wavelength. units of photons cm-2 s-1 A-1.
   wavelength dependence not implemented yet"
  
  ;; using helioecliptic coordinates, lookup zodiacal light in S10 units,
  ;; convert from S10 to flux, then scale to proper wavelength
  (let* ((zl-in-s10 (interpolate-zodiacal-light-table 
                     (3vect-to-helioecliptic position time)))
         (zl-flux (s10-to-flux zl-in-s10)))
    (scale-zodiacal-light-to-wavelength zl-flux wavelength)))


(defun S10-TO-FLUX (brightness)
  
  "return flux in photons cm-2 s-1 A-1 corresponding to brightness in S10"
  
  ;;; ZL literature uses S10 units. These are the number of 10th magnitude
  ;;; stars per square degree, so you multiply by flux per single S10
  (* brightness *flux-per-s10*))


(defun SCALE-ZODIACAL-LIGHT-TO-WAVELENGTH (flux wavelength)
  
  "given flux at reference wavelength, return zodiacal light at 
    indicated wavelength"
  
  (declare (ignore wavelength))
  
  ;; stubbed out, but will eventually scale to solar spectrum
  flux)


(defun MIN-ZODIACAL-LIGHT (position wavelength)
  
  "return minimum possible value of zodiacal light brightness seen
    at that position at that wavelength. units of photons cm-2 s-1.
    wavelength dependence not implemented yet"
  
  (let* ((helio-lat (second (3vect-cartesian-to-spherical 
                             (celestial-to-ecliptic position))))
         (zl-in-s10 (interpolate-min-zodiacal-light-table helio-lat))
         (zl-flux (s10-to-flux zl-in-s10)))
    (scale-zodiacal-light-to-wavelength zl-flux wavelength)))


;;; ----------------------------------------------------------------------
;;;                                         ARRAY OF ZODIACAL LIGHT VALUES
;;;
;;; Taken from Levasseur-Regourd and Dumont 
;;;  1980 Astronomy and Astrophysics Vol 84, p 277-279.
;;; Entries are brightness in S10 units
;;; Paper does not give entries for flux very close (30 degrees or so) to sun. 
;;; I have used nearest known value, which will be a gross underestimate. 
;;; However telescope will not point that close to sun anyway.


(defparameter *ZODIACAL-LIGHT-ARRAY*
  (make-array 
   '(37 19) :element-type 'integer :initial-contents
   '(
     ; 0 degrees helioecliptic longitude
     ;0 to 90 degrees helioecliptic latitude in increment of 5 degrees
     (1930 1210 825 615 580 490 500 360 275 215 170 140 118 103 90 80 74 68 63)
     ; 5 degrees
     (1930 1210 825 615 580 490 490 355 271 212 169 139 118 103 90 80 74 68 63)
     ;10 degrees
     (1930 1210 825 615 580 490 460 340 260 206 167 138 117 102 90 80 74 68 63)
     ;15 degrees
     (1930 1210 825 615 580 490 410 310 242 196 162 136 115 100 89 80 73 67 63)
     ;20 degrees
     (1930 1210 825 615 580 490 355 278 226 185 157 130 111 98 88 79 73 67 63)
     ;25 degrees
     (1930 1210 825 615 580 490 355 278 226 185 157 130 111 98 88 79 73 67 63)
     ;30 degrees
     (1930 1210 825 615 475 370 285 230 194 162 140 119 103 93 84 76 70 66 63)
     ;35 degrees
     (1275 905 630 495 384 312 250 208 177 151 132 113 99 90 82 75 69 66 63)
     ;40 degrees
     (920 695 510 400 316 262 220 186 160 140 123 108 95 87 80 74 68 65 63)
     ;45 degrees
     (712 550 422 335 270 228 195 168 146 130 115 103 92 84 78 72 67 65 63)
     ;50 degrees
     (572 458 355 285 238 200 173 152 135 120 108 98 89 82 76 71 67 65 63)
     ;55 degrees
     (470 395 310 253 209 179 158 140 125 113 103 93 85 79 74 70 66 64 63)
     ;60 degrees
     (394 342 275 228 190 163 143 129 116 105 96 89 82 77 72 69 66 64 63)
     ;65 degrees
     (338 305 250 205 174 152 133 120 109 99 92 85 79 75 70 68 65 64 63)
     ;70 degrees
     (296 273 228 188 162 142 124 112 103 95 88 82 77 73 69 67 65 64 63)
     ;75 degrees
     (264 248 210 177 153 134 118 107 98 91 85 79 74 71 68 66 64 63 63)
     ;80 degrees
     (239 227 197 167 144 127 113 103 94 87 82 77 72 70 67 65 64 63 63)
     ;85 degrees
     (219 211 186 158 137 121 108 99 90 84 79 74 70 68 65 64 63 63 63)
     ;90 degrees
     (202 196 176 151 130 115 103 95 87 81 76 72 68 66 64 64 63 63 63)
     ;95 degrees
     (187 184 168 144 125 111 99 92 84 79 74 70 66 64 63 63 62 63 63)
     ;100 degrees
     (175 172 160 137 120 107 96 89 82 77 72 68 65 63 62 62 62 62 63)
     ;105 degrees
     (166 164 154 133 117 104 93 86 80 75 70 67 64 62 61 62 62 62 63)
     ;110 degrees
     (158 156 148 128 113 101 91 84 78 73 68 65 63 61 61 61 62 62 63)
     ;115 degrees
     (152 150 143 124 111 100 89 82 76 71 67 64 62 61 60 61 61 62 63)
     ;120 degrees
     (147 145 138 120 108 98 88 81 75 70 66 63 61 60 59 60 61 62 63)
     ;125 degrees
     (144 142 135 118 106 96 87 80 74 69 65 63 61 60 59 60 61 62 63)
     ;130 degrees
     (141 140 132 116 105 95 86 80 74 69 65 63 61 60 59 60 60 62 63)
     ;135 degrees
     (140 139 130 115 105 95 86 80 74 70 66 63 61 59 58 59 60 62 63)
     ;140 degrees
     (139 138 129 115 105 96 87 81 75 71 67 64 62 60 58 59 60 61 63)
     ;145 degrees
     (139 138 129 115 106 97 89 83 77 73 69 65 62 60 58 58 59 64 63)
     ;150 degrees
     (140 139 129 116 107 99 91 86 80 75 71 67 63 60 58 58 59 61 63)
     ;155 degrees
     (143 140 130 118 110 102 94 89 83 78 73 68 64 61 58 58 59 61 63)
     ;160 degrees
     (147 144 134 122 113 106 98 93 86 80 75 69 65 61 59 58 59 61 63)
     ;165 degrees
     (153 150 140 129 118 110 102 95 88 81 75 70 65 62 59 58 58 61 63)
     ;170 degrees
     (161 158 147 135 123 114 104 96 89 82 76 70 66 62 59 58 58 60 63)
     ;175 degrees 
     (169 163 151 138 126 115 105 96 89 82 76 71 66 62 59 58 58 60 63)
     ;180 degrees
     (180 166 152 139 127 116 105 96 89 82 76 71 66 62 59 58 58 60 63)))
  "array of zodiacal light values") 


(defun INTERPOLATE-ZODIACAL-LIGHT-TABLE (helioecliptic-pos)
  
  "return brightness of zodiacal light in units of S10 for specified
    helioecliptic coordinates"
  
  ;; interpolation is very simple: round coords to grid points in table.
  ;; should be fine for long range planning, and if ever needed, its
  ;; an easy change to add a real 2 coordinate interpolation scheme
  (multiple-value-bind (index1 index2) 
                       (helioecliptic-to-array-index helioecliptic-pos)
    (aref *zodiacal-light-array* index1 index2)))

#| test data
(interpolate-zodiacal-light-table (list (radians 95) (radians 40)));;  84	
(interpolate-zodiacal-light-table (list (radians 180) (radians 90))) ;; 63
|#


(defun HELIOECLIPTIC-TO-ARRAY-INDEX (helioecliptic-pos)
  
  "converts helioecliptic position into indices in *zodiacal-light-array*"
  
  ;; table is every 5 degrees from helio-long = 0 to 180 and lat = 0 to 90
  (let ((helio-long (first helioecliptic-pos))
        (helio-lat (second helioecliptic-pos)))
    (values (round (degrees helio-long) 5) 
            (round (degrees helio-lat) 5))))


(defun PRINT-ZODIACAL-LIGHT-VALUES (&key (min-long 0) (min-lat 0) 
                                         (file "zl.dat"))
  
  "print the zodiacal light values in a format suitable for MacSpin 3d plotting.
   format is long, lat, ZL"
  
  (with-open-file (stream file :direction :output :if-exists :supersede)
    (format stream "helioecliptic-long helioecliptic-lat zodiacal-light")
    (do ((long min-long (+ long 5)))
        ((> long 180))
      (do ((lat min-lat (+ lat 5)))
          ((> lat 90))
        (format stream "~% ~d ~d ~d" long lat
                (interpolate-zodiacal-light-table 
                 (list (radians long) (radians lat))))))))


;;; ----------------------------------------------------------------------
;;;                                 ARRAY OF MINIMUM ZODIACAL LIGHT VALUES
;;; data taken from same reference as zodiacal light table


(defparameter *MIN-ZODIACAL-LIGHT-ARRAY*
  (make-array 
   '(19) :element-type 'integer :initial-contents
   ;0 to 90 degrees helioecliptic latitude in increment of 5 degrees
   
   '(139 138 129 115 105 95 86 80 74 69 65 63 61 59 58 58 59 60 63))
  "array of minimum zodiacal light values")


(defun INTERPOLATE-MIN-ZODIACAL-LIGHT-TABLE (helio-lat)
  
  "return brightness of zodiacal light in units of S10 for specified 
    helioecliptic longitude"
  
  ;; interpolation is very simple: round coords to grid points in table.
  ;; should be fine for long range planning, and if ever needed, its
  ;; an easy change to add a real 2 coordinate interpolation scheme
  (aref *min-zodiacal-light-array* 
        (helioecliptic-to-min-array-index helio-lat)))

#| test data
(interpolate-min-zodiacal-light-table (radians 40)) ;;  74
(interpolate-min-zodiacal-light-table (radians 90)) ;;  63
|#

(defun HELIOECLIPTIC-TO-MIN-ARRAY-INDEX (helio-lat)
  
  "converts helioecliptic latitude into index in *min-zodiacal-light-array*"
  
  ;; table is every 5 degrees from lat = 0 to 90
  (round (degrees (abs helio-lat)) 5))


(defun 3VECT-TO-HELIOECLIPTIC (3vec time)
  
  "return helioecliptic longitude and latitude for 3vector position at time"
  
  ;; helioecliptic longitude is defined as ecliptic longitude minus
  ;; ecliptic longitude of sun with angle always between 0 and 180
  ;; helioecliptic latitude is absolute value of ecliptic latitude
  ;; used to determine zodiacal light
  (declare (special *sun*))
  (let* ((sun-ecliptic-long
          (first
           (3vect-cartesian-to-spherical 
            (celestial-to-ecliptic (position-vector *sun* time)))))
         (ecliptic-pos (3vect-cartesian-to-spherical 
                        (celestial-to-ecliptic 3vec)))
         (ecliptic-long (first ecliptic-pos))
         (ecliptic-lat (second ecliptic-pos)))
    (list (included-angle ecliptic-long sun-ecliptic-long) 
          (abs ecliptic-lat))))


;;; ======================================================================
;;;                                                     CLASS:  MOON-CLASS


(defclass MOON-CLASS (astronomical-object)
  ((reference-epoch
       :initform  238.5
       :initarg  :reference-epoch
       :accessor  reference-epoch)		; TJD, 1980.0
   (ecc
       :initform  0.0549
       :initarg  :ecc
       :accessor  ecc)
   (mean-long-at-epoch
       :initform  (radians 64.975464)
       :initarg  :mean-long-at-epoch
       :accessor  mean-long-at-epoch)
   (mean-long-of-perigee-at-epoch
       :initform  (radians 349.383063)
       :initarg  :mean-long-of-perigee-at-epoch
       :accessor  mean-long-of-perigee-at-epoch)
   (mean-long-of-node-at-epoch
       :initform  (radians 151.950429)
       :initarg  :mean-long-of-node-at-epoch
       :accessor  mean-long-of-node-at-epoch)
   (inclination
       :initform  (radians 5.145396)
       :initarg  :inclination
       :accessor  inclination)
   (sin-inclination
       :initform  (sin (radians 5.145396))
       :initarg  :sin-inclination
       :accessor  sin-inclination)
   (cos-inclination
       :initform  (cos (radians 5.145396))
       :initarg  :cos-inclination
       :accessor  cos-inclination)
   (angular-size
       :initform  (radians 0.5181)
       :initarg  :angular-size
       :accessor  angular-size)
   (semi-major-axis
       :initform  384401.0
       :initarg  :semi-major-axis
       :accessor  semi-major-axis)		;km
   ;; for state: remember last ecliptic coord calculation
   ;; these are set by method :ecliptic-coordinates
   (time-of-long-lat
       :initform  nil
       :initarg  :time-of-long-lat
       :accessor  time-of-long-lat)
   (last-ecliptic-longitude
       :initform  nil
       :initarg  :last-ecliptic-longitude
       :accessor  last-ecliptic-longitude)
   (last-ecliptic-latitude
       :initform  nil
       :initarg  :last-ecliptic-latitude
       :accessor  last-ecliptic-latitude)
   ;; needed for computing phase and distance
   (last-corrected-anomaly
       :initform  nil
       :initarg  :last-corrected-anomaly
       :accessor  last-corrected-anomaly)
   (last-center-correction
       :initform  nil
       :initarg  :last-center-correction
       :accessor  last-center-correction)
   (last-true-longitude
       :initform  nil
       :initarg  :last-true-longitude
       :accessor  last-true-longitude)))


(defmethod ECLIPTIC-COORDINATES ((moon-class moon-class) jd 
                                 &optional (print-steps nil))
  
  "return moon ecliptic coordinates as multiple values
    longitude latitude, in radians"
  
  (if (and (time-of-long-lat moon-class) 
           (= jd (time-of-long-lat moon-class)))
    
    (values (last-ecliptic-longitude moon-class) 
            (last-ecliptic-latitude moon-class))
    
    (let* ((days-since-1980 (- jd (reference-epoch moon-class)))
           (sun-longitude (approx-ecliptic-longitude *sun* jd))
           (sun-mean-anomaly (mean-anomaly *sun* jd))
           (l
            (mod (+ (mean-long-at-epoch moon-class) 
                    (* (radians 13.1763966) days-since-1980))
                 2pi))
           (mm
            (- l (* (radians 0.111404) days-since-1980)
               (mean-long-of-perigee-at-epoch moon-class)))
           (n
            (- (mean-long-of-node-at-epoch moon-class) 
               (* (radians 0.0529539) days-since-1980)))
           (c (- l sun-longitude))
           (ev (* (radians 1.2739) (sin (- (* 2 c) mm))))
           (sin-sun-mean-anomaly (sin sun-mean-anomaly))
           (ae (* (radians 0.1858) sin-sun-mean-anomaly))
           (a3 (* (radians 0.37) sin-sun-mean-anomaly))
           (mm-prime (- (+ mm ev) (+ ae a3)))
           (ec (* (radians 6.2886) (sin mm-prime)))
           (a4 (* (radians 0.214) (sin (* 2 mm-prime))))
           (l-prime (+ l ev ec (- ae) a4))
           (v (* (radians 0.6583) (sin (* 2 (- l-prime sun-longitude)))))
           (l-prime-prime (+ l-prime v))
           (n-prime (- n (* (radians 0.16) sin-sun-mean-anomaly)))
           (lmn (- l-prime-prime n-prime))
           (sin-lmn (sin lmn))
           (y (* sin-lmn (cos-inclination moon-class)))
           (x (cos lmn))
           (moon-longitude (+ n-prime (atan y x)))
           (moon-latitude (asin (* sin-lmn (sin-inclination moon-class)))))
      (when print-steps
        ;;; steps below are in Duffet-Smith, pp. 141-142
        (format t "~%2.  days since 1980 ~a" days-since-1980)
        (format t "~%3.  sun long ~a deg" (degrees sun-longitude))
        (format t "~%    sun mean anom ~a deg" (degrees sun-mean-anomaly))
        (format t "~%4.  l ~a deg" (degrees l))
        (format t "~%5.  Mm ~a deg" (degrees mm))
        (format t "~%6.  N ~a deg" (degrees n))
        (format t "~%7.  Ev ~a deg" (degrees ev))
        (format t "~%8.  Ae ~a deg" (degrees ae))
        (format t "~%    A3 ~a deg" (degrees a3))
        (format t "~%9.  Mm' ~a deg" (degrees mm-prime))
        (format t "~%10. Ec ~a deg" (degrees ec))
        (format t "~%11. A4 ~a deg" (degrees a4))
        (format t "~%12. l' ~a deg" (degrees l-prime))
        (format t "~%13. V ~a deg" (degrees v))
        (format t "~%14. l'' ~a deg" (degrees l-prime-prime))
        (format t "~%15. N' ~a deg" (degrees n-prime))
        (format t "~%16. y ~a" y)
        (format t "~%17. x ~a" x)
        (format t "~%19. long ~a deg" (degrees moon-longitude))
        (format t "~%20. lat ~a deg" (degrees moon-latitude)))
      
      ;; save state
      (setf (last-ecliptic-longitude moon-class) moon-longitude
            (last-ecliptic-latitude moon-class) moon-latitude
            (time-of-long-lat moon-class) jd
            (last-corrected-anomaly moon-class) mm-prime
            (last-center-correction moon-class) ec
            (last-true-longitude moon-class) l-prime-prime)
      (values moon-longitude moon-latitude))))


(defmethod ECLIPTIC-LONGITUDE ((moon-class moon-class) jd)
  
  (multiple-value-bind (longitude latitude) 
                       (ecliptic-coordinates moon-class jd)
    (declare (ignore latitude))
    (mod longitude 2pi)))


(defmethod ECLIPTIC-LATITUDE ((moon-class moon-class) jd)
  (multiple-value-bind (longitude latitude) 
                       (ecliptic-coordinates moon-class jd)
    (declare (ignore longitude))
    latitude))


(defmethod ECLIPTIC-3VECTOR ((moon-class moon-class) jd)
  (3vect-spherical-to-cartesian 
   (multiple-value-list (ecliptic-coordinates moon-class jd))))


(defmethod POSITION-VECTOR ((moon-class moon-class) jd)
  (ecliptic-to-celestial 
   (ecliptic-3vector moon-class jd)))


(defmethod DISTANCE ((moon-class moon-class) jd &optional (print-steps nil))
  ;; get current values for corrected anomaly and center correction
  (if (not (and (time-of-long-lat moon-class) 
                (= jd (time-of-long-lat moon-class))))
    
    (ecliptic-coordinates moon-class jd))
  
  (let* ((rho-prime
          (/ (- 1 (* (ecc moon-class) (ecc moon-class)))
             (+ 1
                (* (ecc moon-class)
                   (cos
                    (+ (last-corrected-anomaly moon-class) 
                       (last-center-correction moon-class)))))))
         (rho (* rho-prime (semi-major-axis moon-class))))
    (when print-steps
      (format t "~%1.  corr anom ~a deg" 
              (degrees (last-corrected-anomaly moon-class)))
      (format t "~%    cent corr ~a deg" 
              (degrees (last-center-correction moon-class)))
      (format t "~%2.  rho prime ~a" rho-prime))
    rho))


(defmethod APPARENT-DIAMETER ((moon-class moon-class) jd)
  "apparent diameter of moon, radians"
  (let* ((rho (distance moon-class jd))
	 (diameter (/ (angular-size moon-class) (/ rho (semi-major-axis moon-class)))))
    diameter)) 


(defmethod MOON-PHASE ((moon-class moon-class) jd)
  
  "compute moon phase (0=new moon, 1=full-moon)"
  
  ;; get current values for true longitude
  (if (not (and (time-of-long-lat moon-class) (= jd (time-of-long-lat moon-class))))
    (ecliptic-coordinates moon-class jd))
  (let* ((sun-longitude (approx-ecliptic-longitude *sun* jd)))
    (* 0.5 (- 1 (cos (- (last-true-longitude moon-class) sun-longitude))))))


(defmethod TEST-CASE ((moon-class moon-class))
  
  "test case from Duffett-Smith, Practical Astronomy with Your Calculator,
    1st edition, p. 141-142"
  
  (let* ((jd (utjd (+ 26 (/ 16.0 24.0) (/ 50 86400)) 2 1979))	; date of test case
         (lambda-beta
          (mapcar #'degrees (multiple-value-list (ecliptic-coordinates moon-class jd t))))
         (ra-dec
          (mapcar #'degrees (3vect-cartesian-to-spherical (position-vector moon-class jd))))
         (hms (degrees-of-ra-to-hms (first ra-dec)))
         (dms (degrees-of-dec-to-dms (second ra-dec)))
         (phase (moon-phase moon-class jd)))
    (format t "~%MOON test case, date 26-Feb-1979 16:00:50 (JD ~a)" jd)
    (format t "~%  longitude: ~ad (cf. 337.011006d)" (first lambda-beta))
    (format t "~%  latitude : ~ad (cf. 0.991900d)" (second lambda-beta))
    (format t
            "~%  RA,Dec   : ~dh ~dm ~,2fs, ~dd ~d' ~,2f'' ~
             (cf. 22h 33m 27s, 8d 01m 01s)"
            (first hms) (second hms) (third hms) (first dms) (second dms) (third dms))
    (format t "~%  phase ~a" phase)
    nil)
  
  ;; test case for distance, angular size:  p. 149
  (let* ((jd (utjd 6 9 1979))
         (dist (distance moon-class jd t))
         (diam (apparent-diameter moon-class jd)))
    (format t "~%  on date jd ~a ~a" jd (jdut jd))
    (format t "~%  distance ~a (cf. 0.945101) as fraction of a"
            (/ dist (semi-major-axis moon-class)))
    (format t "~%  diameter ~ad (cf. 0.548d)" (degrees diam)))
  nil)


;;; ======================================================================
;;;                                                         MOON EXCLUSION
;;;
;;; moon exclusion:  compute the times that a target is within a specified
;;; angle of the moon
;;; KNOWN INEFFICIENCY:  should check that target is too far off the ecliptic
;;; to ever be within specified angle of the moon (won't cause error,
;;; just does extra work)
;;; 

(defvar *MOON-LONGITUDE-TABLE* nil) 

(defstruct MOON-LONGITUDE-TABLE
  (initialized nil)
  start-date
  end-date
  number-of-dates
  date-array
  longitude-array)

;; Coerced start/end to double-float - JLS 3/89

(defun INITIALIZE-MOON-LONGITUDE-TABLE (start end &key (interval 2))
  "Populate a moon-longitude-table and set *moon-longitude-table* to point
    to it.  Initialize the fields of the table are initialized.
    interval (days) must be small compared to period (i.e. year)"
  (setf start (coerce start 'double-float)
        end (coerce end 'double-float))
  
  ;; create a table struct if it doesn't already exist
  (if (not *moon-longitude-table*)
    (setf *moon-longitude-table* (make-moon-longitude-table)))
  
  ;; make the struct and populate scalar fields
  (let ((n (1+ (ceiling (- end start) interval))))
    
    ;;(format t  ~%Number of dates: ~d  N)
    (setf (moon-longitude-table-start-date *moon-longitude-table*) start
          (moon-longitude-table-end-date *moon-longitude-table*) end
          (moon-longitude-table-number-of-dates *moon-longitude-table*) n
          (moon-longitude-table-date-array *moon-longitude-table*)
          (make-array n :element-type 'double-float)
          (moon-longitude-table-longitude-array *moon-longitude-table*)
          (make-array n :element-type 'double-float))
    
    ;; fill the arrays
    (dotimes (i n)
      (let* ((date (+ start (* i interval)))
             (longitude (ecliptic-longitude *moon* date)))
        (setf (aref (moon-longitude-table-date-array 
                     *moon-longitude-table*) i) date
              (aref (moon-longitude-table-longitude-array 
                     *moon-longitude-table*) i) longitude)
        ;;(format t  ~%~a ~a  date longitude)
        )))
  (setf (moon-longitude-table-initialized *moon-longitude-table*) t)
  *moon-longitude-table*)


(defun ADJUST-MOON-LONGITUDE-TABLE (start end &key (interval 2))
  (if (not *moon-longitude-table*)
    (initialize-moon-longitude-table start end :interval interval)
    (if (or (< start (moon-longitude-table-start-date *moon-longitude-table*))
            (> end (moon-longitude-table-end-date *moon-longitude-table*)))
      (initialize-moon-longitude-table
       (min start (moon-longitude-table-start-date *moon-longitude-table*))
       (max end (moon-longitude-table-end-date 
                 *moon-longitude-table*)) :interval interval)))
  *moon-longitude-table*)


(defun DATES-OF-SAME-TARGET-AND-MOON-LONGITUDE (target-longitude start end)
  (adjust-moon-longitude-table start end)
  (let ((result (quote nil))
        (target-longitude-plus-2pi (+ 2pi target-longitude)))
    (dotimes (i (- (moon-longitude-table-number-of-dates 
                    *moon-longitude-table*) 1))
      (let ((d1 (aref (moon-longitude-table-date-array 
                       *moon-longitude-table*) i))
            (d2 (aref (moon-longitude-table-date-array 
                       *moon-longitude-table*) (1+ i)))
            (l1 (aref (moon-longitude-table-longitude-array 
                       *moon-longitude-table*) i))
            (l2 (aref (moon-longitude-table-longitude-array 
                       *moon-longitude-table*) (1+ i))))
        (unless (or (< d2 start) (> d1 end))
          (if (< l2 l1)
            (incf l2 2pi))
          
          ;;(format t   i=~d  i)
          (cond
           ((and (>= target-longitude l1) (< target-longitude l2))
            (let* ((slope (/ (- l2 l1) (- d2 d1)))
                   (est (+ d1 (/ (- target-longitude l1) slope)))
                   (check (ecliptic-longitude *moon* est)))
              (declare (ignore check))
              (push est result)
              
              ;;(format t  ~% match at date ~a (moon long=~a) ~a ~a  est check d1 d2)
              ))
           ((and (>= target-longitude-plus-2pi l1) (< target-longitude-plus-2pi l2))
            (let* ((slope (/ (- l2 l1) (- d2 d1)))
                   (est (+ d1 (/ (- target-longitude-plus-2pi l1) slope)))
                   (check (ecliptic-longitude *moon* est)))
              (declare (ignore check))
              (push est result)
              ;;(format t  ~% match at date ~a (moon long=~a) ~a ~a  est check d1 d2)
              ))
           (t nil)))))
    
    (nreverse result)))


(defun TARGET-MOON-EXCLUSION (target-position-vector start end exclusion-angle)
  
  "Return list of times (entry1 exit1 entry2 exit2 ...) for entering into moon
    exclusion region.  Dates are extended by a month in both directions so 
    the result will include any exclusions overlapping with specified 
    start-end range.  Max exclusion angle is 90deg"
  
  (let* ((target-ecliptic-3vector 
          (celestial-to-ecliptic target-position-vector))
         (target-ecliptic-long-lat 
          (3vect-cartesian-to-spherical target-ecliptic-3vector))
         (target-ecliptic-longitude (first target-ecliptic-long-lat))
         ;; (target-ecliptic-latitude (second target-ecliptic-long-lat))
         (date-list
          (dates-of-same-target-and-moon-longitude 
           target-ecliptic-longitude (- start 30) (+ end 30)))
         (cos-exclusion-angle (cos exclusion-angle))
         start-exclusion
         end-exclusion
         (result (quote nil)))
    (dolist (d date-list)
      (setf start-exclusion
            (bisection-root (- d 7) d cos-exclusion-angle
                            #'(lambda (x)
                                (3vect-dot-product target-position-vector
                                 (position-vector *moon* x))))
            end-exclusion
            (bisection-root d (+ d 7) cos-exclusion-angle
                            #'(lambda (x)
                                (3vect-dot-product target-position-vector
                                 (position-vector *moon* x)))))
      (when (and start-exclusion end-exclusion)
        (push start-exclusion result)
        (push end-exclusion result))
      )
    (nreverse result)))

#|  Test case:
(dmy-to-time '1-jan-88)
(dmy-to-time '1-jan-90)

(initialize-moon-longitude-table 3161 3892 :interval 2)
(adjust-moon-longitude-table 3500 4500 :interval 2)
(dates-of-same-target-and-moon-longitude 0 3161 3892)

(time (target-moon-exclusion '(1 0 0) 3161 3892 (radians 15)))

|#

;;; ======================================================================
;;;                                                      CLASS:  SATELLITE


(defclass SATELLITE (astronomical-object)
  (   
   ;; sensors
   (sensor-list
    :initform nil
    :reader sensor-list
    :documentation
    "list of sensor instances for this satellite")
   (named-vector-list
    :initform nil :reader named-vector-list :documentation
    "List of (cons name vector) specifying body-centered planes or vectors
      in the body frame of the satellite")
   ))


;;; ======================================================================
;;;                                                         CLASS: SENSORS


(defclass SENSOR (astronomical-object)
  ((attached-to 
    :initform nil :initarg :attached-to :reader attached-to
    :documentation 
    "The satellite instance this sensor is attached to")
   (boresight-vector
    :initform nil :initarg :boresight-vector :reader boresight-vector
    :documentation
    "Boresight vector of sensor in satellite body frame")
   (fov-type
    :initform :CONE :initarg :fov-type :reader fov-type
    :documentation
    "Type of FOV:  may be :CONE or :RECT")
   (fov-cone-half-angle
    :initform nil :initarg :fov-cone-half-angle :reader fov-cone-half-angle
    :documentation
    "Angle from boresight within which sensor operates.  This is defined even
      if fov type is not :CONE, in which case this angle should be larger
      than the angle from any point in the fov to the boresight.")
   (cos-fov-cone-half-angle
    :initform nil :initarg :cos-fov-cone-half-angle :reader cos-fov-cone-half-angle
    :documentation
    "Cos fov-cone-half-angle")

   ;; slots only used for :RECT FOV
   (fov1-vector
    :initform nil :initarg :fov1-vector :reader fov1-vector
    :documentation
    "One of two vectors determining rectangular FOV.  Perpendicular to 
      boresight vector, given in satellite body frame.")
   (fov1-half-angle
    :initform nil :initarg :fov1-half-angle :reader fov1-half-angle
    :documentation
    "Angular size of FOV in boresight-vector fov1-vector plane, radians.
      Points in FOV are between pi/2 - fov1-half-angle and pi/2 + fov1-half-angle
      from fov1-vector")
   (cos-fov1-plus
    :documentation "cos pi/2 + fov1-half-angle")
   (cos-fov1-minus
    :documentation "cos pi/2 - fov1-half-angle")

   (fov2-vector
    :initform nil :initarg :fov2-vector :reader fov2-vector
    :documentation
    "One of two vectors determining rectangular FOV.  Perpendicular to 
      boresight vector, given in satellite body frame.")
   (fov2-half-angle
    :initform nil :initarg :fov2-half-angle :reader fov2-half-angle
    :documentation
    "Angular size of FOV in boresight-vector fov2-vector plane, radians.
      Points in FOV are between pi/2 - fov2-half-angle and pi/2 + fov2-half-angle
      from fov2-vector")
   (cos-fov2-plus
    :documentation "cos pi/2 + fov2-half-angle")
   (cos-fov2-minus
    :documentation "cos pi/2 - fov2-half-angle")

   ))
         

;;; --- constructor
       
(defun MAKE-SENSOR (&key name satellite boresight (fov-type :cone)
                         fov-cone-half-angle 
                         fov1-vector fov1-half-angle
                         fov2-vector fov2-half-angle 
                         (units :radians))

  "Make a sensor instance and attach it to the specified satellite.
    Return sensor instance. Arguments are:
    name - sensor name, keyword
    satellite - satellite instance to which sensor attached
    boresight - 3vector, satellite body frame
    fov-type - :CONE or :RECT
    fov-cone-half-angle - max size of FOV, half-angle.  For :CONE
     this is the actual FOV, for :RECT it is the bounding angle
    units - degrees or radians, default radians. Refers to the angles
     fov-cone-half-angle, fov1-half-angle, fov2-half-angle
    -- only for fov-type :RECT
    fov1-vector - vector in satellite body frame perp to boresight
    fov1-half-angle - half angle of FOV in fov1-vector boresight plane
    fov2-vector - vector in satellite body frame perp to boresight
    fov2-half-angle - half angle of FOV in fov2-vector boresight plane"

  ;; error check args: this is dumb now
  (unless (and name satellite boresight
               (and (listp boresight) (= (length boresight) 3))
               fov-type
               (or (eq fov-type :CONE) (eq fov-type :RECT))
               fov-cone-half-angle
               (numberp fov-cone-half-angle)
               (typep satellite 'satellite))
    (error "error in arguments to MAKE-SENSOR"))
  ;; for :RECT need other vectors, angles
  (when (eq fov-type :RECT)
    (unless (and fov1-vector fov2-vector fov1-half-angle fov2-half-angle)
      (error "MAKE-SENSOR :RECT needs fov and half angles")))

  (let* ((normalized-boresight-vector (3vect-normalize boresight))
         (cone-angle (if (eq units :radians) fov-cone-half-angle
                         (radians fov-cone-half-angle)))
         (sensor-instance
          (make-instance 'sensor
                         :name name
                         :attached-to satellite
                         :boresight-vector normalized-boresight-vector
                         :fov-type fov-type
                         :fov-cone-half-angle cone-angle)))
    (setf (slot-value sensor-instance 'cos-fov-cone-half-angle)
          (cos cone-angle))
    ;; if it's a rectangular FOV, record other vectors
    (when (eq fov-type :RECT)
      (let ((fov1 (3vect-normalize fov1-vector))
            (fov2 (3vect-normalize fov2-vector))
            (angle1 (if (eq units :radians) fov1-half-angle
                        (radians fov1-half-angle)))
            (angle2 (if (eq units :radians) fov2-half-angle
                        (radians fov2-half-angle))))
        (setf (slot-value sensor-instance 'fov1-vector) fov1)
        (setf (slot-value sensor-instance 'fov1-half-angle) angle1)
        (setf (slot-value sensor-instance 'cos-fov1-plus) (cos (+ pi/2 angle1)))
        (setf (slot-value sensor-instance 'cos-fov1-minus) (cos (- pi/2 angle1)))
        (setf (slot-value sensor-instance 'fov2-vector) fov2)
        (setf (slot-value sensor-instance 'fov2-half-angle) angle2)
        (setf (slot-value sensor-instance 'cos-fov2-plus) (cos (+ pi/2 angle2)))
        (setf (slot-value sensor-instance 'cos-fov2-minus) (cos (- pi/2 angle2)))
        ))
    (add-sensor satellite sensor-instance)
    sensor-instance))


(defmethod SHOW ((s sensor))
  "Print description to sensor to standard output"
  (format t "~% Sensor: ~a attached to ~a" (name s) (name (attached-to s)))
  (format t "~%   boresight angles to V1 ~,2f deg, V2 ~,2f deg, V3 ~,2f deg"
          (degrees (acos (first (boresight-vector s))))
          (degrees (acos (second (boresight-vector s))))
          (degrees (acos (third (boresight-vector s)))))
  (format t "~%   fov-type: ~a  FOV cone half-angle: ~,4f rad (~,2f deg)"
          (fov-type s)
          (fov-cone-half-angle s)
          (degrees (fov-cone-half-angle s)))
  (when (eq (fov-type s) :RECT)
    (format t "~%   FOV1 half-angle ~,2fdeg, angles to V1 ~,2f deg, V2 ~,2f deg, V3 ~,2f deg"
            (degrees (fov1-half-angle s))
            (degrees (acos (first (fov1-vector s))))
            (degrees (acos (second (fov1-vector s))))
            (degrees (acos (third (fov1-vector s)))))
    (format t "~%   FOV2 half-angle ~,2fdeg, angles to V1 ~,2f deg, V2 ~,2f deg, V3 ~,2f deg"
            (degrees (fov2-half-angle s))
            (degrees (acos (first (fov2-vector s))))
            (degrees (acos (second (fov2-vector s))))
            (degrees (acos (third (fov2-vector s)))))
    ))


(defmethod PRINT-OBJECT ((s sensor) stream)
  (format stream "#<~a ~a ~a>" 
          (name (attached-to s)) "SENSOR" (name s)))

#|
(find-sensor *EUVE* :fhst1)
(show (find-sensor *EUVE* :fhst1))
(find-sensor *ASTROD* :STTA)
(show (find-sensor *ASTROD* :STTA))
(show (find-sensor *ASTROD* :STTB))
|#
;;; --- sensor methods

(defmethod ADD-SENSOR ((sat satellite) (s sensor))
  "Add sensor to satellite"
  (setf (slot-value sat 'sensor-list)
        (cons s (sensor-list sat))))

(defmethod DELETE-SENSOR ((sat satellite) (s sensor))
  "Delete sensor from satellite"
  (setf (slot-value sat 'sensor-list)
        (delete s (sensor-list sat))))

(defmethod FIND-SENSOR ((sat satellite) (sensor-name t))
  "Return sensor object with specified name, or nil if not found"
  (find sensor-name (sensor-list sat) :key #'name :test #'equal))

;;; --- standard astronomical object methods ---
;;; A sensor differs from conventional astronomical object types in that
;;; its position is the direction it is LOOKING, not where the physical
;;; sensor is.  The distance is at infinity.

(defmethod POSITION-VECTOR ((s sensor) jd)
  "Unit 3-vector from earth center to astronomical object"
  (sensor-view-vector s jd))

(defmethod DISTANCE ((s sensor) jd &optional (print-steps nil))
  "Distance in km of astronomical-object from earth center"
  (declare (ignore jd print-steps))
  ;; nil means infinite distance
  nil)

(defmethod VELOCITY-VECTOR ((s sensor) jd)
  "Unit geocentric velocity vector of astronomical object"
  (declare (ignore jd))
  (error "Not specified for sensors"))

(defmethod VELOCITY ((s sensor) jd)
  "Speed of object in km/sec"
  (declare (ignore jd))
  (error "Not specified for sensors"))


;;; ======================================================================
;;;                                                          NAMED VECTORS

(defmethod ADD-NAMED-VECTOR ((s satellite) name vector)
  "Add a named vector to the satellite's list."
  (setf (slot-value s 'named-vector-list)
        (cons (cons name (3vect-normalize vector))
              (named-vector-list s))))

;;; no deletion method defined, so don't make mistakes

(defmethod NAMED-VECTOR-NAMES ((s satellite))
  "Return names of all named vectors as a list"
  (mapcar #'first (named-vector-list s)))



;;; ======================================================================
;;;                                      CLASS:  LOW-EARTH-ORBIT SATELLITE
;;;
;;; for ST etc., circular uniformly regressing orbit assumed
;;;


(defclass LOW-EARTH-ORBIT-SATELLITE (satellite)
  ((inclination
       :initform  nil
       :initarg  :inclination
       :accessor  inclination)			; radians
   (time-of-asc-node
       :initform  nil
       :initarg  :time-of-asc-node
       :accessor  time-of-asc-node)		; JD
   (ra-of-asc-node
       :initform  nil
       :initarg  :ra-of-asc-node
       :accessor  ra-of-asc-node)		; radians
   (nodal-regression-rate
       :initform  nil
       :initarg  :nodal-regression-rate
       :accessor  nodal-regression-rate)	; radians/day
   (nodal-period
       :initform  nil
       :initarg  :nodal-period
       :accessor  nodal-period)			; days
   (semimajor-axis
       :initform  nil
       :initarg  :semimajor-axis
       :accessor  semimajor-axis)		; km
   ;; derived
   (object-speed
       :initform  nil
       :initarg  :object-speed
       :accessor  object-speed)			;km/s
   ;; to keep track of state
   (time-of-orientation
       :initform  nil
       :initarg  :time-of-orientation
       :accessor  time-of-orientation)		; last time of orbital orientation matrix
   (orbit-orientation-matrix
       :initform  nil
       :initarg  :orbit-orientation-matrix
       :accessor  orbit-orientation-matrix)	; orbit matrix at time-of-orientation
   ;; the columns of orbit-orientation matrix are:
   ;;  m = ascending node
   ;;  n = pi/2 from asc. node in direction of travel
   ;;  p = orbital pole (= ang. momemtum vector)
   ;;  RHS:  m x n = p  (orbit is in m-n plane)
   (fix-orbit-orientation
    :initform nil :documentation
    "Flag:  if t, orbit orientation is not recomputed even if time changes.
     If nil, orbit orientation is recomputed only if time changes.  The purpose
     is efficiency when working over intervals for which the orbit plane can be 
     safely assumed constant.  Users should NOT set this flag:  use the
     macro WITH-FIXED-ORBIT-ORIENTATION which includes an unwind-protect")

   (time-of-position
       :initform  nil
       :initarg  :time-of-position
       :accessor  time-of-position)		; last time position/velocity asked for
   (last-position-vector
       :initform  nil
       :initarg  :last-position-vector
       :accessor  last-position-vector)		; unit vector
   (last-velocity-vector
       :initform  nil
       :initarg  :last-velocity-vector
       :accessor  last-velocity-vector)		; unit vector

   ;; value in days, a small fraction of an orbit, used for iteration guesses
   (epsilon
       :initform  1.0e-4
       :initarg  :epsilon
       :accessor  epsilon)

   ;; for computing roll
   (roll-spec
    :initform nil :initarg :roll-spec :reader roll-spec
    :documentation
    "How the satellite should compute it's orientation when asked.  Legal
     entries are: a list of the form (<target> type key value ...)
     where <target> is an astronomical object instance, type is a keyword
     specifying one of :nominal, :offset-from-nominal, :fixed.  Legal
     forms are:
      (<target> :nominal)
      (<target> :offset-from-nominal :roll-angle-offset angle)
      (<target> :fixed :roll-angle angle)
     Where angles are in radians.")
   (last-roll-time
    :initform nil :documentation
    "last time roll was computed:  corresponding orientation matrix is stored
      in slot last-orientation-matrix.  If nil roll is unknown and must be
      recomputed.")
   (last-orientation-matrix
    :initform nil :documentation
    "Last orientation matrix calculated")
   (last-nominal-orientation-matrix 
    :initform nil :reader last-nominal-orientation-matrix
    :documentation
    "Nominal orientation matrix at the last time orientation was calculated")
   (fix-orientation
    :initform nil :documentation
    "Flag:  if t, orientation is not recomputed even if time changes.
     If nil, orientation is recomputed only if time changes.  The purpose
     is efficiency when working over intervals for which roll can be 
     safely assumed constant.  Users should NOT set this flag:  use the
     macro WITH-FIXED-ORIENTATION which includes an unwind-protect")
   (roll-limit-type
    :initform :V1V3 :initarg :roll-limit-type :reader roll-limit-type
    :documentation
    "How the offset from nominal roll is limited:  value can be :V1V3, for
      limit on sun angle from V1-V3 plane (appropriate for rotatable panels
      about V2), or :V3 for limit on sun angle from V3 (appropriate for
      fixed panels with normal vector V3")
   (roll-limit-angle 
    :initform (radians 30) :initarg :roll-limit-angle :reader roll-limit-angle
    :documentation
    "Limit on sun angle from V1-V3 plane or from V3 axis, depending on
      value of roll-limit-type.  Radians")

   ;; Table storing BETA angle over planning interval
   (beta-table-start
       :initform  nil
       :initarg  :beta-table-start
       :accessor  beta-table-start)
   (beta-table-end
       :initform  nil
       :initarg  :beta-table-end
       :accessor  beta-table-end)
   (beta-table-interval
       :initform  1
       :initarg  :beta-table-interval
       :accessor  beta-table-interval)		; In days
   (beta-table
       :initform  nil
       :initarg  :beta-table
       :accessor  beta-table)			; Angle between sun and satellite orbital plane
   ))


;;; --- print method

(defmethod PRINT-OBJECT ((LEOS low-earth-orbit-satellite) stream)

  "object print method"

  (format stream "<LOW-EARTH-ORBIT-SATELLITE ~A>" (name LEOS)))


(defmethod SHOW ((LEOS low-earth-orbit-satellite))
  (format t "~%Low earth orbit satellite: ~a" (name LEOS))
  (format t "~%  Inclination: ~a rad" (inclination LEOS))	; radians
  (format t ", ~,3f deg" (degrees (inclination LEOS)))
  (format t "~%  T asc. node: JD ~a" (time-of-asc-node LEOS))  ; JD
  (format t ", ~a" (format-abs-time (time-of-asc-node LEOS)))
  (format t "~%  RA asc. node: ~a rad" (ra-of-asc-node LEOS))  ; radians
  (format t ", ~,3f deg" (degrees (ra-of-asc-node LEOS)))
  (format t "~%  Nodal regression rate: ~a rad/day"
          (nodal-regression-rate LEOS))  ; radians/day
  (format t ", ~,3f deg/day" (degrees (nodal-regression-rate LEOS)))
  (format t "~%  Period: ~a d" (nodal-period LEOS))	; days
  (format t ", ~,2f min" (* 1440.0 (nodal-period LEOS)))
  (format t "~%  Semimajor axis: ~a km" (semimajor-axis LEOS))	; km
  (format t "~%  Roll spec:  ~a" (roll-spec LEOS))
  (format t "~%  Roll limit type ~a, angle ~,3f deg"
          (roll-limit-type LEOS) (degrees (roll-limit-angle LEOS)))

  ;; derived
  (format t "~%  Speed: ~a km/s" (object-speed LEOS))  ;km/s
  
  ;; to keep track of state
  (format t "~%  Time of last orbit orientation: JD ~a"
          (time-of-orientation LEOS))
  (format t "~%  Orbital orientation: ~a" (orbit-orientation-matrix LEOS))
  ; last time position/velocity asked for
  (format t "~%  Time of last pos: JD ~a" (time-of-position LEOS))
  (format t "~%  Last pos: ~a~%  RA, Dec.: ~a" (last-position-vector LEOS)
          (if (last-position-vector LEOS)
            (mapcar #'degrees
                    (3vect-cartesian-to-spherical
                     (last-position-vector LEOS)))))
  (format t "~%  Last vel: ~a" (last-velocity-vector LEOS))
  (when (named-vector-names LEOS)
    (format t "~%  Named vectors: ~a" (named-vector-names LEOS)))
  (when (sensor-list LEOS)
    (dolist (sensor (sensor-list LEOS))
      (show sensor))))


;;; --- constructor

(defun MAKE-LOW-EARTH-ORBIT-SATELLITE (n i tnode ranode regrate period axis
                                         &key 
                                         (roll-limit-type :V1V3)
                                         (roll-limit-angle (radians 30))
                                         (type 'low-earth-orbit-satellite))
  "required arguments and units:
     n name string
     i inclination degrees
     tnode time of ascending node crossing, JD
     ranode RA of ascending node at crossing, degrees
     regrate nodal regression rate, deg/day (- = retrograde)
     period nodal period, minutes
     axis semimajor axis, km"
  (let ((instance (make-instance type)))
    (setf (name instance) n)
    (setf (inclination instance) (coerce (radians i) 'double-float))
    (setf (time-of-asc-node instance) (coerce tnode 'double-float))
    (setf (ra-of-asc-node instance) (coerce (radians ranode) 'double-float))
    (setf (nodal-regression-rate instance) (coerce (radians regrate) 'double-float))
    (setf (nodal-period instance) (coerce (/ period 1440) 'double-float))
    (setf (semimajor-axis instance) (coerce axis 'double-float))
    (setf (object-speed instance) (coerce (/ (* 2pi axis) (* period 60)) 'double-float))
    (setf (slot-value instance 'roll-limit-type) roll-limit-type)
    (setf (slot-value instance 'roll-limit-angle) (coerce roll-limit-angle 'double-float))
    (update-satellite-table n instance)
    instance))


(defmethod UPDATE-ORBIT-PARAMETERS 
  ((LEOS low-earth-orbit-satellite)
   &key
   inclination
   time-asc-node
   RA-asc-node
   regression-rate
   period
   semimajor-axis)

  "Update orbit model parameters.  Keyword arguments are:
     inclination  - degrees
     time-asc-node -  time of ascending node crossing, JD or date
     RA-asc-node -  RA of ascending node at crossing, degrees
     regression-rate -  nodal regression rate, deg/day (- = retrograde)
     period  - nodal period, minutes
     semimajor-axis - km
    All are required."

  (cond
   ((and inclination time-asc-node RA-asc-node regression-rate
         period semimajor-axis)
    (setf (inclination LEOS) (coerce (radians inclination) 'double-float))
    (setf (time-of-asc-node LEOS) (coerce (dmy-to-time time-asc-node) 'double-float))
    (setf (ra-of-asc-node LEOS) (coerce (radians RA-asc-node) 'double-float))
    (setf (nodal-regression-rate LEOS) (coerce (radians regression-rate) 'double-float))
    (setf (nodal-period LEOS) (coerce (/ period 1440) 'double-float))
    (setf (semimajor-axis LEOS) (coerce semimajor-axis 'double-float))
    (setf (object-speed LEOS) (coerce (/ (* 2pi semimajor-axis) (* period 60)) 'double-float))
    t)
   (t ;; some args missing
    (error "All keyword arguments are required"))))


;;; --- orientation and roll

(defmethod SET-SATELLITE-ROLL-TYPE ((LEOS low-earth-orbit-satellite)
                                    (target astronomical-object)
                                    roll-type 
                                    &key
                                    roll-offset-angle
                                    roll-angle)

  "Set the roll calculation method for the satellite.  
    Type values are:
     nil - no default type (signal error if roll calculation requested) 
     :nominal - use nominal roll for date
     :offset-from-nominal - offset from nominal for date by required
       keyword arg roll-offset-angle
     :fixed - fix roll as specified by required keyword arg roll-angle"

  (let ((spec nil))
    ;; legality check
    (cond 
     
     ((null roll-type)) ; ok, no roll spec
     
     ((eq :NOMINAL roll-type)
      (setf spec (list target roll-type)))

     ((and (eq :OFFSET-FROM-NOMINAL roll-type)
           (numberp roll-offset-angle)
           (null roll-angle))
      (setf spec (list target roll-type :roll-offset-angle roll-offset-angle)))
     
     ((and (eq :FIXED roll-type)
           (numberp roll-angle)
           (null roll-offset-angle))
      (setf spec (list target roll-type :roll-angle roll-angle)))

     (t (error "unrecognized roll spec ~a ~a ~a"
               roll-type roll-offset-angle roll-angle)))
    (setf (slot-value LEOS 'roll-spec) spec)
    (setf (slot-value LEOS 'last-roll-time) nil)
    spec))


(defmethod GET-SATELLITE-ROLL-TYPE ((LEOS low-earth-orbit-satellite))

  "Return roll-spec in a form suitable for printing, i.e. convert object
    references to sensible printable values"

  (let ((spec (roll-spec LEOS)))
    (cond
     ;; nil spec just returns nil
     ((null spec) nil)
     
     ;; if the target is fixed, replace by ra and dec (radians)
     ((typep (first spec) 'fixed-astronomical-object)
      (cons (list (ra (first spec)) (dec (first spec))) (rest spec)))
     
     ;; otherwise use name
     (t (cons (name (first spec)) (rest spec))))))


(defmethod SATELLITE-ORIENTATION-MATRIX ((LEOS low-earth-orbit-satellite) jd)

  "Return orientation matrix for satellite at time jd.  Roll method must have
    been set by SET-SATELLITE-ROLL-TYPE.  Error if no method known.
    Also records slot last-nominal-orientation-matrix for nominal orientation.
    Orientation matrix returned has the following form:
     ((V1x V1y V1z) (V2x V2y V2z) (V3x V3y V3z))"
  
  ;; unknown 
  (if (null (roll-spec LEOS)) (error "can't compute roll"))
  
  (let ((prev-time (slot-value LEOS 'last-roll-time))
	(fix-orientation (slot-value LEOS 'fix-orientation)))

    ;; same roll-spec and time as last call?
    (if (and prev-time 
	     (or fix-orientation
		 (= prev-time jd)))

      ;; return prev matrix
      (slot-value LEOS 'last-orientation-matrix)

      ;; otherwise compute
      (let* ((spec (roll-spec LEOS))
             (target (first spec))
             (type (second spec))
             (keywords (cddr spec))
             (orient-matrix nil)
             (nominal-orient-matrix nil))

        ;; calculate nominal roll
        (let* ((v1 (line-of-sight LEOS target jd))
               (sun-vector (position-vector *SUN* jd))
               (v3 (3vect-normalize
                    (3vect-cross-product v1
                     (3vect-cross-product sun-vector v1))))
               (v2 (3vect-cross-product v3 v1))
               )
          ;; check sun on +v3
          (if (< (3vect-dot-product v3 sun-vector) 0)
            (error "sun on -V3"))
          (setf nominal-orient-matrix (list v1 v2 v3))

        (cond 

         ((eq type :nominal)
            ;; returned value
            (setf orient-matrix (list v1 v2 v3)))

         ((eq type :offset-from-nominal)
          (let* ((offset (getf keywords :roll-offset-angle))
                 (rotmat (3x3rotation-matrix v1 offset))
                 (v2-rot (3x3matrix-times-vector rotmat v2))
                 (v3-rot (3x3matrix-times-vector rotmat v3)))
            (setf orient-matrix (list v1 v2-rot v3-rot))))

         ((eq type :fixed)
          (let* ((roll (getf keywords :roll-angle))
                 (triad (3vect-to-triad v1))
                 ;; elements of triad are reference v1, v2, v3
                 (rotmat (3x3rotation-matrix v1 roll))
                 (v2-rot (3x3matrix-times-vector rotmat (second triad)))
                 (v3-rot (3x3matrix-times-vector rotmat (third triad))))
            (setf orient-matrix (list v1 v2-rot v3-rot))))

         ;; OTHERS GO HERE
         
         (t (error "don't know how to compute type ~a" type))))
        
        ;; save
        (setf (slot-value LEOS 'last-roll-time) jd
              (slot-value LEOS 'last-orientation-matrix) orient-matrix
              (slot-value LEOS 'last-nominal-orientation-matrix)
              nominal-orient-matrix)
        orient-matrix))))


(defmethod SATELLITE-ROLL-LIMIT ((LEOS low-earth-orbit-satellite) jd)

  "Return satellite roll limit half angle, i.e. the angle about the V1
    axis that the satellite can roll without violating constraint.
    Radians.  Uses slot ROLL-LIMIT-TYPE to determine whether angle is
    from V1-V3 plane (like HST) or from V3 (like fixed-panel satellites).
    Uses slot ROLL-LIMIT-ANGLE for maximum angular distance between
    V1-V3 plane or V3 axis, as appropriate.  Return NIL if there is no
    orientation specified, or if the roll-limit type is unknown, or
    there is no roll-limit angle."
  
  ;; ensure that nomimal orientation matrix has been calculated
  (satellite-orientation-matrix LEOS jd)

  ;; compute roll limit
  (let* ((nominal-orient-matrix (slot-value LEOS 'last-nominal-orientation-matrix))
         (nominal-V3 (third nominal-orient-matrix))
         (roll-limit-type (slot-value LEOS 'roll-limit-type))
         (roll-limit-angle (slot-value LEOS 'roll-limit-angle))
         (sun (position-vector *SUN* jd)))
    
    (cond ((or (null nominal-V3)
               (null roll-limit-angle))
           ;; can't calculate
           nil)
    
          ((eq roll-limit-type :V1V3)
           (let ((sin-roll (/ (sin roll-limit-angle)
                              (3vect-dot-product nominal-v3 sun))))
             (if (< sin-roll 1) (asin sin-roll) nil)))

          ((eq roll-limit-type :V3)
           (let ((cos-roll (/ (cos roll-limit-angle)
                              (3vect-dot-product nominal-v3 sun))))
             (if (< cos-roll 1) (acos cos-roll) nil)))

          (t nil))))


(defmethod SATELLITE-ROLL-FROM-NOMINAL ((LEOS low-earth-orbit-satellite) jd)
  "Return angle in radians of roll from nominal.  This is a right-handed
    rotation angle about the V1 axis which takes nominal to actual
    orientation.  Return nil if not defined"

  (let* ((orient-matrix (satellite-orientation-matrix LEOS jd))
         (nominal-orient-matrix (slot-value LEOS 'last-nominal-orientation-matrix)))
    (cond ((and orient-matrix nominal-orient-matrix)
           (let ((nom-v2 (second nominal-orient-matrix))
                 (nom-v3 (third nominal-orient-matrix))
                 (v2 (second orient-matrix)))
             (atan (3vect-dot-product v2 nom-v3)
                   (3vect-dot-product v2 nom-v2))))
          (t nil))))
    
    
#|
(position-vector *SUN* (dmy-to-time '21-mar-91))
(setf foo (make-fixed-astronomical-object "foo" :ra 0 :dec 90))
(show *astrod*)
(ra foo)
(dec foo)
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 80) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 70) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 61) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 60) :units :degrees))

(set-satellite-roll-type *ASTROD* foo :nominal)
(set-satellite-roll-type *ASTROD* foo :offset-from-nominal 
                         :roll-offset-angle (radians 50))
(set-satellite-roll-type *ASTROD* foo :offset-from-nominal 
                         :roll-offset-angle (radians -5))
(set-satellite-roll-type *ASTROD* foo :fixed 
                         :roll-angle (radians 180))
(satellite-orientation-matrix *astrod* (dmy-to-time '21-mar-91))
(slot-value *astrod* 'last-nominal-orientation-matrix)
(describe-satellite-orientation *astrod* (dmy-to-time '21-mar-91))
(degrees (satellite-roll-limit *astrod* (dmy-to-time '21-mar-91)))
(degrees (satellite-roll-from-nominal *astrod* (dmy-to-time '21-mar-91)))

(show *st*)
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 35) :units :degrees))
(set-satellite-roll-type *st* foo :nominal)
(set-satellite-roll-type *st* foo :offset-from-nominal 
                         :roll-offset-angle (radians 50))
(describe-satellite-orientation *st* (dmy-to-time '21-mar-91))
(degrees (satellite-roll-limit *st* (dmy-to-time '21-mar-91)))
(degrees (satellite-roll-from-nominal *st* (dmy-to-time '21-mar-91)))
|#

(defmacro WITH-FIXED-ORIENTATION (satellite &rest forms)

  "Execute forms with orientation fixed.  May be nested.  If the
    orientation is already fixed, then last-roll-time is used.
    If it is not, then last-roll-time is nulled.  This will cause
    the roll to be recomputed for the first time found in forms."

  (let ((sym (gensym)))
  `(let ((,sym (slot-value ,satellite 'fix-orientation)))
     (unwind-protect
	 (progn (setf (slot-value ,satellite 'fix-orientation) t)
		(if (not ,sym) 
		    (setf (slot-value ,satellite 'last-roll-time) nil))
		,@forms)
       (setf (slot-value ,satellite 'fix-orientation) ,sym)))))

#|
(set-satellite-roll-type 
 *st* (make-fixed-astronomical-object "foo" :ra 0 :dec 0) :nominal)
(describe-satellite-orientation *st* 4000.0)
(print (satellite-orientation-matrix *st* 4000.0))
(print (satellite-orientation-matrix *st* 4001.0))
(with-fixed-orientation
 *st*
 (print (satellite-orientation-matrix *st* 4000.0))
 (print (satellite-orientation-matrix *st* 4001.0))
 )
(with-fixed-orientation
 *st*
 (format t "~%fixed: ~a last-roll-time: ~a" (slot-value *st* 'fix-orientation)
	 (slot-value *st* 'last-roll-time))
 (print (satellite-orientation-matrix *st* 4100.0))
 (print (satellite-orientation-matrix *st* 4101.0))
 )
|#

(defmethod NAMED-VECTOR ((LEOS low-earth-orbit-satellite) name jd)

  "Compute satellite orientation and then named vector in celestial frame.
   Return nil if named vector not found."

  (let* ((orientation-matrix (satellite-orientation-matrix LEOS jd))
         (entry (assoc name (named-vector-list LEOS)))
         (body-vect (rest entry)))
    (if body-vect
      (3x3matrix-times-vector orientation-matrix body-vect))))


(defmethod SENSOR-VIEW-VECTOR ((sensor sensor) jd)

  "Return sensor boresight vector given satellite orientation
    matrix."

  (3x3matrix-times-vector
   (satellite-orientation-matrix (attached-to sensor) jd)
   (boresight-vector sensor)))


(defmethod SENSOR-CONE-FOV ((sensor sensor) jd)
  
  "Return multiple values describing sensor cone field of view.
    Returned values are:
     boresight - 3vect in direction of boresight
     cos-half-angle - cos half angle of FOV.
    For :CONE sensors, this is the actual FOV.  For :RECT sensors, 
    this should be the circumscribed cone for the FOV. 
    To test whether a position is potentially in the FOV, 
    need (dot-prod position boresight) > cos-half-angle"

  (values (sensor-view-vector sensor jd)
          (slot-value sensor 'cos-fov-cone-half-angle)))


(defmethod SENSOR-FOV1 ((sensor sensor) jd)
  
  "Return multiple values describing sensor rectangular field of view.
    Returned values are:
     fov - 3vect in direction of fov1, equatorial coord
     cos-plus - cos (pi/2 + fov-half-angle)
     cos-minus - cos (pi/2 - fov-half-angle)
    For :CONE sensors this returns nil for all three values.
    For :RECT sensors, this describes one axis of the FOV 
    To test whether a position is potentially in the FOV, 
    need   cos-minus >= (dot-prod position fov) >= cos-plus"

  (cond ((eq (fov-type sensor) :cone)
         (values nil nil nil))
        (t 
         (values (3x3matrix-times-vector
                  (satellite-orientation-matrix (attached-to sensor) jd)
                  (fov1-vector sensor))
                 (slot-value sensor 'cos-fov1-plus)
                 (slot-value sensor 'cos-fov1-minus)))))


(defmethod SENSOR-FOV2 ((sensor sensor) jd)
  
  "Return multiple values describing sensor rectangular field of view.
    Returned values are:
     fov - 3vect in direction of fov2, equatorial coord
     cos-plus - cos (pi/2 + fov-half-angle)
     cos-minus - cos (pi/2 - fov-half-angle)
    For :CONE sensors this returns nil for all three values.
    For :RECT sensors, this describes one axis of the FOV 
    To test whether a position is potentially in the FOV, 
    need   cos-minus >= (dot-prod position fov) >= cos-plus"

  (cond ((eq (fov-type sensor) :cone)
         (values nil nil nil))
        (t 
         (values (3x3matrix-times-vector
                  (satellite-orientation-matrix (attached-to sensor) jd)
                  (fov2-vector sensor))
                 (slot-value sensor 'cos-fov2-plus)
                 (slot-value sensor 'cos-fov2-minus)))))


(defmethod POSITION-IN-SENSOR-FOV-P ((sensor sensor) jd pos-vector)
  
  "Return non-nil if position 3vector is in sensor FOV, nil otherwise.
    Works for FOV of type :CONE and :RECT."
  
  ;; first check if in cone, then check rect
  (multiple-value-bind 
    (boresight cos-boresight-limit) (sensor-cone-fov sensor jd)
    (if (< (3vect-dot-product boresight pos-vector) cos-boresight-limit)
      (return-from position-in-sensor-fov-p nil)))
  
  (when (eq (fov-type sensor) :RECT)               ; only check if :RECT
    ;; now check FOV1
    (multiple-value-bind
      (fov1 cos1-plus cos1-minus) (sensor-fov1 sensor jd)
      (let ((dot (3vect-dot-product pos-vector fov1)))
        (if (or (< dot cos1-plus) (> dot cos1-minus))
          (return-from position-in-sensor-fov-p nil))))
    
    ;; then FOV2
    (multiple-value-bind
      (fov2 cos2-plus cos2-minus) (sensor-fov2 sensor jd)
      (let ((dot (3vect-dot-product pos-vector fov2)))
        (if (or (< dot cos2-plus) (> dot cos2-minus))
          (return-from position-in-sensor-fov-p nil))))
    )
  
  ;; passed all tests, must be in FOV
  t)


#|
(setf foo (make-fixed-astronomical-object "foo" :ra 0 :dec 90))
(show *astrod*)
(ra foo)
(dec foo)
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 80) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 70) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 61) :units :degrees))
(reset-fixed-position foo (ra-dec-to-cartesian-3vect '(180 60) :units :degrees))

(set-satellite-roll-type *ASTROD* foo :nominal)
(set-satellite-roll-type *ASTROD* foo :offset-from-nominal 
                         :roll-offset-angle (radians 5))
(set-satellite-roll-type *ASTROD* foo :offset-from-nominal 
                         :roll-offset-angle (radians -5))
(describe (find-sensor *astrod* :stta))
(describe-satellite-orientation *astrod* (dmy-to-time '21-mar-91))
(sensor-view-vector (find-sensor *astrod* :stta) (dmy-to-time '21-mar-91))
(sensor-cone-fov (find-sensor *astrod* :stta) (dmy-to-time '21-mar-91))
(sensor-fov1  (find-sensor *astrod* :stta) (dmy-to-time '21-mar-91))
(sensor-fov2  (find-sensor *astrod* :stta) (dmy-to-time '21-mar-91))
(position-in-sensor-fov-p (find-sensor *astrod* :stta) (dmy-to-time '21-mar-91)
                          (ra-dec-to-cartesian-3vect '(135 0) :units :degrees))
|#


(defmethod DESCRIBE-SATELLITE-ORIENTATION ((LEOS low-earth-orbit-satellite) jd
                                           &key (sensors t) (named-vectors t))
  
  "Compute and describe orientation at time"
  
  (describe-satellite-orientation-matrix 
   LEOS (satellite-orientation-matrix LEOS jd) jd
   :sensors sensors :named-vectors named-vectors))
                                         

(defmethod DESCRIBE-SATELLITE-ORIENTATION-MATRIX 
  ((LEOS low-earth-orbit-satellite) matrix jd
   &key (sensors t) (named-vectors t))

  "Input orientation matrix for satellite LEOS at time jd, print info
    about matrix to standard output.  If keyword arg :sensors is non-nil
    describe the sensor view directions.  If keyword arg :named-vectors
    describe the orientation of the named vectors.
    This is for debugging/testing."

  (let* ((v1 (first matrix))
         (v2 (second matrix))
         (v3 (third matrix))
         (sun-vector (position-vector *sun* jd))
         (sun-ra-dec (position-ra-dec sun-vector :units :degrees))
         (pointing-ra-dec (position-ra-dec v1 :units :degrees))
         (pointing-angle-to-sun (3vect-separation v1 sun-vector :units :degrees))
         (sun-angle-from-v2 (3vect-separation v2 sun-vector :units :degrees))
         (sun-angle-from-v3 (3vect-separation v3 sun-vector :units :degrees))
         )
    (format t "~%Pointing: ~,2f ~,2f deg (vect: ~,5f ~,5f ~,5f)"
            (first pointing-ra-dec) (second pointing-ra-dec)
            (first v1) (second v1) (third v1))
    (format t "~%Roll: ~a" (get-satellite-roll-type LEOS))
    (format t "~%Sun: ~,2f ~,2f deg" (first sun-ra-dec) (second sun-ra-dec))
    (format t "~%Pointing angle to sun:  ~,2f" pointing-angle-to-sun)
    (format t "~%Sun angle from v1-v3 plane (+ = towards v2): ~,2f"
            (- 90. sun-angle-from-v2))
    (format t "~%Sun angle from v3: ~,2f" sun-angle-from-v3)
    (when named-vectors
      (dolist (name (named-vector-names LEOS))
        (let* ((v (named-vector LEOS name jd))
               (ra-dec (position-ra-dec v :units :degrees))
               (sun-angle (3vect-separation v sun-vector :units :degrees)))
          (format t "~% ~15a ra ~6,2f dec ~6,2f deg, sun angle ~6,2f deg, elev ~6,2f deg"
                  name (first ra-dec) (second ra-dec) sun-angle
                  (- 90 sun-angle)))))
    (when sensors
      (dolist (sensor (sensor-list LEOS))
        (let* ((boresight (sensor-view-vector sensor jd))
               (boresight-ra-dec (position-ra-dec boresight :units :degrees))
               (boresight-angle-to-sun (3vect-separation boresight sun-vector
                                        :units :degrees)))
          (format t "~% Sensor: ~10a  boresight ra ~6,2f dec ~6,2f deg, sun angle ~6,2f deg"
                  (name sensor) (first boresight-ra-dec) (second boresight-ra-dec)
                  boresight-angle-to-sun))))
    ))


(defmethod ORBIT-ORIENTATION ((LEOS low-earth-orbit-satellite) jd)
  
  "Return orbit-orientation matrix mnp at specified time"
  
  ;; if time is set and is the same as last call
  (if (and (time-of-orientation LEOS)
           (or (slot-value LEOS 'fix-orbit-orientation)
	       (= (time-of-orientation LEOS) jd)))

    ;; then return previous orientation matrix
    (orbit-orientation-matrix LEOS)
    
    ;; else recalculate
    (let* ((ra (mod (+ (ra-of-asc-node LEOS)
                       (* (- jd (time-of-asc-node LEOS))
                          (nodal-regression-rate LEOS)))
                    2pi)) ; RA of asc node at input time
           
           ;; vector to current asc node
           (m (3vect-spherical-to-cartesian (list ra 0)))
           
           ;; y orthogonal to x in orbital plane
           ;; (rotate ra+90 about line of nodes by inclination angle)
           (n (3vect-rotate-about-pole 
               (3vect-spherical-to-cartesian (list (+ ra pi/2) 0)) m
               (inclination LEOS)))
           
           ;; orbit pole is m cross n
           (p (3vect-cross-product m n))
           ;; orient matrix
           (orient-matrix (list m n p)))
      
      ;; set instance variables
      (setf (time-of-orientation LEOS) jd)
      (setf (orbit-orientation-matrix LEOS) orient-matrix)
      ;; returned value
      orient-matrix)))


(defmacro WITH-FIXED-ORBIT-ORIENTATION (satellite &rest forms)

  "Execute forms with orbit orientation (i.e. orbit plane) fixed.  
    May be nested.  If already fixed, then time-of-orientation is used.
    If it is not, then time-of-orientation is nulled.  This will cause
    the plane to be recomputed for the first time found in forms."

  (let ((sym (gensym)))
  `(let ((,sym (slot-value ,satellite 'fix-orbit-orientation)))
     (unwind-protect
	 (progn (setf (slot-value ,satellite 'fix-orbit-orientation) t)
		(if (not ,sym) 
		    (setf (slot-value ,satellite 'time-of-orientation) nil))
		,@forms)
       (setf (slot-value ,satellite 'fix-orbit-orientation) ,sym)))))

#|
(print (orbital-pole-vector *st* 4000.0))
(print (orbital-pole-vector *st* 4010.0))
(with-fixed-orbit-orientation
 *st*
 (break)
 (print (orbital-pole-vector *st* 4000.0))
 (print (orbital-pole-vector *st* 4010.0))
 )
|#


(defmethod ORBITAL-POLE-VECTOR ((LEOS low-earth-orbit-satellite) jd)

  "return orbit pole 3-vector at specified time"

  (third (orbit-orientation LEOS jd)))


(defmethod ORBITAL-PHASE ((LEOS low-earth-orbit-satellite) jd)

  "return orbital phase as a function of time:  0 = ascending node
    range is 0 to 2pi"

  (* 2pi
     (mod
      (/ (- jd (time-of-asc-node LEOS))
	 (nodal-period LEOS))
      1))) 


(defmethod TIME-OF-PREV-ASC-NODE ((LEOS low-earth-orbit-satellite) jd)

  "return time of first ascending node crossing before input jd"

  (+ (time-of-asc-node LEOS)
     (*
      (floor (- jd (time-of-asc-node LEOS))
	     (nodal-period LEOS))
      (nodal-period LEOS)))) 


(defmethod TIME-OF-NEXT-ASC-NODE ((LEOS low-earth-orbit-satellite) jd)

  "return time of first ascending node crossing before input jd"

  (+ (nodal-period LEOS)
     (time-of-prev-asc-node LEOS jd))) 


(defmethod ASC-NODE-LIST ((LEOS low-earth-orbit-satellite) jd1 jd2)

  "return list of ascending nodes within specified times"

  (let* ((time (time-of-prev-asc-node LEOS jd1))
	 (result))
    (do ((tnext time
	  (time-of-next-asc-node LEOS
	   (+ tnext (epsilon LEOS)))))
	((> tnext jd2)
	 (nreverse result))

      ;; error detect
      (when (and result (= (first result) tnext))
	(error "~%ASC-NODE-LIST: same times! ~a" tnext))

      (push tnext result))))


(defmethod ORBIT-NUMBER ((LEOS low-earth-orbit-satellite) jd)

  "orbit number for ascending node"

  ;; a cheap kludge
  (round
   (/ (- jd (time-of-asc-node LEOS))
      (nodal-period LEOS)))) 


(defmethod POSITION-VECTOR ((LEOS low-earth-orbit-satellite) jd)

  "Return position vector for satellite.
   This will set time, position, and velocity instance variables"

  ;; if time is the same as last call
  (if (and (time-of-position LEOS)
      (= jd (time-of-position LEOS)))

    ;; then return previous position
    (last-position-vector LEOS)

    ;; else recalculate
    (let* ((phase (orbital-phase LEOS jd))

	   ;; get orbit basis vectors
	   (basis (orbit-orientation LEOS jd))

	   ;; compute m and n components of satellite position
	   (mcomp (3vect-times-scalar (cos phase) (first basis)))
	   (ncomp (3vect-times-scalar (sin phase) (second basis))))

      ;; set instance variables
      (setf (time-of-position LEOS) jd)

      ;; add to get total position vector
      (setf (last-position-vector LEOS) (3vect-add mcomp ncomp))
      (setf (last-velocity-vector LEOS)
	    (3vect-cross-product (third basis) (last-position-vector LEOS)))

      ;; return position vector
      (last-position-vector LEOS)))) 


(defmethod DISTANCE ((LEOS low-earth-orbit-satellite) jd &optional (print-steps nil))
  "return distance in km from center of earth"
  (declare (ignore jd print-steps))
  (semimajor-axis LEOS)) 


(defmethod VELOCITY-VECTOR ((LEOS low-earth-orbit-satellite) jd)
  "Return velocity unit vector"

  ;; if time is not known or differs from last call
  (if (or (null (time-of-position LEOS))
      (/= jd (time-of-position LEOS)))

    ;; then calculate (position computes velocity as side effect)
    (position-vector LEOS jd))
  (last-velocity-vector LEOS)) 


(defmethod VELOCITY ((LEOS low-earth-orbit-satellite) jd)
  "Return speed in km/sec"
  (declare (ignore jd))
  (object-speed LEOS)) 


(defmethod IN-ORBIT-FRAME ((LEOS low-earth-orbit-satellite) v jd)
  "return components of vector v in orbital frame at time jd
    components of v are specified in equatorial frame
    components are along m, n, p vectors respectively"
  (3x3matrix-transpose-times-vector (orbit-orientation LEOS jd) v))


(defmethod IN-ORBIT-FRAME ((LEOS low-earth-orbit-satellite)
                           (object astronomical-object)
                           jd)
  "return components of line-of-sight from LEOS to object
    in orbital frame at time jd
    components of v are specified in equatorial frame
    components are along m, n, p vectors respectively"
  (3x3matrix-transpose-times-vector (orbit-orientation LEOS jd)
   (line-of-sight LEOS object jd)))


(defmethod ORBITAL-LONGITUDE-LATITUDE ((LEOS low-earth-orbit-satellite) v jd
                                       &key (units :radians))
  "Return list (longitude latitude) in radians for vector v in orbit frame 
    of satellite at time jd.  :units may be either :degrees or :radians (default)"
  (let ((long-lat (3vect-cartesian-to-spherical
                   (in-orbit-frame LEOS v jd))))
    (if (eq units :degrees)
      (mapcar #'degrees long-lat)
      long-lat)))


(defmethod ORBITAL-LONGITUDE-LATITUDE ((LEOS low-earth-orbit-satellite) 
                                       (object astronomical-object)
                                       jd
                                       &key (units :radians))
  "Return list (longitude latitude) in radians for line-of-sight vector
    from LEOS to object in orbit frame of satellite at time jd.
    Units may be :degrees or :radians (default)"
  (orbital-longitude-latitude
   LEOS (line-of-sight LEOS object jd) jd :units units))


(defmethod ITERATE-TO-HORIZON ((LEOS low-earth-orbit-satellite) target jd &optional (limb-angle 0))
  "calculate occultation time iteratively given an initial guess jd"
  (do* (
	;; t1 t2 a1 a2 are updated in loop body
	(t1 jd)
	(t2 (+ jd (epsilon LEOS)))
	(a1 (angle-to-horizon LEOS target t1 limb-angle))
	(a2 (angle-to-horizon LEOS target t2 limb-angle))
	;; updated from the above
	;; dadt is slope (a2-a1)/(t2-t1)
	(dadt (/ (- a2 a1) (epsilon LEOS))
	 (if (/= t2 t1)
	   (/ (- a2 a1) (- t2 t1))
	   nil))

	;; delta-t is how far t1 is from linear estimate of a=0
	(delta-t (if (and dadt (/= 0 dadt))
		   (/ (- a1) dadt)
		   nil)
	 (if (and dadt (/= 0 dadt))
	   (/ (- a1) dadt)
	   nil))

	;; tzero is the estimated time of a=0
	(tzero (if delta-t
		 (+ t1 delta-t)
		 nil) (if delta-t
						 (+ t1 delta-t)
						 nil)))
       ((or (null tzero) (<= (abs delta-t) (epsilon LEOS)))	;completion test
	tzero)    ;returned value

    ;;(format t  ~% input off by ~a deg (~a sec)  (degrees a1) (* delta-t 86400))
    ;; next iteration:  decide which pair of points to use for 
    ;; linear extrapolation
    ;; if t less than t1 + (t2-t1)/2
    (if (< tzero (+ t1 (* 0.5 (- t2 t1))))

      ;; then if t < t1
      (if (< tzero t1)

	;; then
	(setf t2 t1
	      a2 a1
	      t1 tzero
	      a1 (angle-to-horizon LEOS target t1 limb-angle))

	;; else (t >= t1)
	(setf t2 tzero
	      a2 (angle-to-horizon LEOS target t2 limb-angle)))

      ;; else t >= t1 + (t2-t1)/2
      (if (> tzero t2)

	;; if t > t2
	(setf t1 t2
	      a1 a2
	      t2 tzero
	      a2 (angle-to-horizon LEOS target t2 limb-angle))

	;; else t <= t2
	(setf t1 tzero
	      a1 (angle-to-horizon LEOS target t1 limb-angle))))
    ;;(format t   ~as  (round (* delta-t 86400)))
    )) 


(defmethod TIME-OF-NEXT-MIDNIGHT ((LEOS low-earth-orbit-satellite) target jd)
  "ESTIMATE time of next midnight within +/- one orbit"
  (let* ((target-orb-long
	  (first
	   (3vect-cartesian-to-spherical
	    (in-orbit-frame LEOS
	     (line-of-sight LEOS target jd) jd))))
	 (current-phase (orbital-phase LEOS jd))

	 ;; phase of deepest occultation (midnight)
	 (deepest-phase
	  (if (< target-orb-long pi)
	    (+ pi target-orb-long)
	    (- target-orb-long pi)))

	 ;; time of deepest occultation
	 (deepest-time
	  (+ jd
	     (* (nodal-period LEOS) (/ (- deepest-phase current-phase) 2pi)))))
    deepest-time))


(defmethod MIDNIGHT-LIST ((LEOS low-earth-orbit-satellite) target jd1 jd2 
                          &key (tolerance 1.0e-4))
  ;; tolerance in days

  "return list of midnights between specified times jd1 to jd2
    result returned is list of time sin-latitude cons pairs, where 
    sin-latitude is sin of target LOS orbital latitude"

  (do* (
	;; pad times by 2 orbits in each direction
	(t1 (- jd1 (* 2 (nodal-period LEOS))))
	(t2 (+ jd2 (* 2 (nodal-period LEOS))))
        ;; only include midnights in orbits that overlap input interval:
        ;; these are the limiting times -- for conservatism, the times are
        ;; extended by tolerance in case the input time are node crossing
        ;; times
        (tmin (time-of-prev-asc-node LEOS (- jd1 tolerance)))
        (tmax (time-of-next-asc-node LEOS (+ jd2 tolerance)))

	;; estimated first midnight
	(time (time-of-next-midnight LEOS target t1))

	;; local variables used in iteration
	target-vector
	target-long
	earth-phase
	earth-long
	time-correction

	;; for returned value (list of times)
	(result (quote nil)))
       ((>= time t2)				;stopping condition
	(nreverse result))			;returned value

    ;;(format t  ~%Initial time: ~a  time)
    (setf target-vector
	  (in-orbit-frame LEOS
	   (line-of-sight LEOS target time) time)
	  target-long (3vect-project-to-xy-plane target-vector)
	  earth-phase (+ pi (orbital-phase LEOS time))
	  earth-long (list (cos earth-phase) (sin earth-phase) 0)
	  time-correction
	  (*
	   (asin
	    (- (* (first earth-long) (second target-long))
	       (* (second earth-long) (first target-long))))
	   (/ (nodal-period LEOS) 2pi))
	  time (+ time time-correction))
    ;;(format t  ~% - corr: ~a  time-correction)

    ;; if we pass the following test then the time has converged
    ;; otherwise it didn't converge:  refine and try again
    (when (< (abs time-correction) tolerance)
      ;; this midnight converged: save only if within bounding times
      (if (and (>= time tmin) (<= time tmax))
        (setf result (cons (cons time (third target-vector)) result)))
      ;; update time: next guess is one period later
      (setf time (+ time (nodal-period LEOS))))))


(defmethod OCCULTATION-TIME-PAIR ((LEOS low-earth-orbit-satellite) target 
				  time sin-orb-lat sin2-orb-lat
				  cos2-orb-lat limb-angle &key (which :both) (high-accuracy t))
  "compute occultation times for a specified target and limb angle.
    Input is target: instance of astronomical-object
             time: a midnight time i.e. time when LOS vector from satellite
               to target is closest to earth center
             sin-orb-lat: sin of orbital latitude at time
             sin2-orb-lat: sin**2 of orb lat at time
             cos2-orb-lat: cos**2 of orb lat at time
             limb-angle: radians
             which:  can be either :start, :end, or :both
             high-accuracy t/nil
    Output list (start end) times for occultation (depending on value of which),
     or nil if no occultation occurs"
  (let* (
	 ;; assumed that satellite distance is constant over interval
	 (orb-lat (abs (asin sin-orb-lat)))
	 (earth-half-angle (asin (/ *earth-radius* (distance LEOS time))))
	 (occ-half-angle (+ earth-half-angle limb-angle)))
    ;;(format t "~%OCC-TIME-PAIR: earth half angle ~,4f (~,4f deg) occ half angle ~,4f (~,4f deg)"
    ;;	    earth-half-angle (degrees earth-half-angle) occ-half-angle (degrees occ-half-angle))
    ;; check if occultation occurs at this limb angle
    (cond
     ((and (> occ-half-angle pi/2) (> orb-lat (- pi occ-half-angle)))
      ;; occulted the whole orbit
      ;;(format t "~%--- occulted whole orbit")
      (list (- time (/ (nodal-period LEOS) 2.0))
	    (+ time (/ (nodal-period LEOS) 2.0))))
     ((< orb-lat occ-half-angle)
      ;; yes:  compute times
      ;;(format t "~%cos-occ-angle ~a" (cos (* 2 occ-half-angle)))
      ;;(format t "~%cos-occ-long-range ~a" (/ (+ (cos (* 2 occ-half-angle)) sin2-orb-lat) cos2-orb-lat))
       
      (let* ((cos-occ-angle (cos (* 2 occ-half-angle)))
	     (occ-longitude-range (acos (/ (+ cos-occ-angle sin2-orb-lat) cos2-orb-lat)))
	     (half-occ-duration
	      (* (nodal-period LEOS) (/ occ-longitude-range 2pi) 0.5))
	     (start-time
	      (if (or (eq which :both) (eq which :start))
		  (if high-accuracy
		      (iterate-to-horizon LEOS target
					  (- time half-occ-duration) limb-angle)
		    (- time half-occ-duration))))
	     (end-time
	      (if (or (eq which :both) (eq which :end))
		  (if high-accuracy
		      (iterate-to-horizon LEOS target
					  (+ time half-occ-duration) limb-angle)
		    (+ time half-occ-duration)))))
	;;(format t "~%cos-occ-angle: ~,4f occ-long-range ~,4f (~,4f deg)"
	;;	 cos-occ-angle occ-longitude-range (degrees occ-longitude-range))
	;;(format t " half-occ-duration ~,4f (~,4f min)"
	;;	 half-occ-duration (* half-occ-duration 1440))
	(list start-time end-time)))
     (t nil)))) 


(defmethod OCCULTATION-LIST ((LEOS low-earth-orbit-satellite) target jd1 jd2 
                             &key (limb-angle 0)
                             (file nil) 
                             (high-accuracy t))
  "return start/end occultation times, i.e. list of 
    (start1 end1 start2 end2 ...) if file is nil,
   or write the results to file (which should be an output stream)"
  (let* ((midnights (midnight-list LEOS target jd1 jd2))

	 ;; assumed that satellite distance is constant over interval
	 (earth-half-angle (asin (/ *earth-radius* (distance LEOS jd1))))
	 (occ-half-angle (+ earth-half-angle limb-angle))
	 (sin-occ-half-angle (sin occ-half-angle))
	 ;;; not used   (cos-occ-angle (cos (* 2 occ-half-angle)))
	 ;; locals used in loop below
	 time
	 sin-orb-lat
	 sin2-orb-lat
	 cos2-orb-lat
	 start-end
	 start-time
	 end-time
	 ;; for result
	 (result (quote nil)))
    (dolist (m midnights)
      (setf time (first m)
	    sin-orb-lat (rest m))
      (when (< (abs sin-orb-lat) sin-occ-half-angle)

	;; ok: there are occultations
	(setf 
	  ;; sin^2 of orbital latitude = cos^2(pi/2 - latitude)
	  ;; sin(latitude) is 3rd component of LOS vector
	  sin2-orb-lat (* sin-orb-lat sin-orb-lat)
	  cos2-orb-lat (- 1 sin2-orb-lat)
	  start-end
	  (occultation-time-pair LEOS target time sin-orb-lat
				 sin2-orb-lat cos2-orb-lat limb-angle :which :both :high-accuracy high-accuracy)
	  start-time (first start-end)
	  end-time (second start-end))

	;; put out the results to a file or list
	(if (and start-time end-time)
	  (cond
	    ((streamp file) (princ start-time file) (princ " " file) (princ end-time file)
	     (terpri file))
	    (t (push start-time result) (push end-time result)))))
      ;end when
      )

    ;end loop over midnights
    (nreverse result))) 


(defmethod SUNLIT-LIMB ((LEOS low-earth-orbit-satellite) target time)
  (let* ((horizon-angle (angle-to-horizon LEOS target time))
	 (my-position (position-vector LEOS time))
	 (my-distance (distance LEOS time))
	 (los-vector (line-of-sight LEOS target time))
	 (distance-along-los
	  (/ (sqrt (- (* my-distance my-distance) (* *earth-radius* *earth-radius*)))
	     (cos horizon-angle)))
	 (normalized-limb-vector
	  (3vect-normalize
	   (3vect-linear-comb my-distance my-position distance-along-los los-vector)))

	 ;; vector from earth center to point on earth's limb just
	 ;; below line of sight to target
	 (limb-vector (3vect-times-scalar *earth-radius* normalized-limb-vector))
	 (sun-vector (3vect-times-scalar (distance *sun* time) (position-vector *sun* time)))
	 (los-to-sun (3vect-normalize (3vect-subtract sun-vector limb-vector)))
	 (dot-with-sun (3vect-dot-product normalized-limb-vector los-to-sun)))

    ;; if dot product is positive then sun is visible from limb-vector
    (> dot-with-sun 0))) 


(defmethod BRIGHT-DARK-OCCULTATION-LIST ((LEOS low-earth-orbit-satellite) target jd1 jd2 
					 &key (tolerance 1.0e-4)
					 ;tolerance in days
					 (bright-limb-angle (radians 15)) (dark-limb-angle (radians 5))
					 (high-accuracy t) (file nil))
  ;; results are a list unless file is a stream, in which case the
  ;; times are written to the stream
  ;; dark must be less than or equal to bright
  (do* (;; pad times by 2 orbits in each direction
	(t1 (- jd1 (* 2 (nodal-period LEOS))))
	(t2 (+ jd2 (* 2 (nodal-period LEOS))))
	;; estimated first midnight
	(time (time-of-next-midnight LEOS target t1))
	(nodal-period/2pi (/ (nodal-period LEOS) 2pi))
	;; local variables used in iteration
	target-vector
	target-long
	earth-phase
	time-correction
	(result (quote nil)))
       ((>= time t2)				;stopping condition
	(nreverse result))			;returned value

    (setf target-vector
	  (in-orbit-frame LEOS
	   (line-of-sight LEOS target time) time)
	  target-long (3vect-project-to-xy-plane target-vector)
	  earth-phase (+ pi (orbital-phase LEOS time))
	  time-correction
	  (*
	   (asin
	    (- (* (cos earth-phase) (second target-long))
	       (* (sin earth-phase) (first target-long))))
	   nodal-period/2pi)
	  time (+ time time-correction))

    ;; if we pass the following test then the time has converged
    ;; otherwise it didn't converge:  refine and try again
    (when (< (abs time-correction) tolerance)
      ;; this midnight converged: get occ times and move on to next
      ;; note that target vector used below is for time - time-correction
      (let* ((sin-orb-lat (third target-vector))

	     ;; sin^2 of orbital latitude = cos^2(pi/2 - latitude)
	     ;; sin(latitude) is 3rd component of LOS vector
	     (sin2-orb-lat (* sin-orb-lat sin-orb-lat))
	     (cos2-orb-lat (- 1 sin2-orb-lat))
	     (geo-occ-start
	      (if (not (= bright-limb-angle dark-limb-angle))
		(first
		 (occultation-time-pair LEOS target time sin-orb-lat
		  sin2-orb-lat cos2-orb-lat 0 :which :start :high-accuracy high-accuracy))))
	     occ-start
	     occ-end)
	(cond
	  ((= bright-limb-angle dark-limb-angle) 
	   ;; no need to check sun illumination
	   (let ((start-end
		  (occultation-time-pair LEOS target time sin-orb-lat
		   sin2-orb-lat cos2-orb-lat bright-limb-angle :which :both :high-accuracy
		   high-accuracy)))
	     (if start-end
	       (setf occ-start (first start-end)
		     occ-end (second start-end)))))
	  (geo-occ-start
	   (let* ((geo-occ-end
		   (second
		    (occultation-time-pair LEOS target time sin-orb-lat
		     sin2-orb-lat cos2-orb-lat 0 :which :end :high-accuracy high-accuracy))))
	     (setf occ-start
		   (first
		    (if (sunlit-limb LEOS target geo-occ-start)
		      (occultation-time-pair LEOS target time sin-orb-lat
		       sin2-orb-lat cos2-orb-lat bright-limb-angle :which :start :high-accuracy
		       high-accuracy)
		      (occultation-time-pair LEOS target time sin-orb-lat
		       sin2-orb-lat cos2-orb-lat dark-limb-angle :which :start :high-accuracy
		       high-accuracy)))
		   occ-end
		   (second
		    (if (sunlit-limb LEOS target geo-occ-end)
		      (occultation-time-pair LEOS target time sin-orb-lat
		       sin2-orb-lat cos2-orb-lat bright-limb-angle :which :end :high-accuracy
		       high-accuracy)
		      (occultation-time-pair LEOS target time sin-orb-lat
		       sin2-orb-lat cos2-orb-lat dark-limb-angle :which :end :high-accuracy
		       high-accuracy))))))
	  ((sunlit-limb LEOS target time)

	   ;; limb under deepest occ is sunlit:  use bright angle
	   (let ((start-end
		  (occultation-time-pair LEOS target time sin-orb-lat
		   sin2-orb-lat cos2-orb-lat bright-limb-angle :which :both :high-accuracy
		   high-accuracy)))
	     (if start-end
	       (setf occ-start (first start-end)
		     occ-end (second start-end)))))
	  (t 
	   ;; use dark limb angle
	   (let ((start-end
		  (occultation-time-pair LEOS target time sin-orb-lat
		   sin2-orb-lat cos2-orb-lat dark-limb-angle :which :both :high-accuracy
		   high-accuracy)))
	     (if start-end
	       (setf occ-start (first start-end)
		     occ-end (second start-end))))))

	;(format t  ~%~a(~a,~a) to ~a(~a,~a) 
	;        occ-start (send self :sunlit-limb target occ-start)
	;        (degrees (send self :angle-to-horizon target occ-start))
	;        occ-end (send self :sunlit-limb target occ-end)
	;        (degrees (send self :angle-to-horizon target occ-end)))
	(if (and occ-start occ-end)
	  (cond
	    ((streamp file) (princ occ-start file) (princ " " file) (princ occ-end file)
	     (terpri file))
	    (t (push occ-start result) (push occ-end result)))))

      ;end let*
      ;; go on to next orbit
      (setf time (+ time (nodal-period LEOS)))))) 


(defmethod OCCULTATION-MISS-TIME ((LEOS low-earth-orbit-satellite) target jd &optional (limb-angle 0))
  "input time at which occultation entry/exit expected
    output is approx time in seconds (rounded to nearest tenth)
    by which missed"
  (let* ((t1 jd)
	 (t2 (+ jd (epsilon LEOS)))
	 (a1 (angle-to-horizon LEOS target t1 limb-angle))
	 (a2 (angle-to-horizon LEOS target t2 limb-angle))
	 (dadt (/ (- a2 a1) (epsilon LEOS)))
	 (delta-t (/ (- a1) dadt)))
    (/ (round (* delta-t 864000)) 10))) 


;;; --- new methods for occultation by illuminated earth, including 
;;; terminator.  Added MDJ 11Nov91.

;;; this replaces BRIGHT-DARK-OCCULTATION-LIST with tests for distance to
;;; bright/dark earth

(defmethod EARTH-ILLUM-OCCULTATION-LIST
  ((LEOS low-earth-orbit-satellite) 
   (target astronomical-object) 
   jd1 jd2 
   &key 
   (bright-limb-angle (radians 20))
   (dark-limb-angle (radians 5))
   (terminator-angle (radians 100))
   (file nil) 
   (high-accuracy t)
   (initial-step 0.000694) ;1 minute
   (cut-step-factor 0.2)
   (convergence 0.000001157) ;0.1 second
   (debug nil))
  
  "Return start/end occultation times, i.e. list of 
     (start1 end1 start2 end2 ...) if file is nil,
    or write the results to file (which should be an output stream).
    See STEP-CHECK-VISIBILITY for description of initial-step, cut-step-factor,
    and convergence.
    Voluminous *standard-output* if debug is t."

  (when debug
    (format t "~%LEOS ~a target ~a jd1 ~a jd2 ~a"
	    LEOS target jd1 jd2))

  (cond 
   
   ((= bright-limb-angle dark-limb-angle)
    ;; same angles, use plain occultation finder
    (occultation-list LEOS target jd1 jd2 
                      :limb-angle dark-limb-angle
                      :file file
                      :high-accuracy high-accuracy))
   
   (t ;; need fullblown calculation
    (let* (
           ;; assumed that satellite distance is constant over interval
           (earth-half-angle (asin (/ *earth-radius* (distance LEOS jd1))))
           
           (bright-occ-half-angle (+ earth-half-angle bright-limb-angle))
           (bright-sin-occ-half-angle (sin bright-occ-half-angle))
           (dark-occ-half-angle (+ earth-half-angle dark-limb-angle))
           (dark-sin-occ-half-angle (sin dark-occ-half-angle))
           
           ;; for result
           (result nil)
           )

      (dolist (m (midnight-list LEOS target jd1 jd2))
        (let* ((time (first m))
              (sin-orb-lat (rest m))
              ;; sin^2 of orbital latitude = cos^2(pi/2 - latitude)
              ;; sin(latitude) is 3rd component of LOS vector
	      (sin2-orb-lat (* sin-orb-lat sin-orb-lat))
	      (cos2-orb-lat (- 1 sin2-orb-lat))
              (dark-occ nil)              ; t if occultation at dark-limb-angle
              dark-occ-start dark-occ-end ; times if dark-occ is t
              (bright-occ nil)            ; t if occultation at bright-limb-angle
              bright-occ-start bright-occ-end   ;times if bright-occ is t
              occ-start occ-end           ; overall start/end
              )

          ;; get bright/dark occ times, if any
	  (setf dark-occ
	    (occultation-time-pair 
	     LEOS target time sin-orb-lat sin2-orb-lat cos2-orb-lat 
	     dark-limb-angle :which :both :high-accuracy high-accuracy))
	  (setf dark-occ-start (first dark-occ)
                dark-occ-end   (second dark-occ))
	  (setf bright-occ
	    (occultation-time-pair  
	     LEOS target time sin-orb-lat sin2-orb-lat cos2-orb-lat 
	     bright-limb-angle :which :both :high-accuracy high-accuracy))
	  (setf bright-occ-start (first bright-occ)
		bright-occ-end   (second bright-occ))

	  (when debug
	    (format t "~%dark-occ start ~a end ~a" dark-occ-start dark-occ-end)
	    (format t "~%bright-occ start ~a end ~a" bright-occ-start bright-occ-end)
	    )
	    
          
          (cond                             ; 3 cases
           
           ;; --- Case 1:  no occultation
           ((and (null dark-occ) (null bright-occ))
            nil)
           
           ;; --- Case 2:  bright occ only: check from start to midnight, 
           ;; then end to midnight
           ((and bright-occ (not dark-occ))
            (if debug (format t "~%~%~%-- START bright occ only"))
            (setf occ-start
                  (step-check-visibility LEOS target bright-occ-start bright-occ-end
                                         :bright-limb-angle bright-limb-angle
                                         :dark-limb-angle dark-limb-angle
                                         :terminator-angle terminator-angle
                                         :initial-step initial-step
                                         :cut-step-factor cut-step-factor
                                         :convergence convergence
                                         :debug debug))
            (if debug (format t "~%-- END bright occ only"))
            (setf occ-end
                  (step-check-visibility LEOS target bright-occ-end bright-occ-start
                                         :bright-limb-angle bright-limb-angle
                                         :dark-limb-angle dark-limb-angle
                                         :terminator-angle terminator-angle 
                                         :initial-step initial-step
                                         :cut-step-factor cut-step-factor
                                         :convergence convergence
                                         :debug debug))
            (when (> occ-start occ-end)
              ;; if times reverse, then no occultation
              (setf occ-start nil occ-end nil))
            )
           
           ;; --- Case 3:  both bright and dark occ
           ((and bright-occ dark-occ)	; bright and dark occ
            ;; check from bright start to dark start, then bright end to dark end
            (if debug (format t "~%~%~%-- START bright and dark occ"))
            (setf occ-start
                  (step-check-visibility LEOS target bright-occ-start dark-occ-start
                                         :bright-limb-angle bright-limb-angle
                                         :dark-limb-angle dark-limb-angle
                                         :terminator-angle terminator-angle 
                                         :initial-step initial-step
                                         :cut-step-factor cut-step-factor
                                         :convergence convergence
                                         :debug debug))
            (if debug (format t "~%-- END bright and dark occ"))
            (setf occ-end
                  (step-check-visibility LEOS target bright-occ-end dark-occ-end
                                         :bright-limb-angle bright-limb-angle
                                         :dark-limb-angle dark-limb-angle
                                         :terminator-angle terminator-angle 
                                         :initial-step initial-step
                                         :cut-step-factor cut-step-factor
                                         :convergence convergence
                                         :debug debug))
            )

	   ;; --- error case: dark occ, but no bright occ
	   (t (format t "--- error case: dark but no bright occ")
	      (format t "~%limb angles bright: ~,4f (~,4f deg) dark: ~,4f (~,4f deg)"
		      bright-limb-angle (degrees bright-limb-angle)
		      dark-limb-angle (degrees dark-limb-angle))
	      (format t "~%earth half angle ~,4f (~,4f deg) term ~,4f (~,4f deg)" 
		      earth-half-angle (degrees earth-half-angle)
		      terminator-angle (degrees terminator-angle))
	      (format t "~%occ half angle bright: ~,4f (~,4f deg) dark: ~,4f (~,4f deg)"
		      bright-occ-half-angle (degrees bright-occ-half-angle)
		      dark-occ-half-angle (degrees dark-occ-half-angle))
	      (format t "~%sin orb lat ~,4f, orb lat ~,4f (~,4f deg)"
		      sin-orb-lat (asin sin-orb-lat) (degrees (asin sin-orb-lat)))
	      (format t "~%dark   sin-occ-half-angle ~,4f occ? ~a"
		      dark-sin-occ-half-angle (< (abs sin-orb-lat) dark-sin-occ-half-angle))
	      (format t "~%bright sin-occ-half-angle ~,4f occ? ~a"
		      bright-sin-occ-half-angle (< (abs sin-orb-lat) bright-sin-occ-half-angle))

	      (error "dark but no bright occ"))
	   )				;end cond
          
          (when (and debug occ-start occ-end)
            (format t "~%  at occ start:~a  end:~a"
                    (illum-description LEOS target occ-start :terminator-angle terminator-angle)
                    (illum-description LEOS target occ-end   :terminator-angle terminator-angle)))
          
          ;; put out the results to a file or list
          (if (and occ-start occ-end)
            (cond ((streamp file) 
                   (princ occ-start file) 
                   (princ " " file) 
                   (princ occ-end file)
                   (terpri file))
                  (t (push occ-start result) (push occ-end result))))
          
          ))                                ; end loop over midnights
      
      (nreverse result)))))



(defmethod STEP-CHECK-VISIBILITY ((LEOS low-earth-orbit-satellite)
                                  (target astronomical-object)
                                  jd1 jd2 
                                  &key 
                                  (bright-limb-angle (radians 20))
                                  (dark-limb-angle (radians 5))
                                  (terminator-angle (radians 100))
                                  (initial-step 0.000694) ;1 minute
                                  (cut-step-factor 0.2)
                                  (convergence 0.000001157) ;0.1 second
                                  (debug nil)
                                  )

  "Method for finding last time between two times which does not violate
    dark/bright pointing restrictions.  Auxiliary method for EARTH-ILLUM-
    OCCULTATION-LIST.  
    Input arguments are:
     LEOS - the satellite (viewer)
     target - the target
     jd1, jd2:  start and end times for search.  Either of jd1 > jd2 or 
      jd1 < jd2 is OK.  The search starts at jd1 and steps toward jd2,
      returning the last time when the limb angle limits are OK.
      If they are violated at jd1 or jd1+/-convergence, then jd1 is returned
      immediately.  If they are not violated by the time jd2 is reached,
      then jd2 is returned.
     bright-limb-angle - radians, can't look closer than this angle to the
      illuminated earth (either limb or terminator)
     dark-limb-angle - radians, can't look closer than this angle to the earth:
       bright-limb-angle > dark-limb-angle
     terminator-angle - radians, angle from sun which defines terminator.
     initial-step - days, size of initial step from jd1 to jd2
     cut-step-factor - if step causes transition from allowed to not allowed
      configuration, they cut the step size by this factor and try again.
      must be <1, e.g. 0.2
     convergence - days, if difference between two steps is less than this,
      and configuration one one step is legal but other one is not, then
      return the legal time as the solution."
     
  (let* ((dt (if (> jd2 jd1) initial-step (- initial-step)))
         (LAT jd1) ;last allowed time
         (jd1-eps (+ jd1 (* (/ dt (abs dt)) convergence)))
         (horiz-angle 
          (los-to-horizon-angle 
           (position-vector LEOS jd1-eps) 
           (distance LEOS jd1-eps)
           (line-of-sight LEOS target jd1-eps)
           *earth-radius* 
           :limb-angle 0))
         (illum-angle (angle-to-illum-earth
                       (position-vector LEOS jd1-eps)
                       (distance LEOS jd1-eps)
                       (position-vector *sun* jd1-eps)
                       terminator-angle
                       (line-of-sight LEOS target jd1-eps) 
                       *earth-radius*))
         )

    (when debug
      (format t "~%  illum at jd1: ~a" 
              (illum-description LEOS target jd1 :terminator-angle terminator-angle))
      (format t "~%  at jd1 + eps: ~a" 
              (illum-description LEOS target (+ jd1 (* (/ dt (abs dt)) convergence))
                     :terminator-angle terminator-angle)))

    ;; if configuration at jd1+convergence does not meet conditions, then
    ;; exit with initial time
    (when (not (and (>= horiz-angle dark-limb-angle)
                    (or (null illum-angle)
                        (>= illum-angle bright-limb-angle))))
      (if debug (format t "~%  -- exiting with jd1"))
      (return-from step-check-visibility LAT))

    (loop 
      ;; done?
      (if (< (abs dt) convergence) (return nil))

      (let ((time (+ LAT dt)))
        (cond 
         ((or (and (> dt 0) (> time jd2))
              (and (< dt 0) (< time jd2)))
          ;; cut step to prevent overstep end time
          (setf dt (* dt cut-step-factor))
          (if debug (format t "~%    overstep dt->~7,2fsec" (* 86400 dt)))
          )
         (t 
          ;; is new time allowed?
          (let* ((sat (position-vector LEOS time))
                 (sat-dist (distance LEOS time))
                 (los (line-of-sight LEOS target time))
                 (sun (position-vector *sun* time))
                 (horiz-angle 
                  (los-to-horizon-angle 
                   sat sat-dist los *earth-radius* :limb-angle 0))
                 ;; only compute illum angle if horiz-angle > dark-limb-angle
                 (illum-angle-list
                  (if (>= horiz-angle dark-limb-angle)
                    (multiple-value-list
                     (angle-to-illum-earth sat sat-dist sun terminator-angle
                                           los *earth-radius*))))
                 (illum-angle (first illum-angle-list))
                 (illum-angle-type (second illum-angle-list))
                 )
            (if debug
              (format t "~%    t=~7,4fmin, dt=~7,2fsec"
                      (* 1440.0 (- time jd1)) (* 86400 dt)))
            (cond 
             ((and (>= horiz-angle dark-limb-angle)
                   (or (null illum-angle)
                       (>= illum-angle bright-limb-angle)))
              ;; time is allowed
              (if debug
                (format t " allowed, illum ~a ~a horiz ~9,5fd"
                        (if illum-angle (format nil "~9,5fd" (degrees illum-angle)) nil)
                        illum-angle-type
                        (degrees horiz-angle)))
              (setf LAT time))
             (t ;; time not allowed: backup
              (setf dt (* dt cut-step-factor))
              (if debug
                (format t " not allowed, illum ~a ~a horiz ~9,5fd, dt->~7,2fsec"
                        (if illum-angle (format nil "~9,5fd" (degrees illum-angle)) nil)
                        illum-angle-type
                        (degrees horiz-angle)
                        (* 86400 dt)))
              ))))))

          )                           ; end loop
    ;; returned value is LAT
    (if debug
      (format t "~%  illum at LAT: ~a" 
              (illum-description LEOS target LAT :terminator-angle terminator-angle)))
    LAT))


(defmethod ILLUM-DESCRIPTION ((LEOS low-earth-orbit-satellite)
                  (target astronomical-object)
                  time 
                  &key 
                  (terminator-angle (radians 100)))

  "return string describing illumination state
    Used for debugging (i.e. when debug=t)."

  (let* ((sat (position-vector LEOS time))
         (sat-dist (distance LEOS time))
         (los (line-of-sight LEOS target time))
         (sun (position-vector *sun* time))
         (horiz-angle 
          (los-to-horizon-angle 
           sat sat-dist los *earth-radius* :limb-angle 0))
         )
    (multiple-value-bind
      (illum-angle angle-type)
      (angle-to-illum-earth sat sat-dist sun terminator-angle
                            los *earth-radius*)
      ;;(list (if illum-angle (degrees illum-angle) nil)
      ;;      angle-type
      ;;      (degrees horiz-angle))
      (format nil "~a h=~9,5fd term=~,1fd"
              (if illum-angle 
		  (format nil "illum=~9,5fd ~a" (degrees illum-angle) angle-type)
		(format nil "no illum ~a" angle-type))
              (degrees horiz-angle)
	      (degrees terminator-angle))
      )))


;;; ======================================================================
;;;                                         CLASS: GEOSTATIONARY-SATELLITE
;;;
;;; for TDRS
;;; geostationary equatorial satellite class


(defclass GEOSTATIONARY-SATELLITE (satellite)
  ((longitude
       :initform  nil
       :initarg  :longitude
       :accessor  longitude)			; radians
   (semimajor-axis
       :initform  nil
       :initarg  :semimajor-axis
       :accessor  semimajor-axis)		; km
   (period
       :initform  nil
       :initarg  :period
       :accessor  period)			; days
   (object-speed
       :initform  nil
       :initarg  :object-speed
       :accessor  object-speed)			; km/s
   (position-in-earth-frame
       :initform  nil
       :initarg  :position-in-earth-frame
       :accessor  position-in-earth-frame)	; unit vector, referred to earth orientation 
   ;; for keeping track of state
   (time-of-position
       :initform  nil
       :initarg  :time-of-position
       :accessor  time-of-position)
   (last-position-vector
       :initform  nil
       :initarg  :last-position-vector
       :accessor  last-position-vector)))


(defun MAKE-GEOSTATIONARY-SATELLITE (n long p axis &aux instance)

  "Make an instance of the geostationary-satellite flavoir.
    required arguments:
    n name, string
    long longitude, degrees
    p period, seconds
    axis semimajor axis, km"

  (setf instance (make-instance 'geostationary-satellite))
  (setf (name instance) n)
  (setf (longitude instance) (radians long))
  (setf (semimajor-axis instance) axis)
  (setf (period instance) (/ p 86400))
  (setf (object-speed instance) (/ (* 2pi axis) p))
  (setf (position-in-earth-frame instance) 
        (3vect-spherical-to-cartesian (list long 0)))
  (update-satellite-table n instance)
  instance)


(defmethod UPDATE-ORBIT-PARAMETERS ((GEOS geostationary-satellite)
                                    &key longitude period semimajor-axis)

  "Update orbit model parameters.  Keyword arguments are:
     longitude - degrees
     period  - nodal period, seconds
     semimajor-axis - km
    All are required."

  (cond
   ((and longitude period semimajor-axis)
    (setf (longitude GEOS) (radians longitude))
    (setf (semimajor-axis GEOS) semimajor-axis)
    (setf (period GEOS) (/ period 86400))
    (setf (object-speed GEOS) (/ (* 2pi semimajor-axis) period))
    (setf (position-in-earth-frame GEOS) 
          (3vect-spherical-to-cartesian (list longitude 0)))
    t)
   (t ;; some args missing
    (error "All keyword arguments are required"))))


(defmethod SHOW ((geostationary-satellite geostationary-satellite))
  (format t "~%Geostationary satellite: ~a" 
          (name geostationary-satellite))
  (format t "~%  Longitude: ~a deg" 
          (degrees (longitude geostationary-satellite)))
  (format t "~%  Period: ~a s" 
          (* (period geostationary-satellite) 86400))
  (format t "~%  Speed: ~a km/s" 
          (object-speed geostationary-satellite))
  (format t "~%  Semimajor axis: ~a km" 
          (semimajor-axis geostationary-satellite))
  (format t "~%  Position in earth frame: ~a" 
          (position-in-earth-frame geostationary-satellite))
  (format t "~%  Time of last position: JD ~a" 
          (time-of-position geostationary-satellite))
  (format t "~%  Position: ~a" 
          (last-position-vector geostationary-satellite))
  (format t "~%  RA,Dec.: ~a deg"
          (if (last-position-vector geostationary-satellite)
            (mapcar #'degrees
                    (3vect-cartesian-to-spherical 
                     (last-position-vector geostationary-satellite))))))


(defmethod POSITION-VECTOR ((geostationary-satellite geostationary-satellite) jd)
  
  ;; if time known and same as previous call 
  (if (and (time-of-position geostationary-satellite)
           (= (time-of-position geostationary-satellite) jd))
    
    ;; then return same position
    (last-position-vector geostationary-satellite)
    
    ;; else calculate
    (let ((e (earth-orientation jd)))		; earth orientation matrix
      (setf (last-position-vector geostationary-satellite)
            (3vect-linear-comb 
             (cos (longitude geostationary-satellite)) (first e)
             (sin (longitude geostationary-satellite)) (second e)))
      (setf (time-of-position geostationary-satellite) jd)
      (last-position-vector geostationary-satellite))))


(defmethod DISTANCE ((geostationary-satellite geostationary-satellite) jd 
                     &optional (print-steps nil))
  "returns distance from earth center in km"
  (declare (ignore jd print-steps))
  (semimajor-axis geostationary-satellite))

;;; no velocity-vector method for this class

(defmethod VELOCITY ((geostationary-satellite geostationary-satellite) jd)
  "return speed in km/s"
  (declare (ignore jd))
  (object-speed geostationary-satellite))

;;; ======================================================================
;;;                                      CLASS:  FIXED-ASTRONOMICAL-OBJECT
;;;
;;; i.e. can be described by a position and geocentric distance only
;;; targets (actually nonmoving targets)


(defclass FIXED-ASTRONOMICAL-OBJECT (astronomical-object)
  ((fixed-position-vector
       :initform  nil
       :initarg  :fixed-position-vector
       :accessor  fixed-position-vector)	; unit vector
   ;; position dependent slots
   ;; the following are set by setting the position vector
   (ra
       :initform  nil
       :initarg  :ra
       :accessor  ra)				;RA, radians
   (dec
       :initform  nil
       :initarg  :dec
       :accessor  dec)				;dec, radians
   (ecliptic-position-vector
       :initform  nil
       :initarg  :ecliptic-position-vector
       :accessor  ecliptic-position-vector)	;ecliptic unit vect
   (lambda-value
       :initform  nil
       :initarg  :lambda-value
       :accessor  lambda-value)			;ecliptic longitude, radians
   (beta
       :initform  nil
       :initarg  :beta
       :accessor  beta)				;ecliptic latitute, radians
   (pa-nep
       :initform  nil
       :initarg  :pa-nep
       :accessor  pa-nep)			;PA of north ecliptic pole
   ;; end of position dependent slots
   (geocentric-distance
       :initform  nil
       :initarg  :geocentric-distance
       :accessor  geocentric-distance)		; nil if at infinity
						; else geocentric distance in km
   ))



(defun MAKE-FIXED-ASTRONOMICAL-OBJECT (n &key ra dec position-vector distance)
  
  "Make an instance of the fixed-astronomical-object class.  Required arguments:
    n - name string
    Either :ra and :dec in degrees, or :position-vector unit 3vector
    Optional:
    :distance in km (if missing, target assumed at infinity)"
  
  (let ((instance (make-instance 'fixed-astronomical-object)))
    (setf (name instance) n)
    (cond
     ((and ra dec)
      (reset-fixed-position 
       instance
       (3vect-spherical-to-cartesian (list (radians ra) (radians dec)))))
     (position-vector (reset-fixed-position instance position-vector))
     (t (error "Either :RA & :DEC or :POSITION-VECTOR required for ~a" n)))
    (setf (geocentric-distance instance) distance)
    instance))


(defmethod RESET-FIXED-POSITION ((FAO fixed-astronomical-object) 
                                 celestial-3vect 
                                 &key
                                 (obliquity *default-ecliptic-obliquity*))
  
  "Input position vector, set all dependent fields"
  
  (let* ((alpha-delta (3vect-cartesian-to-spherical celestial-3vect))
         (ecliptic-3vect 
          (celestial-to-ecliptic celestial-3vect :obliquity obliquity))
         (lambda-beta (3vect-cartesian-to-spherical ecliptic-3vect))
         (PA-pole
          (PA-of-north-ecliptic-pole 
           (first alpha-delta) (second alpha-delta)
           (second lambda-beta) :obliquity obliquity)))
    ;; set the instance variables
    (setf (fixed-position-vector FAO)    celestial-3vect
          (ra FAO)                       (first alpha-delta)
          (dec FAO)                      (second alpha-delta)
          (ecliptic-position-vector FAO) ecliptic-3vect
          (lambda-value FAO)             (first lambda-beta)
          (beta FAO)                     (second lambda-beta)
          (PA-nep FAO)                   PA-pole)
    t))


(defmethod SHOW ((fixed-astronomical-object fixed-astronomical-object))

  (format t "~%Target: ~a" 
          (name fixed-astronomical-object))
  (format t "~%  Position: ~a" 
          (fixed-position-vector fixed-astronomical-object))
  (format t "~%  RA,Dec: ~a, ~a deg" 
          (degrees (ra fixed-astronomical-object))
          (degrees (dec fixed-astronomical-object)))
  (format t "~%  Distance: ~a km"
          (if (geocentric-distance fixed-astronomical-object)
            (geocentric-distance fixed-astronomical-object)
            "(infinity)")))


(defmethod PRINT-OBJECT ((fixed-astronomical-object fixed-astronomical-object) 
                         stream)
  (format stream "#<~a ~a ~a>" 
          (type-of fixed-astronomical-object)
          (name fixed-astronomical-object) 
          (fixed-position-vector fixed-astronomical-object)))


(defmethod POSITION-VECTOR ((fixed-astronomical-object fixed-astronomical-object) jd)
  (declare (ignore jd))
  (fixed-position-vector fixed-astronomical-object))


(defmethod VELOCITY ((fixed-astronomical-object fixed-astronomical-object) jd)
  (declare (ignore jd))
  0.0)

(defmethod VELOCITY-VECTOR ((fixed-astronomical-object fixed-astronomical-object) jd)
  (declare (ignore jd))
  '(0 0 0))


(defmethod DISTANCE ((fixed-astronomical-object fixed-astronomical-object) jd 
                     &optional (print-steps nil))
  (declare (ignore jd print-steps))
  (geocentric-distance fixed-astronomical-object))


;;; ======================================================================
;;;                                      CLASS: MOVING-ASTRONOMICAL-OBJECT



(defclass MOVING-ASTRONOMICAL-OBJECT (astronomical-object)
  (
   (position-data
    :initform nil :initarg :position-data :accessor position-data
    :documentation 
    "A pointer to a structure which contains the epehemeris list for the target. 
      The ephemeris list is not included directly because they can become 
      quite large." )
   (visibility-pcf
    :initform nil :initarg :visibility-pcf :accessor visibility-pcf
    :documentation
    "A pcf giving target visibility for the moving target")
   )
  (:documentation
   "A structure for moving objects"))


(defclass EPHEMERIS-LIST ()
  (
   (ephemeris-list
    :initform nil :initarg :ephemeris-list :accessor ephemeris-list
    :documentation
    "A list of ephemeris data where each element contains three key word 
       argument pairs for time, position-vector, and velocity-vector")
   (ephemeris-write-date
    :initform nil :initarg :ephemeris-write-date :accessor ephemeris-write-date
    :documentation
    "The write date of the ephemeris file used to construct the object. 
       Used to determine cache hits")
   (ephemeris-start-date
    :initform nil :initarg :ephemeris-start-date :accessor ephemeris-start-date
    :documentation
    "The earliest time point for which ephemeris data is available")
   (ephemeris-end-date
    :initform nil :initarg :ephemeris-end-date :accessor ephemeris-end-date
    :documentation
    "the latest time for which ephemeris data is available")
   (target-id
    :initform nil :initarg  :target-id :accessor target-id
    :documentation
    "The id of the target")
   )
  (:documentation 
   "An object which stores the ephemeris list for a moving astronomical object. 
     That is this object is attached to a moving-astronomical-object.  
     One could ask why the ephemeris list itself is not directly attached
     to the moving-astronomical-object. The answer is one of readability. 
     Potentially ephemeris lists can be very long.  If they were attached 
     directly to the moving object then they could over flow screens when 
     inspecting moving target objects."))


(defmethod PRINT-OBJECT ((ephemeris-list ephemeris-list) stream)
  (format stream "#<Ephemeris-list ~A>" (target-id ephemeris-list)))


(defun INTERPOLATE-POSITION-BETWEEN-EPHEMERIS-DATA 
       (position1 time1 position2 time2 time)
  
  "Returns a position vector which represents the position of an object at 
    time given that the position of the object at time1 is position1 and 
    that the position of the object at time2 is position2.  All times are 
    given as julian dates and all positions are given as normalized 3 vectors. 
    The procedure checks to ensure that time is between time1 and time2"
  
  (cond ((or (< time time1) (> time time2))
         (format t "~%Cannot interpolate: time ~s is not in between ~s ~s~%" 
                 time time1 time2))
        (t
         (let* ((pole (3vect-cross-product position1 position2))
                (angle (3vect-separation position1 position2))
                (percent (/ (- time time1) (- time2 time1))))
           (3vect-rotate-about-pole position1 pole (* angle percent))))))


(defun EPHEMERIS-POINTS-NEAREST-TIME (ephemeris-list time)
  
  "Given a list of time position pairs find the time points nearest the time.  
    The function returns a single ephemeris point if it is fuzzy equal to 
    an element on the ephemeris-list.  The function returns nil if the time 
    is either before the first element on the ephemeris-list or after the 
    last time on the ephemeris-list. The time is given as a julian date. Each
    element of an ephemeris list is a list of keyword pairs with keys for 
    time position-vector and velocity vector. The velocity vector is 
    currently not used."
  
  (let ((previous (car ephemeris-list))
        (remainder-list (cdr ephemeris-list)))
    (cond ((absolute-fuzzy-equal time (getf previous :time))
           (list previous))
          ((< time (getf previous :time))
           nil)
          (t
           (dolist (current remainder-list)
             (cond ((absolute-fuzzy-equal time (getf current :time))
                    (return-from ephemeris-points-nearest-time (list current)))
                   ((and (> time (getf previous :time))
                         (< time (getf current :time)))
                    (return-from ephemeris-points-nearest-time 
                      (list previous current))))
             (setf previous current))
           nil))))


(defmethod POSITION-VECTOR ((moving-astronomical-object moving-astronomical-object) jd)
  
  (let* ((position-data (position-data moving-astronomical-object))
	 (ephemeris-list (ephemeris-list position-data))
         (nearest-points (ephemeris-points-nearest-time ephemeris-list jd)))
    (cond 
     
     ((equal (list-length nearest-points) 2) ; interpolate between the points
      (let ((position-before-jd (car nearest-points))
            (position-after-jd (cadr nearest-points)))
        (cond ((absolute-fuzzy-equal (getf position-before-jd :position-vector)
                                     (getf position-after-jd :position-vector))
               (getf position-before-jd :position-vector))
              (t
               (interpolate-position-between-ephemeris-data 
                (getf position-before-jd :position-vector)
                (getf position-before-jd :time)
                (getf position-after-jd :position-vector)
                (getf position-after-jd :time)
                jd)))))
     
     ((equal (list-length nearest-points) 1)
      (getf (car nearest-points) :position-vector))
     
     (t 
      (cond ((and (< jd (ephemeris-start-date position-data))
		  (> 2 (- (ephemeris-start-date position-data) jd)))
	     (getf (car ephemeris-list) :position-vector))
	    ((and (> jd (ephemeris-end-date position-data))
		  (> 2 (- jd (ephemeris-end-date position-data))))
	     (getf (car (last ephemeris-list)) :position-vector))
	    (t
	     (format t "~%Unable to determine position of ~s" 
		     (name moving-astronomical-object))
	     (format t "~%NO EPHEMERIS data present for time ~s~%" jd)
	     (format t "~%RETURNING BOGUS POSITION (1 1 1)~%") 
	     '(1 1 1)))))))


(defmethod DISTANCE ((moving-astronomical-object moving-astronomical-object) 
                     jd &optional (print-steps nil))
  
  "Distance in km of astronomical-object from earth center"
  
  (declare (ignore jd print-steps))
  nil)


(defmethod VELOCITY-VECTOR 
  ((moving-astronomical-object moving-astronomical-object) jd)
  
  "Unit geocentric velocity vector of astronomical object"
  
  (declare (ignore jd))
  nil)


(defmethod VELOCITY 
  ((moving-astronomical-object moving-astronomical-object) jd)
  
  "Speed of object in km/sec"
  
  (declare (ignore jd))
  nil)

;;; ======================================================================
;;;                                                    CLASS:  OBSERVATORY
;;; ground-based
;;;


(defclass OBSERVATORY (astronomical-object)
  ;; We consider an observatory an astronomical object
  ;; note, no writer functions. You can't move an observatory after its created!!
  
  ((name     :reader name :initarg :name
             :documentation "the name, a string")
   (latitude :reader latitude :initarg :latitude
             :documentation "geographic latitutde in radians, -pi/2 to pi/2")
   (longitude :reader longitude :initarg :longitude
              :documentation "geographic longitude in radians, 
              0 to 2pi, measured EASTWARD")
   (altitude :reader altitude :initarg :altitude
             :documentation "altitude in meters above sea level")
   )
  (:documentation "A ground based observatory"))


(defmethod PRINT-OBJECT ((obj observatory) stream)
  (format stream "#<OBSERVATORY ~a long ~,2f lat ~,2f>"
          (name obj) (degrees (longitude obj)) (degrees (latitude obj))))


(defun MAKE-OBSERVATORY (name latitude longitude altitude)
  
  "Given input arguments, returns a ground based observatory
    name is a string, latitude and longitude are in decimal DEGREES,
    longitude is 0 to 360 measured EASTWARD, altitude is in METERS."
  
  ;; note, i put error checking of arguments in this make function
  ;; instead of specializations of initialize-instance :before and :after
  ;; because the doc string is more accessible to the user and the
  ;; error code is closer to the function that gets the parameters
  ;; (ie before and after specializations can be hard to find)
  
  (unless (stringp name)
    (error "Observatory name ~a illegal, must be a string" name))
  (when (> (abs latitude) 90)
    (error "latitude ~a illegal, must be between +/- 90 degrees" latitude))
  (when (or (< longitude 0) (> longitude 360))
    (error "longitude ~a illegal, must be between 0 and 360 degrees" longitude))
  (when (or (< altitude -100) (> altitude 10000))
    (error "altitude ~a illegal, must be between -100 and 10000 meters" 
           altitude))
  (make-instance 'observatory :name (string-upcase name) 
                 :latitude (radians latitude)
                 :longitude (radians longitude) 
                 :altitude altitude))

#| some observatories for testing:
 (setf *stsci* (make-observatory "stsci"  39.25  (- 360 76.5)  10))
 (setf *equator* (make-observatory "equatorial" 0 90 0)
 (setf *south-pole* (make-observatory "cold" -90 90 10))
|#


(defmethod SHOW ((OBS observatory))
  (format t "~%~a, a ground-based observatory" (name OBS))
  (format t "~% at east longitude ~,3f, latitude ~,3f"
          (degrees (longitude OBS)) 
          (degrees (latitude OBS)))
  (format t "~% ~a meters above sea level"
          (altitude OBS)))


(defmethod POSITION-VECTOR ((OBS observatory) jd)
  
  "Return the normalized earth-centered position vector for an observatory"
  
  ;; the Greenwich mean siderial time is the RA above Greenwich,
  ;;  add the longitude of your observatory to get the RA there
  ;; since observatory is fixed on earth's surface, the declination 
  ;;  of the position-vector
  ;; is always the latitude of the observatory.
  
  (3vect-spherical-to-cartesian 
   (list (+ (longitude obs) (gmst jd)) (latitude obs))))


#| A test case:
;; 4418 is 10 june at noon in greenwich; we are 5 hours earlier, 
;; thus our noon is 4418.208333

(let* (
       (zero-point (first (earth-orientation 4418.208333)))
       (rotated-by-long (3vect-rotate-about-pole zero-point '(0 0 1) (radians (- 360 76.5))))
       (lat-axis (3vect-cross-product rotated-by-long '(0 0 1)))
       (obs-position (3vect-rotate-about-pole rotated-by-long lat-axis (radians 39.25))))
  (print zero-point)
  (print rotated-by-long)
  (print lat-axis)
  (print obs-position)
  (print "")
  (print (position-ra-dec zero-point :units :degrees))
  (print (position-ra-dec rotated-by-long :units :degrees))
  (print (position-ra-dec lat-axis :units :degrees))
  (print (position-ra-dec obs-position :units :degrees))
  
  (values)
  )


(do ((jd 4414.5 (+ jd .02)))
    ((> jd 4415.5))
  (format t "~% ~,2f ~a ~a ~a" jd (format-abs-time jd)
          (position-vector *stsci* jd)
          (position-ra-dec (position-vector *stsci* jd) :units :degrees)))

|#

(defmethod DISTANCE ((OBS observatory) jd &optional (print-steps nil))
  "return distance in km from center of earth"
  (declare (ignore jd print-steps)
           (special *earth-radius*))
  ;altitude is in m, earth radius is in km
  (+ *earth-radius* (/ (altitude OBS) 1000)))


;;; no velocity-vector method for this class

(defmethod VELOCITY ((OBS observatory) jd)
  
  "Return speed in km/sec"
  
  ;speed is distance divided by time
  ;the distance travelled is the circumference of the earth at that latitude!
  ;is 86400 sec/day good enough??
  (let ((circumference (* 2 pi (* (distance OBS jd) (cos (latitude OBS))))))
    (/ circumference 86400)))

;;; ----------------------------------------------------------------------
;;; methods for airmass and zenith angle of targets seen from observatories
;;;

(defmethod ZENITH-ANGLE-OF-OBJECT ((OBS observatory) 
                                   (OBJECT astronomical-object) jd)
  (angle-from-zenith obs object jd :units :radians))


;;; often it is possible to work with the COSINE of the zenith angle
;;; and avoid multiple cos/acos calls and subsequent roundoff

(defmethod COS-ZENITH-ANGLE-OF-OBJECT ((OBS observatory) 
                                       (OBJECT astronomical-object) jd)
  (cos-angle-from-zenith obs object jd))


(defmethod AIRMASS-OF-OBJECT ((OBS observatory) 
                              (OBJECT astronomical-object) jd)
  (cos-zenith-angle-to-airmass (cos-zenith-angle-of-object obs object jd)))


(defun COS-ZENITH-ANGLE-TO-AIRMASS (cos &optional (max-airmass 10.0))
  
  "Given the cosine of a zenith angle, convert to airmass.
    max-airmass is returned for cosine <= min-cosine"
  
  ;;this function uses the relation airmass = 1/cosine, 
  ;; which comes from a plane-parallel atmosphere. 
  ;; this is fine for our purposes and the higher order angle terms
  ;; which result from a curved atmosphere can be ignored since
  ;; they are only important for large airmass
  
  ;; max airmass prevents dividing by zero at horizon
  
  (let ((min-cos (/ 1.0 max-airmass)))
    (if (<= cos min-cos)
      max-airmass
      (/ 1.0 cos))))

;(cos-zenith-angle-to-airmass (cos (radians 0)))
;(cos-zenith-angle-to-airmass (cos (radians 30)))
;(cos-zenith-angle-to-airmass (cos (radians 86)) 15)


(defun ZENITH-ANGLE-TO-AIRMASS (Z)
  
  "given the zenith angle in radians, convert to airmass"
  
  (cos-zenith-angle-to-airmass (cos z)))


(defmethod OBJECT-ALWAYS-BELOW-HORIZON ((OBS observatory) 
                                        (OBJECT fixed-astronomical-object))
  
  "Returns t if the object is never above the horizon
   and therefore can never be seen, nil otherwise"
  
  ;; simple model: assuming a 90 degree horizon, infinite object distance
  ;; and ignore any effects of altitude of observatory
  ;; then if latitude of obs and declination of object differ by
  ;; more than 90 degrees, its always below the horizon
  
  (if (>= (abs (- (latitude OBS) (dec OBJECT))) pi/2) t))


;;; --- observatory in day or night
;;; this gives the zenith angle of the sun, now all you need is something
;;; to turn angle into a twilight measure. <90 is daylight, >110? dark?

(defmethod ZENITH-ANGLE-OF-SUN ((OBS observatory) jd)
  (zenith-angle-of-object OBS *sun* jd))


#|
(utjd 7 6 1991) = 4414.5

(do ((jd 4414.5 (+ jd .01)))
    ((> jd 4415.5))
  (format t "~% ~,2f ~a ~,2f ~a ~a" 
	  jd (format-abs-time jd)
	  (degrees (zenith-angle-of-sun *stsci* jd))
	  (position-ra-dec (position-vector *stsci* jd) :units :degrees)
	  (position-ra-dec (position-vector *sun* jd) :units :degrees)))

(do ((jd 4414.5 (+ jd .01)))
    ((> jd 4415.5))
  (format t "~% ~,2f ~a ~a" 
	  jd (format-abs-time jd)
	  (position-ra-dec (position-vector *stsci* jd) :units :degrees)))

|#

;;; --- Moon
;;; for moon you want zenith angle and phase
;;; when moon's phase is near new, its suit is 1
;;; when moon's phase is not new, its suit depends on zenith angle

;(moon-phase *moon* 4144.5)

(defmethod ZENITH-ANGLE-OF-MOON ((OBS observatory) jd)
  (zenith-angle-of-object OBS *moon* jd))


;;; ======================================================================
;;; CLASS: GROUND-STATION
;;;
;;; subclass of observatory

(defclass GROUND-STATION (observatory)
  ( (uplink-horizon-angle 
     :initform 0 :initarg :uplink-horizon-angle :documentation
     "minimum angle above horizon, radians, for uplink from ground station
       to satellite")
    (cos-uplink-zenith-angle
     :initform 0 :initarg :cos-uplink-zenith-angle :documentation
     "cos (90 - uplink-horizon-angle)")
    (downlink-horizon-angle 
     :initform 0 :initarg :downlink-horizon-angle :documentation
     "minimum angle above horizon, radians, for downlink from satellite
       to ground-station")
    (cos-downlink-zenith-angle
     :initform 0 :initarg :cos-downlink-zenith-angle :documentation
     "cos (90 - downlink-horizon-angle)")
    )
  (:documentation "Satellite ground station"))


(defmethod PRINT-OBJECT ((obj GROUND-STATION) stream)
  (format stream "#<GROUND-STATION ~a long ~,2f lat ~,2f>"
          (name obj) (degrees (longitude obj)) (degrees (latitude obj))))


(defun MAKE-GROUND-STATION (name latitude longitude altitude
                                 &key 
                                 (uplink-horizon-angle 0)
                                 (downlink-horizon-angle 0))
  
  "Given input arguments, returns a ground station instance.
    name is a string, latitude and longitude are in decimal DEGREES,
    longitude is 0 to 360 measured EASTWARD, altitude is in METERS.
    Uplink and downlink limit angles are degrees from horizon."
  
  (unless (stringp name)
    (error "Observatory name ~a illegal, must be a string" name))
  (when (> (abs latitude) 90)
    (error "latitude ~a illegal, must be between +/- 90 degrees" latitude))
  (when (or (< longitude 0) (> longitude 360))
    (error "longitude ~a illegal, must be between 0 and 360 degrees" longitude))
  (when (or (< altitude -100) (> altitude 10000))
    (error "altitude ~a illegal, must be between -100 and 10000 meters" 
           altitude))
  (when (or (< uplink-horizon-angle 0) (> uplink-horizon-angle 90))
    (error "uplink-horizon-angle must be between 0 and 90 degrees"))
  (when (or (< downlink-horizon-angle 0) (> downlink-horizon-angle 90))
    (error "downlink-horizon-angle must be between 0 and 90 degrees"))
  
  (make-instance 
   'ground-station
   :name (string-upcase name) 
   :latitude (radians latitude)
   :longitude (radians longitude) 
   :altitude altitude
   :uplink-horizon-angle (radians uplink-horizon-angle)
   :cos-uplink-zenith-angle (cos (radians (- 90 uplink-horizon-angle)))
   :downlink-horizon-angle (radians downlink-horizon-angle)
   :cos-downlink-zenith-angle (cos (radians (- 90 downlink-horizon-angle)))))


(defmethod SHOW ((obj ground-station))
  (format t "~%~a, a ground station" (name obj))
  (format t "~% at east longitude ~,3f, latitude ~,3f"
          (degrees (longitude obj)) 
          (degrees (latitude obj)))
  (format t "~% ~a meters above sea level"
          (altitude obj))
  (format t "~% horizon angle for uplink ~,3fdeg, downlink: ~,3fdeg"
          (degrees (slot-value obj 'uplink-horizon-angle))
          (degrees (slot-value obj 'downlink-horizon-angle))))


;;; ======================================================================
;;;               INTERPOLATION IN ENTRY/EXIT TABLE VS. ASC NODE LONGITUDE


(defun INTERPOLATE-ENTRY-EXIT-TABLE (longitude 
                                     &key (table nil)
                                     (debug nil))
  "Interpolate in entry/exit table to find crossing times for
    a satellite on any given orbit.  Given longitude of satellite asc node 
    (degrees), return nil if no crossing, else 4 multiple values:
      start-offset - in days from asc node time
      end-offset - in days from asc node time
      (long lat) of entry (degrees)
      (long lat) of exit (degrees)
    Format of table is list of records, each record is:
    (long entry entry-long entry-lat exit exit-long exit-lat)
    where:
     long - longitude asc node of orbit, dec degrees
     entry - time of entry in days from asc node time
     entry-long - longitude of entry, dec deg
     entry-lat - latitude of entry, dec deg
     exit - time of exit
     exit-long - longitude of entry, dec deg
     entry-lat - latitude of entry, dec deg.
    Note that the last six entries (entry through exit-lat) will be nil
    when there is no crossing in the orbit.
    Table must be in increasing longitude order, but transition from
    360->0 is ok in the middle.  Interpolation will wrap around and
    interpolate between first and last entries in table, so it doesn't
    matter what longitude the table starts at."
  
  ;; do* cyclically binds entries in list.  E.g. if list was
  ;; '(1 2 3 4 5) this do* would bind rec1 and rec2 successively to:
  ;; (5 1) (1 2) (2 3) (3 4) (4 5)
  (do* ((todo table (rest todo))
        (rec1 (first (last todo)) rec2)
        (rec2 (first todo) (first todo))
        )
       ((or (null rec2) (null todo)) nil)

    (let ((long1 (first rec1))
          (long2 (first rec2))   ; longitudes in DEGREES here
          (alpha nil)                   ; ranges from 0 to 1, for interpolation
                                        ; between first and second point
                                        ; alpha=0 -> first, alpha=1 -> second
          )
      (cond ((or (null (second rec1)) (null (second rec2)))
             ;; no interpolation: one of recs has no entry/exit data
             )
            ((and (>= long2 long1) (>= longitude long1) (<= longitude long2))
             ;; normal case (increasing angle)
             (setf alpha (/ (- longitude long1) (- long2 long1))))
            ((<= long2 long1)           ; long1 < 360, long2 > 0
             (cond ((and (>= longitude long1) (<= longitude 360))
                    (setf alpha (/ (- longitude long1)
                                   (- (+ long2 360) long1))))
                   ((and (<= longitude long2) (>= longitude 0))
                    (setf alpha (/ (- (+ longitude 360) long1)
                                   (- (+ long2 360) long1)))))))
      ;; if alpha non-nil then interpolate
      (when alpha
        (when debug
          (format t "~%alpha: ~,3f  long1: ~,3f long2: ~,3f longitude: ~,3f"
                  alpha long1 long2 longitude)
          (format t "~%~a ~%~a" rec1 rec2))
        (let ((entry1 (second rec1))
              (entry2 (second rec2))
              (exit1 (fifth rec1))
              (exit2 (fifth rec2))
              (entry-vect1 (ra-dec-to-cartesian-3vect
                            (list (third rec1) (fourth rec1))
                            :units :degrees))
              (entry-vect2 (ra-dec-to-cartesian-3vect
                            (list (third rec2) (fourth rec2))
                            :units :degrees))
              (exit-vect1 (ra-dec-to-cartesian-3vect
                           (list (sixth rec1) (seventh rec1))
                           :units :degrees))
              (exit-vect2 (ra-dec-to-cartesian-3vect
                           (list (sixth rec2) (seventh rec2))
                           :units :degrees)))
          ;; interpolate
        (return (values
                 (+ entry1 (* alpha (- entry2 entry1)))
                 (+ exit1  (* alpha (- exit2  exit1 )))
                 (position-ra-dec
                  (3vect-normalize
                   (3vect-add entry-vect1
                    (3vect-times-scalar alpha 
                     (3vect-subtract entry-vect2 entry-vect1))))
                  :units :degrees)
                 (position-ra-dec
                  (3vect-normalize
                   (3vect-add exit-vect1
                    (3vect-times-scalar alpha 
                     (3vect-subtract exit-vect2 exit-vect1))))
                  :units :degrees)
                 )))))))


;;; ======================================================================
;;;                                                       SPECIFIC OBJECTS

(setf *SUN* (make-instance 'sun-class))

(setf *MOON* (make-instance 'moon-class))

(setf *ST*
      (make-low-earth-orbit-satellite 
       "HST"                                 ;name
       28.5                                  ;inclination, deg
       2135.520145833                        ;time asc node, TJD
       64.906                                ;RA asc node, deg
       -6.7046                               ;nodal regression rate, deg/day
       94.42896                              ;period, minutes
       6878.165                              ;semimajor axis, km
       ))
(make-sensor 
 :name :FHST1
 :satellite *ST* 
 :boresight '( 0.0006394751  0.0057162757 -0.99998346)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))
(make-sensor 
 :name :FHST2
 :satellite *ST* 
 :boresight '(-0.6549442800 -0.3742016600 -0.65652198)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))
(make-sensor 
 :name :FHST3
 :satellite *ST* 
 :boresight '(-0.6555800300  0.3806178100 -0.65218472)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))
(add-named-vector *ST* :V3 '(0 0 1))

(setf *TDRSE* 
      (make-geostationary-satellite 
       "TDRS-East"                           ;name
       -41.0                                 ;longitude, deg.
       86164.09054                           ;period, sec.
       42164.69))                            ;semimajor axis, km

(setf *TDRSW* 
      (make-geostationary-satellite 
       "TDRS-West"                           ;name
       -171.0                                ;longitude, deg
       86164.09054                           ;period, sec
       42164.69))                            ;semimajor axis, km


#| ===========
This is what an orbit.model for HST file should contain:

(setf *current-orbit-model*    "TEST"
      *orbit-model-start-date*  (dmy-to-time '1-jan-90)
      *orbit-model-end-date*    (dmy-to-time '1-jan-95)
      )
  
(update-orbit-parameters
 *ST*
 :inclination       28.5                      ;deg
 :time-asc-node     (dmy-to-time '1-jan-90)   ;TJD or date
 :RA-asc-node       0                         ;deg
 :regression-rate   -6.4                      ;deg/day
 :period            96.58                     ;minutes
 :semimajor-axis    (+ 6378.14 600)           ;km
 )

(update-orbit-parameters
 *TDRSE* 
 :longitude         -41.0                    ;deg
 :period            86164.09054              ;sec
 :semimajor-axis    42164.69                 ;km
 )

(update-orbit-parameters
 *TDRSW*
 :longitude         -171.0                   ;deg
 :period            86164.09054              ;sec
 :semimajor-axis    42164.69                 ;km
 )


==============|#


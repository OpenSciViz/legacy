;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; xte-csp.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: xte-csp.lisp,v $
;;; Revision 4.11  1995/10/19 14:06:19  buehler
;;; Reinstate guarding max function to prevent slews before start of STS.
;;;
;;; Revision 4.10  1995/10/16  23:00:00  buehler
;;; Default values of XTE-REL-ADJUST, XTE-ABS-ADJUST now set to 0,
;;; Oversubscription factor default now set to .15.
;;;
;;; Revision 4.9  1995/08/03  03:44:52  buehler
;;; Include slot for roll angle in tasks; give it default initialization of 0.0
;;; and output it in the .positions file.
;;;
;;; Revision 4.8  1995/04/24  14:42:57  buehler
;;; Enforce minimum setup time for slews. Output roster TJDs and
;;; positions file in non-d0 format for both Lisp 4.1 and 4.2.
;;;
;;; Revision 4.7  1995/03/27  22:37:53  buehler
;;; Insert carriage return at end of .positions file..
;;;
;;; Revision 4.6  1995/03/11  14:16:08  buehler
;;; Use global to get the ephemeris file name to write to event roster.
;;;
;;; Revision 4.5  1995/02/11  21:36:03  buehler
;;; Do not write occult event to roster if it is later than end of observation.
;;;
;;; Revision 4.4  1995/02/11  02:31:28  buehler
;;; Sort task list chronologically before writing out occult lists.
;;;
;;; Revision 4.3  1995/02/10  16:50:19  buehler
;;; Correct bug in which name of first target was not appearing in roster.
;;; Adjust occult list so only occult events for current target are reported.
;;;
;;; Revision 4.2  1995/02/03  23:29:55  buehler
;;; Comment out a number of debugging displays in event roster code.
;;;
;;; Revision 4.1  1995/01/19  19:23:11  buehler
;;; Commented-out displays sprinkled through SAVE-EVENT-ROSTER for
;;; future debugging use.
;;;
;;; Revision 4.0  1995/01/09  14:29:54  buehler
;;; Check for case when arc cosine of value very close to 1 gives a complex.
;;; This caused blowups when calculating 0 slew during optimization.
;;;
;;; Revision 3.9  1994/11/29  15:15:57  buehler
;;; Strike punctuation from .positions file output.
;;;
;;; Revision 3.8  1994/11/28  22:51:59  buehler
;;; Incorporates telemetry constraints, plus interrupt-not and orbit-night,
;;; from Mark Johnston.
;;;
;;; Revision 3.4  1994/09/21  18:59:55  buehler
;;; Add occultations to event file; output .positions file on save.
;;;
;;; Revision 3.3  1994/09/17  22:23:13  buehler
;;; Code to write event roster transferred here from climif-xte.
;;;
;;; Revision 3.2  1994/09/11  16:11:23  buehler
;;; Added telemetry to keys allowed by DEF-XTE-EXPOSURE. However,
;;; the program ignores the additional key.
;;;
;;; Revision 3.1  1994/05/30  14:17:50  buehler
;;; Added functions to save observation starts and SAA transitions
;;; in an event roster file ( <name>.events on /sched directory).
;;;
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;;
;;; Revision 1.16  1994/02/12  16:00:00  buehler
;;; Modified translation of root to targ and constraint file names, so
;;; that name derives solely from name of target, not from sched file.
;;;
;;; Revision 1.15  1992/12/01  12:59:00  buehler
;;; fixed bug in which schedule-report bombed whenever bin was empty.
;;;
;;; Revision 1.14  1992/10/07  12:27:19  johnston
;;; fixed to convert / to - in target names to prevent
;;; .prep file errors in Unix
;;;
;;; Revision 1.13  1992/10/06  11:00:35  johnston
;;; changed prepping so that each target in a .targ file gets its
;;; own .prep file.
;;;
;;; Revision 1.12  1992/10/05  19:57:22  johnston
;;; fixed bug where efficiency must be zero where absolute
;;; constraint intervals are zero.
;;;
;;; Revision 1.11  1992/10/05  12:40:04  johnston
;;; consistency with MIT version
;;;
;;; Revision 1.10  1992/10/04  12:53:53  johnston
;;; Merged numerous changed from MIT and ScI
;;;
;;; Revision 1.9  1992/04/22  16:53:06  johnston
;;; Added BDR capacity constraint stubs, other major changes
;;;
;;; Revision 1.8  1991/12/01  00:14:29  johnston
;;; rework postscript
;;;
;;; Revision 1.7  1991/11/26  15:34:48  johnston
;;; major upgrade
;;;
;;; Revision 1.6  91/06/17  22:30:03  johnston
;;; fixed paren missing
;;; 
;;; Revision 1.5  91/06/17  22:20:24  johnston
;;; extended short-term schedule report to optimize schedule and print results
;;; 
;;; Revision 1.4  91/06/11  12:19:18  johnston
;;; minor changes
;;; 
;;; Revision 1.3  91/06/06  00:58:12  johnston
;;; more plot changes
;;; 
;;; Revision 1.2  91/06/06  00:22:24  johnston
;;; fixes to plots, but still some problems
;;; 
;;; Revision 1.1  91/06/05  23:02:07  johnston
;;; Initial revision
;;; 
;;; 

;;; ----------------------------------------------------------------------
;;; Design Notes: 
#| 

It is assumed in this file that long- and short-term tasks are mapped
one-to-one, and that all have IDs (tasks) or NAMEs (vars) that are
lisp symbols.

With this assumption, it is possible to store long-term and short-term
commitment values (times) on the symbol plist.  This is done below via
assign-value and unassign-value :after methods for XTE-VARS and
XTE-SHORT-TERM-VARS, and also for RE-INITIALIZE-VARIABLE.


These committed times are then referenced to save the schedule, and
set when the schedule is restored.


Specifically:

- there are two times stored on the symbol plist for the task.
long-term and short-term, both in TJD.

- when a long term task is created, clear the commit times (both)

- after a long-term schedule has been built, restore the commit-times
(both) to the symbol plists, and make the commitment in the long-term
schedule (this is part of restoring a saved schedule)

- after a short-term schedule is built, loop over all vars in the
short term schedule.  If they have commit times in range, then
make them.  Otherwise clear the commit times.

- when a long-term var is committed: store the long-term time.  No
effect on the short-term time.

- when a long-term-var is made uncommitted: set the long-term time to
nil (not committed). No effect on the short-term time.

- when a short-term var is committed: store the commit time for short
term, no change to long-term

- when a short-term var is uncommitted: same.

- when saving the schedule, loop over the vars in the long-term CSP
and save the long and short times to the schedule file

- provide a function which takes a short term schedule and fixes the
long term schedule to be consistent with it.



|#
;;; ----------------------------------------------------------------------


;;; -----------------------------------------------------------------------
;;;                                                                 GLOBALS
;;; -----------------------------------------------------------------------

;;; PARAMETER GLOBALS - CHANGE AS NEEDED

(defvar *XTE-SUITABILITY-SCALE* 100
  "Multiply suitability values (0 to 1) by this value and round
to get integral preference values")

(defvar *XTE-DEFAULT-PRIORITY* 100
  "Default priority value, numeric")


;;; default orbit model is *DEFAULT-XTE-ORBIT-MODEL*
;;; defined in xte-fast-event.lisp


;;; --- schedule times: initial values are good for testing

(defvar *XTE-SCHEDULE-START* nil
  "Time (TJD) of XTE schedule start")
(setf *XTE-SCHEDULE-START* (dmy-to-time '1-jan-1996))

(defvar *XTE-SCHEDULE-END* nil
  "Time (TJD) of XTE schedule end")
(setf *XTE-SCHEDULE-END*   (dmy-to-time '10-mar-1996))

(defvar *XTE-TIME-QUANTUM* nil
  "Time in days of XTE time discretization interval")
(setf *XTE-TIME-QUANTUM*   1)

;;; Next three parameters determine mapping from quantized time
;;; to "segments".  New for XTE, not present for EUVE

(defvar *XTE-SEGMENT-START* nil
  "Start time TJD of first segment.  Must be between *XTE-SCHEDULE-START*
and *XTE-SCHEDULE-END*")
(setf *XTE-SEGMENT-START* (dmy-to-time '1-jan-1996))

(defvar *XTE-SEGMENT-DURATION* nil
  "Uniform segment duration, days.  Must be >= 1 quantum in duration.
Actual duration will be rounded to nearest quantum.")
(setf *XTE-SEGMENT-DURATION* 7.0)

(defvar *XTE-SEGMENT-COUNT* nil
  "Total number of segments.  Must be set so that end of last segment is 
between *XTE-SCHEDULE-START* and *XTE-SCHEDULE-END*")
(setf *XTE-SEGMENT-COUNT* 10)

(defvar *XTE-CAPACITY-UNIT* nil
  "Time in days for discretization of time allocated to segments.  
e.g. 0.01 days")
(setf *XTE-CAPACITY-UNIT* 0.00388) ;335 sec.;

(defvar *XTE-OVERSUBSCRIPTION-FACTOR* 0
  "factor by which to oversubscribe bins.  E.g. 0 means fill exactly
to limit; 0.2 means 20% oversubscription, etc.")
(setf   *XTE-OVERSUBSCRIPTION-FACTOR* 0.15)


;;; used to record default file lists, and for saving/restoring schedule
(defvar *XTE-TARG-FILES* nil)
(defvar *XTE-CONSTRAINT-FILES* nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; globals referenced in other files
;;; the next lines are added 6-10-92
;;; this one is used in GET-ORIGINAL-TARGET-LIST in xte-functions.lisp
(defvar *original-target-list* nil)
(setf   *original-target-list* nil)

(defvar *short-term-segment-number* nil)
(setf   *short-term-segment-number* nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ----------------------------------------------------------------------
;;; PARAMETERS FOR SHORT-TERM SCHEDULING
;;; ----------------------------------------------------------------------

(defvar *XTE-SHORT-TERM-TIME-QUANTUM* nil
  "time quantum in days for short term scheduling. E.g. (/ 1 1440.0) = 1min")
(setf   *XTE-SHORT-TERM-TIME-QUANTUM* (/ 5.0 1440.0))

(defvar *XTE-SLEW-RATE* nil "in degrees/minute")
(setf   *XTE-SLEW-RATE* 12) ;Full circle in half an hour

(defvar *XTE-SLEW-SETUP-TIME* (/ 130 60)
  "time in minutes for changing between observations, over and above slewing")

(defvar *XTE-ABS-ADJUST* nil
  "time in seconds by which exposure time can be adjusted without impact")
(setf   *XTE-ABS-ADJUST* 0) ;600s = 10min

(defvar *XTE-REL-ADJUST* nil
  "fraction of exposure time which can be added/subtracted without impact")
(setf   *XTE-REL-ADJUST* 0.0) ;0.3 = +/- 30% of exp time can be changed

(defvar *XTE-MIN-SEGMENT-SIZE* nil
  "time in seconds of minimum exposure time segment on target, if exposure
must be interrupted")
(setf   *XTE-MIN-SEGMENT-SIZE* 900) ;900s = 15min min segment size

;;; ----------------------------------------------------------------------
;;; PARAMETERS FOR ONBOARD DATA CONSTRAINT
;;; ----------------------------------------------------------------------
;;; new mdj/nov94

(defvar *XTE-ONBOARD-CAPACITY*
  350000
  "onboard storage capacity, kilobits")
(setf *XTE-ONBOARD-CAPACITY* 350000)

(defvar *XTE-READOUT-RATE*
  20
  "rate at which data is continuously read out, kilobits/sec")
(setf *XTE-READOUT-RATE* 20)

;;; runout time (minutes) is:
;;; (/ (/ *XTE-ONBOARD-CAPACITY* *XTE-READOUT-RATE*) 60.0) ~ 292min = 4.8h
;;; in quanta: 
;;; 
(defvar *XTE-ONBOARD-ACCOUNTING-INTERVAL*
  36
  "size of bin over which onboard storage capacity is binned, in units 
    short-term of quanta (see *XTE-SHORT-TERM-TIME-QUANTUM*)")
(setf *XTE-ONBOARD-ACCOUNTING-INTERVAL* 36)


;;; ----------------------------------------------------------------------
;;;                                               CURRENT SCHEDULE OBJECTS
;;; ----------------------------------------------------------------------

;; both of these are bound to instances of type obs-csp-chronika
;; only one of each supported for now

(defvar *XTE-LONG-TERM-SCHEDULE* nil)

(defvar *XTE-SHORT-TERM-SCHEDULE* nil)

;;; ----------------------------------------------------------------------
;;;                                                           REPORT TIMES
;;; ----------------------------------------------------------------------


(defun REPORT-XTE-SCHEDULE-TIMES (&optional (stream t))
  "Report times to output stream as lisp comments"
  ;; (report-XTE-schedule-times)
  (format stream "~%;;; Schedule times:")
  (format stream "~%;;;  Start TJD ~8,3f (~a)"
	  *XTE-SCHEDULE-START* 
	  (format-abs-time *XTE-SCHEDULE-START*))
  (format stream "~%;;;  End   TJD ~8,3f (~a)"
	  *XTE-SCHEDULE-END*
	  (format-abs-time *XTE-SCHEDULE-END*))
  (format stream "~%;;;  One time unit (XTE quantum) is ~f days"
	  *XTE-TIME-QUANTUM*)
  (format stream "~%;;;  Start to end is ~,3f quanta"
	  (/ (- *xte-schedule-end* *xte-schedule-start*)
	     *xte-time-quantum*))
  )

(defun REPORT-XTE-SEGMENT-TIMES (&optional (stream t))
  "Report segment times to output stream as lisp comments"
  ;; (report-xte-segment-times)
  (format stream "~%;;; Segment times:")
  (format stream "~%;;;  Start segment:  TJD ~8,3f (~a)"
	  *XTE-SEGMENT-START*
	  (format-abs-time *XTE-SEGMENT-START*))
  (format stream "~%;;;  duration:           ~8,3f days"
	  *XTE-SEGMENT-DURATION*)
  (format stream "~%;;;  number of segments: ~4d"
	  *XTE-SEGMENT-COUNT*)
  )

;;; ----------------------------------------------------------------------
;;;                                            STORE TIMES ON TASK SYMBOLS
;;; ----------------------------------------------------------------------


(defun SET-LONG-TERM-COMMITMENT-TIME (symbol time)
  (setf (get symbol :LT) time))

(defun SET-SHORT-TERM-COMMITMENT-TIME (symbol time)
  (setf (get symbol :ST) time))

(defun LONG-TERM-COMMITMENT-TIME (symbol)
  (get symbol :LT))

(defun SHORT-TERM-COMMITMENT-TIME (symbol)
  (get symbol :ST))


;;; -----------------------------------------------------------------------
;;;                                     FUNCTIONS FOR PREPPING TARGET FILES
;;; -----------------------------------------------------------------------

;;; this is the form expected in the .targ files:

;;; mdj/nov94: added three new parameters to DEF-XTE-EXPOSURE:
;;;  telemetry non-interrupt shadow-only
(defmacro DEF-XTE-EXPOSURE (&key name ra dec exp-time priority 
                                 telemetry non-interrupt shadow-only)
  `(prep-XTE-target :name ,(string-it name)
		    :ra ,ra :dec ,dec :exp-time ,exp-time
		    ,@(if priority (list :priority priority))
                    ,@(if telemetry (list :telemetry telemetry))
                    ,@(if non-interrupt (list :non-interrupt non-interrupt))
                    ,@(if shadow-only (list :shadow-only shadow-only))
                    ))


(defun WRITE-XTE-PREP-FILE-HEADER (root targID stream)
  
  "filename is the name of the .targ file.  Write a header to the prep
file (stream) with times, defaults, etc."
  ;; (write-XTE-prep-file-header "foo" "targid" t)
  
  (format stream "~%;;; XTE prep file for ~a targID ~a" root targID)
  (format stream "~%;;; Created ~a" (get-formatted-universal-time))
  (format stream "~%;;; Default orbit model: ~a" *default-XTE-orbit-model*)
  (report-XTE-schedule-times stream))


;;; ----------------------------------------------------------------------
;;;                                           List available files by type
;;; ----------------------------------------------------------------------

(defun XTE-TARG-FILES ()
  "return list of all .targ files"
  ;; (XTE-targ-files)
  ;; now using logical pathnames:
  (let ((model (translate-logical-pathname "spike:obs;")))
    (reverse
     (mapcar
      #'pathname-name
      (directory 
       (make-pathname :directory (pathname-directory model)
		      :name :wild
		      :type "targ"))))))

;;; converted to no-op: now not easy to tell which are prepped and which
;;; aren't - MDJ 10/5/92

(defun XTE-PREPPED-TARG-FILES ()
  "return list of targ files which are prepped"
  ;; (xte-prepped-targ-files)
  nil)
#| formerly:
(let ((result nil))
  (dolist (targ (xte-targ-files))
	  (if (not (xte-targ-file-needs-to-be-prepped targ))
	      (push targ result)))
  (nreverse result))
|#


(defun XTE-CONSTRAINT-FILES ()
  "return list of all .constr files"
  ;; (XTE-constraint-files)
  (let ((model (translate-logical-pathname "spike:obs;")))
    (reverse
     (mapcar
      #'pathname-name
      (directory 
       (make-pathname :directory (pathname-directory model)
		      :name :wild
		      :type "constr"))))))


(defun XTE-SCHED-FILES ()
  "return list of all .sched files"
  ;; (XTE-sched-files)
  (let ((model (translate-logical-pathname "spike:sched;")))
    (reverse
     (mapcar
      #'pathname-name
      (directory 
       (make-pathname :directory (pathname-directory model)
		      :name :wild
		      :type "sched"))))))



;;; ----------------------------------------------------------------------
;;;                                      Translate root names to pathnames
;;; ----------------------------------------------------------------------

(defun canonicalize-target-ID-string (targID)
  "convert target ID into string suitable for naming files"
  ;; (canonicalize-target-ID-string 'abc/def) -> "abc-def"
  (let ((str (string-it-downcase targID)))
    (if (position #\/ str)
	(setf str (substitute #\- #\/ str)))
    str))

;;; Added reb 5/27/94
(defun XTE-ROOT-TO-EXT (root ext)
  "input root, extension name, return full pathname of .<ext> file"
  ;; (XTE-root-to-ext "XTE-test" "dat")
  (let ((model (translate-logical-pathname "spike:sched;")))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (string-it-downcase root)
		   :type ext)))

(defun XTE-ROOT-TO-TARG (root)
  "input root, return full pathname of .targ file"
  ;; (XTE-root-to-targ "XTE-test")
  (let ((model (translate-logical-pathname "spike:obs;")))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (string-it-downcase root)
		   :type "targ")))

(defun XTE-ROOT-TO-PREP (root targID)
  "input root, return full pathname of .prep file"
  ;; (XTE-root-to-prep "XTE-test" "foo")
  (let ((model (translate-logical-pathname "spike:prep;"))
	(tstr (canonicalize-target-ID-string targID)))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (format nil "~a"  tstr)
		   :type "prep")))

(defun XTE-ROOT-TO-PREPC (root targID)
  "input root, return full pathname of .prepc file"
  ;; (XTE-root-to-prepc "XTE-test" "foo")
  (let ((model (translate-logical-pathname "spike:prep;"))
	(tstr (canonicalize-target-ID-string targID)))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (format nil "~a" tstr)
		   :type "prepc")))

(defun XTE-ROOT-TO-CONSTR (root)
  "input root, return full pathname of .constr file"
  ;; (XTE-root-to-constr "XTE-test")
  (let ((model (translate-logical-pathname "spike:obs;")))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (string-it-downcase root)
		   :type "constr")))

(defun XTE-ROOT-TO-SCHED (root)
  "input root, return full pathname of .sched file"
  ;; (XTE-ROOT-TO-SCHED "foo")
  (let ((model (translate-logical-pathname "spike:sched;")))
    (make-pathname :host (pathname-host model)
		   :device (pathname-device model)
		   :directory (pathname-directory model)
		   :name (string-it-downcase root)
		   :type "sched")))

(defun XTE-PATHNAME-TO-ROOT (pathname)
  "input pathname, convert to root.  If root is input, 
return as is.  Root is just the pathname name"
  ;; (xte-pathname-to-root #p"Macintosh HD:spike:obs:xte-test1.constr")
  ;; (xte-pathname-to-root #p"/grendel/data1/jul92/obs/xte-test1.constr")
  (cond ((pathnamep pathname)
	 (pathname-name pathname))
	(t pathname)))

;; (mapcar 'xte-pathname-to-root (constraint-files *xte-long-term-schedule*))


;;; ----------------------------------------------------------------------
;;;                                     Operations on targ/prep etc. files
;;; ----------------------------------------------------------------------

;;; this function now always returns t

(defun XTE-TARG-FILE-NEEDS-TO-BE-PREPPED (root)
  
  "return non-nil if prep file doesn't exist, or is dated earlier than
corresponding target file"
  ;; (xte-targ-file-needs-to-be-prepped "xte-test")
  ;; (xte-targ-file-needs-to-be-prepped 'xte-test)
  
  (declare (ignore root))
  t)


(defun XTE-PREP-FILE-NEEDS-TO-BE-COMPILED (root targID)
  
  "return non-nil if prepc file doesn't exist, or is dated earlier than
corresponding prep file"
  ;; (XTE-prep-file-needs-to-be-compiled "XTE-test" "u-cep")
  
  (let ((prep (XTE-root-to-prep root targID))
	(prepc (XTE-root-to-prepc root targID)))
    (if (not (probe-file prep))
	(error ".prep file ~a does not exist" prep))
    (let* ((prepc-file-exists (probe-file prepc))
	   (prep-time (file-write-date prep))
	   (prepc-time (if prepc-file-exists 
			   (file-write-date prepc))))
      (or (null prepc-file-exists)
	  (< prepc-time prep-time)))))

;;; --- deleted 10/5/92 MDJ
;;; (defvar *XTE-PREP-STREAM* t "stream or t, where to write prepped results")

;;; --- new 10/5/92 MDJ for split prepping
(defvar *XTE-TARG-ROOT* nil "root name of currently loaded .targ file")


(defun XTE-PREP-FILE (root &key &allow-other-keys)
  
  "prep a file unconditionally"
  ;; (XTE-prep-file "XTE-test")
  
  (let ((targ (XTE-root-to-targ root))
	(*XTE-TARG-ROOT* root))
    (load targ)))


(defun XTE-COMPILE-PREP-FILE (root targID)
  
  "compile a prep a file unconditionally: input pathname extension must be .prep,
compiled file extension is .prepc"
  ;;(XTE-compile-prep-file "XTE-test" "u-cep")
  
  (let ((prep (XTE-root-to-prep root targID))
	(prepc (XTE-root-to-prepc root targID)))
    (cond ((probe-file prep)
	   (compile-file prep :output-file prepc))
	  (t (error "prep file does not exist: root ~a, targID ~a"
		    root targID)))))


;;; ----------------------------------------------------------------------
;;;                                             Init, prep, and load files
;;; ----------------------------------------------------------------------

(defun XTE-INIT-PREP-AND-LOAD (targ-file-list 
			       &key
			       (constraint-files nil)
			       (name "xte")
			       &allow-other-keys)
  "Input is a list of .targ file pathnames.  Create chronika,
prep .targs if necessary, or load already prepped file,
and create CSP.  If keyword arg compile is t, then .prep files
are compiled (to make .prepc files).
constraint-files is a list of files specifying absolute and relative constraints
which is to be loaded after target are defined."
  ;; (XTE-init-prep-and-load '("XTE-test") :compile t)
  
  (let ((csp-chronika))
    
    ;; setup
    (setf csp-chronika
	  (make-obs-csp-chronika 
	   :obs-csp-chronika-type         'xte-segmented-obs-csp-chronika
	   :name                          name
	   :directory                     "spike:sched;"
	   :tjd-start                     *XTE-SCHEDULE-START* 
	   :tjd-end                       *XTE-SCHEDULE-END*
	   :tjd-quantum                   *XTE-TIME-QUANTUM*
	   :chronika-type                 'obs-chronika
	   :task-type                     'xte-task
	   :csp-type                      'segmented-obs-csp
	   :var-type                      'xte-var
	   :default-priority              *XTE-DEFAULT-PRIORITY*
	   :suitability-scale             *XTE-SUITABILITY-SCALE*
	   :segment-start                 *XTE-SEGMENT-START*
	   :segment-duration              *XTE-SEGMENT-DURATION*
	   :segment-count                 *XTE-SEGMENT-COUNT*
	   :segment-capacity-unit-in-days *XTE-CAPACITY-UNIT*
	   :segment-oversubscription      *XTE-OVERSUBSCRIPTION-FACTOR*
	   :capacity-constraint-type      'segmented-obs-cap-constraint
	   :targ-files                    targ-file-list
	   :constraint-files              constraint-files
	   ))
    (open-chronika csp-chronika)
    
    ;; load target files - this will cause prepping if needed
    ;; but only for those which have changed
    (dolist (root targ-file-list)
	    (let ((targ (xte-root-to-targ root))
		  (*XTE-TARG-ROOT* root))
	      (load targ)))
    ;; now load the prepc files
    (dolist (root targ-file-list)
	    (let ((targ (xte-root-to-targ root)))
	      (with-open-file (stream targ :direction :input)
			      (loop
			       (let ((form (read stream nil nil)))
				 (cond ((null form) (return nil))
				       ((and (listp form)
					     (eq (first form) 'def-xte-exposure))
					(load (xte-root-to-prepc
					       root (string-it-downcase 
						     (getf (rest form) :name)))))
				       (t nil)))))))
    
    ;; load constraint files
    (load-constraint-files 
     csp-chronika 
     (mapcar #'XTE-root-to-constr constraint-files))
    (validate-constraints
     csp-chronika (obs-chronika csp-chronika) (obs-abs-constraint csp-chronika))
    (validate-constraints
     csp-chronika (obs-chronika csp-chronika) (obs-rel-constraint csp-chronika))
    
    (close-obs-csp-chronika csp-chronika)
    (setf *XTE-LONG-TERM-SCHEDULE* csp-chronika)))


;;; -----------------------------------------------------------------------
;;; FUNCTION TO PREPARE AN XTE TARGET FOR SCHEDULING
;;; -----------------------------------------------------------------------
;;;
;;; this function returns a form which changes only if a target needs to
;;; be reprepped.  
;;;
;;; BEWARE that changing this function while there is
;;; a large body of prepped files could cause them all to be re-prepped!!!
;;;
;;; BEWARE: if you add a property to def-xte-exposure and then 
;;; add it to prep-xte-target, you should probably add it to the
;;; XTE-TARGET-PREP-CHANGE-INDICATOR function as well
;;;
;;; Future enhancements:
;;; - should add other important event calc parameters here: sun angle, etc.


(defun XTE-TARGET-PREP-CHANGE-INDICATOR 
       (name ra dec exp-time id priority
	     orbit-model 
             ;; next three are new mdj/nov94
             telemetry non-interrupt shadow-only)
  (declare (ignore priority))
  (format nil "~a ~,4f ~,4f ~a ~a ~a ~a ~a ~a"
	  name ra dec exp-time  id 
	  orbit-model
          telemetry non-interrupt shadow-only))


;;; this function is called by DEF-XTE-EXPOSURE to create the make-task
;;; form for the .prep file


(defun PREP-XTE-TARGET 
  (&key 
   ;; required
   name ra dec exp-time 
   
   ;; defaultable:
   id
   
   (priority    *XTE-DEFAULT-PRIORITY*)
   (orbit-model *DEFAULT-XTE-ORBIT-MODEL*)
   (start-date  *XTE-SCHEDULE-START*)
   (end-date    *XTE-SCHEDULE-END*)
   
   ;; mdj/nov94 new keywords
   (telemetry      0)
   (non-interrupt  nil)
   (shadow-only    nil)

   ;; root
   (root        *XTE-TARG-ROOT*)
   )
  
  "Compute constraints for target, format them as a make-task statement, 
and write to stream.
Arguments are:
:name - name string
:id - id, symbol.  If nil, name is converted to symbol and used as ID.  
Note: IDs must be unique, so if multiple observations of same target 
are made, each must have unique ID.
:ra - RA decimal degrees
:dec - DEC decimal degrees
:exp-time - seconds
:priority - priority value, low number = high priority, 
default *XTE-DEFAULT-PRIORITY*
:orbit-model - model name string, default *DEFAULT-XTE-ORBIT-MODEL*
:start-date - start date for prep range, default *XTE-SCHEDULE-START*
:end-date - end date for prep range, default *XTE-SCHEDULE-END*"
  
  (let* ((prep-change-indicator
	  (XTE-TARGET-PREP-CHANGE-INDICATOR 
	   name ra dec exp-time  id priority
	   orbit-model
           telemetry non-interrupt shadow-only))
	 (prep-file (xte-root-to-prep root name))
	 (prep-file-exists (probe-file prep-file))
	 (test-start-date nil)
	 (test-end-date   nil)
	 (test-form       nil)
	 (time0  0)                     ; for timing
	 (time1  0)                     ; for timing
	 )
    (when prep-file-exists
	  (with-open-file (stream prep-file :direction :input)
			  (setf test-start-date (read stream nil nil)
				test-end-date   (read stream nil nil)
				test-form       (read stream nil nil))))
    
    ;; unless prep file exists, and first form is equal to indicator,
    ;; then re-prep
    
    ;;(format t "~%indicator: ~s" prep-change-indicator)
    ;;(format t "~%from file: ~s" test-form)
    ;;(format t "~%equal? ~a" (equal test-form prep-change-indicator))
    
    (unless (and test-start-date
		 test-end-date
		 test-form
		 (equal test-form prep-change-indicator)
		 (>= start-date test-start-date)
		 (<= end-date   test-end-date))
	    
	    ;; -- prep
	    (with-open-file (stream prep-file
				    :direction :output
				    :if-does-not-exist :create
				    :if-exists :supersede)
			    ;; write header 
			    (write-XTE-prep-file-header nil name stream)
			    
			    ;; save indicator and dates
			    (terpri stream)
			    (prin1 start-date stream)
			    (terpri stream)
			    (prin1 end-date stream)
			    (terpri stream)
			    (prin1 prep-change-indicator stream)
			    (terpri stream)
			    
			    ;; do prepping
			    (format t "~%Prepping ~20a (RA ~7,3f Dec ~7,3f)..." name ra dec)
			    (setf time0 (get-internal-run-time))
			    
			    ;; clear the cache if it's getting large...
			    (if (> (event-cache-size (current-xte-event-calc)) 100)
				(clear-event-cache (current-xte-event-calc)))
			    
                            ;; mdj/nov94: new constraint params for dark, non-int
			    (set-XTE-constraint-parameters
			     :name name :ra ra :dec dec
			     :start-date start-date
			     :end-date end-date
			     :orbit-model orbit-model
                             :shadow-flag shadow-only
                             :non-interrupt non-interrupt
                             :non-interrupt-duration (/ exp-time 86400.0))

			    (let ((efficiency (get-xte-efficiency-pcf))
				  (orb-viewing (get-xte-orbital-viewing-pcf))
				  (sun (get-xte-sun-pcf))
				  (moon (get-xte-moon-pcf))
				  total
				  make-task-form)
			      
			      ;; compute total suitability
			      (setf total (pcf-multiply sun (pcf-multiply moon orb-viewing)))
			      ;; modify efficiency so that it is zero whenever absolute suitability
			      ;; is zero
			      (setf efficiency 
				    (pcf-combine efficiency total 
						 #'(lambda (effic tot) 
						     (if (> tot 0) effic 0))))
			      ;; write to stream
                              ;; new mdj/nov94: pass through new param for telemetry,
                              ;;  non-int, shadow
			      (setf make-task-form 
				    `(make-XTE-task 
				      :name ,name
				      :id ',(or id (intern (string-it-upcase name)))
				      :ra ,ra
				      :dec ,dec
				      :exp-time ,exp-time
				      :priority ,priority
				      :available-time-intervals
				      ',(pcf-to-interval-list efficiency start-date end-date)
				      :abs-constraint-intervals
				      ',(pcf-to-interval-list total start-date end-date)
                                      :telemetry ,telemetry 
                                      :non-interrupt ,non-interrupt 
                                      :shadow-only ,shadow-only
                                      ))
			      (pprint make-task-form stream)))
	    (setf time1 (get-internal-run-time))
	    (format t " run time: ~6,1f sec"
		    (/ (- time1 time0) internal-time-units-per-second))
	    (compile-file (xte-root-to-prep root name)
			  :output-file (xte-root-to-prepc root name))
	    ;; -- end prepping
	    
	    
	    )))



;;; ----------------------------------------------------------------------
;;;                              CLASS:  XTE-SEGMENTED-OBS-CSP-CHRONIKA
;;; ----------------------------------------------------------------------
;;;
;;; new class to record targ files
;;;

(defclass 
  XTE-SEGMENTED-OBS-CSP-CHRONIKA 
  
  (SEGMENTED-OBS-CSP-CHRONIKA)
  
  (
   
   (targ-files
    :initform nil
    :accessor targ-files
    :documentation "list of targ file roots loaded")
   
   ;; next three slots are for keeping track of locked bins
   (locked-bins
    :initform nil
    :accessor locked-bins
    :documentation 
    "list of bins which are locked, i.e.
vars not already assigned there should be prevented from
going there")
   
   (locked-bin-locked-var-table
    :initform (make-hash-table)
    :documentation "hashtable, key is bin, value is
list of vars locked due to the locking of the bin")
   
   (locked-bin-removed-domain-table
    :initform (make-hash-table)
    :documentation "hash table, key is bin, value is
list of (var . value) removed from domain due to the
locking of the bin")
   ))

;;; --- store the targ files

(defmethod INIT-OBS-CSP-CHRONIKA :AFTER 
  ((SOCC xte-segmented-obs-csp-chronika) args)
  
  (setf (slot-value SOCC 'targ-files)
	(getf args :targ-files nil)))


;;; --- print object method...

(defmethod PRINT-OBJECT ((SOCC xte-segmented-obs-csp-chronika) stream)
  (format stream "<XTE long-term schedule>"))


;;; --- lock/unlock bin methods

(defmethod LOCK-BIN ((SOCC xte-segmented-obs-csp-chronika) bin)
  (with-slots 
   (locked-bins 
    locked-bin-locked-var-table
    locked-bin-removed-domain-table) SOCC
    
    (when (not (member bin locked-bins))
	  (let* ((csp (obs-csp SOCC))
		 (cap-constraint (obs-cap-constraint SOCC))
		 (vars-in-bin (vars-assigned-to-bin cap-constraint bin))
		 ;; keep track to undo
		 (vars-to-lock nil)
		 (domain-values-to-remove nil)
		 )
	    
	    (do-for-all-vars
	     :var v :csp csp
	     :form
	     (cond 
	      
	      ;; assigned to bin but not already locked: lock it
	      ((and (member v vars-in-bin)
		    (not (locked-p v)))
	       (lock-var v (assigned-value v))
	       (push v vars-to-lock)
	       )
	      
	      ;; assigned and locked
	      ((and (member v vars-in-bin)
		    (locked-p v))
	       ;; no action: already locked
	       )
	      
	      ;; not assigned: 
	      (t 	    
	       (dolist (val (bin-to-values csp cap-constraint v bin))
		       (when (value-in-domain-p v val)
			     (remove-value-from-domain v val :permanent t)
			     (push (cons v val) domain-values-to-remove))))))
	    
	    ;; update slots
	    (pushnew bin locked-bins)
	    (setf (gethash bin locked-bin-locked-var-table) vars-to-lock)
	    (setf (gethash bin locked-bin-removed-domain-table)
		  domain-values-to-remove)
	    
	    ))))


(defmethod UNLOCK-BIN ((SOCC xte-segmented-obs-csp-chronika) bin)
  (with-slots 
   (locked-bins 
    locked-bin-locked-var-table
    locked-bin-removed-domain-table) SOCC
    
    (when (member bin locked-bins)
	  (dolist (v (gethash bin locked-bin-locked-var-table))
		  (unlock-var v))
	  
	  (dolist (vv (gethash bin locked-bin-removed-domain-table))
		  (restore-value-to-domain (first vv) (rest vv)
					   :permanent t))
	  
	  (setf locked-bins (remove bin locked-bins))
	  (setf (gethash bin locked-bin-locked-var-table) nil)
	  (setf (gethash bin locked-bin-removed-domain-table) nil)
	  )))


(defmethod UNLOCK-ALL-BINS ((SOCC xte-segmented-obs-csp-chronika))
  (let* (;; (csp (obs-csp SOCC))
	 (cap-constraint (obs-cap-constraint SOCC))
	 (bins (bin-count cap-constraint))
	 )
    (dotimes (i bins)
	     (unlock-bin SOCC i))))


(defmethod BIN-IS-LOCKED-P ((SOCC xte-segmented-obs-csp-chronika) bin)
  (with-slots 
   (locked-bins) SOCC
   (member bin locked-bins)))




#|
(describe *xte-long-term-schedule*)
(lock-bin *xte-long-term-schedule* 0)
(lock-bin *xte-long-term-schedule* 1)

(unlock-bin *xte-long-term-schedule* 0)
(unlock-bin *xte-long-term-schedule* 1)

(mp:process-reset
 (mp:process-name-to-process "Long-term Schedule"))
|#

;;; -----------------------------------------------------------------------
;;;                                    METHOD FOR SAVING/RESTORING SCHEDULE
;;; -----------------------------------------------------------------------

#|
(save-xte-schedule-to-file :root "foo")
(save-xte-schedule-parameters-to-file :root "foo2") 
(ed "macintosh hd:spike:sched:foo.sched")
(restore-xte-schedule-from-file "foo")
(restore-xte-schedule-from-file "foo2")
(restore-xte-schedule-from-file "foo3")
(recreate-xte-schedule)
(describe 'recreate-xte-schedule)
|#


;;; --- function to save an existing (loaded) schedule to a file

(defun SAVE-XTE-SCHEDULE-TO-FILE (&key (occ *xte-long-term-schedule*)
				       (root nil))
  
  ;; name will be root if specified, otherwise the name in the obs-csp-chronika
  ;; or "dummy" if there is none
  
  (let* ((root-name (cond (root (string-it root))
			  ((obs-csp-chronika-name occ))
			  (t "dummy")))
	 (filename (xte-root-to-sched root-name)))
    ;; (format t "~%Saving to file: ~a" filename)
    (with-open-file (stream filename
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :supersede)

		    (with-slots
		     (targ-files constraint-files obs-csp-chronika-name
				 obs-chronika-tjd-start obs-chronika-tjd-end obs-chronika-quantum
				 obs-default-priority obs-suitability-scale obs-segment-start
				 obs-segment-duration obs-segment-count
				 obs-segment-capacity-unit-in-days
				 obs-segment-oversubscription
				 obs-csp locked-bins)
		     occ
		     
		     ;; save the name in the object
		     (setf obs-csp-chronika-name root-name)
		     
		     ;; construct form for loading
		     (format stream ";;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-")
		     (time-stamp stream ";; saved: ")
		     
		     (format stream "~%(setf *XTE-SCHEDULE-START*    ~a)" OBS-CHRONIKA-TJD-START)
		     (format stream "~%(setf *XTE-SCHEDULE-END*      ~a)" OBS-CHRONIKA-TJD-END)
		     (format stream "~%(setf *XTE-TIME-QUANTUM*      ~a)" OBS-CHRONIKA-QUANTUM)
		     (format stream "~%(setf *XTE-DEFAULT-PRIORITY*  ~a)" OBS-DEFAULT-PRIORITY)
		     (format stream "~%(setf *XTE-SUITABILITY-SCALE* ~a)" OBS-SUITABILITY-SCALE)
		     (format stream "~%(setf *XTE-SEGMENT-START*     ~a)" OBS-SEGMENT-START)
		     (format stream "~%(setf *XTE-SEGMENT-DURATION*  ~a)" OBS-SEGMENT-DURATION)
		     (format stream "~%(setf *XTE-SEGMENT-COUNT*     ~a)" OBS-SEGMENT-COUNT)
		     (format stream "~%(setf *XTE-CAPACITY-UNIT*     ~a)" OBS-SEGMENT-CAPACITY-UNIT-IN-DAYS)
		     (format stream "~%(setf *XTE-OVERSUBSCRIPTION-FACTOR* ~a)" 
			     OBS-SEGMENT-OVERSUBSCRIPTION)
		     
		     (format stream "~%(setf *XTE-TARG-FILES* '~s)" targ-files)
		     (format stream "~%(setf *XTE-CONSTRAINT-FILES* '~s)"
			     (mapcar #'xte-pathname-to-root constraint-files))
		     
		     
		     (format stream "~%(DEFUN RECREATE-XTE-SCHEDULE (&key")
		     
		     (format stream "~%    (name             ~s)" root-name)
		     (format stream "~%    (targ-files       '~s)" targ-files)
		     (format stream "~%    (constraint-files '~s)" 
			     (mapcar #'xte-pathname-to-root constraint-files))
		     
		     (format stream "~%    (locked-vars      '~s)"
			     (mapcar
			      #'(lambda (v) (list (var-name v) (assigned-value v)))
			      (locked-var-list obs-csp)))
		     
		     (let ((permanently-ignored-vars nil))
		       (dolist (v (ignored-var-list obs-csp))
			       (if (ignored-permanently-p v)
				   (push (var-name v) permanently-ignored-vars)))
		       (format stream "~%    (ignored-vars     '~s)" 
			       permanently-ignored-vars))
		     
		     (format stream "~%    (assigned-vars    '~s)"
			     (list-assignments obs-csp :var-mapping 'var-name
					       :form :list))
		     
		     (format stream "~%    (commit-times '(")
		     (dolist (task (task-list (obs-chronika *xte-long-term-schedule*)))
			     (format stream "~%          (~a ~a ~a)" (id task) 
				     (long-term-commitment-time (id task))
				     (short-term-commitment-time (id task))))
		     (format stream "~%      ))")
		     (format stream "~%    (locked-bins       '~s)" locked-bins)	
		     
		     ;; end let bindings
		     (format stream "~%    )")
		     
		     (format stream "~%  (xte-init-prep-and-load")
		     (format stream "~%    targ-files")
		     (format stream "~%    :constraint-files constraint-files")
		     (format stream "~%    :name name)")
		     
		     ;; restore locked, ignored, assigned vars:
		     (format stream "~%  (dolist (v locked-vars)")
		    (format stream "~%  (let (the-var (var-named (obs-csp *xte-long-term-schedule*) (first v)))")
		    (format stream "~%    (if (second v)")
		    (format stream "~%      (lock-var ")
		    (format stream " the-var (second v))")
		    (format stream "~%      (setf (slot-value the-var 'locked-p) t))))")
		    
		     (format stream "~%  (dolist (v ignored-vars)")
		     (format stream "~%    (ignore-var (var-named (obs-csp *xte-long-term-schedule*) v)")
		     (format stream " :permanent t))")
		     
		     (format stream "~%  (dolist (v assigned-vars)")
		     (format stream "~%     (assign-value (var-named (obs-csp *xte-long-term-schedule*)")
		     (format stream " (first v)) (second v)))")
		     
		     (format stream "~%  (dolist (bin locked-bins)")
		     (format stream "~%     (lock-bin *xte-long-term-schedule* bin))")
		     
		     ;; restore commit times
		     (format stream "~%  (dolist (s commit-times)")
		     (format stream "~%    (set-long-term-commitment-time (first s) (second s))")
		     (format stream "~%    (set-short-term-commitment-time (first s) (third s)))")
		     
		     (format stream "~% )")
		     
		     ))
	;; reb 4/94 Record name of saved file in saved-scheds.current
  (let* ((filename (xte-root-to-ext "saved-scheds" "current")))
    ;; (format t "~%Saving to file: ~a" filename)
    (with-open-file (stream filename
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :append)
	(format stream "~a~%" root-name)))))


;;; --- function to save parameter settings to a schedule file
;;; --- BEWARE!! KEEP THIS FUNCTION CONSISTENT WITH THE ONE ABOVE!!

(defun SAVE-XTE-SCHEDULE-PARAMETERS-TO-FILE 
  (&key (root nil))
  ;; name is the argument root, it must be specified (or "dummy" is the default)
  (let* ((root-name (cond (root (string-it root))
			  (t "dummy")))
	 (filename (xte-root-to-sched root-name)))
    ;; (format t "~%Saving to file: ~a" filename)
    (with-open-file (stream filename
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :supersede)
		    
		    ;; construct form for loading
		    (format stream ";;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-")      
		    (time-stamp stream ";; saved: ")
		    
		    (format stream "~%(setf *XTE-SCHEDULE-START*    ~a)" *XTE-SCHEDULE-START*)
		    (format stream "~%(setf *XTE-SCHEDULE-END*      ~a)" *XTE-SCHEDULE-END*)
		    (format stream "~%(setf *XTE-TIME-QUANTUM*      ~a)" *XTE-TIME-QUANTUM*)
		    (format stream "~%(setf *XTE-DEFAULT-PRIORITY*  ~a)" *XTE-DEFAULT-PRIORITY*)
		    (format stream "~%(setf *XTE-SUITABILITY-SCALE* ~a)" *XTE-SUITABILITY-SCALE*)
		    (format stream "~%(setf *XTE-SEGMENT-START*     ~a)" *XTE-SEGMENT-START*)
		    (format stream "~%(setf *XTE-SEGMENT-DURATION*  ~a)" *XTE-SEGMENT-DURATION*)
		    (format stream "~%(setf *XTE-SEGMENT-COUNT*     ~a)" *XTE-SEGMENT-COUNT*)
		    (format stream "~%(setf *XTE-CAPACITY-UNIT*     ~a)" *XTE-CAPACITY-UNIT*)
		    (format stream "~%(setf *XTE-OVERSUBSCRIPTION-FACTOR* ~a)" 
			    *XTE-OVERSUBSCRIPTION-FACTOR*)
		    
		    (format stream "~%(setf *XTE-TARG-FILES* '~s)" *XTE-TARG-FILES*)
		    (format stream "~%(setf *XTE-CONSTRAINT-FILES* '~s)"
			    *XTE-CONSTRAINT-FILES*)
		    
		    
		    
		    (format stream "~%(DEFUN RECREATE-XTE-SCHEDULE (&key")
		    
		    (format stream "~%    (name             ~s)" root-name)
		    (format stream "~%    (targ-files       '~s)" *XTE-TARG-FILES*)
		    (format stream "~%    (constraint-files '~s)" *XTE-CONSTRAINT-FILES*)
		    
		    (format stream "~%    (locked-vars      nil)")
		    (format stream "~%    (ignored-vars     nil)")
		    (format stream "~%    (assigned-vars    nil)")
		    (format stream "~%    (commit-times     nil)")
		    (format stream "~%    (locked-bins      nil)")
		    
		    ;; end key vars
		    (format stream "~%    )")
		    
		    (format stream "~%  (xte-init-prep-and-load")
		    (format stream "~%    targ-files")
		    (format stream "~%    :constraint-files constraint-files")
		    (format stream "~%    :name name)")
		    
		    ;; restore locked, ignored, assigned vars:
		    (format stream "~%  (dolist (v locked-vars)")
		    (format stream "~%    (lock-var (var-named (obs-csp *xte-long-term-schedule*)")
		    (format stream " (first v)) (second v)))")
		    
		    (format stream "~%  (dolist (v ignored-vars)")
		    (format stream "~%    (ignore-var (var-named (obs-csp *xte-long-term-schedule*) v)")
		    (format stream " :permanent t))")
		    
		    (format stream "~%  (dolist (v assigned-vars)")
		    (format stream "~%     (assign-value (var-named (obs-csp *xte-long-term-schedule*)")
		    (format stream " (first v)) (second v)))")
		    
		    ;; restore commit times
		    (format stream "~%  (dolist (s commit-times)")
		    (format stream "~%    (set-long-term-commitment-time (first s) (second s))")
		    (format stream "~%    (set-short-term-commitment-time (first s) (third s)))")
		    
		    (format stream "~%  (dolist (bin locked-bins)")
		    (format stream "~%     (lock-bin *xte-long-term-schedule* bin))")
		    
		    (format stream "~% )")
		    
		    )))


(defun RESTORE-XTE-SCHEDULE-FROM-FILE (root)
  "restore saved schedule, return t if OK, nil if error"
  ;; always in the same package!
  (let ((filename (xte-root-to-sched root)))
    (cond ((probe-file filename)
	   (let ((*package* (find-package 'user))
		 #+:Allegro (lisp:*load-print* nil)
		 #+:Allegro (lisp:*load-verbose* nil)
		 #+:Allegro (excl:*redefinition-warnings* nil)
		 )
	     (load filename))
	   t)
	  (t ;; no file found
	   nil))))

(defun SAVE-XTE-ROSTER-TO-FILE
    (&key (occ nil )
	  (evt-file-name nil)
	  (do-saa t) (do-occult t) (do-commits t))
  (with-slots
    (obs-chronika-tjd-start obs-chronika-tjd-end)
    occ
    (let* ((roster nil)
	 (start-time obs-chronika-tjd-start)
	 (end-time obs-chronika-tjd-end)
	 (saa-list(pcf-zero-intervals
		  (saa-pcf *xte* 
			   start-time
			   end-time
			   :table *SAA-ENTRY-EXIT-TABLE*)
		  start-time
		  end-time))
	   (asc-node-1 (time-of-next-asc-node *XTE* start-time))
	   (asc-node-2 (time-of-next-asc-node *XTE*
					      
					      (+ asc-node-1 .01)))
	   (chronika (chronika (first
				(task-list (obs-chronika occ)))))
	   )
      (let* ((tjd-end start-time)
	     (occult-list nil)
	     (prev-task nil)
	     (prev-tjd-end nil)
	     (slew-start nil)
	     (slew-duration 0)
	     (tasks-sorted (task-list (obs-chronika occ))))
	(setq tasks-sorted 
	  (remove-if #'(lambda (x) (null (short-term-commitment-time (id x))))
		     tasks-sorted))
	(setq tasks-sorted
	      (sort tasks-sorted
		    #'<
		    :key #'(lambda (x)
			     (short-term-commitment-time (id x)))))

      ;;  (format t "~%REB - Beginning Roster Save")
      (dolist (task tasks-sorted)
	(let* ((var (task-var task))
	       (start-end (multiple-value-list (assigned-time var)))
	       (start (first start-end))
	       (end (second start-end))
	       (tjd-start (short-term-commitment-time (id task)))
	       )

	(cond (tjd-start
	       (setq tjd-end (+ tjd-start 
				(duration-in-qtime-to-days
				 chronika
				 (- end start))))
	       (cond (prev-task
		      ;; (format t "~%REB - Setting slews ")
		      ;; (format t "~%for ~a" (id task))
		      (setf slew-duration
			(xte-slew-time-in-days
			 (pos-vector prev-task)
			 (pos-vector task)
			 (position-vector *SUN* tjd-start)
			 (slew-rate (chronika task))))
		      (setf slew-start prev-tjd-end))
		     (t 
		      (setf slew-duration (/ 15 (* 24 60)))
		      (setf slew-start 
			(max (- tjd-start slew-duration)
			     obs-chronika-tjd-start))))
	       
	       ;; (format t "~%REB - Adding slew to roster")
	       (setq roster 
		 (cons (list "Begin_slew"
			     slew-start
			     (target-name task))
		       roster))
	       ;;(format t "~%REB - Adding end of slew to roster")

	       (setq roster 
		 (cons (list "End_slew"
			     (+ slew-start slew-duration)
			     (target-name task))
		       roster))
	       
	       ;;(format t "~%REB - Gather occult events for ~a" (id task))
	       ;;(format t "~%REB     TJD start: ~a, TJD end: ~a"
		;;       tjd-start tjd-end)

	       (setq occult-list
		 (earth-illum-occultation-list 
		  *XTE*
		  (make-fixed-astronomical-object
		   (id task)
		   :position-vector 
		   (ra-dec-to-cartesian-3vect
		    (list (slot-value task 'target-ra)
			  (slot-value task 'target-dec))
		    :units :degrees))
		  slew-start tjd-end))
	       ;;(format t "~% occult list: ~a" occult-list)

	       (let ((parity 0))
		 (dolist (occ-event occult-list)
		   (cond ((>= occ-event tjd-end) nil)
			 ((equal parity 0)
			  ;;(format t "~%        Enter occult at ~a " occ-event)
			  (if (<= occ-event  slew-start)
			      (setf occ-event slew-start))
			  (setq roster 
			    (cons (list "Enter_occultation" occ-event
					(id task))
				  roster)))
			 (t
			  ;;(format t "~%        Exit occult at  ~a " occ-event)			  
			  (when (<= occ-event  slew-start)
			    (format t "~% Warning: Exit-occult appears before begin slew for ~a at ~a"
				    (id task) occ-event)
			    (setf occ-event slew-start))
			  (setq roster
			    (cons (list "Exit_occultation" occ-event
					(id task))
				  roster))))
		   (setq parity (mod (+ 1 parity) 2))))
	       )
	      (t nil))
	 ;;(format t "~%REB - Done with task ~a " (id task))

	(cond (task
	       (setf prev-tjd-end tjd-end)
	       (setf prev-task task)))))
      
      ;;(format t "~%REB - Gather SAAs")
    (dolist (saa-event saa-list)
	    (let* ((SAA-entry (first saa-event))
		   (SAA-exit (second saa-event)))
	      (setq roster
		    (cons (list "Enter_SAA" SAA-entry NIL)
			  roster))
	      (setq roster
		    (cons (list "Exit_SAA" SAA-exit NIL)
			  roster))
	      ))
    (setq roster (nreverse roster))
    ;;(format t "~%REB - Sort roster")

    (setq roster (stable-sort roster #'< :key #'cadr))

    ;; Associate orbital events with an observation, where appropriate
    (let ((prev-task 
	   (id (first tasks-sorted))))
      (dolist (event
                 roster)
	      (cond ((null (third event))
		     (cond ((eql (third event) "Begin_ASM_dwell_seq")
			    nil)
			   (t (setf (third event) prev-task))))
              (t (setq prev-task (third event)))))))
     ;;(format t "~%REB - Write roster")

;; Roster is collected; write it out to the file.  
(let* ((root-name (cond (evt-file-name (string-it evt-file-name))
			  ((obs-csp-chronika-name occ))
			  (t "dummy")))
	 (filename (xte-root-to-ext root-name "events")))
    ;; (format t "~%Saving to file: ~a" filename)
    (write-task-to-ra-dec-table occ root-name)
    (with-open-file 
     (stream filename
	     :direction :output
	     :if-does-not-exist :create
	     :if-exists :supersede)
     (format stream "~%;;;Frame name ~a"
	     (clim:frame-name clim:*application-frame*))
     (format stream "~%(EVENT-ROSTER")
     (format stream "~%  (INITIAL_ORBIT_STATE VIEWING)")
     (format stream "~%  (ROSTER_START ~a)" obs-chronika-tjd-start)
     (format stream "~%  (ROSTER_END ~a)" obs-chronika-tjd-end)
     (format stream "~%  (ORBIT_FILE_NAME ~a)"
	     (if *XTE-EPHEMERIS-FILE-NAME*
		 *XTE-EPHEMERIS-FILE-NAME*
	       
;;	     (let ( (ofn (fdf-file-name-containing-jd
;;			  (+ 1 obs-chronika-tjd-start))))
;;	       (if ofn
;;		   ofn
;;	       "None"))
	       "None"))
	       
     (format stream "~%  (EVENT ~a ~a ~a)"
	     "Ascending_node" asc-node-1 NIL)
     (format stream "~%  (EVENT ~a ~a ~a)"
	     "Ascending_node" asc-node-2 NIL)
     (dolist (event roster)
	      (format stream "~%  (EVENT ~a ~18f ~a)"
		      (first event) (second event) (third event)))
     (format stream "~%)~%")))
)))

(defun WRITE-TASK-TO-RA-DEC-TABLE (occ root-name)
  "Write ASCII file translating task id to right ascension and declination
  of target"
  (let ((filename 
	 (xte-root-to-ext root-name "positions")))
    (with-open-file 
      (stream filename
	      :direction :output
	      :if-does-not-exist :create
	      :if-exists :supersede)
      (dolist (task (task-list (obs-chronika occ)))
        (cond ( (short-term-commitment-time (id task))
                ;;; TBI: write these on a file
                (format stream "~%Task ~a RA ~18f DEC ~18f Roll 0.0"
                        (id task)
                        (slot-value task 'target-ra)
                        (slot-value task 'target-dec)))
              (t nil)))
      (format stream "~%")
      )))

	  
;;; -----------------------------------------------------------------------
;;;                                                     CLASS:  XTE-TASK 
;;; -----------------------------------------------------------------------


(defclass XTE-TASK (FIXED-OBS-TASK)
  (
   ;; mdj/nov94: three new slots added for telemetry, non-int, shadow
   (telemetry
    :initform 0 :accessor telemetry
    :documentation "telemetry rate when taking data, kbits/sec")
   (non-interrupt
    :initform nil :accessor non-interrupt
    :documentation "flag, t if data must be taken in a non-interruptible
      period, nil otherwise")
   (shadow-only
    :initform nil :accessor shadow-only
    :documentation "flag, t if data must be taken in shadow only, 
      nil otherwise")
  ;; reb/jun95: slot added for roll angle 
  (roll-angle
   :initform 0.0 :accessor roll-angle
   :documentation "angle from satellite q axis to sun vector")
  )
  (:documentation "The class of tasks representing XTE exposures"))


(defun MAKE-XTE-TASK (&rest 
		      args 
		      &key (obs-csp-chronika *CURRENT-OBS-CSP-CHRONIKA*)
		      &allow-other-keys)
  (if (not obs-csp-chronika)
      (error "attempt to create task without having defined obs-csp-chronika"))
  ;; (format t ".")			;show busy
  (let ((args (append args (task-initargs obs-csp-chronika))))
    (apply 'make-task args)))


(defmethod INIT-TASK :AFTER ((task XTE-TASK) args)
  ;; mdj/nov94: store the values for telemetry, non-int, shadow
  (setf (slot-value task 'telemetry) (getf args :telemetry))
  (setf (slot-value task 'non-interrupt) (getf args :non-interrupt))
  (setf (slot-value task 'shadow-only) (getf args :shadow-only))

  ;; at task create time, clear the commit times recorded on the
  ;; task ID symbol
  (let ((id-sym (ID task)))
    (set-long-term-commitment-time id-sym nil)
    (set-short-term-commitment-time id-sym nil))
  )


(defmethod COMPUTE-DURATION-IPCF ((task XTE-TASK)
				  (chr OBS-CHRONIKA)
				  efficiency-ipcf
				  abs-ipcf
				  fractional-exp-qtime)
  "return two values:
duration-interval-list (quantized) - to construct FDSIA
duration-ipcf (possibly fractional) - for task slot"
  ;; (XTE-init-prep-and-load '("xte-test") :compile t)
  
  ;;(format t "~%Task: ~a" (id task))
  ;;(format t "~%effic ipcf ~a" 
  ;;        (ipcf-to-interval-list 
  ;;         efficiency-ipcf (qtime-start chr) (qtime-end chr)))
  ;;(format t "~%abs   ipcf ~a"
  ;;        (ipcf-to-interval-list 
  ;;         abs-ipcf (qtime-start chr) (qtime-end chr)))
  ;;(format t "~%exp qtime ~a" fractional-exp-qtime)
  
  (multiple-value-bind 
   (int-list ipcf)
   (duration-ipcf-and-intervals
    efficiency-ipcf abs-ipcf fractional-exp-qtime 
    (qtime-start chr) (qtime-end chr)
    :min-duration-limit 0
    :get-integral-duration-by :floor
    :remove-intervals-with-zero-task-duration nil
    :zero-task-durations t
    :duration-ipcf-is-integral nil
    :duration-ipcf-max-duration 0
    :verbose nil)
   
   ;;(format t "~%int list: ~a" int-list)
   ;;(format t "~%dur ipcf: ~a" 
   ;;        (ipcf-to-interval-list 
   ;;         ipcf (qtime-start chr) (qtime-end chr)))
   
   (values int-list ipcf))
  
  )

#| Formerly, the above method contained the following code:
replaced by call to general function which can handle options required.
MDJ 28Nov91
(let ((duration-ipcf nil)
      (duration-interval-list nil))
  
  ;; construct duration ipcf:  time will be quanta, value will be duration in
  ;; quanta.  If efficiency is zero, or if abs suitability is zero,
  ;; then duration is set to be 0.
  (setf duration-ipcf
	(ipcf-combine 
	 efficiency-ipcf
	 abs-ipcf
	 #'(lambda (effic abs) 
	     (cond 
	      ;; if efficiency or abs suit is zero, duration is 0
	      ((or (= 0 effic) (= 0 abs)) 0)
	      (t ;; otherwise compute duration as fractional qtime
	       (/ fractional-exp-qtime effic))))))
  
  ;; convert duration ipcf to interval list
  (setf duration-interval-list 
	(ipcf-to-interval-list 
	 duration-ipcf (qtime-start chr) (qtime-end chr)))
  
  ;; filter intervals
  ;; remove any intervals with duration =0: these have either 0 efficiency
  ;; or 0 suitability
  (setf duration-interval-list
	(remove-if #'(lambda (x) (= 0 (third x))) duration-interval-list))
  ;; set integral duration of any remaining intervals to 0
  (dolist (interval duration-interval-list)
	  (setf (third interval) 0))
  
  (values duration-interval-list
	  duration-ipcf))
|#


(defmethod PRINT-OBJECT ((task XTE-task) stream)
  (format stream "<XTE-TASK ~a>" (target-name task)))


(defclass XTE-VAR (SEGMENTED-OBS-VAR)
  ()
  (:documentation "foo"))

;;; mdj/nov94: 3 new methods to get params stored in task: 
;;; telemetry, non-interrupt, shadow-only

(defmethod TELEMETRY ((v XTE-VAR))
  (telemetry (task v)))

(defmethod NON-INTERRUPT ((v XTE-VAR))
  (non-interrupt (task v)))

(defmethod SHADOW-ONLY ((v XTE-VAR))
  (shadow-only (task v)))


(defmethod VAR-OVERLAP-EFFECT ((csp OBS-CSP) (v XTE-VAR) value)
  (declare (ignore value)))


;;; these methods record commit times automatically on the symbol
;;; property lists:

(defmethod ASSIGN-VALUE :AFTER ((v XTE-VAR) value)
  (let ((task (task v)))
    (cond (value
	   (set-long-term-commitment-time 
	    (id task)
	    (qtime-to-TJD 
	     (chronika task)
	     (FDSIA-val-to-time (FDSIA task) value)
	     )))
	  (t ;; no value
	   (set-long-term-commitment-time
	    (id task) nil)))))


(defmethod UNASSIGN-VALUE :AFTER ((v XTE-VAR))
  (let ((task (task v)))
    (set-long-term-commitment-time
     (id task) nil)))

(defmethod RE-INITIALIZE-VARIABLE :AFTER ((v XTE-VAR))
  (let ((task (task v)))
    (set-long-term-commitment-time
     (id task) nil)))




;;; -----------------------------------------------------------------------
;;; REPORTS FOR XTE LONG-TERM SCHEDULES
;;; -----------------------------------------------------------------------

(defmethod SHOW-SEGMENTS ((schedule SEGMENTED-OBS-CSP-CHRONIKA)
			  &optional (stream t))
  
  "printed representation of segments to stream"
  ;; (show-segments *xte-long-term-schedule*)
  
  (let ((csp (obs-csp schedule))
	(chronika (obs-chronika schedule)))
    
    (format stream "~%~d segments" (total-number-of-segments csp))
    (format stream "~%  start of first ~5d TJD ~9,4f (~a)"
	    (start-time-of-first-segment csp)
	    (qtime-to-TJD chronika (start-time-of-first-segment csp))
	    (format-abs-time 
	     (qtime-to-TJD chronika (start-time-of-first-segment csp))))
    (format stream "~%  end of last    ~5d TJD ~9,4f (~a)"
	    (end-time-of-last-segment csp)
	    (qtime-to-TJD chronika (end-time-of-last-segment csp))
	    (format-abs-time 
	     (qtime-to-TJD chronika (end-time-of-last-segment csp))))
    (if (duration-of-uniform-segment csp)
	;; nil if non-uniform segments
	(format stream "~%  duration ~d (days: ~,4f)"
		(duration-of-uniform-segment csp)
		(duration-in-qtime-to-days chronika
					   (duration-of-uniform-segment csp))))
    (dotimes (i (total-number-of-segments csp))
	     (format stream "~%seg: ~3d  ~3d-~3d (TJD ~9,4f-~9,4f) ~a-~a" i 
		     (start-of-segment csp i)
		     (end-of-segment csp i)
		     (qtime-to-TJD chronika  (start-of-segment csp i))
		     (qtime-to-TJD chronika  (end-of-segment csp i))
		     (format-abs-time 
		      (qtime-to-TJD chronika (start-of-segment csp i)))
		     (format-abs-time 
		      (qtime-to-TJD chronika (end-of-segment csp i)))))
    
    ))


;;; formerly print-long-term-report from xte-functions
;;; moved here 10/10/92 MDJ
#|
(xte-long-term-report (obs-csp *xte-long-term-schedule*) :stream t)
|#

(defun XTE-LONG-TERM-REPORT (csp &key (stream t))
  
  (let ((long-term-target-list nil))
    
    (do-for-all-vars
     :var v :csp csp
     :predicate (has-assigned-value-p v)
     :form (let* ((name (target-name (task v)))
		  )
	     (push (list name)
		   long-term-target-list)))
    
    (format stream "~%Target Information")
    (format stream "~%name   ")
    (format stream "~%")
    
    (dolist (target long-term-target-list)
	    (format stream "~%~18a ~13a ~13a ~13a ~13a"
		    (first target)
		    (second target)
		    (third target)
		    (fourth target)
		    (fifth target)))
    
    (format stream "~% ")
    
    (schedule-report csp :stream stream)
    
    ))


#|======== FIX OR KILL ==========
;;; -----------------------------------------------------------------------
;;; special methods for XTE-CSPs

;;; Note:  unassign methods here should be rewritten to use unmake-assignments

(defmethod UNASSIGN-UNTIL-NO-CONFLICTS ((csp XTE-CSP))
  "Undo assignments (max conflicts first) until none left"
  (let ((var (a-random-var-with-max-conflicts csp)))
    (cond ((and var 
		(assigned-value-has-conflicts-p var))
	   (unassign-value var)
	   (unassign-until-no-conflicts csp))
	  (t nil))))


(defmethod UNASSIGN-MIN-PREFERENCE-UNTIL-NO-CONFLICTS ((csp XTE-CSP))
  
  "Another way to unassign vars until conflicts are removed:  this one
removes vars with max conflicts breaking ties by min preference
(picking a max conflicts assignments and undoing it should reduce
	 the conflicts on the greatest number of other variables) until 
there are no conflicts"

(let ((vars (apply-var-selection 
	     (apply-var-selection (all-vars-with-conflicts csp)
				  '(conflicts-on-assigned-value :max))	    
	     '(assigned-value-preference :min))))
  (when vars
	(unassign-value (a-random-element-from-list vars))
	(unassign-min-preference-until-no-conflicts csp))))


(defmethod UNASSIGN-LOWEST-EFFICIENCY-VARS ((csp XTE-CSP))
  
  (let ((var-eff-list nil))
    (dolist (var (all-vars-with-conflicts csp))
	    (push (cons var (/ (duration-given-start-at (task var) (assigned-value var))
			       (minimum-nonzero-duration (task var))))
		  var-eff-list))
    (setf var-eff-list (sort var-eff-list #'> :key #'rest))
    ;; (format t "~%unassign order: ~a" var-eff-list)
    (loop
     (if (null var-eff-list) (return nil))
     (let* ((var-eff (pop var-eff-list))
	    (var (first var-eff))
	    (eff (rest var-eff)))
       (cond ((= eff 1) 
	      (unassign-min-preference-until-no-conflicts csp)
	      (return nil))
	     (t (assigned-value-has-conflicts-p var)
		(unassign-value var)))))))
============|#



#| test
(setf (restriction-time zzz) nil)
(non-zero-intervals zzz)

(make-XTE-CSP "XTE")
(csp-state-summary *XTE-csp*)
(show-csp-state *XTE-csp* :preferences t)
(describe (var *XTE-csp* 0))
zzz
(describe *XTE-chronika*)
(fdsia (lookup-task *xte-chronika* 'dummy-3))

(remove-domain-values-in-range-list
 *XTE-csp* (var *XTE-csp* 0)
 (times-excluded-by-suitability *XTE-csp* zzz) :permanent t)
(remove-domain-values-in-range-list
 *XTE-csp* (var *XTE-csp* 0)
 '((10 30)) :permanent t)
(times-excluded-by-boundary *XTE-csp* zzz)
(earliest-start zzz)
(latest-start zzz)
(start-given-end-at zzz 60)
(value-preference-list zzz)

(XTE-init-prep-and-load '("XTE-test.targ"))
(XTE-init-prep-and-load '("/grendel/data1/may91/xte/xte-test.targ"))
(non-zero-intervals (lookup-task *XTE-chronika* 'dummy-1))
(non-zero-intervals (lookup-task *XTE-chronika* 'dummy-2))
(non-zero-intervals (lookup-task *XTE-chronika* 'dummy-3))

(var-overlap-effect *XTE-csp* (var-named *XTE-csp* 'dummy-1) 5)
(var-overlap-effect *XTE-csp* (var-named *XTE-csp* 'dummy-2) 12)
(var-overlap-effect *XTE-csp* (var-named *XTE-csp* 'ad_leo) 25)
(var-overlap-effect *XTE-csp* (var-named *XTE-csp* 'mk_421) 2)
(var-overlap-effect *XTE-csp* (var-named *XTE-csp* 'mk_421) 3)

(var-effect *XTE-csp* (var-named *XTE-csp* 'dummy-1) 25)
(var-effect *XTE-csp* (var-named *XTE-csp* 'ad_leo) 12)
(var-effect *XTE-csp* (var-named *XTE-csp* 'ad_leo) 25)
(var-effect *XTE-csp* (var-named *XTE-csp* 'mk_421) 2)
(var-effect *XTE-csp* (var-named *XTE-csp* 'mk_421) 3)


(print (bin-usage *xte-capcon*))
(check-capacity-constraint *xte-capcon*)
(reset-csp *XTE-csp*)
(show-csp-state *XTE-csp*)
(assign-value (var-named *XTE-csp* 'dummy-1) 25)
(assign-value (var-named *XTE-csp* 'dummy-2) 24)
(assign-value (var-named *XTE-csp* 'dummy-3) 23)
(assigned-value (var-named *XTE-csp* 'dummy-1))
(assigned-time (var-named *XTE-csp* 'dummy-1))
(unassign-value (var-named *XTE-csp* 'dummy-1))
(unassign-value (var-named *XTE-csp* 'dummy-2))
(unassign-value (var-named *XTE-csp* 'dummy-3))

(vars-assigned-to-bin (obs-cap-constraint *current-obs-csp-chronika*) 0)
(xte-schedule-report *xte-csp*)
|#



;;; -----------------------------------------------------------------------
;;; POSTSCRIPT PLOTS OF XTE CSP
;;; -----------------------------------------------------------------------

#|
(generate-XTE-csp-plot "clipper:1xte-long-term.ps"
		       :obs-csp-chronika *xte-long-term-schedule*)
(generate-XTE-csp-plot "/grende/data1/may91/xte/temp.ps"
		       :obs-csp-chronika *xte-long-term-schedule*)
|#

(defun GENERATE-XTE-CSP-PLOT (file &key obs-csp-chronika)
  
  "Given file name, write postscript plot file for specified CSP"
  (let* ((csp (obs-csp obs-csp-chronika))
	 (chronika (obs-chronika obs-csp-chronika))
	 (qstart (qtime-start chronika))
	 (qend (qtime-end chronika))
	 (tjd-start (qtime-to-tjd chronika qstart))
	 (tjd-end (qtime-to-tjd chronika qend)))
    
    (let* ((gp (make-ps-gwindow :file file))
	   ;; page loc of rect left, right: same throughout
	   (pleft 60) (pright 550)
	   ;; window loc left right: same throughout
	   (wleft 0.1) (wright 0.98)
	   ;; current page top bottom
	   (initial-page-top 700)
	   (ptop nil) 
	   (pbottom initial-page-top) ;; this is the initial value of page top
	   )
      
      ;; setup
      (record-prolog gp)
      
      (XTE-plot-csp-header
       csp :where gp
       :page-left pleft :page-right pright :page-bottom initial-page-top 
       :page-top (+ initial-page-top 50))
      
      (let* ((assigned-vars (all-assigned-vars csp))
	     (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
	     (unassigned-vars (all-unassigned-vars csp)))
	
	(dolist (var (append sorted-vars unassigned-vars))
		
		(let* ((task (task var)))
		  
		  (format t "~%Task: ~a" task)
		  (setf ptop pbottom pbottom (- pbottom 35)) ;number is height of rect
		  
		  (XTE-csp-var-plot
		   :where gp :var var :task task :label-string ""
		   :page-left pleft :page-right pright :page-bottom pbottom 
		   :page-top ptop :window-left wleft :window-right wright)
		  
		  (when (< pbottom 100)
			
			;; DRAW TIME AXIS OVER WHOLE PAGE
			(set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
			(set-loc-of-drawing-window gp wleft wright 0 1)
			(set-x-window-scale gp tjd-start tjd-end)
			(time-label gp tjd-start tjd-end :label t
				    :min-pixels-for-tics 5
				    :min-pixels-for-labels 20)
			
			
			;; new page
			(record-nextpage gp)
			;; header on new page
			(setf ptop nil pbottom initial-page-top)
			(XTE-plot-csp-header
			 csp :where gp
			 :page-left pleft :page-right pright 
			 :page-bottom initial-page-top :page-top (+ initial-page-top 50))
			
			
			))))
      
      ;; DRAW TIME AXIS OVER WHOLE PAGE
      (set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
      (set-loc-of-drawing-window gp wleft wright 0 1)
      (set-x-window-scale gp tjd-start tjd-end)
      (time-label gp tjd-start tjd-end :label t
		  :min-pixels-for-tics 5
		  :min-pixels-for-labels 20)
      
      (record-wrapup gp))))


(defun XTE-PLOT-CSP-HEADER 
  (csp
   &key
   where page-left page-right page-bottom page-top)
  
  "Plot header for CSP"
  
  ;; page-left page-right page-bottom page-top
  (set-page-loc-of-drawing-rect where page-left page-right page-bottom page-top)
  ;; window-left window-right window-bottom window-top
  (set-loc-of-drawing-window where 0 1 0 1)
  (record-font where "Helvetica-Bold" 9)
  (string-at-line-in-rect
   where :line-number 1 :font-size 9
   :string (format nil "Spike STScI ~a, p. ~a" 
		   (get-formatted-universal-time)
		   (current-page-number where)))
  
  (record-font where "Helvetica" 9)
  (string-at-line-in-rect
   where :line-number 3 :font-size 9
   :string (format nil " ~d tasks, ~a assigned, ~a assigned with conflicts"
		   (var-count csp) (assigned-var-count csp) (conflicting-var-count csp)))
  (string-at-line-in-rect 
   where :line-number 4 :font-size 9
   :string (format nil " mean preference: ~,2f mean conflicts: ~,2f  summed preference: ~,d summed conflicts: ~d"
		   (mean-preference csp) (mean-conflict-count csp) (summed-preference csp) (summed-conflict-count csp)))
  (string-at-line-in-rect 
   where :line-number 5 :font-size 9
   :string (format nil " scheduled duration ~,2f days, min duration ~,2f days, gaps ~,2f days"
		   (total-scheduled-duration csp) (total-min-duration csp)
		   (total-gaps csp)))
  )


(defun XTE-CSP-VAR-PLOT (&key where var task
			      label-string				      
			      page-left page-right page-bottom page-top
			      window-left window-right)
  
  "Plot XTE CSP var.  Size of 50 works well."
  
  ;; NOTE: preferences assumed to run 0 to 100
  (let* ((chronika (chronika task))
	 (csp (csp var))
	 (cap-con (obs-cap-constraint (obs-csp-chronika csp)))
	 (scale-factor 1.2)
	 (assignment (assigned-value var))
	 (current-bin
	  (if assignment (value-to-bin csp cap-con var assignment)))
	 (bin-start (if current-bin (start-of-segment csp current-bin)))
	 (bin-end (if current-bin (+ (start-of-segment csp current-bin)
				     (duration-of-segment csp current-bin))))
	 ;;(start-end (multiple-value-list (assigned-time var)))
	 ;;(start-time (first start-end))
	 ;;(end-time (second start-end))
	 (assignment-conf (assigned-value-has-conflicts-p var))
	 )
    
    
    (time-value-plot
     where :start-time (qtime-start chronika) :end-time (qtime-end chronika)
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right :window-bottom 0.1 :window-top 0.80
     ;; :window-left window-left :window-right window-right :window-bottom 0.5 :window-top 0.82
     :pcf (suitability task)
     :linewidth 0.25
     :rect-title (format nil "~a  RA: ~,3f (~a) Dec: ~,3f (~a)  Exp. time ~a sec  Priority: ~a ~a"
			 (target-name task)
			 (target-ra task)
			 (let ((hms (degrees-of-ra-to-hms (target-ra task))))
			   (format nil "~dh ~2,'0dm ~,1fs" (first hms) (second hms) (third hms)))
			 (target-dec task)
			 (let ((dms (degrees-of-dec-to-dms (target-dec task))))
			   (format nil "~dd ~2,'0dm ~ds" (first dms) (second dms) (round (third dms))))
			 (exp-time task)
			 (priority task)
			 label-string)
     :rect-font-size 6
     :window-title (XTE-task-plot-string var) :window-font-size 5
     :y-scale-min 0 :y-scale-max scale-factor ; pref-scale 
     :box-rect t :box-window t)
    (set-y-window-scale where 0 scale-factor)
    (label-y-axis where 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
    
    (set-page-loc-of-drawing-rect where page-left page-right page-bottom page-top) ;LRBT
    (set-loc-of-drawing-window where window-left window-right 0.0 0.1) ;LRBT
    ;; (set-loc-of-drawing-window window-left window-right 0.4 0.5) ;LRBT
    (set-x-window-scale where (qtime-start chronika) (qtime-end chronika))
    (set-y-window-scale where 0 1)
    (box-window where)
    
    ;; draw GRAY for conflicts, black for OK
    (when assignment
	  (plot-interval-list
	   where (list (list bin-start bin-end 1))
	   :fill (if assignment-conf 0.5 0)))
    
    ))


(defmethod XTE-TASK-PLOT-STRING ((var XTE-VAR))
  "Return string describing scheduling status of XTE var, for postscript plots"
  (let* ((task (task var))
	 (chronika (chronika task))
	 ;;(name (target-name task))
	 ;;(exp-time (exp-time task))
	 (assignment (assignment var))
	 (start-end (multiple-value-list (assigned-time var)))
	 (start (first start-end))
	 ;;(end (second start-end))
	 (conf (conflicts-on-assigned-value var))
	 (max-pref (max-preference var))
	 ;;(min-duration (minimum-nonzero-duration task))
	 ;; only valid if start is known
	 ;;(effic (if start (efficiency-at-time task start)))
	 (duration (if start (duration-at-time task start)))
	 (pref (if assignment (value-preference var assignment)))
	 ;;(act-exp-time (if start (exposure-duration-at-time task start)))
	 )
    (if start
	(format nil "Start: ~16a (~d), dur: ~5,2fd, #confl:~d, pref:~5,2f pref/max:~5,2f"
		(task-formatted-abs-qtime task start) start 
		(duration-in-qtime-to-days chronika duration)
		conf pref (/ pref max-pref))
      ;; unassigned
      (format nil "-- not scheduled --"))))


;;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; +                SHORT TERM SCHEDULING FOR ASTRO-D                   +
;;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;;; ======================================================================
;;;                                  CLASS: XTE-SHORT-TERM-CSP-CHRONIKA

(defclass XTE-SHORT-TERM-CSP-CHRONIKA (OBS-CSP-CHRONIKA)
  ((slew-rate 
    :reader slew-rate
    :documentation "slew rate, degrees/minute"
    )
   (abs-adjust 
    :reader abs-adjust
    :documentation 
    "amount in seconds by which exp-time can be adjusted without impact"
    )
   (rel-adjust
    :reader rel-adjust
    :documentation
    "fraction of exp-time which can be added/subtracted without impact"
    )
   (min-segment-size
    :reader min-segment-size
    :documentation
    "minimum exposure time on target in seconds for an exposure which
must be broken into segments"
    )   
   )
  (:documentation "short term scheduling for XTE"))


(defmethod INIT-OBS-CSP-CHRONIKA :AFTER ((occ XTE-SHORT-TERM-CSP-CHRONIKA)
					 args)
  (let ((rate (getf args :slew-rate))
	(abs-adjust (getf args :abs-adjust))
	(rel-adjust (getf args :rel-adjust))
	(min-segment-size (getf args :min-segment-size))
	)
    (setf (slot-value occ 'slew-rate) rate)
    (setf (slot-value occ 'abs-adjust) abs-adjust)
    (setf (slot-value occ 'rel-adjust) rel-adjust)
    (setf (slot-value occ 'min-segment-size) min-segment-size)
    ))


(defmethod CHRONIKA-INITARGS ((occ XTE-SHORT-TERM-CSP-CHRONIKA))
  (append (list :slew-rate (slew-rate occ))
	  (call-next-method occ)))


(defmethod TASK-INITARGS ((occ XTE-SHORT-TERM-CSP-CHRONIKA))
  (append (list :abs-adjust (abs-adjust occ)
		:rel-adjust (rel-adjust occ)
		:min-segment-size (min-segment-size occ))
	  (call-next-method)))



;;; ======================================================================
;;;                                      CLASS: XTE-SHORT-TERM-CHRONIKA

(defclass XTE-SHORT-TERM-CHRONIKA (OBS-CHRONIKA)
  ((slew-rate :reader slew-rate))
  (:documentation "foo"))


(defmethod INIT-CHRONIKA :AFTER ((chr XTE-SHORT-TERM-CHRONIKA) args)
  (let ((rate (getf args :slew-rate)))
    (setf (slot-value chr 'slew-rate) rate)))


;;; ======================================================================
;;;                                          CLASS: XTE-SHORT-TERM-TASK

(defclass XTE-SHORT-TERM-TASK (FIXED-OBS-TASK)
  ((tjd-abs-intervals 
    :initform nil :reader tjd-abs-intervals
    :documentation
    "absolute constraint intervals, time in TJD, value is suitability of
      overlap.  Not start to start suitability, but overlap.
      Like returned from event calculator."
    )
   (interrupted-intervals
    :initform nil :accessor interrupted-intervals
    :documentation
    "interrupted intervals for this task as scheduled via laydown-all
      across occultations, SAA, and other events.  Use the function 
      SCHED-INTERVALS-FROM-LAYDOWN to return intervals as a function
      of start time.")

   ;; new slots mdj/nov94 to hold telemetry rate, non-interrupt, shadow-only
   (telemetry
    :initform 0 :accessor telemetry
    :documentation "telemetry rate when taking data, kbits/sec")
   (non-interrupt
    :initform nil :accessor non-interrupt
    :documentation "flag, t if data must be taken in a non-interruptible
      period, nil otherwise")
   (shadow-only
    :initform nil :accessor shadow-only
    :documentation "flag, t if data must be taken in shadow only, 
      nil otherwise")
   )
  (:documentation "short-term XTE task"))


(defmethod INIT-TASK :AFTER ((task xte-short-term-task) args)
  (setf (slot-value task 'tjd-abs-intervals)
	(remove-if
	 #'(lambda (x) (= 0 (third x)))
	 (getf args :abs-constraint-intervals)))
  ;; mdj/nov94: store the values for telemetry, non-int, shadow
  (setf (slot-value task 'telemetry) (getf args :telemetry))
  (setf (slot-value task 'non-interrupt) (getf args :non-interrupt))
  (setf (slot-value task 'shadow-only) (getf args :shadow-only))
  )


;;; this overrides the default call to compute-task-duration-ipcf
;;; in order to do a more detailed layout of short term task scheduling

(defmethod INIT-TASK-DURATION-IPCF ((task XTE-SHORT-TERM-TASK) args)
  ;; This method is for short-term tasks laid out around interruptions
  ;; efficiency is computed.
  "return multiple values:
efficiency-ipcf - the ipcf for recording efficiency to store in the 
task slot
duration-ipcf - the ipcf possibly with fractional quanta duration for
recording in the task slot.
duration-interval-list - an interval list of the form
((early-start late-start duration)...) for constructing the FDSIA.  
times and duration values must be integral.
abs-interval-list - interval list ((start end suit)...), in qtime, 
with suitability of overlapping interval
All args available to init-task are available in args plist."

(let* ( ;; needed args
       (chr (getf args :chr))
       (exp-time (getf args :exp-time))
       ;; efficiency: not used
       ;;(available-time-intervals (getf args :available-time-intervals))
       (abs-constraint-intervals (getf args :abs-constraint-intervals))
       (abs-adjust-sec (getf args :abs-adjust))
       (rel-adjust (getf args :rel-adjust))
       (min-segment-size-sec (getf args :min-segment-size 0))
       
       ;; derived quantities
       (abs-adjust (if abs-adjust-sec
		       (duration-in-days-to-fractional-qtime
			chr (/ abs-adjust-sec 86400.0))))
       (min-segment-size
	(duration-in-days-to-qtime chr (/ min-segment-size-sec 86400)))
       (fractional-exp-qtime (duration-in-days-to-fractional-qtime 
			      chr (/ exp-time 86400.0)))
       (exp-qtime (round fractional-exp-qtime))
       (duration-ipcf nil)
       (efficiency-ipcf nil)
       (abs-ipcf (convert-tjd-interval-list-to-qtime-ipcf
		  chr abs-constraint-intervals))
       (abs-interval-list nil)
       (abs-intervals-excluding-zero
	(remove-if 
	 #'(lambda (x) (= (third x) 0))
	 (ipcf-to-interval-list abs-ipcf (qtime-start chr) (qtime-end chr))))
       )
  
  ;;(format t "~%abs-int ex 0: ~a" abs-intervals-excluding-zero)
  (multiple-value-bind
   ;; returned interval lists are (early-start late-end dur or eff)
   (duration-interval-list efficiency-interval-list detail-intervals)
   (laydown-all task exp-qtime 
		abs-intervals-excluding-zero
		;; these should be task initargs
		min-segment-size
		rel-adjust
		abs-adjust
		:details t 
		:default-dur :nominal
		)
   ;; the returned duration intervals are in form (early-start late-end duration)
   ;; need to convert to (early-start late-start duration)
   (dolist (int duration-interval-list)
	   (setf (second int)
		 (- (second int) (third int))))
   ;;(format t "~%int list s-s: ~a" duration-interval-list)
   
   ;; store details in task:
   (setf (interrupted-intervals task) detail-intervals)
   
   ;; compute remaining quantities
   (setf efficiency-ipcf (ipcf-from-interval-list efficiency-interval-list))
   (setf duration-ipcf (ipcf-from-interval-list duration-interval-list))
   (setf abs-ipcf
	 (ipcf-multiply abs-ipcf efficiency-ipcf))
   (setf abs-interval-list
	 (ipcf-to-interval-list abs-ipcf (qtime-start chr) (qtime-end chr)))
   
   ;; returned values
   (values efficiency-ipcf 
	   duration-ipcf
	   duration-interval-list 
	   abs-interval-list))))

(defmethod DETAIL-INTERVALS ((task XTE-SHORT-TERM-TASK) start-time)
  "input task and start-time (qtime), return detailed intervals (in qtime)
for task ((start1 end1)(start2 end2)...)"
  (SCHED-INTERVALS-FROM-LAYDOWN
   start-time (interrupted-intervals task)))


(defmethod TASK-TO-TASK-OVERHEAD-IN-DAYS ((task1 XTE-SHORT-TERM-TASK)
					  (task2 XTE-SHORT-TERM-TASK))
  ;; return overhead (separation) time in days
  (let* ((chronika (chronika task1))
	 (rate (slew-rate chronika)); rate is deg/minute
	 (sep (task-angular-separation task1 task2)); sep in degrees
	 (time (/ sep rate)))
    ;; time is in minutes, convert to days, then quanta
    (/ time 1440)))


(defmethod TASK-TO-TASK-OVERHEAD ((task1 XTE-SHORT-TERM-TASK)
				  (task2 XTE-SHORT-TERM-TASK))
  ;; return overhead (separation) time in quanta
  (let* ((chronika (chronika task1)))
    (ceiling 
     (duration-in-days-to-fractional-qtime 
      chronika (task-to-task-overhead-in-days task1 task2)))))



(defmethod TASK-FORMATTED-ABS-TIME ((task XTE-SHORT-TERM-TASK) TJD)
  (let ((s (format-abs-time TJD)))
    (concatenate 'string
		 " "
		 (subseq s 0 2)
		 (subseq s 3 6)
		 (subseq s 9 11)
		 (subseq s 11 18))))

(defun XTE-SLEW-TIME-IN-DAYS (targ1-vector targ2-vector
				  sun-vector rate)
  "Give task to task overhead, doing a slew that maintains sun vector 
  perpendicular to the axis of XTE's solar panels. Slew rate is in
  degrees per minute. A minimum setup time is included."
  (let* ((orient1 (sun-avoiding-matrix targ1-vector sun-vector))
	 (orient2 (sun-avoiding-matrix targ2-vector sun-vector))
	 (sep (orientation-angular-separation orient1 orient2)) ; sep in deg
	 (minutes (+ *XTE-SLEW-SETUP-TIME* (/ sep rate))))
    ;; time is in minutes, convert to days, then quanta
    (/ minutes 1440)))

(defun SUN-AVOIDING-MATRIX (xtev sunv)
  "Given an XTE target position vector xtev, and the current sun vector
   sunv, give an orientation matrix for the XTE that points to the
   target and is so rotated around the XTE-target axis that the sun
   vector is perpendicular to the rotation axis of the solar panels."
  (let ((y-axis (3vect-normalize
	( 3vect-cross-product xtev sunv))))
    (list xtev
	  y-axis
	  (3vect-cross-product xtev y-axis)))
  )

(defmethod ORIENTATION-ANGULAR-SEPARATION (orient1 orient2)
  "Given 2 orientation matrices, determine the size in degrees of the
   rotation required to pass from the first orientation to the second."
  (let* ((sep nil)
	 (rot-matrix
	  (3x3matrix-times-matrix
	   (3x3matrix-transpose orient1)
	   orient2))
	 (trace (+ (first (first rot-matrix))
		   (second (second rot-matrix))
		   (third (third rot-matrix)))))
    (setf sep 
      (* (/ 180 pi) (acos 
		     (/ (- trace 1) 2))))
    (if (equalp (type-of sep) 'complex)
	(realpart sep)
      sep))
)

;;; ======================================================================
;;;                                           CLASS: XTE-SHORT-TERM-CSP

(defclass
  XTE-SHORT-TERM-CSP (OBS-CSP)
  (
   ;; optimization
   (optimized-p
    :reader optimized-p	:initform nil
    :documentation "t if schedule optimized, nil otherwise")
   (optimization-table
    :reader optimization-table :initform nil
    :documentation "hash table for task optimization")
   (optimized-total-exp-time 
    :reader optimized-total-exp-time :initform nil
    :documentation "summed total exposure time for optimized schedule, days")
   (optimized-total-overhead
    :reader optimized-total-overhead :initform nil
    :documentation "summed total overhead for optimized schedule, days")
   )
  (:documentation "foo"))


;;; ======================================================================
;;;                                           CLASS: XTE-SHORT-TERM-VAR

(defclass XTE-SHORT-TERM-VAR (OBS-VAR)
  ()
  (:documentation "xte short term var"))


(defun MAKE-XTE-SHORT-TERM-TASK 
  (&rest args 
	 &key (obs-csp-chronika *CURRENT-OBS-CSP-CHRONIKA*)
	 &allow-other-keys)
  (let ((args (append args (task-initargs obs-csp-chronika))))
    (apply 'make-task args)))

#|
(show-xte-short-term-var 
 (var (obs-csp *xte-short-term-schedule*) 0))
|#

;;; these methods record commit times automatically on the symbol
;;; property lists:

(defmethod ASSIGN-VALUE :AFTER ((v XTE-SHORT-TERM-VAR) value)
  (let ((task (task v)))
    (cond (value
	   (set-short-term-commitment-time 
	    (id task)
	    (qtime-to-TJD 
	     (chronika task)
	     (FDSIA-val-to-time (FDSIA task) value)
	     )))
	  (t ;; no value
	   (set-short-term-commitment-time
	    (id task) nil)))))


(defmethod UNASSIGN-VALUE :AFTER ((v XTE-SHORT-TERM-VAR))
  (let ((task (task v)))
    (set-short-term-commitment-time
     (id task) nil)))


(defmethod RE-INITIALIZE-VARIABLE :AFTER ((v XTE-SHORT-TERM-VAR))
  (let ((task (task v)))
    (set-short-term-commitment-time
     (id task) nil)))




(defmethod SHOW-XTE-SHORT-TERM-VAR ((v XTE-SHORT-TERM-VAR))
  (let* ((value-count (value-count v))
	 (task (task v))
	 (chr (chronika task))
	 (FDSIA (FDSIA task))
	 (TJD-abs-pcf (pcf-from-interval-list (TJD-abs-intervals task)))
	 (qtime-abs-pcf (convert-tjd-interval-list-to-qtime-ipcf
			 chr (TJD-abs-intervals task))))
    (format t "~%var: ~a task: ~a" (var-number v)
	    task)
    (format t "~%val  start end dur pref  effic dur   start TJD                 abs suit")
    (dotimes (i value-count)
	     (multiple-value-bind
	      (start end)
	      (FDSIA-val-to-time FDSIA i)
	      
	      (format t "~%~3d:~a~4d-~4d ~3d ~3d  ~6,3f ~5a ~a <~a ~a ~a> ~4a" 
		      i (if (value-in-domain-p v i) " " "x")
		      start end
		      (FDSIA-duration-given-start-at FDSIA start)
		      (value-preference v i)
		      
		      (ipcf-get-value start (efficiency-ipcf task))
		      (ipcf-get-value start (duration-ipcf task))
		      (format-abs-time (qtime-to-TJD chr start))
		      (pcf-get-value (qtime-to-TJD chr (1- start)) TJD-abs-pcf)
		      (pcf-get-value (qtime-to-TJD chr start) TJD-abs-pcf)
		      (pcf-get-value (qtime-to-TJD chr (1+ start)) TJD-abs-pcf)
		      (ipcf-get-value start qtime-abs-pcf)
		      ))))
  )


;;; ----------------------------------------------------------------------
;;; Constructor: make short term schedule from existing long term schedule
;;; ----------------------------------------------------------------------

;;; *BUG* this will crash unless there is a pass list available or calculate
;;; fixed 1May92 MDJ to not require pass list to be passed in.  But will
;;; still need work in overhead routines.

(defun MAKE-XTE-SHORT-TERM-SCHEDULE (segment-number)
  
  "Input: segment number from current long term schedule"
  
  ;;   the next line is added 6-9-92
  ;;   this is used by ST-UNOBSERVED-TARGETS in sched-short-term.lisp
  (setf *short-term-segment-number* segment-number)
  
  ;; the next lines are added 8-26-92
  (get-original-target-list)
  (lt-unobserved-targets *original-target-list*)
  (rest-of-long-term-targets *short-term-segment-number*)
  
  (let* ((long-term-obs-csp *XTE-LONG-TERM-SCHEDULE*)
	 (csp (obs-csp long-term-obs-csp))
	 (vars (vars-assigned-to-bin
		(obs-cap-constraint long-term-obs-csp) segment-number))
	 (tasks (mapcar #'task vars))
	 (TJD-start (qtime-to-tjd 
		     (obs-chronika long-term-obs-csp)
		     (start-of-segment csp segment-number)))
	 (TJD-end (qtime-to-tjd 
		   (obs-chronika long-term-obs-csp)
		   (+ (start-of-segment csp segment-number)
		      (duration-of-segment csp segment-number)))
		  )
	 (short-term-obs-csp nil))
    
    ;; the next line is added 8-21-92 
    (make-saa-list TJD-start TJD-end)
    
    (setf short-term-obs-csp
	  (make-obs-csp-chronika  
	   :obs-csp-chronika-type 'xte-short-term-csp-chronika
	   :name                  (obs-csp-chronika-name long-term-obs-csp)
	   :directory             (obs-csp-chronika-directory long-term-obs-csp)
	   :tjd-start             tjd-start
	   :tjd-end               tjd-end
	   :tjd-quantum           *XTE-SHORT-TERM-TIME-QUANTUM*
	   :chronika-type         'xte-short-term-chronika
	   :task-type             'xte-short-term-task
	   :csp-type              'xte-short-term-csp
	   :var-type              'xte-short-term-var
	   :default-priority      (obs-default-priority long-term-obs-csp)
	   :suitability-scale     (obs-suitability-scale long-term-obs-csp)
	   :slew-rate             *XTE-SLEW-RATE* ;deg/min
	   :abs-adjust            *XTE-ABS-ADJUST* ;sec
	   :rel-adjust            *XTE-REL-ADJUST*
	   :min-segment-size      *XTE-MIN-SEGMENT-SIZE* ;sec
	   ))
    (open-chronika short-term-obs-csp)
    
    
    ;; make tasks
    (dolist (task tasks)
      (format t "~%target: ~a " (target-name task))
      ;; new mdj/nov94: pass through new constraint settings here
      (set-XTE-constraint-parameters
       :name (target-name task) 
       :ra (target-ra task)
       :dec (target-dec task)
       :start-date TJD-start
       :end-date TJD-end
       :orbit-model *DEFAULT-XTE-ORBIT-MODEL*
       :shadow-flag (shadow-only task)
       :non-interrupt (non-interrupt task)
       :non-interrupt-duration (/ (exp-time task) 86400.0)
       )
      (let* ((saa (get-xte-saa-short-term-pcf))
             (dark (if (shadow-only task) (get-xte-dark-short-term-pcf)
                       *unity-pcf*))
             (occ (get-xte-occ-short-term-pcf))
             (sun (get-xte-sun-pcf))
             (pcf (pcf-multiply sun (pcf-multiply occ (pcf-multiply dark saa))))
             (abs-constr-int (pcf-to-interval-list pcf TJD-start TJD-end))
             )
        (make-xte-short-term-task
         :id            (id task)
         :name          (target-name task)
         :ra            (target-ra task)
         :dec           (target-dec task)
         :exp-time      (exp-time task)
         :priority (priority task)
         ;; this is efficiency
         :available-time-intervals (list (list TJD-start TJD-end 1))
         ;; suitable times
         :abs-constraint-intervals abs-constr-int
         ;; new mdj/nov94
         :telemetry     (telemetry task)
         :non-interrupt (non-interrupt task)
         :shadow-only   (shadow-only task)
         )))				;end loop over tasks
    
    ;; copy constraints from long-term schedule
    (setf (slot-value short-term-obs-csp 'constraints)
	  (copy-list (constraints long-term-obs-csp)))
    
    ;; tasks made, close chronika
    (close-obs-csp-chronika short-term-obs-csp)
    
    ;; mdj/nov94: create capacity constraint for onboard data storage
    (make-capacity-constraint
     (obs-csp short-term-obs-csp)
     ;; --- standard args
     :capcon-type 'ONBOARD-CAP
     :name 'ONBOARD-CAPACITY
     :limit-array nil
     :remove-values-if-capacity-exceeded nil ;was t
     :remove-values-if-not-in-any-bin :permanent ; was t
     :weight 1
     ;; --- additional args for INIT-...
     :chronika (obs-chronika short-term-obs-csp)
     :interval *XTE-ONBOARD-ACCOUNTING-INTERVAL*
     :capacity *XTE-ONBOARD-CAPACITY*
     )
    (setf *XTE-SHORT-TERM-SCHEDULE* short-term-obs-csp)
    
    ;; restore commit times if known
    (dolist (task (task-list (obs-chronika *xte-short-term-schedule*)))
      (let ((commit-time (short-term-commitment-time (id task))))
        (cond ((and commit-time 
                    (>= commit-time TJD-start)
                    (<= commit-time TJD-end))
               (assign-value 
                (task-var task)
                (FDSIA-time-to-val (FDSIA task)
                                   (TJD-to-qtime 
                                    (chronika task)
                                    commit-time))))
              (commit-time
               ;; have commit time, but it's not in range: clear it
               (set-short-term-commitment-time (id task) nil))
              (t 
               ;; no action
               ))))
    ))

;;; ----------------------------------------------------------------------
;;;      make short-term schedule just from list of positions & other data
;;; ----------------------------------------------------------------------
;;; this is for testing only
;;; modified mdj/nov94 for telemetry, shadow, non-int

(defun MAKE-TEST-XTE-SHORT-TERM-SCHEDULE 
       (TJD-start TJD-end TJD-quantum exp-list
	          &key 
	          (constraint-list nil))
  
  "Input: start and end dates (TJD), quantum (days),
    list of exposures, ie list of:
      (name ra dec exptime priority telemetry non-interrupt shadow-only)
    where name is a symbol (NOT string), ra & dec are decimal degrees, and exptime
    is seconds.  All tasks have same priority."
  
  (let* ((short-term-obs-csp nil))
    
    (setf short-term-obs-csp
	  (make-obs-csp-chronika  
	   :obs-csp-chronika-type 'xte-short-term-csp-chronika
	   :name "XTE-TEST"
	   :directory nil
	   :tjd-start tjd-start
	   :tjd-end tjd-end
	   :tjd-quantum tjd-quantum
	   :chronika-type 'xte-short-term-chronika
	   :task-type     'xte-short-term-task
	   :csp-type      'xte-short-term-csp
	   :var-type      'xte-short-term-var
	   :default-priority 100
	   :suitability-scale 100
	   :slew-rate *XTE-SLEW-RATE* ;deg/min
	   :abs-adjust *XTE-ABS-ADJUST* ;sec
	   :rel-adjust *XTE-REL-ADJUST*
	   :min-segment-size *XTE-MIN-SEGMENT-SIZE* ;sec
	   ))
    (open-chronika short-term-obs-csp)
    (dolist (constraint constraint-list)
      (add-to-constraint-list short-term-obs-csp constraint))
    
    ;; make tasks
    (dolist (task exp-list)
      (format t "~%target: ~a " (first task))
      (set-XTE-constraint-parameters
       :name (first task)
       :ra (second task)
       :dec (third task)
       :start-date TJD-start
       :end-date TJD-end
       :orbit-model *DEFAULT-XTE-ORBIT-MODEL*
       :shadow-flag (eighth task)
       :non-interrupt (seventh task)
       :non-interrupt-duration (/ (fourth task) 86400.0)
       )
      (let* ((saa (get-xte-saa-short-term-PCF))
             (dark (if (eighth task) (get-xte-dark-short-term-pcf)
                       *unity-pcf*))
             (occ (get-xte-occ-short-term-pcf))
             (sun (get-xte-sun-pcf))
             (pcf (pcf-multiply sun (pcf-multiply occ (pcf-multiply dark saa))))
             (abs-constr-int (pcf-to-interval-list pcf TJD-start TJD-end))
             )
        
        (make-xte-short-term-task
         :id       (first task)
         :name     (first task)
         :ra       (second task)
         :dec      (third task)
         :exp-time (fourth task)
         :priority (fifth task)
         ;; this is efficiency
         :available-time-intervals (list (list TJD-start TJD-end 1))
         ;; suitable times
         :abs-constraint-intervals abs-constr-int
         :telemetry (sixth task)
         :non-interrupt (seventh task)
         :shadow-only (eighth task)
         ))) ;end loop over tasks
    
    
    ;; tasks made, close chronika
    (close-obs-csp-chronika short-term-obs-csp)
    
    ;;; mdj/nov94: create capacity constraint for onboard data storage
    (make-capacity-constraint 
     (obs-csp short-term-obs-csp)
     ;; --- standard args
     :capcon-type 'ONBOARD-CAP
     :name 'ONBOARD-CAPACITY
     :limit-array nil
     :remove-values-if-capacity-exceeded nil ;was t
     :remove-values-if-not-in-any-bin :permanent ; was t
     :weight 1
     ;; --- additional args for INIT-...
     :chronika (obs-chronika short-term-obs-csp)
     :interval *XTE-ONBOARD-ACCOUNTING-INTERVAL*
     :capacity *XTE-ONBOARD-CAPACITY*
     )
    (setf *XTE-SHORT-TERM-SCHEDULE* short-term-obs-csp)))

#| === example for testing above:
;; 15 feb has a cvz interval, 1 jan does not
(time
 (make-test-xte-short-term-schedule 
  (dmy-to-time '15-feb-96)  ; 6083.5 = 1 Jan 96
  (+ 1 (dmy-to-time '15-feb-96))  ; 6090.5 
  (/ 5.0 1440.0)
  ;; name          ra dec exp pri telem nonint shadow
  '((z1             0 67 10000 3)
    (z2             0 67 10000 3   50)
    (z2-nonint      0 67 10000 1   50     t    nil)
    (z2-dark        0 67 10000 1   50   nil      t)
    (z2-dark-nonint 0 67 10000 1   50     t      t)
    )
  :constraint-list nil
  ))
(check-capacity-constraint 
 (find-capacity-constraint (obs-csp *xte-short-term-schedule*) 'ONBOARD-CAPACITY))
(onboard-capacity-list 
 *xte-short-term-schedule* :capcon-name 'ONBOARD-CAPACITY
 :only-used-bins nil :stream t)
(inspect *xte-short-term-schedule*)
=== |#

;;; ----------------------------------------------------------------------
;;;                     Make long-term schedule consistent with short term
;;; ----------------------------------------------------------------------

(defun MAKE-XTE-LONG-TERM-CONFORM-TO-SHORT-TERM 
  (&key (long-term-schedule *xte-long-term-schedule*)
	(short-term-schedule *xte-short-term-schedule*))
  
  (do-for-all-vars 
   :var v :csp (obs-csp short-term-schedule)
   :form 
   (let* ((st-value (assigned-value v))
	  (st-task (task v))
	  (st-time-TJD (if st-value
			   (qtime-to-TJD
			    (chronika st-task)
			    (FDSIA-val-to-time (FDSIA st-task) st-value))))
	  (id (ID st-task))
	  (lt-task (lookup-task (obs-chronika long-term-schedule) id))
	  (lt-var (task-var lt-task))
	  (lt-old-value (assigned-value lt-var))
	  (lt-new-value (if st-time-TJD
			    (FDSIA-time-to-val (FDSIA lt-task)
					       (TJD-to-qtime
						(chronika lt-task)
						st-time-TJD)))))
     ;; change if lt-old-value and lt-new-value are not the same
     (when (not (equal lt-old-value lt-new-value))
	   (format t "~%~a: changing value from ~a to ~a"
		   id lt-old-value lt-new-value)
	   (assign-value lt-var lt-new-value)
	   ))))


;;; -----------------------------------------------------------------------
;;; POST-SCHEDULING OPTIMIZATION
;;; -----------------------------------------------------------------------

(defmethod SPANNING-TJD-INTERVALS ((var xte-short-term-var))
  
  "return TJD interval list for task that spans the scheduled task time"
  
  (let* ((task (task var))
	 (chronika (chronika task))
	 (int (tjd-abs-intervals task))
	 tjd-start tjd-end
	 tjd-start-min tjd-end-min
	 tjd-start-max tjd-end-max
	 pos-start pos-end
	 sub-int)
    (multiple-value-bind
     (start end)
     (assigned-time var)
					;      (format t "~%qtime start ~a end ~a" start end)
					;      (format t "~%intervals: ~a" int)
     (setf tjd-start (qtime-to-tjd chronika start))
     (setf tjd-start-min (qtime-to-tjd chronika (- start 1)))
     (setf tjd-start-max (qtime-to-tjd chronika (+ start 1)))
     (setf tjd-end (qtime-to-tjd chronika end))
     (setf tjd-end-min (qtime-to-tjd chronika (- end 1)))
     (setf tjd-end-max (qtime-to-tjd chronika (+ end 1)))
					;      (format t "~%tjd   start ~a end ~a" tjd-start tjd-end)
					;      (format t "~% min  start ~a end ~a" tjd-start-min tjd-end-min)
					;      (format t "~% max  start ~a end ~a" tjd-start-max tjd-end-max)
     (setf pos-start
	   (position-if #'(lambda (x)
			    (or (and (>= tjd-start (first x))
				     (<= tjd-start (second x)))
				(and (>= tjd-start-min (first x))
				     (<= tjd-start-min (second x)))
				(and (>= tjd-start-max (first x))
				     (<= tjd-start-max (second x)))))
			int))
     (setf pos-end
	   (position-if #'(lambda (x)
			    (or (and (>= tjd-end (first x))
				     (<= tjd-end (second x)))
				(and (>= tjd-end-min (first x))
				     (<= tjd-end-min (second x)))
				(and (>= tjd-end-max (first x))
				     (<= tjd-end-max (second x)))))
			int))
					;     (format t "~%pos in intervals: start ~a end ~a" pos-start pos-end)
					;     (format t "~% first interval: ~a" (elt int pos-start))
					;     (format t "~% last  interval: ~a" (elt int pos-end))
     (setf sub-int (copy-tree (subseq int pos-start (1+ pos-end))))
					;     (format t "~%subsequence: ~a" sub-int)
     (values tjd-start tjd-end (elt int pos-start) (elt int pos-end) sub-int)
     )))

;;; CAVEAT:  optimization is not automatically cleared when assignments change.
;;; It must be explicitly cleared by call to NO-OPTIMIZE.

;;; reader methods:
;;;
;;; (OPTIMIZED-TOTAL-EXP-TIME CSP) => total exposure time in days, nil if not optimized
;;;
;;; (OPTIMIZED-TOTAL-OVERHEAD CSP) => total overhead in days, nil if not optimized
;;;
;;; (OPTIMIZED-P CSP) => t if optimized, nil otherwise
;;;


(defmethod OPTIMIZED-EXP-EFFICIENCY ((obs-csp-chronika xte-short-term-csp-chronika))
  
  "Return optimized efficiency as fraction of chronika duration, or nil if not
optimized"
  
  (cond ((optimized-p (obs-csp obs-csp-chronika))
	 (let* ((csp (obs-csp obs-csp-chronika))
		(chronika (obs-chronika obs-csp-chronika))
		(tjd-chronika-start (qtime-to-tjd chronika (qtime-start chronika)))
		(tjd-chronika-end (qtime-to-tjd chronika (qtime-end chronika)))
		(tjd-chronika-dur (- tjd-chronika-end tjd-chronika-start))
		(exp-time (optimized-total-exp-time csp)))
	   (/ exp-time tjd-chronika-dur)))
	(t nil)))


(defmethod OPTIMIZED-VAR-EXP-TIME ((var xte-short-term-var))
  
  "Return optimized total exp time if available (in days), else nil"
  
  (let ((csp (csp var)))
    (when (optimized-p csp)
	  (summed-interval-duration
	   (rest (gethash var (optimization-table csp)))))))


(defmethod OPTIMIZED-VAR-OVERHEAD ((var xte-short-term-var))
  
  "Return optimized total overhead if available (in days), else nil"
  
  (let ((csp (csp var)))
    (when (optimized-p csp)
	  (first (gethash var (optimization-table csp))))))


(defmethod NO-OPTIMIZE ((csp xte-short-term-csp))
  
  "clear optimization if any"
  
  (setf (slot-value csp 'optimized-p) nil)
  (setf (slot-value csp 'optimization-table) nil)
  (setf (slot-value csp 'optimized-total-exp-time) nil)
  (setf (slot-value csp 'optimized-total-overhead) nil)
  )


(defmethod OPTIMIZABLE-P ((csp xte-short-term-csp))
  
  "return non-nil if current state of schedule permits call to
short-term-optimize"
  
  ;; currently:  must have at least one assignment, and no conflicts
  (and (> (assigned-var-count csp) 0)
       (= (conflicting-var-count csp) 0)))


(defmethod SHORT-TERM-OPTIMIZE ((csp xte-short-term-csp))
  
  "Return hash table, key var, value is 
(overhead (t1 t2)(tt3 t4)...), i.e. overhead + 
list of time intervals in TJD where var scheduled.  
Overhead is in days, from key task to next task. Nil when there
is no successor task.  Optimized."

(no-optimize csp)			;clear any existing optimization

;; only optimize when all assignments without conflicts
(when (optimizable-p csp)
      
      (let* ((result (make-hash-table))
	     (assigned-vars (all-assigned-vars csp))
	     (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time)))
	
	(let ((pred-var nil)
	      (succ-var nil)
	      start-pred end-pred qstart-pred qend-pred 
	      first-int-pred last-int-pred all-int-pred
	      qstart-succ qend-succ 
	      first-int-succ last-int-succ all-int-succ
	      
	      (start-succ nil)
	      (end-succ nil)
	      (pred-succ-overhead nil)
	      (sun-at-start-succ nil)
	      )
	  
	  ;; init
	  (setf succ-var (pop sorted-vars))
	  (multiple-value-bind
	   (qstart qend first-int last-int all-int)
	   (spanning-tjd-intervals succ-var)
	   (setf qstart-succ     qstart
		 qend-succ       qend
		 first-int-succ  first-int
		 last-int-succ   last-int
		 all-int-succ    all-int)
	   (setf start-succ (first first-int))
	   (setf end-succ (min qend (second last-int)))
	   )
	  
	  (loop
	   (setf pred-var succ-var
		 start-pred start-succ
		 end-pred end-succ
		 qstart-pred qstart-succ
		 qend-pred qend-succ
		 first-int-pred first-int-succ
		 last-int-pred last-int-succ
		 all-int-pred all-int-succ)
	   (setf succ-var (pop sorted-vars))
	   (when (not succ-var) 
		 ;;done
		 (return nil))
	   
	   (multiple-value-bind
	    (qstart qend first-int last-int all-int)
	    (spanning-tjd-intervals succ-var)
	    (setf qstart-succ     qstart
		  qend-succ       qend
		  first-int-succ  first-int
		  last-int-succ   last-int
		  all-int-succ    all-int))
	   
	    (setf sun-at-start-succ 
	      (position-vector *SUN* qstart-succ))
	    ;;; TBI: determine overhead for two tasks given sun vector.
	    (setf pred-succ-overhead
	      (xte-slew-time-in-days (pos-vector (task pred-var))
				     (pos-vector (task succ-var))
				     sun-at-start-succ
				     (slew-rate (chronika (task pred-var)))))
	   
	   ;; figure out end of pred and start of succ
	   (let* ((EF-pred (min qend-pred (second last-int-pred)))
		  (SS (max (+ EF-pred pred-succ-overhead) (first first-int-succ)))
		  (EP (min (- SS pred-succ-overhead) (second last-int-pred))))
	     (setf end-pred EP)
	     (setf start-succ SS))
	   
	   ;; pred is done: output
	   ;;(format t "~%var:  ~a start ~a end ~a" pred-var start-pred end-pred)
	   ;;(format t "~%  qstart end: ~a in JD ~a ~a"
	   ;;        (multiple-value-list (assigned-time pred-var))
	   ;;        qstart-pred qend-pred)
	   ;; copy constraint intervals and adjust to make sched intervals
	   (let ((sched-int (copy-tree all-int-pred)))
	     (setf (first (first sched-int)) start-pred)
	     (setf (second (first (last sched-int))) end-pred)
	     ;;(format t "~% all int: ~a" all-int-pred)
	     ;;(format t "~% sched int: ~a" sched-int)
	     ;;(format t "~% exp time ~a actual q ~a actual ~a"
	     ;;        (exp-time (task pred-var))
	     ;;        (exposure-duration-at-time (task pred-var)
	     ;;                                   (assigned-time pred-var))
	     ;;        (* 86400 (summed-interval-duration sched-int)))
	     (setf (gethash pred-var result)
		   (cons pred-succ-overhead sched-int))
	     
	     )
	   
	   )				;end loop
	  
	  ;; put out final task
	  ;; pred is done: output
	  ;;(format t "~%var:  ~a" pred-var)
	  ;; copy constraint intervals and adjust to make sched intervals
	  (let ((sched-int (copy-tree all-int-pred)))
	    (setf (first (first sched-int)) start-pred)
	    ;; go to end of last interval
	    ;;(format t "~% all int: ~a" all-int-pred)
	    ;;(format t "~% sched int: ~a" sched-int)
	    ;;(format t "~% exp time ~a actual q ~a actual ~a"
	    ;;        (exp-time (task pred-var))
	    ;;        (exposure-duration-at-time (task pred-var)
	    ;;                                   (assigned-time pred-var))
	    ;;        (* 86400 (summed-interval-duration sched-int)))
	    (setf (gethash pred-var result)
		  (cons nil sched-int))
	    )
	  
	  )
	;; save in object
	(setf (slot-value csp 'optimized-p) t)
	(setf (slot-value csp 'optimization-table) result)
	(let ((tot-exp 0) (tot-oh 0))
	  (maphash #'(lambda (key val)
		       (declare (ignore key))
		       (incf tot-exp (summed-interval-duration (rest val)))
		       (incf tot-oh (or (first val) 0)))
		   result)
	  (setf (slot-value csp 'optimized-total-exp-time) tot-exp)
	  (setf (slot-value csp 'optimized-total-overhead) tot-oh))
	
	result)))


;;; -----------------------------------------------------------------------
;;; POSTSCRIPT PLOTS FOR SHORT-TERM SCHEDULES
;;; -----------------------------------------------------------------------

(defun GENERATE-XTE-CSP-PLOT-SHORT-TERM 
  (file 
   &key (obs-csp-chronika *XTE-SHORT-TERM-SCHEDULE*)
   (start-fraction 0)
   (end-fraction 1.0)
   (skip-out-of-range t)
   (skip-unscheduled t)
   )
  
  "Given file name, write postscript plot file for specified CSP"
  (let* ((csp (obs-csp obs-csp-chronika))
	 (chronika (obs-chronika obs-csp-chronika))
	 (opt-table (short-term-optimize (obs-csp obs-csp-chronika)))
	 (tjd-chronika-start (qtime-to-tjd chronika (qtime-start chronika)))
	 (tjd-chronika-end (qtime-to-tjd chronika (qtime-end chronika)))
	 (tjd-chronika-dur (- tjd-chronika-end tjd-chronika-start))
	 ;; tjd plot range
	 (tjd-start (+ tjd-chronika-start (* start-fraction tjd-chronika-dur)))
	 (tjd-end (+ tjd-chronika-start (* end-fraction tjd-chronika-dur)))
	 ;; qtime plot range
	 (qtime-start (tjd-to-qtime chronika tjd-start))
	 (qtime-end (tjd-to-qtime chronika tjd-end))
	 ;; summary of optimized schedule
	 (total-exp-time 0)		;total exp time in days
	 (total-overhead 0)		;total overhead in days
	 (total-efficiency 0)
	 )
    
    (when opt-table
	  (maphash #'(lambda (key val)
		       (declare (ignore key))
		       (incf total-overhead (or (first val) 0))
		       (incf total-exp-time (summed-interval-duration (rest val))))
		   opt-table))
    (setf total-efficiency (/ total-exp-time tjd-chronika-dur))
    
    ;; reset tjd start to integral qtimes
    (setf tjd-start (qtime-to-tjd chronika qtime-start))
    (setf tjd-end (qtime-to-tjd chronika qtime-end))
    (format t "~%plot range ~,3f (~d) to ~,3f (~d)"
	    tjd-start qtime-start tjd-end qtime-end)
    
    (let* ((gp (make-ps-gwindow :file file))
	   ;; page loc of rect left, right: same throughout
	   (pleft 60) (pright 550)
	   ;; window loc left right: same throughout
	   (wleft 0.1) (wright 0.98)
	   ;; current page top bottom
	   (initial-page-top 700)
	   (ptop nil) 
	   (pbottom initial-page-top) ;; this is the initial value of page top
	   )
      
      ;; setup
      (record-prolog gp)
      
      (XTE-plot-csp-header-short-term
       csp :where gp 
       :page-left pleft :page-right pright :page-bottom initial-page-top 
       :page-top (+ initial-page-top 50)
       :total-exp-time total-exp-time 
       :total-overhead total-overhead :total-efficiency total-efficiency)
      
      (let* ((assigned-vars (all-assigned-vars csp))
	     (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
	     (unassigned-vars (all-unassigned-vars csp)))
	
	(dolist (var (if skip-unscheduled sorted-vars
		       (append sorted-vars unassigned-vars)))
		
		
		(let* ((task (task var))
		       (opt-results (if opt-table (gethash var opt-table)))
		       (overhead (if opt-results (first opt-results)))
		       (sched-int (if opt-results (rest opt-results)))
		       (start-end (multiple-value-list (assigned-time var)))
		       (skip-var nil))
		  
		  ;; decide whether to skip
		  (setf skip-var (and skip-out-of-range
				      (first start-end)
                                      (second start-end)
				      (or (< (second start-end) qtime-start)
					  (> (first start-end) qtime-end))))
		  (unless skip-var
			  
			  (format t "~%Task: ~a" task)
			  (setf ptop pbottom pbottom (- pbottom 40)) ;number is height of rect
			  
			  (XTE-csp-var-plot-SHORT-TERM
			   :where gp :var var :task task :overhead overhead 
			   :sched-int sched-int
			   :qstart qtime-start :qend qtime-end :tstart tjd-start :tend tjd-end
			   :label-string ""
			   :page-left pleft :page-right pright :page-bottom pbottom 
			   :page-top ptop :window-left wleft :window-right wright)
			  
			  (when (< pbottom 100)
				
				;; DRAW TIME AXIS OVER WHOLE PAGE
				(set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
				(set-loc-of-drawing-window gp wleft wright 0 1)
				(set-x-window-scale gp tjd-start tjd-end)
				(time-label gp tjd-start tjd-end :label t
					    :min-pixels-for-tics 5
					    :min-pixels-for-labels 20)
				
				;; insert the following to start a new page
				(record-nextpage gp)
				
				(setf ptop nil pbottom initial-page-top)
				(XTE-plot-csp-header-SHORT-TERM
				 csp :where gp
				 :page-left pleft :page-right pright :page-bottom initial-page-top :page-top (+ initial-page-top 50)
				 :total-exp-time total-exp-time :total-overhead total-overhead 
				 :total-efficiency total-efficiency))
			  
			  ))))
      
      ;; DRAW TIME AXIS OVER WHOLE PAGE
      (set-page-loc-of-drawing-rect gp pleft pright pbottom initial-page-top)
      (set-loc-of-drawing-window gp wleft wright 0 1)
      (set-x-window-scale gp tjd-start tjd-end)
      (time-label gp tjd-start tjd-end :label t
		  :min-pixels-for-tics 5
		  :min-pixels-for-labels 20)
      
      (record-wrapup gp))))


(defun XTE-PLOT-CSP-HEADER-SHORT-TERM
  (csp
   &key where
   page-left page-right page-bottom page-top
   total-exp-time total-overhead total-efficiency)
  
  "Plot header for CSP - short term version"
  
  ;; page-left page-right page-bottom page-top
  (set-page-loc-of-drawing-rect 
   where page-left page-right page-bottom page-top)
  ;; window-left window-right window-bottom window-top
  (set-loc-of-drawing-window 
   where 0 1 0 1)
  (record-font where "Helvetica-Bold" 9)
  (string-at-line-in-rect
   where :line-number 1 :font-size 9
   :string (format nil "Spike STScI (short-term scheduling) ~a, p. ~a" 
		   (get-formatted-universal-time)
		   (current-page-number where)))
  
  (record-font where "Helvetica" 9)
  (string-at-line-in-rect 
   where :line-number 3 :font-size 9
   :string 
   (with-output-to-string (stream)
			  (format stream " ~d tasks, ~a assigned, ~a assigned with conflicts"
				  (var-count csp) (assigned-var-count csp) 
				  (conflicting-var-count csp))
			  (format stream " exp time: ~,3fd, overhead ~,3fd, efficiency: ~,2f%"
				  total-exp-time total-overhead (* 100 total-efficiency))))
  
  (string-at-line-in-rect 
   where :line-number 4 :font-size 9
   :string 
   (with-output-to-string (stream)
			  (format stream " mean preference: ~,2f mean conflicts: ~,2f"
				  (mean-preference csp) (mean-conflict-count csp))
			  (format stream "  summed preference: ~,d summed conflicts: ~d"
				  (summed-preference csp) (summed-conflict-count csp))))
  
  (string-at-line-in-rect 
   where :line-number 5 :font-size 9
   :string (format 
	    nil " scheduled duration ~,2f days, min duration ~,2f days, gaps ~,2f days"
	    (total-scheduled-duration csp) 
	    (total-min-duration csp)
	    (total-gaps csp)))
  
  )


(defun XTE-CSP-VAR-PLOT-SHORT-TERM
  (&key var task where overhead sched-int
	label-string	
	qstart qend tstart tend
	page-left page-right page-bottom page-top
	window-left window-right)
  
  "Plot XTE CSP var.  Size of 50 works well."
  
  ;; NOTE: preferences assumed to run 0 to 100
  (let* (;;(chronika (chronika task))
	 ;;(csp (csp var))
	 ;;(FDSIA (FDSIA task))
	 ;;(pref-curve nil)
	 (scale-factor 1.5)
	 (assignment (assigned-value var))
	 (start-end (multiple-value-list (assigned-time var)))
	 (start-time (first start-end))
	 (end-time (second start-end))
	 (assignment-conf (assigned-value-has-conflicts-p var))
	 )
    (time-value-plot
     where :start-time qstart :end-time qend
     :page-left page-left :page-right page-right :page-bottom page-bottom :page-top page-top
     :window-left window-left :window-right window-right 
     :window-bottom 0.35 :window-top 0.80
     :pcf (suitability task) :linewidth 0.25
     :rect-title (format nil "~a  RA: ~,3f (~a) Dec: ~,3f (~a)  Exp. time ~a sec  Priority: ~a ~a"
			 (target-name task)
			 (target-ra task)
			 (let ((hms (degrees-of-ra-to-hms (target-ra task))))
			   (format nil "~dh ~2,'0dm ~,1fs" (first hms) (second hms) (third hms)))
			 (target-dec task)
			 (let ((dms (degrees-of-dec-to-dms (target-dec task))))
			   (format nil "~dd ~2,'0dm ~ds" (first dms) (second dms) (round (third dms))))
			 (exp-time task)
			 (priority task)
			 label-string)
     :rect-font-size 6
     :window-title (XTE-task-plot-string-short-term var) :window-font-size 5
     :y-scale-min 0 :y-scale-max scale-factor :box-rect t :box-window t)
    (set-y-window-scale where 0 scale-factor)
    (label-y-axis where 0 scale-factor :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 scale-factor :tic-value 0.5 :tic-size 3 :label t :label-format "~,1f") 
    
    ;; draw qtime where assigned
    (set-page-loc-of-drawing-rect 
     where page-left page-right page-bottom page-top) ;LRBT
    (set-loc-of-drawing-window where window-left window-right 0.3 0.35) ;LRBT
    (set-x-window-scale where qstart qend)
    (set-y-window-scale where 0 1)
    ;; draw GRAY for conflicts, black for OK
    (when assignment
	  (plot-interval-list where (list (list start-time end-time 1))
			      :fill (if assignment-conf 0.5 0)))
    
    (set-page-loc-of-drawing-rect
     where page-left page-right page-bottom page-top) ;LRBT
    (set-loc-of-drawing-window
     where window-left window-right 0.10 0.20) ;LRBT
    (set-x-window-scale where tstart tend)
    (set-y-window-scale where 0 1)
    (plot-interval-list where (tjd-abs-intervals task) :fill 1)
    
    (when sched-int
	  (plot-interval-list where sched-int :fill 0)
	  (record-font where "Helvetica" 5)
	  (string-at-rect-left-window-bottom 
	   where :string 
	   (if overhead 
	       (format nil "~ds OH: ~ds"
		       (round (* 86400 (summed-interval-duration sched-int)))
		       (round (* 86400 overhead)))
	     (format nil "~ds"
		     (round (* 86400 (summed-interval-duration sched-int))))))
	  (when overhead
		(let ((act-end (second (first (last sched-int)))))
		  (plot-interval-list where
				      (list (list act-end (+ act-end overhead))) :fill 0.75))))
    ))


(defmethod XTE-TASK-PLOT-STRING-short-term ((var XTE-SHORT-TERM-VAR))
  "Return string describing scheduling status of XTE var, for postscript plots"
  (let* ((task (task var))
	 (chronika (chronika task))
	 ;;(name (target-name task))
	 (exp-time (exp-time task))
	 (assignment (assignment var))
	 (start-end (multiple-value-list (assigned-time var)))
	 (start (first start-end))
	 ;;(end (second start-end))
	 (conf (conflicts-on-assigned-value var))
	 (max-pref (max-preference var))
	 ;;(min-duration (minimum-nonzero-duration task))
	 ;; only valid if start is known
	 (effic (if start (efficiency-at-time task start)))
	 (duration (if start (duration-at-time task start)))
	 (pref (if assignment (value-preference var assignment)))
	 (act-exp-time (if start (exposure-duration-at-time task start)))
	 )
    (if start
	(format nil "Start: ~16a (~d), dur: ~5,2fd, exp time ~ds (req ~ds), effic ~5,2f, #confl:~d, pref:~5,2f pref/max:~5,2f"
		(task-formatted-abs-qtime task start) start 
		(duration-in-qtime-to-days chronika duration)
		act-exp-time exp-time
		effic conf pref (/ pref max-pref))
      ;; unassigned
      (format nil "-- not scheduled --"))))


(defmethod SCHEDULE-REPORT ((csp xte-short-term-csp) 
			    &key (stream t) (optimize t))
  
  "Report schedule status to stream"
  ;; (schedule-report (obs-csp -occ))
  
  ;; report all assignments and gaps
  (format stream "~%Scheduled activities:")
  (format stream
	  "~%Name            start date       start end  dur   dur    min   -exp. time---  conf suit  suit/")
  
  (format stream
	  "~%                                                (days)   dur    req  act/req    #        max")

  (if (null (all-vars csp))               ; Avoid crash on 4th let* assignment
      (progn (format stream "~%--- No tasks in this bin ---~%")
	     (return-from schedule-report nil)))
  
  (let* ((assigned-vars (all-assigned-vars csp))
	 (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
	 (opt-table (if optimize (short-term-optimize csp)))
	 (chronika (chronika (task (first (all-vars csp)))))
	 (tjd-chronika-start (qtime-to-tjd chronika (qtime-start chronika)))
	 (tjd-chronika-end (qtime-to-tjd chronika (qtime-end chronika)))
	 (tjd-chronika-dur (- tjd-chronika-end tjd-chronika-start))
	 (overall-start (start chronika))
	 (overall-end   (end chronika))
	 (prev-end overall-start)
	 (sched-days (duration-in-qtime-to-days 
		      chronika (- overall-end overall-start)))
	 (total-gap 0)
	 (total-sched 0)
	 (total-min 0)
	 (total-exp 0)
	 ;; for optimized schedule
	 (total-exp-time 0)
	 (total-overhead 0)
	 (total-efficiency 0)
	 )	      
    
    ;; report assigned vars
    (dolist (var sorted-vars)
	    (when (< prev-end (assigned-time var))
		  ;; gap
		  (format stream "~%---gap(~4d)---                  ~4d ~4d" 
			  (- (assigned-time var) prev-end) prev-end (assigned-time var))
		  (incf total-gap (- (assigned-time var) prev-end)))
	    (incf total-sched (duration-given-start-at (task var) (assigned-time var)))
	    (incf total-min (minimum-nonzero-duration (task var)))
	    (incf total-exp (exposure-duration-at-time (task var) (assigned-time var)))
	    (format stream "~%~a" (task-report var))
	    ;; flag if actual>minimum duration
	    (if (> (duration-given-start-at (task var) (assigned-time var))
		   (minimum-nonzero-duration (task var)))
		(format stream "*"))
	    
	    (when optimize
		  (let* ((task (task var))
			 (opt-results (gethash var opt-table))
			 (overhead (if opt-results (first opt-results)))
			 (sched-int (if opt-results (rest opt-results)))
			 (summed (if sched-int (summed-interval-duration sched-int))))
		    (dolist (interval sched-int)
			    (format stream "~%                ~a  -  ~a" (task-formatted-abs-time task (first interval))
				    (task-formatted-abs-time task (second interval))))
		    (format stream   "~%   Total: ~,3fd (~ds)  Overhead following task: ~ds"
			    summed (round (* 86400 summed)) (round (* 86400 (or overhead 0))))))
	    
	    (setf prev-end (max prev-end (end-given-start-at (task var) (assigned-time var)))))

    ;; gap at end?
    (when (/= prev-end overall-end)
	  (incf total-gap (- overall-end prev-end))
	  (format stream "~%---gap(~4d)---                  ~4d ~4d" 
		  (- overall-end prev-end) prev-end overall-end))
    (format stream "~%Schedule time span  : ~6,2f days" sched-days)
    (format stream "~%Total scheduled time: ~6,2f days (~6,2f%)" 
	    (duration-in-qtime-to-days chronika total-sched)
	    (* 100 (/ (duration-in-qtime-to-days chronika total-sched) 
		      sched-days)))
    (format stream "~%Total gaps          : ~6,2f days (~6,2f%)" 
	    (duration-in-qtime-to-days chronika total-gap)
	    (* 100 (/ (duration-in-qtime-to-days chronika total-gap) 
		      sched-days)))
    (when (> total-exp 0)
	  (format stream "~%Total exp time      : ~6,2f days (~d sec, ~6,2f% of schedule time span)"
		  (/ total-exp 86400.0) total-exp 
		  (* 100 (/ (/ total-exp 86400.0) sched-days))))
    (if (> total-min 0)
	(format stream "~%Total minimum duration: ~6,2f days (expansion factor actual/min: ~7,3f)"
		(duration-in-qtime-to-days chronika total-min)
		(/ total-sched total-min)))
    (when opt-table
	  (maphash #'(lambda (key val)
		       (declare (ignore key))
		       (incf total-overhead (or (first val) 0))
		       (incf total-exp-time (summed-interval-duration (rest val))))
		   opt-table)
	  (setf total-efficiency (/ total-exp-time tjd-chronika-dur))
	  (format stream "~%Optimized schedule:")
	  (format stream "~%  Total exp time : ~6,2fd (~10ds)" total-exp-time (round (* 86400 total-exp-time)))
	  (format stream "~%  Total overhead : ~6,2fd (~10ds)" total-overhead (round (* 86400 total-overhead)))
	  (format stream "~%  Efficiency     : ~6,2f%" (* total-efficiency 100)))
    
    
    (unless (all-vars-assigned-p csp)
	    (setf total-min 0)
	    (format stream "~%Unscheduled activities:")
	    (dolist (var (all-unassigned-vars csp))
		    (format stream "~%~a" (task-report var))
		    (incf total-min (minimum-nonzero-duration (task var)))
		    )
	    (format stream "~%Total minimum time not scheduled:  ~6,2f days"
		    (duration-in-qtime-to-days chronika total-min)))
    
    (format stream "~% ~d tasks, ~a assigned, ~a assigned with conflicts"
	    (var-count csp) (assigned-var-count csp) (conflicting-var-count csp))
    (format stream "~% mean preference  : ~8,2f  mean conflicts  : ~8,2f"
	    (mean-preference csp) (mean-conflict-count csp))
    (format stream "~% summed preference: ~8d  summed conflicts: ~8d"
	    (summed-preference csp) (summed-conflict-count csp))
    
    ))


(defun XTE-SHORT-TERM-REPORT (csp &key (stream t))
  
  "report short-term schedule and targets to stream"
  
  (let ((short-term-target-list nil))
    
    (do-for-all-vars
     :var v :csp csp
     :predicate (has-assigned-value-p v)
     :form (let* ((name (target-name (task v)))
		  )
	     (push (list name)
		   short-term-target-list)))
    
    (format stream "~%Target Information")
    (format stream  "~%name    ")
    (format stream "~%")
    
    (dolist (target short-term-target-list)
	    (format stream "~%~18a ~13a ~13a ~13a ~13a"
		    (first target)
		    (second target)
		    (third target)
		    (fourth target)
		    (fifth target)))
    
    (format stream "~% ")
    
    (schedule-report csp :stream stream)
    ))


(defun XTE-REPORT-CONTACT-PASS (&key (stream t))
  
  (report-pass-list *current-ground-station-pass-list*
		    :stream stream)
  
  #| Takashi's code replaced by above:
(with-slots (start-time end-time pass-list file) *CURRENT-GROUND-STATION-PASS-LIST*    
	    
	    (format stream "~%Contact Pass Report")
	    (format stream "~% Starting Time: ~a   Ending Time : ~a " start-time end-time)
	    (format stream  "~% Entry                    Exit")
	    
	    (dolist (pass pass-list)
		    (with-slots (station-abbrev type pass-start pass-end available) pass
				(let* ((comment (if available " " "not available")))
				  (format stream "~% ~20a    ~20a ~5a ~5a" pass-start pass-end station-abbrev comment)
				  )				; let*
				)				;with-slots
		    )					;dolist
	    )					;with-slots
|#
)



(defun PLOT-XTE-TASK-DETAILS (schedule &key
				       id ; task ID
				       where 
				       file
				       ;; these defaults for the Mac
				       (ptop 10)
				       (pbottom 500)
				       )
  
  (let* (;; page loc of rect left, right: same throughout
	 (pleft 60) (pright 550) ;pleft was 60, pright was 550
	 ;; window loc left right: same throughout
	 (wleft 0.1) (wright 0.98)
	 ;; current page top bottom
	 (chr (obs-chronika schedule))
	 ;;(csp (obs-csp schedule))
	 (qstart (qtime-start chr))
	 (qend (qtime-end chr))
	 (tjd-start (qtime-to-tjd chr qstart))
	 (tjd-end (qtime-to-tjd chr qend))
	 (task (lookup-task chr id))
	 ;;(var (task-var task))
	 
	 )
    (if (not task) (error "task ~a not found" id))
    
    (cond (where (clear-gwindow-history where))
	  ((and (not where) file)
	   (setf where
		 (make-ps-gwindow :file file)))
	  (t (error "either :where or :file must be specified")))
    
    (record-prolog where)
    
    (set-page-loc-of-drawing-rect
     where pleft pright pbottom ptop) ;LRBT
    (box-rect where)
    
    (record-font where "Helvetica-Bold" 9)
    (string-at-line-in-rect 
     where :line-number 1 :font-size 9
     :string (format nil "~a" id))
    
    ;; SUITABILITY
    (set-loc-of-drawing-window where wleft wright 0.8 0.95) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string "Task suitability")
    
    (set-x-window-scale where qstart qend)
    (set-y-window-scale where -0.1 1.1)
    (plot-pcf where (suitability task) :linewidth 0.25)
    (label-y-axis where 0 1.0 :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 1.0 :tic-value 0.2 :tic-size 2 :label-format "~,1f")
					;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    (plot-interval-bars where (non-zero-intervals task) -0.1 0 :fill 0)
    
    ;; DURATION
    (set-loc-of-drawing-window where wleft wright 0.7 0.79) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string "Task duration")
    (set-x-window-scale where qstart qend)
    (let ((scale (+ 0.1 (max 1 (pcf-max (duration-ipcf task) qstart qend)))))
      ;; (format t "~%duration scale is ~a" scale)
      (set-y-window-scale where 0 scale)
      (plot-pcf where (duration-ipcf task) :linewidth 0.25)
      (label-y-axis where 0 scale :tic-value 1 :tic-size 1 :label nil)
      (label-y-axis where 0 1 :tic-value 1 :tic-size 1 :label-format "~d")
      (label-y-axis where 0 scale :tic-value 5 :tic-size 2 :label-format "~d"))
					;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    
    ;; EFFICIENCY
    (set-loc-of-drawing-window where wleft wright 0.6 0.69) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string (format nil "Task efficiency (exp time ~as)" (exp-time task)))
    (set-x-window-scale where qstart qend)
    (let ((scale 1.1))
      (set-y-window-scale where 0 scale)
      (plot-pcf where (efficiency-ipcf task) :linewidth 0.25)
      (label-y-axis where 0 scale :tic-value 0.1 :tic-size 1 :label nil)
      (label-y-axis where 0 scale :tic-value 0.2 :tic-size 2 :label-format "~,1f"))
					;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    
    ;; COMPUTED CONSTRAINTS
    (set-XTE-constraint-parameters
     :name (id task) :ra (target-ra task) :dec (target-dec task)
     :start-date *XTE-schedule-start*
     :end-date *XTE-schedule-end*
     :orbit-model *default-XTE-orbit-model*
     :shadow-flag (shadow-only task)
     :non-interrupt (non-interrupt task)
     :non-interrupt-duration (/ (exp-time task) 86400.0))
    (let ((efficiency (get-XTE-efficiency-pcf))
	  (orb-viewing (get-XTE-orbital-viewing-pcf))
	  (sun (get-XTE-sun-pcf))
	  (moon (get-XTE-moon-pcf))
	  )
      
      ;; ORB VIEWING
      (set-loc-of-drawing-window where wleft wright 0.45 0.59) ;LRBT
      (box-window where)
      (record-font where "Helvetica" 7)
      (string-at-line-in-window
       where :line-number 1 :font-size 7
       :string "Computed orb viewing suitability")
      (set-x-window-scale where tjd-start tjd-end)
      (set-y-window-scale where -0.1 1.1)
      (plot-pcf where orb-viewing :linewidth 0.25)
      (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
      (label-y-axis where 0 1 :tic-value 0.2 :tic-size 2 :label-format "~,1f")
					;(label-x-axis where (ceiling tjd-start) (floor tjd-end) 
					;              :tic-value 1 :tic-size 1 :label nil)
      (plot-interval-bars 
       where (pcf-non-zero-intervals orb-viewing tjd-start tjd-end) 
       -0.1 -0.05 :fill 0)
      
      ;; EFFICIENCY
      (set-loc-of-drawing-window where wleft wright 0.30 0.44) ;LRBT
      (box-window where)
      (record-font where "Helvetica" 7)
      (string-at-line-in-window
       where :line-number 1 :font-size 7
       :string "Computed efficiency")
      (set-x-window-scale where tjd-start tjd-end)
      (set-y-window-scale where -0.1 1.1)
      (plot-pcf where efficiency :linewidth 0.25)
      (label-y-axis where 0 1 :tic-value 0.1 :tic-size 1 :label nil)
      (label-y-axis where 0 1 :tic-value 0.2 :tic-size 2 :label-format "~,1f")
					;(label-x-axis where (ceiling tjd-start) (floor tjd-end) 
					;             :tic-value 1 :tic-size 1 :label nil)
      (plot-interval-bars 
       where (pcf-non-zero-intervals efficiency tjd-start tjd-end) 
       -0.1 -0.05 :fill 0)
      
      ;; SUN
      (set-loc-of-drawing-window where wleft wright 0.25 0.29) ;LRBT
      (box-window where)
      (record-font where "Helvetica" 7)
      (string-at-line-in-window
       where :line-number 1 :font-size 7
       :string "Computed sun suitability")
      (set-x-window-scale where tjd-start tjd-end)
      (set-y-window-scale where 0 1.1)
      (plot-pcf where sun :linewidth 0.25)
      (label-y-axis where 0 1 :tic-value 1 :tic-size 2 :label-format "~,1f")
					;(label-x-axis where (ceiling tjd-start) (floor tjd-end) 
					;              :tic-value 1 :tic-size 1 :label nil)
      
      ;; MOON
      (set-loc-of-drawing-window where wleft wright 0.20 0.24) ;LRBT
      (box-window where)
      (record-font where "Helvetica" 7)
      (string-at-line-in-window
       where :line-number 1 :font-size 7
       :string "Computed moon suitability")
      (set-x-window-scale where tjd-start tjd-end)
      (set-y-window-scale where 0 1.1)
      (plot-pcf where moon :linewidth 0.25)
      (label-y-axis where 0 1 :tic-value 1 :tic-size 2 :label-format "~,1f")
					;(label-x-axis where (ceiling tjd-start) (floor tjd-end) 
					;              :tic-value 1 :tic-size 1 :label nil)
      
      )
    
    ;; DRAW TIME AXIS OVER WHOLE PAGE
    (set-loc-of-drawing-window where wleft wright 0 1)
    (set-x-window-scale where tjd-start tjd-end)
    (time-label where tjd-start tjd-end :label t
		:min-pixels-for-tics 5
		:min-pixels-for-labels 20)
    
    
    (record-wrapup where)
    ))


;; this function is only for testing - it can be removed

(defun QUICK-PLOT-XTE-TASK-DETAILS (schedule &key
					     id ; task ID
					     where 
					     file
					     ;; these defaults for the Mac
					     (ptop 10)
					     (pbottom 500)
					     )
  
  (let* (;; page loc of rect left, right: same throughout
	 (pleft 60) (pright 550)	;pleft was 60, pright was 550
	 ;; window loc left right: same throughout
	 (wleft 0.1) (wright 0.98)
	 ;; current page top bottom
	 (chr (obs-chronika schedule))
	 ;;(csp (obs-csp schedule))
	 (qstart (qtime-start chr))
	 (qend (qtime-end chr))
	 (tjd-start (qtime-to-tjd chr qstart))
	 (tjd-end (qtime-to-tjd chr qend))
	 (task (lookup-task chr id))
	 ;;(var (task-var task))
	 
	 )
    (if (not task) (error "task ~a not found" id))
    
    (cond (where (clear-gwindow-history where))
	  ((and (not where) file)
	   (setf where
		 (make-ps-gwindow :file file)))
	  (t (error "either :where or :file must be specified")))
    
    (record-prolog where)
    
    (set-page-loc-of-drawing-rect
     where pleft pright pbottom ptop)	;LRBT
    (box-rect where)
    
    (record-font where "Helvetica-Bold" 9)
    (string-at-line-in-rect 
     where :line-number 1 :font-size 9
     :string (format nil "~a" id))
    
    ;; SUITABILITY
    (set-loc-of-drawing-window where wleft wright 0.8 0.95) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string "Task suitability")
    
    (set-x-window-scale where qstart qend)
    (set-y-window-scale where -0.1 1.1)
    (plot-pcf where (suitability task) :linewidth 0.25)
    (label-y-axis where 0 1.0 :tic-value 0.1 :tic-size 1 :label nil)
    (label-y-axis where 0 1.0 :tic-value 0.2 :tic-size 2 :label-format "~,1f")
    ;;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    (plot-interval-bars where (non-zero-intervals task) -0.1 0 :fill 0)
    
    ;; DURATION
    (set-loc-of-drawing-window where wleft wright 0.7 0.79) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string "Task duration")
    (set-x-window-scale where qstart qend)
    (let ((scale (+ 0.1 (max 1 (pcf-max (duration-ipcf task) qstart qend)))))
      ;; (format t "~%duration scale is ~a" scale)
      (set-y-window-scale where 0 scale)
      (plot-pcf where (duration-ipcf task) :linewidth 0.25)
      (label-y-axis where 0 scale :tic-value 1 :tic-size 1 :label nil)
      (label-y-axis where 0 1 :tic-value 1 :tic-size 1 :label-format "~d")
      (label-y-axis where 0 scale :tic-value 5 :tic-size 2 :label-format "~d"))
    ;;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    
    ;; EFFICIENCY
    (set-loc-of-drawing-window where wleft wright 0.6 0.69) ;LRBT
    (box-window where)
    (record-font where "Helvetica" 7)
    (string-at-line-in-window
     where :line-number 1 :font-size 7
     :string (format nil "Task efficiency (exp time ~as)" (exp-time task)))
    (set-x-window-scale where qstart qend)
    (let ((scale 1.1))
      (set-y-window-scale where 0 scale)
      (plot-pcf where (efficiency-ipcf task) :linewidth 0.25)
      (label-y-axis where 0 scale :tic-value 0.1 :tic-size 1 :label nil)
      (label-y-axis where 0 scale :tic-value 0.2 :tic-size 2 :label-format "~,1f"))
    ;;(label-x-axis  where qstart qend :tic-value 1 :tic-size 1 :label nil)
    
    ;; COMPUTED CONSTRAINTS - skip
    
    ;; DRAW TIME AXIS OVER WHOLE PAGE
    (set-loc-of-drawing-window where wleft wright 0 1)
    (set-x-window-scale where tjd-start tjd-end)
    (time-label where tjd-start tjd-end :label t
		:min-pixels-for-tics 5
		:min-pixels-for-labels 20)
    
    
    (record-wrapup where)
    ))



;;; ======================================================================
;;;                                                        DUMMY FUNCTIONS
;;; ======================================================================
;;; The following are functions fully defined in xte-functions.lisp,
;;; a file local to the MIT version of Astro-D Spike only.
;;; These dummy definitions allow execution of the contents of xte-csp.
;;; The dummy definitions will be overridden when xte-functions.lisp
;;; is loaded, which must be *after* this file

(defun GET-ORIGINAL-TARGET-LIST ()
  )

(defun LT-UNOBSERVED-TARGETS (target-list)
  (declare (ignore target-list))
  )

(defun REST-OF-LONG-TERM-TARGETS (segment-number)
  (declare (ignore segment-number))
  )

(defun MAKE-SAA-LIST (TJD-start TJD-end)
  (declare (ignore TJD-start TJD-end))
  )

(defvar *SAA-COUNT* 0)
(defvar *SAA-LIST*  (make-hash-table :size 200))

(defun ST-UNOBSERVED-TARGETS ()
  )


;;; ======================================================================
;;;                          CAPACITY CONSTRAINT FOR ONBOARD DATA RECORDER
;;; ======================================================================
;;;
;;; from here to end of file is new mdj/nov94 for data recorder constraint
;;; 

;;; see above for globals:
;;;  *XTE-ONBOARD-CAPACITY* 350000 kbits
;;;  *XTE-READOUT-RATE* 20 kbit/s
;;;  *XTE-ONBOARD-ACCOUNTING-INTERVAL* 6 quanta
;;; =) runout time is (/ (/ 350000 20) 3600.0) = 4.86 hours

;;; ----------------------------------------------------------------------
;;;                                                     CLASS: ONBOARD-CAP
;;; ----------------------------------------------------------------------

(defclass ONBOARD-CAP (GENERAL-CAPACITY-CONSTRAINT
                       GENERAL-TIME-TO-SEGMENT-MAPPING)
  ((capacity-limit
    :initform nil
    :documentation "total permitted capacity usage per bin, kbits")
   (capacity-check-interval-duration
    :initform 1
    :documentation "check capacity in bins of this size, quanta")
   ;; for the following three fields: key is var, value is array indexed
   ;; by value or bin
   (val-to-bins-cache    :initform (make-hash-table))
   (bin-to-vals-cache    :initform (make-hash-table))
   (val-bin-usage-cache  :initform (make-hash-table))
   )
  (:documentation 
   "Onboard data storage capacity constraint for XTE"))

;;; ----------------------------------------------------------------------
;;; Init method for ONBOARD-CAP:
;;; args will be passed from make-capacity-constraint and are:
;;;  :chronika - chronika instance
;;;  :capacity - total capacity amount per bin, kbits
;;;  :interval - bin interval, quanta

(defmethod INIT-CAPACITY-CONSTRAINT ((cap ONBOARD-CAP) 
                                     (csp XTE-SHORT-TERM-CSP) 
                                     args)
  (let ((chr       (getf args :chronika))
        (capacity  (getf args :capacity))
        (interval  (getf args :interval))
        )
    
    ;; error detect
    (unless (and chr (typep chr 'CHRONIKA))
      (error "arg :CHRONIKA missing or invalid"))
    (unless (and capacity (numberp capacity) (>= capacity 0)) 
      (error "arg :CAPACITY must be number >= 0"))
    (unless (and interval  (numberp interval) (>= interval 0))
      (error "arg :INTERVAL is in error"))
    
    ;; set up segments for bins
    (let* ((qstart (start chr))
           (qend (end chr))
           (number (floor (- qend qstart) interval))
           )
      (format t "~%Onboard constraint: segment size ~d (=~a days), # bins: ~d"
              interval (duration-in-qtime-to-days chr interval)
              number)
      (init-time-to-segment-mapping 
       cap
       :segment-start qstart
       :segment-duration interval
       :segment-count number
       ))
    ;; set up limit array
    (let* ((count (total-number-of-segments cap))
           (lim (make-array count))
           (used (make-array count :initial-element 0)))
      
      ;; fill limit array
      (dotimes (i count)
        (setf (aref lim i) capacity))
      
      ;; store in slots
      (setf (slot-value cap 'bin-limit) lim)
      (setf (slot-value cap 'bin-usage) used)
      (setf (slot-value cap 'bin-count) count)
      (setf (slot-value cap 'capacity-limit) capacity)
      )
    
    ;; set up the cache (requires all vars to have been already set up)
    (calculate-all-val-bins-map cap csp)
    
    cap))

;;; ----------------------------------------------------------------------
;;; define basic methods (note bin-to-values not cached here; this could
;;; be done in the cap constraint instance)

(defmethod VALUE-TO-BIN
           ((c XTE-SHORT-TERM-CSP) (capcon ONBOARD-CAP)  (v XTE-SHORT-TERM-VAR) value)
  (declare (ignore value))
  (error "erroneous call to this method!"))


(defmethod CALCULATE-ALL-VAL-BINS-MAP ((capcon ONBOARD-CAP) (CSP XTE-SHORT-TERM-CSP))
  "compute and fill the cache for all vars in the CSP. See
    calculate-val-bins-map for details"
  (with-slots
    (val-to-bins-cache bin-to-vals-cache val-bin-usage-cache)
    capcon
    
    (do-for-all-ignored-and-unignored-vars
     :var var :csp csp
     :form
     (multiple-value-bind
       (val-to-bins-array bin-to-vals-array val-bin-usage-array)
       (CALCULATE-VAL-BINS-MAP capcon var)
       
       (setf (gethash var val-to-bins-cache) val-to-bins-array)
       (setf (gethash var bin-to-vals-cache) bin-to-vals-array)
       (setf (gethash var val-bin-usage-cache) val-bin-usage-array)

       ))))

   
(defmethod CALCULATE-VAL-BINS-MAP ((capcon ONBOARD-CAP) (v XTE-SHORT-TERM-VAR))

  "input capacity constraint of type ONBOARD-CAP (which incorporates a TIME-TO-
    SEGMENT-MAPPING object) and a var, return three arrays:
     val-to-bins-array - (aref val-to-bins-array val) => list of bins 
      corresponding to val
     bin-to-vals-array - (aref bin-to-vals-array bin) => list of vals 
      corresponding to bin
     val-bin-usage-array - (rest (assoc bin (aref val-bin-usage-array val))) =>
      duration of var that occurs in bin given start corresponding to val"


  (let* ((count (value-count v))
         (task (task v))
         (fdsia (fdsia task))
         ;; these record bin <--> val mapping, and usage
         (val-to-bins-array (make-array count :initial-element nil))
         (bin-to-vals-array (make-array (bin-count capcon) :initial-element nil))
         (val-bin-usage-array (make-array count :initial-element nil))
         (verbose nil))
    (dotimes (i count)
      (if verbose (format t "~%val: ~a" i))
      (let* ((start-time (fdsia-val-to-time fdsia i))
             (detail-intervals (detail-intervals task start-time))
             (current-seg nil)
             (current-quantity-in-seg 0)
             (all-times-in-segments t)
             (result nil))
        (if verbose (format t " time ~a ~a" start-time detail-intervals))
        (dolist (int detail-intervals)
          (loop-over-value-range
           time (first int) (1- (second int))
           (let ((seg (time-to-segment capcon time)))
             ;;(format t "~%    t: ~a seg: ~a" time seg)
             (cond 
              ((not seg)                ; not in segment: no mapping value->bin
               (setf all-times-in-segments nil))
              ;; in segment
              ((and current-seg (= seg current-seg))
               (incf current-quantity-in-seg)
               )
              ((and current-seg (/= seg current-seg))
               (push (cons current-seg current-quantity-in-seg) result)
               (setf current-seg seg)
               (setf current-quantity-in-seg 1))
              ((and (not current-seg) seg)
               (setf current-seg seg)
               (setf current-quantity-in-seg 1))
              (t (error "?")))          ; end cond
             ;;(format t " cur-seg ~a quant ~a res ~a" current-seg 
             ;;        current-quantity-in-seg result)
             ))                         ; end loop over value range time
          )                             ; end dolist
        ;; save info for last segment, then reverse to get time order
        (if current-seg
          (push (cons current-seg current-quantity-in-seg) result))
        (setf result (nreverse result))
        ;; report result
        (if verbose (format t " result: ~a" result))
        (if (and verbose (not all-times-in-segments))
          (format t " *some times not in segs*"))

        ;; save in cache arrays
        (when all-times-in-segments
          (setf (aref val-bin-usage-array i) result)
          (setf (aref val-to-bins-array i) (mapcar #'first result))
          
          ;; consistency check
          (let* ((s (summed-interval-duration detail-intervals))
                 (p (apply '+ (mapcar 'rest result)))
                 (diff (- s p)))
            (if verbose (format t " ~a-~a=~a" s p diff))
            (if (/= diff 0)
              (error "inconsistency in time/seg"))))
        
        ))                              ; end dotimes i
    
    ;; given val-to-bins-array, construct bin-to-vals array
    (dotimes (i count)
      (dolist (bin (aref val-to-bins-array i))
        (pushnew i (aref bin-to-vals-array bin))))
    ;; consistency check again
    (val-bins-map-consistency-check val-to-bins-array bin-to-vals-array)

    (values
     val-to-bins-array
     bin-to-vals-array
     val-bin-usage-array)
     ))


(defun VAL-BINS-MAP-CONSISTENCY-CHECK 
       (val-to-bins-array bin-to-vals-array &key (verbose nil))
  "check for consistency in val <-> bin mapping, report cerror if any problems
    found"
  (let* ((value-count (array-dimension val-to-bins-array 0))
         (bin-count (array-dimension bin-to-vals-array 0)))
    (if verbose (format t "~%vals: ~d bins ~d" value-count bin-count))
    
    (dotimes (bin bin-count)
      (dolist (val (aref bin-to-vals-array bin))
        (unless (member bin (aref val-to-bins-array val))
          (format t "~%bin->val ~d->~d, but error: val->bin ~a->~a"
                  bin (aref bin-to-vals-array bin)
                  val (aref val-to-bins-array val))
          (cerror "nothing changes" "consistency problem"))))

    (dotimes (val value-count)
      (dolist (bin (aref val-to-bins-array val))
        (unless (member val (aref bin-to-vals-array bin))
          (format t "~%val->bin ~d->~d, but error: bin->val ~a->~a"
                  val (aref val-to-bins-array val)
                  bin (aref bin-to-vals-array bin)
                  )
          (cerror "nothing changes" "consistency problem"))))
    ))


(defmethod VALUE-TO-BINS
  ((c XTE-SHORT-TERM-CSP) (capcon ONBOARD-CAP) (v XTE-SHORT-TERM-VAR) value)
  ;; cache lookup
  (with-slots (val-to-bins-cache) capcon
    (aref (gethash v val-to-bins-cache) value)))


(defmethod BIN-TO-VALUES 
  ((c XTE-SHORT-TERM-CSP) (capcon ONBOARD-CAP) (v XTE-SHORT-TERM-VAR) bin)
  ;; cache lookup
  (with-slots (bin-to-vals-cache) capcon
    (aref (gethash v bin-to-vals-cache) bin)))
  

(defmethod REQUIRED-CAPACITY 
           ((c XTE-SHORT-TERM-CSP) (capcon ONBOARD-CAP) (v XTE-SHORT-TERM-VAR) bin value)
  (with-slots (val-bin-usage-cache) capcon
    (let* ((val-bin-usage-array (gethash v val-bin-usage-cache))
           ;; tbin is total time in bin in qtime
           (tbin (rest (assoc bin (aref val-bin-usage-array value))))
           ;; readout rate in kbits/s net
           (telemetry-rate-kbits/sec 
            (max (- (or (telemetry (task v)) 0) *XTE-READOUT-RATE*) 0)))
      
      ;; returned value
      (* telemetry-rate-kbits/sec tbin *XTE-SHORT-TERM-TIME-QUANTUM* 86400)
      
      )))


(defmethod MAX-REQUIRED-CAPACITY 
  ((c XTE-SHORT-TERM-CSP) (capcon ONBOARD-CAP) (v XTE-SHORT-TERM-VAR))
  ;; t is always valid as default: means that max capacity is not known
  t)


;;; --- new method for ONBOARD capacity reporting: extend this as necessary

(defmethod ONBOARD-CAPACITY-LIST ((OCC XTE-SHORT-TERM-CSP-CHRONIKA)
                              &key 
                              (capcon-name 'ONBOARD-CAPACITY)
                              (only-used-bins t)
			      (stream t))
  
  "List bin info"
  
  (let* (;;(chronika (obs-chronika OCC))
         (csp (obs-csp OCC))
         (cap-con (find-capacity-constraint csp capcon-name)))
    
    (cond 
     
     ;; cps or capacity constraint not present - return nil (no bins defined)
     ((or (not csp) (not cap-con))
      nil)
     
     (t ; get bin info
      (let* ((bin-count (bin-count cap-con))
             
             (result nil))
        
        ;; report assigned vars
        (dotimes (current-bin bin-count)
          
          (let ((limit (aref (bin-limit cap-con) current-bin))
                (usage (aref (bin-usage cap-con) current-bin)))
            (when (or (not only-used-bins)
                      (and only-used-bins (/= usage 0)))
              (format stream "~%Segment: ~3d  limit: ~6,2f, used ~6,2f =~6,2f%"
                      current-bin
                      limit
                      usage
                      (* 100 (/ usage limit))
                      ))
            
            ))
        ;; returned value
        result)))))



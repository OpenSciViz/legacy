;;; raw-lts-report
;;; Simply formatted long term schedule report defined in XTE build 2
;;; for testing purposes.
;;; RCS Stamp: $Id: raw-lts-report.lisp,v 3.0 1994/04/01 19:36:12 buehler Submitted $
;;; Author: Ed Morgan

(defmethod XSOC-TASK-REPORT ((var segmented-obs-var))
  "Return string describing scheduling status of var"
  (let* ((task (task var))
         (chronika (chronika task))
         (name (target-name task))
         (exp-time (exp-time task))
         (assigned-value (assigned-value var))
         (start-end (multiple-value-list (assigned-time var)))
         (start (first start-end))
         ;; (end   (second start-end))
         (conf (conflicts-on-assigned-value var))
         (max-pref (max-preference var))
         ;; (min-duration (minimum-nonzero-duration task))
         (max-effic (ipcf-max (efficiency-ipcf task)))
         ;; only valid if start is known
         assigned-pref assigned-duration assigned-exp-time assigned-effic
         )


    (when start ;if there is an assignment
      (setf 
       assigned-pref (value-preference var assigned-value)
       assigned-duration (duration-in-qtime-to-days 
                          chronika (ipcf-get-value start (duration-ipcf task)))
       assigned-exp-time (exposure-duration-at-time task start)
       assigned-effic (ipcf-get-value start (efficiency-ipcf task))))
    
    (if start
      (format nil " ~15a "
              name )
      ;; unassigned
      (format nil "~15a "
              name))))


(defmethod XSOC-SCHEDULE-REPORT ((csp segmented-obs-csp) &key (stream t) &allow-other-keys)
  
  "Report schedule status to stream"
  ;; (schedule-report (obs-csp -occ))
  ;; (obs-cap-constraint (obs-csp-chronika (obs-csp -occ)))
    
  (let* ((assigned-vars (all-assigned-vars csp))
         (sorted-vars (safe-sort assigned-vars #'< :key #'assigned-time))
         (chronika (chronika (task (first (all-vars csp)))))
         (cap-con (obs-cap-constraint (obs-csp-chronika csp)))
         (cap-unit-quanta (capacity-unit-in-quanta csp))
         (cap-unit-days (duration-in-qtime-to-days chronika cap-unit-quanta))
         (oversub (oversubscription-factor csp))
         (overall-start (start chronika))
         (overall-end   (end chronika))
         (sched-days (duration-in-qtime-to-days 
                      chronika (- overall-end overall-start)))
         (total-sched 0) ;qtime
         total-sched-days ;days
         (total-exp 0) ;seconds
         total-exp-days ;days
         (prev-bin nil)
         (current-bin nil)
         )
    
    ;; report all assignments and gaps
    ;; report assigned vars
    (dolist (var sorted-vars)
      (setf current-bin (value-to-bin csp cap-con var (assigned-value var)))
      (format stream "~%~d~a" current-bin (xsoc-task-report var))
      )
    
    (unless (all-vars-assigned-p csp)
      (setf total-exp 0)
      (dolist (var (all-unassigned-vars csp))
        (incf total-exp (exp-time (task var)))
        (format stream "~%~d~a" -1 (xsoc-task-report var))
        ))
    (format stream "~%" )
    ))


#!/bin/sh
# @(#)ltags.sh	1.5 04/22/93  Written By:  J. Flanagan, WaveStar Technology
# Shell script to generate lisp tags on stdout (requires ltags.awk).

(for file in $*
 do
 gawk -f ltags.awk $file
 done) | sort

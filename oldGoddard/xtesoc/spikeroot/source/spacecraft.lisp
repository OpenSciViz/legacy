;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; spacecraft.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: spacecraft.lisp,v $
;;; Revision 4.3  1995/10/14 20:39:38  buehler
;;; Avoid setting up a default orbit model for XTE. Variable *XTE* will
;;; now be defined in leo-ephem.lisp instead.
;;;
;;; Revision 4.2  1995/02/03  23:28:43  buehler
;;; Temporarily undo the use of PCA sensor.
;;;
;;; Revision 4.1  1995/01/19  19:13:28  buehler
;;; Default model time of ascending node upped to 2-jan-1995.
;;;
;;; Revision 4.0  1995/01/16  16:16:46  buehler
;;; Add an XTE sensor named PCA. The parameters here are wild guesses,
;;; to be improved in a later version.
;;;
;;; Revision 3.1  1994/09/07  16:51:45  buehler
;;; Change name of XTE satellite from "ASTRO-D".
;;;
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.5  1991/12/01  00:27:35  johnston
;;; minor changes
;;;
;;; Revision 1.4  1991/11/26  15:46:18  johnston
;;; minor changes
;;;
;;; Revision 1.3  1991/06/05  22:47:03  johnston
;;; added newline at end of file
;;;
;;; Revision 1.2  91/06/05  22:46:11  johnston
;;; added new satellite Astro-D
;;; 
;;; Revision 1.1  91/04/05  08:38:05  johnston
;;; Initial revision
;;; 
;;;
;;; spacecraft.lisp
;;;
;;; definitions of spacecraft
;;;


;;; ----------------------------------------------------------------------
;;;                                                                  H S T
;;; ----------------------------------------------------------------------

;;; NOTE: *ST* and TDRS are defvar'd in astro-objects, so that shouldn't
;;; done again in this file.  Other satellites do need defvar's here.

(setf *ST*
      (make-low-earth-orbit-satellite 
       "HST"                                 ;name
       28.5                                  ;inclination, deg
       2135.520145833                        ;time asc node, TJD
       64.906                                ;RA asc node, deg
       -6.7046                               ;nodal regression rate, deg/day
       94.42896                              ;period, minutes
       6878.165                              ;semimajor axis, km
       ))


;;; --- HST SENSORS

(make-sensor 
 :name :FHST1
 :satellite *ST* 
 :boresight '( 0.0006394751  0.0057162757 -0.99998346)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))

(make-sensor 
 :name :FHST2
 :satellite *ST* 
 :boresight '(-0.6549442800 -0.3742016600 -0.65652198)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))

(make-sensor 
 :name :FHST3
 :satellite *ST* 
 :boresight '(-0.6555800300  0.3806178100 -0.65218472)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))

;;; --- HST NAMED VECTORS

(add-named-vector *ST* :V3 '(0 0 1))

;;; ----------------------------------------------------------------------
;;;                                                      T D R S - E A S T
;;; ----------------------------------------------------------------------

(setf *TDRSE* 
      (make-geostationary-satellite 
       "TDRS-East"                           ;name
       -41.0                                 ;longitude, deg.
       86164.09054                           ;period, sec.
       42164.69))                            ;semimajor axis, km

;;; ----------------------------------------------------------------------
;;;                                                      T D R S - W E S T
;;; ----------------------------------------------------------------------

(setf *TDRSW* 
      (make-geostationary-satellite 
       "TDRS-West"                           ;name
       -171.0                                ;longitude, deg
       86164.09054                           ;period, sec
       42164.69))                            ;semimajor axis, km


;;; ----------------------------------------------------------------------
;;;                                                                E U V E
;;; ----------------------------------------------------------------------

(defvar *EUVE* nil "EUVE satellite")
(setf *EUVE*
      (make-low-earth-orbit-satellite 
       "EUVE"                                ;name
       28.5                                  ;inclination, deg
       (dmy-to-time '1-jan-1990)             ;time asc node, TJD
       0                                     ;RA asc node, deg
       -6.4                                  ;nodal regression rate, deg/day
       96.58                                 ;period, minutes
       6978.1                                ;semimajor axis, km
       ))

;;; --- EUVE SENSORS

;;; Note:  satellite body vectors are defined as for HST:
;;; correspondence with X,Y,Z payload module coordinates is
;;;  v1 = x, v2 = z, v3 = -y

(make-sensor 
 :name :FHST1
 :satellite *EUVE* 
 :boresight '(0.5948473 0.1467463 -0.7903304)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))

(make-sensor 
 :name :FHST2
 :satellite *EUVE* 
 :boresight '(-0.5948473 0.1467463 -0.7903304)
 :fov-type :cone
 :fov-cone-half-angle (radians 4))

;; square fields of view not supported at present:  here make
;; circular with 32 deg half angle

(make-sensor 
 :name :FSS
 :satellite *EUVE* 
 :boresight '(-0.9330127 0.0870968 0.3491439)
 :fov-type :cone
 :fov-cone-half-angle (radians 32))

;;; --- EUVE NAMED VECTORS

;; both scanners have same orientation
(add-named-vector *EUVE* :SCANNER   '(0 0 -1))

(add-named-vector *EUVE* :MPS-NORMAL   '(0   0.866 -0.5))
(add-named-vector *EUVE* :MACS-NORMAL  '(0  -0.866 -0.5))
(add-named-vector *EUVE* :C&DH-NORMAL  '(0   0      1))


;;; ----------------------------------------------------------------------
;;;                                                          A S T R O - D
;;; ----------------------------------------------------------------------

;;; (defvar *XTE* nil "XTE satellite")
(setf *XTE* *XTE-EPHEMERIS-MODEL*)
  #|
      (make-low-earth-orbit-satellite 
       "XTE"                                ;name
       28.5                                  ;inclination, deg
       (dmy-to-time '2-jan-1995)             ;time asc node, TJD
       0                                     ;RA asc node, deg
       -6.4                                  ;nodal regression rate, deg/day
       96.58                                 ;period, minutes
       6978.1                                ;semimajor axis, km
       :roll-limit-type :V3
       :roll-limit-angle (radians 30)
       ))
|#
;;; --- XTE SENSORS
;;;
;;; correspondence between ASTRO-D body frame and V1 V2 V3:
;;;  V1 = Z, V3 = Y, V2 = X

(let ((sin45 (sin (radians 45))))
#|
  (make-sensor 
   :name :PCA
   :satellite *XTE* 
   :boresight (list 0 0 1)   ; tracker Z axis
   :fov-type :RECT
   ;; distance from boresight to corner of FOV
   :fov-cone-half-angle (sqrt (+ (* 2.5 2.5) (* 5 5)))
   :units :degrees
   :fov1-vector (list 1 0 0)   ; tracker X axis
   :fov1-half-angle 2.5
   :fov2-vector (list 0 1 0)            ; tracker Y axis
   :fov2-half-angle 5)
|#
  (make-sensor 
   :name :STTA
   :satellite *XTE* 
   :boresight (list 0 (- sin45) (- sin45))   ; tracker Z axis
   :fov-type :RECT
   ;; distance from boresight to corner of FOV
   :fov-cone-half-angle (sqrt (+ (* 2.5 2.5) (* 5 5)))
   :units :degrees
   :fov1-vector (list 0 sin45 (- sin45))   ; tracker X axis
   :fov1-half-angle 2.5
   :fov2-vector (list 1 0 0)            ; tracker Y axis
   :fov2-half-angle 5)

  (make-sensor 
   :name :STTB
   :satellite *XTE* 
   :boresight (list 0 sin45 (- sin45))   ; tracker Z axis
   :fov-type :RECT
   :fov-cone-half-angle (sqrt (+ (* 2.5 2.5) (* 5 5)))
   :units :degrees
   :fov1-vector (list 0 sin45 sin45)   ; tracker X axis
   :fov1-half-angle 2.5
   :fov2-vector (list 1 0 0)            ; tracker Y axis
   :fov2-half-angle 5)

  )

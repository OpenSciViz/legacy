;;; -*- Mode:Common-Lisp; Package:COMMON-LISP-USER; Base:10 -*-
;;;
;;; astro-utilities.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: astro-utilities.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.54  1992/11/11  17:46:12  johnston
;;; rewrote ipcf-from-interval-list to be much faster if list is ordered,
;;; but to behave the old way if it's not.  Also, fixed abs-time-to-tjd
;;; to use a double version of the ticks-to-time *conversion-factor*,
;;; which greatly sped up time conversion.
;;;
;;; Revision 1.53  1992/04/22  17:01:40  johnston
;;; Add new functions:
;;; - select randomly from discrete distributions
;;; - loop through time ranges at specified resolution
;;; - shift intervals early or late to avoid overlapping other intervals
;;;
;;; Revision 1.52  1991/12/18  16:01:04  giuliano
;;; Fixed pcf-multiply so that multiplying two non-zero suitabilities
;;; together will never go below a given value.  This prevents
;;; underflow conditions.
;;;
;;; Revision 1.51  1991/12/01  00:13:47  johnston
;;; minor changes
;;;
;;; Revision 1.50  1991/11/26  15:34:14  johnston
;;; minor changes (mostly timing routines)
;;;
;;; Revision 1.49  1991/10/10  15:42:13  mrose
;;; New function pretty-seconds.
;;;
;;; Revision 1.48  1991/10/04  16:27:28  mrose
;;; New pretty-string-rel-time.
;;;
;;; Revision 1.47  1991/09/23  13:04:17  henry
;;; Added function Radians-To-Arcsecs.
;;;
;;; Revision 1.46  1991/09/04  17:32:56  sponsler
;;; STIF3 changes
;;;
;;; Revision 1.45  91/08/21  17:01:00  giuliano
;;; Updated tjd-to-short-sogs-date to be compatible with SPARC station.
;;; 
;;; Revision 1.44  91/08/07  12:01:13  gerb
;;; rounded rel time instead of using ceiling 
;;; 
;;; Revision 1.43  91/08/06  14:36:43  sponsler
;;; Fn: PA-OF-NORTH-ECLIPTIC-POLE - check for alpha,delta = 90 degrees
;;; 
;;; Revision 1.42  91/08/02  16:13:47  henry
;;; Added new function Angular-Separation-From-RA-Dec.
;;; 
;;; Revision 1.41  91/08/01  10:01:17  mrose
;;; Rewrote REL-TIME to fix problems with floating point imprecision.
;;; 
;;; Revision 1.40  91/07/05  11:41:42  lucks
;;; Added function ARCSECS-TO-DEGREES.
;;; 
;;; Revision 1.39  91/06/06  10:51:34  giuliano
;;; Fixed the definition of the stats function so that it handles lists
;;; with zero and one elements.  OPR 20146
;;; 
;;; Revision 1.38  91/06/06  09:15:38  mrose
;;; Make REL-TIME a function, take ceiling of magnitude, so negative
;;; numbers are handled better.
;;; 
;;; Revision 1.37  91/06/05  22:42:23  johnston
;;; fixes to 3vect separation for identical vectors;
;;; added new routine to loop over times in ipcfs
;;; 
;;; Revision 1.36  91/05/05  14:58:04  mrose
;;; time conv for su-links.
;;; 
;;; Revision 1.35  91/04/11  11:13:29  krueger
;;; Added synonym function "pcf-clip-start-and-end-intervals"
;;; 
;;; Revision 1.34  91/03/28  17:02:47  henry
;;; Changed *SOGS-TIME-START* from a defconstant to a defparameter to
;;; avoid compile-time reference to functions defined in same file.
;;; 
;;; Revision 1.33  91/03/28  16:50:37  sponsler
;;; Changed def of TJD-TO-ABS-TIME
;;; 
;;; Revision 1.32  91/03/28  16:23:24  sponsler
;;; TJD-TO-ABS-TIME function added
;;; 
;;; Revision 1.31  91/03/28  13:37:10  sponsler
;;; SOGS date support: *sogs-time-start*, tjd-to-sogs-time, etc
;;; 
;;; Revision 1.30  91/03/25  13:19:32  johnston
;;; no changes
;;; 
;;; Revision 1.29  91/03/18  22:18:07  johnston
;;; General cleanup, added routines that were in other files.
;;; 
;;; Revision 1.28  91/02/27  11:59:55  sponsler
;;; LINEAR-INTERPOLATION more efficient now (check for endpoints)
;;; 
;;; Revision 1.27  91/01/16  20:45:34  lucks
;;; Added keyword parameter "universal-time" to date-string.
;;; 
;;; Revision 1.26  91/01/03  09:31:53  miller
;;; moved pcf-remove-minus-infinity-token from nnif.lisp to astro-utilities.lisp
;;; 
;;; Revision 1.25  90/12/11  16:01:19  krueger
;;; PCF-LATEST-NONZERO-INTERVAL-END does not always return the correct
;;; value when the arguments range-start and range-end are supplied.
;;; Commented out the current version (updated for speed).  Uncommented
;;; original version of this function which works.
;;; 
;;; Revision 1.24  90/10/18  11:54:49  sponsler
;;; New fns: short-sogs-date-to-tjd and its inverse
;;; 
;;; Revision 1.23  90/10/11  11:21:44  mrose
;;; Fix format spec in format-universal-time
;;; 
;;; Revision 1.22  90/10/05  10:09:02  gerb
;;; added even function
;;; 
;;; Revision 1.21  90/09/24  16:44:59  gerb
;;; added slope arctan function
;;; 
;;; Revision 1.20  90/08/23  09:32:50  gerb
;;; coerced pi to be double float so acos works properly on the vax
;;; 
;;; Revision 1.19  90/07/17  15:24:08  mrose
;;; New functions SQUARE, SUM-OF-SQUARES.
;;; 
;;; Revision 1.18  90/06/26  14:09:16  sponsler
;;; CONVERT-ANGLES-TO-UNITS added.
;;; 
;;; Revision 1.17  90/05/28  18:36:22  vick
;;; moved replace-minus-inf.w/symbol here from fast-events
;;; 
;;; Revision 1.16  90/05/23  22:10:08  vick
;;; removed replace-minus-infinity-by-symbol until i check in the new fast event stuff in en
;;; entirely
;;; 
;;; Revision 1.15  90/05/22  07:55:43  vick
;;; added function replace-minus-infinity-in-pcf-by-symbol from fast-events
;;; 
;;; Revision 1.14  90/05/08  18:34:11  mrose
;;; New routine degrees-to-arcsecs.
;;; 
;;; Revision 1.13  90/04/03  11:17:20  sponsler
;;; New fn: pcf-one-if-nonzero
;;; 
;;; Revision 1.12  90/03/29  03:05:53  vick
;;; added geometric-mean 
;;; 
;;; Revision 1.11  90/03/20  18:03:31  sponsler
;;; mod-pcf-for-input and mod-pcf-for-output repaired.
;;; 
;;; Revision 1.10  90/03/01  18:50:37  sponsler
;;; CORRECT-ANGLE now deals with negative angles, too.
;;; 
;;; Revision 1.9  90/02/15  11:50:08  miller
;;; added jd-to-day-month
;;; 
;;; Revision 1.8  90/02/14  10:13:35  sponsler
;;; New functions: ANGLE-SUBTRACT, ANGLE-ADD, SLOPE-AND-INTERCEPT,
;;;  CORRECT-ANGLE, ADD-2PI-TO-ANGLE, SURROUNDING-POINTS
;;; 
;;; Revision 1.7  90/01/18  11:38:53  miller
;;; added stats to find mean, std deviation, min and max
;;; 
;;; Revision 1.6  90/01/16  15:54:15  miller
;;; corrected *days-in-month* so september hath 30 days
;;; added functions tjd-from-sogs-date and day-of-year-to-day-month-year
;;; 
;;; Revision 1.5  89/12/26  14:01:15  miller
;;; added random-3vect
;;; 
;;; Revision 1.4  89/12/07  00:30:52  don
;;; Force double precision for Unix port (eval-when (eval compile load) ...
;;; 
;;; Revision 1.3  89/11/21  12:56:15  miller
;;; added newling to end of file to make unix happy
;;; 
;;; Revision 1.2  89/11/21  12:53:41  miller
;;; added second optional parameter to time-stamp so you can choose what
;;; comment string it will preceed time with
;;; 

;;; ----------------------------------------------------------------------
;;;
;;; astro-utilities.lisp
;;;
;;;
;;; Created: 24-Oct-1987 MDJ from smaller files in utilities directory
;;; modified 25-Oct-1987 MDJ: cleaned up PCF code (added macros)
;;; modified 12-Dec-1987 MDJ: new version of PCF-non-zero-intervals
;;; modified 31-Jan-1988 MDJ: new routines for finding average, max,
;;;                           and means of PCFs over lists of intervals
;;; modified  6-Mar-1988 MDJ: recoded some PCF routines for efficiency
;;; modified  8-May-1988 MDJ: merged Jeff's routines
;;; modified 23-Jan-1989 SDV: changed pcf routines for greater eff.
;;;                           (see notes by pcf functions for details)
;;; modified 26-Jun-1989 MDJ: commented out some of Shon's PCF routine
;;;                           changes until they can be investigated
;;;                           on very large PCFs;  added integer time
;;;                           routines and integer pcf routines
;;; modified 20-Jul-1989 MDJ: added SOGS output date format routine
;;; modified 13-sep-1989 RK:  added JD-to-TJD macro and 
;;;                           Convert-JD-in-pair-list-to-TJD
;;; 10-12-89 jls:             Changed DATE-TIME fn slightly
;;; 7 Dec 89 DAR              Added (eval-when ( eval compile load) to
;;;                           setf...'double-float to force double precision
;;;                           on SUNs
;;;
;;; ----------------------------------------------------------------------


;;; ----------------------------------------------------------------------
;;;                                         Double Precision
;;;
;;; all numbers should be treated as double precision
;;;

(eval-when (eval compile load)
  (setf *read-default-float-format* 'double-float))



;;; ----------------------------------------------------------------------
;;;                                         Constants based on PI
;;; used for angles etc.
;;;
;;; ----------------------------------------------------------------------

;; Modified Aug. 23, 1990  AAG   Pi must be coerced into a double float
;;    number because of bugs in the vax lisp which cannot handle long-float
;;    formats

(defconstant 2PI (* (coerce pi 'double-float) 2) "2 * PI")

(defconstant PI/2 (/ (coerce pi 'double-float) 2) "PI / 2.0")

(defconstant RTOD (/ 180 (coerce pi 'double-float))
  "Conversion factor from radians to degrees:  180/PI")

;;; ----------------------------------------------------------------------
;;;                                         Infinity
;;; ----------------------------------------------------------------------

(defconstant *MINUS-INFINITY* most-negative-double-float)
(defconstant *PLUS-INFINITY* most-positive-double-float)


;;; ----------------------------------------------------------------------
;;;                                         Astronomical Constants
;;; ----------------------------------------------------------------------

(defconstant *earth-radius* 6378.14 "Radius of the earth in km")

;;; ----------------------------------------------------------------------
;;;                                         Statistics etc.
;;; ----------------------------------------------------------------------


(defun GEOMETRIC-MEAN (&rest numbers)
  
  "takes the geometric mean of a bunch of numbers (caveat: I did my best 
   but this is subject to some roundoff error). It flattens lists before it takes the mean
   so you can either say (geometric-mean n1 n2 n3 n4 n5 n6 ....) or 
  (geometric-mean '(n1 n2 n3 n4 n5 n6 ....))"
  
  (let ((the-numbers  (flatten numbers numbers)))
    (if (some #'zerop the-numbers)
      0
      (coerce
       (exp (/ (apply #'+ (mapcar #'(lambda (x)
                                      (log (coerce x 'double-float)))
                                  the-numbers))
               (coerce (length the-numbers) 'double-float )))
       'single-float))))

(defun AVERAGE (&rest numbers)
  "Calculate the average of NUMBERS."
  (/ (apply #'+ numbers) (length numbers)))

(defun MEDIAN (&rest numbers)
  "Returns the median number from list NUMBERS."
  ;; (median 2 3 4 5 100) => 4
  (let ((length (length numbers)))
    (nth (round (/ length 2)) (safe-sort numbers #'<))))

(defun SQUARE (x)
  "Return the square of X"
  (* x x))

(defun EVEN (x)
  "Round to the nearest even integer, Written Oct. 5, 1990  AAG"
  (* 2 (truncate (+ 0.5 (/ x 2)))))

(defun SUM-OF-SQUARES (&rest numbers)
  "Return the sum of the squares of the arguments."
  (reduce #'+ (mapcar #'square numbers)))

(defun STATS (list)
  "given a list of numbers, return multiple values for the 
   mean, standard deviation, minimum and maximum.
   (Standard deviation is standard deviation of the sample,
   not the universe, i.e. the square root of the sum of the
   square of the values minus the average divided by the 
   number of values minus 1)"
  (cond
    ((null list) nil)
    (t
     (let* ((n 0)
	    (sum 0)
	    (sum-square 0)
	    (average 0)
	    (sigma 0)
	    (min (first list))
	    (max (first list)))
       (dolist (l list)
	 (incf n)
	 (incf sum l)
	 (incf sum-square (* l l))
	 (when (> l max) (setf max l))
	 (when (< l min) (setf min l))
	 )
       (setf average (/ sum n))
       (setf sigma (cond ((equal n 1) 0)
			 (t (sqrt (/ (- sum-square (/ (* sum sum) n)) (1- n))))))
       (values average sigma min max)))))

;;; ----------------------------------------------------------------------
;;;                                         RANDOM NUMBERS ETC.
;;; ----------------------------------------------------------------------

(defun RANDOM-NEVER-ZERO (number &optional (value-if-zero 1) state)
  
  "Similar to random function, but never returns zero - if random
    returns 0, this returns value-if-zero. State is arg to CL random function.
    useful when you want a random number of things but don't want 0 things"
  
  (let ((rand (if (random-state-p state) 
                (random number state)
                (random number))))
    (if (<= rand 0) 
      value-if-zero
      rand)))


(defun RANDOM-NORMAL (&key (mean 0) (sigma 1) state)
  
  "Return a pseudo random number drawn from the Normal (Gaussian)
   distribution with specified mean and sigma. State is argument
   to CL random function."
  
  ;;refer to Hamming p 388
  ;; sum of 12 uniformly distributed random numbers on [0,1] gives close
  ;; approx to normal dist with mean of 6 and variance of 1
  ;; so subtract shift to get mean of 0, multiply by desired sigma and add mean
  ;; code below is capable of handling any sample size if you change value
  ;; of variable sample (but dont change the 12.0 in factor)
  
  (let* ((sum 0)
         (sample 12)
         (shift (/ sample 2.0))
         (factor (sqrt (/ 12.0 sample))))
    (dotimes (i sample)
      (incf sum (random 1.0 state))) 
    (+ mean (* sigma factor (- sum shift)))))


(defun RANDOM-NORMAL-2 (&key (mean 0) (sigma 1) state)
  
  "Returns 2 pseudo random numbers drawn from the Normal (Gaussian)
   distribution with specified mean and sigma. State is argument
   to CL random function. Generated via Box-Muller method."
  ;; refer to Kalos and Whitlock Monte Carlo Methods Vol 1 p 48
  ;; this actually generates two values
  
  (let ((r1 (random 1.0 state))
        (r2 (random 1.0 state)))
    (values
     (+ mean (* sigma (* (sqrt (* -2.0 (log r1) ))(cos (* 2 pi r2)))))
     (+ mean (* sigma (* (sqrt (* -2.0 (log r1) ))(sin (* 2 pi r2))))))))


(defun RANDOMIZE-LIST (list &key (length (list-length list)))
  
  "Returns a new list with the elements of list in random order.
  list is unchanged"
  
  (let ((working-list (copy-list list))
        (length-source (list-length list))
        tail
        index
        result)
    (if 
      (> length length-source)
      (error "RANDOMIZE-LIST -- size of subset ~a cannot be greater than size of original set ~a" 
             (list-length list) length))
    ;;; At each iteration, select and delete a random element from the list.
    (do ((len length-source (1- len)) (length-result 0 (1+ length-result)))
        ((>= length-result length) result)
      (setf index (random len))
      (if 
        (zerop index)			; a special case if the first element is selected; otherwise we want to 
        ; access the list beginning with the element just prior to the selected one;
        ; is there a way to avoid this specialization?
        (push (pop working-list) result)
        (progn
          (setf tail (nthcdr (1- index) working-list)) ; the sublist begining with the element just
          ; prior to the one selected
          (push (second tail) result) ;; the selected element
          (rplacd tail (rest (rest tail)))))))) ;; delete the selected element


(defun N-RANDOM-ELEMENTS-FROM-LIST (list N &key (test #'equal))
  
  "Return list of N elements chosen from random from input list"
  ;; (N-random-elements-from-list '(0 1 2 3 4) 1)
  ;; (N-random-elements-from-list '(0 1 2 3 4) 5)
  ;; (N-random-elements-from-list '(0 1 2 3 4) 6)
  ;; (N-random-elements-from-list nil 6)
  
  (let ((result nil)
        (len (length list)))
    (cond ((= N 1)
           (setf result (list (elt list (random len)))))
          ((>= N len)
           (setf result list))
          (t (loop
               (pushnew (elt list (random len)) result
                        :test test)
               (if (= (length result) N) (return nil)))))
    result))


(defun A-RANDOM-ELEMENT-FROM-LIST (list)
  
  "Return one element from the list, selected at random"
  ;; (a-random-element-from-list '(0 1 2 3 4))
  ;; (a-random-element-from-list nil)
  
  (if (null list)
    nil
    (elt list (random (length list)))))


;;;>> I believe the English word here is "permute".  In any case, is this any
;;;>> different from RANDOMIZE-LIST above?

(defun PERMUTATE (sequence)
  "takes a sequence and returns a permutation of that sequence"
  (let ((length (length sequence))
        temp random-index
        (sequence (copy-seq sequence)))
    (dotimes (i length)
      (setf temp (elt sequence i))
      (setf random-index (random length))
      (setf (elt sequence i) (elt sequence random-index ))
      (setf (elt sequence random-index) temp))
    sequence))


(defun MAKE-RANDOM-SUBSET-LIST (list &key (length (list-length list)))
  
  "Given a list return a list of length at max :length composed of 
    elements from the original list. See also randomize-list. above."
  
  (aux-make-random-subset-list nil list length 0))


(defun AUX-MAKE-RANDOM-SUBSET-LIST (so-far to-go max-length last-length)

  "Aux function for make-random-subset-list"

  (cond ((null to-go)
         (nreverse so-far))
        ((equal last-length max-length)
         (nreverse so-far))
        ((zerop (random 2))
         (aux-make-random-subset-list so-far (cdr to-go) max-length last-length))
        (t
         (aux-make-random-subset-list
          (cons (car to-go) so-far) (cdr to-go) max-length (1+ last-length)))))

;;; ----------------------------------------------------------------------
;;;                                         Random selection from discrete
;;;                                         distributions


;;; (normalize-discrete-pdist '(1 2 3 4)) => (1/10 1/5 3/10 2/5)

(defun NORMALIZE-DISCRETE-PDIST (rel-plist)
  "rel plist is a list of numbers proportional to probability.  Normalize
    so that each is the probability, and sum prob = 1"
  (let ((result nil)
        (sum 0))
    (setf sum (apply #'+ rel-plist))
    (if (<= sum 0) (error "sum probabilities <= 0"))
    (dolist (p rel-plist)
      (push (/ p sum) result))
    (nreverse result)))


;;; (cum-pdist '(0.1 0.1 0.5 0.25 0.05)) => (0.1 0.2 0.7 0.95 1.0)

(defun CUM-PDIST (plist)
  "input normalized discrete probability list, return cum prob distrib
    as list"
  (let ((result nil)
        (cump 0))
    (dolist (p plist)
      (incf cump p)
      (push cump result))
    (nreverse result)))

(defun SELECT-FROM-RANDOM-DISCRETE-DISTRIBUTION
       (&key REL-PDIST PDIST CUM-PDIST)
  
  "input one of:
     rel-pdist: list of numbers s.t. value of ith element in list is proportional
      to probability of i, i = 0,...,(1- (length list))
     pdist: normalized rel-pdist, s.t. sum of values = 1
     cum-pdist: cum version of pdist, s.t. ith element in list is prob. of
       any of 0,...,i
    return: selection from distribution i in the range 0,...,(1- (length list))
    Note:  for efficiency provide cum-pdist, since if will be computed each
     time if not provided as input"
  
  (when (and rel-pdist (not pdist) (not cum-pdist))
    (setf cum-pdist (cum-pdist (normalize-discrete-pdist rel-pdist))))
  (when (and pdist (not cum-pdist))
    (setf cum-pdist (cum-pdist pdist)))
  (if (not cum-pdist) (error "No cum prob distribution available"))
  
  (let ((rand (random 1.0))
        (count 0))
    (dolist (p cum-pdist)
      (if (<= rand p) (return count) (incf count)))
    count))

;; (SELECT-FROM-RANDOM-DISCRETE-DISTRIBUTION :rel-pdist '(1 10)) => 0 or 1

#| more extensive test of select...
(let* ((rel-pdist '(1 2 4))
       (cum-pdist (cum-pdist (normalize-discrete-pdist rel-pdist)))
       (result (make-array 3 :initial-element 0)))
  (format t "~%rel: ~a~%norm rel: ~a = ~a~%cum: ~a = ~a" 
          rel-pdist (normalize-discrete-pdist rel-pdist)
          (mapcar #'float (normalize-discrete-pdist rel-pdist))
          cum-pdist (mapcar #'float cum-pdist))
  (dotimes (i 1000)
    (incf (aref result (select-from-random-discrete-distribution 
                        :cum-pdist cum-pdist))))
  (let ((*print-array* t))
    (format t "~%result: ~a" result)))

|#

;; (random-discrete-selection '((:a 1)(:b 4))) => :B 80% of time, :A 20%

(defun RANDOM-DISCRETE-SELECTION (item-list)

  "input item list in the form: ((item1 p1)(item2 p2)...)
    return a selected item-i from the list with probability relative to p-i
    Note: not the most efficient for extensive multiple calls"

  (let ((items (mapcar #'first item-list))
        (prob (mapcar #'second item-list)))
    (elt items (select-from-random-discrete-distribution :rel-pdist prob))))

;;; ----------------------------------------------------------------------
;;;                                         Angle utilities
;;;
;;; angle conversion and manipulation routines, including those for
;;; astronomical angle format conversion (hours, minutes, seconds etc.)
;;;


(defun DEGREES (angle)
  "Convert angle from radians to degrees"
  (* angle rtod))


(defun RADIANS (angle)
  "Convert angle from degrees to radians"
  (/ angle rtod))


(defun DEGREES-TO-ARCSECS (x)
  "Convert angle from degrees to arc-seconds"
  (* x 3600.0))

(defun RADIANS-TO-ARCSECS (angle)
  "Convert angle directly from radians to arc-seconds"
  (Degrees-To-Arcsecs (Degrees angle)))

(defun ARCSECS-TO-DEGREES (x)
  "Convert angle from arc-seconds to degrees"
  (/ x 3600.0))



#| New JLS 26 june 90
(macroexpand '(convert-angles-to-units :degrees theta beta alpha)) =>
(SETQ ALPHA (DEGREES ALPHA)
      BETA (DEGREES BETA)
      THETA (DEGREES THETA))
|#

(defmacro CONVERT-ANGLES-TO-UNITS (units &rest angles)
  "Process all symbols in list ANGLES.
   Converts the angle bound to a symbol to UNITS
   which may be either :DEGREES or :RADIANS"
  (let ((fn (ecase units
	      (:degrees 'degrees)
	      (:radians 'radians)))
	form)
    (dolist (angle angles)
      (setf form (cons angle (cons `(,fn ,angle) form))))
    (cons 'setq form)))


;;; The next two routines are useful only for presenting "astronomer-
;;; friendly" coordinate output.  Both reduce the precision of the
;;; input angle.

(defun DEGREES-OF-RA-TO-HMS (ra)
  "Convert right ascension in decimal degrees to list of
    (hours minutes seconds)
    Seconds are returned to one decimal point only."
  (let* ((dec-hours (/ ra 15))
         (h (floor dec-hours))
         (rem1 (mod dec-hours 1))
         (m (floor (* rem1 60)))
         (rem2 (- rem1 (/ m 60)))
         (s (/ (round (* 10.0 (* rem2 3600))) 10.0)))
    (list h m s)))


(defun DEGREES-OF-DEC-TO-DMS (dec)
  "Convert Declination in decimal degrees to list of
    (degrees minutes seconds)
    Integral seconds only returned"
  (let* ((abs-dec (abs dec))
         (d (floor abs-dec))
         (rem1 (mod abs-dec 1))
         (m (floor (* rem1 60)))
         (rem2 (- rem1 (/ m 60)))
         (s (round (* rem2 3600))))
    ;; put the sign on the most significant component
    (if (< dec 0)
      (cond ((not (zerop d)) (setf d (- d)))
            ((not (zerop m)) (setf m (- m)))
            ((not (zerop s)) (setf s (- s)))))
    (list d m s)))


;;; utility to convert hours, minutes, seconds to degrees of RA
;;; i.e. inverse of above

(defun HMS-TO-RA (h m s)
  (if (or (< h 0) (> h 23)
          (< m 0) (> m 59)
          (< s 0) (> s 59.9999))
    (error "input out of range: h,m,s ~a ~a ~a" h m s))
  (* 15.0 (+ h (/ m 60.0) (/ s 3600.0))))


;;; utility to convert degrees, minutes, seconds to degrees of Dec

(defun DMS-TO-DEC (sign d m s)
  (if (not (or (eq sign '+) (eq sign '-)))
    (error "sign must be '+ or '-, but value is ~a" sign))
  (if (or (< d 0) (> d 359)
          (< m 0) (> m 59)
          (< s 0) (> s 59.9999))
    (error "input out of range: d,m,s ~a ~a ~a" d m s))
  (let ((abs-value (+ d (/ m 60.0) (/ s 3600.0))))
    (if (eq sign '-) (- abs-value) abs-value)))


;;; utilities for ordering angles


(defun INCLUDED-ANGLE (angle1 angle2 &key (units :radians))
  "return the smallest angle between angle1 and angle2, ie included angle
  less than pi/2. UNITS can be :RADIANS or DEGREES"
  
  ;; rem with 2pi to handle angles greater than 2pi
  ;; abs since included angle is defined as positive
  ;; min used since diff may be complement of included angle, depending on order of arguments  
  
  (let* ((fullcircle (case units (:radians 2pi) (:degrees 360)))
	 (diff (abs (- (rem angle1 fullcircle) (rem angle2 fullcircle)))))
    (min diff (- fullcircle diff))
    ))



;;; --- new functions add by JLS 13-feb-90

#|
(angle-subtract 100 200 :units :degrees) => 260
(angle-subtract 100 100 :units :degrees) => 0
(angle-subtract 100 20 :units :degrees) => 80
|#
(defun ANGLE-SUBTRACT (angle1 angle2 &key (units :radians))
  "Subtract ANGLE2 from ANGLE1.
   If the answer is negative, add 2pi radians to it."
  (let ((answer (- angle1 angle2)))
    (if (minusp answer)
	(+ answer (case units
		    (:radians 2pi)
		    (:degrees 360)))
	answer)))


#|
(angle-add 200 200 :units :degrees) => 40
(angle-add 200 160 :units :degrees) => 360
(angle-add 200 20 :units :degrees) => 220
|#
(defun ANGLE-ADD (angle1 angle2 &key (units :radians))
  "Add ANGLE2 to ANGLE1.
   If the answer is greater then a 2pi, subtract 2pi radians from it."
  (let ((answer (+ angle1 angle2))
	(correction (case units (:radians 2pi) (:degrees 360))))
    (if (> answer correction)
	(- answer correction)
	answer)))




#|
(slope-and-intercept 2 2 2 5) => :INFINITY NIL
(slope-and-intercept 2 2 4 4) => 1 0
(slope-and-intercept 2 3 5 7) => 1.33 0.33
|#
(defun SLOPE-AND-INTERCEPT (x1 y1 x2 y2)
  "Given two points in cartesian system, returns the SLOPE and the Y-INTERCEPT.
   If the slope is infinite, then :INFINITY and NIL are returned."
  (let* ((rise (float (- y2 y1)))
	 (run (- x2 x1))
	 (slope (if (equal run 0) :infinity
		    (/ rise run)))
	 (intercept (if (equal slope :infinity) nil
			(- y1 (* slope x1)))))
    (values slope intercept)))




#|
(correct-angle 1000 :units :degrees) => 280
(correct-angle 730 :units :degrees) => 10
(correct-angle -100 :units :degrees) => 260
(correct-angle -2) => 4.28
|#
(defun CORRECT-ANGLE (angle &key (units :radians))
  "Adjust ANGLE so that it is between 0 and + 359 degrees (inclusive).
   360 will be returned as 0.
   This includes correcting NEGATIVE angles.
   UNITS specifies radians or degrees; returns same units."
  (if (minusp angle) (make-angle-positive angle :units units)
      (iterate LOCALS (circle (if (equal units :radians) 2pi 360))
	       UNTIL (< angle circle)
	       DOING (setf angle (- angle circle))
	       RETURN angle)))


(defun ADD-2PI-TO-ANGLE (angle &key (units :radians))
  "Add a circle to ANGLE.
   UNITS can be :RADIANS or :DEGREES"
  (+ angle (case units
	     (:radians 2pi)
	     (:degrees 360))))

#|
(make-angle-positive -20 :units :degrees) => 340
(make-angle-positive -360 :units :degrees) => 0
(make-angle-positive -500 :units :degrees) => 220
(make-angle-positive -2) => 4.28
(make-angle-positive (minus pi)) => pi
|#
(defun MAKE-ANGLE-POSITIVE (angle &key (units :radians))
  "Loop: If angle is zero or above return it. Otherwise add a circle and loop again.
   UNITS may be :RADIANS or :DEGREES."
  (iterate LOCALS (circle (if (equal units :radians) 2pi 360))
	   UNTIL (or (plusp angle) (zerop angle))
	   DOING (setf angle (+ angle circle))
	   RETURN angle))

(defun CONVERT-ANGLE (angle-in-radians &key (to :DEGREES))
  "Makes it easy to convert radians if desired.
   Argument TO can be :DEGREES or :RADIANS"
  (ecase to
    (:RADIANS angle-in-radians)
    (:DEGREES (degrees angle-in-radians))))

#|
(angle-in-quadrant-p 45 1 :units :degrees) => T
(angle-in-quadrant-p 44 2 :units :degrees) => NIL
(angle-in-quadrant-p 190 3 :units :degrees) => T
|#
(defun ANGLE-IN-QUADRANT-P (angle quadrant &key (units :radians))
  "If ANGLE is in QUADRANT then T is returned.
   Quadrants are numbered from 1 to 4 counter-clockwise."
  (unless (fixnump quadrant) (error "Quadrant must be an integer."))
  (unless (and (> quadrant 0) (< quadrant 5)) (error "Illegal quadrant number."))
  (equal quadrant (quadrant-of-angle angle :units units)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;  EXTERNAL FUNCTION: Slope-Arctan
;
;  This function takes an arc tangent of a slope, given an x and y componant
;
;  ARGUMENTS
;     y-componant              The y componant of the slope
;     x-componant              The x componant of the slope
;
;  RETURNS
;     the arctangent of the slope
;
;  HISTORY
;     Written Sept. 24, 1990  AAG
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun Slope-Arctan (y-componant x-componant)
  (cond
    ((zerop x-componant)
     (cond
       ((zerop y-componant)
	0) ; use zero as a convention for arctan 0/0
       (t
	(/ (coerce pi 'double-float) 2))))
    (t
     (atan (/ y-componant x-componant))))
  )

#|
(quadrant-of-angle 0 :units :degrees) => 1
(quadrant-of-angle 45 :units :degrees) => 1
(quadrant-of-angle 100 :units :degrees) => 2
(quadrant-of-angle 200 :units :degrees) => 3
(quadrant-of-angle 300 :units :degrees) => 4
(quadrant-of-angle 380 :units :degrees) => 1 
|#
(defun QUADRANT-OF-ANGLE (angle &key (units :radians))
  "Returns an integer from 1-4 counting counter-clockwise
   starting from 0 degrees."
  (let ((quarter-circle (if (equal units :radians) pi/2 90)))
    (setq angle (correct-angle angle :units units))
    (cond ((> quarter-circle angle) 1)
	  ((> (* 2 quarter-circle) angle) 2)
	  ((> (* 3 quarter-circle) angle) 3)
	  (t 4))))




#|
(surrounding-points 18 :modulus 3) => 18 18
(surrounding-points 16 :modulus 3) => 15 18
(surrounding-points 3402 :modulus 3 :relative-to 3346.5) => 3400 3403
|#
(defun SURROUNDING-POINTS (point &key (modulus 1)
			              (relative-to 0))
  "Returns integer points surrounding POINT.
   These points are divisible by MODULUS.
   RELATIVE-TO is an offset that is first subtracted
   and then added to the results; this allows flexibility
   in determining where a conceptual counting zero is
   for the POINT."
  (setq relative-to (floor relative-to)
	point (- point relative-to))
  (values (+ (floor-modulo point modulus) relative-to)
	  (+ (ceiling-modulo point modulus) relative-to)))


#|
(linear-interpolation 2 2 2 5 3) => generates an error
(linear-interpolation 2 2 4 4 3) => 3
(linear-interpolation 0 2 5 9 2.4) => 5.4
(linear-interpolation 10 10 20 20 19.999999999) => 20
|#
(defun LINEAR-INTERPOLATION (x1 y1 x2 y2 x &key (tolerance 1.0e-10))
  "Determine Y by interpolating from x1y1 to x2y2"
  (cond ((fuzzy-equal x x1 tolerance) y1)
	((fuzzy-equal x x2 tolerance) y2)
	(t (multiple-value-bind (slope intercept)
	       (slope-and-intercept x1 y1 x2 y2)
	     (if (equal slope :infinity)
		 (error "Infinite SLOPE calculated for line from X1Y1 to X2Y2.~%~
	       ~9TCannot, therefore, do interpolation accurately.")
		 (+ (* slope x) intercept))))))



#|
(floor-modulo 18 3) => 18
(floor-modulo 17 3) => 15
(floor-modulo 12 5) => 10
|#
(defun FLOOR-MODULO (value modulus)
  "Returns the integer that is <= VALUE and
   that is a multiple of MODULUS
   and that is no further away from VALUE than MODULUS.
   Ex: (floor-modulo 17 3) => 15"
  (let ((quotient (floor value modulus)))
    (* modulus quotient)))



#|
(ceiling-modulo 18 3) => 18
(ceiling-modulo 17 3) => 18
(ceiling-modulo 12 5) => 15
|#
(defun CEILING-MODULO (value modulus)
  "Returns the integer that is >= VALUE and
   that is is a multiple of MODULUS.
   and that is no further away from VALUE than MODULUS.
   Ex: (ceiling-modulo 17 3) => 18"
  (let ((quotient (ceiling value modulus)))
    (* modulus quotient)))



;;; ----------------------------------------------------------------------
;;;                                         Earth Utilities
;;;
;;; utilities for astronomical calculations relating to the earth


(defun GMST (jd)
  "Calculate Greenwich Mean Siderial Time in radians, given the
    (Truncated) Julian Date as input.  Algorithm is from Taff.
    Note:  -28980 = JD 1900.0 - 2444000.0 (TJD)"
  (let* ((tu (/ (- jd -28980) 36525))
         (fracday (+ (- jd (truncate jd)) 0.5))
         (afms (+ (/ (+ (* 6 3600) 
                        (* 38 60) 
                        45.836) 
                     86400)
                  (* tu 
                     (/ 8640184.542 86400))
                  (* tu 
                     tu 
                     (/ 0.0929 86400))))
         (GMST (mod (+ fracday afms) 1)))
    (* 2pi GMST)))


(defun EARTH-ORIENTATION (jd)
  "Calculate orientation matrix of earth body reference vectors
    at the specified time.  The matrix is defined as follows:
    Using mean sidereal time (MST) compute celestial vectors
    X,Y,Z corresponding to earth orientation.  Z is along 
    North pole, X on equator at Greenwich meridian, Y makes
    an orthogonal RHS.  All are unit length and are returned as the
    three columns of the earth's orientation matrix."
  ;; Alternative computation (see ETRIAD)
  ;;  (setf x (3vect-spherical-to-cartesian (list MST 0)))
  ;;  (setf y (3vect-spherical-to-cartesian (list (+ MST pi/2) 0)))
  ;;  (setf z (3vect-cross-product x y))
  ;;  (list x y z)
  (let* ((MST (GMST jd))			; Mean Sidereal Time
	 (cos-MST (cos MST))			; cos of MST
	 (sin-MST (sin MST)))			; sin of MST
    (list (list cos-MST sin-MST 0)		; construct the matrix
	  (list (- sin-MST) cos-MST 0)
	  '(0 0 1))))


;;; ----------------------------------------------------------------------
;;;                                         Roll & V3 PA utilities

;;; spherical coordinates below:  
;;;   alpha, delta = celestial longitude, latitute
;;;   lambda beta = ecliptic longitude latitude


(defun SIN-ANGLE-TO-SUN 
       (lambda beta 
	 &key sun-longitude v3-ecliptic-PA)
  "Compute sin of angle to sun given ecliptic longitude and latitude
    lambda and beta and ONE of sun-longitude or v3 ecliptic position angle"
  (cond ((and sun-longitude (null v3-ecliptic-PA))
         (let ((diff (- sun-longitude lambda)))
           (cond ((zerop diff)
                  (sin beta))
                 (t (let ((term (* (cos diff) (cos beta))))
                      (sqrt (- 1.0
                               (* term term))))))))
        ((and (null sun-longitude) v3-ecliptic-PA)
         (let ((term (* (cos beta) (sin v3-ecliptic-PA))))
           (/ (abs (sin beta))
              (sqrt (- 1.0 (* term term))))))
        (t (error "either sun-longitude or V3 ecl PA must be specified"))))

                       
;;; Changed 6AUG91 JLS

(defun PA-OF-NORTH-ECLIPTIC-POLE  (alpha delta beta
				   &key (obliquity *default-ecliptic-obliquity*))
  "Compute position angle of north ecliptic pole, i.e. w.r.t. north
    celestial pole with target as vertex of angle.
   Target is at ALPHA,DELTA (in radians)
   BETA is ecliptic latitude of the target"
  (declare (special *default-ecliptic-obliquity*))
  (let ((cos-delta (cos delta))
	(cos-beta (cos beta)))
    (cond ((or (zerop cos-delta)
	       (zerop cos-beta))
	   0.0)   ;; If Target is NCP or NEP and therefore PA is undefined, we define it as zero.
	  (t (let* ((x (/ (- (cos obliquity) (* (sin delta) (sin beta)))
			  (* cos-delta cos-beta)))
		    (y (if (< (abs x) 1.0) (acos x) 0.0)))
	       (if (> (cos alpha) 0.0)
		   (- y)
		   y))))))


;;; ----------------------------------------------------------------------
;;;                                         Vector & Matrix Utilities
;;;
;;; utility 3-vector and 3x3-matrix routines
;;; Adapted from ASTROLIB & PEP's skysubs.c (mostly)
;;;
;;;
;;; Notes on data representation:
;;; Although the representation of vectors and matrices should NOT be
;;; assumed anywhere outside this file, the routines herein are based on
;;; a specific choice as follows:
;;; --- vectors:
;;;     a 3-vector is represented as list of three numbers
;;; --- matrices:
;;;     a 3x3 matrix is represented as list of three 3-vectors 
;;;     which are the COLUMNS of the matrix

;;; RA and Dec (or Latitude and Longitude) are represented as a list
;;; of two angles (in radians)

;;; Notes on nomenclature:
;;; vector routines normally start with 3vect-
;;; matrix routines normally start with 3x3matrix-

;;; Notes on error checking: there isn't much here.  Sigh.

;;; ----------------------------------------------------------------------
;;;                                         Basic Vector Functions


(defun 3VECT-ADD (v1 v2)
  "Add two 3-vectors"
  (mapcar #'+ v1 v2))


(defun 3VECT-SUBTRACT (v1 v2)
  "Subtract two 3-vectors"
  (mapcar #'- v1 v2))


(defun 3VECT-DOT-PRODUCT (v1 v2)
  "Return the dot product of two 3-vectors"
  (apply #'+ (mapcar #'* v1 v2)))


(defun 3VECT-CROSS-PRODUCT (v1 v2)
  "Return the cross product of two 3-vectors"
  (list (- (* (second v1)(third v2))
           (* (third v1)(second v2)))
        (- (* (third v1)(first v2))
           (* (first v1)(third v2)))
        (- (* (first v1)(second v2))
           (* (second v1)(first v2)))))


(defun 3VECT-MAGNITUDE (v)
  "Return the magtitude of a 3-vector"
  (sqrt (apply #'+ (mapcar #'* v v))))


(defun 3VECT-NORMALIZE (v)
  (let ((mag (3vect-magnitude v)))
    (list (/ (first v) mag)
	  (/ (second v) mag)
	  (/ (third v) mag))))


(defun 3VECT-PROJECT-TO-XY-PLANE (v)
  "Project input vector to xy plane and normalize"
  ;; this will die if the vector v has only a z component
  (let ((norm (sqrt (+ (* (first v) (first v))
		       (* (second v) (second v))))))
    (list (/ (first v) norm)
	  (/ (second v) norm)
	  0)))


(defun 3VECT-LINEAR-COMB (a1 v1 a2 v2)
  "Return linear combination of two 3-vectors:
    a1*v1 + a2*v2 where a1 a2 scalars, v1 v2 vectors"
  (list (+ (* a1 (first v1))
           (* a2 (first v2)))
        (+ (* a1 (second v1))
           (* a2 (second v2)))
        (+ (* a1 (third v1))
           (* a2 (third v2)))))


(defun 3VECT-TIMES-SCALAR (a v)
  "Return the product of a scalar a times a 3-vector v"
  (list (* a (first v))
        (* a (second v))
        (* a (third v))))

(defun 3VECT-SEPARATION (v1 v2 &key (units :radians))
  "Return angular distance between vectors v1 and v2.
    Optional keyword arg specifies :radians or degrees"
  ;; (3vect-separation '(1 0 0) '(0 1 0))
  ;; (3vect-separation '(1 0 0) '(0 1 0) :units :degrees)

  (let* ((dot-prod (3vect-dot-product 
		    (3vect-normalize v1)
		    (3vect-normalize v2)))
	 (result (cond ((<= (abs dot-prod) 1)
			(acos dot-prod))
		       ((> dot-prod 1) 0)
		       ((< dot-prod -1) pi))))
    (if (eq units :degrees)
      (degrees result)
      result)))

(defun RANDOM-3VECT (&optional (state *random-state*))
 "returns a list (x y z) of a 3 vector randomly drawn from
  unit sphere. state is argument to CL random function"

 ;converted from fortran function rands in astrolibe
 ;i'm not exactly sure what the algorithm is
 ;but it does seem to work

 (let (a b (r1 1) r2)
   (do ()
       ((<= r1 0.25))
     (setf a (- (random 1.0 state) 0.5))
     (setf b (- (random 1.0 state) 0.5))
     (setf r1 (+ (* a a) (* b b))))
   (setf r2 (* 8.0 (sqrt (- 0.25 r1))))
   (list (* a r2)
         (* b r2)
         (- (* 8.0 r1) 1.0))))

;;; ----------------------------------------------------------------------
;;;                                         Longitude & Latitude

;;; vector conversion to/from ra, dec (longitude latitude)

(defun 3VECT-CARTESIAN-TO-SPHERICAL (v)
  "Convert 3-vector v to list (longitude latitude)"
  (let ((temp (sqrt (+ (* (first v)(first v))
                       (* (second v)(second v)))))
        (long)
        (lat))
    (if (/= temp 0.0)
	;; then vector not along + or - Z-axis
	(progn
	  (setf long (atan (second v) (first v)))
	  (if (< long 0.0)
	      (setf long (+ long 2pi)))
	  (setf lat (atan (/ (third v) temp))))
	;; else vector lies along + or minus z axis
	(progn
	  (setf long 0.0)
	  (setf lat (if (> (third v) 0.0)
			pi/2
			(- pi/2)))))
    (list long lat)))


(defun 3VECT-SPHERICAL-TO-CARTESIAN (long-lat)
  "Convert list (longitude latitude) to a 3-vector"
  (let* ((long (first long-lat))
         (lat (second long-lat))
         (temp (cos lat)))
    (list (* temp (cos long))
	  (* temp (sin long))
	  (sin lat))))


#| test case 
(let* ((v (3vect-normalize '(1 2 2)))
      (r1 (position-ra-dec v :units :radians))
      (r2 (position-ra-dec v :units :degrees))
      (r3 (3vect-cartesian-to-spherical v)))
  ;; these should be the same
  (print "vect -> ra dec")
  (print r1)
  (print (mapcar #'radians r2))
  (print r3)
  ;; convert back
  (print "ra dec -> vect")
  (print (3vect-spherical-to-cartesian r1))
  (print (ra-dec-to-cartesian-3vect r2 :units :degrees))
  (print (ra-dec-to-cartesian-3vect r3 :units :radians))
  )
|#

(defun POSITION-RA-DEC (3vect &key (units :radians))
  "Convert a triple position vector to ra and dec.
   UNITS can be either :RADIANS or :DEGREES.
   Returns a list."
  (let ((result (3vect-cartesian-to-spherical 3vect)))
    (case units
      (:radians result)
      (:degrees (list (degrees (first result))
		      (degrees (second result)))))))


(defun RA-DEC-TO-CARTESIAN-3VECT (ra-dec &key (units :radians))
  "Convert a list containing ra and dec to a triple position vector.
   Units can be :RADIANS or :DEGREES"
  (3vect-spherical-to-cartesian
    (case units
	  (:radians ra-dec)
	  (:degrees (list (radians (first ra-dec))
			  (radians (second ra-dec)))))))

;;Written August 2, 1991 RLH

(defun Angular-Separation-From-RA-DEC (point1 point2 &key (units :radians))

  "Returns the angular separation between two points in space.  Each 
   point should be a list of RA and DEC.  Units can be :RADIANS or :DEGREES."

  (3vect-Separation
    (RA-Dec-To-Cartesian-3vect point1 :units units)
    (RA-Dec-To-Cartesian-3vect point2 :units units)
    :units units)
  )

;;; ----------------------------------------------------------------------
;;;                                         Matrix routines

;;; Definitions:
;;; an orientation matrix: r[i][j] is defined as follows:
;;;
;;; Let the orientation be defined by an orthonormal system x1', x2',
;;; x3', and let x1, x2, x3 be orthonormal basis vectors:
;;; (For the sky, x1 is the vernal equinox, x3 is north cel. pole,
;;; and x1 X x2 = x3.  For ST, x1, x2 x3 are V1, V2, V3, or U1, U2, U3.)
;;;
;;; Then xi' = r * xi, i=1,2,3, e.g.
;;;
;;;     x1'(1) = sum    r(1,j) * x1(j)
;;;             j=1,3
;;;
;;; Thus x1' has components (r11, r21, r31), i.e. the first
;;; column of r, etc.
;;; 
;;; The lisp convention is to store the matrix columnwise, i.e.: the 
;;; matrix (r11 r12 r13)
;;;        (r21 r22 r23)
;;;        (r31 r32 r33)
;;; is stored as ((r11 r21 r31)(r12 r22 r32)(r13 r23 r33))


(defun 3X3MATRIX-TIMES-VECTOR (r v)
  "Multiple 3x3 matrix r times 3-vector v"
  (let ((r1 (mapcar #'first r))			; get rows
	(r2 (mapcar #'second r)) 
	(r3 (mapcar #'third r)))
    (list (3vect-dot-product v r1)
	  (3vect-dot-product v r2)
	  (3vect-dot-product v r3))))


(defun 3X3MATRIX-TRANSPOSE-TIMES-VECTOR (r v)
  "Multiply the transpose of the 3x3 matrix r times the vector v"
  (list (3vect-dot-product v (first r))
        (3vect-dot-product v (second r))
        (3vect-dot-product v (third r))))


(defun 3X3MATRIX-TRANSPOSE (r)
  "Return transpose of the 3x3 matrix r"
  (let ((row1 (mapcar #'first r))		; get rows
	(row2 (mapcar #'second r)) 
	(row3 (mapcar #'third r)))
    (list row1 row2 row3)))


(defun 3X3MATRIX-TIMES-MATRIX (r1 r2)
  "Return matrix product of 3x3 matrices r1 and r2"
  (let ((row1 (mapcar #'first r1))		; get rows
	(row2 (mapcar #'second r1)) 
	(row3 (mapcar #'third r1))
	(column1 (first r2))
	(column2 (second r2))
	(column3 (third r2)))
    (list (list (3vect-dot-product row1 column1)
		(3vect-dot-product row2 column1)
		(3vect-dot-product row3 column1))
	  (list (3vect-dot-product row1 column2)
		(3vect-dot-product row2 column2)
		(3vect-dot-product row3 column2))
	  (list (3vect-dot-product row1 column3)
		(3vect-dot-product row2 column3)
		(3vect-dot-product row3 column3)))))


(defun 3X3MATRIX-TIMES-SCALAR (a r)
  "Multiple the scalar a times every element of the 3x3 matrix r"
  (list (3vect-times-scalar a (first r))
        (3vect-times-scalar a (second r))
        (3vect-times-scalar a (third r))))


(defun 3X3MATRIX-ADD (r1 r2)
  "Return the sum of the 3x3 matrices r1 and r2"
  (list (3vect-add (first r1)(first r2))
        (3vect-add (second r1)(second r2))
        (3vect-add (third r1)(third r2))))


;;; ----------------------------------------------------------------------
;;;                                         Rotations

(defun 3X3ROTATION-MATRIX (pole angle)
  "Return matrix corresponding to rotation about pole by angle"
  (let* ((p (3vect-normalize pole))
         (p0 (first p))
         (p1 (second p))
         (p2 (third p))
         (j (list (list 0.0 p2 (- p1))
                  (list (- p2) 0.0 p0)
                  (list p1 (- p0) 0.0)))
         (j2 (list (list (- (+ (* p1 p1) (* p2 p2))) (* p0 p1) (* p0 p2))
                   (list (* p0 p1) (- (+ (* p0 p0) (* p2 p2))) (* p1 p2))
                   (list (* p0 p2) (* p1 p2) (- (+ (* p0 p0) (* p1 p1))))))
         (cj (sin angle))
         (cj2 (- 1 (cos angle)))
         (result '((1 0 0)(0 1 0)(0 0 1))))
    (setf j (3x3matrix-times-scalar cj j))
    (setf j2 (3x3matrix-times-scalar cj2 j2))
    (setf result (3x3matrix-add result j))
    (setf result (3x3matrix-add result j2))
    result))


(defun 3X3MATRIX-ROTATION-AXIS (r)
  "Return pole and angle corresponding to rotation matrix r.
    Function value returned is angle."
  (if (/= (3x3matrix-rotation-angle r) 0.0)
      (3vect-normalize (list (- (third (second r)) (second (third r)))
                             (- (first (third r)) (third (first r)))
                             (- (second (first r)) (first (second r)))))
      ;; return unit x-vector if null rotation
      '(1 0 0)))


(defun 3X3MATRIX-ROTATION-ANGLE (r)
  "Return the rotation angle determined by the input rotation matrix r"
  (let ((cos-angle (* 0.5 (+ (first (first r))
			     (second (second r))
			     (third (third r))
			     -1.0))))
    (if (> (abs cos-angle) 1.0)
	0.0
	(acos cos-angle))))


(defun 3VECT-ROTATE-ABOUT-POLE (x pole angle)
  "Rotate vector x about pole by angle"
  (3x3matrix-times-vector
    (3x3rotation-matrix pole angle)
    x))


;;; ----------------------------------------------------------------------
;;;                                         Triads and Tangent Planes


(defun 3VECT-TO-TRIAD (v)
  "Return rotation matrix which defines triad centered
    on vector v.  Columns of r are unit basis vectors of triad as follows:
     column 1: v
     column 2: A, local east, increasing longitude
     column 3: D, local north, increasing latitude
    v, A, and D form a right-handed system
    known bugs:  blows up if v is z-axis"
  (let* ((v (3vect-normalize v))
         (a (3vect-normalize (list (- (second v)) (first v) 0.0)))
         (d (3vect-cross-product v a)))
    (list v a d)))


(defun 3VECT-TO-TP-COORD (triad x)
  "Return xsi and eta, tangent plane coordinates of vector v,
    as a list (xsi eta)"
  (let ((x-dot-v (3vect-dot-product (first triad) x))
        (x-dot-a (3vect-dot-product (second triad) x))
        (x-dot-d (3vect-dot-product (third triad) x)))
    (list (/ x-dot-a x-dot-v)
	  (/ x-dot-d x-dot-v))))


(defun 3VECT-FROM-TP-COORD (triad xsi-eta)
  "Return vector corresponding to the list xsi-eta = (xsi eta)"
  (3vect-normalize 
    (3vect-add (first triad)
               (3vect-linear-comb (first xsi-eta) (second triad)
                                  (second xsi-eta) (third triad)))))


;;; ----------------------------------------------------------------------
;;;                                         Display Functions

;;; mostly for debugging, some routines to print properties of vectors
;;; and matrices


(defun 3VECT-PRINT (v &optional (stream t))
  (format stream "mag: ~a" (3vect-magnitude v))
  (format stream "  RA, Dec (deg): ~a~%"
          (mapcar #'degrees (3vect-cartesian-to-spherical v))))


(defun 3x3MATRIX-PRINT (r &optional (stream t))
  (format stream "col1: ")(3vect-print (first r))
  (format stream "col2: ")(3vect-print (second r))
  (format stream "col3: ")(3vect-print (third r))
  (format stream "col1.col2: ~a" (3vect-dot-product (first r)(second r)))
  (format stream "  col1.col3: ~a" (3vect-dot-product (first r)(third r)))
  (format stream "  col2.col3: ~a" (3vect-dot-product (second r)(third r))))


;;; ----------------------------------------------------------------------
;;;                                         Time Utilities
;;;
;;; utility routines for time calculations
;;;
;;; Representation:  time is represented internally in units of days.
;;; Zero is the value *JD-Reference*, which is a Julian Date
;;; corresponding to May 6.5 1979
;;;
;;; test cases
;;; (1) JD 2446135.5201458333 is 11-Mar-1985 00:29:00.6 UT
;;; (2) JD 2444062.917474537040 is 8-Jul-1979 10:01:09.8 UT
;;;     GMST is 1.327432538448 radians


;;; ----------------------------------------------------------------------
;;;                                         Constants

(defconstant *JD-Reference* 2444000
  "The reference origin for time.  Corresponds to May 6.5 1979")


(defconstant *month-names* '(("Jan" . 1)  ("Feb" . 2)  ("Mar" . 3)
                             ("Apr" . 4)  ("May" . 5)  ("Jun" . 6)
                             ("Jul" . 7)  ("Aug" . 8)  ("Sep" . 9)
                             ("Oct" . 10) ("Nov" . 11) ("Dec" . 12)))

(defconstant *day-of-week-names* '(("Mon" . 0) ("Tue" . 1) ("Wed" . 2)
                                   ("Thu" . 3) ("Fri" . 4) ("Sat" . 5)
                                   ("Sun" . 6) ))

;; Merged 7-12-88
(defconstant *days-in-month* '((1 . 31) (2 . 28) (3 . 31) (4  . 30) (5  . 31) (6  . 30)
	                       (7 . 31) (8 . 31) (9 . 30) (10 . 31) (11 . 30) (12 . 31)))

;;; ----------------------------------------------------------------------
;;;                                         Time Function Definitions


(defun MONTH-STRING-TO-MONTH-NUMBER (m)
  "Return number of month given 3 character name, e.g. Jan -> 1
    (case insensitive)
    uses *month-names* as lookup table"
  (rest (assoc m *month-names* :test #'string-equal)))


(defun MONTH-NUMBER-TO-MONTH-STRING (m)
  "Return 3 character name of month given month number:  e.g. 1 -> Jan
    uses *month-names* as lookup table"
  (first (rassoc m *month-names*)))


(defun DAY-OF-WEEK-STRING-TO-DAY-OF-WEEK (m)
  "Return number of day given 3 character name, e.g. Mon -> 0
    (case insensitive)"
  (rest (assoc m *day-of-week-names* :test #'string-equal)))


(defun DAY-OF-WEEK-TO-DAY-OF-WEEK-STRING (m)
  "Return 3 character name of day given day of week number:  e.g. 0 -> Mon"
  (first (rassoc m *day-of-week-names*)))


(defun HOUR-MINUTE-SECOND-TO-FRACTIONAL-DAY (h m s &optional (ms 0))
  ;; (* 86400 (hour-minute-second-to-fractional-day 0 0 0 1))
  (+ (/ h 24.0)
     (/ m 1440.0)
     (/ s 86400.0)
     (/ ms 86400000.0)))



(defun FRACTIONAL-DAY-TO-HOUR-MINUTE-SECOND (v)
  "Return list: hour minute second number-of-milliseconds"
  (let* ((msec (round (* (abs v) 86400000))) ;round to nearest msec
	 (hours (truncate (/ msec 3600000)))
	 (rem1 (- msec (* hours 3600000)))
	 (minutes (truncate (/ rem1 60000)))
	 (rem2 (- msec (* hours 3600000) (* minutes 60000)))
	 (seconds (truncate (/ rem2 1000)))
	 (rem3 (- msec (* hours 3600000) (* minutes 60000) (* seconds 1000))))
    (list hours minutes seconds (round rem3))))


(defun IS-LEAP-YEAR (y)
  "Return t if the input year is a leap year"
  ;; if the year is divisible by 4
  (if (zerop (rem y 4))
      ;; if the year is divisible by 100
      (if (zerop (rem y 100))
          ;; then it is a leap year only if it is divisible by 400
          (if (zerop (rem y 400)) t nil)
          ;; otherwise it is a leap year
          t)
      ;; not divisible by 4, so not a leap year
      nil))


(defun DAY-MONTH-YEAR-TO-DAY-OF-YEAR (d month y)
  "Return day of year given date month year.
    input month may be month number or caseless string, e.g. Jan"
  (let ((m (if (stringp month) 
               (month-string-to-month-number month)
               month)))
    (if (<= m 2)
	;; month less than or equal to 2
	(setf m (truncate 
		  (/ (* (- m 1) 
			(if (is-leap-year y) 62 63))
		     2)))
	;; month greater than 2
	(setf m (- (truncate (* (+ m 1) 
				(/ 153 5)))
		   (if (is-leap-year y) 62 63))))
    (+ m d)))


(defun DAY-OF-YEAR-TO-DAY-MONTH-YEAR (doy year)
 "return list (day month year), given day of year and year"

 ;you need year to know if its a leap year

 (do* ((month 1 (+ month 1))
       ;each iteration, subtract days-in-month, which is for the last month
       (days-left doy (- days-left days-in-month))
       ;now update days in this month, handling leap years correctly
       (days-in-month 
        (number-of-days-in-normal-month 1)
        (if (and (= month 2) (is-leap-year year))
          (+ 1 (number-of-days-in-normal-month month))
          (number-of-days-in-normal-month month))))
      ((<= days-left days-in-month) (list days-left month year))))


(defun JD-TO-DAY-MONTH (tjd)
  "returns a string of the form DD MMM for the specified TJD.
  Use when you don't care about the year or fractional days"
  (let ((dmy (jdut tjd)))
    (format nil "~2d ~3a" 
            (round (first dmy)) 
            (month-number-to-month-string (second dmy)))
    ))


(defun JDUT (jd)
  "Returns list (day month year), where day is fractional"
  (let* ((i (+ *JD-Reference* (floor (+ jd 0.5))))
         (f (rem (+ jd 0.5) 1))
         (a (if (> i 2299160)
                (floor (/ (- i 1867216.25) 36524.25))))
         (b (if (> i 2299160)
                (- (+ i 1 a) (floor (/ a 4)))
                i))
         (c (+ b 1524))
         (d (floor (/ (- c 122.1) 365.25)))
         (e (floor (* d 365.25)))
         (g (floor (/ (- c e) 30.6001)))
         (day (- (+ (- c e) f) (floor (* 30.6001 g))))
         (month (if (< g 13.5)
                    (- g 1)
                    (- g 13)))
         (year (if (> month 2.5)
                   (- d 4716)
                   (- d 4715))))
    (list day month year)))



(defmacro JD-to-TJD (JD)
  "Converts JD to TJD by substracting *JD-Reference* "
  `(- ,JD *JD-Reference*))


(defun UTJD (day month year)
  "Compute Julian date
    Input is day month year, where day may be fractional and month may 
    be caseless string Jan etc. or number: Jan=1, etc.
    Valid only for Gregorian calendar"
  (let* ((m (if (stringp month) 
                (month-string-to-month-number month)
                month))
         (y-prime (if (<= m 2) (- year 1) year))
         (m-prime (if (<= m 2) (+ m 12) m))
         (a (floor (/ y-prime 100.0)))
         (b (+ (- 2 a) (floor (/ a 4.0))))
         (c (floor (* 365.25 y-prime)))
         (d (floor (* 30.6001 (+ m-prime 1))))
         (jd (+ b c d day (- 1720994.5 *JD-Reference*))))
    jd))


;;; Beware:  because of roundoff, day-of-week can give unexpected results
;;; e.g.:  
;;;  (day-of-week 4000.5) => 3
;;;  (day-of-week 4001.5) => 3!!!
;;;  (day-of-week 4001.5000001) => 4
;;; if the times are near day boundaries, add epsilon

(defun DAY-OF-WEEK (TJD)
  "return day of week number for input TJD.  0=Mon, etc."
  (multiple-value-bind
    (rounded remainder) (floor (/ (+ TJD *jd-reference* 0.5) 7))
    (declare (ignore rounded))
    (floor (* remainder 7))))


(defun DAY-OF-WEEK-STRING (TJD)
  "return string for day of week for input TJD"
  (day-of-week-to-day-of-week-string
   (day-of-week TJD)))


(defun WEEK-OF-YEAR (TJD)
  "return week number in year, integral: Jan 1.0 starts the first
    week, Jan 8.0 the second, 15.0 the third, etc.  Weeks are 
    NOT numbered based on Monday starts."
  (let* ((dmy (jdut tjd))
	 (start-of-year (utjd 1.0 1 (third dmy))))
    (1+ (floor (- tjd start-of-year) 7))))

#| test
(week-of-year (utjd 0 1 1992))
(week-of-year (utjd 1 1 1992))
(week-of-year (utjd 7.0 1 1992))
(week-of-year (utjd 7.9999 1 1992))
(week-of-year (utjd 8.0001 1 1992))
(week-of-year (utjd 14.9999 1 1992))
(week-of-year (utjd 15.0001 1 1992))
(week-of-year (utjd 31 12 1992))
(day-of-week (utjd 11.5 11 1991))
(day-of-week-string (utjd 17.0 2 1985)) => "Sun"
(day-of-week-string (utjd 17.0001 2 1985)) => "Sun"
(day-of-week-string (utjd 16.9999 2 1985)) => "Sat"
(day-of-week-string (utjd 17.9999 2 1985)) => "Sun"
(day-of-week-string (utjd 18.0001 2 1985)) => "Mon"
|#


(defun YMDHMS-TO-JD (y mon d &optional h min s (ms 0))
  "convert year, month (number), day of month, hour, minute, second
    to TJD.  h, min, and s may be nil, interpreted as zero."
  (+ (utjd d mon y)
     (hour-minute-second-to-fractional-day 
      (or h 0) (or min 0) (or s 0) (or ms 0))))


(defun JD-TO-YMDHMS (tjd)
  "convert TJD to list (year month day hour minute second millisecond)"
  (let* ((ut (jdut tjd))
         (day-with-frac (first ut))
         (month (second ut))
         (year (third ut)))
    (multiple-value-bind (int-day frac-day) (floor day-with-frac)
      (let ((hms (fractional-day-to-hour-minute-second frac-day)))
        (append (list year month int-day) hms)))))

;;; (apply 'ymdhms-to-jd (jd-to-ymdhms 4012.1))
;;; (apply 'ymdhms-to-jd (jd-to-ymdhms 4012.33213141))

;;; ----------------------------------------------------------------------
;;;                                         Universal Time Functions

;;; this is universal time in Steele's sense, not UT or UTC
#| test cases:
(format-universal-time (get-universal-time))
(get-formatted-universal-time)
(time-stamp)
(date-time)
|#

(defun FORMAT-UNIVERSAL-TIME (ut) 
  "given time in Common Lisp universal time format, return string formatted
    as dd-mmm-yyyy hh:mm:ss.s"
  (Multiple-Value-Bind 
    (Sec Min Hour Date Mon Year Day-Of-Week Daylight-Savings-Time-P Time-Zone) 
      (Decode-Universal-Time ut)
    (declare (ignore time-zone daylight-savings-time-p day-of-week))
    (format nil "~2,'0d-~a-~a ~2,'0d:~2,'0d:~4,1,,,'0f" 
	    date (month-number-to-month-string mon) year hour min sec)))


(defun GET-FORMATTED-UNIVERSAL-TIME ()
  "returns string with current time of day formatted with format-universal-time"
  (format-universal-time (get-universal-time)))


(defun TIME-STAMP (&optional (stream t) (comment ""))
  "format current time to stream, preceeded by line feed and comment string
   Ex:
    (time-stamp t \";;; \") -> ;;; 21-Nov-1989 17:47:35.0 " 
  (format stream "~%~a~a" 
	  comment (get-formatted-universal-time)))

(defun DATE-TIME (&key universal-time)
 "Generate a string that consists of month day year hour and minute
  in the form MM-DD-YY-HH-MM. If universal-time is specified, the corresponding 
  string is generated. If not specified, a string corresponding to the current
  universal time is returned."
  (multiple-value-bind (secs minutes hours day month year)
      (decode-universal-time (or universal-time (get-universal-time)))
    (declare (ignore secs))
    (format nil "~a-~a-~a-~a-~a"
	    month day (subseq (format nil "~a" year) 2) hours minutes)))

(defun CORRECTED-UTJD-FOR-TODAY ()
  "Returns the corrected julian date (eg, 3759.5) for today."
  (multiple-value-bind (secs minutes hours day month year)
      (decode-universal-time (get-universal-time))
    (declare (ignore minutes hours secs))
    (utjd day month year)))

(defun DATE-STRING (universal-time)
  "UNIVERSAL-TIME should be a big integer.
   Returns something like: 9/21/1989 19:9:18"
  (multiple-value-bind (secs minutes hours day month year)
      (decode-universal-time universal-time)
    (format nil "~a/~a/~a ~a:~a:~a" month day year hours minutes secs)))

(defun DATE-STRING-FOR-TODAY ()
  "Returns date and time (like 9/21/1989 19:9:18) in human readable form for NOW"
  (date-string (get-universal-time)))


;;; ----------------------------------------------------------------------
;;;                                         Output Formatting Routines


(defun FORMAT-ABS-TIME (jd)
  "Return formatted string for absolute time.  Input jd is JD"
  (let* ((day-month-year (jdut jd))
         (year (third day-month-year))
         (day-of-year (apply #'day-month-year-to-day-of-year day-month-year))
         (int-day-of-year (floor day-of-year))
         (frac-day (- day-of-year int-day-of-year)))
        (format nil "~2,'0d-~a-~2,'0d ~a"
                (truncate (first day-month-year))
                (month-number-to-month-string (second day-month-year))
                year 
                (format-delta-time-with-hours frac-day))))


(defun FORMAT-DELTA-TIME-WITH-HOURS (v)
  "Given time as fractional days, return formatted string showing hours,
    minutes, seconds.
    Leading hours and minutes are always included, even if zero"
  (let* ((sign-string (if (minusp v) "-" ""))
         (msec (round (* (abs v) 86400.0 1000.0)));round to nearest msec
         (hours (truncate (/ msec 3600000)))
         (rem1 (- msec (* hours 3600000)))
         (minutes (truncate (/ rem1 60000)))
         (rem2 (- msec (* hours 3600000) (* minutes 60000)))
         (seconds (/ rem2 1000)))
        (format nil "~a~2,'0dh~2,'0dm~6,3,0,'?,'0fs" sign-string hours minutes seconds)))


(defun FORMAT-DELTA-TIME (v)
  "Given time as fractional days, return formatted string showing hours,
    minutes, seconds.
    Leading hours and minutes are not included if zero"
  (let* ((sign-string (if (minusp v) "-" ""))
         (msec (round (* (abs v) 86400.0 1000.0)));round to nearest msec
         (hours (truncate (/ msec 3600000)))
         (rem1 (- msec (* hours 3600000)))
         (minutes (truncate (/ rem1 60000)))
         (rem2 (- msec (* hours 3600000) (* minutes 60000)))
         ;;(seconds (truncate (/ rem2 1000)))
         (frac-seconds (/ rem2 1000))
         ;;(msec (- msec (* hours 3600000) (* minutes 60000) (* seconds 1000)))
	 )
    (cond
      ((not (zerop hours))
       (format nil "~a~2,'0dh~2,'0dm~6,3,0,'?,'0fs" sign-string hours minutes frac-seconds))
      ((not (zerop minutes))
       (format nil "~a~2,'0dm~6,3,0,'?,'0fs" sign-string minutes frac-seconds))
      (t
        (format nil "~a~6,3,0,'?,'0fs" sign-string frac-seconds)))))

;;; functions to work in SOGS' format which is YYYY.DOY:HH:MM:SS
;;;  (DOY is day of year)

;; (sogs-date-from-tjd 4046.5) - "1990.155:00:00:00"
;; (tjd-from-sogs-date "1990.155:00:00:00") - 4046.5
;; (sogs-date-from-tjd 4047.49) - "1990.155:23:45:36"
;; (tjd-from-sogs-date "1990.155:23:45:36") - 4047.489583333372
;; (sogs-date-from-tjd 4256.5) - "1990.365:00:00:00"
;; (tjd-from-sogs-date "1990.365:00:00:00") - 4256.5

(defun TJD-FROM-SOGS-DATE (sogs-date-string)
 "given a sogs date string of the form YYYY.DOY:HH:MM:SS,
  return the truncated Julian date. YYYY is the year,
  DOY is the day of the year, HH, MM and SS are hours, minutes
  and seconds."

 (unless (stringp sogs-date-string)
   (error "~a is not a string" sogs-date-string))
 (let (string year doy dmy-list day month hour minute second)

   ;parse sog-ese input: replace first period, all colons with space
   ;tack on parens and read as list
   (setf string
         (substitute #\space #\. sogs-date-string :count 1))
   (setf string
         (concatenate 'string "(" (substitute #\space #\: string) ")"))
   (setf string (read-from-string string))

   ;now set components of date
   (setf year (first string))
   (setf doy (second string))
   (setf hour (third string))
   (setf minute (fourth string))
   (setf second (fifth string))

   ;get day and month from doy
   (setf dmy-list (day-of-year-to-day-month-year doy year))
   (setf day (first dmy-list))
   (setf month (second dmy-list))

   ;use hours and minutes to make fractional day
   (setf day (+ day (hour-minute-second-to-fractional-day hour minute 0)))

   ;now the answer is upon us
   (utjd day month year)
))

(defun SOGS-DATE-FROM-TJD (tjd)
"given a truncated Julian date, return a string with the date
 in SOGS form YYYY.DOY:HH:MM:SS"
  (let* ((ut-list (jdut tjd))
	 (fractional-day (first ut-list))
	 (month (second ut-list))
	 (year (third ut-list))
	 (integer-day (floor fractional-day))
	 (day-fraction (- fractional-day integer-day))
	 (hms-list (fractional-day-to-hour-minute-second day-fraction))
	 (hour (first hms-list))
	 (minute (second hms-list))
	 (second (third hms-list))
	 (day-of-year (day-month-year-to-day-of-year integer-day month year)))
    (format nil "~4d.~3,'0d:~2,'0d:~2,'0d:~2,'0d"
	    year day-of-year hour minute second)))

;;(sogs-date-from-tjd (dmy-to-time '1-jan-1990))
;;(format-abs-time (utjd 25.224 1 1990))
;;(sogs-date-from-tjd (utjd 25.224 1 1990))



(defconstant *YEAR-ZERO-OF-CURRENT-CENTURY* 1900
  "1900 for 20th, 2000 for 21st.  Must be changed manually.")

#|
(short-sogs-date-to-tjd "90.100")
|#
(defun SHORT-SOGS-DATE-TO-TJD (sogs-date)
  "SOGS-DATE should be a string like \"90.100\""
  (let* ((symbols (list-of-symbols-from-string sogs-date #\.))
	 (year (+ *YEAR-ZERO-OF-CURRENT-CENTURY* (read-from-string (first symbols))))
	 list)
    (setf list (day-of-year-to-day-month-year (read-from-string (second symbols))
					      year))
    (utjd (first list) (second list) year)))


#|  Tests:
(tjd-to-short-sogs-date (short-sogs-date-to-tjd "90.150"))
"90.150"

(tjd-to-short-sogs-date 4041.5)
"90.150"

(tjd-to-short-sogs-date 3980.5)
"90.089"

(tjd-to-short-sogs-date 3900.5)
"90.009"
|#

(defun TJD-TO-SHORT-SOGS-DATE (tjd)
  "Converts truncated julian date to a short sogs format (year.days)
   Example: 4041.5 -> 90.150
   The DAYS component will always be padded to three digits"
  (let* ((date-list (jdut tjd))
	 (day (first date-list))
	 (month (second date-list))
	 (year (third date-list)))
    (concatenate 'string
		 (string-it (subseq (string-it year) 2))
		 "."
		 (format nil "~3,'0d" (round (day-month-year-to-day-of-year day month year))))))
	 
;;; ----------------------------------------------------------------------
;;;                                         Input Conversion Routines


;;; examples of input time formats for dmy-to-time:
;;;  (dmy-to-time '(1 "Jan" 1987))
;;;  (dmy-to-time 1 "Jan" 1987)
;;;  (dmy-to-time 1 'Jan 1987)
;;;  (dmy-to-time 1 1 1987)
;;;  (dmy-to-time "1-Jan-1987")
;;;  (dmy-to-time "1-1-1987")
;;;  (dmy-to-time '12-Jun-1987)
;;;  Note: number interpreted as JD...
;;;  (dmy-to-time 2446579.23)
;;;  (dmy-to-time 1 13 1987)

;;;  SDV added call to valid-day-month-year-list to check for validity of date

(defun DMY-TO-TIME (&rest input)
  "Convert a date/time specification to internal units"
  
  (let (prepped-input)
    
    ;; if input is a list consisting of a list
    (if (and (= (list-length input) 1)
	     (listp (first input)))
	;; use the inner list
	(setf prepped-input (first input))
	;; otherwise use the original input
	(setf prepped-input input))
    
    ;; if input is a symbol, interpret as a string
    (if (and (= (list-length prepped-input) 1)
	     (symbolp (first prepped-input)))
	(setf (first prepped-input) (princ-to-string (first prepped-input))))
    
    ;; if input is a single string, stick on parentheses and read it as a list
    ;; substitute spaces for hyphens
    (if (and (= (list-length prepped-input) 1)
	     (stringp (first prepped-input)))
	(setf prepped-input (read-from-string
			      (concatenate 
				'string "(" 
				(substitute #\space #\- (copy-seq (first prepped-input)))
				")"))))
    
    ;; if second element of input is a symbol, convert it to string (month)
    (if (and (= (list-length prepped-input) 3)
	     (symbolp (second prepped-input)))
	(setf (second prepped-input) (princ-to-string (second prepped-input))))
    
    ;; allow 2-digit years
    (if (and (numberp (third prepped-input))
	     (< (third prepped-input) 100))
	(setf (third prepped-input) (+ 1900 (third prepped-input))))

    ;; check for valid types
    (cond ((and (= (list-length prepped-input) 1)
		(numberp (first prepped-input)))
	   ;; if a number then assume it is JD
	   (first prepped-input))
	  ((and (= (list-length prepped-input) 3)
		(numberp (first prepped-input))
		(or (numberp (second prepped-input))
		    (stringp (second prepped-input)))
		(numberp (third prepped-input))
		(valid-day-month-year-list prepped-input))
	   (apply #'utjd prepped-input))
	  ;; Softer error
	  (t  ;;(error "~%dmy-to-time: error in input ~a" input)
	     nil))))



(defun TIME-TO-DMY (time)
  "Given a date specification in internal units, return list
    (day month year)"
  (jdut time))

;;; Merged 7-12-88
;;; examples of input for number-of-days-in-normal-month:
;;;  (number-of-days-in-normal-month 2)  ;; 28
;;;  (number-of-days-in-normal-month 12)  ;; 31

(defun number-of-days-in-normal-month (month)
  (cdr (nth (1- month) *days-in-month*)))

;;; examples of input for valid-day-month-year
;;; (valid-day-month-year-list  '(1 23 89)  ;;; nil
;;; (valid-day-month-year-list  '(23 1 89)  ;;; t

(defun VALID-DAY-MONTH-YEAR-LIST (day-month-year-list)
  "Returns T if the list contains numeric representations
   of day month and year like (23 1 89)"
  (let* ((day (first day-month-year-list))
	(month (second day-month-year-list))
	(month-number (if (numberp month)
			  month
			  (month-string-to-month-number month)))
	(year  (third day-month-year-list)))

    (and (<= month-number 12)
	 (valid-day-for-month-and-year day month-number year))))

(defun VALID-DAY-FOR-MONTH-AND-YEAR (day month year)
  (let ((allowed-days (number-of-days-in-normal-month month)))
    (or  (<= day  allowed-days)
	 (and (is-leap-year year)
	      (eql month 2)
	      (<= day (1+ allowed-days))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;  Convert-JD-in-paired-list-to-TJD
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun Convert-JD-in-paired-list-to-TJD(pair-list)
  "Takes a list comprising <JD x> where x can be anything
   and turns it into a list of tuples <TJD x>.
   JD - Julian Date
   TJD - Truncated Julian date
   
   Caveat: pair-list must comprise pairs, i.e. even number of elements
           and the first element of each pair must be a double float.
  "

  (do* ((new-pair-list nil))
       ;; end condition: all elements exhausted. 
       ;; return the reverse of list
       ((null pair-list) (reverse new-pair-list))

    ;; the new list is in reverse order
    ;; pop the time in JD, get the TJD and push it on to the new list
    (push (JD-to-TJD (pop pair-list)) new-pair-list)
    ;; push the "other" element in the tuple on the new list
    (push (pop pair-list) new-pair-list) 
    
    ))

;;; ----------------------------------------------------------------------
;;;                                         Integer Time Routines
;;;
;;; The following provide for conversion from various external time formats
;;; to a standardized "integer" time measured in ticks.  The conversion
;;; factor is lexically private to these functions and macros.
;;;
;;; Note:  some of the following are macros, not functions, to avoid the
;;; runtime overhead of keywords in arg lists.  This means that all of
;;; the system will have to be recompiled if the conversion factor changes.
;;;
;;; For the present, the conversion factor means that time is measured 
;;; in milliseconds.
;;;
;;; These routines call the above time utilities for conversion.
;;;
;;; - MDJ 26-Jun-1989
;;;
;;; updated 20 Jul 1989: Vax can't use in macro of lexically private var
;;; conversion-factor: changed to defconstant
;;;

;;; the factor is number of ticks per day
;;; e.g. if time tick is millisecond, then conversion-factor is
;;; (* 86400 1000) = (* seconds-per-day tics-per-second)

;;; +++++++++++++++++++++++++++++++++++++++++++++++++++
;;; ABS-TIME CONVERSION FACTOR:
;;; - NOTE: if this is ever changed, you must change
;;;   both the int and float versions!!!!!

(eval-when (compile load eval)
  (defconstant *conversion-factor* 86400000
    "Number of ticks (milliseconds) per day"))

(eval-when (compile load eval)
  (defconstant *conversion-factor-double* 86400000.0d0
    "Number of ticks (milliseconds) per day"))
;;;
;;; +++++++++++++++++++++++++++++++++++++++++++++++++++

(defmacro ABS-TIME (&key day month year (hour 0) (minute 0) (second 0)
		    date jd tjd)
  
  "Convert from various date formats to standard dates (measured in ticks):
     The following formats are allowed:

      (abs-time &key day month year)
      (abs-time &key day month year hour minute second)
       - note: month may be either 3-char month string e.g. 'Mar' or month number 1=Jan, etc.

      (abs-time &key date) - note: date formats are the same as those accepted by dmy-to-time
       - e.g. string or symbol 1-Jan-89, 1-jan-1989, etc.
      (abs-time &key jd)   - full julian date
      (abs-time &key tjd)  - truncated julian date, i.e. (- jd *jd-reference*)

     Output units are integer (possibly bignum) ticks"
  
  (cond ((and day month year (= hour 0) (= minute 0) (= second 0))
	 `(ceiling (* (utjd ,day ,month ,year) ,*conversion-factor*)))
	((and day month year)
	 `(ceiling (* (+ (utjd ,day ,month ,year)
			 (hour-minute-second-to-fractional-day ,hour ,minute ,second))
		      ,*conversion-factor*)))
	(date
	 `(ceiling (* (dmy-to-time ,date) ,*conversion-factor*)))
	(jd
	 `(ceiling (* (- ,jd *jd-reference*) ,*conversion-factor*)))
	(tjd
	 `(ceiling (* ,tjd ,*conversion-factor*)))
	(t (error "unrecognized keyword arguments"))))


;;; Aug  1 1991 MBR
;;; Rewrote.  Old version had accuracy problems, because it first converted to
;;; floating point days, then to ticks.  This version takes pains to avoid
;;; floating point numbers, unless they're passed in via the arglist.  But if
;;; they are passed in, the result may be off a bit bcause of floating point
;;; imprecision.  For example, under Allegro 4.1-alpha,
;;;    (rel-time :days 0.6) -> 51840000
;;; Note that this is a multiple of one second.  But
;;;    (rel-time :days 1.6) -> 138240001
;;; which is not a multiple of one second.
;;;
;;; Aug  7, 1991 AAG Round to nearest tick instead of ceiling
(defun REL-TIME (&key (weeks 0) (days 0) (hours 0) (minutes 0) (seconds 0))
  "Convert from relative time format to standard time (measured in ticks).
The sign of the result is determined by the size of the largest non-zero 
component (i.e weeks if present, else days if present, etc.)"
  (let* ((w (abs weeks))
	 (d (+ (abs days) (* w 7)))
	 (h (+ (abs hours) (* d 24)))
	 (m (+ (abs minutes) (* h 60)))
	 (s (+ (abs seconds) (* m 60)))
	 ;; make sure the sign is an integer
	 (sign (cond ((/= weeks   0) (round (signum weeks)))
		     ((/= days    0) (round (signum days )))
		     ((/= hours   0) (round (signum hours)))
		     ((/= minutes 0) (round (signum minutes)))
		     ((/= seconds 0) (round (signum seconds)))
		     (t +1))))
    
    (* (round (* s 1000))
       sign)))


;;; routines to convert from standard time to JD, TJD, or fractional days
;;; note: these formerly divided by *conversion-factor* (usually forming a
;;; ratio), then were coerced to double. The change below makes it a one
;;; step process - at 4x speedup!

(defun ABS-TIME-TO-TJD (abs-time)
  
  "Convert from standard time (ticks) to truncated julian date"
  
  (/ abs-time *conversion-factor-double*))


(defun ABS-TIME-TO-JD (abs-time)
  
  "Convert from standard time (ticks) to full julian date"
  
  (+ *JD-reference*
     (/ abs-time *conversion-factor-double*)))


(defun REL-TIME-TO-FRACTIONAL-DAY (rel-time)

  "Convert from standard relative time (ticks) to fractional days"
  
  (/ rel-time *conversion-factor-double*))


(defun ROUND-SECONDS-TO-1/1000 (seconds)
  "Rounds a number of seconds to the nearest 1/1000."
  (coerce (/ (round (* seconds 1000)) 1000)
          'double-float))


(defun REL-TIME-TO-SECONDS (rel-time)
  "Convert from standard relative time (ticks) to seconds.
    Return value is rounded to nearest 1/1000 second."
  (if (and rel-time
           (numberp rel-time))
    (let ((seconds (* (rel-time-to-fractional-day rel-time) 3600 24)))
      (round-seconds-to-1/1000 seconds))))


(defun PRETTY-STRING-ABS-TIME (abs-time)
  "Given abs time, return string with time & date formatted"
  (format-abs-time (abs-time-to-tjd abs-time)))


(defun PRETTY-STRING-REL-TIME (rel-time)
  "Given rel time, return string with time formatted."
  ;; Oct  4 1991 mrose.  Print "+INF" and "-INF"
  (cond ((null rel-time) nil)
        ((= rel-time *plus-infinity*) "+INF")
        ((= rel-time *minus-infinity*) "-INF")
        (t (multiple-value-bind (int-days frac-days)
                                (truncate (rel-time-to-fractional-day rel-time))
             (cond ((/= 0 int-days)
                    (format nil "~dd~a" int-days 
                            (format-delta-time (abs frac-days))))
                   (t 
                    (format-delta-time frac-days)))))))


(defun PRETTY-SECONDS (seconds)
  "Represent SECONDS as a string like \"02h30m33.445s\"."
  ;; Oct 10 1991 mrose
  (pretty-string-rel-time (rel-time :seconds (ceiling seconds))))

#| 
     Test cases: these should be moved to a test file

(abs-time :day 11 :month "Mar" :year 1985)
(abs-time :day 11 :month "Mar" :year 1985 :hour 0 :minute 29 :second 0.6)
(abs-time :date "1-Jan-1989")
(abs-time :date '1-jan-89)
(abs-time :jd 2446135.5201458333)
(abs-time :tjd 100)
(pretty-string-abs-time (abs-time :date "1-Jan-1989"))
(pretty-string-abs-time 
 (abs-time :day 11 :month "Mar" :year 1985 :hour 0 :minute 29 :second 0.6))

(rel-time :weeks 1)
(rel-time :days 1)
(pretty-string-rel-time (rel-time :hours 2 :minutes 30 :seconds 33.445))
(pretty-string-rel-time (rel-time :hours -2 :minutes 30 :seconds 33.445))
(pretty-string-rel-time (rel-time :minutes 30 :seconds 33.445))
(pretty-string-rel-time (rel-time :seconds 33.445))
(pretty-string-rel-time (rel-time :days 1))
(pretty-string-rel-time (rel-time :days 2 :hours 3))
(pretty-string-rel-time (rel-time :days -2.6))
(pretty-string-rel-time (rel-time :days +2.6))

(abs-time-to-tjd (abs-time :day 11 :month "Mar" :year 1985))
(abs-time-to-tjd (abs-time :tjd 1001.1234))
(abs-time-to-jd (abs-time :day 11 :month "Mar" :year 1985 
                          :hour 0 :minute 29 :second 0.6))
(rel-time-to-fractional-day 86400000)
(rel-time-to-fractional-day (rel-time :days 1.223334559))
|#


#|
(convert-intervals-integer-to-tjd '((3906.5 4001.5) (4502.5 4503.5)))
|#
(defun CONVERT-INTERVALS-INTEGER-TO-TJD (intervals)
  "Converts time intervals from Integer to TJD.
   If INTERVALS = nil, returns nil."
  (if intervals
      (map-preserving-structure #'abs-time-to-tjd intervals)))

#|
(abs-time-to-tjd (tjd-to-abs-time 3927))
3927.0
|#

(defun TJD-TO-ABS-TIME (tjd)
  "Converts Truncated Julian Date to Absolute Time (in ticks)"
  (ceiling (* tjd *conversion-factor*)))   ; Replacement for ABS-TIME call
  ;;(abs-time :tjd tjd))


(defvar *LENGTH-OF-YEAR* 365.24)


#|
(sogs-time-to-tjd (tjd-to-sogs-time 4500))
4500.0

(tjd-to-sogs-time (sogs-time-to-tjd 331205000))
331204992
--- some roundoff error here ???
|#

(defparameter *SOGS-TIME-START*
	     (/ (tjd-to-abs-time (dmy-to-time '1-jan-1980)) 1000)
  "The number of seconds at time = jan 1 1980")

(defun TJD-TO-SOGS-TIME (tjd)
  "Convert truncated julian date to SOGS time"
  (- (truncate (float (/ (tjd-to-abs-time tjd) 1000))) *SOGS-TIME-START*))


(defun SOGS-TIME-TO-ABS-TIME (sogs-time)
  "Convert sogs time to absolute time"
  (* (+ *SOGS-TIME-START* sogs-time) 1000))

(defun SOGS-TIME-TO-TJD (sogs-time)
  "Convert sogs time to truncated julian date"
  (abs-time-to-tjd (sogs-time-to-abs-time sogs-time)))

;;; ----------------------------------------------------------------------
;;;                                         Time range looping


;;; enhancements, someday:
;;; - check action forms, only bind vars which are actually referenced
;;; - add references to day-of-week, month-string, day-of-year, but only
;;;   if references so don't have to pay compute overhead unless needed
;;; MDJ 11Nov91


(defmacro LOOP-OVER-TIME-IN-RANGE
          (&key TJD-start TJD-end 
                (TJD 'TJD) (day-of-month 'day-of-month) (month 'month)
                (year 'year)
                (hour 'hour) (minute 'minute) (second 'second);; vars bound
                day-action hour-action minute-action second-action)

  "Macro to loop over time in specified range, performing actions at each
    day, hour, minute, or second.  Specified symbols are bound to
    TJD, day-of-month, month (number), year, hour, minuts, and second.
    All arguments are keywords:
     :TJD-start - start of range
     :TJD-end - end of range
    The following are symbols which will be bound as indicated when the actions
    are executed.  Defaults are indicated.
     :TJD 'TJD
     :day-of-month 'day-of-month
     :month 'month
     :hour 'hour
     :minute 'minute
     :second 'second
    The following are forms executed on specified times, with symbols above
    bound:
     :day-action
     :hour-action
     :minute-action
     :second-action"

  `(let ((_jdut nil)
         (_hms nil)
         (_tjd-min nil)
         (_counter nil)
         (,tjd nil)
         (,day-of-month nil)
         (,month nil)
         (,year nil)
         (,hour nil)
         (,minute nil)
         (,second nil)
         )

     (when ',day-action
       (setf ,tjd (- (floor ,TJD-start) 0.5))
       (setf _tjd-min nil _counter 0)
       (loop
         (cond ((> ,tjd ,tjd-end)
                (return nil))
               ((and (>= ,tjd ,tjd-start) (<= ,tjd ,tjd-end))
                (setf _jdut (jdut ,tjd))
                (setf ,year (third _jdut))
                (setf ,month (second _jdut))
                (setf ,day-of-month (floor (first _jdut)))
                (setf _hms (fractional-day-to-hour-minute-second
                            (- (first _jdut) ,day-of-month)))
                (setf ,hour (first _hms))
                (setf ,minute (second _hms))
                (setf ,second (third _hms))
                ,day-action
                (setf ,tjd (+ ,tjd 1)))
               (t (setf ,tjd (+ ,tjd 1))))))

     (when ',hour-action
       (setf _tjd-min (- (floor ,tjd-start) (/ 1.0 24.0))
             _counter 0)
       (loop
         (setf ,tjd (+ _tjd-min (* _counter (/ 1.0 24.0))))
         (cond ((> ,tjd ,tjd-end)
                (return nil))
               ((and (>= ,tjd ,tjd-start) (<= ,tjd ,tjd-end))
                (setf _jdut (jdut ,tjd))
                (setf ,year (third _jdut))
                (setf ,month (second _jdut))
                (setf ,day-of-month (floor (first _jdut)))
                (setf _hms (fractional-day-to-hour-minute-second
                            (- (first _jdut) ,day-of-month)))
                (setf ,hour (first _hms))
                (setf ,minute (second _hms))
                (setf ,second (third _hms))
                ,hour-action
                (incf _counter))
               (t (incf _counter)))))

     (when ',minute-action
       (setf _tjd-min (- (floor ,tjd-start) (/ 1.0 1440.0))
             _counter 0)
       (loop
         (setf ,tjd (+ _tjd-min (* _counter (/ 1.0 1440.0))))
         (cond ((> ,tjd ,tjd-end)
                (return nil))
               ((and (>= ,tjd ,tjd-start) (<= ,tjd ,tjd-end))
                (setf _jdut (jdut ,tjd))
                (setf ,year (third _jdut))
                (setf ,month (second _jdut))
                (setf ,day-of-month (floor (first _jdut)))
                (setf _hms (fractional-day-to-hour-minute-second
                            (- (first _jdut) ,day-of-month)))
                (setf ,hour (first _hms))
                (setf ,minute (second _hms))
                (setf ,second (third _hms))
                ,minute-action
                (incf _counter))
               (t (incf _counter)))))

     (when ',second-action
       (setf _tjd-min (- (floor ,tjd-start) (/ 1.0 86400.0))
             _counter 0)
       (loop
         (setf ,tjd (+ _tjd-min (* _counter (/ 1.0 86400.0))))
         (cond ((> ,tjd ,tjd-end)
                (return nil))
               ((and (>= ,tjd ,tjd-start) (<= ,tjd ,tjd-end))
                (setf _jdut (jdut ,tjd))
                (setf ,year (third _jdut))
                (setf ,month (second _jdut))
                (setf ,day-of-month (floor (first _jdut)))
                (setf _hms (fractional-day-to-hour-minute-second
                            (- (first _jdut) ,day-of-month)))
                (setf ,hour (first _hms))
                (setf ,minute (second _hms))
                (setf ,second (third _hms))
                ,second-action
                (incf _counter))
               (t (incf _counter)))))


     ))

#| Test:

(jdut 4000.0) => (18.5 4 1990)

(loop-over-time-in-range
 :TJD-start 3999.0 :TJD-end 4003.0
 :day-action 
 (progn (format t "~%TJD ~,4f ~f/~f/~f ~a ~a" 
                TJD month day-of-month year
                (day-of-week-string TJD)
                (day-of-week-string (+ 1.0e-5 TJD)))))

(loop-over-time-in-range
 :TJD-start 4000.0 :TJD-end 4002.0
 :day-action 
 (progn 
   (format t "~%TJD ~,4f year ~a month ~a day-of-month ~a hour ~a minute ~a second ~a"
           TJD year month day-of-month hour minute second)))

(loop-over-time-in-range
 :TJD-start 4000.0 :TJD-end 4002.0
 :hour-action 
 (progn 
   (format t "~%TJD ~,4f year ~a month ~a day-of-month ~a hour ~a minute ~a second ~a"
           TJD year month day-of-month hour minute second)))
(loop-over-time-in-range
 :TJD-start 4000.0 :TJD-end 4000.01
 :minute-action 
 (progn 
   (format t "~%TJD ~,4f year ~a month ~a day-of-month ~a hour ~a minute ~a second ~a"
           TJD year month day-of-month hour minute second)))
(loop-over-time-in-range
 :TJD-start 4000.0 :TJD-end 4000.0001
 :second-action 
 (progn 
   (format t "~%TJD ~a year ~a month ~a day-of-month ~a hour ~a minute ~a second ~a"
           TJD year month day-of-month hour minute second)))
|#


(defmacro LOOP-THROUGH-SUBINTERVALS (t1 t2 start end steps &rest forms)

  "loop over interval from start to end.  t1 and t2 are symbols 
    bound to start and end of subintervals.  start and end are the
    upper and lower limits of the interval.  steps is the number of
    subintervals.  Forms are executed once for each subinterval.
    Example:
     (loop-through-subintervals
       time1 time2 10 15 5
       (print time1)) => prints the integers 10 11 ... 14"

  (let ((i (gentemp "I"))
        (gstart (gentemp "START"))
        (gend   (gentemp "END"))
        (gdelta (gentemp "DELTA"))
        (gsteps (gentemp "STEPS"))
        )
    `(let* (,t1 ,t2 (,gstart ,start) (,gend ,end) (,gsteps ,steps)
                (,gdelta (/ (- ,gend ,gstart) ,gsteps)))
       (dotimes (,i ,gsteps)
         (setf ,t1 (+ ,gstart (* ,i ,gdelta)))
         (setf ,t2 (+ ,t1 ,gdelta))
         ,@forms))))


(defmacro LOOP-THROUGH-INTERVAL (x start end steps &rest forms)

  "loop over interval from start to end.  x is a symbol
    bound to evenly spaced values within interval.  start and end are the
    upper and lower limits of the interval.  steps is the number of
    subintervals into which start-end is divided.  
    Forms are executed once for each subinterval.
    Example:
     (loop-through-interval
       x 10 15 5
       (print x)) => prints the integers 10 11 ... 15"

  (let ((i (gentemp "I"))
        (gstart (gentemp "START"))
        (gend   (gentemp "END"))
        (gdelta (gentemp "DELTA"))
        (gsteps (gentemp "STEPS"))
        )
    `(let* (,x (,gstart ,start) (,gend ,end) (,gsteps ,steps)
                (,gdelta (/ (- ,gend ,gstart) ,gsteps)))
       (dotimes (,i (1+ ,gsteps))
         (setf ,x (+ ,gstart (* ,i ,gdelta)))
         ,@forms))))

#|
(loop-through-interval 
 t1 2 10 3 
 (format t "~%~f" t1))
|#




;;; ----------------------------------------------------------------------
;;;                                         Range manipulation


(defun INVERT-RANGE-LIST (range-list max)

  "input is a list of ranges '((r1 r2)(r3 r4)...)
    s.t. all values are between 0 and (max-1), 
    and the starts of the ranges are in ascending order.
    The end of one range and the start of the next may overlap.
    Return list of ranges NOT in the input list
    e.g. (invert-range-list '((0 1)(5 10)(18 18)) 20)
            => ((2 4) (11 17) (19 19))
         (invert-range-list nil 20) => ((0 19))"

  (cond 
   ((null range-list)
    (list (list 0 (1- max))))
   (t
    (let ((result nil)
          (current nil)
          (last-range nil)
          )
      (unless (= 0 (first (first range-list)))
        (push (list 0 (1- (first (first range-list)))) result))
      (setf current (first range-list))
      (setf last-range current)
      (dolist (next (rest range-list))
        (let ((start (1+ (second current)))
              (end (1- (first next))))
          (if (>= end start) (push (list start end) result)))
        (setf current next)
        (setf last-range next))
      (when (and last-range (< (second last-range) (1- max) ))
        (push (list (1+ (second last-range)) (1- max)) result))
      (nreverse result)))))

#| test cases
(invert-range-list nil 20)
(invert-range-list '((0 19)) 20)
(invert-range-list '((0 12)) 20)
(invert-range-list '((10 19)) 20)
(invert-range-list '((0 5)(10 19)) 20)
(invert-range-list '((0 5)(10 19)) 20)
(invert-range-list '((1 5)(10 18)) 20)
(invert-range-list '((0 1)(5 10)(18 19)) 20)
(invert-range-list '((1 1)(5 10)(18 19)) 20)
(invert-range-list '((0 1)(5 10)(18 18)) 20)
(invert-range-list '((1 1)(5 10)(18 18)) 20)
(invert-range-list '((0 5)(5 19)(18 18)) 20)
|#

;;; ----------------------------------------------------------------------
;;;                                         Lists of intervals

;;; see also interval list conversions below in PCF section


(defun SUMMED-INTERVAL-DURATION (interval-list)
  
  "given a list of intervals ((t1 t2 [val1])...)
    return the total time in all the intervals
    Example:  (summed-interval-duration '((1 2 0)(5 10 2))) => 6"
  
  (let ((total 0))
    (dolist (i interval-list)
      (incf total (- (second i) (first i))))
    total))


;;; ----------------------------------------------------------------------
;;;                                         Rationalizing numbers etc.

;; DON'T use rationalize below: causes explorer to go into infinite (?)
;; loop 4/29/90 MDJ

(defun RATIONALIZE-NUMBER-OR-INFINITY (item)
  
  "if the input item is a float, return the symbols *minus-infinity* or 
    *plus-infinity* if the number is equal to one of these, 
    otherwise the rationalized form. All other types are returned unchanged."
  ;;(rationalize-number-or-infinity t)
  ;;(rationalize-number-or-infinity nil)
  ;;(rationalize-number-or-infinity 1)
  ;;(rationalize-number-or-infinity 1.0)
  ;;(rationalize-number-or-infinity 1.00002)
  ;;(rationalize-number-or-infinity (sqrt 2.0))
  ;;(rationalize-number-or-infinity *minus-infinity*)
  ;;(rationalize-number-or-infinity *plus-infinity*)
  ;;(rationalize-number-or-infinity '(a b c))
  
  (cond ((typep item 'float)
         (cond ((= item *minus-infinity*) '*minus-infinity*)
               ((= item *plus-infinity*) '*plus-infinity*)
               (t (rational item))))
        (t item)))


;; THIS VERSION SEEMED TO HANG ON THE EXPLORER:  MICROCODE LOOP? BEWARE
;; (defun RATIONALIZE-NUMBERS-IN-NESTED-LIST (nlist)
;;   "convert numbers in nested list into rationals e.g.:
;;     input: (1 2.3 4.55555067 .5 .25 .18443 (1.223 9))
;;     output: (1 23/10 215811054/47373209 1/2 1/4 18443/100000 (1223/1000 9))
;;    convert the numbers *minus-infinity* and *plus-infinity* into the
;;    symbols."
;;
;;  (cond ((null nlist) nil)
;;	((listp nlist) 
;;	 (if (numberp (first nlist))
;;	     (cond ((= *minus-infinity* (first nlist))
;;		    (cons '*minus-infinity*
;;			  (rationalize-numbers-in-nested-list (rest nlist))))
;;		   ((= *plus-infinity* (first nlist))
;;		    (cons '*plus-infinity*
;;			  (rationalize-numbers-in-nested-list (rest nlist))))
;;		   (t (cons (rationalize (first nlist))
;;			    (rationalize-numbers-in-nested-list (rest nlist)))))
;;	     (cons (rationalize-numbers-in-nested-list (first nlist))
;;		   (rationalize-numbers-in-nested-list (rest nlist)))))
;;	(t nlist))
;;
;;  ;; this gets rid of recursion but only goes one level deep in list
;;  (mapcar #'(lambda (x)
;;	      (if (listp x)
;;		  (mapcar #'rationalize-number-or-infinity x)
;;		  (rationalize-number-or-infinity x)))
;;	  nlist)
;;  )


(defun RATIONALIZE-NUMBERS-IN-NESTED-LIST (nlist)
  
  "convert numbers in nested list into rationals e.g.:
   input: (1 2.3 4.55555067 .5 .25 .18443 (1.223 9))
   output: (1 23/10 215811054/47373209 1/2 1/4 18443/100000 (1223/1000 9))
   convert the numbers *minus-infinity* and *plus-infinity* into the
   symbols."
  ;;(rationalize-numbers-in-nested-list '(1 2 3))
  ;;(rationalize-numbers-in-nested-list (cons *minus-infinity* '(1 2.21 3)))
  ;;(rationalize-numbers-in-nested-list (cons *plus-infinity* '(1 2.21 3)))
  ;;(rationalize-numbers-in-nested-list (list *minus-infinity* *plus-infinity*))
  ;;(rationalize-numbers-in-nested-list '(1 (2 3) 4 t nil (nil 1 t) nil))
  ;;(rationalize-numbers-in-nested-list '(t))
  ;;(rationalize-numbers-in-nested-list '(1 2.3 4.55555067 .5 (1 2.3 4.55555067 .5 .25 .18443) .25 .18443))
  ;;(rationalize-numbers-in-nested-list '(1 2.3 4.55555067 .5 .25 .18443 (1.223 9)))
  
  (map-preserving-structure #'rationalize-number-or-infinity nlist))


;;; ----------------------------------------------------------------------
;;;                                         Piecewise-Constant Functions
;;;                                         (PCF) routines
;;;

#|----- Representation:

PCFs are lists of start-time values (t1 v1 t2 v2 ... t(i) v(i) ...)
e.g. '(1 2 9 10) is a PCF with values:
  domain: 1 to 9; value 2
  domain: 9 to *PLUS-INFINITY*; value 10

Constraints:
o  t values must be non-decreasing
o  there must be at least one t-v pair
o  "spike" are presently allowed, e.g.
   '(0 0 5 1 5 0) represents a PCF with a value of 1 at exactly 5,
   but zero everywhere else.  Spikes are indicated by t(i) = t(i+1)

Notes:
- All routines here are non-destructive:  they return a new PCF
- NOT included:  deferred evaluation, or any record of what 
  limits have been calculated
  one possibility:  allow segment values to be nil (or 'unknown)
  instead of numbers to indicate that value is not known

-----|#


(defvar  *DEFAULT-PCF-VALUE* )
(setf  *DEFAULT-PCF-VALUE* 1)

;;; special-purpose macros for use with PCF routines

;;; Note:  on the TI, (max a b) coerces the returned value to the
;;; most restrictive type of the arguments, e.g. (max 1.0 2) => 2.0
;;; Same with min.  Thus they are not used below, instead macros
;;; are defined to return whichever value is larger

(defmacro max-of-two-values (value1 value2)
  "Return larger of the two input values"
  `(if (> ,value1 ,value2) ,value1 ,value2))

(defmacro min-of-two-values (value1 value2)
  "Return smaller of the two input values"
  `(if (< ,value1 ,value2) ,value1 ,value2))


(defmacro PCF-loop-over-intervals (PCF start end value remaining-PCF done-flag
				   return-forms &rest body)
  "Generate a loop over each interval of a PCF.
    'PCF, 'start, 'end, 'value, 'remaining-PCF, and 'done-flag are SYMBOLS which are
    substituted into a DO* loop.  'return-forms is a LIST of forms, the last of
    which is the value returned from the loop (if it is exited normally).
    Normal exit occurs when (1) there are no more intervals, or
    (2) when done-flag is non-nil.
    Each form of 'body is included in the loop."
  `(do* ((,remaining-PCF ,PCF (cddr ,remaining-PCF))
	 (,start (first ,remaining-PCF) (first ,remaining-PCF))
	 (,value (second ,remaining-PCF) (second ,remaining-PCF))
	 (,end (if (third ,remaining-PCF) (third ,remaining-PCF) *PLUS-INFINITY*)
	  (if (third ,remaining-PCF) (third ,remaining-PCF) *PLUS-INFINITY*))
	 (,done-flag nil))
	((or ,done-flag (null ,remaining-PCF))
	 ,@return-forms)
     ,@body))

#|-----  Test case for PCF-loop-over-intervals:
    Place the cursor on the opening paren and ctrl-shift-M to see expansion.
(pcf-loop-over-intervals PCF t1 t2 val left-to-do done
			 ((nreverse r))
			 (format t "body-1")
			 (format t "body-2"))
-----|#


;;; special-purpose macro for building up PCFs in time order

#|----- Constructing PCFs in time order

It is very common in the algorithms below to build up a PCF in reverse
order, then reverse the list at the end to get the result.  In this case
one is calculating interval values for increasingly larger time values,
and pushing them onto the front of a list.  The following macro provides
a general and central way to do this:

   (add-interval-to-reversed-PCF-list result t v)

Here result, t, and v are symbols bound to the reverse PCF, time, and
values respectively.  result must be a list, t and v must be numbers.
The macro destructively changes result.

For example: let tn and vn be the most recent values in the PCF, 
i.e. result is of the form '(vn tn vn-1 tn-1 ...)
and it is necessary to add a new value v at time t, as in the following
diagram:

      vn *- - - 
            *--- v
    -----(
             
    -----+--+---->
        tn  t

There are the following cases:
(1)  result is null
     this is the first interval in the PCF.
     action:  push t and v on result
(2)  t > tn and v = vn
     current value is the same as immediately preceding value
     no action
(3)  t > tn and v /= vn
     the value changes at tn
     action:  push t and v on result
(4)  t = tn and v = vn
     see case (2) above (no action)
(5)  t = tn and v /= vn:
     This breaks up into subcases depending on whether vn-1, tn-1 exist
     and their values, and whether spikes are allowed.  All cases are
     described here, but the macro currently implements only the 
     case where spikes are allowed.
  (5a) vn-1 does not exist, i.e. result is (vn tn)
       action:  push t and v on result [spikes]
                (setf (car result) v) [no spikes]
  (5b) vn-1 exists and tn-1 < tn and v = vn-1
       action:  push t and v on result [spikes]
                pop vn and tn from result [no spikes]
  (5c) vn-1 exists and tn-1 < tn and v /= vn-1
       action:  push t and v on result [spikes]
                (setf (car result) v) [no spikes]
  -- next 2 cases only arise if spike are allowed --
  (5d) vn-1 exists and tn-1 = tn and v = vn-1
       action:  pop vn and tn from result [spikes]
  (5e) vn-1 exists and tn-1 = tn and v /= vn-1
       action:  (setf (car result) v)

-----|#
	      
(defmacro add-interval-to-reversed-PCF-list (result tt v)
  `(cond ((null ,result)			; (1)
	  (push ,tt ,result)
	  (push ,v ,result))
	 ((> ,tt (second ,result))
	  (when (/= ,v (first ,result))		; (3)
	    (push ,tt ,result)
	    (push ,v ,result)))
	 (t					; (= tt (second result))
	  (when (/= ,v (first ,result))
	    (cond ((null (third ,result))	; (5a)
		   (push ,tt ,result)
		   (push ,v ,result))
		  ((< (fourth ,result) (second ,result))	; (5b,c)
		   (push ,tt ,result)
		   (push ,v ,result))
		  (t (cond ((= ,v (third ,result))	; (5d)
			    (pop ,result)
			    (pop ,result))
			   (t			; (5e)
			    (setf (first ,result) ,v)))))))))


(defun PCF-create (value)
  "Create a PCF with specified constant value 
    from *MINUS-INFINITY* to *PLUS-INFINITY*"
  (list *MINUS-INFINITY* value))


;;; bug fix 8 March 89 MDJ:  
;;; this didn't handle *plus-infinity* or *minus-infinity*
;;; as start or end times properly

(defun PCF-create-with-single-interval (start end value-in value-out)
  "Create a PCF with a value of value-in from start to end, and
    value-out otherwise"
  (cond ((= value-in value-out)
	 (PCF-create value-out))
	((< end start)
	 (PCF-create value-out))
        ((and (= start *MINUS-INFINITY*)
              (= end *PLUS-INFINITY*))
         (list start value-in))
        ((= end *PLUS-INFINITY*)
         (list *MINUS-INFINITY* value-out start value-in))
        ((= start *MINUS-INFINITY*)
         (list start value-in end value-out))
	(t
	 (list *MINUS-INFINITY* value-out start value-in end value-out))))

;;;(PCF-create-with-single-interval *minus-infinity* 10 1 0)
;;;(PCF-create-with-single-interval *minus-infinity* *plus-infinity* 1 0)
;;;(PCF-create-with-single-interval 1.5 2.5 0 1)
;;;(PCF-create-with-single-interval 1.5 2.5 1 1)
;;;(PCF-create-with-single-interval 1.5 1.4 1 0)


(defun PCF-copy (pcf)
  "Return a copy of the input PCF"
  (copy-list pcf))


;;; two special PCFs:

(defvar *ZERO-PCF* (PCF-create 0))
(defvar *UNITY-PCF* (PCF-create 1))

;;; Added  SDV  1/23/89
(defun PCF-zero-p(p)
  "t iff p is a *zero-pcf*"
  (equal p  *zero-pcf*))

;;; Added  SDV  1/23/89
(defun PCF-unity-p  (p)
  "t iff p is a *unity-pcf*"
  (equal p *unity-pcf*))

(defun PCF-set-value (new-start new-end new-value pcf)
  "Set the PCF to have the specified value between start and end.
    non-destructive:  returns a new PCF"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      
      (cond ((and (>= new-start start) 
		  (<= new-end end))
	     ;; new interval is wholly contained in this interval
	     (when (> new-start start)
	       (add-interval-to-reversed-PCF-list result start value))
	     (add-interval-to-reversed-PCF-list result new-start new-value)
	     (when (< new-end end)
	       (add-interval-to-reversed-PCF-list result new-end value))
	     )
	    ((and (< new-start start)
		  (> new-end end))
	     ;; old interval wholly contained in new interval
	     )
	    ((and (>= new-start start)
		  (> new-end end)
		  (< new-start end))
	     ;; new segment starts in this interval but doesn't end
	     (when (> new-start start)
	       (add-interval-to-reversed-PCF-list result start value))
	     (add-interval-to-reversed-PCF-list result new-start new-value)
	     )
	    ((and (< new-start start)
		  (<= new-end end)
		  (> new-end start))
	     ;; segment ends in this interval but didn't start here
	     (add-interval-to-reversed-PCF-list result new-end value)
	     )
	    (t ;; no change
	     (add-interval-to-reversed-PCF-list result start value)
	     )))))


(defun PCF-get-value (time pcf)
  "Return the value of the PCF at the specified time.
    If time is less than the first time of the PCF then nil is returned"
  (let (result)
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      (when (or (and (>= time start)
		     (< time end))
		(= time start end))
	(setf result value done t)))))


(defun PCF-transform (pcf transformer-function)
  "Apply a transformer function to value of each interval in PCF.
    The transformer function must take a single numeric arg and
    return a number.  Return new PCF with transformed values"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      ;; transform each interval in turn
      (add-interval-to-reversed-PCF-list
	result start (funcall transformer-function value)))))


;; new function added 28-Oct-88 MDJ
;; utility to shift the time origin of a PCF
;; (print (pcf-shift-time '(0 0 1 1 5 0) 10))
;; (print (pcf-shift-time (pcf-create-with-single-interval 5 10 1 0) 0))
;; (print (pcf-shift-time (pcf-create-with-single-interval 5 10 1 0) -1))
;; (print (pcf-shift-time (pcf-create-with-single-interval 5 10 1 0) 1))

(defun PCF-SHIFT-TIME (pcf time)
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      (add-interval-to-reversed-PCF-list
	result (if (= start *minus-infinity*)
		   *minus-infinity*
		   (- start time))
	value))))


(defun SHIFT-INTERVAL-LIST (interval-list offset)

  "Shift an interval list of the form ((t1 t2 val) (t3 t4 val) ...)
    or ((t1 t2) (t3 t4) ...) by the specified time, i.e. the times are 
    translated from t to t-offset.  Non-destructive, i.e. a new interval 
    list is returned. The shift direction is the same as PCF-shift-time"

  (let ((result nil))
    (dolist (i interval-list)
      (let ((a (first i))
            (b (second i))
            (c (third i)))
        (push (if c
                (list (- a offset) (- b offset) c)
                (list (- a offset) (- b offset)))
              result)))
    (nreverse result)))
#|
(shift-interval-list '((5 10)) 1)
(shift-interval-list '((5 10) (20 30)) 1)
(shift-interval-list '((5 10 1) (20 30 1.1)) 1)
(shift-interval-list '((5 10 1) (20 30 1.1)) -1)
|#

(defun PCF-TRANSFORM-TIME (pcf time-modifier-fcn)
   "Apply the time-modifier-fcn of one arg to all times in the  
    input PCF"
  
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      (add-interval-to-reversed-PCF-list
	result (if (= start *minus-infinity*)
		   *minus-infinity*
		   (funcall time-modifier-fcn start))
	value))))


(defun TRANSFORM-TIME-IN-INTERVAL-LIST (interval-list time-modifier-fcn)
  "Apply the time-modifier-fcn of one arg to each time in an interval
    list ((t1 t2) (t3 t4)...) or ((t1 t2 val) (t3 t4 val)...)"
  (let ((result nil))
    (dolist (i interval-list)
      (let ((a (first i))
            (b (second i))
            (c (third i)))
        (push (if c
                (list (funcall time-modifier-fcn a)
		      (funcall time-modifier-fcn b)
		      c)
                (list (funcall time-modifier-fcn a)
		      (funcall time-modifier-fcn b)))
              result)))
    (nreverse result)))


(defun PCF-scalar-multiply (pcf scalar)
  "Multiply a PCF times a scalar"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      ;; transform each interval in turn
      (add-interval-to-reversed-PCF-list
	result start (* value scalar)))))


(defun PCF-normalize (pcf normalization-factor)
  "Normalize a PCF by the input factor, i.e.
    multiply by 1/normalization-factor.  If the factor
    is zero, a zero pcf is returned"
  (cond ((zerop normalization-factor)
	 *zero-PCF*)
	(t (pcf-scalar-multiply pcf (/ 1 normalization-factor)))))


(defun PCF-round (pcf round-to)
  "Round values in PCF to specified precision.
    E.g. round-to of 0.1 will round to nearest 0.1 in precision"
  (pcf-transform pcf
		 #'(lambda (x) (* (round x round-to) round-to))))


(defun PCF-min (pcf &optional 
		(range-start *MINUS-INFINITY*)
		(range-end *PLUS-INFINITY*))
  "Find the minimum value of a PCF, optionally in a specified range
    (only intervals with nonzero overlap with the range are considered)"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(if (null result)
	    (setf result value)
	    (setf result (min-of-two-values result value)))))))


(defun PCF-max (pcf &optional 
		(range-start *MINUS-INFINITY*)
		(range-end *PLUS-INFINITY*))
  "Find the maximum value of a PCF, optionally in a specified range
    (only intervals with nonzero overlap with the range are considered)"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(if (null result)
	    (setf result value)
	    (setf result (max-of-two-values result value)))))))


(defun PCF-integral (pcf &optional 
                         (range-start *MINUS-INFINITY*)
                         (range-end *PLUS-INFINITY*))
  "Find the integral value of a PCF, optionally in a specified range
    (only intervals with nonzero overlap with the range are considered)"
  (let ((result 0)
	term)
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)

       (unless (or (<= end range-start)
                   (>= start range-end)
                   (zerop value))

               (setf term
                     (* value 
                        (cond ((and (<= start range-start) (<= end range-end))
                               (- end range-start))
                              ((and (<= start range-start) (>= end range-end))
                               (- range-end range-start))
                              ((and (>= start range-start) (<= end range-end))
                               (- end start))
                              ((and (>= start range-start) (>= end range-end))
                               (- range-end start))
                              (t (format t "~%ERROR in PCF-integrate:")
                                 (format t "~%impossible overlap conditions")
                                 0))))
               (if (null result)
                   (setf result term)
                   (setf result (+ result term)))))))


(defun mean-PCF-in-range (pcf start end)
  "Average a PCF over a specified range."
  (coerce (/ (PCF-integral pcf start end)
	     (- end start))
	  'float))


(defun PCF-combine (f1 f2 combiner-function)
  "Combine two PCFs 'f1 and 'f2 with the funtion #'combiner-function.
    The combiner function should take two numeric arguments and
    return their combined value."
  (let ((result nil))
    (do* ((f1-v (second f1))
	  (f2-v (second f2))
	  (f1-next-t (third f1))
	  (f2-next-t (third f2))
	  (current-t (max-of-two-values (first f1) (first f2)))
	  (current-v (funcall combiner-function f1-v f2-v))
	  (done nil)
	  )
	 (done
	   (nreverse result))
      
      ;; put current interval and value in the result list
      (add-interval-to-reversed-PCF-list result current-t current-v)
      
      (cond
	((and (null f1-next-t) (null f2-next-t))
	 ;;(format t "~%done:  f1-next-t and f2-next-t are both null.")
	 (setf done t))
	((or (null f1-next-t)
	     (and (not (null f2-next-t))
		  (> f1-next-t f2-next-t)))
	 ;;(format t "~%advancing along f2 ")
	 (setf current-t f2-next-t)
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf current-v (funcall combiner-function f1-v f2-v))          
	 )
	((or (null f2-next-t)
	     (< f1-next-t f2-next-t))
	 ;;(format t "~%advancing along f1 ")
	 (setf current-t f1-next-t)
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (funcall combiner-function f1-v f2-v))
	 )
	((= f1-next-t f2-next-t)
	 ;;(format t "~%advancing along f1 & f2 ")
	 (setf current-t f2-next-t)		; = f1-next-t
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (funcall combiner-function f1-v f2-v))
	 )
	(t (format t "~%ERROR! PCF-combine dropped through cond")
	   (setf result nil)
	   (setf done t)
	   )))))

;;; the following is a specialization of PCF-combine which is a little
;;; smarter about zero and one.  First a macro to do the actual combination:

;;; pcf multiply will not let a non-zero value get lower than approximately-zero

(defvar *APPROXIMATELY-ZERO* 0.00000000001)


(defmacro multiply-and-mark-difference (a b marker)
  `(cond ((= ,b 0) 0)
	 ((= ,a 0) 
	  (setf ,marker t)
	  0)
	 ((= ,a 1) ,b)
	 ((= ,b 1) 
	  (setf ,marker t)
	  ,a)
	((<= ,b *approximately-zero*)
	 (setf ,marker t)
	 *approximately-zero*)
	((<= ,a *approximately-zero*)
	 (setf ,marker t)
	 *approximately-zero*)
	(t
	 (setf ,marker t)
	 (* ,a ,b))))

;;; new version of PCF-multiply:  this version first checks to see if 
;;; the result will be equal to f2 in which case f2 is returned without
;;; consing up a new product PCF.  If not, the old version is called
;;; to construct the product.

(defvar *PCF-multiply-counter* 0)
(defvar *PCF-multiply-with-cons-counter* 0)
(defun init-PCF-multiply-counters ()
  (setf *PCF-multiply-counter* 0)
  (setf *PCF-multiply-with-cons-counter* 0))
(defun show-PCF-multiply-counters ()
  (format t "~%~d calls to PCF-multiply, ~d required cons"
	  *PCF-multiply-counter*
	  *PCF-multiply-with-cons-counter*))


(defun PCF-multiply (f1 f2)
  (incf *PCF-multiply-counter*)
  (cond ((PCF-multiply-try-nocons f1 f2)
	 (values f2 nil))
	(t (incf *PCF-multiply-with-cons-counter*)
	   (PCF-multiply-with-cons f1 f2))))


(defun PCF-multiply-try-nocons (f1 f2)
  "Multiply two PCFs 'f1 and 'f2.
    This version returns nil as soon as the first difference is
    detected, otherwise it just returns t (in which case the
    product is f2)"
  (let ((f2-changed nil))
    (do* ((f1-v (second f1))
	  (f2-v (second f2))
	  (f1-next-t (third f1))
	  (f2-next-t (third f2))
	  (current-t (max-of-two-values (first f1) (first f2)))
	  (current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	  (done nil)
	  )
	 (done
	   (if f2-changed nil t))
            
      (cond
	((and (null f1-next-t) (null f2-next-t))
	 ;;(format t "~%done:  f1-next-t and f2-next-t are both null.")
	 (setf done t))
	((or (null f1-next-t)
	     (and (not (null f2-next-t))
		  (> f1-next-t f2-next-t)))
	 ;;(format t "~%advancing along f2 ")
	 (setf current-t f2-next-t)
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	 (if f2-changed (return-from PCF-multiply-try-nocons nil))
	 )
	((or (null f2-next-t)
	     (< f1-next-t f2-next-t))
	 ;;(format t "~%advancing along f1 ")
	 (setf current-t f1-next-t)
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	 (if f2-changed (return-from PCF-multiply-try-nocons nil))
	 )
	((= f1-next-t f2-next-t)
	 ;;(format t "~%advancing along f1 & f2 ")
	 (setf current-t f2-next-t)		; = f1-next-t
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	 (if f2-changed (return-from PCF-multiply-try-nocons nil))
	 )
	(t (format t "~%ERROR! Dropped through cond")
	   (setf done t)
	   )))))


;;; this is identically the original PCF-multiply:  it always conses up
;;; the product PCF

(defun PCF-multiply-with-cons (f1 f2)
  "Multiply two PCFs 'f1 and 'f2.
    Returns two values:  the product PCF, and t if the product is
    different from f2"
  (let ((result nil)
	(f2-changed nil))
    (do* ((f1-v (second f1))
	  (f2-v (second f2))
	  (f1-next-t (third f1))
	  (f2-next-t (third f2))
	  (current-t (max-of-two-values (first f1) (first f2)))
	  (current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	  (done nil)
	  )
	 (done
	   (values (nreverse result) f2-changed))
      
      ;; put current interval and value in the result list
      (add-interval-to-reversed-PCF-list result current-t current-v)
      
      (cond
	((and (null f1-next-t) (null f2-next-t))
	 ;;(format t "~%done:  f1-next-t and f2-next-t are both null.")
	 (setf done t))
	((or (null f1-next-t)
	     (and (not (null f2-next-t))
		  (> f1-next-t f2-next-t)))
	 ;;(format t "~%advancing along f2 ")
	 (setf current-t f2-next-t)
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))          
	 )
	((or (null f2-next-t)
	     (< f1-next-t f2-next-t))
	 ;;(format t "~%advancing along f1 ")
 	 (setf current-t f1-next-t)
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	 )
	((= f1-next-t f2-next-t)
	 ;;(format t "~%advancing along f1 & f2 ")
	 (setf current-t f2-next-t)		; = f1-next-t
	 (setf f2 (cddr f2))
	 (setf f2-v (second f2))
	 (setf f2-next-t (third f2))
	 (setf f1 (cddr f1))
	 (setf f1-v (second f1))
	 (setf f1-next-t (third f1))
	 (setf current-v (multiply-and-mark-difference f1-v f2-v f2-changed))
	 )
	(t (format t "~%ERROR! PCF-multiply dropped through cond")
	   (setf result nil)
	   (setf done t)
	   )))))

;;; Never fix something that aint broken SDV
;;; Resurrected 6/27/89

(defun PCF-non-zero-intervals (pcf &optional 
			       (range-start *MINUS-INFINITY*)
			       (range-end *PLUS-INFINITY*))
  "Returns a list of (start end) times when the input PCF is non-zero"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))

    (unless (or (<= end range-start)
		(>= start range-end))
      (when (not (zerop value))
	(if (null result)
	    ;; start the output list
	    (push (list (max-of-two-values start range-start)
			(min-of-two-values end range-end))
		  result)
	    ;; add to output list
	    (let ((tmin (max-of-two-values start range-start))
		  (tmax (min-of-two-values end range-end))
		  (old-tmax (second (first result))))
	      ;; see whether the new interval can be merged with the previous one
	      (if (> tmin old-tmax)
		  ;; add new nonzero interval to the result
		  (push (list tmin tmax) result)
		  ;; modify the end time of the most recent interval addede
		  (setf (second (first result)) tmax)))))))))


(defun PCF-longest-non-zero-interval (pcf)
  "Returns the longest non-zero interval in a pcf"
  (let ((value 0)
        (intervals (pcf-non-zero-intervals pcf))
        (current-value 0))
    (dolist (current intervals)
      (setf current-value (- (cadr current) (car current)))
      (cond ((> current-value value)
             (setf value current-value))))
    value))



;;; Changed function 1/23/89 Newer faster algo.
;;; doesn't check ranges unless they are less they are really restrictive
;;; (i.e not  *MINUS-INFINITY*,  *PLUS-INFINITY*). Also cheaper in terms of
;;; consing because it does not cons up intervals that will have to be thrown away.
;;; changed 8 Mar 89 MDJ to make names PRIVATE!

;;; Added  SDV  1/13/89  
#|
(defun PCF-non-zero-intervals (pcf &optional 
			       (range-start *MINUS-INFINITY*)
			       (range-end *PLUS-INFINITY*))
  "Returns a list of (start end) times when the input PCF is non-zero"
    (unless (PCF-zero-p pcf)
      (let ((intervals 	(PCF-aux-non-zero-intervals pcf)))
	(if (or (not (eql  range-start  *MINUS-INFINITY*))
		(not (eql range-end    *PLUS-INFINITY*)))
	    (PCF-aux-clip-start-and-end-intervals intervals range-start range-end)
	    intervals))))
|#

(defun PCF-clip-start-and-end-intervals (intervals start end)
  "Synonym for pcf-aux-clip-start-and-end-intervals function"
  (PCF-aux-clip-start-and-end-intervals intervals start end))

(defun PCF-aux-clip-start-and-end-intervals (intervals start end)
  "takes a set of intervals and clips the start intervals such that no
   intervals are returned prior to the start and proceeding the end (that is 
   these intervals are clipped away. Used as an aux. function to PCF-non-zero-intervals "

  (PCF-aux-clip-end-interval (PCF-aux-clip-start-interval intervals start) end))

(defun PCF-aux-clip-start-interval (intervals start)
  "takes away the intervals before the start, used as aux. function of
   PCF-aux-clip-start-and-end-intervals "
  (if (<= start (first (first intervals)))
      intervals
      (PCF-aux-aux-clip-start-interval  intervals start nil)))

(defun PCF-aux-aux-clip-start-interval (intervals start old-end)
  (let* ((first-interval (first intervals))
	 (interval-start (first first-interval))
	 (interval-end (second first-interval)))
    ;;(format t "~%I-S: ~s  I-E: ~s  O-E: ~s S: ~s" interval-start interval-end old-end start)
    (cond ((null first-interval)
	   nil)
	  ((and old-end 
		(<= old-end start interval-start))
	   intervals)
	  ((<= interval-start start interval-end)
	   (cons (list start interval-end) (rest intervals)))
	  (t
	   (PCF-aux-aux-clip-start-interval (rest intervals) start interval-end)))))

(defun PCF-aux-clip-end-interval (intervals end)
  "takes away the intervals after the end, used as an aux. function for
   PCF-aux-clip-start-and-end-intervals."
  (when intervals
    (let ((reversed-intervals (reverse intervals)))
      (if (>= end (second (first reversed-intervals)))
	  intervals
	  (nreverse (PCF-aux-aux-clip-end-interval reversed-intervals end nil))))))

(defun PCF-aux-aux-clip-end-interval (intervals end old-start)
  "aux function for PCF-aux-clip-end-interval, aux. funct. for PCF-aux-clip-end-interval."
  (let* ((first-interval (first intervals))
	 (interval-start (first first-interval))
	 (interval-end (second first-interval)))
    ;(format t "~%I-S: ~s  I-E: ~s  E: ~s OS: ~s" interval-start interval-end end  old-start)
    (cond ((null first-interval)
	   nil)
	  ((< interval-end end old-start)
	   intervals)
	  ((<= interval-start end interval-end)
	   (cons (list interval-start end) (rest intervals)))
	  (t
	   (PCF-aux-aux-clip-end-interval (rest intervals) end interval-start)))))

(defun PCF-aux-non-zero-intervals (pcf)
  "Returns the non zero intervals for a pcf as a list of the
   form : ( (t_i t_i+1)* ) (i.e a list of sublists of length two)
   defining intervals where the pcf is not zero"
  (if (zerop (second pcf))
      (PCF-aux-look-for-valley (nthcdr 4 pcf) (list (third pcf)))
      (PCF-aux-look-for-valley (nthcdr 2 pcf) (list (first pcf)))))

(defun PCF-aux-look-for-valley (to-go so-far)
  "aux. function for PCF-aux-non-zero-intervals "
  (declare (inline PCF-aux-make-intervals-from-reversed-list ))
  (cond ((null to-go)
	 (PCF-aux-make-intervals-from-reversed-list so-far))
	((zerop (second to-go))
	 (PCF-aux-look-for-valley (nthcdr 4 to-go) (cons (third to-go) (cons (first to-go) so-far))))
	(t
	 (PCF-aux-look-for-valley (nthcdr 2 to-go) so-far))))

(defun PCF-aux-make-intervals-from-reversed-list (list)
  "takes the list accumulated from PCF-aux-look-for-valley and turns them into intervals"
  (cond ((null (first list))                       ;; ran off in zero interval
	 (make-dual-sublists (nreverse (rest list))))
	((oddp (length list))                      ; ran off in non-zero interval : *plus-infinity* is implicit
	 (make-dual-sublists (nreverse (cons *plus-infinity* list))))
	(t
	 (make-dual-sublists (nreverse list)))))


(defun PCF-non-zero-range-fraction (pcf range-start range-end)
  (let ((result 0))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((/ result (- range-end range-start)))
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(if (not (zerop value))
	    (incf result (- (min-of-two-values end range-end)
			    (max-of-two-values start range-start))))))))


(defun PCF-zero-range-fraction (pcf range-start range-end)
  (let ((result 0))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((/ result (- range-end range-start)))
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(if (zerop value)
	    (incf result (- (min-of-two-values end range-end)
			    (max-of-two-values start range-start))))))))


(defun PCF-to-interval-list (pcf &optional 
			     (range-start *MINUS-INFINITY*)
			     (range-end *PLUS-INFINITY*))
  "Converts a PCF to a list of triples (start end value) over the range
    specified"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(push (list (max-of-two-values start range-start)
		    (min-of-two-values end range-end)
		    value)
	      result)))))


(defun PCF-from-interval-list (interval-list)
  "Convert a list of time intervals and values into a PCF.
    The interval list is of the form ((t1 t2 1)(t3 t4 v2)...)
    The third element (value) of any list can be omitted:  it
    defaults to 1.
    Times not specified in any interval default to zero"
  (let ((result *ZERO-PCF*))
    (dolist (i interval-list)
      (setf result 
	    (PCF-set-value (dmy-to-time (first i))
			   (dmy-to-time (second i))
			   (if (third i) (third i) 1)
			   result)))
    result))
  

;;; Added 8-March-89 MDJ for Trans

(defun PCF-non-zero-intervals-shrunk (pcf1 pcf2)
  "Return t if nonzero intervals of pcf2 are subset of those of pcf1"
  (let ((test-pcf (pcf-combine pcf1 pcf2
                               #'(lambda (x y) 
                                   (if (and (= 0 x)
                                            (/= 0 y))
                                     1 0)))))
    (= 0 (pcf-max test-pcf))))

;; (pcf-non-zero-intervals-shrunk '(0 0 5 1 10 0) '(0 0 5 1 10 0))
;; (pcf-non-zero-intervals-shrunk '(0 0 5 1 10 0) '(0 0 6 1 8 0))
;; (pcf-non-zero-intervals-shrunk '(0 0 5 1 10 0) '(0 0 3 1 10 0))



(defun PCF-sinusoid (start end delta period zero-phase-time DC amplitude)
  "Create a sinusoidal PCF between start and end (zero elsewhere)
    The formula is DC + amplitude * sin( 2pi (x - zero-phase-time)/period)
    delta is the size of the PCF intervals"
  (let ((temp-PCF *ZERO-PCF*))
    (do* ((t1 start (+ t1 delta))
	  (t2 (min-of-two-values (+ t1 delta) end)
	      (min-of-two-values (+ t1 delta) end)))
	 ((>= t1 end) temp-PCF)
      (setf temp-PCF (PCF-set-value t1 t2
				    (+ DC
				       (* amplitude
					  (sin (/ (* 2pi
						     (- (+ t1 (/ delta 2.0))
							zero-phase-time))
						  period))))
				    temp-PCF)))))


(defun PCF-SHRINK-NONZERO-INTERVALS (PCF amount-to-shrink)
  "Shrink the ends of non-zero intervals by the specified amount"
  (if PCF
      (let ((i-list (PCF-non-zero-intervals PCF))
	    (result-i-list '())
	    ;; for constructing new intervals
	    start
	    end)
	(dolist (i i-list)
	  (setf start (first i)
		end (second i))
	  ;; shink the end
	  (if (/= end *PLUS-INFINITY*)
	      (setf end (- end amount-to-shrink)))
	  ;; accumulate new interval
	  (if (>= end start)
	      (push (list start end 1) result-i-list)))
	;; now compute PCF wherever activity is allowed to start
	;; and multiply this by the original PCF
	(PCF-multiply
	  PCF
	  (PCF-from-interval-list (nreverse result-i-list))))))


;;; bug fixed 30-Aug-88 MDJ

(defun PCF-kill-small-intervals (pcf min-size)
  "return PCF with any non-zero intervals smaller than min-size removed"
  (if PCF
      (let ((i-list (PCF-non-zero-intervals PCF))
	    result)
	(add-interval-to-reversed-PCF-list
	  result *minus-infinity* 0)
	(dolist (i i-list)
	  (add-interval-to-reversed-PCF-list
	    result (first i) (if (>= (- (second i) (first i)) min-size)
				 1 0))
	  (add-interval-to-reversed-PCF-list
	    result (second i) 0))
	(PCF-multiply
	  PCF
	  (nreverse result)))))


(defun PCF-trim-interval-ends (pcf trim-size)
  "return PCF with any non-zero intervals reduced by trim-size from
    their ends, e.g.
              +------------------+
              |                  |
     ---------+                  +--------
                   <--trim-size-->
    becomes:
              +----+
              |    |
     ---------+    +----------------------
     "
  (let (result
	)
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      (cond ((or (zerop value)
		 (< (- end trim-size) start))
	     ;; if value is zero, or if trimming would make interval go away...
	     (add-interval-to-reversed-PCF-list result start 0))
	    (t
	     (add-interval-to-reversed-PCF-list result start value)
	     (add-interval-to-reversed-PCF-list result (- end trim-size) 0))))))


;;(princ (pcf-replicate-periodic '(0 1 2 3 4) '(-1 0 0.25 1 0.75 0)))

(defun PCF-replicate-periodic (zero-phase-time-list one-period-PCF)
  "replicate a sample PCF over a range of periodic intervals.
    The one-period PCF should cover a range from 0 to 1.  The actual times
    in the copies are determined by the time between successive zero-phase-times,
    which need not be uniform"
  (let ((t1 (first zero-phase-time-list))
	result)
    (dolist (t2 (rest zero-phase-time-list))
      ;;(format t "~%~a to ~a" t1 t2)
      (add-interval-to-reversed-PCF-list result t1 (pcf-get-value 0 one-period-PCF))
      (PCF-loop-over-intervals one-period-PCF start end value left-to-do done
			       (nil)
			       (cond ((>= start 0)
				      (add-interval-to-reversed-PCF-list
					result (+ t1 (* start (- t2 t1))) value))
				     ((> start 1)
				      (setf done t))))
      (setf t1 t2))
    (nreverse result)))

(defun PCF-earliest-nonzero-interval-start (pcf &optional 
					    (range-start *MINUS-INFINITY*)
					    (range-end *PLUS-INFINITY*))
  "Return the start of the earliest nonzero interval within the
    range specified.  Return nil if no nonzero interval found"
  (PCF-loop-over-intervals
    pcf start end value left-to-do done
    (nil)
    
    (unless (or (<= end range-start)
		(>= start range-end))
      (when (not (zerop value))
	;; immediate return with the answer
	(return (max-of-two-values start range-start))))))


;;; Added  SDV  1/13/89 To be used w/ pcf-restricted-p

(defun PCF-earliest-zero-interval-start (pcf &key (range-start *MINUS-INFINITY*)
				    (range-end *PLUS-INFINITY*))
  "Return the start of the earliest nonzero interval within the
    range specified.  Return nil if no nonzero interval found"
  (PCF-loop-over-intervals
    pcf start end value left-to-do done
    (nil)
    
    (unless (or (<= end range-start)
		(>= start range-end))
      (when (zerop value)
	;; immediate return with the answer
	(return (max-of-two-values start range-start))))))


;;; Added  SDV  1/23/89 Got rid of call to pcf-min, moved from spike-core

(defun PCF-restricted-p (pcf &key (start  *MINUS-INFINITY*) (end  *PLUS-INFINITY*))
  "Is there any portion of this pcf that is zero  over the interval
   of the current planning session?"
  (PCF-earliest-zero-interval-start pcf :range-start start :range-end end))
 


(defun PCF-latest-nonzero-interval-end (pcf &optional 
					    (range-start *MINUS-INFINITY*)
					    (range-end *PLUS-INFINITY*))
  "Return the end of the latest nonzero interval within the
    range specified.  Return nil if no nonzero interval found"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      
      (unless (or (<= end range-start)
		  (>= start range-end))
	(when (not (zerop value))
	  (setf result (min-of-two-values end range-end)))))))

;; Commented out 12/11/90 - does not always work properly when range-start
;;  and range-end are supplied. Use original version above.  TPK.
;; Changed 1/23/89 Looks from rear instead of from front
#|
(defun PCF-latest-nonzero-interval-end (pcf &optional 
					    (range-start *MINUS-INFINITY*)
					    (range-end *PLUS-INFINITY*))
  "Return the end of the latest nonzero interval within the
    range specified.  Return nil if no nonzero interval found"
  (if (PCF-zero-p pcf)
	 nil
	 (let (( reversed-pcf (reverse pcf)))
	   (cond ((or (not (equal range-start *MINUS-INFINITY*))
		      (not (equal range-end *PLUS-INFINITY*)))
		  (aux-restricted-PCF-latest-nonzero-interval-end  (cons  *PLUS-INFINITY* reversed-pcf)
								   range-start range-end)
		  )
		 (t
		  (aux-PCF-latest-nonzero-interval-end (cons  *PLUS-INFINITY* reversed-pcf))
		  )))))
|#

(defun aux-PCF-latest-nonzero-interval-end (reversed-pcf)
  "aux. function for PCF-latest-nonzero-interval-end"
  (when (second reversed-pcf)
    (if (not (zerop (second reversed-pcf)))
	(first reversed-pcf) 
	(aux-PCF-latest-nonzero-interval-end (cddr reversed-pcf)))))

(defun aux-restricted-PCF-latest-nonzero-interval-end (reversed-pcf range-start range-end)
  "aux. function for PCF-latest-nonzero-interval-end"
  (when (second reversed-pcf)
    (let ((value (second reversed-pcf))
	  end start)
      (if (and (not (zerop value))
	       (setf end (first reversed-pcf)))
	  (if (<= end range-start)
	      nil
	      (if (and (setf start (third reversed-pcf))
		       (>= start range-end))
		  nil
		  value))
	  (aux-restricted-PCF-latest-nonzero-interval-end (cddr reversed-pcf) range-start range-end)))))



;;; Added 8-March MDJ for Trans:  

(defun PCF-bounds-and-intervals (PCF &optional 
                                     (range-start *MINUS-INFINITY*)
                                     (range-end *PLUS-INFINITY*))
  "Return multiple values:  earliest start, latest start, non-zero interval 
    list"
  (let ((result nil)
        (early nil)
        (late nil))
    (PCF-loop-over-intervals
     pcf start end value left-to-do done
     ((values early late (nreverse result)))
     
     (unless (or (<= end range-start)
                 (>= start range-end))
       (when (not (zerop value))
         ;; early
         (if (not early) (setf early (max-of-two-values start range-start)))
         ;; late
         (setf late (min-of-two-values end range-end))
         ;; intervals
         (if (null result)
           ;; then start the output list
           (push (list (max-of-two-values start range-start)
                       (min-of-two-values end range-end))
                 result)
           ;; else add to output list
           (let ((tmin (max-of-two-values start range-start))
                 (tmax (min-of-two-values end range-end))
                 (old-tmax (second (first result))))
             ;; see whether the new interval can be merged with the previous one
             (if (> tmin old-tmax)
               ;; add new nonzero interval to the result
               (push (list tmin tmax) result)
               ;; modify the end time of the most recent interval addede
               (setf (second (first result)) tmax)))))))))

#| test:
(pcf-bounds-and-intervals '(0 0 1 1 5 0)) => 1 5 ((1 5))
(pcf-bounds-and-intervals '(0 0 1 1 5 0 10 1 11 0)) => 1 11 ((1 5)(10 11))
(pcf-bounds-and-intervals '(0 0 1 1 5 0 10 1 11 0) 2 10.5) => 2 10.5 ((2 5)(10 10.5))
(pcf-bounds-and-intervals *zero-pcf*) => nil nil nil
|#


;;; routines to find pcf means, maxes, or integrals over a list of 
;;; intervals or over a partitioned time range

(defun PCF-integral-over-partition (pcf partition)
  "Find the integral value of a PCF over the contiguous intervals
    specified by a partition (t1 t2 ... tn) of the interval t1 to tn.  
    A list of values are returned corresponding to the intervals 
    [t1,t2), [t2,t3), ..."
  (let* ((result-list '())
         (result 0)
         (number-of-intervals (1- (list-length partition)))
         (range-start (pop partition))
         (range-end (pop partition))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null range-end)
		   ;;(format t "~%range-end is nil")
		   (setf done t)
		   (return nil))
		  ((< range-end start)
		   (push result result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (< range-end end))
		   (incf result (* value (- range-end start)))
		   (push result result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (>= range-end end))
		   (incf result (* value (- end start)))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (< range-end end))
		   (incf result (* value (- range-end range-start)))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (push result result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   )
		  ((and (>= range-start start) 
			(< range-start end) 
			(>= range-end end))
		   (incf result (* value (- end range-start)))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  ((null partition)
		   (push result result-list)
		   (setf done t)
		   ;;(format t "~%interval list is empty")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= (list-length result-list) number-of-intervals)
	(error "PCF-integral-over-partition:  wrong number of values found"))
    
    ;; returned value
    (nreverse result-list)
    ))


(defun PCF-integral-over-intervals (pcf intervals)
  "Find the integral value of a PCF over the intervals
    specified by the intervals ((t1 t2) (t3 t4) ...).  
    The intervals must be in increasing order of start time.
    A list of values are returned corresponding to the intervals."
  (let* ((result-list '())
         (result 0)
         (number-of-intervals (list-length intervals))
         (interval (pop intervals))
         (range-start (first interval))
         (range-end (second interval))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null interval)
		   ;;(format t "~%interval is nil")
		   (setf done t)
		   (return nil))
		  ((< range-end start)
		   (push result result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (< range-end end))
		   (incf result (* value (- range-end start)))
		   (push result result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (>= range-end end))
		   (incf result (* value (- end start)))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (< range-end end))
		   (incf result (* value (- range-end range-start)))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (push result result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   )
		  ((and (>= range-start start) 
			(< range-start end) 
			(>= range-end end))
		   (incf result (* value (- end range-start)))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= (list-length result-list) number-of-intervals)
	(error "PCF-integral-over-intervals:  wrong number of values found"))
    
    ;; returned value
    (nreverse result-list)
    ))


(defun mean-PCF-over-partition (pcf partition)
  "Find the mean value of a PCF over the contiguous intervals
    specified by a partition (t1 t2 ... tn) of the interval t1 to tn.  
    A list of values are returned corresponding to the intervals 
    [t1,t2), [t2,t3), ..."
  (let* ((result-list '())
         (result 0)
         (number-of-intervals (1- (list-length partition)))
         (range-start (pop partition))
         (range-end (pop partition))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null range-end)
		   ;;(format t "~%range-end is nil")
		   (setf done t)
		   (return nil))
		  ((< range-end start)
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (< range-end end))
		   (incf result (* value (- range-end start)))
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (>= range-end end))
		   (incf result (* value (- end start)))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (< range-end end))
		   (incf result (* value (- range-end range-start)))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf range-start range-end)
		   (setf range-end (pop partition))
		   )
		  ((and (>= range-start start) 
			(< range-start end) 
			(>= range-end end))
		   (incf result (* value (- end range-start)))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  ((null partition)
		   (push (/ result (- range-end range-start)) result-list)
		   (setf done t)
		   ;;(format t "~%interval list is empty")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= (list-length result-list) number-of-intervals)
	(error "mean-PCF-over-partition:  wrong number of values found"))
    
    ;; returned value
    (nreverse result-list)
    ))

(defun mean-PCF-over-intervals (pcf intervals)
  "Find the mean value of a PCF over the intervals
    specified by the intervals ((t1 t2) (t3 t4) ...).  
    The intervals must be in increasing order of start time.
    A list of values are returned corresponding to the intervals."
  (let* ((result-list '())
         (result 0)
         (number-of-intervals (list-length intervals))
         (interval (pop intervals))
         (range-start (first interval))
         (range-end (second interval))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null interval)
		   ;;(format t "~%interval is nil")
		   (setf done t)
		   (return nil))
		  ((< range-end start)
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (< range-end end))
		   (incf result (* value (- range-end start)))
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (>= range-end end))
		   (incf result (* value (- end start)))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (< range-end end))
		   (incf result (* value (- range-end range-start)))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (push (/ result (- range-end range-start)) result-list)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   )
		  ((and (>= range-start start) 
			(< range-start end) 
			(>= range-end end))
		   (incf result (* value (- end range-start)))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= (list-length result-list) number-of-intervals)
	(error "mean-PCF-over-intervals:  wrong number of values found"))
    
    ;; returned value
    (nreverse result-list)
    ))

;;; identical to the previous function except that the result is
;;; put into the supplied vector

(defun mean-PCF-over-intervals-to-vector (pcf intervals result-vector)
  "Find the mean value of a PCF over the intervals
    specified by the intervals ((t1 t2) (t3 t4) ...).  
    The intervals must be in increasing order of start time.
    The results are destructively put into the array result-vector"
  (declare (optimize speed (safety 0)))
  (let* ((counter 0)
         (result 0)
         (number-of-intervals (list-length intervals))
         (interval (pop intervals))
         (range-start (first interval))
         (range-end (second interval))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null interval)
		   ;;(format t "~%interval is nil")
		   (setf done t)
		   (return nil))
		  ((< range-end start)
		   (setf (aref result-vector counter)
			 (/ result (- range-end range-start)))
		   (incf counter)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (< range-end end))
		   (incf result (* value (- range-end start)))
		   (setf (aref result-vector counter)
			 (/ result (- range-end range-start)))
		   (incf counter)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (< range-start start) (>= range-end end))
		   (incf result (* value (- end start)))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (< range-end end))
		   (incf result (* value (- range-end range-start)))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (setf (aref result-vector counter)
			 (/ result (- range-end range-start)))
		   (incf counter)
		   (setf result 0)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   )
		  ((and (>= range-start start) 
			(< range-start end) 
			(>= range-end end))
		   (incf result (* value (- end range-start)))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))

    ;; error check:
    (if (/= counter number-of-intervals)
	(error "mean-PCF-over-intervals:  wrong number of values found"))
    
    ;; returned value
    result-vector
    ))


(defun pcf-ave-over-range (pcf s e N)
  "average PCF over a range divided into N intervals.  s and e are
    the start and end of the range of interest"
  (let ((interval-size (/ (- e s) N))
	partition
	average-list
	(result (reverse *zero-PCF*))
	)
    ;; make partition of the interval into N uniform sub-intervals
    (dotimes (i (+ N 1))
      (push (- e (* i interval-size)) partition))
    ;; get averages
    (setf average-list (mean-PCF-over-partition pcf partition))
    ;; reconstruct PCF holding averages
    (dolist (v average-list)
      (add-interval-to-reversed-PCF-list
	result (first partition) v)
      (pop partition))
    (add-interval-to-reversed-PCF-list result (first partition) 0)
    ;; returned PCF
    (nreverse result)))


(defun PCF-max-over-intervals (pcf intervals)
  "Find the maximum value of a PCF over the intervals
    specified by the intervals ((t1 t2) (t3 t4) ...).  
    The intervals must be in increasing order of start time.
    A list of values are returned corresponding to the intervals."
  (let* ((result-list '())
         (result 0)
         (number-of-intervals (list-length intervals))
         (interval (pop intervals))
         (range-start (first interval))
         (range-end (second interval))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null interval)
		   ;;(format t "~%interval is nil")
		   (setf done t)
		   (return nil))
		  ((<= range-end start)
		   (push result result-list)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (<= range-start start) (<= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   (push result result-list)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (<= range-start start) (>= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (<= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (push result result-list)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   )
		  ((and (>= range-start start) 
			(<= range-start end) 
			(>= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= (list-length result-list) number-of-intervals)
	(error "PCF-max-over-intervals:  wrong number of values found"))
    
    ;; returned value
    (nreverse result-list)
    ))



;;; identical to the previous function except that the result is put
;;; into the supplied vector

(defun PCF-max-over-intervals-to-vector (pcf intervals result-vector)
  "Find the maximum value of a PCF over the intervals
    specified by the intervals ((t1 t2) (t3 t4) ...).  
    The intervals must be in increasing order of start time.
    The results are destructively put into the array result-vector"
  (let* ((counter 0)
         (result 0)
         (number-of-intervals (list-length intervals))
         (interval (pop intervals))
         (range-start (first interval))
         (range-end (second interval))
         )
    
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (nil)
      
      ;; 6 cases of interest
      (loop (cond ((null interval)
		   ;;(format t "~%interval is nil")
		   (setf done t)
		   (return nil))
		  ((<= range-end start)
		   (setf (aref result-vector counter) result)
		   (incf counter)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case I, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (<= range-start start) (<= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   (setf (aref result-vector counter) result)
		   (incf counter)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   ;;(format t "~%case II, new range is ~a to ~a" range-start range-end)
		   )
		  ((and (<= range-start start) (>= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case III, quitting loop")
		   (return nil))
		  ((and (>= range-start start) (<= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case IV value ~a over range ~a to ~a"
		   ;;        value range-start range-end)
		   (setf (aref result-vector counter) result)
		   (incf counter)
		   (setf result nil)
		   (setf interval (pop intervals))
		   (setf range-start (first interval))
		   (setf range-end (second interval))
		   )
		  ((and (>= range-start start) 
			(<= range-start end) 
			(>= range-end end))
		   (setf result (if result (max-of-two-values result value) value))
		   ;;(format t "~%case V, quitting loop")
		   (return nil))
		  ((>= range-start start)
		   ;;(format t "~%case VI")
		   (return nil))
		  (t (error "~%ERROR:  dropped through overlap tests!")
		     (return nil)))))
    ;; error check:
    (if (/= counter number-of-intervals)
	(error "PCF-max-over-intervals:  wrong number of values found"))
    
    ;; returned value
    result-vector
    ))


;;; utility to convert list (start1 end1 start2 end2 ...)
;;; into PCF with value value-inside-interval between each start/end 
;;; and value value-outside-interval between each end and start

(defun PCF-FROM-START-END-LIST 
       (start-end-list value-inside-interval value-outside-interval)
  (let ((result '())
        (start-flag t))
    (add-interval-to-reversed-PCF-list 
     result *minus-infinity* value-outside-interval)
    (dolist (time start-end-list)
      (cond (start-flag
             (add-interval-to-reversed-PCF-list result time value-inside-interval)
             (setf start-flag nil))
            (t
             (add-interval-to-reversed-PCF-list result time value-outside-interval)
             (setf start-flag t))))
    (nreverse result)))

;; (pcf-from-start-end-list '(1 2 10 11) 1 0)


;;; utility to convert file of start1 end1 start2 end2 ...
;;; into PCF with value value-inside-interval between each start/end 
;;; and value value-outside-interval between each end and start

(defun PCF-FROM-START-END-FILE 
       (stream value-inside-interval value-outside-interval)
  (let (time
        (result '())
        (start-flag t))
    (add-interval-to-reversed-PCF-list 
     result *minus-infinity* value-outside-interval)
    (loop
      (setf time (read stream nil nil))
      (if (null time) (return nil))
      (cond (start-flag
             (add-interval-to-reversed-PCF-list result time value-inside-interval)
             (setf start-flag nil))
            (t
             (add-interval-to-reversed-PCF-list result time value-outside-interval)
             (setf start-flag t))))
    (nreverse result)))

;;; added JLS 3 Apr 90
#|
(pcf-one-if-nonzero '(1000 0.1 2000 0.5 3000 0.2 4000 0 5000 0.4))
(-1.7976931348623157e308 0 1000 1 4000 0 5000 1)
|#
(defun PCF-ONE-IF-NONZERO (pcf)
  "If a suitability value is non-zero, it is replaced by 1."
  (pcf-from-interval-list (pcf-non-zero-intervals pcf)))


#|   PCF I/O MOD functions
The following 4 functions are useful because due to
floating point differences on different architectures
minus-infinity may be different.
When PCFs are file away, then the symbol *minus-infinity* is
stored and NOT the value.
When the PCF is read from a file, the substitution goes from
symbol to value.
|#

(defun SAVE-PCF-TO-STREAM (stream PCF)
  (let ((*print-level* nil)
        (*print-length* nil))
    (if (= (first PCF) *minus-infinity*)
      (print (cons '*minus-infinity* (rest PCF)) stream)
      (print PCF stream))))

(defun READ-PCF-FROM-STREAM (stream &aux result)
  (setf result (read stream))
  (if (eq (first result) '*minus-infinity*)
    (cons *minus-infinity* (rest result))
    result))

#|
(mod-pcf-for-output '(-1.7976931348623157e308 1 3892.5 0 3939.5 0.1)) =>
(*MINUS-INFINITY* 1 3892.5 0 3939.5 0.1)
(mod-pcf-for-output '(3892.5 0 3939.5 0.1)) => (3892.5 0 3939.5 0.1)
|#
(defun MOD-PCF-FOR-OUTPUT (PCF)
  "Replace first element of PCF with symbol *MINUS-INFINITY*
   if it is the numeric value MINUS-INFINITY"
  (if (= (first PCF) *minus-infinity*)
      (cons '*minus-infinity* (rest PCF))
      pcf))

#|
(mod-pcf-for-input '(*MINUS-INFINITY* 1 3892.5 0 3939.5 0.1)) =>
(-1.7976931348623157e308 1 3892.5 0 3939.5 0.1)

(mod-pcf-for-input '(3892.5 0 3939.5 0.1)) =>
(3892.5 0 3939.5 0.1)

|#

;;; Following two operators are inverses, 
;;; i.e. (PCF-REMOVE-MINUS-INFINITY-TOKEN (REPLACE-MINUS-INFINITY-IN-PCF-BY-SYMBOL foo))
;;; gives same pcf (tho not eq to original pcf)

(defun REPLACE-MINUS-INFINITY-IN-PCF-BY-SYMBOL (list)
  (if (and list (listp list) (= *minus-infinity* (first list)))
      (cons '*minus-infinity* (rest list))
      list))

;;(REPLACE-MINUS-INFINITY-IN-PCF-BY-SYMBOL (pcf-create-with-single-interval 0 1 2 0))
;;(REPLACE-MINUS-INFINITY-IN-PCF-BY-SYMBOL '(0 1 2))
;;(REPLACE-MINUS-INFINITY-IN-PCF-BY-SYMBOL nil)

(defun PCF-REMOVE-MINUS-INFINITY-TOKEN (foo)
    "destructively modifies a pcf which begins with '*minus-infinity*,
     by substituting the value of *minus-infinity*. returns the pcf."
    (if (eq (first foo) '*minus-infinity*)
	(setf (first foo) *minus-infinity*))
    foo)

(defun MOD-PCF-FOR-INPUT (pcf)
  "Replace first element of PCF with numeric value
   if it is the symbol *MINUS-INFINITY*"
  (if (eq (first pcf) '*minus-infinity*)
      (cons *minus-infinity* (rest pcf))
      pcf))

#| Test read/restore PCF from stream 
(setf p (pcf-create 0))
(setf p (pcf-set-value 1 2 1 p))
(setf p (pcf-set-value 10 11 2 p))
(setf scratch-output 
      (open "scratch.lisp" :direction :output :if-exists :supersede))
(save-pcf-to-stream scratch-output p)
(close scratch-output)
(setf scratch-input 
      (open "scratch.lisp" :direction :input))
(read-pcf-from-stream scratch-input)
(file-position scratch-input :start)
(equal p (read-pcf-from-stream scratch-input))
(close scratch-input)
(ed "scratch.lisp")
|#

;;; Moved this here 28AUG91
(defun CONVERT-TJD-PCF-TO-IPCF (tjd-pcf)
  "convert a pcf with time represented in TJD to an integral pcf
    with time represented in abs-time ticks"

  ;; use this to convert pcfs returned by hst-abs-constraints
  ;; into micro-spike constraints
  (let ((tjd-intervals (pcf-to-interval-list tjd-pcf))
	(i-intervals nil))
    (dolist (interval tjd-intervals)
      (let* ((start (first interval))
	     (end (second interval))
	     (scaled-start
	      (if (= start *minus-infinity*)
		*minus-infinity*
		(abs-time :tjd start)))
	     (scaled-end (if (= end *plus-infinity*)
			   *plus-infinity*
			   (- (abs-time :tjd end) 1)))
	     (value (third interval)))
	(push (list scaled-start scaled-end value) i-intervals)))
    (ipcf-from-interval-list i-intervals))) 


;;; the following are special purpose PCF combiner or transformer functions

(defun MAKE-NUMBER-UNITY (number)
  "Given a number, returns 0 if the number is zero, 1 otherwise.
    Used as a PCF-transform function."
  (if (zerop number) 0 1))


(defun SUM-IF-NEITHER-ZERO (a b)
  "If a and b are both nonzero, then return their sum;
    if either are zero, return zero"
  (if (or (zerop a)(zerop b))
      0
      (+ a b)))

;;; ----------------------------------------------------------------------
;;;                                         INTEGER PCF ROUTINES
;;;
;;; These represent a modification to the above PCF routines where times
;;; are integral and intervals are specified to INCLUDE their endpoints
;;; (continuous PCFs include their start point but not their endpoint).
;;;
;;; Some of these routines are identical to the continuous version, but
;;; for consistency all which have been tested and verified to be the
;;; same still have definitions here with an ipcf- extension.
;;;
;;; - MDJ 26-Jun-1989


;;; these don't change but have been named ipcf for consistency

(defun iPCF-create (value)
  (PCF-create value))

(defun iPCF-copy (pcf)
  (PCF-copy pcf))

(defun iPCF-get-value (time pcf)
  (PCF-get-value time pcf))

(defun iPCF-transform (pcf transformer-function)
  (PCF-transform pcf transformer-function))

(defun iPCF-SHIFT-TIME (pcf time)
  (PCF-SHIFT-TIME pcf time))

(defun iPCF-combine (f1 f2 combiner-function)
  (PCF-combine f1 f2 combiner-function))

(defun iPCF-multiply (f1 f2)
  (PCF-multiply f1 f2))

(defun iPCF-non-zero-intervals-shrunk (pcf1 pcf2)
  (PCF-non-zero-intervals-shrunk pcf1 pcf2))




;;; these change to handle inclusive integer intervals




(defun iPCF-set-value (new-start new-end new-value pcf)
  "Set the PCF to have the specified value between start and end.
    non-destructive:  returns a new PCF"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      ((nreverse result))
      
      (cond ((and (>= new-start start) 
		  (< new-end end))
	     ;; new interval is wholly contained in this interval
             (cond ((> new-start start)
                    (cond ((< new-end (1- end))
                           (add-interval-to-reversed-PCF-list 
                            result start value)
                           (add-interval-to-reversed-PCF-list 
                            result new-start new-value)
                           (add-interval-to-reversed-PCF-list 
                            result (1+ new-end) value))
                          (t ;; new-end = old-end
                           (add-interval-to-reversed-PCF-list 
                            result start value)
                           (add-interval-to-reversed-PCF-list 
                            result new-start new-value)
                           )))
                   (t ;; new-start = old-start
                    (cond ((< new-end (1- end))
                           (add-interval-to-reversed-PCF-list 
                            result start new-value)
                           (add-interval-to-reversed-PCF-list 
                            result (1+ new-end) value))
                          (t ;; new-end = old-end
                           (add-interval-to-reversed-PCF-list 
                            result start new-value)))))
	     )
	    ((and (< new-start start)
		  (>= new-end end))
	     ;; old interval wholly contained in new interval
	     )
	    ((and (>= new-start start)
		  (>= new-end end)
		  (<= new-start end))
	     ;; new segment starts in this interval but doesn't end
	     (when (> new-start start)
	       (add-interval-to-reversed-PCF-list result start value))
	     (add-interval-to-reversed-PCF-list result new-start new-value)
	     )
	    ((and (< new-start start)
		  (< new-end end)
		  (>= new-end start))
	     ;; segment ends in this interval but didn't start here
	     (when (< new-end (1- end))
               (add-interval-to-reversed-PCF-list result (1+ new-end) value))
	     )
	    (t ;; no change
	     (add-interval-to-reversed-PCF-list result start value)
	     )))))

(defun iPCF-create-with-single-interval (start end value-in value-out)
  "Create a PCF with a value of value-in from start to end, and
    value-out otherwise.  Start and end inclusive and must be integers.
    The pcf returned is for integral time."
  (cond ((= value-in value-out)
	 (iPCF-create value-out))
	((< end start)
	 (iPCF-create value-out))
        ((and (= start *MINUS-INFINITY*)
              (= end *PLUS-INFINITY*))
         (list start value-in))
        ((= end *PLUS-INFINITY*)
         (list *MINUS-INFINITY* value-out start value-in))
        ((= start *MINUS-INFINITY*)
         (list start value-in (1+ end) value-out))
	(t
	 (list *MINUS-INFINITY* value-out start value-in 
               (1+ end) value-out))))


;;; new version of the following 11/10/92, much faster if the
;;; interval list is ordered.  Old code follows and is commented
;;; out.  If the interval list is NOT ordered, then the equivalent
;;; old function is called.

(defun iPCF-from-interval-list (interval-list)

  "Convert a list of time intervals and values into a PCF.
    The interval list is of the form ((t1 t2 v1)(t3 t4 v2)...)
    Times must be integral (e.g. ticks).  The intervals
    specified are inclusive, i.e. they include their endpoints.
    The third element (value) of any list can be omitted:  it
    defaults to 1.
    Times not specified in any interval default to zero"

  ;; note: no error checking that times are integral
  ;; no checking for interval overlap:  interval values are set in order
  ;; they occur in the list, so the last specified values win

  (let ((try-ordered
         (ipcf-from-interval-list-ordered interval-list)))
    (cond ((eq try-ordered :BAD-INTERVAL-LIST)
           (iPCF-from-interval-list-general interval-list))
          (t try-ordered))))


(defun iPCF-from-interval-list-general (interval-list)
  
  "see iPCF-from-interval-list: this function is called only if
    the input intervals are not ordered or contain overlaps. 
    Otherwise iPCF-from-interval-list-ordered is called (which is much
    faster)"
  
  (let ((result *ZERO-PCF*))
    (dolist (i interval-list)
      (let ((t1 (first i))
            (t2 (second i)))
        (setf result 
	  (iPCF-set-value (min-of-two-values t1 t2)
			  (max-of-two-values t1 t2)
			  (or (third i) 1)
			  result))))
    result))


(defun iPCF-from-interval-list-ordered (interval-list)

  "see iPCF-from-interval-list for description.  This function
    works quickly if intervals are ordered and non-overlapping,
    or if, at worst, intervals are contained in and abut the end of
    previous intervals.  If not, this function returns the keyword
    :BAD-INTERVAL-LIST and the caller is responsibility for dealing
    with the problem"

  (let ((result nil)
        (prev-interval nil)
        (prev-start nil)
        (prev-end nil)
        )
    (setf result (list 0 *minus-infinity*))
    
    (dolist (interval interval-list)
      (let* ((t1 (first interval))
             (t2 (second interval))
             (start (min-of-two-values t1 t2))
             (end   (max-of-two-values t1 t2))
             (val (or (third interval) 1)))

        (cond
	 ;; no previous interval: this is the first
         ((not prev-interval)
          (push start result)
          (push val result)
          (push (1+ end) result)
          (push 0 result))

	 ;; current interval is >= previous one and abuts
	 ;;   prev interval: +------+
	 ;;   current:              +----+
	 ;;    or                    +---+
         ((and prev-interval
               (or (= start prev-end)
                   (= start (1+ prev-end))))
          (setf (first result) val)
          (setf (second result) start)
          (push (1+ end) result)
          (push 0 result))

	 ;; current interval is > previous and doesn't abut
	 ;;  prev: +-----+
	 ;;  current:         +----+
         ((and prev-interval
               (> start prev-end))
          (push start result)
          (push val result)
          (push (1+ end) result)
          (push 0 result))

	 ;; current interval contained in and abuts end of previous
	 ;;  prev: +-------+
	 ;;  current  +----+
         ((and prev-interval
               (>= start prev-start)
               (= end prev-end))
          (setf (second result) start)
          (setf (first result) val)
          (push (1+ end) result)
          (push 0 result))

         (t 
;          (format t "~%ipcf-from-interval-list warning: intervals unordered/overlapping")
          (return-from iPCF-from-interval-list-ordered
            :BAD-INTERVAL-LIST))
         )				; end cond

	;; save prev values
        (setf prev-interval interval)
        (setf prev-start start)
        (setf prev-end   end)))

    (nreverse result)))


#|===============   THIS IS THE OLD (OBSOLETE) VERSION 11/10/92 MDJ
(defun iPCF-from-interval-list (interval-list)
  "Convert a list of time intervals and values into a PCF.
    The interval list is of the form ((t1 t2 1)(t3 t4 v2)...)
    Times must be in standard abs-time form (ticks).  The intervals
    specified are inclusive, i.e. they include their endpoints.
    The third element (value) of any list can be omitted:  it
    defaults to 1.
    Times not specified in any interval default to zero"
  ;; note: no error checking that times are integral
  ;; no checking for interval overlap:  interval values are set in order
  ;; they occur in the list, so the last specified values win
  (let ((result *ZERO-PCF*))
    (dolist (i interval-list)
      (setf result 
            (iPCF-set-value (first i)
                            (second i)
                            (if (third i) (third i) 1)
                            result)))
    result))
===================|#


(defun iPCF-to-interval-list (pcf &optional 
                                  (range-start *MINUS-INFINITY*)
                                  (range-end *PLUS-INFINITY*))
  "Converts a PCF to a list of triples (start end value) over the range
    specified.  The range is inclusive (i.e. the endpoints are included).
    The PCF is assumed to represent an integral time inclusive PCF."
  (let ((result nil))
    (PCF-loop-over-intervals
     pcf start end value left-to-do done
     ((nreverse result))
     
     (unless (or (<= end range-start)
                 (> start range-end))
       (push (list (max-of-two-values start range-start)
                   (cond ((<= end range-end)
                          (1- end))
                         ((> end range-end)
                          range-end))
                   value)
             result)))))


(defun iPCF-non-zero-intervals (pcf &optional 
                                    (range-start *MINUS-INFINITY*)
                                    (range-end *PLUS-INFINITY*))
  "Returns a list of (start end) times when the input PCF is non-zero.
    Times are integral and inclusive."
  (let ((result nil))
    (PCF-loop-over-intervals
     pcf start end value left-to-do done
     ((nreverse result))
     
     (unless (or (<= end range-start)
                 (> start range-end))
       (when (not (zerop value))
         (if (null result)
           
           ;; start the output list
           (push (list (max-of-two-values start range-start)
                       (if (<= end range-end) (1- end) range-end))
                 result)
           
           ;; add to output list
           (let ((tmin (max-of-two-values start range-start))
                 (tmax (if (<= end range-end) (1- end) range-end))
                 (old-tmax (1+ (second (first result)))))
             ;; see whether the new interval can be merged with the previous one
             (if (> tmin old-tmax)
               ;; add new nonzero interval to the result
               (push (list tmin tmax) result)
               ;; modify the end time of the most recent interval addede
               (setf (second (first result)) tmax)))))))))


(defun iPCF-bounds-and-intervals (PCF &optional 
                                     (range-start *MINUS-INFINITY*)
                                     (range-end *PLUS-INFINITY*))
  "Return multiple values:  earliest start, latest start, non-zero interval 
    list"
  (let ((result nil)
        (early nil)
        (late nil))
    (PCF-loop-over-intervals
     pcf start end value left-to-do done
     ((values early late (nreverse result)))
     
     (unless (or (<= end range-start)
                 (> start range-end))
       (when (not (zerop value))
         ;; early
         (if (not early) (setf early (max-of-two-values start range-start)))
         ;; late
         (setf late (if (<= end range-end) (1- end) range-end))
         ;; intervals
         (if (null result)
           ;; then start the output list
           (push (list (max-of-two-values start range-start)
                       (if (<= end range-end) (1- end) range-end))
                 result)
           ;; else add to output list
           (let ((tmin (max-of-two-values start range-start))
                 (tmax (if (<= end range-end) (1- end) range-end))
                 (old-tmax (1+ (second (first result)))))
             ;; see whether the new interval can be merged with the previous one
             (if (> tmin old-tmax)
               ;; add new nonzero interval to the result
               (push (list tmin tmax) result)
               ;; modify the end time of the most recent interval addede
               (setf (second (first result)) tmax)))))))))


(defun iPCF-min (pcf &optional 
		(range-start *MINUS-INFINITY*)
		(range-end *PLUS-INFINITY*))
  "Find the minimum value of a PCF, optionally in a specified range
    (only intervals with nonzero overlap with the range are considered)"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      
      (unless (or (<= end range-start)
		  (> start range-end))
	(if (null result)
	    (setf result value)
	    (setf result (min-of-two-values result value)))))))

(defun iPCF-max (pcf &optional 
		(range-start *MINUS-INFINITY*)
		(range-end *PLUS-INFINITY*))
  "Find the maximum value of a PCF, optionally in a specified range
    (only intervals with nonzero overlap with the range are considered)"
  (let ((result nil))
    (PCF-loop-over-intervals
      pcf start end value left-to-do done
      (result)
      
      (unless (or (<= end range-start)
		  (> start range-end))
	(if (null result)
	    (setf result value)
	    (setf result (max-of-two-values result value)))))))


(defun iPCF-SHRINK-NONZERO-INTERVALS (PCF amount-to-shrink)
  "Shrink the ends of non-zero intervals by the specified amount"
  (if PCF
      (let ((i-list (iPCF-non-zero-intervals PCF))
	    (result-i-list '())
	    ;; for constructing new intervals
	    start
	    end)
	(dolist (i i-list)
	  (setf start (first i)
		end (second i))
	  ;; shink the end
	  (if (/= end *PLUS-INFINITY*)
	      (setf end (- end amount-to-shrink)))
	  ;; accumulate new interval
	  (if (>= end start)
	      (push (list start end 1) result-i-list)))
	;; now compute PCF wherever activity is allowed to start
	;; and multiply this by the original PCF
	(iPCF-multiply
	  PCF
	  (iPCF-from-interval-list (nreverse result-i-list))))))



(defun IPCF-ZERO-INTERVALS (ipcf)

  "input ipcf, output list of start/end times ((t1 t2)(t3 t4)...)
    when the ipcf is zero"

  (let ((result nil))
    (mapcar #'(lambda (interval)
                (if (= 0 (third interval))
                  (push (list (first interval) (second interval)) result)))
            (ipcf-to-interval-list ipcf))
    (nreverse result)))


(defmacro FOR-EACH-TIME-IN-IPCF (ipcf start end time value &rest body)

  "Execute body for each integral time between start and end inclusive.
    Variables named time and value are bound to the time and value
    of the input IPCF.  Example:
     (for-each-time-in-ipcf ipcf 10 100 time value
       (format t '~%t=~a value=~a' time value))"

  `(dolist (interval (ipcf-to-interval-list ,ipcf ,start ,end))
     (do ((,time (first interval) (1+ ,time))
          (,value (third interval) (third interval)))
         ((> ,time (second interval)) nil)
       ,@body)))


(defun IPCF-AVERAGE-OVER-RANGE (ipcf start end)
  
  "Return average value of ipcf between start and end, inclusive"
  
  (if (< end start) (error "end (~a) < start (~a)" end start))
  (let ((sum 0)
        (count (1+ (- end start))))
    (loop-over-value-range
     i start end
     (let ((val (ipcf-get-value i ipcf)))
       (if (not val) (error "no IPCF value at time ~a ~a" i ipcf))
       (incf sum val)))
    (/ sum count)))

#| Test cases:

(ipcf-average-over-range (ipcf-from-interval-list '((1 5 1)(6 10 2)(15 20 5)))
                         15 21)
(ipcf-to-interval-list (ipcf-create-with-single-interval 4 6 3 0))
(ipcf-to-interval-list '(0 0 4 3 7 0))
(ipcf-to-interval-list (ipcf-set-value 4 6 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 4 5 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 5 6 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 5 5 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 3 7 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 4 10 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 5 10 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 6 10 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 7 10 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 1 3 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 1 4 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 1 5 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 1 6 99 '(0 0 4 3 7 0)))
(ipcf-to-interval-list (ipcf-set-value 1 7 99 '(0 0 4 3 7 0)))


(ipcf-to-interval-list 
 (iPCF-create-with-single-interval *minus-infinity* 10 1 0))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval *minus-infinity* *plus-infinity* 1 0))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval 15 *plus-infinity* 1 0))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval 1 3 0 1))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval 1 3 1 0))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval 1 2 1 1))
(ipcf-to-interval-list 
 (iPCF-create-with-single-interval 2 1 1 0))

(setf p (ipcf-create-with-single-interval 1 5 2 0))
(ipcf-to-interval-list p)

(setf ilist
      (ipcf-from-interval-list '((0 1 0)(2 3 1)(4 6 3)(7 7 1)(8 10 0)
                                 (11 15 1))))
(ipcf-to-interval-list ilist)
(ipcf-to-interval-list ilist 3 7)
(ipcf-to-interval-list ilist 3 6)
(ipcf-to-interval-list ilist 0 10)

(ipcf-non-zero-intervals ilist)
(ipcf-non-zero-intervals ilist 2 7)
(ipcf-non-zero-intervals ilist 8 10)
(ipcf-non-zero-intervals ilist 8 11)
(ipcf-non-zero-intervals ilist 7 10)

(ipcf-bounds-and-intervals ilist)
(ipcf-bounds-and-intervals ilist 2 7)
(ipcf-bounds-and-intervals ilist 8 10)
(ipcf-bounds-and-intervals ilist 8 11)
(ipcf-bounds-and-intervals ilist 7 10)

(ipcf-get-value 10 ilist)

(ipcf-to-interval-list (ipcf-shift-time ilist 2))
(ipcf-to-interval-list (ipcf-shift-time ilist -2))

(ipcf-to-interval-list 
 (ipcf-multiply (ipcf-create-with-single-interval 1 2 1 0)
               (ipcf-create-with-single-interval 3 4 1 0)))

(ipcf-to-interval-list (ipcf-transform ilist #'make-number-unity))

(ipcf-max ilist)
(ipcf-max ilist 0 3)
(ipcf-max ilist 0 4)
(ipcf-max ilist 1 4)
(ipcf-min ilist)
(ipcf-min ilist 0 3)
(ipcf-min ilist 0 4)
(ipcf-min ilist 1 4)
(ipcf-min ilist 3 7)
(ipcf-min ilist 3 6)
(ipcf-min ilist 4 7)

;; note that ipcf won't work on interval lists where end of one interval
;; is start of next
(ipcf-to-interval-list
 (ipcf-from-interval-list '((1 2 1)(2 2 5)(2 8 1)(8 10 9))))

(print (ipcf-to-interval-list
	 (ipcf-shrink-nonzero-intervals
	   (ipcf-from-interval-list '((0 10 1)(11 20 2))) 0)))
(print (ipcf-to-interval-list
	 (ipcf-shrink-nonzero-intervals
	   (ipcf-from-interval-list '((-3 4 2)(9 10 2))) 2)))

|#


;;; ----------------------------------------------------------------------
;;;                                         Shift intervals


#| Shift an interval later or earlier in time to avoid overlapping a 
"forbidden" interval.

Case 1: Move to later time
----------------------------

Forbidden intervals: slew cannot occur during downlinks:
Overlap cases are:
         i1           i2 = downlink
          +--+--+--+--+
  --+--+--+--+--+--+--+--+--+--+--+--+--+--
Task end = t1, task+slew = t2
   t1     t2
  --|=====|--+--+--+--+--+--+--+--+--+--+-- no change
   t1           t2
  --|===========|--+--+--+--+--+--+--+--+-- shift by (i2-t1)
                   to |===========|
   t1                    t2
  --|====================|--+--+--+--+--+-- shift by (i2-t1)
                   to |====================|
                t1 t2
  --+--+--+--+--|==|--+--+--+--+--+--+--+-- shift by (i2-t1)
                   to |==|
                t1       t2
  --+--+--+--+--|========|--+--+--+--+--+-- shift by (i2-t1)
                   to |========|
                            t1    t2
  --+--+--+--+--+--+--+--+--|=====|--+--+-- no change

if t1 <= i1 and t2 > i1 then shift by i2-t1
if t1 >i1 and t1 < i2 then shift by i2-t1
 

Case 2: Move to earlier time
----------------------------

Overlap cases are:
                     i1           i2 = downlink start/end
                      +--+--+--+--+
  --+--+--+--+--+--+--+--+--+--+--+--+--+--
Task end = t1, task+slew = t2
               t1     t2
  --+--+--+--+--|=====|--+--+--+--+--+--+--+--+--+--+-- no change
               t1           t2
  --+--+--+--+--|===========|--+--+--+--+--+--+--+--+-- shift by -(t2-i1)
       to |===========|
               t1                    t2
  --+--+--+--+--|====================|--+--+--+--+--+-- shift by -(t2-i1)
 |====================|
                           t1 t2
  --+--+--+--+--+--+--+--+--|==|--+--+--+--+--+--+--+-- shift by -(t2-i1)
                to |==|
                           t1       t2
  --+--+--+--+--+--+--+--+--|========|--+--+--+--+--+-- shift by -(t2-i1)
          to |========|
                                       t1    t2
  --+--+--+--+--+--+--+--+--+--+--+--+--|=====|--+--+-- no change

if t2 >= i2 and t1 < i2 then shift by -(t2-i1)
if t2 < i2 and t2 > i1 then shift by -(t2-i1)

 |#


(defun SHIFT-INTERVAL-LATER-TO-AVOID-OVERLAP (t1 t2 no-overhead-intervals)

  "Input arguments:
    no-overhead-intervals - list of start-end lists representing times
     during which overhead is not permitted
     e.g. ((start1 end1)(start2 end2)...)
     Intervals must be in strictly INCREASING order.
    t1 and t2: interval start and end time.
   output:
    task-end-overhead-delay - time to delay t1 and t2 by in order to 
    avoid any overlap with any intervals in the no-overhead-intervals"
  
  (let* ((delay 0)
         )
    ;;(format t "~%t1,t2: ~a ~a" t1 t2)
    (do* ((intervals-list no-overhead-intervals (rest intervals-list))
          (next-interval (first intervals-list) (first intervals-list))
          (done nil))
         ((or done (not intervals-list)) nil)
      ;;(format t "~%next-: ~a" next-interval)
      (let ((i1 (first next-interval))
            (i2 (second next-interval)))
        (when  (or (and (<= t1 i1) (> t2 i1))
                   (and (>  t1 i1) (< t1 i2)))
          (setf delay (- i2 t1))
          ;;(format t "~% DELAY BY ~a" delay)
          (setf done t))
        (when (and (<= t1 i1) (<= t2 i1))
          (setf done t))
        ))
    
    delay))


(defun SHIFT-INTERVAL-EARLIER-TO-AVOID-OVERLAP (t1 t2 no-overhead-intervals)

  "Input arguments:
    no-overhead-intervals - list of start-end lists representing times
     during which overhead is not permitted
     e.g. ((start1 end1)(start2 end2)...)
     Intervals must be in strictly INCREASING order.
    t1 and t2: interval start and end time.
   output:
    task-end-overhead-delay - time to delay t1 and t2 by in order to 
    avoid any overlap with any intervals in the no-overhead-intervals.
    overhead will be NEGATIVE to indicate shift earlier in time."

  (let* ((delay 0)
         )
    ;;(format t "~%t1,t2: ~a ~a" t1 t2)
    (do* ((intervals-list no-overhead-intervals (rest intervals-list))
          (next-interval (first intervals-list) (first intervals-list))
          (done nil))
         ((or done (not intervals-list)) nil)
      ;;(format t "~%next-: ~a" next-interval)
      (let ((i1 (first next-interval))
            (i2 (second next-interval)))
        (when (or (and (>= t2 i2) (< t1 i2))
                  (and (<  t2 i2) (> t2 i1)))
          (setf delay (- i1 t2))
          ;;(format t "~% DELAY BY ~a" delay)
          (setf done t))
        
        ))
    
    delay))

#|
;; test case

(shift-interval-later-to-avoid-overlap 9 12 '((5 10)(15 20)))
(shift-interval-earlier-to-avoid-overlap 19 21 '((5 10)(15 20)))


(let ((first-start 2)
      (dur 2)
      (last-start 21)
      (intervals '((5 10)(15 20))))
  (loop-over-value-range
   t1 first-start last-start
   (let* ((t2 (+ t1 dur))
          (late-shift (shift-interval-later-to-avoid-overlap t1 t2 intervals))
          (early-shift (shift-interval-earlier-to-avoid-overlap t1 t2 intervals)))
     (format t "~% ~2d-~2d: late ~3d to ~2d-~2d early by ~3d to ~2d-~2d"
             t1 t2
             late-shift (+ t1 late-shift) (+ t2 late-shift)
             early-shift (+ t1 early-shift) (+ t2 early-shift)))))
|#

;;; ----------------------------------------------------------------------
;;;                                         Sky grid routines


(defvar *SKY-CELLS-PER-EDGE* 99
  "The number of cells along each edge of the celestial cube")


(defun 3VECT-TO-CELL-ID (r)
  (3cell-to-cell-ID (3vect-to-3cell r)))


(defun 3VECT-TO-3CELL (r)
  (let* ((max-component-value (apply #'max r))
         (min-component-value (apply #'min r))
         (max-component (position max-component-value r))
         (min-component (position min-component-value r))
         ;; rmax
         (face-component (if (> (abs max-component-value)
                                (abs min-component-value))
                             max-component
                             min-component))
         ;; value of rmax
         (face-component-value (elt r face-component))
         (face-number (case face-component
                            (0 (if (< face-component-value 0) 1 2))
                            (1 (if (< face-component-value 0) 3 4))
                            (2 (if (< face-component-value 0) 5 6))))
         ;; c1
         (first-coord 
           (rest (assoc face-number
                        '((1 . 2)(2 . 1)(3 . 0)(4 . 2)(5 . 1)(6 . 0)))))
         ;; c2
         (second-coord 
           (rest (assoc face-number
                        '((1 . 1)(2 . 2)(3 . 2)(4 . 0)(5 . 0)(6 . 1)))))
         (first-coord-value (elt r first-coord))
         (second-coord-value (elt r second-coord))
         (first-coord-cell (compute-face-cell-index 
                             (/ first-coord-value 
                                (abs face-component-value))))
         (second-coord-cell (compute-face-cell-index 
                              (/ second-coord-value 
                                 (abs face-component-value))))
         )
        
        ;(format t "~%max ~a (component ~a) min ~a (component ~a)"
        ;        max-component-value max-component
        ;        min-component-value min-component)
        ;(format t "~%face component ~a value ~a number ~a"
        ;        face-component face-component-value face-number)
        ;(format t "~%first cell component ~a value ~a cell ~a"
        ;        first-coord first-coord-value first-coord-cell)
        ;(format t "~%second cell component ~a value ~a cell ~a"
        ;        second-coord second-coord-value second-coord-cell)
        
        ;; returned result is list (face n1 n2)
        (list face-number first-coord-cell second-coord-cell)
        )
  )


(defun COMPUTE-FACE-CELL-INDEX (c)
  "convert face coordinates to cell number
    maps -1 to 1 and 1 to *SKY-CELLS-PER-EDGE*"
  (min (1+ (floor (* *SKY-CELLS-PER-EDGE* 
                     (/ (1+ c)
                        2))))
       *SKY-CELLS-PER-EDGE*))


(defun FACE-CENTER-FROM-CELL-INDEX (n)
  "inverse of compute-face-cell-index
    n runs from 1 to *SKY-CELLS-PER-EDGE*"
  (1- (/ (* 2 (1- n))
         *SKY-CELLS-PER-EDGE*)))
  

(defun 3CELL-TO-CELL-ID (cell-list)
  "convert cell triple to unique cell ID number"
  (let ((multiplier (1+ *SKY-CELLS-PER-EDGE*)))
    (+ (* (first cell-list) multiplier multiplier)
       (* (second cell-list) multiplier)
       (third cell-list))))


(defun CELL-ID-TO-3CELL (ID)
  "convert cell ID to list of face and cell numbers"
  (let* ((multiplier (1+ *SKY-CELLS-PER-EDGE*))
	 (face (floor ID (* multiplier multiplier)))
         (temp (mod ID (* multiplier multiplier)))
         (first-cell (floor temp multiplier))
         (second-cell (mod temp multiplier)))
    (list face first-cell second-cell)))


(defun 3CELL-TO-3VECT (cell-list)
  "convert 3cell list to normalized 3-vector
    if integral cell numbers are used the resulting vector
    is the 'lower/lower' corner of the cell
    to get the center of cell (f n1 n2), use
    (f (+ n1 .5) (+ n2 .5)) as the cell coords for this routine"
    (let ((face (first cell-list))
	  (c1 (second cell-list))
	  (c2 (third cell-list)))
      (3vect-normalize
	(case face
	      (1 (list -1 
		       (face-center-from-cell-index c2)
		       (face-center-from-cell-index c1)))
	      (2 (list 1 
		       (face-center-from-cell-index c1)
		       (face-center-from-cell-index c2)))
	      (3 (list (face-center-from-cell-index c1)
		       -1
		       (face-center-from-cell-index c2)))
	      (4 (list (face-center-from-cell-index c2)
		       1
		       (face-center-from-cell-index c1)))
	      (5 (list (face-center-from-cell-index c2)
		       (face-center-from-cell-index c1)
		       -1))
	      (6 (list (face-center-from-cell-index c1)
		       (face-center-from-cell-index c2)
		       1))
	      ))))


(defun 3CELL-TO-CENTER-3VECT (cell-list)
  (3cell-to-3vect (list (first cell-list)
                        (+ 0.5 (second cell-list))
                        (+ 0.5 (third cell-list)))))


(defun CELL-ID-TO-CENTER-3VECT (ID)
  "given cell ID, return vector to center of cell"
  ;; e.g. (cell-ID-to-center-3vect 15050)
  (3cell-to-center-3vect (cell-id-to-3cell id)))


#|  there was a problem with numerical roundoff on the TI:
    this routine could sometimes get the dot product of a
    vector with itself to be e.g. 1.00000002, which acos
    returns as complex.  Simple fix is to return zero
    immediately if the two cells are the same. |#


(defun 3CELL-SEPARATION (cell-list-1 cell-list-2)
  "returns angle (radians) between cells
    here taken to be between cell centers
    (but could be defined in some other way)"
  (cond ((equal cell-list-1 cell-list-2)
	 0)
	(t
	 (acos
	   (3vect-dot-product
	     (3cell-to-center-3vect cell-list-1)
	     (3cell-to-center-3vect cell-list-2))))))


(defun 3CELL-CORNER-VECTOR-LIST (cell-list)
  "return list of four corner vectors of a cell"
  (list (3cell-to-3vect cell-list)
        (3cell-to-3vect (list (first cell-list)
                              (1+ (second cell-list))
                              (third cell-list)))
        (3cell-to-3vect (list (first cell-list)
                              (1+ (second cell-list))
                              (1+ (third cell-list))))
        (3cell-to-3vect (list (first cell-list)
                              (second cell-list)
                              (1+ (third cell-list))))))


(defun SKY-CELL-NEIGHBORS (cell-list)
  "return list of cell lists for all neighbors of input cell"
  (let ((face (first cell-list))
        (c1 (second cell-list))
        (c2 (third cell-list))
        (N *SKY-CELLS-PER-EDGE*)
        )
       (case face
             (1 (odd-face-neighbors face c1 c2 N
                                    4 6 3 5))
             (3 (odd-face-neighbors face c1 c2 N
                                    6 2 5 1))
             (5 (odd-face-neighbors face c1 c2 N
                                    2 4 1 3))
             (2 (even-face-neighbors face c1 c2 N
                                    6 4 5 3))
             (4 (even-face-neighbors face c1 c2 N
                                    2 6 1 5))
             (6 (even-face-neighbors face c1 c2 N
                                    4 2 3 1)))))


(defun ODD-FACE-NEIGHBORS (face c1 c2 N north east south west)
  (let ((N-1 (1- N))
	(c1-1 (1- c1))
	(c1+1 (1+ c1))
	(c2-1 (1- c2))
	(c2+1 (1+ c2)))
    (cond 
      ;; corners
      ;; backquote templates below are cells read from left to right 
      ;; from diagrams in document
      ((and (= c1 1) (= c2 1))
       `((,west 2 1) (,face 1 2) (,face 2 2)
	 (,west 1 1) (,face 2 1)
	 (,south 1 1) (,south 1 2)))
      ((and (= c1 1) (= c2 N))
       `((,north 1 1) (,north 2 1)
	 (,west ,N 1) (,face 2 ,N)
	 (,west ,N-1 1) (,face 1 ,N-1) (,face 2 ,N-1)))
      ((and (= c1 N) (= c2 1))
       `((,face ,N-1 2) (,face ,N 2) (,east 1 2)
	 (,face ,N-1 1) (,east 1 1)
	 (,south 1 ,N-1) (,south 1 ,N)))
      ((and (= c1 N) (= c2 N))
       `((,north ,N-1 1) (,north ,N 1)
	 (,face ,N-1 ,N) (,east 1 ,N)
	 (,face ,N-1 ,N-1) (,face ,N ,N-1) (,east 1 ,N-1)))
      ;; edges:  3 cells on adjacent face listed first
      ((= c1 1)
       `((,west ,c2-1 1) (,west ,c2 1) (,west ,c2+1 1)
	 (,face 1 ,c2-1) (,face 1 ,c2+1)
	 (,face 2 ,c2-1) (,face 2 ,c2) (,face 2 ,c2+1)))
      ((= c1 N)
       `((,east 1 ,c2-1) (,east 1 ,c2) (,east 1 ,c2+1)
	 (,face ,N ,c2-1) (,face ,N ,c2+1)
	 (,face ,N-1 ,c2-1) (,face ,N-1 ,c2) (,face ,N-1 ,c2+1)))
      ((= c2 1)
       `((,south 1 ,c1-1) (,south 1 ,c1) (,south 1 ,c1+1)
	 (,face ,c1-1 1) (,face ,c1+1 1)
	 (,face ,c1-1 2) (,face ,c1 2) (,face ,c1+1 2)))
      ((= c2 N)
       `((,north ,c1-1 1) (,north ,c1 1) (,north ,c1+1 1)
	 (,face ,c1-1 ,N) (,face ,c1+1 ,N)
	 (,face ,c1-1 ,N-1) (,face ,c1 ,N-1) (,face ,c1+1 ,N-1)))
      ;; interior face cell
      (t `((,face ,c1-1 ,c2-1) (,face ,c1-1 ,c2) (,face ,c1-1 ,c2+1)
	   (,face ,c1 ,c2-1) (,face ,c1 ,c2+1)
	   (,face ,c1+1 ,c2-1) (,face ,c1+1 ,c2) (,face ,c1+1 ,c2+1))))))
    
    
(defun EVEN-FACE-NEIGHBORS (face c1 c2 N north east south west)
  (let ((N-1 (1- N))
	(c1-1 (1- c1))
	(c1+1 (1+ c1))
	(c2-1 (1- c2))
	(c2+1 (1+ c2)))
    (cond 
      ;; corners
      ;; backquote templates below are cells read from left to right 
      ;; from diagrams in document
      ((and (= c1 1) (= c2 1))
       `((,west ,N 2) (,face 1 2) (,face 2 2)
	 (,west ,N 1) (,face 2 1)
	 (,south 1 ,N) (,south 2 ,N)))
      ((and (= c1 1) (= c2 N))
       `((,north ,N 1) (,north ,N 2)
	 (,west ,N ,N) (,face 2 ,N)
	 (,west ,N ,N-1) (,face 1 ,N-1) (,face 2 ,N-1)))
      ((and (= c1 N) (= c2 1))
       `((,face ,N-1 2) (,face ,N 2) (,east 2 ,N)
	 (,face ,N-1 1) (,east 1 ,N)
	 (,south ,N-1 ,N) (,south ,N ,N)))
      ((and (= c1 N) (= c2 N))
       `((,north ,N ,N-1) (,north ,N ,N)
	 (,face ,N-1 ,N) (,east ,N ,N)
	 (,face ,N-1 ,N-1) (,face ,N ,N-1) (,east ,N-1 ,N)))
      ;; edges:  3 cells on adjacent face listed first
      ((= c1 1)
       `((,west ,N ,c2-1) (,west ,N ,c2) (,west ,N ,c2+1)
	 (,face 1 ,c2-1) (,face 1 ,c2+1)
	 (,face 2 ,c2-1) (,face 2 ,c2) (,face 2 ,c2+1)))
      ((= c1 N)
       `((,east ,c2-1 ,N) (,east ,c2 ,N) (,east ,c2+1 ,N)
	 (,face ,N ,c2-1) (,face ,N ,c2+1)
	 (,face ,N-1 ,c2-1) (,face ,N-1 ,c2) (,face ,N-1 ,c2+1)))
      ((= c2 1)
       `((,south ,c1-1 ,N) (,south ,c1 ,N) (,south ,c1+1 ,N)
	 (,face ,c1-1 1) (,face ,c1+1 1)
	 (,face ,c1-1 2) (,face ,c1 2) (,face ,c1+1 2)))
      ((= c2 N)
       `((,north ,N ,c1-1) (,north ,N ,c1) (,north ,N ,c1+1)
	 (,face ,c1-1 ,N) (,face ,c1+1 ,N)
	 (,face ,c1-1 ,N-1) (,face ,c1 ,N-1) (,face ,c1+1 ,N-1)))
      ;; interior face cell
      (t `((,face ,c1-1 ,c2-1) (,face ,c1-1 ,c2) (,face ,c1-1 ,c2+1)
	   (,face ,c1 ,c2-1) (,face ,c1 ,c2+1)
	   (,face ,c1+1 ,c2-1) (,face ,c1+1 ,c2) (,face ,c1+1 ,c2+1))))))



(defun SKY-CELL-NEIGHBORS-IN-REGION (cell-list region-size)
  "accumulates neighbor cells of the input cell 
    finds all cells within region-size of input cell
    see 3cell-separation for metric"
  (let* ((test-cell cell-list)
         (cells-to-check (list cell-list))
         (interior-cells '())
         )
        (do* ((current-cell (first cells-to-check) (first cells-to-check))
              (neighbors '() '())
              )
             ((null cells-to-check) ;termination
              interior-cells) ;returned value
             
             ;; pop current-cell off list to check
             (pop cells-to-check)
             
             ;(format t "~%checking ~a, sep ~a (~a)"
             ;        current-cell (3cell-separation test-cell current-cell)
             ;        (if (member current-cell interior-cells :test #'equal)
             ;            "known interior" "not decided"))
             
             ;; if the current cell is not already known to be an interior
             ;; cell and is within the region boundaries
             (when (and (not (member current-cell interior-cells :test #'equal))
                        (< (3cell-separation test-cell current-cell) 
                           region-size))
                   
                   ;; then add it to interior-cells
                   (push current-cell interior-cells)
                   ;(format t " --> interior")
                   
                   ;; and add it's neighbors to cells-to-check if they aren't
                   ;; already there and it they're not known to be interior
                   (setf neighbors (sky-cell-neighbors current-cell))
                   (dolist (n neighbors)
                           (if (not (member n interior-cells :test #'equal))
                               (pushnew n cells-to-check :test #'equal)))))))


(defun SKYGRID-STATISTICS (cells-per-edge)
  (let* ((*SKY-CELLS-PER-EDGE* cells-per-edge)
         (total-cells (* 6 cells-per-edge cells-per-edge))
         
         (corner (3vect-normalize '(1 1 1)))
         (corner-cell (3vect-to-3cell corner))
         (corner-cell-center (3cell-to-center-3vect corner-cell))
         (corner-cell-corners (3cell-corner-vector-list corner-cell))
         
         (center '(1 0 0))
         (center-cell (3vect-to-3cell center))
         (center-cell-center (3cell-to-center-3vect center-cell))
         (center-cell-corners (3cell-corner-vector-list center-cell))
         
         )
        (format t "~%Cells per edge: ~a" cells-per-edge)
        (format t "~%total # cells: ~a" total-cells)
        (format t "~%sterad/cell: ~a, sq deg/cell: ~a"
                (/ (* 4 pi) total-cells) (/ 41253.0 total-cells))
        
        (format t "~%corner cell ~a" corner-cell)
        (format t "~%  corner to corner: ~a ~a deg"
                (degrees (acos (3vect-dot-product 
                                 (first corner-cell-corners)
                                 (third corner-cell-corners))))
                (degrees (acos (3vect-dot-product 
                                 (second corner-cell-corners)
                                 (fourth corner-cell-corners)))))
        (format t "~%  center to corner: ")
        (dolist (c corner-cell-corners)
                (format t " ~a" (degrees (acos (3vect-dot-product 
                                                 corner-cell-center c)))))
        
        (format t "~%center cell ~a" center-cell)
        (format t "~%  corner to corner: ~a ~a deg"
                (degrees (acos (3vect-dot-product 
                                 (first center-cell-corners)
                                 (third center-cell-corners))))
                (degrees (acos (3vect-dot-product 
                                 (second center-cell-corners)
                                 (fourth center-cell-corners)))))
        (format t "~%  center to corner: ")
        (dolist (c center-cell-corners)
                (format t " ~a" (degrees (acos (3vect-dot-product 
                                                 center-cell-center c)))))))


(defun TEST-NEIGHBORS (face)
  (let* ((N 4)
         (*SKY-CELLS-PER-EDGE* N))
    (list
        (format nil "face: ~a" face)
        (format nil "corner 11: ~a" (sky-cell-neighbors `(,face  1  1)))
        (format nil "corner 1N: ~a" (sky-cell-neighbors `(,face  1 ,N)))
        (format nil "corner N1: ~a" (sky-cell-neighbors `(,face ,N  1)))
        (format nil "corner NN: ~a" (sky-cell-neighbors `(,face ,N ,N)))
        (format nil "edge c1=1: ~a" (sky-cell-neighbors `(,face  1  2)))
        (format nil "edge c1=N: ~a" (sky-cell-neighbors `(,face ,N  2)))
        (format nil "edge c2=1: ~a" (sky-cell-neighbors `(,face  2  1)))
        (format nil "edge c2=N: ~a" (sky-cell-neighbors `(,face  2 ,N)))
        (format nil "interior:  ~a" (sky-cell-neighbors `(,face  2  2)))
        )))


;;; ----------------------------------------------------------------------
;;;                                                            Root finder

;;; iterative root finder for f(x)=g between x=a and x=b
;;; convergence is by a relative tolerance on the x range
;;; uses inverse linear interpolation and bisection

;;(bisection-root 0.0 10.0 27 #'(lambda (x) (* x x x)))

(defun bisection-root (a b g func &optional (rel-tol 1.0e-6))
  (let ((func-a (funcall func a))
        (func-b (funcall func b)))
    ;; check that function changes sign from a to b
    (cond ((= func-a g)
           a)
          ((= func-b g)
           b)
          ((> (* (- func-a g) (- func-b g)) 0)
           ;;(format t "~% no root")
           nil)
          (t (do* ((fa func-a)
                   (fb func-b)
                   (guess (+ a (/ (* (- b a) (- g fa)) (- fb fa)))
                          (+ a (/ (* (- b a) (- g fa)) (- fb fa))))
                   (fguess (funcall func guess) (funcall func guess))
                   (done nil))
                  (done guess)
               ;;(format t "~% a ~a b ~a guess ~a" a b guess)
               
               (cond ((or (= fguess g)
                          (< (- b a) (* rel-tol guess)))
                      (setf done t))
                     ((< (* (- fguess g) (- fa g)) 0)
                      (setf b guess fb fguess))
                     ((< (* (- fguess g) (- fb g)) 0)
                      (setf a guess fa fguess))
                     (t (error "no convergence")))
               
               ;; bisect
               (let* ((mid (+ a (/ (- b a) 2)))
                      (fmid (funcall func mid)))
                 (cond ((< (* (- fmid g) (- fa g)) 0)
                        (setf b mid fb fmid))
                       ((< (* (- fmid g) (- fb g)) 0)
                        (setf a mid fa fmid))
                       (t nil)))
               
               )))))


;;; ----------------------------------------------------------------------
;;;                                              Solve quadratic equations

(defun SOLVE-QUADRATIC (a b c)
  
  "Solutions to the quadratic equation a x^2 + b x + c = 0.
    Multiple values are returned:
     root1 roo2 type
    where root1 and root2 are the two roots, and type is
    one of:  :real, :complex, :imaginary, :degenerate, :none.
    :degenerate means coeff a is zero, so equation is linear with one root.
    :none means both a and b are zero, so there is no solution.
    Both roots are nil if :none, second root is same as first if :degenerate."
  
  ;; test cases:  
  ;; (solve-quadratic 1 16 2) => -0.12599212598818887 -15.874007874011811 :REAL
  ;; (solve-quadratic 1 0 4) => #c(0 2) #c(0 -2) :IMAGINARY
  ;; (solve-quadratic 0 2 1) => -1/2 -1/2 :DEGENERATE
  ;; (solve-quadratic 0 0 2) => NIL NIL :NONE

  (cond 
   
   ;; no solution?
   ((and (= a 0) (= b 0))
    (values nil nil :none))

   ;; linear degenerate?
   ((= a 0)
    (let ((root (/ (- c) b)))
    (values root root :degenerate)))
   
   (t ;; non-degenerate case
    (let* ((discrim (- (* b b) (* 4 a c)))
           (sqrt-discrim (sqrt discrim))
           (-b (- b))
           (2a (* 2 a))
           (root1 (/ (+ -b sqrt-discrim) 2a))
           (root2 (/ (- -b sqrt-discrim) 2a)))
      (values 
       root1 root2
       (cond ((>= discrim 0) :real)
             ((and (< discrim 0) (= b 0)) :imaginary)
             (t :complex)))))))

#| 
;;; TEST QUADRATIC SOLVER:

(dolist (abc '((1 16 2)
               (12 1 4)
               (1 0 4)
               (0 2 1)
               (0 0 2)))
  (let ((a (first abc)) (b (second abc)) (c (third abc)))
    (multiple-value-bind
      (root1 root2 type)
      (solve-quadratic a b c)
      (format t "~%~%Coeff: a ~a, b ~a, c ~a" a b c)
      (format t "~%type: ~a  roots: ~a ~a" type root1 root2)
      (when (or (eq type :real) (eq type :complex) (eq type :imaginary))
        (format t "~%a x^2 + b x + c = ~a, ~a"
                (+ (* a root1 root1) (* b root1) c)
                (+ (* a root2 root2) (* b root2) c))))))
;; Results:
Coeff: a 1, b 16, c 2
type: REAL  roots: -0.12599212598818887 -15.874007874011811
a x^2 + b x + c = 1.7763568394002505E-15, 0.0

Coeff: a 12, b 1, c 4
type: COMPLEX  roots: #c(-0.041666666666666664 0.5758447900452189) 
 #c(-0.041666666666666664 -0.5758447900452189)
a x^2 + b x + c = #c(-8.881784197001252E-16 0.0), #c(-8.881784197001252E-16 0.0)

Coeff: a 1, b 0, c 4
type: IMAGINARY  roots: #c(0 2) #c(0 -2)
a x^2 + b x + c = 0, 0

Coeff: a 0, b 2, c 1
type: DEGENERATE  roots: -1/2 NIL

Coeff: a 0, b 0, c 2
type: NONE  roots: NIL NIL
|#


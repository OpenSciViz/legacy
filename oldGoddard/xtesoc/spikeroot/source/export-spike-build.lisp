;;; -*- Mode:Common-Lisp; Base:10 -*-
;;;
;;; export-spike-build.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: export-spike-build.lisp,v $
;;; Revision 3.1  1994/11/15 22:41:41  buehler
;;; Added *XTE-SLEW-SETUP-TIME*.
;;;
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.6  1992/10/05  19:49:31  johnston
;;; made default compilation with higher debug level (slower
;;; execution).
;;;
;;; Revision 1.5  1992/10/04  13:05:16  johnston
;;; many changes (including CLIM interface)
;;;
;;; Revision 1.4  1992/04/22  16:43:46  johnston
;;; new files for ROSAT, ground-based, data recorder constraint
;;; for AstroD.
;;;
;;; Revision 1.3  1991/12/12  12:31:44  johnston
;;; changed in-package statement for compatibility with Allegro 4.0
;;;
;;; Revision 1.2  1991/12/01  00:18:15  johnston
;;; fixed export for GINA UIF
;;;
;;; Revision 1.1  1991/11/26  15:39:12  johnston
;;; Initial revision
;;;
;;;
;;;
;;; ======================================================================
;;;                                                                PACKAGE

;;; ?? Should this be COMMON-LISP-USER?  Differences between Allegro
;;; 4.0 and 4.1 make this a problem, so leaving it USER for the present
;;; -MDJ 12/12/91

#+:Allegro
(in-package USER)

#+:CCL
(in-package CL-USER)


;;; ======================================================================
;;;                                                             *FEATURES*

;;; for compile-time conditioning:

(pushnew :export-spike *features*)


;;; ======================================================================
;;;                                              LOGICAL PATHNAME FACILITY

#+:ALLEGRO
(let ((EXCL::*ENABLE-PACKAGE-LOCKED-ERRORS* nil))
  (progn
    (if (not (probe-file "lib/sun-pathnames.fasl"))
	(compile-file "source/sun-pathnames.lisp" 
		      :output-file "lib/sun-pathnames.fasl"))
    (load "lib/sun-pathnames.fasl")))

;;; ======================================================================
;;;                                                      LOGICAL PATHNAMES
;;; logical pathname definitions: host is "spike"

#+:CCL
(setf (logical-pathname-translations "spike")
      '(
        ;; the next two can be different directories, if desired
        ("source;*" "Macintosh HD:spike:source:*.lisp")
        ("lib;*" "Macintosh HD:spike:lib:*.fasl")

        ;; Warning! note extra directory level required here.
        ;; Subdirectories of event-files contain directories named
        ;; by orbit model.
        ("event-files;*;*.*" "Macintosh HD:spike:event-files:*:*.*")

        ;; the next two can be different directories, if desired
        ("obs;*.*" "Macintosh HD:spike:obs:*.*")
        ("prep;*.*" "Macintosh HD:spike:obs:*.*")
        ("sched;*.*" "Macintosh HD:spike:sched:*.*")

        ;; new 10/8/92
        ("report;*.*" "Macintosh HD:spike:report:*.*")
	("temp;*.*" "Macintosh HD:spike:temp:*.*")
        ))

#+:ALLEGRO
(setf (logical-pathname-translations "spike")
      '(
        ;; the next two can be different directories, if desired
        ("source;*"          "source/*.lisp")
        ("lib;*"             "lib/*.fasl")

        ;; Warning! note extra directory level required here.
        ;; Subdirectories of event-files contain directories named
        ;; by orbit model.
        ("event-files;*;*.*" "event-files/*/*.*")

        ;; the next two can be different directories, if desired
        ("obs;*.*"           "obs/*.*")
        ("prep;*.*"          "obs/*.*")
        ("sched;*.*"         "sched/*.*")

        ;; new 10/8/92
        ("report;*.*"        "report/*.*")
	("temp;*.*"          "temp/*.*")
        ))
#| 
;;; TEST LOGICAL NAMES:  ON MAC
(translate-logical-pathname "spike:source;repair")
(translate-logical-pathname "spike:lib;repair")
(translate-logical-pathname "spike:event-files;euve;orbit.model")
(directory (translate-logical-pathname "spike:event-files;*;"))
(pathname-directory (translate-logical-pathname "spike:event-files;*;"))
(translate-logical-pathname "spike:obs;euve-test.targ")
(translate-logical-pathname "spike:obs;euve-test.prep")
;; this works on Mac and Sun, but is a hack.  Maybe when Sun comes out with
;; real logical pathname feature...
(let* ((pathmodel (translate-logical-pathname 
                   #+:ALLEGRO "spike:event-files;z;z.z"
                   #-:ALLEGRO "spike:event-files;*;"))
       (dirpath 
        #+:ALLEGRO
        (make-pathname :directory (butlast (pathname-directory pathmodel))
                               :name :wild)
        #-:ALLEGRO pathmodel))
  (mapcar #'(lambda (x) 
              #+:ALLEGRO (pathname-name x)
              #-:ALLEGRO (first (last (pathname-directory x))))
          (directory dirpath)))

;;; TEST LOGICAL NAMES:  ON SUN
;; this works on sun, not on Mac
(let ((pathmodel (translate-logical-pathname 
                  "spike:event-files;dummy;dummy.dummy")))
  (mapcar #'(lambda (x) (pathname-name x))
          (directory
           (make-pathname :directory (butlast (pathname-directory pathmodel))
                          :name :wild))))
(list-orbit-models)
(directory (orbit-root))
(get-orbit-model-pathname "euve")
(valid-orbit-model-p "euve")
|#

;;; ======================================================================
;;;                                                            MODULE LIST
;;; in order of compile/load


(defvar *export-spike-modules* nil)
(setf   *export-spike-modules*
        '(
          "general-utilities"
          "astro-utilities"
          ;;; "postscript-utilities" --defunct
          "gwindow"
          "postscript-gwindow"
          "astro-objects"
          "spacecraft"
          "saa"
          "ground-station"

          "fast-event"
          ;"hst-fast-event" ; not really needed for export, but useful for testing
          "event-ps-plot"
          
          "repair"
          "repair-capcon"
          "micro-spike"
          "short-term"
          "time-to-segments"
          "obs-sched"
          
	  "xte-fast-event"
          "xte-event-ps-plot"
          "xte-csp"
          ;; "xte-demo" - don't need to compile/load

          ;"rosat-fast-event"
          ;"rosat-event-ps-plot"
          ;"rosat-csp"
          ;; "rosat-demo" - don't need to compile/load

          ;"euve-fast-event"
          ;"euve-event-ps-plot"
          ;"euve-csp"
          ;; "euve-demo" - don't need to compile/load

          ;"gb-csp"
          ))

(if (find-package 'CLIM)
  (setf *export-spike-modules*
        (append *export-spike-modules*
                '(
                  ;; CLIM user I/F
                  "climif-package"
		  "clim-gwindow"
		  "climif-help"
                  "climif-main"
                  "climif-xte"
                  )))
  (format t "~% *~% * CLIM not available~% *"))


;;; ======================================================================
;;;                                         Pathnames


(defun EXPORT-SPIKE-SOURCE-PATHNAME (module)
  ;; (export-spike-source-pathname "general-utilities")
  (translate-logical-pathname 
   (format nil "spike:source;~a" module)))

(defun EXPORT-SPIKE-COMPILED-PATHNAME (module)
  ;; (export-spike-compiled-pathname "general-utilities")
  (translate-logical-pathname 
   (format nil "spike:lib;~a" module)))


;;; ======================================================================
;;;                                         Test for need to compile

(defun EXPORT-SPIKE-MODULE-NEEDS-TO-BE-COMPILED (module)
  
  ;; (export-spike-module-needs-to-be-compiled "general-utilities")
  
  (let* ((source-file (export-spike-source-pathname module))
         (comp-file (export-spike-compiled-pathname module))
         (comp-file-exists (probe-file comp-file))
         (source-time (file-write-date source-file))
         (comp-time (if comp-file-exists 
                      (file-write-date comp-file))))
    (or (null comp-file-exists)
        (< comp-time source-time))))


;;; ======================================================================
;;;                                         Compile and Load

(defun COMPILE-AND-LOAD-EXPORT-SPIKE (&key (debug t))
  ;; unconditional
  
  (cond (debug (proclaim '(optimize (speed 1)(safety 1)(space 1)(debug 2))))
	(t     (proclaim '(optimize (speed 3)(safety 0)(space 0)(debug 0)))))
  #+:Allegro (explain-compiler-settings)

  (dolist (module *export-spike-modules*)
    (let ((source (export-spike-source-pathname module))
          (compiled (export-spike-compiled-pathname module)))
      (format t "~%~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      (format t "~%+ compiling and loading: ~a" module)
      (format t "~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      (compile-file source :output-file compiled)
      (load compiled))))


(defun LOAD-EXPORT-SPIKE ()
  ;; load compiled files unconditionally
  (dolist (module *export-spike-modules*)
    (let ((compiled (export-spike-compiled-pathname module)))
      (cond ((probe-file compiled) (load compiled))
            (t (format t "~%*** module: ~a not compiled and not loaded"))))))


(defun INCREMENTAL-COMPILE-AND-LOAD-EXPORT-SPIKE (&key (debug t))

  (cond (debug (proclaim '(optimize (speed 1)(safety 1)(space 1)(debug 2))))
	(t     (proclaim '(optimize (speed 3)(safety 0)(space 0)(debug 0)))))
  #+:Allegro (explain-compiler-settings)

  (dolist (module *export-spike-modules*)
    (let ((source (export-spike-source-pathname module))
          (compiled (export-spike-compiled-pathname module)))
      (when (export-spike-module-needs-to-be-compiled module)
        (format t "~%~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        (format t "~%+ compiling and loading: ~a" module)
        (format t "~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        (compile-file source :output-file compiled)
        (load compiled)))))

(defun LOAD-EXPORT-SPIKE-COMPILE-IF-NEEDED (&key (debug t))
  ;; load, compile only if needed

  (cond (debug (proclaim '(optimize (speed 1)(safety 1)(space 1)(debug 2))))
	(t     (proclaim '(optimize (speed 3)(safety 0)(space 0)(debug 0)))))
  #+:Allegro (explain-compiler-settings)

  (dolist (module *export-spike-modules*)
    (let ((source (export-spike-source-pathname module))
          (compiled (export-spike-compiled-pathname module)))
      (when (export-spike-module-needs-to-be-compiled module)
        (format t "~%~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        (format t "~%+ compiling and loading: ~a" module)
        (format t "~%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        (compile-file source :output-file compiled))
      (load compiled))))


#| TEST:

(compile-and-load-export-spike :debug t)
(compile-and-load-export-spike :debug nil)
(load-export-spike)

(incremental-compile-and-load-export-spike :debug t)
(incremental-compile-and-load-export-spike :debug nil)
(load-export-spike-compile-if-needed)
|#

;;; ======================================================================
;;;                                                       SPIKE-DIAGNOSTIC

(defun SPIKE-DIAGNOSTIC (string &rest junk)
  (declare (ignore junk))
  (format t "~%~a" string))


;;; ======================================================================
;;;                                                SPIKE-DEFAULT-FILE-NAME

(defun SPIKE-DEFAULT-FILE-NAME (proposal version type &rest args)
  "return default filename of specified type"

  (declare (ignore proposal version type args))
  nil)


;;; ======================================================================
;;;                                                                   LOAD

(load-export-spike-compile-if-needed  :debug t)


;;; ======================================================================
;;;                                                       EXPORTED SYMBOLS

;;; what needs to be exported from user
;;; for xte
(export '(xte-targ-files
	  xte-prepped-targ-files
	  report-xte-schedule-times
	  report-xte-segment-times
	  xte-targ-file-needs-to-be-prepped
	  xte-init-prep-and-load
	  make-xte-task
	  xte-prep-file))

(export '(*XTE-SUITABILITY-SCALE*
	  *XTE-DEFAULT-PRIORITY*
	  *DEFAULT-XTE-ORBIT-MODEL*
	  *XTE-SCHEDULE-START*
	  *XTE-SCHEDULE-END*
	  *XTE-TIME-QUANTUM*
	  *XTE-SEGMENT-START*
	  *XTE-SEGMENT-DURATION*
	  *XTE-SEGMENT-COUNT*
	  *XTE-CAPACITY-UNIT*
	  *XTE-CAPACITY-UNIT*
	  *XTE-OVERSUBSCRIPTION-FACTOR*))

(export '(*XTE-SHORT-TERM-TIME-QUANTUM*
	  *XTE-SLEW-RATE* 
	  *XTE-SLEW-SETUP-TIME*
	  *XTE-ABS-ADJUST*
	  *XTE-REL-ADJUST*
	  *XTE-MIN-SEGMENT-SIZE*))

(export '(*XTE-LONG-TERM-SCHEDULE*  
	  *XTE-SHORT-TERM-SCHEDULE*))

(export '(*DEFAULT-XTE-SUN-ANGLE-TABLE*
	  *DEFAULT-XTE-SUN-SAMPLE-INTERVAL*
	  *DEFAULT-XTE-MOON-ANGLE-TABLE*
	  *DEFAULT-XTE-MOON-SAMPLE-INTERVAL*
	  *DEFAULT-XTE-BRIGHT-LIMB-ANGLE*
	  *DEFAULT-XTE-DARK-LIMB-ANGLE*
	  *DEFAULT-XTE-DARK-FLAG*
	  *DEFAULT-XTE-NORTH-FLAG*
	  *DEFAULT-XTE-SOUTH-VIEWING-FACTOR*
	  *DEFAULT-XTE-MIN-VIEWING-TIME*
	  *DEFAULT-XTE-VIEWING-TABLE*
	  *DEFAULT-XTE-ORB-VIEWING-SAMPLE-INTERVAL*))

;;; for EUVE
(export '(euve-targ-files
	  euve-prepped-targ-files
	  report-euve-schedule-times
	  report-euve-segment-times
	  euve-targ-file-needs-to-be-prepped
	  euve-init-prep-and-load
	  make-euve-task
	  euve-prep-file))

(export '(*EUVE-SCHEDULE*
	  *EUVE-SUITABILITY-SCALE*
	  *EUVE-DEFAULT-PRIORITY*
	  *EUVE-SCHEDULE-START*
	  *EUVE-SCHEDULE-END*
	  *EUVE-TIME-QUANTUM*))

;;; general
(export '(early-greedy-schedule 
	  max-pref-schedule 
	  elminc-schedule
	  obs-csp obs-chronika
	  schedule-report 
	  csp-state-summary
	  list-orbit-models))

;;; Exported utilities
(export '(is-leap-year
	  dmy-to-time
	  day-month-year-to-day-of-year
	  month-string-to-month-number
	  month-number-to-month-string
	  *days-in-month*
	  jdut
	  format-abs-time
	  utjd
	  pcf-to-interval-list
	  pcf-from-interval-list
	  pcf-set-value
	  fractional-day-to-hour-minute-second))




;;; ======================================================================
;;;                                 COMPARISON WITH SPIKE LIBRARY VERSIONS


#| === setup script ===

;; the ScI library
(init-spike-library-list
 '("/cerb/p1/develop/spike" "/cerb/p1/develop/utilities"))
;; a downloaded copy from MIT
(init-spike-library-list '("/grendel/data1/mit"))

(chdir "/grendel/data1/oct92")
(init-temp-spike-library-list)

(compare-with-spike-lib :max-lines 50)


Sample output from above:


SPIKE library recorded - 497 files---------------------------------------------------------------------

Temp library recorded - 162 files---------------------------------------------------------------------

astro-objects.lisp has rcs version ;;; Revision 1.3  1991/12/01  00:11:52  johnston
  but library is ;;; Revision 1.4  1991/12/12  18:05:46  giuliano
astro-utilities.lisp has rcs version ;;; Revision 1.51  1991/12/01  00:13:47  johnston
  but library is ;;; Revision 1.52  1991/12/18  16:01:04  giuliano
event-ps-plot.lisp has rcs version ;;; Revision 1.14  1991/12/01  00:18:40  johnston
  but library is ;;; Revision 1.16  1991/12/19  16:50:00  sponsler
hst-fast-event.lisp has rcs version ;;; Revision 1.1  1991/11/26  15:40:41  johnston
  but library is ;;; Revision 1.2  1991/11/27  22:48:11  giuliano
Of 162 files in temp area
  66 had types to be ignored
  63 of the remainder are not in the Spike library
  Of those in the library, 4 are different, 29 are the same
Files in temp area, not in library:
  obs/xray9.targ
  obs/xray8.targ
  obs/xray7.targ
  obs/xray6.targ
  obs/xray5.targ
  obs/xray4.targ
  obs/xray3.targ
  obs/xray2.targ
  obs/xray10.targ
  obs/xray1.targ
  obs/euve-fake3.targ
  obs/euve-fake2.targ
  obs/euve-fake1.targ
  source/export-spike-build.lisp~
  source/#obs-sched.lisp#
  ./#spike-lib-diff.lisp#
  ./#junktmp.tmp#
  ./spike-lib-diff.lisp~
  ./spike-lib-diff.lisp
  ./oku.tmp
  ./junktmp.tmp
  ./temp.lisp
  uif/obss.tmp
  uif/gina-uih.lisp~
  uif/sched-lt-auto.lisp~
  uif/sched-st-auto.lisp~
  uif/foofoo.tmp
  uif/euve/euve-sched-st-auto.lisp
  uif/euve/euve-uif-build.lisp
  uif/euve/euve-sched-st-obs-sort.lisp
  uif/euve/euve-sched-st-obs-criteria.lisp
  uif/euve/euve-sched-short-term.lisp
  uif/euve/euve-sched-main-shell.lisp
  uif/euve/euve-sched-app.lisp
  uif/euve/euve-uif.lisp
  uif/xte-uif-build.lisp
  uif/uih-protocol2.text
  uif/notes-oct28.text
  uif/notes-oct22.text
  uif/notes-oct21.text
  uif/gina-patch.text
  uif/gina-notes.text
  uif/sched-triple-editor.lisp
  uif/sched-st-obs-sort.lisp
  uif/sched-st-obs-criteria.lisp
  uif/sched-st-auto.lisp
  uif/sched-short-term.lisp
  uif/todo.text
  uif/sched-prep-param-dialog.lisp
  uif/sched-main-shell.lisp
  uif/sched-lt-param.lisp
  uif/sched-lt-obs-sort.lisp
  uif/sched-lt-obs-criteria.lisp
  uif/sched-lt-auto.lisp
  uif/sched-long-term.lisp
  uif/sched-date-dialog.lisp
  uif/sched-constraint-dialog.lisp
  uif/sched-app.lisp
  uif/postx-utilities.lisp
  uif/instance-editor2.lisp
  uif/gina-uih.lisp
  uif/xte-uif.lisp
  uif/xte-csp-delta.lisp

   === end setup script ===|#

#+:Allegro
(defun COMPARE-WITH-SPIKE-LIB (&key (ignore-types '("fasl" "prep" "prepc" "model"))
				    (max-lines 30))
  
  (let ((not-in-libe nil)
	(changed-files nil)
	(files-with-ignored-types nil)
	(unchanged-files nil))
    
    (dolist (temp-file *temp-spike-libe*)
      (let* ((name (pathname-name temp-file))
	     (type (pathname-type temp-file))
	     (ignored-type (member type ignore-types :test #'string-equal))
	     (different nil))
	(if ignored-type (push temp-file files-with-ignored-types))
	(unless ignored-type
	  (let ((lib-file (in-spike-library-p temp-file)))
	    (cond (lib-file
		   ;; temp-file is pathname of file in temporary tree
		   ;; lib-file is pathname of file in library

		   ;; check RCS version is the same
		   (let ((rcs-libe (get-rcs-version lib-file))
			 (rcs-temp (get-rcs-version temp-file)))
		     (unless (string-equal rcs-libe rcs-temp)
		       ;; rcs versions are different
		       (setf different t)
		       (format t "~%~%RCS VERSION DIFFERENCE: ~a.~a" name type)
		       (format t "~%  temporary is   ~a" rcs-temp)
		       (format t "~%  but library is ~a" rcs-libe)))

		   ;; do actual file diff
		   (let ((file-different
			  (check-for-file-differences
			   lib-file temp-file :max-lines max-lines))
			 (counter 0))
		     (when file-different
		       (setf different t)
		       (format t   "~%~%FILE CONTENT CHANGE: ~a.~a" name type)
		       (dolist (line file-different)
			 (format t "~%  +++ ~a" line)
			 (incf counter)
			 (when (> counter max-lines)
			   (format t "~%  +++ etc. etc. ...")
			   (return nil)))))
		   
		   ;; note if different
		   (cond (different (push lib-file changed-files))
			 ((not different) (push lib-file unchanged-files)))

		   )
		  (t (push temp-file not-in-libe)))
	    ))))


    (format t "~%Of ~d files in temp area"
	    (list-length *temp-spike-libe*))
    (format t "~%  ~d had types to be ignored" (list-length files-with-ignored-types))
    (format t "~%  ~d of the remainder are not in the Spike library"
	    (list-length not-in-libe))
    (format t "~%  Of those in the library, ~d are different, ~d are the same"
	    (list-length changed-files)
	    (list-length unchanged-files))
    (format t "~%Files in temp area, not in library:")
    (dolist (item not-in-libe)
      (format t "~%  ~a" item))

    ))
    

#+:Allegro
(defun CHECK-FOR-FILE-DIFFERENCES (file1 file2 &key (temp-file "junk-diff.tmp") (max-lines 20))
  "diff the files, return list of difference lines"
  (let ((outfile temp-file)
	(result nil)
	(counter 0))

    (if (probe-file outfile) (delete-file outfile))
    
    (run-shell-command
     ;; "diff  \"/grendel/data1/aug92/source/general-utilities.lisp\" \"/cerb/p1/develop/utilities/general-utilities.lisp\""
     (format nil "diff ~s ~s" (namestring file1) (namestring file2))
     :output outfile)
    
    (with-open-file (stream temp-file
		     :direction :input)
      (loop
	(let ((line (read-line stream nil nil)))
	  (cond (line
		 (push line result)
		 (incf counter))
		(t (return nil))))
	(if (> counter max-lines) (return nil))))

    (nreverse result)))

  
  
#+:Allegro
(defvar *temp-spike-libe* nil)

#+:Allegro
(defun INIT-TEMP-SPIKE-LIBRARY-LIST (&optional (root "."))
  
  "init temp spike library list:  set the global *temp-spike-libe*
    to a list of files in the tree starting at the specified root"

  (setf *temp-spike-libe* nil)
  (setf *temp-spike-libe*
    (recurse-dir root))
  (format t "~%Temp library recorded - ~d files"
	  (list-length *temp-spike-libe*)))


#+:Allegro
(defvar *spike-libe* nil)

#+:Allegro
(defun INIT-SPIKE-LIBRARY-LIST (roots)
  
  "init spike library file list.  Input is list of root directories.
    Global *spike-libe* is used for results"

  (setf *spike-libe* nil)
  (dolist (root roots)
    (setf *spike-libe*
      (append *spike-libe*
	      (recurse-dir root))))
  (format t "~%SPIKE library recorded - ~d files"
	  (list-length *spike-libe*))
  nil)


#+:Allegro
(defun IN-SPIKE-LIBRARY-P (file)
  
  "return pathname if file is in spike library, nil otherwise.  
    if multiple files in library, put out warning and return
    first."

  (let ((name (pathname-name file))
	(type (pathname-type file))
	(result nil))

    ;; look in list for files that match
    (dolist (lib-file *spike-libe*)
      (when (and (equal name (pathname-name lib-file))
		 (equal type (pathname-type lib-file)))
	(push lib-file result)))

    ;; return
    (cond ((and result (= (list-length result) 1))
	   (first result))
	  ((and result (> (list-length result) 1))
	   (warn "More than one file matches in library ~a.~a" name type)
	   (first result))
	  ((null result)
	   nil)
	  (t (error "dropped through test")))))


#+:Allegro
(defun GET-RCS-VERSION (file)

  "input filename, return string with rcs version number in it, e.g.
    (get-rcs-version \"/cerb/p1/develop/utilities/astro-utilities.lisp\")
    returns:
    \";;; Revision 1.52  1991/12/18  16:01:04  giuliano\" "

  (with-open-file (stream file
		   :direction :input
		   :if-does-not-exist :error)
    (let ((counter 0)
	  line)
      (loop
	  (setf line (read-line stream nil nil))
	(if (not line) (return nil))
	(when (search "$Log: " line)
	  ;; next line is revision
	  (return (read-line stream)))
	(incf counter)
	(if (> counter 100) (return nil))))))


#+:Allegro
(defun RECURSE-DIR (&optional (root "."))

  "return all files in directory tree starting at root,
    e.g. (recurse-dir \"/cerb/p1/spike/\") => all files in tree
    as pathnames"

  (let ((result nil)
	(contents (directory root :directories-are-files t)))
    (dolist (item contents)
      
      ;;(format t "~%~a ~a ~a" (pathname-directory item) (pathname-name item) (pathname-type item))
      
      (cond ((null (pathname-type item))
	     (let ((dir 
		    (namestring
		     (make-pathname :directory
				    (append (pathname-directory item)
					    (list (pathname-name item)))))))
	       (setf result (append (recurse-dir dir) result))
	       ;;(format t "~%***  <~a>" dir)
	       ))
	    
	    (t (setf result (cons item result)))))
    
    result))







#| Misc junk...

(dolist (path (directory "/grendel/data1/nov91/source/"))
  (when (string-equal "lisp" (pathname-type path))
    (print path)))

(format-universal-time 
 (file-write-date "/grendel/data1/nov91/source/astro-utilities.lisp"))

(run-shell-command "who" :output "/grendel/data1/nov91/junktmp.tmp")

(check-for-file-differences "/grendel/data1/aug92/source/event-ps-plot.lisp" "/cerb/p1/develop/spike/events/event-ps-plot.lisp")
			       
;;; OK THIS WORKS!!
(let ((outfile  "/grendel/data1/aug92/junktmp.tmp"))
  (if (probe-file outfile) (delete-file outfile))
  (run-shell-command
   "diff  \"/grendel/data1/aug92/source/event-ps-plot.lisp\" \"/cerb/p1/develop/spike/events/event-ps-plot.lisp\""
   :output outfile))
(let ((outfile  "/grendel/data1/aug92/junktmp.tmp"))
  (if (probe-file outfile) (delete-file outfile))
  (run-shell-command
   "diff  \"/grendel/data1/aug92/source/time-to-segments.lisp\" \"/cerb/p1/develop/spike/extensions/time-to-segments.lisp\""
   :output outfile))
(with-open-file (stream "/grendel/data1/aug92/junktmp.tmp")
  (file-length stream))
 
(directory "/cerb/p1/develop/spike/" :directories-are-files t)

(recurse-dir "/cerb/p1/develop/spike/")

(recurse-dir "/grendel/data1/nov91/source/")

(search "foo" "bogus fo none")
(get-rcs-version "/grendel/data1/nov91/source/astro-utilities.lisp")
(get-rcs-version "/cerb/p1/develop/utilities/astro-utilities.lisp")
(get-rcs-version "/cerb/p1/develop/utilities/astro-utilities.lisp")

(directory ".")
(recurse-dir ".")

(in-spike-library-p "xray9.targ")
|#

;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; fast-event.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: fast-event.lisp,v $
;;; Revision 4.0  1995/01/16 16:09:42  buehler
;;; Minor reformatting.
;;;
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.33  1991/12/01  00:19:05  johnston
;;; minor changes
;;;
;;; Revision 1.32  1991/11/26  15:38:03  johnston
;;; major rework; changes described in memo 25Nov91 MDJ
;;;
;;; Revision 1.31  1991/10/15  17:25:49  sponsler
;;; OPR 21768: DIAGNOSTIC changed to SPIKE-DIAGNOSTIC
;;;
;;; Revision 1.30  1991/09/04  18:16:25  sponsler
;;; STIF3 Changes
;;;
;;; Revision 1.29  1991/08/23  20:52:05  giuliano
;;; Made changes to handle missing ephemeris data for moving targets.
;;; Fixed logic for setting the viewing-time-constraint parameter.
;;;
;;; Revision 1.28  91/08/21  11:30:48  giuliano
;;; Updated the event calculator to handle moving targets.  Should allow
;;; existing code to use fixed target routines without any change. Added
;;; routines to integrate orbital event pcfs with moving target visibility
;;; windows.
;;; 
;;; Revision 1.27  91/06/05  22:48:20  johnston
;;; minor fixes
;;; 
;;; Revision 1.26  91/04/28  17:59:47  johnston
;;; fixed problem with *event-calc-type* being overridden by EUVE
;;; event calculator.  Now type is explicit.
;;; 
;;; Revision 1.25  91/03/28  09:22:07  johnston
;;; fixed but in compute-orb-viewing-pcf when time was out of orbit
;;; model range
;;; 
;;; Revision 1.24  91/03/25  13:21:59  johnston
;;; improvements, major.
;;; 
;;; Revision 1.23  91/03/18  23:26:23  johnston
;;; changed defmethod to defun for set-sensor-limits...
;;; 
;;; Revision 1.22  91/03/18  22:23:16  johnston
;;; major restructuring
;;; added sensor and orientation pcf calculation
;;; 
;;; Revision 1.21  90/12/07  07:25:48  krueger
;;; Converted Flavor code to CLOS.  Many changes.
;;; 
;;; Revision 1.20  90/08/22  11:17:45  vick
;;; fixed the zod light event calc bug by altering the functions
;;; zodiacal-light-constraint-pcf and write-calls-from-event-hash-table.
;;; 
;;; Revision 1.19  90/05/29  07:43:24  vick
;;; corrected bugged write-calls-from-event-hash-table
;;; 
;;; Revision 1.18  90/05/28  19:52:12  vick
;;; minor cleanup
;;; 
;;; Revision 1.17  90/05/28  19:33:01  vick
;;; minor changes
;;; 
;;; Revision 1.16  90/05/28  19:18:28  vick
;;;  small changes
;;; 
;;; Revision 1.15  90/05/28  19:09:11  vick
;;; minor changes
;;; 
;;; Revision 1.14  90/05/28  19:00:15  vick
;;; minor fixes
;;; 
;;; Revision 1.13  90/05/28  18:30:39  vick
;;;  re-eng. and consolidation w/ old event calc.
;;; 
;;; Revision 1.12  90/04/30  12:45:46  miller
;;; commented out debugging format statements
;;; 
;;; Revision 1.11  90/04/29  20:09:08  johnston
;;; changed rationalize to rational to workaround TI bug
;;; 
;;; Revision 1.10  90/04/29  02:05:28  johnston
;;; minor fixes for CVZ constraints
;;; 
;;; Revision 1.9  90/04/29  01:11:00  johnston
;;; fixed compiler warning
;;; 
;;; Revision 1.8  90/04/29  01:07:28  johnston
;;; fixed problem with rationalized number lists
;;; added CVZ handling for min-viewing
;;; 
;;; Revision 1.7  90/04/29  00:18:16  johnston
;;; revised dark, min-viewing, saa constraints
;;; 
;;; Revision 1.6  90/04/28  23:11:45  johnston
;;; added SAA avoidance
;;; 
;;; Revision 1.5  90/04/27  18:58:55  johnston
;;; more fixes to cache writer
;;; 
;;; Revision 1.4  90/04/27  16:42:15  johnston
;;; fixed cache writer so equal tests true
;;; 
;;; Revision 1.3  90/04/27  14:01:23  johnston
;;; reformated saved event file
;;; 
;;; Revision 1.2  90/04/24  15:07:54  johnston
;;; added caching by proposal
;;; 
;;; Revision 1.1  90/04/12  18:42:38  johnston
;;; Initial revision
;;; 

;;; ----------------------------------------------------------------------
;;;                                         G L O B A L S

;;; --- none

;;; ----------------------------------------------------------------------
;;;                                    Event calculation class definitions

(defclass EVENT-CALC-POINTING ()
  (
   (name
    :initform nil :initarg :name :accessor name
    :documentation 
    "name of object determining pointing, for display only")
   
   ;; the target
   (astro-object
    :initform nil :initarg :astro-object :accessor astro-object
    :documentation
    "instance of astronomical object (fixed or moving), used for calls to
        orbit and visibility calculation routines")
   
   ;; the observer
   (observer-name 
    :initform nil :accessor observer-name
    :documentation
    "name of observer (string, or stringable object).  Used to update
      observer instance after new orbit model is loaded")
   (observer
    :initform *ST* :initarg :observer :accessor observer
    :documentation 
    "The observing object.  Usually this should be an instance of
      low-earth-orbit-satellite class, e.g. HST.")
   )
  (:documentation 
   "mixin for representing event calculation pointing parameters"))


(defclass EVENT-CALC-DATES ()
  (
   ;; dates
   (start-date
    :initform nil :initarg :start-date :accessor start-date
    :documentation 
    "start date for event calculation, TJD")
   (end-date
    :initform nil :initarg :end-date :accessor end-date
    :documentation 
    "end date for event calculation, TJD")
   (default-sample-interval
     :initform nil :accessor default-sample-interval
     :documentation 
     "default sample interval, days")
   (default-sample-count
     :initform nil :accessor default-sample-count
     :documentation 
     "number of samples in range start to end")
   )
  (:documentation 
   "mixin for representing event calculation date parameters"))
  


(defclass EVENT-CALC-CACHE ()
  (
   (EC-cache
    :initform (make-hash-table :test #'equal)
    :accessor EC-cache
    :documentation 
    "hash table, key specification of constraint, value is pcf")
   (save-EC-cache
    :initform nil :accessor save-EC-cache
    :documentation
    "flag t if cache needs to be saved, nil otherwise")
   (EC-cache-calls :initform 0 
                   :documentation "number of calls to retrieve from cache")
   (EC-cache-hits :initform 0 :documentation "number of calls with hits")
   )
  (:documentation
   "mixin for event-calculation cache"))


;;; ----------------------------------------------------------------------
;;; Each of the following can assume that they occur mixed in with 
;;; EVENT-CALC-POINTING, EVENT-CALC-DATES, and EVENT-CALC-CACHE
;;; ----------------------------------------------------------------------

(defclass EVENT-CALC-ORB-VIEWING ()
  (
   ;; constraint parameters
   
   (orbit-model
    :initform nil :initarg :orbit-model :accessor orbit-model
    :documentation 
    "orbit model name")
   
   (orb-viewing-sample-interval
    :initform nil :documentation
    "interval in days at which orbital viewing constraints are sampled")
   
   ;; ----------------------------------------------------------------------
   ;; parameters to determine observing time available in an orbit
   ;; ----------------------------------------------------------------------
   (bright-limb-angle
    :initform nil :initarg :bright-limb-angle :accessor bright-limb-angle
    :documentation 
    "angle to bright earth, radians, or nil if no bright earth constraint")
   (dark-limb-angle
    :initform nil :initarg :dark-limb-angle :accessor dark-limb-angle
    :documentation 
    "angle to dark limb, radians, or nil if no limb angle constraint")
   (terminator-angle
    :initform (radians 90) :initarg terminator-angle :accessor terminator-angle
    :documentation
    "Angle from sun to points on earth surface considered to be on terminator.
      Radians. Nominally 90 deg for a geometric earth; may be larger")
   
   ;; ----------------------------------------------------------------------
   ;; shadow viewing parameters
   ;; ----------------------------------------------------------------------
   (dark
    :initform nil :initarg :dark :accessor dark
    :documentation 
    "flag, t if dark only, nil if don't care")
   (dark-sun-limb-angle
    :initform (radians -0.25) :initarg :dark-sun-limb-angle :accessor dark-sun-limb-angle
    :documentation
    "limb angle of sun below which satellite is in shadow. + is above
      horizon, - below, in radians.  Nominal size of sun is 0.5 deg, so
      this angle defaults to -0.25 deg")
   
   ;; ----------------------------------------------------------------------
   ;; northern hemisphere viewing (SAA avoidance)
   ;; ----------------------------------------------------------------------
   (northern-hemisphere-only
    :initform nil :initarg :northern-hemisphere-only :accessor northern-hemisphere-only
    :documentation 
    "t if northern hemisphere visibility only, else nil if don't care")
   
   ;; ----------------------------------------------------------------------
   ;; accuracy control
   ;; ----------------------------------------------------------------------
   (high-accuracy
    :initform nil :initarg :high-accuracy :accessor high-accuracy
    :documentation 
    "flag: t if high accuracy is required, nil otherwise")
   
   ;; ----------------------------------------------------------------------
   ;; requested vs. available viewing time
   ;; ----------------------------------------------------------------------
   (viewing-time
    :initform nil :initarg :viewing-time :accessor viewing-time
    :documentation
    "Requested observing time, days.  May be interruptible or not (see 
      slot 'interruptible.")
   (interruptible
    :initform nil :initarg :interruptible
    :accessor interruptible
    :documentation
    "Flag, nil means viewing-time must be obtained in one uninterrupted 
      visibility period, t means that viewing-time may be obtained from 
      multiple interrupted segments.  If interruptible, then 'min-viewing-time,
      if non-nil, gives minimum size of interrupted segment")
   (min-viewing-time
    :initform nil :initarg :min-viewing-time :accessor min-viewing-time
    :documentation 
    "The MINIMUM size of an orbital visibility interval, days.
      Applies only to interruptible observations, since for non-interruptible
      ones the 'viewing-time slot contains the duration.")
   (viewing-table
    :initform nil :initarg :viewing-table :accessor viewing-table
    :documentation 
    "table of suitability as function of ratio of 'viewing-time 
       or 'min-viewing-time value to size of orbital visibility interval.")
   
   ;; ----------------------------------------------------------------------
   ;; sensor constraint angles (star trackers, etc.)
   ;; ----------------------------------------------------------------------
   ;; sensor constraint angles
   (sensor-limits
    :initform (make-hash-table :test #'equal)
    :documentation
    "hash table, key is sensor NAME, value is list of constraint angles
      as a plist as follows:
        (:sun <ang> :moon <ang> :bright-limb <ang> :dark-limb <ang>)
      All angles in radians.")
   )
  (:documentation
   "mixin for representing orbital viewing constraint parameters"))


(defclass EVENT-CALC-ZODIACAL-LIGHT ()
  (
   (zodiacal-light-sample-interval
    :initform nil :documentation
    "sample interval in days")
   (zodiacal-light-constraint
    :initform nil :initarg :zodiacal-light-constraint :accessor zodiacal-light-constraint
    :documentation 
    "t if constrained by zodiacal light, nil otherwise")
   (zod-light-wavelength
    :initform nil :initarg :zod-light-wavelength :accessor zod-light-wavelength
    :documentation 
    "wavelength for zodiacal light calculation, nil if no constraint ???")
   (zod-light-brightness
    :initform nil :initarg :zod-light-brightness :accessor zod-light-brightness
    :documentation 
    "brightness for zodiacal light calculation, nil if no constraint, units ???")
   )
  (:documentation
   "mixin for representing zodiacal light constraint"))


(defclass EVENT-CALC-SUN ()
  (
   (sun-sample-interval
    :initform nil :documentation
    "sample interval in days")
   (sun-constraint
    :initform nil :initarg :sun-constraint :accessor sun-constraint
    :documentation 
    "t if sun constraint to be calculated, nil otherwise")
   (sun-suit-table
    :initform nil :initarg :sun-suit-table :accessor sun-suit-table
    :documentation 
    "sun angle suitability table")
   )
  (:documentation
   "mixin for sun avoidance constraint"))


(defclass EVENT-CALC-MOON ()
  (
   (moon-sample-interval
    :initform nil :documentation
    "sample interval in days")
   (moon-constraint
    :initform nil :initarg :moon-constraint :accessor moon-constraint
    :documentation 
    "t if moon constraint to be calculated, nil otherwise")
   (moon-suit-table
    :initform nil :initarg :moon-suit-table :accessor moon-suit-table
    :documentation 
    "moon angle suitability table")
   )
  (:documentation
   "mixin for moon avoidance constraint"))



;;; ----------------------------------------------------------------------
;;;                                        Utilities for constraint tables
;;; ----------------------------------------------------------------------

;;; in several places lookup tables are used for constraint specification.
;;; these are provided on input as lists of triples:
;;;  ((t1 t2 val1) (t3 t4 val2)...
;;; t1 etc. are usually times, but may be angles - see below.


(defun VALIDATE-CONSTRAINT-INTERVAL-TABLE (table)
  
  "check a table supposed to be in the form:
    ((t1 t2 val1) (t3 t4 val1) ...)
   return t if ok, nil otherwise.  This only checks that the input is a 
   list of triples."
  ;;(validate-constraint-interval-table '((0 1 2)(1 2 4)(2 3 9)))
  ;;(validate-constraint-interval-table 'abc)
  ;;(validate-constraint-interval-table '((0 1 2) 1 2 3))
  
  (cond ((listp table)
         (dolist (item table)
           (if (or (not (listp item))
                   (not (= (list-length item) 3)))
             (return-from validate-constraint-interval-table nil)))
         t ; list is ok
         )
        ;; error in format
        (t nil)))


(defun CONVERT-ANGLE-TABLE-TO-PCF (table)

  "Convert angle table from degrees in interval list form to radians in pcf form.
    input is table of form:  ((ang1 ang2 val1)(ang3 ang4 val2)...) where ang are degrees.
    returned value is PCF with angles in RADIANS"
  ;;(convert-angle-table-to-pcf '((0 20 1)(20 40 0)(40 180 1)))

  (let ((interval-list nil))
    (dolist (entry table)
      (push (list (radians (first entry))
		  (radians (second entry))
		  (third entry))
	    interval-list))
    (pcf-from-interval-list (reverse interval-list))))



;;; ----------------------------------------------------------------------
;;;                                        Methods for EVENT-CALC-POINTING
;;; ----------------------------------------------------------------------

	

(defmethod SET-TARGET-FOR-EVENT-CALCULATION 
  ((ECP event-calc-pointing)
   &key
   target-name 
   target-ra 
   target-dec 
   target-position-vector 
   ra-dec-in-degrees 
   cell-center
   the-observer
   the-observer-name
   &allow-other-keys)
  
  "Set up the position and target slots in the event-calc-pointing
    structure.  If cell-center is t, the position is moved to the sky cell 
    center.  
    If observer is provided, then it and its name are stored in slots.
    If observer is nil but a name is provided, then the name only is stored.
    Return t if OK, nil if error."
  
  (let ((ok t)
        (derived-position-vector nil))
    
    (with-slots (name astro-object 
                      observer observer-name)
      ECP
      
      ;; null old fields
      (setf name nil
            astro-object nil
            observer-name nil)
      
      ;; set observer only if input is non-nil
      (cond 
       (the-observer 
        (setf observer the-observer)
        (setf observer-name (name the-observer)))
       (the-observer-name
        ;; observer is nil, but name is provided: store it
        (setf observer-name the-observer-name)))
      
      ;; make instance of fixed astro object
      (cond 
       ((and target-ra target-dec (null target-position-vector))
        (setf derived-position-vector
              (if ra-dec-in-degrees
                (3vect-spherical-to-cartesian (list (radians target-ra) (radians target-dec)))
                (3vect-spherical-to-cartesian (list target-ra target-dec)))))
       ((and target-position-vector (null target-ra) (null target-dec))
        (setf derived-position-vector target-position-vector))
       (t (spike-diagnostic 
           (with-output-to-string (stream)
             (format stream "target position inconsistency: either ra & dec ")
             (format stream "or position-vector must be specified")
             (format stream "~%target-name: ~a ra ~a dec ~a pos vector ~a"
                     target-name target-ra target-dec target-position-vector)))
          (setf ok nil)))
      
      (when ok
        
        ;; convert to cell center if required
        (if cell-center
          (setf derived-position-vector
                (cell-id-to-center-3vect
                 (3vect-to-cell-id derived-position-vector))))
        
        ;; make instance of astro object
        (setf astro-object
              (make-fixed-astronomical-object
               target-name :position-vector derived-position-vector))
        
        ;; populate slots
        (setf name target-name)
        )
      
      ;; returned value
      ok)))


(defmethod SET-MOVING-TARGET-FOR-EVENT-CALCULATION
  ((ECP event-calc-pointing)
   &key
   target-name 
   position-data 
   visibility-pcf 
   the-observer
   the-observer-name
   &allow-other-keys)

  "Set up pointing specification for moving target."
  
  (with-slots (name astro-object observer observer-name) ECP
    
    ;; null old fields
    (setf name nil
          astro-object nil
          observer-name nil)
    
    ;; set observer only if input is non-nil
    (cond (the-observer 
           (setf observer the-observer)
           (setf observer-name (name the-observer)))
          (the-observer-name
           ;; observer is nil, but name is provided: store it
           (setf observer-name the-observer-name)))

    ;; record name
    (setf name target-name)

    ;; make instance of astro object
    (setf astro-object
          (make-instance 'moving-astronomical-object))
    (setf (name astro-object) name)
    (setf (visibility-pcf astro-object) visibility-pcf)
    (setf (position-data astro-object) position-data)))


(defmethod GET-TARGET-FOR-EVENT-CALCULATION ((ECP event-calc-pointing))
  
  "Return list charactizing target pointing.  To be appended with other
    constraint specs for cache index. For moving targets the target name and 
    the ephemeris file write date are used as a hash index."
  
  (let ((astro-object (astro-object ECP)))
    (cond
     
     ;; fixed: use RA DEC
     ((typep astro-object 'fixed-astronomical-object)
      (list (ra astro-object) (dec astro-object)))
     
     ;; moving: use ephemeris write date
     ((typep astro-object 'moving-astronomical-object)
      (list (name astro-object) (ephemeris-write-date (position-data astro-object))))
     
     (t (spike-diagnostic
         (with-output-to-string (stream)
           (format stream "Unknown astronomical object type in event calculation")
           (format stream "~s" (type-of astro-object))))))))


(defmethod SET-ROLL-FOR-EVENT-CALCULATION ((ECP event-calc-pointing) args)

  "Set roll (orientation) specification for event calculation.
    Args are passed through to OBSERVER using specific method.  If
    new observer types are defined, the roll methods need to be added
    here.  Note that target (pointing) must be defined BEFORE roll can be
    defined.  Return t if successful, nil if error.
    Examples for low-earth-orbit-satellites:
     (set-roll-for-event-calculation ECP '(nil)) ;make roll undefined
     (set-roll-for-event-calculation ECP '(:nominal))
     (set-roll-for-event-calculation ECP '(:fixed :roll-angle 0.123))
     (set-roll-for-event-calculation ECP '(:offset-from-nominal 
                                           :roll-offset-angle -0.05)) "

  (cond 

   ((typep (observer ECP) 'low-earth-orbit-satellite)
    (apply 'set-satellite-roll-type
           (append (list (observer ECP)
                         (astro-object ECP))
                   args))
    t)
   (t (spike-diagnostic
       (with-output-to-string (stream)
         (format stream "Can't set roll for event calculation")
         (format stream "observer: ~a, args: ~a" (observer ECP) args)))
      nil)))


(defmethod GET-ROLL-FOR-EVENT-CALCULATION ((ECP event-calc-pointing))
  
  "Return charactization of roll (orientation) specification.  Return nil
    if orientation is undefined."
  
  (cond ((typep (observer ECP) 'low-earth-orbit-satellite)
         (get-satellite-roll-type (observer ECP)))
        (t nil)))


;;; ----------------------------------------------------------------------
;;;                                         Methods for EVENT-CALC-DATES
;;; ----------------------------------------------------------------------


(defmethod SET-DATES-FOR-EVENT-CALCULATION 
  ((ECD event-calc-dates) &key start end sample &allow-other-keys)
  
  "Set the date range for event calculation, and the default sample size (days).
    Return t if ok, nil if error.
    start and end are converted to TJD from standard formats, e.g. 1-jul-1990"
  
  (let* ((ok t)
         ;; convert to standard date format
         (start-time (if start (dmy-to-time start)))
         (end-time (if end (dmy-to-time end))))
    
    (with-slots (start-date end-date 
                            default-sample-interval 
                            default-sample-count) ECD
      
      ;; null old values
      (setf start-date nil
            end-date nil
            default-sample-interval nil
            default-sample-count nil)
      
      ;; check parameters and set new values
      (cond
       
       ;; valid parameters
       ((and start-time end-time sample
             (> sample 0)
             (> end-time start-time))
        (let* ((number-of-samples
                (1+ (ceiling (/ (- end-time start-time) sample)))))
          ;; store slot values
          (setf start-date start-time
                end-date end-time
                default-sample-interval sample
                default-sample-count number-of-samples)))
       
       ;; error: parameters not valid
       (t (spike-diagnostic 
           (with-output-to-string (stream)
             (format stream "Invalid date range/sample specification:")
             (format stream "start ~a end ~a sample-interval ~a" 
                     start end sample)))
          (setf ok nil)))
      
      ;; returned value
      ok)))


(defmethod GET-DATES-FOR-EVENT-CALCULATION ((ECD event-calc-dates))
  
  "Return list characterizing date range for event calculation.  Appended
    to other constraint specs for cache index.
    Note:  do NOT include sample interval, since is set separately for
    each constraint type."
  
  (list (start-date ECD) (end-date ECD)))


;;; ----------------------------------------------------------------------
;;;                                           Methods for EVENT-CALC-CACHE
;;; ----------------------------------------------------------------------


(defmethod EVENT-CACHE-INITIALIZE ((ECC event-calc-cache))

  "Clear the event calculator cache, init counters"

  (clrhash (slot-value ECC 'EC-cache))
  (setf (slot-value ECC 'EC-cache-calls) 0
        (slot-value ECC 'EC-cache-hits) 0
        (slot-value ECC 'save-EC-cache) nil)
  t)


(defmethod PUT-IN-EVENT-CACHE ((ECC event-calc-cache) key value)
  
  "Store in event calculator cache under specified key.  Mark the cache
    as needing to be saved.  value is returned."
  
  (setf (gethash key (slot-value ECC 'EC-cache)) value
        (slot-value ECC 'save-EC-cache) t)
  value)


(defmethod GET-FROM-EVENT-CACHE ((ECC event-calc-cache) key)

  "Get from event calculator cache, return value if found else nil.
    Update cache hit record."

  (let ((result (gethash key (slot-value ECC 'EC-cache))))
    (setf (slot-value ECC 'EC-cache-calls) 
          (1+ (slot-value ECC 'EC-cache-calls)))
    (if result
      (setf (slot-value ECC 'EC-cache-hits) 
            (1+ (slot-value ECC 'EC-cache-hits))))
    result))


(defmethod EVENT-CACHE-DESCRIPTION ((ECC event-calc-cache) &optional (stream t))
  
  "Report cache status, optionally to stream"
  
  (let* ((calls (slot-value ECC 'EC-cache-calls))
         (hits (slot-value ECC 'EC-cache-hits))
         (misses (- calls hits)))
    (format stream "~%event cache size ~d, need to save? ~a"
            (hash-table-count (slot-value ECC 'EC-cache))
            (if (slot-value ECC 'save-EC-cache) "YES" "NO"))
    (format stream "~%event cache:  ~d calls, ~d from cache (~,2f%), ~d computed (~,2f%)"
            calls hits
            (* 100 (if (/= 0 calls)  (/ hits   calls) 0))
            misses
            (* 100 (if (/= 0 calls)  (/ misses calls) 0)))))

(defmethod EVENT-CACHE-SIZE ((ECC event-calc-cache))
  
  "Report cache size"
  
  (hash-table-count (slot-value ECC 'EC-cache)))


(defmethod EVENT-CACHE-NEEDS-TO-BE-SAVED ((ECC event-calc-cache))
  
  "Return t if any cache needs to be saved, nil otherwise"
  ;; (event-cache-needs-to-be-saved)
  
  (slot-value ECC 'save-EC-cache))


(defmethod CLEAR-EVENT-CACHE ((ECC event-calc-cache))

  "Clear all event caches"
  ;; (clear-event-cache)

  (event-cache-initialize ECC))


(defmethod EVENT-CACHE-REPORT ((ECC event-calc-cache) &key (stream t))
  
  "Report status of event cache to stream"
  ;; (event-cache-report)
  
  (event-cache-description ECC stream))


(defmethod SAVE-EVENT-CACHE ((ECC event-calc-cache)
                             filename 
                             &key 
                             (compile nil) 
                             (verbose t) 
                             (header nil))
  
  "Save event cache to file if it needs to be saved.  If the optional
    keyword arg :compile is provided, it should be the name of the compiled
    file.  If nil only the source file is written.
    If verbose is t then a message is written to standard output.
    The keyword arg :header should be a list of strings, written to the
    output file before the cache is saved.  If these are comments they should
    have the semicolons (or whatever) in the strings - none are added here."
  ;; (save-event-cache "xanadu:mar91:cache.tmp")
  
  (when (event-cache-needs-to-be-saved ECC)
    (if verbose (format t "~%Saving event cache to ~a" filename))
    
    (with-open-file (stream filename
                            :direction :output
                            :if-exists :supersede
                            :if-does-not-exist :create)
      ;; write the header
      (dolist (header-line header)
        (format stream "~a" header-line)
        (terpri stream))
      ;; write cache contents
      (write-calls-from-event-hash-table
       (slot-value ECC 'EC-cache) stream)
      )                               ; close stream
    ;; mark as not needing to be saved
    (setf (slot-value ECC 'save-EC-cache) nil)
    
    ;; compile the file if requested
    (if compile
      (compile-file filename :output-file compile))
    
    ))

(defvar *CURRENT-ECC* nil
  "special, bound to instance of event-calc-cache only at the time a cache is
    being restored")

(defmethod RESTORE-EVENT-CACHE ((ECC event-calc-cache)
                                filename 
                                &key 
                                (verbose t))
  
  "Clear current cache, then restore event cache from filename, if it exists."  
  ;; (restore-event-cache "xanadu:mar91:cache.tmp")
  
  (clear-event-cache ECC)
  (cond ((probe-file filename)
         (if verbose (format t "~%Restoring event cache from ~a" filename))
         (let ((*CURRENT-ECC* ECC)) (load filename))
         (setf (slot-value ECC 'save-EC-cache) nil)
         (if verbose (event-cache-report ECC)))
        (t (spike-diagnostic
            (format nil "event cache file ~a not found, cannot load"
                    filename)))))


;;; --- cache writer function: map over cache and save forms for restoring it

(defun WRITE-CALLS-FROM-EVENT-HASH-TABLE (table stream)
  
  "map over the hash table (table) writing out calls to stream to 
    restore cache entries."
  
  (flet ((replace-minus-infinity (pcf)
           (cond ((<= (first pcf) *minus-infinity* )
                  `(cons *minus-infinity* ',(rest pcf)))
                 (t
                  `(cons *minus-infinity* '( 1 ,@pcf))))))
    (maphash #'(lambda (params pcf)
                 (pprint `(add-cache :params ',params 
                                     :pcf ,(replace-minus-infinity pcf))
                         stream)
                 (terpri stream))
             table)))


;;; --- cache reader function: the form written in the saved cache file

(defun ADD-CACHE (&key params pcf)
  "add element into cache"
  (cond ((typep *CURRENT-ECC* 'event-calc-cache)
         (put-in-event-cache *CURRENT-ECC* params pcf))
        (t (spike-diagnostic "*CURRENT-ECC* is not a cache - can't restore"))))


;;; ----------------------------------------------------------------------
;;;                                     Methods for EVENT-CALC-ORB-VIEWING
;;; ----------------------------------------------------------------------

(defmethod SET-VIEWING-PARMS-FOR-EVENT-CALCULATION 
  ((ECOV event-calc-orb-viewing)
   &key
   orbit-model-name
   bright-angle 
   dark-angle 
   terminator-limit
   limb-angles-in-degrees
   
   dark-flag
   dark-sun-limb-limit
   
   view-duration
   view-interruptible
   view-minimum-duration
   view-suitability-table
   
   north-flag
   
   high-accuracy-flag
   interval
   &allow-other-keys)
  
  "Set orbital viewing parameters for event calculation.  Return t if ok, 
    nil if error.  If limb-angles-in-degrees is t, then bright-angle, dark-angle
    are converted from degrees to radians.  Other angles (terminator-limit,
    dark-sun-limb-limit) are RADIANS."
  
  (let ((ok t))					;error flag
    
    (with-slots 
      (orbit-model bright-limb-angle dark-limb-angle
                   dark northern-hemisphere-only high-accuracy
                   viewing-time interruptible 
                   min-viewing-time viewing-table 
                   orb-viewing-sample-interval
                   terminator-angle dark-sun-limb-angle)
      ECOV
      
      ;; load the orbit model, save the name in the slot
      (setf orbit-model (load-orbit-model orbit-model-name))
      ;; loading the orbit model may redefine the observer instance
      ;; update the slot based on the satellite name table
      (setf (observer ECOV) (get-satellite-by-name (observer-name ECOV)))
      
      ;; set sample interval
      (setf orb-viewing-sample-interval interval)
      
      ;; angles:  null and reset
      (setf bright-limb-angle nil
            dark-limb-angle nil
            terminator-angle nil
            dark-sun-limb-angle nil)
      (setf dark-sun-limb-angle (or dark-sun-limb-limit 0))
      (setf terminator-angle (or terminator-limit (radians 90)))
      (let ((bright-angle (or bright-angle 0))
            (dark-angle (or dark-angle 0)))
        ;;convert to radians
        (setf bright-angle (if limb-angles-in-degrees 
                             (radians bright-angle) bright-angle))
        (setf dark-angle (if limb-angles-in-degrees 
                           (radians dark-angle) dark-angle))
        ;; store
        (setf bright-limb-angle bright-angle
              dark-limb-angle dark-angle))
      (when (> dark-limb-angle bright-limb-angle)
        (spike-diagnostic "error: dark limb limit > bright limb")
        (setf ok nil))
      
      ;; store flags
      (setf dark dark-flag
            northern-hemisphere-only north-flag
            high-accuracy high-accuracy-flag)
      
      ;; viewing duration constraint:
      (if (not (validate-constraint-interval-table view-suitability-table))
        (spike-diagnostic 
         (format nil "error in view-suitability-table format ~a" 
                 view-suitability-table)))
      ;; null out any old values
      (setf viewing-time nil
            interruptible nil
            min-viewing-time nil
            viewing-table nil)
      (setf viewing-time view-duration
            interruptible view-interruptible
            min-viewing-time view-minimum-duration
            viewing-table (if view-suitability-table
                            (pcf-from-interval-list view-suitability-table)
                            nil))
      
      ;; consistency check
      (when (and viewing-table (not viewing-time))
        (spike-diagnostic "viewing-table specified, but not viewing-time")
        (setf ok nil))
      (when (and viewing-time interruptible (not min-viewing-time))
        (spike-diagnostic "no min viewing time for interruptible observation")
        (setf ok nil))
      
      ;; returned value
      ok)))


(defmethod SET-SENSOR-LIMITS-FOR-EVENT-CALCULATION 
  ((ECOV event-calc-orb-viewing)
   sensor-name
   &key sun moon bright-limb dark-limb (angles-in-degrees nil) &allow-other-keys)
  
  "Record sensor limits for event calculation.  Angles are in radians,
    unless angles-in-degrees is t.
    Missing angles will have corresponding constraint ignored."
  
  (setf (gethash sensor-name (slot-value ECOV 'sensor-limits))
        (list :sun (if sun (if angles-in-degrees (radians sun) sun))
              :moon (if moon (if angles-in-degrees (radians moon) moon))
              :bright-limb (if bright-limb (if angles-in-degrees 
                                             (radians bright-limb) bright-limb))
              :dark-limb (if dark-limb (if angles-in-degrees 
                                         (radians dark-limb) dark-limb))
              )))


(defmethod GET-SENSOR-LIMIT-ANGLE ((ECOV event-calc-orb-viewing)
                             sensor-name type)

  "Return sensor angle limit.  sensor-name is the name used in SET-SENSOR-LIMITS...
    type is one of :sun, :moon, :bright-limb, :dark-limb
    Returned angle is in radians.  Return nil if angle not found."

  (getf (gethash sensor-name (slot-value ECOV 'sensor-limits)) type))


(defmethod ORB-VIEWING-SAMPLE-INTERVAL ((E event-calc-orb-viewing))
  "Return sample interval for orbital viewing constraint"
  (or (slot-value E 'orb-viewing-sample-interval)
      (default-sample-interval E)))


(defmethod ORB-VIEWING-SAMPLE-COUNT ((E event-calc-orb-viewing))
  "return number of samples"
  (1+ (ceiling (/ (- (end-date E) (start-date E)) 
                  (orb-viewing-sample-interval E)))))


(defmethod GET-VIEWING-PARMS-FOR-EVENT-CALCULATION
  ((e event-calc-orb-viewing))
  "return a list of parameter values which characterize viewing parameters
    for event calculation.  The purpose is:  two calculations with the same
    set of parameters will have the same event constraint."
  
  (rationalize-numbers-in-nested-list     
   (append '(:orb-viewing)
           (get-target-for-event-calculation e)
           (get-dates-for-event-calculation e)
           (list  (orb-viewing-sample-interval e)
                  (intern (string-it-upcase (orbit-model e)))
                  (bright-limb-angle e)
                  (dark-limb-angle e)
                  (terminator-angle e)
                  (dark e)
                  (dark-sun-limb-angle e)
                  (northern-hemisphere-only e)
                  (high-accuracy e)
                  (viewing-time e)
                  (interruptible e)
                  (min-viewing-time e)
                  (replace-minus-infinity-in-pcf-by-symbol 
                   (viewing-table e))))))



(defmethod ORBITAL-VIEWING-PCF ((ECOV event-calc-orb-viewing))
  
  "get orb viewing pcf either from cache or by computing it"
  
  (or (get-from-event-cache 
       ECOV (get-viewing-parms-for-event-calculation ECOV))
      (cond ((typep (astro-object ECOV) 'FIXED-ASTRONOMICAL-OBJECT) 
             (compute-orbital-viewing-pcf ECOV))
            ((typep (astro-object ECOV) 'MOVING-ASTRONOMICAL-OBJECT)
             (integrate-visibility-windows-with-orbital-pcf 
              ECOV (compute-orbital-viewing-pcf 
                    ECOV :cache-results nil))))))

		    
;;; ----------------------------------------------------------------------
;;;                                                   ORB-VIEWING sampling
;;; ----------------------------------------------------------------------

;;; NOTE:  DO NOT ACCESS RETURNED VALUE DIRECTLY:  use the macro 
;;; WITH-ORB-VIEWING-SAMPLE to obtain values for all computed quantities.


(defmethod ORB-VIEWING-SAMPLE ((ECOV event-calc-orb-viewing) time
                               &key 
                               (omit-geo-pcf nil)
                               (omit-dark-limb-pcf nil)
                               (omit-bright-limb-pcf nil)
                               (omit-vis-pcf nil)
                               (omit-dark-pcf nil)
			       (sensor-name-list nil)
			       (named-vector-list nil)
			       (verbose nil))
  
  "Compute and return sample at time.  See WITH-ORB-VIEWING-SAMPLE for 
    how to access returned value.  Return nil if no orbit model, or if
    orbit-model is :none.  Return nil if time is outside orbit model
    range.  Both bright-limb-angle and dark-limb-angle must be set:
    if one is not then nil is returned.  If sensor-name-list or 
    named-vector-list is provided, then data for these is computed and
    returned with the sample.  It the :omit-... keywords are t, then
    the corresponding PCF is not calculated and the result will be a
    pcf which is unity over the sample orbit, 0 outside.
    NOTE:  if :omit-geo-pcf is t, then the time of midnight will be 
     the middle of the orbit.
    NOTE:  this uses WITH-FIXED-ORIENTATION and WITH-FIXED-ORBIT-ORIENTATION
     for efficiency"
  
  (with-slots 
    (astro-object
     orbit-model
     observer
     bright-limb-angle
     dark-limb-angle
     high-accuracy
     dark-sun-limb-angle
     terminator-angle)
    ECOV
    
    ;; error check: must have orbit model and be in time range: try to find
    ;; as many errors as possible before giving up
    (let ((ok t))
      (when (null orbit-model)
        (spike-diagnostic "no orbit model") (setf ok nil))
      (when (eq orbit-model :none)
        (spike-diagnostic "orbit model is :NONE") (setf ok nil))
      (when (not (in-orbit-model-range time))
        (spike-diagnostic "time not in orbit model range") (setf ok nil))
      (when (or (null bright-limb-angle) (null dark-limb-angle))
        (spike-diagnostic "both bright and dark limb angle must be specified")
        (setf ok nil))
      (when (not ok)
        (spike-diagnostic "returning nil from orbit-sample")
        (return-from orb-viewing-sample nil)))
    
    (when verbose
      ;; just print what is going to be calculated
      (format t "~%T=~9,4f, omit ~a, sensors ~a, named vect ~a"
              time
              (if (or omit-geo-pcf omit-dark-limb-pcf omit-bright-limb-pcf
                      omit-vis-pcf omit-dark-pcf)
                (format nil "~a~a~a~a~a"
                        (if omit-geo-pcf "GEO " "")
                        (if omit-dark-limb-pcf "DARK-LIMB " "")
                        (if omit-bright-limb-pcf "BRIGHT-LIMB " "")
                        (if omit-vis-pcf "VIS " "")
                        (if omit-dark-pcf "DARK " ""))
                "NONE ")
              sensor-name-list
              named-vector-list))

    ;; compute 
    (with-fixed-orientation 
      observer
      (with-fixed-orbit-orientation
        observer
        (let* ((t1 (dmy-to-time time))
               (n1 (time-of-next-asc-node observer t1))
               (n2 (time-of-next-asc-node observer (+ n1 0.0001)))
               ;; this is heuristic midnight:  the center of the geometric unocculted interval 
               (midnight nil)
               (orb-pcf (pcf-create-with-single-interval n1 n2 1 0))
               (geo-pcf            *unity-pcf*)
               (dark-limb-pcf      *unity-pcf*)
               (bright-limb-pcf    *unity-pcf*)
               (vis-pcf            *unity-pcf*)
               ;; use 0.1 sec for high-accuracy, 5 sec if not
               (convergence        (if high-accuracy 1.1574e-6 5.787e-5))
               (dark-pcf           *unity-pcf*)
               (north-pcf          *unity-pcf*)
               (sensor-results nil)
               (named-vector-results nil))
          
          ;; set geometric pcf geo-pcf
          (let ((occ-list (if (not omit-geo-pcf)
                            (occultation-list 
                             observer astro-object n1 n2 :limb-angle 0
                             :high-accuracy high-accuracy))))
            (setf geo-pcf
                  ;; note pcf value is 0 if target is occulted, 1 if visible
                  (if omit-geo-pcf
                    orb-pcf
                    (pcf-multiply 
                     orb-pcf (pcf-from-start-end-list occ-list 0 1)))))
          
          ;; midnight is center of geo pcf
          ;; if :omit-geo-pcf is t, this will be the center of the orbit
          (let* ((start-end (convert-orb-pcf-to-start-end geo-pcf))
                 (mid (+ (first start-end) 
                         (/ (- (second start-end) (first start-end)) 2.0))))
            (setf midnight (if (and (>= mid n1) (<= mid n2))
                             mid
                             (- mid (- n2 n1)))))
          ;; set dark angle
          (let ((occ-list (if (not omit-dark-limb-pcf)
                            (occultation-list observer astro-object n1 n2 
                                              :limb-angle dark-limb-angle
                                              :high-accuracy high-accuracy))))
            (setf dark-limb-pcf
                  (if omit-dark-limb-pcf
                    orb-pcf
                    ;; note pcf value is 0 if target is occulted, 1 if visible
                    (pcf-multiply 
                     orb-pcf (pcf-from-start-end-list occ-list 0 1)))))
          
          ;; set bright angle pcf
          (let ((occ-list (if (not omit-bright-limb-pcf)
                            (occultation-list observer astro-object n1 n2 
                                              :limb-angle bright-limb-angle
                                              :high-accuracy high-accuracy))))
            (setf bright-limb-pcf
                  (if omit-bright-limb-pcf
                    orb-pcf
                    ;; note pcf value is 0 if target is occulted, 1 if visible
                    (pcf-multiply 
                     orb-pcf (pcf-from-start-end-list occ-list 0 1)))))
          
          ;; bright and dark = vis pcf
          (let ((occ-list (if (not omit-vis-pcf)
                            (EARTH-ILLUM-OCCULTATION-LIST 
                             observer astro-object n1 n2 
                             :bright-limb-angle bright-limb-angle
                             :dark-limb-angle dark-limb-angle
                             :terminator-angle terminator-angle
                             :high-accuracy high-accuracy
                             :convergence convergence))))
            (setf vis-pcf 
                  (if omit-vis-pcf
                    orb-pcf
                    ;; note pcf value is 0 if target is occulted, 1 if visible
                    (pcf-multiply 
                     orb-pcf (pcf-from-start-end-list occ-list 0 1)))))
          
          ;; dark (shadow) pcf
          (let ((occ-list (if (not omit-dark-pcf)
                            (occultation-list observer *sun* n1 n2 
                                              :limb-angle dark-sun-limb-angle
                                              :high-accuracy high-accuracy))))
            (setf dark-pcf 
                  (if omit-dark-pcf
                    orb-pcf
                    ;; pcf is 1 if ST is in shadow, 0 otherwise
                    (pcf-multiply 
                     orb-pcf (pcf-from-start-end-list occ-list 1 0)))))
          
          
          ;; north: always compute, even if flag not set:  PCF builder can ignore
          (setf north-pcf
                (pcf-create-with-single-interval
                 n1 (+ n1 (* 0.5 (- n2 n1))) 1 0))
          
          (dolist (sensor-name sensor-name-list)
            (push (cons sensor-name
                        (sensor-sample ECOV n1 n2 sensor-name))
                  sensor-results))
          
          (dolist (named-vector-name named-vector-list)
            (push (cons named-vector-name
                        (named-vector observer named-vector-name time))
                  named-vector-results))
          
          ;; returned value
          (list n1 n2 midnight orb-pcf geo-pcf dark-limb-pcf bright-limb-pcf
                vis-pcf dark-pcf north-pcf sensor-results named-vector-results))))
    
    ))


(defmacro WITH-ORB-VIEWING-SAMPLE (sample &rest body)
  
  "Execute body with local bindings from an orb-viewing sample.  
    All times are TJD.  All PCFs are zero outside the orbit boundaries
    n1 to n2.
    Bindings are:
     n1 - orbit start (asc. node)
     n2 - orbit end (asc. node)
     midnight - time of deepest target occultation, if any (midpoint
      of geo-pcf non-zero intervals)
     orb-pcf - PCF 1 between n1 and n2, 0 otherwise
     geo-pcf - PCF 1 when target limb angle > 0
     dark-limb-pcf - PCF 1 when target limb angle > dark-limb-angle
     bright-limb-pcf - PCF 1 when target limb angle > bright-limb-angle
     vis-pcf - PCF 1 when line-of-sight from viewer to target is outside
      the dark limb angle from the earth limb, and the bright limb angle from
      the illuminated earth
     dark-pcf - PCF 1 when observer in shadow
     north-pcf - PCF 1 when observer in northern hemisphere
    Local function bindings (angles are in radians):
     (sensor-sun-angle <sensor-name>) - angle of sensor boresight to sun
     (sensor-moon-angle <sensor-name>) - angle of sensor boresight to moon
     (sensor-vis-pcf <sensor-name>) - pcf 1 when sensor boresight above
      bright or dark limb angle from earth
     (named-vector-position <named-vector-name>) - 3vector position at 
      sample time
    Notes: 
     Omitted quantities that are pcfs are 1 in the orbit range n1 to n2,
      0 outside.  
     Sensors must have been present in sensor-name-list for sensor data to 
      be computed.
     named-vector must have been present in named-vector-list.
     sensor-vis-pcf is only available when limb angles where recorded by
      a call to set-sensor-limits-for-event-calculation.  Otherwise the call
      returns *unity-pcf*.
    "
  
  (let* ((samp (gensym))
         (len (gensym))
         (locals nil)
         (sensor-calls nil)
         (ignore-list nil)
         )

    ;; build list of references to variables
    (if (find-symbol-in-form 'n1 body) 
      (push `(n1              (first   ,samp)) locals))
    (if (find-symbol-in-form 'n2 body) 
      (push `(n2              (second  ,samp)) locals))
    (if (find-symbol-in-form 'midnight body) 
      (push `(midnight        (third   ,samp)) locals))
    (if (find-symbol-in-form 'orb-pcf body) 
      (push `(orb-pcf         (fourth  ,samp)) locals))
    (if (find-symbol-in-form 'geo-pcf body) 
      (push `(geo-pcf         (fifth   ,samp)) locals))
    (if (find-symbol-in-form 'dark-limb-pcf body) 
      (push `(dark-limb-pcf   (sixth   ,samp)) locals))
    (if (find-symbol-in-form 'bright-limb-pcf body) 
      (push `(bright-limb-pcf (seventh ,samp)) locals))
    (if (find-symbol-in-form 'vis-pcf body) 
      (push `(vis-pcf         (eighth  ,samp)) locals))
    (if (find-symbol-in-form 'dark-pcf body) 
      (push `(dark-pcf        (ninth   ,samp)) locals))
    (if (find-symbol-in-form 'north-pcf body) 
      (push `(north-pcf     (tenth   ,samp)) locals))
    (push `(,len            (length  ,samp)) locals)
    (push `(,samp           ,sample) locals)

    ;; build list of references to functions for sensor angles
    (if (find-symbol-in-form 'sensor-sun-angle body)
      (push `(sensor-sun-angle 
              (sensor)  (getf (rest (assoc sensor (if (>= ,len 11) (elt ,samp 10) nil))) 
                              :sun-angle))
            sensor-calls))
    (if (find-symbol-in-form 'sensor-moon-angle body)
      (push `(sensor-moon-angle 
              (sensor) (getf (rest (assoc sensor (if (>= ,len 11) (elt ,samp 10) nil))) 
                             :moon-angle))
            sensor-calls))
    (if (find-symbol-in-form 'sensor-vis-pcf body)
      (push `(sensor-vis-pcf
              (sensor) (getf (rest (assoc sensor (if (>= ,len 11) (elt ,samp 10) nil))) 
                             :vis-pcf))
            sensor-calls))
    (if (find-symbol-in-form 'named-vector-position body)
      (push `(named-vector-position
              (named-vector) (rest 
                              (assoc named-vector 
                                     (if (>= ,len 12) (elt ,samp 11) nil))))
            sensor-calls))

    (if (not sensor-calls) (push len ignore-list))

  `(let* ,locals
     ,(if ignore-list
         (list 'declare (cons 'ignore ignore-list)) nil)
     (flet ,sensor-calls       
       ,@body))))


(defmethod DESCRIBE-ORB-VIEWING-SAMPLE ((ECOV event-calc-orb-viewing)
                                        sample &key sensors named-vectors)
  
  "print description of times in orb-viewing-sample to standard output.
    most useful for debugging, not used in constraint calculations"
  
  (with-orb-viewing-sample sample
    (format t "~%orb start ~9,4f ~a" n1 (format-abs-time n1))
    (format t "~%  period: ~6,2f min" (* 1440 (- n2 n1)))
    (format t "~%  midnight after ~6,2f min from orb start"
            (* 1440 (- midnight n1)))
    (format t "~%  visibility (minutes) above:")
    (format t "~%  horiz ~6,2f, dark limit ~6,2f, bright limit ~6,2f"
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals geo-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals dark-limb-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals bright-limb-pcf)))
            )
    (format t "~%  vis: ~6,2f, dark: ~6,2f, dark-vis ~6,2f"
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals vis-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals dark-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals 
                                               (pcf-multiply vis-pcf dark-pcf))))
            )
    (dolist (sensor sensors)
      (format t "~% ~a" sensor)
      (if (get-sensor-limit-angle ECOV sensor :sun)
        (format t " angle to sun ~6,2f deg" (degrees (sensor-sun-angle sensor))))
      (if (get-sensor-limit-angle ECOV sensor :moon)
        (format t " angle to moon ~6,2f deg" (degrees (sensor-moon-angle sensor))))
      (if (and (get-sensor-limit-angle ECOV sensor :bright-limb)
               (get-sensor-limit-angle ECOV sensor :dark-limb))
        (format t "~%   vis: ~6,2f min, vis with target: ~6,2f min"
                (* 1440 (summed-interval-duration 
                         (pcf-non-zero-intervals (sensor-vis-pcf sensor))))
                (* 1440 (summed-interval-duration 
                         (pcf-non-zero-intervals 
                          (pcf-multiply vis-pcf (sensor-vis-pcf sensor)))))
                )))
    (dolist (named-vector named-vectors)
      (format t "~% ~a ~a" named-vector (named-vector-position named-vector)))
    ))


(defmethod SENSOR-SAMPLE ((ECOV event-calc-orb-viewing) n1 n2 sensor)
  
  "return characteristics of sensor view between two node crossings
    n1 and n2.  
    SENSOR can either be a sensor instance or the name of a sensor attached 
    to the observer (suitable for use with find-sensor).
    Result is a plist (:sun-angle <angle> :moon-angle <angle> :vis-pcf <pcf)
    vis-pcf is *unity-pcf* unless both bright and dark limb angles provided."

  (with-slots (observer
               high-accuracy
               terminator-angle) ECOV
    (if (null (roll-spec observer)) 
      (error "Can't compute roll, so can't compute sensor"))
    (let ((sensor-instance (if (typep sensor 'sensor)
                             sensor
                             (find-sensor observer sensor))))
      (when sensor-instance
        (let* ((sensor-vector (sensor-view-vector sensor-instance n1))
               (sensor-name (name sensor-instance))
               (bright-limb (get-sensor-limit-angle ECOV sensor-name :bright-limb))
               (dark-limb (get-sensor-limit-angle ECOV sensor-name :dark-limb))
               (sun-angle (3vect-separation
                           sensor-vector
                           (position-vector *sun* n1)))
               (moon-angle (3vect-separation
                            sensor-vector
                            (position-vector *moon* n1)))
               (orb-pcf (pcf-create-with-single-interval n1 n2 1 0))
               (sensor-vis-pcf *unity-pcf*)
               ;; use 0.1 sec for high-accuracy, 5 sec if not
               (convergence        (if high-accuracy 1.1574e-6 5.787e-5))
               )
          
	  (when (and bright-limb dark-limb)
	    (let ((occ-list (EARTH-ILLUM-OCCULTATION-LIST 
			     observer sensor-instance n1 n2 
			     :bright-limb-angle bright-limb
			     :dark-limb-angle dark-limb
                             :terminator-angle terminator-angle
			     :high-accuracy high-accuracy
                             :convergence convergence)))
	      ;; note pcf value is 0 if target is occulted, 1 if visible
	      (setf sensor-vis-pcf 
		(pcf-multiply orb-pcf (pcf-from-start-end-list occ-list 0 1)))))

          (list :sun-angle sun-angle 
                :moon-angle moon-angle 
                :vis-pcf sensor-vis-pcf))))))


(defmethod ORB-VIEWING-SAMPLE-LIST ((ECOV event-calc-orb-viewing)
                                    &key 
                                    (sensor-name-list nil)
                                    (named-vector-list nil)
                                    (range nil))
  
  "Compute and return sample list for time range.  If RANGE is specified,
    it must be a list (start end) which overrides the start-date and end-date
    in the event-calc-orb-viewing object.  In this case only samples between
    the specified range start and end are computed and returned in the list.
    See WITH-ORB-VIEWING-SAMPLE-LIST and WITH-ORB-VIEWING-SAMPLE for 
    how to access the returned value"
  
  (with-slots (start-date
               end-date
               orbit-model
               bright-limb-angle
               dark-limb-angle)
	      ECOV
    
	      ;; error check: must have orbit model
	      (when (or (null orbit-model) 
			(eq orbit-model :none))
		(warn "no orbit model specified - cannot compute orbital viewing constraint")
		(return-from orb-viewing-sample-list nil))
	      ;; must have bright and dark limb angles
	      (when (or (null bright-limb-angle) (null dark-limb-angle))
		(warn "no limb angles specified - cannot compute orbital viewing constraint")
		(return-from orb-viewing-sample-list nil))
	      ;; must have start and end dates
	      (when (or (null start-date) (null end-date))
		(warn "no start/end dates specified - cannot compute orbital viewing constraint")
		(return-from orb-viewing-sample-list nil))

	      ;; sample and build list
	      (let* ((specified-start (first range))
		     (specified-end   (second range))
		     (sample-interval (orb-viewing-sample-interval ECOV))
		     (sample-count (orb-viewing-sample-count ECOV))
		     (half-sample-interval (* 0.5 sample-interval))
		     interval-start interval-end interval-middle
		     (result nil))
		(dotimes (i sample-count)
		  ;;force end of previous interval to be start of current one
		  (setf interval-start (if interval-end interval-end 
					 (- start-date half-sample-interval)))
		  (setf interval-middle (+ interval-start half-sample-interval))
		  (setf interval-end   (+ interval-start sample-interval))
        
		  ;; only compute samples if in time range: must be within orbit model
		  ;; dates and either have no subrange specified, or be within the subrange
		  (when (and (in-orbit-model-range interval-middle)
			     (or (null range) ; no sub-range specified
				 (and (>= interval-middle specified-start)
				      (<= interval-middle specified-end))))
          
		    (push (cons interval-start
				(cons interval-end 
				      (orb-viewing-sample 
				       ECOV interval-middle
				       :sensor-name-list sensor-name-list
				       :named-vector-list named-vector-list)))
			  result)
          
		    ))
      
		;; returned result
		(nreverse result))))


(defmacro WITH-ORB-VIEWING-SAMPLE-LIST (sample-list &rest body)
  
  "Execute body once for each sample in sample-list, with local bindings
    as specified by WITH-ORB-VIEWING-SAMPLE, but in addition the bindings:
      sample-start - TJD of sample start
      sample-end - TJD of sample end
      orb-viewing-sample - the sample, can be accessed via the 
       macro WITH-ORB-VIEWING-SAMPLE)"
  
  (let* ((_sample (gensym))
         (locals nil))
    (if (find-symbol-in-form 'sample-start body)
      (push `(sample-start (first ,_sample)) locals))
    (if (find-symbol-in-form 'sample-end body)
      (push `(sample-end (second ,_sample)) locals))
    (if (find-symbol-in-form 'orb-viewing-sample body)
      (push `(orb-viewing-sample (cddr ,_sample)) locals))
    
    `(dolist (,_sample ,sample-list)
       (let ,locals
         (with-orb-viewing-sample (cddr ,_sample) ,@body)))))


(defmethod DESCRIBE-ORB-VIEWING-SAMPLE-LIST 
  ((ECOV event-calc-orb-viewing)
   sample-list &key sensors)
  
  "print description of times in orb-viewing-sample-list to standard output.
    most useful for debugging, not used in constraint calculations"
  
  (format t "~%asc node  mm/dd hh:mm  mid  geo   dark  brite vis   dark  dkvis")
  (with-orb-viewing-sample-list sample-list
    (format t "~%~9,4f ~a" n1 
            (let ((ymdhms (jd-to-ymdhms n1)))
              (format nil "~2,'0d/~2,'0d ~2,'0d:~2,'0d"
                      (second ymdhms) (third ymdhms) (fourth ymdhms) (fifth ymdhms))))
    (format t "~6,2f" (* 1440 (- midnight n1)))
    (format t "~6,2f~6,2f~6,2f"
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals geo-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals dark-limb-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals bright-limb-pcf)))
            )
    (format t "~6,2f~6,2f~6,2f"
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals vis-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals dark-pcf)))
            (* 1440 (summed-interval-duration (pcf-non-zero-intervals 
                                               (pcf-multiply vis-pcf dark-pcf))))
            )
    (dolist (sensor sensors)
      (format t " ~6a" sensor)
      (if (get-sensor-limit-angle ECOV sensor :sun)
        (format t "~6,2fd" (degrees (sensor-sun-angle sensor))))
      (if (get-sensor-limit-angle ECOV sensor :moon)
        (format t "~6,2fd" (degrees (sensor-moon-angle sensor))))
      (if (and (get-sensor-limit-angle ECOV sensor :bright-limb)
               (get-sensor-limit-angle ECOV sensor :dark-limb))
        (format t "~6,2f~6,2f"
                (* 1440 (summed-interval-duration 
                         (pcf-non-zero-intervals (sensor-vis-pcf sensor))))
                (* 1440 (summed-interval-duration 
                         (pcf-non-zero-intervals 
                          (pcf-multiply vis-pcf (sensor-vis-pcf sensor)))))
                )))))


(defmethod MAX-VISIBILITY-INTERVAL-SIZE ((ECOV event-calc-orb-viewing)
                                         sample-list)
  "Given orb-viewing sample list and ECOV, return size of largest
    visibility period (in days), taking into account bright/dark
    limb constraints and dark constraint"
  
  (let ((max-int 0))
    (with-orb-viewing-sample-list sample-list
      (setf max-int
            (max max-int
                 (summed-interval-duration
                  (pcf-non-zero-intervals
                   (if (dark ECOV)
                     (pcf-multiply vis-pcf dark-pcf)
                     vis-pcf)
                   n1 n2)))))
    max-int))
                  
    

(defmethod COMPUTE-ORBITAL-VIEWING-PCF ((ECOV event-calc-orb-viewing) 
                                        &key 
                                        (cache-results t)
                                        (stream nil)
                                        &allow-other-keys)
  
  "Compute and return orbital viewing pcf for the interval and constraint
    parameters recorded in the event-calc-orb-viewing object.
    If :stream is non-nil, report components of suitability calculation
    to it."
  
  (let ((sample-list (orb-viewing-sample-list ECOV))
        (interval-list nil)
        result
        )
    
    (when stream
      ;; write some header info
      (format stream "~%name: ~a" (name ECOV))
      (format stream "~%  dark ~a  sun angle for dark: ~6,2f deg"
              (dark ECOV) (degrees (dark-sun-limb-angle ECOV)))
      (format stream "~%  dark earth ~6,2f deg bright earth ~6,2f deg, terminator ~6,2f deg"
              (degrees (dark-limb-angle ECOV))
              (degrees (bright-limb-angle ECOV))
              (degrees (terminator-angle ECOV)))
      (format stream "~%  view time ~a, ~a min-view ~a"
              (if (viewing-time ECOV)
                (format nil "~6,2f min" (* 1440 (viewing-time ECOV))))
              (if (viewing-time ECOV)
                (if (interruptible ECOV) "INTERRUPTIBLE" "NON-INTERRUPIBLE") "")
              (if (min-viewing-time ECOV)
                (format nil "~6,2f min" (* 1440 (min-viewing-time ECOV)))))
      (format stream "~%  max vis interval: ~6,2f min"
              (* 1440 (max-visibility-interval-size ECOV sample-list)))
      ) ;end when
    
    (with-orb-viewing-sample-list sample-list
      (let ((suit-value (compute-orbital-viewing-suitability 
                         ECOV
                         :sample orb-viewing-sample
                         :stream stream)))
        
        ;; save the computed suitability on the interval list
        (if (null interval-list) (push (list *minus-infinity* sample-start 1) 
                                       interval-list))
        (push (list sample-start sample-end suit-value) interval-list)
        ))
    
    ;; turn the interval list into a pcf
    (cond ((null interval-list) (setf result *unity-pcf*))
          (t (push (list (second (first interval-list)) *plus-infinity* 1) 
                   interval-list)
             (setf result (pcf-from-interval-list interval-list))))
    
    ;; store in cache
    (if cache-results
      (put-in-event-cache 
       ECOV (get-viewing-parms-for-event-calculation ECOV) result))
    
    ;; return pcf
    result))


(defmethod COMPUTE-ORBITAL-VIEWING-SUITABILITY
  ((ECOV event-calc-orb-viewing) 
   &key
   sample
   (stream nil)
   &allow-other-keys)
  
  "Compute suitability value for specified orb-viewing sample.  If :stream
    is non-nil, write information about calculation to stream."
  
  (with-orb-viewing-sample sample
    (let* ((vis-duration (summed-interval-duration
                          (pcf-non-zero-intervals vis-pcf)))
           (vis-to-total-ratio (/ vis-duration (- n2 n1)))
           suit-value
           (dark-term 1)
           (north-term 1)
           (view-term 1))
      
      ;; adjust for dark time if required
      (if (dark ECOV)
        (setf dark-term 
              (dark-suit-factor ECOV n1 n2 vis-pcf dark-pcf)))
      
      ;; adjust for preference for viewing in northern hemisphere
      (if (northern-hemisphere-only ECOV)
        (setf north-term 
              (north-suit-factor ECOV n1 n2 vis-pcf north-pcf)))
      
      ;; adjust for size of viewing interval compared to size of observation
      (if (viewing-time ECOV)
        (setf view-term 
              (min-viewing-suit-factor ECOV n1 n2 vis-pcf dark-pcf)))
      
      ;; get suitability
      (setf suit-value
            (* vis-to-total-ratio dark-term north-term view-term))
      
      ;; round, but don't round to zero
      (setf suit-value
            (if (< suit-value 0.005) ;bigger than this will be rounded to 0.01
              0.005
              (coerce (/ (round suit-value 0.01) 100) 'double-float)))
      
      ;; report if desired
      (when stream
        (format stream "~%~9,4f vis/tot ~5,3f dark ~5,3f north ~5,3f dur ~5,3f tot ~5,3f"
                n1 vis-to-total-ratio dark-term north-term
                view-term suit-value)
        )
      
      suit-value)))

;;; ----------------------------------------------------------------------
;;;                                       detailed methods for orb viewing
;;; ----------------------------------------------------------------------


;;; --- Orbital PCF manipulation routines ---

;;; ORB PCF manipulation functions;
;;; pcfs here are zero outside the orbit start & end (n1 n2) and have at most
;;; one non-zero interval (which may be split across the orbit boundary at n2)

(defun REF-TIME-FROM-ORB-PCF (pcf)

  "Input pcf which is non-zero only within some time range
    return time of zero to non-zero transition
      n1                 n2
      +------------------+   
        |-------| => return t1
        t1      t2 (non-zero between t1 and t2)

      |----|     |-------| => return t3
      t1   t2    t3      t4 (non-zero between t1 and t2, t3 and t4)
     "
  ;; (ref-time-from-orb-pcf (pcf-from-interval-list '((0 1) (5 10)))) => 5
  ;; (ref-time-from-orb-pcf (pcf-from-interval-list '((4 7)))) => 4
  
  (first (convert-orb-pcf-to-start-end pcf)))


(defun CONVERT-PCF-TO-INTERVALS-WRT-TIME (n1 n2 ref-time pcf
                                             &key 
                                             (units :days)
                                             (tolerance 1.0e-6))

  "n1 and n2 are the start and end of the orbit
    ref-time is a time within n1 and n2 (if not, it is adjusted to fall
     within this range and a diagnostic is emitted)
    pcf is a pcf with non-zero intervals which fall within n1 and n2
     (may have arbitrary number of intervals)
    returned value is list of non-zero intervals transformed to be relative 
    to and within the interval ref-time (+ ref-time (- n2 n1)).
    Keyword arg :units specifies output units: values may be 
    :days (default) :hours, :minutes.
    Tolerance is fraction of orbit period for tolerance on equal time values.
    Wraparound is handled properly, i.e. 

       |=====|     |=====| interval spanning end of n1 n2
       +-----------------+
       ^       ^         ^
       n1      ref       n2

    is converted to:

           |==========|
       +-----------------+
       ^                 ^
       ref               ref + (n2-n1)
    "

  (let* ((interval-list (pcf-non-zero-intervals pcf n1 n2))
         (period (- n2 n1))
         (tol (* tolerance period))
         (first-interval (first interval-list))
         (last-interval  (first (last interval-list)))
         (start-of-first-interval (first first-interval))
         (end-of-first-interval   (second first-interval))
         (start-of-last-interval  (first last-interval))
         (end-of-last-interval    (second last-interval))
         ;; same-at-wraparound is t if start of first interval should be
         ;; merged with end of last interval
         (same-at-wraparound (and start-of-first-interval
                                  end-of-last-interval
                                  (<= (abs (- (+ start-of-first-interval period)
                                              end-of-last-interval)) 
                                      tol)))
         (scale (cond ((eq units :minutes) 1440)
                      ((eq units :hours) 24)
                      ((eq units :days) 1)
                      (t (spike-diagnostic
                          (format nil "CONVERT-PCF-TO-INTERVALS-WRT-TIME: units error ~a" 
                                  units))
                         1)))
	 result)

    ;; ensure ref-time is in range n1 to n2
    (when (or (< ref-time n1) (> ref-time n2))
      (spike-diagnostic
       (with-output-to-string (stream)
         (format stream "~%CONVERT-PCF-TO-INTERVALS-WRT-TIME:")
         (format stream " ref-time not in orbit")
         (format stream "  n1: ~a n2: ~a ref-time: ~a" n1 n2 ref-time)
         (format stream "  pcf: ~a" pcf)
         (format stream "  units: ~a tolerance: ~a" units tolerance)))
      ;; adjust times
      (if (< ref-time n1)
        (loop (if (> ref-time n1) (return) (setf ref-time (+ ref-time period)))))
      (if (> ref-time n2)
        (loop (if (< ref-time n2) (return) (setf ref-time (- ref-time period))))))

    (cond 

     ((not interval-list)
      ;; no intervals, return nil
      nil)

     ;; ref-time is at orbit start or end: just shift, scale,
     ;; and return the interval list
     ((or (= ref-time n1) (= ref-time n2))
      (setf result (shift-interval-list interval-list n1))
      ;; scale the times
      (dolist (interval interval-list)
        (setf (first interval)  (* (first interval)  scale))
        (setf (second interval) (* (second interval) scale)))
      ;; return
      result)

     (t ; ref-time within n1 n2
      (let (;; make a shifted copy of the interval list for the interval
            ;; n2, n2+period
            (shifted-interval-list (shift-interval-list interval-list (- period)))
            ;; replicated & shifted interval list (2 periods)
            two-period-interval-list)
        ;; construct the two-period interval list shifted to ref-time origin
        (setf two-period-interval-list
              (shift-interval-list 
               (cond (same-at-wraparound
                      (append (butlast interval-list)
                              (list
                               (list start-of-last-interval
                                     (+ end-of-first-interval period)))
                              (rest shifted-interval-list)))
                     (t (append interval-list shifted-interval-list)))
               ref-time))
        ;; exclude intervals outside [0, period], or of zero duration, and scale
        (setf result nil)
        (dolist (interval two-period-interval-list)
          (cond ((or (< (second interval) 0) (> (first interval) period))
                 ;; skip this interval: out of interval [0,period]
                 )
                ((= (max 0 (first interval)) (min period (second interval)))
                 ;; zero duration interval
                 )
                (t (push (list (* scale (max 0 (first interval)))
                               (* scale (min period (second interval))))
                         result))))
        ;; return
        (nreverse result))))))



(defun CONVERT-ORB-PCF-TO-START-END (pcf)
  
  "For a SINGLE INTERVAL PCF which is possibly split across an orbit boundary:
       n1                        n2
       |-------------------------|
   (1)    *****************
          t1              t2
   (2) *****                ******
       t1  t2               t3   t4
   return the start and end of the interval such that end > start
   Case 1: return list t1 t2
   Case 2: return list t3 (+ t3 (- t4 t3) (- t2 t1))
   If there are 0 intervals or more than two then return nil."
  ;;(convert-orb-pcf-to-start-end '(0 1 2 0 8 1 10 0)) => (8 12)
  ;;(convert-orb-pcf-to-start-end '(0 1 2 0)) => (0 2)
  ;;(convert-orb-pcf-to-start-end '(0 0)) => nil
  ;;(convert-orb-pcf-to-start-end '(0 1 2 0 8 1 10 0 12 1 15 0)) => nil
  
  (let* ((intervals (pcf-non-zero-intervals pcf))
         (interval-count (list-length intervals))
         )
    (cond ((= 0 interval-count) nil)
          ((= 1 interval-count)
           (list (first (first intervals)) (second (first intervals))))
          ((= 2 interval-count)
           (let ((t1 (first (first intervals)))
                 (t2 (second (first intervals)))
                 (t3 (first (second intervals)))
                 (t4 (second (second intervals))))
             (list t3 (+  t4 (- t2 t1)))))
          (t (spike-diagnostic
              (with-output-to-string (stream)
                (format stream "CONVERT-ORB-PCF-TO-START-END: > 2 intervals")
                (format stream "~%pcf: ~a, intervals ~a" pcf intervals)))
             nil))))




(defun BRIGHT-DARK-BRIGHT-TIMES (n1 n2 vis-pcf vis-dark-pcf)
  
  "input vis-pcf (1 when target visible, 0 when not) and vis-dark-pcf (1 when
    observer in shadow and visible, 0 otherwise), return multiple values
    for up to four interval durations :
     bright, dark, bright, dark
    Intervals which don't exist have duration 0."
  
  (let* (;; ref-time is start of visibility
         (ref-time (ref-time-from-orb-pcf vis-pcf))
         (vis-int (first (convert-pcf-to-intervals-wrt-time 
                          n1 n2 ref-time vis-pcf)))
         (vis-dark-intervals (convert-pcf-to-intervals-wrt-time 
                              n1 n2 ref-time vis-dark-pcf))
         (vis-dark1-int (first vis-dark-intervals))
         (vis-dark2-int (second vis-dark-intervals))
         ;; there is only one vis-int
         (vis-start (first vis-int))
         (vis-end (second vis-int))
         ;; there can be 0, one, or two vis-dark-int
         (vis-dark1-start (first vis-dark1-int))
         (vis-dark1-end (second vis-dark1-int))
         (vis-dark2-start (first vis-dark2-int))
         (vis-dark2-end (second vis-dark2-int))
         ;; for returned value
         (t1 0)(t2 0)(t3 0)(t4 0)
         )
        
    (cond 
     
     ;; no dark intervals
     ((not vis-dark1-int)
      (setf t1 (- vis-end vis-start)))
     
     ;; one dark interval
     ((and vis-dark1-int (not vis-dark2-int))
      (cond
       ((and (> vis-dark1-start vis-start) (< vis-dark1-end vis-end))
        ;; vis   +------------------+
        ;; dark      +--------+
        (setf t1 (- vis-dark1-start vis-start))
        (setf t2 (- vis-dark1-end vis-dark1-start))
        (setf t3 (- vis-end vis-dark1-end)))
       
       ((> vis-dark1-start vis-start)
        ;; vis   +------------------+
        ;; dark      +--------------+
        (setf t1 (- vis-dark1-start vis-start))
        (setf t2 (- vis-dark1-end vis-dark1-start)))
       
       ((< vis-dark1-end vis-end)
        ;; vis   +------------------+
        ;; dark  +--------+
        (setf t2 (- vis-dark1-end vis-dark1-start))
        (setf t3 (- vis-end vis-dark1-end)))
       
       (t 
        ;; vis   +------------------+
        ;; dark  +------------------+
        (setf t2 (- vis-dark1-end vis-dark1-start)))))
      
      ;; two dark intervals
     ((and vis-dark1-int vis-dark2-int)
      ;; vis   +------------------+
      ;; dark     +-----+    +----+
      ;;        t1  t2    t3  t4
      (setf t1 (- vis-dark1-start vis-start))
      (setf t2 (- vis-dark1-end vis-dark1-start))
      (setf t3 (- vis-dark2-start vis-dark1-end))
      (setf t4 (- vis-dark2-end vis-dark2-start))))

    (values t1 t2 t3 t4)))


(defun TRACKER-CLEAR-AT-END-OF-OCCULTATION (n1 n2 vis-pcf tracker-vis-pcf clear-tolerance)

  "Input orbit start & end n1 n2, target vis-pcf, and tracker-vis-pcf,
    return duration that tracker is clear (tracker-vis-pcf 1) at end of occultation
    interval (vis-pcf 0).  Duration returned is in days.
    If the end of tracker clear is within CLEAR-TOLERANCE of the
    end of occultation, the tracker clear interval is returned, otherwise 0.
    Set CLEAR-TOLERANCE to zero to force tracker clear precisely at
    end of occultation

     Occultation interval    |-----------------------|
                             o1                      o2
                                               |-----| c = clear tolerance
                                               o2-c
                                |----------------|
     Tracker clear interval     t1               t2
     during occultation
  
     Return t2-t1 if t2 is between o2-c and o2"

  (let* ((invis-pcf (pcf-multiply
		     (pcf-create-with-single-interval n1 n2 1 0)
		     (pcf-transform vis-pcf #'(lambda (x) (if (= 0 x) 1 0)))))
	 (tracker-clear-in-occ-pcf (pcf-multiply tracker-vis-pcf invis-pcf))
	 (ref-time (ref-time-from-orb-pcf invis-pcf))
	 (occ-int (first (convert-pcf-to-intervals-wrt-time n1 n2 ref-time invis-pcf)))
	 (tracker-int (first (convert-pcf-to-intervals-wrt-time n1 n2 ref-time tracker-clear-in-occ-pcf)))
	 ;; only if interval ends match is tracker clear at end of occultation
	 (result 
	  (if (and tracker-int 
		   occ-int 
		   (>= (second tracker-int) 
		       (- (second occ-int) clear-tolerance)))
	      (- (second tracker-int) (first tracker-int))
	    0)))
    ;;(format t "~%ref-time ~a occ-int ~a tracker-int ~a" ref-time occ-int tracker-int)
    result))


;;; --- Specialized sample-list functions ---

;;; These functions take as input a sample list such as that generated
;;; by orb-viewing-sample-list, then compute PCFs from it.  


(defun TRACKER-CLEAR-END-OCC-PCF (FTEC &key sample-list sensor clear-tolerance sensor2)
  
  "Return PCF value that is time (minutes) that tracker is clear at end of occultation.  
    Also obeys sun and moon constraints:  suitability is 0 if either angle violated.
    If two sensors are supplied (sensor2 is specified), then the vis-pcfs for the two
    sensors are multiplied before computing suitability.  This is therefore the two-
    tracker availability."
  
  (let ((sun-limit   (get-sensor-limit-angle FTEC sensor  :sun))
	(sun-limit2  (if sensor2 (get-sensor-limit-angle FTEC sensor2 :sun)))
	(moon-limit  (get-sensor-limit-angle FTEC sensor  :moon))
	(moon-limit2 (if sensor2 (get-sensor-limit-angle FTEC sensor2 :moon)))
	(suit nil))
    (cond (sensor
	   (with-orb-viewing-sample-list
	    sample-list
	    (if (null suit) (push (list *minus-infinity* sample-start 0) suit))
	    (push (list sample-start sample-end 
			;; zero if sun or moon violated
			(cond 
			 ;; suitability is zero if any sun/moon angles violated
			 ((and sun-limit (<= (sensor-sun-angle sensor) sun-limit)) 0)
			 ((and sensor2 sun-limit2 (<= (sensor-sun-angle sensor2) sun-limit2)) 0)
			 ((and moon-limit (<= (sensor-moon-angle sensor) moon-limit)) 0)
			 ((and sensor2 moon-limit2 (<= (sensor-moon-angle sensor2) moon-limit2)) 0)
			 ;; sun/moon OK, so compute suitability
			 (t 
			  ;; convert from days to minutes
			  (* 1440 (tracker-clear-at-end-of-occultation 
				   n1 n2 vis-pcf 
				   (if sensor2
				       (pcf-multiply (sensor-vis-pcf sensor) (sensor-vis-pcf sensor2))
				     (sensor-vis-pcf sensor))
				   clear-tolerance)))))
		  suit))
	   (setf suit (if (null suit) *unity-pcf*
			(pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 0) suit))))
	   suit)
	  (t *zero-PCF*))))


(defun TRACKER-SUN-MOON-AVOIDANCE-PCF (FTEC &key sample-list sensor)
  "return PCF 1 when sensor sun and moon limits are ok, 0 when violated"
  (let ((sun-limit (get-sensor-limit-angle FTEC sensor :sun))
	(moon-limit (get-sensor-limit-angle FTEC sensor :moon))
	(suit nil))
    (cond (sensor
	   (with-orb-viewing-sample-list
	    sample-list
	    (if (null suit) (push (list *minus-infinity* sample-start 1) suit))
	    (push (list sample-start sample-end 
			(if (or (and sun-limit (<= (sensor-sun-angle sensor) sun-limit))
				(and moon-limit (<= (sensor-moon-angle sensor) moon-limit)))
			    0 1))
		  suit))
	   (setf suit (if (null suit) *unity-pcf*
			(pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 1) suit))))
	   suit)
	  (t *unity-PCF*))))


;;; --- Default suitability components for orbital viewing ---

(defmethod DARK-SUIT-FACTOR ((ECOV event-calc-orb-viewing) 
                             n1 n2 vis-pcf dark-pcf)
  
  "Input n1 n2 node crossing times for orbit sample,
    vis-pcf target visibility pcf (1 visible, 0 not)
    dark-pcf observer in shadow pcf (1 in shadow, 0 not),
    return suitability factor for dark viewing"
  
  (let* (;; cvz-tolerance: fraction of orbital period such that if target
         ;; viewing interval is >= this, then it can be assumed in the 
         ;; cvz and there is no viewing time constraint
         (cvz-tolerance 0.999)
         (vis-dark-pcf (pcf-multiply vis-pcf dark-pcf))
         (vis-intervals (pcf-non-zero-intervals vis-pcf))
         (dark-intervals (pcf-non-zero-intervals dark-pcf))
         (vis-dark-intervals (pcf-non-zero-intervals vis-dark-pcf))
         (vis-start-end-list (convert-orb-pcf-to-start-end vis-pcf))
         (vis-end (second vis-start-end-list))
         (end-of-vis-penalty 0.5)
         factor)

    ;; compute fraction of the shadow interval also in shadow visibility
    ;; i.e. if all shadow viewing occurs while target unocculted, then
    ;; factor is 1. If all while target occulted, then factor is 0.
    (setf factor
          (/ (summed-interval-duration vis-dark-intervals)
             (summed-interval-duration dark-intervals)))

    ;; if in sunlight at the end of the viewing period, reduce
    ;; suitability by a factor of 0.5
    ;; this is only relevant if not in CVZ
    (with-slots (observer) ECOV
      (when (and (< (summed-interval-duration vis-intervals)
                    (* cvz-tolerance (- n2 n1)))
                 (> (angle-to-horizon observer *SUN* vis-end) 0))
        ;; positive angle means sun above horizon as seen from observer:
        ;; i.e. observer is in sunlight at end of visibility period.  
        ;; this is bad, so cut the suitability
        (setf factor (* end-of-vis-penalty factor))))

    factor))


(defmethod NORTH-SUIT-FACTOR ((ECOV event-calc-orb-viewing) 
                              n1 n2 vis-pcf north-pcf)

  "Input n1 n2 node crossing times for orbit sample,
    vis-pcf target visibility pcf (1 visible, 0 not)
    north-pcf observer in northern hemisphere pcf (1 in north, 0 not),
    return suitability factor for northern hemisphere viewing"

  (declare (ignore n1 n2))
  (let* ((vis-north-pcf (pcf-multiply vis-pcf north-pcf))
	 (vis-intervals (pcf-non-zero-intervals vis-pcf))
	 (vis-north-intervals (pcf-non-zero-intervals vis-north-pcf))
	 ;; fraction of the visibility period in north
	 (vis-north-fraction
	   (/ (summed-interval-duration vis-north-intervals)
	      (summed-interval-duration vis-intervals)))
	 ;; penalty should be between 0 and 1: 0 means no penalty, 1 means maximum.
	 (penalty 0.2)
	 factor)
    ;; if vis north fraction is 1, then all visibility is in northern hemisphere
    ;; if zero, then all in south
    ;; apply penalty factor for the difference from 1
    ;; vis-north-fraction 1 => factor is 1
    ;; vis-north-fraction 0 => factor is 1-penalty
    (setf factor (- 1 (* penalty (- 1 vis-north-fraction))))
    factor))


(defmethod MIN-VIEWING-SUIT-FACTOR ((ECOV event-calc-orb-viewing) 
                                    n1 n2 vis-pcf dark-pcf)
  
  "input n1 n2 node crossing times for orbit sample,
    vis-pcf target visibility pcf (1 visible, 0 not)
    return suitability factor based on size of viewing interval"
  
  (let (;; cvz-tolerance: fraction of orbital period such that if target
        ;; viewing interval is >= this, then it can be assumed in the 
        ;; cvz and there is no viewing time constraint
        (cvz-tolerance 0.999)
        ;; returned factor to multiply other suitability terms for viewing-time
        (factor 1))

    (with-slots (dark
                 viewing-time
                 interruptible
                 min-viewing-time
                 ;; viewing-table not used in this version
                 ;; viewing-table 
                 )
      ECOV

      (when viewing-time
        ;; only calculate if a viewing time is specified

        (let* (;; the pcf representing the time during which the observation can be made
               (viewing-pcf (pcf-multiply 
                             vis-pcf 
                             (if dark dark-pcf *unity-PCF*)))
               (viewing-intervals (pcf-non-zero-intervals
                                   viewing-pcf n1 n2))
               (viewing-intervals-duration
                (summed-interval-duration viewing-intervals)))
        
          (cond
           
           ;; in CVZ?
           ((>= viewing-intervals-duration (* cvz-tolerance (- n2 n1)))
            ;; yes, return unity factor
            (setf factor 1))

           (interruptible
            ;; an interruptible observation: constrain only if there is a 
            ;; min-viewing-time specified
            (setf factor 
                  (cond ((and min-viewing-time
                              (> min-viewing-time viewing-intervals-duration))
                         0)
                        (t 1)))
            )

           (t ;; non-interruptible
            ;; constraint if visibility < viewing time
            (setf factor
                  (cond ((> viewing-time viewing-intervals-duration) 0)
                        (t 1)))))))

      ;; returned factor
      factor)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;       Methods for computing moving target orbital viewing constraints
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun INTEGRATE-VISIBILITY-WINDOWS-WITH-ORBITAL-PCF (ECOV plain-pcf)
  
  "Adjusts the orbital viewing pcf for a moving target to reflect moving 
    target visibility windows.  Adjustments are made for short visibility 
    windows to reflect the fact that orbital event samples do not have 
    certain zero points (we do not know where the orbits are placed in absolute
    time). In addition, adjustments are made to short windows which are 
    interruptible.   The resultant pcf will be zero when the target visibility 
    window is zero and possibly non-zero otherwise"
  
  (let ((interval-list (list (list *minus-infinity*  (start-date ECOV) 1)))
        (visibility-intervals (pcf-non-zero-intervals 
                               (visibility-pcf (astro-object ECOV))))
        (temp-pcf nil)
        (final-pcf nil))
    
    (dolist (interval visibility-intervals)
      (let* ((tjd-start (car interval))
             (tjd-end (cadr interval))
             (window-time (- tjd-end tjd-start))
             (mid-point-of-window (+ tjd-start (/ window-time 2))))
        
        (cond 
         
         ((and (in-orbit-model-range mid-point-of-window)
               (< (start-date ecov) mid-point-of-window)
               (> (end-date ecov) mid-point-of-window))
          (with-orb-viewing-sample
            (orb-viewing-sample ecov mid-point-of-window)
            (let* ((orbit-start n1)
                   (orbit-end   n2)
                   (orbit-time (- orbit-end orbit-start)))
              ;;(format t "~%Looking at moving target visibility window start ~s end ~s orbit-time ~s~%"
              ;;	 tjd-start tjd-end orbit-time)
              ;;(break)
              
              (cond 
               
               ;; Is the visibility window small enough to look at?
               ;; Code for both interruptible and non
               ((<= window-time (* 2 orbit-time)) 
                (let* (;; If we combine the north pcf with vis and dark then saa 
                       ;; avoidance becomes a hard constraint for moving targets. 
                       ;; For now saa avoidance is ignored when combining with 
                       ;; visibility intervals
                       (combined-pcf 
                        (pcf-multiply vis-pcf (if (dark ECOV) dark-pcf *unity-PCF*)))
                       (non-zero-duration 
                        (cond ((multiple-suitability-windows-in-orbit-p 
                                combined-pcf orbit-start orbit-end)
                               (PCF-longest-non-zero-interval combined-pcf))
                              (t (summed-interval-duration 
                                  (pcf-non-zero-intervals combined-pcf)))))
                       (occultation-time (- orbit-time non-zero-duration))
                       (suitability 
                        (suitability-of-moving-target-window 
                         (viewing-time ECOV) window-time orbit-time
                         occultation-time))
                       )                  ; end let* bindings
                  
                  ;;(format t "~%viewing ~s window ~s orbit ~s occultation ~s suitability ~s~%"
                  ;;     (viewing-time ECOV) window-time orbit-time occultation-time suitability)
                  
                  (push (list tjd-start tjd-end suitability) interval-list)))
               
               ((not (interruptible ECOV))
                ;; i.e. the exposure is not interruptible
                (push (list tjd-start tjd-end 1) interval-list))
               
               ;; interruptible moving target window of length greater than 
               ;; two orbits
               (t 
                (let* (;; subtract one for the float in the orbit model
                       (orbits-in-window (floor (- (/ window-time orbit-time) 1))) 
                       ;; If we combine the north pcf with vis and dark then saa 
                       ;; avoidance becomes a hard constraint formoving targets. 
                       ;; For now saa avoidance is ignored when combinng with 
                       ;; visibility intervals
                       (combined-pcf 
                        (pcf-multiply vis-pcf (if (dark ECOV) dark-pcf *unity-PCF*)))
                       (non-zero-duration (summed-interval-duration 
                                           (pcf-non-zero-intervals combined-pcf)))
                       (viewing-time-in-visibility-window 
                        (* orbits-in-window non-zero-duration)))   ; end let bindings
                  
                  (cond ((> (viewing-time ECOV) 
                            viewing-time-in-visibility-window)
                         (push (list tjd-start tjd-end 0.01) interval-list))
                        (t 
                         (push (list tjd-start tjd-end 1) interval-list))))))))
          ) ;end first cond clause
         
         (t
          (push (list tjd-start tjd-end 1) interval-list)))))   ; end loop over intervals
    
    (setf final-pcf
          (cond (interval-list 
                 (push (list (end-date ECOV) *plus-infinity* 1) interval-list)
                 (setf temp-pcf (pcf-from-interval-list interval-list))
                 (pcf-multiply temp-pcf plain-pcf))
                (t
                 plain-pcf)))
    
    (put-in-event-cache
     ECOV (get-viewing-parms-for-event-calculation ECOV) final-pcf)
    
    ;; returned value
    final-pcf))


(defun MULTIPLE-SUITABILITY-WINDOWS-IN-ORBIT-P (pcf orbit-start orbit-end)
  
  "Returns t or nil depending on whether or not the pcf for the orbit 
    contains more than one non-zero regions. The function is complicated by 
    the periodic nature of orbital pcfs. In particular if the pcf contains 
    two regions one which abut the start and end of the orbit then the pcf 
    has only one non zero interval"
  
  (let* ((intervals (pcf-non-zero-intervals pcf))
         (number-of-intervals (list-length intervals)))
    (cond ((< number-of-intervals 2) nil)
          ((> number-of-intervals 2) t)
          (t
           (cond ((and (equal (caar intervals) orbit-start)
                       (equal (nth 1 (nth 1 intervals)) orbit-end)) nil)
                 (t t))))))


(defun SUITABILITY-OF-MOVING-TARGET-WINDOW (exposure-time
                                            window-time
                                            orbit-time
                                            occultation-time)
  
  "Determines the suitability of a single moving target window. 
    Based upon the idea that all possible zero points of the orbit are 
    equally likely."
  
  (cond ((> exposure-time window-time) 0)
        ((> exposure-time (- orbit-time occultation-time)) 0)
        ((equal occultation-time 0) 1)
        ((> window-time (* 2 orbit-time)) 1)
        ((> window-time (+  (* 2 exposure-time) occultation-time)) 1)
        (t (- 1 (/ (- exposure-time 
                      (- window-time (+ occultation-time exposure-time)))
                   orbit-time)))))


;;; ----------------------------------------------------------------------
;;;                                         Methods for EVENT-CALC-SUN
;;; ----------------------------------------------------------------------

(defmethod SET-SUN-EXCLUSION-FOR-EVENT-CALCULATION
	   ((ECS event-calc-sun)
            &key
	    sun-angle-table 
            interval
            &allow-other-keys)
  
  "store parameters for sun constraint.  return t if ok, nil if error"
  
  (let ((ok t))
    
    (with-slots (sun-constraint sun-suit-table
                                sun-sample-interval) ECS
      
      ;; null old values
      (setf sun-constraint nil sun-suit-table nil)
      (setf sun-sample-interval interval)

      ;; store new values if table supplied
      (when sun-angle-table
	(cond
	  ((validate-constraint-interval-table sun-angle-table)
	   (setf sun-constraint t
		 sun-suit-table (convert-angle-table-to-pcf sun-angle-table)))
	  ;; not valid table: spike-diagnostic, and return nil
	  (t (spike-diagnostic (format nil "invalid format for sun-angle-table ~a" sun-angle-table))
	     (setf ok nil)))))

    ;; returned value
    ok))

(defmethod SUN-SAMPLE-INTERVAL ((E event-calc-sun))
  "Return sample interval for constraint"
  (or (slot-value E 'sun-sample-interval)
      (default-sample-interval E)))

(defmethod SUN-SAMPLE-COUNT ((E event-calc-sun))
  "return number of samples"
  (1+ (ceiling (/ (- (end-date E) (start-date E)) 
                  (sun-sample-interval E)))))


(defmethod GET-SUN-EXCLUSION-FOR-EVENT-CALCULATION
  ((E event-calc-sun))
  "return list charactizing computed sun constraint"
  (rationalize-numbers-in-nested-list     
   (append '(:sun)
           (get-target-for-event-calculation e)
           (get-dates-for-event-calculation e)
           (list (sun-sample-interval e)
                 (replace-minus-infinity-in-pcf-by-symbol 
                  (slot-value e 'sun-suit-table))))))


(defmethod SUN-CONSTRAINT-PCF ((ECS event-calc-sun))
  "get sun constraint pcf either from cache or by computing it"
  (or (get-from-event-cache
       ECS (get-sun-exclusion-for-event-calculation ECS))
      (compute-sun-constraint-pcf ECS)))


(defmethod SUN-SAMPLE ((ECS event-calc-sun) time)
  "Compute and return sample at time.  Use WITH-SUN-SAMPLE to access
    returned value"
  (with-slots (sun-constraint
               sun-suit-table
               astro-object) ECS
    (if (not sun-constraint) (return-from sun-sample nil))
    (let* ((angle (3vect-separation 
		    (position-vector *SUN* time)
		    (position-vector astro-object time)))
           (suit (pcf-get-value angle sun-suit-table)))
      (list time angle suit))))


(defmacro WITH-SUN-SAMPLE (sample &rest body)
  
  "Execute body with local bindings from a sun sample.  Bindings are:
    time - time of sample, TJD
    angle - angle of target to sun, radians
    suitability - suitability value from sun-constraint table"
  
  (let* ((samp (gensym))
         (locals nil))
    (if (find-symbol-in-form 'time body)
      (push `(time (first ,samp)) locals))
    (if (find-symbol-in-form 'angle body)
      (push `(angle (second ,samp)) locals))
    (if (find-symbol-in-form 'suitability body)
      (push `(suitability (third ,samp)) locals))
    (push `(,samp ,sample) locals)
    
    `(let* ,locals
       ,@body)))


(defmethod SUN-SAMPLE-LIST ((ECS event-calc-sun))
  "Compute and return sample list for time range.  Use WITH-SUN-SAMPLE-LIST
    and WITH-SUN-SAMPLE to access the returned value"
  (with-slots 
    (start-date
     end-date
     sun-constraint)
    ECS
    
    ;; check if applies: return nil if not
    (if (not sun-constraint) (return-from sun-sample-list nil))
    ;; if applies, but have no dates, then warn
    (when (or (null start-date) (null end-date))
      (warn "no start/end date specified - cannot compute sun constraint")
      (return-from sun-sample-list nil))
    
    ;; sample and build list
    (let* ((sample-interval (sun-sample-interval ECS))
           (sample-count (sun-sample-count ECS))
           (half-sample-interval (* 0.5 sample-interval))
           interval-start interval-end interval-middle
           (result nil))
      
      (dotimes (i sample-count)
        ;;force end of previous interval to be start of current one
        (setf interval-start (if interval-end interval-end 
                                 (- start-date half-sample-interval)))
        (setf interval-middle (+ interval-start half-sample-interval))
        (setf interval-end   (+ interval-start sample-interval))
        
        (push (cons interval-start
                    (cons interval-end (sun-sample ECS interval-middle)))
              result))
      
      ;; returned result
      (nreverse result))))


(defmacro WITH-SUN-SAMPLE-LIST (sample-list &rest body)

  "Execute body once for each sample in sample-list, with local bindings
    as specified by WITH-SUN-SAMPLE, but in addition the bindings:
     sample-start and sample-end (TJD)"

  (let* ((_sample (gensym))
         (locals nil))
    (if (find-symbol-in-form 'sample-start body)
      (push `(sample-start (first ,_sample)) locals))
    (if (find-symbol-in-form 'sample-end body)
      (push `(sample-end (second ,_sample)) locals))
    
    `(dolist (,_sample ,sample-list)
       (let ,locals
         (with-sun-sample (cddr ,_sample) ,@body)))))


(defmethod COMPUTE-SUN-CONSTRAINT-PCF ((ECS event-calc-sun))
  "compute and return orbital viewing pcf for the interval and constraint
    parameters recorded in the event-calc-sun object"
  (with-slots (sun-constraint) ECS
    
    (cond 
     ((not sun-constraint) *unity-PCF*)
     
     (t ;; sample and build pcf
      (let ((interval-list nil)
            result)
        
        ;; iterate over sun sample list and build interval list
        (with-sun-sample-list
          (sun-sample-list ECS)
          (if (null interval-list) 
            (push (list *minus-infinity* sample-start 1) interval-list))
          (push (list sample-start sample-end suitability) interval-list))
        
        ;; build pcf
        (cond ((null interval-list)
               (setf result *unity-pcf*))
              (t (push (list (second (first interval-list)) *plus-infinity* 1) 
                       interval-list)
                 (setf result (pcf-from-interval-list interval-list))))
        
        ;; save in cache
        (put-in-event-cache 
         ECS (get-sun-exclusion-for-event-calculation ECS) result)
        
        ;; returned pcf
        result)))))


;;; ----------------------------------------------------------------------
;;;                                         Methods for EVENT-CALC-MOON
;;; ----------------------------------------------------------------------


(defmethod SET-MOON-EXCLUSION-FOR-EVENT-CALCULATION
	   ((ECM event-calc-moon)
	    &key 
            moon-angle-table 
            interval
            &allow-other-keys)

  "store parameters for moon constraint.  return t if ok, nil if error"

  (let ((ok t))
    
    (with-slots (moon-constraint moon-suit-table moon-sample-interval) ECM
      
      ;; null old values
      (setf moon-constraint nil moon-suit-table nil)
      
      (setf moon-sample-interval interval)

      ;; store new values if table supplied
      (when moon-angle-table
	(cond
	  ((validate-constraint-interval-table moon-angle-table)
	   (setf moon-constraint t
		 moon-suit-table (convert-angle-table-to-pcf moon-angle-table)))
	  ;; not valid table: spike-diagnostic, and return nil
	  (t (spike-diagnostic (format nil "invalid format for moon-angle-table ~a" moon-angle-table))
	     (setf ok nil)))))

    ;; returned value
    ok))


(defmethod MOON-SAMPLE-INTERVAL ((E event-calc-moon))
  "Return sample interval for constraint"
  (or (slot-value E 'moon-sample-interval)
      (default-sample-interval E)))


(defmethod MOON-SAMPLE-COUNT ((E event-calc-moon))
  "return number of samples"
  (1+ (ceiling (/ (- (end-date E) (start-date E)) 
                  (moon-sample-interval E)))))


(defmethod GET-MOON-EXCLUSION-FOR-EVENT-CALCULATION
  ((e event-calc-moon))
  "return list charactizing computed moon constraint"
  (rationalize-numbers-in-nested-list     
   (append '(:moon)
           (get-target-for-event-calculation e)
           (get-dates-for-event-calculation e)
           (list  (moon-sample-interval e)
                  (replace-minus-infinity-in-pcf-by-symbol 
                   (slot-value e 'moon-suit-table))))))


(defmethod MOON-CONSTRAINT-PCF ((ECM event-calc-moon))
  "get moon constraint pcf either from cache or by computing it"
  (or (get-from-event-cache
       ECM (get-moon-exclusion-for-event-calculation ECM))
      (compute-moon-constraint-pcf ECM)))


(defmethod MOON-SAMPLE ((ECM event-calc-moon) time)
  "Compute and return sample at time.  Use WITH-MOON-SAMPLE to access
    returned value"
  (with-slots (moon-constraint
               moon-suit-table
               astro-object) ECM
    (if (not moon-constraint) (return-from moon-sample nil))
    (let* ((angle (3vect-separation 
		    (position-vector *moon* time)
		    (position-vector astro-object time)))
           (suit (pcf-get-value angle moon-suit-table)))
      (list time angle suit))))


(defmacro WITH-MOON-SAMPLE (sample &rest body)

  "Execute body with local bindings from a moon sample.  Bindings are:
    time - time of sample, TJD
    angle - angle of target to moon, radians
    suitability - suitability value from moon-constraint table"

  (let* ((samp (gensym))
         (locals nil))
    (if (find-symbol-in-form 'time body)
      (push `(time (first ,samp)) locals))
    (if (find-symbol-in-form 'angle body)
      (push `(angle (second ,samp)) locals))
    (if (find-symbol-in-form 'suitability body)
      (push `(suitability (third ,samp)) locals))
    (push `(,samp ,sample) locals)
    
    `(let* ,locals
       ,@body)))


(defmethod MOON-SAMPLE-LIST ((ECM event-calc-moon))
  "Compute and return sample list for time range.  Use WITH-MOON-SAMPLE-LIST
    and WITH-MOON-SAMPLE to access the returned value"
  (with-slots (start-date end-date moon-constraint) ECM
    
   ;; check if applies: return nil if not
   (if (not moon-constraint) (return-from moon-sample-list nil))
   ;; if applies, but have no dates, then warn
   (when (or (null start-date) (null end-date))
     (warn "no start/end date specified - cannot compute moon constraint")
     (return-from moon-sample-list nil))
    
   ;; sample and build list
   (let* ((sample-interval (moon-sample-interval ECM))
	  (sample-count (moon-sample-count ECM))
	  (half-sample-interval (* 0.5 sample-interval))
	  interval-start interval-end interval-middle
	  (result nil))
      
     (dotimes (i sample-count)
       ;;force end of previous interval to be start of current one
       (setf interval-start (if interval-end interval-end 
			      (- start-date half-sample-interval)))
       (setf interval-middle (+ interval-start half-sample-interval))
       (setf interval-end   (+ interval-start sample-interval))
        
       (push (cons interval-start
		   (cons interval-end (moon-sample ECM interval-middle)))
	     result))

     ;; returned result
     (nreverse result))))


(defmacro WITH-MOON-SAMPLE-LIST (sample-list &rest body)
  
  "Execute body once for each sample in sample-list, with local bindings
    as specified by WITH-MOON-SAMPLE, but in addition the bindings:
     sample-start and sample-end (TJD)"
  
  (let* ((_sample (gensym))
         (locals nil))
    (if (find-symbol-in-form 'sample-start body)
      (push `(sample-start (first ,_sample)) locals))
    (if (find-symbol-in-form 'sample-end body)
      (push `(sample-end (second ,_sample)) locals))
    
    `(dolist (,_sample ,sample-list)
       (let ,locals
         (with-moon-sample (cddr ,_sample) ,@body)))))


(defmethod COMPUTE-MOON-CONSTRAINT-PCF ((ECM event-calc-moon))
  "compute and return orbital viewing pcf for the interval and constraint
    parameters recorded in the event-calc-moon object"
  (with-slots (moon-constraint) ECM
    
    (cond 
     ((not moon-constraint) *unity-PCF*)
     
     (t ;; sample and build pcf
      (let ((interval-list nil)
            result)
        
        ;; iterate over moon sample list and build interval list
        (with-moon-sample-list
          (moon-sample-list ECM)
          (if (null interval-list) 
            (push (list *minus-infinity* sample-start 1) interval-list))
          (push (list sample-start sample-end suitability) interval-list))
        
        ;; build pcf
        (cond ((null interval-list)
               (setf result *unity-pcf*))
              (t (push (list (second (first interval-list)) *plus-infinity* 1) 
                       interval-list)
                 (setf result (pcf-from-interval-list interval-list))))
        
        ;; save in cache
        (put-in-event-cache
         ECM (get-moon-exclusion-for-event-calculation ECM) result)
        
        ;; returned pcf
        result)))))


;;; ----------------------------------------------------------------------
;;;                                  Methods for EVENT-CALC-ZODIACAL-LIGHT
;;; ----------------------------------------------------------------------


(defmethod SET-ZOD-LIGHT-FOR-EVENT-CALCULATION
  ((ECZ event-calc-zodiacal-light)
   &key
   zod-wavelength 
   zod-brightness 
   interval
   &allow-other-keys)
  
  "store parameters for zodiacal light calculation.  If nil, no
    constraint will be computed.  Return t if ok, nil if error."
  ;; units???
  
  (with-slots (zodiacal-light-constraint 
               zod-light-brightness 
               zod-light-wavelength
               zodiacal-light-sample-interval) ECZ
    
    ;; null current slots
    (setf zodiacal-light-constraint nil
          zod-light-brightness nil
          zod-light-wavelength nil
          zodiacal-light-sample-interval nil)
    
    (if (and zod-wavelength zod-brightness)
      (setf zodiacal-light-constraint t
            zod-light-brightness zod-brightness
            zod-light-wavelength zod-wavelength
            zodiacal-light-sample-interval interval))
    
    t ;; no error checking
    ))


(defmethod ZODIACAL-LIGHT-SAMPLE-INTERVAL ((E event-calc-zodiacal-light))
  "Return sample interval for constraint"
  (or (slot-value E 'zodiacal-light-sample-interval)
      (default-sample-interval E)))


(defmethod ZODIACAL-LIGHT-SAMPLE-COUNT ((E event-calc-zodiacal-light))
  "return number of samples"
  (1+ (ceiling (/ (- (end-date E) (start-date E)) 
                  (zodiacal-light-sample-interval E)))))


(defmethod GET-ZOD-LIGHT-FOR-EVENT-CALCULATION
  ((ECZ event-calc-zodiacal-light))
  "return list charactizing computed zodiacal light constraint"
  (rationalize-numbers-in-nested-list     
   (append '(:zod)
           (get-target-for-event-calculation ECZ)
           (get-dates-for-event-calculation ECZ)
           (list  (zodiacal-light-sample-interval ECZ)
                  (slot-value ECZ 'zod-light-brightness)
                  (slot-value ECZ 'zod-light-wavelength)))))


(defmethod ZODIACAL-LIGHT-CONSTRAINT-PCF ((ECZ event-calc-zodiacal-light))
  "get zod light constraint pcf either from cache or by computing it"
  (let ((cache-value 
         (get-from-event-cache 
          ECZ (get-zod-light-for-event-calculation ECZ))))
    (cond ((null cache-value)  ; no cache value
           (compute-zodiacal-light-constraint-pcf ECZ))
          ((listp cache-value)  ; cache-value and it has the right format
           cache-value)
          (t ; theres a value in the cache but its not a pcf
           (compute-zodiacal-light-constraint-pcf ECZ)))))

(defmethod COMPUTE-ZODIACAL-LIGHT-CONSTRAINT-PCF ((ECZ event-calc-zodiacal-light))
  "compute and return orbital viewing pcf for the interval and constraint
    parameters recorded in the event-calc-zodiacal-light object"
  (with-slots (start-date
               end-date
               zodiacal-light-constraint
               zod-light-wavelength
               zod-light-brightness
               astro-object)
    ECZ
    
    ;; check if applies:
    (if (not zodiacal-light-constraint)
      (return-from compute-zodiacal-light-constraint-pcf *unity-PCF*))
    
    (let ((result
           (calculate-zodiacal-light-PCF 
            start-date end-date astro-object
            zod-light-brightness zod-light-wavelength)))
      ;; store
      (put-in-event-cache
       ECZ (get-zod-light-for-event-calculation ECZ) result)
      ;; return
      result)))


;;; ======================================================================
;;; Classes and methods above were generic:  below we construct an new
;;; class which combines all those required.  A "current instance" of 
;;; this new class is created, and the externally callable interface to
;;; event calculation is formulated in terms of this instance
;;; ======================================================================


;;; ----------------------------------------------------------------------
;;;               EVENT-CALC Class:  combination of individual constraints
;;; ----------------------------------------------------------------------

(defclass EVENT-CALC 
  (event-calc-pointing 
   event-calc-dates
   event-calc-cache
   event-calc-orb-viewing
   event-calc-zodiacal-light
   event-calc-sun
   event-calc-moon)
  (
   )
  (:documentation "for recording event calculation parameters & PCFs"))


;;; ----------------------------------------------------------------------
;;;                                                 ORBIT MODEL MANAGEMENT
;;; ----------------------------------------------------------------------

;;; --- Files and Directories

(defvar *ORBIT-ROOT* nil
  "the root directory containing orbit models and event files")

;;; *orbit-root* is set when first referenced:

(defun ORBIT-ROOT ()

  ;; changed 3Nov91 MDJ to use logical names for export spike
  ;; (orbit-root)

  (or *ORBIT-ROOT*

      #+:EXPORT-SPIKE
      (let* ((root (translate-logical-pathname "spike:event-files;*;"))
             (host (pathname-host root))
             (device (pathname-device root))
             (dir (pathname-directory root))
             (name (pathname-name root))
             (type (pathname-type root))
             (version (pathname-type root)))
      (setf *ORBIT-ROOT*
            (make-pathname
             :host      host
             :device    device
             :directory (butlast dir)
             :name      name
             :type      type
             :version   version)))

      #-:EXPORT-SPIKE
      (setf *ORBIT-ROOT*
            (create-machine-independent-path
             :host      'spike
             :device    'event-device
             :directory '$SPIKE-EVENT-FILE-ROOT
             :name      :wild
             :type      :wild))
      ))

;;; Deleted *orbit-independent-event-directory* as this is obsolete - MDJ 15Mar91

;;; --- Current Orbit Model

;; default orbit model
(defvar *DEFAULT-ORBIT-MODEL*                nil)
(setf   *DEFAULT-ORBIT-MODEL*                ":NONE")

(defvar *CURRENT-ORBIT-MODEL*                nil 
  "the name of the current orbit model (string)")

(defun CURRENT-ORBIT-MODEL ()
  "Returns the name of the current orbit model as a string"
  *current-orbit-model*)

(defun SET-CURRENT-ORBIT-MODEL (model)
  "sets the *current-orbit-model* to model/ used w/ a defsetf"
  (setf *current-orbit-model* model))

(defsetf CURRENT-ORBIT-MODEL SET-CURRENT-ORBIT-MODEL)

;;; --- Orbit Model Date Range

(defvar *ORBIT-MODEL-START-DATE*             nil)
(defvar *ORBIT-MODEL-END-DATE*               nil)

(defun IN-ORBIT-MODEL-RANGE (time)
  "Returns t when time falls between *orbit-model-start-date* and
    *orbit-model-end-date*"
  (and *orbit-model-start-date*
       *orbit-model-end-date*
       (>= time *orbit-model-start-date*)
       (<= time *orbit-model-end-date*)))


(defun DATES-OUTSIDE-ORBIT-MODEL (start end)
  "Given two TJD dates, return nil if they are contained
    within the start/end dates of the orbit model.
    If one or both is outside, return non-nil object which is a list
    describing the date mismatch.  If no orbit model dates available,
    return non-nil."

  (let ((result nil))
    (cond ((and *orbit-model-start-date* *orbit-model-end-date*)
           (if (< start  *orbit-model-start-date*)
             (push 'start-before-orbit-model-start result))
           (if (> start *orbit-model-end-date*)
             (push 'start-after-orbit-model-start result))
           (if (> end  *orbit-model-end-date*)
             (push 'end-after-orbit-model-end result))
           (if (< end *orbit-model-start-date*)
             (push 'end-before-orbit-model-start result)))
          (t (push 'no-orbit-model-dates result)))
    result))

;; note hack for Mac below: hope it worked better on other machines MDJ 15Mar91

(defun LIST-ORBIT-MODELS ()
  
  "Return list of valid orbit models (as strings)"
  
  ;; changed 3Nov91 MDJ to use logical names for export spike
  ;; test: (list-orbit-models)
  
  (let ((models
         
         #-:EXPORT-SPIKE
         (mapcar #'pathname-name
                 (dirs-in-directory (machine-dependent-path (orbit-root))))
         
         #+:EXPORT-SPIKE
         (let* ((pathmodel (translate-logical-pathname 
                            #+:ALLEGRO "spike:event-files;z;z.z"
                            #-:ALLEGRO "spike:event-files;*;"))
                (dirpath 
                 #+:ALLEGRO
                 (make-pathname :directory (butlast (pathname-directory pathmodel))
                                :name :wild)
                 #-:ALLEGRO pathmodel))
           (mapcar #'(lambda (x) 
                       #+:ALLEGRO (pathname-name x)
                       #-:ALLEGRO (first (last (pathname-directory x))))
                   (directory dirpath)))
         ))
    models))


(defun VALID-ORBIT-MODEL-P (orbit-model-name)
  
  "A predicate that takes the name of an orbit model as a string (of anycase) 
    or symbol and returns the name of the model (as a string) if there is 
    one, else nil"
  ;; (valid-orbit-model-p "test")
  ;; (valid-orbit-model-p "TEST")
  ;; (valid-orbit-model-p "foo")
  ;; (valid-orbit-model-p :none)
  
  (let ((test 
         (member (string-it orbit-model-name) 
                 (cons (string-it :none) (list-orbit-models)) :test #'equalp)))
    (when test
      (first test))))


(defun ORBIT-MODEL-STATUS (&key (ST t) (TDRSE t) (TDRSW t))
  
  "Display orbit model status to standard output.  Args determine what to
    display:  set to t to display appropriate object, nil to suppress"
  ;; (orbit-model-status)
  
  (declare (special *orbit-model-start-date*
                    *orbit-model-end-date*
                    *ST* *TDRSE* *TDRSW*))
  
  (let ((current-orbit-model (current-orbit-model)))
    
    (cond ((and  current-orbit-model
                 (not (eq current-orbit-model :none)))
           
           (format t "~%Orbit model   ~a~%Valid from  ~a   to   ~a ~%From  <~a>"
                   current-orbit-model
                   (format-abs-time *orbit-model-start-date*)
                   (format-abs-time *orbit-model-end-date*)
                   (GET-ORBIT-MODEL-PATHNAME current-orbit-model))
           
           (when ST
             (format t "~%~% ************ ST Orbit Parameters ************")
             (describe *ST*))
           
           (when TDRSE
             (format t "~% ************ TDRSE Orbit Parameters ************")
             (describe *TDRSE*))
           
           (when TDRSW
             (format t "~% ************ TDRSW Orbit Parameters ************")
             (describe *TDRSW*)))
          
          ((eq current-orbit-model :none)
           (format t "~%Current orbit model is :NONE"))
          
          (t
           (format t "~%No current orbit model is defined")))))


(defun GET-ORBIT-MODEL-PATHNAME (orbit-model)

  "get the path associated w/ the orbit model"

  ;; changed 3Nov91 MDJ to use logical names for export spike
  ;;(get-orbit-model-pathname "test")
  
  #+:EXPORT-SPIKE
  (translate-logical-pathname
   (format nil "spike:event-files;~a;orbit.model"
	   (string-it-downcase orbit-model)))
  
  #-:EXPORT-SPIKE
  (create-machine-dependent-path
   :host      'spike
   :device    'event-device
   :directory (list '$SPIKE-EVENT-FILE-ROOT orbit-model)
   :name      'orbit
   :type      'model)
  
  )


(defun INITIALIZE-EVENT-CALCULATOR (orbit-model &key (reload nil))
  "Initialize event calculation.
    If orbit-model is :none then all is cleared.
    If reload is t then the orbit model is explicitly reloaded from disk."
  ;; (initialize-event-calculator "test")
  ;; (initialize-event-calculator "test" :reload t)
  ;; (initialize-event-calculator :none)

  (when (valid-orbit-model-p orbit-model)
    
    (cond ((eq orbit-model :none)
           (setf (current-orbit-model)   :none)
           (setf *orbit-model-start-date* nil)
           (setf *orbit-model-end-date*   nil))
          (t
           (load-orbit-model orbit-model :reload reload)))))


(defun LOAD-ORBIT-MODEL (orbit-model-name &key (reload nil))

  "Load an orbit model. If the name specified is the same as the current
    orbit model, returns orbit-model as a string in uppercase
   if the orbit-model-name is :none this is a no op. if reload is non-nil 
   the orbit model file is reloaded" 
  ;; (load-orbit-model "test")
  ;; (load-orbit-model :none)
  
  (let (orbit-model ok) 
    (cond ((and reload orbit-model-name
		(not (eq orbit-model-name :none)))
	   (cond ((probe-file (get-orbit-model-pathname orbit-model-name))
		  (load (get-orbit-model-pathname orbit-model-name))
		  (setf orbit-model (string-upcase orbit-model-name)))
		 (t (spike-diagnostic  (format nil "orbit model ~a not found" orbit-model-name) :severity "E")
		    (setf ok nil))))
	  ((and orbit-model-name
		(not (eq orbit-model-name :none))
		(not (string-equal (string-it orbit-model-name) 
                                   (CURRENT-ORBIT-MODEL))))
	   (cond ((probe-file (get-orbit-model-pathname orbit-model-name))
		  (load (get-orbit-model-pathname orbit-model-name))
		  (setf orbit-model (string-upcase orbit-model-name)))
		 (t (spike-diagnostic (format nil "orbit model ~a not found" orbit-model-name)  :severity "E")
		    (setf ok nil))))
	  (t (setf orbit-model (string-upcase orbit-model-name))))
    (values orbit-model ok)))



;;; ----------------------------------------------------------------------
;;;                                                        ALIVENESS TESTS
;;; ----------------------------------------------------------------------

#|
;;; This is a quick aliveness test for most of the basic event calculator
;;; functions

(print *orbit-root*)
(print (orbit-root))
(print *default-orbit-model*)
(print *current-orbit-model*)
(list-orbit-models)
(valid-orbit-model-p "test")
(load-orbit-model "test")
(load-orbit-model "test" :reload t)
(initialize-event-calculator "test")
(initialize-event-calculator "test" :reload t)
(orbit-model-status)
(in-orbit-model-range (dmy-to-time '1-jan-1991))
(in-orbit-model-range (dmy-to-time '1-jan-1891))

;; create event-calc instance
(setf *test-EC* (make-instance 'event-calc))

(set-dates-for-event-calculation
 *test-EC* :start (dmy-to-time '1-jan-1991) :end (dmy-to-time '15-jan-1991)
 :sample 1)

(set-target-for-event-calculation
 *test-EC* :target-name "Fred" :target-ra 90.0 :target-dec 45.0
 :ra-dec-in-degrees t :cell-center nil
 :the-observer *st* :the-observer-name (name *st*))

(set-roll-for-event-calculation
 *test-EC* '(:nominal))

;; these will override any defaults - check that slots set properly
(set-viewing-parms-for-event-calculation
 *test-EC* :orbit-model-name "test"
 :bright-angle (radians 20) :dark-angle (radians 10) 
 :terminator-limit (radians 104)
 :limb-angles-in-degrees nil
 :dark-flag t :dark-sun-limb-limit (radians -1.0)
 :view-duration 0.01
 :view-interruptible t :view-minimum-duration 0.002
 :view-suitability-table '((0 0.5 0)(0.5 1.5 1)(1.5 999 0))
 :north-flag t :high-accuracy-flag t
 :interval 1)

(set-sensor-limits-for-event-calculation
 *test-EC*
 :fhst1 :sun 50 :moon 15 :bright-limb 50 :dark-limb 20
 :angles-in-degrees t)

(set-sun-exclusion-for-event-calculation
 *test-EC* :sun-angle-table '((0 60 0)(60 180 1)) :interval 1)
(set-moon-exclusion-for-event-calculation
 *test-EC* :moon-angle-table '((0 15 0)(15 180 1)) :interval 1/3)
(set-zod-light-for-event-calculation
 *test-EC* :zod-wavelength 5500 :zod-brightness 1e-10
 :interval 1)

(describe *test-EC*)

(event-cache-report *test-EC*)
(print (event-cache-needs-to-be-saved *test-EC*))
(clear-event-cache *test-EC*)

;;; compute constraints
(sun-constraint-pcf *test-EC*)
(moon-constraint-pcf *test-EC*)
(zodiacal-light-constraint-pcf *test-EC*)
(orbital-viewing-pcf *test-EC*)

(event-cache-report *test-EC*)
(print (event-cache-needs-to-be-saved *test-EC*))
(save-event-cache *test-EC* "clipper:temp.cache" :compile nil :verbose t)
(clear-event-cache *test-EC*)
(event-cache-report *test-EC*)

;; restore cache, make sure can read from it
(restore-event-cache *test-EC* "clipper:temp.cache" :verbose t)
(event-cache-report *test-EC*)
(sun-constraint-pcf *test-EC*)
(moon-constraint-pcf *test-EC*)
(zodiacal-light-constraint-pcf *test-EC*)
(orbital-viewing-pcf *test-EC*)
(event-cache-report *test-EC*)

;; test sample list functions
(with-orb-viewing-sample 
  (orb-viewing-sample *test-EC*
                      (dmy-to-time '1-jan-1990)
                      :sensor-name-list '(:fhst1)
                      :named-vector-list '(:v3))
  (format t "~%~%~a~%~a~%~a~%~a~%~a~%~a~%~a~%~a~%~a~%~a"
          north-pcf dark-pcf vis-pcf bright-limb-pcf dark-limb-pcf 
          geo-pcf orb-pcf midnight n2 n1)
  (format t "~%sensor sun ~a ~a ~a"
          (sensor-sun-angle  :fhst1)
          (sensor-moon-angle :fhst1)
          (sensor-vis-pcf    :fhst1))
  (format t "~%v3: ~a" (named-vector-position :v3)))

(setf *test-sample* (orb-viewing-sample *test-EC*
                                        (dmy-to-time '1-jan-1990)
                                        :sensor-name-list '(:fhst1)
                                        :named-vector-list '(:v3)))
(describe-orb-viewing-sample 
 *test-EC* *test-sample* :sensors '(:fhst1) :named-vectors '(:v3))
;; sample list, first without sensors
(setf *test-sample-list* (orb-viewing-sample-list *test-EC*))
(describe-orb-viewing-sample-list *test-EC* *test-sample-list*)
;; test sample list with sensors
(setf *test-sample-list* (orb-viewing-sample-list 
                          *test-EC*
                          :sensor-name-list '(:fhst1)
                          :named-vector-list '(:v3)))
(describe-orb-viewing-sample-list *test-EC* *test-sample-list*
                                  :sensors '(:fhst1))
(with-sun-sample (sun-sample *test-EC* 4100.0)
  (format t "~%time ~a angle ~a suit ~a" time angle suitability)
  (format t "~% angle in degrees ~a" (degrees angle)))
(with-sun-sample-list (sun-sample-list *test-EC*)
  (format t "~%time ~9,4f angle ~6,2f deg suit ~6,2f" 
          time (degrees angle) suitability))
(with-moon-sample (moon-sample *test-EC* 4100.0)
  (format t "~%time ~a angle ~a suit ~a" time angle suitability)
  (format t "~% angle in degrees ~a" (degrees angle)))
(with-moon-sample-list (moon-sample-list *test-EC*)
  (format t "~%time ~9,4f angle ~6,2f deg suit ~6,2f" 
          time (degrees angle) suitability))


;; cache keys
(get-roll-for-event-calculation *test-EC*)
(get-target-for-event-calculation *test-EC*)
(get-dates-for-event-calculation *test-EC*)
(get-viewing-parms-for-event-calculation *test-EC*)
(get-sun-exclusion-for-event-calculation *test-EC*)
(get-moon-exclusion-for-event-calculation *test-EC*)
(get-zod-light-for-event-calculation *test-EC*)


;; misc
(degrees (get-sensor-limit-angle *test-EC* :fhst1 :sun))
(degrees (get-sensor-limit-angle *test-EC* :fhst1 :moon))
(degrees (get-sensor-limit-angle *test-EC* :fhst1 :bright-limb))
(degrees (get-sensor-limit-angle *test-EC* :fhst1 :dark-limb))
(observer *test-EC*)
(observer-name *test-EC*)


;; test cases for specialized functions:


(let ((pcf1 (pcf-from-interval-list '((0 2 1)(7 10 1))))
      (pcf2 (pcf-from-interval-list '((2 5 1)))))
  (print (convert-pcf-to-intervals-wrt-time 0 10 0 pcf1))
  (print (convert-pcf-to-intervals-wrt-time 0 10 1 pcf1))
  (print (convert-pcf-to-intervals-wrt-time 0 10 2 pcf1))
  (print (convert-pcf-to-intervals-wrt-time 0 10 3 pcf1))
  (print (convert-pcf-to-intervals-wrt-time 0 10 3 pcf1 :units :days))
  (print (convert-pcf-to-intervals-wrt-time 0 10 3 pcf1 :units :hours))
  (print (convert-pcf-to-intervals-wrt-time 0 10 3 pcf1 :units :minutes))
  (print (convert-pcf-to-intervals-wrt-time 0 10 0 pcf2))
  (print (convert-pcf-to-intervals-wrt-time 0 10 1 pcf2))
  (print (convert-pcf-to-intervals-wrt-time 0 10 2 pcf2))
  (print (convert-pcf-to-intervals-wrt-time 0 10 3 pcf2))
  )

(setf pcf1 (pcf-from-interval-list '((0 2 1)(7 10 1))))
(convert-pcf-to-intervals-wrt-time 0 10 0 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 1 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 2 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 7 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 8 pcf1)
(setf pcf1 (pcf-from-interval-list '((0 2 1)(7 9.999999 1))))
(convert-pcf-to-intervals-wrt-time 0 10 5 pcf1)
(setf pcf1 (pcf-from-interval-list '((0 2 1) (3 4 1) (7 10 1))))
(convert-pcf-to-intervals-wrt-time 0 10 0 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 10 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 1 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 0.5 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 3 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 0.5 pcf1 :units :days)
(convert-pcf-to-intervals-wrt-time 0 10 0.5 pcf1 :units :hours)
(convert-pcf-to-intervals-wrt-time 0 10 0.5 pcf1 :units :minutes)
(convert-pcf-to-intervals-wrt-time 0 10 0.5 pcf1 :units :foos)
(convert-pcf-to-intervals-wrt-time 0 10 15 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 -5 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 5 pcf1)
(setf pcf1 (pcf-from-interval-list '((1 2 1) (3 4 1) (7 10 1))))
(convert-pcf-to-intervals-wrt-time 0 10 0 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 1 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 2 pcf1)
(convert-pcf-to-intervals-wrt-time 0 10 3 pcf1)

;; test case for bright-dark-bright
(let ((n1 0)(n2 10)
      (vis-pcf (pcf-from-interval-list '((1 5))))
      ;;(vis-pcf (pcf-from-interval-list '((0 1)(7 10))))
      (dark-pcf nil))
  (dotimes (i 10)
    (cond ((<= (+ i 4) n2)
	   (setf dark-pcf (pcf-from-interval-list (list (list i (+ i 4))))))
	  (t
	   (setf dark-pcf (pcf-from-interval-list
			   (list (list n1 (+ n1 (- (+ i 4) n2))) (list i n2))))))
    (format t "~% ~a dark: ~a bdbd: ~a" i 
	    (pcf-non-zero-intervals (pcf-multiply vis-pcf dark-pcf))
	    (multiple-value-list 
             (bright-dark-bright-times n1 n2 vis-pcf 
                                       (pcf-multiply vis-pcf dark-pcf))))))
(setf visz (pcf-from-interval-list '((2 8 1))))
(multiple-value-list
 (bright-dark-bright-times 0 10 visz (pcf-from-interval-list '((2 3 1)))))
(multiple-value-list
 (bright-dark-bright-times 0 10 visz (pcf-from-interval-list '((4 5 1)))))
(multiple-value-list
 (bright-dark-bright-times 0 10 visz (pcf-from-interval-list '((2 3 1)(4 8 1)))))
(multiple-value-list
 (bright-dark-bright-times 0 10 visz (pcf-from-interval-list '((3 5 1)(7 8 1)))))

;; test case for tracker-clear...
(let ((n1 0)(n2 10)
      ;;(vis-pcf (pcf-from-interval-list '((1 5))))
      (vis-pcf (pcf-from-interval-list '((0 1)(7 10))))
      (pcf nil)) ;; tracker-vis-pcf
  (dotimes (i 10)
    (cond ((<= (+ i 4) n2)
	   (setf pcf (pcf-from-interval-list (list (list i (+ i 4))))))
	  (t
	   (setf pcf (pcf-from-interval-list
			   (list (list n1 (+ n1 (- (+ i 4) n2))) (list i n2))))))
    (format t "~% ~a tracker: ~a clear at end: ~a" i 
	    (pcf-non-zero-intervals pcf)
	    (tracker-clear-at-end-of-occultation n1 n2 vis-pcf pcf 0))))
|#

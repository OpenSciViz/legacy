;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; clim-gwindow.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: clim-gwindow.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.1  1992/10/04  13:28:45  johnston
;;; Initial revision
;;;
;;;


;;; ======================================================================
;;;                                                    CLASS: CLIM-GWINDOW

(defclass CLIM-GWINDOW (IMMED-GWINDOW)
  ((page-height
    :initform 792 :initarg :page-height
    :documentation
    "Height of page in pixel units, for inverting the coordinate system so
      y runs up instead of down the screen.  Default is 11*72=792 =
      size of postscript page")
   (text-style
    :initform clim:*default-text-style*
    :documentation 
    "most recently specified text style, used when displaying text")
   )
  (:documentation "CLIM version of gwindow objects"))



;;; ----------------------------------------------------------------------
;;; method modifications


(defmethod y-to-page-vert :around ((g clim-gwindow) y)
  ;; round coords
  (declare (ignore y))
  ;; THIS FLIPS Y FOR CONSISTENCY WITH POSTSCRIPT FILES...
  (- (slot-value g 'page-height) (call-next-method))
  )

(defmethod y-rect-top ((g clim-gwindow))
  (- (slot-value g 'page-height) (slot-value g 'ypage-max)))

(defmethod y-rect-bottom ((g clim-gwindow))
  (- (slot-value g 'page-height) (slot-value g 'ypage-min)))

;;; any references to ypage-min, ypage-max need to be fixed for inversion
;;; of coordinates on CLIM windows

(defmethod BOX-RECT ((g clim-gwindow) &key (linewidth 0.25))
  "draw a box around the drawing rectangle"
  (with-slots (xpage-min xpage-max ypage-min ypage-max page-height) g
    (let ((xy-list nil))
      (push (list     xpage-min  (1+ (- page-height ypage-min))) xy-list)
      (push (list     xpage-min      (- page-height ypage-max))  xy-list)
      (push (list (1+ xpage-max)     (- page-height ypage-max))  xy-list)
      (push (list (1+ xpage-max) (1+ (- page-height ypage-min))) xy-list)
      (push (list     xpage-min  (1+ (- page-height ypage-min))) xy-list)
      (record-polygon g xy-list :frame t :linewidth linewidth))))



(defmethod INITIALIZE-INSTANCE :AFTER ((self clim-gwindow)
                                       &rest initargs)
  (declare (ignore initargs))
  ;; stubbed
  )


;;; --- override method: no action (immediate window)

(defmethod PLAY-GWINDOW-HISTORY ((g clim-gwindow))
  nil)


;;; ----------------------------------------------------------------------
;;; drawing primitives

(defmethod gwindow-prolog ((g clim-gwindow))
  nil)

(defmethod gwindow-wrapup ((g clim-gwindow))
  nil)

(defmethod gwindow-nextpage ((g clim-gwindow))
  nil)

(defmethod gwindow-moveto ((g clim-gwindow) x y)
  ;; moveto is a no-op: the clim window has no current point position
  (declare (ignore x y))
  nil)

(defmethod gwindow-lineto ((g clim-gwindow) x y linewidth dash1 dash2)
  (with-slots (last-x last-y drawing-destination) g
    ;;(format drawing-destination "~%%--- gwindow-lineto")
    (clim:draw-line* drawing-destination 
                     last-x last-y x y
                     :line-thickness linewidth
                     :line-dashes (cond ((and (not dash1) (not dash2))
                                         nil) ;solid
                                        ((and dash1 (numberp dash1) 
                                              dash2 (numberp dash2))
                                         (list dash1 dash2)) ; dash
                                        (t t))   ; default dash
                     )))


(defmethod gwindow-line ((g clim-gwindow) x1 y1 x2 y2 linewidth dash1 dash2)
  ;;(format drawing-destination "~%%--- gwindow-line")
  (with-slots (drawing-destination) g
    (clim:draw-line* drawing-destination 
                     x1 y1 x2 y2
                     :line-thickness linewidth
                     :line-dashes (cond ((and (not dash1) (not dash2))
                                         nil) ;solid
                                        ((and dash1 (numberp dash1) 
                                              dash2 (numberp dash2))
                                         (list dash1 dash2)) ; dash
                                        (t t))   ; default dash
                     )))


(defmethod gwindow-polyline ((g clim-gwindow) xy-list 
                             linewidth dash1 dash2
                             symbol symbol-scale)
  (let ((dashspec (cond ((and (not dash1) (not dash2))
                         nil) ;solid
                        ((and dash1 (numberp dash1) 
                              dash2 (numberp dash2))
                         (list dash1 dash2)) ; dash
                        (t t)))   ; default dash
        )
    (with-slots (drawing-destination) g
      ;;(format drawing-destination "~%%--- gwindow-polyline")
      (let ((prev-x (first (first xy-list)))
            (prev-y (second (first xy-list))))
        ;; plot the curve first
        (dolist (xy (rest xy-list))
          (let ((x (first xy))
                (y (second xy)))
            (clim:draw-line* drawing-destination
                             prev-x prev-y x y
                             :line-thickness linewidth
                             :line-dashes dashspec)
            (setf prev-x x prev-y y)
            ))                          ; end dolist
        
        ;; plot symbol
        (when symbol
          (dolist (xy xy-list)
            (let ((x (first xy))
                  (y (second xy)))
              (dolist (sym symbol)
                (let ((dx1 (* (first sym) symbol-scale))
                      (dy1 (* (second sym) symbol-scale))
                      (dx2 (* (third sym) symbol-scale))
                      (dy2 (* (fourth sym) symbol-scale)))
                  (clim:draw-line* 
                   drawing-destination
                   (+ x dx1) (+ y dy1) (+ x dx2) (+ y dy2)
                   :line-thickness linewidth)
                  )))
            )                           ; end dolist
          )				; end when symbol
        ))))


(defmethod gwindow-polygon ((g clim-gwindow) xy-list 
                            frame fill linewidth dash1 dash2)
  ;; if fill is a number, it is interpreted as a gray
  ;; otherwise it is treated as a valid :ink argument
  (let ((dashspec (cond ((and (not dash1) (not dash2)) nil) ;solid
                        ((and dash1 (numberp dash1) 
                              dash2 (numberp dash2))
                         (list dash1 dash2)) ; dash
                        (t t)))   ; default dash
        (clim-xy nil)
        )
    (with-slots (drawing-destination) g
      ;;(format drawing-destination "~%%--- gwindow-polygon")
      (push (first (first xy-list)) clim-xy)
      (push (second (first xy-list)) clim-xy)
      (dolist (xy (rest xy-list))
        (push (first xy) clim-xy)
        (push (second xy) clim-xy)
        )
      (setf clim-xy (reverse clim-xy))
      
      (when fill
        (clim:draw-polygon* drawing-destination
                            clim-xy
                            :line-thickness 0
                            :closed t :filled t
                            :ink (if (numberp fill)
				     (clim:make-gray-color fill)
				   fill)))
      (when frame
        (clim:draw-polygon* drawing-destination
                            clim-xy
                            :line-thickness linewidth
                            :line-dashes dashspec
                            :closed t :filled nil))
      )))


(defmethod gwindow-polyseg ((g clim-gwindow) seg-list 
                            linewidth dash1 dash2)
  (let ((dashspec (cond ((and (not dash1) (not dash2)) nil) ;solid
                        ((and dash1 (numberp dash1) 
                              dash2 (numberp dash2))
                         (list dash1 dash2)) ; dash
                        (t t)))   ; default dash
        )
    (with-slots (drawing-destination) g
      ;;(format drawing-destination "~%%--- gwindow-polyseg")
      
      (dolist (seg seg-list)
        (let ((x1 (first seg)) (y1 (second seg))
              (x2 (third seg)) (y2 (fourth seg)))
          (clim:draw-line* drawing-destination
                           x1 y1 x2 y2
                           :line-thickness linewidth
                           :line-dashes dashspec)
          )))))

(defun CONVERT-PS-FONT-FOR-CLIM (font-name font-size)
  "convert postscript-style font and size into CLIM text style specification:
    suppoft Helvetica, Times, Courier, bold and italic, and integral sizes"
  ;; examples:
  ;; (convert-ps-font-for-clim "helvetica-bold" 12)
  ;; (convert-ps-font-for-clim "helvetica" 9)
  ;; (convert-ps-font-for-clim "helvetica" 6)
  ;; (convert-ps-font-for-clim "courier" 12)
  ;; (convert-ps-font-for-clim "times-bold-italic" 17)
  ;; (convert-ps-font-for-clim "helvetica-italic" 18)
  (let* ((pos-dash (position #\- font-name))
         (the-name (if pos-dash (subseq font-name 0 pos-dash)
                       font-name))
         (the-style (if pos-dash (subseq font-name (1+ pos-dash)
                                         (length font-name))
                        "plain"))
         (family (cond ((equalp the-name "Helvetica") :sans-serif)
                       ((equalp the-name "Times") :serif)
                       ((equalp the-name "Courier") :fix)
                       (t :sans-serif)))
         ;; note: don't support (:bold :italic)
         (face (cond ((equalp the-style "bold") :bold)
                     ((equalp the-style "oblique") :italic)
                     (t nil)))
         (size (cond ((< font-size 6) :tiny)
                     ((= font-size 6) :tiny)
                     ((= font-size 7) :tiny)
                     ((= font-size 8) :very-small)
                     ((= font-size 9) :very-small)
                     ((= font-size 10) :small)
                     ((= font-size 11) :normal)
                     ((= font-size 12) :normal)
                     ((= font-size 13) :normal)
                     ((= font-size 14) :large)
                     ((= font-size 15) :large)
                     ((= font-size 16) :large)
                     ((= font-size 17) :large)
                     ((= font-size 18) :very-large)
                     ((= font-size 19) :very-large)
                     ((= font-size 20) :very-large)
                     ((= font-size 21) :very-large)
                     ((= font-size 22) :very-large)
                     (t :huge)
                     ))
         
         )
  (clim:make-text-style family face size)))

(defmethod gwindow-font ((g clim-gwindow) font-name font-size)
  (with-slots (text-style) g
    (let* ((the-style (convert-ps-font-for-clim font-name font-size))
           )
      ;;(setf (clim:medium-text-style drawing-destination) the-style)
      (setf text-style the-style))))

(defmethod gwindow-leftshow ((g clim-gwindow) x y string)
  (with-slots (drawing-destination text-style) g
    (clim:draw-text* drawing-destination
                     string x y :align-x :left
                     :text-style text-style)))

(defmethod gwindow-rightshow ((g clim-gwindow) x y string)
  (with-slots (drawing-destination text-style) g
    (clim:draw-text* drawing-destination
                     string x y :align-x :right
                     :text-style text-style)))

;;; ======================================================================
;;;                                                            CONSTRUCTOR

(defun MAKE-CLIM-GWINDOW (&key (stream t) (page-height 792))
  ;; return gwindow instance
  (let ((instance (make-instance 'clim-gwindow
                                 :page-height page-height)))
    (with-slots (drawing-destination) instance
      (setf drawing-destination stream))
    instance))


;;; ======================================================================
;;;                                             CHANGE DRAWING DESTINATION

(defmethod SET-DRAWING-DESTINATION ((g clim-gwindow) new-dest)
  "change the destination..."
  (setf (slot-value g 'drawing-destination) new-dest))
 

;;; ======================================================================
;;;                                                              TEST CASE
;;;
;;; the following is a CLIM applicatio that illustrates drawing with the
;;; gwindow code.  To run it, eval the commented region then use:
;;;   (make-clim-gwindow-test *clim-root*)
;;; where *clim-root* is a root window, created e.g. by
;;;   (setf *clim-root* (clim:open-root-window :MCL))
;;; (or :CLX if on Sun).
;;; 

#|

(clim:define-application-frame CLIM-GWINDOW-TEST
  ()                                    ; superclasses
  ;; state
  ((gw :initform nil)                   ; gwindow instance
   (curve :initform nil)
   )
  (:panes
   ((menu :command-menu)
    (picture :application
             :scroll-bars :both
             :default-text-style '(:sans-serif :roman :small)
             :display-function 'display-clim-picture
             :display-after-commands nil)
    ))

  (:layout
   ((default
      (:column 1
               (menu :compute)
               (picture :rest))))))


(defmethod DISPLAY-CLIM-PICTURE ((frame clim-gwindow-test) stream)
  (with-slots (gw curve) frame

    ;; Note: the record-... functions work in terms of device coordinates
    ;; and so are not inverted for output.
    ;; i.e. the origin is at window top left, and y runs down.

    (record-prolog gw)
    
    (let ((x1 10)(y1 10)(x2 30)(y2 50)(dx 10))
      (record-line gw x1 y1 x2 y2)
      (incf x1 dx)(incf x2 dx)
      (record-line gw x1 y1 x2 y2 :linewidth 2)
      (incf x1 dx)(incf x2 dx)
      (record-line gw x1 y1 x2 y2 :linewidth 1    :dash1 1 :dash2 0)
      (incf x1 dx)(incf x2 dx)
      (record-line gw x1 y1 x2 y2 :linewidth 0.25 :dash1 2 :dash2 0)
      (incf x1 dx)(incf x2 dx)
      (record-line gw x1 y1 x2 y2 :linewidth 0.25 :dash1 3 :dash2 0)
      )

    (let ((plist '((10 60)(20 65)(30 70)(40 75))))
      (flet ((shiftx (plist how-much)
               (mapcar #'(lambda (x) (list (+ (first x) how-much) (second x)))
                       plist)))
        (record-polyline gw plist)
        (record-polyline gw (shiftx plist 20)
                         :symbol '((1 1 -1 -1) (1 -1 -1 1)) :symbol-scale 3)
        (record-polyline gw (shiftx plist 40)
                         :symbol '((1 1 -1 -1) (1 -1 -1 1)
                                   (-1 0 1 0) (0 -1 0 1)) :symbol-scale 3)
        ))

    (let ((plist '((0 0) (0 30) (30 30) (30 0) (0 0))))
      (flet ((shiftxy (plist dx dy)
               (mapcar #'(lambda (p) (list (+ (first p) dx) (+ (second p) dy)))
                       plist)))
        (record-polygon gw (shiftxy plist 10 100) :frame t)
        (record-polygon gw (shiftxy plist 20 110) :frame t)
        (record-polygon gw (shiftxy plist 30 120) :fill 0.25 :frame t)
        (record-polygon gw (shiftxy plist 40 130) :frame nil :fill 0.5)
        (record-polygon gw (shiftxy plist 50 140) :frame nil :fill 0)
        ))
    
    (let ((x 20)(y 200)(dy 15))
      (record-font gw "Helvetica-Bold" 9)
      (record-leftshow gw x y "Helv bold 9: Now is the time for all good men")
      (record-rightshow gw x y "foo")
      (record-font gw "Helvetica" 6)
      (incf y dy)
      (record-leftshow gw x y "Helv 6: Now is the time for all good men")
      (record-font gw "Helvetica-Oblique" 10)
      (incf y dy)
      (record-leftshow gw x y "Helv obl 10: Now is the time for all good men")
      (record-font gw "Helvetica" 9)
      (incf y dy)
      (record-leftshow gw x y "Helv 9: Now is the time for all good men")
      )

    ;; higher level routines work in t-v coord, which are translated for output

    ;; left right bottom top
    (set-page-loc-of-drawing-rect gw 250 500 10 150)
    (box-rect gw)
    (record-font gw "Times-Bold" 12)
    (string-at-line-in-rect 
     gw :line-number 1 :font-size 12 
     :string (format nil "Testing 123: interval bar plot"))
    (set-loc-of-drawing-window gw 0.2 0.95 0.1 0.9)
    (box-window gw)
    (set-x-window-scale gw 0 10)
    (set-y-window-scale gw 0 1)
    (plot-interval-bars gw '((0 1)(3 4)(6 8)) 0.8 0.9)
    (plot-interval-bars gw '((0 1)(3 4)(6 8)) 0.75 0.8 :fill 0)
    (plot-interval-bars gw '((0 1)(3 4)(6 8)) 0.65 0.7 :fill 0.5)
    (plot-interval-bars gw '(2 3 5 6 7 8 9 10) 0.4 0.5)

    (set-page-loc-of-drawing-rect gw 250 500 160 250)
    (box-rect gw)
    (record-font gw "Helvetica" 10)
    (string-at-line-in-rect 
     gw :line-number 1 :font-size 12 
     :string (format nil "Another test: curve drawing"))
    (set-loc-of-drawing-window gw 0.2 0.95 0.15 0.8)
    (box-window gw)
    (set-x-window-scale gw 0 6.0)
    (set-y-window-scale gw -1.1 1.1)
    (plot-curve gw curve :wrap-fraction 0 :symbol *cross-symbol*)
    (plot-curve gw curve :wrap-fraction 0 :symbol nil)
    (label-y-axis gw -1.0 1.0 :tic-value 0.2 :tic-size 3 :font-size 9
                  :label-format "~,2f")
    (label-x-axis gw 0 6.0 :tic-value 1 :tic-size 3 :font-size 9
                  :label-format "~,2f")
    (label-x-axis gw 0 6.0 :tic-value 0.5 :tic-size 1 :label nil)

    (set-page-loc-of-drawing-rect gw 250 400 260 410)
    (box-rect gw)
    (set-loc-of-drawing-window gw 0 1 0 1)
    (set-x-window-scale gw -1.1 1.1)
    (set-y-window-scale gw -1.1 1.1)
    ;; circle as region
    (plot-poly gw (do* ((xy-list nil)
                        (x 0 (+ 0.1 x)))
                       ((> x 2pi) xy-list)
                    (push (list (sin x) (cos x)) xy-list)))
    (plot-poly gw (do* ((xy-list nil)
                        (x 0 (+ 0.1 x)))
                       ((> x 2pi) xy-list)
                    (push (list (* 0.8 (sin x)) (* 0.8 (cos x))) xy-list))
               :frame nil :fill 0.5)
    (plot-poly gw (do* ((xy-list nil)
                        (x 0 (+ 0.1 x)))
                       ((> x 2pi) xy-list)
                    (push (list (* 0.5 (sin x)) (* 0.5 (cos x))) xy-list))
               :frame nil :fill 0)
    (string-at-line-in-rect gw :line-number 1 
                            :string "Circles!")
    

    (record-wrapup gw)
    ))


(define-CLIM-GWINDOW-TEST-command
  (com-redisplay :menu "ReDisplay" :name "ReDisplay")
  ()
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'picture)))
    (clim:window-clear stream)
    (display-clim-picture clim::*application-frame* stream)))

(define-CLIM-GWINDOW-TEST-command
  (com-refresh :menu "Refresh" :name "Refresh")
  ()
  ;; on Mac, this seems to bring back dead scroll bars...
  (let ((stream (clim:get-frame-pane clim::*application-frame* 'picture)))
    (clim:window-refresh stream)
    (stream-force-output stream)
    ))


(define-CLIM-GWINDOW-TEST-command 
  (com-exit :menu "Exit" :name "Exit") 
  ()
  (clim:frame-exit clim:*application-frame*))

(defmethod INITIALIZE-INSTANCE :AFTER ((frame clim-gwindow-test) &rest args)
  (declare (ignore args))
  (with-slots (gw curve) frame
    (setf gw (make-clim-gwindow 
              :stream (clim:get-frame-pane frame 'picture) 
              :page-height 500))
    
    (setf curve nil)
    (do* ((x 0 (+ x 0.1))
          )
         ((> x 6.0) nil)
      (push (cons x (sin x)) curve))
    )
  )


(defvar *clim-gwindow-test-frame* nil)

(defun make-clim-gwindow-test (root)
  (setf *clim-gwindow-test-frame* 
        (clim:make-application-frame 'CLIM-GWINDOW-TEST :parent root
                                     :width 500 :height 350
                                     :pretty-name "CLIM GWINDOW TESTBED"
                                     ))
  
  (clim:run-frame-top-level *clim-gwindow-test-frame*)
  )

|#

;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; xte-fast-event.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: xte-fast-event.lisp,v $
;;; Revision 4.3  1995/04/20 22:18:56  buehler
;;; When calculating dark efficiency, avoid dividing by zero by assigning
;;; zero efficiency to pcf intervals of zero length.
;;;
;;; Revision 4.2  1995/03/03  05:51:19  buehler
;;; Force XTE-event-calc to be aware of the orbit model with a setf in
;;; make-list-of-orb-viewing-sample-lists.
;;;
;;; Revision 4.1  1995/02/03  23:29:23  buehler
;;; Temporarily undo the use of PCA sensor.
;;;
;;; Revision 4.0  1995/01/16  16:15:43  buehler
;;; Use sensor named :PCA instead of :STTA and :STTB.
;;;
;;; Revision 3.1  1994/11/28  22:55:44  buehler
;;; Incorporates telemetry constraint updates from Mark Johnston.
;;;
;;; Revision 3.0  1994/04/01  19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.5  1991/12/01  00:15:21  johnston
;;; include star tracker in suitability calculation
;;;
;;; Revision 1.4  1991/11/26  15:35:37  johnston
;;; rework for changes to fast-event Nov91
;;;
;;; Revision 1.3  1991/08/21  11:27:41  giuliano
;;; Changed interface functions for the event calcualtor.
;;;
;;; Revision 1.2  91/06/11  12:17:55  johnston
;;; renamed get-efficiency-pcf to avoid conflict with euve
;;; 
;;; Revision 1.1  91/06/05  23:01:04  johnston
;;; Initial revision
;;; 
;;; ----------------------------------------------------------------------
;;; extensions to fast-event for ASTRO-D
;;; May 1991 (derived from EUVE constraint calculators)
;;;


;;; ----------------------------------------------------------------------
;;;                                  ASTRO-D default constraint parameters
;;; ----------------------------------------------------------------------

;;; Sun constraint table:  list of triples (start-angle end-angle suitability)
;;; Angles are in degrees, representing angle of boresight from sun.
(defvar *DEFAULT-XTE-SUN-ANGLE-TABLE*)
(setf   *DEFAULT-XTE-SUN-ANGLE-TABLE*
      '((0 30 0)(30 180 1)))

;;; Sun sample interval, days
(defvar *DEFAULT-XTE-SUN-SAMPLE-INTERVAL*)
(setf   *DEFAULT-XTE-SUN-SAMPLE-INTERVAL* 1)

;;; Moon constraint table:  list of triples (start-angle end-angle suitability)
;;; Angles are in degrees, representing angle of boresight from moon.
(defvar *DEFAULT-XTE-MOON-ANGLE-TABLE*)
(setf   *DEFAULT-XTE-MOON-ANGLE-TABLE*
      '((0 3 0)(3 180 1)))

;;; Moon sample interval, days
(defvar *DEFAULT-XTE-MOON-SAMPLE-INTERVAL*)
(setf   *DEFAULT-XTE-MOON-SAMPLE-INTERVAL* 1/3)

;;; Name of default orbit model
(defvar *DEFAULT-XTE-ORBIT-MODEL*)
(setf   *DEFAULT-XTE-ORBIT-MODEL* "XTE")

;;; Minimum angle of boresight to bright earth limb, degrees
(defvar *DEFAULT-XTE-BRIGHT-LIMB-ANGLE*)
(setf   *DEFAULT-XTE-BRIGHT-LIMB-ANGLE* 5)

;;; Minimum angle of boresight to dark earth limb, degrees
(defvar *DEFAULT-XTE-DARK-LIMB-ANGLE*)
(setf   *DEFAULT-XTE-DARK-LIMB-ANGLE* 5)

;;; Angle from sun to terminator, degrees
(defvar *DEFAULT-XTE-TERMINATOR-ANGLE*)
(setf   *DEFAULT-XTE-TERMINATOR-ANGLE* 90)

;;; Flag, t if visibility must be in XTE shadow, nil if sunlight viewing
;;; allowed
(defvar *DEFAULT-XTE-DARK-FLAG*)
(setf   *DEFAULT-XTE-DARK-FLAG* nil)

;;; Angle of sun to horizon below which it is considered set. Negative 
;;; values mean below horizon.  Degrees.
(defvar *DEFAULT-XTE-SUN-DARK-LIMB-ANGLE*)
(setf   *DEFAULT-XTE-SUN-DARK-LIMB-ANGLE* -0.25)

;;; Flag, t if visibility in norther hemisphere should be encouraged, nil
;;; otherwise.
(defvar *DEFAULT-XTE-NORTH-FLAG*)
(setf   *DEFAULT-XTE-NORTH-FLAG* nil)

;;; Penalty for observing in Southern hemisphere:  this is the factor
;;; by which available observing time changes when dark visibility is
;;; entirely in the southern hemisphere. 
(defvar *DEFAULT-XTE-SOUTH-VIEWING-FACTOR*)
(setf   *DEFAULT-XTE-SOUTH-VIEWING-FACTOR* 1.0)

;;; Viewing time, nil if this should be ignored.  In days.
(defvar *DEFAULT-XTE-VIEWING-TIME*)
(setf   *DEFAULT-XTE-VIEWING-TIME* nil)

;;; Flag, t if observations are interruptible, nil if not.  If t, 
;;; *DEFAULT-XTE-MIN-VIEWING-TIME* should have a value given.
(defvar *DEFAULT-XTE-INTERRUPTIBLE*)
(setf   *DEFAULT-XTE-INTERRUPTIBLE* t)

;;; Minimum viewing time, nil if this should be ignored.  In days.
(defvar *DEFAULT-XTE-MIN-VIEWING-TIME*)
(setf   *DEFAULT-XTE-MIN-VIEWING-TIME* (/ 5 1440))

;;; Table for computing suitability based on viewing interval:
;;;  TBD
(defvar *DEFAULT-XTE-VIEWING-TABLE*)
(setf   *DEFAULT-XTE-VIEWING-TABLE* nil)

;;; Orbital viewing sample interval, days
(defvar *DEFAULT-XTE-ORB-VIEWING-SAMPLE-INTERVAL*)
(setf   *DEFAULT-XTE-ORB-VIEWING-SAMPLE-INTERVAL* 1)

;;; entry/exit table vs asc node longitude, for SAA
(defvar *DEFAULT-XTE-SAA-TABLE*)
(progn ;put in progn so assigned table doesn't echo to screen
  (setf   *DEFAULT-XTE-SAA-TABLE* *SAA-entry-exit-table*)
  nil)

;;; ----------------------------------------------------------------------
;;; sensor angle limits

;;; Minimum angle of FHST boresight to sun, degrees.
(defvar *DEFAULT-XTE-FHST-SUN-LIMIT*)
(setf   *DEFAULT-XTE-FHST-SUN-LIMIT* 30)

;;; Minimum angle of FHST boresight to moon, degrees. 
(defvar *DEFAULT-XTE-FHST-MOON-LIMIT*)
(setf   *DEFAULT-XTE-FHST-MOON-LIMIT* 10)

;;; Minimum angle of FHST boresight to bright earth limb, degrees.
(defvar *DEFAULT-XTE-FHST-BRIGHT-LIMB-LIMIT*)
(setf   *DEFAULT-XTE-FHST-BRIGHT-LIMB-LIMIT* 5)

;;; Minimum angle of FHST boresight to dark earth limb, degrees.
(defvar *DEFAULT-XTE-FHST-DARK-LIMB-LIMIT*)
(setf   *DEFAULT-XTE-FHST-DARK-LIMB-LIMIT* 1)


;;; ----------------------------------------------------------------------
;;;          XTE-EVENT-CALC Class:  combination of individual 
;;;          constraints tailored for XTE
;;; ----------------------------------------------------------------------

(defclass XTE-EVENT-CALC (event-calc)
  (
   )
  (:documentation "for recording event calculation parameters"))


;;; ----------------------------------------------------------------------
;;;                                 The current XTE-EVENT-CALC instance
;;; ----------------------------------------------------------------------

(defvar *CURRENT-XTE-EVENT-CALC* nil
  "the currently active XTE-event-calc instance")

(setf *CURRENT-XTE-EVENT-CALC* nil)


(defun CURRENT-XTE-EVENT-CALC ()
  
  "return current XTE-event-calc instance, or create and return if one does
    not exist."
  
  (or *CURRENT-XTE-EVENT-CALC*
      (setf *CURRENT-XTE-EVENT-CALC* (make-instance 'XTE-event-calc))))

;;; ----------------------------------------------------------------------
;;;          METHODS FOR XTE-EVENT-CALC:  only those which override ST
;;;          methods need to be defined here.
;;; ----------------------------------------------------------------------

;;; Sun, moon, are unchanged from HST, so the existing
;;; methods are all OK.  Orbital viewing is different.  Only the different
;;; methods are defined below.

;;; GET-VIEWING-PARMS-FOR-EVENT-CALCULATION
;;;  this provides the key for the orb-viewing cache table.  It may be
;;;  necessary to add additional parameters.  The cache readers and writers
;;;  will continue to work as before.

;;; NOTE:  the XTE orb viewing constraint stores TWO types of PCFs in the cache:
;;; one is the orb viewing suitability, the other is the observing efficiency
;;; i.e. ratio of available time for exposure to wall clock time.  The following
;;; methods provide for caching and access to the second type

;;; ----------------------------------------------------------------------
;;;          ORBITAL VIEWING (LONG-TERM)
;;; ----------------------------------------------------------------------

(defmethod GET-EFFICIENCY-PARMS-FOR-EVENT-CALCULATION
  ((AEC xte-event-calc))

  "return a list of parameter values which characterize efficiency parameters
    for event calculation.  The purpose is:  two calculations with the same
    set of parameters will have the same event constraint.  NOTE:  this differs
    from viewing parms only by having the keyword :EFFICIENCY at the front"
  
  (cons :efficiency
	(get-viewing-parms-for-event-calculation AEC)))


(defmethod EFFICIENCY-PCF ((AEC xte-event-calc))
  
  "get efficiency pcf either from cache or by computing it"
  
  (cond ((get-from-event-cache 
          AEC (get-efficiency-parms-for-event-calculation AEC)))
        (t (compute-orbital-viewing-pcf AEC)
           (get-from-event-cache 
            AEC (get-efficiency-parms-for-event-calculation AEC)))))



;;; -- override the ST method with specific calculation for XTE

(defmethod ORB-VIEWING-SAMPLE :AROUND
  ((AEC XTE-event-calc) time
   &key 
   (omit-geo-pcf nil)
   (omit-dark-limb-pcf nil)
   (omit-bright-limb-pcf nil)
   (omit-vis-pcf nil)
   (omit-dark-pcf nil)
   (sensor-name-list nil)
   (named-vector-list nil)
   (verbose nil))
  ;; call-next-method with constructed arg list
  (declare (ignore omit-geo-pcf omit-dark-limb-pcf omit-bright-limb-pcf
                   omit-vis-pcf omit-dark-pcf))
  (call-next-method
   AEC time 
   :omit-geo-pcf          t 
   :omit-dark-limb-pcf    t 
   :omit-bright-limb-pcf  t
   :omit-vis-pcf          nil
   :omit-dark-pcf         (not (dark AEC))
   :sensor-name-list      sensor-name-list
   :named-vector-list     named-vector-list
   :verbose               verbose)
  )

(defmethod COMPUTE-ORBITAL-VIEWING-PCF ((AEC XTE-event-calc) 
                                        &key &allow-other-keys)
  
  "Compute and return orbital viewing pcf for the interval and constraint
    parameters recorded in the XTE-event-calc object.  For efficiency, this
    version only computes for legal sun limits, therefore these limits must
    have been set up ahead of time.  NOTE:  for XTE this version caches the
    efficiency PCF as well as the orb-viewing pcf.

    PCFs here are rounded, see parameter in let (e.g. 0.001)"
  
  (with-slots (start-date
               end-date
               ;;dark
               ;;northern-hemisphere-only
               )
      AEC
    
    (let* ((round-to 0.001)		; round PCFs to this value

	   ;; sun visibility PCF and intervals
           (sun-pcf (sun-constraint-pcf AEC))
           (sun-intervals (pcf-non-zero-intervals sun-pcf start-date end-date))
	   (list-of-orb-viewing-sample-lists
	    (make-list-of-orb-viewing-sample-lists AEC :non-zero-intervals sun-intervals))

	   ;; components of total suitability
	   total-viewing-pcf
	   total-north-pcf
	   ;; total pcf
	   total-pcf
	   ;; efficiency
	   efficiency-pcf
	   )
      ;; NOTE: if dark flag is nil, then dark is as if not set, i.e. dark
      ;; vis is simply vis
      (multiple-value-bind
	  (;; dark vis time in days
	   dark-time-pcf 
	   ;; dark vis time, normalized to max=1
	   norm-dark-time-pcf 
	   ;; north dark vis time in days
	   north-dark-time-pcf)
	  (XTE-dark-vis-pcf
	   AEC :non-zero-intervals sun-intervals
	   :list-of-orb-viewing-sample-lists list-of-orb-viewing-sample-lists)
	  
	  ;; viewing
	  (setf total-viewing-pcf norm-dark-time-pcf)

	  ;; north
	  (setf total-north-pcf
	    (pcf-combine dark-time-pcf north-dark-time-pcf
			 #'XTE-north-vis-suitability))

	  ;; grand total
	  (setf total-pcf
	    (pcf-multiply  total-viewing-pcf total-north-pcf))

	  ;; efficiency
	  (setf efficiency-pcf
	    (XTE-efficiency-pcf
	     AEC :non-zero-intervals sun-intervals
	     :list-of-orb-viewing-sample-lists list-of-orb-viewing-sample-lists))          
	  ;; round
	  (setf total-pcf (pcf-round total-pcf round-to))
	  (setf efficiency-pcf (pcf-round efficiency-pcf round-to))
      
	  ;; store in cache (note TWO pcfs stored here)
	  (put-in-event-cache 
	   AEC (get-viewing-parms-for-event-calculation AEC) total-pcf)
	  (put-in-event-cache 
	   AEC (get-efficiency-parms-for-event-calculation AEC)
	   efficiency-pcf)
      
	  ;; return pcf
	  total-pcf))))



(defun XTE-NORTH-VIS-SUITABILITY (vis-dark-time north-vis-dark-time)
  "input total time when target is viewable,
    and same also when XTE in northern hemisphere.  
    Units must be the same.  Return suitability factor."
  
  ;; magic number here!
  (let ((min-south-viewing-suitability 0.1))
    (cond ((= 0 vis-dark-time) 0)
          (t (max min-south-viewing-suitability
                  (/ north-vis-dark-time vis-dark-time))))))


(defun XTE-TRACKER-VIS-TO-SUITABILITY (both-avail either-avail)
  
  "both-avail is fraction of target dark vis that both trackers
    available, either-avail is same but for either tracker available.
    Return suitability value."
  ;; (xte-tracker-vis-to-suitability 0.8 0.6)

  ;; there are magic numbers here! beware...
  (cond ((> both-avail   0.90) 1)
	((> either-avail 0.80) either-avail)
        ((> either-avail 0.60) 0.01)
	(t 0.01)))


(defmethod MAKE-LIST-OF-ORB-VIEWING-SAMPLE-LISTS
    ((AEC XTE-event-calc)
     &key non-zero-intervals
;; (
     (sensor-name-list '(:STTA :STTB)))
;;     (sensor-name-list '(:PCA)))

  "Input list of non-zero-intervals, return list of orb-viewing-sample-lists
    that cover only the specified non-zero intervals"

  (setf (observer AEC) *XTE*)  
  ;;; reb 03-03-95 force XTE-event-calc to be aware of the orbit model.
  ;;;              (Ask Mark Johnston if there's a better way to do this.)
  (let ((result nil))
    (dolist (interval non-zero-intervals)
      (push (orb-viewing-sample-list AEC :sensor-name-list sensor-name-list
				     :range interval)
	    result))
    (nreverse result)))


(defmethod XTE-EFFICIENCY-PCF ((AEC XTE-event-calc)
				&key non-zero-intervals
				list-of-orb-viewing-sample-lists)
  "Input:
     non-zero-intervals: list of non-zero-intervals ((t1 t2)(t3 t4)...)
      during sample lists are valid
     list-of-orb-viewing-sample-lists: as named, i.e. (sample-list1 sample-list2 ...)
      where sample-list1 corresponds to t1 t2, etc.
    return:
      PCF which has value estimated observing efficiency
       in intervals in non-zero-intervals"

  (let ((mask-pcf (pcf-from-interval-list non-zero-intervals))
	(eff-interval-list nil)
	)
    (dolist (sample-list list-of-orb-viewing-sample-lists)
      (with-orb-viewing-sample-list
       sample-list
       (let* ((period (- n2 n1))
	      (view-pcf (pcf-multiply vis-pcf (if (dark AEC) dark-pcf *unity-pcf*)))
	      (dark-vis (summed-interval-duration
			 (pcf-non-zero-intervals view-pcf)))
	      (north-dark-vis (summed-interval-duration
			       (pcf-non-zero-intervals
				(pcf-multiply view-pcf north-pcf))))
	      (south-dark-vis (- dark-vis north-dark-vis)))

	 (push (list sample-start sample-end
		     ;; efficiency is ratio of dark vis to orbital period 
		     ;; (since observing is only allowed during dark time)
		     ;; REB 4/20/95 efficiency is 0 for 0-length interval
		     (if (< n1 n2)
			 (/ (+ north-dark-vis
			       (* *DEFAULT-XTE-SOUTH-VIEWING-FACTOR* 
				  south-dark-vis))
			    period)
		       0)
		     )
	       eff-interval-list))))
    (pcf-multiply
     mask-pcf (pcf-from-interval-list eff-interval-list))))


(defmethod XTE-DARK-VIS-PCF ((AEC XTE-event-calc)
			      &key non-zero-intervals
			      list-of-orb-viewing-sample-lists)
  "Input:
     non-zero-intervals: list of non-zero-intervals ((t1 t2)(t3 t4)...)
      during sample lists are valid
     list-of-orb-viewing-sample-lists: as named, i.e. (sample-list1 sample-list2 ...)
      where sample-list1 corresponds to t1 t2, etc.
    return three values:
      PCF which has value total dark-vis viewing time (days) only
       in intervals in non-zero-intervals
      same, but normalized to max time = 1
      same, but total north dark vis in days"

  (let ((mask-pcf (pcf-from-interval-list non-zero-intervals))
	(dark-interval-list nil)
	(north-dark-interval-list)
	(dark-time-pcf nil))
    (dolist (sample-list list-of-orb-viewing-sample-lists)
      (with-orb-viewing-sample-list
       sample-list
       (let ((view-pcf (pcf-multiply vis-pcf (if (dark AEC) dark-pcf *unity-pcf*))))
	 (push (list sample-start sample-end
		     (summed-interval-duration
		      (pcf-non-zero-intervals view-pcf)))
	       dark-interval-list)
	 (push (list sample-start sample-end 
		     (summed-interval-duration
		      (pcf-non-zero-intervals (pcf-multiply view-pcf north-pcf))))
	       north-dark-interval-list))))
    (setf dark-time-pcf 
      (pcf-multiply mask-pcf (pcf-from-interval-list dark-interval-list)))
    (values
     dark-time-pcf
     (pcf-normalize dark-time-pcf (pcf-max dark-time-pcf))
     (pcf-multiply
      mask-pcf (pcf-from-interval-list north-dark-interval-list)))))

(defmethod XTE-TRACKER-PCF ((EC XTE-event-calc)
        &key non-zero-intervals
        list-of-orb-viewing-sample-lists
        sensor1 sensor2)
  (values
    *unity-pcf*
    *unity-pcf*))

#|
(defmethod XTE-TRACKER-PCF ((AEC XTE-event-calc)
			     &key non-zero-intervals
			     list-of-orb-viewing-sample-lists
			     sensor1 sensor2)
  "Input:
     non-zero-intervals: list of non-zero-intervals ((t1 t2)(t3 t4)...)
      during sample lists are valid
     list-of-orb-viewing-sample-lists: as named, i.e. (sample-list1 sample-list2 ...)
      where sample-list1 corresponds to t1 t2, etc.
    return multiple values:
      PCF which has value = fraction of shadow-vis time that BOTH trackers is useable
       in intervals in non-zero-intervals
      same, fraction when EITHER tracker available"

  (let ((both-pcf *zero-pcf*)
	(either-pcf *zero-pcf*)
	(mask-pcf (pcf-from-interval-list non-zero-intervals)))
    (dolist (sample-list list-of-orb-viewing-sample-lists)
      (setf both-pcf
	(pcf-combine both-pcf 
		     (XTE-both-trackers-avail-dark-pcf
		      AEC :sample-list sample-list :sensor1 sensor1 :sensor2 sensor2)
		     #'+))
      (setf either-pcf
	(pcf-combine either-pcf 
		     (XTE-either-tracker-avail-dark-pcf
		      AEC :sample-list sample-list :sensor1 sensor1 :sensor2 sensor2)
		     #'+)))
    (values
     (pcf-multiply mask-pcf both-pcf)
     (pcf-multiply mask-pcf either-pcf))))


;;; ----------------------------------------------------------------------
;;;                                       STAR TRACKER AVAILIBILITY (XTE)
;;; ----------------------------------------------------------------------


(defmethod XTE-TRACKER-AVAIL-DARK-PCF ((AEC XTE-event-calc) 
                                        &key sample-list sensor)
  
  "Return PCF value that is fraction of dark visibility period that sensor is clear
    and target is visible and observer is in shadow.
    Also obeys sun and moon constraints:  suitability is 0 if either angle violated."
  
  (let ((sun-limit (get-sensor-limit-angle AEC sensor :sun))
	(moon-limit (get-sensor-limit-angle AEC sensor :moon))
	(dark-vis-pcf nil)
	(dark-vis-time nil)
	(suit nil))
    (cond (sensor
	   (with-orb-viewing-sample-list
	    sample-list
	    (if (null suit) (push (list *minus-infinity* sample-start 0) suit))
	    (setf dark-vis-pcf (pcf-multiply vis-pcf dark-pcf))
	    (setf dark-vis-time 
	      (summed-interval-duration (pcf-non-zero-intervals dark-vis-pcf)))
	    (push (list sample-start sample-end 
			;; zero if sun or moon violated
			(cond ((and sun-limit  (<= (sensor-sun-angle sensor) sun-limit))   0)
			      ((and moon-limit (<= (sensor-moon-angle sensor) moon-limit)) 0)
			      ((= 0 dark-vis-time) 0)
			      (t (/ (summed-interval-duration
				     (pcf-non-zero-intervals
				      (pcf-multiply (sensor-vis-pcf sensor) dark-vis-pcf)))
				    dark-vis-time))))
		  suit))
	   (setf suit (if (null suit) *zero-pcf*
			(pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 0) suit))))
	   suit)
	  (t *zero-PCF*))))


(defmethod XTE-BOTH-TRACKERS-AVAIL-DARK-PCF ((AEC XTE-event-calc) &key sample-list sensor1 sensor2)
  
  "Return PCF value that is fraction of dark visibility period that both sensors are clear
    and target is visible and observer is in shadow.
    Also obeys sun and moon constraints:  suitability is 0 if either angle violated."
  
  (let ((sun-limit1 (get-sensor-limit-angle AEC sensor1 :sun))
	(moon-limit1 (get-sensor-limit-angle AEC sensor1 :moon))
	(sun-limit2 (get-sensor-limit-angle AEC sensor2 :sun))
	(moon-limit2 (get-sensor-limit-angle AEC sensor2 :moon))
	(dark-vis-pcf nil)
	(dark-vis-time nil)
	(suit nil))
    (cond ((and sensor1 sensor2)
	   (with-orb-viewing-sample-list
	    sample-list
	    (if (null suit) (push (list *minus-infinity* sample-start 0) suit))
	    (setf dark-vis-pcf (pcf-multiply vis-pcf dark-pcf))
	    (setf dark-vis-time 
	      (summed-interval-duration (pcf-non-zero-intervals dark-vis-pcf)))
	    (push (list sample-start sample-end 
			;; zero if sun or moon violated
			(cond ((and sun-limit1  (<= (sensor-sun-angle sensor1) sun-limit1))   0)
			      ((and moon-limit1 (<= (sensor-moon-angle sensor1) moon-limit1)) 0)
			      ((and sun-limit2  (<= (sensor-sun-angle sensor2) sun-limit2))   0)
			      ((and moon-limit2 (<= (sensor-moon-angle sensor2) moon-limit2)) 0)
			      ((= 0 dark-vis-time) 0)
			      (t (/ (summed-interval-duration
				     (pcf-non-zero-intervals
				      (pcf-multiply (sensor-vis-pcf sensor1)
						    (pcf-multiply (sensor-vis-pcf sensor2) 
								  dark-vis-pcf))))
				    dark-vis-time))))
		  suit))
	   (setf suit (if (null suit) *zero-pcf*
			(pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 0) suit))))
	   suit)
	  (t *zero-PCF*))))


(defmethod XTE-EITHER-TRACKER-AVAIL-DARK-PCF ((AEC XTE-event-calc) &key sample-list sensor1 sensor2)
  
  "Return PCF value that is fraction of dark visibility period that either sensor is clear
    and target is visible and observer is in shadow.
    Also obeys sun and moon constraints:  suitability is 0 if either angle violated."
  
  (let ((sun-limit1 (get-sensor-limit-angle AEC sensor1 :sun))
	(moon-limit1 (get-sensor-limit-angle AEC sensor1 :moon))
	(sun-limit2 (get-sensor-limit-angle AEC sensor2 :sun))
	(moon-limit2 (get-sensor-limit-angle AEC sensor2 :moon))
	(dark-vis-pcf nil)
	(dark-vis-time nil)
	(suit nil))
    (cond ((and sensor1 sensor2)
	   (with-orb-viewing-sample-list
	    sample-list
	    (if (null suit) (push (list *minus-infinity* sample-start 0) suit))
	    (setf dark-vis-pcf (pcf-multiply vis-pcf dark-pcf))
	    (setf dark-vis-time 
	      (summed-interval-duration (pcf-non-zero-intervals dark-vis-pcf)))
	    (push (list sample-start sample-end 
			;; zero if sun or moon violated for BOTH trackers
			(cond 
			 ;; both trackers violate sun or moon limits
			 ((and (or (and sun-limit1  (<= (sensor-sun-angle sensor1) sun-limit1))
				   (and moon-limit2 (<= (sensor-moon-angle sensor2) moon-limit2)))
			       (or (and sun-limit2  (<= (sensor-sun-angle sensor2) sun-limit2))
				   (and moon-limit1 (<= (sensor-moon-angle sensor1) moon-limit1))))
			  0)
			 ;; no dark time on target
			 ((= 0 dark-vis-time) 0)
			 (t (/ (summed-interval-duration
				(pcf-non-zero-intervals
				 (pcf-combine
				  (pcf-multiply (sensor-vis-pcf sensor1) dark-vis-pcf)
				  (pcf-multiply (sensor-vis-pcf sensor2) dark-vis-pcf)
				  #'+)))
			       dark-vis-time))))
		  suit))
	   (setf suit (if (null suit) *zero-pcf*
			(pcf-from-interval-list (cons (list (second (first suit)) *plus-infinity* 0) suit))))
	   suit)
	  (t *zero-PCF*))))

|#
;;; ----------------------------------------------------------------------
;;;          ORBITAL-VIEWING (SHORT-TERM)
;;; ----------------------------------------------------------------------


;;; --- SAA ---

(defmethod GET-SAA-PARMS-FOR-SHORT-TERM-EVENTS
  ((AEC XTE-event-calc))
  (rationalize-numbers-in-nested-list     
   (append '(:SHORT-TERM-SAA)
           (get-dates-for-event-calculation AEC)
           (list  
            (intern (string-it-upcase (orbit-model AEC)))))))


(defmethod SAA-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "get SAA PCF pcf either from cache or by computing it"
  (cond ((get-from-event-cache AEC (get-saa-parms-for-short-term-events AEC)))
        (t (compute-SAA-short-term-pcf AEC))))


(defmethod COMPUTE-SAA-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "Calculate SAA PCF"
  (with-slots (start-date
               end-date
               observer) AEC
    (let ((pcf (SAA-PCF observer start-date end-date 
                        :table *DEFAULT-XTE-SAA-TABLE*)))
      (put-in-event-cache 
       AEC (get-saa-parms-for-short-term-events AEC) pcf)
      pcf)))



;;; --- dark ---

(defmethod GET-DARK-PARMS-FOR-SHORT-TERM-EVENTS
  ((AEC XTE-event-calc))
  (rationalize-numbers-in-nested-list     
   (append '(:SHORT-TERM-DARK)
           (get-dates-for-event-calculation AEC)
           (list  (dark-sun-limb-angle AEC)
                  (intern (string-it-upcase (orbit-model AEC)))
                  (high-accuracy AEC)
                  ))))

(defmethod DARK-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "get DARK PCF pcf either from cache or by computing it"
    (cond ((get-from-event-cache
            AEC (get-dark-parms-for-short-term-events AEC)))
          (t (compute-dark-short-term-pcf AEC))))


(defmethod COMPUTE-DARK-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "Calculate DARK PCF"
  (with-slots (start-date
               end-date
               observer
               dark-sun-limb-angle
               high-accuracy) AEC
    (let* ((occ-list (occultation-list observer *sun* start-date end-date 
                                       :limb-angle dark-sun-limb-angle
                                       :high-accuracy high-accuracy))
           ;; pcf is 1 if observer is in shadow, 0 otherwise
           (dark-pcf (pcf-from-start-end-list occ-list 1 0)))
      ;; cache and return
      (put-in-event-cache
       AEC (get-dark-parms-for-short-term-events AEC) dark-pcf)
      dark-pcf)))



;;; --- target occultation ---

(defmethod GET-OCC-PARMS-FOR-SHORT-TERM-EVENTS
  ((AEC XTE-event-calc))
  (rationalize-numbers-in-nested-list     
   (append '(:SHORT-TERM-OCC)
           (get-target-for-event-calculation AEC)
           (get-dates-for-event-calculation AEC)
           (list  (intern (string-it-upcase (orbit-model AEC)))
                  (bright-limb-angle AEC)
                  (dark-limb-angle AEC)
                  (terminator-angle AEC)
                  (high-accuracy AEC)
                  (viewing-time AEC)
                  (interruptible AEC)
                  (min-viewing-time AEC)
                  (replace-minus-infinity-in-pcf-by-symbol 
                   (viewing-table AEC))))))


(defmethod OCC-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "get OCC PCF pcf either from cache or by computing it"
  (cond ((get-from-event-cache 
          AEC (get-occ-parms-for-short-term-events AEC)))
        (t (compute-occ-short-term-pcf AEC))))


(defmethod COMPUTE-OCC-SHORT-TERM-PCF ((AEC XTE-event-calc))
  "Calculate OCC PCF"
  (with-slots (start-date
               end-date
               orbit-model
               observer
               astro-object
               bright-limb-angle
               dark-limb-angle
               terminator-angle
               high-accuracy) AEC
    
    ;; error check: must have orbit model and be in time range
    (if (or (null orbit-model) 
            (eq orbit-model :none)
            (not (in-orbit-model-range start-date))
            (not (in-orbit-model-range end-date)))
      (return-from COMPUTE-OCC-SHORT-TERM-PCF nil))
    ;; must have bright and dark limb angles
    (if (or (null bright-limb-angle) (null dark-limb-angle))
      (return-from COMPUTE-OCC-SHORT-TERM-PCF nil))
    
    ;; bright + dark = vis pcf
    (let* ((occ-list (earth-illum-occultation-list
                      observer astro-object start-date end-date 
                      :bright-limb-angle bright-limb-angle
                      :dark-limb-angle dark-limb-angle
                      :terminator-angle terminator-angle
                      :high-accuracy high-accuracy))
           ;; note pcf value is 0 if target is occulted, 1 if visible
           (vis-pcf (min-viewing-filter 
                     AEC (pcf-from-start-end-list occ-list 0 1))))
      
      ;; cache and return
      (put-in-event-cache
       AEC (get-occ-parms-for-short-term-events AEC) vis-pcf)
      
      vis-pcf)))


(defmethod MIN-VIEWING-FILTER ((AEC XTE-event-calc) 
                               vis-pcf)
  
  "input a visibility PCF (1 visible, 0 not)
    return PCF factor based on size of viewing interval"
  
  (let ((result vis-pcf))

    (with-slots (start-date
                 end-date
                 viewing-time
                 interruptible
                 min-viewing-time
                 ;; viewing-table
                 )
      AEC

      (when viewing-time
        (let ((nzi (pcf-non-zero-intervals vis-pcf start-date end-date))
              (accum-list nil))
          (dolist (interval nzi)
            (let ((duration (- (second interval) (first interval))))

              (cond 

               ((and interruptible 
                     min-viewing-time
                     (> min-viewing-time duration))
                ;; drop the interval: too short
                )

               ((and (not interruptible)
                     viewing-time
                     (> viewing-time duration))
                ;; not interruptible and interval too short: drop it
                )
               
               ;; neither: keep interval without change
               (t (push interval accum-list))))) ;; end loop over intervals
          ;; set result
          (setf result (pcf-from-interval-list (nreverse accum-list)))))
      result)))



;;; ----------------------------------------------------------------------
;;;          Functions to set up event-calc parameters.
;;;          tailored for ASTRO-D.
;;;
;;; ST versions are SET-HST-..., XTE are SET-XTE-...
;;; The differernces are in the defaults for the arguments:  these 
;;; reference XTE default constraint parameters.
;;; ----------------------------------------------------------------------
;;;


;;; mdj/nov94: added params here to control non-int, shadow viewing
;;; these just become args on the call to set-XTE-event-calc-orbital-viewing
(defun SET-XTE-CONSTRAINT-PARAMETERS
       (&key name ra dec start-date end-date orbit-model
             (shadow-flag nil)
             (non-interrupt nil) (non-interrupt-duration 0)
             )
  
  "Function to set up all XTE constraint parameter values.  This version
    uses defaults for all parameters and tables.  It can be specialized to
    take other arguments if constraint parameters need to change in different
    circumstances.  Arguments:
     name - string or symbol
     ra - RA in degrees
     dec - Dec in degrees
     orbit-model - name of orbit model (string)
     start-date - start date (TJD or time format)
     end-date - end date (TJD or time format)
     shadow-flag - nil for anytime viewing (default), t for shadow
     non-interrupt - nil (default) or t if must all be taken in one viewing int.
     non-interrupt-duration - only relevant if non-interrupt is t: this is the
      size of the min viewing interval, in days
     "
  (set-XTE-event-calc-pointing 
   :name (string-it name) :ra ra :dec dec :units :DEGREES)
  (set-XTE-event-calc-dates :start start-date :end end-date)
  (set-XTE-event-calc-roll :nominal)
  (set-XTE-event-calc-sun-constraint)
  (set-XTE-event-calc-moon-constraint)
  (cond (non-interrupt
         (set-XTE-event-calc-orbital-viewing 
          :orbit-model orbit-model
          :dark shadow-flag
          :viewing-time non-interrupt-duration
          :interruptible nil))
        (t ;; normal (interruptible case)
         (set-XTE-event-calc-orbital-viewing 
          :orbit-model orbit-model
          :dark shadow-flag)))
  (set-XTE-event-calc-fhst-limits)
  )

;;; --- set up individual constraint parameters ---

(defun SET-XTE-EVENT-CALC-POINTING (&key name ra dec position-vector
                                            (units :DEGREES)
                                            (cell-center nil)
                                            (observer *XTE*)
                                            (observer-name "XTE")
                                            )
  
  "Set up pointing data for event calculation.  Usage:
    :name is print name (optional)
    :ra and :dec OR :position-vector can be specified
    :units - :RADIANS or :DEGREES.  Only used if :ra and :dec are supplied.
      default is :DEGREES
    :cell-center - if t, the position is adjusted to the nearest cell
      center.  Default is nil
    :observer - instance of viewing satellite, default *XTE*
    :observer name - should be specified if observer
      instance does not exist when this function is called.  
      Default is \"XTE\" "
  
  (set-target-for-event-calculation
   (current-XTE-event-calc)
   :target-name (string-it name)
   :target-ra ra 
   :target-dec dec 
   :target-position-vector position-vector
   :ra-dec-in-degrees (cond ((eq units :degrees) t)
                            ((eq units :radians) nil)
                            (t (error "unknown units")))
   
   :cell-center cell-center 
   :the-observer observer
   :the-observer-name observer-name))


(defun SET-XTE-EVENT-CALC-ROLL (roll-type 
                                   &key roll-offset-angle 
                                   roll-angle)
  
  "Set roll (orientation) for event calculation.  Must be set after
    pointing is specified (with SET-EVENT-CALC-POINTING).  roll-spec
    for low-earth-orbit-satellites is of the form:
     nil - clear roll spec
     :nominal - use nominal roll
     :offset-from-nominal :roll-offset-angle <angle> 
     :fixed :roll-angle <angle>
    Angles are in radians.  Examples:
     (set-event-calc-roll nil) ; clear roll
     (set-event-calc-roll :nominal)
     (set-event-calc-roll :offset-from-nominal :roll-offset-angle 0.1)
     (set-event-calc-roll :fixed :roll-angle -0.1)
     "
  (let (roll-spec)
    (cond ((or (eq roll-type nil)
               (eq roll-type :nominal))
           (setf roll-spec (list roll-type)))
          ((eq roll-type :offset-from-nominal)
           (setf roll-spec (list roll-type :roll-offset-angle roll-offset-angle)))
          ((eq roll-type :fixed)
           (setf roll-spec (list roll-type :roll-angle roll-angle)))
          (t (error "invalid roll type: ~a" roll-type)))
    (set-roll-for-event-calculation (current-XTE-event-calc) roll-spec)))


(defun SET-XTE-EVENT-CALC-DATES (&key start end (default-sample-interval 1))
  
  "Set up date ranges for event calculation.  Usage:
    :start and :end are required, may be in date or time format
    e.g. '1-jan-1991, 4289.5, etc.
    :default-sample-interval is in days, default is 1"
  ;; (set-event-calc-dates :start '1-jan-1991 :end '31-dec-1992)
  
  (set-dates-for-event-calculation
   (current-XTE-event-calc)
   :start start 
   :end end 
   :sample default-sample-interval))


(defun SET-XTE-EVENT-CALC-SUN-CONSTRAINT 
       (&key 
        (angle-table *DEFAULT-XTE-SUN-ANGLE-TABLE*)
        (sample-interval *DEFAULT-XTE-SUN-SAMPLE-INTERVAL*))
  
  "Set up sun constraint.
    :angle-table is a list of angle ranges in DEGREES and suitability
      values, e.g. '((0 50 0)(50 180 1))
      If nil the constraint will be turned off.
    :sample-interval is interval in days, or nil to use default."
  ;; (set-event-calc-sun-constraint :angle-table '((0 50 0)(50 180 1)))
  ;; (set-event-calc-sun-constraint :angle-table nil)
  
  (set-sun-exclusion-for-event-calculation 
   (current-XTE-event-calc)
   :sun-angle-table angle-table 
   :interval sample-interval))
  

(defun SET-XTE-EVENT-CALC-MOON-CONSTRAINT 
       (&key 
        (angle-table *DEFAULT-XTE-MOON-ANGLE-TABLE*)
        (sample-interval *DEFAULT-XTE-MOON-SAMPLE-INTERVAL*))
  
  "Set up moon constraint.
    :angle-table is a list of angle ranges in DEGREES and suitability
      values, e.g. '((0 15 0)(15 20 0.5)(20 180 1))
      If nil the constraint will be turned off.
    :sample-interval is interval in days, or nil to use default."
  ;; (set-event-calc-moon-constraint :angle-table '((0 50 0)(50 180 1)))
  
  (set-moon-exclusion-for-event-calculation 
   (current-XTE-event-calc)
   :moon-angle-table angle-table 
   :interval sample-interval))
  

(defun SET-XTE-EVENT-CALC-ZODIACAL-LIGHT (&key (wavelength nil)
                                                  (brightness nil)
                                                  (sample-interval nil))
  
  "Set up zodiacal light constraint.
    :wavelength is wavelength
    :brightness is target flux density at wavelength
   If either is nil the constraint will be turned off."
  ;; (set-event-calc-zodiacal-light) ;turn off
  ;; (set-event-calc-zodiacal-light :wavelength 5500 :brightness 1e-10)
  
  (set-zod-light-for-event-calculation 
   (current-XTE-event-calc)
   :zod-wavelength wavelength 
   :zod-brightness brightness 
   :interval sample-interval))


(defun SET-XTE-EVENT-CALC-ORBITAL-VIEWING 
       (&key (orbit-model *DEFAULT-XTE-ORBIT-MODEL*)
             (bright-limb-angle *DEFAULT-XTE-BRIGHT-LIMB-ANGLE*)
             (dark-limb-angle *DEFAULT-XTE-DARK-LIMB-ANGLE*)
             (limb-angles-in-degrees t)
             (terminator-angle *DEFAULT-XTE-TERMINATOR-ANGLE*)
             (terminator-angle-in-degrees t)
             (dark *DEFAULT-XTE-DARK-FLAG*)
             (dark-sun-limb-angle *DEFAULT-XTE-SUN-DARK-LIMB-ANGLE*)
             (dark-sun-limb-angle-in-degrees t)
             (northern-hemisphere-only *DEFAULT-XTE-NORTH-FLAG*)
             (viewing-time *DEFAULT-XTE-VIEWING-TIME*)
             (interruptible *DEFAULT-XTE-INTERRUPTIBLE*)
             (min-viewing-time *DEFAULT-XTE-MIN-VIEWING-TIME*)
             (view-suitability-table *DEFAULT-XTE-VIEWING-TABLE*)
             (high-accuracy nil)
             (sample-interval *DEFAULT-XTE-ORB-VIEWING-SAMPLE-INTERVAL*)
             )
  
  "Set up orbital viewing constraint.
    :orbit-model is orbit model name (automatically loaded if necessary)
    :bright-limb-angle and :dark-limb-angle are earth avoidance angles;
      both are required
    :limb-angles-in-degrees should be t if both limb angles are degrees, 
      nil if radians
    :terminator-angle may be degrees or radians, depending on flag
      :terminator-angle-in-degrees: it is angle to terminator from sun on
      earth surface
    :dark is t if observation requires shadow viewing, nil otherwise
    :dark-sun-limb-angle may be radians or degrees, depending on 
       :dark-sun-limb-angle-in-degrees. Negative limb angles
       are below horizon
    :northern-hemisphere-only is t if preference for viewing in norther
       hemisphere is to be applied, nil otherwise
    :viewing-time is observation time in days, if viewing duration constraint
      is to be calculated, nil otherwise
    :interruptible is flag t if observation is interruptible, nil if not.
      If non-interruptible, then :viewing-time is taken to be the duration.
      If interruptible, then :min-viewing-time should be specified as the 
      minimum size of an interrupted segment
    :min-viewing-time in days is minimum duration of interrupted segment for
      and interruptible observation
    :view-suitability-table is TBD, default is nil.
    :high-accuracy may be t or nil
    :sample-interval is interval in days, or nil for default"

  (set-viewing-parms-for-event-calculation
   (current-XTE-event-calc)
   :orbit-model-name orbit-model
   
   :bright-angle bright-limb-angle 
   :dark-angle dark-limb-angle
   :limb-angles-in-degrees limb-angles-in-degrees    
   :terminator-limit (if terminator-angle-in-degrees 
                       (radians terminator-angle) terminator-angle)
   
   :dark-flag dark 
   :dark-sun-limb-limit (if dark-sun-limb-angle-in-degrees
                          (radians dark-sun-limb-angle) dark-sun-limb-angle)
   
   :view-duration viewing-time
   :view-interruptible interruptible
   :view-minimum-duration min-viewing-time
   :view-suitability-table view-suitability-table
   
   :north-flag northern-hemisphere-only
   
   :high-accuracy-flag high-accuracy
   :interval sample-interval))


(defun SET-XTE-EVENT-CALC-SENSOR-LIMITS 
       (&key sensor-name sun moon bright-limb dark-limb (angles-in-degrees nil))
  
  "Set up limits for sensor viewing angles.  Angles may be in radians or
    degrees (in which case angles-in-degrees should be t).
    Missing angles are ignored."
  
  (set-sensor-limits-for-event-calculation 
   (current-XTE-event-calc)
   sensor-name
   :sun sun :moon moon :bright-limb bright-limb :dark-limb dark-limb 
   :angles-in-degrees angles-in-degrees))


(defun SET-XTE-EVENT-CALC-FHST-LIMITS
       (&key 
        (sun *DEFAULT-XTE-FHST-SUN-LIMIT*)
        (moon *DEFAULT-XTE-FHST-MOON-LIMIT*)
        (bright-limb *DEFAULT-XTE-FHST-BRIGHT-LIMB-LIMIT*)
        (dark-limb *DEFAULT-XTE-FHST-DARK-LIMB-LIMIT*)
        (angles-in-degrees t))
  
  "Set up limits for XTE star tracker viewing angles.  
    Names of trackers are hardwired to :STTA and :STTB  Angles
    are the same for BOTH trackers.
    Angles may be in radians or degrees 
    (in which case angles-in-degrees should be t).
    Missing angles are ignored."

  (set-sensor-limits-for-event-calculation 
   (current-XTE-event-calc)
   :STTA
   :sun sun :moon moon :bright-limb bright-limb :dark-limb dark-limb 
   :angles-in-degrees angles-in-degrees)
  (set-sensor-limits-for-event-calculation 
   (current-XTE-event-calc)
   :STTB
   :sun sun :moon moon :bright-limb bright-limb :dark-limb dark-limb 
   :angles-in-degrees angles-in-degrees))


;;; ----------------------------------------------------------------------
;;;                               Externally callable routines to get PCFs
;;; ----------------------------------------------------------------------


;;; --- long-term ---

(defun GET-XTE-SUN-PCF ()
  (sun-constraint-pcf (current-XTE-event-calc)))

(defun GET-XTE-MOON-PCF ()
  (moon-constraint-pcf (current-XTE-event-calc)))

(defun GET-XTE-ZODIACAL-LIGHT-PCF ()
  (zodiacal-light-constraint-pcf (current-XTE-event-calc)))

(defun GET-XTE-ORBITAL-VIEWING-PCF ()
  (orbital-viewing-pcf (current-XTE-event-calc)))

(defun GET-XTE-EFFICIENCY-PCF ()
  (efficiency-pcf (current-XTE-event-calc)))

;;; --- short-term ---

(defun GET-XTE-SAA-SHORT-TERM-PCF ()
  "Return pcf with 0 when observer is in SAA, 1 when outside"
  (saa-short-term-pcf (current-XTE-event-calc)))

(defun GET-XTE-DARK-SHORT-TERM-PCF ()
  "Return pcf with 0 when observer is in shadow, 1 when in sunlight"
  (dark-short-term-pcf (current-XTE-event-calc)))

(defun GET-XTE-OCC-SHORT-TERM-PCF ()
  "Return pcf with 0 when target is occulted, 1 when visible"
  (occ-short-term-pcf (current-XTE-event-calc)))




#| test case:

;;; LONG TERM
(set-XTE-constraint-parameters
       :name "Dummy_X-1" :ra 244.9 :dec 19.9
       :start-date '1-jan-92 :end-date '1-mar-92
       :orbit-model "XTE")
(current-xte-event-calc)
(get-efficiency-parms-for-event-calculation 
        (current-xte-event-calc))
(get-viewing-parms-for-event-calculation 
        (current-xte-event-calc))
(describe (current-xte-event-calc))


(event-cache-report (current-xte-event-calc))
(clear-event-cache (current-xte-event-calc))
(get-xte-orbital-viewing-pcf)
(get-xte-efficiency-pcf)
(get-xte-moon-pcf)
(get-xte-sun-pcf)

(event-cache-report (current-xte-event-calc))
(event-cache-needs-to-be-saved (current-xte-event-calc))
(save-event-cache (current-xte-event-calc) "clipper:temp.cache" 
                  :compile nil :verbose t)
(clear-event-cache (current-xte-event-calc))
(event-cache-report (current-xte-event-calc))
(restore-event-cache (current-xte-event-calc)
                     "clipper:temp.cache" 
                     :verbose t)
(event-cache-report (current-xte-event-calc))


;;; TEST:  SHORT-TERM

(set-XTE-constraint-parameters
 :name "Dummy_X-1" :ra 244.9 :dec 19.9
 :start-date '1-jan-92 :end-date '2-jan-92
 :orbit-model "XTE")
(clear-event-cache (current-xte-event-calc))
(get-saa-parms-for-short-term-events 
 (current-xte-event-calc))
(get-xte-saa-short-term-pcf)
(get-xte-dark-short-term-pcf)
(get-xte-occ-short-term-pcf)

(event-cache-report (current-xte-event-calc))
|#

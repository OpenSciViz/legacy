;;;
;;; GET-HOST-STRING (string &optional (host-delimiter ":")           [FUNCTION]
;;;                  (start 0) end)
;;;
;;; PARSE-WITH-STRING-DELIMITER (delim string &key (start 0) end)    [FUNCTION]
;;;
;;; PARSE-WITH-STRING-DELIMITER* (delim string &key (start 0) end    [FUNCTION]
;;;                               include-last)
;;;
;;; PARALLEL-SUBSTITUTE (string mappings)                            [FUNCTION]
;;;
;;; NAME-SUBSTITUTION (string mappings)                              [FUNCTION]
;;;
;;; *LOGICAL-PATHNAME-TRANSLATIONS-TABLE* ((make-hash-table :test    [VARIABLE]
;;;                                        #'equal))
;;;
;;; CANONICALIZE-LOGICAL-HOSTNAME (host)                             [FUNCTION]
;;;
;;; LOGICAL-PATHNAME-TRANSLATIONS (host)                             [FUNCTION]
;;;    If HOST is the host component of a logical pathname and has been
;;;    defined as a logical pathname host name by SETF of
;;;    LOGICAL-PATHNAME-TRANSLATIONS, this function returns the list of
;;;    translations for the specified HOST. Each translation is a list of at
;;;    least two elements, a from-wildname and a to-wildname. The former is
;;;    a logical pathname whose host is the specified HOST. (I.e., the host
;;;    of the from-pathname need not be explicitly specified.) The latter is
;;;    any pathname. If to-wildname coerces to a logical pathname,
;;;    TRANSLATE-LOGICAL-PATHNAME will retranslate the result, repeatedly if
;;;    necessary. Translations are listed in the order listed, so more
;;;    specific from-wildnames must precede more general ones.
;;;
;;; (SETF LOGICAL-PATHNAME-TRANSLATIONS) (translations)          [SETF MAPPING]
;;;    (setf (logical-pathname-translations host) translations) sets the
;;;    list of translations for the logical pathname host to translations.
;;;    If host is a string that has not previously been used as a logical
;;;    pathname host, a new logical pathname host is defined; otherwise an
;;;    existing host's translations are replaced. Logical pathname host
;;;    names are compared with string-equal.
;;;
;;; *PHYSICAL-HOST-TABLE* ((make-hash-table :test #'equal))          [VARIABLE]
;;;    Table of physical hosts and system types for those hosts. 
;;;    Valid (implemented) types include :vms, :explorer, :symbolics,
;;;    :unix. 
;;;
;;; PHYSICAL-HOST-TYPE (host)                                        [FUNCTION]
;;;
;;; (SETF PHYSICAL-HOST-TYPE) (type)                             [SETF MAPPING]
;;;
;;; LOCAL-HOST-TABLE ("nethosts.txt")                                [CONSTANT]
;;;
;;; LOAD-PHYSICAL-HOSTAB (                                           [FUNCTION]
;;;                       &optional (local-hostab local-host-table))
;;;    Loads the physical host namespace table. This is compatible with
;;;    vms and symbolics host tables. Hostab line format should look
;;;    something like:
;;;         HOST NAME,CHAOS #,STATUS,SYSTEM-TYPE,MACHINE-TYPE,NICKNAMES
;;;
;;; HOST-TYPE (host)                                                 [FUNCTION]
;;;
;;; PATHNAME-HOST-TYPE (pathname)                                    [FUNCTION]
;;;
;;; TRANSLATION-RULE (host-type case char-mappings                  [STRUCTURE]
;;;                   component-mappings version-case type-case
;;;                   name-case component-case)
;;;
;;; *PERMANENT-TRANSLATION-RULES* (nil)                              [VARIABLE]
;;;    Alist of default translation rules for each type of host.
;;;
;;; *DEFAULT-TRANSLATION-RULE* ((make-translation-rule))             [VARIABLE]
;;;
;;; DEFINE-TRANSLATION-RULE (host-type &key case char-mappings          [MACRO]
;;;                          component-mappings version-case type-case
;;;                          name-case component-case)
;;;    Defines translation rules for hosts of type host-type.
;;;    Case may be nil (unchanged), :upper, :lower, or :capitalize.
;;;    Char-mappings is a list of character substitutions which occur in
;;;    parallel. Component-mappings is a list of string substitutions.
;;;
;;; FIND-TRANSLATION-RULE (host-type)                                [FUNCTION]
;;;
;;; CHOOSE-CASE (rule level)                                         [FUNCTION]
;;;
;;; CASIFY (thing case)                                              [FUNCTION]
;;;
;;; *DEFAULT-CANONICAL-TYPES* ((make-hash-table :test #'equal))      [VARIABLE]
;;;    Alists of canonical types and default surface types.
;;;
;;; *CANONICAL-TYPES-ALIST* ((make-hash-table :test #'equal))        [VARIABLE]
;;;    Alists of canonical types and surface types for various hosts.
;;;
;;; DEFINE-CANONICAL (level canonical default &body specs)              [MACRO]
;;;    Defines a new canonical type. Level specifies whether it is a 
;;;    canonical type, version, name, or component. Default is a string
;;;    containing the default surface type for any kind of host not
;;;    mentioned explicitly. The body contains a list of specs that
;;;    define the surface types that indicate the new canonical type for
;;;    each host. For systems with more than one possible default surface
;;;    form, the form that appears first becomes the preferred form for the
;;;    type. 
;;;
;;; MEMBER-OR-EQ (x list-or-atom)                                    [FUNCTION]
;;;
;;; SURFACE-FORM (canonical host-type &optional (level 'type))       [FUNCTION]
;;;
;;; CANONICALIZE (surface-form host-type &optional (level 'type))    [FUNCTION]
;;;
;;; PHYSICAL-PATHNAME (host device directory name type version)     [STRUCTURE]
;;;    Physical-Pathname is the underlying structure for a pathname.
;;;
;;; %PRINT-PHYSICAL-PATHNAME (pname stream depth)                    [FUNCTION]
;;;
;;; MAKE-PHYSICAL-PATHNAME (&key host device directory name type     [FUNCTION]
;;;                         version)
;;;
;;; ENSURE-PHYSICAL-PATHNAME (thing)                                 [FUNCTION]
;;;
;;; PHYSICAL-PATHNAME-HOST (pathname)                                [FUNCTION]
;;;    Returns the host of PATHNAME, which may be a string or pathname.
;;;
;;; PHYSICAL-PATHNAME-DEVICE (pathname)                              [FUNCTION]
;;;    Returns the device of PATHNAME, which may be a string or pathname.
;;;
;;; PHYSICAL-PATHNAME-DIRECTORY (pathname)                           [FUNCTION]
;;;    Returns the directory of PATHNAME, which may be a string or pathname.
;;;
;;; PHYSICAL-PATHNAME-NAME (pathname)                                [FUNCTION]
;;;    Returns the name of PATHNAME, which may be a string or pathname.
;;;
;;; PHYSICAL-PATHNAME-TYPE (pathname)                                [FUNCTION]
;;;    Returns the type of PATHNAME, which may be a string or pathname.
;;;
;;; PHYSICAL-PATHNAME-VERSION (pathname)                             [FUNCTION]
;;;    Returns the version of PATHNAME, which may be a string or pathname.
;;;
;;; LOGICAL-PATHNAME "()"                                           [STRUCTURE]
;;;    Logical-pathname is the underlying structure for a logical pathname.
;;;
;;; %PRINT-LOGICAL-PATHNAME (pname stream depth)                     [FUNCTION]
;;;
;;; MAKE-LOGICAL-PATHNAME (&key host directory name type version)    [FUNCTION]
;;;
;;; ENSURE-LOGICAL-PATHNAME (thing)                                  [FUNCTION]
;;;
;;; LOGICAL-PATHNAME-HOST (logical-pathname)                         [FUNCTION]
;;;    Returns the logical-pathname-host of LOGICAL-PATHNAME. 
;;;    LOGICAL-PATHNAME may be a string or logical pathname.
;;;
;;; LOGICAL-PATHNAME-DIRECTORY (logical-pathname)                    [FUNCTION]
;;;    Returns the logical-pathname-directory of LOGICAL-PATHNAME.
;;;    LOGICAL-PATHNAME may be a string or logical pathname.
;;;
;;; LOGICAL-PATHNAME-NAME (logical-pathname)                         [FUNCTION]
;;;    Returns the logical-pathname-name of LOGICAL-PATHNAME. 
;;;    LOGICAL-PATHNAME may be a string or logical pathname.
;;;
;;; LOGICAL-PATHNAME-TYPE (logical-pathname)                         [FUNCTION]
;;;    Returns the logical-pathname-type of LOGICAL-PATHNAME. 
;;;    LOGICAL-PATHNAME may be a string or logical pathname.
;;;
;;; LOGICAL-PATHNAME-VERSION (logical-pathname)                      [FUNCTION]
;;;    Returns the logical-pathname-type of LOGICAL-PATHNAME. 
;;;    LOGICAL-PATHNAME may be a string or logical pathname.
;;;
;;; LOGICAL-NAMESTRING (logical-pathname)                            [FUNCTION]
;;;    Returns the full form of LOGICAL-PATHNAME as a string.
;;;
;;; %DIRECTORY-STRING (dirlist &optional (host-type :logical)        [FUNCTION]
;;;                    (dir-delim #\;))
;;;    Converts a vector of the form #("foo" "bar" ... "baz") into
;;;    a string of the form "foo;bar;...;baz;"
;;;
;;; %VERSION-TO-STRING (version &optional (host-type :logical))      [FUNCTION]
;;;
;;; PHYSICAL-NAMESTRING (pathname)                                   [FUNCTION]
;;;
;;; LOGICAL-PATHNAME (thing &optional host)                          [FUNCTION]
;;;    Converts THING to a logical pathname and returns it. THING may be
;;;    a logical pathname, a logical pathname namestring containing a
;;;    host component, or a stream for which the pathname function
;;;    returns a logical pathname.
;;;
;;; PHYSICAL-PATHNAME (thing &optional host)                         [FUNCTION]
;;;    Converts THING to a physical-pathname and returns it. THING may be
;;;    a pathname, a pathname namestring containing a
;;;    host component, or a stream for which the file-name function
;;;    returns a pathname.
;;;
;;; PARSE-GENERIC-NAMESTRING (thing &optional host                   [FUNCTION]
;;;                           (defaults *default-pathname-defaults*)
;;;                           &key (start 0) end junk-allowed)
;;;    Convert namestring into a pathname.
;;;
;;; DO-GENERIC-PATHNAME-PARSE (string host-type &optional (start 0)  [FUNCTION]
;;;                            end)
;;;    Splits string into a logical host, a vector of directories, a file
;;;    name, a file type, and a file version.
;;;
;;; PARSE-GENERIC-PATHNAME (string &optional (start 0) end           [FUNCTION]
;;;                         (host-delim ":") (lead-is-abs t)
;;;                         (dir-delim "/") (name-delim ".")
;;;                         (type-delim ".") (version-delim "."))
;;;    Splits string into a host, vector of directories, a file name, type,
;;;    and version. Parses generic pathnames.
;;;
;;; PARSE-VMS-PATHNAME (string &optional (start 0) end)              [FUNCTION]
;;;    Splits string into a host, vector of directories, a file name, type,
;;;    and version. Parses VMS pathnames of the following formats:
;;;          host::device:[dir1.dir2...]name.type;version
;;;          host::device:<dir1.dir2...>name.type.version
;;;          host:device:<dir1.dir2...>name.type.version &c
;;;    .. = :wild-inferiors
;;;
;;; PARSE-EXPLORER-PATHNAME (string &optional (start 0) end)         [FUNCTION]
;;;    Splits string into a host, vector of directories, a file name, type,
;;;    and version. Parses TI Explorer pathnames of the following format:
;;;          host:dir1.dir2...;name.type#version
;;;
;;; *TRANSLATION-OUTPUT* (:namestring)                               [VARIABLE]
;;;    Specifies whether the output of translate-logical-pathname
;;;    should be a :namestring or a :pathname made with
;;;    lisp:make-pathname, or :as-is.
;;;
;;; DIRECTORY-STRUCTURE-TYPE ((cond                                  [CONSTANT]
;;;                           ((string-equal
;;;                           (lisp-implementation-type) "vax lisp")
;;;                           'list) (t 'list)))
;;;
;;; CONVERT-GENERIC-PATHNAME (pathname                               [FUNCTION]
;;;                           &optional
;;;                           (output-type *translation-output*))
;;;
;;; *CIRCULARITY-CHECK-TABLE* ((make-hash-table :test #'equal))      [VARIABLE]
;;;    This table is used to prevent infinite circular loops in the logical
;;;    pathname resolution. If a pathname's entry in this table is set
;;;    to T, it has already been "seen". Seeing such a pathname twice
;;;    is an error.
;;;
;;; TRANSLATE-LOGICAL-PATHNAME (logical-pathname                     [FUNCTION]
;;;                             &optional
;;;                             (output-format
;;;                             *translation-output*))
;;;    Translates a logical pathname to the corresponding physical pathname.
;;;    The pathname argument is first coerced to a logical pathname [this
;;;    should really be pathname, but for that we'd have to redefine
;;;    make-pathname and friends to check whether the host is a logical
;;;    host]. If the coerced argument is a logical pathname, the first
;;;    matching translation (according to LOGICAL-PATHNAME-MATCH-P) of the
;;;    logical pathname host is applied, as if by calling
;;;    TRANSLATE-LOGICAL-PATHNAME-AUX. If the result is a logical pathname,
;;;    this process is repeated. When the result is finally a physical
;;;    pathname, it is returned. If no translation matches a logical
;;;    pathname, or the resolution process loops, an error is signaled.
;;;    TRANSLATE-LOGICAL-PATHNAME may perform additional translations,
;;;    to provide translation of file types to local naming conventions,
;;;    to accommodate physical file systems with names of limited length, or
;;;    to deal with special character requirements such as translating
;;;    hyphens to underscores or uppercase letters to lowercase.
;;;
;;; RESOLVE-LOGICAL-PATHNAME (logical-pathname                       [FUNCTION]
;;;                           &optional
;;;                           (output-format *translation-output*))
;;;
;;; CHECK-LOGICAL-PATHNAME (pathname)                                [FUNCTION]
;;;
;;; MAP-LOGICAL-PATHNAME (logical-pathname host                      [FUNCTION]
;;;                       &optional
;;;                       (output-format *translation-output*))
;;;
;;; LOGICAL-PATHNAME-MATCH-P (logical-pathname from-pathname)        [FUNCTION]
;;;
;;; TRANSLATE-LOGICAL-PATHNAME-AUX (logical-pathname from-pathname   [FUNCTION]
;;;                                 to-pathname
;;;                                 &optional
;;;                                 (output-format
;;;                                 *translation-output*))
;;;
;;; *NULL-VECTOR* ((coerce nil 'simple-vector))                      [VARIABLE]
;;;
;;; WILDCARD-WORDP (string)                                          [FUNCTION]
;;;
;;; MUST-MATCH (thing)                                               [FUNCTION]
;;;
;;; MATCH-WILDCARD-WORD (template string)                            [FUNCTION]
;;;
;;; MATCH-STRINGS (template string &optional (t-start 0)             [FUNCTION]
;;;                (s-start 0))
;;;
;;; MATCH-DIRECTORIES (template dirs &optional (t-start 0)           [FUNCTION]
;;;                    (d-start 0))
;;;
;;; MAP-WILDCARD-WORD (string source target                          [FUNCTION]
;;;                    &optional case char-mappings string-mappings)
;;;
;;; MAP-STRINGS (string source target &optional (result "")          [FUNCTION]
;;;              (s-start 0) (st-start 0) (tt-start 0))
;;;
;;; MAP-DIRECTORIES (dirs source target                              [FUNCTION]
;;;                  &optional (result *null-vector*) (d-start 0)
;;;                  (s-start 0) (t-start 0) case char-map
;;;                  string-map)
;;;
;;; APPEND-LOGICAL-DIRECTORIES (absolute-dir relative-dir)           [FUNCTION]
;;;
;;; REAL-FILENAME (filename)                                         [FUNCTION]
;;;
;;; CONVERT-FILE-FUNCTION (name)                                        [MACRO]
;;;
;;; CONVERT-FILE-FUNCTION-2-ARGS (name)                                 [MACRO]
;;;

;;;
;;; CAP-CSP ""                                                          [CLASS]
;;;    Superclasses
;;;    csp
;;;    Initialization Arguments
;;;    Readers
;;;    capacity-constraints	Generic Function
;;;    	cap-csp
;;;    Returns list of capacity constraint instances
;;;    Writers
;;;    setf (capacity-constraints cap-csp)	Generic Setf Form
;;;    	capacity-constraints
;;;    The capacity-constraints argument should be a list of capacity
;;;    constraint instances 
;;;
;;; RESET-CSP :BEFORE ((c cap-csp))                                    [METHOD]
;;;
;;; RESET-CSP :AFTER ((c cap-csp))                                     [METHOD]
;;;
;;; INIT-BEFORE-LOCKING-VARS ((c cap-csp))                             [METHOD]
;;;
;;; SHOW-CSP-STATE :AFTER ((c cap-csp) &key)                           [METHOD]
;;;
;;; CHECK-CSP-STATE :AFTER ((c cap-csp))                               [METHOD]
;;;
;;; WHY-CONFLICTS ((csp cap-csp) var value)                            [METHOD]
;;;    Extension to why-conflicts for capacity constraints
;;;
;;; CSP-VAR-VALUE-ASSIGN :AFTER ((c cap-csp) (v var) value)            [METHOD]
;;;    var v has its assignment made to value: update state
;;;
;;; CSP-VAR-VALUE-UNASSIGN :AFTER ((c cap-csp) (v var) value)          [METHOD]
;;;    var v has its assignment unmade from value: update state
;;;
;;; ASSIGNMENTS-WOULD-CONFLICT-P ((c cap-csp) (v1 var) value1          [METHOD]
;;;                               (v2 var))
;;;    return t if v2 has an assigned value that would conflict with
;;;    assigning val1 to v1, nil otherwise
;;;       NOT IMPLEMENTED FOR CAP-CSP
;;;
;;; FIND-CAPACITY-CONSTRAINT (cap name)                      [GENERIC FUNCTION]
;;;    Find capacity constraint instance by name.  Return instance if found,
;;;       nil if not
;;;
;;; FIND-CAPACITY-CONSTRAINT ((c csp) name)                            [METHOD]
;;;
;;; FIND-CAPACITY-CONSTRAINT ((c cap-csp) name)                        [METHOD]
;;;
;;; CAPACITY-CONSTRAINT ""                                              [CLASS]
;;;    Superclasses
;;;    None.
;;;    Initialization Arguments
;;;    The :csp argument is a 
;;;    The :name argument is a 
;;;    The :bin-count argument is a 
;;;    The :bin-limit argument is a 
;;;    The :bin-usage argument is a 
;;;    The :remove-values-from-domain-if-exceed-capacity-p argument is a 
;;;    The :remove-values-from-domain-if-not-in-any-bin-p argument is a 
;;;    The :cap-constraint-weight argument is a 
;;;    Readers
;;;    csp	Generic Function
;;;    	capacity-constraint
;;;    Returns csp instance to which this constraint attached
;;;    name	Generic Function
;;;    	capacity-constraint
;;;    Returns print name of this constraint
;;;    bin-count	Generic Function
;;;    	capacity-constraint
;;;    Returns number of bins
;;;    bin-limit	Generic Function
;;;    	capacity-constraint
;;;    Returns array length bin-count of limits on bin usage
;;;    bin-usage	Generic Function
;;;    	capacity-constraint
;;;    Returns array length bin-count of current bin usage
;;;    remove-values-from-domain-if-exceed-capacity-p	Generic Function
;;;    	capacity-constraint
;;;    Returns what to do on initialization when values require more
;;;    capacity than is available in a bin:
;;;         If t, then values are removed from the domain temporarily
;;;         If :permanent then values are removed from domain permanently
;;;         If nil, the conflict count is increased
;;;    remove-values-from-domain-if-not-in-any-bin-p	Generic Function
;;;    	capacity-constraint
;;;    Returns what to do on initialization with values that do not belong
;;;    to any bin:  choices are:
;;;         nil - no action
;;;         t - values are removed from domain temporarily
;;;         :permanent - values are removed from domain permanently
;;;    cap-constraint-weight	Generic Function
;;;    	capacity-constraint
;;;    Returns weight of constraint for conflict count changes: default 1
;;;    Writers
;;;    setf (csp capacity-constraint)	Generic Setf Form
;;;    	csp
;;;    The csp argument should be a csp instance to which this constraint
;;;    attached setf (name capacity-constraint)	Generic Setf Form
;;;    	name
;;;    The name argument should be a print name of this constraint
;;;    setf (bin-count capacity-constraint)	Generic Setf Form
;;;    	bin-count
;;;    The bin-count argument should be a number of bins
;;;    setf (bin-limit capacity-constraint)	Generic Setf Form
;;;    	bin-limit
;;;    The bin-limit argument should be a array length bin-count of limits
;;;    on bin usage setf (bin-usage capacity-constraint)	Generic Setf Form
;;;    	bin-usage
;;;    The bin-usage argument should be a array length bin-count of current
;;;    bin usage setf (remove-values-from-domain-if-exceed-capacity-p
;;;    capacity-constraint)	Generic Setf Form
;;;    remove-values-from-domain-if-exceed-capacity-p The
;;;    remove-values-from-domain-if-exceed-capacity-p argument should be a
;;;    what to do on initialization when values require more capacity than
;;;    is available in a bin: If t, then values are removed from the domain
;;;    temporarily If :permanent then values are removed from domain
;;;    permanently If nil, the conflict count is increased setf
;;;    (remove-values-from-domain-if-not-in-any-bin-p
;;;    capacity-constraint)	Generic Setf Form
;;;    remove-values-from-domain-if-not-in-any-bin-p The
;;;    remove-values-from-domain-if-not-in-any-bin-p argument should be a
;;;    what to do on initialization with values that do not belong to any
;;;    bin:  choices are: nil - no action t - values are removed from domain
;;;    temporarily :permanent - values are removed from domain permanently
;;;    setf (cap-constraint-weight capacity-constraint)	Generic Setf Form
;;;    cap-constraint-weight The cap-constraint-weight argument should be a
;;;    weight of constraint for conflict count changes: default 1 
;;;
;;; GENERAL-CAPACITY-CONSTRAINT ""                                      [CLASS]
;;;    Generalization of capacity constraints to handle multiple bins for
;;;    values as well as multiple values for bins
;;;    Superclasses
;;;    capacity-constraint
;;;    Initialization Arguments
;;;    None.
;;;    Readers
;;;    None.
;;;    Writers
;;;    None.
;;;
;;; MAKE-CAPACITY-CONSTRAINT (csp &rest args                         [FUNCTION]
;;;                           &key
;;;                           (capcon-type 'capacity-constraint)
;;;                           (name "anon") limit-array
;;;                           (remove-values-if-capacity-exceeded t)
;;;                           (remove-values-if-not-in-any-bin nil)
;;;                           (weight 1) &allow-other-keys)
;;;    Create and attach a capacity constraint.  
;;;       CSP must be a CSP of type cap-csp with no current assignments.
;;;       limit-array must be an array of length bin-count of bin limiting
;;;    quantities. If limit-array is nil, then it must be set by
;;;    INIT-CAPACITY-CONSTRAINT along with the bin-count and bin-usage
;;;    slots. All others default as follows:
;;;        capcon-type = capacity-constraint
;;;        name = Anon
;;;        remove-values-if-capacity-exceeded defaults to t
;;;        remove-values-if-not-in-any-bin defaults to nil
;;;
;;; INIT-CAPACITY-CONSTRAINT ((capcon capacity-constraint) csp args)   [METHOD]
;;;    Default method for initializing a capacity constraint.  Currently
;;;        it does nothing for base class.  Args is the keyword/value list 
;;;        given to the constructor make-capacity-constraint.
;;;
;;; REMOVE-CAPACITY-CONSTRAINT ((c cap-csp)                            [METHOD]
;;;                             (cap-con capacity-constraint))
;;;    remove a capacity constraint.  Returned value is list of assignments
;;;       in effect (as from get-assignments; could be input to
;;;    put-assignments). Assignments are all unmade.
;;;
;;; REQUIRED-CAPACITY (csp cap-con var bin value)            [GENERIC FUNCTION]
;;;    function called with 5 args:  csp, cap constraint, var, bin, and
;;;    value. Returns capacity required in order to make assignment without
;;;    conflicts. May return nil to mean no capacity required
;;;
;;; MAX-REQUIRED-CAPACITY (csp cap-con var)                  [GENERIC FUNCTION]
;;;    function called with 3 args:  csp, cap constraint, and var.  Returns 
;;;         max capacity required in order to make assignment without
;;;    conflicts anywhere in domain.  This function can return nil to
;;;    indicate NO capacity required anywhere, or t to indicate that the
;;;    maximum is not known.  This is only used to improve efficiency so t
;;;    is OK as default.
;;;
;;; CONSUMED-CAPACITY (csp cap-con var bin value)            [GENERIC FUNCTION]
;;;    function called with 5 args:  csp, cap constraint, var, bin, and
;;;    value. Returns capacity consumed when var assigned to bin number. 
;;;    Usually this is the same as required-capacity.  In all cases it must
;;;    be less than or equal to required capacity.  May return nil to
;;;    indicate no capacity required
;;;
;;; VALUE-TO-BIN (csp cap-con var value)                     [GENERIC FUNCTION]
;;;    function which takes as args the csp, the capacity constraint
;;;    instance, a var, and a value: returns bin number corresponding to
;;;    value. Default is value number = bin number.  A value is not required
;;;    to belong to a bin, i.e. this function can return nil.
;;;
;;; VALUE-TO-BINS (csp cap-con var value)                    [GENERIC FUNCTION]
;;;    function which takes as args the csp, the capacity constraint
;;;    instance, a var, and a value: returns list of bin numbers
;;;    corresponding to value. Default is value number = bin number.  A
;;;    value is not required to belong to a bin, i.e. this function can
;;;    return nil. 
;;;
;;; BIN-TO-VALUES (csp cap-con var bin)                      [GENERIC FUNCTION]
;;;    function which takes as args the csp, the capacity constraint
;;;    instance, a var, and a bin number: returns list of values
;;;    corresponding to bin.  Default is value number = bin number.  May
;;;    return nil, i.e. a bin does not correspond to any values for a var.
;;;
;;; REQUIRED-CAPACITY ((c csp) (capcon capacity-constraint) (v var)    [METHOD]
;;;                    bin val)
;;;
;;; MAX-REQUIRED-CAPACITY ((c csp) (capcon capacity-constraint)        [METHOD]
;;;                        (v var))
;;;
;;; CONSUMED-CAPACITY ((c csp) (capcon capacity-constraint) (v var)    [METHOD]
;;;                    bin val)
;;;
;;; VALUE-TO-BIN ((c csp) (capcon capacity-constraint) (v var) val)    [METHOD]
;;;
;;; BIN-TO-VALUES ((c csp) (capcon capacity-constraint) (v var) bin)   [METHOD]
;;;
;;; VALUE-TO-BIN ((c csp) (capcon general-capacity-constraint)         [METHOD]
;;;               (v var) val)
;;;
;;; VALUE-TO-BINS ((c csp) (capcon general-capacity-constraint)        [METHOD]
;;;                (v var) val)
;;;
;;; PRINT-OBJECT ((capcon capacity-constraint) stream)                 [METHOD]
;;;
;;; RESET-CONSTRAINT ((capcon capacity-constraint))                    [METHOD]
;;;    Reset bin usage.  Note that if there are locked vars, init
;;;       conflicts must be called to set current bin usage.
;;;
;;; SHOW-CONSTRAINT ((capcon capacity-constraint) stream)              [METHOD]
;;;    Print capacity constraint description to stream
;;;
;;; VARS-ASSIGNED-TO-BIN ((capcon capacity-constraint) bin)            [METHOD]
;;;    Return list of vars assigned to specified bin.  Includes both
;;;       locked and unignored vars.
;;;
;;; CONFLICTING-ASSIGNMENTS ((capcon capacity-constraint) (v var)      [METHOD]
;;;                          value)
;;;    Return list of vars whose current assignments conflict with the
;;;    assignment of var to value, due to this capacity constraint only
;;;
;;; INIT-CAPACITY-CONSTRAINT-CONFLICTS ((capcon capacity-constraint))  [METHOD]
;;;    Initialize conflict counts for a capacity constraint.  
;;;       Looks at all vars (unignored, ignored, and locked).  
;;;       Conflict counts are always updated.  Depending on flag 
;;;       remove-values-from-domain-if-exceed-capacity-p, values are also
;;;    removed from domain.
;;;       THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
;;;        (i.e. before locked vars are assigned).
;;;
;;; INIT-CAPACITY-CONSTRAINT-DOMAINS ((capcon capacity-constraint))    [METHOD]
;;;    Initialize conflict counts for a capacity constraint.  
;;;       Looks at all vars (unignored, ignored, and locked).  
;;;       Conflict counts are always updated.  Depending on flag 
;;;       remove-values-from-domain-if-exceed-capacity-p, values are also
;;;    removed from domain.
;;;       THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
;;;        (i.e. before locked vars are assigned).
;;;
;;; CHECK-CAPACITY-CONSTRAINT ((capcon capacity-constraint))           [METHOD]
;;;    Check that usage matches assignments.  Print any discrepancies to
;;;    standard output.  Debugging only.
;;;
;;; CHANGE-BIN-LIMIT ((capcon capacity-constraint) bin new-limit)      [METHOD]
;;;    Change usage limit on specified bin to new value. Update conflict
;;;       counts on all variables.  new-limit can either be more or
;;;       less than old limit.  Note:  this only checks values currently in
;;;    domain, and does not remove values from domain if they exceed
;;;    capacity. To do this, change bin limits, then reset CSP, making sure
;;;    that flag remove-values-from-domain-if-exceed-capacity-p is t
;;;
;;; VARS-ASSIGNED-TO-BIN ((capcon general-capacity-constraint) bin)    [METHOD]
;;;    Return list of vars assigned to specified bin.  Includes both
;;;       locked and unignored vars.
;;;
;;; CONFLICTING-ASSIGNMENTS ((capcon general-capacity-constraint)      [METHOD]
;;;                          (v var) value)
;;;    Return list of vars whose current assignments conflict with the
;;;    assignment of var to value, due to this capacity constraint only
;;;
;;; INIT-CAPACITY-CONSTRAINT-CONFLICTS ((capcon                        [METHOD]
;;;                                     general-capacity-constraint))
;;;    Initialize conflict counts for a capacity constraint.  
;;;       Looks at all vars (unignored, ignored, and locked).  
;;;       Conflict counts are always updated.  Depending on flag 
;;;       remove-values-from-domain-if-exceed-capacity-p, values are also
;;;    removed from domain.
;;;       THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
;;;        (i.e. before locked vars are assigned).
;;;
;;; INIT-CAPACITY-CONSTRAINT-DOMAINS ((capcon                          [METHOD]
;;;                                   general-capacity-constraint))
;;;    Initialize conflict counts for a capacity constraint.  
;;;       Looks at all vars (unignored, ignored, and locked).  
;;;       Conflict counts are always updated.  Depending on flag 
;;;       remove-values-from-domain-if-exceed-capacity-p, values are also
;;;    removed from domain.
;;;       THIS MUST BE CALLED WHILE THERE ARE NO ASSIGNMENTS 
;;;        (i.e. before locked vars are assigned).
;;;
;;; CHECK-CAPACITY-CONSTRAINT ((capcon general-capacity-constraint))   [METHOD]
;;;    Check that usage matches assignments.  Print any discrepancies to
;;;    standard output.  Debugging only.
;;;
;;; CHANGE-BIN-LIMIT ((capcon general-capacity-constraint) bin         [METHOD]
;;;                   new-limit)
;;;    Change usage limit on specified bin to new value. Update conflict
;;;       counts on all variables.  new-limit can either be more or
;;;       less than old limit.  Note:  this only checks values currently in
;;;    domain, and does not remove values from domain if they exceed
;;;    capacity. To do this, change bin limits, then reset CSP, making sure
;;;    that flag remove-values-from-domain-if-exceed-capacity-p is t
;;;
;;; CAPACITY-CONSTRAINT-VAR-VALUE-ASSIGN ((capcon                      [METHOD]
;;;                                       general-capacity-constraint)
;;;                                       (v var) value)
;;;    var v has its assignment made to value: update state
;;;
;;; CAPACITY-CONSTRAINT-VAR-VALUE-UNASSIGN ((capcon                    [METHOD]
;;;                                         general-capacity-constraint) (v var) value)
;;;    var v has its assignment unmade from value: update state
;;;
;;; WOULD-FIT-BEFORE-WILL-FIT-NOW (old-bin-usage new-bin-usage       [FUNCTION]
;;;                                bin-limit var-assignment-changed
;;;                                consumed-by-value-assignment-changed var-to-check required-by-value-to-check var-to-check-assigned-to-bin-to-check-p
;;;                                consumed-by-var-to-check-assigned-value)
;;;    Return multiple values
;;;        would-fit-before:   non-nil if value-to-check was not in
;;;    conflict with a capacity constraint when old-bin-usage was committed
;;;        will-fit-now:       non-nil if value-to-check is not in conflict
;;;    with capacity constraint when new-bin-usage is committed
;;;    bin-to-check refers to the bin being checked.
;;;    var identifiers are only used to test for equality, i.e. whether
;;;    var-assignment-changed is the same as var-to-check or not.
;;;    The args are all numeric >= 0 unless specified:
;;;        -- quantites related to the bin --
;;;        old-bin-usage              : quantity of resource previously
;;;    consumed by assignments in bin-to-check. This must include
;;;                                     all of the capacity committed by
;;;    all assignments except the one currently being made.
;;;        new-bin-usage              : quantity of resource consumed after
;;;    changed assignment in bin-to-check. This must include
;;;                                     all of the capacity committed by
;;;    all assignments made. If new-bin-usage > old-bin-usage the
;;;                                     var is being assigned, else it is
;;;    being unassigned. bin-limit                  : maximum quantity of
;;;    resource consumable without conflict in bin-to-check
;;;        -- quantities related to the variable/value currently being
;;;    assigned -- var-assignment-changed     : number of var currently
;;;    being assigned/unassigned
;;;        consumed-by-value-assignment-changed:  capacity consumed by 
;;;                                    
;;;    var-assignment-changed/value-assignment-changed : = |new-bin-usage -
;;;    old-bin-usage| -- quantities related to the variable/value being
;;;    checked for conflicts-- var-to-check               : number of the
;;;    var being checked for conflicts. required-by-value-to-check :
;;;    quantity of resource required by var for assignment to
;;;    value-to-check.  May be nil if no capacity required.
;;;        var-to-check-assigned-to-bin-to-check-p  : t if the var-to-check
;;;    is assigned to the bin being checked, nil if not assigned
;;;                                     or if value-assigned does not
;;;    belong to any bin consumed-by-var-to-check-assigned-value: quantity
;;;    of resource consumed by assigned value of var-to-check,
;;;                                     or nil if not assigned or if
;;;    assigned value does not require resource
;;;        
;;;
;;; CAPACITY-CONSTRAINT-VAR-VALUE-ASSIGN ((capcon                      [METHOD]
;;;                                       capacity-constraint)
;;;                                       (v var) value)
;;;    var v has its assignment made to value: update state
;;;
;;; CAPACITY-CONSTRAINT-VAR-VALUE-UNASSIGN ((capcon                    [METHOD]
;;;                                         capacity-constraint)
;;;                                         (v var) value)
;;;    var v has its assignment unmade from value: update state
;;;

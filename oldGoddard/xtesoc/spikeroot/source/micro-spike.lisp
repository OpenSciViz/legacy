;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; micro-spike.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: micro-spike.lisp,v $
;;; Revision 3.0  1994/04/01 19:36:12  buehler
;;; Initial XTE version.
;;;
;;; Revision 1.22  1991/12/01  00:22:05  johnston
;;; eliminated redundant readers and accessors; cleaned up other compiler
;;; warnings.  No substantive changes.
;;;
;;; Revision 1.21  1991/11/26  15:41:27  johnston
;;; no substantive changes
;;;
;;; Revision 1.20  1991/10/07  15:55:00  mrose
;;; Add comments.
;;;
;;; Revision 1.19  1991/09/04  19:29:38  sponsler
;;; STIF3:
;;; New functions: tasks-related-to-task, all-tasks-related-to-task
;;;
;;; Revision 1.18  91/07/19  16:55:00  gerb
;;; reported collapsed constraints to record
;;; 
;;; Revision 1.17  91/07/16  14:07:22  gerb
;;; added hook for constraint propagation reports
;;; 
;;; Revision 1.16  91/06/06  08:51:30  johnston
;;; added method INIT-CHRONIKA for specializable chronika initialization
;;; 
;;; Revision 1.15  91/05/17  15:26:21  gerb
;;; made min-time-separation-constraint more flexible
;;; 
;;; Revision 1.14  91/05/01  14:27:11  krueger
;;; Added UNDO-ALL-RESTRICTIONS-IN-CHRONIKA.  Used by CSP
;;; 
;;; Revision 1.13  91/04/30  09:51:19  giuliano
;;; Defined new interface functions for adding constraints to micro-spike.
;;; The function mutually-constrain was replaced by new functions.  There is a
;;; new function for each of the cases in mutually-constrain. 
;;; Added new slots to the definition of rel-time-interface-constraint to allow
;;; calls to micro spike to be stored in a log.  Added keyword arguments to
;;; constraint functions for logging constraints.
;;; 
;;; Revision 1.12  91/04/15  13:02:17  johnston
;;; added newline at end of file
;;; 
;;; Revision 1.11  91/04/11  11:14:35  krueger
;;; Added Defgeneric's for effect-of-task-on-task-given-interval and
;;; limit-based-on-boundaries.  Added method undo-restriction, which
;;; was needed by the CSP applicatiion
;;; 
;;; Revision 1.10  91/03/25  13:20:16  johnston
;;; added task constraint methods for csp, added init-task method
;;; 
;;; Revision 1.9  91/01/24  10:45:09  mrose
;;; Fix pcf-i-before-j.
;;; 
;;; Revision 1.8  91/01/15  13:44:10  mrose
;;; Modify tasks-affected-via-constraint and tasks-affecting-via-constraint.
;;; Modify specialization for collabsible-binary-rel-constraint.  Map over 
;;; constrains/constrained-by, returning list of tasks collapsed wrt them.
;;; 
;;; Revision 1.7  90/12/12  10:10:38  mrose
;;; Add "constrains to be unschedulable" errors in DEFINE-BINARY-CONSTRAINT
;;; method for collapsible-binary-rel-constraint.
;;; 
;;; Revision 1.6  90/11/26  17:07:00  mrose
;;; Fix bug crashing trans when early start of pcf is nil.
;;; 
;;; Revision 1.5  90/10/24  19:26:35  mrose
;;; New strictness.
;;; 
;;; Revision 1.4  89/12/04  01:47:27  johnston
;;; added new constraint function strict-pair-order-asap
;;; 
;;; Revision 1.3  89/11/17  04:10:49  johnston
;;; fixed bug in check-arc-and-propagate
;;; 
;;; Revision 1.2  89/11/12  03:21:27  johnston
;;; got rid of remove-duplicates from filter-arcs-to-check
;;; for speed.
;;; 
;;;
;;; micro-spike.lisp
;;;
;;; CLOS activities and constraints for Transformation sequencing and links
;;;
;;; Updated 18 Apr 1989 MDJ for TI CLOS
;;; 21 Apr 1989 MDJ class names changed to not collide with Flavors Spike
;;;  9 Jun 1989 GM et all - documentation
;;; 26 Jun 1989 MDJ more documentation, revision of some methods, added
;;;             CPU cutoff on path consistency, etc.
;;; 16 Jul 1989 MDJ more changes to chronika, constraints, etc. bug fixes
;;; 29 Sep 1989 MDJ performance improvements in check-arcs-and-propagate,
;;;             made CPU cutoff more strict, added some preference constraints
;;;

#|----------------------------------------------------------------------

Notational conventions:

  chr chronika
  c constraint
  i,j,... tasks. Also a, b,... in a few places,
                 (since tasks used to be known as activities)

Note:  Don't use 'self as a variable in methods:  this is a special variable
on Explorer and is used in flavor methods

Dependencies:  this file depends on astro-utilities (and possibly
general-utilities) only.  It does not depend on transformation syntax
or CBASE.  DO NOT INTRODUCE THESE DEPENCENCIES.

These routines define general objects and methods for tasks (entities
to schedule, analogs of activities in flavors spike) and task
constraints (there are analogs of both absolute and relative constraints).


----------------------------------------------------------------------|#

;;; ==================================================================
;;;                                     GLOBALS

;;; current default chronika instance.  Set by make-chronika, default
;;; used by make-task

(defvar *CURRENT-CHRONIKA* nil)


;;; *verbose-constraint-prop* and *very-verbose-constraint-prop* are
;;; t or a stream, or nil.
;;; if non-nil, the value of each changed slot is sent to the specified
;;; stream

(defvar *VERBOSE-CONSTRAINT-PROP* nil)
(defvar *VERY-VERBOSE-CONSTRAINT-PROP* nil)

;;; Added AAG, 7-15-91.  Hook for trans report generator

(defvar *RECORD-CONSTRAINTS-FOR-TRANS-REPORT* nil)

(defmacro Record-Constraint-For-Trans
	  (&optional (t1 'i) (t2 'j) (instance 'instance) 
	    (sr 'special-requirement) (constraint 'c))
  `(when *RECORD-CONSTRAINTS-FOR-TRANS-REPORT*
     (CPR-Log-Constrant-Propagation-Event 
       ,constraint ,t1 ,t2  ,sr ,instance ))
  )

;;; ==================================================================
;;;                                     CHRONIKA

(defclass CHRONIKA () 
  ((name
     :initform "" :initarg :name :accessor name
     :documentation
     "Identifying name of this chronika (string)")
   
   ;; Start and end times:
   
   (start
     :initform 0 :initarg :start :accessor start
     :documentation 
     "All scheduling for the tasks in a chronika will be limited to start
      at or after this time") 
   (end 
     :initform 10000 :initarg :end :accessor end
     :documentation 
     "All scheduling for the tasks in a chronika will be limited to end
      at or before this time")
   
   ;; member tasks:
   
   (task-table
     :initform (make-hash-table) :accessor task-table
     :documentation 
     "hash-table of tasks in the chronika: 
      key is task ID, value is task instance")
   
   (task-list
     :initform nil :accessor task-list
     :documentation 
     "list of task instances (i.e. list of all keys in task-table).
      This is redundant with the task-table for speed of access")
   
   ;; constraint propagation status
   
   (consistent
     :initform nil :accessor consistent
     :documentation  
     "flag specifying constraint consistency status: t if constraints currently 
      consistent, nil otherwise (i.e. constraint propagation is necessary
      before suitabilities can be accessed)")
   
   (propagating-constraints 
     :initform nil :accessor propagating-constraints
     :documentation 
     "flag specifying constraint propagation status:  t while constraints are 
      being propagated, nil otherwise.  This is used to prevent constraint 
      propagation from starting while it is already in progress (since many 
      functions initiate constraint propagation and they can call each other")
   
   ;; history keeping slots:
   
   (keep-history 
     :initform nil :initarg :keep-history :accessor keep-history
     :documentation 
     "flag: t if history is being recorded, otherwise nil.
      Use setf to change.")
   
   (history 
     :initform (list 'history-start) :accessor history
     :documentation 
     "history is a list of triples (object slot value) or symbols.
      Symbols are markers: they are added to the front of list after
      constraint propagation is complete.
      Backing up the history goes from symbol to symbol.
      'history-start is a reserved symbol and indicates the start of
      all history records:  no further backing up is possible")
  
   )

  (:documentation 
    "A CHRONIKA is a collection of tasks (entities to schedule)
     A task can only belong to one chronika.  
     A chronika can contain an arbitrary number of tasks"))



(defun MAKE-CHRONIKA (&rest args
                            &key 
                            (name "anonymous")
                            (start 0)
                            (end 100)
                            (keep-history nil)
                            (chronika-type 'chronika)
                            &allow-other-keys)

  "Create a new chronika instance and make it the current chronika.
    The chronika instance is returned."
  
  (let ((instance 
	  (make-instance chronika-type 
                         :name name
			 :start start
			 :end end
			 :keep-history keep-history)))
    (setf *current-chronika* instance)
    (init-chronika instance args)
    instance))


(defmethod INIT-CHRONIKA ((chr chronika) args)
  "special initialization for chronika.  Args is keyword value list
    as supplied to MAKE-CHRONIKA"
  (declare (ignore args))
  ;; default does nothing
  )


(defmethod PRINT-OBJECT ((chr chronika) stream)
  "print method for chronika instances"
  (format stream "<Chronika ~a with ~d tasks>"
          (name chr)
          (list-length (task-list chr))))


(defmethod LOOKUP-TASK ((chr chronika) id)
	   
  "given task ID, look up and return task.  Return
    task instance, or nil if none with that ID"
  
  (gethash id (task-table chr)))



(defmethod CHECK-CONSISTENCY ((chr chronika))

  "Propagate constraints for all activities in chronika, i.e. until
    all are consistent. Returns t if chronika is consistent (via 
    constraint propagation or if already consistent), nil if not."
  
  (cond
    
    ;; only propagate if not already in progress
    ((propagating-constraints chr)
     ;; returned value is nil
     nil)
    
    ;; only propagate when not consistent
    ;; ?? need an unwind-protect in here...
    ((consistent chr)
     ;; return t, i.e. chronika is consistent
     t)

    ;; not consistent and not already propagating:  start
    ;; the propagation
    (t
     
     ;; set flag indicating constraint propagation in progress
     (setf (propagating-constraints chr) t)
     
     ;; loop until all tasks have consistent constraints
     (let ((counter 0))				       ;count iterations
       (if *VERY-VERBOSE-CONSTRAINT-PROP*
	   (format *VERY-VERBOSE-CONSTRAINT-PROP* "~%propagating..."))
       (loop
	 (if (consistent chr) (return nil))
	 (incf counter)
	 (if *VERY-VERBOSE-CONSTRAINT-PROP*
	     (format *VERY-VERBOSE-CONSTRAINT-PROP* "~%Iteration: ~d" counter))
	 ;; mark as consistent
	 (setf (consistent chr) t)
	 ;; update suitabilities of all tasks:  if still consistent
	 ;; then done (see top of loop), otherwise repeat the loop
	 (dolist (a (task-list chr))
	   ;; compute suitability of a task based on scheduling times
	   ;; of those to which it is related
	   (update-suitability a))))
     ;; exited from loop:  all suitabilities are now consistent
     
     ;; set flag that constraint propagation complete
     (setf (propagating-constraints chr) nil)
     
     ;; put history marker on top of list indicating this is 
     ;; a consistent point to back up to
     (mark-history chr)

     ;; update status
     (update-chronika-status chr)
     
     ;; update any graphics if available
     ;; ?? always call a graphics updater here...
     (if (fboundp 'update-chronika-picture)
	 ;; use funcall to prevent compiler warnings about undefined
	 ;; function
	 (funcall 'update-chronika-picture))
     
     ;; returned value:  t if constraints are consistent
     t)))


(defmethod UPDATE-CHRONIKA-STATUS ((chr chronika))
  
  "Derive any necessary information about the status of the
    chronika.  This method called after consistency checking
    completes and the chronika is consistent.  Default is
    no action."

  nil)


(defmethod PRINT-TASK-DIAGNOSTICS ((chr chronika) &optional (stream t))

  "Print diagnostic messages for all tasks in the chronika"

  (dolist (a (task-list chr))
    (let ((messages (diagnostic-messages a)))
      (when messages
        (format stream "~%Task: ~a" (id a))
        (dolist (m messages)
          (format stream "~%  ~a" m))))))


;;; history-keeping methods
;;; Note: this should be generalized to handle more than slot-value changes

(defmethod ADD-TO-HISTORY ((chr chronika) object slot value)

  "Add record to history indicating that object had slot changed from value"

  (if (keep-history chr)
    (push (list object slot value) (history chr)))
    (values))


(defmethod MARK-HISTORY ((chr chronika))

  "Push a marker on the history stack.  History backups only proceed from
    marker to marker. Returns history marker if history keeping is in
    effect, otherwise nil."

  (if (keep-history chr)
      (let ((marker (gentemp "HISTORY-")))
	(push marker (history chr))
	marker)))

(defmethod CLEAR-HISTORY ((chr chronika))

  "Initialize the history stack. No return value"

  (setf (history chr) (list 'history-start))
  (values))


(defmethod HISTORY-MARKER ((chr chronika))

  "Return the current history marker.  The caller can save this marker
    and back up to this point later."

  ;; marker must be meaningful, so propagate constraints if necessary
  (check-consistency chr)

  ;; if there's not a marker at the top of the history, then put one there
  ;; if there is, then just return it
  (if (symbolp (first (history chr)))
    (first (history chr))
    (mark-history chr)))


(defmethod BACKUP-HISTORY ((chr chronika))
  
  "Back up history to previous marker. If history keeping is disabled,
    a warning is signalled"
  
  (let ((keep-flag (keep-history chr))
        (hist (history chr))
        (changed-tasks (make-hash-table :test #'eq)))
    
    ;; ?? is this necessary
    (check-consistency chr)
    
    ;; don't want to keep history while rolling back history, so turn
    ;; it off here.  Restore history-keeping status before exiting below.
    (setf (keep-history chr) nil)
    
    (cond 
      
      ((not hist)
       (warn "no history available"))
      
      (t (if *VERBOSE-CONSTRAINT-PROP*
	     (format *VERBOSE-CONSTRAINT-PROP*
		     "~%Backing up from ~a" (first hist)))
	 (dolist (h (rest hist))
	   (when (symbolp h)
	     (if *VERBOSE-CONSTRAINT-PROP*
		 (format *VERBOSE-CONSTRAINT-PROP*
			 "~%Backup complete to ~a" h))
	     (setf (history chr)
		   (subseq hist (position h hist)))
	     (return))
	   (setf (slot-value (first h) (second h)) (third h))
	   (if (typep (first h) 'task)
	       (setf (gethash (first h) changed-tasks) t))
	   )
	 ;; tell constraints that task has changed
	 (maphash #'(lambda (task val)
		      (declare (ignore val))
		      (dolist (c (constraints task))
			(task-has-changed c task)))
		  changed-tasks)))
    
    ;; restore history-keeping status flag
    (setf (keep-history chr) keep-flag)
    
    (check-consistency chr)
    
    ;; update any graphics if available
    (if (fboundp 'update-chronika-picture)
	(funcall 'update-chronika-picture))
    ))


(defmethod BACKUP-HISTORY-TO-MARKER ((chr chronika) marker)
  
  "Back up history to a specified marker.  A warning is signalled if 
    the marker doesn't exist, or if history is already backed
    up to that marker."
  
  (cond 
   
   ((eq marker (history-marker chr))
    (warn "already at specified marker ~a" marker))
   
   ((position marker (history chr))
    (loop (backup-history chr)
          (if (eq marker (history-marker chr))
            (return))))
   
   (t (warn "marker ~a not found" marker))))


(defmethod DEFAULT-RESTRICTION ((chr chronika))

  "return the PCF representing the default restriction for this
    chronika"

  *unity-PCF*)

;;; ==================================================================
;;;                                     TASK

(defclass TASK () 

  ((id 
     :initform (gentemp "T-") :initarg :id :reader id
     :documentation
     "Unique task ID for this task")
   
   (chronika 
     :initarg :chronika :reader chronika
     :documentation 
     "The chronika to which this task belongs")
   
   (diagnostic-messages 
     :initform nil :accessor diagnostic-messages
     :documentation 
     "List of diagnostic messages (strings)")
   
   ;; task durations
   
   (minimum-duration 
     :initform 0 :initarg :minimum-duration :reader minimum-duration
     :documentation
     "Minimum task duration, integer ticks")

   (maximum-duration
     :initform nil :initarg :maximum-duration
     :documentation
     "Maximum task duration, integer ticks.  If nil, then the minimum duration
      is taken to be the maximum duration.")
   
   (properties
     :initform nil :reader properties
     :documentation 
     "Property list (:key1 value1 ...); properties are arbitrary keywords")
   
   (constraints
     :initform nil :reader constraints
     :documentation
     "List of constraint instances for this task")
   
   (directly-related-tasks 
     :initform t
     :documentation
     "t means the value has not been calculated yet
      or is out of date and needs to be recalculated.
      readers are defined below which override the defaults.")

   (all-related-tasks
     :initform t
     :documentation
     "t means the value has not been calculated yet
      or is out of date and needs to be recalculated.
      readers are defined below which override the defaults.")
   
   (executed 
     :initform nil :reader executed
     :documentation
     "nil if not executed, else PCF with spike at actual start time.
      Note: this slot should not be written directly but only via 
      the execution-time method.")

   ;; Note: 'execution-time slot remove and a method defined to serve the same
   ;; purpose -- MDJ 16Jun89
   
   (restriction
     :initform *unity-pcf* :reader restriction
     :documentation
     "restriction PCF")

   ;; Note: 'restriction-time slot removed and a method defined
   ;; which serves the same purpose -- MDJ 16Jun89

   (abs-suitability
     :initform t
     :documentation
     "absolute suitability PCF")

   (rel-suitability
     :initform t
     :documentation
     "relative suitability PCF")
   
   (suitability
     :initform t
     :documentation
     "total suitability (from executed, or from combination of 
      restriction, abs- and rel- suitability)")
   
   ;; derived from suitability, user should not setf these
   
   (earliest-start
     :initform t
     :documentation
     "time of earliest task start.
      If t then the value needs to be calculated.")
   
   (latest-start
     :initform t
     :documentation
     "time of latest task start.
      If t then the value needs to be calculated.")

   (non-zero-intervals
     :initform t
     :documentation
     "list of times of non-zero suitability, 
      e.g. ((t1 t2)(t3 t4) ... (tn-1 tn)).
      If t then the value needs to be calculated.")
   
   (check-suitability
     :initform t :reader check-suitability
     :documentation
     "flag: if t, suitability must be recalculated.
      Various :before methods on other slots set this to t;
      recomputing the suitability of a task sets this back to nil")
   
   )

  (:documentation 
   "Class TASK, the parent class of all activities to schedule")
  )


(defun INFORM-SLOT-VALUE-SETF (object slot value)

  "Inform user that a slot is having it's value changed by
    (setf (slot-value ...)), i.e. bypassing accessors.
    Call this BEFORE the setf.
    Message only emitted if *VERY-VERBOSE-CONSTRAINT-PROP* is non-nil.
    This is useful in debugging to see every detail of what is
    happening to object slots."
  
  (if *VERY-VERBOSE-CONSTRAINT-PROP*
      (when (not (equal (slot-value object slot) value))
	(format *VERY-VERBOSE-CONSTRAINT-PROP*
		"~% s-v: ~20a of ~10a, ~a -> ~a"
		slot object (slot-value object slot) value))))


;;; history keeping for task slots
;;; NOTE:  
;;; - any changes to slots must be reflected in mapcar list below
;;; - any defmethod (setf .)'s must incorporate history keeping calls as shown

(defun KEEP-HISTORY-FOR-TASK-SLOT (slot)

  "Function which defines a method for the specified task slot name
    which records history whenever the slot if changed.
    The input slot argument is the slot name symbol."

  (eval `(defmethod (setf ,slot) (new-value (i task))
           ;; verbose mode:  write changes to specified stream
           ;; keep history if value changed: equal used for test
           (when (not (equal (slot-value i ',slot) new-value))
             (if *VERY-VERBOSE-CONSTRAINT-PROP*
               (format *VERY-VERBOSE-CONSTRAINT-PROP*
                       "~% acc: ~20a of ~10a, ~a -> ~a"
                       ',slot i (slot-value i ',slot) new-value))
             (add-to-history (chronika i) i ',slot 
                             (slot-value i ',slot))
             ;; make the slot change
             (setf (slot-value i ',slot) new-value)))))


;;; Define the history-keeping methods for all task slots in the following 
;;; list:

;;; Note:  the following form will create a (defmethod (setf ...)) for each
;;; slot in the list, using the history keeping slot to track changes.

(mapcar #'KEEP-HISTORY-FOR-TASK-SLOT
        '(id chronika minimum-duration maximum-duration properties 
          constraints directly-related-tasks all-related-tasks 
          executed restriction
          abs-suitability rel-suitability suitability 
          earliest-start latest-start non-zero-intervals check-suitability))


(defmethod PRINT-OBJECT ((i task) stream)
  (format stream "<task ~a>" (id i)))



(defun MAKE-TASK (&rest args
                        &key (minimum-duration 0)
                        (maximum-duration nil)
                        (id nil)
                        (chr *current-chronika*)
                        (task-type 'task)
                        &allow-other-keys)

  
  "Create an instance of a task and attach it to the specified chronika.
    Returned value is task instance.  If unspecified, the id is automatically
    generated.  The chronika defaults to *current-chronika*."
  
  ;; error check: chr must be a chonika instance
  (if (not (typep chr 'chronika))
    (error "~a is not an instance of class chronika" chr))
  
  (let* (instance
         (task-id (if id id (gentemp "A-")))
         (existing-task (gethash task-id (task-table chr))))
    ;; error check: id must be unique in this chronika
    (if existing-task
      (error "Task ~a already exists in chronika ~a"
             task-id chr))
    
    ;; create the task instance
    (setf instance (make-instance task-type
                                  :id task-id
                                  :minimum-duration minimum-duration
                                  :maximum-duration maximum-duration
                                  :chronika chr))
    
    ;; put the task in the chronika table, keyed by id
    (setf (gethash task-id (task-table chr)) instance)
    ;; add to the list of all tasks
    (pushnew instance (task-list chr))
    ;; mark chronika as inconsistent
    (setf (consistent chr) nil)
    
    ;; call init-task on the new instance
    (init-task instance args)

    ;; returned value is task instance
    instance))


(defmethod INIT-TASK ((task task) args)

  "Method for initializing a task.  Args is a plist (:key val ...)
    Default does nothing.  Specialize for other task types."
  (declare (ignore args))
  nil)


;;; Diagnostic messages
;;; Note:  use the accessor (diagnostic-messages i) to get list of strings

(defmethod RECORD-DIAGNOSTIC-MESSAGE ((i task)
                                      message-string)

  "Add a diagnostic message to a task's lists of messages"

  (push message-string (diagnostic-messages i)))


(defmethod CLEAR-DIAGNOSTIC-MESSAGES ((i task))

  "Clear the task list of diagnostic messages"

  (setf (diagnostic-messages i) nil))


;;; methods for setting/getting task properties

(defmethod GET-TASK-PROPERTY ((i task) property)

  "return current value for specified property"

  (getf (properties i) property))


(defmethod SET-TASK-PROPERTY ((i task) property value)

  "set task property to specified value"

  (setf (getf (properties i) property) value))


(defmethod SCHEDULABLE-P ((i task))

  "Return non-nil if the task can be scheduled somewhere in the
    chronika time range.  Executed tasks are considered schedulable."

  (non-zero-intervals i))


(defmethod UNSCHEDULABLE-TASKS ((chr chronika))
  
  "return a list of unschedulable tasks in the chronika"

  (let ((result nil))
    (dolist (task (task-list chr))
      (if (not (schedulable-p task))
	  (push task result)))
    result))


(defmethod NON-ZERO-INTERVAL-DURATION ((i task))

  "sum of the total time in a task's nonzero intervals"

  (let ((result 0))
    (dolist (interval (non-zero-intervals i))
      (incf result (1+ (- (second interval) (first interval)))))
    result))


;;; default duration methods (constant duration, fixed at minimum-duration)

(defmethod DURATION-GIVEN-END-AT ((i task) time)

  "Return the duration of a task given that it ends at time"

  ;; default method uses constant minimum duration
  (declare (ignore time))
  (minimum-duration i))


(defmethod DURATION-GIVEN-START-AT ((i task) time)

  "Return the duration of a task given that it starts at time"

  (declare (ignore time))
  (minimum-duration i))


(defmethod START-GIVEN-END-AT ((i task) time)

  "Return the start time of a task given that it ends at time"

  (- time (duration-given-end-at i time)))


(defmethod END-GIVEN-START-AT ((i task) time)

  "Return the end time of a task given that it starts at time"

  (+ time (duration-given-start-at i time)))


;;; End methods

;;; Note:  analogous start methods (earliest-start, latest-start)
;;;  are already defined as slot accessors

(defmethod EARLIEST-END ((i task))

  "Return the earliest end time of a task if it's schedulable, else nil"

  (if (schedulable-p i)
      (end-given-start-at i (earliest-start i))))


(defmethod LATEST-END ((i task))

  "Return the latest end time of a task if it's schedulable, else nil"

  (if (schedulable-p i)
      (end-given-start-at i (latest-start i))))


;;; Non-destructive methods (no changes to task slot values)

(defmethod DEFAULT-PCF ((i task))
  
  "Compute and return default PCF for an task:  start and end 
    within owning chronika.  No slots are changed."
  
  (iPCF-create-with-single-interval
   (start (chronika i))
   (start-given-end-at i (end (chronika i)))
   1 
   0))


(defmethod COMPUTE-SUITABILITY ((i task))
  
  "Calculate and return suitability PCF for task.
    No slots are changed."
  
  (cond 
   
   ;; if executed return the execution time PCF
   ((executed i))
   
   ;; otherwise calculate suitability from components
   (t (ipcf-multiply (ipcf-multiply (restriction i)
                                    (abs-suitability i))
                     (rel-suitability i)))))


(defmethod MAXIMUM-DURATION ((i task))

  "Return maximum duration for a task.  If no value in slot, then
    return minimum-duration"

  (let ((old-value (slot-value i 'maximum-duration)))
    (if old-value
      ;; if there is a value return it
      old-value
      ;; otherwise return the minimum duration
      (minimum-duration i))))


;;; Destructive methods:  these change task slot values


(defmethod UPDATE-SUITABILITY ((i task))
  
  "Update suitability PCF if needed, i.e. if check-suitability flag is t.
    Compute the current suitability function and store it in the suitability
    slot.  Returned value is nil."
  
  ;; when the check-suitability flag is non-nil
  (when (check-suitability i)
    
    ;; mark as no longer needing checking:  do this first, in case something
    ;; during the suitability calculation could cause it to be reset again
    (setf (check-suitability i) nil)

    ;; compute and store the suitability
    ;; (see the setf method for suitability to see the side effects of this)
    (if *VERY-VERBOSE-CONSTRAINT-PROP*
	(format *VERY-VERBOSE-CONSTRAINT-PROP* "~%Updating suitability of ~a" (id i)))

    (setf (suitability i) (compute-suitability i))))


(defmethod OPEN-SUITABILITY ((i task))

  "Method to indicate that the non-zero-intervals for an task may expand.
    Set flags indicating that rel-suitability and suitability of all related
    tasks should be recomputed."

  (dolist (j (all-related-tasks i))
    ;; note that this will open up i as well as all related tasks
    ;; (since i is related to itself)
    (setf (rel-suitability j) t
	  (suitability j) t)))


;;; -------------------------------------------------------------------------
;;; "Virtual" slots:  these appear externally to be slots (they can 
;;; be read and written as slots) but are implemented dynamically below

;;; Note: The 'executed slot holds a PCF which is 1 only at the time of 
;;; execution.  You should not write to a task's 'executed slot directly:
;;; use the execution-time accessor instead:
;;;  (setf (execution-time i) time)
;;; History-keeping is through the 'executed accessor, however.

(defmethod EXECUTION-TIME ((i task))
  
  "Return time of execution of a task if it is executed, else nil"
  
  (let ((execution-PCF (executed i)))
    (cond (execution-PCF
           (first (second (ipcf-to-interval-list execution-PCF))))
          (t nil))))


(defmethod (SETF EXECUTION-TIME) (new-value (i task))
  
  "Marks a task as executed at the specified time
    (regardless of whether its suitability is non-zero at that time).
    You can also change execution time of task with this.
    To mark task as not executed, supply a time of nil.
    Executed tasks have their restriction PCFs changed to the
    default-restriction PCF for the chronika."
  
  ;; Note:  this operates through the 'executed slot in a task, where the
  ;; PCF representing execution is actually stored.  There is no explicit
  ;; execution-time slot:  it is derived on demand from the 'executed slot
  ;; contents.
  
  (let ((old-value (execution-time i)))
    
    (cond ((equal old-value new-value))		       ; no change
          
          ((null new-value)			       ; making un-executed
           (setf (executed i) nil)
           (setf (check-suitability i) t)
           (open-suitability i))
          
          ((null old-value)			       ; making executed from un-executed
           (setf (executed i)
                 (ipcf-create-with-single-interval new-value new-value 1 0))
	   (setf (restriction i) (default-restriction (chronika i)))
           (setf (check-suitability i) t))
          
          (t					       ; changing execution time
           (setf (executed i)
                 (ipcf-create-with-single-interval new-value new-value 1 0))
           (setf (check-suitability i) t)
           (open-suitability i)))))


(defmethod RESTRICTION-TIME ((i task))
  
  "If a task is restricted to a single specific time, this method
    returns that time.  If it is not restricted, or is restricted to
    one or more intervals, then return nil"
  
  (let* ((restriction-PCF (restriction i))
         (restriction-intervals (if restriction-PCF
                                  (ipcf-non-zero-intervals restriction-PCF)))
         (first-restriction-interval (first restriction-intervals)))
    
    (cond
     
     ;; if restriction intervals of the form ((t1 t1)) then return t1
     ((and restriction-intervals
           (= 1 (list-length restriction-intervals))
           (= (first first-restriction-interval)
              (second first-restriction-interval)))
      (first first-restriction-interval))
     
     ;; else return nil
     (t nil))))


(defmethod (SETF RESTRICTION-TIME) (new-value (i task))
  
  "Restrict a task to a specified time.  To unrestrict, supply a 
    a value of nil."
  
  ;; Note:  this method operates through the 'restriction slot.  There is
  ;; no restriction-time slot, but rather the restriction time (if any)
  ;; is computed on demand from the contents of the 'restriction slot.
  
  (cond (new-value
         ;; new-value is the restriction time: construct PCF and set
         ;; the restriction to it
         (setf (restriction i)
               (ipcf-create-with-single-interval new-value new-value 1 0)))
        
        (t ; make unrestricted
         (setf (restriction i) (default-restriction (chronika i))))))


(defgeneric UNDO-RESTRICTION (task &optional optional-arg)
  (:documentation 
    "MicroSpike method - remove restrictions on task
      task = TASK
      optional-arg = ignored

     Segmentation method - Un-restrict a scheduling-cluster and recalculate the implications...
                           This effectively sets the restriction PCF for a cluster to unity.
                           'cluster-or-ID may be a scheduling cluster, or an activity or activity ID
                           (in which case the scheduling cluster it belongs to is used)
                           This method will eventually be deleted.
     task = SEGMENTATION
     optional-arg = cluster-or-id"))

(defmethod UNDO-RESTRICTION ((task task) &optional for-congruence)
  "remove restrictions for task"
  (declare (ignore for-congruence))
  (setf (restriction-time task) nil)
  )
  
(defgeneric UNDO-ALL-RESTRICTIONS-IN-CHRONIKA (chronika)
  (:documentation "Removes all restrictions for all tasks in 
                   the chronika."))

(defmethod UNDO-ALL-RESTRICTIONS-IN-CHRONIKA ((chronika chronika))
  "Removes all restrictions for all tasks in the chronika."

  (dolist (task (task-list chronika))
    (undo-restriction task))
 )

;;; -------------------------------------------------------------------------
;;; methods which modify default accessors

;;; ----------------------------------------------------------------------
;;; NOTE: should define :AROUND methods for ABS-SUITABILITY, REL-SUITABILITY,
;;; SUITABILITY, others? such that if task is executed, no change is made
;;; to the slot.
;;; ----------------------------------------------------------------------

(defmethod (SETF RESTRICTION) :BEFORE (new-value (i task))
  
  "Set the restriction PCF for a task to the specified new-value
    (which must be a PCF).  To unrestrict a task, supply a restriction
    PCF of *unity-PCF* or the default for the chronika."
  
  (let ((old-value (slot-value i 'restriction)))
    
    (cond
     
     ;; no change? do nothing
     ((equal old-value new-value))
     
     (t ;; there is a change in the restriction PCF value
      
      ;; mark suitability as needing to be recomputed
      (setf (check-suitability i) t)
      ;; see whether restriction is shrinking or expanding:
      (if (not (ipcf-non-zero-intervals-shrunk old-value new-value))
        ;; if pcf expanded then open suitability 
        (open-suitability i))))))

;;; -------------------------------------------------------------------------
;;; Note: a number of methods below follow the schema:
;;;
;;;  (defmethod SLOTNAME ((i task))
;;;    (let ((slot-contents (slot-value i 'slotname)))
;;;      (when (eq t slot-contents)
;;;        ;; compute slot contents
;;;        (setf slot-contents (some-horrible-calculation))
;;;        ;; stuff it in slot
;;;        (setf (slot-value i 'slotname) slot-contents))
;;;      ;; return contents
;;;      slot-contents))
;;;
;;; Even though the slot contents change, the old value (t) is
;;; not recorded in the history:  this is OK, because if the history
;;; is ever rolled back the contents will be consistent.  The following 
;;; shows how this works:
;;;
#|
 slot      history stack
 ----      -------------
 (1 2 3)   hist-6                     ;; after constraint prop (hist-6 is a marker)
 t         (1 2 3) hist-6             ;; change to t, history keeps old value
 (4 5 6)   t (1 2 3) hist-6           ;; this history-keeping step is skipped
                                      ;;  by setting slot-value directly
 -- propagate constraints and push new marker --
 (7 8 9)   hist-7 (4 5 6) (1 2 3) hist-6  ;; no t in the history stack
 -- backup history ---
 (1 2 3)   hist-6                     ;; see? didn't lose anything
|#
;;; -------------------------------------------------------------------------


(defmethod DIRECTLY-RELATED-TASKS ((i task))

  "Return a list of tasks directly related to the input task. Tasks are
    always related to themselves."
  
  (let ((slot-contents (slot-value i 'directly-related-tasks)))
    
    (when (eq t slot-contents)
      ;; value needs to be computed
      ;; derive the list of related tasks
      (setf slot-contents (list i))
      (dolist (c (constraints i))
        (setf slot-contents
              (union slot-contents
                     (tasks-affected-via-constraint c i))))
      ;; save it in the slot
      (inform-slot-value-setf i 'directly-related-tasks slot-contents)
      (setf (slot-value i 'directly-related-tasks) slot-contents))

    ;; return the list
    slot-contents))


;;; working-table: used to keep track of which activities are known
;;; related as transitive closure is calculated. Key is task
;;; and value is not used (set to t just cause you have to specify a value)

(let ((working-table (make-hash-table :test #'eq)))
  
  (defmethod ALL-RELATED-TASKS ((i task))

    "Return a list of all tasks related to task, including those
      related through one or more intermediate tasks. A task is always
      related at least to itself."
    
    (let ((slot-contents (slot-value i 'all-related-tasks)))
      
      ;; when the slot contains t then the list must be calculated
      (when (eq t slot-contents)
        (clrhash working-table)
        (let ((initial-list (list i))
              working-list
	      changes)
	  
          (loop
	    
            ;; setup
            (setf working-list nil
		  changes nil)

	    ;; build working-list
            (dolist (j initial-list)
              (setf working-list (append working-list
                                         (directly-related-tasks j))))

	    ;; mark in working-table
            (dolist (j working-list)
	      ;; j is related to at least one task in table
	      ;; if j is not already in working-table...
              (when (not (gethash j working-table))
		;; then put it there and add it to the list to be checked
		;; in the next loop
                (push j initial-list)
                (setf changes t
                      (gethash j working-table) t)))
	    
            ;; terminate loop if all tasks directly related to those the
	    ;; list are in the table
            (if (not changes) (return)))
	  
          ;; build the list to save
          (setf slot-contents nil)
          (maphash #'(lambda (key val)
		       (declare (ignore val))
		       (push key slot-contents))
                   working-table)
	  
	  ;; put it in the slot
	  (inform-slot-value-setf i 'all-related-tasks slot-contents)
          (setf (slot-value i 'all-related-tasks) slot-contents)))
      
      ;; returned value
      slot-contents))
  
  ) ; end let


(defmethod ABS-SUITABILITY ((i task))
  
  "Return the absolute suitability of a task"
  
  (let ((slot-contents (slot-value i 'abs-suitability)))
    
    ;; if the value is t (which is the initform value)
    ;; then compute the abs suitability and put it in the slot.
    ;; You don't have to look at the 'check-suitability slot
    ;; or (check-consistency chr) since absolute constraints
    ;; don't change once calculated (unless e.g. a new constraint
    ;; is attached).
    
    (when (eq t slot-contents)
      
      ;; value needs to be recalculated: start with the default PCF
      (setf slot-contents (default-PCF i))
      
      ;; get the abs constraint PCF from each attached constraint
      (dolist (c (constraints i))
        (let ((pcf (constraint-abs-PCF c i)))
          (when pcf
            ;; and multiply the lot to get the final abs suitability
            (setf slot-contents (ipcf-multiply pcf slot-contents)))))
      
      ;; save it in the slot
      (inform-slot-value-setf i 'abs-suitability slot-contents)
      (setf (slot-value i 'abs-suitability) slot-contents))
    
    ;; returned value
    slot-contents))


(defmethod (SETF ABS-SUITABILITY) :BEFORE (new-value (i task))
  
  "If a task's abs-suitability slot is ever set to t, mark the
    task to have its suitability recalculated"
  
  ;; the abs suitability can be set to t e.g. if a new constraint is 
  ;; attached on the fly (see attach-constraint)
  
  (when (eq t new-value)
    (setf (check-suitability i) t)))


(defmethod REL-SUITABILITY ((i task))
  
  "Return the relative suitability of a task"
  
  ;; Make sure chronika is consistent before getting value to return.
  ;; This is needed because i may depend on j which depends on k:
  ;; if k has changed then we know j needs to be re-calculated, but we
  ;; don't necessarily know that i does.  This makes sure that all 
  ;; tasks on which this depends are up to date unless we are in the middle
  ;; of constraint propagation, in which case this call has no effect.
  ;; In this case the slot may still contain t, and we have to compute
  ;; an estimated rel-suitability.
  
  (check-consistency (chronika i))
  
  (let ((slot-contents (slot-value i 'rel-suitability)))
    
    ;; when t compute value
    (when (eq t slot-contents)
      
      ;;start with unity PCF
      (setf slot-contents *unity-PCF*)
      
      ;; multiply by the PCF from each relative constraint
      (dolist (c (constraints i))
        (let ((pcf (constraint-rel-PCF c i)))
          (if pcf
            (setf slot-contents (ipcf-multiply pcf slot-contents)))))
      
      ;; store value in slot
      (inform-slot-value-setf i 'rel-suitability slot-contents)
      (setf (slot-value i 'rel-suitability) slot-contents))
    
    ;; returned value
    slot-contents))


(defmethod (SETF REL-SUITABILITY) :BEFORE (new-value (i task))

  "If a task's rel-suitability slot is ever set to t, mark the
    task to have its suitability recalculated"

  (when (eq t new-value)
    (setf (check-suitability i) t)))


(defmethod SUITABILITY ((i task))
  
  "Return the suitability function (PCF) for a task"
  
  ;; Unlike most of the other methods of this type, suitability does not
  ;; compute and store the current suitability:  instead it stores
  ;; everything that can be determined excluding rel suitability,
  ;; sets the check-consistency flag, and then propagates constraints.
  ;; The reason for this is as follows:  suppose i constrains j and
  ;; j constrains i:  then
  ;;   (suitability i) depends on
  ;;   (rel-suitability j) depends on 
  ;;   (suitability i), i.e. the calculation is circular!
  ;; So the approach is to put in an estimate for the suitability using 
  ;; all the information know, then propagate constraints until the 
  ;; suitabilities are known.  Finally whatever value ends up in the slot
  ;; will be returned to the caller.
  
  (let ((slot-contents (slot-value i 'suitability))
	(temp-suitability nil))
    
    ;; when t (means slot value not calculated) compute and save initial guess
    (when (eq t slot-contents)
      
      ;; default (initial) suitability excludes rel suitability
      ;; since the call to check-consistency will include it
      ;; note that by using the suitability accessor here, the 
      ;; dependent quantities will also be set (earliest-start, etc.)
      (setf temp-suitability (or (executed i)
				 (ipcf-multiply (restriction i)
						(abs-suitability i))))
      (setf (suitability i) temp-suitability) 
      ;; indicate that this is not the full suitability
      (setf (check-suitability i) t)))
  
  ;; always check consistency before returning value
  (check-consistency (chronika i))
  
  ;; returned value is whatever ended up in the slot
  (slot-value i 'suitability))


(defmethod SET-BOUNDS-AND-INTERVALS ((i task) pcf)

  "Auxiliary method for updating earliest-start, latest-start, and
    non-zero-intervals given a value for the task suitability 'pcf"

  (multiple-value-bind (early late intervals)
      (iPCF-bounds-and-intervals pcf)
    (setf (earliest-start i)     early)
    (setf (latest-start i)       late)
    (setf (non-zero-intervals i) intervals)))
  

(defmethod (SETF SUITABILITY) :BEFORE (new-value (i task))
  
  "Set value of task's suitability. Also properly set values of slots
    depending on suitability.  A value of t means that the suitability
    needs to be re-calculated before next access returns."
  
  (cond
   
   ((eq t new-value)
    ;; new-value is t, i.e. unknown: must check before use
    ;; mark dependent items as unknown
    (setf (check-suitability  i) t
          (earliest-start     i) t
          (latest-start       i) t
          (non-zero-intervals i) t
	  ))
   
   (t ;; new value is a PCF

    (let ((slot-contents (slot-value i 'suitability)))

      ;; check whether value is different
      (cond

	((or (eq slot-contents t)
	     (not (equal slot-contents new-value)))
	 ;; the suitability is changing
	 (if *VERBOSE-CONSTRAINT-PROP*
	     (format *VERBOSE-CONSTRAINT-PROP* " ~a" (id i)))
	 ;; store dependent quantities
	 ;; note that we don't set check-suitability to nil here, since
	 ;; that should be taken care of externally (if the suitability really
	 ;; is consistent)
	 (set-bounds-and-intervals i new-value))

	;; no change in suitability
	(t nil))))))


(defmethod (SETF SUITABILITY) :AFTER (new-value (i task))

  "After method for suitability:  update any graphics if present"
  
  (declare (ignore new-value))
  ;; draw if possible
  ;; (if (fboundp update-task-picture)
  ;;    (update-task-picture i))
  )

;;; the following accessors depend on suitability.  They
;;; explicitly call (check-consistency (chronika i)) first
;;; for the following reason:
;;; suppose i => j => k where "=>"is "constrains"
;;; and suppose the suitability of i changes.  Then j will be 
;;; marked as needing its suitability recomputed, but not k.
;;; Yet k's suitability will likely be changed when j's changes.
;;; Thus constraints must be propagated before k's suitability 
;;; (and dependent quantities) can be obtained.
;;;
;;; Note that once constraints are fully propagated, the following
;;; slots must have explicit values (i.e. t is not a possible value).
;;; However, in the midst of constraint propagation, t may be found
;;; in the slot, in which case a temporary value is returned.
;;;

(defmethod EARLIEST-START ((i task))
  
  "Return the earliest start time for a task."
  
  (check-consistency (chronika i))

  (let ((slot-contents (slot-value i 'earliest-start)))
    
    (cond ((eq t slot-contents)
           ;; if slot contains t, compute a value to return
	   (set-bounds-and-intervals i (suitability i))
	   (slot-value i 'earliest-start))

          ;; return slot contents
          (t slot-contents))))


(defmethod LATEST-START ((i task))
  
  "Return the latest start time for a task."
  
  (check-consistency (chronika i))

  (let ((slot-contents (slot-value i 'latest-start)))
    
    (cond ((eq t slot-contents)
           ;; if slot contains t, compute a value to return
	   (set-bounds-and-intervals i (suitability i))
	   (slot-value i 'latest-start))

          ;; return slot contents
          (t slot-contents))))


(defmethod NON-ZERO-INTERVALS ((i task))
  
  "Return the list of non-zero intervals for a task 
    in the form ((t1 t2)(t3 t4)...)"

  (check-consistency (chronika i))
  
  (let ((slot-contents (slot-value i 'non-zero-intervals)))
    
    (cond ((eq t slot-contents)
           ;; if slot contains t, compute a value to return
           ;; but don't store in slot
	   (set-bounds-and-intervals i (suitability i))
	   (slot-value i 'non-zero-intervals))
          
          ;; return slot contents
          (t slot-contents))))


(defmethod (SETF NON-ZERO-INTERVALS) :BEFORE (new-value (i task))
  
  "Method to set the non-zero-intervals of a task"
  
  ;; If i constrains j, then (rel-suitability j) depends on (suitability i)
  ;; only through the non-zero-intervals of i, i.e. where Si(t) /= 0.
  ;; Thus if the non-zero-intervals of i ever change, all tasks j constrained
  ;; by i need to have their rel-suitability re-calculated.
  
  (let ((old-value (slot-value i 'non-zero-intervals)))
    
    ;; storing a new intervals list
    (when (not (equal new-value old-value))
      
      (if *VERBOSE-CONSTRAINT-PROP*
	  (format *VERBOSE-CONSTRAINT-PROP* "*"))
      ;;(format t "<~a>" (id i))

      ;; non-zero-intervals value is changing
      
      ;; inform all tasks constrained by this one that they need to 
      ;; update their relative suitability
      (dolist (j (directly-related-tasks i))
	(unless (eq j i) (setf (rel-suitability j) t)))
      
      ;; inform constraints that this task has changed
      (dolist (c (constraints i))
	(task-has-changed c i)))))



(defmethod (SETF CHECK-SUITABILITY) :BEFORE (new-value (i task))
  
  "When a task's check-suitability flag changes from nil to t, 
    mark its chronika as no longer being consistent"
  
  (let ((old-value (slot-value i 'check-suitability)))

    (when (and (eq t new-value)
               (null old-value))
      (setf (consistent (chronika i)) nil))))




;;; miscellaneous functions

(defmethod RESTRICT-TO-EARLIEST-START ((i task))
  "Restrict task to its earliest start, returning the restriction-time
    or nil if task is not schedulable"
  (if (schedulable-p i)
      (let ((e (earliest-start i)))
	(setf (restriction-time i) e))))


(defmethod RESTRICT-TO-LATEST-START ((i task))
  "Restrict task to its latest start, returning the restriction-time
    or nil if task is not schedulable"
  (if (schedulable-p i)
      (let ((e (latest-start i)))
	(setf (restriction-time i) e))))


(defmethod RESTRICT-TO-MAX-SUITABILITY ((i task))
  
  "Restrict task to intervals where suitability functions has its 
    maximum value (this may be a point, single interval, or set of disjoint
    intervals).  Returned value is the restriction PCF, or nil if the task
    not schedulable."
  
  (if (schedulable-p i)
    (let* ((max-suit (ipcf-max (suitability i)))
           (restr (ipcf-transform (suitability i)
                                  ;; the lambda below converts values equal 
                                  ;; to max to 1, otherwise to zero
                                  #'(lambda (x) (if (= x max-suit) 1 0)))))
      (setf (restriction i) restr))))


;;; ==================================================================
;;;                                     TASK-CONSTRAINT

(defclass TASK-CONSTRAINT () 
  (
   (source
     :initform nil :initarg :source :accessor source
     :documentation
     "Description of the origin of this constraint")
   
   (tasks
     :initform nil :accessor tasks
     :documentation
     "list of tasks participating directly in this constraint")
   
   (closed
     :initform nil :accessor closed
     :documentation
     "set to t when the constraint is closed")
   (constraint-log
     :initform nil :initarg :constraint-log :accessor constraint-log
     :documentation
     "A list recording calls to micro-spike on the constraint.  Each element of the list is a itself a list and records 
     calls to micro spike which result from the same special requirement. The head of each nested list is an instance key
     which is unique for each instance of a special requirement.  The tail of each list is a list of related calls")
   (loggingp
     :initform nil :initarg :loggingp :accessor loggingp
     :documentation
     "Varaible which controls whether or not constraints are logged")

   )
  
  (:documentation "Parent class for all task constraints"))

;;; what "closed" means depends on the type of constraint:
;;; in some cases it may mean that further changes are not
;;; allowed.  Closing a constraint attaches it to the 
;;; activities it constrains.


;;; General task-constraint methods

(defmethod CONSTRAINT-ABS-PCF ((c task-constraint) (i task))
  
  "Return absolute suitability PCF for task a derived from this constraint,
    or nil if task is not involved in this constraint or if constraint
    has no effect on this task."
  
  nil)


(defmethod CONSTRAINT-REL-PCF ((c task-constraint) (i task))
  
  "Return relative suitability PCF for task a derived from this constraint,
    or nil if there is none"
  
  nil)


(defmethod TASK-HAS-CHANGED ((c task-constraint) (i task))
  
  "This will be called whenever the non-zero-intervals for an task
    changes, or after a history backup.  If a constraint maintains internal
    state info this is how it knows to update it."
  
  nil)


(defmethod TASKS-AFFECTED-VIA-CONSTRAINT ((c task-constraint) (i task))

  "Return list of tasks whose suitabilities depend on task i via this
    constraint, or nil if none"

  nil)


(defmethod TASKS-AFFECTING-VIA-CONSTRAINT ((c task-constraint) (i task))

  "Return list of tasks which can affect the suitability of 
    task i, via this constraint, or nil if none"

  nil)


(defmethod TASKS-RELATED-BY-CONSTRAINT ((c task-constraint))

  "Return list of tasks which can influence each other, based on
    this constraint"

  nil)


(defmethod ACTIVATE-CONSTRAINT ((c task-constraint))

  "Close constraint and attach to tasks. Returns constraint instance."
  
  ;; close it
  (setf (closed c) t)
  
  ;; attach it to each task
  (dolist (i (tasks c))
    (attach-constraint i c))
  
  ;; returned value is constraint instance
  c)


(defmethod DE-ACTIVATE-CONSTRAINT ((c task-constraint))

  "Detach constraint from tasks. Returns constraint instance."

  ;; detach from each task
  (dolist (i (tasks c))
    (detach-constraint i c))

  ;; returned value is constraint instance
  c)


(defmethod PRINT-OBJECT ((c task-constraint) stream)
  ;;(call-next-method)
  (format stream "<~a>" (source c)))


(defmethod CONSTRAINT-DETAILS ((c task-constraint) &optional (stream t))

  "Print details of a constraint to the specified stream.  Generally
    for debugging only"

  (format stream "~%Constraint ~a, source: ~a, closed: ~a"
          c (source c) (closed c)))


(defmethod ATTACH-CONSTRAINT ((i task) (c task-constraint))
  
  "Attach a constraint to a task"
  
  ;; add to task constraints list
  ;; pushnew means it can be added more than once without ill affect
  (pushnew c (constraints i))
  
  ;; set flags indicating suitability etc. need to be recomputed
  (setf (abs-suitability         i) t
        (rel-suitability         i) t
        (directly-related-tasks  i) t
        (all-related-tasks       i) t)
  
  ;; returned value is task instance
  i)



(defmethod LOG-CONSTRAINT ((c task-constraint) instance item)
"adds an item to the log list for the constraint if logging is turned on and the instance is non nil"
  (cond ((and (loggingp c) instance)
	 (let* ((log (constraint-log c))
		(related-constraints (assoc instance log :test #'equalp)))
	   (cond (related-constraints
		  (pushnew item (cdr related-constraints)))
		 (t
		  (pushnew (list instance item) (constraint-log c))))))))

(defmethod TURN-ON-CONSTRAINT-LOGGING ((c task-constraint))
"Sets the loggingp field to t for task constraints"
  (setf (loggingp c) t))

;;; ?? CHECK THIS for order of action

(defmethod DETACH-CONSTRAINT ((i task) (c task-constraint))
  
  "Detach a constraint from a task.  Returned value is task instance."
  
  ;; note: use a non-destructive remove, or else history will be wrong!
  
  ;; remove from constraints list
  (setf (constraints i) (remove c (constraints i)))
  
  ;; open the suitability
  (open-suitability i)
  
  ;; set flags indicating that suitability etc. needs to be recomputed
  (setf (abs-suitability         i) t
        (rel-suitability         i) t
        (directly-related-tasks  i) t
        (all-related-tasks       i) t)

  ;; return task instance
  i)

(defgeneric EFFECT-OF-TASK-ON-TASK-GIVEN-INTERVAL (c
						   affecting-task
						   affected-task
						   affecting-task-early-start
						   affecting-task-late-start)
  (:documentation "Given that affecting-task has specified early and late starts,
                   return suitability of affected-task based on current-constraint. 
                   Return nil if affecting-task does not affect affected-task.
                   C = constraint (same-orient-task-constraint, orient-from-task-constraint,
                                   binary-rel-constraint, task-constraint)
                   Affecting-task = task or subclasses
                   Affected-task  = task or subclasses
                   Affecting-task-early-start = ticks
                   Affecting-task-late-start  = ticks."))

(defmethod EFFECT-OF-TASK-ON-TASK-GIVEN-INTERVAL ((c task-constraint)
                                                  (i task) (j task)
                                                  i-early-start i-late-start)

  "given that task i has specified early and late starts, return suitability
    of task j based on current constraint. Return nil if task i does not 
    affect j."

  (declare (ignore i-early-start i-late-start))
  nil)

(defgeneric LIMIT-BASED-ON-BOUNDARIES (constraint
                                       i
                                       start
                                       end)
  (:documentation
  "Return the relative suitability of i due to all tasks j s.t. j => i,
    given that tasks i and all j are restricted to start and end within specified
    limits.  If any j are executed or restricted, then the start/end boundaries
    are ignored and the execution time or restriction intervals are used
    instead.  If i or any j have early or late starts which are inside the
    start/end limits, these more restrictive times are used instead of the
    boundary.
    Three values are returned:
     pcf:  rel suitability of i given all of j restricted to within start/end,
      or that j limited by execution or restriction.
      nil if there are no j which affect i.
     unsched-flag: t if i is unschedulable
     unsched-components: list of all j s.t. j => i and j alone causes
       i to be unschedulable.
    Note that i may be unsched even if no single constraining task makes
    it so."))

(defmethod LIMIT-BASED-ON-BOUNDARIES ((c task-constraint) (i task)
                                      start end)
  
  (values
    (iPCF-create-with-single-interval
      (max start (earliest-start i))
      (min (start-given-end-at i end) (latest-start i))
      1 0)
    nil
    nil))


;;; ==================================================================
;;;                                     ABS-TASK-CONSTRAINT


(defclass ABS-TASK-CONSTRAINT (task-constraint)
  (
   (task-window-table
     :initform (make-hash-table) :accessor task-window-table
     :documentation
     "Table of absolute time constraints for a set of tasks.  
      key: task instance, value PCF")
   )
  
  (:documentation "Simple absolute time suitabilities on one or more tasks")
  )


(defun MAKE-ABS-TASK-CONSTRAINT (source)

  "Creates and returns an instance of an abs-task-constraint."

  (make-instance 'abs-task-constraint :source source))


(defmethod ADD-ABS-TASK-WINDOW ((c abs-task-constraint)
                                (i task) pcf)
  
  "Given task and its absolute PCF, adds it to the specified constraint
    (combining it with previous PCF for that task and constraint, if any).
    If this makes task have a zero PCF, record as diagnostic message for task.
    Returns resulting (combined) PCF."

  ;; if constraint is closed, tell task to update its abs suitability
  ;; This relies on the fact that constraint changes here only become more
  ;; restrictive

  ;; add to task list if not already there
  (pushnew i (tasks c))
  
  (let ((old-pcf (gethash i (task-window-table c)))
        (new-pcf nil))
    
    (cond 

      ;; if there is an old pcf multiply the old and new to get the
      ;; new constraint
      (old-pcf
       (multiple-value-bind (prod prod-changed)
	   (ipcf-multiply pcf old-pcf)
	 (if prod-changed
	   (setf new-pcf prod))))

      (t (setf new-pcf pcf)))

    ;; now new-pcf is a pcf only if the constraint changes
    
    (when new-pcf

      ;; store the new pcf in the table
      (setf (gethash i (task-window-table c)) new-pcf)

      ;; if constraint is closed, inform the task to check its
      ;; abs suitability
      (if (closed c)
	  (setf (abs-suitability i) t))

      ;; if the PCF became *zero-PCF* then record diagnostic
      (when (and (equal *zero-PCF* new-PCF)
		 (not (equal *zero-PCF* old-PCF)))
	(record-diagnostic-message
	  i (format nil "ABS window: ~a is unschedulable" i)))
      
      )    
    
    ;; returned value is combined PCF or nil if constraint does not change
    new-pcf))


(defmethod CONSTRAINT-ABS-PCF ((c abs-task-constraint) (i task))

  "Return the abs-suitability PCF for the specified task, or nil if
    these is none."

  (gethash i (task-window-table c)))


(defmethod COPY-AND-FILTER-ABS-CONSTRAINT
	   ((c abs-task-constraint)
	    single-task-filter-fcn
	    &optional
	    task-mapping-function)
  
  "Make a copy of a constraint including only a specified subset of
    tasks.  single-task-filter-fcn when applied to a task returns non-nil if
    the task should be in the copy, nil otherwise.  Optionally the constraint
    can apply to a different set of tasks:  if task-mapping-function is supplied
    it must take a task i as argument and return another i-prime:  the
    copied constraint will refer to the mapped tasks only."

  (let ((c-copy (make-instance (Kludgy-Type-of-Workaround c)
			       :source (source c))))
    (dolist (i (tasks c))
      (when (funcall single-task-filter-fcn i)
	(add-abs-task-window
	  c-copy
	  (if task-mapping-function
	      ;; if there is a mapper apply it
	      (funcall task-mapping-function i)
	      ;; otherwise copied constraint refers to original task
	      i)
	  (constraint-abs-pcf c i))))
    ;; return the copy
    c-copy))


;;; ==================================================================
;;;                                     BINARY-REL-CONSTRAINT

#| ?? TO DO:

Add:  measure of how much a task is constrained by others
Ability to stop processing of adding constraint when it makes some task
  unschedulable
|#


(defclass BINARY-REL-CONSTRAINT (task-constraint)
  (
   (b-table
    :initform (make-hash-table :test #'eq) :accessor b-table
    :documentation
    "B functions stored as PCFs.
       Key is task, value is a hash table with task as key, B fcn as value,
       e.g. if i constrains j,  then
        (gethash i (gethash j (b-table c)))
       returns PCF representing Bj(t;ti)")
   
   (b-intervals-table
    :initform (make-hash-table :test #'eq) :accessor b-intervals-table
    :documentation
    "B fns stored as intervals excluding those where
       PCF is zero. Key is task, value is a hash table with task as key,
       intervals list as value.
       e.g. if i constrains j, 
        (gethash i (gethash j (b-intervals-table c)))
       returns intervals")
   
   (b-nonzero-intervals-table
    :initform (make-hash-table :test #'eq) :accessor b-nonzero-intervals-table
    :documentation
    "B fns stored as nonzero intervals
       Key is task, value is a hash table with task as key, intervals as value,
       e.g. if i constrains j, 
        (gethash i (gethash j (b-nonzero-intervals-table c)))
       returns nonzero intervals")
   
   (constrained-by-table
    :initform (make-hash-table :test #'eq) :accessor constrained-by-table
    :documentation
    "key is task, value list of tasks which key task is constrained by.
     e.g if i => j, then i constrains j, j is constrained by i, and
     (gethash j (constrained-by-table c)) is a list containing i")
   
   (constrains-table
    :initform (make-hash-table :test #'eq) :accessor constrains-table
    :documentation
    "key is task, value list of tasks that key task constrains.
     e.g. if i => j, then i constrains j, j is constrained by i, and
     (gethash i (constrains-table c)) is a list containing j")

   (path-consistent
     :initform nil :accessor path-consistent
     :documentation
     "flag, t when path consistent, nil otherwise")

   (constraint-history
     :initform nil :accessor constraint-history
     :documentation
     "list of changes to constraint, for backing out constraints
      if needed")
   )
  
  (:documentation
   "Binary relative constraint on a set of tasks.
    time vs. space: time wins
    B-function PCFs stored in three forms here:  
    as PCFs, intervals ((t1 t2 val)..) and non-zero intervals ((t1 tn)...).
    Only intervals are needed to compute relative suitability.
    The others are needed during constraint definition and path-consistency.
    Note for the 3 b-table slots, the value in the hash table is itself
    a hash table, i.e. a 2D array of constraints")
  
  )


(defun MAKE-BINARY-REL-CONSTRAINT (source)

  "Create and return an instance of a binary-rel-constraint"

  (make-instance 'binary-rel-constraint :source source))


(defun SUBSTITUTE-FOR-INFINITY-IN-LIST (list)

  "To make PCFs and interval lists more readable, this does a non-destructive
    substitution of '+inf for *plus-infinity* and '-inf for *minus-infinity*
    in the input list or nested list"

  (subst '+inf *plus-infinity*
         (subst '-inf *minus-infinity* list)))


(defmethod CONSTRAINT-DETAILS ((c binary-rel-constraint) &optional (stream t))

  "Formats to stream interesting information about specified constraint"

  (format stream "~%tasks in constraint ~a ~a: ~a"
    c (source c) (mapcar #'id (tasks c)))

  (format stream "~% path consistent: ~a" (path-consistent c))
  
  (maphash #'(lambda (key val)
               (format stream "~%~a constrains: ~a" (id key) (mapcar #'id val)))
           (constrains-table c))

  (maphash #'(lambda (key val)
               (format stream "~%~a constrained by: ~a" (id key) (mapcar #'id val)))
           (constrained-by-table c))

  (maphash #'(lambda (key val)
               (format stream "~%~a constrained by:" (id key))
               (maphash #'(lambda (key2 val2)
                            (format stream "~%  ~a: ~a, ~a" (id key2) 
                                    (substitute-for-infinity-in-list val2)
                                    (substitute-for-infinity-in-list 
                                     (current-constraint c key2 key)))
			    
                            )
                        val)
               )
           (b-intervals-table c))
  
  )


(defmethod DELETE-TASK-FROM-TABLES ((c binary-rel-constraint)
				    (i task))

  "Delete a task from the tables of the specified constraint,
    however, task remains in constraints tasks slot. Return value
    not useful."

  ;; Used by collapse-arc for a collapsible-binary-rel-constraint

  (let ((i-constrains     (gethash i (constrains-table c)))
        (i-constrained-by (gethash i (constrained-by-table c)))
        )
    
    ;; don't remove from the tasks list
    ;;(setf (tasks c) (remove i (tasks c)))
    
    (remhash i (b-table c))
    (remhash i (b-intervals-table c))
    (remhash i (b-nonzero-intervals-table c))
    (remhash i (constrains-table c))
    (remhash i (constrained-by-table c))
    
    ;; for all j such that i => j, remove i from the table of activities
    ;; that constrain j
    (dolist (j i-constrains)
      (setf (gethash j (constrained-by-table c))
            (remove i (gethash j (constrained-by-table c)))))
    
    ;; for all j such that j => i, remove i from the table of activities
    ;; that j constrains; also remove the B-function Bi(t;tj) and the
    ;; intervals and nonzero-intervals that correspond to it
    (dolist (j i-constrained-by)
      (setf (gethash j (constrains-table c))
            (remove i (gethash j (constrains-table c))))
      (let ((table (gethash j (b-table c)))
            (intervals-table (gethash j (b-intervals-table c)))
            (nonzero-intervals-table (gethash j (b-nonzero-intervals-table c))))
        (remhash i table)
        (remhash i intervals-table)
        (remhash i nonzero-intervals-table)))))


(defmethod EFFECT-OF-TASK-ON-TASK-GIVEN-INTERVAL ((c binary-rel-constraint)
                                                  (i task) (j task)
                                                  i-early-start i-late-start)

  (let ((cci (current-constraint-intervals c i j)))
    (cond (cci
           (binary-rel-component
            (list (list i-early-start i-late-start))
            cci))
          (t nil))))


(defmethod LIMIT-BASED-ON-BOUNDARIES ((c binary-rel-constraint) (i task)
                                      start end)
  
    
  (let* ((source-tasks (tasks-affecting-via-constraint c i))
         (pcf (iPCF-create-with-single-interval
               (max start (earliest-start i))
               (min (start-given-end-at i end) (latest-start i))
               1 0))
         (unsched-components nil)
         (unsched-flag nil)
         pcf-component)
    
    (cond 
     
     ;; no constraining tasks, so return pcf
     ((null source-tasks))
     
     ;; otherwise get contribution from each constraining task
     (t (dolist (j source-tasks)
          (setf pcf-component 
                (binary-rel-component
                 (or (ipcf-non-zero-intervals (executed j))
                     (ipcf-non-zero-intervals
                      (if (not (equal (restriction j) *unity-pcf*))
                        (restriction j)))
                     (list (list (max start
                                      (earliest-start j))
                                 (min (start-given-end-at j end)
                                      (latest-start j)))))
                 (current-constraint-intervals c j i)))
          ;;(format t "~%~a ~a" j pcf-component)
          (setf pcf (ipcf-multiply pcf-component pcf))
          
          ;; catch unschedulables, note, and exit
          
          (when (equal pcf-component *zero-PCF*)
            ;;(format t "~%in ~a: component ~a is unschedulable" 
            ;;         (id i) (id j))
            ;; note unsched due to this task
            (push j unsched-components)
            )
          
          (when (equal pcf *zero-PCF*)
            ;;(format t "~%rel suitability of ~a is *zero-pcf*"
            ;;        (id i))
            ;; note unsched
            (setf unsched-flag t)
            )
          
          ) ;; end loop over source-tasks
        ))
    ;; returned values
    (values pcf unsched-flag unsched-components)))


(defmethod CURRENT-CONSTRAINT ((c binary-rel-constraint) 
                               (i task) (j task))

  "Return current constraint PCF 'i constrains j' if present, nil otherwise,
    i.e. Bj(t;ti)"
  
  (let* ((b-table (b-table c))
         (source-table (gethash j b-table))
         (source (if source-table (gethash i source-table))))
    source))


(defmethod CURRENT-CONSTRAINT-INTERVALS ((c binary-rel-constraint) 
                                         (i task) (j task))

  "Return current constraint 'i constrains j' if present, nil otherwise,
    i.e. interval representation of Bj(t;ti)"

  (let* ((b-intervals-table (b-intervals-table c))
         (source-table (gethash j b-intervals-table))
         (source (if source-table (gethash i source-table))))
    source))


(defmethod CURRENT-CONSTRAINT-NONZERO-INTERVALS ((c binary-rel-constraint) 
                                                 (i task) (j task))

  "Return current constraint 'i constrains j' if present, nil otherwise,
    i.e. nonzero interval representation of Bj(t;ti)"

  (let* ((b-nonzero-intervals-table (b-nonzero-intervals-table c))
         (source-table (gethash j b-nonzero-intervals-table))
         (source (if source-table (gethash i source-table))))
    source))


(defmethod INDUCED-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task) (k task))
  
  "Return induced constraint PCF i on k via j, if any.  I.e. 
    if i => j => k, return constraint i => k.
    Return  nil if no constraint or if constraint B PCF is unity-valued 
    everywhere."
  
  (let* ((ij-nonzero-intervals (current-constraint-nonzero-intervals c i j))
         (jk-intervals (current-constraint-intervals c j k))
         (induced-ik nil))
    
    (setf induced-ik
          (binary-rel-component ij-nonzero-intervals
                                jk-intervals))
    
    ;; induced constraints are 0/1-valued so that repeated
    ;; multiplication does not cause problems.
    ;; ?? is this essential?
    (when induced-ik
      (setf induced-ik (ipcf-transform induced-ik #'make-number-unity)))
    
    ;; returned value
    (if (and induced-ik
             (not (equal induced-ik *unity-PCF*)))
      ;; if there is an induced constraint then return it
      induced-ik
      ;; else return nil
      nil)))


;;; In the following:  a returned arc e.g. i => j is returned as (cons i j)
;;; A list of arcs is a list of conses of this type

(defmethod ADD-FORWARD-INDUCED-ARCS ((c binary-rel-constraint)
                                     (i task) (j task))
  
  "Add arcs i => k induced by j, i.e. via i => j => k for all k.
    Return list of arcs changed."
  
  (let ((changes nil))
    
    ;; check forward from j
    (dolist (k (gethash j (constrains-table c)))
      ;; i => j => k
      (when (not (eq i k))
	(let ((induced-ik (induced-constraint c i j k)))
	  (when induced-ik
	    (multiple-value-bind (changed unsched)
		(define-binary-constraint c i k induced-ik)
	      (declare (ignore unsched))
	      (when changed 
		(Record-Constraint-For-Trans i k j "PROPAGATION")
		(push (cons i k) changes))
	      
	      ;; debug output
	      (when changed
		(if *VERY-VERBOSE-CONSTRAINT-PROP*
                    (format *VERY-VERBOSE-CONSTRAINT-PROP*
			    "~%~a => ~a via ~a ~a"
			    (id i) (id k) (id j) induced-ik))
		(if *VERBOSE-CONSTRAINT-PROP*
		    (format *VERBOSE-CONSTRAINT-PROP* "+"))
		)
	      
	      )))))
    
    changes))


(defmethod ADD-BACKWARD-INDUCED-ARCS ((c binary-rel-constraint)
                                      (i task) (j task))
  
  "Add arcs k => j induced by i, i.e. via k => i => j.
    Return list of arcs changed"
  
  (let ((changes nil))
    
    ;; check backwards from i
    (dolist (k (gethash i (constrained-by-table c)))
      ;; k => i => j
      (when (not (eq k j))
	(let ((induced-kj (induced-constraint c k i j)))
	  (when induced-kj
	    (multiple-value-bind (changed unsched)
		(define-binary-constraint c k j induced-kj)
	      (declare (ignore unsched))
	      (when changed 
		(Record-Constraint-For-Trans k j i "PROPAGATION")
		(push (cons k j) changes))
	      
	      ;; debug output
	      (when changed
		(if *VERY-VERBOSE-CONSTRAINT-PROP*
                    (format *VERY-VERBOSE-CONSTRAINT-PROP*
			    "~%~a => ~a via ~a ~a"
			    (id k) (id j) (id i) induced-kj))
		(if *VERBOSE-CONSTRAINT-PROP*
		    (format *VERBOSE-CONSTRAINT-PROP* "-"))
		)
	      
	      )))))
    
    changes))


(defmethod ADD-INDUCED-ARCS-TO-CONSTRAINT ((c binary-rel-constraint)
                                           (i task) (j task))
  
  "Add forwards and backwards induced arcs to a constraint i => j, i.e. 
    induced arcs p => j and i=> q via p => i => j => q, all p and q.
    Return list of arcs changed."
  
  (if (eq i j)
    (error "induced arcs requested for same task ~a" (id i)))
  
  (append (add-forward-induced-arcs  c i j)
          (add-backward-induced-arcs c i j)))


(defmethod MAKE-PATH-CONSISTENT ((c binary-rel-constraint))
  
  "Add all possible induced arcs to a constraint"
    
  (let ((to-check nil))
    
    ;; build list of all known arcs
    (dolist (i (tasks c))
      (dolist (j (gethash i (constrains-table c)))
        (push (cons i j) to-check)))
    
    ;; check all arcs on the list and propagate if there are
    ;; any induced arcs discovered
    (if to-check
	(check-arcs-and-propagate c to-check))))
  

(defmethod PCF-I-BEFORE-J ((c binary-rel-constraint) (i task) (j task) duration-i)
  "Return the pcf representing starting task i before task j."
  (let* ((ij-pcf (or (current-constraint c i j)
		     *unity-pcf*))
	 (j-start-pcf (pcf-create-with-single-interval *minus-infinity* duration-i 0 1))
	 (combined-pcf (ipcf-multiply ij-pcf j-start-pcf)))
    combined-pcf))

(defmethod EARLIEST-START-REL
    ((c binary-rel-constraint) (i task) (j task) &key (default *plus-infinity*))
  "Return the earliest start time of j, as constrained by i."
  (or (pcf-bounds :earliest-start (pcf-i-before-j c i j (minimum-duration i)) j)
      default))

(defmethod LATEST-START-REL
    ((c binary-rel-constraint) (i task) (j task) &key (default *plus-infinity*))
  "Return the latest start time of j, as constrained by i."
  (or (pcf-bounds :latest-start (pcf-i-before-j c i j (maximum-duration i)) j)
      default))

(defmethod EARLIEST-END-REL
    ((c binary-rel-constraint) (i task) (j task) &key (default *minus-infinity*))
  "Return the earliest start time of j, as constrained by i."
  (or (pcf-bounds :earliest-end (pcf-i-before-j c i j (minimum-duration i)) j)
      default))

(defmethod LATEST-END-REL
    ((c binary-rel-constraint) (i task) (j task) &key (default *minus-infinity*))
  "Return the latest end time of j, as constrained by i."
  (or (pcf-bounds :latest-end (pcf-i-before-j c i j (maximum-duration i)) j)
      default))


(defgeneric PCF-BOUNDS (when-keyword pcf task)
	    (:documentation
"Return one of EARLIEST-START, LATEST-START, EARLIEST-END, OR LATEST-END
for PCF and TASK."))
	    
(defmethod PCF-BOUNDS ((when (eql :EARLIEST-START)) pcf (task task))
  (multiple-value-bind (early-start late-start nonzero-intervals)
      (ipcf-bounds-and-intervals pcf)
    (declare (ignore nonzero-intervals late-start))
    early-start))

(defmethod PCF-BOUNDS  ((when (eql :LATEST-START)) pcf (task task))
  (multiple-value-bind (early-start late-start nonzero-intervals)
      (ipcf-bounds-and-intervals pcf)
    (declare (ignore nonzero-intervals early-start))
    late-start))

(defmethod PCF-BOUNDS ((when (eql :EARLIEST-END)) pcf (task task))
  (multiple-value-bind (early-start late-start nonzero-intervals)
      (ipcf-bounds-and-intervals pcf)
    (declare (ignore nonzero-intervals late-start))
    (if early-start
	(+ early-start (minimum-duration task))
      nil)))

(defmethod PCF-BOUNDS ((when (eql :LATEST-END)) pcf (task task))
  (multiple-value-bind (early-start late-start nonzero-intervals)
      (ipcf-bounds-and-intervals pcf)
    (declare (ignore nonzero-intervals early-start))
    (if late-start
	(+ late-start (maximum-duration task))
      nil)))


(defmethod NONZERO-INTERVALS ((c binary-rel-constraint) (i task) (j task))
  (let* ((ij-pcf (current-constraint c i j)))
    (multiple-value-bind  (early-start late-start nonzero-intervals)
	(ipcf-bounds-and-intervals ij-pcf)
      (declare (ignore early-start late-start))
      nonzero-intervals)))


;;; Controlling arc propagation (in path consistency)

(let
  (
   ;; time in seconds after which to give up on arc propagation
   ;; if nil then never give up
   (arc-propagation-time-limit nil) ;; in seconds
   
   ;; this var is set to t when check-arcs-and propagate times
   ;; out.  It can be reset to nil with the function
   ;;  reset-arc-propagation-timeout
   ;; it can be tested with the function 
   ;;  arc-propagation-timed-out-p
   (arc-propagation-timed-out nil)
   )
  
  (defun SET-ARC-PROPAGATION-TIME-LIMIT (value)
    "Set propagation time limit:  value is in seconds of real
       time, or nil to indicate no limit exists.  If value is
       0, then propagation is marked as already timed out."
    (cond ((or (and (numberp value)
		    (>= value 0))
	       (null value))
	   (setf arc-propagation-time-limit value)
	   (setf arc-propagation-timed-out
		 (if (and (numberp value) (= 0 value)) t nil)))
	  
	  (t
	   (error "value must be >0 or nil"))))

  (defun ARC-PROPAGATION-TIME-LIMIT ()
    "Return current time limit, in seconds, or nil if none"
    arc-propagation-time-limit)
  
  (defun RESET-ARC-PROPAGATION-TIMEOUT ()
    "Reset timeout marker to indicate NOT timed out"
    (setf   arc-propagation-timed-out nil))
  
  
  (defun ARC-PROPAGATION-TIMED-OUT-P ()
    "Return t if timed out, nil otherwise"
    arc-propagation-timed-out)
  
  
  (defun SET-ARC-PROPAGATION-TIMEOUT ()
    "Set timeout marker to indicate timed out"
    (setf   arc-propagation-timed-out t))
  
  )						;end lexical closure


(defmethod FILTER-ARCS-TO-CHECK ((c binary-rel-constraint)
				 arc-list)
  
  "filter the input list of arcs ((cons i j)...) where i and j are tasks
    and return a list of equivalent arcs.  This method only removes
    duplicates."
  
  ;; not worth the time to find duplicates for large problems
  (if *VERBOSE-CONSTRAINT-PROP*
      (format *VERBOSE-CONSTRAINT-PROP* "[arcs:~d]" (list-length arc-list)))
  
  ;;(remove-duplicates arc-list
  ;;		     :test #'(lambda (x y)
  ;;			       (and (eq (first x) (first y))
  ;;				    (eq (rest  x) (rest  y)))))

  ;; just return input arc list
  arc-list
  )


(defmethod CHECK-ARCS-AND-PROPAGATE ((c binary-rel-constraint)
				     to-check)
  
  "Check a list of arcs to see whether any additional arcs can be
    deduced (or any changes to existing arcs can be found).  If
    so, iteratively propagate their effects until there are no changes."
  
  ;; to check is a list of arcs:  ((cons i j)...)
  ;; no checking if timed out
  (if (not (arc-propagation-timed-out-p))
      (let (changes
	    loop-start
	    timed-out-in-arc-loop)

	(setf loop-start (get-internal-real-time))
	(loop
	  (setf changes nil)
	  (setf to-check (filter-arcs-to-check c to-check))
	  (if *VERBOSE-CONSTRAINT-PROP*
	      (format *VERBOSE-CONSTRAINT-PROP* "[~a]" (list-length to-check)))
	  
	  (setf timed-out-in-arc-loop nil)
	  (dolist (arc to-check)
	    (cond ((and (arc-propagation-time-limit)
			(> (/ (- (get-internal-real-time)
				 loop-start)
			      internal-time-units-per-second)
			   (arc-propagation-time-limit)))
		   ;; timed out
		   (set-arc-propagation-timeout)
		   (setf timed-out-in-arc-loop t)
		   (return nil))
		  (t
		   (if *VERBOSE-CONSTRAINT-PROP*
		       (format *VERBOSE-CONSTRAINT-PROP* "."))
		   (setf changes (append changes
					 (add-induced-arcs-to-constraint 
					   c (first arc) (rest arc)))))))
	  
	  ;; exit if done
	  (when (and (null changes)
		     (not timed-out-in-arc-loop))
	    ;; mark constraint as path consistent
	    (setf (path-consistent c) t)
	    (return))
	  
	  ;; exit if time exceeded
	  (when (and (arc-propagation-time-limit)
		     (> (/ (- (get-internal-real-time)
			      loop-start)
			   internal-time-units-per-second)
			(arc-propagation-time-limit)))
	    (set-arc-propagation-timeout)
	    (if *VERBOSE-CONSTRAINT-PROP*
		(format *VERBOSE-CONSTRAINT-PROP*
			"~%OUT OF TIME in arc propagation (used ~,1f seconds)"
			(/ (- (get-internal-real-time)
			      loop-start)
			   internal-time-units-per-second)))
	    (return))
	  
	  (setf to-check changes)))))


(defmethod CHECK-ARC-AND-PROPAGATE ((c binary-rel-constraint)
                                    (i task) (j task))

  "Add induced arcs between i and j i.e. based on i => j and j => i.
    Propagate any changes if found."

  ;; only if there is an arc
  (if (or (current-constraint c i j)
	  (current-constraint c j i))
      (check-arcs-and-propagate c (list (cons i j) (cons j i)))))



(defmethod DEFINE-BINARY-CONSTRAINT ((c binary-rel-constraint)
                                     (i task) (j task) PCF)
  
  "Add the fact that i constrains j to a constraint.
    If i starts at t=0, the PCF gives the suitability
    of j at other times, i.e. the PCF represents Bj(t;ti)
    Multiple values are returned:
     changed: non-nil if the constraint changed
     unsched: non-nil if i constrains j to be unschedulable as a result
              of this constraint addition"
  
  ;; the constraint may be either open or closed:  if open, only
  ;; fields in the constraint are updated.  If closed, then the
  ;; task relative suitability is set to indicate that it must be
  ;; checked
  ;; Note that by the nature of this constraint, PCFs can only
  ;; become MORE restrictive, not less.  Thus only relative suitabilities
  ;; need to be reset. If constraints became LESS restrictive, then
  ;; open-suitability would need to be called.

  ;; error check
  (if (eq i j) (error "task ~a cannot constraint itself" i))
  (if (null pcf) (error "null pcf"))

  (let (
        ;; the hash tables holding the B functions and intervals
        (b-table (b-table c))
        (b-intervals-table (b-intervals-table c))
        (b-nonzero-intervals-table (b-nonzero-intervals-table c))
        
        ;; the constrains/constrained-by tables
        (constrained-by-table (constrained-by-table c))
        (constrains-table (constrains-table c))
        
	;; the sub-tables of the B tables (which might
	;; have to be created if they don't exist)
        i-table
        i-intervals-table
        i-nonzero-intervals-table
        )
    
    ;; add i and j to task list if not already there
    (pushnew i (tasks c))
    (pushnew j (tasks c))
    
    ;; record which constrains which: i => j
    (pushnew i (gethash j constrained-by-table))       ;j constrained by i
    (pushnew j (gethash i constrains-table))	       ;i constrains j
    
    ;; make sub-tables for constrained task if they don't exist
    (if (not (gethash j b-table))
      (setf (gethash j b-table)                   (make-hash-table :test #'eq)))
    (if (not (gethash j b-intervals-table))
      (setf (gethash j b-intervals-table)         (make-hash-table :test #'eq)))
    (if (not (gethash j b-nonzero-intervals-table))
      (setf (gethash j b-nonzero-intervals-table) (make-hash-table :test #'eq)))
    
    ;; set the pointers to the subtables
    (setf i-table                   (gethash j b-table)
          i-intervals-table         (gethash j b-intervals-table)
          i-nonzero-intervals-table (gethash j b-nonzero-intervals-table))
    
    ;; insert constraint: multiply with any already present
    (let* ((old-pcf      (gethash i i-table))
           (new-pcf      nil)
           (unsched-flag (equal *zero-PCF* old-pcf)))
      
      ;; get new PCF and see if changed
      (cond
       
       ;; new constraint?
       ((null old-pcf)
        (setf new-pcf pcf))
       
       ;; old constraint:  multiply with existing constraint
       (t 
        (multiple-value-bind (prod prod-changed)
                             (ipcf-multiply pcf old-pcf)
          (when prod-changed
            (setf new-pcf prod)))))
      
      ;; now new-pcf represents the cumulative effect of i on j
      
      (when new-pcf
        
        ;; set flag if j now unschedulable as a result of adding this constraint
        (when (and new-pcf
                   (equal new-PCF *zero-PCF*)
                   (or (null old-pcf)
                       (not (equal old-pcf *zero-PCF*))))
          (setf unsched-flag t)
          ;; only record diagnostic if news, i.e if j is not already 
          ;; unschedulable for some other reason
          (if (not (constrained-unschedulable c j))
            (record-diagnostic-message 
             j (format nil "~a constrains~% ~a to be unschedulable"
                       i j))))
        
        ;; set table entries
        (setf 
         ;; the B function PCF
         (gethash i i-table)                   new-pcf
         ;; remove intervals where value is zero
         (gethash i i-intervals-table)         (delete-if 
                                                #'(lambda (x) (= 0 (third x)))
                                                (ipcf-to-interval-list new-pcf))
         ;; nonzero intervals of the B function
         (gethash i i-nonzero-intervals-table) (ipcf-non-zero-intervals new-pcf)
	 ;; mark the constraint as not necessarily path consistent
	 (path-consistent c) nil
         )
        
	;; if the constraint is closed, then inform j that it needs to update
	;; its list of related tasks and its rel sultability
	(if (closed c)
	    (setf (rel-suitability         j) t
		  (directly-related-tasks  j) t
		  (all-related-tasks       j) t))

        ) ;end when new-pcf
      
      ;; returned values
      (values new-pcf unsched-flag)
      
      )))


(defmethod CONSTRAINED-UNSCHEDULABLE ((c binary-rel-constraint)
                                      (i task))
  
  "Return t if task i is constrained to be unschedulable by one or more
    other tasks, i.e. j => i to be unschedulable."
  
  (dolist (j (tasks-affecting-via-constraint c i))
    (if (null (current-constraint-intervals c j i))
      (return t))))


(defmethod CONSTRAINT-REL-PCF ((c binary-rel-constraint) (i task))
  
  "Return the relative suitability of i due to all tasks j s.t. j => i"
    
  (let* ((source-tasks (tasks-affecting-via-constraint c i))
         (pcf *unity-pcf*)
         pcf-component)
    
    (cond 
     
     ;; no constraining tasks, so no pcf
     ((null source-tasks)
      (setf pcf nil))
     
     ;; otherwise get contribution from each constraining task
     (t (dolist (j source-tasks)
          (setf pcf-component 
                (binary-rel-component (non-zero-intervals j) 
                                      (current-constraint-intervals c j i)))
          (setf pcf (ipcf-multiply pcf-component pcf))
          
          ;; catch unschedulables, note, and exit
          
          (when (equal pcf-component *zero-PCF*)
            ;;(format t "~%in ~a: component ~a is unschedulable" 
            ;;         (id i) (id j))
            ;; break out of loop over source tasks
            (return)
            )
          
          (when (equal pcf *zero-PCF*)
            ;;(format t "~%rel suitability of ~a is *zero-pcf*"
            ;;        (id i))
            ;; break out of loop over source tasks
            (return)
            )
          
          ) ;; end loop over source-tasks
        ))
    pcf))


(defmethod TASKS-AFFECTED-VIA-CONSTRAINT ((c binary-rel-constraint) 
                                          (i task))
  
  "Return list of tasks that i affects via this constraint"
  
  (gethash i (constrains-table c)))


(defmethod TASKS-AFFECTING-VIA-CONSTRAINT ((c binary-rel-constraint) 
                                           (i task))
  "Return list of tasks which can affect the suitability of 
    the input task, via this constraint"
  
  (gethash i (constrained-by-table c)))


(defmethod TASKS-RELATED-BY-CONSTRAINT ((c binary-rel-constraint))
  
  "Return list of all tasks related by this constraint"
  
  (tasks c))



#| test:
(binary-rel-component '((0 0)) '((5 10 0.5)))
 => (-1.797693134862316E308 0 5 0.5 10 0)
(binary-rel-component '((0 0)) '((-10 10 2)))
 => (-1.797693134862316E308 0 -10 2 10 0)
(binary-rel-component '((-1 1) (100 101)) '((-10 10 2)))
 => (-1.797693134862316E308 0 -11 2 11 0 90 2 111 0)
;; precedence (before & after)
(binary-rel-component '((0 10)(20 30)) (list (list *minus-infinity* -5 1)))
 => (-1.797693134862316E308 1 25 0)
(binary-rel-component '((0 10)(20 30)) (list (list 5 *plus-infinity* 1)))
 => (-1.797693134862316E308 0 5 1)
;; min sep of 5
(binary-rel-component '((0 0)) (list (list *minus-infinity* -5 1)
                                     (list 5 *plus-infinity* 1)))
 => (-1.797693134862316E308 1 -5 0 5 1)
(binary-rel-component '((0 10)) (list (list *minus-infinity* -5 1)
                                      (list 5 *plus-infinity* 1)))
 => (-1.797693134862316E308 1)
(binary-rel-component (list (list *minus-infinity* 0))
                      (list (list *minus-infinity* -5 1)))
 => (-1.797693134862316E308 1 -5 0)
|#

(defun BINARY-REL-COMPONENT (i-intervals B-intervals)

  ;; notation is that i constrains j and we seek Sj(t)
  ;; i-intervals is list '((t1 t2) (t3 t4)...) where suitability
  ;; of source task i is non-zero
  ;; B-intervals is list '((b1 b2 val)(b3 b4 val)...) from B function
  ;; Bj(t;ti)
  ;; return suitability Sj = max{ti/=0} Bj(t;ti)

  (let ((pcf *zero-pcf*))
    
    (dolist (i i-intervals)
      (let ((t-minus (first i))
            (t-plus (second i)))
        
        (dolist (b B-intervals)
          ;; B1 can be *minus-infinity* or B2 can be *plus-infinity*
          (let ((B1 (first b))
                (B2 (second b))
                (B-val (third b))
                c1 c2)
            ;; combine including infinite endpoints of intervals
            (setf c1 (cond ((or (= B1 *minus-infinity*)
                                (= t-minus *minus-infinity*))
                            *minus-infinity*)
                           (t (+ t-minus B1))))
            (setf c2 (cond ((or (= B2 *plus-infinity*)
                                (= t-plus *plus-infinity*))
                            *plus-infinity*)
                           (t (+ t-plus B2))))
            ;; build and combine pcf
            (setf pcf
                  (ipcf-combine pcf
                                (ipcf-create-with-single-interval c1 c2 B-val 0)
                                #'max))))))

    ;; returned value
    pcf))

(defmethod PRECEDENCE-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset (propagate nil) (instance nil))
   ;; offset is min sep from end of i to start of j

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset))
#|
  (format t "~%precedence task1 ~s task2 ~s offset ~s min1 ~s min2 ~s~%"
	  (id i) (id j) offset (minimum-duration i) (minimum-duration j))
|#
  (let ((d-i (minimum-duration i)))
    (define-binary-constraint c i j
      (ipcf-create-with-single-interval 
	(+ d-i (if offset offset 0))
	*plus-infinity*
	1 0))
    (Record-Constraint-For-Trans i j)
    (define-binary-constraint c j i
      (ipcf-create-with-single-interval 
	*minus-infinity*
	(- (+ d-i (if offset offset 0)))
	1 0))
    (Record-Constraint-For-Trans j i)
    (if propagate
	(check-arc-and-propagate c i j))
    
))

(defmethod PRECEDENCE-FIXED-OFFSET-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset (propagate nil)(instance nil))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset))
  ;; offset is sep from end of i to start of j
  (let* ((d-i (minimum-duration i))
         (sep (+ d-i (if offset offset 0)))
         (minus-sep (- sep)))
    (define-binary-constraint c i j
      (ipcf-create-with-single-interval
	sep sep 1 0))
    (Record-Constraint-For-Trans i j)
    (define-binary-constraint c j i
      (ipcf-create-with-single-interval 
	minus-sep minus-sep 1 0))
    (Record-Constraint-For-Trans j i)
    (if propagate
	(check-arc-and-propagate c i j))
    
))

(defmethod FIXED-OFFSET-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset (propagate nil) (instance nil))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset))
  ;; offset is sep from end of i to start of j
  (let* ((d-i (minimum-duration i))
	 (sep (+ d-i (if offset offset 0)))
         (minus-sep (- sep)))

    (define-binary-constraint c i j
      (ipcf-create-with-single-interval 
	sep sep 1 0))
    (Record-Constraint-For-Trans i j)
    (define-binary-constraint c j i
      (ipcf-create-with-single-interval 
	minus-sep minus-sep 1 0))
    (Record-Constraint-For-Trans j i)
      
    (if propagate
	(check-arc-and-propagate c i j))
    
))

(defmethod PRECEDENCE-ASAP-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) (propagate nil) (instance nil))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j))
  ;; constrain j to start as soon as possible after
  ;; the end of i
  (let* ((d-i (minimum-duration i))
         (made-link t)
	 (min-max-sep (min-max-separation c i j))
	 (min-sep (if min-max-sep (first min-max-sep)))
	 (max-sep (if min-max-sep (second min-max-sep)))
	 delta sep minus-sep)
    (cond
      ((or (null min-max-sep)
	   (< max-sep 0))
       ;; either constrained unschedulable, or j is
       ;; constrained so that it cannot start after i ends
       (setf made-link nil))
      (t
       ;; delta is sep from end of i to start of j
       (setf delta (max-of-two-values 0 min-sep))
       ;; sep is separation from start of i to start of j
       (setf sep (+ d-i delta))
       (setf minus-sep (- sep))
       (define-binary-constraint c i j
	 (ipcf-create-with-single-interval 
	   sep sep 1 0))
       (Record-Constraint-For-Trans i j)
       (define-binary-constraint c j i
	 (ipcf-create-with-single-interval 
	   minus-sep minus-sep 1 0))
       (Record-Constraint-For-Trans j i)
       ))      
    (if (and propagate made-link)
	(check-arc-and-propagate c i j))
    
))

(defmethod PRECEDENCE-PREFERENCE-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) value1 value2 (propagate nil) (instance nil))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :value1 value1 :value2 value2))
  ;; prefer i before j
  ;; value1 is suitability before, value2 is suitability after
  (let ((d-i (minimum-duration i)))
    (define-binary-constraint c i j
      (ipcf-create-with-single-interval 
	*minus-infinity* (- d-i 1)
	value1 value2))
    (Record-Constraint-For-Trans i j)
    (define-binary-constraint c j i
      (ipcf-create-with-single-interval
	(- (- d-i 1)) *plus-infinity*
	value1 value2))
    (Record-Constraint-For-Trans j i)
    (if propagate
	(check-arc-and-propagate c i j))
    
))

(defmethod MAX-TIME-SEPARATION-CONSTRAINT
    ((c binary-rel-constraint) (i task) (j task)
     &key type (special-requirement nil) offset (propagate nil) (instance nil))
  ;; offset is max time from start of i to end of j
  (log-constraint c instance (list :type type :special-requirement special-requirement
				   :task1 i :task2 j :offset offset))
  #|(format t "~%in max time task1 ~s task1 ~s offset ~s min1 ~s min2 ~s"
	  (id i) (id j) offset (minimum-duration i) (minimum-duration j))|#
  (let ((d-i (minimum-duration i))
        (d-j (minimum-duration j))
        (made-link t))
    (cond ((and offset
		(>= offset (+ d-i d-j)))
	   (define-binary-constraint c i j
				     (ipcf-create-with-single-interval 
				      (- (- offset d-i)) (- offset d-j) 1 0))
	   (Record-Constraint-For-Trans i j)
	   (define-binary-constraint c j i
				     (ipcf-create-with-single-interval
				      (- (- offset d-j)) (- offset d-i) 1 0))
	   (Record-Constraint-For-Trans j i))
	  (t (setf made-link nil)
	     (cerror "No constraint will be defined" 
		     "Offset for max-time-separation not >= Di+Dj")))
    (if (and propagate made-link)
	(check-arc-and-propagate c i j))))

(defmethod MAX-TIME-SEPARATION-PREFERENCE-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset offset2 (propagate nil) (instance nil))
  ;; linearly decreasing value of PCF from minimum to maximum
  ;; the maximum separation (start of i to end of j, or vice versa)
  ;; is given by offset which must be >= Di+Dj
  ;; offset2, if specified, must be less than offset:  separations
  ;; less than offset2 have suitability 1.  If not specified, offset2
  ;; defaults to Di+Dj
  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset :offset2 offset2))
  (let ((d-i (minimum-duration i))
        (d-j (minimum-duration j))
        (made-link t))
    (cond ((and offset
		(>= offset (+ d-i d-j)))
	   (let* ((min-offset (if (and offset2
				       (< offset2 offset))
				  offset2
				  (+ d-i d-j)))
		  (i->j-PCF *zero-PCF*)
		  (j->i-PCF *zero-PCF*)
		  (val-change (/ 1 (1+ (- offset min-offset))))
		  (current-offset offset)
		  (val val-change)
		  (rounded-val nil)
		  (round-to 20) ;e.g. 10, 100, etc.
		  (round-frac (/ 1.0 round-to)))
		
	     (loop
	       (if (< current-offset min-offset) (return))
	       ;; val rounded
	       (setf rounded-val (coerce (/ (round val round-frac) round-to) 'float))
	       ;;(format t "~%Offset: ~d, val: ~a" current-offset rounded-val)
	       (setf i->j-PCF
		     (ipcf-set-value
		       (- (- current-offset d-i)) (- current-offset d-j) rounded-val i->j-PCF))
	       (setf j->i-PCF
		     (ipcf-set-value
		       (- (- current-offset d-j)) (- current-offset d-i) rounded-val j->i-PCF))
	       (setf val (+ val val-change))
	       (setf current-offset (- current-offset 1)))
		
	     (define-binary-constraint c i j i->j-PCF)
	     (Record-Constraint-For-Trans i j)
	     (define-binary-constraint c j i j->i-PCF)
	     (Record-Constraint-For-Trans j i)
	     ))
	     
	  (t (setf made-link nil)
	     (cerror "No constraint will be defined" 
		     "Offset for max-time-separation-preference not >= Di+Dj")))
    (if (and propagate made-link)
	(check-arc-and-propagate c i j))
    
))

(defmethod PRECEDENCE-PREFERRED-ASAP-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset offset2 (propagate nil) (instance nil))
  ;; j as soon as possible after i, with min separation
  ;; between end of i and start of j = offset (default 0), and
  ;; max separation between start of i and end of j = offset2
  (precedence-constraint c i j :type :precedence :offset offset)
  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset :offset2 offset2))
  (if offset2
      ;; note reversal of meaning offset and offset2 - bad idea.
    (max-time-separation-preference-constraint c i j :type :max-time-separation-preference
					       :offset offset2
					       :offset2 (+ (if offset offset 0)
							   (minimum-duration i)
							   (minimum-duration j)))
    (if propagate
	(check-arc-and-propagate c i j))
    
))

;;; Modified May 17, 1991  AAG - allowed separations to be different when
;;; i precedes j from when j precedes i.  This is needed for the trans
;;; implementation of phase criticals

(defmethod MIN-TIME-SEPARATION-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) offset (propagate nil) (instance nil)
			       (i-j-offset offset)    ; offset when i precedes j 
			       (j-i-offset offset))   ; offset when j precedes i

  (when (and offset (not (equalp i-j-offset offset)) (not (equalp j-i-offset offset)))
    (cerror "offset value will be ignored"
	    "offset specified with i-j-offset and j-i-offset"))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j :offset offset :i-j-offset i-j-offset
				 :j-i-offset j-i-offset))
  (let ((d-i (minimum-duration i))
        (d-j (minimum-duration j))
        (made-link t))
    (cond ((or (null offset)
	       (and offset
		    (>= offset 0)))
	   ;; if there is an offset it must be >= 0; if missing,
	   ;; zero is assumed
	   (define-binary-constraint c i j
	     (ipcf-create-with-single-interval
	       (1+ (- (+ d-j (if j-i-offset j-i-offset 0))))
	       (1- (+ d-i (if i-j-offset i-j-offset 0)))
	       0 1))
	   (Record-Constraint-For-Trans i j)
	   (define-binary-constraint c j i
	     (ipcf-create-with-single-interval
	       (1+ (- (+ d-i (if i-j-offset i-j-offset 0))))
	       (1- (+ d-j (if j-i-offset j-i-offset 0)))
	       0 1))
	   (Record-Constraint-For-Trans j i)
	   )
	  (t (setf made-link nil)
	     (cerror "No constraint will be defined"
		     "Offset for min-time-separation not >= 0")))
    (if (and propagate made-link)
	(check-arc-and-propagate c i j))
    
))

(defmethod SIMULTANEOUS-CONSTRAINT ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type (special-requirement nil) (propagate nil) (instance nil))

  (log-constraint c instance (list :type type :special-requirement special-requirement
				 :task1 i :task2 j))
  (define-binary-constraint c i j
    (ipcf-create-with-single-interval 0 0 1 0))
  (Record-Constraint-For-Trans i j)
  (define-binary-constraint c j i
    (ipcf-create-with-single-interval 0 0 1 0))
  (Record-Constraint-For-Trans i j)
      
  (if propagate
      (check-arc-and-propagate c i j))
    
)
 

;;; SIMPLER INTERFACE FOR DEFINING CONSTRAINTS OF STANDARD TYPE
;;; Note:  can pre-propagate when constraints defined: this is faster
;;; than path-consistency post facto

(defmethod MUTUALLY-CONSTRAIN ((c binary-rel-constraint)
                               (i task) (j task)
                               &key type offset offset2 value1 value2 (propagate nil))
  "High-level function to add a link i constrains j, j constrains i, of
    various types.  c is the constraint, i and j are tasks, type is the type
    of constraint.  Some types require additional values to be specified.
    :propagate t means check the added arcs and propagate constraints
    (i.e. maintain path-consistency)"
  
  (declare (ignore type offset offset2 value1 value2 propagate))
  (format t "~%~%mutually-constrain should no longer be called~%")
  (break)
  
  )

;;; MULTIPLE PAIRWISE CONSTRAINTS

;; ?? if propagate is t, should really propagate after each binary
;; constraint defined, not as a group when done

(defmethod PRECEDE ((c binary-rel-constraint) (first-task task) succ-list &key (instance nil)
		    (special-requirement nil))
  "Constrain FIRST-TASK to come before all tasks in SUCC-LIST"
  (declare (special pred-succ-table-built))
  (let ((arcs nil))
    (dolist (succ succ-list)
      (unless (eq first-task succ)
	(precedence-constraint c first-task succ :type :precedence :instance instance
			       :special-requirement special-requirement)
	(when pred-succ-table-built
	  (set-precedence first-task succ))
	(push (cons first-task succ) arcs)
	(push (cons succ first-task) arcs)))
    (check-arcs-and-propagate c arcs)
    nil))

;;; Force i before j, using :precedence
;;; For :max-time-separation, offset is max time from start-i to end-j.
;;; start-i to start-j = t1+t2, therefore offset = t1 + t2 + duration-j
;;; Use :precedence for min separation, offset = min sep from end-i to start-j
;;; = (t1 - t2) - duration-i
;;;
(defmethod AFTER-ORDER ((c binary-rel-constraint) (i task) (j task) t1 t2 &key (propagate nil) (instance nil))
  "Constrain j after i by t1 +/- t2.  t1 is start-to-start time."
  (precedence-constraint c i j :type :precedence :instance instance)
  (max-time-separation-constraint c i j :type :max-time-separation :offset (+ t1 t2 (maximum-duration j)) :instance instance)
  (precedence-constraint c i j :type :precedence :offset (- t1 t2 (minimum-duration i)) :instance instance)
  (when propagate
    (check-arcs-and-propagate c (list (cons i j) (cons j i)))))


(defmethod ORDER-PREFERENCE ((c binary-rel-constraint)
			     task-list &key (propagate nil) (instance nil))
  (dolist (i task-list)
    (let ((followers (rest (subseq task-list (position i task-list)))))
      ;; between all pairs
      (when (first followers)
        (dolist (j followers)
	  (precedence-preference-constraint c i j :type :precedence-preference
					    :value1 0.9 :value2 1.1 :propagate propagate
					    :instance instance)
                              )))))


(defmethod STRICT-ORDER ((c binary-rel-constraint) task-list &key (propagate nil) special-requirement (instance nil))
  ;; add precedence constraint to successive pairs

  (let ((arcs nil))
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do))
	  (j (second to-do) (second to-do)))
	 ((null j) nil)
      
      (precedence-constraint c i j 
			     :type :precedence
			     :special-requirement special-requirement
			     :instance instance)
      (when propagate
	(push (cons i j) arcs)
	(push (cons j i) arcs)))
    (when propagate
      (check-arcs-and-propagate c arcs))))


(defmethod STRICT-ORDER-NOGAP ((c binary-rel-constraint)
			       task-list &key (propagate nil) special-requirement (instance nil))

  (let ((arcs nil))
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do))
	  (j (second to-do) (second to-do)))
	 ((null j) nil)
      
      (precedence-fixed-offset-constraint c i j 
					  :type :precedence-fixed-offset
					  :special-requirement special-requirement
					  :instance instance)

      (when propagate
	(push (cons i j) arcs)
	(push (cons j i) arcs)))
    (when propagate
      (check-arcs-and-propagate c arcs))))


(defmethod STRICT-ORDER-ASAP ((c binary-rel-constraint)
			       task-list &key (propagate nil) (instance nil) (special-requirement nil))


  (let ((arcs nil))
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do))
	  (j (second to-do) (second to-do)))
	 ((null j) nil)
      
      (precedence-asap-constraint 
	c i j :type :precedence-asap :instance instance :special-requirement special-requirement)
      (when propagate
	(push (cons i j) arcs)
	(push (cons j i) arcs)))
    (when propagate
      (check-arcs-and-propagate c arcs))))

(defmethod STRICT-PAIR-ORDER-ASAP ((c binary-rel-constraint)
				   task1 task2 &key (special-requirement nil) (propagate nil) (instance nil))

  (precedence-asap-constraint 
    c task1 task2 :type :precedence-asap :propagate propagate :instance instance :special-requirement
    special-requirement))

;;; generalize to allow arbitrary fcn specifying separation

(defmethod NO-OVERLAP ((c binary-rel-constraint)
		       task-list &key (propagate nil) (instance nil))
  (let ((arcs nil))
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do)))
	 ((null i) nil)
      
      (dolist (j (rest to-do))
	(min-time-separation-constraint c i j :type :min-time-separation :instance instance)
	(when propagate
	  (push (cons i j) arcs)
	  (push (cons j i) arcs))))
    (when propagate
      (check-arcs-and-propagate c arcs))))


(defmethod GROUP-WITHIN ((c binary-rel-constraint)
			 task-list group-time &key (propagate nil) special-requirement (instance nil))
  (let ((arcs nil))
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do)))
	 ((null i) nil)
      
      (dolist (j (rest to-do))
	(max-time-separation-constraint c i j 
					:type :max-time-separation 
					:offset group-time 
					:special-requirement special-requirement
					:instance instance)	
	(Record-Constraint-For-Trans i j)
	(when propagate
	  (push (cons i j) arcs)
	  (push (cons j i) arcs))))
    (when propagate
      (check-arcs-and-propagate c arcs))))


(defmethod GROUP-WITHIN-PREFERENCE ((c binary-rel-constraint)
				    task-list group-time
				    &key
				    (min-group-time nil) (propagate nil)(instance nil))
  ;; group time is the MAX separation of any pair of tasks
  ;; min-group-time is a time such that offsets in this range have unity suitability
  ;; if not specified, it defaults to the sum of min durations of the tasks in task-list
  (let ((arcs nil)
	(offset2 min-group-time))

    (when (not offset2)
      ;; compute it
      (setf offset2 0)
      (dolist (i task-list)
	(incf offset2 (minimum-duration i))))
    
    (do* ((to-do task-list (rest to-do))
	  (i (first to-do) (first to-do)))
	 ((null i) nil)
      
      (dolist (j (rest to-do))
	(max-time-separation-preference-constraint c i j :type :max-time-separation-preference 
						   :offset group-time 
						   :offset2 offset2
						   :instance instance)
	(when propagate
	  (push (cons i j) arcs)
	  (push (cons j i) arcs))))
    (when propagate
      (check-arcs-and-propagate c arcs))))


;;; METHODS TO RETURN INFORMATION ABOUT HOW A TASK IS CONSTRAINED

(defmethod ORDER-DETERMINED ((c binary-rel-constraint)
                             (i task) (j task))
  
  "Return :before if i must come before j, :after if i must follow j,
    or nil in any other case"
  
  ;; constraint can be either open or closed
  (let* ((ij-pcf (current-constraint c i j))
         max-before max-after max-in-overlap)
    
    (if ij-pcf
	(setf max-before 
	      (ipcf-max ij-pcf *minus-infinity* (- (minimum-duration j)))
	      max-after 
	      (ipcf-max ij-pcf (minimum-duration i) *plus-infinity*)
	      max-in-overlap
	      (ipcf-max ij-pcf 
			(- (- (minimum-duration j) 1))
			(- (minimum-duration i) 1))))

    ;;(format t "~%pcf ~a" ij-pcf)
    ;;(format t "~%max before ~a, after ~a, during ~a"
    ;;        max-before max-after max-in-overlap)
    ;; pcf is: given i, where must j start
    
    (cond
      
      ((null ij-pcf)
       ;; no relationship
       nil)
      
      ((equal ij-pcf *zero-PCF*)
       ;; constrained unschedulable
       nil)
    
      ((null max-in-overlap)
       ;; this case can arise if i and j are of zero duration
       ;; and are constrained to be simultaneous
       nil)
  
      ((and (> max-after       0)
	    (= max-in-overlap  0)
	    (= max-before      0))
       :before)
      
      ((and (> max-before      0)
	    (= max-in-overlap  0)
	    (= max-after       0))
       :after)
      
      (t nil))))


(defmethod ORDER-POSSIBILITIES ((c binary-rel-constraint)
                                (i task) (j task))
  
  "Return list of up to three ordering possibilities for i and j:
    :before if i can come before j, :overlap if i and j can overlap,
    :after if i can follow j.  If i does not constrain j, then return
    list '(:before :after :overlap) indicating that all three relationships
    are possible."
  
  ;; constraint can be either open or closed
  (let* ((ij-pcf (current-constraint c i j))
         (result nil)
         max-before max-after max-in-overlap)
          
      (if ij-pcf
	  (setf max-before 
		(ipcf-max ij-pcf *minus-infinity* (- (minimum-duration j)))
		max-after 
		(ipcf-max ij-pcf (minimum-duration i) *plus-infinity*)
		max-in-overlap
		(ipcf-max ij-pcf 
			  (- (- (minimum-duration j) 1))
			  (- (minimum-duration i) 1))))
      
      ;;(format t "~%pcf ~a" ij-pcf)
      ;;(format t "~%max before ~a, after ~a, during ~a"
      ;;        max-before max-after max-in-overlap)
      ;; pcf is: given i, where must j start
      
      (cond
       
       ((or (null ij-pcf)
	    (null max-in-overlap))
	;; null max-in-overlap can arise if i and j are of zero
	;; duration and constrained to be simultaneous
        (setf result (list :before :overlap :after)))
       
       
       (t (if (> max-after      0) (push :before  result))
          (if (> max-before     0) (push :after   result))
          (if (> max-in-overlap 0) (push :overlap result))))
      
      result))


(defmethod PREFERRED-ORDER ((c binary-rel-constraint)
                            (i task) (j task))
  
  "return :before if i preferred to come before j, :after if i 
    preferred to follow j, or nil for any other cases"
  
  ;; constraint can be either open or closed
  (let* ((ij-pcf (current-constraint c i j))
         max-before max-after max-in-overlap)
    
    (if ij-pcf
	(setf max-before 
	      (ipcf-max ij-pcf *minus-infinity* (- (minimum-duration j)))
	      max-after 
	      (ipcf-max ij-pcf (minimum-duration i) *plus-infinity*)
	      max-in-overlap
	      (ipcf-max ij-pcf 
			(- (- (minimum-duration j) 1))
			(- (minimum-duration i) 1))))
    
    ;; pcf is: given i, where must j start
    
    (cond ((null ij-pcf)
	   ;; no relationship
	   nil)
	  
	  ((or (= 0 max-after)
	       (= 0 max-before))
	   nil)
	  
	  ((> max-after  max-before)
	   :before)
	  
	  ((< max-after  max-before)
	   :after)
	  
	  (t nil))))


(defmethod MIN-MAX-SEPARATION ((c binary-rel-constraint) (i task) (j task))
  
  "Return list (min max) time separation from end of i to start of
    j, if known, else nil"
  
  ;; constraint can be either open or closed
  (let* ((ij-pcf (current-constraint c i j)))
    
    (multiple-value-bind  (early late nonzero-intervals)
                          (ipcf-bounds-and-intervals ij-pcf)
      
      (declare (ignore nonzero-intervals))
      
      (if (and early late)
        
        ;; if both early and late times are known then
        ;; prepare list
        (list (- early (minimum-duration i))
              (- late  (minimum-duration i)))
        
        ;; else not known
        nil))))


(defmethod TASK-CAN-IMMEDIATELY-FOLLOW
	   ((c binary-rel-constraint) (i task) (j task))
  
  "Return t if j can immediately follow i, or if there is no 
    constraint between i and j, else return nil (i.e.
    if there is a constraint and j is not allowed to start
    at the end of i"
  
  ;; constraint can be either open or closed
  (let ((ij-pcf (current-constraint c i j)))
          
    ;; pcf is: given i, where must j start
    (cond ((null ij-pcf)
	   ;; no constraint
	   t)
	  ((equal *zero-PCF* ij-pcf)
	   ;; constrained unschedulable
	   nil)
	  ((= (ipcf-get-value (minimum-duration i) ij-pcf) 0)
	   ;; suitability = 0 at end of i
	   nil)
	  (t
	   ;; ok for j to start
	   t))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;  INTERNAL FUNCTION: Kludgy-Type-of-Workaround
;
;  This function attempts to emulate the behavior of type of which is buggy
;  in pcl.  It is slow and uses implementation dependent information.  In
;  short is a temporary workaround and MUST be replaced.
;
;  ARGUMENTS
;     obj           a clos object
;
;  RETURNS
;     the symbol type of obj
;
;  HISTORY
;     Hacked Sept. 18, 1989  AAG
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun Kludgy-Type-of-Workaround (obj)
  (class-name (class-of obj)))


(defgeneric COPY-AND-FILTER-TASKS (constraint
				   single-task-filter-fcn
				   two-task-filter-fcn
				   &optional task-mapping-function)
  (:documentation
   "Make a copy of a constraint including only a specified subset of tasks.
single-task-filter-fcn when applied to a task returns non-nil if the task
should be in the copy, nil otherwise.  two-task-filter-fcn takes two tasks
i and j as arguments and returns non-nil if a constraint i -> j should be
included in the copy, nil otherwise.  Optionally the constraint can apply
to a different set of tasks: if task-mapping-function is supplied it must
take a task i as argument and return another i-prime: the copied constraint
will refer to the mapped tasks only."))

(defmethod COPY-AND-FILTER-TASKS ((c binary-rel-constraint)
				  single-task-filter-fcn
				  two-task-filter-fcn
				  &optional
				  task-mapping-function) 
  (let ((c-copy (make-instance (Kludgy-Type-of-Workaround c)
		  :source (source c))))
    (dolist (i (tasks c))
      (when (funcall single-task-filter-fcn i)
	;;(format t "~%keeping ~a ~a => " i (if (task-collapsed c i) "col" ""))
	(dolist (j (tasks c))		;(tasks-affected-via-constraint c i)
	  (when (and (not (eq i j))
		     (funcall single-task-filter-fcn j)
		     (funcall two-task-filter-fcn i j))
	    (let ((pcf (current-constraint c i j)))
	      (when pcf
		;;(Format t " ~a" j)
		(define-binary-constraint
		 c-copy
		 (if task-mapping-function (funcall task-mapping-function i) i)
		 (if task-mapping-function (funcall task-mapping-function j) j)
		 pcf)))))))
    ;; if the input is path consistent, so is the copy
    (if (path-consistent c)
	(setf (path-consistent c-copy) t))
    ;; returned value
    c-copy))
      
;;; NOTE: should probably define an :around method for collapsible 
;;; binary rel constraint which collapses the constraint after the
;;; copy is made?


;;; ==================================================================
;;;                                     COLLAPSIBLE-BINARY-REL-CONSTRAINT

;; extension to binary rel constraint that collapses tasks
;; related by fixed time offsets

(defclass COLLAPSIBLE-BINARY-REL-CONSTRAINT (binary-rel-constraint)
  (
   (fixed-offset-table 
    :initform (make-hash-table :test #'eq)
    :accessor fixed-offset-table
    :documentation
    "key a task, value (cons b offset), b task
     where a is collapsed task, offset as specified from b")
   
   (collapsed-tasks-table 
    :initform (make-hash-table :test #'eq)
    :accessor collapsed-tasks-table
    :documentation
    "key a task, value list of all collapsed tasks offset from a")
   )
  
  (:documentation
   "Specialization binary relative constraint which more efficiently 
    handles groups of tasks with fixed time offsets")
  )


(defun MAKE-COLLAPSIBLE-BINARY-REL-CONSTRAINT (source)

  "Create an instance of a collapsible binary rel constraint"
  
  (make-instance 'collapsible-binary-rel-constraint :source source))


(defmethod CONSTRAINT-DETAILS :AFTER ((c collapsible-binary-rel-constraint)
                                      &optional (stream t))
  
  (format stream "~%Collapsed:")
  (maphash #'(lambda (key val)
               (format stream "~%~a ~a by ~a" 
                       (id key) (id (first val)) (rest val)))
           (fixed-offset-table c)))


(defmethod TASK-COLLAPSED ((c collapsible-binary-rel-constraint)
                           (i task))

  "Return nil if j not collapsed, else (cons i D) where j is
    collapsed and is offset by D from task i"
  
  (gethash i (fixed-offset-table c)))


(defmethod TASKS-COLLAPSED-WITH-RESPECT-TO 
	   ((c collapsible-binary-rel-constraint)
	    (i task))
  
  "Return list of tasks collapsed with respect to input task i,
    or nil if none"
  
  (gethash i (collapsed-tasks-table c)))


(defmethod TASKS-ARE-COLLAPSIBLE ((c collapsible-binary-rel-constraint)
				  (i task) (j task))

  "Return fixed time separation due to mutual constraint i <=> j
    if any exists (i.e. j can be collapsed w.r.t. i with this time offset)
    else nil"

  ;; j cannot be collapsed w.r.t. i if j is already collapsed or if there
  ;; are any tasks already collapsed w.r.t j

  (when (and (not (task-collapsed c j))
	     (not (tasks-collapsed-with-respect-to c j)))

  (let ((ij-NZI (current-constraint-nonzero-intervals c i j))
	(ji-NZI (current-constraint-nonzero-intervals c j i)))
    (when (and (eq (list-length ij-NZI) 1)
	       (eq (list-length ji-NZI) 1)
	       )
      (let ((ij1 (first (first ij-NZI)))
	    (ij2 (second (first ij-NZI)))
	    (ji1 (first (first ji-NZI)))
	    (ji2 (second (first ji-NZI))))
	;; since times are integers this test can be on equality of times
	(if (and (/= ij1 *minus-infinity*)
		 (/= ij2 *plus-infinity*)
		 (/= ji1 *minus-infinity*)
		 (/= ji2 *plus-infinity*)
		 (= ij2 ij1)
		 (= ji2 ji1)
		 (= (+ ij1 ij2) (+ (- ji1) (- ji2))))
	    ;; j can be collapsed w.r.t. i by this time
	    ij1
	    ;; no collapse possible
	    nil
	    ))))))


(defmethod COLLAPSE-CONSTRAINT ((c collapsible-binary-rel-constraint))

  "Find all tasks that are constrained to have a fixed time offset to
    another task and simplify the constraint to handle this case directly"

  (let ((found-collapse nil))
    (dolist (i (tasks c))
      (when (not (task-collapsed c i))
        
        (loop
          (setf found-collapse nil)
          
          (dolist (j (gethash i (constrains-table c)))
            
	    (let ((j-offset (tasks-are-collapsible c i j)))
	      (when j-offset
		;; j will be collapsed task
		(collapse-arc c i j j-offset)
		(setf found-collapse t)
		)))
	  
	  ;; break out of loop
	  (if (not found-collapse) (return)))))))


;;; A collapsed task j is one with a fixed time offset to another task i
;;; Whether i or j becomes collapsed is arbitrary.

(defmethod COLLAPSE-ARC ((c collapsible-binary-rel-constraint)
                         (i task) (j task) offset)
  
  "Collapse arc from i to j: j disappears from tables. Returned
    value is the collapsed task j."

  (if *VERBOSE-CONSTRAINT-PROP*
      (format *VERBOSE-CONSTRAINT-PROP*
	      "~%Collapsing ~a w.r.t. ~a by time ~a"
	      (id j) (id i) offset))
  
  ;; add any arcs p => i => j => q, all p and q
  (add-induced-arcs-to-constraint c i j)
  (add-induced-arcs-to-constraint c j i)
  
  ;; store task and offset in fixed-offset-table
  (setf (gethash j (fixed-offset-table c)) (cons i offset))
  
  ;; record the fact that j is fixed offset from i 
  (push j (gethash i (collapsed-tasks-table c)))
  
  ;; delete j from all tables except task list
  (delete-task-from-tables c j)
  (when  *RECORD-CONSTRAINTS-FOR-TRANS-REPORT*
    (CPR-Record-Collapse j offset i))
  ;; returned value is task j
  j)





;;; MODIFIED METHODS FOR COLLAPSED TASKS

(defmethod CURRENT-CONSTRAINT
  ((c collapsible-binary-rel-constraint) (i task) (j task))
  
  "Return current constraint PCF 'i constrains j' if present, nil otherwise,
    i.e. Bj(t;ti)"

  ;;  (format t "~%                                        =COL: ~a ~a "
  ;; (if (task-collapsed c i) "Y" "N")
  ;; (if (task-collapsed c j) "Y" "N"))
  (let ((offset nil)
        (offset-act nil)
        (offset-time nil))
    
    (cond
     
     ;; i is a collapsed task
     ((setf offset (task-collapsed c i))
      (setf offset-act (first offset)
            offset-time (rest offset))
      ;; (format t "i (~a) collapsed" i)
      (cond ((eq j offset-act)
             (ipcf-create-with-single-interval
              (- offset-time) (- offset-time) 1 0))
            (t (ipcf-shift-time (current-constraint c offset-act j)
                                offset-time)))
      )
     
     ;; j is a collapsed task
     ((setf offset (task-collapsed c j))
      (setf offset-act (first offset)
            offset-time (rest offset))
      ;; (format t "j (~a) collapsed" i)
      (cond ((eq i offset-act)
             (ipcf-create-with-single-interval
              offset-time offset-time 1 0))
            (t (ipcf-shift-time (current-constraint c i offset-act)
                                (- offset-time))))
      )
     
     ;; neither is collapsed:  use usual method
     (t (call-next-method)))))


(defmethod CURRENT-CONSTRAINT-INTERVALS
	   ((c collapsible-binary-rel-constraint) 
	    (i task) (j task))

  "return current constraint PCF 'i constrains j' if present, nil otherwise,
    i.e. Bj(t;ti)"
  
  (let ((offset nil)
        (offset-act nil)
        (offset-time nil))
    
    (cond
      
      ;; i is collapsed
      ((setf offset (task-collapsed c i))
       (setf offset-act (first offset)
	     offset-time (rest offset))
       (cond ((eq j offset-act)
	      (list (list (- offset-time) (- offset-time) 1)))
	     (t (shift-interval-list
		  (current-constraint-intervals c offset-act j)
		  offset-time)))
       )
      
      ;; j is collapsed
      ((setf offset (task-collapsed c j))
       (setf offset-act (first offset)
	     offset-time (rest offset))
       (cond ((eq i offset-act)
	      (list (list offset-time offset-time 1)))
	     (t (shift-interval-list
		  (current-constraint-intervals c i offset-act)
		  (- offset-time))
		)))
      
      ;; neither collapsed: usual method
      (t (call-next-method)))))


(defmethod CURRENT-CONSTRAINT-NONZERO-INTERVALS 
	   ((c collapsible-binary-rel-constraint) 
	    (i task) (j task))
  "return current constraint PCF 'i constrains j' if present, nil otherwise,
    i.e. Bj(t;ti)"
  
  (let ((offset nil)
        (offset-act nil)
        (offset-time nil))
    
    (cond
      
      ;; i is collapsed
      ((setf offset (task-collapsed c i))
       (setf offset-act (first offset)
	     offset-time (rest offset))
       ;; (format t "~%   i (~a) collapsed" i)
       (cond ((eq j offset-act)
	      (list (list (- offset-time) (- offset-time))))
	     (t (shift-interval-list
		  (current-constraint-nonzero-intervals c offset-act j)
		  offset-time)))
       )
      
      ;; j is collapsed
      ((setf offset (task-collapsed c j))
       (setf offset-act (first offset)
	     offset-time (rest offset))
       ;; (format t "~%   j (~a) collapsed" j)
       (cond ((eq i offset-act)
	      (list (list offset-time offset-time)))
	     (t (shift-interval-list
		  (current-constraint-nonzero-intervals c i offset-act)
		  (- offset-time))
		)))
      
      ;; neither collapsed: usual method
      (t (call-next-method)))))


(defmethod TASKS-AFFECTED-VIA-CONSTRAINT
    ((c collapsible-binary-rel-constraint) 
     (i task))
  (let ((collapsed (task-collapsed c i)))
    (cond (collapsed
	   (cons (first collapsed)
		 (gethash (first collapsed) (constrains-table c))))
	  (t
	   (let ((constrains (gethash i (constrains-table c))))
	     (apply #'append
		    constrains
		    (gethash i (collapsed-tasks-table c))
		    (mapcar #'(lambda(task)
				(gethash task (collapsed-tasks-table c)))
			    constrains)))))))


(defmethod TASKS-AFFECTING-VIA-CONSTRAINT
    ((c collapsible-binary-rel-constraint) 
     (i task))
  (let ((collapsed (task-collapsed c i)))
    (cond (collapsed
	   (cons (first collapsed)
		 (gethash (first collapsed) (constrained-by-table c))))
	  (t
	   (let ((constrained-by (gethash i (constrained-by-table c))))
	     (apply #'append
		    constrained-by
		    (gethash i (collapsed-tasks-table c))
		    (mapcar #'(lambda(task)
				(gethash task (collapsed-tasks-table c)))
			    constrained-by)))))))


(defmethod MAKE-PATH-CONSISTENT :BEFORE
  ((c collapsible-binary-rel-constraint))

  ;; collapse before doing path consistency
  (collapse-constraint c))


(defmethod DEFINE-BINARY-CONSTRAINT
    ((c collapsible-binary-rel-constraint)
     (i task) (j task) PCF)
  
  (let ((i-collapsed (task-collapsed c i))
        (j-collapsed (task-collapsed c j)))
    
    (cond 
     
     ;; if neither one is collapsed then just use usual method
     ((and (not i-collapsed) (not j-collapsed))
      ;;(format t "~%call next method: ~a ~a ~a"
      ;;        (id i) (id j) pcf)
      (call-next-method))
     
     ;; if either i collapsed w.r.t. j or vice versa, just check
     ;; that new constraint doesn't make one or the other unschedulable
     ((or (and i-collapsed
               (eq j (first i-collapsed)))
          (and j-collapsed
               (eq i (first j-collapsed)))
	  (and i-collapsed j-collapsed
	       (eq (first i-collapsed) (first j-collapsed))))
      ;; check only
      (when (equal *zero-PCF* (ipcf-multiply PCF (current-constraint c i j)))
	(record-diagnostic-message j (format nil "~a constrains~% ~a to be unschedulable" i j))
	;; return values (changed unsched) as nil
	(values nil nil)))
     
     ;; if i is collapsed, then place shifted constraint on j
     (i-collapsed
      (define-binary-constraint c (first i-collapsed) j
				(ipcf-shift-time PCF (- (rest i-collapsed)))))
     
     ;; if j is collapsed, then place shifted constraint on i
     (j-collapsed
      (define-binary-constraint c i (first j-collapsed)
				(ipcf-shift-time PCF (rest j-collapsed))))
     
     ;; note that this will be called recursively if both i and j
     ;; are collapsed
     
     ;; not possible case
     (t (error "case not recognized"))
     
     )))

(defmethod FILTER-ARCS-TO-CHECK ((c collapsible-binary-rel-constraint)
				 arc-list)
  
  "filter the input list of arcs ((cons i j)...) where i and j are tasks
    and return a list of equivalent arcs that refer to tasks that are not collapsed
    and which contains no duplicates."
  
  (let ((filtered-arc-list nil))
    (dolist (arc arc-list)
      (let* ((i (first arc))
	     (j (rest arc))
	     (i-collapsed (task-collapsed c i))
	     (j-collapsed (task-collapsed c j)))
	
	(cond 
	  
	  ;; neither collapsed, usual method
	  ((and (not i-collapsed) (not j-collapsed))
	   (push arc filtered-arc-list))
	  
	  ;; if either i collapsed w.r.t. j or vice versa then
	  ;; do nothing, or if both are collapsed with respect to 
	  ;; the same task
	  ((or (and i-collapsed
		    (eq j (first i-collapsed)))
	       (and j-collapsed
		    (eq i (first j-collapsed)))
	       (and i-collapsed
		    j-collapsed
		    (eq (first i-collapsed) (first j-collapsed)))))
	  
	  ((and i-collapsed j-collapsed)
	   (push (cons (first i-collapsed) (first j-collapsed)) filtered-arc-list))
	  
	  ((and i-collapsed (not j-collapsed))
	   (push (cons (first i-collapsed) j) filtered-arc-list))
	  
	  ((and j-collapsed (not i-collapsed))
	   (push (cons i (first j-collapsed)) filtered-arc-list))
	  
	  (t (error "case not recognized")))))
    
    ;; not worth the time to find duplicates for large problems
    (if *VERBOSE-CONSTRAINT-PROP*
	(format *VERBOSE-CONSTRAINT-PROP* "[arcs:~d]" (list-length arc-list)))
    
    ;;    (remove-duplicates filtered-arc-list
    ;;		       :test #'(lambda (x y)
    ;;				 (and (eq (first x) (first y))
    ;;				      (eq (rest  x) (rest  y)))))
    
    filtered-arc-list
    
    ))


(defmethod CHECK-ARC-AND-PROPAGATE
	   ((c collapsible-binary-rel-constraint)
	    (i task) (j task))
  
  (let ((i-collapsed (task-collapsed c i))
        (j-collapsed (task-collapsed c j)))
    
    (cond 
      
      ;; neither collapsed, usual method
      ((and (not i-collapsed) (not j-collapsed))
       (call-next-method))
      
      ;; if either i collapsed w.r.t. j or vice versa then
      ;; do nothing, or if both are collapsed with respect to 
      ;; the same task
      ((or (and i-collapsed
		(eq j (first i-collapsed)))
	   (and j-collapsed
		(eq i (first j-collapsed)))
	   (and i-collapsed
		j-collapsed
		(eq (first i-collapsed) (first j-collapsed)))))
      
      (i-collapsed
       (check-arcs-and-propagate 
	 c (list (cons (first i-collapsed) j) (cons j (first i-collapsed)))))
      
      (j-collapsed
       (check-arcs-and-propagate 
	 c (list (cons i (first j-collapsed)) (cons (first j-collapsed) i))))
      
      (t (error "case not recognized"))
      
      )))


(defmethod STRICT-ORDER-NOGAP :AFTER
  ((c collapsible-binary-rel-constraint)
   task-list &key (propagate nil))
  
  "after defining the constraint, collapse the tasks in the sequence
    with respect to the the first task in the sequence"
  
  (declare (ignore propagate))
  (let ((i (first task-list)))
    
    (dolist (j (rest task-list))
      (let ((offset (tasks-are-collapsible c i j)))
	(when offset
	  (collapse-arc c i j offset))))))

;;; ==================================================================
#|
These methods were moved from TRANS-ORDER during STIF phase 3 work
29 AUG 91

|#

(defmethod TASKS-RELATED-TO-TASK ((c binary-rel-constraint)
				  (i task))

  "Return list of tasks related to the input task via the 
    specified constraint"

  (let ((constrains-list (tasks-affected-via-constraint c i))
        (constrained-by-list (tasks-affecting-via-constraint c i))
        related-tasks
        )
    (setf related-tasks
          (remove-duplicates (append (list i)
                                     constrains-list
                                     constrained-by-list)))
    related-tasks))


(let ((tracking-table (make-hash-table :test #'eq)))
  
  (defmethod ALL-TASKS-RELATED-TO-TASK ((c binary-rel-constraint)
					(i task))
    
    "Return list of all tasks related to the input task
      via the specified constraint"
    
    (clrhash tracking-table)
    (setf (gethash i tracking-table) t)
    (let ((todo nil)
          (changes (list i)))
      (loop
        (setf todo changes)
        (setf changes nil)
        (dolist (task todo)
          (dolist (j (tasks-related-to-task c task))
            (cond ((gethash j tracking-table)
                   nil)
                  (t (setf (gethash j tracking-table) t)
                     (push j changes)))))
        (if (null changes) (return))))
    (let ((result nil))
      (maphash #'(lambda (key val)
                   (declare (ignore val))
                   (push key result))
               tracking-table)
      result)))

#|

;;; **********************************************************************
;;; Windows for displaying chronika's
;;;
;;; eval or compile one of the progn's below (Mac or TI)
;;;
;;; note: suitabilities come from slot-value so that drawing doesn't interfere
;;; with constraint propagation


;;; *********************
;;; ***** TI  WINDOWS ***
;;; *********************

#+IGNORE-ME
(progn 
  ;examples of fun things to do with TI Explorer windows:

  (send *chronika-window* :select)
  (send *chronika-window* :expose)

  (send *chronika-window* :clear-screen)

  (progn
    (send *chronika-window* :expose)
    (send *chronika-window* :clear-screen)
    )

  ;; must force output if not selected
  (w:sheet-force-access
   (*chronika-window* ignore)
   (send *chronika-window* :clear-screen)
   (dotimes (i 10) (format *chronika-window* "hi there turkey!"))  
   (send *chronika-window* :set-cursorpos 100 200)
   (dotimes (i 10) (format *chronika-window* "hi there turkey!"))
   ;; draws white rectangle
   (send *chronika-window* :draw-filled-rectangle 20 60 200 20 w:black)
   ;; 
   (send *chronika-window* :draw-filled-rectangle 25 65 200 20 w:white))
  )

#+:TI
(progn
  
  (defvar *chronika-window* nil)
  (setf *chronika-window* (make-instance 'w:window :save-bits t))
  
  (defun UPDATE-CHRONIKA-PICTURE ()
    (if *chronika-window*
	(window-draw-contents)))
  
  
  (defun UPDATE-TASK-PICTURE (a)
    (if *chronika-window*
	(draw-one-task a)))
  
  
;; (update-chronika-picture)
  
  (defun WINDOW-DRAW-CONTENTS ()
    (send *chronika-window* :select)
    (w:sheet-force-access (*chronika-window* ignore)
      (send *chronika-window* :clear-screen)
      (let ((h 5)
	    (v 0)
	    (delta-v 20)
	    (delta-h 60)
	    (pcf-scale 8)			;pixels for pcf value of 1
	    )
	(if *current-chronika*
	    (dolist (a (task-list *current-chronika*))
	      (incf v delta-v)
	      (send *chronika-window* :set-cursorpos
		    h (- v (send *chronika-window* :line-height)))	;(move-to h v)
	      (format *chronika-window* "~a" (id a))
	      ;;(format t "~%task ~a" (id a))
	      (draw-pcf (+ h delta-h) v (slot-value a 'suitability) pcf-scale)
	      )))))
  
  
  (defun DRAW-ONE-TASK (task-to-draw)
    (let ((h 5)
	  (v 0)
	  (delta-v 20)
	  (delta-h 60)
	  (pcf-scale 8)				;pixels for pcf value of 1
	  )
      (if *current-chronika*
	  (dolist (a (task-list *current-chronika*))
	    (incf v delta-v)
	    (send *chronika-window* :set-cursorpos h v)	; (move-to h v)
	    (when (eq a task-to-draw)
	      (format *chronika-window* "~a" (id a))
	      ;;(format t "~%task ~a" (id a))
	      (draw-pcf (+ h delta-h) v (slot-value a 'suitability) pcf-scale)
	      )))))
  
  
  (defun DRAW-PCF (h v pcf pcf-scale)
    (let* ((x1 h) 
	   (x2 (- (send *chronika-window* :width) 10)) ;; (point-h (window-size))
	   (dx (- x2 x1))
	   (s (start *current-chronika*))
	   (e (end *current-chronika*))
	   (span (- e s))
	   t1 t2 val
	   )
      ;; changed: (erase-rect x1 (- v pcf-scale) x2 v) to
      (send *chronika-window* :draw-filled-rectangle
	    x1 (- v pcf-scale) (- x2 x1) pcf-scale w:white)
      ;; changed: (move-to x1 v) (line-to x2 v) to:
      (send *chronika-window* :draw-line x1 v x2 v)
      (when (not (eq t pcf))
	(dolist (i (ipcf-to-interval-list pcf))
	  (when (not (= 0 (third i)))
	    (setf t1 (max (first i) s))
	    (setf t2 (min (second i) e))
	    (setf t1 (round (+ x1 (* (/ (- t1 s) span) dx))))
	    (setf t2 (round (+ x1 (* (/ (- t2 s) span) dx))))
	    (setf val (round (- v (* (third i) pcf-scale))))
	    ;;(format t "  x ~a to ~a, y ~a to ~a" t1 t2 v val)
	    (cond ((> t2 t1)
		   ;; changed: (paint-rect t1 val t2 v) to
		   (send *chronika-window* :draw-filled-rectangle
			 t1 val (- t2 t1) (- v val))
		   )
		  ((= t1 t2)
		   ;; (move-to t1 val) (line-to t1 v)
		   (send *chronika-window* :draw-line t1 val t1 v)
		   )))))))
  
  )


;;; *********************
;;; ***** MAC WINDOWS ***
;;; *********************

#+:CCL
(progn 

  (defvar *chronika-window* nil)
  (require 'quickdraw)

  (setf *chronika-window*
	(oneof *window*
	       :window-title "Chronika Window"
	       :window-type :document-with-zoom
	       :window-font '("geneva" 9)
	       :close-box-p nil))
  
  (defun update-chronika-picture ()
    (if *chronika-window*
	(ask *chronika-window* (window-draw-contents))))
  
  (defun update-task-picture (a)
    (if *chronika-window*
	(ask *chronika-window* (draw-one-task a))))
  
  ;; (update-chronika-picture)
  
  (defobfun (window-draw-contents *chronika-window*) ()
    (set-window-font '("geneva" 9))
    (erase-rect (make-point 0 0) (window-size))
    (let ((h 5)
	  (v 0)
	  (delta-v 15)
	  (delta-h 40)
	  (pcf-scale 8)				;pixels for pcf value of 1
	  )
      (if *current-chronika*
	  (dolist (a (task-list *current-chronika*))
	    (incf v delta-v)
	    (move-to h v)
	    (format *chronika-window* "~a" (id a))
	    ;;(format t "~%task ~a" (id a))
	    (draw-pcf (+ h delta-h) v (slot-value a 'suitability) pcf-scale)
	    ))
      
      )
    )
  
  (defobfun (draw-one-task *chronika-window*) (task-to-draw)
    (let ((h 5)
	  (v 0)
	  (delta-v 15)
	  (delta-h 40)
	  (pcf-scale 8)				;pixels for pcf value of 1
	  )
      (if *current-chronika*
	  (dolist (a (task-list *current-chronika*))
	    (incf v delta-v)
	    (move-to h v)
	    (when (eq a task-to-draw)
	      (format *chronika-window* "~a" (id a))
	      ;;(format t "~%task ~a" (id a))
	      (draw-pcf (+ h delta-h) v (slot-value a 'suitability) pcf-scale)
	      )))
      
      )
    )
  
  (defobfun (draw-pcf *chronika-window*) (h v pcf pcf-scale )
    (let* ((x1 h) 
	   (x2 (- (point-h (window-size)) 4))
	   (dx (- x2 x1))
	   (s (start *current-chronika*))
	   (e (end *current-chronika*))
	   (span (- e s))
	   t1 t2 val
	   )
      (erase-rect x1 (- v pcf-scale) x2 v)
      (move-to x1 v)
      (line-to x2 v)
      (when (not (eq t pcf))
	(dolist (i (ipcf-to-interval-list pcf))
	  (when (not (= 0 (third i)))
	    (setf t1 (max (first i) s))
	    (setf t2 (min (second i) e))
	    (setf t1 (round (+ x1 (* (/ (- t1 s) span) dx))))
	    (setf t2 (round (+ x1 (* (/ (- t2 s) span) dx))))
	    (setf val (round (- v (* (third i) pcf-scale))))
	    ;;(format t "  x ~a to ~a, y ~a to ~a" t1 t2 v val)
	    (cond ((> t2 t1)
		   (paint-rect t1 val t2 v))
		  ((= t1 t2)
		   (move-to t1 val) (line-to t1 v)))
	    )
	  ))
      )))


;;; **************************
;;; ***** Common Windows *****
;;; **************************

#+:cw-x  ;; functions to plot rel constraints on Common Windows
(progn
  
  (defun MOVE-TO (h v)
    "Move the cursor to the specified position.  Origin is lower left."
    (setf (window-stream-x-position *chronika-window*) h)
    (setf (window-stream-y-position *chronika-window*) v))
  
  (defvar *CHRONIKA-WINDOW* nil  "The window to display the pcf.")
  
;;; The following vars are only used to create *chronika-window*.
;;; The window functions use the accessors (window-stream-inner-whatever)
;;; to determine the window size.
  (defvar *chw-left*    50 "Chronika window, left offset.")
  (defvar *chw-bottom* 500 "Chronika window, bottom offset.")
  (defvar *chw-width* 1000 "Chronika window, width offset.")
  (defvar *chw-height* 300 "Chronika window, height offset.")
  (defvar *chw-accessor* #'(lambda(task)(constraint-rel-pcf *current-rel-constraint* task))
    "Function returning the pcf to plot.")
  (defvar *chw-intervals* 10 
    "Number of intervals in pcf plot.  Equivalent to one less than the
number of tick marks.")
  
  (initialize-common-windows)
  (progn 
    (setf *chronika-window* (make-window-stream :left *chw-left*
						:bottom *chw-bottom*
						:width  *chw-width*
						:height *chw-height*
						:title "Chronika Window"
						:activate-p t))
    (setf (window-stream-truncate-lines-p *chronika-window*) t))
  
  
  
  (defun UPDATE-CHRONIKA-PICTURE ()
    (when *chronika-window*
      (window-draw-contents)))
  
  
  (defun UPDATE-TASK-PICTURE (a)
    (when *chronika-window*
      (draw-one-task a)))
  
  ;; (update-chronika-picture)
  
  (defun WINDOW-DRAW-CONTENTS ()
    (activate *chronika-window*)
    (clear *chronika-window*)
    (reset *chronika-window*)
    (draw-one-task nil)			;draw all tasks
    (draw-x-axis)
    )
  
  #+:IGNORE-ME
  (progn
    (activate *chronika-window*)
    (clear *chronika-window*)
    (reset *chronika-window*)
    (setf (restriction-time a) 0)
    (draw-one-task a :accessor #'(lambda(x)
				   (compute-suitability a)))
    (draw-x-axis)
    (draw-one-task b :accessor #'(lambda(x)(constraint-rel-pcf *current-rel-constraint* x)))
    (draw-one-task c :accessor #'(lambda(x)(constraint-rel-pcf *current-rel-constraint* x)))
    (draw-one-task d :accessor #'(lambda(x)(constraint-rel-pcf *current-rel-constraint* x)))
    )
  
  (defun ORDERER-DRAW (reverse-list  unordered &key (title "Chronika Window"))
    (activate *chronika-window*)
    (clear *chronika-window*)
    (reset *chronika-window*)
    (write-title *chronika-window* title)
    (draw-x-axis)
    (do* ((tasks (reverse reverse-list) (cdr tasks))
	  (task (first tasks) (first tasks))
	  (end-time (minimum-duration task) (+ end-time (if task
							    (minimum-duration task)
							  0))))
	((null tasks))
      (restrict-to-earliest-start task)
      (draw-one-task task :accessor #'(lambda(x)
					(compute-suitability task))))
    (dolist (task unordered)
      (draw-one-task task :accessor #'(lambda(x)
					(constraint-rel-pcf *current-rel-constraint* x)))))
  
  
  (defun DRAW-ONE-TASK (task-to-draw &key (accessor *chw-accessor*))
    (let ((h 5)
	  (v (window-stream-inner-height *chronika-window*))
	  (delta-v 20)
	  (delta-h 60)
	  (pcf-scale 6)			;pixels for pcf value of 1
	  )
      (when *current-chronika*
	(dolist (a (task-list *current-chronika*))
	  (decf v delta-v)
	  (move-to h v)
	  (when (or (eq task-to-draw nil)
		    (eq task-to-draw a))
	    (format *chronika-window* "~a" (id a))
	    (draw-pcf (+ h delta-h) v (funcall accessor a) pcf-scale a)
	    )))))
  
  
  (defun PLOT-RANGE (pos1 pos2 color)
    "Draw a filled rectangle bounded by pos1 and pos2.  
If the x coords or the y coords are equal, draw a line."
    (cond ((or (= (position-x pos1) (position-x pos2))
	       (= (position-y pos1) (position-y pos2)))
	   (draw-line *chronika-window* pos1 pos2 :color color))
	  (t
	   (draw-filled-rectangle *chronika-window*
				  pos1 
				  (- (position-x pos2) (position-x pos1))
				  (- (position-y pos2) (position-y pos1))
				  :color color))))
  
  
  (defun DRAW-PCF (h v pcf pcf-scale task)
    (let* ((right-margin 10)
	   (x1 h)
	   (x2 (- (window-stream-inner-width *chronika-window*) right-margin))
	   (dx (- x2 x1))
	   (s (start *current-chronika*))
	   (e (end *current-chronika*))
	   (span (- e s))
	   t1 t2 val 
	   early-end late-end early-end-pixels late-end-pixels
	   )
      (draw-filled-rectangle *chronika-window* 
			     (make-position :x x1 :y v)
			     (- x2 x1) (* 2 pcf-scale)
			     :color (window-stream-background-color *chronika-window*))
      (draw-line *chronika-window* 
		 (make-position :x x1 :y v)
		 (make-position :x x2 :y v))
      (when (not (eq t pcf))
	(dolist (i (ipcf-to-interval-list pcf))
	  (when (not (= 0 (third i)))
	    (setf t1 (max (first i) s))
	    (setf t2 (min (second i) e))
	    (setf early-end (max (+ (first i) (minimum-duration task)) s))
	    (setf late-end (min (+ (second i) (maximum-duration task)) e))
	    (setf t1 (round (+ x1 (* (/ (- t1 s) span) dx))))
	    (setf t2 (round (+ x1 (* (/ (- t2 s) span) dx))))
	    (setf early-end-pixels (round (+ x1 (* (/ (- early-end s) span) dx))))
	    (setf late-end-pixels (round (+ x1 (* (/ (- late-end s) span) dx))))
	    (setf val (round (* (third i) pcf-scale)))
	    ;;(format t "  x ~a to ~a, y ~a to ~a" t1 t2 v val)
	    (plot-range (make-position :x t1 :y v)
			(make-position :x t2 :y (+ v val))
			red)
	    (plot-range (make-position :x early-end-pixels :y (+ v val))
			(make-position :x late-end-pixels :y (+ v val val))
			(window-stream-foreground-color *chronika-window*)))))))
  
  
  (defun DRAW-X-AXIS (&key (first-tick-value 0)
			   (delta-tick (round (/ (- (end *current-chronika*)
						    (start *current-chronika*))
						 *chw-intervals*))) ;num-ticks - 1
			   axis-left
			   axis-bottom
			   )
    (let ((h 5)
	  (v (window-stream-inner-height *chronika-window*))
	  (delta-v 20)
	  (delta-h 60)
	  (right-margin 10)
	  (start (start *current-chronika*))
	  (end (end *current-chronika*))
	  )
      (unless axis-left
	(setf axis-left (+ h delta-h)))
      (unless axis-bottom
	(setf axis-bottom (- v (* (1+ (length (task-list *current-chronika*)))
				  delta-v))))
      (draw-filled-rectangle *chronika-window*
			     (make-position 
			      :x axis-left
			      :y (- axis-bottom 3 (window-stream-linefeed-height *chronika-window*)))
			     (- (window-stream-inner-width *chronika-window*) axis-left)
			     (+ (window-stream-linefeed-height *chronika-window*) 7)
			     :color (window-stream-background-color *chronika-window*))
      (draw-line *chronika-window* 
		 (make-position :x axis-left :y axis-bottom)
		 (make-position :x (- (window-stream-inner-width *chronika-window*)
				      right-margin)
				:y axis-bottom))
      (do ((tick-value first-tick-value (+ tick-value delta-tick))
	   (tick-count 0 (1+ tick-count)))
	  ((> tick-value (end *current-chronika*)))
	(let ((x-coord  (+ axis-left (* (/ (- (max tick-value start) start)
					   (- end start))
					(- (window-stream-inner-width *chronika-window*)
					   axis-left
					   right-margin)))))
	  (draw-line *chronika-window*
		     (make-position :x x-coord :y (- axis-bottom 3))
		     (make-position :x x-coord :y (+ axis-bottom 3)))
	  (move-to x-coord (- axis-bottom 3 (window-stream-linefeed-height *chronika-window*)))
	  (format *chronika-window* "~a" tick-value)
	  ))))
  )

; Common Windows test
#+:cw-x
(defun common-windows-test ()
  (make-chronika :name "Test3")
  (setf e1 (make-idb-exposure-task :minimum-duration 10 :idb-exposure-id "e1" :parallel-p nil))
  (setf e2 (make-idb-exposure-task :minimum-duration 10 :idb-exposure-id "e2" :parallel-p nil))
  (setf e3 (make-idb-exposure-task :minimum-duration 50 :idb-exposure-id "e3" :parallel-p nil))
  (setf e4 (make-idb-exposure-task :minimum-duration 10 :idb-exposure-id "e4" :parallel-p nil))
  (setf *current-rel-constraint* (make-binary-rel-constraint :test3))
  (strict-order *current-rel-constraint* (list e1 e2) :propagate t)
  (after-order *current-rel-constraint* e1 e3 25 10 :propagate t)
  (after-order *current-rel-constraint* e1 e4 60 10 :propagate t)
  (strict-order *current-rel-constraint* (list e2 e4) :propagate t)

  
  (unless (equal (pred-order-by-constraint-strictness-alpha
		  *current-rel-constraint* (list e1) (list e2 e3) (list e2 e3 e4))
		(list e2))
    (error "Ordered returned unexpected value."))
  (setf (restriction-time e1) 0)
  (orderer-draw (list e1) (list e2 e3 e4)
		:title "Test 3.  order= 1, 2(strict), 3(strict), 4"))


|#

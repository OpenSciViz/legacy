;;;
;;; EARTH-POLY-REGION ""                                                [CLASS]
;;;    SAA regions (or other polygonal regions defined on earth surface)
;;;    Superclasses
;;;    None.
;;;    Initialization Arguments
;;;    The :name argument is a 
;;;    The :point-list argument is a 
;;;    Readers
;;;    Writers
;;;
;;; PRINT-OBJECT ((obj earth-poly-region) stream)                      [METHOD]
;;;
;;; MAKE-EARTH-POLY-REGION (&key (name "???") point-list)            [FUNCTION]
;;;    Make an instance of an EARTH-POLY-REGION class.  Input arguments are:
;;;       :name <string> - print name
;;;       :point-list ((long1 lat1)(long2 lat2)...) - list of longitude
;;;    latitude pairs in decimal degrees specifying vertices of region on
;;;    earth's surface.
;;;       Returned value is instance of earth-poly-region
;;;
;;; SET-DERIVED-EARTH-POLY-REGION-QUANTITIES ((epr                     [METHOD]
;;;                                           earth-poly-region))
;;;    Calculate derived quantities for earth-poly-region.  It is assumed
;;;       that the point-list slot contains the list of vertex points.
;;;
;;; DESCRIBE-EARTH-POLY-REGION ((epr earth-poly-region)                [METHOD]
;;;                             &key (stream t))
;;;    Send readable description of EARTH-POLY-REGION to an output stream.
;;;       Default is *standard-output*.
;;;
;;; POINT-IN-EARTH-POLY-REGION ((epr earth-poly-region) vect)          [METHOD]
;;;    Return t if vect is within region.  Vect is in earth frame i.e. z
;;;    along north pole, x towards Greenwich on equator, y makes RHS
;;;
;;; OBJECT-IN-EARTH-POLY-REGION ((epr earth-poly-region)               [METHOD]
;;;                              (ao astronomical-object) jd)
;;;    Return t if object is in earth poly region at time, nil otherwise.
;;;
;;; *SAA* (nil)                                                      [VARIABLE]
;;;
;;; TABULATE-REGION-ENTRY-EXIT ((leos low-earth-orbit-satellite)       [METHOD]
;;;                             (epr earth-poly-region)
;;;                             &key (steps 360) (debug nil))
;;;    Create a table on *standard-output* of region (e.g. SAA) entry, 
;;;       exit times (in days relative to ascending node), and earth
;;;    longitude & latitude of entry/exit (in decimal degrees.  Table is
;;;    keyed by longitude of ascending node.  Orbit model is successively
;;;    modified by changing the time of asc node by the number of steps
;;;    specified 
;;;
;;; REGION-ENTRY-EXIT-WRT-ASC-NODE ((leos low-earth-orbit-satellite)   [METHOD]
;;;                                 (epr earth-poly-region)
;;;                                 &key (steps-per-orbit 100)
;;;                                 (tolerance 1.0e-5) (debug nil))
;;;    Compute and return times of entry/exit of low-earth-orbit-satellite
;;;    in/out of polygonal region on earth surface.  The orbit is checked
;;;    from the asc node for one nodal period, broken into steps-per-orbit
;;;    samples. Five values are returned:
;;;        long-asc-node - longitude of the orbit ascending node, degrees
;;;        entry-time - days, from time of asc node
;;;        entry-long-lat - list (longitude latitude) of satellite position
;;;    above earth at time of entry.  In degrees.
;;;        exit-time - days, from time of asc node
;;;        exit-long-lat - list (longitude latitude) of satellite position
;;;    above earth at time of exit.  In degrees.
;;;       Note:  last four returned values will be NIL if there is no
;;;    entry/exit in the orbit.
;;;
;;; BISECT-FOR-REGION-ENTRY-EXIT ((leos low-earth-orbit-satellite)     [METHOD]
;;;                               (epr earth-poly-region) &key t1 in1
;;;                               t2 in2 (tolerance 1.0e-5))
;;;    Refine estimate of polygonal region entry/exit times.  Input args
;;;       are:
;;;       LEOS - low-earth-orbit-satellite object instance
;;;       EPR - earth poly region instance
;;;       t1 and t2 are TJD times bounding entry or exit,
;;;       and in1 and in2 are flags indicating whether LEOS is in (t) or
;;;    out (nil) of region at t1 and t2, respectively.
;;;       It must be known that satellite is in region at t1 and out at t2,
;;;    or vice versa.  Further, t1 must be < t2, and in1 and in2 must be
;;;       either t/nil or nil/t.  Bisection method it used, to a time
;;;    tolerance specified (in days).  Returned value is time of entry/exit,
;;;    as TJD. 
;;;
;;; *SAA-ENTRY-EXIT-TABLE* (nil)                                     [VARIABLE]
;;;
;;; SAA-PCF ((leos low-earth-orbit-satellite) jd1 jd2                  [METHOD]
;;;          &key (table *saa-entry-exit-table*))
;;;    Return PCF valued 1 when not in SAA, 0 when in SAA, over the
;;;    specified time range.  A table for interpolation must be specified.
;;;
;;; POSSIBLE-SAA-TIMES ((leos low-earth-orbit-satellite) jd1 jd2       [METHOD]
;;;                     latitude)
;;;    Return list of start end times when the satellite can potentially
;;;    be in the SAA, i.e. when it's latitude is below the specified
;;;    angle (radians) Latitude must be zero or negative, i.e. in the
;;;    southern hemisphere. 
;;;
;;; PROBABILITY-NOT-IN-SAA ((leos low-earth-orbit-satellite) jd1 jd2)  [METHOD]
;;;    Return PCF valued as probability that low earth orbit satellite is
;;;    not in the SAA
;;;
;;; SIMPLE-SAA-PCF ((leos low-earth-orbit-satellite) jd1 jd2           [METHOD]
;;;                 &key (step 0.01))
;;;    Compute PCF valued 0 when satellite is in the SAA, 1 otherwise.
;;;       Step is the fraction of the orbital period used to check for
;;;       SAA presence.  The model is a simple circular region.
;;;

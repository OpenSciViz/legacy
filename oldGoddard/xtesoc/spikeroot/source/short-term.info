;;;
;;; FDSI-EARLY-START (fdsi)                                          [FUNCTION]
;;;
;;; SET-FDSI-EARLY-START (fdsi value)                                [FUNCTION]
;;;
;;; (SETF FDSI-EARLY-START) set-fdsi-early-start                 [SETF MAPPING]
;;;
;;; FDSI-EARLY-FINISH (fdsi)                                         [FUNCTION]
;;;
;;; SET-FDSI-EARLY-FINISH (fdsi value)                               [FUNCTION]
;;;
;;; (SETF FDSI-EARLY-FINISH) set-fdsi-early-finish               [SETF MAPPING]
;;;
;;; FDSI-LATE-START (fdsi)                                           [FUNCTION]
;;;
;;; SET-FDSI-LATE-START (fdsi value)                                 [FUNCTION]
;;;
;;; (SETF FDSI-LATE-START) set-fdsi-late-start                   [SETF MAPPING]
;;;
;;; FDSI-LATE-FINISH (fdsi)                                          [FUNCTION]
;;;
;;; SET-FDSI-LATE-FINISH (fdsi value)                                [FUNCTION]
;;;
;;; (SETF FDSI-LATE-FINISH) set-fdsi-late-finish                 [SETF MAPPING]
;;;
;;; FDSI-DURATION (fdsi)                                             [FUNCTION]
;;;
;;; SET-FDSI-DURATION (fdsi value)                                   [FUNCTION]
;;;
;;; (SETF FDSI-DURATION) set-fdsi-duration                       [SETF MAPPING]
;;;
;;; FDSI-VAL-ES (fdsi)                                               [FUNCTION]
;;;
;;; SET-FDSI-VAL-ES (fdsi value)                                     [FUNCTION]
;;;
;;; (SETF FDSI-VAL-ES) set-fdsi-val-es                           [SETF MAPPING]
;;;
;;; FDSI-VAL-LS (fdsi)                                               [FUNCTION]
;;;
;;; SET-FDSI-VAL-LS (fdsi value)                                     [FUNCTION]
;;;
;;; (SETF FDSI-VAL-LS) set-fdsi-val-ls                           [SETF MAPPING]
;;;
;;; MAKE-FDSI (&key early-start early-finish late-start late-finish  [FUNCTION]
;;;            duration val-es val-ls)
;;;    Make an FDSI object with specified field values. Returned value is
;;;    FDSI. 
;;;
;;; MAKE-FDSIA (&key interval-list interval-type (min-duration nil)  [FUNCTION]
;;;             (max-duration nil) (specified-duration nil))
;;;    make an FDSIA = array of FDSI.
;;;       :interval-list - list of (t1 t2 [dur])
;;;         where t1 is early start of interval, t2 is latest start or end 
;;;         (depending on the keyword arg :interval-type)
;;;         and dur is duration if scheduled in the interval
;;;         All must be fixnums.  Duration may be omitted and will be
;;;    ignored if :specified-duration argument is present.
;;;         Intervals must be increasing time order by t1
;;;        :interval-type - must be one of :START-START or :START-END
;;;         if :start-start then t1 and t2 are interpreted as early and
;;;    late starts if :start-end then t1 is early start and t2 is late end
;;;         no default - this must be specified.
;;;        :min-duration - nil or number s.t. intervals where the duration
;;;         is < min-duration are excluded from the FDSIA
;;;        :max-duration - nil or number s.t. intervals where the duration
;;;    is > max-duration are excluded from the FDSIA
;;;        :specified-duration - if a number, this overrides any dur in the
;;;    interval list.  Default is nil and the duration must be present
;;;         in the intervals.
;;;        Note that duration for min-duration and max-duration is the dur
;;;    value in the interval (t1 t2 dur), not (- t2 t1) or some other
;;;    quantity. returned value is FDSIA corresponding to input interval
;;;    list, or nil if no viable intervals exist.
;;;
;;; FDSIA-EARLIEST-START (fdsia)                                     [FUNCTION]
;;;    return earliest start time of any interval in FDSIA
;;;
;;; FDSIA-EARLIEST-FINISH (fdsia)                                    [FUNCTION]
;;;    return earliest finish time of any interval in FDSIA
;;;
;;; FDSIA-LATEST-START (fdsia)                                       [FUNCTION]
;;;    return latest start time of any interval in FDSIA
;;;
;;; FDSIA-LATEST-FINISH (fdsia)                                      [FUNCTION]
;;;    return latest finish time of any interval in FDSIA
;;;
;;; FDSIA-FIRST-VAL (fdsia)                                          [FUNCTION]
;;;    return value corresponding to earliest start in earliest interval of
;;;    FDSIA 
;;;
;;; FDSIA-LAST-VAL (fdsia)                                           [FUNCTION]
;;;    return value corresponding to latest start in latest interval of
;;;    FDSIA 
;;;
;;; FDSIA-VAL-COUNT (fdsia)                                          [FUNCTION]
;;;    return number of values (start times) in FDSIA
;;;
;;; FDSIA-TIME-TO-VAL (fdsia time)                                   [FUNCTION]
;;;    Return multiple values:  
;;;       first is val corresponding to start time, nil if no
;;;    correspondence second is FDSI in which start time falls
;;;
;;; FDSIA-TIME-TO-NEXT-VAL (fdsia time)                              [FUNCTION]
;;;    Given an early-start time, return one of the following:
;;;       If time corresponds to a specific val, return it
;;;       Otherwise return the next val after time if there is one; if not
;;;    return nil 
;;;
;;; FDSIA-TIME-TO-PREV-VAL (fdsia time)                              [FUNCTION]
;;;    Given an early-start time, return one of the following:
;;;       If time corresponds to a specific val, return it
;;;       Otherwise return the first val before time if there is one; if
;;;    not return nil 
;;;
;;; FDSIA-TIME-RANGE-TO-VAL-RANGE (fdsia time-range)                 [FUNCTION]
;;;    Input is a start time range (t1 t2), return value range (v1 v2) 
;;;       s.t. all values corresponding to times between t1 and t2
;;;    inclusive are between v1 and v2 inclusive.  Return nil if the times
;;;    are out of range of the FDSIA.
;;;
;;; FDSIA-TIME-RANGE-LIST-TO-VAL-RANGE-LIST (fdsia time-range-list)  [FUNCTION]
;;;    Input is a start time range list ((t1 t2)(t3 t4)...), 
;;;       return corresponding value range list ((v1 v2) (v3 v4)...)
;;;       s.t. all values corresponding to times between any t(i) and
;;;    t(i+1) inclusive are between some pair of values inclusive.
;;;       Return nil if the times are out of range of the FDSIA.
;;;
;;; FDSIA-VAL-TO-TIME (fdsia val)                                    [FUNCTION]
;;;    return multiple values start-time end-time corresponding to
;;;       val, nil if no correspondence
;;;
;;; FDSIA-MIN-DURATION (fdsia)                                       [FUNCTION]
;;;    return min duration for all FDSIs in FDSIA
;;;
;;; FDSIA-MAX-DURATION (fdsia)                                       [FUNCTION]
;;;    return max duration for all FDSIs in FDSIA
;;;
;;; FDSIA-DURATION-GIVEN-START-AT (fdsia time)                       [FUNCTION]
;;;    Return duration if started at time, nil if time is not a valid start
;;;
;;; FDSIA-DURATION-GIVEN-END-AT (fdsia time)                         [FUNCTION]
;;;    Return duration if ends at time, nil if time is not a valid end
;;;
;;; FDSIA-START-GIVEN-END-AT (fdsia time)                            [FUNCTION]
;;;    return end time if started at time, or nil if time is not a valid end
;;;
;;; FDSIA-END-GIVEN-START-AT (fdsia time)                            [FUNCTION]
;;;    return start time if ended at time, or nil if time is not a valid
;;;    start 
;;;
;;; FDSI-START-RANGE-EXCLUDED-BY-OVERLAP (fdsi t1 t2)                [FUNCTION]
;;;    return multiple values start-val end-val excluded by overlap, nil for
;;;       both if no overlap
;;;
;;; FDSIA-OVERLAP-EXCLUSION (fdsia t1 t2)                            [FUNCTION]
;;;    Return list of value ranges for which start of end of a task can
;;;       overlap the interval t1 through t2
;;;
;;; DO-FOR-EACH-TIME-IN-FDSIA (fdsia &key time value duration fdsi      [MACRO]
;;;                            body)
;;;    Execute body once for each discrete start time in FDSIA.
;;;        :time, :value, and :duration are symbols bound to corresponding
;;;    time, value, and duration if started at time.  They are optional:
;;;    only those referenced in body need be supplied.
;;;        :FDSI is optional: if present, it is a symbol bound to the
;;;    current FDSI :body is the form to execute
;;;
;;; DO-FOR-EACH-VALUE-IN-FDSIA (fdsia &key value start-time end-time    [MACRO]
;;;                             duration fdsi body)
;;;    Execute body once for each discrete value in FDSIA.
;;;       :value is symbol bound to value
;;;       :start-time and :end-time are symbols bound to times
;;;    corresponding to value :duration is symbol is bound to duration
;;;       :FDSI is a symbol bound to the current FDSI
;;;       All are optional: only those required in body need be supplied.
;;;       :body is the form to execute
;;;
;;; FDSIA-MAP-TO-INTERVAL-LIST (fdsia interval-list)                 [FUNCTION]
;;;    Input is an FDSIA and an interval-list ((t1 t2 suit)..) such as
;;;       generated by ipcf-to-interval-list.
;;;       Two values are returned:  
;;;        - a list of suit values corresponding to each start time
;;;          in the FDSIA.  This can be used to set preferences in the CSP.
;;;        - an interval list covering the times and values of the list
;;;    above. Only legal start times are included in the intervals returned.
;;;       If there are insufficient intervals in the interval-list, nil is
;;;       returned and a warning is emitted.
;;;
;;; FDSIA-MAP-AVERAGE-TO-INTERVAL-LIST (fdsia interval-list)         [FUNCTION]
;;;    Input is an FDSIA and an interval-list ((t1 t2 suit)..) such as
;;;       generated by ipcf-to-interval-list.
;;;       Two values are returned:  
;;;        - a list of suit values corresponding to the AVERAGE suitability
;;;          for each start time in the FDSIA.  This can be used to set
;;;    preferences in the CSP.
;;;        - an interval list covering the times and values of the list
;;;    above. Only legal start times are included in the intervals returned.
;;;       If there are insufficient intervals in the interval-list, nil is
;;;       returned and a warning is emitted.
;;;
;;; AVERAGE-OVER-CURRENT-AND-FUTURE-INTERVALS (start end             [FUNCTION]
;;;                                            current-interval
;;;                                            remaining-intervals)
;;;    Intervals (current and list of remaining intervals)
;;;       are of form (t1 t2 value), asc order and non-overlapping.  
;;;       Get average value over the period start to end inclusive.  
;;;       start is in the range t1 <= start <= end.  Second value
;;;       returned is number of values in intervals that were averaged.
;;;
;;; ARRAY-FIND-FIRST-GREATER-THAN-OR-EQUAL (array value key)         [FUNCTION]
;;;    input array and key function which can be applied to an array
;;;       element and which returns a numeric value.  Array must be
;;;       in STRICTLY ascending order by value returned by key.  
;;;       Find first element (i.e. with smallest index) with 
;;;        (funcall key element) >= value.
;;;       Return multiple values:  array-element and index, or both 
;;;       nil if not in range
;;;
;;; ARRAY-FIND-LAST-LESS-THAN-OR-EQUAL (array value key)             [FUNCTION]
;;;    input array and key function which can be applied to an array
;;;       element and which returns a numeric value.  Array must be
;;;       in STRICTLY ascending order by value returned by key.  
;;;       Find last element (i.e. with largest index) with 
;;;        (funcall key element) <= value.
;;;       Return multiple values:  array-element and index, or both 
;;;       nil if not in range
;;;
;;; LAYDOWN (task forward remaining-dur t0 i-list min-seg-dur        [FUNCTION]
;;;          max-seg-sep seg-overhead reverse-done-segs
;;;          reverse-i-list cum-overhead)
;;;
;;; LAYDOWN-ALL-TEST (&key dur min-seg-dur rel-dur-adjust            [FUNCTION]
;;;                   abs-dur-adjust intervals
;;;                   (default-dur :nominal))
;;;
;;; LAYDOWN-ALL (task dur intervals min-seg-dur rel-dur-adjust       [FUNCTION]
;;;              abs-dur-adjust &key (details nil)
;;;              (default-dur :min) (verbose nil))
;;;    Args:
;;;       dur - nominal duration (required)
;;;       intervals - list of (start end) intervals within which task must
;;;        be scheduled
;;;       min-seg-dur - minimum duration of any segment
;;;       rel-dur-adjust - fraction of dur which may be added or subtracted
;;;        to/from nominal duration, nil if not applicable
;;;       abs-dur-adjust - time which may be added/subtracted to/from
;;;    nominal duration, nil if not applicable
;;;        If both rel and abs adjustments are provided, then whichever
;;;    gives the largest potential adjustment is used.
;;;    --keyword args--
;;;       :details - if t, return list of scheduled intervals for each
;;;    start time as a list of 'specifications' in the form:
;;;         (spec1 spec2 ...)
;;;        where a speci is (t1 t2 ((s1 e1)(s2 e2)...))
;;;         where  t1 and t2 are early start and late start, and 
;;;         (s e)(s e)... lists are start and end times of scheduled
;;;    intervals given start times at t1.  Must be adjusted for start times
;;;    >t1 and <= t2. Default is nil (no details).
;;;       :default-dur - which duration to use for nominal scheduling. 
;;;    Legal values are  :min, :nominal, or :max.
;;;       
;;;       All times are integral and in the same units.  Adjustment times
;;;    are calculated then rounded.
;;;       Duration will be extended or shortened within limits above to
;;;    improve efficiency.
;;;    Returned multiple values:
;;;       dur-list - list of (t1 t2 d) s.t. task has duration d if 
;;;        started and ended between t1 and t2 inclusive (this is suitable 
;;;        for the interval-list for MAKE-FDSIA of type :start-end)
;;;       eff-list - list of (t1 t2 eff) s.t. task has efficiency eff if
;;;        started and ended between t1 and t2 inclusive
;;;       detailed-schedule-results - see details argument above for format
;;;
;;; SCHED-INTERVALS-FROM-LAYDOWN (start laydown-list)                [FUNCTION]
;;;    input is the detail list returned from LAYDOWN-ALL and a start time.
;;;       return a list of actual scheduled intervals for the specified
;;;    start. return nil if start is not legal.
;;;
;;; LAYDOWN-FORWARD-ADJUST-FOR-EFFICIENCY (&key task dur dur-min     [FUNCTION]
;;;                                        dur-max min-seg-dur t0
;;;                                        i-list (verbose nil)
;;;                                        (default-dur :nominal))
;;;    Function to lay out a task in detail in the intervals in i-list
;;;       and adjust the duration between dur-min and dur-max to maximize
;;;       efficiency.  
;;;       Arguments:
;;;        :task - identifier only, not referenced
;;;        :dur - nominal duration
;;;        :dur-min - min allowed duration
;;;        :dur-max - max allowed duration
;;;        :min-seg-dur - min size of scheduled segment
;;;        :t0 - start time for scheduling
;;;        :i-list - list of intervals ((t1 t2)(t3 t4)...) within which
;;;    task must be scheduled
;;;        :verbose - print to standard output if t
;;;        :default-dur can be :min, :max, or :nominal to determine which
;;;    duration duration is used when seeing whether task fits.  If not one
;;;    of the above three values, :nominal is used with no warning or error.
;;;

;;; xte-autolts.lisp
;;; To be called from XTE SPIKE Wrapper.
;;; Expects a file $SPIKEROOT/autosts-params.lisp defining variables
;;; segnum (bin # to be scheduled), sched-count (number of schedules to
;;; be produced) and sts-strategy (symbol for short term strategy to
;;; be used.
;;; 
;;; Iterates sched-count times: Load the schedule in sched/auto.sched 
;;; Run given short-term strategy. Print report.
;;; Designate last strategy as "SELECTED" and plot it.

(load "source/xte-build-spike")
(load-orbit-model "xte")

;;; clear out old saved-scheds file.
(with-open-file (stream "sched/saved-scheds.current"
		 :direction :output
		 :if-exists :supersede
		 :if-does-not-exist :create)
)

(load "autosts-params")  ;; get segnum, strategy, sched-count, init sched name.
(load (concatenate 'string
	"sched/" init-sched-root ".sched"))
(do ((schedroot))
    ((equal sched-count 0) nil)
  (setf schedroot (concatenate 'string "sts" (princ-to-string segnum)))
  (restore-xte-schedule-from-file init-sched-root)
  (recreate-xte-schedule)
  (setf sched-count (- sched-count 1))
  (make-xte-short-term-schedule segnum)
  (funcall sts-strategy *XTE-SHORT-TERM-SCHEDULE*)
  (save-xte-schedule-to-file :root schedroot)
  (save-xte-roster-to-file :occ *XTE-SHORT-TERM-SCHEDULE*
			   :evt-file-name schedroot)
  (with-open-file (stream "sched/saved-scheds.current"
		   :direction :output
		   :if-exists :append
		   :if-does-not-exist :create)
    (if (equal 0 sched-count)
	(progn
	  (format stream "SELECTED ~a~%" schedroot)
	  (generate-XTE-csp-plot-short-term
	   (concatenate 'string
	     (princ-to-string sts-strategy) "_"
	     (princ-to-string (+ 1 sched-count)) "b"
	     (princ-to-string schedroot) ".ps"))
	  )))
  (with-open-file (strm (concatenate 'string
			     (princ-to-string sts-strategy)
			   "_"
			   (princ-to-string (+ 1 sched-count))
			   ".week"
			   (princ-to-string schedroot))
		   :direction :output
		   :if-exists :supersede
		   :if-does-not-exist :create)
		   
  (schedule-report (obs-csp *XTE-SHORT-TERM-SCHEDULE*)
		   :stream strm))) 

(exit)



;;; xte-autolts.lisp
;;; To be called from XTE SPIKE Wrapper SPIKELongTermScheduler::schedule().
;;; Wrapper will have written the input file sched/auto.sched.

(load "source/xte-build-spike")
;;(load "source/raw-lts-report")

;;(setf *XTE-SCHEDULE-START*  (dmy-to-time '1-jan-1996))
;;(setf *XTE-SCHEDULE-END*  (dmy-to-time '24-jun-1996))
;;(setf *XTE-SEGMENT-START*  (dmy-to-time '1-jan-1996))
;;(setf *XTE-SEGMENT-COUNT* 25)

(load-orbit-model "xte")
(load "sched/auto.sched")
(restore-xte-schedule-from-file "auto")
(recreate-xte-schedule)
(report-xte-schedule-times)

;;; clear out old saved-scheds file.
(with-open-file (stream "sched/saved-scheds.current"
		 :direction :output
		 :if-exists :supersede
		 :if-does-not-exist :create)
)

(early-greedy-schedule *XTE-LONG-TERM-SCHEDULE*)
(save-xte-schedule-to-file :root "lts-select")
(with-open-file (stream "sched/saved-scheds.current"
		 :direction :output
		 :if-exists :append
		 :if-does-not-exist :create)
  (format stream "SELECTED lts-select~%"))

#|
(with-open-file (stream "report/raw_lts.rpt"
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)
  (xsoc-schedule-report (obs-csp *XTE-LONG-TERM-SCHEDULE*) :stream stream))
|#
(exit)

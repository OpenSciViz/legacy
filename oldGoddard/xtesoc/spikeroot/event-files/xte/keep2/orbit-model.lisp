;; MIT SPIKE Release
;; File Name $RCSfile: orbit.model,v $
;; Revision: $Revision: 3.1 $
;; Checked in on: $Date: 1994/09/07 18:48:04 $
;; $Log: orbit.model,v $
# Revision 3.1  1994/09/07  18:48:04  buehler
# Circular model now assigned to *XTE-CIRCULAR* rather than *XTE*.
#
# Revision 3.0  1994/09/07  18:41:59  buehler
# Version received from STI.
#
;;;Revision 1.3  1992/12/15  15:44:36  buehler
;;;Extended time covered by orbit model to 1-1-97
;;;
;;;Revision 1.2  1992/12/15  12:34:01  ehm
;;;fixed removed bogus RCS header
;;;
;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; XTE (from EUVE parameters Spring '91)
;;; MDJ
;;;

;;; Note that dates are dates covered by the suite of orbit
;;; models - a version of *XTE* for each week's ephemeris
;;; and a circular approximation for the AO Perid in *XTE-CIRCULAR*
(setf *current-orbit-model*    "XTE"
      *orbit-model-start-date*  (dmy-to-time '1-jan-96)
      *orbit-model-end-date*    (dmy-to-time '1-jan-97)
      )
#|
;;; If circular parameters have been loaded already somehow,
;;; stick with those. Otherwise load in these defaults.

(if (null (inclination *XTE-CIRCULAR*))
(update-orbit-parameters
 *XTE-CIRCULAR*
 :inclination       23                      ;deg
 :time-asc-node     6099.4902547   ;TJD or date
 :RA-asc-node       0                         ;deg
 :regression-rate   -6.695788                      ;deg/day
 :period            96.3429                    ;minutes
 :semimajor-axis    (+ 6378.14 600)           ;km
 ))

|#



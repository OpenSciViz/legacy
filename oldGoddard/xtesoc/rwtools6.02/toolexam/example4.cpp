/*
 * Example 4: class BinaryTree, storing and retrieving collectable strings
 *
 * $Id: example4.cpp,v 2.2 1993/10/19 14:35:26 griswolf Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example4.cpp,v $
 * Revision 2.2  1993/10/19  14:35:26  griswolf
 * flush cout (cin not tied to cout for some compilers)
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:08   KEFFER
 * Tools.h++ V5.1
 * 
 */


// Declarations for Binary Tree class:
#include <rw/bintree.h>

/*
 * Declarations for class RWCollectableString.  
 * This class inherits class RWCString and class RWCollectable.  
 * Class RWCollectable has a virtual function compareTo() that is redefined 
 * by class RWCollectableString.  This function is used to order the 
 * RWCollectableStrings in the binary tree.
 */
#include <rw/collstr.h>
#include <rw/rstream.h>

/*
 * "STARTWRAP" and "ENDWRAP" are macros that allow inclusion of ANSI-C header
 * files using standard "C" linkage.  They are necessary for C++ compilers that
 * do not supply their own special "C++" header files and, instead, rely on
 * standard C header files.
 */
STARTWRAP
#include <stdlib.h>
ENDWRAP

/*
 * The following complex conditional inclusion owes its existence to
 * everyone going their own way, defining "standard" include files.
 */

#ifdef __ZTC__
#  include <fstream.hpp>
#else
#  ifdef __GLOCK__
#    include <fstream.hxx>
#  else
#    include <fstream.h>
#  endif
#endif

/*
 * A pointer to this function will be handed to RWBinaryTree::apply() 
 * to enumerate the members of the collection.
 */
static void
printStrings(RWCollectable* c, void*)
{
  /*
   * Cast the RWCollectable pointer to a RWCollectableString pointer,
   * then dereference and print.  RWCollectableString inherits
   * its ability to be printed from its base class RWCString.
   */
  cout << * (RWCollectableString*) c << " ";
}


main()
{
  RWBinaryTree		B;
  RWCollectableString	aWord;
  RWCollectableString*	pWord;
  int i = 0;

  cout << "***** Example using a Binary Tree (SortedCollection) ******\n";
  
  ifstream inputFile("textfile.in", ios::in);
  if(!inputFile){
    cerr << "Cannot open file textfile.in.\n";
    exit(1);
  }

  cout << "Reading from file \"textfile.in\"...\n";

  // Read until we hit an EOF:
  while ( inputFile >> aWord ) {

    // Transfer it to something off the heap and insert:
    pWord = new RWCollectableString(aWord);
    B.insert(pWord);

    cout << i++ << " " << *pWord << endl;
  }

  cout << "done.\n\nA total of " << i << " words were read.\n";
  cout << "Contents of the tree are:\n\n";
  B.apply(printStrings, 0);    // Uses global function defined above.
  cout << "\n\n";

  // Loop to do various things to the table:
  char option;
  
  while(1){

    cout << "(i)nsert (s)earch (d)elete (c)lear (l)ist e(x)it:\t";

    // Check for EOF or terminating character:
    if ( !(cin >> option).good() || option=='x' || option=='X' ) break;

    switch ( option ) {
    case 'i':		// Insert a word in tree.
    case 'I':
      cout << "Enter word:\t" << flush;
      pWord = new RWCollectableString;
      if(pWord){
        if( (cin >> *pWord).good() ) B.insert(pWord);	// Check for failed stream
        else delete pWord;
      }
      else cerr << "Out of memory.\n";
      break;
    case 's':		// Find the number of occurrences of a word.
    case 'S':
      cout << "Enter word:\t" << flush;
      cin >> aWord;
      cout << B.occurrencesOf(&aWord) << " occurrences of word in tree.\n";
      break;
    case 'd':   	// Remove a word.
    case 'D':
      cout << "Enter word:\t" << flush;
      cin >> aWord;
      B.removeAndDestroy(&aWord);
      break;
    case 'c':
    case 'C':		// Both clear AND destroy the contents
      B.clearAndDestroy();
      break;
    case 'l':
    case 'L':		// List the contents of the tree, in order.
      cout << B.entries() << " entries in tree:\n";
      B.apply(printStrings,0);
      cout << "\n\n";
      break;
    default:
      cerr << "Unrecognized.\n";
    } 	// End switch
  }	// End while

  cout << endl;		// To pretty things up.

  B.clearAndDestroy();	// Not really essential, but good style

  return 0;
}

/*
 * Example 7: FileManager.  Saves a linked list of ints.
 * This example appears in Section 11.2 of the manual.
 *
 * $Id: example7.cpp,v 2.2 1993/08/07 17:00:59 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example7.cpp,v $
 * Revision 2.2  1993/08/07  17:00:59  keffer
 * Now flushes output before read to work around Visual C++ problem.
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:10   KEFFER
 * Tools.h++ V5.1
 * 
 */

#include <rw/filemgr.h>
#include <rw/rstream.h>

struct DiskNode {
  int      data;
  RWoffset nextNode;
};

main()
{
  cout << "Test of a linked-list on disk, using the RWFileManager.\n";

  RWFileManager fm("linklist.dat");
  
  // Allocate space for offset to start of the linked list:
  fm.allocate(sizeof(RWoffset));
  // Allocate space for the first link:
  RWoffset thisNode = fm.allocate(sizeof(DiskNode));
  
  fm.SeekTo(fm.start());
  fm.Write(thisNode);
  
  DiskNode n;
  int temp;
  RWoffset lastNode;
  cout << "Input a series of integers, then EOF to end:\n" << flush;

  /* Borland bug necessitates explicit test for good. */  
  while ( (cin >> temp).good() ){
    n.data = temp;
    n.nextNode = fm.allocate(sizeof(DiskNode));
    fm.SeekTo(thisNode);
    fm.Write(n.data);
    fm.Write(n.nextNode);
    lastNode = thisNode;
    thisNode = n.nextNode;
  }
  
  fm.deallocate(n.nextNode);
  n.nextNode = RWNIL;
  fm.SeekTo(lastNode);
  fm.Write(n.data);	
  fm.Write(n.nextNode);
  cout << "Now run example8 to read them in.\n" << flush;
  return 0;
}

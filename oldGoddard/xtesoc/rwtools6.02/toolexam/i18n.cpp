/*
 * Internationalization (I18N) example.
 *
 * $Id: i18n.cpp,v 1.6 1993/09/07 04:13:34 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: i18n.cpp,v $
 * Revision 1.6  1993/09/07  04:13:34  jims
 * Port to release version of Windows NT SDK
 *
 * Revision 1.5  1993/08/07  17:05:04  keffer
 * Clarified some comments.
 *
 * Revision 1.4  1993/08/07  05:03:44  jims
 * Change name of RWZoneCommon to "RWZoneSimple"
 *
 */

#include <rw/rstream.h>
#include <rw/cstring.h>
#include <rw/locale.h>
#include <rw/rwdate.h>
#include <rw/rwtime.h>
STARTWRAP
#include <assert.h>
ENDWRAP

// Set the strings for the French and German locales:
#ifndef _MSC_VER
  const char *const fr = "fr";
  const char *const de = "de";
#else /* Microsoft */
  const char *const fr = "french";
  const char *const de = "german";
#endif

int main()
{
  RWDate today = RWDate::now();
  cout << "Today's date in the default locale:            "
       << today << endl;

  RWLocale& here = *new RWLocaleSnapshot("");
  cout << "Today's date using your environment:           "
       << today.asString('x', here) << endl;

  RWLocale::global(&here);
  cout << "The same, but installed as the default locale: "
       << today << endl;

  RWLocale& german = *new RWLocaleSnapshot(de); // or, "de_DE"

  cout << "In German (if available):                      "
       << today.asString('x', german) << endl;

  RWCString str;
  cout << "Now enter a date in German: " << flush;
  str.readLine(cin);
  today = RWDate(str, german);
  if (today.isValid())
    cout << "Printed using the default locale: " << today << endl;

#ifndef RW_IOS_XALLOC_BROKEN
  german.imbue(cin);
  cout << "Enter another date in German: " << flush;
  cin >> today;  // read a German date!
  if (today.isValid())
    cout << today << endl;
#endif

  RWZoneSimple newYorkZone(RWZone::USEastern, RWZone::NoAm);
  RWZoneSimple parisZone  (RWZone::Europe,    RWZone::WeEu);
  RWTime leaveNewYork(RWDate(20, 12, 1993), 23,00,00, newYorkZone);
  RWTime leaveParis  (RWDate(30,  3, 1994), 05,00,00, parisZone);

  RWTime arriveParis(leaveNewYork + long(7 * 3600));
  RWTime arriveNewYork(leaveParis + long(7 * 3600));

  RWLocaleSnapshot french(fr);
  cout << "Arrive' au Paris a` "
       << arriveParis.asString('c', parisZone, french)
       << ", heure local." << endl;

  cout << "Arrive in New York at "
       << arriveNewYork.asString('c', newYorkZone)
       << ", local time." << endl;

  static RWDaylightRule sudAmerica =
     { 0, 0, TRUE, {8, 4, 0, 120}, {2, 0, 0, 120}};

  RWZoneSimple  ciudadSud( RWZone::Atlantic, &sudAmerica );

  // RWZone::local(new RWZoneSimple(RWZone::Europe, RWZone::WeEu));

  {
  RWTime now = RWTime::now();
  cout << now.hour() << ":" << now.minute() << endl;
  }

  {
  RWTime now = RWTime::now();
  cout << now.asString('H') << ":" << now.asString('M') << endl;
  }

  {
  RWTime now = RWTime::now();
  struct tm tmbuf;
  now.extract(&tmbuf);
  const RWLocale& here = RWLocale::global();  // the default global locale
  cout << here.asString(&tmbuf, 'H') << ":"
       << here.asString(&tmbuf, 'M') << endl;
  }

  {
  RWLocaleSnapshot french(fr);
  double f = 1234567.89;
  long i = 987654;
  RWCString fs = french.asString(f, 2);
  RWCString is = french.asString(i);
  if (french.stringToNum(fs, &f) &&
      french.stringToNum(is, &i))  // verify the conversion
    cout << f << "\t" << i << endl
         << fs << "\t" << is << endl;
  }

  double sawbuck = 1000.;

  double price = 999.;  // $9.99
  double penny = 1.;    //  $.01
  assert(price + penny == sawbuck);

  {
  const RWLocale& here = RWLocale::global();
  double sawbuck = 1000.;
  RWCString tenNone  = here.moneyAsString(sawbuck, RWLocale::NONE);
  RWCString tenLocal = here.moneyAsString(sawbuck, RWLocale::LOCAL);
  RWCString tenIntl  = here.moneyAsString(sawbuck, RWLocale::INTL);
  if (here.stringToMoney(tenNone,  &sawbuck) &&
      here.stringToMoney(tenLocal, &sawbuck) &&
      here.stringToMoney(tenIntl,  &sawbuck))  // verify conversion
    cout << sawbuck << "  " << tenNone << "  "
         << tenLocal << "  " << tenIntl << "  " << endl;
  }

  return 0;
}

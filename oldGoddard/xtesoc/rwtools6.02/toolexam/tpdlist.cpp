/*
 * RWTPtrDlist<T> example, from the manual.
 *
 * $Id: tpdlist.cpp,v 2.3 1993/06/16 01:11:04 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1992 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tpdlist.cpp,v $
 * Revision 2.3  1993/06/16  01:11:04  keffer
 * Added RCS keywords.
 *
 *
 */

#include <rw/tpdlist.h>
#include <rw/rstream.h>
#include <string.h>

class Dog {
  char* name;
public:
  Dog(){name=0;}
  Dog( const char* c) {
    name = new char[strlen(c)+1];
    strcpy(name, c);
  }

  ~Dog() { delete name; }

  // Define a copy constructor:
  Dog(const Dog& dog) {
    name = new char[strlen(dog.name)+1];
    strcpy(name, dog.name);
  }

  // Define an assignment operator:
  void operator=(const Dog& dog) {
    if (this!=&dog) {
      delete name;
      name = new char[strlen(dog.name)+1];
      strcpy(name, dog.name);
    }
  }

  // Define an equality test operator:
  int operator==(const Dog& dog) const {
    return strcmp(name, dog.name)==0;
  }

  // Define a less-than operator
  int operator<(const Dog& dog) const {
	return strcmp(name, dog.name) < 0;
  }

  friend ostream& operator<<(ostream& str, const Dog& dog){
    str << dog.name;
    return str;
  }
};

main()
{
  RWTPtrDlist<Dog> terriers;
  terriers.insert(new Dog("Cairn Terrier"));
  terriers.insert(new Dog("Irish Terrier"));
  terriers.insert(new Dog("Schnauzer"));

  Dog key1("Schnauzer");
  cout << "The list " <<
    (terriers.contains(&key1) ? "does " : "does not ") <<
    "contain a Schnauzer\n";

  Dog key2("Irish Terrier");
  terriers.insertAt(
      terriers.index(&key2),
      new Dog("Fox Terrier")
    );

  Dog* d;
  while (!terriers.isEmpty()) {
    d = terriers.get();
    cout << *d << endl;
    delete d;
  }

  return 0;
}

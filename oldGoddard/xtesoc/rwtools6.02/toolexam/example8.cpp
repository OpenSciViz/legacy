/*
 * Example 8: FileManager.  Retrives the linked list of ints saved by Example 7.
 * This example appears in Section 11.2 of the manual.
 *
 * $Id: example8.cpp,v 2.1 1992/12/04 05:07:27 myersn Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: example8.cpp,v $
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:10   KEFFER
 * Tools.h++ V5.1
 * 
 */

#include <rw/filemgr.h>
#include <rw/rstream.h>

struct DiskNode {
  int      data;
  RWoffset nextNode;
};

main()
{
  RWFileManager fm("linklist.dat");
  
  fm.SeekTo(fm.start());
  RWoffset next;
  fm.Read(next);
  
  DiskNode n;
  while (next != RWNIL) {
    fm.SeekTo(next);
    fm.Read(n.data);
    fm.Read(n.nextNode);
    cout << n.data << "\n";
    next = n.nextNode;
  }
  cout.flush();
  return 0;
}


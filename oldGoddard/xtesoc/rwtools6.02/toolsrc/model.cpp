/*
 * RWModel --- maintains a list of dependent clients
 *
 * $Id: model.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: model.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1992/11/16  04:24:00  keffer
 * Broke out RWModel functionality into RWModel and RWModelClient
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   18 Feb 1992 19:28:38   KEFFER
 * Suppressed bogus "possible unintended assignment" warning.
 * 
 *    Rev 1.0   17 Nov 1991 13:34:48   keffer
 * Initial revision.
 */

#include "rw/model.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: model.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWModel::RWModel()
{

}

void
RWModel::addDependent(RWModelClient* m)
{
  dependList.append(m);
}

void
RWModel::removeDependent(RWModelClient* m)
{
  dependList.remove(m);
}


void
RWModel::changed(void* pData)
{
  RWModelClient* m;
  RWOrderedIterator next(dependList);
  while ((m = (RWModelClient*)next())!=0) m->updateFrom(this, pData);
}

/*
 * Definitions for Singly linked list of RWCollectables.
 *
 * $Id: slistcol.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: slistcol.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/05/18  19:19:53  keffer
 * Merged with slistctd.cpp
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   29 May 1992 11:59:32   KEFFER
 * Removed a superfluous RWSlist::clear() in the destructor.
 * 
 *    Rev 1.2   04 Mar 1992 09:17:30   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.1   28 Oct 1991 09:24:30   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:46   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/slistcol.h"
#include "defcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: slistcol.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWSlistCollectables, __RWSLISTCOLLECTABLES)

RWSlistCollectables::RWSlistCollectables() : RWSlist() {;}
RWSlistCollectables::~RWSlistCollectables() {;}

/*
 * Equivalence operator.  Check to make sure that the ordering is
 * the same and that the lists are the same length.
 */
RWBoolean
RWSlistCollectables::operator==(const RWSlistCollectables& sc) const
{
  RWSlistCollectablesIterator sci1(*(RWSlistCollectables*)this);
  RWSlistCollectablesIterator sci2((RWSlistCollectables&)sc);
  RWCollectable *item1, *item2;

  while( (item1=sci1()) != 0 ){
    item2 = sci2();
    if( item2==rwnil || !item1->isEqual(item2) ) return FALSE;
  }

  return sci2()==rwnil;		// Check for extra links in list 2
}

/*
 * This cannot be inline due to a bug in the Borland optimizer:
 */
RWSlistCollectablesIterator::RWSlistCollectablesIterator(RWSlistCollectables& s)
  : RWSlistIterator(s)
{ } 


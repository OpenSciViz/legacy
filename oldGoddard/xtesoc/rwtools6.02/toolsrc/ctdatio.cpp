/*
 * RWCollectableDate I/O
 *
 * $Id: ctdatio.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctdatio.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   28 Oct 1991 09:23:56   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:00   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colldate.h"
#include "rw/vstream.h"
#include "rw/rwfile.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctdatio.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

void
RWCollectableDate::restoreGuts(RWvistream& strm)
{
  RWDate::restoreFrom(strm);
}

void
RWCollectableDate::restoreGuts(RWFile& f)
{
  RWDate::restoreFrom(f);
}

void
RWCollectableDate::saveGuts(RWvostream& strm) const
{
  RWDate::saveOn(strm);
}

void
RWCollectableDate::saveGuts(RWFile& f) const
{
  RWDate::saveOn(f);
}

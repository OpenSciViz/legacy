/*
 * Definitions for Doubly linked list of RWCollectables.
 *
 * $Id: dlistcol.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dlistcol.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/05/18  19:19:34  keffer
 * Merged with dlistctd.cpp
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   04 Mar 1992 09:17:22   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.1   28 Oct 1991 09:24:06   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:18   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/dlistcol.h"
#include "defcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: dlistcol.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWDlistCollectables, __RWDLISTCOLLECTABLES)

RWDlistCollectables::RWDlistCollectables() : RWDlist() {;}
RWDlistCollectables::~RWDlistCollectables() {;}

/*
 * Equivalence operator.  Check to make sure that the ordering is
 * the same and that the lists are the same length.
 */
RWBoolean
RWDlistCollectables::operator==(const RWDlistCollectables& dc) const
{
  RWDlistCollectablesIterator dci1(*(RWDlistCollectables*)this);
  RWDlistCollectablesIterator dci2((RWDlistCollectables&)dc);
  RWCollectable *item1, *item2;

  while( (item1=dci1()) != 0 ){
    item2 = dci2();
    if( item2==rwnil || !item1->isEqual(item2) ) return FALSE;
  }

  return dci2()==rwnil;		// Check for extra links in list 2
}

/*
 * This cannot be inline due to a bug in the Borland optimizer.
 */
RWDlistCollectablesIterator::RWDlistCollectablesIterator(RWDlistCollectables& s)
 : RWDlistIterator(s)
{ }


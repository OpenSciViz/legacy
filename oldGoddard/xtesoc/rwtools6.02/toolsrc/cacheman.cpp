/*
 * Definitions for the cache manager RWCacheManager.
 *
 * $Id: cacheman.cpp,v 2.4 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cacheman.cpp,v $
 * Revision 2.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.3  1993/03/23  02:21:40  keffer
 * Eliminated int to unsigned conversions.
 *
 * Revision 2.2  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.1  1992/10/31  23:46:03  keffer
 * Check for BSD changed to RW_NON_ANSI_HEADERS
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   29 May 1992 09:46:40   KEFFER
 * Introduced CPP_ANSI_RECURSION macro
 * 
 *    Rev 1.3   27 May 1992 18:08:58   KEFFER
 * RWMEMCK compatible.
 * 
 *    Rev 1.2   04 Mar 1992 09:17:18   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.1   28 Oct 1991 09:23:52   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:50   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/cacheman.h"
#include "rw/rwfile.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: cacheman.cpp,v $ $Revision: 2.4 $ $Date: 1993/09/10 03:59:57 $");

STARTWRAP
#if defined(RW_NON_ANSI_HEADERS)
#  include <memory.h>		/* Looking for memcpy() */
#else
#  include <string.h>
#endif
ENDWRAP

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

/*
 * Construct a cache manager for blocks of size blocksz and mxblks buffers
 */
RWCacheManager::RWCacheManager(RWFile* file, unsigned blocksz, unsigned mxblks)
{
  theFile_ = file;
  maxblocks_ = mxblks;
  nused_ = 0;
  blocksize_ = blocksz;
  buff_ = new char[blocksize_*maxblocks_];
  diskAddrs_ = new RWoffset[maxblocks_];
  useCounts_ = new unsigned[maxblocks_];
}

RWCacheManager::~RWCacheManager()
{
  flush();
  delete useCounts_;
  delete diskAddrs_;
  delete buff_;
}

RWBoolean
RWCacheManager::flush()
{
  // Because we use "write through", this is all that's necessary:
  return theFile_->Flush();
}

void
RWCacheManager::invalidate()
{
  nused_ = 0;
}

RWBoolean
RWCacheManager::read(RWoffset locn, void* dat)
{
  RWPRECONDITION( dat!=rwnil );
  unsigned islot = ageAndFindSlot(locn);

  if(islot == RW_NPOS){
    /*
     * Not in buffer;  we'll have to read it in from disk.
     * Get a free slot.
     */
    if( (islot = getFreeSlot()) == RW_NPOS ) return FALSE;
    diskAddrs_[islot] = locn;
    if( !theFile_->SeekTo(locn) || 
        !theFile_->Read( buff_+islot*blocksize_, blocksize_ ) )
      return FALSE;
  }
  useCounts_[islot] = 0;
  memcpy(dat, buff_+islot*blocksize_, blocksize_);
  return TRUE;
}

RWBoolean
RWCacheManager::write(RWoffset locn, void* dat)
{
  RWPRECONDITION( dat!=rwnil );
  unsigned islot = ageAndFindSlot(locn);

  if(islot == RW_NPOS){
    /*
     * Not in buffer; find a free slot.
     */
    if( (islot = getFreeSlot()) == RW_NPOS ) return FALSE;
    diskAddrs_[islot] = locn;
  }

  useCounts_[islot] = 0;
  memcpy(buff_+islot*blocksize_, dat, blocksize_);
  return theFile_->SeekTo(locn) && theFile_->Write( buff_+islot*blocksize_, blocksize_ );
}

/************************************************
 *		Private functions		*
 ************************************************/
unsigned
RWCacheManager::ageAndFindSlot(RWoffset locn)
{
  unsigned islot = RW_NPOS;
  for(register unsigned i = 0; i<nused_; i++){
    if(diskAddrs_[i] == locn) islot = i;
    useCounts_[i]++;		// Age the blocks
  }
  return islot;
}

RWBoolean
RWCacheManager::flush(unsigned)
{
  return theFile_->Flush();
}

unsigned
RWCacheManager::getFreeSlot()
{
  unsigned islot;

  if(nused_ < maxblocks_){
    islot = nused_++;		// Found an unused slot.
  }
  else {
    // No free slots; get the Least Recently Used block
    islot = LRU();
    if( !flush(islot) ) islot = RW_NPOS;
  }
  return islot;
}

unsigned
RWCacheManager::LRU() const
{
  unsigned islot = 0;
  unsigned maxCount = useCounts_[0];
  for(register unsigned i=1; i<nused_; i++){
    if(useCounts_[i] > maxCount){ maxCount=useCounts_[islot=i]; }
  }
  return islot;
}

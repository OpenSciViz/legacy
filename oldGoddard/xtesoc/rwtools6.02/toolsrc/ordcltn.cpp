/*
 * Definitions for RWOrdered (OrderedCollection)
 *
 * $Id: ordcltn.cpp,v 2.10 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ordcltn.cpp,v $
 * Revision 2.10  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.9  1993/05/31  21:46:32  keffer
 * New messaging architecture for localization
 *
 * Revision 2.8  1993/05/18  15:57:03  dealys
 * merged with orddf.cpp
 *
 * Revision 2.7  1993/05/18  00:48:57  keffer
 * Introduced new exception handling classes
 *
 * Revision 2.6  1993/05/14  21:01:16  dealys
 * removed iterator code
 *
 * Revision 2.5  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.4  1993/03/24  23:49:27  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.3  1993/01/25  18:17:44  keffer
 * RW_NO_CONST_OVERLOADS->RW_NO_CONST_OVERLOAD
 *
 * Revision 2.2  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.1  1992/11/15  00:02:30  keffer
 * Added explicit "this->" dereference to suppress unfounded
 * cfront 3.0 warnings.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   04 Mar 1992 09:17:24   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.3   05 Nov 1991 14:06:22   keffer
 * Removed RWXXXXErr macros --- now done by error package
 * 
 *    Rev 1.2   28 Oct 1991 09:24:22   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.1   29 Jul 1991 14:20:42   keffer
 * Return type of asOrderedCollection() is now RWOrdered.
 * 
 *    Rev 1.0   28 Jul 1991 08:39:32   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/ordcltn.h"
#include "rw/rwerr.h"
#include "rw/toolerr.h"
#include "defcol.h"          

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ordcltn.cpp,v $ $Revision: 2.10 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWOrdered, __RWORDERED)

RWOrdered::RWOrdered(size_t size) :
  vec(size, rwnil)
{
  nitems = 0;
}                                                       

RWBoolean
RWOrdered::operator==(const RWOrdered& rwo) const
{
  if( nitems!=rwo.nitems )return FALSE;

  for( register size_t i=0; i<nitems; i++ )
    if( !this->vec(i)->isEqual(rwo.vec(i))) return FALSE;

  return TRUE;
}

RWCollectable*
RWOrdered::append(RWCollectable* c)
{
  return RWOrdered::insertAt(nitems, c);
}

void
RWOrdered::apply(RWapplyCollectable ap, void* x)
{
  RWPRECONDITION( ap!=0 );
  for( register size_t i = 0; i<nitems; i++ )
    (*ap)(vec(i), x);
}

RWCollectable*&
RWOrdered::at(size_t inx)	
{
  return (*this)[inx];		// This will do bounds checking
}

#ifndef RW_NO_CONST_OVERLOAD
const RWCollectable*
RWOrdered::at(size_t inx) const
{
  const RWCollectable* temp = (*(RWOrdered*)this)[inx];
  return temp;
}
#endif

void
RWOrdered::clear()
{
  nitems = 0;
  if(vec.length() > DEFAULT_CAPACITY)	// If vector has grown, shrink it back down
	vec.resize(DEFAULT_CAPACITY);
}

RWCollectable*
RWOrdered::find(const RWCollectable* c) const
{
  size_t indx = index(c);
  return indx==RW_NPOS ? rwnil : this->vec(indx);
}

/*
 * Returns first element, or nil if there is no first element.
 */
RWCollectable*
RWOrdered::first() const
{
  return nitems>0 ? this->vec(0) : rwnil;
}

size_t
RWOrdered::index(const RWCollectable* c) const
{
  for(register size_t i=0; i<nitems; i++)
    if(this->vec(i)->isEqual(c)) return i;
  return RW_NPOS;
}

RWCollectable*
RWOrdered::insert(RWCollectable* c)
{
  return RWOrdered::insertAt(nitems, c);
}

// For backwards compatiblity:
RWCollectable*
RWOrdered::insertAfter(int ipt, RWCollectable* c)
{
  return insertAt((size_t)(ipt+1), c);
}

RWCollectable*
RWOrdered::insertAt(size_t ipt, RWCollectable* c)
{
  if(ipt>nitems )
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX, (unsigned)ipt, (unsigned)nitems) ));

  // Check for overflow:
  if(nitems>=vec.length())
    vec.resize(vec.length()+DEFAULT_RESIZE);

  // Slide right:
  for(register size_t i=nitems; i>ipt; i--)
    vec(i) = vec(i-1);

  nitems++;
  return vec(ipt) = c;
}

RWCollectable*
RWOrdered::last() const
{
  return nitems>0 ? this->vec(nitems-1) : rwnil;
}

size_t
RWOrdered::occurrencesOf(const RWCollectable* c) const
{
  register size_t total = 0;
  size_t idx = index(c);
  if(idx!=RW_NPOS)
  {
    for(register size_t i = idx; i<nitems; i++)
      if(this->vec(i)->isEqual(c)) total++;
  }
  return total;
}

RWCollectable*
RWOrdered::remove(const RWCollectable* c)
{
  size_t i = index(c);
  return i==RW_NPOS ? rwnil : removeAt(i);
}

RWCollectable*
RWOrdered::prepend(RWCollectable* c)
{
  return RWOrdered::insertAt(0,c);
}

void
RWOrdered::resize(size_t N)
{
  if( N>=nitems ) vec.resize(N);
}

/******************** Miscellaneous functions *****************************/

RWOrdered
RWCollection::asOrderedCollection() const
{
  RWOrdered temp(entries());
  temp += *this;
  return temp;
}

void
RWOrdered::boundsCheck(size_t ipt) const
{
  if(ipt>=nitems )
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX, (unsigned)ipt, (unsigned)nitems-1) ));
}

RWCollectable*
RWOrdered::removeAt(size_t ipt)
{
  RWCollectable* victim = vec(ipt);
  // Slide left:
  for(register size_t i=ipt; i<nitems-1; i++)
    vec(i) = vec(i+1);
  nitems--;
  return victim;
}


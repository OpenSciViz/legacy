/*
 * Definitions for Rogue Wave Utility and obsolete Error Functions
 *
 * $Id: utility.cpp,v 2.8 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: utility.cpp,v $
 * Revision 2.8  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.7  1993/08/06  04:33:45  randall
 * removed use of RW_CAFE_ALPHA
 *
 * Revision 2.6  1993/05/31  21:46:32  keffer
 * New messaging architecture for localization
 *
 * Revision 2.5  1993/05/18  00:48:57  keffer
 * Introduced new exception handling classes
 *
 * Revision 2.3  1993/02/11  22:42:59  keffer
 * Added IBM xlC to the list of compilers without genericerror().
 *
 * Revision 2.2  1993/02/07  19:53:58  keffer
 * genericerror now exported from DLL.
 *
 * Revision 2.1  1992/11/26  19:50:28  myersn
 * port to Gnu gcc.  Also, simplify the dummy definition at the end.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.9   30 May 1992 16:34:08   KEFFER
 * Inserted rwdummy1() to keep fussy librarians happy.
 * 
 *    Rev 1.7   28 May 1992 19:52:58   KEFFER
 * Ported to Metaware High-C
 * 
 *    Rev 1.6   24 Mar 1992 13:30:40   KEFFER
 * Ported to Microsoft C/C++ V7.0
 * 
 *    Rev 1.5   05 Nov 1991 14:07:04   keffer
 * Removed RWXXXXErr macros --- now done by error package
 * 
 *    Rev 1.4   17 Oct 1991 09:15:32   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.3   08 Oct 1991 13:45:18   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.2   29 Jul 1991 14:41:00   keffer
 * Now #includes "rw/defs.h" before checking type of compiler.
 * 
 *    Rev 1.1   24 Jul 1991 12:56:20   keffer
 * Added pvcs keywords
 *
 */

#include "rw/defs.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: utility.cpp,v $ $Revision: 2.8 $ $Date: 1993/09/10 03:59:57 $");

/*
 * These guys declared "genericerror()", but
 * didn't bother offering a definition:
 */

#if defined(__TURBOC__) || defined(__ZTC__) || defined(__OREGON__) || defined(_MSC_VER) || defined(__HIGHC__) || defined(__GNUC__) || defined(__xlC__)

#include "rw/rwerr.h"
#include "rw/coreerr.h"

int rwexport
genericerror(int ern, char* msg)
{
  RWTHROW( RWxmsg(RWMessage( RWCORE_GENERIC, ern, msg) ));
  return 1;
}
#endif

/********************************************************
 *							*
 *	Error Functions --- not used anymore, but	*
 *	included for backwards compatibility.		*
 *							*
 ********************************************************/

#ifdef V3_COMPATIBLE

STARTWRAP
#include <stdlib.h>
#include <stdio.h>
ENDWRAP

typedef void (*RWErrorHandler)(RWSeverity, const char*, const char*);

void
RWDefaultErrorRoutine(RWSeverity gravity, const char* routine, const char* comment)
{
  fputs("\n",stderr);
  fputs("** ",stderr);
  fputs(routine,stderr);
  fputs("\n",stderr);

  fputs("** ", stderr);
  fputs(comment, stderr);
  fputs("\n", stderr);

  switch (gravity) {
  case RWWARNING:
    fputs("** Processing continues.\n", stderr);
    break;
  case RWDEFAULT:
  case RWFATAL:
  default:
    fputs("** Processing terminated.\n", stderr);
    exit(gravity);
  };
}

RWErrorHandler RWErrorRoutine = RWDefaultErrorRoutine;

void
RWError(RWSeverity severity, const char* routine, const char* comment)
{
  (*RWErrorRoutine)(severity, routine, comment);
}

RWErrorHandler
setRWErrorHandler(RWErrorHandler routine)
{
  RWErrorHandler temp = RWErrorRoutine;
  RWErrorRoutine = routine;
  return temp;
}

#endif

// This definition is always compiled in case nothing else is,
// in order to quiet down some fussy librarians:
int rwUtilityDummy;

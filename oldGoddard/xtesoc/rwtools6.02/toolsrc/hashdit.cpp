/*
 * Definitions for Hashed Dictionary Iterators.
 *
 * $Id: hashdit.cpp,v 1.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: hashdit.cpp,v $
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  19:51:40  dealys
 * Initial revision
 *
 *
 */

#include "rw/hashdict.h"
#include "rw/collass.h"
#include "rw/idenset.h"
#include "rw/slistcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: hashdit.cpp,v $ $Revision: 1.2 $ $Date: 1993/09/10 03:59:57 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif


/*********************************************************
*					 		  *
*      Methods for Class RWHashDictionaryIterator	  *
*							  *
**********************************************************/

RWCollectable*
RWHashDictionaryIterator::findNext(const RWCollectable* key)
{
  const RWCollectableAssociation* a = (const RWCollectableAssociation*)RWSetIterator::findNext(key);
  return a ? a->key() : rwnil;
}

RWCollectable*
RWHashDictionaryIterator::key() const
{
  const RWCollectableAssociation* a = (const RWCollectableAssociation*)RWSetIterator::key();
  return a ? a->key() : rwnil;
}

RWCollectable*
RWHashDictionaryIterator::operator()()
{
  const RWCollectableAssociation* a = (const RWCollectableAssociation*)RWSetIterator::operator()();
  return a ? a->key() : rwnil;
}

RWCollectable*
RWHashDictionaryIterator::remove()
{
  RWCollectable* ret;
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWSetIterator::remove();
  if(a){
    ret = a->key();
    delete a;
  }
  else
    ret = rwnil;
  return ret;
}

RWCollectable*
RWHashDictionaryIterator::removeNext(const RWCollectable* key)
{
  RWCollectable* ret;
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWSetIterator::removeNext(key);
  if(a){
    ret = a->key();
    delete a;
  }
  else
    ret = rwnil;
  return ret;
}

RWCollectable*
RWHashDictionaryIterator::value() const
{
  const RWCollectableAssociation* a = (const RWCollectableAssociation*)RWSetIterator::key();
  return a ? a->value() : rwnil;
}

RWCollectable*
RWHashDictionaryIterator::value(RWCollectable* newValue) const
{
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWSetIterator::key();
  return a ? a->value(newValue) : rwnil;
}

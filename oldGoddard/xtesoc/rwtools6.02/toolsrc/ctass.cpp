/*
 * Definitions for RWCollectableAssociation
 *
 * $Id: ctass.cpp,v 2.5 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctass.cpp,v $
 * Revision 2.5  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.4  1993/07/03  23:55:58  keffer
 * Simplified and improved the clearAndDestroy() algorithm.
 *
 * Revision 2.3  1993/05/18  16:15:20  dealys
 * merged with ctassdf.cpp
 *
 * Revision 2.2  1993/03/17  21:21:15  keffer
 * Return type of binaryStoreSize is now RWspace
 *
 * Revision 2.1  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.9   17 Jul 1992 11:06:46   KEFFER
 * Fixed error in binaryStoreSize calculation.
 * 
 *    Rev 1.8   29 May 1992 09:46:40   KEFFER
 * Introduced CPP_ANSI_RECURSION macro
 * 
 *    Rev 1.7   28 May 1992 15:35:30   KEFFER
 * Introduced RWhashAddress() for identity hashes.
 * 
 *    Rev 1.6   27 May 1992 18:08:58   KEFFER
 * RWMEMCK compatible.
 * 
 *    Rev 1.5   25 May 1992 15:28:56   KEFFER
 * One less function call in RWCollectableIDAssociation::hash().
 * 
 *    Rev 1.4   29 Apr 1992 14:50:50   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 *    Rev 1.3   04 Mar 1992 09:17:20   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.2   28 Oct 1991 09:23:54   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:52   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collass.h"
#include "defcol.h"      

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctass.cpp,v $ $Revision: 2.5 $ $Date: 1993/09/10 03:59:57 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

RWDEFINE_COLLECTABLE2(RWCollectableAssociation,   __RWCOLLECTABLEASSOCIATION)
RWDEFINE_COLLECTABLE2(RWCollectableIDAssociation, __RWCOLLECTABLEIDASSOCIATION)

RWCollectableAssociation::RWCollectableAssociation() :
  ky(rwnil),
  val(rwnil)
{
}
 
RWCollectableIDAssociation::RWCollectableIDAssociation() :
    RWCollectableAssociation()
{
}
                           

RWCollectable*
RWCollectableAssociation::value(RWCollectable* newValue)
{
  register RWCollectable* temp = val;
  val = newValue;
  return temp;
}

RWspace
RWCollectableAssociation::binaryStoreSize() const
{
  return ky->recursiveStoreSize() + val->recursiveStoreSize();
}

int
RWCollectableAssociation::compareTo(const RWCollectable* a) const
{
  RWPRECONDITION( a!=rwnil );
  return -a->compareTo(ky);
}

unsigned
RWCollectableAssociation::hash() const
{
  return ky->hash();
}

RWBoolean
RWCollectableAssociation::isEqual(const RWCollectable* a) const
{
  RWPRECONDITION( a!=rwnil );
  return a->isEqual(ky);
}

/***************** RWCollectableIDAssociation members ****************/

unsigned
RWCollectableIDAssociation::hash() const
{
  return RWhashAddress((void*)ky);
}

int
RWCollectableIDAssociation::compareTo(const RWCollectable* a) const
{
   return (key() < a) ? -1 : (key() == a) ? 0 : 1;
}

RWBoolean
RWCollectableIDAssociation::isEqual(const RWCollectable* a) const
{
  RWPRECONDITION( a!=rwnil );
  if(a->isA() == __RWCOLLECTABLEIDASSOCIATION)
     return ((RWCollectableIDAssociation*)a)->key() == ky;
  else
     return a == ky;
}


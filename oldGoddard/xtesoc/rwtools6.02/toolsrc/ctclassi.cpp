/*
 * RWCollection I/O
 *
 * $Id: ctclassi.cpp,v 2.4 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctclassi.cpp,v $
 * Revision 2.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.3  1993/07/09  18:15:14  randall
 * for port to Centerline made static member function argument a reference
 *
 * Revision 2.2  1993/04/14  18:39:46  keffer
 * static function writeObjToXXX() changed to a static m.f.
 * storeObjToXXX()
 *
 * Revision 2.1  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   25 May 1992 15:31:46   KEFFER
 * restoreFrom -> recursiveRestoreFrom; saveOn -> recursiveSaveOn
 * 
 *    Rev 1.2   18 Feb 1992 19:27:38   KEFFER
 * Now calls clear() before restoreGuts().
 * 
 *    Rev 1.1   28 Oct 1991 09:23:54   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:56   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colclass.h"
#include "rw/vstream.h"
#include "rw/rwfile.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctclassi.cpp,v $ $Revision: 2.4 $ $Date: 1993/09/10 03:59:57 $");

/*
 * Virtual function to store all the members of an
 * arbitrary collection to output:
 */

void
RWCollection::saveGuts(RWvostream& s) const
{
  s << entries();

  if( s.good() ){
    // "this" cast used to suppress warnings.
    ((RWCollection*)this)->apply(&RWCollection::saveObjToStream, &s);
  }
}

void
RWCollection::saveGuts(RWFile& file) const
{
  size_t N = entries();
  file.Write(N);

  // Cast done to suppress unfounded "const" warnings:
  ((RWCollection*)this)->apply(&RWCollection::saveObjToRWFile, &file);
}

void
RWCollection::restoreGuts(RWvistream& s)
{
  size_t nobjs;

  clear();
  s >> nobjs;
  if( !s.good() ) return;

  while (nobjs--) {
    RWCollectable* c = recursiveRestoreFrom(s);
    if( !s.good() ) return;
    insert(c);
  }
}

void
RWCollection::restoreGuts(RWFile& file)
{
  size_t nobjs;

  clear();
  file.Read(nobjs);

  while (nobjs--) {
    RWCollectable* c = recursiveRestoreFrom(file);
    if( file.Error() ) break;
    insert(c);
  }
}

/* static */ void 
RWCollection::saveObjToStream(RWCollectable* c, void* x)
{
  RWvostream& strm = *(RWvostream*)x;
  if(strm.good())
    c->recursiveSaveOn(strm);
}

/* static */ void
RWCollection::saveObjToRWFile(RWCollectable* c, void* x)
{
  RWFile& f = *(RWFile*)x;
  if( !f.Error() )
    c->recursiveSaveOn(f);
}

/*
 * RWInstanceManager
 *
 * $Id: instmgr.cpp,v 1.7 1993/11/18 01:20:44 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: instmgr.cpp,v $
 * Revision 1.7  1993/11/18  01:20:44  jims
 * Move support for OS/2 MT from rwtsd to instmgr
 *
 * Revision 1.6  1993/11/17  02:12:50  myersn
 * add OS/2 support
 *
 * Revision 1.5  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.4  1993/08/05  11:40:24  jims
 * Remove destructor; instmgr for WIN16 DLLs only
 *
 * Revision 1.3  1993/07/29  06:42:17  jims
 * Change RW_WIN32_API to __WIN32__
 *
 * Revision 1.2  1993/07/09  08:04:10  jims
 * Change FAR to rwfar; Port to Windows NT
 *
 * Revision 1.1  1993/04/12  12:03:21  jims
 * Initial revision
 *
 * Revision 2.3  1993/02/05  07:36:14  jims
 * Now uses rwtsd.dll (Rogue Wave Task Specific Data) library
 *
 * Revision 2.2  1992/11/20  18:51:56  keffer
 * DEBUG -> RWDEBUG
 *
 * Revision 2.1  1992/11/16  04:23:53  keffer
 * Reflects changes to RWModel
 *
 * Revision 2.0  1992/10/23  03:36:49  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   07 Jul 1992 16:22:22   KEFFER
 * Added additional preconditions.
 * 
 *    Rev 1.3   22 Jun 1992 17:54:24   KEFFER
 * Instance mgr now uses fallback position if no process has been registered
 * 
 *    Rev 1.2   18 Mar 1992 11:50:22   KEFFER
 * 
 *    Rev 1.1   18 Mar 1992 11:44:42   KEFFER
 * Changed nil to rwnil.
 * 
 *    Rev 1.0   12 Nov 1991 13:53:22   keffer
 * Initial revision.
 */

#include "rw/instmgr.h"
#ifdef __OS2___ 
#  include <stdlib.h>     // looking for size_t
#  include <stddef.h>     // looking for _threadid
#  include <limits.h>     // looking for ULONG_MAX
#endif

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: instmgr.cpp,v $ $Revision: 1.7 $ $Date: 1993/11/18 01:20:44 $");

/****************************************
 *					*
 *   Win16-DLL / Multi-thread CODE	*
 *					*
 ****************************************/

#if (defined(__DLL__) && defined(__WIN16__)) || defined(RW_MULTI_THREAD)

void rwfar*
RWInstanceManager::addValue()
{
#ifdef RWDEBUG
  assert(currentValue()==rwnil);
#endif

  void rwfar* value = newValue();	 // init value provided by specializing class

#if defined(__DLL__) && defined(__WIN16__) || defined(__OS2__)
  RWSetTaskSpecificData(tsd_key, value);

#elif defined(sun)
  thr_setspecific(tsd_key, value);

#elif defined(__WIN32__)
  TlsSetValue(tsd_key, value);

#endif 
  return value;
}

void rwfar*
RWInstanceManager::currentValue()
{
#if defined(__DLL__) && defined(__WIN16__) || defined(__OS2__)
  return RWGetTaskSpecificData(tsd_key); 

#elif defined(sun)
  void *value;
  thr_getspecific(tsd_key, &value);
  return value;

#elif defined(__WIN32__)
  return TlsGetValue(tsd_key);

#endif
}

void
RWInstanceManager::freeValue()
{
  deleteValue(currentValue());	  // give value to specializing class to delete

// Now make sure the next call to currentValue() returns NULL:
#if defined(__DLL__) && defined(__WIN16__) || defined(__OS2__)
  RWReleaseTaskSpecificData(tsd_key);

#elif defined(sun)
  thr_setspecific(tsd_key, NULL); 

#elif defined(__WIN32__)
  TlsSetValue(tsd_key, NULL);
#endif
}

RWInstanceManager::RWInstanceManager()
{
// Get a key -- Thread lib should init each thread to have NULL associated
//              with this key.

#if defined(__DLL__) && defined(__WIN16__) || defined(__OS2__)
  tsd_key = RWGetTaskSpecificKey();

#elif defined(sun)
  thr_keycreate(&tsd_key, NULL);

#elif defined(__WIN32__)
  tsd_key = TlsAlloc();
#endif
}

// =========================================================================
//  For OS/2 Only:
// -------------------------------------------------------------------------
#ifdef __OS2___ 

#include <stdlib.h>     // looking for size_t
#include <stddef.h>	// looking for _threadid
#include <limits.h>	// looking for ULONG_MAX
#include "rw/rwtsd.h"

// Note: this value must match the typedef for RWTSDKEY in instmgr.h
RWTSDKEY const MAX_KEY = ULONG_MAX;

/*
 * Threads obtain Keys through which they request that this facility
 * maintain thread-specific data. For each Key, we may store and
 * retrieve a single pointer---often to a structure holding several items
 * of interest---for each thread. As several threads may each hold
 * one or more keys, each piece of thread specific data is defined by
 * the combiniation of a thread ID (TID) and a Key. 
 * 
 * The global taskList (defined below) is a linked list with one
 * TaskEntry for each thread.  Each TaskEntry contains a linked-list
 * of KeyEntrys, where the actual task-specific data is held. 
 *
 * (As soon as enough compilers support templates, KeyEntry and
 *  TaskEntry should be instances of the same class template)
 */

/*
 * for some reason, the CSet compiler confuses unsigned, int and
 * unsigned long for TIDs. However the type TID is unsigned long, so
 * we'll try to use that...
 */
	 
struct KeyEntry {
  RWTSDKEY		hKey;
  void  *		taskSpecificData;
  KeyEntry		*next;

  KeyEntry		*find(RWTSDKEY h);
  void			add(KeyEntry *entry);
  KeyEntry		*remove(RWTSDKEY h, KeyEntry** victim);

  KeyEntry(RWTSDKEY h, void  *tsd)
    : hKey(h), taskSpecificData(tsd), next(0) { }  
};

struct TaskEntry  {
  TID 	hTask;
  KeyEntry	*keyList;
  TaskEntry	*next;              //   for chaining

  TaskEntry     *find(TID h);
  void		add(TaskEntry *entry);
  TaskEntry     *remove(TID h, TaskEntry** victim);

  TaskEntry(TID);
  ~TaskEntry();
};   	  

/*
 * Build a new task entry
 */
TaskEntry::TaskEntry(TID h) : hTask(h), keyList(0), next(0)
{
}
  
// before deleting a TaskEntry, delete all of its keys:
TaskEntry::~TaskEntry()
{
  KeyEntry *key = keyList;
  while (key) {
    KeyEntry *temp = key->next;
    delete key;
    key = temp;
  }
}

// ================================
//  TaskEntry member functions:
// --------------------------------

TaskEntry *TaskEntry::find(TID h)
{
  TaskEntry *entry = this;
  while (entry) {
    if (entry->hTask == h)
      return entry;
    else
      entry = entry->next;
  }
  return 0;
}

void TaskEntry::add(TaskEntry *newEntry)
{
  TaskEntry *entry = this;
  while (entry->next) entry = entry->next;
  entry->next = newEntry;
}	  

/*
 * Returns the list with the entry for h removed, sets *victim to
 * the entry which was removed, or 0 if no entry removed:
 */
TaskEntry *TaskEntry::remove(TID h, TaskEntry** victim)
{
  if (hTask == h) {
    *victim = this;
    return next;
  }

  TaskEntry *prev  = this;
  TaskEntry *entry = this->next;
  while (entry) {
    if (entry->hTask == h) {
      *victim = entry;
      prev->next = entry->next;
      return this;
    }
    else {
      prev = entry;
      entry = entry->next;
    }
  }	    	    	  
  *victim = 0;
  return this;  	  	  
}

// ================================
//  KeyEntry member functions:
// --------------------------------

KeyEntry *KeyEntry::find(RWTSDKEY h)
{
  KeyEntry *entry = this;
  while (entry) {
    if (entry->hKey == h)
      return entry;
    else
      entry = entry->next;
  }
  return 0;
}

void KeyEntry::add(KeyEntry *newEntry)
{
  KeyEntry *entry = this;
  while (entry->next) entry = entry->next;
  entry->next = newEntry;
}	  

/*
 * Returns the list with the entry for h removed, sets *victim to
 * the entry which was removed, or 0 if no entry removed:
 */
KeyEntry *KeyEntry::remove(RWTSDKEY h, KeyEntry** victim)
{
  if (hKey == h) {
    *victim = this;
    return next;
  }

  KeyEntry *prev  = this;
  KeyEntry *entry = this->next;

  while (entry) {
    if (entry->hKey == h) {
      *victim = entry;
      prev->next = entry->next;
      return this;
    }
    else {
      prev = entry;
      entry = entry->next;
    }
  }	    	    	  
  *victim = 0;
  return this;  	  	  
}

// =====================
//  Internal functions
// ---------------------

TaskEntry *taskList = 0;

// delete the record for a particular task & key:
static void releaseTaskSpecificData(RWTSDKEY hKey, TID hTask)
{
  TaskEntry *task;
  KeyEntry  *removedEntry;
    	
  if (!taskList || 0 == (task = taskList->find(hTask)) || !task->keyList)
    return;
  else {
    task->keyList = task->keyList->remove(hKey, &removedEntry);
    delete removedEntry;
  }
}

//========================================================================
// Public Functions
// -----------------------------------------------------------------------

/*
 * Return the next key in sequence, 0 if out of keys.  Key numbers
 * are not reused, but at one key/second it would take 136 years
 * to run out.
 */
RWTSDKEY
RWGetTaskSpecificKey()
{
  static RWTSDKEY key = 0;
  if (key == MAX_KEY)
    return 0;
  else
    return ++key;
}
      	      	
/*
 * Keep task specific data on behalf of the passed key
 * for the current task.  Call exitProc when the task exits.
 * Return 1 if successful, 0 if key already has TSD for this task.
 */
int
RWSetTaskSpecificData(RWTSDKEY hKey, void * tsd)
{
  TID hCurTask = RWTHREADID;
  TaskEntry *task = 0;
  KeyEntry *key = 0;

  if (taskList) { 
    task = taskList->find(hCurTask);
    if (task) {
      if (task->keyList) {
        key = task->keyList->find(hKey);
        if (key) // ERROR: key already has tsd for this task
          return 0;
        else {
          task->keyList->add(new KeyEntry(hKey, tsd));
        }
      }
      else { // task has no keyList, make new entry:
	task->keyList = new KeyEntry(hKey, tsd);
      }
    }
    else { // Current task not in taskList, make new entry:
      task = new TaskEntry(hCurTask);
      task->keyList = new KeyEntry(hKey, tsd);
      taskList->add(task);
    }	    
  }
  else { // No taskList yet, make new entry:
    taskList = new TaskEntry(hCurTask);
    taskList->keyList = new KeyEntry(hKey, tsd);
  }
  return 1;
}


// Return the data being kept under this key for the current task:
void *
RWGetTaskSpecificData(RWTSDKEY hKey)
{
  TaskEntry *task;
  KeyEntry *key;
  
  if (!taskList || 0 == (task = taskList->find( RWTHREADID ))) 
    return 0;    // Task not found
  else if (!task->keyList || 0 == (key = task->keyList->find(hKey))) 
    return 0;    // Key not found
  else
    return key->taskSpecificData;
}

/*
 * Stop keeping data under this key for the current task,
 * and return the data that was being kept (presumably so
 * it can be deleted or otherwise cleaned-up).
 */
void *
RWReleaseTaskSpecificData(RWTSDKEY hKey)
{
  void * tsd = RWGetTaskSpecificData(hKey);
  releaseTaskSpecificData(hKey, RWTHREADID);
  return tsd;
}

#endif /* __OS2__ */

#else	/* Neither Win16-DLL nor Multi-threaded */

#  error RWInstanceManager only to be used in Win16-DLL or MT Envirnonments

#endif	/* (__DLL__ &&__WIN16) || RW_MULTI_THREAD */

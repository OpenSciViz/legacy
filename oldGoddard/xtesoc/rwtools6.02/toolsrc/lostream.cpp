/*
 * Definitions for RWLocale stream operations.
 *
 * $Id: lostream.cpp,v 1.7 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * (503) 754-3010   Fax: (503) 757-6650  email: support@roguewave.com
 *
 * Copyright (C) 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: lostream.cpp,v $
 * Revision 1.7  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.6  1993/08/06  04:32:21  randall
 * removed use of RW_CAFE_ALPHA.
 *
 * Revision 1.5  1993/04/15  02:14:05  myersn
 * add RWLocale::unimbue(ios&), guard against entire lack of ios::xalloc().
 *
 * Revision 1.5  1993/04/15  02:06:44  myersn
 * add RWLocale::unimbue(), guard against entire lack of ios::xalloc() support.
 *
 * Revision 1.4  1993/04/12  21:54:31  myersn
 * add RW_IOS_XALLOC_BROKEN guards around use of ios::pword().
 *
 * Revision 1.3  1993/04/12  11:50:46  jims
 * Added work-around for Sun Cafe Alpha problem with MT and ios::xalloc init
 *
 * Revision 1.2  1993/04/09  23:19:29  myersn
 * declare RWLocale::imbue() const.
 *
// Revision 1.1  1993/04/06  17:10:55  myersn
// Initial revision
//
 */

#include <iostream.h>
#include <rw/locale.h>

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: lostream.cpp,v $ $Revision: 1.7 $ $Date: 1993/09/10 03:59:57 $");

#ifndef RW_IOS_XALLOC_BROKEN

// the magic cookie to pass to stream.pword():
static int localeIndex = ios::xalloc();

#endif /* RW_IOS_XALLOC_BROKEN */

const RWLocale&
RWLocale::of(ios& s)
{
#ifndef RW_IOS_XALLOC_BROKEN

  const RWLocale* loc = (const RWLocale*) s.pword(localeIndex);
  if (loc)
    return *loc;
  else
#endif
    return RWLocale::global();
}

#ifndef RW_IOS_XALLOC_BROKEN

const RWLocale*
RWLocale::imbue(ios& s) const
{
  void*& p = s.pword(localeIndex);
  const RWLocale* loc = (const RWLocale*) p;
  p = (void*) this;
  return loc;
}

const RWLocale*
RWLocale::unimbue(ios& s)
{
  void*& p = s.pword(localeIndex);
  const RWLocale* loc = (const RWLocale*) p;
  p = 0;
  return loc;
}

#endif /* RW_IOS_XALLOC_BROKEN */

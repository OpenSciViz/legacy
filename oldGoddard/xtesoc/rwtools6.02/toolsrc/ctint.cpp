/*
 * Definitions for RWCollectable Ints
 *
 * $Id: ctint.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctint.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/05/18  16:31:15  dealys
 * merged with ctintdf.cpp
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   04 Mar 1992 10:27:18   KEFFER
 * Changed PRECONDITION to RWPRECONDITION
 * 
 *    Rev 1.1   28 Oct 1991 09:23:58   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:02   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collint.h"
#include "defcol.h"
                     
RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctint.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWCollectableInt, __RWCOLLECTABLEINT)

RWCollectableInt::RWCollectableInt() : RWInteger() { }         

int
RWCollectableInt::compareTo(const RWCollectable* c) const
{
  RWPRECONDITION( c!=0 );
  return  value() -  ((const RWCollectableInt*)c)->value();
}

unsigned
RWCollectableInt::hash() const
{ return (unsigned)value(); }

RWBoolean
RWCollectableInt::isEqual(const RWCollectable* c) const
{
  RWPRECONDITION( c!=0 );
  if( c->isA() != RWCollectableInt::isA() ) return FALSE;
  return  value() ==  ((const RWCollectableInt*)c)->value();
}


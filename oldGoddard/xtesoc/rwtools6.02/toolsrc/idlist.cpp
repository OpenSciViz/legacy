/*
 * RWIsvDlist: Definitions for intrusive doubly-linked lists.
 *
 * $Id: idlist.cpp,v 2.9 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: idlist.cpp,v $
 * Revision 2.9  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.8  1993/05/31  21:46:32  keffer
 * New messaging architecture for localization
 *
 * Revision 2.7  1993/05/18  00:48:57  keffer
 * Introduced new exception handling classes
 *
 * Revision 2.6  1993/05/14  20:30:14  dealys
 * removed iterator code
 *
 * Revision 2.5  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.4  1993/03/24  23:49:27  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.3  1993/01/29  20:09:26  keffer
 * Added precondition in removeReference()
 *
 * Revision 2.2  1993/01/23  00:34:51  keffer
 * Performance enhancements; simplified; flatter inheritance tree.
 *
 *
 *    Rev 1.2   28 May 1992 16:24:14   KEFFER
 * Introduced RWPRECONDITION2 macro
 * 
 *    Rev 1.1   25 May 1992 15:38:12   KEFFER
 * 
 *    Rev 1.0   15 Apr 1992 19:48:38   KEFFER
 * Initial revision.
 * 
 */

/*
 * Member data head_.next_ points to the actual start of
 * the list, or tail_ if there are no members of the list.
 * Member data head_.prev_ always points to itself.
 *
 * Member data tail_.prev_ points to the last member of
 * the list, or to head_ if there are no members of the list.
 * Member data tail_.next_ always points to itself.
 */

#include "rw/idlist.h"
#include "rw/rwerr.h"
#include "rw/toolerr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: idlist.cpp,v $ $Revision: 2.9 $ $Date: 1993/09/10 03:59:57 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

/********************************************************
*							*
*		RWIsvDlist definitions			*
*							*
********************************************************/


/*
 * Construct a list with one link, given by "a".  In the "debug" version,
 * the "next" pointer in "a" must be nil, signalling that it is not being
 * used in any other list.
 */
RWIsvDlist::RWIsvDlist(RWIsvDlink* a)
{
  RWPRECONDITION2(a, "RWIsvDlist::RWIsvDlist(RWIsvDlink*): Attempt to construct with nil pointer");
  RWPRECONDITION2(a->next_==rwnil, "RWIsvDlist::RWIsvDlist(RWIsvDlink*): Attempt to add intrusive link to more than one list");

  head_.next_ = tail_.prev_ = a;
  a->next_    = tail_.next_ = &tail_;
  a->prev_    = head_.prev_ = &head_;
  nitems_     = 1;
}

/*
 * Find link "i".  The index "i" must be in range.
 */
RWIsvDlink*
RWIsvDlist::at(size_t i) const
{
  if(i>=entries())
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX,
				   (unsigned)i,
				   (unsigned)entries()-1) ));

  register RWIsvDlink* link = head_.next_;
  while (i--) link = link->next_;
  return link;
}

/*
 * Clear the list.  The debug implementation walks the list, setting each
 * "next" pointer to nil.  This allows other routines to detect whether
 * a link is being inserted into more than one collection.
 */
void   
RWIsvDlist::clear()
{
#ifdef RWDEBUG
  register RWIsvDlink* link = first();
  while (link)
  {
    link = link->clear();
  }
#endif
  init();
}

RWBoolean
RWIsvDlist::containsReference(const RWIsvDlink* a) const
{
  RWPRECONDITION2(a, "RWIsvDlist::containsReference(const RWIsvDlink*): nil pointer");
  RWPRECONDITION(head_.next_ != rwnil);

  if (a)
  {
    const RWIsvDlink* cursor = head_.next_;
    // Cast away constness to suppress unwarranted cfront warning:
    while (cursor != &((RWIsvDlist*)this)->tail_)
    {
      if (cursor == a) return TRUE;
      cursor = cursor->next_;
    }
  }
  return FALSE;
}


/*
 * Put link "a" at position "i".  The link formerly at
 * position "i" becomes link "i+1".
 */
void
RWIsvDlist::insertAt(size_t i, RWIsvDlink* a)
{
  RWPRECONDITION2(a, "RWIsvDlist::insertAt(size_t,RWIsvDlink*): nil pointer");
  RWPRECONDITION2(a->next_==rwnil, "RWIsvDlist::insertAt(size_t,RWIsvDlink*): Attempt to add intrusive link to more than one list");

  if(i>entries())
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX, (unsigned)i, (unsigned)entries()-1) ));

  RWIsvDlink* prev = i ? at(i-1) : &head_;

  RWASSERT(prev);

  insertAfterLink(prev, a);
}


/*
 * Remove and return the link at position "i".
 */
RWIsvDlink*
RWIsvDlist::removeAt(size_t i)
{
  if(i>=entries())
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX, (unsigned)i, (unsigned)entries()-1) ));

  return removeReference(at(i));
}


/*
 * Remove and return the link with address "a".  Returns nil if the
 * link is nil.
 */

RWIsvDlink*
RWIsvDlist::removeReference(RWIsvDlink* a)
{
  if (!a)
    return rwnil;

  RWPRECONDITION2(a != &head_ && a != &tail_, "RWIsvDlist::removeReference(RWIsvDlink*): Internal error");

  a->prev_->next_ = a->next_;
  a->next_->prev_ = a->prev_;

#ifdef RWDEBUG
  a->next_ = rwnil;
#endif

  nitems_--;

  return a;
}

/********************************************************
*							*
*		RWIsvDlist protected functions		*
*							*
********************************************************/

void
RWIsvDlist::init()
{
  head_.next_ = tail_.next_ = &tail_;
  head_.prev_ = tail_.prev_ = &head_;
  nitems_     = 0;
}


/*
 * Protected function to insert an object. 
 * Link "a" is inserted to the right of link "sl".
 * Neither "a" nor "sl" can be nil.
 */
void
RWIsvDlist::insertAfterLink(RWIsvDlink* sl, RWIsvDlink* a)
{
  RWPRECONDITION2(sl!=rwnil, "RWIsvDlist::insertAfterLink(RWIsvDlink*, RWIsvDlink*): Internal error: attempt to add link after nil link");
  RWPRECONDITION2(a!=rwnil, "RWIsvDlist::insertAfterLink(RWIsvDlink*, RWIsvDlink*): Attempt to add nil link");
  RWPRECONDITION2(a->next_==rwnil, "RWIsvDlist::insertAfterLink(RWIsvDlink*, RWIsvDlink*): Attempt to add intrusive link to more than one list");

  a->next_  = sl->next_;
  a->prev_  = sl;
  a->next_->prev_ = sl->next_ = a;
  ++nitems_;
}

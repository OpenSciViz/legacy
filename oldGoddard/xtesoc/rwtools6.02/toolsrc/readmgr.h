#ifndef __RWREADMGR_H__
#define __RWREADMGR_H__

/*
 * RWReadManager: returns a unique RWReadTable for each thread.
 *
 * $Id: readmgr.h,v 1.3 1993/11/09 09:01:23 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: readmgr.h,v $
 * Revision 1.3  1993/11/09  09:01:23  jims
 * Port to ObjectStore
 *
 * Revision 1.2  1993/08/05  01:07:10  jims
 * Use rwfar instead of FAR
 *
 * Revision 1.1  1993/04/12  12:10:43  jims
 * Initial revision
 *
 * Revision 2.1  1993/02/05  07:28:23  jims
 * Modified to use new version of Instance Manager
 *
 * Revision 2.0  1992/10/23  03:36:49  keffer
 * RCS Baseline version
 *
 * 
 */

#include "rw/tooldefs.h"  // Use to stem "unreasonable include nesting"
#include "rwstore.h"
#include "rw/instmgr.h"

class RWReadManager : public RWInstanceManager {
public:
  RWReadTable*		currentReadTable();
  RWReadTable*		newReadTable();

  // Inherited from RWInstanceManager:
  virtual void rwfar*	newValue();
  virtual void          deleteValue(void rwfar* value);
};


extern RWReadManager rwReadManager;

#endif	/* __RWREADMGR_H__ */


/*
 * Definitions for RWHashTableIterator.
 *
 * $Id: hashtbit.cpp,v 1.3 1993/11/09 08:37:30 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: hashtbit.cpp,v $
 * Revision 1.3  1993/11/09  08:37:30  jims
 * Port to ObjectStore
 *
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  20:15:42  dealys
 * Initial revision
 *
 *
 */

#include "rw/hashtab.h"
#include "rw/slistcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: hashtbit.cpp,v $ $Revision: 1.3 $ $Date: 1993/11/09 08:37:30 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

#define BUCKET         RWSlistCollectables  
#define BUCKETITERATOR RWSlistCollectablesIterator


/****************************************************************
 *								*
 *			RWHashTableIterator			*
 *								*
 ****************************************************************/


RWHashTableIterator::RWHashTableIterator(RWHashTable& h) :
 myHash_(&h),
 idx_(RW_NPOS),
 iterator_(rwnil)
{
  reset();
}

RWHashTableIterator::RWHashTableIterator(const RWHashTableIterator& h) :
 myHash_(h.myHash_),
 idx_(h.idx_)
{
  iterator_ = h.iterator_ ? new BUCKETITERATOR(*h.iterator_) : rwnil;
}

RWHashTableIterator::~RWHashTableIterator()
{
   delete iterator_;
}

RWHashTableIterator&
RWHashTableIterator::operator=(const RWHashTableIterator& h)
{
  if (this != &h)
  {
    delete iterator_;
    myHash_   = h.myHash_;
    idx_      = h.idx_;
    iterator_ = h.iterator_ ? new BUCKETITERATOR(*h.iterator_) : rwnil;
  }
  return *this;
}

RWCollectable*
RWHashTableIterator::operator()()
{
  RWCollectable* p=rwnil;
  while (iterator_ && (p=(*iterator_)())==rwnil )
    nextIterator();
  return p;
}

RWCollectable*
RWHashTableIterator::findNext(const RWCollectable* a)
{
  RWCollectable* p=rwnil;
  while (iterator_ && (p=iterator_->findNext(a))==rwnil )
    nextIterator();
  return p;
}

RWCollectable*
RWHashTableIterator::remove()
{
  RWCollectable* p = iterator_ ? iterator_->remove() : rwnil;
  if (p) --myHash_->nitems_;
  return p;
}

RWCollectable*
RWHashTableIterator::removeNext(const RWCollectable* a)
{
  RWCollectable* p=rwnil;
  while (iterator_ && (p=iterator_->removeNext(a))==rwnil )
    nextIterator();
  if (p) --myHash_->nitems_;
  return p;
}

void
RWHashTableIterator::reset()
{
  delete iterator_;
  iterator_ = rwnil;

  for (idx_=0; idx_ < myHash_->buckets(); idx_++)
  {
    if (myHash_->table_(idx_))
    {
      iterator_ = new BUCKETITERATOR(*myHash_->table_(idx_));
      return;
    }
  }
}

RWCollectable*
RWHashTableIterator::key() const
{
   return iterator_ ? iterator_->key() : rwnil;
}

void
RWHashTableIterator::nextIterator()
{
  delete iterator_;		// Invalidate old iterator
  iterator_ = rwnil;

  while (++idx_ < myHash_->buckets())
  {
    if (myHash_->table_(idx_))
    {
      iterator_ = new BUCKETITERATOR(*myHash_->table_(idx_));
      return;
    }
  }
}


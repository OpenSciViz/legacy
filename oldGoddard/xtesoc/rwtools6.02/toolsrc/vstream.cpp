/*
 * Definitions for abstract base virtual streams class
 *
 * $Id: vstream.cpp,v 1.3 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: vstream.cpp,v $
 * Revision 1.3  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.2  1993/04/15  00:49:18  keffer
 * Added RWvios::dummy() to be used for virtual function definition
 * heuristics.
 *
 * Revision 1.1  1993/02/23  14:58:50  alv
 * Initial revision
 *
 */

#include "rw/vstream.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: vstream.cpp,v $ $Revision: 1.3 $ $Date: 1993/09/10 03:59:57 $");

int RWvios::dummy() const { return 0; }

/*
 * RWTimer: measures elapsed time
 *
 * $Id: timer.cpp,v 2.7 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: timer.cpp,v $
 * Revision 2.7  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.6  1993/01/25  21:59:54  keffer
 * Simplified absoluteTime() so that it now relies on clock()
 * for all machines.
 *
 * Revision 2.4  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 *    Rev 1.0   19 May 1992 15:30:42   KEFFER
 * Initial revision.
 */

#include "rw/timer.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: timer.cpp,v $ $Revision: 2.7 $ $Date: 1993/09/10 03:59:57 $");

#ifdef RW_NO_CLOCK

  // The function clock() is not declared in either <time.h>
  // (which is where it should be according to Standard C), 
  // or in <stdlib.h> (a common alternative).  Declare it
  // here.
  // This is a traditional representation for clock().
  // You may have to adjust CLOCKS_PER_SEC to match your own machine.

  extern "C" long clock();
  static const double CLOCKS_PER_SEC = 1e06;

#else

# include <time.h>
# include <stdlib.h>

#endif

RWTimer::RWTimer() :
  startTime_(0),
  stopTime_(0),
  isStopped_(TRUE)
{
}

double
RWTimer::elapsedTime() const
{
  return (isStopped_ ? stopTime_ : absoluteTime()) - startTime_;
}

void
RWTimer::reset()
{
  startTime_ = 0;
  stopTime_  = 0;
  isStopped_ = TRUE;
}

void
RWTimer::start()
{
  startTime_ = absoluteTime() - elapsedTime();
  isStopped_ = FALSE;
}

void
RWTimer::stop()
{
  stopTime_ = absoluteTime();
  isStopped_ = TRUE;
}

/************************************************
 *						*
 *		Private functions		*
 *						*
 ************************************************/

double
RWTimer::absoluteTime()
{
  return (double)clock() / CLOCKS_PER_SEC;
}

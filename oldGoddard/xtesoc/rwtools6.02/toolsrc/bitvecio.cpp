
/*
 * RWBitVec I/O
 *
 * $Id: bitvecio.cpp,v 2.10 1993/11/09 05:27:54 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: bitvecio.cpp,v $
 * Revision 2.10  1993/11/09  05:27:54  jims
 * Port to ObjectStore
 *
 * Revision 2.9  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.8  1993/06/06  00:45:00  keffer
 * Prettied up formatting on output.
 *
 * Revision 2.7  1993/05/18  00:45:13  keffer
 * Moved overloaded shift operators from bitvec.cpp to here.
 *
 * Revision 2.6  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.4  1993/02/23  04:29:24  myersn
 * fix scanFrom() to eat white space properly.
 *
 * Revision 2.3  1993/01/27  20:02:53  keffer
 * Changed variable names.
 *
 * Revision 2.2  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.1  1992/11/15  22:00:08  keffer
 * No longer uses function rwEatwhite()
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   29 May 1992 09:46:36   KEFFER
 * Introduced CPP_ANSI_RECURSION macro
 * 
 *    Rev 1.4   27 May 1992 18:08:54   KEFFER
 * RWMEMCK compatible.
 * 
 *    Rev 1.3   24 Mar 1992 19:27:32   KEFFER
 * Ported to MetaWare High C.
 * 
 *    Rev 1.2   28 Oct 1991 09:23:50   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.1   08 Sep 1991 19:12:54   keffer
 * Changed name of eatwhite() to rwEatwhite()
 * 
 *    Rev 1.0   28 Jul 1991 08:38:46   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/bitvec.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: bitvecio.cpp,v $ $Revision: 2.10 $ $Date: 1993/11/09 05:27:54 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

const size_t default_resize = 32;	// Used for input

ostream&
RWBitVec::printOn(ostream& strm) const
{
  strm << "[\n  ";
  for (size_t i=0; i<length(); i++) {
    if(i>0 && (i%25 == 0)) strm << "\n  ";
    strm << ( (*this)(i) ? "1 " : "0 ");
  }
  return strm << "\n]";
}

void
RWBitVec::restoreFrom(RWvistream& strm)
{
  size_t oldnb = nbytes();		// Save old data space length

  strm >> npts_;		// Find new number of bits
				
  if( strm.good() ){
    size_t nb = nbytes();	// Get new data space length
    if( nb!=oldnb) { 		// Get new data space if they differ
      delete vec_;
      vec_ = new RWByte[nb];
    }
    strm.get((char*)vec_,nb);	// Get the bit pattern
  }
}

void
RWBitVec::restoreFrom(RWFile& file)
{
  size_t oldnb = nbytes();	// Save old data space length

  if( file.Read(npts_) ){
    size_t nb = nbytes();	// Get new data space length
    if( nb!=oldnb) { 		// Get new data space if they differ
      delete vec_;
      vec_ = new RWByte[nb];
    }
    file.Read((char*)vec_, nb);
  }
}

istream&
RWBitVec::scanFrom(istream& s)
{
  register size_t nextspace = 0;
  RWBoolean item;
  char c = 0;	// The first character read from the stream
  
  s >> ws >> c;	// Munch through any leading white space
  
  if (s && c != '[') {
    // Scan input stream, resizing as necessary.
    // Keep scanning till we can't scan no mo'
    s.putback(c);
    while( (s >> item).good() ) {
      if(nextspace >= length()) resize(length()+default_resize);
      (*this)(nextspace++) = item;
    }
  } else {  // Scan input stream, stop scanning at the matching ']' character
    
    s >> ws >> c;	// Munch through any leading white space

    while ( s && (c != ']')  ) {
      s.putback(c);
      if( (s >> item).good() ) {
        if( nextspace >= length() ) resize(length()+default_resize);
        (*this)(nextspace++) = item;
	s >> ws >> c;
      }
    }
  }

  if( s.good() ) {
    if( nextspace != length() ) resize(nextspace);
  }

  return s;
}

void
RWBitVec::saveOn(RWvostream& strm) const
{
  strm << npts_;
  size_t nb = nbytes();
  strm.put((char*)vec_,nb);
}
  
void
RWBitVec::saveOn(RWFile& file) const
{
  file.Write(npts_);
  file.Write((char*)vec_, nbytes());
}

ostream& rwexport
operator<<(ostream& strm, const RWBitVec& u)
{
  return u.printOn(strm);
}

istream& rwexport
operator>>(istream& strm, RWBitVec& u)
{
  return u.scanFrom(strm);
}

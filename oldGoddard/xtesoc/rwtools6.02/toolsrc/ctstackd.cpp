/*
 * $Id: ctstackd.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctstackd.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   05 Nov 1991 14:06:00   keffer
 * Removed RWXXXXErr macros --- now done by error package
 * 
 *    Rev 1.0   28 Jul 1991 08:39:08   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/stackcol.h"
#include "defcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctstackd.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWSlistCollectablesStack, __RWSLISTCOLLECTABLESSTACK)

RWSlistCollectablesStack::RWSlistCollectablesStack() { }


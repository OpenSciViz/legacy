/*
 * Definitions for RWSet --- hashed table lookups.
 *
 * $Id: rwset.cpp,v 2.5 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. 
 * This software is subject to copyright protection under the laws of 
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwset.cpp,v $
 * Revision 2.5  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.4  1993/05/18  16:37:03  dealys
 * merged with rwsetdf.cpp
 *
 * Revision 2.3  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.2  1993/03/24  23:49:27  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.10   25 May 1992 15:35:50   KEFFER
 * Optimized, reducing size.
 * 
 *    Rev 1.9   29 Apr 1992 14:50:58   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 */

#include "rw/rwset.h"
#include "rw/slistcol.h"
#include "defcol.h"
#include "rw/toolerr.h"   

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: rwset.cpp,v $ $Revision: 2.5 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWSet, __RWSET)

// Constructor for hash table of specified size:
RWSet::RWSet(size_t N) : RWHashTable(N) { }
                                                      
                          

/************* Canonical collection class functions ****************/

/*
 * Offer a special definition of clearAndDestroy() for Sets that can
 * optimize on the fact that duplicates are not allowed.
 */
void
RWSet::clearAndDestroy()
{
  apply (rwDestroy, 0);
  RWSet::clear();
}

/*
 * Add an item to this table.  If it is already in the
 * table, then return the old instance. 
 */
RWCollectable*
RWSet::insert(RWCollectable* a)
{
  RWCollectable* ret;
  size_t idx = hashIndex(a);
  if (table_(idx)==rwnil || (ret=table_(idx)->find(a))==rwnil) {
    insertIndex(idx, a);
    ret = a;
  }
  return ret;
}

/*
 * Override occurrencesOf() to something more efficient:
 */
size_t
RWSet::occurrencesOf(const RWCollectable* a) const
{
  return find(a) ? 1 : 0;
}

/************************ Special RWSet functions ***************************/

/* override operator<= for set since it is easier */
RWBoolean
RWSet::operator<=(const RWSet& h) const
{
  RWCollectable* p;
  RWSetIterator iterator(*(RWSet*)this);	// Cast away constness
  while ((p=iterator())!=rwnil) {
    if (contains(p) && !h.contains(p)) return FALSE;
  }
  return TRUE;
}

/************************ RWSet utilities ****************************/

RWBoolean
RWSet::isEqual(const RWCollectable* c) const
{
  if( c->isA() != __RWSET ) return FALSE;
  return RWSet::operator==(*(const RWSet*)c);
}

/********************* Conversion functions ****************************/

RWSet
RWCollection::asSet() const
{
  RWSet aSet( entries()/2 );	// Pick a lambda of 2.0
  aSet += *this;
  return aSet;
}


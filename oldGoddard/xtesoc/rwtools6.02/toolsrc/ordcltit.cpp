/*
 * Definitions for RWOrderedIterator
 *
 * $Id: ordcltit.cpp,v 1.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ordcltit.cpp,v $
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  21:01:16  dealys
 * Initial revision
 *
 *
 */

#include "rw/ordcltn.h"
#include "rw/toolerr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ordcltit.cpp,v $ $Revision: 1.2 $ $Date: 1993/09/10 03:59:57 $");


/********************** RWOrderedIterator methods *****************************/

RWCollectable*
RWOrderedIterator::findNext(const RWCollectable* c)
{
  while( ++here < theCollection->nitems )
    if(theCollection->vec(here)->isEqual(c)) return theCollection->vec(here);
  return rwnil;
}

RWCollectable*
RWOrderedIterator::key() const
{
  return here==RW_NPOS ? rwnil : theCollection->vec(here);
}

RWCollectable*
RWOrderedIterator::operator()()
{
  return ++here<theCollection->nitems ? theCollection->vec(here) : rwnil;
}

/*
 * Definitions for class RWTime
 *
 * $Id: rwtime.cpp,v 2.22 1993/10/29 06:15:58 myersn Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Implementation of class RWTime
 * Provides an object that represents a RWTime, stored as the number of
 * seconds since January 1, 1901.
 *
 * Parts of this code have been taken from "The NIH Class Library",
 * a public domain C++ class library written by Keith Gorlen, of the
 * National Institute of Health.
 *
 ***************************************************************************
 *
 * $Log: rwtime.cpp,v $
 * Revision 2.22  1993/10/29  06:15:58  myersn
 * check for a bad date when constructing a time from one.
 *
 * Revision 2.21  1993/09/15  19:12:15  randall
 * changed BSD guard to RW_STRUCT_TM_TZ for extra struct tm members
 *
 * Revision 2.20  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.19  1993/08/06  04:33:45  randall
 * removed use of RW_CAFE_ALPHA
 *
 * Revision 2.18  1993/07/26  05:07:24  myersn
 * under BSD, set struct tm members tm_zone and tm_gmtoff.
 *
 * Revision 2.17  1993/07/09  09:40:29  jims
 * Use localtime_r for Sun only
 *
 * Revision 2.16  1993/06/02  22:24:29  randall
 * added use of mt-safe function localtime_r(...)
 *
 * Revision 2.15  1993/05/01  01:16:43  keffer
 * refDate and maxDate are no longer static data in RWTime.
 *
 * Revision 2.14  1993/04/12  19:25:54  keffer
 * Added static member function RWTime::now().
 *
 * Revision 2.12  1993/03/31  02:52:39  myersn
 * add constructor from RWCString and RWLocale.
 *
 * Revision 2.11  1993/03/26  23:26:45  myersn
 * fix RWTime::extract() setting tm_isdst member.
 *
 * Revision 2.10  1993/03/25  05:55:15  myersn
 * change minuteTy etc. to unsigned.  secFrom_Jan_1_(etc) becomes rwEpoch.
 *
 * Revision 2.9  1993/03/17  21:21:15  keffer
 * far->rwfar; added SYSV and BSD to unix
 *
 * Revision 2.8  1993/02/17  03:12:30  keffer
 * Changed const notation to follow style guide
 *
 * Revision 2.7  1992/12/04  05:02:03  myersn
 * make RWLocale::global() and RWZone::local() more flexible
 *
 * Revision 2.6  1992/12/02  02:38:00  myersn
 * removed mutual recursion between RWTime::extract() and
 * RWDate::RWDate(const RWTime&, const RWZone&)
 *
 * Revision 2.5  1992/12/01  05:44:03  myersn
 * *** empty log message ***
 *
 * Revision 2.4  1992/11/27  21:01:11  myersn
 * added RWZone interface
 *
 * Revision 2.4  1992/11/27  21:01:11  myersn
 * added RWZone interface
 *
 * Revision 2.3  1992/11/20  02:55:42  myersn
 * adjustments for support of locales, time zones, and struct tm
 *
 * Revision 2.2  1992/11/14  00:15:39  myersn
 * fold in interface to struct tm and RWLocale
 *
 * Revision 2.1  1992/11/02  00:38:03  myersn
 * fix spelling of "Bering" time zone name
 * 
 *    Rev 1.11   07 Jun 1992 14:27:22   KEFFER
 * Ported to Liant: added prototype for gettimeofday().
 * 
 *    Rev 1.10   24 Apr 1992 10:39:46   KEFFER
 * Ported to DEC's C++.
 * M.F. localSecs() now adjusts for DST using UTC, not local time.
 * Simplified buildFrom() algorithm.
 * 
 *    Rev 1.9   04 Mar 1992 10:27:20   KEFFER
 * Changed PRECONDITION to RWPRECONDITION
 * 
 *    Rev 1.8   25 Nov 1991 12:40:00   keffer
 * TIME_ZONE is now consistently declared "long"
 * 
 *    Rev 1.6   13 Nov 1991 11:49:18   keffer
 * TIME_ZONE and DST_OBSERVED obtained from functions for the DLL version.
 * 
 *    Rev 1.5   28 Oct 1991 09:24:28   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.4   20 Sep 1991 19:07:34   keffer
 * Added additional preconditions.
 * Corrected error in compareTo().
 * 
 *    Rev 1.3   08 Sep 1991 19:25:40   keffer
 * Corrected type conversion error for times between midnight and 1AM.
 * 
 *    Rev 1.2   22 Aug 1991 10:19:40   keffer
 * Simplified construction of times by defining m.f. buildFrom()
 * 
 *    Rev 1.1   29 Jul 1991 14:42:12   keffer
 * VMS must also hardwire in timezone.
 * 
 */

#include "rw/rwdate.h"
#include "rw/rwtime.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: rwtime.cpp,v $ $Revision: 2.22 $ $Date: 1993/10/29 06:15:58 $");

static const unsigned long SECONDS_IN_DAY  = 86400L;
static const unsigned long SECONDS_IN_HOUR = 3600L;
static const unsigned      SECONDS_IN_MIN  = 60;

// RWTime constants

const RWDate refDate((unsigned)0, (unsigned)0);
const RWDate maxDate((unsigned)49709, (unsigned)0); // ((2**32)-1)/SECONDS_IN_DAY -1

/****************************************************************
 *								*
 *			CONSTRUCTORS				*
 *								*
 ****************************************************************/

// Specified time and today's date:
RWTime::RWTime(unsigned h,
	       unsigned m,
	       unsigned s,
	       const RWZone& zone)
{
  RWDate today;
  sec = RWTime::buildFrom(today, h, m, s, zone);
}

RWTime::RWTime(const struct tm* tmbuf,
	       const RWZone& zone)
{
  RWDate date(tmbuf);
  sec = RWTime::buildFrom(date, tmbuf->tm_hour,
			  tmbuf->tm_min, tmbuf->tm_sec, zone);
}

RWTime::RWTime(const RWDate& date,
               const RWCString& str,
               const RWZone& zone,
               const RWLocale& locale)
{
  struct tm tmbuf;
  sec = date.isValid() && locale.stringToTime(str, &tmbuf) ?
    buildFrom(date,
	      tmbuf.tm_hour,
	      tmbuf.tm_min,
	      tmbuf.tm_sec,
	      zone) : 0;
}

/*************** conversion from RWTime to RWDate *******************/

RWDate::RWDate(const RWTime& time, const RWZone& zone)
{
  struct tm tmbuf;
  time.extract(&tmbuf, zone);
  *this = RWDate(&tmbuf);
}

/****************************************************************
 *								*
 *			PUBLIC FUNCTIONS			*
 *								*
 ****************************************************************/

int
RWTime::compareTo(const RWTime* t) const
{
  RWPRECONDITION( t!=0 );
  RWPRECONDITION( RWTime::isValid() );
  RWPRECONDITION( t->RWTime::isValid() );
  return sec == t->sec ? 0 : (sec > t->sec ? 1 : -1);
}

// Hash function:
unsigned
RWTime::hash() const
{
  RWPRECONDITION( RWTime::isValid() );
  return (unsigned)sec;
}

void
RWTime::extract(struct tm* tmbuf,
		const RWZone& zone) const
{
  if (!isValid()) { RWDate::clobber(tmbuf); return; }
  if (zone.daylightObserved()) {
    RWTime daytime(sec - zone.altZoneOffset());
    daytime.extract(tmbuf, RWZone::utc());  // this recursive call is OK.
    if (zone.isDaylight(tmbuf)) {
      tmbuf->tm_isdst = 1;
#ifdef RW_STRUCT_TM_TZ
      tmbuf->tm_zone = (char*) zone.altZoneName().data();
      tmbuf->tm_gmtoff = zone.altZoneOffset();
#endif
      return;
    }
  }
  RWTime offtime(sec - zone.timeZoneOffset());
  RWDate date((unsigned)(offtime.sec / SECONDS_IN_DAY), 0);
  date.extract(tmbuf);
  tmbuf->tm_hour = offtime.hourGMT();
  tmbuf->tm_min = offtime.minuteGMT();
  tmbuf->tm_sec = offtime.second();
  tmbuf->tm_isdst = 0;
#ifdef RW_STRUCT_TM_TZ
  tmbuf->tm_zone = (char*) zone.timeZoneName().data();
  tmbuf->tm_gmtoff = zone.timeZoneOffset();
#endif  
}

unsigned
RWTime::hour(const RWZone& zone) const
{
  RWPRECONDITION( RWTime::isValid() );
  struct tm tmbuf;
  extract(&tmbuf, zone);
  return tmbuf.tm_hour;
}

unsigned   
RWTime::hourGMT() const 
{
  RWPRECONDITION( RWTime::isValid() );
  return (sec % SECONDS_IN_DAY) / SECONDS_IN_HOUR;
} 

RWBoolean 
RWTime::isDST(const RWZone& zone) const
{
  RWPRECONDITION( RWTime::isValid() );
  if (!zone.daylightObserved()) return FALSE;
  struct tm tmbuf;
  RWTime off(sec + zone.altZoneOffset());
  extract(&tmbuf, RWZone::utc());
  return zone.isDaylight(&tmbuf);
}

RWTime
RWTime::max(const RWTime& t) const 
{
  RWPRECONDITION( RWTime::isValid() );
  RWPRECONDITION( t.RWTime::isValid() );
  if ( *this > t ) return *this;
  return t;
}

RWTime
RWTime::min(const RWTime& t) const 
{
  RWPRECONDITION( RWTime::isValid() );
  RWPRECONDITION( t.RWTime::isValid() );
  if ( *this < t ) return *this;
  return t;
}

unsigned
RWTime::minute(const RWZone& zone) const
{
  RWPRECONDITION( RWTime::isValid() );
  struct tm tmbuf;
  extract(&tmbuf, zone);
  return tmbuf.tm_min;
}

unsigned 
RWTime::minuteGMT() const 
{
  RWPRECONDITION( RWTime::isValid() );
  return ((sec % SECONDS_IN_DAY) % SECONDS_IN_HOUR) / SECONDS_IN_MIN;
} 

// second(); local time or UTC

unsigned 
RWTime::second() const 
{
  RWPRECONDITION( RWTime::isValid() );
  return ((sec % SECONDS_IN_DAY) % SECONDS_IN_HOUR) % SECONDS_IN_MIN;
}

/*************** public static member functions *******************/

/* static */ RWTime 
RWTime::beginDST(unsigned year, const RWZone& zone)
{
  if (zone.daylightObserved()) {
    struct tm tmbuf;
    tmbuf.tm_year = year - 1900;
    zone.getBeginDaylight(&tmbuf);
    return RWTime(&tmbuf, zone);
  } else
    return RWTime((unsigned long) 0);
}

/* static */ RWTime
RWTime::endDST(unsigned year, const RWZone& zone)
{
  if (zone.daylightObserved()) {
    struct tm tmbuf;
    tmbuf.tm_year = year - 1900;
    zone.getEndDaylight(&tmbuf);
    return RWTime(&tmbuf, zone);
  } else
    return RWTime((unsigned long)0);
}

/* static */ RWTime
RWTime::now()
{
  return RWTime(RWTime::currentTime());
}

/****************************************************************
 *								*
 *			PROTECTED FUNCTIONS			*
 *								*
 ****************************************************************/

RWBoolean 
RWTime::assertDate(const RWDate& date)
{
  return date.between(refDate,maxDate);
}   

/*
 * Set self from the specified (local) Date, hour, minute, and second.
 * Note: this algorithm will fail if DST correction is something other
 * than an hour.
 * It is complicated by the DST boundary problem: 
 * 1) Times in the phantom zone between 2AM and 3AM when DST is invoked
 *    are invalid.
 * 2) Times in the hour after 1AM when DST ends, are redundant.
 *    In this case, standard time should be chosen.
 */



/****************************************************************
 *								*
 *			PRIVATE FUNCTIONS			*
 *								*
 ****************************************************************/

/* static */ unsigned long
RWTime::buildFrom(
  const RWDate& date,
  unsigned h, unsigned m, unsigned s,
  const RWZone& zone)
{
  if (!date.isValid()) return 0;
  unsigned long secs = SECONDS_IN_DAY  * (date - refDate) 
                     + SECONDS_IN_HOUR * h
                     + SECONDS_IN_MIN  * m
                     +                   s;
  if (zone.daylightObserved())
  {
    struct tm tmbuf;
    date.extract(&tmbuf);
    tmbuf.tm_hour = h; tmbuf.tm_min = m; tmbuf.tm_sec = s;
    tmbuf.tm_wday = date.weekDay() % 7;  // convert to [0..6] range
    if (zone.isDaylight(&tmbuf))
    { 
      secs += zone.altZoneOffset();
      return secs;
    }
  }
  secs += zone.timeZoneOffset();  
  return secs;
}

unsigned long
RWTime::currentTime()
{
#if defined (SYSV) || defined (BSD) || defined(unix)
  unsigned long secs = (unsigned long)(time(0));
  secs += rwEpoch;
  return secs;
#else  /* MS-DOS: use time struct. This is awkward, but guarantees correct local time */
  time_t ltime;
  time(&ltime);
  struct tm t;
#if defined (sun) && defined(RW_MULTI_THREAD)
  localtime_r(&ltime, &t);
#else
  t = *localtime(&ltime);
#endif    /*  end RW_MULTI_THREAD  */
  // Construct the date.  The time struct returns int, so casts are used.
  RWDate today((unsigned)t.tm_mday, (unsigned)(t.tm_mon + 1), (unsigned)t.tm_year);
  return buildFrom(today, (unsigned)t.tm_hour, (unsigned)t.tm_min, (unsigned)t.tm_sec);
#endif
}

/*
 * RWCollectableString I/O
 *
 * $Id: ctstrio.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctstrio.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   04 Mar 1992 10:26:54   KEFFER
 * Changed RWString to RWCString
 * 
 *    Rev 1.1   28 Oct 1991 09:24:02   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:12   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collstr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctstrio.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

void
RWCollectableString::restoreGuts(RWvistream& s)
{
  RWCString::restoreFrom(s);
}

void
RWCollectableString::restoreGuts(RWFile& file)
{
  RWCString::restoreFrom(file);
}

void
RWCollectableString::saveGuts(RWvostream& s) const
{
  RWCString::saveOn(s);
}

void
RWCollectableString::saveGuts(RWFile& file) const
{
  RWCString::saveOn(file);
}



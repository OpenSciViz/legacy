/*
 * Definitions for the abstract base class RWCollectable
 *
 * $Id: ct.cpp,v 2.3 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ct.cpp,v $
 * Revision 2.3  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.2  1993/05/18  19:14:01  keffer
 * Merged with ctdf.cpp
 *
 * Revision 2.1  1993/03/17  21:21:15  keffer
 * Return type of binaryStoreSize is now RWspace
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   28 May 1992 15:35:30   KEFFER
 * Introduced RWhashAddress() for identity hashes.
 * 
 *    Rev 1.3   25 May 1992 15:28:20   KEFFER
 * Now includes segment when hashing in RWCollectable::hash().
 * 
 *    Rev 1.2   04 Mar 1992 09:17:18   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.1   28 Oct 1991 09:23:52   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:52   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collect.h"
#include "defcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ct.cpp,v $ $Revision: 2.3 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWCollectable, __RWCOLLECTABLE)

RWCollectable::RWCollectable() { }
RWCollectable::~RWCollectable() { }

RWspace
RWCollectable::binaryStoreSize() const {return 0;}

unsigned
RWCollectable::hash() const { return RWhashAddress((void*)this); }

RWBoolean
RWCollectable::isEqual(const RWCollectable* c) const { return this==c; }

int
RWCollectable::compareTo(const RWCollectable* c) const
{
  return this==c ? 0 : (this>c ? 1 : -1);
}

RWBoolean rwexport
rwIsEqualFun(const void* a, const void* b)
{
  RWPRECONDITION( a!=rwnil );
  return ((const RWCollectable*)a)->isEqual((const RWCollectable*)b);
}


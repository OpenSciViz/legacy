/*
 * Definitions of wide-character string functions that were not supplied 
 *   by the compiler vendor.
 *
 * $Id: wcsutil.cpp,v 1.6 1993/09/13 06:21:26 myersn Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of 
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: wcsutil.cpp,v $
 * Revision 1.6  1993/09/13  06:21:26  myersn
 * add wcscspn definition
 *
 * Revision 1.5  1993/09/12  21:10:21  keffer
 * All wide character utility functions are now declared in rwwchar.h
 *
 * Revision 1.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.3  1993/08/21  22:14:06  keffer
 * Fixed RCS ident.
 *
 *
 */


#include "rw/defs.h"
#include "rwwchar.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: wcsutil.cpp,v $ $Revision: 1.6 $ $Date: 1993/09/13 06:21:26 $");


#if defined(RW_SUPPLY_WSTR)

extern "C" wchar_t *wcschr(const wchar_t *wstr, wchar_t wc)
{
  while (*wstr != wc)
    if (!*wstr++) return NULL;  // not found

  return (wchar_t*) wstr;
}


extern "C" wchar_t *wcsrchr(const wchar_t *wstr, wchar_t wc)
{
  const wchar_t* p = NULL;

  do {
    if (*wstr == wc)
      p = wstr;
  } while (*wstr++);
  
  return (wchar_t*) p;
}

  
extern "C" wchar_t *wcspbrk(const wchar_t *wstr, const wchar_t *wcset)
{
  while (*wstr && !wcschr(wcset, *wstr)) ++wstr;
  
  return *wstr ? (wchar_t*) wstr : NULL;
}


extern "C" size_t wcsspn(const wchar_t *wstr, const wchar_t *wcset)
{
  for (size_t i=0; *wstr; ++i, ++wstr) 
    if (!wcschr(wcset, *wstr)) break;

  return i;
}

extern "C" size_t wcscspn(const wchar_t *wstr, const wchar_t *wcset)
{
  for (size_t i=0; *wstr; ++i, ++wstr) 
    if (wcschr(wcset, *wstr)) break;

  return i;
}


#else  /* RW_SUPPLY_WSTR */

// This is to quiet fussy libraries if nothing gets compiled:
int rwDummy_wcsutil_cpp;

#endif /* RW_SUPPLY_WSTR */

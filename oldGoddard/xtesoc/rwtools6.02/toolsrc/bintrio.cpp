/*
 * RWBinaryTree I/O
 *
 * $Id: bintrio.cpp,v 2.5 1993/09/14 04:10:22 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 * $Log: bintrio.cpp,v $
 * Revision 2.5  1993/09/14  04:10:22  keffer
 * Ported to Symantec.  Does not accept "address of" functions.
 *
 * Revision 2.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.3  1993/07/09  18:14:07  randall
 * for port to Centerline made static member function argument a reference.
 *
 * Revision 2.2  1993/04/14  18:39:16  keffer
 * Moved declaration for RWTreeNode into the .cpp file.
 * Now does level saveGuts(), removing the need for a balance()
 * on restore.
 *
 * Revision 2.1  1992/11/15  22:07:11  keffer
 * Removed use of macro NL
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 * 
 *    Rev 1.2   28 Oct 1991 09:23:48   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.1   28 Jul 1991 12:38:12   keffer
 * No longer uses macro Const
 * 
 *    Rev 1.0   28 Jul 1991 08:38:42   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/bintree.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: bintrio.cpp,v $ $Revision: 2.5 $ $Date: 1993/09/14 04:10:22 $");

void
RWBinaryTree::saveGuts(RWvostream& os) const
{
  os << entries();
  if( os.good() ) { // cast away const to avoid warnings
#ifdef __SC__
    ((RWBinaryTree*)this)->levelApply( RWCollection::saveObjToStream, &os);
#else
    ((RWBinaryTree*)this)->levelApply(&RWCollection::saveObjToStream, &os);
#endif
  }
}
    
void
RWBinaryTree::saveGuts(RWFile& f) const
{
  unsigned N = entries();
  f.Write(N);
  // cast away const to avoid const warnings
#ifdef __SC__
  ((RWBinaryTree*)this)->levelApply( RWCollection::saveObjToRWFile, &f);
#else
  ((RWBinaryTree*)this)->levelApply(&RWCollection::saveObjToRWFile, &f);
#endif
}


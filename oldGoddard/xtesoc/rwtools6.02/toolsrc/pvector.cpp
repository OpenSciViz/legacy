
/*
 * Definitions for RWPtrVector
 *
 * $Id: pvector.cpp,v 1.7 1993/11/09 09:01:23 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: pvector.cpp,v $
 * Revision 1.7  1993/11/09  09:01:23  jims
 * Port to ObjectStore
 *
 * Revision 1.6  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.5  1993/05/31  21:46:32  keffer
 * New messaging architecture for localization
 *
 * Revision 1.4  1993/05/18  00:48:57  keffer
 * Introduced new exception handling classes
 *
 * Revision 1.3  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 1.2  1993/03/24  23:49:27  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.1  1993/02/17  18:26:24  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */

#include "rw/pvector.h"
#include "rw/rwerr.h"
#include "rw/toolerr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: pvector.cpp,v $ $Revision: 1.7 $ $Date: 1993/11/09 09:01:23 $");

RWPtrVector::RWPtrVector(register size_t n, void* p)
{
  register RWvoid* dst = array_ = new RWvoid[npts_=n];
  while (n--) *dst++ = p;
}


RWPtrVector::RWPtrVector(const RWPtrVector& a)
{
  register size_t i= npts_ = a.npts_;
  register RWvoid* dst = array_ = new RWvoid[i];
  register RWvoid* src = a.array_;
  while (i--) *dst++ = *src++;
}

RWPtrVector&
RWPtrVector::operator=(const RWPtrVector& a)
{
  if(array_ != a.array_){
    RWVECTOR_DELETE(npts_) array_;	/* Disconnect from old array_ */
    register size_t i = npts_ = a.npts_;
    register RWvoid* dst = array_ = new RWvoid[i];
    register RWvoid* src = a.array_;
    while (i--) *dst++ = *src++;
  }
  return *this;
}
										
RWPtrVector&
RWPtrVector::operator=(void* p)
{
  for (register size_t i=0; i<npts_; i++)
    array_[i] = p;
  return *this;
}

void
RWPtrVector::reshape(size_t N)
{
  if (N==npts_) return;
  RWvoid* newArray = new RWvoid[N];
  register size_t i = (N<=npts_) ? N:npts_;
  register RWvoid* src = array_;
  register RWvoid* dst = newArray;
  while (i--) *dst++ = *src++;
  RWVECTOR_DELETE(npts_) array_;
  array_ = newArray;
  npts_ = N;
}

void
RWPtrVector::resize(size_t N)
{
  size_t oldN = length();
  reshape(N);
  for (register size_t i = oldN; i<N; i++)
    array_[i] = rwnil;
}

void
RWPtrVector::boundsCheck(size_t i) const
{
  if (i>=npts_)
    RWTHROW( RWBoundsErr(RWMessage(RWTOOL_INDEX, (unsigned)i, (unsigned)npts_-1) ));
}

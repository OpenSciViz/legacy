/*
 * Definitions for RWCollectable Dates
 *
 * $Id: ctdate.cpp,v 2.3 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctdate.cpp,v $
 * Revision 2.3  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.2  1993/05/18  16:18:49  dealys
 * merged with ctdatdf.cpp
 *
 * Revision 2.1  1993/04/07  02:59:02  myersn
 * eliminate use of member operator==
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   28 Oct 1991 09:23:56   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:58   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colldate.h"
#include "defcol.h"  

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctdate.cpp,v $ $Revision: 2.3 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWCollectableDate, __RWCOLLECTABLEDATE)

RWCollectableDate::RWCollectableDate() : RWDate() { }               

int
RWCollectableDate::compareTo(const RWCollectable* c) const
{
  return RWDate::compareTo( (const RWDate*)(const RWCollectableDate*)c );
}

unsigned
RWCollectableDate::hash() const
{
  return RWDate::hash();
}

RWBoolean
RWCollectableDate::isEqual(const RWCollectable* c) const
{
  if( c->isA() != RWCollectableDate::isA() ) return FALSE;
  return *this == *(const RWCollectableDate*)c;
}


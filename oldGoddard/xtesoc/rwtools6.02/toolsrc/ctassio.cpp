/*
 * RWCollectableAssociation I/O
 *
 * $Id: ctassio.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctassio.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   25 May 1992 15:30:02   KEFFER
 * saveOn -> recursiveSaveOn; restoreFrom -> recursiveRestoreFrom;
 * 
 *    Rev 1.1   28 Oct 1991 09:23:54   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:38:54   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collass.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctassio.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

void
RWCollectableAssociation::restoreGuts(RWvistream& strm)
{ 
  ky  =  recursiveRestoreFrom(strm);
  val =  recursiveRestoreFrom(strm);
}

void
RWCollectableAssociation::restoreGuts(RWFile& f)
{ 
  ky  =  recursiveRestoreFrom(f);
  val =  recursiveRestoreFrom(f);
}

void
RWCollectableAssociation::saveGuts(RWvostream& strm) const
{
  ky->recursiveSaveOn(strm);
  val->recursiveSaveOn(strm);
}

void
RWCollectableAssociation::saveGuts(RWFile& f) const
{
  ky->recursiveSaveOn(f);
  val->recursiveSaveOn(f);
}



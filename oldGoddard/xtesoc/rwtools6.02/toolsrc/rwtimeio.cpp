/*
 * RWTime I/O
 *
 * $Id: rwtimeio.cpp,v 2.12 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwtimeio.cpp,v $
 * Revision 2.12  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.11  1993/04/06  17:08:07  myersn
 * use RWLocale::of(s) for operator >> formatting.
 *
 * Revision 2.10  1993/03/25  05:56:33  myersn
 * add zone argument to call of RWLocale::asString in RWTime::asString.
 *
 * Revision 2.9  1993/02/17  03:12:30  keffer
 * Changed const notation to follow style guide
 *
 * Revision 2.8  1993/02/04  20:24:20  jims
 * Added rwexport to operator<<(ostream&, RWTime&)
 *
 * Revision 2.7  1993/01/29  20:20:37  myersn
 * extend RWTime::asString() interface.
 *
 *
 * Revision 2.4  1992/11/27  21:01:11  myersn
 * added RWZone interface
 *
 * Revision 2.3  1992/11/20  02:55:42  myersn
 * adjustments for support of locales, time zones, and struct tm
 *
 * Revision 2.2  1992/11/15  21:51:11  keffer
 * Removed old V1.2 stream support
 *
 * Revision 2.1  1992/11/14  00:15:39  myersn
 * fold in interface to struct tm and RWLocale
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.9   29 May 1992 12:41:58   KEFFER
 * Ported to Glock under Unix.
 * 
 *    Rev 1.8   04 Mar 1992 10:26:56   KEFFER
 * Changed RWString to RWCString
 * 
 *    Rev 1.7   14 Nov 1991 10:08:12   keffer
 * 
 *    Rev 1.6   13 Nov 1991 11:49:06   keffer
 * Static variables now maintained by an instance manager
 * 
 *    Rev 1.5   05 Nov 1991 14:06:44   keffer
 * Removed RWXXXXErr macros --- now done by error package
 * 
 *    Rev 1.4   29 Oct 1991 14:00:04   keffer
 * Added military time (24 hour clock) print option.
 * 
 *    Rev 1.3   28 Oct 1991 09:24:28   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.2   09 Oct 1991 18:09:28   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.1   24 Sep 1991 18:51:30   keffer
 * msg scratch space now comes off the stack
 * 
 */

#include "rw/rwtime.h"
#include "rw/cstring.h"
#include "rw/vstream.h"
#include "rw/rwfile.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: rwtimeio.cpp,v $ $Revision: 2.12 $ $Date: 1993/09/10 03:59:57 $");

RWCString
RWTime::asString(
  char format,
  const RWZone& zone,
  const RWLocale& locale) const
{
  struct tm tmbuf;
  extract(&tmbuf, zone);
  RWCString result;
  if (format == '\0') {
    result = locale.asString(&tmbuf, 'x', zone);
    result += " ";
    format = 'X';
  }
  result += locale.asString(&tmbuf, format, zone);
  return result;
}

ostream& rwexport
operator<<(ostream& s, const RWTime& t)
{
  s << t.asString('\0', RWZone::local(), RWLocale::of(s));
  return s;
}

void
RWTime::restoreFrom(RWvistream& s)
{
  s >> sec;
}

void
RWTime::restoreFrom(RWFile& file)
{
  file.Read(sec);
}

void
RWTime::saveOn(RWvostream& s) const
{
  s << sec;
}

void
RWTime::saveOn(RWFile& file) const
{
  file.Write(sec);
}

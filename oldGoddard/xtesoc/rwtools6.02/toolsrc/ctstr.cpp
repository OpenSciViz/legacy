/*
 * Definitions for RWCollectable Strings
 *
 * $Id: ctstr.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctstr.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/05/18  16:34:06  dealys
 * merged with ctstrdf.cpp
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   04 Mar 1992 10:26:52   KEFFER
 * Changed RWString to RWCString
 * 
 *    Rev 1.1   28 Oct 1991 09:24:00   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:10   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collstr.h"
#include "defcol.h"     

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctstr.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWCollectableString, __RWCOLLECTABLESTRING)

RWCollectableString::RWCollectableString() : RWCString() { }
                                                                  
int 
RWCollectableString::compareTo(const RWCollectable* c) const
{
  return RWCString::compareTo(*(const RWCString*)(const RWCollectableString*)c);
}

unsigned 
RWCollectableString::hash() const
{
  return RWCString::hash();
}  

RWBoolean
RWCollectableString::isEqual(const RWCollectable* c) const
{
  if( c->isA() != RWCollectableString::isA() ) return FALSE;
  return RWCString::compareTo(*(const RWCString*)(const RWCollectableString*)c)==0;
}


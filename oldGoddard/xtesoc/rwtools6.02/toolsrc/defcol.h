#ifndef __RWDEFCOL_H__
#define __RWDEFCOL_H__

/*
 * This header file is used internally by Tools.h++.  When compiling
 * Tools.h++ as a DLL, it does not insert a creator function into the
 * Factory.  Otherwise it does.  
 *
 * $Id: defcol.h,v 2.2 1993/08/05 11:47:26 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: defcol.h,v $
 * Revision 2.2  1993/08/05  11:47:26  jims
 * Distinguish between WIN16 DLLs and WIN32 DLLs by checking
 * for __WIN16__
 *
 * Revision 2.1  1993/03/13  02:26:58  keffer
 * ClassID->RWClassID
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   27 May 1992 18:28:36   KEFFER
 * Uses new V5.1 RWDEFINE_COLLECTABLE.
 * 
 *    Rev 1.2   13 Nov 1991 11:54:02   keffer
 * Added destructor for Init(className)
 * 
 *    Rev 1.1   28 Oct 1991 09:24:04   keffer
 * Include file path now <rw/xxx.h>
 * 
 */

#if defined(__DLL__)  /* Use for both Win16 and Win32 DLLs */

#define RWDEFINE_COLLECTABLE2(className, id)		\
RWCollectable* className::copy() const			\
{ return new className(*this); }			\
RWClassID className::isA() const			\
{ return id; }						\
RWCollectable* className::newSpecies() const		\
{ return new className; }

#else	/* not __DLL__ */

/*
 * If not compiling for a DLL, use the regular version:
 */
#define RWDEFINE_COLLECTABLE2(className, id)		\
RWDEFINE_COLLECTABLE(className, id)

#endif	/* __DLL__ */

#endif

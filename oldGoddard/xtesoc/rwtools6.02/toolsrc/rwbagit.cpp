/*
 * Definitions for RWBagIterator
 *
 * $Id: rwbagit.cpp,v 1.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwbagit.cpp,v $
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  21:11:31  dealys
 * Initial revision
 *
 *
 */

#include "rw/defs.h"	/* This include necessary to avoid nestling limits in /lib/cpp */
#include "rw/rwbag.h"
#include "rw/collint.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: rwbagit.cpp,v $ $Revision: 1.2 $ $Date: 1993/09/10 03:59:57 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif


/************************************************
 *						*
 *		RWBagIterator definitions	*
 *						*
 ************************************************/

RWBagIterator::RWBagIterator(const RWBag& bag) :
  contentsIterator(((RWBag&)bag).contents)
{
  count=0;
}

RWCollectable*
RWBagIterator::findNext(const RWCollectable* c)
{
  if(count==0 || !currentItem->isEqual(c) ){
    currentItem = contentsIterator.findNext(c);
    if(currentItem==rwnil) return rwnil;
    const RWCollectableInt* tally = (const RWCollectableInt*)contentsIterator.value();
    count = tally->value();
  }
  count--;
  return currentItem;
}

RWCollectable*
RWBagIterator::operator()()
{
  if(count==0){
    currentItem = contentsIterator();
    if(currentItem==rwnil) return rwnil;
    const RWCollectableInt* tally = (const RWCollectableInt*)contentsIterator.value();
    count = tally->value();
  }
  count--;
  return currentItem;
}

void
RWBagIterator::reset()
{
  contentsIterator.reset();
  count = 0;
}

RWCollectable*
RWBagIterator::key() const
{
  return currentItem;
}

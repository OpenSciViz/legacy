/*
 * RWHashTable special functions
 *
 * $Id: hashspec.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: hashspec.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   22 May 1992 17:02:22   KEFFER
 * Simplified algorithms
 * 
 *    Rev 1.0   22 May 1992 16:39:22   KEFFER
 * Initial revision.
 *
 */

#include "rw/hashtab.h"
#include "rw/rwset.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: hashspec.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

/****************************************************************
 *								*
 *			RWHashTable				*
 *			special functions			*
 *								*
 ****************************************************************/


RWBoolean
RWHashTable::operator<=(const RWHashTable& h) const
{
  RWSet set = asSet();
  RWSetIterator iterator(set);
  RWCollectable* p;
  while ((p=iterator())!=rwnil) {
    if (occurrencesOf(p) > h.occurrencesOf(p)) return FALSE;
  }
  return TRUE;
}

RWBoolean
RWHashTable::isEqual(const RWCollectable* c) const
{
  if( c->isA() != RWHashTable::isA() ) return FALSE;
  return RWHashTable::operator==(*(const RWHashTable*)c);
}

RWBoolean
RWHashTable::operator==(const RWHashTable& h) const
{
  return nitems_==h.nitems_ && operator<=(h);
}

/* Do two sets differ? */
RWBoolean
RWHashTable::operator!=(const RWHashTable& h) const
{
  return !operator==(h);
}

/*
 * Definitions for the RWWTokenizer class
 *
 * $Id: wtoken.cpp,v 1.11 1993/09/10 20:05:33 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: wtoken.cpp,v $
 * Revision 1.11  1993/09/10  20:05:33  keffer
 * Repositioned RW_RCSID macro.
 *
 * Revision 1.10  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.9  1993/08/09  03:55:22  myersn
 * tolerate nulls.
 *
 * Revision 1.8  1993/05/29  18:48:24  keffer
 * Renamed or added a dummy variable in case nothing gets compiled
 *
 * Revision 1.7  1993/05/24  19:12:22  keffer
 * Ported to xlC compiler.
 *
 * Revision 1.6  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 1.5  1993/03/22  23:12:26  keffer
 * Make indices unsigned instead of int.
 *
 * Revision 1.4  1993/02/16  19:11:39  myersn
 * install #ifndef RW_NO_WSTR protection.
 *
 * Revision 1.2  1993/02/11  20:50:52  myersn
 * change W" \t\n" to {(wchar_t)' ',(wchar_t'\t',...}; compilers still broken.
 *
 * Revision 1.1  1993/02/11  02:02:04  myersn
 * Initial revision
 *
 */

#include "rw/compiler.h"
#ifndef RW_NO_WSTR

#include "rw/wtoken.h"
#include "rwwchar.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: wtoken.cpp,v $ $Revision: 1.11 $ $Date: 1993/09/10 20:05:33 $");

RWWTokenizer::RWWTokenizer(const RWWString& s)
{
  theString = &s;
  place = rwnil;
}

RWWSubString
RWWTokenizer::operator()(const wchar_t* ws)
{
  RWPRECONDITION( ws!=0 );
  wchar_t const* eos = theString->data() + theString->length();

  // Check to see if this the first time through...
  if (place==rwnil)
    place = theString->data();	// Initialize 'place'

  size_t extent = 0;
  while (1) {
    if (place >= eos) return (*theString)(RW_NPOS,0);
    place += wcsspn(place, ws);
    extent = wcscspn(place, ws);
    if (extent) break;
    ++place; // skip null
  }
  size_t start = place - theString->data();
  place += extent;		// Advance the placeholder

  return (*theString)(start, extent);
}

RWWSubString
RWWTokenizer::operator()()
{
  // Sun CC 3.0 doesn't support W" \t\n".
  static const wchar_t white[4] =
    { (wchar_t)' ', (wchar_t)'\t', (wchar_t)'\n', 0 };
  return operator()(white);
}

#else
// This is to quiet fussy libraries if nothing gets compiled:
int rwDummy_wtoken_cpp;
#endif /* RW_NO_WSTR */

/*
 * Definitions for RWInteger
 *
 * $Id: rwintio.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwintio.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   28 Oct 1991 09:24:24   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:40   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/rwint.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: rwintio.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

void
RWInteger::restoreFrom(RWvistream& s)
{ s >> intBase; }

void
RWInteger::restoreFrom(RWFile& f)
{ f.Read(intBase); }

void
RWInteger::saveOn(RWvostream& s) const
{ s << intBase; }

void
RWInteger::saveOn(RWFile& f) const
{ f.Write(intBase); }

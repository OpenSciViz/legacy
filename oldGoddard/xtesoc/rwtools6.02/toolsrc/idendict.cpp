
/*
 * Definitions for RWIdentityDictionary
 *
 * $Id: idendict.cpp,v 2.4 1993/11/09 08:37:30 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class is very similar similar to a regular hashed dictionary.
 * The only difference is that an item is found by requiring that it
 * be "identical to" the key, rather than "equal to".  This is done by
 * using RWCollectableIDAssociations in place of RWCollectableAssociations.
 *
 * Accomplishing this can be done very simply by just overriding the
 * virtual function "newAssociation()".
 *
 ***************************************************************************
 *
 * $Log: idendict.cpp,v $
 * Revision 2.4  1993/11/09  08:37:30  jims
 * Port to ObjectStore
 *
 * Revision 2.3  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.2  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.1  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.6   29 May 1992 09:46:50   KEFFER
 * Introduced CPP_ANSI_RECURSION macro
 * 
 *    Rev 1.5   27 May 1992 18:09:04   KEFFER
 * RWMEMCK compatible.
 * 
 *    Rev 1.4   25 May 1992 15:37:12   KEFFER
 * Optimized, reducing size, by using virtual functions.
 * 
 *    Rev 1.3   29 Apr 1992 14:50:56   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 *    Rev 1.1   28 Oct 1991 09:24:14   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:28   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/defs.h"	/* necessary to avoid nesting limits in /lib/cpp */
#include "rw/idendict.h"
#include "rw/collass.h"
#include "defcol.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: idendict.cpp,v $ $Revision: 2.4 $ $Date: 1993/11/09 08:37:30 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

RWDEFINE_COLLECTABLE2(RWIdentityDictionary, __RWIDENTITYDICTIONARY)

RWIdentityDictionary::RWIdentityDictionary(size_t N) :
  RWHashDictionary(N)
{
}

/*
 * Protected virtual function to find the association for a given
 * key, or return nil if there is no such association.
 */
RWCollectableAssociation*
RWIdentityDictionary::findAssociation(const RWCollectable* key) const
{
  RWCollectableIDAssociation assoc((RWCollectable*)key,rwnil);
  return (RWCollectableAssociation*)RWSet::find(&assoc);
}


RWCollectableAssociation*
RWIdentityDictionary::newAssociation(RWCollectable* k, RWCollectable* v) const
{
  return new RWCollectableIDAssociation(k,v);
}


/* Protected virtual function to remove and return the association
 * for a given key, or return nil if there is no such association.
 */
RWCollectableAssociation*
RWIdentityDictionary::removeAssociation(const RWCollectable* key)
{
  RWCollectableIDAssociation assoc((RWCollectable*)key,rwnil);
  return (RWCollectableAssociation*)RWSet::remove(&assoc);
}


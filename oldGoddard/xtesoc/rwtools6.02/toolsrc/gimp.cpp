
/*
 * Implementations of generic classes, used by Tools.h++
 *
 * $Id: gimp.cpp,v 2.3 1993/11/09 08:06:08 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: gimp.cpp,v $
 * Revision 2.3  1993/11/09  08:06:08  jims
 * Port to ObjectStore
 *
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/04/01  00:52:59  myersn
 * change GVector mention to RWGVector.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   28 Oct 1991 09:24:12   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:22   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

/*
 * This file is used to implement (i.e., provide a definition for) 
 * generic.h type classes that are used internally by Tools.h++.
 * Right now only one class falls into that category: RWGVector(RWCollectableP).
 */

#include "rw/tooldefs.h"
#include "rw/gvector.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: gimp.cpp,v $ $Revision: 2.3 $ $Date: 1993/11/09 08:06:08 $");

/*
 * Declare then implement a vector of pointers to RWCollectables:
 */

declare(RWGVector,RWCollectableP)
implement(RWGVector,RWCollectableP)


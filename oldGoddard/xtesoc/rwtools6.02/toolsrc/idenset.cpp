/*
 * Definitions for RWIdentitySet (an Identity Set).
 *
 * $Id: idenset.cpp,v 2.5 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class is very similar similar to a regular RWSet (i.e., a Set).
 * The only difference is that an item is found by requiring that it
 * be "identical to" the key, rather than "equal to".  This is done
 * by overriding a few functions, substituting reference semantics
 * for value semantics.
 *
 ***************************************************************************
 *
 * $Log: idenset.cpp,v $
 * Revision 2.5  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.4  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.3  1993/03/20  23:02:24  keffer
 * Eliminated int to unsigned type conversion
 *
 * Revision 2.1  1992/11/15  00:31:14  keffer
 * Added explicit "this->" dereference to suppress unfounded
 * cfront 3.0 warnings.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   25 May 1992 15:37:26   KEFFER
 * Optimized, reducing size, by using virtual functions.
 * 
 *    Rev 1.2   29 Apr 1992 14:50:56   KEFFER
 * Hashing now uses chaining to resolve collisions
 * 
 *    Rev 1.1   28 Oct 1991 09:24:14   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:28   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/idenset.h"
#include "rw/slistcol.h"
#include "defcol.h"
#define CHAIN         RWSlistCollectables

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: idenset.cpp,v $ $Revision: 2.5 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWIdentitySet, __RWIDENTITYSET)

RWIdentitySet::RWIdentitySet(size_t N) :
  RWSet(N)
{
}

/*
 * Find the object that is *identical to* "a".
 */
RWCollectable*
RWIdentitySet::find(const RWCollectable* a) const
{
  CHAIN* p = this->table_(hashIndexReference(a));
  return p ? p->findReference(a) : rwnil;
}

/*
 * Insert, using identity semantics, rather than value semantics.
 */
RWCollectable*
RWIdentitySet::insert(RWCollectable* a)
{
  RWCollectable* ret;
  size_t idx = hashIndexReference(a);
  if (table_(idx)==rwnil || (ret=table_(idx)->findReference(a))==rwnil) {
    insertIndex(idx, a);
    ret = a;
  }
  return ret;
}

/*
 * Remove the object that is identical to "a".  If nothing is found,
 * return nil;
 */
RWCollectable*
RWIdentitySet::remove(const RWCollectable* a)
{
  CHAIN* p = table_(hashIndexReference(a));
  RWCollectable* c = p ? p->removeReference(a) : rwnil;
  if (c) --nitems_;	// Decrement if found
  RWPOSTCONDITION(c==rwnil || c==a);
  return c;
}

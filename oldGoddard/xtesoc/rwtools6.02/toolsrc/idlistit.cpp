/*
 * RWIsvDlistIterator 
 *
 * $Id: idlistit.cpp,v 1.3 1993/11/13 23:05:50 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991, 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: idlistit.cpp,v $
 * Revision 1.3  1993/11/13  23:05:50  keffer
 * Fixed bug in RWIsvDlistIterator::operator-=(size_t n);
 *
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  20:30:14  dealys
 * Initial revision
 *
 * 
 */

/*
 * Member data head_.next_ points to the actual start of
 * the list, or tail_ if there are no members of the list.
 * Member data head_.prev_ always points to itself.
 *
 * Member data tail_.prev_ points to the last member of
 * the list, or to head_ if there are no members of the list.
 * Member data tail_.next_ always points to itself.
 */

#include "rw/idlist.h"
#include "rw/toolerr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: idlistit.cpp,v $ $Revision: 1.3 $ $Date: 1993/11/13 23:05:50 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif



/********************************************************
*							*
*		RWIsvDlistIterator definitions		*
*							*
********************************************************/

/*
 * The variable "dhere_" points to the "current position" of
 * the iterator (sometimes called "point" or the "cursor 
 * position").  When the iterator is constructed, dhere_
 * points to the "head_" link of the list.  This link
 * points to the actual first link of the list.
 * Each call to operator++() advances the cursor one position.
 *
 */


/*
 * Advance iterator n links then return.
 */

RWIsvDlink*
RWIsvDlistIterator::operator+=(size_t n)
{
  while (n--)
    ++(*this);
  return dhere_ == &dlist_->tail_ ? rwnil : dhere_;
}

/*
 * Backup iterator n links then return.
 */

RWIsvDlink*
RWIsvDlistIterator::operator-=(size_t n)
{
  while (n--)
    --(*this);
  return dhere_ == &dlist_->head_ ? rwnil : dhere_;
}

RWBoolean
RWIsvDlistIterator::atFirst() const
{
  return isActive() && dhere_ == dlist_->head_.next_;
}

RWBoolean
RWIsvDlistIterator::atLast() const
{
  return isActive() && dhere_ == dlist_->tail_.prev_;
}

void
RWIsvDlistIterator::toFirst()
{
  dhere_ = dlist_->head_.next_;
}

void
RWIsvDlistIterator::toLast()
{
  dhere_ = dlist_->isEmpty() ? &dlist_->tail_ : dlist_->tail_.prev_;
}


/*
 * Insert the link pointed to by a after the current link.  There must
 * be a current link (that is, the iterator must still be active).
 */
void
RWIsvDlistIterator::insertAfterPoint(RWIsvDlink* a)
{
  RWPRECONDITION2(a, "RWIsvDlistIterator::insertAfterPoint(RWIsvDlink*): nil pointer");
  RWPRECONDITION2(a->next_==rwnil, "RWIsvDlistIterator::insertAfterPoint(RWIsvDlink*): Attempt to add intrusive link to more than one list");
  RWPRECONDITION2(dhere_ != &dlist_->tail_, "RWIsvDlistIterator::insertAfterPoint(RWIsvDlink*): Attempt to add link while iterator is inactive");

  dlist_->insertAfterLink(dhere_, a);
}	       

/*
 * Remove the link pointed to by the iterator.  The iterator must
 * be active.  The iterator is left pointing to the *previous* item in
 * the list.
 */

RWIsvDlink*
RWIsvDlistIterator::remove()
{
  // Check for inactive iterator:
  if (!isActive()) return rwnil;

  // Backup the iterator to the previous link:
  dhere_ = dhere_->prev_;

  // Now remove the link at the old position:
  return dlist_->removeReference(dhere_->next_);
}

// Reset the iterator to work with a new collection
void
RWIsvDlistIterator::reset(RWIsvDlist& s)
{
  dlist_ = &s;
  reset();
}

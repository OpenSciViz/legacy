#ifndef __RWSTORETABLE_H__
#define __RWSTORETABLE_H__

/*
 * Definitions for internal classes used to store collectables
 *
 * $Id: rwstore.h,v 2.0 1992/10/23 03:36:08 keffer Rel $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwstore.h,v $
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   13 Nov 1991 11:53:42   keffer
 * Typedefs RWReadTable to an ordered collection.
 * 
 *    Rev 1.1   28 Oct 1991 09:24:26   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 09:23:06   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */


/**
 *******************  RWStoreTable declarations ***************************
 **/

#include "rw/rwset.h"

class RWExport RWStoreTable : public RWSet {
public:
  RWStoreTable();
  ~RWStoreTable();
  RWBoolean		add(const RWCollectable*, int&);
};

class RWExport RWStoreEntry : public RWCollectable {
friend class RWStoreTable;
  const RWCollectable*	item;
  int			objectNumber;
public:
  RWStoreEntry(const RWCollectable* c, int n){item=c; objectNumber=n;}
  unsigned		hash() const;
  RWBoolean		isEqual(const RWCollectable* c) const;
};

/**
 *******************  RWReadTable declarations ***************************
 **/

// For the read table, just use an ordered collection and typedef it:
#include "rw/ordcltn.h"
typedef RWOrdered RWReadTable;

#endif	/* __RWSTORETABLE_H__ */

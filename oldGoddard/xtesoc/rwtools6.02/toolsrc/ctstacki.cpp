/*
 * RWCollectableStack I/O
 *
 * $Id: ctstacki.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctstacki.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   04 Aug 1992 19:23:12   KEFFER
 * restoreGuts now calls RWSlistCollectables::append instead of just append.
 * 
 *    Rev 1.2   25 May 1992 15:35:04   KEFFER
 * saveOn -> recursiveSaveOn; restoreFrom -> recursiveRestoreFrom.
 * 
 *    Rev 1.1   28 Oct 1991 09:24:00   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:08   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/stackcol.h"
#include "rw/vstream.h"
#include "rw/rwfile.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctstacki.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

/*
 * restoreGuts() must be redefined for a stack.
 * The saveGuts() function stored things so that
 * the first item encountered is the *top* of the
 * stack.  If we were to simply push items back on,
 * the stack would be upside down.
 */

void
RWSlistCollectablesStack::restoreGuts(RWvistream& s)
{
  size_t nobjs;
  s >> nobjs;
  if( !s.good() ) return;

  while(nobjs--) {
    RWCollectable* c = recursiveRestoreFrom(s);
    if( !s.good() ) return;
    RWSlistCollectables::append(c);
  }
}

void
RWSlistCollectablesStack::restoreGuts(RWFile& file)
{
  size_t nobjs;

  if( !file.Read(nobjs) ) return;

  while(nobjs--) {
    RWCollectable* c = recursiveRestoreFrom(file);
    if(file.Error()) return;
    RWSlistCollectables::append(c);
  }
}

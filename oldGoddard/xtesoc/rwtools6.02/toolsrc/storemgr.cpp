/*
 * RWStoreManager --- returns current store, based on current thread.
 *
 * $Id: storemgr.cpp,v 1.4 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: storemgr.cpp,v $
 * Revision 1.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.3  1993/09/03  18:37:49  jims
 * Cast void* to actual class pointer for delete in function deleteValue()
 *
 * Revision 1.2  1993/08/06  03:52:05  randall
 * use rwfar instead of FAR
 *
 * Revision 1.1  1993/04/12  12:11:57  jims
 * Initial revision
 *
 * Revision 2.1  1993/02/05  07:28:23  jims
 * Modified to use new version of Instance Manager
 *
 * Revision 2.0  1992/10/23  03:36:49  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.0   12 Nov 1991 15:39:40   keffer
 * Initial revision.
 */

#include "storemgr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: storemgr.cpp,v $ $Revision: 1.4 $ $Date: 1993/09/10 03:59:57 $");

RWStoreManager rwStoreManager;	// The one-of-a-kind RWStoreTable manager

RWStoreTable*
RWStoreManager::currentStoreTable()
{
  RWStoreTable* table = (RWStoreTable*)currentValue();
  return table;
}

RWStoreTable*
RWStoreManager::newStoreTable()
{
  RWStoreTable* table = (RWStoreTable*)addValue();
  return table;
}

// Redefined from class RWInstanceManager:
void rwfar*
RWStoreManager::newValue()
{
  RWStoreTable* table = new RWStoreTable;
  return (void rwfar*) table;
}

// Redefined from class RWInstanceManager:
void
RWStoreManager::deleteValue(void rwfar* table)
{
  delete (RWStoreTable*)table;
}

/*
 * RWCollectableTime I/O
 *
 * $Id: cttimeio.cpp,v 2.1 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cttimeio.cpp,v $
 * Revision 2.1  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   28 Oct 1991 09:24:02   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:14   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colltime.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: cttimeio.cpp,v $ $Revision: 2.1 $ $Date: 1993/09/10 03:59:57 $");

void
RWCollectableTime::restoreGuts(RWvistream& s)
{
  RWTime::restoreFrom(s);
}

void
RWCollectableTime::restoreGuts(RWFile& f)
{
  RWTime::restoreFrom(f);
}

void
RWCollectableTime::saveGuts(RWvostream& s) const
{
  RWTime::saveOn(s);
}

void
RWCollectableTime::saveGuts(RWFile& f) const
{
  RWTime::saveOn(f);
}

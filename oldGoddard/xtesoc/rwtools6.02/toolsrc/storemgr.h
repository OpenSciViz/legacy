#ifndef __RWSTOREMGR_H__
#define __RWSTOREMGR_H__

/*
 * RWStoreManager: returns a unique RWStoreTable for each thread.
 *
 * $Id: storemgr.h,v 1.2 1993/08/05 01:07:10 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: storemgr.h,v $
 * Revision 1.2  1993/08/05  01:07:10  jims
 * Use rwfar instead of FAR
 *
 * Revision 1.1  1993/04/12  12:11:57  jims
 * Initial revision
 *
 * Revision 2.1  1993/02/05  07:28:23  jims
 * Modified to use new version of Instance Manager
 *
 * Revision 2.0  1992/10/23  03:36:49  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   12 Nov 1991 15:39:38   keffer
 * 
 */

#include "rwstore.h"
#include "rw/instmgr.h"

class RWStoreManager : public RWInstanceManager {
public:
  RWStoreTable*		currentStoreTable();
  RWStoreTable*		newStoreTable();

  // Inherited from RWInstanceManager:
  virtual void rwfar*	newValue();
  virtual void          deleteValue(void rwfar*);
};


extern RWStoreManager rwStoreManager;

#endif	/* __RWSTOREMGR_H__ */

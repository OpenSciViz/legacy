/*
 * Definitions for the abstract base class RWCollection
 *
 * $Id: ctclass.cpp,v 2.7 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * It would be nice if this class could carry a default destructor that called
 * clear() of the deriving class, because nearly every collection class does so.
 * But, the language says that virtual functions in a destructor resolve to
 * the class itself, or farther down the DAG.  The net result is that
 * RWCollection::clear() would get called every time.  See E&S, p. 294.  
 *
 * The code for clearAndDestroy() is complicated by collections that allow
 * duplicates.  It must check to see if an object has already been deleted.
 * It does this by maintaining an IdentitySet of already deleted objects.
 *
 ***************************************************************************
 *
 * $Log: ctclass.cpp,v $
 * Revision 2.7  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.6  1993/09/09  02:39:32  keffer
 * Added member function copyContentsTo().
 *
 * Revision 2.5  1993/07/03  23:55:58  keffer
 * Simplified and improved the clearAndDestroy() algorithm.
 *
 * Revision 2.4  1993/05/18  00:47:20  keffer
 * Replaced RDEBUG check with an assertion.
 *
 * Revision 2.3  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 2.2  1993/03/17  21:21:15  keffer
 * Return type of binaryStoreSize is now RWspace
 *
 * Revision 2.1  1992/11/05  03:47:45  keffer
 * Removed ^Z from the end of file.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   25 May 1992 15:30:52   KEFFER
 * CAPACITY and RESIZE are now non-const, allowing them to be changed.
 * shallowStoreSize -> recursiveStoreSize;
 * RWIdentitySet deadObjects optimized.
 * 
 *    Rev 1.4   04 Mar 1992 09:17:20   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.3   05 Nov 1991 14:05:46   keffer
 * Removed RWXXXXErr macros --- now done by error package
 * 
 *    Rev 1.2   28 Oct 1991 09:23:54   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.1   22 Aug 1991 10:56:36   keffer
 * RWCollectionTestStruct is no longer declared static
 * 
 *    Rev 1.0   28 Jul 1991 08:38:56   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/idenset.h"
#include <assert.h>

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctclass.cpp,v $ $Revision: 2.7 $ $Date: 1993/09/10 03:59:57 $");

// Default initial collection size (where applicable):
size_t RWCollection::DEFAULT_CAPACITY	= RWDEFAULT_CAPACITY;
size_t RWCollection::DEFAULT_RESIZE	= RWDEFAULT_RESIZE;

void
rwDestroy(RWCollectable* s, void*)
{
  delete s;
}

void
RWCollection::clearAndDestroy()
{
  RWIdentitySet nukeSet(entries()/4); // Lambda of 4
  nukeSet += (*this);
  nukeSet.clearAndDestroy();	// All objects are now unique.  Nuke 'em

  clear();
}

RWBoolean		
RWCollection::contains(const RWCollectable* c) const
{
  return find(c) != rwnil;
}

void
RWCollection::removeAndDestroy(const RWCollectable* a)
{
  RWCollectable* c = remove(a);
  delete c;
}

static void
addBinaryStoreSize(RWCollectable* c, void* n)
{
  *(RWspace*)n += c->recursiveStoreSize();
}

RWspace
RWCollection::binaryStoreSize() const
{
  RWspace tot = sizeof(size_t);
  // Cast done to suppress unfounded "const" warnings:
  ((RWCollection*)this)->apply(addBinaryStoreSize, &tot);
  return tot;
}

static void
addItemTo(RWCollectable* item, void* x)
{
  ((RWCollection*)x)->insert(item);
}

static void
copyItemTo(RWCollectable* item, void* x)
{
  ((RWCollection*)x)->insert(item->copy());
}

static void
removeItemFrom(RWCollectable* item, void* x)
{
  ((RWCollection*)x)->remove(item);
}

void
RWCollection::operator+=(const RWCollection& col)
{
  ((RWCollection&)col).apply(::addItemTo, this);
}

void
RWCollection::operator-=(const RWCollection& col)
{
    ((RWCollection&)col).apply(::removeItemFrom, this);
}

void
RWCollection::addContentsTo(RWCollection* collection) const
{
  RWPRECONDITION( collection!=rwnil );
  // Cast necessary to suppress unfounded cfront warnings:
  ((RWCollection*)this)->apply(::addItemTo, collection);
}

void
RWCollection::copyContentsTo(RWCollection* collection) const
{
  RWPRECONDITION( collection!=rwnil );
  // Cast necessary to suppress unfounded cfront warnings:
  ((RWCollection*)this)->apply(::copyItemTo, collection);
}

struct RWCollectionTestStruct {
  RWtestCollectable	functionToApply;
  RWCollection*		collection;
  void*			clientData;
};

static void
addIfTrue(RWCollectable* c, void* capsule)
{
  register struct RWCollectionTestStruct* temp = (struct RWCollectionTestStruct*)capsule;
  if(temp->functionToApply(c, temp->clientData)) temp->collection->insert(c);
}
  
RWCollection*
RWCollection::select(RWtestCollectable tstFun, void* x) const
{
  RWPRECONDITION( tstFun!=0 );
  RWCollectionTestStruct capsule;

  capsule.clientData = x;
  capsule.functionToApply = tstFun;
  capsule.collection = (RWCollection*)newSpecies();

  ((RWCollection*)this)->apply(addIfTrue, &capsule);

  return capsule.collection;
}


/*
 * Definitions for B-Tree Dictionaries
 *
 * $Id: btrdict.cpp,v 2.8 1993/11/09 05:27:54 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: btrdict.cpp,v $
 * Revision 2.8  1993/11/09  05:27:54  jims
 * Port to ObjectStore
 *
 * Revision 2.7  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.6  1993/09/09  02:39:12  keffer
 * Copy constructor and assignment operator now based on copyContentsTo().
 *
 * Revision 2.5  1993/07/03  23:55:58  keffer
 * Simplified and improved the clearAndDestroy() algorithm.
 *
 * Revision 2.4  1993/05/18  16:05:59  dealys
 * merged with btrdictd.cpp
 *
 * Revision 2.3  1993/03/01  16:22:07  keffer
 * Added text messages to PRECONDITION clauses.
 *
 * Revision 2.2  1992/11/19  05:06:42  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 *    Rev 1.5   27 May 1992 18:08:56   KEFFER
 * RWMEMCK compatible.
 * 
 *    Rev 1.4   04 Mar 1992 09:17:16   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.3   28 Oct 1991 09:23:50   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.2   08 Sep 1991 12:58:42   keffer
 * clearAndDestroy() now checks for already deleted keys and values.
 * 
 *    Rev 1.1   22 Aug 1991 10:55:24   keffer
 * RWBTreeDictStruct is no longer declared static
 * 
 */

#include "rw/btrdict.h"
#include "rw/collass.h"
#include "rw/idenset.h"
#include "defcol.h"
                     
RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: btrdict.cpp,v $ $Revision: 2.8 $ $Date: 1993/11/09 05:27:54 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif

RWDEFINE_COLLECTABLE2(RWBTreeDictionary, __RWBTREEDICTIONARY)

RWBTreeDictionary::RWBTreeDictionary() : RWBTree() { }                        

/*
 * Dictionary copy constructor.  Start with an empty RWBTree.
 * Then add copies of all items in the base class of self to it.
 * This will result in the associations being copied, but not the
 * collected items.
 */

RWBTreeDictionary::RWBTreeDictionary(const RWBTreeDictionary& btrdict)
 : RWBTree()
{
  RWPRECONDITION(entries()==0);
  btrdict.RWBTree::copyContentsTo(this);
  RWPOSTCONDITION(entries()==btrdict.entries());
}

/*
 * Similar strategy for the assignment operator.
 */
void
RWBTreeDictionary::operator=(const RWBTreeDictionary& btrdict)
{
  clear();
  btrdict.RWBTree::copyContentsTo(this);
  RWPOSTCONDITION(entries()==btrdict.entries());
}

RWBTreeDictionary::~RWBTreeDictionary()
{
  RWBTreeDictionary::clear();
}

/********************************************************
*							*
*	      Methods for Class RWBTreeDictionary	*
*							*
**********************************************************/

/*
 * The following is designed to allow examination of key and value
 * pairs without using a RWBTree Iterator (which we don't have).
 * Basically, we repackage the call to applyToKeyAndValue() as a call 
 * to the base class's apply(), using a special struct to hold the
 * necessary values.   This approach is recursive.
 */

struct RWBTreeDictStruct {
  RWapplyKeyAndValue	functionToApply;
  void*			clientData;
};

static void
applyAssociation(RWCollectable* c, void* capsule)
{
  struct RWBTreeDictStruct* temp	= (struct RWBTreeDictStruct*)capsule;
  RWCollectableAssociation* ca	= (RWCollectableAssociation*)c;
  (temp->functionToApply)(ca->key(), ca->value(), temp->clientData);
}

// Member function to examine keys and values
void	 
RWBTreeDictionary::applyToKeyAndValue(RWapplyKeyAndValue ap, void* x)
{
  RWPRECONDITION2( ap!=rwnil,"RWBTreeDictionary::applyToKeyAndValue(RWapplyKeyAndValue ap, void* x): nil function pointer" );
  RWBTreeDictStruct capsule;
  capsule.clientData		= x;
  capsule.functionToApply	= ap;
  RWBTree::apply(applyAssociation, &capsule);
}

void
RWBTreeDictionary::clear()
{
  RWBTree::apply(rwDestroy, 0);
  RWBTree::clear();
}

static void
addKeyAndValue(RWCollectable* key, RWCollectable* val, void* x)
{
  ((RWCollection*)x)->insert(key);
  ((RWCollection*)x)->insert(val);
}

/*
 * Deletes all keys AND values.
 */
void
RWBTreeDictionary::clearAndDestroy()
{
  RWIdentitySet nukeSet(entries()/4); // Lambda of approximately 8
  applyToKeyAndValue(addKeyAndValue, &nukeSet);
  nukeSet.clearAndDestroy();	// Nuke 'em
  clear();			// Will delete the associations.
}

/*
 * Returns matching key.
 */
RWCollectable*  
RWBTreeDictionary::find(const RWCollectable* key) const
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::find(const RWCollectable* key) const: nil pointer" );
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::find(key);
  return a ? a->key() : rwnil;
}

/*
 * Returns matching key, puts value in second argument.
 */
RWCollectable*	
RWBTreeDictionary::findKeyAndValue(const RWCollectable* key, RWCollectable*& value) const
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::findKeyAndValue(const RWCollectable*, RWCollectable*& ) const: nil pointer" );
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::find(key);
  RWCollectable* ret;
  if (a) {
    ret = a->key();
    value = a->value();
  }
  else{
    ret = rwnil;
    value = rwnil;
  }
  return ret;
}

/*
 * Given a key, returns the associated value.
 */

RWCollectable*
RWBTreeDictionary::findValue(const RWCollectable* key) const
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::findValue(const RWCollectable* key) const: nil pointer" );
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::find(key);
  return a ? a->value() : rwnil;
}

/*
 * Given a key, replaces the associated value.  Returns the old value.
 * Returns nil if the value cannot be found.
 */
RWCollectable*
RWBTreeDictionary::findValue(const RWCollectable* key, RWCollectable* newValue) const
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::findValue(const RWCollectable* , RWCollectable* ) const: nil pointer" );
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::find(key);
  return a ? a->value(newValue) : rwnil;
}

/*
 * Returns the key if successful.  If the key is already
 * in the Dictionary, then returns rwnil.
 */
RWCollectable*
RWBTreeDictionary::insertKeyAndValue(RWCollectable* key, RWCollectable* val)
{

  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::insertKeyAndValue(RWCollectable* key, RWCollectable* val): nil pointer" );
  RWCollectableAssociation* a = new RWCollectableAssociation(key, val);
  RWCollectableAssociation* b = (RWCollectableAssociation*)RWBTree::insert(a);
  if ( a != b ) {
    delete a;			// Key already in the tree.
    return rwnil;
  }
  else
    return key;
}

/*
 * Removes the key and value from the dictionary and returns the key.
 */
RWCollectable*  
RWBTreeDictionary::remove(const RWCollectable* key)
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::remove(const RWCollectable* key): nil pointer" );
  RWCollectable* ret;
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::remove(key);
  if(a){
    ret = a->key();
    delete a;
  }
  else
    ret = rwnil;
  return ret;
}

/*
 * Removes the key and value from a dictionary AND deletes both of them.
 */
void
RWBTreeDictionary::removeAndDestroy(const RWCollectable* key)
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::removeAndDestroy(const RWCollectable* key): nil pointer" );
  RWCollectableAssociation* c = (RWCollectableAssociation*)RWBTree::remove(key);
  if(c){
#ifdef __ATT__
    RWCollectable* deadMeat = c->value();	// Temporary necessary for cfront.
    delete deadMeat;
    deadMeat = c->key();
    delete deadMeat;
#else
    delete c->value();
    delete c->key();
#endif
    delete c;
  }
}

/*
 * Removes the key and value from the dictionary, returns the key, puts
 * the value in the second argument.
 */
RWCollectable*	 
RWBTreeDictionary::removeKeyAndValue(const RWCollectable* key, RWCollectable*& value)
{
  RWPRECONDITION2( key!=rwnil,"RWBTreeDictionary::removeKeyAndValue(const RWCollectable*, RWCollectable*&): nil pointer" );
  RWCollectableAssociation* a = (RWCollectableAssociation*)RWBTree::remove(key);
  RWCollectable* ret;
  if (a) {
    ret = a->key();
    value = a->value();
    delete a;
  }
  else{
    ret = rwnil;
    value = rwnil;
  }
  return ret;
}

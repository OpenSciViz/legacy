/*
 * Definitions for binary tree iterator class.
 *
 * $Id: bintrit.cpp,v 1.3 1993/11/09 04:25:14 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc. 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 *
 * $Log: bintrit.cpp,v $
 * Revision 1.3  1993/11/09  04:25:14  jims
 * Moved definition of class RWTreeNode back to header file
 *
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  18:37:02  dealys
 * Initial revision
 *
 *
 */

#include "rw/bintree.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: bintrit.cpp,v $ $Revision: 1.3 $ $Date: 1993/11/09 04:25:14 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif
   
/****************************************************************
 *								*
 *		RWBinaryTreeIterator class definitions		*
 *								*
 ****************************************************************/

/*
 * Implementing an iterator for a binary tree is not trivial.
 * The main complication is that you must maintain a stack of
 * visited parent nodes to remember where you are in the tree
 */

RWBinaryTreeIterator::RWBinaryTreeIterator(const RWBinaryTree& b)
{
  tree = &b;
  here = rwnil;
}

RWCollectable*
RWBinaryTreeIterator::findNext(const RWCollectable* a)
{
  RWPRECONDITION2(a!=rwnil,"RWBinaryTreeIterator::findNext(const RWCollectable*): nil pointer");

  int compare;
  
  if ( tree->isEmpty() )return rwnil;
  
  /* Test for first time through: */
  if ( here == rwnil ) {
restart: ;
    stack.clear();		// Make sure stack is cleared.
    here = tree->root;		// Initialize "here" to root.
    compare = -here->e->compareTo(a);
    if ( compare == 0 )return here->e; // Is root the target?
  } else {			    
    /* Not first time through.  Do a test. */
    compare = -here->e->compareTo(a);
    /* Three possiblities...
     *   the given value is less than the current value; it won't
     *   be found.  Just return. */
    if ( compare < 0 ) {
      reset();
      return rwnil;
    }

    /*   the give value is greater than the current value, start all over.*/
    if ( compare > 0 ) goto restart;

    /*   else, the given value is the same as the current value;
     *   If you think about it, it must be in the right-hand branch.
     *   Just carry on.  If it exists, we'll find it. */

  }
  
  
  /*
   * The objective is to advance to the next node that compares to zero.
   */
  
  do {
    stack.push((RWTreeNode*)here);
    // Which way do we branch?
    here = compare < 0 ? here->left : here->right;
    // Is there a "next object" at all?
    if ( here==rwnil ) {
      reset();
      return rwnil;
    }
    compare = -here->e->compareTo(a);
  } while (compare);
  
  return here->e;
}

RWCollectable*
RWBinaryTreeIterator::key() const
{
  return here ? here->e : rwnil;
}

/*
 * Advance and return "next" object.
 */
RWCollectable*
RWBinaryTreeIterator::operator()()
{
  if ( tree->isEmpty() )return rwnil;

  /* Test for first time through: */
  if ( here==rwnil ) {
    stack.clear();		// Clear stack.
    here = tree->root;		// Set here to root node.
    /* Descend the left branch all the way to the bottom;
       this will be the "first" member of the tree. */
    descendLeft();
  }

/*
 * The pointer "here" points to the "current node".  The overall
 * objective is to find the "next largest node".  Because the binary tree
 * is sorted, this next node can be in only one of two places.  (1) If
 * the current node has a right-hand branch, then it must be in the
 * "smallest valued" node in that right-hand branch.  Descend the branch all the
 * way to the bottom, taking all left-hand branches.  (2) If the current
 * node does not have a right hand branch, then the next node must be a
 * parent.  But, it can only be a parent such that we are in a left-hand
 * branch of that parent (otherwise the current node would have a larger
 * value).  Hence, unwind the stack, looking for a parent such that we are not
 * in its right-hand branch.  This parent will be the "next value". 
 */

  // Is there a right-hand branch for the current node?
  else if ( here->right ) {
    // Yes.  Push its root onto the stack and descend it.
    stack.push((RWTreeNode*)here);
    here = here->right;
    descendLeft();
  }
  else {
    /* No right-hand branch for the current node.
     * Unwind the stack, looking for a parent where we are the
     * left-hand daughter. */
    RWTreeNode* daughter;
    do {
      daughter = (RWTreeNode*) here;	// Save what will be the daughter.
      here = stack.pop();		// Get the parent.
      if ( here == rwnil ) return rwnil;	// stack empty?
    } while ( here->right == daughter );// Continue while we are right-hand daughter
  }
  return here->e;
}

void
RWBinaryTreeIterator::reset()
{
  stack.clear();
  here = rwnil;
}

/**************** RWBinaryTreeIterator Utilities ******************/

/*
 * Descend the left branch as far as we can go.
 * Leaves "here" pointing to the leaf and all parents
 * on the stack.
 */

void
RWBinaryTreeIterator::descendLeft()
{
  while ( here->left ) {
    stack.push((RWTreeNode*)here);
    here = here->left;
  }
}


#ifdef RDEBUG

#include "rw/rstream.h"
#include "rw/collint.h"

/**************** RWBinaryTree Debugging Utilities ******************/


/*
 * Included only if debugging.  Note, assumes that
 * all members are RWCollectableInt's.  I.e., this is NOT a general
 * testing utility.
 */

ostream&
operator<<(ostream& s, const RWBinaryTree& bt)
{
  unsigned level = 0;
  bt.printChildren(bt.root, s, level, '-');
  return s;
}

void
RWBinaryTree::printChildren(const RWTreeNode* t, ostream& s, unsigned& level, char parent) const
{
  if(t){
    level++;
    printChildren(t->right, s, level, '/');
    if(t->e->isA() == __RWCOLLECTABLEINT){	// This supplies some small measure of protection
      for(int i = 0; i<level; i++)  s << "  ";
      s << parent << " " << ((RWCollectableInt*)(t->e))->value() << endl;
    }
    printChildren(t->left, s, level, '\\');
    level--;
  }
}
#endif


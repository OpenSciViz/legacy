/*
 * Definitions for RWCString RWFile and vstream Input/Output functions
 *
 * $Id: cstrio2.cpp,v 2.2 1993/11/18 22:11:38 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989 - 1993.
 *
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * These functions are included in their own object file to remove
 * dependency between RWCStrings and RWFile/RWvstreams
 *
 ***************************************************************************
 *
 * $Log: cstrio2.cpp,v $
 * Revision 2.2  1993/11/18  22:11:38  jims
 * SaveOn and RestoreFrom now checks vstream version number
 *
 * Revision 2.1  1993/11/17  04:11:57  keffer
 * Split RWCString I/O functions
 *
 * Revision 2.0  1993/10/28  23:06:55  alv
 * moved RWFile and vstream I/O from cstrngio to here
 *
 */

#include <rw/cstring.h>
#include <rw/rwfile.h>
#include <rw/vstream.h>

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: cstrio2.cpp,v $ $Revision: 2.2 $ $Date: 1993/11/18 22:11:38 $");

void
RWCString::restoreFrom(RWvistream& strm)
{
  size_t len;

  strm >> len;			// Get the string length & check for errors.
  if (strm.good())
  {
    clobber(len);

    RWASSERT(capacity() >= len);
    if (strm.version() != 0) {
      strm.getString(pref_->data(), len+1);
      pref_->nchars_ = len;
    } 
    else {
      strm.get(pref_->data(), len);
      (*pref_)[pref_->nchars_ = len] = '\0';
    }
  }
}

void
RWCString::restoreFrom(RWFile& file)
{
  size_t len;

  if(file.Read(len))		// Get the string length & check for errors.
  {
    clobber(len);

    RWASSERT(capacity() >= len);
    file.Read(pref_->data(), len); // Read the string.
    (*pref_)[pref_->nchars_ = len] = '\0';
  }
}

void
RWCString::saveOn(RWvostream& strm) const
{
  strm << length();
  if (strm.version() != 0)
    strm.putString(data(), length());  // output as characters
  else
    strm.put(data(), length());        // version 0: output as numbers
}

void
RWCString::saveOn(RWFile& file) const
{
  size_t len = length();

  // Store the number of characters, then the string itself,
  // without the terminating null:
  if( file.Write(len) )  file.Write(data(), len);
}


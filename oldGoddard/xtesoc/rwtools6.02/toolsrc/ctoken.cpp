/*
 * Definitions for the RWCTokenizer class
 *
 * $Id: ctoken.cpp,v 1.8 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctoken.cpp,v $
 * Revision 1.8  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.7  1993/07/29  16:02:01  myersn
 * simplify logic.
 *
 * Revision 1.6  1993/07/29  07:34:19  myersn
 * tolerate nulls.
 *
 * Revision 1.5  1993/04/09  20:57:09  keffer
 * Indexing now done using size_t
 *
 * Revision 1.4  1993/03/20  22:55:13  keffer
 * Eliminated int to unsigned conversion.
 *
 * Revision 1.3  1993/02/14  05:23:03  myersn
 * use size_t for indices, not int (and RW_NPOS rather than -1 for bad index).
 *
 * Revision 1.2  1993/02/11  01:50:34  myersn
 * tolerate nulls.
 *
 * Revision 1.1  1993/02/11  00:50:13  myersn
 * Initial revision
 *
 * Revision 2.2  1992/11/14  23:23:38  keffer
 * Now includes header file <string.h> (which used to be included
 * by "rw/cstring.h".
 *
 * Revision 2.1  1992/11/05  18:59:55  keffer
 * Removed ^Z from end-of-file.
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   04 Mar 1992 09:17:30   KEFFER
 * nil replaced with rwnil
 * 
 *    Rev 1.2   28 Oct 1991 09:24:32   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.1   22 Aug 1991 15:04:48   keffer
 * operator()() no longer takes a default argument, as per the ARM.
 * 
 *    Rev 1.0   28 Jul 1991 08:39:52   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/ctoken.h"
#include <string.h>

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: ctoken.cpp,v $ $Revision: 1.8 $ $Date: 1993/09/10 03:59:57 $");

RWCTokenizer::RWCTokenizer(const RWCString& s)
{
  theString = &s;
  place = rwnil;
}

RWCSubString
RWCTokenizer::operator()(const char* ws)
{
  RWPRECONDITION( ws!=0 );

  char const* eos = theString->data() + theString->length();

  if (place==rwnil)		// the first time through?
    place = theString->data();	// Initialize 'place'

  size_t extent = 0;
  while (1) {
    if (place >= eos) return (*theString)(RW_NPOS,0);
    place += strspn(place, ws);
    extent = strcspn(place, ws);
    if (extent) break;
    ++place; // skip null
  }
  size_t start = place - theString->data();
  place += extent;		// Advance the placeholder

  return (*theString)(start, extent);
}

RWCSubString
RWCTokenizer::operator()()
  { return operator()(" \t\n"); }


/*
 * Definitions for doubly-linked list iterators.
 *
 * $Id: dlistit.cpp,v 1.3 1993/11/09 08:06:08 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1990, 1991, 1992, 1992, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dlistit.cpp,v $
 * Revision 1.3  1993/11/09  08:06:08  jims
 * Port to ObjectStore
 *
 * Revision 1.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.1  1993/05/14  19:32:30  dealys
 * Initial revision
 *
 * 
 */

#include "rw/dlist.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: dlistit.cpp,v $ $Revision: 1.3 $ $Date: 1993/11/09 08:06:08 $");

#ifndef RW_NO_CPP_RECURSION
# define new rwnew
#endif


/********************************************************
*							*
*		RWDlistIterator definitions		*
*							*
********************************************************/

void*
RWDlistIterator::operator+=(size_t n)
{
  void* p = key();
  while (n--)
    p = ++(*this);
  return p;
}

void*
RWDlistIterator::operator-=(size_t n)
{
  void* p = key();
  while (n--)
    p = --(*this);
  return p;
}

void*
RWDlistIterator::findNext(RWtestGeneric tst, const void* x)
{
  RWPRECONDITION( tst!=0 );
  void* p;
  while ( (p = (*this)()) != 0 )  // advances iterator
    if ( (*tst)(p, x) ) return p;
  return rwnil;
}

void*
RWDlistIterator::findNextReference(const void* a)
{
  void* p;
  while ( (p = (*this)()) != 0 ){	// This will advance iterator.
    if ( p==a )	 return p;		// Is this the victim?
  }
  return rwnil;
}

void*
RWDlistIterator::insertAfterPoint(void* a)
{
  RWIsvDlistIterator::insertAfterPoint(new RWPDlink(a));
  return a;
}

/*
 * Remove first occurrence where the tester returns true.
 */
void*
RWDlistIterator::removeNext(RWtestGeneric tst, const void* x)
{
  RWPRECONDITION( tst!=rwnil );

  void* p;
  while ( (p = ++(*this)) != rwnil )
  {
    if ( tst(p, x) )		// hit?
      return remove();
  }
  return rwnil;
}

/*
 * Remove first occurrence of the pointer "a".
 */
void*
RWDlistIterator::removeNextReference(const void* a)
{
  RWPRECONDITION( a!=rwnil );

  void* p;
  while ( (p = ++(*this)) != rwnil )
  {
    if (p==a)
      return remove();
  }
  return rwnil;
}


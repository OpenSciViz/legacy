/*
 * RWReadManager --- returns current read table, based on current thread.
 *
 * $Id: readmgr.cpp,v 1.4 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: readmgr.cpp,v $
 * Revision 1.4  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 1.3  1993/09/03  18:37:49  jims
 * Cast void* to actual class pointer for delete in function deleteValue()
 *
 * Revision 1.2  1993/08/06  03:53:24  randall
 * use rwfar instead of FAR
 *
 * Revision 1.1  1993/04/12  12:08:54  jims
 * Initial revision
 *
 * Revision 2.1  1993/02/05  07:28:23  jims
 * Modified to use new version of Instance Manager
 *
 * Revision 2.0  1992/10/23  03:36:49  keffer
 * RCS Baseline version
 *
 */

#include "readmgr.h"

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: readmgr.cpp,v $ $Revision: 1.4 $ $Date: 1993/09/10 03:59:57 $");

RWReadManager rwReadManager;	// The one-of-a-kind RWReadTable manager

RWReadTable*
RWReadManager::currentReadTable()
{
  RWReadTable* table = (RWReadTable*)currentValue();
  return table;
}

RWReadTable*
RWReadManager::newReadTable()
{
  RWReadTable* table = (RWReadTable*)addValue();
  return table;
}

// Redefined from class RWInstanceManager:
void rwfar*
RWReadManager::newValue()
{
  RWReadTable* table = new RWReadTable;
  return (void rwfar*) table;
}

// Redefined from class RWInstanceManager:
void
RWReadManager::deleteValue(void rwfar *table)
{
  delete (RWReadTable*)table;
}

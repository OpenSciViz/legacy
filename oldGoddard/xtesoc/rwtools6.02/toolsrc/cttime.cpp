/*
 * Definitions for RWCollectable Times
 *
 * $Id: cttime.cpp,v 2.2 1993/09/10 03:59:57 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cttime.cpp,v $
 * Revision 2.2  1993/09/10  03:59:57  keffer
 * Added RW_RCSID macro.
 *
 * Revision 2.1  1993/05/18  16:28:26  dealys
 * merged with ctimedf.cpp
 *
 * Revision 2.0  1992/10/23  03:36:08  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   04 Mar 1992 10:27:18   KEFFER
 * Changed PRECONDITION to RWPRECONDITION
 * 
 *    Rev 1.1   28 Oct 1991 09:24:02   keffer
 * Include file path now <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:39:14   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/colltime.h"
#include "defcol.h"   

RW_RCSID("Copyright (C) Rogue Wave Software --- $RCSfile: cttime.cpp,v $ $Revision: 2.2 $ $Date: 1993/09/10 03:59:57 $");

RWDEFINE_COLLECTABLE2(RWCollectableTime, __RWCOLLECTABLETIME)

RWCollectableTime::RWCollectableTime() : RWTime() { }            

int
RWCollectableTime::compareTo(const RWCollectable* c) const
{
  return RWTime::compareTo( (const RWTime*)(const RWCollectableTime*)c );
}

unsigned
RWCollectableTime::hash() const
{
  return RWTime::hash();
}

RWBoolean
RWCollectableTime::isEqual(const RWCollectable* c) const
{
  RWPRECONDITION( c!=0 );
  if( c->isA() != RWCollectableTime::isA() ) return FALSE;
  return *(const RWTime*)this ==  *(const RWTime*)(const RWCollectableTime*)c;
}


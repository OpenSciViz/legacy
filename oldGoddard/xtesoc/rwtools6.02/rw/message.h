#ifndef __RWMESSAGE_H__
#define __RWMESSAGE_H__

/*
 * Message string facility
 *
 * $Id: message.h,v 1.3 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: message.h,v $
 * Revision 1.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.2  1993/05/31  21:45:26  keffer
 * New messaging architecture for localization
 *
 * Revision 1.1  1993/05/18  00:41:12  keffer
 * Initial revision
 *
 *
 */

#include "rw/defs.h"


struct RWCatMsg
{
  const char* domainName_;	// Called "Catalog Name" by catgets()
  int         msgNumber_;	// Message number
  const char* msg_;		// Default string (catgets())
				//   or lookup key (gettext())
};

typedef const RWCatMsg* RWMsgId;

class RWExport RWMessage
{
public:
  RWMessage(const char*);
  RWMessage(RWMsgId ...);
  RWMessage(const RWMessage&);
  ~RWMessage();
  RWMessage& operator=(const RWMessage&);
  const char* str() const {return msg_;}
private:
  char* msg_;
};

const char* rwexport rwMsgLookup(RWMsgId msgId);

#endif /* __RWMESSAGE_H__ */

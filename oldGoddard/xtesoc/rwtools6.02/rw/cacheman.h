#ifndef __RWCACHEMAN_H__
#define __RWCACHEMAN_H__

/*
 * RWCacheManager --- caches fixed length blocks to and from disk
 *
 * $Id: cacheman.h,v 2.3 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cacheman.h,v $
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.2  1993/03/23  02:21:53  keffer
 * Eliminated int to unsigned conversions.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.2   18 Feb 1992 09:54:10   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:08   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:12:58   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/tooldefs.h"

class RWExport RWCacheManager
{

public:

  RWCacheManager(RWFile* file, unsigned blocksz, unsigned mxblks = 10);
  ~RWCacheManager();

  RWBoolean		flush();	// Perform any pending writes.
  void			invalidate();	// Invalidate the entire cache
  RWBoolean		read(RWoffset locn, void* dat);
  RWBoolean		write(RWoffset locn, void* dat);

private:

  RWCacheManager(const RWCacheManager&); // Private to insure no copies
  void			operator=(const RWCacheManager&); // Ditto
  unsigned		ageAndFindSlot(RWoffset);
  RWBoolean		flush(unsigned);
  unsigned		getFreeSlot();
  unsigned		LRU() const; // Find least recently used block

private:

  RWFile*		theFile_;
  unsigned		maxblocks_; // Max # blocks in buff_er
  unsigned		nused_;	    // Number being used.
  unsigned		blocksize_; // Size of a block
  RWoffset*		diskAddrs_; // Its disk address
  unsigned*		useCounts_; // Use count for the block
  char*			buff_;	    // The set of blocks.
};

#endif

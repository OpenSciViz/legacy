#ifndef __RWRAND_H__
#define __RWRAND_H__
/* 
 * Random number generator classes
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * These random number classes are new to math.h++ v5.1.  The old classes
 * are available in math.h++ 5.1 in rand*.h.  They will be phased out.
 *
 * RWRandGenerator:    random number generator - spits out random longs
 *
 * RWRand:             abstract base class for distribution generators
 *   RWRandUniform:    random numbers from a uniform distribution
 *   RWRandNormal:     random numbers from a normal distribution
 *   RWRandExponential:random numbers from an exponential distribution
 *   RWRandGamma:      random numbers from a gamma distribution
 *   RWRandPoisson:    random integers from a Poisson distribution
 *   RWRandBinomial:   random integers from a binomial distribution
 *
 * Each class in the RWRand hierarchy contains a RWRandGenerator object
 * that controls the sequence of numbers generated.  RWRandGenerators can
 * either share state with each other or be independent - you can either
 * have all your generators key off of one generator, or have each use
 * independent generators.
 * 
 * Using the default generator state will not guarantee repeatable
 * sequences, even if you restart() it using a specific seed.  This is
 * because the global state used by default may be shared amoung several
 * applications in a shared library or multithreaded environment.  There
 * is no protection against multiple threads of control using the global
 * random generator at the same time.  The results of doing this can be,
 * well, random.  To achieve repeatable sequences construct a generator
 * with a specific seed value and use that in all your RWRand objects.
 * 
 * Use the randFill() member functions in the vector/matrix/array routines
 * to create bunches of random numbers.
 *
 * All the action of the generator happens in the RWRandGeneratorState
 * class.  If it is desireable, it would be easy enough to broaden the
 * interface a little so that users could plug in their own particular
 * random number generators.  The interface would be analagous to the one
 * used to get your own memory block types into vectors.
 *
 ***************************************************************************
 *
 * $Log: rand.h,v $
 * Revision 1.2  1993/09/17  14:16:17  alv
 * add random complex numbers
 *
 * Revision 1.1  1993/09/17  02:19:34  alv
 * Initial revision
 *
 */

#include "rw/mathdefs.h"
#include "rw/dcomplex.h"      /* Looking for typedef for DComplex */

class RWRandGeneratorState;   // Reference counted shared part of generator objects

class RWRandGenerator {
private:
  RWRandGeneratorState *state_;            // reference counted guts of the generator
public:
  RWRandGenerator();                       // uses shared global random state
  RWRandGenerator(long seed);              // specify the seed
  RWRandGenerator(const RWRandGenerator&); // generators will share state
  ~RWRandGenerator();

  long   operator()();                     // returns a random long integer
  long   seed()       const;               // return seed used to initialize
  void   restart();                        // restart with a random seed
  void   restart(long seed);               // restart with specified seed
  void   reference(const RWRandGenerator&);// set state to argument's state
  void   deepenShallowCopy();              // make state independent (but don't change it)

  void   operator=(const RWRandGenerator&);// not implemented, here to avoid the compiler building one
};

class RWRand {
protected:                         // not private since derived classes use the generator
  RWRandGenerator generator_;
public:
  RWRand();
  RWRand(const RWRandGenerator&);

  virtual double operator()() =0;          // return a random deviate
  virtual DComplex complex();              // default: arg is chosen from uniform(0,2pi), norm using op()
  virtual double lowBound() const =0;      // lower boundary of > 99% of distribution
  virtual double highBound() const =0;     // upper boundary of > 99% of distribution
  RWRandGenerator generator() const;       // return a generator with same state as mine
  void setGenerator(const RWRandGenerator&);//set the random number generator
  void setGenerator(const RWRand&);        // set to generator inside argument
};

class RWRandUniform : public RWRand {
private:
  double a_,b_;                            // values generated are in [a,b]
public:
  RWRandUniform();                         // uses default range of [0,1]
  RWRandUniform(double a, double b);       // sets range to [a,b]
  RWRandUniform(const RWRandUniform&);
  RWRandUniform(const RWRandGenerator&, double a=0, double b=1);
  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  void           setRange(double a, double b)      {a_=a; b_=b;}
};

class RWRandNormal : public RWRand {
private:
  double mean_, variance_;
public:
  RWRandNormal();                          // mean of zero, variance of one
  RWRandNormal(double mean, double variance);
  RWRandNormal(const RWRandNormal&);
  RWRandNormal(const RWRandGenerator&, double mean=0, double variance=1);

  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  void   setMean(double mean)              {mean_=mean;}
  void   setVariance(double var)           {variance_=var;}
  double mean() const                      {return mean_;}
  double variance() const                  {return variance_;}
};

class RWRandExponential : public RWRand {
private:
  double lambda_;                          // 1/lambda is mean time between events
public:
  RWRandExponential(double lambda=1);
  RWRandExponential(const RWRandExponential&);
  RWRandExponential(const RWRandGenerator&, double lambda=1);

  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  double         lamdba() const            {return lambda_;}
  void           setLambda(double lambda)  {lambda_=lambda;}
};

/*
 * RWRandGamma, RWRandPoisson, RWRandBinomial are not implemented yet.
 */
#if 0

class RWRandGamma : public RWRand {
private:
  int order_;                              // order is number of events which occur
public:
  RWRandGamma(int order=1);
  RWRandGamma(const RWRandGamma&);
  RWRandGamma(const RWRandGenerator&, int order=1);
  
  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  int            order() const              {return order_;}
  void           setOrder(int order)        {order_=order;}
};

class RWRandPoisson : public RWRand {
private:
  double mean_;                            // mean of the distribution
public:
  RWRandPoisson(double mean=1);
  RWRandPoisson(const RWRandPoisson&);
  RWRandPoisson(const RWRandGenerator&, double mean=1);

  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  double         mean() const               {return mean_;}
  void           setMean(double mean)       {mean_=mean;}
};
 
class RWRandBinomial : public RWRand {
private:
  int n_;                                   // number of trials
  double p_;                                // probability of success
public:
  RWRandBinomial();
  RWRandBinomial(int n, double p);
  RWRandBinomial(const RWRandBinomial&);
  RWRandBinomial(const RWRandGenerator&, int n=1, double p=0.5);
  
  virtual double operator()();
  virtual double lowBound() const;
  virtual double highBound() const;
  int            n() const                  {return n_;}
  double         p() const                  {return p_;}
  void           setN(int n)                {n_=n;}
  void           setP(double p)             {p_=p;}
};
#endif
#endif

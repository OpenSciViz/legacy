#ifndef __RWUCVECPIK_H__
#define __RWUCVECPIK_H__

/*
 * Declarations for UCharVecPick --- picks elements out of a UCharVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The UCharVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class UCharVecPick {
private:
  UCharVec		V;
  const IntVec		pick;
  UCharVecPick(UCharVec& v, const IntVec& x);	// Constructor is private
  friend		UCharVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const UCharVec&);
  void			operator=(const UCharVecPick&);
  void			operator=(UChar);

  inline UChar&	operator()(int i);
  inline UChar		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
UCharVecPick::UCharVecPick(UCharVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline UChar& UCharVecPick::operator()(int i)       { return V( pick(i) ); }
inline UChar  UCharVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__UCVECPIK_H__*/

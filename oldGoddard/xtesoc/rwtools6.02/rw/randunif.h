#ifndef __RWRANDUNIF_H__
#define __RWRANDUNIF_H__

/*
 * Declarations for RandUniform: Uniformally distributed random numbers.
 *
 * $Header: /users/rcs/mathrw/randunif.h,v 1.2 1993/01/26 17:10:40 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randunif.h,v $
 * Revision 1.2  1993/01/26  17:10:40  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:08:42  alv
 * Initial revision
 *
 * 
 *    Rev 1.4   17 Oct 1991 09:12:54   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   03 Sep 1991 09:55:48   keffer
 * Changed to an additive congruential generator.  Now each generator
 * has an independent "state".
 * 
 *    Rev 1.1   24 Jul 1991 13:01:12   keffer
 * Added pvcs keywords
 *
 */

#include "rw/defs.h"

class DoubleGenMat;
class DoubleVec;

class RandUniform {
private:
  long			table[55];
  int			index;
protected:
  long	 		seeder;		// Random number generator seed.
public:
  RandUniform();			// Initialize a generator with clock time.
  RandUniform(long s){reseed(s);}	// Initialize a generator with seed.

  long  		seed() const {return seeder;}
  void			reseed(long);			// Reseed with given seed

  double    		randValue();			// Return a random number.
  void    		randValue(double*, unsigned n);	// Return an array of random numbers.
  DoubleVec 		randValue(unsigned n);		// Return a vector of n random numbers.
  DoubleGenMat 		randValue(unsigned nr, unsigned nc);	// Return a matrix of random numbers.
};
  
#endif /*__RWRANDUNIF_H__*/


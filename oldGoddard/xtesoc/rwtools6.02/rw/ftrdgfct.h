#ifndef __RWFTRDGFCT_H__
#define __RWFTRDGFCT_H__

/*
 * FloatTriDiagFact - A factorization of a tri-diagonal matrix
 *
 * Generated from template $Id: xfct.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1988-1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This header file will work both with lapack.h++ and linpack.h++.
 */

#include "rw/ftrdgmat.h"
#include "rw/ivec.h"

class FloatTriDiagFact {
private:
    FloatVec d;      // diagonal
    FloatVec dl;     // subdiagonal
    FloatVec du;     // superdiagonal
    FloatVec d2;     // second superdiagonal of U
    long *pvts;            // used by Lapack.h++
    IntVec pivots;         // used by Linpack.h++
    // The factorization and the pivot vector.
    // This cannot be a union because IntVec has a constructor

union {
    long   info;           // used by Lapack.h++
};
  // If info is non-zero, something went wrong in the factorization.
  
union { 
    Float    Anorm;          // used by Lapack.h++
    Float    cond;           // used by Linpack.h++
};
    // If Anorm is non-negative, then it is the infinity norm of the
    // original matrix.  This is used when computing the condition
    // number.
    // cond is the reciprocal condition number if calculated.  If
    // condition number not calculated contains 0 for singularity
    // else -1.

protected:
           
    static const char *className;
    // For use in error messages

    void dofactor(RWBoolean estimateCondition);
    // Do the factorization.  Call this after the matrix 
    // variable factorization is set up.

    void calldi(const FloatTriDiagMat& A, int job, Float* det, int *inert) const;
    // call the linpack xxxdi routine with the job number indicated
    // and with the 2 byte array det.

public:
    FloatTriDiagFact();
    FloatTriDiagFact( const FloatTriDiagFact& );
    FloatTriDiagFact( const FloatTriDiagMat& A, RWBoolean estimateCondition=TRUE );
    void factor( const FloatTriDiagMat& A, RWBoolean estimateCondition=TRUE );
    ~FloatTriDiagFact();
    // The constructors.
    // You can save a little bit of time by not estimating the
    // condition of the factorization, and thus giving up any 
    // knowledge of how accurate solutions obtained using this
    // factorization will be.

    RWBoolean good() const { return !fail(); }
    RWBoolean fail() const;
    RWBoolean isSingular() const;
    int       rows() const { return d.length(); }
    int       cols() const { return d.length(); }
    Float condition() const;
    // If fail() returns true calling solve() or inverse() or ... may
    // fail.  condition() returns an approximation to the reciprocal
    // condition number of the factorization.

    FloatVec solve(const FloatVec& b) const;
    FloatGenMat solve(const FloatGenMat& b) const;
    Float determinant() const;
    // solve and inverse functions
};

FloatVec solve(const FloatTriDiagFact& A, const FloatVec& b);
FloatGenMat solve(const FloatTriDiagFact& A, const FloatGenMat& b);
Float    determinant (const FloatTriDiagFact& A);


Float condition(const FloatTriDiagFact& A);

#endif

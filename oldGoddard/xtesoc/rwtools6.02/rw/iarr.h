#ifndef __RWIARR_H__
#define __RWIARR_H__

/*
 * Declarations for Int precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a int or int&.
 */

#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class IntArray : public RWArrayView {
private:
    IntArray(const RWDataView&, int*, const IntVec&, const IntVec&); // For real() and imag() and slices

public:

    /****************
     * Constructors *
     ****************/

    IntArray();                                 // Declares a scalar (0D array)
    IntArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    IntArray(unsigned,unsigned,unsigned,RWUninitialized);
    IntArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    IntArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    IntArray(unsigned,unsigned,unsigned,RWRand&);
    IntArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    IntArray(const IntVec& n, int val);
    IntArray(unsigned,unsigned,unsigned, int val);
    IntArray(unsigned,unsigned,unsigned,unsigned, int val);
    IntArray(const char *);
    IntArray(const IntArray& a);
    IntArray(const int* dat, const IntVec& n);  // Copy of dat will be made
    IntArray(const int* dat, unsigned,unsigned,unsigned);
    IntArray(const int* dat, unsigned,unsigned,unsigned,unsigned);
    IntArray(const IntVec& vec, const IntVec& n); // View of dat will be taken
    IntArray(const IntVec& vec, unsigned,unsigned,unsigned);
    IntArray(const IntVec& vec, unsigned,unsigned,unsigned,unsigned);
    IntArray(RWBlock *block, const IntVec& n);  // Use a custom block for the data
    IntArray(RWBlock *block, unsigned,unsigned,unsigned);
    IntArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    IntArray(const IntVec&);                    // View will be taken
    IntArray(const IntGenMat&);                 // View will be taken

    operator DoubleArray() const;               // Conversion to DoubleArray

    /********************
     * Member functions *
     ********************/

    IntArray      apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    IntArray      apply(XmathFunTy)     const;
#endif  
    unsigned      binaryStoreSize()     const;   // Storage requirements
    IntArray      copy()                const;   // Copy with distinct instance variables
    int*          data()                         {return (int*)begin;}
    const int*    data()                const    {return (int*)begin;}
    IntArray      deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    IntArray&     reference(const IntArray& v);  // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    IntArray      slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    int&            operator[](const IntVec& i); // With bounds checking
    int             operator[](const IntVec& i) const;
    int&            operator()(const IntVec& i); // With optional bounds checking
    int             operator()(const IntVec& i) const;
    int&            operator()(int,int,int);
    int             operator()(int,int,int) const;
    int&            operator()(int,int,int,int);
    int             operator()(int,int,int,int) const;
    IntVec          operator()(const RWSlice&,int,int);
    const IntVec    operator()(const RWSlice&,int,int) const;
    IntVec          operator()(int,const RWSlice&,int);
    const IntVec    operator()(int,const RWSlice&,int) const;
    IntVec          operator()(int,int,const RWSlice&);
    const IntVec    operator()(int,int,const RWSlice&) const;
    IntVec          operator()(const RWSlice&,int,int,int);
    const IntVec    operator()(const RWSlice&,int,int,int) const;
    IntVec          operator()(int,const RWSlice&,int,int);
    const IntVec    operator()(int,const RWSlice&,int,int) const;
    IntVec          operator()(int,int,const RWSlice&,int);
    const IntVec    operator()(int,int,const RWSlice&,int) const;
    IntVec          operator()(int,int,int,const RWSlice&);
    const IntVec    operator()(int,int,int,const RWSlice&) const;
    IntGenMat       operator()(int,const RWSlice&,const RWSlice&);
    const IntGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    IntGenMat       operator()(const RWSlice&,int,const RWSlice&);
    const IntGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    IntGenMat       operator()(const RWSlice&,const RWSlice&,int);
    const IntGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    IntGenMat       operator()(int,int,const RWSlice&,const RWSlice&);
    const IntGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    IntGenMat       operator()(int,const RWSlice&,int,const RWSlice&);
    const IntGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    IntGenMat       operator()(int,const RWSlice&,const RWSlice&,int);
    const IntGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    IntGenMat       operator()(const RWSlice&,int,int,const RWSlice&);
    const IntGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    IntGenMat       operator()(const RWSlice&,int,const RWSlice&,int);
    const IntGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    IntGenMat       operator()(const RWSlice&,const RWSlice&,int,int);
    const IntGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    IntArray        operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const IntArray  operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    IntArray        operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const IntArray  operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    IntArray        operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const IntArray  operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    IntArray        operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const IntArray  operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    IntArray        operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const IntArray  operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    IntArray        operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const IntArray  operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    IntArray&   operator=(const IntArray& v);   // Must be same length as v
    IntArray&   operator=(int);
    RWBoolean   operator==(const IntArray&) const;
    RWBoolean   operator!=(const IntArray& v) const;
    IntArray&   operator+=(int);
    IntArray&   operator-=(int s)               { return operator+=(-s); }
    IntArray&   operator*=(int);
    IntArray&   operator/=(int s);
    IntArray&   operator+=(const IntArray&);
    IntArray&   operator-=(const IntArray&);
    IntArray&   operator*=(const IntArray&);
    IntArray&   operator/=(const IntArray&);
    IntArray&   operator++();                   // Prefix operator
    IntArray&   operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

    friend int       toScalar(const IntArray&);    // For 0-D arrays
    friend IntVec    toVec(const IntArray&);       // For 1-D arrays
    friend IntGenMat toGenMat(const IntArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

           IntArray    operator-(const IntArray&);
    inline IntArray    operator+(const IntArray& v) { return v; }
           IntArray    operator+(const IntArray&, const IntArray&);
           IntArray    operator-(const IntArray&, const IntArray&);
           IntArray    operator*(const IntArray&, const IntArray&);
           IntArray    operator/(const IntArray&, const IntArray&);
           IntArray    operator+(const IntArray&,int);
           IntArray    operator+(int, const IntArray&);
           IntArray    operator-(const IntArray&,int);
           IntArray    operator-(int, const IntArray&);
           IntArray    operator*(const IntArray&,int);
           IntArray    operator*(int, const IntArray&);
           IntArray    operator/(const IntArray&,int);
           IntArray    operator/(int, const IntArray&);
           ostream&    operator<<(ostream& s, const IntArray& v);
           istream&    operator>>(istream& s, IntArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline IntArray    operator*(int s, const IntArray& V) {return V*s;}
    inline IntArray    operator+(int s, const IntArray& V) {return V+s;}
    inline IntArray    operator-(const IntArray& V, int s) {return V+(int)(-s);}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           IntArray    abs(const IntArray& V);
           int         dot(const IntArray&,const IntArray&); // Return sum_ij...k (Aij...k * Bij...k)
           IntArray    dot(const IntVec&,const IntArray&); // Return sum_i (Vi * Aij...k)
           IntArray    dot(const IntArray&,const IntVec&); // Return sum_k (Ai...jk * Vk)
           IntVec      maxIndex(const IntArray&); // Returns *index* of max value
           int         maxValue(const IntArray&);
           IntVec      minIndex(const IntArray&); // Returns *index* of min value
           int         minValue(const IntArray&);
           int         prod(const IntArray&);
           int         sum(const IntArray&);


    /**** Functions for type conversion ****/

           IntArray    toInt(const DoubleArray&);
           IntArray    toInt(const FloatArray&);

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline int& IntArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline int IntArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline int& IntArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline int IntArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline int& IntArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline int IntArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline int& IntArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline int IntArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWIARR_H__*/

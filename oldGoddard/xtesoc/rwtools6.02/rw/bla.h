#ifndef __RWBLAX_H__
#define __RWBLAX_H__
/*
 * The wrapper above contains the X to distinguish it from
 * the math.h++ 5.0.0 of the wrapper for rw/rwbla.h. 
 */

/*
 * $Header: /users/rcs/mathrw/bla.h,v 1.9 1993/07/06 18:29:19 alv Exp $
 *
 * Prototypes for old style f2c translated level 1 blas, and new
 * style Cobalt Blue FOR_C++ translated level 1,2,3 blas.  The former
 * are used by default, the latter if RW_CPPBLAS is defined.  This
 * allows development on lapack.h++ to proceed while math.h++ continues
 * to use the old style blas.  Eventually, math.++ will also use the
 * new style blas and the old ones will be eliminated.
 *
 * Copyright (1991-1993) by Rogue Wave Associates.
 * This software is subject to copyright protection under
 * the laws of the United States and other countries.
 *
 * $Log: bla.h,v $
 * Revision 1.9  1993/07/06  18:29:19  alv
 * commented out single precision complex blas in an effort to ease
 * MS-DOS memory problems
 *
 * Revision 1.7  1993/06/21  16:56:16  alv
 * modify wrapper to avoid conflict with rwbla.h
 *
 * Revision 1.6  1993/03/19  22:34:08  alv
 * changed include mathdefs.h to rw/mathdefs.h for bcc
 *
 * Revision 1.5  1993/03/19  21:56:19  alv
 * added RWBLADECL linkage specifier
 *
 * Revision 1.4  1993/03/06  00:55:18  alv
 * eliminated const where info is passed back through args
 *
 * Revision 1.3  1993/03/05  23:04:10  alv
 * changed ref parms to const ref
 *
 * Revision 1.2  1993/03/03  01:05:15  alv
 * updated for lapack.h++
 *
 */

#if defined(RWLAPACK) || defined(RW_CPPBLAS)

/*
 * Prototypes for level 1,2,3 blas in float, double, FComplex, and DComplex
 * precisions.  Also includes prototypes for lsame() and xerbla().
 *
 * Prototypes built using Cobalt Blue's FOR_C++ v1.1 (P) Fortran to C
 * translator, and then hand massaged to fit the Rogue Wave guidelines
 * for lapack.h++.  The original Cobalt Blue generated header files have
 * been retained ([xsdcz]bluebla.h) to allow future translation of Fortran
 * files that use the blas.
 */

/*
 * Need to actually include dcomplex.h --- since DComplex may just
 * be a typedef we can't just forward declare it as a class.
 */
#include "rw/mathdefs.h"
#include "rw/dcomplex.h"
class FComplex;

/*
 * The RWBLADECL macro is set in mathdefs.h for versions of math.h++ higher
 * than 5.0.0.  We set it here in case this is math.h++ 5.0.0 and it has
 * not been redefined in mathdefs.h already
 */
#if RWMATH<=0x500 && !defined(RWBLADECL)
#define RWBLADECL extern "C"
#endif

RWBLADECL int      lsame(const char&,const char&);
RWBLADECL void     xerbla(char*,const long&);

RWBLADECL long     isamax(const long&,float[],const long&);
RWBLADECL float    sasum(const long&,float[],const long&);
RWBLADECL void     saxpy(const long&,const float&,float[],const long&,float[],const long&);
RWBLADECL float    scasum(const long&,FComplex[],const long&);
RWBLADECL float    scnrm2(const long&,FComplex[],const long&);
RWBLADECL void     scopy(const long&,float[],const long&,float[],const long&);
RWBLADECL float    sdot(const long&,float[],const long&,float[],const long&);
RWBLADECL void     sgbmv(const char&,const long&,const long&,const long&,const long&,const float&,float*,const long&,float[],const long&,const float&,float[],const long&);
RWBLADECL void     sgemm(const char&,const char&,const long&,const long&,const long&,const float&,float*,const long&,float*,const long&,const float&,float*,const long&);
RWBLADECL void     sgemv(const char&,const long&,const long&,const float&,float*,const long&,float[],const long&,const float&,float[],const long&);
RWBLADECL void     sger(const long&,const long&,const float&,float[],const long&,float[],const long&,float*,const long&);
RWBLADECL float    snrm2(const long&,float[],const long&);
RWBLADECL void     srot(const long&,float[],const long&,float[],const long&,const float&,const float&);
RWBLADECL void     srotg(float&,float&,float&,float&);
RWBLADECL void     ssbmv(const char&,const long&,const long&,const float&,float*,const long&,float[],const long&,const float&,float[],const long&);
RWBLADECL void     sscal(const long&,const float&,float[],const long&);
RWBLADECL void     sspmv(const char&,const long&,const float&,float[],float[],const long&,const float&,float[],const long&);
RWBLADECL void     sspr(const char&,const long&,const float&,float[],const long&,float[]);
RWBLADECL void     sspr2(const char&,const long&,const float&,float[],const long&,float[],const long&,float[]);
RWBLADECL void     sswap(const long&,float[],const long&,float[],const long&);
RWBLADECL void     ssymm(const char&,const char&,const long&,const long&,const float&,float*,const long&,float*,const long&,const float&,float*,const long&);
RWBLADECL void     ssymv(const char&,const long&,const float&,float*,const long&,float[],const long&,const float&,float[],const long&);
RWBLADECL void     ssyr(const char&,const long&,const float&,float[],const long&,float*,const long&);
RWBLADECL void     ssyr2(const char&,const long&,const float&,float[],const long&,float[],const long&,float*,const long&);
RWBLADECL void     ssyr2k(const char&,const char&,const long&,const long&,const float&,float*,const long&,float*,const long&,const float&,float*,const long&);
RWBLADECL void     ssyrk(const char&,const char&,const long&,const long&,const float&,float*,const long&,const float&,float*,const long&);
RWBLADECL void     stbmv(const char&,const char&,const char&,const long&,const long&,float*,const long&,float[],const long&);
RWBLADECL void     stbsv(const char&,const char&,const char&,const long&,const long&,float*,const long&,float[],const long&);
RWBLADECL void     stpmv(const char&,const char&,const char&,const long&,float[],float[],const long&);
RWBLADECL void     stpsv(const char&,const char&,const char&,const long&,float[],float[],const long&);
RWBLADECL void     strmm(const char&,const char&,const char&,const char&,const long&,const long&,const float&,float*,const long&,float*,const long&);
RWBLADECL void     strmv(const char&,const char&,const char&,const long&,float*,const long&,float[],const long&);
RWBLADECL void     strsm(const char&,const char&,const char&,const char&,const long&,const long&,const float&,float*,const long&,float*,const long&);
RWBLADECL void     strsv(const char&,const char&,const char&,const long&,float*,const long&,float[],const long&);

/*
 * Comment out the single precision complex blas, since they are
 * not currently in use in lapack.h++.  Keeping the number of declarations
 * down helps avoid memory problems with MS-DOS.
 */
#if 0
RWBLADECL void     caxpy(const long&,const FComplex&,FComplex[],const long&,FComplex[],const long&);
RWBLADECL void     ccopy(const long&,FComplex[],const long&,FComplex[],const long&);
RWBLADECL FComplex cdotc(const long&,FComplex[],const long&,FComplex[],const long&);
RWBLADECL FComplex cdotu(const long&,FComplex[],const long&,FComplex[],const long&);
RWBLADECL void     cgbmv(const char&,const long&,const long&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex[],const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     cgemm(const char&,const char&,const long&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&,const FComplex&,FComplex*,const long&);
RWBLADECL void     cgemv(const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex[],const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     cgerc(const long&,const long&,const FComplex&,FComplex[],const long&,FComplex[],const long&,FComplex*,const long&);
RWBLADECL void     cgeru(const long&,const long&,const FComplex&,FComplex[],const long&,FComplex[],const long&,FComplex*,const long&);
RWBLADECL void     chbmv(const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex[],const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     chemm(const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&,const FComplex&,FComplex*,const long&);
RWBLADECL void     chemv(const char&,const long&,const FComplex&,FComplex*,const long&,FComplex[],const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     cher(const char&,const long&,const float&,FComplex[],const long&,FComplex*,const long&);
RWBLADECL void     cher2(const char&,const long&,const FComplex&,FComplex[],const long&,FComplex[],const long&,FComplex*,const long&);
RWBLADECL void     cher2k(const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&,const float&,FComplex*,const long&);
RWBLADECL void     cherk(const char&,const char&,const long&,const long&,const float&,FComplex*,const long&,const float&,FComplex*,const long&);
RWBLADECL void     chpmv(const char&,const long&,const FComplex&,FComplex[],FComplex[],const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     chpr(const char&,const long&,const float&,FComplex[],const long&,FComplex[]);
RWBLADECL void     chpr2(const char&,const long&,const FComplex&,FComplex[],const long&,FComplex[],const long&,FComplex[]);
RWBLADECL void     crotg(FComplex&,FComplex&,float&,FComplex&);
RWBLADECL void     cscal(const long&,const FComplex&,FComplex[],const long&);
RWBLADECL void     csrot(const long&,FComplex[],const long&,FComplex[],const long&,const float&,const float&);
RWBLADECL void     csscal(const long&,const float&,FComplex[],const long&);
RWBLADECL void     cswap(const long&,FComplex[],const long&,FComplex[],const long&);
RWBLADECL void     csymm(const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&,const FComplex&,FComplex*,const long&);
RWBLADECL void     csyr2k(const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&,const FComplex&,FComplex*,const long&);
RWBLADECL void     csyrk(const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,const FComplex&,FComplex*,const long&);
RWBLADECL void     ctbmv(const char&,const char&,const char&,const long&,const long&,FComplex*,const long&,FComplex[],const long&);
RWBLADECL void     ctbsv(const char&,const char&,const char&,const long&,const long&,FComplex*,const long&,FComplex[],const long&);
RWBLADECL void     ctpmv(const char&,const char&,const char&,const long&,FComplex[],FComplex[],const long&);
RWBLADECL void     ctpsv(const char&,const char&,const char&,const long&,FComplex[],FComplex[],const long&);
RWBLADECL void     ctrmm(const char&,const char&,const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&);
RWBLADECL void     ctrmv(const char&,const char&,const char&,const long&,FComplex*,const long&,FComplex[],const long&);
RWBLADECL void     ctrsm(const char&,const char&,const char&,const char&,const long&,const long&,const FComplex&,FComplex*,const long&,FComplex*,const long&);
RWBLADECL void     ctrsv(const char&,const char&,const char&,const long&,FComplex*,const long&,FComplex[],const long&);
RWBLADECL long     icamax(const long&,FComplex[],const long&);
#endif

RWBLADECL double   dasum(const long&,double[],const long&);
RWBLADECL void     daxpy(const long&,const double&,double[],const long&,double[],const long&);
RWBLADECL double   dcabs1(const DComplex&);
RWBLADECL void     dcopy(const long&,double[],const long&,double[],const long&);
RWBLADECL double   ddot(const long&,double[],const long&,double[],const long&);
RWBLADECL void     dgbmv(const char&,const long&,const long&,const long&,const long&,const double&,double*,const long&,double[],const long&,const double&,double[],const long&);
RWBLADECL void     dgemm(const char&,const char&,const long&,const long&,const long&,const double&,double*,const long&,double*,const long&,const double&,double*,const long&);
RWBLADECL void     dgemv(const char&,const long&,const long&,const double&,double*,const long&,double[],const long&,const double&,double[],const long&);
RWBLADECL void     dger(const long&,const long&,const double&,double[],const long&,double[],const long&,double*,const long&);
RWBLADECL double   dnrm2(const long&,double[],const long&);
RWBLADECL void     drot(const long&,double[],const long&,double[],const long&,const double&,const double&);
RWBLADECL void     drotg(double&,double&,double&,double&);
RWBLADECL void     dsbmv(const char&,const long&,const long&,const double&,double*,const long&,double[],const long&,const double&,double[],const long&);
RWBLADECL void     dscal(const long&,const double&,double[],const long&);
RWBLADECL void     dspmv(const char&,const long&,const double&,double[],double[],const long&,const double&,double[],const long&);
RWBLADECL void     dspr(const char&,const long&,const double&,double[],const long&,double[]);
RWBLADECL void     dspr2(const char&,const long&,const double&,double[],const long&,double[],const long&,double[]);
RWBLADECL void     dswap(const long&,double[],const long&,double[],const long&);
RWBLADECL void     dsymm(const char&,const char&,const long&,const long&,const double&,double*,const long&,double*,const long&,const double&,double*,const long&);
RWBLADECL void     dsymv(const char&,const long&,const double&,double*,const long&,double[],const long&,const double&,double[],const long&);
RWBLADECL void     dsyr(const char&,const long&,const double&,double[],const long&,double*,const long&);
RWBLADECL void     dsyr2(const char&,const long&,const double&,double[],const long&,double[],const long&,double*,const long&);
RWBLADECL void     dsyr2k(const char&,const char&,const long&,const long&,const double&,double*,const long&,double*,const long&,const double&,double*,const long&);
RWBLADECL void     dsyrk(const char&,const char&,const long&,const long&,const double&,double*,const long&,const double&,double*,const long&);
RWBLADECL void     dtbmv(const char&,const char&,const char&,const long&,const long&,double*,const long&,double[],const long&);
RWBLADECL void     dtbsv(const char&,const char&,const char&,const long&,const long&,double*,const long&,double[],const long&);
RWBLADECL void     dtpmv(const char&,const char&,const char&,const long&,double[],double[],const long&);
RWBLADECL void     dtpsv(const char&,const char&,const char&,const long&,double[],double[],const long&);
RWBLADECL void     dtrmm(const char&,const char&,const char&,const char&,const long&,const long&,const double&,double*,const long&,double*,const long&);
RWBLADECL void     dtrmv(const char&,const char&,const char&,const long&,double*,const long&,double[],const long&);
RWBLADECL void     dtrsm(const char&,const char&,const char&,const char&,const long&,const long&,const double&,double*,const long&,double*,const long&);
RWBLADECL void     dtrsv(const char&,const char&,const char&,const long&,double*,const long&,double[],const long&);
RWBLADECL double   dzasum(const long&,DComplex[],const long&);
RWBLADECL double   dznrm2(const long&,DComplex[],const long&);
RWBLADECL long     idamax(const long&,double[],const long&);

RWBLADECL long     izamax(const long&,DComplex[],const long&);
RWBLADECL void     zaxpy(const long&,const DComplex&,DComplex[],const long&,DComplex[],const long&);
RWBLADECL void     zcopy(const long&,DComplex[],const long&,DComplex[],const long&);
RWBLADECL DComplex zdotc(const long&,DComplex[],const long&,DComplex[],const long&);
RWBLADECL DComplex zdotu(const long&,DComplex[],const long&,DComplex[],const long&);
RWBLADECL void     zdrot(const long&,DComplex[],const long&,DComplex[],const long&,const double&,const double&);
RWBLADECL void     zdscal(const long&,const double&,DComplex[],const long&);
RWBLADECL void     zgbmv(const char&,const long&,const long&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex[],const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zgemm(const char&,const char&,const long&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&,const DComplex&,DComplex*,const long&);
RWBLADECL void     zgemv(const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex[],const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zgerc(const long&,const long&,const DComplex&,DComplex[],const long&,DComplex[],const long&,DComplex*,const long&);
RWBLADECL void     zgeru(const long&,const long&,const DComplex&,DComplex[],const long&,DComplex[],const long&,DComplex*,const long&);
RWBLADECL void     zhbmv(const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex[],const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zhemm(const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&,const DComplex&,DComplex*,const long&);
RWBLADECL void     zhemv(const char&,const long&,const DComplex&,DComplex*,const long&,DComplex[],const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zher(const char&,const long&,const double&,DComplex[],const long&,DComplex*,const long&);
RWBLADECL void     zher2(const char&,const long&,const DComplex&,DComplex[],const long&,DComplex[],const long&,DComplex*,const long&);
RWBLADECL void     zher2k(const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&,const double&,DComplex*,const long&);
RWBLADECL void     zherk(const char&,const char&,const long&,const long&,const double&,DComplex*,const long&,const double&,DComplex*,const long&);
RWBLADECL void     zhpmv(const char&,const long&,const DComplex&,DComplex[],DComplex[],const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zhpr(const char&,const long&,const double&,DComplex[],const long&,DComplex[]);
RWBLADECL void     zhpr2(const char&,const long&,const DComplex&,DComplex[],const long&,DComplex[],const long&,DComplex[]);
RWBLADECL void     zrotg(DComplex&,DComplex&,double&,DComplex&);
RWBLADECL void     zscal(const long&,const DComplex&,DComplex[],const long&);
RWBLADECL void     zswap(const long&,DComplex[],const long&,DComplex[],const long&);
RWBLADECL void     zsymm(const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&,const DComplex&,DComplex*,const long&);
RWBLADECL void     zsyr2k(const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&,const DComplex&,DComplex*,const long&);
RWBLADECL void     zsyrk(const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,const DComplex&,DComplex*,const long&);
RWBLADECL void     ztbmv(const char&,const char&,const char&,const long&,const long&,DComplex*,const long&,DComplex[],const long&);
RWBLADECL void     ztbsv(const char&,const char&,const char&,const long&,const long&,DComplex*,const long&,DComplex[],const long&);
RWBLADECL void     ztpmv(const char&,const char&,const char&,const long&,DComplex[],DComplex[],const long&);
RWBLADECL void     ztpsv(const char&,const char&,const char&,const long&,DComplex[],DComplex[],const long&);
RWBLADECL void     ztrmm(const char&,const char&,const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&);
RWBLADECL void     ztrmv(const char&,const char&,const char&,const long&,DComplex*,const long&,DComplex[],const long&);
RWBLADECL void     ztrsm(const char&,const char&,const char&,const char&,const long&,const long&,const DComplex&,DComplex*,const long&,DComplex*,const long&);
RWBLADECL void     ztrsv(const char&,const char&,const char&,const long&,DComplex*,const long&,DComplex[],const long&);

#else   /* Use old style blas */

/*
 * Function prototypes for the Basic Linear Algebra package (the "BLAs").
 *
 * There are two parts... first there are a set of #defines which
 * determine the actual names of the routines.  By default, a
 * routine named xxxx is called as xxxx_.  You may wish to change
 * this, for example to use a customized set of blas with a
 * different naming convention.
 *
 * After this section of defines comes the second section... the
 * declaration of the default BLAs.
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#  include "rw/mathdefs.h"		/* Looking for "FLinkage" and "FDecl" */
#  include "rw/dcomplex.h"		/* Get declarations for DComplex */
#  include "rw/fcomplex.h"		/* Get declarations for FComplex */

#if defined(__cplusplus)
extern FLinkage {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

#define caxpy caxpy_
#define ccopy ccopy_
#define cdotc cdotc_
#define cdotu cdotu_
#define crotg crotg_
#define cscal cscal_
#define csrot csrot_
#define csscal csscal_
#define cswap cswap_
#define dasum dasum_
#define daxpy daxpy_
#define dcopy dcopy_
#define ddot ddot_
#define dnrm2 dnrm2_
#define drot drot_
#define drotg drotg_
#define dscal dscal_
#define dswap dswap_
#define dzasum dzasum_
#define dznrm2 dznrm2_
#define icamax icamax_
#define idamax idamax_
#define isamax isamax_
#define izamax izamax_
#define sasum sasum_
#define saxpy saxpy_
#define scasum scasum_
#define scnrm2 scnrm2_
#define scopy scopy_
#define sdot sdot_
#define snrm2 snrm2_
#define srot srot_
#define srotg srotg_
#define sscal sscal_
#define sswap sswap_
#define zaxpy zaxpy_
#define zcopy zcopy_
#define zdotc zdotc_
#define zdotu zdotu_
#define zdrot zdrot_
#define zdscal zdscal_
#define zrotg zrotg_
#define zscal zscal_
#define zswap zswap_

int    FDecl caxpy  P_((int *n, FComplex *ca, FComplex *cx, int *incx, FComplex *cy, int* incy));
int    FDecl ccopy  P_((int *n, FComplex *cx, int *incx, FComplex *cy, int *incy));
int    FDecl cdotc  P_((FComplex *ret_val, int *n, FComplex *cx, int *incx, FComplex *cy, int *incy));
int    FDecl cdotu  P_((FComplex *ret_val, int *n, FComplex *cx, int *incx, FComplex *cy, int *incy));
int    FDecl crotg  P_((FComplex *ca, FComplex *cb, float *c, FComplex *s));
int    FDecl cscal  P_((int *n, FComplex *ca, FComplex *cx, int *incx));
int    FDecl csrot  P_((int *n, FComplex *cx, int *incx, FComplex *cy, int *incy, float *c, float *s));
int    FDecl csscal P_((int *n, float *sa, FComplex *cx, int *incx));
int    FDecl cswap  P_((int *n, FComplex *cx, int *incx, FComplex *cy, int *incy));
double FDecl dasum  P_((int *n, double *dx, int *incx));
int    FDecl daxpy  P_((int *n, double *da,   double *dx,   int *incx, double *dy,   int *incy));
int    FDecl dcopy  P_((int *n, double *dx,   int *incx, double *dy,   int *incy));
double FDecl ddot   P_((int *n, double *dx, int *incx, double *dy, int *incy));
double FDecl dnrm2  P_((int *n, double *dx, int *incx));
int    FDecl drot   P_((int *n, double *dx, int *incx, double *dy, int *incy, double *c, double *s));
int    FDecl drotg  P_((double *da, double *db, double *c, double *s));
int    FDecl dscal  P_((int *n, double *da, double *dx, int *incx));
int    FDecl dswap  P_((int *n, double *dx,   int *incx, double *dy,   int *incy));
double FDecl dzasum P_((int *n, DComplex *zx, int *incx));
double FDecl dznrm2 P_((int *n, DComplex *zx, int *incx));
int    FDecl icamax P_((int *n, FComplex *cx, int *incx));
int    FDecl idamax P_((int *n, double *dx, int *incx));
int    FDecl isamax P_((int *n, float *sx, int *incx));
int    FDecl izamax P_((int *n, DComplex *zx, int *incx));
double FDecl sasum  P_((int *n, float *sx, int *incx));
int    FDecl saxpy  P_((int *n, float *sa,    float *sx,    int *incx, float *sy,    int *incy));
double FDecl scasum P_((int *n, FComplex *cx, int *incx));
double FDecl scnrm2 P_((int *n, FComplex *cx, int *incx));
int    FDecl scopy  P_((int *n, float *sx,    int *incx, float *sy,    int *incy));
double FDecl sdot   P_((int *n, float *sx,  int *incx, float *sy,  int *incy));
double FDecl snrm2  P_((int *n, float *sx, int *incx));
int    FDecl srot   P_((int *n, float *sx, int *incx, float *sy, int *incy, float *c, float *s));
int    FDecl srotg  P_((float *sa, float *sb, float *c, float *s));
int    FDecl sscal  P_((int *n, float *sa, float *sx, int *incx));
int    FDecl sswap  P_((int *n, float *sx,    int *incx, float *sy,    int *incy));
int    FDecl zaxpy  P_((int *n, DComplex *za, DComplex *zx, int *incx, DComplex *zy, int *incy));
int    FDecl zcopy  P_((int *n, DComplex *zx, int *incx, DComplex *zy, int *incy));
int    FDecl zdotc  P_((DComplex *ret_val, int *n, DComplex *zx, int *incx, DComplex *zy, int *incy));
int    FDecl zdotu  P_((DComplex *ret_val, int *n, DComplex *zx, int *incx, DComplex *zy, int *incy));
int    FDecl zdrot  P_((int *n, DComplex *zx, int *incx, DComplex *zy, int *incy, double *c, double *s));
int    FDecl zdscal P_((int *n, double *da, DComplex *zx, int *incx));
int    FDecl zrotg  P_((DComplex *ca, DComplex *cb, double *c, DComplex *s));
int    FDecl zscal  P_((int *n, DComplex *za, DComplex *zx, int *incx));
int    FDecl zswap  P_((int *n, DComplex *zx, int *incx, DComplex *zy, int *incy));

#undef P_

#if defined(__cplusplus)
}
#endif

#endif

#endif /*__RWBLA_H__*/


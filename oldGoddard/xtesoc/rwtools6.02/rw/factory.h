
#ifndef __RWFACTORY_H__
#define __RWFACTORY_H__

/*
 * RWFactory --- can create an instance of a registered class
 *
 * $Id: factory.h,v 2.6 1993/09/03 02:08:13 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: factory.h,v $
 * Revision 2.6  1993/09/03  02:08:13  keffer
 * Macro _CLASSDLL is now named _RWTOOLSDLL
 *
 * Revision 2.5  1993/08/05  11:49:12  jims
 * Distinguish between using a WIN16 DLL from a WIN32 DLL by
 * checking for __WIN16__
 *
 * Revision 2.4  1993/04/12  12:32:43  jims
 * The Factory now shared among multiple threads within a single process
 *
 * Revision 2.3  1993/03/13  02:21:20  keffer
 * ClassID->RWClassId
 *
 * Revision 2.2  1993/02/03  00:19:55  jims
 * Removed #include directive for procinit.h
 *
 * Revision 2.1  1993/01/15  22:23:49  keffer
 * Tools.h++ V5.2 alpha
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.5   13 May 1992 10:25:46   KEFFER
 * Changed typedef for userCreator to RWuserCreator.
 * 
 *    Rev 1.4   18 Mar 1992 11:28:32   KEFFER
 * Initializes instance manager.
 * 
 *    Rev 1.3   18 Feb 1992 09:54:20   KEFFER
 * 
 *    Rev 1.2   12 Nov 1991 13:15:00   keffer
 * Factory can now live in the DLL.
 * 
 *    Rev 1.1   28 Oct 1991 09:08:14   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 */

#include "rw/rwset.h"

class RWExport RWCollectable;

class RWExport RWFactory : public RWSet {
public:
  RWFactory();
  ~RWFactory();	

  void 			addFunction(RWuserCreator, RWClassID);
  // Create object with given class id:
  RWCollectable*	create(RWClassID) const; 
  RWuserCreator		getFunction(RWClassID) const;
  void			removeFunction(RWClassID);
};

RWFactory* rwexport	getRWFactory();

#if defined(_RWTOOLSDLL) && defined(__WIN16__)
  /* DLL situation; get the proper factory from the instance manager. */
# define theFactory (getRWFactory())
#else
  extern RWFactory* theFactory;	// The one-of-a-kind global factory.
#endif

#endif /* __RWFACTORY_H__ */

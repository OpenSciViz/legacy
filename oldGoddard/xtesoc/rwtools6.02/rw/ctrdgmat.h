#ifndef __RWCTRDGMAT_H__
#define __RWCTRDGMAT_H__
/*
 * DComplexTriDiagMat - A tridiagonal matrix of DComplexs
 *
 * Stores a tridiagonal matrix.
 *
 * The matrix is stored in the same format as a banded matrix with
 * upper and lower bandwidth one.
 * .fi
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleTriDiagMat;
class DComplexGenMat;
class DComplexBandMat;

class DComplexTriDiagMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DComplexTriDiagMat();
DComplexTriDiagMat( const DComplexTriDiagMat& );
DComplexTriDiagMat(unsigned n, unsigned nAgain);
DComplexTriDiagMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexTriDiagMat(const DoubleTriDiagMat& re);
DComplexTriDiagMat(const DoubleTriDiagMat& re, const DoubleTriDiagMat& im);
~DComplexTriDiagMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 3; }
inline unsigned lowerBandwidth() const { return 1; }
inline unsigned upperBandwidth() const { return 1; }
inline unsigned halfBandwidth() const  { return 1; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline RODComplexRef operator()(int i, int j);
inline RODComplexRef ref(int i, int j);
       RODComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexTriDiagMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DComplexVec diagonal(int =0) const;
       DComplexVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexTriDiagMat& operator=(const DComplexTriDiagMat& m);
DComplexTriDiagMat& reference(DComplexTriDiagMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexTriDiagMat copy() const;
DComplexTriDiagMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexTriDiagMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexTriDiagMat& X);
RWBoolean operator!=(const DComplexTriDiagMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexTriDiagMat& operator+=(const DComplexTriDiagMat& m);
DComplexTriDiagMat& operator-=(const DComplexTriDiagMat& m);
DComplexTriDiagMat& operator*=(const DComplexTriDiagMat& m);
DComplexTriDiagMat& operator*=(DComplex);
DComplexTriDiagMat& operator/=(const DComplexTriDiagMat& m);
DComplexTriDiagMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexTriDiagMat operator-(const DComplexTriDiagMat&);	// Unary minus
DComplexTriDiagMat operator+(const DComplexTriDiagMat&);	// Unary plus
DComplexTriDiagMat operator*(const DComplexTriDiagMat&, const DComplexTriDiagMat&);
DComplexTriDiagMat operator/(const DComplexTriDiagMat&, const DComplexTriDiagMat&);
DComplexTriDiagMat operator+(const DComplexTriDiagMat&, const DComplexTriDiagMat&);
DComplexTriDiagMat operator-(const DComplexTriDiagMat&, const DComplexTriDiagMat&);
DComplexTriDiagMat operator*(const DComplexTriDiagMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexTriDiagMat operator*(DComplex x, const DComplexTriDiagMat& A) { return A*x; }
#else
       DComplexTriDiagMat operator*(DComplex x, const DComplexTriDiagMat& A);
#endif
DComplexTriDiagMat operator/(const DComplexTriDiagMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexTriDiagMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexTriDiagMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexTriDiagMat transpose(const DComplexTriDiagMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexTriDiagMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexTriDiagMat& A);
  // inner products

DComplexTriDiagMat toTriDiagMat( const DComplexGenMat& A );
DComplexTriDiagMat toTriDiagMat( const DComplexBandMat& A );

DoubleTriDiagMat abs(const DComplexTriDiagMat& A);
DComplexTriDiagMat conj(const DComplexTriDiagMat& A);
DoubleTriDiagMat real(const DComplexTriDiagMat& A);
DoubleTriDiagMat imag(const DComplexTriDiagMat& A);
DoubleTriDiagMat norm(const DComplexTriDiagMat& A);
DoubleTriDiagMat arg(const DComplexTriDiagMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexTriDiagMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return ( (i-j>1) || (i-j<(-1)) ) ? DComplex(0,0) : vec(i+1+j*2);
#endif
}

inline DComplex DComplexTriDiagMat::set(int i, int j, DComplex x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i+1+j*2)=x;
#endif
}

inline RODComplexRef DComplexTriDiagMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODComplexRef DComplexTriDiagMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexTriDiagMat::operator()(int i, int j) const { return val(i,j); }

inline DComplexVec DComplexTriDiagMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? 1 + i*2 : 1 + iabs, n-iabs, 3 );
#endif
}

#endif

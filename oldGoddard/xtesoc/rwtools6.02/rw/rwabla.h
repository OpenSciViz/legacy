#ifndef __RWABLA_H__
#define __RWABLA_H__

/*
 * SChar precision Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwabla.h,v 1.1 1993/01/23 00:08:42 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwabla.h,v $
 * Revision 1.1  1993/01/23  00:08:42  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:12:42   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:48   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwacopy P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwaset  P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar));
int    FDecl rwasame P_((unsigned n, const SChar* restrict x, int incx, const SChar* restrict y, int incy));
int    FDecl rwamax P_((unsigned n, const SChar* restrict x, int incx));
int    FDecl rwamin P_((unsigned n, const SChar* restrict x, int incx));

void   FDecl rwadot P_((unsigned n, const SChar* restrict x, int incx, const SChar* restrict y, int incy, SChar* result));
void   FDecl rwacdot P_((unsigned n, const SChar* restrict x, int incx, const SChar* restrict y, int incy, SChar* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwa_aplvv P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_amivv P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_amuvv P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_advvv P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwa_aplvs P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar));
void   FDecl rwa_amuvs P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar));
void   FDecl rwa_advvs P_((unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwa_plvv P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_mivv P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_muvv P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy));
void   FDecl rwa_dvvv P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwa_plvs P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar));
void   FDecl rwa_muvs P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar));
void   FDecl rwa_dvvs P_((unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwa_misv P_((unsigned n, SChar* restrict z, const SChar* scalar, const SChar* restrict x, int incx));
void   FDecl rwa_dvsv P_((unsigned n, SChar* restrict z, const SChar* scalar, const SChar* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


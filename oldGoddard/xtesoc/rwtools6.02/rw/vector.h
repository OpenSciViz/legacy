#ifndef __RWVECTOR_H__
#define __RWVECTOR_H__
/* 
 * Base class for vector views.
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 */

#include "rw/dataview.h"

/*
 * This is the part of a vector common to all types of vectors.
 */

class RWVecView : public RWDataView {
protected:
  unsigned   npts;
  int        step;

  RWVecView();
  RWVecView(unsigned n, size_t s);
  RWVecView(RWBlock *block, unsigned n);
  RWVecView(const RWVecView& v);
  RWVecView(const RWDataView& v, void *b, unsigned l, int s) : RWDataView(v,b) {npts=l; step=s;}

  void      reference(const RWVecView&);

public:  // These functions need to be public so that they can be called by global functions
  void boundsCheck(int i)             const;  // Ensure 0<=i<npts
  void lengthCheck(unsigned  i)       const;  // Check that length of self is i
  void emptyErr(const char* fname)    const;  // Error: vector not empty
//static void  versionErr(int,int);           // Error: version number wrong in restoreFrom

public:  // These functions really are for public consumption
  unsigned   length()                 const   {return npts;}
  static int setFormatting(int);              // Change # items per line
  int        stride()                 const   {return step;}
};

#endif /* RW_VECTOR_H */


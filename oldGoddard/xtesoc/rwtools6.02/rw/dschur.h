#ifndef __RWDSCHUR_H__
#define __RWDSCHUR_H__
/*
 * DoubleSchurDecomp - Schur decomposition of a matrix
 *
 * Generated from template $Id: xschur.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * The Schur decomposition is often more useful than a full
 * eigendecomposition.  It provides the eigenvalues, and orthogonal
 * bases for invariant subspaces --- what more could you want?
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Schur decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/dvec.h"
#include "rw/cvec.h"
#include "rw/dgenmat.h"
#include "rw/dbal.h"
class DoubleHessenbergDecomp;

class DoubleSchurDecomp {
private:
  DoubleBalanceTransform B_;  // Balancing transformation
  DoubleGenMat           T_;  // Schur matrix
  DoubleGenMat           Z_;  // Schur vectors
  DComplexVec              w_;  // Eigenvalues

public:                                          
  DoubleSchurDecomp();
  DoubleSchurDecomp(const DoubleGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  DoubleSchurDecomp(const DoubleBalanceDecomp&);
  DoubleSchurDecomp(const DoubleHessenbergDecomp&);
  void factor(const DoubleGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const DoubleBalanceDecomp&);
  void factor(const DoubleHessenbergDecomp&);
  unsigned            cols()                  const {return T_.cols();}
  unsigned            rows()                  const {return T_.rows();}
  RWBoolean           good()                  const;// did this decomposition get computed properly
  DoubleGenMat           B()                     const;
  DoubleGenMat           Z()                     const;
  DoubleGenMat           T()                     const;
  DComplex                 eigenvalue(int)         const;
  DComplexVec              eigenvalues()           const;
  DoubleVec              Bx(const DoubleVec&)       const;
  DoubleVec              BInvTx(const DoubleVec&)   const;
  DoubleVec              Zx(const DoubleVec&)       const;
  DoubleVec              ZTx(const DoubleVec&)      const;
  DoubleGenMat           BX(const DoubleGenMat&)    const;
  DoubleGenMat           BInvTX(const DoubleGenMat&)const;
  DoubleGenMat           ZX(const DoubleGenMat&)    const;
  DoubleGenMat           ZTX(const DoubleGenMat&)   const;
  int                 move(int,int);              // Move an eigenvalue, returns where it actually ended up
  RWBoolean           moveToFront(const IntVec&); // Move the listed eigenvalues to the front (returns TRUE if all went well)
};

#endif

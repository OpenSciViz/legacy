#ifndef __RWSTRSTREA_H__
#define __RWSTRSTREA_H__

/*
 * Includes either strstream.h or strstrea.h, depending
 * on the compiler.
 *
 * $Id: strstrea.h,v 1.2 1993/10/04 21:44:59 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * This file exists because on the Microsoft NT compiler, trying
 * to include <strstream.h> fails.  For some stupid reason, the
 * NT compiler doesn't find strstrea.h, even though the headers are
 * stored on a fat file system.
 *
 ***************************************************************************
 *
 * $Log: strstrea.h,v $
 * Revision 1.2  1993/10/04  21:44:59  alv
 * ported to Windows NT
 *
 * Revision 1.1  1993/10/01  22:20:09  alv
 * Initial revision
 *
 */

#include "rw/defs.h"   /* Need to know about __WIN32__ */

#if defined(__WIN32__)
#include <strstrea.h>
#else
#include <strstream.h>
#endif

#endif /* __RWSTRSTREA_H__ */

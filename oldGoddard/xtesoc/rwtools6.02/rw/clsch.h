#ifndef __RWCLSCH_H__
#define __RWCLSCH_H__
/*
 * DComplexLeastSqCh:  solve least square Ch problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsch.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This least squares class only works for full rank problems.  For
 * more accurate, robust least squares classes, use the QR or SV
 * least squares factorizations.  This class has the advantage of
 * being the fastest of the three.
 */

#include "rw/cpdfct.h"
#include "rw/cgenmat.h"

class DComplexLeastSqCh {
private:
  DComplexGenMat A_;
  DComplexPDFact decomp_;   // Cholesky decomposition of the normal equations
public:                                          
  DComplexLeastSqCh();
  DComplexLeastSqCh(const DComplexGenMat& A);
  void      factor(const DComplexGenMat& A);
  RWBoolean good() const {return decomp_.good();}
  RWBoolean fail() const {return decomp_.fail();}
  unsigned  rows() const {return A_.rows();}
  unsigned  cols() const {return A_.cols();}
  unsigned  rank() const {return A_.cols();}
  DComplexVec    residual(const DComplexVec& data) const;
  Double       residualNorm(const DComplexVec& data) const;
  DComplexVec    solve(const DComplexVec& data) const;
};

inline DComplexVec solve(   const DComplexLeastSqCh& A, const DComplexVec& b) {return A.solve(b);}
inline DComplexVec residual(const DComplexLeastSqCh& A, const DComplexVec& b) {return A.residual(b);}
inline DComplex    residualNorm(const DComplexLeastSqCh& A, const DComplexVec& b) {return A.residualNorm(b);}

#endif

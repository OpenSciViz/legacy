#ifndef __RWDBAL_H__
#define __RWDBAL_H__

/*
 * DoubleBalanceTransform - combined permutation and diagonal balancing transformation
 * DoubleBalanceDecomp    - balance decomposition of a matrix
 *
 * Generated from template $Id: xbal.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This is the representation of a balancing transformation.  Sometimes you
 * can improve the quality of eigenvectors and eigenvalues by first scaling
 * the matrix so the rows and columns have approximately the same norm.  This
 * is done by the transformation encapsulated in these classes.
 *
 * The Transform class encapsulates just the transform itself.  The Decomp
 * class encapsulates a decomposition of a particular matrix.  The only way
 * to set up a Transform is via the Decomp class.  This is true because the
 * transform's only constructor (excepting the copy ctor) is private, and
 * Decomp is a friend class.
 */ 

#include "rw/lapkdefs.h"
#include "rw/dvec.h"
#include "rw/dgenmat.h"

class DoubleBalanceTransform {
friend class DoubleBalanceDecomp;
private:
  char    job_;      // Encodes whether this does scaling, permuting, or both
  long    ilo_,ihi_; // breakup of the matrix into disconnected pieces, if possible
  DoubleVec  scale_;    // details of the permutations and scaling factors
  void init(DoubleGenMat *A, RWBoolean permute, RWBoolean scale);
     // A is overwritten with the balanced matrix.
public:
  DoubleBalanceTransform();
  void         operator=(const DoubleBalanceTransform&);
  unsigned     cols()                        const {return scale_.length();}
  unsigned     rows()                        const {return scale_.length();}
  unsigned     lowIndex()                    const {return ilo_;}
  unsigned     highIndex()                   const {return ihi_;}
  DoubleGenMat    transform(const DoubleGenMat&)   const;// Compute BX
  DoubleGenMat    invTransform(const DoubleGenMat&)const;// Compute inv(B)'X
  DoubleVec       transform(const DoubleVec&)      const;// Compute BX
  DoubleVec       invTransform(const DoubleVec&)   const;// Compute inv(B)'X
};

class DoubleBalanceDecomp {
private:
  DoubleGenMat           C_;
  DoubleBalanceTransform B_;
    // C_ must be declared before B_ because B_ uses C_ in its constructor
public:
  DoubleBalanceDecomp();
  DoubleBalanceDecomp(const DoubleGenMat& A, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const DoubleGenMat& A, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  unsigned                   cols()          const {return C_.cols();}
  unsigned                   rows()          const {return C_.rows();}
  const DoubleGenMat            C()             const {return C_;}
  const DoubleBalanceTransform& B()             const {return B_;}
};

#endif

#ifndef __RWFREF_H__
#define __RWFREF_H__
/*
 * Generated from template $Id: xref.h,v 1.1 1993/06/23 18:28:11 alv Exp $
 *
 * Reference classes.
 *
 * Changed having a reference to "data" to using a pointer to "data"
 * because the bcc 3.0 compiler generates incorrect code sometimes for
 * classes containing a reference and no explicit copy constructor.
 * This is really too bad; the reference semantics make more sense here.
 * 
 * The operator<<() output routines were added because the 
 * Borland C++ compiler can't figure out enough to call the
 * conversion routine and then call the proper op<< routine.
 * For some bizarre reason it thinks there is a conflict with
 * converting the ref to a char*.  Hey, I don't pretend to understand.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/rstream.h"

class ROFloatRef {
  private:
    Float*        data;
    RWBoolean   readonly;
    Float         error(Float);   // Called when op=() invoked for a readonly
  public:
    ROFloatRef(Float& x, RWBoolean ro=FALSE) : data(&x) { readonly=ro; }
    ROFloatRef& operator=(Float x) { *data=(readonly?error(x):x); return *this; }
    operator Float()             { return *data; }
    friend ostream& operator<<(ostream& s,ROFloatRef& r) { return s<<*r.data; }
};

class NGFloatRef {
  private:
    Float*        data;
    RWBoolean   negate;
  public:
    NGFloatRef(Float& x, RWBoolean ng=FALSE) : data(&x) { negate=ng; }
    NGFloatRef& operator=(Float x)         { *data=(negate?-x:x); return *this; }
    operator Float()                     { return negate?-*data:*data; }
    friend ostream& operator<<(ostream& s,NGFloatRef& r) { return s<<(r.negate?-*r.data:*r.data); }
};

#endif

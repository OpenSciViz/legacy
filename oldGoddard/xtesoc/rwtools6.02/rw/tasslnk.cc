/*
 * Template definitions for RWTValAssocLink<K,V>
 *
 * $Id: tasslnk.cc,v 1.3 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tasslnk.cc,v $
 * Revision 1.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.2  1993/02/17  18:32:03  keffer
 * Now passes T's by const reference, rather than by value
 *
 * Revision 1.1  1993/01/28  21:11:49  keffer
 * Ported to cfront V3.0
 *
 *
 ***************************************************************************
 */

/*
 * Template-generated single-value constructor.  This can be overridden
 * by the user.
 */
template <class K, class V>
RWTValAssocLink<K,V>::RWTValAssocLink(const K& key) :
  key_(key)
{

}

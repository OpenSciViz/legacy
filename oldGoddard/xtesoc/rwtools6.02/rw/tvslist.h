#ifndef __RWTVSLIST_H__
#define __RWTVSLIST_H__

/*
 * RWTValSlist<T>: Singly-linked list of values of type T.
 *
 * $Id: tvslist.h,v 2.10 1993/11/08 21:14:01 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Stores a *copy* of the inserted item into the collection.
 *
 * Assumes that T has:
 *   - well-defined copy constructor (T::T(const T&) or equiv.);
 *   - well-defined assignment operator (T::operator=(const T&) or equiv.);
 *   - well-defined equality operator (T::operator==(const T&) or equiv.)
 *
 ***************************************************************************
 *
 * $Log: tvslist.h,v $
 * Revision 2.10  1993/11/08  21:14:01  jims
 * Port to ObjectStore
 *
 * Revision 2.9  1993/09/10  02:18:28  keffer
 * apply() now takes a function with a non-const reference, allowing
 * modification of the contained value.
 *
 * Revision 2.8  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.7  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.6  1993/02/17  18:32:03  keffer
 * Now passes T's by const reference, rather than by value
 *
 * Revision 2.5  1993/02/12  00:18:50  keffer
 * Ported to the IBM xlC compiler
 *
 * Revision 2.4  1993/01/27  21:39:46  keffer
 * Now uses separate definitions file
 *
 * Revision 2.3  1993/01/23  00:32:27  keffer
 * Performance enhancements; simplified; flatter inheritance tree.
 *
 *
 */

#include "rw/tislist.h"

template <class T> class RWExport RWTValSlistIterator;

/****************************************************************
 *								*
 *		Declarations for RWTValSlink<T>			*
 *								*
 ****************************************************************/

/*
 * This is the actual link that is stored in the linked list.
 * It includes data of type "T".
 */
template <class T> class RWExport RWTValSlink : public RWIsvSlink
{

public:

  RWTValSlink(const T& a) : info_(a) {;}
  RWTValSlink<T>* next() const	{return (RWTValSlink<T>*)next_;}

  T		info_;

};

/****************************************************************
 *								*
 *		Declarations for RWTValSlist<T>			*
 *								*
 ****************************************************************/

template <class T> class RWExport RWTValSlist 
                             : private RWTIsvSlist< RWTValSlink<T> >
{

public:

  RWTValSlist() {;}
  RWTValSlist(const RWTValSlist<T>&);
  ~RWTValSlist() {clear();}

  // Operators:
  RWTValSlist<T>&	operator=(const RWTValSlist<T>&);

  T&		operator[](size_t i)
	{return RWTIsvSlist<RWTValSlink<T> >::at(i)->info_;}
  T		operator[](size_t i) const
	{return RWTIsvSlist<RWTValSlink<T> >::at(i)->info_;}

  // Member functions:
  void		append(const T& a);

  void		apply(void (*applyFun)(T&, void*), void*);

  T&		at(size_t i)
	{return RWTIsvSlist<RWTValSlink<T> >::at(i)->info_;}
  T		at(size_t i) const
	{return RWTIsvSlist<RWTValSlink<T> >::at(i)->info_;}

  void		clear()
	{RWTIsvSlist<RWTValSlink<T> >::clearAndDestroy();}

  RWBoolean	contains(const T& a) const;
  RWBoolean	contains(RWBoolean (*testFun)(const T&, void*), void*) const;

  size_t	entries() const
	{return RWTIsvSlist<RWTValSlink<T> >::entries();}

  RWBoolean	find(const T& a, T& ret) const; // Find first; return in "ret"
  RWBoolean	find(RWBoolean (*testFun)(const T&, void*), void*, T& ret) const;

  T		first() const
	{return firstLink()->info_;}

  T		get()
	{return peel(RWTIsvSlist<RWTValSlink<T> >::get());}

  size_t	index(const T& a) const;
  size_t	index(RWBoolean (*testFun)(const T&, void*), void*) const;

  void		insert(const T& a);

  void		insertAt(size_t i, const T& a);

  RWBoolean	isEmpty() const
	{return RWTIsvSlist<RWTValSlink<T> >::isEmpty();}

  T		last() const
	{return lastLink()->info_;}

  size_t	occurrencesOf(const T& a) const;
  size_t	occurrencesOf(RWBoolean (*testFun)(const T&, void*), void*) const;

  void		prepend(const T& a);

  RWBoolean	remove(const T& val);
  RWBoolean	remove(RWBoolean (*testFun)(const T&, void*), void*);

  size_t	removeAll(const T& a);
  size_t	removeAll(RWBoolean (*testFun)(const T&, void*), void*);

  T		removeAt(size_t i)
	{return peel(RWTIsvSlist<RWTValSlink<T> >::removeAt(i));}

  T		removeFirst()
	{return peel(RWTIsvSlist<RWTValSlink<T> >::removeFirst());}

  T		removeLast()
	{return peel(RWTIsvSlist<RWTValSlink<T> >::removeLast());}

protected:

  RWTValSlink<T>*	findLeftVal(const T&) const;
  RWTValSlink<T>*	findLeftFun(RWBoolean (*testFun)(const T&, void*), void*) const;

  static T		peel(RWTValSlink<T>* link);

private:

  friend class RWExport RWTValSlistIterator<T> ;

};


/****************************************************************
 *								*
 *		Declarations for RWTValSlistIterator<T>		*
 *								*
 ****************************************************************/

template <class T> class RWExport RWTValSlistIterator :
                   private RWTIsvSlistIterator< RWTValSlink<T> >
{

public:

  RWTValSlistIterator(RWTValSlist<T>& s) :
	RWTIsvSlistIterator<RWTValSlink<T> >(s) {;}

  // Operators:
  RWBoolean	operator++()
	{advance(); return cursor()!=container()->tailLink();}
  RWBoolean	operator+=(size_t n)
	{return RWTIsvSlistIterator<RWTValSlink<T> >::operator+=(n)!=rwnil;}
  RWBoolean	operator()()
	{advance(); return cursor()!=container()->tailLink();}

  // Methods
  RWTValSlist<T>*	container() const
	{return (RWTValSlist<T>*)RWTIsvSlistIterator<RWTValSlink<T> >::container();}

  RWBoolean		findNext(const T& a);
  RWBoolean		findNext(RWBoolean (*testFun)(const T&, void*), void*);

  void			insertAfterPoint(const T& a);

  T			key() const
	{return cursor()->info_;}

  RWBoolean		remove();	// Remove item at cursor

  RWBoolean		removeNext(const T&);
  RWBoolean		removeNext(RWBoolean (*testFun)(const T&, void*), void*);

  void			reset()
	{RWTIsvSlistIterator<RWTValSlink<T> >::reset();}
  void			reset(RWTValSlist<T>& s)
	{RWTIsvSlistIterator<RWTValSlink<T> >::reset(s);}

private:

  // Disallow postfix increment.  Unless we hide it, some compilers will
  // substitute the prefix increment operator in its place.
  RWBoolean		operator++(int);
};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tvslist.cc"
#endif

#endif	/* __RWTVSLIST_H__ */

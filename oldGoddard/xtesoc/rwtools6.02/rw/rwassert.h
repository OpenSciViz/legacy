#ifndef __RWASSERT_H__
#define __RWASSERT_H__

/*
 * rwassert macro definition for testing purposes.
 *
 * $Id: rwassert.h,v 1.6 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwassert.h,v $
 * Revision 1.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/08/18  21:46:37  jims
 * Add macro to allow writing to cout instead of cerr
 *
 * Revision 1.4  1993/08/13  16:09:57  randall
 * moved '\' from back to front of print statement
 *
 * Revision 1.3  1993/02/07  00:27:28  keffer
 * Now includes rwAssertTest() directly in the header file as a static function.
 *
 * Revision 1.2  1993/02/06  21:34:47  keffer
 * Changed macro from ABORT_ON_FAIL to RW_ABORT_ON_FAIL
 *
 * Revision 1.1  1993/01/29  22:49:14  randall
 * Initial revision
 *
 */

#include "rw/rstream.h"

STARTWRAP
#include <assert.h>
ENDWRAP

/* Macro definition of rwassert(), which is used for testing.
 * If RW_ABORT_ON_FAIL is defined on the command line then the
 * system defined assert() is used which terminates the test 
 * program. Otherwise, RWAssertTest is used, which just reports
 * the error and keeps on going.
 *
 * Assertion errors are written to standard error (cerr) by default.
 * If RW_ASSERT_TO_COUT is defined, the errors will be written to
 * standard output (cout) instead.
 */
#ifdef RW_ABORT_ON_FAIL

# define rwassert(a) assert(a)

#else

# define rwassert(a) rwAssertTest(a, __FILE__, __LINE__)
 
  static int
  rwAssertTest(int result, const char* file, int line)
  {
    if (!result)
    {
#ifdef RW_ASSERT_TO_COUT
      cout
#else
      cerr
#endif
           << "\n**** Assert failure in file: " << file
           << "; line: " << line << " ****" << flush;
    }
    return result;
  }

#endif


#endif  /* RWASSERT_H__  */

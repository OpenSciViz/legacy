#ifndef __RWFTD_H__
#define __RWFTD_H__

/*
 * FloatSymTriDiagDecomp      - Abstract base class for tri-diagonal decompositions
 * FloatSymDenseTriDiagDecomp - Decomposition of a symmetric/Hermitian matrix
 * FloatSymBandTriDiagDecomp  - Decomposition of a symmetric/Hermitian banded matrix
 *
 * Generated from template $Id: xtd.h,v 1.2 1993/04/06 20:45:04 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents a decomposition of the form A=QTQ', where A is
 * symmetric/Hermitian, Q is orthogonal/unitary, and T is real tridiagonal.
 * This is usually done as a precursor to computing eigenvalues, since
 * the eigenvalues of A and T are the same, and those of T are easier to
 * compute.
 *
 * This class is used internally in the eigen-decomposition and
 * eigen-server classes.  The only reason a user should be interested
 * in this class is to implement his own routines to compute
 * eigenvalues/vectors of a tri-diagonal matrix as part of a new
 * eigen-server class.
 *
 * Each derived class must supply an implementation of the routine to
 * compute QC for a matrix C.  The routine to compute Qx for a vector x
 * by default calls the QC routine.
 *
 * The banded class can optionally not bother storing Q.  This can save
 * a lot of memory if the eigenvectors are not needed, or if you are
 * content to obtain the eigenvectors by inverse iteration later.
 */ 

#include "rw/lapkdefs.h"
#include "rw/fvec.h"
#include "rw/fgenmat.h"

class FloatSymMat;
class FloatSymBand;

class FloatSymTriDiagDecomp {
private:
  FloatVec diag;    // diagonal of T
  FloatVec offdiag; // sub/super diagonal of T
public:
  void set(const FloatVec& diag, const FloatVec& offdiag);
  virtual FloatVec transform(const FloatVec&) const;        // multiply by Q
  virtual FloatGenMat transform(const FloatGenMat&) const =0;  // multiply by Q
  const FloatVec diagonal()    const {return diag;}
  const FloatVec offDiagonal() const {return offdiag;}
  unsigned     rows()        const {return diag.length();}
  unsigned     cols()        const {return diag.length();}
};

class FloatSymDenseTriDiagDecomp : public FloatSymTriDiagDecomp {
private:
  FloatVec Qdata;   // Data necessary to generate Q (if keepQ==TRUE during construction)
  FloatVec tau;     // Scalar factors of the elementary reflectors
public:
  FloatSymDenseTriDiagDecomp(const FloatSymMat&);
  virtual FloatVec transform(const FloatVec&) const;        // multiply by Q
  virtual FloatGenMat transform(const FloatGenMat&) const;     // multiply by Q
};

class FloatSymBandTriDiagDecomp : public FloatSymTriDiagDecomp {
private:
  FloatGenMat Q;       // The matrix Q (if keepQ was TRUE during construction)
public:
  FloatSymBandTriDiagDecomp(const FloatSymBandMat&, RWBoolean keepQ=TRUE);
  virtual FloatGenMat transform(const FloatGenMat&) const;     // multiply by Q
  virtual FloatVec transform(const FloatVec&) const;        // multiply by Q
};
  
#endif

 

#ifndef __RWCLTRIMAT_H__
#define __RWCLTRIMAT_H__
/*
 * DComplexTriMat - A lower triangular matrix of DComplexs
 *
 * Stores a lower triangular matrix.
 *
 * The matrix is stored in row major order.  This way the vector
 * of data is exactly the same as the matrix's transpose (an UpperTriMat)
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleLowerTriMat;
class DComplexGenMat;
class DComplexUpperTriMat;

class DComplexLowerTriMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix


public:
DComplexLowerTriMat();
DComplexLowerTriMat( const DComplexLowerTriMat& );
DComplexLowerTriMat(unsigned n, unsigned nAgain);
DComplexLowerTriMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexLowerTriMat(const DoubleLowerTriMat& re);
DComplexLowerTriMat(const DoubleLowerTriMat& re, const DoubleLowerTriMat& im);
~DComplexLowerTriMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline RODComplexRef operator()(int i, int j);
inline RODComplexRef ref(int i, int j);
       RODComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexLowerTriMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexLowerTriMat& operator=(const DComplexLowerTriMat& m);
DComplexLowerTriMat& reference(DComplexLowerTriMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexLowerTriMat copy() const;
DComplexLowerTriMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexLowerTriMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexLowerTriMat& X);
RWBoolean operator!=(const DComplexLowerTriMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexLowerTriMat& operator+=(const DComplexLowerTriMat& m);
DComplexLowerTriMat& operator-=(const DComplexLowerTriMat& m);
DComplexLowerTriMat& operator*=(const DComplexLowerTriMat& m);
DComplexLowerTriMat& operator*=(DComplex);
DComplexLowerTriMat& operator/=(const DComplexLowerTriMat& m);
DComplexLowerTriMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexLowerTriMat operator-(const DComplexLowerTriMat&);	// Unary minus
DComplexLowerTriMat operator+(const DComplexLowerTriMat&);	// Unary plus
DComplexLowerTriMat operator*(const DComplexLowerTriMat&, const DComplexLowerTriMat&);
DComplexLowerTriMat operator/(const DComplexLowerTriMat&, const DComplexLowerTriMat&);
DComplexLowerTriMat operator+(const DComplexLowerTriMat&, const DComplexLowerTriMat&);
DComplexLowerTriMat operator-(const DComplexLowerTriMat&, const DComplexLowerTriMat&);
DComplexLowerTriMat operator*(const DComplexLowerTriMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexLowerTriMat operator*(DComplex x, const DComplexLowerTriMat& A) { return A*x; }
#else
       DComplexLowerTriMat operator*(DComplex x, const DComplexLowerTriMat& A);
#endif
DComplexLowerTriMat operator/(const DComplexLowerTriMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexLowerTriMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexLowerTriMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexUpperTriMat transpose(const DComplexLowerTriMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexLowerTriMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexLowerTriMat& A);
  // inner products

DComplexLowerTriMat toLowerTriMat( const DComplexGenMat& A );

DoubleLowerTriMat abs(const DComplexLowerTriMat& A);
DComplexLowerTriMat conj(const DComplexLowerTriMat& A);
DoubleLowerTriMat real(const DComplexLowerTriMat& A);
DoubleLowerTriMat imag(const DComplexLowerTriMat& A);
DoubleLowerTriMat norm(const DComplexLowerTriMat& A);
DoubleLowerTriMat arg(const DComplexLowerTriMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexLowerTriMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return (i<j) ? DComplex(0,0) : vec(i*(i+1)/2+j);
#endif
}

inline DComplex DComplexLowerTriMat::set(int i, int j, DComplex x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i*(i+1)/2+j)=x; 
#endif
}

inline RODComplexRef DComplexLowerTriMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODComplexRef DComplexLowerTriMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexLowerTriMat::operator()(int i, int j) const { return val(i,j); }


#endif

#ifndef __RWDCO_H__
#define __RWDCO_H__
/*
 * DoubleCODecomp: representation of a complete orthogonal decomposition
 *
 * Generated from template $Id: xco.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A complete orthogonal decomposition decomposes a matrix A like this:
 *
 *                    [ T 0 ]
 *            A P = Q [     ] Z'
 *                    [ 0 0 ]
 *
 * where P is a permutation matrix, Q and Z are orthogonal, and T is upper
 * triangular.
 *
 * Implementation details:
 * The Q and P parts of the decomposition are represented using a QRDecomp
 * object.  In order to save copying the R part, CODecomp is made a friend
 * or QRDecomp and we invalidate the R portion of the decomposition,
 * writing T and information needed to compute Z over top of it.  Note
 * that the TZ_ object refers to the same data as QRdecomp_.QR_.
 */

#include "rw/dqr.h"
#include "rw/dutrimat.h"

class DoubleCODecomp {
private:
  DoubleQRDecomp QRdecomp_;           // used to represent Q and P.  The R part of this is invalid.
  DoubleVec      Ztau_;               // scalars needed to recover Z
  DoubleGenMat   TZ_;                 // upper triangular part is T, rest is used to compute Z
  void        dofactor(double tol);// build Ztau_ and TZ_ from QRdecomp_

public:
  DoubleCODecomp();                      // Constructors
  DoubleCODecomp(const DoubleCODecomp&);
  DoubleCODecomp(const DoubleQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  DoubleCODecomp(const DoubleGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  void factor(const DoubleQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  void factor(const DoubleGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  ~DoubleCODecomp();
  void operator=(const DoubleCODecomp&);

  unsigned       rows() const         {return QRdecomp_.rows();}
  unsigned       cols() const         {return QRdecomp_.cols();}
  unsigned       rank() const         {return TZ_.rows();}
  DoubleGenMat      P() const            {return QRdecomp_.P();}
  DoubleUpperTriMat T() const;
  DoubleGenMat      Q() const            {return QRdecomp_.Q();}
  DoubleGenMat      Z() const;

  DoubleVec         Px(const DoubleVec& x) const       {return QRdecomp_.Px(x);}
  DoubleVec         PTx(const DoubleVec& x) const      {return QRdecomp_.PTx(x);}
  DoubleVec         Tx(const DoubleVec& x) const;
  DoubleVec         TTx(const DoubleVec& x) const;
  DoubleVec         Tinvx(const DoubleVec& x) const;
  DoubleVec         TTinvx(const DoubleVec& x) const;
  DoubleVec         Qx(const DoubleVec& x) const       {return QRdecomp_.Qx(x);}
  DoubleVec         QTx(const DoubleVec& x) const      {return QRdecomp_.QTx(x);}
  DoubleVec         Zx(const DoubleVec& x) const;
  DoubleVec         ZTx(const DoubleVec& x) const;
};

#endif

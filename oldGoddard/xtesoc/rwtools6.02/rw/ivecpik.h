#ifndef __RWIVECPIK_H__
#define __RWIVECPIK_H__

/*
 * Declarations for IntVecPick --- picks elements out of a IntVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The IntVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class IntVecPick {
private:
  IntVec		V;
  const IntVec		pick;
  IntVecPick(IntVec& v, const IntVec& x);	// Constructor is private
  friend		IntVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const IntVec&);
  void			operator=(const IntVecPick&);
  void			operator=(int);

  inline int&	operator()(int i);
  inline int		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
IntVecPick::IntVecPick(IntVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline int& IntVecPick::operator()(int i)       { return V( pick(i) ); }
inline int  IntVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__IVECPIK_H__*/

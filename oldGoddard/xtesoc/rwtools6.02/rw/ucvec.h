#ifndef __RWUCVEC_H__
#define __RWUCVEC_H__

/*
 * Declarations for UChar precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/rwslice.h"
#include "rw/vector.h"

class UCharVecPick;
class RWRand;

class UCharVec : public RWVecView {
private:
    UCharVec(const RWDataView&, UChar*, unsigned, int);// For constructing new views
    friend UCharVec toVec(const UCharArray&);
    friend class UCharGenMat;                   // For row,col,diagonal
    friend class UCharArray;                    // For conversion constructor

public:

    /****************
     * Constructors *
     ****************/

    UCharVec() : RWVecView()                    {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    UCharVec(unsigned n) : RWVecView(n,sizeof(UChar)) {}  
#endif
    UCharVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(UChar)) {}  
    UCharVec(unsigned n, RWRand&);
    UCharVec(unsigned n, UChar val);
    UCharVec(unsigned n, UChar val, UChar by);
    UCharVec(const char *);
    UCharVec(const UCharVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    UCharVec(const UCharVecPick& p);
    UCharVec(const UChar* dat, unsigned n);     // Copy of dat will be made
    UCharVec(RWBlock *block, unsigned n);       // Pass a custom block on the free store

    operator IntVec() const;                    // Convert to IntVec

    /********************
     * Member functions *
     ********************/

    UCharVec   apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    UCharVec   apply(XmathFunTy)     const;
#endif
    unsigned   binaryStoreSize()     const;     // Storage requirements
    UCharVec   copy()                const;     // Synonym for deepCopy()
    UChar*     data()                           {return (UChar*)begin;}
    const UChar* data()              const      {return (const UChar*)begin;}
    UCharVec   deepCopy()            const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    UCharVecPick pick(const IntVec& x);         // Return the "picked" elements
    const UCharVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    UCharVec&  reference(const UCharVec& v);    // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    UCharVec   slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    UChar&      operator[](int i);              // With bounds checking
    UChar       operator[](int i) const;        // With bounds checking
    UChar&      operator()(int i);              // With optional bounds checking
    UChar       operator()(int i)        const; // With optional bounds checking
    UCharVec    operator[](const RWSlice&);     // With bounds checking
    const UCharVec operator[](const RWSlice&) const;
    UCharVec    operator()(const RWSlice&);     // With optional bounds checking
    const UCharVec operator()(const RWSlice&) const;
    UCharVec&   operator=(const UCharVec& v);   // Must be same length as v
    UCharVec&   operator=(const UCharVecPick&);
    UCharVec&   operator=(UChar);
    RWBoolean   operator==(const UCharVec&) const;
    RWBoolean   operator!=(const UCharVec& v) const;
    UCharVec&   operator+=(UChar);
    UCharVec&   operator-=(UChar s);
    UCharVec&   operator*=(UChar);
    UCharVec&   operator/=(UChar s);
    UCharVec&   operator+=(const UCharVec&);
    UCharVec&   operator-=(const UCharVec&);
    UCharVec&   operator*=(const UCharVec&);
    UCharVec&   operator/=(const UCharVec&);
    UCharVec&   operator++();                   // Prefix operator
    UCharVec&   operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

    inline UCharVec    operator+(const UCharVec& v) { return v; }
           UCharVec    operator+(const UCharVec&, const UCharVec&);
           UCharVec    operator-(const UCharVec&, const UCharVec&);
           UCharVec    operator*(const UCharVec&, const UCharVec&);
           UCharVec    operator/(const UCharVec&, const UCharVec&);
           UCharVec    operator+(const UCharVec&,UChar);
           UCharVec    operator+(UChar s, const UCharVec& V);
           UCharVec    operator-(const UCharVec& V, UChar s);
           UCharVec    operator-(UChar, const UCharVec&);
           UCharVec    operator*(const UCharVec&,UChar);
           UCharVec    operator*(UChar s, const UCharVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline UChar       operator%(const UCharVec& x, const UCharVec& y) {return dot(x,y);}
           UCharVec    operator/(const UCharVec& V, UChar s);
           UCharVec    operator/(UChar, const UCharVec&);
           ostream&    operator<<(ostream& s, const UCharVec& v);
           istream&    operator>>(istream& s, UCharVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline UCharVec    operator*(UChar s, const UCharVec& V) {return V*s;}
    inline UCharVec    operator+(UChar s, const UCharVec& V) {return V+s;}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

                                                  // No       abs() for UCharVec
           UCharVec    cumsum(const UCharVec&);
           UChar       dot(const UCharVec&,const UCharVec&);
           int         maxIndex(const UCharVec&); // Returns *index* of max value
           UChar       maxValue(const UCharVec&);
           int         minIndex(const UCharVec&); // Returns *index* of min value
           UChar       minValue(const UCharVec&);
           UChar       prod(const UCharVec&);
           UCharVec    reverse(const UCharVec&);
           UChar       sum(const UCharVec&);


    /**** Functions for type conversion ****/

           /* No type conversion functions for UCharVec */


    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline UCharVec::UCharVec(const RWDataView& v, UChar* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline UChar& UCharVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline UChar UCharVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline UChar& UCharVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline UChar UCharVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline UCharVec UCharVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return UCharVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const UCharVec UCharVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return UCharVec(*this, (UChar*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline UCharVec UCharVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return UCharVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const UCharVec UCharVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return UCharVec(*this, (UChar*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline UChar         operator%(const UCharVec& x, const UCharVec& y) {return dot(x,y);}

#endif /* __RWUCVEC_H__ */

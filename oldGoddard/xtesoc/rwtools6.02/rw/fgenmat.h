#ifndef __RWFGENMAT_H__
#define __RWFGENMAT_H__

/*
 * Declarations for Float precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/matrix.h"
#include "rw/fvec.h"

class FloatGenMatPick;
class RWRand;
class FloatSymMat;              // So that the conversion constructors 
class FloatSkewMat;             // to matrix.h++ classes parse successfully
class FloatBandMat;
class FloatSymBandMat;
class FloatUpperTriMat;
class FloatLowerTriMat;
class FloatTriDiagMat;

class FloatGenMat : public RWMatView {
private:
    FloatGenMat(const RWDataView&, float*, unsigned,unsigned, int,int); // For internal use
    friend FloatGenMat toGenMat(const FloatArray&);
    friend class FloatArray;                    // For conversion constructor

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    FloatVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    FloatGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    FloatGenMat() : RWMatView()                 {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    FloatGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(float),s) {}
#endif
    FloatGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(float),s) {}
    FloatGenMat(unsigned m, unsigned n, RWRand&);
    FloatGenMat(unsigned m, unsigned n, float val);
    FloatGenMat(const char *);
    FloatGenMat(const FloatGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    FloatGenMat(const FloatGenMatPick& p);
    FloatGenMat(const float* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    FloatGenMat(const FloatVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    FloatGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data

    FloatGenMat(const FloatSymMat& A);          // Conversion to the matrix
    FloatGenMat(const FloatSkewMat& A);         // types in matrix.h++.
    FloatGenMat(const FloatBandMat& A);
    FloatGenMat(const FloatSymBandMat& A);
    FloatGenMat(const FloatUpperTriMat& A);
    FloatGenMat(const FloatLowerTriMat& A);
    FloatGenMat(const FloatTriDiagMat& A);


    operator DoubleGenMat() const;              // Conversion to DoubleGenMat

    /********************
     * Member functions *
     ********************/

    FloatGenMat apply(mathFunTy)     const;
#ifdef RW_NATIVE_EXTENDED
    FloatGenMat apply(XmathFunTy)    const;
#endif
    float&     bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, float x);    // set() with bounds checking: for matrix.h++ compatability
    float      bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const FloatVec col(int)          const;     // Returns a view of a column
    FloatVec   col(int);                        // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    FloatGenMat copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    float*     data()                           {return (float*)begin;}
    const float* data()              const      {return (float*)begin;}
    FloatGenMat deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const FloatVec diagonal(int =0)  const;     // Returns a view of a diagonal
    FloatVec   diagonal(int =0);                // Returns a view of a diagonal
    FloatGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const FloatGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    float&     ref(int i, int j);               // for matrix.h++ compatability
    FloatGenMat& reference(const FloatGenMat& v); // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const FloatVec row(int)          const;     // Returns a view of a row
    FloatVec   row(int);                        // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, float x);      // for matrix.h++ compatability 
    FloatVec   slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    FloatGenMat slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    float      val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=(float)0;}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    float&               operator()(int,int);           
    float                operator()(int,int) const;
    FloatVec             operator()(int, const RWSlice&);
    const FloatVec       operator()(int, const RWSlice&)            const;
    FloatVec             operator()(const RWSlice&, int);
    const FloatVec       operator()(const RWSlice&, int)            const;
    FloatGenMat          operator()(const RWSlice&, const RWSlice&);
    const FloatGenMat    operator()(const RWSlice&, const RWSlice&) const;
    FloatGenMat& operator=(const FloatGenMat& v); // Must be same size as v
    FloatGenMat& operator=(const FloatGenMatPick&);
    FloatGenMat& operator=(float);
    RWBoolean   operator==(const FloatGenMat&) const;
    RWBoolean   operator!=(const FloatGenMat& v) const;
    FloatGenMat& operator+=(float);
    FloatGenMat& operator-=(float s)            { return operator+=(-s); }
    FloatGenMat& operator*=(float);
    FloatGenMat& operator/=(float s)            { return operator*=(1.0/s); }
    FloatGenMat& operator+=(const FloatGenMat&);
    FloatGenMat& operator-=(const FloatGenMat&);
    FloatGenMat& operator*=(const FloatGenMat&);
    FloatGenMat& operator/=(const FloatGenMat&);
    FloatGenMat& operator++();                  // Prefix operator
    FloatGenMat& operator--();                  // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           FloatGenMat operator-(const FloatGenMat&);
    inline FloatGenMat operator+(const FloatGenMat& v) { return v; }
           FloatGenMat operator+(const FloatGenMat&, const FloatGenMat&);
           FloatGenMat operator-(const FloatGenMat&, const FloatGenMat&);
           FloatGenMat operator*(const FloatGenMat&, const FloatGenMat&);
           FloatGenMat operator/(const FloatGenMat&, const FloatGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline FloatGenMat operator%(const FloatGenMat& A, const FloatGenMat& B) {return product(A,B);}
//  inline FloatVec    operator%(const FloatGenMat& A, const FloatVec& x) {return product(A,x);}
//  inline FloatVec    operator%(const FloatVec& x, const FloatGenMat& A) {return product(x,A);}
           FloatGenMat operator+(const FloatGenMat&,float);
           FloatGenMat operator+(float, const FloatGenMat&);
           FloatGenMat operator-(const FloatGenMat&,float);
           FloatGenMat operator-(float, const FloatGenMat&);
           FloatGenMat operator*(const FloatGenMat&,float);
           FloatGenMat operator*(float, const FloatGenMat&);
           FloatGenMat operator/(const FloatGenMat&,float);
           FloatGenMat operator/(float, const FloatGenMat&);
           ostream&    operator<<(ostream& s, const FloatGenMat& v);
           istream&    operator>>(istream& s, FloatGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline FloatGenMat operator*(float s, const FloatGenMat& V) {return V*s;}
    inline FloatGenMat operator+(float s, const FloatGenMat& V) {return V+s;}
    inline FloatGenMat operator-(const FloatGenMat& V, float s) {return V+(float)(-s);}// cast makes microsoft happy when compiling sarr.cpp
    inline FloatGenMat operator/(const FloatGenMat& V, float s) {return V*(1/s);}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           FloatGenMat abs(const FloatGenMat& V);
           float       dot(const FloatGenMat&,const FloatGenMat&); // Return sum_ij (Aij*Bij)
           void        maxIndex(const FloatGenMat&, int *i, int *j);
           void        minIndex(const FloatGenMat&, int *i, int *j);
           float       maxValue(const FloatGenMat&);
           float       minValue(const FloatGenMat&);
           float       prod(const FloatGenMat&);
           FloatVec    product(const FloatGenMat&, const FloatVec&);
           FloatVec    product(const FloatVec&, const FloatGenMat&);
           FloatGenMat product(const FloatGenMat&, const FloatGenMat&);
           float       sum(const FloatGenMat&);
           FloatGenMat transpose(const FloatGenMat&); // Transpose references this's data
    inline FloatGenMat adjoint(const FloatGenMat& A) {return transpose(A);}
           FloatGenMat transposeProduct(const FloatGenMat&,const FloatGenMat&); // For matrix.h++ compatability



    /**** Functions for both real and complex ****/

           FloatGenMat cos(const FloatGenMat& V);
           FloatGenMat cosh(const FloatGenMat& V);
           FloatGenMat exp(const FloatGenMat& V);
           float       mean(const FloatGenMat& V);
           FloatGenMat pow(const FloatGenMat&,const FloatGenMat&);
           FloatGenMat sin(const FloatGenMat& V);
           FloatGenMat sinh(const FloatGenMat& V);
           FloatGenMat sqrt(const FloatGenMat& V);
           float       variance(const FloatGenMat&);


    /**** Functions for real matrices ****/

           FloatGenMat acos(const FloatGenMat& V);
           FloatGenMat asin(const FloatGenMat& V);
           FloatGenMat atan(const FloatGenMat& V);
           FloatGenMat atan2(const FloatGenMat&,const FloatGenMat&);
           FloatGenMat ceil(const FloatGenMat& V);
           FloatGenMat floor(const FloatGenMat& V);
           FloatGenMat log(const FloatGenMat& V);
           FloatGenMat log10(const FloatGenMat& V);
           FloatGenMat tan(const FloatGenMat& V);
           FloatGenMat tanh(const FloatGenMat& V);


    /**** Functions for type conversion ****/

           FloatGenMat toFloat(const DoubleGenMat&);

    /**** Norm functions ****/
           Float       l1Norm(const FloatGenMat&); // Largest column sum
           Float       linfNorm(const FloatGenMat&); // Largest row sum
           Float       frobNorm(const FloatGenMat&); // Root of sum of squares
           Float       maxNorm(const FloatGenMat&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline float& FloatGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline float FloatGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline FloatVec FloatGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const FloatVec FloatGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline FloatVec FloatGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const FloatVec FloatGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline FloatGenMat FloatGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const FloatGenMat FloatGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const FloatVec FloatGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       FloatVec FloatGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const FloatVec FloatGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       FloatVec FloatGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline float& FloatGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void FloatGenMat::set(int i, int j, float x) {
    (*this)(i,j)=x;
  }

  inline float FloatGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline float& FloatGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void FloatGenMat::bcset(int i, int j, float x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline float FloatGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline FloatGenMat         operator%(const FloatGenMat& A, const FloatGenMat& B) {return product(A,B);}
inline FloatVec      operator%(const FloatGenMat& A, const FloatVec& x) {return product(A,x);}
inline FloatVec      operator%(const FloatVec& x, const FloatGenMat& A) {return product(x,A);}

#endif /* __RWFGENMAT_H__ */

#ifndef __RWTSTACK_H__
#define __RWTSTACK_H__

/*
 * Parameterized stack of T's, implemented using class C.
 *
 * $Id: tstack.h,v 2.4 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * WARNING: This class is very slow when used with singly-linked lists
 *          because they use an inefficient implementation for removeLast().
 *
 ***************************************************************************
 *
 * $Log: tstack.h,v $
 * Revision 2.4  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.2  1993/02/06  22:50:20  keffer
 * Ported to cfront V3.0
 *
 *    Rev 1.0   02 Mar 1992 16:10:52   KEFFER
 * Initial revision.
 */

#ifndef __RWDEFS_H__
#  include "rw/defs.h"
#endif

#ifdef RW_BROKEN_TEMPLATES

template <class T, class C> class RWExport RWTStack
{

public:

  void		clear()		{container_.clear();}
  size_t	entries() const	{return container_.entries();}
  RWBoolean	isEmpty() const	{return container_.isEmpty();}
  void		push(T a)	{container_.append(a);}
  T		pop()		{return container_.removeLast();}
  T		top() const	{return container_.last();}

protected:

  C		container_;

};

#else	/* !RW_BROKEN_TEMPLATES */

template <class T, class C> class RWExport RWTStack : private C
{

public:

  C::clear;
  C::entries;
  C::isEmpty;
  void		push(T a)	{C::append(a);}
  T		pop()		{return C::removeLast();}
  T		top() const	{return C::last();}

};

#endif	/* RW_BROKEN_TEMPLATES */

#endif	/* __RWTSTACK_H__ */

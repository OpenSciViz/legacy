#ifndef  __RWGQUEUE_H__
#define  __RWGQUEUE_H__

/*
 * Declarations for General Queues, implemented as a singly-linked list.
 *
 * $Id: gqueue.h,v 2.6 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: gqueue.h,v $
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/08/03  21:10:38  dealys
 * Ported to MPW C++ 3.3 - RW_BROKEN_TOKEN_PASTE
 *
 * Revision 2.4  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.3  1993/03/25  03:50:41  keffer
 * Added prefix RW to class name
 *
 * Revision 2.2  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   18 Feb 1992 19:23:00   KEFFER
 * Now includes "rw/generic.h".
 * Class tag is now RWExport instead of huge.
 * 
 *    Rev 1.2   28 Oct 1991 09:08:14   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.1   09 Oct 1991 18:34:32   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.0   28 Jul 1991 08:11:20   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/slist.h"
#include "rw/generic.h"

#ifdef RW_BROKEN_TOKEN_PASTE
#  define RWGQueue(type) type##RWGQueue   
#else
#  define RWGQueue(type) name2(type,RWGQueue)
#endif

#define RWGQueuedeclare(type)							\
class RWExport RWGQueue(type) : private RWSlist {				\
public:										\
  RWGQueue(type)() { }								\
  RWGQueue(type)(type* a) : RWSlist(a) { }					\
  RWGQueue(type)(const RWGQueue(type)& q) : RWSlist(q) { }			\
  void operator=(const RWGQueue(type)& q) { RWSlist::operator=(q);}		\
										\
  type* append(type* a)		{return (type*)RWSlist::append(a);}		\
  void clear() {RWSlist::clear();}						\
  RWBoolean contains(RWBoolean (*t)(const type*, const void*), const void* a) const	\
    {return RWSlist::contains((RWtestGeneric)t, a);}				\
  RWBoolean containsReference(const type* a) const				\
    {return RWSlist::containsReference(a);}					\
  size_t entries() const	{return RWSlist::entries();}			\
  type* first() const		{return (type*)RWSlist::first(); }		\
  type* get()			{return (type*)RWSlist::get(); }		\
  type* insert(type* a)		{return (type*)RWSlist::append(a);}		\
  RWBoolean isEmpty() const	{return RWSlist::isEmpty();}			\
  type* last() const		{return (type*)RWSlist::last(); }		\
  size_t occurrencesOf(RWBoolean (*t)(const type*, const void*), const void* a) const\
    {return RWSlist::occurrencesOf((RWtestGeneric)t, a);}			\
  size_t occurrencesOfReference(const type* a) const				\
    {return RWSlist::occurrencesOfReference(a);}				\
};                                                              
                                                                
#endif /* __RWGQUEUE_H__ */

/*
 * Template definitions for RWTIsvDlist<TL> and RWTIsvDlistIterator<TL>
 *
 * $Id: tidlist.cc,v 1.4 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tidlist.cc,v $
 * Revision 1.4  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.2  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.1  1993/01/27  21:38:32  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */

template <class TL> void
RWTIsvDlist<TL>::apply(void (*applyFun)(TL*, void*), void* p)
{
  TL* link = firstLink();
  while (link != tailLink())
  {
    (*applyFun)(link, p);	// Apply the function
    link = (TL*)link->next();	// Advance
  }
}
template <class TL> void
RWTIsvDlist<TL>::clearAndDestroy()
{
  TL* n;
  TL* link = firstLink();
  while (link != tailLink())
  {
    n = (TL*)link->next();
    delete link;
    link = n;
  }
  init();
}
  
template <class TL> TL*
RWTIsvDlist<TL>::find(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      return link;
    link = (TL*)link->next();
  }
  return rwnil;
}


template <class TL> size_t
RWTIsvDlist<TL>::index(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  size_t count = 0;
  const TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      return count;
    link = (const TL*)link->next();
    ++count;
  }
  return RW_NPOS;
}

template <class TL> size_t
RWTIsvDlist<TL>::occurrencesOf(RWBoolean (*testFun)(const TL*, void*), void* d) const
{
  size_t count = 0;
  const TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      ++count;
    link = (const TL*)link->next();
  }
  return count;
}

template <class TL> TL*
RWTIsvDlist<TL>::remove(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  TL* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link, d))
      return removeReference(link);
    link = (TL*)link->next();
  }
  return rwnil;
}

/****************************************************************
 *								*
 *	Definitions for RWTIsvDlistIterator<TL>			*
 *								*
 ****************************************************************/


/*
 * Return first occurrence where the tester returns true.
 */
template <class TL> TL*
RWTIsvDlistIterator<TL>::findNext(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  TL* p;
  while ( (p= (*this)()) != 0 )
  {
    if (testFun(p, d))
      return p;
  }
  return rwnil;
}

/*
 * Remove and return first occurrence where the tester returns true.
 * In a doubly-linked list, we are unencumbered by having to remember
 * the previous link.
 */
template <class TL> TL*
RWTIsvDlistIterator<TL>::removeNext(RWBoolean (*testFun)(const TL*, void*), void* d)
{
  return findNext(testFun, d) ? remove() : rwnil;
}


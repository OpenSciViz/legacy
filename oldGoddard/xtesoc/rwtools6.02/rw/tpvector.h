#ifndef __RWTPVECTOR_H__
#define __RWTPVECTOR_H__

/*
 * RWTPtrVector: Parameterized vector of pointers to T
 *
 * $Id: tpvector.h,v 2.8 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Stores a *pointer* to the item in the vector.  Basically, a typesafe
 * interface to RWPtrVector.
 *
 ***************************************************************************
 *
 * $Log: tpvector.h,v $
 * Revision 2.8  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.7  1993/08/03  00:44:15  keffer
 * Changed return type of data().
 *
 * Revision 2.6  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.5  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.4  1993/02/17  18:27:29  keffer
 * Now based on class RWPtrVector.
 *
 * Revision 2.3  1993/02/11  02:37:29  keffer
 * HP compiler does not understand access adjustment with templates.
 *
 * Revision 2.2  1993/01/28  02:39:24  keffer
 * Ported to cfront V3.0
 *
 *    Rev 1.0   19 Mar 1992 10:33:10   KEFFER
 * Initial revision.
 */

#ifndef __RWPVECTOR_H__
# include "rw/pvector.h"
#endif

template<class T> class RWExport RWTPtrVector : private RWPtrVector
{

public:

  RWTPtrVector() {;}
  RWTPtrVector(size_t n) : RWPtrVector(n)	{;}
  RWTPtrVector(size_t n, T* p) : RWPtrVector(n, p) {;}

  RWTPtrVector<T>&	operator=(const RWTPtrVector<T>& v)
	{return (RWTPtrVector<T>&)RWPtrVector::operator=(v);}
  RWTPtrVector<T>&	operator=(T* p)
	{return (RWTPtrVector<T>&)RWPtrVector::operator=(p);}

  T*& 		operator()(size_t n)       {return (T*&)RWPtrVector::operator()(n);}
  T* 		operator()(size_t n) const {return (T*) RWPtrVector::operator()(n);}
  T*& 		operator[](size_t n)       {return (T*&)RWPtrVector::operator[](n);}
  T* 		operator[](size_t n) const {return (T*) RWPtrVector::operator[](n);}

  T* const *	data() const		{return (T* const *)RWPtrVector::data();}
  size_t	length() const		{return RWPtrVector::length();          }
  void		reshape(size_t N)	{RWPtrVector::reshape(N);               }
  void		resize(size_t N)	{RWPtrVector::resize(N);                }

};

#endif /* __RWTPVECTOR_H__ */

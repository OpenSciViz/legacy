#ifndef __RWIVEC_H__
#define __RWIVEC_H__

/*
 * Declarations for Int precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/rwslice.h"
#include "rw/vector.h"

class IntVecPick;
class RWRand;

class IntVec : public RWVecView {
private:
    IntVec(const RWDataView&, int*, unsigned, int);// For constructing new views
    friend IntVec toVec(const IntArray&);
    friend class IntGenMat;                     // For row,col,diagonal
    friend class IntArray;                      // For conversion constructor

public:

    /****************
     * Constructors *
     ****************/

    IntVec() : RWVecView()                      {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    IntVec(unsigned n) : RWVecView(n,sizeof(Int)) {}  
#endif
    IntVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(Int)) {}  
    IntVec(unsigned n, RWRand&);
    IntVec(unsigned n, int val);
    IntVec(unsigned n, int val, int by);
    IntVec(const char *);
    IntVec(const IntVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    IntVec(const IntVecPick& p);
    IntVec(const int* dat, unsigned n);         // Copy of dat will be made
    IntVec(RWBlock *block, unsigned n);         // Pass a custom block on the free store

    operator DoubleVec() const;                 // Conversion to DoubleVec

    /********************
     * Member functions *
     ********************/

    IntVec     apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    IntVec     apply(XmathFunTy)     const;
#endif
    unsigned   binaryStoreSize()     const;     // Storage requirements
    IntVec     copy()                const;     // Synonym for deepCopy()
    int*       data()                           {return (int*)begin;}
    const int* data()                const      {return (const int*)begin;}
    IntVec     deepCopy()            const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    IntVecPick pick(const IntVec& x);           // Return the "picked" elements
    const IntVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    IntVec&    reference(const IntVec& v);      // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    IntVec     slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    int&        operator[](int i);              // With bounds checking
    int         operator[](int i) const;        // With bounds checking
    int&        operator()(int i);              // With optional bounds checking
    int         operator()(int i)        const; // With optional bounds checking
    IntVec      operator[](const RWSlice&);     // With bounds checking
    const IntVec operator[](const RWSlice&) const;
    IntVec      operator()(const RWSlice&);     // With optional bounds checking
    const IntVec operator()(const RWSlice&) const;
    IntVec&     operator=(const IntVec& v);     // Must be same length as v
    IntVec&     operator=(const IntVecPick&);
    IntVec&     operator=(int);
    RWBoolean   operator==(const IntVec&) const;
    RWBoolean   operator!=(const IntVec& v) const;
    IntVec&     operator+=(int);
    IntVec&     operator-=(int s)               { return operator+=(-s); }
    IntVec&     operator*=(int);
    IntVec&     operator/=(int s);
    IntVec&     operator+=(const IntVec&);
    IntVec&     operator-=(const IntVec&);
    IntVec&     operator*=(const IntVec&);
    IntVec&     operator/=(const IntVec&);
    IntVec&     operator++();                   // Prefix operator
    IntVec&     operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           IntVec      operator-(const IntVec&);
    inline IntVec      operator+(const IntVec& v) { return v; }
           IntVec      operator+(const IntVec&, const IntVec&);
           IntVec      operator-(const IntVec&, const IntVec&);
           IntVec      operator*(const IntVec&, const IntVec&);
           IntVec      operator/(const IntVec&, const IntVec&);
           IntVec      operator+(const IntVec&,int);
           IntVec      operator+(int s, const IntVec& V);
           IntVec      operator-(const IntVec& V, int s);
           IntVec      operator-(int, const IntVec&);
           IntVec      operator*(const IntVec&,int);
           IntVec      operator*(int s, const IntVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline Int         operator%(const IntVec& x, const IntVec& y) {return dot(x,y);}
           IntVec      operator/(const IntVec& V, int s);
           IntVec      operator/(int, const IntVec&);
           ostream&    operator<<(ostream& s, const IntVec& v);
           istream&    operator>>(istream& s, IntVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline IntVec      operator*(int s, const IntVec& V) {return V*s;}
    inline IntVec      operator+(int s, const IntVec& V) {return V+s;}
    inline IntVec      operator-(const IntVec& V, int s) {return V+(int)(-s);} // cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           IntVec      abs(const IntVec& V);
           IntVec      cumsum(const IntVec&);
           IntVec      delta(const IntVec&);
           int         dot(const IntVec&,const IntVec&);
           int         maxIndex(const IntVec&);   // Returns *index* of max value
           int         maxValue(const IntVec&);
           int         minIndex(const IntVec&);   // Returns *index* of min value
           int         minValue(const IntVec&);
           int         prod(const IntVec&);
           IntVec      reverse(const IntVec&);
           int         sum(const IntVec&);


    /**** Functions for type conversion ****/

           IntVec      toInt(const DoubleVec&);
           IntVec      toInt(const FloatVec&);


    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline IntVec::IntVec(const RWDataView& v, int* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline int& IntVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline int IntVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline int& IntVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline int IntVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline IntVec IntVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return IntVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const IntVec IntVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return IntVec(*this, (int*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline IntVec IntVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return IntVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const IntVec IntVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return IntVec(*this, (int*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline Int         operator%(const IntVec& x, const IntVec& y) {return dot(x,y);}

#endif /* __RWIVEC_H__ */

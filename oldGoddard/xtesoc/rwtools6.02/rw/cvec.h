#ifndef __RWCVEC_H__
#define __RWCVEC_H__

/*
 * Declarations for DComplex precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/dvec.h"
#include "rw/dcomplex.h"
#include "rw/rwslice.h"
#include "rw/vector.h"

class DComplexVecPick;
class RWRand;

class DComplexVec : public RWVecView {
private:
    DComplexVec(const RWDataView&, DComplex*, unsigned, int);// For constructing new views
    friend DComplexVec toVec(const DComplexArray&);
    friend class DComplexGenMat;                // For row,col,diagonal
    friend class DComplexArray;                 // For conversion constructor

public:

    /****************
     * Constructors *
     ****************/

    DComplexVec() : RWVecView()                 {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    DComplexVec(unsigned n) : RWVecView(n,sizeof(DComplex)) {}  
#endif
    DComplexVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(DComplex)) {}  
    DComplexVec(unsigned n, RWRand&);
    DComplexVec(unsigned n, DComplex val);
    DComplexVec(unsigned n, DComplex val, DComplex by);
    DComplexVec(const char *);
    DComplexVec(const DComplexVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    DComplexVec(const DComplexVecPick& p);
    DComplexVec(const DComplex* dat, unsigned n); // Copy of dat will be made
    DComplexVec(RWBlock *block, unsigned n);    // Pass a custom block on the free store
    DComplexVec(const DoubleVec& re);           // Conversion from DoubleVec
    DComplexVec(const DoubleVec& re, const DoubleVec& im);


    /********************
     * Member functions *
     ********************/

    DComplexVec apply(CmathFunTy)    const;
    DoubleVec  apply2(CmathFunTy2)   const;
    unsigned   binaryStoreSize()     const;     // Storage requirements
    DComplexVec copy()               const;     // Synonym for deepCopy()
    DComplex*  data()                           {return (DComplex*)begin;}
    const DComplex* data()           const      {return (const DComplex*)begin;}
    DComplexVec deepCopy()           const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    DComplexVecPick pick(const IntVec& x);      // Return the "picked" elements
    const DComplexVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    DComplexVec& reference(const DComplexVec& v); // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    DComplexVec slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    DComplex&   operator[](int i);              // With bounds checking
    DComplex    operator[](int i) const;        // With bounds checking
    DComplex&   operator()(int i);              // With optional bounds checking
    DComplex    operator()(int i)        const; // With optional bounds checking
    DComplexVec operator[](const RWSlice&);     // With bounds checking
    const DComplexVec operator[](const RWSlice&) const;
    DComplexVec operator()(const RWSlice&);     // With optional bounds checking
    const DComplexVec operator()(const RWSlice&) const;
    DComplexVec& operator=(const DComplexVec& v); // Must be same length as v
    DComplexVec& operator=(const DComplexVecPick&);
    DComplexVec& operator=(DComplex);
    RWBoolean   operator==(const DComplexVec&) const;
    RWBoolean   operator!=(const DComplexVec& v) const;
    DComplexVec& operator+=(DComplex);
    DComplexVec& operator-=(DComplex s)         { return operator+=(-s); }
    DComplexVec& operator*=(DComplex);
    DComplexVec& operator/=(DComplex s);
    DComplexVec& operator+=(const DComplexVec&);
    DComplexVec& operator-=(const DComplexVec&);
    DComplexVec& operator*=(const DComplexVec&);
    DComplexVec& operator/=(const DComplexVec&);

    friend DoubleVec real(const DComplexVec&);    // real and imag need access to the
    friend DoubleVec imag(const DComplexVec&);    // RWBlock, so they must be friends
};
  
    /********************
     * Global Operators *
     ********************/

           DComplexVec operator-(const DComplexVec&);
    inline DComplexVec operator+(const DComplexVec& v) { return v; }
           DComplexVec operator+(const DComplexVec&, const DComplexVec&);
           DComplexVec operator-(const DComplexVec&, const DComplexVec&);
           DComplexVec operator*(const DComplexVec&, const DComplexVec&);
           DComplexVec operator/(const DComplexVec&, const DComplexVec&);
           DComplexVec operator+(const DComplexVec&,DComplex);
           DComplexVec operator+(DComplex s, const DComplexVec& V);
           DComplexVec operator-(const DComplexVec& V, DComplex s);
           DComplexVec operator-(DComplex, const DComplexVec&);
           DComplexVec operator*(const DComplexVec&,DComplex);
           DComplexVec operator*(DComplex s, const DComplexVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline DComplex    operator%(const DComplexVec& x, const DComplexVec& y) {return dot(x,y);}
           DComplexVec operator/(const DComplexVec& V, DComplex s);
           DComplexVec operator/(DComplex, const DComplexVec&);
           ostream&    operator<<(ostream& s, const DComplexVec& v);
           istream&    operator>>(istream& s, DComplexVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DComplexVec operator*(DComplex s, const DComplexVec& V) {return V*s;}
    inline DComplexVec operator+(DComplex s, const DComplexVec& V) {return V+s;}
    inline DComplexVec operator-(const DComplexVec& V, DComplex s) {return V+(DComplex)(-s);} // cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleVec   abs(const DComplexVec& V);
           DComplexVec cumsum(const DComplexVec&);
           DComplexVec delta(const DComplexVec&);
           DComplex    dot(const DComplexVec&,const DComplexVec&);
           DComplex    prod(const DComplexVec&);
           DComplexVec reverse(const DComplexVec&);
           DComplex    sum(const DComplexVec&);


    /**** Functions for both real and complex ****/

           DComplexVec cos(const DComplexVec& V);
           DComplexVec cosh(const DComplexVec& V);
           DComplexVec exp(const DComplexVec& V);
           DComplex    mean(const DComplexVec& V);
           DComplexVec pow(const DComplexVec&,const DComplexVec&);
           DComplexVec sin(const DComplexVec& V);
           DComplexVec sinh(const DComplexVec& V);
           DComplexVec sqrt(const DComplexVec& V);
           double      variance(const DComplexVec&);


    /**** Functions for complex vectors ****/

           DoubleVec   arg(const DComplexVec& V);
           DComplexVec conj(const DComplexVec& V);
           DComplex    conjDot(const DComplexVec& v, const DComplexVec& w); // dot(conj(v),w)
           DComplexVec expandConjugateEven(const DComplexVec&);
           DComplexVec expandConjugateOdd(const DComplexVec&);
           DoubleVec   imag(const DComplexVec&);  // Returns reference to data
           DoubleVec   norm(const DComplexVec& V);
           DoubleVec   real(const DComplexVec&);  // Returns reference to data
           DComplexVec rootsOfOne(int N, unsigned nterms);
           DComplexVec rootsOfOne(int N);


    /**** Functions for type conversion ****/

           /* No type conversion functions for DComplexVec */

    /**** Norm functions ****/
           Double         l2Norm(const DComplexVec&);    // Euclidean norm
	   Double         l1Norm(const DComplexVec&);    // sum of absolute values
           Double         linfNorm(const DComplexVec&);  // largest absolute value
           Double         maxNorm(const DComplexVec&);   // Same as linfNorm for vectors
           Double         frobNorm(const DComplexVec&);  // Same as l2Norm for vectors

    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline DComplexVec::DComplexVec(const RWDataView& v, DComplex* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline DComplex& DComplexVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline DComplex DComplexVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline DComplex& DComplexVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline DComplex DComplexVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline DComplexVec DComplexVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return DComplexVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const DComplexVec DComplexVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return DComplexVec(*this, (DComplex*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline DComplexVec DComplexVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return DComplexVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const DComplexVec DComplexVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return DComplexVec(*this, (DComplex*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline DComplex         operator%(const DComplexVec& x, const DComplexVec& y) {return dot(x,y);}

#endif /* __RWCVEC_H__ */

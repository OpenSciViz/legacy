#ifndef __RWCTOKEN_H__
#define __RWCTOKEN_H__

/*
 * RWCTokenizer --- converts strings into sequences of tokens
 *
 * $Id: ctoken.h,v 1.2 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ctoken.h,v $
 * Revision 1.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.1  1993/02/11  00:51:04  myersn
 * Initial revision
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   01 Mar 1992 15:45:24   KEFFER
 * Now uses RWCString instead of RWString.
 * 
 *    Rev 1.2   28 Oct 1991 09:08:26   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.1   22 Aug 1991 10:43:26   keffer
 * operator()() no longer uses default arguments (as per the ARM).
 * 
 *    Rev 1.0   28 Jul 1991 08:17:36   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/cstring.h"

class RWExport RWCTokenizer {
  const RWCString*	theString;
  const char*		place;
public:
  RWCTokenizer(const RWCString& s);	// Construct to lex a string

  // Advance to next token, delimited by s:
  RWCSubString		operator()(const char* s);
  RWCSubString		operator()(); // { return operator()(" \t\n"); }
};

typedef RWCTokenizer RWTokenizer;  // backward compatibility

#endif /* __RWCTOKEN_H__ */

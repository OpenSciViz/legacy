
/*
 * Template definitions for RWTPtrSlistDictionary<K,V> and RWTPtrSlistDictionaryIterator<K,V>
 *
 * $Id: tpsldict.cc,v 1.7 1993/11/08 13:16:37 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tpsldict.cc,v $
 * Revision 1.7  1993/11/08  13:16:37  jims
 * Port to ObjectStore
 *
 * Revision 1.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/09/07  19:41:18  griswolf
 *  fixed applyToKeyAndValue to correctly handle last item
 *
 * Revision 1.4  1993/02/18  23:29:41  keffer
 * Corrected syntax error in postcondition.
 *
 * Revision 1.3  1993/02/17  20:25:41  keffer
 * Now uses RWTPtrAssocLink<KP,VP> rather than RWTValAssocLink<KP*,VP*>
 *
 * Revision 1.2  1993/02/12  23:33:45  keffer
 * Ported to g++ v2.3
 *
 * Revision 1.1  1993/01/28  21:11:49  keffer
 * Ported to cfront V3.0
 *
 *
 ***************************************************************************
 */


// Copy constructor (some of these names get pretty bloody long, eh?)
template <class KP, class VP>
RWTPtrSlistDictionary<KP,VP>::RWTPtrSlistDictionary(const RWTPtrSlistDictionary<KP,VP>& d)  {

  // Cast away "constness", which we will honor anyway...
  RWTPtrSlistDictionaryIterator<KP,VP> next((RWTPtrSlistDictionary<KP,VP>&)d);

  while (++next)
    insertKeyAndValue(next.key(), next.value());

  RWPOSTCONDITION(entries()==d.entries());
}

template <class KP, class VP> RWTPtrSlistDictionary<KP,VP>&
RWTPtrSlistDictionary<KP,VP>::operator=(const RWTPtrSlistDictionary<KP,VP>& d)
{
  if (this!=&d)
  {

    clear();

    // Cast away "constness", which we will honor anyway...
    RWTPtrSlistDictionaryIterator<KP,VP> next((RWTPtrSlistDictionary<KP,VP>&)d);

    while (++next)
      insertKeyAndValue(next.key(), next.value());

  }
  RWPOSTCONDITION(entries()==d.entries());
  return *this;
}

template <class KP, class VP> VP*&
RWTPtrSlistDictionary<KP,VP>::operator[](KP* key)
{
  RWTPtrAssocLink<KP,VP>* assoc = findLink(key);

  if (assoc==rwnil)
    RWTIsvSlist<RWTPtrAssocLink<KP,VP> >::insert(assoc = new RWTPtrAssocLink<KP,VP>(key));

  RWPOSTCONDITION(*key == *(assoc->key_));
  return assoc->value_;
}

template <class KP, class VP> void
RWTPtrSlistDictionary<KP,VP>::applyToKeyAndValue(void (*applyFun)(KP*,VP*&,void*), void* a)
{
  RWTPtrAssocLink<KP,VP>* link = firstLink();
  while (link != tailLink())
  {
    applyFun(link->key_, link->value_, a);
    link = link->next();
  }
}

template <class KP, class VP> void
RWTPtrSlistDictionary<KP,VP>::clearAndDestroy()
{
  RWTPtrAssocLink<KP,VP>* link = firstLink();
  while (link != tailLink())
  {
    RWTPtrAssocLink<KP,VP>* next = link->next();
    delete link->key_;
    delete link->value_;
    delete link;
    link = next;
  }
  init();
}

template <class KP, class VP> RWBoolean
RWTPtrSlistDictionary<KP,VP>::contains(KP* key) const
{
  return findLink(key)!=rwnil;
}

template <class KP, class VP> KP*
RWTPtrSlistDictionary<KP,VP>::find(KP* key) const
{
  RWTPtrAssocLink<KP,VP>* assoc = findLink(key);
  return assoc ? assoc->key_ : rwnil;
}

template <class KP, class VP> KP*
RWTPtrSlistDictionary<KP,VP>::findKeyAndValue(KP* key, VP*& retVal) const
{
  RWTPtrAssocLink<KP,VP>* assoc = findLink(key);
  if (assoc)
  {
    retVal = assoc->value_;
    RWPOSTCONDITION( *key == *assoc->key_ );
    return assoc->key_;
  }
  return rwnil;
}

template <class KP, class VP> VP*
RWTPtrSlistDictionary<KP,VP>::findValue(KP* key) const
{
  RWTPtrAssocLink<KP,VP>* assoc = findLink(key);
  return assoc ? assoc->value_ : rwnil;
}

template <class KP, class VP> KP*
RWTPtrSlistDictionary<KP,VP>::remove(KP* key)
{
  RWTPtrAssocLink<KP,VP>* assoc = removeLink(key);
  if (assoc)
  {
    KP* retKey = assoc->key_;
    delete assoc;
    RWPOSTCONDITION(*retKey == *key);
    return retKey;
  }
  return rwnil;
}

template <class KP, class VP> KP*
RWTPtrSlistDictionary<KP,VP>::removeKeyAndValue(KP* key, VP*& retVal)
{
  RWTPtrAssocLink<KP,VP>* assoc = removeLink(key);
  if (assoc)
  {
    KP* retKey = assoc->key_;
    retVal     = assoc->value_;
    delete assoc;
    RWPOSTCONDITION(*retKey == *key);
    return retKey;
  }
  return rwnil;
}


// Protected functions ---

template <class KP, class VP> RWTPtrAssocLink<KP,VP>*
RWTPtrSlistDictionary<KP,VP>::findLink(KP* key) const
{
  RWTPtrAssocLink<KP,VP>* link = firstLink();
  while (link != tailLink())
  {
    if (*link->key_ == *key)
      return link;
    link = link->next();
  }
  return rwnil;
}


template <class KP, class VP> RWTPtrAssocLink<KP,VP > *
RWTPtrSlistDictionary<KP,VP>::removeLink(KP* key)
{
  RWPRECONDITION(lastLink()->next()==tailLink());

  // Set up a sentinel:
  RWTPtrAssocLink<KP,VP> newTail(key);
  lastLink()->next_ = &newTail;

  // Now search for it:
  RWTPtrAssocLink<KP,VP>* link = headLink();
  while (!(*link->next()->key_ == *key))
    link = link->next();

  // Reset the pointer in the last link:
  lastLink()->next_ = tailLink();

  if (link != lastLink())
  {
    RWPOSTCONDITION(*link->next()->key_ == *key);
    return removeRight(link);
  }
  return rwnil;
}



/*
 * Template definitions for RWTPtrDlist<T> and RWTPtrDlistIterator<T>
 *
 * $Id: tpdlist.cc,v 1.6 1993/11/05 22:38:26 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tpdlist.cc,v $
 * Revision 1.6  1993/11/05  22:38:26  jims
 * Port to objectStore
 *
 * Revision 1.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.4  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.3  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.2  1993/01/28  01:35:03  keffer
 * Corrected error in removeNext()
 *
 * Revision 1.1  1993/01/27  21:38:32  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */

template <class TP> 
RWTPtrDlist<TP>::RWTPtrDlist(const RWTPtrDlist<TP>& s)
{
  // Construct an iterator, casting away "constness"
  // (which we promise to honor anyway):
  RWTPtrDlistIterator<TP> next((RWTPtrDlist<TP>&)s);
  TP* p;

  while ((p = next())!=rwnil)
    append(p);

  RWPOSTCONDITION(s.entries()==entries());
}

template <class TP> RWTPtrDlist<TP>&
RWTPtrDlist<TP>::operator=(const RWTPtrDlist<TP>& s)
{
  if (this!=&s)
  {
    clear();
    // Construct an iterator, casting away "constness"
    // (which we promise to honor anyway):
    RWTPtrDlistIterator<TP> next((RWTPtrDlist<TP>&)s);
    TP* p;

    while ((p = next())!=rwnil)
      append(p);
  }

  RWPOSTCONDITION(s.entries()==entries());
  return *this;
}

template <class TP> void
RWTPtrDlist<TP>::append(TP* a)	
{
  RWTIsvDlist<RWTPtrDlink<TP> >::append(new RWTPtrDlink<TP>(a)); 
}

template <class TP> void
RWTPtrDlist<TP>::apply(void (*applyFun)(TP*, void*), void* d)
{
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    applyFun(link->info_, d);	// Apply the function
    link = link->next();	// Advance
  }
}

template <class TP> void
RWTPtrDlist<TP>::clearAndDestroy()
{
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    RWTPtrDlink<TP>* next = link->next();
    delete link->info_;
    delete link;
    link = next;
  }
  init();
}

template <class TP> RWBoolean
RWTPtrDlist<TP>::contains(TP* p) const
{
  return findVal(p)!=rwnil;
}

template <class TP> RWBoolean
RWTPtrDlist<TP>::contains(RWBoolean (*testFun)(TP*, void*), void* d) const
{
  RWPRECONDITION(testFun!=rwnil);

  return findFun(testFun, d)!=rwnil;
}

template <class TP> TP*
RWTPtrDlist<TP>::find(TP* p) const
{
  RWPRECONDITION(p != rwnil);

  RWTPtrDlink<TP>* link = firstLink();
  while (link!=tailLink())
  {
    if (*link->info_ == *p)
    {
      return link->info_;
    }
    link = link->next();	// Advance
  }
  return rwnil;
}

template <class TP> TP*
RWTPtrDlist<TP>::find(RWBoolean (*testFun)(TP*, void*), void* d) const
{
  RWPRECONDITION(testFun!=rwnil);

  RWTPtrDlink<TP>* link = firstLink();
  while (link!=tailLink())
  {
    if (testFun(link->info_, d))
    {
      return link->info_;
    }
    link = link->next();	// Advance
  }
  return rwnil;
}

template <class TP> size_t
RWTPtrDlist<TP>::index(TP* p) const
{
  RWPRECONDITION(p != rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (*link->info_ == *p)
      return count;
    ++count;
    link = link->next();	// Advance
  }
  return RW_NPOS;
}

template <class TP> size_t
RWTPtrDlist<TP>::index(RWBoolean (*testFun)(TP*, void*), void* d) const
{
  RWPRECONDITION(testFun!=rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link->info_,d))
      return count;
    ++count;
    link = link->next();	// Advance
  }
  return RW_NPOS;
}


template <class TP> void
RWTPtrDlist<TP>::insert(TP* a)
{
  RWTIsvDlist<RWTPtrDlink<TP> >::insert(new RWTPtrDlink<TP>(a)); 
}

template <class TP> void
RWTPtrDlist<TP>::insertAt(size_t i, TP* a)
{
  RWTIsvDlist<RWTPtrDlink<TP> >::insertAt(i, new RWTPtrDlink<TP>(a)); 
}

template <class TP> size_t
RWTPtrDlist<TP>::occurrencesOf(TP* p) const
{
  RWPRECONDITION(p != rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (*link->info_ == *p)
      ++count;
    link = link->next();	// Advance
  }
  return count;
}

template <class TP> size_t
RWTPtrDlist<TP>::occurrencesOf(RWBoolean (*testFun)(TP*, void*), void* d) const
{
  RWPRECONDITION(testFun!=rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link->info_, d))
      ++count;
    link = link->next();	// Advance
  }
  return count;
}

template <class TP> void
RWTPtrDlist<TP>::prepend(TP* a)
{
  RWTIsvDlist<RWTPtrDlink<TP> >::prepend(new RWTPtrDlink<TP>(a)); 
}

template <class TP> TP*
RWTPtrDlist<TP>::remove(TP* p)
{
  RWPRECONDITION(p != rwnil);
  RWPRECONDITION(lastLink()->next()==tailLink());

  TP* ret = rwnil;

  // Set up a sentinel:
  RWTPtrDlink<TP> newTail(p);
  lastLink()->next_ = &newTail;

  // Now search for it:
  RWTPtrDlink<TP>* link = firstLink();
  while (!(*link->info_ == *p))
    link = link->next();

  // Reset the pointer in the last link:
  lastLink()->next_ = tailLink();

  if (link != &newTail)
  {
    RWTPtrDlink<TP>* victim = removeReference(link);
    RWASSERT(victim == link);
    ret = victim->info_;
    delete victim;
  }
  RWPOSTCONDITION(ret==rwnil || *ret == *p);
  return ret;
}

template <class TP> TP*
RWTPtrDlist<TP>::remove(RWBoolean (*testFun)(TP*, void*), void* d)
{
  RWPRECONDITION(testFun!=rwnil);

  TP* ret = peel(removeReference(findFun(testFun, d)));

  RWPOSTCONDITION( ret==rwnil || testFun(ret, d) );

  return ret;
}

template <class TP> size_t
RWTPtrDlist<TP>::removeAll(TP* p)
{
  RWPRECONDITION(p != rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (*link->info_ == *p)
    {
      RWTPtrDlink<TP>* next = link->next();
      delete removeReference(link);
      link = next;
      count++;
    }
    else
      link = link->next();
  }
  return count;
}

template <class TP> size_t
RWTPtrDlist<TP>::removeAll(RWBoolean (*testFun)(TP*, void*), void* d)
{
  RWPRECONDITION(testFun != rwnil);

  size_t count = 0;
  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link->info_, d))
    {
      RWTPtrDlink<TP>* next = link->next();
      delete removeReference(link);
      link = next;
      count++;
    }
    else
      link = link->next();
  }
  return count;
}

/***********************************************************
 *							   *
 *	    RWTPtrDlist<TP> protected functions		   *
 *							   *
 ***********************************************************/

/*
 * Find the link containing the indicated value.
 */
template <class TP> RWTPtrDlink<TP>*
RWTPtrDlist<TP>::findVal(TP* p) const
{
  RWPRECONDITION(p != rwnil);

  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (*link->info_ == *p)
      return link;
    link = link->next();
  }
  return rwnil;
}

/*
 * Find the link that contains info_ that tests true.
 */
template <class TP> RWTPtrDlink<TP>*
RWTPtrDlist<TP>::findFun(RWBoolean (*testFun)(TP*, void*), void* d) const
{
  RWPRECONDITION(testFun!=rwnil);

  RWTPtrDlink<TP>* link = firstLink();
  while (link != tailLink())
  {
    if (testFun(link->info_, d))
      return link;
    link = link->next();
  }
  return rwnil;
}

/*
 * Extracts the value out of a link then throws the link away:
 */
template <class TP> TP*
RWTPtrDlist<TP>::peel(RWTPtrDlink<TP>* link)
{
  if (!link) return rwnil;

  TP* ret = link->info_;
  delete link;
  return ret;
}

/****************************************************************
 *								*
 *	Definitions for RWTPtrDlistIterator<TP>			*
 *								*
 ****************************************************************/

template <class TP> TP*
RWTPtrDlistIterator<TP>::findNext(TP* p)
{
  RWPRECONDITION(p != rwnil);

  TP* a;
  while ( (a = (*this)()) != rwnil)
  {
    if (*a == *p)
      return a;
  }
  return rwnil;
}

template <class TP> TP*
RWTPtrDlistIterator<TP>::findNext(RWBoolean (*testFun)(TP*, void*), void* d)
{
  RWPRECONDITION(testFun!=rwnil);

  TP* a;
  while ( (a = (*this)()) != rwnil)
  {
    if (testFun(a, d))
      return a;
  }
  return rwnil;
}

template <class TP> void
RWTPtrDlistIterator<TP>::insertAfterPoint(TP* a)
{
  RWTIsvDlistIterator<RWTPtrDlink<TP> >::insertAfterPoint(new RWTPtrDlink<TP>(a));
}

template <class TP> TP*
RWTPtrDlistIterator<TP>::remove()
{
  return RWTPtrDlist<TP>::peel(RWTIsvDlistIterator<RWTPtrDlink<TP> >::remove());
}

template <class TP> TP*
RWTPtrDlistIterator<TP>::removeNext(TP* p)
{
  RWPRECONDITION(p != rwnil);

  while (cursor()->next() != container()->tailLink())
  {
    if (*cursor()->next()->info_ == *p)
    {
      return RWTPtrDlist<TP>::peel(container()->removeReference(cursor()->next()));
    }
    advance();
  }
  return rwnil;
}

template <class TP> TP*
RWTPtrDlistIterator<TP>::removeNext(RWBoolean (*testFun)(TP*, void*), void* d)
{
  RWPRECONDITION(testFun!=rwnil);

  while (cursor()->next() != container()->tailLink())
  {
    if (testFun(cursor()->next()->info_, d))
    {
      return RWTPtrDlist<TP>::peel(container()->removeReference(cursor()->next()));
    }
    advance();
  }
  return rwnil;
}


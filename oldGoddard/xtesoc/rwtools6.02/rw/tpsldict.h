#ifndef __RWTPSLDICT_H__
#define __RWTPSLDICT_H__

/*
 * RWTPtrSlistDictionary<KP,VP>: A dictionary of pointers to keys of type KP,
 *   and pointers to values of type VP, implemented as a singly-linked list.
 *
 * $Id: tpsldict.h,v 2.9 1993/11/08 13:16:37 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Assumes that KP has:
 *
 *   - well-defined equality semantics (KP::operator==(const KP&)).
 *
 ***************************************************************************
 *
 * $Log: tpsldict.h,v $
 * Revision 2.9  1993/11/08  13:16:37  jims
 * Port to ObjectStore
 *
 * Revision 2.8  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.7  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.6  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.5  1993/02/17  20:25:41  keffer
 * Now uses RWTPtrAssocLink<KP,VP> rather than RWTValAssocLink<KP*,VP*>
 *
 * Revision 2.4  1993/02/12  23:33:45  keffer
 * Ported to g++ v2.3
 *
 * Revision 2.3  1993/02/12  00:18:50  keffer
 * Ported to the IBM xlC compiler
 *
 * Revision 2.2  1993/01/28  21:11:49  keffer
 * Ported to cfront V3.0
 *
 *    Rev 1.0   25 May 1992 15:59:24   KEFFER
 * Initial revision.
 * 
 */
#include "rw/tislist.h"
#include "rw/tasslnk.h"

template <class KP, class VP> class RWExport RWTPtrSlistDictionaryIterator;

/****************************************************************
 *								*
 *	Declarations for RWTPtrSlistDictionary<KP,VP>		*
 *								*
 ****************************************************************/

template <class KP, class VP>
class RWExport RWTPtrSlistDictionary 
                         : private RWTIsvSlist< RWTPtrAssocLink<KP, VP> >
{

public:

  RWTPtrSlistDictionary() {;}
  RWTPtrSlistDictionary(const RWTPtrSlistDictionary<KP,VP>&);
  ~RWTPtrSlistDictionary()
	{RWTIsvSlist<RWTPtrAssocLink<KP,VP> >::clearAndDestroy();}

  // Operators:
  RWTPtrSlistDictionary<KP,VP>&
			operator=(const RWTPtrSlistDictionary<KP,VP>&);
  VP*&			operator[](KP* key); // Look up key, return value

  // Member functions:
  void			applyToKeyAndValue(void (*applyFun)(KP*,VP*&,void*), void*);

  void			clear()
	{RWTIsvSlist<RWTPtrAssocLink<KP,VP> >::clearAndDestroy();}

  void			clearAndDestroy();

  RWBoolean		contains(KP*) const;	// Contain key?

  size_t		entries() const
	{return RWTIsvSlist<RWTPtrAssocLink<KP,VP> >::entries();}

  RWBoolean		isEmpty() const
	{return RWTIsvSlist<RWTPtrAssocLink<KP,VP> >::isEmpty();}

  KP*			find(KP* key) const;
  KP*			findKeyAndValue(KP* key, VP*& retVal) const;
  VP*			findValue(KP* key) const;

  void			insertKeyAndValue(KP* key, VP* value)
  	{ (*this)[key] = value; }

  KP*			remove(KP* key);
  KP*			removeKeyAndValue(KP* key, VP*& retVal);

protected:

  RWTPtrAssocLink<KP,VP > *	findLink(KP*) const;
  RWTPtrAssocLink<KP,VP > *	removeLink(KP*);

private:

  friend class RWExport RWTPtrSlistDictionaryIterator<KP,VP>;

};


/****************************************************************
 *								*
 *     Declarations for RWTPtrSlistDictionaryIterator<KP,VP>	*
 *								*
 ****************************************************************/

template <class KP, class VP>
class RWExport RWTPtrSlistDictionaryIterator :
	private RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >
{

public:

  // Constructor:
  RWTPtrSlistDictionaryIterator(RWTPtrSlistDictionary<KP,VP>& d)
	: RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >(d) {;}

  // Operators:
  RWBoolean	operator++()
	{advance(); return cursor()!=container()->tailLink();}
  RWBoolean	operator+=(size_t n)
	{return RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >::operator+=(n)!=rwnil;}
  KP*		operator()()
	{advance(); return cursor()==container()->tailLink() ? rwnil : cursor()->key_;}

  RWTPtrSlistDictionary<KP,VP>*	container() const
	{return (RWTPtrSlistDictionary<KP,VP>*)RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >::container();}

  KP*		key() const
	{return cursor()->key_;}

  void		reset()
	{RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >::reset();}
  void		reset(RWTPtrSlistDictionary<KP,VP>& s)
	{RWTIsvSlistIterator<RWTPtrAssocLink<KP,VP> >::reset(s);}

  VP*		value() const
	{return cursor()->value_;}


private:

  // Disallow postfix increment.  Unless we hide it, some compilers will
  // substitute the prefix increment operator in its place.
  RWBoolean		operator++(int);
};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tpsldict.cc"
#endif

#endif	/* __RWTPSLDICT_H__ */


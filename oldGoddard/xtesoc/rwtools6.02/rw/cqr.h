#ifndef __RWCQR_H__
#define __RWCQR_H__
/*
 * DComplexQRDecomp:       representation of a QR decomposition
 * DComplexQRDecompServer: produce QR decompositions, using possibly non-default options
 *
 * Generated from template $Id: xqr.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A QR decomposition decomposes a matrix A like this:
 *
 *                    [ R ]
 *            A P = Q [   ]
 *                    [ 0 ]
 *
 * where P is a permutation matrix, Q is orthogonal, and R is upper
 * triangular if A is taller than wide, and upper trapazoidal is  A
 * is wider than tall.
 */

#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/ivec.h"

class DComplexQRDecomp {
private:
  long      *pivots_;    // nil if no pivoting was done
  DComplexVec    tau_;        // scalars needed to recover Q
  DComplexGenMat QR_;         // upper triangular part is R, rest is used to compute Q

public:
  DComplexQRDecomp();                      // Constructors
  DComplexQRDecomp(const DComplexQRDecomp&);
  DComplexQRDecomp(const DComplexGenMat&);
  void factor(const DComplexGenMat&);
  ~DComplexQRDecomp();
  void operator=(const DComplexQRDecomp&);

  unsigned       rows() const         {return QR_.rows();}
  unsigned       cols() const         {return QR_.cols();}
  DComplexGenMat      P() const;           // Extract components of the decomposition
  DComplexGenMat      R() const;
  DComplexVec         Rdiagonal() const;
  DComplexGenMat      Q() const;

  DComplexVec         Px(const DComplexVec& x) const;    // Compute using the decomposition
  DComplexVec         PTx(const DComplexVec& x) const;
  DComplexVec         Rx(const DComplexVec& x) const;
  DComplexVec         RTx(const DComplexVec& x) const;
  DComplexVec         Rinvx(const DComplexVec& x) const;
  DComplexVec         RTinvx(const DComplexVec& x) const;
  DComplexVec         Qx(const DComplexVec& x) const;
  DComplexVec         QTx(const DComplexVec& x) const;

  friend class DComplexQRDecompServer;
  friend class DComplexCODecomp;
};

class DComplexQRDecompServer {
private:
  RWBoolean pivot_;       // Do we pivot?
  IntVec    initial_;     // Indices of columns to be moved to initial position

public:
  DComplexQRDecompServer();
  void setPivoting(RWBoolean);   // turn pivoting option on or off
  void setInitialIndex(int i);   // i moves to the begin of the decomposition
  void setFreeIndex(int i);      // i is removed from initial and final lists

  DComplexQRDecomp operator()(const DComplexGenMat&) const;   // build a QR decomposition
};
#endif

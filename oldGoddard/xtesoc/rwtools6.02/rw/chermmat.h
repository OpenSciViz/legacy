#ifndef __RWCHERMMAT_H__
#define __RWCHERMMAT_H__
/*
 * DComplexHermMat - A hermetian matrix of DComplexs
 *
 * Stores an Hermitian matrix.  Note that only the lower left 
 * half of the matrix is actually stored.
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"


class DoubleSymMat;
class DoubleSkewMat;
class DComplexGenMat;

class DComplexHermMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix


public:
DComplexHermMat();
DComplexHermMat( const DComplexHermMat& );
DComplexHermMat(unsigned n, unsigned nAgain);
DComplexHermMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexHermMat(const DoubleSymMat& re);
DComplexHermMat(const DoubleSymMat& re, const DoubleSkewMat& im);
~DComplexHermMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline CJDComplexRef operator()(int i, int j);
inline CJDComplexRef ref(int i, int j);
       CJDComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexHermMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexHermMat& operator=(const DComplexHermMat& m);
DComplexHermMat& reference(DComplexHermMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexHermMat copy() const;
DComplexHermMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexHermMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.

void makeDiagonalReal();            // Set imaginary part of diagonal to zero

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexHermMat& X);
RWBoolean operator!=(const DComplexHermMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexHermMat& operator+=(const DComplexHermMat& m);
DComplexHermMat& operator-=(const DComplexHermMat& m);
DComplexHermMat& operator*=(const DComplexHermMat& m);
DComplexHermMat& operator*=(DComplex);
DComplexHermMat& operator/=(const DComplexHermMat& m);
DComplexHermMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexHermMat operator-(const DComplexHermMat&);	// Unary minus
DComplexHermMat operator+(const DComplexHermMat&);	// Unary plus
DComplexHermMat operator*(const DComplexHermMat&, const DComplexHermMat&);
DComplexHermMat operator/(const DComplexHermMat&, const DComplexHermMat&);
DComplexHermMat operator+(const DComplexHermMat&, const DComplexHermMat&);
DComplexHermMat operator-(const DComplexHermMat&, const DComplexHermMat&);
DComplexHermMat operator*(const DComplexHermMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexHermMat operator*(DComplex x, const DComplexHermMat& A) { return A*x; }
#else
       DComplexHermMat operator*(DComplex x, const DComplexHermMat& A);
#endif
DComplexHermMat operator/(const DComplexHermMat& A, DComplex x);
DComplexHermMat operator/(DComplex x, const DComplexHermMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexHermMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexHermMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexHermMat transpose(const DComplexHermMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexHermMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexHermMat& A);
  // inner products

DComplexHermMat toHermMat( const DComplexGenMat& A );
DComplexHermMat upperToHermMat( const DComplexGenMat& A );
DComplexHermMat lowerToHermMat( const DComplexGenMat& A );

DoubleSymMat abs(const DComplexHermMat& A);
DComplexHermMat conj(const DComplexHermMat& A);
DoubleSymMat real(const DComplexHermMat& A);
DoubleSkewMat imag(const DComplexHermMat& A);
DoubleSymMat norm(const DComplexHermMat& A);
DoubleSkewMat arg(const DComplexHermMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexHermMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    DComplex val = vec(index);
    return (i<=j) ?  val : conj(val);
#endif
}

inline DComplex DComplexHermMat::set(int i, int j, DComplex x) {
    return bcset(i,j,x);
}

inline CJDComplexRef DComplexHermMat::ref(int i, int j) {
    return bcref(i,j);
}

inline CJDComplexRef DComplexHermMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexHermMat::operator()(int i, int j) const { return val(i,j); }


#endif

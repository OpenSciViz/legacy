#ifndef __RWHEAP_H__
#define __RWHEAP_H__ 1

/*
 * Declarations for class RWHeap
 *
 * $Header: /u3/jims/tool602/rogue/rw/RCS/heap.h,v 1.2 1993/01/21 21:28:22 myersn Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 * email: support@roguewave.com
 *
 * Copyright (C) 1992. 
 * This software is subject to copyright protection under the laws of 
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Class RWHeap defines an abstract interface to memory allocation
 * utilities.  Implementations derived from RWHeap are expected to
 * provide for deallocation, ideally by global operator delete(), or
 * by other (non-generalizable) means if necessary.
 *
 ***************************************************************************
 *
 * $Log: heap.h,v $
 * Revision 1.2  1993/01/21  21:28:22  myersn
 * disable copy and assignment of RWHeapStackTop.
 *
 * Revision 1.1  1993/01/19  18:40:56  myersn
 * Initial revision
 *
 */

#ifndef __RWCOMPILER_H__
#include <rw/compiler.h>    /* for STARTWRAP/ENDWRAP */
#endif
STARTWRAP
#include <stddef.h>         /* for size_t */
ENDWRAP

class RWHeap {
 private:
  RWHeap(RWHeap const&);            // not defined
  void operator=(RWHeap const&);    // not defined

 protected:
  virtual void* allocate(size_t) = 0;
  RWHeap() {}
  virtual ~RWHeap() = 0; 

  friend void* operator new(size_t);
  friend void* operator new(size_t, RWHeap&);
};

class RWHeapStackTop {
  RWHeap& h_;
  RWHeapStackTop(RWHeapStackTop const&); // not defined
  void operator=(RWHeapStackTop const&); // not defined
 public:
  RWHeapStackTop(RWHeap& h);
  ~RWHeapStackTop();
};

inline void*
operator new(size_t size, RWHeap& heap) { return heap.allocate(size); }

#endif /* __RWHEAP_H__

#ifndef  __RWSTACKCOL_H__
#define  __RWSTACKCOL_H__

/*
 * Declarations for a Stack of RWCollectables, implemented as a Singly-linked list.
 *
 * $Id: stackcol.h,v 2.7 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: stackcol.h,v $
 * Revision 2.7  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.6  1993/04/13  23:05:00  keffer
 * Now allows functions find(), contains().
 *
 * Revision 2.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.3  1993/01/25  18:12:13  keffer
 * RW_NO_CONST_OVERLOADS->RW_NO_CONST_OVERLOAD
 *
 * Revision 2.1  1992/11/19  05:45:01  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 * 
 *    Rev 1.4   22 May 1992 17:04:18   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   04 Mar 1992 09:04:48   KEFFER
 * nil changed to rwnil
 * 
 *    Rev 1.1   28 Oct 1991 09:08:24   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:17:26   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

/*
 * Restricted interface to class RWSlistCollectables to implement a
 * last in first out (LIFO) stack.
 */

#include "rw/slistcol.h"

/****************************************************************
 *								*
 *		RWSlistCollectablesStack			*
 *								*
 ****************************************************************/

class RWExport RWSlistCollectablesStack : public RWSlistCollectables {

  RWDECLARE_COLLECTABLE(RWSlistCollectablesStack)

public:

  RWSlistCollectablesStack();
  RWSlistCollectablesStack(RWCollectable* a) : RWSlistCollectables(a) { }

  /************* Virtual functions inherited from RWSlistCollectables ***********/
//virtual void			apply(RWapplyCollectable ap, void* x) 
//virtual RWCollectable*&	at(size_t i)			{return RWSlistCollectables::at(i);}
//virtual const RWCollectable*	at(size_t) const		{return rwnil;}
//virtual void 			clear();
//virtual void			clearAndDestroy();
//virtual RWBoolean 		contains(const RWCollectable* a) const;
//RWBoolean			containsReference(const RWCollectable* a) const'
//virtual size_t		entries() const;
//virtual RWCollectable*	find(const RWCollectable*) const
//RWCollectable*		findReference(const RWCollectable*) const
//virtual RWCollectable* 	first() const	// Same as top()
//virtual RWCollectable*	get()		// Same as pop()
//virtual size_t		index(const RWCollectable*) const
  virtual RWCollectable*	insert(RWCollectable* a)	// Same as push()
    { return prepend(a); }
//virtual RWBoolean		isEmpty() const 
//virtual RWCollectable*	last() const	// Return value at bottom of stack
//virtual size_t		occurrencesOf(const RWCollectable* a) const
//size_t			occurrencesOfReference(const RWCollectable* a) const
//virtual RWCollectable*	prepend(RWCollectable*);
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual RWCollectable*	remove(const RWCollectable*) // Treated as pop(); argument ignored
    {return RWSlistCollectables::get();}

  /********************* Special functions **************************/
  void				push(RWCollectable* a)	{RWSlistCollectables::prepend(a);}
  RWCollectable*		pop()	   		{return RWSlistCollectables::get();}
  RWCollectable*		top() const 		{return RWSlistCollectables::first();}

private:

  /******************** Disallowed functions ************************/
  virtual RWCollectable*	append(RWCollectable*)		{return rwnil;}
  RWCollectable*		removeReference(const RWCollectable*)     {return rwnil;  }
  virtual void			removeAndDestroy(const RWCollectable*)	  {;}
};                                                              

#endif /* __RWSTACKCOL_H__ */

#ifndef __RWTSD_H__
#define __RWTSD_H__

/*
 * Declarations for Task Specific Data calls to rwtsd.dll
 *   or to the OS2 task specific data manager
 *
 * $Id: rwtsd.h,v 1.8 1993/11/18 01:20:44 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwtsd.h,v $
 * Revision 1.8  1993/11/18  01:20:44  jims
 * Move support for OS/2 MT from rwtsd to instmgr
 *
 * Revision 1.7  1993/11/16  09:03:34  myersn
 * support OS/2
 *
 * Revision 1.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/08/05  11:40:24  jims
 * Remove exitProc and GlobalRelease... function
 *
 * Revision 1.4  1993/02/10  02:45:35  jims
 * Now compiles under STRICT; RWTSDKEY typedef changed to unsigned long
 *
 * Revision 1.3  1993/02/05  07:55:14  jims
 * Cosmetic changes
 *
 * Revision 1.2  1993/02/03  20:15:55  jims
 * Added test to see if header file already included.
 * Added header and log information
 *
 * 
 */

#if defined(__DLL__) && defined(__WIN16__)
#  include <windows.h>
#  define rwfar FAR
#  define rwpascal PASCAL
   typedef unsigned long RWTSDKEY;
#else
#  error Task Specific Data used only for DLLs
#endif


RWTSDKEY rwfar rwpascal 
RWGetTaskSpecificKey();

int rwfar rwpascal 
RWSetTaskSpecificData(RWTSDKEY hKey, void rwfar*);

void rwfar* rwfar rwpascal 
RWGetTaskSpecificData(RWTSDKEY hKey);

void rwfar* rwfar rwpascal 
RWReleaseTaskSpecificData(RWTSDKEY hKey);

#endif  /* __RWTSD_H__ */

#ifndef __RWVSTREAM_H__
#define __RWVSTREAM_H__

/*
 * rw[io]stream --- abstract base class for I/O
 *
 * $Id: vstream.h,v 2.15 1993/11/18 22:12:19 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: vstream.h,v $
 * Revision 2.15  1993/11/18  22:12:19  jims
 * add version number to vstreams for backward compatibility
 *
 * Revision 2.14  1993/11/17  04:12:46  keffer
 * Added function putString(), to allow support of embedded nulls
 *
 * Revision 2.13  1993/11/14  21:35:26  keffer
 * Added virtual destructor for RWvios
 *
 * Revision 2.12  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.11  1993/08/19  01:22:53  keffer
 * Added default constructor for RWios.
 *
 * Revision 2.10  1993/06/05  17:55:20  keffer
 * Put RWvios pure virtuals in RWv[io]stream to avoid cfront V2.0 bug.
 *
 * Revision 2.9  1993/04/15  00:49:45  keffer
 * Cfront "sorry, not implemented" when attempting merge of
 * conversion functions.
 *
 * Revision 2.8  1993/04/14  21:03:52  keffer
 * Changed inheritance hierarchy to allow XDR streams, which cannot use
 * streambuf model.
 *
 * Revision 2.7  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.6  1993/02/23  14:54:09  alv
 * made constructors non-inline; this avoids vtable duplication by
 * giving an object file in which to put the vtable.
 *
 * Revision 2.5  1993/01/22  17:38:12  keffer
 * V1.2 style streams no longer supported.
 *
 * Revision 2.3  1992/11/26  21:04:33  myersn
 * add members to operate on wchar_t values and strings.
 *
 * Revision 2.2  1992/11/19  05:45:01  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.1  1992/11/15  22:12:09  keffer
 * Removed use of macro NL
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 *    Rev 1.5   05 Nov 1991 13:52:52   keffer
 * Can now live in the DLL
 * 
 *    Rev 1.4   17 Oct 1991 09:12:58   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   29 Jul 1991 11:33:30   keffer
 * Macro RW_NO_OVERLOAD_UCHAR checks for overloaded unsigned char
 * 
 *    Rev 1.1   24 Jul 1991 13:06:50   keffer
 * Added pvcs keywords
 *
 */

#include "rw/rstream.h"

/*
 * vstreams maintain a version number to help with backward compatibility 
 * issues.  See RWCString::saveOn for an example of how the version number
 * can be used internally.  Users of the library can set the version
 * number of a stream to force it to behave as in a previous incarnation.
 *
 *     Version 0: Baseline (virtually introduced with Tool600)
 *     Version 1: RWCStrings now save/restore themselves to/from virtual
 *                streams as sequence of characters, not numbers.  (Introduced
 *                with Tool602).
 */
static const unsigned current_version = 1;

/************************************************
 *						*
 *		class RWvios			*
 *						*
 ************************************************/

class RWExport RWvios
{

public:

  virtual ~RWvios() {;}

  // Parallel the ios interface:
  virtual int		eof()          = 0;
  virtual int		fail()         = 0;
  virtual int		bad()          = 0;
  virtual int		good()         = 0;
  virtual int		rdstate()      = 0;
  virtual void		clear(int v=0) = 0;

  // For virtual function definition heuristic:
  int		dummy() const;

};


/************************************************
 *						*
 *		class RWios			*
 *						*
 ************************************************/

// Convenience class for virtual streams that want ios type functionality,
// but cannot use streambufs in their implementation:

class RWExport RWios : public virtual RWvios
{

public:

  RWios() : status(0) {;}
  virtual int		eof()           {return status & ios::eofbit;}
  virtual int		fail()          {return status & ios::failbit;}
  virtual int		bad()           {return status & ios::badbit;}
  virtual int		good()          {return status==0;}
  virtual int		rdstate()       {return status;}
  virtual void		clear(int v=0)  {status = v;}

protected:

  int status;

};


/************************************************
 *						*
 *		class RWvistream		*
 *						*
 ************************************************/

class RWExport RWvistream : public virtual RWvios
{

public:

  virtual int		eof()          = 0;
  virtual int		fail()         = 0;
  virtual int		bad()          = 0;
  virtual int		good()         = 0;
  virtual int		rdstate()      = 0;
  virtual void		clear(int v=0) = 0;

  virtual int		get()          = 0;		// Treat as number; EOF on end-of-file
  virtual RWvistream&	get(char&)     = 0;		// Treat as number
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	get(wchar_t&)  = 0;	        // Treat as character
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	get(unsigned char&) = 0;	// Treat as number
#endif
  virtual RWvistream&	get(char*   , size_t N) = 0;
  virtual RWvistream&	get(double* , size_t N) = 0;
  virtual RWvistream&	get(float*  , size_t N) = 0;
  virtual RWvistream&	get(int*    , size_t N) = 0;
  virtual RWvistream&	get(long*   , size_t N) = 0;
  virtual RWvistream&	get(short*  , size_t N) = 0;
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	get(wchar_t*, size_t N) = 0;
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	get(unsigned char* , size_t N) = 0;
#endif
  virtual RWvistream&	get(unsigned short*, size_t N) = 0;
  virtual RWvistream&	get(unsigned int*  , size_t N) = 0;
  virtual RWvistream&	get(unsigned long* , size_t N) = 0;
  virtual RWvistream&	getString(char* s  , size_t N) = 0;	// Treat as character string
  virtual RWvistream&	operator>>(char&)    = 0;		// Treat as character
  virtual RWvistream&	operator>>(double&)  = 0;
  virtual RWvistream&	operator>>(float&)   = 0;
  virtual RWvistream&	operator>>(int&)     = 0;
  virtual RWvistream&	operator>>(long&)    = 0;
  virtual RWvistream&	operator>>(short&)   = 0;
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	operator>>(wchar_t&) = 0;
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	operator>>(unsigned char&) = 0;
#endif
  virtual RWvistream&	operator>>(unsigned short&) = 0;
  virtual RWvistream&	operator>>(unsigned int&)   = 0;
  virtual RWvistream&	operator>>(unsigned long&)  = 0;

// For backwards compatibility
private:
  unsigned version_;
public:
  void version(unsigned v) { version_ = v; }
  unsigned version() { return version_; }
// constructor
  RWvistream() : version_(current_version) { }
};


/************************************************
 *						*
 *		class RWvostream		*
 *						*
 ************************************************/

class RWExport RWvostream : public virtual RWvios
{

public:

  virtual int		eof()          = 0;
  virtual int		fail()         = 0;
  virtual int		bad()          = 0;
  virtual int		good()         = 0;
  virtual int		rdstate()      = 0;
  virtual void		clear(int v=0) = 0;

  virtual RWvostream&	putString(const char*, size_t) = 0;	// Treat as character string
  virtual RWvostream&	operator<<(const char*) = 0;		// Treat as character string
  virtual RWvostream&	operator<<(char) = 0;			// Treat as character
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvostream&	operator<<(wchar_t) = 0;
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvostream&	operator<<(unsigned char)  = 0;
#endif
  virtual RWvostream&	operator<<(double)         = 0;
  virtual RWvostream&	operator<<(float)          = 0;
  virtual RWvostream&	operator<<(int)            = 0;
  virtual RWvostream&	operator<<(unsigned int)   = 0;
  virtual RWvostream&	operator<<(long)           = 0;
  virtual RWvostream&	operator<<(unsigned long)  = 0;
  virtual RWvostream&	operator<<(short)          = 0;
  virtual RWvostream&	operator<<(unsigned short) = 0;

  virtual RWvostream&	put(char) = 0;				// Treat as number
  virtual RWvostream&	put(const char* p, size_t N) = 0;	// Treat as vector of numbers
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvostream&	put(wchar_t) = 0;
  virtual RWvostream&	put(const wchar_t* p, size_t N) = 0;
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvostream&	put(unsigned char) = 0;
  virtual RWvostream&	put(const unsigned char*  , size_t N) = 0;
#endif
  virtual RWvostream&	put(const short*          , size_t N) = 0;
  virtual RWvostream&	put(const unsigned short* , size_t N) = 0;
  virtual RWvostream&	put(const int*            , size_t N) = 0;
  virtual RWvostream&	put(const unsigned int*   , size_t N) = 0;
  virtual RWvostream&	put(const long*           , size_t N) = 0;
  virtual RWvostream&	put(const unsigned long*  , size_t N) = 0;
  virtual RWvostream&	put(const float*          , size_t N) = 0;
  virtual RWvostream&	put(const double*         , size_t N) = 0;

// For backwards compatibility
private:
  unsigned version_;
public:
  void version(unsigned v) { version_ = v; }
  unsigned version() { return version_; }
// constructor
  RWvostream() : version_(current_version) { }
};


#endif /* __RWVSTREAM_H__ */

#ifndef __RWDFFT_H__
#define __RWDFFT_H__

/*
 * Double Precision FFT server
 *
 * $Header: /users/rcs/mathrw/dfft.h,v 1.1 1993/01/23 00:08:33 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 * 
 ***************************************************************************
 *
 * $Log: dfft.h,v $
 * Revision 1.1  1993/01/23  00:08:33  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:44   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:00:56   keffer
 * Added pvcs keywords
 *
 */

/*
 * This class does fourier transforms involving a real sequence.  It is
 * derived from class DComplexFFTServer.
 * 
 * Let V(j), j=0,1,...,2N-1 be a 2N point long real sequence.  Its
 * transform will be a complex conjugate even sequence, that is, 
 * C(n) == conj(C(-n)) or C(n) == conj(C(2N-n)).
 * 
 * The routine fourier() returns the lower half of this complex conjugate
 * even sequence, that is C(n), n=0,..,N.  The upper half can be
 * recovered from the relationship C(n) == conj(C(2N-n)).  Note that the
 * requirement that the length of the original sequence be 2N means that
 * V must have an even number of points.  The resulting complex conjugate
 * even sequence C(n) will have N + 1 complex points, for a total of 2N+2
 * points.  The two extra points are the imaginary parts of C(0) and
 * C(N), which are always zero.  The transform calculated is:
 * 
 *                 2N-1
 *          C(n) = sum V(j) exp(-pi * n * j * I / 2N); n=0,...,2N-1
 *                 j=0
 * 
 * Given the lower half of C(n), the routine ifourier() calculates the
 * Inverse Fourier transform (IDFT):
 * 
 *                 2N-1
 *          V(j) = sum C(n) exp( pi * n * j * I / 2N); j=0,...,2N-1
 *                 n=0
 * 
 * (only the lower half C(n), n=0,...,N  should be given).
 * 
 * Note that the transform is not normalized: calling fourier(),
 * followed by ifourier() will leave the sequence multiplied by 2N.  
 * 
 * The server is set up to do a transform for a specified sequence
 * length (2N).  This length can be specified one of three ways:  Either
 * (1) at construction time; (2) by calling function setOrder(); or (3)
 * (the easiest!) let it figure it out for itself from the length of the
 * sequence handed to it.  If the sequence length changes, it will
 * automagically reconfigure (a relatively expensive calculation).
 */

#include "rw/cfft.h"
     
class DoubleFFTServer : private DComplexFFTServer{
  unsigned		serverLength;
  DComplexVec		roots_of_1;
  DComplexVec		conjroots_of_1;
protected:
  void			checkEven(int);
public:
  DoubleFFTServer();
  DoubleFFTServer(unsigned Nlength);  // construct for sequence Nlength points long
  DoubleFFTServer(const DoubleFFTServer&);

  void			operator=(const DoubleFFTServer&);

  unsigned		order()	const {return serverLength;}
  void			setOrder(unsigned Nlength);   // Set up for sequence Nlength point long

  /****************  TRANSFORMS ****************/

  // Returns DFT of a real sequence, which is a conjugate even sequence:
  DComplexVec		fourier(const DoubleVec&);

  // Returns IDFT of a complex conjugate even sequence, which is a real sequence:
  DoubleVec		ifourier(const DComplexVec& v);

};

#endif /*__RWDFFT_H__*/

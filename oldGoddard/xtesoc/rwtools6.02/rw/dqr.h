#ifndef __RWDQR_H__
#define __RWDQR_H__
/*
 * DoubleQRDecomp:       representation of a QR decomposition
 * DoubleQRDecompServer: produce QR decompositions, using possibly non-default options
 *
 * Generated from template $Id: xqr.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A QR decomposition decomposes a matrix A like this:
 *
 *                    [ R ]
 *            A P = Q [   ]
 *                    [ 0 ]
 *
 * where P is a permutation matrix, Q is orthogonal, and R is upper
 * triangular if A is taller than wide, and upper trapazoidal is  A
 * is wider than tall.
 */

#include "rw/dgenmat.h"
#include "rw/dvec.h"
#include "rw/ivec.h"

class DoubleQRDecomp {
private:
  long      *pivots_;    // nil if no pivoting was done
  DoubleVec    tau_;        // scalars needed to recover Q
  DoubleGenMat QR_;         // upper triangular part is R, rest is used to compute Q

public:
  DoubleQRDecomp();                      // Constructors
  DoubleQRDecomp(const DoubleQRDecomp&);
  DoubleQRDecomp(const DoubleGenMat&);
  void factor(const DoubleGenMat&);
  ~DoubleQRDecomp();
  void operator=(const DoubleQRDecomp&);

  unsigned       rows() const         {return QR_.rows();}
  unsigned       cols() const         {return QR_.cols();}
  DoubleGenMat      P() const;           // Extract components of the decomposition
  DoubleGenMat      R() const;
  DoubleVec         Rdiagonal() const;
  DoubleGenMat      Q() const;

  DoubleVec         Px(const DoubleVec& x) const;    // Compute using the decomposition
  DoubleVec         PTx(const DoubleVec& x) const;
  DoubleVec         Rx(const DoubleVec& x) const;
  DoubleVec         RTx(const DoubleVec& x) const;
  DoubleVec         Rinvx(const DoubleVec& x) const;
  DoubleVec         RTinvx(const DoubleVec& x) const;
  DoubleVec         Qx(const DoubleVec& x) const;
  DoubleVec         QTx(const DoubleVec& x) const;

  friend class DoubleQRDecompServer;
  friend class DoubleCODecomp;
};

class DoubleQRDecompServer {
private:
  RWBoolean pivot_;       // Do we pivot?
  IntVec    initial_;     // Indices of columns to be moved to initial position

public:
  DoubleQRDecompServer();
  void setPivoting(RWBoolean);   // turn pivoting option on or off
  void setInitialIndex(int i);   // i moves to the begin of the decomposition
  void setFreeIndex(int i);      // i is removed from initial and final lists

  DoubleQRDecomp operator()(const DoubleGenMat&) const;   // build a QR decomposition
};
#endif

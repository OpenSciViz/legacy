#ifndef __RWLAPK_ERROR_H__
#define __RWLAPK_ERROR_H__

/*
 * Error messages for LAPACK.h++
 *
 * $Id: lapkerr.h,v 1.2 1993/10/15 05:52:03 alv Exp $
 *
 ****************************************************************************
 *
 *  Rogue Wave Software, Inc.
 *  P.O. Box 2328
 *  Corvallis, OR 97339
 *
 *  Copyright (C) 1990, 1991. This software is subject to copyright protection
 *  under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: lapkerr.h,v $
 * Revision 1.2  1993/10/15  05:52:03  alv
 * updated for Math.h++ v5.1 error handling
 *
 *
 */

#include "rw/message.h"

extern const RWMsgId RWLAPK_BALANCE;
extern const RWMsgId RWLAPK_BANDSIZE;
extern const RWMsgId RWLAPK_CANTFREE;
extern const RWMsgId RWLAPK_CANTFREEZE;
extern const RWMsgId RWLAPK_CANTSOLVE;
extern const RWMsgId RWLAPK_CANTSOLVELS;
extern const RWMsgId RWLAPK_CHANGEDCOMPLEXCONSTANT;
extern const RWMsgId RWLAPK_CHANGEDCONSTANT;
extern const RWMsgId RWLAPK_CONDITION;
extern const RWMsgId RWLAPK_DIAGOUTOFBOUNDS;
extern const RWMsgId RWLAPK_LEADINGSUBMATRIXORDER;
extern const RWMsgId RWLAPK_MATMATPROD;
extern const RWMsgId RWLAPK_MATSIZE;
extern const RWMsgId RWLAPK_MATVECPROD;
extern const RWMsgId RWLAPK_NOEIG;
extern const RWMsgId RWLAPK_NOSV;
extern const RWMsgId RWLAPK_NOTHERM;
extern const RWMsgId RWLAPK_NOTSETABLE;
extern const RWMsgId RWLAPK_NOTSQUARE;
extern const RWMsgId RWLAPK_NOTSYM;
extern const RWMsgId RWLAPK_NUMBERPOINTS;
extern const RWMsgId RWLAPK_NUMBERPOINTSBAND;
extern const RWMsgId RWLAPK_OFFDIAG;
extern const RWMsgId RWLAPK_OUTOFBOUNDS;
extern const RWMsgId RWLAPK_QNOTCOMPUTED;
extern const RWMsgId RWLAPK_RESTORESIZE;
extern const RWMsgId RWLAPK_RSINGULAR;
extern const RWMsgId RWLAPK_SCHURREORDER;
extern const RWMsgId RWLAPK_VECLENGTH;
extern const RWMsgId RWLAPK_VECMATPROD;
extern const RWMsgId RWLAPK_WRONGNUMPOINTS;

#endif /* __RWLAPK_ERROR_H__ */

#ifndef __RWCSV_H__
#define __RWCSV_H__
/*
 * DComplexSVDecomp:   Singular value decomposition
 * DComplexSVServer:   SVD server using bidiagonal reduction
 * DComplexSVQRServer: SVD server using QR and then bidiagonal reduction
 *
 * Generated from template $Id: xsv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This class represents the singular value decomposition of an
 * mxn matrix A:
 *
 *                          [ s1                 ] 
 *                          [    .               ]  
 *            [           ] [      .             ] [   v1'   ]
 *            [           ] [        .           ] [    .    ]
 *        A = [ u1 ... um ] [          sr        ] [    .    ]
 *            [           ] [             0      ] [    .    ]
 *            [           ] [               .    ] [   vn'   ]
 *                          [                 .  ]
 *                          [                   .]
 *
 * where s1,...,sr are real positive numbers called the singular values
 * of A; u1,...,um are the left singular vectors of A; and v1,...,vn are
 * the right singular vectors of A.  The left singular vectors for an
 * orthonormal basis for Rm, the right singular vectors are an orthonormal
 * basis for Rn.  The singular values are ordered so s1>=s2>=...>=sr.
 * Often, only some of the singular vectors are needed; the default
 * server computes only the first r.  The number of singular values, r,
 * is the rank of A.  Often we truncate the singular value decomposition
 * and ignore any singular values less than some threshold, then r becomes
 * the numberical rank of A.  Doing this can yield more meaningful
 * computational results, since otherwise the insignificant singular
 * values can dominate the computation.
 */ 

#include "rw/lapkdefs.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"

class DComplexSVDecomp {
private:
  DoubleVec    sigma_;       // the singular values
  DComplexGenMat U_;           // columns are the computed left singular values
  DComplexGenMat VT_;          // rows are the computed right singular values
  RWBoolean computedAll_; // Did I compute everything the server tried to compute?

public:
  DComplexSVDecomp();
  DComplexSVDecomp(const DComplexSVDecomp&);
  DComplexSVDecomp(const DComplexGenMat&, Double tol=0);  // compute SVD using default server
  void factor(const DComplexGenMat&, Double tol=0);  // compute SVD using default server
  void operator=(const DComplexSVDecomp& x);

  unsigned        cols()             const {return VT_.cols();}
  Double             singularValue(int) const;
  const DoubleVec    singularValues()   const {return sigma_;}
  const DComplexVec    leftVector(int)    const;
  const DComplexGenMat leftVectors()      const {return U_;}
  RWBoolean       good()             const {return computedAll_;}
  RWBoolean       fail()             const {return !computedAll_;}
  unsigned        numLeftVectors()   const {return U_.cols();}
  unsigned        numRightVectors()  const {return VT_.rows();}
  unsigned        rank()             const {return sigma_.length();}
  const DComplexVec    rightVector(int)   const;
  const DComplexGenMat rightVectors()     const {return conj(transpose(VT_));}
  unsigned        rows()             const {return U_.rows();}
  void            truncate(Double tol);    // truncate singular values less than tol
               
  friend class DComplexSVServer;
  friend class DComplexSVQRServer;
};

class DComplexSVServer {
protected:
  RWBoolean computeAllLeftVecs_;      // compute all m left vecs if true
  RWBoolean computeAllRightVecs_;     // compute all n right vecs if true
  RWBoolean computeDefaultLeftVecs_;  // compute first min(m,n) left vecs if true
  RWBoolean computeDefaultRightVecs_; // compute first min(m,n) right vecs if true
  unsigned  numLeftVecs_;             // number of left vectors to compute (if switches all off)
  unsigned  numRightVecs_;            // number of right vectors to compute (if switches all off)
  Double       tol_;                     // when to disregard singular values

public:
  DComplexSVServer();
  virtual DComplexSVDecomp operator()(const DComplexGenMat&) const;
  void        computeAllVectors();
  void        computeAllLeftVectors();
  void        computeAllRightVectors();
  void        computeDefaultVectors();
  void        computeDefaultLeftVectors();
  void        computeDefaultRightVectors();
  void        computeVectors(unsigned);
  void        computeLeftVectors(unsigned);
  void        computeRightVectors(unsigned);
  void        setTolerance(Double);
};

class DComplexSVQRServer : public DComplexSVServer
{
  DComplexSVQRServer();
  virtual DComplexSVDecomp operator()(const DComplexGenMat&) const;
};

#endif


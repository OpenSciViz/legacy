#ifndef __RWDARR_H__
#define __RWDARR_H__

/*
 * Declarations for Double precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a double or double&.
 */

class DComplexArray;
#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class DoubleArray : public RWArrayView {
private:
    DoubleArray(const RWDataView&, double*, const IntVec&, const IntVec&); // For real() and imag() and slices
    friend DoubleArray real(const DComplexArray&);
    friend DoubleArray imag(const DComplexArray&);

public:

    /****************
     * Constructors *
     ****************/

    DoubleArray();                              // Declares a scalar (0D array)
    DoubleArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    DoubleArray(unsigned,unsigned,unsigned,RWUninitialized);
    DoubleArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    DoubleArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    DoubleArray(unsigned,unsigned,unsigned,RWRand&);
    DoubleArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    DoubleArray(const IntVec& n, double val);
    DoubleArray(unsigned,unsigned,unsigned, double val);
    DoubleArray(unsigned,unsigned,unsigned,unsigned, double val);
    DoubleArray(const char *);
    DoubleArray(const DoubleArray& a);
    DoubleArray(const double* dat, const IntVec& n); // Copy of dat will be made
    DoubleArray(const double* dat, unsigned,unsigned,unsigned);
    DoubleArray(const double* dat, unsigned,unsigned,unsigned,unsigned);
    DoubleArray(const DoubleVec& vec, const IntVec& n); // View of dat will be taken
    DoubleArray(const DoubleVec& vec, unsigned,unsigned,unsigned);
    DoubleArray(const DoubleVec& vec, unsigned,unsigned,unsigned,unsigned);
    DoubleArray(RWBlock *block, const IntVec& n); // Use a custom block for the data
    DoubleArray(RWBlock *block, unsigned,unsigned,unsigned);
    DoubleArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    DoubleArray(const DoubleVec&);              // View will be taken
    DoubleArray(const DoubleGenMat&);           // View will be taken


    /********************
     * Member functions *
     ********************/

    DoubleArray   apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    DoubleArray   apply(XmathFunTy)     const;
#endif  
    unsigned      binaryStoreSize()     const;   // Storage requirements
    DoubleArray   copy()                const;   // Copy with distinct instance variables
    double*       data()                         {return (double*)begin;}
    const double* data()                const    {return (double*)begin;}
    DoubleArray   deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    DoubleArray&  reference(const DoubleArray& v); // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    DoubleArray   slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    double&         operator[](const IntVec& i); // With bounds checking
    double          operator[](const IntVec& i) const;
    double&         operator()(const IntVec& i); // With optional bounds checking
    double          operator()(const IntVec& i) const;
    double&         operator()(int,int,int);
    double          operator()(int,int,int) const;
    double&         operator()(int,int,int,int);
    double          operator()(int,int,int,int) const;
    DoubleVec       operator()(const RWSlice&,int,int);
    const DoubleVec operator()(const RWSlice&,int,int) const;
    DoubleVec       operator()(int,const RWSlice&,int);
    const DoubleVec operator()(int,const RWSlice&,int) const;
    DoubleVec       operator()(int,int,const RWSlice&);
    const DoubleVec operator()(int,int,const RWSlice&) const;
    DoubleVec       operator()(const RWSlice&,int,int,int);
    const DoubleVec operator()(const RWSlice&,int,int,int) const;
    DoubleVec       operator()(int,const RWSlice&,int,int);
    const DoubleVec operator()(int,const RWSlice&,int,int) const;
    DoubleVec       operator()(int,int,const RWSlice&,int);
    const DoubleVec operator()(int,int,const RWSlice&,int) const;
    DoubleVec       operator()(int,int,int,const RWSlice&);
    const DoubleVec operator()(int,int,int,const RWSlice&) const;
    DoubleGenMat    operator()(int,const RWSlice&,const RWSlice&);
    const DoubleGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    DoubleGenMat    operator()(const RWSlice&,int,const RWSlice&);
    const DoubleGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    DoubleGenMat    operator()(const RWSlice&,const RWSlice&,int);
    const DoubleGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    DoubleGenMat    operator()(int,int,const RWSlice&,const RWSlice&);
    const DoubleGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    DoubleGenMat    operator()(int,const RWSlice&,int,const RWSlice&);
    const DoubleGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    DoubleGenMat    operator()(int,const RWSlice&,const RWSlice&,int);
    const DoubleGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    DoubleGenMat    operator()(const RWSlice&,int,int,const RWSlice&);
    const DoubleGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    DoubleGenMat    operator()(const RWSlice&,int,const RWSlice&,int);
    const DoubleGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    DoubleGenMat    operator()(const RWSlice&,const RWSlice&,int,int);
    const DoubleGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    DoubleArray     operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const DoubleArray operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    DoubleArray     operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const DoubleArray operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    DoubleArray     operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const DoubleArray operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    DoubleArray     operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const DoubleArray operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    DoubleArray     operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const DoubleArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    DoubleArray     operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const DoubleArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    DoubleArray& operator=(const DoubleArray& v); // Must be same length as v
    DoubleArray& operator=(double);
    RWBoolean   operator==(const DoubleArray&) const;
    RWBoolean   operator!=(const DoubleArray& v) const;
    DoubleArray& operator+=(double);
    DoubleArray& operator-=(double s)           { return operator+=(-s); }
    DoubleArray& operator*=(double);
    DoubleArray& operator/=(double s)           { return operator*=(1.0/s); }
    DoubleArray& operator+=(const DoubleArray&);
    DoubleArray& operator-=(const DoubleArray&);
    DoubleArray& operator*=(const DoubleArray&);
    DoubleArray& operator/=(const DoubleArray&);
    DoubleArray& operator++();                  // Prefix operator
    DoubleArray& operator--();                  // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

    friend double       toScalar(const DoubleArray&);    // For 0-D arrays
    friend DoubleVec    toVec(const DoubleArray&);       // For 1-D arrays
    friend DoubleGenMat toGenMat(const DoubleArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

           DoubleArray operator-(const DoubleArray&);
    inline DoubleArray operator+(const DoubleArray& v) { return v; }
           DoubleArray operator+(const DoubleArray&, const DoubleArray&);
           DoubleArray operator-(const DoubleArray&, const DoubleArray&);
           DoubleArray operator*(const DoubleArray&, const DoubleArray&);
           DoubleArray operator/(const DoubleArray&, const DoubleArray&);
           DoubleArray operator+(const DoubleArray&,double);
           DoubleArray operator+(double, const DoubleArray&);
           DoubleArray operator-(const DoubleArray&,double);
           DoubleArray operator-(double, const DoubleArray&);
           DoubleArray operator*(const DoubleArray&,double);
           DoubleArray operator*(double, const DoubleArray&);
           DoubleArray operator/(const DoubleArray&,double);
           DoubleArray operator/(double, const DoubleArray&);
           ostream&    operator<<(ostream& s, const DoubleArray& v);
           istream&    operator>>(istream& s, DoubleArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DoubleArray operator*(double s, const DoubleArray& V) {return V*s;}
    inline DoubleArray operator+(double s, const DoubleArray& V) {return V+s;}
    inline DoubleArray operator-(const DoubleArray& V, double s) {return V+(double)(-s);}
    inline DoubleArray operator/(const DoubleArray& V, double s) {return V*(1/s);}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleArray abs(const DoubleArray& V);
           double      dot(const DoubleArray&,const DoubleArray&); // Return sum_ij...k (Aij...k * Bij...k)
           DoubleArray dot(const DoubleVec&,const DoubleArray&); // Return sum_i (Vi * Aij...k)
           DoubleArray dot(const DoubleArray&,const DoubleVec&); // Return sum_k (Ai...jk * Vk)
           IntVec      maxIndex(const DoubleArray&); // Returns *index* of max value
           double      maxValue(const DoubleArray&);
           IntVec      minIndex(const DoubleArray&); // Returns *index* of min value
           double      minValue(const DoubleArray&);
           double      prod(const DoubleArray&);
           double      sum(const DoubleArray&);


    /**** Functions for both real and complex ****/

           DoubleArray cos(const DoubleArray& V);
           DoubleArray cosh(const DoubleArray& V);
           DoubleArray exp(const DoubleArray& V);
           double      mean(const DoubleArray& V);
           DoubleArray pow(const DoubleArray&,const DoubleArray&);
           DoubleArray sin(const DoubleArray& V);
           DoubleArray sinh(const DoubleArray& V);
           DoubleArray sqrt(const DoubleArray& V);
           double      variance(const DoubleArray&);


    /**** Functions for real arrays ****/

           DoubleArray acos(const DoubleArray& V);
           DoubleArray asin(const DoubleArray& V);
           DoubleArray atan(const DoubleArray& V);
           DoubleArray atan2(const DoubleArray&,const DoubleArray&);
           DoubleArray ceil(const DoubleArray& V);
           DoubleArray floor(const DoubleArray& V);
           DoubleArray log(const DoubleArray& V);
           DoubleArray log10(const DoubleArray& V);
           DoubleArray tan(const DoubleArray& V);
           DoubleArray tanh(const DoubleArray& V);


    /**** Functions for type conversion ****/

                                                  // No type conversion functions for DoubleArray

    /**** Norm functions ****/
           Double      frobNorm(const DoubleArray&); // Root of sum of squares
           Double      maxNorm(const DoubleArray&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline double& DoubleArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline double DoubleArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline double& DoubleArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline double DoubleArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline double& DoubleArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline double DoubleArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline double& DoubleArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline double DoubleArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWDARR_H__*/

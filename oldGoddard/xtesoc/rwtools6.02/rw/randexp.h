#ifndef __RWRANDEXP_H__
#define __RWRANDEXP_H__

/*
 * Declarations for RandExp: Exponentially distributed random numbers.
 *
 * $Header: /users/rcs/mathrw/randexp.h,v 1.1 1993/01/23 00:08:40 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randexp.h,v $
 * Revision 1.1  1993/01/23  00:08:40  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:52   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:06   keffer
 * Added pvcs keywords
 *
 */

#include "rw/randunif.h"
STARTWRAP
#include <math.h>
ENDWRAP

class RandExp : public RandUniform {
public:
  RandExp()		: RandUniform()  { }
  RandExp(unsigned n)	: RandUniform(n) { }
 
  double    		randValue() {return -(::log(RandUniform::randValue()));}
  void    		randValue(double*, unsigned n);	// Return an array of random numbers.
  DoubleVec 		randValue(unsigned n); 
  DoubleGenMat 		randValue(unsigned nr, unsigned nc);
};

#endif /*__RWRANDEXP_H__*/

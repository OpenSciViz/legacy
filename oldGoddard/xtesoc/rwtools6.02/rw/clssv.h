#ifndef __RWCLSSV_H__
#define __RWCLSSV_H__
/*
 * DComplexLeastSqSV:  solve least square SV problem with orthogonal decomposition
 *
 * Generated from template $Id: xlssv.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * Uses a singular value decomposition to solve the linear
 * least squares problem.  The solution is the minimum norm solution
 * which minimizes the residual.
 *
 * When building a factorization using either a constructor or one
 * of the factor functions, you can supply a tolerance parameter.
 * Entries along the diagonal of the R factor of the SV decomposition
 * less than this tolerance are treated as zero.  Using this can
 * prevent insignificant entries of R from corrupting your solution.
 *
 * The factor functions and constructors taking a SV decomposition
 * are useful if you'd like to examine the singular values
 * for yourself and decide what tolerance to use.
 */

#include "rw/csv.h"

class DComplexLeastSqSV : public DComplexSVDecomp {
public:
  DComplexLeastSqSV();
  DComplexLeastSqSV(const DComplexGenMat& A, Double tol=0);
  DComplexLeastSqSV(const DComplexSVDecomp& A, Double tol=0);
//void      factor(const DComplexGenMat& A, Double tol=0);
//RWBoolean good() const;
//RWBoolean fail() const;
//unsigned  rows() const {return decomp_.rows();}
//unsigned  cols() const {return decomp_.cols();}
//unsigned  rank() const {return decomp_.rank();}
//void      truncate(Double tol);
  DComplexVec    residual(const DComplexVec& data) const;
  Double       residualNorm(const DComplexVec& data) const;
  DComplexVec    solve(const DComplexVec& data) const;
};

inline DComplexVec solve(   const DComplexLeastSqSV& A, const DComplexVec& b) {return A.solve(b);}
inline DComplexVec residual(const DComplexLeastSqSV& A, const DComplexVec& b) {return A.residual(b);}
inline DComplex    residualNorm(const DComplexLeastSqSV& A, const DComplexVec& b) {return A.residualNorm(b);}

#endif

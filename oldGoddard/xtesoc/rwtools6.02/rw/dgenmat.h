#ifndef __RWDGENMAT_H__
#define __RWDGENMAT_H__

/*
 * Declarations for Double precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/matrix.h"
#include "rw/dvec.h"

class DoubleGenMatPick;
class RWRand;
class DoubleSymMat;             // So that the conversion constructors 
class DoubleSkewMat;            // to matrix.h++ classes parse successfully
class DoubleBandMat;
class DoubleSymBandMat;
class DoubleUpperTriMat;
class DoubleLowerTriMat;
class DoubleTriDiagMat;

class DoubleGenMat : public RWMatView {
private:
    DoubleGenMat(const RWDataView&, double*, unsigned,unsigned, int,int); // For internal use
    friend DoubleGenMat toGenMat(const DoubleArray&);
    friend class DoubleArray;                   // For conversion constructor
    friend DoubleGenMat real(const DComplexGenMat&);
    friend DoubleGenMat imag(const DComplexGenMat&);

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    DoubleVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    DoubleGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    DoubleGenMat() : RWMatView()                {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    DoubleGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(double),s) {}
#endif
    DoubleGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(double),s) {}
    DoubleGenMat(unsigned m, unsigned n, RWRand&);
    DoubleGenMat(unsigned m, unsigned n, double val);
    DoubleGenMat(const char *);
    DoubleGenMat(const DoubleGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    DoubleGenMat(const DoubleGenMatPick& p);
    DoubleGenMat(const double* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    DoubleGenMat(const DoubleVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    DoubleGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data

    DoubleGenMat(const DoubleSymMat& A);        // Conversion to the matrix
    DoubleGenMat(const DoubleSkewMat& A);       // types in matrix.h++.
    DoubleGenMat(const DoubleBandMat& A);
    DoubleGenMat(const DoubleSymBandMat& A);
    DoubleGenMat(const DoubleUpperTriMat& A);
    DoubleGenMat(const DoubleLowerTriMat& A);
    DoubleGenMat(const DoubleTriDiagMat& A);



    /********************
     * Member functions *
     ********************/

    DoubleGenMat apply(mathFunTy)    const;
#ifdef RW_NATIVE_EXTENDED
    DoubleGenMat apply(XmathFunTy)   const;
#endif
    double&    bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, double x);   // set() with bounds checking: for matrix.h++ compatability
    double     bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const DoubleVec col(int)         const;     // Returns a view of a column
    DoubleVec  col(int);                        // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    DoubleGenMat copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    double*    data()                           {return (double*)begin;}
    const double* data()             const      {return (double*)begin;}
    DoubleGenMat deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const DoubleVec diagonal(int =0) const;     // Returns a view of a diagonal
    DoubleVec  diagonal(int =0);                // Returns a view of a diagonal
    DoubleGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const DoubleGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    double&    ref(int i, int j);               // for matrix.h++ compatability
    DoubleGenMat& reference(const DoubleGenMat& v); // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const DoubleVec row(int)         const;     // Returns a view of a row
    DoubleVec  row(int);                        // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, double x);     // for matrix.h++ compatability 
    DoubleVec  slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    DoubleGenMat slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    double     val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=(double)0;}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    double&              operator()(int,int);           
    double               operator()(int,int) const;
    DoubleVec            operator()(int, const RWSlice&);
    const DoubleVec      operator()(int, const RWSlice&)            const;
    DoubleVec            operator()(const RWSlice&, int);
    const DoubleVec      operator()(const RWSlice&, int)            const;
    DoubleGenMat         operator()(const RWSlice&, const RWSlice&);
    const DoubleGenMat   operator()(const RWSlice&, const RWSlice&) const;
    DoubleGenMat& operator=(const DoubleGenMat& v); // Must be same size as v
    DoubleGenMat& operator=(const DoubleGenMatPick&);
    DoubleGenMat& operator=(double);
    RWBoolean   operator==(const DoubleGenMat&) const;
    RWBoolean   operator!=(const DoubleGenMat& v) const;
    DoubleGenMat& operator+=(double);
    DoubleGenMat& operator-=(double s)          { return operator+=(-s); }
    DoubleGenMat& operator*=(double);
    DoubleGenMat& operator/=(double s)          { return operator*=(1.0/s); }
    DoubleGenMat& operator+=(const DoubleGenMat&);
    DoubleGenMat& operator-=(const DoubleGenMat&);
    DoubleGenMat& operator*=(const DoubleGenMat&);
    DoubleGenMat& operator/=(const DoubleGenMat&);
    DoubleGenMat& operator++();                 // Prefix operator
    DoubleGenMat& operator--();                 // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           DoubleGenMat operator-(const DoubleGenMat&);
    inline DoubleGenMat operator+(const DoubleGenMat& v) { return v; }
           DoubleGenMat operator+(const DoubleGenMat&, const DoubleGenMat&);
           DoubleGenMat operator-(const DoubleGenMat&, const DoubleGenMat&);
           DoubleGenMat operator*(const DoubleGenMat&, const DoubleGenMat&);
           DoubleGenMat operator/(const DoubleGenMat&, const DoubleGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline DoubleGenMat operator%(const DoubleGenMat& A, const DoubleGenMat& B) {return product(A,B);}
//  inline DoubleVec   operator%(const DoubleGenMat& A, const DoubleVec& x) {return product(A,x);}
//  inline DoubleVec   operator%(const DoubleVec& x, const DoubleGenMat& A) {return product(x,A);}
           DoubleGenMat operator+(const DoubleGenMat&,double);
           DoubleGenMat operator+(double, const DoubleGenMat&);
           DoubleGenMat operator-(const DoubleGenMat&,double);
           DoubleGenMat operator-(double, const DoubleGenMat&);
           DoubleGenMat operator*(const DoubleGenMat&,double);
           DoubleGenMat operator*(double, const DoubleGenMat&);
           DoubleGenMat operator/(const DoubleGenMat&,double);
           DoubleGenMat operator/(double, const DoubleGenMat&);
           ostream&    operator<<(ostream& s, const DoubleGenMat& v);
           istream&    operator>>(istream& s, DoubleGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DoubleGenMat operator*(double s, const DoubleGenMat& V) {return V*s;}
    inline DoubleGenMat operator+(double s, const DoubleGenMat& V) {return V+s;}
    inline DoubleGenMat operator-(const DoubleGenMat& V, double s) {return V+(double)(-s);}// cast makes microsoft happy when compiling sarr.cpp
    inline DoubleGenMat operator/(const DoubleGenMat& V, double s) {return V*(1/s);}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleGenMat abs(const DoubleGenMat& V);
           double      dot(const DoubleGenMat&,const DoubleGenMat&); // Return sum_ij (Aij*Bij)
           void        maxIndex(const DoubleGenMat&, int *i, int *j);
           void        minIndex(const DoubleGenMat&, int *i, int *j);
           double      maxValue(const DoubleGenMat&);
           double      minValue(const DoubleGenMat&);
           double      prod(const DoubleGenMat&);
           DoubleVec   product(const DoubleGenMat&, const DoubleVec&);
           DoubleVec   product(const DoubleVec&, const DoubleGenMat&);
           DoubleGenMat product(const DoubleGenMat&, const DoubleGenMat&);
           double      sum(const DoubleGenMat&);
           DoubleGenMat transpose(const DoubleGenMat&); // Transpose references this's data
    inline DoubleGenMat adjoint(const DoubleGenMat& A) {return transpose(A);}
           DoubleGenMat transposeProduct(const DoubleGenMat&,const DoubleGenMat&); // For matrix.h++ compatability



    /**** Functions for both real and complex ****/

           DoubleGenMat cos(const DoubleGenMat& V);
           DoubleGenMat cosh(const DoubleGenMat& V);
           DoubleGenMat exp(const DoubleGenMat& V);
           double      mean(const DoubleGenMat& V);
           DoubleGenMat pow(const DoubleGenMat&,const DoubleGenMat&);
           DoubleGenMat sin(const DoubleGenMat& V);
           DoubleGenMat sinh(const DoubleGenMat& V);
           DoubleGenMat sqrt(const DoubleGenMat& V);
           double      variance(const DoubleGenMat&);


    /**** Functions for real matrices ****/

           DoubleGenMat acos(const DoubleGenMat& V);
           DoubleGenMat asin(const DoubleGenMat& V);
           DoubleGenMat atan(const DoubleGenMat& V);
           DoubleGenMat atan2(const DoubleGenMat&,const DoubleGenMat&);
           DoubleGenMat ceil(const DoubleGenMat& V);
           DoubleGenMat floor(const DoubleGenMat& V);
           DoubleGenMat log(const DoubleGenMat& V);
           DoubleGenMat log10(const DoubleGenMat& V);
           DoubleGenMat tan(const DoubleGenMat& V);
           DoubleGenMat tanh(const DoubleGenMat& V);


    /**** Functions for type conversion ****/

                                                  // No type conversion functions for DoubleGenMat

    /**** Norm functions ****/
           Double      l1Norm(const DoubleGenMat&); // Largest column sum
           Double      linfNorm(const DoubleGenMat&); // Largest row sum
           Double      frobNorm(const DoubleGenMat&); // Root of sum of squares
           Double      maxNorm(const DoubleGenMat&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline double& DoubleGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline double DoubleGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline DoubleVec DoubleGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const DoubleVec DoubleGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline DoubleVec DoubleGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const DoubleVec DoubleGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline DoubleGenMat DoubleGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const DoubleGenMat DoubleGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const DoubleVec DoubleGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       DoubleVec DoubleGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const DoubleVec DoubleGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       DoubleVec DoubleGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline double& DoubleGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void DoubleGenMat::set(int i, int j, double x) {
    (*this)(i,j)=x;
  }

  inline double DoubleGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline double& DoubleGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void DoubleGenMat::bcset(int i, int j, double x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline double DoubleGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline DoubleGenMat         operator%(const DoubleGenMat& A, const DoubleGenMat& B) {return product(A,B);}
inline DoubleVec      operator%(const DoubleGenMat& A, const DoubleVec& x) {return product(A,x);}
inline DoubleVec      operator%(const DoubleVec& x, const DoubleGenMat& A) {return product(x,A);}

#endif /* __RWDGENMAT_H__ */

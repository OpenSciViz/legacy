#ifndef __RWFSEIGSRV_H__
#define __RWFSEIGSRV_H__

/*
 * FloatSymEigServer     - Abstract base for symmetric/Hermitian eigenservers
 * FloatSymQREigServer   - QR method, this is the default server
 * FloatSymPDQREigServer - QR method for positive definite matrices
 * FloatSymRFQREigServer - Root free variant of QR (can't compute eigenvectors)
 * FloatSymBisEigServer  - Bisection method
 *
 * Generated from template $Id: xseigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Symmetric/Hermitian eigen-decomposition servers.  This classes
 * represent various algorithms for computing spectral
 * factorizations.  They are useful for people who demand more
 * control over the computation of eigen values/vectors than
 * is provided by the constructors in FloatSymEigDecomp.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/fgenmat.h"
#include "rw/fvec.h"
#include "rw/fsymeig.h"  
class FloatSymTriDiagDecomp;

class FloatSymEigServer {
protected:
  static unsigned&  n(FloatSymEigDecomp& x)           {return x.n;}
  static FloatVec&    lambda(FloatSymEigDecomp& x)      {return x.lambda;}
  static FloatGenMat& P(FloatSymEigDecomp& x)           {return x.P;}
  static RWBoolean& computedAll(FloatSymEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(FloatSymEigDecomp& x)    {return x.accurate;}
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual FloatSymEigDecomp operator()(const FloatSymMat&);
  virtual FloatSymEigDecomp operator()(const FloatSymBandMat&);
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&) =0;
    // The op() which take matrices work by first building a 
    // tri-diagonal decomposition, computing its eigen-decomposition,
    // and then transforming to the original matrix.  The function
    // to decompose a tri diagonal matrix is called decompose() rather
    // than op() to allow its redefinition in subclasses without hiding
    // the op() functions.

  virtual RWBoolean computeEigenVectors() const =0;
    // Is this server configured to compute eigenvectors as well as
    // eigenvalues?  This is nice to know when forming the tri-diagonal
    // decomposition of a matrix, so we can decide whether or not
    // to keep the orthogonal matrix part.
};

class FloatSymQREigServer : public FloatSymEigServer {
private:
  RWBoolean computeVecs_;
public:
  FloatSymQREigServer(RWBoolean computeVecs=TRUE);
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
  
class FloatSymPDQREigServer : public FloatSymEigServer {
private:
  RWBoolean computeVecs_;
public:
  FloatSymPDQREigServer(RWBoolean computeVecs=TRUE);
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
    
class FloatSymRFQREigServer : public FloatSymEigServer {
public:
  FloatSymRFQREigServer();
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
};
                
class FloatSymSomeEigServer  : public FloatSymEigServer {
private:
  RWBoolean computeVecs_;
  Float       tolerance_;
  RWSlice   range;  
public:
  FloatSymSomeEigServer(RWBoolean computeVecs=TRUE);
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&);
  Float               setTolerance(Float);
  RWSlice           setRange(const RWSlice&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

class FloatSymRangeEigServer  : public FloatSymEigServer {
private:
  RWBoolean computeVecs_;
  Float       tolerance_;
  Float       small_;
  Float       large_;
public:
  FloatSymRangeEigServer(RWBoolean computeVecs=TRUE);
  virtual FloatSymEigDecomp   decompose(const FloatSymTriDiagDecomp&);
  Float               setTolerance(Float);
  void              setRange(Float,Float);
  Float               setSmall(Float);
  Float               setLarge(Float);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

#endif
 

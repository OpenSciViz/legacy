#ifndef __RWCEIG_H__
#define __RWCEIG_H__

/*
 * DComplexEigDecomp - Spectral factorization of a symmetric/Hermitian matrix
 *
 * Generated from template $Id: xeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents the eigenvalues/vectors of a non-symmetric
 * matrix.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/deig.h"

class DComplexEigDecomp {
private:
  unsigned  n;           // dimension of the matrix
  DComplexVec    lambda;      // computed eigenvalues (length may be < n)
  DComplexGenMat P;           // columns contain left eigenvectors
  DComplexGenMat Q;           // columns contain right eigenvectors
  RWBoolean computedAll; // Did I compute everything the server tried to compute?
  RWBoolean accurate;    // Were all results as accurate as possible?

public:
  DComplexEigDecomp();
  DComplexEigDecomp(const DoubleEigDecomp&);
  DComplexEigDecomp(const DComplexGenMat&, RWBoolean computeVecs=TRUE);
  void factor(const DComplexGenMat&, RWBoolean computeVecs=TRUE);

  void      operator=(const DComplexEigDecomp&);

  unsigned        cols()           const {return n;}
  DComplex             eigenValue(int)  const;
  const DComplexVec    eigenValues()    const {return lambda;}
  const DComplexVec    leftEigenVector(int)  const;
  const DComplexGenMat leftEigenVectors()    const;
  const DComplexVec    rightEigenVector(int) const;
  const DComplexGenMat rightEigenVectors()   const;
  RWBoolean       good()           const; // True if everything went perfectly
  RWBoolean       inaccurate()     const; // Either failure, or some things are not fully accurate
  RWBoolean       fail()           const; // An eigenvalue or vector wasn't computed
  unsigned        numEigenValues() const {return lambda.length();}
  unsigned        numLeftEigenVectors()  const {return P.cols();}
  unsigned        numRightEigenVectors() const {return Q.cols();}
  unsigned        rows()           const {return n;}
               
  friend class DComplexEigServer;
};

#endif


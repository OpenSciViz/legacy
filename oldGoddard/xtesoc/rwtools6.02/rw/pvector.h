#ifndef __RWPVECTOR_H__
#define __RWPVECTOR_H__

/*
 * RWPtrVector: Vector of pointers
 *
 * $Id: pvector.h,v 1.9 1993/11/04 13:15:09 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: pvector.h,v $
 * Revision 1.9  1993/11/04  13:15:09  jims
 * Add ppp version header
 *
 * Revision 1.8  1993/11/04  12:44:22  jims
 * Port to ObjectStore
 *
 * Revision 1.7  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.6  1993/05/16  21:14:00  keffer
 * Header file "rw/toolerr.h" no longer included.
 *
 * Revision 1.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.4  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.3  1993/03/15  02:28:02  keffer
 * Corrected error in RWVECTOR_DELETE
 *
 * Revision 1.2  1993/03/14  20:47:26  keffer
 * Removed m.f. ref(); return type of data() is now "void* const*".
 *
 * Revision 1.1  1993/02/17  18:27:09  keffer
 * Initial revision
 *
 */


#ifndef __RWTOOLDEFS_H__
#  include "rw/tooldefs.h"
#endif

class RWExport RWPtrVector
{

public:

  RWPtrVector()           : npts_(0), array_(0)        {;}
  RWPtrVector(size_t n) : npts_(n), array_(new RWvoid[n]) {;}
  RWPtrVector(size_t n, void* ival);
  RWPtrVector(const RWPtrVector&);
  ~RWPtrVector()		{ RWVECTOR_DELETE(npts_) array_; }

  RWPtrVector&	operator=(const RWPtrVector&);
  RWPtrVector&	operator=(void* p);               // All elements initialized to p
  void* const*	data() const     {return array_;} // Cannot change vector elements
  size_t	length() const   {return npts_;}
  void		reshape(size_t);
  void		resize(size_t);
  void*&	operator()(size_t i){
#ifdef RWBOUNDS_CHECK
    boundsCheck(i);
#endif
    return array_[i]; }
  void*			operator()(size_t i) const {
#ifdef RWBOUNDS_CHECK
    boundsCheck(i);
#endif
    return array_[i]; }
  void*&		operator[](size_t i)       { boundsCheck(i); return array_[i];}
  void*			operator[](size_t i) const { boundsCheck(i); return array_[i];}

protected:

  void			boundsCheck(size_t i) const;

  size_t		npts_;
  RWvoid*		array_;

};

#endif /* __RWPVECTOR_H__ */

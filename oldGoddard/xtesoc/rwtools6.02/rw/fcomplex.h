#ifndef __RWFCOMPLEX_H__
#define __RWFCOMPLEX_H__

/*
 * Declarations for the Rogue Wave complex class FComplex
 *
 * $Header: /users/rcs/mathrw/fcomplex.h,v 1.4 1993/03/09 15:35:12 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989-1993. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: fcomplex.h,v $
 * Revision 1.4  1993/03/09  15:35:12  alv
 * added full suite of divide operators
 *
 * Revision 1.3  1993/03/08  15:37:30  alv
 * disambiguified DComplex->FComplex converter implementation
 *
 * Revision 1.2  1993/03/03  01:05:15  alv
 * updated for lapack.h++
 *
 * Revision 1.1  1993/01/23  00:08:42  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:56   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:12   keffer
 * Added pvcs keywords
 *
 */

#ifdef __cplusplus

/*
 * I don't like including the double precision complex class here,
 * but it is the only way to get DComplex set for the conversion 
 * functions.  I can't forward declare DComplex since it is a typedef.
 */
#include <rw/dcomplex.h>

class FComplex {
  float  re, im;
public:
  FComplex()                              {re=0; im=0;}
  FComplex(float r)                       {re=r; im=0; }
  FComplex(float r, float i)              {re=r; im=i; }

  // Conversion from and to DComplex
  // Casting necessary to resolve abiguities for complexes that
  // define real/imag only for non-const ref.
  operator DComplex()                     {return DComplex(re,im);}
  FComplex(const DComplex& x)             {re=real(*(DComplex*)&x); im=imag(*(DComplex*)&x);}

  // Following 3 lines should not be necessary, but
  // the class a bug in the Zortech optimizer requires them:
  FComplex(const FComplex& c)             {re=c.re; im=c.im;}
  FComplex& operator=(const FComplex& c)  {re=c.re; im=c.im; return *this;}
  FComplex& operator=(float x)            {re=x;    im=0;    return *this;}

  friend  float    real(const FComplex& a){return a.re;}
  friend  float    imag(const FComplex& a){return a.im;}

  friend  float    abs(const FComplex&);
  friend  float    norm(const FComplex&);
  friend  float    arg(const FComplex&);
  friend  FComplex conj(const FComplex& a){return FComplex(a.re, -a.im);}
  friend  FComplex cos(const FComplex&);
  friend  FComplex cosh(const FComplex&);
  friend  FComplex exp(const FComplex&);
  friend  FComplex log(const FComplex&);
  friend  FComplex pow(float, const FComplex&);
  friend  FComplex pow(const FComplex&, int);
  friend  FComplex pow(const FComplex&, float);
  friend  FComplex pow(const FComplex&, const FComplex&);
  friend  FComplex polar(float, float = 0);
  friend  FComplex sin(const FComplex&);
  friend  FComplex sinh(const FComplex&);
  friend  FComplex sqrt(const FComplex&);

  friend  FComplex operator+(const FComplex& a1, const FComplex& a2) {return FComplex(a1.re+a2.re, a1.im+a2.im);}
  friend  FComplex operator+(float s, const FComplex& a2)            {return FComplex(s+a2.re, a2.im);}
  friend  FComplex operator+(const FComplex& a1, float s)            {return FComplex(a1.re+s, a1.im);}
  friend  FComplex operator-(const FComplex& a)                      {return FComplex(-a.re, -a.im);}
  friend  FComplex operator-(const FComplex& a1, const FComplex& a2) {return FComplex(a1.re-a2.re, a1.im-a2.im);}
  friend  FComplex operator-(float s, const FComplex& a2)            {return FComplex(s-a2.re, -a2.im);}
  friend  FComplex operator-(const FComplex& a1, float s)            {return FComplex(a1.re-s, a1.im);}
  friend  FComplex operator*(const FComplex& a1, const FComplex& a2) {return FComplex(a1.re*a2.re-a1.im*a2.im, a1.re*a2.im+a1.im*a2.re);}
  friend  FComplex operator*(const FComplex& a1, float s)            {return FComplex(s*a1.re, s*a1.im);}
  friend  FComplex operator*(float s, const FComplex& a1)            {return FComplex(s*a1.re, s*a1.im);}
  friend  FComplex operator/(const FComplex& a1, const FComplex& a2);  
  friend  FComplex operator/(const FComplex& a1, float s)            {return FComplex(a1.re/s, a1.im/s);}
  friend  FComplex operator/(float s, const FComplex& a1);
  friend  FComplex operator/(const FComplex& a1, const FComplex& a2);  
  friend  int      operator==(const FComplex& a, const FComplex& b)  {return (a.re==b.re && a.im==b.im);}
  friend  int      operator!=(const FComplex& a, const FComplex& b)  {return (a.re!=b.re || a.im!=b.im);}
  
  void    operator+=(const FComplex& a)  {re+=a.re; im+=a.im;}
  void    operator-=(const FComplex& a)  {re-=a.re; im-=a.im;}
  void    operator*=(const FComplex& a)  {re=re*a.re-im*a.im; im=im*a.re+re*a.im;}
  void    operator/=(const FComplex& a);

  friend  ostream& operator<<(ostream&, const FComplex&);
  friend  istream& operator>>(istream&, const FComplex&);
};

#else  /* not C++ */

typedef struct { float r,i; } FComplex;

#endif

#endif /* __RWFCOMPLEX_H__ */

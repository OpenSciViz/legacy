#ifndef __RWFSBNDMAT_H__
#define __RWFSBNDMAT_H__
/*
 * FloatSymBandMat - A symmetric banded matrix of Floats
 *
 * Stores a symmetric banded matrix.  All entries farther above or below    
 * the diagonal that the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top left
 * corner are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"
#include "rw/fref.h"

extern Float rwFloatZero;	// This constant is sometimes returned by reference

class DoubleSymBandMat;
class FloatGenMat;
class FloatBandMat;

class FloatSymBandMat {
private:

FloatVec vec;
unsigned n;
unsigned bandu;		// The upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
FloatSymBandMat();
FloatSymBandMat( const FloatSymBandMat& );
FloatSymBandMat(unsigned n, unsigned nAgain, unsigned halfWidth);
FloatSymBandMat(const FloatVec& data, unsigned n, unsigned nAgain, unsigned halfWidth);
~FloatSymBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 2*bandu+1; }
inline unsigned halfBandwidth() const  { return bandu; }
inline unsigned lowerBandwidth() const { return bandu; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline ROFloatRef operator()(int i, int j);
inline ROFloatRef ref(int i, int j);
       ROFloatRef bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatSymBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline FloatVec diagonal(int =0) const;
       FloatVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatSymBandMat& operator=(const FloatSymBandMat& m);
FloatSymBandMat& reference(FloatSymBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatSymBandMat copy() const;
FloatSymBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatSymBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned halfWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatSymBandMat& X);
RWBoolean operator!=(const FloatSymBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatSymBandMat& operator+=(const FloatSymBandMat& m);
FloatSymBandMat& operator-=(const FloatSymBandMat& m);
FloatSymBandMat& operator*=(const FloatSymBandMat& m);
FloatSymBandMat& operator*=(Float);
FloatSymBandMat& operator/=(const FloatSymBandMat& m);
FloatSymBandMat& operator/=(Float);
  // assignment operators.  self must be same size as m.


};

FloatSymBandMat operator-(const FloatSymBandMat&);	// Unary minus
FloatSymBandMat operator+(const FloatSymBandMat&);	// Unary plus
FloatSymBandMat operator*(const FloatSymBandMat&, const FloatSymBandMat&);
FloatSymBandMat operator/(const FloatSymBandMat&, const FloatSymBandMat&);
FloatSymBandMat operator+(const FloatSymBandMat&, const FloatSymBandMat&);
FloatSymBandMat operator-(const FloatSymBandMat&, const FloatSymBandMat&);
FloatSymBandMat operator*(const FloatSymBandMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatSymBandMat operator*(Float x, const FloatSymBandMat& A) { return A*x; }
#else
       FloatSymBandMat operator*(Float x, const FloatSymBandMat& A);
#endif
FloatSymBandMat operator/(const FloatSymBandMat& A, Float x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatSymBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatSymBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatSymBandMat transpose(const FloatSymBandMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatSymBandMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatSymBandMat& A);
  // inner products

FloatSymBandMat toFloat(const DoubleSymBandMat& A);
FloatSymBandMat toSymBandMat( const FloatGenMat& A, unsigned halfband );
FloatSymBandMat toSymBandMat( const FloatBandMat& A );

FloatSymBandMat abs(const FloatSymBandMat& A);

inline Float minValue(const FloatSymBandMat& A) { return minValue(A.dataVec()); }
inline Float maxValue(const FloatSymBandMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Float FloatSymBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline Float FloatSymBandMat::set(int i, int j, Float x) {
    return bcset(i,j,x);
}

inline ROFloatRef FloatSymBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline ROFloatRef FloatSymBandMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatSymBandMat::operator()(int i, int j) const { return val(i,j); }

inline FloatVec FloatSymBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
#endif
}

#endif

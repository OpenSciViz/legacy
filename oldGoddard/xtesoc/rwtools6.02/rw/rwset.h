#ifndef  __RWSET_H__
#define  __RWSET_H__

/*
 * Declarations for RWSet --- hash table lookup.
 *
 * $Id: rwset.h,v 2.7 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Duplicates are not allowed.
 * Hash table look up with chaining: derived from RWHashTable
 *
 * $Log: rwset.h,v $
 * Revision 2.7  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.6  1993/07/19  20:45:26  keffer
 * friend classes now use elaborated-type-specifier (ARM Sec. 11.4)
 *
 * Revision 2.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.4  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.3  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.1  1992/11/19  05:45:01  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.6   25 May 1992 15:50:40   KEFFER
 * Optimized, reducing size.
 * 
 *    Rev 1.5   22 May 1992 17:04:16   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.3   29 Apr 1992 14:51:20   KEFFER
 * Hashing now uses chaining to resolve collisions
 *
 */

#include "rw/hashtab.h"
#include "rw/iterator.h"

class RWExport RWSetIterator;

/****************************************************************
 *								*
 *			RWSet					*
 *								*
 ****************************************************************/

class RWExport RWSet : public RWHashTable {

  friend class RWExport RWSetIterator;
  RWDECLARE_COLLECTABLE(RWSet)

public:

  RWSet(size_t N = RWCollection::DEFAULT_CAPACITY);
  RWSet (const RWSet& s) :  RWHashTable(s) { }

  /******************** Member operators ****************************/
  void				operator=(const RWSet& s) { RWHashTable::operator=(s);}
  RWBoolean 			operator<=(const RWSet&) const;
//RWBoolean 			operator==(const RWSet&) const;
//RWBoolean 			operator!=(const RWSet&) const;

  /****************** Virtual member functions *******************/
//virtual void			apply(RWapplyCollectable, void*);
//virtual RWspace		binaryStoreSize() const;
//virtual void			clear();
  virtual void			clearAndDestroy();
//virtual int			compareTo(const RWCollectable*) const;
//virtual RWBoolean		contains(const RWCollectable*) const;
//virtual size_t		entries() const;
//virtual RWCollectable*	find(const RWCollectable*) const;
//virtual unsigned		hash() const;
  virtual RWCollectable*	insert(RWCollectable*);
//virtual RWBoolean		isEmpty() const		{return items==0;}
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual size_t		occurrencesOf(const RWCollectable* a) const;
//virtual RWCollectable*	remove(const RWCollectable*);
//virtual void			removeAndDestroy(const RWCollectable*); 
//virtual void			restoreGuts(RWvistream&);
//virtual void			restoreGuts(RWFile&);
//virtual void			saveGuts(RWvostream&) const;
//virtual void			saveGuts(RWFile&) const;

/********************** Special functions **********************************/
#ifdef RDEBUG
  // Print status of hash table:
  friend ostream&		operator<<(ostream&, const RWSet&);
#endif

protected:

};

/****************************************************************
 *								*
 *			RWSetIterator				*
 *								*
 ****************************************************************/

/*
 * RWSetIterator is an exact clone of RWHashTableIterator --
 */

class RWExport RWSetIterator : public RWHashTableIterator {

public:

  RWSetIterator(RWSet& h) : RWHashTableIterator(h) { }
#ifdef RW_NO_ACCESS_ADJUSTMENT
  void				reset() { RWHashTableIterator::reset(); }
#else
  RWHashTableIterator::reset;
#endif

};

#endif /* __RWSET_H__ */

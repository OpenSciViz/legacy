#ifndef __RWRANDPOIS_H__
#define __RWRANDPOIS_H__

/*
 * Declarations for RandPoisson: Poisson distributed random numbers.
 *
 * $Header: /users/rcs/mathrw/randpois.h,v 1.1 1993/01/23 00:08:41 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randpois.h,v $
 * Revision 1.1  1993/01/23  00:08:41  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:54   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:10   keffer
 * Added pvcs keywords
 *
 */

#include "rw/randunif.h"

class RandPoisson : public RandUniform {
private:		// Private date used by the algorithms.
  double 		g, lmean, sq;
protected:
  double 		p_mean;
public:
  RandPoisson(double m);            
  RandPoisson(unsigned n, double m);
 
  double    		poissonMean() const     {return p_mean;}
  void      		setPoissonMean(double m);

  double    		randValue();           // Return a random number 
  void    		randValue(double*, unsigned n);	// Return an array of random numbers.
  DoubleVec 		randValue(unsigned n); // Return a vector of n random numbers 
  DoubleGenMat		randValue(unsigned nr, unsigned nc);  // Return a matrix of random numbers
};

#endif /*__RWRANDPOIS_H__*/
                                


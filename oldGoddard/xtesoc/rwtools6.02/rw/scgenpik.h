#ifndef __SCGENPIK_H__
#define __SCGENPIK_H__

/*
 * Declarations for SCharGenMatPick --- picks elements out of a SCharGenMat
 *
 * Generated from template $Id: genpik.htm,v 1.3 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The SCharGenMatPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class SCharGenMatPick {
friend class SCharGenMat;
private:
                        SCharGenMatPick(SCharGenMat&,const IntVec&,const IntVec&);
  SCharGenMat&             V;
  const IntVec     rowpick;
  const IntVec     colpick;
protected:         
  void                  assertElements() const;
  void                  lengthCheck(unsigned,unsigned) const;
public:            
  void                  operator=(const SCharGenMat&);
  void                  operator=(const SCharGenMatPick&);
  void                  operator=(SChar);
                   
  unsigned              rows() const    { return rowpick.length(); }
  unsigned              cols() const    { return colpick.length(); }
};

/************************************************
 *                                              *
 *              I N L I N E S                   *
 *                                              *
 ************************************************/

inline
SCharGenMatPick::SCharGenMatPick(SCharGenMat& v, const IntVec& x, const IntVec& y) :
  V(v), rowpick(x), colpick(y)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

#endif /*__SCGENPIK_H__*/

#ifndef __RWBUFPAGE_H__
#define __RWBUFPAGE_H__

/*
 * RWBufferedPageHeap: A virtual page heap accessed through a buffer
 *
 * $Id: bufpage.h,v 2.3 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: bufpage.h,v $
 * Revision 2.3  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.2  1993/03/23  02:45:12  keffer
 * Changed variable names; eliminated int to unsigned conversions.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.0   11 Mar 1992 14:09:58   KEFFER
 * Initial revision.
 */

#include "rw/vpage.h"
STARTWRAP
#include <stddef.h>
ENDWRAP

class RWExport RWBufferedPageHeap : public RWVirtualPageHeap {
  unsigned		nBuffers_;	// Number of buffers (each is pageSize() big)
  RWvoid*		buffers_;	// Points to an array of pointers to buffers
  RWHandle*		handles_;	// Page handle associated with each buffer
  short*		lockCounts_;	// Lock count for each buffer
  unsigned*		age_;		// Age of buffer since accessed
  RWBoolean*		dirty_;		// Whether this buffer has changed since swap in
protected:
  unsigned		ageAndFindHandle(RWHandle);	// Find slot for given handle and age all slots
  unsigned		findHandle(RWHandle);		// Find slot for given handle
  unsigned		findUnusedSlot();		// Find an unused slot
  unsigned		swapPageIn(RWHandle);		// Swap in page with given handle
  unsigned		swapOutLRUSlot();		// Swap out the Least Recently Used page
  virtual RWBoolean	swapIn(RWHandle, void*)  = 0;	// Supplied by specializing class
  virtual RWBoolean	swapOut(RWHandle, void*) = 0;
public:
  RWBufferedPageHeap(unsigned pgsize, unsigned nbufs=10);
  virtual		~RWBufferedPageHeap();

  RWBoolean		isValid()		{return buffers_!=NULL;}

  // Inherited from RWVirtualPageHeap:
  virtual RWHandle	allocate()           = 0;	// Allocate a page
  virtual void		deallocate(RWHandle);		// Deallocate it
  virtual void		dirty(RWHandle);		// Declare page as dirty
  virtual void*		lock(RWHandle);			// Lock a page
  virtual void		unlock(RWHandle);		// Unlock a page
};

#endif	/* __RWBUFPAGE_H__ */

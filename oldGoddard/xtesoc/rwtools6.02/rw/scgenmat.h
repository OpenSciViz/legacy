#ifndef __RWSCGENMAT_H__
#define __RWSCGENMAT_H__

/*
 * Declarations for SChar precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/matrix.h"
#include "rw/scvec.h"

class SCharGenMatPick;
class RWRand;

class SCharGenMat : public RWMatView {
private:
    SCharGenMat(const RWDataView&, SChar*, unsigned,unsigned, int,int); // For internal use
    friend SCharGenMat toGenMat(const SCharArray&);
    friend class SCharArray;                    // For conversion constructor

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    SCharVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    SCharGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    SCharGenMat() : RWMatView()                 {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    SCharGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(SChar),s) {}
#endif
    SCharGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(SChar),s) {}
    SCharGenMat(unsigned m, unsigned n, RWRand&);
    SCharGenMat(unsigned m, unsigned n, SChar val);
    SCharGenMat(const char *);
    SCharGenMat(const SCharGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    SCharGenMat(const SCharGenMatPick& p);
    SCharGenMat(const SChar* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    SCharGenMat(const SCharVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    SCharGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data



    operator IntGenMat() const;                 // Convert to IntGenMat

    /********************
     * Member functions *
     ********************/

    SCharGenMat apply(mathFunTy)     const;
#ifdef RW_NATIVE_EXTENDED
    SCharGenMat apply(XmathFunTy)    const;
#endif
    SChar&     bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, SChar x);    // set() with bounds checking: for matrix.h++ compatability
    SChar      bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const SCharVec col(int)          const;     // Returns a view of a column
    SCharVec   col(int);                        // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    SCharGenMat copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    SChar*     data()                           {return (SChar*)begin;}
    const SChar* data()              const      {return (SChar*)begin;}
    SCharGenMat deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const SCharVec diagonal(int =0)  const;     // Returns a view of a diagonal
    SCharVec   diagonal(int =0);                // Returns a view of a diagonal
    SCharGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const SCharGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    SChar&     ref(int i, int j);               // for matrix.h++ compatability
    SCharGenMat& reference(const SCharGenMat& v); // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const SCharVec row(int)          const;     // Returns a view of a row
    SCharVec   row(int);                        // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, SChar x);      // for matrix.h++ compatability 
    SCharVec   slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    SCharGenMat slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    SChar      val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=(SChar)0;}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    SChar&               operator()(int,int);           
    SChar                operator()(int,int) const;
    SCharVec             operator()(int, const RWSlice&);
    const SCharVec       operator()(int, const RWSlice&)            const;
    SCharVec             operator()(const RWSlice&, int);
    const SCharVec       operator()(const RWSlice&, int)            const;
    SCharGenMat          operator()(const RWSlice&, const RWSlice&);
    const SCharGenMat    operator()(const RWSlice&, const RWSlice&) const;
    SCharGenMat& operator=(const SCharGenMat& v); // Must be same size as v
    SCharGenMat& operator=(const SCharGenMatPick&);
    SCharGenMat& operator=(SChar);
    RWBoolean   operator==(const SCharGenMat&) const;
    RWBoolean   operator!=(const SCharGenMat& v) const;
    SCharGenMat& operator+=(SChar);
    SCharGenMat& operator-=(SChar s)            { return operator+=(-s); }
    SCharGenMat& operator*=(SChar);
    SCharGenMat& operator/=(SChar);
    SCharGenMat& operator+=(const SCharGenMat&);
    SCharGenMat& operator-=(const SCharGenMat&);
    SCharGenMat& operator*=(const SCharGenMat&);
    SCharGenMat& operator/=(const SCharGenMat&);
    SCharGenMat& operator++();                  // Prefix operator
    SCharGenMat& operator--();                  // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           SCharGenMat operator-(const SCharGenMat&);
    inline SCharGenMat operator+(const SCharGenMat& v) { return v; }
           SCharGenMat operator+(const SCharGenMat&, const SCharGenMat&);
           SCharGenMat operator-(const SCharGenMat&, const SCharGenMat&);
           SCharGenMat operator*(const SCharGenMat&, const SCharGenMat&);
           SCharGenMat operator/(const SCharGenMat&, const SCharGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline SCharGenMat operator%(const SCharGenMat& A, const SCharGenMat& B) {return product(A,B);}
//  inline SCharVec    operator%(const SCharGenMat& A, const SCharVec& x) {return product(A,x);}
//  inline SCharVec    operator%(const SCharVec& x, const SCharGenMat& A) {return product(x,A);}
           SCharGenMat operator+(const SCharGenMat&,SChar);
           SCharGenMat operator+(SChar, const SCharGenMat&);
           SCharGenMat operator-(const SCharGenMat&,SChar);
           SCharGenMat operator-(SChar, const SCharGenMat&);
           SCharGenMat operator*(const SCharGenMat&,SChar);
           SCharGenMat operator*(SChar, const SCharGenMat&);
           SCharGenMat operator/(const SCharGenMat&,SChar);
           SCharGenMat operator/(SChar, const SCharGenMat&);
           ostream&    operator<<(ostream& s, const SCharGenMat& v);
           istream&    operator>>(istream& s, SCharGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline SCharGenMat operator*(SChar s, const SCharGenMat& V) {return V*s;}
    inline SCharGenMat operator+(SChar s, const SCharGenMat& V) {return V+s;}
    inline SCharGenMat operator-(const SCharGenMat& V, SChar s) {return V+(SChar)(-s);}// cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           SCharGenMat abs(const SCharGenMat& V);
           SChar       dot(const SCharGenMat&,const SCharGenMat&); // Return sum_ij (Aij*Bij)
           void        maxIndex(const SCharGenMat&, int *i, int *j);
           void        minIndex(const SCharGenMat&, int *i, int *j);
           SChar       maxValue(const SCharGenMat&);
           SChar       minValue(const SCharGenMat&);
           SChar       prod(const SCharGenMat&);
           SCharVec    product(const SCharGenMat&, const SCharVec&);
           SCharVec    product(const SCharVec&, const SCharGenMat&);
           SCharGenMat product(const SCharGenMat&, const SCharGenMat&);
           SChar       sum(const SCharGenMat&);
           SCharGenMat transpose(const SCharGenMat&); // Transpose references this's data
    inline SCharGenMat adjoint(const SCharGenMat& A) {return transpose(A);}
           SCharGenMat transposeProduct(const SCharGenMat&,const SCharGenMat&); // For matrix.h++ compatability



    /**** Functions for type conversion ****/

           SCharGenMat toChar(const IntGenMat&);

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline SChar& SCharGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline SChar SCharGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline SCharVec SCharGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const SCharVec SCharGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline SCharVec SCharGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const SCharVec SCharGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline SCharGenMat SCharGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const SCharGenMat SCharGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const SCharVec SCharGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       SCharVec SCharGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const SCharVec SCharGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       SCharVec SCharGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline SChar& SCharGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void SCharGenMat::set(int i, int j, SChar x) {
    (*this)(i,j)=x;
  }

  inline SChar SCharGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline SChar& SCharGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void SCharGenMat::bcset(int i, int j, SChar x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline SChar SCharGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline SCharGenMat         operator%(const SCharGenMat& A, const SCharGenMat& B) {return product(A,B);}
inline SCharVec      operator%(const SCharGenMat& A, const SCharVec& x) {return product(A,x);}
inline SCharVec      operator%(const SCharVec& x, const SCharGenMat& A) {return product(x,A);}

#endif /* __RWSCGENMAT_H__ */

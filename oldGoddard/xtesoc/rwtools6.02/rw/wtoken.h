#ifndef __RWWTOKEN_H__
#define __RWWTOKEN_H__

/*
 * RWTokenizer --- converts strings into sequences of tokens
 *
 * $Id: wtoken.h,v 1.2 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: wtoken.h,v $
 * Revision 1.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.1  1993/02/11  02:05:23  myersn
 * Initial revision
 *
 */

#include "rw/wstring.h"

class RWExport RWWTokenizer {
  const RWWString*	theString;
  const wchar_t*	place;
public:
  RWWTokenizer(const RWWString& s);	// Construct to lex a string

  // Advance to next token, delimited by s:
  RWWSubString		operator()(const wchar_t* s);
  RWWSubString		operator()(); // { return operator()(L" \t\n"); }
};

#endif /* __RWWTOKEN_H__ */

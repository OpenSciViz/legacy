#ifndef __RWCEIGSRV_H__
#define __RWCEIGSRV_H__

/*
 * DComplexEigServer     - Abstract base for eigenservers
 * DComplexSchurEigServer- Schur decomposition method, this is the default server
 * DComplexHessEigServer - Don't generate Schur decomp - get eigenvectors by inverse iteration
 *
 * Generated from template $Id: xeigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/ivec.h"
#include "rw/ceig.h"  
#include "rw/deig.h"
class DComplexBalanceDecomp;
class DComplexHessenbergDecomp;
class DComplexSchurDecomp;

class DComplexEigServer {
protected:
  static unsigned&  n(DComplexEigDecomp& x)           {return x.n;}
  static DComplexVec&    lambda(DComplexEigDecomp& x)      {return x.lambda;}
  static DComplexGenMat& P(DComplexEigDecomp& x)           {return x.P;}
  static DComplexGenMat& Q(DComplexEigDecomp& x)           {return x.Q;}
  static RWBoolean& computedAll(DComplexEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(DComplexEigDecomp& x)    {return x.accurate;}
  static unsigned&  n(DoubleEigDecomp& x)           {return x.n;}
  static DComplexVec&    lambda(DoubleEigDecomp& x)      {return x.lambda;}
  static DoubleGenMat& P(DoubleEigDecomp& x)           {return x.P;}
  static DoubleGenMat& Q(DoubleEigDecomp& x)           {return x.Q;}
  static RWBoolean& computedAll(DoubleEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(DoubleEigDecomp& x)    {return x.accurate;}
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual DComplexEigDecomp operator()(const DComplexGenMat&) =0;
    // Compute a decomposition.  The subclasses have operator()
    // functions for intermediate forms, like Hessenberg and Schur
    // decompositions.
};

class DComplexSchurEigServer : public DComplexEigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)
public:
  DComplexSchurEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual DComplexEigDecomp operator()(const DComplexGenMat& A);
  virtual DComplexEigDecomp operator()(const DComplexBalanceDecomp& A);
  virtual DComplexEigDecomp operator()(const DComplexHessenbergDecomp& A);
  virtual DComplexEigDecomp operator()(const DComplexSchurDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};
  
class DComplexHessEigServer : public DComplexEigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)]
  DComplexEigDecomp setEigenVals(const DComplexHessenbergDecomp& H);   // Compute eigenvalues
public:
  DComplexHessEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual DComplexEigDecomp operator()(const DComplexGenMat& A);
  virtual DComplexEigDecomp operator()(const DComplexBalanceDecomp& A);
  virtual DComplexEigDecomp operator()(const DComplexHessenbergDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};

#endif

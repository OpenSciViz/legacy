#ifndef __MATHDEFS_H__
#define __MATHDEFS_H__

/*
 * Math.h++ common definitions
 *
 * $Header: /users/rcs/mathrw/mathdefs.h,v 1.16 1993/10/15 05:49:09 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#define RWMATH 0x0512      /* Version number */

#include "rw/defs.h"

/*
 * Include math.h here, so it is included at only one point
 * in the Rogue wave code.  This way we can put the fix for
 * the Borland OS/2 optimizer bug here.                    
 *
 * Don't include math.h if the language is not C++.  The Borland
 * math.h defines a macro cabs(z) which conflicts with the function
 * cabs() defined in mathsrc/f2cincl.h.  This causes problems when
 * compiling c files in the lpakc directory.
 */
#ifdef __cplusplus
STARTWRAP
#  include <math.h>
ENDWRAP
#  if defined(__TURBOC__) && defined(__OS2__)
#    undef sin
#    undef cos
#  endif
#endif
 
/*********************************************************************

                U S E R   T U N A B L E   S E C T I O N 

 *********************************************************************/

/*
 * This section has various parameters that you might 
 * have to set depending on your compiler and operating system.
 * Be sure to take a look at the user-tunable section in compiler.h as well.
 */

/*
 * Uncomment this line to allow vector, matrix, and array constructors
 * which take only the dimensions and no initializer.  This is provided
 * for backward compatability.  In the new version of math.h++, these
 * constructors have been replaced by allowing rwUninitialized as an
 * initializer.  This cuts down un ambiguous and potentially unwanted
 * type conversions.
 */

/* #define RW_ALLOW_OLD_ARRAY_CONSTRUCTORS */

/*
 * Uncomment the following line if your compiler has "restricted" pointers.
 * This is relatively rare.  If you do not know what restricted pointers are,
 * you probably should leave this commented.
 */

/* #define HAS_RESTRICT 1 */

/*
 * Uncomment the following if your -> C <- compiler has "native" complex;
 * that is, complex is a type built directly into the C compiler, 
 * NOT implemented as a separate class by a C++ compiler.  (This is a NCEG extension)
 */

/* #define HAS_NATIVE_COMPLEX 1 */

/*
 * There are two issues involved in determining how to set the following
 * two macros: (1) What calling convention to use when calling an
 * external function and (2) what linkage to use.
 *
 * Set to whatever your compiler requires for "Fortran" linkage, using the
 * extern "linkage" { } type construct.
 * For most compilers "C" linkage will work fine.
 */
#define FLinkage "C"

/*
 * Set to whatever your compiler requires for the Fortran calling
 * convention.  Usually, this is the same as the "pascal" convention.
 * For Borland, Turbo, Zortech, Glockenspiel you need do nothing.
 * For all other compilers, if you do nothing everything will still work, 
 * you just won't be able to link with an external BLA package.
 */

/* #define FDecl pascal */

/*
 * Set the linkage and calling conventions for the bla source
 * and the lapack source.
 *
 * extern "C" is useful for debugging, or for linking with externally
 * defined blas.  Once Borland has the bugs out, _fastcall will be a
 * good one for speed, especially for the blas.
 *
 * If this is NT, we can't use "C" linkage because the Microsoft NT
 * compiler balks at "C" functions which return a C++ class object
 * (eg a DComplex object).
 */

#if defined(_MSC_VER) && defined(__WIN32__)
#define RWBLADECL
#define RWLAPKDECL
#else
#define RWBLADECL extern "C"
#define RWLAPKDECL extern "C"
#endif

/*
 * Define TRUE if sizeof(Complex)==2*sizeof(double);
 * This is true for all known compilers:
 */
#define COMPLEX_PACKS 1

/* 
 * Uncomment the following if your complex data goes (imaginary, real),
 * rather than the more usual (real, imaginary).  This setting is ignored unless
 * COMPLEX_PACKS is also defined.
 */
/* #define IMAG_LEADS 1 */

/*
 * Uncomment the following to insist that the Rogue Wave defined DComplex
 * class be used, rather than the native compiler complex class.
 */
/* #define RW_USE_RW_COMPLEX 1 */

/*
 * Uncomment the following if your operating system supports memory
 * mapped files.  This will allow you to set up arrays whose data 
 * blocks are memory mapped files - useful when looking at very large
 * files when you only need to access some of the data in them.
 */
/* #define RW_HAS_MAPPED_FILES */


/*************************************************************************
**************************************************************************
**                                                                      **
**              From here on, it's pretty much boilerplate              **
**              and rarely requires any tuning.                         **
**                                                                      **
**************************************************************************
**************************************************************************/

/* The Cray has restricted pointers and native complex: */

#ifdef _CRAY
#  define HAS_RESTRICT       1
#  define HAS_NATIVE_COMPLEX 1
#endif

/* Disable restricted pointers if the compiler doesn't support them: */
#ifndef HAS_RESTRICT
#  define restrict
#endif

#ifndef FLinkage
#  define FLinkage "C"
#endif

#ifndef FDecl
#  if defined(__ZTC__) || defined(MSC_BACKEND)
#    define FDecl _pascal
#  else
#    ifdef __TURBOC__
#      define FDecl pascal
#    else
#      define FDecl     /* Use the default (usually C) calling convention */
#    endif
#  endif
#endif

#ifdef __cplusplus

/* ZTC 2.0 needs the "extern C" wrapper: */
#if defined(__ZTC__)  && defined (__cplusplus)
  extern "C" { 
    typedef double              (*mathFunTy)(double);
  }
#else
#  if defined(__GNUG__) && defined(__HAVE_68881__)
    /* GNU 68881 inline functions require this: */
    typedef const double        (*mathFunTy)(double);
#  else
    typedef double              (*mathFunTy)(double);
#  endif
#endif

#ifdef RW_NATIVE_EXTENDED
   typedef extended             (*XmathFunTy)(extended);
#endif

/*
 * sun and AIX have memory mapped files using mmap()
 */
#if defined(sun) || defined(_AIX)
#define RW_HAS_MAPPED_FILES
#endif


class DComplexVec;
class DoubleVec;
class FloatVec;
class IntVec;
class SCharVec;
class UCharVec;
class DComplexGenMat;
class DoubleGenMat;
class FloatGenMat;
class IntGenMat;
class SCharGenMat;
class UCharGenMat;
class DComplexArray;
class DoubleArray;
class FloatArray;
class IntArray;
class SCharArray;
class UCharArray;
#endif  /* __cplusplus */

#if defined(__ATT1__)
   /* Bug in ATT1 does not allow typedefs: */
#  define Double        double
#  define Float         float
#  define Int           int
#  define Char          char
#else
   typedef double       Double;
   typedef float        Float;
   typedef int          Int;
   typedef char         Char;
#endif

typedef unsigned char   UChar; 
#if defined(RW_NO_SCHAR) || ( !defined(__cplusplus) && !defined(__HIGHC__) && !defined(_AIX) && !defined(_CRAY) )
  typedef char          SChar;
#else
  typedef signed char   SChar;   
#endif


/*
 *  Class ID definitions
 */

#define __CGEMATRIX                     0x9001
#define __DCOMPLEXGENMATPICK            0x9002
#define __CLUDECOMP                     0x9003
#define __DCOMPLEX                      0x9004
#define __DCOMPLEXFFT2DSERVER           0x9004
#define __DCOMPLEXFFTSERVER             0x9005
#define __DCOMPLEXVECPICK               0x9006
#define __DCOMPLEXVEC                   0x9007
#define __DGEMATRIX                     0x9008
#define __DOUBLEGENMATPICK              0x9009
#define __DLUDECOMP                     0x900a
#define __DOUBLECOSINESERVER            0x900b
#define __DOUBLEFFTSERVER               0x900c
#define __DOUBLEVECPICK                 0x900d
#define __DOUBLEVEC                     0x900e
#define __FGEMATRIX                     0x900f
#define __FLOATGENMATPICK               0x9010
#define __FLUDECOMP                     0x9011
#define __FLOATVECPICK                  0x9012
#define __FLOATVEC                      0x9013
#define __HISTOGRAM                     0x9014
#define __IGEMATRIX                     0x9015
#define __INTGENMATPICK                 0x9016
#define __INTVECPICK                    0x9017
#define __INTVEC                        0x9018
#define __LEASTSQFIT                    0x9019
#define __RWBLOCK                       0x901a
#define __RANDBINOMIAL                  0x901b
#define __RANDEXP                       0x901c
#define __RANDGAMMA                     0x901d
#define __RANDNORMAL                    0x901e
#define __RANDPOISSON                   0x901f
#define __RANDUNIFORM                   0x9020
#define __SCGEMATRIX                    0x9021
#define __SCHARGENMATPICK               0x9022
#define __SCHARVECPICK                  0x9023
#define __SCHARVEC                      0x9024
#define __UCGEMATRIX                    0x9025
#define __UCHARGENMATPICK               0x9026
#define __UCHARVECPICK                  0x9027
#define __UCHARVEC                      0x9028

#define __DCOMPLEXARRAY                 0x9029
#define __DOUBLEARRAY                   0x902a
#define __FLOATARRAY                    0x902b
#define __INTARRAY                      0x902c
#define __SCHARARRAY                    0x902d
#define __UCHARARRAY                    0x902e

/*
 * xGEMATRIX has been renamed
 */

#define __DCOMPLEXGENMAT __CGEMATRIX
#define __DOUBLEGENMAT   __DGEMATRIX
#define __FLOATGENMAT    __FGEMATRIX
#define __INTGENMAT      __IGEMATRIX
#define __SCHARGENMAT    __SCGEMATRIX
#define __UCHARGENMAT    __UCGEMATRIX

/*
 * $Log: mathdefs.h,v $
 * Revision 1.16  1993/10/15  05:49:09  alv
 * changed RWBLADECL and RWLAPKDECL for NT
 *
 * Revision 1.15  1993/09/19  23:14:10  alv
 * updated RWMATH to 510
 *
 * Revision 1.14  1993/09/01  16:15:35  alv
 * Uses new 'rwUninitialized' form for simple constructor
 *
 * Revision 1.13  1993/08/17  17:11:05  alv
 * added ability to use custom RWBlocks
 *
 * Revision 1.12  1993/08/05  23:26:10  griswolf
 * Fix typo introduced with Cray changes.
 *
 * Revision 1.11  1993/08/05  21:20:13  dealys
 * ported to MPW C++ 3.3
 *
 * Revision 1.10  1993/06/23  19:54:15  alv
 * fix for cray: they also have chars which default to unsigned
 *
 * Revision 1.9  1993/03/19  22:51:19  alv
 * added a '1' to a before emtyp #define
 *
 * Revision 1.8  1993/03/19  22:40:49  alv
 * moved complex tunables into mathdefs.h
 *
 * Revision 1.7  1993/03/19  16:10:37  alv
 * added RWLAPKDECL and RWBLADECL
 *
 * Revision 1.6  1993/03/18  21:56:32  alv
 * Now cc on RS6000 takes an SChar to be "signed char", not "char"
 * since it uses unsigned char by default
 *
 * Revision 1.5  1993/03/01  17:53:00  alv
 * ported to Metaware High C++
 *
 * Revision 1.4  1993/01/29  19:57:41  alv
 * SChar is now "signed char" unless RW_NO_SCHAR is set, or not C++
 *
 * Revision 1.3  1993/01/27  19:31:34  alv
 * added RWMATH version number
 *
 * Revision 1.2  1993/01/26  17:08:53  alv
 * Removed a nested comment to quiet sun warnings
 *
 * Revision 1.1  1993/01/23  00:08:37  alv
 * Initial revision
 *
 * 
 *    Rev 1.7   18 Mar 1992 13:19:18   KEFFER
 * 
 *    Rev 1.6   17 Oct 1991 09:12:50   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.4   02 Aug 1991 13:35:54   keffer
 * Removed declarations for the obsolete RWstoreID() and RWreadID()
 * 
 *    Rev 1.3   27 Jul 1991 21:26:32   keffer
 * Ported to System V.
 * 
 *    Rev 1.2   26 Jul 1991 19:52:02   keffer
 * Removed #include <stddef.h> which is already done in defs.h
 * 
 *    Rev 1.1   24 Jul 1991 13:01:02   keffer
 * Added pvcs keywords
 *
 */

#endif /* __MATHDEFS_H__ */


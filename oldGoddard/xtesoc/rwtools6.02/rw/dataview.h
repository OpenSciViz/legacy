#ifndef __RWDATAVIEW_H__
#define __RWDATAVIEW_H__
/* 
 * Data block classes and base view class
 *
 * This is the base class for vector, matrix, and array view 
 * classes.  The classes used to represent data blocks are
 * also declared here.
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * The RWBlock hierarchy of classes provide a handle to a data block.
 * Each data block has associated with it one RWBlock object.  The 
 * RWBlock object provides reference counting on the data block and
 * also is responsible for allocating and cleaning up the memory in
 * the data block.  This allocation and de-allocation occurs in the
 * constructor and destructor of the RWBlock.  The base class in the
 * hierarchy, RWBlock, does no allocation of memory or cleaning up.
 * This allows you to use memory allocated from some other source with
 * Math.h++.  The derived class RWNewBlock is used in most circumstances;
 * it allocates memory off the heap using new, and restores the memory
 * using delete.  The class RWMappedBlock provides a way to access
 * large arrays via memory mapped files.  You can create your own memory
 * management schemes by deriving new classes from RWBlock.
 *
 * The RWDataView class collects together things common to
 * all classes which view a data block (ie vectors, matrices,
 * arrays).
 */

#include <stdlib.h>      /* looking for size_t */
#include "rw/mathdefs.h"
#include "rw/ref.h"

class RWBlock : public RWReference {          // An unmanaged block of data
private:
  void*          data_;    // The data
  unsigned       len_;     // The number of bytes of data
protected:
  RWBlock() {}                           // This ctor and init are for derived class constructors
  void init(void *data, unsigned len)    {data_=data; len_=len;}
public:
  RWBlock(void *data, unsigned len)      {data_=data; len_=len;}
  RWBlock(const RWBlock&);               // reference count gets set to zero
  RWBlock(RWReference::RWReferenceFlag); // special constructor for statically initialized RWBlocks
  virtual  ~RWBlock()                    {}
  void     *data()  const                {return data_;}
  unsigned length() const                {return len_;}   // length in bytes
};

class RWNewBlock : public RWBlock {           // A data block on the heap
public:
  RWNewBlock(unsigned nelem, size_t elemsize);
  ~RWNewBlock();
};

#if defined(RW_HAS_MAPPED_FILES)
class RWMappedBlock : public RWBlock {        // Memory mapped file data block
public:
  RWMappedBlock(const char *filename, size_t offset=0);        // mode defaults to O_RDONLY (not using default arg avoids including fcntl.h)
  RWMappedBlock(const char *filename, size_t offset, int mode);
  ~RWMappedBlock();
  RWBoolean good() const             {return data()!=0;}
  RWBoolean fail() const             {return data()==0;}
};
#endif

class RWDataView {
private:
  RWBlock      *block;                            // The data block which is being viewed
protected:
  void         *begin;                            // Beginning of the data being viewed
                                
  RWDataView();                                   // Sets block to nilblock
  RWDataView(RWBlock *block);                     // Use any block you like
  RWDataView(unsigned n, size_t s);               // Uses RWNewBlock
  RWDataView(const RWDataView& x);
  RWDataView(const RWDataView& v, void *b);       // Let's you refer to offsets from a block
  ~RWDataView();

  void         reference(const RWDataView&);

public:  // These functions need to be public so that they can be called by global functions
  static void  versionErr(int,int);                 // Error: version number wrong in restoreFrom

public:
/*** Do not change the enum values: Cfront 2.0 needs them in other files! ***/
  enum Storage {COLUMN_MAJOR=0, ROW_MAJOR=1, RWEITHER=2}; // RWEITHER is for internal use

  RWBoolean    sameDataBlock(const RWDataView& x) {return block==x.block;}
  RWBoolean    isSimpleView();                    // True if there is only 1 ref and begin points to block beginning
};  

/*
 * The following routines can be used to log allocation and
 * deallocation of data blocks for debugging.  See the large
 * comment in dataview.cpp for details.  Note that this is
 * primarily intended for debugging Rogue Wave code internally,
 * not for debugging user level code.
 */

class ostream;
ostream*      rwblockSetLogStream(ostream* s);
void          rwblockReport(ostream&);
unsigned long rwblockBytesAllocated();
unsigned      rwblockBlocksAllocated();

#endif /* __RWDATAVIEW_H__ */

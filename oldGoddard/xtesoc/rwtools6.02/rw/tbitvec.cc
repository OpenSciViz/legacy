/*
 * Template definitions for RWTBitVec<N>
 *
 * $Id: tbitvec.cc,v 1.6 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: tbitvec.cc,v $
 * Revision 1.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 1.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 1.4  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 1.3  1993/02/12  20:19:03  keffer
 * Ported to g++ v2.3
 *
 * Revision 1.2  1993/02/06  22:13:47  keffer
 * Ported to cfront V3.0
 *
 * Revision 1.1  1993/01/27  21:38:32  keffer
 * Initial revision
 *
 *
 ***************************************************************************
 */

// Neither cfront v3.0 nor Gnu g++ can handle template static data;
// Borland and Metaware can.

#if defined(RW_BROKEN_TEMPLATES) || defined(__GNUC__)
# define nfull_ ((size_t)N>>3)
# define mask_ ((RWByte)((1 << ((size_t)N&7)) - 1))
#else
  template <int N> const size_t RWTBitVec<N>::nfull_ = (size_t)N >> 3;
  template <int N> const RWByte   RWTBitVec<N>::mask_  = (1 << ((size_t)N&7)) - 1;
#endif

template <int N> RWBoolean
RWTBitVec<N>::operator==(RWBoolean b) const
{
  register RWByte val = b ? ~0 : 0;

  // Check the full bytes:
  for (register size_t i=0; i<nfull_; i++) if (vec_[i] != val) return FALSE;
  
  // Check the last (partially full) byte, if any:
  return (nfull_==sizeof(vec_)) ? TRUE : (val & mask_) == (vec_[nfull_] & mask_);
}

template <int N> RWBoolean
RWTBitVec<N>::operator==(const RWTBitVec<N>& u) const
{
  // Check the full bytes:
  for (register size_t i=0; i<nfull_; i++)
    if (vec_[i] != u.vec_[i])
      return FALSE;

  // Check the last (partially full) byte, if any:
  return (nfull_==sizeof(vec_)) ? TRUE : (vec_[nfull_] & mask_) == (u.vec_[nfull_] & mask_);
}

#if defined(RW_BROKEN_TEMPLATES) || defined(__GNUC__)
# undef nfull_
# undef mask_
#endif

#ifndef __RWFSKEWMAT_H__
#define __RWFSKEWMAT_H__
/*
 * FloatSkewMat - An skew symmetric matrix of Floats
 *
 * Stores a skew symmetric matrix.  Note that only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"
#include "rw/fref.h"


class DoubleSkewMat;
class FloatGenMat;
class FloatSymMat;

class FloatSkewMat {
private:

FloatVec vec;
unsigned n;
  // The data which define the matrix


public:
FloatSkewMat();
FloatSkewMat( const FloatSkewMat& );
FloatSkewMat(unsigned n, unsigned nAgain);
FloatSkewMat(const FloatVec& data, unsigned n, unsigned nAgain);
~FloatSkewMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline NGFloatRef operator()(int i, int j);
inline NGFloatRef ref(int i, int j);
       NGFloatRef bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatSkewMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatSkewMat& operator=(const FloatSkewMat& m);
FloatSkewMat& reference(FloatSkewMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatSkewMat copy() const;
FloatSkewMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatSkewMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatSkewMat& X);
RWBoolean operator!=(const FloatSkewMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatSkewMat& operator+=(const FloatSkewMat& m);
FloatSkewMat& operator-=(const FloatSkewMat& m);
FloatSkewMat& operator*=(const FloatSkewMat& m);
FloatSkewMat& operator*=(Float);
FloatSkewMat& operator/=(const FloatSkewMat& m);
FloatSkewMat& operator/=(Float);
  // assignment operators.  self must be same size as m.


};

FloatSkewMat operator-(const FloatSkewMat&);	// Unary minus
FloatSkewMat operator+(const FloatSkewMat&);	// Unary plus
FloatSkewMat operator*(const FloatSkewMat&, const FloatSkewMat&);
FloatSkewMat operator/(const FloatSkewMat&, const FloatSkewMat&);
FloatSkewMat operator+(const FloatSkewMat&, const FloatSkewMat&);
FloatSkewMat operator-(const FloatSkewMat&, const FloatSkewMat&);
FloatSkewMat operator*(const FloatSkewMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatSkewMat operator*(Float x, const FloatSkewMat& A) { return A*x; }
#else
       FloatSkewMat operator*(Float x, const FloatSkewMat& A);
#endif
FloatSkewMat operator/(const FloatSkewMat& A, Float x);
FloatSkewMat operator/(Float x, const FloatSkewMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatSkewMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatSkewMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatSkewMat transpose(const FloatSkewMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatSkewMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatSkewMat& A);
  // inner products

FloatSkewMat toFloat(const DoubleSkewMat& A);
FloatSkewMat toSkewMat( const FloatGenMat& A, RWBoolean keepMainDiag = TRUE );

FloatSymMat abs(const FloatSkewMat& A);



/*
 * Inline functions
 */

inline Float FloatSkewMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    Float val = vec(index);
    return (i<=j) ?  val : -val;
#endif
}

inline Float FloatSkewMat::set(int i, int j, Float x) {
    return bcset(i,j,x);
}

inline NGFloatRef FloatSkewMat::ref(int i, int j) {
    return bcref(i,j);
}

inline NGFloatRef FloatSkewMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatSkewMat::operator()(int i, int j) const { return val(i,j); }


#endif

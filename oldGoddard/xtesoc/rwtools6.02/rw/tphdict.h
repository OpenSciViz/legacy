#ifndef __RWTPHDICT_H__
#define __RWTPHDICT_H__

/*
 * RWTPtrHashDictionary: A parameterized hashing dictionary using pointers
 *                       to keys of type K and pointers to values of type V.
 *
 * $Id: tphdict.h,v 2.9 1993/11/08 12:17:42 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Assumes that KP has:
 *
 *   - well-defined equality semantics (KP::operator==(const KP&)).
 *
 ***************************************************************************
 *
 * $Log: tphdict.h,v $
 * Revision 2.9  1993/11/08  12:17:42  jims
 * Port to ObjectStore
 *
 * Revision 2.8  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.7  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.6  1993/03/24  01:29:51  keffer
 * Indexing operations now used unsigned
 *
 * Revision 2.5  1993/02/17  19:07:23  keffer
 * Now uses RWTPtrVector<T> instead of RWTValVector<T> for the buckets
 *
 * Revision 2.4  1993/02/12  00:59:40  keffer
 * Ported to the IBM xlC compiler
 *
 * Revision 2.3  1993/02/07  22:32:54  keffer
 * Added copy constructor and assignment operator for iterator
 *
 * Revision 2.2  1993/01/29  20:02:12  keffer
 * Ported to cfront V3.0
 *
 *    Rev 1.0   02 Mar 1992 16:10:54   KEFFER
 * Initial revision.
 */

#include "rw/tpsldict.h"
#include "rw/tpvector.h"

template <class KP, class VP> class RWExport RWTPtrHashDictionaryIterator;

template <class KP, class VP> class RWExport RWTPtrHashDictionary
{

public:

  // Constructors:
  RWTPtrHashDictionary
  (
    unsigned (*hashKey)(const KP&),	// Hashing function
    size_t size = RWDEFAULT_CAPACITY	// No. of buckets
  );

  RWTPtrHashDictionary
  (
    const RWTPtrHashDictionary<KP,VP>&
  );

  ~RWTPtrHashDictionary()
  {
    clear();
  }

  // Operators:
  RWTPtrHashDictionary<KP,VP> &	operator=(const RWTPtrHashDictionary<KP,VP>&);

  // Look up key, add if not there:
  VP*&			operator[](KP* key);

  // Member functions:
  void			applyToKeyAndValue(void (*applyFun)(KP*,VP*&,void*), void*);

  void			clear();
  void			clearAndDestroy();

  RWBoolean		contains(KP*) const;	// Contain key?

  size_t		entries() const		{return nitems_;}

  RWBoolean		isEmpty() const		{return nitems_==0;}

  KP*			find(KP* key) const;
  VP*			findValue(KP* key) const;
  KP*			findKeyAndValue(KP* key, VP*& retVal) const;

  void			insertKeyAndValue(KP* key, VP* value)
	{ (*this)[key] = value;}

  KP*			remove(KP* k);

  void			resize(size_t); // Change # of buckets

protected:

  size_t		hashIndex(KP* val) const
	{ return (size_t)(*hashFun_)(*val) % table_.length(); }

  RWTPtrVector<RWTPtrSlistDictionary<KP,VP> > table_;
  size_t		nitems_;
  unsigned		(*hashFun_)(const KP&);

private:

friend class RWTPtrHashDictionaryIterator<KP,VP>;

};

/****************************************************************
 *								*
 *	Declarations for RWTPtrHashDictionaryIterator<KP,VP>	*
 *								*
 ****************************************************************/

template <class KP, class VP>
class RWExport RWTPtrHashDictionaryIterator
{

public:

  RWTPtrHashDictionaryIterator(RWTPtrHashDictionary<KP,VP>& s);
  RWTPtrHashDictionaryIterator(const RWTPtrHashDictionaryIterator<KP,VP>& h);
  ~RWTPtrHashDictionaryIterator() {delete iterator_;}

  RWTPtrHashDictionaryIterator<KP,VP>&
		operator=(const RWTPtrHashDictionaryIterator<KP,VP>& h);

  RWBoolean		operator++();		// Advance and test
  KP*			operator()()
    { return ++(*this)? iterator_->key() : rwnil; }

  RWTPtrHashDictionary<KP,VP>* container() const	{return myDict_;}

  KP*			key() const		{return iterator_->key();}

  void			reset();
  void			reset(RWTPtrHashDictionary<KP,VP>& s);

  VP*			value() const		{return iterator_->value();}

private:

  RWTPtrHashDictionary<KP,VP>* myDict_;
  size_t idx_;
  RWTPtrSlistDictionaryIterator<KP,VP>*	iterator_;

  void			nextChain();		// Advance to the next chain

  // Disallow postfix increment.  Unless we hide it, some compilers will
  // substitute the prefix increment operator in its place.
  RWBoolean		operator++(int);

};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tphdict.cc"
#endif

#endif	/* __RWTPHDICT_H__ */


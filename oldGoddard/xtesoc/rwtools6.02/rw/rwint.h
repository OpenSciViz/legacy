#ifndef __RWINT_H__
#define __RWINT_H__

/*
 * Declarations for an integer class.
 *
 * $Id: rwint.h,v 2.7 1993/09/10 02:03:24 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwint.h,v $
 * Revision 2.7  1993/09/10  02:03:24  keffer
 * Added explicit copy constructors and assignment operators to
 * support g++ which otherwise generates non-optimized versions.
 *
 * Revision 2.6  1993/06/06  00:35:01  keffer
 * operator<<(ostream&, const RWInteger&) is no longer a friend.
 *
 * Revision 2.5  1993/04/09  02:50:00  keffer
 * Added support for operator<< and >> for virtual streams and RWFile.
 *
 * Revision 2.4  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.3  1993/03/13  05:32:27  keffer
 * ZTC_TYPE_CONVERSION_BUG->RW_ZTC_TYPE_CONVERSION_BUG
 *
 *    Rev 1.1   28 Oct 1991 09:08:20   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:16:28   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/tooldefs.h"
#include "rw/rstream.h"

class RWExport RWInteger{
  int 			intBase;
public:
  RWInteger(int i=0) 	{intBase = i;}

  // The following two should not be necessary, but are required to
  // support feeble compilers:
  RWInteger(const RWInteger& i) : intBase(i.intBase) {;}
  RWInteger& operator=(const RWInteger& i) {intBase=i.intBase; return *this;}

  RWBoolean		operator==(const RWInteger& i) const {return intBase==i.intBase;}

#ifndef RW_ZTC_TYPE_CONVERSION_BUG
  operator		int() const {return intBase;}
#endif
  RWspace		binaryStoreSize() const		{return sizeof(int);}
  void			restoreFrom(RWvistream& s);
  void			restoreFrom(RWFile& f);
  void			saveOn(RWvostream& s) const;
  void			saveOn(RWFile& f) const;

  int			value() const {return intBase;}
  int			value(int newval) {int temp=intBase; intBase=newval; return temp;}

friend istream& operator>>(istream& i, RWInteger& x)
		{ return i >> x.intBase;}
};

inline ostream& operator<<(ostream& o, const RWInteger& x)
		{ return o << x.value(); }
inline RWvistream& operator>>(RWvistream& str,       RWInteger& x)
		{ x.restoreFrom(str);  return str;  }
inline RWFile&     operator>>(RWFile& file,          RWInteger& x)
		{ x.restoreFrom(file); return file; }
inline RWvostream& operator<<(RWvostream& str, const RWInteger& x)
		{ x.saveOn(str);       return str;  }
inline RWFile&     operator<<(RWFile& file,    const RWInteger& x)
		{ x.saveOn(file);      return file; }

#endif /* __RWINT_H__ */

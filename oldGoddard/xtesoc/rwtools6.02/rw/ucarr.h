#ifndef __RWUCARR_H__
#define __RWUCARR_H__

/*
 * Declarations for UChar precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a UChar or UChar&.
 */

#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class UCharArray : public RWArrayView {
private:
    UCharArray(const RWDataView&, UChar*, const IntVec&, const IntVec&); // For real() and imag() and slices

public:

    /****************
     * Constructors *
     ****************/

    UCharArray();                               // Declares a scalar (0D array)
    UCharArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    UCharArray(unsigned,unsigned,unsigned,RWUninitialized);
    UCharArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    UCharArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    UCharArray(unsigned,unsigned,unsigned,RWRand&);
    UCharArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    UCharArray(const IntVec& n, UChar val);
    UCharArray(unsigned,unsigned,unsigned, UChar val);
    UCharArray(unsigned,unsigned,unsigned,unsigned, UChar val);
    UCharArray(const char *);
    UCharArray(const UCharArray& a);
    UCharArray(const UChar* dat, const IntVec& n); // Copy of dat will be made
    UCharArray(const UChar* dat, unsigned,unsigned,unsigned);
    UCharArray(const UChar* dat, unsigned,unsigned,unsigned,unsigned);
    UCharArray(const UCharVec& vec, const IntVec& n); // View of dat will be taken
    UCharArray(const UCharVec& vec, unsigned,unsigned,unsigned);
    UCharArray(const UCharVec& vec, unsigned,unsigned,unsigned,unsigned);
    UCharArray(RWBlock *block, const IntVec& n); // Use a custom block for the data
    UCharArray(RWBlock *block, unsigned,unsigned,unsigned);
    UCharArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    UCharArray(const UCharVec&);                // View will be taken
    UCharArray(const UCharGenMat&);             // View will be taken

    operator IntArray() const;                  // Convert to IntArray

    /********************
     * Member functions *
     ********************/

    UCharArray    apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    UCharArray    apply(XmathFunTy)     const;
#endif  
    unsigned      binaryStoreSize()     const;   // Storage requirements
    UCharArray    copy()                const;   // Copy with distinct instance variables
    UChar*        data()                         {return (UChar*)begin;}
    const UChar*  data()                const    {return (UChar*)begin;}
    UCharArray    deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    UCharArray&   reference(const UCharArray& v); // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    UCharArray    slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    UChar&          operator[](const IntVec& i); // With bounds checking
    UChar           operator[](const IntVec& i) const;
    UChar&          operator()(const IntVec& i); // With optional bounds checking
    UChar           operator()(const IntVec& i) const;
    UChar&          operator()(int,int,int);
    UChar           operator()(int,int,int) const;
    UChar&          operator()(int,int,int,int);
    UChar           operator()(int,int,int,int) const;
    UCharVec        operator()(const RWSlice&,int,int);
    const UCharVec  operator()(const RWSlice&,int,int) const;
    UCharVec        operator()(int,const RWSlice&,int);
    const UCharVec  operator()(int,const RWSlice&,int) const;
    UCharVec        operator()(int,int,const RWSlice&);
    const UCharVec  operator()(int,int,const RWSlice&) const;
    UCharVec        operator()(const RWSlice&,int,int,int);
    const UCharVec  operator()(const RWSlice&,int,int,int) const;
    UCharVec        operator()(int,const RWSlice&,int,int);
    const UCharVec  operator()(int,const RWSlice&,int,int) const;
    UCharVec        operator()(int,int,const RWSlice&,int);
    const UCharVec  operator()(int,int,const RWSlice&,int) const;
    UCharVec        operator()(int,int,int,const RWSlice&);
    const UCharVec  operator()(int,int,int,const RWSlice&) const;
    UCharGenMat     operator()(int,const RWSlice&,const RWSlice&);
    const UCharGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    UCharGenMat     operator()(const RWSlice&,int,const RWSlice&);
    const UCharGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    UCharGenMat     operator()(const RWSlice&,const RWSlice&,int);
    const UCharGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    UCharGenMat     operator()(int,int,const RWSlice&,const RWSlice&);
    const UCharGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    UCharGenMat     operator()(int,const RWSlice&,int,const RWSlice&);
    const UCharGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    UCharGenMat     operator()(int,const RWSlice&,const RWSlice&,int);
    const UCharGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    UCharGenMat     operator()(const RWSlice&,int,int,const RWSlice&);
    const UCharGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    UCharGenMat     operator()(const RWSlice&,int,const RWSlice&,int);
    const UCharGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    UCharGenMat     operator()(const RWSlice&,const RWSlice&,int,int);
    const UCharGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    UCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const UCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    UCharArray      operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const UCharArray operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    UCharArray      operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const UCharArray operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    UCharArray      operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const UCharArray operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    UCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const UCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    UCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const UCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    UCharArray& operator=(const UCharArray& v); // Must be same length as v
    UCharArray& operator=(UChar);
    RWBoolean   operator==(const UCharArray&) const;
    RWBoolean   operator!=(const UCharArray& v) const;
    UCharArray& operator+=(UChar);
    UCharArray& operator-=(UChar s);
    UCharArray& operator*=(UChar);
    UCharArray& operator/=(UChar s);
    UCharArray& operator+=(const UCharArray&);
    UCharArray& operator-=(const UCharArray&);
    UCharArray& operator*=(const UCharArray&);
    UCharArray& operator/=(const UCharArray&);
    UCharArray& operator++();                   // Prefix operator
    UCharArray& operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

    friend UChar       toScalar(const UCharArray&);    // For 0-D arrays
    friend UCharVec    toVec(const UCharArray&);       // For 1-D arrays
    friend UCharGenMat toGenMat(const UCharArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

    inline UCharArray  operator+(const UCharArray& v) { return v; }
           UCharArray  operator+(const UCharArray&, const UCharArray&);
           UCharArray  operator-(const UCharArray&, const UCharArray&);
           UCharArray  operator*(const UCharArray&, const UCharArray&);
           UCharArray  operator/(const UCharArray&, const UCharArray&);
           UCharArray  operator+(const UCharArray&,UChar);
           UCharArray  operator+(UChar, const UCharArray&);
           UCharArray  operator-(const UCharArray&,UChar);
           UCharArray  operator-(UChar, const UCharArray&);
           UCharArray  operator*(const UCharArray&,UChar);
           UCharArray  operator*(UChar, const UCharArray&);
           UCharArray  operator/(const UCharArray&,UChar);
           UCharArray  operator/(UChar, const UCharArray&);
           ostream&    operator<<(ostream& s, const UCharArray& v);
           istream&    operator>>(istream& s, UCharArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline UCharArray  operator*(UChar s, const UCharArray& V) {return V*s;}
    inline UCharArray  operator+(UChar s, const UCharArray& V) {return V+s;}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

                                                  // No       abs() for UCharArray
           UChar       dot(const UCharArray&,const UCharArray&); // Return sum_ij...k (Aij...k * Bij...k)
           UCharArray  dot(const UCharVec&,const UCharArray&); // Return sum_i (Vi * Aij...k)
           UCharArray  dot(const UCharArray&,const UCharVec&); // Return sum_k (Ai...jk * Vk)
           IntVec      maxIndex(const UCharArray&); // Returns *index* of max value
           UChar       maxValue(const UCharArray&);
           IntVec      minIndex(const UCharArray&); // Returns *index* of min value
           UChar       minValue(const UCharArray&);
           UChar       prod(const UCharArray&);
           UChar       sum(const UCharArray&);


    /**** Functions for type conversion ****/

                                                  // No type conversion functions for UCharArray

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline UChar& UCharArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline UChar UCharArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline UChar& UCharArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline UChar UCharArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline UChar& UCharArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline UChar UCharArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline UChar& UCharArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline UChar UCharArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWUCARR_H__*/

#ifndef __RWCSCHUR_H__
#define __RWCSCHUR_H__
/*
 * DComplexSchurDecomp - Schur decomposition of a matrix
 *
 * Generated from template $Id: xschur.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * The Schur decomposition is often more useful than a full
 * eigendecomposition.  It provides the eigenvalues, and orthogonal
 * bases for invariant subspaces --- what more could you want?
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Schur decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/cvec.h"
#include "rw/cvec.h"
#include "rw/cgenmat.h"
#include "rw/cbal.h"
class DComplexHessenbergDecomp;

class DComplexSchurDecomp {
private:
  DComplexBalanceTransform B_;  // Balancing transformation
  DComplexGenMat           T_;  // Schur matrix
  DComplexGenMat           Z_;  // Schur vectors
  DComplexVec              w_;  // Eigenvalues

public:                                          
  DComplexSchurDecomp();
  DComplexSchurDecomp(const DComplexGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  DComplexSchurDecomp(const DComplexBalanceDecomp&);
  DComplexSchurDecomp(const DComplexHessenbergDecomp&);
  void factor(const DComplexGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const DComplexBalanceDecomp&);
  void factor(const DComplexHessenbergDecomp&);
  unsigned            cols()                  const {return T_.cols();}
  unsigned            rows()                  const {return T_.rows();}
  RWBoolean           good()                  const;// did this decomposition get computed properly
  DComplexGenMat           B()                     const;
  DComplexGenMat           Z()                     const;
  DComplexGenMat           T()                     const;
  DComplex                 eigenvalue(int)         const;
  DComplexVec              eigenvalues()           const;
  DComplexVec              Bx(const DComplexVec&)       const;
  DComplexVec              BInvTx(const DComplexVec&)   const;
  DComplexVec              Zx(const DComplexVec&)       const;
  DComplexVec              ZTx(const DComplexVec&)      const;
  DComplexGenMat           BX(const DComplexGenMat&)    const;
  DComplexGenMat           BInvTX(const DComplexGenMat&)const;
  DComplexGenMat           ZX(const DComplexGenMat&)    const;
  DComplexGenMat           ZTX(const DComplexGenMat&)   const;
  int                 move(int,int);              // Move an eigenvalue, returns where it actually ended up
  RWBoolean           moveToFront(const IntVec&); // Move the listed eigenvalues to the front (returns TRUE if all went well)
};

#endif

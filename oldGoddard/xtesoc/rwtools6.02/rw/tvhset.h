#ifndef __RWTVHSET_H__
#define __RWTVHSET_H__

/*
 * RWTValHashSet<T>:  A Set of values of type T, using a hashed lookup
 *
 * $Id: tvhset.h,v 2.7 1993/11/08 20:32:10 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class implements a parameterized Set of types T.  In a Set,
 * only one instance of an object of a given value can be inserted into
 * the collection.
 *
 * The implementation uses a hash table.
 *
 * Example use of this class:
 *
 *   #include <rw/cstring.h>
 *   #include <rw/tvhset.h>
 *   
 *   unsigned myHash(const RWCString& s){ return s.hash(); }
 *   
 *   RWTValHashSet<RWCString> set(myHash);	// A Set of RWCStrings
 *   
 *   set.insert("a string");	// Type conversion: char* to RWCString happens
 *   set.insert("another string");
 *   set.insert("a string");	// Rejected (already exists in collection)
 *   set.contains("a string");	// Returns true.
 *
 *
 * Note that the constructor for RWTValHashSet<T> takes a function with
 * prototype
 *
 *   unsigned hashFun(const T&);
 *
 * It should return a suitable hashing value for an instance of class T.
 * Usually, the definition for such a function is trivial because hashing
 * functions have been defined for all Rogue Wave supplied classes.
 *
 ***************************************************************************
 *
 * $Log: tvhset.h,v $
 * Revision 2.7  1993/11/08  20:32:10  jims
 * Port to ObjectStore
 *
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/06/03  20:47:30  griswolf
 * remove inline insert() to avoid Sun CC problem.
 *
 * Revision 2.4  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.3  1993/02/17  18:32:03  keffer
 * Now passes T's by const reference, rather than by value
 *
 * Revision 2.2  1993/01/29  01:27:09  keffer
 * Ported to cfront v3.0
 *
 *    Rev 1.2   17 Mar 1992 12:00:44   KEFFER
 * 
 *    Rev 1.1   04 Mar 1992 10:16:46   KEFFER
 * 
 *    Rev 1.0   02 Mar 1992 16:10:54   KEFFER
 * Initial revision.
 */

#include "rw/tvhasht.h"

/****************************************************************
 *								*
 *		Declarations for RWTValHashSet<T>		*
 *								*
 ****************************************************************/

template <class T> class RWExport RWTValHashSet : public RWTValHashTable<T>
{

public:

  RWTValHashSet
  (
    unsigned (*hashFun)(const T&),
    size_t size = RWDEFAULT_CAPACITY
  ) : RWTValHashTable<T>(hashFun, size)
  {;}

  // Member functions:
  virtual void		insert(const T& val);
};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tvhset.cc"
#endif


#endif	/* __RWTVHSET_H__ */


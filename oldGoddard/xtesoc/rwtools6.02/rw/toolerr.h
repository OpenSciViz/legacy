#ifndef __RWTOOLERR_H__
#define __RWTOOLERR_H__ 1

/*
 * Error messages for Tools.h++
 *
 * $Id: toolerr.h,v 2.3 1993/10/27 19:10:54 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: toolerr.h,v $
 * Revision 2.3  1993/10/27  19:10:54  jims
 * For DLLs, RWTOOL_XXX are exported functions, not objects
 *
 * Revision 2.2  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.1  1993/05/18  00:40:48  keffer
 * Rewrote to use new messaging and exception handling facility.
 *
 */

#include "rw/message.h"

#ifdef _RWTOOLSDLL
#  define RWTOOL_ALLOCOUT     RWTOOL_ALLOCOUT()
#  define RWTOOL_BADRE        RWTOOL_BADRE()
#  define RWTOOL_CRABS        RWTOOL_CRABS()
#  define RWTOOL_FLIST        RWTOOL_FLIST()
#  define RWTOOL_ID           RWTOOL_ID()
#  define RWTOOL_INDEX        RWTOOL_INDEX()
#  define RWTOOL_LOCK         RWTOOL_LOCK()
#  define RWTOOL_LONGINDEX    RWTOOL_LONGINDEX()
#  define RWTOOL_MAGIC        RWTOOL_MAGIC()
#  define RWTOOL_NEVECL       RWTOOL_NEVECL()
#  define RWTOOL_NOCREATE     RWTOOL_NOCREATE()
#  define RWTOOL_NOTALLOW     RWTOOL_NOTALLOW()
#  define RWTOOL_READERR      RWTOOL_READERR()
#  define RWTOOL_REF          RWTOOL_REF()
#  define RWTOOL_SEEKERR      RWTOOL_SEEKERR()
#  define RWTOOL_STREAM       RWTOOL_STREAM()
#  define RWTOOL_SUBSTRING    RWTOOL_SUBSTRING()
#  define RWTOOL_UNLOCK       RWTOOL_UNLOCK()
#  define RWTOOL_WRITEERR     RWTOOL_WRITEERR()
#endif /* _RWTOOLSDLL */

extern const RWMsgId rwexport RWTOOL_ALLOCOUT;
extern const RWMsgId rwexport RWTOOL_BADRE;
extern const RWMsgId rwexport RWTOOL_CRABS;
extern const RWMsgId rwexport RWTOOL_FLIST;
extern const RWMsgId rwexport RWTOOL_ID;
extern const RWMsgId rwexport RWTOOL_INDEX;
extern const RWMsgId rwexport RWTOOL_LOCK;
extern const RWMsgId rwexport RWTOOL_LONGINDEX;
extern const RWMsgId rwexport RWTOOL_MAGIC;
extern const RWMsgId rwexport RWTOOL_NEVECL;
extern const RWMsgId rwexport RWTOOL_NOCREATE;
extern const RWMsgId rwexport RWTOOL_NOTALLOW;
extern const RWMsgId rwexport RWTOOL_READERR;
extern const RWMsgId rwexport RWTOOL_REF;
extern const RWMsgId rwexport RWTOOL_SEEKERR;
extern const RWMsgId rwexport RWTOOL_STREAM;
extern const RWMsgId rwexport RWTOOL_SUBSTRING;
extern const RWMsgId rwexport RWTOOL_UNLOCK;
extern const RWMsgId rwexport RWTOOL_WRITEERR;

#endif  /*  __RWTOOLERR_H__ */

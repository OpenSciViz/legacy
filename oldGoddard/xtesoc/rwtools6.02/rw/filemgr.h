#ifndef __RWFILEMGR_H__
#define __RWFILEMGR_H__

/*
 * RWFileManager --- manages free space in a file
 *
 * $Id: filemgr.h,v 2.6 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: filemgr.h,v $
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/07/19  20:45:26  keffer
 * friend classes now use elaborated-type-specifier (ARM Sec. 11.4)
 *
 * Revision 2.4  1993/05/18  00:41:46  keffer
 * Now uses new exception handling facility
 *
 * Revision 2.3  1993/04/09  23:09:56  keffer
 * Extensive rewrite.  Allocations now done in units of RWspace.
 *
 * Revision 2.2  1993/03/01  23:28:38  keffer
 * Renamed variables.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 *    Rev 1.2   18 Feb 1992 09:54:20   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:14   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:15:00   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/rwfile.h"
#include "rw/tooldefs.h"
#include "rw/rwerr.h"

class RWListManager;

class RWExport RWFileManager : public RWFile
{

public:

  RWFileManager(const char* fname);
  ~RWFileManager();

  RWoffset		allocate(RWspace);  	// Allocate storage 
  void			deallocate(RWoffset);	// Deallocate storage.
  RWoffset		endData() const  {return endOfData_;} 
  RWoffset		start()   const	 {return startOfData_;}

#ifdef RDEBUG
  RWoffset              walkFreeList(RWoffset&, int&, RWspace&);
  void			summarize();
#endif

protected:

  RWoffset              allocateAtEnd(RWspace);
  RWBoolean             deallocateFromEnd(RWoffset, RWspace);
  void                  readErr();
  void                  seekErr();
  void                  writeErr();
  RWoffset              rootOffset() const;

private:

  // Cannot have 2 managers for the same file.
  RWFileManager(const RWFileManager&);
  void			operator=(const RWFileManager&);

private:

  RWListManager*        filemgr_;       // Implementation
  RWoffset		startOfData_;	// Offset to first data in file.
  RWoffset		endOfData_;	// Offset to last data in file.

friend class RWListManager;
};

#endif /* __RWFILEMGR_H__ */

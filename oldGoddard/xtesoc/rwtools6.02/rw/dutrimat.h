#ifndef __RWDUTRIMAT_H__
#define __RWDUTRIMAT_H__
/*
 * DoubleTriMat - An upper triangular matrix of Doubles
 *
 * Stores an upper triangular matrix.
 *
 * The matrix is stored in column major order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"

extern Double rwDoubleZero;	// This constant is sometimes returned by reference

class FloatUpperTriMat;
class DoubleGenMat;
class DoubleLowerTriMat;

class DoubleUpperTriMat {
private:

DoubleVec vec;
unsigned n;
  // The data which define the matrix


public:
DoubleUpperTriMat();
DoubleUpperTriMat( const DoubleUpperTriMat& );
DoubleUpperTriMat(unsigned n, unsigned nAgain);
DoubleUpperTriMat(const DoubleVec& data, unsigned n, unsigned nAgain);
DoubleUpperTriMat(const FloatUpperTriMat&);
~DoubleUpperTriMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline RODoubleRef operator()(int i, int j);
inline RODoubleRef ref(int i, int j);
       RODoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleUpperTriMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleUpperTriMat& operator=(const DoubleUpperTriMat& m);
DoubleUpperTriMat& reference(DoubleUpperTriMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleUpperTriMat copy() const;
DoubleUpperTriMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleUpperTriMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleUpperTriMat& X);
RWBoolean operator!=(const DoubleUpperTriMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleUpperTriMat& operator+=(const DoubleUpperTriMat& m);
DoubleUpperTriMat& operator-=(const DoubleUpperTriMat& m);
DoubleUpperTriMat& operator*=(const DoubleUpperTriMat& m);
DoubleUpperTriMat& operator*=(Double);
DoubleUpperTriMat& operator/=(const DoubleUpperTriMat& m);
DoubleUpperTriMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleUpperTriMat operator-(const DoubleUpperTriMat&);	// Unary minus
DoubleUpperTriMat operator+(const DoubleUpperTriMat&);	// Unary plus
DoubleUpperTriMat operator*(const DoubleUpperTriMat&, const DoubleUpperTriMat&);
DoubleUpperTriMat operator/(const DoubleUpperTriMat&, const DoubleUpperTriMat&);
DoubleUpperTriMat operator+(const DoubleUpperTriMat&, const DoubleUpperTriMat&);
DoubleUpperTriMat operator-(const DoubleUpperTriMat&, const DoubleUpperTriMat&);
DoubleUpperTriMat operator*(const DoubleUpperTriMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleUpperTriMat operator*(Double x, const DoubleUpperTriMat& A) { return A*x; }
#else
       DoubleUpperTriMat operator*(Double x, const DoubleUpperTriMat& A);
#endif
DoubleUpperTriMat operator/(const DoubleUpperTriMat& A, Double x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleUpperTriMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleUpperTriMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleLowerTriMat transpose(const DoubleUpperTriMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleUpperTriMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleUpperTriMat& A);
  // inner products

DoubleUpperTriMat toUpperTriMat( const DoubleGenMat& A );

DoubleUpperTriMat abs(const DoubleUpperTriMat& A);

inline Double minValue(const DoubleUpperTriMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleUpperTriMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Double DoubleUpperTriMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return (j<i) ? (Double)(0) : vec(j*(j+1)/2+i);
#endif
}

inline Double DoubleUpperTriMat::set(int i, int j, Double x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(j*(j+1)/2+i)=x; 
#endif
}

inline RODoubleRef DoubleUpperTriMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODoubleRef DoubleUpperTriMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleUpperTriMat::operator()(int i, int j) const { return val(i,j); }


#endif

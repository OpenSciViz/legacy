#ifndef __RWFVEC_H__
#define __RWFVEC_H__

/*
 * Declarations for Float precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/rwslice.h"
#include "rw/vector.h"

class FloatVecPick;
class RWRand;

class FloatVec : public RWVecView {
private:
    FloatVec(const RWDataView&, float*, unsigned, int);// For constructing new views
    friend FloatVec toVec(const FloatArray&);
    friend class FloatGenMat;                   // For row,col,diagonal
    friend class FloatArray;                    // For conversion constructor

public:

    /****************
     * Constructors *
     ****************/

    FloatVec() : RWVecView()                    {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    FloatVec(unsigned n) : RWVecView(n,sizeof(Float)) {}  
#endif
    FloatVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(Float)) {}  
    FloatVec(unsigned n, RWRand&);
    FloatVec(unsigned n, float val);
    FloatVec(unsigned n, float val, float by);
    FloatVec(const char *);
    FloatVec(const FloatVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    FloatVec(const FloatVecPick& p);
    FloatVec(const float* dat, unsigned n);     // Copy of dat will be made
    FloatVec(RWBlock *block, unsigned n);       // Pass a custom block on the free store

    operator DoubleVec() const;                 // Conversion to DoubleVec

    /********************
     * Member functions *
     ********************/

    FloatVec   apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    FloatVec   apply(XmathFunTy)     const;
#endif
    unsigned   binaryStoreSize()     const;     // Storage requirements
    FloatVec   copy()                const;     // Synonym for deepCopy()
    float*     data()                           {return (float*)begin;}
    const float* data()              const      {return (const float*)begin;}
    FloatVec   deepCopy()            const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    FloatVecPick pick(const IntVec& x);         // Return the "picked" elements
    const FloatVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    FloatVec&  reference(const FloatVec& v);    // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    FloatVec   slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    float&      operator[](int i);              // With bounds checking
    float       operator[](int i) const;        // With bounds checking
    float&      operator()(int i);              // With optional bounds checking
    float       operator()(int i)        const; // With optional bounds checking
    FloatVec    operator[](const RWSlice&);     // With bounds checking
    const FloatVec operator[](const RWSlice&) const;
    FloatVec    operator()(const RWSlice&);     // With optional bounds checking
    const FloatVec operator()(const RWSlice&) const;
    FloatVec&   operator=(const FloatVec& v);   // Must be same length as v
    FloatVec&   operator=(const FloatVecPick&);
    FloatVec&   operator=(float);
    RWBoolean   operator==(const FloatVec&) const;
    RWBoolean   operator!=(const FloatVec& v) const;
    FloatVec&   operator+=(float);
    FloatVec&   operator-=(float s)             { return operator+=(-s); }
    FloatVec&   operator*=(float);
    FloatVec&   operator/=(float s)             { return operator*=(1.0/s); }
    FloatVec&   operator+=(const FloatVec&);
    FloatVec&   operator-=(const FloatVec&);
    FloatVec&   operator*=(const FloatVec&);
    FloatVec&   operator/=(const FloatVec&);
    FloatVec&   operator++();                   // Prefix operator
    FloatVec&   operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           FloatVec    operator-(const FloatVec&);
    inline FloatVec    operator+(const FloatVec& v) { return v; }
           FloatVec    operator+(const FloatVec&, const FloatVec&);
           FloatVec    operator-(const FloatVec&, const FloatVec&);
           FloatVec    operator*(const FloatVec&, const FloatVec&);
           FloatVec    operator/(const FloatVec&, const FloatVec&);
           FloatVec    operator+(const FloatVec&,float);
           FloatVec    operator+(float s, const FloatVec& V);
           FloatVec    operator-(const FloatVec& V, float s);
           FloatVec    operator-(float, const FloatVec&);
           FloatVec    operator*(const FloatVec&,float);
           FloatVec    operator*(float s, const FloatVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline Float       operator%(const FloatVec& x, const FloatVec& y) {return dot(x,y);}
           FloatVec    operator/(const FloatVec& V, float s);
           FloatVec    operator/(float, const FloatVec&);
           ostream&    operator<<(ostream& s, const FloatVec& v);
           istream&    operator>>(istream& s, FloatVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline FloatVec    operator*(float s, const FloatVec& V) {return V*s;}
    inline FloatVec    operator+(float s, const FloatVec& V) {return V+s;}
    inline FloatVec    operator-(const FloatVec& V, float s) {return V+(float)(-s);} // cast makes microsoft happy when compiling sarr.cpp
    inline FloatVec    operator/(const FloatVec& V, float s) {return V*(1/s);}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           FloatVec    abs(const FloatVec& V);
           FloatVec    cumsum(const FloatVec&);
           FloatVec    delta(const FloatVec&);
           float       dot(const FloatVec&,const FloatVec&);
           int         maxIndex(const FloatVec&); // Returns *index* of max value
           float       maxValue(const FloatVec&);
           int         minIndex(const FloatVec&); // Returns *index* of min value
           float       minValue(const FloatVec&);
           float       prod(const FloatVec&);
           FloatVec    reverse(const FloatVec&);
           float       sum(const FloatVec&);


    /**** Functions for both real and complex ****/

           FloatVec    cos(const FloatVec& V);
           FloatVec    cosh(const FloatVec& V);
           FloatVec    exp(const FloatVec& V);
           float       mean(const FloatVec& V);
           FloatVec    pow(const FloatVec&,const FloatVec&);
           FloatVec    sin(const FloatVec& V);
           FloatVec    sinh(const FloatVec& V);
           FloatVec    sqrt(const FloatVec& V);
           float       variance(const FloatVec&);


    /**** Functions for real vectors ****/

           FloatVec    acos(const FloatVec& V);
           FloatVec    asin(const FloatVec& V);
           FloatVec    atan(const FloatVec& V);
           FloatVec    atan2(const FloatVec&,const FloatVec&);
           FloatVec    ceil(const FloatVec& V);
           FloatVec    expandEven(const FloatVec&);
           FloatVec    expandOdd(const FloatVec&);
           FloatVec    floor(const FloatVec& V);
           FloatVec    log(const FloatVec& V);
           FloatVec    log10(const FloatVec& V);
           FloatVec    tan(const FloatVec& V);
           FloatVec    tanh(const FloatVec& V);


    /**** Functions for type conversion ****/

           FloatVec    toFloat(const DoubleVec&);

    /**** Norm functions ****/
           Float         l2Norm(const FloatVec&);    // Euclidean norm
	   Float         l1Norm(const FloatVec&);    // sum of absolute values
           Float         linfNorm(const FloatVec&);  // largest absolute value
           Float         maxNorm(const FloatVec&);   // Same as linfNorm for vectors
           Float         frobNorm(const FloatVec&);  // Same as l2Norm for vectors

    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline FloatVec::FloatVec(const RWDataView& v, float* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline float& FloatVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline float FloatVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline float& FloatVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline float FloatVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline FloatVec FloatVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return FloatVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const FloatVec FloatVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return FloatVec(*this, (float*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline FloatVec FloatVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return FloatVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const FloatVec FloatVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return FloatVec(*this, (float*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline Float         operator%(const FloatVec& x, const FloatVec& y) {return dot(x,y);}

#endif /* __RWFVEC_H__ */

#ifndef __RWTPHSET_H__
#define __RWTPHSET_H__

/*
 * RWTPtrHashSet<T>:  A Set of pointers to type T, using a hashed lookup
 *
 * $Id: tphset.h,v 2.6 1993/11/08 13:16:37 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class implements a parameterized Set of pointers to type T.
 * In a Set, only one instance of an object of a given value can be 
 * inserted into the collection.
 *
 * The implementation uses a hash table.
 *
 * Example use of this class:
 *
 *   #include <rw/cstring.h>
 *   #include <rw/tphset.h>
 *   
 *   unsigned myHash(const RWCString& s){ return s.hash(); }
 *   
 *   RWTPtrHashSet<RWCString> set(myHash);	// A Set of RWCStrings
 *   
 *   set.insert(new RWCString("a string"));
 *   set.insert(new RWCString("another string"));
 *   set.insert(new RWCString("a string"));	// Rejected (already exists in collection)
 *   RWCString key("a string");
 *   set.contains(&key);	// Returns true.
 *
 *
 * Note that the constructor for RWTPtrHashSet<T> takes a function with
 * prototype
 *
 *   unsigned hashFun(const T&);
 *
 * It should return a suitable hashing value for an instance of class T.
 * Usually, the definition for such a function is trivial because hashing
 * functions have been defined for all Rogue Wave supplied classes.
 *
 ***************************************************************************
 *
 * $Log: tphset.h,v $
 * Revision 2.6  1993/11/08  13:16:37  jims
 * Port to ObjectStore
 *
 * Revision 2.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.4  1993/06/03  20:49:59  griswolf
 * remove inline insert() to avoid Sun CC problem.
 *
 * Revision 2.3  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.2  1993/01/29  01:27:09  keffer
 * Ported to cfront v3.0
 *
 *    Rev 1.0   25 May 1992 15:59:24   KEFFER
 * Initial revision.
 * 
 */

#include "rw/tphasht.h"

/****************************************************************
 *								*
 *		Declarations for RWTPtrHashSet<T>		*
 *								*
 ****************************************************************/

template <class T> class RWExport RWTPtrHashSet : public RWTPtrHashTable<T>
{

public:

  RWTPtrHashSet
  (
    unsigned (*hashFun)(const T&),
    size_t size = RWDEFAULT_CAPACITY
  ) : RWTPtrHashTable<T>(hashFun, size) { }

  // Member functions:
  virtual void		insert(T* a);
};

#ifdef RW_COMPILE_INSTANTIATE
# include "rw/tphset.cc"
#endif

#endif	/* __RWTPHSET_H__ */


#ifndef __RWDBLA_H__
#define __RWDBLA_H__

/*
 * double precision Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwdbla.h,v 1.1 1993/01/23 00:08:43 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwdbla.h,v $
 * Revision 1.1  1993/01/23  00:08:43  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:13:02   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:48   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwdcopy P_((unsigned n, double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwdset  P_((unsigned n, double* restrict x, int incx, const double* restrict scalar));
int    FDecl rwdsame P_((unsigned n, const double* restrict x, int incx, const double* restrict y, int incy));
int    FDecl rwdmax P_((unsigned n, const double* restrict x, int incx));
int    FDecl rwdmin P_((unsigned n, const double* restrict x, int incx));

void   FDecl rwddot P_((unsigned n, const double* restrict x, int incx, const double* restrict y, int incy, double* result));
void   FDecl rwdcdot P_((unsigned n, const double* restrict x, int incx, const double* restrict y, int incy, double* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwd_aplvv P_((unsigned n, double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_amivv P_((unsigned n, double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_amuvv P_((unsigned n, double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_advvv P_((unsigned n, double* restrict x, int incx, const double* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwd_aplvs P_((unsigned n, double* restrict x, int incx, const double* restrict scalar));
void   FDecl rwd_amuvs P_((unsigned n, double* restrict x, int incx, const double* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwd_plvv P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_mivv P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_muvv P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy));
void   FDecl rwd_dvvv P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwd_plvs P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar));
void   FDecl rwd_muvs P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar));
void   FDecl rwd_dvvs P_((unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwd_misv P_((unsigned n, double* restrict z, const double* scalar, const double* restrict x, int incx));
void   FDecl rwd_dvsv P_((unsigned n, double* restrict z, const double* scalar, const double* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


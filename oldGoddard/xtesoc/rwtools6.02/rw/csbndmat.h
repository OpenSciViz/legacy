#ifndef __RWCSBNDMAT_H__
#define __RWCSBNDMAT_H__
/*
 * DComplexSymBandMat - A symmetric banded matrix of DComplexs
 *
 * Stores a symmetric banded matrix.  All entries farther above or below    
 * the diagonal that the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top left
 * corner are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleSymBandMat;
class DComplexGenMat;
class DComplexBandMat;

class DComplexSymBandMat {
private:

DComplexVec vec;
unsigned n;
unsigned bandu;		// The upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DComplexSymBandMat();
DComplexSymBandMat( const DComplexSymBandMat& );
DComplexSymBandMat(unsigned n, unsigned nAgain, unsigned halfWidth);
DComplexSymBandMat(const DComplexVec& data, unsigned n, unsigned nAgain, unsigned halfWidth);
DComplexSymBandMat(const DoubleSymBandMat& re);
DComplexSymBandMat(const DoubleSymBandMat& re, const DoubleSymBandMat& im);
~DComplexSymBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 2*bandu+1; }
inline unsigned halfBandwidth() const  { return bandu; }
inline unsigned lowerBandwidth() const { return bandu; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline RODComplexRef operator()(int i, int j);
inline RODComplexRef ref(int i, int j);
       RODComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexSymBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DComplexVec diagonal(int =0) const;
       DComplexVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexSymBandMat& operator=(const DComplexSymBandMat& m);
DComplexSymBandMat& reference(DComplexSymBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexSymBandMat copy() const;
DComplexSymBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexSymBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned halfWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexSymBandMat& X);
RWBoolean operator!=(const DComplexSymBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexSymBandMat& operator+=(const DComplexSymBandMat& m);
DComplexSymBandMat& operator-=(const DComplexSymBandMat& m);
DComplexSymBandMat& operator*=(const DComplexSymBandMat& m);
DComplexSymBandMat& operator*=(DComplex);
DComplexSymBandMat& operator/=(const DComplexSymBandMat& m);
DComplexSymBandMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexSymBandMat operator-(const DComplexSymBandMat&);	// Unary minus
DComplexSymBandMat operator+(const DComplexSymBandMat&);	// Unary plus
DComplexSymBandMat operator*(const DComplexSymBandMat&, const DComplexSymBandMat&);
DComplexSymBandMat operator/(const DComplexSymBandMat&, const DComplexSymBandMat&);
DComplexSymBandMat operator+(const DComplexSymBandMat&, const DComplexSymBandMat&);
DComplexSymBandMat operator-(const DComplexSymBandMat&, const DComplexSymBandMat&);
DComplexSymBandMat operator*(const DComplexSymBandMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexSymBandMat operator*(DComplex x, const DComplexSymBandMat& A) { return A*x; }
#else
       DComplexSymBandMat operator*(DComplex x, const DComplexSymBandMat& A);
#endif
DComplexSymBandMat operator/(const DComplexSymBandMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexSymBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexSymBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexSymBandMat transpose(const DComplexSymBandMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexSymBandMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexSymBandMat& A);
  // inner products

DComplexSymBandMat toSymBandMat( const DComplexGenMat& A, unsigned halfband );
DComplexSymBandMat toSymBandMat( const DComplexBandMat& A );

DoubleSymBandMat abs(const DComplexSymBandMat& A);
DComplexSymBandMat conj(const DComplexSymBandMat& A);
DoubleSymBandMat real(const DComplexSymBandMat& A);
DoubleSymBandMat imag(const DComplexSymBandMat& A);
DoubleSymBandMat norm(const DComplexSymBandMat& A);
DoubleSymBandMat arg(const DComplexSymBandMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexSymBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline DComplex DComplexSymBandMat::set(int i, int j, DComplex x) {
    return bcset(i,j,x);
}

inline RODComplexRef DComplexSymBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODComplexRef DComplexSymBandMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexSymBandMat::operator()(int i, int j) const { return val(i,j); }

inline DComplexVec DComplexSymBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
#endif
}

#endif

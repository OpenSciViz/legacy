#ifndef __RWREGEXP_H__
#define __RWREGEXP_H__

/*
 * Declarations for class RWCRegexp --- Regular Expression
 *
 * $Id: regexp.h,v 2.8 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: regexp.h,v $
 * Revision 2.8  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.7  1993/05/19  00:01:25  keffer
 * Constructor takes const char*
 *
 * Revision 2.6  1993/05/14  00:20:27  myersn
 * add RWCRegexp(const RWCString&) constructor.
 *
 * Revision 2.4  1993/02/14  05:25:40  myersn
 * change indices from int to size_t, with RW_NPOS as the bad index.
 *
 * Revision 2.3  1993/01/29  20:28:44  myersn
 * change pattern string type to allow 8-bit clean matching.
 *
 * Revision 2.1  1992/11/16  04:10:24  keffer
 * Changed name from RWRegexp to RWCRegexp, leaving a typedef
 * for backwards compatibility.
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.3   04 Mar 1992 10:22:14   KEFFER
 * RWString -> RWCString
 * 
 *    Rev 1.1   28 Oct 1991 09:08:20   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:16:10   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#ifndef __RWTOOLDEFS_H__
# include "rw/tooldefs.h"
#endif

#if defined(RWTOOLS) && (RWTOOLS >= 0x0600)
  typedef unsigned short RWPatternType;
#else
  typedef unsigned char  RWPatternType;
#endif

class RWExport RWCString;

class RWExport RWCRegexp
{
public:
  enum statVal {OK=0, ILLEGAL, TOOLONG};
  RWCRegexp(const char*);
  RWCRegexp(const RWCString&);
  RWCRegexp(const RWCRegexp&);
  ~RWCRegexp();

  RWCRegexp&		operator=(const RWCRegexp&);
  RWCRegexp&		operator=(const RWCString&);	// Recompiles pattern
  RWCRegexp&		operator=(const char*);		// Recompiles pattern
  size_t		index(const RWCString& str, size_t* len, size_t start=0) const;
  statVal		status();	// Return & clear status
private:
  void			copyPattern(const RWCRegexp&);
  void			genPattern(const char*);
  RWPatternType*	thePattern_;	// Compiled pattern
  statVal		stat_;		// Status
  static const unsigned maxpat_;	// Max length of compiled pattern
};

typedef RWCRegexp RWRegexp;	// For backwards compatibility

#endif /* __RWREGEXP_H__ */

#ifndef __RWDVECPIK_H__
#define __RWDVECPIK_H__

/*
 * Declarations for DoubleVecPick --- picks elements out of a DoubleVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The DoubleVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class DoubleVecPick {
private:
  DoubleVec		V;
  const IntVec		pick;
  DoubleVecPick(DoubleVec& v, const IntVec& x);	// Constructor is private
  friend		DoubleVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const DoubleVec&);
  void			operator=(const DoubleVecPick&);
  void			operator=(double);

  inline double&	operator()(int i);
  inline double		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
DoubleVecPick::DoubleVecPick(DoubleVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline double& DoubleVecPick::operator()(int i)       { return V( pick(i) ); }
inline double  DoubleVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__DVECPIK_H__*/

#ifndef __RWLPAKFNS_H__
#define __RWLPAKFNS_H__

/*
 * Declarations of the linpack.h++ free functions.
 *
 * $Header: /users/rcs/mathrw/lpakfns.h,v 1.1 1993/01/23 00:08:36 alv Exp $
 *
 * These are the functions to solve systems of equations for 
 * triangular and tri-diagonal systems.  Also routines for finding
 * inverses and determinants of triangular systems.
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 *
 * $Log: lpakfns.h,v $
 * Revision 1.1  1993/01/23  00:08:36  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:13:06   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   16 Aug 1991 09:07:46   keffer
 * Initial revision.
 */

/*
 * Need to actually define DComplex because a determinant function
 * returns it.
 */
#include "rw/dcomplex.h"

class FloatVec;
class FloatTriDiagMat;
class FloatLowerTriMat;
class FloatUpperTriMat;
class FloatGenMat;
class DoubleVec;
class DoubleTriDiagMat;
class DoubleLowerTriMat;
class DoubleUpperTriMat;
class DoubleGenMat;
class DComplexVec;
class DComplexTriDiagMat;
class DComplexLowerTriMat;
class DComplexUpperTriMat;
class DComplexGenMat;

DComplexVec	solve(const DComplexLowerTriMat& A, const DComplexVec& b);
DComplexVec	solve(const DComplexTriDiagMat& A, const DComplexVec& b);
DComplexVec	solve(const DComplexUpperTriMat& A, const DComplexVec& b);
DoubleVec	solve(const DoubleLowerTriMat& A, const DoubleVec& b);
DoubleVec	solve(const DoubleTriDiagMat& A, const DoubleVec& b);
DoubleVec	solve(const DoubleUpperTriMat& A, const DoubleVec& b);
FloatVec	solve(const FloatLowerTriMat& A, const FloatVec& b);
FloatVec	solve(const FloatTriDiagMat& A, const FloatVec& b);
FloatVec	solve(const FloatUpperTriMat& A, const FloatVec& b);

DComplex	determinant(const DComplexLowerTriMat& A);
DComplex	determinant(const DComplexUpperTriMat& A);
double		determinant(const DoubleLowerTriMat& A);
double		determinant(const DoubleUpperTriMat& A);
float		determinant(const FloatLowerTriMat& A);
float		determinant(const FloatUpperTriMat& A);

DComplexLowerTriMat	inverse(const DComplexLowerTriMat& A);
DComplexUpperTriMat	inverse(const DComplexUpperTriMat& A);
DoubleLowerTriMat	inverse(const DoubleLowerTriMat& A);
DoubleUpperTriMat	inverse(const DoubleUpperTriMat& A);
FloatLowerTriMat	inverse(const FloatLowerTriMat& A);
FloatUpperTriMat	inverse(const FloatUpperTriMat& A);

DComplexVec	lowerTriConjTransposeSolve(const DComplexGenMat& A, const DComplexVec& b);
DComplexVec	lowerTriSolve(const DComplexGenMat& A, const DComplexVec& b);
DComplexVec	upperTriConjTransposeSolve(const DComplexGenMat& A, const DComplexVec& b);
DComplexVec	upperTriSolve(const DComplexGenMat& A, const DComplexVec& b);
DoubleVec	lowerTriSolve(const DoubleGenMat& A, const DoubleVec& b);
DoubleVec	lowerTriTransposeSolve(const DoubleGenMat& A, const DoubleVec& b);
DoubleVec	upperTriSolve(const DoubleGenMat& A, const DoubleVec& b);
DoubleVec	upperTriTransposeSolve(const DoubleGenMat& A, const DoubleVec& b);
FloatVec	lowerTriSolve(const FloatGenMat& A, const FloatVec& b);
FloatVec	lowerTriTransposeSolve(const FloatGenMat& A, const FloatVec& b);
FloatVec	upperTriSolve(const FloatGenMat& A, const FloatVec& b);
FloatVec	upperTriTransposeSolve(const FloatGenMat& A, const FloatVec& b);


#endif /*__RWLPAKFNS_H__*/

#ifndef __RWARRAY_H__
#define __RWARRAY_H__
/* 
 * Base class for array views and other classes needed for arrays.
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 */

#include "rw/ivec.h"

/*
 * Note that there can be no default constructor for RWArrayView.
 * A default array is zero-dimensional, a scalar, and we need to
 * know the size of the thing being stored.  To generate this array
 * use RWArrayView( IntVec(), sizeof(<thing>) ).
 */
class RWArrayView : public RWDataView {
protected:
  IntVec      npts;                           // Length of the array
  IntVec      step;                           // Stride length of the array

  RWArrayView(const IntVec& ivec, size_t s, Storage=COLUMN_MAJOR);
  RWArrayView(unsigned, unsigned, unsigned, size_t s, Storage=COLUMN_MAJOR);
  RWArrayView(unsigned, unsigned, unsigned, unsigned, size_t s, Storage=COLUMN_MAJOR);
  RWArrayView(RWBlock *block, const IntVec& ivec, Storage=COLUMN_MAJOR);
  RWArrayView(RWBlock *block, unsigned, unsigned, unsigned, Storage=COLUMN_MAJOR);
  RWArrayView(RWBlock *block, unsigned, unsigned, unsigned, unsigned, Storage=COLUMN_MAJOR);
  RWArrayView(const RWArrayView&);
  RWArrayView(const RWDataView& v, void *b, const IntVec& n, const IntVec& s);

  void      reference(const RWArrayView&);

public:  // These functions need to be public so that they can be called by global functions
  void boundsCheck(const IntVec& i)     const; // Ensure 0<=i<npts
  void boundsCheck(int,int,int)         const;
  void boundsCheck(int,int,int,int)     const;
  void nonnegCheck(const IntVec& n)     const; // Ensure all entries in n are non-negative
  void lengthCheck(const IntVec& i)     const; // Check that length of self is i
  void lengthCheck(int,int,int)         const;
  void lengthCheck(int,int,int,int)     const;
  void sliceCheck(const IntVec&, const IntVec&, const IntGenMat&)  const; // Check that slice is valid
  void dimensionCheck(int p)            const; // check p==#dimensions
  void dimensionLengthCheck(int i,int l)const; // check dimension i has length l
  void numPointsCheck(const char*,int)  const; // Check that there is at least n points       
//void versionErr(int,int)              const; // Error: version number wrong in restoreFrom

public:  // These functions really are for public consumption
  unsigned      dimension()             const  {return npts.length();}
  const IntVec& length()                const  {return npts;}
  int           length(int i)           const  {return npts(i);}
  const IntVec& stride()                const  {return step;}
  int           stride(int i)           const  {return step(i);}
};
                   

/*
 * The RWMultiIndex class provides an easy way to loop over an 
 * arbitrary number of indices from 0,0,...,0 to n1,...,np.
 * Use like so:
 *   for(RWMultiIndex i(limit); i; ++i) { }
 */

class RWMultiIndex : public IntVec {
private:
  int    isOK;
  IntVec npts;
public:
  RWMultiIndex( const IntVec& n );
  void operator++();
  void operator++(int) { operator++(); }
  operator void* ()    { return isOK ? (void*)1 : 0; }
};

/*
 * The rest contains internal details of the implementation.  They
 * may change in future releases
 */

/*
 * setupCompactArrayStep is used to set up the stride vector for
 * a compact array.  As a side effect, the number of points in the
 * array is computed - return this as it is often useful.
 */
#ifdef RW_GLOBAL_ENUMS
  int setupCompactArrayStep(            Storage storage, const IntVec& n, IntVec *step);
#else
  int setupCompactArrayStep(RWDataView::Storage storage, const IntVec& n, IntVec *step);
#endif

/*
 * The ArrayLooper class is used internally by the array code.
 * This class is not documented in the manual, and may dissappear or
 * change in a future release, so I wouldn't use it if I were you!
 *
 * It is used (internally) as follows:
 *
 * for( ArrayLooper l(npts,step); l; ++l ) {
 *   // Do something with the following elements
 *   // begin[l.start+0*l.stride],...,begin[l.start+l.length*l.stride]
 * }
 *
 * The above loop visits every point in the array exactly once, in
 * a fairly efficient way.  If an array is compact, then all the elements
 * will have been visited after only one iteration of the loop.
 *
 * This is used in all the arithmetic operators, and in some other
 * functions which want to visit all the elements of an array.
 *
 * The DoubleArrayLooper class is similiar, except it loops over two
 * arrays (with the same dimensions) at the same time.
 */

class ArrayLooper {
  IntVec npts;
  IntVec step;
  IntVec state;

  RWBoolean isOK;

public:
  int      start;
  unsigned length;
  int      stride;

  ArrayLooper(const IntVec&,const IntVec&);

  void operator++();
  operator void*() { return isOK ? (void*)1 : 0; }
};

class DoubleArrayLooper {
  IntVec npts;
  IntVec step1;
  IntVec step2;
  IntVec state;

  RWBoolean isOK;

public:
  unsigned length;
  int      start1;
  int      start2;
  int      stride1;
  int      stride2;

  DoubleArrayLooper(const IntVec&,const IntVec&,const IntVec&);

  void operator++();
  operator void*() { return isOK ? (void*)1 : 0; }
};

#endif /*__RWARRAY_H__*/

#ifndef __RWIGENMAT_H__
#define __RWIGENMAT_H__

/*
 * Declarations for Int precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/matrix.h"
#include "rw/ivec.h"

class IntGenMatPick;
class RWRand;

class IntGenMat : public RWMatView {
private:
    IntGenMat(const RWDataView&, int*, unsigned,unsigned, int,int); // For internal use
    friend IntGenMat toGenMat(const IntArray&);
    friend class IntArray;                      // For conversion constructor

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    IntVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    IntGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    IntGenMat() : RWMatView()                   {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    IntGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(int),s) {}
#endif
    IntGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(int),s) {}
    IntGenMat(unsigned m, unsigned n, RWRand&);
    IntGenMat(unsigned m, unsigned n, int val);
    IntGenMat(const char *);
    IntGenMat(const IntGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    IntGenMat(const IntGenMatPick& p);
    IntGenMat(const int* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    IntGenMat(const IntVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    IntGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data



    operator DoubleGenMat() const;              // Conversion to DoubleGenMat

    /********************
     * Member functions *
     ********************/

    IntGenMat  apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    IntGenMat  apply(XmathFunTy)     const;
#endif
    int&       bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, int x);      // set() with bounds checking: for matrix.h++ compatability
    int        bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const IntVec col(int)            const;     // Returns a view of a column
    IntVec     col(int);                        // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    IntGenMat  copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    int*       data()                           {return (int*)begin;}
    const int* data()                const      {return (int*)begin;}
    IntGenMat  deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const IntVec diagonal(int =0)    const;     // Returns a view of a diagonal
    IntVec     diagonal(int =0);                // Returns a view of a diagonal
    IntGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const IntGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    int&       ref(int i, int j);               // for matrix.h++ compatability
    IntGenMat& reference(const IntGenMat& v);   // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const IntVec row(int)            const;     // Returns a view of a row
    IntVec     row(int);                        // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, int x);        // for matrix.h++ compatability 
    IntVec     slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    IntGenMat  slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    int        val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=(int)0;}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    int&                 operator()(int,int);           
    int                  operator()(int,int) const;
    IntVec               operator()(int, const RWSlice&);
    const IntVec         operator()(int, const RWSlice&)            const;
    IntVec               operator()(const RWSlice&, int);
    const IntVec         operator()(const RWSlice&, int)            const;
    IntGenMat            operator()(const RWSlice&, const RWSlice&);
    const IntGenMat      operator()(const RWSlice&, const RWSlice&) const;
    IntGenMat&  operator=(const IntGenMat& v);  // Must be same size as v
    IntGenMat&  operator=(const IntGenMatPick&);
    IntGenMat&  operator=(int);
    RWBoolean   operator==(const IntGenMat&) const;
    RWBoolean   operator!=(const IntGenMat& v) const;
    IntGenMat&  operator+=(int);
    IntGenMat&  operator-=(int s)               { return operator+=(-s); }
    IntGenMat&  operator*=(int);
    IntGenMat&  operator/=(int);
    IntGenMat&  operator+=(const IntGenMat&);
    IntGenMat&  operator-=(const IntGenMat&);
    IntGenMat&  operator*=(const IntGenMat&);
    IntGenMat&  operator/=(const IntGenMat&);
    IntGenMat&  operator++();                   // Prefix operator
    IntGenMat&  operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           IntGenMat   operator-(const IntGenMat&);
    inline IntGenMat   operator+(const IntGenMat& v) { return v; }
           IntGenMat   operator+(const IntGenMat&, const IntGenMat&);
           IntGenMat   operator-(const IntGenMat&, const IntGenMat&);
           IntGenMat   operator*(const IntGenMat&, const IntGenMat&);
           IntGenMat   operator/(const IntGenMat&, const IntGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline IntGenMat   operator%(const IntGenMat& A, const IntGenMat& B) {return product(A,B);}
//  inline IntVec      operator%(const IntGenMat& A, const IntVec& x) {return product(A,x);}
//  inline IntVec      operator%(const IntVec& x, const IntGenMat& A) {return product(x,A);}
           IntGenMat   operator+(const IntGenMat&,int);
           IntGenMat   operator+(int, const IntGenMat&);
           IntGenMat   operator-(const IntGenMat&,int);
           IntGenMat   operator-(int, const IntGenMat&);
           IntGenMat   operator*(const IntGenMat&,int);
           IntGenMat   operator*(int, const IntGenMat&);
           IntGenMat   operator/(const IntGenMat&,int);
           IntGenMat   operator/(int, const IntGenMat&);
           ostream&    operator<<(ostream& s, const IntGenMat& v);
           istream&    operator>>(istream& s, IntGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline IntGenMat   operator*(int s, const IntGenMat& V) {return V*s;}
    inline IntGenMat   operator+(int s, const IntGenMat& V) {return V+s;}
    inline IntGenMat   operator-(const IntGenMat& V, int s) {return V+(int)(-s);}// cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           IntGenMat   abs(const IntGenMat& V);
           int         dot(const IntGenMat&,const IntGenMat&); // Return sum_ij (Aij*Bij)
           void        maxIndex(const IntGenMat&, int *i, int *j);
           void        minIndex(const IntGenMat&, int *i, int *j);
           int         maxValue(const IntGenMat&);
           int         minValue(const IntGenMat&);
           int         prod(const IntGenMat&);
           IntVec      product(const IntGenMat&, const IntVec&);
           IntVec      product(const IntVec&, const IntGenMat&);
           IntGenMat   product(const IntGenMat&, const IntGenMat&);
           int         sum(const IntGenMat&);
           IntGenMat   transpose(const IntGenMat&); // Transpose references this's data
    inline IntGenMat   adjoint(const IntGenMat& A) {return transpose(A);}
           IntGenMat   transposeProduct(const IntGenMat&,const IntGenMat&); // For matrix.h++ compatability



    /**** Functions for type conversion ****/

           IntGenMat   toInt(const DoubleGenMat&);
           IntGenMat   toInt(const FloatGenMat&);

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline int& IntGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline int IntGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline IntVec IntGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const IntVec IntGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline IntVec IntGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const IntVec IntGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline IntGenMat IntGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const IntGenMat IntGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const IntVec IntGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       IntVec IntGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const IntVec IntGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       IntVec IntGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline int& IntGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void IntGenMat::set(int i, int j, int x) {
    (*this)(i,j)=x;
  }

  inline int IntGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline int& IntGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void IntGenMat::bcset(int i, int j, int x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline int IntGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline IntGenMat         operator%(const IntGenMat& A, const IntGenMat& B) {return product(A,B);}
inline IntVec      operator%(const IntGenMat& A, const IntVec& x) {return product(A,x);}
inline IntVec      operator%(const IntVec& x, const IntGenMat& A) {return product(x,A);}

#endif /* __RWIGENMAT_H__ */

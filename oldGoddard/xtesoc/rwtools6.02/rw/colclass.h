#ifndef __RWCOLCLASS_H__
#define __RWCOLCLASS_H__

/*
 * Base class for the Smalltalk(TM)-like collection classes.
 *
 * $Id: colclass.h,v 2.7 1993/09/09 02:38:31 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: colclass.h,v $
 * Revision 2.7  1993/09/09  02:38:31  keffer
 * Added member function copyContentsTo().
 *
 * Revision 2.6  1993/04/14  18:38:37  keffer
 * Added declaration for static m.f. saveObjToStream() and
 * saveObjToRWFile()
 *
 * Revision 2.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.4  1993/03/17  21:07:06  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.2  1993/02/06  03:22:47  keffer
 * Added RWExport tags to RWBag, RWOrdered, etc. declarations.
 *
 *    Rev 1.5   12 Jun 1992 14:35:16   KEFFER
 * Ported to Computer Innovations C++; inserted declarations for RWBag, etc.
 * 
 *    Rev 1.4   25 May 1992 15:46:38   KEFFER
 * CAPACITY and RESIZE are now non-const and can be changed.
 * 
 *    Rev 1.2   28 Oct 1991 09:08:08   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.1   29 Jul 1991 13:53:02   keffer
 * Changed return type of asBag(), asOrderedCollection(), asSet() and
 * asSortedCollection() to their RW names.
 * 
 *    Rev 1.0   28 Jul 1991 08:13:12   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/collect.h"

class RWExport RWBag;
class RWExport RWOrdered;
class RWExport RWSet;
class RWExport RWBinaryTree;

/****************************************************************
 *								*
 *			RWCollection				*
 *								*
 ****************************************************************/

class RWExport RWCollection : public RWCollectable {

public:

  static size_t		DEFAULT_CAPACITY;
  static size_t		DEFAULT_RESIZE;

public:

  /************* Virtual function inherited from RWCollectable *****************/
  virtual RWspace		binaryStoreSize() const;
  virtual RWClassID		isA() const {return __RWCOLLECTION;}
  virtual void			restoreGuts(RWvistream&);
  virtual void			restoreGuts(RWFile&);
  virtual void			saveGuts(RWvostream&) const;
  virtual void			saveGuts(RWFile&) const;

  /****************  Added virtual functions for RWCollections ****************/
  virtual void			apply(RWapplyCollectable, void*) = 0;
  virtual void			clear() = 0;
  virtual void			clearAndDestroy();
  virtual RWBoolean		contains(const RWCollectable*) const;
  virtual size_t		entries() const = 0;	// Total entries
  virtual RWCollectable*	find(const RWCollectable*) const = 0;		// First occurrence
  virtual RWCollectable*	insert(RWCollectable*) = 0;
  virtual RWBoolean		isEmpty() const = 0;
  virtual size_t		occurrencesOf(const RWCollectable*) const = 0;
  virtual RWCollectable*	remove(const RWCollectable*) = 0;	// Remove first occurrence
  virtual void			removeAndDestroy(const RWCollectable*); 

  /************************ Special functions *******************************/
  void				operator+=(const RWCollection& c); // Add items of c to self
  void				operator-=(const RWCollection& c); // Remove items of c from self
  void				addContentsTo(RWCollection*) const;
  void				copyContentsTo(RWCollection*) const;
  RWBag				asBag() const;
  RWOrdered			asOrderedCollection() const;
  RWSet				asSet() const;
  RWBinaryTree			asSortedCollection() const;
  RWCollection*			select(RWtestCollectable, void*) const;

protected:

  static void			saveObjToStream(RWCollectable*, void*);
  static void			saveObjToRWFile(RWCollectable*, void*);

};	  

typedef void (*RWapplyKeyAndValue)(RWCollectable*, RWCollectable*, void*);

#endif /* __RWCOLCLASS_H__ */

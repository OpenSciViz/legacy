#ifndef __RWFCO_H__
#define __RWFCO_H__
/*
 * FloatCODecomp: representation of a complete orthogonal decomposition
 *
 * Generated from template $Id: xco.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A complete orthogonal decomposition decomposes a matrix A like this:
 *
 *                    [ T 0 ]
 *            A P = Q [     ] Z'
 *                    [ 0 0 ]
 *
 * where P is a permutation matrix, Q and Z are orthogonal, and T is upper
 * triangular.
 *
 * Implementation details:
 * The Q and P parts of the decomposition are represented using a QRDecomp
 * object.  In order to save copying the R part, CODecomp is made a friend
 * or QRDecomp and we invalidate the R portion of the decomposition,
 * writing T and information needed to compute Z over top of it.  Note
 * that the TZ_ object refers to the same data as QRdecomp_.QR_.
 */

#include "rw/fqr.h"
#include "rw/futrimat.h"

class FloatCODecomp {
private:
  FloatQRDecomp QRdecomp_;           // used to represent Q and P.  The R part of this is invalid.
  FloatVec      Ztau_;               // scalars needed to recover Z
  FloatGenMat   TZ_;                 // upper triangular part is T, rest is used to compute Z
  void        dofactor(double tol);// build Ztau_ and TZ_ from QRdecomp_

public:
  FloatCODecomp();                      // Constructors
  FloatCODecomp(const FloatCODecomp&);
  FloatCODecomp(const FloatQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  FloatCODecomp(const FloatGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  void factor(const FloatQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  void factor(const FloatGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  ~FloatCODecomp();
  void operator=(const FloatCODecomp&);

  unsigned       rows() const         {return QRdecomp_.rows();}
  unsigned       cols() const         {return QRdecomp_.cols();}
  unsigned       rank() const         {return TZ_.rows();}
  FloatGenMat      P() const            {return QRdecomp_.P();}
  FloatUpperTriMat T() const;
  FloatGenMat      Q() const            {return QRdecomp_.Q();}
  FloatGenMat      Z() const;

  FloatVec         Px(const FloatVec& x) const       {return QRdecomp_.Px(x);}
  FloatVec         PTx(const FloatVec& x) const      {return QRdecomp_.PTx(x);}
  FloatVec         Tx(const FloatVec& x) const;
  FloatVec         TTx(const FloatVec& x) const;
  FloatVec         Tinvx(const FloatVec& x) const;
  FloatVec         TTinvx(const FloatVec& x) const;
  FloatVec         Qx(const FloatVec& x) const       {return QRdecomp_.Qx(x);}
  FloatVec         QTx(const FloatVec& x) const      {return QRdecomp_.QTx(x);}
  FloatVec         Zx(const FloatVec& x) const;
  FloatVec         ZTx(const FloatVec& x) const;
};

#endif

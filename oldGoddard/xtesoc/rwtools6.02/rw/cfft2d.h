#ifndef __RWCFFT2D_H__
#define __RWCFFT2D_H__

/*
 * Double Precision 2-Dimensional Complex FFT server
 *
 * $Header: /users/rcs/mathrw/cfft2d.h,v 1.1 1993/01/23 00:08:32 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cfft2d.h,v $
 * Revision 1.1  1993/01/23  00:08:32  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:42   keffer
 * Changed include path to <rw/xxx.h>
 * 
 */

#include "rw/dcomplex.h"
#include "rw/cgenmat.h"
#include "rw/cfft.h"
     
class DComplexFFT2DServer {
  DComplexFFTServer	FFTrow;
  DComplexFFTServer	FFTcol;
  unsigned		Nrows;
  unsigned		Ncols;
public:
  // constructors
  DComplexFFT2DServer();
  DComplexFFT2DServer(unsigned nrows, unsigned ncols);
  DComplexFFT2DServer(unsigned nrows);		// Assumes square matrix
  DComplexFFT2DServer(const DComplexFFT2DServer&);

  // Assignment  
  void			  operator=(const DComplexFFT2DServer&);

  unsigned		rows() const {return Nrows;}
  unsigned		cols() const {return Ncols;}
  void			  setRows(unsigned);  // set new Nrows
  void			  setCols(unsigned);  // set new Ncols

  // Returns DFT of a complex matrix:
  DComplexGenMat		fourier(const DComplexGenMat&);

  // Returns IDFT of a complex matrix:
  DComplexGenMat		ifourier(const DComplexGenMat&);
};
#endif /* __RWCFFT2D_H__ */

#ifndef __RWBAG_H__
#define __RWBAG_H__

/*
 * Declarations for RWBag --- an unordered collection.
 *
 * $Id: rwbag.h,v 2.6 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwbag.h,v $
 * Revision 2.6  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.5  1993/07/19  20:45:26  keffer
 * friend classes now use elaborated-type-specifier (ARM Sec. 11.4)
 *
 * Revision 2.4  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.3  1993/03/17  21:05:21  keffer
 * Return type of binaryStoreSize() is now RWspace
 *
 * Revision 2.2  1993/02/07  18:45:31  keffer
 * Added default value of 0 to resize().
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 * 
 *    Rev 1.4   04 Aug 1992 19:00:26   KEFFER
 * Added "virtual" keyword to destructor for documentation purposes.
 * 
 *    Rev 1.3   22 May 1992 17:04:14   KEFFER
 * Now uses RWDECLARE_COLLECTABLE() macro
 * 
 *    Rev 1.2   18 Feb 1992 09:54:36   KEFFER
 * 
 *    Rev 1.1   28 Oct 1991 09:08:20   keffer
 * Changed inclusions to <rw/xxx.h>
 * 
 *    Rev 1.0   28 Jul 1991 08:16:18   keffer
 * Tools.h++ V4.0.5 PVCS baseline version
 *
 */

#include "rw/hashdict.h"

class RWExport RWBagIterator;

/****************************************************************
 *								*
 *			RWBag					*
 *								*
 ****************************************************************/

class RWExport RWBag : public RWCollection
{

  friend class RWExport RWBagIterator;
  RWDECLARE_COLLECTABLE(RWBag)

public:

  RWBag(size_t n = RWCollection::DEFAULT_CAPACITY);
  RWBag(const RWBag&);
  virtual ~RWBag();

  /******************** Member operators ****************************/
  void			operator=(const RWBag&);
  RWBoolean		operator==(const RWBag&)const;

  /****************** Virtual member functions *******************/
  virtual void			apply(RWapplyCollectable, void*);
//virtual RWspace		binaryStoreSize() const;
  virtual void			clear();
  virtual void			clearAndDestroy();
//virtual int			compareTo(const RWCollectable*) const;
//virtual RWBoolean		contains(const RWCollectable*) const;
  virtual size_t		entries() const			{return totalEntries;}
  virtual RWCollectable*	find(const RWCollectable*) const;
//virtual size_t		hash() const;
  virtual RWCollectable*	insert(RWCollectable*);
  virtual RWBoolean		isEmpty() const {return totalEntries==0;}
  virtual RWBoolean		isEqual(const RWCollectable*) const;
  virtual size_t		occurrencesOf(const RWCollectable*) const;
  virtual RWCollectable*	remove(const RWCollectable*);	// Remove first occurrence
//virtual void			removeAndDestroy(const RWCollectable*); 
//virtual void			restoreGuts(RWvistream&);
//virtual void			restoreGuts(RWFile&);
//virtual void			saveGuts(RWvostream&) const;
//virtual void			saveGuts(RWFile&) const;

/********************* Special functions **********************************/

  RWCollectable*		insertWithOccurrences(RWCollectable*, size_t);
  void				resize(size_t n = 0);

private:

  size_t		totalEntries;
  RWHashDictionary	contents;	// Dictionary of number of occurrences.

  void			deepenTally();
};

class RWExport RWBagIterator : public RWIterator
{

public:

  RWBagIterator(const RWBag&);

  /******************* Virtual function inherited from RWIterator ******************/
  virtual RWCollectable*	findNext(const RWCollectable*);
  virtual RWCollectable*	key() const;		// Return current item
  virtual RWCollectable*	operator()();
  virtual void			reset();

private:

  RWHashDictionaryIterator	contentsIterator;
  int				count;
  RWCollectable*		currentItem;

};

#endif /* __RWBAG_H__ */

#ifndef __RWIBLA_H__
#define __RWIBLA_H__

/*
 * int precision Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwibla.h,v 1.1 1993/01/23 00:08:43 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwibla.h,v $
 * Revision 1.1  1993/01/23  00:08:43  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:12:58   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:50   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwicopy P_((unsigned n, int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwiset  P_((unsigned n, int* restrict x, int incx, const int* restrict scalar));
int    FDecl rwisame P_((unsigned n, const int* restrict x, int incx, const int* restrict y, int incy));
int    FDecl rwimax P_((unsigned n, const int* restrict x, int incx));
int    FDecl rwimin P_((unsigned n, const int* restrict x, int incx));

void   FDecl rwidot P_((unsigned n, const int* restrict x, int incx, const int* restrict y, int incy, int* result));
void   FDecl rwicdot P_((unsigned n, const int* restrict x, int incx, const int* restrict y, int incy, int* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwi_aplvv P_((unsigned n, int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_amivv P_((unsigned n, int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_amuvv P_((unsigned n, int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_advvv P_((unsigned n, int* restrict x, int incx, const int* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwi_aplvs P_((unsigned n, int* restrict x, int incx, const int* restrict scalar));
void   FDecl rwi_amuvs P_((unsigned n, int* restrict x, int incx, const int* restrict scalar));
void   FDecl rwi_advvs P_((unsigned n, int* restrict x, int incx, const int* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwi_plvv P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_mivv P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_muvv P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy));
void   FDecl rwi_dvvv P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwi_plvs P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar));
void   FDecl rwi_muvs P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar));
void   FDecl rwi_dvvs P_((unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwi_misv P_((unsigned n, int* restrict z, const int* scalar, const int* restrict x, int incx));
void   FDecl rwi_dvsv P_((unsigned n, int* restrict z, const int* scalar, const int* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


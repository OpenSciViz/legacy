#ifndef __RWCSYMMAT_H__
#define __RWCSYMMAT_H__
/*
 * DComplexSymMat - A symmetric matrix of DComplexs
 *
 * Stores a symmetric matrix.  Only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"


class DoubleSymMat;
class DComplexGenMat;

class DComplexSymMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix


public:
DComplexSymMat();
DComplexSymMat( const DComplexSymMat& );
DComplexSymMat(unsigned n, unsigned nAgain);
DComplexSymMat(unsigned n, unsigned nAgain, DComplex initval);
DComplexSymMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexSymMat(const DoubleSymMat& re);
DComplexSymMat(const DoubleSymMat& re, const DoubleSymMat& im);
~DComplexSymMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline DComplex& operator()(int i, int j);
inline DComplex& ref(int i, int j);
       DComplex& bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexSymMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexSymMat& operator=(const DComplexSymMat& m);
DComplexSymMat& reference(DComplexSymMat& m);
  // reference() makes an alias, operator= makes a copy.

DComplexSymMat& operator=(DComplex x)   { vec=DComplex(x); return *this; }
  // Sets all elements of the matrix equal to x

void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexSymMat copy() const;
DComplexSymMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexSymMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.

DComplexSymMat apply(CmathFunTy) const;
DoubleSymMat apply2(CmathFunTy2) const;
  // Each element in the matrix returned by apply is
  // the result of calling f() on the corresponding element
  // in this matrix.  

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexSymMat& X);
RWBoolean operator!=(const DComplexSymMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexSymMat& operator+=(DComplex);
DComplexSymMat& operator-=(DComplex);
DComplexSymMat& operator+=(const DComplexSymMat& m);
DComplexSymMat& operator-=(const DComplexSymMat& m);
DComplexSymMat& operator*=(const DComplexSymMat& m);
DComplexSymMat& operator*=(DComplex);
DComplexSymMat& operator/=(const DComplexSymMat& m);
DComplexSymMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexSymMat operator-(const DComplexSymMat&);	// Unary minus
DComplexSymMat operator+(const DComplexSymMat&);	// Unary plus
DComplexSymMat operator*(const DComplexSymMat&, const DComplexSymMat&);
DComplexSymMat operator/(const DComplexSymMat&, const DComplexSymMat&);
DComplexSymMat operator+(const DComplexSymMat&, const DComplexSymMat&);
DComplexSymMat operator-(const DComplexSymMat&, const DComplexSymMat&);
DComplexSymMat operator*(const DComplexSymMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexSymMat operator*(DComplex x, const DComplexSymMat& A) { return A*x; }
#else
       DComplexSymMat operator*(DComplex x, const DComplexSymMat& A);
#endif
DComplexSymMat operator/(const DComplexSymMat& A, DComplex x);
DComplexSymMat operator/(DComplex x, const DComplexSymMat& A);
DComplexSymMat operator+(const DComplexSymMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexSymMat operator+(DComplex x, const DComplexSymMat& A) { return A+x; }
inline DComplexSymMat operator-(const DComplexSymMat& A, DComplex x) { return A+(-x); }
#else
       DComplexSymMat operator+(DComplex x, const DComplexSymMat& A);
       DComplexSymMat operator-(const DComplexSymMat& A, DComplex x);
#endif
DComplexSymMat operator-(DComplex x, const DComplexSymMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexSymMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexSymMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexSymMat transpose(const DComplexSymMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexSymMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexSymMat& A);
  // inner products

DComplexSymMat toSymMat( const DComplexGenMat& A );
DComplexSymMat upperToSymMat( const DComplexGenMat& A );
DComplexSymMat lowerToSymMat( const DComplexGenMat& A );

DoubleSymMat abs(const DComplexSymMat& A);
DComplexSymMat conj(const DComplexSymMat& A);
DoubleSymMat real(const DComplexSymMat& A);
DoubleSymMat imag(const DComplexSymMat& A);
DoubleSymMat norm(const DComplexSymMat& A);
DoubleSymMat arg(const DComplexSymMat& A);


inline DComplexSymMat cos(const DComplexSymMat& A)  { return A.apply(::cos); }
inline DComplexSymMat cosh(const DComplexSymMat& A) { return A.apply(::cosh); }
inline DComplexSymMat exp(const DComplexSymMat& A)  { return A.apply(::exp); } 
inline DComplexSymMat log(const DComplexSymMat& A)  { return A.apply(::log); }
inline DComplexSymMat sin(const DComplexSymMat& A)  { return A.apply(::sin); }
inline DComplexSymMat sinh(const DComplexSymMat& A) { return A.apply(::sinh); }
inline DComplexSymMat sqrt(const DComplexSymMat& A) { return A.apply(::sqrt); }
  // Math functions applicable to both real and complex types.


/*
 * Inline functions
 */

inline DComplex DComplexSymMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline DComplex DComplexSymMat::set(int i, int j, DComplex x) {
    return bcset(i,j,x);
}

inline DComplex& DComplexSymMat::ref(int i, int j) {
#ifdef RWBOUNDS_CHECK
return bcref(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline DComplex& DComplexSymMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexSymMat::operator()(int i, int j) const { return val(i,j); }


#endif

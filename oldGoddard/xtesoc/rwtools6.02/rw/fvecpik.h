#ifndef __RWFVECPIK_H__
#define __RWFVECPIK_H__

/*
 * Declarations for FloatVecPick --- picks elements out of a FloatVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The FloatVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class FloatVecPick {
private:
  FloatVec		V;
  const IntVec		pick;
  FloatVecPick(FloatVec& v, const IntVec& x);	// Constructor is private
  friend		FloatVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const FloatVec&);
  void			operator=(const FloatVecPick&);
  void			operator=(float);

  inline float&	operator()(int i);
  inline float		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
FloatVecPick::FloatVecPick(FloatVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline float& FloatVecPick::operator()(int i)       { return V( pick(i) ); }
inline float  FloatVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__FVECPIK_H__*/

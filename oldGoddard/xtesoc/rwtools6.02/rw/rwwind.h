#ifndef __RWWIND_H__
#define __RWWIND_H__

/*
 * Microsoft Windows related directives.
 *
 * $Id: rwwind.h,v 2.6 1993/09/11 10:32:21 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1991 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwwind.h,v $
 * Revision 2.6  1993/09/11  10:32:21  jims
 * Move toward support for DLLs under Win32
 *
 * Revision 2.5  1993/09/03  02:08:13  keffer
 * Macro _CLASSDLL is now named _RWTOOLSDLL
 *
 * Revision 2.4  1993/08/05  11:44:46  jims
 * Port to WIN32
 *
 * Revision 2.3  1993/04/10  23:48:21  jims
 * No longer sets RW_MULTI_THREAD flag for DLL situation
 *
 * Revision 2.2  1993/02/07  17:41:54  keffer
 * Moved the definition for _RWCLASSTYPE from rwwind.h to
 * defs.h; this allows the library to be used with far virtual
 * tables by non-Windows code.
 *
 * Revision 2.1  1993/01/15  22:23:49  keffer
 * Tools.h++ V5.2 alpha
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.4   22 Jun 1992 17:52:50   KEFFER
 * Now allows a DLL to be built that uses the Tools.h++ DLL.
 * 
 *    Rev 1.3   28 May 1992 11:04:28   KEFFER
 * Can now build static Windows library using Microsoft C/C++ 7.0
 * 
 *    Rev 1.2   17 Mar 1992 12:00:14   KEFFER
 * Fine-tuned.
 * 
 *    Rev 1.1   18 Feb 1992 09:54:42   KEFFER
 * 
 *    Rev 1.0   17 Nov 1991 13:40:14   keffer
 * Initial revision.
 *
 */

/*
 * Special stuff for 16-bit Windows (__WIN16__) 
 * and Windows NT / Win32s (__WIN32__).
 *
 * Under Windows, these header files can be used in two different modes: 
 * as part of a DLL, or as part of a regular (static) library.  
 * For each of these two ways, we can either be COMPILING the library,
 * or USING the library.  This makes a matrix of four possible situations.
 *
 *************************************************************************
 ******************************  DLL *************************************
 *************************************************************************
 *
 *                         Creating a DLL
 *
 * The macro __DLL__ should be define when compiling to create a DLL.
 *
 *     The Borland compilers automatically do this if either the -WD or 
 *     -WDE switch is being used.  In this situation, the macros 
 *     RWExport and rwexport expand to _export.
 *
 *     For other compilers, we must define __DLL__ where appropriate
 *     if the compiler doesn't.  See the example for Microsoft below.
 *
 *     RWExport and rwexport expand to _export by default.  Massage as
 *     necessary for your compiler; again see below for the Microsoft
 *     specific directives.
 *
 *                       Using the resultant DLL
 *
 * In order to use the resultant DLL, you must define the macro _RWTOOLSDLL
 * when compiling.  This will communicate to the Rogue Wave header files
 * your intention to use a DLL version of the Rogue Wave library.
 *
 * If you intend to use a DLL version of the Borland RTL as well, then you
 * should also define the macro _RTLDLL, as per Borland's instructions.
 *
 * You must also specify the -WS switch ("smart callbacks") for the 
 * Borland Compiler.
 *
 *
 *************************************************************************
 ***********************  Static Windows Library **************************
 *************************************************************************
 *
 *
 *                Creating a RW static Windows library
 *
 * If your intention is to create a Rogue Wave library to be used as 
 * as a static Windows library, then one of the macros __WIN16__ or __WIN32__
 * must have been defined in compiler.h (or by the compiler directly).
 *
 * Borland:	__WIN16__ will be defined if both _Windows and __MSDOS__ 
 *              is defined.  _Windows will be defined automatically if 
 *              any -W switch is used.  Borland defines __WIN32__ automatically
 *              in their 32-bit compiler for Windows NT / Win32s.
 *
 * Microsoft:	Microsoft automatically defines _WINDOWS if you use the
 *		/GA (preferred) or /GW switch.  __WIN16__ will be defined 
 *              if both _WINDOWS and _MSDOS are defined.
 *              __WIN32__ will only be defined if either it, or WIN32, 
 *              is explicitly defined on the cl386 command line.
 *
 * 
 *                 Using a RW static Windows library
 *
 * Nothing special needs to be done.  Just link compile with the appropriate
 * compile switches and link in the RW static Windows library.
 *
 */


/*
 * Check for Microsoft C/C++ and massage as necessary.
 */
#if defined(_MSC_VER)
#  if defined(_WINDLL) && !defined(__DLL__)
#    define __DLL__ 1
#  endif
#  if !defined(_export)
#    define _export __export
#  endif
#endif


/* For backwards compatibility: */
#if defined(_RWCLASSDLL) && !defined(_RWTOOLSDLL)
# define _RWTOOLSDLL 1
#endif

#if defined(_RWTOOLSDLL)
#  if !defined(__LARGE__) && !defined(__WIN32__)
#    error   Must use large or flat memory model when compiling or using the Tools.h++ DLL!
#  endif
#  if defined(_RWBUILDDLL)
     // Compiling the Tools.h++ DLL.
#    ifndef _MSC_VER
#      define RWExport _export	/* Mark classes as exported */
#      define rwexport _export	/*    & functions as well   */
#    else /* Microsoft: */
#      define RWExport __declspec(dllexport)	/* Mark classes as exported */
#      define rwexport __declspec(dllexport)    /*    & functions as well   */
#    endif
#  else
     // Using the Tools.h++ DLL.
#    if defined(__WIN16__)
#      define RWExport huge	/* Mark classes as huge  */
#      define rwexport far	/*    & functions as far */
#    elif defined(__WIN32__)
#      ifndef _MSC_VER
#        define RWExport _import     
#        define rwexport
#      else
#        define RWExport __declspec(dllimport)     
#        define rwexport
#      endif  
#    endif
#  endif
#else
   // Neither compiling, nor using the Tools.h++ DLL.
#  define RWExport
#  define rwexport
#endif

#endif /* __RWWIND_H__ */

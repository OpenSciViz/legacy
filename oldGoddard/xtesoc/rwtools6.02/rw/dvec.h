#ifndef __RWDVEC_H__
#define __RWDVEC_H__

/*
 * Declarations for Double precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/rwslice.h"
#include "rw/vector.h"

class DoubleVecPick;
class RWRand;

class DoubleVec : public RWVecView {
private:
    DoubleVec(const RWDataView&, double*, unsigned, int);// For constructing new views
    friend DoubleVec toVec(const DoubleArray&);
    friend class DoubleGenMat;                  // For row,col,diagonal
    friend class DoubleArray;                   // For conversion constructor
    friend DoubleVec real(const DComplexVec&);
    friend DoubleVec imag(const DComplexVec&);

public:

    /****************
     * Constructors *
     ****************/

    DoubleVec() : RWVecView()                   {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    DoubleVec(unsigned n) : RWVecView(n,sizeof(Double)) {}  
#endif
    DoubleVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(Double)) {}  
    DoubleVec(unsigned n, RWRand&);
    DoubleVec(unsigned n, double val);
    DoubleVec(unsigned n, double val, double by);
    DoubleVec(const char *);
    DoubleVec(const DoubleVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    DoubleVec(const DoubleVecPick& p);
    DoubleVec(const double* dat, unsigned n);   // Copy of dat will be made
    DoubleVec(RWBlock *block, unsigned n);      // Pass a custom block on the free store


    /********************
     * Member functions *
     ********************/

    DoubleVec  apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    DoubleVec  apply(XmathFunTy)     const;
#endif
    unsigned   binaryStoreSize()     const;     // Storage requirements
    DoubleVec  copy()                const;     // Synonym for deepCopy()
    double*    data()                           {return (double*)begin;}
    const double* data()             const      {return (const double*)begin;}
    DoubleVec  deepCopy()            const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    DoubleVecPick pick(const IntVec& x);        // Return the "picked" elements
    const DoubleVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    DoubleVec& reference(const DoubleVec& v);   // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    DoubleVec  slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    double&     operator[](int i);              // With bounds checking
    double      operator[](int i) const;        // With bounds checking
    double&     operator()(int i);              // With optional bounds checking
    double      operator()(int i)        const; // With optional bounds checking
    DoubleVec   operator[](const RWSlice&);     // With bounds checking
    const DoubleVec operator[](const RWSlice&) const;
    DoubleVec   operator()(const RWSlice&);     // With optional bounds checking
    const DoubleVec operator()(const RWSlice&) const;
    DoubleVec&  operator=(const DoubleVec& v);  // Must be same length as v
    DoubleVec&  operator=(const DoubleVecPick&);
    DoubleVec&  operator=(double);
    RWBoolean   operator==(const DoubleVec&) const;
    RWBoolean   operator!=(const DoubleVec& v) const;
    DoubleVec&  operator+=(double);
    DoubleVec&  operator-=(double s)            { return operator+=(-s); }
    DoubleVec&  operator*=(double);
    DoubleVec&  operator/=(double s)            { return operator*=(1.0/s); }
    DoubleVec&  operator+=(const DoubleVec&);
    DoubleVec&  operator-=(const DoubleVec&);
    DoubleVec&  operator*=(const DoubleVec&);
    DoubleVec&  operator/=(const DoubleVec&);
    DoubleVec&  operator++();                   // Prefix operator
    DoubleVec&  operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           DoubleVec   operator-(const DoubleVec&);
    inline DoubleVec   operator+(const DoubleVec& v) { return v; }
           DoubleVec   operator+(const DoubleVec&, const DoubleVec&);
           DoubleVec   operator-(const DoubleVec&, const DoubleVec&);
           DoubleVec   operator*(const DoubleVec&, const DoubleVec&);
           DoubleVec   operator/(const DoubleVec&, const DoubleVec&);
           DoubleVec   operator+(const DoubleVec&,double);
           DoubleVec   operator+(double s, const DoubleVec& V);
           DoubleVec   operator-(const DoubleVec& V, double s);
           DoubleVec   operator-(double, const DoubleVec&);
           DoubleVec   operator*(const DoubleVec&,double);
           DoubleVec   operator*(double s, const DoubleVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline Double      operator%(const DoubleVec& x, const DoubleVec& y) {return dot(x,y);}
           DoubleVec   operator/(const DoubleVec& V, double s);
           DoubleVec   operator/(double, const DoubleVec&);
           ostream&    operator<<(ostream& s, const DoubleVec& v);
           istream&    operator>>(istream& s, DoubleVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DoubleVec   operator*(double s, const DoubleVec& V) {return V*s;}
    inline DoubleVec   operator+(double s, const DoubleVec& V) {return V+s;}
    inline DoubleVec   operator-(const DoubleVec& V, double s) {return V+(double)(-s);} // cast makes microsoft happy when compiling sarr.cpp
    inline DoubleVec   operator/(const DoubleVec& V, double s) {return V*(1/s);}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleVec   abs(const DoubleVec& V);
           DoubleVec   cumsum(const DoubleVec&);
           DoubleVec   delta(const DoubleVec&);
           double      dot(const DoubleVec&,const DoubleVec&);
           int         maxIndex(const DoubleVec&); // Returns *index* of max value
           double      maxValue(const DoubleVec&);
           int         minIndex(const DoubleVec&); // Returns *index* of min value
           double      minValue(const DoubleVec&);
           double      prod(const DoubleVec&);
           DoubleVec   reverse(const DoubleVec&);
           double      sum(const DoubleVec&);


    /**** Functions for both real and complex ****/

           DoubleVec   cos(const DoubleVec& V);
           DoubleVec   cosh(const DoubleVec& V);
           DoubleVec   exp(const DoubleVec& V);
           double      mean(const DoubleVec& V);
           DoubleVec   pow(const DoubleVec&,const DoubleVec&);
           DoubleVec   sin(const DoubleVec& V);
           DoubleVec   sinh(const DoubleVec& V);
           DoubleVec   sqrt(const DoubleVec& V);
           double      variance(const DoubleVec&);


    /**** Functions for real vectors ****/

           DoubleVec   acos(const DoubleVec& V);
           DoubleVec   asin(const DoubleVec& V);
           DoubleVec   atan(const DoubleVec& V);
           DoubleVec   atan2(const DoubleVec&,const DoubleVec&);
           DoubleVec   ceil(const DoubleVec& V);
           DoubleVec   expandEven(const DoubleVec&);
           DoubleVec   expandOdd(const DoubleVec&);
           DoubleVec   floor(const DoubleVec& V);
           DoubleVec   log(const DoubleVec& V);
           DoubleVec   log10(const DoubleVec& V);
           DoubleVec   tan(const DoubleVec& V);
           DoubleVec   tanh(const DoubleVec& V);


    /**** Functions for type conversion ****/

           /* No type conversion functions for DoubleVec */

    /**** Norm functions ****/
           Double         l2Norm(const DoubleVec&);    // Euclidean norm
	   Double         l1Norm(const DoubleVec&);    // sum of absolute values
           Double         linfNorm(const DoubleVec&);  // largest absolute value
           Double         maxNorm(const DoubleVec&);   // Same as linfNorm for vectors
           Double         frobNorm(const DoubleVec&);  // Same as l2Norm for vectors

    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline DoubleVec::DoubleVec(const RWDataView& v, double* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline double& DoubleVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline double DoubleVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline double& DoubleVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline double DoubleVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline DoubleVec DoubleVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return DoubleVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const DoubleVec DoubleVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return DoubleVec(*this, (double*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline DoubleVec DoubleVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return DoubleVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const DoubleVec DoubleVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return DoubleVec(*this, (double*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline Double         operator%(const DoubleVec& x, const DoubleVec& y) {return dot(x,y);}

#endif /* __RWDVEC_H__ */

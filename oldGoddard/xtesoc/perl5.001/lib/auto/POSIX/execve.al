# NOTE: Derived from ../../lib/POSIX.pm.  Changes made here will be lost.
package POSIX;

sub execve {
    unimpl "execve() is C-specific, stopped";
    execve($_[0]);
}

1;

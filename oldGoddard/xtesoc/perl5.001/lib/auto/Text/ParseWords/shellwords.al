# NOTE: Derived from lib/Text/ParseWords.pm.  Changes made here will be lost.
package Text::ParseWords;

sub shellwords {

    # A &shellwords() replacement is included to demonstrate the new package.
    # This version differs from the original in that it will _NOT_ default
    # to using $_ if no arguments are given.  I personally find the old behavior
    # to be a mis-feature.

    local(@lines) = @_;
    $lines[$#lines] =~ s/\s+$//;
    &quotewords('\s+', 0, @lines);
}



1;

Artistic		The "Artistic License"
Changes			Differences between Perl 4 and Perl 5
Configure		Portability tool
Copying			The GNU General Public License
Doc/perl5-notes		Samples of new functionality
EXTERN.h		Included before foreign .h files
INTERN.h		Included before domestic .h files
MANIFEST		This list of files
Makefile.SH		A script that generates Makefile
README			The Instructions
README.vms		Notes about VMS
Todo			The Wishlist
XSUB.h			Include file for extension subroutines
autosplit		Splits up autoloader functions
av.c			Array value code
av.h			Array value header
c2ph.SH			program to translate dbx stabs to perl
c2ph.doc		documentation for c2ph
cflags.SH		A script that emits C compilation flags per file
config.H		Sample config.h
config_h.SH		Produces config.h
configpm		Produces lib/Config.pm
cop.h			Control operator header
cv.h			Code value header
deb.c			Debugging routines
doSH			Script to run all the *.SH files
doio.c			I/O operations
doop.c			Support code for various operations
dosish.h		Some defines for MS/DOSish machines
dump.c			Debugging output
eg/ADB			An adb wrapper to put in your crash dir
eg/README		Intro to example perl scripts
eg/changes		A program to list recently changed files
eg/client		A sample client
eg/down			A program to do things to subdirectories
eg/dus			A program to do du -s on non-mounted dirs
eg/findcp		A find wrapper that implements a -cp switch
eg/findtar		A find wrapper that pumps out a tar file
eg/g/gcp		A program to do a global rcp
eg/g/gcp.man		Manual page for gcp
eg/g/ged		A program to do a global edit
eg/g/ghosts		A sample /etc/ghosts file
eg/g/gsh		A program to do a global rsh
eg/g/gsh.man		Manual page for gsh
eg/muck			A program to find missing make dependencies
eg/muck.man		Manual page for muck
eg/myrup		A program to find lightly loaded machines
eg/nih			Script to insert #! workaround
eg/relink		A program to change symbolic links
eg/rename		A program to rename files
eg/rmfrom		A program to feed doomed filenames to
eg/scan/scan_df		Scan for filesystem anomalies
eg/scan/scan_last	Scan for login anomalies
eg/scan/scan_messages	Scan for console message anomalies
eg/scan/scan_passwd	Scan for passwd file anomalies
eg/scan/scan_ps		Scan for process anomalies
eg/scan/scan_sudo	Scan for sudo anomalies
eg/scan/scan_suid	Scan for setuid anomalies
eg/scan/scanner		An anomaly reporter
eg/server		A sample server
eg/shmkill		A program to remove unused shared memory
eg/sysvipc/README	Intro to Sys V IPC examples
eg/sysvipc/ipcmsg	Example of SYS V IPC message queues
eg/sysvipc/ipcsem	Example of Sys V IPC semaphores
eg/sysvipc/ipcshm	Example of Sys V IPC shared memory
eg/travesty		A program to print travesties of its input text
eg/unuc			Un-uppercases an all-uppercase text
eg/uudecode		A version of uudecode
eg/van/empty		A program to empty the trashcan
eg/van/unvanish		A program to undo what vanish does
eg/van/vanexp		A program to expire vanished files
eg/van/vanish		A program to put files in a trashcan
eg/who			A sample who program
eg/wrapsuid		A setuid script wrapper generator
emacs/cperl-mode	An alternate perl-mode
emacs/emacs19		Notes about emacs 19
emacs/perl-mode.el	Emacs major mode for perl
emacs/perldb.el		Emacs debugging
emacs/perldb.pl		Emacs debugging
emacs/tedstuff		Some optional patches
embed.h			Maps symbols to safer names
embed_h.sh		Produces embed.h
ext/DB_File/DB_File.pm		Berkeley DB extension Perl module
ext/DB_File/DB_File.xs		Berkeley DB extension external subroutines
ext/DB_File/DB_File_BS		Berkeley DB extension mkbootstrap fodder
ext/DB_File/Makefile.PL		Berkeley DB extension makefile writer
ext/DB_File/typemap		Berkeley DB extension interface types
ext/DynaLoader/DynaLoader.doc	Dynamic Loader specification
ext/DynaLoader/DynaLoader.pm	Dynamic Loader perl module
ext/DynaLoader/Makefile.PL	Dynamic Loader makefile writer
ext/DynaLoader/README		Dynamic Loader notes and intro
ext/DynaLoader/dl_aix.xs	AIX implementation
ext/DynaLoader/dl_dld.xs	GNU dld style implementation
ext/DynaLoader/dl_dlopen.xs	BSD/SunOS4&5 dlopen() style implementation
ext/DynaLoader/dl_hpux.xs	HP-UX implementation
ext/DynaLoader/dl_next.xs	Next implementation
ext/DynaLoader/dl_none.xs	Stub implementation
ext/DynaLoader/dl_vms.xs	VMS implementation
ext/DynaLoader/dlutils.c	Dynamic loader utilities for dl_*.xs files
ext/Fcntl/Fcntl.pm		Fcntl extension Perl module
ext/Fcntl/Fcntl.xs		Fcntl extension external subroutines
ext/Fcntl/MANIFEST		Fcntl extension file list
ext/Fcntl/Makefile.PL		Fcntl extension makefile writer
ext/GDBM_File/GDBM_File.pm	GDBM extension Perl module
ext/GDBM_File/GDBM_File.xs	GDBM extension external subroutines
ext/GDBM_File/Makefile.PL	GDBM extension makefile writer
ext/GDBM_File/typemap		GDBM extension interface types
ext/NDBM_File/Makefile.PL	NDBM extension makefile writer
ext/NDBM_File/NDBM_File.pm	NDBM extension Perl module
ext/NDBM_File/NDBM_File.xs	NDBM extension external subroutines
ext/NDBM_File/typemap		NDBM extension interface types
ext/ODBM_File/Makefile.PL	ODBM extension makefile writer
ext/ODBM_File/ODBM_File.pm	ODBM extension Perl module
ext/ODBM_File/ODBM_File.xs	ODBM extension external subroutines
ext/ODBM_File/typemap		ODBM extension interface types
ext/POSIX/Makefile.PL		POSIX extension makefile writer
ext/POSIX/POSIX.pm		POSIX extension Perl module
ext/POSIX/POSIX.xs		POSIX extension external subroutines
ext/POSIX/typemap		POSIX extension interface types
ext/SDBM_File/Makefile.PL	SDBM extension makefile writer
ext/SDBM_File/SDBM_File.pm	SDBM extension Perl module
ext/SDBM_File/SDBM_File.xs	SDBM extension external subroutines
ext/SDBM_File/sdbm/CHANGES		SDBM kit
ext/SDBM_File/sdbm/COMPARE		SDBM kit
ext/SDBM_File/sdbm/Makefile.PL		SDBM kit
ext/SDBM_File/sdbm/README		SDBM kit
ext/SDBM_File/sdbm/README.too		SDBM kit
ext/SDBM_File/sdbm/biblio		SDBM kit
ext/SDBM_File/sdbm/dba.c		SDBM kit
ext/SDBM_File/sdbm/dbd.c		SDBM kit
ext/SDBM_File/sdbm/dbe.1		SDBM kit
ext/SDBM_File/sdbm/dbe.c		SDBM kit
ext/SDBM_File/sdbm/dbm.c		SDBM kit
ext/SDBM_File/sdbm/dbm.h		SDBM kit
ext/SDBM_File/sdbm/dbu.c		SDBM kit
ext/SDBM_File/sdbm/grind		SDBM kit
ext/SDBM_File/sdbm/hash.c		SDBM kit
ext/SDBM_File/sdbm/linux.patches	SDBM kit
ext/SDBM_File/sdbm/makefile.sdbm	SDBM kit
ext/SDBM_File/sdbm/pair.c		SDBM kit
ext/SDBM_File/sdbm/pair.h		SDBM kit
ext/SDBM_File/sdbm/readme.ms		SDBM kit
ext/SDBM_File/sdbm/readme.ps		SDBM kit
ext/SDBM_File/sdbm/sdbm.3		SDBM kit
ext/SDBM_File/sdbm/sdbm.c		SDBM kit
ext/SDBM_File/sdbm/sdbm.h		SDBM kit
ext/SDBM_File/sdbm/tune.h		SDBM kit
ext/SDBM_File/sdbm/util.c		SDBM kit
ext/SDBM_File/typemap	SDBM extension interface types
ext/Socket/Makefile.PL	Socket extension makefile writer
ext/Socket/Socket.pm	Socket extension Perl module
ext/Socket/Socket.xs	Socket extension external subroutines
ext/util/extliblist	Used by extension Makefile.PL to make lib lists
ext/util/make_ext	Used by Makefile to execute extension Makefiles
ext/util/mkbootstrap	Turns ext/*/*_BS into bootstrap info
form.h			Public declarations for the above
global.sym		Symbols that need hiding when embedded
globals.c		File to declare global symbols (for shared library)
gv.c			Glob value code
gv.h			Glob value header
h2ph.SH			A thing to turn C .h files into perl .ph files
h2pl/README		How to turn .ph files into .pl files
h2pl/cbreak.pl		cbreak routines using .ph
h2pl/cbreak2.pl		cbreak routines using .pl
h2pl/eg/sizeof.ph	Sample sizeof array initialization
h2pl/eg/sys/errno.pl	Sample translated errno.pl
h2pl/eg/sys/ioctl.pl	Sample translated ioctl.pl
h2pl/eg/sysexits.pl	Sample translated sysexits.pl
h2pl/getioctlsizes	Program to extract types from ioctl.h
h2pl/mksizes		Program to make %sizeof array
h2pl/mkvars		Program to make .pl from .ph files
h2pl/tcbreak		cbreak test routine using .ph
h2pl/tcbreak2		cbreak test routine using .pl
h2xs.SH			Program to make .xs files from C header files
handy.h			Handy definitions
hints/3b1.sh		Hints for named architecture
hints/3b1cc		Hints for named architecture
hints/README.hints	Notes about hints.
hints/aix.sh		Hints for named architecture
hints/altos486.sh	Hints for named architecture
hints/apollo.sh		Hints for named architecture
hints/aux.sh		Hints for named architecture
hints/bsd386.sh		Hints for named architecture
hints/convexos.sh	Hints for named architecture
hints/cxux.sh		Hints for named architecture
hints/dec_osf.sh	Hints for named architecture
hints/dgux.sh		Hints for named architecture
hints/dnix.sh		Hints for named architecture
hints/dynix.sh		Hints for named architecture
hints/esix4.sh		Hints for named architecture
hints/fps.sh		Hints for named architecture
hints/freebsd.sh	Hints for named architecture
hints/genix.sh		Hints for named architecture
hints/greenhills.sh	Hints for named architecture
hints/hpux_9.sh		Hints for named architecture
hints/i386.sh		Hints for named architecture
hints/irix_4.sh		Hints for named architecture
hints/irix_5.sh		Hints for named architecture
hints/irix_6.sh		Hints for named architecture
hints/isc.sh		Hints for named architecture
hints/isc_2.sh		Hints for named architecture
hints/linux.sh		Hints for named architecture
hints/machten.sh	Hints for named architecture
hints/mips.sh		Hints for named architecture
hints/mpc.sh		Hints for named architecture
hints/mpeix.sh		Hints for named architecture
hints/ncr_tower.sh	Hints for named architecture
hints/netbsd.sh		Hints for named architecture
hints/next_3_0.sh	Hints for named architecture
hints/next_3_2.sh	Hints for named architecture
hints/opus.sh		Hints for named architecture
hints/powerunix.sh	Hints for named architecture
hints/sco_2_3_0.sh	Hints for named architecture
hints/sco_2_3_1.sh	Hints for named architecture
hints/sco_2_3_2.sh	Hints for named architecture
hints/sco_2_3_3.sh	Hints for named architecture
hints/sco_2_3_4.sh	Hints for named architecture
hints/sco_3.sh		Hints for named architecture
hints/solaris_2.sh	Hints for named architecture
hints/stellar.sh	Hints for named architecture
hints/sunos_4_0.sh	Hints for named architecture
hints/sunos_4_1.sh	Hints for named architecture
hints/svr4.sh		Hints for named architecture
hints/ti1500.sh		Hints for named architecture
hints/titanos.sh	Hints for named architecture
hints/ultrix_4.sh	Hints for named architecture
hints/unicos.sh		Hints for named architecture
hints/unisysdynix.sh	Hints for named architecture
hints/utekv.sh		Hints for named architecture
hints/uts.sh		Hints for named architecture
hv.c			Hash value code
hv.h			Hash value header
installperl		Perl script to do "make install" dirty work
interp.sym		Interpreter specific symbols to hide in a struct
ioctl.pl		Sample ioctl.pl
keywords.h		The keyword numbers
keywords.pl		Program to write keywords.h
lib/AnyDBM_File.pm	Perl module to emulate dbmopen
lib/AutoLoader.pm	Autoloader base class
lib/AutoSplit.pm	A module to split up autoload functions
lib/Benchmark.pm	A module to time pieces of code and such
lib/Carp.pm		Error message base class
lib/Cwd.pm		Various cwd routines (getcwd, fastcwd, chdir)
lib/English.pm		Readable aliases for short variables
lib/Env.pm		Map environment into ordinary variables
lib/Exporter.pm		Exporter base class
lib/ExtUtils/MakeMaker.pm	Write Makefiles for extensions
lib/ExtUtils/typemap		Extension interface types
lib/ExtUtils/xsubpp		External subroutine preprocessor
lib/File/Basename.pm	A module to emulate the basename program
lib/File/CheckTree.pm	Perl module supporting wholesale file mode validation
lib/File/Find.pm	Routines to do a find
lib/File/Path.pm	A module to do things like `mkdir -p' and `rm -r'
lib/FileHandle.pm	FileHandle methods
lib/Getopt/Long.pm	A module to fetch command options (GetOptions)
lib/Getopt/Std.pm	A module to fetch command options (getopt, getopts)
lib/I18N/Collate.pm	Routines to do strxfrm-based collation
lib/IPC/Open2.pm	Open a two-ended pipe
lib/IPC/Open3.pm	Open a three-ended pipe!
lib/Math/BigFloat.pm	An arbitrary precision floating-point arithmetic package
lib/Math/BigInt.pm	An arbitrary precision integer arithmetic package
lib/Math/Complex.pm	A Complex package
lib/Net/Ping.pm		Ping methods
lib/Search/Dict.pm	A module to do binary search on dictionaries
lib/Shell.pm		A module to make AUTOLOADEed system() calls
lib/Sys/Hostname.pm	Hostname methods
lib/Sys/Syslog.pm	Perl module supporting syslogging
lib/Term/Cap.pm		Perl module supporting termcap usage
lib/Term/Complete.pm	A command completion subroutine
lib/Test/Harness.pm	A test harness
lib/Text/Abbrev.pm	An abbreviation table builder
lib/Text/ParseWords.pm	Perl module to split words on arbitrary delimiter
lib/Text/Soundex.pm	Perl module to implement Soundex
lib/Text/Tabs.pm	Do expand and unexpand
lib/TieHash.pm		Base class for tied hashes
lib/SubstrHash.pm	Compact hash for known key, value and table size
lib/Time/Local.pm	Reverse translation of localtime, gmtime
lib/abbrev.pl		An abbreviation table builder
lib/assert.pl		assertion and panic with stack trace
lib/bigfloat.pl		An arbitrary precision floating point package
lib/bigint.pl		An arbitrary precision integer arithmetic package
lib/bigrat.pl		An arbitrary precision rational arithmetic package
lib/cacheout.pl		Manages output filehandles when you need too many
lib/chat2.inter		A chat2 with interaction
lib/chat2.pl		Randal's famous expect-ish routines
lib/complete.pl		A command completion subroutine
lib/ctime.pl		A ctime workalike
lib/dotsh.pl		Code to "dot" in a shell script
lib/dumpvar.pl		A variable dumper
lib/exceptions.pl	catch and throw routines
lib/fastcwd.pl		a faster but more dangerous getcwd
lib/find.pl		A find emulator--used by find2perl
lib/finddepth.pl	A depth-first find emulator--used by find2perl
lib/flush.pl		Routines to do single flush
lib/ftp.pl		FTP code
lib/getcwd.pl		A getcwd() emulator
lib/getopt.pl		Perl library supporting option parsing
lib/getopts.pl		Perl library supporting option parsing
lib/hostname.pl		Old hostname code
lib/importenv.pl	Perl routine to get environment into variables
lib/integer.pm		For "use integer"
lib/less.pm		For "use less"
lib/look.pl		A "look" equivalent
lib/newgetopt.pl	A perl library supporting long option parsing
lib/open2.pl		Open a two-ended pipe
lib/open3.pl		Open a three-ended pipe
lib/perl5db.pl		Perl debugging routines
lib/pwd.pl		Routines to keep track of PWD environment variable
lib/shellwords.pl	Perl library to split into words with shell quoting
lib/sigtrap.pm		For trapping an abort and giving traceback
lib/stat.pl		Perl library supporting stat function
lib/strict.pm		For "use strict"
lib/subs.pm		Declare overriding subs
lib/syslog.pl		Perl library supporting syslogging
lib/tainted.pl		Old code for tainting
lib/termcap.pl		Perl library supporting termcap usage
lib/timelocal.pl	Perl library supporting inverse of localtime, gmtime
lib/validate.pl		Perl library supporting wholesale file mode validation
makeaperl.SH		perl script that produces a new perl binary
makedepend.SH		Precursor to makedepend
makedir.SH		Precursor to makedir
malloc.c		A version of malloc you might not want
mg.c			Magic code
mg.h			Magic header
minimod.PL		Writes lib/ExtUtils/Miniperl.pm
miniperlmain.c		Basic perl w/o dynamic loading or extensions
mv-if-diff		Script to mv a file if it changed
myconfig		Prints summary of the current configuration
op.c			Opcode syntax tree code
op.h			Opcode syntax tree header
opcode.h		Automatically generated opcode header
opcode.pl		Opcode header generatore
patchlevel.h		The current patch level of perl
perl.c			main()
perl.h			Global declarations
perl_exp.SH		Creates list of exported symbols for AIX.
perlsh			A poor man's perl shell
perly.c			A byacc'ed perly.y
perly.c.diff		Fixup perly.c to allow recursion
perly.fixer		A program to remove yacc stack limitations
perly.h			The header file for perly.c
perly.y			Yacc grammar for perl
pl2pm			A pl to pm translator
pod/Makefile		Make pods into something else
pod/modpods/Abbrev.pod		Doc for Abbrev.pm
pod/modpods/AnyDBMFile.pod	Doc for AnyDBMFile.pm
pod/modpods/AutoLoader.pod	Doc for AutoLoader.pm
pod/modpods/AutoSplit.pod	Doc for AutoSplit.pm
pod/modpods/Basename.pod	Doc for Basename.pm
pod/modpods/Benchmark.pod	Doc for Benchmark.pm
pod/modpods/Carp.pod		Doc for Carp.pm
pod/modpods/CheckTree.pod	Doc for CheckTree.pm
pod/modpods/Collate.pod		Doc for Collate.pm
pod/modpods/Config.pod		Doc for Config.pm
pod/modpods/Cwd.pod		Doc for Cwd.pm
pod/modpods/DB_File.pod		Doc for File.pm
pod/modpods/Dynaloader.pod	Doc for Dynaloader.pm
pod/modpods/English.pod		Doc for English.pm
pod/modpods/Env.pod		Doc for Env.pm
pod/modpods/Exporter.pod	Doc for Exporter.pm
pod/modpods/Fcntl.pod		Doc for Fcntl.pm
pod/modpods/FileHandle.pod	Doc for FileHandle.pm
pod/modpods/Find.pod		Doc for Find.pm
pod/modpods/Finddepth.pod	Doc for Finddepth.pm
pod/modpods/GetOptions.pod	Doc for GetOptions.pm
pod/modpods/Getopt.pod		Doc for Getopt.pm
pod/modpods/MakeMaker.pod	Doc for MakeMaker.pm
pod/modpods/Open2.pod		Doc for Open2.pm
pod/modpods/Open3.pod		Doc for Open3.pm
pod/modpods/POSIX.pod		Doc for POSIX.pm
pod/modpods/Ping.pod		Doc for Ping.pm
pod/modpods/Socket.pod		Doc for Socket.pm
pod/modpods/integer.pod		Doc for integer.pm
pod/modpods/less.pod		Doc for less.pm
pod/modpods/sigtrap.pod		Doc for sigtrap.pm
pod/modpods/strict.pod		Doc for strict.pm
pod/modpods/subs.pod		Doc for subs.pm
pod/perl.pod		Top level perl man page
pod/perlapi.pod		XS api info
pod/perlbook.pod	Book info
pod/perlbot.pod		Object-oriented Bag o' Tricks
pod/perlcall.pod	Callback info
pod/perldata.pod	Data structure info
pod/perldebug.pod	Debugger info
pod/perldiag.pod	Diagnostic info
pod/perlembed.pod	Embedding info
pod/perlform.pod	Format info
pod/perlfunc.pod	Function info
pod/perlguts.pod	Internals info
pod/perlipc.pod		IPC info
pod/perlmod.pod		Module info
pod/perlobj.pod		Object info
pod/perlop.pod		Operator info
pod/perlovl.pod		Overloading info
pod/perlpod.pod		Pod info
pod/perlre.pod		Regular expression info
pod/perlref.pod		References info
pod/perlrun.pod		Execution info
pod/perlsec.pod		Security info
pod/perlstyle.pod	Style info
pod/perlsub.pod		Subroutine info
pod/perlsyn.pod		Syntax info
pod/perltrap.pod	Trap info
pod/perlvar.pod		Variable info
pod/pod2html		Translator to turn pod into HTML
pod/pod2latex		Translator to turn pod into LaTeX
pod/pod2man		Translator to turn pod into manpage
pod/splitman		Splits perlfunc into multiple man pages
pp.c			Push/Pop code
pp.h			Push/Pop code defs
pp_ctl.c		Push/Pop code for control flow
pp_hot.c		Push/Pop code for heavily used opcodes
pp_sys.c		Push/Pop code for system interaction
proto.h			Prototypes
regcomp.c		Regular expression compiler
regcomp.h		Private declarations for above
regexec.c		Regular expression evaluator
regexp.h		Public declarations for the above
run.c			The interpreter loop
scope.c			Scope entry and exit code
scope.h			Scope entry and exit header
sv.c			Scalar value code
sv.h			Scalar value header
t/README		Instructions for regression tests
t/TEST			The regression tester
t/base/cond.t		See if conditionals work
t/base/if.t		See if if works
t/base/lex.t		See if lexical items work
t/base/pat.t		See if pattern matching works
t/base/term.t		See if various terms work
t/cmd/elsif.t		See if else-if works
t/cmd/for.t		See if for loops work
t/cmd/mod.t		See if statement modifiers work
t/cmd/subval.t		See if subroutine values work
t/cmd/switch.t		See if switch optimizations work
t/cmd/while.t		See if while loops work
t/comp/cmdopt.t		See if command optimization works
t/comp/cpp.t		See if C preprocessor works
t/comp/decl.t		See if declarations work
t/comp/multiline.t	See if multiline strings work
t/comp/package.t	See if packages work
t/comp/script.t		See if script invokation works
t/comp/term.t		See if more terms work
t/io/argv.t		See if ARGV stuff works
t/io/dup.t		See if >& works right
t/io/fs.t		See if directory manipulations work
t/io/inplace.t		See if inplace editing works
t/io/pipe.t		See if secure pipes work
t/io/print.t		See if print commands work
t/io/tell.t		See if file seeking works
t/lib/anydbm.t		See if AnyDBM_File works
t/lib/bigint.t		See if bigint.pl works
t/lib/bigintpm.t	See if BigInt.pm works
t/lib/db-btree.t	See if DB_File works
t/lib/db-hash.t		See if DB_File works
t/lib/db-recno.t	See if DB_File works
t/lib/english.t		See if English works
t/lib/gdbm.t		See if GDBM_File works
t/lib/ndbm.t		See if NDBM_File works
t/lib/odbm.t		See if ODBM_File works
t/lib/posix.t		See if POSIX works
t/lib/sdbm.t		See if SDBM_File works
t/lib/soundex.t		See if Soundex works
t/op/append.t		See if . works
t/op/array.t		See if array operations work
t/op/auto.t		See if autoincrement et all work
t/op/chop.t		See if chop works
t/op/cond.t		See if conditional expressions work
t/op/delete.t		See if delete works
t/op/do.t		See if subroutines work
t/op/each.t		See if associative iterators work
t/op/eval.t		See if eval operator works
t/op/exec.t		See if exec and system work
t/op/exp.t		See if math functions work
t/op/flip.t		See if range operator works
t/op/fork.t		See if fork works
t/op/glob.t		See if <*> works
t/op/goto.t		See if goto works
t/op/groups.t		See if $( works
t/op/index.t		See if index works
t/op/int.t		See if int works
t/op/join.t		See if join works
t/op/list.t		See if array lists work
t/op/local.t		See if local works
t/op/magic.t		See if magic variables work
t/op/misc.t		See if miscellaneous bugs have been fixed
t/op/mkdir.t		See if mkdir works
t/op/my.t		See if lexical scoping works
t/op/oct.t		See if oct and hex work
t/op/ord.t		See if ord works
t/op/overload.t		See if operator overload works
t/op/pack.t		See if pack and unpack work
t/op/pat.t		See if esoteric patterns work
t/op/push.t		See if push and pop work
t/op/quotemeta.t	See if quotemeta works
t/op/rand.t		See if rand works
t/op/range.t		See if .. works
t/op/re_tests		Input file for op.regexp
t/op/read.t		See if read() works
t/op/readdir.t		See if readdir() works
t/op/ref.t		See if refs and objects work
t/op/regexp.t		See if regular expressions work
t/op/repeat.t		See if x operator works
t/op/sleep.t		See if sleep works
t/op/sort.t		See if sort works
t/op/split.t		See if split works
t/op/sprintf.t		See if sprintf works
t/op/stat.t		See if stat works
t/op/study.t		See if study works
t/op/subst.t		See if substitution works
t/op/substr.t		See if substr works
t/op/time.t		See if time functions work
t/op/undef.t		See if undef works
t/op/unshift.t		See if unshift works
t/op/vec.t		See if vectors work
t/op/write.t		See if write works
t/re_tests		Regular expressions for regexp.t
taint.c			Tainting code
toke.c			The tokener
unixish.h		Defines that are assumed on Unix
util.c			Utility routines
util.h			Public declarations for the above
vms/Makefile		VMS port
vms/config.vms		VMS port
vms/descrip.mms		VMS port
vms/ext/Filespec.pm	VMS-Unix file syntax interconversion
vms/ext/MM_VMS.pm	VMS-specific methods for MakeMaker
vms/ext/VMS/stdio/Makefile.PL	MakeMaker driver for VMS::stdio
vms/ext/VMS/stdio/stdio.pm	VMS options to stdio routines
vms/ext/VMS/stdio/stdio.xs	VMS options to stdio routines
vms/gen_shrfls.pl	VMS port
vms/genconfig.pl	VMS port
vms/genopt.com		VMS port
vms/mms2make.pl		VMS port
vms/perlshr.c		VMS port
vms/perlvms.pod		VMS port
vms/sockadapt.c		VMS port
vms/sockadapt.h		VMS port
vms/test.com		VMS port
vms/vms.c		VMS port
vms/vmsish.h		VMS port
vms/writemain.pl	VMS port
writemain.SH		Generate perlmain.c from miniperlmain.c+extensions
x2p/EXTERN.h		Same as above
x2p/INTERN.h		Same as above
x2p/Makefile.SH		Precursor to Makefile
x2p/a2p.c		Output of a2p.y run through byacc
x2p/a2p.h		Global declarations
x2p/a2p.man		Manual page for awk to perl translator
x2p/a2p.y		A yacc grammer for awk
x2p/a2py.c		Awk compiler, sort of
x2p/cflags.SH		A script that emits C compilation flags per file
x2p/find2perl.SH	A find to perl translator
x2p/handy.h		Handy definitions
x2p/hash.c		Associative arrays again
x2p/hash.h		Public declarations for the above
x2p/s2p.SH		Sed to perl translator
x2p/s2p.man		Manual page for sed to perl translator
x2p/str.c		String handling package
x2p/str.h		Public declarations for the above
x2p/util.c		Utility routines
x2p/util.h		Public declarations for the above
x2p/walk.c		Parse tree walker
xf			A script to translate Perl 4 symbols to Perl 5

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "sdbm/sdbm.h"

typedef DBM* SDBM_File;
#define sdbm_TIEHASH(dbtype,filename,flags,mode) sdbm_open(filename,flags,mode)
#define sdbm_FETCH(db,key)			sdbm_fetch(db,key)
#define sdbm_STORE(db,key,value,flags)		sdbm_store(db,key,value,flags)
#define sdbm_DELETE(db,key)			sdbm_delete(db,key)
#define sdbm_FIRSTKEY(db)			sdbm_firstkey(db)
#define sdbm_NEXTKEY(db,key)			sdbm_nextkey(db)


XS(XS_SDBM_File_sdbm_TIEHASH)
{
    dXSARGS;
    if (items != 4) {
	croak("Usage: SDBM_File::TIEHASH(dbtype, filename, flags, mode)");
    }
    {
	char *	dbtype = (char *)SvPV(ST(0),na);
	char *	filename = (char *)SvPV(ST(1),na);
	int	flags = (int)SvIV(ST(2));
	int	mode = (int)SvIV(ST(3));
	SDBM_File	RETVAL;

	RETVAL = sdbm_TIEHASH(dbtype, filename, flags, mode);
	ST(0) = sv_newmortal();
	sv_setref_pv(ST(0), "SDBM_File", (void*)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_DESTROY)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: SDBM_File::DESTROY(db)");
    }
    {
	SDBM_File	db;

	if (SvROK(ST(0))) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not a reference");
	sdbm_close(db);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_FETCH)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: SDBM_File::FETCH(db, key)");
    }
    {
	SDBM_File	db;
	datum	key;
	datum	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = sdbm_FETCH(db, key);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_STORE)
{
    dXSARGS;
    if (items < 3 || items > 4) {
	croak("Usage: SDBM_File::STORE(db, key, value, flags = DBM_REPLACE)");
    }
    {
	SDBM_File	db;
	datum	key;
	datum	value;
	int	flags;
	int	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	value.dptr = SvPV(ST(2), na);
	value.dsize = (int)na;;

	if (items < 4)
	    flags = DBM_REPLACE;
	else {
	    flags = (int)SvIV(ST(3));
	}

	RETVAL = sdbm_STORE(db, key, value, flags);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
	if (RETVAL) {
	    if (RETVAL < 0 && errno == EPERM)
		croak("No write permission to sdbm file");
	    croak("sdbm store returned %d, errno %d, key \"%s\"",
			RETVAL,errno,key.dptr);
	    sdbm_clearerr(db);
	}
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_DELETE)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: SDBM_File::DELETE(db, key)");
    }
    {
	SDBM_File	db;
	datum	key;
	int	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = sdbm_DELETE(db, key);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_FIRSTKEY)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: SDBM_File::FIRSTKEY(db)");
    }
    {
	SDBM_File	db;
	datum	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	RETVAL = sdbm_FIRSTKEY(db);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_NEXTKEY)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: SDBM_File::NEXTKEY(db, key)");
    }
    {
	SDBM_File	db;
	datum	key;
	datum	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = sdbm_NEXTKEY(db, key);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_error)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: SDBM_File::error(db)");
    }
    {
	SDBM_File	db;
	int	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	RETVAL = sdbm_error(db);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_SDBM_File_sdbm_clearerr)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: SDBM_File::clearerr(db)");
    }
    {
	SDBM_File	db;
	int	RETVAL;

	if (sv_isa(ST(0), "SDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (SDBM_File) tmp;
	}
	else
	    croak("db is not of type SDBM_File");

	RETVAL = sdbm_clearerr(db);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
    }
    XSRETURN(1);
}

XS(boot_SDBM_File)
{
    dXSARGS;
    char* file = __FILE__;

    newXS("SDBM_File::TIEHASH", XS_SDBM_File_sdbm_TIEHASH, file);
    newXS("SDBM_File::DESTROY", XS_SDBM_File_sdbm_DESTROY, file);
    newXS("SDBM_File::FETCH", XS_SDBM_File_sdbm_FETCH, file);
    newXS("SDBM_File::STORE", XS_SDBM_File_sdbm_STORE, file);
    newXS("SDBM_File::DELETE", XS_SDBM_File_sdbm_DELETE, file);
    newXS("SDBM_File::FIRSTKEY", XS_SDBM_File_sdbm_FIRSTKEY, file);
    newXS("SDBM_File::NEXTKEY", XS_SDBM_File_sdbm_NEXTKEY, file);
    newXS("SDBM_File::error", XS_SDBM_File_sdbm_error, file);
    newXS("SDBM_File::clearerr", XS_SDBM_File_sdbm_clearerr, file);
    ST(0) = &sv_yes;
    XSRETURN(1);
}

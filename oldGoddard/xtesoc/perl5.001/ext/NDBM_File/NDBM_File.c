#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <ndbm.h>

typedef DBM* NDBM_File;
#define dbm_TIEHASH(dbtype,filename,flags,mode) dbm_open(filename,flags,mode)
#define dbm_FETCH(db,key)			dbm_fetch(db,key)
#define dbm_STORE(db,key,value,flags)		dbm_store(db,key,value,flags)
#define dbm_DELETE(db,key)			dbm_delete(db,key)
#define dbm_FIRSTKEY(db)			dbm_firstkey(db)
#define dbm_NEXTKEY(db,key)			dbm_nextkey(db)

XS(XS_NDBM_File_dbm_TIEHASH)
{
    dXSARGS;
    if (items != 4) {
	croak("Usage: NDBM_File::TIEHASH(dbtype, filename, flags, mode)");
    }
    {
	char *	dbtype = (char *)SvPV(ST(0),na);
	char *	filename = (char *)SvPV(ST(1),na);
	int	flags = (int)SvIV(ST(2));
	int	mode = (int)SvIV(ST(3));
	NDBM_File	RETVAL;

	RETVAL = dbm_TIEHASH(dbtype, filename, flags, mode);
	ST(0) = sv_newmortal();
	sv_setref_pv(ST(0), "NDBM_File", (void*)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_DESTROY)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: NDBM_File::DESTROY(db)");
    }
    {
	NDBM_File	db;

	if (SvROK(ST(0))) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not a reference");
	dbm_close(db);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_FETCH)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: NDBM_File::FETCH(db, key)");
    }
    {
	NDBM_File	db;
	datum	key;
	datum	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = dbm_FETCH(db, key);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_STORE)
{
    dXSARGS;
    if (items < 3 || items > 4) {
	croak("Usage: NDBM_File::STORE(db, key, value, flags = DBM_REPLACE)");
    }
    {
	NDBM_File	db;
	datum	key;
	datum	value;
	int	flags;
	int	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	value.dptr = SvPV(ST(2), na);
	value.dsize = (int)na;;

	if (items < 4)
	    flags = DBM_REPLACE;
	else {
	    flags = (int)SvIV(ST(3));
	}

	RETVAL = dbm_STORE(db, key, value, flags);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
	if (RETVAL) {
	    if (RETVAL < 0 && errno == EPERM)
		croak("No write permission to ndbm file");
	    croak("ndbm store returned %d, errno %d, key \"%s\"",
			RETVAL,errno,key.dptr);
	    dbm_clearerr(db);
	}
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_DELETE)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: NDBM_File::DELETE(db, key)");
    }
    {
	NDBM_File	db;
	datum	key;
	int	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = dbm_DELETE(db, key);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_FIRSTKEY)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: NDBM_File::FIRSTKEY(db)");
    }
    {
	NDBM_File	db;
	datum	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	RETVAL = dbm_FIRSTKEY(db);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_NEXTKEY)
{
    dXSARGS;
    if (items != 2) {
	croak("Usage: NDBM_File::NEXTKEY(db, key)");
    }
    {
	NDBM_File	db;
	datum	key;
	datum	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	key.dptr = SvPV(ST(1), na);
	key.dsize = (int)na;;

	RETVAL = dbm_NEXTKEY(db, key);
	ST(0) = sv_newmortal();
	sv_setpvn(ST(0), RETVAL.dptr, RETVAL.dsize);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_error)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: NDBM_File::error(db)");
    }
    {
	NDBM_File	db;
	int	RETVAL;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	RETVAL = dbm_error(db);
	ST(0) = sv_newmortal();
	sv_setiv(ST(0), (IV)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_NDBM_File_dbm_clearerr)
{
    dXSARGS;
    if (items != 1) {
	croak("Usage: NDBM_File::clearerr(db)");
    }
    {
	NDBM_File	db;

	if (sv_isa(ST(0), "NDBM_File")) {
	    IV tmp = SvIV((SV*)SvRV(ST(0)));
	    db = (NDBM_File) tmp;
	}
	else
	    croak("db is not of type NDBM_File");

	dbm_clearerr(db);
    }
    XSRETURN(1);
}

XS(boot_NDBM_File)
{
    dXSARGS;
    char* file = __FILE__;

    newXS("NDBM_File::TIEHASH", XS_NDBM_File_dbm_TIEHASH, file);
    newXS("NDBM_File::DESTROY", XS_NDBM_File_dbm_DESTROY, file);
    newXS("NDBM_File::FETCH", XS_NDBM_File_dbm_FETCH, file);
    newXS("NDBM_File::STORE", XS_NDBM_File_dbm_STORE, file);
    newXS("NDBM_File::DELETE", XS_NDBM_File_dbm_DELETE, file);
    newXS("NDBM_File::FIRSTKEY", XS_NDBM_File_dbm_FIRSTKEY, file);
    newXS("NDBM_File::NEXTKEY", XS_NDBM_File_dbm_NEXTKEY, file);
    newXS("NDBM_File::error", XS_NDBM_File_dbm_error, file);
    newXS("NDBM_File::clearerr", XS_NDBM_File_dbm_clearerr, file);
    ST(0) = &sv_yes;
    XSRETURN(1);
}

#include <varargs.h>

main() { xxx("foo"); }

xxx(va_alist)
va_dcl
{
	va_list args;
	char buf[10];

	va_start(args);
	exit((unsigned long)vsprintf(buf,"%s",args) > 10L);
}

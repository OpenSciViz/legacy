.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLCALL 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlcall \- Perl calling conventions from C
.SH "DESCRIPTION"
\fBWARNING : This document is still under construction. 
There are bound to be a number of inaccuracies, so tread very carefully for now.\fR
.PP
The purpose of this document is to show you how to write \fIcallbacks\fR, 
i.e. how to call Perl from C. The main
focus is on how to interface back to Perl from a bit of C code that has itself
been run by Perl, i.e. the \*(L'main\*(R' program is a Perl script; you are using it
to execute
a section of code written in C; that bit of C code wants you to do something
with a particular event, so you want a Perl sub to be executed whenever it
happens.
.PP
Examples where this is necessary include
.Ip "\(bu " 5
You have created an \s-1XSUB\s0 interface to an application's C \s-1API\s0.
.Sp
A fairly common feature in applications is to allow you to define a C
function that will get called whenever something nasty occurs.
What we would like is for a Perl sub to be called instead.
.Ip "\(bu" 5
The classic example of where callbacks are used is in an event driven program 
like for X\-windows.
In this case your register functions to be called whenever a specific events
occur, e.g. a mouse button is pressed.
.PP
Although the techniques described are applicable to embedding Perl
in a C program, this is not the primary goal of this document. For details
on embedding Perl in C refer to the \fIperlembed\fR manpage (currently unwritten).
.PP
Before you launch yourself head first into the rest of this document, it would 
be a good idea to have read the following two documents \- the \fIperlapi\fR manpage and the \fIperlguts\fR manpage.
.PP
This stuff is easier to explain using examples. But first here are a few
definitions anyway.
.Sh "Definitions"
Perl has a number of C functions which allow you to call Perl subs. They are
.PP
.Vb 4
\&    I32 perl_call_sv(SV* sv, I32 flags) ;
\&    I32 perl_call_pv(char *subname, I32 flags) ;
\&    I32 perl_call_method(char *methname, I32 flags) ;
\&    I32 perl_call_argv(char *subname, I32 flags, register char **argv) ;
.Ve
The key function is \fIperl_call_sv\fR. All the other functions make use of
\fIperl_call_sv\fR to do what they do.
.PP
\fIperl_call_sv\fR takes two parameters, the first is an \s-1SV\s0*. This allows you to 
specify the Perl sub to be called either as a C string (which has first been 
converted to an \s-1SV\s0) or a reference to a 
sub. Example 7, shows you how you can make use of \fIperl_call_sv\fR.
The second parameter, \f(CWflags\fR, is a general purpose option command. 
This parameter is common to all the \fIperl_call_*\fR functions. 
It is discussed in the next section.
.PP
The function, \fIperl_call_pv\fR, is similar as \fIperl_call_sv\fR except it 
expects it's first parameter has to be a C char* which identifies the Perl 
sub you want to call, e.g. \f(CWperl_call_pv("fred", 0)\fR.
.PP
The function \fIperl_call_method\fR expects its first argument to contain a 
blessed reference to a class. Using that reference it looks up and calls \f(CWmethname\fR 
from that class. See example 9.
.PP
\fIperl_call_argv\fR calls the Perl sub specified by the \f(CWsubname\fR parameter. 
It also takes the usual \f(CWflags\fR parameter. 
The final parameter, \f(CWargv\fR, consists of a 
list of C strings to be sent to the Perl sub. See example 8.
.PP
All the functions return a number. This is a count of the number of items
returned by the Perl sub on the stack.
.PP
As a general rule you should \fIalways\fR check the return value from these 
functions.
Even if you are only expecting a particular number of values to be returned 
from the Perl sub, there is nothing to stop someone from doing something
unexpected \- don't say you havn't been warned.
.Sh "Flag Values"
The \f(CWflags\fR parameter in all the \fIperl_call_*\fR functions consists of any 
combination of the symbols defined below, \s-1OR\s0'ed together.
.Ip "G_SCALAR	" 5
Calls the Perl sub in a scalar context.
.Sp
Whatever the Perl sub actually returns, we only want a scalar. If the perl sub 
does return a scalar, the return value from the \fIperl_call_*\fR function 
will be 1 or 0. If 1, then the value actually returned by the Perl sub will 
be contained
on the top of the stack. 
If 0, then the sub has probably called \fIdie\fR or you have 
used the G_DISCARD flag.
.Sp
If the Perl sub returns a list, the \fIperl_call_*\fR function will still
only return 1 or 0. If 1, then the number of elements in the list 
will be stored on top of the stack.
The actual values of the list will not be accessable. 
.Sp
G_SCALAR is the default flag setting for all the functions.
.Ip "G_ARRAY	" 5
Calls the Perl sub in a list context.
.Sp
The return code from the \fIperl_call_*\fR functions will indicate how
many elements of the stack are used to store the array.
.Ip "G_DISCARD	" 5
If you are not interested in the values returned by the Perl sub then setting
this flag will make Perl get rid of them automatically for you. This will take
precedence to either G_SCALAR or G_ARRAY.
.Sp
If you do 
not set this flag then you may need to explicitly get rid of temporary values.
See example 3 for details.
.Ip "G_NOARGS	" 5
If you are not passing any parameters to the Perl sub, you can save a bit of 
time by setting this flag. It has the effect of of not creating the \f(CW@_\fR array 
for the Perl sub.
.Sp
A point worth noting is that if this flag is specified the Perl sub called can 
still access an \f(CW@_\fR array from a previous Perl sub. 
This functionality can be illustrated with the perl code below
.Sp
.Vb 2
\&        sub fred
\&          { print "@_\en"  }
.Ve
.Vb 2
\&        sub joe
\&          { &fred }
.Ve
.Vb 1
\&        &joe(1,2,3) ;
.Ve
This will print
.Sp
.Vb 1
\&        1 2 3
.Ve
What has happened is that \f(CWfred\fR accesses the \f(CW@_\fR array which belongs to \f(CWjoe\fR.
.Ip "G_EVAL	" 5
If the Perl sub you are calling has the ability to terminate 
abnormally, e.g. by calling \fIdie\fR or by not actually existing, and 
you want to catch this type of event, specify this flag setting. It will put 
an \fIeval { }\fR around the sub call.
.Sp
Whenever control returns from the \fIperl_call_*\fR function you need to
check the \f(CW$@\fR variable as you would in a normal Perl script. 
See example 6 for details of how to do this.
.SH "EXAMPLES"
Enough of the definition talk, let's have a few examples.
.PP
Perl provides many macros to assist in accessing the Perl stack. 
These macros should always be used when interfacing to Perl internals.
Hopefully this should make the code less vulnerable to changes made to
Perl in the future.
.PP
Another point worth noting is that in the first series of examples I have 
only made use of the \fIperl_call_pv\fR function. 
This has only been done to ease you into the 
topic. Wherever possible, if the choice is between using \fIperl_call_pv\fR 
and \fIperl_call_sv\fR, I would always try to use \fIperl_call_sv\fR.
.PP
The code for these examples is stored in the file \fIperlcall.tar\fR. 
(Once this document settles down, all the example code will be available in the file).
.Sh "Example1: No Parameters, Nothing returned"
This first trivial example will call a Perl sub, \fIPrintUID\fR, to print 
out the \s-1UID\s0 of the process. 
.PP
.Vb 4
\&    sub PrintUID
\&    {
\&        print "UID is $<\en" ;
\&    }
.Ve
and here is the C to call it
.PP
.Vb 4
\&    void
\&    call_PrintUID()
\&    {
\&        dSP ;
.Ve
.Vb 3
\&        PUSHMARK(sp) ;
\&        perl_call_pv("PrintUID", G_DISCARD|G_NOARGS) ;
\&    }
.Ve
Simple, eh. 
.PP
A few points to note about this example. 
.Ip "1. " 5
We aren't passing any parameters to \fIPrintUID\fR so G_NOARGS
can be specified.
.Ip "2." 5
Ignore \f(CWdSP\fR and \f(CWPUSHMARK(sp)\fR for now. They will be discussed in the next
example.
.Ip "3. " 5
We aren't interested in anything returned from \fIPrintUID\fR, so
G_DISCARD is specified. Even if \fIPrintUID\fR was changed to actually
return some \fIvalue\fR\|(s), having specified G_DISCARD will mean that they
will be wiped by the time control returns from \fIperl_call_pv\fR.
.Ip "4. " 5
Because we specified G_DISCARD, it is not necessary to check 
the value returned from \fIperl_call_sv\fR. It will always be 0.
.Ip "5." 5
As \fIperl_call_pv\fR is being used, the Perl sub is specified as a C string.
.Sh "Example 2: Passing Parameters"
Now let's make a slightly more complex example. This time we want 
to call a Perl sub
which will take 2 parameters \- a string (\f(CW$s\fR) and an integer (\f(CW$n\fR). 
The sub will simply print the first \f(CW$n\fR characters of the string.
.PP
So the Perl sub would look like this
.PP
.Vb 5
\&    sub LeftString
\&    {
\&        my($s, $n) = @_ ;
\&        print substr($s, 0, $n), "\en" ;
\&    }
.Ve
The C function required to call \fILeftString\fR would look like this.
.PP
.Vb 6
\&    static void
\&    call_LeftString(a, b)
\&    char * a ;
\&    int b ;
\&    {
\&        dSP ;
.Ve
.Vb 4
\&        PUSHMARK(sp) ;
\&        XPUSHs(sv_2mortal(newSVpv(a, 0)));
\&        XPUSHs(sv_2mortal(newSViv(b)));
\&        PUTBACK ;
.Ve
.Vb 2
\&        perl_call_pv("LeftString", G_DISCARD);
\&    }
.Ve
Here are a few notes on the C function \fIcall_LeftString\fR.
.Ip "1. " 5
The only flag specified this time is G_DISCARD. As we are passing 2 
parameters to the Perl sub this time, we have not specified G_NOARGS.
.Ip "2. " 5
Parameters are passed to the Perl sub using the Perl stack.
This is the purpose of the code beginning with the line \f(CWdSP\fR and ending
with the line \f(CWPUTBACK\fR.
.Ip "3." 5
If you are going to put something onto the Perl stack, you need to know
where to put it. This is the purpose of the macro \f(CWdSP\fR \-
it declares and initialises a local copy of the Perl stack pointer.
.Sp
All the other macros which will be used in this example require you to
have used this macro. 
.Sp
If you are calling a Perl sub directly from an \s-1XSUB\s0 function, it is 
not necessary to explicitly use the \f(CWdSP\fR macro \- it will be declared for you.
.Ip "4." 5
Any parameters to be pushed onto the stack should be bracketed by the
\f(CWPUSHMARK\fR and \f(CWPUTBACK\fR macros. 
The purpose of these two macros, in this context, is to automatically count
the number of parameters you are pushing. Then whenever Perl is creating
the \f(CW@_\fR array for the sub, it knows how big to make it.
.Sp
The \f(CWPUSHMARK\fR macro tells Perl to make a mental note of the current stack
pointer. Even if you aren't passing any parameters (like in Example 1) you must 
still call the \f(CWPUSHMARK\fR macro before you can call any of 
the \fIperl_call_*\fR functions \- Perl still needs to know that there are 
no parameters.
.Sp
The \f(CWPUTBACK\fR macro sets the global copy of the stack pointer to be the
same as our local copy. If we didn't do this \fIperl_call_pv\fR wouldn't
know where the two parameters we pushed were \- remember that up to now
all the stack pointer manipulation we have done is with our local copy,
\fInot\fR the global copy.
.Ip "5." 5
Next, we come to XPUSHs. This is where the parameters actually get
pushed onto the stack. In this case we are pushing a string and an integer.
.Sp
See the section \fI\s-1XSUB\s0's \s-1AND\s0  \s-1THE\s0 \s-1ARGUMENT\s0 \s-1STACK\s0\fR in the \fIperlguts\fR manpage for
details on how the \s-1XPUSH\s0 macros work.
.Ip "6." 5
Finally, \fILeftString\fR can now be called via the \fIperl_call_pv\fR function.
.Sh "Example 3: Returning a Scalar"
Now for an example of dealing with the values returned from a Perl sub.
.PP
Here is a Perl sub, \fIAdder\fR,  which takes 2 integer parameters and simply 
returns their sum.
.PP
.Vb 5
\&    sub Adder
\&    {
\&        my($a, $b) = @_ ;
\&        $a + $b ;
\&    }
.Ve
As we are now concerned with the return value from \fIAdder\fR, the C function
is now a bit more complex.
.PP
.Vb 7
\&    static void
\&    call_Adder(a, b)
\&    int a ;
\&    int b ;
\&    {
\&        dSP ;
\&        int count ;
.Ve
.Vb 2
\&        ENTER ;
\&        SAVETMPS;
.Ve
.Vb 4
\&        PUSHMARK(sp) ;
\&        XPUSHs(sv_2mortal(newSViv(a)));
\&        XPUSHs(sv_2mortal(newSViv(b)));
\&        PUTBACK ;
.Ve
.Vb 1
\&        count = perl_call_pv("Adder", G_SCALAR);
.Ve
.Vb 1
\&        SPAGAIN ;
.Ve
.Vb 2
\&        if (count != 1)
\&            croak("Big trouble\en") ;
.Ve
.Vb 1
\&        printf ("The sum of %d and %d is %d\en", a, b, POPi) ;
.Ve
.Vb 4
\&        PUTBACK ;
\&        FREETMPS ;
\&        LEAVE ;
\&    }
.Ve
Points to note this time are
.Ip "1. " 5
The only flag specified this time was G_SCALAR. That means the \f(CW@_\fR array
will be created and that the value returned by \fIAdder\fR will still
exist after the call to \fIperl_call_pv\fR.
.Ip "2." 5
Because we are interested in what is returned from \fIAdder\fR we cannot specify
G_DISCARD. This means that we will have to tidy up the Perl stack and dispose
of any temporary values ourselves. This is the purpose of 
.Sp
.Vb 2
\&        ENTER ;
\&        SAVETMPS ;
.Ve
at the start of the function, and
.Sp
.Vb 2
\&        FREETMPS ;
\&        LEAVE ;
.Ve
at the end. The \f(CWENTER\fR/\f(CWSAVETMPS\fR pair creates a boundary for any 
temporaries we create. 
This means that the temporaries we get rid of will be limited to those which
were created after these calls.
.Sp
The \f(CWFREETMPS\fR/\f(CWLEAVE\fR pair will get rid of any values returned by the Perl 
sub, plus it will also dump the mortal \s-1SV\s0's we created. 
Having \f(CWENTER\fR/\f(CWSAVETMPS\fR at the beginning
of the code makes sure that no other mortals are destroyed.
.Ip "3." 5
The purpose of the macro \f(CWSPAGAIN\fR is to refresh the local copy of the
stack pointer. This is necessary because it is possible that the memory
allocated to the Perl stack has been re-allocated whilst in the \fIperl_call_pv\fR
call.
.Sp
If you are making use of the Perl stack pointer in your code you must always
refresh the your local copy using \s-1SPAGAIN\s0 whenever you make use of
of the \fIperl_call_*\fR functions or any other Perl internal function.
.Ip "4. " 5
Although only a single value was expected to be returned from \fIAdder\fR, it is
still good practice to check the return code from \fIperl_call_pv\fR anyway.
.Sp
Expecting a single value is not quite the same as knowing that there will
be one. If someone modified \fIAdder\fR to return a list and we didn't check
for that possibility and take appropriate action the Perl stack would end 
up in an inconsistant state. That is something you \fIreally\fR don't want
to ever happen.
.Ip "5." 5
The \f(CWPOPi\fR macro is used here to pop the return value from the stack. In this
case we wanted an integer, so \f(CWPOPi\fR was used.
.Sp
Here is the complete list of \s-1POP\s0 macros available, along with the types they 
return.
.Sp
.Vb 5
\&        POPs    SV
\&        POPp    pointer
\&        POPn    double
\&        POPi    integer
\&        POPl    long
.Ve
.Ip "6." 5
The final \f(CWPUTBACK\fR is used to leave the Perl stack in a consistant state 
before exiting the function. This is
necessary because when we popped the return value from the stack with \f(CWPOPi\fR it
only updated our local copy of the stack pointer. Remember, \f(CWPUTBACK\fR sets the
global stack pointer to be the same as our local copy.
.Sh "Example 4: Returning a list of values"
Now, let's extend the previous example to return both the sum of the parameters 
and the difference.
.PP
Here is the Perl sub
.PP
.Vb 5
\&    sub AddSubtract
\&    {
\&       my($a, $b) = @_ ;
\&       ($a+$b, $a-$b) ;
\&    }
.Ve
and this is the C function
.PP
.Vb 7
\&    static void
\&    call_AddSubtract(a, b)
\&    int a ;
\&    int b ;
\&    {
\&        dSP ;
\&        int count ;
.Ve
.Vb 2
\&        ENTER ;
\&        SAVETMPS;
.Ve
.Vb 4
\&        PUSHMARK(sp) ;
\&        XPUSHs(sv_2mortal(newSViv(a)));
\&        XPUSHs(sv_2mortal(newSViv(b)));
\&        PUTBACK ;
.Ve
.Vb 1
\&        count = perl_call_pv("AddSubtract", G_ARRAY);
.Ve
.Vb 1
\&        SPAGAIN ;
.Ve
.Vb 2
\&        if (count != 2)
\&            croak("Big trouble\en") ;
.Ve
.Vb 2
\&        printf ("%d - %d = %d\en", a, b, POPi) ;
\&        printf ("%d + %d = %d\en", a, b, POPi) ;
.Ve
.Vb 4
\&        PUTBACK ;
\&        FREETMPS ;
\&        LEAVE ;
\&    }
.Ve
Notes
.Ip "1." 5
We wanted array context, so we used G_ARRAY.
.Ip "2." 5
Not surprisingly there are 2 POPi's this time  because we were retrieving 2
values from the stack. The main point to note is that they came off the stack in
reverse order.
.Sh "Example 5: Returning Data from Perl via the parameter list"
It is also possible to return values directly via the parameter list \-
whether it is actually desirable to do it is another matter entirely.
.PP
The Perl sub, \fIInc\fR, below takes 2 parameters and increments each.
.PP
.Vb 5
\&    sub Inc
\&    {
\&        ++ $_[0] ;
\&        ++ $_[1] ;
\&    }
.Ve
and here is a C function to call it.
.PP
.Vb 9
\&    static void
\&    call_Inc(a, b)
\&    int a ;
\&    int b ;
\&    {
\&        dSP ;
\&        int count ;
\&        SV * sva ;
\&        SV * svb ;
.Ve
.Vb 2
\&        ENTER ;
\&        SAVETMPS;
.Ve
.Vb 2
\&        sva = sv_2mortal(newSViv(a)) ;
\&        svb = sv_2mortal(newSViv(b)) ;
.Ve
.Vb 4
\&        PUSHMARK(sp) ;
\&        XPUSHs(sva);
\&        XPUSHs(svb);
\&        PUTBACK ;
.Ve
.Vb 1
\&        count = perl_call_pv("Inc", G_DISCARD);
.Ve
.Vb 2
\&        if (count != 0)
\&            croak ("call_Inc : expected 0 return value from 'Inc', got %d\en", count) ;
.Ve
.Vb 2
\&        printf ("%d + 1 = %d\en", a, SvIV(sva)) ;
\&        printf ("%d + 1 = %d\en", b, SvIV(svb)) ;
.Ve
.Vb 3
\&        FREETMPS ;
\&        LEAVE ; 
\&    }
.Ve
To be able to access the two parameters that were pushed onto the stack 
after they return from \fIperl_call_pv\fR it is necessary to make a note of
their addresses \- thus the two variables \f(CWsva\fR and \f(CWsvb\fR. 
.PP
The reason this is necessary is that
the area of the Perl stack which held them
will very likely have been overwritten by something else by the time control
returns from \fIperl_call_pv\fR.
.Sh "Example 6: Using G_EVAL"
Now an example using G_EVAL. Below is a Perl sub which computes the 
difference of its 2 parameters. If this would result in a negative result,
the sub calls \fIdie\fR.
.PP
.Vb 3
\&    sub Subtract
\&    {
\&        my ($a, $b) = @_ ;
.Ve
.Vb 1
\&        die "death can be fatal\en" if $a < $b ;
.Ve
.Vb 2
\&        $a - $b ;
\&    }
.Ve
and some C to call it
.PP
.Vb 8
\&    static void
\&    call_Subtract(a, b)
\&    int a ;
\&    int b ;
\&    {
\&        dSP ;
\&        int count ;
\&        SV * sv ;
.Ve
.Vb 2
\&        ENTER ;
\&        SAVETMPS;
.Ve
.Vb 4
\&        PUSHMARK(sp) ;
\&        XPUSHs(sv_2mortal(newSViv(a)));
\&        XPUSHs(sv_2mortal(newSViv(b)));
\&        PUTBACK ;
.Ve
.Vb 1
\&        count = perl_call_pv("Subtract", G_EVAL|G_SCALAR);
.Ve
.Vb 4
\&        /* Check the eval first */
\&        sv = GvSV(gv_fetchpv("@", TRUE, SVt_PV));
\&        if (SvTRUE(sv))
\&            printf ("Uh oh - %s\en", SvPV(sv, na)) ;
.Ve
.Vb 1
\&        SPAGAIN ;
.Ve
.Vb 2
\&        if (count != 1)
\&            croak ("call_Subtract : expected 1 return value from 'Subtract', got %d\en", count) ;
.Ve
.Vb 1
\&        printf ("%d - %d = %d\en", a, b, POPi) ;
.Ve
.Vb 3
\&        PUTBACK ;
\&        FREETMPS ;
\&        LEAVE ;
.Ve
.Vb 1
\&    }
.Ve
If \fIcall_Subtract\fR is called thus
.PP
.Vb 1
\&        call_Subtract(4, 5)
.Ve
the following will be printed
.PP
.Vb 1
\&        Uh oh - death can be fatal
.Ve
Notes
.Ip "1." 5
We want to be able to catch the \fIdie\fR so we have used the G_EVAL flag.
Not specifying this flag would mean that the program would terminate.
.Ip "2." 5
The code 
.Sp
.Vb 3
\&        sv = GvSV(gv_fetchpv("@", TRUE, SVt_PV));
\&        if (SvTRUE(sv))
\&            printf ("Uh oh - %s\en", SvPVx(sv, na)) ;
.Ve
is the equivalent of this bit of Perl
.Sp
.Vb 1
\&        print "Uh oh - $@\en" if $@ ;
.Ve
.Sh "Example 7: Using perl_call_sv"
In all the previous examples I have \*(L'hard-wried\*(R' the name of the Perl sub to
be called from C. 
Sometimes though, it is necessary to be able to specify the name
of the Perl sub from within the Perl script.
.PP
Consider the Perl code below
.PP
.Vb 4
\&        sub fred
\&        {
\&            print "Hello there\en" ;
\&        }
.Ve
.Vb 1
\&        CallSub("fred") ;
.Ve
here is a snippet of \s-1XSUB\s0 which defines \fICallSub\fR.
.PP
.Vb 6
\&        void
\&        CallSub(name)
\&                char *  name
\&                CODE:
\&                PUSHMARK(sp) ;
\&                perl_call_pv(name, G_DISCARD|G_NOARGS) ;
.Ve
That is fine as far as it goes. The thing is, it only allows the Perl sub to be
specified as a string. 
For perl 4 this was adequate, but Perl 5 allows references to 
subs and anonymous subs. This is where \fIperl_call_sv\fR is useful.
.PP
The code below for \fICallSub\fR is identical to the previous time except that the
\f(CWname\fR parameter is now defined as an \s-1SV\s0* and we use \fIperl_call_sv\fR instead of
\fIperl_call_pv\fR.
.PP
.Vb 6
\&        void
\&        CallSub(name)
\&                SV*     name
\&                CODE:
\&                PUSHMARK(sp) ;
\&                perl_call_sv(name, G_DISCARD|G_NOARGS) ;
.Ve
As we are using an \s-1SV\s0 to call \fIfred\fR the following can all be used
.PP
.Vb 5
\&        CallSub("fred") ;
\&        Callsub(\e&fred) ;
\&        $ref = \e&fred ;
\&        CallSub($ref) ;
\&        CallSub( sub { print "Hello there\en" } ) ;
.Ve
As you can see, \fIperl_call_sv\fR gives you greater flexibility in how you 
can specify the Perl sub.
.Sh "Example 8: Using perl_call_argv"
Here is a Perl sub which prints whatever parameters are passed to it.
.PP
.Vb 3
\&        sub PrintList
\&        {
\&            my(@list) = @_ ;
.Ve
.Vb 2
\&            foreach (@list) { print "$_\en" }
\&        }
.Ve
and here is an example of \fIperl_call_argv\fR which will call \fIPrintList\fR.
.PP
.Vb 4
\&        call_PrintList
\&        {
\&            dSP ;
\&            char * words[] = {"alpha", "beta", "gamma", "delta", NULL } ;
.Ve
.Vb 2
\&            perl_call_argv("PrintList", words, G_DISCARD) ;
\&        }
.Ve
Note that it is not necessary to call \f(CWPUSHMARK\fR in this instance. This is
because \fIperl_call_argv\fR will do it for you.
.Sh "Example 9: Using perl_call_method"
[This section is under construction]
.PP
Consider the following Perl code
.PP
.Vb 2
\&        {
\&          package Mine ;
.Ve
.Vb 3
\&          sub new     { bless [@_] }
\&          sub Display { print $_[0][1], "\en" }
\&        }
.Ve
.Vb 2
\&        $a = new Mine ('red', 'green', 'blue') ;
\&        call_Display($a, 'Display') ;
.Ve
The method \f(CWDisplay\fR just prints out the first element of the list.
Here is a \s-1XSUB\s0 implementation of \fIcall_Display\fR.
.PP
.Vb 8
\&        void
\&        call_Display(ref, method)
\&            SV *    ref
\&            char *  method
\&            CODE:
\&            PUSHMARK(sp);
\&            XPUSHs(ref);
\&            PUTBACK;
.Ve
.Vb 1
\&            perl_call_method(method, G_DISCARD) ;
.Ve
.Sh "Strategies for storing Context Information"
[This section is under construction]
.PP
One of the trickiest problems to overcome when designing a callback interface 
is figuring 
out how to store the mapping between the C callback functions and the 
Perl equivalent.
.PP
Consider the following example.
.Sh "Alternate Stack Manipulation"
[This section is under construction]
.PP
Although I have only made use of the \s-1POP\s0* macros to access values returned 
from Perl subs, it is also possible to bypass these macros and read the 
stack directly.
.PP
The code below is example 4 recoded to 
.SH "SEE ALSO"
the \fIperlapi\fR manpage, the \fIperlguts\fR manpage, the \fIperlembed\fR manpage
.SH "AUTHOR"
Paul Marquess <pmarquess@bfsec.bt.co.uk>
.PP
Special thanks to the following people who assisted in the creation of the 
document.
.PP
Jeff Okamoto, Tim Bunce.
.SH "DATE"
Version 0.4, 17th October 1994

.rn }` ''

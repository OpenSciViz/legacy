.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLREF 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlref \- Perl references and nested data structures
.SH "DESCRIPTION"
In Perl 4 it was difficult to represent complex data structures, because
all references had to be symbolic, and even that was difficult to do when
you wanted to refer to a variable rather than a symbol table entry.  Perl
5 not only makes it easier to use symbolic references to variables, but
lets you have \*(L"hard\*(R" references to any piece of data.  Any scalar may hold
a hard reference.  Since arrays and hashes contain scalars, you can now
easily build arrays of arrays, arrays of hashes, hashes of arrays, arrays
of hashes of functions, and so on.
.PP
Hard references are smart\*(--they keep track of reference counts for you,
automatically freeing the thing referred to when its reference count
goes to zero.  If that thing happens to be an object, the object is
destructed.  See the \fIperlobj\fR manpage for more about objects.  (In a sense,
everything in Perl is an object, but we usually reserve the word for
references to objects that have been officially \*(L"blessed\*(R" into a class package.)
.PP
A symbolic reference contains the name of a variable, just as a
symbolic link in the filesystem merely contains the name of a file.  
The \f(CW*glob\fR notation is a kind of symbolic reference.  Hard references
are more like hard links in the file system: merely another way
at getting at the same underlying object, irrespective of its name.
.PP
\*(L"Hard\*(R" references are easy to use in Perl.  There is just one
overriding principle:  Perl does no implicit referencing or
dereferencing.  When a scalar is holding a reference, it always behaves
as a scalar.  It doesn't magically start being an array or a hash
unless you tell it so explicitly by dereferencing it.
.PP
References can be constructed several ways.
.Ip "1." 4
By using the backslash operator on a variable, subroutine, or value.
(This works much like the & (address-of) operator works in C.)  Note
that this typically creates \fI\s-1ANOTHER\s0\fR reference to a variable, since
there's already a reference to the variable in the symbol table.  But
the symbol table reference might go away, and you'll still have the
reference that the backslash returned.  Here are some examples:
.Sp
.Vb 4
\&    $scalarref = \e$foo;
\&    $arrayref  = \e@ARGV;
\&    $hashref   = \e%ENV;
\&    $coderef   = \e&handler;
.Ve
.Ip "2." 4
A reference to an anonymous array can be constructed using square
brackets:
.Sp
.Vb 1
\&    $arrayref = [1, 2, ['a', 'b', 'c']];
.Ve
Here we've constructed a reference to an anonymous array of three elements
whose final element is itself reference to another anonymous array of three
elements.  (The multidimensional syntax described later can be used to
access this.  For example, after the above, \f(CW$arrayref\fR\->[2][1] would have
the value \*(L"b\*(R".)
.Ip "3." 4
A reference to an anonymous hash can be constructed using curly
brackets:
.Sp
.Vb 4
\&    $hashref = {
\&        'Adam'  => 'Eve',
\&        'Clyde' => 'Bonnie',
\&    };
.Ve
Anonymous hash and array constructors can be intermixed freely to
produce as complicated a structure as you want.  The multidimensional
syntax described below works for these too.  The values above are
literals, but variables and expressions would work just as well, because
assignment operators in Perl (even within \fIlocal()\fR or \fImy()\fR) are executable
statements, not compile-time declarations.
.Sp
Because curly brackets (braces) are used for several other things
including BLOCKs, you may occasionally have to disambiguate braces at the
beginning of a statement by putting a \f(CW+\fR or a \f(CWreturn\fR in front so
that Perl realizes the opening brace isn't starting a \s-1BLOCK\s0.  The economy and
mnemonic value of using curlies is deemed worth this occasional extra
hassle.
.Sp
For example, if you wanted a function to make a new hash and return a
reference to it, you have these options:
.Sp
.Vb 3
\&    sub hashem {        { @_ } }   # silently wrong
\&    sub hashem {       +{ @_ } }   # ok
\&    sub hashem { return { @_ } }   # ok
.Ve
.Ip "4." 4
A reference to an anonymous subroutine can be constructed by using
\f(CWsub\fR without a subname:
.Sp
.Vb 1
\&    $coderef = sub { print "Boink!\en" };
.Ve
Note the presence of the semicolon.  Except for the fact that the code
inside isn't executed immediately, a \f(CWsub {}\fR is not so much a
declaration as it is an operator, like \f(CWdo{}\fR or \f(CWeval{}\fR.  (However, no
matter how many times you execute that line (unless you're in an
\f(CWeval("...")\fR), \f(CW$coderef\fR will still have a reference to the \fI\s-1SAME\s0\fR
anonymous subroutine.)
.Sp
Anonymous subroutines act as closures with respect to \fImy()\fR variables,
that is, variables visible lexically within the current scope.  Closure
is a notion out of the Lisp world that says if you define an anonymous
function in a particular lexical context, it pretends to run in that
context even when it's called outside of the context.
.Sp
In human terms, it's a funny way of passing arguments to a subroutine when
you define it as well as when you call it.  It's useful for setting up
little bits of code to run later, such as callbacks.  You can even
do object-oriented stuff with it, though Perl provides a different
mechanism to do that already\*(--see the \fIperlobj\fR manpage.
.Sp
You can also think of closure as a way to write a subroutine template without
using eval.  (In fact, in version 5.000, eval was the \fIonly\fR way to get
closures.  You may wish to use \*(L"require 5.001\*(R" if you use closures.)
.Sp
Here's a small example of how closures works:
.Sp
.Vb 6
\&    sub newprint {
\&        my $x = shift;
\&        return sub { my $y = shift; print "$x, $y!\en"; };
\&    }
\&    $h = newprint("Howdy");
\&    $g = newprint("Greetings");
.Ve
.Vb 1
\&    # Time passes...
.Ve
.Vb 2
\&    &$h("world");
\&    &$g("earthlings");
.Ve
This prints
.Sp
.Vb 2
\&    Howdy, world!
\&    Greetings, earthlings!
.Ve
Note particularly that \f(CW$x\fR continues to refer to the value passed into
\fInewprint()\fR *despite* the fact that the \*(L"my \f(CW$x\fR\*(R" has seemingly gone out of
scope by the time the anonymous subroutine runs.  That's what closure
is all about.
.Sp
This only applies to lexical variables, by the way.  Dynamic variables
continue to work as they have always worked.  Closure is not something
that most Perl programmers need trouble themselves about to begin with.
.Ip "5." 4
References are often returned by special subroutines called constructors.
Perl objects are just references to a special kind of object that happens to know
which package it's associated with.  Constructors are just special
subroutines that know how to create that association.  They do so by
starting with an ordinary reference, and it remains an ordinary reference
even while it's also being an object.  Constructors are customarily
named \fInew()\fR, but don't have to be:
.Sp
.Vb 1
\&    $objref = new Doggie (Tail => 'short', Ears => 'long');
.Ve
.Ip "6." 4
References of the appropriate type can spring into existence if you
dereference them in a context that assumes they exist.  Since we haven't
talked about dereferencing yet, we can't show you any examples yet.
.PP
That's it for creating references.  By now you're probably dying to
know how to use references to get back to your long-lost data.  There
are several basic methods.
.Ip "1." 4
Anywhere you'd put an identifier as part of a variable or subroutine
name, you can replace the identifier with a simple scalar variable
containing a reference of the correct type:
.Sp
.Vb 5
\&    $bar = $$scalarref;
\&    push(@$arrayref, $filename);
\&    $$arrayref[0] = "January";
\&    $$hashref{"KEY"} = "VALUE";
\&    &$coderef(1,2,3);
.Ve
It's important to understand that we are specifically \fI\s-1NOT\s0\fR dereferencing
\f(CW$arrayref[0]\fR or \f(CW$hashref{"KEY"}\fR there.  The dereference of the
scalar variable happens \fI\s-1BEFORE\s0\fR it does any key lookups.  Anything more
complicated than a simple scalar variable must use methods 2 or 3 below.
However, a \*(L"simple scalar\*(R" includes an identifier that itself uses method
1 recursively.  Therefore, the following prints \*(L"howdy\*(R".
.Sp
.Vb 2
\&    $refrefref = \e\e\e"howdy";
\&    print $$$$refrefref;
.Ve
.Ip "2." 4
Anywhere you'd put an identifier as part of a variable or subroutine
name, you can replace the identifier with a \s-1BLOCK\s0 returning a reference
of the correct type.  In other words, the previous examples could be
written like this:
.Sp
.Vb 5
\&    $bar = ${$scalarref};
\&    push(@{$arrayref}, $filename);
\&    ${$arrayref}[0] = "January";
\&    ${$hashref}{"KEY"} = "VALUE";
\&    &{$coderef}(1,2,3);
.Ve
Admittedly, it's a little silly to use the curlies in this case, but
the \s-1BLOCK\s0 can contain any arbitrary expression, in particular,
subscripted expressions:
.Sp
.Vb 1
\&    &{ $dispatch{$index} }(1,2,3);      # call correct routine 
.Ve
Because of being able to omit the curlies for the simple case of \f(CW$$x\fR,
people often make the mistake of viewing the dereferencing symbols as
proper operators, and wonder about their precedence.  If they were,
though, you could use parens instead of braces.  That's not the case.
Consider the difference below; case 0 is a short-hand version of case 1,
\fI\s-1NOT\s0\fR case 2:
.Sp
.Vb 4
\&    $$hashref{"KEY"}   = "VALUE";       # CASE 0
\&    ${$hashref}{"KEY"} = "VALUE";       # CASE 1
\&    ${$hashref{"KEY"}} = "VALUE";       # CASE 2
\&    ${$hashref->{"KEY"}} = "VALUE";     # CASE 3
.Ve
Case 2 is also deceptive in that you're accessing a variable
called \f(CW%hashref\fR, not dereferencing through \f(CW$hashref\fR to the hash
it's presumably referencing.  That would be case 3.
.Ip "3." 4
The case of individual array elements arises often enough that it gets
cumbersome to use method 2.  As a form of syntactic sugar, the two
lines like that above can be written:
.Sp
.Vb 2
\&    $arrayref->[0] = "January";
\&    $hashref->{"KEY"} = "VALUE";
.Ve
The left side of the array can be any expression returning a reference,
including a previous dereference.  Note that \f(CW$array[$x]\fR is \fI\s-1NOT\s0\fR the
same thing as \f(CW$array->[$x]\fR here:
.Sp
.Vb 1
\&    $array[$x]->{"foo"}->[0] = "January";
.Ve
This is one of the cases we mentioned earlier in which references could
spring into existence when in an lvalue context.  Before this
statement, \f(CW$array[$x]\fR may have been undefined.  If so, it's
automatically defined with a hash reference so that we can look up
\f(CW{"foo"}\fR in it.  Likewise \f(CW$array[$x]->{"foo"}\fR will automatically get
defined with an array reference so that we can look up \f(CW[0]\fR in it.
.Sp
One more thing here.  The arrow is optional \fI\s-1BETWEEN\s0\fR brackets
subscripts, so you can shrink the above down to
.Sp
.Vb 1
\&    $array[$x]{"foo"}[0] = "January";
.Ve
Which, in the degenerate case of using only ordinary arrays, gives you
multidimensional arrays just like C's:
.Sp
.Vb 1
\&    $score[$x][$y][$z] += 42;
.Ve
Well, okay, not entirely like C's arrays, actually.  C doesn't know how
to grow its arrays on demand.  Perl does.
.Ip "4." 4
If a reference happens to be a reference to an object, then there are
probably methods to access the things referred to, and you should probably
stick to those methods unless you're in the class package that defines the
object's methods.  In other words, be nice, and don't violate the object's
encapsulation without a very good reason.  Perl does not enforce
encapsulation.  We are not totalitarians here.  We do expect some basic
civility though.
.PP
The \fIref()\fR operator may be used to determine what type of thing the
reference is pointing to.  See the \fIperlfunc\fR manpage.
.PP
The \fIbless()\fR operator may be used to associate a reference with a package
functioning as an object class.  See the \fIperlobj\fR manpage.
.PP
A type glob may be dereferenced the same way a reference can, since
the dereference syntax always indicates the kind of reference desired.
So \f(CW${*foo}\fR and \f(CW${\e$foo}\fR both indicate the same scalar variable.
.PP
Here's a trick for interpolating a subroutine call into a string:
.PP
.Vb 1
\&    print "My sub returned ${\emysub(1,2,3)}\en";
.Ve
The way it works is that when the \f(CW${...}\fR is seen in the double-quoted
string, it's evaluated as a block.  The block executes the call to
\f(CWmysub(1,2,3)\fR, and then takes a reference to that.  So the whole block
returns a reference to a scalar, which is then dereferenced by \f(CW${...}\fR
and stuck into the double-quoted string.
.Sh "Symbolic references"
We said that references spring into existence as necessary if they are
undefined, but we didn't say what happens if a value used as a
reference is already defined, but \fI\s-1ISN\s0'T\fR a hard reference.  If you
use it as a reference in this case, it'll be treated as a symbolic
reference.  That is, the value of the scalar is taken to be the \fI\s-1NAME\s0\fR
of a variable, rather than a direct link to a (possibly) anonymous
value.
.PP
People frequently expect it to work like this.  So it does.
.PP
.Vb 9
\&    $name = "foo";
\&    $$name = 1;                 # Sets $foo
\&    ${$name} = 2;               # Sets $foo
\&    ${$name x 2} = 3;           # Sets $foofoo
\&    $name->[0] = 4;             # Sets $foo[0]
\&    @$name = ();                # Clears @foo
\&    &$name();                   # Calls &foo() (as in Perl 4)
\&    $pack = "THAT";
\&    ${"${pack}::$name"} = 5;    # Sets $THAT::foo without eval
.Ve
This is very powerful, and slightly dangerous, in that it's possible
to intend (with the utmost sincerity) to use a hard reference, and
accidentally use a symbolic reference instead.  To protect against
that, you can say
.PP
.Vb 1
\&    use strict 'refs';
.Ve
and then only hard references will be allowed for the rest of the enclosing
block.  An inner block may countermand that with 
.PP
.Vb 1
\&    no strict 'refs';
.Ve
Only package variables are visible to symbolic references.  Lexical
variables (declared with \fImy()\fR) aren't in a symbol table, and thus are
invisible to this mechanism.  For example:
.PP
.Vb 6
\&    local($value) = 10;
\&    $ref = \e$value;
\&    {
\&        my $value = 20;
\&        print $$ref;
\&    } 
.Ve
This will still print 10, not 20.  Remember that \fIlocal()\fR affects package
variables, which are all \*(L"global\*(R" to the package.
.Sh "Not-so-symbolic references"
A new feature contributing to readability in 5.001 is that the brackets
around a symbolic reference behave more like quotes, just as they
always have within a string.  That is,
.PP
.Vb 2
\&    $push = "pop on ";
\&    print "${push}over";
.Ve
has always meant to print \*(L"pop on over\*(R", despite the fact that push is
a reserved word.  This has been generalized to work the same outside
of quotes, so that
.PP
.Vb 1
\&    print ${push} . "over";
.Ve
and even
.PP
.Vb 1
\&    print ${ push } . "over";
.Ve
will have the same effect.  (This would have been a syntax error in
5.000, though Perl 4 allowed it in the spaceless form.)  Note that this
construct is \fInot\fR considered to be a symbolic reference when you're
using strict refs:
.PP
.Vb 3
\&    use strict 'refs';
\&    ${ bareword };      # Okay, means $bareword.
\&    ${ "bareword" };    # Error, symbolic reference.
.Ve
Similarly, because of all the subscripting that is done using single
words, we've applied the same rule to any bareword that is used for
subscripting a hash.  So now, instead of writing
.PP
.Vb 1
\&    $array{ "aaa" }{ "bbb" }{ "ccc" }
.Ve
you can just write
.PP
.Vb 1
\&    $array{ aaa }{ bbb }{ ccc }
.Ve
and not worry about whether the subscripts are reserved words.  In the
rare event that you do wish to do something like
.PP
.Vb 1
\&    $array{ shift }
.Ve
you can force interpretation as a reserved word by adding anything that
makes it more than a bareword:
.PP
.Vb 3
\&    $array{ shift() }
\&    $array{ +shift }
\&    $array{ shift @_ }
.Ve
The \fB\-w\fR switch will warn you if it interprets a reserved word as a string.
But it will no longer warn you about using lowercase words, since the
string is effectively quoted.
.Sh "\s-1WARNING\s0"
You may not (usefully) use a reference as the key to a hash.  It will be
converted into a string:
.PP
.Vb 1
\&    $x{ \e$a } = $a;
.Ve
If you try to dereference the key, it won't do a hard dereference, and 
you won't accomplish what you're attemping.
.Sh "Further Reading"
Besides the obvious documents, source code can be instructive.
Some rather pathological examples of the use of references can be found
in the \fIt/op/ref.t\fR regression test in the Perl source directory.

.rn }` ''

.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLDATA 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perldata \- Perl data structures
.SH "DESCRIPTION"
.Sh "Variable names"
Perl has three data structures: scalars, arrays of scalars, and
associative arrays of scalars, known as \*(L"hashes\*(R".  Normal arrays are
indexed by number, starting with 0.  (Negative subscripts count from
the end.)  Hash arrays are indexed by string.
.PP
Scalar values are always named with \*(L'$\*(R', even when referring to a scalar
that is part of an array.  It works like the English word \*(L"the\*(R".  Thus
we have:
.PP
.Vb 4
\&    $days               # the simple scalar value "days"
\&    $days[28]           # the 29th element of array @days
\&    $days{'Feb'}        # the 'Feb' value from hash %days
\&    $#days              # the last index of array @days
.Ve
but entire arrays or array slices are denoted by \*(L'@\*(R', which works much like
the word \*(L"these\*(R" or \*(L"those":
.PP
.Vb 3
\&    @days               # ($days[0], $days[1],... $days[n])
\&    @days[3,4,5]        # same as @days[3..5]
\&    @days{'a','c'}      # same as ($days{'a'},$days{'c'})
.Ve
and entire hashes are denoted by \*(L'%':
.PP
.Vb 1
\&    %days               # (key1, val1, key2, val2 ...)
.Ve
In addition, subroutines are named with an initial \*(L'&\*(R', though this is
optional when it's otherwise unambiguous (just as \*(L"do\*(R" is often
redundant in English).  Symbol table entries can be named with an
initial \*(L'*\*(R', but you don't really care about that yet.
.PP
Every variable type has its own namespace.  You can, without fear of
conflict, use the same name for a scalar variable, an array, or a hash
(or, for that matter, a filehandle, a subroutine name, or a label).
This means that \f(CW$foo\fR and \f(CW@foo\fR are two different variables.  It also
means that \f(CW$foo[1]\fR is a part of \f(CW@foo\fR, not a part of \f(CW$foo\fR.  This may
seem a bit weird, but that's okay, because it is weird.
.PP
Since variable and array references always start with \*(L'$\*(R', \*(L'@\*(R', or \*(L'%\*(R',
the \*(L"reserved\*(R" words aren't in fact reserved with respect to variable
names.  (They \s-1ARE\s0 reserved with respect to labels and filehandles,
however, which don't have an initial special character.  You can't have
a filehandle named \*(L"log\*(R", for instance.  Hint: you could say
\f(CWopen(LOG,'logfile')\fR rather than \f(CWopen(log,'logfile')\fR.  Using uppercase
filehandles also improves readability and protects you from conflict
with future reserved words.)  Case \fI\s-1IS\s0\fR significant\*(--"\s-1FOO\s0\*(R", \*(L"Foo\*(R" and
\*(L"foo\*(R" are all different names.  Names that start with a letter or
underscore may also contain digits and underscores.
.PP
It is possible to replace such an alphanumeric name with an expression
that returns a reference to an object of that type.  For a description
of this, see the \fIperlref\fR manpage.
.PP
Names that start with a digit may only contain more digits.  Names
which do not start with a letter, underscore,  or digit are limited to
one character, e.g.  \*(L"$%\*(R" or \*(L"$$\*(R".  (Most of these one character names
have a predefined significance to Perl.  For instance, $$ is the
current process id.)
.Sh "Context"
The interpretation of operations and values in Perl sometimes depends
on the requirements of the context around the operation or value.
There are two major contexts: scalar and list.  Certain operations
return list values in contexts wanting a list, and scalar values
otherwise.  (If this is true of an operation it will be mentioned in
the documentation for that operation.)  In other words, Perl overloads
certain operations based on whether the expected return value is
singular or plural.  (Some words in English work this way, like \*(L"fish\*(R"
and \*(L"sheep\*(R".)
.PP
In a reciprocal fashion, an operation provides either a scalar or a
list context to each of its arguments.  For example, if you say
.PP
.Vb 1
\&    int( <STDIN> )
.Ve
the integer operation provides a scalar context for the <\s-1STDIN\s0>
operator, which responds by reading one line from \s-1STDIN\s0 and passing it
back to the integer operation, which will then find the integer value
of that line and return that.  If, on the other hand, you say
.PP
.Vb 1
\&    sort( <STDIN> )
.Ve
then the sort operation provides a list context for <\s-1STDIN\s0>, which
will proceed to read every line available up to the end of file, and
pass that list of lines back to the sort routine, which will then
sort those lines and return them as a list to whatever the context
of the sort was.
.PP
Assignment is a little bit special in that it uses its left argument to
determine the context for the right argument.  Assignment to a scalar
evaluates the righthand side in a scalar context, while assignment to
an array or array slice evaluates the righthand side in a list
context.  Assignment to a list also evaluates the righthand side in a
list context.
.PP
User defined subroutines may choose to care whether they are being
called in a scalar or list context, but most subroutines do not
need to care, because scalars are automatically interpolated into
lists.  See the \f(CWwantarray\fR entry in the \fIperlfunc\fR manpage.
.Sh "Scalar values"
Scalar variables may contain various kinds of singular data, such as
numbers, strings and references.  In general, conversion from one form
to another is transparent.  (A scalar may not contain multiple values,
but may contain a reference to an array or hash containing multiple
values.)  Because of the automatic conversion of scalars, operations and
functions that return scalars don't need to care (and, in fact, can't
care) whether the context is looking for a string or a number.
.PP
A scalar value is interpreted as \s-1TRUE\s0 in the Boolean sense if it is not
the null string or the number 0 (or its string equivalent, \*(L"0").  The
Boolean context is just a special kind of scalar context.
.PP
There are actually two varieties of null scalars: defined and
undefined.  Undefined null scalars are returned when there is no real
value for something, such as when there was an error, or at end of
file, or when you refer to an uninitialized variable or element of an
array.  An undefined null scalar may become defined the first time you
use it as if it were defined, but prior to that you can use the
\fIdefined()\fR operator to determine whether the value is defined or not.
.PP
The length of an array is a scalar value.  You may find the length of
array \f(CW@days\fR by evaluating \f(CW$#days\fR, as in \fBcsh\fR.  (Actually, it's not
the length of the array, it's the subscript of the last element, since
there is (ordinarily) a 0th element.)  Assigning to \f(CW$#days\fR changes the
length of the array.  Shortening an array by this method destroys
intervening values.  Lengthening an array that was previously shortened
\fI\s-1NO\s0 \s-1LONGER\s0\fR recovers the values that were in those elements.  (It used to
in Perl 4, but we had to break this make to make sure destructors were
called when expected.)  You can also gain some measure of efficiency by
preextending an array that is going to get big.  (You can also extend
an array by assigning to an element that is off the end of the array.)
You can truncate an array down to nothing by assigning the null list ()
to it.  The following are equivalent:
.PP
.Vb 2
\&    @whatever = ();
\&    $#whatever = $[ - 1;
.Ve
If you evaluate a named array in a scalar context, it returns the length of
the array.  (Note that this is not true of lists, which return the
last value, like the C comma operator.)  The following is always true:
.PP
.Vb 1
\&    scalar(@whatever) == $#whatever - $[ + 1;
.Ve
Version 5 of Perl changed the semantics of $[: files that don't set
the value of $[ no longer need to worry about whether another
file changed its value.  (In other words, use of $[ is deprecated.)
So in general you can just assume that
.PP
.Vb 1
\&    scalar(@whatever) == $#whatever + 1;
.Ve
If you evaluate a hash in a scalar context, it returns a value which is
true if and only if the hash contains any key/value pairs.  (If there
are any key/value pairs, the value returned is a string consisting of
the number of used buckets and the number of allocated buckets, separated
by a slash.  This is pretty much only useful to find out whether Perl's
(compiled in) hashing algorithm is performing poorly on your data set.
For example, you stick 10,000 things in a hash, but evaluating \f(CW%HASH\fR in
scalar context reveals \*(L"1/16\*(R", which means only one out of sixteen buckets
has been touched, and presumably contains all 10,000 of your items.  This
isn't supposed to happen.)
.Sh "Scalar value constructors"
Numeric literals are specified in any of the customary floating point or
integer formats:
.PP
.Vb 6
\&    12345
\&    12345.67
\&    .23E-10
\&    0xffff              # hex
\&    0377                # octal
\&    4_294_967_296       # underline for legibility
.Ve
String literals are delimited by either single or double quotes.  They
work much like shell quotes:  double-quoted string literals are subject
to backslash and variable substitution; single-quoted strings are not
(except for \*(L"\f(CW\e'\fR\*(R" and \*(L"\f(CW\e\e\fR").  The usual Unix backslash rules apply for making
characters such as newline, tab, etc., as well as some more exotic
forms.  See the \f(CWqq\fR entry in the \fIperlop\fR manpage for a list.
.PP
You can also embed newlines directly in your strings, i.e. they can end
on a different line than they begin.  This is nice, but if you forget
your trailing quote, the error will not be reported until Perl finds
another line containing the quote character, which may be much further
on in the script.  Variable substitution inside strings is limited to
scalar variables, arrays, and array slices.  (In other words,
identifiers beginning with $ or @, followed by an optional bracketed
expression as a subscript.)  The following code segment prints out \*(L"The
price is \f(CW$100\fR.\*(R"
.PP
.Vb 2
\&    $Price = '$100';    # not interpreted
\&    print "The price is $Price.\en";     # interpreted
.Ve
As in some shells, you can put curly brackets around the identifier to
delimit it from following alphanumerics.  In fact, an identifier
within such curlies is forced to be a string, as is any single
identifier within a hash subscript.  Our earlier example,
.PP
.Vb 1
\&    $days{'Feb'}
.Ve
can be written as
.PP
.Vb 1
\&    $days{Feb}
.Ve
and the quotes will be assumed automatically.  But anything more complicated
in the subscript will be interpreted as an expression.
.PP
Note that a
single-quoted string must be separated from a preceding word by a
space, since single quote is a valid (though deprecated) character in
an identifier (see the \f(CWPackages\fR entry in the \fIperlmod\fR manpage).
.PP
Two special literals are _\|_LINE_\|_ and _\|_FILE_\|_, which represent the
current line number and filename at that point in your program.  They
may only be used as separate tokens; they will not be interpolated into
strings.  In addition, the token _\|_END_\|_ may be used to indicate the
logical end of the script before the actual end of file.  Any following
text is ignored, but may be read via the \s-1DATA\s0 filehandle.  (The \s-1DATA\s0
filehandle may read data only from the main script, but not from any
required file or evaluated string.)  The two control characters ^D and
^Z are synonyms for _\|_END_\|_.
.PP
A word that has no other interpretation in the grammar will
be treated as if it were a quoted string.  These are known as
\*(L"barewords\*(R".  As with filehandles and labels, a bareword that consists
entirely of lowercase letters risks conflict with future reserved
words, and if you use the \fB\-w\fR switch, Perl will warn you about any
such words.  Some people may wish to outlaw barewords entirely.  If you
say
.PP
.Vb 1
\&    use strict 'subs';
.Ve
then any bareword that would \s-1NOT\s0 be interpreted as a subroutine call
produces a compile-time error instead.  The restriction lasts to the
end of the enclosing block.  An inner block may countermand this 
by saying \f(CWno strict 'subs'\fR.
.PP
Array variables are interpolated into double-quoted strings by joining all
the elements of the array with the delimiter specified in the \f(CW$"\fR
variable, space by default.  The following are equivalent:
.PP
.Vb 2
\&    $temp = join($",@ARGV);
\&    system "echo $temp";
.Ve
.Vb 1
\&    system "echo @ARGV";
.Ve
Within search patterns (which also undergo double-quotish substitution)
there is a bad ambiguity:  Is \f(CW/$foo[bar]/\fR to be interpreted as
\f(CW/${foo}[bar]/\fR (where \f(CW[bar]\fR is a character class for the regular
expression) or as \f(CW/${foo[bar]}/\fR (where \f(CW[bar]\fR is the subscript to array
\f(CW@foo\fR)?  If \f(CW@foo\fR doesn't otherwise exist, then it's obviously a
character class.  If \f(CW@foo\fR exists, Perl takes a good guess about \f(CW[bar]\fR,
and is almost always right.  If it does guess wrong, or if you're just
plain paranoid, you can force the correct interpretation with curly
brackets as above.
.PP
A line-oriented form of quoting is based on the shell \*(L"here-doc\*(R" syntax.
Following a \f(CW<<\fR you specify a string to terminate the quoted material,
and all lines following the current line down to the terminating string
are the value of the item.  The terminating string may be either an
identifier (a word), or some quoted text.  If quoted, the type of
quotes you use determines the treatment of the text, just as in regular
quoting.  An unquoted identifier works like double quotes.  There must
be no space between the \f(CW<<\fR and the identifier.  (If you put a space it
will be treated as a null identifier, which is valid, and matches the
first blank line\*(--see the Merry Christmas example below.)  The terminating
string must appear by itself (unquoted and with no surrounding
whitespace) on the terminating line.
.PP
.Vb 3
\&        print <<EOF;    # same as above
\&    The price is $Price.
\&    EOF
.Ve
.Vb 3
\&        print <<"EOF";  # same as above
\&    The price is $Price.
\&    EOF
.Ve
.Vb 2
\&        print << x 10;  # Legal but discouraged.  Use <<"".
\&    Merry Christmas!
.Ve
.Vb 4
\&        print <<`EOC`;  # execute commands
\&    echo hi there
\&    echo lo there
\&    EOC
.Ve
.Vb 5
\&        print <<"foo", <<"bar"; # you can stack them
\&    I said foo.
\&    foo
\&    I said bar.
\&    bar
.Ve
.Vb 6
\&        myfunc(<<"THIS", 23, <<'THAT'');
\&    Here's a line
\&    or two.
\&    THIS
\&    and here another.
\&    THAT
.Ve
Just don't forget that you have to put a semicolon on the end 
to finish the statement, as Perl doesn't know you're not going to 
try to do this:
.PP
.Vb 4
\&        print <<ABC
\&    179231
\&    ABC
\&        + 20;
.Ve
.Sh "List value constructors"
List values are denoted by separating individual values by commas
(and enclosing the list in parentheses where precedence requires it):
.PP
.Vb 1
\&    (LIST)
.Ve
In a context not requiring a list value, the value of the list
literal is the value of the final element, as with the C comma operator.
For example,
.PP
.Vb 1
\&    @foo = ('cc', '-E', $bar);
.Ve
assigns the entire list value to array foo, but
.PP
.Vb 1
\&    $foo = ('cc', '-E', $bar);
.Ve
assigns the value of variable bar to variable foo.  Note that the value
of an actual array in a scalar context is the length of the array; the
following assigns to \f(CW$foo\fR the value 3:
.PP
.Vb 2
\&    @foo = ('cc', '-E', $bar);
\&    $foo = @foo;                # $foo gets 3
.Ve
You may have an optional comma before the closing parenthesis of an
list literal, so that you can say:
.PP
.Vb 5
\&    @foo = (
\&        1,
\&        2,
\&        3,
\&    );
.Ve
LISTs do automatic interpolation of sublists.  That is, when a \s-1LIST\s0 is
evaluated, each element of the list is evaluated in a list context, and
the resulting list value is interpolated into \s-1LIST\s0 just as if each
individual element were a member of \s-1LIST\s0.  Thus arrays lose their
identity in a \s-1LIST\s0\*(--the list
.PP
.Vb 1
\&    (@foo,@bar,&SomeSub)
.Ve
contains all the elements of \f(CW@foo\fR followed by all the elements of \f(CW@bar\fR,
followed by all the elements returned by the subroutine named SomeSub.
To make a list reference that does \fI\s-1NOT\s0\fR interpolate, see the \fIperlref\fR manpage.
.PP
The null list is represented by ().  Interpolating it in a list
has no effect.  Thus ((),(),()) is equivalent to ().  Similarly,
interpolating an array with no elements is the same as if no
array had been interpolated at that point.
.PP
A list value may also be subscripted like a normal array.  You must
put the list in parentheses to avoid ambiguity.  Examples:
.PP
.Vb 2
\&    # Stat returns list value.
\&    $time = (stat($file))[8];
.Ve
.Vb 2
\&    # Find a hex digit.
\&    $hexdigit = ('a','b','c','d','e','f')[$digit-10];
.Ve
.Vb 2
\&    # A "reverse comma operator".
\&    return (pop(@foo),pop(@foo))[0];
.Ve
Lists may be assigned to if and only if each element of the list
is legal to assign to:
.PP
.Vb 1
\&    ($a, $b, $c) = (1, 2, 3);
.Ve
.Vb 1
\&    ($map{'red'}, $map{'blue'}, $map{'green'}) = (0x00f, 0x0f0, 0xf00);
.Ve
The final element may be an array or a hash:
.PP
.Vb 2
\&    ($a, $b, @rest) = split;
\&    local($a, $b, %rest) = @_;
.Ve
You can actually put an array anywhere in the list, but the first array
in the list will soak up all the values, and anything after it will get
a null value.  This may be useful in a \fIlocal()\fR or \fImy()\fR.
.PP
A hash literal contains pairs of values to be interpreted
as a key and a value:
.PP
.Vb 2
\&    # same as map assignment above
\&    %map = ('red',0x00f,'blue',0x0f0,'green',0xf00);
.Ve
It is often more readable to use the \f(CW=>\fR operator between key/value pairs
(the \f(CW=>\fR operator is actually nothing more than a more visually 
distinctive synonym for a comma):
.PP
.Vb 5
\&    %map = (
\&             'red'   => 0x00f,
\&             'blue'  => 0x0f0,
\&             'green' => 0xf00,
\&           );
.Ve
Array assignment in a scalar context returns the number of elements
produced by the expression on the right side of the assignment:
.PP
.Vb 1
\&    $x = (($foo,$bar) = (3,2,1));       # set $x to 3, not 2
.Ve
This is very handy when you want to do a list assignment in a Boolean
context, since most list functions return a null list when finished,
which when assigned produces a 0, which is interpreted as \s-1FALSE\s0.

.rn }` ''

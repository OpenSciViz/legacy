=head1 NAME

sigtrap - Perl pragma to enable stack backtrace on unexpected signals

=head1 SYNOPSIS

    use sigtrap;
    use sigtrap qw(BUS SEGV PIPE SYS ABRT TRAP);

=head1 DESCRIPTION

The C<sigtrap> pragma initializes some default signal handlers that print
a stack dump of your Perl program, then sends itself a SIGABRT.  This
provides a nice starting point if something horrible goes wrong.

By default, handlers are installed for the ABRT, BUS, EMT, FPE, ILL, PIPE,
QUIT, SEGV, SYS, TERM, and TRAP signals.

See L<perlmod/Pragmatic Modules>.

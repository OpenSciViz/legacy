=head1 NAME

IPC::Open3, open3 - open a process for reading, writing, and error handling

=head1 SYNOPSIS

    $pid = open3('WTRFH', 'RDRFH', 'ERRFH' 
		    'some cmd and args', 'optarg', ...);

=head1 DESCRIPTION

Extremely similar to open2(), open3() spawns the given $cmd and
connects RDRFH for reading, WTRFH for writing, and ERRFH for errors.  If
ERRFH is '', or the same as RDRFH, then STDOUT and STDERR of the child are
on the same file handle.

If WTRFH begins with ">&", then WTRFH will be closed in the parent, and
the child will read from it directly.  if RDRFH or ERRFH begins with
">&", then the child will send output directly to that file handle.  In both
cases, there will be a dup(2) instead of a pipe(2) made.

All caveats from open2() continue to apply.  See L<open2> for details.


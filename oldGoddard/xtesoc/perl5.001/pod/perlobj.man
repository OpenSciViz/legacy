.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLOBJ 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlobj \- Perl objects
.SH "DESCRIPTION"
First of all, you need to understand what references are in Perl.  See
the \fIperlref\fR manpage for that.  
.PP
Here are three very simple definitions that you should find reassuring.
.Ip "1." 4
An object is simply a reference that happens to know which class it
belongs to.
.Ip "2." 4
A class is simply a package that happens to provide methods to deal
with object references.
.Ip "3." 4
A method is simply a subroutine that expects an object reference (or
a package name, for static methods) as the first argument.
.PP
We'll cover these points now in more depth.
.Sh "An Object is Simply a Reference"
Unlike say \*(C+, Perl doesn't provide any special syntax for
constructors.  A constructor is merely a subroutine that returns a
reference that has been \*(L"blessed\*(R" into a class, generally the
class that the subroutine is defined in.  Here is a typical
constructor:
.PP
.Vb 2
\&    package Critter;
\&    sub new { bless {} }
.Ve
The \f(CW{}\fR constructs a reference to an anonymous hash containing no 
key/value pairs.  The \fIbless()\fR takes that reference and tells the object
it references that it's now a Critter, and returns the reference.
This is for convenience, since the referenced object itself knows that
it has been blessed, and its reference to it could have been returned 
directly, like this:
.PP
.Vb 5
\&    sub new {
\&        my $self = {};
\&        bless $self;
\&        return $self;
\&    }
.Ve
In fact, you often see such a thing in more complicated constructors
that wish to call methods in the class as part of the construction:
.PP
.Vb 6
\&    sub new {
\&        my $self = {}
\&        bless $self;
\&        $self->initialize();
\&        $self;
\&    }
.Ve
Within the class package, the methods will typically deal with the
reference as an ordinary reference.  Outside the class package,
the reference is generally treated as an opaque value that may
only be accessed through the class's methods.
.PP
A constructor may re-bless a referenced object currently belonging to
another class, but then the new class is responsible for all cleanup
later.  The previous blessing is forgotten, as an object may only
belong to one class at a time.  (Although of course it's free to 
inherit methods from many classes.)
.PP
A clarification:  Perl objects are blessed.  References are not.  Objects
know which package they belong to.  References do not.  The \fIbless()\fR
function simply uses the reference in order to find the object.  Consider
the following example:
.PP
.Vb 4
\&    $a = {};
\&    $b = $a;
\&    bless $a, BLAH;
\&    print "\e$b is a ", ref($b), "\en";
.Ve
This reports \f(CW$b\fR as being a \s-1BLAH\s0, so obviously \fIbless()\fR 
operated on the object and not on the reference.
.Sh "A Class is Simply a Package"
Unlike say \*(C+, Perl doesn't provide any special syntax for class
definitions.  You just use a package as a class by putting method
definitions into the class.
.PP
There is a special array within each package called \f(CW@ISA\fR which says
where else to look for a method if you can't find it in the current
package.  This is how Perl implements inheritance.  Each element of the
\f(CW@ISA\fR array is just the name of another package that happens to be a
class package.  The classes are searched (depth first) for missing
methods in the order that they occur in \f(CW@ISA\fR.  The classes accessible
through \f(CW@ISA\fR are known as base classes of the current class.
.PP
If a missing method is found in one of the base classes, it is cached
in the current class for efficiency.  Changing \f(CW@ISA\fR or defining new
subroutines invalidates the cache and causes Perl to do the lookup again.
.PP
If a method isn't found, but an \s-1AUTOLOAD\s0 routine is found, then
that is called on behalf of the missing method.
.PP
If neither a method nor an \s-1AUTOLOAD\s0 routine is found in \f(CW@ISA\fR, then one
last try is made for the method (or an \s-1AUTOLOAD\s0 routine) in a class
called \s-1UNIVERSAL\s0.  If that doesn't work, Perl finally gives up and
complains.
.PP
Perl classes only do method inheritance.  Data inheritance is left
up to the class itself.  By and large, this is not a problem in Perl,
because most classes model the attributes of their object using
an anonymous hash, which serves as its own little namespace to be
carved up by the various classes that might want to do something
with the object.
.Sh "A Method is Simply a Subroutine"
Unlike say \*(C+, Perl doesn't provide any special syntax for method
definition.  (It does provide a little syntax for method invocation
though.  More on that later.)  A method expects its first argument
to be the object or package it is being invoked on.  There are just two
types of methods, which we'll call static and virtual, in honor of
the two \*(C+ method types they most closely resemble.
.PP
A static method expects a class name as the first argument.  It
provides functionality for the class as a whole, not for any individual
object belonging to the class.  Constructors are typically static
methods.  Many static methods simply ignore their first argument, since
they already know what package they're in, and don't care what package
they were invoked via.  (These aren't necessarily the same, since
static methods follow the inheritance tree just like ordinary virtual
methods.)  Another typical use for static methods is to look up an
object by name:
.PP
.Vb 4
\&    sub find {
\&        my ($class, $name) = @_;
\&        $objtable{$name};
\&    }
.Ve
A virtual method expects an object reference as its first argument.
Typically it shifts the first argument into a \*(L"self\*(R" or \*(L"this\*(R" variable,
and then uses that as an ordinary reference.
.PP
.Vb 7
\&    sub display {
\&        my $self = shift;
\&        my @keys = @_ ? @_ : sort keys %$self;
\&        foreach $key (@keys) {
\&            print "\et$key => $self->{$key}\en";
\&        }
\&    }
.Ve
.Sh "Method Invocation"
There are two ways to invoke a method, one of which you're already
familiar with, and the other of which will look familiar.  Perl 4
already had an \*(L"indirect object\*(R" syntax that you use when you say
.PP
.Vb 1
\&    print STDERR "help!!!\en";
.Ve
This same syntax can be used to call either static or virtual methods.
We'll use the two methods defined above, the static method to lookup
an object reference and the virtual method to print out its attributes.
.PP
.Vb 2
\&    $fred = find Critter "Fred";
\&    display $fred 'Height', 'Weight';
.Ve
These could be combined into one statement by using a \s-1BLOCK\s0 in the
indirect object slot:
.PP
.Vb 1
\&    display {find Critter "Fred"} 'Height', 'Weight';
.Ve
For \*(C+ fans, there's also a syntax using \-> notation that does exactly
the same thing.  The parentheses are required if there are any arguments.
.PP
.Vb 2
\&    $fred = Critter->find("Fred");
\&    $fred->display('Height', 'Weight');
.Ve
or in one statement,
.PP
.Vb 1
\&    Critter->find("Fred")->display('Height', 'Weight');
.Ve
There are times when one syntax is more readable, and times when the
other syntax is more readable.  The indirect object syntax is less
cluttered, but it has the same ambiguity as ordinary list operators.
Indirect object method calls are parsed using the same rule as list
operators: \*(L"If it looks like a function, it is a function\*(R".  (Presuming
for the moment that you think two words in a row can look like a
function name.  \*(C+ programmers seem to think so with some regularity,
especially when the first word is \*(L"new\*(R".)  Thus, the parens of
.PP
.Vb 1
\&    new Critter ('Barney', 1.5, 70)
.Ve
are assumed to surround \s-1ALL\s0 the arguments of the method call, regardless
of what comes after.  Saying
.PP
.Vb 1
\&    new Critter ('Bam' x 2), 1.4, 45
.Ve
would be equivalent to
.PP
.Vb 1
\&    Critter->new('Bam' x 2), 1.4, 45
.Ve
which is unlikely to do what you want.
.PP
There are times when you wish to specify which class's method to use.
In this case, you can call your method as an ordinary subroutine
call, being sure to pass the requisite first argument explicitly:
.PP
.Vb 2
\&    $fred =  MyCritter::find("Critter", "Fred");
\&    MyCritter::display($fred, 'Height', 'Weight');
.Ve
Note however, that this does not do any inheritance.  If you merely
wish to specify that Perl should \fI\s-1START\s0\fR looking for a method in a
particular package, use an ordinary method call, but qualify the method
name with the package like this:
.PP
.Vb 2
\&    $fred = Critter->MyCritter::find("Fred");
\&    $fred->MyCritter::display('Height', 'Weight');
.Ve
Sometimes you want to call a method when you don't know the method name
ahead of time.  You can use the arrow form, replacing the method name
with a simple scalar variable containing the method name:
.PP
.Vb 2
\&    $method = $fast ? "findfirst" : "findbest";
\&    $fred->$method(@args);
.Ve
.Sh "Destructors"
When the last reference to an object goes away, the object is
automatically destroyed.  (This may even be after you exit, if you've
stored references in global variables.)  If you want to capture control
just before the object is freed, you may define a \s-1DESTROY\s0 method in
your class.  It will automatically be called at the appropriate moment,
and you can do any extra cleanup you need to do.
.PP
Perl doesn't do nested destruction for you.  If your constructor
reblessed a reference from one of your base classes, your \s-1DESTROY\s0 may
need to call \s-1DESTROY\s0 for any base classes that need it.  But this only
applies to reblessed objects\*(--an object reference that is merely
\fI\s-1CONTAINED\s0\fR in the current object will be freed and destroyed
automatically when the current object is freed.
.Sh "\s-1WARNING\s0"
An indirect object is limited to a name, a scalar variable, or a block,
because it would have to do too much lookahead otherwise, just like any
other postfix dereference in the language.  The left side of \-> is not so
limited, because it's an infix operator, not a postfix operator.  
.PP
That means that below, A and B are equivalent to each other, and C and D
are equivalent, but \s-1AB\s0 and \s-1CD\s0 are different:
.PP
.Vb 4
\&    A: method $obref->{"fieldname"} 
\&    B: (method $obref)->{"fieldname"}
\&    C: $obref->{"fieldname"}->method() 
\&    D: method {$obref->{"fieldname"}}
.Ve
.Sh "Summary"
That's about all there is to it.  Now you just need to go off and buy a
book about object-oriented design methodology, and bang your forehead
with it for the next six months or so.
.SH "SEE ALSO"
You should also check out the \fIperlbot\fR manpage for other object tricks, traps, and tips.

.rn }` ''

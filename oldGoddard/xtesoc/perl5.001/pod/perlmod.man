.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLMOD 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlmod \- Perl modules (packages)
.SH "DESCRIPTION"
.Sh "Packages"
Perl provides a mechanism for alternative namespaces to protect packages
from stomping on each others variables.  In fact, apart from certain magical
variables, there's really no such thing as a global variable in Perl.
By default, a Perl script starts
compiling into the package known as \f(CWmain\fR.  You can switch namespaces
using the \f(CWpackage\fR declaration.  The scope of the package declaration is
from the declaration itself to the end of the enclosing block (the same
scope as the \fIlocal()\fR operator).  Typically it would be the first
declaration in a file to be included by the \f(CWrequire\fR operator.  You can
switch into a package in more than one place; it merely influences which
symbol table is used by the compiler for the rest of that block.  You can
refer to variables and filehandles in other packages by prefixing the
identifier with the package name and a double colon:
\f(CW$Package::Variable\fR.  If the package name is null, the \f(CWmain\fR package
as assumed.  That is, \f(CW$::sail\fR is equivalent to \f(CW$main::sail\fR.
.PP
(The old package delimiter was a single quote, but double colon
is now the preferred delimiter, in part because it's more readable
to humans, and in part because it's more readable to \fBemacs\fR macros.
It also makes \*(C+ programmers feel like they know what's going on.)
.PP
Packages may be nested inside other packages: \f(CW$OUTER::INNER::var\fR.  This
implies nothing about the order of name lookups, however.  All symbols
are either local to the current package, or must be fully qualified
from the outer package name down.  For instance, there is nowhere
within package \f(CWOUTER\fR that \f(CW$INNER::var\fR refers to \f(CW$OUTER::INNER::var\fR.
It would treat package \f(CWINNER\fR as a totally separate global package.
.PP
Only identifiers starting with letters (or underscore) are stored in a
package's symbol table.  All other symbols are kept in package \f(CWmain\fR.
In addition, the identifiers \s-1STDIN\s0, \s-1STDOUT\s0, \s-1STDERR\s0, \s-1ARGV\s0,
\s-1ARGVOUT\s0, \s-1ENV\s0, \s-1INC\s0 and \s-1SIG\s0 are forced to be in package \f(CWmain\fR,
even when used for other purposes than their built-in one.  Note also
that, if you have a package called \f(CWm\fR, \f(CWs\fR or \f(CWy\fR, then you can't use
the qualified form of an identifier because it will be interpreted instead
as a pattern match, a substitution, or a translation.
.PP
(Variables beginning with underscore used to be forced into package
main, but we decided it was more useful for package writers to be able
to use leading underscore to indicate private variables and method names.)
.PP
\fIEval()\fRed strings are compiled in the package in which the \fIeval()\fR was
compiled.  (Assignments to \f(CW$SIG{}\fR, however, assume the signal
handler specified is in the \f(CWmain\fR package.  Qualify the signal handler
name if you wish to have a signal handler in a package.)  For an
example, examine \fIperldb.pl\fR in the Perl library.  It initially switches
to the \f(CWDB\fR package so that the debugger doesn't interfere with variables
in the script you are trying to debug.  At various points, however, it
temporarily switches back to the \f(CWmain\fR package to evaluate various
expressions in the context of the \f(CWmain\fR package (or wherever you came
from).  See the \fIperldebug\fR manpage.
.Sh "Symbol Tables"
The symbol table for a package happens to be stored in the associative
array of that name appended with two colons.  The main symbol table's
name is thus \f(CW%main::\fR, or \f(CW%::\fR for short.  Likewise the nested package
mentioned earlier is named \f(CW%OUTER::INNER::\fR.
.PP
The value in each entry of the associative array is what you are
referring to when you use the \f(CW*name\fR notation.  In fact, the following
have the same effect, though the first is more efficient because it
does the symbol table lookups at compile time:
.PP
.Vb 2
\&    local(*main::foo) = *main::bar; local($main::{'foo'}) =
\&    $main::{'bar'};
.Ve
You can use this to print out all the variables in a package, for
instance.  Here is \fIdumpvar.pl\fR from the Perl library:
.PP
.Vb 9
\&   package dumpvar;
\&   sub main::dumpvar {
\&       ($package) = @_;
\&       local(*stab) = eval("*${package}::");
\&       while (($key,$val) = each(%stab)) {
\&           local(*entry) = $val;
\&           if (defined $entry) {
\&               print "\e$$key = '$entry'\en";
\&           }
.Ve
.Vb 7
\&           if (defined @entry) {
\&               print "\e@$key = (\en";
\&               foreach $num ($[ .. $#entry) {
\&                   print "  $num\et'",$entry[$num],"'\en";
\&               }
\&               print ")\en";
\&           }
.Ve
.Vb 9
\&           if ($key ne "${package}::" && defined %entry) {
\&               print "\e%$key = (\en";
\&               foreach $key (sort keys(%entry)) {
\&                   print "  $key\et'",$entry{$key},"'\en";
\&               }
\&               print ")\en";
\&           }
\&       }
\&   }
.Ve
Note that even though the subroutine is compiled in package \f(CWdumpvar\fR,
the name of the subroutine is qualified so that its name is inserted
into package \f(CWmain\fR.
.PP
Assignment to a symbol table entry performs an aliasing operation,
i.e.,
.PP
.Vb 1
\&    *dick = *richard;
.Ve
causes variables, subroutines and file handles accessible via the
identifier \f(CWrichard\fR to also be accessible via the symbol \f(CWdick\fR.  If
you only want to alias a particular variable or subroutine, you can
assign a reference instead:
.PP
.Vb 1
\&    *dick = \e$richard;
.Ve
makes \f(CW$richard\fR and \f(CW$dick\fR the same variable, but leaves
\f(CW@richard\fR and \f(CW@dick\fR as separate arrays.  Tricky, eh?
.Sh "Package Constructors and Destructors"
There are two special subroutine definitions that function as package
constructors and destructors.  These are the \f(CWBEGIN\fR and \f(CWEND\fR
routines.  The \f(CWsub\fR is optional for these routines.
.PP
A \f(CWBEGIN\fR subroutine is executed as soon as possible, that is, the
moment it is completely defined, even before the rest of the containing
file is parsed.  You may have multiple \f(CWBEGIN\fR blocks within a
file\*(--they will execute in order of definition.  Because a \f(CWBEGIN\fR
block executes immediately, it can pull in definitions of subroutines
and such from other files in time to be visible to the rest of the
file.
.PP
An \f(CWEND\fR subroutine is executed as late as possible, that is, when the
interpreter is being exited, even if it is exiting as a result of a
\fIdie()\fR function.  (But not if it's is being blown out of the water by a
signal\*(--you have to trap that yourself (if you can).)  You may have
multiple \f(CWEND\fR blocks within a file\*(--they will execute in reverse
order of definition; that is: last in, first out (\s-1LIFO\s0).
.PP
Note that when you use the \fB\-n\fR and \fB\-p\fR switches to Perl, \f(CWBEGIN\fR
and \f(CWEND\fR work just as they do in \fBawk\fR, as a degenerate case.
.Sh "Perl Classes"
There is no special class syntax in Perl 5, but a package may function
as a class if it provides subroutines that function as methods.  Such a
package may also derive some of its methods from another class package
by listing the other package name in its \f(CW@ISA\fR array.  For more on
this, see the \fIperlobj\fR manpage.
.Sh "Perl Modules"
In Perl 5, the notion of packages has been extended into the notion of
modules.  A module is a package that is defined in a library file of
the same name, and is designed to be reusable.  It may do this by
providing a mechanism for exporting some of its symbols into the symbol
table of any package using it.  Or it may function as a class
definition and make its semantics available implicitly through method
calls on the class and its objects, without explicit exportation of any
symbols.  Or it can do a little of both.
.PP
Perl modules are included by saying
.PP
.Vb 1
\&    use Module;
.Ve
or
.PP
.Vb 1
\&    use Module LIST;
.Ve
This is exactly equivalent to
.PP
.Vb 1
\&    BEGIN { require "Module.pm"; import Module; }
.Ve
or
.PP
.Vb 1
\&    BEGIN { require "Module.pm"; import Module LIST; }
.Ve
All Perl module files have the extension \fI.pm\fR.  \f(CWuse\fR assumes this so
that you don't have to spell out \*(L"\fIModule.pm\fR\*(R" in quotes.  This also
helps to differentiate new modules from old \fI.pl\fR and \fI.ph\fR files.
Module names are also capitalized unless they're functioning as pragmas,
\*(L"Pragmas\*(R" are in effect compiler directives, and are sometimes called
\*(L"pragmatic modules\*(R" (or even \*(L"pragmata\*(R" if you're a classicist).
.PP
Because the \f(CWuse\fR statement implies a \f(CWBEGIN\fR block, the importation
of semantics happens at the moment the \f(CWuse\fR statement is compiled,
before the rest of the file is compiled.  This is how it is able
to function as a pragma mechanism, and also how modules are able to
declare subroutines that are then visible as list operators for
the rest of the current file.  This will not work if you use \f(CWrequire\fR
instead of \f(CWuse\fR.  Therefore, if you're planning on the module altering
your namespace, use \f(CWuse\fR; otherwise, use \f(CWrequire\fR.  Otherwise you 
can get into this problem:
.PP
.Vb 2
\&    require Cwd;                # make Cwd:: accessible
\&    $here = Cwd::getcwd();      
.Ve
.Vb 2
\&    use Cwd;                    # import names from Cwd:: 
\&    $here = getcwd();
.Ve
.Vb 2
\&    require Cwd;                # make Cwd:: accessible
\&    $here = getcwd();           # oops! no main::getcwd()
.Ve
Perl packages may be nested inside other package names, so we can have
package names containing \f(CW::\fR.  But if we used that package name
directly as a filename it would makes for unwieldy or impossible
filenames on some systems.  Therefore, if a module's name is, say,
\f(CWText::Soundex\fR, then its definition is actually found in the library
file \fIText/Soundex.pm\fR.
.PP
Perl modules always have a \fI.pm\fR file, but there may also be dynamically
linked executables or autoloaded subroutine definitions associated with
the module.  If so, these will be entirely transparent to the user of
the module.  It is the responsibility of the \fI.pm\fR file to load (or
arrange to autoload) any additional functionality.  The \s-1POSIX\s0 module
happens to do both dynamic loading and autoloading, but the user can
just say \f(CWuse POSIX\fR to get it all.
.PP
For more information on writing extension modules, see the \fIperlapi\fR manpage
and the \fIperlguts\fR manpage.
.SH "NOTE"
Perl does not enforce private and public parts of its modules as you may
have been used to in other languages like \*(C+, Ada, or Modula-17.  Perl
doesn't have an infatuation with enforced privacy.  It would prefer
that you stayed out of its living room because you weren't invited, not
because it has a shotgun.
.PP
The module and its user have a contract, part of which is common law,
and part of which is \*(L"written\*(R".  Part of the common law contract is
that a module doesn't pollute any namespace it wasn't asked to.  The
written contract for the module (AKA documentation) may make other
provisions.  But then you know when you \f(CWuse RedefineTheWorld\fR that
you're redefining the world and willing to take the consequences.
.SH "THE PERL MODULE LIBRARY"
A number of modules are included the the Perl distribution.  These are
described below, and all end in \fI.pm\fR.  You may also discover files in 
the library directory that end in either \fI.pl\fR or \fI.ph\fR.  These are old
libraries supplied so that old programs that use them still run.  The
\fI.pl\fR files will all eventually be converted into standard modules, and
the \fI.ph\fR files made by \fBh2ph\fR will probably end up as extension modules
made by \fBh2xs\fR.  (Some \fI.ph\fR values may already be available through the
POSIX module.)  The \fBpl2pm\fR file in the distribution may help in your
conversion, but it's just a mechanical process, so is far from bullet proof.
.Sh "Pragmatic Modules"
They work somewhat like pragmas in that they tend to affect the compilation of
your program, and thus will usually only work well when used within a
\f(CWuse\fR, or \f(CWno\fR.  These are locally scoped, so an inner \s-1BLOCK\s0
may countermand any of these by saying
.PP
.Vb 2
\&    no integer;
\&    no strict 'refs';
.Ve
which lasts until the end of that \s-1BLOCK\s0.
.PP
The following programs are defined (and have their own documentation).
.Ip "\f(CWinteger\fR" 12
Perl pragma to compute arithmetic in integer instead of double
.Ip "\f(CWless\fR" 12
Perl pragma to request less of something from the compiler
.Ip "\f(CWsigtrap\fR" 12
Perl pragma to enable stack backtrace on unexpected signals
.Ip "\f(CWstrict\fR" 12
Perl pragma to restrict unsafe constructs
.Ip "\f(CWsubs\fR" 12
Perl pragma to predeclare sub names
.Sh "Standard Modules"
The following modules are all expected to behave in a well-defined
manner with respect to namespace pollution because they use the
Exporter module.
See their own documentation for details.
.Ip "\f(CWAbbrev\fR" 12
create an abbreviation table from a list
.Ip "\f(CWAnyDBM_File\fR" 12
provide framework for multiple DBMs 
.Ip "\f(CWAutoLoader\fR" 12
load functions only on demand
.Ip "\f(CWAutoSplit\fR" 12
split a package for autoloading
.Ip "\f(CWBasename\fR" 12
parse file name and path from a specification
.Ip "\f(CWBenchmark\fR" 12
benchmark running times of code 
.Ip "\f(CWCarp\fR" 12
warn or die of errors (from perspective of caller)
.Ip "\f(CWCheckTree\fR" 12
run many filetest checks on a tree
.Ip "\f(CWCollate\fR" 12
compare 8-bit scalar data according to the current locale
.Ip "\f(CWConfig\fR" 12
access Perl configuration option
.Ip "\f(CWCwd\fR" 12
get pathname of current working directory
.Ip "\f(CWDynaLoader\fR" 12
Dynamically load C libraries into Perl code 
.Ip "\f(CWEnglish\fR" 12
use nice English (or \fBawk\fR) names for ugly punctuation variables
.Ip "\f(CWEnv\fR" 12
Perl module that imports environment variables
.Ip "\f(CWExporter\fR" 12
module to control namespace manipulations 
.Ip "\f(CWFcntl\fR" 12
load the C Fcntl.h defines
.Ip "\f(CWFileHandle\fR" 12
supply object methods for filehandles 
.Ip "\f(CWFind\fR" 12
traverse a file tree
.Ip "\f(CWFinddepth\fR" 12
traverse a directory structure depth-first
.Ip "\f(CWGetopt\fR" 12
basic and extended \fIgetopt\fR\|(3) processing
.Ip "\f(CWMakeMaker\fR" 12
generate a Makefile for Perl extension
.Ip "\f(CWOpen2\fR" 12
open a process for both reading and writing
.Ip "\f(CWOpen3\fR" 12
open a process for reading, writing, and error handling
.Ip "\f(CWPOSIX\fR" 12
Perl interface to \s-1IEEE\s0 1003.1 namespace
.Ip "\f(CWPing\fR" 12
check a host for upness
.Ip "\f(CWSocket\fR" 12
load the C socket.h defines
.Sh "Extension Modules"
Extension modules are written in C (or a mix of Perl and C) and get
dynamically loaded into Perl if and when you need them.  Supported
extension modules include the Socket, Fcntl, and \s-1POSIX\s0 modules.
.PP
The following are popular C extension modules, which while available at
Perl 5.0 release time, do not come bundled (at least, not completely)
due to their size, volatility, or simply lack of time for adequate testing
and configuration across the multitude of platforms on which Perl was
beta-tested.  You are encouraged to look for them in \fIarchie\fR\|(1L), the Perl
\s-1FAQ\s0 or Meta-\s-1FAQ\s0, the \s-1WWW\s0 page, and even with their authors before randomly
posting asking for their present condition and disposition.  There's no
guarantee that the names or addresses below have not changed since printing,
and in fact, they probably have!
.Ip "\f(CWCurses\fR" 12
Written by William Setzer <\fIWilliam_Setzer@ncsu.edu\fR>, while not
included with the standard distribution, this extension module ports to
most systems.  \s-1FTP\s0 from your nearest Perl archive site, or try
.Sp
.Vb 1
\&        ftp://ftp.ncsu.edu/pub/math/wsetzer/cursperl5??.tar.gz
.Ve
It is currently in alpha test, so the name and ftp location may
change.
.Ip "\f(CWDBI\fR" 12
This is the portable database interface written by
<\fITim.Bunce@ig.co.uk\fR>.  This supersedes the many perl4 ports for
database extensions.  The official archive for DBperl extensions is
\fIftp.demon.co.uk:/pub/perl/db\fR.  This archive contains copies of perl4
ports for Ingres, Oracle, Sybase, Informix, Unify, Postgres, and
Interbase, as well as rdb and shql and other non-\s-1SQL\s0 systems.
.Ip "\f(CWDB_File\fR" 12
Fastest and most restriction-free of the \s-1DBM\s0 bindings, this extension module 
uses the popular Berkeley \s-1DB\s0 to \fItie()\fR into your hashes.  This has a
standardly-distributed man page and dynamic loading extension module, but
you'll have to fetch the Berkeley code yourself.  See the \fIDB_File\fR manpage for
where.
.Ip "\f(CWSx\fR" 12
This extension module is a front to the Athena and Xlib libraries for Perl
\s-1GUI\s0 programming, originally written by by Dominic Giampaolo
<\fIdbg@sgi.com\fR>, then and rewritten for Sx by Fre\*'de\*'ric
Chauveau <\fIfmc@pasteur.fr\fR>.  It's available for \s-1FTP\s0 from
.Sp
.Vb 1
\&    ftp.pasteur.fr:/pub/Perl/Sx.tar.gz
.Ve
.Ip "\f(CWTk\fR" 12
This extension module is an object-oriented Perl5 binding to the popular
tcl/tk X11 package.  However, you need know no \s-1TCL\s0 to use it!
It was written by Malcolm Beattie <\fImbeattie@sable.ox.ac.uk\fR>.
If you are unable to locate it using \fIarchie\fR\|(1L) or a similar
tool, you may try retrieving it from \fI/private/Tk-october.tar.gz\fR
from Malcolm's machine listed above.

.rn }` ''

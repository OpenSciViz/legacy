.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLSUB 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlsub \- Perl subroutines
.SH "SYNOPSIS"
To declare subroutines:
.PP
.Vb 2
\&    sub NAME;      # A "forward" declaration.
\&    sub NAME BLOCK # A declaration and a definition.
.Ve
To define an anonymous subroutine at runtime:
.PP
.Vb 1
\&    $subref = sub BLOCK;
.Ve
To import subroutines:
.PP
.Vb 1
\&    use PACKAGE qw(NAME1 NAME2 NAME3);
.Ve
To call subroutines:
.PP
.Vb 4
\&    &NAME          # Passes current @_ to subroutine.
\&    &NAME(LIST);   # Parens required with & form.
\&    NAME(LIST);    # & is optional with parens.
\&    NAME LIST;     # Parens optional if predeclared/imported.
.Ve
.SH "DESCRIPTION"
Any arguments passed to the routine come in as array \f(CW@_\fR, that is
($_[0], \f(CW$_\fR[1], ...).  The array \f(CW@_\fR is a local array, but its values are
references to the actual scalar parameters.  The return value of the
subroutine is the value of the last expression evaluated, and can be
either an array value or a scalar value.  Alternatively, a return
statement may be used to specify the returned value and exit the
subroutine.  To create local variables see the \fIlocal()\fR and \fImy()\fR
operators.
.PP
A subroutine may be called using the \*(L"&\*(R" prefix.  The \*(L"&\*(R" is optional in Perl
5, and so are the parens if the subroutine has been predeclared.
(Note, however, that the \*(L"&\*(R" is \fINOT\fR optional when you're just naming the
subroutine, such as when it's used as an argument to \fIdefined()\fR or
\fIundef()\fR.  Nor is it optional when you want to do an indirect subroutine
call with a subroutine name or reference using the \f(CW&$subref()\fR or
\f(CW&{$subref}()\fR constructs.  See the \fIperlref\fR manpage for more on that.)
.PP
Example:
.PP
.Vb 7
\&    sub MAX {
\&        my $max = pop(@_);
\&        foreach $foo (@_) {
\&            $max = $foo if $max < $foo;
\&        }
\&        $max;
\&    }
.Ve
.Vb 2
\&    ...
\&    $bestday = &MAX($mon,$tue,$wed,$thu,$fri);
.Ve
Example:
.PP
.Vb 2
\&    # get a line, combining continuation lines
\&    #  that start with whitespace
.Ve
.Vb 12
\&    sub get_line {
\&        $thisline = $lookahead;
\&        LINE: while ($lookahead = <STDIN>) {
\&            if ($lookahead =~ /^[ \et]/) {
\&                $thisline .= $lookahead;
\&            }
\&            else {
\&                last LINE;
\&            }
\&        }
\&        $thisline;
\&    }
.Ve
.Vb 4
\&    $lookahead = <STDIN>;       # get first line
\&    while ($_ = get_line()) {
\&        ...
\&    }
.Ve
Use array assignment to a local list to name your formal arguments:
.PP
.Vb 4
\&    sub maybeset {
\&        my($key, $value) = @_;
\&        $foo{$key} = $value unless $foo{$key};
\&    }
.Ve
This also has the effect of turning call-by-reference into
call-by-value, since the assignment copies the values.
.PP
Subroutines may be called recursively.  If a subroutine is called using
the \*(L"&\*(R" form, the argument list is optional.  If omitted, no \f(CW@_\fR array is
set up for the subroutine; the \f(CW@_\fR array at the time of the call is
visible to subroutine instead.
.PP
.Vb 2
\&    &foo(1,2,3);        # pass three arguments
\&    foo(1,2,3);         # the same
.Ve
.Vb 3
\&    foo();              # pass a null list
\&    &foo();             # the same
\&    &foo;               # pass no arguments--more efficient
.Ve
If a module wants to create a private subroutine that cannot be called
from outside the module, it can declare a lexical variable containing
an anonymous sub reference:
.PP
.Vb 2
\&    my $subref = sub { ... }
\&    &$subref(1,2,3);
.Ve
As long as the reference is never returned by any function within the module,
no outside module can see the subroutine, since its name is not in any
package's symbol table.
.Sh "Passing Symbol Table Entries"
[Note:  The mechanism described in this section works fine in Perl 5, but
the new reference mechanism is generally easier to work with.  See the \fIperlref\fR manpage.]
.PP
Sometimes you don't want to pass the value of an array to a subroutine
but rather the name of it, so that the subroutine can modify the global
copy of it rather than working with a local copy.  In perl you can
refer to all the objects of a particular name by prefixing the name
with a star: \f(CW*foo\fR.  This is often known as a \*(L"type glob\*(R", since the
star on the front can be thought of as a wildcard match for all the
funny prefix characters on variables and subroutines and such.
.PP
When evaluated, the type glob produces a scalar value that represents
all the objects of that name, including any filehandle, format or
subroutine.  When assigned to, it causes the name mentioned to refer to
whatever \*(L"*\*(R" value was assigned to it.  Example:
.PP
.Vb 8
\&    sub doubleary {
\&        local(*someary) = @_;
\&        foreach $elem (@someary) {
\&            $elem *= 2;
\&        }
\&    }
\&    doubleary(*foo);
\&    doubleary(*bar);
.Ve
Note that scalars are already passed by reference, so you can modify
scalar arguments without using this mechanism by referring explicitly
to \f(CW$_\fR[0] etc.  You can modify all the elements of an array by passing
all the elements as scalars, but you have to use the * mechanism (or
the equivalent reference mechanism) to push, pop or change the size of
an array.  It will certainly be faster to pass the typeglob (or reference).
.PP
Even if you don't want to modify an array, this mechanism is useful for
passing multiple arrays in a single \s-1LIST\s0, since normally the \s-1LIST\s0
mechanism will merge all the array values so that you can't extract out
the individual arrays.
.Sh "Overriding builtin functions"
Many builtin functions may be overridden, though this should only be
tried occasionally and for good reason.  Typically this might be
done by a package attempting to emulate missing builtin functionality
on a non-Unix system.
.PP
Overriding may only be done by importing the name from a
module\*(--ordinary predeclaration isn't good enough.  However, the
\f(CWsubs\fR pragma (compiler directive) lets you, in effect, predeclare subs
via the import syntax, and these names may then override the builtin ones:
.PP
.Vb 3
\&    use subs 'chdir', 'chroot', 'chmod', 'chown';
\&    chdir $somewhere;
\&    sub chdir { ... }
.Ve
Library modules should not in general export builtin names like \*(L"open\*(R"
or \*(L"chdir\*(R" as part of their default \f(CW@EXPORT\fR list, since these may
sneak into someone else's namespace and change the semantics unexpectedly.
Instead, if the module adds the name to the \f(CW@EXPORT_OK\fR list, then it's
possible for a user to import the name explicitly, but not implicitly.
That is, they could say
.PP
.Vb 1
\&    use Module 'open';
.Ve
and it would import the open override, but if they said
.PP
.Vb 1
\&    use Module;
.Ve
they would get the default imports without the overrides.
.Sh "Autoloading"
If you call a subroutine that is undefined, you would ordinarily get an
immediate fatal error complaining that the subroutine doesn't exist.
(Likewise for subroutines being used as methods, when the method
doesn't exist in any of the base classes of the class package.) If,
however, there is an \f(CWAUTOLOAD\fR subroutine defined in the package or
packages that were searched for the original subroutine, then that
\f(CWAUTOLOAD\fR subroutine is called with the arguments that would have been
passed to the original subroutine.  The fully qualified name of the
original subroutine magically appears in the \f(CW$AUTOLOAD\fR variable in the
same package as the \f(CWAUTOLOAD\fR routine.  The name is not passed as an
ordinary argument because, er, well, just because, that's why...
.PP
Most \f(CWAUTOLOAD\fR routines will load in a definition for the subroutine in
question using eval, and then execute that subroutine using a special
form of \*(L"goto\*(R" that erases the stack frame of the \f(CWAUTOLOAD\fR routine
without a trace.  (See the standard \f(CWAutoLoader\fR module, for example.)
But an \f(CWAUTOLOAD\fR routine can also just emulate the routine and never
define it.  A good example of this is the standard Shell module, which
can treat undefined subroutine calls as calls to Unix programs.
.PP
There are mechanisms available for modules to help them split themselves
up into autoloadable files to be used with the standard AutoLoader module.
See the document on extensions.

.rn }` ''

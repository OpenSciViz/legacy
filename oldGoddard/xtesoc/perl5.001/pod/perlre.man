.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLRE 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlre \- Perl regular expressions
.SH "DESCRIPTION"
For a description of how to use regular expressions in matching
operations, see \f(CWm//\fR and \f(CWs///\fR in the \fIperlop\fR manpage.  The matching operations can
have various modifiers, some of which relate to the interpretation of
the regular expression inside.  These are:
.PP
.Vb 4
\&    i   Do case-insensitive pattern matching.
\&    m   Treat string as multiple lines.
\&    s   Treat string as single line.
\&    x   Use extended regular expressions.
.Ve
These are usually written as \*(L"the \f(CW/x\fR modifier\*(R", even though the delimiter
in question might not actually be a slash.  In fact, any of these
modifiers may also be embedded within the regular expression itself using
the new \f(CW(?...)\fR construct.  See below.
.PP
The \f(CW/x\fR modifier itself needs a little more explanation.  It tells the
regular expression parser to ignore whitespace that is not backslashed
or within a character class.  You can use this to break up your regular
expression into (slightly) more readable parts.  Together with the
capability of embedding comments described later, this goes a long
way towards making Perl 5 a readable language.  See the C comment
deletion code in the \fIperlop\fR manpage.
.Sh "Regular Expressions"
The patterns used in pattern matching are regular expressions such as
those supplied in the Version 8 regexp routines.  (In fact, the
routines are derived (distantly) from Henry Spencer's freely
redistributable reimplementation of the V8 routines.)
See the section on \fIVersion 8 Regular Expressions\fR for details.
.PP
In particular the following metacharacters have their standard \fIegrep\fR\-ish
meanings:
.PP
.Vb 7
\&    \e   Quote the next metacharacter
\&    ^   Match the beginning of the line
\&    .   Match any character (except newline)
\&    $   Match the end of the line
\&    |   Alternation
\&    ()  Grouping
\&    []  Character class
.Ve
By default, the \*(L"^\*(R" character is guaranteed to match only at the
beginning of the string, the \*(L"$\*(R" character only at the end (or before the
newline at the end) and Perl does certain optimizations with the
assumption that the string contains only one line.  Embedded newlines
will not be matched by \*(L"^\*(R" or \*(L"$\*(R".  You may, however, wish to treat a
string as a multi-line buffer, such that the \*(L"^\*(R" will match after any
newline within the string, and \*(L"$\*(R" will match before any newline.  At the
cost of a little more overhead, you can do this by using the /m modifier
on the pattern match operator.  (Older programs did this by setting \f(CW$*\fR,
but this practice is deprecated in Perl 5.)
.PP
To facilitate multi-line substitutions, the \*(L".\*(R" character never matches a
newline unless you use the \f(CW/s\fR modifier, which tells Perl to pretend
the string is a single line\*(--even if it isn't.  The \f(CW/s\fR modifier also
overrides the setting of \f(CW$*\fR, in case you have some (badly behaved) older
code that sets it in another module.
.PP
The following standard quantifiers are recognized:
.PP
.Vb 6
\&    *      Match 0 or more times
\&    +      Match 1 or more times
\&    ?      Match 1 or 0 times
\&    {n}    Match exactly n times
\&    {n,}   Match at least n times
\&    {n,m}  Match at least n but not more than m times
.Ve
(If a curly bracket occurs in any other context, it is treated
as a regular character.)  The \*(L"*\*(R" modifier is equivalent to \f(CW{0,}\fR, the \*(L"+\*(R"
modifier to \f(CW{1,}\fR, and the \*(L"?\*(R" modifier to \f(CW{0,1}\fR.  There is no limit to the
size of n or m, but large numbers will chew up more memory. 
.PP
By default, a quantified subpattern is \*(L"greedy\*(R", that is, it will match as
many times as possible without causing the rest pattern not to match.  The
standard quantifiers are all \*(L"greedy\*(R", in that they match as many
occurrences as possible (given a particular starting location) without
causing the pattern to fail.  If you want it to match the minimum number
of times possible, follow the quantifier with a \*(L"?\*(R" after any of them.
Note that the meanings don't change, just the \*(L"gravity":
.PP
.Vb 6
\&    *?     Match 0 or more times
\&    +?     Match 1 or more times
\&    ??     Match 0 or 1 time
\&    {n}?   Match exactly n times
\&    {n,}?  Match at least n times
\&    {n,m}? Match at least n but not more than m times
.Ve
Since patterns are processed as double quoted strings, the following
also work:
.PP
.Vb 16
\&    \et          tab
\&    \en          newline
\&    \er          return
\&    \ef          form feed
\&    \ev          vertical tab, whatever that is
\&    \ea          alarm (bell)
\&    \ee          escape
\&    \e033        octal char
\&    \ex1b        hex char
\&    \ec[         control char
\&    \el          lowercase next char
\&    \eu          uppercase next char
\&    \eL          lowercase till \eE
\&    \eU          uppercase till \eE
\&    \eE          end case modification
\&    \eQ          quote regexp metacharacters till \eE
.Ve
In addition, Perl defines the following:
.PP
.Vb 6
\&    \ew  Match a "word" character (alphanumeric plus "_")
\&    \eW  Match a non-word character
\&    \es  Match a whitespace character
\&    \eS  Match a non-whitespace character
\&    \ed  Match a digit character
\&    \eD  Match a non-digit character
.Ve
Note that \f(CW\ew\fR matches a single alphanumeric character, not a whole
word.  To match a word you'd need to say \f(CW\ew+\fR.  You may use \f(CW\ew\fR, \f(CW\eW\fR, \f(CW\es\fR,
\f(CW\eS\fR, \f(CW\ed\fR and \f(CW\eD\fR within character classes (though not as either end of a
range).
.PP
Perl defines the following zero-width assertions:
.PP
.Vb 5
\&    \eb  Match a word boundary
\&    \eB  Match a non-(word boundary)
\&    \eA  Match only at beginning of string
\&    \eZ  Match only at end of string
\&    \eG  Match only where previous m//g left off
.Ve
A word boundary (\f(CW\eb\fR) is defined as a spot between two characters that
has a \f(CW\ew\fR on one side of it and and a \f(CW\eW\fR on the other side of it (in
either order), counting the imaginary characters off the beginning and
end of the string as matching a \f(CW\eW\fR.  (Within character classes \f(CW\eb\fR
represents backspace rather than a word boundary.)  The \f(CW\eA\fR and \f(CW\eZ\fR are
just like \*(L"^\*(R" and \*(L"$\*(R" except that they won't match multiple times when the
\f(CW/m\fR modifier is used, while \*(L"^\*(R" and \*(L"$\*(R" will match at every internal line
boundary.
.PP
When the bracketing construct \f(CW( ... )\fR is used, \e<digit> matches the
digit'th substring.  (Outside of the pattern, always use \*(L"$\*(R" instead of
\*(L"\e\*(R" in front of the digit.  The scope of $<digit> (and \f(CW$`\fR, \f(CW$&\fR, and \f(CW$')\fR
extends to the end of the enclosing \s-1BLOCK\s0 or eval string, or to the
next pattern match with subexpressions.  
If you want to
use parentheses to delimit subpattern (e.g. a set of alternatives) without
saving it as a subpattern, follow the ( with a ?.
The \e<digit> notation
sometimes works outside the current pattern, but should not be relied
upon.)  You may have as many parentheses as you wish.  If you have more
than 9 substrings, the variables \f(CW$10\fR, \f(CW$11\fR, ... refer to the
corresponding substring.  Within the pattern, \e10, \e11, etc. refer back
to substrings if there have been at least that many left parens before
the backreference.  Otherwise (for backward compatibilty) \e10 is the
same as \e010, a backspace, and \e11 the same as \e011, a tab.  And so
on.  (\e1 through \e9 are always backreferences.)
.PP
\f(CW$+\fR returns whatever the last bracket match matched.  \f(CW$&\fR returns the
entire matched string.  ($0 used to return the same thing, but not any
more.)  \f(CW$`\fR returns everything before the matched string.  \f(CW$'\fR returns
everything after the matched string.  Examples:
.PP
.Vb 1
\&    s/^([^ ]*) *([^ ]*)/$2 $1/;     # swap first two words
.Ve
.Vb 5
\&    if (/Time: (..):(..):(..)/) {
\&        $hours = $1;
\&        $minutes = $2;
\&        $seconds = $3;
\&    }
.Ve
You will note that all backslashed metacharacters in Perl are
alphanumeric, such as \f(CW\eb\fR, \f(CW\ew\fR, \f(CW\en\fR.  Unlike some other regular expression
languages, there are no backslashed symbols that aren't alphanumeric.
So anything that looks like \e\e, \e(, \e), \e<, \e>, \e{, or \e} is always
interpreted as a literal character, not a metacharacter.  This makes it
simple to quote a string that you want to use for a pattern but that
you are afraid might contain metacharacters.  Simply quote all the
non-alphanumeric characters:
.PP
.Vb 1
\&    $pattern =~ s/(\eW)/\e\e$1/g;
.Ve
You can also use the built-in \fIquotemeta()\fR function to do this.
An even easier way to quote metacharacters right in the match operator
is to say 
.PP
.Vb 1
\&    /$unquoted\eQ$quoted\eE$unquoted/
.Ve
Perl 5 defines a consistent extension syntax for regular expressions.
The syntax is a pair of parens with a question mark as the first thing
within the parens (this was a syntax error in Perl 4).  The character
after the question mark gives the function of the extension.  Several
extensions are already supported:
.Ip "(?#text)" 10
A comment.  The text is ignored.
.Ip "(?:regexp)" 10
This groups things like \*(L"()\*(R" but doesn't make backrefences like \*(L"()\*(R" does.  So
.Sp
.Vb 1
\&    split(/\eb(?:a|b|c)\eb/)
.Ve
is like
.Sp
.Vb 1
\&    split(/\eb(a|b|c)\eb/)
.Ve
but doesn't spit out extra fields.
.Ip "(?=regexp)" 10
A zero-width positive lookahead assertion.  For example, \f(CW/\ew+(?=\et)/\fR
matches a word followed by a tab, without including the tab in \f(CW$&\fR.
.Ip "(?!regexp)" 10
A zero-width negative lookahead assertion.  For example \f(CW/foo(?!bar)/\fR
matches any occurrence of \*(L"foo\*(R" that isn't followed by \*(L"bar\*(R".  Note
however that lookahead and lookbehind are \s-1NOT\s0 the same thing.  You cannot
use this for lookbehind: \f(CW/(?!foo)bar/\fR will not find an occurrence of
\*(L"bar\*(R" that is preceded by something which is not \*(L"foo\*(R".  That's because
the \f(CW(?!foo)\fR is just saying that the next thing cannot be \*(L"foo\*(R"\*(--and
it's not, it's a \*(L"bar\*(R", so \*(L"foobar\*(R" will match.  You would have to do
something like \f(CW/(?foo)...bar/\fR for that.   We say \*(L"like\*(R" because there's
the case of your \*(L"bar\*(R" not having three characters before it.  You could
cover that this way: \f(CW/(?:(?!foo)...|^..?)bar/\fR.  Sometimes it's still 
easier just to say:
.Sp
.Vb 1
\&    if (/foo/ && $` =~ /bar$/) 
.Ve
.Ip "(?imsx)" 10
One or more embedded pattern-match modifiers.  This is particularly
useful for patterns that are specified in a table somewhere, some of
which want to be case sensitive, and some of which don't.  The case
insensitive ones merely need to include \f(CW(?i)\fR at the front of the
pattern.  For example:
.Sp
.Vb 2
\&    $pattern = "foobar";
\&    if ( /$pattern/i ) 
.Ve
.Vb 1
\&    # more flexible:
.Ve
.Vb 2
\&    $pattern = "(?i)foobar";
\&    if ( /$pattern/ ) 
.Ve
.PP
The specific choice of question mark for this and the new minimal
matching construct was because 1) question mark is pretty rare in older
regular expressions, and 2) whenever you see one, you should stop
and \*(L"question\*(R" exactly what is going on.  That's psychology...
.Sh "Version 8 Regular Expressions"
In case you're not familiar with the \*(L"regular\*(R" Version 8 regexp
routines, here are the pattern-matching rules not described above.
.PP
Any single character matches itself, unless it is a \fImetacharacter\fR
with a special meaning described here or above.  You can cause
characters which normally function as metacharacters to be interpreted
literally by prefixing them with a \*(L"\e\*(R" (e.g. \*(L"\e.\*(R" matches a \*(L".\*(R", not any
character; \*(L"\e\e\*(R" matches a \*(L"\e").  A series of characters matches that
series of characters in the target string, so the pattern \f(CWblurfl\fR
would match \*(L"blurfl\*(R" in the target string.
.PP
You can specify a character class, by enclosing a list of characters
in \f(CW[]\fR, which will match any one of the characters in the list.  If the
first character after the \*(L"[\*(R" is \*(L"^\*(R", the class matches any character not
in the list.  Within a list, the \*(L"\-\*(R" character is used to specify a
range, so that \f(CWa-z\fR represents all the characters between \*(L"a\*(R" and \*(L"z\*(R",
inclusive.
.PP
Characters may be specified using a metacharacter syntax much like that
used in C: \*(L"\en\*(R" matches a newline, \*(L"\et\*(R" a tab, \*(L"\er\*(R" a carriage return,
\*(L"\ef\*(R" a form feed, etc.  More generally, \e\fInnn\fR, where \fInnn\fR is a string
of octal digits, matches the character whose \s-1ASCII\s0 value is \fInnn\fR.
Similarly, \ex\fInn\fR, where \fInn\fR are hexidecimal digits, matches the
character whose \s-1ASCII\s0 value is \fInn\fR. The expression \ec\fIx\fR matches the
\s-1ASCII\s0 character control-\fIx\fR.  Finally, the \*(L".\*(R" metacharacter matches any
character except \*(L"\en\*(R" (unless you use \f(CW/s\fR).
.PP
You can specify a series of alternatives for a pattern using \*(L"|\*(R" to
separate them, so that \f(CWfee|fie|foe\fR will match any of \*(L"fee\*(R", \*(L"fie\*(R",
or \*(L"foe\*(R" in the target string (as would \f(CWf(e|i|o)e\fR).  Note that the
first alternative includes everything from the last pattern delimiter
("(\*(R", \*(L"[\*(R", or the beginning of the pattern) up to the first \*(L"|\*(R", and
the last alternative contains everything from the last \*(L"|\*(R" to the next
pattern delimiter.  For this reason, it's common practice to include
alternatives in parentheses, to minimize confusion about where they
start and end.  Note however that \*(L"|\*(R" is interpreted as a literal with
square brackets, so if you write \f(CW[fee|fie|foe]\fR you're really only
matching \f(CW[feio|]\fR.
.PP
Within a pattern, you may designate subpatterns for later reference by
enclosing them in parentheses, and you may refer back to the \fIn\fRth
subpattern later in the pattern using the metacharacter \e\fIn\fR. 
Subpatterns are numbered based on the left to right order of their
opening parenthesis.  Note that a backreference matches whatever
actually matched the subpattern in the string being examined, not the
rules for that subpattern.  Therefore, \f(CW(0|0x)\ed*\es\e1\ed*\fR will
match \*(L"0x1234 0x4321\*(R",but not \*(L"0x1234 01234\*(R", since subpattern 1
actually matched \*(L"0x\*(R", even though the rule \f(CW0|0x\fR could
potentially match the leading 0 in the second number.

.rn }` ''

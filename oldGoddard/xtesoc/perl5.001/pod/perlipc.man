.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLIPC 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlipc \- Perl interprocess communication
.SH "DESCRIPTION"
The IPC facilities of Perl are built on the Berkeley socket mechanism.
If you don't have sockets, you can ignore this section.  The calls have
the same names as the corresponding system calls, but the arguments
tend to differ, for two reasons.  First, Perl file handles work
differently than C file descriptors.  Second, Perl already knows the
length of its strings, so you don't need to pass that information.
.Sh "Client/Server Communication"
Here's a sample \s-1TCP\s0 client.
.PP
.Vb 3
\&    ($them,$port) = @ARGV;
\&    $port = 2345 unless $port;
\&    $them = 'localhost' unless $them;
.Ve
.Vb 2
\&    $SIG{'INT'} = 'dokill';
\&    sub dokill { kill 9,$child if $child; }
.Ve
.Vb 1
\&    use Socket;
.Ve
.Vb 2
\&    $sockaddr = 'S n a4 x8';
\&    chop($hostname = `hostname`);
.Ve
.Vb 6
\&    ($name, $aliases, $proto) = getprotobyname('tcp');
\&    ($name, $aliases, $port) = getservbyname($port, 'tcp')
\&        unless $port =~ /^\ed+$/;
\&    ($name, $aliases, $type, $len, $thisaddr) =
\&                    gethostbyname($hostname);
\&    ($name, $aliases, $type, $len, $thataddr) = gethostbyname($them);
.Ve
.Vb 2
\&    $this = pack($sockaddr, AF_INET, 0, $thisaddr);
\&    $that = pack($sockaddr, AF_INET, $port, $thataddr);
.Ve
.Vb 3
\&    socket(S, PF_INET, SOCK_STREAM, $proto) || die "socket: $!";
\&    bind(S, $this) || die "bind: $!";
\&    connect(S, $that) || die "connect: $!";
.Ve
.Vb 1
\&    select(S); $| = 1; select(stdout);
.Ve
.Vb 12
\&    if ($child = fork) {
\&        while (<>) {
\&            print S;
\&        }
\&        sleep 3;
\&        do dokill();
\&    }
\&    else {
\&        while (<S>) {
\&            print;
\&        }
\&    }
.Ve
And here's a server:
.PP
.Vb 2
\&    ($port) = @ARGV;
\&    $port = 2345 unless $port;
.Ve
.Vb 1
\&    use Socket;
.Ve
.Vb 1
\&    $sockaddr = 'S n a4 x8';
.Ve
.Vb 3
\&    ($name, $aliases, $proto) = getprotobyname('tcp');
\&    ($name, $aliases, $port) = getservbyname($port, 'tcp')
\&        unless $port =~ /^\ed+$/;
.Ve
.Vb 1
\&    $this = pack($sockaddr, AF_INET, $port, "\e0\e0\e0\e0");
.Ve
.Vb 1
\&    select(NS); $| = 1; select(stdout);
.Ve
.Vb 3
\&    socket(S, PF_INET, SOCK_STREAM, $proto) || die "socket: $!";
\&    bind(S, $this) || die "bind: $!";
\&    listen(S, 5) || die "connect: $!";
.Ve
.Vb 1
\&    select(S); $| = 1; select(stdout);
.Ve
.Vb 4
\&    for (;;) {
\&        print "Listening again\en";
\&        ($addr = accept(NS,S)) || die $!;
\&        print "accept ok\en";
.Ve
.Vb 3
\&        ($af,$port,$inetaddr) = unpack($sockaddr,$addr);
\&        @inetaddr = unpack('C4',$inetaddr);
\&        print "$af $port @inetaddr\en";
.Ve
.Vb 5
\&        while (<NS>) {
\&            print;
\&            print NS;
\&        }
\&    }
.Ve
.Sh "SysV \s-1IPC\s0"
Here's a small example showing shared memory usage:
.PP
.Vb 5
\&    $IPC_PRIVATE = 0;
\&    $IPC_RMID = 0;
\&    $size = 2000;
\&    $key = shmget($IPC_PRIVATE, $size , 0777 );
\&    die if !defined($key);
.Ve
.Vb 3
\&    $message = "Message #1";
\&    shmwrite($key, $message, 0, 60 ) || die "$!";
\&    shmread($key,$buff,0,60) || die "$!";
.Ve
.Vb 1
\&    print $buff,"\en";
.Ve
.Vb 2
\&    print "deleting $key\en";
\&    shmctl($key ,$IPC_RMID, 0) || die "$!";
.Ve
Here's an example of a semaphore:
.PP
.Vb 6
\&    $IPC_KEY = 1234;
\&    $IPC_RMID = 0;
\&    $IPC_CREATE = 0001000;
\&    $key = semget($IPC_KEY, $nsems , 0666 | $IPC_CREATE );
\&    die if !defined($key);
\&    print "$key\en";
.Ve
Put this code in a separate file to be run in more that one process
Call the file \fItake\fR:
.PP
.Vb 1
\&    # create a semaphore
.Ve
.Vb 3
\&    $IPC_KEY = 1234;
\&    $key = semget($IPC_KEY,  0 , 0 );
\&    die if !defined($key);
.Ve
.Vb 2
\&    $semnum = 0;
\&    $semflag = 0;
.Ve
.Vb 4
\&    # 'take' semaphore
\&    # wait for semaphore to be zero
\&    $semop = 0;
\&    $opstring1 = pack("sss", $semnum, $semop, $semflag);
.Ve
.Vb 4
\&    # Increment the semaphore count
\&    $semop = 1;
\&    $opstring2 = pack("sss", $semnum, $semop,  $semflag);
\&    $opstring = $opstring1 . $opstring2;
.Ve
.Vb 1
\&    semop($key,$opstring) || die "$!";
.Ve
Put this code in a separate file to be run in more that one process
Call this file \fIgive\fR:
.PP
.Vb 3
\&    #'give' the semaphore
\&    # run this in the original process and you will see
\&    # that the second process continues
.Ve
.Vb 3
\&    $IPC_KEY = 1234;
\&    $key = semget($IPC_KEY, 0, 0);
\&    die if !defined($key);
.Ve
.Vb 2
\&    $semnum = 0;
\&    $semflag = 0;
.Ve
.Vb 3
\&    # Decrement the semaphore count
\&    $semop = -1;
\&    $opstring = pack("sss", $semnum, $semop, $semflag);
.Ve
.Vb 1
\&    semop($key,$opstring) || die "$!";
.Ve

.rn }` ''

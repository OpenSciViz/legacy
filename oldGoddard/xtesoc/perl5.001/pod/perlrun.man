.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLRUN 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlrun \- how to execute the Perl interpreter
.SH "SYNOPSIS"
\fBperl\fR [switches] filename args
.SH "DESCRIPTION"
Upon startup, Perl looks for your script in one of the following
places:
.Ip "1." 4
Specified line by line via \fB\-e\fR switches on the command line.
.Ip "2." 4
Contained in the file specified by the first filename on the command line.
(Note that systems supporting the #! notation invoke interpreters this way.)
.Ip "3." 4
Passed in implicitly via standard input.  This only works if there are
no filename arguments\*(--to pass arguments to a \s-1STDIN\s0 script you
must explicitly specify a \*(L"\-\*(R" for the script name.
.PP
With methods 2 and 3, Perl starts parsing the input file from the
beginning, unless you've specified a \fB\-x\fR switch, in which case it
scans for the first line starting with #! and containing the word
\*(L"perl\*(R", and starts there instead.  This is useful for running a script
embedded in a larger message.  (In this case you would indicate the end
of the script using the _\|_END_\|_ token.)
.PP
As of Perl 5, the #! line is always examined for switches as the line is
being parsed.  Thus, if you're on a machine that only allows one argument
with the #! line, or worse, doesn't even recognize the #! line, you still
can get consistent switch behavior regardless of how Perl was invoked,
even if \fB\-x\fR was used to find the beginning of the script.
.PP
Because many operating systems silently chop off kernel interpretation of
the #! line after 32 characters, some switches may be passed in on the
command line, and some may not; you could even get a \*(L"\-\*(R" without its
letter, if you're not careful.  You probably want to make sure that all
your switches fall either before or after that 32 character boundary.
Most switches don't actually care if they're processed redundantly, but
getting a \- instead of a complete switch could cause Perl to try to
execute standard input instead of your script.  And a partial \fB\-I\fR switch
could also cause odd results.
.PP
Parsing of the #! switches starts wherever \*(L"perl\*(R" is mentioned in the line.
The sequences \*(L"\-*\*(R" and \*(L"\- \*(L" are specifically ignored so that you could,
if you were so inclined, say
.PP
.Vb 3
\&    #!/bin/sh -- # -*- perl -*- -p
\&    eval 'exec perl $0 -S ${1+"$@"}'
\&        if 0;
.Ve
to let Perl see the \fB\-p\fR switch.
.PP
If the #! line does not contain the word \*(L"perl\*(R", the program named after
the #! is executed instead of the Perl interpreter.  This is slightly
bizarre, but it helps people on machines that don't do #!, because they
can tell a program that their \s-1SHELL\s0 is /usr/bin/perl, and Perl will then
dispatch the program to the correct interpreter for them.
.PP
After locating your script, Perl compiles the entire script to an
internal form.  If there are any compilation errors, execution of the
script is not attempted.  (This is unlike the typical shell script,
which might run partway through before finding a syntax error.)
.PP
If the script is syntactically correct, it is executed.  If the script
runs off the end without hitting an \fIexit()\fR or \fIdie()\fR operator, an implicit
\f(CWexit(0)\fR is provided to indicate successful completion.
.Sh "Switches"
A single-character switch may be combined with the following switch, if
any.
.PP
.Vb 1
\&    #!/usr/bin/perl -spi.bak    # same as -s -p -i.bak
.Ve
Switches include:
.Ip "\fB\-0\fR\fIdigits\fR" 5
specifies the record separator (\f(CW$/\fR) as an octal number.  If there are
no digits, the null character is the separator.  Other switches may
precede or follow the digits.  For example, if you have a version of
\fBfind\fR which can print filenames terminated by the null character, you
can say this:
.Sp
.Vb 1
\&    find . -name '*.bak' -print0 | perl -n0e unlink
.Ve
The special value 00 will cause Perl to slurp files in paragraph mode.
The value 0777 will cause Perl to slurp files whole since there is no
legal character with that value.
.Ip "\fB\-a\fR" 5
turns on autosplit mode when used with a \fB\-n\fR or \fB\-p\fR.  An implicit
split command to the \f(CW@F\fR array is done as the first thing inside the
implicit while loop produced by the \fB\-n\fR or \fB\-p\fR.
.Sp
.Vb 1
\&    perl -ane 'print pop(@F), "\en";'
.Ve
is equivalent to
.Sp
.Vb 4
\&    while (<>) {
\&        @F = split(' ');
\&        print pop(@F), "\en";
\&    }
.Ve
An alternate delimiter may be specified using \fB\-F\fR.
.Ip "\fB\-c\fR" 5
causes Perl to check the syntax of the script and then exit without
executing it.  Actually, it will execute \f(CWBEGIN\fR and \f(CWuse\fR blocks,
since these are considered part of the compilation.
.Ip "\fB\-d\fR" 5
runs the script under the Perl debugger.  See the \fIperldebug\fR manpage.
.Ip "\fB\-D\fR\fInumber\fR" 5
.Ip "\fB\-D\fR\fIlist\fR" 5
sets debugging flags.  To watch how it executes your script, use
\fB\-D14\fR.  (This only works if debugging is compiled into your
Perl.)  Another nice value is \fB\-D1024\fR, which lists your compiled
syntax tree.  And \fB\-D512\fR displays compiled regular expressions. As an
alternative specify a list of letters instead of numbers (e.g. \fB\-D14\fR is
equivalent to \fB\-Dtls\fR):
.Sp
.Vb 16
\&        1  p  Tokenizing and Parsing
\&        2  s  Stack Snapshots
\&        4  l  Label Stack Processing
\&        8  t  Trace Execution
\&       16  o  Operator Node Construction
\&       32  c  String/Numeric Conversions
\&       64  P  Print Preprocessor Command for -P
\&      128  m  Memory Allocation
\&      256  f  Format Processing
\&      512  r  Regular Expression Parsing
\&     1024  x  Syntax Tree Dump
\&     2048  u  Tainting Checks
\&     4096  L  Memory Leaks (not supported anymore)
\&     8192  H  Hash Dump -- usurps values()
\&    16384  X  Scratchpad Allocation
\&    32768  D  Cleaning Up
.Ve
.Ip "\fB\-e\fR \fIcommandline\fR" 5
may be used to enter one line of script.  
If \fB\-e\fR is given, Perl
will not look for a script filename in the argument list.  
Multiple \fB\-e\fR commands may
be given to build up a multi-line script.  
Make sure to use semicolons where you would in a normal program.
.Ip "\fB\-F\fR\fIregexp\fR" 5
specifies a regular expression to split on if \fB\-a\fR is also in effect.
If regexp has \f(CW//\fR around it, the slashes will be ignored.
.Ip "\fB\-i\fR\fIextension\fR" 5
specifies that files processed by the \f(CW<>\fR construct are to be edited
in-place.  It does this by renaming the input file, opening the output
file by the original name, and selecting that output file as the default
for \fIprint()\fR statements.  The extension, if supplied, is added to the name
of the old file to make a backup copy.  If no extension is supplied, no
backup is made.  From the shell, saying
.Sp
.Vb 1
\&    $ perl -p -i.bak -e "s/foo/bar/; ... "
.Ve
is the same as using the script:
.Sp
.Vb 2
\&    #!/usr/bin/perl -pi.bak
\&    s/foo/bar/;
.Ve
which is equivalent to
.Sp
.Vb 14
\&    #!/usr/bin/perl
\&    while (<>) {
\&        if ($ARGV ne $oldargv) {
\&            rename($ARGV, $ARGV . '.bak');
\&            open(ARGVOUT, ">$ARGV");
\&            select(ARGVOUT);
\&            $oldargv = $ARGV;
\&        }
\&        s/foo/bar/;
\&    }
\&    continue {
\&        print;  # this prints to original filename
\&    }
\&    select(STDOUT);
.Ve
except that the \fB\-i\fR form doesn't need to compare \f(CW$ARGV\fR to \f(CW$oldargv\fR to
know when the filename has changed.  It does, however, use \s-1ARGVOUT\s0 for
the selected filehandle.  Note that \s-1STDOUT\s0 is restored as the
default output filehandle after the loop.
.Sp
You can use \f(CWeof\fR without parenthesis to locate the end of each input file, 
in case you want to append to each file, or reset line numbering (see 
example in the \f(CWeof\fR entry in the \fIperlfunc\fR manpage).
.Ip "\fB\-I\fR\fIdirectory\fR" 5
may be used in conjunction with \fB\-P\fR to tell the C preprocessor where
to look for include files.  By default /usr/include and /usr/lib/perl
are searched.
.Ip "\fB\-l\fR\fIoctnum\fR" 5
enables automatic line-ending processing.  It has two effects:  first,
it automatically chomps the line terminator when used with \fB\-n\fR or
\fB\-p\fR, and second, it assigns \*(L"\f(CW$\e\fR\*(R" to have the value of \fIoctnum\fR so that
any print statements will have that line terminator added back on.  If
\fIoctnum\fR is omitted, sets \*(L"\f(CW$\e\fR\*(R" to the current value of \*(L"\f(CW$/\fR\*(R".  For
instance, to trim lines to 80 columns:
.Sp
.Vb 1
\&    perl -lpe 'substr($_, 80) = ""'
.Ve
Note that the assignment \f(CW$\e = $/\fR is done when the switch is processed,
so the input record separator can be different than the output record
separator if the \fB\-l\fR switch is followed by a \fB\-0\fR switch:
.Sp
.Vb 1
\&    gnufind / -print0 | perl -ln0e 'print "found $_" if -p'
.Ve
This sets $\e to newline and then sets $/ to the null character.
.Ip "\fB\-n\fR" 5
causes Perl to assume the following loop around your script, which
makes it iterate over filename arguments somewhat like \fBsed \-n\fR or
\fBawk\fR:
.Sp
.Vb 3
\&    while (<>) {
\&        ...             # your script goes here
\&    }
.Ve
Note that the lines are not printed by default.  See \fB\-p\fR to have
lines printed.  Here is an efficient way to delete all files older than
a week:
.Sp
.Vb 1
\&    find . -mtime +7 -print | perl -nle 'unlink;'
.Ve
This is faster than using the \f(CW-exec\fR switch of \fBfind\fR because you don't
have to start a process on every filename found.
.Sp
\f(CWBEGIN\fR and \f(CWEND\fR blocks may be used to capture control before or after
the implicit loop, just as in \fBawk\fR.
.Ip "\fB\-p\fR" 5
causes Perl to assume the following loop around your script, which
makes it iterate over filename arguments somewhat like \fBsed\fR:
.Sp
.Vb 5
\&    while (<>) {
\&        ...             # your script goes here
\&    } continue {
\&        print;
\&    }
.Ve
Note that the lines are printed automatically.  To suppress printing
use the \fB\-n\fR switch.  A \fB\-p\fR overrides a \fB\-n\fR switch.
.Sp
\f(CWBEGIN\fR and \f(CWEND\fR blocks may be used to capture control before or after
the implicit loop, just as in awk.
.Ip "\fB\-P\fR" 5
causes your script to be run through the C preprocessor before
compilation by Perl.  (Since both comments and cpp directives begin
with the # character, you should avoid starting comments with any words
recognized by the C preprocessor such as \*(L"if\*(R", \*(L"else\*(R" or \*(L"define\*(R".)
.Ip "\fB\-s\fR" 5
enables some rudimentary switch parsing for switches on the command
line after the script name but before any filename arguments (or before
a \fB--\fR).  Any switch found there is removed from \f(CW@ARGV\fR and sets the
corresponding variable in the Perl script.  The following script
prints \*(L"true\*(R" if and only if the script is invoked with a \fB\-xyz\fR switch.
.Sp
.Vb 2
\&    #!/usr/bin/perl -s
\&    if ($xyz) { print "true\en"; }
.Ve
.Ip "\fB\-S\fR" 5
makes Perl use the \s-1PATH\s0 environment variable to search for the
script (unless the name of the script starts with a slash).  Typically
this is used to emulate #! startup on machines that don't support #!,
in the following manner:
.Sp
.Vb 3
\&    #!/usr/bin/perl
\&    eval "exec /usr/bin/perl -S $0 $*"
\&            if $running_under_some_shell;
.Ve
The system ignores the first line and feeds the script to /bin/sh,
which proceeds to try to execute the Perl script as a shell script.
The shell executes the second line as a normal shell command, and thus
starts up the Perl interpreter.  On some systems \f(CW$0\fR doesn't always
contain the full pathname, so the \fB\-S\fR tells Perl to search for the
script if necessary.  After Perl locates the script, it parses the
lines and ignores them because the variable \f(CW$running_under_some_shell\fR
is never true.  A better construct than \f(CW$*\fR would be \f(CW${1+"$@"}\fR, which
handles embedded spaces and such in the filenames, but doesn't work if
the script is being interpreted by csh.  In order to start up sh rather
than csh, some systems may have to replace the #! line with a line
containing just a colon, which will be politely ignored by Perl.  Other
systems can't control that, and need a totally devious construct that
will work under any of csh, sh or Perl, such as the following:
.Sp
.Vb 3
\&        eval '(exit $?0)' && eval 'exec /usr/bin/perl -S $0 ${1+"$@"}'
\&        & eval 'exec /usr/bin/perl -S $0 $argv:q'
\&                if 0;
.Ve
.Ip "\fB\-T\fR" 5
forces \*(L"taint\*(R" checks to be turned on.  Ordinarily these checks are
done only when running setuid or setgid.  See the \fIperlsec\fR manpage.
.Ip "\fB\-u\fR" 5
causes Perl to dump core after compiling your script.  You can then
take this core dump and turn it into an executable file by using the
\fBundump\fR program (not supplied).  This speeds startup at the expense of
some disk space (which you can minimize by stripping the executable).
(Still, a \*(L"hello world\*(R" executable comes out to about 200K on my
machine.)  If you want to execute a portion of your script before dumping,
use the \fIdump()\fR operator instead.  Note: availability of \fBundump\fR is
platform specific and may not be available for a specific port of
Perl.
.Ip "\fB\-U\fR" 5
allows Perl to do unsafe operations.  Currently the only \*(L"unsafe\*(R"
operations are the unlinking of directories while running as superuser,
and running setuid programs with fatal taint checks turned into
warnings.
.Ip "\fB\-v\fR" 5
prints the version and patchlevel of your Perl executable.
.Ip "\fB\-w\fR" 5
prints warnings about identifiers that are mentioned only once, and
scalar variables that are used before being set.  Also warns about
redefined subroutines, and references to undefined filehandles or
filehandles opened readonly that you are attempting to write on.  Also
warns you if you use values as a number that doesn't look like numbers, using
an array as though it were a scalar, if
your subroutines recurse more than 100 deep, and innumerable other things.
See the \fIperldiag\fR manpage and the \fIperltrap\fR manpage.
.Ip "\fB\-x\fR \fIdirectory\fR" 5
tells Perl that the script is embedded in a message.  Leading
garbage will be discarded until the first line that starts with #! and
contains the string \*(L"perl\*(R".  Any meaningful switches on that line will
be applied (but only one group of switches, as with normal #!
processing).  If a directory name is specified, Perl will switch to
that directory before running the script.  The \fB\-x\fR switch only
controls the the disposal of leading garbage.  The script must be
terminated with \f(CW__END__\fR if there is trailing garbage to be ignored (the
script can process any or all of the trailing garbage via the \s-1DATA\s0
filehandle if desired).

.rn }` ''

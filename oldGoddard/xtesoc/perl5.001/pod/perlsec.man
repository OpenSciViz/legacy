.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLSEC 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlsec \- Perl security
.SH "DESCRIPTION"
Perl is designed to make it easy to write secure setuid and setgid
scripts.  Unlike shells, which are based on multiple substitution
passes on each line of the script, Perl uses a more conventional
evaluation scheme with fewer hidden \*(L"gotchas\*(R".  Additionally, since the
language has more built-in functionality, it has to rely less upon
external (and possibly untrustworthy) programs to accomplish its
purposes.
.PP
Beyond the obvious problems that stem from giving special privileges to
such flexible systems as scripts, on many operating systems, setuid
scripts are inherently insecure right from the start.  This is because
that between the time that the kernel opens up the file to see what to
run, and when the now setuid interpreter it ran turns around and reopens
the file so it can interpret it, things may have changed, especially if
you have symbolic links on your system.
.PP
Fortunately, sometimes this kernel \*(L"feature\*(R" can be disabled.
Unfortunately, there are two ways to disable it.  The system can simply
outlaw scripts with the setuid bit set, which doesn't help much.
Alternately, it can simply ignore the setuid bit on scripts.  If the
latter is true, Perl can emulate the setuid and setgid mechanism when it
notices the otherwise useless setuid/gid bits on Perl scripts.  It does
this via a special executable called \fBsuidperl\fR that is automatically
invoked for you if it's needed.
.PP
If, however, the kernel setuid script feature isn't disabled, Perl will
complain loudly that your setuid script is insecure.  You'll need to
either disable the kernel setuid script feature, or put a C wrapper around
the script.  See the program \fBwrapsuid\fR in the \fIeg\fR directory of your
Perl distribution for how to go about doing this.
.PP
There are some systems on which setuid scripts are free of this inherent
security bug.  For example, recent releases of Solaris are like this.  On
such systems, when the kernel passes the name of the setuid script to open
to the interpreter, rather than using a pathname subject to mettling, it
instead passes /dev/fd/3.  This is a special file already opened on the
script, so that there can be no race condition for evil scripts to
exploit.  On these systems, Perl should be compiled with
\f(CW-DSETUID_SCRIPTS_ARE_SECURE_NOW\fR.  The \fBConfigure\fR program that builds
Perl tries to figure this out for itself.
.PP
When Perl is executing a setuid script, it takes special precautions to
prevent you from falling into any obvious traps.  (In some ways, a Perl
script is more secure than the corresponding C program.)  Any command line
argument, environment variable, or input is marked as \*(L"tainted\*(R", and may
not be used, directly or indirectly, in any command that invokes a
subshell, or in any command that modifies files, directories, or
processes.  Any variable that is set within an expression that has
previously referenced a tainted value also becomes tainted (even if it is
logically impossible for the tainted value to influence the variable).
For example:
.PP
.Vb 5
\&    $foo = shift;               # $foo is tainted
\&    $bar = $foo,'bar';          # $bar is also tainted
\&    $xxx = <>;                  # Tainted
\&    $path = $ENV{'PATH'};       # Tainted, but see below
\&    $abc = 'abc';               # Not tainted
.Ve
.Vb 4
\&    system "echo $foo";         # Insecure
\&    system "/bin/echo", $foo;   # Secure (doesn't use sh)
\&    system "echo $bar";         # Insecure
\&    system "echo $abc";         # Insecure until PATH set
.Ve
.Vb 2
\&    $ENV{'PATH'} = '/bin:/usr/bin';
\&    $ENV{'IFS'} = '' if $ENV{'IFS'} ne '';
.Ve
.Vb 2
\&    $path = $ENV{'PATH'};       # Not tainted
\&    system "echo $abc";         # Is secure now!
.Ve
.Vb 2
\&    open(FOO,"$foo");           # OK
\&    open(FOO,">$foo");          # Not OK
.Ve
.Vb 2
\&    open(FOO,"echo $foo|");     # Not OK, but...
\&    open(FOO,"-|") || exec 'echo', $foo;        # OK
.Ve
.Vb 1
\&    $zzz = `echo $foo`;         # Insecure, zzz tainted
.Ve
.Vb 2
\&    unlink $abc,$foo;           # Insecure
\&    umask $foo;                 # Insecure
.Ve
.Vb 3
\&    exec "echo $foo";           # Insecure
\&    exec "echo", $foo;          # Secure (doesn't use sh)
\&    exec "sh", '-c', $foo;      # Considered secure, alas
.Ve
The taintedness is associated with each scalar value, so some elements
of an array can be tainted, and others not.
.PP
If you try to do something insecure, you will get a fatal error saying
something like \*(L"Insecure dependency\*(R" or \*(L"Insecure PATH\*(R".  Note that you
can still write an insecure system call or exec, but only by explicitly
doing something like the last example above.  You can also bypass the
tainting mechanism by referencing subpatterns\*(--Perl presumes that if
you reference a substring using \f(CW$1\fR, \f(CW$2\fR, etc, you knew what you were
doing when you wrote the pattern:
.PP
.Vb 2
\&    $ARGV[0] =~ /^-P(\ew+)$/;
\&    $printer = $1;              # Not tainted
.Ve
This is fairly secure since \f(CW\ew+\fR doesn't match shell metacharacters.
Use of \f(CW/.+/\fR would have been insecure, but Perl doesn't check for that,
so you must be careful with your patterns.  This is the \fIONLY\fR mechanism
for untainting user supplied filenames if you want to do file operations
on them (unless you make \f(CW$>\fR equal to \f(CW$<\fR ).
.PP
For \*(L"Insecure \f(CW$ENV\fR{PATH}\*(R" messages, you need to set \f(CW$ENV{'PATH'}\fR to a known
value, and each directory in the path must be non-writable by the world.
A frequently voiced gripe is that you can get this message even
if the pathname to an executable is fully qualified.  But Perl can't
know that the executable in question isn't going to execute some other
program depending on the PATH.
.PP
It's also possible to get into trouble with other operations that don't
care whether they use tainted values.  Make judicious use of the file
tests in dealing with any user-supplied filenames.  When possible, do
opens and such after setting \f(CW$> = $<\fR.  (Remember group IDs,
too!) Perl doesn't prevent you from opening tainted filenames for reading,
so be careful what you print out.  The tainting mechanism is intended to
prevent stupid mistakes, not to remove the need for thought.

.rn }` ''

.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLTRAP 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perltrap \- Perl traps for the unwary
.SH "DESCRIPTION"
The biggest trap of all is forgetting to use the \fB\-w\fR switch;
see the \fIperlrun\fR manpage.  Making your entire program runnable under
.PP
.Vb 1
\&    use strict;
.Ve
can help make your program more bullet-proof, but sometimes
it's too annoying for quick throw-away programs.
.Sh "Awk Traps"
Accustomed \fBawk\fR users should take special note of the following:
.Ip "\(bu" 4
The English module, loaded via
.Sp
.Vb 1
\&    use English;
.Ve
allows you to refer to special variables (like \f(CW$RS\fR) as 
though they were in \fBawk\fR; see the \fIperlvar\fR manpage for details.
.Ip "\(bu" 4
Semicolons are required after all simple statements in Perl (except
at the end of a block).  Newline is not a statement delimiter.
.Ip "\(bu" 4
Curly brackets are required on \f(CWif\fRs and \f(CWwhile\fRs.
.Ip "\(bu" 4
Variables begin with \*(L"$\*(R" or \*(L"@\*(R" in Perl.
.Ip "\(bu" 4
Arrays index from 0.  Likewise string positions in \fIsubstr()\fR and
\fIindex()\fR.
.Ip "\(bu" 4
You have to decide whether your array has numeric or string indices.
.Ip "\(bu" 4
Associative array values do not spring into existence upon mere
reference.
.Ip "\(bu" 4
You have to decide whether you want to use string or numeric
comparisons.
.Ip "\(bu" 4
Reading an input line does not split it for you.  You get to split it
yourself to an array.  And \fIsplit()\fR operator has different
arguments.
.Ip "\(bu" 4
The current input line is normally in \f(CW$_\fR, not \f(CW$0\fR.  It generally does
not have the newline stripped.  ($0 is the name of the program
executed.)  See the \fIperlvar\fR manpage.
.Ip "\(bu" 4
$<\fIdigit\fR> does not refer to fields\*(--it refers to substrings matched by
the last match pattern.
.Ip "\(bu" 4
The \fIprint()\fR statement does not add field and record separators unless
you set \f(CW$,\fR and \f(CW$.\fR.  You can set \f(CW$OFS\fR and \f(CW$ORS\fR if you're using
the English module.
.Ip "\(bu" 4
You must open your files before you print to them.
.Ip "\(bu" 4
The range operator is \*(L"..\*(R", not comma.  The comma operator works as in
C.
.Ip "\(bu" 4
The match operator is \*(L"=~\*(R", not \*(L"~\*(R".  ("~\*(R" is the one's complement
operator, as in C.)
.Ip "\(bu" 4
The exponentiation operator is \*(L"**\*(R", not \*(L"^\*(R".  \*(L"^\*(R" is the \s-1XOR\s0
operator, as in C.  (You know, one could get the feeling that \fBawk\fR is
basically incompatible with C.)
.Ip "\(bu" 4
The concatenation operator is \*(L".\*(R", not the null string.  (Using the
null string would render \f(CW/pat/ /pat/\fR unparsable, since the third slash
would be interpreted as a division operator\*(--the tokener is in fact
slightly context sensitive for operators like \*(L"/\*(R", \*(L"?\*(R", and \*(L">\*(R".
And in fact, \*(L".\*(R" itself can be the beginning of a number.)
.Ip "\(bu" 4
The \f(CWnext\fR, \f(CWexit\fR, and \f(CWcontinue\fR keywords work differently.
.Ip "\(bu" 4
The following variables work differently:
.Sp
.Vb 15
\&      Awk       Perl
\&      ARGC      $#ARGV or scalar @ARGV
\&      ARGV[0]   $0
\&      FILENAME  $ARGV
\&      FNR       $. - something
\&      FS        (whatever you like)
\&      NF        $#Fld, or some such
\&      NR        $.
\&      OFMT      $#
\&      OFS       $,
\&      ORS       $\e
\&      RLENGTH   length($&)
\&      RS        $/
\&      RSTART    length($`)
\&      SUBSEP    $;
.Ve
.Ip "\(bu" 4
You cannot set \f(CW$RS\fR to a pattern, only a string.
.Ip "\(bu" 4
When in doubt, run the \fBawk\fR construct through \fBa2p\fR and see what it
gives you.
.Sh "C Traps"
Cerebral C programmers should take note of the following:
.Ip "\(bu" 4
Curly brackets are required on \f(CWif\fR's and \f(CWwhile\fR's.
.Ip "\(bu" 4
You must use \f(CWelsif\fR rather than \f(CWelse if\fR.
.Ip "\(bu" 4
The \f(CWbreak\fR and \f(CWcontinue\fR keywords from C become in 
Perl \f(CWlast\fR and \f(CWnext\fR, respectively.
Unlike in C, these do \fI\s-1NOT\s0\fR work within a \f(CWdo { } while\fR construct.
.Ip "\(bu" 4
There's no switch statement.  (But it's easy to build one on the fly.)
.Ip "\(bu" 4
Variables begin with \*(L"$\*(R" or \*(L"@\*(R" in Perl.
.Ip "\(bu" 4
\fIprintf()\fR does not implement the \*(L"*\*(R" format for interpolating
field widths, but it's trivial to use interpolation of double-quoted
strings to achieve the same effect.
.Ip "\(bu" 4
Comments begin with \*(L"#\*(R", not \*(L"/*\*(R".
.Ip "\(bu" 4
You can't take the address of anything, although a similar operator
in Perl 5 is the backslash, which creates a reference.
.Ip "\(bu" 4
\f(CWARGV\fR must be capitalized.
.Ip "\(bu" 4
System calls such as \fIlink()\fR, \fIunlink()\fR, \fIrename()\fR, etc. return nonzero for
success, not 0.
.Ip "\(bu" 4
Signal handlers deal with signal names, not numbers.  Use \f(CWkill -l\fR
to find their names on your system.
.Sh "Sed Traps"
Seasoned \fBsed\fR programmers should take note of the following:
.Ip "\(bu" 4
Backreferences in substitutions use \*(L"$\*(R" rather than \*(L"\e\*(R".
.Ip "\(bu" 4
The pattern matching metacharacters \*(L"(\*(R", \*(L")\*(R", and \*(L"|\*(R" do not have backslashes
in front.
.Ip "\(bu" 4
The range operator is \f(CW...\fR, rather than comma.
.Sh "Shell Traps"
Sharp shell programmers should take note of the following:
.Ip "\(bu" 4
The backtick operator does variable interpretation without regard to
the presence of single quotes in the command.
.Ip "\(bu" 4
The backtick operator does no translation of the return value, unlike \fBcsh\fR.
.Ip "\(bu" 4
Shells (especially \fBcsh\fR) do several levels of substitution on each
command line.  Perl does substitution only in certain constructs
such as double quotes, backticks, angle brackets, and search patterns.
.Ip "\(bu" 4
Shells interpret scripts a little bit at a time.  Perl compiles the
entire program before executing it (except for \f(CWBEGIN\fR blocks, which
execute at compile time).
.Ip "\(bu" 4
The arguments are available via \f(CW@ARGV\fR, not \f(CW$1\fR, \f(CW$2\fR, etc.
.Ip "\(bu" 4
The environment is not automatically made available as separate scalar
variables.
.Sh "Perl Traps"
Practicing Perl Programmers should take note of the following:
.Ip "\(bu" 4
Remember that many operations behave differently in a list
context than they do in a scalar one.  See the \fIperldata\fR manpage for details.
.Ip "\(bu" 4
Avoid barewords if you can, especially all lower-case ones.
You can't tell just by looking at it whether a bareword is 
a function or a string.  By using quotes on strings and 
parens on function calls, you won't ever get them confused.
.Ip "\(bu" 4
You cannot discern from mere inspection which built-ins
are unary operators (like \fIchop()\fR and \fIchdir()\fR) 
and which are list operators (like \fIprint()\fR and \fIunlink()\fR).
(User-defined subroutines can \fBonly\fR be list operators, never
unary ones.)  See the \fIperlop\fR manpage.
.Ip "\(bu" 4
People have a hard time remembering that some functions
default to \f(CW$_\fR, or \f(CW@ARGV\fR, or whatever, but that others which
you might expect to do not.  
.Ip "\(bu " 4
The <\s-1FH\s0> construct is not the name of the filehandle, it is a readline
operation on that handle.  The data read is only assigned to \f(CW$_\fR if the
file read is the sole condition in a while loop:
.Sp
.Vb 3
\&    while (<FH>)      { }
\&    while ($_ = <FH>) { }..
\&    <FH>;  # data discarded!
.Ve
.Ip "\(bu " 4
Remember not to use \*(L"\f(CW=\fR\*(R" when you need \*(L"\f(CW=~\fR\*(R";
these two constructs are quite different:
.Sp
.Vb 2
\&    $x =  /foo/;
\&    $x =~ /foo/;
.Ve
.Ip "\(bu" 4
The \f(CWdo {}\fR construct isn't a real loop that you can use 
loop control on.
.Ip "\(bu" 4
Use \fImy()\fR for local variables whenever you can get away with 
it (but see the \fIperlform\fR manpage for where you can't).  
Using \fIlocal()\fR actually gives a local value to a global 
variable, which leaves you open to unforeseen side-effects
of dynamic scoping.
.Sh "Perl4 Traps"
Penitent Perl 4 Programmers should take note of the following
incompatible changes that occurred between release 4 and release 5:
.Ip "\(bu" 4
\f(CW@\fR now always interpolates an array in double-quotish strings.  Some programs
may now need to use backslash to protect any \f(CW@\fR that shouldn't interpolate.
.Ip "\(bu" 4
Barewords that used to look like strings to Perl will now look like subroutine
calls if a subroutine by that name is defined before the compiler sees them.
For example:
.Sp
.Vb 2
\&    sub SeeYa { die "Hasta la vista, baby!" }
\&    $SIG{'QUIT'} = SeeYa;
.Ve
In Perl 4, that set the signal handler; in Perl 5, it actually calls the
function!  You may use the \fB\-w\fR switch to find such places.
.Ip "\(bu" 4
Symbols starting with \f(CW_\fR are no longer forced into package \f(CWmain\fR, except
for \f(CW$_\fR itself (and \f(CW@_\fR, etc.).
.Ip "\(bu" 4
\f(CWs'$lhs'$rhs'\fR now does no interpolation on either side.  It used to
interpolate \f(CW$lhs\fR but not \f(CW$rhs\fR.
.Ip "\(bu" 4
The second and third arguments of \fIsplice()\fR are now evaluated in scalar
context (as the book says) rather than list context.
.Ip "\(bu" 4
These are now semantic errors because of precedence:
.Sp
.Vb 2
\&    shift @list + 20;   
\&    $n = keys %map + 20; 
.Ve
Because if that were to work, then this couldn't:
.Sp
.Vb 1
\&    sleep $dormancy + 20;
.Ve
.Ip "\(bu" 4
\f(CWopen FOO || die\fR is now incorrect.  You need parens around the filehandle.
While temporarily supported, using such a construct will 
generate a non-fatal (but non-suppressible) warning.
.Ip "\(bu" 4
The elements of argument lists for formats are now evaluated in list
context.  This means you can interpolate list values now.
.Ip "\(bu" 4
You can't do a \f(CWgoto\fR into a block that is optimized away.  Darn.
.Ip "\(bu" 4
It is no longer syntactically legal to use whitespace as the name
of a variable, or as a delimiter for any kind of quote construct.
Double darn.
.Ip "\(bu" 4
The \fIcaller()\fR function now returns a false value in a scalar context if there
is no caller.  This lets library files determine if they're being required.
.Ip "\(bu" 4
\f(CWm//g\fR now attaches its state to the searched string rather than the
regular expression.
.Ip "\(bu" 4
\f(CWreverse\fR is no longer allowed as the name of a sort subroutine.
.Ip "\(bu" 4
\fBtaintperl\fR is no longer a separate executable.  There is now a \fB\-T\fR
switch to turn on tainting when it isn't turned on automatically.
.Ip "\(bu" 4
Double-quoted strings may no longer end with an unescaped \f(CW$\fR or \f(CW@\fR.
.Ip "\(bu" 4
The archaic \f(CWwhile/if\fR \s-1BLOCK\s0 \s-1BLOCK\s0 syntax is no longer supported.
.Ip "\(bu" 4
Negative array subscripts now count from the end of the array.
.Ip "\(bu" 4
The comma operator in a scalar context is now guaranteed to give a
scalar context to its arguments.
.Ip "\(bu" 4
The \f(CW**\fR operator now binds more tightly than unary minus.  
It was documented to work this way before, but didn't.
.Ip "\(bu" 4
Setting \f(CW$#array\fR lower now discards array elements.
.Ip "\(bu" 4
\fIdelete()\fR is not guaranteed to return the old value for \fItie()\fRd arrays,
since this capability may be onerous for some modules to implement.
.Ip "\(bu" 4
The construct \*(L"this is $$x\*(R" used to interpolate the pid at that
point, but now tries to dereference \f(CW$x\fR.  \f(CW$$\fR by itself still
works fine, however.
.Ip "\(bu" 4
Some error messages will be different.
.Ip "\(bu" 4
Some bugs may have been inadvertently removed.

.rn }` ''

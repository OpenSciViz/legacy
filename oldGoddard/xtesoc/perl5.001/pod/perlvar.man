.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLVAR 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlvar \- Perl predefined variables
.SH "DESCRIPTION"
.Sh "Predefined Names"
The following names have special meaning to Perl.  Most of the
punctuational names have reasonable mnemonics, or analogues in one of
the shells.  Nevertheless, if you wish to use the long variable names,
you just need to say
.PP
.Vb 1
\&    use English;
.Ve
at the top of your program.  This will alias all the short names to the
long names in the current package.  Some of them even have medium names,
generally borrowed from \fBawk\fR.
.PP
To go a step further, those variables that depend on the currently
selected filehandle may instead be set by calling an object method on
the FileHandle object.  (Summary lines below for this contain the word
\s-1HANDLE\s0.)  First you must say
.PP
.Vb 1
\&    use FileHandle;
.Ve
after which you may use either
.PP
.Vb 1
\&    method HANDLE EXPR
.Ve
or
.PP
.Vb 1
\&    HANDLE->method(EXPR)
.Ve
Each of the methods returns the old value of the FileHandle attribute.
The methods each take an optional \s-1EXPR\s0, which if supplied specifies the
new value for the FileHandle attribute in question.  If not supplied,
most of the methods do nothing to the current value, except for
\fIautoflush()\fR, which will assume a 1 for you, just to be different.
.PP
A few of these variables are considered \*(L"read-only\*(R".  This means that if
you try to assign to this variable, either directly or indirectly through
a reference, you'll raise a run-time exception.
.Ip "$\s-1ARG\s0" 8
.Ip "$_" 8
The default input and pattern-searching space.  The following pairs are
equivalent:
.Sp
.Vb 2
\&    while (<>) {...}    # only equivalent in while!
\&    while ($_ = <>) {...}
.Ve
.Vb 2
\&    /^Subject:/
\&    $_ =~ /^Subject:/
.Ve
.Vb 2
\&    tr/a-z/A-Z/
\&    $_ =~ tr/a-z/A-Z/
.Ve
.Vb 2
\&    chop
\&    chop($_)
.Ve
(Mnemonic: underline is understood in certain operations.)
.Ip "$<\fIdigit\fR>" 8
Contains the subpattern from the corresponding set of parentheses in
the last pattern matched, not counting patterns matched in nested
blocks that have been exited already.  (Mnemonic: like \edigit.)
These variables are all read-only.
.Ip "$\s-1MATCH\s0" 8
.Ip "$&" 8
The string matched by the last successful pattern match (not counting
any matches hidden within a \s-1BLOCK\s0 or \fIeval()\fR enclosed by the current
\s-1BLOCK\s0).  (Mnemonic: like & in some editors.)  This variable is read-only.
.Ip "$\s-1PREMATCH\s0" 8
.Ip "$`" 8
The string preceding whatever was matched by the last successful
pattern match (not counting any matches hidden within a \s-1BLOCK\s0 or eval
enclosed by the current \s-1BLOCK\s0).  (Mnemonic: ` often precedes a quoted
string.)  This variable is read-only.
.Ip "$\s-1POSTMATCH\s0" 8
.Ip "$\*(R'" 8
The string following whatever was matched by the last successful
pattern match (not counting any matches hidden within a \s-1BLOCK\s0 or \fIeval()\fR
enclosed by the current \s-1BLOCK\s0).  (Mnemonic: \*(L' often follows a quoted
string.)  Example:
.Sp
.Vb 3
\&    $_ = 'abcdefghi';
\&    /def/;
\&    print "$`:$&:$'\en";         # prints abc:def:ghi
.Ve
This variable is read-only.
.Ip "$\s-1LAST_PAREN_MATCH\s0" 8
.Ip "$+" 8
The last bracket matched by the last search pattern.  This is useful if
you don't know which of a set of alternative patterns matched.  For
example:
.Sp
.Vb 1
\&    /Version: (.*)|Revision: (.*)/ && ($rev = $+);
.Ve
(Mnemonic: be positive and forward looking.)
This variable is read-only.
.Ip "$\s-1MULTILINE_MATCHING\s0" 8
.Ip "$*" 8
Set to 1 to do multiline matching within a string, 0 to tell Perl
that it can assume that strings contain a single line, for the purpose
of optimizing pattern matches.  Pattern matches on strings containing
multiple newlines can produce confusing results when \*(L"\f(CW$*\fR\*(R" is 0.  Default
is 0.  (Mnemonic: * matches multiple things.)  Note that this variable
only influences the interpretation of \*(L"\f(CW^\fR\*(R" and \*(L"\f(CW$\fR\*(R".  A literal newline can
be searched for even when \f(CW$* == 0\fR.
.Sp
Use of \*(L"\f(CW$*\fR\*(R" is deprecated in Perl 5.
.Ip "input_line_number \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1INPUT_LINE_NUMBER\s0" 8
.Ip "$\s-1NR\s0" 8
.Ip "$." 8
The current input line number of the last filehandle that was read.
This variable should be considered read-only.  
Remember that only an explicit close on the filehandle
resets the line number.  Since \*(L"\f(CW<>\fR\*(R" never does an explicit close, line
numbers increase across \s-1ARGV\s0 files (but see examples under \fIeof()\fR).
(Mnemonic: many programs use \*(L".\*(R" to mean the current line number.)
.Ip "input_record_separator \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1INPUT_RECORD_SEPARATOR\s0" 8
.Ip "$\s-1RS\s0" 8
.Ip "$/" 8
The input record separator, newline by default.  Works like \fBawk\fR's \s-1RS\s0
variable, including treating blank lines as delimiters if set to the
null string.  You may set it to a multicharacter string to match a
multi-character delimiter.  Note that setting it to \f(CW"\en\en"\fR means
something slightly different than setting it to \f(CW""\fR, if the file
contains consecutive blank lines.  Setting it to \f(CW""\fR will treat two or
more consecutive blank lines as a single blank line.  Setting it to
\f(CW"\en\en"\fR will blindly assume that the next input character belongs to the
next paragraph, even if it's a newline.  (Mnemonic: / is used to
delimit line boundaries when quoting poetry.)
.Sp
.Vb 3
\&    undef $/;
\&    $_ = <FH>;          # whole file now here
\&    s/\en[ \et]+/ /g;
.Ve
.Ip "autoflush \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1OUTPUT_AUTOFLUSH\s0" 8
.Ip "$|" 8
If set to nonzero, forces a flush after every write or print on the
currently selected output channel.  Default is 0.  Note that \s-1STDOUT\s0
will typically be line buffered if output is to the terminal and block
buffered otherwise.  Setting this variable is useful primarily when you
are outputting to a pipe, such as when you are running a Perl script
under rsh and want to see the output as it's happening.  (Mnemonic:
when you want your pipes to be piping hot.)
.Ip "output_field_separator \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1OUTPUT_FIELD_SEPARATOR\s0" 8
.Ip "$\s-1OFS\s0" 8
.Ip "$," 8
The output field separator for the print operator.  Ordinarily the
print operator simply prints out the comma separated fields you
specify.  In order to get behavior more like \fBawk\fR, set this variable
as you would set \fBawk\fR's \s-1OFS\s0 variable to specify what is printed
between fields.  (Mnemonic: what is printed when there is a , in your
print statement.)
.Ip "output_record_separator \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1OUTPUT_RECORD_SEPARATOR\s0" 8
.Ip "$\s-1ORS\s0" 8
.Ip "$\e" 8
The output record separator for the print operator.  Ordinarily the
print operator simply prints out the comma separated fields you
specify, with no trailing newline or record separator assumed.  In
order to get behavior more like \fBawk\fR, set this variable as you would
set \fBawk\fR's \s-1ORS\s0 variable to specify what is printed at the end of the
print.  (Mnemonic: you set \*(L"\f(CW$\e\fR\*(R" instead of adding \en at the end of the
print.  Also, it's just like /, but it's what you get \*(L"back\*(R" from
Perl.)
.Ip "$\s-1LIST_SEPARATOR\s0" 8
.Ip "$\*(R"" 8
This is like \*(L"\f(CW$,\fR\*(R" except that it applies to array values interpolated
into a double-quoted string (or similar interpreted string).  Default
is a space.  (Mnemonic: obvious, I think.)
.Ip "$\s-1SUBSCRIPT_SEPARATOR\s0" 8
.Ip "$\s-1SUBSEP\s0" 8
.Ip "$;" 8
The subscript separator for multi-dimensional array emulation.  If you
refer to a hash element as
.Sp
.Vb 1
\&    $foo{$a,$b,$c}
.Ve
it really means
.Sp
.Vb 1
\&    $foo{join($;, $a, $b, $c)}
.Ve
But don't put
.Sp
.Vb 1
\&    @foo{$a,$b,$c}      # a slice--note the @
.Ve
which means
.Sp
.Vb 1
\&    ($foo{$a},$foo{$b},$foo{$c})
.Ve
Default is \*(L"\e034\*(R", the same as \s-1SUBSEP\s0 in \fBawk\fR.  Note that if your
keys contain binary data there might not be any safe value for \*(L"\f(CW$;\fR\*(R".
(Mnemonic: comma (the syntactic subscript separator) is a
semi-semicolon.  Yeah, I know, it's pretty lame, but \*(L"\f(CW$,\fR\*(R" is already
taken for something more important.)
.Sp
Consider using \*(L"real\*(R" multi-dimensional arrays in Perl 5.
.Ip "$\s-1OFMT\s0" 8
.Ip "$#" 8
The output format for printed numbers.  This variable is a half-hearted
attempt to emulate \fBawk\fR's \s-1OFMT\s0 variable.  There are times, however,
when \fBawk\fR and Perl have differing notions of what is in fact
numeric.  Also, the initial value is %.20g rather than %.6g, so you
need to set \*(L"\f(CW$#\fR\*(R" explicitly to get \fBawk\fR's value.  (Mnemonic: # is the
number sign.)
.Sp
Use of \*(L"\f(CW$#\fR\*(R" is deprecated in Perl 5.
.Ip "format_page_number \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_PAGE_NUMBER\s0" 8
.Ip "$%" 8
The current page number of the currently selected output channel.
(Mnemonic: % is page number in \fBnroff\fR.)
.Ip "format_lines_per_page \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_LINES_PER_PAGE\s0" 8
.Ip "$=" 8
The current page length (printable lines) of the currently selected
output channel.  Default is 60.  (Mnemonic: = has horizontal lines.)
.Ip "format_lines_left \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_LINES_LEFT\s0" 8
.Ip "$-" 8
The number of lines left on the page of the currently selected output
channel.  (Mnemonic: lines_on_page \- lines_printed.)
.Ip "format_name \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_NAME\s0" 8
.Ip "$~" 8
The name of the current report format for the currently selected output
channel.  Default is name of the filehandle.  (Mnemonic: brother to
\*(L"\f(CW$^\fR\*(R".)
.Ip "format_top_name \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_TOP_NAME\s0" 8
.Ip "$^" 8
The name of the current top-of-page format for the currently selected
output channel.  Default is name of the filehandle with _TOP
appended.  (Mnemonic: points to top of page.)
.Ip "format_line_break_characters \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_LINE_BREAK_CHARACTERS\s0" 8
.Ip "$:" 8
The current set of characters after which a string may be broken to
fill continuation fields (starting with ^) in a format.  Default is 
\*(L"\ \en-\*(R", to break on whitespace or hyphens.  (Mnemonic: a \*(L"colon\*(R" in
poetry is a part of a line.)
.Ip "format_formfeed \s-1HANDLE\s0 \s-1EXPR\s0" 8
.Ip "$\s-1FORMAT_FORMFEED\s0" 8
.Ip "$^L" 8
What formats output to perform a formfeed.  Default is \ef.
.Ip "$\s-1ACCUMULATOR\s0" 8
.Ip "$^A" 8
The current value of the \fIwrite()\fR accumulator for \fIformat()\fR lines.  A format
contains \fIformline()\fR commands that put their result into \f(CW$^A\fR.  After
calling its format, \fIwrite()\fR prints out the contents of \f(CW$^A\fR and empties.
So you never actually see the contents of \f(CW$^A\fR unless you call
\fIformline()\fR yourself and then look at it.  See the \fIperlform\fR manpage and
the \f(CWformline()\fR entry in the \fIperlfunc\fR manpage.
.Ip "$\s-1CHILD_ERROR\s0" 8
.Ip "$?" 8
The status returned by the last pipe close, backtick (\f(CW``\fR) command,
or \fIsystem()\fR operator.  Note that this is the status word returned by
the \fIwait()\fR system call, so the exit value of the subprocess is actually
(\f(CW$? >> 8\fR).  Thus on many systems, \f(CW$? & 255\fR gives which signal,
if any, the process died from, and whether there was a core dump.
(Mnemonic: similar to \fBsh\fR and \fBksh\fR.)
.Ip "$\s-1OS_ERROR\s0" 8
.Ip "$\s-1ERRNO\s0" 8
.Ip "$!" 8
If used in a numeric context, yields the current value of errno, with
all the usual caveats.  (This means that you shouldn't depend on the
value of \*(L"\f(CW$!\fR\*(R" to be anything in particular unless you've gotten a
specific error return indicating a system error.)  If used in a string
context, yields the corresponding system error string.  You can assign
to \*(L"\f(CW$!\fR\*(R" in order to set \fIerrno\fR if, for instance, you want \*(L"\f(CW$!\fR\*(R" to return the
string for error \fIn\fR, or you want to set the exit value for the \fIdie()\fR
operator.  (Mnemonic: What just went bang?)
.Ip "$\s-1EVAL_ERROR\s0" 8
.Ip "$@" 8
The Perl syntax error message from the last \fIeval()\fR command.  If null, the
last \fIeval()\fR parsed and executed correctly (although the operations you
invoked may have failed in the normal fashion).  (Mnemonic: Where was
the syntax error \*(L"at\*(R"?)
.Sp
Note that warning messages are not collected in this variable.  You can,
however, set up a routine to process warnings by setting \f(CW$SIG\fR{_\|_WARN_\|_} below.
.Ip "$\s-1PROCESS_ID\s0" 8
.Ip "$\s-1PID\s0" 8
.Ip "$$" 8
The process number of the Perl running this script.  (Mnemonic: same
as shells.)
.Ip "$\s-1REAL_USER_ID\s0" 8
.Ip "$\s-1UID\s0" 8
.Ip "$<" 8
The real uid of this process.  (Mnemonic: it's the uid you came \fI\s-1FROM\s0\fR,
if you're running setuid.)
.Ip "$\s-1EFFECTIVE_USER_ID\s0" 8
.Ip "$\s-1EUID\s0" 8
.Ip "$>" 8
The effective uid of this process.  Example:
.Sp
.Vb 2
\&    $< = $>;            # set real to effective uid
\&    ($<,$>) = ($>,$<);  # swap real and effective uid
.Ve
(Mnemonic: it's the uid you went \fI\s-1TO\s0\fR, if you're running setuid.)  Note:
\*(L"\f(CW$<\fR\*(R" and \*(L"\f(CW$>\fR\*(R" can only be swapped on machines supporting \fIsetreuid()\fR.
.Ip "$\s-1REAL_GROUP_ID\s0" 8
.Ip "$\s-1GID\s0" 8
.Ip "$(" 8
The real gid of this process.  If you are on a machine that supports
membership in multiple groups simultaneously, gives a space separated
list of groups you are in.  The first number is the one returned by
\fIgetgid()\fR, and the subsequent ones by \fIgetgroups()\fR, one of which may be
the same as the first number.  (Mnemonic: parentheses are used to \fI\s-1GROUP\s0\fR
things.  The real gid is the group you \fI\s-1LEFT\s0\fR, if you're running setgid.)
.Ip "$\s-1EFFECTIVE_GROUP_ID\s0" 8
.Ip "$\s-1EGID\s0" 8
.Ip "$)" 8
The effective gid of this process.  If you are on a machine that
supports membership in multiple groups simultaneously, gives a space
separated list of groups you are in.  The first number is the one
returned by \fIgetegid()\fR, and the subsequent ones by \fIgetgroups()\fR, one of
which may be the same as the first number.  (Mnemonic: parentheses are
used to \fI\s-1GROUP\s0\fR things.  The effective gid is the group that's \fI\s-1RIGHT\s0\fR for
you, if you're running setgid.)
.Sp
Note: \*(L"\f(CW$<\fR\*(R", \*(L"\f(CW$>\fR\*(R", \*(L"\f(CW$(\fR\*(R" and \*(L"\f(CW$)\fR\*(R" can only be set on machines
that support the corresponding \fIset[re][ug]id()\fR routine.  \*(L"\f(CW$(\fR\*(R" and \*(L"\f(CW$)\fR\*(R" 
can only be swapped on machines supporting \fIsetregid()\fR.
.Ip "$\s-1PROGRAM_NAME\s0" 8
.Ip "$0" 8
Contains the name of the file containing the Perl script being
executed.  Assigning to \*(L"\f(CW$0\fR\*(R" modifies the argument area that the \fIps\fR\|(1)
program sees.  This is more useful as a way of indicating the
current program state than it is for hiding the program you're running.
(Mnemonic: same as \fBsh\fR and \fBksh\fR.)
.Ip "$[" 8
The index of the first element in an array, and of the first character
in a substring.  Default is 0, but you could set it to 1 to make
Perl behave more like \fBawk\fR (or Fortran) when subscripting and when
evaluating the \fIindex()\fR and \fIsubstr()\fR functions.  (Mnemonic: [ begins
subscripts.)
.Sp
As of Perl 5, assignment to \*(L"\f(CW$[\fR\*(R" is treated as a compiler directive,
and cannot influence the behavior of any other file.  Its use is
discouraged.
.Ip "$\s-1PERL_VERSION\s0" 8
.Ip "$]" 8
The string printed out when you say \f(CWperl -v\fR.  It can be used to
determine at the beginning of a script whether the perl interpreter
executing the script is in the right range of versions.  If used in a
numeric context, returns the version + patchlevel / 1000.  Example:
.Sp
.Vb 5
\&    # see if getc is available
\&    ($version,$patchlevel) =
\&             $] =~ /(\ed+\e.\ed+).*\enPatch level: (\ed+)/;
\&    print STDERR "(No filename completion available.)\en"
\&             if $version * 1000 + $patchlevel < 2016;
.Ve
or, used numerically,
.Sp
.Vb 1
\&    warn "No checksumming!\en" if $] < 3.019;
.Ve
(Mnemonic: Is this version of perl in the right bracket?)
.Ip "$\s-1DEBUGGING\s0" 8
.Ip "$^D" 8
The current value of the debugging flags.  (Mnemonic: value of \fB\-D\fR
switch.)
.Ip "$\s-1SYSTEM_FD_MAX\s0" 8
.Ip "$^F" 8
The maximum system file descriptor, ordinarily 2.  System file
descriptors are passed to \fIexec()\fRed processes, while higher file
descriptors are not.  Also, during an \fIopen()\fR, system file descriptors are
preserved even if the \fIopen()\fR fails.  (Ordinary file descriptors are
closed before the \fIopen()\fR is attempted.)  Note that the close-on-exec
status of a file descriptor will be decided according to the value of
\f(CW$^F\fR at the time of the open, not the time of the exec.
.Ip "$\s-1INPLACE_EDIT\s0" 8
.Ip "$^I" 8
The current value of the inplace-edit extension.  Use \f(CWundef\fR to disable
inplace editing.  (Mnemonic: value of \fB\-i\fR switch.)
.Ip "$\s-1PERLDB\s0" 8
.Ip "$^P" 8
The internal flag that the debugger clears so that it doesn't debug
itself.  You could conceivable disable debugging yourself by clearing
it.
.Ip "$\s-1BASETIME\s0" 8
.Ip "$^T" 8
The time at which the script began running, in seconds since the
epoch (beginning of 1970).  The values returned by the \fB\-M\fR, \fB\-A\fR 
and \fB\-C\fR filetests are
based on this value.
.Ip "$\s-1WARNING\s0" 8
.Ip "$^W" 8
The current value of the warning switch, either \s-1TRUE\s0 or \s-1FALSE\s0.  (Mnemonic: related to the
\fB\-w\fR switch.)
.Ip "$\s-1EXECUTABLE_NAME\s0" 8
.Ip "$^X" 8
The name that the Perl binary itself was executed as, from C's \f(CWargv[0]\fR.
.Ip "$\s-1ARGV\s0" 8
contains the name of the current file when reading from <>.
.Ip "@\s-1ARGV\s0" 8
The array \f(CW@ARGV\fR contains the command line arguments intended for the
script.  Note that \f(CW$#ARGV\fR is the generally number of arguments minus
one, since \f(CW$ARGV[0]\fR is the first argument, \fI\s-1NOT\s0\fR the command name.  See
\*(L"\f(CW$0\fR\*(R" for the command name.
.Ip "@\s-1INC\s0" 8
The array \f(CW@INC\fR contains the list of places to look for Perl scripts to
be evaluated by the \f(CWdo EXPR\fR, \f(CWrequire\fR, or \f(CWuse\fR constructs.  It
initially consists of the arguments to any \fB\-I\fR command line switches,
followed by the default Perl library, probably \*(L"/usr/local/lib/perl\*(R",
followed by \*(L".\*(R", to represent the current directory.
.Ip "%\s-1INC\s0" 8
The hash \f(CW%INC\fR contains entries for each filename that has
been included via \f(CWdo\fR or \f(CWrequire\fR.  The key is the filename you
specified, and the value is the location of the file actually found.
The \f(CWrequire\fR command uses this array to determine whether a given file
has already been included.
.Ip "$\s-1ENV\s0{expr}" 8
The hash \f(CW%ENV\fR contains your current environment.  Setting a
value in \f(CWENV\fR changes the environment for child processes.
.Ip "$\s-1SIG\s0{expr}" 8
The hash \f(CW%SIG\fR is used to set signal handlers for various
signals.  Example:
.Sp
.Vb 6
\&    sub handler {       # 1st argument is signal name
\&        local($sig) = @_;
\&        print "Caught a SIG$sig--shutting down\en";
\&        close(LOG);
\&        exit(0);
\&    }
.Ve
.Vb 5
\&    $SIG{'INT'} = 'handler';
\&    $SIG{'QUIT'} = 'handler';
\&    ...
\&    $SIG{'INT'} = 'DEFAULT';    # restore default action
\&    $SIG{'QUIT'} = 'IGNORE';    # ignore SIGQUIT
.Ve
The \f(CW%SIG\fR array only contains values for the signals actually set within
the Perl script.  Here are some other examples:
.Sp
.Vb 4
\&    $SIG{PIPE} = Plumber;       # SCARY!!
\&    $SIG{"PIPE"} = "Plumber";   # just fine, assumes main::Plumber
\&    $SIG{"PIPE"} = \e&Plumber;   # just fine; assume current Plumber
\&    $SIG{"PIPE"} = Plumber();   # oops, what did Plumber() return??
.Ve
The one marked scary is problematic because it's a bareword, which means
sometimes it's a string representing the function, and sometimes it's 
going to call the subroutine call right then and there!  Best to be sure
and quote it or take a reference to it.  *Plumber works too.  See the \fIperlsubs\fR manpage.
.Sp
Certain internal hooks can be also set using the \f(CW%SIG\fR hash.  The
routine indicated by \f(CW$SIG\fR{_\|_WARN_\|_} is called when a warning message is
about to be printed.  The warning message is passed as the first
argument.  The presence of a _\|_WARN_\|_ hook causes the ordinary printing
of warnings to \s-1STDERR\s0 to be suppressed.  You can use this to save warnings
in a variable, or turn warnings into fatal errors, like this:
.Sp
.Vb 2
\&    local $SIG{__WARN__} = sub { die $_[0] };
\&    eval $proggie;
.Ve
The routine indicated by \f(CW$SIG\fR{_\|_DIE_\|_} is called when a fatal exception
is about to be thrown.  The error message is passed as the first
argument.  When a _\|_DIE_\|_ hook routine returns, the exception
processing continues as it would have in the absence of the hook,
unless the hook routine itself exits via a goto, a loop exit, or a die.

.rn }` ''

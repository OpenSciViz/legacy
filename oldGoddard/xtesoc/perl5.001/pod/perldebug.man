.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLDEBUG 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perldebug \- Perl debugging
.SH "DESCRIPTION"
First of all, have you tried using the \fB\-w\fR switch?
.Sh "Debugging"
If you invoke Perl with a \fB\-d\fR switch, your script will be run under the
debugger.  However, the Perl debugger is not a separate program as it is
in a C environment.  Instead, the \fB\-d\fR flag tells the compiler to insert
source information into the pseudocode it's about to hand to the
interpreter.  (That means your code must compile correctly for the
debugger to work on it.)  Then when the interpreter starts up, it
pre-loads a Perl library file containing the debugger itself.  The program
will halt before the first executable statement (but see below) and ask
you for one of the following commands:
.Ip "h" 12
Prints out a help message.
.Ip "T" 12
Stack trace.
If you do bizarre things to your \f(CW@_\fR arguments in a subroutine, the stack
backtrace will not always show the original values.
.Ip "s" 12
Single step.  Executes until it reaches the beginning of another
statement.
.Ip "n" 12
Next.  Executes over subroutine calls, until it reaches the beginning
of the next statement.
.Ip "f" 12
Finish.  Executes statements until it has finished the current
subroutine.
.Ip "c" 12
Continue.  Executes until the next breakpoint is reached.
.Ip "c line" 12
Continue to the specified line.  Inserts a one-time-only breakpoint at
the specified line.
.Ip "<\s-1CR\s0>" 12
Repeat last n or s.
.Ip "l min+incr" 12
List incr+1 lines starting at min.  If min is omitted, starts where
last listing left off.  If incr is omitted, previous value of incr is
used.
.Ip "l min-max" 12
List lines in the indicated range.
.Ip "l line" 12
List just the indicated line.
.Ip "l" 12
List next window.
.Ip "-" 12
List previous window.
.Ip "w line" 12
List window (a few lines worth of code) around line.
.Ip "l subname" 12
List subroutine.  If it's a long subroutine it just lists the
beginning.  Use \*(L"l\*(R" to list more.
.Ip "/pattern/" 12
Regular expression search forward in the source code for pattern; the
final / is optional.
.Ip "?pattern?" 12
Regular expression search backward in the source code for pattern; the
final ? is optional.
.Ip "L" 12
List lines that have breakpoints or actions.
.Ip "S" 12
Lists the names of all subroutines.
.Ip "t" 12
Toggle trace mode on or off.
.Ip "b line [ condition ]" 12
Set a breakpoint.  If line is omitted, sets a breakpoint on the line
that is about to be executed.  If a condition is specified, it is
evaluated each time the statement is reached and a breakpoint is taken
only if the condition is true.  Breakpoints may only be set on lines
that begin an executable statement.  Conditions don't use \f(CWif\fR:
.Sp
.Vb 2
\&    b 237 $x > 30
\&    b 33 /pattern/i
.Ve
.Ip "b subname [ condition ]" 12
Set breakpoint at first executable line of subroutine.
.Ip "d line" 12
Delete breakpoint.  If line is omitted, deletes the breakpoint on the
line that is about to be executed.
.Ip "D" 12
Delete all breakpoints.
.Ip "a line command" 12
Set an action for line.  A multiline command may be entered by
backslashing the newlines.  This command is Perl code, not another
debugger command.
.Ip "A" 12
Delete all line actions.
.Ip "< command" 12
Set an action to happen before every debugger prompt.  A multiline
command may be entered by backslashing the newlines.
.Ip "> command" 12
Set an action to happen after the prompt when you've just given a
command to return to executing the script.  A multiline command may be
entered by backslashing the newlines.
.Ip "V package [symbols]" 12
Display all (or some) variables in package (defaulting to the \f(CWmain\fR
package) using a data pretty-printer (hashes show their keys and values so
you see what's what, control characters are made printable, etc.).  Make
sure you don't put the type specifier (like $) there, just the symbol
names, like this:
.Sp
.Vb 1
\&    V DB filename line 
.Ve
.Ip "X [symbols] " 12
Same as as \*(L"V\*(R" command, but within the current package.  
.Ip "! number" 12
Redo a debugging command.  If number is omitted, redoes the previous
command.
.Ip "! \-number" 12
Redo the command that was that many commands ago.
.Ip "H \-number" 12
Display last n commands.  Only commands longer than one character are
listed.  If number is omitted, lists them all.
.Ip "q or ^D" 12
Quit.  ("quit\*(R" doesn't work for this.)
.Ip "command" 12
Execute command as a Perl statement.  A missing semicolon will be
supplied.
.Ip "p expr" 12
Same as \f(CWprint DB::OUT expr\fR.  The \s-1DB::OUT\s0 filehandle is opened to
/dev/tty, regardless of where \s-1STDOUT\s0 may be redirected to.
.PP
Any command you type in that isn't recognized by the debugger will be
directly executed (\f(CWeval\fR'd) as Perl code.  Leading white space will
cause the debugger to think it's \f(CWNOT\fR a debugger command.
.PP
If you have any compile-time executable statements (code within a \s-1BEGIN\s0 
block or a \f(CWuse\fR statement), these will \fI\s-1NOT\s0\fR be stopped by debugger,
although \f(CWrequire\fRs will.  From your own code, however, you can transfer
control back to the debugger using the following statement, which is harmless
if the debugger is not running:
.PP
.Vb 1
\&    $DB::single = 1;
.Ve
.Sh "Customization"
If you want to modify the debugger, copy \fIperl5db.pl\fR from the Perl
library to another name and modify it as necessary.  You'll also want
to set environment variable \s-1PERL5DB\s0 to say something like this:
.PP
.Vb 1
\&    BEGIN { require "myperl5db.pl" }
.Ve
You can do some customization by setting up a \fI.perldb\fR file which
contains initialization code.  For instance, you could make aliases
like these (the last one in particular most people seem to expect to 
be there):
.PP
.Vb 5
\&    $DB::alias{'len'} = 's/^len(.*)/p length($1)/';
\&    $DB::alias{'stop'} = 's/^stop (at|in)/b/';
\&    $DB::alias{'.'} = 's/^\e./p '
\&                    . '"\e$DB::sub(\e$DB::filename:\e$DB::line):\et"'
\&                    . ',\e$DB::dbline[\e$DB::line]/' ;
.Ve
.Sh "Other resources"
You did try the \fB\-w\fR switch, didn't you?
.SH "BUGS"
If your program \fIexit()\fRs or \fIdie()\fRs, so does the debugger.
.PP
There's no builtin way to restart the debugger without exiting and coming back
into it.  You could use an alias like this:
.PP
.Vb 1
\&    $DB::alias{'rerun'} = 'exec "perl -d $DB::filename"';
.Ve
But you'd lose any pending breakpoint information, and that might not
be the right path, etc.

.rn }` ''

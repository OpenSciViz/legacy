.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLBOT 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlbot \- Bag'o Object Tricks For Perl5 (the BOT)
.SH "INTRODUCTION"
The following collection of tricks and hints is intended to whet curious
appetites about such things as the use of instance variables and the
mechanics of object and class relationships.  The reader is encouraged to
consult relevant textbooks for discussion of Object Oriented definitions and
methodology.  This is not intended as a comprehensive guide to Perl5's
object oriented features, nor should it be construed as a style guide.
.PP
The Perl motto still holds:  There's more than one way to do it.
.SH "INSTANCE VARIABLES"
An anonymous array or anonymous hash can be used to hold instance
variables.  Named parameters are also demonstrated.
.PP
.Vb 1
\&        package Foo;
.Ve
.Vb 8
\&        sub new {
\&                my $type = shift;
\&                my %params = @_;
\&                my $self = {};
\&                $self->{'High'} = $params{'High'};
\&                $self->{'Low'}  = $params{'Low'};
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package Bar;
.Ve
.Vb 8
\&        sub new {
\&                my $type = shift;
\&                my %params = @_;
\&                my $self = [];
\&                $self->[0] = $params{'Left'};
\&                $self->[1] = $params{'Right'};
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 3
\&        $a = new Foo ( 'High' => 42, 'Low' => 11 );
\&        print "High=$a->{'High'}\en";
\&        print "Low=$a->{'Low'}\en";
.Ve
.Vb 3
\&        $b = new Bar ( 'Left' => 78, 'Right' => 40 );
\&        print "Left=$b->[0]\en";
\&        print "Right=$b->[1]\en";
.Ve
.SH "SCALAR INSTANCE VARIABLES"
An anonymous scalar can be used when only one instance variable is needed.
.PP
.Vb 1
\&        package Foo;
.Ve
.Vb 6
\&        sub new {
\&                my $type = shift;
\&                my $self;
\&                $self = shift;
\&                bless \e$self;
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 2
\&        $a = new Foo 42;
\&        print "a=$$a\en";
.Ve
.SH "INSTANCE VARIABLE INHERITANCE"
This example demonstrates how one might inherit instance variables from a
superclass for inclusion in the new class.  This requires calling the
superclass's constructor and adding one's own instance variables to the new
object.
.PP
.Vb 1
\&        package Bar;
.Ve
.Vb 5
\&        sub new {
\&                my $self = {};
\&                $self->{'buz'} = 42;
\&                bless $self;
\&        }
.Ve
.Vb 2
\&        package Foo;
\&        @ISA = qw( Bar );
.Ve
.Vb 5
\&        sub new {
\&                my $self = new Bar;
\&                $self->{'biz'} = 11;
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 3
\&        $a = new Foo;
\&        print "buz = ", $a->{'buz'}, "\en";
\&        print "biz = ", $a->{'biz'}, "\en";
.Ve
.SH "OBJECT RELATIONSHIPS"
The following demonstrates how one might implement \*(L"containing\*(R" and \*(L"using\*(R"
relationships between objects.
.PP
.Vb 1
\&        package Bar;
.Ve
.Vb 5
\&        sub new {
\&                my $self = {};
\&                $self->{'buz'} = 42;
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package Foo;
.Ve
.Vb 6
\&        sub new {
\&                my $self = {};
\&                $self->{'Bar'} = new Bar ();
\&                $self->{'biz'} = 11;
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 3
\&        $a = new Foo;
\&        print "buz = ", $a->{'Bar'}->{'buz'}, "\en";
\&        print "biz = ", $a->{'biz'}, "\en";
.Ve
.SH "OVERRIDING SUPERCLASS METHODS"
The following example demonstrates how one might override a superclass
method and then call the method after it has been overridden.  The
Foo::Inherit class allows the programmer to call an overridden superclass
method without actually knowing where that method is defined.
.PP
.Vb 2
\&        package Buz;
\&        sub goo { print "here's the goo\en" }
.Ve
.Vb 2
\&        package Bar; @ISA = qw( Buz );
\&        sub google { print "google here\en" }
.Ve
.Vb 2
\&        package Baz;
\&        sub mumble { print "mumbling\en" }
.Ve
.Vb 3
\&        package Foo;
\&        @ISA = qw( Bar Baz );
\&        @Foo::Inherit::ISA = @ISA;  # Access to overridden methods.
.Ve
.Vb 14
\&        sub new { bless [] }
\&        sub grr { print "grumble\en" }
\&        sub goo {
\&                my $self = shift;
\&                $self->Foo::Inherit::goo();
\&        }
\&        sub mumble {
\&                my $self = shift;
\&                $self->Foo::Inherit::mumble();
\&        }
\&        sub google {
\&                my $self = shift;
\&                $self->Foo::Inherit::google();
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 5
\&        $foo = new Foo;
\&        $foo->mumble;
\&        $foo->grr;
\&        $foo->goo;
\&        $foo->google;
.Ve
.SH "USING RELATIONSHIP WITH SDBM "
This example demonstrates an interface for the SDBM class.  This creates a
\*(L"using\*(R" relationship between the SDBM class and the new class Mydbm.
.PP
.Vb 2
\&        use SDBM_File;
\&        use POSIX;
.Ve
.Vb 1
\&        package Mydbm;
.Ve
.Vb 19
\&        sub TIEHASH {
\&            my $self = shift;
\&            my $ref  = SDBM_File->new(@_);
\&            bless {'dbm' => $ref};
\&        }
\&        sub FETCH {
\&            my $self = shift;
\&            my $ref  = $self->{'dbm'};
\&            $ref->FETCH(@_);
\&        }
\&        sub STORE {
\&            my $self = shift; 
\&            if (defined $_[0]){
\&                my $ref = $self->{'dbm'};
\&                $ref->STORE(@_);
\&            } else {
\&                die "Cannot STORE an undefined key in Mydbm\en";
\&            }
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 3
\&        tie %foo, Mydbm, "Sdbm", O_RDWR|O_CREAT, 0640;
\&        $foo{'bar'} = 123;
\&        print "foo-bar = $foo{'bar'}\en";
.Ve
.Vb 3
\&        tie %bar, Mydbm, "Sdbm2", O_RDWR|O_CREAT, 0640;
\&        $bar{'Cathy'} = 456;
\&        print "bar-Cathy = $bar{'Cathy'}\en";
.Ve
.SH "THINKING OF CODE REUSE"
One strength of Object-Oriented languages is the ease with which old code
can use new code.  The following examples will demonstrate first how one can
hinder code reuse and then how one can promote code reuse.
.PP
This first example illustrates a class which uses a fully-qualified method
call to access the \*(L"private\*(R" method \fIBAZ()\fR.  The second example will show
that it is impossible to override the \fIBAZ()\fR method.
.PP
.Vb 1
\&        package FOO;
.Ve
.Vb 5
\&        sub new { bless {} }
\&        sub bar {
\&                my $self = shift;
\&                $self->FOO::private::BAZ;
\&        }
.Ve
.Vb 1
\&        package FOO::private;
.Ve
.Vb 3
\&        sub BAZ {
\&                print "in BAZ\en";
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 2
\&        $a = FOO->new;
\&        $a->bar;
.Ve
Now we try to override the \fIBAZ()\fR method.  We would like \fIFOO::bar()\fR to call
\fIGOOP::BAZ()\fR, but this cannot happen since \fIFOO::bar()\fR explicitly calls
\fIFOO::private::BAZ()\fR.
.PP
.Vb 1
\&        package FOO;
.Ve
.Vb 5
\&        sub new { bless {} }
\&        sub bar {
\&                my $self = shift;
\&                $self->FOO::private::BAZ;
\&        }
.Ve
.Vb 1
\&        package FOO::private;
.Ve
.Vb 3
\&        sub BAZ {
\&                print "in BAZ\en";
\&        }
.Ve
.Vb 3
\&        package GOOP;
\&        @ISA = qw( FOO );
\&        sub new { bless {} }
.Ve
.Vb 3
\&        sub BAZ {
\&                print "in GOOP::BAZ\en";
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 2
\&        $a = GOOP->new;
\&        $a->bar;
.Ve
To create reusable code we must modify class FOO, flattening class
FOO::private.  The next example shows a reusable class FOO which allows the
method \fIGOOP::BAZ()\fR to be used in place of \fIFOO::BAZ()\fR.
.PP
.Vb 1
\&        package FOO;
.Ve
.Vb 5
\&        sub new { bless {} }
\&        sub bar {
\&                my $self = shift;
\&                $self->BAZ;
\&        }
.Ve
.Vb 3
\&        sub BAZ {
\&                print "in BAZ\en";
\&        }
.Ve
.Vb 2
\&        package GOOP;
\&        @ISA = qw( FOO );
.Ve
.Vb 4
\&        sub new { bless {} }
\&        sub BAZ {
\&                print "in GOOP::BAZ\en";
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 2
\&        $a = GOOP->new;
\&        $a->bar;
.Ve
.SH "CLASS CONTEXT AND THE OBJECT"
Use the object to solve package and class context problems.  Everything a
method needs should be available via the object or should be passed as a
parameter to the method.
.PP
A class will sometimes have static or global data to be used by the
methods.  A subclass may want to override that data and replace it with new
data.  When this happens the superclass may not know how to find the new
copy of the data.
.PP
This problem can be solved by using the object to define the context of the
method.  Let the method look in the object for a reference to the data.  The
alternative is to force the method to go hunting for the data ("Is it in my
class, or in a subclass?  Which subclass?"), and this can be inconvenient
and will lead to hackery.  It is better to just let the object tell the
method where that data is located.
.PP
.Vb 1
\&        package Bar;
.Ve
.Vb 1
\&        %fizzle = ( 'Password' => 'XYZZY' );
.Ve
.Vb 5
\&        sub new {
\&                my $self = {};
\&                $self->{'fizzle'} = \e%fizzle;
\&                bless $self;
\&        }
.Ve
.Vb 8
\&        sub enter {
\&                my $self = shift;
\&        
\&                # Don't try to guess if we should use %Bar::fizzle
\&                # or %Foo::fizzle.  The object already knows which
\&                # we should use, so just ask it.
\&                #
\&                my $fizzle = $self->{'fizzle'};
.Ve
.Vb 2
\&                print "The word is ", $fizzle->{'Password'}, "\en";
\&        }
.Ve
.Vb 2
\&        package Foo;
\&        @ISA = qw( Bar );
.Ve
.Vb 1
\&        %fizzle = ( 'Password' => 'Rumple' );
.Ve
.Vb 5
\&        sub new {
\&                my $self = Bar->new;
\&                $self->{'fizzle'} = \e%fizzle;
\&                bless $self;
\&        }
.Ve
.Vb 1
\&        package main;
.Ve
.Vb 4
\&        $a = Bar->new;
\&        $b = Foo->new;
\&        $a->enter;
\&        $b->enter;
.Ve

.rn }` ''

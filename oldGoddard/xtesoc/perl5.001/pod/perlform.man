.rn '' }`
''' $RCSfile$$Revision$$Date$
''' 
''' $Log$
''' 
.de Sh
.br
.if t .Sp
.ne 5
.PP
\fB\\$1\fR
.PP
..
.de Sp
.if t .sp .5v
.if n .sp
..
.de Ip
.br
.ie \\n(.$>=3 .ne \\$3
.el .ne 3
.IP "\\$1" \\$2
..
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
'''
'''
'''     Set up \*(-- to give an unbreakable dash;
'''     string Tr holds user defined translation string.
'''     Bell System Logo is used as a dummy character.
'''
.tr \(*W-|\(bv\*(Tr
.ie n \{\
.ds -- \(*W-
.if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\" diablo 12 pitch
.ds L" ""
.ds R" ""
.ds L' '
.ds R' '
'br\}
.el\{\
.ds -- \(em\|
.tr \*(Tr
.ds L" ``
.ds R" ''
.ds L' `
.ds R' '
.if t .ds PI \(*p
.if n .ds PI PI
'br\}
.TH PERLFORM 1 "\*(RP"
.UC
.if n .hy 0 
.if n .na
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.de CQ          \" put $1 in typewriter font
.ft CW
'if n "\c
'if t \\&\\$1\c
'if n \\&\\$1\c
'if n \&"
\\&\\$2 \\$3 \\$4 \\$5 \\$6 \\$7
'.ft R
..
.\" @(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2
.	\" AM - accent mark definitions
.bd S B 3
.	\" fudge factors for nroff and troff
.if n \{\
.	ds #H 0
.	ds #V .8m
.	ds #F .3m
.	ds #[ \f1
.	ds #] \fP
.\}
.if t \{\
.	ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.	ds #V .6m
.	ds #F 0
.	ds #[ \&
.	ds #] \&
.\}
.	\" simple accents for nroff and troff
.if n \{\
.	ds ' \&
.	ds ` \&
.	ds ^ \&
.	ds , \&
.	ds ~ ~
.	ds ? ?
.	ds ! !
.	ds / 
.	ds q 
.\}
.if t \{\
.	ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.	ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.	ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.	ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.	ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.	ds ? \s-2c\h'-\w'c'u*7/10'\u\h'\*(#H'\zi\d\s+2\h'\w'c'u*8/10'
.	ds ! \s-2\(or\s+2\h'-\w'\(or'u'\v'-.8m'.\v'.8m'
.	ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.	ds q o\h'-\w'o'u*8/10'\s-4\v'.4m'\z\(*i\v'-.4m'\s+4\h'\w'o'u*8/10'
.\}
.	\" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds v \\k:\h'-(\\n(.wu*9/10-\*(#H)'\v'-\*(#V'\*(#[\s-4v\s0\v'\*(#V'\h'|\\n:u'\*(#]
.ds _ \\k:\h'-(\\n(.wu*9/10-\*(#H+(\*(#F*2/3))'\v'-.4m'\z\(hy\v'.4m'\h'|\\n:u'
.ds . \\k:\h'-(\\n(.wu*8/10)'\v'\*(#V*4/10'\z.\v'-\*(#V*4/10'\h'|\\n:u'
.ds 3 \*(#[\v'.2m'\s-2\&3\s0\v'-.2m'\*(#]
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.ds oe o\h'-(\w'o'u*4/10)'e
.ds Oe O\h'-(\w'O'u*4/10)'E
.	\" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.	\" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.	ds : e
.	ds 8 ss
.	ds v \h'-1'\o'\(aa\(ga'
.	ds _ \h'-1'^
.	ds . \h'-1'.
.	ds 3 3
.	ds o a
.	ds d- d\h'-1'\(ga
.	ds D- D\h'-1'\(hy
.	ds th \o'bp'
.	ds Th \o'LP'
.	ds ae ae
.	ds Ae AE
.	ds oe oe
.	ds Oe OE
.\}
.rm #[ #] #H #V #F C
.SH "NAME"
perlform \- Perl formats
.SH "DESCRIPTION"
Perl has a mechanism to help you generate simple reports and charts.  To
facilitate this, Perl helps you lay out your output page in your code in a
fashion that's close to how it will look when it's printed.  It can keep
track of things like how many lines on a page, what page you're, when to
print page headers, etc.  Keywords are borrowed from FORTRAN:
\fIformat()\fR to declare and \fIwrite()\fR to execute; see their entries in
the \fIperlfunc\fR manpage.  Fortunately, the layout is much more legible, more like
BASIC's PRINT USING statement.  Think of it as a poor man's \fInroff\fR\|(1).
.PP
Formats, like packages and subroutines, are declared rather than executed,
so they may occur at any point in your program.  (Usually it's best to
keep them all together though.) They have their own namespace apart from
all the other \*(L"types\*(R" in Perl.  This means that if you have a function
named \*(L"Foo\*(R", it is not the same thing as having a format named \*(L"Foo\*(R".
However, the default name for the format associated with a given
filehandle is the same as the name of the filehandle.  Thus, the default
format for STDOUT is name \*(L"STDOUT\*(R", and the default format for filehandle
TEMP is name \*(L"TEMP\*(R".  They just look the same.  They aren't.
.PP
Output record formats are declared as follows:
.PP
.Vb 3
\&    format NAME =
\&    FORMLIST
\&    .
.Ve
If name is omitted, format \*(L"STDOUT\*(R" is defined.  FORMLIST consists of a
sequence of lines, each of which may be of one of three types:
.Ip "1." 4
A comment, indicated by putting a \*(L'#\*(R' in the first column.
.Ip "2." 4
A \*(L"picture\*(R" line giving the format for one output line.
.Ip "3." 4
An argument line supplying values to plug into the previous picture line.
.PP
Picture lines are printed exactly as they look, except for certain fields
that substitute values into the line.  Each field in a picture line starts
with either \*(L"@\*(R" (at) or \*(L"^\*(R" (caret).  These lines do not undergo any kind
of variable interpolation.  The at field (not to be confused with the array
marker @) is the normal kind of field; the other kind, caret fields, are used
to do rudimentary multi-line text block filling.  The length of the field
is supplied by padding out the field with multiple \*(L"<\*(R", \*(L">\*(R", or \*(L"|\*(R"
characters to specify, respectively, left justification, right
justification, or centering.  If the variable would exceed the width
specified, it is truncated.
.PP
As an alternate form of right justification, you may also use \*(L"#\*(R"
characters (with an optional \*(L".") to specify a numeric field.  This way
you can line up the decimal points.  If any value supplied for these
fields contains a newline, only the text up to the newline is printed.
Finally, the special field \*(L"@*\*(R" can be used for printing multi-line,
non-truncated values; it should appear by itself on a line.
.PP
The values are specified on the following line in the same order as
the picture fields.  The expressions providing the values should be
separated by commas.  The expressions are all evaluated in a list context
before the line is processed, so a single list expression could produce
multiple list elements.  The expressions may be spread out to more than
one line if enclosed in braces.  If so, the opening brace must be the first
token on the first line.
.PP
Picture fields that begin with ^ rather than @ are treated specially.
With a # field, the field is blanked out if the value is undefined.  For
other field types, the caret enables a kind of fill mode.  Instead of an
arbitrary expression, the value supplied must be a scalar variable name
that contains a text string.  Perl puts as much text as it can into the
field, and then chops off the front of the string so that the next time
the variable is referenced, more of the text can be printed.  (Yes, this
means that the variable itself is altered during execution of the \fIwrite()\fR
call, and is not returned.)  Normally you would use a sequence of fields
in a vertical stack to print out a block of text.  You might wish to end
the final field with the text \*(L"...\*(R", which will appear in the output if
the text was too long to appear in its entirety.  You can change which
characters are legal to break on by changing the variable \f(CW$:\fR (that's
\f(CW$FORMAT_LINE_BREAK_CHARACTERS\fR if you're using the English module) to a
list of the desired characters.
.PP
Using caret fields can produce variable length records.  If the text
to be formatted is short, you can suppress blank lines by putting a
\*(L"~\*(R" (tilde) character anywhere in the line.  The tilde will be translated
to a space upon output.  If you put a second tilde contiguous to the
first, the line will be repeated until all the fields on the line are
exhausted.  (If you use a field of the at variety, the expression you
supply had better not give the same value every time forever!)
.PP
Top-of-form processing is by default handled by a format with the 
same name as the current filehandle with \*(L"_TOP\*(R" concatenated to it.
It's triggered at the top of each page.  See <perlfunc/\fIwrite()\fR>.
.PP
Examples:
.PP
.Vb 10
\& # a report on the /etc/passwd file
\& format STDOUT_TOP =
\&                         Passwd File
\& Name                Login    Office   Uid   Gid Home
\& ------------------------------------------------------------------
\& .
\& format STDOUT =
\& @<<<<<<<<<<<<<<<<<< @||||||| @<<<<<<@>>>> @>>>> @<<<<<<<<<<<<<<<<<
\& $name,              $login,  $office,$uid,$gid, $home
\& .
.Ve
.Vb 29
\& # a report from a bug report form
\& format STDOUT_TOP =
\&                         Bug Reports
\& @<<<<<<<<<<<<<<<<<<<<<<<     @|||         @>>>>>>>>>>>>>>>>>>>>>>>
\& $system,                      $%,         $date
\& ------------------------------------------------------------------
\& .
\& format STDOUT =
\& Subject: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&          $subject
\& Index: @<<<<<<<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&        $index,                       $description
\& Priority: @<<<<<<<<<< Date: @<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&           $priority,        $date,   $description
\& From: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&       $from,                         $description
\& Assigned to: @<<<<<<<<<<<<<<<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&              $programmer,            $description
\& ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&                                      $description
\& ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&                                      $description
\& ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&                                      $description
\& ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\&                                      $description
\& ~                                    ^<<<<<<<<<<<<<<<<<<<<<<<...
\&                                      $description
\& .
.Ve
It is possible to intermix \fIprint()\fRs with \fIwrite()\fRs on the same output
channel, but you'll have to handle $\- ($\s-1FORMAT_LINES_LEFT\s0)
yourself.
.Sh "Format Variables"
The current format name is stored in the variable \f(CW$~\fR ($\s-1FORMAT_NAME\s0),
and the current top of form format name is in \f(CW$^\fR ($\s-1FORMAT_TOP_NAME\s0).
The current output page number is stored in \f(CW$%\fR ($\s-1FORMAT_PAGE_NUMBER\s0),
and the number of lines on the page is in \f(CW$=\fR ($\s-1FORMAT_LINES_PER_PAGE\s0).
Whether to autoflush output on this handle is stored in \f(CW$|\fR
($\s-1OUTPUT_AUTOFLUSH\s0).  The string output before each top of page (except
the first) is stored in \f(CW$^L\fR ($\s-1FORMAT_FORMFEED\s0).  These variables are
set on a per-filehandle basis, so you'll need to \fIselect()\fR into a different
one to affect them:
.PP
.Vb 4
\&    select((select(OUTF), 
\&            $~ = "My_Other_Format",
\&            $^ = "My_Top_Format"
\&           )[0]);
.Ve
Pretty ugly, eh?  It's a common idiom though, so don't be too surprised
when you see it.  You can at least use a temporary variable to hold
the previous filehandle: (this is a much better approach in general,
because not only does legibility improve, you now have intermediary
stage in the expression to single-step the debugger through):
.PP
.Vb 4
\&    $ofh = select(OUTF);
\&    $~ = "My_Other_Format";
\&    $^ = "My_Top_Format";
\&    select($ofh);
.Ve
If you use the English module, you can even read the variable names:
.PP
.Vb 5
\&    use English;
\&    $ofh = select(OUTF);
\&    $FORMAT_NAME     = "My_Other_Format";
\&    $FORMAT_TOP_NAME = "My_Top_Format";
\&    select($ofh);
.Ve
But you still have those funny \fIselect()\fRs.  So just use the FileHandle
module.  Now, you can access these special variables using lower-case
method names instead:
.PP
.Vb 3
\&    use FileHandle;
\&    format_name     OUTF "My_Other_Format";
\&    format_top_name OUTF "My_Top_Format";
.Ve
Much better!
.SH "NOTES"
Since the values line may contain arbitrary expressions (for at fields, 
not caret fields), you can farm out more sophisticated processing
to other functions, like \fIsprintf()\fR or one of your own.  For example:
.PP
.Vb 4
\&    format Ident = 
\&        @<<<<<<<<<<<<<<<
\&        &commify($n)
\&    .
.Ve
To get a real at or caret into the field, do this:
.PP
.Vb 4
\&    format Ident = 
\&    I have an @ here.
\&            "@"
\&    .
.Ve
To center a whole line of text, do something like this:
.PP
.Vb 4
\&    format Ident = 
\&    @|||||||||||||||||||||||||||||||||||||||||||||||
\&            "Some text line"
\&    .
.Ve
There is no builtin way to say \*(L"float this to the right hand side
of the page, however wide it is.\*(R"  You have to specify where it goes.
The truly desperate can generate their own format on the fly, based
on the current number of columns, and then \fIeval()\fR it:
.PP
.Vb 9
\&    $format  = "format STDOUT = \en";
\&             . '^' . '<' x $cols . "\en";
\&             . '$entry' . "\en";
\&             . "\et^" . "<" x ($cols-8) . "~~\en";
\&             . '$entry' . "\en";
\&             . ".\en";
\&    print $format if $Debugging;
\&    eval $format; 
\&    die $@ if $@;
.Ve
Which would generate a format looking something like this:
.PP
.Vb 6
\& format STDOUT = 
\& ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
\& $entry
\&         ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<~~
\& $entry
\& .
.Ve
Here's a little program that's somewhat like \fIfmt\fR\|(1):
.PP
.Vb 3
\& format = 
\& ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ~~
\& $_
.Ve
.Vb 1
\& .
.Ve
.Vb 5
\& $/ = '';
\& while (<>) {
\&     s/\es*\en\es*/ /g;
\&     write;
\& } 
.Ve
.Sh "Footers"
While \f(CW$FORMAT_TOP_NAME\fR contains the name of the current header format,
there is no corresponding mechanism to automatically do the same thing
for a footer.  Not knowing how big a format is going to be until you
evaluate it is one of the major problems.  It's on the \s-1TODO\s0 list.
.PP
Here's one strategy:  If you have a fixed-size footer, you can get footers
by checking \f(CW$FORMAT_LINES_LEFT\fR before each \fIwrite()\fR and print the footer
yourself if necessary.
.PP
Here's another strategy; open a pipe to yourself, using \f(CWopen(MESELF, "|-")\fR 
(see the \f(CWopen()\fR entry in the \fIperlfunc\fR manpage) and always \fIwrite()\fR to \s-1MESELF\s0 instead of
\s-1STDOUT\s0.  Have your child process postprocesses its \s-1STDIN\s0 to rearrange
headers and footers however you like.  Not very convenient, but doable.
.Sh "Accessing Formatting Internals"
For low-level access to the formatting mechanism.  you may use \fIformline()\fR
and access \f(CW$^A\fR (the \f(CW$ACCUMULATOR\fR variable) directly.
.PP
For example:
.PP
.Vb 3
\&    $str = formline <<'END', 1,2,3;
\&    @<<<  @|||  @>>>
\&    END
.Ve
.Vb 1
\&    print "Wow, I just stored `$^A' in the accumulator!\en";
.Ve
Or to make an \fIswrite()\fR subroutine which is to \fIwrite()\fR what \fIsprintf()\fR
is to \fIprintf()\fR, do this:
.PP
.Vb 8
\&    use Carp;
\&    sub swrite {
\&        croak "usage: swrite PICTURE ARGS" unless @_;
\&        my $format = shift;
\&        $^A = "";
\&        formline($format,@_);
\&        return $^A;
\&    } 
.Ve
.Vb 5
\&    $string = swrite(<<'END', 1, 2, 3);
\& Check me out
\& @<<<  @|||  @>>>
\& END
\&    print $string;
.Ve
.SH "WARNING"
Lexical variables (declared with \*(L"my") are not visible within a
format unless the format is declared within the scope of the lexical
variable.  (They weren't visiblie at all before version 5.001.)

.rn }` ''

.\"
.\"  SCCSID = @(#) AtShading.man (V6.0) Sun Aug 16 13:28:37 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtShading 3X
.SH NAME
.B "AtShading -- Specify grayscales in a way compatible with X and PostScript"
.SH SYNOPSIS
.B #include <X11/At/Shading.h>
.nf
.sp
\fB
typedef enum {
	AtGRAY0, AtGRAY1, AtGRAY2, AtGRAY3, AtGRAY4, AtGRAY5,
	AtGRAY6, AtGRAY7, AtGRAY8, AtGRAY9, AtGRAY10
} AtShading;
.sp
Pixmap AtShadingGetPixmap(\fIscreen, shading, fg, bg\fP)
void AtShadingReleasePixmap(\fIpixmap\fP)
char *AtShadingPS(\fIshading\fP)
	Screen *\fIscreen\fP;
	AtShading \fIshading\fP;
	Pixel \fIfg, bg\fP;
.sp
void AtRegisterShadingConverter()
.fi
\fR
.SH DESCRIPTION
The AtShading routines provide access to grayscale or filling
information in a way that is compatible with both X and PostScript.
The intention is to give a single description of a filled area that is
identifiably different from the other fill patterns, is portable, and
corresponds more-or-less to the light-dark range.

In an X window, AtGRAY0 corresponds to solid foreground fill, and
increasing gray levels includes more background.

On PostScript output, AtGRAY0 refers to solid black, AtGRAY1 to 10%
black, etc up to AtGRAY10 being solid white.

The X version caches pixmap/display/color combinations and reference
counts them, so server resources are freed as they are no longer
needed.

.IP AtShadingGetPixmap
Return a pointer to a Pixmap for the specified combination of screen,
shading and pixel values.  This may be a pointer to an exiting Pixmap.
This should be deleted with a call to AtShadingReleasePixmap() rather
than XFreePixmap().

.IP AtShadingReleasePixmap
Decrement the reference count to this pixmap and free it if no other
reference to it exists.

.IP AtShadingPS
Returns a pointer to a static string that contains the PostScript
required to set the graphics state to the specified shading pattern.
In the current implementation, this is a simple string "0.9 setgray",
but this may change with more sophisticated versions.

.IP AtRegisterShadingConverter
this registers a type converter function to convert strings to
AtShading types with the X type mechanism.  Suitable for calling from
the class_initialize routine of classes that have AtShading resources.

.SH BUGS
The current implementation has only six distinct Pixmaps for the X
version, and they don't correspond to the light-dark spectrum in any
way.  Better pixmaps gladly received.

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH NOTES
PostScript is a trademark of Adobe Systems Incorporated.

.SH COPYRIGHT

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

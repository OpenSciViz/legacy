.\"
.\"  SCCSID = @(#) AtTextPlot.man (V6.0) Sun Aug 16 13:28:58 1992
.\"
.\"  Copyright (C) 1990 by the Massachusetts Institute of Technology
.\"
.TH AtTextPlot 3X
.SH NAME
.B AtTextPlot -- the text plot widget class
.SH SYNOPSIS
.B #include <X11/At/TextPlot.h>

.SH DESCRIPTION

The AtTextPlot widget class displays formatted text in an AtPlotter
widget.  The text can be placed in the plotter widget in two ways:
attached to a pair of real coordinates or attached to a pair of pixel
coordinates.  When attached to a real point, the plot will "float"
(i.e. if the coordinate scale changes, the plot will be drawn at the
real points new location).  When attached to a pixel point, the plot
will be drawn in that position even if the coordinate scale changes.
If the physical size of the plotter changes, the plot will be drawn
such that it remains in the same relative position (percentage-wise).

.SH CLASSES
The class pointer is atTextPlotWidgetClass, and the class name is
AtTextPlot.  It is a subclass of AtPlot.

.SH RESOURCES
The AtTextPlot widget inherits all the resources of the Object and AtPlot
widget classes.  In addition, it has the following new resources:

.TS
center tab(#);
cB s s
lI lI lI lI
l l l l.
AtTextPlot Widget Resources
Name#Class#Type#Default
.sp
XtNlabel#XtCLabel#String#NULL
XtNfontFamily#XtCFontFamily#String#dynamic
XtNfontSize#XtCFontSize#AtFontSize#AtFontNORMAL
XtNfontStyle#XtCFontStyle#AtFontStyle#AtFontPLAIN
XtNjustify#XtCJustify#AtJustify#AtTextJUSTIFY_CENTER
XtNx#XtCPosition#int#0
XtNy#XtCPosition#int#0
XtNfloatingPosition#XtCFloatingPosition#Boolean#True
XtNfloatingX#XtCFloatingX#double#0.0
XtNfloatingY#XtCFloatingY#double#0.0
.TE

.IP XtNlabel
Specifies the formatted text label to be displayed.

.IP XtNfontFamily
Specifies the font family to be used in displaying the plot.  The
default value is inherited from the parent AtPlotter widget.

.IP XtNfontSize
Specifies the font size for the plot.

.IP XtNfontStyle
Specifies the font style for the plot.

.IP XtNjustify
Specifies the justification for the plot.  If AtTextJUSTIFY_LEFT, the
plot text will begin at the specified coordinates.  If
AtTextJUSTIFY_CENTER, the plot text will be centered at the specified
coordinates.  If AtTextJUSTIFY_RIGHT, the plot text will end at the
specified coordinates.

.IP XtNx
Specifies the x coordinate (in pixels) for the plot.  If
XtNfloatingPosition is True, this resource is ignored.  At all times,
querying this resource will return the current x pixel coordinate for
the plot.

.IP XtNy
Specifies the y coordinate (in pixels) for the plot.  If
XtNfloatingPosition is True, this resource is ignored.  At all times,
querying this resource will return the current y pixel coordinate for
the plot.

.IP XtNfloatingPosition
Specifies whether the plot should "float" with a specified user (real)
point (if True) or attach itself to a specified (pixel) point on the
plotter (if False).

.IP XtNfloatingX
Specifies the x coordinate (in the user coordinate system) for the
plot.  If XtNfloatingPosition is False, this resource is ignored.

.IP XtNfloatingY
Specifies the y coordinate (in the user coordinate system) for the
plot.  If XtNfloatingPosition is False, this resource is ignored.

.SH USAGE

.nf
textplot = XtCreateWidget(\fItextplot_name\fP, atTextPlotWidgetClass, \fIparent\fP, \fIargs\fP, \fInum_args\fP)
	String \fItextplot_name\fP;
	WidgetClass atTextPlotWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

.SH "SEE ALSO"
AtText(3X), AtFontFamily(3X), AtPlot(3X), AtPlotter(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
David Flanagan (MIT Project Athena) wrote the code
for version V4.
No changes for version V5-beta and V6.0.

.SH COPYRIGHT

Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

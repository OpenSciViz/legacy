.\"
.\"  SCCSID = @(#) AtLinePlot.man (V6.0) Sun Aug 16 13:27:56 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtLinePlot 3X
.SH NAME
.B "AtLinePlot -- Simple Line Plots"
.SH SYNOPSIS
.B #include <X11/At/LinePlot.h>

.SH DESCRIPTION

The AtLinePlot class is the simplest of the plotting classes in the
AtPlotter widget set.  It uses the facilities of the AtSPlot
superclass to access the data to be plotted, and uses the resources of
the AtPlot class to display a single line joining each (X,Y) point.

.SH CLASSES
The class pointer is atLinePlotWidgetClass, and the class name is
AtLinePlot.  It is a subclass of AtSPlot, and uses the facilities of
AtSPlot and AtPlot classes.

.SH RESOURCES

The AtLinePlot widget inherits all the resources from the AtSPlot,
AtPlot and Object widget classes.  It defines no additional resources
or routines.

.SH PUBLIC ROUTINES
.nf
\fB
void AtLinePlotAttachData(\fIspw, data, type, stride, start, num\fP);
void AtLinePlotExtendData(\fIspw, num\fP);
.sp
AtSPlotWidget \fIspw\fP;
XtPointer \fIdata\fP;
AtDataType \fItype\fP;
Cardinal \fIstride, start, num\fP;
\fR
.fi
The header file <At/LinePlot.h> provides these aliases for the AtSPlot
member routines.

.SH USAGE

.nf
line = XtCreateWidget(\fIline_name\fP, atLinePlotWidgetClass, \fIparent\fP, \fIargs\fP, \fInum_args\fP)
	String \fIline_name\fP;
	WidgetClass atLinePlotWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

The parent widget should be of the AtPlotter widget class, or a fatal
error will result.

.SH "SEE ALSO"
AtPlotter(3X), AtSPlot(3X), AtPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH COPYRIGHT

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

.\"
.\"  SCCSID = @(#) AtLabelAxis.man (V6.0) Sun Aug 16 13:27:44 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtLabelAxis 3X
.SH NAME
.B "AtLabelAxis -- Axes labeled with programmer-defined strings"
.SH SYNOPSIS
.B #include <X11/At/LabelAxis.h>

.SH DESCRIPTION
The AtLabelAxis class provides facilities for the application
programmer to specify which integral values in the axis should have
tics, and attach arbitrary labels to those tics.  The XtNticInterval
resource will be set to 0, and no subtics will be generated.

This class is only concerned with the user-space details of tic
positions and tic labels.  The superclass AtAxisCore supplied the
logic and resources for actually drawing the axis.

.SH CLASSES
The class pointer is atLabelAxisWidgetClass, and the class name is
AtLabelAxis.  It is a subclass of AtAxisCore and relies on the superclass
code and resources for most of the functionality.

.SH RESOURCES

The AtLabelAxis widget inherits all the resources from the AtAxisCore,
AtPlot and Object widget classes.  In addition, it has the following new
resource:

.TS
center tab(#);
cB s s
lI lI lI lI
l l l l.
AtLabelAxis Resources
Name#Class#Type#Default
.sp
XtNautoScale#XtCAutoScale#XtRBoolean#True
.TE

.IP XtNautoScale
If this is True, then the XtNmin and XtNmax resources (inherited from
the AtAxisCore superclass) will be set automatically from the bounds
of the parent AtPlotter widget, rounded to the nearest integer.  If
XtNautoScale is False, the XtNmin and XtNmax resources are calculated
from the arguments to the AtLabelAxisAttachData routine.

.SH PUBLIC ROUTINES

.nf
\fB
void AtLabelAxisAttachData(\fIlaw, data, stride, start, num\fP)
	AtLabelAxisWidget \fIlaw\fP;
	String *\fIdata\fP;
	Cardinal \fIstride, start, num\fP;
\fR
.fi
This routine is used to attach application data to the axis widget.
The widget expects \fInum\fP character pointers in an array, with
\fIstride\fP bytes between each one.  (In an array of pointers,
\fIstride\fP will be \fIsizeof(String)\fP; in an array of structures,
the \fIstride\fP will be the size of the structure as returned by
\fIsizeof()\fP.)  \fIData\fP points to the first pointer in that
array.

If the \fIn\fPth pointer is non-NULL and points to a non-empty string,
then a tic is placed at position \fIstart + n\fP and is labeled with
the given string.  The label string can contain formatting commands as
described in
.IR AtText (3).

If no label is specified for the endpoints of the axis, a tic mark
with no label is generated.

.SH USAGE

.nf
axis = XtCreateWidget(\fIaxis_name\fP, atLabelAxisWidgetClass, \fIparent\fP,
		      \fIargs\fP, \fInum_args\fP)
	String \fIaxis_name\fP;
	WidgetClass atLabelAxisWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

The parent widget should be of the AtPlotter widget class, or a fatal
error will result.

.SH "SEE ALSO"
AtPlotter(3X), AtAxisCore(3X), AtPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH COPYRIGHT

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

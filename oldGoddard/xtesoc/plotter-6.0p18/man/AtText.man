.\"
.\"  SCCSID = @(#) AtText.man (V6.0) Sun Aug 16 13:28:47 1992
.\"
.\"  Copyright (C) 1990 by the Massachusetts Institute of Technology
.\"
.TH AtText 3X
.SH NAME
.B AtText -- Multifont and mathematical text display

.SH SYNOPSIS

.nj
.nf
\fB
#include <X11/At/Text.h>

AtText* AtTextCreate(\fIstring, family, size, style\fP);
void AtTextDestroy(\fItext\fP);
void AtTextReformat(\fItext, family, size, style\fP);
void AtTextRotate(\fItext\fP);
.sp
int AtTextWidth(\fItext\fP);
int AtTextAscent(\fItext\fP);
int AtTextDescent(\fItext\fP);
int AtTextHeight(\fItext\fP);
void AtTextDraw(\fIdpy, drawable, gc, text, x, y\fP);
void AtTextDrawJustified(\fIdpy, drawable,gc,text,hjust,vjust,x,y,w,h\fP);
.sp
int AtTextPSWidth(\fItext\fP);
int AtTextPSAscent(\fItext\fP);
int AtTextPSDescent(\fItext\fP);
void AtTextPSDraw(\fIfile, text, x, y\fP);
void AtTextWritePostscriptProlog(\fIfile\fP);
.sp
	char *\fIstring\fP;
	AtFontFamily *\fIfamily\fP;
	int \fIsize\fP;
	AtText *\fItext\fP;
	Display *\fIdpy\fP;
	Drawable \fIdrawable\fP;
	GC \fIgc\fP;
	int \fIx,y,w,h\fP;
	int \fIhjust,vjust\fP;
	FILE *\fIfile\fP;
\fP
.fi

.SH DESCRIPTION

These routines allow for the display of formatted multi-font text
including greek letters, mathematical symbols, superscripts and
subscripts.  Text by default is displayed horizontally, left to right,
but may be rotated 90 degrees counterclockwise and displayed vertically.
Text is currently restricted to a single line and may be displayed left
or right (or top or bottom) justified or centered within a bounding box.
Formatted text is described using a syntax similar to that used by the
.I Scribe
text formatter.  The supported commands and environments are described
below.  The AtText functions are as follows:

.IP "\fBAtTextCreate()\fP"
This function parses the formatting commands in \fIstring\fP and
formats the text using fonts from \fIfamily\fP and the initial font
size \fIsize\fP and initial style \fIstyle\fP.  (The size and style,
but not family, can be changed mid-string using the environments
described below).  It returns a pointer to an AtText structure which
is used in all future calls to AtText routines.

.IP "\fBAtTextDestroy()\fP"
This function frees up all resources associated with \fItext\fP, then
frees the AtText structure itself.  \fItext\fP should not be referenced
again after this call.

.IP "\fBAtTextReformat()\fP"
This function re-formats \fItext\fP using the fonts in \fIfamily\fP at
the initial size given by \fIsize\fP with the initial style given by
\fIstyle\fP.

.IP "\fBAtTextRotate()\fP"
This function rotates \fItext\fP by 90 degrees counterclockwise.  Future
calls to \fBAtTextDraw()\fP will display the text vertically.  This
function changes the width, ascent, and descent of \fItext\fP.  Invoking
this function more than once on \fItext\fP has no effect.

.IP "\fBAtTextWidth()\fP"
This function returns the width in pixels that \fItext\fP would take up
if it were to be displayed as currently formatted.  \fBAtTextWidth\fP is
implemented as a macro.

.IP "\fBAtTextAscent()\fP"
This function returns the height in pixels that \fItext\fP would take up
above the baseline if it were to be displayed as currently formatted.
\fBAtTextAscent\fP is implemented as a macro.

.IP "\fBAtTextDescent()\fP"
This function returns the height in pixels that \fItext\fP would take
up below the baseline if it were to be displayed as currently formatted.
\fBAtTextDescent\fP is implemented as a macro.

.IP "\fBAtTextHeight()\fP"
This function return the total height of the text, equal to the ascent
plus the descent.  It is implemented as a macro.

.IP "\fBAtTextDraw()\fP"
This function displays \fItext\fP using \fIdpy\fP, \fIdrawable\fP
and \fIgc\fP at coordinates (\fIx\fP,\fIy\fP).  \fIx\fP is the leftmost
pixel of the text, and \fIy\fP is the baseline of the text.

.IP "\fBAtTextDrawJustified()\fP"
This function draws \fItext\fP horizontally and vertically justified in
the bounding box described by \fIx,y,w,h\fP.  \fIdpy, drawable,\fP and
\fIgc\fP are as usual.  \fIhjust\fP and \fIvjust\fP describe the
horizontal and vertical justification of the text.  They should each
have one of the following values: AtTextJUSTIFY_LEFT, AtTextJUSTIFY_TOP,
AtTextJUSTIFY_CENTER, AtTextJUSTIFY_RIGHT, or AtTextJUSTIFY_BOTTOM.

.IP "\fBAtTextPSWidth()\fP"
.IP "\fBAtTextPSAscent()\fP"
.IP "\fBAtTextPSDecent()\fP"
.IP "\fBAtTextPSDraw()\fP"
These routines are the PostScript equivalents to the previous
routines.  The PostScript and screen widths will not necessarily be
the same.

.IP  "\fBAtTextWritePostscriptProlog()\fP"
This writes a simple two-line prolog for the text facilities to the
given file.

.SH FORMATTING

Text is formatted by using embedded commands, all of which begin with an
"@" character.  An environment is a specification to display some piece
delimited text in a special way.  Example environments are bold and
superscript.  To specify an environment, follow the at-sign with the
environment name, followed by the delimited text.  The delimited text is
displayed (without delimiters) as specified by the environment.  Valid
delimiter pairs are () [] {} and <>.  Environments can be nested.  As an
example, the string "@b[H@-(2)O]" would display the molecular formula
for water in bold face.  Occasionally, it may be desirable to display
a closing delimiter within an environment.  If the environment is
delimited by open and closing angle brackets, for example, it is fine to
use any of ")", "]", and "}" within the environment.  To display ">",
however, use "@>".  The same applies for the other delimiters.
Supported environments are listed below.

Mathematical symbols are generated with an at-sign followed by the name
of the symbol.  Spaces following a symbol name are not displayed.  In
order to explicitly insert a space, use an at-sign followed by a space.
For example, to display the equation "a is proportional to b" without
any spaces between the characters, use "a@propto b".  To display the
string with spaces on either side of the "proportional to" sign, use "a
@propto@ b".  Symbol names are not case sensitive.  Supported symbols
are listed in the table below.

Greek letters can be displayed using the greek environment ("@g"), or by
following an at-sign with the spelled out name of the letter.  In the
environment, letters are replaced with their closest greek equivalent.
"a" maps to alpha, and "g" to gamma, for example.  Case is significant,
so "G" maps to a capital gamma.  Case is also significant in the other
form.  "@beta" give a lower case beta, and "@Delta" gives an upper case
delta.

Implemented environments are:

.TS
center tab(#);
cB s
cI cI
c l.
AtText Environments
name#effect

b#display text in bold face
i#display text in italic face
g#display greek characters
r#display text in roman (plain) face
bigger#display text in a bigger font size
smaller#display text in a smaller font size
+#display text as a superscript
-#display text as a subscript.
.TE

Defined symbol names are:

.TS
center tab(#);
cB s
cI cI
lB s
a l.
AtText Symbols
symbol name#description

Operators
times#multiplication sign (like an "x")
cdot#dot in middle of line
div#division sign
otimes#multiplication sign in a circle
oplus#addition sign in a circle
.sp
.T&
lB s, a l.
Relations
neq#not equal to
lte#less than or equal
gte#greater than or equal
approx#approximately equal (two squiggles)
eqv#equivalent (three horizontal bars)
similar#similar (one squiggle)
cong#congruent (2 bars and a squiggle)
propto#is proportional to
perp#is perpendicular to
.sp
.T&
lB s, a l.
Boolean Algebra
not#logical not symbol
and#logical and symbol
or#logical or symbol
.sp
.T&
lB s, a l.
Set Theory
emptyset#the empty set, circle with a slash
inter#intersection (upside down U)
union#union (big "U")
prsupset#proper superset
supset#superset
notsubset#not a subset
prsubset#proper subset
subset#subset
in#member of
notin#not a member of
.sp
.T&
lB s, a l.
Script letters
function#script lowercase f
aleph#aleph
iset#script capital I, set of integers
rset#script capital R, set of real numbers
weierstrass#?
.sp
.T&
lB s, a l.
Other Mathematical Symbols
forall#universal quantifier (upside down 'A')
exists#existential quantifier (backwards 'E')
partial#partial derivative
nabla#gradient, upside down delta
infty#infinity
ni#backwards "member of" sign
therefore#therefore (three dots in a triangle)
pm#plus or minus
prime#prime or minute sign
dblprime#double prime or second sign
ldots#ellipsis
bullet#filled circle, "bullet"
ast#centered asterisk
degr#degree symbol
angle#angle symbol
.sp
.T&
lB s, a l.
Greek Variants
upsilon1#variant of capital upsilon
theta1#variant of lowercase theta
sigma1#variant of lowercase sigma
phi1#variant of lowercase phi
omega1#variant of lowercase omega
.sp
.T&
lB s, a l.
Arrows
leftarrow#left arrow
uparrow#up arrow
rightarrow#right arrow
downarrow#down arrow
leftrightarrow#arrow pointing left and right
dbleftarrow#double left arrow
dbuparrow#double up arrow
dbrightarrow#double right arrow, implies
dbdownarrow#double down arrow
dbleftrightarrow#double left and right arrow, if and only if
.sp
.T&
lB s, a l.
Publishing symbols
registered#registered trademark symbol (R in circle)
copyright#copyright symbol (C in circle)
trademark#trademark symbol (superscript TM)
.sp
.T&
lB a, a l.
Misc
clubs#playing cards clubs
diamonds#playing cards diamonds
hearts#playing cards hearts
spades#playing cards diamonds

.TE

.SH STATUS

The AtText facility will be undergoing further development.  Future
additions include support for multi-line text, and support for more
complicated mathematics, including fractions, square-roots, integrals,
and summations.  Cut-and-paste is also planned.  Another future
enhancement may be the ability to define "hot-spots" in the text which
respond to mouse clicks -- this would provide hypertext capability.

.SH EXAMPLE
.nj
.nf
AtText *t;
AtFontFamily *ff;
FILE *fp;

ff = AtFontFamilyCreate(dpy, "courier");
t = AtTextCreate("@g(D)G@+[H@-{2}O]", ff, AtFontNORMAL, AtFontPLAIN);
AtTextRotate(t);
AtTextDraw(dpy, win, gc, t, 100,100);
AtTextPSDraw(fp, t, 100, 200);
.fi

.SH AUTHOR
David Flanagan (MIT Project Athena) wrote the code
for version V4.
No changes for versions V5-beta.
Minor bug fixes for V6.0.

.SH NOTES

PostScript is a trademark of Adobe Systems Incorporated.
.SH COPYRIGHT

Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

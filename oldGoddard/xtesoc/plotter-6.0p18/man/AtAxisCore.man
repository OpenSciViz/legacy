.\"
.\"  SCCSID = @(#) AtAxisCore.man (V6.0) Sun Aug 16 13:27:00 1992
.\"
.\"  Copyright (C) 1990 by the Massachusetts Institute of Technology
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"  Copyright (C) 1992 by University of Paderborn
.\"
.TH AtAxisCore 3X
.SH NAME
.B "AtAxisCore -- the guts of all the Axis widget classes"
.SH SYNOPSIS
.B #include <X11/At/AxisCore.h>

.SH DESCRIPTION

The AtAxisCore widget class is the root of all
axis widget classes in the AtPlotter widget set.
It is a meta-class and should not be instantiated by applications.
Instead, one of the subclasses should be used.
The subclass provides all the calculation of XtNmin,
XtNmax and XtNticInterval resources,
as well as deciding the (user-space)
position of the tics and subtics
and the labels associated with each tic.
The AtAxisCore code then handles all the conversions to pixel
locations as well as drawing the axis on the screen and in PostScript.
Hence all the resources that determine the appearance
of the axis are part of the AtAxisCore class.

Each axis object consists of the following components:
.IP -
A line that represents the actual axis.
This line is drawn using the resources inherited from the AtPlot class.
The "inside" of the axis is the side that faces the main plotting area;
the other side (towards the edge of the graph widget) is called the "outside".
And who said computers were confusing?
.IP -
An optional line that represents the frame of the actual axis.
This line is drawn using the resources from the axis line
on the opposite side of the axis on the plotting area.
.IP -
An optional line that represents the origin of the axis.
This line also is drawn using the resources from the axis line.
Of course, this line can only be drawn if the origin (0.0)
is located within the axis range.
.IP -
An optional label associated with the axis.
This label is displayed outside the axis,
and outside the tic labels (if any).
The color and style information for the axis is independently set.
.IP -
A set of tics.
Each tic can be marked on the inside and/or outside of the axis line,
and is drawn in the same style and color as the axis.
.IP -
A set of labels associated with each tic.
The color, size and style of these labels can be set.
These labels are known as numbers in the resource names
(i.e. XtNdrawNumbers et al.).
They can be either inside or outside the axis, or hidden completely.
.IP -
A grid and optionally a subgrid.
These are drawn in the same color as the main axis
but with dotted lines.
A grid line is drawn at each tic position if the XtNdrawGrid resource is True.
A subgrid line is drawn at each subtic position if the
XtNdrawSubgrid resource is True.
.IP -
There may be subtics associated with each tic that divides the
interval between tics (if the axis subclass supports them).
Like tics, they are drawn in the same color as the main axis,
and may be inside and/or outside the axis.

All axis widget classes must be children of an AtPlotter widget;
it is a fatal error to create them as children of another class.

.SH CLASSES
The class pointer is atAxisCoreWidgetClass,
and the class name is AtAxisCore.
It is a subclass of AtPlot and Object.

.SH RESOURCES

The AtAxisCore widget inherits all the resources
from the AtPlot and Object widget classes.
In addition, AtAxisCore has the following new resources:

.TS
center tab(#);
cB s s s
lI lI lI lI
l l l l.
AtAxis Resources
Name#Class#Type#Default
.sp
XtNaxisColor#XtCForeground#XtRPixel#XtDefaultForeground
XtNaxisWidth#XtCAxisWidth#XtRDimension#1
XtNdrawFrame#XtCDrawFrame#XtRBoolean#True
XtNdrawGrid#XtCDrawGrid#XtRBoolean#True
XtNdrawNumbers#XtCDrawNumbers#XtRBoolean#True
XtNdrawOrigin#XtCDrawOrigin#XtRBoolean#True
XtNdrawSubgrid#XtCDrawSubgrid#XtRBoolean#False
XtNfontFamily#XtCFontFamily#XtRString#dynamic
XtNlabel#XtCLabel#XtRString#NULL
XtNlabelColor#XtCForeground#XtRPixel#XtDefaultForeground
XtNlabelSize#XtCFontSize#XtRFontSize#AtFontNORMAL
XtNlabelStyle#XtCFontStyle#XtRFontStyle#AtFontPLAIN
XtNmax#XtCMax#XtRDouble#1.0
XtNmin#XtCMin#XtRDouble#0.0
XtNmirror#XtCMirror#XtRBoolean#False
XtNnumberColor#XtCForeground#XtRPixel#XtDefaultForeground
XtNnumberSize#XtCFontSize#XtRFontSize#AtFontSMALL
XtNnumberStyle#XtCFontStyle#XtRFontStyle#AtFontPLAIN
XtNnumberWidth#XtCNumberWidth#XtRDimension#0
XtNnumbersOutside#XtCNumbersOutside#XtRBoolean#True
XtNrangeCallback#XtCCallback#XtRCallback#NULL
XtNsubticLength#XtCTicLength#XtRDimension#2
XtNticInterval#XtCTicInterval#XtRDouble#1.0
XtNticLength#XtCTicLength#XtRDimension#5
XtNticsInside#XtCTicsInside#XtRBoolean#False
XtNticsOutside#XtCTicsOutside#XtRBoolean#True
XtNvertical#XtCVertical#XtRBoolean#False
.TE

.IP XtNaxisColor
Specifies the color of the axis, origin and frame lines.

.IP XtNaxisWidth
Specifies the width of the axis, origin and frame lines in pixels.

.IP XtNdrawFrame
Specifies whether a frame line should be drawn at the
opposite side of the axis line on the plotting area.

.IP XtNdrawGrid
Specifies whether dotted lines should be drawn at each tic mark
perpendicular to the axis across the plotting region.
If True, it creates a "graph paper" effect.

.IP XtNdrawNumbers
Specifies whether the axis tic labels should be displayed.

.IP XtNdrawOrigin
Specifies whether a line should be drawn at the origin of the axis.

.IP XtNdrawSubgrid
Specifies whether dotted lines should be drawn at each subtic mark
perpendicular to the axis across the plotting region.

.IP XtNfontFamily
Specifies the font family to use for the axis.
All strings on this axis come from this family.
The default value is the value of the
XtNfontFamily resource of the parent AtPlotter widget.

.IP XtNlabel
Specifies the string to display as the axis label.
Specifying an empty string (or NULL) will disable the label on the axis,
reclaiming the space for the main graph area.

.IP XtNlabelColor
Specifies the color of the axis label.

.IP XtNlabelSize
Specifies the initial font size for the axis label.

.IP XtNlabelStyle
Specifies the initial font style for the axis label.

.IP XtNmax
Specifies the maximum value to be displayed on the axis.

.IP XtNmin
Specifies the minimum value to be displayed on the axis.

.IP XtNmirror
Specifies whether the axis should be drawn as
a mirror image of its "normal" configuration.
A "normal" axis is drawn on the left or bottom of the plotting region
(i.e. an X or Y axis).
An axis with this resource set to True would be appropriate
for the top or right of a a plotting area
(i.e. an X2 axis or a Y2 axis).
The AtPlotter widget requires the value of this resource
to be set as appropriate.
Once the widget is created, this resource cannot be changed.

.IP XtNnumberColor
Specifies the color of the tic labels.

.IP XtNnumberSize
Specifies the initial font size used for the tic labels.

.IP XtNnumberStyle
Specifies the initial font style used for the tic labels.

.IP XtNnumberWidth
Specifies the number of pixels that should be reserved for the numbers.
The default value 0 means the number width will be calculated automatically.

.IP XtNnumbersOutside
If True, this resource specifies that the tic labels
will be drawn "outside" the axis.
Otherwise, they will be drawn inside the axis
(and hence overwrite (or be overwritten by) the main graph elements).
If the XtNdrawNumbers resource is False, this resource has no effect.

.IP XtNrangeCallback
This callback routine is called as the last stage
of determining the min and max resource for this axis.
The called routine can change the calculated min and max values if required.
See the section on \fICALLBACKS\fP below.

.IP XtNsubticLength
Specifies the length in pixels of the subtic marks.
This many pixels are drawn on the "outside" or "inside" of the axis,
or both, or neither depending on the state of the XtNticsOutside
and XtNticsInside resources.

.IP XtNticInterval
Specifies the interval (in the user coordinate system,
not pixels) between tics.

.IP XtNticLength
Specifies the length in pixels of the tic marks.
This many pixels are drawn on the "outside" or "inside" of the axis,
or both, or neither depending on the state of the XtNticsOutside
and XtNticsInside resources.

.IP XtNticsInside
Specifies whether tic and subtic marks should be drawn
on the "inside" of the axis.

.IP XtNticsOutside
Specifies whether tic and subtic marks should be drawn
on the "outside" of the axis.

.IP XtNvertical
Specifies whether the axis is to be drawn vertically.
If this resource is set to False,
the axis will be drawn horizontally.
The AtPlotter widget required the correct value of this resource
as appropriate if the axis is one of the currently used axes.
It cannot be changed after the widget is created.

.SH "PUBLIC ROUTINES"
These routines are intended primarily for use
by other elements of the AtPlotter widget set,
but may be of use to application writers.

.nf
\fB
void AtAxisGetBounds(\fIw, minp, maxp\fP);
AtScale *AtAxisGetScale(\fIw\fP);
Dimension AtAxisGetNumberWidth(\fIw\fP);
AtTransform AtAxisGetTransform(\fIw\fP);
	AtAxisCoreWidget \fIw\fP;
	double *\fIminp\fP, *\fImaxp\fP;
\fR
.fi

.IP AtAxisGetBounds()
Returns in the pointed-to doubles the min and max of this axis as it
is currently displayed.  Is the equivalent of doing XtGetValues for
the XtNmin and XtNmax resources.

.IP AtAxisGetNumberWidth()
Calculates and returns the width of the numbers.

.IP AtAxisGetScale()
Returns the AtScale object that this axis is using
to scale plots on the screen.

.IP AtAxisGetTransform()
Returns the type of transform for this axis.  Is the equivalent of
.br
\fIAtScaleGetTransform(AtAxisGetScale(widget))\fP.

.SH "PLOTTER ROUTINES"

These routines are globally accessible, but are designed for use
between elements of the AtPlotter widget set only (primarily by the
parent AtPlotter widget.)  They probably should not be called from
applications.

.nf
\fB
void AtAxisAskRange(\fIw,  minp, *maxp\fP);
void AtAxisDrawPS(\fIw, fp, scale, x1, y1, x2,  y2,  grid_len\fP);
Boolean AtAxisSetPosition(\fIw, x1, y1, x2, y2, grid_len\fP);
int AtAxisWidth(\fIw\fP);
int AtAxisWidthPS(\fIw\fP);
	AtAxisCoreWidget \fIw\fP;
	int \fIx1, y1, x2, y2, grid_len\fP;
	FILE *\fIfp\fP;
	AtScale *\fIscale\fP;
\fR
.fi
.IP AtAxisAskRange()
This is the procedure that the AtPlotter calls
to pass the min and max from the plots to the axis.
It calls the AtAxisCore range class method,
calls the XtNrangeCallback function,
then accepts the min and max as determined.
It then calculates and stores the axis width.

.IP AtAxisDrawPS()
Dumps the PostScript version of this axis on the file specified.
This cannot be done using the AtPlot draw_ps class member function
because the AtScale set up with the PostScript dimensions needs
to be passed in.

.IP AtAxisSetPosition()
The AtPlotter calls this routine to set the pixel location
of the axis after the layout has been determined.
Returns True if the axis length has changed enough
that a recalc is required because the number of tics might have changed.

.IP AtAxisWidth()
Returns the stored axis width so the AtPlotter can layout
the pixel locations of all the graph elements.

.IP AtAxisWidthPS()
returns the width of the axis in PostScript units
so the PostScript version of the graph can be laid out.

.SH USAGE

The AtAxisCore widget should never be created by applications.
Rather, use one of the subclasses.

.SH "CALLBACK INFORMATION"

The XtNrangeCallback list is called by the AtAxisAskRange member function,
which is in turn called whenever the parent AtPlotter widget
considers that the axis bounds may have changed
(usually, when plot data has changed,
but some resources changes fill force a recalc).
The call_data argument to this callback is of type AtAxisRangeArgs:

.nf
\fB
typedef struct {
	double *minp, *maxp, *tic_intervalp;
	int *max_widthp;
} AtAxisRangeArgs;
\fP
.fi
.PP
The called function is free to modify the pointed-to values as required;
the values will be used to scale plots attached to this axis
with no further modification.

.SH "SEE ALSO"
AtPlotter(3X), AtText(3X), AtFontFamily(3X), AtPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
David Flanagan (MIT Project Athena)
and Chris Craig (MIT Project Athena)
wrote the code for version V4.
The version V5-beta was completely re-written using some code
and many algorithms from that version by Gregory Bond,
Burdett, Buckeridge & Young Ltd. (gnb@bby.oz.au).
A log of changes and additions for this version V6.0
were done by Peter Klingebiel,
University of Paderborn (klin@iat.uni-paderborn.de).

.SH NOTES
PostScript is a trademark of Adobe Systems Incorporated.

.SH COPYRIGHT

Copyright 1990,1991 by the Massachusetts Institute of Technology
.br
Copyright 1991 by Burdett, Buckeridge and Young Ltd.
.br
Copyright 1992 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

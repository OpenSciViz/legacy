.\"
.\"  SCCSID = @(#) AtPlot.man (V6.0) Sun Aug 16 13:28:06 1992
.\"
.\"  Copyright (C) 1990 by the Massachusetts Institute of Technology
.\"
.TH AtPlot 3X
.SH NAME
.B AtPlot -- the Plot widget meta-class
.SH SYNOPSIS
.B #include <X11/At/Plot.h>

.SH DESCRIPTION

AtPlot should never be instantiated.  It is intended to be a
superclass for other plot classes and thus has no display semantics.
All plots created as children of an AtPlotter widget should be
subclasses of AtPlot, and have their own names and set of resources.
The AtPlot class provides a set of member routines (for use within the
plotter widget set) and a number of resources that specify the
thickness, style and color for elements of the graph.  The convention
is that the subclasses use the resources from the AtPlot class to draw
the primary part of their display, and define additional resources if
there is more than one component.

.SH CLASSES
The class pointer is atPlotWidgetClass, and the class name is AtPlot.

.SH RESOURCES
The AtPlot widget inherits all the resources from the Object widget
class.  In addition, it has the following new resources:

.TS
center tab(#);
cB s s s
lI lI lI lI
l l l l.
AtPlot Widget Resources
Name#Class#Type#Default
.sp
XtNforeground#XtCForeGround#XtRPixel#XtDefaultForeground
XtNbackground#XtCBackGround#XtRPixel#dynamic
XtNlineWidth#XtCLineWidth#XtRInt#0
XtNlineStyle#XtCLineStyle#XtRLineStyle#LineSolid
XtNdashLength#XtCDashLength#XtRInt#4
XtNfastUpdate#XtCFastUpdate#XtRBoolean#False
.sp
.TE

.IP XtNforeground
Specifies the foreground color to use to draw the plot.

.IP XtNbackground
Specifies the background color to use to draw the plot.
The default value is the XtNbackgroundPixel resource of the parent
AtPlotter widget.

.IP XtNlineWidth
Specifies the line width to use to draw the plot.  The default value of
0 will give a line a single pixel wide.  This may be faster than
specifying a width of 1.

.IP XtNlineStyle
Specifies the line style to use to draw the plot.  This resource is used
for the line_style field of a GC.  Possible values are LineSolid,
LineDoubleDash, or LineOnOffDash.

.IP XtNdashLength
Specifies the dash length to use when drawing dashed lines.

.IP XtNfastUpdate
Provides a "hint" as to whether the plot should attempt to configure
itself so fast updates (not requiring the AtPlotter to redraw itself
completely) are possible.  Typically, a plot might draw itself using
the XOR function if this resource is True, which often results in a
"messier" plot than normal.  If fast updates are not necessary (i.e.
the data isn't changing in real time), this resource should probably
remain False.

.SH PLOTTER ROUTINES

This routine is exported for the benefit of subclasses.
.nf
\fB
void AtPlotPSLineStyle(\fIfp, w\fP)
FILE *\fIfp\fP;
AtPlotWidget \fIw\fP;
\fR
.fi
Writes a short piece of PostScript to the named file that sets the
PostScript graphics state to draw lines somewhat similar to the screen
display implied by the resources of the named widget.

.SH BUGS
The XtNfastUpdate  mode is implemented  in only a  few  subclasses and
tested in none.  You probably shouldn't rely  on it.  (This version of
the widget set is significantly more efficient than previous versions,
so XtNfastUpdate may no longer be needed.)

.SH "SEE ALSO"
AtPlotter(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
David Flanagan (MIT Project Athena), Chris Craig (MIT Project Athena),
and Kambiz Soroushian (MIT Project Athena) wrote the code for version V4.
No changes for versions V5-beta and V6.0.

.SH COPYRIGHT

Copyright (C) 1990 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

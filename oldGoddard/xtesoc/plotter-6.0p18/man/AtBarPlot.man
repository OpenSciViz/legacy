.\"
.\"  SCCSID = @(#) AtBarPlot.man (V6.0) Sun Aug 16 13:27:10 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtBarPlot 3X
.SH NAME
.B "AtBarPlot -- Simple Bar Plots"
.SH SYNOPSIS
.B #include <X11/At/BarPlot.h>

.SH DESCRIPTION
The AtBarPlot class provides simple bar plots,
using the facilities of the AtSPlot class to access application data.
The X values of the bars are successive integers,
and the Y values are given by application data.
Each bar on the plot can be outlined and/or filled.
The outlining is done using the resources inherited from the AtPlot class.
The filling can be in the same or a different colour,
using one of 11 stipple patterns if required.

.SH CLASSES
The class pointer is atBarPlotWidgetClass,
and the class name is AtBarPlot.
It is a subclass of AtSPlot,
and uses the facilities of AtSPlot and AtPlot classes.

.SH RESOURCES

The AtBarPlot widget inherits all the resources from the AtSPlot,
AtPlot and Object widget classes.
It defines the following extra resources:
.TS
center tab(#);
cB s s s
lI lI lI lI
l l l l.
AtBarPlot Widget Resources
Name#Class#Type#Default
.sp
XtNcellWidth#XtCCellWidth#XtRDouble#1.0
XtNcellOffset#XtCCellOffset#XtRDouble#0.0
XtNdoOutline#XtCDoOutline#XtRBoolean#False
XtNdoFill#XtCDoFill#XtRBoolean#True
XtNzeroMin#XtCZeroMin#XtRBoolean#True
XtNfillColor#XtCForeground#XtRPixel#XtDefaultForeground
XtNshading#XtCShading#XtRShading#AtGRAY0
XtNscreenShade#XtCScreenShade#XtRBoolean#True
.TE

.IP XtNcellWidth
This resource specifies the width of each bar in the graph.
The default is 1.0, which implies that adjacent bars will touch.
Legal values are 0.0 through 1.0.

.IP XtNcellOffset
This specifies where in the cell \fIx\fP through \fIx + 1\fP
the left hand edge of the bar for value \fIx\fP is placed.
Default value is 0.0,
which implies that the "hot slot" for the bar will be the top
left hand corner (i.e. this will be position (X,Y) on the graph).
Legal values are from -1.0 through 1.0.
In conjunction with the XtNcellWidth resource,
this can be used to make "overlapping bar" graphs.

.IP XtNdoOutline
If True, the outline for the bars is drawn, using the color,
line style etc inherited from the AtPlot class.
If false, the outline is not drawn.

.IP XtNdoFill
If True, the interior of the bars is filled,
using the XtNfillColor and the XtNshading if required.
If False, the fill is not done.
At least one of XtNdoFill and XtNdoOutline must be True;
XtNdoFill will be forced True if required to enforce this.

.IP XtNzeroMin
If True, this forces the minimum value of the plot
(as reported to the AtPlotter widget) to be 0.
Otherwise, the lowest Y value is used.
This can be useful in maintaining proper perspective.
What the parent plotter (and in particular, the relevant Y axis)
does with this reported minimum will vary.

.IP XtNfillColor
This is the color used for the filling of the bars when XtNdoFill is True.

.IP XtNshading
The filled part of the bar can be either solid color
(implied when XtNshading is AtGray0),
or stippled in a series of "percent gray" stipples,
(known as AtGray1 through AtGray10).
On the screen display,
a stipple using the XtNfillColor and XtNbackground
(from AtPlot class) colors will be used.
When PostScript output is generated,
these shading values translate into a gray scale argument
to the setgray operator.

.IP XtNscreenShade
When True, the stipple pixmap implied by the XtNshading resource is
applied to the filled area of the bars when displayed on the screen.
When False, the filled areas on the screen display of the bar plot are
drawn in solid foreground color irrespective of the value of XtNshading.
This is useful so that different colors can be used
on color displays for identifying different bar plots,
and the grayscale stipples used on mono screens and PostScript output.

.SH PUBLIC ROUTINES
.nf
\fB
void AtBarPlotAttachData(\fIspw, data, type, stride, start, num\fP);
void AtBarPlotExtendData(\fIspw, num\fP);
.sp
	AtSPlotWidget \fIspw\fP;
	XtPointer \fIdata\fP;
	AtDataType \fItype\fP;
	Cardinal \fIstride, start, num\fP;
\fR
.fi
The header file <At/BarPlot.h> provides these aliases for the AtSPlot
member routines.

.SH USAGE

.nf
barplot = XtCreateWidget(\fIbarplot_name\fP, atBarPlotWidgetClass, \fIparent\fP, \fIargs\fP, \fInum_args\fP)
	String \fIbarplot_name\fP;
	WidgetClass atBarPlotWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

The parent widget should be of the AtPlotter widget class,
or a fatal error will result.

.SH BUGS
The stipples provided by the XtNshading resource are not true
percentage-gray stipples,
and in fact only 5 separate stipples are provided.
Sources of good stipples would be welcome.

.SH "SEE ALSO"
AtPlotter(3X), AtSPlot(3X), AtPlot(3X), AtShading(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH NOTES
PostScript is a trademark of Adobe Systems Incorporated.

.SH COPYRIGHT

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

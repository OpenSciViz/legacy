.\"
.\"  SCCSID = @(#) AtSPlot.man (V6.0) Sun Aug 16 13:28:26 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtSPlot 3X
.SH NAME
.B AtSPlot -- Plot meta-class for disecting structures.
.SH SYNOPSIS
.B #include <X11/At/SPlot.h>

.SH DESCRIPTION

The AtSPlot class is a placeholder for some code and resources for
plot classes that need access to application data.  Its main purpose
is to provide two public functions that applications use to attach
data to the plot widgets, as well as support routines for the widget
subclasses.

.SH CLASSES
The class pointer is atSPlotWidgetClass, and the class name is
AtSPlot.  It is a subclass of the AtPlot and Object widget classes.

.SH RESOURCES

The AtSPlot widget inherits all the resources from the AtPlot and
Object widget classes.  It defines no new resources.

.SH PUBLIC FUNCTIONS
.nf
\fB
typedef enum { AtDouble, AtFloat, AtInt } AtDataType;

void AtSPlotAttachData(\fIspw, data, type, stride, start, num\fP);
void AtSPlotExtendData(\fIspw, num\fP);
.sp
	AtSPlotWidget \fIspw\fP;
	XtPointer \fIdata\fP;
	AtDataType \fItype\fP;
	Cardinal \fIstride, start, num\fP;
\fR
.fi
.sp
Most subclasses of AtSPlot provide \fB#define\fP'd aliases for these
routines.

.IP AtSPlotAttachData()
This routine is used to attach application data to the subclass plot
widget. The widget expects \fInum\fP data items in an array, with
\fIstride\fP bytes between each one.  (In an array of doubles,
\fIstride\fP will be \fIsizeof(double)\fP; in an array of structures,
the \fIstride\fP will be the size of the structure as returned by
\fIsizeof()\fP.)  \fIData\fP points to the first item in that
array.  Each item is of the type described in \fItype\fP, an
enumerated type.  Currently, only \fBfloat\fP, \fBdouble\fP and \fBint\fP
are supported, but that is easily extended.  All values are converted
to doubles by the atSPlot routines, so the rest of the widget set
works in doubles only.

Each of the \fInum\fP items in the array is the Y value of an (X,Y)
pair.  The X value for the \fIn\fPth item in the array is \fIstart +
n\fP.  This implies that there are no holes in the sequence of data.

The data in the array pointed to by \fIdata\fP should remain valid and
unchanged as it may be accessed any time should PostScript output be
generated or should the parent AtPlotter widget resize or change
internal layout.

If the application data changes, a new call to AtSPlotAttachData
\fImust\fP be made (even with the same parameters) so that a new
bounding box will be calculated and the axis scales readjusted if
necessary.

.IP AtSPlotDataExtended()
This changes the number of datapoints in the array to \fInum\fP.  The
existing elements of the array should not have changed.

If some extra data has been added to the end of the valid data (for
instance because the data represents a time series and the next
collection point has just occurred), then this routine will plot the
extra data on the existing graph with the minimum possible recalculation
and redisplay.  Should this not be possible, (because the new data
exceeds existing axis bounds in either X or Y axes) then the effect is
the same as the equivalent call to AtSPlotAttachData().

.SH PLOTTER ROUTINES
The following routine is mainly intended for use by subclasses of
AtSPlot, but applications may find it useful.  It may be implemented
as a macro in some environments.

.nf
\fB
double AtSPlotGetValue(\fIp, i\fP)
	AtSPlotWidget \fIp\fP;
	Cardinal \fIi\fP;
\fR
.fi
Get the \fIi\fPth data item for the AtSPlot widget, as defined by the
latest call to AtSPlotAttachData().

.SH USAGE

The AtSPlot widget should not be instantiated.  Instead, use one of
the subclasses.

.SH "SEE ALSO"
AtPlotter(3X), AtPlot(3X), AtLinePlot(3X), AtBarPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH COPYRIGHT
Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

.\"
.\"  SCCSID = @(#) AtFrameAxis.man (V6.0) Sun Aug 16 13:27:33 1992
.\"
.\"  Copyright (C) 1991 by Burdett, Buckeridge and Young Ltd.
.\"
.TH AtFrameAxis 3X
.SH NAME
.B "AtFrameAxis -- A Cuckoo-like Axis class"
.SH SYNOPSIS
.B #include <X11/At/FrameAxis.h>

.SH DESCRIPTION
The AtFrameAxis class relies on another AtAxisCore widget to calculate
the user-space values.  When the AtPlotter parent requests this
information, the AtFrameAxis widget returns the value from the
attached widget.  This is useful for putting the same axis on two
edges of a graph (which is where the name ``Frame Axis'' comes from)
or for ensuring the same axis values on two associated graphs.

When attached to a widget, the AtFrameAxis widget registers a function
with the XtNrangeCallback of the target axis to track any changes to
its user-space values.  Hence applications should not remove all
callbacks from a widget that is the target of an AtFrameAxis
attachment.

The target widget must not be destroyed until the AtFrameAxis has been
detached.  Specifying NULL as the widget will detach the frame axis
from any existing widget.  It is a fatal error for a frame axis widget
to be an active axis on a graph (i.e. have plots scaled against it)
without being attached to another axis.

This class is only concerned with the user-space details of tic
positions and tic labels.  The superclass AtAxisCore supplied the
logic and resources for actually drawing the axis.

.SH CLASSES
The class pointer is atFrameAxisWidgetClass, and the class name is
AtFrameAxis.  It is a subclass of AtAxisCore and relies on the superclass
code and resources for most of the functionality.

.SH RESOURCES

The AtFrameAxis widget inherits all the resources from the AtAxisCore,
AtPlot and Object widget classes.  In addition, it has the following new
resource:

.TS
center tab(#);
cB s s
lI lI lI lI
l l l l.
AtFrameAxis Resources
Name#Class#Type#Default
.sp
XtNwidget#AtCWidget#XtRWidget#NULL
.TE

.IP XtNwidget
This is the widget from which this AtFrameAxis widget is extracting
data.  The target widget must be from a subclass of AtAxisCore;
another AtFrameAxis widget is acceptable.

.SH PUBLIC ROUTINES

.nf
\fB
void AtFrameAxisAttachAxis(\fIfaw, target\fP)
	AtFrameAxisWidget \fIfaw\fP;
	AtAxisCoreWidget \fItarget\fP;
\fR
.fi
This is equivalent to setting the XtNwidget resource via a call to
XtSetValues().

.SH USAGE

.nf
axis = XtCreateWidget(\fIaxis_name\fP, atFrameAxisWidgetClass, \fIparent\fP,
		      \fIargs\fP, \fInum_args\fP)
	String \fIaxis_name\fP;
	WidgetClass atFrameAxisWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

The parent widget should be of the AtPlotter widget class, or a fatal
error will result.

.SH "SEE ALSO"
AtPlotter(3X), AtAxisCore(3X), AtPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Gregory Bond, Burdett, Buckeridge and Young Ltd. (gnb@bby.oz.au)
wrote the code for version V5-beta.
No changes for version V6.0.

.SH COPYRIGHT

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

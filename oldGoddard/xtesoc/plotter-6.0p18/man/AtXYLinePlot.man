.\"
.\"  SCCSID = @(#) AtXYLinePlot.man (V6.0) Sun Aug 16 13:29:17 1992
.\"
.\"  Copyright (C) 1992 by University of Paderborn
.\"
.TH AtXYLinePlot 3X
.SH NAME
.B "AtXYLinePlot -- XY Line Plots"
.SH SYNOPSIS
\fB
#include <X11/At/XYLinePlot.h>
.nf

/* The line types */
typedef enum {
	AtPlotLINES, AtPlotPOINTS, AtPlotIMPULSES, AtPlotSTEPS,
	AtPlotLINEPOINTS, AtPlotLINEIMPULSES, AtPlotBARS
} AtPlotLineType;

/* The line styles */
typedef enum {
	AtLineSOLID, AtLineDOTTED, AtLineDASHED, AtLineDOTDASHED,
	AtLineDOTTED2, AtLineDOTTED3, AtLineDOTTED4, AtLineDOTTED5,
	AtLineDASHED3, AtLineDASHED4, AtLineDASHED5, AtLineDOTDASHED2
} AtPlotLineStyle;

/* The marker types */
typedef enum {
	AtMarkRECTANGLE, AtMarkPLUS, AtMarkXMARK, AtMarkSTAR, AtMarkDIAMOND,
	AtMarkTRIANGLE1, AtMarkTRIANGLE2, AtMarkTRIANGLE3, AtMarkTRIANGLE4
} AtPlotMarkType;

\fR
.fi
.SH DESCRIPTION

The AtXYLinePlot class allows to plot data lines
using 7 different line types,
12 different line styles and
9 different marker types for the data points.
Possible line types from data type AtPlotLineType
are simple lines,
points only, impulses, steps, lines with points
lines with impulses, and bars.
The possible line styles from type AtPlotLineType are
solid, dotted, dashed and dot-dashed.
For drawing the data points these can be marked
with the marker type from data type AtPlotMarkType.
The AtXYLinePlot class uses the facilities of the AtXYPlot
superclass to access the data to be plotted,
and uses the resources of the AtPlot class to display a single line.

.SH CLASSES
The class pointer is atXYLinePlotWidgetClass,
and the class name is AtXYLinePlot.
It is a subclass of AtXYPlot,
and uses the facilities of AtXYPlot and AtPlot classes.

.SH RESOURCES

The AtXYLinePlot widget inherits all the resources from the
AtXYPlot, AtPlot and Object widget classes.
It defines the following new resources:

.TS
center tab(#);
cB s s
lI lI lI lI
l l l l.
AtXYLinePlot Resources
Name#Class#Type#Default
.sp
XtNplotLineStyle#XtCPlotLineStyle#XtRPlotLineStyle#AtLineSOLID
XtNplotLineType#XtCPlotLineType#XtRPlotLineType#AtPlotLINES
XtNplotMarkType#XtCPlotMarkType#XtRPlotMarkType#AtMarkRECTANGLE
.TE

.IP XtNplotLineStyle
Specifies the line style in which the line will be drawn.
12 different line styles from enumerated type
AtPlotLineStyle are possible.

.IP XtNplotLineType
Specifies the line type of the line.
7 different line types from enumerated type
AtPlotLineType are possible.

.IP XtNplotMarkStyle
Specifies the marker type if a line
is drawn with its data points.
9 different markers from enumerated type
AtPlotMarkType are possible.

.SH PUBLIC ROUTINES
For arrays of simple (X,Y) points the following data types are defined
and are to be attached to the plotter with one procedure for
each data type:

.nf
\fB
typedef struct {
	double x;
	double y;
} AtDoublePoint;

typedef struct {
	float x;
	float y;
} AtFloatPoint;

typedef struct {
	int x;
	int y;
} AtIntPoint;

void AtXYLinePlotAttachDoublePoints(\fIspw, ddata, start, num\fP);
void AtXYLinePlotAttachDoublePoints(\fIspw, fdata, start, num\fP);
void AtXYLinePlotAttachDoublePoints(\fIspw, idata, start, num\fP);
.sp
	AtXYPlotWidget \fIspw\fP;
	AtDoublePoint \fI*ddata\fP;
	AtFloatPoint \fI*fdata\fP;
	AtIntPoint \fI*idata\fP;
.sp

void AtXYLinePlotAttachData(\fIspw, xdata, xtype, xstride, ydata, ytype, ystride, start, num\fP);
void AtXYLinePlotExtendData(\fIspw, num\fP);
.sp
	AtSPlotWidget \fIspw\fP;
	XtPointer \fIxdata, ydata\fP;
	AtDataType \fIxtype, ytype\fP;
	Cardinal \fIxstride, ystride, start, num\fP;
\fR
.fi
.sp
The header file <At/XYLinePlot.h> provides these aliases
for the AtXYPlot member routines.

.SH USAGE

.nf
line = XtCreateWidget(\fIline_name\fP, atXYLinePlotWidgetClass, \fIparent\fP, \fIargs\fP, \fInum_args\fP)
	String \fIline_name\fP;
	WidgetClass atXYLinePlotWidgetClass;
	Widget \fIparent\fP;
	ArgList \fIargs\fP;
	Cardinal \fInum_args\fP;
.fi

The parent widget should be of the AtPlotter widget class,
or a fatal error will result.

.SH "SEE ALSO"
AtPlotter(3X), AtXYPlot(3X), AtPlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Peter Klingebiel,
University of Paderborn,
using some code from the AtLinePlot class.

.SH COPYRIGHT

Copyright 1992 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

.\"
.\"  SCCSID = @(#) AtFontFamily.man (V6.0) Sun Aug 16 13:27:19 1992
.\"
.\"  Copyright (C) 1990 by the Massachusetts Institute of Technology
.\"
.TH AtFontFamily 3X
.SH NAME
.B AtFontFamily -- Font convenience routines

.SH SYNOPSIS
.nf
.nj
\fB
#include <X11/At/FontFamily.h>
.sp
AtFontFamily *AtFontFamilyGet(\fIdisplay, name\fP);
void AtFontFamilyRelease(\fIfamily\fP);
XFontStruct *AtFontFetch(\fIfamily, face, size\fP);
char *AtFontFamilyGetName(\fIfamily\fP);
Display *AtFontFamilyGetDisplay(\fIfamily\fP);
int AtFontBigger(\fIsize\fP);
int AtFontSmaller(\fIsize\fP);
int AtFontPointSize(\fIfamily, size\fP);
int AtFontPixelSize(\fIfamily, size\fP);
int AtFontEmphasize(\fIface\fP);
int AtFontDeemphasize(\fIface\fP);
int AtFontItalicize(\fIface\fP);
int AtFontDeitalicize(\fIface\fP);
int AtFontStringToSize(\fIstring\fP);
	Display *\fIdisplay\fP;
	char *\fIname\fP;
	AtFontFamily *\fIfamily\fP;
	int \fIface,size\fP;
	char \fI*string\fP;
.sp
void AtRegisterFontStyleConverter();
void AtRegisterFontSizeConverter();
\fP
.fi

.SH DESCRIPTION

These functions provide a convenient interface for handling X fonts
in environments that want to use multiple fonts, typefaces or sizes.
An AtFontFamily is a shared resource which encapsulates
all necessary information about the fonts of a single family.
Querying an AtFontFamily with a font face and size returns
a pointer to an XFontStruct.
The table below lists currently supported font families.

Both font faces and sizes are described by integers.
The table below lists symbolic names for legal faces and sizes.

.TS
center tab(#);
cB s s
ceI ceI ceI
ce ce ce.
Supported Font Families, Faces, and Sizes
Families#Faces#Sizes
.sp
"helvetica"#AtFontPLAIN#AtFontSMALLEST
"times"#AtFontBOLD#AtFontSMALL
"new century schoolbook"#AtFontITALIC#AtFontMEDIUM
"charter"#AtFontBOLDITALIC#AtFontNORMAL
"courier"#AtFontBIG
"symbol"#AtFontBIGGEST
.TE

.IP \fBAtFontFamilyGet()\fP
This function returns an AtFontFamily for the family \fIname\fP on the
display \fIdisplay\fP.  An AtFontFamily is an immutable resource, and as
such is sharable.  \fIAtFontFamilyGet()\fP implements a cache -- only
one instance of an AtFontFamily is created for each \fIdisplay, name\fP
pair.  Because of the caching, creating a font family should be treated
as an inexpensive function.  It is appropriate, for example, to call
\fIAtFontFamilyGet()\fP for each instantiation of a widget.

.IP \fBAtFontFamilyRelease()\fP
This function releases \fIfamily\fP.  If this is the last reference to
that AtFontFamily, then all the resources associated with it are freed
up.  No further references should be made to \fIfamily\fP.

.IP \fBAtFontFetch()\fP
This function gets a font of the specified \fIfamily, face,\fP and
\fIsize\fP.  This is an efficient operation -- fonts are not loaded and
queried until first requested, and once requested, the XFontStruct is
cached in the AtFontFamily structure.  If no font in the specified face
or size is available in the font family, \fIAtFontFetch()\fP returns a
font that is as close as possible.

.IP \fBAtFontFamilyGetName()\fP
This function returns the name (a character string) of the \fIfamily\fP.

.IP \fBAtFontFamilyGetDisplay()\fP
This function returns the display on which \fIfamily\fP is defined.

.IP \fBAtFontBigger()\fP
This function returns the font size one bigger than \fIsize\fP.  If
\fIsize\fP is the biggest supported then, it returns \fIsize\fP
unmodified.

.IP \fBAtFontSmaller()\fP
This function returns the font size one smaller than \fIsize\fP.  If
\fIsize\fP is the smallest supported then, it returns \fIsize\fP
unmodified.

.IP \fBAtFontPointSize()\fP
This function returns the size in points of a font of size \fIsize\fP in
font \fIfamily\fP.

.IP \fBAtFontPixelSize()\fP
This function returns the size in pixels of a font of size \fIsize\fP in
font \fIfamily\fP.

.IP "\fBAtFontEmpahsize(), AtFontDeemphasize()\fP"
These functions add and remove the bold font attribute from \fIface\fP.

.IP "\fBAtFontItalicize() , AtFontDeitalicize()\fP"
These functions add and remove the bold font attribute from \fIface\fP.

.IP \fBAtFontStringToSize()\fP
This function will convert any of the strings "smallest", "small",
"medium", "normal", "big" and "biggest" to the corresponding font
size.  In addition, if the string is an integer, it will be converted
into the font size which is closest to that number of points.  If the
string doesn't match any size, or the integer is out of range, it
returns -1.  This function is useful when parsing font sizes from the
X resource database.

.IP \fBAtRegisterFontSizeConverter()\fP
.IP \fBAtRegisterFontStyleConverter()\fP
These two functions register converters with the Xt Intrinsics
necessary for converting Strings to the relevant types.

.SH USAGE

These routines are designed so that font families and sizes can easily
be specified from X resource files.  The intended use is that the user
of the application should specify the font family for the application
(as a general rule, most applications should not mix fonts from
different families) as well as the base font size for the application.
The application is free to change typefaces where appropriate (with the
understanding that italic fonts do not render well on most displays)
and, where appropriate, may use bigger or smaller font sizes than that
specified as the base.  The font size must be adjusted up or down
relative to the user-supplied base -- it should not be specified
absolutely.  When the user does not specify a base font size,
AtFontNORMAL is an appropriate default.
.PP
Widgets that use these font routines should have a AtNfontFamily resource
of resource type String.  They should call \fIAtFontFamilyGet()\fP in
their Initialize methods to create the font family and
\fIAtFontFamilyRelease()\fP in their Destroy method.  Both of these
procedures may also have to be called from the XtSetValues method.  Font
families should be thought of as analogous to GCs, which are (as used in
widgets) also shared resources.

Widgets that use these font routines should also call
\fIAtRegisterFontSizeConverter()\fP and
\fIAtRegisterFontStyleConverter()\fP in their class initialize
routines so the relevant conversions can be made as widgets are
created.

.SH EXAMPLE
.nj
.nf
Display *dpy;
AtFontFamily *ff;
XFontStruct *f;

ff = AtFontFamilyGet(dpy, "new century schoolbook");
f = AtFontFetch(ff,AtFontITALIC, AtFontNORMAL);
.fi

.SH AUTHOR
David Flanagan (MIT Project Athena) wrote the code for version V4.
No changes for versions V5-beta and V6.0.

.SH COPYRIGHT
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

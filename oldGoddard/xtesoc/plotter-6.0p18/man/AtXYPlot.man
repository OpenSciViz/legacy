.\"
.\"  SCCSID = @(#) AtXYPlot.man (V6.0 pl4) Sun Aug 16 13:26:25 1992
.\"
.\"  Copyright (C) 1992 by University of Paderborn
.\"
.TH AtXYPlot 3X
.SH NAME
.B AtXYPlot -- Plot meta-class for XY plots
.SH SYNOPSIS
.B #include <X11/At/XYPlot.h>

.SH DESCRIPTION

The AtXYPlot class is a placeholder for some code and resources
for XY plot classes that need access to application data.
Its main purpose is to provide public functions that applications use
to attach data to the plot widgets,
as well as support routines for the widget subclasses.

.SH CLASSES
The class pointer is atXYPlotWidgetClass,
and the class name is AtXYPlot.
It is a subclass of the AtPlot and Object widget classes.

.SH RESOURCES

The AtXYPlot widget inherits all the resources from the
AtPlot and Object widget classes.
It defines the following new resources:

.TS
center tab(#);
cB s s
lI lI lI lI
l l l l.
AtXYPlot Resources
Name#Class#Type#Default
.sp
XtNxOffset#XtCXOffset#XtRDouble#0.0
XtNyOffset#XtCYOffset#XtRDouble#0.0
.TE

.IP XtNxOffset
Specifies an offset to be added to the data in X direction
when displaying the data. The data are not changed.

.IP XtNyOffset
Specifies an offset to be added to the data in Y direction
when displaying the data. The data are not changed.

.SH PUBLIC FUNCTIONS
.nf
\fB
void AtXYPlotAttachData(\fIspw, xdata, xtype, xstride, ydata, ytype, ystride, start, num\fP);
void AtXYPlotExtendData(\fIspw, num\fP);
.sp
	AtXYPlotWidget \fIspw\fP;
	XtPointer \fIxdata, ydata\fP;
	AtDataType \fIxtype, ytype\fP;
	Cardinal \fIxstride, ystride, start, num\fP;
\fR
.fi
.sp
Most subclasses of AtXYPlot
provide \fB#define\fP'd aliases for these routines.

.IP AtXYPlotAttachData()
This routine is used to attach application data to the subclass plot widget.
The widget expects \fInum\fP data items in an array,
with \fIxstride\fP bytes between the x values,
and \fIystride\fP bytes between the y values.
\fIXdata\fP points to the first x item in that array,
\fIydata\fP points to the first y item in that array,
Each item is of the type described in \fIxtype\fP and \fIytype\fP,
an enumerated type.
Currently, only \fBfloat\fP, \fBdouble\fP and \fBint\fP are supported,
but that is easily extended.
All values are converted to doubles by the atXYPlot routines,
so the rest of the widget set works in doubles only.

The data in the array pointed to by \fIxdata\fP and \fIydata\fR
should remain valid and unchanged as it may be accessed
any time should PostScript output be generated
or should the parent AtPlotter widget resize or change internal layout.

If the application data changes, a new call to AtXYPlotAttachData
\fImust\fP be made (even with the same parameters) so that a new
bounding box will be calculated and the axis scales readjusted if
necessary.

.IP AtXYPlotDataExtended()
This changes the number of datapoints in the array to \fInum\fP.
The existing elements of the array should not have changed.

If some extra data has been added to the end of the valid data
(for instance because the data represents a time series
and the next collection point has just occurred),
then this routine will plot the extra data on the existing graph
with the minimum possible recalculation and redisplay.
Should this not be possible,
(because the new data exceeds existing axis bounds in either X or Y axes)
then the effect is the same as the equivalent call to AtXYPlotAttachData().

.SH PLOTTER ROUTINES
The following routines are mainly intended for use by subclasses of AtXYPlot,
but applications may find it useful.
They may be implemented as a macro in some environments.

.nf
\fB
double AtXYPlotGetXValue(\fIp, i\fP)
double AtXYPlotGetYValue(\fIp, i\fP)
	AtXYPlotWidget \fIp\fP;
	Cardinal \fIi\fP;
\fR
.fi
Get the \fIi\fPth x or y data item for the AtXYPlot widget,
as defined by the latest call to AtXYPlotAttachData().

.SH USAGE

The AtXYPlot widget should not be instantiated.
Instead, use one of the subclasses.

.SH "SEE ALSO"
AtPlotter(3X), AtPlot(3X), AtXYLinePlot(3X)
.br
``Using The AthenaTools Plotter Widget Set''

.SH AUTHORS
Peter Klingebiel,
University of Paderborn,
using some code from the AtSPlot class.

.SH COPYRIGHT
Copyright 1992 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

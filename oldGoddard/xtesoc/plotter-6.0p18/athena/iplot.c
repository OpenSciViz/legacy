/*
 *      iplot.c
 *      Test 9: plotter using the athena widget set
 *
 *      Test plotting 6 xy-polygons
 *
 *      klin, Thu Sep  9 20:31:29 1993
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Paned.h>

#include <X11/At/Plotter.h>
#include <X11/At/XYAxis.h>
#include <X11/At/XYLabelAxis.h>
#include <X11/At/XYPolygonPlot.h>

/*
 *      Fallback ressources
 */

static String fallbacks[] = {
  "*plotter.width: 800",
  "*plotter.height: 400",
  "*plotter.borderWidth: 2",
  "*xaxis.drawOrigin: false",
  "*xaxis.drawGrid: true",
  "*yaxis.drawOrigin: true",
  "*yaxis.drawGrid: true",
  "*pol1.doFill: true",
  "*pol1.doOutline: true",
  "*pol1.screenShade: true",
  "*pol1.shading: gray1",
  "*pol2.doFill: true",
  "*pol2.doOutline: true",
  "*pol2.screenShade: true",
  "*pol2.shading: gray2",
  "*pol3.doFill: true",
  "*pol3.doOutline: true",
  "*pol3.screenShade: true",
  "*pol3.shading: gray3",
  "*pol4.doFill: true",
  "*pol4.doOutline: true",
  "*pol4.screenShade: true",
  "*pol4.shading: gray4",
  "*pol5.doFill: true",
  "*pol5.doOutline: true",
  "*pol5.screenShade: true",
  "*pol5.shading: gray5",
  "*pol6.doFill: true",
  "*pol6.doOutline: true",
  "*pol6.screenShade: true",
  "*pol6.shading: gray6",
  NULL,
};

/*
 *      The plotter widgets
 */

static Widget plotter, xaxis, yaxis;
static Widget pol1, pol2, pol3, pol4, pol5, pol6;

/*
 *      Create the plotter, the axes and the lines
 */

void MakePlotter(parent)
  Widget parent;
{
  Arg args[16];
  double wid, off;
  int n;

  /* Create the plotter */
  n = 0;
  XtSetArg(args[n], XtNtitle, "Plotter Demo I"); n++;
  XtSetArg(args[n], XtNlegendTitle, "Month"); n++;
  XtSetArg(args[n], XtNshowLegend, True); n++;
  plotter = XtCreateManagedWidget("plotter", atPlotterWidgetClass,
				  parent, args, n);

  /* Create the x and y axes */
  n = 0;
  XtSetArg(args[n], XtNlabel, "Test Number"); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  xaxis = XtCreateWidget("xaxis", atXYLabelAxisWidgetClass, plotter, args, n);

  n = 0;
  XtSetArg(args[n], XtNlabel, "Benchstones"); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNvertical, True); n++;
  XtSetArg(args[n], XtNlinTicFormat, "%g"); n++;
  yaxis = XtCreateWidget("yaxis", atXYAxisWidgetClass, plotter, args, n);

  /* Attach the axes */
  n = 0;
  XtSetArg(args[n], XtNxAxis,  xaxis); n++;
  XtSetArg(args[n], XtNyAxis,  yaxis); n++;
  XtSetValues(plotter, args, n);

  /* Create the plots */
  pol1 = XtVaCreateWidget("pol1", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "Feb",
			   NULL);
  pol2 = XtVaCreateWidget("pol2", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "Mar",
			   NULL);
  pol3 = XtVaCreateWidget("pol3", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "Apr",
			   NULL);
  pol4 = XtVaCreateWidget("pol4", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "May",
			   NULL);
  pol5 = XtVaCreateWidget("pol5", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "Jun",
			   NULL);
  pol6 = XtVaCreateWidget("pol6", atXYPolygonPlotWidgetClass, plotter,
			   XtNlegendName, "Jul",
			   NULL);
}

/*
 *      The data
 */

static char *bench[] = {
  "#1", "#2", "#3", "#4", "#5", "#6",
  "#7", "#8", "#9", "#10", "#11", "#12",
};

struct data {
  double x;
  double y1;
  double y2;
  double y3;
  double y4;
  double y5;
  double y6;
  char  *l;
};

#define NNUM 12

static struct data data[NNUM+2];

/*
 *      Create the plot data
 */

void MakeData()
{
  double sin(), cos();
  double x, y;
  int i;

  data[0].x  = 0.0;
  data[0].y1 = data[0].y2 = data[0].y3 = data[0].y4 = data[0].y5 = data[0].y6 = 0.0;
  data[0].l  = NULL;
  for(i = 0, x = 0.0; i < NNUM; i++, x += 1.0) {
    y = sin(x) * cos(x);
    y = Abs(y) + 1.2;
    data[i+1].x  = x;
    data[i+1].y1 = 87 * y;
    data[i+1].y2 = 56 * y;
    data[i+1].y3 = 44 * y;
    data[i+1].y4 = 29 * y;
    data[i+1].y5 = 21 * y;
    data[i+1].y6 = 14 * y;
    data[i+1].l  = bench[i];
  }
  ++i;
  data[i].x  = x;
  data[i].y1 = data[i].y2 = data[i].y3 = data[i].y4 = data[i].y5 = data[i].y6 = 0.0;
  data[i].l  = NULL;
}

/*
 *      Attach the data to the lines
 */

void AttachData()
{
  /* Attach the label data to the xaxis */
  AtXYLabelAxisAttachData(xaxis,
    (XtPointer)&data[1].x, AtDouble, sizeof(struct data),
    (XtPointer)&data[1].l, sizeof(struct data),
    1, NNUM);
  /* Attach data from own defined data type struct data */
  AtXYPolygonPlotAttachData(pol1,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y1, AtDouble, sizeof(struct data),
    1, NNUM+2);
  AtXYPolygonPlotAttachData(pol2,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y2, AtDouble, sizeof(struct data),
    1, NNUM+2);
  AtXYPolygonPlotAttachData(pol3,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y3, AtDouble, sizeof(struct data),
    1, NNUM+2);
  AtXYPolygonPlotAttachData(pol4,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y4, AtDouble, sizeof(struct data),
    1, NNUM+2);
  AtXYPolygonPlotAttachData(pol5,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y5, AtDouble, sizeof(struct data),
    1, NNUM+2);
  AtXYPolygonPlotAttachData(pol6,
    (XtPointer)&data[0].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[0].y6, AtDouble, sizeof(struct data),
    1, NNUM+2);
}


/*
 *      Other widgets
 */

static Widget form;
static Widget quitbutton, printbutton;
static Widget dragbutton;

/*
 *      The button callbacks
 */

#ifndef __STDC__
extern int exit();
#endif

void quit_callback()
{
  exit(0);
}

void print_callback()
{
  AtPlotterGeneratePostscript("iplot.ps", (AtPlotterWidget) plotter,
			      "iplot.ps", 50, 50, 700, 500, True);
  printf("Print callback: Plot dumped to iplot.ps\n");
}

static double oldxmin, oldxmax, oldymin, oldymax;

void get_drag_callback(widget, client_data, call_data)
  Widget widget;
  XtPointer client_data;
  AtRectangleCallbackData *call_data;
{
  Arg args[4];
  static double x1, y1, x2, y2;
  AtTransform tx, ty;

  tx = AtAxisGetTransform(xaxis);
  ty = AtAxisGetTransform(yaxis);
  x1 = call_data->x11;
  x2 = call_data->x12;
  y1 = call_data->y11;
  y2 = call_data->y12;
  XtSetArg(args[0], XtNmin, &x1);
  XtSetArg(args[1], XtNmax, &x2);
  XtSetArg(args[2], XtNautoScale, False);
  XtSetValues(xaxis, args, 3);
  XtSetArg(args[0], XtNmin, &y1);
  XtSetArg(args[1], XtNmax, &y2);
  XtSetArg(args[2], XtNautoScale, False);
  XtSetValues(yaxis, args, 3);
  printf("Get drag callback: tx=%d x1=%lf x2=%lf ty=%d y1=%lf y2=%lf\n",
	  tx, x1, x2, ty, y1, y2);
}

void drag_callback()
{
  Arg args[4];
  int n;
  static int t = 0;

  if(t) {
    XtSetArg(args[0], XtNmin, &oldxmin);
    XtSetArg(args[1], XtNmax, &oldxmax);
    XtSetArg(args[2], XtNautoScale, True);
    XtSetValues(xaxis, args, 3);
    XtSetArg(args[0], XtNmin, &oldymin);
    XtSetArg(args[1], XtNmax, &oldymax);
    XtSetArg(args[2], XtNautoScale, True);
    XtSetValues(yaxis, args, 3);
    XtRemoveAllCallbacks(plotter, XtNdragCallback);
    printf("Drag callback: get drag 1 -> 0\n");
    t = 0;
  }
  else {
    XtVaGetValues(xaxis, XtNmin, &oldxmin, XtNmax, &oldxmax, NULL);
    XtVaGetValues(yaxis, XtNmin, &oldymin, XtNmax, &oldymax, NULL);
    XtAddCallback(plotter, XtNdragCallback, get_drag_callback, NULL);
    printf("Drag callback: get drag 0 -> 1\n");
    t = 1;
  }
}

/*
 *      Create form and buttons
 */

void MakeForm(parent)
  Widget parent;
{
  form = XtVaCreateManagedWidget("form", formWidgetClass, parent, NULL);
  quitbutton = XtVaCreateManagedWidget("quit", commandWidgetClass, form,
					XtNlabel, "Quit",
					XtNleft, XtChainLeft,
					NULL);
  XtAddCallback(quitbutton, XtNcallback, quit_callback, NULL);

  printbutton = XtVaCreateManagedWidget("print", commandWidgetClass, form,
					XtNlabel, "Print",
					XtNfromHoriz, quitbutton,
					NULL);
  XtAddCallback(printbutton, XtNcallback, print_callback, NULL);

  dragbutton = XtVaCreateManagedWidget("drag", commandWidgetClass, form,
				       XtNlabel, "Drag",
				       XtNfromHoriz, printbutton,
				       NULL);
  XtAddCallback(dragbutton, XtNcallback, drag_callback, NULL);
}

/*
 *      Main
 */

main(argc, argv)
  int argc;
  char **argv;
{
  Widget appShell;
  XtAppContext app;

  appShell = XtAppInitialize(&app, "Plotter", NULL, 0, &argc, argv, fallbacks, NULL, 0);

  MakeForm(appShell);

  MakePlotter(form);
  XtVaSetValues(plotter, XtNleft, XtChainLeft, XtNfromVert, quitbutton, NULL);

  MakeData();
  AttachData();

  XtRealizeWidget(appShell);
  XtAppMainLoop(app);

  exit(0);
}

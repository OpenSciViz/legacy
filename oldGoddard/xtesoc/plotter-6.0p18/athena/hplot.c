/*
 *      hplot.c
 *      Test 8: plotter using the athena widget set
 *
 *      Test plotting 3 xy-bars against an xy-label axis
 *
 *      klin, Thu Sep  9 20:31:29 1993
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Paned.h>

#include <X11/At/Plotter.h>
#include <X11/At/XYAxis.h>
#include <X11/At/XYLabelAxis.h>
#include <X11/At/XYFrameAxis.h>
#include <X11/At/XYBarPlot.h>

/*
 *      Fallback ressources
 */

static String fallbacks[] = {
  "*plotter.width: 800",
  "*plotter.height: 400",
  "*plotter.borderWidth: 2",
  "*xaxis.drawOrigin: false",
  "*yaxis.drawOrigin: true",
  "*yaxis.drawGrid: true",
  "*bar1.doFill: true",
  "*bar1.doOutline: true",
  "*bar1.screenShade: true",
  "*bar1.shading: gray6",
  "*bar2.doFill: true",
  "*bar2.doOutline: true",
  "*bar2.screenShade: true",
  "*bar2.shading: gray4",
  "*bar3.doFill: true",
  "*bar3.doOutline: true",
  "*bar3.screenShade: true",
  "*bar3.shading: gray2",
  NULL,
};

/*
 *      The plotter widgets
 */

static Widget plotter, xaxis, faxis, yaxis;
static Widget bar1, bar2, bar3;

/*
 *      Create the plotter, the axes and the lines
 */

void MakePlotter(parent)
  Widget parent;
{
  Arg args[16];
  double wid, off;
  int n;

  /* Create the plotter */
  n = 0;
  XtSetArg(args[n], XtNtitle, "Plotter Demo H"); n++;
  XtSetArg(args[n], XtNlegendTitle, "Salesmen"); n++;
  XtSetArg(args[n], XtNshowLegend, True); n++;
  plotter = XtCreateManagedWidget("plotter", atPlotterWidgetClass,
				  parent, args, n);

  /* Create the x and y axes */
  n = 0;
  XtSetArg(args[n], XtNlabel, "1993"); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNdrawGrid, False); n++;
  xaxis = XtCreateWidget("xaxis", atXYLabelAxisWidgetClass, plotter, args, n);

  n = 0;
  XtSetArg(args[n], XtNlabel, "Volumes"); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNvertical, True); n++;
  XtSetArg(args[n], XtNlinTicFormat, "%g"); n++;
  yaxis = XtCreateWidget("yaxis", atXYAxisWidgetClass, plotter, args, n);

  /* Create the frame axis */
  n = 0;
  XtSetArg(args[n], XtNmirror, True); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNdrawGrid, False); n++;
  XtSetArg(args[n], XtNwidget, xaxis); n++;
  faxis = XtCreateWidget("xaxis", atXYFrameAxisWidgetClass, plotter, args, n);

  /* Attach the axes */
  n = 0;
  XtSetArg(args[n], XtNxAxis,  xaxis); n++;
  XtSetArg(args[n], XtNyAxis,  yaxis); n++;
  XtSetArg(args[n], XtNx2Axis, faxis); n++;
  XtSetValues(plotter, args, n);

  /* Create the plots */
  wid = 0.5;
  off = -0.15;
  bar1  = XtVaCreateWidget("bar1", atXYBarPlotWidgetClass, plotter,
			   XtNlegendName, "Mr. X. X.",
			   XtNcalcWidth, False,
			   XtNbarPosition, AtPositionCENTER,
			   XtNbarWidth, &wid,
			   XtNbarOffset, &off,
			   NULL);
  off = 0.0;
  bar2  = XtVaCreateWidget("bar2", atXYBarPlotWidgetClass, plotter,
			   XtNlegendName, "Mr. Y. Y.",
			   XtNcalcWidth, False,
			   XtNbarPosition, AtPositionCENTER,
			   XtNbarWidth, &wid,
			   XtNbarOffset, &off,
			   NULL);
  off = 0.15;
  bar3  = XtVaCreateWidget("bar3", atXYBarPlotWidgetClass, plotter,
			   XtNlegendName, "Mr. Z. Z.",
			   XtNcalcWidth, False,
			   XtNbarPosition, AtPositionCENTER,
			   XtNbarWidth, &wid,
			   XtNbarOffset, &off,
			   NULL);
}

/*
 *      The data
 */

static char *month[] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

struct data {
     double x;
     double y1;
     double y2;
     double y3;
     char  *l;
};


#define NST  1
#define NSIZ 12
#define NNUM 14

static struct data data[NNUM];

/*
 *      Create the plot data
 */

void MakeData()
{
  double cos();
  double x, y;
  int i;

  for(i = 0, x = -8.0; i < NNUM; i++, x += 1.0) {
    data[i].x = x;
    data[i].l = NULL;
  }
  for(i = NST; i < (NST+NSIZ); i++) {
    y = Abs(cos(data[i].x));
    if (y < 0.0)
      y /= 2.0;
    data[i].y1 = 3.67 * y;
    data[i].y2 = 2.34 * y;
    data[i].y3 = 1.98 * y;
    data[i].l  = XtNewString(month[i-NST]);
  }
}

/*
 *      Attach the data to the lines
 */

void AttachData()
{
  /* Attach the label data to the xaxis */
  AtXYLabelAxisAttachData(xaxis,
    (XtPointer)&data[0].x, AtDouble, sizeof(struct data),
    (XtPointer)&data[0].l, sizeof(struct data),
    1, NNUM);
  /* Attach data from own defined data type struct data */
  AtXYBarPlotAttachData(bar1,
    (XtPointer)&data[NST].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[NST].y1, AtDouble, sizeof(struct data),
    NST+1, NSIZ);
  AtXYBarPlotAttachData(bar2,
    (XtPointer)&data[NST].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[NST].y2, AtDouble, sizeof(struct data),
    NST+1, NSIZ);
  AtXYBarPlotAttachData(bar3,
    (XtPointer)&data[NST].x,  AtDouble, sizeof(struct data),
    (XtPointer)&data[NST].y3, AtDouble, sizeof(struct data),
    NST+1, NSIZ);
}


/*
 *      Other widgets
 */

static Widget form;
static Widget quitbutton, printbutton;
static Widget dragbutton;

/*
 *      The button callbacks
 */

#ifndef __STDC__
extern int exit();
#endif

void quit_callback()
{
  exit(0);
}

void print_callback()
{
  AtPlotterGeneratePostscript("hplot.ps", (AtPlotterWidget) plotter,
			      "hplot.ps", 50, 50, 700, 500, True);
  printf("Print callback: Plot dumped to hplot.ps\n");
}

static double oldxmin, oldxmax, oldymin, oldymax;

void get_drag_callback(widget, client_data, call_data)
  Widget widget;
  XtPointer client_data;
  AtRectangleCallbackData *call_data;
{
  Arg args[4];
  static double x1, y1, x2, y2;
  AtTransform tx, ty;

  tx = AtAxisGetTransform(xaxis);
  ty = AtAxisGetTransform(yaxis);
  x1 = call_data->x11;
  x2 = call_data->x12;
  y1 = call_data->y11;
  y2 = call_data->y12;
  XtSetArg(args[0], XtNmin, &x1);
  XtSetArg(args[1], XtNmax, &x2);
  XtSetArg(args[2], XtNautoScale, False);
  XtSetValues(xaxis, args, 3);
  XtSetArg(args[0], XtNmin, &y1);
  XtSetArg(args[1], XtNmax, &y2);
  XtSetArg(args[2], XtNautoScale, False);
  XtSetValues(yaxis, args, 3);
  printf("Get drag callback: tx=%d x1=%lf x2=%lf ty=%d y1=%lf y2=%lf\n",
	  tx, x1, x2, ty, y1, y2);
}

void drag_callback()
{
  Arg args[4];
  int n;
  static int t = 0;

  if(t) {
    XtSetArg(args[0], XtNmin, &oldxmin);
    XtSetArg(args[1], XtNmax, &oldxmax);
    XtSetArg(args[2], XtNautoScale, True);
    XtSetValues(xaxis, args, 3);
    XtSetArg(args[0], XtNmin, &oldymin);
    XtSetArg(args[1], XtNmax, &oldymax);
    XtSetArg(args[2], XtNautoScale, True);
    XtSetValues(yaxis, args, 3);
    XtRemoveAllCallbacks(plotter, XtNdragCallback);
    printf("Drag callback: get drag 1 -> 0\n");
    t = 0;
  }
  else {
    XtVaGetValues(xaxis, XtNmin, &oldxmin, XtNmax, &oldxmax, NULL);
    XtVaGetValues(yaxis, XtNmin, &oldymin, XtNmax, &oldymax, NULL);
    XtAddCallback(plotter, XtNdragCallback, get_drag_callback, NULL);
    printf("Drag callback: get drag 0 -> 1\n");
    t = 1;
  }
}

/*
 *      Create form and buttons
 */

void MakeForm(parent)
  Widget parent;
{
  form = XtVaCreateManagedWidget("form", formWidgetClass, parent, NULL);
  quitbutton = XtVaCreateManagedWidget("quit", commandWidgetClass, form,
					XtNlabel, "Quit",
					XtNleft, XtChainLeft,
					NULL);
  XtAddCallback(quitbutton, XtNcallback, quit_callback, NULL);

  printbutton = XtVaCreateManagedWidget("print", commandWidgetClass, form,
					XtNlabel, "Print",
					XtNfromHoriz, quitbutton,
					NULL);
  XtAddCallback(printbutton, XtNcallback, print_callback, NULL);

  dragbutton = XtVaCreateManagedWidget("drag", commandWidgetClass, form,
				       XtNlabel, "Drag",
				       XtNfromHoriz, printbutton,
				       NULL);
  XtAddCallback(dragbutton, XtNcallback, drag_callback, NULL);
}

/*
 *      Main
 */

main(argc, argv)
  int argc;
  char **argv;
{
  Widget appShell;
  XtAppContext app;

  appShell = XtAppInitialize(&app, "Plotter", NULL, 0, &argc, argv, fallbacks, NULL, 0);

  MakeForm(appShell);

  MakePlotter(form);
  XtVaSetValues(plotter, XtNleft, XtChainLeft, XtNfromVert, quitbutton, NULL);

  MakeData();
  AttachData();

  XtRealizeWidget(appShell);
  XtAppMainLoop(app);

  exit(0);
}

/*
 *      fplot.c
 *      Test 6: plotter using the athena widget set
 *
 *      Simulate an oscilloscope
 *
 *      klin, Fri Aug 20 13:56:28 1993
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Paned.h>

#include <X11/At/Plotter.h>
#include <X11/At/XYAxis.h>
#include <X11/At/XYLinePlot.h>

#define IVTM 25                 /* Update interval: 25 ms       */

#define DWID 420                /* Plotter width                */
#define DHEI 440                /* Plotter height               */
#define MINW 220                /* Min window width             */
#define MINH 240                /* Min window height            */

#define MIN  -5.0               /* X,Y min                      */
#define MAX   5.0               /* X,Y max                      */
#define TIC   2.5               /* Tic interval                 */
#define NST   4                 /* # of subtics                 */

#define CNUM 50                 /* # of data points             */

/*
 *      The application context, widgets and timer
 */

static XtAppContext appcontext;
static Widget form;
static Widget quitbutton, startbutton, stopbutton, contbutton, stepbutton;
static XtIntervalId timer = 0;

/*
 *      The plotter widgets
 */

static Widget plotter, xaxis, yaxis;
static Widget line0, line1, line2, line3, line4;

/*
 *      The 'dynamic' plot data
 */

typedef struct _curve {
  double x1;
  double x2;
  double y1;
  double y2;
} curve;

static curve data[CNUM];
static int number;

static Boolean running = False;

void SetTimer();

/*
 *      Create the plotter, the axes and the lines
 */

void MakePlotter(parent)
  Widget parent;
{
  Arg args[24];
  double xmin, xmax, ymin, ymax, tic;
  int n;

  /* Create the plotter */
  n = 0;
  XtSetArg(args[n], XtNtitle, "Oscilloscope Demo"); n++;
  XtSetArg(args[n], XtNshowLegend, False); n++;
  XtSetArg(args[n], XtNwidth,  DWID); n++;
  XtSetArg(args[n], XtNheight, DHEI); n++;
  XtSetArg(args[n], XtNusePixmap, True); n++;
  XtSetArg(args[n], XtNuseCursors, False); n++;
  plotter = XtCreateManagedWidget("plotter", atPlotterWidgetClass,
				  parent, args, n);

  /* Create the x and y axes */
  n = 0;
  XtSetArg(args[n], XtNaxisInOrigin, True); n++;
  XtSetArg(args[n], XtNdrawGrid, True); n++;
  XtSetArg(args[n], XtNdrawNumbers, False); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNdrawNumbers, False); n++;
  XtSetArg(args[n], XtNaxisWidth, 1); n++;
  XtSetArg(args[n], XtNlinTicFormat, "%g"); n++;
  tic = TIC;
  XtSetArg(args[n], XtNticInterval, &tic); n++;
  XtSetArg(args[n], XtNautoTics, False); n++;
  XtSetArg(args[n], XtNnumSubtics, NST); n++;
  XtSetArg(args[n], XtNautoSubtics, False); n++;
  xmin = MIN;
  XtSetArg(args[n], XtNmin, &xmin); n++;
  xmax = MAX;
  XtSetArg(args[n], XtNmax, &xmax); n++;
  XtSetArg(args[n], XtNautoScale, False); n++;
  xaxis = XtCreateWidget("xaxis", atXYAxisWidgetClass, plotter, args, n);

  n = 0;
  XtSetArg(args[n], XtNvertical, True); n++;
  XtSetArg(args[n], XtNaxisInOrigin, True); n++;
  XtSetArg(args[n], XtNdrawGrid, True); n++;
  XtSetArg(args[n], XtNdrawNumbers, False); n++;
  XtSetArg(args[n], XtNticsInside, False); n++;
  XtSetArg(args[n], XtNticsOutside, False); n++;
  XtSetArg(args[n], XtNdrawNumbers, False); n++;
  XtSetArg(args[n], XtNaxisWidth, 1); n++;
  XtSetArg(args[n], XtNlinTicFormat, "%g"); n++;
  tic = TIC;
  XtSetArg(args[n], XtNticInterval, &tic); n++;
  XtSetArg(args[n], XtNautoTics, False); n++;
  XtSetArg(args[n], XtNnumSubtics, NST); n++;
  XtSetArg(args[n], XtNautoSubtics, False); n++;
  ymin = MIN;
  XtSetArg(args[n], XtNmin, &ymin); n++;
  ymax = MAX;
  XtSetArg(args[n], XtNmax, &ymax); n++;
  XtSetArg(args[n], XtNautoScale, False); n++;
  yaxis = XtCreateWidget("yaxis", atXYAxisWidgetClass, plotter, args, n);

  /* Attach the axes */
  XtVaSetValues(plotter, XtNxAxis, xaxis, XtNyAxis, yaxis, NULL);

  /* Create the lines */
  line0 = XtVaCreateWidget("line0", atXYLinePlotWidgetClass, plotter, NULL);
  line1 = XtVaCreateWidget("line1", atXYLinePlotWidgetClass, plotter, NULL);
  line2 = XtVaCreateWidget("line2", atXYLinePlotWidgetClass, plotter, NULL);
  line3 = XtVaCreateWidget("line3", atXYLinePlotWidgetClass, plotter, NULL);
  line4 = XtVaCreateWidget("line4", atXYLinePlotWidgetClass, plotter, NULL);
}

/*
 *      Attach the data to the lines
 */

void AttachData(n)
  int n;
{
  AtXYLinePlotAttachData(line0,
			 (XtPointer) &data[0].x1, AtDouble, sizeof(curve),
			 (XtPointer) &data[0].y1, AtDouble, sizeof(curve),
			 1, n);
  AtXYLinePlotAttachData(line1,
			 (XtPointer) &data[0].x1, AtDouble, sizeof(curve),
			 (XtPointer) &data[0].y1, AtDouble, sizeof(curve),
			 1, n);
  AtXYLinePlotAttachData(line2,
			 (XtPointer) &data[0].x1, AtDouble, sizeof(curve),
			 (XtPointer) &data[0].y1, AtDouble, sizeof(curve),
			 1, n);
}

/*
 *      Create the data
 */

void MakeData()
{
  int i;
  double x, dx;

  x  = MIN;
  dx = (MAX - MIN) / CNUM;
  for(i = 0; i < CNUM; i++) {
    data[i].x1 = x;
    x += dx;
  }
}

/*
 *      Extend the data
 */

void ExtendData()
{
  int i0, i1, i2, n0, n1, n2;

  data[number].y1 = data[number].x1 * (double) sin(data[number].x1);
  data[number].y2 = data[number].x1 * (double) cos(data[number].x1);
  data[0].x2 = data[number].x1;
  data[0].y2 = (data[number].y1 + data[number].y2) / 2;
  data[number].y1 = data[0].y2;

  if(++number < CNUM) {
    if(number <= 20) {
      i0 = 0;
      n0 = number;
    }
    else {
      i0 = number - 20;
      n0 = 20;
    }
    if(number <= 20) {
      i1 = 0;
      n1 = number;
    }
    else {
      i1 = number - 10;
      n1 = 10;
    }
    if(number <= 5) {
      i2 = 0;
      n2 = number;
    }
    else {
      i2 = number - 5;
      n2 = 5;

    }
    AtXYLinePlotAttachData(line0,
			   (XtPointer) &data[i0].x1, AtDouble, sizeof(curve),
			   (XtPointer) &data[i0].y1, AtDouble, sizeof(curve),
			   i0, n0);
    AtXYLinePlotAttachData(line1,
			   (XtPointer) &data[i1].x1, AtDouble, sizeof(curve),
			   (XtPointer) &data[i1].y1, AtDouble, sizeof(curve),
			   i1, n1);
    AtXYLinePlotAttachData(line2,
			   (XtPointer) &data[i2].x1, AtDouble, sizeof(curve),
			   (XtPointer) &data[i2].y1, AtDouble, sizeof(curve),
			   i2, n2);
  }
  else {
    AttachData(number = 0);
  }

  AtXYLinePlotAttachData(line3,
			 (XtPointer) &data[0].x2,  AtDouble, sizeof(curve),
			 (XtPointer) &data[0].y2, AtDouble, sizeof(curve),
			 0, 1);
  AtXYLinePlotAttachData(line4,
			 (XtPointer) &data[0].x2,  AtDouble, sizeof(curve),
			 (XtPointer) &data[0].y2, AtDouble, sizeof(curve),
			 0, 1);
}

/*
 *      The callbacks
 */

void quit_callback()
{
  exit(0);
}

void start_callback()
{
  if(timer)
    XtRemoveTimeOut(timer);
  AttachData(number = 0);
  running = True;
  SetTimer();
  XtSetSensitive(stopbutton, True);
  XtSetSensitive(contbutton, False);
  XtSetSensitive(stepbutton, False);
  printf("\nStarted ...\n");
}

void cont_callback()
{
  if(number < CNUM) {
    running = True;
    XtSetSensitive(stopbutton, True);
    XtSetSensitive(contbutton, False);
    XtSetSensitive(stepbutton, False);
    SetTimer();
    printf("\nStarted\n");
  }
  else {
    running = False;
    XtSetSensitive(stopbutton, False);
    XtSetSensitive(contbutton, False);
    XtSetSensitive(stepbutton, False);
    printf("\nAlready fininished. Press the <start> button to restart\n");
  }
}

void stop_callback()
{
  running = False;
  if(number < CNUM) {
    XtSetSensitive(contbutton, True);
    XtSetSensitive(stopbutton, False);
    XtSetSensitive(stepbutton, True);
    printf("\nStopped\n");
  }
  else {
    XtSetSensitive(stopbutton, False);
    XtSetSensitive(contbutton, False);
    XtSetSensitive(stepbutton, False);
    printf("\nAlready fininished. Press the <start> button to restart\n");
  }
}

void step_callback()
{
  ExtendData();
}

/*
 *      Create form and buttons
 */

void MakeForm(parent)
  Widget parent;
{
  form = XtVaCreateManagedWidget("form", formWidgetClass, parent, NULL);
  quitbutton = XtVaCreateManagedWidget("quit", commandWidgetClass, form,
					XtNlabel, "Quit",
					XtNleft, XtChainLeft,
					NULL);
  XtAddCallback(quitbutton, XtNcallback, quit_callback, NULL);

  startbutton = XtVaCreateManagedWidget("start", commandWidgetClass, form,
					XtNlabel, "Start",
					XtNfromHoriz, quitbutton,
					NULL);
  XtAddCallback(startbutton, XtNcallback, start_callback, NULL);

  stopbutton = XtVaCreateManagedWidget("stop", commandWidgetClass, form,
				       XtNlabel, "Stop",
				       XtNfromHoriz, startbutton,
				       NULL);
  XtAddCallback(stopbutton, XtNcallback, stop_callback, NULL);
  XtSetSensitive(stopbutton, False);

  contbutton = XtVaCreateManagedWidget("cont", commandWidgetClass, form,
				       XtNlabel, "Continue",
				       XtNfromHoriz, stopbutton,
				       NULL);
  XtAddCallback(contbutton, XtNcallback, cont_callback, NULL);
  XtSetSensitive(contbutton, False);

  stepbutton = XtVaCreateManagedWidget("step", commandWidgetClass, form,
				       XtNlabel, "Step",
				       XtNfromHoriz, contbutton,
				       NULL);
  XtAddCallback(stepbutton, XtNcallback, step_callback, NULL);
  XtSetSensitive(stepbutton, False);
}

/*
 *      The interval timer routine
 */

void SetTimer()
{
  if(running) {
    ExtendData();
    timer = XtAppAddTimeOut(appcontext, IVTM, SetTimer, NULL);
  }
}


/*
 *      Main
 */

main(argc, argv)
  int argc;
  char **argv;
{
  Widget appshell;

  appshell = XtAppInitialize(&appcontext, "Plotter", NULL, 0, &argc, argv, NULL, NULL, 0);

  MakeForm(appshell);

  MakeData();
  MakePlotter(form);
  XtVaSetValues(plotter, XtNleft, XtChainLeft, XtNfromVert, quitbutton, NULL);

  XtVaSetValues(appshell, XtNminWidth,  MINW, XtNminHeight, MINH, NULL);

  XtRealizeWidget(appshell);

  printf("Press the <start> button to start\n");
  XtAppMainLoop(appcontext);

  exit(0);
}

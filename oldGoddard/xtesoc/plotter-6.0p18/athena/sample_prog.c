/*
 *      A simple sample test program; see the Using.tex document for details.
 *
 *      Copyright 1991 by Burdett, Buckeridge & Young Ltd.
 *
 *      klin, Sat Aug 15 13:39:50 1992, <At/..> changed to <X11/At/...>
 *      klin, Thu Sep  9 20:51:01 1993, Add a LabelAxis
 */

/* First, the various headers required */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>

#include <X11/At/Plotter.h>
#include <X11/At/LinePlot.h>
#include <X11/At/BarPlot.h>
#include <X11/At/Axis.h>
#include <X11/At/LabelAxis.h>

/*
 * The application data - weekly sales data for Widgets.
 */
struct profits {
     /* From database */
     long volume;
     float buy_price;
     float sell_price;
     /* calculated values */
     double sales;
     double cost;
     double profit;
};

#define NUM_YEARS 20

struct profits profits[NUM_YEARS];
char *labels[] = {
  "1980",
  NULL,
  NULL,
  NULL,
  NULL,
  "1985",
  NULL,
  NULL,
  NULL,
  NULL,
  "1990",
  NULL,
  NULL,
  NULL,
  NULL,
  "1995",
  NULL,
  NULL,
  NULL,
  NULL,
  "2000",
  NULL,
  NULL,
};

/*
 * Read from the database - a dummy routine, we'll use random numbers
 */
void ReadFromDatabase()
{
     int i;
     extern int rand();

     for (i = 0; i < NUM_YEARS; i++) {
	  /* Volume between 1000 and 5095 */
	  profits[i].volume = (rand() & 0xfff) - 1000;
	  /* Sell price between $15 and $25 */
	  profits[i].sell_price = (double)(rand() & 0xffffff) /
	       0xffffff * 10.0 + 15.0;
	  /* Buy price between $5 and $15 */
	  profits[i].buy_price = (double)(rand() & 0xffffff) /
	       0xffffff * 10.0 + 5.0;
     }
}

/*
 * Calculate the profit from the above data
 */

void CalculateProfit()
{
     int i;

     for (i = 0; i < NUM_YEARS; i++) {
	  profits[i].sales = profits[i].volume *
	       profits[i].sell_price;
	  profits[i].cost = profits[i].volume * profits[i].buy_price;
	  profits[i].profit = profits[i].sales - profits[i].cost;
     }
}


/*****************************************************************
 *
 * This is the interesting bit.  Make a simple graph, profits on left,
 * volume on right, weeknumber on bottom, profit a line graph, volume
 * a bar graph.
 */

Widget plotter, xaxis, x2axis, yaxis, y2axis, line, bar;

void MakePlotter(parent)
Widget parent;
{
     double thousandth = 1.0e-3;

     plotter = XtVaCreateManagedWidget("plotter", atPlotterWidgetClass, parent,
				       XtNtitle, "@diamonds  Widget Sales & Profits  @diamonds",
				       XtNwidth, 600, XtNheight, 450,
				       NULL);
     /* Note axis objects are unmanaged */
     xaxis = XtVaCreateWidget("xaxis", atLabelAxisWidgetClass, plotter,
			      XtNlabel, "Year",
			      XtNdrawGrid, True,
			      XtNdrawSubgrid, True,
			      NULL);

     /* Note Y axis is vertical */
     yaxis = XtVaCreateWidget("yaxis", atAxisWidgetClass, plotter,
			      XtNvertical, True,
			      XtNlabel, "Profit ($1,000's)",
			      XtNticMultiplier, &thousandth,
			      XtNdrawGrid, True,
			      XtNdrawSubgrid, True,
			      NULL);

     /* Note X2 axis is mirrored */
     x2axis = XtVaCreateWidget("x2axis", atAxisWidgetClass, plotter,
			       XtNlabel, "Week Number",
			       XtNmirror, True,
			       XtNdrawGrid, True,
			       XtNdrawSubgrid, True,
			       XtNticFormat, "%.0f",
			       NULL);

     /* Note Y2 axis is vertical and mirrored */
     y2axis = XtVaCreateWidget("y2axis", atAxisWidgetClass, plotter,
			       XtNvertical, True,
			       XtNmirror, True,
			       XtNlabel, "Volume Shipped (1,000s)",
			       XtNdrawGrid, False,
			       XtNticMultiplier, &thousandth,
			       XtNticFormat, "%4.1f",
			       NULL);

     /* Now attach axes to the parent */
     XtVaSetValues(plotter, XtNxAxis,  xaxis,
			    XtNyAxis,  yaxis,
			    XtNx2Axis, x2axis,
			    XtNy2Axis, y2axis,
			    NULL);

     /* Now create the plots */
     /* The volume is scaled against y2 axis */
     bar = XtVaCreateWidget("vol", atBarPlotWidgetClass, plotter,
			    XtNlegendName, "Volume",
			    XtNdoOutline, True,
			    XtNshading, AtGRAY3, /* For better PS */
			    NULL);
     /* But profit against Y axis */
     line = XtVaCreateWidget("profit", atLinePlotWidgetClass, plotter,
			     XtNlegendName, "Profit",
			     XtNlineWidth, 2,
			     XtNuseX2Axis, True,
			     XtNuseY2Axis, True,
			     NULL);
}

/*****************************************************************
 *
 * Attach the application data to the plot children
 */

void AttachData()
{
     AtLinePlotAttachData(line, (XtPointer)&profits[0].profit,
			  AtDouble, sizeof (struct profits),
			  1, NUM_YEARS);
     AtLabelAxisAttachData(xaxis, (XtPointer) &labels[0],
			   sizeof(char *), 1, NUM_YEARS);

     AtBarPlotAttachData(bar, (XtPointer)&profits[0].volume,
			 AtInt, sizeof (struct profits),
			 1, NUM_YEARS);
}

/*****************************************************************
 *
 * This is the boring bit: a form with two buttons and the plotter as
 * children
 */

Widget form, quitbutton, printbutton;

#ifndef __STDC__
extern int exit();
#endif

void quit_callback()
{
     exit(0);
}

void print_callback()
{
     AtPlotterGeneratePostscript("out.ps", (AtPlotterWidget)plotter,
				 "Widget Profits", 0, 0, 400, 250, False);
     fprintf(stderr, "Plot dumped to out.ps\n");
}

void MakeForm(parent)
Widget parent;
{
     form = XtVaCreateManagedWidget("form", formWidgetClass, parent, NULL);
     quitbutton = XtVaCreateManagedWidget("quit", commandWidgetClass, form,
					  XtNlabel, "Quit",
					  XtNleft, XtChainLeft,
					  NULL);
     XtAddCallback(quitbutton, XtNcallback, quit_callback, NULL);
     printbutton = XtVaCreateManagedWidget("print", commandWidgetClass, form,
					   XtNlabel, "Print",
					   XtNfromHoriz, quitbutton,
					   NULL);
     XtAddCallback(printbutton, XtNcallback, print_callback, NULL);
}


int main(ac, av)
Cardinal ac;
char **av;
{
     Widget appShell;
     XtAppContext app;

     appShell = XtAppInitialize(&app, "Test", NULL, 0, &ac, av,
				NULL, NULL, 0);

     MakeForm(appShell);
     MakePlotter(form);
     /* Make the plotter below the buttons! */
     XtVaSetValues(plotter, XtNleft, XtChainLeft,
		   XtNfromVert, quitbutton,
		   NULL);

     /* Now read the data and calculate it */
     ReadFromDatabase();
     CalculateProfit();

     /* Now attach the data to the plot widget */
     AttachData();

     XtRealizeWidget(appShell);

     XtAppMainLoop(app);
     /*NOTREACHED*/
     return 0;
}

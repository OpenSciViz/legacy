/*
** Dialog.c ( Motifation generated - Version 1.1.0e (14.1.1992) )
**
** @Motifation@allow-overwrite@
** If you want to protect this file to be overwritten by Motifation
** modify the line above.
**
** This file was generated on: Tue Jun 30 16:42:59 1992
*/

#include <stdio.h>
#include <Xm/XmP.h>
#include <X11/Shell.h>
#include <Xm/FileSB.h>

#ifndef XtPointer
#  define XtPointer caddr_t
#endif

/*
** IMPORT: external declarations for the callback-functions
*/
extern Widget mainwindow;

/*
** EXPORT: for external use
*/
void  Popup_dialog();
void  Popdown_dialog();
int   dialog_IsPoppedUp = False;

Widget dialog = NULL;   /* DialogShell-Widget */


/**********************************************************************
**
*/
void Popup_dialog(ok_callback, cancel_callback, title, mask)
   void (*ok_callback)(), (*cancel_callback)();
   char *title, *mask;
{
   extern Display *display;
   extern Screen  *screen;
   Arg            args[19];
   Cardinal       argcount;
   XmString       xms[8];
   Pixmap         pixmap;
   XColor         def, exact;
   XColor         def_foreground, def_background;
   XFontStruct    *xfontstruct;


   if( dialog == NULL ) {

     /*
     ** Codegeneration for the Widget dialog
     */
       argcount = 0;
       XtSetArg( args[argcount], XmNx, (Position) 138 ); argcount++;
       XtSetArg( args[argcount], XmNy, (Position) 372 ); argcount++;
       XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
       XtSetArg( args[argcount], XmNnoResize, False ); argcount++;
       XtSetArg( args[argcount], XmNdefaultPosition, False ); argcount++;
       xms[0] = (XmString) XmStringCreateLtoR( "OK", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNokLabelString, xms[0] ); argcount++;
       xms[1] = (XmString) XmStringCreateLtoR( "Cancel", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNcancelLabelString, xms[1] ); argcount++;
       xms[2] = (XmString) XmStringCreateLtoR( "Files", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNlistLabelString, xms[2] ); argcount++;
       xms[3] = (XmString) XmStringCreateLtoR( ".", XmSTRING_DEFAULT_CHARSET );
       xms[4] = (XmString) XmStringCreateLtoR( "Selection", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNselectionLabelString, xms[4] ); argcount++;
       XtSetArg( args[argcount], XmNallowShellResize, False ); argcount++;
       XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 3 ); argcount++;
       XtSetArg( args[argcount], XmNautoUnmanage, True ); argcount++;
     dialog = XmCreateFileSelectionDialog( mainwindow, "dialog", args, argcount );
       XtUnmanageChild( XmFileSelectionBoxGetChild( dialog, XmDIALOG_HELP_BUTTON ) );
       XtUnmanageChild( XmFileSelectionBoxGetChild( dialog, XmDIALOG_APPLY_BUTTON ) );

     XmStringFree( xms[4] );
     XmStringFree( xms[3] );
     XmStringFree( xms[2] );
     XmStringFree( xms[1] );
     XmStringFree( xms[0] );

   }

   argcount = 0;
   xms[0] = (XmString) XmStringCreateLtoR( mask, XmSTRING_DEFAULT_CHARSET );
   XtSetArg( args[argcount], XmNpattern, xms[0] ); argcount++;
   xms[1] = (XmString) XmStringCreateLtoR(title, XmSTRING_DEFAULT_CHARSET );
   XtSetArg( args[argcount], XmNdialogTitle, xms[1] ); argcount++;
   XtSetValues( dialog, args, argcount);
   XmStringFree( xms[1] );
   XmStringFree( xms[0] );
   XtAddCallback( dialog, XmNokCallback, ok_callback, (XtPointer) NULL );
   XtAddCallback( dialog, XmNcancelCallback, cancel_callback, (XtPointer) NULL );

   XtManageChild( dialog );
   dialog_IsPoppedUp = True;

   argcount = 0;
   XtSetArg( args[argcount], XmNshellUnitType, XmPIXELS ); argcount++;
   XtSetArg( args[argcount], XmNminWidth, 360 ); argcount++;
   XtSetValues( XtParent( dialog ), args, argcount);
}

/**********************************************************************
**
*/
void Popdown_dialog()
{
   if( dialog_IsPoppedUp || ( dialog && XtIsManaged( dialog ) ) ) {
     XtUnmanageChild( dialog );
     dialog_IsPoppedUp = False;
   }
}

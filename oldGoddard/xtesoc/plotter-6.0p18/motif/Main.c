/*
** main.c ( Motifation generated - Version 1.1.0e (14.1.1992) )
**
** @Motifation@allow-overwrite@
** If you want to protect this file to be overwritten by Motifation
** modify the line above.
**
** This file was generated on: Mon Jun 29 15:22:57 1992
*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/IntrinsicP.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>

extern void BuildMainApplication();

Boolean display_demo_lines = False;

Display *     display;
Window        window;
Colormap      colormap;
Screen *      screen;
int           screen_number;
Visual *      visual;
Cardinal      planes;
int           depth;
int           quad_width;
XtAppContext  application_context;


/**********************************************************************
**
**
*/
void main( argc, argv )
   int   argc;
   char *argv[];
{
   XrmDatabase  database;
   XFontStruct *font_struct;
   XrmValue     value;
   char        *dummy;

   /*
   **   initialize Toolkit and set some global variable
   */
   XtToolkitInitialize();
   application_context = XtCreateApplicationContext();

   if( ( display = XtOpenDisplay( application_context, NULL, NULL, "Plotter", NULL, 0, &argc, argv)) == NULL ) {
      fprintf( stderr,"\n%s:  Can't open display\n", argv[0] );
      exit( 1 );
   }
   window = DefaultRootWindow(display);

   if(argc > 1 && strcmp(argv[1], "-demo") == 0)
      display_demo_lines = True;

   screen_number = DefaultScreen( display );
   planes = DisplayPlanes( display, screen_number );
   colormap = DefaultColormap( display, screen_number );
   visual = DefaultVisual( display, screen_number );
   screen = XDefaultScreenOfDisplay( display );
   depth = XDefaultDepth( display, screen_number );

   /*
   ** Get the QUAD_WIDTH
   */
   database = XtDatabase( display );
   if( XrmGetResource( database, XmNfontList, XmCFontList, &dummy, &value )
    && (font_struct = XLoadQueryFont( display, value.addr )) ) {
      quad_width = font_struct->ascent+font_struct->descent;
      XFreeFont( display, font_struct );
   } else {
      if( XrmGetResource( database, XmNfont, XmCFont, &dummy, &value )
       && (font_struct = XLoadQueryFont( display, value.addr )) ) {
	 quad_width = font_struct->ascent+font_struct->descent;
	 XFreeFont( display, font_struct );
      } else {
	 if( font_struct = XLoadQueryFont( display, "Fixed" ) ) {
	    quad_width = font_struct->ascent+font_struct->descent;
	    XFreeFont( display, font_struct );
	 } else {
	    quad_width = 10;
	 }
      }
   }
   XmSetFontUnit( display, quad_width );

   BuildMainApplication( display );

   XtAppMainLoop( application_context );
}

/*
 *      Get and return current pointer position
 */

void GetPointerPosition(x, y)
  int *x, *y;
{
  Window rw, cw;
  int wx, wy, m;

  XQueryPointer(display, window, &rw, &cw, x, y, &wx, &wy, &m);
  *x -= 80;
  *y -= 80;
  if(*x < 0) *x = 8;
  if(*y < 0) *y = 8;
}

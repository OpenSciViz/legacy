/*
** message.c ( Motifation generated - Version 1.1.0e (14.1.1992) )
**
** @Motifation@allow-overwrite@
** If you want to protect this file to be overwritten by Motifation
** modify the line above.
**
** This file was generated on: Mon Jun 29 15:22:58 1992
*/

#include <stdio.h>
#include <Xm/XmP.h>
#include <X11/Shell.h>
#include <Xm/MessageB.h>

#ifndef XtPointer
#  define XtPointer caddr_t
#endif

/*
** IMPORT: external declarations for the callback-functions
*/
extern Widget mainwindow;


/*
** EXPORT: for external use
*/
void  Popup_warning();
void  Popdown_warning();
int   warning_IsPoppedUp = False;

Widget warning = NULL;   /* DialogShell-Widget */


/**********************************************************************
**
*/
void Popup_warning(msg)
   char *msg;
{
   extern Display *display;
   extern Screen  *screen;
   Arg            args[18];
   Cardinal       argcount;
   XmString       xms[5];
   Pixmap         pixmap;
   XColor         def, exact;
   XColor         def_foreground, def_background;
   XFontStruct    *xfontstruct;


   if( warning == NULL ) {

     /*
     ** Codegeneration for the Widget warning
     */
       argcount = 0;
       XtSetArg( args[argcount], XmNx, (Position) 355 ); argcount++;
       XtSetArg( args[argcount], XmNy, (Position) 576 ); argcount++;
       XtSetArg( args[argcount], XmNwidth, (Dimension) 660 ); argcount++;
       XtSetArg( args[argcount], XmNheight, (Dimension) 113 ); argcount++;
       XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
       XtSetArg( args[argcount], XmNnoResize, False ); argcount++;
       xms[0] = (XmString) XmStringCreateLtoR( "OK", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNokLabelString, xms[0] ); argcount++;
       XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 3 ); argcount++;
       xms[1] = (XmString) XmStringCreateLtoR( "Plot Machine Warning", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNdialogTitle, xms[1] ); argcount++;
       XtSetArg( args[argcount], XmNallowShellResize, False ); argcount++;
     warning = XmCreateWarningDialog( mainwindow, "warning", args, argcount );
       XtUnmanageChild( XmMessageBoxGetChild( warning, XmDIALOG_CANCEL_BUTTON ) );
       XtUnmanageChild( XmMessageBoxGetChild( warning, XmDIALOG_HELP_BUTTON ) );
     XmStringFree( xms[1] );
     XmStringFree( xms[0] );


   }

   XtManageChild( warning );
   warning_IsPoppedUp = True;

   argcount = 0;
   xms[0] = (XmString) XmStringCreateLtoR( msg, XmSTRING_DEFAULT_CHARSET );
   XtSetArg( args[argcount], XmNmessageString, xms[0] ); argcount++;
   XtSetValues( warning, args, argcount);
   XmStringFree( xms[0] );
}

/**********************************************************************
**
*/
void Popdown_warning()
{
   if( warning_IsPoppedUp || ( warning && XtIsManaged( warning ) ) ) {
     XtUnmanageChild( warning );
     warning_IsPoppedUp = False;
   }
}

/*
** EXPORT: for external use
*/
void  Popup_message();
void  Popdown_message();
int   message_IsPoppedUp = False;

Widget message = NULL;   /* DialogShell-Widget */


/**********************************************************************
**
*/
void Popup_message(msg)
   char *msg;
{
   extern Display *display;
   extern Screen  *screen;
   Arg            args[18];
   Cardinal       argcount;
   XmString       xms[5];
   Pixmap         pixmap;
   XColor         def, exact;
   XColor         def_foreground, def_background;
   XFontStruct    *xfontstruct;


   if( message == NULL ) {

     /*
     ** Codegeneration for the Widget message
     */
       argcount = 0;
       XtSetArg( args[argcount], XmNx, (Position) 355 ); argcount++;
       XtSetArg( args[argcount], XmNy, (Position) 576 ); argcount++;
       XtSetArg( args[argcount], XmNwidth, (Dimension) 660 ); argcount++;
       XtSetArg( args[argcount], XmNheight, (Dimension) 113 ); argcount++;
       XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
       XtSetArg( args[argcount], XmNnoResize, False ); argcount++;
       xms[0] = (XmString) XmStringCreateLtoR( "OK", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNokLabelString, xms[0] ); argcount++;
       XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 3 ); argcount++;
       xms[1] = (XmString) XmStringCreateLtoR( "Plot Machine Message", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNdialogTitle, xms[1] ); argcount++;
       XtSetArg( args[argcount], XmNallowShellResize, False ); argcount++;
     message = XmCreateMessageDialog( mainwindow, "message", args, argcount );
       XtUnmanageChild( XmMessageBoxGetChild( message, XmDIALOG_CANCEL_BUTTON ) );
       XtUnmanageChild( XmMessageBoxGetChild( message, XmDIALOG_HELP_BUTTON ) );
     XmStringFree( xms[1] );
     XmStringFree( xms[0] );


   }

   XtManageChild( message );
   message_IsPoppedUp = True;

   argcount = 0;
   xms[0] = (XmString) XmStringCreateLtoR( msg, XmSTRING_DEFAULT_CHARSET );
   XtSetArg( args[argcount], XmNmessageString, xms[0] ); argcount++;
   XtSetValues( message, args, argcount);
   XmStringFree( xms[0] );
}

/**********************************************************************
**
*/
void Popdown_message()
{
   if( message_IsPoppedUp || ( message && XtIsManaged( message ) ) ) {
     XtUnmanageChild( message );
     message_IsPoppedUp = False;
   }
}

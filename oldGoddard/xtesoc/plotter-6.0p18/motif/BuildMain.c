/*
** BuildMain.c ( Motifation generated - Version 1.1.0e (14.1.1992) )
**
** @Motifation@dont-allow-overwrite@
** If you want to protect this file to be overwritten by Motifation
** modify the line above.
**
** This file was generated on: Wed Aug 12 12:50:44 1992
*/

#include <stdio.h>
#include <Xm/XmP.h>
#include <X11/Shell.h>
#include <Xm/CascadeB.h>
#include <Xm/ToggleB.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/MainW.h>
#include <Xm/RowColumn.h>
#include <Xm/Text.h>

#include <X11/At/XYLinePlot.h>

#ifndef XtPointer
#  define XtPointer caddr_t
#endif

Widget appshell = NULL;
Widget mainwindow = NULL;
Widget linearx_button = NULL;
Widget lineary_button = NULL;
Widget logarithmicx_button = NULL;
Widget logarithmicy_button = NULL;
Widget grid_button = NULL;
Widget subgrid_button = NULL;
Widget title_label = NULL;
Widget plotter_form = NULL;
Widget plotter_frame = NULL;
Widget xpos_label = NULL;
Widget x_text = NULL;
Widget ypos_label = NULL;
Widget y_text = NULL;
Widget ask_label = NULL;
Widget zoom_label = NULL;
Widget slide_label = NULL;
Widget line_button = NULL;

extern void quit_callback();            /* in file Plot.c */
extern void askposition_callback();     /* in file Plot.c */
extern void getposition_callback();     /* in file Plot.c */
extern void zoom_callback();            /* in file Plot.c */
extern void legend_callback();          /* in file Plot.c */
extern void position_callback();        /* in file Plot.c */
extern void pixmap_callback();          /* in file Plot.c */
extern void read_callback();            /* in file Plot.c */
extern void print_callback();           /* in file Plot.c */
extern void transform_callback();       /* in file Plot.c */
extern void linearx_callback();         /* in file Plot.c */
extern void lineary_callback();         /* in file Plot.c */
extern void logarithmicx_callback();    /* in file Plot.c */
extern void logarithmicy_callback();    /* in file Plot.c */
extern void roundx_callback();          /* in file Plot.c */
extern void roundy_callback();          /* in file Plot.c */
extern void grid_callback();            /* in file Plot.c */
extern void subgrid_callback();         /* in file Plot.c */
extern void width_callback();           /* in file Plot.c */
extern void type_callback();            /* in file Plot.c */
extern void style_callback();           /* in file Plot.c */
extern void mark_callback();            /* in file Plot.c */
extern void offset_callback();         /* in file Plot.c */
extern void version_callback();         /* in file Plot.c */
extern void copyright_callback();       /* in file Plot.c */
extern void author_callback();          /* in file Plot.c */
extern void plotterform_callback();     /* in file Plot.c */

/**********************************************************************
**
**
*/
void BuildMainApplication( display )
   Display *display;
{
   extern Screen *screen;
   Arg args[8];
   Cardinal argcount;
   XmString xms[1];
   Pixmap pixmap;
   XColor def, exact;
   XColor def_foreground, def_background;
   XFontStruct *xfontstruct;

   Widget main_menubar = NULL;
   Widget file_pulldown = NULL;
   Widget quit_button = NULL;
   Widget file_button = NULL;
   Widget plot_pulldown = NULL;
   Widget askposition_button = NULL;
   Widget getposition_button = NULL;
   Widget zoom_button = NULL;
   Widget legend_button = NULL;
   Widget position_button = NULL;
   Widget pixmap_button = NULL;
   Widget print_button = NULL;
   Widget read_button = NULL;
   Widget plot_button = NULL;
   Widget axis_pulldown = NULL;
   Widget transform_pulldown = NULL;
   Widget transform_button = NULL;
   Widget round_button = NULL;
   Widget round_pulldown = NULL;
   Widget roundx_button = NULL;
   Widget roundy_button = NULL;
   Widget axis_button = NULL;
   Widget line_pulldown = NULL;
   Widget width_pulldown = NULL;
   Widget width0_button = NULL;
   Widget width1_button = NULL;
   Widget width2_button = NULL;
   Widget width3_button = NULL;
   Widget width_button = NULL;
   Widget type_pulldown = NULL;
   Widget solid_button = NULL;
   Widget points_button = NULL;
   Widget impulses_button = NULL;
   Widget staircase_button = NULL;
   Widget linepoints_button = NULL;
   Widget lineimpulses_button = NULL;
   Widget bars_button = NULL;
   Widget segments_button = NULL;
   Widget type_button = NULL;
   Widget style_pulldown = NULL;
   Widget linesolid_button = NULL;
   Widget linedotted_button = NULL;
   Widget linedashed_button = NULL;
   Widget linedotdashed_button = NULL;
   Widget linedotted3_button = NULL;
   Widget linedashed4_button = NULL;
   Widget linedotdashed2_button = NULL;
   Widget style_button = NULL;
   Widget mark_pulldown = NULL;
   Widget rectangle_button = NULL;
   Widget plus_button = NULL;
   Widget xmark_button = NULL;
   Widget star_button = NULL;
   Widget circle_button = NULL;
   Widget diamond_button = NULL;
   Widget dot_button = NULL;
   Widget triangle1_button = NULL;
   Widget triangle2_button = NULL;
   Widget triangle3_button = NULL;
   Widget triangle4_button = NULL;
   Widget mark_button = NULL;
   Widget offset_button = NULL;
   Widget help_pulldown = NULL;
   Widget version_button = NULL;
   Widget copyright_button = NULL;
   Widget author_button = NULL;
   Widget help_button = NULL;
   Widget main_form = NULL;
   Widget title_form = NULL;
   Widget title_frame = NULL;
   Widget message_form = NULL;
   Widget message_frame = NULL;
   Widget message_rowcolumn = NULL;

     argcount = 0;
     XtSetArg( args[argcount], XmNshellUnitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNallowShellResize, True ); argcount++;
     XtSetArg( args[argcount], XmNtitle, "Plotter" ); argcount++;
   appshell = XtAppCreateShell( NULL, "Plotter", applicationShellWidgetClass, display, args, argcount );

   /*
   ** Codegeneration for the Widget mainwindow
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   mainwindow = XmCreateMainWindow( appshell, "main_window", args, argcount );
   XtManageChild( mainwindow );

   /*
   ** Codegeneration for the Widget main_menubar
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 3 ); argcount++;
     XtSetArg( args[argcount], XmNspacing, (Dimension) 8 ); argcount++;
   main_menubar = XmCreateMenuBar( mainwindow, "main_menubar", args, argcount );
   XtManageChild( main_menubar );

   /*
   ** Codegeneration for the Widget file_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   file_pulldown = XmCreatePulldownMenu( main_menubar, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget read_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Read data", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   read_button = XmCreateCascadeButton( file_pulldown, "button", args, argcount );
     XtAddCallback( read_button, XmNactivateCallback, read_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( read_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget print_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Write postscript", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   print_button = XmCreateCascadeButton( file_pulldown, "button", args, argcount );
     XtAddCallback( print_button, XmNactivateCallback, print_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( print_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget quit_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Quit", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   quit_button = XmCreateCascadeButton( file_pulldown, "button", args, argcount );
     XtAddCallback( quit_button, XmNactivateCallback, quit_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( quit_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget file_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "File", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, file_pulldown ); argcount++;
   file_button = XmCreateCascadeButton( main_menubar, "button", args, argcount );
   XtManageChild( file_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget plot_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   plot_pulldown = XmCreatePulldownMenu( main_menubar, "pulldown", args, argcount );

   /*
   ** Codegeneration for the Widget askposition_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Request position", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, False ); argcount++;
   askposition_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( askposition_button, XmNvalueChangedCallback, askposition_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( askposition_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget getposition_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Show position", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, False ); argcount++;
   getposition_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( getposition_button, XmNvalueChangedCallback, getposition_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( getposition_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget zoom_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Zoom", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, False ); argcount++;
   zoom_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( zoom_button, XmNvalueChangedCallback, zoom_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( zoom_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget legend_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Show Legend", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, True ); argcount++;
   legend_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( legend_button, XmNvalueChangedCallback, legend_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( legend_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget position_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Legend Left", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, False ); argcount++;
   position_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( position_button, XmNvalueChangedCallback, position_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( position_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget pixmap_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Use Pixmap", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, True ); argcount++;
   pixmap_button = XmCreateToggleButton( plot_pulldown, "button", args, argcount );
     XtAddCallback( pixmap_button, XmNvalueChangedCallback, pixmap_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( pixmap_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget plot_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Plot", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, plot_pulldown ); argcount++;
   plot_button = XmCreateCascadeButton( main_menubar, "button", args, argcount );
   XtManageChild( plot_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget axis_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   axis_pulldown = XmCreatePulldownMenu( main_menubar, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget transform_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   transform_pulldown = XmCreatePulldownMenu( axis_pulldown, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget linearx_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linear X", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   linearx_button = XmCreateCascadeButton( transform_pulldown, "button", args, argcount );
     XtAddCallback( linearx_button, XmNactivateCallback, linearx_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( linearx_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget logarithmicx_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Logarithmic X", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   logarithmicx_button = XmCreateCascadeButton( transform_pulldown, "button", args, argcount );
     XtAddCallback( logarithmicx_button, XmNactivateCallback, logarithmicx_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( logarithmicx_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget lineary_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linear Y", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   lineary_button = XmCreateCascadeButton( transform_pulldown, "button", args, argcount );
     XtAddCallback( lineary_button, XmNactivateCallback, lineary_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( lineary_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget logarithmicy_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Logarithmic Y", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   logarithmicy_button = XmCreateCascadeButton( transform_pulldown, "button", args, argcount );
     XtAddCallback( logarithmicy_button, XmNactivateCallback, logarithmicy_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( logarithmicy_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget transform_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Transform", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, transform_pulldown ); argcount++;
   transform_button = XmCreateCascadeButton( axis_pulldown, "button", args, argcount );
     XtAddCallback( transform_pulldown, XmNmapCallback, transform_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( transform_button );
   XmStringFree( xms[0] );


   /*
   ** Codegeneration for the Widget round_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   round_pulldown = XmCreatePulldownMenu( axis_pulldown, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget roundx_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Round X Endpoints", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, True ); argcount++;
   roundx_button = XmCreateToggleButton( round_pulldown, "button", args, argcount );
     XtAddCallback( roundx_button, XmNvalueChangedCallback, roundx_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( roundx_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget roundy_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Round Y Endpoints", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, True ); argcount++;
   roundy_button = XmCreateToggleButton( round_pulldown, "button", args, argcount );
     XtAddCallback( roundy_button, XmNvalueChangedCallback, roundy_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( roundy_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget round_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Round endpoints", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, round_pulldown ); argcount++;
   round_button = XmCreateCascadeButton( axis_pulldown, "button", args, argcount );
   XtManageChild( round_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget grid_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Show Grid", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, True ); argcount++;
   grid_button = XmCreateToggleButton( axis_pulldown, "button", args, argcount );
     XtAddCallback( grid_button, XmNvalueChangedCallback, grid_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( grid_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget subgrid_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Show Subgrid", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNindicatorType, XmN_OF_MANY ); argcount++;
     XtSetArg( args[argcount], XmNvisibleWhenOff, True ); argcount++;
     XtSetArg( args[argcount], XmNset, False ); argcount++;
   subgrid_button = XmCreateToggleButton( axis_pulldown, "button", args, argcount );
     XtAddCallback( subgrid_button, XmNvalueChangedCallback, subgrid_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( subgrid_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget axis_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Axis", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, axis_pulldown ); argcount++;
   axis_button = XmCreateCascadeButton( main_menubar, "button", args, argcount );
   XtManageChild( axis_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget line_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   line_pulldown = XmCreatePulldownMenu( main_menubar, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget width_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   width_pulldown = XmCreatePulldownMenu( line_pulldown, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget width0_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Width 0", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   width0_button = XmCreateCascadeButton( width_pulldown, "button", args, argcount );
     XtAddCallback( width0_button, XmNactivateCallback, width_callback, (XtPointer) 0 ); /* Plot.c */
   XtManageChild( width0_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget width1_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Width 1", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   width1_button = XmCreateCascadeButton( width_pulldown, "button", args, argcount );
     XtAddCallback( width1_button, XmNactivateCallback, width_callback, (XtPointer) 1 ); /* Plot.c */
   XtManageChild( width1_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget width2_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Width 2", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   width2_button = XmCreateCascadeButton( width_pulldown, "button", args, argcount );
     XtAddCallback( width2_button, XmNactivateCallback, width_callback, (XtPointer) 2 ); /* Plot.c */
   XtManageChild( width2_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget width3_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Width 3", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   width3_button = XmCreateCascadeButton( width_pulldown, "button", args, argcount );
     XtAddCallback( width3_button, XmNactivateCallback, width_callback, (XtPointer) 3 ); /* Plot.c */
   XtManageChild( width3_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget width_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linewidth", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, width_pulldown ); argcount++;
   width_button = XmCreateCascadeButton( line_pulldown, "button", args, argcount );
   XtManageChild( width_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget type_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   type_pulldown = XmCreatePulldownMenu( line_pulldown, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget solid_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Lines", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   solid_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( solid_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotLINES ); /* Plot.c */
   XtManageChild( solid_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget points_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Points", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   points_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( points_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotPOINTS ); /* Plot.c */
   XtManageChild( points_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget impulses_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Impulses", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   impulses_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( impulses_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotIMPULSES ); /* Plot.c */
   XtManageChild( impulses_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget staircase_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Steps", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   staircase_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( staircase_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotSTEPS ); /* Plot.c */
   XtManageChild( staircase_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linepoints_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linepoints", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linepoints_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( linepoints_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotLINEPOINTS ); /* Plot.c */
   XtManageChild( linepoints_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget lineimpulses_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Lineimpulses", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   lineimpulses_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( lineimpulses_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotLINEIMPULSES ); /* Plot.c */
   XtManageChild( lineimpulses_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget bars_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Bars", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   bars_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( bars_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotBARS ); /* Plot.c */
   XtManageChild( bars_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget segments_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Segments", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   segments_button = XmCreateCascadeButton( type_pulldown, "button", args, argcount );
     XtAddCallback( segments_button, XmNactivateCallback, type_callback, (XtPointer) AtPlotSEGMENTS ); /* Plot.c */
   XtManageChild( segments_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget type_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linetype", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, type_pulldown ); argcount++;
   type_button = XmCreateCascadeButton( line_pulldown, "button", args, argcount );
   XtManageChild( type_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget style_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   style_pulldown = XmCreatePulldownMenu( line_pulldown, "pulldown", args, argcount );


   /*
   ** Codegeneration for the Widget linesolid_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Solid", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linesolid_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linesolid_button, XmNactivateCallback, style_callback, (XtPointer) AtLineSOLID ); /* Plot.c */
   XtManageChild( linesolid_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedotted_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dotted", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedotted_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedotted_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDOTTED ); /* Plot.c */
   XtManageChild( linedotted_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedashed_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dashed", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedashed_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedashed_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDASHED ); /* Plot.c */
   XtManageChild( linedashed_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedotdashed_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dotdashed", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedotdashed_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedotdashed_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDOTDASHED ); /* Plot.c */
   XtManageChild( linedotdashed_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedotted3_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dotted3", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedotted3_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedotted3_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDOTTED3 ); /* Plot.c */
   XtManageChild( linedotted3_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedashed4_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dashed4", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedashed4_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedashed4_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDASHED4 ); /* Plot.c */
   XtManageChild( linedashed4_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget linedotdashed2_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dotdashed2", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   linedotdashed2_button = XmCreateCascadeButton( style_pulldown, "button", args, argcount );
     XtAddCallback( linedotdashed2_button, XmNactivateCallback, style_callback, (XtPointer) AtLineDOTDASHED2 ); /* Plot.c */
   XtManageChild( linedotdashed2_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget style_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Linestyle", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, style_pulldown ); argcount++;
   style_button = XmCreateCascadeButton( line_pulldown, "button", args, argcount );
   XtManageChild( style_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget mark_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   mark_pulldown = XmCreatePulldownMenu( line_pulldown, "pulldown", args, argcount );

   /*
   ** Codegeneration for the Widget rectangle_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Rectangle", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   rectangle_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( rectangle_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkRECTANGLE ); /* Plot.c */
   XtManageChild( rectangle_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget plus_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Plus", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   plus_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( plus_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkPLUS ); /* Plot.c */
   XtManageChild( plus_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget xmark_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Xmark", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   xmark_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( xmark_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkXMARK ); /* Plot.c */
   XtManageChild( xmark_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget star_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Star", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   star_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( star_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkSTAR ); /* Plot.c */
   XtManageChild( star_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget circle_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Circle", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   circle_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( circle_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkCIRCLE ); /* Plot.c */
   XtManageChild( circle_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget diamond_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Diamond", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   diamond_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( diamond_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkDIAMOND ); /* Plot.c */
   XtManageChild( diamond_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget dot_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Dot", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   dot_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( dot_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkDOT ); /* Plot.c */
   XtManageChild( dot_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget triangle1_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Triangle1", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   triangle1_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( triangle1_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkTRIANGLE1 ); /* Plot.c */
   XtManageChild( triangle1_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget triangle2_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Triangle2", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   triangle2_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( triangle2_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkTRIANGLE2 ); /* Plot.c */
   XtManageChild( triangle2_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget triangle3_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Triangle3", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   triangle3_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( triangle3_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkTRIANGLE3 ); /* Plot.c */
   XtManageChild( triangle3_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget triangle4_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Triangle4", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   triangle4_button = XmCreateCascadeButton( mark_pulldown, "button", args, argcount );
     XtAddCallback( triangle4_button, XmNactivateCallback, mark_callback, (XtPointer) AtMarkTRIANGLE4 ); /* Plot.c */
   XtManageChild( triangle4_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget mark_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Markertype", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, mark_pulldown ); argcount++;
   mark_button = XmCreateCascadeButton( line_pulldown, "button", args, argcount );
   XtManageChild( mark_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget offset_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Offset", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   offset_button = XmCreateCascadeButton( line_pulldown, "button", args, argcount );
     XtAddCallback( offset_button, XmNactivateCallback, offset_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( offset_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget line_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Line", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, line_pulldown ); argcount++;
   line_button = XmCreateCascadeButton( main_menubar, "button", args, argcount );
   XtManageChild( line_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget help_pulldown
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNunitType, Xm100TH_FONT_UNITS ); argcount++;
   help_pulldown = XmCreatePulldownMenu( main_menubar, "pulldown", args, argcount );

   /*
   ** Codegeneration for the Widget version_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Version", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   version_button = XmCreateCascadeButton( help_pulldown, "button", args, argcount );
     XtAddCallback( version_button, XmNactivateCallback, version_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( version_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget copyright_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Copyright", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   copyright_button = XmCreateCascadeButton( help_pulldown, "button", args, argcount );
     XtAddCallback( copyright_button, XmNactivateCallback, copyright_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( copyright_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget author_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Author", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
   author_button = XmCreateCascadeButton( help_pulldown, "button", args, argcount );
     XtAddCallback( author_button, XmNactivateCallback, author_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( author_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget help_button
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Help", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
     XtSetArg( args[argcount], XmNsubMenuId, help_pulldown ); argcount++;
   help_button = XmCreateCascadeButton( main_menubar, "button", args, argcount );
   XtManageChild( help_button );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget main_form
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNresizePolicy, XmRESIZE_NONE ); argcount++;
   main_form = XmCreateForm( mainwindow, "main_form", args, argcount );
   XtManageChild( main_form );

   /*
   ** Codegeneration for the Widget title_form
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNresizePolicy, XmRESIZE_NONE ); argcount++;
   title_form = XmCreateForm( main_form, "title_form", args, argcount );
   XtManageChild( title_form );

   /*
   ** Codegeneration for the Widget title_frame
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 0 ); argcount++;
     XtSetArg( args[argcount], XmNshadowType, XmSHADOW_OUT ); argcount++;
   title_frame = XmCreateFrame( title_form, "title_frame", args, argcount );
   XtManageChild( title_frame );

   /*
   ** Codegeneration for the Widget title_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Title Label", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
   title_label = XmCreateLabel( title_frame, "title_label", args, argcount );
   XtManageChild( title_label );
   XmStringFree( xms[0] );

   /***** start form-attachments for 'title_form' *****/
   argcount = 0;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetValues( title_frame, args, argcount );

   /***** stop form-attachments for 'title_form' *****/
   /*
   ** Codegeneration for the Widget plotter_form
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNresizePolicy, XmRESIZE_ANY ); argcount++;
   plotter_form = XmCreateForm( main_form, "plotter_form", args, argcount );
     XtAddEventHandler( plotter_form, LeaveWindowMask, False, plotterform_callback, (XtPointer) NULL ); /* Plot.c */
   XtManageChild( plotter_form );

   /*
   ** Codegeneration for the Widget plotter_frame
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 6 ); argcount++;
     XtSetArg( args[argcount], XmNshadowType, XmSHADOW_OUT ); argcount++;
   plotter_frame = XmCreateFrame( plotter_form, "plotter_frame", args, argcount );
   XtManageChild( plotter_frame );

   /***** start form-attachments for 'plotter_form' *****/
   argcount = 0;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetValues( plotter_frame, args, argcount );

   /***** stop form-attachments for 'plotter_form' *****/
   /*
   ** Codegeneration for the Widget message_form
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNresizePolicy, XmRESIZE_NONE ); argcount++;
   message_form = XmCreateForm( main_form, "message_form", args, argcount );
   XtManageChild( message_form );

   /*
   ** Codegeneration for the Widget message_frame
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNshadowType, XmSHADOW_OUT ); argcount++;
     XtSetArg( args[argcount], XmNshadowThickness, (Dimension) 3 ); argcount++;
   message_frame = XmCreateFrame( message_form, "message_frame", args, argcount );
   XtManageChild( message_frame );

   /*
   ** Codegeneration for the Widget message_rowcolumn
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNorientation, XmHORIZONTAL ); argcount++;
   message_rowcolumn = XmCreateRowColumn( message_frame, "message_rowcolumn", args, argcount );
   XtManageChild( message_rowcolumn );

   /*
   ** Codegeneration for the Widget xpos_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "X:", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   xpos_label = XmCreateLabel( message_rowcolumn, "label", args, argcount );
   XtManageChild( xpos_label );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget x_text
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNhighlightThickness, (Dimension) 0 ); argcount++;
     XtSetArg( args[argcount], XmNautoShowCursorPosition, False ); argcount++;
     XtSetArg( args[argcount], XmNeditable, False ); argcount++;
     XtSetArg( args[argcount], XmNpendingDelete, False ); argcount++;
     XtSetArg( args[argcount], XmNcursorPositionVisible, False ); argcount++;
     XtSetArg( args[argcount], XmNverifyBell, False ); argcount++;
   x_text = XmCreateText( message_rowcolumn, "text", args, argcount );
   XtManageChild( x_text );

   /*
   ** Codegeneration for the Widget ypos_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Y:", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   ypos_label = XmCreateLabel( message_rowcolumn, "label", args, argcount );
   XtManageChild( ypos_label );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget y_text
   */
     argcount = 0;
     XtSetArg( args[argcount], XmNautoShowCursorPosition, False ); argcount++;
     XtSetArg( args[argcount], XmNeditable, False ); argcount++;
     XtSetArg( args[argcount], XmNpendingDelete, False ); argcount++;
     XtSetArg( args[argcount], XmNcursorPositionVisible, False ); argcount++;
     XtSetArg( args[argcount], XmNverifyBell, False ); argcount++;
     XtSetArg( args[argcount], XmNhighlightThickness, (Dimension) 0 ); argcount++;
   y_text = XmCreateText( message_rowcolumn, "text", args, argcount );
   XtManageChild( y_text );

   /*
   ** Codegeneration for the Widget ask_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Ask", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   ask_label = XmCreateLabel( message_rowcolumn, "label", args, argcount );
   XtManageChild( ask_label );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget zoom_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Zoom", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   zoom_label = XmCreateLabel( message_rowcolumn, "label", args, argcount );
   XtManageChild( zoom_label );
   XmStringFree( xms[0] );

   /*
   ** Codegeneration for the Widget slide_label
   */
     argcount = 0;
     xms[0] = (XmString) XmStringCreateLtoR( "Slide", XmSTRING_DEFAULT_CHARSET );
     XtSetArg( args[argcount], XmNlabelString, xms[0] ); argcount++;
     XtSetArg( args[argcount], XmNsensitive, False ); argcount++;
   slide_label = XmCreateLabel( message_rowcolumn, "label", args, argcount );
   XtManageChild( slide_label );
   XmStringFree( xms[0] );

   /***** start form-attachments for 'message_form' *****/
   argcount = 0;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetValues( message_frame, args, argcount );

   /***** stop form-attachments for 'message_form' *****/
   /***** start form-attachments for 'main_form' *****/
   argcount = 0;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_NONE ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetValues( title_form, args, argcount );

   argcount = 0;
   XtSetArg( args[argcount], XmNtopWidget, title_form ); argcount++;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_WIDGET ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_WIDGET ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNbottomWidget, message_form ); argcount++;
   XtSetValues( plotter_form, args, argcount );

   argcount = 0;
   XtSetArg( args[argcount], XmNtopAttachment, XmATTACH_NONE ); argcount++;
   XtSetArg( args[argcount], XmNbottomAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNleftAttachment, XmATTACH_FORM ); argcount++;
   XtSetArg( args[argcount], XmNrightAttachment, XmATTACH_FORM ); argcount++;
   XtSetValues( message_form, args, argcount );

   /***** stop form-attachments for 'main_form' *****/

   BuildPlotter();

   argcount = 0;
   XtSetArg( args[argcount], XmNmenuHelpWidget, help_button ); argcount++;
   XtSetValues( main_menubar, args, argcount );

   XmMainWindowSetAreas( mainwindow, main_menubar, NULL, NULL, NULL, main_form );
   XtRealizeWidget( appshell );
}

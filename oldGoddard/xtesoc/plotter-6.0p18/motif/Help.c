/*
** Help.c ( Motifation generated - Version 1.1.0e (14.1.1992) )
**
** @Motifation@allow-overwrite@
** If you want to protect this file to be overwritten by Motifation
** modify the line above.
**
** This file was generated on: Mon Aug  3 16:39:37 1992
*/

#include <stdio.h>
#include <Xm/XmP.h>
#include <X11/Shell.h>
#include <Xm/MessageB.h>

#ifndef XtPointer
#  define XtPointer caddr_t
#endif

/*
** IMPORT: external declarations for the callback-functions
*/
extern Widget mainwindow;


/*
** EXPORT: for external use
*/
void  Popup_help_dialog();
void  Popdown_help_dialog();
int   help_dialog_IsPoppedUp = False;

Widget help_dialog = NULL;   /* DialogShell-Widget */

/**********************************************************************
**
*/
void Popup_help(message)
   char *message;
{
   extern Display *display;
   extern Screen  *screen;
   Arg            args[14];
   Cardinal       argcount;
   XmString       xms[4];
   Pixmap         pixmap;
   XColor         def, exact;
   XColor         def_foreground, def_background;
   XFontStruct    *xfontstruct;


   if( help_dialog == NULL ) {

     /*
     ** Codegeneration for the Widget help_dialog
     */
       argcount = 0;
       XtSetArg( args[argcount], XmNx, (Position) 214 ); argcount++;
       XtSetArg( args[argcount], XmNy, (Position) 441 ); argcount++;
       XtSetArg( args[argcount], XmNwidth, (Dimension) 306 ); argcount++;
       XtSetArg( args[argcount], XmNheight, (Dimension) 102 ); argcount++;
       XtSetArg( args[argcount], XmNunitType, XmPIXELS ); argcount++;
       XtSetArg( args[argcount], XmNnoResize, False ); argcount++;
       XtSetArg( args[argcount], XmNdefaultPosition, False ); argcount++;
       xms[0] = (XmString) XmStringCreateLtoR( "OK", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNokLabelString, xms[0] ); argcount++;
       xms[1] = (XmString) XmStringCreateLtoR("Plotter Demo Help", XmSTRING_DEFAULT_CHARSET );
       XtSetArg( args[argcount], XmNdialogTitle, xms[1] ); argcount++;
       XtSetArg( args[argcount], XmNmessageAlignment, XmALIGNMENT_CENTER ); argcount++;
     help_dialog = XmCreateMessageDialog( mainwindow, "dialog", args, argcount );
       XtUnmanageChild( XmMessageBoxGetChild( help_dialog, XmDIALOG_CANCEL_BUTTON ) );
       XtUnmanageChild( XmMessageBoxGetChild( help_dialog, XmDIALOG_HELP_BUTTON ) );
     XmStringFree( xms[1] );
     XmStringFree( xms[0] );


   }

   argcount = 0;
   xms[0] = (XmString) XmStringCreateLtoR( message, XmSTRING_DEFAULT_CHARSET );
   XtSetArg( args[argcount], XmNmessageString, xms[0] ); argcount++;
   XtSetValues( help_dialog, args, argcount );
   XmStringFree( xms[0] );
   XtManageChild( help_dialog );
   help_dialog_IsPoppedUp = True;

   argcount = 0;
   XtSetArg( args[argcount], XmNshellUnitType, XmPIXELS ); argcount++;
   XtSetValues( XtParent( help_dialog ), args, argcount);
}

/**********************************************************************
**
*/
void Popdown_help()
{
   if( help_dialog_IsPoppedUp || ( help_dialog && XtIsManaged( help_dialog ) ) ) {
     XtUnmanageChild( help_dialog );
     help_dialog_IsPoppedUp = False;
   }
}

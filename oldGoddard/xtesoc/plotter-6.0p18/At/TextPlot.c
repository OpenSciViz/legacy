/*
 *      TextPlot.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Mon Jul 27 14:19:09 1992, patchlevel 2
 *                                      Draw() changed for drawing
 *                                      to a pixmap instead of a window
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Wed Sep  8 09:43:34 1993, patchlevel 8
 *                                      XtNverticalJustify added.
 *                                      Some minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/08  TextPlot.c";

/*

Copyright 1992,1993 by University of Paderborn
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#include <X11/At/PlotterP.h>
#include <X11/At/Scale.h>
#include <X11/At/TextPlotP.h>
#include <X11/At/AtConverters.h>

/* The resources */

static double zero = 0.0;
#define off(field) XtOffset(AtTextPlotWidget, textplot.field)
static XtResource resources[] = {
  {
     XtNlabel, XtCLabel,
     XtRString, sizeof(String),
     off(label), XtRString, (XtPointer) NULL
  },
  {
     XtNfontFamily, XtCFontFamily,
     XtRString, sizeof(String),
     off(font_family), XtRImmediate, (XtPointer) NULL
  },
  {
     XtNfontSize, XtCFontSize,
     XtRFontSize, sizeof(int),
     off(font_size), XtRImmediate, (XtPointer) DFLT_FONTNORMAL
  },
  {
     XtNfontStyle, XtCFontStyle,
     XtRFontStyle, sizeof(int),
     off(font_style), XtRImmediate, (XtPointer) AtFontPLAIN
  },
  {
/*   XtNhorizontalJustify, XtCHorizontalJustify, */
     XtNjustify, XtCJustify,
     XtRAtJustify, sizeof(int),
     off(horizontal_justify), XtRImmediate, (XtPointer) AtTextJUSTIFY_CENTER
  },
  {
     XtNverticalJustify, XtCVerticalJustify,
     XtRAtJustify, sizeof(int),
     off(vertical_justify), XtRImmediate, (XtPointer) AtTextJUSTIFY_BOTTOM
  },
  {
     XtNx, XtCPosition,
     XtRInt, sizeof(int),
     off(x), XtRImmediate, (XtPointer) 0
  },
  {
     XtNy, XtCPosition,
     XtRInt, sizeof(int),
     off(y), XtRImmediate, (XtPointer) 0
  },
  {
     XtNfloatingPosition, XtCFloatingPosition,
     XtRBoolean, sizeof(Boolean),
     off(floating_position), XtRImmediate, (XtPointer) True
  },
  {
     XtNfloatingX, XtCFloatingX,
     XtRDouble, sizeof(double),
     off(floating_x), XtRDouble, (XtPointer) &zero
  },
  {
     XtNfloatingY, XtCFloatingY,
     XtRDouble, sizeof(double),
     off(floating_y), XtRDouble, (XtPointer) &zero
  },
};

#undef off

static void ClassInitialize P(());
static void Initialize P((AtTextPlotWidget, AtTextPlotWidget));
static void Destroy P((AtTextPlotWidget));
static Boolean SetValues P((AtTextPlotWidget, AtTextPlotWidget, AtTextPlotWidget));
static void Draw P((AtPlotWidget, Display *, Drawable, Region, int));
static void Recalc P((AtPlotWidget, AtScale *, AtScale *, int, int));
static void DrawPS P((AtPlotWidget, FILE *, AtScale *, AtScale *));

AtTextPlotClassRec atTextPlotClassRec = {
{ /* core part */
    /* superclass         */    (WidgetClass)&atPlotClassRec,
    /* class_name         */    "AtTextPlot",
    /* widget_size        */    sizeof(AtTextPlotRec),
    /* class_initialize   */    ClassInitialize,
    /* class_part_initialize*/  NULL,
    /* class_inited       */    FALSE,
    /* initialize         */    (XtInitProc) Initialize,
    /* initialize_hook    */    NULL,
    /* pad                */    NULL,
    /* pad                */    NULL,
    /* pad                */    0,
    /* resources          */    resources,
    /* num_resources      */    XtNumber(resources),
    /* xrm_class          */    NULLQUARK,
    /* pad                */    FALSE,
    /* pad                */    FALSE,
    /* pad                */    FALSE,
    /* pad                */    FALSE,
    /* destroy            */    (XtWidgetProc)Destroy,
    /* pad                */    NULL,
    /* pad                */    NULL,
    /* set_values         */    (XtSetValuesFunc) SetValues,
    /* set_values_hook    */    NULL,
    /* pad                */    NULL,
    /* get_values_hook    */    NULL,
    /* pad                */    NULL,
    /* version            */    XtVersion,
    /* callback_offsets   */    NULL,
    /* pad                */    NULL,
    /* pad                */    NULL,
    /* pad                */    NULL,
    /* extension            */  NULL
},
/* AtPlotClassPart initialization */
{
    /* draw         */   Draw,
    /* draw_icon    */   NULL,
    /* draw_ps      */   DrawPS,
    /* draw_icon_ps */   NULL,
    /* recalc       */   Recalc
}
};

WidgetClass atTextPlotWidgetClass = (WidgetClass) &atTextPlotClassRec;

static void ClassInitialize()
{
     AtRegisterFontSizeConverter();
     AtRegisterJustifyConverter();
     AtRegisterDoubleConverter();
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

static void Initialize(request, new)
AtTextPlotWidget request, new;
{
     if (new->textplot.label == NULL)
	  new->textplot.label = XtNewString("");
     else
	  new->textplot.label = XtNewString(new->textplot.label);

     if (!new->textplot.font_family) {
	  /* Inherit it from parent */
	  Widget p = XtParent((Widget)new);

	  XtVaGetValues(p, XtNfontFamily, &new->textplot.font_family, NULL);
     }
     new->textplot.font_family = XtNewString(new->textplot.font_family);

     new->textplot.ff = AtFontFamilyGet(XtDisplay(XtParent((Widget)new)),
					new->textplot.font_family);
     new->textplot.label_text = AtTextCreate(new->textplot.label,
					     new->textplot.ff,
					     new->textplot.font_size,
					     new->textplot.font_style);
     new->textplot.xpercent = new->textplot.ypercent = -1;
     new->textplot.xpos = new->textplot.ypos = -1;
}

static void Destroy(w)
AtTextPlotWidget w;
{
     AtTextDestroy(w->textplot.label_text);
     AtFontFamilyRelease(w->textplot.ff);
     XtFree(w->textplot.label);
     XtFree(w->textplot.font_family);
}

static Boolean SetValues(current, request, new)
AtTextPlotWidget current, request, new;
{
     Boolean string_changed = False;
     Boolean family_changed = False;
     Boolean redraw = False;
#define Changed(field) (new->textplot.field != current->textplot.field)

     if (Changed(font_family)) {
	  XtFree(current->textplot.font_family);
	  AtFontFamilyRelease(new->textplot.ff);
	  new->textplot.font_family = XtNewString(new->textplot.font_family);
	  new->textplot.ff = AtFontFamilyGet (XtDisplay(XtParent((Widget)new)),
					      new->textplot.font_family);
	  family_changed = True;
	  redraw = True;
     }

     if (Changed(label)) {
	  XtFree(current->textplot.label);
	  AtTextDestroy (new->textplot.label_text);
	  new->textplot.label = XtNewString(new->textplot.label);
	  new->textplot.label_text =
	       AtTextCreate(new->textplot.label, new->textplot.ff,
			    new->textplot.font_size, new->textplot.font_size);
	  string_changed = True;
	  redraw = True;
     }

     if (!string_changed && (family_changed || Changed(font_size) ||
	 Changed(font_style))) {
	  AtTextReformat(new->textplot.label_text, new->textplot.ff,
			 new->textplot.font_size, new->textplot.font_style);
	  redraw = True;
     }

     if (Changed(x) || Changed(y)) {
	  /* Force the %ages to be recalculated */
	  new->textplot.xpercent = new->textplot.ypercent = -1;
     }

     if (Changed(x) || Changed(y) || Changed(floating_position) ||
	 Changed(floating_x) || Changed(floating_y)) {
	  AtPlotterRecalcThisPlot((Widget) new);
	  redraw = True;
     }

     if (Changed(horizontal_justify) || Changed(vertical_justify)) {
	  AtPlotterRecalcThisPlot((Widget) new);
	  redraw = True;
     }

     if (redraw)
	  AtPlotterRedrawRequired((Widget)new);

     return False;
#undef Changed
}

static void Recalc(plot, xs, ys, from, to)
AtPlotWidget plot;
AtScale *xs,  *ys;
int from, to;
{
     AtTextPlotPart *p = &((AtTextPlotWidget) plot)->textplot;

     /*
      * A textplot is positioned either at a pair of real coordinates,
      * or at a pair of pixel coordinates. If it is set for pixel
      * coord, it rescales according to the %age from when it was created.
      */

     if (p->floating_position) {
	  p->x = AtScaleUserToPixel(xs, p->floating_x);
	  p->y = AtScaleUserToPixel(ys, p->floating_y);
     }
     if (p->xpercent < 0) {
	  /* Recalc xpercent stuff */
	  p->xpercent = (double)(p->x - AtScaleGetLowPix(xs)) /
			(AtScaleGetHighPix(xs) - AtScaleGetLowPix(xs));
	  p->ypercent = (double)(p->y - AtScaleGetLowPix(ys)) /
			(AtScaleGetHighPix(ys) - AtScaleGetLowPix(ys));
     }
     else if (!p->floating_position) {
	  /* Use the x percent stuff to reposition */
	  p->x = AtScaleGetLowPix(xs) + p->xpercent *
		 (AtScaleGetHighPix(xs) - AtScaleGetLowPix(xs));
	  p->y = AtScaleGetLowPix(ys) + p->xpercent *
		 (AtScaleGetHighPix(ys) - AtScaleGetLowPix(ys));
     }

     switch (p->vertical_justify) {
	  case AtTextJUSTIFY_BOTTOM:
	       p->ypos = p->y;
	       break;
	  case AtTextJUSTIFY_CENTER:
	       p->ypos = p->y + AtTextHeight(p->label_text) / 2;
	       break;
	  case AtTextJUSTIFY_TOP:
	       p->ypos = p->y + AtTextHeight(p->label_text);
	       break;
     }
     switch (p->horizontal_justify) {
	  case AtTextJUSTIFY_LEFT:
	       p->xpos = p->x;
	       break;
	  case AtTextJUSTIFY_CENTER:
	       p->xpos = p->x - AtTextWidth(p->label_text) / 2;
	       break;
	  case AtTextJUSTIFY_RIGHT:
	       p->xpos = p->x - AtTextWidth(p->label_text);
	       break;
     }
}

static void Draw(plot, dpy, drw, region, ref)
AtPlotWidget plot;
Display *dpy;
Drawable drw;
Region region;
int ref;
{
     AtTextPlotPart *p = &((AtTextPlotWidget) plot)->textplot;
     Window win = XtWindow(XtParent((Widget) plot));
     AtPlotterWidget pw = (AtPlotterWidget) XtParent((Widget) plot);
     AtPlotterLayout *pwl = &pw->plotter.layout;
     XRectangle plotrectangle;
     Region plotregion;

     /* Get, create and set the clip region */
     plotrectangle.x      = pwl->x1 + 1;
     plotrectangle.y      = pwl->y1 + 1;
     plotrectangle.width  = pwl->width - 2;
     plotrectangle.height = pwl->height - 2;
     plotregion = XCreateRegion();
     XUnionRectWithRegion(&plotrectangle, plotregion, plotregion);
     XSetRegion(dpy, plot->plot.gc, plotregion);

     AtTextDraw(dpy, win, drw, plot->plot.gc, p->label_text, p->xpos, p->ypos);

     /* Unset and destroy the region */
     XSetClipMask(dpy, plot->plot.gc, None);
     XDestroyRegion(plotregion);
}


static void DrawPS(plot, f, xs, ys)
AtPlotWidget plot;
FILE *f;
AtScale *xs, *ys;
{
     AtTextPlotPart *p = &((AtTextPlotWidget) plot)->textplot;
     int x, y;
     int xmin, xmax, ymin, ymax;

     fprintf(f, "%%%%BeginObject: TextPlot %s\n", XtName((Widget) plot));

     if (p->floating_position) {
	  x = AtScaleUserToPixel(xs, p->floating_x);
	  y = AtScaleUserToPixel(ys, p->floating_y);
     }
     else {
	  xmin = AtScaleGetLowPix(xs);
	  xmax = AtScaleGetHighPix(xs);
	  ymin = AtScaleGetLowPix(ys);
	  ymax = AtScaleGetHighPix(ys);
	  x = xmin + (xmax - xmin) * p->xpercent;
	  y = ymin + (ymax - ymin) * p->ypercent;
     }

     switch (p->vertical_justify) {
	  case AtTextJUSTIFY_CENTER:
	       y -= AtTextPSHeight(p->label_text) / 2;
	       break;
	  case AtTextJUSTIFY_TOP:
	       y -= AtTextPSHeight(p->label_text);
	       break;
     }
     switch (p->horizontal_justify) {
	  case AtTextJUSTIFY_CENTER:
	       x -= AtTextPSWidth(p->label_text) / 2;
	       break;
	  case AtTextJUSTIFY_RIGHT:
	       x -= AtTextPSWidth(p->label_text);
	       break;
     }

     AtTextPSDraw(f, p->label_text, x, y);

     fprintf(f, "%%%%EndObject: TextPlot %s\n", XtName((Widget) plot));
}

/*
 *   Creation function
 */

Widget AtCreateTextPlot(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atTextPlotWidgetClass, parent, arglist, argcount));
}

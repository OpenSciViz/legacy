/*
 *      XYTextPlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 20:25:53 1993
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYTextPlot.h"
 */

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   Currently, only some defines for an XYTextPlot widget
 */

#ifndef _At_XYTextPlot_h
#define _At_XYTextPlot_h

#include <X11/At/TextPlot.h>

#define AtXYTextPlotWidgetClass AtTextPlotWidgetClass
#define atXYTextPlotWidgetClass atTextPlotWidgetClass
#define AtXYTextPlotWidget AtXYTextPlotWidget

#ifndef AtIsXYTextPlot
#define AtIsXYTextPlot(w)  XtIsSubclass(w, atTextPlotWidgetClass)
#endif

#define AtCreateXYTextPlot AtCreateTextPlot

#endif /* _At_XYTextPlot_h */

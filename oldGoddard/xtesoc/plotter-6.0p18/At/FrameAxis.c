/*
 *      FrameAxis.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Fri Aug  7 09:50:42 1992, Cast callback procs to keep
 *                                      ANSI C compilers quiet.
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Some minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/07/23  FrameAxis.c";

/*

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 * The frame axis works by stealing the tic positions and labels from
 * another axes somewhere else in the application.
 */

#include <X11/At/FrameAxisP.h>

static void FrameAxisInitialize P((AtFrameAxisWidget, AtFrameAxisWidget));

static Boolean FrameAxisSetValues P((AtFrameAxisWidget, AtFrameAxisWidget,
				  AtFrameAxisWidget));

static void FrameAxisRangeProc P((AtAxisCoreWidget, double *minp,
				 double *maxp, double *tip,
				 int *nwp));
static void FrameAxisCalcProc P((AtAxisCoreWidget));

/* The resources */

#define off(field) XtOffsetOf(AtFrameAxisRec, frameaxis.field)
static XtResource resources[] = {
  {
     XtNwidget, XtCWidget,
     XtRWidget, sizeof(Widget),
     off(widget), XtRImmediate, (XtPointer) NULL
  }
};
#undef off

AtFrameAxisClassRec atFrameAxisClassRec = {
  { /* core fields */
     /* superclass              */      (WidgetClass) &atAxisCoreClassRec,
     /* class_name              */      "AtFrameAxis",
     /* widget_size             */      sizeof(AtFrameAxisRec),
     /* class_initialize        */      NULL,
     /* class_part_initialize   */      NULL,
     /* class_inited            */      FALSE,
     /* initialize              */      (XtInitProc) FrameAxisInitialize,
     /* initialize_hook         */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      0,
     /* resources               */      resources,
     /* num_resources           */      XtNumber(resources),
     /* xrm_class               */      NULLQUARK,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* destroy                 */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* set_values              */      (XtSetValuesFunc) FrameAxisSetValues,
     /* set_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* get_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* version                 */      XtVersion,
     /* callback_private        */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL
     },
  { /* atPlot fields */
     /* draw                    */      XtInheritDraw,
     /* draw_icon               */      XtInheritDrawIcon,
     /* drawPS                  */      XtInheritDrawPS,
     /* draw_iconPS             */      XtInheritDrawIconPS,
     /* recalc                  */      XtInheritRecalc
  },
  { /* frameAxis fields */
     /* range_proc              */      (AtAxisRangeProc) FrameAxisRangeProc,
     /* calc_proc               */      (AtAxisCalcProc) FrameAxisCalcProc
  }
};

WidgetClass atFrameAxisWidgetClass = (WidgetClass)&atFrameAxisClassRec;

/****************************************************************
 *
 * This callback is installed as the rangeCallback of the target
 * widget to make sure we update if he does.
 */
static void watchCB P((Widget, XtPointer, XtPointer));
static void watchCB(w, client_data, call_data)
Widget w;
XtPointer client_data, call_data;
{
     AtAxisCoreWidget target_widget = (AtAxisCoreWidget)w;
     AtFrameAxisWidget this = (AtFrameAxisWidget) client_data;
     /* AtAxisRangeArgs *rap = (AtAxisRangeArgs *)call_data; */

     assert(XtIsSubclass((Widget)target_widget, atAxisCoreWidgetClass));
     assert(XtIsSubclass((Widget)this, atFrameAxisWidgetClass));
     assert(this->frameaxis.widget == target_widget);

     AtPlotterRescaleRequired((Widget)this);
}

/*
 * If the new widget is of the correct type, attach to it, update the
 * instance record and return True.  Else give a warning and return
 * False.
 */
static Boolean AttachWidget P((AtFrameAxisWidget, AtAxisCoreWidget,
			       AtAxisCoreWidget));
static Boolean AttachWidget(faw, old, new)
AtFrameAxisWidget faw;
AtAxisCoreWidget old, new;
{
     if (new && !XtIsSubclass((Widget)new, atAxisCoreWidgetClass)) {
	  XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget)new)),
		       "AtFrameAxis must watch AtAxisCore widget");
	  return False;
     }

     if (old)
	  XtRemoveCallback((Widget) old, XtNrangeCallback,
			   (XtCallbackProc) watchCB, (XtPointer) faw);
     faw->frameaxis.widget = new;
     if (new)
	  XtAddCallback((Widget) new, XtNrangeCallback,
			(XtCallbackProc) watchCB, (XtPointer) faw);
     return True;
}

/*****************************************************************
 *
 * The core member procs
 */

static void FrameAxisInitialize(req, new)
AtFrameAxisWidget req, new;
{
     if (new->frameaxis.widget) {
	  if (!AttachWidget(new, NULL, new->frameaxis.widget))
	       new->frameaxis.widget = NULL;
     }
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}


static Boolean FrameAxisSetValues(old, req, new)
AtFrameAxisWidget old, req, new;
{
#define Changed(fld)      (old->frameaxis.fld != new->frameaxis.fld)
     if (Changed(widget)) {
	  if (AttachWidget(new, old->frameaxis.widget,
			    new->frameaxis.widget)) {
	       new->axiscore.numbers_changed = True;
	       AtPlotterRescaleRequired((Widget)new);
	  } else
	       new->frameaxis.widget = old->frameaxis.widget;
     }
     return False;
}

/*****************************************************************
 *
 * The FrameAxis member functions
 */

/*
 * The range proc just copies the values from the frame axis...
 */
static void FrameAxisRangeProc(w, minp, maxp, tip, nwp)
AtAxisCoreWidget w;
double *minp, *maxp, *tip;
int *nwp;
{
     AtAxisCorePart *acp;

     if (!((AtFrameAxisWidget)w)->frameaxis.widget) {
	  XtAppError(XtWidgetToApplicationContext(XtParent((Widget)w)),
		       "AtFrameAxis displayed without attached AtAxisCore widget");
     }
     acp = &((AtFrameAxisWidget)w)->frameaxis.widget->axiscore;

     *minp = acp->min;
     *maxp = acp->max;
     *tip = acp->tic_interval;
     *nwp = acp->max_num_width;
}


/*
 * The calc proc just copies the existing entries
 */
static void FrameAxisCalcProc(faw)
AtAxisCoreWidget faw;
{
     AtAxisCorePart *acp = &((AtFrameAxisWidget)faw)->frameaxis.widget->axiscore;
     AtAxisCorePart *fa = &faw->axiscore;
     int i;

     if (!((AtFrameAxisWidget)faw)->frameaxis.widget) {
	  XtAppError(XtWidgetToApplicationContext(XtParent((Widget)faw)),
		     "AtFrameAxis recalculated without attached AtAxisCore widget");
     }

     fa->num_tics = acp->num_tics;
     fa->num_subtics = acp->num_subtics;
     fa->tic_values =
	  (double *)XtMalloc(sizeof (double) * fa->num_tics);
     fa->subtic_values =
	  (double *)XtMalloc(sizeof (double) * fa->num_subtics);
     fa->tic_label_string =
	  (String *)XtMalloc(sizeof (String) * fa->num_tics);

     for (i = 0; i < fa->num_tics; i++) {
	  fa->tic_values[i] = acp->tic_values[i];
	  fa->tic_label_string[i] =
	       XtNewString(acp->tic_label_string[i]);
     }

     for (i = 0; i < fa->num_subtics; i++) {
	  fa->subtic_values[i] = acp->subtic_values[i];
     }
}

/*
 *   The member routine
 */
void AtFrameAxisAttachAxis(tw, dw)
Widget tw;
Widget dw;
{
     AtFrameAxisWidget thisw = (AtFrameAxisWidget) tw;
     AtAxisCoreWidget destw = (AtAxisCoreWidget) dw;

     XtCheckSubclass((Widget)thisw, atFrameAxisWidgetClass,
		     "AtFrameAxisAttachAxis can only work on FrameAxis widgets");
     if (AttachWidget(thisw, thisw->frameaxis.widget, destw)) {
	  thisw->axiscore.numbers_changed = True;
	  AtPlotterRescaleRequired((Widget)thisw);
     }
}

/*
 *   Creation function
 */

Widget AtCreateFrameAxis(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atFrameAxisWidgetClass, parent, arglist, argcount));
}

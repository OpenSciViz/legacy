/*
 *      XYPolygonPlot.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 17:12:39 1993
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYPolygonPlot.c";

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   AtXYPolygonPlot widget. It is a subclass of the XYplot for outlined
 *   and/or filled surfaces.
 */

#include <X11/At/PlotterP.h>
#include <X11/At/AxisCoreP.h>
#include <X11/At/XYPolygonPlotP.h>

/*
 *   Forward declare all the private widgetclass routines
 */

static void Draw P((AtXYPolygonPlotWidget, Display *, Drawable, Region, int));
static void DrawIcon P((AtXYPolygonPlotWidget, Display *, Drawable, int, int, int, int, Region));
static void DrawPS P((AtXYPolygonPlotWidget, FILE *, AtScale *, AtScale *));
static void DrawIconPS P((AtXYPolygonPlotWidget, FILE *, int, int, int, int));
static void Recalc P((AtXYPolygonPlotWidget, AtScale *, AtScale *, int, int));
static void ClassInit P((void));
static void Initialize P((AtXYPolygonPlotWidget, AtXYPolygonPlotWidget));
static void Destroy P((AtXYPolygonPlotWidget));
static Boolean SetValues P((AtXYPolygonPlotWidget, AtXYPolygonPlotWidget, AtXYPolygonPlotWidget));
static void Attach P((AtXYPlotWidget, BoundingBox *, int));

/* The resources */

#define off(field) XtOffsetOf(AtXYPolygonPlotRec, pplot.field)
static XtResource resources[] = {
  {
    XtNdoOutline, XtCDoOutline,
    XtRBoolean, sizeof (Boolean),
    off(do_outline), XtRImmediate, (XtPointer) False
  },
  {
    XtNdoFill, XtCDoFill,
    XtRBoolean, sizeof (Boolean),
    off(do_fill), XtRImmediate, (XtPointer) True
  },
  {
    XtNfillColor, XtCForeground,
    XtRPixel, sizeof (Pixel),
    off(fill_color), XtRString, (XtPointer) XtDefaultForeground
  },
  {
    XtNshading, XtCShading,
    XtRShading, sizeof (AtShading),
    off(shading), XtRImmediate, (XtPointer) AtGRAY0
  },
  {
    XtNscreenShade, XtCScreenShade,
    XtRBoolean, sizeof (Boolean),
    off(screen_shade), XtRImmediate, (XtPointer) True
  },
};
#undef off

AtXYPolygonPlotClassRec atXYPolygonPlotClassRec = {
  { /* core fields */
    /* superclass               */      (WidgetClass) &atXYPlotClassRec,
    /* class_name               */      "AtXYPolygonPlot",
    /* widget_size              */      sizeof(AtXYPolygonPlotRec),
    /* class_initialize         */      (XtProc) ClassInit,
    /* class_part_initialize    */      NULL,
    /* class_inited             */      FALSE,
    /* initialize               */      (XtInitProc) Initialize,
    /* initialize_hook          */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      0,
    /* resources                */      resources,
    /* num_resources            */      XtNumber(resources),
    /* xrm_class                */      NULLQUARK,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* destroy                  */      (XtWidgetProc) Destroy,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* set_values               */      (XtSetValuesFunc) SetValues,
    /* set_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* get_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* version                  */      XtVersion,
    /* callback_private         */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL
  },
  { /* atPlot fields */
    /* draw                     */      (AtPlotDrawProc) Draw,
    /* draw_icon                */      (AtPlotDrawIconProc) DrawIcon,
    /* drawPS                   */      (AtPlotDrawPSProc) DrawPS,
    /* draw_iconPS              */      (AtPlotDrawIconPSProc) DrawIconPS,
    /* recalc                   */      (AtPlotRecalcProc) Recalc
  },
  { /* atXYPlot fields */
    /* attach_data              */      (AtXYPlotAttachProc) Attach,
  },
  { /* atXYPolygonPlot fields */
    /* empty                    */      0
  }
};

WidgetClass atXYPolygonPlotWidgetClass = (WidgetClass)&atXYPolygonPlotClassRec;

/*
 *   The core member procs
 */

static void ClassInit()
{
     AtRegisterShadingConverter();
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

/*
 *   Helper functions for GC management
 */

static void GetPolygonGC P((AtXYPolygonPlotWidget));
static void GetPolygonGC(fw)
AtXYPolygonPlotWidget fw;
{
     XGCValues v;
     int mask = GCForeground | GCBackground;

     v.foreground = fw->pplot.fill_color;
     v.background = fw->plot.background;

     if (fw->pplot.shading != AtGRAY0 && fw->pplot.screen_shade) {
	  v.tile = fw->pplot.shading_pixmap =
	       AtShadingGetPixmap(XtScreenOfObject((Widget) fw),
				  fw->pplot.shading,
				  fw->pplot.fill_color, fw->plot.background);
	  v.fill_style = FillTiled;
	  mask |= GCTile | GCFillStyle;
     } else {
	  mask |= GCFillStyle;
	  v.fill_style = FillSolid;
     }
     fw->pplot.fill_gc = XtGetGC(XtParent((Widget) fw), mask, &v);
}

static void FreePolygonGC P((AtXYPolygonPlotWidget));
static void FreePolygonGC(fw)
AtXYPolygonPlotWidget fw;
{
     if (fw->pplot.shading_pixmap != None)
	  AtShadingReleasePixmap(fw->pplot.shading_pixmap);
     fw->pplot.shading_pixmap = None;
     XtReleaseGC(XtParent((Widget)fw), fw->pplot.fill_gc);
}

/*
 *   Initialize
 */

static void Initialize(req, new)
AtXYPolygonPlotWidget req, new;
{
     if (!new->pplot.do_fill && !new->pplot.do_outline) {
	  XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget)new)),
		       "XYPolygonPlot must have either doFill or doOutline");
	  new->pplot.do_fill = True;
     }
     new->pplot.shading_pixmap = None;    /* Avoid bogus frees */
     GetPolygonGC(new);
}

/*
 *   SetValues
 */

static Boolean SetValues(old, req, new)
AtXYPolygonPlotWidget old, req, new;
{
#define Changed(fld) (old->pplot.fld != new->pplot.fld)
     Boolean redraw = False;

     if (Changed(do_fill) || Changed(do_outline)) {
	  if (!new->pplot.do_fill && !new->pplot.do_outline) {
	       XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget)new)),
			    "XYPolygonPlot must have either doFill or doOutline");
	       new->pplot.do_fill = True;
	  }
	  redraw = True;
     }

     if (Changed(fill_color) || Changed(shading) || Changed(screen_shade)) {
	  FreePolygonGC(new);
	  GetPolygonGC(new);
	  redraw = True;
     }

     if (redraw)
	  AtPlotterRedrawRequired((Widget) new);

     return False;
}
#undef Changed

/*
 *   Destroy
 */

static void Destroy(fw)
AtXYPolygonPlotWidget fw;
{
     FreePolygonGC(fw);
}

/*
 *   These routines are the ones called by the parent plot widget
 */

#define fp ((AtXYPolygonPlotWidget)self)
#define PIX ((XPoint *) fp->lplot.pix)
#define OLDPIX ((XPoint *) fp->lplot.old_pix)
/* NB: PIX is not an lvalue (on some very picky compilers!!!) */

/*
 *   Don't need to adjust the bbox, only to allocate the memory.
 */

static void Attach(self, bbp, extending)
AtXYPlotWidget self;
BoundingBox *bbp;
int extending;
{
     if (extending)
	  fp->lplot.pix = XtRealloc((char *) PIX,
				    fp->lplot.num_points * sizeof (XPoint));
     else
	  fp->lplot.pix = XtMalloc(fp->lplot.num_points * sizeof (XPoint));
}

/*
 *   Draw the bar clipped by the given region.
 */

static void Draw(self, dpy, drw, region, refresh)
AtXYPolygonPlotWidget self;
Display *dpy;
Drawable drw;
Region region;
int refresh;
{
     AtPlotterWidget pw = (AtPlotterWidget) XtParent((Widget) self);
     AtPlotterLayout *pwl = &pw->plotter.layout;
     XRectangle linerectangle, fillrectangle;
     Region lineregion, fillregion;

#ifdef TRACE
     fprintf(stderr, "Draw %d bars\n", fpw->lplot.num_points);
#endif

     if (fp->lplot.num_points < 1)      /* Nothing to draw */
	  return;

     /* Get clip regions for outline and filled bar from the plotter layout */
     linerectangle.x      = fillrectangle.x      = pwl->x1 + 1;
     linerectangle.y      = fillrectangle.y      = pwl->y1 + 1;
     linerectangle.width  = fillrectangle.width  = pwl->width - 2;
     linerectangle.height = fillrectangle.height = pwl->height - 2;

     /* Now create and set the regions */
     lineregion = XCreateRegion();
     fillregion = XCreateRegion();
     XUnionRectWithRegion(&linerectangle, lineregion, lineregion);
     XUnionRectWithRegion(&fillrectangle, fillregion, fillregion);
     XSetRegion(dpy, fp->plot.gc, lineregion);
     XSetRegion(dpy, fp->pplot.fill_gc, fillregion);

     if (fp->lplot.old_pix) {
	  if (fp->plot.fast_update && refresh) {
	       /*
		* We are in fast update mode, doing a refresh and have old
		* pix stuff, so draw them to "erase" the old s first
		*/
	       /* Do in reverse order to below!! */
	       if (fp->pplot.do_outline) {
		    XDrawLines(dpy, drw, fp->plot.gc,
			       (XPoint *) fp->lplot.old_pix,
			       fp->lplot.old_num_points, CoordModeOrigin);
		    XDrawLine(dpy, drw, fp->plot.gc,
			      OLDPIX[0].x, OLDPIX[0].y,
			      OLDPIX[fp->lplot.old_num_points-1].x,
			      OLDPIX[fp->lplot.old_num_points-1].y);

	       }
	       if (fp->pplot.do_fill)
		    XFillPolygon(dpy, drw, fp->pplot.fill_gc,
				 (XPoint *) fp->lplot.old_pix,
				 fp->lplot.old_num_points,
				 Complex, CoordModeOrigin);
	  }
	  XtFree((char *) fp->lplot.old_pix);
	  fp->lplot.old_pix = NULL;
	  fp->lplot.old_num_points = 0;
     }

     /* Do the polygon first, then outline */
     if (fp->pplot.do_fill)
	  XFillPolygon(dpy, drw, fp->pplot.fill_gc, PIX, fp->lplot.num_points,
		       Complex, CoordModeOrigin);
     if (fp->pplot.do_outline) {
	  XDrawLines(dpy, drw, fp->plot.gc, PIX, fp->lplot.num_points,
		     CoordModeOrigin);
	  XDrawLine(dpy, drw, fp->plot.gc, PIX[0].x, PIX[0].y,
		    PIX[fp->lplot.num_points-1].x,
		    PIX[fp->lplot.num_points-1].y);
     }

     /* Unset and destroy the regions */
     XSetClipMask(dpy, fp->plot.gc, None);
     XSetClipMask(dpy, fp->pplot.fill_gc, None);
     XDestroyRegion(lineregion);
     XDestroyRegion(fillregion);
}

/*
 *   Draw the "icon" in the given place.
 */

static void DrawIcon(self, dpy, drw, x1, y1, width, height, region)
AtXYPolygonPlotWidget self;
Display *dpy;
Drawable drw;
int x1, y1, width, height;
Region region;
{
     if (fp->pplot.do_fill) {
	  if (region)
	       XSetRegion(dpy, fp->pplot.fill_gc, region);
	  XFillRectangle(dpy, drw, fp->pplot.fill_gc, x1, y1, width, height);
	  if (region)
	       XSetClipMask(dpy, fp->pplot.fill_gc, None);
     }

     if (fp->pplot.do_outline) {
	  if (region)
	       XSetRegion(dpy, fp->plot.gc, region);
	  XDrawRectangle(dpy, drw, fp->plot.gc, x1, y1, width, height);
	  if (region)
	       XSetClipMask(dpy, fp->plot.gc, None);
     }
}

/*
 *   PostScript stuff
 */

static void DrawPS(self, filep, xs, ys)
AtXYPolygonPlotWidget self;
FILE *filep;
AtScale *xs, *ys;
{
     int i, x0, y0, x, y;
     char *shade;

     fprintf(filep, "%%%%BeginObject: AtXYPolygonPlot %s\nGS\n", XtName((Widget) self));

     AtPlotPSLineStyle(filep, (Widget) fp);

     x0 = AtScaleUserToPixel(xs, AtXYPlotGetXValue((AtXYPlotWidget) fp, 0));
     y0 = AtScaleUserToPixel(ys, AtXYPlotGetYValue((AtXYPlotWidget) fp, 0));
     fprintf(filep, "%d %d M\n", x0, y0);
     for (i = 1; i < fp->lplot.num_points; i++) {
	  x = AtScaleUserToPixel(xs, AtXYPlotGetXValue((AtXYPlotWidget) fp, i));
	  y = AtScaleUserToPixel(ys, AtXYPlotGetYValue((AtXYPlotWidget) fp, i));
	  fprintf(filep, "%d %d L\n", x, y);
     }
     fprintf(filep, "%d %d L\n", x0, y0);

     if (fp->pplot.do_fill) {
	  shade = AtShadingPS(fp->pplot.shading);
	  if (fp->pplot.do_outline)
	       fprintf(filep, "GS %s GR ST", shade);
	  else
	       fprintf(filep, "%s", shade);
     }
     else
	  fprintf(filep, "ST");

     fprintf(filep, "\nGR\n%%%%EndObject: AtXYPolygonPlot %s\n", XtName((Widget) self));
}

static void DrawIconPS(self, filep, x1, y1, width, height)
AtXYPolygonPlotWidget self;
FILE *filep;
int x1, y1, width, height;
{
     char term[50], *shade;

     if (fp->pplot.do_fill) {
	  shade = AtShadingPS(fp->pplot.shading);
	  if (fp->pplot.do_outline)
	       sprintf(term, "GS %s GR ST", shade);
	  else
	       strcpy(term, shade);
     }
     else
	  strcpy(term, "ST");


     fprintf(filep, "GS ");
     AtPlotPSLineStyle(filep, (Widget) fp);

     fprintf(filep, " %d %d M %d 0 RL 0 %d RL %d 0 RL CP %s GR\n",
	     x1, y1, width, -height, -width, term);
}

/*
 *   Recalc the data according to the passed x and y scales
 */

static void Recalc(self, xs, ys, from, to)
AtXYPolygonPlotWidget self;
AtScale *xs, *ys;
int from, to;
{
     double x, y, l;
     int i;

#ifdef TRACE
     fprintf(stderr, " -- Recalc from %d to %d\n", from, to);
#endif
     if (from > to) {
	  from = 0;
	  to = fp->lplot.num_points - 1;
     }
     for (i = from; i <= to; i++) {
	  x = AtXYPlotGetXValue((AtXYPlotWidget) fp, i);
	  y = AtXYPlotGetYValue((AtXYPlotWidget) fp, i);
	  if (xs->transform == AtTransformLOGARITHMIC && x <= 0.0) {
	       l = log10(xs->low) - 5.0;
	       x = pow(10.0, l);
	  }
	  if (ys->transform == AtTransformLOGARITHMIC && y <= 0.0) {
	       l = log10(ys->low) - 5.0;
	       y = pow(10.0, l);
	  }
	  PIX[i].x = AtScaleUserToPixel(xs, x);
	  PIX[i].y = AtScaleUserToPixel(ys, y);
     }
}

/*
 *   Creation function
 */

Widget AtCreateXYPolygonPlot(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atXYPolygonPlotWidgetClass, parent, arglist, argcount));
}

/*
 *      XYBarPlot.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 14:49:55 1993
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYBarPlot.c";

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   AtXYBarPlot widget. It is a subclass of the XYplot derived from
 *   the BarPlot widget class..
 */

#include <X11/At/PlotterP.h>
#include <X11/At/AxisCoreP.h>
#include <X11/At/XYBarPlotP.h>

/*
 *   Forward declare all the private widgetclass routines
 */

static void Draw P((AtXYBarPlotWidget, Display *, Drawable, Region, int));
static void DrawIcon P((AtXYBarPlotWidget, Display *, Drawable, int, int, int, int, Region));
static void DrawPS P((AtXYBarPlotWidget, FILE *, AtScale *, AtScale *));
static void DrawIconPS P((AtXYBarPlotWidget, FILE *, int, int, int, int));
static void Recalc P((AtXYBarPlotWidget, AtScale *, AtScale *, int, int));
static void ClassInit P((void));
static void Initialize P((AtXYBarPlotWidget, AtXYBarPlotWidget));
static void Destroy P((AtXYBarPlotWidget));
static Boolean SetValues P((AtXYBarPlotWidget, AtXYBarPlotWidget, AtXYBarPlotWidget));
static void Attach P((AtXYPlotWidget, BoundingBox *, int));

/* The resources */

static double dflt_offset = 0.0;
static double dflt_width = 1.0;

#define off(field) XtOffsetOf(AtXYBarPlotRec, bplot.field)
static XtResource resources[] = {
  {
    XtNdoOutline, XtCDoOutline,
    XtRBoolean, sizeof (Boolean),
    off(do_outline), XtRImmediate, (XtPointer) False
  },
  {
    XtNdoFill, XtCDoFill,
    XtRBoolean, sizeof (Boolean),
    off(do_fill), XtRImmediate, (XtPointer) True
  },
  {
    XtNfillColor, XtCForeground,
    XtRPixel, sizeof (Pixel),
    off(fill_color), XtRString, (XtPointer) XtDefaultForeground
  },
  {
    XtNshading, XtCShading,
    XtRShading, sizeof (AtShading),
    off(shading), XtRImmediate, (XtPointer) AtGRAY0
  },
  {
    XtNscreenShade, XtCScreenShade,
    XtRBoolean, sizeof (Boolean),
    off(screen_shade), XtRImmediate, (XtPointer) True
  },
  {
    XtNbarPosition, XtCBarPosition,
    XtRBarPosition, sizeof(AtBarPosition),
    off(bar_position), XtRImmediate, (XtPointer) AtPositionRIGHT
  },
  {
    XtNbarOrientation, XtCBarOrientation,
    XtRBarOrientation, sizeof(AtBarOrientation),
    off(bar_orientation), XtRImmediate, (XtPointer) AtOrientVERTICAL
  },
  {
    XtNbarOffset, XtCBarOffset,
    XtRDouble, sizeof(double),
    off(bar_offset), XtRDouble, (XtPointer) &dflt_offset
  },
  {
    XtNcalcWidth, XtCCalcWidth,
    XtRBoolean, sizeof(Boolean),
    off(calc_width), XtRImmediate, (XtPointer) True
  },
  {
    XtNbarWidth, XtCBarWidth,
    XtRDouble, sizeof(double),
    off(bar_width), XtRDouble, (XtPointer) &dflt_width
  },
};
#undef off

AtXYBarPlotClassRec atXYBarPlotClassRec = {
  { /* core fields */
    /* superclass               */      (WidgetClass) &atXYPlotClassRec,
    /* class_name               */      "AtXYBarPlot",
    /* widget_size              */      sizeof(AtXYBarPlotRec),
    /* class_initialize         */      (XtProc) ClassInit,
    /* class_part_initialize    */      NULL,
    /* class_inited             */      FALSE,
    /* initialize               */      (XtInitProc) Initialize,
    /* initialize_hook          */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      0,
    /* resources                */      resources,
    /* num_resources            */      XtNumber(resources),
    /* xrm_class                */      NULLQUARK,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* destroy                  */      (XtWidgetProc) Destroy,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* set_values               */      (XtSetValuesFunc) SetValues,
    /* set_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* get_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* version                  */      XtVersion,
    /* callback_private         */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL
  },
  { /* atPlot fields */
    /* draw                     */      (AtPlotDrawProc) Draw,
    /* draw_icon                */      (AtPlotDrawIconProc) DrawIcon,
    /* drawPS                   */      (AtPlotDrawPSProc) DrawPS,
    /* draw_iconPS              */      (AtPlotDrawIconPSProc) DrawIconPS,
    /* recalc                   */      (AtPlotRecalcProc) Recalc
  },
  { /* atXYPlot fields */
    /* attach_data              */      (AtXYPlotAttachProc) Attach,
  },
  { /* atXYBarPlot fields */
    /* empty                    */      0
  }
};

WidgetClass atXYBarPlotWidgetClass = (WidgetClass)&atXYBarPlotClassRec;

/*
 *   The core member procs
 */

static void ClassInit()
{
     AtRegisterBarPositionConverter();
     AtRegisterBarOrientationConverter();
     AtRegisterShadingConverter();
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

/*
 *   Helper functions for GC management
 */

static void GetFillGC P((AtXYBarPlotWidget));
static void GetFillGC(bw)
AtXYBarPlotWidget bw;
{
     XGCValues v;
     int mask = GCForeground | GCBackground;

     v.foreground = bw->bplot.fill_color;
     v.background = bw->plot.background;

     if (bw->bplot.shading != AtGRAY0 && bw->bplot.screen_shade) {
	  v.tile = bw->bplot.shading_pixmap =
	       AtShadingGetPixmap(XtScreenOfObject((Widget)bw),
				  bw->bplot.shading,
				  bw->bplot.fill_color, bw->plot.background);
	  v.fill_style = FillTiled;
	  mask |= GCTile | GCFillStyle;
     } else {
	  mask |= GCFillStyle;
	  v.fill_style = FillSolid;
     }
     bw->bplot.fill_gc = XtGetGC(XtParent((Widget)bw), mask, &v);
}

static void FreeFillGC P((AtXYBarPlotWidget));
static void FreeFillGC(bw)
AtXYBarPlotWidget bw;
{
     if (bw->bplot.shading_pixmap != None)
	  AtShadingReleasePixmap(bw->bplot.shading_pixmap);
     bw->bplot.shading_pixmap = None;
     XtReleaseGC(XtParent((Widget)bw), bw->bplot.fill_gc);
}

/*
 *   Initialize
 */

static void Initialize(req, new)
AtXYBarPlotWidget req, new;
{
     if (!new->bplot.do_fill && !new->bplot.do_outline) {
	  XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget)new)),
		       "XYBarPlot must have either doFill or doOutline");
	  new->bplot.do_fill = True;
     }
     new->bplot.shading_pixmap = None;     /* Avoid bogus frees */
     GetFillGC(new);
     new->bplot.fill_rectangles = NULL;
}

/*
 *   SetValues
 */

static Boolean SetValues(old, req, new)
AtXYBarPlotWidget old, req, new;
{
#define Changed(fld) (old->bplot.fld != new->bplot.fld)
     Boolean recalc = False;
     Boolean redraw = False;

     if (Changed(do_fill) || Changed(do_outline)) {
	  if (!new->bplot.do_fill && !new->bplot.do_outline) {
	       XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget)new)),
			    "XYBarPlot must have either doFill or doOutline");
	       new->bplot.do_fill = True;
	  }
	  redraw = True;
     }

     if (Changed(fill_color) || Changed(shading) || Changed(screen_shade)) {
	  FreeFillGC(new);
	  GetFillGC(new);
	  redraw = True;
     }

     if (Changed(bar_position) || Changed(bar_orientation) ||
	 Changed(calc_width) || Changed(bar_width) || Changed(bar_offset)) {
	  recalc = True;
     }

     if (redraw)
	  AtPlotterRedrawRequired((Widget) new);
     if (recalc)
	  AtPlotterRecalcThisPlot((Widget) new);

     return False;
}
#undef Changed

/*
 *   Destroy
 */

static void Destroy(bw)
AtXYBarPlotWidget bw;
{
     FreeFillGC(bw);
     XtFree((char *) bw->bplot.fill_rectangles);
}

/*
 *   These routines are the ones called by the parent plot widget
 */

#define bp  (&((AtXYBarPlotWidget)self)->bplot)
#define bpw ((AtXYBarPlotWidget)self)
#define PIX ((XRectangle *) bpw->lplot.pix)
/* NB: PIX is not an lvalue (on some very picky compilers!!!) */

/*
 *   Don't need to adjust the bbox, only to allocate the memory.
 */

static void Attach(self, bbp, extending)
AtXYPlotWidget self;
BoundingBox *bbp;
int extending;
{
     if (extending) {
	  bpw->lplot.pix = XtRealloc((char *) PIX,
				     bpw->lplot.num_points * sizeof (XRectangle));
	  bp->fill_rectangles = (XRectangle *)
	       XtRealloc((char *)bp->fill_rectangles,
			 bpw->lplot.num_points * sizeof (XRectangle));
	  bp->saved_bb.xmax = Max(bp->saved_bb.xmax, bbp->xmax);
	  bp->saved_bb.ymax = Max(bp->saved_bb.ymax, bbp->ymax);
	  bp->saved_bb.xmin = Min(bp->saved_bb.xmin, bbp->xmin);
	  bp->saved_bb.ymin = Min(bp->saved_bb.ymin, bbp->ymin);
     }
     else {
	  bp->saved_bb = *bbp;
	  bpw->lplot.pix = XtMalloc(bpw->lplot.num_points * sizeof (XRectangle));
	  XtFree((char *) bp->fill_rectangles);
	  bp->fill_rectangles = (XRectangle *) XtMalloc(bpw->lplot.num_points *
							sizeof (XRectangle));
     }
}

/*
 *   Recalc the data according to the passed x and y scales
 */

static void Recalc(self, xs, ys, from, to)
AtXYBarPlotWidget self;
AtScale *xs, *ys;
int from, to;
{
     int i;
     int x0pix, y0pix, xpix, ypix, wpix, hpix, oldw;
     double x, y, bw, xn, yn;
     XRectangle *rp;

#ifdef TRACE
     fprintf(stderr, "XYBarPlotRecalc from %d to %d\n", from, to);
#endif
     if (from > to) {
	  from = 0;
	  to = bpw->lplot.num_points - 1;
     }

     if (bp->bar_orientation == AtOrientVERTICAL) {
	  if (AtScaleGetLow(ys) < 0 && AtScaleGetHigh(ys) > 0)
	       y0pix = AtScaleUserToPixel(ys, 0.0);
	  else if (AtScaleGetLow(ys) < 0)
	       y0pix = AtScaleGetHighPix(ys);
	  else
	       y0pix = AtScaleGetLowPix(ys);
	  x0pix = AtScaleUserToPixel(xs, 0.0);
     }
     else {
	  if (AtScaleGetLow(xs) < 0 && AtScaleGetHigh(xs) > 0)
	       x0pix = AtScaleUserToPixel(xs, 0.0);
	  else if (AtScaleGetLow(xs) < 0)
	       x0pix = AtScaleGetHighPix(xs);
	  else
	       x0pix = AtScaleGetLowPix(xs);
	  y0pix = AtScaleUserToPixel(ys, 0.0);
     }
     if ((bw = bp->bar_width) < 0.0)
	  bw = -bw;

     rp  = &bp->fill_rectangles[from];

     /* Bar width is pre-defined */
     if (!bp->calc_width) {
	  if (bp->bar_orientation == AtOrientVERTICAL)
	       wpix = AtScaleUserToPixel(xs, bw) - x0pix;
	  else
	       hpix = y0pix - AtScaleUserToPixel(ys, bw);
     }

     /* Calc and set the bars */
     for (i = from; i <= to; i++, rp++) {
	  x = AtXYPlotGetXValue((AtXYPlotWidget) bpw, i);
	  y = AtXYPlotGetYValue((AtXYPlotWidget) bpw, i);
	  /* Calc the width of the bar and position to the right */
	  if (bp->calc_width) {
	       xpix = AtScaleUserToPixel(xs, x);
	       ypix = AtScaleUserToPixel(ys, y);
	       if (bp->bar_orientation == AtOrientVERTICAL) {
		    if (i < to) {
			 xn   = AtXYPlotGetXValue((AtXYPlotWidget) bpw, i + 1);
			 wpix = AtScaleUserToPixel(xs, xn) - xpix;
		    }
		    else {
			 wpix = AtScaleUserToPixel(xs, bw) - x0pix;
		    }
		    hpix = ypix > y0pix ? ypix - y0pix : y0pix - ypix;
		    if (ypix > y0pix)
			 ypix = y0pix;
	       }
	       else {
		    if (i < to) {
			 yn   = AtXYPlotGetYValue((AtXYPlotWidget) bpw, i + 1);
			 hpix = ypix - AtScaleUserToPixel(ys, yn);
		    }
		    else {
			 hpix = y0pix - AtScaleUserToPixel(ys, bw);
		    }
		    ypix -= hpix;
		    wpix = xpix > x0pix ? xpix - x0pix : x0pix - xpix;
		    if (xpix > x0pix)
			 xpix = x0pix;
	       }
	  }
	  /* Bar width is given. Calc positions of the bar */
	  else {
	       if (bp->bar_orientation == AtOrientVERTICAL) {
		    x += bp->bar_offset;
		    /* Calc x1 according to bar position */
		    switch (bp->bar_position) {
			 case AtPositionLEFT:
			      xpix = AtScaleUserToPixel(xs, x - bw);
			      break;
			 case AtPositionCENTER:
			      xpix = AtScaleUserToPixel(xs, x - bw / 2);
			      break;
			 case AtPositionRIGHT:
			 default:
			      xpix = AtScaleUserToPixel(xs, x);
			      break;
		    }
		    ypix = AtScaleUserToPixel(ys, y);
		    hpix = ypix > y0pix ? ypix - y0pix : y0pix - ypix;
		    if (ypix > y0pix)
			 ypix = y0pix;
	       }
	       else {
		    y += bp->bar_offset;
		    /* Calc y1 according to bar position */
		    switch (bp->bar_position) {
			 case AtPositionLEFT:
			      ypix = AtScaleUserToPixel(ys, y - bw);
			      break;
			 case AtPositionCENTER:
			      ypix = AtScaleUserToPixel(ys, y - bw / 2);
			      break;
			 case AtPositionRIGHT:
			 default:
			      ypix = AtScaleUserToPixel(ys, y);
			      break;
		    }
		    ypix -= hpix;
		    xpix = AtScaleUserToPixel(xs, x);
		    wpix = xpix > x0pix ? xpix - x0pix : x0pix - xpix;
		    if (xpix > x0pix)
			 xpix = x0pix;
	       }
	  }
	  PIX[i].x = xpix;
	  PIX[i].y = ypix;
	  PIX[i].width  = wpix;
	  PIX[i].height = hpix;
	  /* Now make the fill rectangles 1 pix smaller each edge */
	  if (bp->bar_orientation == AtOrientVERTICAL) {
	       rp->x = xpix + 1;
	       rp->y = (ypix > y0pix) ? y0pix + 1 : ypix + 1;
	  }
	  else {
	       rp->x = (xpix > x0pix) ? x0pix + 1 : xpix + 1;
	       rp->y = ypix + 1;
	  }
	  rp->width  = Max(wpix - 1, 0);
	  rp->height = Max(hpix - 1, 0);
     }
}

/*
 *   Draw the bar clipped by the given region.
 */

static void Draw(self, dpy, drw, region, refresh)
AtXYBarPlotWidget self;
Display *dpy;
Drawable drw;
Region region;
int refresh;
{
     AtPlotterWidget pw = (AtPlotterWidget) XtParent((Widget) self);
     AtPlotterLayout *pwl = &pw->plotter.layout;
     XRectangle linerectangle, fillrectangle;
     Region lineregion, fillregion;

#ifdef TRACE
     fprintf(stderr, "Draw %d bars\n", bpw->lplot.num_points);
#endif

     if (bpw->lplot.num_points < 1)     /* Nothing to draw */
	  return;

     /* Get clip regions for outline and filled bar from the plotter layout */
     linerectangle.x      = fillrectangle.x      = pwl->x1 + 1;
     linerectangle.y      = fillrectangle.y      = pwl->y1 + 1;
     linerectangle.width  = fillrectangle.width  = pwl->width - 2;
     linerectangle.height = fillrectangle.height = pwl->height - 2;

     /* Now create and set the regions */
     lineregion = XCreateRegion();
     fillregion = XCreateRegion();
     XUnionRectWithRegion(&linerectangle, lineregion, lineregion);
     XUnionRectWithRegion(&fillrectangle, fillregion, fillregion);
     XSetRegion(dpy, bpw->plot.gc, lineregion);
     XSetRegion(dpy, bp->fill_gc, fillregion);

     if (bpw->lplot.old_pix) {
	  if (bpw->plot.fast_update && refresh) {
	       /*
		* We are in fast update mode, doing a refresh and have old
		* pix stuff, so draw them to "erase" the old s first
		*/
	       /* Do in reverse order to below!! */
	       if (bp->do_outline)
		    XDrawRectangles(dpy, drw, bpw->plot.gc,
				    (XRectangle *) bpw->lplot.old_pix,
				    bpw->lplot.old_num_points);
	       if (bp->do_fill)
		    XFillRectangles(dpy, drw, bp->fill_gc,
				    (XRectangle *) bpw->lplot.old_pix,
				    bpw->lplot.old_num_points);
	  }
	  XtFree((char *) bpw->lplot.old_pix);
	  bpw->lplot.old_pix = NULL;
	  bpw->lplot.old_num_points = 0;
     }

     /* Do the centre first, then outline */
     if (bp->do_fill) {
	  XFillRectangles(dpy, drw, bp->fill_gc,
			  bp->do_outline ? bp->fill_rectangles : PIX,
			  bpw->lplot.num_points);
     }
     if (bp->do_outline) {
	  XDrawRectangles(dpy, drw, bpw->plot.gc, PIX, bpw->lplot.num_points);
     }

     /* Unset and destroy the regions */
     XSetClipMask(dpy, bpw->plot.gc, None);
     XSetClipMask(dpy, bp->fill_gc, None);
     XDestroyRegion(lineregion);
     XDestroyRegion(fillregion);
}

/*
 *   Draw the "icon" in the given place.
 */

static void DrawIcon(self, dpy, drw, x1, y1, width, height, region)
AtXYBarPlotWidget self;
Display *dpy;
Drawable drw;
int x1, y1, width, height;
Region region;
{
     if (bp->do_fill) {
	  if (region)
	       XSetRegion(dpy, bp->fill_gc, region);
	  XFillRectangle(dpy, drw, bp->fill_gc, x1, y1, width, height);
	  if (region)
	       XSetClipMask(dpy, bp->fill_gc, None);
     }

     if (bp->do_outline) {
	  if (region)
	       XSetRegion(dpy, bpw->plot.gc, region);
	  XDrawRectangle(dpy, drw, bpw->plot.gc, x1, y1, width, height);
	  if (region)
	       XSetClipMask(dpy, bpw->plot.gc, None);
     }
}

/*
 *   PostScript stuff
 */

static void DrawPS(self, fp, xs, ys)
AtXYBarPlotWidget self;
FILE *fp;
AtScale *xs, *ys;
{
     int x0pix, y0pix, x1pix, y1pix, wpix, hpix, i;
     double x, y, bw;
     char term[100];
     char *shade;

     fprintf(fp, "%%%%BeginObject: AtXYBarPlot\nGS\n", XtName((Widget) self));

     AtPlotPSLineStyle(fp, (Widget) bpw);

     if (bp->bar_orientation == AtOrientVERTICAL) {
	  if (AtScaleGetLow(ys) < 0 && AtScaleGetHigh(ys) > 0)
	       y0pix = AtScaleUserToPixel(ys, 0.0);
	  else if (AtScaleGetLow(ys) < 0)
	       y0pix = AtScaleGetHighPix(ys);
	  else
	       y0pix = AtScaleGetLowPix(ys);
     }
     else {
	  if (AtScaleGetLow(xs) < 0 && AtScaleGetHigh(xs) > 0)
	       x0pix = AtScaleUserToPixel(xs, 0.0);
	  else if (AtScaleGetLow(xs) < 0)
	       x0pix = AtScaleGetHighPix(xs);
	  else
	       x0pix = AtScaleGetLowPix(xs);
     }

     if (bp->do_fill) {
	  shade = AtShadingPS(bp->shading);
	  if (bp->do_outline)
	       sprintf(term, "GS %s GR ST", shade);
	  else
	       strcpy(term, shade);
     }
     else
	  strcpy(term, "ST");

     bw = Abs(bp->bar_width);
     if (bp->bar_orientation == AtOrientVERTICAL)
	  wpix = AtScaleUserToPixel(xs, bw) - AtScaleUserToPixel(xs, 0.0);
     else
	  hpix = AtScaleUserToPixel(ys, bw) - AtScaleUserToPixel(ys, 0.0);

     for (i = 0; i < bpw->lplot.num_points; i++) {
	  if (bp->bar_orientation == AtOrientVERTICAL) {
	       y1pix = AtScaleUserToPixel(ys,
		    AtXYPlotGetYValue((AtXYPlotWidget) bpw, i));
	       if (bp->calc_width) {
		    x0pix = AtScaleUserToPixel(xs,
			 AtXYPlotGetXValue((AtXYPlotWidget) bpw, i));
		    if (i < (bpw->lplot.num_points - 1)) {
			 x1pix = AtScaleUserToPixel(xs,
			      AtXYPlotGetXValue((AtXYPlotWidget) bpw, i+1));
			 wpix = x1pix - x0pix;
		    }
		    else
			 x1pix = x0pix + wpix;
	       }
	       else {
		    x = AtXYPlotGetXValue((AtXYPlotWidget) bpw, i) +
			bp->bar_offset;
		    switch (bp->bar_position) {
			 case AtPositionLEFT:
			      x0pix = AtScaleUserToPixel(xs, x - bw);
			      break;
			 case AtPositionCENTER:
			      x0pix = AtScaleUserToPixel(xs, x - bw / 2);
			      break;
			 case AtPositionRIGHT:
			 default:
			      x0pix = AtScaleUserToPixel(xs, x);
			      break;
		    }
		    x1pix = x0pix + wpix;
	       }
	  }
	  else {
	       x1pix = AtScaleUserToPixel(xs,
		    AtXYPlotGetXValue((AtXYPlotWidget) bpw, i));
	       if (bp->calc_width) {
		    y0pix = AtScaleUserToPixel(ys,
			 AtXYPlotGetYValue((AtXYPlotWidget) bpw, i));
		    if (i < (bpw->lplot.num_points - 1)) {
			 y1pix = AtScaleUserToPixel(ys,
			      AtXYPlotGetYValue((AtXYPlotWidget) bpw, i+1));
			 hpix = y1pix - y0pix;
		    }
		    else
			 y1pix = y0pix + hpix;
	       }
	       else {
		    y = AtXYPlotGetYValue((AtXYPlotWidget) bpw, i) +
			bp->bar_offset;
		    switch (bp->bar_position) {
			 case AtPositionLEFT:
			      y0pix = AtScaleUserToPixel(ys, y - bw);
			      break;
			 case AtPositionCENTER:
			      y0pix = AtScaleUserToPixel(ys, y - bw / 2);
			      break;
			 case AtPositionRIGHT:
			 default:
			      y0pix = AtScaleUserToPixel(ys, y);
			      break;
		    }
		    y1pix = y0pix + hpix;
	       }
	  }
	  fprintf(fp, "%d %d M %d %d L %d %d L %d %d L CP %s\n",
		  x0pix, y0pix, x0pix, y1pix, x1pix, y1pix, x1pix, y0pix, term);
     }

     fprintf(fp, "GR\n%%%%EndObject: AtXYBarPlot %s\n", XtName((Widget) self));
}

static void DrawIconPS(self, fp, x1, y1, width, height)
AtXYBarPlotWidget self;
FILE *fp;
int x1, y1, width, height;
{
     char term[50], *shade;

     if (bp->do_fill) {
	  shade = AtShadingPS(bp->shading);
	  if (bp->do_outline)
	       sprintf(term, "GS %s GR ST", shade);
	  else
	       strcpy(term, shade);
     }
     else
	  strcpy(term, "ST");


     fprintf(fp, "GS ");
     AtPlotPSLineStyle(fp, (Widget) bpw);

     fprintf(fp, " %d %d M %d 0 RL 0 %d RL %d 0 RL CP %s GR\n",
	     x1, y1, width, -height, -width, term);
}

/*
 *   Resource converters
 */

static void AtCvtStringToBarPosition(args, num_args, from, to)
XrmValue *args;
Cardinal num_args;
XrmValue *from, *to;
{
     static AtBarPosition position;

     position = AtPositionINVALID;

     if (strcasecmp(from->addr, "center") == 0)
	  position = AtPositionCENTER;
     else if (strcasecmp(from->addr, "left") == 0)
	  position = AtPositionLEFT;
     else if (strcasecmp(from->addr, "right") == 0)
	  position = AtPositionRIGHT;

     if (position == AtPositionINVALID)
	  XtStringConversionWarning(from->addr, XtRBarPosition);
     else {
	  to->addr = (caddr_t) &position;
	  to->size = sizeof(AtBarPosition);
     }
}

void AtRegisterBarPositionConverter()
{
     static Boolean registered = False;

     if (!registered) {
	  XtAddConverter(XtRString, XtRBarPosition,
			 (XtConverter) AtCvtStringToBarPosition, NULL,0);
	  registered = True;
     }
}

static void AtCvtStringToBarOrientation(args, num_args, from, to)
XrmValue *args;
Cardinal num_args;
XrmValue *from, *to;
{
     static AtBarOrientation orient;

     orient = AtOrientINVALID;

     if (strcasecmp(from->addr, "vertical") == 0)
	  orient = AtOrientVERTICAL;
     else if (strcasecmp(from->addr, "horizontal") == 0)
	  orient = AtOrientHORIZONTAL;

     if (orient == AtOrientINVALID)
	  XtStringConversionWarning(from->addr, XtRBarOrientation);
     else {
	  to->addr = (caddr_t) &orient;
	  to->size = sizeof(AtBarOrientation);
     }
}

void AtRegisterBarOrientationConverter()
{
     static Boolean registered = False;

     if (!registered) {
	  XtAddConverter(XtRString, XtRBarOrientation,
			 (XtConverter) AtCvtStringToBarOrientation, NULL,0);
	  registered = True;
     }
}

/*
 *   Creation function
 */

Widget AtCreateXYBarPlot(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atXYBarPlotWidgetClass, parent, arglist, argcount));
}

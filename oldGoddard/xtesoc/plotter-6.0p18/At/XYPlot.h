/*
 *      XYPlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Resources XtNxOffest and XtNyOffset
 *                                      added.
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Typo fixed (by S. J. Manani).
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/07/23  XYPlot.h"
 */

/*

Copyright 1992 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 * AtSPlot is  non-instanced class that contains resources and routines
 * for accessing data that is currently stored in an array of structures.
 */

#ifndef _At_XYPlot_h
#define _At_XYPlot_h

#include <X11/At/At.h>
#include <X11/At/AtConverters.h>
#include <X11/At/Plot.h>

/* Declare specific AtXYPlotWidget class and instance datatypes */

typedef struct _AtXYPlotClassRec*  AtXYPlotWidgetClass;
typedef struct _AtXYPlotRec*       AtXYPlotWidget;

/* Declare the class constant */

externalref WidgetClass atXYPlotWidgetClass;

#ifndef AtIsXYPlot
#define AtIsXYPlot(w)    XtIsSubclass(w, atXYPlotWidgetClass)
#endif

/*
 *   Resources available to be set/fetched
 */

#define XtNxOffset "xOffset"
#define XtNyOffset "yOffset"

#define XtCXOffset "XOffset"
#define XtCYOffset "YOffset"

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 * The member routine to attach the data.
 * The subclasses of AtXPlot export a #define for this routine.
 */

extern void AtXYPlotAttachData P((Widget, XtPointer, AtDataType, Cardinal,
					  XtPointer, AtDataType, Cardinal,
					  Cardinal, Cardinal));
extern void AtXYPlotExtendData P((Widget, Cardinal));

/*
 * Get the i'th value from the AtXYPlot data.  This declares the function
 * version, a macro is available below.  This is intended mainly for
 * subclasses of AtXYPlot, but applications may find it useful.
 */

extern double AtXYPlotGetValue P((Widget, Cardinal));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 * A macro version of the above.  May be faster, but death to debug!
 */

/* First, get the address of the i'th element */

#define _lp_xptr(p, i) ((XtPointer)((char *)p->lplot.xdata + \
				    p->lplot.xstride * (i)))
#define _lp_yptr(p, i) ((XtPointer)((char *)p->lplot.ydata + \
				    p->lplot.ystride * (i)))

#define AtXYPlotGetXValue(p, i) \
     ((p)->lplot.x_offset + (double) \
      ((p)->lplot.xtype == AtDouble ? *((double *)_lp_xptr((p), (i))) : \
       (p)->lplot.xtype == AtFloat ? (double)*((float *)_lp_xptr((p), (i))) : \
       (p)->lplot.xtype == AtInt ? (double)*((int *)_lp_xptr((p), (i))) : 0.0))

#define AtXYPlotGetYValue(p, i) \
     ((p)->lplot.y_offset + (double) \
      ((p)->lplot.ytype == AtDouble ? *((double *)_lp_yptr((p), (i))) : \
       (p)->lplot.ytype == AtFloat ? (double)*((float *)_lp_yptr((p), (i))) : \
       (p)->lplot.ytype == AtInt ? (double)*((int *)_lp_yptr((p), (i))) : 0.0))

#endif /* _At_XYPlot_h */

/*
 *      TextPlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Fri Aug  7 10:07:13 1992, Some resource names and classes
 *                                      already defined in StringDefs.h
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Wed Sep  8 09:43:34 1993, patchlevel 8
 *                                      XtNverticalJustify added.
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/08  TextPlot.h"
 */

/*

Copyright 1993 by University of Paderborn
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#ifndef _At_TextPlot_h
#define _At_TextPlot_h

#include <X11/At/At.h>
#include <X11/At/FontFamily.h>

#define XtNfloatingPosition "floatingPosition"
#define XtNfloatingX "floatingX"
#define XtNfloatingY "floatingY"
#define XtNhorizontalJustify "horizontalJustify"
#define XtNverticalJustify "verticalJustify"

#define XtCFloatingPosition "FloatingPosition"
#define XtCFloatingX "FloatingX"
#define XtCFloatingY "FloatingY"
#define XtCHorizontalJustify "HorizontalJustify"
#define XtCVerticalJustify "VerticalJustify"

/* Declare specific AtTextPlotWidget class and instance datatypes */

typedef struct _AtTextPlotClassRec *AtTextPlotWidgetClass;
typedef struct _AtTextPlotRec *AtTextPlotWidget;

/* Declare the class constant */

externalref WidgetClass atTextPlotWidgetClass;

#ifndef AtIsTextPlot
#define AtIsTextPlot(w)  XtIsSubclass(w, atTextPlotWidgetClass)
#endif

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Creation function
 */

extern Widget AtCreateTextPlot P((Widget, char *, Arg *, Cardinal));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* _At_TextPlot_h */

/*
 *      FontFamilyPS.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Sat Sep 11 18:53:10 1993, patchlevel 8
 *                                      Close afm-files after reading.
 *                                      Don't exit when PS metric not found.
 *                                      Write to stdout when no filename is
 *                                      given (by Jamie Mazer).
 *                                      Some minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/11  FontFamilyPS.c";

/*

Copyright 1992,1993 by University of Paderborn
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#include <stdlib.h>
#include <X11/At/FontFamilyP.h>

static void ReadPSMetrics P((AtFontFamilyInfo *ffi, int face));
static void ReadPSMetrics(ffi, face)
AtFontFamilyInfo *ffi;
int face;
{
     char buf[1024];
     FILE *f;
     char *status;
     int code, width;

    char* afmpath ;
 
    sprintf(buf,"%s/%s.afm",
      (afmpath = getenv("AFMPATH")) ? afmpath : AFMPATH,
      ffi->psnames[face]);

     /*sprintf(buf,"%s/%s.afm", AFMPATH, ffi->psnames[face]);
     */
     if ((f = fopen(buf, "r")) == NULL) {
	  fprintf(stderr, "Can't read PostScript font metrics. Output may be incorrect!\n");
	  perror(buf);
	  return;
     }

     do {      /* Skip preliminary lines */
	  status = fgets(buf, sizeof(buf), f);
	  if (status == NULL)
	       break;
	  if (strncmp(buf, "Ascender", 8) == 0)
	       sscanf(buf, "Ascender %hd", &ffi->psascents[face]);
	  if (strncmp(buf, "Descender", 9) == 0)
	       sscanf(buf, "Descender %hd", &ffi->psdescents[face]);
     } while (strncmp(buf, "StartCharMetrics", 16));

     while (fgets(buf, sizeof(buf), f)) {    /* Read all the char metrics */
	  if (strncmp(buf, "EndCharMetrics", 14) == 0)
	       break;
	  sscanf(buf, "C %d ; WX %d ;", &code, &width);
	  ffi->psmetrics[face][code] = (short)width;
     }

     fclose(f);
}

static void GetPSMetrics P((AtFontFamily *ff));
static void GetPSMetrics(ff)
AtFontFamily *ff;
{
     AtFontFamilyInfo *info;

     info = INFO(ff);
     if (info->psmetrics != NULL)
	  return;
     info->psmetrics = (PostscriptMetrics *)calloc(4,sizeof(PostscriptMetrics));
     ReadPSMetrics(info, AtFontPLAIN);
     ReadPSMetrics(info, AtFontBOLD);
     ReadPSMetrics(info, AtFontITALIC);
     ReadPSMetrics(info, AtFontBOLDITALIC);
     *SCCSid = *SCCSid;        /* Keep gcc quiet */
}

int AtFontPSTextWidth(ff, face, size, str, len)
AtFontFamily *ff;
int face, size;
char *str;
int len;
{
     AtFontFamilyInfo *info;
     int width = 0;
     int i;

     info = INFO(ff);
     if (info->psmetrics == NULL)
	  GetPSMetrics(ff);
     for (i= 0; i < len; i++)
	  width += info->psmetrics[face][*str++];
     return (width * AtFontPointSize(ff,size))/1000;
}

int AtFontPSAscent(ff, face, size)
AtFontFamily *ff;
int face, size;
{
     AtFontFamilyInfo *info;

     info = INFO(ff);
     if (info->psmetrics == NULL)
	  GetPSMetrics(ff);
     return (info->psascents[face] * AtFontPointSize(ff, size))/1000;
}

int AtFontPSDescent(ff, face, size)
AtFontFamily *ff;
int face, size;
{
     AtFontFamilyInfo *info;

     info = INFO(ff);
     if (info->psmetrics == NULL)
	  GetPSMetrics(ff);
     return (-info->psdescents[face] * AtFontPointSize(ff, size))/1000;
}

char *AtFontPSName(ff, face)
AtFontFamily *ff;
int face;
{
     return (INFO(ff)->psnames[face]);
}

/*
 *      SPlot.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Typos fixed
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Some minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/07/23  SPlot.c";

/*

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 * The AtSPlot object is simply a holder for some resources to allow
 * access to dataitems that are elements of a structure.
 */

#include <X11/At/SPlotP.h>

#ifndef AtSPlotGetValue
double AtSPlotGetValue P((AtSPlotWidget p, Cardinal i));
double AtSPlotGetValue(p, i)
AtSPlotWidget p;
Cardinal i;
{
     XtPointer ptr = _sp_ptr(p, i);

     double ret =
	  ((p)->splot.type == AtDouble ? *((double *)ptr) :
	   (p)->splot.type == AtFloat ? (double)*((float *)ptr) :
	   (p)->splot.type == AtInt ? (double)*((int *)ptr) :
	   0.0);

     return ret;
}
#endif

static void SPlotDestroy P((AtSPlotWidget));
static void SPlotClassPartInit P((WidgetClass));
static void SPlotInit P((AtSPlotWidget, AtSPlotWidget));

AtSPlotClassRec atSPlotClassRec = {
  { /* core fields */
    /* superclass               */      (WidgetClass) &atPlotClassRec,
    /* class_name               */      "AtSPlot",
    /* widget_size              */      sizeof(AtSPlotRec),
    /* class_initialize         */      NULL,
    /* class_part_initialize    */      SPlotClassPartInit,
    /* class_inited             */      FALSE,
    /* initialize               */      (XtInitProc) SPlotInit,
    /* initialize_hook          */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      0,
    /* resources                */      NULL,
    /* num_resources            */      0,
    /* xrm_class                */      NULLQUARK,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* pad                      */      FALSE,
    /* destroy                  */      (XtWidgetProc) SPlotDestroy,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* set_values               */      NULL,
    /* set_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* get_values_hook          */      NULL,
    /* pad                      */      NULL,
    /* version                  */      XtVersion,
    /* callback_private         */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL,
    /* pad                      */      NULL
  },
  { /* atPlot fields */
    /* draw                     */      NULL,
    /* draw_icon                */      NULL,
    /* drawPS                   */      NULL,
    /* draw_iconPS              */      NULL,
    /* recalc                   */      NULL
  },
  { /* sPlot fields */
    /* attach_data              */      NULL
  }
};

WidgetClass atSPlotWidgetClass = (WidgetClass)&atSPlotClassRec;

static void SPlotInit(req, new)
AtSPlotWidget req, new;
{
     AtSPlotPart *sr = &new->splot;

     sr->data = NULL;
     sr->stride = sr->start = sr->num_points = sr->old_num_points = 0;
     sr->type = AtInt;
     sr->pix = sr->old_pix = NULL;
}

static void SPlotDestroy(sp)
AtSPlotWidget sp;
{
     XtFree(sp->splot.pix);
     XtFree(sp->splot.old_pix);
}

static void SPlotClassPartInit(wc)
WidgetClass wc;
{
     AtSPlotWidgetClass super = (AtSPlotWidgetClass) wc->core_class.superclass;
     AtSPlotWidgetClass spc = (AtSPlotWidgetClass) wc;
     if (spc->splot_class.attach_data == XtInheritAttachData) {
	  spc->splot_class.attach_data = super->splot_class.attach_data;
     }
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

/*****************************************************************
 *
 * These are the exported "member" routines
 */
void AtSPlotAttachData(w, data, type, stride, start, num)
Widget w;
XtPointer data;
AtDataType type;
Cardinal stride, start, num;
{
     AtSPlotWidget spw = (AtSPlotWidget) w;
     BoundingBox bb;
     AtSPlotAttachProc adp;
     int i;

     XtCheckSubclass((Widget)spw, atSPlotWidgetClass,
		     "AtSPlotAttachData needs an AtSPlot object");

     if (spw->plot.fast_update) {
	  /* Save a copy of the current pix data and request a refresh */
	  if (spw->splot.old_pix) {
	       /* Is this an error???? */
#ifdef DEBUG
	       fprintf(stderr, "In AtSPlotAttachData - old pix still current!\n");
#endif
	       XtFree((char *)spw->splot.old_pix);
	  }
	  spw->splot.old_pix = spw->splot.pix;
	  spw->splot.old_num_points = spw->splot.num_points;
     } else if (spw->splot.pix)
	  XtFree((char *)spw->splot.pix);

     spw->splot.num_points = num;
     spw->splot.data = data;
     spw->splot.type = type;
     spw->splot.stride = stride;
     spw->splot.start = start;

     bb.xmax = spw->splot.start + spw->splot.num_points - 1;
     bb.xmin = spw->splot.start;
     bb.ymax = -HUGE_VAL;
     bb.ymin = HUGE_VAL;

     for (i = 0; i < spw->splot.num_points; i++) {
	  register double v = AtSPlotGetValue(spw, i);
	  bb.ymax = Max(bb.ymax, v);
	  bb.ymin = Min(bb.ymin, v);
     }

     if (adp = ((AtSPlotWidgetClass)
		spw->object.widget_class)->splot_class.attach_data) {
	  adp(spw, &bb, False);
     }

     AtPlotterPlotDataChanged((Widget)spw, &bb, spw->plot.fast_update);
}

/*
 * We have added some more data, num is the new total
 * Data runs from 0 .. num-1, where x value of first is start.
 */
void AtSPlotExtendData(w, num)
Widget w;
Cardinal num;
{
     AtSPlotWidget spw = (AtSPlotWidget) w;
     BoundingBox bb;
     AtSPlotAttachProc adp;
     int i;
     int old_num = spw->splot.num_points;

     XtCheckSubclass((Widget)spw, atSPlotWidgetClass,
		     "AtSPlotExtendData needs an AtSPlot object");

     bb.xmax = spw->splot.start + num - 1;
     bb.xmin = spw->splot.start + old_num;
     bb.ymax = -HUGE_VAL;
     bb.ymin = HUGE_VAL;

     /* Get the BBox for THE NEW STUFF ONLY */
     for (i = old_num; i < num; i++) {
	  register double v = AtSPlotGetValue(spw, i);
	  bb.ymax = Max(bb.ymax, v);
	  bb.ymin = Min(bb.ymin, v);
     }
     spw->splot.num_points = num;

     if (adp = ((AtSPlotWidgetClass)
		spw->object.widget_class)->splot_class.attach_data) {
	  adp(spw, &bb, True);
     }

     AtPlotterPlotExtended((Widget)spw, &bb, old_num, num - 1);
}

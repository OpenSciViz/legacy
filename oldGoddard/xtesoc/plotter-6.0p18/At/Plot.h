/*
 *      Plot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Mon Jul 27 14:16:59 1992, patchlevel 2
 *                                      Draw() changed for drawing
 *                                      to a pixmap instead of a window
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/07/23  Plot.h"
 */

/*

Copyright 1992 by University of Paderborn
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#ifndef _At_Plot_h
#define _At_Plot_h

#include <X11/At/At.h>
#include <X11/At/Scale.h>

#define XtNlineWidth "lineWidth"
#define XtNlineStyle "lineStyle"
#define XtNdashLength "dashLength"
#define XtNfastUpdate "fastUpdate"

#define XtCLineWidth "LineWidth"
#define XtCLineStyle "LineStyle"
#define XtCDashLength "DashLength"
#define XtCFastUpdate "FastUpdate"

/* declare specific AtPlotWidget class and instance datatypes */

typedef struct _AtPlotClassRec *AtPlotWidgetClass;
typedef struct _AtPlotRec *AtPlotWidget;

/* declare the class constant */

externalref WidgetClass atPlotWidgetClass;

#ifndef AtIsPlot
#define AtIsPlot(w) XtIsSubclass(w, atPlotWidgetClass)
#endif

typedef enum {
	AtDouble,
	AtFloat,
	AtInt
} AtDataType;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Extern routine for PostScript output.
 *   Write a line to the file to set the PostScript GC to reflect
 *   the plot instance record (linewidth, linestyle etc).
 *   Assume 1 PostScript unit is more-or-less one pixel.
 */

extern void AtPlotPSLineStyle P((FILE *, Widget));

/*
 *   These are wrappers around the member functions
 */

extern void AtPlotDraw P((Widget, Display *, Drawable, Region, int));
extern void AtPlotDrawIcon P((Widget, Display *, Drawable, int, int, int, int, Region));
extern void AtPlotDrawPS P((Widget, FILE *, AtScale *, AtScale *));
extern void AtPlotDrawIconPS P((Widget, FILE *, int, int, int, int));
extern void AtPlotRecalc P((Widget, AtScale *, AtScale *, int, int));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* _At_Plot_h */

/*
 *      XYLabelAxis.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 10:25:37 1993
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYLabelAxis.c";

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   Teh XYLabel Axis widget is derived from the LabelAxis widget and
 *   is a subclass of the AxisCore widget class.
 *   The XYLabelAxis class decides the location and label of the tic
 *   marks by looking at the application-supplied data.
 */


#include <X11/At/XYLabelAxisP.h>

static void Initialize P((AtXYLabelAxisWidget, AtXYLabelAxisWidget));
static Boolean SetValues P((AtXYLabelAxisWidget, AtXYLabelAxisWidget,
			    AtXYLabelAxisWidget));
static void RangeProc P((AtAxisCoreWidget, double *minp, double *maxp,
			 double *tip, int *nwp));
static void CalcProc P((AtAxisCoreWidget));

/* No new resources */

AtXYLabelAxisClassRec atXYLabelAxisClassRec = {
  { /* core fields */
     /* superclass              */      (WidgetClass) &atAxisCoreClassRec,
     /* class_name              */      "AtXYLabelAxis",
     /* widget_size             */      sizeof(AtXYLabelAxisRec),
     /* class_initialize        */      NULL,
     /* class_part_initialize   */      NULL,
     /* class_inited            */      FALSE,
     /* initialize              */      (XtInitProc) Initialize,
     /* initialize_hook         */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      0,
     /* resources               */      NULL,
     /* num_resources           */      0,
     /* xrm_class               */      NULLQUARK,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* destroy                 */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* set_values              */      (XtSetValuesFunc) SetValues,
     /* set_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* get_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* version                 */      XtVersion,
     /* callback_private        */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL
     },
  { /* atPlot fields */
     /* draw                    */      XtInheritDraw,
     /* draw_icon               */      XtInheritDrawIcon,
     /* drawPS                  */      XtInheritDrawPS,
     /* draw_iconPS             */      XtInheritDrawIconPS,
     /* recalc                  */      XtInheritRecalc
  },
  { /* labelAxis fields */
     /* range_proc              */      (AtAxisRangeProc) RangeProc,
     /* calc_proc               */      (AtAxisCalcProc) CalcProc
  }
};

WidgetClass atXYLabelAxisWidgetClass = (WidgetClass)&atXYLabelAxisClassRec;

/*
 *   The core member procs
 */

static void Initialize(req, new)
AtXYLabelAxisWidget req, new;
{
     new->laxis.start = new->laxis.num_items = 0;
     new->laxis.first = new->laxis.last = 0;
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

static Boolean SetValues(old, req, new)
AtXYLabelAxisWidget old, req, new;
{
#define Changed(fld)     (old->laxis.fld != new->laxis.fld)
#define AC_Changed(fld)  (old->axiscore.fld != new->axiscore.fld)
     Boolean renum = False;

     if (AC_Changed(font_family) || AC_Changed(number_size)) {
	  renum = True;
     }

     if (renum) {
	  new->axiscore.numbers_changed = True;
	  AtPlotterRescaleRequired((Widget) new);
     }
     return False;

#undef Changed
#undef AC_Changed
}

/*
 *   Useful macros to access the label axis x value and label
 */

#define _la_ptr(p, i) \
     ((XtPointer) ((char *) p->laxis.xdata + p->laxis.xstride * (i)))

#define AxisXValue(p, i) \
    ((double) \
     ((p)->laxis.xtype == AtDouble ? *((double *)_la_ptr((p), (i))) : \
      (p)->laxis.xtype == AtFloat ? (double)*((float *)_la_ptr((p), (i))) : \
      (p)->laxis.xtype == AtInt ? (double)*((int *)_la_ptr((p), (i))) : 0.0))

#define AxisLabel(p, i) \
     ((String *) ((char *) (p)->laxis.ldata + (p)->laxis.lstride * (i)))

/*
 *   The XYLabelAxis member procs
 */

static Boolean CheckData P((AtXYLabelAxisWidget, AtXYLabelAxisPart *));
static Boolean CheckData(lw, la)
AtXYLabelAxisWidget lw;
AtXYLabelAxisPart *la;
{
     if (!la->xdata || !la->ldata || la->start == 0 || la->num_items == 0) {
	  XtAppWarning(XtWidgetToApplicationContext(XtParent((Widget) lw)),
		       "No data attached to AtXYLabelAxis");
	  return False;
     }
     return True;
}

static void RangeProc(w, minp, maxp, tip, nwp)
AtAxisCoreWidget w;
double *minp, *maxp, *tip;
int *nwp;
{
     AtXYLabelAxisWidget lw = (AtXYLabelAxisWidget) w;
     AtXYLabelAxisPart *la = &((AtXYLabelAxisWidget) w)->laxis;
     AtAxisCorePart *ac = &w->axiscore;
     double f, l;
     int ft, lt, i;

     /* Check attached data */
     if (!CheckData(lw, la))
	  return;

     /* Get and set first and last tic */
     la->first = la->start - 1;
     la->last  = la->first + la->num_items - 1;

     if (!w->axiscore.auto_scale) {
	  ft = la->first;
	  lt = la->last;
	  for (i = ft; i <= lt; i++) {
	       if (AxisXValue(lw, i) >= ac->max) {
		    la->last = i;
		    break;
	       }
	  }
	  for (i = lt; i >= ft; i--) {
	       if (AxisXValue(lw, i) <= ac->min) {
		    la->first = i;
		    break;
	       }
	  }
     }

     f = AxisXValue(lw, la->first);
     l = AxisXValue(lw, la->last);
     *minp = Min(f, l);
     *maxp = Max(l, f);
     *tip = 0.0;

     ac->min = *minp * 1.01;       /* Force recalc */
     ac->max = *maxp * 0.99;
}

static void CalcProc(w)
AtAxisCoreWidget w;
{
     AtXYLabelAxisWidget lw = (AtXYLabelAxisWidget) w;
     AtXYLabelAxisPart *la = &((AtXYLabelAxisWidget) w)->laxis;
     AtAxisCorePart *ac = &w->axiscore;
     int nt, ti, i;
     String *l;

     /* Check attached data */
     if (!CheckData(lw, la))
	  return;

     /* Count the valid tics */
     for (nt = 0, i = la->first; i <= la->last; i++) {
	  l = AxisLabel(lw, i);
	  if ((l && *l && **l) || i == la->first || i == la->last)
	       ++nt;
     }

     /* Allocate tic values and labels */
     ac->num_tics = nt;
     ac->num_subtics = 0;
     ac->tic_values = (double *) XtMalloc(sizeof(double) * ac->num_tics);
     ac->subtic_values = NULL;
     ac->tic_label_string = (String *) XtMalloc(sizeof(String) * ac->num_tics);

     /* Set tic values and labels */
     for (ti = 0, i = la->first; i <= la->last && ti < nt; i++) {
	  l = AxisLabel(lw, i);
	  if ((l && *l && **l) || i == la->first || i == la->last) {
	       ac->tic_values[ti] = AxisXValue(lw, i);
	       if (l && *l && **l)
		    ac->tic_label_string[ti] = XtNewString(*l);
	       else
		    ac->tic_label_string[ti] = XtNewString("");
	       ++ti;
	  }
     }
}

/*
 *   The member routine
 */

void AtXYLabelAxisAttachData(w, xdata, xtype, xstride, ldata, lstride, start, num)
Widget w;
XtPointer xdata;
AtDataType xtype;
String *ldata;
Cardinal xstride, lstride, start, num;
{
     AtXYLabelAxisWidget lw = (AtXYLabelAxisWidget) w;
     AtXYLabelAxisPart *la = &lw->laxis;

     XtCheckSubclass((Widget)lw, atXYLabelAxisWidgetClass,
		     "AtXYLabelAxisAttachData needs an AtXYLabelAxisWidget");

     la->xdata = xdata;
     la->xtype = xtype;
     la->xstride = xstride;
     la->ldata = ldata;
     la->lstride = lstride;
     la->start = start;
     la->num_items = num;
     la->first = la->last = 0;

     lw->axiscore.numbers_changed = True;
     AtPlotterRescaleRequired((Widget) lw);
}

/*
 *   Creation function
 */

Widget AtCreateXYLabelAxis(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atXYLabelAxisWidgetClass, parent, arglist, argcount));
}

/*
 *      PlotterPS.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Fri Jul 24 15:07:46 1992, patchlevel 1
 *                                      Bug in star marker fixed.
 *      klin, Mon Jul 27 14:17:53 1992, patchlevel 2
 *                                      Resource XtNlegendLeft added.
 *                                      When generating PostScript for a
 *                                      plotter widget check if it is realized.
 *                                      Otherwise a core dump is written.
 *                                      Bug reported by Gustaf Neumann
 *                                      (neumann@dec4.wu-wien.ac.at).
 *                                      Busy cursor added.
 *      klin, Sat Aug 15 10:07:50 1992, patchlevel 4
 *                                      Minor changes in PS output and
 *                                      callbacks.
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Sat Sep 11 18:53:50 1993, patchlevel 8
 *                                      PS stuff for drawing axis in origin.
 *                                      Some more minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/09/11  PlotterPS.c";

/*

Copyright 1992,1993 by University of Paderborn
Copyright 1990,1991 by the Massachusetts Institute of Technology

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#include <X11/At/Text.h>
#include <X11/At/PlotterP.h>
#include <X11/At/AxisCore.h>
#include <X11/At/Plot.h>

#define NUMCHILDREN(w) (w->composite.num_children)
#define CHILD(w,i) ((AtPlotWidget)(w->composite.children[i]))
#define CONSTRAINT(w,i) \
     ((AtPlotterConstraints)(((Widget)CHILD(w,i))->core.constraints))
#define CONSTRAINTS(cw) \
     ((AtPlotterConstraints)((Widget)cw)->core.constraints)

#define NTHCHILDISDISPLAYED(w, i) (CONSTRAINT(w, i)->plotter.displayed)
#define ISDISPLAYED(cw) (CONSTRAINTS(cw)->plotter.displayed)

#define ISAXIS(cw)  (XtIsSubclass((Widget) cw, atAxisCoreWidgetClass))

#define height(x) AtTextPSAscent(x) + AtTextPSDescent(x)

/*
 *   Calculate the width of the legend for the PostScript version
 */

static int CalcLegendWidth P((AtPlotterWidget));
static int CalcLegendWidth(pw)
AtPlotterWidget pw;
{
     int i, w, mw = 0;
     AtText *t;

     for(i=0; i < NUMCHILDREN(pw); i++) {
	  if (NTHCHILDISDISPLAYED(pw, i)) {
	       t = CONSTRAINT(pw,i)->plotter.legend_text;
	       if (t != NULL) {
		    w = AtTextPSWidth(t);
		    if (w > mw)
			 mw = w;
	       }
	  }
     }
     return mw + 16 + pw->plotter.margin_width;
}

/*
 *   Print out the legend
 */

static void DrawLegend P((FILE *, AtPlotterWidget, int, int, int, int));
static void DrawLegend(f, w, x1, y1, x2, y2)
FILE *f;
AtPlotterWidget w;
int x1, y1, x2, y2;
{
     int x, y, h;
     int i;
     AtText *t;

     fprintf(f, "%%%%BeginObject: AtPlotter legend\nGS\n");

     h = height(w->plotter.legend_title_text) + w->plotter.margin_height;
     for(i=0; i < NUMCHILDREN(w); i++) {
	  if (NTHCHILDISDISPLAYED(w, i)) {
	       t = CONSTRAINT(w,i)->plotter.legend_text;
	       if (t != NULL) h += height(t) + w->plotter.legend_spacing;
	  }
     }

     y = y2 - ((y2 - y1) - h) / 2;
     x = x1 + ((x2 - x1) - AtTextPSWidth(w->plotter.legend_title_text)) / 2;
     AtTextPSDraw(f, w->plotter.legend_title_text, x,
		  y - AtTextPSAscent(w->plotter.legend_title_text));
     y -= height(w->plotter.legend_title_text) + w->plotter.margin_height;

     for(i = 0; i < NUMCHILDREN(w); i++ ) {
	  t = CONSTRAINT(w,i)->plotter.legend_text;
	  if (NTHCHILDISDISPLAYED(w, i) && t != NULL) {
	       h = height(t);
	       AtPlotDrawIconPS((Widget) CHILD(w, i), f, x1, y, 16, h);
	       AtTextPSDraw(f, t, x1 + 16 + w->plotter.margin_width,
			    y - AtTextPSAscent(t));
	       y -= h + w->plotter.legend_spacing;
	  }
     }

     fprintf(f, "GR\n%%%%EndObject: AtPlotter legend\n");
}
#undef height

void AtPlotterDrawPS(f, pw, xll, yll, xur, yur)
FILE *f;
Widget pw;
int xll, yll, xur, yur;
{
     AtPlotterWidget w = (AtPlotterWidget) pw;
     int height, width, i;
     int xaxiswidth, x2axiswidth, yaxiswidth, y2axiswidth;
     int x1, y1, x2, y2;
     int title_x, title_y, legend_x, legend_width;
     AtScale *xscale = NULL, *x2scale = NULL, *yscale = NULL, *y2scale = NULL;
     AtPlotterPart *pp = &w->plotter;
     double min, max;
     AtScale *xs, *ys;
     AtBusyCallbackData cbd;

     /* Set the busy cursor */
     if (w->plotter.use_cursors)
	  XDefineCursor(XtDisplay(w), XtWindow(w), w->plotter.busy_cursor);

     /* Deliver the busy callback */
     if (XtHasCallbacks((Widget) w, XtNbusyCallback) == XtCallbackHasSome) {
	  cbd.reason = AtBusyPOSTSCRIPT;
	  cbd.busy = True;
	  XtCallCallbacks((Widget) w, XtNbusyCallback, (XtPointer) &cbd);
     }
     else
	  cbd.busy = False;

     fprintf(f, "%%%%BeginObject: AtPlotter %s\nGS\n", XtName(pw));

     x1 = xll;
     y1 = yll;
     x2 = xur;
     y2 = yur;

     /* Set up clipping */
     /* Since this is right before a grestore, we don't need to gsave here. */
     fprintf(f, "%d %d M %d %d L %d %d L %d %d L CN\n",
	     x1, y1, x1, y2, x2, y2, x2, y1);

     if (pp->show_legend) {
	  legend_width = CalcLegendWidth(w);
	  if (pp->legend_left) {
	       legend_x = x1 + pp->margin_width;
	       x1 += legend_width + pp->margin_width;
	  }
	  else {
	       x2 -= legend_width + pp->margin_width;
	       legend_x = x2 + pp->margin_width;
	  }
     }

     if (pp->title_text != NULL) {
	  title_y = y2 - AtTextPSAscent(pp->title_text);
	  y2 -= AtTextPSAscent(pp->title_text) +
	       AtTextPSDescent(pp->title_text) + pp->margin_height;
     }

     xaxiswidth = x2axiswidth = yaxiswidth = y2axiswidth = 0;

     if (pp->xaxis && ISDISPLAYED(pp->xaxis))
	  xaxiswidth  = AtAxisWidthPS((Widget) pp->xaxis);
     if (pp->x2axis && ISDISPLAYED(pp->x2axis))
	  x2axiswidth = AtAxisWidthPS((Widget) pp->x2axis);
     if (pp->yaxis && ISDISPLAYED(pp->yaxis))
	  yaxiswidth  = AtAxisWidthPS((Widget) pp->yaxis);
     if (pp->y2axis && ISDISPLAYED(pp->y2axis))
	  y2axiswidth = AtAxisWidthPS((Widget) pp->y2axis);

     x2 -= y2axiswidth;
     x1 += yaxiswidth;
     y1 += xaxiswidth;
     y2 -= x2axiswidth;

     width  = x2 - x1;
     height = y2 - y1;

     if (pp->title_text != NULL)  {
	  title_x = x1 + (width - AtTextPSWidth(pp->title_text))/2;
	  AtTextPSDraw(f, pp->title_text, title_x, title_y);
     }

     if (pp->show_legend)
	  DrawLegend(f, w, legend_x, yll, legend_x + legend_width, yur);

     /* Set up scales */
     if (pp->xaxis) {
	  AtAxisGetBounds((Widget) pp->xaxis, &min, &max);
	  xscale = AtScaleCreate(min, max, x1, x2,
				 AtAxisGetTransform((Widget) pp->xaxis));
     }

     if (pp->x2axis) {
	  AtAxisGetBounds((Widget) pp->x2axis, &min, &max);
	  x2scale = AtScaleCreate(min, max, x1, x2,
				  AtAxisGetTransform((Widget) pp->x2axis));
     }

     if (pp->yaxis) {
	  AtAxisGetBounds((Widget) pp->yaxis, &min, &max);
	  yscale = AtScaleCreate(min, max, y1, y2,
				 AtAxisGetTransform((Widget) pp->yaxis));
     }

     if (pp->y2axis) {
	  AtAxisGetBounds((Widget) pp->y2axis, &min, &max);
	  y2scale = AtScaleCreate(min, max, y1, y2,
				  AtAxisGetTransform((Widget) pp->y2axis));
     }

     /* First, draw the axes */
     for(i = 0; i < NUMCHILDREN(w); i++) {
	  if (!NTHCHILDISDISPLAYED(w, i) || !ISAXIS(CHILD(w, i)))
	       continue;
	  if ((AtAxisCoreWidget) CHILD(w, i) == pp->xaxis)
	       AtAxisDrawPS((Widget) CHILD(w, i), f, xscale, yscale,
			    x1, y1, x2, y1, y2 - y1);
	  else if ((AtAxisCoreWidget) CHILD(w, i) == pp->x2axis)
	       AtAxisDrawPS((Widget) CHILD(w, i), f, x2scale, NULL,
			    x1, y2, x2, y2, y2 - y1);
	  else if ((AtAxisCoreWidget) CHILD(w, i) == pp->yaxis)
	       AtAxisDrawPS((Widget) CHILD(w, i), f, yscale, xscale,
			    x1, y1, x1, y2, x2 - x1);
	  else if ((AtAxisCoreWidget) CHILD(w, i) == pp->y2axis)
	       AtAxisDrawPS((Widget) CHILD(w, i), f, y2scale, NULL,
			    x2, y1, x2, y2, x2 - x1);
     }

     /* Set the clipping region to the plotting area */
     fprintf(f, "%d %d M %d %d L %d %d L %d %d L CN\n",
	     xscale->lowpix,  yscale->lowpix,  xscale->lowpix,  yscale->highpix,
	     xscale->highpix, yscale->highpix, xscale->highpix, yscale->lowpix);

     /* Now draw the plots */
     if (pp->rank_children) {
	  Rank *tmp;
	  for (tmp = pp->ordered_children; tmp; tmp = tmp->next) {
	       if (!ISDISPLAYED(tmp->child) || ISAXIS(tmp->child))
		    continue;
	       xs = CONSTRAINTS(tmp->child)->plotter.use_x2_axis ?
		    x2scale : xscale;
	       ys = CONSTRAINTS(tmp->child)->plotter.use_y2_axis ?
		    y2scale : yscale;
	       AtPlotDrawPS((Widget) tmp->child, f, xs, ys);
	  }
     }
     else {
	  for(i = 0; i < NUMCHILDREN(w); i++) {
	       if (!NTHCHILDISDISPLAYED(w, i) || ISAXIS(CHILD(w, i)))
		    continue;
	       xs = CONSTRAINT(w, i)->plotter.use_x2_axis ?
		    x2scale : xscale;
	       ys = CONSTRAINT(w, i)->plotter.use_y2_axis ?
		    y2scale : yscale;
	       AtPlotDrawPS((Widget) CHILD(w, i), f, xs, ys);
	  }
     }

     /* Finish up */
     fprintf(f, "GR\n%%%%EndObject: AtPlotter %s\n", XtName(pw));

     /* Deliver the busy callback */
     if (cbd.busy) {
	  cbd.reason = AtBusyPOSTSCRIPT;
	  cbd.busy = False;
	  XtCallCallbacks((Widget) w, XtNbusyCallback, (XtPointer) &cbd);
     }

     /* Reset busy cursor */
     if (w->plotter.use_cursors) {
	  XDefineCursor(XtDisplay(w), XtWindow(w), w->plotter.current_cursor);
     }
}

static char prolog[] =
"/IT  { transform round exch round exch itransform } bind def\n\
/F { findfont exch scalefont setfont } bind def\n\
/S { IT moveto show } bind def\n\
/L { IT lineto } bind def\n\
/M { IT moveto } bind def\n\
/RM { IT rmoveto } bind def\n\
/RL { IT rlineto } bind def\n\
/CP { closepath } bind def\n\
/CN { closepath clip newpath } bind def\n\
/ST { stroke } bind def\n\
/GS { gsave } bind def\n\
/GR { grestore } bind def\n\
/SG { setgray fill } bind def\n\
/SD { setdash } bind def\n\
/SL { setlinewidth } bind def\n\
/RT { rotate } bind def\n\
/DOT  { IT 1 0 360 arc fill } bind def\n\
/CIRC { IT 2 0 360 arc stroke } bind def\n\
/HBAR { M -2 0 RM 4 0 RL } bind def\n\
/VBAR { M 0 -2 RM 0 4 RL } bind def\n\
/RECT { M -2 -2 RM 0 4 RL 4 0 RL 0 -4 RL -4 0 RL } bind def\n\
/PLUS { M -2 0 RM 4 0 RL -2 -2 RM 0 4 RL } bind def\n\
/XMRK { M -2 -2 RM 4 4 RL 0 -4 RM -4 4 RL } bind def\n\
/STAR { M -2 -2 RM 4 4 RL 0 -4 RM -4 4 RL 2 0 RM 0 -4 RL -2 2 RM 4 0 RL } bind def\n\
/DIAM { M -2 0 RM 2 2 RL 2 -2 RL -2 -2 RL -2 2 RL } bind def\n\
/TRI1 { M -2 -2 RM 4 0 RL -2 4 RL -2 -4 RL } bind def\n\
/TRI2 { M -2 2 RM 4 0 RL -2 -4 RL -2 4 RL } bind def\n\
/TRI3 { M 2 2 RM 0 -4 RL -4 2 RL 4 2 RL } bind def\n\
/TRI4 { M -2 2 RM 0 -4 RL 4 2 RL -4 -2 RL } bind def\n";

#include <sys/types.h>
#ifndef VMS
# include <pwd.h>
#endif

#ifdef VMS
# include <time.h>
#endif

void AtPlotterGeneratePostscript(filename, w, title, x1, y1, x2, y2, landscape)
char *filename;
Widget w;
char *title;
int x1, y1, x2, y2;
int landscape;
{
     AtPlotterWidget pw = (AtPlotterWidget) w;
     FILE *f;
     char *asctime();
#ifndef VMS
     time_t now = time(NULL);
#else
     char *user_name;
     time_t now;

     time((time_t *) *now);
#endif

     /* Generate PostScript for a realized plotter widget only! */
     if ( !XtIsRealized((Widget) pw)) {
	  XtAppWarning(XtWidgetToApplicationContext((Widget) pw),
		       "Plotter widget is not realized");
	  return;
     }

     if (filename == NULL) {
	  f = stdout;
     }
     else if ((f = fopen(filename, "w+")) == NULL) {
	  XtAppWarning(XtWidgetToApplicationContext((Widget) pw),
		       "Cannot open for write PostScript output file");
	  return;
     }

     /* Do header info */
     fprintf(f,"%%!PS-Adobe-2.0\n");
     fprintf(f,"%%%%Title: %s\n", title);
     fprintf(f,"%%%%Creator: The AthenaTools Plotter Widget Set Version %d.%d pl %d\n",
	     AtVERSION, AtREVISION, AtPATCHLEVEL);
     fprintf(f,"%%%%CreationDate: %s",asctime(localtime(&now)));
#ifndef VMS
     fprintf(f,"%%%%For: %s\n", ((struct passwd *) getpwuid(getuid()))->pw_name);
#else
     fprintf(f, "%%%%For: %s\n", cuserid(user_name));
#endif
     fprintf(f,"%%%%BoundingBox: %d %d %d %d\n", x1, y1, x2, y2);
     fprintf(f,"%%%%Pages: 1\n");
     fprintf(f,"%%%%EndComments\n");

     /* Do prolog */
     fprintf(f, prolog);
     fprintf(f, "%%%%EndProlog\n%%%%Page: 1\n");

     if (landscape) {
	  fprintf(f, "90 rotate\n");
	  fprintf(f, "0 -612 translate\n");
     }
     /* Do the body of the postscript */
     AtPlotterDrawPS(f, (Widget) pw, x1, y1, x2, y2);

     /* Finish up */
     fprintf(f, "showpage\n");

     if (f != stdout)
	  fclose(f);

     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}

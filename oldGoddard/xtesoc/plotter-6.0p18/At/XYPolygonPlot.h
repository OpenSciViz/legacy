/*
 *      XYPolygonPlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 17:12:39 1993
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYPolygonPlot.h"
 */

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#ifndef _At_XYPolygonPlot_h
#define _At_XYPolygonPlot_h

#include <X11/At/At.h>
#include <X11/At/XYPlot.h>
#include <X11/At/BarPlot.h>

/* Declare specific AtPolygonPlotWidget class and instance datatypes */

typedef struct _AtXYPolygonPlotClassRec*     AtXYPolygonPlotWidgetClass;
typedef struct _AtXYPolygonPlotRec*          AtXYPolygonPlotWidget;

/* Declare the class constant */

externalref WidgetClass atXYPolygonPlotWidgetClass;

#ifndef AtIsXYPolygonPlot
#define AtIsXYPolygonPlot(w) XtIsSubclass(w, atXYPolygonPlotWidgetClass)
#endif

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Creation function
 */

extern Widget AtCreateXYPolygonPlot P((Widget, char *, Arg *, Cardinal));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 *   The member procedures. These ones are inherited from XYPlot
 */

#define AtXYPolygonPlotAttachData(w, xp, xt, xstride, yp, yt, ystride, start, num) \
     AtXYPlotAttachData((Widget) w, xp, xt, xstride, \
				    yp, yt, ystride, start, num)
#define AtXYPolygonPlotExtendData(w, n) \
     AtXYPlotExtendData((Widget) w, n)

/*
 *   The member procedures for AtxxPoint data
 */

#define AtXYPolygonPlotAttachDoublePoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtDouble, sizeof(AtDoublePoint), \
	  (XtPointer) &(data)->y, AtDouble, sizeof(AtDoublePoint), \
	  start, num)

#define AtXYPolygonPlotAttachFloatPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtFloat, sizeof(AtFloatPoint), \
	  (XtPointer) &(data)->y, AtFloat, sizeof(AtFloatPoint), \
	  start, num)

#define AtXYPolygonPlotAttachIntPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtInt, sizeof(AtIntPoint), \
	  (XtPointer) &(data)->y, AtInt, sizeof(AtIntPoint), \
	  start, num)

#endif /* _At_XYPolygonPlot_h */

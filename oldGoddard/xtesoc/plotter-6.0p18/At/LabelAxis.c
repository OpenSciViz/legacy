/*
 *      LabelAxis.c
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Some minor changes.
 */
static char SCCSid[] = "@(#) Plotter V6.0  93/07/23  LabelAxis.c";

/*

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 * The LabelAxis class decides the location and label of the tic
 * marks by looking at the application-supplied data.
 */


#include <X11/At/LabelAxisP.h>

static void LabelAxisInitialize P((AtLabelAxisWidget, AtLabelAxisWidget));

static Boolean LabelAxisSetValues P((AtLabelAxisWidget, AtLabelAxisWidget,
				  AtLabelAxisWidget));

static void LabelAxisRangeProc P((AtAxisCoreWidget, double *minp,
				 double *maxp, double *tip,
				 int *nwp));
static void LabelAxisCalcProc P((AtAxisCoreWidget));

/* No new resources */

AtLabelAxisClassRec atLabelAxisClassRec = {
  { /* core fields */
     /* superclass              */      (WidgetClass) &atAxisCoreClassRec,
     /* class_name              */      "AtLabelAxis",
     /* widget_size             */      sizeof(AtLabelAxisRec),
     /* class_initialize        */      NULL,
     /* class_part_initialize   */      NULL,
     /* class_inited            */      FALSE,
     /* initialize              */      (XtInitProc) LabelAxisInitialize,
     /* initialize_hook         */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      0,
     /* resources               */      NULL,
     /* num_resources           */      0,
     /* xrm_class               */      NULLQUARK,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* pad                     */      FALSE,
     /* destroy                 */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* set_values              */      (XtSetValuesFunc) LabelAxisSetValues,
     /* set_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* get_values_hook         */      NULL,
     /* pad                     */      NULL,
     /* version                 */      XtVersion,
     /* callback_private        */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL,
     /* pad                     */      NULL
     },
  { /* atPlot fields */
     /* draw                    */      XtInheritDraw,
     /* draw_icon               */      XtInheritDrawIcon,
     /* drawPS                  */      XtInheritDrawPS,
     /* draw_iconPS             */      XtInheritDrawIconPS,
     /* recalc                  */      XtInheritRecalc
  },
  { /* labelAxis fields */
     /* range_proc              */      (AtAxisRangeProc) LabelAxisRangeProc,
     /* calc_proc               */      (AtAxisCalcProc) LabelAxisCalcProc
  }
};

WidgetClass atLabelAxisWidgetClass = (WidgetClass)&atLabelAxisClassRec;

/*****************************************************************
 *
 * The core member procs
 */

static void LabelAxisInitialize(req, new)
AtLabelAxisWidget req, new;
{
     new->labelaxis.num_items = new->labelaxis.start = 0;
     *SCCSid = *SCCSid;       /* Keep gcc quiet */
}


static Boolean LabelAxisSetValues(old, req, new)
AtLabelAxisWidget old, req, new;
{
#define Changed(fld)      (old->labelaxis.fld != new->labelaxis.fld)
#define AC_Changed(fld) (old->axiscore.fld != new->axiscore.fld)

     Boolean renum = False;

     if (AC_Changed(font_family) ||
	 AC_Changed(number_size)) {
	  renum = True;
     }

     if (renum) {
	  new->axiscore.numbers_changed = True;
	  AtPlotterRescaleRequired((Widget)new);
     }

     return False;
}

/*****************************************************************
 *
 * The LabelAxis member functions
 */

/*
 * The range proc just makes sure they are integer bounds
 */
static void LabelAxisRangeProc(w, minp, maxp, tip, nwp)
AtAxisCoreWidget w;
double *minp, *maxp, *tip;
int *nwp;
{
     AtAxisCorePart *ac = &((AtLabelAxisWidget)w)->axiscore;
     AtLabelAxisPart *la = &((AtLabelAxisWidget)w)->labelaxis;

     if (ac->auto_scale) {
	  *minp = floor(*minp);
	  *maxp = ceil(*maxp);
	  *tip = 0;
     } else {
	  *minp = la->start;
	  *maxp = la->start + la->num_items - 1;
	  *tip = 0;
     }
}


/*
 * The calc proc just copies the existing entries
 */
static void LabelAxisCalcProc(acw)
AtAxisCoreWidget acw;
{
     AtAxisCorePart *ac = &acw->axiscore;
     AtLabelAxisPart *la = &((AtLabelAxisWidget)acw)->labelaxis;
     int i, ni, imin, imax, end;
     String *p;

     imin = floor(ac->min);
     imax = ceil(ac->max);

     /* First, calculate the number of tics */
     ni = 0;

     for (i = Min(imin, la->start), end = la->start + la->num_items;
	  i < end || i <= imax; i++) {
	  if (i < imin) continue;
	  if (i > imax) break;
	  p = (String *)((char *)la->data + (i - la->start) * la->stride);
	  if (i == imin) ni++;
	  else if (i == imax) ni++;
	  else if (p && *p && **p) ni++;
     }
     ac->num_tics = ni;
     ac->num_subtics = 0;       /* For now! */

     /* Now allocate everything */
     ac->tic_values =
	  (double *)XtMalloc(sizeof (double) * ac->num_tics);
     ac->subtic_values =
	  (double *)XtMalloc(sizeof (double) * ac->num_subtics);
     ac->tic_label_string =
	  (String *)XtMalloc(sizeof (String) * ac->num_tics);

     ni = 0;
     for (i = Min(imin, la->start), end = la->start + la->num_items;
	  i < end || i <= imax; i++) {
	  if (i < imin) continue;
	  if (i > imax) break;
	  assert(ni < ac->num_tics);
	  p = (String *)((char *)la->data + (i - la->start) * la->stride);
	  if (i == imin) {
	       ac->tic_values[ni] = i;
	       ac->tic_label_string[ni++] =
		    XtNewString((p && *p && **p) ? *p : "");
	  } else if (i == imax) {
	       ac->tic_values[ni] = i;
	       ac->tic_label_string[ni++] =
		    XtNewString((p && *p && **p) ? *p : "");
	  } else if (p && *p && **p) {
	       ac->tic_values[ni] = i;
	       ac->tic_label_string[ni++] = XtNewString(*p);
	  }
     }
     assert(ni == ac->num_tics);
}



/*
 *   The member routine
 */

void AtLabelAxisAttachData(w, data, stride, start, num)
Widget w;
String *data;
Cardinal stride, start, num;
{
     AtLabelAxisWidget law = (AtLabelAxisWidget) w;
     AtLabelAxisPart *la = &law->labelaxis;

     XtCheckSubclass((Widget)law, atLabelAxisWidgetClass,
		     "AtLabelAxisAttachData needs an AtLabelAxisWidget");

     la->data = data;
     la->stride = stride;
     la->start = start;
     la->num_items = num;

     law->axiscore.numbers_changed = True;
     AtPlotterRescaleRequired((Widget)law);
}

/*
 *   Creation function
 */

Widget AtCreateLabelAxis(parent, name, arglist, argcount)
Widget parent;
char *name;
Arg *arglist;
Cardinal argcount;
{
     return (XtCreateWidget(name, atLabelAxisWidgetClass, parent, arglist, argcount));
}

/*
 *      patchlevel.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Sun Jul 19 20:03:23 1992, V6-beta patchlevel 1
 *      klin, Mon Jul 27 14:45:49 1992, V6-beta patchlevel 2
 *      klin, Sun Aug  2 18:25:06 1992, V6-beta patchlevel 3
 *      klin, Fri Aug  7 10:44:41 1992, V6.0
 *      klin, Sat Aug 15 14:21:19 1992, patchlevel 4
 *      klin, Fri Dec 11 16:28:16 1992, patchlevel 5
 *      klin, Thu Jan 28 14:31:01 1993, patchlevel 6
 *      klin, Fri Feb 12 08:58:24 1993, patchlevel 7
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/07/23  patchlevel.h"
 */

#ifndef _At_patchlevel_h
#define _At_patchlevel_h

#define AtVERSION       6
#define AtREVISION      0
#define AtPATCHLEVEL    8
#define AtVersion       (AtVERSION * 1000 + AtREVISION)

#endif /* _At_patchlevel_h */

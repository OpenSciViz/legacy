/*
 * $Header: defctype.y,v 1.7 1993/08/19 20:00:52 smh Exp $
 *
 * A grammar to parse C struct, union, and typedef declarations sufficient
 * for translation to Allegro Common Lisp DEF-C-TYPE forms.
 *
 * At present there is one shift/reduce conflicts reported in this grammar.
 *
 * A few bits and pieces of this program derive distantly from the
 * venerable cdecl program, author unknown.
 *
 * To Do:
 * 
 * Bitfields are not yet implemented.
 * 
 * Enums are not yet implemented.
 * 
 * Parenthesized declarator lists are not handled everywhere.
 * 
 * Error recovery in this grammar is inadequate.  The grammar is believed
 * to parse syntactically correct C programs, but there will likely be
 * cascading errors if a C program is incorrect.
 * 
 * The code could do a better job grinding the output, but this can easily
 * be done afterwards in your favorite text editor.
 * 
 * The code has not been rigorously checked for memory leaks.
 * Leaks won't matter for any conceivable C program.
 */

%{

#include <stdio.h>

#define YYDEBUG 1

#define	MB_UNSIGNED	0001
#define MB_INT		0002
#define MB_CHAR		0004
#define MB_FLOAT	0010
#define MB_DOUBLE	0020
#define	MB_SHORT	0040
#define	MB_LONG		0100
#define	MB_SC		0200

int modbits;
char *ds(), *cat();
char *malloc();
char *saved_slot;
char *declarator_name;
char *mbcheck();
char *types[96] = {
  NULL, ":unsigned-long", ":long", ":unsigned-long",
  ":char", ":char", NULL, NULL,
  ":single-float", NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  ":double-float", NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,

  ":short", ":unsigned-short", ":short", ":unsigned-short",
  NULL, NULL, NULL, NULL,
  ":single-float", NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,

  ":long", ":unsigned-long", ":long", ":unsigned-long",
  NULL, NULL, NULL, NULL,
  ":double-float", NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL,
  NULL, NULL, NULL, NULL

};

struct TypedefName {
    char *name;
    struct TypedefName * next;
} *TypedefNames;

%}

%union { char *dynstr; }

%token	<dynstr>	NAME TYPENAME NUMBER
%token	<dynstr>	STRUCT UNION ENUM RANDOM
%token	<dynstr>	INT CHAR FLOAT DOUBLE TYPEDEF UNSIGNED LONG SHORT
%token	<dynstr>	VOID CONST VOLATILE STATIC EXTERN
%type	<dynstr>	TypeSpec TypeSpec1 Declarator DeclaratorList
%type	<dynstr>	Decl0 Decl1 Struct
%type	<dynstr>	SWord TWord atom slots slot

%start prog

%%
prog:
|	prog stmt

stmt:	Struct ';'			{ BEGIN P; free ($1); }
|	Struct DeclaratorList ';'	{ BEGIN P; free ($1); }
|	TWord { modbits = 0; } TypeSpec Declarator
			{ struct TypedefName *new;
			  new = (struct TypedefName *)malloc(sizeof *new);
			  new->name = declarator_name;
			  new->next = TypedefNames;
			  TypedefNames = new;
			  printf("(ff:def-c-type %s %s %s)\n",
				 declarator_name, $4, $3);
			  BEGIN P; free($3); free($4);
			}
|	'(' prog ')'
|	'[' prog ']'
|	'{' prog '}'
|	RANDOM
|	';'
|	error ';'	{ modbits = 0; yyerrok; }	/* This is inadequate! */

Struct:	SWord '{' slots '}'	{ $$ = cat($1,$3,NULL,NULL);
				  free($1);
				  if (saved_slot)
				    { free(saved_slot);
				      saved_slot = NULL;
				    }
				}
|	SWord NAME 		{ free($1); $$ = $2; }
|	SWord NAME '{' slots '}'
			{ printf("(ff:def-c-type %s %s%s)\n\n", $2, $1, $4);
			  free($1); free($4); $$ = $2;
			  if (saved_slot)
			    { free(saved_slot);
			      saved_slot = NULL;
			    }
			}
SWord:	STRUCT			{ BEGIN S; $$ = ds(":struct"); }
|	UNION			{ BEGIN S; $$ = ds(":union"); }
TWord:	TYPEDEF			{ BEGIN S; }

slots:	/* empty */		{ $$ = ds("\n"); }
|	slots { modbits = 0; } slot ';'	{ $$ = cat($1, $3, NULL, NULL); }

slot:	TypeSpec Declarator	{ char *tmp;
				  if (saved_slot) free(saved_slot);
				  saved_slot = ds($1);
				  tmp = cat(declarator_name, $2, ds(" "), $1);
				  $$ = cat(ds(" ("), tmp, ds(")\n"), NULL);
				}
|	slot ',' Declarator	{ char *tmp;
				  tmp = cat(declarator_name, $3,
					    ds(" "), ds(saved_slot));
				  free(declarator_name);
				  $$ = cat($1,ds(" ("), tmp, ds(")\n"));
				}

DeclaratorList:	Declarator OptInit		{ free($1); }
|		DeclaratorList ',' Declarator	{ free($3); }

Declarator:	Decl0 NAME Decl1		{ declarator_name = $2;
						  $$ = cat($3,$1,NULL,NULL); }
|		Decl0 '(' Declarator ')' Decl1	{ $$ = cat($3,$1,$5,NULL); }
Decl0:				{ $$ = NULL; }
|		Decl0 '*'	{ $$ = cat($1,ds(" *"),NULL,NULL); }
Decl1:				{ $$ = NULL; }
|	Decl1 '(' stuff ')'	{ $$ = ds(" :function"); }
|	Decl1 '[' ']'		{ $$ = ds(" 0"); }
|	Decl1 '[' NUMBER ']'	{ $$ = cat(ds(" "),$3,$1,NULL); }

OptInit:
|	'=' stmt		/* stmt is over generous */

TypeSpec:
	{ modbits = 0; } TypeSpec1	{ $$ = $2; }

TypeSpec1:
	osc atomlst	{ char *tmp;
		      atomdecl:
			  if (modbits==MB_SC) modbits = MB_INT;
			  tmp = mbcheck();
			  if (tmp==NULL)
			      yyerror("Illegal C primitive type: 0%o\n", modbits);
			  else $$ = ds(tmp);
			}
|	osc Struct		{ $$ = $2; }
|	osc TYPENAME		{ $$ = $2; }
|	osc ENUM NAME error	{ yyerror("Can't hack ENUM yet.");}

atomlst:	atom1
|		atomlst atom1

atom:	INT		{ modbits |= MB_INT; }
|	CHAR		{ modbits |= MB_CHAR; }
|	FLOAT		{ modbits |= MB_FLOAT; }
|	DOUBLE		{ modbits |= MB_DOUBLE; }
|	UNSIGNED	{ modbits |= MB_UNSIGNED; }
|	LONG		{ modbits |= MB_LONG; }
|	SHORT		{ modbits |= MB_SHORT; }

atom1:	atom
|	sc		{ modbits |= MB_SC; }

sc:	VOID
|	CONST
|	VOLATILE
|	STATIC
|	EXTERN
osc:	
|	sclst
sclst:	sc		{ modbits |= MB_SC; }
|	sclst sc

stuff:		{ BEGIN B; } stuff1 { BEGIN S; }
stuff1:
|		stuff1 RANDOM
|		stuff1 '(' stuff1 ')'
|		stuff1 '[' stuff1 ']'
|		stuff1 '{' stuff1 '}'

%%

#include "lex.yy.c"

#define LORS	(MB_LONG|MB_SHORT)
#define UORL	(MB_UNSIGNED|MB_LONG)
#define UORS	(MB_UNSIGNED|MB_SHORT)
#define CORL	(MB_CHAR|MB_LONG)
#define CORS	(MB_CHAR|MB_SHORT)
#define CORU	(MB_CHAR|MB_UNSIGNED)

char *
mbcheck() {
    modbits &= ~MB_SC;
    if ((modbits&LORS) == LORS)
	unsupp("conflicting 'short' and 'long'",NULL);
    /*
    if ((modbits&UORL) == UORL)
	unport("unsigned with long (Ritchie)");
    if ((modbits&UORS) == UORS)
	unport("unsigned with short (Ritchie)");
    if ((modbits&CORU) == CORU)
	unport("unsigned char (Ritchie)");
	*/
    if ((modbits&CORL) == CORL)
	unsupp("long char",NULL);
    if ((modbits&CORS) == CORS)
	unsupp("short char",NULL);
    return(types[modbits]);
}

yyerror(str)
     char *str;
{
  fprintf(stderr,"Line %d: ", yylineno);
  fprintf(stderr,str,*(&str+1),*(&str+2),*(&str+3),*(&str+4),*(&str+5));
  fprintf(stderr,"\n");
}
    
char *malloc();

int gac;
char **gav;

main(ac,av)
     char **av;
{
  gac = ac; gav = av;
  if (ac>2) help();
  if (ac==2) {
    if (freopen(av[1],"r",stdin)==NULL) {
      fprintf(stderr, "Unable to open input file: %s\n", av[1]);
      exit(1);
    }
  }
  BEGIN P;
  return(yyparse());
}

help() {
  fprintf(stderr,"Usage: %s [file]\n", *gav);
  exit(1);
}


unsupp(s,hint)
     char *s, *hint;
{
  fprintf(stderr,"Line %d: Warning: Unsupported in C -- %s\n",yylineno,s);
  if (hint != NULL)
    printf("\t(maybe you mean \"%s\")\n",hint);
}

unport(s)
     char *s;
{
  fprintf(stderr,"Line %d: Warning: Non-portable construction -- %s\n",yylineno,s);
}

yywrap()
{
  return 1;
}

/*
 * Support for dynamic strings:
 * cat creates a string from three input strings.
 * The input strings are freed by cat (so they better have been malloced).
 * ds makes a malloced string from one that's not.
 */

char *
cat(s1,s2,s3,s4)
     char *s1,*s2,*s3,*s4;
{
  register char *newstr;
  register unsigned len = 1;

  if (s1 != NULL) len += strlen(s1);
  if (s2 != NULL) len += strlen(s2);
  if (s3 != NULL) len += strlen(s3);
  if (s4 != NULL) len += strlen(s4);
  newstr = malloc(len);
  strcpy(newstr,"");
  if (s1 != NULL) {
    strcat(newstr,s1);
    free(s1);
  }
  if (s2 != NULL) {
    strcat(newstr,s2);
    free(s2);
  }
  if (s3 != NULL) {
    strcat(newstr,s3);
    free(s3);
  }
  if (s4 != NULL) {
    strcat(newstr,s4);
    free(s4);
  }
  return newstr;
}

char *
ds(s)
     char *s;
{
  register char *p;

  p = malloc((unsigned)(strlen(s)+1));
  strcpy(p,s);
  return p;
}

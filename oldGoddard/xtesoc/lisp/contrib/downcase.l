%{
/* downcase -- A Common Lisp case unifier.
 * smh		-[]-
 * This quick hack is a filter for Common Lisp source code to allow
 * code with inconsistent mixedcase to be compiled and run in a
 * case-sensitive-lower Allegro CL image.
 * It takes standard CL with mixed case and downcases all the code, leaving
 * comments and strings alone.  Any symbol that has backslash or vertical-bar
 * escapes is also left alone.  This probably doesn't cope with all standard
 * lisp syntax, so send bugs to smh@franz.com and I'll try to fix them.
 * But what do you want from a program that's nearly all punctuation?
 *
 * This does handle standard syntax strings, comments, #| ... |#, symbols
 * with multiple escape chars, and single escape chars everywhere it should.
 * It does not handle nested #| ... |# constructs.
 *
 * After running a case-insensitive Common Lisp source file through this
 * filter, here are the things for which you typically still need to search.
 * (The quoted strings are literals for which you might search.)
 *   Calls to "-package" functions with upper-case string args.
 *   Calls to "(intern" and "(find-symbol" with string args built
 * typicaly using calls to format nil.  In addition, it might be useful to
 * search for "(format ()" and "(format nil".
 *
 * To build:
 *   lex downcase.l; cc -O -o downcase lex.yy.c -ll
 *
 * $Header: downcase.l,v 1.6 1993/08/19 20:00:54 smh Exp $
 */

#include <ctype.h>
#define P	{ printf("%s",yytext); }

%}
A	[-A-Za-z0-9./\$%\^&\*\?\!\@\[\]\~\=\+\_\<\>\,]

%%

\#\|([^|]|(\|[^#])|\n)*\|\#	{ P }
{A}+				{ char *s;
				  for ( s=yytext; *s; s++)
				      if (isupper(*s))
					  *s = tolower(*s);
				  P }
\\(.|\n)			{ P }
;.*\n				{ P }
\|([^\\|]|(\\(.|\n)))*\|	{ P }
\"([^\\"]|(\\(.|\n)))*\"	{ P }

%%

#define YYLMAX (50000)		/* handle large #| ... |# */

main(ac,av)
 char **av;
{
    if (ac>2) usage(av[0]);
    if (ac==2) {
	if (freopen(av[1],"r",stdin)==NULL) {
	    fprintf(stderr,"%s: can't open %s\n", av[0], av[1]);
	    exit(1);
	}
    }
    yylex();
}

usage(s)
    char *s;
{
    fprintf(stderr, "Usage: %s [input file]\n", s);
}

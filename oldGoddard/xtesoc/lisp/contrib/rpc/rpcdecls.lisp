;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCDECLS")

(in-package "RPC2")

;; Macros useful for low-level RPC hacking.

(defmacro getbase-integer (base byteoffset)
  "Interpret 32 bits at BYTEOFFSET from BASE as a signed integer."
  `(let ((base (il:\\addbase ,base (foldlo ,byteoffset 
					   *bytes-per-word*))))
     (il:\\makenumber (il:\\getbase base 0)
		      (il:\\getbase base 1))))

(defmacro getbase-unsigned (base byteoffset)
       
  "Interpret 32 bits at BYTEOFFSET from BASE as an unsigned integer."
  ;; This differs from GETBASE-INTEGER only when the high bit is
  ;; on, in which case we are forced to make (choke) a bignum,
  ;; which we try to do efficiently.
  `(let* ((base (il:\\addbase ,base (foldlo ,byteoffset *bytes-per-word*)))
	  (hi (il:\\getbase base 0)))
     (if (> hi 32767)
	 (bignum-make-number hi (il:\\getbase base 1))
	 (il:\\makenumber hi (il:\\getbase base 1)))))

(defmacro integer-from-bytes (byte0 byte1 byte2 byte3)
  "Interprets these 32 bits as a signed integer"
  `(il:\\makenumber (+ (unfold ,byte0 256) ,byte1)
		    (+ (unfold ,byte2 256) ,byte3)))

(defmacro unsigned-from-bytes (byte0 byte1 byte2 byte3)
  "Interprets these 32 bits as an unsigned integer"
  `(let* ((hi (+ (unfold ,byte0 256) ,byte1))
	  (lo (+ (unfold ,byte2 256) ,byte3)))
     (if (> hi 32767)
	 (bignum-make-number hi lo)
	 (il:\\makenumber hi lo))))

(defmacro unsigned-from-signed (value)
  "Interpret the 32 bits of VALUE's representation as an unsigned integer."
  `(let ((value ,value))
     (if (> 0 value)
	 (+ value twoto32nd)
	 value)))

(defmacro putbase-integer (base byteoffset value)
  "Store integer VALUE at BYTEOFFSET bytes beyond BASE."
  ;; Note this handles both "signed" and "unsigned" numbers.  We
  ;; do type analysis here to avoid gratuitous consing when
  ;; handling anything large.
  `(let ((base (il:\\addbase ,base (foldlo ,byteoffset *bytes-per-word*)))
	 (value ,value))
     (cond ((il:smallp value)
					; An immediate value
	    (il:\\putbase base 0 (if (< value 0) 65535 0))
	    (il:\\putbase base 1 (il:\\loloc value)))
	   ((eq (il:ntypx value) il:\\fixp)
					; A 32-bit integer
					; box--just blt it
	    (il:\\blt base value 2))
	   (t (putbase-bignum base value)))))

(defmacro foldlo (form divisor)
  (let ((div (if (constantp divisor) (eval divisor) divisor)))
    (or (and div (il:poweroftwop div))
	(il:\\illegal.arg div))
    (list 'il:lrsh form (il:sub1 (il:integerlength div)))))

(defmacro unfold (form divisor)
  (let ((div (if (constantp divisor) (eval divisor) divisor)))
    (or (and div (il:poweroftwop div))
	(il:\\illegal.arg div))
    (list 'il:llsh form (il:sub1 (il:integerlength div)))))

(defmacro vector-base (vector)
  "Get raw string/vector base address.  Use VECTOR-OFFSET, too,
 unless you know this is a brand new one without displacement."
  `(il:|fetch| (il:oned-array il:base) il:|of| ,vector))

(defmacro vector-offset (vector)
  "Get raw vector offset.  Interpretation depends on element type, of course."
  `(il:|fetch| (il:oned-array il:offset) il:|of| ,vector))

(defmacro padding-bytes (bytecount)
  "Returns number of bytes needed to pad BYTECOUNT bytes
 out to a multiple of 32 bits."
  `(let ((n ,bytecount))
     (- (logand (+ n 3) -4) n)))


;;;; Call methods

(defmacro rpc-method (op stream)
  "Returns the function that implements OP (unevaluated) on STREAM."
  `(,(intern (concatenate 'string "RPC-METHODS-" (string op)) "RPC2")
     (rpc-stream-methods ,stream)))

(defmacro rpc-call-method (op &rest args)
  "Invoke the OP method on ARGS,
 the first of which must be the RPC-STREAM that defines the method"
  `(funcall (rpc-method ,op ,(first args)) ,@args))

(defmacro reinitialize-rpcstream (stream destaddr destsocket)
  "Reuse an existing RPC Stream to send a new packet.
Resets length counters, reinitializes packets, etc."
  `(rpc-call-method initialize ,stream ,destaddr ,destsocket))

(defmacro getbyte (xdrstream)
  "Applies the GETBYTE method of an RPC Stream to read in and return the next byte of the stream."
  `(rpc-call-method getbyte ,xdrstream))

(defmacro getrawbytes (xdrstream base offset nbytes)
  "Applies the GETRAWBYTES method of an RPC stream to read NBYTES bytes from the stream to BASE,OFFSET."
  `(rpc-call-method getrawbytes ,xdrstream ,base ,offset ,nbytes))

(defmacro skipbytes (rpcstream nbytes)
  "Applies the SKIPBYTES method of an RPC stream to skip NBYTES bytes of input."
  `(rpc-call-method skipbytes ,rpcstream ,nbytes))

(defmacro getcell (xdrstream)
  "Applies the GETCELL method of an RPC Stream to read in and return the next cell of the stream. A cell is a 32-bit two's complement integer."
  `(rpc-call-method getcell ,xdrstream))

(defmacro getoffset (xdrstream)
  "Returns dotted pair (base . byteoffset), pointing at current position in incoming packet"
  `(rpc-call-method getoffset ,xdrstream))

(defmacro putbyte (rpcstream value)
  "Applies the PUTBYTE method of an RPC Stream to write the byte VALUE on that stream. VALUE is an integer between 0 and 255 inclusive."
  `(rpc-call-method putbyte ,rpcstream ,value))

(defmacro putrawbytes (rpcstream base offset nbytes)
  "Applies the PUTRAWBYTES method of an RPC stream to write the NBYTES bytes from BASE,OFFSET to the stream."
  `(rpc-call-method putrawbytes ,rpcstream ,base ,offset ,nbytes))

(defmacro zerobytes (rpcstream nbytes)
  "Applies the ZEROBYTES method of an RPC stream to write NBYTES bytes of zero to the output."
  `(rpc-call-method zerobytes ,rpcstream ,nbytes))

(defmacro putcell (rpcstream value)
  "Applies the PUTCELL method of an RPC Stream to write the cell VALUE on that stream. A cell is a 32-bit two's complement integer."
  `(rpc-call-method putcell ,rpcstream ,value))

(defmacro getunsigned (rpcstream)
  "Fetch an unsigned 32-bit integer from RPCSTREAM.  Uses the GETUNSIGNED method."
  `(rpc-call-method getunsigned ,rpcstream))

(defmacro putunsigned (rpcstream value)
  "Write a 32-bit unsigned integer to RPCSTREAM.  Uses PUTCELL method."
  ;; Note that no coercion is need here, because the bits of the
  ;; bignum are the same as the bits of the signed integer.
  `(putcell ,rpcstream ,value))

(defconstant *words-per-cell* 2 "The number of words (16 bits) per cell.")

(defconstant *bytes-per-cell* 4 "Number of 8-bit bytes per RPC cell.")

(defconstant *bytes-per-word* 2)


;;;; Well-known RPC constants

(defconstant *rpc-msg-call* 0
  "Constant 0 in packet means RPC call, 1 means reply")

(defconstant *rpc-msg-reply* 1)

(defconstant *rpc-reply-accepted* 0 "Switch in reply body")

(defconstant *rpc-reply-rejected* 1 "Switch in reply body")

(defconstant *rpc-accept-success* 0 "Switch in accepted reply.")

(defconstant *rpc-accept-program-unavailable* 1)

(defconstant *rpc-version* 2 "This code will only work for SUN RPC Version 2")

(defconstant *internal-time-units-per-msec*
  (/ internal-time-units-per-second 1000)
  "This gets used in EXCHANGE-UDP-PACKETS.")

(defconstant *portmapper-socket* 111 "Well-known socket for portmapper")



;;;; For those that need IP/TCP stuff

(defun load-tcp-exports nil
  (prog1 (il:filesload (il:source) il:tcpexports)
					; Now stop us from loading
					; it again (This really
					; ought to be on
					; TCPEXPORTS)
    (or (get 'il:tcpexports 'il:filedates)
	(setf (get 'il:tcpexports 'il:filedates) t))))

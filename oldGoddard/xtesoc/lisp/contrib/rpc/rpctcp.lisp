;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCTCP")

(in-package "RPC2")

;; Definitions for TCP transport of RPC

(eval-when (eval compile)
       
       ;; Translated (il:files il:rpcdecls) to require forms
       (require "RPCDECLS")
       (load-tcp-exports))

(export '*bytes-per-rm-outrec*)

(defconstant *bytes-per-rm-outrec* 8192 "Size of string in which to store outgoing RPC/RM/TCP/IP messages fragments.")

(defglobalparameter *tcp-rpc-methods* (make-rpc-methods :protocol
                                             'tcp :getbyte 
                                             'tcp-getbyte :getcell
                                             'tcp-getcell :getunsigned
                                             'tcp-getunsigned :putbyte
                                             'tcp-putbyte :putcell
                                             'tcp-putcell :skipbytes
                                             'tcp-skipbytes :zerobytes
                                             'tcp-zerobytes 
                                             :getrawbytes 
                                             'tcp-getrawbytes 
                                             :putrawbytes 
                                             'tcp-putrawbytes :close
                                             'tcp-close :initialize
                                             'tcp-initialize :exchange
                                             'exchange-tcp-packets))

(defun rpc-create-tcp-stream (destaddr destsocket)
       "Create a new RPC Stream with the vector of functions handling a bi-directional TCP stream between the devices."
       (let* ((ostr (il:tcp.open
                     (if (integerp destaddr)
                         destaddr
                         (setq destaddr (rpc-resolve-host destaddr)))
                     (if (integerp destsocket)
                         destsocket
                         (setq destsocket (rpc-find-socket
                                           destaddr
                                           (rpc-resolve-prog
                                            destsocket nil 'tcp)
                                           'tcp)))
                     nil
                     'il:active
                     'il:output))
              (rpcstream (make-rpc-stream :methods *tcp-rpc-methods* 
                                :outstream ostr :instream (
                                                    il:tcp.other.stream
                                                           ostr)
                                :outstring
                                (il:\\allocblock (foldlo 
                                                  *bytes-per-rm-outrec*
                                                        
                                                       *bytes-per-cell*
                                                        ))
                                :destaddr destaddr :destsocket 
                                destsocket)))
             (tcp-initialize rpcstream destaddr destsocket)
             rpcstream))

(defun tcp-initialize (rpcstream destaddr destsocket)
       (declare (ignore destaddr destsocket))
       
       ;; Prepare RPCSTREAM for new output.  All we need to do is
       ;; leave OUTBYTEPTR pointing after the 4 bytes we're saving for
       ;; the header.  Scratch buffer is in the field OUTSTRING, put
       ;; there when the stream was created.
       (setf (rpc-stream-outbyteptr rpcstream)
             4))

(defun tcp-close (rpcstream)
       "Deallocate an RPC Stream. Tries to cleanup after itself."
       (close (rpc-stream-outstream rpcstream))
       (close (rpc-stream-instream rpcstream)))

(defun
 exchange-tcp-packets
 (rpcstream errorflg xid)
 "Given the specified timeout, this routine writes onto the TCP stream and 
waits until it either gets a reply or times out.
"
 
 ;; Yes, I know EXCHANGE-TCP-PACKETS is a misnomer, but I wanted it to
 ;; parallel Exchange-UDP-Packets
 (let* ((outstream (rpc-stream-outstream rpcstream))
        (instream (rpc-stream-instream rpcstream))
        (timeout *msec-until-timeout*)
        (timer (il:setuptimer (or timeout 60000)))
        (event (il:tcp.socket.event (il:tcp.stream.socket outstream)))
        (debugme (and (numberp *debug*)
                      (> *debug* 4)))
        (timeout-count 0))
       (when debugme (break "Ready to write to tcp stream"))
       (rm-forceoutput rpcstream t)
                                           ; Finish whatever fragment
                                           ; we have
       (il:forceoutput outstream)
       (when debugme (format-t 
                     "Output forced out. Will wait ~a msec for reply~%"
                            timeout))
       (loop (il:await.event event timer t)
             (cond ((listen instream)
                    (rm-new-input-record rpcstream)
                    (unless (= (getunsigned rpcstream)
                               xid)
                           (error 
                                 "RPC Reply ID does not match sent ID."
                                  ))
                    (return t))
                   (t (when (and timeout (il:timerexpired? timer))
                                           ; Timed out
                          (let ((result (rpc-handle-timeout
                                         rpcstream
                                         (incf timeout-count)
                                         errorflg)))
                               (unless (eq result :continue)
                                      (return result))))
                      (il:setuptimer (or timeout 60000)
                             timer)
                                           ; Reset the timer and keep
                                           ; going
                      )))))

(defun rm-new-input-record (rpcstream)
       "Call at start of new rm record.  Consumes the record length and stores it in INBYTEPTR."
       
       ;; TCP XDR streams consist of a sequence of "records", one per
       ;; transaction.  Each record contains one or more fragments. 
       ;; The first bit of each fragment is the "last fragment" flag;
       ;; the next 31 bits give the length in bytes of the rest of the
       ;; fragment.
       
       ;; In our management of TCP streams, we use the INBYTEPTR field
       ;; to hold the number of bytes remaining in the fragment on
       ;; input, or the current length of the fragment (including the
       ;; 4 byte length) on output.
       (let* ((instream (rpc-stream-instream rpcstream))
              (nbytes (integer-from-bytes (logand (il:bin instream)
                                                 127)
                                           ; Kill last fragment bit,
                                           ; if any.
                             (il:bin instream)
                             (il:bin instream)
                             (il:bin instream))))
             (when *debug* (format-t "RM Record is to be ~d bytes.~%" 
                                  nbytes))
             (setf (rpc-stream-inbyteptr rpcstream)
                   nbytes)))

(defun tcp-getbyte (rpcstream)
       "Read in one byte from an RM Record"
       (let ((bytesleft (rpc-stream-inbyteptr rpcstream)))
            (when (eql bytesleft 0)
                (rm-new-input-record rpcstream)
                (setq bytesleft (rpc-stream-inbyteptr rpcstream)))
            (setf (rpc-stream-inbyteptr rpcstream)
                  (1- bytesleft))
            (il:bin (rpc-stream-instream rpcstream))))

(defun tcp-getcell (rpcstream)
       "Read in a 4 byte signed integer."
       (let ((bytesleft (rpc-stream-inbyteptr rpcstream)))
            (if (< bytesleft 4)
                
                ;; Not enough bytes left in the record, so do it the
                ;; slow way.
                (integer-from-bytes (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream))
                (let ((stream (rpc-stream-instream rpcstream)))
                     (setf (rpc-stream-inbyteptr rpcstream)
                           (- bytesleft 4))
                     (integer-from-bytes (il:bin stream)
                            (il:bin stream)
                            (il:bin stream)
                            (il:bin stream))))))

(defun tcp-getunsigned (rpcstream)
       "Read in a 4 byte unsigned integer."
       (let ((bytesleft (rpc-stream-inbyteptr rpcstream)))
            (if (< bytesleft 4)
                
                ;; Not enough bytes left in the record, so do it the
                ;; slow way.
                (unsigned-from-bytes (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream)
                       (tcp-getbyte rpcstream))
                (let ((stream (rpc-stream-instream rpcstream)))
                     (setf (rpc-stream-inbyteptr rpcstream)
                           (- bytesleft 4))
                     (unsigned-from-bytes (il:bin stream)
                            (il:bin stream)
                            (il:bin stream)
                            (il:bin stream))))))

(defun tcp-getrawbytes (rpcstream base offset nbytes)
       "Read NBYTES bytes into a new string from as many RM records as needed."
       (let ((instream (rpc-stream-instream rpcstream))
             (first offset)
             bytesleft)
            
            ;; FIRST is the index at which to place the next byte.
            
            ;; NBYTES is the number of bytes remaining to be read.
            
            ;; BYTESLEFT is the number of  bytes remaining in the
            ;; current RM Record.
            (loop (when (<= nbytes (setq bytesleft (
                                                   rpc-stream-inbyteptr
                                                    rpcstream)))
                      
                      ;; Here is the normal case --- the string all
                      ;; comes from the same RM record.
                      (or (zerop nbytes)
                          (il:\\bins instream base first nbytes))
                      (decf (rpc-stream-inbyteptr rpcstream)
                            nbytes)
                      (return base))
                  (il:\\bins instream base first bytesleft)
                  (rm-new-input-record rpcstream)
                  (incf first bytesleft)
                  (decf nbytes bytesleft))))

(defun tcp-skipbytes (rpcstream nbytes)
       (let ((bytesleft (rpc-stream-inbyteptr rpcstream))
             (s (rpc-stream-instream rpcstream)))
            (cond ((< bytesleft nbytes)
                                           ; Crossing an RM boundary,
                                           ; so do it slowly
                   (dotimes (i nbytes)
                       (tcp-getbyte rpcstream)))
                  (t (setf (rpc-stream-inbyteptr rpcstream)
                           (- bytesleft nbytes))
                     (dotimes (i nbytes)
                         (il:bin s))))))

(defun tcp-putbyte (rpcstream byte)
       (let ((rmoffset (rpc-stream-outbyteptr rpcstream)))
            (unless (< rmoffset *bytes-per-rm-outrec*)
                (rm-forceoutput rpcstream nil)
                (setq rmoffset (rpc-stream-outbyteptr rpcstream)))
            (il:\\putbasebyte (rpc-stream-outstring rpcstream)
                   rmoffset byte)
            (setf (rpc-stream-outbyteptr rpcstream)
                  (1+ rmoffset))))

(defun tcp-putcell (rpcstream value)
       (let ((rmoffset (rpc-stream-outbyteptr rpcstream)))
            (when (> rmoffset (- *bytes-per-rm-outrec* 4))
                                           ; Don't have room for 4
                                           ; more bytes
                (rm-forceoutput rpcstream)
                (setq rmoffset (rpc-stream-outbyteptr rpcstream)))
            (putbase-integer (rpc-stream-outstring rpcstream)
                   rmoffset value)
            (setf (rpc-stream-outbyteptr rpcstream)
                  (+ rmoffset 4))))

(defun tcp-putrawbytes (rpcstream base offset nbytes)
       
       ;; Write a sequence of NBYTES bytes to the output stream.
       (let ((first offset)
             (scratch (rpc-stream-outstring rpcstream)))
            
            ;; FIRST is the index of the next byte to be written.
            
            ;; NBYTES is the number of bytes remaining to be written
            ;; out.
            (loop (let* ((rmoffset (rpc-stream-outbyteptr rpcstream))
                         (chunksize (min nbytes (- 
                                                  *bytes-per-rm-outrec*
                                                   rmoffset))))
                                           ; CHUNKSIZE is how much we
                                           ; can write in this record
                        (il:\\movebytes base first scratch rmoffset 
                               chunksize)
                        (setf (rpc-stream-outbyteptr rpcstream)
                              (+ rmoffset chunksize))
                        (when (eql 0 (decf nbytes chunksize))
                                           ; We've written it all
                            (return))
                        
                        ;; Have to flush this record and start a new
                        ;; one for the rest
                        (rm-forceoutput rpcstream nil)
                        (incf first chunksize)))))

(defun tcp-zerobytes (rpcstream nbytes)
       (let ((rmoffset (rpc-stream-outbyteptr rpcstream)))
            (when (> rmoffset (- *bytes-per-rm-outrec* nbytes))
                                           ; Don't have room for this
                                           ; many bytes
                (rm-forceoutput rpcstream)
                (setq rmoffset (rpc-stream-outbyteptr rpcstream)))
            (let ((buf (rpc-stream-outstring rpcstream)))
                 (dotimes (i nbytes)
                     (il:\\putbasebyte buf rmoffset 0)
                     (incf rmoffset))
                 (setf (rpc-stream-outbyteptr rpcstream)
                       rmoffset))))

(defun rm-forceoutput (rpcstream &optional final-fragment-flag)
       
       ;; Send the current record.  If FINAL-FRAGMENT-FLAG is true,
       ;; set the "final fragment" bit.
       (let* ((buffer (rpc-stream-outstring rpcstream))
              (total-length (rpc-stream-outbyteptr rpcstream))
              (net-length (- total-length 4)))
             (putbase-integer buffer 0 net-length)
                                           ; Fill in the 4 bytes of
                                           ; header
             (when final-fragment-flag
                 
                 ;; If this is the final fragment of the RM record, OR
                 ;; in a one to high order bit of RM header.
                 (il:\\putbase buffer 0 (logior 32768 (il:\\getbase
                                                       buffer 0))))
             (il:\\bouts (rpc-stream-outstream rpcstream)
                    buffer 0 total-length)
                                           ; Now reinitialize the
                                           ; buffer
             (setf (rpc-stream-outbyteptr rpcstream)
                   4)))

                                           ; Low-level



;; Filepkg type il:fns not supported: (il:fns il:tcp.stream.socket
;; il:tcp.socket.event)


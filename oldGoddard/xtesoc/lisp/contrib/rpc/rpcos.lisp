;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCOS")

(in-package "RPC2")

;;;; UDP transport in Xerox Medley running on a SUN

(eval-when (eval compile)
  (require "RPCDECLS")
  (require "LLSUBRS"))


;;;; XDR data block

(il:datatype xdr-data-block ((xdr-public 500 il:word)))

(defconstant *cells-per-xdr-data-block* 250
  "Number of 32 bit cells in a data block.")

(defglobalparameter *free-xdr-data-blocks* () "A list of free xdr data blocks.")

(defglobalparameter *max-xdr-data-blocks* 5
  "The maximum size of the data block cache.")

(defun allocate-xdr-data-block ()
  "If an xdr data block is available then return it, otherwise create one."
  (or (pop *free-xdr-data-blocks*)
      (il:|create| xdr-data-block)))

(defun reclaim-xdr-data-block (xdr-data-block)
  (when (< (length *free-xdr-data-blocks*) *max-xdr-data-blocks*)
    (push xdr-data-block *free-xdr-data-blocks*))
  t)

(defglobalparameter *os-rpc-methods* 
    ;; These quotes should really be #', but in Xerox Lisp the
    ;; former is significantly nicer on some machines, and the
    ;; result is the same.
    (make-rpc-methods :protocol		'os-udp
		      :getbyte		'os-udp-getbyte 
		      :getcell		'os-udp-getcell
		      :getunsigned	'os-udp-getunsigned
		      :getrawbytes	'os-udp-getrawbytes 
		      :skipbytes	'os-udp-skipbytes
		      :getoffset	'os-udp-getoffset
		      :putbyte		'os-udp-putbyte
		      :putcell		'os-udp-putcell
		      :putrawbytes	'os-udp-putrawbytes 
		      :zerobytes	'os-udp-zerobytes
		      :close		'os-udp-close 
		      :initialize	'os-udp-initialize
		      :exchange		'os-exchange-udp-packets)
  "Operations vector for RPC streams using OS networking")

(defun create-os-udp-stream (&optional destaddr destsocket)
  (make-rpc-stream :protocol 'udp :methods *os-rpc-methods* 
		   :outstream (allocate-xdr-data-block)
		   :instream (allocate-xdr-data-block)
		   :inbyteptr 0 :outbyteptr 0
		   :destaddr
		   (and destaddr
			(if (integerp destaddr)
			    destaddr
			    (setq destaddr (rpc-resolve-host destaddr))))
		   :destsocket
		   (and destsocket
			(if (integerp destsocket)
			    destsocket
			    (rpc-find-socket
			     destaddr (rpc-resolve-prog destsocket nil 'udp)
			     'udp)))))

(defun os-exchange-udp-packets (rpcstream errorflg xid)
  (let ((timeout *msec-until-timeout*)
	(timeout-count 0))
    (loop
     (cond
       ((il:subrcall
	 il:rpc-call
	 (rpc-stream-destaddr rpcstream)
	 (rpc-stream-destsocket rpcstream)
	 (il:\\dtest (rpc-stream-outstream rpcstream) 'xdr-data-block)
	 (il:\\dtest (rpc-stream-instream rpcstream) 'xdr-data-block)
	 (or timeout 60000)
	 *msec-between-tries*
	 (rpc-stream-outbyteptr rpcstream))
	(setf (rpc-stream-inbyteptr rpcstream) 0)
	(when (and (numberp *debug*) (> *debug* 5))
	  (break "A reply is in (rpc-stream-instream *rpc-stream*)"))
	(unless (= (getunsigned rpcstream) xid)
	  (error "RPC Reply ID does not match sent ID."))
	(return t))
       (timeout				; timed out
	(let ((result
	       (rpc-handle-timeout rpcstream (incf timeout-count) errorflg)))
	  (unless (eq result :continue)
	    (return result))))))))

(defun os-resolve-host (destination)
  ;;  Convert an address from it's string representation into a number.
  (or (read-string-address destination) destination))

(defun os-udp-initialize (stream destaddr destsocket)
  (when destaddr
    (setf (rpc-stream-destaddr stream)
	  (if (integerp destaddr)
	      destaddr
	      (rpc-resolve-host destaddr))))
  (when destsocket
    (setf (rpc-stream-destsocket stream)
	  (if (integerp destsocket)
	      destsocket
	      (rpc-find-socket (rpc-stream-destaddr stream)
			       (rpc-resolve-prog destsocket nil 'udp)
			       'udp))))
  (setf (rpc-stream-outbyteptr stream) 0)
  (setf (rpc-stream-inbyteptr stream) 0))

(defun os-udp-close (rpcstream)
  (reclaim-xdr-data-block (rpc-stream-outstream rpcstream))
  (reclaim-xdr-data-block (rpc-stream-instream rpcstream)))

(defun read-string-address (string-or-atom)
  ;; This is a copy of \IP.READ.STRING.ADDRESS from TCPLLIP, so's we
  ;; don't have to load that file.
  (il:|for| il:char il:|instring| (il:mkstring string-or-atom)
      il:|bind| (il:result il:_ (il:ncreate 'il:fixp)) (il:index il:_ 0) byte
      il:|do|
      (il:|if| (> il:index 3)
	  il:|then|
					; Got 3 parts and there's
					; still more to go, must be
					; bad
	  (return nil)
	  il:|elseif|
	  (eq il:char (il:charcode il:\.))
	  il:|then|
	  (il:|if| byte il:|then| (il:\\putbasebyte il:result il:index 
						    byte))
	  (il:setq byte nil)
	  (il:|add| il:index 1)
	  il:|elseif|
	  (and (il:setq il:char (digit-char-p (int-char il:char)))
	       (< (il:setq byte (+ (il:|if| byte il:|then|
				       (il:times byte 10)
				       il:|else| 0)
				   il:char))
		  256))
	  il:|then|
					; Accumulated decimal
					; digit, and we haven't
					; overflowed a byte yet
	  il:|else|
					; Malformed
	  (return nil))
      il:|finally|
      (il:|if| byte il:|then| (il:\\putbasebyte il:result il:index byte)
	  (il:|add| il:index 1))
      (return (and (eq il:index 4)
		   il:result))))

(defun os-udp-getbyte (rpcstream)
  "Get a byte from the instream of the rpcstream and increment the offset."
  (let ((offset (rpc-stream-inbyteptr rpcstream)))
    (prog1 (il:\\getbasebyte
	    (il:locf (il:|fetch| xdr-public il:|of|
			 (il:\\dtest (rpc-stream-instream rpcstream)
				     'xdr-data-block)))
	    offset)
      (setf (rpc-stream-inbyteptr rpcstream)
	    (1+ offset)))))

(defun os-udp-putbyte (rpcstream byte)
  "Put a byte of data at the next position in the rpcstream and increment the offset."
  (let ((offset (rpc-stream-outbyteptr rpcstream)))
    (il:\\putbasebyte (il:locf (il:|fetch| xdr-public il:|of|
				   (il:\\dtest (rpc-stream-outstream rpcstream)
					       'xdr-data-block)))
		      offset byte)
    (setf (rpc-stream-outbyteptr rpcstream)
	  (1+ offset))))

(defun os-udp-getrawbytes (rpcstream base offset nbytes)
  "Get nbytes bytes from the rpcstream and increment the input pointer."
  (let ((xdr-data-block (il:\\dtest (rpc-stream-instream rpcstream)
                                    'xdr-data-block))
	(inptr (rpc-stream-inbyteptr rpcstream)))
    (il:\\movebytes (il:locf (il:|fetch| (xdr-data-block xdr-public)
				 il:|of| xdr-data-block))
		    inptr base offset nbytes)
    (setf (rpc-stream-inbyteptr rpcstream)
	  (+ inptr nbytes))
    base))

(defun os-udp-putrawbytes (rpcstream base offset nbytes)
  "Write NBYTES bytes into the outstream of rpcstream and increment the output pointer."
  (let ((xdr-data-block (il:\\dtest (rpc-stream-outstream rpcstream)
                                    'xdr-data-block))
	(outptr (rpc-stream-outbyteptr rpcstream)))
    (il:\\movebytes base offset (il:locf (il:|fetch| (xdr-data-block xdr-public)
					     il:|of| xdr-data-block))
		    outptr nbytes)
    (setf (rpc-stream-outbyteptr rpcstream)
	  (+ outptr nbytes))))

(defun os-udp-getcell (rpcstream)
  "Get a cell from the rpcstream and increment the offset."
  (let ((byteoffset (rpc-stream-inbyteptr rpcstream)))
    (cond ((and (>= byteoffset 0)
		(< byteoffset
		   (unfold *cells-per-xdr-data-block* *bytes-per-cell*)))
	   (setf (rpc-stream-inbyteptr rpcstream)
		 (+ byteoffset *bytes-per-cell*))
	   (getbase-integer
	    (il:locf (il:|fetch| (xdr-data-block xdr-public)
			 il:|of| (il:\\dtest (rpc-stream-instream rpcstream)
					     'xdr-data-block)))
	    byteoffset))
	  (t (error "Attempt to fetch cell outside of buffer.")))))

(defun os-udp-getunsigned (rpcstream)
  (let ((byteoffset (rpc-stream-inbyteptr rpcstream)))
    (cond ((and (>= byteoffset 0)
		(< byteoffset (unfold 
			       *cells-per-xdr-data-block*
			       *bytes-per-cell*)))
	   (setf (rpc-stream-inbyteptr rpcstream)
		 (+ byteoffset *bytes-per-cell*))
	   (getbase-unsigned (il:locf
			      (il:|fetch| (xdr-data-block
					   xdr-public)
				  il:|of|
				  (il:\\dtest (
					       rpc-stream-instream
					       rpcstream)
					      'xdr-data-block)))
			     byteoffset))
	  (t (error "Attempt to fetch cell outside of buffer.")
	     ))))

(defun os-udp-putcell (rpcstream value)
  (let ((byteoffset (rpc-stream-outbyteptr rpcstream)))
    (putbase-integer (il:locf (il:|fetch| (xdr-data-block
					   xdr-public)
				  il:|of|
				  (il:\\dtest (
					       rpc-stream-outstream
					       rpcstream)
					      'xdr-data-block)))
		     byteoffset value)
    (setf (rpc-stream-outbyteptr rpcstream)
	  (+ byteoffset 4))))

(defun os-udp-getoffset (rpcstream)
  (cons (rpc-stream-instream rpcstream)
	(rpc-stream-inbyteptr rpcstream)))

(defun os-udp-skipbytes (rpcstream nbytes)
  (incf (rpc-stream-inbyteptr rpcstream)
	nbytes))

(defun os-udp-zerobytes (rpcstream nbytes)
  (let ((offset (rpc-stream-outbyteptr rpcstream))
	(base (il:locf (il:|fetch| xdr-public il:|of|
			   (il:\\dtest (rpc-stream-outstream
					rpcstream)
				       'xdr-data-block)))))
    (dotimes (i nbytes)
      (il:\\putbasebyte base offset 0)
      (incf offset))
    (setf (rpc-stream-outbyteptr rpcstream)
	  offset)))

;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCPORTMAPPER")

(in-package "RPC2")

(eval-when (compile load eval)
  (defstruct mapsequence program vers protocol port))

(defun portmapperdef ()
  ;; Program that maps from programs to ports
  (il:nill)
					; Keep DEFUN from expanding
					; the DEFINE-REMOTE-PROGRAM
					; macro any more than
					; necessary
  (when nil
					; This is what the result
					; type of the DUMP protocol
					; is formally specified as,
					; but there's really no
					; excuse for using this
					; recursive definition in
					; practice. 
    '(mapstruct (:union :boolean (nil :void)
		 (t (:struct mapstruct (program :unsigned)
		     (vers :unsigned)
		     (prot :unsigned)
		     (port :unsigned)
		     (therest mapstruct))))))
  (define-remote-program
      'portmapper 100000 2 'udp :types
      '((mapsequence
	 (:sequence (:struct
		     mapsequence
		     (program :unsigned)
		     (vers :unsigned)
		     (protocol :unsigned)
		     (port :unsigned)))))
      :procedures
      '((null 0 nil nil)
	(lookup 3 (:unsigned :unsigned :unsigned :unsigned)
	 (:unsigned))
	(dump 4 nil (mapsequence))
	(indirect 5 (:unsigned :unsigned :unsigned :string)
	 (:unsigned :string))))

  ;; TCP version of same.  Sad that we need this redundancy.
  (define-remote-program
      'tcpportmapper 100000 2 'tcp :types
      '((mapsequence (:sequence (:struct mapsequence
				 (program :unsigned)
				 (vers :unsigned)
				 (protocol :unsigned)
				 (port :unsigned)))))
      :procedures
      '((null 0 nil nil)
	(lookup 3 (:unsigned :unsigned :unsigned :unsigned)
	 (:unsigned))
	(dump 4 nil (mapsequence))
	(indirect 5 (:unsigned :unsigned :unsigned :string)
	 (:unsigned :string)))))

(eval-when (load)
  (portmapperdef))

;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCSTRUCT")

(in-package "RPC2")

(eval-when (eval compile)
       
       ;; Translated (il:files il:rpcdecls) to require forms
       (require "RPCDECLS")
                                           ; For
                                           ; RPC-STREAM-PRINT-FUNCTION
       (load-tcp-exports))

(defun rpc-stream-print-function (obj stream depth)
       (let ((destaddr (rpc-stream-destaddr obj))
             (destsocket (rpc-stream-destsocket obj)))
            (format stream "#<RPC ~A conn~@[ to ~A~@[#~A~]~]>"
                   (rpc-stream-protocol obj)
                   (if (integerp destaddr)
                       (ipaddress-to-string destaddr)
                       destaddr)
                   (if (typep destsocket 'rpc-program)
                       (rpc-program-name destsocket)
                       destsocket))))

(defstruct (rpc-stream (:print-function rpc-stream-print-function))
       "Holds info and functions for encoding, decoding and executing remote procedure calls"
       protocol                            ; UDP, TCP, STRING, etc.
       ipsocket
                                           ; Local IP Socket
       destaddr
                                           ; Address of the
                                           ; destination.
       destsocket
                                           ; destination socket, or
                                           ; remote program whose
                                           ; socket should be looked
                                           ; up
       instream
                                           ; Incoming (Reply) Stream
                                           ; or Packet
       inbyteptr
                                           ; Byte Pointer to current
                                           ; position in incoming
                                           ; data. For UDP it is the
                                           ; byte pointer in the UDP
                                           ; packet. For TCP it is
                                           ; decremented, saying how
                                           ; many bytes left in this
                                           ; RM record before another
                                           ; RM header must be read.
       outstream
                                           ; UDP outgoing packet, or
                                           ; outgoing stream used by
                                           ; TCP and STRING.
       outbyteptr
                                           ; Byte Pointer to current
                                           ; position in UDP packet or
                                           ; outgoing buffer used to
                                           ; build RM record for TCP.
                                           ; See page 10 of "Remote
                                           ; Procedure Call Protocol
                                           ; Specification" for
                                           ; details of the RM 'Record
                                           ; Marking Standard'.
       outstring
                                           ; Buffer used by TCP to
                                           ; build RM record before
                                           ; sending
       methods
                                           ; Vector of operations
       credentials
                                           ; the credentials, if any,
                                           ; the caller gave us
       credentials-cache
                                           ; possible short
                                           ; credentials the server
                                           ; has given us
       timeout-handler
                                           ; what to do on timeouts
       error-handler
                                           ; what to do returned
                                           ; errors (this may not be
                                           ; the right place for these
                                           ; two)
       socket-cache
                                           ; Cached version of
                                           ; destsocket
       private
                                           ; Can be used by user as
                                           ; desired
       monitorlock
                                           ; Monitor which is locked
                                           ; during PERFORM-RPC.
       )

(defstruct (rpc-methods (:print-function (lambda (obj stream depth)
                                                (format stream 
                                                    "#<~A rpc methods>"
                                                       (
                                                   rpc-methods-protocol
                                                        obj)))))
       "Vector of operations for a kind of rpc stream." protocol 
                                           ; UDP, TCP etc
       
       ;; Input methods
       getbyte
                                           ; Function to read byte of
                                           ; incoming data
       skipbytes
                                           ; (stream #bytes) => skips
                                           ; number of bytes of
                                           ; incoming data
       getcell
                                           ; Function to get 32 bit
                                           ; two's complement integer
                                           ; of incoming data.
       getunsigned
                                           ; Function to get 32 bit
                                           ; unsigned integer from
                                           ; incoming data.
       getoffset
                                           ; Get current offset into
                                           ; incoming data (for
                                           ; string-pointer kludge)
       getrawbytes
                                           ; (stream base offset
                                           ; #bytes) -- bulk read of
                                           ; incoming data
       
       ;; Output methods
       putbyte
                                           ; Function to write byte of
                                           ; outgoing data
       zerobytes
                                           ; (stream #bytes) writes
                                           ; zeros to output.
       putcell
                                           ; Function to write 32 bit
                                           ; two's complement integer
                                           ; of outgoing data.
       putrawbytes
                                           ; (stream base offset
                                           ; #bytes) -- bulk write of
                                           ; incoming data
       
       ;; Miscellaneous methods
       close
                                           ; Function to cleanup
                                           ; stream when closed
       initialize
                                           ; (stream &optional
                                           ; destaddr destsocket) Sets
                                           ; up stream for beginning
                                           ; of call
       exchange
                                           ; (stream errorflg xid)
                                           ; performs the actual RPC
                                           ; exchange
       openp
                                           ; (stream) => true if still
                                           ; open
       )

(defstruct (rpc-program (:print-function (lambda (pgm stream depth)
                                                (format stream 
                                           "#<RPC Program ~S, #~D v~D>"
                                                       (
                                                       rpc-program-name
                                                        pgm)
                                                       (
                                                     rpc-program-number
                                                        pgm)
                                                       (
                                                    rpc-program-version
                                                        pgm)))))
       "Structure describing a Sun RPC Protocol Remote Program."
       (number 0 :type integer)
                                           ; RPC Program Number
       (version 0 :type integer)
                                           ; RPC Version Number
       name
                                           ; String or Symbol. This
                                           ; name is used only by this
                                           ; program and has no
                                           ; significance to the
                                           ; remote program. The name
                                           ; is assumed to uniquely
                                           ; specify an RPC structure.
       protocol
                                           ; A symbol. Either RPC::UDP
                                           ; or RPC::TCP.
       constants
                                           ; List of (<constant>
                                           ; <def>) pairs.
       types
                                           ; List of (<typename>
                                           ; <typedef>) pairs
       inherits
                                           ; List of names of RPC
                                           ; names whose types and
                                           ; constants are inherited
                                           ; by this RPC.
       procedures
                                           ; List of RPC-PROCEDURE
                                           ; structures defining the
                                           ; procedures for this
                                           ; remote program.
       )

(defstruct (rpc-procedure (:print-function (lambda (p stream depth)
                                                  (format stream 
                                             "#<RPC Procedure ~S, #~D>"
                                                         (
                                                     rpc-procedure-name
                                                          p)
                                                         (
                                                  rpc-procedure-procnum
                                                          p)))))
       "Strcture defining a single procedure of a SUN RPC Protcol remote program.
 " name                                    ; The procedure name. A
                                           ; string or symbol.
       (procnum 0 :type integer)
                                           ; The procedure number. An
                                           ; integer.
       argtypes
                                           ; List of argument types.
                                           ; May be typenames or
                                           ; typedefs. NIL for no
                                           ; arguments.
       resulttypes
                                           ; Same as ARGTYPES except
                                           ; for returned values.
       )

(defstruct authentication "Sun RPC Version 2 Authentication Record" 
       type                                ; 0 = NULL
                                           ; 1 = Unix
                                           ; 2 = Short
       string
                                           ; 
                                           ; Encoding of any fields of
                                           ; that type authentication.
                                           ; String is a Common Lisp
                                           ; string rather than an
                                           ; XDR-STRING.
       )

(defun rpc-error-reply-report (condition stream)
       
       ;; Condition reporter for RPC-ERROR-REPLY.  CONDITION has two
       ;; slots, a TYPE and type-specific ARGS.
       (let ((type (rpc-error-reply-type condition))
             (args (rpc-error-reply-args condition)))
            (case type
                (program-unavailable (destructuring-bind
                                      (pgm . more)
                                      args
                                      (format stream 
                              "RPC Program ~@[~A ~]Unavailable~@[ ~A~]"
                                             (and (rpc-program-p pgm)
                                                  (rpc-program-name
                                                   pgm))
                                             more)))
                (program-mismatch (format stream 
                       "RPC Program Version Mismatch: High: ~A Low: ~A"
                                         (first args)
                                         (second args)))
                (procedure-unavailable (format stream 
                                            "RPC Procedure Unavailable"
                                              ))
                (garbage-arguments (format stream 
                                          "RPC Garbage Arguments"))
                (system-error (format stream 
                                     "RPC System Error during call"))
                (rpc-version-mismatch (format stream 
                               "RPC Version Mismatch: High: ~A Low: ~A"
                                             (first args)
                                             (second args)))
                (authentication (format stream 
                                       "Authentication Error: ~A"
                                       (first args)))
                (not-a-reply (format stream 
                               "RPC reply packet not of type reply: ~D"
                                    (first args)))
                (illegal-reply-type (format stream 
                                           "Unknown RPC reply code: ~D"
                                           (first args)))
                (otherwise (format stream "RPC Error: ~A~@[ ~A~]" type
                                  args)))))

(export '(rpc-error rpc-connection-error rpc-timeout rpc-error-reply 
                rpc-no-socket xdr-error rpc-stream rpc-program 
                rpc-procedure authentication rpc-stream-monitorlock 
                rpc-stream-private))

(define-condition rpc-error (error))

(define-condition rpc-connection-error (rpc-error))

(define-condition rpc-timeout (rpc-connection-error)
       nil
       (:report "Timeout of RPC call"))

(define-condition rpc-error-reply (rpc-error)
       (type args)
       (:report rpc-error-reply-report))

(define-condition rpc-no-socket (rpc-connection-error)
       (program address)
       (:report (lambda (condition stream)
                       (let ((prg (rpc-no-socket-program condition)))
                            (format stream 
                           "Host ~A does not supply service ~A over ~A"
                                   (il:\\ip.address.to.string
                                    (rpc-no-socket-address condition))
                                   (rpc-program-name prg)
                                   (rpc-program-protocol prg))))))

(define-condition xdr-error (rpc-connection-error)
       (format-string format-args)
       (:report (lambda (c stream)
                       (apply #'format stream (xdr-error-format-string
                                               c)
                              (xdr-error-format-args c)))))

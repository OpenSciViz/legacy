;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCSRV")

(in-package "RPC-SERVER")

(eval-when (compile load eval)
       (il:importfile "TCPLLIP")
                                           ; Needed for the IP
                                           ; accessfns
       (il:importfile "TCPUDP")
                                           ; Needed for the UDP
                                           ; accessfns
)


;; Can't parse: (il:structures)



;; Can't parse: (il:variables)



;; Can't parse: (il:functions)


(defstruct message "A packet plus a pointer to the current position" 
       packet position)

(defstruct (rpcserver)
       "Describes an RPC server" name number version protocol port)


;;; Miscellaneous functions


(defun format-host-addr (host)
       (format nil "[~d.~d.~d.~d]" (logand (ash host -24)
                                          255)
              (logand (ash host -16)
                     255)
              (logand (ash host -8)
                     255)
              (logand host 255)))


;;; RPC server (really portmapper)



;; The following are constants defined by the RPC spec


(defconstant *rpc.call* 0 "msg_type = CALL")

(defconstant *rpc.reply* 1 "msg_type = REPLY")

(defconstant *rpc.version* 2 "Version of RPC")

(defconstant *rpc.msg-accepted* 0 "reply_stat MSG_ACCEPTED")

(defconstant *rpc.msg-denied* 1 "reply_stat MSG_DENIED")

(defconstant *rpc.success* 0 "accept_status SUCCESS")

(defconstant *rpc.prog-unavail* 1 "No such program")

(defconstant *rpc.prog-mismatch* 2 "This version not implemented")

(defconstant *rpc.proc-unavail* 3 "This procedure not implemented")

(defconstant *rpc.garbage-args* 4 "Bad arguments to proc")


;; The following are constants defined by the port mapper spec


(defconstant *portmap.proc-null* 0 "No op")

(defconstant *portmap.proc-set* 1 "Register service")

(defconstant *portmap.proc-unset* 2 "Unregister service")

(defconstant *portmap.proc-getport* 3 "Get port number of service")

(defconstant *portmap.proc-dump* 4 "List all services")

(defconstant *portmap.proc-callit* 5)

(defvar *rpc-server-outpacket* (il:\\allocate.etherpacket))

(defvar *rpc.servers*
       (list (make-rpcserver :name :portmapper :number 100000 :version
                    2 :protocol :udp :port 111)))

(defun register-rpc-service (name number version protocol port)
       (push (make-rpcserver :name name :number number :version version
                    :protocol protocol :port port)
             *rpc.servers*))

(defun unregister-rpc-service (name number version protocol)
       (let ((server (find-server-program number version protocol)))
            (when server
                (setq *rpc.servers* (delete server *rpc.servers*)))))

(defun rpc-server-main nil (let (rpc-socket)
                                (unwind-protect
                                    (rpc-server-loop
                                     (setq rpc-socket (
                                                     il:udp.open.socket
                                                       111)))
                                    (il:udp.close.socket rpc-socket t))
                                ))

(defun rpc-server-loop (rpc-socket)
       
       ;; This is really just a portmap server, even though it listens
       ;; on the sunrpc UDP port (111).  Requests for all services
       ;; other than portmap are sent to other UDP ports.  The client
       ;; gets the port number by sending a request to portmap.  This
       ;; is where that request is handled.
       (let ((message (make-message))
             xid prog vers proc portmapserver)
            (loop (setf (message-position message)
                        0
                        (message-packet message)
                        (il:udp.get rpc-socket t))
                  (multiple-value-setq (xid prog vers proc)
                         (get-rpc-call-body message))
                  (when (find-server-program prog vers :udp)
                        (portmap-server rpc-socket message proc xid))))
       )

(defun get-rpc-call-body (message)
       (let (xid msg-type rpcvers prog vers proc cred verf)
            (and (setq xid (udp.get.next.long message))
                 (setq msg-type (udp.get.next.long message))
                 (eq msg-type *rpc.call*)
                 (setq rpcvers (udp.get.next.long message))
                 (eql rpcvers *rpc.version*)
                 (integerp (setq prog (udp.get.next.long message)))
                 (integerp (setq vers (udp.get.next.long message)))
                 (integerp (setq proc (udp.get.next.long message)))
                 (setq cred (read-auth message))
                 (setq verf (read-auth message)))
            (values xid prog vers proc cred vers)))

(defun find-server-program (prog-number version protocol)
       (find-if #'(lambda (x)
                         (and (= prog-number (rpcserver-number x))
                              (or (= version 0)
                                  (= version (rpcserver-version x)))
                              (eq protocol (rpcserver-protocol x))))
              *rpc.servers*))

(defun
 portmap-server
 (rpc-socket message proc xid)
 (let (prog-number vers protocol-number server (outpacket 
                                                 *rpc-server-outpacket*
                                                      ))
      (rpc-setup outpacket (message-packet message)
             xid)
      (udp.append.long outpacket *rpc.msg-accepted*)
      (append-null-auth outpacket)
      (case proc
          (0 (udp.append.long outpacket *rpc.success*))
          (3 (udp.append.long outpacket *rpc.success*)
             (udp.append.long outpacket
                    (if (and (integerp (setq prog-number (
                                                      udp.get.next.long
                                                          message)))
                             (integerp (setq vers (udp.get.next.long
                                                   message)))
                             (integerp (setq protocol-number
                                             (udp.get.next.long message
                                                    )))
                             (setq server (find-server-program
                                           prog-number vers
                                           (protocol-name 
                                                  protocol-number))))
                        (rpcserver-port server)
                        0)))
          (otherwise (udp.append.long outpacket *rpc.proc-unavail*)))
      (il:udp.send rpc-socket outpacket)))

(defun protocol-name (number)
       (case number
           (17 :udp)
           (6 :tcp)))

(defun rpc-setup (outpacket inpacket xid)
       (let (host)
            (il:udp.setup outpacket (setq host (il:|fetch| (il:ip
                                                            
                                                     il:ipsourceaddress
                                                            )
                                                      il:|of| inpacket)
                                          )
                   (il:|fetch| (il:udp il:udpsourceport)
                          il:|of| inpacket)
                   0 *rpc-server-socket*)
            (udp.append.long outpacket xid)
            (udp.append.long outpacket *rpc.reply*)
            host))

(defun append-null-auth (outpacket)
       (udp.append.long outpacket 0)
       (udp.append.long outpacket 0))

(defun read-auth (message)
       (let (auth-flavor length)
            (setq auth-flavor (udp.get.next.long message)
                  length
                  (udp.get.next.long message))
            
            ;; For now we don't care about authentication, so just
            ;; skip over it and return T to indicate that it was
            ;; present.  Later we may want to validate it.
            (incf (message-position message)
                  length)
            t))

(defun udp.append.long (packet long)
       (il:udp.append.word packet (ash long -16))
       (il:udp.append.word packet (logand 65535 long)))

(defun udp.append.rpc.string (outpacket string)
       (let ((length (length string))
             rem)
            (udp.append.long outpacket length)
            (il:udp.append.string outpacket string)
                                           ; Have to bad with nulls
            (when (> (setq rem (mod length 4))
                     0)
                (il:udp.append.string outpacket (subseq "    " rem)))))

(defun udp.get.next.long (message)
       (let ((word-offset (ash (message-position message)
                               1))
             high low)
            (and (setq high (udp.get.next.word message))
                 (setq low (udp.get.next.word message))
                 (logior (ash high 16)
                        low))))

(defun udp.get.next.word (message)
       (prog1 (il:udp.get.word (message-packet message)
                     (ash (message-position message)
                          -1))
           (incf (message-position message)
                 2)))

(defun udp.get.rpc.string (message)
       
       ;; Reads the 4-byte length and then the string.
       (let (string (length (udp.get.next.long message)))
            (when length
                (setq string (il:allocstring length))
                (il:\\movebytes (il:|fetch| (il:udp il:udpcontents)
                                       il:|of|
                                       (message-packet message))
                       (message-position message)
                       (il:|fetch| (il:stringp il:base)
                              il:|of| string)
                       (il:|fetch| (il:stringp il:offst)
                              il:|of| string)
                       length)
                (incf (message-position message)
                      (* 4 (ceiling length 4)))
                string)))


;;; FIle handle (common to mount and NFS)


(defvar *assigned-fhandles* nil)

(defun find-fhandle (host dir name create?)
       (or (find-if #'(lambda (fh)
                             (and (string= host (fhandle-host fh))
                                  (string= dir (fhandle-directory
                                                fh))
                                  (equal name (fhandle-name fh))))
                  *assigned-fhandles*)
           (and create? (push (make-fhandle :id (incf *fhandle.counter*
                                                      )
                                     :host host :directory dir :name 
                                     name)
                              *assigned-fhandles*))))


;;; Mount server


(defstruct mountentry host path fhandle)

(defstruct fhandle "NFS file handle" id host dir name)

(defvar *mount-server-outpacket* (il:\\allocate.etherpacket))

(defvar *mount.entries* nil)

(defun mount-server-main nil
       (let (mount-socket)
            (unwind-protect
                (progn (setq mount-socket (il:udp.open.socket))
                       (register-rpc-service :mount 100005 1 :udp
                              (il:udp.socket.number mount-socket))
                       (mount-server-loop mount-socket))
                
                ;; Cleanup forms:
                (unregister-rpc-service :mount 100005 1 :udp)
                (il:udp.close.socket mount-socket t))))

(defun mount-server-loop (mount-socket)
       (let ((message (make-message))
             (outpacket *mount-server-outpacket*)
             host xid prog vers proc)
            (loop (setf (message-position message)
                        0
                        (message-packet message)
                        (il:udp.get mount-socket t))
                  (multiple-value-setq (xid prog vers proc)
                         (get-rpc-call-body message))
                  (setq host (rpc-setup outpacket (message-packet
                                                   message)
                                    xid))
                  (udp.append.long outpacket *rpc.msg-accepted*)
                  (append-null-auth outpacket)
                  (case proc
                      (0 (udp.append.long outpacket *rpc.success*))
                      (1 (mount-add-entry message outpacket host))
                      (otherwise (udp.append.long outpacket 
                                        *rpc.proc-unavail*)))
                  (il:udp.send mount-socket outpacket))))

(defun mount-add-entry (message outpacket host)
       (let (fhandle host/dir (path (udp.get.rpc.string message)))
            (format t "req to mount ~s from host ~s~%" path
                   (format-host-addr host))
            (udp.append.long outpacket *rpc.success*)
            (cond ((setq host/dir (il:directoryname path))
                   (udp.append.long outpacket *nfs.ok*)
                   (setq fhandle (find-fhandle (pathname-host host/dir)
                                        (pathname-directory host/dir)
                                        nil t))
                   (push fhandle *mount.entries*)
                   (udp.append.fhandle outpacket fhandle))
                  (t (udp.append.long outpacket *nfs.notdir*)))))


;;; NFS server



;; NFS file tpes


(defconstant *nfnon* 0 "Non-file")

(defconstant *nfreg* 1 "Regular file")

(defconstant *nfdir* 2 "Directory")


;; NFS error codes


(defconstant *nfs.ok* 0 "No error")

(defconstant *nfserr.perm* 1 "Not owner")

(defconstant *nfserr.noent* 2 "No such file or directory")

(defconstant *nfserr.io* 5 "I/O error")

(defconstant *nfserr.nxio* 6 "No such device or address")

(defconstant *nfserr.access* 13 "Permission denied")

(defconstant *nfserr.exist* 17 "File exists")

(defconstant *nfserr.nodev* 19 "No such device")

(defconstant *nfserr.notdir* 20 "Not a directory")

(defconstant *nfserr.isdir* 21 "Is a directory")

(defconstant *nfserr.fbig* 27 "File too large")

(defconstant *nfserr.nospc* 28 "No space left on device")

(defconstant *nfserr.rofs* 30 "Read-only file system")

(defconstant *nfserr.nametoolong* 63 "File name too long")

(defconstant *nfserr.notempty* 66 "Directory not empty")

(defconstant *nfserr.dquot* 69 "Disc quota exceeded")

(defconstant *nfserr.stale* 70 "Stale NFS file handle")

(defvar *nfs-server-outpacket* (il:\\allocate.etherpacket))

(defun nfs-server-main nil
       (let (nfs-socket)
            (unwind-protect
                (progn (setq nfs-socket (il:udp.open.socket
                                         2049
                                         'il:accept))
                                           ; Apparently NFS has to be
                                           ; on port 2049 even though
                                           ; the port mapper knows
                                           ; about it
                       (register-rpc-service :nfs 100003 2 :udp
                              (il:udp.socket.number nfs-socket))
                       (nfs-server-loop nfs-socket))
                
                ;; Cleanup forms:
                (unregister-rpc-service :nfs 100003 2 :udp)
                (il:udp.close.socket nfs-socket t))))

(defun nfs-server-loop (nfs-socket)
       (let ((message (make-message))
             (outpacket *nfs-server-outpacket*)
             host xid prog vers proc)
            (loop (setf (message-position message)
                        0
                        (message-packet message)
                        (il:udp.get nfs-socket t))
                  (multiple-value-setq (xid prog vers proc)
                         (get-rpc-call-body message))
                  (setq host (rpc-setup outpacket (message-packet
                                                   message)
                                    xid))
                  (udp.append.long outpacket *rpc.msg-accepted*)
                  (append-null-auth outpacket)
                  (case proc
                      (0 (udp.append.long outpacket *rpc.success*))
                      (1 (nfs-get-file-attributes message outpacket 
                                host))
                      (x2 (nfs-set-file-attributes message outpacket 
                                 host))
                      (4 (nfs-lookup-file message outpacket host))
                      (x17 (nfs-get-file-system-attributes message 
                                  outpacket host))
                      (otherwise (udp.append.long outpacket 
                                        *rpc.proc-unavail*)))
                  (il:udp.send nfs-socket outpacket))))

(defun nfs-get-file-attributes (message outpacket host)
       (udp.append.long outpacket *rpc-success*)
       (let (fhandle-id fhandle)
            (setq fhandle-id (udp.get.fhandle message))
            (format t "get-attr ~s from host ~s~%" fhandle (
                                                       format-host-addr
                                                            host))
            (cond ((setq fhandle (find-fhandle-by-id fhandle-id))
                   (udp.append.long outpacket *nfs.ok*)
                   
                   ;; Something to start with until I find out Sun NFS
                   ;; really needs:
                   (dolist (x '(,*nfdir* 16895 1 0 0 1 512 -1 1 -1 -1 0
                                      0 0 0 0 0))
                       (udp.append.long outpacket x)))
                  (t (udp.append.long outpacket *nfserr.stale*)))))

(defun nfs-set-file-attributes nil)

(defun nfs-lookup-file (message outpacket host)
       (udp.append.long outpacket *rpc-success*)
       (let (fhandle-id fhandle new-fhandle filename path truename)
            (setq fhandle-id (udp.get.fhandle message)
                  filename
                  (udp.get.rpc.string message))
            (format t "lookup ~s from host ~s~%" filename (
                                                       format-host-addr
                                                           host))
            (cond ((null (setq fhandle (find-fhandle-by-id fhandle-id))
                         )
                   (udp.append.long outpacket *nfserr.stale*))
                  ((il:directorynamep (setq path (concatenate
                                                  'string
                                                  (fhandle-host fhandle
                                                         )
                                                  (fhandle-dir fhandle)
                                                  filename ">")))
                   (udp.append.long outpacket *nfs.ok*)
                   (setq new-fhandle (find-fhandle (fhandle-host 
                                                          fhandle)
                                            (concatenate 'string
                                                   (fhandle-dir fhandle
                                                          )
                                                   filename)
                                            nil t))
                   (udp.append.fhandle outpacket fhandle))
                  ((setq truename (probe-file (concatenate 'string
                                                     (fhandle-host
                                                      fhandle)
                                                     (fhandle-dir
                                                      fhandle)
                                                     filename)))
                   (udp.append.long outpacket *nfs.ok*)
                   (setq new-fhandle (find-fhandle (fhandle-host 
                                                          fhandle)
                                            (concatenate 'string
                                                   (fhandle-dir fhandle
                                                          )
                                                   filename)
                                            nil t))
                   (udp.append.fhandle outpacket fhandle))
                  (t (udp.append.long outpacket *nfserr.noent*))
                  

;;; NOT DONE YET!

                  (udp.append.long outpacket *nfs.ok*)
                  (t))))

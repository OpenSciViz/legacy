;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCXDR")

(in-package "RPC2")

(eval-when (eval compile)
  (require "RPCDECLS"))

                                           ; Useful Constants


(defconstant twoto31minusone 2147483647)

(defconstant twoto31st 2147483648)

(defconstant twoto32nd 4294967296)

(defconstant twoto32minusone 4294967295)

(defconstant twoto63minusone 9223372036854775807)

(defconstant twoto64minusone 18446744073709551615)

(defconstant twoto64th 18446744073709551616)

(defconstant minus2to31 -2147483648)

(defconstant minus2to63 -9223372036854775808)

(defparameter *xdr-primitive-types*
  '((:integer xdr-read-integer . xdr-write-integer)
    (:boolean xdr-read-boolean . xdr-write-boolean)
    (:unsigned xdr-read-unsigned . xdr-write-unsigned)
    (:hyperinteger xdr-read-hyperinteger . xdr-write-hyperinteger)
    (:hyperunsigned xdr-read-hyperunsigned . xdr-write-hyperunsigned)
    (:string xdr-read-string . xdr-write-string)
    (:void . xdr-read-write-void)
    (:float xdr-read-float . xdr-write-float)
    (:double . xdr-double)
    (:string-pointer xdr-read-string-pointer . xdr-write-string-pointer))
  "An alist of XDR primitive types and the function(s) that encodes/decodes that
type.  If CDR is a list, it is (readfn . writefn).")

(defparameter *xdr-constructed-types*
  '((:enumeration . xdr-codegen-enumeration)
    (:union . xdr-codegen-union)
    (:struct . xdr-codegen-struct)
    (:list . xdr-codegen-list)
    (:fixed-array . xdr-codegen-fixed-array)
    (:counted-array . xdr-codegen-counted-array)
    (:opaque . xdr-codegen-opaque)
    (:skip . xdr-codegen-skip)
    (:sequence . xdr-codegen-sequence)
    (:list-of . xdr-codegen-list-of))
  "Association list of XDR constructed types and the functions that create
functions to read/write them")

(defglobalvar *xdr-codegen-recursivelst* ()
  "Place for XDR-CODEGEN to save recursive functions it found in making an expansion.
A list of TYPSTK structs")

(defstruct typstk 
  "Element on stack of types for which code already generated." 
  prog type xdrproc oper args)

                                           ; Miscellaneous XDR Utility
                                           ; Functions


(defun access-fcn-name (struct field)
       "
Maps struct name and field name (strings or symbols) into the
access function name for that slot."
                                           ; 
       (intern (concatenate 'string (string struct)
                      "-"
                      (string field))
              (symbol-package struct)))

(defun constructor-fcn-name (struct)
       "
Maps a symbol or string naming a defstruct into the constructor function symbol
for that defstruct type"
       (intern (concatenate 'string "MAKE-" (string struct))
              (symbol-package struct)))

(defun find-in-type-stack (prg typ stack)
       "
Find the first element in a list of TYPSTK's such that PRG and TYP
match the PROG and TYPE fields of the TYPSTK.
"
       (dolist (el stack)
           (if (and (eql prg (typstk-prog el))
                    (eql typ (typstk-type el)))
               (return el))))

                                           ; Type Declarations and
                                           ; Predicates


(deftype xdr-integer nil '(and integer (satisfies xdr-integer-p)))

(deftype xdr-unsigned nil '(and integer (satisfies xdr-unsigned-p)))

(deftype xdr-hyperinteger nil '(and integer (satisfies 
                                                   xdr-hyperinteger-p))
       )

(deftype xdr-hyperunsigned nil '(and integer (satisfies 
                                                    xdr-hyperunsigned-p
                                                    )))

(defun xdr-integer-p (i)
       (and (>= i minus2to31)
            (< i twoto31st)))

(defun xdr-unsigned-p (i)
       (or (and (typep i 'fixnum)
                (>= (the fixnum i)
                    0))
           (and (>= i 0)
                (< i twoto32nd))))

(defun xdr-hyperinteger-p (i)
       (and (>= i minus2to63)
            (<= i twoto63minusone)))

(defun xdr-hyperunsigned-p (i)
       (and (>= i 0)
            (<= i twoto64minusone)))

                                           ; XDR Code Generation for
                                           ; Constructed Functions


(defun xdr-codegen-comment nil "
***************************************************
**** Code Generation for XCL Constructed Types ****
***************************************************
The following functions generate code for translating between Common Lisp
and XDR. For each function,
    CONTEXT is an RPC-PROGRAM structure with respect to which a
            typedef is being constructed.
    TYPEDEF is an XDR type definition, and
    OPER is either READ (decode) or WRITE (encode).
For all functions except XDR-CODEGEN, a third argument ARGS is a list of
arguments to the code being generated. It always begins with an XDR-stream argument
and for OPER=WRITE is usually followed by the object to be written.

WARNINGS:

(1) DO NOT, REPEAT DO NOT pass an (XDR-CODEGEN-xxx) as the argument of an
(XDR-CODEGEN-xxx). If you do, you might cause the  code generated for
the argument to be evaluated multiple times in the code for the resulting 
expression.

(2) The XDR-CODEGEN-xxx functions code in-line rather than wrap themselves
in LET's or LAMBDA's or whatever. To avoid complications with functions
that require a location-specifier (CHECK-TYPE or CCASE, for example), an
XDR-CODEGEN-xxx function may ***not*** generate code that assumes that its
arguments ARGS or various COUNTs are legitimate location-specifiers. If a 
CHECK-TYPE or similar function is to be done, a LET (or other binding
mechanism) should be generated to create a legal location-specifier." 
       nil)

(defun xdr-codegen (context typedef oper)
       "
Top-level XDR Code Generation function. Returns code to read/write
an XDR element of type TYPEDEF.

CONTEXT is an RPC-PROGRAM structure with respect to which the
TYPEDEF is interpreted (in terms of inheritance).

TYPEDEF is an XDR Type or Type definition.

OPER is either 'RPC2::READ or 'RPC::WRITE.

See documentation of XDR-CODEGEN-COMMENT.
"
       (setq *xdr-codegen-recursivelst* nil)
       (let* ((args (ecase oper
                        (read '(xdr-stream))
                        (write '(xdr-stream xdr-toplevel-item))))
              (fcn (xdr-codegen-1 context typedef oper args nil)))
             (if fcn
                 (if (null *xdr-codegen-recursivelst*)
                     (list 'lambda args fcn)
                     (list 'lambda args
                           `(labels ,(xdr-codegen-3 
                                            *xdr-codegen-recursivelst*)
                                   ,fcn)))
                 (error "Could not parse XDR Type ~S" typedef))))

(defun
 xdr-codegen-1
 (context typedef oper args stk)
 "Generates code to read or write an element of type TYPEDEF.

CONTEXT, TYPEDEF, and OPER are as in XDR-CODEGEN.

ARGS is a list of the arguments forms for the generated code.
For OPER=READ it will (<rpc-stream-name>), and
For OPER=WRITE it will be (<rpc-stream-name> <element>).

STK is a list of TYPSTK elements, one for each named type above this one in this expansion."
 (or
  (cond
   ((symbolp typedef)
                                           ; Primitive, local or
                                           ; inherited type
    (let
     ((tem (cdr (assoc typedef *xdr-primitive-types*))))
     (cond
      (tem                                 ; Here's how to read/write
                                           ; it.
           (cons (cond ((not (consp tem))
                                           ; Old way is single fn
                        tem)
                       ((eq oper 'read)
                                           ; New way is (read . write)
                        (car tem))
                       (t (cdr tem)))
                 args))
      ((setq tem (find-rpc-typename context typedef))
                                           ; It's defined in this
                                           ; program
       (xdr-codegen-2 context tem oper args stk))
      (t                                   ; Try to inherit it
         (some #'(lambda (progname)
                        (let ((prg (find-rpc-program :name progname))
                              td)
                             (and prg (setq td (find-rpc-typename
                                                prg typedef))
                                  (xdr-codegen-2 prg td oper args stk))
                             ))
               (rpc-program-inherits context))))))
   ((consp typedef)
                                           ; Constructed or qualified
                                           ; type
    (let ((fn (cdr (assoc (car typedef)
                          *xdr-constructed-types*)))
          prg td)
         (cond (fn                         ; Here's a function to
                                           ; generate code
                   (funcall fn context typedef oper args stk))
               ((and (symbolp (car typedef))
                     (symbolp (cdr typedef))
                     (setq prg (find-rpc-program :name (car typedef)))
                     (setq td (find-rpc-typedef prg (cdr typedef))))
                                           ; Qualified def (prgname .
                                           ; type)
                (xdr-codegen-2 prg td oper args stk))))))
  (error "Could not resolve XDR Type Definition: ~S" typedef)))

(defun xdr-codegen-2 (context typename oper args stk)
       "
Expands named types. 

(1) Sees whether type already seen above here in this expansion.
Otherwise,
(2) Notes the name on TYPESTK,
(3) Finds the definition of this type,
(4) Calls XDR-CODEGEN-1 to expand the type definition.
(5) Sees whether the XDR-CODEGEN-1 call found this type below,
     if so, notes this on *XDR-CODEGEN-RECURSIVELST* and returns
     call to the recursive function for this type.
     otherwise just returns the code.
"
       
       ;; Every named type expansion passes through here and gets
       ;; expanded. Since it is only named types that can be
       ;; recursive,  this is the only place we check for recursion
       (or (xdr-codegen-recursion context typename oper args stk)
           (let (td code top)
                                           ; No
                (push (make-typstk :prog context :type typename :oper 
                             oper :args (if (eql oper 'read)
                                            args
                                            '(rpcstream rvalue)))
                      stk)
                                           ; Push type on stack
                (unless (setq td (find-rpc-typedef context typename))
                    (error 
                         "Null type definition for Program ~A, Type ~A"
                           (and context (rpc-program-name context))
                           typename))
                (setq code (xdr-codegen-1 context td oper args stk))
                                           ; Generate code
                (setq top (car stk))
                                           ; "Pop" stack
                (if (null (typstk-xdrproc top))
                                           ; Was this type called
                                           ; recursively?
                    code
                                           ; No, just return code
                    (progn (push top *xdr-codegen-recursivelst*)
                                           ; Yes, save recursive type
                           `(,(typstk-xdrproc top)
                             ,@args)
                                           ; Return call to recursive
                                           ; function
                           )))))

(defun
 xdr-codegen-3
 (rlist)
 
 ;; Generate the set of function definitions for LABELS. RLIST is a
 ;; list of TYPSTK structs.
 (mapcar
  #'(lambda (typstk)
           `(,(typstk-xdrproc typstk)
             ,(typstk-args typstk)
             ,(xdr-codegen-1 (typstk-prog typstk)
                     (or (find-rpc-typedef (typstk-prog typstk)
                                (typstk-type typstk))
                         (error "No typedef for Program ~A, Type ~A"
                                (rpc-program-name (typstk-prog typstk))
                                (typstk-type typstk)))
                     (typstk-oper typstk)
                     (typstk-args typstk)
                     rlist)))
  rlist))

(defun
 xdr-codegen-recursion
 (prg typ oper args stack)
 
 ;;  If type has already be seen, mark as recursive and return code
 ;; calling that function
 (let ((instack (find-in-type-stack prg typ stack)))
      (when instack
                                           ; Seen it before
          (setf (typstk-xdrproc instack)
                (or (typstk-xdrproc instack)
                    (intern (symbol-name (gensym (concatenate
                                                  'string "XDR-"
                                                  (symbol-name oper)
                                                  "-"
                                                  (symbol-name typ)
                                                  "-"))))))
          `(,(typstk-xdrproc instack)
            ,@args))))

(defun xdr-codegen-constant (context constant)
       (cond ((null constant)
              (error 
      "Could not resolve nil constant definition from RPC program ~a~%"
                     (rpc-program-name context)))
             ((integerp constant)
              constant)
                                           ;  Immediate Constant
                                           ; Definition
             ((and (symbolp constant)
                   (or (find-xdr-constant context constant)
                                           ;  Local Constant Definition
                       (some #'(lambda (cntx)
                                      (find-xdr-constant (
                                                       find-rpc-program
                                                          :name cntx)
                                             constant))
                             (rpc-program-inherits context))
                                           ;  Inherited Constant
                                           ; Definition
                       )))
             ((and (consp constant)
                                           ; Qualified Constant
                                           ; Definition 
                   (symbolp (cdr constant))
                   (find-xdr-constant (find-rpc-program :name
                                             (car constant))
                          (cdr constant))))
             ((error "Could not resolve XDR constant ~a~%" constant))))

(defun
 xdr-codegen-enumeration
 (context typedef oper args stk)
 (let
  ((cases (cdr typedef))
   (selector (if (eq oper 'read)
                 'key
                 (cadr args)))
   default parent)
  (cond
   ((eq (first cases)
        :noerrors)
                                           ; Means allow values other
                                           ; than shown here, which we
                                           ; will just represent by
                                           ; the integer value
    (setq cases (cdr cases))
    (setq default selector))
   (t
                                           ; Generate error clause if
                                           ; selector invalid. 
                                           ; Inlcude the type that
                                           ; produced this enumeration
                                           ; if it's simple
    (setq default
          `(xdr-enumeration-fault
            ,selector
            ',oper
            ,@(and (consp stk)
                   (symbolp (setq parent (typstk-type (first stk))))
                   `(',parent))))))
  (if (eq oper 'read)
      `(let
        ((key (xdr-read-integer ,(car args))))
        (case key
            ,.(mapcar #'(lambda (x)
                               `(,(xdr-codegen-constant context
                                         (cadr x))
                                 ',(car x)))
                     cases)
            (otherwise ,default)))
      `(xdr-write-integer
        ,(car args)
        (case ,selector
            ,.(mapcar #'(lambda (x)
                               (list (car x)
                                     (xdr-codegen-constant context
                                            (cadr x))))
                     cases)
            (otherwise ,default))))))

(defun
 xdr-codegen-union
 (context typedef oper args stk)
 "
(UNION <discriminant-type> (<enumeration-element> <arm-type>) ...(<> <>))
Read Calling Sequence: XDR-UNION(xdrstream)
Read Input: An integer followed by the encoding of that arm.
Read Output: The enumeration element from the type of the discriminant
             The discriminant and arm are returned as a dotted pair.
Write Input: An enumeration element and an unencoded arm.
Write calling sequence: XDR-UNION(xdrstream,discriminant,arm)
Write Output: The (integer) encoding of the discriminant and the encoded arm.
"
 (let ((discrim-type (second typedef))
       (xdrstream (first args))
       (unionlist (second args)))
      (if (eq oper 'read)
          `(let ((discriminant ,(xdr-codegen-1 context discrim-type 
                                       oper args stk)))
                (list discriminant
                      (case discriminant
                          ,.(xdr-codegen-union-cases context typedef 
                                   oper args stk))))
          `(progn ,(xdr-codegen-1 context discrim-type oper
                          `(,xdrstream (car ,unionlist))
                          stk)
                  (case (car ,unionlist)
                      ,.(xdr-codegen-union-cases context typedef oper 
                               args stk))))))

(defun
 xdr-codegen-union-cases
 (context typedef oper args stk)
 
 ;; Generate the clauses for a CASE statement which resolves the cases
 ;; of TYPEDEF = (:UNION enum . cases).
 (do ((pairs (cddr typedef)
             (cdr pairs))
      (recurargs (if (eq oper 'read)
                     args
                                           ; For WRITE, ARGS = (stream
                                           ; item).  Recursive calls
                                           ; will write (stream (cadr
                                           ; item)).
                     `(,(first args)
                       (cadr ,(second args)))))
      (arms)
      (pair))
     ((null pairs)
      (nreverse arms))
   (setq pair (car pairs))
   (push `(,(if (or (eq (car pair)
                        'otherwise)
                    (and (symbolp (car pair))
                         (string= (car pair)
                                'default)))
                'otherwise
                                           ; Default arm turns into
                                           ; OTHERWISE case.
                `(,(car pair)))
           ,(xdr-codegen-1 context (cadr pair)
                   oper recurargs stk))
         arms)))

(defun
 xdr-codegen-list
 (context typedef oper args stk)
 "TYPEDEF = (LIST <typedef-1> ... <typedef-n>)"
 (if (eq oper 'read)
     `(list ,.(mapcar #'(lambda (td)
                               (xdr-codegen-1 context td oper args stk)
                               )
                     (cdr typedef)))
     (let
      ((xdrstream (first args))
       (thelist (second args))
       (firsttime t))
      
      ;; Walk down the list we're printing, generating code to write
      ;; the CAR by the appropriate type def.
      `(let
        ((thelist ,(second args)))
        ,@(mapcar
           #'(lambda
              (type)
              (xdr-codegen-1
               context type oper
               `(,xdrstream
                 (car ,(cond (firsttime (setq firsttime nil)
                                    'thelist)
                             (t            ; Get next tail
                                '(setq thelist (cdr thelist))))))
               stk))
           (cdr typedef))))))

(defun
 xdr-codegen-struct
 (context typedef oper args stk)
 "(STRUCT <defstruct-type> (<field-name> <type>) ... (<field-name> <type>))"
 (let
  ((struct-type (cadr typedef))
   (xdrstream (first args))
   (thestruct (second args)))
  (if (eq oper 'read)
      `(,(constructor-fcn-name struct-type)
                                           ; Call the constructor with
                                           ; list of key val ...
        ,@(mapcan #'(lambda (x)
                                           ; X = (slotname type)
                           (list (intern (symbol-name (car x))
                                        (find-package "KEYWORD"))
                                 (xdr-codegen-1 context (cadr x)
                                        oper args stk)))
                 (cddr typedef)))
      `(progn
        ,@(mapcar
           #'(lambda (x)
                    (xdr-codegen-1
                     context
                     (cadr x)
                     oper
                     `(,xdrstream (,(access-fcn-name struct-type
                                           (car x))
                                   ,thestruct))
                     stk))
           (cddr typedef))))))

(defun
 xdr-codegen-fixed-array
 (context typedef oper args stk &optional run-time-count)
 "typedef is (:fixed-array elttype count), or (:counted-array elttype) with RUN-TIME-COUNT giving a run-time expression."
 
 ;; Read or write a sequence of exactly COUNT objects of type ELTTYPE
 (let* ((element-type (second typedef))
        (count (cond (run-time-count)
                     (t                    ; For FIXED-ARRAY, the
                                           ; count is a constant known
                                           ; at compile time and not
                                           ; encoded in the stream
                        (xdr-codegen-constant context (third typedef)))
                     ))
        (xdrstream (first args)))
       (unless run-time-count
           (check-type count (integer 0 *)))
       (if (eq oper 'read)
           `(let* ((thecount ,count)
                   (thearray (make-array thecount)))
                  (dotimes (i thecount thearray)
                      (setf (aref thearray i)
                            ,(xdr-codegen-1 context element-type oper 
                                    args stk))))
           `(let ((thearray ,(second args)))
                 (dotimes (i ,count thearray)
                     ,(xdr-codegen-1 context element-type oper
                             `(,xdrstream (aref thearray i))
                             stk))))))

(defun xdr-codegen-counted-array (context typedef oper args stk)
       "typedef is (:counted-array element-type)"
       
       ;; Counted arrays are just like fixed arrays, except that they
       ;; are preceded by their length (unsigned).
       (if (eq oper 'read)
           (xdr-codegen-fixed-array context typedef oper args stk
                  (xdr-codegen-1 context :unsigned oper args stk))
           (let ((xdrstream (first args)))
                `(let* ((thearray ,(second args))
                        (thecount (length thearray)))
                       ,(xdr-codegen-1 context :unsigned oper
                               `(,xdrstream thecount)
                               stk)
                       ,(xdr-codegen-fixed-array context typedef oper
                               `(,xdrstream thearray)
                               stk
                               'thecount)))))

(defun
 xdr-codegen-list-of
 (context typedef oper args stk)
 "typedef is (:list-of element-type)"
 
 ;; Read or write a list of elements all of the same type.  Stream
 ;; encoding is the length of the list followed by the elements.
 (let
  ((element-type (second typedef))
   (xdrstream (first args)))
  (if (eq oper 'read)
      `(il:to ,(xdr-codegen-1 context :unsigned oper args stk)
              il:collect
              ,(xdr-codegen-1 context element-type oper args stk))
      `(let* ((thelist ,(second args))
              (thecount (list-length thelist)))
             ,(xdr-codegen-1 context :unsigned oper
                     `(,xdrstream thecount)
                     stk)
             (mapc #'(lambda (obj)
                            ,(xdr-codegen-1 context element-type oper
                                    `(,xdrstream obj)
                                    stk))
                   thelist)))))

(defun xdr-codegen-opaque (context typedef oper args stk)
       "Declaration is (opaque <bytecount> <array-type>)"
       (let ((bytecount (xdr-codegen-constant context (second typedef))
                    )
             (element-type (third typedef))
             (xdrstream (first args)))
            (check-type bytecount (integer 0 *)
                   "Opaque size must be integral")
                                           ; Might want to check
                                           ; ELEMENT-TYPE here, too
            (if (eq oper 'read)
                `(xdr-read-array ,xdrstream ,bytecount
                        ',element-type)
                `(xdr-write-array ,xdrstream ,bytecount
                        ',element-type
                        ,(second args)))))

(defun xdr-codegen-skip (context typedef oper args stk)
       (let ((bytecount (xdr-codegen-constant context (second typedef))
                    ))
            (check-type bytecount (integer 0 *))
            `(,(if (eql oper 'read)
                   'xdr-skip-primitive
                   'xdr-zero-primitive)
              ,(first args)
              ,(logand (+ bytecount 3)
                      -4))))

(defun xdr-codegen-sequence (context typedef oper args stk)
       
       ;; Non-recursive way of handling a recursive type.  Each item
       ;; is preceded on the stream with a boolean T, than a boolean
       ;; NIL follows the last arg.  In Lisp, we represent this as a
       ;; simple list.
       (let ((stream (first args))
             (elttype (second typedef)))
            (if (eq oper 'read)
                `(il:while (xdr-read-boolean ,stream)
                        il:collect
                        ,(xdr-codegen-1 context elttype oper args stk))
                `(dolist (el ,(second args)
                             (xdr-boolean ,stream nil))
                     (xdr-write-boolean ,stream t)
                     ,(xdr-codegen-1 context elttype oper
                             `(,stream el)
                             stk)))))

                                           ; XDR run-time primitives


(defun xdr-read-integer (xdrstream)
       (getcell xdrstream))

(defun xdr-write-integer (xdrstream value)
       (check-type value integer)
       (putcell xdrstream value))

(defun xdr-read-boolean (xdrstream)
       (let ((value (getcell xdrstream)))
            (case value
                (0 nil)
                (1 t)
                (otherwise (rpc-signal-error t
                                  `(illegal-boolean ,value))))))

(defun xdr-write-boolean (xdrstream value)
       (putcell xdrstream (if (null value)
                              0
                              1)))

(defun xdr-read-unsigned (xdrstream)
       (getunsigned xdrstream))

(defun xdr-write-unsigned (xdrstream value)
       (putunsigned xdrstream value))

(defun xdr-read-hyperinteger (xdrstream)
       (let ((value (+ (ash (getunsigned xdrstream)
                            32)
                       (getunsigned xdrstream))))
            (if (> value twoto63minusone)
                (- value twoto64th)
                value)))

(defun xdr-write-hyperinteger (xdrstream value)
       (check-type value xdr-hyperinteger)
       (putunsigned xdrstream (ash value -32))
       (putunsigned xdrstream (logand value twoto32minusone)))

(defun xdr-read-hyperunsigned (xdrstream)
       (+ (ash (getunsigned xdrstream)
               32)
          (getunsigned xdrstream)))

(defun xdr-write-hyperunsigned (xdrstream value)
       (check-type value xdr-hyperunsigned)
       (putunsigned xdrstream (ash value -32))
       (putunsigned xdrstream (logand value twoto32minusone)))

(defun xdr-read-string (xdrstream)
       (let* ((nbytes (xdr-unsigned xdrstream))
              (string (il:allocstring nbytes)))
             (getrawbytes xdrstream (vector-base string)
                    0 nbytes)
             (skipbytes xdrstream (padding-bytes nbytes))
             string))

(defun xdr-write-string (xdrstream string)
       (check-type string string)
       (when (and (il:%fat-string-array-p string)
                  (il:%fat-string-array-p (setq string (copy-seq string
                                                              ))))
                                           ; Only 8-bit chars
                                           ; supported in xdr. 
                                           ; COPY-SEQ is just in case
                                           ; it was all thin chars in
                                           ; a formerly fat string.
           (error "XDR string contains NS characters: ~S" string))
       (let ((nbytes (il:nchars string)))
            (xdr-unsigned xdrstream nbytes)
            (putrawbytes xdrstream (vector-base string)
                   (vector-offset string)
                   nbytes)
            (zerobytes xdrstream (padding-bytes nbytes))))

(defun xdr-read-float (xdrstream)
       
       ;; Read a single-precision IEEE floating point number. 
       ;; Fortunately, that's our internal format as well.
       (let ((value (il:ncreate 'il:floatp)))
            (getrawbytes xdrstream value 0 4)))

(defun xdr-write-float (xdrstream v)
       
       ;; Write a single-precision IEEE floating point number. 
       ;; Fortunately, that's our internal format as well.
       (putrawbytes xdrstream (il:\\dtest v 'floatp)
              0 4))

(defun xdr-read-string-pointer (xdrstream)
       "This is a gross hack to handle what amounts to bulk data."
       (let* ((nbytes (xdr-unsigned xdrstream))
              (place (getoffset xdrstream))
              (packet (car place))
              (byteoffset (cdr place)))
             
             ;; This only works for UDP!!
             
             ;; Returns ((packet . byteoffset) . number-of-bytes))
             (prog1 (cons (cons packet byteoffset)
                          nbytes)
                 (skipbytes xdrstream (padding-bytes nbytes)))))

(defun xdr-read-array (xdrstream bytecount element-type)
       "Read an opaque array of bytecount bytes."
       (let ((quadbytecount (logand (+ bytecount 3)
                                   -4)))
            
            ;; QUADBYTECOUNT is BYTECOUNT rounded up to multiple of 4,
            ;; which is the number of actual bytes on the stream. 
            ;; Note that we will always be able to read the extra
            ;; bytes, if any, in the same operation because arrays in
            ;; this system are always a multiple of 4 bytes long.
            (if (eq element-type :unboxed)
                (let ((array (il:\\allocblock (ash quadbytecount -2))))
                                           ; Just get into raw array
                                           ; block
                     (getrawbytes xdrstream array 0 quadbytecount)
                     array)
                (let ((array (make-array bytecount :element-type
                                    (or element-type 'string-char))))
                                           ; Read into array of
                                           ; specified type. We assume
                                           ; the type packs tensely
                     (getrawbytes xdrstream (il:|fetch| (il:oned-array
                                                         il:base)
                                                   il:|of| array)
                            0 quadbytecount)
                     array))))

(defun xdr-write-array (xdrstream bytecount element-type array)
       "Write an opaque array of bytecount bytes."
       (cond ((null array)
                                           ; Convenient shorthand:
                                           ; write all zeros
              (zerobytes xdrstream bytecount))
             ((eq element-type :unboxed)
              (putrawbytes xdrstream array 0 bytecount))
             (t (putrawbytes xdrstream (il:|fetch| (il:oned-array
                                                    il:base)
                                              il:|of| array)
                       (il:|fetch| (il:oned-array il:offset)
                              il:|of| array)
                       bytecount))))

(defun xdr-write-string-pointer (xdrstream value)
       "This is a gross hack to handle what amounts to bulk data."
       
       ;; Value is ((buffer . offset) . nbytes), where the first
       ;; element can be just buffer if offset is zero.
       (let* ((buffer (car value))
              (nbytes (cdr value))
              (outstream (rpc-stream-outstream xdrstream))
              (offset 0))
             (when (consp buffer)
                 (setq offset (cdr buffer)
                       buffer
                       (car buffer)))
             (xdr-unsigned xdrstream nbytes)
             (putrawbytes xdrstream buffer offset nbytes)
             (zerobytes xdrstream (padding-bytes nbytes))))

(defun xdr-skip-primitive (xdrstream n)
       (skipbytes xdrstream n))

(defun xdr-zero-primitive (xdrstream n)
       (zerobytes xdrstream n))

(defmacro xdr-read-write-void (&rest ignore)
       "VOID type compiles into this"
       '(progn nil))

(defun xdr-enumeration-fault (key oper type)
       
       ;; Called when you try to read/write (per OPER) the value KEY,
       ;; which is not valid for an enumeration.  TYPE may be the
       ;; parent type.
       (case oper
           (read 
              (cerror "Return the integer ~D as the value" "Read unrecognized value ~D for enumeration type~@[ ~S~]~@[ in program ~A~]"
                     key type (and *program* (rpc-program-name 
                                                    *program*)))
              key)
           (write (error 
 "~S not a valid value for enumeration type~@[ ~S~]~@[ in program ~A~]"
                         key type (and *program* (rpc-program-name
                                                  *program*))))))

(defun xdr-make-opaque (bytecount &optional element-type)
       "Create an XDR object of type (:opaque bytecount element-type) initialized to zero."
       (if (eq element-type :unboxed)
           (il:\\allocblock (ash (+ bytecount 3)
                                 -2))
           (make-array bytecount :element-type (or element-type
                                                   'string-char))))

                                           ; These are for backward
                                           ; compatibility


(defun xdr-boolean (xdrstream &optional (value t writep))
       (cond (writep (putcell xdrstream (if (null value)
                                            0
                                            1)))
             (t (setq value (getcell xdrstream))
                (ccase value (0 nil)
                       (1 t)))))

(defun xdr-integer (xdrstream &optional (value nil writep))
       (cond (writep (check-type value integer)
                    (putcell xdrstream value))
             (t (getcell xdrstream))))

(defun xdr-unsigned (xdrstream &optional (value nil writep))
       (if writep
           (putunsigned xdrstream value)
           (getunsigned xdrstream)))

(defun xdr-hyperinteger (xdrstream &optional (value nil writep))
       (cond (writep (xdr-write-hyperinteger xdrstream value))
             (t (xdr-read-hyperinteger xdrstream))))

(defun xdr-hyperunsigned (xdrstream &optional (value nil writep))
       (cond (writep (xdr-write-hyperunsigned xdrstream value))
             (t (xdr-read-hyperunsigned xdrstream))))

(defun xdr-string (xdrstream &optional (string nil writep))
       (cond (writep (check-type string string)
                    (when (and (il:%fat-string-array-p string)
                               (il:%fat-string-array-p
                                (setq string (copy-seq string))))
                                           ; Only 8-bit chars
                                           ; supported in xdr. 
                                           ; COPY-SEQ is just in case
                                           ; it was all thin chars in
                                           ; a formerly fat string.
                        (error "XDR string contains NS characters: ~S"
                               string))
                    (let ((nbytes (il:nchars string)))
                         (xdr-unsigned xdrstream nbytes)
                         (putrawbytes xdrstream (vector-base string)
                                (vector-offset string)
                                nbytes)
                         (zerobytes xdrstream (padding-bytes nbytes))))
             (t (let* ((nbytes (xdr-unsigned xdrstream))
                       (string (il:allocstring nbytes)))
                      (getrawbytes xdrstream (vector-base string)
                             0 nbytes)
                      (skipbytes xdrstream (padding-bytes nbytes))
                      string))))

(defun xdr-string-pointer (xdrstream &optional (value t writep))
       "This is a gross hack to handle what amounts to bulk data."
       (if writep
           (let* ((buffer (car value))
                  (nbytes (cdr value))
                  (outstream (rpc-stream-outstream xdrstream))
                  (offset 0))
                                           ; Value is ((buffer .
                                           ; offset) . nbytes), where
                                           ; the first element can be
                                           ; just buffer if offset is
                                           ; zero.
                 (when (consp buffer)
                     (setq offset (cdr buffer)
                           buffer
                           (car buffer)))
                 (xdr-unsigned xdrstream nbytes)
                 (putrawbytes xdrstream buffer offset nbytes)
                 (zerobytes xdrstream (padding-bytes nbytes)))
           (let* ((nbytes (xdr-unsigned xdrstream))
                  (place (getoffset xdrstream))
                  (packet (car place))
                  (byteoffset (cdr place)))
                 
                 ;; This only works for UDP!!
                 
                 ;; Returns ((packet . byteoffset) . number-of-bytes))
                 (prog1 (cons (cons packet byteoffset)
                              nbytes)
                     (skipbytes xdrstream (padding-bytes nbytes))))))

(defun xdr-float (xdrstream &optional (v nil writep))
       
       ;; Read or write a single-precision IEEE floating point number.
       ;; Fortunately, that's our internal format as well.
       (if writep
           (putrawbytes xdrstream (il:\\dtest v 'floatp)
                  0 4)
           (let ((value (il:ncreate 'il:floatp)))
                (getrawbytes xdrstream value 0 4))))

(defun xdr-void (xdrstream &optional (value t writep))
       nil)

(defun xdr-opaque-primitive (xdrstream nbytes &optional
                                   (string nil writep))
       
       ;; Strictly for backward-compatibility with old compiled rpc
       ;; code
       (cond (writep (putrawbytes xdrstream (il:|fetch| (il:stringp
                                                         il:base)
                                                   il:|of| string)
                            (il:|fetch| (il:stringp il:offst)
                                   il:|of| string)
                            nbytes)
                    (unless (eql (setq nbytes (logand nbytes 3))
                                 0)
                        (dotimes (i (- 4 nbytes))
                            (putbyte xdrstream 0))))
             (t (let ((string (il:allocstring nbytes)))
                     (getrawbytes xdrstream (il:|fetch| (il:stringp
                                                         il:base)
                                                   il:|of| string)
                            (il:|fetch| (il:stringp il:offst)
                                   il:|of| string)
                            nbytes)
                     (unless (eql (setq nbytes (logand nbytes 3))
                                  0)
                         (dotimes (i (- 4 nbytes))
                             (getbyte xdrstream)))
                     string))))

;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPC")

(in-package "RPC2")

;;;; SUN REMOTE PROCEDURE CALLS

;;;; Originally written by Jeff Finger at the SUMEX-AIM Computing
;;;; Resource at Stanford University under support from National
;;;; Institutes of Health Grant NIH 5P41 RR00785.

;;;; Modified to work under Medley 1.0-S by Atty Mullins & Doug Cutting.

;;;; Extensively revised by Bill van Melle

(export '(define-remote-program undefine-remote-program 
	  remote-procedure-call call-via-portmapper 
	  create-unix-authentication setup-rpc perform-rpc 
	  open-rpcstream close-rpcstream list-remote-programs 
	  find-rpc-program find-rpc-procedure find-rpc-host 
	  xdr-make-opaque cache-socket clear-cache))

(export '(xdr-codegen xdr-codegen-1 xdr-read-boolean xdr-read-integer 
	  xdr-read-unsigned xdr-read-float xdr-read-string 
	  xdr-write-boolean xdr-write-integer xdr-write-unsigned
	  xdr-write-float xdr-write-string xdr-gencode-inline))

(export '(udp tcp))

(export '(*debug* *compile-xdr-code* *xdr-primitive-types* 
	  *xdr-constructed-types* *rpc-programs* 
	  *msec-until-timeout* *msec-between-tries* 
	  *rpc-ok-to-cache* *rpc-socket-cache* 
	  *rpc-well-known-sockets* *rpc-protocol-types* 
	  *use-os-networking*))

(defglobalparameter *use-os-networking* nil
  "If false, RPC will use Interlisp-D TCP/IP,
 if true RPC will use the host operating system's IPC mechanism.")

(defun rpc-around-exit (&optional event)
  ;; Set flag so when we wake up on Maiko we know to use RPCOS. 
  ;; User still has to have loaded it, though.
  (case event
    ((il:afterlogout il:aftersysout il:aftersavevm il:aftermakesys nil)
     (setq *use-os-networking* (eq (il:machinetype) 'il:maiko)))
    (t nil)))

(eval-when (load)
  ;; Can't parse: (il:appendvars (il:aroundexitfns rpc-around-exit))
					; This is an APPENDVARS so
					; that RPC-AROUND-EXIT runs
					; FIRST at startup, before
					; GREET in particular
  (require "RPCSTRUCT")
  (require "RPCRPC")
  (require "RPCXDR")
					; Load the appropriate
					; transport.
  (cond ((rpc-around-exit)
					; On Maiko
	 (il:filesload (il:sysload) il:rpcos))
	(t				; Load only UDP.  If you
					; want to use RPC over TCP,
					; you must load TCP
					; yourself
	 (il:filesload (il:sysload) il:tcpllip il:tcpudp il:rpcudp)))
       
  (require "RPCPORTMAPPER"))

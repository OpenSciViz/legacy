;;;-*- Package: RPC2; Syntax: Common-Lisp; Mode: Lisp; Base: 10 -*-

;;; Copyright (c) 1988, 1989, 1990 Stanford University and Xerox Corporation.  
;;; All Rights Reserved.

;;; Permission is hereby granted to use, reproduce, and prepare derivative
;;; works of this software provided that any derivative work based upon
;;; this software is licensed to Stanford University and Xerox Corporation
;;; at no charge.  Any distribution of this software or derivative works
;;; must comply with all applicable United States export control laws.
;;; Any copy of this software or of any derivative work must include the
;;; above copyright notice of Stanford University and Xerox Corporation
;;; and this paragraph.  This software is made available AS IS, and
;;; STANFORD UNIVERSITY AND XEROX CORPORATION DISCLAIM ALL WARRANTIES,
;;; EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
;;; AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
;;; LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
;;; EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
;;; NEGLIGENCE) OR STRICT LIABILITY, EVEN IF STANFORD UNIVERSITY AND/OR
;;; XEROX CORPORATION IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

(provide "RPCUDP")

(in-package "RPC2")

;; Definitions for UDP transport of RPC

(eval-when (eval compile)
  (require "RPCDECLS")
  (load-tcp-exports))

                                           ; I'd like these to be
                                           ; inside the eval-when,
                                           ; too, but then they don't
                                           ; get eval'ed when I load
                                           ; the file prop!


(defconstant *udp-fixed-overhead-bytes*
       (+ il:\\udpovlen (unfold (il:indexf (il:|fetch| (il:etherpacket
                                                        il:epbody)
                                                  il:|of| inpacket))
                               *bytes-per-word*))
       "Number of bytes of fixed overhead in a UDP packet before you get to data.  Have to add variable IP header length to this."
       )

(defmacro udp-output-base (packet)
       "Pointer to start of IP image in packet.  Output byte pointers are relative to this."
       `(il:|fetch| (il:ip il:ipbase)
               il:|of|
               ,packet))

(defglobalparameter *udp-rpc-methods* 

       ;; These quotes should really be #', but in Xerox Lisp the
       ;; former is significantly nicer on some machines, and the
       ;; result is the same.
       (make-rpc-methods :protocol 'udp :getbyte 'udp-getbyte :putbyte
              'udp-putbyte :getcell 'udp-getcell :getunsigned
              'udp-getunsigned :getoffset 'udp-getoffset :putcell
              'udp-putcell :skipbytes 'udp-skipbytes :zerobytes
              'udp-zerobytes :getrawbytes 'udp-getrawbytes :putrawbytes
              'udp-putrawbytes :close 'udp-close :initialize
              'udp-initialize :exchange 'exchange-udp-packets)
       "Op vector for UDP version of RPC")

(defun rpc-create-udp-stream (&optional destaddr destsocket)
       "Create a new RPC Stream with the vector of functions set up to for UDP Protocol Datagrams."
       (make-rpc-stream :methods *udp-rpc-methods* :ipsocket
              (il:udp.open.socket nil nil 'udp-handle-icmp)
              :inbyteptr 0 :outstream nil :destaddr
              (and destaddr (if (integerp destaddr)
                                destaddr
                                (rpc-resolve-host destaddr)))
              :destsocket
              (and destsocket (if (integerp destsocket)
                                  destsocket
                                  (rpc-resolve-prog destsocket nil
                                         'udp)))))

(defun udp-initialize (stream destaddr destsocket)
       (when (typep (rpc-stream-instream stream)
                    'il:etherpacket)
           
           ;; Release  Etherpacket used for previous input from remote
           ;; host. This could be done earlier, when PARSE-RPC-STREAM
           ;; finishes with the packet, but since *RPCSTREAM* still
           ;; points at the stream for debugging, it is better to wait
           ;; until now..
           (il:\\release.etherpacket (rpc-stream-instream stream))
           (setf (rpc-stream-instream stream)
                 nil))
       (let ((packet (rpc-stream-outstream stream)))
            
            ;; Construct an empty UDP packet.  Our INBYTEPTR is
            ;; meaningless until a packet arrives.  OUTBYTEPTR is
            ;; relative to IPBASE.
            (when destaddr
                (unless (integerp destaddr)
                    (setq destaddr (rpc-resolve-host destaddr)))
                (setf (rpc-stream-destaddr stream)
                      destaddr))
            (cond (nil (when (null packet)
                           (setf (rpc-stream-outstream stream)
                                 (setq packet (
                                              il:\\allocate.etherpacket
                                               ))))
                       (il:udp.setup packet (or destaddr (
                                                    rpc-stream-destaddr
                                                          stream))
                              nil 0 (rpc-stream-ipsocket stream)))
                  ((null packet)
                                           ; First time thru, give us
                                           ; a packet
                   (setq packet (il:\\allocate.etherpacket))
                   (il:udp.setup packet (or destaddr (
                                                    rpc-stream-destaddr
                                                      stream))
                          nil 0 (rpc-stream-ipsocket stream))
                   (setf (rpc-stream-outstream stream)
                         packet))
                  (destaddr                ; May have wanted to change
                                           ; address
                         (setf (il:|fetch| (il:ip 
                                                il:ipdestinationaddress
                                                  )
                                      il:|of| packet)
                               destaddr)))
            (when destsocket
                                           ; We'll deal with the
                                           ; socket later
                (setf (rpc-stream-destsocket stream)
                      (if (integerp destsocket)
                          destsocket
                          (rpc-resolve-prog destsocket nil 'udp))))
            (let ((emptyudplen (+ il:\\ipovlen il:\\udpovlen)))
                 (setf (rpc-stream-inbyteptr stream)
                       -1
                       (il:|fetch| (il:udp il:udplength)
                              il:|of| packet)
                       il:\\udpovlen
                       (il:|fetch| (il:ip il:iptotallength)
                              il:|of| packet)
                       emptyudplen
                       (rpc-stream-outbyteptr stream)
                       emptyudplen))
            t))

(defun udp-close (rpcstream)
       "Deallocate an RPC Stream. Tries to cleanup after itself."
       (il:udp.close.socket (rpc-stream-ipsocket rpcstream)))

(defun
 exchange-udp-packets
 (rpcstream errorflg xid)
 "Given the specified timeout and time between tries, this routine continues to send out UDP packets until it either gets a reply or times out."
 (let
  ((timer (and *msec-until-timeout* (il:setuptimer *msec-until-timeout*
                                           )))
   (timeout *msec-between-tries*)
   (outpacket (rpc-stream-outstream rpcstream))
   (socket (rpc-stream-ipsocket rpcstream))
   (length (rpc-stream-outbyteptr rpcstream))
   (timeout-count 0)
   (destsocket (rpc-stream-destsocket rpcstream))
   inpacket tried-new-socket)
  (incf (il:|fetch| (il:udp il:udplength)
               il:|of| outpacket)
        (- length (il:|fetch| (il:ip il:iptotallength)
                         il:|of| outpacket)))
  (il:|replace| (il:ip il:iptotallength)
         il:|of| outpacket il:|with| length)
                                           ; Adjust the lengths in the
                                           ; packet to reflect what
                                           ; we've written
  (when (and (numberp *debug*)
             (> *debug* 5))
        (break 
             "About to send packet (rpc-stream-outstream *rpc-stream*)"
               ))
  (loop
   
   ;; This loop keeps sending a packet and waiting for replies until
   ;; someone explicitly returns something
   (when 
       (or (null destsocket)
           (let ((soc destsocket))
                
                ;; Need to fill in the destination socket.  We didn't
                ;; do this in initialize, because we could get
                ;; timeouts on this as well, in which case we want to
                ;; be here to do the error handling.
                (cond ((and (not (consp soc))
                            (or (integerp soc)
                                (integerp (setq soc (rpc-find-socket
                                                     (
                                                    rpc-stream-destaddr
                                                      rpcstream)
                                                     destsocket
                                                     'udp :returnerrors
                                                     )))))
                       (setf (il:|fetch| (il:udp il:udpdestport)
                                    il:|of| outpacket)
                             soc)
                       (setq destsocket nil)
                       t)
                      (t                   ; Got an error back, either
                                           ; from RPC-FIND-SOCKET or
                                           ; UDP-RECOVER-SOCKET on a
                                           ; previous iteration.
                         (cond ((and (eq (cadr soc)
                                         'rpc-timeout)
                                     timer)
                                           ; Drop thru to timeout
                                           ; handling code
                                (il:setuptimer 0 timer)
                                (setq destsocket nil))
                               (t (return-from exchange-udp-packets
                                         (rpc-signal-error errorflg
                                                (cdr soc)))))))))
       (il:\\udp.flush.socket.queue socket)
       (il:udp.send socket outpacket)
       (il:block)
       (loop 
             ;; This loop tries to get a packet in reply to what we
             ;; just sent out
             (cond ((not (setq inpacket (il:udp.get socket timeout)))
                                           ; Nothing came in timeout,
                                           ; so try again.  Back off a
                                           ; little, too.
                    (setq timeout (min (ash timeout 1)
                                       10000))
                    (return))
                   ((not (eq (il:|fetch| (il:ip il:ipprotocol)
                                    il:|of| inpacket)
                             il:\\udp.protocol))
                                           ; ICMP packet saying port
                                           ; unreachable.  Go fix up
                                           ; the socket cache.
                    (cond
                     ((il:\\ip.broadcast.address (il:|fetch|
                                                  (il:ip 
                                                il:ipdestinationaddress
                                                         )
                                                  il:|of| outpacket))
                                           ; This was a broadcast rpc,
                                           ; so ignore this stupid
                                           ; response
                      )
                     ((setq destsocket (udp-recover-socket outpacket 
                                              rpcstream))
                                           ; Got a new socket, so try
                                           ; again
                      (setq tried-new-socket t)
                      (when (and (integerp destsocket)
                                 timer)
                          

                       ;; Actually got a new socket # back, so reset
                       ;; timer as if we only now just started. 
                       ;; Alternative is we got a program back, which
                       ;; really means that portmapper timed out, but
                       ;; we want to do the looping here.
                          (il:setuptimer *msec-until-timeout* timer))
                      (return))
                     (t                    ; Failed--give up.  This
                                           ; looks a lot like the RPC
                                           ; reply "no such program"
                        (return-from exchange-udp-packets
                               (rpc-signal-error
                                errorflg
                                `(program-unavailable ,*program* 
                                        "No process at destination"))))
                     ))
                   (t (setf (rpc-stream-instream rpcstream)
                            inpacket)
                      (setf (rpc-stream-inbyteptr rpcstream)
                            (+ *udp-fixed-overhead-bytes*
                               (unfold (il:|fetch| (il:ip 
                                                      il:ipheaderlength
                                                          )
                                              il:|of| inpacket)
                                      *bytes-per-cell*)))
                                           ; Byte offset of start of
                                           ; udp data
                      (when (and (numberp *debug*)
                                 (> *debug* 5))
                            (break 
                     "A reply is in (rpc-stream-instream *rpc-stream*)"
                                   ))
                      (cond ((= (getunsigned rpcstream)
                                xid)
                                           ; ID's match, so it's ok
                             (when (and (not tried-new-socket)
                                        (program-unavailable-reply-p
                                         rpcstream)
                                        (setq destsocket
                                              (udp-recover-socket
                                               outpacket rpcstream)))
                                           ; Moby Kludge!!!  Sometimes
                                           ; when servers crash and
                                           ; return, old socket has a
                                           ; different program on it,
                                           ; which returns this
                                           ; "program unavailable"
                                           ; error.  Ought to catch
                                           ; this in Parse-rpc-reply,
                                           ; but the recovery is much
                                           ; more awkward there.
                                 (setq tried-new-socket t)
                                 (when (and (integerp destsocket)
                                            timer)
                                     

                       ;; Actually got a new socket # back, so reset
                       ;; timer as if we only now just started. 
                       ;; Alternative is we got a program back, which
                       ;; really means that portmapper timed out, but
                       ;; we want to do the looping here.
                                     (il:setuptimer 
                                            *msec-until-timeout* timer))
                                 (return))
                             (return-from exchange-udp-packets t))
                            ((and (numberp *debug*)
                                  (> *debug* 1))
                             (format-t 
                                    " (discarded packet with wrong ID)"
                                    )))
                                           ; So go around the loop
                                           ; again
                      ))))
   (when (and timer (il:timerexpired? timer))
                                           ; Timed out
       (unless (and (not tried-new-socket)
                    (progn                 ; Time out could be because
                                           ; server crashed, is using
                                           ; new socket, and process
                                           ; on old socket doesn't
                                           ; return error packets.  If
                                           ; UDP-RECOVER-SOCKET
                                           ; returns other than
                                           ; integer, we did not
                                           ; succeed in getting a new
                                           ; socket
                           (setq tried-new-socket t)
                           (integerp (setq destsocket (
                                                     udp-recover-socket
                                                       outpacket 
                                                       rpcstream)))))
           (let ((result (rpc-handle-timeout rpcstream (incf 
                                                          timeout-count
                                                             )
                                errorflg)))
                (unless (eq result :continue)
                       (return-from exchange-udp-packets result))))
       (il:setuptimer *msec-until-timeout* timer)
                                           ; Reset the timer and keep
                                           ; going
       (setq timeout *msec-between-tries*)))))

(defun
 program-unavailable-reply-p
 (rpcstream)
 
 ;; True if the reply waiting in RPCSTREAM is "Program Unavailable". 
 ;; What a kludge.
 
 ;; Code here is heavily optimized.
 (macrolet
  ((eql-getbasecell
    (base celloffset expected)
    
    ;; True if the 32-bit number at CELLOFFSET from BASE is EXPECTED
    (let
     ((offset (if (integerp celloffset)
                  (il:llsh celloffset 1)
                  0)))
     `(let ((base ,(if (integerp celloffset)
                       base
                       `(il:\\addbase ,base (il:llsh ,celloffset 1)))))
           (and (eq (il:\\getbase base (+ ,offset 1))
                    (logand ,expected 65535))
                (eq (il:\\getbase base ,offset)
                    (ash ,expected -16)))))))
  (let ((replybase (il:\\addbase (rpc-stream-instream rpcstream)
                          (il:lrsh (rpc-stream-inbyteptr rpcstream)
                                 1))))
       
       ;; REPLYBASE=> [reply] [accepted] [authtype] [authstring]
       ;; [accept-stat] data
       (and (eql-getbasecell replybase 1 *rpc-reply-accepted*)
            (eql-getbasecell replybase (+ (getbase-unsigned
                                           replybase
                                           (* *bytes-per-cell* 3))
                                           ; Length of authentication
                                           ; string
                                          4)
                   *rpc-accept-program-unavailable*)
            (eql-getbasecell replybase 0 *rpc-msg-reply*)))))

(defun udp-handle-icmp (icmp sentip socket)
       
       ;; Handle an ICMP destination unreachable packet sent in
       ;; response to SENTIP on this SOCKET.
       (when (eql (il:|fetch| (il:icmp il:icmpcode)
                         il:|of| icmp)
                  il:\\icmp.port.unreachable)
                                           ; Need to know about Port
                                           ; unreachable so we can
                                           ; recache the socket. 
                                           ; Queue this up so it will
                                           ; be read by
                                           ; EXCHANGE-UDP-PACKETS
           (il:\\ip.default.inputfn icmp socket)))

(defun
 udp-recover-socket
 (outpacket rpcstream)
 
 ;; Called when we get a port unreachable when trying to send
 ;; OUTPACKET.  This typically happens if server boots and assigns the
 ;; service a different socket.  Returns a new port, or NIL to fail.
 (let
  ((program (rpc-stream-destsocket rpcstream)))
  (and
   (typep program 'rpc-program)
   (let*
    ((destaddr (rpc-stream-destaddr rpcstream))
     (prognum (rpc-program-number program))
     (version (rpc-program-version program))
     (oldentry
      (find-if #'(lambda (entry)
                        (and (eql (car entry)
                                  destaddr)
                             (eql (car (setq entry (cdr entry)))
                                  prognum)
                             (eql (car (setq entry (cdr entry)))
                                  version)
                             (eq (car (setq entry (cdr entry)))
                                 'udp)))
             *rpc-socket-cache*))
     (oldsocket (il:|fetch| (il:udp il:udpdestport)
                       il:|of| outpacket))
     newsocket cachedsocket)
    (cond ((and oldentry (not (eql (setq cachedsocket (fifth oldentry))
                                   oldsocket)))
                                           ; There's a different
                                           ; socket in cache, maybe
                                           ; someone looked it up
                                           ; already, or called us
                                           ; with out of date info
           cachedsocket)
          ((not (integerp (setq newsocket (let ((*rpc-ok-to-cache*
                                                 nil))
                                               
                                           ; Don't let it find the old
                                           ; socket
                                               (rpc-find-socket
                                                destaddr program
                                                'udp :returnerrors)))))
                                           ; Portmapper failed.  We'll
                                           ; just send back the error
                                           ; (or program object if
                                           ; timeout) and let
                                           ; exchange-udp-packets
                                           ; handle it
           (or newsocket program))
          ((eql newsocket oldsocket)
                                           ; Server still thinks it's
                                           ; on this socket, so no use
                                           ; trying again
           nil)
          (t                               ; good, service moved to a
                                           ; different socket, so use
                                           ; that, and record the
                                           ; change
             (if oldentry
                 (setf (fifth oldentry)
                       newsocket)
                 (cache-socket program destaddr newsocket))
             newsocket))))))

(defun udp-getbyte (rpcstream)
       (let ((offset (rpc-stream-inbyteptr rpcstream)))
            (prog1 (il:\\getbasebyte (rpc-stream-instream rpcstream)
                          offset)
                (setf (rpc-stream-inbyteptr rpcstream)
                      (1+ offset)))))

(defun udp-putbyte (rpcstream byte)
       (let ((offset (rpc-stream-outbyteptr rpcstream)))
            (il:\\putbasebyte (udp-output-base (rpc-stream-outstream
                                                rpcstream))
                   offset byte)
            (setf (rpc-stream-outbyteptr rpcstream)
                  (1+ offset))))

(defun udp-getcell (rpcstream)
       (let ((byteoffset (rpc-stream-inbyteptr rpcstream)))
            (prog1 (getbase-integer (rpc-stream-instream rpcstream)
                          byteoffset)
                (setf (rpc-stream-inbyteptr rpcstream)
                      (+ byteoffset *bytes-per-cell*)))))

(defun udp-getunsigned (rpcstream)
       (let ((byteoffset (rpc-stream-inbyteptr rpcstream)))
            (prog1 (getbase-unsigned (rpc-stream-instream rpcstream)
                          byteoffset)
                (setf (rpc-stream-inbyteptr rpcstream)
                      (+ byteoffset *bytes-per-cell*)))))

(defun udp-putcell (rpcstream value)
       "Write a 32-bit value to udp stream."
       (let ((offset (rpc-stream-outbyteptr rpcstream)))
            (putbase-integer (udp-output-base (rpc-stream-outstream
                                               rpcstream))
                   offset value)
            (setf (rpc-stream-outbyteptr rpcstream)
                  (+ offset *bytes-per-cell*))))

(defun udp-getoffset (rpcstream)
       (cons (rpc-stream-instream rpcstream)
             (rpc-stream-inbyteptr rpcstream)))

(defun udp-getrawbytes (rpcstream base offset nbytes)
       "Get NBYTES bytes from rpc stream, store them at base,offset."
       (let ((soffset (rpc-stream-inbyteptr rpcstream)))
            (il:\\movebytes (rpc-stream-instream rpcstream)
                   soffset base offset nbytes)
            (setf (rpc-stream-inbyteptr rpcstream)
                  (+ soffset nbytes))
            base))

(defun udp-putrawbytes (rpcstream base offset nbytes)
       "Put NBYTES bytes to rpc stream from base,offset."
       (let ((pktoffset (rpc-stream-outbyteptr rpcstream)))
            (il:\\movebytes base offset (udp-output-base (
                                                   rpc-stream-outstream
                                                          rpcstream))
                   pktoffset nbytes)
            (setf (rpc-stream-outbyteptr rpcstream)
                  (+ pktoffset nbytes))))

(defun udp-skipbytes (rpcstream nbytes)
       (incf (rpc-stream-inbyteptr rpcstream)
             nbytes))

(defun udp-zerobytes (rpcstream nbytes)
       (let ((base (udp-output-base (rpc-stream-outstream rpcstream)))
             (offset (rpc-stream-outbyteptr rpcstream)))
            (dotimes (i nbytes)
                (il:\\putbasebyte base offset 0)
                (incf offset))
            (setf (rpc-stream-outbyteptr rpcstream)
                  offset)))


;; Filepkg type il:fns not supported: (il:fns il:udp.get.bytes
;; il:\\udp.set.checksum.zero)


                                           ; Prettier printing



;; Filepkg type il:fns not supported: (il:fns il:printudp
;; il:rpc.graphic.charp il:printrpc.anything il:printrpc.auth
;; il:printrpcdata)



;; Can't parse: (il:addvars (il:udpignoreports 520))



;; Filepkg type il:globalvars not supported: (il:globalvars
;; il:udpignoreports)


(eval-when (load)
       (unless (fboundp 'il:\\udp.handle.icmp)
           (il:filesload (il:sysload)
                  il:iprpcpatch))
       (il:changename 'il:udp.send 'il:\\udp.set.checksum 
              'il:\\udp.set.checksum.zero)
       
       ;; Translated (il:files (il:sysload) il:medley-ippatches) to
       ;; require forms
       (require "MEDLEY-IPPATCHES"))

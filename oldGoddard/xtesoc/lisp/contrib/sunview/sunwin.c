/* $Header: sunwin.c,v 1.6 1993/05/05 04:29:30 layer Exp $ */

/* begin additions for sunwindow support */

#include <sunwindow/window_hs.h>

#ifdef OS_4_1
#define OS_4_0
#endif

/* the following functions are written as macros (pixwin.h) so we have to
 * provide our own functions
 */

my_pw_rop(pw,x,y,w,h,op,sp,sx,sy)
struct pixwin *pw;
{
    return pw_rop(pw,x,y,w,h,op,sp,sx,sy);
}

my_pw_write(pw,x,y,w,h,op,spr,sx,sy)
struct pixwin *pw;
{
    return pw_write(pw,x,y,w,h,op,spr,sx,sy);
}

my_pw_writebackground(pw,x,y,w,h,op)
struct pixwin *pw;
{
    return pw_writebackground(pw,x,y,w,h,op);
}

struct pixwin *
my_pw_region(pw,x,y,w,h)
struct pixwin *pw;
{
    return pw_region(pw,x,y,w,h);
}


/* pixrect unmacro defining */

my_pr_rop(dpr, dx, dy, w, h, op, spr, sx, sy)
struct pixrect *dpr,*spr;
{
    return pr_rop(dpr, dx, dy, w, h, op, spr, sx, sy);
}

my_pr_get(pr, x, y)
struct pixrect *pr;
{
    return pr_get(pr, x, y);
}

my_pr_put(pr, x, y, val)
struct pixrect *pr;
{
    return pr_put(pr, x, y, val);
}

my_pr_vector(pr, x0, y0, x1, y1, op, color)
struct pixrect *pr;
{
    return pr_vector(pr, x0, y0, x1, y1, op, color);
}

my_pr_destroy(pr)
struct pixrect *pr;
{
    return pr_destroy(pr);
}

#ifdef OS_3_2
my_pr_close(pr)
struct pixrect *pr;
{
    return pr_destroy(pr);
}
#endif

#ifdef OS_4_0
my_pr_close(pr)
struct pixrect *pr;
{
    return pr_destroy(pr);
}
#endif

/*
 * The following deal with the fact the Sun4 returns structure functions
 * where the usual return value may not actually be a pointer to the struct.
 */

struct pr_size
my_pf_textwidth(len, font, text)
int len;
Pixfont *font;
char *text;
{
    return ( pf_textwidth(len, font, text) );
}

struct pr_size
my_pf_textbatch(where, lengthp, font, text)
struct pr_pos where [];
int *lengthp;
Pixfont *font;
char *text;
{
    return ( pf_textbatch(where, lengthp, font, text) );
}

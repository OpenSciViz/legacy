;;; Recursive Polygons

;;; Steve Gadol, july 22, 1986

;;; POINTS is a list of (fixnum) verteces (X0 Y0 X1 Y1 ...) that describe
;;; a polygon.  X-ORG and Y-ORG defines the origin of this polygon
;;; (CAR SCALINGS) sets the scale factor for this instance of the polygon.
;;;
;;; At each level of the recursion, we draw the polygon at X-ORG,Y-ORG
;;; scaled by (CAR SCALINGS), then recurse to draw the polygon at each
;;; of this polygon, scaled by (CDR SCALINGS).
;;;

;;; $Header: isogons.cl,v 1.6 1993/05/05 04:29:40 layer Exp $

(eval-when (compile eval load)
  (intern (symbol-name :isogon-demo) (find-package :user))
  (intern (symbol-name :kill-isogon-windows) (find-package :user)))

(defpackage :isogons
  (:use :lisp :excl :sunview :ff :cltl1)
  (:export isogon-demo kill-isogon-windows)
  (:import-from :user isogon-demo kill-isogon-windows))

(in-package :isogons)
(eval-when (compile load eval) (require :sunview))

(defvar bw-only nil)

(if* (and (not bw-only) (not (fboundp 'use-rainbow-color-map)))
   then (load "rainbow.o")
        (defforeign-list '((rainbow-setup :entry-point "_rainbow_setup") 
                           (getr) (getg) (getb))) 
        (defun use-rainbow-color-map (pw) 
          (let ((cms-rainbowsize (rainbow-setup))) 
            (pw-setcmsname pw "rainbow") 
            (pw-putcolormap pw 0 cms-rainbowsize (getr) (getg) (getb)))))

(if* (and (boundp 'bw-only) bw-only)
   then (defun use-rainbow-color-map (x) nil))

(defvar *window-list*)
(defvar *color*)

(defun isogon-plot (window  x y new-x new-y)
  (pw-vector window x y new-x new-y pix-src *color*)
  (notify-dispatch))

(defun isogon-clear-canvas (canvas)
  (pw-write canvas 0 0 260 350 pix-src 0 0 0))

(defun isogon-aux (window  points x-org y-org scalings leaves-only 
                          &optional (color 3))
  (when scalings
    (let ((scl (car scalings)))
      (do ((pts points (cddr pts)))
          ((null pts) nil)
        (let ((this-x (+ x-org (* (car pts) scl)))
              (this-y (+ y-org (* (cadr pts) scl)))
              (next-x (+ x-org (* (or (caddr pts) (car points)) scl)))
              (next-y (+ y-org (* (or (cadddr pts) (cadr points)) scl))))
	  (unless (and leaves-only (cdr scalings))
            (isogon-plot window  this-x this-y next-x next-y))))
      ;; recurse, using each vertex as a new origin
      (do ((pts points (cddr pts)))
          ((null pts) nil)
        (let ((this-x (+ x-org (* (car pts) scl)))
              (this-y (+ y-org (* (cadr pts) scl))))
          (isogon-aux window  points this-x this-y (cdr scalings) leaves-only
                      (mod (1+ color) 4)))))))

;;; Create a list of LENGTH fixnums, bounded within -BOUNDS to BOUNDS
;;;
(defun make-points (length bounds)
  (let  ((points '())
         (range (1+  (* 2 bounds))))
    (dotimes (i length)
      (push (- (random range) bounds) points))
    points))

;;; Choose a set of (possibly interesting) points
;;;
(defun choose-points ()
  (case (random 5)
    (0 '(0 2 2 0 0 -2 -2 0))            ; tiny diamond
    (1 '(1 1 -1 -1 1 -1 -1 1))          ; an hourglass
    (2 '(-2 -1 0 2 2 -1))               ; a triangle
    (3 '(-2 1 -1 -2 1 -2 2 1 0 3))      ; perhaps pentagonal
    (4 '(-2 -1 2 -1 -1 2 0 -3 1 2))     ; perhaps a star
))

;;; Chose a set of (possibly interesting) scalings
;;;

(defun choose-scalings ()
  (case (random 5)
    (0 '(24 11 8 ))
    (1 '(15 10 8 ))
    (2 '(18 11 9 ))
    (3 '(21 11 10 ))
    (4 '(17 10 9 7))))

(defun isogon (canvas &optional (x0 100) (y0 100) leaves-only)
  (isogon-aux canvas (choose-points) x0 y0 (choose-scalings) leaves-only 
              (max (random 4) 1)))

(defun isogon-demo (x &optional (x-org 125) (y-org 170))
  (let ((window-list nil))
    (cond ((null *window-list*) (allocate-windows)))
    (unwind-protect
      (progn
       (dotimes (i x)
	 (multiple-value-bind (frame canvas)
	   (allocate-window (mod i 6))
	   (setq *color* (1+ (random 7)))
	   (isogon canvas x-org y-org)
	   (sleep 1)))
       (sleep 3))
      (mapcar '(lambda(x)
		 (window-set (car x) win-show 0 0)
		 (notify-dispatch))
	        *window-list*))
    'user::done))


(defun allocate-windows ()
  (let ((window-list nil))
    (dotimes (i 6)
      (multiple-value-bind  (frame canvas)
	  (make-window :x (* i 40)
	    :y (* i 85)
	    :width 260
	    :height 350 
	    :title "Isogons" )
	  (setq window-list (cons (cons frame canvas) window-list))))
    (setq *window-list* window-list)))

(defun allocate-window (i)
  (let ((window (nth i *window-list*)))
    (isogon-clear-canvas (cdr window))
    (window-set (car window) win-show 1 0)
    (notify-dispatch)
    (values (car window) (cdr window))))

(defun make-window (&key (x 0) (y 0) (width 260) (height 350)
			 (title "lisp-window"))
  (let* ((base (window-create 0 frame win-x x win-y y win-width width
		win-height height frame-label title 0))
	 (cnvs (window-create base canvas 0)))
    (window-set base win-show 0 0)
    (notify-dispatch)
    (let ((canvas (get-canvas-pixwin cnvs)))
      (use-rainbow-color-map canvas)
      (values base canvas))))

(defun get-canvas-pixwin (cnvs)
    (window-get cnvs canvas-pixwin 0))

(defun kill-isogon-windows()
  (mapcar '(lambda (x)
	     (let ((frame (car x)))
	       (window-set frame frame-no-confirm 1 0)
	       (window-destroy frame)
	       (notify-dispatch)))
	  *window-list*)
  (setq *window-list* nil))

(allocate-windows)

(format t "~%;;; Run ISOGON-DEMO to run the demo. Pass to it the number
;;; of times to iterate~%")
(format t "~%;;; Use KILL-ISOGON-WINDOWS to reclaim SunView windows~%")

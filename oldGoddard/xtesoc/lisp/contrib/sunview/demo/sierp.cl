;; Sierpinski demo
;; written by steve gadol of sun.
;; modified to run under excl with the lower case sensitive option set
;;

;; $Header: sierp.cl,v 1.6 1993/05/05 04:29:45 layer Exp $

(eval-when (compile load eval) (require :sunview) (require :notify))

(eval-when (compile eval load)
  (intern (symbol-name :sierpinski) (find-package :user)))

(defpackage :sierp
  (:use :lisp :excl :sunview :ff :cltl1)
  (:import-from :user sierpinski))

(in-package :sierp)

(eval-when (compile) (proclaim '(optimize (speed 3) (safety 0))))

;; command strategy
;; button: 
;;   start drawing 	- *notify-command* to :draw
;;   stop  drawing	- *notify-command* to :exit
;;   exit  program	- *notify-command* to :exit
;;
;; command	select	     		dispatch
;; :draw     begin drawing 
;; :exit     throw to exit loop		throw to stop drawing
;;

(defvar *n*)  
(defvar *h0*)

(defvar *sizes*)
(defvar *iterations*)

(defvar *pw*)
(defvar *x*)
(defvar *y*)
(defvar *h*)
(proclaim '(type fixnum *x* *y* *h* *n* *h0*))

(defvar old-x)
(defvar old-y)
(defvar saved-pw)

(defun init-plot (pw)
  (setq saved-pw pw))

(defun set-plot (x y)
  (setq old-x x)
  (setq old-y y))

(defun plot ()
  (pw-vector saved-pw old-x old-y *x* *y* pix-src 1)
  (setq old-x *x*)
  (setq old-y *y*))

(defun sierpinski ()
  (let* ((base (window-create 0 frame 
			      win-columns 80
			      frame-label    "Sierpinski curves"
					   0))
	 (cntl (window-create base panel
			      win-rows 4
					   0))
	 (cnvs (window-create base canvas
			      win-columns 66
			      win-rows 32
					   0)))

    (panel-create-item cntl panel-choice
      panel-item-x         (attr-col 0)
      panel-item-y         (attr-row 0)
      panel-label-string   "number of iterations:"
      panel-choice-strings  "1" "2" "3" "4" "5" "6" 0
      panel-notify-proc   (register-function 'iteration-func 0)
      0)

    
    (panel-create-item cntl panel-choice
      panel-item-x         (attr-col 0)
      panel-item-y         (attr-row 1)
      panel-label-string   "size to draw:"
      panel-choice-strings "32" "64" "128" "256" "512" 0
      panel-notify-proc (register-function 'size-func 1)
      0)

    (setq *start-drawing-item*
    (panel-create-item cntl panel-button
      panel-item-x         (attr-col 0)
      panel-item-y         (attr-row 3)
      panel-label-image    (panel-button-image
			    cntl "start drawing"
			    15 0)
      panel-notify-proc (register-function 'draw-sierpinski-curves-callback)
      0))

    (setq *panel-exit-item*
    (panel-create-item cntl panel-button
      panel-item-x         (attr-col 20)
      panel-item-y         (attr-row 3)
      panel-label-image    (setq *exit-pgm-image* 
			      (panel-button-image
			    cntl "exit demo"
			    15 0))
      panel-notify-proc (register-function 'abort-pgm)
      0))
    
    (setq *exit-draw-image* 
       (panel-button-image
	cntl "stop drawing" 15 0))
    
    (window-fit base)

    (setq *n*  1
	  *h0* 32)

    (setq *sizes* (make-array '(5)
		    :initial-contents '(32 64 128 256 512)))
    (setq *iterations* (make-array '(6)
			 :initial-contents '(1 2 3 4 5 6)))

    (setq *pw* (window-get cnvs canvas-pixwin))
    (init-plot *pw*)

    (window-set base win-show 1 frame-no-confirm 1 0)
    (unwind-protect
     (catch 'exit (notify-start-loop 'handle-select-command))
     (progn (window-destroy base)
	    (notify-dispatch)
	    0))))

(defun handle-select-command (command value)
   (declare (ignore value))
   (case command
      (:draw (draw-the-sierpinski-curve))
      (:exit (throw 'exit t))))

(defun handle-dispatch-command (command value)
   (declare (ignore value))
   (case command
      (:exit (throw 'draw t))))


(defun-c-callable iteration-func (i k)
    (setq *n* (aref *iterations* k))
  0)


(defun-c-callable size-func (i k)
    (setq *h0* (aref *sizes* k))
  0)


(defun-c-callable draw-sierpinski-curves-callback ()
   (setq *notify-command* :draw)
   (notify-stop)
   0)

(defun-c-callable abort-pgm ()
   (setq *notify-command* :exit)
   (notify-stop)
   0)

(defun draw-the-sierpinski-curve ()
   (panel-set *panel-exit-item* panel-label-image *exit-draw-image* 0)
   (panel-set *start-drawing-item* panel-show-item 0 0)
   (catch 'draw (draw-sierpinski-curves))
   (panel-set *panel-exit-item* panel-label-image *exit-pgm-image* 0)
   (panel-set *start-drawing-item* panel-show-item 1 0)
   )

(defun draw-sierpinski-curves ()
  (pw-write saved-pw 0 0 512 512 pix-src null 0 0)
  
  (let ((x0 0)
	(y0 0))
     (declare (type fixnum x0 y0))

    (setq *h*  (/ *h0* 4)
	  x0   (* 2 *h*)
	  y0   (* 3 *h*))

    (do ((i 1 (1+ i)))
        ((or (> i *n*)
	     (< *h* 2)) nil)

      (setq x0  (- x0 *h*)
	    *h* (/ *h* 2)
	    y0  (+ y0 *h*))

      (setq *x* x0
	    *y* y0)
      (set-plot *x* *y*)

      (a i)  (setq *x* (+ *x* *h*))  (setq *y* (- *y* *h*))  (plot)
      (b i)  (setq *x* (- *x* *h*))  (setq *y* (- *y* *h*))  (plot)
      (c i)  (setq *x* (- *x* *h*))  (setq *y* (+ *y* *h*))  (plot)
      (d i)  (setq *x* (+ *x* *h*))  (setq *y* (+ *y* *h*))  (plot)))
  0)

(defun a (i)
   (let ((im1 (1- i)))
      (notify-dispatch-step 'handle-dispatch-command 4)

    (if (> i 0) (progn
	    (a im1)  (setq *x* (+ *x* *h*))  (setq *y* (- *y* *h*))  (plot)
	    (b im1)  (setq *x* (+ *x* (* 2 *h*)))                    (plot)
	    (d im1)  (setq *x* (+ *x* *h*))  (setq *y* (+ *y* *h*))  (plot)
	    (a im1)))))

(defun b (i)
  (let ((im1 (1- i)))

      (notify-dispatch-step 'handle-dispatch-command 4)
    (if (> i 0) (progn
	    (b im1)  (setq *x* (- *x* *h*))  (setq *y* (- *y* *h*))  (plot)
	    (c im1)  (setq *y* (- *y* (* 2 *h*)))                    (plot)
	    (a im1)  (setq *x* (+ *x* *h*))  (setq *y* (- *y* *h*))  (plot)
	    (b im1)))))

(defun c (i)
  (let ((im1 (1- i)))

      (notify-dispatch-step 'handle-dispatch-command 4)
    (if (> i 0) (progn
	    (c im1)  (setq *x* (- *x* *h*))  (setq *y* (+ *y* *h*))  (plot)
	    (d im1)  (setq *x* (- *x* (* 2 *h*)))                    (plot)
	    (b im1)  (setq *x* (- *x* *h*))  (setq *y* (- *y* *h*))  (plot)
	    (c im1)))))

(defun d (i)
  (let ((im1 (1- i)))

      (notify-dispatch-step 'handle-dispatch-command 4)
    (if (> i 0) (progn
	    (d im1)  (setq *x* (+ *x* *h*))  (setq *y* (+ *y* *h*))  (plot)
	    (a im1)  (setq *y* (+ *y* (* 2 *h*)))                    (plot)
	    (c im1)  (setq *x* (- *x* *h*))  (setq *y* (+ *y* *h*))  (plot)
	    (d im1)))))

(format t "type (sierpinski) to start demo~%")


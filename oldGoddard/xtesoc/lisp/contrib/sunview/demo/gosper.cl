;;; Copyright 1986 Sun Microsystems

;;; dragon curves of the type described by bill gosper

;;; $Header: gosper.cl,v 1.6 1993/05/05 04:29:38 layer Exp $

(eval-when (compile load eval) 
   (require :sunview) 
   (require :foreign)
   (require :notify))

(eval-when (compile eval load)
  (intern (symbol-name :gosper-curves) (find-package :user))
  (intern (symbol-name :abort-gosper-curve) (find-package :user)))


(defpackage :gosper
  (:export gosper-curves abort-gosper-curve)
  (:use :lisp :excl :sunview :ff :cltl1)
  (:import-from :user gosper-curves abort-gosper-curve))

(in-package :gosper)

(defvar bw-only nil)
(if* (and (not bw-only) (not (fboundp 'use-rainbow-color-map)))
   then (load "rainbow.o")
	(defforeign-list '((rainbow-setup :entry-point "_rainbow_setup")
			   (getr) (getg) (getb)))
	(defun use-rainbow-color-map (pw)
	  (let ((cms-rainbowsize (rainbow-setup)))
	    (pw-setcmsname pw "rainbow")
	    (pw-putcolormap pw 0 cms-rainbowsize (getr) (getg) (getb)))))

(if* (and (boundp 'bw-only) bw-only)
   then (defun use-rainbow-color-map (x) nil))


(defconstant *pi-div-4* (/ pi 4.0))
(defconstant *sqrt-2* (sqrt 2.0))
(defvar *old-x*)
(defvar *old-y*)
(defvar *canvas*)
(defvar *min-length* 2)
(defvar *curve-type* 'c-curve)
(defvar *alldone* nil)

;; command strategy
;; button: 
;;   start drawing 	- *notify-command* to :draw
;;   stop  drawing	- *notify-command* to :exit
;;   exit  program	- *notify-command* to :exit
;;
;; command	select	     	 dispatch
;; :draw     begin drawing 
;; :exit     throw to exit loop  throw to stop drawing
;;

(defun select-command-handler (command value)
   (case command
      (:draw (draw-the-gosper-curve))
      (:exit (throw 'exit t))))

(defun dispatch-command-handler (command value)
   (case command
      (:exit (throw 'draw t))))


(defun-c-callable curve-select (i k)
  (setq *curve-type*
	(if (zerop k) 'c-curve 'dragon-curve))
  0)

(defun gosper-curves ()
  (let* ((base (window-create 0 frame
		                frame-label "gosper curves"
				win-height 512
				win-width 512
				0))
	 (cntl (window-create base panel
		                   win-rows 5
				   0))
	 (cnvs (window-create base canvas
				     0)))
    (panel-create-item cntl panel-choice
      panel-item-x    (attr-col 0)
      panel-item-y    (attr-row 0)
      panel-label-string "type of curve:"
      panel-choice-strings "c-curve" "dragon-curve" 0
      panel-notify-proc (register-function 'curve-select)
      0)
    (panel-create-item cntl panel-slider
      panel-item-x         (attr-col 0)
      panel-item-y        (attr-row 2)
      panel-label-string   "minimum line length in pixels"
      panel-value          3
      panel-min-value      2
      panel-max-value      8
      panel-show-range     0
      panel-show-value     1
      panel-notify-proc    (register-function 'gosper-set-resolution)
      0)
    (setq *start-drawing-item*
    (panel-create-item cntl panel-button
      panel-item-x         (attr-col 0)
      panel-item-y         (attr-row 4)
      panel-label-image    (panel-button-image
			     cntl "start drawing" 15 0)
      panel-notify-proc    (register-function 'draw-gosper-curve)
      0))
    (setq *panel-exit-item*
       (panel-create-item cntl panel-button
			  panel-label-image 
			  (setq *exit-pgm-image* 
			     (panel-button-image cntl "exit program" 15 0))
			  panel-notify-proc   
			  (register-function 'abort-gosper-demo) 0))
    (setq *exit-draw-image*
       (panel-button-image cntl "stop drawing" 15 0))
    (window-set base frame-no-confirm 1 0)
    (setq *canvas* (get-canvas-pixwin cnvs))
    (use-rainbow-color-map *canvas*)
    (setq *curve-type* 'c-curve)
    (setq *min-length* 3)
    (setq *alldone* nil)
    (window-set base win-show 1 0)
    (unwind-protect
	  (catch 'exit (notify-start-loop 'select-command-handler))
      (progn (window-destroy base)
             (notify-dispatch)
	     0))))

(defun command-loop ()
   (do ()
	 (nil)
      (setq *notify-command* nil)
      (notify-start)
      (case *notify-command*
	 (:exit (return))
	 (:draw (draw-the-gosper-curve)))))
	 
	 
      
(defun-c-callable abort-gosper-demo ()
   (setq *notify-command* :exit)
   (notify-stop)
   0)

			     
					    
(defun gosper-clear-canvas (canvas)
  (pw-write *canvas* 0 0 512 512 pix-src 0 0 0))

(defun-c-callable draw-gosper-curve ()
  (setq *x-old* 140)
  (setq *y-old* 150)
  (gosper-clear-canvas *canvas*)
  (setq *notify-command* :draw)
  (notify-stop)
  0)

(defun draw-the-gosper-curve ()
   (panel-set *panel-exit-item* panel-label-image *exit-draw-image* 0)
   (panel-set *start-drawing-item* panel-show-item 0 0)
  (catch 'draw
     (funcall *curve-type* *canvas*  200 0 1))
   (panel-set *panel-exit-item* panel-label-image *exit-pgm-image* 0)
   (panel-set *start-drawing-item* panel-show-item 1 0)
  )

(defun abort-gosper-curve () (throw 'stop 0))

(defun-c-callable gosper-set-resolution (item v event)
  (setq *min-length* v))
  
(defun gosper-plot-line (canvas length angle)
  (connect-line
    canvas
    (+ *x-old* (* length (cos angle)))
    (+ *y-old* (* length (sin angle)))))

(defun connect-line (canvas x-new y-new)
  (pw-vector canvas (truncate *x-old*)
                    (truncate *y-old*)
		    (truncate x-new)
		    (truncate y-new)
		    pix-src (1+ (random 6)))
  (setq *x-old* x-new
	*y-old* y-new))


(defun dragon-curve (canvas length angle sign)
   (notify-dispatch-step 'dispatch-command-handler 10)
   (let ((length-div-sqrt-2 (/ length *sqrt-2*))
	 (sign-times-pi-div-4 (* sign *pi-div-4*)))
      (cond (*alldone* nil)
	    ((< length *min-length*) (gosper-plot-line canvas length angle))
	    (t (dragon-curve canvas
			     length-div-sqrt-2
			     (+ angle sign-times-pi-div-4)
			     1.0)
	       (dragon-curve canvas
			     length-div-sqrt-2
			     (- angle sign-times-pi-div-4)
			     -1.0)))))

(defun c-curve (canvas length angle sign)
   (notify-dispatch-step 'dispatch-command-handler 10)
   (let ((length-div-sqrt-2 (/ length *sqrt-2*))
	 (sign-times-pi-div-4 (* sign *pi-div-4*)))
      (cond (*alldone* nil)
	    ((< length *min-length*) (gosper-plot-line canvas length angle))
	    (t (c-curve canvas
			length-div-sqrt-2
			(+ angle *pi-div-4*)
			1.0)
	       (c-curve canvas
			length-div-sqrt-2
			(- angle *pi-div-4*)
			-1.0)))))
    
(format t "~%;;; this demo draws curves of the type first decribed by bill gosper~%")
(format t ";;; run gosper-curves to start the demo~%~%")

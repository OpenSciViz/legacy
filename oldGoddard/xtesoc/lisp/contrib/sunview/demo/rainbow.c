/* $Header: rainbow.c,v 1.4 1993/05/05 04:29:44 layer Exp $ */

#include <sunwindow/cms_rainbow.h>

#define MAX_CMAP_LEN	256

unsigned char r [MAX_CMAP_LEN], g[MAX_CMAP_LEN],  b[MAX_CMAP_LEN];

rainbow_setup()
{   
    cms_rainbowsetup (r, g, b);
    return(CMS_RAINBOWSIZE);
    
}

unsigned char *getr()
{
   return(r);
}

unsigned char *getg()
{
   return(g);
}

unsigned char *getb()
{
   return(b);
}


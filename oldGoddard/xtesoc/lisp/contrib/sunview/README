;; $Header: README,v 1.6 1993/05/05 04:29:20 layer Exp $

		   The Allegro CL Sunview Interface
		    for versions SunOS 3.0 to 4.1

Franz Inc. supplies a package designed to allow Common  Lisp to easily
use  Sunview.  This package  works on monochrome  Sun3 and Sun4's with
SunOS 3.0, 3.2, 3.4, 4.0 or 4.1.  Franz Inc.   intends to adapt it for
other releases of the operating system and color, Franz Inc.  does not
supply official support for this package.  Accordingly, all source for
the package is supplied in this directory.  It is  up to you to modify
any missing or additional entry points  for the hardware configuration
and software release you may be running.

Franz Inc. gratefully acknowledges the help of Sun  and the Lisp group
within Sun (particularly Steve Gadol and Ronald Mak) for parts of this
package and  for   the graphics demo  programs   in the "sunview/demo"
sub-directory.   These  demo  programs illustrate use  of Sunview from
within Lisp.

The C source code consists of:

	sunview.c (the sunview and some sunwindow library names)
	sunwin.c  (C sunwindows macros turned into functions)

These C  sources are a list of  many Sunview  functions and macros and
some Sunwindows macros.  There is a library file,

	sunview-lib.a

which contains all the graphics C macros converted to be functions.

The common lisp source code consists of:

	sunview.cl

This defines most  of Sunview graphics functions  and all the graphics
constants to Lisp.

Note that the Lisp analogues to the C  functions or constants have "-"
replacing "_" and are not mixed case.

The  demo programs have only been  tested  with a case-sensitive-lower
common lisp, but should work  OK  with  a  case-insensitive lisp.   To
build a  version of common  lisp with sunview,  change to the  sunview
directory and give the command:

	% make <options>

which  builds a Common  Lisp  binary   with the graphics library  code
linked in and the graphics calls and constants defined  to Lisp.  This
new binary is about  a megabyte larger than the  original binary (on a
Sun3,  more on a  Sun4).  <options> is  used  to set `make' variables.
The items should be put on the `make' command line  (or changed in the
`Makefile' with a text editor) if needed:

  os	this is the SunOS version, which is one of 3_0, 3_2, 3_4, 3_5,
	4_0 or 4_1.

  machine
	the machine type, either SUN3 or SUN4.

For example, on a Sun4 with SunOS 4.1 the make command would be:

	% make os=4_1 machine=SUN4

It should work in a  standard  cl,  but this hasn't  been tested.  The
dumped lisp will have  a binary of about  4.5  megabytes, so make sure
you have enough space.

In the subdirectory  sunview/demo are a series of  examples of how you
might  use the notifier, how  to make windows,  etc.   Note that **the
notifier  is not  re-entrant!**  This   means  that if   Lisp gets  an
interrupt while  in the notifier  and  re-starts the  notifier without
exiting cleanly,  you  will have problems.  (This  is why  none of the
demos use "window-main-loop").

Known Problems:
1. On the Sun4 with OS 3.x, there is a loader bug which does
   not allow branches in C code of more than 8 Megabytes.
   If you hit this problem you should rebuild the lisp from
   the distribution.  
   
   Copy the files sunview-lib.a and sunviewload.o to the
   "build" directory of the distribution.

   Type in the line (all one line):
	cc ucl.o static.o sunviewload.o sunview-lib.a -lsuntool -lsunwindow -lpixrect -o ucl
   Then do a normal install to rebuild the lisp.  Change to the
   directory where this README is and do the "make" to build a
   Sunview lisp.

2. On the Sun4 with OS 4.0, there is another loader bug which
   trashes the symbol table when too many symbols are present
   This bug is worked around by clearing the symbol table of
   the lisp back to its virgin state after foreign loading in
   the Sunview graphics library.  This means that no Sunview
   entry points remain in the dumped out lisp, so if you need
   to load further C code referencing Sunview entry points,
   use the following (before building the Sunview lisp):
   mv sunviewload.o foo.o
   ld -r sunviewload.o foo.o module1.o module2.o
   (where module1.o and module2.o are the extra C code).
   If you also "defforeign" functions from C functions in the
   other modules, you must insert this code prior to the call
   to "ff:reset-entry-point-table" in sunview.cl.

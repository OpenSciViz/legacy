%{
/* $Header: defctype.l,v 1.5 1993/08/19 20:00:49 smh Exp $ */
#include <ctype.h>

%}

N	[0-9]
X	[0-9a-fA-F]
A	[A-Z_a-z]
AN	[0-9A-Z_a-z]

%Start	P S B

%%

^\#(.|(\\\n))*$			{}
\/\*([^*]|(\*[^/]))*\*\/	{}
\"([^"]|(\\\"))*\"		{}
\'([^']|(\\\'))+\'		{}
[\n\t ]				{}

<P,S>struct		{ yylval.dynstr = NULL; return STRUCT; }
<P,S>typedef		{ yylval.dynstr = NULL; return TYPEDEF; }
<P,S>union		{ yylval.dynstr = NULL; return UNION; }

<S>char			{ yylval.dynstr = NULL; return CHAR; }
<S>double		{ yylval.dynstr = NULL; return DOUBLE; }
<S>enum			{ yylval.dynstr = NULL; return ENUM; }
<S>float		{ yylval.dynstr = NULL; return FLOAT; }
<S>int			{ yylval.dynstr = NULL; return INT; }
<S>long			{ yylval.dynstr = NULL; return LONG; }
<S>short		{ yylval.dynstr = NULL; return SHORT; }
<S>unsigned		{ yylval.dynstr = NULL; return UNSIGNED; }
<S>void			{ yylval.dynstr = NULL; return VOID; }
<S>const		{ yylval.dynstr = NULL; return CONST; }
<S>volatile		{ yylval.dynstr = NULL; return VOLATILE; }
<S>static		{ yylval.dynstr = NULL; return STATIC; }
<S>extern		{ yylval.dynstr = NULL; return EXTERN; }

<S>{A}{AN}*		{ struct TypedefName *p;
			  yylval.dynstr = ds(yytext);
			  for (p=TypedefNames; p; p = p->next)
			      if (strcmp(yytext,p->name)==0) return TYPENAME;
			  return NAME; }
<S>0[xX]{X}+		{ yylval.dynstr = cat(ds("#X"),ds(yytext+2),NULL,NULL);
			  return NUMBER; }
<S>0{N}+		{ yylval.dynstr = cat(ds("#O"),ds(yytext+1),NULL,NULL);
			  return NUMBER; }
<S>{N}+			{ yylval.dynstr = ds(yytext) ; return NUMBER; }

<S>[,*]			{ return *yytext; }

[;\(\)\[\]\{\}]		{ return *yytext; }

@			{ extern int yydebug; yydebug = !yydebug; }

.			{ return RANDOM; }

%%

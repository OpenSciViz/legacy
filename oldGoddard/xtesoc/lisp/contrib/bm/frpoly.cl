;; $Header: frpoly.cl,v 1.11 1993/05/05 03:43:00 layer Exp $

;;;BEGIN
;;;FRPOLY
;;; Franz Lisp benchmark from Fateman
;; test from Berkeley based on polynomial arithmetic.

(declaim (special **ans** **coef** **f** **inc** **i** **k** **qq** **ss**
		  **v** ***x*** ***alpha** ***a*** ***b*** ***chk** ***l**
		  ***p** **q*** **u*** ***var** ***y***
		  frpoly-r frpoly-r2 frpoly-r3 **start** **res1** **res2**
		  **res3**))
;(declare (localf pcoefadd pcplus pcplus1 pplus ptimes ptimes1
;                ptimes2 ptimes3 psimp pctimes pctimes1
;                pplus1))
;; Franz uses maclisp hackery here; you can rewrite lots of ways.
(defmacro pointergp (x y) `(> (get ,x 'order)(get ,y 'order)))

(defmacro pcoefp (e) `(atom ,e))
(defmacro pzerop (x) 
  (let ( (var (gensym)) )
    `(let ((,var ,x))
       (if (numberp ,var) (zerop ,var)))));true for 0 or 0.0
(defmacro pzero () 0)
(defmacro cplus (x y) `(+ ,x ,y))
(defmacro ctimes (x y) `(* ,x ,y))


(defun pcoefadd (e c x) (cond ((pzerop c) x)
                              (t (cons e (cons c x)))))

(defun pcplus (c p) (cond ((pcoefp p) (cplus p c))
                          (t (psimp (car p) (pcplus1 c (cdr p))))))

(defun pcplus1 (c x)
       (cond ((null x)
              (cond ((pzerop c) nil) (t (cons 0 (cons c nil)))))
             ((pzerop (car x)) (pcoefadd 0 (pplus c (cadr x)) nil))
             (t (cons (car x) (cons (cadr x) (pcplus1 c (cddr x)))))))
         
(defun pctimes (c p) (cond ((pcoefp p) (ctimes c p))
                           (t (psimp (car p) (pctimes1 c (cdr p))))))

(defun pctimes1 (c x)
       (cond ((null x) nil)
             (t (pcoefadd (car x)
                          (ptimes c (cadr x))
                          (pctimes1 c (cddr x))))))

(defun pplus (x y) (cond ((pcoefp x) (pcplus x y))
                         ((pcoefp y) (pcplus y x))
                         ((eq (car x) (car y))
                          (psimp (car x) (pplus1 (cdr y) (cdr x))))
                         ((pointergp (car x) (car y))
                          (psimp (car x) (pcplus1 y (cdr x))))
                         (t (psimp (car y) (pcplus1 x (cdr y))))))

(defun pplus1 (x y)
       (cond ((null x) y)
             ((null y) x)
             ((= (the fixnum (car x)) (the fixnum (car y)))
              (pcoefadd (car x)
                        (pplus (cadr x) (cadr y))
                        (pplus1 (cddr x) (cddr y))))
             ((> (the fixnum (car x)) (the fixnum (car y)))
              (cons (car x) (cons (cadr x) (pplus1 (cddr x) y))))
             (t (cons (car y) (cons (cadr y) (pplus1 x (cddr y)))))))

(defun psimp (var x)
       (cond ((null x) 0)
             ((atom x) x)
             ((zerop (car x)) (cadr x))
              (t (cons var x))))

(defun ptimes (x y) (cond ((or (pzerop x) (pzerop y)) (pzero))
                          ((pcoefp x) (pctimes x y))
                          ((pcoefp y) (pctimes y x))
                          ((eq (car x) (car y))
                           (psimp (car x) (ptimes1 (cdr x) (cdr y))))
                          ((pointergp (car x) (car y))
                           (psimp (car x) (pctimes1 y (cdr x))))
                          (t (psimp (car y) (pctimes1 x (cdr y))))))

(defun ptimes1 (***x*** y) (prog (**u*** **v**)
                               (setq **v** (setq **u*** (ptimes2 y)))
                          a    (setq ***x*** (cddr ***x***))
                               (cond ((null ***x***) (return **u***)))
                               (ptimes3 y)
                               (go a)))

(defun ptimes2 (y) (cond ((null y) nil)
                         (t (pcoefadd (+ (car ***x***) (car y))
                                      (ptimes (cadr ***x***) (cadr y))
                                      (ptimes2 (cddr y))))))

(defun ptimes3 (y) 
  (prog (e u c) 
     a1 (cond ((null y) (return nil)))
        (setq e (+ (car ***x***) (car y)))
        (setq c (ptimes (cadr y) (cadr ***x***) ))
        (cond ((pzerop c) (setq y (cddr y)) (go a1))
              ((or (null **v**) (> e (car **v**)))
               (setq **u*** (setq **v** (pplus1 **u*** (list e c))))
               (setq y (cddr y)) (go a1))
              ((= e (car **v**))
               (setq c (pplus c (cadr **v**)))
               (cond ((pzerop c) 
		      (setq **u*** 
			    (setq **v** 
				  (pdiffer1 **u***
					    (list (car **v**) (cadr **v**))))))
                     (t (rplaca (cdr **v**) c)))
               (setq y (cddr y))
               (go a1)))
     a  (cond ((and (cddr **v**) 
		    (> (caddr **v**) e))
	       (setq **v** (cddr **v**)) (go a)))
        (setq u (cdr **v**))
     b  (cond ((or (null (cdr u)) (< (cadr u) e))
               (rplacd u (cons e (cons c (cdr u)))) (go e)))
        (cond ((pzerop (setq c (pplus (caddr u) c))) 
					(rplacd u (cdddr u)) (go d))
              (t (rplaca (cddr u) c)))
     e  (setq u (cddr u))
     d  (setq y (cddr y))
        (cond ((null y) (return nil)))
        (setq e (+ (car ***x***) (car y)))
        (setq c (ptimes (cadr y) (cadr ***x***)))
     c  (cond ((and (cdr u) (> (cadr u) e)) (setq u (cddr u)) (go c)))
        (go b))) 

(defun pexptsq (p n)
  (declare (fixnum n))
  (do ((n (floor n 2) (floor n 2))
       (s (cond ((oddp n) p) (t 1))))
      ;;((zerop n) s)
      ((zerop n) nil) ;;The results make a mess when printed!
    (declare (fixnum n))
    (setq p (ptimes p p))
    (and (oddp n) (setq s (ptimes s p))) ))

(eval-when (load eval)
  (setf (get 'x 'order) 1)
  (setf (get 'y 'order) 2)
  (setf (get 'z 'order) 3)
  (setq frpoly-r (pplus '(x 1 1 0 1) (pplus '(y 1 1) '(z 1 1)))
	;; frpoly-r= x+y+z+1
	frpoly-r2 (ptimes frpoly-r 100000)
	;; frpoly-r2 = 100000*r
	frpoly-r3 (ptimes frpoly-r 1.0)
	;; frpoly-r3 = frpoly-r with floating point coefficients
	))

(defun standard-frpoly-r2-10 ()
  (pexptsq frpoly-r2 10)
  nil)
  
(defun standard-frpoly-r3-10 ()
  (pexptsq frpoly-r3 10)
  nil)

(defun standard-frpoly-r-15 ()
  (pexptsq frpoly-r 15)
  nil)

(defun standard-frpoly-r3-15 ()
  (pexptsq frpoly-r3 15)
  nil)

(defun standard-frpoly-r2-15 ()
  (pexptsq frpoly-r2 15)
  nil)

(defun testfrpoly-r2-10 () (time (standard-frpoly-r2-10)))
(defun testfrpoly-r3-10 () (time (standard-frpoly-r3-10)))
(defun testfrpoly-r-15 () (time (standard-frpoly-r-15)))
(defun testfrpoly-r3-15 () (time (standard-frpoly-r3-15)))
(defun testfrpoly-r2-15 () (time (standard-frpoly-r2-15)))

;; $Header: control.cl,v 1.13 1993/05/05 03:42:43 layer Exp $
;;
;; benchmark control

(eval-when (compile) (proclaim '(optimize (speed 3))))

(if* (null clos::*use-fast-gf*)
   then (load "../src/clos/preload.cl")
	(setq clos::*use-fast-gf* t))

(defparameter *bm-number-of-trials* 3)

(defstruct test
  file 					;the filename
  loaded 				;`t' it file already loaded
  name 					;the benchmark name
  times 				;a list of `time' structures
  )

(defstruct tyme
  user
  system
  gc-user
  gc-system
  total-user
  total-system
  real
  )

(defvar *benches*
  (list
   (make-test :file "boyer" :name 'boyer)
   (make-test :file "browse" :name 'browse)
   (make-test :file "dderiv" :name 'dderiv)
   (make-test :file "deriv" :name 'deriv)
   (make-test :file "destru" :name 'destru)
   (make-test :file "traverse" :name 'traverse-init)
   (make-test :file "traverse" :name 'traverse-run :loaded t)
   (make-test :file "div2" :name 'div2-iter)
   (make-test :file "div2" :name 'div2-recur :loaded t)
   (make-test :file "triang" :name 'triang)
   (make-test :file "tak" :name 'tak)
   (make-test :file "takl" :name 'takl)
   (make-test :file "takr" :name 'takr)
   (make-test :file "ctak" :name 'ctak)
   (make-test :file "stak" :name 'stak)
   (make-test :file "fft" :name 'fft)
   (make-test :file "frpoly" :name 'frpoly-r2-10)
   (make-test :file "frpoly" :name 'frpoly-r3-10 :loaded t)
   (make-test :file "frpoly" :name 'frpoly-r-15  :loaded t)
   (make-test :file "frpoly" :name 'frpoly-r3-15 :loaded t)
   (make-test :file "frpoly" :name 'frpoly-r2-15 :loaded t)
   (make-test :file "puzzle" :name 'puzzle)
   (make-test :file "fprint" :name 'fprint)
   (make-test :file "fread" :name 'fread)
   (make-test :file "tprint" :name 'tprint)))

(defvar .bm-time. nil)

(defmacro bm-time (form)
  `(let ((.oldrealtime (excl::get-internal-real-time))
	 .newrealtime)
     (multiple-value-bind (.olduser .oldsystem .oldgcu .oldgcs)
	 (excl::get-internal-run-times)
       ,form
       (setq .newrealtime (excl::get-internal-real-time))
       (multiple-value-bind (.newuser .newsystem .newgcu .newgcs)
	   (excl::get-internal-run-times)
	 (setq .bm-time.
	   (make-tyme
	    :user (/ (- (- .newuser .olduser) (- .newgcu .oldgcu)) 1000.0)
	    :system (/ (- (- .newsystem .oldsystem) (- .newgcs .oldgcs))
		       1000.0)
	    :gc-user (/ (- .newgcu .oldgcu) 1000.0)
	    :gc-system (/ (- .newgcs .oldgcs) 1000.0)
	    :total-user (/ (- .newuser .olduser) 1000.0)
	    :total-system (/ (- .newsystem .oldsystem) 1000.0)
	    :real (/ (- .newrealtime .oldrealtime) 1000.0)))
	 (format
	  *trace-output*
	  "~&~
;; user ~,3f, sys ~,3f, gc ~,3f, gc sys ~,3f~%~
;;   total ~,3f, total sys ~,3f, real ~,3f~%"
	  (tyme-user .bm-time.)
	  (tyme-system .bm-time.)
	  (tyme-gc-user .bm-time.)
	  (tyme-gc-system .bm-time.)
	  (tyme-total-user .bm-time.)
	  (tyme-total-system .bm-time.)
	  (tyme-real .bm-time.))))))

(defun compile-all-bms (&key (speed 3) (safety 0) (debug 0))
  (excl:without-package-locks
   (let ((old-time (macro-function 'time))
	 (comp::.speed. speed)
	 (comp::.safety. safety)
	 (comp::.debug. debug))
     (setf (macro-function 'time) (macro-function 'bm-time))
     (format t "~&;;benchmark compilation:~%")
     (dotimes (x 3) (gc :tenure))
     (db::save-call-arguments nil t t)
     (bm-time
      (dolist (bench *benches*)
	(if (not (test-loaded bench))
	    (compile-file
	     (merge-pathnames (test-file bench) (make-pathname :type "cl"))))))
     (setf (macro-function 'time) old-time)))
  nil)

(defun run-all-bms (&optional (result-file "results.out"))
  (when (probe-file "/tmp/fprint.tst")
    (delete-file "/tmp/fprint.tst"))
  (db::save-call-arguments nil t t)
  (dolist (bm *benches*) (run-bench bm))
  (with-open-file (stream result-file :direction :output :if-exists :supersede)
    (print-bm-times stream))
  (when (probe-file "/tmp/fprint.tst")
    (delete-file "/tmp/fprint.tst")))

(defun run-bench (bench)
  (if (not (test-loaded bench)) (load (test-file bench)))
  (format t "~&;;~a benchmark:~%" (test-name bench))
  (dotimes (n *bm-number-of-trials*)
    (dotimes (x 3) (gc :tenure))
    (funcall (find-symbol (format nil "~a~a" 'test (test-name bench))))
    (push .bm-time.(test-times bench)))
  (force-output t))

(defun print-bm-times (stream)
  (format stream "~23a~18a~18a~18a~%" "" "Non-GC" "GC" "Total")
  (format stream "~14a~9@a~9@a~9@a~9@a~9@a~9@a~9@a~%"
	  "Name" "User" "Sys" "User" "Sys" "User" "Sys" "Real")
  (dolist (bench *benches*)
    (setf (test-times bench)
      (sort (test-times bench) #'< :key #'tyme-user))
    (let ((btime (car (test-times bench))))
      (format stream "~14a~9,3f~9,3f~9,3f~9,3f~9,3f~9,3f~9,3f~%"
	      (test-name bench)
	      (tyme-user btime) (tyme-system btime) (tyme-gc-user btime)
	      (tyme-gc-system btime) (tyme-total-user btime)
	      (tyme-total-system btime) (tyme-real btime))))
  (force-output stream))

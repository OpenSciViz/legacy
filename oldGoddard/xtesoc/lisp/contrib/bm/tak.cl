;; $Header: tak.cl,v 1.7 1993/05/05 03:43:12 layer Exp $

(defun tak (x y z)
  (declare (fixnum x y z))
  (cond ((not (< y x)) z)
	(t
	 (tak
	   (tak (the fixnum (1- x)) y z)
	   (tak (the fixnum (1- y)) z x)
	   (tak (the fixnum (1- z)) x y)))))

(defun testtak ()
  (print (time
	  (tak 18 12 6)
	  #+ignore
	   (progn (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)
		  (tak 18 12 6)))))

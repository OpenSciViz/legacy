/*
 *
 * This file is provided for the public domain AS IS without any
 * warranties or guarantees whatsoever.  It is provided as part of a workaround
 * to problems that occur with Allegro CL on machines running Yellow Pages or
 * NIS database systems.  It has been tested on SunOS 4.0.3, SunOS 4.1 and
 * IBM RS6000.
 * The routines in this file may need to be modified to run on other platforms
 * and/or operating systems.  Questions and/or comments about the contents
 * of this file can be electronically sent "bugs@franz.com" or phoned in to
 * Franz Inc., (510) 548-3600.
 *
 * This file consists of two parts.  One is a set of redefinitions of routines
 * that can invoke Yellow Pages.  The other is an executable program invoked
 * by the previously mentioned redefinitions.
 *
 * To get the routines file nihroutines.o (which can be linked with ucl.o
 * to create a new ucl.o), use
 *
 *  /bin/cc -DNIH_ROUTINES -c -o nihroutines.o cl_nih.c
 *
 * To get the program, use
 *
 *  /bin/cc -DNIHPROG -o cl_nih cl_nih.c
 *
 * This file also contains a function c_set_nihprog() which is called with
 * a directory namestring specifying the location of the executable.
 * If you want to use this file to create a nihroutines.o that resolves
 * c_set_nihprog(), but does not redefine the YP routines, use
 *
 *  /bin/cc -DNIH_ROUTINES -DUse_System_Routines -c -o nihroutines.o cl_nih.c
 *
 * $Header: cl_nih.c,v 1.29.2.1 1994/02/10 07:22:38 georgej Exp $
 */

#include <stdio.h>
#include <signal.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef SYSTYPE_SVR4
#ifndef SVR4
#define SVR4
#endif
#endif

#ifdef SVR4
# define index strchr
# define rindex strrchr
# define signal cl_nih_signal
#endif

#define SNARF_CLNIH

/*
 * If there are any changes to this file, the version number
 * should be changed.
 */
#define VERSION 2003

/* 
 * Codes for what the cl_nih program knows how to process.
 */
#define GETPWNAM 1
#define GETPWUID 2
#define GETHOSTBYNAME 3
#define GETSERVBYNAME 4
#define GETHOSTBYADDR 5
#define GETHOSTENT 6
/* sethostent and endhostent are supported by bookkeeping in the client */
   

#define NIH_EMPTY_ORDER "NIH_EMPTY"

#ifdef mips
#define vfork fork
#endif

/*
 * If all that's desired from this file is the c_set_nihprog entry
 * point (all that is required if regular system calls are to be used
 * by lisp), then compile this file with -DUse_System_Routines.
 */
#ifdef Use_System_Routines

#ifdef NIH_ROUTINES
c_set_nihprog() {}
#endif

#else

#ifdef NIH_ROUTINES

#if defined(ultrix) && defined(mips)
/*
 * Because earlier versions of ultrix did not supply `-G 0' libraries, ucl
 * was forced to include all C library routines into the lisp.  This
 * causes clashes with routines in the same C library module as those
 * being defined here.  Thus, we stub out the remaining C library module
 * routines.  Of course, if they need to be defined using the cl_nih program,
 * they can be done so in this file.
 */

/* From gethostent.o */     
endhostent() {}
struct hostent *gethostent() {}
int *getsvcorder() {}
hostalias() {}
sethostent() {}
strfind() {}
     
local_hostname_length() {}
dn_find() {}

/* From getpwent.o */
endpwent() {}
struct passwd *getpwent() {}
setpwent() {}

/* From getservent.o */
endservent() {}
struct servent *getservbyport() {}
struct servent *getservent() {}
setservent() {}

#endif /* defined(ultrix) && defined(mips) */

char nihprog[256];
#define NIH_PROG_NAME "cl_nih"

#ifdef SNARF_CLNIH
char *nihcode = (char *) 0;	/* 0 means cl_nih is not in lisp */
long snihcode = 0;		/*
				 * >0 means the value is the code size.
				 * 0 means we've never tried reading in
				 * the cl_nih binary.
				 * -1 means we tried and failed and aren't
				 * going to try again.
				 */
#endif
  
/*
 * Called by lisp to tell nihroutines.o routines where to look for the
 * cl_nih program.
 */
c_set_nihprog(dir_namestring)
char *dir_namestring;
{
    strcpy(nihprog, dir_namestring);
    if (nihprog[strlen(nihprog) - 1] != '/') {
	strcat(nihprog, "/");
    }
    strcat(nihprog, NIH_PROG_NAME);
#ifdef SNARF_CLNIH
    if (!snihcode) {
	snarf_cl_nih_prog();
    }
#endif
}

#ifdef SNARF_CLNIH
int
snarf_cl_nih_prog()
{
    struct stat sbuf;
    int fd;

    if (stat(nihprog, &sbuf) < 0) {
	fprintf(stderr, "Warning: %s not found, will not copy into lisp\n",
		nihprog);
	fflush(stderr);
	snihcode = -1;
	return -1;
    }
   
    if ((fd = open(nihprog, O_RDONLY)) < 0) {
	fprintf(stderr,
		"Warning: could not open %s, will not copy into lisp\n",
		nihprog);
	fflush(stderr);
	snihcode = -1;
	return -1;
    }

    if ((nihcode = (char *)malloc(sbuf.st_size)) == NULL) {
	fprintf(stderr, "Warning: Could not malloc space for nih program\n");
	fflush(stderr);
	close(fd);
	snihcode = -1;
	return -1;
    }

    if (read(fd, nihcode, sbuf.st_size) != sbuf.st_size) {
	fprintf(stderr, "Warning: Could not read nih program into lisp\n");
	close(fd);
	snihcode = -1;
	return -1;
    }

    close(fd);
    snihcode = sbuf.st_size;
    return snihcode;
}
#endif /* SNARF_CLNIH */

#ifdef SVR4
cl_nih_signal(sig, disp)
    int sig;
    void (*disp)();
{
    struct sigaction sa, osa;
    sa.sa_handler = disp;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;
    sigaction(sig, &sa, &osa);
    return ((int)osa.sa_handler);
}
#endif

/*
 * nihopen searches for the cl_nih program and issues warnings if it doesn't
 * find the one it expects.
 */

int curpid = 0;
char cmdbuf[BUFSIZ];
char ofile[BUFSIZ];
char bfile[100];

FILE *
nihopen(nih_command)
char *nih_command;
{
    struct stat sbuf;
    char *use_nih_prog = nihprog;
    char command[BUFSIZ];
    char *find_pname();
    int pid, version, newproc, shell_failure;
    FILE *retval;
    void (*ovtalrm)();

    newproc = ((pid = getpid()) != curpid);
    curpid = pid;

    if (stat(use_nih_prog, &sbuf) < 0) {
#ifdef warn_if_not_in_library
	if (newproc) {
	    fprintf(stderr,
		    "Warning: %s not found, will search $PATH\n", nihprog);
	    fflush(stderr);
	}
#endif
#ifdef SNARF_CLNIH
	/* Try writing out the in-core copy */
	if (snihcode > 0) {
	    int fd;
	    
	    sprintf(bfile, "/tmp/NIH_BIN%d", pid);
	    if ((fd = open(bfile, O_WRONLY | O_CREAT, 0777)) < 0) {
		fprintf(stderr, "Can't open %s.  Giving up...\n", bfile);
		fflush(stderr);
		return (FILE *) NULL;
	    }
	    chmod(bfile, 0755);	/* ensure executable */
	    if (write(fd, nihcode, snihcode) != snihcode) {
		fprintf(stderr, "Failure to write %s.  Giving up...\n", bfile);
		fflush(stderr);
		close(fd);
		unlink(bfile);
		return (FILE *) NULL;
	    }
	    close(fd);
	    use_nih_prog = bfile;
	}
#else
	use_nih_prog = find_pname(NIH_PROG_NAME);
#endif /* SNARF_CLNIH */
	if (!use_nih_prog) {
	    fprintf(stderr,
		    "Warning: %s not found in either library or $PATH.  Giving up...\n",
		    NIH_PROG_NAME);
	    fflush(stderr);
	    return (FILE *) NULL;
	}
    }

    /*
     * Check for executableness.  If we called find_pname, then use_nih_prog
     * is already known to be executable.
     */
    if ((use_nih_prog == nihprog) && !(sbuf.st_mode & 0111)) {
	fprintf(stderr, "%s is not executable.  Giving up...\n",
		use_nih_prog);
	fflush(stderr);
	return (FILE *) NULL;
    }
       
    if (newproc) {
	sprintf(ofile, "/tmp/NIH%d", pid);
    }

    sprintf(command, "%s %s %s", use_nih_prog, ofile, nih_command);

    /*
     * Profiler workaround:  Ignore sigvtalrm around call to sy_shell_from_c
     */
    ovtalrm = (void (*)())signal(SIGVTALRM, SIG_IGN);
    shell_failure = (nih_spawn(command) < 0);
    signal(SIGVTALRM, ovtalrm);
    
    if (shell_failure) {
	fprintf(stderr, "Failed to execute NIH command: %s\n",
		command);
	fflush(stderr);
	return (FILE *) NULL;
    }
    if ((retval = fopen(ofile, "r")) == NULL) {
	fprintf(stderr, "Failed to open NIH command's output file: %s\n",
		ofile);
	return retval;
    }
    unlink(ofile);
#ifdef SNARF_CLNIH
    if (use_nih_prog == bfile) {
	unlink(bfile);
    }
#endif /* SNARF_CLNIH */

    setbuf(retval, cmdbuf);	/* don't let fscanf call malloc */
    fscanf(retval, "%d|", &version);
    if (version != VERSION) {
	if (newproc) {
	    fprintf(stderr,
		    "Warning: %s's version number does not match %d != %d\n",
		    use_nih_prog, version, VERSION);
	    fflush(stderr);
	}
    }
    return retval;
}

#define MAXARGS 100
#define SPACE ' '

nih_spawn(command)
     char *command;
{
    char *args[MAXARGS], *p = command, *index();
    short i;
    int child, status, res;
    void (*ovtalrm)() = (void (*)())signal(SIGVTALRM, SIG_IGN);
    void (*oalrm)() = (void (*)())signal(SIGALRM, SIG_IGN);
	
    
    /*
     * Split the arguments into the argument vector.
     * Note: This assumes arguments don't have embedded spaces.
     */
    args[0] = p;
    for (i = 1; p != NULL; i) {
	p = index(p, SPACE);
	if (p != NULL) {
	    *p = (char)NULL;
	    args[i++] = ++p;
	}
    }
    args[i] = 0;

    if ((child = vfork()) == 0) {
	execv(args[0], args);
	_exit(1);
    }

    if (child < 0) {
	/* error in vfork */
	return(-1);
    }

    while (1) {
#ifdef HasWaitpid
	/* Currently this code is disabled unless a #define HasWaitpid is
	 * installed earlier in this file.
	 */
	res = waitpid(child, &status, 0);
#else
	res = wait(&status);
#endif /* HasWaitpid */
	if (res == child) {
	    break;
	} else if (res == -1) {
	    status = -1;
	}
    }
    signal(SIGVTALRM, ovtalrm);
    signal(SIGALRM, oalrm);
    return(status >> 8);
}

/* Search down path for executable prog */
char *
find_pname(prog)
char *prog;
{
    char myname[BUFSIZ];
    char *cp = (char *)getenv("PATH");
    char *cp2;

    if (cp == 0) {
	cp = ":/bin:/usr/bin";
    }

    if (*cp == ':') {
	if (oktox(prog)) {
	    return prog;
	}
    }
    
    while (*cp) {
	for (cp2 = myname;
	     (*cp != '\0') && (*cp != ':');
	     ) {
	    *cp2++ = *cp++;
	}
	*cp2++ = '/';
	strcpy(cp2, prog);
	if (*cp) {
	    cp++;		/* skip colon */
	}
	if (oktox(myname)) {
	    return myname;
	}
    }
    return (char *)0;		/* can't find program2 */
}

/*
 ------------------ The redefined routines for nihroutines.o --------
 */
  

#define RFIELD(ptr) fscanf(p, "%d|", &length); \
  if (p) { \
    (ptr) = (char *)calloc(length + 1, sizeof(char)); \
    fread((ptr), length, 1, p); \
  } else { \
    (ptr) = NULL; \
  }

#define RNUM(var) fscanf(p, "%d|", &length); (var) = length

struct passwd *
getpwnam(name)
#if defined(NeXT) || defined(SVR4)
    const
#endif
char *name;
{
    char command[BUFSIZ];
    struct passwd *retval;
    FILE *p;
    int length;

    sprintf(command, "%d %s", GETPWNAM, name);
    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct passwd *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct passwd *) NULL;
    }
    retval = (struct passwd *)malloc(sizeof(struct passwd));

    /* Common fields */
    
    RFIELD(retval->pw_name);
    RFIELD(retval->pw_passwd);
    RNUM(retval->pw_uid);
    RNUM(retval->pw_gid);
    RFIELD(retval->pw_gecos);
    RFIELD(retval->pw_dir);
    RFIELD(retval->pw_shell);
    
    /* Machine specific fields */

#if defined(sun) || defined(SVR4)
    RFIELD(retval->pw_comment);
#if defined(__pwd_h) || defined(SVR4) /* flag for 4.1 passwd structure */
    RFIELD(retval->pw_age);
#else
    RNUM(retval->pw_quota);
#endif /* __pwd_h */
#endif /* sun */

#ifdef ultrix
    RFIELD(retval->pw_comment);
#ifdef SYSTEM_FIVE
    RFIELD(retval->pw_age);
#else
    RNUM(retval->pw_quota);
#endif /* system_five */
#endif /* ultrix */

#ifdef hpux
    RFIELD(retval->pw_comment);
    RFIELD(retval->pw_age);
    RNUM(retval->pw_audid);
    RNUM(retval->pw_audflg);
#endif /* hpux */

#if defined(sgi) && !defined(SVR4)
    RFIELD(retval->pw_age);
    RFIELD(retval->pw_comment);
    RNUM(retval->pw_origin);
    RFIELD(retval->pw_yp_passwd);
    RFIELD(retval->pw_yp_gecos);
    RFIELD(retval->pw_yp_dir);
    RFIELD(retval->pw_yp_shell);
    RFIELD(retval->pw_yp_netgroup);
#endif /* sgi */

    fclose(p);
    return retval;
}

struct passwd *
getpwuid(uid)
#ifdef __GNUC__
    uid_t
#else
    int
#endif
	uid;
{
    char command[BUFSIZ];
    struct passwd *retval;
    FILE *p;
    int length;

    sprintf(command, "%d %d", GETPWUID, uid);
    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct passwd *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct passwd *) NULL;
    }
    retval = (struct passwd *)malloc(sizeof(struct passwd));

    /* Common fields */
    
    RFIELD(retval->pw_name);
    RFIELD(retval->pw_passwd);
    RNUM(retval->pw_uid);
    RNUM(retval->pw_gid);
    RFIELD(retval->pw_gecos);
    RFIELD(retval->pw_dir);
    RFIELD(retval->pw_shell);
    
    /* Machine specific fields */

#if defined(sun) || defined(SVR4)
    RFIELD(retval->pw_comment);
#if defined(__pwd_h) || defined(SVR4) /* flag for 4.1 passwd structure */
    RFIELD(retval->pw_age);
#else
    RNUM(retval->pw_quota);
#endif /* __pwd_h */
#endif /* sun */

#ifdef ultrix
    RFIELD(retval->pw_comment);
#ifdef SYSTEM_FIVE
    RFIELD(retval->pw_age);
#else
    RNUM(retval->pw_quota);
#endif /* system_five */
#endif /* ultrix */

#ifdef hpux
    RFIELD(retval->pw_comment);
    RFIELD(retval->pw_age);
    RNUM(retval->pw_audid);
    RNUM(retval->pw_audflg);
#endif /* hpux */

#if defined(sgi) && !defined(SVR4)
    RFIELD(retval->pw_age);
    RFIELD(retval->pw_comment);
    RNUM(retval->pw_origin);
    RFIELD(retval->pw_yp_passwd);
    RFIELD(retval->pw_yp_gecos);
    RFIELD(retval->pw_yp_dir);
    RFIELD(retval->pw_yp_shell);
    RFIELD(retval->pw_yp_netgroup);
#endif /* sgi */

    fclose(p);
    return retval;
}

char * nih_horder = (char *)-1; /* -1 means sethostresorder not called */

int
sethostresorder(order)
     char *order;
{
    nih_horder = order;
    return 0;			/* doc sez return 0 if order was changed,
				 * -1 otherwise
				 */
}

int
_setresordsubr(order)		/* Hopefully, this will properly deal with
				 * internal library calls.
				 */
     char *order;
{
    return sethostresorder(order);
}
    

struct hostent *
gethostbyname(name)
#ifdef SVR4
    const
#endif
char *name;
{
    char command[BUFSIZ];
    struct hostent *retval;
    FILE *p;
    int length, arraysize;
    register int i;

    sprintf(command, "%d %s \"%s\"", GETHOSTBYNAME, name,
	    nih_horder == (char *)-1 ? NIH_EMPTY_ORDER : nih_horder);

    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    retval = (struct hostent *)malloc(sizeof(struct hostent));

    RFIELD(retval->h_name);
    fscanf(p, "%d|", &arraysize); /* number of aliases */
    retval->h_aliases = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	RFIELD(retval->h_aliases[i]);
    }
    retval->h_aliases[i] = (char *)NULL;
    RNUM(retval->h_addrtype);
    RNUM(retval->h_length);
#ifdef h_addr			/* check for h_addr_list  */
    fscanf(p, "%d|", &arraysize); /* number of addresses */
    retval->h_addr_list = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	retval->h_addr_list[i] = (char *)malloc(retval->h_length);
	fread(retval->h_addr_list[i], retval->h_length, 1, p);
    }
    retval->h_addr_list[i] = (char *)NULL;
#else
    retval->h_addr = (char *)malloc(retval->h_length);
    fread(retval->h_addr, retval->h_length, 1, p);
#endif /* h_addr */

    fclose(p);
    return retval;
}

int hostent_counter = 0;

#ifdef mips
void
#endif
sethostent(stayopen)
{
    hostent_counter = 0;
}

#ifdef mips
void
#endif
endhostent()
{
    hostent_counter = 0;
}

struct hostent *
gethostent()
{
    char command[BUFSIZ];
    struct hostent *retval;
    FILE *p;
    int length, arraysize;
    register int i;

    sprintf(command, "%d %d \"%s\"", GETHOSTENT, hostent_counter++,
	    nih_horder == (char *)-1 ? NIH_EMPTY_ORDER : nih_horder);

    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    retval = (struct hostent *)malloc(sizeof(struct hostent));

    RFIELD(retval->h_name);
    fscanf(p, "%d|", &arraysize); /* number of aliases */
    retval->h_aliases = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	RFIELD(retval->h_aliases[i]);
    }
    retval->h_aliases[i] = (char *)NULL;
    RNUM(retval->h_addrtype);
    RNUM(retval->h_length);
#ifdef h_addr			/* check for h_addr_list  */
    fscanf(p, "%d|", &arraysize); /* number of addresses */
    retval->h_addr_list = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	retval->h_addr_list[i] = (char *)malloc(retval->h_length);
	fread(retval->h_addr_list[i], retval->h_length, 1, p);
    }
    retval->h_addr_list[i] = (char *)NULL;
#else
    retval->h_addr = (char *)malloc(retval->h_length);
    fread(retval->h_addr, retval->h_length, 1, p);
#endif /* h_addr */

    fclose(p);
    return retval;
}

struct hostent *
gethostbyaddr(addr, len, type)
#ifdef SVR4
    const
#endif
char *addr;
int len, type;
{
    char command[BUFSIZ];
    struct hostent *retval;
    FILE *p;
    int length, arraysize;
    register int i;

    sprintf(command, "%d %u %d %d \"%s\"", GETHOSTBYADDR,
	    *(unsigned long *)addr, len, type,
	    nih_horder == (char *)-1 ? NIH_EMPTY_ORDER : nih_horder);

    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct hostent *) NULL;
    }
    retval = (struct hostent *)malloc(sizeof(struct hostent));

    RFIELD(retval->h_name);
    fscanf(p, "%d|", &arraysize); /* number of aliases */
    retval->h_aliases = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	RFIELD(retval->h_aliases[i]);
    }
    retval->h_aliases[i] = (char *)NULL;
    RNUM(retval->h_addrtype);
    RNUM(retval->h_length);
#ifdef h_addr			/* check for h_addr_list  */
    fscanf(p, "%d|", &arraysize); /* number of addresses */
    retval->h_addr_list = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	retval->h_addr_list[i] = (char *)malloc(retval->h_length);
	fread(retval->h_addr_list[i], retval->h_length, 1, p);
    }
    retval->h_addr_list[i] = (char *)NULL;
#else
    retval->h_addr = (char *)malloc(retval->h_length);
    fread(retval->h_addr, retval->h_length, 1, p);
#endif /* h_addr */

    fclose(p);
    return retval;
}

struct servent *
getservbyname(name, proto)
#ifdef SVR4
#ifndef netdb_bug
    const
#endif
#endif
char *name, *proto;
{
    char command[BUFSIZ];
    struct servent *retval;
    FILE *p;
    int length, arraysize;
    register int i;

    sprintf(command, "%d %s %s", GETSERVBYNAME, name, proto);
    if (NULL == (p = nihopen(command))) {
	fclose(p);
	return (struct servent *) NULL;
    }
    fscanf(p, "%d|", &length);
    if (!length) {
	fclose(p);
	return (struct servent *) NULL;
    }
    retval = (struct servent *)malloc(sizeof(struct servent));

    RFIELD(retval->s_name);
    fscanf(p, "%d|", &arraysize); /* number of aliases */
    retval->s_aliases = (char **)malloc(sizeof(char **) * arraysize);
    for (i = 0; i < (arraysize - 1); i++) {
	RFIELD(retval->s_aliases[i]);
    }
    RNUM(retval->s_port);
    RFIELD(retval->s_proto);
    fclose(p);
    return retval;
}

void endpwent()
{}


#endif /* NIH_ROUTINES */

#endif /* Use_System_Routines */

#ifdef NIHPROG

/*
 ---------------- cl_nih program which serves the YP requests -----
 */

FILE *out;

main(argc, argv)
int argc;
char *argv[];
{
    if (argc <= 3) {
	fprintf(stderr, "Not enough arguments to NIH\n");
	fflush(stderr);
	exit(-1);
    }

    out = fopen(argv[1], "w");
    
    fprintf(out, "%d|", VERSION);

    switch (atoi(argv[2])) {
      case GETPWNAM:
	dogetpwnam(argv[3]);
	break;
      case GETPWUID:
	dogetpwuid(atoi(argv[3]));
	break;
      case GETHOSTBYNAME:
	dogethostbyname(argv[3], argv[4]);
	break;
      case GETHOSTBYADDR:
	dogethostbyaddr(atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), argv[6]);
	break;
      case GETSERVBYNAME:
	dogetservbyname(argv[3], argv[4]);
	break;
    case GETHOSTENT:
	dogethostent(atoi(argv[3]));
	break;
      default:
	fprintf(stderr, "Uninterpreted command\n");
	fflush(stderr);
	exit(-1);
    }
    fclose(out);
    exit(0);
}

#define WFIELD(ptr) if ((ptr) != NULL) { fprintf(out, "%d|%s", strlen((ptr)), (ptr)); } else { fprintf(out, "0|"); }

dogetpwnam(name)
char *name;
{
    struct passwd *passval = getpwnam(name);

    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */
    
    /* Common fields */
    
    WFIELD(passval->pw_name);
    WFIELD(passval->pw_passwd);
    fprintf(out, "%d|", passval->pw_uid);
    fprintf(out, "%d|", passval->pw_gid);
    WFIELD(passval->pw_gecos);
    WFIELD(passval->pw_dir);
    WFIELD(passval->pw_shell);
    
    /* Machine specific fields */

#if defined(sun) || defined(SVR4)
    WFIELD(passval->pw_comment);
#if defined(__pwd_h) || defined(SVR4) /* flag for 4.1 passwd structure */
    WFIELD(passval->pw_age);
#else
    fprintf(out, "%d|", passval->pw_quota);
#endif /* __pwd_h */
#endif /* sun */

#ifdef ultrix
    WFIELD(passval->pw_comment);
#ifdef SYSTEM_FIVE
    WFIELD(passval->pw_age);
#else
    fprintf(out, "%d|", passval->pw_quota);
#endif /* system_five */
#endif /* ultrix */

#ifdef hpux
    WFIELD(passval->pw_comment);
    WFIELD(passval->pw_age);
    fprintf(out, "%d|", passval->pw_audid);
    fprintf(out, "%d|", passval->pw_audflg);
#endif /* hpux */

#if defined(sgi) && !defined(SVR4)
    WFIELD(passval->pw_age);
    WFIELD(passval->pw_comment);
    fprintf(out, "%d|", passval->pw_origin);
    WFIELD(passval->pw_yp_passwd);
    WFIELD(passval->pw_yp_gecos);
    WFIELD(passval->pw_yp_dir);
    WFIELD(passval->pw_yp_shell);
    WFIELD(passval->pw_yp_netgroup);
#endif /* sgi */
}

dogetpwuid(uid)
int uid;
{
    struct passwd *passval = getpwuid(uid);

    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */
    
    /* Common fields */
    
    WFIELD(passval->pw_name);
    WFIELD(passval->pw_passwd);
    fprintf(out, "%d|", passval->pw_uid);
    fprintf(out, "%d|", passval->pw_gid);
    WFIELD(passval->pw_gecos);
    WFIELD(passval->pw_dir);
    WFIELD(passval->pw_shell);
    
    /* Machine specific fields */

#if defined(sun) || defined(SVR4)
    WFIELD(passval->pw_comment);
#if defined(__pwd_h) || defined(SVR4) /* flag for 4.1 passwd structure */
    WFIELD(passval->pw_age);
#else
    fprintf(out, "%d|", passval->pw_quota);
#endif /* __pwd_h */
#endif /* sun */

#ifdef ultrix
    WFIELD(passval->pw_comment);
#ifdef SYSTEM_FIVE
    WFIELD(passval->pw_age);
#else
    fprintf(out, "%d|", passval->pw_quota);
#endif /* system_five */
#endif /* ultrix */

#ifdef hpux
    WFIELD(passval->pw_comment);
    WFIELD(passval->pw_age);
    fprintf(out, "%d|", passval->pw_audid);
    fprintf(out, "%d|", passval->pw_audflg);
#endif /* hpux */

#if defined(sgi) && !defined(SVR4)
    WFIELD(passval->pw_age);
    WFIELD(passval->pw_comment);
    fprintf(out, "%d|", passval->pw_origin);
    WFIELD(passval->pw_yp_passwd);
    WFIELD(passval->pw_yp_gecos);
    WFIELD(passval->pw_yp_dir);
    WFIELD(passval->pw_yp_shell);
    WFIELD(passval->pw_yp_netgroup);
#endif /* sgi */
}

dogethostbyname(name, order)
char *name, *order;
{
    struct hostent *passval;
    register int n, i;

#if defined(sgi) && !defined(SVR4)
    if (strcmp(order, NIH_EMPTY_ORDER)) {
	sethostresorder(order);
    }
#endif /* sgi */

    passval = gethostbyname(name);
    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */
    
    WFIELD(passval->h_name);
    n  = 0;
    while (1) {
	if (passval->h_aliases[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of aliases */
    for (i = 0; i < (n - 1); i++) {
	WFIELD(passval->h_aliases[i]);
    }
    fprintf(out, "%d|", passval->h_addrtype);
    fprintf(out, "%d|", passval->h_length);
#ifdef h_addr			/* check for h_addr_list */
    n = 0;
    while (1) {
	if (passval->h_addr_list[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of addresses */
    for (i = 0; i < (n - 1); i++) {
	fwrite(passval->h_addr_list[i], passval->h_length, 1, out);
    }
#else
    fwrite(passval->h_addr, passval->h_length, 1, out);
#endif /* h_addr */
}

dogethostent(count)
 int count;
{
    /* count is the number of entries to skip */
    
    struct hostent *passval;
    register int n, i;

#if defined(sgi) && !defined(SVR4)
    if (strcmp(order, NIH_EMPTY_ORDER)) {
	sethostresorder(order);
    }
#endif /* sgi */

    sethostent(1);
    while (count-- > 0)
    {
	if ((passval = gethostent()) == NULL) {
	    endhostent();
	    fprintf(out, "0|");
	    return;
	}
    }
	    
    passval = gethostent();
    endhostent();
    
    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */
    
    WFIELD(passval->h_name);
    n  = 0;
    while (1) {
	if (passval->h_aliases[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of aliases */
    for (i = 0; i < (n - 1); i++) {
	WFIELD(passval->h_aliases[i]);
    }
    fprintf(out, "%d|", passval->h_addrtype);
    fprintf(out, "%d|", passval->h_length);
#ifdef h_addr			/* check for h_addr_list */
    n = 0;
    while (1) {
	if (passval->h_addr_list[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of addresses */
    for (i = 0; i < (n - 1); i++) {
	fwrite(passval->h_addr_list[i], passval->h_length, 1, out);
    }
#else
    fwrite(passval->h_addr, passval->h_length, 1, out);
#endif /* h_addr */
}

dogethostbyaddr(addr, len, type, order)
unsigned long addr;
int len, type;
char *order;
{
    struct hostent *passval;
    register int n, i;


#if defined(sgi) && !defined(SVR4)
    if (strcmp(order, NIH_EMPTY_ORDER)) {
	sethostresorder(order);
    }
#endif /* sgi */

    passval = gethostbyaddr((char *)&addr, len, type);
    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */
    
    WFIELD(passval->h_name);
    n  = 0;
    while (1) {
	if (passval->h_aliases[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of aliases */
    for (i = 0; i < (n - 1); i++) {
	WFIELD(passval->h_aliases[i]);
    }
    fprintf(out, "%d|", passval->h_addrtype);
    fprintf(out, "%d|", passval->h_length);
#ifdef h_addr			/* check for h_addr_list */
    n = 0;
    while (1) {
	if (passval->h_addr_list[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of addresses */
    for (i = 0; i < (n - 1); i++) {
	fwrite(passval->h_addr_list[i], passval->h_length, 1, out);
    }
#else
    fwrite(passval->h_addr, passval->h_length, 1, out);
#endif /* h_addr */
}

dogetservbyname(name, proto)
char *name, *proto;
{
    struct servent *passval = getservbyname(name, proto);
    register int n, i;

    if (passval == NULL) {
	fprintf(out, "0|");
	return;
    }
    fprintf(out, "1|");		/* success */

    WFIELD(passval->s_name);
    n = 0;
    while (1) {
	if (passval->s_aliases[n++] == NULL) {
	    break;
	}
    }
    fprintf(out, "%d|", n);		/* number of aliases */
    for (i = 0; i < (n - 1); i++) {
	WFIELD(passval->s_aliases[i]);
    }
    fprintf(out, "%d|", passval->s_port);
    WFIELD(passval->s_proto);
}

#endif /* NIHPROG */

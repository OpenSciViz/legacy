;;
;;				-[Thu Oct 31 14:57:03 1991 by smh]-
;; 
;; copyright (c) 1985, 1986 Franz Inc, Alameda, Ca. 
;; copyright (c) 1986-1991 Franz Inc, Berkeley, CA
;;
;; The software, data and information contained herein are proprietary
;; to, and comprise valuable trade secrets of, Franz, Inc.  They are
;; given in confidence by Franz, Inc. pursuant to a written license
;; agreement, and may be stored and used only in accordance with the terms
;; of such license.
;;
;; Restricted Rights Legend
;; ------------------------
;; Use, duplication, and disclosure of the software, data and information
;; contained herein by any agency, department or entity of the U.S.
;; Government are subject to restrictions of Restricted Rights for
;; Commercial Software developed at private expense as specified in FAR
;; 52.227-19 or DOD FAR Suppplement 252 52.227-7013 (c) (1) (ii), as
;; applicable.

;; Description:
;;

;;
;; Load Composer
;;
;; $aclHeader: instcomp.cl,v 1.1 91/11/08 15:47:02 cer Exp $

(in-package :system)

(load-application (require :composer) :devel system::*devel*)

(format t "~&; Finished loading Composer...~%")
(force-output)

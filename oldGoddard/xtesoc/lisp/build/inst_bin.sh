#! /bin/sh
#
# $Header: inst_bin.sh,v 1.11 1993/05/05 03:47:55 layer Exp $
#
# usage:
#   install_binary.sh pathname [default_cl_name]

sigs="1 2 13 15"
trap "/bin/echo $0 was interrupted...exiting.; exit 1" $sigs

set -u

if test "$1" = "-n"; then
	title=$2
	shift; shift
else
	title="Allegro CL image"
fi

pwd="`/bin/pwd`"
dest_dir="`dirname $1`"
name="`basename $1`"
if test -n "$2"; then
	from="$2"
else
	from="cl"
fi

if test "`(cd ${dest_dir}; /bin/pwd)`" = "$pwd"; then
	if test "${name}" != "${from}"; then
		echo moving $title to $1...
		rm -f $1
		bin/mv-nfs ${from} $1
		if test $? -ne 0; then
			echo "$0: could not move binary (${from}) to $1"
			echo "$0: check space on ${dest_dir} and re-run config"
			exit 1
		fi
	fi
else
	echo moving $title to $1...
	rm -f $1
	bin/mv-nfs ${from} $1
	if test $? -ne 0; then
		echo "$0: could not move binary (${from}) to $1"
		echo "$0: check space on ${dest_dir} and re-run config"
		exit 1
	fi
	if test -f clwatch; then
		echo copying clwatch to ${dest_dir}...
		rm -f ${dest_dir}/clwatch
		cp clwatch ${dest_dir}/clwatch
		if test $? -ne 0; then
			echo "$0: could not copy clwatch to ${dest_dir}"
			echo "$0: check space on ${dest_dir and re-run config"
			exit 1
		fi
	fi
fi

exit 0


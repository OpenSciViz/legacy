#! /bin/sh -e
# $Header: setup_LLP.m4,v 1.2 1993/12/03 23:52:39 duane Exp $

if test -z "$LD_LIBRARY_PATH"; then
	LD_LIBRARY_PATH=/usr/lib
fi

set -u

svr4=non-zero-length-string
host=sun4
shell=sh
for=

while test $# -gt 0; do
	case $1 in
	-c)	shell=csh
		;;
	motif)
		for=motif
		;;
	openlook)
		for=openlook
		;;
	*)	echo "$0: bad argument: $1" 1>&2
		exit 1
		;;
	esac
	shift
done

if test -z "$for"; then
	echo "usage: $0 [-c] { openlook | motif }" 1>&2
	exit 1
fi

llp="`echo $LD_LIBRARY_PATH | sed 's/:/ /g'`"

if test "$for" = "openlook"; then
	if test -n "$svr4"; then
		target=libXol.so
		libX11=libX11.so
		libXt=libXt.so
	else
		target=libXol.a
		libX11=libX11.a
		libXt=libXt.a
	fi
else
	if test -n "$svr4"; then
		target=libXm.so
		libX11=libX11.so
		libXt=libXt.so
	else
		target=libXm.a
		libX11=libX11.a
		libXt=libXt.a
	fi
fi

car=
cdr=
found=

for l in $llp; do
	if test -z "$car" -a -f $l/$target; then
		car=$l
		if test "$host" != "hpprism"; then
			for lib in $libX11 $libXt; do
				if test ! -f $l/$lib; then
					cat << EOF
$0: $l/$lib does not exist.  It appears that $target does
	not have a companion $lib in $l, which probably means you
	have a partially installed $for.  Check that your LD_LIBRARY_PATH
	is setup correctly and that you are using the correct directory
	for $for.  If you feel $for is installed correctly consult the
	installation manuals or your vendor for $for.
EOF
					exit 1
				fi
			done
		fi
	else
		cdr="$cdr $l"
	fi
done

if test -z "$car"; then
	if test "$for" = "openlook" -a -d /usr/openwin; then
		echo "Warning: $0: assuming openwindows is in /usr/openwin..." 1>&2
		car=/usr/openwin/lib
	else
		echo "$0: could not find $target in LD_LIBRARY_PATH: $LD_LIBRARY_PATH" 1>&2
		exit 1
	fi
fi

llp2=$car
if test -n "$cdr"; then
	for l in $cdr; do
		llp2="$llp2:$l"
	done
fi

echo "LD_LIBRARY_PATH has been setup for $for, and is:" 1>&2
echo "   $llp2" 1>&2

if test "$shell" = "sh"; then
	echo "LD_LIBRARY_PATH=$llp2; export LD_LIBRARY_PATH"
else
	echo setenv LD_LIBRARY_PATH $llp2
fi

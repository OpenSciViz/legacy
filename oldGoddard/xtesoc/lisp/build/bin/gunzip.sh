#! /bin/sh -eu

if test -z "$GUNZIP"; then
	echo "GUNZIP variable is not set"
	exit 1
fi

output_directory=
removez=xxxxx
while test $# -gt 0; do
	case $1 in
	-r)	removez=
		;;
	-d)	output_directory=$2
		shift
		;;
	*)	break
		;;
	esac
	shift
done

if test $# = 0; then
	$GUNZIP
	exit 0
fi

sigs="1 2 13 15"

for file in $*; do
	case $file in
	*$GZIP_EXT)
		z=$file
		f=`echo $file | sed "s,$GZIP_EXT\$,,"`
		;;
	*)
		z=$file$GZIP_EXT
		f=$file
		;;
	esac
	if test -n "$output_directory"; then
		f="$output_directory/`basename $f`"
	fi

	if test -f $z -a -f $f; then
		echo "Both $z and $f exist."
		exit 1
	fi

	if test -f $f; then
		continue
	fi
	if test ! -f $z; then
		echo "$z does not exist"
		exit 1
	fi

	if test -n "$output_directory"; then
		echo "uncompressing $z to $f..."
	else
		echo "uncompressing $z..."
	fi
	rm -f $f
	trap "echo $0 was interrupted.  Will remove $f...;rm -f $f;exit 1" $sigs
	$GUNZIP $z > $f
	trap "echo $0 was interrupted.  Exiting...; exit 1" $sigs
	if test -n "$removez"; then
		rm -f $z
	fi
done

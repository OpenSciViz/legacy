#! /bin/sh -e
# $Header: instr.sh,v 1.6 1993/05/05 03:49:25 layer Exp $

if test -z "$GZIP_EXT"; then
	echo "GZIP_EXT does not have a value" 1>&2
	exit 1
fi

for i in $*; do
	if test ! -f $i; then
		echo $i does not exist...skipping to next file... 1>&2
		continue
	fi
	case $i in
		*$GZIP_EXT)
			$GUNZIP < $i
			;;
		*)
			cat < $i
			;;
	esac
done

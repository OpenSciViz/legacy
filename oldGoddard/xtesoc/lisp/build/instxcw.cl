;;				-[Tue Jan 14 11:27:36 1992 by layer]-
;;
;; Loader for X11 Common Windows
;;

(in-package :system)

(let ((*compiler-not-available-warning* nil))
  (load-application
   (progn
     (require :xcw)
     (let ((g-f-l (find-symbol (symbol-name 'genfontlist) :cw)))
       (unless g-f-l (error "Can't find symbol: cw:genfontlist."))
       (when system::*install-generate-fonts*
	 (handler-case (funcall g-f-l :host system::*install-server-name*)
	   (error (condition)
	     (format
	      *terminal-io*
	      "Error occured during ~s to X server at host ~s:~% ~a~%"
	      g-f-l system::*install-server-name*
	      condition)
	     (format *terminal-io* "NOTE: will build image anyway...~%"))))))
   :devel system::*devel*))

(format t "~&; Finished loading XCW...~%")
(force-output)

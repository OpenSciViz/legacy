/* $Header: main.c,v 1.4 1993/11/05 00:06:52 smh Exp $ */

main(argc,argv,envp)
char **argv;
char **envp;
{
    lisp_main(argc,argv,envp);
}

/*
 * Lisp exits by calling lisp_exit(n).  It is the responsibility of this
 * routine to call exit(n).
 */
lisp_exit(n)
{
    exit(n);
}

#! /bin/sh -eu
# $Header: mv-nfs,v 1.11 1993/08/31 04:57:57 layer Exp $

copy=nil
pflag=
quiet=

while test $# -gt 0; do
	case $1 in
	-c)	copy=t
		;;
	-q)	quiet=xxx
		;;
	-p)	pflag=-p
		;;
	*)	break
		;;
	esac
	shift
done

nargs=$#

files=
while test $# -gt 1; do
	files="$files $1"
	shift
done

if test $# -gt ${nargs} -a ! -d $1; then
	echo "$0: last argument must be a directory for multi-file move: $1"
	exit 1
fi

if test ! -d $1; then
	destdir=`dirname $1`
	base=/`basename $1`
else
	destdir=$1
	base=
fi

if test -f bin/nfspwd; then
	nfspwd=`pwd`/bin/nfspwd
else
	nfspwd=nfspwd
fi

for file in $files; do
	fromdir=`dirname ${file}`

	if test "$copy" = "t"; then
		mv="cp ${file} ${destdir}${base}"
	else
		mv="mv ${file} ${destdir}${base}"
	fi

	if test "`(cd $fromdir; $nfspwd -fstype)`" = "nfs"; then
		# from directory is remote
		cmd="rcp $pflag `(cd $fromdir; $nfspwd)`/`basename ${file}` `(cd ${destdir}; $nfspwd)`${base}"
	elif test "`(cd ${destdir}; $nfspwd -fstype)`" = "nfs"; then
		# to directory is remote
		cmd="rcp $pflag ${file} `(cd ${destdir}; $nfspwd)`${base}"
	else
		# local, just `mv' it
		if test -z "$quiet"; then echo $mv; fi
		$mv
		continue
	fi

	if test -z "$quiet"; then echo $cmd; fi
	if $cmd; then
		if test "$copy" != "t"; then
			cmd="rm ${file}"
			if test -z "$quiet"; then echo $cmd; fi
			$cmd
		fi
	else
		echo "$0: rcp failed, will try and move the image..."
		if $mv; then
			echo "$0: mv successful."
		else
			if test "$copy" != "t"; then
				cmd="rm ${file}"
				if test -z "$quiet"; then echo $cmd; fi
				$cmd
			fi
			echo "$0: mv failed, too."
			exit 1
		fi
	fi
done

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;				-[Fri Nov 19 00:00:52 1993 by duane]-
;;
;; Installation testing and verification
;;
;; This program, when loaded into a newly installed cl, will attempt to
;; verify that the installation was done properly.
;;
;; $Header: installtest.cl,v 1.9 1993/11/24 03:29:57 duane Exp $
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro say (format-string &rest args)
  `(progn
    (format t ,format-string ,@args)
    (terpri)
    (terpri)))

(defmacro pause (message)
  `(let ((char nil))
    (format t
	    "~%~a~%~%--------~%~%~
[type RETURN to continue, or anything else + RETURN to exit]"
      ,message)
    (force-output)
    (setq char (read-char))
    (if (not (char= #\newline char))
	(exit 1))
    (terpri)
    (terpri)))

(defun install-test-break-hook (frame cause continue-string continue-args
				condition)
  (declare (ignore cause frame))
  (format *error-output* "Error: ~a~%" condition)
  (if continue-string
      (format *error-output* "Continuable error: ~a~%" continue-string))
  (format *error-output* "~%TEST FAILED: PLEASE RE-INSTALL LISP~%~%")
  (exit 1))

;; Need to load in db:break-hook first
(require :sundebug)
(setf (db:break-hook) #'install-test-break-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(say "--------- test 1: the library pathname

The trace package, which resides in the library, will now be loaded:")

(require :trace)

(pause "The path before \"code/trace.fasl\" is the path you selected for the
library.  If you want to change this, you should remake Common Lisp before
installing it.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(say "--------- test 2: the compiler

To make sure the system was built correctly, we will now compile a small
program:")

(compile nil '(lambda (x) (foo x)))

(pause "The compiler may or may not have printed anything, but no errors
should have occurred.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(say "--------- test 3: case mode

The case mode is set in the makefile in the rule for creating Lisp.
The current case mode (the value of excl::*current-case-mode*) is
~s."
     excl::*current-case-mode*)

(pause "If this is not what you thought it would be, then please remake
Common Lisp with a different case-mode before installing it.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(say "~%~%Installation succeeded~%~%")
(setf (db:break-hook) nil)

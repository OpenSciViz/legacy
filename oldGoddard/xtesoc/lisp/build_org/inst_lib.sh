#! /bin/sh
#
# $Header: inst_lib.sh,v 1.10.2.1 1994/01/11 22:26:17 georgej Exp $
#
# usage:
#   install_library.sh library

sigs="1 2 13 15"
trap "/bin/echo $0 was interrupted...exiting.; exit 1" $sigs

set -u

library=$1

if test ! -d ${library}; then
	mkdir ${library}
	if test $? -ne 0; then
		echo "$0: could not mkdir ${library}"
		echo "$0: change permissions and re-run config"
		exit 1
	fi
fi

to_dir="`(cd ${library}; /bin/pwd)`"
from_dir="`(cd ../lib; /bin/pwd)`"

case $to_dir in
  ${from_dir})
# no need to copy anything!
	exit 0
	;;
  ${from_dir}/*)
	cat << EOF
$0: $to_dir is a subdirectory of
    $from_dir, which is not allowed.
	Specify the library as just $from_dir
    and it will not be moved.
EOF
	exit 1
	;;
esac

echo removing old library files...

# the files in the place we are copying from:
files="`(cd ${from_dir}; find . ! -type d -print | sed s/${GZIP_EXT}\$//)`"

# things that should be deleted upon copying in the new library:
# N.B.: Don't delete lso_file --  others may depend on it
files="$files cl_nih clm-td clm-toolkits"

for f in ${files}; do
	file=${to_dir}/${f}
	rm -f ${file} ${file}${GZIP_EXT}
	if test -f ${file}; then
		echo "$0: could not remove ${file}"
		echo "$0: check permissions and re-run config"
		exit 1
	fi
	if test -f ${file}${GZIP_EXT}; then
		echo "$0: could not remove ${file}${GZIP_EXT}"
		echo "$0: check permissions and re-run config"
		exit 1
	fi
done

ofiles="`(cd ${to_dir}; find . ! -type d -print)`"
if test "x${ofiles}" != "x"; then
	echo ""
	echo "********************************************************************"
	echo "**** WARNING: there are potentially old library files in"
	echo "****          ${to_dir}:"
	(cd ${to_dir}; ls ${ofiles})
	echo "********************************************************************"
	echo ""
fi

echo copying library to ${to_dir}...
sh bin/cpt ${from_dir} ${to_dir}
if test $? -ne 0; then
	echo "$0: could not copy library to ${to_dir}"
	echo "	check space on ${to_dir} and re-run config"
	exit 1
fi

exit 0

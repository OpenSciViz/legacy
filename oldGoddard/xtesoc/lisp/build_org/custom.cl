;;; $Header: custom.cl,v 1.12 1993/08/31 04:56:32 layer Exp $
;;;
;;; Add to this file any forms you wish to be evaluated before the Allegro
;;; CL image is saved.

;; For SGI users.  Also an example of how to load a module with space
;; efficiency in mind.
#+ignore
(system:load-application (require :iris-gl))

#|
;; the following are examples of how to customize the actions of custom.cl

(let ((acl-foo (sys:getenv "ACL_FOO")))
  (when acl-foo
    ;; load FOO
    (load acl-foo)))

;; a second sytle of customization:

(let ((acl-foo (sys:getenv "ACL_BAR")))
  (when acl-foo
    (push :do-bar *features*)))

#+do-bar
(setq excl:*logical-pathname-translations-database* "/foo/bar/myhosts.lisp")
|#

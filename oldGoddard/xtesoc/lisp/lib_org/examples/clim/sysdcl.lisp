;;; -*- Mode: LISP; Syntax: Common-lisp; Package: USER; Base: 10; Lowercase: Yes -*-
;; 
;; copyright (c) 1991 Franz Inc, Berkeley, CA  All rights reserved.
;;
;; The software, data and information contained herein are proprietary
;; to, and comprise valuable trade secrets of, Franz, Inc.  They are
;; given in confidence by Franz, Inc. pursuant to a written license
;; agreement, and may be stored and used only in accordance with the terms
;; of such license.
;;
;; Restricted Rights Legend
;; ------------------------
;; Use, duplication, and disclosure of the software, data and information
;; contained herein by any agency, department or entity of the U.S.
;; Government are subject to restrictions of Restricted Rights for
;; Commercial Software developed at private expense as specified in FAR
;; 52.227-19 or DOD FAR Supplement 252.227-7013 (c) (1) (ii), as
;; applicable.
;;

;; $fiHeader: sysdcl.lisp,v 1.12 91/08/05 14:36:15 cer Exp $

(in-package :user)

"Copyright (c) 1990, 1991 Symbolics, Inc.  All rights reserved.
Copyright (c) 1991, Franz Inc. All rights reserved
 Portions copyright (c) 1988, 1989, 1990 International Lisp Associates."

(clim-lisp:defpackage clim-demo
  (:use clim-lisp clim)
  (:shadowing-import-from clim-utils
    defun
    flet labels
    defgeneric defmethod
    #+(or lucid excl) with-slots
    dynamic-extent non-dynamic-extent)
  (:export   
    *demo-root*
    start-demo))

(defsys:defsystem clim-demo
  (:default-pathname #+Genera "SYS:CLIM;DEMO;"
		     #+Cloe-Runtime "\\clim\\demo\\"
		     #+lucid "/home/hornig/clim/demo/"
		     #+excl (frob-pathname "demo")
   :default-binary-pathname #+Genera "SYS:CLIM;DEMO;"
			    #+Cloe-Runtime "\\clim\\bin\\"
			    #+lucid "/home/hornig/clim/lcl4/"
                            #+excl (frob-pathname "demo")
   :needed-systems (clim-standalone))

  ("aaai-demo-driver")
  ("graphics-demos" :load-before-compile ("aaai-demo-driver"))
  ("new-cad-demo"   :load-before-compile ("aaai-demo-driver"))
  ("navdata"	    :load-before-compile ())
  ("navfun"         :load-before-compile ("aaai-demo-driver" "navdata"))
  ("listener"       :load-before-compile ("aaai-demo-driver"))
  ("puzzle"         :load-before-compile ("aaai-demo-driver"))
  ("address-book"   :load-before-compile ("aaai-demo-driver"))
  ("thinkadot"      :load-before-compile ("aaai-demo-driver"))
  ("demo-prefill" :features (or Genera Cloe-Runtime))
  )

#+Genera
(defsys:import-into-sct 'clim-demo 
			:pretty-name "CLIM Demo"
			:default-pathname "SYS:CLIM;DEMO;"
			:default-destination-pathname "SYS:CLIM;DEMO;")

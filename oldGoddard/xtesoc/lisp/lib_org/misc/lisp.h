/*
** Excl Common Lisp			-[Fri Nov 12 00:34:54 1993 by duane]-
**
** copyright (c) 1985,1986 Franz Inc, Alameda, Ca. 
** copyright (c) 1986-1991 Franz Inc, Berkeley, CA
**
** The software, data and information contained herein are proprietary
** to, and comprise valuable trade secrets of, Franz, Inc.  They are
** given in confidence by Franz, Inc. pursuant to a written license
** agreement, and may be stored and used only in accordance with the terms
** of such license.
**
** Restricted Rights Legend
** ------------------------
** Use, duplication, and disclosure of the software, data and information
** contained herein by any agency, department or entity of the U.S.
** Government are subject to restrictions of Restricted Rights for
** Commercial Software developed at private expense as specified in FAR
** 52.227-19 or DOD FAR Supplement 252 52.227-7013 (c) (1) (ii), as
** applicable.
**
**
** Note that these macros are subject to change in future releases.
** These explanations apply only to release 3.0 of Allegro
** Common Lisp.
**
** Franz Inc. will endeavor to supply versions of these macros for
** future releases, but does not guarantee all macros will exist in
** such future releases.
**
** $Header: lisp.h,v 1.26 1993/11/24 03:32:36 duane Exp $
*/

/*
** All lisp data types are tagged using the lower 3 bits as a tag value
** for most ports, and the upper 8 bits for a cray.
** Data types fall into two classes:
**  Immediate, where the data is encoded in a 4/8 byte object
**   (these are data types of fixnum and character).
**  Non-immediate, where the lisp value points at the data object
**   (these are all other data types).
**
** For the cray:
** The non-immediate data types are also tagged using the upper 8 bits,
** and the pointer points to the first word of any such Lisp object.
** For other ports:
** The non-immediate data types are also tagged using the lower 3 bits.
** and the pointer points somewhere to within the first 8 bytes of any
** such Lisp object.
**
** There is a non-immediate data tag of "other"; in this case the first
** byte of the Lisp object will encode the type of the object
**
** Note that not all data types are represented by C macros
**
*********************************************************************
*** All non-immediate Lisp values in C should be declared LispVal **
*********************************************************************
*/

#define signal lisp_signal

#ifdef CRAY
typedef int LispVal;
#define GetTag(v) ((long)v >> 56)
#else
typedef char * LispVal;
#define GetTag(v) ((long)v & 7)
#endif


/* the basic tags */
#if defined(CRAY) 
/* Cray is word-oriented, tag at the top of the 64-bit word */
# define FixnumType 0
# define SymbolType 2
# define CharType   3
# define ListType   4
# define OtherType  5
# define NilType    6
# define NegFixnumType 255
#else
#if defined(hp9000s800)
/* HP reverses some tags from others, so as to generate quicker typechecks */
# define FixnumType 0
# define OtherType  1
# define ListType   2
# define OddFixnumType 4
# define CharType   5
# define NilType    6
# define SymbolType 7
#else
/* all others */
# define FixnumType 0
# define ListType   1
# define OtherType  2
# define OddFixnumType 4
# define NilType    5
# define CharType   6
# define SymbolType 7
#endif
#endif

   
/*
** fixnum data type
**
** convert a fixnum to a machine integer
*/
#ifdef uts
extern FixnumToInt();
#else
#ifdef CRAY
#define FixnumToInt(v) (v)
#else
#define FixnumToInt(v) ( (v) >> 2)
#endif
#endif

/*
** convert a machine integer to a fixnum
*/
#ifdef CRAY
#define IntToFixnum(v) (v)
#else
#define IntToFixnum(v) ( (v) << 2)
#endif

#if defined(uts) || defined(m88k) || defined(_IBMR2) || defined(hp9000s800)
#define TagOffset 8
#else
#define TagOffset 0
#endif

#ifdef CRAY
#define OtherTypeOfs 0
#else
#define OtherTypeOfs (OtherType-TagOffset)
#endif

/*
** get the type of an "other" object
*/
#define GetOType(v)  (unsigned long)(*(unsigned char *)((v)-OtherTypeOfs))

#define GetType(v) (( GetTag(v) == OtherType )? GetOType(v) : GetTag(v))


#define SymbolTypeOfs (SymbolType-TagOffset)
#ifndef CRAY
/*
** symbol data type; tag is 111
** return the value of a symbol
** symbol is set up as
** byte 0: type
**	1: flags (most sig. bit: 1 if this is a constant)
**	2: hash value (2 bytes)
**	4: value
**	8: package (pointer to the home package)
**     12: function
**     16: name (pointer to a simple-string)
**     20: plist (pointer to the property list)
**
*/
#define SymbolFlags(s)		(*(char *)(s-SymbolTypeOfs+1))
#define SymbolHashV(s)		(*(short *)(s-SymbolTypeOfs+2))
#define SymbolValue(s)		(*(LispVal *)(s-SymbolTypeOfs+4))
#define SymbolPackage(s)	(*(LispVal *)(s-SymbolTypeOfs+8))
#define SymbolFunct(s)		(*(LispVal *)(s-SymbolTypeOfs+12))
#define SymbolName(s)		(*(LispVal *)(s-SymbolTypeOfs+16))
#define SymbolPlist(s)		(*(LispVal *)(s-SymbolTypeOfs+20))
#define SymbolConstant(s)	(SymbolFlags(s) & 0x80)

struct Symbol {
    char dtype;
    char flags;
    short hash_val;
    LispVal value;
    LispVal package;
    LispVal function;
    LispVal name;
    LispVal plist;
};
#else /* non-4.2 ports */
/* CRAY */
/*
** symbol data type; tag is 010
** return the value of a symbol
** symbol is set up as
** byte 0: value
**	4: package (pointer to the home package)
**	8: function
**     12: name (pointer to a simple-string)
**     16: plist (pointer to the property list)
**     20: flags (most sig. bit: 1 if this is a constant)
**     24: hash value (2 bytes)
*/

#define SymbolStart(s)          (*(LispVal*)((s)+1))
#define SymbolFunct(s)          (*(LispVal*)((s)+2))
#define SymbolPackage(s)        (*(LispVal*)((s)+3))
#define SymbolName(s)           (*(LispVal*)((s)+4))
#define SymbolPlist(s)          (*(LispVal*)((s)+5))
#define SymbolFlags(s)          ((*(int *)((s)+6)) >> 48)
#define SymbolHashV(s)          (((*(int *)((s)+6)) >> 32) && 0xffff)
#define SymbolCode(s)           (*(LispVal*)((s)+7))

struct Symbol {
    LispVal value;
    LispVal start;
    LispVal function;
    LispVal package;
    LispVal name;
    LispVal plist;
    unsigned int flags:16;
    unsigned int hash_val:16;
    LispVal code;
};
#endif /* non 4.2 */

/*
 * function data type
 */
#define FunctionType 8

struct Function {
    char dtype;
    char flags;
    short size;
    int start;
    int hash;
    LispVal name;
    LispVal code;
    LispVal formals;
    LispVal cframe;
    LispVal callcount;
    LispVal locals;
    LispVal const0; /* first constant */
};

/*
** character data type
** for most ports:
** form is  0000 0bbb bbbb bfff ffff fccc cccc cttt
** for the cray:
** form is  tttt tttt ...   ...  bbbb bbbb ffff ffff cccc cccc
** where b are the bits, f are the font, and c is the standard character.
** ttt is the tag.
** convert a standard-char to a machine byte
*/
#ifdef CRAY
#define ScharToChar(v) ((char)( (v) && 0xff))
#define CharToSchar(v) ((LispVal)((int v) | (CharType << 56)))
#else
#define ScharToChar(v) ((char)( (v) >> 3))
#define CharToSchar(v) ((LispVal)((((int)(v)) << 3) | CharType))
#endif

/*
** nil is special as it is both a symbol and a list
** In particular, the symbol accessor macros won't work right.
** Check on the nilness of a pointer.
*/
#define Null(p) ((p) == nilval)
#ifdef mi386
#define nilval (&lv_nil)
extern char lv_nil;
#else
extern LispVal nilval;
#endif

/*
** cons data type
** we store it as a car, then cdr get the cdr of a list;
** the result is a pointer to the cdr of the next cons cell
*/

#ifdef CRAY
#define ListTypeOfs 0
#define ListCdrOfs 1
#else
#define ListTypeOfs (ListType-TagOffset)
#define ListCdrOfs 4
#endif

/*
** get the cdr of a list; the result is the lisp value
*/
#define Cdr(x) (*(LispVal*)((LispVal)x - ListTypeOfs + ListCdrOfs))

/*
** get the car of a list; the result is the lisp value
*/
#define Car(x) (*(LispVal*)((LispVal) x - ListTypeOfs))

struct List {
    LispVal car;
    LispVal cdr;
};

/*
** Arrays: these are of type "other"
** All arrays have a certain bit (bit 6 in this Lisp) set in the type.
** Arrays a subtype of both simple-array and vector have bit 5 set also
*/
#define Array_bit 64
#define Vec_bit 32

/*
** There are two structures used for arrays.
**	fixed size vector: for types a subtype of both simple-array and vector
**	array: everything else
**
** vectors of fixed length; have tag type "other"
**  byte 0: type of the vector
**  byte 1,2,3: size of the vector
**  bytes 4 & on: the data, C style.
**
** arrays have tag type "other"
**  byte 0: type of the vector
**  byte 1: unused
**  bytes 2, 3: number of dimensions (rank)
**  bytes 4-7: fill-pointer-value (number of entries as a fixnum).
**  bytes 8-11: data field (a pointer)
**   1. if array is not a displaced array:
**	pointer -> cons cell whose cdr -> fixed size vector
**   2. if array is displaced:
**	pointer may point to either to another array or to a cons cell
**      whose cdr -> fixed size vector
**  bytes 12-15: displacement (fixnum to add before following
**  bytes 16-19: 	       list of fixnum dimensions).
**  bytes 20-23: Array flags - this is a fixnum 
*/

#if (vax || m_sequent || sun386i || m_isc386 || MIPSEL)
#define A_rank(s) (*((short *)(s - OtherType + 2)) & 0xffff)
#else
#define A_rank(s) (*((int *)(s - OtherTypeOfs)) & 0xffff)
#endif /* vax or m_sequent or mi386 or sun386i or dec3100 */

#ifdef CRAY
#define A_fillp(s) (*(int *)(s - OtherTypeOfs + 1))
#define A_dataptr(s) (*(LispVal*)(s  - OtherTypeOfs + 2))
#define A_disp(s) (*(int *)(s -OtherTypeOfs + 3))
#define A_dims(s)  (*(LispVal*)(s - OtherTypeOfs + 4))
#define A_flags(s) FixnumToInt(*(int *)(s - OtherTypeOfs + 5))
#else
#define A_fillp(s) (*(int *)(s - OtherTypeOfs + 4))
#define A_dataptr(s) (*(LispVal*)(s  - OtherTypeOfs + 8))
#define A_disp(s) (*(int *)(s -OtherTypeOfs + 12))
#define A_dims(s)  (*(LispVal*)(s - OtherTypeOfs + 16))
#define A_flags(s) FixnumToInt(*(int *)(s - OtherTypeOfs + 20))
#endif /* CRAY */

#define A_flag_adj 1	/* bit 0 set if array adjustable */
#define A_flag_fill 2	/* bit 1 set if array has a fill pointer */
#define A_flag_disp 4	/* bit 2 set if displaced to another array */

struct Array {
    unsigned char dtype;
#ifdef CRAY
    unsigned int :40;
#else
    unsigned int :8;
#endif
    unsigned int rank:16;
    LispVal fillp;
    LispVal dataptr;
    int disp;
    LispVal dims;
    int flags;
};


/*
** and the array types
*/
#define A_arrayType 64		/* (array t) */
#define A_bitType 65		/* (array bit) */
#define A_ubyteType 66		/* (array (mod 2^8)) */
#define A_uwordType 67		/* (array (mod 2^16)) */
#define A_ulongType 68		/* (array (mod 2^32[64])) */
#define A_charType 69		/* (array string-char) == string */
#define A_floatType 70		/* (array single-float) */
#define A_doubleType 71		/* (array double-float) */
#define A_byteType 72		/* (array (signed-byte 8)) */
#define A_wordType 73		/* (array (signed-byte 16)) */
#define A_fixnumType 74		/* (array fixnum) */
#define A_longType 75		/* (array (unsigned-byte 32[64])) */

/*
** return a pointer to the first byte in a fixed size vector
*/
#ifdef CRAY
#define sv_data0_adj 1
#define Vecdata(s) ((LispVal*)(((int)(s) & 0xffffffff) + sv_data0_adj))
#else
#define sv_data0_adj (4 - OtherTypeOfs)
#define Vecdata(s) ((LispVal*)((s) + sv_data0_adj))
#endif

/*
** return the size of a vector
*/

#if (vax || m_sequent || sun386i || m_isc386 || CRAY || MIPSEL)
#define sv_size_adj 0
#else
#define sv_size_adj (- OtherTypeOfs)
#endif /* (vax || m_sequent || sun386i || m_isc386 || CRAY || MIPSEL) */
#define VecSize(s) ((0xffffff & (*(int *)((s)+sv_size_adj))))


struct Vector {
    int size; 			/* includes type */
    LispVal data;
};

/*
** a pointer to the first member of a simple-vector == (simple-array t (*))
*/
#define V_svecType 96
#define V_svec(s) Vecdata(s)

/*
** return a pointer to the first member of a vector of bits
** (simple-array bit (*))
*/
#define V_bitType 97
#define V_bit(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of unsigned byte's
** (simple-array (mod 2^8) (*))
*/
#define V_ubyteType 98
#define V_ubyte(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of unsigned short's
** (simple-array (mod 2^16) (*))
*/
#define V_uwordType 99
#define V_uword(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of unsigned longs's
** (simple-array (mod 2^32) (*))
*/
#define V_ulongType 100
#define V_ulong(s)  Vecdata(s)

/*
** return a pointer to a string
** (simple-array standard-char (*)) == (simple-string (*))
*/
#define V_charType 101
#define V_char(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of floats
** (simple-array single-float (*))
*/
#define V_floatType 102
#define V_float(s) ((float *)( Vecdata(s)))

/*
** return a pointer to the first member of a vector of doubles
** (simple-array double-float (*))
*/
#define V_doubleType 103
#define V_double(s) ((double *)( Vecdata(s)))

/*
** return a pointer to the first member of a vector of signed bytes's
** (simple-array (signed-byte 16) (*))
*/
#define V_byteType 104
#define V_byte(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of signed short's
** (simple-array (signed-byte 16) (*))
*/
#define V_wordType 105
#define V_word(s)  Vecdata(s)

/*
** return a pointer to the first member of a vector of fixnums
** each fixnum in the vector is not typed and is a full machine integer
** (simple-array fixnum (*)) (hence in this implementation == 
** 					(simple-array (signed-byte 32) (*)))
*/
#define V_fixnumType 106
#define V_fixnum(s) ((int *)( Vecdata(s)))

/*
** return a pointer to the first member of a vector of signed longs's
** (simple-array (signed-byte 32) (*))
*/
#define V_longType 107
#define V_long(s)  Vecdata(s)

/*
** Some other type codes
*/
#define CodeVecType 108		/* a vector of function code	*/
#define StructType 15		/* type code for a structure-object */
#define InstanceType 128	/* type code for a flavor instance */
#define StandardInstanceType 12	/* type code of CLOS standard-instance */

/* A standard-instance object is representationally similar to a simple
   vector, although the length is always 4 words and the length field
   is used for other purposes. */

struct StandardInstance {
    int dtype;
    LispVal wrapper;		/* A simple-vector identifying the class.
				   See below. */
    LispVal instance_slots;	/* A simple-vector holding the instance slots.
				   The ordering of slots is typically the
				   reverse of the order returned by
				   clos:class-slots on the class, but this can
				   be overridden by the MOP. */
    int unused;			/* Used by foreign function code. */
};

#define WrapperClassSlotsIndex 3 /* The index of the class-slots vector object
				    in the wrapper.  If non null, this is a
				    simple-vector, each element of which is a
				    cons of the symbol naming the slot and the
				    slot value. */
#define WrapperClassIndex 4	/* The index of the class-object in the wrapper. */

/*
 *  Numeric types
 */
#define S_FloatType 16	/* single-float */
#define S_FtoFloat(v)  (* (float *)(v - OtherTypeOfs + 4))

struct Float {
    int dtype;
    float data;
};


#define D_FloatType 17	/* double-float */

#ifdef CRAY
# define D_FtoFloat(v)  (* (double *)((v) + 1))
#else
# define D_FtoFloat(v)  (* (double *)((v) - OtherTypeOfs + 8))
#endif

struct Double {
    int dtype;
    int unused;
    double data;
};

#define BignumType 18	/* bignum */

#define RatioType 19	/* ratio - numerator and denominator Lisp integers */
/* should be C int or bignum pointer */
#ifdef CRAY
#define R_num(v)  (*(LispVal*)((v) + 1))
#define R_den(v)  (*(LispVal*)((v) + 2))
#else
#define R_num(v)  (*(LispVal*)((v) - OtherTypeOfs + 4))
#define R_den(v)  (*(LispVal*)((v) - OtherTypeOfs + 8))
#endif

struct Ratio {
    int dtype;
    LispVal num;
    LispVal den;
};

#define ComplexType 20	/* complex - real and imaginary parts */
#ifdef CRAY
#define C_real(v)  (*(LispVal*)((v) + 1))
#define C_imag(v)  (*(LispVal*)((v) + 2))
#else
#define C_real(v)  (*(LispVal*)((v) - OtherTypeOfs + 4))
#define C_imag(v)  (*(LispVal*)((v) - OtherTypeOfs + 8))
#endif

#ifdef Complex
/* if you get an error on this, then change the name of the structure below */
float readme = "someone defined ``Complex'' which clashes with the following";
#endif /* Complex */

struct Complex {
    int dtype;
    LispVal num;
    LispVal den;
};


(defvar fi:shell-cd-regexp ":?cd" "\
*The regular expression matching the C shell `cd' command and the
Common Lisp :cd top-level command.   If nil, no tracking of directory
changes will be done.")

(make-variable-buffer-local (quote fi:shell-cd-regexp))

(defvar fi:shell-popd-regexp ":?popd" "\
*The regular expression matching the C shell `popd' command and the
Common Lisp :popd top-level command.   If nil, no tracking of directory
changes will be done.")

(make-variable-buffer-local (quote fi:shell-popd-regexp))

(defvar fi:shell-pushd-regexp ":?pushd" "\
*The regular expression matching the C shell `pushd' command and the
Common Lisp :pushd top-level command.   If nil, no tracking of directory
changes will be done.")

(make-variable-buffer-local (quote fi:shell-pushd-regexp))

(defvar fi:subprocess-continuously-show-output-in-visible-buffer t "\
*If t, output from a subprocess to a visible buffer is continuously
shown.  If a subprocess buffer is visible and the window point is beyond
the process output marker, output to that buffer from its associated
process will be continuously visible.  If the window point is before the
process output marker, the window is not updated.  This is a buffer-local
symbol.")

(make-variable-buffer-local (quote fi:subprocess-continuously-show-output-in-visible-buffer))

(defvar fi:subprocess-enable-superkeys nil "\
*If t, certain keys become `superkeys' in subprocess buffers--this
should be set before starting any subprocesses.  The superkeys are C-a,
C-d, C-o,C-u, C-w, C-z, and C-\\, which will behave as they would in the
current local keymap when typed at the end of a subprocess buffer.  If
typed elsewhere, these keys have their normal global binding.  This is a
buffer-local symbol.  Use setq-default to set the default value for this
symbol.")

(make-variable-buffer-local (quote fi:subprocess-enable-superkeys))

(defvar fi:new-screen-for-common-lisp-buffer nil "\
*If non-nil, then starting Common Lisp will cause emacs to create a new
screen for the *common-lisp* buffer, if this version of emacs is capable of
creating separate screens.  If you redefine fi:display-buffer-function this
variable will be ignored.")

(defvar fi:display-buffer-function (if (eq fi::emacs-type (quote lemacs19)) (quote fi::switch-to-buffer-new-screen) (quote fi::switch-to-buffer)) "\
*If non-nil, then the value should be a function taking one argument,
a buffer, which is used to display a buffer when a subprocess is created.")

(defconst fi:subprocess-env-vars (quote (("EMACS" . "t") ("TERM" . "emacs") ("DISPLAY" or (getenv "DISPLAY") (format "%s:0.0" (system-name))) ("TERMCAP" format "emacs:co#%d:tc=unknown:" (screen-width)))) "\
*An alist containing the environment variables to pass to newly created
subprocesses.")

(defvar fi:pop-to-sublisp-buffer-after-lisp-eval nil "\
*If non-nil, then go to the Lisp subprocess buffer after sending
expressions to Lisp (via the functions which eval or compile the region, a
form or the entire buffer).")

(defvar fi:package nil "\
A buffer-local variable whose value is automatically set for editing
modes and will be nil or a string which names a package in the Lisp
world--ie, in a Lisp subprocess running as an inferior of Emacs in some
buffer.  It is used when expressions are sent from an Emacs buffer to a
Lisp process so that the symbols are read into the correct Lisp package.")

(make-variable-buffer-local (quote fi:package))

(defvar fi:readtable nil "\
A buffer-local variable whose value is automatically set for editing
modes and will be nil or a string which names a readtable in the Lisp
world--ie, in a Lisp subprocess running as an inferior of Emacs in some
buffer.  It is used when expressions are sent from an Emacs buffer to a
Lisp process so that the expressions are read using the correct
readtable.")

(make-variable-buffer-local (quote fi:readtable))

(defvar fi:emacs-to-lisp-transaction-directory "/tmp" "\
*The directory in which files for Emacs/Lisp communication are stored.
When using Lisp and Emacs on different machines, this directory should be
accessible on both machine with the same pathname (via the wonders of NFS).")

(defvar fi:echo-evals-from-buffer-in-listener-p nil "\
*If non-nil, forms evalutated directly in fi:common-lisp-mode by the
functions which compile or eval the region, a form or the entire buffer
will be echoed by Common Lisp.")

(defvar fi:start-lisp-interface-arguments (function (lambda (use-background-streams) (list "-e" (format "(excl:start-emacs-lisp-interface %s)" use-background-streams)))) "\
*This value of this variable determines whether or not the emacs-lisp
interface is started automatically when fi:common-lisp is used to run
Common Lisp images.   If non-nil, then a the value of this variable should
be a function of one argument that returns command line argument sufficient
to start the emacs-lisp interface.  The argument is a boolean that
determines whether or not background streams are used (see
fi:use-background-streams).")

(defvar fi:use-background-streams t "\
*If non-nil, then the default function bound to
fi:start-lisp-interface-arguments will cause background streams to be
initialized in ACL (see the function excl:use-background-streams).  Roughly
speaking, background streams cause processes that do output, but which are
not associated with any particular stream, to do it in a unique listener in
an emacs buffer.  This allows MP:PROCESS-RUN-FUNCTION in the Lisp
environment to be more useful.")

(defvar fi:start-lisp-interface-hook nil "\
*A function or a list of functions to call when we get the rendezvous
info from Lisp in the Lisp subprocess buffer.  This is used to
automatically startup the Emacs-Lisp hidden communication via a socket.
fi:start-lisp-interface-arguments initiates the connection with Lisp from
the Emacs side, Lisp then starts up the daemon which listens for
connections and prints a string to the subprocess buffer (which is not
displayed by the process filter for the Common Lisp subprocess), at which
time the hooks are run.")

(defvar fi:common-lisp-buffer-name "*common-lisp*" "\
*Default buffer name used by fi:common-lisp.  This variable is set by
fi:common-lisp when a new buffer name is used.")

(defvar fi:common-lisp-directory nil "\
*Default directory in which the process started by fi:common-lisp uses.
A CD is done into this directory before the process is started.")

(defvar fi:common-lisp-image-name "cl" "\
*Default Common Lisp image used by fi:common-lisp.  The value is a
string that names the image fi:common-lisp invokes.")

(defvar fi:common-lisp-image-arguments nil "\
*Default Common Lisp image arguments when invoked from `fi:common-lisp',
which must be a list of strings.  Each element of the list is one command
line argument.")

(defvar fi:common-lisp-host nil "\
*The host on which fi:common-lisp starts the Common Lisp
subprocess.  The default is the host on which emacs is running.")

(defvar fi:common-lisp-prompt-pattern "^\\(\\[[0-9]+i?c?\\] \\|\\[step\\] \\)?\\(<[-A-Za-z]* ?[0-9]*?>\\|[-A-Za-z0-9]+([0-9]+):\\) " "\
*The regular expression which matches the Common Lisp prompt.
Anything from beginning of line up to the end of what this pattern matches
is deemed to be a prompt.")

(defvar fi:franz-lisp-buffer-name "*franz-lisp*" "\
*Default buffer name used by fi:franz-lisp.")

(defvar fi:franz-lisp-image-name "lisp" "\
*Default Franz Lisp image to invoke from `fi:franz-lisp'.  If the value
is a string then it names the image file or image path that
`fi:franz-lisp' invokes.  Otherwise, the value of this variable is given
to funcall, the result of which should yield a string which is the image
name or path.")

(defvar fi:franz-lisp-image-arguments nil "\
*Default Franz Lisp image arguments when invoked from `fi:franz-lisp'.")

(defvar fi:franz-lisp-host nil "\
*The host on which fi:franz-lisp starts the Franz Lisp
subprocess.  The default is the host on which emacs is running.")

(defvar fi:franz-lisp-process-name nil)

(defvar fi:franz-lisp-prompt-pattern "^[-=]> +\\|^c{[0-9]+} +" "\
*The regular expression which matches the Franz Lisp prompt, used in
Inferior Franz Lisp mode.  Anything from beginning of line up to the end
of what this pattern matches is deemed to be a prompt.")

(defvar fi::last-input-start nil "\
Marker for start of last input in fi:shell-mode or fi:inferior-lisp-mode
buffer.")

(make-variable-buffer-local (quote fi::last-input-start))

(defvar fi::last-input-end nil "\
Marker for end of last input in fi:shell-mode or fi:inferior-lisp-mode
buffer.")

(make-variable-buffer-local (quote fi::last-input-end))

(defvar fi::shell-directory-stack nil "\
List of directories saved by pushd in this buffer's shell.")

(make-variable-buffer-local (quote fi::shell-directory-stack))

(defvar fi::process-name nil "\
Name of inferior lisp process.")

(make-variable-buffer-local (quote fi::process-name))

(defvar fi::common-lisp-first-time t)

(defvar fi::franz-lisp-first-time t)

(defvar fi::common-lisp-backdoor-main-process-name nil "\
The name of the Common Lisp process to which we have a backdoor
connection.")

(defvar fi::lisp-case-mode (quote :unknown) "\
The case in which the Common Lisp we are connected to lives.")

(defvar fi::listener-protocol (quote :listener))

(defvar fi::lisp-host nil "\
Host that is running the lisp in this buffer.  Buffer local.")

(make-variable-buffer-local (quote fi::lisp-host))

(defvar fi::lisp-port nil "\
Port to use in getting new listeners from remote lisp.  Buffer local.")

(make-variable-buffer-local (quote fi::lisp-port))

(defvar fi::lisp-password nil "\
Password to use in getting new listeners from remote lisp.  Buffer local.")

(make-variable-buffer-local (quote fi::lisp-password))

(defvar fi::lisp-ipc-version nil "\
The version of the IPC to which will connect. Buffer local")

(make-variable-buffer-local (quote fi::lisp-ipc-version))

(defvar fi::lisp-is-remote nil "\
Non-nil if the lisp process tied to the current buffer is on another
machine, which implies that it was started via an `rsh'.  This variable is
buffer local.")

(make-variable-buffer-local (quote fi::lisp-is-remote))

(defvar fi::rsh-command nil)

(defvar fi::rsh-args nil)

(defun fi::start-backdoor-interface (proc) (byte-code "� ��	!��� �" [fi::common-lisp-backdoor-main-process-name proc fi:verify-emacs-support process-name fi::reset-metadot-session] 4))

(defun fi:common-lisp (&optional buffer-name directory image-name image-args host) "\
Create a Common Lisp subprocess and put it in buffer named by
BUFFER-NAME, with default-directory of DIRECTORY, using IMAGE-NAME
and IMAGE-ARGS as the binary image pathname and command line
arguments, doing the appropriate magic to execute the process on HOST.

The first time this function is called and when given a prefix argument, all
the above quantities are read from the minibuffer, with defaults coming
from the variables:
	fi:common-lisp-buffer-name
	fi:common-lisp-directory
	fi:common-lisp-image-name
	fi:common-lisp-image-arguments
	fi:common-lisp-host
and the values read are saved in these variables for later use as defaults.
After the first time or when no prefix argument is given, the defaults are
used and no information is read from the minibuffer.

For backward compatibility, BUFFER-NAME can be a number, when called
programmatically, which means look for, and use if found, numbered buffers
of the form \"*common-lisp*<N>\" for N > 2.  If BUFFER-NAME < 0, then find
the first \"free\" buffer name and start a subprocess in that buffer." (interactive (byte-code "�		�!� �
\"� �
!)� �& � &�" [fi::common-lisp-first-time fi:common-lisp-buffer-name name current-prefix-arg fi:common-lisp-directory default-directory fi:common-lisp-image-name fi:common-lisp-image-arguments fi:common-lisp-host fi::get-lisp-interactive-arguments numberp fi::buffer-number-to-buffer get-buffer-create system-name] 12)) (byte-code "ʈ	� ʂ% ��!� ܂\" ��!� ݂\" ��!�	��4 ��!�4 ��!?�< ʂ@ ʉ�t�J �P �P 	t�] �!�c �c t�n �u �u t�� �� �� t�� �� �� �� � ��\"�� �� \"�� ��\"�!� ?�� �� �!=*�� ��\"\"�� ����\"�� ��\"�� ���\"��\"�&����\"�$	�.�8�$���&���.	�" [fi::common-lisp-first-time fi:common-lisp-buffer-name name current-prefix-arg fi:common-lisp-directory default-directory fi:common-lisp-image-name fi:common-lisp-image-arguments fi:common-lisp-host fi::rsh-command nil t fi::shell-buffer-for-common-lisp-interaction-host-name buffer-name directory image-name image-args host local real-args fi:start-lisp-interface-arguments buffer fi::*connection* fi:use-background-streams startup-message proc fi:common-lisp-prompt-pattern fi::command-exists-p "remsh" "rsh" error "can't find the rsh command in your path" y-or-n-p "A make-dist might be in progress.  Continue? " "fi:common-lisp aborted." expand-file-name system-name string= "localhost" fi::calc-buffer-name "common-lisp" get-buffer fi::lep-open-connection-p fi::connection-buffer append funcall concat "
==============================================================
" format "Starting image `%s'
" "  with arguments `%s'
" "  with no arguments
" "  in directory `%s'
" "  on machine `%s'.
" "
" fi::make-subprocess fi:inferior-common-lisp-mode fi::remote-lisp-args fi::common-lisp-subprocess-filter fi::start-backdoor-interface (lambda (local host dir) (byte-code "� �pq�
�)�  �pq�ĉ�
����Ǐ)�" [local fi::lisp-host host fi::lisp-is-remote t nil (byte-code "�!�" [dir cd] 2) ((error (byte-code "��" [nil] 1)))] 3))] 37))

(defun fi:open-lisp-listener (&optional buffer-number buffer-name setup-function) "\
Open a connection to an existing Common Lisp process, started with the
function fi:common-lisp, and create a Lisp Listener (a top-level
interaction).  The Common Lisp can be either local or remote.  The name of
the buffer is \"*lisp-listener*\" with an optional suffix of \"<N>\", for
prefix arguments > 1.  If a negative prefix argument is given, then the
first \"free\" buffer name is found and used.  When called from a program,
the buffer name is the second optional argument." (interactive "p") (byte-code "?� ��!!?� ��!�	�  ̉���!!��, ���!�!�!�!	&	*�" [fi::common-lisp-backdoor-main-process-name setup-function nil buffer proc buffer-name buffer-number fi:common-lisp-prompt-pattern fi:process-running-p get-process error "Common Lisp must be running to open a lisp listener." fi::setup-tcp-connection process-buffer fi::make-tcp-connection "lisp-listener" fi:lisp-listener-mode fi::get-buffer-host fi::get-buffer-port fi::get-buffer-password fi::get-buffer-ipc-version] 19))

(defun fi::setup-tcp-connection (proc) (byte-code "����!#�" [proc format "(progn
      (setf (getf (mp:process-property-list mp:*current-process*) %s) %d)
      (values))
" :emacs-listener-number fi::tcp-listener-generation] 5))

(defun fi:franz-lisp (&optional buffer-name directory image-name image-args host) "\
Create a Franz Lisp subprocess and put it in buffer named by
BUFFER-NAME, with default-directory of DIRECTORY, using IMAGE-NAME
and IMAGE-ARGS as the binary image pathname and command line
arguments, doing the appropriate magic to execute the process on HOST.

The first time this function is called and when given a prefix argument, all
the above quantities are read from the minibuffer, with defaults coming
from the variables:
	fi:franz-lisp-buffer-name
	fi:franz-lisp-directory
	fi:franz-lisp-image-name
	fi:franz-lisp-image-arguments
	fi:franz-lisp-host
and the values read are saved in these variables for later use as defaults.
After the first time or when no prefix argument is given, the defaults are
used and no information is read from the minibuffer.

For backward compatibility, the buffer-name can be a number, when called
programmatically, which means look for, and use if found, numbered buffers
of the form \"*franz-lisp*<N>\" for N > 2.  If BUFFER-NAME < 0, then find
the first \"free\" buffer name and start a subprocess in that buffer." (interactive (byte-code "�		�!� �
\"� �
!)� �& � &�" [fi::franz-lisp-first-time fi:franz-lisp-buffer-name name current-prefix-arg fi:franz-lisp-directory default-directory fi:franz-lisp-image-name fi:franz-lisp-image-arguments fi:franz-lisp-host fi::get-lisp-interactive-arguments numberp fi::buffer-number-to-buffer get-buffer-create system-name] 12)) (byte-code "ʈ	� ʂ% ��!� ւ\" ��!� ׂ\" ��!�	�t�/ �5 �5 	t�B �!�H �H t�S �Z �Z t�e �l �l t�w �~ �~ ��\"�� �� \"������ �� 	�� �� �$���&�!�����.�" [fi::franz-lisp-first-time fi:franz-lisp-buffer-name name current-prefix-arg fi:franz-lisp-directory default-directory fi:franz-lisp-image-name fi:franz-lisp-image-arguments fi:franz-lisp-host fi::rsh-command nil t buffer-name directory image-name image-args host local proc fi:franz-lisp-prompt-pattern fi:franz-lisp-process-name fi::command-exists-p "remsh" "rsh" error "can't find the rsh command in your path" expand-file-name string= "localhost" system-name fi::make-subprocess "franz-lisp" fi:inferior-franz-lisp-mode fi::remote-lisp-args (lambda (local dir) (byte-code "� �� ��Ï�" [local nil (byte-code "�!�" [dir cd] 2) ((error (byte-code "��" [nil] 1)))] 3)) process-name] 22))

(defun fi::remote-lisp-args (host image-name image-args directory) (byte-code "�	������
!�\"� ΂ �� ���#��
!�\"�, ΂- �&F\"�" [fi::rsh-args host fi::rsh-command directory image-name image-args append "sh" "-c" format "%s%s if test -d %s; then cd %s; fi; %s %s%s" fi:member-equal file-name-nondirectory ("remsh" "rsh") "'" "" fi::env-vars mapconcat (lambda (x) (byte-code "� ��Q� " [x "\"" ""] 3)) " " ("remsh" "rsh")] 21))

(defun fi::get-lisp-interactive-arguments (first-time buffer-name buffer directory image-name image-args host) (byte-code "	� �	!� ��( �( 	?�( �	!?�( ��	!!?�� ���\"���\"����\"�E �� \"������R �Y 
$!�G�ZHU�p �t �P�)��

�$	��	\"�� �	!�� 	)������#\"!�%�� �
%*�" [buffer-name buffer local first-time current-prefix-arg host dir directory default-directory file-name image-name nil image-args get-buffer-process fi:process-running-p list read-buffer "Buffer: " read-string "Host: " string= "localhost" system-name expand-file-name read-file-name "Process directory: " 47 1 "/" "Image name: " string-match "[$~]" fi::listify-string read-from-minibuffer "Image arguments (separate by spaces): " mapconcat concat " "] 25))

(defun fi::common-lisp-subprocess-filter (process output &optional stay cruft) (byte-code "�ȍ	�=� �
$��
�\"�8 :�# �@=�2 ��A\"���!�8 �
$))�" [val T$$_0 process output stay cruft t cl-subproc-filter-foo (byte-code "�!�" [process fi::common-lisp-subprocess-filter-1] 2) normal fi::subprocess-filter set-process-filter error message "%s" fi::switch-to-buffer "*Help*"] 9))

(defun fi::common-lisp-subprocess-filter-1 (process) (byte-code "��!q�� ?�| ��	\"� ��	\"�| 	��!��!O	��!��!OP	��!��!O����\"�@���A\"�@����!A\"�@�	���؏��j ň��ڏ�
�
����ݏ,)�" [process output res command xx nil host fi::lisp-port fi::lisp-password fi::lisp-case-mode fi::lisp-ipc-version process-buffer fi::lep-open-connection-p fi::fast-search-string 1 string-match "\\([^ ]*\\)\\(.*\\)\\([^ ]*\\)" match-beginning match-end 3 2 read-from-string downcase (byte-code "��	!A\"�@�" [xx command read-from-string fi::frob-case-from-lisp] 4) ((error (byte-code "��" [nil] 1))) (byte-code "A	G=?� ��	!A\"�@�" [xx command read-from-string fi::frob-case-from-lisp] 4) ((error (byte-code "��" [nil] 1))) condition (byte-code ":� ��\"� � �!����\"�" [fi:start-lisp-interface-hook mapcar funcall throw cl-subproc-filter-foo normal] 5) ((error (byte-code "���B\"�" [condition throw cl-subproc-filter-foo error] 4)))] 17))

(defun fi::make-subprocess (startup-message process-name buffer-name directory mode-function image-prompt image-file image-args &optional filter initial-func mode-hook &rest mode-hook-arguments) (byte-code "	;�	 �
	\"�\"�!� �!�!�!�!�?�i 	:�F �	!�;?�C ��!�V 	;?�P ��	\"��	!��?�a �	�HU�i �	!���s ɂw ��)���㏈db�.�" [remote image-file fi::rsh-command buffer-name process-name buffer process runningp start-up-feed-name nil startup-message directory default-directory fi:subprocess-env-vars image-args filter subprocess-prompt-pattern image-prompt initial-func string= fi::calc-buffer-name get-buffer get-buffer-create get-buffer-process fi:process-running-p funcall error "image-file function didn't return a string" "image-file not a string or cons: %s" substitute-in-file-name 126 0 expand-file-name ((byte-code "�!�db�	;� 	c�
� �
!� 
���$ �!��!����E\"\"����\"��!??�J ɂO ��\"��
�X �\"��e ��!P���r �!�� ��!�db��!��`d\"���`d\"��\"�db���!`\"���菈��!���� �?�� ɂ� �\"�" [buffer startup-message directory default-directory process fi:subprocess-env-vars buffer-name image-file image-args nil filter start-up-feed-name subprocess-prompt-pattern image-prompt initial-func switch-to-buffer file-exists-p delete-process fi::set-environment apply start-process append set-process-sentinel fi::subprocess-sentinel fi:process-running-p error "Couldn't startup %s" set-process-filter fi::subprocess-filter "~/.emacs_" file-name-nondirectory sleep-for 1 insert-file-contents buffer-substring delete-region send-string set-marker process-mark (byte-code "	�#���
�*�" [saved-input-ring fi::input-ring saved-input-ring-yank-pointer fi::input-ring-yank-pointer mode-function mode-hook mode-hook-arguments apply] 4) ((error (byte-code "��" [nil] 1))) make-local-variable fi::make-subprocess-variables funcall] 24)) (byte-code "�	\"�" [fi:display-buffer-function buffer funcall] 3) ((error (byte-code "�!�" [buffer fi::switch-to-buffer] 2)))] 15))

(defun fi::calc-buffer-name (buffer-name process-name) (byte-code ";�	 � ��Q�!� �
\"� 
)�" [buffer-name t name process-name "*" numberp fi::buffer-number-to-buffer] 4))

(defvar fi::tcp-listener-table nil)

(defvar fi::tcp-listener-generation 0)

(defun fi::tcp-listener-generation (proc) (byte-code "	B
B���	\\��)�" [gen fi::tcp-listener-generation fi::tcp-listener-table proc 1] 2))

(defun fi::make-tcp-connection (buffer-name buffer-number mode image-prompt &optional given-host given-service given-password given-ipc-version setup-function) (byte-code "?� ��!���	�Q
\"�	!� �	!�!�!�, ��!!�7 �!
�B �!	�M �!�!�!�_ ΂c ��)���珈db�.	�" [fi::common-lisp-backdoor-main-process-name buffer-name buffer-number buffer default-dir default-directory process-buffer host given-host service given-service password given-password proc nil given-ipc-version fi::listener-protocol setup-function saved-input-ring fi::input-ring saved-input-ring-yank-pointer fi::input-ring-yank-pointer mode subprocess-prompt-pattern image-prompt error "A Common Lisp subprocess has not yet been started." fi::buffer-number-to-buffer "*" get-buffer get-buffer-create get-process fi::get-buffer-host fi::get-buffer-port fi::get-buffer-password get-buffer-process fi:process-running-p ((byte-code "�!�db�
���$����\"�?�$ Ȃ. ����	!\"\"�����!\"\"����
\"\"�?�L ȂT ��\"\"�db���!`\"��!����*���!���� �" [buffer default-directory default-dir proc buffer-name host service given-ipc-version nil fi::listener-protocol password setup-function saved-input-ring fi::input-ring saved-input-ring-yank-pointer fi::input-ring-yank-pointer mode subprocess-prompt-pattern image-prompt switch-to-buffer fi::open-network-stream set-process-sentinel fi::tcp-sentinel process-send-string format "%s
" fi::prin1-to-string "\"%s\"
" "%d
" funcall set-marker process-mark make-local-variable fi::make-subprocess-variables] 19)) (byte-code "�	\"�" [fi:display-buffer-function buffer funcall] 3) ((error (byte-code "�!�" [buffer fi::switch-to-buffer] 2)))] 16))

(defun fi::subprocess-sentinel (process status) (byte-code "� ��	\"?� @ ��� \"?�  �� \"?�( 3 ��� � %����� &)�Ƈ" [fi::lisp-is-remote status nil extra fi::lisp-host fi::rsh-command t string-match "exited abnormally" "" string= system-name format "

It appears that the host you gave to fi:common-lisp (%s) was meant to
be %s--the latter name is how this host is known to GNU Emacs.
If you type:

    M-x fi:common-lisp RET

and answer \"%s\" to the \"Host: \" question, the %s will
probably succeed." fi:error "
It appears that %s to host %s exited abnormally.
This is probably due to the host you specified to fi:common-lisp (%s)
being inaccessible.  Check that 

    %s%% rsh %s date

works--if it does not, then fi:common-lisp will fail.%s"] 17))

(defun fi::tcp-sentinel (process status) (byte-code "�!q�db���!c���" [process t process-buffer format "
---------------------------------------------------------------------
"] 3))

(make-variable-buffer-local (quote fi::subprocess-filter-output-preprocess-hook))

(setq-default fi::subprocess-filter-output-preprocess-hook nil)

(make-variable-buffer-local (quote fi::subprocess-filter-insert-output-hook))

(setq-default fi::subprocess-filter-insert-output-hook nil)

(defun fi::subprocess-filter (process output &optional stay cruft) "\
Filter output from processes tied to buffers.
This function implements continuous output to visible buffers." (byte-code "�
� ��\"��?� ł �\"��p�!=	�!

?�> �
!?�!�!�R �Y �� �#�!G���	?�� q��� `�� �
!��� `��Y���� \\�� ���b��� �#�� �!��`\")�	?��� �� �� b�� �
\"�� �� ��� �
\"�b�	�ł��q.)�" [inhibit-quit t cruft output fi::subprocess-filter-output-preprocess-hook nil old-buffer buffer process in-buffer window-of-buffer no-window xmarker marker marker-point output-length old-point point-not-before-marker new-point fi::subprocess-filter-insert-output-hook fi:subprocess-continuously-show-output-in-visible-buffer stay fi::substitute-chars-in-string ((13)) funcall process-buffer get-buffer-window windowp process-mark marker-position set-marker make-marker 0 window-point insert-string set-window-point] 18))

(defun fi::buffer-number-to-buffer (name number) (byte-code "��\"� ��!��!O��
�V�  �
�R�W 
�W�V ����!?�7 	�R ω����R�!�P �\\���; �	*�W �	!�a �	!)�" [name buffer-name number n t string-match "^\\(.*\\)<[0-9]+>$" match-beginning 1 match-end "<" ">" 0 nil fi:process-running-p 2 get-buffer get-buffer-create] 9))

(defun fi::make-subprocess-variables nil (byte-code "	��É�ǉ�� ��� ��" [fi::input-ring-max fi:default-input-ring-max fi::shell-directory-stack nil fi::last-input-search-string fi::last-input-start fi::last-input-end "" make-marker] 4))

(defvar fi::cdpath nil)

(defun fi::subprocess-watch-for-special-commands nil "\
Watch for special commands like, for example, `cd' in a shell.  We grok
the `cdpath' C shell environment variable, if you add the line

	setenv CDPATH \"$cdpath\"

to your `.cshrc' after the `set cdpath=(...)' in the same file." (byte-code "?�	 	C����ď�" [fi::shell-directory-stack default-directory nil (byte-code "���b�� �!�N ��!b���!�& ��!�& ��!�6 ���!��!\"��F ��!�F ���!��!\"���� !���W �!�� ��!b���!�� ��!�v ����!��!\"!@?�� 	A�	@��� 	GV�� ��!�� ��\\	\"
���Z	\"�\"��	
\"�	))���� �!����!b���!����!�-����!��!\"!@�W�� ��\"�)	GV���!�)�	\"���Z	\"�\"��	\"�	�@�))����!�^���!��!\"��\"�N��!!�P���*����!��	G�W�r��!���	A@	@D	AA\"�	�	@������!����!b���!����!���	��!\"�����!�����!��!\"��\"����!!������\"�������*)�?������!�7�!��
=�	B�	�4�
=�$�	\"�4�
=�4�	�	@\"\"���@��E� �����X?����@#���!?�r����!��
=��	B�	���
=���	\"���
=���	�	@\"\"�ǉ�A���P+*�" [directory nil directory-stack fi::last-input-start fi:in-package-regexp fi:package fi:shell-popd-regexp t n fi::shell-directory-stack tail fi:shell-pushd-regexp head xdir dir fi:shell-cd-regexp fi::cdpath cdpath done looking-at match-end 0 "[ 	]*[':]\\(.*\\)[ 	]*)" "[ 	]*\"\\(.*\\)\"[ 	]*)" "[ 	]*\\(.*\\)[ 	]*)" buffer-substring match-beginning 1 "[ 	]+\\(.*\\)[ 	]*$" set-buffer-modified-p buffer-modified-p ".*&[ 	]*$" "[ 	]+\\+\\([0-9]*\\)" read-from-string message "Directory stack not that deep." nthcdr rplacd append "[ 	]+\\+\\([0-9]+\\)[ 	]*[;
]" "Illegal stack element: %s" "[ 	]+\\([^ 	]+\\)[;
]" string-match "[\\$~]" expand-file-name substitute-in-file-name push "[ 	]*[;
]" 2 rplaca getenv "HOME" "[ 	]+\\([^ 	]+\\)[ 	]*[;
]" "^/" replace file-directory-p cd fi::new-directory fi::listify-cdpath format "%s/%s"] 81) ((error (byte-code "��" [nil] 1)))] 3))

(defun fi::new-directory (old new) (byte-code "��\"� 	�) ��\"� ��	!!�) 	��	\"�& Â' �Q�" [new old t nil string= "." ".." file-name-directory directory-file-name string-match "/$" "/"] 8))

(defun fi::listify-cdpath nil (byte-code "���!���$��\")�" [cdpath fi::shell-command-output-to-string get-buffer-create " *cdpath parsing*" "csh" "-c" "echo $cdpath" fi::explode 32] 6))

(defun fi::get-buffer-host (buffer) "\
Given BUFFER return the value in this buffer of fi::lisp-host." (byte-code "�q�	)�" [buffer fi::lisp-host] 1))

(defun fi::get-buffer-port (buffer) "\
Given BUFFER return the value in this buffer of fi::lisp-port." (byte-code "�q�	)�" [buffer fi::lisp-port] 1))

(defun fi::get-buffer-password (buffer) "\
Given BUFFER returns the values in this buffer of fi::lisp-password" (byte-code "�q�	)�" [buffer fi::lisp-password] 1))

(defun fi::get-buffer-ipc-version (buffer) "\
Given BUFFER returns the values in this buffer of fi::lisp-password" (byte-code "�q�	)�" [buffer fi::lisp-ipc-version] 1))

(defun fi::env-vars nil (byte-code "���#����#�R�" [fi:subprocess-env-vars mapconcat (lambda (x) (format "%s=%s" (car x) (eval (eval (cdr x))))) " " " export " (lambda (x) (car x)) "; "] 7))

(defun fi::set-environment-use-setenv (valist) (byte-code "��
�& 
@���A!� ŉ��@	\"�
A��� *�" [item val valist nil eval "" setenv] 5))

(defun fi::set-environment-use-process-environment (valist) (byte-code "	�\\ �?� 
�= ��@@�Q
@\"�5 �
��@@�@A!#\"�Ɖ�9 
A���
 �?�R ��@@�@A!#B�*�A��� )�" [v valist pe process-environment found nil t string-match "^" "=" rplaca format "%s=%s" eval] 10))

(fset (quote fi::set-environment) (if (boundp (quote process-environment)) (symbol-function (quote fi::set-environment-use-process-environment)) (symbol-function (quote fi::set-environment-use-setenv))))

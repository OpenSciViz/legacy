;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; ---------------------------------------------------------------------
;; user variables

(defparameter *n* 1000000)		; default number of iterations
(defparameter *num-shades* 64)		; number of colors to use     
(defparameter *dot-size* 4)		; width/height in pixels of each block
(defparameter *width* 100)		; number of blocks wide
(defparameter *height* 100)		; number of blocks high
(defparameter *gray-p* nil)		; gray-levels or multi-color
(defparameter *ridge-mode* nil)		; if gray-p, to use smooth shading
					; up to abrupt black-to-white ridges,
                                        ; or instead be all smooth
(defparameter *bitblt-frequency* 1000)	; if *dot-size* is 1 and *bitblt-mode*
					; is non-NIL, how often to
					; blit result to screen
(defparameter *bitblt-mode* t)		; if *dot-size* is 1, whether to blit
					; result to screen occaissionally
					; rather than drawing every block
					; on the screen as it's computed
                                        ; ??? does bitblt-mode work?
(defparameter *print-frequency* nil)    ; nil to turn off counter printing,
					; or e.g. 1000 to print the counter
                                        ; after every 1000 iterations
(defparameter *print-file* nil)         ; file to print count to (nil=screen)


;; ---------------------------------------------------------------------
;; internal variables.
(defparameter *half-num-shades* (floor *num-shades* 2))
(defparameter *bitblt-count* *bitblt-frequency*)
(defparameter *counter* 0)
(defvar *ar* (make-array `(,*width* ,*height*)))
(defvar *win* nil)
(defvar *bitmap* nil)
(defvar *colors* (make-array *num-shades* :initial-element nil))
(defvar *color-list* nil)

;; ---------------------------------------------------------------------
;; "furl" is the user function to call.
;; Pass no-reset-p to continue building previous design.
;; n is the number of iterations (number of pixels to change) on this call.
;; n defaults to *n*.

(defun furl (&optional no-reset-p n)
  (unless (aref *colors* 0)
    (setq *color-list* nil)
    (dotimes (j *num-shades*)
      (setf (aref *colors* j)
	(if *gray-p*
	    (make-color :level
			(min 1.0 (if *ridge-mode* 
				     (* j (/ 1.0 (1- *num-shades*)))
				   (* (- *num-shades*
					 (abs (- *num-shades* (* 2 j))))
				      (/ 1.0 *num-shades*)))))
	  (make-color :hue (min 1.0 (* j (/ 1.0 (1- *num-shades*))))
		      :saturation 1.0 :brightness 1.0)))
      (push (aref *colors* j) *color-list*)))
  (unless (and *win* (eq (window-stream-status *win*) :active)
	       (>= (window-stream-inner-width *win*)(* *dot-size* *width*))
	       (>= (window-stream-inner-height *win*)(* *dot-size* *height*)))
    (setq *win* (make-window-stream :activate-p t
				    :inner-width (* *dot-size* *width*)
				    :inner-height (* *dot-size* *height*))))
  (when (and *bitblt-mode* (= *dot-size* 1))
    (unless (and *bitmap* (>= (bitmap-width *bitmap*) *width*)
		 (>= (bitmap-height *bitmap*) *height*))
      (setq *bitmap* (make-bitmap :width *width* :height *height*
				  :bits-per-pixel 8))))
  (unless no-reset-p
    (dotimes (x *width*)
      (dotimes (y *height*)
	(setf (aref *ar* x y)(random *num-shades*))
	(furl-draw x y (aref *ar* x y)))))
  (dotimes (j (or n *n*))
    (when (and *print-frequency* (minusp (decf *counter*)))
      (setq *counter* (1- *print-frequency*))
      (cond (*print-file*
	     (with-open-file (s *print-file* :direction :output
			      :if-exists :supersede)
	       (format s "~a   ~a~%" (date) j)))
	    (t
	     (print j)(force-output))))
    (let ((x (random *width*))
	  (y (random *height*)))
      (setf (aref *ar* x y)
	(conform-bit *ar* x y *width* *height* *num-shades*))
      (furl-draw x y (aref *ar* x y)))))
	    
;; another user function to rotate colors after furl has run a while.
(defun furl-rotate (&optional reverse-p)
  (dotimes (j *num-shades*)
    (rotate-colors *color-list* (if reverse-p -1 1))))

(defun furl-draw (x y value)
  (cond ((and *bitblt-mode* (= *dot-size* 1))
	 (cw::wsys-set-bitmap-bit *bitmap* x y value)
	 (when (zerop (decf *bitblt-count*))
	   (bitblt *bitmap* 0 0 *win* 0 0 *width* *height* boole-1)
	   (setq *bitblt-count* *bitblt-frequency*)))
	(t
	 (draw-filled-rectangle-xy
	  *win* (* *dot-size* x)(* *dot-size* y) *dot-size* *dot-size*
	  :color (aref *colors* value)))))

(defparameter *neighbors* '#(0 0 0 0 0 0 0 0))
(defparameter *num-neighbors* (length *neighbors*))

(defun conform-bit (array x y width height num-values)
  (let ((j -1))
    (dotimes (ox 3)
      (dotimes (oy 3)
	(unless (and (= ox 1)(= oy 1))
	  (setf (aref *neighbors* (incf j))
	    (aref array
		  (mod (+ x ox -1) width)
		  (mod (+ y oy -1) height)))))))
  (sort *neighbors* #'<)
  (do ((j 0 (1+ j))
       (start (random *num-neighbors*))
       (biggest-dif -1)
       index dif bottom)
      ((> j (1- *num-neighbors*))
       (mod (+ bottom (floor biggest-dif 2)(floor num-values 2))
	    num-values))
    (setq index (mod (+ start j) *num-neighbors*))
    (setq dif (- (if (= index (1- *num-neighbors*))
		     (+ (aref *neighbors* 0) num-values)
		   (aref *neighbors* (1+ index)))
		 (aref *neighbors* index)))
    (when (> dif biggest-dif)
      (setq biggest-dif dif)
      (setq bottom (aref *neighbors* index)))))

(defun date ()
  (multiple-value-bind (second minute hour date month year day)
      (get-decoded-time)
    (format nil "~:(~a-~a-~a ~2,'0d:~2,'0d:~2,'0d (~a)~)"
	    year (nth month '(nil jan feb mar apr may jun
			      jul aug sep oct nov dec))
	    date hour minute second
	    (nth day '(monday tuesday wednesday thursday
		       friday saturday sunday)))))

(format t "~%This version is for gray-scale or color machines.  ~
           ~%If gray-scale, set *gray-p* to t.  ~
           ~%Call (furl) to run; see source for more info.")

;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(defun test (w &optional color)
  (dotimes (j 5)
    (dotimes (k 5)
      (draw-arrow-xy w 100 100 (* j 50) (* k 50)
		     :color (or color black)))))

(defparameter shift 12)
(proclaim '(fixnum 8))

(defun draw-arrow-xy
    (window x1 y1 x2 y2
     &key (head-length-1 0)(head-length-2 15)
	  color operation brush-width dashing)
  (declare (optimize (speed 3)(safety 1))
	   (fixnum x1 y1 x2 y2 head-length-1 head-length-2
		   operation brush-width))
  (let* ((x1a x1)(y1a y1)(x2a x2)(y2a y2)
	 cos sin dx dy adx ady bdx bdy dxy)
    (declare (fixnum x1a y1a x2a y2a cos sin dx dy adx ady bdx bdy dxy))
    (setq dx (- x2 x1)
	  dy (- y2 y1))
    (setq adx (abs dx)
	  ady (abs dy))
    (setq bdx (ash dx shift)
	  bdy (ash dy shift))

    ;; this worse approximation of cos & sin gains you very little
    ;; over the isqrt call below.
;    (setq dxy (+ (max adx ady)
;		 (ash (min adx ady) -1)))

    (setq dxy (isqrt (+ (* adx adx)(* ady ady))))
    (when (plusp dxy)
      (setq cos (round bdx dxy)
	    sin (round bdy dxy))
      (with-graphics-batching
       (when (plusp head-length-1)
	 (setq x1a (+ x1 (ash (* cos head-length-1)(- shift)))
	       y1a (+ y1 (ash (* sin head-length-1)(- shift))))
	 (draw-arrowhead-xy window x1 y1 head-length-1
			    (max 4 (ash head-length-1 -1))
			    (- cos)(- sin) color operation brush-width))
       (when (plusp head-length-2)
	 (setq x2a (- x2 (ash (* cos head-length-2)(- shift)))
	       y2a (- y2 (ash (* sin head-length-2)(- shift))))
	 (draw-arrowhead-xy window x2 y2 head-length-2
			    (max 4 (ash head-length-2 -1))
			    cos sin color operation brush-width))
       (draw-line-xy window x1a y1a x2a y2a
		     :color color :operation operation
		     :brush-width brush-width :dashing dashing)))))

(defun draw-arrowhead-xy
    (window x y length width cos sin color operation brush-width)
  (declare (optimize (speed 3)(safety 1))
	   (fixnum x y length width cos sin operation brush-width))
  (let ((cos-length (ash (* cos length) (- shift)))
	(sin-length (ash (* sin length) (- shift)))	
	(cos-width (ash (* cos width)(- -1 shift)))
	(sin-width (ash (* sin width)(- -1 shift))))
    (declare (fixnum cos-length sin-length cos-width sin-width))
    (let ((x-left (- x cos-length sin-width))
	  (x-right (+ x (- cos-length) sin-width))
	  (y-left (+ y cos-width (- sin-length)))
	  (y-right (- y cos-width sin-length)))
      (declare (fixnum x-left x-right y-left y-right))
      (draw-line-xy window x y x-left y-left :color color
		    :operation operation :brush-width brush-width)
      (draw-line-xy window x-left y-left x-right y-right :color color
		    :operation operation :brush-width brush-width)
      (draw-line-xy window x-right y-right x y :color color
		    :operation operation :brush-width brush-width))))

(format t "~%This defines the following function: ~
           ~% ~
           ~%(defun draw-arrow-xy ~
           ~%  (window x1 y1 x2 y2 ~
           ~%          &key (head-length-1 0)(head-length-2 15) ~
	   ~%               color operation brush-width dashing)~%")


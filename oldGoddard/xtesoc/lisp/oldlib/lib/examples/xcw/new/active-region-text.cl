;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; Active region text example.  Demonstrates making an active region
;; out of a section of printed text, by noting the cursor position
;; just before and just after printing the portion of the text
;; that comprises the active region.  The strings inside <<...>>
;; in *text-alist* appear in a bold font inside active regions.
;; Mousing an active region prints new text associated with that
;; active region into the window.

;; NOTE:  If active region string wraps to next line, the active region
;; will cover only the part on the upper line.  A second active region
;; would be needed to cover the part that wraps to the next line.

(defpackage :user (:use :cw))
(in-package :user)

(defparameter *text-alist*
    '(("foo" "Foo" ""
	     "More important than <<bar>>, <<baz>> or <<nurp nurp>>."
	     "No entry for <<boof>>, so it should do nothing." )
      ("bar" "Bar" ""
       "Less important than <<foo>>."
       "But more important than <<baz>> and <<nurp nurp>>.")
      ("baz" "Baz" ""
       "Less important than both <<foo>> and <<bar>>."
       "But a bit more important than <<nurp nurp>>.")
      ("nurp nurp" "Nurp Nurp" "" "Totally unimportant." "" "See also:" ""
       "  <<foo>>   Real important" "  <<bar>>   Pretty important"
       "  <<baz>>   Kind of important"
       "" "" "Lots of lines here" "to show scrollability." ""
       "Are" "we" "on" "the" "next" "page" "yet?" ""
       "See also:  <<foo>>   <<bar>>   <<baz>>")
      ))

(defparameter *regular-font* (open-font :courier :roman 12))
(defparameter *bold-font* (open-font :courier :roman 12 :weight :bold))

;; Internal var; user should never set this to non-NIL.
(defparameter *create-ars-p* nil)

(defun text-setup ()
  (let* ((window (make-window-stream :left 750 :bottom 100
				     :width 300 :height 200
				     :activate-p t
				     :title "Mouse a bold word.")))
    (setf (window-stream-get window :text)(cdar *text-alist*))
    (enable-window-stream-extent-scrolling window :horizontal nil)
    (setf (window-stream-repaint window) 'ar-text-repaint)
    (modify-window-stream-method window :reshape :after 'ar-text-redo)
    (modify-window-stream-method window :reshape-notify :after 'ar-text-redo)
    (ar-text-redo window)))

(defun ar-text-repaint (window &rest ignore)
  (declare (ignore ignore))
  (clear window)
  (when *create-ars-p* (flush-window-stream-active-regions window))
  (let* ((font-height (max (font-character-height *regular-font*)
			   (font-character-height *bold-font*)))
	 (font-baseline (font-baseline *bold-font*))
	 (inner-width (window-stream-inner-width window))
	 (inner-height (window-stream-inner-height window))
	 (extent-height (window-stream-extent-height window))
	 (extent-bottom (window-stream-extent-bottom window))
	 (extent-top (+ extent-bottom extent-height))
	 text-bottom ar)
    (when *create-ars-p*
      (setf (window-stream-y-offset window)
	(- extent-top inner-height)))
    (do* ((text (window-stream-get window :text)
		(cdr text))
	  (y (- extent-top
		(max (font-ascent *regular-font*)
		     (font-ascent *bold-font*)))
	     (- (window-stream-y-position window) font-height))
	  x1 y1 x2 y2 string-width)
	((null text)
	 (when (and *create-ars-p* y2)
	   (setf (window-stream-extent-height window)
	     (- extent-top
		(setf text-bottom 
		  (min (window-stream-y-position window)
		       (- extent-top inner-height)))))
	   (setf (window-stream-extent-bottom window) text-bottom)))
      (setf (window-stream-x-position window) 0)
      (setf (window-stream-y-position window) y)
      (do* ((string (car text)(car text))
	    (start 0 (+ end 2))
	    (pos (search "<<" string :start2 start)
		 (search "<<" string :start2 start))
	    (end (if pos (search ">>" string :start2 (+ pos 2)))
		 (if pos (search ">>" string :start2 (+ pos 2)))))
	  ((null end)
	   (setf (window-stream-font window) *regular-font*)
	   (format window (subseq string start)))
	(setf (window-stream-font window) *regular-font*)
	(format window (subseq string start pos))
	(when *create-ars-p*
	  (setq x1 (window-stream-x-position window))
	  (setq y1 (window-stream-y-position window)))
	(setf (window-stream-font window) *bold-font*)
	(format window (subseq string (+ pos 2) end))
	(when *create-ars-p*
	  (setq x2 (window-stream-x-position window))
	  (setq y2 (window-stream-y-position window))
	  (setq string-width
	    (if (= y1 y2)
		(- x2 x1)
	      (- inner-width x1)))
	  (setq ar (make-active-region :parent window
				       :activate-p t
				       :left (- x1 2)
				       :width (+ string-width 4)
				       :bottom (- y1 font-baseline)
				       :height font-height))
	  (setf (active-region-get ar :string)(subseq string (+ pos 2) end))
	  (setf (active-region-button ar) '(ar-go))
	  (setf (active-region-mouse-cursor-in ar) '(ar-in))
	  (setf (active-region-mouse-cursor-out ar) '(ar-out)))
	))))

(defun ar-go (ar &rest ignore)
  (declare (ignore ignore))
  (let ((window (active-region-parent ar))
	(text (cdr (assoc (active-region-get ar :string) *text-alist*
			  :test #'string-equal)))
	(*create-ars-p* t))
    (when text
      (setf (window-stream-get window :text) text)
      (ar-text-redo window))))

(defun ar-text-redo (window &rest ignore)
  (declare (ignore ignore))
  (let ((*create-ars-p* t))
    (repaint window)))

(defun ar-in (ar &rest ignore)
  (declare (ignore ignore))
  (box-active-region ar))

(defun ar-out (ar &rest ignore)
  (declare (ignore ignore))
  (box-active-region ar))

(defun box-active-region (ar)
  (draw-rectangle-xy (active-region-parent ar)
		     (active-region-left ar)
		     (active-region-bottom ar)
		     (active-region-width ar)
		     (active-region-height ar)
		     :operation boole-xor))

(defun invert-active-region (ar)
  (complement-rectangle-xy
   (active-region-parent ar)(active-region-left ar)(active-region-bottom ar)
   (active-region-width ar)(active-region-height ar)))

(format t "~%Call (text-setup) to create the window, then mouse bold words.")

;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(in-package 'user)

(defvar *texture-window* nil)

(defparameter *texture*
    (expr-to-bitmap
     '(16 16 1 "H@@@" "H@@@" "N@@C" "CH@N" "@NCH" "@CN@" "@@H@" "@@H@"
       "@@H@" "@@H@" "@CN@" "@NCH" "CH@N" "N@@C" "H@@@" "H@@@")
     :interlisp-d))

(defun test-texture (&optional left bottom)
  (let ((win (return-demo-window
	      '*texture-window* "Feel the Textures" left bottom 240 260))
	)

    ;; Do the circle.
    (draw-filled-circle-xy win 70 70 60)
    (draw-filled-circle-xy win 70 70 45 :texture *white-bitmap*)
    (draw-circle-xy win 70 70 35 :brush-width 2)
    (draw-filled-circle-xy win 70 70 35 :texture *texture*)

    ;; Do the rectangle.
    (draw-filled-rectangle-xy win 150 10 80 60)
    (draw-filled-rectangle-xy win 160 20 60 40 :texture *texture*)

    ;; Do the hexagonal pie.
    (do* ((degrees 0 (+ degrees 60))
	  (radians (/ (* degrees 3.14159) 180)
		   (/ (* degrees 3.14159) 180))
	  (fillp nil (not fillp))
	  (cx 60)(cy 200)(size 50)
	  (back-x nil front-x)(back-y nil front-y)
	  (front-x (floor (+ cx (* size (cos radians))))
		   (floor (+ cx (* size (cos radians)))))
	  (front-y (floor (+ cy (* size (sin radians))))
		   (floor (+ cy (* size (sin radians))))))
	 ((> degrees 360))
      (when back-x
	(draw-filled-triangle-xy win cx cy back-x back-y front-x front-y
				 :texture (if fillp *texture*))
	(when fillp
	  (draw-triangle-xy win cx cy back-x back-y front-x front-y))))

    ;; Do the thing with the vertical crack through it.
    (draw-filled-polygon-xy win '(130 110 190 110 160 145 180 175
			       150 210 130 210))
    (draw-filled-polygon-xy win '(210 110 180 145 200 175 170 210
			       230 210 230 110) :texture *texture*)
    (draw-polygon-xy win '(210 110 180 145 200 175 170 210
			   230 210 230 110))
    ))

;; ------------------------------------------------------------------------
;; Utility function to create or return a window, given the symbol
;; it's stored in.

(defun return-demo-window
    (window-symbol &optional title left bottom inner-width inner-height)
  (let (window)
    (cond ((and (symbolp window-symbol)
		(symbol-value window-symbol)
		(setq window (symbol-value window-symbol))
		(window-stream-p window)
		(not (eq (window-stream-status window) :flushed)))
	   (expose window)
	   (clear window)
	   (set window-symbol window))
	  (t
	   (set window-symbol
		(make-window-stream
		 :left (or left 0) :bottom (or bottom 0)
		 :inner-width (or inner-width 400)
		 :inner-height (or inner-height 300)
		 :activate-p t :title (or title "Demo")))))))

(format t "~%Call (test-texture &optional left bottom) to run.")

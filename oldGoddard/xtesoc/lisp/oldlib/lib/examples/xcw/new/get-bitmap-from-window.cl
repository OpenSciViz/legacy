;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(in-package 'cw)

(export 'get-bitmap-from-window)
(defun get-bitmap-from-window (&optional window region to-bitmap)
  (unless window
    (setq window (get-window "Mouse a common window to blit a bitmap from.")))
  (unless region
    (setq region (get-region window :documentation
			     "Shape the area to copy.")))
  (unless to-bitmap
    (setq to-bitmap (make-bitmap :width (region-width region)
				 :height (region-height region))))
  (bitblt window (region-left region)(region-bottom region)
	  to-bitmap 0 0 (min (region-width region)(bitmap-width to-bitmap))
	  (min (region-height region)(bitmap-height to-bitmap)) boole-1)
  (documentation-print "Now bitblt the returned bitmap somewhere to see it.")
  to-bitmap)

(defun get-window (&optional (prompt "Mouse a window"))
  (documentation-print prompt)
  (get-position)
  (sleep .1)
  (setq w (window-stream-under-mouse)))

(format t "~%Call (get-bitmap-from-window) to try this out.")

;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; Generic Text Scroller for Common Windows.
;;
;; Designed for large amounts of text that are too big to save onto
;; a bitmap stream for blit-scrolling.  Text is saved as a list of
;; lines, with each line represented by this structure (e.g.):
;; #s(TEXT-LINE :TOP 925 :BOTTOM 912 :STRING "a string") 
;; The repaint method determines which lines are in the region
;; to be repainted, and sends only those lines to the server
;; to print.
;;
;;
;; The following user functions allow printing either a lisp form,
;; a single string with newlines embedded in it, a file, or a
;; list of strings (one for each line).
;;
;; show-form (window sexp &key ...)
;; show-text (window string-with-newlines-in-it &key ...)
;; show-file (window pathname &key ...)
;; show-list-of-strings (window list-of-strings &key ...)
;;
;; scroll-to-bottom (window)
;;
;;
;; Optional keyword args:
;;
;; append-p
;;    non-NIL             Append the new text to the bottom of
;;                        the existing text.
;;
;;    NIL (default)       Replace the existing text with the new text.
;;
;; wrap-p
;;    non-NIL (default)   When a line of text is too long to fit the
;;                        width of the window, wrap it to the next line.
;;
;;    NIL                 Clip the extra text off.
;;
;; scroll-p
;;    non-NIL (default)   In non-append mode, scroll to the top of the
;;                        text; in append mode, scroll to the bottom
;;                        of the text.
;;
;;    NIL                 Leave the window scrolled where it was.
;;
;; print-ahead-p
;;    non-NIL             Initially print all the text that lies both
;;                        off and on the window, so that all the wrapped
;;                        lines are counted and the extent height is
;;                        computed exactly, which allows immediate
;;                        scrolling to the bottom of the text.
;;                        NOTE:  Print-ahead ALWAYS happens in append
;;                        mode, due to the presumtion that appended text
;;                        is reasonably small.
;;
;;    NIL (default)       Initially print only the visible lines, which
;;                        saves time with large amounts of text.  The
;;                        extent is calculated as if there are no wrapped
;;                        lines, and then increased as wrapped lines are
;;                        printed.  May require multiple middle clicks
;;                        to scroll to the bottom, if there are in fact
;;                        some wrapped lines.
;; 
;; --------------------------------------------------------------------
;; user functions

(defun show-form
    (window sexp
     &key (append-p nil)(wrap-p t)(scroll-p t)(print-ahead-p nil))
  (show-text window
	     (with-output-to-string (s)(pprint sexp s))
	     :append-p append-p :wrap-p wrap-p
	     :scroll-p scroll-p :print-ahead-p print-ahead-p))

(defun show-text
    (window string-with-newlines-in-it
     &key (append-p nil)(wrap-p t)(scroll-p t)(print-ahead-p nil))
  (show-list-of-strings
   window (make-string-list-from-text string-with-newlines-in-it)
   :append-p append-p :wrap-p wrap-p
   :scroll-p scroll-p :print-ahead-p print-ahead-p))

(defun show-file
    (window pathname
     &key (append-p nil)(wrap-p t)(scroll-p t)(print-ahead-p nil))
  (with-open-file (s pathname)
    (do ((strings nil (nconc strings (list (remove-tabs string))))
	 (string (read-line s nil :eof)
		 (read-line s nil :eof)))
	((eq string :eof)
	 (show-list-of-strings
	  window strings
	  :append-p append-p :wrap-p wrap-p
	  :scroll-p scroll-p :print-ahead-p print-ahead-p)))))

(defun show-list-of-strings
    (window list-of-strings
     &key (append-p nil)(wrap-p t)(scroll-p t)(print-ahead-p nil))
  (if append-p
      (append-list-of-strings window list-of-strings wrap-p scroll-p)
    (show-list-of-strings-2
     window list-of-strings wrap-p scroll-p print-ahead-p)))

(defun scroll-to-bottom (window)
  (print-rest-ahead window)
  (let* ((y-offset (window-stream-extent-bottom window))
	 (y-offset-max (- (+ y-offset
			     (window-stream-extent-height window))
			  (window-stream-inner-height window))))
    (setf (region-bottom scroll-region)(min y-offset y-offset-max))
    (scroll window scroll-region)))

;; --------------------------------------------------------------------
;; non-user functions

;; Normally the extent height is computed assuming that no text lines
;; wrap around, taking up more than one line of space.  The extent is
;; incrementally increased as more lines are printed to the window and
;; wrapped lines are discovered.  This means you sometimes have to
;; middle click multiple times to make it really scroll to the bottom.
;; To avoid this, you can use with-print-ahead to make it initially 
;; print all the lines that lie both on and off the window, so that
;; it knows the exact extent height.  Then you can scroll immediately
;; to the bottom of the text.
(defmacro with-print-ahead (window &body body)
  `(unwind-protect
       (progn
	 (setf (window-stream-get ,window :print-ahead-p) t)
	 ,@body)
     (setf (window-stream-get ,window :print-ahead-p) nil)))

(defstruct text-line
  (top nil)
  (bottom nil)
  (string nil))

(defun print-rest-ahead (window)
  (with-print-ahead window (text-repaint window nil t)))

(defun show-list-of-strings-2
    (window list-of-strings wrap-p scroll-p print-ahead-p)
  (setf (window-stream-get window :wrap-p) wrap-p)
  (setf (window-stream-get window :text) 
    (mapcar #'make-text-line-from-string list-of-strings))
  (setf (window-stream-extent-bottom window) 0)
  (setf (window-stream-extent-height window)
    (* (length list-of-strings)
       (font-character-height (window-stream-font window))))
  (enable-window-stream-extent-scrolling window :vertical t :horizontal nil)
  (unless (member 'text-repaint (window-stream-repaint window))
    (modify-window-stream-method window :repaint :before 'text-repaint))
  (when scroll-p
    (setf (window-stream-y-offset window)
      (- (window-stream-extent-height window)
	 (window-stream-inner-height window))))
  (if print-ahead-p
      (with-print-ahead window (repaint window))
    (repaint window)))

(defun append-list-of-strings (window list-of-strings wrap-p scroll-p)
  (setf (window-stream-get window :wrap-p) wrap-p)
  (let ((added-height (* (length list-of-strings)
			 (font-character-height
			  (window-stream-font window)))))
    (setf (window-stream-get window :text) 
      (nconc (window-stream-get window :text)
	     (mapcar #'make-text-line-from-string list-of-strings)))
    (decf (window-stream-extent-bottom window) added-height)
    (incf (window-stream-extent-height window) added-height)
    (if scroll-p
	(scroll-to-bottom window)
      (print-rest-ahead window))))

(defparameter scroll-region (make-region :left 0))

;; The repaint method --- determines which text lines are in the
;; damaged region and sends only those lines to the server for
;; printing.
(defun text-repaint (window &optional region draw-new-only-p)
  (setf (window-stream-clipping-region window) region)

  ;; This is needed for wrapping after reshaping --- should it be needed?
  (setf (window-stream-right-margin window)
    (if region
	(region-right region)
      (window-stream-inner-width window)))

  (if region
      (clear-area window region)
    (unless draw-new-only-p (clear window)))
  (do* ((font (window-stream-font window))
	(font-height (font-character-height font))
	(font-ascent (font-ascent font))
	(font-baseline (font-baseline font))
	(extent-height (window-stream-extent-height window))
	(extent-bottom (window-stream-extent-bottom window))
	(y-offset (window-stream-y-offset window))
	(region-top (if region
			(region-top region)
		      (+ y-offset (window-stream-inner-height window))))
	(region-bottom (if region
			   (region-bottom region)
			 (max extent-bottom y-offset)))
	(print-bottom (- region-bottom font-ascent))
	(strings (window-stream-get window :text)
		 (cdr strings))
	(top-y (- (+ extent-bottom extent-height) font-ascent))
	(y top-y)
	(new-extent-height nil)
	(print-ahead-p (window-stream-get window :print-ahead-p))
	(wrap-p (window-stream-get window :wrap-p))
	line)
      ((or (null strings)
	   (and (not print-ahead-p)(< y print-bottom)))
       (when new-extent-height
	 (setf (window-stream-extent-height window) new-extent-height)
	 (decf (window-stream-extent-bottom window)
	       (- new-extent-height extent-height)))
       (force-output)
       )
    (setq line (car strings))
    (unless (and (text-line-top line)
		 (text-line-bottom line)
		 (> (text-line-bottom line) region-top))
      (unless (text-line-top line)
	(setf (text-line-top line)(+ y font-ascent)))
      (unless (and draw-new-only-p
		   (text-line-bottom line))
	(setf (window-stream-y-position window) y)
	(setf (window-stream-x-position window) 0)
	(if wrap-p
	    (princ (text-line-string line) window)
	  (draw-string-xy window 0 y (text-line-string line))))
      (unless (text-line-bottom line)
	(setf (text-line-bottom line)
	  (- (window-stream-y-position window) font-baseline))
	(setf new-extent-height
	  (+ (- top-y (window-stream-y-position window))
	     (* font-height (length strings))))))
    (force-output)
    (setf y (- (+ (text-line-bottom line) font-baseline) font-height)))
  (force-output window)
  (when region
    (setf (window-stream-clipping-region window) nil)))

(defun make-text-line-from-string (string)
  (make-text-line :string string))

;; Convert a string-with-newlines-in-it into a list of strings.
(defun make-string-list-from-text (text)
  (do* ((end (length text) start)
	(start (position #\newline text :end end :from-end t)
	       (position #\newline text :end end :from-end t))
	(string (subseq text (1+ (or start -1)) end)
		(subseq text (1+ (or start -1)) end))
	(strings (list string)(cons string strings))
	)
      ((null start)
       strings)))

;; I would just like to say at this time that I hate tabs.
;; This is a hack for printing files with tabs --- it substitues spaces.
(defun remove-tabs (string &optional (tab-length 8))
  (let ((num-tabs (count #\tab string))
	new-string)
    (when (zerop num-tabs)(return-from remove-tabs string))
    (setq new-string (make-string (+ (length string)
				     (* (1- tab-length) num-tabs))))
    (do* ((from-start 0 (1+ pos))
	  (pos (or (position #\tab string :start from-start)
		   (length string))
	       (or (position #\tab string :start from-start)
		   (length string)))
	  (to-start 0 (* (floor (+ to-start len tab-length)
				tab-length)
			 tab-length))
	  (len (- pos from-start)
	       (- pos from-start))
	  (j 0 (1+ j)))
	((= j num-tabs)
	 (replace new-string string :start1 to-start
		  :start2 from-start :end2 pos)
	 (subseq new-string 0 (+ to-start len)))
      (replace new-string string :start1 to-start
	       :start2 from-start :end2 pos)
      (unless (= j num-tabs)
	(replace new-string "        "
		 :start1 (+ to-start len))))))

;; Temporary function to show scroll status of a window for debugging.
(defun st (w)
  (format t "~%~%extent bottom      ~a~
               ~%extent height      ~a~
               ~%y offset           ~a~
               ~%inner-height        ~a~
               ~%num strings        ~a~
               "
	  (window-stream-extent-bottom w)
	  (window-stream-extent-height w)
	  (window-stream-y-offset w)
	  (window-stream-inner-height w)
	  (length (window-stream-get w :text))
	  ))

(format t "~%Calling forms: ~
           ~% ~
           ~%(show-form some-window lisp-form) ~
           ~%(show-text some-window string-with-newlines-in-it) ~
           ~%(show-file some-window pathname) ~
           ~%(show-list-of-strings some-window list-of-strings) ~
           ~% ~
           ~%See source for keyword args.")


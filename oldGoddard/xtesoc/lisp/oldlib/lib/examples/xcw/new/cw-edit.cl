;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(defpackage :cw-edit (:use :cw :lisp :excl))
(in-package :cw-edit)

;; Simple example editor written in common windows.
;; Works only with a fixed-spaced font, and has no scrolling, line-wrap, etc.

;; Current features: 
;; forward-char, backward-char, forward-line, backward-line either with
;;   emacs commands or arrow keys.
;; forward-word, backward-word (no wrap to other lines)
;; start-of-line, end-of-line
;; insert-char, delete-char (with rubout, backspace, or ^d)
;; insert-line, kill-line
;; exit with ^z

;; Call cw-edit to run.  Initial-string may have #\newline's in it.
;; (defun cw-edit (window &optional (columns 40)(rows 8)(initial-string ""))

;; Another fine Cheeprog --- software it counts.

(export 'cw-edit)
(defun cw-edit (window &key columns rows initial-string cursor-mode)
  (let ((font (window-stream-font window)))
    (setf (window-stream-pageable-p window) nil)
    (setf rows (min (or rows 1000)
		    (floor (window-stream-inner-height window)
			   (font-character-height font))))
    (setf columns (min (or columns 1000)
		       (floor (window-stream-inner-width window)
			      (font-character-width font))))
    (setf (window-stream-get window :old-got-char)
      (window-stream-got-character window))
    (setf (window-stream-got-character window) '(edit-got-char))

    (when (fboundp 'user::remove-drawn-cursor)
      (user::remove-drawn-cursor window))
    (setup-buffer window columns rows (or initial-string ""))
    (reset window)

    ;; Load the "drawn-cursor" example to enable a special drawn
    ;; cursor that inverts the area it's over rather than obscuring it.
    (if (fboundp 'user::add-drawn-cursor)
	(user::add-drawn-cursor window cursor-mode)
      (cw::put-up-text-input-cursor window))

    (setf (window-stream-get window :editing-p) t)
    (setf (window-stream-get window :edit-rows) rows)
    (setf (window-stream-get window :edit-columns) columns)
    (setf (window-stream-get window :edit-column) 0)
    (setf (window-stream-get window :edit-row) 0)
    (do ()((not (window-stream-get window :editing-p)))(sleep .2))
    (return-edited-string window)))

;; Dispatch a typed character as an emacs-like command.
(defun edit-got-char (window char)
  (case char
    (#\^b (move-left window))
    (#\^f (move-right window))    
    (#\^n (move-down window))
    (#\^p (move-up window))
    (#\^a (start-of-line window))
    (#\^e (end-of-line window))
    (#\meta-\b (backward-word window))
    (#\meta-\f (forward-word window))
    (#\meta-\< (beginning-of-buffer window))
    (#\meta-\> (end-of-buffer window))
    (#\^d (remove-char window))
    ((#\backspace #\rubout)(delete-char window))
    (#\^o (open-line window))
    ((#\return #\newline)(open-line-below window))
    (#\^k (kill-line window))
    (#\^z (stop-edit window))
    (t (if (graphic-char-p char)
	   (insert-char window char)
	 (beep)))))

(defun move-left (window)
  (let (row)
    (cond ((plusp (window-stream-get window :edit-column))
	   (decf (window-stream-x-position window)
		 (font-character-width (window-stream-font window)))
	   (decf (window-stream-get window :edit-column))
	   t)
	  ((plusp (setq row (window-stream-get window :edit-row)))
	   (goto-char window (1- row)
		      (window-stream-get window :edit-columns))
	   t)
	  (t (beep)
	     nil))))

(defun move-down (window)
  (cond ((< (window-stream-get window :edit-row)
	    (1- (window-stream-get window :edit-rows)))
	 (decf (window-stream-y-position window)
	       (font-character-height (window-stream-font window)))
	 (incf (window-stream-get window :edit-row)))
	(t (beep))))

(defun move-right (window)
  (let (row)
    (cond ((< (window-stream-get window :edit-column)
	      (window-stream-get window :edit-columns))
	   (incf (window-stream-x-position window)
		 (font-character-width (window-stream-font window)))
	   (incf (window-stream-get window :edit-column)))
	  ((< (setq row (window-stream-get window :edit-row))
	      (1- (buffer-num-lines
		   window (window-stream-get window :edit-buffer))))
	   (goto-char window (1+ row) 0))
	  (t (beep)))))

(defun move-up (window)
  (cond ((plusp (window-stream-get window :edit-row))
	 (incf (window-stream-y-position window)
	       (font-character-height (window-stream-font window)))
	 (decf (window-stream-get window :edit-row)))
	(t (beep))))

(defun goto-char (window row column)
  (let ((font (window-stream-font window)))
    (when column
      (setf (window-stream-x-position window)
	(* column (font-character-width font)))
      (setf (window-stream-get window :edit-column) column))
    (when row
      (setf (window-stream-y-position window)
	(- (window-stream-inner-height window)
	   (- (* (1+ row)(font-character-height font))
	      (font-baseline font))))
      (setf (window-stream-get window :edit-row) row))))

(defun start-of-line (window)
  (goto-char window nil 0))

(defun end-of-line (window)
  (goto-char window nil (buffer-line-length
			 (window-stream-get window :edit-buffer)
			 (window-stream-get window :edit-row))))

(defun forward-word (window)
  (let ((buffer (window-stream-get window :edit-buffer))
	(columns (window-stream-get window :edit-columns))
	(row (window-stream-get window :edit-row))
	(column (window-stream-get window :edit-column))
	)
    (let ((next-char (position #\space (aref buffer row)
			       :start column :test-not #'eql)))
      (cond (next-char
	     (goto-char
	      window nil
	      (or (position #\space (aref buffer row)
			    :start next-char)
		  columns)))
	    ((< row (1- (buffer-num-lines window buffer)))
	     (goto-char window (1+ row) 0)
	     (forward-word window))
	    (t (beep))))))

(defun backward-word (window)
  (let ((buffer (window-stream-get window :edit-buffer))
	(columns (window-stream-get window :edit-columns))
	(row (window-stream-get window :edit-row))
	(column (window-stream-get window :edit-column))
	)
    (let ((next-char (position #\space (aref buffer row)
			       :end column :test-not #'eql
			       :from-end t)))
      (cond (next-char
	     (goto-char
	      window nil
	      (1+ (or (position #\space (aref buffer row)
				:end next-char :from-end t)
		      -1))))
	    ((plusp row)
	     (goto-char window (1- row) columns)
	     (backward-word window))
	    (t (beep))))))

(defun insert-char (window char)
  (let* ((font (window-stream-font window))
	 (row-string (aref (window-stream-get window :edit-buffer)
			   (window-stream-get window :edit-row)))
	 (column (window-stream-get window :edit-column))
	 (columns (window-stream-get window :edit-columns))
	 (x-pos (window-stream-x-position window))
	 (y-pos (window-stream-y-position window))
	 (y-base-pos (- y-pos (font-baseline font)))
	 (char-width (font-character-width font))
	 (char-height (font-character-height font)))
    (cond ((< column columns)
	   (do ((j (1- (window-stream-get window :edit-columns))
		   (1- j)))
	       ((<= j column))
	     (setf (aref row-string j)(aref row-string (1- j))))
	   (setf (aref row-string column) char)
	   (bitblt window x-pos y-base-pos
		   window (+ x-pos char-width) y-base-pos
		   (* (- columns column 1) char-width) char-height)
	   (incf (window-stream-get window :edit-column))
	   (clear-rectangle-xy window x-pos y-base-pos char-width char-height)
	   (write-char char window)
	   (force-output window))
	  (t (beep)))))

(defun delete-char (window)
  (when (move-left window)
    (remove-char window)))

(defun remove-char (window)
  (let* ((font (window-stream-font window))
	 (row-string (aref (window-stream-get window :edit-buffer)
			   (window-stream-get window :edit-row)))
	 (column (window-stream-get window :edit-column))
	 (last-column (1- (window-stream-get window :edit-columns)))
	 (x-pos (window-stream-x-position window))
	 (y-pos (window-stream-y-position window))
	 (y-base-pos (- y-pos (font-baseline font)))
	 (inner-width (window-stream-inner-width window))
	 (char-width (font-character-width font))
	 (char-height (font-character-height font)))
    (do ((j column (1+ j)))
	((>= j last-column))
	(setf (aref row-string j)(aref row-string (1+ j))))
    (setf (aref row-string last-column) #\space)
    (bitblt window (+ x-pos char-width) y-base-pos window x-pos y-base-pos
	    (- inner-width (+ x-pos char-width)) char-height)
    (clear-rectangle-xy window (- inner-width char-width) y-base-pos
			char-width char-height)))

(defun open-line-below (window)
  (move-down window)
  (open-line window))

(defun open-line (window)
  (let* ((buffer (window-stream-get window :edit-buffer))
	 (rows (window-stream-get window :edit-rows))
	 (row (window-stream-get window :edit-row))
	 (last-row-string (aref buffer (1- rows)))
	 (font (window-stream-font window))
	 (font-height (font-character-height font))
	 (bottom (- (window-stream-inner-height window)
		    (* rows font-height)))
	 (blit-width (* (window-stream-get window :edit-columns)
			(font-character-width font)))
	 (blit-height (* (- rows row 1) font-height))
	 )
    (when (minusp bottom)
      (incf blit-height bottom)
      (setf bottom 0))
    (do ((j (1- rows)(1- j)))
	((= j row))
      (setf (aref buffer j)(aref buffer (1- j))))
    (setf (aref buffer row) last-row-string)
    (fill last-row-string #\space)
    (bitblt window 0 (+ bottom font-height) window 0 bottom
	    blit-width blit-height)
    (clear-rectangle-xy window 0 (+ bottom blit-height)
			blit-width font-height)
    (goto-char window nil 0)))

(defun kill-line (window)
  (let* ((buffer (window-stream-get window :edit-buffer))
	 (rows (window-stream-get window :edit-rows))
	 (row (window-stream-get window :edit-row))
	 (current-row-string (aref buffer row))
	 (font (window-stream-font window))
	 (font-height (font-character-height font))
	 (bottom (- (window-stream-inner-height window)
		    (* rows font-height)))
	 (blit-width (* (window-stream-get window :edit-columns)
			(font-character-width font)))
	 (blit-height (* (- rows row 1) font-height))
	 )
    (when (minusp bottom)
      (incf blit-height bottom)
      (setf bottom 0))
    (do ((j row (1+ j)))
	((= j (1- rows)))
      (setf (aref buffer j)(aref buffer (1+ j))))
    (setf (aref buffer (1- rows)) current-row-string)
    (fill current-row-string #\space)
    (bitblt window 0 bottom window 0 (+ bottom font-height)
	    blit-width blit-height)
    (clear-rectangle-xy window 0 bottom	blit-width font-height)
    (goto-char window nil 0)))

(defun beginning-of-buffer (window)
  (goto-char window 0 0))

(defun end-of-buffer (window)
  (let* ((buffer (window-stream-get window :edit-buffer))
	 (last-row (1- (buffer-num-lines window buffer))))
    (goto-char window last-row (buffer-line-length buffer last-row))))

(defun stop-edit (window)
  (if (fboundp 'user::remove-drawn-cursor)
      (user::remove-drawn-cursor window)
    (cw::remove-text-input-cursor window))
  (setf (window-stream-got-character window)
    (window-stream-get window :old-got-char))
  (setf (window-stream-get window :editing-p) nil))

(defun setup-buffer (window columns rows initial-string)
  (let ((buffer (window-stream-get window :edit-buffer)))
    (cond ((and buffer
		(= (array-dimension buffer 0) rows)
		(= (array-dimension (aref buffer 0) 0) columns))
	   (clear-buffer buffer)
	   (fill-buffer buffer rows columns initial-string))
	  (t
	   (setf buffer (make-buffer window rows columns))
	   (fill-buffer buffer rows columns initial-string)))
    (display-buffer window buffer)))

(defun fill-buffer (buffer rows columns string)
  (declare (ignore columns))
  (do ((j 0 (1+ j))
       (start 0 (and end (1+ end)))
       end)
      ((or (= j rows)
	   (null start)))
    (setq end (position #\newline string :start start))
    (replace (aref buffer j) string :start2 start :end2 end)))

(defun make-buffer (window rows columns)
  (let ((buffer (make-array rows)))
    (dotimes (j rows)
      (setf (aref buffer j)(make-string columns :initial-element #\space)))
    (setf (window-stream-get window :edit-buffer) buffer)
    buffer))

(defun display-buffer (window buffer)
  (clear window)
  (reset window)
  (dotimes (j (array-dimension buffer 0))
    (princ (aref buffer j) window)
    (terpri window)))
  
(defun clear-buffer (buffer)
  (dotimes (j (array-dimension buffer 0))
    (fill (aref buffer j) #\space)))

;; Anybody can call this at anytime to see what's in the window
;; at some intermediate point.
(defun return-edited-string (window)
  (let* ((buffer (window-stream-get window :edit-buffer))
	 (last-row (1- (buffer-num-lines window buffer))))
    (with-output-to-string (s)
      (dotimes (j (1+ last-row))
	(dotimes (k (buffer-line-length buffer j))
	  (princ (aref (aref buffer j) k) s))
	(unless (= j last-row)
	  (terpri s))))))

(defun buffer-line-length (edit-buffer line-num)
  ;; a real editor would cache the length of each line, but we won't
  ;; to keep the code short.
  (1+ (or (position #\space (aref edit-buffer line-num)
		    :from-end t :test-not #'eql)
	  -1)))

(defun buffer-num-lines (window buffer)
  ;; a real editor would cache this away too.
  (do ((last-row (1- (window-stream-get window :edit-rows))))
      ((plusp (buffer-line-length buffer last-row))
       (1+ last-row))
    (decf last-row)))

(defun beep ()
  (xlib::bell xcw::x-display)
  (xcw::x-flush))

(format t "~%Do (use-package :cw-edit), ~
           ~%call (cw-edit some-common-window) to run, ~
           ~%then try some basic emacs commands in the window. ~
           ~%Use control-z to exit. ~
           ~%See source code for keyword args. ~
           ~%This program should be compiled for speed.~%")

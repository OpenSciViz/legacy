;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; Static scroll bar example.
;; Creates a static scroll bar on a window, itself consisting of a
;; common window, with all code written at the user level.

;; NOTE:  Middle-bar dragging is not implemented.

(defpackage :user (:use :cw))
(in-package :user)

(defparameter *static-scroll-bar-width* 18)

(defparameter *static-scroll-bar-method-wrappers*
    '(((:repaint) repaint-static-scroll-bar)
      ((:expose :expose-notify) expose-static-scroll-bar)
      ((:bury :bury-notify) bury-static-scroll-bar)
      ((:deactivate) deactivate-static-scroll-bar)
      ((:flush :flush-notify) flush-static-scroll-bar)
      ((:move :move-notify :reshape :reshape-notify)
       reattach-static-scroll-bar)))

(defun make-static-scroll-bar (window)
  (let ((bar-window (make-window-stream :activate-p t
				 :title (and (window-stream-title window)
					     " ")
				 :title-font ; make title-bar same height
				 (window-stream-title-font window)
				 :borders (window-stream-borders window)
				 :parent (window-stream-parent window)
				 :left (- (window-stream-left window)
					  *static-scroll-bar-width*)
				 :bottom (window-stream-bottom window)
				 :width *static-scroll-bar-width*
				 :height (window-stream-height window)))
	)
    (setf (window-stream-get bar-window :scrolled-window) window)    
    (setf (window-stream-get window :scroll-bar) bar-window)
    (setf (window-stream-button bar-window) '(static-scroll-bar-button))
    (setf (window-stream-repaint bar-window) '(static-scroll-bar-repaint))
    (disable-window-stream-extent-scrolling window :horizontal nil)
    
    (dolist (methods *static-scroll-bar-method-wrappers*)
      (dolist (method (car methods))
	(modify-window-stream-method window method :after (cadr methods))))
    (repaint bar-window)))

(defvar *scratch-mouse-state* (make-mouse-state))
(defparameter *static-scroll-bar-top-margin* 10)

(defun static-scroll-bar-button (bar-window mouse-state &rest ignore)
  (declare (ignore ignore))
  (let ((window (window-stream-get bar-window :scrolled-window)))
    (unless (and window (not (eq (window-stream-status window) :flushed)))
      (flush bar-window)
      (format t "~%Scroll bar's window no longer existed.~%")
      (return-from static-scroll-bar-button))
    (let ((button-state (mouse-state-button-state mouse-state))
	  (extent-bottom (window-stream-extent-bottom window))
	  (extent-height (window-stream-extent-height window))
	  (window-height (window-stream-inner-height window))
	  (bar-window-height (window-stream-inner-height bar-window)))

      ;; Loop to keep scrolling if user is holding middle button down.
      (do* ((last-y-pos nil y-pos)
	    (first-time-or-middle-button-down-p
	     t
	     (eq (mouse-state-button-state 
		  (get-mouse-state bar-window *scratch-mouse-state*))
		 *middle-button-down*))
	    (y-pos (position-y (mouse-state-position mouse-state))
		   (position-y (mouse-state-position *scratch-mouse-state*))))
	  ((not first-time-or-middle-button-down-p))

	;; Hack --- if middle-click near top, act as if clicked exactly
	;; at top, so moving to top is easy with a single middle click.
	(when (and (eq button-state *middle-button-event*)
		   (< (- bar-window-height y-pos)
		      *static-scroll-bar-top-margin*))
	  (setq y-pos bar-window-height))

	;; If user is holding middle button down but not moving, just sleep.
	(if (and last-y-pos (= last-y-pos y-pos))
	    (sleep .1)

	  ;; Scroll the window, depending on which button was clicked and
	  ;; where the scroll bar window was clicked.
	  (scroll window
		  (make-region
		   :left (window-stream-x-offset window)
		   :width (window-stream-inner-width window)
		   :bottom

		   ;; Don't allow scrolling outside the window's extent.
		   (max extent-bottom
			(min (- (+ extent-bottom extent-height)
				window-height)

			     ;; Absolute scrolling with the middle button.
			     (if (eq button-state *middle-button-event*)
				 (+ extent-bottom
				    (- (round (* (/ extent-height
						    bar-window-height)
						 y-pos))
				       window-height))

			       ;; Relative scrolling with the other buttons.
			       (+ (window-stream-y-offset window)
				  (if (eq button-state *left-button-event*)
				      (- y-pos bar-window-height)
				    (- bar-window-height y-pos))))))

		   :height (window-stream-inner-height window))))))))

(defun static-scroll-bar-repaint (bar-window &rest ignore)
  (declare (ignore ignore))
  (let* ((window (window-stream-get bar-window :scrolled-window))
	 (extent-bottom (window-stream-extent-bottom window))	 
	 (extent-height (window-stream-extent-height window))	 
	 (y-offset (window-stream-y-offset window))
	 (bar-window-height (window-stream-inner-height bar-window))
	 (scale-factor (/ bar-window-height extent-height))
	 bar-bottom bar-height old-bar-bottom old-bar-height
	 source-width source-height draw-width draw-height move-down-p
	 )

    ;; Don't redraw scroll bar if it hasn't moved.
    (when (and (= extent-bottom
		  (or (window-stream-get bar-window :extent-bottom) 0))
	       (= extent-height
		  (or (window-stream-get bar-window :extent-height) 0))
	       (= y-offset
		  (or (window-stream-get bar-window :y-offset) 0))
	       (= bar-window-height
		  (or (window-stream-get bar-window :window-height) 0)))
      (return-from static-scroll-bar-repaint))
    (setf (window-stream-get bar-window :extent-bottom) extent-bottom)
    (setf (window-stream-get bar-window :extent-height) extent-height)
    (setf (window-stream-get bar-window :y-offset) y-offset)
    (setf (window-stream-get bar-window :window-height) bar-window-height)

    (setq old-bar-bottom (window-stream-get bar-window :bar-bottom)
	  old-bar-height (window-stream-get bar-window :bar-height)
	  bar-bottom (round (* (- y-offset extent-bottom) scale-factor))
	  bar-height (max 3 (round (* (window-stream-inner-height window)
				      scale-factor)))
	  source-width (bitmap-width *grey-bitmap*)
	  source-height (bitmap-height *grey-bitmap*)
	  draw-width (window-stream-inner-width bar-window))
    (setf (window-stream-get bar-window :bar-bottom) bar-bottom)
    (setf (window-stream-get bar-window :bar-height) bar-height)

    ;; If we know where the scroll bar is now, just erase a bit off one
    ;; end and draw a bit onto the other end to prevent flashing.
    (cond ((and old-bar-height old-bar-bottom
		(= old-bar-height bar-height))
	   (setq draw-height (min (abs (- old-bar-bottom bar-bottom))
				  bar-height)
		 move-down-p (< bar-bottom old-bar-bottom))
	   (clear-rectangle-xy bar-window 0 (if move-down-p
					 (- (+ old-bar-bottom bar-height)
					    draw-height)
				       old-bar-bottom)
			       draw-width draw-height)
	   (draw-filled-rectangle-xy bar-window 0 (if move-down-p
					       bar-bottom
					     (- (+ bar-bottom bar-height)
						draw-height))
				     draw-width draw-height
				     :texture *grey-bitmap*))

	  ;; Otherwise just redraw the whole scroll bar.
	  (t
	   (clear bar-window)
	   (draw-filled-rectangle-xy
	    bar-window 0 bar-bottom draw-width bar-height
	    :texture *grey-bitmap*)))))

(defvar *static-scroll-bar-last-window* nil)
(defvar *static-scroll-bar-last-region* nil)

(defun reattach-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (let ((bar-window (window-stream-get window :scroll-bar))
	(region (window-stream-region window))
	bar-width)

    ;; Ignore the move/reshape notification from the window manager
    ;; if it passes the same window and region as the previous notification.
    ;; Use this to get rid of most of the redundancy.
    (when (and *static-scroll-bar-last-window*
	       (eq *static-scroll-bar-last-window* window)
	       (region= *static-scroll-bar-last-region* region))
      (return-from reattach-static-scroll-bar))
    (setq *static-scroll-bar-last-window* window
	  *static-scroll-bar-last-region* region)
    
    ;; Get rid of this cached value so the bar will have to be redrawn
    ;; completely next time, instead of the fancy incremental draw.
    (setf (window-stream-get bar-window :bar-height) nil)

    ;; Move/shape the bar window to fit the application window.
    (when (and bar-window
	       (not (eq (window-stream-status bar-window) :flushed)))
      (setq bar-width (window-stream-width bar-window))
      (setf (window-stream-region bar-window)
	(make-region :left (- (window-stream-left window) bar-width)
		     :bottom (window-stream-bottom window)
		     :width bar-width
		     :height (window-stream-height window)))
      (repaint bar-window))))

(defun region= (r1 r2)
  (and (= (region-left r1)(region-left r2))
       (= (region-bottom r1)(region-bottom r2))
       (= (region-width r1)(region-width r2))
       (= (region-height r1)(region-height r2))))

;; method wrappers to keep the scroll bar attached to its window.
(defun repaint-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (bring-scroll-bar-along window 'repaint))
(defun expose-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (bring-scroll-bar-along window 'expose))
(defun bury-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (bring-scroll-bar-along window 'bury))
(defun deactivate-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (bring-scroll-bar-along window 'deactivate))
(defun flush-static-scroll-bar (window &rest ignore)
  (declare (ignore ignore))
  (bring-scroll-bar-along window 'flush))
  
(defun bring-scroll-bar-along (window method)  
  (let ((bar-window (window-stream-get window :scroll-bar)))
    (when (and bar-window
	       (not (eq (window-stream-status bar-window) :flushed)))
      (funcall method bar-window))))

(format t "~%Call (make-static-scroll-bar some-window) to create a scroll bar.")

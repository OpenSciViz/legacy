;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(defparameter *color-255* (make-color :red 0.0 :green 0.0 :blue 0.0))
(setf (xcw::color-.pixel *color-255*) 255)

(defun bitblt-foreground
    (source-object source-left source-bottom
     dest-object dest-left dest-bottom
     &optional width height (color black) clipping-region replicate)

  ;; erase the source foreground pixels from the destination
  (setf (bitmap-foreground-color source-object) *color-255*)
  (bitblt source-object source-left source-bottom
	  dest-object dest-left dest-bottom
	  width height boole-andc1 clipping-region replicate)
  
  ;; draw source in ior mode to leave surrounding pixels on the destination
  (setf (bitmap-foreground-color source-object) color)
  (bitblt source-object source-left source-bottom
	  dest-object dest-left dest-bottom
	  width height boole-ior clipping-region replicate))

(format t "~%This defines the function: ~
         ~%~%(defun bitblt-foreground ~
           ~%   (source-object source-left source-bottom ~
           ~%    dest-object dest-left dest-bottom ~
           ~%    optional width height (color black) ~
           ~%    clipping-region replicate)~%")

;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; A way to get combined extent scrolling and simulated
;; page scrolling of text printed to a window (since cw doesn't
;; implement the combination of the two).

;; Call (multiscroll-setup) and then print stuff to *stream*.

;; --------------------------------------------------------------------
;; General code to advise a window stream to call scroll-if-needed
;; after being printed to.  Shows how to advise printing to any
;; stream in Allegro CL.

(defun window-unkeep-scrolled (window)
  (funcall (excl::sm_misc window) window :unkeep-scrolled nil))

(defun window-keep-scrolled (window)
  (let ((wchr (excl::sm_write-char window))
	(wstr (excl::sm_write-string window))
	(misc (excl::sm_misc window)))
    (labels ((keep-scrolled-misc (window key arg)
	       (case key
		 (:unkeep-scrolled (setf (excl::sm_write-char   window) wchr
					 (excl::sm_write-string window) wstr
					 (excl::sm_misc         window) misc)
				   t)
		 (t (funcall misc window key arg))))

	     (keep-scrolled-write-char (window char)
	       (prog1
		   (funcall wchr window char)
		 (when (eql char #\newline)
		   (scroll-if-needed window))))

	     (keep-scrolled-write-string (window string start cnt)
	       (prog1
		   (funcall wstr window string start cnt)
		 (scroll-if-needed window)))
	     )
      (setf (excl::sm_write-char   window) #'keep-scrolled-write-char
	    (excl::sm_write-string window) #'keep-scrolled-write-string
	    (excl::sm_misc         window) #'keep-scrolled-misc)
      window)))

;; --------------------------------------------------------------------
;; Example-specific code

(defvar *stream* nil)
(defvar *screens-to-save* 4)

(defvar *ws* nil)
(defvar *bs* nil)
(defvar *saved-text-height* nil)
(defvar *multiscroll-windows* nil)
(defvar *multiscroll-process* nil)

(defun multiscroll-setup ()
  (unless (and *ws* (window-stream-p *ws*)
	       (not (eq (window-stream-status *ws*) :flushed)))
    (setq *ws* (make-window-stream :activate-p t :left 750 :bottom 100
				   :width 250 :height 300)))
  (window-keep-scrolled *ws*)
  (setf (window-stream-repaint *ws*) '(multiscroll-repaint))
  (enable-window-stream-extent-scrolling *ws*)
  (setf (window-stream-extent-bottom *ws*) 0)
  (setf (window-stream-extent-height *ws*)(window-stream-inner-height *ws*))
  (setf (window-stream-y-offset *ws*) 0)
  (clear *ws*)(reset *ws*)
  (unless (and *bs* (window-stream-p *bs*)
	       (not (eq (window-stream-status *bs*) :flushed)))
    (setq *bs* (make-window-stream
		:inner-width (window-stream-inner-width *ws*)
		:inner-height (* *screens-to-save*
				 (window-stream-inner-height *ws*)))))
  (setq *saved-text-height* (window-stream-height *bs*))
  (clear *bs*)(reset *bs*)
  (setq *stream* (make-broadcast-stream *bs* *ws*)))

(defun scroll-if-needed (window)
  (let* ((text-bottom (- (window-stream-y-position window)
			 (font-baseline (window-stream-font window))))
	 (new-height (- (window-stream-extent-bottom window) text-bottom)))
    (when (plusp new-height)
      (decf (window-stream-extent-bottom window) new-height)
      (incf (window-stream-extent-height window)
	    (max 0 (min (- *saved-text-height*
			   (window-stream-extent-height window))
			new-height)))
      (scroll window (make-region :left (window-stream-x-offset window)
				  :bottom text-bottom
				  :width (window-stream-inner-width window)
				  :height (window-stream-inner-height
					   window))))))

(defun multiscroll-repaint (window &rest ignore)
  (bitblt *bs* 0 (+ (- (window-stream-y-position *bs*)
		       (font-baseline (window-stream-font window)))
		    (- (window-stream-y-offset window)
		       (window-stream-extent-bottom window)))
	  window
	  (window-stream-x-offset window)
	  (window-stream-y-offset window)
	  (window-stream-inner-width window)
	  (window-stream-inner-height window)
	  boole-1))

(format t "~%Call (multiscroll-setup) and then print stuff to *stream*.")


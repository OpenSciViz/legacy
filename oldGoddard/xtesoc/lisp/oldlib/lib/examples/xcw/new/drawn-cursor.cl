;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;; ----------------------------------------------------------------
;; User functions add-drawn-cursor and remove-drawn-cursor.  These should
;; be called *after* setting up the got-character method for the window,
;; so that the text cursor erasing and drawing will wrap any other
;; got-character behavior.

;; Mode defaults to inverting the cursor region.
;; Other modes are (:outline :underscore :bar).

(defun add-drawn-cursor (window &optional mode)
  (remove-drawn-cursor window)
  (setf (window-stream-get window :cursor-on-p) t)
  (setf (window-stream-get window :cursor-region) nil)
  (setf (window-stream-get window :cursor-mode) mode)
  (update-cursor window)
  (modify-window-stream-method window :got-character :before
			       'got-character-erase-cursor)
  (modify-window-stream-method window :got-character :after
			       'got-character-update-cursor))

(defun remove-drawn-cursor (window)
  (when (window-stream-get window :cursor-on-p)
    (erase-cursor window)
    (modify-window-stream-method window :got-character :remove
				 'got-character-erase-cursor)
    (modify-window-stream-method window :got-character :remove
				 'got-character-update-cursor)
    (setf (window-stream-get window :cursor-on-p) nil)))

;; ----------------------------------------------------------------
;; Lower-level user functions.
;; Use these to erase and update the cursor by hand.
;; Always call erase-cursor before something might draw where
;; the cursor is now drawn.  Call update-cursor whenever
;; its position in the window may have changed, in order to
;; move and redraw it.

(defun update-cursor (window &optional x y)
  (when (window-stream-get window :cursor-on-p)
    (let ((cursor-region (window-cursor-region window)))
      (when (window-stream-get window :cursor-visible-p)
	(draw-cursor window cursor-region))
      (setf (region-left cursor-region)
	(or x (window-stream-x-position window)))
      (setf (region-bottom cursor-region)
	(- (or y (window-stream-y-position window))
	   (window-stream-get window :font-baseline)))
      (draw-cursor window cursor-region)
      (setf (window-stream-get window :cursor-visible-p) t))))
  
(defun erase-cursor (window)
  (when (window-stream-get window :cursor-visible-p)
    (draw-cursor window (window-cursor-region window))
    (setf (window-stream-get window :cursor-visible-p) nil)))

;; ----------------------------------------------------------------
;; Non-user functions.

;; If you want to draw another type of cursor, modify this function
;; to do it, and pass in the added mode to add-drawn-cursor.
;; The drawing should always be done in boole-xor mode, so that this
;; code can erase it by simply calling this function a second time.
(defun draw-cursor (window cursor-region)
  (case (window-stream-get window :cursor-mode)
    (:outline
     (draw-rectangle-xy
      window (region-left cursor-region)(region-bottom cursor-region)
      (region-width cursor-region)(region-height cursor-region)
      :operation boole-xor))
    (:underscore
     (draw-line-xy
      window (region-left cursor-region)(region-bottom cursor-region)
      (region-right cursor-region)(region-bottom cursor-region)
      :operation boole-xor :brush-width 3))
    (:bar
     (draw-line-xy
      window (region-left cursor-region)(region-bottom cursor-region)
      (region-left cursor-region)(region-top cursor-region)
      :operation boole-xor))
    (t
     (complement-area window cursor-region))))

(defun window-cursor-region (window)
  (or (window-stream-get window :cursor-region)
      (setup-cursor-region window)))

(defun setup-cursor-region (window)
  (let* ((font (window-stream-font window))
	 (region (make-region)))
    (setf (window-stream-get window :cursor-region) region)
    (setf (window-stream-get window :cursor-visible-p) nil)
    (setf (region-width region)(font-string-width font "J"))
    (setf (region-height region)(font-character-height font))
    (setf (window-stream-get window :font-baseline)
      (font-baseline font))
    region))

(defun got-character-erase-cursor (wstr ch)
  (unless (cw::wsys-break-on-interrupt wstr ch)
    (erase-cursor wstr))
  ch)

(defun got-character-update-cursor (wstr ch)
  (unless (cw::wsys-break-on-interrupt wstr ch)
    (update-cursor wstr))
  ch)

(format t "~%Call (add-drawn-cursor some-window) to set up a drawn cursor, and  (remove-drawn-cursor some-window) to remove it.~%")

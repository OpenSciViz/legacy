This directory contains sample applications written in Common Windows.
Loading each one gives brief instructions on how to run it.  Here are
brief descriptions of the examples:

----------------------------------------------------------------------
active-region-menu.cl

Constructs a static menu using a window with active regions.  When the
user mouses on an item, a prompt window appears asking for input.  A
side effect is produced, in this case simply printing the user's
answer.  Shows how to avoid a block between queued mouse events that
wait on each other.

----------------------------------------------------------------------
active-region-text.cl

Prints text to a window, with certain key strings printed in bold
type and made into active regions.  Mousing on one of these active
regions prints new text associated with that item into the window.

----------------------------------------------------------------------
color-bitblt.cl

Provides the function "bitblt-foreground", which allows you to blit
the foreground of a standard 1-bit-per-pixel bitmap in some color,
without clearing the background areas of the bitmap from the
destination.

----------------------------------------------------------------------
cw-edit.cl

A simple but usable editor utilizing some basic emacs commands.

----------------------------------------------------------------------
draw-arrow-xy.cl

Provides the function draw-arrow-xy, which draws fast lines with
arrowheads by approximating the trigonometry with only integer
arithmetic.

----------------------------------------------------------------------
drawn-cursor.cl

The built-in xcw text cursor is an x window that obscures anything
beneath it.  This code creates a cursor that is drawn in boole-xor
mode to allow viewing whatever is drawn "underneath" it.  The cw-edit
example above knows to use this code if it's loaded in.

----------------------------------------------------------------------
font-test.cl

Displays all the available fonts.

----------------------------------------------------------------------
furl.cl

For color or gray-scale machines.  Produces a pleasant effect by
starting with a random array of pixels which are gradually and
randomly influenced by their neighbors.

----------------------------------------------------------------------
get-bitmap-from-window.cl

Simple example to interactively create a bitmap from a section of
some window.

----------------------------------------------------------------------
keep-scrolled.cl

Shows how to advise an extent-scrolled window stream so that it
scrolls to the bottom automatically when text is printed to it.

----------------------------------------------------------------------
static-scroll-bar.cl

Attaches a static (non-pop-up) scroll bar to a window.  (The scroll bar
is simply another common window, and all code is user level.)

----------------------------------------------------------------------
text-scroller.cl

A general text scroller.  Suitable for large amounts of text that
would require too large a bitmap stream to save it all there
in printed form.

----------------------------------------------------------------------
texture.cl

Draws a simple example of filling shapes with a texture bitmap.

----------------------------------------------------------------------

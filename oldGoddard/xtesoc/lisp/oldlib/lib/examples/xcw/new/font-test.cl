;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

(defparameter *fwind* nil)
(defparameter *fonts-left* nil)

(defun cw-font-test ()
  (if (and *fwind* (not (eq (window-stream-status *fwind*) :flushed)))
      (expose *fwind*)
    (let ((region (get-region *root-window*
			     :documentation "Shape Window")))
      (setq *fwind* (make-window-stream
		     :left (region-left region) :bottom (region-bottom region)
		     :width (region-width region)
		     :height (region-height region)
		     :borders 4 :title "Left to See Next Font; Right to End"
		     :activate-p t))))
  (setq *fonts-left* (installed-fonts))
  (setf (window-stream-button *fwind*)
	'(cw-font-test-button)))

(defun cw-font-test-button (window mouse-state transition)
  (declare (ignore transition))
  (if (and *fonts-left*
	   (not (eql (mouse-state-button-state mouse-state)
		     *right-button-down*)))
      (let* (font server real-font
		  (system-font-height (font-character-height *system-font*))
		  (system-font-baseline (font-baseline *system-font*))
		  (y (- (window-stream-inner-height window)
			system-font-height))
		  (spacing 5))
	(clear window)
	(do* ((font (car *fonts-left*)(car *fonts-left*))
	      (server-name (cdr (or (assoc :font-descriptor font)  ; pilot
				    (assoc :server-name font)))     ; final
			   (if font (cdr (or (assoc :font-descriptor font)
					     (assoc :server-name font)))))
	      (real-font (open-font-named server-name)
			 (if font (open-font-named server-name)))
	      (font-height (font-character-height real-font)
			   (if font (font-character-height real-font)))
	      (font-baseline (font-baseline real-font)
			     (if font (font-baseline real-font))))
	     ((or (null *fonts-left*)
		  (< y (+ (* 2 system-font-height) font-height))))
	     (setq *fonts-left* (cdr *fonts-left*))
	     (draw-string-xy window 10 y server-name :font *system-font*)
	     (decf y system-font-height)
	     (draw-string-xy window 10 y (format nil "~a" real-font)
			     :font *system-font*)
	     (decf y (+ system-font-baseline (- font-height font-baseline)))
	     (draw-string-xy window 10 y "AaBbCcDdEeFfGgHhIiJj0123456789"
			     :font real-font)
	     (decf y (+ spacing font-baseline (- system-font-height
						 system-font-baseline)))
	  ))
    (flush window)))

(format t "~%Call (cw-font-test), then click the window to see fonts.")

;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.

(defpackage :user (:use :cw))
(in-package :user)

(defconstant *sw-radius* 75)

(defparameter subw       nil)

(defparameter *sw-circles*
  (list (list 100 100 black)
	(list 200 150 red)
	(list 300 200 green)
	(list 400 250 blue)
	(list 500 300 yellow)
	(list 600 350 purple)
	(list 700 400 turquoise)))

(defun subwindemo ()
  (setq subw (make-window *root-window*
	       (make-region :left 950 :bottom 600 :width 500 :height 400))))

(defun make-window (p r)
  (let ((cw::*default-mat-width* 5)
	(w (make-window-stream
	     :left   (region-left   r)
	     :bottom (region-bottom r)
	     :width  (region-width  r)
	     :height (region-height r)
	     :parent p
	     :title "Subwindows Demo")))
    
    (setf (window-stream-mat-color w) red)
    (modify-window-stream-method w :button  :after 'sw-button-proc)
    (modify-window-stream-method w :repaint :after 'sw-repaint-proc)
    
    (setf (window-stream-extent-width  w) 800)
    (setf (window-stream-extent-height w) 500)
    (enable-window-stream-extent-scrolling w)
    (repaint w)
    (activate w)
    w))

(defun sw-repaint-proc (w region)
  (unless region
    (setq region (make-region
		   :left   (window-stream-x-offset w)
		   :bottom (window-stream-y-offset w)
		   :width  (window-stream-width  w)
		   :height (window-stream-height w))))
  (clear-area w region)
  (dolist (c *sw-circles*)
    (let* ((x (first c))
	   (y (second c))
	   (r (make-region
		:left   (- x *sw-radius*)
		:bottom (- y *sw-radius*)
		:width  (* 2 *sw-radius*)
		:height (* 2 *sw-radius*)))
	   (color (third c)))
      (when (region-intersection r region)
	(draw-circle-xy w x y *sw-radius*
	  :color color :brush-width 3)))))

(defun sw-button-proc (window state &optional action)
  (let ((button (mouse-state-button-state state)))
    (when (eq button *left-button-down*)
      (let ((r (get-region window)))
	(make-window window r)))))

(format t "
To start:  (subwindemo)

Click with the left mouse button in any window
to create a subwindow.

")

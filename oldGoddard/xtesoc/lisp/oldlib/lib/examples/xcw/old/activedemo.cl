;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;
;;;  Active Regions Demo
;;;
;;;  Ronald Mak
;;;  (c) 1987 by Sun Microsystems, Inc.
;;;

(defpackage :user (:use :cw))
(in-package :user)

(defparameter activew      nil)
(defparameter *fill-color* black)

(defparameter *rlist*
  (list
    (make-region :left 10  :bottom 10  :width 50 :height 30)
    (make-region :left 110 :bottom 10  :width 50 :height 30)
    (make-region :left 20  :bottom 150 :width 50 :height 30)
    (make-region :left 30  :bottom 100 :width 50 :height 30)
    (make-region :left 50  :bottom 200 :width 50 :height 30)
    (make-region :left 70  :bottom 60  :width 50 :height 30)
    (make-region :left 170 :bottom 180 :width 50 :height 30)
    (make-region :left 200 :bottom 250 :width 50 :height 30)))


(defun activedemo ()
  (setq activew (make-window-stream
		  :left 1000 :bottom 900 :width 300 :height 300
		  :title "Active Regions Demo"))
  
  (setf (window-stream-extent-width  activew) 400)
  (setf (window-stream-extent-height activew) 500)
  (enable-window-stream-extent-scrolling activew)
  (modify-window-stream-method activew :repaint :after 'show-regions)
  (add-right-button-menu activew)
  
  (dolist (r *rlist*) 
    (let ((ar (make-active-region 
	       :left (region-left r)
	       :bottom (region-bottom r)
	       :width (region-width r)
	       :height (region-height r)
	       :parent activew
	       :activate-p t)))
	  (setf (active-region-mouse-cursor-in ar) 'draw-fill-color)
	  (modify-active-region-method ar :mouse-cursor-out :after 'draw-box)))

  (activate activew)
  (repaint activew))


(defun show-regions (w region)
  (if* region
   then (clear-area w region)
   else (clear-rectangle-xy w 0 0 400 500))
  (dolist (r (window-stream-active-regions w))
    (draw-outline w r)))


(defun draw-outline (w r)
  (draw-rectangle-xy w
    (active-region-left r)  (active-region-bottom r)
    (active-region-width r) (active-region-height r)
    :color black))


(defun draw-fill-white (w r)
  (draw-filled-rectangle-xy w
    (active-region-left r)  (active-region-bottom r)
    (active-region-width r) (active-region-height r) :color white))


(defun draw-fill-color (ar &rest args)
  (declare (ignore args))
  (draw-filled-rectangle-xy (active-region-parent ar)
    (active-region-left  ar) (active-region-bottom ar)
    (active-region-width ar) (active-region-height ar) :color *fill-color*))


(defun draw-box (ar &rest args)
  (declare (ignore args))
  (let ((w (active-region-parent ar)))
    (draw-fill-white w ar)
    (draw-outline    w ar)))


(format t "~%To start:  (activedemo)~%~%")

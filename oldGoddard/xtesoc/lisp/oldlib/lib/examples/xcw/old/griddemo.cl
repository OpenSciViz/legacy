;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;
;;;  Active Grids Demo
;;;
;;;  (c) 1987 by Sun Microsystems, Inc.
;;;  (c) 1987 1988 by Franz Inc.
;;;

(defpackage :user (:use :cw))
(in-package :user)

(defstruct (grid (:include region))
  (rows 1 :type fixnum)
  (columns 1 :type fixnum))

(defparameter debug-io nil)

(defparameter *grid1* nil)
(defparameter *grid2* nil)

(defparameter gridw      nil)
(defparameter *grid-fill-color* blue)

(defparameter *glist*
  (list
   (make-grid :left 50 :bottom 50 :width 100 :height 50 :columns 2 :rows 4)
   (make-grid :left 75 :bottom 25 :width 30 :height 100 :columns 5 :rows 1)))

(defparameter *grid-rlist*
  (mapcar #'(lambda (grid) (make-region
			    :left   (grid-left   grid)
			    :bottom (grid-bottom grid)
			    :width  (grid-width  grid)
			    :height (grid-height grid)))
	  *glist*))	      

(defparameter *grid1-actions-list* nil)
(defparameter *grid2-actions-list* nil)
(defparameter *grid1-menu* nil)
(defparameter *grid2-menu* nil)
(defparameter *grids-list* nil)
(defparameter *grids-menu* nil)

(defun griddemo ()
  (setq gridw (make-window-stream
		  :left (* 2/3 (window-stream-width *root-window*))
		  :bottom (* 2/3 (window-stream-height *root-window*))
		  :width 300 :height 300
		  :title "Active Grids Demo"))
  (setf (window-stream-extent-width  gridw) 400)
  (setf (window-stream-extent-height gridw) 500)
  (enable-window-stream-extent-scrolling gridw)
  (modify-window-stream-method gridw :repaint :after 'grid-show-regions)
  (setf (window-stream-right-button-down gridw) 'grid-button-down)
  #+obsolete
  (add-right-button-menu gridw)
  
  (dolist (g *glist*)
    (let ((ar (make-active-region 
		:left    (grid-left    g)
		:bottom  (grid-bottom  g)
		:width   (grid-width   g)
		:height  (grid-height  g)
		:rows    (grid-rows    g)
		:columns (grid-columns g)
		:activate-p t
		:parent gridw)))
      (setf (active-region-mouse-cursor-in ar) 'grid-draw-fill-color)
      (setf (active-region-mouse-cursor-out ar) 'grid-draw-box)))
  (let ((ars (window-stream-active-regions gridw)))
    (setf *grid1* (second ars))
    (setf *grid2* (first ars))
    (setf (active-region-right-button-down *grid1*) 'grid1-button-down)
    (setf (active-region-right-button-down *grid2*) 'grid2-button-down))
  
  (repaint gridw)
  (activate gridw))


(defun grid-show-regions (w region)
  (declare (optimize (speed 3))
	   (type window-stream w))
  ;;
  (with-graphics-batching
    (if* region
       then (clear-area w region)
       else
	    (clear-area w (window-stream-inner-region w)))
    
    (let ((left 0)
	  (bottom 0)
	  (width 0)
	  (height 0))
      (declare (fixnum left bottom width height))
      
      (dolist (g *glist*)
	(declare (type grid g))
	
	(setf bottom (grid-bottom g))
	(setf width (grid-width g))
	(setf height (grid-height g))
	
	(dotimes (row (grid-rows g))
	  (declare (fixnum row))
	  
	  (setf left (grid-left g))
	  (dotimes (col (grid-columns g))
	    (declare (fixnum col))
	    
	    (draw-rectangle-xy w left bottom 
			       #+cw-x (1- width)
			       #+cw-news width
			       #+cw-x (1- height)
			       #+cw-news height :color black)
	    (incf left width))
	  (incf bottom height))))))


(defun grid-draw-outline (w r)
  (draw-rectangle-xy w
    (region-left r)  (region-bottom r)
    #+cw-x (1- (region-width r)) #+cw-news (region-width r)
    #+cw-x (1- (region-height r)) #+cw-news (region-height r)
    :color black))


(defun grid-draw-fill-white (w r)
  (draw-filled-rectangle-xy w
    (region-left r)  (region-bottom r)
    (region-width r) (region-height r) :color white))


(defun grid-draw-fill-color (ar state &optional mouse-action)
  (declare (ignore mouse-action))
  (let* ((w (active-region-parent ar))
	 (p (mouse-state-position state))
	 (x (position-x p))
	 (y (position-y p))
	 region)
    (when w
      (multiple-value-bind (row col) (active-region-grid-coordinates ar x y)
	#+cw-news
	(if debug-io
	    (format debug-io "Entering ar ~a cell ~a,~a (~a,~a)~%"
		    (scw::ar-news-id ar) row col x y))
	(setf region (active-region-grid-region ar row col))
	(draw-filled-rectangle-xy
	 w
	 (region-left  region) (region-bottom region)
	 (region-width region) (region-height region)
	 :color *grid-fill-color*)))))


(defun grid-draw-box (ar state &optional mouse-action)
  (declare (ignore mouse-action))
  (let* ((w (active-region-parent ar))
	 (p (mouse-state-position state))
	 (x (position-x p))
	 (y (position-y p))
	 region)
    (when w
      (multiple-value-bind (row col) (active-region-grid-coordinates ar x y)
	#+cw-news
	(if debug-io
	    (format debug-io " Exiting ar ~a cell ~a,~a (~a,~a)~%"
		    (scw::ar-news-id ar) row col x y))
	(setf region (active-region-grid-region ar row col))
	(grid-draw-fill-white w region)
	(grid-draw-outline    w region)))))
  
(eval-when (eval load)

	   (setq *grid1-actions-list*
		 `(("Expose" expose-ar1)
		   ("Bury" bury-ar1)
		   ("Activate" activate-ar1)
		   ("Deactivate" deactivate-ar1)))
	   (setq *grid2-actions-list*
		 `(("Expose" expose-ar2)
		   ("Bury" bury-ar2)
		   ("Activate" activate-ar2)
		   ("Deactivate" deactivate-ar2)))

	   (setq *grid1-menu* (make-pop-up-menu *grid1-actions-list*
						:title "5x1 actions"))
	   (setq *grid2-menu* (make-pop-up-menu *grid2-actions-list*))

	   
	   (setq *grids-list*
	     `(("Grid 1 (5x1) => " ,*grid1-menu*)
	       ("Grid 2 (2x4) => " ,*grid2-menu*)))
		   
	   (setq *grids-menu* (make-pop-up-menu *grids-list*))
	   )

(defun grid-button-down (stream mouse-state &optional mouse-action)
  (declare (ignore stream mouse-state mouse-action))
  (multiple-value-bind (choice menu)
      (pop-up-menu-choose *grids-menu*)
    (if menu
	(funcall choice gridw))))

(defun grid1-button-down (stream mouse-state &optional mouse-action)
  (declare (ignore stream mouse-state mouse-action))
  (multiple-value-bind (choice menu)
      (pop-up-menu-choose *grid1-menu*)
    (if menu
	(funcall choice gridw))))

(defun grid2-button-down (stream mouse-state &optional mouse-action)
  (declare (ignore stream mouse-state mouse-action))
  (multiple-value-bind (choice menu)
      (pop-up-menu-choose *grid2-menu*)
    (if menu
	(funcall choice gridw))))

(defun expose-ar1 (window)
  (declare (ignore window))
  (expose *grid1*))

(defun bury-ar1 (window)
  (declare (ignore window))
  (bury *grid1*))

(defun activate-ar1 (window)
  (declare (ignore window))
  (activate *grid1*))

(defun deactivate-ar1 (window)
  (declare (ignore window))
  (deactivate *grid1*))

(defun expose-ar2 (window)
  (declare (ignore window))
  (expose *grid2*))

(defun bury-ar2 (window)
  (declare (ignore window))
  (bury *grid2*))

(defun activate-ar2 (window)
  (declare (ignore window))
  (activate *grid2*))

(defun deactivate-ar2 (window)
  (declare (ignore window))
  (deactivate *grid2*))

(format t "~%Right button over window brings up a menu of ~
           active region operations.~2% To start:  (griddemo)~2%")




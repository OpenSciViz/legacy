;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;
;;; X Common Windows
;;;      menudemo.cl -- Walking menu demo
;;;	 

(defpackage :user (:use :cw))
(in-package :user)

(unless (common-windows-initialized-p)
  (initialize-common-windows))


(defun print-protista (&optional (s *terminal-io*))
  (format s "protista~%"))
(defun print-plants (&optional (s *terminal-io*))
  (format s "plants~%"))
(defun print-invertebrates (&optional (s *terminal-io*))
  (format s "invertebrates~%"))
(defun print-fish (&optional (s *terminal-io*))
  (format s "fish~%"))
(defun print-amphibians (&optional (s *terminal-io*))
  (format s "amphibians~%"))
(defun print-reptiles (&optional (s *terminal-io*))
  (format s "reptiles~%"))
(defun print-birds (&optional (s *terminal-io*))
  (format s "birds~%"))
(defun print-mammals (&optional (s *terminal-io*))
  (format s "mammals~%"))


(defparameter *vertebrates-list*
  `(("Fish" print-fish "Scaley things that swim")
    ("Amphibians" print-amphibians "Slimey things that hop")
    ("Reptiles" print-reptiles "Sandpapery things that crawl")
    ("Birds" print-birds "Feathery things that fly")
    ("Mammals" print-mammals "Furry things that scamper")))

(defparameter *vertebrates-menu* (make-pop-up-menu *vertebrates-list*))

(defparameter *animals-list*
  `(("Invertebrates" print-invertebrates "Spineless things")
    ("Vertebrates =>" ,*vertebrates-menu* "Spinefull things")))

(defparameter *animals-menu* (make-pop-up-menu *animals-list*))

(defparameter *living-list*
  `(("Animals =>" ,*animals-menu* "Not the group")
    ("Protista" print-protista "Terribly exciting critters")
    ("Plants" print-plants "Ah, clorophyll...")))

(defparameter *living-menu* (make-pop-up-menu *living-list*))

(defvar mw)


(defun mw-button-down (window mouse-state mouse-transition)
  (declare (ignore mouse-state mouse-transition))
  
  (multiple-value-bind (choice menu)
      (pop-up-menu-choose *living-menu*)
    (if menu
	(funcall choice window))))


(defun menudemo ()
  (setq mw (make-window-stream :title "Menu Demo"
			       :left 1000 :bottom 850
			       :width 150 :height 200
			       :activate-p t))
  (clear mw)
  (setf (window-stream-right-button-down mw) 'mw-button-down)
  (enable-window-stream-page-scrolling mw))


(eval-when (eval load)
	   (format t "~2%To run:  (menudemo)~2%")
	   (format t "Hold down the right button over the window~%")
	   (format t "to select a menu item.  The item will then~%")
	   (format t "be written upon the window.~2%")
	   (format t "To destroy the window:  (flush mw)~2%"))

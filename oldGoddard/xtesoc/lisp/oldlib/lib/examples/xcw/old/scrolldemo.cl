;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.

(defpackage :user (:use :cw))
(in-package :user)

#+cw-news
(unless (common-windows-initialized-p)
  (initialize-common-windows))

(defvar scrollw nil)
(defvar sanfran nil)

(defun scrolldemo ()
  (let ((*default-mat-width* 5)
	(*default-border-width* 3))
    (multiple-value-bind (dl db dr dt)
	(default-window-stream-frame :titled-p t :scrollable-p t)
    
      (setq scrollw (make-window-stream :parent *root-window*
					:title "Scrolling Bitmap Demo"
					:width (+ 288 5 5 dl dr)
					:height (+ 225 5 5 db dt)
					:left 900 :bottom 400)))
    
    (unless sanfran
      (setq sanfran (read-bitmap "~cox/X/sanfrancisco.xbm")))

    (modify-window-stream-method scrollw :repaint :after 'sf-repaint)
    
    (setf (window-stream-extent-width scrollw) 1152)
    (setf (window-stream-extent-height scrollw) 900)
    (enable-window-stream-extent-scrolling scrollw)
    
    (repaint scrollw)
    (expose scrollw)
    scrollw))

(defun sf-repaint (ws region)
  (declare (optimize (speed 3))
	   (type window-stream ws)
	   (type region region))
  
  (if* (not region)
     then
	  (let ((x-offset (window-stream-x-offset ws))
		(y-offset (window-stream-y-offset ws))
		(inner-width (window-stream-inner-width ws))
		(inner-height (window-stream-inner-height ws)))
	    (declare (fixnum x-offset y-offset inner-width inner-height))
	    
	    (bitblt sanfran x-offset y-offset ws x-offset y-offset
		    inner-width inner-height))
     else
	  (let ((left (region-left region))
		(bottom (region-bottom region)))
	    (declare (fixnum left bottom))
	    (bitblt sanfran left bottom ws left bottom (region-width region)
		    (region-height region)))))

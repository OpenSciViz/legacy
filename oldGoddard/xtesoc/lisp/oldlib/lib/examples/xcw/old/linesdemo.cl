;;; Copyright (c) 1990 Franz Inc, Berkeley, Ca.
;;;
;;; Permission is granted to any individual or institution to use, copy,
;;; modify, and distribute this software, provided that this complete
;;; copyright and permission notice is maintained, intact, in all copies and
;;; supporting documentation.
;;;
;;; Franz Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;
;;;  Line Drawing Demo
;;;
;;;  Aaron Endelman
;;;  (c) 1987 by Sun Microsystems, Inc.
;;;

(defpackage :user (:use :cw))
(in-package :user)

#+cw-news
(unless (common-windows-initialized-p)
  (initialize-common-windows))

(defparameter linesw      nil)

(defun linesdemo ()
  (setq linesw (make-window-stream
		  :left 200 :bottom 50 :width 300 :height 300
		  :title "Line Drawing Demo"))
  
  (setf (window-stream-extent-width  linesw) 400)
  (setf (window-stream-extent-height linesw) 500)
  (enable-window-stream-extent-scrolling linesw)
  (modify-window-stream-method linesw :repaint :after 'repaint-lines)
  
  (repaint linesw)
  (activate linesw))


(defun repaint-lines (w region)
  (if* region
     then (clear-area w region)
     else (clear-rectangle-xy w 0 0 400 500))
  (with-graphics-batching
    (draw-line-xy w 0 0 400 500   :color green :dashing '(5 10 15 20))
    (draw-line-xy w 0 500 400 0   :color green :dashing '(5 10 15 20))
    (draw-line-xy w 200 0 200 500 :color purple)
    (draw-line-xy w 0 250 400 250 :color purple)
    (draw-rectangle-xy w 50 50 300 400 :color red :dashing 10 :brush-width 4)
    (dolist (r '(10 20 30 40 50))
      (let ((dashing (/ r 5))
	    (brush-width (- 5 (/ r 10))))
	(dolist (x '(100 200 300))
	  (dolist (y '(100 200 300 400))
	    (draw-circle-xy w x y r
			    :color blue
			    :dashing dashing
			    :brush-width brush-width)))))))


(format t "~%To start:  (linesdemo)~2%")

;;; $Header: hosts.cl,v 1.1 1993/08/31 04:56:33 layer Exp $
;;;
;;; This is an example logical host translations database.  It is used by
;;; load-logical-pathname-translations to lookup the translations for a
;;; given logical host.

;; the format of items in this file is
;;   - host
;;   - translation
;; the host form is not evaluated, the translation form is evaluated.

#|
"foo"
(list "**;*.*" "/foo/**/*.*")

"bar"
(list "**;*.*" "foo:**;*.*")
|#

[ DESCRIPTIONS File Last Updated: 15-May-96 16:03:12 GMT ]

  This directory contains all the publicly available patches for the
following product, version, architecture, and distribution directory:

		Allegro CL
		4.2
		sparc-solaris2
		lib/update/

  There are two types of files in this directory:

DESCRIPTIONS
 -- This information file.

patch<n><v>.fasl
 -- (where <n> is a non-negative number) and <v> is a version number, in
    various formats depending on Allegro CL version.  These are the actual
    patch files.  Descriptions of the individual patches are below.  You
    can take as many or as few of these patch files as you want or think
    you need.  These patch files should be installed in the
    lib/update/ subdirectory of your Allegro CL distribution
    directory (the directory into which the Allegro CL distribution
    tape was read).


  Patches can be loaded either all together automatically at build
time or individually by hand after the lisp is built.  We recommend
patches be loaded automatically at build time.

Automatic Patch Loading

  The way to automatically load all the patches you have is to place
all the "patch<n><v>.fasl" patches you want in the lib/update/
subdirectory of your Allegro CL distribution directory (the directory
into which the Allegro CL distribution tape or CD was read).  Once these
files are in place, you can rebuild your Allegro CL by running config.
The patch files will be loaded in at the appropriate part of the build
process.


Manual Patch Loading

  To load a patch file manually, one simply loads the "patch<n><v>.fasl"
file using the Common Lisp LOAD function.  For versions of Allegro CL
4.3 and later, use the SYS:LOAD-PATCHES function.

Manual Patch Loading Example:

 user(2): :ld code/excl-s         ; (needed for 4.3)
 ;   Fast loading code/excl-s.fasl
 ;;; Installing excl-s patch, version 1
 user(3): (sys:load-patches "patch" "update/")
 ; Fast loading
 ;    <...>/update/patch0123-01.fasl
 ; Fast loading
 ;     <...>/update/patch0123-02.fasl
 ; Fast loading
 ;    <...>/update/patch0124-01.fasl
 ; Fast loading
 ;    <...>/update/patch0125-01.fasl
 nil
 user(4): 


Patch File Descriptions

  This section lists the descriptions of the patch files available in
this directory.  All patches publicly available as of the date on
the top of this file are listed here.  The descriptions listed are
admittedly cryptic and may not reveal all the problems that the
patches fix.  Also not every "known" problem is fixable by a patch
file.  Contact Franz Inc. (see below) if you have questions about
patches or have a problem which seems not to be fixed by any of the
patches in this directory.

  Gaps in the numbering do not necessarily indicate missing patches.

  If you have any questions about any patch or the patch loading
procedure, please contact Franz Inc. by email to "bugs@franz.com", or
"uunet!franz!bugs"; or by phone to (510) 548-3600 (during Pacific Time
work hours); or by fax to (510) 548-8253.




_ _ _ _ _ Descriptions Listing _ _ _ _ _

Patches that have been updated:

patch0257.3 : Fix sparc peephole overflow bug
patch0257.2 : Superseded patch, check for updated version of this patch
patch0256.2 : Allow Unix fifos to have non-blocking open/read.
(requires coreq fio.o and qrio.o patches)
patch0250.2 : Remove some compiler infinite loops
patch0244.2 : Remove save-image reference in dumplisp not-available error msg
patch0240.2 : Fix reading/scaling of denormalized floats, singles and doubles
patch0238.5 : Speed up vector sequence functions
patch0238.4 : Superseded patch, check for updated version of this patch
patch0238.3 : Superseded patch, check for updated version of this patch
patch0238.2 : Superseded patch, check for updated version of this patch
patch0237.2 : Make funcalls within CLOS code faster, smaller
patch0236.2 : Fix chdir for certain *default-pathname-defaults*
patch0233.2 : Fix profiler illegal svref when shared libraries used
patch0232.3 : Fix binding of comp:internal-optimize-switch
patch0232.2 : Superseded patch, check for updated version of this patch
patch0231.6 : Provide more aggressive virtual-register linking
patch0231.5 : Superseded patch, check for updated version of this patch
patch0231.4 : Superceded patch, check for updated version of this patch
patch0231.3 : Superceded patch, check for updated version of this patch
patch0231.2 : Superceded patch, check for updated version of this patch
patch0230.3 : Provide for tight loops (one branch instead of two)
patch0230.2 : Superseded patch, check for updated version of this patch
patch0226.4 : underlying support for unboxed defctype slot accesses
patch0226.3 : Superseded patch, check for updated version of this patch
patch0226.2 : Superseded patch, check for updated version of this patch
patch0224.3 : Fix fixnum/bignum/excl::foreign passing to direct-call ff
patch0224.2 : Superseded patch, check for updated version of this patch
patch0223.2 : reduce fasl-write hashtable growth from make-load-form
patch0219.5 : Faster, less consing floor_2op_1ret, ceiling_2op_1ret, round_2op_1ret
patch0219.4 : Superceded patch, check for updated version of this patch
patch0219.3 : Superceded patch, check for updated version of this patch
patch0219.2 : Superceded patch, check for updated version of this patch
patch0217.8 : fix register allocation bugs
patch0217.7 : Superseded patch, check for updated version of this patch
patch0217.6 : Superseded patch, check for updated version of this patch
patch0217.5 : Superseded patch, check for updated version of this patch
patch0217.4 : Superceded patch, check for updated version of this patch
patch0217.3 : Superceded patch, check for updated version of this patch
patch0217.2 : Superceded patch, check for updated version of this patch
patch0216.3 : fix for foreign-arguments alignment problem
patch0216.2 : Superceded patch, check for updated version of this patch
patch0213.3 : Fix stack-consed &rest arg interactions w/tail-merging
patch0213.2 : Superseded patch, check for updated version of this patch
patch0207.4 : Speed up reading, pretty-printing for 4.2
patch0207.3 : Superseded patch, check for updated version of this patch
patch0207.2 : Superseded patch, check for updated version of this patch
patch0206.4 : Implement stack-allocated (simple-array t (*))
patch0206.3 : Superseded patch, check for updated version of this patch
patch0206.2 : Superceded patch, check for updated version of this patch
patch0205.10 : Speed up sequence functions for 4.2
patch0205.9 : Superseded patch, check for updated version of this patch
patch0205.8 : Superseded patch, check for updated version of this patch
patch0205.7 : Superseded patch, check for updated version of this patch
patch0205.6 : Superseded patch, check for updated version of this patch
patch0205.5 : Superseded patch, check for updated version of this patch
patch0205.4 : Superseded patch, check for updated version of this patch
patch0205.3 : Superseded patch, check for updated version of this patch
patch0205.2 : Superseded patch, check for updated version of this patch
patch0197.4 : Optimize char= (2-arg), char<, etc. compilation
patch0197.3 : Superseded patch, check for updated version of this patch
patch0197.2 : Superceded patch, check for updated version of this patch
patch0195.3 : Add malloc info to room t (needs new coreq .o patch)
patch0195.2 : Add malloc info to room t (needs coreq .o patches)
patch0194.4 : bogus call-undefined-fnc warnings for defmethods
patch0194.3 : Superceded patch, check for updated version of this patch
patch0194.2 : Superceded patch, check for updated version of this patch
patch0193.2 : fix nonsimple array print, stream-write-string bus error on non-array
patch0190.4 : fix multiple-value-bind register allocation problem
patch0190.3 : Superseded patch, check for updated version of this patch
patch0190.2 : Superceded patch, check for updated version of this patch
patch0190.1 : Superceded patch, check for updated version of this patch
patch0178.9 : make compiled eql (and equal) faster
patch0178.8 : Superseded patch, check for updated version of this patch
patch0178.7 : Superseded patch, check for updated version of this patch
patch0178.6 : Superseded patch, check for updated version of this patch
patch0178.5 : Superseded patch, check for updated version of this patch
patch0178.4 : Superceded patch, check for updated version of this patch
patch0178.3 : Superceded patch, check for updated version of this patch
patch0178.2 : Superceded patch, check for updated version of this patch

Regular patches:

patch0259 : Allow unboxed setf of cstruct slot
patch0257 : Superseded patch, check for updated version of this patch
patch0256 : Superseded patch, check for updated version of this patch
patch0255 : Fix resize-areas to accept bignum values
patch0253 : Make map work with 'simple-bit-vector spec
patch0250 : Superseded patch, check for updated version of this patch
patch0249 : Make gcd on one argument return absolute value
patch0248 : Don't force boxing into dead variables
patch0247 : Keep rationalize from overflowing due to divergence
patch0246 : Fix decode-float inaccuracy
patch0245 : format should not rebind *standard-output*
patch0244 : Superseded patch, check for updated version of this patch
patch0243 : Fix dribble-bug for restricted runtime, add *dribble-bug-hooks*
patch0241 : Set *, **, ***, etc. before printing result, in case of error
patch0240 : Superseded patch, check for updated version of this patch
patch0239 : Make #n# reader macros scan standard-object inst slots (nonportable extension)
patch0238 : Superseded patch, check for updated version of this patch
patch0237 : Superseded patch, check for updated version of this patch
patch0236 : Superseded patch, check for updated version of this patch
patch0235 : 4X speedup in string compare functions
patch0234 : Speed up values and values-list
patch0233 : Superseded patch, check for updated version of this patch
patch0232 : Superceded patch, check for updated version of this patch
patch0231 : Superceded patch, check for updated version of this patch
patch0230 : Superceded patch, check for updated version of this patch
patch0229 : Invalidate class-prototype on class redefinition
patch0227 : Detect superclass loops caused by forward-referenced-class definition
patch0226 : Superceded patch, check for updated version of this patch
patch0225 : Allow usage of -B option at runtime startup
patch0224 : Superceded patch, check for updated version of this patch
patch0223 : Superseded patch, check for updated version of this patch
patch0222 : Speed up subst for 4.2
patch0221 : printer is sometimes uninterruptible
patch0220 : Fix register allocation for certain tail-merged arg setups
patch0219 : Make floor_2op_1ret, ceiling_2op_1ret, round_2op_1ret cons less
patch0218 : namestring should return logical-pathname namestrings
patch0217 : Superceded patch, check for updated version of this patch
patch0216 : Superceded patch, check for updated version of this patch
patch0213 : Superceded patch, check for updated version of this patch
patch0212 : certain setf expanders failed to pass &env
patch0211 : fix with-package-iterator
patch0209 : excl::pure-string fails on non-simple string
patch0208 : Speed up array functions for 4.2
patch0207 : Superseded patch, check for updated version of this patch
patch0206 : Superceded patch, check for updated version of this patch
patch0205 : Superceded patch, check for updated version of this patch
patch0204 : Fix stream-write-string for synonym-streams
patch0203 : Pulled patch, ask bug@franz.com for full-file defsys patch
patch0202 : Fix to allow parse-integer to accept null end argument
patch0200 : bug3071 --
Adds some controls to kill cl when the initial terminal-io stream
is closed.  This patch is somewhat experimental and may be updated
in the future.
patch0198 : fix bad-stack behavior after (dumplisp :checkpoint t)
patch0197 : Superceded patch, check for updated version of this patch
patch0196 : add type propagator for abs
patch0195 : Superceded patch, check for updated version of this patch
patch0194 : Superceded patch, check for updated version of this patch
patch0193 : Superceded patch, check for updated version of this patch
patch0192 : bug with eql-specialized meths on MOP gfs
patch0191 : instance slot order should reverse cpl order
patch0190 : Superceded patch, check for updated version of this patch
patch0189 : Fix for expansions of macros within with-slots 
patch0188 : Have MACHINE-VERSION and SOFTWARE-VERSION return meaningful values
patch0187 : Fix map-into with no argument sequences
patch0186 : Improve performance of (setf named-slot) within with-slots



(defvar fi:inferior-common-lisp-mode-map nil "\
The inferior-common-lisp major-mode keymap.")

(defvar fi:inferior-common-lisp-mode-super-key-map nil "\
Used for super-key processing in inferior-common-lisp mode.")

(defvar fi:inferior-franz-lisp-mode-map nil "\
The inferior-franz-lisp major-mode keymap.")

(defvar fi:inferior-franz-lisp-mode-super-key-map nil "\
Used for super-key processing in inferior-franz-lisp mode.")

(defvar fi:lisp-listener-mode-map nil "\
The tcp-lisp major-mode keymap.")

(defvar fi:lisp-listener-mode-super-key-map nil "\
Used for super-key processing in tcp-lisp mode.")

(defvar fi:common-lisp-mode-map nil "\
Major mode map used when editing Common Lisp source.")

(defvar fi:franz-lisp-mode-map nil "\
Major mode map used when editing Franz Lisp source.")

(defvar fi:emacs-lisp-mode-map nil "\
Major mode map used when editing GNU Emacs Lisp source.")

(defvar fi:lisp-mode-syntax-table nil "\
The value of which is the syntax table for all Lisp modes, except Emacs
Lisp mode.")

(defvar fi:emacs-lisp-mode-syntax-table nil "\
The value of which is the syntax table for Emacs Lisp mode.")

(defvar fi:common-lisp-file-types (quote (".cl" ".lisp" ".lsp")) "\
*A list of the file types which are automatically put in
fi:common-lisp-mode.")

(defvar fi:lisp-do-indentation t "\
*When non-nil, do FI-style indentation in Lisp modes.")

(defvar fi:subprocess-mode nil "\
Non-nil when buffer has a subprocess.")

(defvar fi:common-lisp-mode-hook (function (lambda nil (when (not (fi:member-equal "; pkg:" mode-line-process)) (setq mode-line-process (append mode-line-process (quote ((fi:package ("; pkg:" fi:package))))))) (when (not (fi:member-equal "; rt:" mode-line-process)) (setq mode-line-process (append mode-line-process (quote ((fi:readtable ("; rt:" fi:readtable))))))))) "\
*The initial value of this hook, which is run whenever a Lisp mode is
entered, causes the `package' and readtable (if any) to be displayed in the
mode line.  It uses MODE-LINE-PROCESS, which has no use in non-subprocess
buffers.")

(defvar fi:in-package-regexp nil "\
*If non-nil, the regular expression that describes the IN-PACKAGE form,
for purposes of tracking package changes in a subprocess Lisp buffer.  The
value of this is taken from fi:default-in-package-regexp in Lisp subprocess
buffers, but is nil elsewhere.")

(make-variable-buffer-local (quote fi:in-package-regexp))

(defvar fi:default-in-package-regexp "(in-package\\>\\|:pa\\>\\|:pac\\>\\|:pack\\>\\|:packa\\>\\|:packag\\>\\|:package\\>" "\
*The regular expression matching the Lisp expression to change the
current package.  The two things this must match are the IN-PACKAGE macro
form and all the possible instances of the :package top-level command.
If nil, no automatic package tracking will be done.")

(defun fi:inferior-common-lisp-mode (&optional mode-hook &rest mode-hook-args) "\
Major mode for interacting with Common Lisp subprocesses.
The keymap for this mode is bound to fi:inferior-common-lisp-mode-map:
\\{fi:inferior-common-lisp-mode-map}
Entry to this mode runs the following hooks:

	fi:lisp-mode-hook
	fi:subprocess-mode-hook
	fi:inferior-common-lisp-mode-hook

in the above order.

When calling from a program, arguments are MODE-HOOK and MODE-HOOK-ARGS,
the former is applied to the latter just after killing all local variables
but before doing any other mode setup." (interactive) (byte-code "Ɉ� �� �	\"�̉�͉��!�� �?�+ � ����\"�?�: �� \"���!���Չ�����#�" [mode-hook mode-hook-args major-mode mode-name fi:lisp-mode-syntax-table fi:inferior-common-lisp-mode-super-key-map fi:inferior-common-lisp-mode-map fi:subprocess-super-key-map fi:lisp-indent-hook-property nil kill-all-local-variables apply fi:inferior-common-lisp-mode "Inferior Common Lisp" set-syntax-table fi::lisp-subprocess-mode-variables make-keymap fi::subprocess-mode-super-keys sub-lisp fi::inferior-lisp-mode-commands use-local-map fi:common-lisp-indent-hook run-hooks fi:lisp-mode-hook fi:subprocess-mode-hook fi:inferior-common-lisp-mode-hook] 13))

(defun fi:inferior-franz-lisp-mode (&optional mode-hook &rest mode-hook-args) "\
Major mode for interacting with Franz Lisp subprocesses.
The keymap for this mode is bound to fi:inferior-franz-lisp-mode-map:
\\{fi:inferior-franz-lisp-mode-map}
Entry to this mode runs the following hooks:

	fi:lisp-mode-hook
	fi:subprocess-mode-hook
	fi:inferior-franz-lisp-mode-hook

in the above order.

When calling from a program, arguments are MODE-HOOK and MODE-HOOK-ARGS,
the former is applied to the latter just after killing all local variables
but before doing any other mode setup." (interactive) (byte-code "Ɉ� �� �	\"�̉�͉��!�� �?�+ � ����\"�?�: �� \"���!���Չ�����#�" [mode-hook mode-hook-args major-mode mode-name fi:lisp-mode-syntax-table fi:inferior-franz-lisp-mode-super-key-map fi:inferior-franz-lisp-mode-map fi:subprocess-super-key-map fi:lisp-indent-hook-property nil kill-all-local-variables apply fi:inferior-franz-lisp-mode "Inferior Franz Lisp" set-syntax-table fi::lisp-subprocess-mode-variables make-keymap fi::subprocess-mode-super-keys sub-lisp fi::inferior-lisp-mode-commands use-local-map fi:franz-lisp-indent-hook run-hooks fi:lisp-mode-hook fi:subprocess-mode-hook fi:inferior-franz-lisp-mode-hook] 13))

(defun fi:lisp-listener-mode (&optional mode-hook) "\
Major mode for interacting with Common Lisp over a socket.
The keymap for this mode is bound to fi:lisp-listener-mode-map:
\\{fi:lisp-listener-mode-map}
Entry to this mode runs the following hooks:

	fi:lisp-mode-hook
	fi:subprocess-mode-hook
	fi:lisp-listener-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "Ȉ� �� �!�ˉ�̉��!�� �?�* � ����\"�?�7 �� \"���!���ԉ�����#�" [mode-hook major-mode mode-name fi:lisp-mode-syntax-table fi:lisp-listener-mode-super-key-map fi:lisp-listener-mode-map fi:subprocess-super-key-map fi:lisp-indent-hook-property nil kill-all-local-variables funcall fi:lisp-listener-mode "TCP Common Lisp" set-syntax-table fi::lisp-subprocess-mode-variables make-keymap fi::subprocess-mode-super-keys tcp-lisp fi::lisp-listener-mode-commands use-local-map fi:common-lisp-indent-hook run-hooks fi:lisp-mode-hook fi:subprocess-mode-hook fi:lisp-listener-mode-hook] 13))

(defun fi:common-lisp-mode (&optional mode-hook) "\
Major mode for editing Lisp code to run in Common Lisp.
The keymap for this mode is bound to fi:common-lisp-mode-map:
\\{fi:common-lisp-mode-map}
Entry to this mode runs the following hooks:

	fi:lisp-mode-hook
	fi:common-lisp-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ň� �� �!�ˉ�̉��!�� �� �?�. � �����#��!���Ӊ����\"�" [mode-hook major-mode mode-name fi:lisp-mode-syntax-table fi:common-lisp-mode-map nil fi::process-name fi::common-lisp-backdoor-main-process-name fi:lisp-indent-hook-property kill-all-local-variables funcall fi:common-lisp-mode "Common Lisp" set-syntax-table fi::lisp-edit-mode-setup fi:parse-mode-line-and-package make-keymap fi::lisp-mode-commands use-local-map fi:common-lisp-indent-hook run-hooks fi:lisp-mode-hook fi:common-lisp-mode-hook] 11))

(defun lisp-mode (&optional mode-hook) "\
See fi:common-lisp-mode.  This function is here so that set-auto-mode
will go into the FI Common Lisp mode when ``mode: lisp'' appears in
the file modeline." (interactive) (byte-code "���!�" [mode-hook nil fi:common-lisp-mode] 2))

(defun common-lisp-mode (&optional mode-hook) "\
See fi:common-lisp-mode.  This function is here so that set-auto-mode
will go into the FI Common Lisp mode when ``mode: common-lisp'' appears in
the file modeline." (interactive) (byte-code "���!�" [mode-hook nil fi:common-lisp-mode] 2))

(defun fi:franz-lisp-mode (&optional mode-hook) "\
Major mode for editing Lisp code to run in Franz Lisp.
The keymap for this mode is bound to fi:franz-lisp-mode-map:
\\{fi:franz-lisp-mode-map}
Entry to this mode runs the following hooks:

	fi:lisp-mode-hook
	fi:franz-lisp-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ň� �� �!�ˉ�̉��!�� �� �?�. � �����#��!���Ӊ����\"�" [mode-hook major-mode mode-name fi:lisp-mode-syntax-table fi:franz-lisp-mode-map nil fi::process-name fi:franz-lisp-process-name fi:lisp-indent-hook-property kill-all-local-variables funcall fi:franz-lisp-mode "Franz Lisp" set-syntax-table fi::lisp-edit-mode-setup fi:parse-mode-line-and-package make-keymap fi::lisp-mode-commands use-local-map fi:franz-lisp-indent-hook run-hooks fi:lisp-mode-hook fi:franz-lisp-mode-hook] 11))

(defun fi:emacs-lisp-mode (&optional mode-hook) "\
Major mode for editing Lisp code to run in Emacs Lisp.
The keymap for this mode is bound to fi:emacs-lisp-mode-map:
\\{fi:emacs-lisp-mode-map}
Entry to this mode runs the fi:emacs-lisp-mode-hook hook.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ň� �� �!�ɉ�ʉ��!�� �?�+ � �����#��!�Љ���!�" [mode-hook major-mode mode-name fi:emacs-lisp-mode-syntax-table fi:emacs-lisp-mode-map nil fi:lisp-indent-hook-property kill-all-local-variables funcall fi:emacs-lisp-mode "Emacs Lisp" set-syntax-table fi::lisp-edit-mode-setup make-keymap fi::lisp-mode-commands use-local-map fi:emacs-lisp-indent-hook run-hooks fi:emacs-lisp-mode-hook] 9))

(defun fi::lisp-edit-mode-setup nil (byte-code "� �" [fi::lisp-mode-setup-common] 2))

(defun fi::lisp-subprocess-mode-variables nil (byte-code "��!����� �" [fi:subprocess-mode t make-local-variable fi::lisp-mode-setup-common] 3))

(defun fi::lisp-mode-setup-common nil (byte-code "	������!��P����!�����!�Չ���!�։���!�׉	�
�� ��!�؉���!�ى���!�Ή����������&����������&����������&��� ��!�" [fi:in-package-regexp fi:default-in-package-regexp local-abbrev-table lisp-mode-abbrev-table paragraph-start page-delimiter paragraph-separate comment-start comment-start-skip comment-column fi:lisp-do-indentation indent-line-function comment-indent-hook parse-sexp-ignore-comments nil fi::lisp-most-recent-parse-result fi::calculate-lisp-indent-state-temp fi::lisp-indent-state-temp t make-local-variable "^$\\|" ";" ";+[ 	]*" 40 fi:lisp-indent-line fi:lisp-comment-indent list 0 lisp-mode-variables] 18))

(defvar fi:default-package "user" "\
*The name of the package to use as the default package, if there is no
package specification in the mode line.  See fi:parse-mode-line-and-package
for more information.")

(defun fi:parse-mode-line-and-package nil "\
Determine the current package in which the buffer is defined.
The buffer's IN-PACKAGE form and the -*- mode line are parsed for this
information.  This function is automatically called when a Common Lisp
source file is visited, but may be executed explicitly to re-parse the
package.

When using Allegro CL 4.2 or later, the ``Readtable: '' can be used to name
the readtable used for evaluations given to Lisp from emacs." (interactive) (byte-code "Ĉ��!����
���%��" [fi:readtable fi:package fi:default-package t nil fi::parse-mode-line "readtable" "package" fi::parse-package-from-buffer] 7))

(defun fi::parse-mode-line (key &optional default-value messagep fail-hook list-value-ok) (byte-code "����\"����eb��ъ� �`)�#�5 ��!�`���ъ� �`)�#?�= Ȃ� ��!���!�`��b����#?�\\ Ȃ� b��
�#?�n Ȃ� ��!�`��V�� �����#�� ��!�� b���!�`Y�� 	��� �� ��!fU�� ��\\`\"�� �`\"
������
!@!!�)�?�� ?�� Ȃeb��!

?�� Ȃ�
�)��Ȃ	��?�Ȃ-�(��#�-��#�.)�" [case-fold-search t search-string key value found start end nil default-value val list-value-ok fail-hook messagep format "%s:" search-forward "-*-" end-of-line skip-chars-forward " 	" forward-char -3 skip-chars-backward ":" ";" -1 string-to-char "(" buffer-substring 1 downcase symbol-name read-from-string funcall message "%s specification is `%s'" "using default key specification of `%s'"] 26))

(defun fi::parse-package-from-buffer nil (byte-code "eb�����#�?� ��� ��!����#���!�\"�!@9�R ��!�\"�U�K �!��O�O �!�� :�e �@=�e A@9�� �A@!��\"�U�� ��O�� )�� ;�� �,�*�" [pos nil t value start end p-string p name re-search-forward "^(in-package[	 ]*" match-end 0 search-forward ")" match-beginning buffer-substring read-from-string elt symbol-name 58 1 quote] 15))

(setq auto-mode-alist (copy-alist auto-mode-alist))

(defun fi::def-auto-mode (string mode) (byte-code "�	
\"� �\"� 	B
B�)�" [xx string auto-mode-alist mode assoc rplacd] 4))

(fi::def-auto-mode "\\.l$" (quote fi:franz-lisp-mode))

(let ((list fi:common-lisp-file-types)) (while list (fi::def-auto-mode (concat "\\" (car list) "$") (quote fi:common-lisp-mode)) (setq list (cdr list))))

(defvar fi:define-emacs-lisp-mode t "\
*If non-nil, then use the FI supplied mode for editing .el files.")

(when fi:define-emacs-lisp-mode (fi::def-auto-mode "\\.el$" (quote fi:emacs-lisp-mode)) (fi::def-auto-mode "[]>:/]\\..*emacs" (quote fi:emacs-lisp-mode)))

(if (not fi:emacs-lisp-mode-syntax-table) (let ((i 0)) (setq fi:emacs-lisp-mode-syntax-table (make-syntax-table)) (while (< i 48) (modify-syntax-entry i "_   " fi:emacs-lisp-mode-syntax-table) (setq i (1+ i))) (setq i (1+ 57)) (while (< i 65) (modify-syntax-entry i "_   " fi:emacs-lisp-mode-syntax-table) (setq i (1+ i))) (setq i (1+ 90)) (while (< i 97) (modify-syntax-entry i "_   " fi:emacs-lisp-mode-syntax-table) (setq i (1+ i))) (setq i (1+ 122)) (while (< i 128) (modify-syntax-entry i "_   " fi:emacs-lisp-mode-syntax-table) (setq i (1+ i))) (modify-syntax-entry 32 "    " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 9 "    " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 10 ">   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 12 ">   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 59 "<   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 96 "'   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 39 "'   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 44 "'   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 46 "'   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 35 "'   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 34 "\"    " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 92 "\\   " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 40 "()  " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 41 ")(  " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 91 "(]  " fi:emacs-lisp-mode-syntax-table) (modify-syntax-entry 93 ")[  " fi:emacs-lisp-mode-syntax-table)))

(if (not fi:lisp-mode-syntax-table) (progn (setq fi:lisp-mode-syntax-table (copy-syntax-table fi:emacs-lisp-mode-syntax-table)) (modify-syntax-entry 42 "w   " fi:lisp-mode-syntax-table) (modify-syntax-entry 46 "w   " fi:lisp-mode-syntax-table) (modify-syntax-entry 124 "\"   " fi:lisp-mode-syntax-table) (modify-syntax-entry 91 "_   " fi:lisp-mode-syntax-table) (modify-syntax-entry 93 "_   " fi:lisp-mode-syntax-table)))

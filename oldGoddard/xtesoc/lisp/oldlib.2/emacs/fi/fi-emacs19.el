;; gnu emacs v19 specific hacks for the Franz Inc. emacs-lisp interface
;;
;; Copyright (c) 1993 Franz Inc, Berkeley, Ca.
;;
;; Permission is granted to any individual or institution to use, copy,
;; modify, and distribute this software, provided that this complete
;; copyright and permission notice is maintained, intact, in all copies and
;; supporting documentation.
;;
;; Franz Incorporated provides this software "as is" without
;; express or implied warranty.
;;
;; $Header: fi-emacs19.el,v 2.2 1993/07/23 07:02:36 layer Exp $

;; nothing yet...

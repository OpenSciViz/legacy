
(defvar fi:su-mode-map nil "\
The su major-mode keymap.")

(defvar fi:su-mode-super-key-map nil "\
Used for super-key processing in su mode.")

(defvar fi:remote-su-mode-map nil "\
The su major-mode keymap.")

(defvar fi:remote-su-mode-super-key-map nil "\
Used for super-key processing in su mode.")

(defvar fi:su-prompt-pattern "^[-_.a-zA-Z0-9]*[#$%>] *" "\
*Regexp used by Newline command in su mode to match subshell prompts.
Anything from beginning of line up to the end of what this pattern matches
is deemed to be prompt, and is not re-executed.")

(defvar fi:su-initial-input "stty -echo nl tabs
" "*The initial input sent to the su subprocess, after the first prompt
is seen.")

(defun fi:su-mode (&optional mode-hook) "\
Major mode for interacting with an inferior su.
The keymap for this mode is bound to fi:su-mode-map:
\\{fi:su-mode-map}
Entry to this mode runs the following hooks:

	fi:subprocess-mode-hook
	fi:su-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ƈ� �� �!�ɉ�ʉ�?�# � ����\"�?�1 �� �#���!������\"�" [mode-hook major-mode mode-name fi:su-mode-super-key-map fi:su-mode-map fi:subprocess-super-key-map nil kill-all-local-variables funcall fi:su-mode "Su" make-keymap fi::subprocess-mode-super-keys shell fi::subprocess-mode-commands use-local-map run-hooks fi:subprocess-mode-hook fi:su-mode-hook] 10))

(defun fi:remote-su-mode (&optional mode-hook) "\
Major mode for interacting with an remote inferior su.
The keymap for this mode is bound to fi:remote-su-mode-map:
\\{fi:remote-su-mode-map}
Entry to this mode runs the following hooks:

	fi:subprocess-mode-hook
	fi:rlogin-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ƈ� �� �!�ɉ�ʉ�?�# � ����\"�?�1 �� �#���!������\"�" [mode-hook major-mode mode-name fi:remote-su-mode-super-key-map fi:remote-su-mode-map fi:subprocess-super-key-map nil kill-all-local-variables funcall fi:remote-su-mode "Remote Su" make-keymap fi::subprocess-mode-super-keys rlogin fi::subprocess-mode-commands use-local-map run-hooks fi:subprocess-mode-hook fi:rlogin-mode-hook] 10))

(defun fi:su (&optional buffer-number) "\
Start an su in a buffer whose name is determined from the optional
prefix argument BUFFER-NUMBER.  Su buffer names start with `*su*'
and end with an optional \"<N>\".  If BUFFER-NUMBER is not given it defaults
to 1.  If BUFFER-NUMBER is 1, then the trailing \"<1>\" is omited.  If
BUFFER-NUMBER is < 0, then the first available buffer name is chosen (a
buffer with no process attached to it." (interactive "p") (byte-code "�����	
����&	�" [nil buffer-number default-directory fi:su-prompt-pattern fi::make-subprocess "root" fi:su-mode "su" fi::su-filter] 10))

(defun fi:remote-root-login (&optional buffer-number host) "\
Start a remote root rlogin in a buffer whose name is determined from the
optional prefix argument BUFFER-NUMBER and the HOST.  Remote root Rlogin
buffer names start with `*root-HOST*' and end with an optional \"<N>\".  If
BUFFER-NUMBER is not given it defaults to 1.  If BUFFER-NUMBER is 1, then
the trailing \"<1>\" is omited.  If BUFFER-NUMBER is < 0, then the first
available buffer name is chosen (a buffer with no process attached to it.

The host name is read from the minibuffer." (interactive "p
sRemote host: ") (byte-code "�������
\"��
��E�&	)�" [fi:subprocess-env-vars nil host buffer-number default-directory fi:su-prompt-pattern (("EMACS" . "t") ("TERM" . "dumb") ("DISPLAY" getenv "DISPLAY")) fi::make-subprocess format "root-%s" fi:remote-su-mode "rlogin" "-l" "root" fi::su-filter] 12))

(defvar fi::su-password nil)

(defun fi::su-filter (process output) "\
Filter for `fi:su' subprocess buffers.
Watch for the first shell prompt from the su, then send the
string bound to fi:su-initial-input, and turn ourself off." (byte-code "�	\"���!q���	\"�  � ���
�P\"�> �� ��!)�> ��\"�ŉ��> �\")�" [process output fi::su-password t subprocess-prompt-pattern nil fi:su-initial-input fi::subprocess-filter process-buffer string-match "assword" fi::read-password send-string "
" beginning-of-line looking-at set-process-filter] 11))

;; $Header: fi-site-load.el,v 1.2 1993/06/29 23:21:01 layer Exp $
;; This file can be loaded at emacs build time to have the emacs-lisp
;; interface preloaded into the image.

(setq fi::build-time t)

(load "fi-site-init")

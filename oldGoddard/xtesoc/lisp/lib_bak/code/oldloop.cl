;;;   -*- Mode: LISP; Syntax: Common-lisp; Lowercase:T -*-

;; $Header: oldloop.cl,v 1.6 1993/11/05 00:12:00 smh Exp $

#+Cloe-Runtime
(car (push "@(#) oldloop.rcl 1.4 90/01/22 18:59:23" system::*module-identifications*))

(provide :oldloop)

(in-package :excl)

(eval-when (compile eval load) (require :loop))

(defparameter *loop-old-universe*
  (let ((w (make-standard-loop-universe
	     :keywords '((named (loop-do-named))
			 (initially (loop-do-initially))
			 (finally (loop-do-finally))
			 (nodeclare (loop-nodeclare))
			 (do (loop-do-do))
			 (doing (loop-do-do))
			 (return (loop-do-return))
			 (collect (loop-list-collection list))
			 (collecting (loop-list-collection list))
			 (append (loop-list-collection append))
			 (appending (loop-list-collection append))
			 (nconc (loop-list-collection nconc))
			 (nconcing (loop-list-collection nconc))
			 (count (loop-sum-collection count integer fixnum))
			 (counting (loop-sum-collection count integer fixnum))
			 (sum (loop-sum-collection sum number number))
			 (summing (loop-sum-collection sum number number))
			 (maximize (loop-maxmin-collection max))
			 (minimize (loop-maxmin-collection min))
			 (maximizing (loop-maxmin-collection max))
			 (minimizing (loop-maxmin-collection min))
			 (always (loop-do-always nil nil))	; Normal, do always
			 (never (loop-do-always nil t))	; Negate the test on always.
			 (thereis (loop-do-thereis nil))
			 (while (loop-do-while nil :while))	; Normal, do while
			 (until (loop-do-while t :until))	; Negate the test on while
			 (when (loop-do-if when nil))	; Normal, do when
			 (if (loop-do-if if nil))	; synonymous
			 (unless (loop-do-if unless t))	; Negate the test on when
			 (with (loop-do-with)))
	     :for-keywords '((= (loop-genera-for-equals))
			     (first (loop-genera-for-first))
			     (in (loop-for-in))
			     (on (loop-for-on))
			     (from (loop-for-arithmetic :from))
			     (downfrom (loop-for-arithmetic :downfrom))
			     (upfrom (loop-for-arithmetic :upfrom))
			     (below (loop-for-arithmetic :below))
			     (to (loop-for-arithmetic :to))
			     (being (loop-for-being)))
	     :iteration-keywords '((for (loop-do-for))
				   (as (loop-do-for))
				   (repeat (loop-do-repeat)))
	     :ansi nil
	     :type-keywords '((fixnum fixnum)
			      (float  float)
			      (flonum float)
			      (integer integer)
			      (number number)
			      (notype t))
	     )))
    (add-loop-path '(hash-elements) 'loop-old-hash-table-iteration-path w
		   :preposition-groups '((:of :in) (:with-key))
		   :inclusive-permitted nil)
    (add-loop-path '(sequence-element sequence-elements) 'loop-sequence-elements-path w
		   :preposition-groups '((:of :in) (:from :downfrom) (:to :downto :below :above) (:by))
		   :inclusive-permitted nil
		   :user-data '(:fetch-function elt
				:size-function length
				:sequence-type sequence
				:element-type t))
    (add-loop-path '(array-element array-elements) 'loop-sequence-elements-path w
		   :preposition-groups '((:of :in) (:from :downfrom) (:to :downto :below :above) (:by))
		   :inclusive-permitted nil
		   :user-data '(:fetch-function aref
				:size-function length
				:sequence-type vector
				:element-type t))
    (add-loop-path '(interned-symbol interned-symbols) 'loop-sequence-elements-path w
		   :preposition-groups '((:in))
		   :inclusive-permitted nil
		   :user-data '(:symbol-types (:internal :external :inherited)))
    (add-loop-path '(local-interned-symbol local-interned-symbols) 'loop-sequence-elements-path w
		   :preposition-groups '((:in))
		   :inclusive-permitted nil
		   :user-data '(:symbol-types (:internal :external)))
    w))


(defun loop-nodeclare (&aux (varlist (loop-pop-source)))
  (unless (do ((l varlist (cdr l))) ((atom l) (null l))
	    (unless (symbolp (car l)) (return nil)))
    (loop-error "The argument to a LOOP NODECLARE clause must be a list of variable names."))
  (setq *loop-nodeclare* (append varlist *loop-nodeclare*)))


;;;; Various FOR/AS Subdispatches


;;;Effectively untouched Genera-compatible version.
(defun loop-genera-for-equals (var val data-type)
  (cond ((loop-tequal (car *loop-source-code*) :then)
	 ;;FOR var = first THEN next
	 (loop-pop-source)
	 (loop-make-iteration-variable var val data-type)
	 `(() (,var ,(loop-get-form)) () ()
	   () () () ()))
	(t (loop-make-iteration-variable var nil data-type)
	   (let ((varval (list var val)))
	     (cond (*loop-emitted-body*
		    (loop-emit-body (loop-make-desetq varval))
		    '(() () () ()))
		   (`(() ,varval () ())))))))


(defun loop-genera-for-first (var val data-type)
  (unless (loop-tequal (car *loop-source-code*) :then)
    (loop-error "LOOP found ~S where THEN expected." (car *loop-source-code*)))
  (loop-pop-source)
  (loop-make-iteration-variable var nil data-type)
  `(() (,var ,(loop-get-form)) () ()
    () (,var ,val) () ()))



(defun loop-old-hash-table-iteration-path (variable data-type prep-phrases)
  (let ((ht-var (loop-gentemp 'loop-hashtab-))
	(next-fn (loop-gentemp 'loop-hashtab-next-))
	(key-var nil)
	(bindings nil)
	(post-steps nil)
	(dummy-var nil))
    #-Genera (setq dummy-var (loop-when-it-variable))	;If we cannot use a NIL in multiple-value-setq varlist.
    (dolist (e prep-phrases)
      (ecase (car e)
	((:of :in) (push `(,ht-var ,(cadr e)) bindings))
	(:with-key (setq key-var (second e)))))
    (unless bindings (loop-error "Missing OF or IN."))
    (unless key-var (setq key-var (loop-gentemp 'loop-hash-elements-key-)))
    (when (consp variable)
      (push `(,variable nil ,data-type) bindings)
      (setq post-steps `(,variable ,(setq variable (loop-gentemp 'loop-hash-elements-value))) data-type nil))
    (when (consp key-var)
      (push `(,key-var nil) bindings)
      (setq post-steps `(,key-var ,(setq key-var (loop-gentemp 'loop-hash-elements-key)) ,@post-steps)))
    (push `(,key-var nil) bindings)
    (push `(,variable nil ,data-type) bindings)
    (push `(with-hash-table-iterator (,next-fn ,ht-var)) *loop-wrappers*)
    `(,bindings ()
      ()
      ()
      (not (multiple-value-setq (,dummy-var ,key-var ,variable) (,next-fn)))
      ,post-steps)))



(eval-when (compile eval load)
  (unless (find-package :loop)
    (make-package :loop :use nil)))


(import '(excl::loop-tequal
	   excl::loop-tmember
	   excl::loop-tassoc
	   excl::loop-finish
	   )
	(find-package 'loop))

(export '(loop::loop
	   loop::loop-finish
	   loop::loop-tequal
	   loop::loop-tmember
	   loop::loop-tassoc
	   loop::define-loop-sequence-path
	   loop::define-loop-path
	   loop::define-loop-macro
	   )
	(find-package 'loop))


(defmacro loop:define-loop-sequence-path (path-name-or-names fetch-function size-function
				     &optional sequence-type element-type)
  "Defines a sequence iteration path.  PATH-NAME-OR-NAMES is either an
atomic path name or a list of path names.  FETCHFUN is a function of
two arguments, the sequence and the index of the item to be fetched.
Indexing is assumed to be zero-origined.  SIZEFUN is a function of
one argument, the sequence; it should return the number of elements in
the sequence.  SEQUENCE-TYPE is the name of the data-type of the
sequence, and ELEMENT-TYPE is the name of the data-type of the elements
of the sequence."
  `(eval-when (eval compile load)
     (add-loop-path ,path-name-or-names 'loop-sequence-elements-path *loop-old-universe*
		    :preposition-groups '((:of :in) (:from :downfrom :upfrom) (:to :downto :below :above) (:by))
		    :inclusive-permitted nil
		    :user-data '(:fetch-function ,fetch-function
				 :size-function ,size-function
				 :sequence-type ,sequence-type
				 :element-type ,element-type))))




(defun add-old-style-loop-method (method-name-or-names method-function list-of-allowable-prepositions method-specific-data)
  (setq method-name-or-names (if (atom method-name-or-names) (list method-name-or-names) method-name-or-names))
  (dolist (method-name method-name-or-names)
    (add-loop-path method-name
		   #'(lambda (iter-var iter-var-data-type prep-phrases &key inclusive user-data)
		       (declare (ignore user-data))
		       (funcall method-function
				method-name iter-var iter-var-data-type prep-phrases
				inclusive list-of-allowable-prepositions method-specific-data))
		   *loop-old-universe*
		   :preposition-groups (mapcar #'list list-of-allowable-prepositions)
		   :inclusive-permitted t)))

(defmacro loop:define-loop-path (method-name-or-names method-function
				 list-of-allowable-prepositions
				 &rest method-specific-data)
  `(eval-when (eval compile load)
     (add-old-style-loop-method
       ',method-name-or-names ',method-function
       ',list-of-allowable-prepositions ',method-specific-data)))


(defmacro loop:loop (&environment env &rest keywords-and-forms)
  (loop-standard-expansion keywords-and-forms env *loop-old-universe*))


(defmacro loop:define-loop-macro (keyword)
  `(defmacro ,keyword (&whole whole-form &rest keywords-and-forms &environment env)
     (declare (ignore keywords-and-forms))
     (loop-translate whole-form env *loop-old-universe*)))

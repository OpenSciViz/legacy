;; -*- mode: common-lisp; package: user -*-
;;
;; Copyright (c) 1993 Franz Inc, Berkeley, CA
;;
;; Auto zoom on error.
;;
;; Licensed users of Allegro CL may include the following macro in their
;; product, provided that product is only compiled with a licensed Allegro
;; CL compiler.
;;
;; $Header: autozoom.cl,v 1.4 1993/11/05 00:10:05 smh Exp $

(in-package :user)

(defmacro with-auto-zoom-and-exit ((place &key (no-unwind t) (exit t))
				   &body body)
  (let ((p (gensym)))
    `(let ((,p ,place))
       (handler-bind
	   ((error
	     #'(lambda (e)
		 (setq tpl::*user-top-level*
		   #'(lambda ()
		       (setq tpl::*user-top-level* nil)
		       (ignore-errors ;;prevent recursion
			(with-standard-io-syntax
			  (let ((*print-readably* nil)
				(*print-miser-width* 40)
				(*print-circle* t)
				(*print-pretty* t)
				(tpl:*zoom-print-level* nil)
				(tpl:*zoom-print-length* nil))
			    (if* (streamp ,p)
			       then (let ((*terminal-io* ,p)
					  (*standard-output* ,p))
				      (tpl:do-command "zoom" :count t :all t))
			       else (with-open-file
					(s ,p :direction :output
					 :if-exists :supersede)
				      (let ((*terminal-io* s)
					    (*standard-output* s))
					(format s "Error: ~a~%~%" e)
					(tpl:do-command "zoom" :count t
							:all t)))))))
		       ,@(when exit
			   `((exit 1 :no-unwind ,no-unwind))))))))
	 ,@body))))

;;; -*- Mode: LISP; Syntax: Common-lisp; Package: CLIM-DEMO; Base: 10 -*-
;; 
;; copyright (c) 1991 Franz Inc, Berkeley, CA  All rights reserved.
;;
;; The software, data and information contained herein are proprietary
;; to, and comprise valuable trade secrets of, Franz, Inc.  They are
;; given in confidence by Franz, Inc. pursuant to a written license
;; agreement, and may be stored and used only in accordance with the terms
;; of such license.
;;
;; Restricted Rights Legend
;; ------------------------
;; Use, duplication, and disclosure of the software, data and information
;; contained herein by any agency, department or entity of the U.S.
;; Government are subject to restrictions of Restricted Rights for
;; Commercial Software developed at private expense as specified in FAR
;; 52.227-19 or DOD FAR Supplement 252.227-7013 (c) (1) (ii), as
;; applicable.
;;
;; $fiHeader$


(in-package 'clim-demo)

"Copyright (c) 1990, International Lisp Associates.  All rights reserved.
"
(defun grid-demo (stream &optional (count 20))
  #-Silica
  (clim:window-expose stream)
  (clim:window-clear stream)
  (let* ((i 0)
	 (width 400)
	 (height 400)
	 (rows 4)
	 (columns 2)
	 (cell-width (round width columns))
	 (cell-height (round height rows))
	 old-col old-row
	 (medium #+silica (silica:sheet-medium stream) #-silica stream))
    (flet ((draw-cell (row col op)
	     #+silica
	     (draw-rectangle* medium (* col cell-width) (* row cell-height)
			      (* (1+ col) cell-width) (* (1+ row) cell-height)
			      :ink (case op
				     (:erase +background+)
				     (:draw +foreground+)))
	     #-Silica
	     (draw-rectangle* (* col cell-width) (* row cell-height)
			      (* (1+ col) cell-width) (* (1+ row) cell-height)
			      :stream stream
			      :ink (case op
					   (:erase *background*)
					   (:draw *foreground*)))))
      (with-output-recording-options
       (stream :record-p nil :draw-p t)
       (clim::tracking-pointer 
	((stream-primary-pointer stream))
	(:pointer-motion (window x y)
			 ;; (princ "X")
			 (force-output)
			 (let* ((cur-col (floor x cell-width))
				(cur-row (floor y cell-height)))
			   (unless (and (eql cur-col old-col)
					(eql cur-row old-row))
			     (when old-col
			       (draw-cell old-row old-col :erase))
			     (draw-cell cur-row cur-col :draw)
			     (setq old-row cur-row old-col cur-col)))
			 (incf i)
			 (when (> i count)
			   (return-from grid-demo))))))))

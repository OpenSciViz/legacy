;;; -*- Mode: LISP; Syntax: Common-lisp; Package: CLIM-DEMO; Base: 10; Lowercase: Yes -*-
;; 
;; copyright (c) 1991 Franz Inc, Berkeley, CA  All rights reserved.
;;
;; The software, data and information contained herein are proprietary
;; to, and comprise valuable trade secrets of, Franz, Inc.  They are
;; given in confidence by Franz, Inc. pursuant to a written license
;; agreement, and may be stored and used only in accordance with the terms
;; of such license.
;;
;; Restricted Rights Legend
;; ------------------------
;; Use, duplication, and disclosure of the software, data and information
;; contained herein by any agency, department or entity of the U.S.
;; Government are subject to restrictions of Restricted Rights for
;; Commercial Software developed at private expense as specified in FAR
;; 52.227-19 or DOD FAR Supplement 252.227-7013 (c) (1) (ii), as
;; applicable.
;;

;; $fiHeader: aaai-demo-driver.lisp,v 1.10 91/09/03 09:26:34 cer Exp $

(in-package :clim-demo)

"Copyright (c) 1990, 1991 Symbolics, Inc.  All rights reserved."
"Copyright (c) 1991, Franz Inc. All rights reserved"

(defvar *demo-root* nil)

(defvar *demos* nil)

(defmacro define-demo (name start-form)
  `(clim-utils:push-unique (cons ,name ',start-form) *demos*
			   :test #'string-equal :key #'car))

(define-demo "Exit" (exit-demo))

(defun exit-demo () (throw 'exit-demo nil))

(define-presentation-type demo-menu-item ())

(defun size-demo-frame (root desired-left desired-top desired-width desired-height)
  (declare (values left top right bottom))
  (multiple-value-bind (left top right bottom)
      (window-inside-edges root)
    (let ((desired-right (+ desired-left desired-width))
	  (desired-bottom (+ desired-top desired-height)))
      (when (> desired-right right)
	(setf desired-right right
	      desired-left (max left (- desired-right desired-width))))
      (when (> desired-bottom bottom)
	(setf desired-bottom bottom
	      desired-top (max top (- desired-bottom desired-height))))
      (values desired-left desired-top desired-right desired-bottom))))

(defun start-demo (&optional (root *demo-root*))
  (setq *demo-root*
    (or root
	(progn
	  (lisp:format t "~&No current value for *DEMO-ROOT*.  Use what value? ")
	  (setq root (eval (lisp:read))))))
  (labels ((demo-menu-drawer (stream type &rest args)
	     (declare (dynamic-extent args))
	     (with-text-style ('(:serif :roman :very-large) stream)
	       (apply #'draw-standard-menu stream type args)))
	   (demo-menu-choose (list associated-window)
	     (with-menu (menu associated-window)
	       (setf (window-label menu)
		     '("Clim Demonstrations" :text-style (:fix :bold :normal)))
	       (menu-choose-from-drawer
		 menu 'demo-menu-item
		 #'(lambda (stream type)
		     (demo-menu-drawer stream type list nil))
		 :cache t
		 :unique-id 'demo-menu :id-test #'eql
		 :cache-value *demos* :cache-test #'equal))))
    (catch 'exit-demo
      (loop
	(let* ((demo-name (demo-menu-choose (nreverse (map 'list #'car *demos*)) root))
	       (demo-form (cdr (assoc demo-name *demos* :test #'string-equal))))
	  (when demo-form
	    ;;--- kludge to keep from blowout when demo not yet loaede
	    (if (fboundp (first demo-form))
		(eval demo-form)
		(beep root))))))))

(defparameter *color-stream-p* t)
(defun color-stream-p (stream)
  #-Genera (declare (ignore stream))
  #-Genera *color-stream-p*		;--- kludge
  #+Genera (if (typep stream 'clim::sheet-window-stream)
	       (slot-value stream 'clim::color-p)
	       *color-stream-p*))

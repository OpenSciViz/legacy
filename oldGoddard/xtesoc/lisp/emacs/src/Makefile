# Master makefile for GNU Emacs 18.59 and Epoch 4.2
#
# Choose the machine on which you want to compile and install
# and make sure the make variables for it are not inside comments and that
# the variables for all other machines are inside comments.  If a machine
# is not mentioned, then it is not known whether or not Emacs and Epoch will
# work properly.
#
# There are three entry points:
#   make all		compile emacs and epoch
#   make install	install emacs and epoch.
#   make distclean	remove files made by "make all"

MAKE = make  # BSD doesn't have it as a default.

emacs_version=18.59
epoch_version=4.2
lemacs_version=19.8

bindir = /usr/local
libdir = /usr/local/lib/
mandir = /usr/local/man/man1

# the names of the library directories
emacs_dir = emacs
epoch_dir = epoch
lemacs_dir = lemacs

CC = cc

###############################################################################

############# sun4

sun4 = m_file=m-sparc.h s_file=s-sunos4-1.h
sun4_4_0 = m_file=m-sparc.h s_file=s-sunos4-0.h CFLAGS=-DNOSTDHDRS

all_sun4:; $(MAKE) $(sun4) all
install_sun4:; $(MAKE) $(sun4) install

all_sun4_4_0:; $(MAKE) $(sun4_4_0) all
install_sun4_4_0:; $(MAKE) $(sun4_4_0) install

############# sun3

sun3 = m_file=m-sun3.h s_file=s-sunos4-1.h
sun3_4_0 = m_file=m-sun3.h s_file=s-sunos4-0.h CFLAGS=-DNOSTDHDRS 

all_sun3:; $(MAKE) $(sun3) all
install_sun3:; $(MAKE) $(sun3) install

all_sun3_4_0:; $(MAKE) $(sun3_4_0) all
install_sun3_4_0:; $(MAKE) $(sun3_4_0) install

############# rs6000

rs6000 = m_file=m-ibmrs6000.h s_file=s-aix3-1.h

all_rs6000:; $(MAKE) $(rs6000) all
install_rs6000:; $(MAKE) $(rs6000) install

############# next

next = m_file=m-next.h s_file=s-bsd4-3.h 'CFLAGS=-bsd -g' 

all_next:; $(MAKE) $(next) all
install_next:; $(MAKE) $(next) install

############# sgi4d

sgi4d = m_file=iris5d.h s_file=s-irix5-0.h LOADLIBES=-lmld \
		INSTALL=../install.sh CP=cp

all_sgi4d:; $(MAKE) $(sgi4d) all
install_sgi4d:; $(MAKE) $(sgi4d) install

############# decstation (not just 3100's)

dec3100 = m_file=m-pmax.h s_file=s-bsd4-3.h DEC=-

all_dec3100:; $(MAKE) $(dec3100) all
install_dec3100:; $(MAKE) $(dec3100) install

###############################################################################

# some versions of make use the SHELL environment variable as the shell
# used for processing commands
SHELL = /bin/sh

# this variable is named thus because it is a hack to get around a bug in
# ultrix's /bin/sh, which I personally reported 4 years ago.  The bug is
# that the "if test ..." construct causes /bin/sh to abort when the -e
# flag is set (as it is when make commands are given to /bin/sh).
DEC = 

CP = cp -p
INSTALL = install
LIB_RESOLV = -lresolv
xmkmf = xmkmf

DESTDIR = 
COMMON = BINDIR=$(DESTDIR)$(bindir) MANDIR=$(DESTDIR)$(mandir)\
	m_file=${m_file} s_file=${s_file}
EMACS = LIBDIR=$(libdir)$(emacs_dir) ILIBDIR=$(DESTDIR)$(libdir)$(emacs_dir)\
	$(COMMON)
EPOCH = LIBDIR=$(libdir)$(epoch_dir) ILIBDIR=$(DESTDIR)$(libdir)$(epoch_dir)\
	$(COMMON)
LEMACS = LIBDIR=$(libdir)$(lemacs_dir) \
	ILIBDIR=$(DESTDIR)$(libdir)$(lemacs_dir) $(COMMON)
xmake = $(MAKE) "INSTALL=$(INSTALL)" CC=${CC} \
	'MFLAGS="CPPFLAGS=$(CPPFLAGS)" "LIB_RESOLV=$(LIB_RESOLV)" \
	 `if test -n "$(CPP)"; then echo CPP=$(CPP); fi` \
	 "xmkmf=${xmkmf}" \
	 "LOADLIBES=$(LOADLIBES)" "XCFLAGS=$(CFLAGS)"'

all_emacs:
	$(DEC)if test -d emacs-$(emacs_version); then (cd emacs-$(emacs_version); $(xmake) $(EMACS)); fi

all_epoch:
	$(DEC)if test -d epoch-$(epoch_version); then (cd epoch-$(epoch_version); $(xmake) $(EPOCH)); fi

all_lemacs:
	$(DEC)if test -d lemacs-$(lemacs_version); then (cd lemacs-$(lemacs_version); $(xmake) $(LEMACS)); fi

install_emacs:
	$(DEC)@if test -f $(DESTDIR)$(bindir)/emacs; then\
		echo "$(DESTDIR)$(bindir)/emacs exists.  Remove it.";\
	fi
	$(DEC)@if test -d $(DESTDIR)$(libdir)$(emacs_dir); then\
		echo "$(DESTDIR)$(libdir)$(emacs_dir) exists.  Remove it.";\
	fi
	mkdirs $(DESTDIR)$(bindir)
	mkdirs $(DESTDIR)$(libdir)
	mkdirs $(DESTDIR)$(mandir)
	$(DEC)if test -d emacs-$(emacvs_version); then\
		(cd emacs-$(emacs_version); $(xmake) install $(EMACS));\
	fi

install_epoch:
	$(DEC)@if test -d epoch-$(epoch_version) -a -f $(DESTDIR)$(bindir)/epoch; then\
		echo "$(DESTDIR)$(bindir)/epoch exists.  Remove it.";\
	fi
	$(DEC)@if test -d epoch-$(epoch_version) -a -d $(DESTDIR)$(libdir)$(epoch_dir); then\
		echo "$(DESTDIR)$(libdir)$(epoch_dir) exists.  Remove it.";\
	fi
	mkdirs $(DESTDIR)$(bindir)
	mkdirs $(DESTDIR)$(libdir)
	mkdirs $(DESTDIR)$(mandir)
	$(DEC)if test -d epoch-$(epoch_version); then\
		(cd epoch-$(epoch_version); $(xmake) install $(EPOCH));\
	fi

install_lemacs:
	$(DEC)@if test -d lemacs-$(lemacs_version) -a -f $(DESTDIR)$(bindir)/lemacs; then\
		echo "$(DESTDIR)$(bindir)/lemacs exists.  Remove it.";\
	fi
	$(DEC)@if test -d lemacs-$(lemacs_version) -a -d $(DESTDIR)$(libdir)$(lemacs_dir); then\
		echo "$(DESTDIR)$(libdir)$(lemacs_dir) exists.  Remove it.";\
	fi
	mkdirs $(DESTDIR)$(bindir)
	mkdirs $(DESTDIR)$(libdir)
	mkdirs $(DESTDIR)$(mandir)
	$(DEC)if test -d lemacs-$(lemacs_version); then\
		(cd lemacs-$(lemacs_version); $(xmake) install $(LEMACS));\
	fi

distclean:
	$(DEC)if test -d emacs-$(emacs_version); then (cd emacs-$(emacs_version); $(xmake) distclean); fi
	$(DEC)if test -d epoch-$(epoch_version); then (cd epoch-$(epoch_version); $(xmake) distclean); fi
	$(DEC)if test -d lemacs-$(lemacs_version); then (cd lemacs-$(lemacs_version); $(xmake) distclean); fi

echo_bindir:
	@echo $(bindir)

echo_libdir:
	@echo $(libdir)

#! /bin/sh -eu
# $Header: clean_emacs.m4,v 1.1 1993/08/31 04:55:43 layer Exp $
#
# This script will clean all but the essential parts of an emacs
# needed to run the emacs/epoch/lemacs binaries

all=

while test $# -gt 0; do
	case $1 in
	-a)	all=xxx
		echo "Will clean .el files that have a corresponding .elc..."
		;;
	*)	echo "$0: unknown argument: $1"
		exit 1
		;;
	esac
	shift
done

if test ! -f clean_emacs.sh -a ! -d src; then
	echo "you are not in the emacs/ directory."
	exit 1
fi

cd src

emacs="emacs-18.59"
epoch="epoch-xxxx"
lemacs="lemacs-19.8"

for emacsdir in $emacs $epoch $lemacs; do
	if test ! -d $emacsdir; then
		continue
	fi
	echo "Cleaning $emacsdir..."
	(cd $emacsdir;
	 for file in *; do
		if test ! -d $file; then
			rm -f $file
			continue
		fi;
		case $file in
		lisp)
			if test -n "$all"; then
				for el in `find lisp -name '*.el' -print`; do
					if test -f ${el}c; then
						rm -f $el
					fi
				done
			fi
			;;
		lock|etc|info)
			;;
		*)	rm -fr $file
			;;
		esac
	 done)
done

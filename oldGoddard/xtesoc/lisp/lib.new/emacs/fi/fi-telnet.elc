
(defvar fi:telnet-mode-map nil "\
The telnet major-mode keymap.")

(defvar fi:telnet-mode-super-key-map nil "\
Used for super-key processing in telnet mode.")

(defvar fi:telnet-image-name "telnet" "\
*Default telnet image to invoke from FI:TELNET.  If the value
is a string then it names the image file or image path that
FI:TELNET invokes.  Otherwise, the value of this variable is given
to funcall, the result of which should yield a string which is the image
name or path.")

(defvar fi:telnet-image-arguments nil "\
*Default telnet image arguments when invoked from FI:TELNET.")

(defvar fi:telnet-prompt-pattern "^[-_.a-zA-Z0-9]*[#$%>] *" "\
*Regexp used by Newline command in telnet mode to match subshell prompts.
Anything from beginning of line up to the end of what this pattern matches
is deemed to be prompt, and is not re-executed.")

(defvar fi:telnet-initial-input "stty -echo nl
" "*The initial input sent to the telnet subprocess, after the first prompt
is seen.")

(defun fi:telnet-mode (&optional mode-hook) "\
Major mode for interacting with an inferior telnet.
The keymap for this mode is bound to fi:telnet-mode-map:
\\{fi:telnet-mode-map}
Entry to this mode runs the following hooks:

	fi:subprocess-mode-hook
	fi:telnet-mode-hook

in the above order.

When calling from a program, argument is MODE-HOOK,
which is funcall'd just after killing all local variables but before doing
any other mode setup." (interactive) (byte-code "ศห  ฬ!อฮ?. ฯ ะั\"าำิ#)?< ีฯ ึ#ื!ศศ	ศ
ฺุู\"" [mode-hook major-mode mode-name fi:telnet-mode-super-key-map map fi:telnet-mode-map fi:subprocess-super-key-map fi:shell-popd-regexp nil fi:shell-pushd-regexp fi:shell-cd-regexp kill-all-local-variables funcall fi:telnet-mode "Telnet" make-keymap fi::subprocess-mode-super-keys rlogin define-key "m" fi:telnet-start-garbage-filter fi::subprocess-mode-commands telnet use-local-map run-hooks fi:subprocess-mode-hook fi:telnet-mode-hook] 11))

(defun fi:telnet (&optional buffer-number host) "\
Start an telnet in a buffer whose name is determined from the optional
prefix argument BUFFER-NUMBER and the HOST.  Telnet buffer names start with
`*HOST*' and end with an optional \"<N>\".  If BUFFER-NUMBER is not given
it defaults to 1.  If BUFFER-NUMBER is 1, then the trailing \"<1>\" is
omited.  If BUFFER-NUMBER is < 0, then the first available buffer name is
chosen (a buffer with no process attached to it.

The host name is read from the minibuffer.

The telnet image file and image arguments are taken from the variables
`fi:telnet-image-name' and `fi:telnet-image-arguments'." (interactive "p
sTelnet to host: ") (byte-code "มศษม
ส
Bห&	)" [fi:subprocess-env-vars nil host buffer-number default-directory fi:telnet-prompt-pattern fi:telnet-image-name fi:telnet-image-arguments (("EMACS" . "t") ("TERM" . "dumb") ("DISPLAY" getenv "DISPLAY")) fi::make-subprocess fi:telnet-mode fi::telnet-filter] 10))

(defun fi:telnet-start-garbage-filter nil "\
Start a filter that removes ^M's at the end of lines." (interactive) (byte-code "ภมยp!ร\"" [nil set-process-filter get-buffer-process fi::telnet-garbage-filter] 4))

(defun fi::telnet-filter (process output) "\
Filter for `fi:telnet' subprocess buffers.
Watch for the first shell prompt from the telnet, then send the
string bound to fi:telnet-initial-input, and turn ourself off." (byte-code "ว	
ร#ศษ
\" ส ห	ฬP\"7 อ ฮ!)0 ฯ	ว\"ห	\"7 q)" [old-buffer process output t password subprocess-prompt-pattern fi:telnet-initial-input fi::subprocess-filter string-match "assword" fi::read-password send-string "
" beginning-of-line looking-at set-process-filter] 10))

(defun fi::telnet-garbage-filter (process output) "\
Filter for telnet subprocess buffers when \"stty nl\" doesn't cause
those nasty ^M's to go away." (byte-code "ฤ	
รร$ q)" [old-buffer process output t fi::subprocess-filter] 5))

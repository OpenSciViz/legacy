
(defvar fi::ss-help "Debugger commands:\\<fi:scan-stack-mode-map>

\\[fi:ss-continue]	:continue
\\[fi:ss-pop]	:pop
\\[fi:ss-reset]	:reset
\\[fi:lisp-delete-pop-up-window]	same as \\[fi:ss-quit] below
\\[fi:ss-set-current]	make frame under the point the current frame
\\[fi:ss-disassemble]	disassemble the function
\\[fi:ss-restart]	restart function (give prefix to specify different form)
\\[fi:ss-toggle-all]	toggle visibility of all frames (by default a subset are visible)
\\[fi:ss-next-frame]	next frame
\\[fi:ss-edit]	edit source corresponding to function
\\[fi:ss-revert-stack]	revert stack from Lisp
\\[fi:ss-unhide-help-text]	Causes this help text to become visible
\\[fi:ss-locals]	display the lexical variables
\\[fi:ss-pprint]	pretty print
\\[fi:ss-quit]	switch back to \"%s\" buffer
\\[fi:ss-return]	return a value
\\[fi:ss-previous-frame]	previous frame

Type \\[fi:ss-hide-help-text] to hide this help summary.

")

(defconst fi:scan-stack-mode-map nil)

(defconst fi:scan-stack-mode-display-help t "\
*If non-nil, then display help text at the beginning of the scan stack
mode buffer.")

(defconst fi::ss-current-frame-regexp "^ ->")

(defconst fi::ss-ok-frame-regexp "^   (")

(defconst fi::ss-show-all-frames nil)

(defvar fi::ss-process-name nil)

(make-variable-buffer-local (quote fi::ss-process-name))

(defvar fi::ss-debugger-from-buffer nil)

(make-variable-buffer-local (quote fi::ss-debugger-from-buffer))

(defun fi:scan-stack (&optional all) "\
Debug a Common Lisp process, which is read, with completion, from the
minibuffer.   The \"Initial Lisp Listener\" is the default process.  The
debugging occurs on a stack scan, created by :zoom on the Common Lisp
process. With argument ALL, do a \":zoom :all t\"." (interactive "P") (byte-code "È� �� 
?� Â p������ F�	E�C�&*�" [process-name from-buffer fi:subprocess-mode nil t fi::ss-show-all-frames all fi:lisp-push-window-configuration fi::buffer-process lep::send-request-in-new-session lep::zoom-session :process-name :all (lambda (stack from-buffer process-name) (byte-code "���	!\"�!�
� � �� �c�� ��	\"����!��. !\"c�??�= ł? � )�� ��	!�� )�" [buffer-name process-name buffer-read-only stack from-buffer nil fi::ss-help fi::ss-debugger-from-buffer fi:scan-stack-mode-display-help fi::ss-current-frame-regexp format "*debugger:%s*" fi::make-pretty-process-name pop-to-buffer toggle-read-only erase-buffer beginning-of-buffer fi:scan-stack-mode substitute-command-keys fi:ss-hide-help-text re-search-forward beginning-of-line] 15)) (lambda (error) (byte-code "��\"�" [error message "Cannot zoom on stack: %s"] 3))] 9))

(defun fi::make-pretty-process-name (process-name) (byte-code "	�	G��
W�< 
H���U�2 �U�+ �P��2 �!P��
�\\��� �-�" [s process-name i max res c nil t 0 "" 42 32 "-" char-to-string 1] 5))

(defun fi:scan-stack-mode (&optional from-buffer process-name) "\
Major mode for debugging a Common Lisp process.
The keymap for this mode is bound to fi:scan-stack-mode-map
\\{fi:scan-stack-mode-map}
Entry to this mode runs the fi:scan-stack-mode-hook hook." (byte-code "	� �
�
 �)���Ή�ω�?�� � � 	�	��#��	��#��	��#��	��#���	#����#����#����#����#����#����#����#����#����#����#����#����#����#����#��*��!�
?�� � �̉���!�" [saved-from-buffer fi::ss-debugger-from-buffer from-buffer fi::ss-process-name process-name major-mode mode-name fi:scan-stack-mode-map map ccmap buffer-read-only truncate-lines t kill-all-local-variables fi:scan-stack-mode "Scan stack mode" make-keymap define-key "" fi:ss-continue "" fi:ss-pop "" fi:ss-reset " " fi:lisp-delete-pop-up-window fi:ss-hide-help-text "." fi:ss-set-current "D" fi:ss-disassemble "R" fi:ss-restart "d" fi:ss-next-frame "a" fi:ss-toggle-all "e" fi:ss-edit "g" fi:ss-revert-stack "h" fi:ss-unhide-help-text "l" fi:ss-locals "p" fi:ss-pprint "q" fi:ss-quit "r" fi:ss-return "u" fi:ss-previous-frame use-local-map toggle-read-only run-hooks fi:scan-stack-mode-hook] 26))

(defun fi:ss-next-frame nil "\
Go to the next frame." (interactive) (byte-code "��� ���!���!���!�" [nil beginning-of-line forward-char 3 forward-sexp 1 forward-line] 5))

(defun fi:ss-previous-frame nil "\
Go to the previous frame." (interactive) (byte-code "��� ���!�� �" [nil beginning-of-line forward-sexp -1] 4))

(defun fi:ss-reset nil "\
Do a :reset on the process being debugged.  This causes the process
being debugged to throw out to the outer most read-eval-print loop, and
causes the debugger buffer to be buried and the window configuration as it
was before this mode was entered to be restored." (interactive) (byte-code "������#�" [t nil fi::do-tpl-command-on-process "reset"] 4))

(defun fi:ss-continue nil "\
Do a :continue on the process being debugged.  This causes the process
being debugged to continue from a continuable error, taking the default
restart (restart number 0)." (interactive) (byte-code "������#�" [t nil fi::do-tpl-command-on-process "continue"] 4))

(defun fi:ss-pop nil "\
Do a :pop on the process being debugged.  This causes the process being
debugged to pop out to the next outer most read-eval-print loop, and
causes the debugger buffer to be buried and the window configuration as it
was before this mode was entered to be restored." (interactive) (byte-code "������#�" [t nil fi::do-tpl-command-on-process "pop"] 4))

(defun fi:ss-return nil "\
Do a :return on the process being debugged.  This causes the process
being debugged to return a value from the current frame, as if the error
never occured.  The form to evaluate to obtain the return value for the
current frame is read from the minibuffer and evaluated in the Common Lisp
environment.  The debugger buffer is buried and the window configuration as
it was before this mode was entered is restored." (interactive) (byte-code "����������\"D$�" [t nil fi::do-tpl-command-on-process "return" read-from-string read-string "Form (evaluated in the Lisp environment): " "nil"] 8))

(defun fi:ss-restart (new-form) "\
Do a :restart on the process being debugged.  This causes the process
being debugged to restart the execution of the function associated with the
current frame.  With argument NEW-FORM, a form to evaluate to obtain the
function and arguments to be restarted is read from the minibuffer and
evaluated in the Common Lisp environment.  The default function and
arguments are the ones in the current frame.   The debugger buffer is
buried and the window configuration as it was before this mode was entered
is restored." (interactive "P") (byte-code "����	?�  ���!D$�" [t new-form nil fi::do-tpl-command-on-process "restart" read-from-string read-string "Form (evaluated in the Lisp environment): "] 7))

(defun fi:ss-edit nil "\
Find the source file associated with the function in the current frame
and pop up a buffer with that definition visible." (interactive) (byte-code "������#�" [nil t fi::do-tpl-command-on-process "edit"] 4))

(defun fi:ss-revert-stack nil "\
Cause the stack in the debugger buffer to be synchronized with the
actual stack in the Common Lisp environment.  This is useful when commands
are typed in the *common-lisp* buffer which change the state of the process
being debugged." (interactive) (byte-code "��� �" [nil fi:scan-stack] 2))

(defun fi:ss-toggle-all nil "\
Toggle showing all frames in the currently debugged process stack.  By
default, there are certain types of frames hidden because they offer no
additional information." (interactive) (byte-code "��?��� �" [fi::ss-show-all-frames nil fi:scan-stack] 2))

(defun fi:ss-set-current nil "\
Make the frame to which the point lies the current frame for future
operations.  It is not necessary to use this command, usually, since most
commands make the frame to which the point lies the current frame before
performing their assigned action." (interactive) (byte-code "��� �) �V� ������&�% ����[��&��!)�" [offset nil fi::offset-from-current-frame 0 fi::do-tpl-command-on-process "dn" :zoom "up" fi::make-stack-frame-current] 9))

(defun fi:ss-quit nil "\
Quit debugging the Common Lisp process.  The debugger buffer is buried
and the window configuration as it was before this mode was entered is
restored." (interactive) (byte-code "��� �� �db�" [nil bury-buffer fi:lisp-delete-pop-up-window] 3))

(defun fi:ss-disassemble nil "\
Disassemble the function associated with the current frame, putting the
disassembly into a help buffer and positioning the point on the instruction
that will next be executed if the current error can be continued." (interactive) (byte-code "È� � �����	F�	D�C�&*�" [process-name offset t nil fi::buffer-process fi::offset-from-current-frame lep::send-request-in-new-session lep::disassemble-session :process-name :offset (lambda (text pc offset) (byte-code "?�	 �� �!��
��$�" [offset nil text pc fi::make-stack-frame-current fi::show-some-text-1 fi::disassemble-hook] 6)) (lambda (error) (byte-code "��\"�" [error message "Cannot dissassemble: %s"] 3))] 9))

(defun fi::disassemble-hook (pc) (byte-code "?�	 �� ���\"��#?� �� � ��c�" [pc nil t re-search-forward format "^[ 	]*%s:" beginning-of-line ">"] 5))

(defun fi:ss-locals nil "\
Find the local variables to the function associated with the current
frame, and display them in a help buffer.  See the Allegro CL compiler
switch compiler:save-local-names-switch for information on accessing local
variables in the debugger." (interactive) (byte-code "È� � �����	F�	D�C�&*�" [process-name offset t nil fi::buffer-process fi::offset-from-current-frame lep::send-request-in-new-session lep::local-session :process-name :offset (lambda (text offset) (byte-code "?�	 �� �!��
�\"�" [offset nil text fi::make-stack-frame-current fi::show-some-text-1] 4)) (lambda (error) (byte-code "��\"�" [error message "Cannot find locals: %s"] 3))] 9))

(defun fi:ss-pprint nil "\
Pretty print the current frame, function and arguments, into a help
buffer." (interactive) (byte-code "È� � �����	F�	D�C�&*�" [process-name offset t nil fi::buffer-process fi::offset-from-current-frame lep::send-request-in-new-session lep::pprint-frame-session :process-name :offset (lambda (text offset) (byte-code "?�	 �� �!��
�\"�" [offset nil text fi::make-stack-frame-current fi::show-some-text-1] 4)) (lambda (error) (byte-code "��\"�" [error message "Cannot pprint: %s"] 3))] 9))

(defun fi:ss-hide-help-text nil "\
Hide the help text at the beginning of the debugger buffer." (interactive) (byte-code "���� �� �����#� db�� ��`d\"���)�" [nil t fi:scan-stack-mode-display-help widen beginning-of-buffer re-search-forward "^Evaluation stack:$" beginning-of-line narrow-to-region] 7))

(defun fi:ss-unhide-help-text nil "\
Unhide the help text at the beginning of the debugger buffer." (interactive) (byte-code "�� )�� ����" [fi:scan-stack-mode-display-help t nil widen recenter] 4))

(defun fi::do-tpl-command-on-process (done set-current-frame command &rest args) (byte-code "� 
?� Â � ���������	&
�	D�D�&*�" [process-name offset set-current-frame nil t command args done fi::buffer-process fi::offset-from-current-frame lep::send-request-in-new-session lep::tpl-command-session list :process-name :command :args :done :offset (lambda (done offset) (byte-code "�	 � � 	?�  �	!�" [done offset nil fi:ss-quit fi::make-stack-frame-current] 3)) (lambda (error process-name) (byte-code "��\"�� ���!����	\"!� � �" [error process-name message "Lisp error: %s" beep sit-for 2 y-or-n-p format "Revert stack from process \"%s\"? " fi:scan-stack] 7))] 16))

(defun fi::offset-from-current-frame nil (byte-code "� ��!� ��q �
!?� ��!�����#�% ��4 ���#�1 Ă4 ��!)��� ��!?�F �
!�d �\\���Y ��!�` ��!���!��9 )��n �p [*�" [fi::ss-current-frame-regexp nil fi::ss-ok-frame-regexp down t lines beginning-of-line looking-at error "Not on a frame." re-search-forward re-search-backward "Can't find current frame indicator." 0 1 backward-sexp forward-sexp forward-line] 15))

(defun fi::make-stack-frame-current (offset) (byte-code "� ���!��c�� ��[���!�' ��!��!���!�- �!�� �̎�	!?�: ��!���!))�" [offset fi::ss-current-frame-regexp toggle-read-only delete-char 3 " ->" beginning-of-line plusp forward-char forward-sexp forward-line 1 ((byte-code "� �" [toggle-read-only] 2)) looking-at error "Not on current frame." replace-match "   "] 13))

(defun fi::buffer-process nil (byte-code "� ��!�" [fi::ss-process-name t fi::read-lisp-process-name "Process to debug: "] 2))

(defun fi::read-lisp-process-name (prompt) (byte-code "��!@A��\"��!� �� !��
	���%*�" [processes completions prompt nil t lep::eval-session-in-lisp lep::list-all-processes-session mapcar (lambda (x) (byte-code "C�" [x] 1)) fboundp epoch::mapraised-screen minibuf-screen completing-read "Initial Lisp Listener"] 11))

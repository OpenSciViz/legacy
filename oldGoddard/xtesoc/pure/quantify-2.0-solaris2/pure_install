#!/bin/sh
#
# pure_install
# 
#	Installation and license setup for Pure Software's products.
#
# Copyright (c) 1993 Pure Software.
#
# This file contains proprietary and confidential information and
# remains the unpublished property of Pure Software. Use, disclosure,
# or reproduction is prohibited except as permitted by express 
# written license agreement with Pure Software.
#

#set -x 

PATH=.:/bin:/usr/bin:/usr/ucb:$PATH
export PATH
umask 0
unset LD_LIBRARY_PATH
unset LD_RUN_PATH
unset LD_OPTIONS

my_name=pure_install

initial_exit=yes
script_finished_ok=no

purela_licenses_added=no
serial_actually_added="not-a-serial"
tests_passed_file=.tests.passed

la_db_true_name=""
la_db_local_name=""
la_db_was_link=no
la_db_existed_on_entry=no

STARS="****************************************************************************"
LINES="----------------------------------------------------------------------------"

#####
#	Calculate program suffix, based on OS rev.
#####
case "`uname -sr`" in
	SunOS\ 4.*)		os_suffix=sunos4;;
	SunOSCMW*)		os_suffix=sunos4;;	# Docs for this are lacking
	SunOS\ 5.*)		os_suffix=sunos5;;
	HI-UX*)			os_suffix=hpux;;
	HP-UX*)			os_suffix=hpux;;
	*)				echo "Unsupported host operating system: abort."; exit 1;;
esac


#####
#	Misc utility functions
#####

install_failed_call_support() {

	echo "   ****************************************************"
	echo "   *                                                  *"
	echo "   *   There appears to be an installation problem.   *"
	echo "   *   Please call or email technical support.        *"
	echo "   *                                                  *"
	echo "   ****************************************************"
	
	echo
	echo "$my_name FAILED"
}

cleanup_and_exit()
{
	if test $initial_exit = yes
	then
		initial_exit=no
		echo ""
		if test $script_finished_ok = yes
		then
			if test "$cur_err_complete_msg" != "$init_err_complete_msg"
			then
				echo "$cur_err_complete_msg"
			else
				echo "$my_name completed OK"
			fi
			exit 0
		else
			install_failed_call_support
			exit 1
		fi
	fi
}

product_banner()
{
	echo "$LINES"
	echo "| Installing $product"
	echo "$LINES"
}

#####
#	While files remain to unpack, unpack them:
#		*.Z		uncompress
#		*.gz	unzip
#####
unpack_all_files()
{
	got_some=1
	while test $got_some = 1
	do
		got_some=0

		comprs=`find . -print | sed -e '/[^\/]\.Z$/!d'`
		if test ! -z "$comprs"
		then
			for comp in $comprs
			do
				attempt "uncompress $comp"
			done
			got_some=1
		fi

		gzips=`find . -print | sed -e '/[^\/]\.gz$/!d'`
		if test ! -z "$gzips"
		then
			for zip in $gzips
			do
				attempt "gunzip $zip"
			done
			got_some=1
		fi
	done
}

checksum_files()
{
	attempt "chk_manifest Manifest"
}

maybe_exec()
{
	maybe_prog="$1"
	status=0
	if test -x "$maybe_prog"
	then
		shift
		echo "$LINES"
		attempt_soft_fail "$maybe_prog" "$@"
		status=$?
	fi

	return $status
}

pre_install()
{
	maybe_exec $product.pre_install "$@"
}

post_install()
{
	maybe_exec $product.post_install "$@"
}

test_install()
{
	# Run the test installation script if it exists ...
	maybe_exec $product.test_install "$@"

	if [ $? != 0 ]
	then
		rm -f $tests_passed_file
		script_finished_ok=no
		cleanup_and_exit 1
	else
		# Let support know that the installation passed 
		touch $tests_passed_file
		chmod 777 $tests_passed_file
	fi
}


################################################################################
#
#	get_installation_type() - This routine is used to get information
#		concerning the type of installation.  The following variables
#		are set:
#
#			install_type	- "purchased" or "eval" installation?
#
################################################################################

get_installation_type()
{
	# Give the user some information ...
	echo
	echo "   $product can be used with either a purchased license or"
	echo "   a temporary evaluation license."
	echo

	# Ask the user if they are interested in doing an EVAL license ...
	yes_or_no "Do you want to enter an evaluation password" yes

	if test $response = y
	then
		install_type=eval
	else
		install_type=purchased
	fi
}

arg_error()
{
	# Technically, we permit -verify for in-house use.
	echo $my_name: This program takes no parameters.
	cleanup_and_exit 1
}

maybe_do_just_verify_then_exit()
{
	if test $# = 1
	then
		if test $1 != "-verify"
		then
			arg_error
		else
			unpack_all_files
			checksum_files
			echo "$LINES"
			script_finished_ok=yes
			cleanup_and_exit 0
		fi
	else
		if test $# != 0
		then
			arg_error
		fi
	fi
}

initial_installation()
{
	echo
	echo "$LINES"
	echo "Verifying that all files have correct permissions and contents..."

	unpack_all_files
	checksum_files

	pre_install $product $version "$prod_date" $install_type

	echo "$LINES"
}

final_installation()
{
	post_install $product $version "$prod_date" $install_type
	test_install $product $version "$prod_date" $install_type
}

hard_or_soft_links()
{
	got_dir=0
	while test $got_dir != 1
	do
		got_dir=1

		echo ""
		question="Which directory was $product previously installed in"
		prompt_and_read "$question"

		echo

		if test ! -d "$response"
		then
			got_dir=0
			echo "$response does not exist or is not a directory."
		fi
		if test ! -f "$response/$product.purela"
		then
			got_dir=0
			echo "$response is not a $product installation."
		fi
		if test ! -r "$response/$product.purela"
		then
			got_dir=0
			echo "$response/$product.purela cannot be read."
			echo "Fix that problem before continuing, or redo this installation"
			echo "indicating you do not have licenses currently installed."
		fi
	done

	old_license_file="$response/$product.purela"
	new_license_file=./$product.purela

	ln "$old_license_file" "$new_license_file" >/dev/null 2>&1
	status=$?
	if test $status != 0
	then
		echo "$STARS"
		echo "* WARNING: could not make a hard link between the license files."
		echo "$STARS"
		attempt "ln -s $old_license_file $new_license_file >/dev/null 2>&1"
		echo "* A symbolic link was made.  This will work OK but if"
		echo "* the old directory is deleted, then the license file will"
		echo "* disappear and the licenses will need to be reinstalled."
		echo "$STARS"
	fi
}

#####
#	Figure out what $1 is soft-linked to, whether it actually exists or not.
#	If it in turn is a soft link, continue tracing until the actual file is
#	reached.  Return the information to the caller.
#####
linked_to()
{
	link="$1"

	while test -h "$link"
	do
		link=`ls -l "$link" | sed 's/.*-> //'`
	done

	is_linked_as="$link"
}


################################################################################
#
#	license_file_setup() - This routine is used to setup some license
#		database parameters.  The following variables are set:
#
#		la_db_local_name	= $product.purela
#		la_db_true_name		= The real name of the database file
#					  minus any symbolic links.
#		la_db_was_link		= Was the database file a symlink
#		la_db_existed_on_entry	= Did we craete the database file?
#
################################################################################

license_file_setup()
{
	# Set up some default parameters ...
	la_db_local_name=$product.purela
	la_db_true_name=$la_db_local_name
	la_db_was_link=no
	la_db_existed_on_entry=no
	la_db_ignore_new_license=no

	# If the local database is hanging off a symlink, find where it points
	if test -h $la_db_local_name
	then
		linked_to $la_db_local_name
		la_db_was_link=yes
		la_db_true_name=$is_linked_as
	fi

	# Keep track of whether or not the database was already there ...
	[ -f $la_db_true_name ] && la_db_existed_on_entry=yes

	# We're finished if we're dealing with an 'eval' installation ...
	[ "$install_type" = "eval" ] && return

	# If the database exists, we are finished ...
	[ "$la_db_existed_on_entry" = "yes" ] && return

	# If the database doesn't exist, ask the user for an existing one ...
	echo
	echo "$LINES"
	echo
	echo "   We suggest you share licenses between multiple versions of $product"
	echo "   such as between $product-1.1-HP-UX and $product-2.0-SunOS."
	echo

	echo "   Do you have any other version of $product online from which"
	yes_or_no "the license file may be linked and shared" yes

	# Try to link in an existing database if the user has requested it
	if [ "$response" = "y" ]
	then 
		hard_or_soft_links
		la_db_ignore_new_license=yes
	fi

}


#####
#	"licenses" is able to install any kind of license, eval or regular.
#	This permits someone e.g. to re-enter an
#	eval license after the license file was trashed, without having to
#	redo the entire installation.
#####
get_SN_quantity_duration()
{
	case "$1" in
		eval)
			quantity="$eval_quantity"
			serial="$eval_serial"
			print_padded_response "Quantity" "$quantity"
			print_padded_response "License Serial No." "$serial"
			get_date
			;;
		*)
			get_quantity
			get_serial
			get_date "NEVER"
			;;
	esac
	prompt_and_read_padded "Password"
	password=$response
}


#####
#	Assuming all parameters are defined, try to do the actual installation.
#####
really_install_license()
{
	output=`purela add						\
				-product="$product"			\
				-expiration="$date"			\
				-quantity="$quantity"		\
				-licensee="$cname"			\
				-serial="$serial"			\
				-version="$version"			\
				-password="$password"`
	status="$?"
	if test $status != "0"
	then
		echo "The purela command failed."
		echo "$output"
	else
		purela_licenses_added=yes
		serial_actually_added=$serial
	fi
}

#####
#	Ask for parameters then do the actual installation.
#####
install_licenses()
{
	status=1
	while test $status != 0
	do
		echo
		echo "$LINES"
		echo
		echo "Enter information EXACTLY as it appears on your license certificate."
		get_company_name $install_type
		get_SN_quantity_duration $install_type

		echo ""
		echo "Installing licenses ..."
		really_install_license
	done
}

do_admins()
{
	user_name=`who am i | awk '{print $1}' | sed -e 's/.*!//'`
	case $install_type in
		purchased)
			echo
			echo "$LINES"
			echo
			echo "Enter the email address of the administrator for $product."
			prompt_and_read_with_default "Email address" "$user_name"
			attempt "purela options -product=$product -admin-list='""$response""'"
			;;
		eval)
			attempt "purela options -product=$product -admin-list=$user_name"
			;;
		*)
			;;
	esac
}

check_needs_licenses()
{
	echo ""
	yes_or_no "Did you get a new license certificate" "no"
	if test $response = y
	then
		need_licensing=yes
	else
		need_licensing=no
	fi
}

do_licensing()
{

	[ "$la_db_ignore_new_license" = "yes" ] && return

	# Install the new licenses
	install_licenses

	if [ "$install_type" = "purchased" ] 
	then
		do_admins
	fi

	echo ""
	echo "To see your licenses, run this command from the installation directory:"
	echo "   ./purela print"
}

offer_to_install_and_maybe_install()
{
	echo
	echo "$LINES"
	echo
	repeat_offer=yes
	while test $repeat_offer = yes
	do
		repeat_offer=no
		echo "$product man pages are currently in `pwd`/man"
		echo
		echo "   We recommend adding this to your MANPATH instead of installing them"
		echo "   in another location in the filesystem."
		echo
		yes_or_no "Install man pages" "no"
		if test $response = y
		then
			prompt_and_read "Destination for man pages"
			man_dir="$response"
			if test ! -d "$man_dir"
			then
				echo "Your answer $man_dir is not a directory."
				repeat_offer=yes
			else
				if test ! -w "$man_dir"
				then
					echo "You do not have permission to modify $man_dir"
					repeat_offer=yes
				else
					(cd man; tar cf - *) | (cd "$man_dir"; tar xpf -) >/tmp/.manp$$ 2>&1
					if test $? != 0 -o -s /tmp/.manp$$
					then
						echo "Unable to copy the man pages to $man_dir.  The attempt"
						echo "to copy produced these error messages in $man_dir:"
						cat /tmp/.manp$$ | sed -e 's/^/   /'
						repeat_offer=yes
					fi
					rm /tmp/.manp$$
				fi
			fi
		fi
		if test $repeat_offer = yes
		then
			echo ""
			echo "The man pages may either be left in the installation directory, in"
			echo "which case users may wish to update their MANPATH variable, or they"
			echo "may be copied into another location such as the system man page"
			echo "directory.  You may need special privileges to write into such a"
			echo "directory, or you may need to delete old versions of the files"
			echo "before this program will be permitted to write the new versions."
			echo ""
		fi
	done

	echo
}

man_pages()
{
	case $install_type in
		purchased)
			offer_to_install_and_maybe_install
			;;
	esac
}

#####
# 	Set up stuff
#####

if test ! -r pure_install -o ! -r .pure_install
then
	echo "The directory `pwd` does not"
	echo "appear to be a Pure Software product installation directory."
	echo "You must cd into the product's installation directory before"
	echo "giving the command"
	echo "   ./$my_name"
	exit 1
fi

# lic_shared.proto
#
#	Shared license utility functions.
#
# Copyright (c) 1993-1995 Pure Software.
#
# This file contains proprietary and confidential information and
# remains the unpublished property of Pure Software. Use, disclosure,
# or reproduction is prohibited except as permitted by express
# written license agreement with Pure Software.
#


#####
#	You might want to customize these constants.
#####
MAX_QUANTITY=10000
eval_serial=EVAL
eval_quantity=10000
norm_quantity=""
beta_quantity=10
univ_quantity=10000

#	For building a list of errors which happened without killing this program
init_err_complete_msg="WARNING: $my_name completed with errors:"
cur_err_complete_msg="$init_err_complete_msg"

verify_OS()
{
	case "`uname -sr`" in
		SunOS\ 4.*)		os_suffix=sunos4;;
		SunOSCMW*)		os_suffix=sunos4;;	# Docs for this are lacking
		SunOS\ 5.*)		os_suffix=sunos5;;
		HI-UX*)			os_suffix=hpux;;
		HP-UX*)			os_suffix=hpux;;
		*)				echo "Unsupported host operating system: abort.";
						cleanup_and_exit 1;;
	esac

	if test sunos5 != $os_suffix
	then
		echo "This operation can be performed only on sunos5; you"
		echo "currently are attempting it on $os_suffix."
		cleanup_and_exit 1
	fi
}


welcome()
{
	echo ""
	my_vers=`echo "$1" | sed -e 's/$[^:]*: //g' -e 's/ //' -e 's/ $)/)/'`
	echo "Welcome to $my_name!  $my_vers"
	echo ""
}


#####
#	Replacement for dirname for systems which don't have it.
#
#	Perhaps it should be smart about checking $PATH and using the
#	true dirname if it exists?
#####
my_dirname()
{
	dn_path="$1"

	#####
	#	This pattern means "everything through the last / in the dn_path".
	#	However, that strips too much for dn_paths with just one segment.
	#	So, if it's only one segment, add the appropriate leading / or ./ to
	#	cope with short dn_paths.
	#	This code still can't handle patterns which have multiple consecutive
	#	/ embedded, or trailing / marks, which are technically valid:
	#		bill/
	#		/ralph/
	#		/aa//bb///ccc////dddd/////
	#	But, ls and pwd will never produce them for us, so we're probably ok.
	#####
	regexp='\(.*\)/'

	case "$dn_path" in
		/*/*)
				;;
		/*)
				dn_path=/"$dn_path"
				;;
		*/*)
				;;
		*)
				dn_path=./"$dn_path"
				;;
	esac

	result_path=`expr match "$dn_path" "$regexp"`
}


calculate_name_and_home()
{
	#####
	#	Calculate the "true name" of the program to run.
	#	$0 will contain my pathname.
	#####
	my_name=`basename $0`
	if test $os_suffix = sunos4
	then
		my_dirname $0
		my_home=$result_path
	else
		my_home=`dirname $0`
	fi
	prev_name="$my_name"

	if test ! -x $my_home/$my_name
	then
		# Hmmm, $0 parsed funny.
		echo Cannot determine the location of $my_name, cannot determine why.
		echo Abort.
		cleanup_and_exit 1
	fi

	#####
	#	Rely on the fact that the "product" is really a link to this wrapper,
	#	so the next-to-last name is the actual product name no matter what
	#	they linked it as.  Trust people who abort the search with a
	#	.thisispurehome to know what they are doing.
	#####
	#	While our name is a soft link to something else....
	while [ -h $my_home/$my_name -a ! -f $my_home/.thisispurehome ]
	do
		# Extract what it points to; this is probably the thing to speed up
		points_to=`ls -l $my_home/$my_name | sed 's/.*->//'`

		# The filename is now whatever the pointed-to filename was
		prev_name=$my_name
		my_name=`basename $points_to`

		# Try to resolve what the directory actually is
		if test $os_suffix = sunos4
		then
			my_dirname $points_to
			new_my_home=$result_path
		else
			new_my_home=`dirname $points_to`
		fi
		case $new_my_home in
			/*) # for absolute pathnames, cd absolutely
				my_home=`cd $new_my_home; pwd`
				;;
			*)  # for relative pathnames, cd relative to last known location
				my_home=`cd $my_home/$new_my_home; pwd`
				;;
		esac
	done
}

cd_to_proper_location()
{
	calculate_name_and_home
	cd $my_home
}


#####
#	tab-->blank, multiple blanks-->single blank, leading/trailing blanks wiped
#####
min_white()
{
	minimized=`echo "$1" | tr -s '\011' ' ' | sed -e 's/^ *//' -e 's/ *$//'`
}

prt_prompt()
{
	awk 'END {printf "   %s ", "'"$1"'"}' </dev/null
}

#####
#	Get an input.  Lots of control over formatting of questions.
#
#	Is there a default value?					yes/no
#	What character indicates end of question?	"?" vs ":"
#	Do we pad questions to minimum size?		yes/no
#
#	Examples:
#
#	    Enter name                       : <answer>
#		Enter expiration [2-july-1993]   : <answer>
#		Do you have licenses? <answer>
#		Install man pages? [no] <answer>
#####
prompt_and_read_raw()
{
	prompt="$1"
	question="$2"
	padding="$3"
	default="$4"

	response=""

	# Presentation form of default has [] around it
	if test ! -z "$default"
	then
		view_def=" [$default]"
	else
		view_def=""
	fi

	if test -z "$padding"
	then
		# No padding, default to right of question mark
		true_prompt="$prompt$question$view_def"
	else
		# Padded, so default to left of question mark
		format="%${padding}s$question"
		true_prompt=`
			awk 'END {printf "'"$format"'", "'"$prompt$view_def"'"}' </dev/null`
	fi

	#	Insist we get a response
	while test -z "$response"
	do
		prt_prompt "$true_prompt"
		if read new_response
		then
			min_white "$new_response"
			new_response="$minimized"
		else
			echo "Exiting"
			cleanup_and_exit 1
		fi

		if test -z "$new_response"
		then
			response="$default"
		else
			response="$new_response"
		fi
	done
}

################################################################################
#
#	print_padded_response_raw() - This routine is used to simulate
#		the output seen by prompt_and_read_raw().
#
################################################################################

print_padded_response_raw() {

	# Retrieve paramters
	prompt="$1"
	question="$2"
	padding="$3"
	response="$4"

	if test -z "$padding"
	then
		# No padding, default to right of question mark
		echo "   $prompt$question $response"
	else
		# Padded, so default to left of question mark
		format="   %${padding}s$question $response\n"
		awk 'END {printf "'"$format"'", "'"$prompt"'"}' </dev/null
	fi
}

#####
#	Basic Q&A functions
#####
prompt_and_read()
{
	prompt="$1"

	prompt_and_read_raw "$prompt" "?" "" ""
}

prompt_and_read_with_default()
{
	prompt="$1"
	default="$2"

	prompt_and_read_raw "$prompt" "?" "" "$default"
}

#####
#	Padded versions (keep padding the same, share it with print_values)
#####
pretty_pad="-39"
prompt_and_read_padded()
{
	prompt="$1"

	prompt_and_read_raw "$prompt" ":" "$pretty_pad" ""
}

print_padded_response()
{
	print_padded_response_raw "$1" ":" "$pretty_pad" "$2"
}

prompt_and_read_with_default_padded()
{
	prompt="$1"
	default="$2"

	prompt_and_read_raw "$prompt" ":" "$pretty_pad" "$default"
}
# try to get a yes/no answer from someone
yes_or_no()
{
	query="$1"
	default="$2"
	padding="$3"
	response=""
	until test ! -z "$response"
	do
		if test -z "$padding"
		then
			prompt_and_read_with_default "$query" "$default"
		else
			prompt_and_read_with_default_padded "$query" "$default"
		fi
		case "$response" in
			y|Y|yes|Yes|YES)	response=y;;
			n|N|no|No|NO)		response=n;;
			*)					response="";
								echo "Answer either 'yes' or 'no'.";;
		esac
	done
}

get_product_by_asking()
{
	prodlist="Purify Quantify PureLink PureCoverage"
	product=""
	until test ! -z "$product"; do
		prompt_and_read_padded "Product name"
		product="$response"
		case $product in
			purify|Purify)
				product=Purify
				version="2.1"
				break
				;;
			purelink|Purelink|PureLink)
				product=PureLink
				version="1.1"
				break
				;;
			quantify|Quantify)
				product=Quantify
				version="1.0"
				break
				;;
			purecov|purecoverage|PureCoverage)
				product=PureCoverage
				version="1.0"
				break
				;;
			test*)
				version="1.0"
				break
				;;
			*)
				echo "You entered '$response'"
				echo "Please enter one of:  $prodlist"
				product="" ;;
		esac
	done
}

get_special_attributes()
{
	attributes=""
	yes_or_no "Set special license attributes" no pad
	if test $response = y
	then
		attributes="-"

		#echo "   Which special attributes apply to this license?"
		yes_or_no "   University (-u)" no pad
		if test $response = y
		then
			attributes="${attributes}u"
			norm_duration="$univ_duration"
			norm_quantity="$univ_quantity"
		fi

		yes_or_no "   Read-only filesystem (-r)" no pad
		if test $response = y
		then
			attributes="${attributes}r"
		fi

		yes_or_no "   Beta (-b)" no pad
		if test $response = y
		then
			attributes="${attributes}b"
			norm_duration="$beta_duration"
			norm_quantity="$beta_quantity"
		fi

		yes_or_no "   NoRegister (-n)" no pad	# Same as beta (at present),
		if test $response = y					# but different name in case
		then
			attributes="${attributes}n"
			norm_duration="$beta_duration"
			norm_quantity="$beta_quantity"
		fi

		if test "$attributes" = "-"
		then
			attributes=""
		fi
	fi
}


get_product_and_version_by_asking()
{
	get_product_by_asking
	prompt_and_read_with_default_padded "License Compatibility Version" \
		$version
	version="$response"
}

extract_from_file()
{
	file=$1
	name=$2

	extracted=`grep "$name" $file | sed -e 's/[^:]*: //'`
	if test -z "$extracted"
	then
		echo "$my_name: File $file is not in proper format.  (no $name info.)"
		cleanup_and_exit 1
	fi
}

get_product_and_version_from_inst_dir()
{
	if test ! -r .pure_install
	then
		echo "Cannot find '.pure_install' file, installation cannot continue."
		cleanup_and_exit 1
	fi
	extract_from_file .pure_install name
	product="$extracted"
	extract_from_file .pure_install version
	version="$extracted"
	extract_from_file .pure_install date
	prod_date="$extracted"
}

#####
#	Attempt an operation which automatically terminates the script on failure
#####
attempt()
{
	eval "$@"
	status=$?
	if test $status != 0
	then
		echo "$my_name: FATAL: '$@' failed."
		cleanup_and_exit $status
	fi
}

#####
#	Attempt an operation which prompts the user whether it should abort
#	the script.
#####
attempt_soft_fail()
{
	the_command="$@"
	eval "$@"
	status=$?
	if test $status != 0
	then
		echo "$my_name: ERROR: '$@' failed."

		# Don't put a \ on the next line, and don't join them; this
		# is correct:
		cur_err_complete_msg="$cur_err_complete_msg
            '$the_command' failed"
	fi

	return $status
}

get_product_and_version()
{
	if test "$1" = "Issue"
	then
		get_product_and_version_by_asking
	else
		get_product_and_version_from_inst_dir
	fi
}

get_date()
{
	prompt_and_read_with_default_padded "Expiration" $1
	date="$response"
}

get_PO_number()
{
	prompt_and_read_padded "P.O. number"
	purch_order="$response"
}

get_misc_info()
{
	prompt_and_read_with_default_padded "Extra info" "none"
	misc_info="$response"
}

get_contact()
{
	prompt_and_read_padded "Contact info"
	contact="$response"
}

get_serial()
{
	def_serial="$1"
	prompt_and_read_with_default_padded "License Serial No." "$def_serial"
	serial="$response"
}

get_number()
{
	def_quant="$1"
	prompt_str="$2"

	the_number=""
	while test -z "$the_number"
	do
		prompt_and_read_with_default_padded "$prompt_str" "$def_quant"
		the_number=`echo $response | tr -cd '[0-9]'`
		if test -z "$the_number"
		then
			echo "Your selection ($response) is not a valid number."
		else
			if test "$the_number" -le 0
			then
				echo "The number ($the_number) must be greater than 0."
				the_number=""
			fi
		fi
	done
}

get_quantity()
{
	get_number "$1" "Quantity"

	while [ $the_number -gt $MAX_QUANTITY ]
	do
		echo "You've entered '$the_number' licenses which exceeds" \
			"the maximum of ($MAX_QUANTITY)."

		get_number "$1" "Quantity"
	done

	quantity=$the_number
}

get_company_name()
{
	# If we are an EVAL user, infer the name
	if test "$1" = eval
	then
		response="$product Evaluation User"
		print_padded_response "Company Name" "$response"
	else
		prompt_and_read_padded "Company Name"
	fi
	cname=`echo "$response" | awk '{printf "%s\n", substr($0,0,29)}'`
	min_white "$cname"
	cname="$minimized"
	if test "$cname" != "$response"
	then
		echo "WARNING: '$response' was longer than"
		echo "the limit of 29 characters, so it has been shortened to"
		echo "'$cname'."
	fi
}

print_values()
{
	echo        	 "   Company Name                           : $cname"
	echo        	 "   Quantity                               : $quantity"
	echo			 "   License Serial No.                     : $serial"
	if test "$1" != "EvalBlock"
	then
		echo       	 "   Expiration Date                        : $date"
		if test "$1" = "Issue"
		then
			echo	 "   Product                                : $product"
			echo     "   License Compatibility Version          : $version"
			echo     "   Attributes                             : $attributes"
			echo   	 "   PO number                              : $purch_order"
			echo   	 "   Contact                                : $contact"
			echo	 "   Extra info                             : $misc_info"
			if test ! -z "$password"
			then
				echo "   Password                               :" \
														 "$password$attributes"
				echo "   Issued at                              : `date`"
				echo "   Purela vers                            : $version_info"
				echo "   Issued by                              :" \
												"`who am i | awk '{print $1}'`"
				echo "   Certif code                            :" \
					"|$cname|$product|$quantity|$serial|$date|$password$attributes"
			fi
		else
			echo	 "   Password                               : $password"
		fi
	fi
}

confirm_values()
{
	lic_type=$1
	echo        ""
	echo        "============================"
	if test $lic_type = Install
	then
		echo    	"Confirm values from license certificate."
	else
		echo    	"$lic_type license for $product:"
	fi
	print_values "$lic_type"
	echo        "============================"
	yes_or_no "OK to continue" "yes"
	if test "$response" != "y"
	then
		if test $lic_type = Issue
		then
			echo "$0 aborted at your request."
			cleanup_and_exit 1
		else
			status=1
		fi
	else
		status=0
	fi
}



#####
#	No need to handle exit conditions until successfully initialized
#####
trap "cleanup_and_exit 1" 0 1 2 3

#####
#	Let's do it!
#####
verify_OS

get_product_and_version "Install"
product_banner

maybe_do_just_verify_then_exit $*

initial_installation

get_installation_type

license_file_setup
do_licensing 
man_pages

final_installation

script_finished_ok=yes
cleanup_and_exit 0




#####
# This file is edited with vi tabs set to 4 chars wide (:set ts=4).  Keep this
# at the bottom of the file and EMACS will treat it the same way.
# Local Variables: 
#   tab-width:4 
# End: 
#####

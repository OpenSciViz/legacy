#!/bin/sh
#
# pure_test
# Tests if the Quantify installation works
# It can be run many times safely
#
# Copyright (c) 1992-1995 Pure Software.
#
# This file contains proprietary and confidential information and
# remains the unpublished property of Pure Software. Use, disclosure,
# or reproduction is prohibited except as permitted by express written
# license agreement with Pure Software.
#

argv0=$0
# Customize this script for the proper product
product="quantify"
Product="Quantify"
productbuildoptions="-windows=no -record-system-calls=no"
diskspace_required="5000"
diskspace_pretty="5 megabytes"

# ---- Initialize the verbose option ----
if test "$1" = "-v"; then
	set -x
fi

# ---- Verify operating system compatibility ----
# This needs to be done first to ensure that the rest of this script
# is kosher.
UNAME=`uname -sr`

case "$UNAME" in
  SunOS\ 4.1*)
       	os=sunos4 ;;
  SunOS\ 5.0*|SunOS\ 5.1*)
	echo "** $UNAME is an outdated version of the operating system." ;
	echo "** $Product has not been fully tested on this version of the OS." ;
	echo "** We recommend you upgrade to a more recent OS version." ;
	echo "** Continuing with test anyway..." ;
        os=solaris2 ;;
  SunOS\ 5.2*)
	echo "** $Product has not been fully tested on this version of the OS." ;
	echo "** We recommend you upgrade to a more recent OS version." ;
	echo "** Continuing with test anyway..." ;
        os=solaris2 ;;
  SunOS\ 5.*)
        os=solaris2 ;;
  HP-UX*)
	os=hpux ;;
  HI-UX*)
	os=hpux ;;
  *)
	echo "Sorry, this version of $Product does not work on this operating system" ;
	exit 1 ;;
esac

echo_no_nl () {
case "$os" in
  sunos4)
	echo -n "$*" ;;
  solaris2)
	echo "$*\c" ;;
  hpux)
	echo "$*\c" ;;
esac
}

# ---- Set PATH ----
# Record user path, then reset path to system defaults.  This
# way we can invoke things like test, echo, uname without worrying
# about where they are found.  This is for portability.
userpath=$PATH
case "$os" in
  sunos4)
	PATH=/bin:/usr/bin ;;
  solaris2)
	PATH=/bin:/usr/bin ;;
  hpux)
	PATH=/bin:/usr/bin ;;
esac

# ---- Get directory name of where this script lives ----
pure_home=`expr $argv0 : '\(.*/\).*'`
pure_home=`PATH=.:$userpath ${pure_home}${product} -printhomedir`;
if test ! -n "$pure_home" -o ! -d "$pure_home"; then
    echo "$0: Error: Could not find $product in the current PATH"
    echo "Set the environment variable PATH to include $product"
    echo "For example if $product has been installed in /usr/local/$product: "
    echo "   If you use sh:"
    echo "       tutorial% PATH=/usr/local/$product:\$PATH; export PATH"
    echo "   If you use csh:"
    echo "       tutorial% set path=( /usr/local/$product \$path )"
    echo
    exit 1;
fi

# ---- Check if the directory is writable ----
if test ! -w "$pure_home";  then
	echo "Error: The directory $pure_home is not writable by you,"
	echo "please chmod or chown it"
	exit 1;
fi

# ---- Check if the $product program exists ----
if test ! -x $pure_home/$product; then
	echo "Error: The $product program is missing"
	exit 1;
fi

# ---- Check if the ld program exists ----
if test ! -x $pure_home/nld/ld; then
	echo "Error: The nld/ld program is missing"
	exit 1;
fi

# ---- Check if the test subdirectory exists ----
if test ! -d $pure_home/test; then
	echo "Error: The test sub-directory is missing"
	exit 1;
fi

# ---- The Banner ----
echo
echo Testing $product in $pure_home
echo

umask 000

# ---- Make sure a compiler is available ----
# Look for cc in a number of standard places.
# If it isn't found, look on the user's search path.
# If nothing is found, look for gcc on the user's search path.
# NOTE: You can specify the compiler here to prevent the
#       run-time search from taking place.
echo_no_nl "Looking for compiler.."
cc=
case "$os" in
  sunos4)
	supported_compilers="cc gcc gcc2" ;
	compiler_path="${userpath}" ;
	std_compilers="/bin/cc" ;;
  solaris2)
	supported_compilers="cc gcc gcc2" ;
	compiler_path="`echo :${userpath}: | sed -e 's#:/usr/ucb:#:#g' -e 's/:/ /g'`" ;
	std_compilers="/opt/SUNWspro/bin/cc \
			/opt/gnu/bin/gcc /usr/local/bin/gcc \
			/usr/cygnus/bin/gcc /usr/cygnus/progressive/bin/gcc" ;;
  hpux)
	supported_compilers="cc gcc gcc2" ;
	compiler_path="${userpath}" ;
	std_compilers="/bin/cc" ;;
esac

for compiler in $std_compilers; do
    if [ -x $compiler ]; then
        cc=$compiler
        break
    fi
done
for compiler in $supported_compilers; do
    if [ "$cc" ]; then
        break
    fi
    for d in $compiler_path; do
        if [ -x $d/$compiler ]; then
            cc=$d/$compiler
            break
        fi
    done
done

if [ "$cc" ]; then
    echo "using ${cc}.."
 else
    echo "$0: Error: No supported compiler found in the current PATH"
    echo "Set the environment variable PATH to include one of the"
    echo "supported compilers (${supported_compilers}) and try again."
    echo
    exit 1
fi

case "$os" in
  sunos4 | solaris2)
	case "$cc" in
	*/cc)
		statically="-Bstatic" ;
		dynamically="-Bdynamic" ;;
	*/gcc*)
		statically="-static" ;
		dynamically="" ;;
	esac ;;
  hpux)
	case "$cc" in
	*/cc)
		statically="-Wl,-a,archive" ;
		dynamically="" ;;
	*/gcc*)
		statically="" ;
# gcc always links statically on hp
		dynamically="" ;;
	esac ;;
esac

# ----- Check the disk space ----
df_output=`df -k .`
diskspace=`echo $df_output | awk ' { print $11 } '`

if test "$diskspace" -le $diskspace_required; then
	echo Disk-space available: $diskspace
	echo "Error: Not enough disk space available."
	echo "       $Product will grow to about $diskspace_pretty."
	echo "Enter y to continue anyway, or just return to quit now."
	read foo
	if [ "x$foo" != "xy" ] ; then
		exit 1
	fi
fi

# ---- Define testing routines ----

run_basic_test () {
type=$1
ldcmd=$2

echo "Testing $product with $type linking.."

$cc -o test.plain.$type $ldcmd hello_world.c $pure_home/${product}_stubs.a
if [ ! -x test.plain.$type ]; then
    echo "Error: Could not compile test program $pure_home/test/hello_world.c"
    echo "Please verify compiler installation and try again."
    exit 1
fi

$pure_home/$product $productbuildoptions $cc -o test.pure.$type $ldcmd hello_world.c
if [ ! -x test.pure.$type ]; then
    echo "Error: $Product did not build the test program"
    exit 1
fi

echo_no_nl ".."

echo "$Product test" > output.plain.$type
$pure_home/test/test.plain.$type >> output.plain.$type 2>&1

echo_no_nl ".."

$pure_home/test/test.pure.$type > output.pure.$type 2> output.pureerror.$type

echo_no_nl ".."

if diff output.pure.$type output.plain.$type > /dev/null; then
    echo_no_nl ".."
 else
    echo ""
    echo "$Product produced incorrect results running the test program"
    exit 1
fi

#search for output, "Hello World"
if grep "ello" output.pure.$type > /dev/null; then
    echo_no_nl ".."
 else
    echo ""
    echo "Error: $Product did not build and run the test program correctly"
    exit 1
fi

#search for errors, "This is occurring while in:"
if grep "hile in" output.pureerror.$type > /dev/null; then
    echo ""
    echo "Error: $Product did not build and run the test program correctly"
    exit 1
fi

echo_no_nl ".."
}

run_product_specific_test () {
type=$1
ldcmd=$2

echo "Done"
}

# ---- Now start testing ----
# cd there to pick up new .$product, and to run the test suite.
cd $pure_home/test

# ---- Remove old files left around ----
rm -f test.plain* test.pure* output.plain* output.pure* output.pureerror*

# Build options are set above
run_basic_test			static  $statically
run_product_specific_test	static
run_basic_test			dynamic $dynamically
run_product_specific_test	dynamic

echo
echo "--------------------------------------------------------"
echo "***                                                     "
echo "***  $Product built and ran the test program correctly. "
echo "***       You can now use $Product on your code.        "
echo "***                                                     "
echo "--------------------------------------------------------"
echo

exit 0

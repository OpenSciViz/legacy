#!/bin/sh

# this script is intended to be called by a c program wrapper,
# with some special arguments, followed by the compiler and its arguments.
# the program wrapper does Other Important Things, so it shouldn't be
# bypassed, no matter what you do to this script itself.

# -----------------------------------------------------------------------------

# protect ourselves from the user's path (save original in $userpath)

userpath=$PATH
PATH=/usr/bin:/usr/sbin:$PATH
export PATH

IFS=' 	
'
export IFS

# -----------------------------------------------------------------------------

# extract the arguments
# examples for each argument are shown; these particular examples
# would never work together.

# the name of this driver (without .sh), ex.: "purify"
driver_name=$1;         shift;

# the name of the product, ex.: "Purify"
pretty_name=$1;         shift;

# the uname, ex.: "SunOS 4.1.1"
os_version=$1;		shift;

# the directory this product is installed in, ex.: "/usr/local/purify"
install_dir=$1;		shift;

# the dir with a fake ld, ex.: "/usr/local/purify/nld"
fake_linker_dir=$1;     shift;

# if collect is in use, a perhaps different dir to use instead of
# fake_linker_dir, ex.: "/usr/local/purelink/ild".
fake_collector_dir=$1;  shift;

# the program to use as the the real linker, ex.: "/usr/local/purelink/ald"
real_linker=$1;	        shift;

# the program which is the real collector, ex.: "/usr/local/gnu/collect"
real_collector=$1;	shift;

# some special option to handle and then exit; ignore rest of line
preemptive_option=$1;   shift;

# whether to trace what doing or not, either "1" or ""
is_trace=$1;            shift;

# where the compiler was found
compiler_path=$1;	shift;

# base name of the compiler, w/o directory
compiler_basename=$1;	shift;

# the next arguments are reserved for future use
# the compiler and its arguments follow these.
dummy_a=$1;		shift;
dummy_b=$1;		shift;

# -----------------------------------------------------------------------------

# maybe trace
if test -n "$is_trace"; then
  echo "driver_name=${driver_name}"
  echo "pretty_name=${pretty_name}"
  echo "os_version=${os_version}"
  echo "install_dir=${install_dir}"
  echo "fake_linker_dir=${fake_linker_dir}"
  echo "fake_collector_dir=${fake_collector_dir}"
  echo "real_linker=${real_linker}"
  echo "real_collector=${real_collector}"
  echo "preemptive_option=${preemptive_option}"
  echo "is_trace=${is_trace}"
  echo "compiler_path=$compiler_path"
  echo "compiler_basename=$compiler_basename"
# echo "dummy_a=${dummy_a}"
# echo "dummy_b=${dummy_b}"
  echo "compilername=${1}"
  echo "compilerplusargs=$@"
  echo "PURIFYOPTIONS=${PURIFYOPTIONS}"
  echo "PUREBUILDOPTIONS=${PUREBUILDOPTIONS}"
  echo "PURECOMMANDOPTIONS=${PURECOMMANDOPTIONS}"
  echo "QUANTIFYOPTIONS=${QUANTIFYOPTIONS}"
  echo "PURELINKOPTIONS=${PURELINKOPTIONS}"
  echo "FTOPTIONS=${FTOPTIONS}"
  echo "PURE_LINKER=${PURE_LINKER}"
  echo "PURE_COLLECTOR=${PURE_COLLECTOR}"
  echo "PURE_HOME=${PURE_HOME}"
  echo "ALD_DONT=${ALD_DONT}"
  echo "DRIVER_STDERR=${DRIVER_STDERR}"
fi

# -----------------------------------------------------------------------------

show_usage()
{
    echo "Usage: ${driver_name} [${driver_name} options] cc foo.o ..."
    echo "For an options list, type: ${driver_name} -options"
    echo "For the version ID, type:  ${driver_name} -version"
echo " Contact us in the US at:"
echo "   support@pure.com        or (408) 720 1600"
echo " In Europe:"
echo "   support@europe.pure.com or (+31) 2503 85420"
echo " In Japan:"
echo "                              (03) 3863 9283"
}

# maybe handle some preemptive option and exit
if test -n "${preemptive_option}"; then
  case $preemptive_option in
    -usage|-help)
      show_usage
      exit 0;
    ;;

    -options)
      case $driver_name in
	purify)
	  echo "Please see the Purify manual or man page, which list additional options."
	  echo "Some commonly used options include:"
	  echo " -version		# give the version ID of Purify."
	  echo " -printhomedir		# print installation directory."
	  echo " -linker=[/bin/ld]	# specify an alternate linker to use."
	  echo " -collector=[/usr/local/lib/gcc-lib/sparc-sun-sunos4.1/2.3.1/ld # use with g++."
	  echo " -cache-dir=/usr/home/my-cache # place to store Purify'd object files."
	  echo " -always-use-cache-dir	# don't store Purify'd files alongside originals."
	  echo " -windows=no		# ASCII output instead of Purify Viewer."
	  echo " -verbose		# obtain a trace of Purify's build-time behavior."
	  echo "To view saved .pv files use:"
	  echo " purify -view {files.pv} # view saved files.pv."
	;;
	purecov)
	  echo "Please see the PureCoverage manual or man page, which list additional options."
	  echo "Some commonly used options include:"
	  echo " -version		# give the version ID of PureCoverage."
	  echo " -printhomedir		# print installation directory."
	  echo " -linker=[/bin/ld]	# specify an alternate linker to use."
	  echo " -collector=[/usr/local/lib/gcc-lib/sparc-sun-sunos4.1/2.3.1/ld # use with g++."
	  echo " -cache-dir=/usr/home/my-cache # place to store instrumented object files."
	  echo " -always-use-cache-dir	# don't store instrumented files alongside originals."
	  echo " -force-merge		# don't discard old data when"
	  echo "                          merging data from recompiled code."
	  echo " -verbose		# obtain a trace of PureCoverage build-time behavior."
	  echo "To view accumulated coverage data, use:"
	  echo " purecov -view {files.pcv}"
	  echo "To export coverage data to a script, use:"
	  echo " purecov -export {files.pcv}"
	  echo "To merge coverage data from separate files.pcv, use:"
	  echo " purecov -merge=result.pcv [-force-merge] {files.pcv}"
	;;
	quantify)
  	  echo "Please see the Quantify man page or the Quantify manual, which lists additional options."
  	  echo "Frequently used Quantify options:"
  	  echo " -linker			- the linker to use"
  	  echo " -collection-granularity	- the granularity of instrumentation"
  	  echo " 				  possible values: function, block, or line"
  	  echo " -use-machine			- the machine to use for instrumentation"
  	  echo " -record-data			- whether to record any data initially"
  	  echo " -record-child-process-data	- whether to record data of forked processes"
  	  echo " -record-system-calls		- whether system calls are timed"
  	  echo " -avoid-recording-system-calls	- a list of syscall numbers to avoid timing"
  	  echo " -record-register-window-traps	- whether to record register window traps"
  	  echo " -save-data-on-signals		- whether to save data on fatal signals"
  	  echo " -run-at-exit			- a sh(1) script to run when the program exits"
  	  echo " -run-at-save			- a sh(1) script to run whenever data is saved"
  	  echo " -windows			- whether to start qv(1) in display mode"
	;;
	purelink)
# make sure first option seen is -home
	  PURELINKOPTIONS=""; export PURELINKOPTIONS;
  	  ${fake_linker_dir}/ld -home=${install_dir} -options
	;;
	*)
	  echo "${driver_name}: unknown driver for -options"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    -version)
      case $driver_name in
	purify*|purecov*|quantify*)
  	  ${install_dir}/rtslave -version
        ;;

	purelink*)
	  PURELINKOPTIONS=""; export PURELINKOPTIONS;
  	  ${fake_linker_dir}/ld -home=${install_dir} -version
        ;;
	*)
	  echo "${driver_name}: unknown driver for -version"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    -test*license)
      case $driver_name in
	purify|purecov)
	  PURIFYOPTIONS=""; export PURIFYOPTIONS;
	  ${install_dir}/rtslave -test-license -home=${install_dir}
	  exit $?
        ;;
        *)
	  echo "${driver_name}: unknown preemptive option /${preemptive_option}/"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    -view)
      case $driver_name in
	purify)
	  if test -x ${install_dir}/pv.pure ; then
	    exec ${install_dir}/pv.pure -viewfile= "$@" 
	  else
	    exec ${install_dir}/pv -viewfile= "$@"
	  fi
	  exit 1;
	;;
	quantify)
	  if test -x ${install_dir}/qv.pure ; then
	    exec ${install_dir}/qv.pure "$@"
	  else
	    exec ${install_dir}/qv "$@"
	  fi
	  exit 1;
	;;
	purecov)
	  if test -x ${install_dir}/pcv.pure ; then
	    exec ${install_dir}/pcv.pure $PURECOMMANDOPTIONS "$@" $PUREBUILDOPTIONS
	  else
	    exec ${install_dir}/pcv $PURECOMMANDOPTIONS "$@" $PUREBUILDOPTIONS
	  fi
	  exit 1;
	;;
        *)
	  echo "${driver_name}: unknown preemptive option /${preemptive_option}/"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    -merge*)
      case $driver_name in
	purecov)
	  if test -x ${install_dir}/pcm.pure ; then
	    ${install_dir}/pcm.pure $PURECOMMANDOPTIONS "$preemptive_option" \
				"$@" $PUREBUILDOPTIONS
	  else
	    ${install_dir}/pcm $PURECOMMANDOPTIONS "$preemptive_option" \
				"$@" $PUREBUILDOPTIONS
	  fi
	  exit $?
	;;
        *)
	  echo "${driver_name}: unknown preemptive option /${preemptive_option}/"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    -export*)
      case $driver_name in
	purecov)
	  if test -x ${install_dir}/pcx.pure ; then
	    ${install_dir}/pcx.pure $PURECOMMANDOPTIONS "$preemptive_option" \
				"$@" $PUREBUILDOPTIONS
	  else
	    ${install_dir}/pcx $PURECOMMANDOPTIONS "$preemptive_option" \
				"$@" $PUREBUILDOPTIONS
	  fi
	  exit $?
	;;
        *)
	  echo "${driver_name}: unknown preemptive option /${preemptive_option}/"
	  exit 1;
	;;
      esac
      exit 0;
    ;;

    *)
      echo "${driver_name}: unknown preemptive option /${preemptive_option}/"
      exit 1;
    ;;

  esac
fi

# -----------------------------------------------------------------------------

# get compilername, and shift
# if no compiler, print usage and exit 1
compilername=$1
if test -z "$compilername"; then
  echo "Not enough arguments supplied to ${driver_name}."
  show_usage
  exit 1
fi
shift

# -----------------------------------------------------------------------------

# Check version of OS.
case $os_version in
  SunOS\ 5*)
	;;
  *)
	echo "$pretty_name: this version of $driver_name only works under" ;
	echo "    SunOS 5.x." ;
	echo "You are apparently running on $os_version" ;
	exit 1 ;;
esac

# -----------------------------------------------------------------------------

# Handle one Pure Software product calling another.
# "purify" and "quantify" can't be used simultaneously.
# We insist on purelink going first in part so we can detect that reliably.

case $compiler_basename in
  purify*|purecov*)
    case $driver_name in
      purelink*)
        # pass on -linker corresponding to this version of purelink
	# to purify or quantify.
	# also pass on -collector, if we were given it.
	# purify follows the "first option wins" policy in the product
	# environment variable. because we are prepending these options,
	# this means that any explicit -linker option to purify
	# will be ignored, and if both purelink and purify are given
	# -collector, purelink wins.
	extra_options="-linker=${real_linker}"
	if [ -n "${real_collector}" ]; then
	  extra_options="${extra_options} -collector=${real_collector}"
	fi
	PATH=$userpath exec $compilername $extra_options "$@"
        echo "${pretty_name}: ${driver_name} couldn't find an executable ${compilername}."
        exit 1
      ;;
      purecov*|purify*)
	other_dir=`dirname ${compiler_path}`
	if cmp -s ${install_dir}/.Version.engine  ${other_dir}/.Version.engine ; then
	  PATH=$userpath exec $compilername "$@"
	  echo "${pretty_name}: ${driver_name} couldn't find an executable ${compilername}."
	else
	  echo "${pretty_name}: Attempting to use incompatible versions of Purify and PureCoverage."
	  echo "Using ${compiler_basename} from ${other_dir}"
	  PURECOMMANDOPTIONS= ${compiler_path} -version
	  echo "Using ${driver_name} from ${install_dir}"
	  PURECOMMANDOPTIONS= ${install_dir}/${driver_name} -version
	  echo "Please check your PATH variable or pathnames for purify and purecov."
	fi
        exit 1
      ;;
      *)
        echo "${pretty_name}: Sorry, ${driver_name} can't be used together with ${compilername}."
        exit 1
      ;;
    esac
  ;;

  quantify*)
    case $driver_name in
      purelink*)
        # pass on -linker corresponding to this version of purelink
	# to purify or quantify.
	# also pass on -collector, if we were given it.
	# purify follows the "first option wins" policy in the product
	# environment variable. because we are prepending these options,
	# this means that any explicit -linker option to purify
	# will be ignored, and if both purelink and purify are given
	# -collector, purelink wins.
	extra_options="-linker=${real_linker}"
	if [ -n "${real_collector}" ]; then
	  extra_options="${extra_options} -collector=${real_collector}"
	fi
	PATH=$userpath exec $compilername $extra_options "$@"
        echo "${pretty_name}: ${driver_name} couldn't find an executable ${compilername}."
        exit 1
      ;;
      *)
        echo "${pretty_name}: Sorry, ${driver_name} can't be used together with ${compilername}."
        exit 1
      ;;
    esac
  ;;

  purelink*)
    case $driver_name in
      purelink)
        echo "${pretty_name}: ${driver_name} and ${compilername} can't appear together on the link line"
	exit 1
      ;;
      *)
        echo "${pretty_name}: ${compilername} must go first on the link line, before ${driver_name}"
        exit 1
      ;;
    esac
  ;;

  # At this point, $compiler_basename probably really is a compiler, not a
  # another Pure Software driver being nested within this one.
  # But just so we have a ghost of a chance of backward compatibility, we
  # assume that anything with a ".purehome" file in the directory is a Pure
  # Software product.
  *)
    if [ -r `dirname ${compiler_path}`/.purehome ]; then
      # push ourselves onto the stack
      DRIVER_HISTORY="${DRIVER_HISTORY}:${fake_linker_dir}/${driver_name}"

      # invoke the thing, whatever it is
      PATH=$userpath exec $compilername $extra_options "$@"
      echo "${pretty_name}: ${driver_name} couldn't find an executable ${compilername}."
      exit 1
    fi
  ;;
      
esac

# -----------------------------------------------------------------------------

intercept_dir=${fake_linker_dir}

case $compiler_basename in
  gcc*|g++*)

    case $compiler_basename in
    g++*)
      case $driver_name in
      purify)
	PURECOMMANDOPTIONS="-g++=yes ${PURECOMMANDOPTIONS}"; export PURECOMMANDOPTIONS;
      ;;
      quantify)
	QUANTIFYOPTIONS="-g++=yes ${QUANTIFYOPTIONS}"; export QUANTIFYOPTIONS;
      ;;

      ftify)
	FTOPTIONS="-g++=yes ${FTOPTIONS}"; export FTOPTIONS;
      ;;
      esac
    ;;
    esac
    # General gcc/g++ case:

    # After our engine, which is called ld, finishes it job, it calls
    # CallLinker(), which calls gcc's collector if it's g++ compiled.
    # gcc2.5.x calls it's collector ld as well. When this collector finishes
    # it's job, it searches for the real_ld, called ld, while ignoring any
    # directory specified by the COLLECT_NAME env variable.
    # If we don't put purify's engine in this env variable, our engine will
    # get called AGAIN by gcc's collector.  BAD!!
    #
    # There is also an env variable called COLLECT_NAMES. The collector
    # uses this environment variable to determine whether it is in the
    # first or second pass. Originally we were setting this variable, which
    # was very bad because the collector finds it in the env on the first pass
    # and ld is execvp'ed without the collector actually getting to the nm.
    # Static constructors will then never get called during execution of the program.

    if [ "${COLLECT_NAME}" = "" ]; then
	COLLECT_NAME="${fake_linker_dir}/ld"
    else
	COLLECT_NAME="${COLLECT_NAME}:${fake_linker_dir}/ld";
    fi
    export COLLECT_NAME

    # The gcc family doesn't support multiple -B options.
    # so can't use -B to specify the linker directory, if the user already is.
    # Testing for this is somewhat complicated because any -B option might
    # be preceeded by a -Xlinker option, which is an escape mechanism to pass
    # the next option directly to the linker.  We take care of the most common
    # cases there by ignoreing options -Bstatic and -Bdynamic.
    # The environment variable GCC_EXEC_PREFIX is an alternative, but there too,
    # there is just one value it can have.
    # We print an error if either is already used.
    # We don't use GCC_EXEC_PREFIX because that will hit everything below us,
    # which will foul up what linker the collector calls.
    # so not: GCC_EXEC_PREFIX=$intercept_dir/; export GCC_EXEC_PREFIX,
    # but instead we use -B.
    # Using -B has the downside that the compiler may print a warning like:
    # "gcc2.3.1: file path prefix `/usr/p/Purify/nld/' never used",
    # causing a fatal error to some filters.
    if echo $* | sed s/-Bstatic// |sed s/-Bdynamic// | egrep -s ' -B' ; then
      echo "${pretty_name}: You cannot use the -B<path> option to gcc or g++ with ${pretty_name}."
      echo "Please contact support@pure.com for further information."
      status=1
    elif test -n "$GCC_EXEC_PREFIX"; then
      echo "${pretty_name}: You have set the gcc variable GCC_EXEC_PREFIX."
      echo "This does not work with ${pretty_name}."
      echo "Please contact support@pure.com for further information."
      status=1
    fi
  ;;
esac

# if there is a specified a collector, make sure we call it even if not g++
# fake_collector_dir == fake_linker_dir for purify and quantify, but not
# for purelink.
if test -n "$real_collector"; then
  intercept_dir=${fake_collector_dir}
fi
# -----------------------------------------------------------------------------
# site-wide options may be conveniently set here.
# For example, for Purify, you could uncomment the following line:
# PURIFYOPTIONS="$PURIFYOPTIONS -cache-dir=/usr/local/my_cache"; export PURIFYOPTIONS
# Purify options are parsed left-to-right until the required option is found,
# so adding site-wide options at the end allows them to be overridden.
# PureLink is the opposite; the last occurence wins, so you would do this:
# PURELINKOPTIONS="-banner=2 $PURELINKOPTIONS"; export PURELINKOPTIONS
# -----------------------------------------------------------------------------
# set some environment variables for HP's Blinclink

case $driver_name in
 */purify*|*purify*)
    BLD_PURIFYING=true; export BLD_PURIFYING
#e.g:    PURIFYOPTIONS="${PURIFYOPTIONS} -cache-dir=/usr/local/pure/cache -system-directories="/lib:/opt:/usr/lib:/usr/5lib:/usr/ucb/lib:/usr/lang:/usr/local"; export PURIFYOPTIONS
    ;;
 */quantify*|*quantify*)
    BLD_PURIFYING=0; export BLD_PURIFYING
    BLD_QUANTIFYING=true; export BLD_QUANTIFYING
    ;;
esac

BLD_LINKER=$intercept_dir/ld; export BLD_LINKER


# -----------------------------------------------------------------------------
# Arrange for ${intercept_dir}/ld to be called as the linker.
# This is generally done by passing to the compiler an additional
# directory for it to look for ld in, namely $intercept_dir.
# ${intercept_dir}/ld is the outermost program to invoke instead of /bin/ld.
# For example, it may be purify, or quantify.
# --THIS OS HAS NO COLLECT, NOR NEED FOR IT.--
# The driver program above us already checked that ${intercept_dir}/ld
# is executable.

# By the time the linker (fake or real) is called,
# it is difficult at best to recover the name of the compiler.
# that is why we have to set some of these options here,
# such as -ignore-signals.

case $compiler_basename in

# GNU gcc compiler family (including g++)
    gcc*|g++*)
	PATH=$userpath $compilername -B$intercept_dir/ "$@"
        status=$?
        ;;

# Sun cc
    cc*)
        PATH=$userpath $compilername -Yl,$intercept_dir "$@"
        status=$?
        ;;

# Sun acc
    acc*)
        PATH=$userpath $compilername -Qpath $intercept_dir "$@"
        status=$?
        ;;

# Sun CC, etc.
    CC*|C\+\+*)
	# We assume that C++'s which are binaries support -Qpath,
	# while C++'s which are scripts are AT&T cfront-derived and don't
	# take -Qpath, but do accept a ccC environment variable which specifies
	# the C compiler to use. In that latter case, we put our driver name
	# in front of the C compiler name, so later when the compilation stage
	# is invoked on $ccC, our driver will be called _again_, that time
	# with a different compiler which we know how to handle.
	if file ${compiler_path} | egrep -s 'script' ; then
	    # CenterLine C++ might call clcc, not cc.
	    # But clcc may not be in the user's path, and the C++
	    # we are called with may not be in the CenterLine installation
	    # directory nor a symbolic link to it, but instead a script
	    # which calls the real CenterLine CC script (because that is
	    # what CenterLine advises). So we may not be able to find clcc,
	    # _and_ we may not be able to tell if this is CenterLine's C++.
	    # sigh.
	    #
	    # In general, if you know what C compiler you want invoked
	    # underneath your CC script, fill it in here.
	    # If this isn't specified (it isn't, as shipped), we try
	    # for clcc if we suspect this is CenterLine, otherwise we use cc.

	    # maybe somebody has edited this script to force the C compiler
	    force_C_compiler=""
	    if [ -n "$force_C_compiler" ]; then
	      ccC="${install_dir}/${driver_name} $force_C_compiler"

	    # if ccC is already set, just put our driver in front.
            elif test -n "$ccC"; then
	      ccC="${install_dir}/${driver_name} $ccC"

	    # maybe this is CenterLine, try for clcc
	    elif sed 3q ${compiler_path} | egrep -s 'CenterLine' ; then
	      # first try for clcc in same directory as CC (expr does dirname)
		clcc_dir=`expr \( x"$compiler_path" : x'\(/\)$' \) \| \( x"$compiler_path" : x'\(.*\)/[^/]*$' \) \| .`
		if [ -x ${clcc_dir}/clcc ]; then
		  ccC="${install_dir}/${driver_name} ${clcc_dir}/clcc -traditional -w"
		elif clcc_prog=`which clcc 2>/dev/null` ; then
		  ccC="${install_dir}/${driver_name} ${clcc_prog} -traditional -w"
		else
		  ccC="${install_dir}/${driver_name} cc"
		fi

            # otherwise, just use cc as the C compiler (hope it is in path)
	    else
		ccC="${install_dir}/${driver_name} cc"
            fi
            export ccC
            PATH=$userpath $compilername "$@"
            status=$?
        else
	    # Program based CC:
            PATH=$userpath $compilername -Qpath $intercept_dir "$@"
            status=$?
        fi
        ;;

# CenterLine's clcc
    clcc*)
        PATH=$userpath $compilername -Hlinker=$intercept_dir/ld "$@"
        status=$?
        ;;

# ld direct
    ld*)
        PURE_LINKER="${compilername}" PATH=$userpath ${intercept_dir}/ld "$@"
        status=$?
        ;;

#
# Preliminary testing with the Apogee compiler looks
# reasonable, but until more extensive testing is completed,
# leave this compiler commented out.
# If you want to use the Apogee compiler with Purify,
# uncomment the following lines.
#
# Apogee C and Fortran Compilers
     *apcc|*apCC|*apcf|*apf77|*apf90)
         PATH=$userpath $compilername -Yl,$intercept_dir "$@"
         status=$?
         ;;


#
# The rest are hangovers from a previous life.
# They may not work as expected, and are thus commented out until tested.
#

# 
# # Metaware hc
#     hc*)
#         PATH=$userpath $compilername -Hlinker=$intercept_dir/ld "$@"
#         status=$?
#         ;;
# 
# Sun's fortran
     f77*)
         PATH=$userpath $compilername -Qpath $intercept_dir "$@"
         status=$?
         ;;
#
# # Lucid compiler
#     lcc*)
#         PATH=$userpath $compilername -Y l,$intercept_dir "$@"
#         status=$?
#         ;;
#
# ObjectStore
    OSCC)
# From: William Brew <bill@drums.reasoning.com> Dec 1992
# added patterns for matching OSCC

# Uncomment if you're using pre 3.0 Objectstore OSCC
# and make sure extraarg="" in the cases below
#	if test -z "$osccC"; then
#		osccC="${install_dir}/${driver_name} cc"
#	else
#		osccC="${install_dir}/${driver_name} $osccC"
#	fi
#	export osccC
#	extraarg=""

	case $driver_name in
	  purify|purecov)
	  extraarg="-purify"
	  PURECOMMANDOPTIONS="${PURECOMMANDOPTIONS} -ignore-signals=SIGSEGV -check-mmaps=no -search-mmaps=yes";
	  export PURECOMMANDOPTIONS;
	;;
	  quantify)
	  extraarg="-quantify"
	  QUANTIFYOPTIONS="${QUANTIFYOPTIONS} -ignore-signals=SIGSEGV";
	  export QUANTIFYOPTIONS;
	;;
	  purelink)
	  extraarg="-purelink"
	;;
	*)
	;;
	esac
	PATH=$userpath $compilername ${extraarg} "$@"
	status=$?
	;;

# ObjectStore some more
    *os_do_link)
#        ObjectStore from Object Design, Inc.
#        The linker, os_do_link, is a script that invokes OSCC, a version of cfront/CC.
	compschemaname=$1
	appschemaname=$2
	execfile=$3
	# get rid of compschema, appschema, exec, and OSCC
	shift
	shift
	shift
	shift
	case $driver_name in
	  purify|purecov)
	  PURECOMMANDOPTIONS="${PURECOMMANDOPTIONS} -ignore-signals=SIGSEGV -check-mmaps=no -search-mmaps=yes";
	  export PURECOMMANDOPTIONS;
	;;
	  quantify)
	  QUANTIFYOPTIONS="${QUANTIFYOPTIONS} -ignore-signals=SIGSEGV";
	  export QUANTIFYOPTIONS;
	;;
	*)
	;;
	esac
	PATH=$userpath $compilername "$compschemaname" "$appschemaname" "$execfile" OSCC -Qpath $intercept_dir "$@"
	status=$?
	;;

# # Taumetric Oregon C++
#     occ*)
# # From: Steve Covault <support@taumet.com> Jan 1993
# # added patterns for matching Oregon C++
# 	if test -z "$occC"; then
# 		occC="${install_dir}/${driver_name} cc"
# 	else
# 		occC="${install_dir}/${driver_name} $occC"
# 	fi
# 	export occC
#         OREGONLNT=$intercept_dir/ld
# 	export OREGONLNT
# 	PATH=$userpath $compilername "$@"
# 	status=$?
# 	;;
#

# that's all folks
    *)
        echo "Sorry, '$compilername' is an unknown compiler."
        echo "Usage: ${driver_name} <compiler> foo.o ..."
	echo "${pretty_name} only knows about the following compilers:"
	echo "	cc, acc, CC, gcc, g++, clcc, OSCC and ld."
	if [ "$driver_name" = "purelink" ]; then
	  echo "purelink can also be followed by quantify or purify instead of a compiler"
	fi
        echo "See details in script: ${install_dir}/${driver_name}.sh"
        exit 1
        ;;
esac



exit $status

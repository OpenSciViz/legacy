/*
 * Stub definitions to satisfy your linker when not using purify/quantify.
 * When you are using purify/quantify these calls are intercepted.
 *
 * Explicitly no copyright.
 * You may recompile and redistribute these definitions as required.
 */

/*
 * The "stub" definition tokens are used both to
 * select a function and name a temporary file.
 * The definition names are shortened so there will be no filename conflicts
 * when ar goes to compose pure_stubs.a.
 *
 * The following script may be used to build the archive pure_stubs.a.
 *
 * #!/bin/sh
 * STUBS=`awk '/^#ifdef.*STUB$/ { print $2 }' < pure_stubs.c`
 * FILES=
 * for stub_def in ${STUBS} ; do
 *     file=`echo ${stub_def} | sed 's/$/.o/'`
 *     echo "cc -D${stub_def} -c pure_stubs.c -o ${file}"
 *     cc -D${stub_def} -c pure_stubs.c -o ${file}
 *     FILES="${FILES} ${file}"
 * done
 * rm pure_stubs.a
 * ar cq pure_stubs.a ${FILES}
 * ranlib pure_stubs.a
 *
 */

/* Prototype macro to suit non ANSI compilers. */
#if defined(__STDC__)
#define P(args) args
#else
#define P(args) ()
#define const
#endif

#ifdef p_stop_here_internal_PSTUB
void purify_stop_here_internal P(())		{ }
#endif

#ifdef p_variables_PSTUB
unsigned long purify_report_address;
unsigned long purify_report_number;
unsigned long purify_report_type;
unsigned long purify_report_result;
#endif

#ifdef p_new_inuse_PSTUB
int purify_new_inuse P(())			{ return 0; }
#endif
#ifdef p_all_inuse_PSTUB
int purify_all_inuse P(())			{ return 0; }
#endif
#ifdef p_clear_inuse_PSTUB
int purify_clear_inuse P(())			{ return 0; }
#endif
#ifdef p_new_leaks_PSTUB
int purify_new_leaks P(())			{ return 0; }
#endif
#ifdef p_all_leaks_PSTUB
int purify_all_leaks P(())			{ return 0; }
#endif
#ifdef p_clear_leaks_PSTUB
int purify_clear_leaks P(())			{ return 0; }
#endif

#ifdef p_all_fds_inuse_PSTUB
int purify_all_fds_inuse P(())			{ return 0; }
#endif
#ifdef p_new_fds_inuse_PSTUB
int purify_new_fds_inuse P(())			{ return 0; }
#endif

#ifdef p_new_messages_PSTUB
int purify_new_messages P(())			{ return 0; }
#endif
#ifdef p_all_messages_PSTUB
int purify_all_messages P(())			{ return 0; }
#endif
#ifdef p_clear_new_messages_PSTUB
int purify_clear_new_messages P(())		{ return 0; }
#endif
/* These should be removed in a future release because they duplicate the _messages functions */
#ifdef p_new_reports_PSTUB
int purify_new_reports P(())			{ return 0; }
#endif
#ifdef p_all_reports_PSTUB
int purify_all_reports P(())			{ return 0; }
#endif
#ifdef p_clear_new_reports_PSTUB
int purify_clear_new_reports P(())		{ return 0; }
#endif

#ifdef p_start_batch_PSTUB
int purify_start_batch P(())			{ return 0; }
#endif
#ifdef p_start_batch_show_first_PSTUB
int purify_start_batch_show_first P(())		{ return 0; }
#endif
#ifdef p_stop_batch_PSTUB
int purify_stop_batch P(())			{ return 0; }
#endif
#ifdef p_print_PSTUB
int purify_print P((void *addr))		{ return 0; }
#endif
#ifdef p_describe_PSTUB
int purify_describe P((void *addr))		{ return 0; }
#endif
#ifdef p_what_colors_PSTUB
int purify_what_colors P((void *addr, int len)) { return 0; }
#endif
#ifdef p_watch_PSTUB
int purify_watch P((const char *addr))		{ return 0; }
#endif
#ifdef p_watch_1_PSTUB
int purify_watch_1 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_2_PSTUB
int purify_watch_2 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_4_PSTUB
int purify_watch_4 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_8_PSTUB
int purify_watch_8 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_w_1_PSTUB
int purify_watch_w_1 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_w_2_PSTUB
int purify_watch_w_2 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_w_4_PSTUB
int purify_watch_w_4 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_w_8_PSTUB
int purify_watch_w_8 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_rw_1_PSTUB
int purify_watch_rw_1 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_rw_2_PSTUB
int purify_watch_rw_2 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_rw_4_PSTUB
int purify_watch_rw_4 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_rw_8_PSTUB
int purify_watch_rw_8 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_r_1_PSTUB
int purify_watch_r_1 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_r_2_PSTUB
int purify_watch_r_2 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_r_4_PSTUB
int purify_watch_r_4 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_r_8_PSTUB
int purify_watch_r_8 P((const char *addr))	{ return 0; }
#endif
#ifdef p_watch_ast_PSTUB
int purify_watch_ast P((const char *addr, int size, int type)) { return 0; }
#endif
#ifdef p_watch_n_PSTUB
int purify_watch_n P((const char *addr, int size, int type)) { return 0; }
#endif
#ifdef p_watch_info_PSTUB
int purify_watch_info P(())			{ return 0; }
#endif
#ifdef p_watch_remove_PSTUB
int purify_watch_remove P((int watchno))	{ return 0; }
#endif
#ifdef p_watch_remove_all_PSTUB
int purify_watch_remove_all P(( ))		{ return 0; }
#endif

#ifdef p_printf_PSTUB
int purify_printf P(())				{ return 0; }
#endif
#ifdef p_printf_with_call_chain_PSTUB
int purify_printf_with_call_chain P(())		{ return 0; }
#endif
#ifdef p_printf_with_banner_PSTUB
int purify_printf_with_banner P(())		{ return 0; }
#endif
#ifdef p_logfile_printf_PSTUB
int purify_logfile_printf P(())			{ return 0; }
#endif

#ifdef pure_printf_PSTUB
int pure_printf P(()) 				{ return 0; }
#endif
#ifdef pure_printf_with_banner_PSTUB
int pure_printf_with_banner P(())		{ return 0; }
#endif
#ifdef pure_logfile_printf_PSTUB
int pure_logfile_printf P(())			{ return 0; }
#endif

#ifdef p_call_original_PSTUB
void *purify_call_original P((...))		{ return 0; }
#endif

#ifdef p_process_file_PSTUB
int
purify_process_file (in, out)
  const char *in;
  char *out;
{
    strcpy(out, in);
    return 0; /* Purify not running */
}
#endif

#ifdef p_note_loaded_file_PSTUB
void purify_note_loaded_file P((char* filename, unsigned int where_loaded)) { }
#endif


#ifdef p_is_running_PSTUB
int purify_is_running P(())
{
    return 0;
}
#endif

#ifdef p_unsafe_memcpy_PSTUB
extern void *memcpy ();
char *
purify_unsafe_memcpy(dst, src, n)
  char *dst;
  char *src;
  int n;
{
    return (char *)memcpy(dst, src, n);
}
#endif

#ifdef p_assert_is_readable_PSTUB
int
purify_assert_is_readable(src, n)
  const char *src;
  int n;
{
    return 1;
}
#endif

#ifdef p_assert_is_writable_PSTUB
int
purify_assert_is_writable(dst, n)
  const char *dst;
  int n;
{
    return 1;
}
#endif

#ifdef _p_is_readable_PSTUB
int
_p_is_readable(src, n)
  char *src;
  int n;
{
    return 1;
}
#endif

#ifdef _p_is_writable_PSTUB
int
_p_is_writable(src, n)
  char *src;
  int n;
{
    return 1;
}
#endif

#ifdef _p_is_initialized_PSTUB
int
_p_is_initialized(src, n)
  char *src;
  int n;
{
    return 1;
}
#endif

/*
 * Pool Allocator functionality
 * ============================
 */

#ifdef p_set_pool_id_PSTUB
void
purify_set_pool_id(mem, id)
  char *mem;
  int id;
{
}
#endif

#ifdef p_get_pool_id_PSTUB
int
purify_get_pool_id(mem)
  char *mem;
{
    return 0;
}
#endif

#ifdef p_set_user_data_PSTUB
void
purify_set_user_data(mem, data)
  char *mem;
  void *data;
{
}
#endif

#ifdef p_get_user_data_PSTUB
void *
purify_get_user_data(mem)
  char *mem;
{
    return 0;
}
#endif

#ifdef p_map_pool_PSTUB
void
purify_map_pool(id, fn)
  int id;
  void (*fn)();
{
}
#endif

#ifdef p_map_pool_id_PSTUB
void
purify_map_pool_id(fn)
  void (*fn)();
{
}
#endif

#ifdef p_start_of_block_PSTUB
char *
purify_start_of_block(mem)
  char *mem;
{
    return (char *)0;
}
#endif

#ifdef p_size_of_block_PSTUB
int
purify_size_of_block(mem)
  char *mem;
{
    return 0;
}
#endif

/*
 * Raw malloc interface.
 * ====================
 */

#ifdef p_add_malloc_marker_size_PSTUB
int
purify_add_malloc_marker_size(size)
  int size;
{
    return ((size + 7) & ~7);
}
#endif

#ifdef p_malloc_header_size_PSTUB
int
purify_malloc_header_size(size)
  int size;
{
    return 0;
}
#endif

#ifdef p_malloc_PSTUB
extern char *malloc();
char *
purify_malloc(size)
  int size;
{
    return malloc(size);
}
#endif

#ifdef p_free_PSTUB
extern int free();
int
purify_free(p)
  char *p;
{
    return free(p);
}
#endif

#ifdef p_realloc_PSTUB
extern char *realloc();
char *
purify_realloc(p, size)
  char *p;
  int size;
{
    return realloc(p, size);
}
#endif

#ifdef p_calloc_PSTUB
extern char *calloc();
char *
purify_calloc(nelem, size)
  int nelem;
  int size;
{
    return calloc(nelem, size);
}
#endif

#ifdef p_memalign_PSTUB
extern char *memalign();
char*
purify_memalign(alignment, len)
  int alignment;
  int len;
{
    return memalign(alignment, len);
}
#endif

#if !defined(HPUX)

#ifdef p_cfree_PSTUB
extern int cfree();
int
purify_cfree(ptr)
  char *ptr;
{
    return cfree(ptr);
}
#endif

#ifdef p_valloc_PSTUB
extern char *valloc();
char*
purify_valloc(size)
  unsigned size;
{
    return valloc(size);
}
#endif

#endif /* !HPUX */

#ifdef p_brk_PSTUB
extern int brk();
int
purify_brk(limit)
  char *limit;
{
    return brk(limit);
}
#endif

#ifdef p_sbrk_PSTUB
extern char *sbrk();
char *
purify_sbrk(incr)
  int incr;
{
    return sbrk(incr);
}
#endif

#ifdef p_mark_as_malloc_PSTUB
char *
purify_mark_as_malloc(block, user_size, skip_stack_frames)
  char *block;
  int user_size;
  int skip_stack_frames;
{
    return block;
}
#endif

#ifdef p_mark_as_free_PSTUB
char *
purify_mark_as_free(user_block, skip_stack_frames)
  char *user_block;
  int skip_stack_frames;
{
    return user_block;
}
#endif

#ifdef p_clear_malloc_markers_PSTUB
int
purify_clear_malloc_markers(block, size)
  char *block;
  int size;
{
    return 0;
}
#endif

#ifdef p_list_malloc_blocks_PSTUB
void
purify_list_malloc_blocks(mem, size)
  char *mem;
  int size;
{
}
#endif

#ifdef _p_red_PSTUB
void
_p_red(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_red_addr_PSTUB
void
_p_red_addr(addr1, addr2, size, islocal)
  void *addr1,*addr2; unsigned int size; int islocal;
{
}
#endif

#ifdef _p_green_PSTUB
void
_p_green(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_yellow_PSTUB
void
_p_yellow(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_blue_PSTUB
void
_p_blue(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_mark_as_initialized_PSTUB
void
_p_mark_as_initialized(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_mark_as_uninitialized_PSTUB
void
_p_mark_as_uninitialized(addr, len)
  void *addr;
  int len;
{
}
#endif

#ifdef _p_mark_for_trap_PSTUB
void
_p_mark_for_trap(addr, len)
  void* addr;
  int len;
{
}
#endif

#ifdef _p_mark_for_no_trap_PSTUB
void
_p_mark_for_no_trap(addr, len)
  void* addr;
  int len;
{
}
#endif

#ifdef _p_lift_watchpoints_PSTUB
int
_p_lift_watchpoints(marked, marked_len)
  char *marked;
  int marked_len;
{
    return 0;
}
#endif

#ifdef _p_restore_watchpoints_PSTUB
void
_p_restore_watchpoints(marked, marked_len)
  char *marked;
  int marked_len;
{
}
#endif

#ifdef _pc_is_running_XSTUB
int purecov_is_running ()
{
    return 0;
}
#endif

#ifdef _pc_set_filename_XSTUB
int
purecov_set_filename(filename)
  const char *filename;
{
    return 0;
}
#endif

#ifdef _pc_save_data_XSTUB
int
purecov_save_data()
{
    return 0;
}
#endif

#ifdef _pc_clear_data_XSTUB
int
purecov_clear_data()
{
    return 0;
}
#endif

#ifdef _pc_disable_save_XSTUB
int
purecov_disable_save()
{
    return 0;
}
#endif

#ifdef _pc_enable_save_XSTUB
int
purecov_enable_save()
{
    return 0;
}
#endif

/* The thread stubs are for both Purify and Quantify */
#ifdef a_name_thread_QSTUB
#define a_name_thread_ALL_STUB
#endif
#ifdef a_name_thread_PSTUB
#define a_name_thread_ALL_STUB
#endif
#ifdef a_name_thread_ALL_STUB
int pure_name_thread P((const char * name))     { return 0; }
#endif


/* Quantify specific definitions */
/*
 * To avoid file name conflicts, tokens should abbreivate:
 * record/recording	-> rec
 * start		-> stt
 * stop			-> stp
 */
#ifdef q_stop_here_internal_QSTUB
void quantify_stop_here_internal P(())		{ }
#endif

#ifdef q_disable_rec_QSTUB
int quantify_disable_recording_data P((void))	{ return 0; }
#endif
#ifdef q_rec_data_QSTUB
int quantify_record_data P((int on_off))	{ return 0; }
#endif
#ifdef q_stt_rec_data_QSTUB
int quantify_start_recording_data P(())		{ return 0; }
#endif
#ifdef q_stp_rec_data_QSTUB
int quantify_stop_recording_data P(())		{ return 0; }
#endif

#ifdef q_rec_system_calls_QSTUB
int quantify_record_system_calls P((int on_off)) { return 0; }
#endif
#ifdef q_stt_rec_system_calls_QSTUB
int quantify_start_recording_system_calls P(())	{ return 0; }
int quantify_start_recording_system_call  P((char *syscalls)) 	{ return 0; }
#endif
#ifdef q_stp_rec_system_calls_QSTUB
int quantify_stop_recording_system_calls P(())	{ return 0; }
int quantify_stop_recording_system_call  P((char *syscalls))	{ return 0; }
#endif

#ifdef q_rec_rw_traps_QSTUB
int quantify_record_register_window_traps P((int on_off)) { return 0; }
#endif
#ifdef q_stt_rec_rw_traps_QSTUB
int quantify_start_recording_register_window_traps P(()) { return 0; }
#endif
#ifdef q_stp_rec_rw_traps_QSTUB
int quantify_stop_recording_register_window_traps P(())	{ return 0; }
#endif

#ifdef	q_rec_dyn_lib_QSTUB
int quantify_start_recording_dynamic_library_data P((void))	{ return 0; }
int quantify_stop_recording_dynamic_library_data P((void))	{ return 0; }
#endif	/* q_rec_dyn_lib_QSTUB */


#ifdef q_clear_data_QSTUB
int quantify_clear_data P(())			{ return 0; }
#endif
#ifdef q_save_data_QSTUB
int quantify_save_data P(())			{ return 0; }
#endif
#ifdef q_save_data_to_file_QSTUB
int quantify_save_data_to_file P((char *filename)) { return 0; }
#endif
#ifdef q_add_annotation_QSTUB
int quantify_add_annotation P((char *str))	{ return 0; }
#endif
#ifdef pure_printf_QSTUB
int pure_printf P(()) 				{ return 0; }
#endif
#ifdef pure_printf_with_banner_QSTUB
int pure_printf_with_banner P(())		{ return 0; }
#endif
#ifdef pure_logfile_printf_QSTUB
int pure_logfile_printf P(())			{ return 0; }
#endif
#ifdef q_help_QSTUB
int quantify_help P(()) 			{ return 0; }
#endif
#ifdef q_print_recording_state_QSTUB
int quantify_print_recording_state P(())	{ return 0; }
#endif

#ifdef q_process_file_QSTUB
int quantify_process_file (in, out)
     const char *in;
     char *out;
{
    while ((*out++ = *in++));
    return 0; /* Quantify not running */
}
#endif

#ifdef q_note_loaded_file_QSTUB
void quantify_note_loaded_file P((char* filename,
				  unsigned int where_loaded)) { return; }
#endif

/* version in Quantify object file returns TRUE */
#ifdef q_is_running_QSTUB
int quantify_is_running P(())			{ return 0; }
#endif


#ifdef	q_is_rec_data_QSTUB
int quantify_is_recording_data P((void))	{ return 0; }
#endif	/* q_is_rec_data_QSTUB */

#ifdef	q_rec_syscalls_QSTUB
int quantify_is_recording_system_calls P((void))	{ return 0; }
#endif	/* q_rec_syscalls_QSTUB */

#ifdef	q_rec_syscall_QSTUB
int quantify_is_recording_system_call P((char *call))	{ return 0; }
#endif	/* q_rec_syscall_QSTUB */

#ifdef	q_is_rec_rw_traps_QSTUB
int quantify_is_recording_register_window_traps P((void))	{ return 0; }
#endif	/* q_is_rec_rw_traps_QSTUB */

#ifdef	q_is_rec_dynlib_QSTUB
int quantify_is_recording_dynamic_library_data P((void))	{ return 0; }
#endif	/* q_is_rec_dynlib_QSTUB */

#ifdef q_thr_disable_rec_QSTUB
int quantify_thread_disable_recording_data P((void))	{ return 0; }
#endif
#ifdef q_thr_rec_data_QSTUB
int quantify_thread_record_data P((int on_off))	{ return 0; }
#endif
#ifdef q_thr_stt_rec_data_QSTUB
int quantify_thread_start_recording_data P(())		{ return 0; }
#endif
#ifdef q_thr_stp_rec_data_QSTUB
int quantify_thread_stop_recording_data P(())		{ return 0; }
#endif

#ifdef q_thr_rec_system_calls_QSTUB
int quantify_thread_record_system_calls P((int on_off)) { return 0; }
#endif
#ifdef q_thr_stt_rec_system_calls_QSTUB
int quantify_thread_start_recording_system_calls P(())	{ return 0; }
int quantify_thread_start_recording_system_call  P((char *syscalls)) 	{ return 0; }
#endif
#ifdef q_thr_stp_rec_system_calls_QSTUB
int quantify_thread_stop_recording_system_calls P(())	{ return 0; }
int quantify_thread_stop_recording_system_call  P((char *syscalls))	{ return 0; }
#endif

#ifdef q_thr_rec_rw_traps_QSTUB
int quantify_thread_record_register_window_traps P((int on_off)) { return 0; }
#endif
#ifdef q_thr_stt_rec_rw_traps_QSTUB
int quantify_thread_start_recording_register_window_traps P(()) { return 0; }
#endif
#ifdef q_thr_stp_rec_rw_traps_QSTUB
int quantify_thread_stop_recording_register_window_traps P(())	{ return 0; }
#endif

#ifdef	q_thr_rec_dyn_lib_QSTUB
int quantify_thread_start_recording_dynamic_library_data P((void))	{ return 0; }
int quantify_thread_stop_recording_dynamic_library_data P((void))	{ return 0; }
#endif	/* q_thr_rec_dyn_lib_QSTUB */


#ifdef q_thr_clear_data_QSTUB
int quantify_thread_clear_data P(())			{ return 0; }
#endif
#ifdef q_thr_save_data_QSTUB
int quantify_thread_save_data P(())			{ return 0; }
#endif
#ifdef q_thr_save_data_to_file_QSTUB
int quantify_thread_save_data_to_file P((char *filename)) { return 0; }
#endif
#ifdef q_thr_add_annotation_QSTUB
int quantify_thread_add_annotation P((char *str))	{ return 0; }
#endif

#ifdef	q_thr_is_rec_data_QSTUB
int quantify_thread_is_recording_data P((void))	{ return 0; }
#endif	/* q_thr_is_rec_data_QSTUB */

#ifdef	q_thr_is_rec_syscalls_QSTUB
int quantify_thread_is_recording_system_calls P((void))	{ return 0; }
#endif	/* q_thr_is_rec_syscalls_QSTUB */

#ifdef	q_thr_is_rec_syscall_QSTUB
int quantify_thread_is_recording_system_call P((char *call))	{ return 0; }
#endif	/* q_thr_is_rec_syscall_QSTUB */

#ifdef	q_thr_is_rec_rw_traps_QSTUB
int quantify_thread_is_recording_register_window_traps P((void))	{ return 0; }
#endif	/* q_thr_is_rec_rw_traps_QSTUB */

#ifdef	q_thr_is_rec_dynlib_QSTUB
int quantify_thread_is_recording_dynamic_library_data P((void))	{ return 0; }
#endif	/* q_thr_is_rec_dynlib_QSTUB */
  

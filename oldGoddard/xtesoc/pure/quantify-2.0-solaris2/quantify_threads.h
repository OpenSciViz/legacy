/*
 * The Quantify Threads API
 *
 * Copyright (c) 1992-1995 Pure Software.
 *
 * This file contains proprietary and confidential information and
 * remains the unpublished property of Pure Software. Use, disclosure,
 * or reproduction is prohibited except as permitted by express written
 * license agreement with Pure Software.
 *
 */

/*
 * This file contains prototypes for and descriptions of Quantify API
 * (Application Program Interface) functions that are specific to
 * multi-threaded programs.  For prototypes for and descriptions of
 * the more general-purpose Quantify API functions, see the file
 * `quantify -printhomedir`/quantify.h
 */


#ifndef QUANTIFY_THREADS_H
#define QUANTIFY_THREADS_H


#if defined (c_plusplus) || defined (__cplusplus)
extern "C" {
#endif
 
/*
 * Provide prototypes for ANSI C and C++, but skip them for KR C.
 */
#ifndef PROTO
#if defined (c_plusplus) || defined (__cplusplus) || defined (__STDC__)
#define PROTO(a)    a
#else
#define PROTO(a)    ()
#endif
#endif


/*
 * Take advantage of GNU gcc/g++ printf format checking.
 * These macros may be appended to printf-like functions to invoke checking.
 */
#ifndef PRINTF_FORMAT
#if (__GNUC__ == 2 && ! defined(__cplusplus))
/* fmt is no (starting at 1) of format string, arg1 is no of first arg */
#define     PRINTF_FORMAT(fmt, arg1)                \
  __attribute__ ((format(printf, fmt, arg1)))
#else
#define     PRINTF_FORMAT(fmt, arg1)
#endif
#endif /* ! PRINTF_FORMAT */


#if 0	/* get around emacs c-mode problem */
}
#endif






/*
 * The following function disables collection of all data by Quantify
 * for the current thread.
 * 
 * CAUTION: Once you call this function, you cannot re-enable data
 * collection for this thread.  No data is recorded and no data is
 * transmitted. The program is modified to incur the minimum overhead
 * when disabled.
 *
 * This function always returns TRUE (1).
 */
int quantify_thread_disable_recording_data PROTO((void));



/*
 * The following functions tell Quantify to start and stop recording all 
 * program performance data for the current thread.  By default, a
 * Quantify'd program records data for all threads automatically.  
 * 
 * You can turn off system call timing and register window trap timing
 * for the current thread separately using the other functions below.
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 * 
 * See the descriptions of quantify_start/stop_recording_data() 
 * in quantify.h for more information.
 */
int quantify_thread_start_recording_data PROTO((void));
int quantify_thread_stop_recording_data PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording data
 * for the current thread by calling the following predicate function.
 * The function returns TRUE (1) if Quantify is currently recording 
 * program performance data, and FALSE (0) if it is not.
 */
int quantify_thread_is_recording_data PROTO((void));



/*
 * Normally, Quantify times operating system calls that your program 
 * makes using a real-time clock.  This allows Quantify to report, 
 * for example, how much time your program spent reading or writing files.
 * 
 * The following functions tell Quantify to start and stop recording 
 * operating system call performance data for the current thread.
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 * 
 * See the descriptions of quantify_start/stop_recording_system_calls() 
 * in quantify.h for more information.
 */
int quantify_thread_start_recording_system_calls PROTO((void));
int quantify_thread_stop_recording_system_calls PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording system
 * calls for the current thread by calling the following predicate function.
 * The function returns TRUE (1) if Quantify is currently recording system
 * calls for the current thread, and FALSE (0) if it is not.
 */
int quantify_thread_is_recording_system_calls PROTO((void));



/*
 * The SPARC processor contains a set of "register windows" which can
 * speed up function calls in some instances.  The set of register
 * windows acts as a cache to memory.  However, when your function
 * calling depth exceeds the number of register windows available (7 or 8
 * function-calls depending on the processor chip), your program takes a
 * performance hit when the operating system is called to make a new
 * register window available.  Quantify records data on how many register
 * window traps your program causes, and accounts for the performance 
 * degradation that they cause.
 * 
 * The following functions tell Quantify to start and stop recording
 * register window trap performance data for the current thread.
 *
 * By default, a Quantify'd program does not record register window traps. 
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 * 
 * See the descriptions of quantify_start_recording_register_window_traps()
 * and quantify_stop_recording_register_window_traps() in quantify.h for
 * more information.
 */
int quantify_thread_start_recording_register_window_traps PROTO((void));
int quantify_thread_stop_recording_register_window_traps PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording register
 * window traps for the current thread by calling the following predicate
 * function.  The function returns TRUE (1) if Quantify is currently
 * recording register window traps, and FALSE (0) if it is not.
 *
 * If you are running on a platform which does not have register windows
 * (e.g., HP workstations) this function will return FALSE (0).
 */
int quantify_thread_is_recording_register_window_traps PROTO((void));



/*
 * If your program is dynamically linked (uses shared libraries), the
 * system's dynamic linker runs at program startup time to bind the
 * shared libraries to your program.  You can also call a function to
 * load a shared library while your program is running (dlopen on SunOS4
 * and Solaris 2.x, and shl_load on HPUX).
 * 
 * Quantify records the elapsed time taken by the dynamic linker at
 * both of these times, and distributes the times normally.
 * 
 * The following functions tell Quantify to start and stop recording
 * dynamic library data for the current thread.  It can be useful, for
 * example, to disregard the initial startup cost of very short-running
 * dynamically linked programs, because the time spent linking can
 * overwhelm the time actually running the program.  Cf. a dynamically
 * linked "hello world" program. 
 * 
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 * 
 * See the descriptions of quantify_start/stop_recording_dynamic_library_data() 
 * in quantify.h for more information.
 */
int quantify_thread_start_recording_dynamic_library_data PROTO((void));
int quantify_thread_stop_recording_dynamic_library_data PROTO((void));

/* 
 * 
 * You can determine whether your Quantify'd program is recording dynamic
 * library data for the current thread by calling the following predicate
 * function.  The function returns TRUE (1) if Quantify is currently recording
 * dynamic library data, and FALSE (0) if it is not.
 */
int quantify_thread_is_recording_dynamic_library_data PROTO((void));



/*
 * The following function tells Quantify to clear all the data it has
 * recorded about the current thread's performance to this point.  You
 * can use this function, for example, to ignore the performance of the
 * startup phase of your program.
 * 
 * This function always returns TRUE (1).
 */
int quantify_thread_clear_data PROTO((void));



/* 
 * The following functions tell Quantify to save all the data recorded
 * since program start (or the last call to quantify_thread_clear_data())
 * into a series of datafiles, one for each thread.
 * 
 * You can use these functions to save data snapshots about what your
 * program is doing at various times.
 * 
 * The data can be viewed later with the "qv" program as follows:
 * 
 * % qv {qv_options} {datafile_name}
 * 
 * 
 * Function quantify_thread_save_data() saves your accumulated
 * performance data to a group of files using the default (or user-set) 
 * filename prefix.  See the text below about changing the form of the
 * filename.
 *
 * With the default prefix, each call to quantify_thread_save_data() will
 * use a new set of datafile names.
 *
 * The first call will save files of the form a.out.12345.0.composite.qv,
 * a.out.12345.0.stk_0.qv, a.out.12345.0.stk_1.qv, a.out.12345.0.stk_2.qv,
 * etc.  Each time you call quantify_thread_save_data() the dataset number
 * (the .0. above) will increment.
 * 
 * 
 * The function quantify_thread_save_data_to_file() allows you to specify
 * a filename prefix for the current datafile.  This filename overrides
 * any prefix set by the -filename-prefix option.  
 * 
 * When saving thread data, if you do not use a %T directive anywhere
 * within the filename, Quantify will append one for you so that the
 * threads are guaranteed to be written to separate files.
 * 
 * See quantify.h for more information on file naming.
 * 
 * NOTE: These functions call quantify_clear_data() after saving
 * the data.
 * 
 * These functions return TRUE (1) if successful, and FALSE (0)
 * otherwise.
 */
int quantify_thread_save_data PROTO((void));
int quantify_thread_save_data_to_file PROTO((char *filename));



/*
 * The following function tells Quantify to save the argument string in
 * the NEXT output datafile written by quantify_save_data() or
 * quantify_thread_save_data() (the datafile corresponding to the current
 * part of the program's run, for the current thread).  These annotations
 * can be viewed later using the "qv" program.  
 * 
 * If only composite data is saved, the annotations for all threads will
 * be saved in the composite qv file.
 *
 * If thread groups are saved into "stack" qv files, the annotations for
 * each thread will be saved in the qv file corresponding to its stack.
 *
 * The function is typically used to mark datafiles with important
 * information about how the data was recorded (e.g., what the program
 * arguments were, who ran the program, or what datafiles were used).
 * 
 * This function returns the length of the string, or 0 if it is passed a
 * NULL pointer.
 */
int quantify_thread_add_annotation PROTO((char *str));


#if defined (c_plusplus) || defined (__cplusplus)
}
#endif

#endif /* QUANTIFY_THREADS_H */

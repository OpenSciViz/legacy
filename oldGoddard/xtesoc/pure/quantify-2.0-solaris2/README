Release notes for Quantify 2.0 Solaris 2

Changes from Quantify 1
=======================

  - The data collection performance for code
    compiled -g has been significantly improved.

  - Annotated source is supported for -g code
    linked in shared libraries and files linked
    'ld -r'.

  - The program summary reports time that was
    excluded from the dataset.  Time is typically
    excluded as the result of using options such
    as -record-register-window-traps or
    -avoid-recording-system-calls.  You can
    control this report using the new option
    -report-excluded-time. 

  - The application executable is no longer
    required to review a saved data file using qv.
    The binary data file is self-contained and can
    be saved and reviewed at any time.  Note that
    binary data files, although sometimes large,
    are highly compressible.

  - The ease-of-use of qv has been significantly
    improved.  Many of these changes were based on
    customer feedback.  For example, you can find
    displayed functions by name under each data
    analysis window.  Some of the major
    improvements are listed below.  See the
    "Analyzing Data Interactively" chapter in the
    Quantify User's Guide.

  - The call graph window supports changing the
    focus to arbitrary functions in the call
    graph.  The subtree below this new focus is
    shown and the line thickness and annotated
    data are scaled properly based on the the new
    focus function's function+descendant time.
    See "Focusing on Subtrees" in the Quantify
    User's Guide. This has replaced the "Indicate
    Subtree" functionality in the call graph.

  - A right-mouse-activated popup menu is
    available for each function in the call graph.
    It supports locating callers and descendants,
    expanding and collapsing the subtree below
    that function, and changing the call graph
    focus.  This has replaced the "Prune" slider
    in the call graph.

  - You can change the look-and-feel of qv by
    modifying X resource values.  Quantify loads
    any X resources specified in the .qvrc file in
    your home directory.  If there is no .qvrc
    file, Quantify will write an example copy when
    you exit qv.  You may edit the values in this
    file.  See "Changing the Look-and-Feel of qv"
    in the Quantify User's Guide.  Comments in the
    .qvrc file describe what aspects of qv can be
    customized. 

  - Quantify generates improved unique function
    names for each static functions and functions
    with no name in the symbol table.  See
    "Ensuring Unique Function Names" in the
    Quantify User's Guide.

Supported systems
=================

  Operating system and Hardware
  -----------------------------

    Quantify has been tested with Solaris
    versions 2.3, and 2.4 on SPARC platforms.
    Solaris 2.2 is not supported.
    
  Compilers
  ---------

    Quantify supports the following compilers:
    - SPARCWorks C and C++ versions:
         2.x and 3.x
    - GNU gcc and g++ versions:
         2.5.6, 2.5.8, and 2.6.0
    - CenterLine C (clcc) versions:
         1.x

  Threads
  -------

    Quantify supports these threads packages:
    - The native Solaris libthread library.
    - The Solaris Pthreads library, libpthread.
    - Transarc DCE threads.  

Restrictions and Known Issues
=============================

  General
  -------

  - Because of operating system differences,
    programs instrumented on one version of
    Solaris may crash or generate incorrect
    results if run on a different version of the
    operating system.

  - Quantify does not support use of the
    LD_PRELOAD environment variable.

  Data Collection
  ---------------

   - Calls to dlclose() that would cause the
     library to be unmapped can cause qv to
     incorrectly attribute data to the improper
     functions.  Quantify intercepts calls to
     dlclose() and prevents libraries from being
     unmapped.



  User Interface
  --------------

   - Under most window managers, if a non-explicit
     focus policy is used, qv can steal the focus
     from a non-qv window.  Click on the window
     title to reestablish proper focus.

   - Under twm and tvtwm, Quantify dialogs can be
     iconified. Once iconified, however, they
     cannot be de-iconified and qv will no longer
     respond. You should avoid iconifying dialogs
     under these window managers.

   - Under olvwm, dialogs are sometimes placed on
     the wrong virtual screen unless qv is running
     on the main virtual screen.  This is fixed in
     the latest version of olvwm.

  - A bug in the libX library shipped with
    OpenWindows 5.3 causes X client programs to
    either hang or crash when the display refuses
    a connection.  This problem is especially
    notable when displaying to HP-UX displays.
    This bug effects all X programs (for example,
    xterm) and is not a Quantify bug.

    To workaround this problem, ensure that
    display permission for the client machine has
    been granted on the X server machine, e.g.,
       % xhost + machineName

  Compilers
  ---------


  - FORTRAN routines with multiple entry points
    are not supported when compiled -g.

  - The GNU gcc extensions are not tested against
    Quantify.  Most gcc extensions will
    probably work fine.  Known limitations at
    present include problems with nested functions
    (e.g.: making a pointer to a nested function
    and attempting to call through it will not
    work).

  Threads
  -------

  - Customers using unsupported threads packages
    should contact Pure Software technical support
    (support@pure.com) to ensure compatibility.




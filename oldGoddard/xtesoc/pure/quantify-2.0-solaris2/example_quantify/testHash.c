/****************************************************************************
 *
 *       testHash.c
 *
 *         This file is a testing harness for the hash module. To use this
 *         module we must include "hash.h", and link with hash.o.
 *
 ****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

#ifndef TRUE
#define TRUE	1
#define FALSE	0
#endif

#include "config.h"
#include "hash.h"

typedef char *testEntry;
/* Maximum size of a key */
#define KEYSIZE 100

/* fillTestTable
 *   Function to read a file of table_size strings and fill our test table. 
 *   The test table contains all of the values and keys
 *   which we will use in testing the hash module. Keys will
 *   be the strings in the table, and values will be pointers to the entry
 *   in the array. We do this to make sure keys and values are different
 *   which is more likely to be the case in real use.
 */
static testEntry *fillTestTable(filename, table_size)
     char* filename;
     int table_size;
{
  FILE *fp = fopen(filename,"r");
  testEntry *testTable;
  int index;
  char buf[KEYSIZE], *retval, *str;
  if (fp == NULL) {
      perror("testHash encountered an error opening the test file");
      exit(1);
  }
  testTable = (testEntry *)malloc(table_size * sizeof(testEntry));

  for (index = 0; index < table_size; index++) {
    testTable[index] = "unsupplied";
    if ((retval = fgets(buf, KEYSIZE, fp)) != NULL) {
	str = (char*)malloc((strlen(buf)+1)*sizeof(char));
	strcpy(str,buf);
	testTable[index] = str;
    }

    if (ferror(fp)) {
	fclose(fp);
	perror("testHash encounted an error while reading the test file");
	exit(1);
    }
  }
  return testTable;
}

/* reportError
 *   Just prints out the error, gives us a function to break on for errors.
 */
static void reportError(msg)
     char* msg;
{
  printf("Error: %s", msg);
}

/* testMakeHashTable
 */
static hashtable* testMakeHashTable()
{
  hashtable* ht;
  ht = makeHashTable();
  if (!ht) {
    reportError("should be creating a hashtable, returned NULL.\n");
  }
  return(ht);
}

/* testPutHash
 *   Put entries from lower to upper from the testTable into the hashtable.
 *   Check the values that get returned, and signal error if they are
 *   incorrect. If oldValue is true then we should expect an old value to
 *   be in the table before we insert one, so we should get one back.
 *   Returns TRUE if the tests succeed.
 */
static int testPutHash(ht, testTable, lower, upper, oldValue)
     hashtable* ht;
     testEntry* testTable;
     int lower;
     int upper;
     boolean oldValue;
{
  int index;
  int tests_succeeded = TRUE; /* assume tests succeed */
  void* value;
  for (index = lower; index < upper; index++) {
    value = putHash(ht, testTable[index], &testTable[index]);
    if (value) {
      if (!oldValue) {
	reportError("shouldn't be overwriting values.\n");
	printf("       Overwrote %d with %d.\n", 
	       (char**)value - testTable, index);
	tests_succeeded = FALSE;
      }
    }
    else {
      if (oldValue) {
	reportError("should be overwriting values.\n");
	printf("       Did not find old value for %d.\n", index);
	tests_succeeded = FALSE;
      }
    }
  }
  return tests_succeeded;
}

/* testGetHash
 *   Get entries from lower to upper from the hashtable.
 *   Check the values that get returned, and signal error if they are
 *   incorrect. If oldValue is true then we should expect to get a
 *   value back.
 *   Returns TRUE if the tests succeed.
 */
static int testGetHash(ht, testTable, lower, upper, oldValue)
     hashtable* ht;
     testEntry* testTable;
     int lower;
     int upper;
     boolean oldValue;
{
  int index;
  int tests_succeeded = TRUE;
  void* value;
  char key[KEYSIZE];
  for (index = lower; index < upper; index++) {
    sprintf(key, "%s", testTable[index]);
    value = getHash(ht, key);
    if (value) {
      if (oldValue) {
	if (index != (char**)value - testTable) {
	  reportError("found the wrong value.\n");
	  printf("       Found %d, and should have found %d.\n",
		 (char**)value - testTable, index);
	  tests_succeeded = FALSE;
	}
      }
      else {
	reportError("should not have found a value.\n");
	printf("       Found %d, when it should have been empty.\n", index);
      }
    }
    else {
      if (oldValue) {
	reportError("could not find value.\n");
	printf("       Should have found value %d.\n", index);
	tests_succeeded = FALSE;
      }
    }
  }
  return tests_succeeded;
}

/* testRemHash
 *   Remove entries from lower to upper from the hashtable.
 *   Check the values that get returned, and signal error if they are
 *   incorrect. If oldValue is true then we should expect an old value to
 *   be in the table when we remove it, so we should get one back.
 */
static int testRemHash(ht, testTable, lower, upper, oldValue)
     hashtable* ht;
     testEntry* testTable;
     int lower;
     int upper;
     boolean oldValue;
{
  int index;
  int tests_succeeded = TRUE;
  void* value;
  char key[KEYSIZE];
  for (index = lower; index < upper; index++) {
    sprintf(key, "%s", testTable[index]);
    value = remHash(ht, key);
    if (value) {
      if (oldValue) {
	if (index != (char**)value - testTable) {
	  reportError("removed the wrong value.\n");
	  printf("       removed %d, and should have removed %d.\n",
		 (char**)value - testTable, index);
	  tests_succeeded = FALSE;
	}
      }
      else {
	reportError("should not have removed a value.\n");
	printf("       removed %d, when it should have been empty.\n", index);
	tests_succeeded = FALSE;
      }
    }
    else {
      if (oldValue) {
	reportError("did not remove value.\n");
	printf("       Should have removed value %d.\n", index);
	tests_succeeded = FALSE;
      }
    }
  }
  return tests_succeeded;
}

/* testDelHashTable
 *   Don't actually do any real checking here, but we will at least know
 *   where it is dying if it does.
 */
static int testDelHashTable(ht)
     hashtable* ht;
{
  delHashTable(ht);
  return TRUE;
}

#include "quantify.h"
/* testHashTable
 *   Create a hashtable and do a bunch of operations on it checking that
 *   they are done correctly, and then delete the hashtable. Each operation
 *   tests whether the hashtable values should be present or not.
 */
static int testHashTable(testTable, table_size)
     testEntry *testTable;
     int table_size;
{
  int tests_succeeded = TRUE;
  /* Make the hashtable */
  hashtable* ht = testMakeHashTable();
  if (ht == NULL)
    return FALSE;
  /* Put them all in the hashtable and ensure they were not there before */
  tests_succeeded &= testPutHash(ht, testTable,  0, table_size, FALSE);
  /* Now put them in again and make sure they were already there */
  tests_succeeded &= testGetHash(ht, testTable,  0, table_size, TRUE);
  /* Lose the first half of them and make sure there was something deleted */
  tests_succeeded &= testRemHash(ht, testTable,  0,  table_size/2, TRUE);
  /* Try to remove them again and make sure there was nothing to remove */
  tests_succeeded &= testRemHash(ht, testTable,  0,  table_size/2, FALSE);
  /* Try to find the first set again and ensure no one is there */
  tests_succeeded &= testGetHash(ht, testTable,  0,  table_size/2, FALSE);
  /* But make sure the rest are still there */
  tests_succeeded &= testGetHash(ht, testTable, table_size/2, table_size, TRUE);
  /* Put the first half back in again and make sure they are all new */
  tests_succeeded &= testPutHash(ht, testTable,  0,  table_size/2, FALSE);

  /* Try to put the last half in again and ensure they are already there */
#ifdef TIME_SINGLE_FUNCTION_CALL_DATA
  quantify_clear_data();			/* drop any data so far */
  quantify_stop_recording_system_calls();	/* don't care about any calls to OS */
  tests_succeeded &= testPutHash(ht, testTable, table_size/2, table_size, TRUE);
  /* ALT quantify_save_data(); */
  quantify_stop_recording_data();		/* don't get any more data */
#else
  tests_succeeded &= testPutHash(ht, testTable, table_size/2, table_size, TRUE);
#endif

  /* Delete the table */
  tests_succeeded &= testDelHashTable(ht);

  return tests_succeeded;
}
/* main
 *   Read a dataset of strings and then test the hashtable code
 *   against that distribution of strings.
 */
int main(argc, argv)
     int argc;
     char **argv;
{
  if (argc > 2) {
      int table_size       = atoi(argv[1]);
      char *filename	   = argv[2];
      testEntry* testTable = fillTestTable(filename, table_size);

      if (argc > 3) {
	  hashtable_backbone_size = atoi(argv[3]);
      }
      printf("Testing the first %d entries from %s with a hashtable of size %d.\n",
	     table_size, filename, hashtable_backbone_size);

      if (testHashTable(testTable, table_size)) {
	  printf("All tests passed.\n");
      } 
  } else {
      reportError("Usage: testHash  <n> <filename> [<backbone_size>]\n");
      exit(1);
  }
  exit(0);
}  

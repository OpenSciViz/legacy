.TH Quantify 1 "15 January 1995" "Pure Software Inc." "LICENSED SOFTWARE"
.\"
.\" MANPAGE for QUANTIFY
.\"
.\"
.\"
.SH NAME
quantify \- tool for analyzing performance bottlenecks
.SH SYNOPSIS
.br
.B quantify 
[
.I \-options
]
$(CC) $(CFLAGS) 
[
\-o program
] file.o ... -llibrary...

.\"
.SH DESCRIPTION
.\"

Quantify records the time spent in the functions of a program by inserting
counting instructions at function and basic-block entry points.  Quantify
records data for all functions called by the program, even if they occur in
shared libraries or third-party libraries, with or without source code.  In
addition, Quantify times individual operating system calls made by library
functions.  Quantify accurately distributes accumulated time from a function
to its callers and records minimum and maximum function times.

All data is recorded in terms of machine instruction cycles and converted to
elapsed times according to the clock rate of the machine.  The collected data
reflect the cost of the original program's instructions and automatically
exclude any Quantify counting overhead.  Several options and a programmatic
API permit collecting different types of timing data for all or a portion of
the program's execution.

Data collected can be stored in binary data files that can be analyzed using
qv (1), Quantify's graphical display and support tool.  The data may also be
exported to a text data file for non-interactive analysis.  Options permit
Quantify'd programs to execute arbitrary shell scripts whenever data is saved
or the program exits, permitting automated performance analysis (see the
options -run-at-save and -run-at-exit).

For source files compiled with debugging information (-g), Quantify can record
the number of instruction cycles executed on either a basic-block or line
basis and display this information in an annotated copy of the source file.
By default Quantify records data on a line-by-line basis.

.SS Building
Add the word \fBquantify\fP to the beginning of the link line
in your makefile, and relink the program.  For example:
.nf
    prog:     $(OBJS)
            \fBquantify\fP \fI-options\fP $(CC) $(CFLAGS) -o prog $(OBJS) $(LIBS)
.fi
.LP
Quantify modifies a \fIcopy\fP of the object code at link time to produce an
equivalent program that has additional profiling instructions
built in. The \fI-options\fP specified on the command line are Quantify
options that customize its operation (see below).
.SS Running
The Quantify'd program is run exactly like the original program. 
Timing data are collected in memory and transmitted to the analysis program.  If
the data are collected non-interactively, it is written to a binary data file
at program exit. Any Quantify reports and messages are directed to the
standard error output, which can be redirected.
.LP
.SS Inspecting the data
Upon exit from your program, the collected data are available for inspection in
qv. The two main windows are the Function List window, which permits you
to sort the called functions according to different criteria, and the Call
Graph window, which permits you to inspect all or a portion of the dynamic
call graph.  This graph displays all the functions that were called
during the run and their callers.
Additional windows are available, showing the detailed data for an
individual function, and, for suitably compiled programs, annotated source
code reflecting data collected on a basic-block or line-basis.  
All reports, including a Postscript version of the call graph, 
may be written to files on demand.
.LP
.SS Collecting different data
Quantify has a simple but powerful API that allows you to collect different
types of data during different phases of your program operation.  To use the
API, simply place breakpoints in your Quantify'd program at the beginning and
end of the phase you want to time.  Once your program has reached these
breakpoints you can call any of the API functions described below.  All the
functions are available for both programmatic use and from your debugger.
.LP
.SS Machine descriptions
Quantify analyzes your program's instructions based 
on a particular machine configuration, including the processor implementation. 
Different instructions require different numbers of cycles to complete 
depending upon the implementation of the machine's CPU.
Quantify's .machine files contain descriptions of many 
SPARC-based
machines.  
See the platform-specific .machine file itself for more information.
Please contact support@pure.com to add new machine descriptions.

.SH LICENSE ADVISOR
Quantify and qv are licensed for a certain \fInumber\fP of users.  
When that number of users is exceeded, the user is warned but the Quantify'd 
program continues to execute and collect data.

.SH OPTIONS
.LP
Quantify's operation can be customized for your program. By specifying
options either at program build-time or program run-time, you can affect
which data Quantify collects, what information the reports contain, how
the data look, and where the data are stored. Other options permit you to
control how the program is Quantify'd and linked.
.LP
Each option is a word or phrase that begins with a hyphen, for example,
-run-at-exit. All options have default values, shown below. You
may override these values using the assignment syntax:
.nf

    -option-name[=[value]]
.fi
.LP
Note that no spaces are permitted on either side of the equal sign (=) if
it is supplied.
.LP
Quantify looks for options either on the command line used
to build your Quantify'd program, or from the environment variables
QUANTIFYOPTIONS or PUREOPTIONS. For more information on option processing, see
\fIControlling Quantify\fP in your Quantify User's Guide.
.if n .ig IG
.\" This is the TROFF VERSION.
.de T2
.ne 1i
.nf
\fB\s-1\\$1\s0\fP\h'|3.5i'\fB\s-1\\$2\s0\fP\h'|5i'\fB\s-1\\$3\s0\fP
.fi
..
.IG
.if t .ig IG
.\" This is the NROFF VERSION.
.de T2
.ne 1i
.nf
\h'|-0.25i'\fB\s-1\\$1\s0\fP\h'|3.1i'\fB\s-1\\$2\s0\fP\h'|4.5i'\fB\s-1\\$3\s0\fP
.fi
..
.IG
\" for directives  $1 == directive, $2 == text
.if n .ig IG
.\" This is the TROFF VERSION.
.de T3
.ne 1i
.nf
\h'|0.25i'\fB\s-1\\$1\s0\fP\h'|1.0i'\s-1\\$2\s0
.fi
..
.IG
.if t .ig IG
.\" This is the NROFF VERSION.
.de T3
.ne 1i
.nf
\h'|0.25i'\fB\s-1\\$1\s0\fP\h'|1.0i'\s-1\\$2\s0
.fi
..
.IG
.LP
.T2 "OPTION" "TYPE" "DEFAULT"
.LP
.SS Build-time Options
\&
.T2 "-cache-dir" "directory" "\fIquantifyhome\fR\fB/cache"
.LP
This option sets the global directory where Quantify'd versions of libraries 
will be cached.
.LP
.T2 "-always-use-cache-dir" "boolean" "no"
.LP
This option forces all Quantify'd versions of libraries to be written to the 
global cache directory.
.LP
.T2 "-ignore-runtime-environment" "boolean" "no"
.LP
This option prevents the run-time Quantify environment
from overriding the option values with which the program was built. This is useful if
you are building a Quantify'd program for someone else to run, and you
want to make sure that the options and suppressions you specify are
in effect at run-time.
.LP
.T2 "-use-machine" "string" "\fIcomputed\fP"
.LP
This option specifies which machine description (in the .machine file) 
to use when determining expected instruction cycle.  By default,
Quantify attempts to determine the machine according to its 
hostid (2)
information. If this fails, Quantify prints a warning that indicates which
default machine will be used.
.LP
.T2 "-collection-granularity" "string" "line"
.LP
This option specifies the data collection granularity to use when Quantify'ing files compiled with debugger information (-g). 
If not explicitly stated, Quantify instruments each line with a separate counter.  The other option values are \fBbasic-block\fP, 
which instruments only the lines associated with basic blocks, or \fBfunction\fP, which instruments at the function-level only.  
Quantify always instruments files compiled without debugging information with granularity \fBfunction\fP.  Line granularity
provides more detail than basic-block granularity in an annotated source display, at the cost of slower data collection times.
Similarly, basic-block granularity provides more detail than function granularity, again, at the cost of slower data collection times.  
Use the -collection-granularity to trade off information detail against data collection speed.
.LP
.T2 "-g++" "boolean" "no"
.LP
This option specifies that the g++ compiler is being used. This option
additionally tells the linker to invoke the appropriate collect
program (specified using the -collector option).
.LP
.T2 "-linker" "program-name" "/bin/ld"
.LP
This option specifies the name of the linker that Quantify should invoke to 
produce the executable. 
.LP
.T2 "-collector" "program-name" ""
.LP
This option specifies the name of the collect program to be used to sequence 
and collect static constructors in `C++' code. If you use g++, you must
specify both the -g++=yes option and the name of the collect program used by
the g++ compiler. You can find the name of the collect program using the
g++ compiler with the command:
.LP
.nf
    % g++ -v foo.c
.fi
.LP
If this collect program is /usr/lib/gcc-ld use:
.LP
.nf
    % quantify -g++=yes -collector=/usr/lib/gcc-ld g++ foo.c
.fi
.LP
.T2 "-version" "" ""
.LP
This option prints Quantify's version.
.LP
.T2 "-print-home-dir" "" ""
.LP
This option prints Quantify's installation directory.
.LP

.SS Run-time Options
\&
.T2 "-logfile" "file-name" "stderr"
.LP
This option specifies the name of the logfile for any Quantify output. Normally,
Quantify routes its output to stderr. The filename may include one or more of
these special directives: %p is replaced by the process id, %v is replaced by
program-name, and %V is replaced by the full pathname of program-name 
with underscores replacing slashes. The
keywords are useful when you fork many Quantify'd programs and want
separate logfiles for each one.
.LP
.T2 "-append-logfile" "boolean" "no"
.LP
This option enables Quantify output to be appended to the current logfile rather than
replacing it.
.LP
.T2 "-copy-fd-output-to-logfile" "fd-list" ""
.LP
This option enables output from other sources to be appended to the logfile.
This option is specified as a comma-delimited list of file descriptors. Output
written to these file descriptors is copied into the logfile and helps you
reconstruct what the user did. By default, no output is copied; to copy output
written to `stdout' and `stderr' into the logfile, interspersed with Quantify
output, use:
.nf

     % quantify -copy-fd-output-to-logfile=1,2 cc foo.c
.fi
.T2 "-auto-mount-prefix" "directory" "/tmp_mnt"
.LP
This option specifies the directory prefix used by the file system
auto-mounter (usually /tmp_mnt) to mount remote file systems in
NFS environments. Use this option to strip the prefix, if present, to
improve the readability of source filenames in Quantify reports.
.LP
.T2 "-fds" "integer" "26"
.LP
This option changes the default set of file descriptors used by Quantify in case
they clash with the ones used by the user program. To use file descriptors
57 and 58 instead of the default 26 and 27 use:
.LP
.nf   
    % setenv QUANTIFYOPTIONS -fds=57
.fi
.LP
.T2 "-program-name" "program-name" "argv[0]"
.LP
This option specifies the full pathname of the Quantify'd program if argv[0] 
does not name the executable file itself. This can occur if you change argv[0]
in your program. For example, many programs that are invoked by a script
rename their argv[0] to the name of the script. 
.LP
.T2 "-user-path" "dir list" ""
.LP
This option specifies a colon-delimited list of directories that Quantify should
search to find source files or programs at run-time. Normally, the program can be found
by looking at argv[0] and your $PATH, but it is possible to exec a program
not on your path. If your program is forked by another and Quantify cannot
find it, set this option to the directory
where your program is stored. This option takes precedence over $PATH. Use
this option if qv reports that it cannot find a source file and it resides in
a different directory.
.LP
.T2 "-run-at-exit" "string" ""
.LP
This option specifies a string to pass to sh (1) 
when the program exits.  You may use double-quotes around a string containing spaces.
.LP
.T2 "-run-at-save" "string" ""
.LP
This option specifies a string to pass to sh (1) 
when the quantify_save_data() API function is called and
when the data is saved at program exit.  Both -run-at-save and -run-at-exit
scripts can refer to the following filename directives
(in addition to the directives listed under the -filename-prefix options below):
.LP
.nf
.T3 "%b" "The binary file name"
.T3 "%s" "A /tmp file containing the program summary"
.T3 "%x" "A /tmp file containing the export data"
.fi
.LP
.LP
.T2 "-filename-prefix" "string" "%v.%p.%n"
.LP
This option specifies the prefix to use when forming default data file names.
Filename extensions (e.g., "qv" for the binary data file) are added to the
expanded prefix to form a full pathname. There are several directives
available to use in the filename prefix:
.LP
.nf
.T3 "%V" "The full pathname of the program, with underscores replacing slashes"
.T3 "%v" "The name of the program"
.T3 "%p" "The process id (pid)"
.T3 "%n" "The sequence number for the data set, starting at 0"
.T3 "%d" "The current date (mmddyy)"
.T3 "%t" "The current time (hh:mm:ss)"
.T3 "%T" "Thread id."
.fi
.LP
.LP
.T2 "-write-export-file" "filename" "none"
.LP
This option specifies the name of the file used to write the export data file
for a dataset.  By default, the value "none" specifies that no export file is
written.  If no value is specified using "-write-export-file=", the export
file is written to the default export data filename based on the value of the
-filename-prefix option.  Otherwise the string value is used to form the
export file name by appending the ".qx" extension to the value. 
.LP
.T2 "-write-summary-file" "filename" "/dev/tty"
.LP
This option specifies the name of the file used to write the program summary
for a dataset.  By default, the program summary is written to the current
output. Specifying the value "none" avoids writing the summary.  If no value
is specified using "-write-summary-file=", the summary file is written to a
filename based on the value of the "-filename-prefix" option.  Otherwise the
string value is used to form the summary file name by appending the ".qs"
extension to the value. 
.LP
.T2 "-windows" "boolean" "yes"
.LP
This option specifies whether to start qv in display mode to analyze the data.
.LP
.T2 "-record-data" "boolean" "yes"
.LP
This option specifies whether Quantify should immediately begin collecting data when the program is started. 
If the value is "no", you can start recording data using the API function quantify_start_recording_data(), described below.
.LP
.T2 "-record-child-process-data" "boolean" "no"
.LP
This option specifies whether Quantify should collect data 
in any child processes created using fork(2).
.LP
.T2 "-save-data-on-signals" "boolean" "yes"
.LP
This option specifies whether Quantify should save data when it receives a fatal signal.  
This option only applies if the program itself does not install a signal handler for 
the fatal signal.
.LP
.T2 "-record-system-calls" "boolean" "yes"
.LP
This option specifies whether Quantify should time system calls.
If the value is "no", you can start recording data using the API function 
quantify_start_recording_system_calls(), described below.
.LP
.T2 "-measure-timed-calls" "string" "elapsed-time"
.LP
This option specifies how Quantify should measure timed calls, 
such as system calls and dynamic library operations.  
If the value is \fIelapsed-time\fP, Quantify records the elapsed (wall-clock) time.  
You can also measure \fIuser+system\fP, \fIuser\fP, or \fIsystem\fP time.
.LP
.T2 "-avoid-recording-system-calls" "syscall list" "\fIsystem-dependent\fB"
.LP
This option lists the system calls to avoid timing.  
The value is a comma-delimited list of system call numbers or names.
See /usr/include/sys/syscall.h for possible system call numbers.
.LP
.T2 "-record-register-window-traps" "boolean" "no"
.LP
This option specifies whether Quantify should record register window traps.
If the value is "no", you can start recording data using the API function 
quantify_start_recording_register_window_traps(), described below.
See the Quantify User's Guide for a discussion of register window traps.  This
option applies only to SPARC architectures.
.LP
.T2 "-save-thread-data" "string" "composite"
.LP
This option specifies what data Quantify should save for a threaded program.  
The value is a comma-delimited list of types of data to save.
By default Quantify only saves the combined data from all threads.  In addition to \fIcomposite\fP 
you may specify \fIstack\fP, which will save data for each stack into a separate dataset.  These
datasets can be viewed with qv.
.LP
.T2 "-threads" "boolean" "no"
.LP
This option specifies whether the Quantify'd program is multi-threaded.
For programs that use the supported threads packages, this option is set automatically.
.LP
.T2 "-max-threads" "integer" "20"
.LP
This option specifies the number of threads expected during a run.
.LP
.T2 "-threads-stack-change" "integer" "0x1000"
.LP
This option specifies the separation, in bytes, between stacks for different threads.

.LP
.SH API
Quantify's application programming interface can be used by programs
as necessary and in interactive debugging environments like 
dbx and gdb.

.SS Data Collection API
\&
.T2 "quantify_is_running()"
.LP
Returns 1 if the program is Quantify'd.
.LP
.T2 "quantify_start_recording_data()"
.T2 "quantify_stop_recording_data()"
.T2 "quantify_is_recording_data()"
.LP
These functions start and stop recording function data.  
They also control recording system call 
and register window trap
data, if they are being recorded.
.LP
.T2 "quantify_start_recording_system_calls()"
.T2 "quantify_stop_recording_system_calls()"
.T2 "quantify_is_recording_system_calls()"
.LP
If function data is being recorded, these functions control whether system calls are timed, 
except those specified by the -avoid-recording-system-calls option.
.LP
.T2 "quantify_start_recording_system_call(system_call)"
.T2 "quantify_stop_recording_system_call(system_call)"
.T2 "quantify_is_recording_system_call(system_call)"
.LP
If function data is being recorded, these functions control whether a specific system call is timed.  
The argument is a string specifying the name or number of a system call.
.LP
.T2 "quantify_start_recording_register_window_traps()"
.T2 "quantify_stop_recording_register_window_traps()"
.T2 "quantify_is_recording_register_window_traps()"
.LP
If function data is being recorded, 
these functions control whether time spent handling register window traps is recorded.  
This option applies to SPARC processors only.
See the Quantify User's Guide for a discussion of register windows.
.LP
.T2 "quantify_print_recording_state()"
.LP
This function prints the current recording state.
.LP
.T2 "quantify_save_data()"
.T2 "quantify_save_data_to_file(char *filename)"
.LP
These functions write the current data to a file and then clear all data.
The binary data file can be analyzed subsequently using qv.
The quantify_save_data function writes data to the file 
specified by the -filename-prefix option.
The quantify_save_data_to_file function requires an explicit filename.
The %b directive in the -run-at-save and -run-at-exit options will 
be expanded into the binary data file name before it is passed to the shell. 
.LP
.T2 "quantify_clear_data()"
.LP
This function clears all accumulated function data.  
.LP
.T2 "quantify_add_annotation(char *annotation)"
.LP
This function adds the annotation to the next binary data file.  These annotations are
printed using the -print-annotations option of qv.
.LP
.T2 "quantify_help()"
.LP
This function prints a list of API functions.
.LP

.SH FILES
.IP "*_pure_q\fImg\fP_\fIv\fP*"
Cached object and library files translated by Quantify. 
The q\fImg\fP portion indicates the machine type 
and collection granularity of the Quantify'd file.
The \fIv\fP portion indicates the version number of Quantify.
.IP "\fIquantifyhome\fP/cache/...
Central cache directory structure for caching translated object files and
libraries when they cannot be written alongside the original files or when you
use the -always-use-cache-dir option.
.IP ".pure, .lock.*"
Locking files preventing simultaneous file access by more than one user.
.IP ".quantify.solaris2"
Specifies special build-time directives for Quantify.  At present, none are required.
.IP "\fIquantifyhome\fP/.machine.solaris2"
Contains a database of machines and, optionally, a directive for which machine to use 
when instrumenting a program.
.IP "\fIquantifyhome\fP/Quantify.purela"
Contains license related information.
.IP "\fIquantifyhome\fP/quantify.h"
Include file to use with the Quantify API.
.IP "\fIquantifyhome\fP/quantify_threads.h"
Include file to use with the Quantify API for threads.
.IP "\fIquantifyhome\fP/quantify_stubs.a"
Library to be included to provide dummy Quantify API function
definitions for use when not linking under Quantify.
.SH SEE ALSO
qv (1)
.SH CAVEATS
Compared with the un-Quantify'd program, the Quantify'd program typically runs
about 2 to 5 times slower and requires very little additional dynamic memory.
Occasionally the memory references required to update Quantify's accumulators
will cause the paging behavior to degrade performance.  In this case, running
the Quantify'd program on a machine with more memory would be advantageous.
.LP
Quantify coerces all calls to vfork (2) into calls to fork (2).
.LP
If Quantify does change the behavior of the program (except
as mentioned above), or produces incorrect data reports, please report these
bugs to the appropriate address:
.nf
.ta \w'MM'u +\w'MM'u +\w'support@europe.pure.com MM'u +0.5i
	Contact us in the US at:
		support@pure.com	or (408) 720 1600
	In Europe:
		support@europe.pure.com	or (+31) 2503 85420
	In Japan:
			   (03) 3863 9283
.LP

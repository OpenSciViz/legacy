/*
 * The Quantify API
 *
 * Copyright (c) 1992-1994 Pure Software.
 *
 * This file contains proprietary and confidential information and
 * remains the unpublished property of Pure Software. Use, disclosure,
 * or reproduction is prohibited except as permitted by express written
 * license agreement with Pure Software.
 *
 */

#ifndef QUANTIFY_H
#define QUANTIFY_H


#if defined (c_plusplus) || defined (__cplusplus)
extern "C" {
#endif
 
/*
 * Provide prototypes for ANSI C and C++, but skip them for KR C.
 */
#ifndef PROTO
#if defined (c_plusplus) || defined (__cplusplus) || defined (__STDC__)
#define PROTO(a)    a
#else
#define PROTO(a)    ()
#endif
#endif


/*
 * Take advantage of GNU gcc/g++ printf format checking.
 * These macros may be appended to printf-like functions to invoke checking.
 */
#ifndef PRINTF_FORMAT
#if (__GNUC__ == 2 && ! defined(__cplusplus))
/* fmt is no (starting at 1) of format string, arg1 is no of first arg */
#define     PRINTF_FORMAT(fmt, arg1)                \
  __attribute__ ((format(printf, fmt, arg1)))
#else
#define     PRINTF_FORMAT(fmt, arg1)
#endif
#endif /* ! PRINTF_FORMAT */


#if 0	/* get around emacs c-mode problem */
}
#endif

/*
 * The following function returns TRUE if Quantify is running.  
 * The quantify_stubs.a file contains a definition that returns FALSE 
 * if your program is not linked with Quantify.
 */
int quantify_is_running PROTO((void));



/*
 * The following function disables collection of all data by Quantify.  
 * CAUTION: Once you call this function, you cannot re-enable data
 * collection for this process.  No data is recorded and no data is
 * transmitted. The program is modified to incur the minimum overhead
 * when disabled.
 *
 * This function always returns TRUE (1).
 */
int quantify_disable_recording_data PROTO((void));



/*
 * The following functions tell Quantify to start and stop recording all 
 * program performance data.  By default, a Quantify'd program starts 
 * recording data automatically.  
 * 
 * You can turn off system call timing and register window trap timing
 * separately using the other functions below.
 *
 * Using option -record-data={yes|no} is like calling one of these
 * functions before your program begins executing.
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 */
int quantify_start_recording_data PROTO((void));
int quantify_stop_recording_data PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording data
 * by calling the following predicate function.  The function returns
 * TRUE (1) if Quantify is currently recording all program performance 
 * data, and FALSE (0) if it is not.
 */
int quantify_is_recording_data PROTO((void));



/*
 * Normally, Quantify times operating system calls that your program 
 * makes using a real-time clock.  This allows Quantify to report, 
 * for example, how much time your program spent reading or writing files.
 * 
 * The following functions tell Quantify to start and stop recording 
 * operating system call performance data.
 *
 * By default, a Quantify'd program times system calls. Using Quantify 
 * option -record-system-calls={yes|no} is like calling one of these
 * functions before your program begins executing.
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 */
int quantify_start_recording_system_calls PROTO((void));
int quantify_stop_recording_system_calls PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording system
 * calls by calling the following predicate function.  The function
 * returns TRUE (1) if Quantify is currently recording system calls,
 * and FALSE (0) if it is not.
 */
int quantify_is_recording_system_calls PROTO((void));



/*
 * Quantify maintains a set of individual system calls to avoid timing.
 * This is useful, for example, for X programs which wait for user input
 * with the select() function, which calls the SYS_select system call.
 * Normally, one does not want to see this "user wait time" mixed in with
 * the performance data.
 * 
 * The Quantify option -avoid-recording-system-calls lets you specify
 * a set of system calls to avoid.  SYS_select is among those system calls
 * that Quantify avoids timing by default.  See the Quantify manual for the 
 * complete list for your platform.
 *
 * The following functions let you start and stop timing individual or sets
 * of system calls while your program is running.  This could be useful,
 * for example, if you have an X program that also does database queries,
 * where the database query function that you call ultimately communicates
 * with the database engine using select().  You may want to have Quantify
 * time select() during database activity, but not during GUI activity, so
 * that the time Quantify records includes the delays from the database.
 * 
 * You can call these API functions from your code to accomplish this sort
 * of goal.
 * 
 * These functions take a system call number or a string of system call
 * numbers as an argument.  See the file /usr/include/sys/syscall.h for
 * a list of available system calls and their numbers.
 *
 * To turn on recording of SYS_select and SYS_poll (using SunOS4 system
 * call numbers in the example) you could use:
 *
 * quantify_start_recording_system_call(93);
 * quantify_start_recording_system_call(153);
 * 
 * or
 * 
 * quantify_start_recording_system_call("93,153");
 * 
 * or
 * 
 * quantify_start_recording_system_call("SYS_select,SYS_poll");
 * 
 *
 * Note that overall system call recording must be turned on to record
 * data for any individual system calls.  Overall system call recording
 * is controlled by the option -record-system-calls and the Quantify API
 * functions quantify_start/stop_recording_system_calls().
 * 
 * These functions return TRUE (1) if the arguments were correctly formed
 * and FALSE (0) otherwise.  
 */
int quantify_start_recording_system_call PROTO((char *));
int quantify_stop_recording_system_call PROTO((char *));

/* 
 * You can determine whether your Quantify'd program is recording a
 * particular system call by calling the following predicate function.
 * The function returns TRUE (1) if Quantify is currently recording
 * the specified system call, and FALSE (0) if it is not.
 *
 * This function takes as an argument a *single* system call number in
 * numerical or string form, like the functions above.
 *
 * Note that overall system call recording controls the recording of all
 * system calls en masse, but has no effect on the recording state of
 * individual system calls.  The controls are independent.  Overall
 * system call recording is controlled by the Quantify option
 * -record-system-calls and the Quantify API functions
 * quantify_start/stop_recording_system_calls()
 */
int quantify_is_recording_system_call PROTO((char *));



/*
 * The SPARC processor contains a set of "register windows" which can
 * speed up function calls in some instances.  The set of register
 * windows acts as a cache to memory.  However, when your function
 * calling depth exceeds the number of register windows available (7 or 8
 * function-calls depending on the processor chip), your program takes a
 * performance hit when the operating system is called to make a new
 * register window available.  Quantify records data on how many register
 * window traps your program causes, and accounts for the performance 
 * degradation that they cause.
 * 
 * The following functions tell Quantify to start and stop recording
 * register window trap performance data.
 *
 * By default, a Quantify'd program does not record register window traps. 
 *
 * Using option -record-register-window-traps={yes|no} is like calling
 * one of these functions before your program begins executing.
 *
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 */
int quantify_start_recording_register_window_traps PROTO((void));
int quantify_stop_recording_register_window_traps PROTO((void));

/* 
 * You can determine whether your Quantify'd program is recording register
 * window traps by calling the following predicate function.  The function
 * returns TRUE (1) if Quantify is currently recording register window traps,
 * and FALSE (0) if it is not.
 *
 * If you are running on a platform which does not have register windows
 * (e.g., HP workstations) this function will return FALSE (0).
 */
int quantify_is_recording_register_window_traps PROTO((void));



/*
 * If your program is dynamically linked (uses shared libraries), the
 * system's dynamic linker runs at program startup time to bind the
 * shared libraries to your program.  You can also call a function to
 * load a shared library while your program is running (dlopen on SunOS4
 * and Solaris 2.x, and shl_load on HPUX).
 * 
 * Quantify records the elapsed time taken by the dynamic linker at
 * both of these times, and distributes the times normally.
 * 
 * The following functions tell Quantify to start and stop recording
 * dynamic library data.  It can be useful, for example, to disregard the
 * initial startup cost of very short-running dynamically linked
 * programs, because the time spent linking can overwhelm the time
 * actually running the program.  Cf. a dynamically linked "hello world"
 * program. 
 * 
 * By default, a Quantify'd program times dynamic library data. Using
 * Quantify option -record-dynamic-library-data={yes|no} is like calling
 * one of these functions before your program begins executing.
 * 
 * The functions return TRUE (1) if they change the state of Quantify
 * data recording, and FALSE (0) otherwise.
 * 
 */
int quantify_start_recording_dynamic_library_data PROTO((void));
int quantify_stop_recording_dynamic_library_data PROTO((void));

/* 
 * 
 * You can determine whether your Quantify'd program is recording dynamic
 * library data by calling the following predicate function.  The
 * function returns TRUE (1) if Quantify is currently recording dynamic
 * library data, and FALSE (0) if it is not.
 * 
 */
int quantify_is_recording_dynamic_library_data PROTO((void));



/*
 * The following function tells Quantify to clear all the data it has
 * recorded about your program's performance to this point.  You can use this
 * function, for example, to ignore the performance of the startup phase of
 * your program.
 *
 * This function always returns TRUE (1).
 */
int quantify_clear_data PROTO((void));



/*
 * The following functions tell Quantify to save all the data recorded since
 * program start (or the last call to quantify_clear_data()) into a datafile
 * (a ".qv" file).
 *
 * You can use these functions to save data snapshots about what your program
 * is doing at various times.
 *
 * The data can be viewed later with program "qv" as follows:
 *
 * % qv {qv_options} {datafile_name}
 *
 *
 * Function quantify_save_data() saves your accumulated performance data to
 * a file using the default (or user-set) filename prefix.  See the text
 * below about changing the form of the filename.
 *
 * With the default prefix, each call to quantify_save_data() will use a new
 * datafile name (a.out.12345.0.qv, a.out.12345.1.qv, a.out.12345.2.qv, etc.)
 *
 *
 * The function quantify_save_data_to_file() allows you to specify a
 * filename prefix for the current datafile.  This filename overrides any
 * prefix set by the -filename-prefix option.
 * 
 * Calling quantify_save_data_to_file("foo") will save data in the file
 * "foo.qv".
 *
 * 
 * FILE NAMING ESOTERICA
 * By default, Quantify supplies an filename prefix of "%v.%p.%n".
 * With the default prefix, each call to quantify_save_data() will use a new
 * datafile name (a.out.12345.0.qv, a.out.12345.1.qv, a.out.12345.2.qv, etc.)
 * 
 * You can change the filename prefix with the "-filename-prefix" option.
 * This prefix will be used for every datafile saved with quantify_save_data().
 * 
 * There are several directives that you can use in the filename prefix
 * (either with the -filename-prefix option, or in the filename passed to
 * quantify_save_data_to_file()).  These can help you identify your data
 * files:
 * 
 *  - %p expands to the process id
 *  - %v expands to the simple program name (argv[0])
 *  - %V expands to full pathname of the program, with _ replacing /
 *  - %t expands to the current time (hh:mm:ss)
 *  - %d expands to the current date (yymmdd)
 *  - %n expands to a sequential number (0, then 1, then 2...) which
 *       is incremented each time you call the data-saving functions.
 *
 * NOTE: These functions call quantify_clear_data() after saving the data.
 * 
 * These functions return TRUE (1) if successful, and FALSE (0) otherwise.
 */
int quantify_save_data PROTO((void));
int quantify_save_data_to_file PROTO((char *filename));



/*
 * The following function tells Quantify to save the argument string in the
 * NEXT output datafile written by quantify_save_data() (the datafile
 * corresponding to the current part of the program's run).  These annotations
 * can be viewed later using the "qv" program.  The function is typically used
 * to mark datafiles with important information about how the data was
 * recorded (e.g., what the program arguments were, who ran the program, or
 * what datafiles were used).
 *
 * This function returns the length of the string, or 0 if it is passed a
 * NULL pointer.
 */
int quantify_add_annotation PROTO((char *str));



/*
 * Print out a message to stderr if Quantify is running (the stub function in 
 * the stubs library does nothing).  Useful, for example, to print out a
 * message saying that the program is Quantify'd.
 * 
 * This function always returns 0.
 */
int pure_printf PROTO((const char *fmt, ...))	    PRINTF_FORMAT(1, 2);

/*
 * Like the above, but precede the message with a Quantify banner.
 * 
 * This function always returns 0.
 */
int pure_printf_with_banner PROTO((const char *fmt, ...)) PRINTF_FORMAT(1, 2);

/*
 * Print a list of Quantify API functions.
 * 
 * This function always returns 1.
 */
int quantify_help PROTO((void));

/*
 * Print the current recording state.
 * 
 * This function always returns 1.
 */
int quantify_print_recording_state PROTO((void));

/*
 * The following functions are stricly for those customers who have written
 * their own dynamic linker.
 *
 * The function quantify_process_file() is an undocumented function that
 * Quantify's the file 'in' and puts the name of the quantify'd file in 'out'.
 * This can be used with custom dynamic loaders.
 *
 * NOTE: The Sun dynamic library loader (dlopen) is already handled
 * automatically by Quantify.
 */
int quantify_process_file PROTO((const char in[1024], char out[1024]));

/*
 * Tell the "qv" symbol table lookup routines where files are mapped in memory.
 * 
 * This is a nop now, because qv doesn't look up the symbols.  Looking up
 * the symbols is now done at link time by the Quantify engine.
 */
int quantify_note_loaded_file PROTO((const char* filename, unsigned int where_loaded));

#if defined (c_plusplus) || defined (__cplusplus)
}
#endif

#endif /* QUANTIFY_H */

#!/bin/sh
#
# Script to print out the Purify options that a program was built with
#
# Copyright (c) 1993-1994 Pure Software.
#
# This file contains proprietary and confidential information and
# remains the unpublished property of Pure Software. Use, disclosure,
# or reproduction is prohibited except as permitted by express written
# license agreement with Pure Software.
#


# ------ Handy functions ------
report_error() {
  echo `basename $0`: $*
}

usage () {
  echo "Usage:"
  echo "    `basename $0` <Purify instrumented program name>"
}


# ------ Verify arguments ------
if [ -z "$1" ]; then
  usage
  exit 1
fi

prog=$1

if [ ! -x $prog ]; then
  report_error $prog is not an executable file
  usage
  exit 1
fi


# ------ OS setup ------
case "`uname -sr`" in
  SunOS\ 4.*)
    adb=/bin/adb ;
    sed=/bin/sed ;
    tail=/usr/ucb/tail ;
    tr=/bin/tr ;;
  SunOS\ 5.*)
    adb=/bin/adb ;
    sed=/bin/sed ;
    tail=/bin/tail ;
    tr=/bin/tr ;;
  HP-UX\ *)
    adb=/usr/bin/adb ;
    sed=/bin/sed ;
    tail=/bin/tail ;
    tr=/usr/bin/tr ;;
  *)
    report_error This script does not work on operating system `uname -sr` ;
    exit 1 ;;
esac


# ------ Use adb to print out the string value ------
report_sym_value () {
  # $1 is the label to use when displaying the value.
  # $2 is the symbol name to look for.

  symvalue=`echo "10000\\$w
  ${2}?s" \
	| $adb $prog \
	| $tail -1`

  if [ "$symvalue" = "symbol not found" ]; then
    echo $1 -- symbol not found --
  else
    echo "$symvalue" | $tr '	' ' ' | $sed "s/.*${symname}: */${1}/"
  fi
}


report_sym_value "Build time options: " _p_build_options_arg
report_sym_value "Command line options (run time defaults): " _p_command_options_arg

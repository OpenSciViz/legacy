#! /bin/sh
#
# pure_debug
# script executed to debug an application
#	$1 == the full application path and name
#	$2 == the process ID (zero for no current process)
#	remaining == debugger flags
#

#
# Figure out what kind of OS we are running on.
#
case "`uname -sr`" in
  HP-UX*)
    hpux=hpux ;;
  SunOS\ 4.*)
    sunos4=sunos4 ;;
  SunOS\ 5.*)
    sunos5=sunos5 ;;
  *)
    echo "Don't know this OS" 1>2 ;
    exit 1 ;;
esac

run_in_window ()
{
  # Runs a command in a terminal window.
  # Usage: run_in_window <title> <cmd> <args...>

  title="${1}"
  shift

  # Determine the type of terminal window and size to use.
  # Use PURE_TERM if defined, else default to something reasonable.
  if [ $hpux ]; then
    term=hpterm
  else
    term=xterm
  fi
  term="${PURE_TERM:-"${term}"}"

  # Some terminal emulators accept the -name option.
  # You can use this option to change the X application name that
  # these emulators use to look up their default options.
  # Example:
  #   xterm -name pureXTerm ...
  # along with an X resource
  #   pureXTerm*background: banana

  termtype () {
    basename $1
  }
  case "`termtype $term`" in

    xterm)
      $term -title "$title" -e "$@" ;;

    shelltool)
      $term -title "$title" "$@" ;;

    hpterm)
      $term -title "$title" -e "$@" ;;

    *)
      # Default to xterm syntax and hope for the best.
      $term -title "$title" -e "$@" ;;

  esac
}

title="Debug `basename $1`"

#
# Figure out what debugger to use.
#
if [ $hpux ]; then
  debugger="${PURE_DEBUGGER:-purify_xdb}"
  emacs_debugger=xdb
else
  debugger="${PURE_DEBUGGER:-dbx}"
  emacs_debugger=gdb
fi

#
# Parse the arguments
#
cd `dirname $1`
prog="`basename $1`"
shift
pid=$2
if [ "$pid" = "0" ]; then
  pid=""
fi
shift
# Remaining arguments are passed directly to the debugger.

case "`basename $debugger`" in

  dbx)
    run_in_window "$title" $debugger "$@" $prog $pid ;;

  gdb)
    run_in_window "$title" $debugger "$@" $prog $pid ;;

  xdb|purify_xdb)
    run_in_window "$title" purify_xdb ${pid:+"-P $pid"} "$@" $prog ;;

  softdebug|purify_softdebug)
    purify_softdebug -dbargs ${pid:+"-P $pid"} "$@" $prog ;;
      
  debugger|dbxtool)
    # Sun window interface to dbx
    $debugger "$@" $prog $pid ;;

  adb)
    if [ $sunos4 ]; then
      # No way to attach to a running program?
      run_in_window "$title" $debugger $prog
    elif [ $sunos5 ]; then
      # Want to supply adb the command "0t${pid}:A", but how?
      run_in_window "$title" $debugger $prog
    else
      run_in_window "$title" $debugger ${pid:+"-P${pid}"} "$@" $prog
    fi ;;

  emacs|lemacs|emacs19|emacs18)
    # Hack: Emacs as a debugger means to run gdb (or xdb) in emacs.
    lispfile=/tmp/`basename $0`_${prog}_$$.el ;
    cat > $lispfile <<-EOF
	($emacs_debugger "$prog")
	(if (not (string-equal "" "$pid")) (${emacs_debugger}-call "attach $pid"))
	EOF
    $debugger -l $lispfile ;
    rm -rf $lispfile ;;

  *)
    # Hope for the best...
    run_in_window "$title" $debugger $prog $pid ;;

esac

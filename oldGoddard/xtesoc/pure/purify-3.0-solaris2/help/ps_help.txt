!
! Hypertext help for common PS UI objects.
!
! Copyright (C) 1994 Pure Software, Inc.
! This file contains proprietary and confidential
! information and remains the unpublished property
! of Pure Software. Use, disclosure, or
! reproduction is prohibited except as permitted
! by express written license agreement with Pure
! Software.
!
!**********************************************************************
! Topics for generic Pure Software objects
!**********************************************************************
!
! Expand toolbar button.
@Topic@Expand@PS_OutlineExpand
Expands the selected item in the outline view.

! Collapse toolbar button.
! Collapse outline glyph.
@Topic@Collapse@PS_OutlineCollapse
Collapses the selected item in the outline view.

@Topic@Collapse All@PS_OutlineCollapseAll
Collapse the selected item and all of its sub-items.

@Topic@Collapse Children@PS_OutlineCollapseChildren
Collapse all the sub-items of the selected item.

! Expand glyph in outline.
@Topic@Expand and show@PS_OutlineExpand,PS_OutlineMakeVisible
Click on this glyph to expand the associated item.  The window is
scrolled to make as much of the item visible as possible.  Click again
to collapse the item.

@Topic@Toggle@PS_OutlineExpandCollapse
Collapse or expand the selected item.

@Topic@Expand All@PS_OutlineExpandAll
Expand this item and all of its sub-items.

@Topic@Next@PS_OutlineNext
Select the next selectable item.

@Topic@Next same level@PS_OutlineNextSameLevel
Select the next selectable item at this level.

@Topic@Previous@PS_OutlinePrevious
Select the previous selectable item.

@Topic@Previous same level@PS_OutlinePreviousSameLevel
Select the previous selectable item at this level.

@Topic@First@PS_OutlineFirst
Go to the first selectable item.

@Topic@Last@PS_OutlineLast
Go to the last selectable item.

@Topic@Make Visible@PS_OutlineMakeVisible
Make the current selected item visible.

@Topic@To Top@PS_OutlineToTop
Position the current selected item at the top of the display.

@Topic@Select@PS_OutlineSelect
Select the current item.

@Topic@Here@PS_OutlineHere
Shows the current line of interest.

#! /bin/sh
#
# pure_run
# script executed to re-run an application
#	$1 == the full application path and name
#	$2 == arguments
#

#
# Figure out what kind of OS we are running on.
#
case "`uname -sr`" in
  HP-UX*)
    hpux=hpux ;;
  SunOS\ 4.*)
    sunos4=sunos4 ;;
  SunOS\ 5.*)
    sunos5=sunos5 ;;
  *)
    echo "Don't know this OS" 1>2 ;
    exit 1 ;;
esac

run_in_window ()
{
  # Runs a command in a terminal window.
  # Usage: run_in_window <title> <cmd> <args...>

  title="${1}"
  shift

  # Determine the type of terminal window and size to use.
  # Use PURE_TERM if defined, else default to something reasonable.
  if [ $hpux ]; then
    term=hpterm
  else
    term=xterm
  fi
  term="${PURE_TERM:-"${term}"}"

  # Some terminal emulators accept the -name option.
  # You can use this option to change the X application name that
  # these emulators use to look up their default options.
  # Example:
  #   xterm -name pureXTerm ...
  # along with an X resource
  #   pureXTerm*background: banana

  termtype () {
    basename $1
  }
  case "`termtype $term`" in

    xterm)
      $term -title "$title" -e "$@" ;;

    shelltool)
      $term -title "$title" "$@" ;;

    hpterm)
      $term -title "$title" -e "$@" ;;

    *)
      # Default to xterm syntax and hope for the best.
      $term -title "$title" -e "$@" ;;

  esac
}

title="`basename $1`"

marker="*******************************************************"

banner ()
{
  # First argument is the banner message.
  echo "$marker"
  echo "*"
  echo "*    $1"
  echo "*"
  echo "$marker"
  echo
}

destroy_window ()
{
  # First argument to this function is the exit status.
  # Second argument is a message to display.
  echo
  echo "$marker"
  echo "*"
  if [ ! -z "$2" ]; then
    echo "*    $2"
  fi
  echo "*    Hit <Return> to destroy window."
  echo "*"
  echo "$marker"
  read wait_and_then_return
  exit ${1:-0}
}


special_flag="!ThisIsASpecialPureRunFlag!"

if [ "$1" = "$special_flag" ]; then
  #
  # This is a recursive invocation of the script, so we
  # are running in a new window.
  # Run the program and exit after user acknowledgment.
  #
  shift
  prog="$1"
  shift
  args="$@"
  name="`basename $prog`"
  # Now run the program.
  banner "Starting \"${prog}${args:+ ${args}}\"."
  $prog $args
  destroy_window $? "Finished \"$name\"."
else
  #
  # The script is being invoked directly, so reinvoke in a window
  # with a bit of magic so we can detect the recursion.
  #
  run_in_window "$title" $0 $special_flag "$@"
fi

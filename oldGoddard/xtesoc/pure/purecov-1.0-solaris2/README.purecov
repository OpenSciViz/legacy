Release notes for PureCoverage 1.0 Solaris 2

Supported systems
=================

  Operating system and Hardware
  -----------------------------

    PureCoverage has been tested with Solaris
    versions 2.3, and 2.4 on SPARC platforms.

  Compilers
  ---------

    PureCoverage supports the following compilers:
    - SPARCWorks C and C++ versions:
         2.x and 3.x
    - GNU gcc and g++ versions:
         2.5.6 and 2.5.8
    - CenterLine C (clcc) versions:
         1.x

  Threads
  -------

    PureCoverage supports these threads packages:
    - The native Solaris libthread library.


Restrictions and Known Issues
=============================

  General
  -------

  - PureCoverage will not run on short
    (14-character) file systems.

  User Interface
  --------------

  - If a large number of items are selected,
    "Expand all" followed by "Collapse all" can
    crash some unpatched versions of the
    OpenWindows 3.0 server.

  Compilers
  ---------

  - The GNU gcc extensions are not tested against
    PureCoverage.  Most gcc extensions will
    probably work fine.  Known limitations at
    present include problems with nested functions
    (e.g.: making a pointer to a nested function
    and attempting to call through it will not
    work).

  - Code compiled with SPARCWorks 3.0 compilers
    using the flags -fPIC or -fpic, at
    optimization levels -O1 and -O2 which is not
    made into a shared library may generate
    warnings about switch tables and fail to
    be instrumented.

  SPARCWorks Debugger
  -------------------

  - The new SPARCWorks debugger, version 3, does
    not recognize instrumented multi-threaded
    applications as being multi-threaded.

    As a partial workaround, you can use adb.
    There are three adb commands that support
    Solaris light-weight processes:
	$L	lists all lwps
	$l	shows the current lwp number
	n:l	switches the focus to lwp number n

    If you use the flags THR_BOUND and THR_NEW_LWP
    when you create threads, each thread will be
    assigned its own lwp.  You can then use adb to
    inspect the state of all threads.

# Sample makefile template
# May also be used in instrumenting Hello World with PureCoverage
# Use make -f hello_world.Makefile PDIR=<purecovhome> a.out.pure

# PureCoverage related flags
PFLAGS	=
PDIR	= `purecov -print-home-dir`
PURECOV	= purecov $(PFLAGS)
PSTUBS	= $(PDIR)/purecov_stubs.a

# General flags
CC	= cc
CFLAGS	= -g -I$(PDIR)

# Targets
all: a.out a.out.pure

a.out: hello_world.c
	 $(CC) $(CFLAGS) -o $@ $? $(PSTUBS)

a.out.pure: hello_world.c
	 $(PURECOV) $(CC) $(CFLAGS) -o $@ $? $(PSTUBS)

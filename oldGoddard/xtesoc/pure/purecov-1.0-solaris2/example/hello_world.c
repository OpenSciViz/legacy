/*
 * This is a test program used to demonstrate the use of PureCoverage
 * to increase test coverage.
 */

#include <stdio.h>

void display_hello_world();
void display_message();

main(argc, argv)
    int argc;
    char** argv;
{
    if (argc == 1)
        display_hello_world();
    else
        display_message(argv[1]);
    exit(0);
}

void
display_hello_world()
{
	printf("Hello, World\n");
}

void
display_message(s)
	char *s;
{
	printf("%s, World\n", s);
}

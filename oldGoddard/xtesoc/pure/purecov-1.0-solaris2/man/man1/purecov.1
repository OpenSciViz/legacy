.TH PureCoverage 1 "30 August 1994" "Pure Software Inc." "LICENSED SOFTWARE"
.\"
.\" MANPAGE for PureCoverage
.\"
.hw PureCoverage
.SH NAME
purecov \- detect untested portions of programs
.SH SYNOPSIS
.nf
\fBpurecov \fP[\fB\-\fIoption\fR .\|.\|.] $(CC) $(CFLAGS) \-o \fIprogram\fP $(OBJS) $(LIBS)
\fBpurecov \-view\fR [\fIprograms\fB.pcv\fR]
\fBpurecov \-export\fR[\fB=\fIascii.data\fR] \fIprograms\fB.pcv\fR
\fBpurecov \-merge=\fImerged\fP.pcv \fIprograms\fB.pcv\fR
.\"
.SH DESCRIPTION
.\"
PureCoverage 1.0 detects unused portions of your program \(em places
where more intensive testing is necessary to exercise the code.
PureCoverage tracks coverage information across multiple runs of a
program, and permits analysis of coverage for shared portions of
multiple programs.
.LP
PureCoverage includes an interactive Viewer for analysis of the
coverage information, a merging tool for manipulating coverage
data files, and an export tool for converting the data into an
ASCII format suitable for further processing by scripts and other
programs.  PureCoverage also includes a variety of scripts
for performing data analysis and manipulation, and also as examples
for users wishing to write their own scripts.
.SS Setting Up
PureCoverage is typically loaded from CD-ROM, and then installed using
the \fBpure_install\fP command.  You will need a license certificate
with a valid password; enter the information requested exactly as
printed on the certificate.  For more information, see the ``System
Administrator's Guide.''
.LP
To use PureCoverage, create a symbolic link from where \fBpurecov\fP
is installed (\fIpurecovhome\fB/purecov\fR) to
\fB/usr/local/bin/purecov\fP (or some other directory on each user's
\fB$PATH\fP):
.LP
.nf
    example% \fBln \-s \fIpurecovhome\fP/purecov /usr/local/bin/purecov\fR
.fi
.LP
PureCoverage follows this symbolic link to find the other files
it needs in the installation directory.
Alternatively, add \fIpurecovhome\fP to each user's \fB$PATH\fP.
.LP
The PureCoverage scripts should also be made available using
a similar process.  The scripts normally reside in the directory
\fIpurecovhome\fB/scripts\fR.
.SS Building
Add the word \fBpurecov\fP to the beginning of the link line
in your Makefile.  For example:
.LP
.nf
.if t \{
    \fIprogram\fP: $(OBJS)
            \fBpurecov\fR [\fB\-\fIoption\fR .\|.\|.] $(CC) $(CFLAGS) \-o \fIprogram\fP $(OBJS) $(LIBS)\}
.if n \{
    \fIprogram\fP: $(OBJS)
            \fBpurecov\fR [\fB\-\fIoption\fR .\|.\|.] $(CC) $(CFLAGS) \-o \fIprogram\fP \\
                    $(OBJS) $(LIBS)\}
.fi
.LP
PureCoverage modifies the object code at link time to produce an
equivalent program which has additional counting instructions
built in. The \fB\-\fIoptions\fR specified on the command line are PureCoverage
options to customize its operation.
.SS Running
The PureCoverage-instrumented program is run exactly as the original program.
When the program finishes running, a file \fIprogram\fB.pcv\fR will
be created or updated with the new coverage data.
.SS Viewing and Manipulating Data
To invoke the Viewer on the resulting file, use the command:
.LP
.nf
    example% \fBpurecov \-view\fR [\fB\-\fIoption\fR .\|.\|.] \fIprogram\fB.pcv\fR
.fi
.LP
This displays the coverage data in the interactive Viewer.  To convert
a \fB.pcv\fR file into an ASCII format for further processing, use the
command:
.LP
.nf
.if t \{
    example% \fBpurecov \-export\fR[\fB=\fIascii.data\fR] [\fB\-\fIoption\fR .\|.\|.] \fIprogram\fB.pcv\fR\}
.if n \{
    example% \fBpurecov \-export\fR[\fB=\fIascii.data\fR] [\fB\-\fIoption\fR .\|.\|.] \\
                 \fIprogram\fB.pcv\fR\}
.fi
.LP
This sends the output to a file named \fIascii.data\fP; if this
filename is not supplied, output will be sent to the standard output,
which may then be redirected to another program.  To combine several
\fB.pcv\fP files into a single \fB.pcv\fP file, use the command:
.LP
.nf
.if t \{
    example% \fBpurecov \-merge=\fImerged\fB.pcv\fR [\fB\-\fIoption\fR .\|.\|.] \fIfirst\fP.pcv \fIsecond\fB.pcv .\|.\|.\}
.if n \{
    example% \fBpurecov \-merge=\fImerged\fB.pcv\fR [\fB\-\fIoption\fR .\|.\|.] \\
                 \fIfirst\fB.pcv \fIsecond\fB.pcv .\|.\|.\}
.fi
.LP
All three of these commands permit specification of multiple \fB.pcv\fP files,
not just \fB\-merge\fP.
.SS Purify and PureCoverage
PureCoverage is designed to work with Purify to collect code coverage
information at the same time as monitoring the code for access errors
and memory leaks.
.LP
To create a program instrumented by Purify and PureCoverage, use a
command like:
.LP
.nf
.if t \{
    \fBpurify\fR [\fB\-\fIoption\fR .\|.\|.] \fBpurecov\fR [\fB\-\fIoption\fR .\|.\|.] $(CC) $(CFLAGS) \-o \fIprogram\fP $(OBJS) $(LIBS)
\}
.if n \{
    \fBpurify\fR [\fB\-\fIoption\fR .\|.\|.] \fBpurecov\fR [\fB\-\fIoption\fR .\|.\|.] \\
            $(CC) $(CFLAGS) \-o \fIprogram\fP $(OBJS) $(LIBS)
\}
.fi
.SH OPTIONS
.LP
PureCoverage's operation can be customized for your program.  By specifying
options either at program build-time or program run-time, you can affect
where PureCoverage data goes and how the program is instrumented and linked.
.LP
.hw CountsFile
Each option is a word or phrase that begins with a hyphen, for example
\fB\-counts\-file\fP.  PureCoverage ignores the case and any hyphens
or underscores in the option name.  The option \fB\-counts\-file\fP is
equivalent to \fB\-COUNTS_FILE\fP and \fB\-CountsFile\fP.  All options
have default values.  You may override these values using the
assignment syntax:
.LP
.nf
    \fB\-\fIoption-name\fR[\fB=\fR[\fIvalue\fR]]
.fi
.LP
Note that no spaces are permitted on either side of the equals sign (=) if
it is supplied.
.LP
.hw PURECOVOPTIONS PUREOPTIONS
You may specify PureCoverage options:
.de BB
.IP "\h'.25i'\(bu"
..
.BB
on the build-time link line; or
.BB
in the environment variables \fB$PURECOVOPTIONS\fP or
\fB$PUREOPTIONS\fP.
.LP
.SS Link Line Options
Options may be specified on the link line, e.g.:
.LP
.nf
    \fBpurecov \-cache\-dir=$(HOME)/pcache\fP $(CC) .\|.\|.
.fi
.LP
Build-time options apply to the PureCoverage build command being run.
Run-time options may also be specified on the link line.  These are
built into the executable and become the default values at run-time
when the PureCoverage-instrumented executable is run.  This is a
convenient way to build a program with non-standard default values for
certain options, for example:
.LP
.nf
    \fBpurecov \-counts\-file=counts\fP $(CC) .\|.\|.
.fi
.SS Environment Variable Options
Any option may be specified in the environment variables
\fB$PURECOVOPTIONS\fP or \fB$PUREOPTIONS\fP.

Note: Values in \fB$PURECOVOPTIONS\fP have precedence over values
from \fB$PUREOPTIONS\fP.

Build-time options specified in environment variables are applied when
applications are built.  Options specified explicitly on the
build-time link line override environmental values.

Run-time options specified in environment variables are applied when
PureCoverage-instrumented applications are run.  The environmental
values in force when the program is run override any defaults
specified on the link line when the application was built.

Note: If an option is specified more than once in an environment
variable, the first value is used.  To add an overriding value for the
\fB\-log\-file\fP option, for example, without changing other options
specified, use a command like:
.LP
.nf
    csh% \fBsetenv PURECOVOPTIONS "\-log\-file=new $PURECOVOPTIONS"\fP
    ksh$ \fBexport PURECOVOPTIONS="\-log\-file=new $PURECOVOPTIONS"\fP
.fi
.LP
.\"
.\" Options stuff -- keep in sync with purify_options.proto.
.\"
.\" These should probably go into purecov_options.proto or something
.\" (and maybe pure_options.proto for options common to P, PC, and Q),
.\" but there aren't too many of them and they don't show up in the
.\" help text anyway.
.\"
.ta 3i \n(.lu-\n(.iuR
.\" to have middle field centered, use this instead:
.\" .ta (\n(.lu-(3u*\n(.iu/4u))/2uC \n(.lu-\n(.iuR
.de TX
.nf
\h'-\n(.iu/4u'\fB\\$1\t\\$2\t\\$3\fR
.fi
..
.de T2
.ne 1i
.LP
.TX "\-\\$1" "\\$2" "\\$3"
.LP
..
.ne 3i
.TX "OPTION" "TYPE" "DEFAULT"
.SS Caching Options (build-time options)
.T2 "cache\-dir" "directory" "\fIpurecovhome\fP\fB/cache\fP"
This option sets the global directory where PureCoverage-instrumented
versions of libraries will be cached.
.T2 "always\-use\-cache\-dir" "bool" "no"
This option forces all PureCoverage-instrumented versions of libraries
to be written to the global cache directory.  When you use the default
value, PureCoverage caches to the original library's directory, if it
is writable.  If the directory is not writable, PureCoverage caches it
to the value of \fB\-cache\-dir\fP.
.SS Linker Options (build-time options)
.T2 "linker" "program-name" "/bin/ld"
This option specifies the name of the linker that PureCoverage should
invoke to produce the executable.
.SS Logfile Options
.T2 "log\-file" "filename" "stderr"
This option specifies the name of the logfile for PureCoverage
messages.  Normally, PureCoverage routes its report to the standard
error stream.  The filename may include one or more of these special
keywords:
.BB
\fB%p\fP is replaced by the process id;
.BB
\fB%v\fP is replaced by the program name; and
.BB
\fB%V\fP is replaced by the full pathname of the program.
.LP
These keywords are useful when you fork many PureCoverage-instrumented
programs and want separate logfiles for each one.
.T2 "append\-logfile" "bool" "no"
This option enables PureCoverage output to be appended to the current
logfile rather than replacing it.
.SS Filename Options
.T2 "auto\-mount\-prefix" "directory" "/tmp_mnt"
This option is used to specify the directory prefix used by the file
system auto-mounter, usually \fB/tmp_mnt\fP, to mount remote file
systems in NFS environments.  Use this option to strip the prefix, if
present, to improve the readability of source filenames in
PureCoverage reports.
.T2 "counts\-file" "filename" "\fIprogram\fP.pcv"
This option specifies where the resulting coverage data will be
written when the program exits.  The filename may include one or more of
these special keywords:
.BB
\fB%p\fP is replaced by the process id;
.BB
\fB%v\fP is replaced by the program name; and
.BB
\fB%V\fP is replaced by the full pathname of the program.
.LP
If the specified file already exists, the resulting coverage data will
be combined with the coverage data already present in the file.
.T2 "program\-name" "program name" "argv[0]"
This option specifies the full pathname of the
PureCoverage-instrumented program if \fBargv[0]\fP contains an
undesirable or incorrect value.  This can happen if you change
\fBargv[0]\fP in your program, or if your program is invoked by an
\fBexec()\fP call whose path differs from the argument it passes as
\fBargv[0]\fP to your program.  In such cases, PureCoverage can not
find the program file, and therefore can not interpret addresses as
function names.
.LP
You may need to use this option if you find little or no symbolic
information in the reports from your PureCoverage-instrumented
program.
.T2 "user\-path" "dir list" "none"
This option specifies a colon-separated list of directories in which
to search to find programs and source code.  For example, if you have
a source tree in your home directory but you also link with GNU source
code, you could use:
.LP
.nf
    \fB\-user\-path=/usr/home/chris/src:/usr/local/gnu/src\fP
.fi
.LP
PureCoverage searches for the executable file from which to read the
symbol table in your $PATH, and then in directories listed in
\fB\-user\-path\fP.  See also \fB\-program\-name\fP.
.SS Additional Options
.T2 "force\-merge" "bool" "no"
Normally, PureCoverage accumulates data for a given file across
multiple executions of the programs using that file.  After that file
is recompiled, the old data will be disregarded and replaced with new
data when the program next runs.  This ensures that coverage data is
always accurate with respect to the latest version of each source
file.
.LP
In certain circumstances, it may be desirable to override this behavior.
A typical situation is when a single source file is conditionally compiled
with slight differences for use in two different programs.  If you try to
merge the coverage data for these two programs, PureCoverage will report
that one of them is out of date relative to the other and discard the
``old'' data.
.LP
The \fB\-force\-merge=yes\fP option will cause PureCoverage to combine
the data regardless of the timestamps stored in the data.  Care must
be exercised in using this option to ensure that data may be
meaningfully combined.
.LP
.TX "\-handle\-signals" "signal list" "none"
.TX "\-ignore\-signals" "signal list" "system dependent"
.LP
PureCoverage installs a signal handler for many of the possible
software signals which may be delivered to an instrumented process, to
print an informative message and save coverage data to the \fB.pcv\fP
file when the process crashes.
.LP
The option value specifies which signals are reported and cause data
to be saved.  It is a comma-delimited list of signal names.  The
default value of \fB\-ignore\-signals\fP is system dependent.  The
defaults are:
.LP
.nf
.BB
SunOS 4.1.x:
SIGHUP,SIGINT,SIGQUIT,SIGILL,SIGTRAP,SIGIOT,SIGABRT,
SIGEMT,SIGFPE,SIGKILL,SIGBUS,SIGSEGV,SIGSYS,SIGPIPE,
SIGTERM,SIGXCPU,SIGXFSZ,SIGLOST,SIGUSR1,SIGUSR2
.BB
Solaris 2.x:
SIGHUP,SIGINT,SIGQUIT,SIGILL,SIGTRAP,SIGIOT,SIGABRT,
SIGEMT,SIGFPE,SIGKILL,SIGBUS,SIGSEGV,SIGSYS,SIGPIPE,
SIGTERM,SIGUSR1,SIGUSR2,SIGPOLL,SIGXCPU,SIGXFSZ,
SIGFREEZE,SIGTHAW,SIGRTMIN,SIGRTMAX
.BB
HP\-UX:
SIGHUP,SIGINT,SIGQUIT,SIGILL,SIGTRAP,SIGABRT,SIGEMT,
SIGFPE,SIGKILL,SIGBUS,SIGSEGV,SIGSYS,SIGPIPE,SIGTERM,
SIGUSR1,SIGUSR2,SIGLOST,SIGRESERVE,SIGDIL
.fi
.LP
For instance, if you want PureCoverage to save data on the non-fatal
signals SIGALRM and SIGVTALRM, use:
.LP
.nf
    \fB\-handle\-signals=SIGALRM,SIGVTALRM\fP
.fi
.LP
.hw SIGSEGV
If you do not want PureCoverage to save data on the fatal signal
SIGSEGV because your program handles it, use:
.LP
.nf
    \fB\-ignore\-signals=SIGSEGV\fP
.fi
.LP
.hw SIGSTOP SIGKILL SIGTRAP
Note: PureCoverage does not ever handle SIGSTOP, SIGKILL, and SIGTRAP
signals, since doing so interferes with debuggers and PureCoverage's
own operation.
.LP
See the man pages for \fBsignal()\fP and \fBsigmask()\fP, and the
files \fB/usr/include/signal.h\fP and \fB/usr/include/sys/signal.h\fP
for more information on signals.
.\"
.\" Redefine TX and T2 for API functions.
.de TX
.nf
\h'-\n(.iu/4u'\fB\\$1\fR
.fi
..
.de T2
.ne 1i
.LP
.TX "\\$1"
.LP
..
.SH APPLICATION PROGRAMMING INTERFACE
PureCoverage's application programming interface can be used by programs
as necessary and in interactive debugging environments like dbx, gdb,
and xdb.
.T2 "int purecov_is_running()"
This function returns 1 if the executable has been instrumented with
PureCoverage, 0 otherwise.  This function may be used to enclose
special purpose application code to execute in the
PureCoverage-instrumented environment.  For example:
.LP
.nf
    \fBif (purecov_is_running())  {\fP
    \fB    install_gui_coverage_reset_button();\fP
    \fB}\fP
.fi
.T2 "int purecov_save_data()"
This function causes coverage data to be saved immediately.  Counts
are then reset to zero, to avoid double counting.
.T2 "int purecov_clear_data()"
This function causes the coverage data to be reset to zero, discarding
collected data.
.T2 "int purecov_set_filename(const char* \fIfilename\fP)"
This function specifies that coverage counts should be saved to
\fIfilename\fP.  Embedded \fB%p\fP, \fB%v\fP, and \fB%V\fP are turned
into the program process id, the program name, and the full pathname
of the program, respectively.
.ne 1i
.LP
.TX "int purecov_enable_save()"
.TX "int purecov_disable_save()"
.LP
These functions are used to enable or disable the automatic
saving of coverage data at program exit.
.LP
.SH FILES
.IP "\fB*_pure_*\fP"
Cached object and library files translated by PureCoverage.
.IP "\fIpurecovhome\fB/cache/.\|.\|.\fR"
Central cache directory structure for caching translated object files and
libraries when they cannot be written alongside the original files.
.IP "\fB.pure\fP, \fB.lock.*\fP"
Locking files preventing simultaneous file access by more than one user.
.IP "\fB.purecov\fP, \fB.purecov.sunos4\fP, \fB.purecov.solaris2\fP, \fB.purecov.hpux\fP"
Used to specify exclude directives to ignore coverage in uninteresting
libraries.
.IP "\fIpurecovhome\fB/PureCoverage.purela\fR"
Contains passwords and a list of regular users for use by the Pure
License Advisor.
.IP "\fIpurecovhome\fB/purecov.h\fR"
Include file to use with PureCoverage API.
.IP "\fIpurecovhome\fB/purecov_stubs.a\fR"
Library to be included to provide dummy PureCoverage API function
definitions for use when not linking under PureCoverage.
.IP "\fIpurecovhome\fB/PureCoverage.XResources\fR"
Default X resource values.
.SH SEE ALSO
\fBpurify(1)\fP,
\fBdbx(1)\fP, \fBgdb(1)\fP,
\fBpc_annotate(1)\fP, \fBpc_below(1)\fP, \fBpc_diff(1)\fP,
\fBpc_email(1)\fP, \fBpc_ssheet(1)\fP, \fBpc_summary(1)\fP
.SH CAVEATS
A PureCoverage-instrumented program normally runs about 2 times
slower, and takes about 10% more memory, than if not processed by
PureCoverage.  Occasionally the additional memory required will cause
the paging behavior to degrade performance to a greater degree.  In
this case, running the PureCoverage-instrumented program on a machine
with more memory would be advantageous.
.LP
A PureCoverage-instrumented program can, because of its different
memory usage patterns, cause code that works by accident to behave
slightly differently.  For example, values read from uninitialized or
unallocated locations can differ with and without PureCoverage.  You
can use Purify to detect such code.  PureCoverage will not change the
behavior of your code in any other way.
.LP
If PureCoverage does change the behavior of the program (except as
mentioned above), or produces incorrect coverage reports, please
report such bugs to the appropriate address:
.nf
.ta \w'MM'u +\w'MM'u +\w'support@europe.pure.com MM'u +0.5i
	Contact us in the US at:
		support@pure.com	or (408) 720 1600
	In Europe:
		support@europe.pure.com	or (+1) (408) 720 1600
.LP

! Hypertext help for PureCoverage 1.0
! Copyright (C) 1994 Pure Software, Inc.
! All Rights Reserved.
!
!*************************************************
!
! Lines starting with '!' are comments
!
!*************************************************
!
!
!*************************************************
!
!
!*************************************************
!
! Entries to be displayed in the Help pulldown menu
!
!*************************************************
!
@OI_default@On version@about_purecov
@OI_default@On license@license
@OI_default@PureCoverage overview@purecov_overview
@OI_default@Release notes@release-notes
@OI_default@Bug report@bug-report
!
!*************************************************
!
@Topic@About PureCoverage@about_purecov
PureCoverage 1.0 Solaris 2
 
PureCoverage, Copyright 1994 Pure Software Inc.
All rights reserved.
U.S. Patent No. 5,193,180;
  other U.S. and foreign patents pending.
Pure Software, Purify, Quantify, and PureLink
  are registered trademarks of Pure Software Inc.
PureCoverage and PureLA
  are trademarks of Pure Software Inc.
 
 Contact us in the US at:
   support@pure.com        or (408) 720 1600
 In Europe:
   support@europe.pure.com or (+1) (408) 720 1600
!
!*************************************************
!
@Topic@PureCoverage license@license
PURE SOFTWARE INC.  SIMPLE LICENSE AGREEMENT

1.  GRANT OF LICENSE.  This Agreement permits the
use of the enclosed Software (including its
link-time and run-time modules) by the number of
Regular Users for which you have licenses. A
Regular User is a person who has used the Software
more than twelve times. Once a person is a Regular
User, he or she remains a Regular User until you
reasonably expect that he or she will cease all
use of the Software for the following six months.

2.  YOUR RESPONSIBILITIES.  You agree to have a
reasonable process in place to ensure that the
number of Regular Users does not exceed the number
of licenses. Having more Regular Users than
licenses is a material violation of this
Agreement.

3.  COPIES.  You may make a reasonable number of
copies of the Software as long as you take
reasonable measures to ensure that each copy is
used only as allowed by the terms of this
Agreement.

4.  EVALUATION.  If you are using the Software
under a Pure Software evaluation program, the term
of the license will be set at the discretion of
Pure Software, and the number of Regular Users at
your site is unrestricted.

5.  BREACH.  This Agreement will terminate if you
materially breach any provision of this Agreement
and fail to cure such breach (and demonstrate
compliance to Pure Software) within thirty (30)
days of written notice from Pure Software. Upon
termination, you will cease all use of the
Software and destroy all written materials and
copies of the Software.

6.  OWNERSHIP AND RESTRICTIONS.  The Software is
owned by Pure Software.  This license does not
constitute a sale of the Software. You may not
rent, lease, or sublicense the Software. You may
not reverse engineer, decompile, or disassemble
the Software. This Agreement is not assignable or
transferable without the express written consent
of Pure Software.

7.  LIMITED WARRANTY.  Pure Software warrants that
the Software will perform substantially in
accordance with the accompanying written materials
for a period of ninety (90) days from the date of
receipt. Pure Software's entire liability and your
exclusive remedy under any warranty or legal
theory shall be replacement of the Software or
return of the price paid.

8.  NO OTHER WARRANTIES.  PURE SOFTWARE DISCLAIMS
ALL OTHER WARRANTIES, EITHER EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE, WITH RESPECT TO THE SOFTWARE, AND THE
ACCOMPANYING WRITTEN MATERIALS. EXCEPT FOR THE
LIMITED WARRANTY PROVIDED ABOVE, THE SOFTWARE IS
PROVIDED AS IS.

9.  LIMITATION OF REMEDIES AND DAMAGES; FORCE
MAJEURE.  PURE SOFTWARE SHALL NOT BE RESPONSIBLE
OR LIABLE WITH RESPECT TO ANY SUBJECT MATTER OF
THIS AGREEMENT OR ANY ATTACHMENT, PRODUCT ORDER,
SCHEDULE OR TERMS AND CONDITIONS RELATED THERETO
UNDER ANY CONTRACT, NEGLIGENCE, STRICT LIABILITY
OR OTHER THEORY:
  A. FOR LOSS OR INACCURACY OF DATA OR (EXCEPT FOR
     RETURN OF AMOUNTS PAID TO PURE SOFTWARE
     THEREFORE). COST OF PROCUREMENT OF SUBSTITUTE
     GOODS, SERVICES OR TECHNOLOGY.
  B. FOR ANY INDIRECT, INCIDENTAL OR CONSEQUENTIAL
     DAMAGES INCLUDING, BUT NOT LIMITED TO, LOSS
     OF REVENUES AND LOSS OF PROFITS, OR
  C. FOR ANY MATTER BEYOND ITS REASONABLE CONTROL.

10. U.S. GOVERNMENT MATTERS.  The Software and
documentation are provided with RESTRICTED RIGHTS.
Use, duplication, or disclosure by the Government
is subject to restrictions as set forth in
subparagraph (c)(1)(ii) of the Rights in Technical
Data and Computer Software clause at DFARS
252.227-7013 or subparagraphs (c)(1) and (2) of
the Commercial Computer Software-Restricted Rights
at 48 CFR 52.227-19, as applicable.  Contractor is
Pure Software Inc., 1309 South Mary Avenue,
Sunnyvale, CA 94087.

11. ENFORCEABILITY.  If any provision of this
Agreement shall be held illegal or unenforceable,
that provision shall be limited or eliminated to
the minimum extent necessary so that this
Agreement shall otherwise remain in full force and
effect, and enforceable.

April 7, 1994-Revision PUR-0005-013
!
!*************************************************
!
@Topic@Release notes@release-notes
Release notes for PureCoverage 1.0 Solaris 2

Supported systems
=================

  Operating system and Hardware
  -----------------------------

    PureCoverage has been tested with Solaris
    versions 2.3, and 2.4 on SPARC platforms.

  Compilers
  ---------

    PureCoverage supports the following compilers:
    - SPARCWorks C and C++ versions:
         2.x and 3.x
    - GNU gcc and g++ versions:
         2.5.6 and 2.5.8
    - CenterLine C (clcc) versions:
         1.x

  Threads
  -------

    PureCoverage supports these threads packages:
    - The native Solaris libthread library.


Restrictions and Known Issues
=============================

  General
  -------

  - PureCoverage will not run on short
    (14-character) file systems.

  User Interface
  --------------

  - If a large number of items are selected,
    "Expand all" followed by "Collapse all" can
    crash some unpatched versions of the
    OpenWindows 3.0 server.

  Compilers
  ---------

  - The GNU gcc extensions are not tested against
    PureCoverage.  Most gcc extensions will
    probably work fine.  Known limitations at
    present include problems with nested functions
    (e.g.: making a pointer to a nested function
    and attempting to call through it will not
    work).

  - Code compiled with SPARCWorks 3.0 compilers
    using the flags -fPIC or -fpic, at
    optimization levels -O1 and -O2 which is not
    made into a shared library may generate
    warnings about switch tables and fail to
    be instrumented.

  SPARCWorks Debugger
  -------------------

  - The new SPARCWorks debugger, version 3, does
    not recognize instrumented multi-threaded
    applications as being multi-threaded.

    As a partial workaround, you can use adb.
    There are three adb commands that support
    Solaris light-weight processes:
	$L	lists all lwps
	$l	shows the current lwp number
	n:l	switches the focus to lwp number n

    If you use the flags THR_BOUND and THR_NEW_LWP
    when you create threads, each thread will be
    assigned its own lwp.  You can then use adb to
    inspect the state of all threads.
!
!*************************************************
!
@Topic@Bug report@bug-report
Thanks for taking the time to report your problems
to us.  We are anxious to make our products as
robust and solid as possible, and we value your
help.  Please be sure to provide as complete a
description of the problem as you can, and include
any test-case material available, especially any
object files or libraries causing problems, and
any debugger traces you may have obtained.  Please
be sure to include product, platform, and versions
numbers.  The product may have produced a lengthy
debugging dump if it crashed; please be sure to
enclose all of any such report along with a
description of what you were doing at the time.

You can find this blank bug report form in the
product installation directory as:
    `purecov -print-home-dir`/BUG_REPORT.
Please fill it in and email it to the address
shown.

--------------------------------------------
To: purecov-beta@pure.com
Subject: PureCoverage bug report
----------------------

Product: PureCoverage 1.0 Solaris 2

Type of bug:
    (e.g.: installation,
    	   build-time warning, build-time crash,
    	   run-time warning, run-time crash,
    	   unexpected results,
           user-interface problem, ...)

Platform: (Vendor, type):

Operating system revision: (e.g. `uname -a`)

Synopsis (one-line specific title for bug):

Description of the problem:



How to reproduce, if possible:



Any work-arounds used:



Other notes:
!
!*************************************************
!
! OI can generate calls for the help topic "unknown"
@Topic@unknown@purecov
@PureCoverage overview@PureCoverage overview@purecov_overview
!
!*************************************************
!
There is no specific help for your current
selection.  The "PureCoverage overview" menu above
will introduce you to PureCoverage, the data it
collects, and how to operate the tool.
!
!*************************************************
!
!
!*************************************************
!
! Entries above should remain there, and in that order
!
!*************************************************
!
!
!*************************************************
!
@Topic@PureCoverage overview@purecov_overview
@See also@Viewer@pcOutline
@See also@Annotated source@pcAnnotatedSource
@See also@Running scripts@pcScriptOutput
This is the graphical interface to PureCoverage.
PureCoverage collects test coverage information
for your program(s) and enables you to view it
in summary or detail form.

Help for various menus and regions of windows
can be obtained by selecting "On Context"
from the "Help" pulldown on the top-right of
the windows, and then clicking on the object
for which help is desired.

Coverage information may be from a single .pcv
file or from many .pcv files depending on the
"purecov -view" command used.  If the data is
from more than one .pcv file, then it can
potentially be from several different programs
which share some code or from several versions
of a single program, collected as the program
evolved.  In this case, only the up-to-date
data will be presented; data for new versions
of object files will have replaced the data
for old versions of the object files.

There are three kinds of windows which may be
displayed by the graphical interface:

 * The Viewer window is always present and
   contains a hierarchical summary of the
   coverage data.

 * The Annotated Source window can be opened to
   show line-by-line coverage for a specific file.

 * The Script Output window can be opened
   by running a PureCoverage data analysis script
   from within the graphical interface.

The PureCoverage data analysis scripts can also
be invoked from a shell, or you can write your
own programs to process the coverage data
which can be converted to an ASCII format using
the "purecov -export" command.
!
!*************************************************
!
@Topic@Annotated source@pcAnnotatedSource
@See also@Next Unused@pcNextUnused
@See also@Prev Unused@pcPrevUnused
@See also@Go To Line@pcGoto
@See also@Find@pcFind
The PureCoverage Annotated Source window has a
menu bar across the top of the window, a searching
menu across the bottom of the window, and a
source display region in the middle of the window.
The source display region is further sub-divided
into line numbers, line counts, and highlighted
source.

By default, the program is shown with unused lines
highlighted in reverse video.  Other lines have
a non-0 number of times they were used shown to
their left.

The counts have a limited range, 0 thru 9999
and "10K+".  10K+ is used to indicate any value
greater than 9999.

The display of line numbers and line counts can
be disabled and re-enabled using selections from
the "View" pulldown on the top menu bar.  Items
from the "View" menu can also be used to alter
the change the highlighting from unused to used
lines.
!
!*************************************************
!
@Topic@Viewer@pcOutline
The PureCoverage Viewer window has a 
a coverage summary region, a title region, and
a menu bar.

Coverage Summary Region
-----------------------

The Coverage summary region is a scrollable
region which displays a hierarchically organized
summary of the coverage information.  Each line
in the summary represents a different item in the
hierarchy.  The hierarchy is organized into four
levels: Total Coverage, Directory, File, and
Function.

Each line is divided into columns of data.  The
leftmost column contains the name of the item.
The remaining columns present the coverage
information for the item.

Title Region
------------

The title region indicates which data is being
shown in the Coverage Summary Region.  The
columns displayed can be modified using the
"Select columns..." menu from the "View"
pulldown on the menu bar.

The title region for the leftmost column (which
contains the names of the items) indicates the
order by which the entries are sorted.  The sort
criterion can be modified using the "Select
sorting order" menu from the "View" pulldown
on the menu bar.

Expanding and Collapsing Items
------------------------------
Most items can appear in either an expanded or
collapsed state.  When an item is collapsed, its
summary is shown, but not the summary of any of
its children in the hierarchy.  When an item
is expanded the summary for each of its children
is also displayed.  You can also expand or
collapse sub-items.  Initially the Total
Coverage summary is expanded and the directories
within it are collapsed.
 
Lines that can be expanded or collapsed have an
Item Button displayed at the beginning of the
line. Click left on the item button to toggle the
state of the item.
 
The image of the Item button changes to reflect
the current state of the item. The item button
attached to an expanded item is a downward
pointing triangle. The item button attached to a
collapsed item is a right pointing triangle.

The function summary lines are different from
the other lines in two ways.  First, they will
have a Source button instead of the Item button.
This button looks like very tiny lines of code.
Clicking on this button will open the Annotated
Source window, positioned on the indicated
function.  This can also be used to reposition
the view in the Annotated Source window after
it is already opened to the desired file.

Second, functions have either an "X" or a "check"
mark under the columns for used and unused
functions instead of a number.  This makes it more
obvious which functions have been used and which
have not.
!
!*************************************************
!
@Topic@Running scripts@pcScriptOutput
@See also@Script Dialog@pcRunScript
PureCoverage scripts are installed in a
sub-directory (called "scripts") of the root
of the PureCoverage installation.  They may
be run from the shell or from within the
graphical interface.

To run them from within the graphical interface,
select the "Run script...." item from the "File"
pulldown on the Viewer's menu bar.

The name of the script can be selected from the
dialog, and arguments to the script may be typed
into the entry field to the right of the script
name.

The data inside the graphical interface will be
fed into the script and the output from the
script will be shown in a new window.

The output from the script will be filtered
and a summary will be appended to the output
before it is placed into the window.
!
!*************************************************
!
@Topic@Script Dialog@pcRunScript
@See also@Running Scripts@pcScriptOutput
@See also@Scripts Menu@pcScriptMenu
@See also@Script Arguments@pcScriptArgs
@Argument descriptions@pc_annotate@pc_annotate
@Argument descriptions@pc_below@pc_below
@Argument descriptions@pc_diff@pc_diff
@Argument descriptions@pc_email@pc_email
@Argument descriptions@pc_ssheet@pc_ssheet
@Argument descriptions@pc_summary@pc_summary
This dialog is used to run a script from within
the PureCoverage graphical interface.  Select the
name of the script to run from the menu, and enter
any arguments to the script in the entry field.
!
!*************************************************
!
@Topic@Scripts Menu@pcScriptMenu
@Argument descriptions@pc_annotate@pc_annotate
@Argument descriptions@pc_below@pc_below
@Argument descriptions@pc_diff@pc_diff
@Argument descriptions@pc_email@pc_email
@Argument descriptions@pc_ssheet@pc_ssheet
@Argument descriptions@pc_summary@pc_summary
This menu contains the list of PureCoverage
scripts which can be run from within the graphical
interface.  The default script is listed on the
menu button.  Other scripts can be selected by
holding down the left mouse button over the menu,
and sweeping across the popup until the desired
script is selected.
!
!*************************************************
!
@Topic@Script Arguments@pcScriptArgs
@Argument descriptions@pc_annotate@pc_annotate
@Argument descriptions@pc_below@pc_below
@Argument descriptions@pc_diff@pc_diff
@Argument descriptions@pc_email@pc_email
@Argument descriptions@pc_ssheet@pc_ssheet
@Argument descriptions@pc_summary@pc_summary
Enter the command-line arguments, if any, to the
script in this field.  Shell metacharacters (such
as "*" or "|") should be quoted or otherwise
escaped.
!
!*************************************************
!
@Topic@pc_annotate@pc_annotate
@See also@Annotated source@pcAnnotatedSource
This script produces annotate source listings
similar to those in the Annotated Source window.
Valid arguments are:

-type=line       (the default)
-type=block      To get basic-block annotations
                 instead of line annotations.

-file=<filename> To restrict the annotation to
                 a specific file.

It is OK to specify more than one -file= option.
!
!*************************************************
!
@Topic@pc_below@pc_below
This script produces a list of source files which
have line coverage below a certain percent.  To
control the percent, use the option
   -percent=<PCT>
The default value for <PCT> is 80; values must
be in the range 1 to 100.
!
!*************************************************
!
@Topic@pc_diff@pc_diff
This script reports on the differences between
two PureCoverage data sets (.pcv files).  When
run from the graphical interface, one of these
data sets must be the data loaded in the Viewer.
The other data set should be identified by giving
the name of the .pcv file it is in.

In the entry field, specify both "-" and the name
of the appropriate .pcv file, in either order.
Whichever one on the left will be considered the
"old" data, and the one on the right will be
considered the "new" data.

For example, you might use
     myprog.pcv.old -
or
     - myprog.pcv.new
!
!*************************************************
!
@Topic@pc_email@pc_email
@See also@pc_below@pc_below
This script reads the output of pc_below, then
examines the RCS entries for each file listed
in the report to determine who most recently
modified that file.  Those people are sent an
email indicating that the files have coverage
below the amount from the pc_below report.

The arguments to pc_email are the same as those
for pc_below.
!
!*************************************************
!
@Topic@pc_ssheet@pc_ssheet
This script converts data into tab-separated text
suitable for loading into a spreadsheet program.

There are no valid arguments for this script.
!
!*************************************************
!
@Topic@pc_summary@pc_summary
This script produces a coverage summary report,
listing the coverage for each function by file.
The only argument it accepts is:

-file=<filename> To select the files to be
                 reported.

It is OK to specify more than one -file= option.
!
!*************************************************
!
@Topic@File commands@pcFilePulldown
@See also@Viewer@pcOutline
@Related topics@Viewing commands@pcViewPulldown
@Related topics@Outline expanding/collapsing@pcActions
This pulldown menu gives you access to menus
controlling the reading and writing of files of
various kinds.  It also includes commands to
manage the windows.
!
!*************************************************
!
@Topic@View pulldown@pcViewPulldown
@See also@Viewer@pcOutline
@Related topics@File commands@pcFilePulldown
@Related topics@Outline expanding/collapsing@pcActions
This pulldown menu gives you access to menus
controlling how items are displayed in this
window.
!
!*************************************************
!
@Topic@Outline expanding/collapsing@pcActions
@See also@Viewer@pcOutline
@Related topics@File commands@pcFilePulldown
@Related topics@Viewing commands@pcViewPulldown
This pulldown menu contains buttons for expanding
and collapsing items in the outline region of the
Viewer window.  These commands are also available
via pop-up menu, by using the mouse to pop up the
menu.
!
!*************************************************
!
@Topic@Viewing Annotated Source@PC_ScrollToFunction
This button is used to scroll the annotated source
window to the coverage data for the function it is
located next to.  If necessary the window will be
created and if necessary the file shown in the
window will be changed.
!
!*************************************************
!
@Topic@Used Function@pcFunctionUsedGlyph
This icon is used to mark that a function was used
at least once.
!
!*************************************************
!
@Topic@Unused Function@pcFunctionUnusedGlyph
This icon is used to indicate functions which have
never been used.
!
!*************************************************
!
@Topic@Summary Information@pcOutlineGang
@See also@Viewer@pcOutline
These subwindows contain the summary information
for the .pcv file(s) loaded in this session of
the PureCoverage graphical interface.
!
!*************************************************
!
@Topic@Annotated Source@pcDetailGang
@See also@Annotated source@pcAnnotatedSource
These subwindows contain the source and 
annotations of that source for some file
covered using PureCoverage.
!
!*************************************************
!
@Topic@Next Unused@pcNextUnused
This button will cause the window to scroll to the
next block of unused code lines.  It will skip
over any unused lines in the current block.
!
!*************************************************
!
@Topic@Prev Unused@pcPrevUnused
This button will cause the window to scroll to the
previous block of unused code lines.  It will skip
over any unused lines in the current block.
!
!*************************************************
!
@Topic@Searching Functions@pcDetailSearch
These buttons and entry fields are used to
manipulate the portion of the program shown in the
annotated source window.
!
!*************************************************
!
@Topic@Go To Line@pcGoto
Enter the source line number you wish to see and
hit return.  The window will be positioned so that
the requested line is displayed in the window.
!
!*************************************************
!
@Topic@Find@pcFind
Enter a pattern to be searched for and hit return.
The next line containing the pattern will be show
in the window.  Patterns are case-sensitive.  The
character ? in a pattern matches any single
character.  The character * in a pattern will
match any series of characters.
!
!*************************************************
!
@Topic@Columns Shown in the Viewer Window@pcColumns
Items selected on this menu define which columns
will be shown in the Viewer window.
!
!*************************************************
!
@Topic@How Names are Shown@pcNames
This menu controls how names of things are
displayed in the columns of the Viewer window, and
also how they are truncated when they are wider
than the column reserved for them.
!
!*************************************************
!
@Topic@Information Message@pcInformationDialog
This dialog informs you of some fact it may be
important for you to know.
!
!*************************************************
!
@Topic@Warning Message@pcWarningDialog
This dialog warns you about some situation which
may require your attention.
!
!*************************************************
!
@Topic@Error Message@pcErrorDialog
This dialog informs you of some error which
occurred during operation of the program.
!
!*************************************************
!
@Topic@Is it OK to continue?@pcOKToContinue
Some noteworthy situation has occurred, as
described in the dialog box.  If you click CANCEL,
then the operation will be aborted.  If you click
OK, then the operation will proceed despite the
situation.
!
!*************************************************
!
@Topic@Enter Filename@pcFilenames
This dialog is used to select files.  You can use
the directory listing to identify files you wish
to select.  If you specify a directory name in
the filename field, then a listing of the the
contents of that directory will appear in the
directory listing region.

You may specify a regular expression in the
filename field (using ? for any single character
or a * for any sequence of characters), in which
case only files matching the pattern will be
listed in the directory listing section.
!
!*************************************************
!
@Topic@Help menu@@help_menu
!
The Help menu allows you to create the Help window
and select context-sensitive help.

On context
    After selecting this menu item, click anywhere
    in the PureCoverage windows to show help on
    that item in the Help window.

On version
    Show detailed version information in the Help
    window.

On license
    Show the complete text of the license
    agreement in the Help window. 

PureCoverage overview
    Show an overview of PureCoverage in the Help
    window. 

Release notes
    Show known issues in the Help window.

Bug report
    Show information on how to submit a bug
    report.
!

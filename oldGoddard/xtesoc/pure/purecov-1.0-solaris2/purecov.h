/*
 * Public declarations.
 */

#ifndef PURECOV_H
#define PURECOV_H

#if defined (c_plusplus) || defined (__cplusplus)
extern "C" {
#endif
 
/*
 * Provide prototypes for ANSI C and C++, but skip them for KR C.
 */
#ifndef PROTO
#if defined (c_plusplus) || defined (__cplusplus) || defined (__STDC__)
#define PROTO(a)    a
#else
#define PROTO(a)    ()
#endif
#endif

/*
 * return TRUE if purecov is running
 */
int purecov_is_running PROTO((void));

/*
 * Set the filename to be used to store the coverage counts.
 * Embedded %V %v and %p are turned into full_argv, argv, and pid respectively.
 */
int purecov_set_filename PROTO((const char *filename));

/*
 * Save counts data now, explicitly.
 * Counts are cleared to zero to avoid double counting.
 */
int purecov_save_data PROTO((void));

/*
 * Clear counts to zero, discarding collected coverage data.
 */
int purecov_clear_data PROTO((void));

/*
 * Disable the normal automatic save on exit, exec, etc.
 */
int purecov_disable_save PROTO((void));

/*
 * Re-enable the normal automatic save on exit, exec, etc.
 */
int purecov_enable_save PROTO((void));

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* PURECOV_H */

#include <stdio.h>

#if defined(__cplusplus)
extern "C" { int purify_is_running(void); }
#else
#if defined(_STDC_)
int purify_is_running(void);
#else
int purify_is_running();
#endif
#endif

#if defined(__cplusplus) || defined(_STDC_)
int main(int argc, const char **argv)
#else
int main(argc, argv) int argc; char **argv;
#endif
{
    if (purify_is_running()) {
	printf("Purify test\n");
    }
    printf("Hello, World\n");
    return 0;
}

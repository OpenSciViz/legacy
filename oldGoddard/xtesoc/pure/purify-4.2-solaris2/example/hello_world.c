/*
 * Copyright (c) 1992-1995 Rational Software Corp.
 *
 * This file contains proprietary information of Rational Software.  Use,
 * disclosure, or reproduction is prohibited except as permitted by express
 * written license agreement with Rational Software.
 *
 *
 * This is a test program used in Purifying Hello World.
 */
          
#include <stdio.h>
#include <malloc.h>
          
static char *helloWorld = "Hello, World";

main()
{
   char *mystr = malloc(strlen(helloWorld));
          
   strncpy(mystr, helloWorld, 12);
   printf("%s\n", mystr);
}



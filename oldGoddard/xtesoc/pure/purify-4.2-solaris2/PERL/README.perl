
This directory includes information relating to the GNU perl program
delivered with your Rational Software product, plus the binary itself. 

The perl program is licensed under the terms of the GNU General
Public License.  The terms of the GNU General Public License are in
the file COPYING.

The source to the program is available from Rational Software.  To obtain
a copy, you may use anonymous ftp to host ftp.pureatria.com, and copy the
file perl-4.0pl36.tar.Z.  If you do not have access to anonymous ftp,
but would still like a copy, please contact support@rational.com to arrange 
for a copy.  It is also possible to obtain versions of this program from
several GNU archive sites on the Internet listed below.


*************************************************************************
        HOW TO GET AND BUILD PERL
*************************************************************************

Note:- The retrieval information below to get Perl is constantly subject
to change, since changes are constantly being made to Perl, which leads
to a change in version numbers.
 
Step 1: FTP to a perl source site 
You can get Perl from one of the following sites:
 
        jpl-devvax.jpl.nasa.gov 128.149.1.143
        tut.cis.ohio-state.edu  128.146.8.60
 
Step 2: Get the sources  
cd to the pub/perl directory.
 
patches - this directory contains numbered patches to update any existing
	Perl sources to the latest patch level.
 
kits@<number> - the actual distribution. <number> indicates patch level.
	Get all the files in this directory, with the latest patch from 
	above mentioned patch directory.  (If there is more than 1 kits 
	directory, get the contents of latest one).
 
Step 3: Build the program
Uncompress and untar the file.
 
As per the README file: 

(a) Run the Configure program. Making it publicly accessible to everyone 
	from the /usr/local/bin directory is recommended.
 
(b) Do a "makedepend".
 
(c) Run "make".

**************************************************************************


/********************************************************************
 * Copyright (c) 1992-1995 Rational Software Corp.
 *
 * This file contains proprietary information of Rational Software.  Use,
 * disclosure, or reproduction is prohibited except as permitted by express
 * written license agreement with Rational Software.
 *
 *
 *  This is a test program to illustrate Purify's Watchpoints
 *
 ********************************************************************/
extern int errno;

int main()
{
    int i;

    printf("Note: &errno = 0x%x.\n", &errno);
    purify_watch(&errno);

    errno = 0;
    close(1000);

    exit(0);
}

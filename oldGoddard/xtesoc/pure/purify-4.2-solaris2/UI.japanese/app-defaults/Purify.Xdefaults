!**********************************************************************
! Purify viewer X resource settings.
!**********************************************************************
!
! Copyright (c) 1994,1995 Rational Software Corp.
!
! This file contains proprietary and confidential information and
! remains the unpublished property of Rational Software. Use, disclosure,
! or reproduction is prohibited except as permitted by express written
! license agreement with Rational Software.
!

! This file lists the X resources you can use to customize the Purify viewer.

! The first time you run the Purify viewer, the resource file
!     <purifyhome>/UI.japanese/app-defaults/Purify.Xdefaults
! will be copied to your home directory as $HOME/.purify.japanese.Xdefaults

! Each resource specification below reflects the default value used by
! the Purify viewer.  Lines that begin with a "!" (such as this one)
! are comment lines.
! NOTE: All resource specifications are commented out in this file.

! To customize the Purify viewer for your own needs, you can simply
! edit your .purify.japanese.Xdefaults file.  To do so, remove the "!"
! from the start of a resource line and change the value.

! NOTE: Resources specified in this file have higher precedence than
! those specified though the X resource mechanism (eg. in .Xdefaults).
! If you would rather use the standard X resource mechanism to
! customize the Purify viewer, do not modify this file.  Alternatively
! you can replace .purify.japanese.Xdefaults with a dummy file.  For
! example, in csh, use
!    rm ~/.purify.japanese.Xdefaults; touch ~/.purify.japanese.Xdefaults

!**********************************************************************
! To control whether or not the toolbar is shown
!
!    true == show the toolbar (default)
!   false == don't show the toolbar
!
! purify*viewToolbar.selected: true


!**********************************************************************
! To control whether or not the program controls are shown
!
!    true == show the program controls
!   false == don't show the program controls (default)
!
! purify*viewProgramControls.selected: false


!**********************************************************************
! This resource determines whether or not the scrollbars in the outline
! are dynamic.  A value of true means the scrollbars continuously update
! when outline objects are expanding or collapsing.  False means that
! the scrollbars don't update until the expand/collapse operation is 
! complete.
!
!    true == scrollbars dynamically update (default)
!   false == scrollbars don't dynamically update
!
! purify*dynamicScrollbars: true


!
!**********************************************************************
! To include portions of the call chain in the error summary line.
!
!   0 == no chain information displayed (default)
!  >0 == that many call chain entries shown
!        To get the whole call chain, specify a large number like 1000
!
! purify*summaryChainLength: 0


!**********************************************************************
! To enable/disable the raising of the purify viewer window when a new
! run starts
!
!    true == auto raise window (default)
!   false == don't auto raise window
!
! purify*raiseWindow: true


!**********************************************************************
! How many source lines to display when expanding a portion of a call
! chain.  This number will be adjusted so that there are the same
! number of lines before and after the line of interest.  The default
! is 7.
!
! purify*sourceLines: 7


!**********************************************************************
! Whether or not to pop up the confirmation dialog box when exiting
! when there are runs that have not been saved
!
!    true == ask for confirmation via dialog box
!   false == don't ask for confirmation, just exit (default)
!
! purify*confirmExit: false

!**********************************************************************
! Whether or not to pop up the confirmation dialog box when removing
! runs that have not been saved.
!
!    true == ask for confirmation via dialog box
!   false == don't ask for confirmation, just remove (default)
!
! purify*confirmRemove: false


!**********************************************************************
! A sample of enhanced strings.  The "Color" string means only apply
! these resources on non-monochrome displays.  This includes gray scale
! displays that have more than two colors.  Other valid display 
! types that can be used are:
!
!   Monochrome
!   StaticGray
!   GrayScale
!   StaticColor
!   PseudoColor
!   TrueColor
!   DirectColor
!
! This is a list of strings I want to enhance in the viewer display.  You
! should not attempt to enhance strings with the following characters:
!
!   * - asterisk
!   " - double quote
!   : - colon
!   . - period
!   ? - question mark
!
! Although the use of spaces within strings looks odd, it works fine.
!
!purify*Color*enhanceStrings: "ABR", "local variable", "[Suppressed]"
!
! Now for each of the strings listed above, I can specify an enhanceForeground
!    and an enhanceBackground color as follows:
!
!purify*Color*ABR.enhanceForeground: white
!purify*Color*ABR.enhanceBackground: red
!
!purify*Color*local variable.enhanceForeground: red
!
!purify*Color*[Suppressed].enhanceBackground: #d8d8d8


!**********************************************************************
! Command strings used in the "Program controls" buttons
!
! purify*makeCommandString:	pure_run make %D%v
! purify*runCommandString:	pure_run %D%v
! purify*debugCommandString:	pure_debug %D%v %%p
! purify*killCommandString:	kill %%p
! purify*editCommandString:	pure_edit FILENAME


!**********************************************************************
! Sample keyboard mnemonics and accelerators
!
!*file.mnemonics:		Meta	<Key>F: display_submenu()
!*view.mnemonics:		Meta	<Key>V: display_submenu()
!*actions.mnemonics:		Meta	<Key>A: display_submenu()
!*options.mnemonics:		Meta	<Key>O: display_submenu()
!
!*open.mnemonics:			<Key>O: fire()
!*open.accelerators:		Alt	<Key>O: fire()
!*saveAs.mnemonics:			<Key>A: fire()
!*saveAs.accelerators:		Alt	<Key>A: fire()
!*export.mnemonics:			<Key>E: fire()
!*export.accelerators:		Alt	<Key>E: fire()
!*print.mnemonics:			<Key>P: fire()
!*print.accelerators:		Alt	<Key>P: fire()
!*reread.mnemonics:			<Key>R: fire()
!*reread.accelerators:		Alt	<Key>R: fire()
!*exit.mnemonics:			<Key>x: fire()
!*exit.accelerators:		Alt	<Key>X: fire()
!
!*runtime.mnemonics:			<Key>R: fire()
!*suppressions.mnemonics:		<Key>S: fire()


!**********************************************************************
! A terminal emulator (xterm) will be started in response to clicking
! on one of the "Program control" buttons.  X resources can be set to
! modify the appearence of the terminal emulator.
!
! For example, if you wanted to enable scrollbars, make the window
! 60 lines tall, and set the background to pink, the resources would be:
!
! xterm
!xterm*scrollBar:	true
!xterm*geometry:		80x60
!xterm*background:	pink


!**********************************************************************
! Language -- this enables things like charEncodeType and fonts

! purify*language: ja_JP

!**********************************************************************
! Fonts


! The default ja_JP fontSet uses a fixed-width font, but we need helvetica.
! purify*ja_JP*fontSet: -*-helvetica-bold-r-normal--*-120-*-*-*-*-*-*, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0208.1983-0, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0201.1976-0

! None of these objects will have Japanese text in them, so use simple fontSets.
! purify*ja_JP*OI_multi_text.fontSet: fixed
! purify*ja_JP*OI_scroll_text.fontSet: fixed
! purify*ja_JP*OI_entry_field.fontSet: fixed
! purify*ja_JP*PS_OI_MultiText.fontSet: fixed
! purify*ja_JP*PS_OI_ScrollText.fontSet: fixed
! purify*ja_JP*PS_OI_ScrollTextGang.fontSet: fixed

! These will, however:
!   outline viewer text pane (inline text fragments may contain Kanji)
! purify*ja_JP*outline.@text.fontSet: fixed, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0208.1983-0, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0201.1976-0
!   help window text pane
! purify*ja_JP*OI_app_window.@text.fontSet: fixed, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0208.1983-0, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0201.1976-0
!   options dialog hint field
! purify*ja_JP*hint.fontSet: fixed, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0208.1983-0, -*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0201.1976-0

! The maximum length of the hint strings (number of characters; this
! is much smaller for Japanese than for English, because the
! characters are bigger).
! purify*ja_JP*hint.displayLength: 35

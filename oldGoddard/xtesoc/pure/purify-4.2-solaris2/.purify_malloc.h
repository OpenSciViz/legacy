/*
 * Declarations of unsupported public Purify functions.
 *
 * Copyright (c) 1992 Pure Software.
 *
 * This file contains proprietary and confidential information and
 * remains the unpublished property of Pure Software. Use, disclosure,
 * or reproduction is prohibited except as permitted by express written
 * license agreement with Pure Software.
 */

#ifndef PURIFY_MALLOC_H
#define PURIFY_MALLOC_H

/*
 * Provide prototypes for ANSI C and C++, but skip them for KR C.
 */

#if defined (__cplusplus) || defined (c_plusplus)
#define PROTO(a)	a
extern "C" {
#else
#if defined (__STDC__)
#define PROTO(a)	a
#else
#define PROTO(a)	()
#endif
#endif

/*
 * Convert user's size request into minimum required for a purify malloc block.
 */
int		purify_add_malloc_marker_size PROTO((int size));
int		purify_malloc_header_size PROTO((void));

/*
 * Interface to malloc, free, brk, sbrk avoiding most purify processing.
 */
char *		purify_malloc PROTO((int size));
int		purify_free PROTO((char *mem));
char *		purify_realloc PROTO((char *old, int size));
char *		purify_calloc PROTO((int nelem, int size));
int		purify_cfree PROTO((char *ptr));
char *		purify_memalign PROTO((int alignment, int len));
char *		purify_valloc PROTO((unsigned size));

int		purify_brk PROTO((char *limit));
char *		purify_sbrk PROTO((int incr));

/*
 * Take a block of memory of size > purify_add_malloc_marker_size(user_size)
 * allocated by your special purpose allocator and mark it up so that
 * Purify sees it as a block allocated by call to malloc from the function
 * skip_stack_frames above here.
 * Returns a pointer to user_size bytes available for the end user.
 */
char *		purify_mark_as_malloc PROTO((char *block, int user_size,
					     int skip_stack_frames));
/*
 * Take a block of user memory and mark it up so that Purify sees it
 * as freed by a call to free skip_stack_frames above here.
 * Returns larger enclosing block (as passed to purify_mark_as_malloc).
 * In case of error (e.g. not allocated by purify_mark_as_malloc), returns NULL.
 * In current implementation, there is nowhere to record the free-time stack,
 * so the skip_stack_frames paramter is ignored, and free'd blocks have
 * no additional data.
 */
char *		purify_mark_as_free PROTO((char *user_block,
					   int skip_stack_frames));

/*
 * Take an arbitrary block of memory and sets the freed flag in all
 * outstanding malloc-markers.
 * Returns the number of outstanding markers freed.
 */
int		purify_clear_malloc_markers PROTO((char *block, int size));

/*
 * Do "purify_describe" on all blocks in region from block for size bytes.
 */
void		purify_list_malloc_blocks PROTO((char *block, int size));

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* PURIFY_MALLOC_H */

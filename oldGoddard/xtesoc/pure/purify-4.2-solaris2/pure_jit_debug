#!/bin/sh
#
# Copyright (c) 1995-1996 Rational Software Corp.
#
# This file contains proprietary and confidential information and
# remains the unpublished property of Rational Software. Use, disclosure,
# or reproduction is prohibited except as permitted by express written1f
# license agreement with Rational Software.
#
# pure_jit_debugger
#   script executed to debug an application JustInTime
#
# Arguments:
#	$1 == the full pathname of the application
#	$2 == the process ID
#	$3 == debugger
#	remaining == debugger flags
#
# Exit status:
#	0 == debugger successfully attached to process,
#		and breakpoint is set at purify_stop_here
#	1 == process already running under debugger
#	2 == unable to attach to process
#	3 == debugger not found
#	4 == unknown debugger specified
#	5 == debugger did not attach in alloted time
#	7 == unknown error
#

#
# Figure out what kind of OS we are running on.
#
case "`uname -sr`" in
  HP-UX*)
    hpux=hpux ;;
  SunOS\ 4.*)
    sunos4=sunos4 ;;
  SunOS\ 5.*)
    sunos5=sunos5 ;;
  IRIX*\ [56]*)
    irix=irix ;;	
  *)
    echo "Don't know this OS" 1>&2 ;
    exit 1 ;;
esac

run_in_window ()
{
  # Runs a command in a terminal window.
  # Usage: run_in_window <title> <cmd> <args...>

  title="${1}"
  shift

  # Determine the type of terminal window and size to use.
  # Use PURE_TERM if defined, else default to something reasonable.
  if [ $hpux ]; then
    term=hpterm
  elif [ $irix ] ; then
    term=winterm
  else
    term=xterm
  fi
  term="${PURE_TERM:-"${term}"}"

  # Some terminal emulators accept the -name option.
  # You can use this option to change the X application name that
  # these emulators use to look up their default options.
  # Example:
  #   xterm -name pureXTerm ...
  # along with an X resource
  #   pureXTerm*background: banana

  termtype () {
    basename $1
  }
  case "`termtype $term`" in

    xterm)
      $term -title "$title" -e "$@" ;;

    winterm)
      $term -title "$title" -e "$@" ;;

    shelltool)
      $term -title "$title" "$@" ;;

    hpterm)
      $term -title "$title" -e "$@" ;;

    dtterm)
      $term -title "$title" -e "$@" ;;

    *)
      # Default to xterm syntax and hope for the best.
      $term -title "$title" -e "$@" ;;

  esac
}


#
# Exit status codes
#
SUCCESS=0
ALREADY_DEBUGGED=1
ATTACH_FAILED=2
NO_DEBUGGER=3
UNKNOWN_DEBUGGER=4
TIMEOUT=5
FAILURE=7


#
# Parse the arguments
#
if [ $# -lt 3 ]; then
  echo $0: not enough arguments -- missing debugger name\? 1>&2
  exit $FAILURE
fi
prog="`basename $1`"
prog_dir="`dirname $1`"
cd $prog_dir
if [ "$prog_dir" = "." ]; then
  prog_dir="`pwd`"
fi
shift

pid="$1"
shift

debugger="$1"
shift
# Remaining args are flags to debugger


#
# Check for the file /tmp/pure_jit_debug_running.$pid
# If that file already exists, it indicates that a
# JIT debugger is already attached to the program.
#
# When the instrumented process exits the viewer
# takes care to delete the file, so these files
# should not accumulate in /tmp.
#
jit_flag_file="/tmp/`basename $0`_running.$pid"
if [ -f $jit_flag_file ]; then
  exit $ALREADY_DEBUGGED
fi


# Make sure we cleanup after ourselves
cmd_file="/tmp/`basename $0`_cmd.$pid"
trap "rm -f $cmd_file" 0


# Window title
title="JIT debug ($prog)"


# Message to print in the debugger when successfully attached.
# Beware of using shell meta-variables in the string...
started_msg="==== JIT debugger successfully attached to program ===="


# Maximum number of seconds to wait for the debugger to start up.
maxWait=${PURE_JIT_DEBUG_MAX_WAIT:-60}


#
# Functions that generate debugger commands to execute.
#

gdb_commands ()
{
  echo "attach $pid"
  echo "break purify_stop_here"
  echo "shell touch $jit_flag_file"
  echo "echo ${started_msg}\\n"
  echo "continue"
}

dbx_commands ()
{
  echo "stop in purify_stop_here"
  echo "sh touch $jit_flag_file"
  echo "sh echo ${started_msg}"
  echo "cont"
}

xdb_commands ()
{
  echo "b purify_stop_here"
  echo "! touch $jit_flag_file"
  echo "# $started_msg"
  echo "c"
}

cvd_commands ()
{
  echo "stop in purify_stop_here"
  echo "sh touch $jit_flag_file"
  echo "sh echo $started_msg"
  echo "source $1"
  echo "continue"
}

dde_commands ()
{
  echo "breakpoint purify_stop_here"
  echo "shell touch $jit_flag_file"
  echo "shell echo $started_msg"
  echo "go"
}

emacs_gdb_commands ()
{
  echo "(gdb \"$prog_dir/$prog\")"
  echo "(gdb-call \"attach $pid\")"
  echo "(gdb-call \"break purify_stop_here\")"
  echo "(gdb-call \"shell touch $jit_flag_file\")"
  echo "(gdb-call \"echo $started_msg\")"
  echo "(gdb-call \"continue\")"
}

emacs_xdb_commands ()
{
  # There appears to be no way to start up xdb under emacs
  # and attach to an existing process!
  echo "(xdb \"$prog_dir/$prog\")"
  echo "(xdb-call \"b purify_stop_here\")"
  echo "(xdb-call \"! touch $jit_flag_file\")"
  echo "(xdb-call \"# $started_msg\")"
  echo "(xdb-call \"c\")"
}

# Start up the debugger, attaching to the process.

case "`basename $debugger`" in

  dbx)
    # There are many different versions of dbx...

    if [ "$sunos4" -o "$sunos5" ]; then
      #old_dbx=1
      if [ "$old_dbx" ]; then
	# Old versions of dbx use the -s option to specify
	# a file to read instead of the standard .dbxinit file.
	# Also, if you specify the file, it is executed before
	# the debugger attaches to the program.
	### STILL TO DO
	###   This doesn't work -- the debugger won't attach.
	echo "debug $prog $pid" > $cmd_file
	if [ -r ./.dbxinit ]; then
	  echo "source ./.dbxinit" >> $cmd_file
	elif [ -r $HOME/.dbxinit ]; then
	  echo "source $HOME/.dbxinit" >> $cmd_file
	fi
	dbx_commands >> $cmd_file
	run_in_window "$title" $debugger -s $cmd_file "$@" &
      else
	dbx_commands > $cmd_file
	run_in_window "$title" $debugger -c "source $cmd_file" "$@" $prog $pid &
      fi

    elif [ "$irix" ]; then
      # IRIX version of dbx uses the -c option to specify
      # a file to read instead of the standard .dbxinit file.
      if [ -r ./.dbxinit ]; then
	echo "playback input ./.dbxinit" >> $cmd_file
      elif [ -r $HOME/.dbxinit ]; then
	echo "playback input $HOME/.dbxinit" >> $cmd_file
      fi
      dbx_commands >> $cmd_file
      run_in_window "$title" $debugger -c  $cmd_file "$@" -p $pid &

    else
      exit $UNKNOWN_DEBUGGER
    fi ;;

  gdb)
    gdb_commands > $cmd_file
    run_in_window "$title" $debugger -x $cmd_file "$@" $prog &;;

  xdb)
    xdb_commands > $cmd_file
    # Convert xdb --> purify_xdb
    run_in_window "$title" `purify -printhomedir`/purify_xdb -P $pid -p $cmd_file "$@" $prog &;;

  purify_xdb)
    xdb_commands > $cmd_file
    run_in_window "$title" $debugger -P $pid -p $cmd_file "$@" $prog &;;

  debugger)
    # Sun interface to dbx
    dbx_commands > $cmd_file
    $debugger -title "$title" -c "source $cmd_file" "$@" $prog $pid &;;

  dbxtool)
    # Old Sun interface to dbx
    dbx_commands > $cmd_file
    $debugger -title "$title" -c "source $cmd_file" "$@" $prog $pid &;;

  cvd)
    # SGI WorkShop debugger
    # find old startup file = $CVDINIT or ./.cvdrc or ~/.cvdrc or /dev/null
    if [ -r ${CVDINIT:-/dev/null/nosuchfile} ]; then
        old_cmd_file=$CVDINIT
    elif [ -r ./.cvdrc ]; then
	old_cmd_file=./.cvdrc
    elif [ -r $HOME/.cvdrc ]; then
        old_cmd_file=$HOME/.cvdrc
    else
        old_cmd_file=/dev/null
    fi
    cvd_commands $old_cmd_file > $cmd_file
    env CVDINIT=$cmd_file $debugger -pid $pid "$@" &;;

  softdebug)
    ### SOFTDEBUG IS NOT TESTED ON HPUX
    # Convert softdebug --> purify_softdebug
    # The "-do" argument really works, and you can pass multiple
    # "-do" args and they're executed in order.
    echo "debug -attach $pid $prog" > $cmd_file
    dde_commands >> $cmd_file
    `purify -printhomedir`/purify_softdebug -do "input -from $cmd_file" &;;

  dde)
    ### DDE IS NOT TESTED ON HPUX
    # Convert dde --> purify_dde
    # The "-do" argument really works, and you can pass multiple
    # "-do" args and they're executed in order, so we don't
    # need to worry about calling purify_dde here!
    #
    # Must include the command to attach in the cmd_file,
    # since dde appears to ignore the command line arguments
    # if there is a "-do ..." argument.
    echo "debug -attach $pid $prog" > $cmd_file
    dde_commands >> $cmd_file
    `purify -printhomedir`/purify_dde -do "input -from $cmd_file" &;;

  *emacs*)
    # Hack: Emacs as a debugger means to run gdb (or xdb) in emacs.
    if [ $hpux ]; then
      # This does not work -- there is no way to start up
      # xdb in emacs attached to an existing process.
      emacs_xdb_commands > $cmd_file
    else
      emacs_gdb_commands > $cmd_file
    fi
    $debugger -l $cmd_file & ;;

  gnudoit)
    # Hack: gnudoit as a debugger means to run gdb (or xdb) via gnudoit.
    # gnudoit sends commands to an existing xemacs.
    echo "(new-frame)" > $cmd_file
    if [ $hpux ]; then
      # This does not work -- there is no way to start up
      # xdb in emacs attached to an existing process.
      emacs_xdb_commands >> $cmd_file
    else
      emacs_gdb_commands >> $cmd_file
    fi
    cat $cmd_file | $debugger > /dev/null & ;;

  *)
    echo unknown debugger $debugger 1>&2
    exit $UNKNOWN_DEBUGGER ;;

esac



#
# Wait for the debugger to attach.
#

echo "Waiting for JIT debugger to attach..." 1>&2
checkWait=2
while [ $maxWait -gt 0 ]; do
  if [ -f $jit_flag_file ]; then
    # Debugger is attached.  Wait another second or so, just to be safe.
    sleep 1
    echo "Debugger attached; continuing." 1>&2
    exit $SUCCESS
  fi
  sleep $checkWait
  maxWait=`expr $maxWait - $checkWait`
done
# Fall through if ran out of time.
echo "Giving up waiting for debugger." 1>&2
exit $TIMEOUT

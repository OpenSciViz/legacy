#! /bin/sh
# @(#)xmakemap	1.1 92/02/26 xmakemap SMI
#
#         Copyright (C) 1992  Sun Microsystems, Inc
#                   All rights reserved.
#           Notice of copyright on this source code
#           product does not indicate publication.
# 
# RESTRICTED RIGHTS LEGEND: Use, duplication, or disclosure by 
# the U.S. Government is subject to restrictions as set forth 
# in subparagraph (c)(1)(ii) of the Rights in Technical Data
# and Computer Software Clause at DFARS 252.227-7013 (Oct. 1988) 
# and FAR 52.227-19 (c) (June 1987).
#
# Sun Microsystems, Inc.
# 2550 Garcia Avenue
# Mountain View, California 94043
#

# xmakemap -- make a keyboard mapping to be used as input to xmodmap
#
# Usage: xmakemap > $HOME/.xmodmaprc
#
# Notes:
#    OpenWindows (i.e., an X server) must be running to use xmakemap.
#    Run xmakemap and edit $HOME/.xmodmaprc to customize it as desired
#    (read "Notes" in $HOME/.xmodmaprc).
#

echo '! File:
!    .xmodmaprc
!
! Goal:
!    Customize keyboard mappings
!
! Location (where to put this file):
!    $HOME/.xmodmaprc
!
! Usage:
!    xmodmap $HOME/.xmodmaprc
!
! Notes:
!  - If you would like your keytable to be customized each time OW
!    is run, you should place the appropriate command in $HOME/.xinitrc.
!    For example:
!
!    if [ -f $HOME/.xmodmaprc ]; then
!        xmodmap $HOME/.xmodmaprc
!    fi
!
!  - This file should be customized as desired, commenting out each
!    unneeded entry with '!'.  Note that all clients will be notified
!    when the keytable changes and the keytable will be distributed to
!    all clients upon their request.  Since distribution takes time for
!    each client, users should place the command *first* in .xinitrc
!    (before most clients are running) and should *not* place the
!    command in the background.
!
!  - Refer to man page for xmodmap(1) for more information.
!

!
! clear modifier groups
!

clear shift
clear lock
clear control
clear mod1
clear mod2
clear mod3
clear mod4
clear mod5

!
! set up keys
!
!       Key   Unshifted       Shifted         AltGraph        AltGraph-Shifted
!       ---   --------------- --------------- --------------- ----------------
'
xmodmap -pk | awk ' { \
    { \
	x = $1; \
        if (x >= 7 && x <= 132 && NF > 1) { \
	    $3 = substr($3, 2, length($3)-2); \
	    $5 = substr($5, 2, length($5)-2); \
	    $7 = substr($7, 2, length($7)-2); \
	    $9 = substr($9, 2, length($9)-2); \
	    printf("keycode %3s = %-15s %-15s %-15s %-15s\n",$1,$3,$5,$7,$9); \
	} \
    } \
} '

echo '
!
! set modifier groups
!
'
xmodmap | awk ' { \
    if ($1 == "shift") { \
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "lock") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "control") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "mod1") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "mod2") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "mod3") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "mod4") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
    if ($1 == "mod5") {
	printf "add %-7s = %s %s %s %s\n", $1, $2, $4, $6, $8 \
    } \
} ' 

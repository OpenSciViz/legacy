#pragma ident	"@(#)dga_externaldefs.h	1.6	93/11/12 SMI"

#ifndef	_DGA_EXTERNALDEFS_H
#define	_DGA_EXTERNALDEFS_H

/************************************************************
Copyright 1993 by Sun Microsystems, Inc. Mountain View, CA.

                    All Rights Reserved

Permission  to  use,  copy,  modify,  and  distribute   this
software  and  its documentation for any purpose and without
fee is hereby granted, provided that the above copyright no-
tice  appear  in all copies and that both that copyright no-
tice and this permission notice appear in  supporting  docu-
mentation,  and  that the names of Sun or MIT not be used in
advertising or publicity pertaining to distribution  of  the
software  without specific prior written permission. Sun and
M.I.T. make no representations about the suitability of this
software for any purpose. It is provided "as is" without any
express or implied warranty.

SUN DISCLAIMS ALL WARRANTIES WITH REGARD TO  THIS  SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FIT-
NESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL SUN BE  LI-
ABLE  FOR  ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,  DATA  OR
PROFITS,  WHETHER  IN  AN  ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION  WITH
THE USE OR PERFORMANCE OF THIS SOFTWARE.

********************************************************/

/*
** External defines for libdga.
*/

#define DGA_DRAW_WINDOW			0
#define DGA_DRAW_PIXMAP			1

#define DGA_MBACCESS_NONE		0
#define DGA_MBACCESS_SINGLEADDR		1
#define DGA_MBACCESS_MULTIADDR		2

#define DGA_SITE_NULL			0
#define DGA_SITE_DEVICE			1
#define DGA_SITE_SYSTEM			2

#define DGA_SITECHG_UNKNOWN		0
#define DGA_SITECHG_INITIAL		1
#define DGA_SITECHG_ZOMBIE		2
#define DGA_SITECHG_ALIAS		3
#define DGA_SITECHG_CACHE		4
#define DGA_SITECHG_MB			5

#define DGA_MBCHG_UNKNOWN		0
#define DGA_MBCHG_ACTIVATION		1
#define DGA_MBCHG_DEACTIVATION		2
#define DGA_MBCHG_REPLACEMENT	        3

#define DGA_VIS_UNOBSCURED		0
#define DGA_VIS_PARTIALLY_OBSCURED	1
#define DGA_VIS_FULLY_OBSCURED		2

#define DGA_MAX_GRABBABLE_BUFS       16

#endif /* _DGA_EXTERNALDEFS_H */

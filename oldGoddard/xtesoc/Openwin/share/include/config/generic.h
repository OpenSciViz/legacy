/*
**      @(#)generic.h	1.1 91/02/25
*/

/*
** Copyright (c) 1991 by Sun Microsystems, Inc.
*/

/*
** generic.h:
**	This file contains the #defines et al which are selected by a
**	given "build" configuration.
**
** This file should be included at the top of those source files that don't
** belong to a particular class of code, i.e., they don't belong
** to the "server" class of code.  This is the way in which the configuration
** of this "generic" class of code is selected.
**
**
**	Supported configurations
**		SUN4C
**		SUN4
**		SUN386i
**		SUN486i
**		AT386
*/

#ifndef _generic_h_
#define _generic_h_


/*
**	Special cases for Sun
*/

#ifdef SUN4C_NEWS
# define SUN4C
# define NEWS_ONLY
#endif

#ifdef SUN4_NEWS
# define SUN4
# define NEWS_ONLY
#endif

#ifdef SUN4C_EVQ
# define SUN4C
#endif

#ifdef SUN4_EVQ
# define SUN4
#endif

#ifdef SUN4C	/* SS1 + cg6/bw2/cg3 */
# define SUN
# define BIGENDIAN
# undef  LITTLEENDIAN
# define WINDOWGRABBER
# define XINPUT
#endif	/* SUN4C */

#ifdef SUN4	/* 4/110, 4/260, ... */
# define SUN
# define BIGENDIAN
# undef  LITTLEENDIAN
# define WINDOWGRABBER
# define XINPUT
#endif	/* SUN4 */

#ifdef SUN4SVR4
# ifndef SYSV
# define SYSV
# endif
# define SUN
# define BIGENDIAN
# undef  LITTLEENDIAN
# define WINDOWGRABBER
# ifdef notyet
# define XINPUT
# endif
#endif	/* SUN4 */

#if defined(SUN386I) || defined (SUN486I) /* Roadrunner 386 or 486 */
# define SUN
# undef  BIGENDIAN
# define LITTLEENDIAN
#endif

#ifdef AT386	/* AT&T System Vr4 & EGA/VGA */
#endif

/*
**	Global defines
**
** XNEWS		Always defined in Sun's window system source tree
** SH_SMALL		Shapes requires this
** SHARECANVAS		Canvas sharing
** WINDOWGRABBER	Sun Direct Framebuffer interface
** REFCOUNT_BY_FUNCTION
** MULTI
** TCPNODELAY
** NOUNIXSOCKETS	to turn off Unix domain sockets
** SUN_SECURE_RPC	to turn on secure RPC based authentication
** FOLIO
** I18NL3		Enable Internationalization level 3 messaging
*/

#ifndef XNEWS
#define XNEWS
#endif
#ifndef SH_SMALL
#define SH_SMALL
#endif
#ifndef MERGE
#define MERGE
#endif
#ifndef MULTI
#define MULTI
#endif

#ifdef SUN
#ifndef SHARECANVAS
#    define SHARECANVAS 
#endif
#ifndef OWGX
#    define OWGX
#endif
#ifndef OWGX_OPTO_DAMAGE
#    define OWGX_OPTO_DAMAGE
#endif
#ifndef FOLIO
#    define FOLIO
#endif
#    undef  NOUNIXSOCKETS
#ifndef TCPNODELAY
#    define TCPNODELAY
#endif
#ifndef TCPCONN
#    define TCPCONN
#endif
#ifndef UNIXCONN
#    define UNIXCONN
#endif
#if !defined(SYSV) && !defined(DNETCONN)
#    define DNETCONN
#endif
#ifndef SUN_SECURE_RPC
#    define SUN_SECURE_RPC
#endif
#ifndef SHAPEEXT
#    define SHAPEEXT		/* X11R4 shape extension */
#endif
#ifndef MULTIBUF
#    define MULTIBUF		/* X11R4 multibuffering extension */
#endif
#    undef  I18NL3		/* We have dgettext() and friends */


/*
 * The vendor string identifies the vendor responsible for the
 * server executable.
 */
#    define VENDOR_STRING "X11/NeWS - Sun Microsystems Inc."

#endif /* SUN */

#endif	/*_generic_h_*/


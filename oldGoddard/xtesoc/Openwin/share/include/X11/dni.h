/* @(#)dni.h 23.2 90/11/16 Copyr 1986 Sun Micro */

/*
 * Copyright (c) 1986 by Sun Microsystems, Inc.
 */
 
/*
 * dni.h - include file for SunLink DNI applications
 */
 
/*
 * DNI error codes that are returned in the external error variable errno
 */
#ifndef   NO_ERROR
#define   NO_ERROR         0	/* success, no error detected               */
#endif

#define		LOWEST_DNI_ERROR	170	/* Lowball Error Code */
#define		LOWEST_SESS_ERROR	170

#define   NOT_CONNECTED   170	/* Specified logical link does not exist    */
#define   PROC_ERROR      171	/* Network software processing error        */
#define   TRUNCATED       172	/* Data was truncated                       */
#define   BAD_LCN         173	/* Logical link number specified is invalid */
#define   BY_OBJECT       174	/* Logical link normally closed or rejected */
#define   NET_RESOUR      175	/* Insufficient resources at remote node    */
#define   NODE_NAME       176	/* Unrecognized node name                   */
#define   NODE_DOWN       177	/* Remote node is not accepting new links   */
#define   BAD_OBJECT      178	/* Specified remote object does not exist   */
#define   OBJ_NAME        179	/* Specified task name is invalid           */
#define   OBJ_BUSY        180	/* Insufficient resources at remote node    */
#define   MANAGEMENT      181	/* Link disconnected by network             */
#define   REMOTE_ABORT    182	/* Link aborted by remote program           */
#define   BAD_NAME        183	/* Specified node or device name is invalid */
#define   LOCAL_SHUT      184	/* Local node is not accepting new links    */
#define   ACCESS_CONT     185	/* Remote node rejected access information  */
#define   NODE_REAS       186	/* Insufficient resources at local node     */
#define   FAILED          187	/* Remote node failed to respond            */
#define   NODE_UNREACH    188	/* Remote node is unreachable               */
#define   ALREADY         189	/* Logical link identifier already in use   */

#define		HIGHEST_SESS_ERROR	189

#define   UNINIT          190	/* Network software is not initialized      */
#define   SEND_TIMEOUT    191	/* Controller didn't ack message from driver*/
#define   RECV_TIMEOUT    192	/* Timeout of expected recv from controller */
#define   XLN_SEND_ERR    193	/* Controller error on transmit             */
#define   XLN_RECV_ERR    194	/* Controller error on receive              */
#define   USER_ABORT      195	/* Program aborted by interactive user      */
#define   INV_ACCESS_MODE 196	/* Invalid access attempt on read or write  */
#define   NO_DATA_AVAIL   197	/* No data available                        */
#define   BAD_RECORD_STAT 198	/* Invalid value in SesRecord status field  */
#define   SIZE_TOO_BIG    199	/* Size of transfer exceeds maximum allowed */
#define   INVALID_SIZE    199	/* Size is negative or greater than max     */
#define   OUT_OF_SPACE    200	/* Out of space on controller               */
#define   COMM_FAIL       201	/* Unidentified controller failure          */
#define   BAD_COMMAND     202	/* Invalid command or ioctl function        */
#define   FLOW_CONTROL    203	/* Transmit failed, link is flow controlled */
#define   CL_DATA_AVAIL   206	/* Remote node closed link, data available  */
#define   INT_DATA        207	/* Buffer contains interrupt data           */
#define   BEG_OF_MESSAGE  208	/* Buffer contains the beginning of a msg   */
#define   MID_OF_MESSAGE  209	/* Buffer contains the middle of a msg      */
#define   END_OF_MESSAGE  210	/* Buffer contains the end of a msg         */
#define   COMPLETE        211	/* Buffer contains a complete message       */
#define   UNIDENTIFIED    215	/* Unknown error received from remote node  */
#define   UNKNOWN         215	/* Unknown error received from remote node  */
#define   DUPE_NODE_NAME  216	/* Duplicate node name detected             */
#define   DUPE_NODE_NUM   217	/* Duplicate node number detected           */
#define   NODE_NUM_REQUIRED 218	/* Node numbers are required for all nodes  */
#define	  NOT_SUPPORTED   219	/* Function not yet supported               */

#define		HIGHEST_DNI_ERROR 219	/* Highest DNI Error Code */


#define DNI_MAX_IO 32767	/* Max # of bytes to read/write */

/*
 * I/O options - use with SES_IO_TYPE ioctl
 */
typedef struct ses_io_type {
	short	io_flags;	/* io mode flags */
#define SES_IO_NBIO		1	/* non-blocking io */
#define SES_IO_RECORD_MODE	2	/* record mode */
#define SES_IO_ASYNCH_MODE	4	/* asynch io */
#define SES_IO_INT_MODE		8	/* interrupt data mode */
	short	io_io_signal;	/* signal to deliver when io avail */
	short	io_int_signal;	/* signal to deliver when int data avail */
} IoOptions;

/* Image16 Type Definition
 *
 * The Image16 typedef is used in the following open block,
 * session data, and interrupt data definitions.
 */
typedef struct image_16 {
	unsigned char	im_length;	/* Image length */
	char		im_data[16];	/* Image data */
} Image16;

/* Open Block Data
 *
 * The open_block structure contains the access control information
 * necessary for establishing a logical link. This structure must be
 * used in the SES_LINK_ACCESS and SES_GET_AI IOCTL function calls.
 */
#define   NODE_LEN     7
#define   TASK_LEN    17
#define   USER_LEN    20
#define   ACCT_LEN    20
#define   PASS_LEN    20
#define   DATA_LEN    16

typedef struct open_block {
	char    op_node_name[NODE_LEN];	/* System, or node, name    */
	char    op_task_name[TASK_LEN];	/* Task name                */
	short   op_object_nbr;	        /* Object number            */
	char    op_userid[USER_LEN];	/* User name                */
	char    op_account[ACCT_LEN];	/* User account number      */
	char    op_password[PASS_LEN];	/* User password            */
	Image16 op_opt_data;	        /* Optional data            */
} OpenBlock;

/* Session Data 
 *
 * The session_data structure is used for passing data between 
 * a requestor and a server process for the following IOCTL function
 * calls: SES_ACCEPT, SES_REJECT, SES_ABORT, and SES_DISCONNECT.
 * The reason for the action and the session data are application
 * dependent. 
 */
typedef struct session_data {
	short   sd_reason;	/* Reason for action taken  */
	Image16 sd_data;	/* Session data             */
} SessionData;


/* Session Status Values
 *
 * The SES_STATUS IOCTL function call will
 * return one of the following status values.
 */
#define  NO_LINK       0	/* no logical link with the given number */
#define  LINK_OPEN     1	/* logical link is open but not connected */
#define  LINK_CONNECT  2	/* logical link is open and connected */
#define  CLOSING       3	/* remote system closed link,
				   waiting for local close to complete */
#define  ABORTED       4	/* remote system aborted the link,
				   waiting for local close to complete */

/*
 * Used with SES_CHECK_DATA ioctl, mainly for speed to avoid calling select
 */
#define SES_RCV_DATA_AVAIL	1  /* session data is available to be read */
#define SES_INT_DATA_AVAIL	2  /* interrupt data is available to be read */

/* Interrupt Data 
 *
 * Interrupt data is an Image16 structure. This structure must
 * be used when passing interrupt data between a requestor and a
 * server process. It is used in both the SES_XMT_INT and 
 * the SES_RCV_INT IOCTL function calls.  The interrupt data is
 * application dependent.  
 */
typedef Image16 IntData;

typedef char TaskName[TASK_LEN];	/* mostly for SES_NAME_SERVER ioctl */

/*
 * DNI SESSION INTERFACE IOCTL REQUEST TYPES
 */
#define SES_GET_LINK	_IO(S, 101)		/* get a free link if avail */
#define	SES_IO_TYPE	_IOW(S, 102, struct ses_io_type) /* define IO opts */
#define	SES_LINK_ACCESS	_IOWR(S, 103, OpenBlock) /* active open */
#define	SES_NUM_SERVER	_IOW(S, 104, short)	/* by number passive open */
#define	SES_NAME_SERVER	_IOW(S, 105, TaskName)	/* by name passive open */
#define	SES_GET_AI	_IOR(S, 106, OpenBlock) /* get access control info */
#define	SES_ACCEPT	_IOW(S, 107, SessionData) /* accept the link */
#define	SES_REJECT	_IOW(S, 108, SessionData) /* reject the link */
#define SES_DISCONNECT	_IOW(S, 109, SessionData) /* disconnect logical link */
#define	SES_ABORT	_IOW(S, 110, SessionData) /* abort the logical link */
#define	SES_STATUS	_IOR(S, 111, int)	/* get link status */
#define	SES_RCV_INT	_IOR(S, 112, Image16)	/* receive interrupt data */
#define SES_XMT_INT	_IOW(S, 113, Image16)	/* transmit interrupt data */
#define SES_CHECK_DATA	_IOR(S, 114, int)	/* recv or int data avail? */
#define SES_GET_OPT_DATA _IOR(S, 115, Image16)	/* retrieve optional data */
#define SES_GET_MSG_TYPE _IOR(S, 116, short)	/* last msg type rcvd */

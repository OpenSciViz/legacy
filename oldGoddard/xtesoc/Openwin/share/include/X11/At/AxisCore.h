/*
 *      AxisCore.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Mon Jul 27 14:16:10 1992, patchlevel 2
 *                                      Added new resource XtNnumberWidth
 *                                      and function AtAxisGetNumberWidth()
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Thu Jul 22 14:49:27 1993, patchlevel 8
 *                                      New resources XtNdrawAxis, XtNdrawTics
 *                                      and XtNgridColor added.
 *                                      Minor changes.
 *      klin, Wed Sep  8 13:13:39 1993, patchlevel 8
 *                                      New resources XtNdrawAxis, XtNdrawTics,
 *                                      XtNgridColor, XtNaxisInOrigin,
 *                                      XtNnumbersInOrigin and XtNaxisMargin
 *                                      and all needed stuff added.
 *                                      New drawing proc AtAxisDraw().
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/08  AxisCore.h"
 */

/*

Copyright 1992,1993 by University of Paderborn
Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   The AtAxisCore class is the parent placeholder for for all Axis
 *   classes.  It handles maintaing and calling the callback lists, and
 *   knows how to draw the axis and labels from the stuff stored in the
 *   private part of the record.  It has no recalc proceedure so
 *   actually setting up these arrays has to be done by subclasses.
 *
 *   The range and position functions can be chained from subclass range
 *   and position functions.
 */

#ifndef _At_AxisCore_h
#define _At_AxisCore_h

#include <X11/At/At.h>
#include <X11/At/AtConverters.h>
#include <X11/At/Plot.h>
#include <X11/At/FontFamily.h>
#include <X11/At/Text.h>
#include <X11/At/Scale.h>

/* declare specific AtAxisCoreWidget class and instance datatypes */

typedef struct _AtAxisCoreClassRec*     AtAxisCoreWidgetClass;
typedef struct _AtAxisCoreRec*          AtAxisCoreWidget;

/* declare the class constant */

externalref WidgetClass  atAxisCoreWidgetClass;

#ifndef AtIsAxisCore
#define AtIsAxisCore(w)  XtIsSubclass(w, atAxisCoreWidgetClass)
#endif

/*
 *   Resources available to be set/fetched
 */

#define XtNmin "min"
#define XtNmax "max"
#define XtNticInterval "ticInterval"
#define XtNautoScale "autoScale"
#define XtNroundEndpoints "roundEndpoints"
#define XtNendpointPosition "endpointPosition"
#define XtNvertical "vertical"
#define XtNmirror "mirror"
#define XtNaxisMargin "axisMargin"
#define XtNrangeCallback "rangeCallback"
#define XtNdrawLabel "drawLabel"
#define XtNlabelSize "labelSize"
#define XtNlabelStyle "labelStyle"
#define XtNlabelColor "labelColor"
#define XtNnumberWidth "numberWidth"
#define XtNnumberSize "numberSize"
#define XtNnumberStyle "numberStyle"
#define XtNdrawAxis "drawAxis"
#define XtNnumberColor "numberColor"
#define XtNaxisInOrigin "axisInOrigin"
#define XtNnumbersInOrigin "numbersInOrigin"
#define XtNdrawTics "drawTics"
#define XtNticsInside "ticsInside"
#define XtNticsOutside "ticsOutside"
#define XtNticLength "ticLength"
#define XtNsubticLength "subticLength"
#define XtNdrawNumbers "drawNumbers"
#define XtNnumbersOutside "numbersOutside"
#define XtNdrawGrid "drawGrid"
#define XtNdrawSubgrid "drawSubgrid"
#define XtNdrawOrigin "drawOrigin"
#define XtNdrawFrame "drawFrame"
#define XtNaxisWidth "axisWidth"
#define XtNaxisColor "axisColor"
#define XtNgridColor "gridColor"
#define XtNticMultiplier "ticMultiplier"

#define XtCMin "Min"
#define XtCMax "Max"
#define XtCTicInterval "TicInterval"
#define XtCAutoScale "AutoScale"
#define XtCRoundEndpoints "RoundEndpoints"
#define XtCEndpointPosition "EndpointPosition"
#define XtCVertical "Vertical"
#define XtCMirror "Mirror"
#define XtCDrawLabel "DrawLabel"
#define XtCAxisMargin "AxisMargin"
#define XtCNumberWidth "NumberWidth"
#define XtCAxisInOrigin "AxisInOrigin"
#define XtCNumbersInOrigin "NumbersInOrigin"
#define XtCDrawAxis "DrawAxis"
#define XtCDrawTics "DrawTics"
#define XtCTicsInside "TicsInside"
#define XtCTicsOutside "TicsOutside"
#define XtCTicLength "TicLength"
#define XtCDrawNumbers "DrawNumbers"
#define XtCNumbersOutside "NumbersOutside"
#define XtCDrawGrid "DrawGrid"
#define XtCDrawSubgrid "DrawSubgrid"
#define XtCDrawOrigin "DrawOrigin"
#define XtCDrawFrame "DrawFrame"
#define XtCAxisWidth "AxisWidth"
#define XtCTicMultiplier "TicMultiplier"

/*
 *   These are the argument types for the callback lists:
 */

typedef struct {
     double     *minp, *maxp, *tic_intervalp;
     int        *max_widthp;
} AtAxisRangeArgs;


#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Call the member functions via this wrapper
 */

extern void AtAxisAskRange P((Widget, double *, double *, int));

/*
 * Returns TRUE if size change requires tic_interval to be recalculated
 * via a call to AtAxisAskRange (again)
 */

extern Boolean AtAxisSetPosition P((Widget, int, int, int, int, int));

/*
 *      Simple member functions
 */

extern AtScale *AtAxisGetScale P((Widget));
extern int AtAxisWidth P((Widget));
extern int AtAxisWidthPS P((Widget));
extern void AtAxisDrawPS P((Widget, FILE *, AtScale *, AtScale *, int, int, int, int, int));
extern void AtAxisGetBounds P((Widget, double *, double *));
extern AtTransform AtAxisGetTransform P((Widget));
extern int AtAxisGetNumberWidth P((Widget));
extern void AtAxisDraw P((Widget, Display *, Drawable, Region));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* _At_AxisCore_h */

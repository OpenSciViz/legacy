/*
 *      LinePlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Fri Jul 23 17:41:23 1993, patchlevel 8
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/07/23  LinePlot.h"
 */

/*

Copyright 1991 by Burdett, Buckeridge & Young Ltd.

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 * The timplest sort of ployt possible.  A line connecting the Y
 * values.  X values are integers without holes.
 */

#ifndef _At_LinePlot_h
#define _At_LinePlot_h

#include <X11/At/At.h>
#include <X11/At/SPlot.h>

/* declare specific AtLinePlotWidget class and instance datatypes */

typedef struct _AtLinePlotClassRec*     AtLinePlotWidgetClass;
typedef struct _AtLinePlotRec*          AtLinePlotWidget;

/* declare the class constant */

externalref WidgetClass atLinePlotWidgetClass;

#ifndef AtIsLinePlot
#define AtIsLinePlot(w)  XtIsSubclass(w, atLinePlotWidgetClass)
#endif

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Creation function
 */

extern Widget AtCreateLinePlot P((Widget, char *, Arg *, Cardinal));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 *   The member procedures. These ones are inherited from SPlot
 */

#define AtLinePlotAttachData(w,p,t,stride,start,num) \
     AtSPlotAttachData((Widget) w, p, t, stride, start, num)
#define AtLinePlotExtendData(w, n) AtSPlotExtendData((Widget) w, n)

#endif /* _At_LinePlot_h */

/*
 *      XYBarPlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 14:49:55 1993
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYBarPlot.h"
 */

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#ifndef _At_XYBarPlot_h
#define _At_XYBarPlot_h

#include <X11/At/At.h>
#include <X11/At/XYPlot.h>
#include <X11/At/BarPlot.h>

/*
 *   Additional resources to be set/fetched
 */

#define XtNbarPosition "barPosition"
#define XtNbarOrientation "barOrientation"
#define XtNbarOffset "barOffset"
#define XtNbarWidth "barWidth"
#define XtNcalcWidth "calcWidth"

#define XtCBarPosition "BarPosition"
#define XtCBarOrientation "BarOrientation"
#define XtCBarOffset "BarOffset"
#define XtCBarWidth "BarWidth"
#define XtCCalcWidth "CalcWidth"

#define XtRBarPosition "BarPosition"
#define XtRBarOrientation "BarOrientation"

/* Declare specific AtBarPlotWidget class and instance datatypes */

typedef struct _AtXYBarPlotClassRec*    AtXYBarPlotWidgetClass;
typedef struct _AtXYBarPlotRec*         AtXYBarPlotWidget;

/* Declare the class constant */

externalref WidgetClass atXYBarPlotWidgetClass;

#ifndef AtIsXYBarPlot
#define AtIsXYBarPlot(w) XtIsSubclass(w, atXYBarPlotWidgetClass)
#endif

/*
 *   The bar positions
 */

typedef enum {
     AtPositionRIGHT,         /* position right, default */
     AtPositionCENTER,        /* position center */
     AtPositionLEFT,          /* position left */
     AtPositionINVALID        /* invalid: use default */
} AtBarPosition;

/*
 *   The bar orientations
 */

typedef enum {
     AtOrientVERTICAL,        /* vertical orientation, default */
     AtOrientHORIZONTAL,      /* horizontal orientation */
     AtOrientINVALID          /* invalid: use default */
} AtBarOrientation;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Creation function
 */

extern Widget AtCreateXYBarPlot P((Widget, char *, Arg *, Cardinal));

/*
 *   Converter procedure
 */

extern void AtRegisterBarPositionConverter P((void));
extern void AtRegisterBarOrientationConverter P((void));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 *   The member procedures. These ones are inherited from XYPlot
 */

#define AtXYBarPlotAttachData(w, xp, xt, xstride, yp, yt, ystride, start, num) \
     AtXYPlotAttachData((Widget) w, xp, xt, xstride, \
					    yp, yt, ystride, start, num)
#define AtXYBarPlotExtendData(w, n) \
     AtXYPlotExtendData((Widget) w, n)

/*
 *   The member procedures for AtxxPoint data
 */

#define AtXYBarPlotAttachDoublePoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtDouble, sizeof(AtDoublePoint), \
	  (XtPointer) &(data)->y, AtDouble, sizeof(AtDoublePoint), \
	  start, num)

#define AtXYBarPlotAttachFloatPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtFloat, sizeof(AtFloatPoint), \
	  (XtPointer) &(data)->y, AtFloat, sizeof(AtFloatPoint), \
	  start, num)

#define AtXYBarPlotAttachIntPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtInt, sizeof(AtIntPoint), \
	  (XtPointer) &(data)->y, AtInt, sizeof(AtIntPoint), \
	  start, num)

#endif /* _At_XYBarPlot_h */

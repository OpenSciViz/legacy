/*
 *      XYPolygonPlotP.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Thu Sep  9 17:12:39 1993
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/09  XYPolygonPlotP.h"
 */

/*

Copyright 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

#ifndef _At_XYPolygonPlotP_h
#define _At_XYPolygonPlotP_h

#include <X11/At/At.h>
#include <X11/At/XYPlotP.h>
#include <X11/At/XYPolygonPlot.h>

typedef struct {
    int empty;
} AtXYPolygonPlotClassPart;

typedef struct _AtXYPolygonPlotClassRec {
     ObjectClassPart          object_class;
     AtPlotClassPart          plot_class;
     AtXYPlotClassPart        lplot_class;
     AtXYPolygonPlotClassPart pplot_class;
} AtXYPolygonPlotClassRec;

externalref AtXYPolygonPlotClassRec atXYPolygonPlotClassRec;

typedef struct {
     /* Resources */
     Boolean             do_fill;
     Boolean             do_outline;
     Pixel               fill_color;
     AtShading           shading;
     Boolean             screen_shade;

     /* Internal State */
     BoundingBox         saved_bb; /* Needed so the zeroMin resource works */
     Pixmap              shading_pixmap;
     GC                  fill_gc;
} AtXYPolygonPlotPart;

typedef struct _AtXYPolygonPlotRec {
     ObjectPart          object;
     AtPlotPart          plot;
     AtXYPlotPart        lplot;
     AtXYPolygonPlotPart pplot;
} AtXYPolygonPlotRec;


#endif /* _At_XYPolygonPlotP_h */

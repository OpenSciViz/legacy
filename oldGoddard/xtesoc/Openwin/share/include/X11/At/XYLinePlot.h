/*
 *      XYLinePlot.h
 *
 *      The AthenaTools Plotter Widget Set - Version 6.0
 *
 *      klin, Tue Jul  7 13:59:47 1992
 *      klin, Mon Jul 27 14:19:43 1992, patchlevel 2
 *                                      Plot types steps and bars added
 *      klin, Sat Aug 15 10:31:50 1992, patchlevel 4
 *                                      Changed <At/..> to <X11/At/..>.
 *      klin, Sat Sep 11 18:46:04 1993, patchlevel 8
 *                                      Line types SEGMENTS and SEGMENTPOINTS,
 *                                      mark styles DOT, CIRCLE, HORIZONTALBAR
 *                                      and VERTICALBAR added (by Jamie Mazer).
 *                                      Some minor changes.
 *
 *      SCCSid[] = "@(#) Plotter V6.0  93/09/11  XYLinePlot.h"
 */

/*

Copyright 1992, 1993 by University of Paderborn

All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of the firms, institutes
or employers of the authors not be used in advertising or publicity
pertaining to distribution of the software without specific, written
prior permission.

THE AUTHORS AND THEIR FIRMS, INSTITUTES OR EMPLOYERS DISCLAIM ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHORS AND THEIR FIRMS,
INSTITUTES OR EMPLOYERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

*/

/*
 *   The more complicated sort of plot.
 *   A line connecting the X and Y values attached to the axes.
 */

#ifndef _At_XYLinePlot_h
#define _At_XYLinePlot_h

#include <X11/At/At.h>
#include <X11/At/XYPlot.h>

/*
 *   Resources available to be set/fetched
 */

#define XtNplotLineType "plotLineType"
#define XtNplotLineStyle "plotLineStyle"
#define XtNplotMarkType "plotMarkType"

#define XtCPlotLineType "PlotLineType"
#define XtCPlotLineStyle "PlotLineStyle"
#define XtCPlotMarkType "PlotMarkType"

#define XtRPlotLineType "PlotLineType"
#define XtRPlotLineStyle "PlotLineStyle"
#define XtRPlotMarkType "PlotMarkType"

/* Declare specific AtXYLinePlotWidget class and instance datatypes */

typedef struct _AtXYLinePlotClassRec*   AtXYLinePlotWidgetClass;
typedef struct _AtXYLinePlotRec*        AtXYLinePlotWidget;

/* Declare the class constant */

externalref WidgetClass atXYLinePlotWidgetClass;

#ifndef AtIsXYLinePlot
#define AtIsXYLinePlot(w)   XtIsSubclass(w, atXYLinePlotWidgetClass)
#endif

/*
 *   The plot line types
 */

typedef enum {
     AtPlotLINES,             /* lines, default */
     AtPlotPOINTS,            /* points */
     AtPlotIMPULSES,          /* impulses */
     AtPlotSEGMENTS,          /* line segments */
     AtPlotSTEPS,             /* staircase */
     AtPlotBARS,              /* bars */
     AtPlotLINEPOINTS,        /* lines with points */
     AtPlotLINEIMPULSES,      /* lines with impulses */
     AtPlotSEGMENTPOINTS,     /* segments with points */
     AtPlotINVALID            /* invalid: use default */
} AtPlotLineType;

/*
 *   The plot line styles
 */

typedef enum {
     AtLineSOLID,             /* solid, default */
     AtLineDOTTED,            /* dotted 1 */
     AtLineDASHED,            /* dashed 2 */
     AtLineDOTDASHED,         /* dot dashed 1 */
     AtLineDOTTED2,           /* dotted 2 */
     AtLineDOTTED3,           /* dotted 3 */
     AtLineDOTTED4,           /* dotted 4 */
     AtLineDOTTED5,           /* dotted 5 */
     AtLineDASHED3,           /* dashed 3 */
     AtLineDASHED4,           /* dashed 4 */
     AtLineDASHED5,           /* dashed 5 */
     AtLineDOTDASHED2,        /* dot dashed 2 */
     AtLineINVALID            /* invalid: use default */
} AtPlotLineStyle;

/*
 *   The plot marker types
 */

typedef enum {
     AtMarkRECTANGLE,         /* rectangle, default */
     AtMarkDOT,               /* plain dots */
     AtMarkCIRCLE,            /* circle */
     AtMarkPLUS,              /* plus sign */
     AtMarkXMARK,             /* x-mark sign */
     AtMarkSTAR,              /* star */
     AtMarkDIAMOND,           /* diamond */
     AtMarkTRIANGLE1,         /* bottom triangle */
     AtMarkTRIANGLE2,         /* top triangle */
     AtMarkTRIANGLE3,         /* left triangle */
     AtMarkTRIANGLE4,         /* right triangle */
     AtMarkHORIZONTALBAR,     /* horizontal bar */
     AtMarkVERTICALBAR,       /* vertical bar */
     AtMarkINVALID            /* invalid: use default */
} AtPlotMarkType;

#define AtMarkHBAR AtMarkHORIZONTALBAR
#define AtMarkVBAR AtMarkVERTICALBAR

/*
 *   Some types for attaching data made easy
 */

typedef struct {
     double x;
     double y;
} AtDoublePoint;

typedef struct {
     float x;
     float y;
} AtFloatPoint;

typedef struct {
     int x;
     int y;
} AtIntPoint;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*
 *   Creation function
 */

extern Widget AtCreateXYLinePlot P((Widget, char *, Arg *, Cardinal));

/*
 *   Converter procedures
 */

extern void AtRegisterPlotLineTypeConverter P((void));
extern void AtRegisterPlotLineStyleConverter P((void));
extern void AtRegisterPlotMarkTypeConverter P((void));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 *   The member procedures. These ones are inherited from XYPlot
 */

#define AtXYLinePlotAttachData(w, xp, xt, xstride, yp, yt, ystride, start, num) \
     AtXYPlotAttachData((Widget) w, xp, xt, xstride, \
					    yp, yt, ystride, start, num)
#define AtXYLinePlotExtendData(w, n) \
     AtXYPlotExtendData((Widget) w, n)

/*
 *   The member procedures for AtxxPoint data
 */

#define AtXYLinePlotAttachDoublePoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtDouble, sizeof(AtDoublePoint), \
	  (XtPointer) &(data)->y, AtDouble, sizeof(AtDoublePoint), \
	  start, num)

#define AtXYLinePlotAttachFloatPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtFloat, sizeof(AtFloatPoint), \
	  (XtPointer) &(data)->y, AtFloat, sizeof(AtFloatPoint), \
	  start, num)

#define AtXYLinePlotAttachIntPoints(w, data, start, num) \
     AtXYPlotAttachData((Widget) w, \
	  (XtPointer) &(data)->x, AtInt, sizeof(AtIntPoint), \
	  (XtPointer) &(data)->y, AtInt, sizeof(AtIntPoint), \
	  start, num)

#endif /* _At_XYLinePlot_h */

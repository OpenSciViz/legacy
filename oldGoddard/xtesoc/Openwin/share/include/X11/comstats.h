/* @(#)comstats.h 7.1 90/06/28 Copyr 1986 Sun Micro */

/*
 * Copyright (c) 1986 by Sun Microsystems, Inc.
 */
 
/* 
 * The Statistics database is used by the CommUnity layers
 * to maintain counters for packets received under various
 * conditions
 */

typedef struct statistics_db {
    long time_when_zero;	/* time since the database was cleared */
    long circ_sec_since_zero;	/* time since the database was cleared  */
    long bad_format;		/* pkts discarded because bad routing hdr */
    long forwarded;		/* number of pkts forwarded */
    long user_msgs_sent;	/* nbr of user messages sent */
    long user_msgs_recv;	/* nbr of user messages received */
    long user_bytes_sent;	/* nbr of user bytes transmitted */
    long user_bytes_recv;	/* nbr of user bytes received */
    long total_bytes_sent;	/* total nbr of bytes transmitted */
    long total_bytes_recv;	/* total nbr of bytes received */

    /* Note: any additional counters added must be LONG word values  */

    /* the order of the remaining counters MUST not be changed       */

    long xmit_noerror;	/* number of frames transmitted without err */
    long xmit_aborted;	/* number of transmitted frames aborted */
    long sqe_test_err;	/* number of SQE test errors detected */
    long spare_field;	/* reserved for some future use... */
    long recv_noerror;	/* number of frames received without error  */
    long recv_align;	/* number of recv'd alignment errors */
    long recv_crc_err;	/* number of recv'd CRC errors */
    long frames_lost;	/* number of good frames not received */
} CommStats;

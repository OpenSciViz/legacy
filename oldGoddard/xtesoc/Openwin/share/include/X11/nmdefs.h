/*      @(#)nmdefs.h 7.1 90/06/28 SMI	*/

/*
 * Copyright (c) 1986 by Sun Microsystems, Inc.
 */

/* UNIX Network Management Definitions */

typedef int NmarsStatus;

#define MAX_ENTITY_SIZE 17
#define MAX_NODE 4096

#define NODE 0
#define LINE 1
#define LOGGING 2
#define CIRCUIT 3
#define MODULE 4
#define AREA 5
#define EXEC 6
#define LINK 7

/* Node States */
#define NM_STATE_ON          0
#define NM_STATE_OFF         1
#define NM_STATE_SHUT        2
#define NM_STATE_RESTRICTED  3
#define NM_STATE_REACHABLE   4
#define NM_STATE_UNREACHABLE 5

/* Node Types */
#define NM_ROUTING_III    0
#define NM_NONROUTING_III 1
#define NM_AREA           3
#define NM_ROUTING_IV     4
#define NM_NONROUTING_IV  5

/* Circuit Types */
#define NM_DDCMP_POINT    0
#define NM_ETHERNET_CIRCUIT       6

#define NM_NSP_VER_LEN            0x06
#define NM_NSP_FLD_LEN            0x01
#define NM_NSP_VER                0x04
#define NM_NSP_ECO                0x00
#define NM_NSP_USER_ECO           0x00

/* Network Management Response Codes */
#define NM_SUCCESS          0x01
#define NM_VERSION_GOOD     NM_SUCCESS
#define NM_UNREC_FUNC_OP    0xFF
#define NM_INVAL_MSG_FMT    0xFE
#define NM_PRIV_VIOL        0xFD
#define NM_OVRSZD_MGMT_CMD  0xFC
#define NM_MGMT_PGM_ERR     0xFB
#define NM_LOGIC_ERROR      NM_MGMT_PGM_ERR
#define NM_UNREC_PARM_TYPE  0xFA
#define NM_INCOMP_MGMT_VER  0xF9
#define NM_VERSION_SKEW     NM_INCOMP_MGMT_VER
#define NM_UNRECOG_COMP     0xF8	/* Entity type in error detail */
#define NM_RECORD_NOT_FOUND NM_UNRECOG_COMP
#define NM_INVAL_ID         0xF7
#define NM_LINE_COMM_ERR    0xF6
#define NM_COMP_IN_WRNG_ST  0xF5
#define NM_FILE_OPEN_ERROR  0xF3

/* Error Details defined as follows:
    Value    Keywords
    -----    --------
      0      Permanent Database
      6      Volatile Database    */

#define NM_INVAL_FILE_CONT  0xF2
#define NM_RESOURCE_ERROR   0xF1
#define NM_INVAL_PARM_VALUE 0xF0
#define NM_DUPLICATE_RECORD NM_INVAL_PARM_VALUE
#define NM_DUP_NODE_NUM     NM_INVAL_PARM_VALUE
#define NM_DUP_NODE_NAME    NM_INVAL_PARM_VALUE
#define NM_LINE_PROT_ERROR  0xEF
#define NM_FILE_IO_ERROR    0xEE
#define NM_REC_RD_FAILED    NM_FILE_IO_ERROR
#define NM_REC_WR_FAILED    NM_FILE_IO_ERROR
#define NM_VER_WRITE_FAILED NM_FILE_IO_ERROR
#define NM_VER_READ_FAILED  NM_FILE_IO_ERROR
#define NM_FSEEK_ERROR      NM_FILE_IO_ERROR
#define NM_CLOSE_ERR        NM_FILE_IO_ERROR
#define NM_IOCTL_ERROR      NM_FILE_IO_ERROR
#define NM_MIRROR_LINK_DIS  0xED
#define NM_OUT_OF_SPACE     0xEC
#define NM_ALLOC_FAIL       NM_OUT_OF_SPACE
#define NM_MIRROR_CONN_FAIL 0xEB
#define NM_PARM_NOT_APPLIC  0xEA
#define NM_PARM_VAL_LONG    0xE9
#define NM_HARDWARE_FAILURE 0xE8
#define NM_SYS_SPEC_FUNC_NS 0xE6
#define NM_INV_PARM_GRPING  0xE5
#define NM_BAD_LOOP_RESP    0xE4
#define NM_PARM_MISSING     0xE3
#define NM_DONE             0x80

typedef struct m_common {
    short mc_priv;		/* Privilige Level set by /dev/nm */
    char mc_ver[6];		/* Nmars version number */
    short mc_cmd;		/* FUNCTION REQUESTED */
    short mc_status;		/* Result of operation */
    short mc_et;		/* Entity Type : NODE,CIRC,LOGGING,LINE */
    unsigned short mc_nn;	/* Node Number */
    /* Entity Name - 1st byte contains length */
    char mc_en[MAX_ENTITY_SIZE];
    char mc_pad;		/* Padding */
    short mc_st_at;		/* Start at entity # */
    short mc_curr;		/* Current entity entity # */
} M_Com;

typedef struct des_rou_db {
    char drb_cm;		/* Coded Multiple Indicator */
    char drb_dt1;		/* Data Type - Designated Router Node Addr */
    unsigned short drb_na;	/* Data - Designated Router Node Address */
    char drb_dt2;		/* Data Type - Designated Router Node Name */
    char drb_nn[NODE_LENGTH +  1]; /* Data - Designated Router Node Name */
    unsigned char router_prio;	/* router priority */
    char padding[3];
} drb;

/* Why is this a copy of NodeTable?  Why not use the real thing? */
typedef struct ses_node_table_record {
    NodeTable *sntr_next;	/* Ptr. to next Session Node Table Record. */
    union {
	unsigned short sntr_snn;
	unsigned char sntr_cnn[2];
    } sntr_na;			/* Node Address */
    unsigned short sntr_adj;	/* Adjacency Flag */
    unsigned short sntr_circ;	/* Circuit Type */
    unsigned char sntr_circ_name[CIRCUIT_LENGTH + 1];	/* Circuit Name */
    unsigned short sntr_ht;	/* Hello Timer */
    unsigned short sntr_tout;	/* Routing Hello timeout value */
    NspNode *sntr_nrn_ptr;	/* Ptr. to NSP Remote Node Record */
    unsigned char sntr_nn[NODE_LENGTH + 1];	/* Node Name */
    unsigned char sntr_ntype;	/* Node Type */
} Ses_Node_Rec;

typedef struct {
    unsigned short nm_host_id;	/* host_id */
    unsigned short nm_node_id;	/* node_id */
    unsigned short nm_area_id;	/* area_id */
    unsigned short nm_madd;	/* Maximum Address */
    unsigned short nm_mbnr;	/* Maximum Broadcast NonRouters */
    unsigned char nm_state;	/* CommUnity_State - ON or OFF */
    unsigned char nm_ea[7];	/* ethernet_address - image format */
    unsigned char nm_ver[6];	/* Network Management Version # */
    unsigned char nm_rver[6];	/* Routing Version Number */
    unsigned char nm_marea;	/* Maximum Area */
    unsigned char nm_node_name[7]; /* Node Name string */
    unsigned char nm_id[33];	/* Identification */
} NODEparams;

typedef struct {
    Utiny nm_lct;		/* Local Circuit Type */
    Utiny nm_cn[17];		/* Single Ethernet Circuit Id */
    short nm_state;		/* circuit state */
    Ushort nm_hello_timer;	/* Hello timer */
} CIRCUITBLK;

#define MAX_CIRCUITS 1
typedef struct {
    Ushort nm_total_circuits;
    CIRCUITBLK nm_circuits[MAX_CIRCUITS];
} CIRCUITparams;

typedef struct {
    Utiny nm_lp;		/* Line Protocol */
    char nm_ln[17];		/* Single Ethernet Line Id */
    Utiny nm_state;		/* line state */
} LINEBLK;

#define MAX_LINES 1
typedef struct {
    Ushort nm_total_lines;
    LINEBLK nm_lines[MAX_LINES];
} LINEparams;

typedef struct {
    struct m_common m_snt_c;
    Ses_Node_Rec m_snt_d;
} M_Snt;

typedef struct {
    struct m_common m_ln_c;
    NODEparams m_ln_ld;
    Ses_Node_Rec m_ln_sd;
} M_LN_Snt;

typedef struct {
    struct m_common m_nln_c;
    struct nsp_params m_nln_d;
} M_Nln;

typedef struct {
    struct m_common m_nrn_c;
    struct nsp_node m_nrn_d;
} M_Nrn;

typedef struct {
    struct m_common m_drb_c;
    struct des_rou_db m_drb_d;
} M_Drb;

typedef struct {
    struct m_common m_cst_c;
    struct statistics_db m_cst_d;
} M_Cst;

typedef struct {
    struct m_common m_s_n_c;
    Ses_Node_Rec m_s_d;
    struct nsp_node m_n_d;
} M_Snt_Nrn;

typedef struct {
    struct m_common m_nm_c;
    NODEparams m_nm_d;
} M_Node;

typedef struct {
    struct m_common m_nm_c;
    LINEparams m_nm_d;
} M_Line;

typedef struct {
    struct m_common m_nm_c;
    CIRCUITparams m_nm_d;
} M_Circuit;

/*
 * DNI NETWORK MANAGEMENT IOCTL REQUESTS
 */
#define NMARS_DNI_SET_ADDRESS	_IOW(M, 101, int)
#define	NMARS_READ_SNT		_IOWR(M, 102, M_Snt)
#define	NMARS_READ_NLN		_IOWR(M, 103, M_Nln)
#define	NMARS_READ_NRN		_IOWR(M, 104, M_Nrn)
#define	NMARS_READ_DRB		_IOWR(M, 105, M_Drb)
#define	NMARS_READ_CST		_IOWR(M, 106, M_Cst)
#define	NMARS_WRITE_SNT		_IOWR(M, 107, M_Snt)
#define	NMARS_WRITE_NLN		_IOWR(M, 108, M_Nln)
#define	NMARS_WRITE_NRN		_IOWR(M, 109, M_Nrn)
#define	NMARS_WRITE_DRB		_IOWR(M, 110, M_Drb)
#define	NMARS_WRITE_CST		_IOWR(M, 111, M_Cst)
#define	NMARS_SNT_CREATE	_IOWR(M, 112, M_Com)
#define	NMARS_SNT_DELETE	_IOWR(M, 113, M_Com)
#define	NMARS_CLEAR_SNT		_IOWR(M, 114, M_Com)
#define	NMARS_CLEAR_NRN		_IOWR(M, 115, M_Com)
#define NMARS_READ_VER  	_IOWR(M, 116, M_Com)
#define NMARS_READ_SNT_NRN	_IOWR(M, 117, M_Snt_Nrn)
#define NMARS_READ_LN_SNT	_IOWR(M, 118, M_LN_Snt)
#define NMARS_DNI_ON	 	_IOWR(M, 119, M_Com)
#define NMARS_DNI_OFF		_IOWR(M, 120, M_Com)
#define NMARS_CHECK_INIT	_IOWR(M, 121, short)
#define NMARS_READ_NODE		_IOWR(M, 122, M_Node)
#define NMARS_WRITE_NODE	_IOWR(M, 123, M_Node)
#define NMARS_READ_LINE		_IOWR(M, 124, M_Line)
#define NMARS_WRITE_LINE	_IOWR(M, 125, M_Line)
#define NMARS_READ_CIRCUIT	_IOWR(M, 126, M_Circuit)
#define NMARS_WRITE_CIRCUIT	_IOWR(M, 127, M_Circuit)

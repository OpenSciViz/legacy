/*
 * Copyright (c) 1990 by Sun Microsystems, Inc.
 */

#ifndef	_NETDNI_DNI_H_
#define	_NETDNI_DNI_H_

#pragma ident	"@(#)dni.h	1.1	91/12/13	SMI"

#include	<sys/types.h>
#ifdef	_KERNEL
#include	<sys/stream.h>
#include	<sys/byteorder.h>
#endif	/*_KERNEL*/

/*
 * Used in t_open
 */
#define	DNIDEVICE	"/dev/nsp"

/*
 * port number <= 127 are special
 */
#define	DNPORT_PRIV	127

/*
 * Some constants we will need
 */
#define	DN_NODELEN	7		/* node name len */
#define	DN_TASKLEN	16		/* task name len */
#define	DN_ACCTLEN	40		/* acct. name len */
#define	DN_USERLEN	40		/* user name len */
#define	DN_PASSLEN	40		/* password length */
#define	DN_DATALEN	16		/* connect data size */

struct sockaddr_dn {
	short	sdn_family;		/* AF_DECnet */
	u_char	sdn_format;		/* format of object */
	u_char	sdn_port;		/* object # if used */
	u_short	sdn_gid;		/* group code */
	u_short	sdn_uid;		/* user code */
	u_char	sdn_namelen;		/* object name length */
	u_char	sdn_name[DN_TASKLEN];	/* object name */
	u_short	sdn_addr;		/* 16 bit DECnet addr */
};

/*
 * Values for sdn_format
 */
#define	DNADDR_FMT0	0		/* object number specified */
#define	DNADDR_FMT1	1		/* object name */
#define	DNADDR_FMT2	2		/* gid/uid and object name */

/*
 * The following structure is used for access info and data.
 * This structure is used as the "connect data" in a t_connect call
 * and returned to the user in a t_listen call.
 */
struct accessinfo_dn {
	u_short	dna_userl;
	u_char	dna_user[DN_USERLEN];	/* User ID string */
	u_short	dna_passl;
	u_char	dna_pass[DN_PASSLEN];	/* Password string */
	u_short	dna_acctl;
	u_char	dna_acct[DN_ACCTLEN];	/* Account string */
	u_short	dna_datal;
	u_char	dna_data[DN_DATALEN];	/* user data */
};

/*
 * Option specification structure.
 */
struct opthdr_dn {
	u_short	dopt_name;	/* names of the options */
	u_short	dopt_len;	/* length of next field */
	uint	dopt_value;
};

/*
 * Only option available for now, value of 1 to turn it on.
 */
#define	DN_RECORDMODE	1

#endif /*_NETDNI_DNI_H_*/

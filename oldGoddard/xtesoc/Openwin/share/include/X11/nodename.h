/*      @(#)nodename.h 7.1 90/06/28 SMI	*/

/*
 * Copyright (c) 1986 by Sun Microsystems, Inc.
 */
 
/* Nodename.h - structure for Community node data base */

#define HELLO_TIMER	15	/* send hello every HELLO_TIMER seconds */
#define NODE_TIMER	3	/* node timeout - make it BCT3MULT */
#define NODE_LENGTH	6
#define CIRCUIT_LENGTH	16

/* node database structure declaration */
typedef struct nsp_node {
    short           nsp_sslz;	/* Seconds since last zeroed */
    short           active;	/* number of links to this node */
    short           delay;	/* estimated round trip delay */
    long            ubytxmt;	/* total user data bytes transmitted */
    long            ubytrcv;	/* total user data bytes recieved */
    long            umsgxmt;	/* total user messages received */
    long            umsgrcv;	/* total user messages transmitted */
    short           conxmt;	/* connect initiate messages transmitted */
    short           conrcv;	/* connect initiate messages recieved */
    short           conrej;	/* connect reject messages transmitted */
    short           timeout;	/* number of timeouts */
} NspNode;

typedef struct node_table {
    struct node_table *not_fwd;	/* Ptr. to next in list */
    unsigned short  node_num;	/* Node Number */
    short           adjacent;	/* Boolean: Is this node adjacent? */
    short           circuit;	/* Circuit Type ?? */
    unsigned char   circ_name[CIRCUIT_LENGTH + 1];  /* Circuit Name */
    short           hello_timer;/* Adjacent Node Listen Timer */
    short           timeout;	/* Node Listen Timer countdown */
    NspNode         *nodeptr;	/* Ptr. to NSP adjacent node record */
    char            not_name[NODE_LENGTH + 1];	/* Node Name */
    char            node_type;	/* Adjacent Node Type */
} NodeTable;

/* nsp internal data base - also read and reset by network management */
struct nsp_params {
    unsigned short  nsp_seg_size;	/* nsp's segment size */
    short           nsp_delay;		/* delay factor */
    short           nsp_weight;		/* round trip delay est. factor */
    short           nsp_retrans;	/* retransmit threshold */
    short           nsp_inactive;	/* inactivity timer value */
    short           nsp_ports;		/* maximum number of ports allowed */
    short           nsp_acktimeout;	/* ack msg timer in ms */
    short           nsp_flow_ctrl;	/* flow control type we're using */
    char            nsp_v[6];		/* NSP version number */
};

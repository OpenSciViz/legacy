/*
 * ttsample1 -- dynamic pattern, procedural notification
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <string.h>

#include <desktop/tt_c.h>

Xv_opaque base_frame, controls, slider, gauge, button;
char *my_procid;

void	broadcast_value();
void	receive_tt_message();
void	create_ui_components();


void
main(argc, argv)
	int		argc;
	char		**argv;
{
	int ttfd;
	Tt_pattern pat;

	/*
	 * Initialize XView and create ui components
	 */
	xv_init(XV_INIT_ARGC_PTR_ARGV, &argc, argv, 0);
	create_ui_components();

	/*
	 * Initialize ToolTalk, using the initial default session, and
	 * obtain the file descriptor that will become active whenever
	 * ToolTalk has a message for this process.
	 */

	my_procid = tt_open();
	ttfd = tt_fd();

	/*
	 * Arrange for XView to call receive_tt_message when the ToolTalk
	 * file descriptor becomes active.
	 */

	notify_set_input_func(base_frame,
			      (Notify_func)receive_tt_message,
			      ttfd);
	
	/*
	 * Create and register a pattern so ToolTalk knows we are interested
	 * in "ttsample1_value" messages within the session we join.
	 */

	pat = tt_pattern_create();
	tt_pattern_category_set(pat, TT_OBSERVE);
	tt_pattern_scope_add(pat, TT_SESSION);
	tt_pattern_op_add(pat, "ttsample1_value");
	tt_pattern_register(pat);

	/*
	 * Join the default session
	 */

	tt_session_join(tt_default_session());

	/*
	 * Turn control over to XView.
	 */
	xv_main_loop(base_frame);
	
	/*
	 * Before leaving, allow ToolTalk to clean up.
	 */
	tt_close();

	exit(0);
}


/*
 * When the button is pressed, broadcast the new slider value.
 */
void
broadcast_value(item, event)
	Panel_item	item;
	Event		*event;
{
	Tt_message msg_out;

	/*
	 * Create and send a ToolTalk notice message
	 * ttsample1_value(in int <new value)
	 */

	msg_out = tt_pnotice_create(TT_SESSION, "ttsample1_value");
	tt_message_arg_add(msg_out, TT_IN, "integer", NULL);
	tt_message_arg_ival_set(msg_out, 0, (int)xv_get(slider, PANEL_VALUE));
	tt_message_send(msg_out);

	/*
	 * Since this message is a notice, we don't expect a reply, so
	 * there's no reason to keep a handle for the message.
	 */

	tt_message_destroy(msg_out);
}

/*
 * When a ToolTalk message is available, receive it; if it's a
 * ttsample1_value message, update the gauge with the new value.
 */
void
receive_tt_message()
{
	Tt_message msg_in;
	int mark;
	int val_in;
	char *op;
	Tt_status err;

	msg_in = tt_message_receive();

	/*
	 * It's possible that the file descriptor would become active
	 * even though ToolTalk doesn't really have a message for us.
	 * The returned message handle is NULL in this case.
	 */

	if (msg_in == NULL) return;


	/*
	 * Get a storage mark so we can easily free all the data
	 * ToolTalk returns to us.
	 */

	mark = tt_mark();

	op = tt_message_op(msg_in);
	err = tt_ptr_error(op);
	if (err > TT_WRN_LAST) {
		printf( "tt_message_op(): %s\n", tt_status_message(err));
	} else if (op != 0) {
		if (0==strcmp("ttsample1_value", tt_message_op(msg_in))) {
			tt_message_arg_ival(msg_in, 0, &val_in);
			xv_set(gauge, PANEL_VALUE, val_in, NULL);
		}
	}

	tt_message_destroy(msg_in);
	tt_release(mark);
	return;
}
	    
/*
 * Straight XView calls for creating the ui elements.  No
 * ToolTalk-specific code here.
 */
void
create_ui_components()
{
	base_frame = xv_create(NULL, FRAME,
		XV_WIDTH, 269,
		XV_HEIGHT, 104,
		XV_LABEL, "ToolTalk Sample 1",
		FRAME_SHOW_RESIZE_CORNER, FALSE,
		NULL);
	controls = xv_create(base_frame, PANEL,
		XV_X, 0, XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		WIN_BORDER, FALSE,
		NULL);
	slider = xv_create(controls, PANEL_SLIDER,
		XV_X, 51, XV_Y, 16,
		XV_WIDTH, 158, XV_HEIGHT, 14,
		PANEL_VALUE_X, 102, PANEL_VALUE_Y, 16,
		PANEL_LABEL_X, 52,
		PANEL_SLIDER_WIDTH, 100,
		PANEL_LABEL_STRING, "Send:",
		PANEL_SLIDER_END_BOXES, FALSE,
		PANEL_SHOW_RANGE, FALSE,
		PANEL_SHOW_VALUE, FALSE,
		PANEL_MIN_VALUE, 0,
		PANEL_MAX_VALUE, 100,
		PANEL_TICKS, 0,
		NULL);
	gauge = xv_create(controls, PANEL_GAUGE,
		XV_X, 23, XV_Y, 48,
		XV_WIDTH, 189, XV_HEIGHT, 24,
		PANEL_VALUE_X, 96, PANEL_VALUE_Y, 48,
		PANEL_GAUGE_WIDTH, 100,
		PANEL_LABEL_STRING, "Received:",
		PANEL_SHOW_RANGE, TRUE,
		PANEL_MIN_VALUE, 0,
		PANEL_MAX_VALUE, 100,
		PANEL_TICKS, 0,
		NULL);
	button = xv_create(controls, PANEL_BUTTON,
		XV_X, 96, XV_Y, 76,
		XV_WIDTH, 76, XV_HEIGHT, 19,
		PANEL_LABEL_STRING, "Broadcast",
		PANEL_NOTIFY_PROC, broadcast_value,
		NULL);
}

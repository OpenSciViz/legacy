/*
 * arg_edit.h
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#ifndef	__ARG_LIST_H__
#define	__ARG_LIST_H__

#define	CHAR_SET(_ip_, _i_, _v_)						\
	if(item == NULL)						\
	{								\
		xv_set(_ip_->_i_, PANEL_VALUE, "", NULL);			\
	}								\
	else								\
	{								\
		xv_set(_ip_->_i_, PANEL_VALUE, item->_v_, NULL);		\
	}

/* for ENUM_SET: _d_ should be the default enum value
                 _o_ should be minus of the lowest numbered enum value used.
		 (hence _d_ >= _o_) 
*/
#define	ENUM_SET(_ip_, _i_, _v_, _d_, _o_)					\
	if(item == NULL)						\
	{								\
		xv_set(_ip_->_i_, PANEL_VALUE, _d_+_o_, NULL);		\
	}								\
	else								\
	{								\
		xv_set(_ip_->_i_, PANEL_VALUE, (item->_v_)+_o_, NULL);	\
	}

#define	CHAR_GET(_ip_, _i_, _v_)						\
	if(item->_v_ != NULL)						\
	{								\
		free(item->_v_);					\
	}								\
	if(_ip_ != NULL)							\
	{								\
		char	*c = (char *)xv_get(_ip_->_i_, PANEL_VALUE);	\
		if(*c)							\
		{							\
			item->_v_ = strdup(c);				\
		}							\
		else							\
		{							\
			item->_v_ = (char *)malloc(1);			\
			*item->_v_ = '\0';				\
		}							\
	}

#define	ENUM_GET(_ip_, _i_, _v_)						\
	if(_ip_ != NULL)							\
		item->_v_ = _v_##_name(xv_get(_ip_->_i_,\
					      PANEL_CHOICE_STRING,\
					      xv_get(_ip_->_i_, PANEL_VALUE)));


typedef enum kindofadd {
	ADD	= 0,
        BADD	= 1,
        IADD	= 2} KindOfAdd;


typedef	struct	arg_list
{
	char		*label;
	Tt_mode		mode;
	char		*slotname;
	char		*type;
	char		*value;
	KindOfAdd	add_kind;
} Arg_list;

typedef struct
{
	char		*label;
	Tt_address	address;
	Tt_class	class;
	Tt_scope	scope;
	char		*session;
	char		*file;
	char		*object;
	char		*op;
	char		*otype;
	char		*handler;
	char		*handler_ptype;
	Tt_disposition	disposition;
	char		*sender_ptype;
	int		arg_count;
	Arg_list	**list;
	Arg_list	**cntxtlist;
} Msg_list;

#endif __ARG_LIST_H__

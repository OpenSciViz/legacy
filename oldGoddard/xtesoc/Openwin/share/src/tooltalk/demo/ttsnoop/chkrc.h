/* 
 * chkrc.h
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#ifndef _CHKRC_H
#define _CHKRC_H

#include <desktop/tt_c.h>

#define stringize(s) #s
#define chkrc(f) 						\
	(							\
		(void)(						\
			(TT_OK == (ttrc=(f))) ||		\
		        perror_tt(ttrc,				\
				  stringize(f),			\
				  __FILE__, __LINE__)		\
		)						\
	)
static	Tt_status	ttrc;

#endif

/*
 * display_handlers.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"


/*
 * Notify callback function for `apply_button'.
 */
void
display_display_popup_apply_button_notify_callback(Panel_item item, Event *event)
{
	display_display_popup_objects *ip = (display_display_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		set_display_value(ip);
	}
}

/*
 * Notify callback function for `reset_button'.
 */
void
display_display_popup_reset_button_notify_callback(Panel_item item, Event *event)
{
	display_display_popup_objects *ip = (display_display_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		reset_display_value(ip);
	}
}

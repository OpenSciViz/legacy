/*  
 * tt_enum_xlate.h
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#ifndef	TT_ENUM_XLATE_H
#define TT_ENUM_XLATE_H

#include <desktop/tt_c.h>
#include "arg_edit.h"

typedef	struct
{
	Tt_scope	val;
	char		name[20];
} Scope_xlate;

typedef	struct
{
	Tt_class	val;
	char		name[20];
} Class_xlate;

typedef	struct
{
	Tt_disposition	val;
	char		name[20];
} Disposition_xlate;

typedef	struct
{
	Tt_mode		val;
	char		name[20];
} Mode_xlate;

typedef	struct
{
	KindOfAdd	val;
	char		name[10];
} Add_Kind_xlate;


typedef	struct
{
	Tt_address	val;
	char		name[20];
} Address_xlate;

typedef	struct
{
	Tt_state	val;
	char		name[20];
} State_xlate;

typedef	struct
{
	Tt_category	val;
	char		name[20];
} Category_xlate;

Tt_address	address_name();
char		*address_val();
Tt_class	class_name();
char		*class_val();
Tt_disposition	disposition_name();
char		*disposition_val();
Tt_mode		mode_name();
char		*mode_val();
KindOfAdd	add_kind_name();
char*		add_kind_val();
Tt_scope	scope_name();
char		*scope_val();
Tt_state	state_name();
char		*state_val();
Tt_category	category_name();
char		*category_val();

#endif

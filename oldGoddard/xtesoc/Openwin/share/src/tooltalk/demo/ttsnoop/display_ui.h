#ifndef	display_HEADER
#define	display_HEADER

/*
 * display_ui.h - User interface object and function declarations.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

extern Attr_attribute	INSTANCE;


typedef struct {
	Xv_opaque	display_popup;
	Xv_opaque	panel;
	Xv_opaque	display_setting;
	Xv_opaque	apply_button;
	Xv_opaque	reset_button;
} display_display_popup_objects;

extern display_display_popup_objects	*display_display_popup_objects_initialize(display_display_popup_objects *, Xv_opaque);

extern Xv_opaque	display_display_popup_display_popup_create(display_display_popup_objects *, Xv_opaque);
extern Xv_opaque	display_display_popup_panel_create(display_display_popup_objects *, Xv_opaque);
extern Xv_opaque	display_display_popup_display_setting_create(display_display_popup_objects *, Xv_opaque);
extern Xv_opaque	display_display_popup_apply_button_create(display_display_popup_objects *, Xv_opaque);
extern Xv_opaque	display_display_popup_reset_button_create(display_display_popup_objects *, Xv_opaque);
#endif

/*
 * display_ui.c - User interface object initialization functions.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/canvas.h>
#include <xview/panel.h>
#include <xview/scrollbar.h>
#include <xview/svrimage.h>
#include <xview/termsw.h>
#include <xview/text.h>
#include <xview/tty.h>
#include <xview/xv_xrect.h>
#include "display_ui.h"

/*
 * Initialize an instance of object `display_popup'.
 */
display_display_popup_objects *
display_display_popup_objects_initialize(display_display_popup_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (display_display_popup_objects *) calloc(1, sizeof (display_display_popup_objects))))
		return (display_display_popup_objects *) NULL;
	if (!ip->display_popup)
		ip->display_popup = display_display_popup_display_popup_create(ip, owner);
	if (!ip->panel)
		ip->panel = display_display_popup_panel_create(ip, ip->display_popup);
	if (!ip->display_setting)
		ip->display_setting = display_display_popup_display_setting_create(ip, ip->panel);
	if (!ip->apply_button)
		ip->apply_button = display_display_popup_apply_button_create(ip, ip->panel);
	if (!ip->reset_button)
		ip->reset_button = display_display_popup_reset_button_create(ip, ip->panel);
	return ip;
}

/*
 * Create object `display_popup' in the specified instance.
 */
Xv_opaque
display_display_popup_display_popup_create(display_display_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, FRAME_CMD,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 281,
		XV_HEIGHT, 389,
		XV_LABEL, "Display Values",
		FRAME_SHOW_FOOTER, TRUE,
		FRAME_SHOW_RESIZE_CORNER, TRUE,
		FRAME_CMD_PUSHPIN_IN, TRUE,
		NULL);
	xv_set(xv_get(obj, FRAME_CMD_PANEL), WIN_SHOW, FALSE, NULL);
	return obj;
}

/*
 * Create object `panel' in the specified instance.
 */
Xv_opaque
display_display_popup_panel_create(display_display_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "display:panel",
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		WIN_BORDER, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `display_setting' in the specified instance.
 */
Xv_opaque
display_display_popup_display_setting_create(display_display_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE, PANEL_FEEDBACK, PANEL_MARKED,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "display:display_setting",
		XV_X, 8,
		XV_Y, 8,
		PANEL_CHOICE_NCOLS, 2,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_CHOICE_STRINGS,
			"Tt_address",
			"Handler",
			"Handler ptype",
			"Object",
			"Otype",
			"Op",
			"Opnum",
			"Tt_scope",
			"Session",
			"File Name",
			"Tt_category",
			"Tt_class",
			"Tt_disposition",
			"Tt_state",
			"Tt_status",
			"Status string",
			"Sender",
			"Sender pthpe",
			"uid",
			"gid",
			"Tt_mode",
			"Argument Type",
			"Argument Value",
			"Contexts",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `apply_button' in the specified instance.
 */
Xv_opaque
display_display_popup_apply_button_create(display_display_popup_objects *ip, Xv_opaque owner)
{
	extern void		display_display_popup_apply_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "display:apply_button",
		XV_X, 81,
		XV_Y, 360,
		PANEL_LABEL_STRING, "Apply",
		PANEL_NOTIFY_PROC, display_display_popup_apply_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `reset_button' in the specified instance.
 */
Xv_opaque
display_display_popup_reset_button_create(display_display_popup_objects *ip, Xv_opaque owner)
{
	extern void		display_display_popup_reset_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "display:reset_button",
		XV_X, 141,
		XV_Y, 360,
		PANEL_LABEL_STRING, "Reset",
		PANEL_NOTIFY_PROC, display_display_popup_reset_button_notify_callback,
		NULL);
	return obj;
}


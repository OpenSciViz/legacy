;GIL-3
(
(
	:type                   :popup-window
	:name                   pattern_contexts_popup
	:owner                  frame
	:width                  476
	:height                 209
	:background-color       ""
	:foreground-color       ""
	:label                  "Edit Receive Contexts"
	:label-type             :string
	:initial-state          :invisible
	:show-footer            nil
	:resizable              nil
	:pinned                 t
	:done-handler           nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :control-area
	:name                   controls1
	:owner                  pattern_contexts_popup
	:help                   ""
	:x                      0
	:y                      0
	:width                  476
	:height                 209
	:background-color       ""
	:foreground-color       ""
	:initial-state          :visible
	:show-border            nil
	:menu                   nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :scrolling-list
	:name                   receive_contexts_list
	:owner                  controls1
	:help                   ""
	:x                      8
	:y                      16
	:width                  430
	:height                 74
	:value-x                8
	:value-y                16
	:rows                   3
	:foreground-color       ""
	:label                  ""
	:title                  ""
	:label-type             :string
	:layout-type            :horizontal
	:read-only              nil
	:multiple-selections    nil
	:selection-required     nil
	:initial-state          :active
	:droppable              nil
	:default-drop-site      nil
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:initial-list-values    ()
	:initial-list-glyphs    ()
	:initial-selections     ()
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   context_name
	:owner                  controls1
	:help                   "Name: name of this context"
	:x                      13
	:y                      110
	:width                  450
	:height                 15
	:value-x                63
	:value-y                110
	:value-length           50
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Name:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   context_type
	:owner                  controls1
	:help                   "Type: one of \"int\", \"char\", or \"bytes\"."
	:x                      18
	:y                      130
	:width                  125
	:height                 15
	:value-x                63
	:value-y                130
	:value-length           10
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Type:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   context_value
	:owner                  controls1
	:help                   "Value: a value appropriate to the type, i.e. 55 for a type of int, \"fred\" for a type of char, or  for a type of bytes."
	:x                      13
	:y                      150
	:width                  450
	:height                 15
	:value-x                63
	:value-y                150
	:value-length           50
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Value:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   context_add_button
	:owner                  controls1
	:help                   "Add Context:
Add the current values of the Name and Value
fields to the list of contexts."
	:x                      149
	:y                      180
	:width                  42
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Add"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   context_delete_button
	:owner                  controls1
	:help                   "Delete Context:
Delete the selected entry from the list
of contexts."
	:x                      199
	:y                      180
	:width                  57
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Delete"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   context_change_button
	:owner                  controls1
	:help                   "Change Context:
Change the selected entry to what's in the
Name and Value fields."
	:x                      264
	:y                      180
	:width                  64
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Change"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
)

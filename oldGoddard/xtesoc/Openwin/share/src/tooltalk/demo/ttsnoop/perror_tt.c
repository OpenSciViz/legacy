/*  
 * perror_tt.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <desktop/tt_c.h>
#include <stdio.h>


#define MAX_ERROR_POPUP_MESSAGE_SIZE 1024

extern void dbtt_ttsnoop_error_popup_display(const char* message);
char	*tt_errors();

Tt_status
perror_tt(Tt_status ttrc, char *call, char *file, int line)
{
	char		*procid = tt_default_procid();
	char buffer[MAX_ERROR_POPUP_MESSAGE_SIZE];

	if (ttrc == TT_ERR_UNIMP)
		sprintf(buffer,
			"%s:%d:%s UNIMPLEMENTED (%s)\n",
			file, line, call, tt_default_procid());
	else 
	  sprintf(buffer,
		  "%s:%d:%s failed with code %s (%s)\n",
		  file, line, call, tt_errors(ttrc), 
		  tt_default_procid());

	fprintf(stderr, "%s\n", buffer);
	dbtt_ttsnoop_error_popup_display(buffer);

	return ttrc;
}

char	*
tt_errors(Tt_status err_num)
{
	static char	unknown[32];

	switch(err_num)
	{
        case	TT_OK:
		return("TT_OK");
	case	TT_WRN_NOTFOUND:
		return("TT_WRN_NOTFOUND");
	case	TT_WRN_STALE_OBJID:
		return("TT_WRN_STALE_OBJID");
	case	TT_WRN_STOPPED:
		return("TT_WRN_STOPPED");
	case	TT_WRN_SAME_OBJID:
		return("TT_WRN_SAME_OBJID");
	case	TT_WRN_START_MESSAGE:
		return("TT_WRN_START_MESSAGE");
	case	TT_WRN_APPFIRST:
		return("TT_WRN_APPFIRST");
	case	TT_WRN_LAST:
		return("TT_WRN_LAST");
	case	TT_ERR_CLASS:
		return("TT_ERR_CLASS");
	case	TT_ERR_DBAVAIL:
		return("TT_ERR_DBAVAIL");
	case	TT_ERR_DBEXIST:
		return("TT_ERR_DBEXIST");
	case	TT_ERR_FILE:
		return("TT_ERR_FILE");
	case	TT_ERR_INVALID:
		return("TT_ERR_INVALID");
	case	TT_ERR_MODE:
		return("TT_ERR_MODE");
	case	TT_ERR_ACCESS:
		return("TT_ERR_ACCESS");
	case	TT_ERR_NOMP:
		return("TT_ERR_NOMP");
	case	TT_ERR_NOTHANDLER:
		return("TT_ERR_NOTHANDLER");
	case	TT_ERR_NUM:
		return("TT_ERR_NUM");
	case	TT_ERR_OBJID:
		return("TT_ERR_OBJID");
	case	TT_ERR_OP:
		return("TT_ERR_OP");
	case	TT_ERR_OTYPE:
		return("TT_ERR_OTYPE");
	case	TT_ERR_ADDRESS:
		return("TT_ERR_ADDRESS");
	case	TT_ERR_PATH:
		return("TT_ERR_PATH");
	case	TT_ERR_POINTER:
		return("TT_ERR_POINTER");
	case	TT_ERR_PROCID:
		return("TT_ERR_PROCID");
	case	TT_ERR_PROPLEN:
		return("TT_ERR_PROPLEN");
	case	TT_ERR_PROPNAME:
		return("TT_ERR_PROPNAME");
	case	TT_ERR_PTYPE:
		return("TT_ERR_PTYPE");
	case	TT_ERR_DISPOSITION:
		return("TT_ERR_DISPOSITION");
	case	TT_ERR_SCOPE:
		return("TT_ERR_SCOPE");
	case	TT_ERR_SESSION:
		return("TT_ERR_SESSION");
	case	TT_ERR_VTYPE:
		return("TT_ERR_VTYPE");
	case	TT_ERR_NO_VALUE:
		return("TT_ERR_NO_VALUE");
	case	TT_ERR_INTERNAL:
		return("TT_ERR_INTERNAL");
	case	TT_ERR_READONLY:
		return("TT_ERR_READONLY");
	case	TT_ERR_NO_MATCH:
		return("TT_ERR_NO_MATCH");
	case	TT_ERR_UNIMP:
		return("TT_ERR_UNIMP");
	case	TT_ERR_OVERFLOW:
		return("TT_ERR_OVERFLOW");
	case	TT_ERR_PTYPE_START:
		return("TT_ERR_PTYPE_START");
	case	TT_ERR_CATEGORY:
		return("TT_ERR_CATEGORY");
	case	TT_ERR_SLOTNAME:
		return("TT_ERR_SLOTNAME");
	case	TT_ERR_NOMEM:
		return("TT_ERR_NOMEM");
	case	TT_ERR_STATE:
		return("TT_ERR_STATE");
	case	TT_ERR_DBCONSIST:
		return("TT_ERR_DBCONSIST");
	case	TT_ERR_DBFULL:
		return("TT_ERR_DBFULL");
	case	TT_ERR_DBUPDATE:
		return("TT_ERR_DBUPDATE");
	case	TT_ERR_XDR:
		return("TT_ERR_XDR");
	case	TT_ERR_APPFIRST:
		return("TT_ERR_APPFIRST");
	case	TT_ERR_LAST:
		return("TT_ERR_LAST");
	case	TT_STATUS_LAST:
		return("TT_STATUS_LAST");
	default:
		sprintf(unknown, "Unknown value (%d)\0", err_num);
		return(unknown);
	}
}

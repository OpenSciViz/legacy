/*
 * dbtt_handlers.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"

extern dbtt_about_ttsnoop_popup_objects  *Dbtt_about_ttsnoop_popup;

menu_send_message(Xv_opaque m, Xv_opaque mi)
{
        int     row = xv_get(mi, MENU_CLIENT_DATA);
 
        if(set_row(Send_send_msg_popup->message_list, row))
        {
                send_message(Send_send_msg_popup);
        }
}
 
/*
 * Menu handler for `msg_menu'.
 */
Menu
display_menu(Menu menu, Menu_generate op)
{
        dbtt_frame_objects * ip = (dbtt_frame_objects *) xv_get(menu, XV_KEY_DATA, INSTANCE);
        static  char            **list = 0;
        Xv_opaque               mi;
        int                     j, i, n;
        
        switch (op) {
        case MENU_DISPLAY:
                list = (char **)get_labels(Send_send_msg_popup->message_list);
                n = xv_get(menu, MENU_NITEMS); /* don't forget the title */
                for(i = 2, j = 0; list[j]; i++, j++)
                {
                        mi = xv_create(NULL, MENUITEM,
                                MENU_STRING_ITEM, list[j],
                                MENU_RELEASE,
                                MENU_CLIENT_DATA,       j,
                                MENU_NOTIFY_PROC, menu_send_message,
                                NULL);
                        if(i <= n)
                        {
                                xv_set(menu, MENU_REPLACE, i, mi, NULL);
                        }
                        else
                        {
                                xv_set(menu, MENU_APPEND_ITEM, mi, NULL);
                        }
                }
                for(; i <= n; i++)
                {
                        xv_set(menu, MENU_REMOVE, i, NULL);
                }
                break;
 
        case MENU_DISPLAY_DONE:
                break;
 
        case MENU_NOTIFY:
                break;
 
        case MENU_NOTIFY_DONE:
                free(list);
                break;
        }
        return menu;
}

/*
 * Notify callback function for `mode_setting'.
 */
void
dbtt_frame_mode_setting_notify_callback(Panel_item item, int value, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

	if (value == 1)
	{
		stop_mon();
	}
	
	if (value == 0)
	{
		start_mon();
	}

        if (value == 0)
        {
		clear_copyright(ip);
        }
}

/*
 * Notify callback function for `clear_button'.
 */
void
dbtt_frame_clear_button_notify_callback(Panel_item item, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		textsw_reset(ip->textsw, 0, 0);
	}
}

/*
 * Notify callback function for `about_ttsnoop_button'.
 */
void
dbtt_frame_about_ttsnoop_button_notify_callback(Panel_item item, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	xv_set(Dbtt_about_ttsnoop_popup->about_ttsnoop_popup, XV_SHOW, TRUE, NULL);
}

/*
 * Notify callback function for `display_button'.
 */
void
dbtt_frame_display_button_notify_callback(Panel_item item, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	xv_set(Display_display_popup->display_popup, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
	xv_set(Display_display_popup->display_popup, XV_SHOW, TRUE, NULL);
}

/*
 * Notify callback function for `send_button'.
 */
void
dbtt_frame_send_button_notify_callback(Panel_item item, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	xv_set(Send_send_msg_popup->send_msg_popup, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
	xv_set(Send_send_msg_popup->send_msg_popup, XV_SHOW, TRUE, NULL);
}

/*
 * Notify callback function for `receive_button'.
 */
void
dbtt_frame_receive_button_notify_callback(Panel_item item, Event *event)
{
	dbtt_frame_objects *ip = (dbtt_frame_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	xv_set(Pat_receive_msg_popup->receive_msg_popup, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
	xv_set(Pat_receive_msg_popup->receive_msg_popup, XV_SHOW, TRUE, NULL);
}

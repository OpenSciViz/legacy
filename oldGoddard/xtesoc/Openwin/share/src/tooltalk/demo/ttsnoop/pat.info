#
# Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
# 
:pat_edit_receive_contexts
Edit Receive Contexts:
Add, delete, or change contexts you want to
receive.
# 
:receive_msg_panel
Patterns
--------
Since messages are not explicitly directed to a
particular receiver by the sending process, a
"pattern matching" method is used to determine
the receiver. Tools register descriptions of the
messages in which they are interested and
ToolTalk uses these descriptions to infer the
routing of the message. Separate sets of patterns
are kept to describe the messages the tool wants
to handle and the messages the tool wants to
observe. Tools wishing to receive messages
declare their interest either dynamically at run
time or statically at installation time. A
dynamic registration consists of a set of
patterns against which all messages are
compared.  If a message matches the pattern, the
tool which registered the pattern is eligible to
receive the message.

Before receiving procedural messages through
ToolTalk, a process must register with the
message passer. By registering, the process gives
patterns to ToolTalk; which then delivers
messages that match those patterns to the
process.  The patterns can be created in two
ways:

Statically. Through a ptype. A process can
declare its ptype to the message passer; ToolTalk
then generates patterns from each signature in
the ptype.  These generated patterns can be
modified by joining and quitting sessions and
files.

Dynamically. A process can create patterns "on
the fly" and register them with ToolTalk. A
typical use of this facility would be a message
logging utility that simply observes all messages
that go by, displaying them in a window; such a
utility would be useful in debugging message
protocols.

This application uses only the Dynamic patterns.

To register a pattern, first you must allocate a
new pattern, fill in the proper information, then
register it.  When you are through with the
pattern (which is only after you no longer are
interested in messages that match it), free the
storage for the pattern.

To allocate the new pattern, use
tt_pattern_create(). It returns a "handle" or
"opaque pointer" to the pattern; use this handle
on succeeding calls to reference this pattern.

To fill in pattern information, use the
tt_pattern_*_add() calls; there is one of these
for each attribute of the pattern. It is possible
to supply multiple values for each attribute in
the pattern; the pattern attribute matches a
message attribute if any of the values in the
pattern match the value in the message.

This application uses non-exclusive settings and
comma separated values for the multiple
attributes.

To match messages without regard to the value in
a particular attribute, simply omit the attribute
from the pattern.

If no pattern attribute is specified, the
ToolTalk service counts the message attribute as
matched. The fewer pattern attributes you
specify, the more messages you become eligible to
receive.

If there are multiple values specified for a
pattern attribute, one of the values must match
the message attribute value. If no value matches,
the ToolTalk service will not consider your
application as a receiver.

The attributes Category and Scope must always be
supplied.

When the pattern is complete, register the
pattern with tt_pattern_register(), and then join
sessions or files as required by SELECT'ing the
Apply button.

Since the primary effect of joining files and
sessions is to update currently registered
patterns, patterns that are registered after
the joins will not be updated. You should either
register all patterns before joining, or re-do
the joins after registering new patterns (alternatively
one can do session or file sets on the pattern
directly). This applies to patterns registered 
implicitly by joining ptypes, as well.

# 
:address_setting
Address:
For each address button selected, does a
tt_pattern_address_add.  Hence the address 
part of the pattern will match a message that 
is addressed to any of the selected addresses. 
No selection acts like wildcard don't care.

Because there are many types of tools and
different users will use different sets of tools
at different times, it's often impossible for the
sender of a message to identify the precise
recipient of a message.  Instead, the sender
gives an operation name which specifies the
meaning of the message.

	TT_PROCEDURE: Op and Args supplied.

	TT_OBJECT: Object, Op, and Args
	supplied.

	TT_HANDLER: Handler, Op, and Args
	supplied. (NOTE: Tooltalk really doesn't
	do any pattern matching on point to 
	point messages - it just directly passes
	them on - therefore, you really can't monitor
	point to point messages.)

	TT_OTYPE: Otype, Op, and Args supplied.

# 
:object_textfield
Object:
Comma separated list of objids of the objects
which will match (object match will occur if
any of the listed objids appears in a
message). For each objid, a
tt_pattern_object_add is performed. 
No entries acts like wildcard don't care.

# 
:otype_textfield
Otype:
Comma separated list of object types to which
will match (otype match will occur if any of
the listed otypes appears in a message. For
each otype, a tt_pattern_otype_add is
performed.  No entries acts like
wildcard don't care.


# 
:op_textfield
OP:
Comma separated list of operations that
describes the notification or request being
made. For each operation, a tt_pattern_op_add
is performed. Op match will occur if any of
the listed ops appears in a message.  No
entries acts like wildcard don't care.

# 
:scope_setting
Scope:
For each scope button selected, does a
tt_pattern_scope_add.  Hence the scope 
part of the pattern will match a message that 
is scoped to any of the selected scopes. 
One button must be set and ttsnoop
defaults this to TT_SESSION.
For the scopes involving files, you really need
to specify some file names, since you can't
actually scope to every file in the universe.

Scope:
Use scope session if messages from other
processes in the same session as your process are
desired; use scope file if messages about a file
is desired.

	TT_SESSION: Receive messages from other
	processes in your session.

	TT_FILE: Receive messages about the file
	joined.

	TT_BOTH: Receive messages about a file
	and the session.

	TT_FILE_IN_SESSION:  Receive messages for
	the file joined while in this session.


# 
:file_name_textfield
File Name:
Comma separated list of file names to which 
the message is scoped. For each file name, a 
tt_pattern_file_add is performed.  
Filename match will occur
if any of the listed filenames appears in 
a message. No entries matches nothing.

# 
:session_textfield
Session:
Currently a read only field showing the current
session. If scope is one of TT_SESSION, TT_BOTH,
or TT_FILE_IN_SESSION, a tt_pattern_session_add
is performed using tt_default_session().

# 
:category_textfield
Category:
We can currently only observe.

Use category observe if you just want to look at
the messages; use category handle to volunteer to
be the unique handler of the message.

	TT_OBSERVE: Observing processes just get
	copies of the message for their own
	information. Any number of processes can
	observe a message. Even if the message is
	a request, observers cannot return values
	to the sender. Often, the action taken by
	observers just affects the interactive
	display of the underlying tool data.

	TT_HANDLE: Handling processes actually
	perform an action based on the message.
	Only one process will handle any given
	message.  If the message is a request,
	the handling process is the process that
	returns any values. Generally, the action
	taken by a handler affects the
	persistently-stored representation of the
	tool's data.

# 
:class_setting
Class:
For each class button selected, does a
tt_pattern_class_add.  Hence the class 
part of the pattern will match a message that 
is classed to any of the selected classes. 
No selections acts like wildcard don't care.


Use class request for messages that return
values, or for which you want feedback telling
you when the message is handled or queued, or
when a process is started to handle the request.
Use class notification for messages that just
notify other processes of events.

	TT_NOTICE: Notice are messages that
	inform other tools that some event has
	happened. Tools sending notices don't
	expect replies back; the sender is just
	letting the other tools know what's
	happening.

	TT_REQUEST: Requests are messages that
	ask another tool to perform an action.
	Often, but not always, the requesting
	tool expects some value returned. This
	value is returned as a reply to the
	request. Even if no value is returned,
	the tool processing the request sends a
	reply indicating success or failure.

# 
:state_setting
State:
For each state button selected, does a
tt_pattern_state_add.  Hence the state 
part of the pattern will match a message that 
is in any of the selected states. 
No selections acts like wildcard don't care.

Values for the State attribute of a
message.  Possible values and their meanings
are:

	TT_CREATED: Message has been created but
	not yet sent. Only the sender of a
	message will see a message in this
	state.

	TT_SENT: Message has been sent but not
	yet handled.

	TT_HANDLED: Message has been handled,
	return values are valid.

	TT_FAILED: Message could not be delivered
	to a handler.

	TT_QUEUED: Message has been queued for
	later delivery.

	TT_STARTED: Attempting to start a process
	to handle the message.

	TT_REJECTED: Message has been rejected by
	a possible handler.  This state is seen
	only by the rejecting process; ToolTalk
	changes the state back to TT_SENT before
	delivering the message to another
	possible handler.

# 
:disposition_setting
Disposition:
For each disposition button selected, does a
tt_pattern_disposition_add.  Hence the
disposition part of the pattern will match a
message that is dispositioned to any of the
selected dispositions.  No selections acts
like wildcard don't care.

Specifies the action to take if the message
cannot be handled by any running process. Queue
if the message should be queued until a process
of the Handler_Ptype registers. Start if a
process of the Handler_Ptype should be started.
	TT_QUEUE: Queue the message until a
	process of the proper ptype receives the
	message.

	TT_START: Attempt to start a process of
	the proper ptype if none is running.

Note that Tt_disposition values can be added
together, so that TT_QUEUE+TT_START means both to
queue the message and to try to start a process.
This can be useful if the start can fail (or be
vetoed by the user), to ensure the message is
processed as soon as an eligible process does
start.

# 
:sender_textfield
Sender:
Comma separated list of
Sender procids.  For each sender procid, a
tt_pattern_sender_add is performed.  (sender
will match if the message is sent from a
listed sender) No entries acts like wildcard
don't care.


# 
:sender_ptype_textfield
Sender ptype:
Comma separated list of Senders ptypes.  For
each sender ptype, a
tt_pattern_sender_ptype_add is performed.
(sender will match if the message is sent
with one of the listed ptypes.) No entries
acts like wildcard don't care.


# 
:arg_list
Arguments:
Lists any arguments you specify using the
controls below

# 
:mode_setting
Mode:
Specifies the mode of a message argument.

Possible values and meanings are:
	TT_IN: The argument is written by the
	sender and can be read by the handler 
	and any observers.

	TT_OUT: The argument is written by the
	handler and can be read by the sender 
	and any reply observers.

	TT_INOUT: The argument can be  written and
	read by the sender and the handler and
	any observers.

# 
:arg_vtype_textfield
VType:
String indicating the 'application' type.
Use ALL to match any vtype. Remember: 
This is application type space 
and has nothing to do with 'C' types.

# 
:arg_value_textfield
Value:
Entering NULL enters a NULL for the value. (Use
NULL to match any value.)  Else
value interpretation is dependent on kind of add.
If a Add or Badd is used, value becomes whatever
string was entered. If Iadd is used, value is the
result of atoi(whatever_was_entered).
[Sorry, no way yet to enter
full 8 bit bytes for Badds.]

#
:kind_of_add_setting
Kind of Add:
Determines which one of 
tt_pattern_arg_add
tt_pattern_barg_add
tt_pattern_iarg_add
is done when adding the argument

# 
:add_button
Add Button:
Add an argument with the Mode, VType and Value,
using the Kind Of Add specified.

# 
:delete_button
Delete Button:
Delete the selected argument.

# 
:change_button
Change Button:
Change the selected argument to the current
values of Mode, Type and Value,
using the Kind Of Add specified

# 
:apply_button
Register to receive messages that match this
pattern


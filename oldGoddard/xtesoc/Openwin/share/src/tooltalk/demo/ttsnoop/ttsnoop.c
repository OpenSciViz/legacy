/*
 * ttsnoop.c - Contains main() for project ttsnoop
 * 
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved. 
 */

char    copyright[] = 
"This file is a product of Sun Microsystems, Inc. and is provided for \n\
unrestricted use provided that this legend is included on all tape media \n\
and as a part of the software program in whole or part. Users may copy, \n\
modify or distribute this file at will. \n\
 \n\
THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE \n\
WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR \n\
PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRAC- TICE. \n\
 \n\
This file is provided with no support and without any obligation on the part \n\
of Sun Microsystems, Inc. to assist in its use, correction, modification \n\
or enhancement. \n\
 \n\
SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE \n\
INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE OR \n\
ANY PART THEREOF. \n\
 \n\
In no event will Sun Microsystems, Inc. be liable for any lost revenue or \n\
profits or other special, indirect and consequential damages, even if Sun \n\
has been advised of the possibility of such damages. \n\
 \n\
Sun Microsystems, Inc. 2550 Garcia Avenue Mountain View, California 94043 \n\
 \n\
Copyright (c) 1992 by Sun Microsystems. All rights reserved. \n\
\n\n\
Obtaining TTSnoop Help\n\
----------------------\n\
To obtain general TTSnoop help press the About TTSnoop button.\n\
Help for individual buttons and settings can be obtained by\n\
pressing the help key while the mouse is over the button or\n\
setting of interest. Your HELPPATH environment variable must\n\
contain the directory containing TTSnoop's .info files for this\n\
to work.  To see what api calls are being used by ttsnoop,\n\
use the -t switch when invoking ttsnoop.\n\
";

char	help[] =
"Obtaining TTSnoop Help\n\
----------------------\n\
Help for individual buttons and settings can be obtained by\n\
pressing the help key while the mouse is over the button or\n\
setting of interest.  Your HELPPATH environment variable must\n\
contain the directory containing TTSnoop's .info files for this\n\
to work. To see what api calls are being used by ttsnoop,\n\
use the -t switch when invoking ttsnoop.\n\n\
Using TTSnoop\n\
-------------\n\
To observe messages select the Start setting choice.\n\
\n\
To turn off message observation select the Stop\n\
setting choice.\n\
\n\
To limit the types of messages:\n\
        1) Select Patterns button\n\
        2) Enter the type of pattern you want\n\
           to observe\n\
        3) Select Apply button\n\
\n\
To highlite information that is displayed:\n\
        1) Select Display button\n\
        2) Mark those items that you want to highlite\n\
        3) Select Apply button\n\
\n\
To send a message:\n\
        1) Select the Messages button\n\
        2) Compose the message\n\
        3) Select the Send Message button\n\
\n\
To store a message:\n\
        1) Select the Message button\n\
        2) Compose the message\n\
        3) Select the Add Message button\n\
\n\
To send a stored message:\n\
        Select the message from the\n\
        Send Message menu\n\
\n\
To clear the message output window select the Clear button.\n\
\n\
General ToolTalk Information\n\
----------------------------\n\
Before sending or receiving object oriented\n\
messages through ToolTalk, a process must\n\
register with the message passer. By registering,\n\
the process tells ToolTalk several things:\n\
\n\
        The process type (ptype) of the process.\n\
        This allows the message passer to direct\n\
        messages implemented by this ptype to\n\
        this process.\n\
\n\
        The sessions the process participates\n\
        in.\n\
\n\
        The documents that the process is\n\
        observing. Messages to objects in these\n\
        documents can then be routed to the\n\
        process.\n\
\n\
Note that these registrations generally modify\n\
generated patterns. It would be possible to use\n\
the pattern manipulation API calls to observe or\n\
handle object-oriented messages, but the methods\n\
described here are much shorter since they take\n\
advantage of the information declared in the type\n\
definitions.\n\
\n\
Use tt_session_join to become part of a session.\n\
When joining, you must provide the session id of\n\
the session to join.\n\
\n\
When your tool no longer needs ToolTalk services,\n\
quit the session with tt_session_quit.\n\
\n\
When your tool loads a file, it should join the\n\
collection of processes interested in that file\n\
by calling tt_file_join(); when through with the\n\
file, quit the collection by calling\n\
tt_file_quit(). Some tools may have several files\n\
open at once; other tools may only open one at a\n\
time.\n\
";

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include <desktop/tt_c.h>
#include "dbtt_ui.h"
#include "pat_ui.h"
#include "send_ui.h"
#include "display_ui.h"
#include "send_contexts_ui.h"
#include "pattern_contexts_ui.h"

/*
 * External variable declarations.
 */
dbtt_frame_objects			*Dbtt_frame;
dbtt_about_ttsnoop_popup_objects	*Dbtt_about_ttsnoop_popup;
dbtt_ttsnoop_error_popup_objects	*Dbtt_ttsnoop_error_popup;
pat_receive_msg_popup_objects		*Pat_receive_msg_popup;
send_send_msg_popup_objects		*Send_send_msg_popup;
send_contexts_send_contexts_popup_objects *Edit_send_contexts_popup;
pattern_contexts_pattern_contexts_popup_objects *Edit_receive_contexts_popup;
int					Display_value;
int					Display_reset_value;

char	*procid;
display_display_popup_objects	*Display_display_popup;

/*
 * function to display error message in popup
 */
void
dbtt_ttsnoop_error_popup_display(char* message)
{
XBell((Display*)xv_get(Dbtt_ttsnoop_error_popup->ttsnoop_error_textsw, XV_DISPLAY), 50);
textsw_insert(Dbtt_ttsnoop_error_popup->ttsnoop_error_textsw, message, strlen(message));
xv_set(Dbtt_ttsnoop_error_popup->ttsnoop_error_popup, XV_SHOW, TRUE, NULL);
}


Notify_value
destroy_func(Notify_client client, Destroy_status status)
{
	switch(status)
	{
	case	DESTROY_CHECKING:
		break;
	case	DESTROY_CLEANUP:
		textsw_reset(Dbtt_frame->textsw, 0, 0);
		return(notify_next_destroy_func(client, status));
		break;
	case	DESTROY_SAVE_YOURSELF:
		break;
	case	DESTROY_PROCESS_DEATH:
		break;
	default:
		printf("received %X\n", status);
	}
	return (NOTIFY_DONE);
}


/*
 * Instance XV_KEY_DATA key.  An instance is a set of related
 * user interface objects.  A pointer to an object's instance
 * is stored under this key in every object.  This must be a
 * global variable.
 */
Attr_attribute	INSTANCE;

/*
 * If trace_on is 1, then pattern generation code and message
 * generation code is display on stdout.
 */
int	trace_on;

/*
 * main for project ttsnoop
 */
void
main(int argc, char **argv)
{
	/*
	 * Initialize XView.
	 */
	xv_init(XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
	INSTANCE = xv_unique_key();

	/*
	 * Initialize the message display values
	 */
	Display_value = 0;
	Display_reset_value = 0;


	/*
	 * Check for options 
	 */
	trace_on = 0;
	if (argc > 1) {
		if (!strcmp(argv[1], "-t")) {  /* trace option */
			trace_on = 1;
		}
		else  /* show usage */
		  {printf("Usage: ttsnoop [-t]\n\t-t traces api calls to stdout\n");
		   exit(0);
		  }
	}
	
	/*
	 * Initialize user interface components.
	 */
	Dbtt_frame = dbtt_frame_objects_initialize(NULL, NULL);
	xv_set(Dbtt_frame->textsw,
		TEXTSW_CONTENTS, copyright,
		NULL);
	textsw_normalize_view(Dbtt_frame->textsw, 0);

	Dbtt_about_ttsnoop_popup = dbtt_about_ttsnoop_popup_objects_initialize(NULL, Dbtt_frame->frame);	
	xv_set(Dbtt_about_ttsnoop_popup->about_ttsnoop_textsw,
		TEXTSW_CONTENTS, help,
		NULL);
	textsw_normalize_view(Dbtt_about_ttsnoop_popup->about_ttsnoop_textsw, 0);

	Dbtt_ttsnoop_error_popup = dbtt_ttsnoop_error_popup_objects_initialize(NULL, Dbtt_frame->frame);	
	textsw_normalize_view(Dbtt_ttsnoop_error_popup->ttsnoop_error_textsw, 0);

	Display_display_popup =
	    display_display_popup_objects_initialize(NULL, Dbtt_frame->frame);

	Send_send_msg_popup =
	    send_send_msg_popup_objects_initialize(NULL, Dbtt_frame->frame);

	Pat_receive_msg_popup =
	    pat_receive_msg_popup_objects_initialize(NULL, Dbtt_frame->frame);

	Edit_send_contexts_popup =
	    send_contexts_send_contexts_popup_objects_initialize(NULL,
							    Dbtt_frame->frame);
	Edit_receive_contexts_popup =
	    pattern_contexts_pattern_contexts_popup_objects_initialize(NULL,
							    Dbtt_frame->frame);

	notify_interpose_destroy_func(Dbtt_frame->frame, destroy_func);

	if((tt_pointer_error(procid = tt_open()) != TT_OK))
	{
		fprintf(stderr, "Could not open ToolTalk.\n\nExiting.\n");
		exit(1);
	}

	send_init(Send_send_msg_popup);
	pattern_init(Pat_receive_msg_popup);
	set_pattern(Pat_receive_msg_popup);

        /* send/receive contexts popup init */
        send_contexts_init(Edit_send_contexts_popup);
        pattern_contexts_init(Edit_receive_contexts_popup);

	/*
	 * Turn control over to XView.
	 */
	xv_main_loop(Dbtt_frame->frame);
	exit(0);
}

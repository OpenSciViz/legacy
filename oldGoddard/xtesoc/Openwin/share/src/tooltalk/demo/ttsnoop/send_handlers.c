/*
 * send_handlers.c - Notify and event callback function stubs.
 * 
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved. 
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"

/*
 * Notify callback function for `send_button'.
 */
void
send_send_msg_popup_send_button_notify_callback(Panel_item item, Event *event)
{
	send_send_msg_popup_objects *ip = (send_send_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		send_message(ip);
	}
}

/*
 * Notify callback function for `Edit Send Contexts...' button.
 */
void
send_send_msg_popup_send_edit_send_contexts_button_callback(Panel_item item, Event *event)
{
        xv_set(Edit_send_contexts_popup->send_contexts_popup, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
        xv_set(Edit_send_contexts_popup->send_contexts_popup, XV_SHOW, TRUE, NULL);
}

/*
 * Notify callback function for Scope setting panel choice.
 */
void 
msg_scope_setting_notify_callback(Panel_item item, int value, Event *event)
{send_send_msg_popup_objects *ip;

 /* get panel we're part of */
 ip = (send_send_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

 /* disable/enable the filename entry */
 xv_set(ip->file_name_textfield, PANEL_INACTIVE, (value > 0) ? FALSE : TRUE, NULL);

 return;
}


/*
 * Notify callback function for address setting panel choice.
 */
void 
msg_address_setting_notify_callback(Panel_item item, int value, Event *event)
{send_send_msg_popup_objects *ip;

 /* get panel we're part of */
 ip = (send_send_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

 /* (in)activate object addressing */
 xv_set(ip->object_textfield, PANEL_INACTIVE, (value == 1) ? FALSE : TRUE, NULL);

 /* (in)activate handler addressing */
 xv_set(ip->handler_textfield,  PANEL_INACTIVE, (value == 2) ? FALSE : TRUE, NULL);

 /* (in)activate otype addressing */
 xv_set(ip->otype_textfield,  PANEL_INACTIVE,  (value == 3) ? FALSE : TRUE, NULL);

 return;
}

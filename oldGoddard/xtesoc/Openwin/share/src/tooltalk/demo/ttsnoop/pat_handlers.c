/*
 * pat_handlers.c - Notify and event callback function stubs.
 *  
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"

/*
 * Notify callback function for `list_add_button'.
 */
void
pat_receive_msg_popup_list_add_button_notify_callback(Panel_item item, Event *event)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
}

/*
 * Notify callback function for address setting panel toggle.
 */
void
pat_address_setting_notify_callback(Panel_item item, int value, Event *event)
{pat_receive_msg_popup_objects *ip; 

 /* get panel we're part of */
 ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

 /* (in)activate object addressing */
  xv_set(ip->object_textfield, PANEL_INACTIVE, (value & 0x2) ? FALSE : TRUE, NULL);

 /* (in)activate otype addressing */
 xv_set(ip->otype_textfield,  PANEL_INACTIVE, (value & 0x8) ? FALSE : TRUE, NULL);

}

/*
 * Notify callback function for `add_button'.
 */
void
pat_receive_msg_popup_add_button_notify_callback(Panel_item item, Event *event)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		pat_add_arg(ip);
	}
}

/*
 * Notify callback function for `delete_button'.
 */
void
pat_receive_msg_popup_delete_button_notify_callback(Panel_item item, Event *event)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		pat_delete_arg(ip);
	}
}

/*
 * Notify callback function for `change_button'.
 */
void
pat_receive_msg_popup_change_button_notify_callback(Panel_item item, Event *event)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		pat_change_arg(ip);
	}
}

/*
 * Notify callback function for `apply_button'.
 */
void
pat_receive_msg_popup_apply_button_notify_callback(Panel_item item, Event *event)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	{
		set_pattern(ip);
	}
}

/*
 * Notify callback function for `arg_list'.
 */
int
list_item_select(Panel_item	item,
		 char	       *string,
		 Xv_opaque	client_data,
		 Panel_list_op	op,
		 Event	       *event,
		 int		row)
{
	pat_receive_msg_popup_objects *ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);
	
	switch(op) {
	case PANEL_LIST_OP_DESELECT:
		pat_set_arg_num(-1, ip);
		break;

	case PANEL_LIST_OP_SELECT:
		pat_set_arg_num(row, ip);
		break;

	case PANEL_LIST_OP_VALIDATE:
		break;

	case PANEL_LIST_OP_DELETE:
		break;
	}
	
	return XV_OK;
}


/*
 * Notify callback function for `Edit Receive Contexts...' button.
 */
void
pat_receive_msg_popup_pat_edit_receive_contexts_button_callback(Panel_item item, Event *event)
{
        xv_set(Edit_receive_contexts_popup->pattern_contexts_popup, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
        xv_set(Edit_receive_contexts_popup->pattern_contexts_popup, XV_SHOW, TRUE, NULL);
}


/*
 * Notify callback function for Scope setting panel toggle.
 */
void 
pat_scope_setting_notify_callback(Panel_item item, int value, Event *event)
{pat_receive_msg_popup_objects *ip;

 /* get panel we're part of */
 ip = (pat_receive_msg_popup_objects *) xv_get(item, XV_KEY_DATA, INSTANCE);

 /* make sure we always have at least one selected - default to session  */
 if (value == 0) xv_set(ip->scope_setting, PANEL_VALUE, 0x1, NULL);

 return;
}

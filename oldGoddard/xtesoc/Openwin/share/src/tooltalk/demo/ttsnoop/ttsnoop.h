/*
 * ttsnoop.h - Contains the object declarations for project ttsnoop
 *  
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include "display_ui.h"
#include "send_ui.h"
#include "dbtt_ui.h"
#include "pat_ui.h"
#include "send_contexts_ui.h"
#include "pattern_contexts_ui.h"

extern	dbtt_frame_objects	*Dbtt_frame;
extern	display_display_popup_objects	*Display_display_popup;
extern	send_send_msg_popup_objects	*Send_send_msg_popup;
extern	pat_receive_msg_popup_objects	*Pat_receive_msg_popup;
extern  send_contexts_send_contexts_popup_objects *Edit_send_contexts_popup;
extern  pattern_contexts_pattern_contexts_popup_objects *Edit_receive_contexts_popup;

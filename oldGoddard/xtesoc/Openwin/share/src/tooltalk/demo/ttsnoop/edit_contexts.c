/* 
 * edit_contexts.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved. 
 */

#include <xview/xview.h>
#include <xview/panel.h>
#include <desktop/tt_c.h>
#include <unistd.h>
#include "chkrc.h"
#include "pattern_contexts_ui.h"
#include "send_contexts_ui.h"
#include "arg_edit.h"

Arg_list *recv_contexts_get_arg();
Arg_list *recv_contexts_put_arg();

char     *contexts_label_arg();

Arg_list *send_contexts_get_arg();
Arg_list *send_contexts_put_arg();

send_contexts_send_contexts_popup_objects *send_ip = 0;
pattern_contexts_pattern_contexts_popup_objects  *recv_ip = 0;

/* pattern_contexts_init() - initialize panel list item for Edit Receive
 *	Contexts button after popup has been created.
 */
pattern_contexts_init(pattern_contexts_pattern_contexts_popup_objects *ip_tmp)
{
	recv_ip = ip_tmp;

	init_lst(recv_ip->receive_contexts_list,
		recv_ip->context_add_button,
		recv_ip->context_delete_button,
		recv_ip->context_change_button,
		NULL,
		recv_contexts_get_arg,
		recv_contexts_put_arg,
		contexts_label_arg);

	xv_set(recv_ip->context_name, PANEL_VALUE, "", NULL);
	xv_set(recv_ip->context_kind_of_add, PANEL_VALUE, ADD, NULL);
	xv_set(recv_ip->context_value, PANEL_VALUE, "", NULL);
}


/* send_contexts_init() - initialize panel list item for Edit Send
 *	Contexts button after popup has been created.
 */
send_contexts_init(send_contexts_send_contexts_popup_objects *ip_tmp)
{
	send_ip = ip_tmp;

	init_lst(send_ip->send_contexts_list,
		send_ip->context_add_button,
		send_ip->context_delete_button,
		send_ip->context_change_button,
		NULL,
		send_contexts_get_arg,
		send_contexts_put_arg,
		contexts_label_arg);

	xv_set(send_ip->context_name, PANEL_VALUE, "", NULL);
	xv_set(send_ip->context_kind_of_add,  PANEL_VALUE, ADD, NULL);
	xv_set(send_ip->context_value, PANEL_VALUE, "", NULL);
}

Arg_list *
send_contexts_get_arg(Arg_list *item)
{
        if(item == NULL)
        {
                item = (Arg_list *)malloc(sizeof(Arg_list));
                memset((void *)item, '\0', sizeof(Arg_list));
        }
        CHAR_GET(send_ip, context_name, slotname);
        ENUM_GET(send_ip, context_kind_of_add, add_kind);
        CHAR_GET(send_ip, context_value, value);

        return(item);
}

Arg_list *
send_contexts_put_arg(Arg_list *item)
{
        CHAR_SET(send_ip, context_name, slotname);
	ENUM_SET(send_ip, context_kind_of_add, add_kind, 0, 0);
        CHAR_SET(send_ip, context_value, value);
}

Arg_list *
recv_contexts_get_arg(Arg_list *item)
{
        if(item == NULL)
        {
                item = (Arg_list *)malloc(sizeof(Arg_list));
                memset((void *)item, '\0', sizeof(Arg_list));
        }
        CHAR_GET(recv_ip, context_name, slotname);
	ENUM_GET(recv_ip, context_kind_of_add, add_kind);
        CHAR_GET(recv_ip, context_value, value);

        return(item);
}

Arg_list *
recv_contexts_put_arg(Arg_list *item)
{
        CHAR_SET(recv_ip, context_name, slotname);
        ENUM_SET(recv_ip, context_kind_of_add, add_kind, 0, 0);
        CHAR_SET(recv_ip, context_value, value);
}

char *
contexts_label_arg(Arg_list *item)
{
        static  char    buffer[1024];

	sprintf(buffer, "%s %s(slotname) %s(value)\0",
	       (item->add_kind == ADD) ? "ADD" : ((item->add_kind == BADD) ? "BADD" : "IADD"),
		item->slotname,
		item->value);

	return(buffer);
}

/*
 * pat_ui.c - User interface object initialization functions.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/canvas.h>
#include <xview/panel.h>
#include <xview/scrollbar.h>
#include <xview/svrimage.h>
#include <xview/termsw.h>
#include <xview/text.h>
#include <xview/tty.h>
#include <xview/xv_xrect.h>
#include "pat_ui.h"

/*
 * Initialize an instance of object `receive_msg_popup'.
 */
pat_receive_msg_popup_objects *
pat_receive_msg_popup_objects_initialize(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (pat_receive_msg_popup_objects *) calloc(1, sizeof (pat_receive_msg_popup_objects))))
		return (pat_receive_msg_popup_objects *) NULL;
	if (!ip->receive_msg_popup)
		ip->receive_msg_popup = pat_receive_msg_popup_receive_msg_popup_create(ip, owner);
	if (!ip->controls1)
		ip->controls1 = pat_receive_msg_popup_controls1_create(ip, ip->receive_msg_popup);
	if (!ip->pat_edit_receive_contexts)
		ip->pat_edit_receive_contexts = pat_receive_msg_popup_pat_edit_receive_contexts_create(ip, ip->controls1);
	if (!ip->receive_msg_panel)
		ip->receive_msg_panel = pat_receive_msg_popup_receive_msg_panel_create(ip, ip->receive_msg_popup);
	if (!ip->address_setting)
		ip->address_setting = pat_receive_msg_popup_address_setting_create(ip, ip->receive_msg_panel);
	if (!ip->object_textfield)
		ip->object_textfield = pat_receive_msg_popup_object_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->otype_textfield)
		ip->otype_textfield = pat_receive_msg_popup_otype_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->op_textfield)
		ip->op_textfield = pat_receive_msg_popup_op_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->scope_setting)
		ip->scope_setting = pat_receive_msg_popup_scope_setting_create(ip, ip->receive_msg_panel);
	if (!ip->file_name_textfield)
		ip->file_name_textfield = pat_receive_msg_popup_file_name_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->session_textfield)
		ip->session_textfield = pat_receive_msg_popup_session_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->category_textfield)
		ip->category_textfield = pat_receive_msg_popup_category_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->class_setting)
		ip->class_setting = pat_receive_msg_popup_class_setting_create(ip, ip->receive_msg_panel);
	if (!ip->state_setting)
		ip->state_setting = pat_receive_msg_popup_state_setting_create(ip, ip->receive_msg_panel);
	if (!ip->disposition_setting)
		ip->disposition_setting = pat_receive_msg_popup_disposition_setting_create(ip, ip->receive_msg_panel);
	if (!ip->sender_textfield)
		ip->sender_textfield = pat_receive_msg_popup_sender_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->sender_ptype_textfield)
		ip->sender_ptype_textfield = pat_receive_msg_popup_sender_ptype_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->arg_list)
		ip->arg_list = pat_receive_msg_popup_arg_list_create(ip, ip->receive_msg_panel);
	if (!ip->mode_setting)
		ip->mode_setting = pat_receive_msg_popup_mode_setting_create(ip, ip->receive_msg_panel);
	if (!ip->arg_vtype_textfield)
		ip->arg_vtype_textfield = pat_receive_msg_popup_arg_vtype_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->arg_value_textfield)
		ip->arg_value_textfield = pat_receive_msg_popup_arg_value_textfield_create(ip, ip->receive_msg_panel);
	if (!ip->kind_of_add)
		ip->kind_of_add = pat_receive_msg_popup_kind_of_add_setting_create(ip, ip->receive_msg_panel);
	if (!ip->add_button)
		ip->add_button = pat_receive_msg_popup_add_button_create(ip, ip->receive_msg_panel);
	if (!ip->delete_button)
		ip->delete_button = pat_receive_msg_popup_delete_button_create(ip, ip->receive_msg_panel);
	if (!ip->change_button)
		ip->change_button = pat_receive_msg_popup_change_button_create(ip, ip->receive_msg_panel);
	if (!ip->apply_button)
		ip->apply_button = pat_receive_msg_popup_apply_button_create(ip, ip->receive_msg_panel);
	return ip;
}

/*
 * Create object `receive_msg_popup' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_receive_msg_popup_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, FRAME_CMD,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 551,
		XV_HEIGHT, 695,
		XV_LABEL, "Pattern Match",
		FRAME_SHOW_FOOTER, TRUE,
		FRAME_SHOW_RESIZE_CORNER, TRUE,
		FRAME_CMD_PUSHPIN_IN, TRUE,
		NULL);
	xv_set(xv_get(obj, FRAME_CMD_PANEL), WIN_SHOW, FALSE, NULL);
	return obj;
}

/*
 * Create object `controls1' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_controls1_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL,
		XV_KEY_DATA, INSTANCE, ip,
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, 36,
		WIN_BORDER, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `pat_edit_receive_contexts' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_pat_edit_receive_contexts_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	void pat_receive_msg_popup_pat_edit_receive_contexts_button_callback();
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:pat_edit_receive_contexts",
		XV_X, 196,
		XV_Y, 9,
		PANEL_LABEL_STRING, "Edit Receive Contexts...",
		PANEL_NOTIFY_PROC,
		    pat_receive_msg_popup_pat_edit_receive_contexts_button_callback,
		NULL);
	return obj;
}

/*
 * Create object `receive_msg_panel' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_receive_msg_panel_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:receive_msg_panel",
		XV_X, 0,
		XV_Y, (int)xv_get(ip->controls1, XV_Y) +
		      (int)xv_get(ip->controls1, XV_HEIGHT),
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		WIN_BORDER, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `address_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_address_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern void pat_address_setting_notify_callback(Panel_item, int, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:address_setting",
		XV_X, 39,
		XV_Y, 8,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_LABEL_STRING, "Address:",
	/*	PANEL_NOTIFY_PROC, pat_address_setting_notify_callback, */
		PANEL_CHOICE_STRINGS,
			"TT_PROCEDURE",
			"TT_OBJECT",
			"TT_HANDLER",
			"TT_OTYPE",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `object_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_object_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:object_textfield",
		XV_X, 49,
		XV_Y, 36,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Object:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `otype_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_otype_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:otype_textfield",
		XV_X, 53,
		XV_Y, 56,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Otype:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `op_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_op_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:op_textfield",
		XV_X, 75,
		XV_Y, 76,
		PANEL_VALUE_DISPLAY_LENGTH, 30,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "OP:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `scope_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_scope_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
        extern void pat_scope_setting_notify_callback(Panel_item, int, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:scope_setting",
		XV_X, 54,
		XV_Y, 106,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_LABEL_STRING, "Scope:",
		PANEL_CHOICE_STRINGS,
			"TT_SESSION",
			"TT_FILE",
			"TT_BOTH",
			"TT_FILE_IN_SESSION",
			NULL,
		PANEL_VALUE, 1,
		PANEL_NOTIFY_PROC, pat_scope_setting_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `file_name_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_file_name_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:file_name_textfield",
		XV_X, 26,
		XV_Y, 134,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "File Name:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `session_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_session_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:session_textfield",
		XV_X, 40,
		XV_Y, 154,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Session:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, TRUE,
		NULL);
	return obj;
}

/*
 * Create object `category_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_category_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:category_textfield",
		XV_X, 34,
		XV_Y, 184,
		PANEL_VALUE_DISPLAY_LENGTH, 25,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Category:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_VALUE_UNDERLINED, FALSE,
		PANEL_VALUE, "TT_OBSERVE",
		PANEL_READ_ONLY, TRUE,
		NULL);
	return obj;
}

/*
 * Create object `class_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_class_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:class_setting",
		XV_X, 56,
		XV_Y, 214,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_LABEL_STRING, "Class:",
		PANEL_CHOICE_STRINGS,
			"TT_NOTICE",
			"TT_REQUEST",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `state_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_state_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:state_setting",
		XV_X, 60,
		XV_Y, 252,
		PANEL_CHOICE_NROWS, 2,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_LABEL_STRING, "State:",
		PANEL_CHOICE_STRINGS,
			"TT_CREATED",
			"TT_SENT",
			"TT_HANDLED",
			"TT_FAILED",
			"TT_QUEUED",
			"TT_STARTED",
			"TT_REJECTED",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `disposition_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_disposition_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TOGGLE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:disposition_setting",
		XV_X, 14,
		XV_Y, 319,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_LABEL_STRING, "Disposition:",
		PANEL_CHOICE_STRINGS,
			"TT_DISCARD",
			"TT_QUEUE",
			"TT_START",
			"TT_QUEUE+TT_START",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `sender_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_sender_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:sender_textfield",
		XV_X, 47,
		XV_Y, 357,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Sender:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `sender_ptype_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_sender_ptype_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:sender_ptype_textfield",
		XV_X, 4,
		XV_Y, 377,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Sender ptype:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `arg_list' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_arg_list_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern int		list_item_select(Panel_item, char *, Xv_opaque, Panel_list_op, Event *, int);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_LIST,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:arg_list",
		XV_X, 66,
		XV_Y, 407,
		PANEL_LIST_WIDTH, 400,
		PANEL_LIST_DISPLAY_ROWS, 3,
		PANEL_LABEL_STRING, "Arguments",
		PANEL_LAYOUT, PANEL_VERTICAL,
		PANEL_READ_ONLY, FALSE,
		PANEL_CHOOSE_ONE, TRUE,
		PANEL_CHOOSE_NONE, TRUE,
		PANEL_NOTIFY_PROC, list_item_select,
		NULL);
	return obj;
}

/*
 * Create object `mode_setting' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_mode_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_CHOICE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:mode_setting",
		XV_X, 32,
		XV_Y, 508,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_CHOOSE_NONE, FALSE,
		PANEL_LABEL_STRING, "Mode:",
		PANEL_CHOICE_STRINGS,
			"TT_IN",
			"TT_OUT",
			"TT_INOUT",
			NULL,
		NULL);
	return obj;
}

/*
 * Create object `arg_vtype_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_arg_vtype_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:arg_vtype_textfield",
		XV_X, 106,
		XV_Y, 541,
		PANEL_VALUE_DISPLAY_LENGTH, 8,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "VType:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `arg_value_textfield' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_arg_value_textfield_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:arg_value_textfield",
		XV_X, 235,
		XV_Y, 541,
		PANEL_VALUE_DISPLAY_LENGTH, 20,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Value:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `kind_of_add' panel in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_kind_of_add_setting_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner,  PANEL_CHOICE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:kind_of_add_setting",
		XV_X, 275,
		XV_Y, 508,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_CHOOSE_NONE, FALSE,
		PANEL_LABEL_STRING, "Kind of Add:",
		PANEL_CHOICE_STRINGS,
			"Add",
			"Badd",
			"Iadd",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `add_button' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_add_button_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern void		pat_receive_msg_popup_add_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:add_button",
		XV_X, 134,
		XV_Y, 566,
		PANEL_LABEL_STRING, "Add",
		PANEL_NOTIFY_PROC, pat_receive_msg_popup_add_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `delete_button' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_delete_button_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern void		pat_receive_msg_popup_delete_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:delete_button",
		XV_X, 236,
		XV_Y, 566,
		PANEL_LABEL_STRING, "Delete",
		PANEL_INACTIVE, TRUE,
		PANEL_NOTIFY_PROC, pat_receive_msg_popup_delete_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `change_button' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_change_button_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern void		pat_receive_msg_popup_change_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:change_button",
		XV_X, 353,
		XV_Y, 566,
		PANEL_LABEL_STRING, "Change",
		PANEL_INACTIVE, TRUE,
		PANEL_NOTIFY_PROC, pat_receive_msg_popup_change_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `apply_button' in the specified instance.
 */
Xv_opaque
pat_receive_msg_popup_apply_button_create(pat_receive_msg_popup_objects *ip, Xv_opaque owner)
{
	extern void		pat_receive_msg_popup_apply_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pat:apply_button",
		XV_X, 249,
		XV_Y, 600,
		PANEL_LABEL_STRING, "Apply",
		PANEL_NOTIFY_PROC, pat_receive_msg_popup_apply_button_notify_callback,
		NULL);
	xv_set(owner, PANEL_DEFAULT_ITEM, obj, NULL);
	return obj;
}


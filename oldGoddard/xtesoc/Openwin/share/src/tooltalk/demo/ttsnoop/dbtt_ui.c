/*
 * display.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/canvas.h>
#include <xview/panel.h>
#include <xview/scrollbar.h>
#include <xview/svrimage.h>
#include <xview/termsw.h>
#include <xview/text.h>
#include <xview/tty.h>
#include <xview/xv_xrect.h>
#include "dbtt_ui.h"

/*
 * Create object `msg_menu' in the specified instance.
 */
Xv_opaque
dbtt_msg_menu_create(caddr_t ip, Xv_opaque owner)
{
	extern Menu		display_menu(Menu, Menu_generate);
	Xv_opaque	obj;
	
	obj = xv_create(XV_NULL, MENU_COMMAND_MENU,
		XV_KEY_DATA, INSTANCE, ip,
		MENU_GEN_PROC, display_menu,
		MENU_TITLE_ITEM, owner ? "" : "Messages",
		MENU_GEN_PIN_WINDOW, owner, "Messages",
		NULL);
	return obj;
}

/*
 * Initialize an instance of object `frame'.
 */
dbtt_frame_objects *
dbtt_frame_objects_initialize(dbtt_frame_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (dbtt_frame_objects *) calloc(1, sizeof (dbtt_frame_objects))))
		return (dbtt_frame_objects *) NULL;
	if (!ip->frame)
		ip->frame = dbtt_frame_frame_create(ip, owner);
	if (!ip->panel)
		ip->panel = dbtt_frame_panel_create(ip, ip->frame);
	if (!ip->mode_setting)
		ip->mode_setting = dbtt_frame_mode_setting_create(ip, ip->panel);
	if (!ip->clear_button)
		ip->clear_button = dbtt_frame_clear_button_create(ip, ip->panel);
	if (!ip->about_ttsnoop_button)
		ip->about_ttsnoop_button = dbtt_frame_about_ttsnoop_button_create(ip, ip->panel);
	if (!ip->display_button)
		ip->display_button = dbtt_frame_display_button_create(ip, ip->panel);
	if (!ip->send_button)
		ip->send_button = dbtt_frame_send_button_create(ip, ip->panel);
	if (!ip->receive_button)
		ip->receive_button = dbtt_frame_receive_button_create(ip, ip->panel);
	if (!ip->send_msg_button)
		ip->send_msg_button = dbtt_frame_send_msg_button_create(ip, ip->panel);
	if (!ip->textsw)
		ip->textsw = dbtt_frame_textsw_create(ip, ip->frame);
	return ip;
}

/*
 * Create object `frame' in the specified instance.
 */
Xv_opaque
dbtt_frame_frame_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	Xv_opaque		frame_image;
	static unsigned short	frame_bits[] = {
#include "ttsnoop.icon"
	};
	Xv_opaque		frame_image_mask;
	static unsigned short	frame_mask_bits[] = {
#include "ttsnoop_mask.icon"
	};
	
	frame_image = xv_create(XV_NULL, SERVER_IMAGE,
		SERVER_IMAGE_DEPTH, 1,
		SERVER_IMAGE_BITS, frame_bits,
		XV_WIDTH, 64,
		XV_HEIGHT, 64,
		NULL);
	frame_image_mask = xv_create(XV_NULL, SERVER_IMAGE,
		SERVER_IMAGE_DEPTH, 1,
		SERVER_IMAGE_BITS, frame_mask_bits,
		XV_WIDTH, 64,
		XV_HEIGHT, 64,
		NULL);
	obj = xv_create(owner, FRAME,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 688,
		XV_HEIGHT, 505,
		XV_LABEL, "ToolTalk Snooper (Demo)",
		FRAME_SHOW_FOOTER, TRUE,
		FRAME_SHOW_RESIZE_CORNER, TRUE,
		FRAME_ICON, xv_create(XV_NULL, ICON,
			ICON_IMAGE, frame_image,
			ICON_MASK_IMAGE, frame_image_mask,
			NULL),
		NULL);
	return obj;
}

/*
 * Create object `panel' in the specified instance.
 */
Xv_opaque
dbtt_frame_panel_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:panel",
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, 35,
		WIN_BORDER, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `mode_setting' in the specified instance.
 */
Xv_opaque
dbtt_frame_mode_setting_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_mode_setting_notify_callback(Panel_item, int, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_CHOICE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:mode_setting",
		XV_X, 10,
		XV_Y, 6,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_CHOOSE_NONE, FALSE,
		PANEL_NOTIFY_PROC, dbtt_frame_mode_setting_notify_callback,
		PANEL_CHOICE_STRINGS,
			"Start",
			"Stop",
			NULL,
		PANEL_VALUE, 1,
		NULL);
	return obj;
}

/*
 * Create object `clear_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_clear_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_clear_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:clear_button",
		XV_X, 103,
		XV_Y, 8,
		PANEL_LABEL_STRING, "Clear",
		PANEL_NOTIFY_PROC, dbtt_frame_clear_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `about_ttsnoop_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_about_ttsnoop_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_about_ttsnoop_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_X, 158,
		XV_Y, 8,
		PANEL_LABEL_STRING, "About TTSnoop...",
		PANEL_NOTIFY_PROC, dbtt_frame_about_ttsnoop_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `display_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_display_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_display_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:display_button",
		XV_X, 310,
		XV_Y, 8,
		PANEL_LABEL_STRING, "Display ...",
		PANEL_NOTIFY_PROC, dbtt_frame_display_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `send_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_send_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_send_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:send_button",
		XV_X, 391,
		XV_Y, 8,
		PANEL_LABEL_STRING, "Messages...",
		PANEL_NOTIFY_PROC, dbtt_frame_send_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `receive_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_receive_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern void		dbtt_frame_receive_button_notify_callback(Panel_item, Event *);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:receive_button",
		XV_X, 480,
		XV_Y, 8,
		PANEL_LABEL_STRING, "Patterns...",
		PANEL_NOTIFY_PROC, dbtt_frame_receive_button_notify_callback,
		NULL);
	return obj;
}

/*
 * Create object `send_msg_button' in the specified instance.
 */
Xv_opaque
dbtt_frame_send_msg_button_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	extern Xv_opaque	dbtt_msg_menu_create(caddr_t, Xv_opaque);
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "dbtt:send_msg_button",
		XV_X, 563,
		XV_Y, 8,
		PANEL_LABEL_STRING, "Send Message",
		PANEL_ITEM_MENU, dbtt_msg_menu_create((caddr_t) ip, ip->frame),
		NULL);
	return obj;
}

/*
 * Create object `textsw' in the specified instance.
 */
Xv_opaque
dbtt_frame_textsw_create(dbtt_frame_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, TEXTSW,
		XV_KEY_DATA, INSTANCE, ip,
		XV_X, 0,
		XV_Y, (int)xv_get(ip->panel, XV_Y) +
		      (int)xv_get(ip->panel, XV_HEIGHT),
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		OPENWIN_SHOW_BORDERS, TRUE,
		TEXTSW_BROWSING, TRUE,
		TEXTSW_DISABLE_LOAD, TRUE,
		TEXTSW_MEMORY_MAXIMUM, 0,
		NULL);
	return obj;
}

/*
 * Initialize an instance of object `about_ttsnoop_popup'.
 */
dbtt_about_ttsnoop_popup_objects *
dbtt_about_ttsnoop_popup_objects_initialize(dbtt_about_ttsnoop_popup_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (dbtt_about_ttsnoop_popup_objects *) calloc(1, sizeof (dbtt_about_ttsnoop_popup_objects))))
		return (dbtt_about_ttsnoop_popup_objects *) NULL;
	if (!ip->about_ttsnoop_popup)
		ip->about_ttsnoop_popup = dbtt_about_ttsnoop_popup_about_ttsnoop_popup_create(ip, owner);
	if (!ip->about_ttsnoop_textsw)
		ip->about_ttsnoop_textsw = dbtt_about_ttsnoop_popup_about_ttsnoop_textsw_create(ip, ip->about_ttsnoop_popup);
	return ip;
}

/*
 * Create object `about_ttsnoop_popup' in the specified instance.
 */
Xv_opaque
dbtt_about_ttsnoop_popup_about_ttsnoop_popup_create(dbtt_about_ttsnoop_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, FRAME_CMD,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 387,
		XV_HEIGHT, 226,
		XV_LABEL, "About TTSnoop",
		XV_SHOW, FALSE,
		FRAME_SHOW_FOOTER, TRUE,
		FRAME_SHOW_RESIZE_CORNER, TRUE,
		FRAME_CMD_PUSHPIN_IN, TRUE,
		NULL);
	xv_set(xv_get(obj, FRAME_CMD_PANEL), WIN_SHOW, FALSE, NULL);
	return obj;
}

/*
 * Create object `about_ttsnoop_textsw' in the specified instance.
 */
Xv_opaque
dbtt_about_ttsnoop_popup_about_ttsnoop_textsw_create(dbtt_about_ttsnoop_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, TEXTSW,
		XV_KEY_DATA, INSTANCE, ip,
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		OPENWIN_SHOW_BORDERS, TRUE,
		TEXTSW_BROWSING, TRUE,
		TEXTSW_DISABLE_LOAD, TRUE,
		NULL);
	return obj;
}





/*
 * Initialize an instance of object `ttsnoop_error_popup'.
 */
dbtt_ttsnoop_error_popup_objects *
dbtt_ttsnoop_error_popup_objects_initialize(dbtt_ttsnoop_error_popup_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (dbtt_ttsnoop_error_popup_objects *) calloc(1, sizeof (dbtt_ttsnoop_error_popup_objects))))
		return (dbtt_ttsnoop_error_popup_objects *) NULL;
	if (!ip->ttsnoop_error_popup)
		ip->ttsnoop_error_popup = dbtt_ttsnoop_error_popup_create(ip, owner);
	if (!ip->ttsnoop_error_textsw)
		ip->ttsnoop_error_textsw = dbtt_ttsnoop_error_textsw_create(ip, ip->ttsnoop_error_popup);
	return ip;
}

/*
 * Create object `ttsnoop_error_popup' in the specified instance.
 */
Xv_opaque
dbtt_ttsnoop_error_popup_create(dbtt_ttsnoop_error_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, FRAME_CMD,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 387,
		XV_HEIGHT, 226,
		XV_LABEL, "TTSnoop - Error Log",
		XV_SHOW, FALSE,
		FRAME_SHOW_FOOTER, TRUE,
		FRAME_SHOW_RESIZE_CORNER, TRUE,
		FRAME_CMD_PUSHPIN_IN, TRUE,
		NULL);
	xv_set(xv_get(obj, FRAME_CMD_PANEL), WIN_SHOW, FALSE, NULL);
	return obj;
}

/*
 * Create object `ttsnoop_error_textsw' in the specified instance.
 */
Xv_opaque
dbtt_ttsnoop_error_textsw_create(dbtt_ttsnoop_error_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, TEXTSW,
		XV_KEY_DATA, INSTANCE, ip,
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		OPENWIN_SHOW_BORDERS, TRUE,
		TEXTSW_BROWSING, TRUE,
		TEXTSW_DISABLE_LOAD, TRUE,
		NULL);
	return obj;
}





;GIL-3
(
(
	:type                   :popup-window
	:name                   send_msg_popup
	:owner                  frame
	:width                  527
	:height                 747
	:background-color       ""
	:foreground-color       ""
	:label                  "Send Message"
	:label-type             :string
	:initial-state          :invisible
	:show-footer            t
	:resizable              t
	:pinned                 t
	:done-handler           nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :control-area
	:name                   send_list_panel
	:owner                  send_msg_popup
	:help                   ""
	:x                      0
	:y                      0
	:width                  527
	:height                 159
	:background-color       ""
	:foreground-color       ""
	:initial-state          :visible
	:show-border            nil
	:menu                   nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   add_message_button
	:owner                  send_list_panel
	:help                   ""
	:x                      13
	:y                      9
	:width                  97
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Add Message"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   change_message_button
	:owner                  send_list_panel
	:help                   ""
	:x                      120
	:y                      9
	:width                  119
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Change Message"
	:label-type             :string
	:initial-state          :inactive
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   delete_message_button
	:owner                  send_list_panel
	:help                   ""
	:x                      249
	:y                      9
	:width                  112
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Delete Message"
	:label-type             :string
	:initial-state          :inactive
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   send_edit_send_contexts_button
	:owner                  send_list_panel
	:help                   "Edit Send Contexts: popup window to edit the list of contexts you wish to send."
	:x                      371
	:y                      9
	:width                  143
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Edit Send Contexts..."
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :scrolling-list
	:name                   message_list
	:owner                  send_list_panel
	:help                   ""
	:x                      8
	:y                      32
	:width                  490
	:height                 91
	:value-x                8
	:value-y                49
	:rows                   3
	:foreground-color       ""
	:label                  "Messages"
	:title                  ""
	:label-type             :string
	:layout-type            :vertical
	:read-only              nil
	:multiple-selections    nil
	:selection-required     nil
	:initial-state          :active
	:droppable              nil
	:default-drop-site      nil
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:initial-list-values    ()
	:initial-list-glyphs    ()
	:initial-selections     ()
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   label_textfield
	:owner                  send_list_panel
	:help                   ""
	:x                      97
	:y                      133
	:width                  331
	:height                 15
	:value-x                188
	:value-y                133
	:value-length           30
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Description:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :control-area
	:name                   send_msg_panel
	:owner                  send_msg_popup
	:help                   "Message
-------
To send a message, first allocate a new message,
fill in the proper information, then send it. Any
reply will show up in the same message. When
finished with the message free the storage for
the message. (for value returning requests, this
is after any needed return values are copied out;
for notifications, this is right after its been
sent),


To allocate the new message, use
tt_message_create(). It returns a \"handle\" or
\"opaque pointer\" to the message; use this handle
on succeeding calls to reference this message.


To fill in message information, use the
tt_message_*_set() calls; there is one of these
for each attribute of the message.

You should set Class, Address, Op and Args.

Send the message with tt_message_send().

"
	:x                      0
	:y                      159
	:width                  527
	:height                 588
	:background-color       ""
	:foreground-color       ""
	:initial-state          :visible
	:show-border            t
	:menu                   nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :setting
	:name                   address_setting
	:owner                  send_msg_panel
	:help                   "Address:
Because there are many types of tools and
different users will use different sets of tools
at different times, it's often impossible for the
sender of a message to identify the precise
recipient of a message.  Instead, the sender
gives an operation name which specifies the
meaning of the message and may give an object to
which the message is directed, or the object type
(otype).

Use object or otype for object oriented
messages.  If the address is handler, specify the
handler.

	TT_PROCEDURE: Op and Args supplied.

	TT_OBJECT: Object, Op, and Args
	supplied.

	TT_HANDLER: Handler, Op, and Args
	supplied.

	TT_OTYPE: Otype, Op, and Args supplied.
"
	:x                      47
	:y                      8
	:width                  443
	:height                 23
	:value-x                113
	:value-y                8
	:rows                   1
	:columns                0
	:layout-type            :horizontal
	:foreground-color       ""
	:setting-type           :exclusive
	:selection-required     t
	:label                  "Address:"
	:label-type             :string
	:notify-handler         nil
	:event-handler          nil
	:choices                ("TT_PROCEDURE" "TT_OBJECT" "TT_HANDLER" "TT_OTYPE" )
	:choice-label-types     (:string :string :string :string )
	:choice-colors          ("" "" "" "" )
	:initial-selections     (t nil nil nil )
	:initial-state          :active
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   handler_textfield
	:owner                  send_msg_panel
	:help                   "Handler:
If you know the exact procid of the handler, you
can address messages to it directly. The usual
way this would happen would be for one process to
make a general request and then pick the Handler
attribute out of the reply, directing further
messages to the same handler; this allows two
processes to rendezvous through broadcast message
passing and then go into a dialogue.
"
	:x                      48
	:y                      36
	:width                  385
	:height                 15
	:value-x                113
	:value-y                36
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Handler:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :inactive
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   handler_ptype_textfield
	:owner                  send_msg_panel
	:help                   "Handler_Ptype:
If you know the ptype of the process that will
handle the message, fill it in. You would know
the ptype if you consulted the ptype definition
to find the details of the protocol.
"
	:x                      5
	:y                      56
	:width                  428
	:height                 15
	:value-x                113
	:value-y                56
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Handler ptype:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   object_textfield
	:owner                  send_msg_panel
	:help                   "Object:
Fill in the objid of the object to which the
message is to be sent.
"
	:x                      57
	:y                      76
	:width                  376
	:height                 15
	:value-x                113
	:value-y                76
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Object:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :inactive
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   otype_textfield
	:owner                  send_msg_panel
	:help                   "OType:
Type of the object.

"
	:x                      61
	:y                      96
	:width                  372
	:height                 15
	:value-x                113
	:value-y                96
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Otype:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :inactive
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   op_textfield
	:owner                  send_msg_panel
	:help                   "Op:
Fill in the operation that describes the
notification or request being made. Consult
the otype definition for the target object to
determine the operation name.
"
	:x                      83
	:y                      116
	:width                  350
	:height                 15
	:value-x                113
	:value-y                116
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "OP:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :setting
	:name                   scope_setting
	:owner                  send_msg_panel
	:help                   "Scope:
Use scope session if messages from other
processes in the same session as your process are
desired; use scope file if messages about a file
is desired.

	TT_SESSION: Receive messages from other
	processes in your session.

	TT_FILE: Receive messages about the file
	joined.

	TT_BOTH: Receive messages about a file
	and the session.

	TT_FILE_IN_SESSION:  Receive messages for
	the file joined while in this session.
"
	:x                      62
	:y                      146
	:width                  424
	:height                 23
	:value-x                113
	:value-y                146
	:rows                   1
	:columns                0
	:layout-type            :horizontal
	:foreground-color       ""
	:setting-type           :exclusive
	:selection-required     t
	:label                  "Scope:"
	:label-type             :string
	:notify-handler         nil
	:event-handler          nil
	:choices                ("TT_SESSION" "TT_FILE" "TT_BOTH" "TT_FILE_IN_SESSION" )
	:choice-label-types     (:string :string :string :string )
	:choice-colors          ("" "" "" "" )
	:initial-selections     (t nil nil nil )
	:initial-state          :active
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   session_textfield
	:owner                  send_msg_panel
	:help                   "Session:
For scope session, specify the session
(tt_default_session() by default).

"
	:x                      48
	:y                      174
	:width                  385
	:height                 15
	:value-x                113
	:value-y                174
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Session:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   file_name_textfield
	:owner                  send_msg_panel
	:help                   "File:
For file scope, specify the name of the file.
"
	:x                      34
	:y                      194
	:width                  399
	:height                 15
	:value-x                113
	:value-y                194
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "File Name:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :inactive
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :setting
	:name                   class_setting
	:owner                  send_msg_panel
	:help                   "Class:
Use class request for messages that return
values, or for which you want feedback telling
you when the message is handled or queued, or
when a process is started to handle the request.
Use class notification for messages that just
notify other processes of events.

	TT_NOTICE: Notice are messages that
	inform other tools that some event has
	happened. Tools sending notices don't
	expect replies back; the sender is just
	letting the other tools know what's
	happening.

	TT_REQUEST: Requests are messages that
	ask another tool to perform an action.
	Often, but not always, the requesting
	tool expects some value returned. This
	value is returned as a reply to the
	request. Even if no value is returned,
	the tool processing the request sends a
	reply indicating success or failure.
"
	:x                      64
	:y                      224
	:width                  232
	:height                 23
	:value-x                113
	:value-y                224
	:rows                   1
	:columns                0
	:layout-type            :horizontal
	:foreground-color       ""
	:setting-type           :exclusive
	:selection-required     t
	:label                  "Class:"
	:label-type             :string
	:notify-handler         nil
	:event-handler          nil
	:choices                ("TT_NOTICE" "TT_REQUEST" )
	:choice-label-types     (:string :string )
	:choice-colors          ("" "" )
	:initial-selections     (t nil )
	:initial-state          :active
	:user-data              ()
	:actions                ()
)
(
	:type                   :setting
	:name                   disposition_setting
	:owner                  send_msg_panel
	:help                   "Disposition:
Specifies the action to take if the message
cannot be handled by any running process. Queue
if the message should be queued until a process
of the Handler_Ptype registers. Start if a
process of the Handler_Ptype should be started.
	TT_QUEUE: Queue the message until a
	process of the proper ptype receives the
	message.

	TT_START: Attempt to start a process of
	the proper ptype if none is running.

Note that Tt_disposition values can be added
together, so that TT_QUEUE+TT_START means both to
queue the message and to try to start a process.
This can be useful if the start can fail (or be
vetoed by the user), to ensure the message is
processed as soon as an eligible process does
start.
"
	:x                      22
	:y                      262
	:width                  499
	:height                 23
	:value-x                113
	:value-y                262
	:rows                   1
	:columns                0
	:layout-type            :horizontal
	:foreground-color       ""
	:setting-type           :exclusive
	:selection-required     t
	:label                  "Disposition:"
	:label-type             :string
	:notify-handler         nil
	:event-handler          nil
	:choices                ("TT_DISCARD" "TT_QUEUE" "TT_START" "TT_QUEUE+TT_START" )
	:choice-label-types     (:string :string :string :string )
	:choice-colors          ("" "" "" "" )
	:initial-selections     (t nil nil nil )
	:initial-state          :active
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   sender_ptype_textfield
	:owner                  send_msg_panel
	:help                   "Sender_Ptype:
The ptype of the process that sent the message.
"
	:x                      12
	:y                      300
	:width                  421
	:height                 15
	:value-x                113
	:value-y                300
	:value-length           40
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Sender ptype:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :message
	:name                   status_message
	:owner                  send_msg_panel
	:help                   "Status:
A unique number indicating the results of the
message returned by the replier. This number must
be larger than TT_ERR_LAST (2047).
"
	:x                      55
	:y                      325
	:width                  50
	:height                 13
	:foreground-color       ""
	:label                  "Status: "
	:label-type             :string
	:label-bold             t
	:initial-state          :active
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :message
	:name                   status_discription_message
	:owner                  send_msg_panel
	:help                   "Status string:
A text description of the Status of the message.
"
	:x                      115
	:y                      325
	:width                  146
	:height                 13
	:foreground-color       ""
	:label                  "Set by receiving process"
	:label-type             :string
	:label-bold             nil
	:initial-state          :active
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :scrolling-list
	:name                   arg_list
	:owner                  send_msg_panel
	:help                   "Args:
Fill in any arguments specific to the operation.
Use tt_message_arg_add to add each argument in
turn. For each argument, you must specify: mode
(in, out, or inout), type, and if the mode is in
or inout, the value.
"
	:x                      54
	:y                      353
	:width                  400
	:height                 91
	:value-x                54
	:value-y                370
	:rows                   3
	:foreground-color       ""
	:label                  "Arguments"
	:title                  ""
	:label-type             :string
	:layout-type            :vertical
	:read-only              nil
	:multiple-selections    nil
	:selection-required     nil
	:initial-state          :active
	:droppable              nil
	:default-drop-site      nil
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:initial-list-values    ()
	:initial-list-glyphs    ()
	:initial-selections     ()
	:user-data              ()
	:actions                ()
)
(
	:type                   :setting
	:name                   mode_setting
	:owner                  send_msg_panel
	:help                   "Mode:
Specifies the mode of a message argument.
Possible values and meanings are:
	TT_IN: The argument is written by the
	sender and read by the handler and any
	observers.


	TT_OUT: The argument is written by the
	handler and read by the sender and any
	reply observers.


	TT_INOUT: The argument is written and
	read by the sender and the handler and
	any observers.
"
	:x                      141
	:y                      454
	:width                  247
	:height                 23
	:value-x                188
	:value-y                454
	:rows                   1
	:columns                0
	:layout-type            :horizontal
	:foreground-color       ""
	:setting-type           :exclusive
	:selection-required     t
	:label                  "Mode:"
	:label-type             :string
	:notify-handler         nil
	:event-handler          nil
	:choices                ("TT_IN" "TT_OUT" "TT_INOUT" )
	:choice-label-types     (:string :string :string )
	:choice-colors          ("" "" "" )
	:initial-selections     (t nil nil )
	:initial-state          :active
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   type_textfield
	:owner                  send_msg_panel
	:help                   "Type:
String indicating the type. Common values are
\"string\" and \"int\" although developers can create
there own.
"
	:x                      143
	:y                      483
	:width                  109
	:height                 15
	:value-x                188
	:value-y                483
	:value-length           8
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Type:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :text-field
	:name                   value_textfield
	:owner                  send_msg_panel
	:help                   "Value:
Actual data of the value dependant on the type.
"
	:x                      138
	:y                      504
	:width                  210
	:height                 15
	:value-x                188
	:value-y                504
	:value-length           20
	:stored-length          80
	:rows                   3
	:foreground-color       ""
	:text-type              :alphanumeric
	:label                  "Value:"
	:label-type             :string
	:layout-type            :horizontal
	:value-underlined       t
	:initial-value          ""
	:initial-state          :active
	:read-only              nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   add_button
	:owner                  send_msg_panel
	:help                   "Add Button:
Add an argument with the Mode, Type and Value.
"
	:x                      132
	:y                      529
	:width                  42
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Add"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   delete_button
	:owner                  send_msg_panel
	:help                   "Delete Button:
Delete the selected argument."
	:x                      224
	:y                      529
	:width                  57
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Delete"
	:label-type             :string
	:initial-state          :inactive
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   change_button
	:owner                  send_msg_panel
	:help                   "Change Button:
Change the selected argument to the current
values of Mode, Type and Value.
"
	:x                      331
	:y                      529
	:width                  64
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Change"
	:label-type             :string
	:initial-state          :inactive
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                ()
)
(
	:type                   :button
	:name                   send_button
	:owner                  send_msg_panel
	:help                   "Send this message. Also print out to standard output the Tooltalk calls that will be made to
create and send this message."
	:x                      212
	:y                      563
	:width                  103
	:height                 19
	:constant-width         nil
	:button-type            :normal
	:foreground-color       ""
	:label                  "Send Message"
	:label-type             :string
	:initial-state          :active
	:menu                   nil
	:notify-handler         nil
	:event-handler          nil
	:user-data              ()
	:actions                (
		(
		:from                   (send_msg_popup send_button)
		:when                   (Notify )
		:to                     (send_msg_popup send_button)
		:function_type          ExecuteCode
		:arg_type               (:string)
		:action                 ("send_message(ip);")
		)
	)
)
)

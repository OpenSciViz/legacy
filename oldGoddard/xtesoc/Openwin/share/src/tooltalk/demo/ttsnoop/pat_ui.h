#ifndef	pat_HEADER
#define	pat_HEADER

/*
 * pat_ui.h - User interface object and function declarations.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

extern Attr_attribute	INSTANCE;


typedef struct {
	Xv_opaque	receive_msg_popup;
	Xv_opaque	controls1;
	Xv_opaque	pat_edit_receive_contexts;
	Xv_opaque	receive_msg_panel;
	Xv_opaque	address_setting;
	Xv_opaque	object_textfield;
	Xv_opaque	otype_textfield;
	Xv_opaque	op_textfield;
	Xv_opaque	scope_setting;
	Xv_opaque	file_name_textfield;
	Xv_opaque	session_textfield;
	Xv_opaque	category_textfield;
	Xv_opaque	class_setting;
	Xv_opaque	state_setting;
	Xv_opaque	disposition_setting;
	Xv_opaque	sender_textfield;
	Xv_opaque	sender_ptype_textfield;
	Xv_opaque	arg_list;
	Xv_opaque	mode_setting;
	Xv_opaque	arg_vtype_textfield;
	Xv_opaque	arg_value_textfield;
	Xv_opaque	kind_of_add;
	Xv_opaque	add_button;
	Xv_opaque	delete_button;
	Xv_opaque	change_button;
	Xv_opaque	apply_button;
} pat_receive_msg_popup_objects;

extern pat_receive_msg_popup_objects	*pat_receive_msg_popup_objects_initialize(pat_receive_msg_popup_objects *, Xv_opaque);

extern Xv_opaque	pat_receive_msg_popup_receive_msg_popup_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_controls1_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_pat_edit_receive_contexts_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_receive_msg_panel_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_address_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_object_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_otype_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_op_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_scope_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_file_name_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_session_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_category_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_class_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_state_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_disposition_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_sender_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_sender_ptype_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_arg_list_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_mode_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_arg_vtype_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_arg_value_textfield_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_kind_of_add_setting_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_add_button_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_delete_button_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_change_button_create(pat_receive_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	pat_receive_msg_popup_apply_button_create(pat_receive_msg_popup_objects *, Xv_opaque);
#endif

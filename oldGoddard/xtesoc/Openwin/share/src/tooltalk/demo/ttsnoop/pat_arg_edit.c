/*  
 * pat_arg_edit.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <xview/xview.h>
#include <xview/panel.h>
#include <desktop/tt_c.h>
#include "ttsnoop.h"
#include "arg_edit.h"

pat_set_arg_num(int num, pat_receive_msg_popup_objects *ip)
{
	Arg_list *item;
	int	  last;

	last = xv_get(ip->mode_setting, PANEL_CLIENT_DATA);
	if(last != -1)
	{
		xv_set(ip->arg_list,
			PANEL_LIST_SELECTED,	last, FALSE,
			NULL);
	}

	if(num == -1)
	{
		xv_set(ip->arg_vtype_textfield, PANEL_VALUE,"", NULL);
		xv_set(ip->arg_value_textfield, PANEL_VALUE,"", NULL);
		xv_set(ip->delete_button, PANEL_INACTIVE, TRUE, NULL);
		xv_set(ip->change_button, PANEL_INACTIVE, TRUE, NULL);
		xv_set(ip->kind_of_add, PANEL_VALUE, ADD, NULL);
	}
	else
	{
		item = (Arg_list *)xv_get(ip->arg_list,
			PANEL_LIST_CLIENT_DATA, num);
		xv_set(ip->arg_vtype_textfield, PANEL_VALUE,item->type, NULL);
		xv_set(ip->arg_value_textfield, PANEL_VALUE,item->value, NULL);
		xv_set(ip->mode_setting, PANEL_VALUE, item->mode-1, NULL);
		xv_set(ip->delete_button, PANEL_INACTIVE, FALSE, NULL);
		xv_set(ip->change_button, PANEL_INACTIVE, FALSE, NULL);
		xv_set(ip->kind_of_add, PANEL_VALUE, item->add_kind, NULL);
	}
	xv_set(ip->mode_setting, PANEL_CLIENT_DATA, num, NULL);
}

pat_add_arg(pat_receive_msg_popup_objects *ip)
{
	Arg_list	*new;
	char		buffer[1024];
	int		nrows = xv_get(ip->arg_list, PANEL_LIST_NROWS);
	int		mode = xv_get(ip->mode_setting, PANEL_VALUE);
	int		i;

	new = (Arg_list *)malloc(sizeof(Arg_list));

	new->type = strdup((char *)xv_get(ip->arg_vtype_textfield,
					  PANEL_VALUE));
	new->value = strdup((char *)xv_get(ip->arg_value_textfield,
					   PANEL_VALUE));
	new->mode = mode_name(xv_get(ip->mode_setting,
				     PANEL_CHOICE_STRING,
				     mode));

	new->add_kind = xv_get(ip->kind_of_add, PANEL_VALUE);

	sprintf(buffer, "%5s %5s(mode) %5s(vtype) %5s(value)\0",
		        (new->add_kind == ADD) ? "ADD" : ((new->add_kind == BADD) ? "BADD" : "IADD"),
			mode_val(new->mode),
			new->type,
			new->value);

	pat_set_arg_num(-1, ip);

	xv_set(ip->arg_list,
		PANEL_LIST_INSERT,	nrows,
		PANEL_LIST_STRING,	nrows, buffer,
		PANEL_LIST_CLIENT_DATA,	nrows, new,
		NULL);
	xv_set(ip->arg_list, PANEL_LIST_SELECT, nrows, TRUE, NULL);
	pat_set_arg_num(nrows, ip);
}

pat_change_arg(pat_receive_msg_popup_objects *ip)
{
	Arg_list	*item;
	char		buffer[1024];
	int	selected = xv_get(ip->mode_setting, PANEL_CLIENT_DATA);
	int		mode = xv_get(ip->mode_setting, PANEL_VALUE);
	int		i;

	item = (Arg_list *)xv_get(ip->arg_list,
		PANEL_LIST_CLIENT_DATA, selected);

	free(item->type);
	free(item->value);

	item->type = strdup((char *)xv_get(ip->arg_vtype_textfield,
					   PANEL_VALUE));
	item->value = strdup((char *)xv_get(ip->arg_value_textfield,
					    PANEL_VALUE));
	item->mode = mode_name(xv_get(ip->mode_setting,
				      PANEL_CHOICE_STRING,
				      xv_get(ip->mode_setting, PANEL_VALUE)));

	item->add_kind = xv_get(ip->kind_of_add, PANEL_VALUE);

	sprintf(buffer, "%5s %5s(mode) %5s(vtype) %5s(value)\0",
		        (item->add_kind == ADD) ? "ADD" : ((item->add_kind == BADD) ? "BADD" : "IADD"),
			mode_val(item->mode),
			item->type,
			item->value);

	xv_set(ip->arg_list,
		PANEL_LIST_STRING,	selected, buffer,
		PANEL_LIST_CLIENT_DATA,	selected, item,
		NULL);
}

pat_delete_arg(pat_receive_msg_popup_objects *ip)
{
	Arg_list	*item;
	char		buffer[1024];
	int	selected = xv_get(ip->mode_setting, PANEL_CLIENT_DATA);

	item = (Arg_list *)xv_get(ip->arg_list,
		PANEL_LIST_CLIENT_DATA, selected);

	free(item->type);
	free(item->value);
	free(item);

	pat_set_arg_num(-1, ip);

	xv_set(ip->arg_list,
		PANEL_LIST_DELETE,	selected,
		NULL);
}


/* routines below are used for the received contexts popup */

context_set_arg_num(int num, pattern_contexts_pattern_contexts_popup_objects *ip)
{
	Arg_list *item;
	int	  last;

	last = xv_get(ip->receive_contexts_list, PANEL_CLIENT_DATA);
	if(last != -1)
	{
		xv_set(ip->receive_contexts_list,
			PANEL_LIST_SELECTED,	last, FALSE,
			NULL);
	}

	if(num == -1)
	{
		xv_set(ip->context_kind_of_add, PANEL_LIST_SELECT, ADD, TRUE, NULL);
		xv_set(ip->context_value, PANEL_VALUE,"", NULL);
		xv_set(ip->context_delete_button, PANEL_INACTIVE, TRUE, NULL);
		xv_set(ip->context_change_button, PANEL_INACTIVE, TRUE, NULL);
	}
	else
	{
		item = (Arg_list *)xv_get(ip->receive_contexts_list,
			PANEL_LIST_CLIENT_DATA, num);
		xv_set(ip->context_kind_of_add, PANEL_LIST_SELECT, item->add_kind, TRUE, NULL);
		xv_set(ip->context_value, PANEL_VALUE,item->value, NULL);
		xv_set(ip->context_name, PANEL_VALUE, item->slotname, NULL);
		xv_set(ip->context_delete_button, PANEL_INACTIVE, FALSE, NULL);
		xv_set(ip->context_change_button, PANEL_INACTIVE, FALSE, NULL);
	}
	xv_set(ip->receive_contexts_list, PANEL_CLIENT_DATA, num, NULL);
}

context_add_arg(pattern_contexts_pattern_contexts_popup_objects *ip)
{
	Arg_list	*new;
	char		buffer[1024];
	int		nrows = xv_get(ip->receive_contexts_list, PANEL_LIST_NROWS);
	int		i;

	new = (Arg_list *)malloc(sizeof(Arg_list));

	new->slotname = strdup((char *)xv_get(ip->context_name, PANEL_VALUE));

	new->value = strdup((char *)xv_get(ip->context_value, PANEL_VALUE));

	new->add_kind = xv_get(ip->context_kind_of_add, PANEL_VALUE);

	sprintf(buffer, "%s %s(slotname) %s(value)\0",
	       (new->add_kind == ADD) ? "ADD" : ((new->add_kind == BADD) ? "BADD" : "IADD"),
		new->slotname,
		new->value);

	context_set_arg_num(-1, ip);

	xv_set(ip->receive_contexts_list,
		PANEL_LIST_INSERT,	nrows,
		PANEL_LIST_STRING,	nrows, buffer,
		PANEL_LIST_CLIENT_DATA,	nrows, new,
		NULL);
	xv_set(ip->receive_contexts_list, PANEL_LIST_SELECT, nrows, TRUE, NULL);
	context_set_arg_num(nrows, ip);
}

context_change_arg(pattern_contexts_pattern_contexts_popup_objects *ip)
{
	Arg_list	*item;
	char		buffer[1024];
	int	selected = xv_get(ip->receive_contexts_list, PANEL_CLIENT_DATA);
	int		i;

	item = (Arg_list *)xv_get(ip->receive_contexts_list,
				  PANEL_LIST_CLIENT_DATA, 1);

	free(item->slotname);
	free(item->value);

	item->slotname = strdup((char *)xv_get(ip->context_name, PANEL_VALUE));

	item->add_kind = xv_get(ip->context_kind_of_add, PANEL_VALUE);

	item->value = strdup((char *)xv_get(ip->context_value, PANEL_VALUE));

	sprintf(buffer, "%s %s(slotname) %s(value)\0",
	       (item->add_kind == ADD) ? "ADD" : ((item->add_kind == BADD) ? "BADD" : "IADD"),
		item->slotname,
		item->value);

	xv_set(ip->receive_contexts_list, PANEL_LIST_STRING, selected, buffer,
		PANEL_LIST_CLIENT_DATA,	selected, item,
		NULL);
}

context_delete_arg(pattern_contexts_pattern_contexts_popup_objects *ip)
{
	Arg_list	*item;
	char		buffer[1024];
	int	selected = xv_get(ip->receive_contexts_list, PANEL_CLIENT_DATA);

	item = (Arg_list *)xv_get(ip->receive_contexts_list,
				  PANEL_LIST_CLIENT_DATA, 1);

	free(item->slotname);
	free(item->value);
	free(item);

	context_set_arg_num(-1, ip);

	xv_set(ip->receive_contexts_list, PANEL_LIST_DELETE, 1, NULL);
}

/*  
 * display.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <xview/xview.h>
#include <stdio.h>
#include <stdarg.h>
#include <values.h>

tprintf(Xv_opaque textsw, char *format, ...)
{
	char	buffer[2000];
	va_list	ap;

	va_start(ap, format);
	vsprintf(buffer, format, ap);
	va_end(ap);

	textsw_insert(textsw, buffer, strlen(buffer));
}


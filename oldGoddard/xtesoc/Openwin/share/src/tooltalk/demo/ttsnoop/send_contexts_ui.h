#ifndef	send_contexts_HEADER
#define	send_contexts_HEADER

/*
 * send_contexts_ui.h - User interface object and function declarations.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

extern Attr_attribute	INSTANCE;


typedef struct {
	Xv_opaque	send_contexts_popup;
	Xv_opaque	controls1;
	Xv_opaque	send_contexts_list;
	Xv_opaque	context_name;
	Xv_opaque	context_kind_of_add;
	Xv_opaque	context_value;
	Xv_opaque	context_add_button;
	Xv_opaque	context_delete_button;
	Xv_opaque	context_change_button;
} send_contexts_send_contexts_popup_objects;

extern send_contexts_send_contexts_popup_objects	*send_contexts_send_contexts_popup_objects_initialize(send_contexts_send_contexts_popup_objects *, Xv_opaque);

extern Xv_opaque	send_contexts_send_contexts_popup_send_contexts_popup_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_controls1_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_send_contexts_list_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_name_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_kind_of_add_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_value_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_add_button_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_delete_button_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
extern Xv_opaque	send_contexts_send_contexts_popup_context_change_button_create(send_contexts_send_contexts_popup_objects *, Xv_opaque);
#endif

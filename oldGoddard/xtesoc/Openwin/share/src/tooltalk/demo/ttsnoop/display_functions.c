/*  
 * display_functions.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"

extern int Display_value;
extern int Display_reset_value;

void
set_display_value(display_display_popup_objects *ip)
{
        Display_reset_value = Display_value = xv_get(ip->display_setting,
						     PANEL_VALUE);
}

void
reset_display_value(display_display_popup_objects *ip)
{
        Display_value = Display_reset_value;
	xv_set(ip->display_setting, PANEL_VALUE, Display_value, NULL);
}

/* 
 * tt_enum_xlate.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved. 
 */

#include "tt_enum_xlate.h"

Scope_xlate	scope_xlate[] =
{
	{TT_SESSION,		"TT_SESSION"},
	{TT_FILE,		"TT_FILE"},
	{TT_BOTH,		"TT_BOTH"},
	{TT_FILE_IN_SESSION,	"TT_FILE_IN_SESSION"}
};

Class_xlate		class_xlate[] =
{
	{TT_NOTICE,		"TT_NOTICE"},
	{TT_REQUEST,		"TT_REQUEST"}
};

Disposition_xlate	disposition_xlate[] =
{
	{TT_DISCARD,		"TT_DISCARD"},
	{TT_QUEUE,		"TT_QUEUE"},
	{TT_START,		"TT_START"},
	{TT_QUEUE+TT_START,	"TT_QUEUE+TT_START"}
};

Address_xlate		address_xlate[] =
{
	{TT_PROCEDURE,		"TT_PROCEDURE"},
	{TT_OBJECT,		"TT_OBJECT"},
	{TT_HANDLER,		"TT_HANDLER"},
	{TT_OTYPE,		"TT_OTYPE"}
};

State_xlate		state_xlate[] =
{
	{TT_CREATED,		"TT_CREATED"},
	{TT_SENT,		"TT_SENT"},
	{TT_HANDLED,		"TT_HANDLED"},
	{TT_FAILED,		"TT_FAILED"},
	{TT_QUEUED,		"TT_QUEUED"},
	{TT_STARTED,		"TT_STARTED"},
	{TT_REJECTED,		"TT_REJECTED"}
};

Mode_xlate		mode_xlate[] =
{
	{TT_IN,			"TT_IN"},
	{TT_OUT,		"TT_OUT"},
	{TT_INOUT,		"TT_INOUT"}
};

Add_Kind_xlate		add_kind_xlate[] =
{
  {ADD, "Add"},
  {BADD, "Badd"},
  {IADD, "Iadd"}

};

Category_xlate		category_xlate[] =
{
	{TT_OBSERVE,		"TT_OBSERVE"},
	{TT_HANDLE,		"TT_HANDLE"},
};

Tt_scope
scope_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(scope_xlate)/sizeof(Scope_xlate); i++)
	{
		if(strcmp(scope_xlate[i].name, name) == 0)
		{
			return(scope_xlate[i].val);
		}
	}
	return(scope_xlate[sizeof(scope_xlate)/sizeof(Scope_xlate)-1].val);
}

char	*
scope_val(Tt_scope val)
{
	int	i;

	for(i = 0; i < sizeof(scope_xlate)/sizeof(Scope_xlate); i++)
	{
		if(scope_xlate[i].val == val)
		{
			return(scope_xlate[i].name);
		}
	}
	return("(undefined)");
}

Tt_class
class_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(class_xlate)/sizeof(Class_xlate); i++)
	{
		if(strcmp(class_xlate[i].name, name) == 0)
		{
			return(class_xlate[i].val);
		}
	}
	return(class_xlate[sizeof(class_xlate)/sizeof(Class_xlate)-1].val);
}

char	*
class_val(Tt_class val)
{
	int	i;

	for(i = 0; i < sizeof(class_xlate)/sizeof(Class_xlate); i++)
	{
		if(class_xlate[i].val == val)
		{
			return(class_xlate[i].name);
		}
	}
	return("(undefined)");
}

Tt_disposition
disposition_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(disposition_xlate)/sizeof(Disposition_xlate); i++)
	{
		if(strcmp(disposition_xlate[i].name, name) == 0)
		{
			return(disposition_xlate[i].val);
		}
	}
	return(disposition_xlate[sizeof(disposition_xlate)/sizeof(Disposition_xlate)-1].val);
}

char	*
disposition_val(Tt_disposition val)
{
	int	i;

	for(i = 0; i < sizeof(disposition_xlate)/sizeof(Disposition_xlate); i++)
	{
		if(disposition_xlate[i].val == val)
		{
			return(disposition_xlate[i].name);
		}
	}
	return("(undefined)");
}

Tt_mode
mode_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(mode_xlate)/sizeof(Mode_xlate); i++)
	{
		if(strcmp(mode_xlate[i].name, name) == 0)
		{
			return(mode_xlate[i].val);
		}
	}
	return(mode_xlate[sizeof(mode_xlate)/sizeof(Mode_xlate)-1].val);
}

char	*
mode_val(Tt_mode val)
{
	int	i;

	for(i = 0; i < sizeof(mode_xlate)/sizeof(Mode_xlate); i++)
	{
		if(mode_xlate[i].val == val)
		{
			return(mode_xlate[i].name);
		}
	}
	return("(undefined)");
}

char	*
add_kind_val(KindOfAdd val)
{
	int	i;

	for(i = 0; i < sizeof(add_kind_xlate)/sizeof(Add_Kind_xlate); i++)
	{
		if(add_kind_xlate[i].val == val)
		{
			return(add_kind_xlate[i].name);
		}
	}
	return("(undefined)");
}

KindOfAdd
add_kind_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(add_kind_xlate)/sizeof(Add_Kind_xlate); i++)
	{
		if(strcmp(add_kind_xlate[i].name, name) == 0)
		{
			return(add_kind_xlate[i].val);
		}
	}
	return(add_kind_xlate[sizeof(add_kind_xlate)/sizeof(Add_Kind_xlate)-1].val);
}

Tt_address
address_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(address_xlate)/sizeof(Address_xlate); i++)
	{
		if(strcmp(address_xlate[i].name, name) == 0)
		{
			return(address_xlate[i].val);
		}
	}
	return(address_xlate[sizeof(address_xlate)/sizeof(Address_xlate)-1].val);
}

char	*
address_val(Tt_address val)
{
	int	i;

	for(i = 0; i < sizeof(address_xlate)/sizeof(Address_xlate); i++)
	{
		if(address_xlate[i].val == val)
		{
			return(address_xlate[i].name);
		}
	}
	return("(undefined)");
}

Tt_state
state_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(state_xlate)/sizeof(State_xlate); i++)
	{
		if(strcmp(state_xlate[i].name, name) == 0)
		{
			return(state_xlate[i].val);
		}
	}
	return(state_xlate[sizeof(state_xlate)/sizeof(State_xlate)-1].val);
}

char	*
state_val(Tt_state val)
{
	int	i;

	for(i = 0; i < sizeof(state_xlate)/sizeof(State_xlate); i++)
	{
		if(state_xlate[i].val == val)
		{
			return(state_xlate[i].name);
		}
	}
	return("(undefined)");
}

Tt_category
category_name(char *name)
{
	int	i;

	for(i = 0; i < sizeof(category_xlate)/sizeof(Category_xlate); i++)
	{
		if(strcmp(category_xlate[i].name, name) == 0)
		{
			return(category_xlate[i].val);
		}
	}
	return(category_xlate[sizeof(category_xlate)/sizeof(Category_xlate)-1].val);
}

char	*
category_val(Tt_category val)
{
	int	i;

	for(i = 0; i < sizeof(category_xlate)/sizeof(Category_xlate); i++)
	{
		if(category_xlate[i].val == val)
		{
			return(category_xlate[i].name);
		}
	}
	return("(undefined)");
}

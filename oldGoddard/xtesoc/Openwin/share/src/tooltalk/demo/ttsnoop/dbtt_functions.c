/*  
 * dbtt_functions.c
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "ttsnoop.h"

void
clear_copyright(dbtt_frame_objects *ip)
{
	static int clear;

	if (!clear) {
		clear = 1;
		textsw_reset(ip->textsw, 0, 0);
	}
}

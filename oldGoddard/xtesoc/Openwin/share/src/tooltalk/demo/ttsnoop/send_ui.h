#ifndef	send_HEADER
#define	send_HEADER

/*
 * send_ui.h - User interface object and function declarations.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

extern Attr_attribute	INSTANCE;


typedef struct {
	Xv_opaque	send_msg_popup;
	Xv_opaque	send_list_panel;
	Xv_opaque	add_message_button;
	Xv_opaque	change_message_button;
	Xv_opaque	delete_message_button;
	Xv_opaque	send_edit_send_contexts_button;
	Xv_opaque	message_list;
	Xv_opaque	label_textfield;
	Xv_opaque	send_msg_panel;
	Xv_opaque	address_setting;
	Xv_opaque	handler_textfield;
	Xv_opaque	handler_ptype_textfield;
	Xv_opaque	object_textfield;
	Xv_opaque	otype_textfield;
	Xv_opaque	op_textfield;
	Xv_opaque	scope_setting;
	Xv_opaque	session_textfield;
	Xv_opaque	file_name_textfield;
	Xv_opaque	class_setting;
	Xv_opaque	disposition_setting;
	Xv_opaque	sender_ptype_textfield;
/*	Xv_opaque	status_message;
	Xv_opaque	status_discription_message; */
	Xv_opaque	arg_list;
	Xv_opaque	mode_setting;
	Xv_opaque	type_textfield;
	Xv_opaque	value_textfield;
	Xv_opaque	kind_of_add;
	Xv_opaque	add_button;
	Xv_opaque	delete_button;
	Xv_opaque	change_button;
	Xv_opaque	send_button;
} send_send_msg_popup_objects;

extern send_send_msg_popup_objects	*send_send_msg_popup_objects_initialize(send_send_msg_popup_objects *, Xv_opaque);

extern Xv_opaque	send_send_msg_popup_send_msg_popup_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_send_list_panel_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_add_message_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_change_message_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_delete_message_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_send_edit_send_contexts_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_message_list_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_label_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_send_msg_panel_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_address_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_handler_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_handler_ptype_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_object_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_otype_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_op_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_scope_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_session_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_file_name_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_class_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_disposition_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_sender_ptype_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_arg_list_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_mode_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_type_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_value_textfield_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_kind_of_add_setting_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_add_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_delete_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_change_button_create(send_send_msg_popup_objects *, Xv_opaque);
extern Xv_opaque	send_send_msg_popup_send_button_create(send_send_msg_popup_objects *, Xv_opaque);
#endif

#ifndef	dbtt_HEADER
#define	dbtt_HEADER

/*
 * dbtt_ui.h - User interface object and function declarations.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

extern Attr_attribute	INSTANCE;

extern Xv_opaque	dbtt_msg_menu_create(caddr_t, Xv_opaque);

typedef struct {
	Xv_opaque	frame;
	Xv_opaque	panel;
	Xv_opaque	mode_setting;
	Xv_opaque	clear_button;
	Xv_opaque	about_ttsnoop_button;
	Xv_opaque	display_button;
	Xv_opaque	send_button;
	Xv_opaque	receive_button;
	Xv_opaque	send_msg_button;
	Xv_opaque	textsw;
} dbtt_frame_objects;

extern dbtt_frame_objects	*dbtt_frame_objects_initialize(dbtt_frame_objects *, Xv_opaque);

extern Xv_opaque	dbtt_frame_frame_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_panel_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_mode_setting_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_clear_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_about_ttsnoop_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_display_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_send_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_receive_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_send_msg_button_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_textsw_create(dbtt_frame_objects *, Xv_opaque);
extern Xv_opaque	dbtt_frame_error_popup_create(dbtt_frame_objects *, Xv_opaque);

typedef struct {
	Xv_opaque	about_ttsnoop_popup;
	Xv_opaque	about_ttsnoop_textsw;
} dbtt_about_ttsnoop_popup_objects;

extern dbtt_about_ttsnoop_popup_objects	*dbtt_about_ttsnoop_popup_objects_initialize(dbtt_about_ttsnoop_popup_objects *, Xv_opaque);

extern Xv_opaque	dbtt_about_ttsnoop_popup_about_ttsnoop_popup_create(dbtt_about_ttsnoop_popup_objects *, Xv_opaque);
extern Xv_opaque	dbtt_about_ttsnoop_popup_about_ttsnoop_textsw_create(dbtt_about_ttsnoop_popup_objects *, Xv_opaque);

typedef struct {
	Xv_opaque	ttsnoop_error_popup;
	Xv_opaque	ttsnoop_error_textsw;
} dbtt_ttsnoop_error_popup_objects;

extern dbtt_ttsnoop_error_popup_objects *dbtt_ttsnoop_error_popup_objects_initialize(dbtt_ttsnoop_error_popup_objects *, Xv_opaque);

extern Xv_opaque	dbtt_ttsnoop_error_popup_create(dbtt_ttsnoop_error_popup_objects *, Xv_opaque);
extern Xv_opaque	dbtt_ttsnoop_error_textsw_create(dbtt_ttsnoop_error_popup_objects *, Xv_opaque);



#endif

/*
 * pattern_contexts_ui.c - User interface object initialization functions.
 *
 * Copyright (c) 1992 Sun Microsystems, Inc.  All rights reserved.
 */

#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/canvas.h>
#include <xview/panel.h>
#include <xview/scrollbar.h>
#include <xview/svrimage.h>
#include <xview/termsw.h>
#include <xview/text.h>
#include <xview/tty.h>
#include <xview/xv_xrect.h>
#include "pattern_contexts_ui.h"

/*
 * Initialize an instance of object `pattern_contexts_popup'.
 */
pattern_contexts_pattern_contexts_popup_objects *
pattern_contexts_pattern_contexts_popup_objects_initialize(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	if (!ip && !(ip = (pattern_contexts_pattern_contexts_popup_objects *) calloc(1, sizeof (pattern_contexts_pattern_contexts_popup_objects))))
		return (pattern_contexts_pattern_contexts_popup_objects *) NULL;
	if (!ip->pattern_contexts_popup)
		ip->pattern_contexts_popup = pattern_contexts_pattern_contexts_popup_pattern_contexts_popup_create(ip, owner);
	if (!ip->controls1)
		ip->controls1 = pattern_contexts_pattern_contexts_popup_controls1_create(ip, ip->pattern_contexts_popup);
	if (!ip->receive_contexts_list)
		ip->receive_contexts_list = pattern_contexts_pattern_contexts_popup_receive_contexts_list_create(ip, ip->controls1);
	if (!ip->context_name)
		ip->context_name = pattern_contexts_pattern_contexts_popup_context_name_create(ip, ip->controls1);
	if (!ip->context_kind_of_add)
		ip->context_kind_of_add = pattern_contexts_pattern_contexts_popup_context_kind_of_add_create(ip, ip->controls1);
	if (!ip->context_value)
		ip->context_value = pattern_contexts_pattern_contexts_popup_context_value_create(ip, ip->controls1);
	if (!ip->context_add_button)
		ip->context_add_button = pattern_contexts_pattern_contexts_popup_context_add_button_create(ip, ip->controls1);
	if (!ip->context_delete_button)
		ip->context_delete_button = pattern_contexts_pattern_contexts_popup_context_delete_button_create(ip, ip->controls1);
	if (!ip->context_change_button)
		ip->context_change_button = pattern_contexts_pattern_contexts_popup_context_change_button_create(ip, ip->controls1);
	return ip;
}

/*
 * Create object `pattern_contexts_popup' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_pattern_contexts_popup_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, FRAME_CMD,
		XV_KEY_DATA, INSTANCE, ip,
		XV_WIDTH, 476,
		XV_HEIGHT, 209,
		XV_LABEL, "Edit Receive Contexts",
		FRAME_SHOW_FOOTER, FALSE,
		FRAME_SHOW_RESIZE_CORNER, FALSE,
		FRAME_CMD_PUSHPIN_IN, TRUE,
		NULL);
	xv_set(xv_get(obj, FRAME_CMD_PANEL), WIN_SHOW, FALSE, NULL);
	return obj;
}

/*
 * Create object `controls1' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_controls1_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:contexts_panel",
		XV_X, 0,
		XV_Y, 0,
		XV_WIDTH, WIN_EXTEND_TO_EDGE,
		XV_HEIGHT, WIN_EXTEND_TO_EDGE,
		WIN_BORDER, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `receive_contexts_list' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_receive_contexts_list_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_LIST,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:contexts_list",
		XV_X, 8,
		XV_Y, 16,
		PANEL_LIST_WIDTH, 430,
		PANEL_LIST_DISPLAY_ROWS, 3,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		PANEL_CHOOSE_ONE, TRUE,
		PANEL_CHOOSE_NONE, TRUE,
		NULL);
	return obj;
}

/*
 * Create object `context_name' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_name_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_name",
		XV_X, 13,
		XV_Y, 110,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Name:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `context_kind_of_add' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_kind_of_add_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_CHOICE,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_kind_of_add",
		XV_X, 13,
		XV_Y, 150,
		PANEL_CHOICE_NROWS, 1,
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_CHOOSE_NONE, FALSE,
		PANEL_LABEL_STRING, "Kind of Add:",
		PANEL_CHOICE_STRINGS,
			"Add",
			"Badd",
			"Iadd",
			NULL,
		PANEL_VALUE, 0,
		NULL);
	return obj;
}

/*
 * Create object `context_value' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_value_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_TEXT,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_value",
		XV_X, 13,
		XV_Y, 130,
		PANEL_VALUE_DISPLAY_LENGTH, 50,
		PANEL_VALUE_STORED_LENGTH, 80,
		PANEL_LABEL_STRING, "Value:",
		PANEL_LAYOUT, PANEL_HORIZONTAL,
		PANEL_READ_ONLY, FALSE,
		NULL);
	return obj;
}

/*
 * Create object `context_add_button' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_add_button_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_add_button",
		XV_X, 149,
		XV_Y, 180,
		PANEL_LABEL_STRING, "Add",
		NULL);
	return obj;
}

/*
 * Create object `context_delete_button' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_delete_button_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_delete_button",
		XV_X, 199,
		XV_Y, 180,
		PANEL_LABEL_STRING, "Delete",
		NULL);
	return obj;
}

/*
 * Create object `context_change_button' in the specified instance.
 */
Xv_opaque
pattern_contexts_pattern_contexts_popup_context_change_button_create(pattern_contexts_pattern_contexts_popup_objects *ip, Xv_opaque owner)
{
	Xv_opaque	obj;
	
	obj = xv_create(owner, PANEL_BUTTON,
		XV_KEY_DATA, INSTANCE, ip,
		XV_HELP_DATA, "pattern_contexts:context_change_button",
		XV_X, 264,
		XV_Y, 180,
		PANEL_LABEL_STRING, "Change",
		NULL);
	return obj;
}


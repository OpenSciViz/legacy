/*
 *
 * cntl.c
 *
 * Copyright (c) 1990 by Sun Microsystems, Inc.
 */

/* 
 * An example controller for the remote-control editor defined in edit.c.
 * Puts up a simple panel with a text field to enter the filename to
 * operate on and sends out ToolTalk messages to start up an editor to
 * carry out the operations. Also allows browsing any sub-file specs
 * defined in the file.
 */
#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <xview/xview.h>
#include <xview/canvas.h>
#include <xview/icon_load.h>
#include <xview/panel.h>
#include <xview/scrollbar.h>
#include <xview/svrimage.h>
#include <xview/termsw.h>
#include <xview/text.h>
#include <xview/tty.h>
#include <xview/xv_xrect.h>
#include <desktop/tt_c.h>

Xv_opaque	cntl_ui_base_window;
Xv_opaque	cntl_ui_base_controls;
Xv_opaque	cntl_ui_file_button;
Xv_opaque	cntl_ui_file_field;
Xv_opaque	cntl_ui_file_menu;

Xv_opaque	cntl_ui_saveas_popup;
Xv_opaque	cntl_ui_saveas_controls;
Xv_opaque	cntl_ui_save_as_directory;
Xv_opaque	cntl_ui_new_filename;
Xv_opaque	cntl_ui_save_as_button;

Xv_opaque	cntl_ui_obj_popup;
Xv_opaque	cntl_ui_obj_controls;
Xv_opaque	cntl_ui_olist;
Xv_opaque	cntl_ui_hilite_button;
Xv_opaque       cntl_ui_obj_field;

char		*cntl_objid = (char *)0;

Tt_scope	msg_scope = TT_FILE_IN_SESSION;

void
main(argc, argv)
	int		argc;
	char		**argv;
{
	void		cntl_ui_initialize();

	/*
	 * Initialize XView.
	 */
	xv_init(XV_INIT_ARGC_PTR_ARGV, &argc, argv, 0);
	/*
	 * Initialize user interface components.
	 */
	cntl_ui_initialize();
	
	if (! cntl_init_tt()) {
		fprintf(stderr,"%s: Can't initialize ToolTalk\n", argv[0]);
		exit(1);
	}
	/*
	 * Turn control over to XView.
	 */
	xv_main_loop(cntl_ui_base_window);
	tt_close();
	exit(0);
}


/* 
 * Initialize our ToolTalk environment.
 */
int
cntl_init_tt()
{
	char			*procid = tt_open();
	int			ttfd;
	void			cntl_receive_tt_message();
	Tt_pattern		pat;
	Tt_callback_action	cntl_update_obj_panel_callback();


	if (tt_pointer_error(procid) != TT_OK) {
		return 0;
	}
	ttfd = tt_fd();

	/* register a dynamic pattern to catch whenever the remote */
	/* editor creates a sub-file object so that we can update our */
	/* object query panel. */

	pat = tt_pattern_create();
	tt_pattern_op_add(pat, "Sun_EditDemo_new_object");
	tt_pattern_scope_add(pat, msg_scope);
	tt_pattern_category_set(pat, TT_OBSERVE);
	tt_pattern_callback_add(pat, cntl_update_obj_panel_callback);
	if (tt_pattern_register(pat) != TT_OK) {
		return 0;
	}

	notify_set_input_func(cntl_ui_base_window,
			      (Notify_func)cntl_receive_tt_message,
			      ttfd);

	tt_session_join(tt_default_session());

	tt_free(procid);
	return 1;
}



/* 
 * Handle any incoming ToolTalk messages. Since all the messages we send
 * out have callback functions, no further action is required after doing
 * a tt_message_receive.
 */
void
cntl_receive_tt_message()
{
	Tt_message		msg;

	msg = tt_message_receive();
	if (msg != NULL &&  tt_pointer_error(msg) == TT_ERR_NOMP) {
		fprintf(stderr,"ToolTalk server down.\n");
		exit(0);
	}
}


/* 
 * Callback for our dynamic pattern informing us when an object has
 * been added to a file.  We use this information to update the file
 * objects panel if it is up.
 */
Tt_callback_action
cntl_update_obj_panel_callback(m, p)
     Tt_message m;
     Tt_pattern p;
{
	int 	mark;

	mark = tt_mark();

	if (cntl_is_same_file(tt_message_file(m),
			      (char *)xv_get(cntl_ui_file_field,
					     PANEL_VALUE))) {

		/* update the objects panel if it's showing */
		/* objects for the same file as the one in the */
		/* message. */
		cntl_update_obj_panel();
	}

	/*
	 * no further action required for this message. Destroy it
	 * and return TT_CALLBACK_PROCESSED so no other callbacks will
	 * be run for the message.
	 */
	tt_message_destroy(m);
	tt_release(mark);
	return TT_CALLBACK_PROCESSED;
}


/* 
 * Default callback for all the ToolTalk messages we send.
 */
Tt_callback_action
cntl_msg_callback(m, p)
     Tt_message m;
     Tt_pattern p;
{
	int		mark;
	char		msg[255];
	char		*errstr;


	mark = tt_mark();
	switch (tt_message_state(m)) {
	      case TT_STARTED:
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER,
		       "Starting editor...", NULL);
		break;
	      case TT_HANDLED:
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
		break;
	      case TT_FAILED:
		errstr = tt_message_status_string(m);
		if (tt_pointer_error(errstr) == TT_OK && errstr) {
			sprintf(msg,"%s failed: %s", tt_message_op(m), errstr);
		} else if (tt_message_status(m) == TT_ERR_NO_MATCH) {
			sprintf(msg,"%s failed: Couldn't contact editor",
				tt_message_op(m),
				tt_status_message(tt_message_status(m)));
		} else {
			sprintf(msg,"%s failed: %s",
				tt_message_op(m),
				tt_status_message(tt_message_status(m)));
		}
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, msg, NULL);
		break;
	      default:
		break;
	}

	/*
	 * no further action required for this message. Destroy it
	 * and return TT_CALLBACK_PROCESSED so no other callbacks will
	 * be run for the message.
	 */
	tt_message_destroy(m);
	tt_release(mark);
	return TT_CALLBACK_PROCESSED;
}


/* 
 * Handle the edit actions by sending out a message to Sun_EditDemo to
 * start an editing session.
 */
void
cntl_edit(file)
     char *file;
{
	Tt_message	msg;

	xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
	msg = tt_prequest_create(msg_scope, "Sun_EditDemo_edit");
	tt_message_file_set(msg, file);
	tt_message_callback_add(msg, cntl_msg_callback);
	tt_message_send(msg);
}


/* 
 * Handle the save actions by sending out a message to the Sun_EditDemo
 * editing this file to save the file.
 */
void
cntl_save(file)
     char *file;
{
	Tt_message	msg;

	xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
	msg = tt_prequest_create(msg_scope, "Sun_EditDemo_save");
	tt_message_file_set(msg, file);
	tt_message_callback_add(msg, cntl_msg_callback);
	tt_message_send(msg);
}



/* 
 * Shut down the Sun_EditDemo that is editing this file.
 */
void
cntl_close(file)
     char *file;
{
	Tt_message	msg;

	xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
	msg = tt_prequest_create(msg_scope, "Sun_EditDemo_close");
	tt_message_file_set(msg, file);
	tt_message_callback_add(msg, cntl_msg_callback);
	tt_message_send(msg);
}


/* 
 * Instruct the Sun_EditDemo that is editing this file to save the file
 * under a new name.
 */
void
cntl_save_as(file, dir, newfile)
     char *file;
     char *dir;
     char *newfile;
{
	Tt_message	msg;
	char		buf[255];
	int		dirlen;

	msg = tt_prequest_create(msg_scope, "Sun_EditDemo_save");

	/* construct new filename out of directory and new file */
	/* fields. */

	dirlen = strlen(dir);
	if (dirlen > 0 && dir[dirlen - 1] == '/') {
		sprintf(buf,"%s%s", dir, newfile);
	} else {
		sprintf(buf,"%s/%s", dir, newfile);
	}

	tt_message_arg_add(msg, TT_IN, "string", buf);
	tt_message_file_set(msg, file);
	tt_message_callback_add(msg, cntl_msg_callback);
	tt_message_send(msg);
}



/*
 * Menu handler for `file_menu (Edit)'.
 */
Menu_item
cntl_ui_edit(item, op)
	Menu_item	item;
	Menu_generate	op;
{
	if (op == MENU_NOTIFY) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
		cntl_edit((char *)xv_get(cntl_ui_file_field, PANEL_VALUE));
	}
	return item;
}

/*
 * Menu handler for `file_menu (Save)'.
 */
Menu_item
cntl_ui_save(item, op)
	Menu_item	item;
	Menu_generate	op;
{
	if (op == MENU_NOTIFY) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
		cntl_save((char *)xv_get(cntl_ui_file_field, PANEL_VALUE));
	}
	return item;
}

/*
 * Menu handler for `file_menu (Save as...)'.
 */
Menu_item
cntl_ui_save_as(item, op)
	Menu_item	item;
	Menu_generate	op;
{
	if (op == MENU_NOTIFY) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
		xv_set(cntl_ui_saveas_popup,  WIN_SHOW, TRUE, NULL);
	}
	return item;
}

/*
 * Menu handler for `file_menu (Close)'.
 */
Menu_item
cntl_ui_close(item, op)
	Menu_item	item;
	Menu_generate	op;
{
	if (op == MENU_NOTIFY) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
		cntl_close((char *)xv_get(cntl_ui_file_field, PANEL_VALUE));
	}

	return item;
}

/*
 * Notify callback function for `save_as_button'.
 */
void
cntl_ui_save_as_button_handler(item, event)
	Panel_item	item;
	Event		*event;
{
	xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);
	cntl_save_as((char *)xv_get(cntl_ui_file_field, PANEL_VALUE),
		     (char *)xv_get(cntl_ui_save_as_directory,  PANEL_VALUE),
		     (char *)xv_get(cntl_ui_new_filename, PANEL_VALUE));
}

/* 
 * Function to insert the objid given into the scrolling lists of objects
 * for a file. Used inside tt_file_objects_query as it iterates through
 * all the ToolTalk objects in a file.
 */
Tt_filter_action
cntl_gather_specs(objid, list_count, acc)
     char *objid;
     void *list_count;
     void *acc;
{
	int *i = (int *)list_count;

	xv_set(cntl_ui_olist, PANEL_LIST_INSERT, *i,
	       PANEL_LIST_STRING, *i, objid,
	       NULL);

	*i = (*i + 1);

	/* continue processing */
	return TT_FILTER_CONTINUE;
}
       
     
/* 
 * Called to update the scrolling list of objects for a file. Uses
 * tt_file_objects_query to find all the ToolTalk objects.
 */
int
cntl_update_obj_panel()
{
        static int         list_item = 0;
	char               *file;
	int                i;

	cntl_objid = (char *)0;

	for (i = list_item; i >= 0; i--) {
		xv_set(cntl_ui_olist, PANEL_LIST_DELETE, i, NULL);
	}

	list_item = 0;
	file = (char *)xv_get(cntl_ui_file_field, PANEL_VALUE);
	if  (tt_file_objects_query(file,
				   (Tt_filter_function)cntl_gather_specs,
				   &list_item, NULL) != TT_OK) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER,
		       "Couldn't query objects for file", NULL);
		return 0;
	}

	return 1;
}


/* 
 * Notify callback function for "File objects..." menu item.
 */
Menu_item
cntl_ui_file_objects(item, op)
     Menu_item		item;
     Menu_generate	op;
{
	if (op == MENU_NOTIFY) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER, "", NULL);

		if (cntl_update_obj_panel()) {
			(void)tt_file_join((char *)xv_get(cntl_ui_file_field,
							  PANEL_VALUE));
			xv_set(cntl_ui_obj_popup,  WIN_SHOW, TRUE, NULL);
		}
	}

	return item;
}


/*
 * Notify callback function for `cntl_ui_olist'.
 */
int
cntl_ui_olist_handler(item, string, client_data, op, event)
	Panel_item	item;
	char		*string;
	Xv_opaque	client_data;
	Panel_list_op	op;
	Event		*event;
{
	if (op == PANEL_LIST_OP_SELECT) {
		cntl_objid = strdup(string);
	}

	return XV_OK;
}


/*
 * Notify callback function for `cntl_ui_hilite_button'.
 */
void
cntl_ui_hilite_button_handler(item, event)
	Panel_item	item;
	Event		*event;
{
	Tt_message	msg;
	
	if (cntl_objid == (char *)0) {
		xv_set(cntl_ui_base_window, FRAME_LEFT_FOOTER,
		       "No object id selected", NULL);
		return;
	}
	msg = tt_orequest_create(cntl_objid, "hilite_obj");
	tt_message_arg_add(msg, TT_IN, "string", cntl_objid);
	tt_message_callback_add(msg, cntl_msg_callback);
	tt_message_send(msg);
}


/*
 * Initialize ui environment.
 */
void
cntl_ui_initialize()
{
	void		cntl_ui_saveas_popup_initialize();
	void		cntl_ui_obj_popup_initialize();

	cntl_ui_base_window = xv_create(NULL, FRAME,
					XV_WIDTH, 339,
					XV_HEIGHT, 61,
					XV_LABEL, "Sun_EditDemo_cntl",
					FRAME_CLOSED, FALSE,
					FRAME_SHOW_FOOTER, TRUE,
					FRAME_SHOW_RESIZE_CORNER, TRUE,
					NULL);

	cntl_ui_base_controls = xv_create(cntl_ui_base_window, PANEL,
					  XV_X, 0,
					  XV_Y, 0,
					  XV_WIDTH, WIN_EXTEND_TO_EDGE,
					  XV_HEIGHT, WIN_EXTEND_TO_EDGE,
					  WIN_BORDER, FALSE,
					  NULL);

	cntl_ui_file_menu = xv_create(XV_NULL, MENU_COMMAND_MENU,
				      MENU_ITEM,
				      MENU_STRING, "Edit",
				      MENU_GEN_PROC, cntl_ui_edit,
				      NULL,
				      MENU_ITEM,
				      MENU_STRING, "Save",
				      MENU_GEN_PROC, cntl_ui_save,
				      NULL,
				      MENU_ITEM,
				      MENU_STRING, "Save as...",
				      MENU_GEN_PROC, cntl_ui_save_as,
				      NULL,
				      MENU_ITEM,
				      MENU_STRING, "File Objects...",
				      MENU_GEN_PROC, cntl_ui_file_objects,
				      NULL,
				      MENU_ITEM,
				      MENU_STRING, "Close",
				      MENU_GEN_PROC, cntl_ui_close,
				      NULL,
				      MENU_GEN_PIN_WINDOW,
				      (Xv_opaque) cntl_ui_base_window, "File",
				      NULL);

	cntl_ui_file_button = xv_create(cntl_ui_base_controls, PANEL_BUTTON,
					XV_X, 16,
					XV_Y, 8,
					XV_WIDTH, 54,
					XV_HEIGHT, 19,
					PANEL_LABEL_STRING, "File",
					PANEL_ITEM_MENU, cntl_ui_file_menu,
					NULL);

	cntl_ui_file_field = xv_create(cntl_ui_base_controls, PANEL_TEXT,
				       XV_X, 16,
				       XV_Y, 40,
				       XV_WIDTH, 316,
				       XV_HEIGHT, 15,
				       PANEL_LABEL_STRING, "File:",
				       PANEL_VALUE_X, 52,
				       PANEL_VALUE_Y, 40,
				       PANEL_LAYOUT, PANEL_HORIZONTAL,
				       PANEL_VALUE_DISPLAY_LENGTH, 35,
				       PANEL_VALUE_STORED_LENGTH, 80,
				       PANEL_READ_ONLY, FALSE,
				       NULL);

	cntl_ui_saveas_popup_initialize();
	cntl_ui_obj_popup_initialize();
}


void
cntl_ui_saveas_popup_initialize()
{
	cntl_ui_saveas_popup = xv_create(cntl_ui_base_window, FRAME_CMD,
					 XV_WIDTH, 399,
					 XV_HEIGHT, 105,
					 XV_LABEL,
					 "Sun_EditMenu_cntl: Save As",
					 XV_SHOW, FALSE,
					 FRAME_SHOW_FOOTER, TRUE,
					 FRAME_SHOW_RESIZE_CORNER, TRUE,
					 FRAME_CMD_PUSHPIN_IN, FALSE,
					 NULL);

	xv_set(xv_get(cntl_ui_saveas_popup, FRAME_CMD_PANEL),
	       WIN_SHOW, FALSE, NULL);

	cntl_ui_saveas_controls =  xv_create(cntl_ui_saveas_popup, PANEL,
					     XV_X, 0,
					     XV_Y, 0,
					     XV_WIDTH, WIN_EXTEND_TO_EDGE,
					     XV_HEIGHT, WIN_EXTEND_TO_EDGE,
					     WIN_BORDER, FALSE,
					     NULL);

	cntl_ui_save_as_directory = xv_create(cntl_ui_saveas_controls,
					      PANEL_TEXT,
					      XV_X, 24,
					      XV_Y, 12,
					      XV_WIDTH, 354,
					      XV_HEIGHT, 15,
					      PANEL_LABEL_STRING, "Directory:",
					      PANEL_VALUE_X, 98,
					      PANEL_VALUE_Y, 12,
					      PANEL_LAYOUT, PANEL_HORIZONTAL,
					      PANEL_VALUE_DISPLAY_LENGTH, 35,
					      PANEL_VALUE_STORED_LENGTH, 80,
					      PANEL_READ_ONLY, FALSE,
					      NULL);

	cntl_ui_new_filename = xv_create(cntl_ui_saveas_controls, PANEL_TEXT,
					 XV_X, 20,
					 XV_Y, 44,
					 XV_WIDTH, 354,
					 XV_HEIGHT, 15,
					 PANEL_LABEL_STRING, "Filename:",
					 PANEL_VALUE_X, 94,
					 PANEL_VALUE_Y, 44,
					 PANEL_LAYOUT, PANEL_HORIZONTAL,
					 PANEL_VALUE_DISPLAY_LENGTH, 35,
					 PANEL_VALUE_STORED_LENGTH, 80,
					 PANEL_READ_ONLY, FALSE,
					 NULL);

	cntl_ui_save_as_button = xv_create(cntl_ui_saveas_controls,
					   PANEL_BUTTON,
					   XV_X, 168,
					   XV_Y, 80,
					   XV_WIDTH, 46,
					   XV_HEIGHT, 19,
					   PANEL_LABEL_STRING, "Save",
					   PANEL_NOTIFY_PROC,
					   cntl_ui_save_as_button_handler,
					   NULL);
}



void
cntl_ui_obj_popup_initialize()
{
	cntl_ui_obj_popup = xv_create(cntl_ui_base_window, FRAME_CMD,
				      XV_WIDTH, 406,
				      XV_HEIGHT, 204,
				      XV_LABEL,
				      "Sun_EditDemo_cntl: File Objects",
				      XV_SHOW, FALSE,
				      FRAME_SHOW_FOOTER, TRUE,
				      FRAME_SHOW_RESIZE_CORNER, TRUE,
				      FRAME_CMD_PUSHPIN_IN, FALSE,
				      NULL);

	xv_set(xv_get(cntl_ui_obj_popup, FRAME_CMD_PANEL),
	       WIN_SHOW, FALSE, NULL);

	cntl_ui_obj_controls =  xv_create(cntl_ui_obj_popup, PANEL,
					  XV_X, 0,
					  XV_Y, 0,
					  XV_WIDTH, WIN_EXTEND_TO_EDGE,
					  XV_HEIGHT, WIN_EXTEND_TO_EDGE,
					  WIN_BORDER, FALSE,
					  NULL);

	cntl_ui_olist = xv_create(cntl_ui_obj_controls, PANEL_LIST,
				  XV_X, 40,
				  XV_Y, 36,
				  PANEL_LIST_WIDTH, 200,
				  XV_HEIGHT, 74,
				  PANEL_LABEL_STRING, "File Objects:",
				  PANEL_LAYOUT, PANEL_HORIZONTAL,
				  PANEL_LIST_DISPLAY_ROWS, 3,
				  PANEL_READ_ONLY, FALSE,
				  PANEL_CHOOSE_ONE, TRUE,
				  PANEL_CHOOSE_NONE, TRUE,
				  PANEL_NOTIFY_PROC, cntl_ui_olist_handler,
				  NULL);

	cntl_ui_hilite_button = xv_create(cntl_ui_obj_controls, PANEL_BUTTON,
					  XV_X, 156,
					  XV_Y, 160,
					  XV_WIDTH, 63,
					  XV_HEIGHT, 19,
					  PANEL_LABEL_STRING, "Display",
					  PANEL_NOTIFY_PROC,
					  cntl_ui_hilite_button_handler,
					  NULL);
}


int
cntl_is_same_file(f1, f2)
     char	*f1;
     char	*f2;
{
	struct stat	f1_stat;
	struct stat	f2_stat;

	if (!strcmp(f1,f2)) {
		return(1);
	}

	return((0 == stat(f1, &f1_stat)) &&
	       (0 == stat(f2, &f2_stat)) &&
	       (f1_stat.st_ino == f2_stat.st_ino) &&
	       (f1_stat.st_dev == f2_stat.st_dev));
}

	

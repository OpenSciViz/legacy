/*
 *
 * Sun_EditDemo_opnums.h
 *
 * Copyright (c) 1990 by Sun Microsystems, Inc.
 */

/* 
 * Symbolic definitions for all the opnums used by edit.c. This allows
 * both the edit.types file and edit.c file to share the same
 * definitions. 
 */
#define	SUN_EDITDEMO_EDIT	0
#define	SUN_EDITDEMO_SAVE	1
#define	SUN_EDITDEMO_SAVE_AS	2
#define	SUN_EDITDEMO_CLOSE	3
#define SUN_EDITDEMO_HILITE_OBJ	4


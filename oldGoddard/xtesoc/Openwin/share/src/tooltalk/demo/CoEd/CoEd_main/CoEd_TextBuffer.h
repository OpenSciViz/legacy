/* -*-C++-*-
 *
 * CoEd_TextBuffer.h
 *
 * Copyright (c) 1991 by Sun Microsystems.  All Rights Reserved.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the names of Sun
 * Microsystems and its subsidiaries not be used in advertising or
 * publicity pertaining to distribution of the software without
 * specific, written prior permission.  Sun Microsystems and its
 * subsidiaries make no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * Sun Microsystems and its subsidiaries disclaim all warranties with
 * regard to this software, including all implied warranties of
 * merchantability and fitness.  In no event shall Sun Microsystems or
 * its subsidiaries be liable for any special, indirect or
 * consequential damages or any damages whatsoever resulting from loss
 * of use, data or profits, whether in an action of contract,
 * negligence or other tortious action, arising out of or in
 * connection with the use or performance of this software.
 */

#ifndef CoEd_TextBuffer_h
#define CoEd_TextBuffer_h

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <Xol/OpenLook.h>
#include <Xol/Menu.h>
#include <Xol/OblongButt.h>
#include <X11/Shell.h>
#include <Xol/FooterPane.h>
#include <Xol/Form.h>
#include <Xol/StaticText.h>
#include <Xol/BulletinBo.h>
#include <Xol/MenuButton.h>
#include <Xol/ScrolledWi.h>

#ifndef SVR4
#define LocationOfPosition		_HIDE_LocationOfPosition
#define ReplaceBlockInTextBuffer	_HIDE_ReplaceBlockInTextBuffer
#endif SVR4

#include <Xol/TextEdit.h>

#ifndef SVR4
#undef LocationOfPosition
#undef ReplaceBlockInTextBuffer

#include "OlitC++.h"
#endif SVR4

#include <CoEd.h>

class CoEdTextBuffer {
    public:
	CoEdTextBuffer( TextBuffer *textBuffer );

	CoEdStatus	insertText( long start, long end,
					    const char *text );
	CoEdStatus	save();
	CoEdStatus	revert();
	CoEdStatus	rename( const char *newPath );
	int		insertingText() const { return _insertingText; }
	
    private:
	TextBuffer	       *_textBuffer;
	int			_insertingText;
};

#endif CoEd_TextBuffer_h

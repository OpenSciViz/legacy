/*
 * @(#)ttdesktop.cc	1.4 93/11/10
 *
 * Copyright (c) 1993 by Sun Microsystems, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <poll.h>
#ifndef SVR4
#include <sys/time.h>
#endif // SVR4
#include <sys/resource.h>
#include <desktop/tt_c.h>
#include "ttdesktop.h"

extern void  readStringIntoTextEdit(const char *);
extern void  readFileIntoTextEdit(const char *);
extern char *getTextEditContents();

char   *myProcID	= 0;
int	myTtFD		= -1;

void
printErr(
	const char     *whence,
	const char     *expr,
	Tt_status	err
)
{
	fprintf( stderr, "ttedit(%d): %s: %s: %s\n", getpid(), whence, expr,
		 tt_status_message( err ));
}

void
printErr(
	const char     *whence,
	const char     *msg
)
{
	fprintf( stderr, "ttedit(%d): %s: %s\n", getpid(), whence, msg );
}

void
pError(
	const char     *whence,
	const char     *msg
)
{
	fprintf( stderr, "ttedit(%d): %s: %s: ", getpid(), whence, msg );
	perror( 0 );
}

Tt_callback_action
dealWithTTReply(
	Tt_message reply,
	Tt_pattern // pattern
)
{
	static const char *here = "dealWithTTReply";

	char *op = tt_message_op(reply);
        Tt_status status = tt_ptr_error( op );
        if (status != TT_OK) {
                printErr( here, "tt_message_op()", status );
                return TT_CALLBACK_PROCESSED;
        }

	if (!strcmp(op, "Get_Locale")) {
		char *locale = tt_message_arg_val(reply, 1);
        	status = tt_ptr_error( locale );
        	if (status != TT_OK) { 
                	printErr( here, "tt_message_arg_val()", status );
                	return TT_CALLBACK_PROCESSED;
        	} 
		printf("Get_Local - Locale: %s\n",
		       (locale ? locale : "(NULL)"));
	}
	else if (!strcmp(op, "Get_XInfo")) {
		char *display = tt_message_arg_val(reply, 0);
        	status = tt_ptr_error( display );
        	if (status != TT_OK) { 
                	printErr( here, "tt_message_arg_val()", status );
                	return TT_CALLBACK_PROCESSED;
        	} 
		printf("Get_XInfo - Display: %s\n",
		       (display ? display : "(NULL)"));

		char *visual = tt_message_arg_val(reply, 1);
        	status = tt_ptr_error( visual );
        	if (status != TT_OK) { 
                	printErr( here, "tt_message_arg_val()", status );
                	return TT_CALLBACK_PROCESSED;
        	} 
		printf("Get_XInfo - Visual: %s\n",
		       (visual ? visual : "(NULL)"));

		int depth;
		status = tt_message_arg_ival(reply, 2, &depth);
        	if (status != TT_OK) { 
                	printErr( here, "tt_message_arg_ival()", status );
                	return TT_CALLBACK_PROCESSED;
        	} 
		printf("Get_XInfo - Depth: %d\n", depth);
	}
	else if (!strcmp(op, "Get_Geometry")) {
		int width;
		status = tt_message_arg_ival(reply, 0, &width);
                if (status != TT_OK) {   
                        printErr( here, "tt_message_arg_ival()", status );
                        return TT_CALLBACK_PROCESSED;   
                } 
		printf("Get_Geometry - Width: %d\n", width);

		int height;
		status = tt_message_arg_ival(reply, 1, &height);
                if (status != TT_OK) {   
                        printErr( here, "tt_message_arg_ival()", status );
                        return TT_CALLBACK_PROCESSED;   
                } 
		printf("Get_Geometry - Height: %d\n", height);

		int x_offset;
		status = tt_message_arg_ival(reply, 2, &x_offset);
                if (status != TT_OK) {   
                        printErr( here, "tt_message_arg_ival()", status );
                        return TT_CALLBACK_PROCESSED;   
                } 
		printf("Get_Geometry - X Offset: %d\n", x_offset);

		int y_offset;
		status = tt_message_arg_ival(reply, 3, &y_offset);
                if (status != TT_OK) {   
                        printErr( here, "tt_message_arg_ival()", status );
                        return TT_CALLBACK_PROCESSED;   
                } 
		printf("Get_Geometry - Y Offset: %d\n", y_offset);
	}

	return TT_CALLBACK_PROCESSED;
}

Tt_status
dealWithRequest(
	Tt_message msg
)
{
	static const char *here = "dealWithTTRequest";

	char *op = tt_message_op(msg);
        Tt_status status = tt_ptr_error( op );
        if (status != TT_OK) {
	        (void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                printErr( here, "tt_message_op()", status );
		return status;
        }

	char *parent_proc_id = tt_message_sender( msg );
	status = tt_ptr_error( parent_proc_id );
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
		printErr( here, "tt_message_sender()", status );
		return status;
	}

	// Send a status message indicating that the message was received
	Tt_message status_msg = tt_pnotice_create(TT_SESSION, "Status");
	status = tt_ptr_error(status_msg);
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_pnotice_create()", status );
		return status;
	}
        status = tt_message_address_set(status_msg, TT_HANDLER);
        if (status != TT_OK) {
                printErr( here, "tt_message_address_set()", status );
		(void)tt_message_fail(msg);
                (void)tt_message_destroy( msg );
                (void)tt_message_destroy( status_msg );
                return status;
        }
	status = tt_message_handler_set(status_msg, parent_proc_id);
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_handler_set()", status );
		return status;
	}
	status = tt_message_arg_add(status_msg, 
				    TT_IN, "string", "Message Received");
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_arg_add()", status );
		return status;
	}
	status = tt_message_arg_add(status_msg, 
				    TT_IN, "string", "SunSoft");
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_arg_add()", status );
		return status;
	}
	status = tt_message_arg_add(status_msg, 
				    TT_IN, "string", "CoEd");
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_arg_add()", status );
		return status;
	}
	status = tt_message_arg_add(status_msg, 
				    TT_IN, "string", "0.9Demo");
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_arg_add()", status );
		return status;
	}
	status = tt_message_send(status_msg);
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
                (void)tt_message_destroy( status_msg );
		printErr( here, "tt_message_send()", status );
		return status;
	}
        (void)tt_message_destroy( status_msg );

	if (!strcmp(op, "Edit")) {
	        char *contents = (char *)NULL;

	        Tt_mode mode = tt_message_arg_mode(msg, 0);
		status = tt_int_error(mode);
		if (status != TT_OK) {
			(void)tt_message_fail(msg);
			(void)tt_message_destroy(msg);
			printErr( here, "tt_message_arg_mode()", status );
			return status;
		}

		if (mode == TT_INOUT) {
	                contents = tt_message_arg_val(msg, 0);
			status = tt_ptr_error(contents);
			if (status != TT_OK) {
			        (void)tt_message_fail(msg);
				(void)tt_message_destroy(msg);
				printErr( here,
					  "tt_message_arg_val()", 
					  status );
				return status;
			}
		}

		if (contents) {
		        readStringIntoTextEdit(contents);
		}
		else {
		        char *file = tt_message_file(msg);
			status = tt_ptr_error(contents);
			if (status != TT_OK) {
			        (void)tt_message_fail(msg);
				(void)tt_message_destroy(msg);
			        printErr( here,
					  "tt_message_arg_val()",
					  status );
			        return status;
			}

			if (file) {
				readFileIntoTextEdit(file);
			}
		}

		contents = getTextEditContents();

		status = tt_message_arg_val_set(msg, 0, contents);
		if (status != TT_OK) {
			(void)tt_message_fail(msg);
			(void)tt_message_destroy(msg);
			printErr( here, "tt_message_arg_set()", status );
			return status;
		}
	}
	else if (!strcmp(op, "Display")) {
	        char *contents = tt_message_arg_val(msg, 0);
		status = tt_ptr_error(contents);
		if (status != TT_OK) {
		        (void)tt_message_fail(msg);
			(void)tt_message_destroy(msg);
		        printErr( here, "tt_message_arg_val()", status );
			return status;
		}

		if (contents) {
		        readStringIntoTextEdit(contents);
		}
		else {
		        char *file = tt_message_file(msg);
			status = tt_ptr_error(contents);
			if (status != TT_OK) {
			        (void)tt_message_fail(msg);
				(void)tt_message_destroy(msg);
			        printErr( here,
					  "tt_message_arg_val()",
					  status );
			        return status;
			}

			if (!file) {
			        (void)tt_message_fail(msg);
				(void)tt_message_destroy(msg);
			}

			readFileIntoTextEdit(file);
		}
	}

	// ***********************************************************
	// ************************* NOTE ****************************
	// ***********************************************************
	// If this is an "Edit" message, normally we should not reply
	// until the user is "done" editing.  Then the contents of the
	// reply should contain the users edit result.  Since this is
	// just a demo, an immediate reply was done.  In the case of
	// the "Display" message, an immediate reply is appropriate.
	// ***********************************************************
	status = tt_message_reply(msg);
        if (status != TT_OK) {
                printErr( here, "tt_message_reply()", status );
	}

	return TT_OK;
}

Tt_status
dealWithNotice(
	Tt_message // msg
)
{
	return TT_OK;
}

Tt_status
dealWithTTActivity()
{
	Tt_message msg = tt_message_receive();
	Tt_status status = tt_ptr_error( msg );
	if (status != TT_OK) {
		return status;
	}
	if (msg == 0) {
		return TT_OK;
	}
	Tt_class theClass = tt_message_class( msg );
	if (theClass == TT_REQUEST) {
		return dealWithRequest( msg );
	} else if (theClass == TT_NOTICE) {
		return dealWithNotice( msg );
	} else {
		return TT_ERR_CLASS;
	}
}

Tt_status
runTtEventLoop()
{
	static const char *here = "runTtEventLoop()";
	struct rlimit nofile;
	getrlimit(RLIMIT_NOFILE, &nofile);
	struct pollfd fds[ 1 ];
	/*
	 * ToolTalk message input
	 */
	fds[ 0 ].fd = myTtFD;
	fds[ 0 ].events = POLLIN;

	Tt_status status = TT_OK;
	int quit = 0;
	while (! quit ) {
		int activeFDs = poll( fds, 1, 5000 );
		if (activeFDs == 0) {
			quit = 1;
			continue;
		} else if (activeFDs < 0) {
			if (! quit) {
				pError( here, "poll()" );
				quit = 1;
			}
			continue;
		}
		if (fds[ 0 ].revents & POLLIN) {
			status = dealWithTTActivity();
			if (status == TT_ERR_NOMP) {
			}
		}
	}
	return status;
}

Tt_message
createDesktopQuery(
	const char	       *parentProcID,
	const char	       *op,
	Tt_message_callback	callback,
	Tt_message	       *pMsg
)
{
	static const char *here = "createDesktopQuery()";

	Tt_message msg = tt_prequest_create(TT_SESSION, op);
	Tt_status status = tt_ptr_error( msg );
	if (status != TT_OK) {
		printErr( here, "tt_message_create()", status );
		return msg;
	}
	status = tt_message_address_set( msg, TT_HANDLER );
	if (status != TT_OK) {
		printErr( here, "tt_message_address_set()", status );
		tt_message_destroy( msg );
		return (Tt_message)tt_error_pointer( status );
	}
	status = tt_message_handler_set( msg, parentProcID );
	if (status != TT_OK) {
		printErr( here, "tt_message_handler_set()", status );
		tt_message_destroy( msg );
		return (Tt_message)tt_error_pointer( status );
	}
	status = tt_message_callback_add( msg, callback );
	if (status != TT_OK) {
		printErr( here, "tt_message_callback_add()", status );
		tt_message_destroy( msg );
		return (Tt_message)tt_error_pointer( status );
	}
	status = tt_message_user_set( msg, OfficialMsgRefKey, pMsg );
	if (status != TT_OK) {
		printErr( here, "tt_message_user_set()", status );
		tt_message_destroy( msg );
		return (Tt_message)tt_error_pointer( status );
	}
	return msg;
}

int
numOutStandingMsgs(
	Tt_message msgs[],
	int numMsgs
)
{
	int numOutStandingMsgs = 0;
	for (int i = 0; i++; i < numMsgs) {
		if (msgs[i] != 0) {
			numOutStandingMsgs++;
		}
	}
	return numOutStandingMsgs;
}

Tt_status
queryDesktop(
	const char *parentProcID,
	int // ttFD
)
{
	static const char *here = "queryDesktop()";
	Tt_message msgs[ 3 ];

	msgs[0] = createDesktopQuery( parentProcID, "Get_Locale",
				      dealWithTTReply, &msgs[0] );
	// XXX add counterfoil to all these queries
	Tt_status status = tt_ptr_error( msgs[0] );
	if (status != TT_OK) {
		return status;
	}
	status = tt_message_arg_add( msgs[0], TT_IN, "string", "LC_ALL" );
	if (status != TT_OK) {
		printErr( here, "tt_message_arg_add()", status );
		tt_message_destroy( msgs[0] );
		return status;
	}
	status = tt_message_arg_add( msgs[0], TT_OUT, "string", (char *)0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_arg_add()", status );
		tt_message_destroy( msgs[0] );
		return status;
	}
	status = tt_message_send( msgs[0] );
	if (status != TT_OK) {
		printErr( here, "tt_message_send()", status );
		tt_message_destroy( msgs[0] );
		return status;
	}

	msgs[1] = createDesktopQuery( parentProcID, "Get_XInfo",
				      dealWithTTReply, &msgs[1] );
	status = tt_ptr_error( msgs[1] );
	if (status != TT_OK) {
		return status;
	}
	status = tt_message_arg_add( msgs[1], TT_OUT, "string", (char *)0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_arg_add()", status );
		tt_message_destroy( msgs[1] );
		return status;
	}
	status = tt_message_arg_add( msgs[1], TT_OUT, "string", (char *)0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_arg_add()", status );
		tt_message_destroy( msgs[1] );
		return status;
	}
	status = tt_message_iarg_add( msgs[1], TT_OUT, "string", 0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_iarg_add()", status );
		tt_message_destroy( msgs[1] );
		return status;
	}
	status = tt_message_send( msgs[1] );
	if (status != TT_OK) {
		printErr( here, "tt_message_send()", status );
		tt_message_destroy( msgs[1] );
		return status;
	}

	msgs[2] = createDesktopQuery( parentProcID, "Get_Geometry",
				      dealWithTTReply, &msgs[2] );
	status = tt_ptr_error( msgs[2] );
	if (status != TT_OK) {
		return status;
	}
	status = tt_message_iarg_add( msgs[2], TT_OUT, "width", 0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_iarg_add()", status );
		tt_message_destroy( msgs[2] );
		return status;
	}
	status = tt_message_iarg_add( msgs[2], TT_OUT, "height", 0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_iarg_add()", status );
		tt_message_destroy( msgs[2] );
		return status;
	}
	status = tt_message_iarg_add( msgs[2], TT_OUT, "xOffset", 0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_iarg_add()", status );
		tt_message_destroy( msgs[2] );
		return status;
	}
	status = tt_message_iarg_add( msgs[2], TT_OUT, "yOffset", 0 );
	if (status != TT_OK) {
		printErr( here, "tt_message_iarg_add()", status );
		tt_message_destroy( msgs[2] );
		return status;
	}
	status = tt_message_send( msgs[2] );
	if (status != TT_OK) {
		printErr( here, "tt_message_send()", status );
		tt_message_destroy( msgs[2] );
		return status;
	}

	return runTtEventLoop();
}

Tt_status
initDesktop(
	char *& /*procID*/,
	int &ttFD
)
{
	static const char *here = "initDesktop";

	Tt_status status = tt_ptype_declare("Sun_CoEd_demo");
	if (status != TT_OK) {
	        printErr( here, "tt_ptype_declare()", status );
	}

	status = tt_session_join(tt_default_session());
        if (status != TT_OK) {
                printErr( here, "tt_session_join()", status );
        }
 
	Tt_message msg = tt_message_receive();
	if (msg == 0) {
		//
		// No new messages; we must not have been
		// started by ToolTalk.
		//
		return TT_OK;
	}
	status = tt_ptr_error( msg );
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
		printErr( here, "tt_message_sender()", status );
		return status;
	}

	if (tt_message_status (msg) != TT_WRN_START_MESSAGE) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
		if (status == TT_OK) {
			printErr( here, "ToolTalk bug: got an unbidden "
				  "message which was not a START_MSG!" );
		} else {
			printErr( here, "tt_message_status()", status );
		}
		return status;
	}

	char *parentProcID = tt_message_sender( msg );
	status = tt_ptr_error( parentProcID );
	if (status != TT_OK) {
		(void)tt_message_fail(msg);
		(void)tt_message_destroy(msg);
		printErr( here, "tt_message_sender()", status );
		return status;
	}
	status = queryDesktop( parentProcID, ttFD );

	if (status == TT_OK) {
// TT_VERSION is defined in ToolTalk 1.1 and greater.  tt_message_accept
// does not exit in earlier version of ToolTalk.
#ifdef TT_VERSION
		status = tt_message_accept( msg );
#endif
		if (status == TT_OK) {
        		Tt_class theClass = tt_message_class( msg );
        		if (theClass == TT_REQUEST) {
                		return dealWithRequest( msg );
        		} else if (theClass == TT_NOTICE) {
                		return dealWithNotice( msg );
        		} else {
                		return TT_ERR_CLASS;
        		}
		}
	}
	else {
		(void)tt_message_fail( msg );
		(void)tt_message_destroy(msg);
	}

	return status;
}

Tt_status
initTT(
	char *&procID,
	int &ttFD
)
{
	static const char *here = "initTT";

	procID = tt_open();
	Tt_status status = tt_ptr_error( procID );
	if (status != TT_OK) {
		printErr( here, "tt_open()", status );
		return status;
	}
	ttFD = tt_fd();
	status = tt_int_error( ttFD );
	if (status != TT_OK) {
		printErr( here, "tt_fd()", status );
		return status;
	}
	status = initDesktop( procID, ttFD );
	if (status == TT_ERR_NOMP) {
		procID = 0;
		ttFD = -1;
	}
	return status;
}

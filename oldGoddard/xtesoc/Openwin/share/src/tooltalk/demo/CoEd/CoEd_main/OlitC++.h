/*
 * Copyright (c) 1991 by Sun Microsystems.  All Rights Reserved.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the names of Sun
 * Microsystems and its subsidiaries not be used in advertising or
 * publicity pertaining to distribution of the software without
 * specific, written prior permission.  Sun Microsystems and its
 * subsidiaries make no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * Sun Microsystems and its subsidiaries disclaim all warranties with
 * regard to this software, including all implied warranties of
 * merchantability and fitness.  In no event shall Sun Microsystems or
 * its subsidiaries be liable for any special, indirect or
 * consequential damages or any damages whatsoever resulting from loss
 * of use, data or profits, whether in an action of contract,
 * negligence or other tortious action, arising out of or in
 * connection with the use or performance of this software.
 */

extern "C" { Widget OlToolkitInitialize( XtPointer ); }
extern "C" { TextPosition PositionOfLocation(...); }
extern "C" { TextLocation LocationOfPosition(...); }
extern "C" { TextPosition LastTextBufferPosition( TextBuffer *text ); }
extern "C" { EditResult ReplaceBlockInTextBuffer(...); }
extern "C" { TextBuffer *OlTextEditTextBuffer(...); }
extern "C" { void RegisterTextBufferUpdate(...); }
extern "C" { SaveResult SaveTextBuffer(TextBuffer *, char *); }
extern "C" { TextLocation LastTextBufferLocation(TextBuffer *); }
extern "C" { char *GetTextBufferBlock(TextBuffer *,
				      TextLocation,
				      TextLocation); }
extern "C" { void FreeTextBuffer(TextBuffer *, TextUpdateFunction, caddr_t); }
extern "C" { TextBuffer *ReadFileIntoTextBuffer(char *,
					        TextUpdateFunction,
					        caddr_t); }
extern "C" { void XtFree(caddr_t); }

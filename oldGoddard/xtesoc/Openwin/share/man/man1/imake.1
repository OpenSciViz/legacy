.\" Copyright (c) 1994, Sun Microsystems, Inc.
.TH imake 1 "24 March 1992"
.IX "imake" "" "\f3imake\f1(1) \(em \f3make\f1 utility interface" ""
.SH NAME
imake \- C preprocessor interface to the make utility
.SH SYNOPSIS
.B imake
[
.BI -D define
] [
.BI -I dir
] [
.BI -T template
]
[
.BI -f \ filename
] [
.BI -s \ filename
] [
.B -e
] [
.B -v
]
.SH DESCRIPTION
.B imake
is used to 
generate \f3Makefiles\f1 from a template, a set of 
.BR cpp (1)
macro functions,
and a per-directory input file called an \f3Imakefile\f1.  This allows machine
dependencies (such has compiler options, alternate command names, and special
.BR make (1S)
rules) to be kept separate from the descriptions of the
various items to be built.
.SH OPTIONS
The following command line options may be passed to
.BR imake :
.TP 8
.B \-D\fIdefine\f1
This option is passed directly to \f3cpp\f1.  It is typically used to set
directory-specific variables.  For example, the X Window System uses this
flag to set \fITOPDIR\f1 to the name of the directory containing the top
of the core distribution and \fICURDIR\f1 to the name of the current 
directory, relative to the top.
.TP 8
.B \-I\fIdirectory\f1
This option is passed directly to
.BR cpp (1).
It is typically used to indicate the directory in which the
.B imake
template and configuration
files may be found.
.TP 8
.B \-T\fItemplate\f1
This option specifies the name of the master template file (which is usually
located in the directory specified with \fI\-I\f1) used by \f3cpp\f1.
The default is \f3Imake.tmpl\f1.
.TP 8
.B \-f \fIfilename\f1
This option specifies the name of the per-directory input file.  The default
is \f3Imakefile\f1.
.TP 8
.B \-s \fIfilename\f1
This option specifies the name of the \f3make\f1 description file to be 
generated but \f3make\f1 should not be invoked.
If the \fIfilename\f1 is a dash (-), the 
output is written to \f3stdout\f1.  The default is to generate, but
not execute, a \f3Makefile\f1.
.TP 8
.B \-e
This option indicates the
.B imake
should execute the generated
\f3Makefile\f1.  The default is to leave this to the user.
.TP 8
.B \-v
This option indicates that
.B imake
should print the \f3cpp\f1 command line 
that it is using to generate the \f3Makefile\f1.
.SH "HOW IT WORKS"
.B imake
invokes \f3cpp\f1 with any \f3\-I\f1 or \f3-D\f1 flags passed
on the command line and passes it the following 3 lines:
.sp
.nf
		#define IMAKE_TEMPLATE "Imake.tmpl"
		#define INCLUDE_IMAKEFILE "Imakefile"
		#include IMAKE_TEMPLATE
.fi
.sp
where \fIImake.tmpl\f1 and \fIImakefile\f1 may be overridden by the 
\f3\-T\f1 and \f3\-f\f1 command options, respectively.  If the 
\f3Imakefile\f1 contains any lines beginning with a '#' character
that is not followed by a \f3cpp\f1 directive (\f3#include\f1,
\f3#define\f1, \f3#undef\f1, \f3#ifdef\f1, \f3#else\f1, \f3#endif\f1,
or \f3#if\f1),
.B imake
will make a temporary \f3makefile\f1 in
which the '#' lines are prepended with the string ``/**/'' (so that
\f3cpp\f1 will copy the line into the \f3Makefile\f1 as a comment).
.PP
The \f3Imakefile\f1 reads in file containing machine-dependent parameters 
(specified as \f3cpp\f1 symbols), a site-specific parameters file, a file
containing \f3cpp\f1 macro functions for generating \f3make\f1 rules, and
finally the \f3Imakefile\f1 (specified by INCLUDE_IMAKEFILE) in the current 
directory.  The \f3Imakefile\f1 uses the macro functions to indicate what
targets should be built;
.B imake
takes care of generating the appropriate
rules.
.PP
The rules file (usually named \f3Imake.rules\f1 in the configuration
directory) contains a variety of \f3cpp\f1 macro functions that are
configured according to the current platform.
.B imake
replaces 
any occurrences of the string ``@@'' with a newline to allow macros that
generate more than one line of \f3make\f1 rules.  
For example, the macro
.ta .8i 1.6i 5i
.nf
.sp
#define	program_target(program, objlist)	@@\e
program:	objlist		@@\e
	$(CC) -o $@ objlist $(LDFLAGS)
.sp
.fi
when called with
.I "program_target(foo, foo1.o foo2.o)"
will expand to
.nf
.sp
foo:	foo1.o foo2.o
	$(CC) -o $@ foo1.o foo2.o $(LDFLAGS)
.sp
.fi
.PP
On systems whose \f3cpp\f1 reduces multiple tabs and spaces to a single
space,
.B imake
attempts to put back any necessary tabs (\f3make\f1 is
very picky about the difference between tabs and spaces).  For this reason,
colons (:) in command lines must be preceded by a backslash (\\).
.SH "USE WITH THE X WINDOW SYSTEM"
The X Window System uses
.B imake
extensively, for both full builds within
the source tree and external software.  As mentioned above, two special
variables, \fITOPDIR\f1 and \fICURDIR\f1 set to make referencing files
using relative path names easier.  For example, the following command is
generated automatically to build the \f3Makefile\f1 in the directory
\f3lib/X/\f1 (relative to the top of the sources):
.sp
.nf
	%  ../.././config/imake  -I../.././config \\
		-DTOPDIR=../../. -DCURDIR=./lib/X
.fi
.sp
When building X programs outside the source tree, a special symbol
\f3UseInstalled\f1 is defined and \fITOPDIR\f1 and
\fICURDIR\f1 are omitted.  If the configuration files have been
properly installed, the script
.BR xmkmf (1)
may be used to specify
the proper options:
.sp
.nf
	%  xmkmf
.fi
.sp
The command \f3make Makefiles\f1 can then be used to generate \f3Makefiles\f1
in any subdirectories.
.SH FILES
.ta 3i
\f3usr/tmp/tmp-imake\f1.\fInnnnnn\f1 	temporary input file for \f3cpp\f1
.br
\f3usr/tmp/tmp-make\f1.\fInnnnnn\f1	temporary input file for \f3make\f1
.br
\f3usr/ccs/lib/cpp\f1	default C preprocessor
.SH "SEE ALSO"
.BR make (1S),
.BR execvp (2),
.BR xmkmf (1)
.br
S. I. Feldman
.I
Make \- A Program for Maintaining Computer Programs
.SH "ENVIRONMENT VARIABLES"
The following environment variables may be set, however their use is not
recommended as they introduce dependencies that are not readily apparent
when
.B imake
is run:
.TP 5
.B IMAKEINCLUDE
If defined, this should be a valid include argument for the
C preprocessor.  E.g.
\fI-I\f1
.BR /usr/include/local .
Actually, any valid
.B cpp
argument will work here.
.TP 5
.B IMAKECPP
If defined, this should be a valid path to a preprocessor program.
E.g.
.BR /usr/local/cpp .
By default,
.B imake
will use
.BR /lib/cpp .
.TP 5
.B IMAKEMAKE
If defined, this should be a valid path to a make program.
E.g.
.BR /usr/local/make .
By default,
.B imake
will use whatever
.B make
program is found using
.BR  execvp (2).
.SH "BUGS"
Comments should be preceded by ``/**/#'' to protect them from \f3cpp\f1.
.SH "AUTHOR"
Todd Brunhoff, Tektronix and MIT Project Athena; Jim Fulton, MIT X Consortium

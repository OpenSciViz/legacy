.\" @(#)Xserver.1 1.19 94/04/22
.\" Copyright (c) 1994 Sun Microsystems, Inc.
.TH Xserver 1 "25 January 1994"
.IX "Xserver" "general" "Xserver" "general information \(em \f3Xserver\f1(1)"
.SH NAME
Xserver \- X Window System server
.SH SYNOPSIS
.B X
[:displaynumber] [\-option ...] [ttyname]
.SH DESCRIPTION
.B X
is the generic name for the X Window System server.  It is frequently a link
or a copy of the appropriate server binary for driving the most frequently
used server on a given machine.
.SH STARTING THE SERVER
The server is usually started from the X Display Manager program
.BR xdm (1).
This utility is run from the system boot files and takes care of keeping
the server running, prompting for usernames and passwords, and starting up
the user sessions.  It is easily configured for sites that wish to provide
nice, consistent interfaces for novice users (loading convenient sets of
resources, starting up a window manager, clock, and nice selection of 
terminal emulator windows).
.PP
Installations that run more than one window system will still need to use the
.BR xinit (1)
utility.  However, \f3xinit\fP is to be considered a tool for
building startup scripts and is not intended for use by end users.  Site
administrators are \f3strongly\fP urged to use \f3xdm\fP,
or build other interfaces for novice users.
.PP
When the X server starts up, it takes over the display.  If you 
are running on a workstation whose console is the display, you cannot log into
the console while the server is running.
.SH "NETWORK CONNECTIONS"
The X server supports connections made using the following reliable
byte-streams:
.TP 4
.I TCP\/IP
.br
Listen on port 6000+\fIn\fP, where \fIn\fP is the display number.
.TP 4
.I "Unix Domain"
Use \f3/tmp/.X11-unix/X\fP\fIn\fP as the filename for 
the socket, where \fIn\fP is the display number.
.TP 4
.I "DECnet"
.br
Respond to connections to object \f3X$X\fP\fIn\fP, where \fIn\fP
is the display number.  This is not supported in all environments.
.SH OPTIONS
All X servers accept the following command line options:
.TP 8
.B \-a \fInumber\fP
Set pointer acceleration (i.e. the ratio of how much is reported to how much
the user actually moved the pointer).  The default is 2.
.TP 8
.B \-ac
Disable host-based access control mechanisms.  Enables access by any host,
and permits any host to modify the access control list.
Use with caution.
This option exists primarily for executing remote test suites.
.TP 8
.B \-audit \fIlevel\fP
Set the audit trail level.  The default is 1; only connection
rejections are reported.  Level 2 also reports successful
connections and disconnects.  Level 0 turns off the audit trail.
Audit output is sent on the standard error output stream.
.TP 8
.B \-auth \fIauthorization-file\fP
Specify a file which contains a collection of authorization records used
to authenticate access.  See also
.BR xdm (1) 
.TP 8
.B bc
Disable certain kinds of error checking for bug compatibility with
previous releases (e.g., to work around bugs in R2 and R3 xterms and toolkits).
Deprecated.
.TP 8
.B \-bs
Disable backing store support on all screens.
.TP 8
.B \-c
Turn off key-click.
.TP 8
.B c \fIvolume\fP
Set key-click volume, range is 0-100.
.TP 8
.B \-cc \fIclass\fP
Set the visual class for the root window of color screens.
Class numbers are as specified in the X protocol.
Not implemented in all servers.
.TP 8
.B \-co \fIfilename\fP
Set name of RGB color database.
.TP 8
.B \-core
The server dumps core on fatal errors.
.TP 8
.B \-dpi \fIresolution\fP
Set the resolution of the screen, in dots per inch.
Use when the server cannot determine the screen size from the hardware.
The default is 90.
.TP 8
.B \-f \fIvolume\fP
Set beep (bell) volume, range is 0-100.  The default is 50.
.TP 8
.B \-fc \fIcursorFont\fP
Set default cursor font.
.TP 8
.B \-fn \fIfont\fP
Set default font.
.TP 8
.B \-fp \fIfontPath\fP
Set the font search path.  This path is a comma separated list of
directories which the X server uses to search for font databases.
.TP 8
.B \-ep \fIencodingPath\fP
Set the font encoding search path.
.TP 8
.B \-help
Print a usage message.
.TP 8
.B \-I
All following command line arguments are ignored.
.TP 8
.B \-logo
Turn on the X Window System logo display in the screen-saver.
There is currently no way to change this from a client.
.TP 8
.B nologo
Turn off the X Window System logo display in the screen-saver.
There is currently no way to change this from a client.
.TP 8
.B \-p \fIminutes\fP
Set screen-saver pattern cycle time in minutes.  The default is 10
minutes.
.TP 8
.B \-pn
The server is to continue running if it fails to establish all of
its well-known sockets, but establishes at least one.
.TP 8
.B \-r
Turn off auto-repeat.
.TP 8
.B r
Turn on auto-repeat.
.TP 8
.B \-s \fIminutes\fP
Set screen-saver timeout time in minutes.  The default is 10 minutes.
.TP 8
.B \-su
Disable save under support on all screens.
.TP 8
.B \-t \fInumber\fP
Set pointer acceleration threshold in pixels (i.e. after how many pixels
pointer acceleration should take effect).  The default is 4.
.TP 8
.B \-terminate
The server is to terminate instead of resetting.
.TP 8
.B \-to \fIseconds\fP
Set default connection timeout in seconds.  The default is 60 seconds.
.TP 8
.B \-tst
Disable all testing extensions (e.g., XTEST, XTrap, XTestExtension1).
.TP 8
.B tty\fIxx\fP
ignored, for servers started the ancient way (from init).
.TP 8
.B v
Set video-off screen-saver preference.
.TP 8
.B \-v
Set video-on screen-saver preference.
.TP 8
.B \-wm
Force the default backing-store of all windows to be WhenMapped;
an inexpensive way of getting backing-store to apply to all windows.
.TP 8
.B \-x \fIextension\fP
Load the specified extension at init.
Not supported in most implementations.
.PP
You can also have the X server connect to
.BR xdm (1)
using XDMCP.  Although this is not typically useful as it does not allow \f3xdm\fP
to manage the server process,
it can be used to debug XDMCP implementations, and serves as a sample
implementation of the server side of XDMCP.  For more information on this
protocol, see the \fIX Display Manager Control Protocol\fP specification.
The following options control the behavior of XDMCP.
.TP 8
.B \-query \fIhost-name\fP
Enable XDMCP and send Query packets to the specified host.
.TP 8
.B \-broadcast
Enable XDMCP and broadcast BroadcastQuery packets to the network.  The
first responding display manager will be chosen for the session.
.TP 8
.B \-indirect \fIhost-name\fP
Enable XDMCP and send IndirectQuery packets to the specified host.
.TP 8
.B \-port \fIport-num\fP
Use an alternate port number for XDMCP packets.  Must be specified before
any \-query, \-broadcast or \-indirect options.
.TP 8
.B \-once
Terminate the server after one session.
.TP 8
.B \-class \fIdisplay-class\fP
XDMCP has an additional display qualifier used in resource lookup for
display-specific options.  This option sets that value, by default it 
is "MIT-Unspecified" (not a very useful value).
.TP 8
.B \-displayID \fIdisplay-id\fP
Yet another XDMCP specific value, this one allows the display manager to
identify each display so that it can locate the shared key.
.PP
Many servers also have device-specific command line options.  See the
manual pages for the individual servers for more details.
.SH SECURITY
.PP
The X server implements a simple authorization protocol,
MIT-MAGIC-COOKIE-1 which uses data private to authorized clients and the
server.  This is a rather trivial scheme; if the client passes authorization
data which is the same as that held by the server, it is allowed connection
access.  This scheme
is worse than the host-based access control mechanisms in environments with
unsecure networks as it allows any host to connect, given that it has
the private key.  But in many environments, this level of
security is better than the host-based scheme as it provides access control
on a per-user instead of per-host basis.
.PP
In addition, the server provides support for a DES-based authorization
scheme, SUN-DES-1, using Sun's Secure RPC.
It involves encrypting data with the X server's public key.
See the 
.\" .I OpenWindows 3.3 Programmer's Guide
.TZ OWPG
for more information.
.PP
The authorization data is passed to the server in a private file named with
the \f3\-auth\fP command line option.  Before accepting
connections after a reset or when the server is starting,
it reads this file.  If this file contains authorization records, the
local host is not allowed access to the server; only
clients which send one of the authorization records contained in the file in
the connection setup information are allowed access.  See the
.BR xauth (1)
manual page for a description of the binary format of this file.
.PP
The X server also uses a host-based access control list for deciding
whether or not to accept connections from clients on a particular machine.
If no other authorization mechanism is being used,
this list initially consists of the host on which the server is running as
well as any machines listed in the file \f3/etc/X\f2\fIn\f3.hosts\f1, where
\f2n\f1 is the display number of the server.  The file
contains either an Internet hostname (e.g. expo.lcs.mit.edu) or a DECnet
hostname in double colon format (e.g. hydra::).  Each hostname must be
newline separated with no leading or trailing whitespace.  For example:
.sp
.in +8
.nf 
joesworkstation
corporate.company.com
star::
bigcpu::
.fi
.in -8
.PP
Users add or remove hosts from this list and enable or disable access
control using the \f3xhost\f1 command from the same machine as the server.
.PP
The X protocol intrinsically does not have any notion of window operation
permissions or place any restrictions on what a client can do; if a program can
connect to a display, it has full run of the screen.  Sites that have better
authentication and authorization systems (such as Kerberos) might wish to make
use of the hooks in the libraries and the server to provide additional
security models.
.SH SIGNALS
The X server handles the following signals:
.TP 8
.I SIGHUP
Close all existing connections, free all
resources, and restore server defaults.  This signal is sent by the 
display manager
whenever the user's primary application (usually an 
.BR xterm (1)
or window manager) exits to force the server to clean up and prepare
for the next user.
.TP 8
.I SIGTERM
The server exits cleanly.
.TP 8
.I SIGUSR1
This signal is used quite differently from either of the above.  When the
server starts, determines if SIGUSR1 is set to SIG_IGN.
If this is true the server sends a SIGUSR1 to
its parent process after it has set up the various connection schemes.
\f3Xdm\fP uses this feature to recognize when the server is ready for
client connections (see
.BR xdm (1)
).
.SH FONTS
Fonts are normally stored as individual files across various directories.  
The X server
can obtain fonts from directories and/or from font servers.
The list of directories and font servers
the X server uses when trying to open a font is controlled
by the \fIfont path\fP.  Although most sites will choose to have the X server
start up with the appropriate font path (see the \f3\-fp\fP),
This path can be overridden using the
.BR xset (1)
program.
.PP
The default font path for the X server includes eight directories:
.TP 8
.I $OPENWINHOME/lib/X11/fonts/misc
This directory contains many miscellaneous bitmap fonts that are useful on all
systems.  It contains a family of fixed-width fonts,
a family of fixed-width fonts from Dale Schumacher,
several Kana fonts from Sony Corporation,
two JIS Kanji fonts,
two Hangul fonts from Daewoo Electronics,
two Hebrew fonts from Joseph Friedman,
the standard cursor font, two cursor fonts from
Digital Equipment Corporation, and cursor and glyph fonts
from Sun Microsystems.
It also has various font name aliases for the fonts, including
\f3fixed\fP and \f3variable\fP.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/Speedo
This directory contains outline fonts for Bitstream's Speedo rasterizer.
A single font face, contributed by Bitstream, Inc., in normal, bold, italic, 
and bold italic, is provided.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/75dpi
This directory contains bitmap fonts contributed by Adobe Systems, Inc.,
Digital Equipment Corporation, Bitstream, Inc.,
Bigelow and Holmes, and Sun Microsystems, Inc.
for 75 dots per inch displays.  An integrated selection of sizes, styles, 
and weights are provided for each family.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/100dpi
This directory contains 100 dots per inch versions of some of the fonts in the 
\fI75dpi\fP directory.  
.TP 8
.I $OPENWINHOME/lib/X11/fonts/F3
This directory contains scalable outlie fonts for the F3 format.  57
typefaces are present.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/F3bitmaps
This directory contains bitmaps in various point sized for the 57 F3
fonts.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/Xt+
This directory contains fonts used by OLIT.
.TP 8
.I $OPENWINHOME/lib/X11/fonts/Type1
This directory contains outline Adobe Type1 fonts.
.PP
Font databases are created by executing the
.BR mkfontdir (1)
program in the
directory containing the compiled versions of the fonts (the \f3.pcf\fP files).
Whenever fonts are added to a directory, \f3mkfontdir\fP should be executed
so that the server can find the new fonts.  If \f3mkfontdir\fP is not
run, the server will not find any fonts in the directory.
.SH DIAGNOSTICS
Too numerous to list them all.
If run from
.BR init (1M),
errors are typically logged
in the file \f3/usr/adm/X*msgs\fP,
.SH FILES
.TP 30
/etc/X*.hosts
Initial access control list
.TP 30
$OPENWINHOME/lib/X11/fonts/misc
Bitmap font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/75dpi
Bitmap font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/100dpi 
Bitmap font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/Speedo
Outline font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/F3
F3 outline font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/F3bitmaps
Bitmap font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/Xt+
OLIT font directory
.TP 30
$OPENWINHOME/lib/X11/fonts/Type1
Outline font directories
.TP 30
$OPENWINHOME/lib/X11/fonts/PEX
PEX font directories
.TP 30
$OPENWINHOME/lib/X11/rgb.txt
Color database
.TP 30
/tmp/.X11-unix/X*
Unix domain socket
.TP 30
/usr/adm/X*msgs
Error log file
.SH "SEE ALSO"
.BR X11 (7),
.BR bdftopcf (1),
.BR mkfontdir (1),
.BR xauth (1),
.BR xdm (1),
.BR xhost (1),
.BR xinit (1),
.BR xset (1),
.BR xsetroot (1),
.BR xterm (1),
.BR Xsun (1)
.I "X Window System Protocol,"
.I "Definition of the Porting Layer for the X v11 Sample Server,"
.I "Strategies for Porting the X v11 Sample Server,"
.I "Godzilla's Guide to Porting the X V11 Sample Server"
.SH BUGS
The option syntax is inconsistent with itself and \f3xset\fP(1).
.PP
The acceleration option should take a numerator and a denominator like the
protocol.
.PP
If \f3X\fP
dies before its clients, new clients won't be able to connect until all
existing connections have their TCP TIME_WAIT timers expire.
.PP
The color database is missing a large number of colors.
.PP
.SH COPYRIGHT
Copyright 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991
Massachusetts Institute of Technology.
.br
See \f3X11\fP(7) for a full statement of rights and permissions.
.SH AUTHORS
The sample server was originally written by Susan Angebranndt, Raymond
Drewry, Philip Karlton, and Todd Newman, from Digital Equipment
Corporation, with support from a large cast.  It has since been
extensively rewritten by Keith Packard and Bob Scheifler, from MIT.

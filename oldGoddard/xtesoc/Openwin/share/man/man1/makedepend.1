.\" Copyright (c) 1994, Sun Microsystems, Inc.
.TH makedepend 1 "19 March 1992"
.IX "makedepend" "" "\f3makedepend\f1(1) \(em create makefile dependencies" ""
.SH NAME
makedepend \- create dependencies in makefiles
.SH SYNOPSIS
.B makedepend
[
.B \-D \fIname=def\fP
] [
.B \-D \fIname\fP
] [
.B \-I \fIincludedir\fP
] [
.B \-f \fImakefile\fP
] [
.B \-o \fIobjsuffix\fP
]
.br
	[
.B \-s \fIstring\fP
] [
.B \-w \fIwidth\fP
] [
.B \-\ 
.I \- otheroptions
]
.I sourcefile 
...
.br
.SH DESCRIPTION
.B Makedepend
reads each
.I sourcefile
in sequence and parses it like a C-preprocessor,
processing all
.I #include,
.I #define,
.I #undef,
.I #ifdef,
.I #ifndef,
.I #endif,
.I #if
and
.I #else
directives so that it can correctly tell which
.I #include,
directives would be used in a compilation.
Any
.I #include
directives can reference files having other
.I #include
directives--parsing will occur in these files as well.
.PP
Every file that a
.I sourcefile
includes,
directly or indirectly,
is what
.B makedepend
calls a \fIdependency\fP.
These dependencies are then written to a
.B makefile
in such a way that
.BR make (1S)
will know which object files must be recompiled when a dependency has changed.
.PP
By default,
.B makedepend
places its output in the file named
.B makefile
if it exists, otherwise
.B Makefile
.
An alternate makefile may be specified with the
.B -f
option.
It first searches the makefile for
the line
.sp
    # DO NOT DELETE THIS LINE -- make depend depends on it.
.sp
or one provided with the
.B -s
option,
as a delimiter for the dependency output.
If it finds it, it will delete everything
following this to the end of the \fBmakefile\fP
and put the output after this line.
If it doesn't find it, the program
will append the string to the end of the \fBmakefile\fP
and place the output following that.
For each
.I sourcefile
appearing on the command line,
.B makedepend
puts lines in the \fBmakefile\fP of the form
.sp
     \fIsourcefile\fP.\fBo:\fP \fIdfile\fP ...
.sp
Where \fIsourcefile\fP.\fBo\fP is the name from the command
line with its suffix replaced with \fB.o\fP,
and \fIdfile\fP is a dependency discovered in a
.I #include
directive while parsing
.I sourcefile
or one of the files it included.
.SH EXAMPLE
Normally,
.B makedepend
will be used in a \fBmakefile\fP target so that typing "make depend" will
bring the dependencies up to date for the \fBmakefile\fP.
For example,
.nf
    SRCS = file1.c file2.c ...
    CFLAGS = -O -DHACK -I../foobar -xyz
    depend:
            makedepend -- $(CFLAGS) -- $(SRCS)
.fi
.SH OPTIONS
.B Makedepend
will ignore any option that it does not understand so that you may use
the same arguments that you would for
.BR cc (1B).
.TP 5
.B \-D 
.I name=def 
or 
.B \-D
.I name
Define.
This places a definition for
.I name
in
.B makedepend's
symbol table.
Without 
.I =def
the symbol becomes defined as "1".
.TP 5
.B \-I
.I includedir
Include directory.
This option tells
.B makedepend
to prepend
.I includedir
to its list of directories to search when it encounters
a
.I #include
directive.
By default,
.B makedepend
only searches 
.B /usr/include
.
.TP 5
.B \-f
.I makefile
Filename.
This allows you to specify an alternate \fBmakefile\fP in which
.B makedepend
can place its output.
.TP 5
.B \-o
.I objsuffix
Object file suffix.
Some systems may have object files whose suffix is something other
than ".o".
This option allows you to specify another suffix, such as
".b" with
.I -o.b
or ":obj"
with
.I -o:obj
and so forth.
.TP 5
.B \-s
.I string
Starting string delimiter.
This option permits you to specify
a different string for
.B makedepend
to look for in the makefile.
.TP 5
.B \-w
.I width
Line width.
Normally,
.B makedepend
will ensure that every output line that it writes will be no wider than
78 characters for the sake of readability.
This option enables you to change this width.
.TP 5
.B \-\ \-
.I options
.B \-\ \-
If
.B makedepend
encounters a double hyphen (\-\ \-) in the argument list,
then any unrecognized argument following it
will be silently ignored; a second double hyphen terminates this
special treatment.
In this way,
.B makedepend
can be made to safely ignore esoteric compiler arguments that might
normally be found in a CFLAGS
.B make
macro (see the
.B EXAMPLE
section above).
All options that
.B makedepend
recognizes and appear between the pair of double hyphens
are processed normally.
.SH ALGORITHM
The approach used in this program enables it to run an order of magnitude
faster than any other \fIdependency generator\fP
.  Central to this performance are two assumptions:
that all files compiled by a single
.B makefile 
will be compiled with roughly the same
.B -I
and
.B -D
options;
and that most files in a single directory will include largely the
same files.
.PP
Given these assumptions,
.B makedepend
expects to be called once for each \fBmakefile\fP, with
all source files that are maintained by the
\fBmakefile\fP appearing on the command line.
It parses each source and include
file exactly once, maintaining an internal symbol table
for each.
Thus, the first file on the command line will take an amount of time
proportional to the amount of time that a normal C preprocessor takes.
But on subsequent files, if it encounter's an include file
that it has already parsed, it does not parse it again.
.PP
For example,
imagine you are compiling two files,
.B file1.c
and
.B file2.c,
they each include the header file
.B header.h,
and the file
.B header.h
in turn includes the files
.B def1.h
and
.B def2.h.
When you run the command
.sp
    makedepend file1.c file2.c
.sp
.B makedepend
will parse
.B file1.c
and consequently,
.B header.h
and then
.B def1.h
and
.B def2.h.
It then decides that the dependencies for this file are
.sp
    file1.o: header.h def1.h def2.h
.sp
But when the program parses
.B file2.c
and discovers that it, too, includes
.B header.h,
it does not parse the file, but simply adds
.B header.h,
.B def1.h
and
.B def2.h
to the list of dependencies for
.B file2.o.
.SH "SEE ALSO"
.BR cc (1B) ,
.BR make (1S)
.SH BUGS
If you do not have the source for \fBcpp\fP, the Berkeley C preprocessor,
then
.B makedepend
will be compiled in such a way that all
.I #if
directives will evaluate to "true" regardless of their actual value.
This may cause the wrong
.I #include
directives to be evaluated.
.B Makedepend
should simply have its own parser written for
.I #if
expressions.
.PP
Imagine you are parsing two files,
.B file1.c
and
.B file2.c,
each includes the file
.B def.h.
The list of files that
.B def.h
includes might truly be different when
.B def.h
is included by
.B file1.c
than when it is included by
.B file2.c.
But once
.B makedepend
arrives at a list of dependencies for a file,
it is cast in concrete.
.SH AUTHOR
Todd Brunhoff, Tektronix, Inc. and MIT Project Athena

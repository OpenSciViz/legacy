.\" Copyright (c) 1994 Sun Microsystems, Inc.
.TH viewres 1 "21 May 1991"
.IX "viewres" "" "\f3viewres\f1(1) \(em display widget class hierarchy" ""
.SH NAME
viewres \- graphical class browser for Xt
.SH SYNOPSIS
.B "viewres"
[-option ...]
.SH DESCRIPTION
.PP
The
.B viewres
program displays a tree showing the widget class hierarchy of
the Athena Widget Set.  Each node in the tree can be expanded to show the
resources that the corresponding class adds (i.e. does not inherit from its
parent) when a widget is created.  This application allows the user to visually
examine the structure and inherited resources for the Athena Widget Set.
.SH OPTIONS
.B Viewres
accepts all of the standard toolkit command line options as well as the following:
.TP 8
.B \-top \fIname\f1
This option specifies the name of the highest widget in the hierarchy to 
display.  This is typically used to limit the display to a subset of the
tree.  The default is \fIObject\f1.
.TP 8
.B \-variable
This option indicates that the widget variable names (as declared in 
header files) should be displayed in the nodes rather than the widget
class name.  This is sometimes useful to distinguish widget classes that
share the same name (such as \fIText\f1).
.TP 8
.B \-vertical
This option indicates that the tree should be displayed top to bottom
rather left to right.
.SH "VIEW MENU"
The way in which the tree is displayed may be changed with through the 
entries in the \f3View\f1 menu:
.TP 8
.B "Show Variable Names"
This entry causes the node labels to be set to the variable names used
to declare the corresponding widget class.  This operation may also be 
performed with the \f3SetLabelType(variable)\f1 translation.
.TP 8
.B "Show Class Names"
This entry causes the node labels to be set to the class names used when
specifying resources.  This operation may also be performed with the
\f3SetLabelType(class)\f1 translation.
.TP 8
.B "Layout Horizontal"
This entry causes the tree to be laid out from left to right.  This operation
may also be performed with the \fISetOrientation(West)\f1 translation.
.TP 8
.B "Layout Vertical"
This entry causes the tree to be laid out from top to bottom.  This operation
may also be performed with the \fISetOrientation(North)\f1 translation.
.TP 8
.B "Show Resource Boxes"
This entry expands the selected nodes (see next section) to show the 
new widget and constraint resources.  This operation
may also be performed with the \fIResources(on)\f1 translation.
.TP 8
.B "Hide Resource Boxes"
This entry removes the resource displays from the selected nodes (usually to
conserve space).  This operation
may also be performed with the \fIResources(off)\f1 translation.
.SH "SELECT MENU"
Resources for a single widget class can be displayed by clicking
\f3Button2\f1 on the corresponding node, or by adding the node to the
selection list with \f3Button1\f1 and using the \f3Show Resource Boxes\f1
entry in the \f3View\f1 menu.  Since \f3Button1\f1 actually toggles the
selection state of a node, clicking on a selected node will cause it to be
removed from the selected list.
.PP
Collections of nodes may also be selected through the various entries in
the \f3Select\f1 menu:
.TP 8
.B "Unselect All"
This entry removes all nodes from the selection list.  This operation
may also be performed with the \fISelect(nothing)\f1 translation.
.TP 8
.B "Select All"
This entry adds all nodes to the selection list.  This operation
may also be performed with the \fISelect(all)\f1 translation.
.TP 8
.B "Invert All"
This entry adds unselected nodes to, and removes selected nodes from, the
selection list.  This operation
may also be performed with the \fISelect(invert)\f1 translation.
.TP 8
.B "Select Parent"
This entry selects the immediate parents of all selected nodes.  This operation
may also be performed with the \fISelect(parent)\f1 translation.
.TP 8
.B "Select Ancestors"
This entry recursively selects all parents of all selected nodes.  This 
operation may also be performed with the \fISelect(ancestors)\f1 translation.
.TP 8
.B "Select Children"
This entry selects the immediate children of all selected nodes.  This 
operation may also be performed with the \fISelect(children)\f1 translation.
.TP 8
.B "Select Descendants"
This entry recursively selects all children of all selected nodes.  This 
operation may also be performed with the \fISelect(descendants)\f1 translation.
.TP 8
.B "Select Has Resources"
This entry selects all nodes that add new resources (regular or constraint)
to their corresponding widget classes.  This operation
may also be performed with the \fISelect(resources)\f1 translation.
.TP 8
.B "Select Shown Resource Boxes"
This entry selects all nodes whose resource boxes are currently expanded
(usually so that they can be closed with \f3Hide Resource Boxes\f1).  This 
operation may also be performed with the \fISelect(shown)\f1 translation.
.SH "ACTIONS"
The following application actions are provided:
.TP 8
.B "Quit()"
.br
This action causes \f3viewres\f1 to exit.
.TP 8
.B "SetLabelType(\fItype\f1)"
This action sets the node labels to display the widget \fIvariable\f1 or 
\fIclass\f1 names, according to the argument \fItype\f1.
.TP 8
.B "SetOrientation(\fIdirection\f1)"
This action sets the root of the tree to be one of the following areas of
the window:  \fIWest\f1, \fINorth\f1, \fIEast\f1, or \fISouth\f1.
.TP 8
.B "Select(\fIwhat\f1)"
This action selects the indicated nodes, as described in the \f3VIEW MENU\f1
section: \fInothing\f1 (unselects all nodes), \fIinvert\f1, \fIparent\f1,
\fIancestors\f1, \fIchildren\f1, \fIdescendants\f1, \fIresources\f1, 
\fIshown\f1.
.TP 8
.B "Resources(\fIop\f1)"
This action turns \fIon\f1, \fIoff\f1, or \fItoggles\f1 the resource boxes
for the selected nodes.
If invoked from within one of the nodes (through the keyboard or pointer),
only that node is used.
.SH "WIDGET HIERARCHY"
Resources may be specified for the following widgets:
.sp
.nf
.ta .5i 1.0i 1.5i 2.0i 2.5i 3.0i 3.5i 4.0i 4.5i 5.0i 5.5i 6.0i 6.5i 7.0i
Viewres viewres
	Paned pane
		Box buttonbox
			Command quit
			MenuButton view
				SimpleMenu viewMenu
					SmeBSB layoutHorizontal
					SmeBSB layoutVertical
					SmeLine line1
					SmeBSB namesVariable
					SmeBSB namesClass
					SmeLine line2
					SmeBSB viewResources
					SmeBSB viewNoResources
			MenuButton select
				SimpleMenu selectMenu
					SmeBSB unselect
					SmeBSB selectAll
					SmeBSB selectInvert
					SmeLine line1
					SmeBSB selectParent
					SmeBSB selectAncestors
					SmeBSB selectChildren
					SmeBSB selectDescendants
					SmeLine line2
					SmeBSB selectHasResources
					SmeBSB selectShownResources
		Form treeform
			Porthole porthole
				Tree tree
					Box \fIvariable-name\f1
						Toggle \fIvariable-name\f1
						List \fIvariable-name\f1
			Panner panner
.fi
.sp
where \fIvariable-name\f1 is the widget variable name of each node.
.SH "SEE ALSO"
.BR X11 (7),
.BR xrdb (1), 
.BR listres (1),
.BR editres (1), 
.BR appres (1), 
appropriate widget documents
.SH COPYRIGHT
Copyright 1990, Massachusetts Institute of Technology.
.br
See 
.B X11 (7)
for a full statement of rights and permissions.
.SH AUTHOR
Jim Fulton, MIT X Consortium

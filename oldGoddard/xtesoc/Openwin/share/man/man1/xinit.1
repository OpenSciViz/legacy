.\" Copyright (c) 1994 Sun Microsystems, Inc.
.TH xinit 1 "18 March 1994"
.IX "xinit" "" "\f3xinit\f1(1) \(em Xserver initialization program" ""
.IX "Xserver" "init" "Xserver" "initialization program \(em \f3xinit\f1(1)"
.SH NAME
xinit \- X Window System initializer
.SH SYNOPSIS
.B xinit
[ [
.I client
]
.I options
] [
.B \-\^\-
[
.I server
] [
.I display
]
.I options
]
.SH DESCRIPTION
The \f3xinit\f1 program is used to start the X Window System server and a first
client program on systems that
cannot start X directly from \fI/etc/init\f1 or in environments
that use multiple window systems.  When this first client exits, 
\f3xinit\f1 will kill the X server and then terminate.
.PP
If no specific client program is given on the command line,
\f3xinit\f1 will look for a file in the user's home directory
called \fI.xinitrc\f1 to run as a shell script to start up client programs.
If no such file exists, \f3xinit\f1 will use the following as a default:
.sp
	xterm  \-geometry  +1+1  \-n  login  \-display  :0
.sp
.PP
If no specific server program is given on the command line,
\f3xinit\f1 will look for a file in the user's home directory
called \fI.xserverrc\f1 to run as a shell script to start up the server.
If no such file exists, \f3xinit\f1 will use the following as a default:
.sp
	X  :0
.sp
Note that this assumes that there is a program named \fIX\f1 in the current
search path.  However, servers are usually named \fIXdisplaytype\f1 where 
\fIdisplaytype\f1 is the type of graphics display which is driven by this
server.  The site administrator should, therefore, make a link to the 
appropriate type of server on the machine, or create a shell script that
runs \f3xinit\f1 with the appropriate server.
.PP
An important point is that programs which are run by \fI\.xinitrc\f1
should be run in the background if they do 
not exit right away, so that they don't prevent other programs from
starting up.
However, the last long-lived program started (usually
a window manager or terminal emulator) should be
left in the foreground so that the script won't exit (which
indicates that the user is done and that \f3xinit\f1 should exit).
.PP
An alternate client and/or server may be specified on the
command line.  The desired client program and its arguments should be given
as the first command line arguments to \f3xinit\f1.  To specify a particular
server command line, append a double dash (\-\^\-) to the \f3xinit\f1 command
line (after any client and arguments) followed by the desired server command.
.PP
Both the client program name and the server program name must begin with a
slash (/) or a period (.).  Otherwise, they are treated as an arguments to be
appended to their respective startup lines.  This makes it possible to 
add arguments (for example, foreground and background colors) without 
having to retype the whole command line.
.PP
If an explicit server name is not given and the first argument following the
double dash (\-\^\-) is a colon followed by a digit, \f3xinit\f1 will use that 
number as the display 
number instead of zero.  All remaining arguments are appended to the server 
command line.
.PP
.SH EXAMPLES
Below are several examples of how command line arguments in \f3xinit\f1 are
used.
.TP 8
.B "xinit"
This will start up a server named \fIX\f1 and run the user's \fI\.xinitrc\f1,
if it exists, or else start an \fIxterm\f1.
.TP 8
.B "xinit \-\^\- /usr/bin/X11/Xqdss  :1"
This is how one could start a specific type of server on an alternate display.
.TP 8
.B "xinit \-geometry =80x65+10+10 \-fn 8x13 \-j \-fg white \-bg navy"
This will start up a server named \fIX\f1, and will append the given
arguments to the default \fIxterm\f1 command.  It will ignore \fI\.xinitrc\f1.
.TP 8
.B "xinit \-e widgets \-\^\- ./Xsun \-l \-c"
This will use the command \fI\.\/Xsun \-l \-c\f1 to start the server and will
append the arguments \fI\-e widgets\f1 to the default \fIxterm\f1 command.
.TP 8
.B "xinit /usr/ucb/rsh fasthost cpupig \-display ws:1 \-\^\-  :1 \-a 2 \-t 5"
This will start a server named \fIX\f1 on display 1 with the arguments
\fI\-a 2 \-t 5\f1.  It will then start a remote shell on the machine
\f3fasthost\f1 in which it will run the command \fIcpupig\f1, telling it
to display back on the local workstation.
.PP
Below is a sample \fI\.xinitrc\f1 that starts a clock, several terminals, and
leaves the window manager running as the ``last'' application.  Assuming that
the window manager has been configured properly, the user
then chooses the ``Exit'' menu item to shut down X.
.sp
.in +8
.nf
xrdb \-load $HOME/.Xresources
xsetroot \-solid gray &
xclock \-g 50x50\-0+0 \-bw 0 &
xload \-g 50x50\-50+0 \-bw 0 &
xterm \-g 80x24+0+0 &
xterm \-g 80x24+0\-0 &
twm
.fi
.in -8
.sp
Sites that want to create a common startup environment could simply create
a default \fI\.xinitrc\f1 that references a site-wide startup file:
.sp
.in +8
.nf
#!/bin/sh
\&. /usr/local/lib/site.xinitrc
.fi
.in -8
.sp
Another approach is to write a script that starts \f3xinit\f1 with a specific
shell script.  Such scripts are usually named \fIx11\f1, \fIxstart\f1, or
\fIstartx\f1 and are a convenient way to provide a simple interface for
novice users:
.sp
.in +8
.nf
#!/bin/sh
xinit /usr/local/lib/site.xinitrc \-\^\- /usr/bin/X11/X bc
.fi
.in -8
.sp
.SH "ENVIRONMENT VARIABLES"
.TP 15
.B DISPLAY
This variable gets set to the name of the display to which clients should
connect.
.TP 15
.B XINITRC
This variable specifies an init file containing shell commands to start up the
initial windows.  By default, \fI\.xinitrc\f1 in the home directory will be 
used.
.SH FILES
.TP 15
.I .xinitrc
default client script
.TP 15
.I xterm
client to run if \fI.xinitrc\f1 does not exist
.TP 15
.I .xserverrc
default server script
.TP 15
.I X
server to run if \fI.xserverrc\f1 does not exist
.SH SEE ALSO
.BR olwm (1),
.BR openwin (1),
.BR props (1),
.BR X11 (7),
.BR Xserver (1),
.BR xterm (1)
.SH COPYRIGHT
Copyright 1988, Massachusetts Institute of Technology.
.br
See
.BR X11 (7)
for a full statement of rights and permissions.
.SH AUTHOR
Bob Scheifler, MIT Laboratory for Computer Science

.ds xL Programming With Xlib
.TH XGrabDevice 3X11 "Release 5" "X Version 11" "X FUNCTIONS"
.SH NAME
XGrabDevice, XUngrabDevice \- grab/release the specified extension device
.SH SYNTAX
.nf
\f3
int XGrabDevice\^(\^\fIdisplay\f1, \fIdevice\f1\^, \fIgrab_window\f1\^, \fIowner_events\f1\^, \fIevent_count\f1\^, \fIevent_list\f1\^,
\fIthis_device_mode\f1\^, \fIother_devices_mode\f1\^, \fItime\f1\^)
      Display *\fIdisplay\f1\^;
      XDevice *\fIdevice\f1\^;
      Window \fIgrab_window\f1\^;
      Bool \fIowner_events\f1\^;
      int \fIevent_count\f1\^;
      XEventClass *\fIevent_list\f1\^;
      int \fIthis_device_mode\f1\^, \fIother_devices_mode\f1\^;
      Time \fItime\f1\^;

XUngrabDevice\^(\^\fIdisplay\f1, \fIdevice\f1\^, \fItime\f1\^)
      Display *\fIdisplay\f1\^;
      XDevice *\fIdevice\f1\^;
      Time \fItime\f1\^;
.fi
\f1
.SH ARGUMENTS
.TP 12
.I display
Specifies the connection to the X server.
.TP 12
.I device
Specifies the device to be grabbed or released.
.TP 12
.I grab_window
Specifies the id of a window to be associated with the device.
.TP 12
.I owner_events
Specifies a Boolean value that indicates whether the 
events from the device 
are to be reported as usual or reported with respect to the grab window 
if selected by the event list.
.TP 12
.I event_count
Specifies the number of elements in the event_list array.
.TP 12
.I event_list
Specifies a pointer to a list of event classes that indicates which events
the client wishes to receive.  These event classes must have been
obtained specifying the device being grabbed.
.TP 12
.I this_device_mode
Specifies further processing of events from this device.
You can pass 
\fIGrabModeSync\f1
or
\fIGrabModeAsync\f1.
.TP 12
.I other_devices_mode
Specifies further processing of events from other devices.
You can pass 
\fIGrabModeSync\f1 
or
\fIGrabModeAsync\f1.
.TP 12
.I time
Specifies the time.
You can pass either a timestamp or
\fICurrentTime\f1.
.SH DESCRIPTION
The
\f3XGrabDevice\f1
request actively grabs control of the device and generates
\fIDeviceFocusIn\f1
and
\fIDeviceFocusOut\f1
events.
Further device events are reported only to the
grabbing client.
\f3XGrabDevice\f1
overrides any active device grab by this client.
event_list is a pointer to a list of event classes.  This list indicates
which events the client wishes to receive while the grab is active.
If owner_events is 
\fIFalse\f1 , 
all generated device events are reported with
respect to grab_window if selected.
If owner_events is 
\fITrue\f1  
and if a generated
device event would normally be reported to this client, it is reported
normally; otherwise, the event is reported with respect to the
grab_window, and is only reported if specified in the event_list.
.P
If the this_device_mode argument is 
\fIGrabModeAsync\f1 ,
device event processing continues
as usual. 
If the device is currently frozen by this client, 
then processing of device events is resumed.
If the this_device_mode  argument is
\fIGrabModeSync\f1 ,
the state of the device (as seen by client applications) appears to freeze,
and the X server generates no further device events until the
grabbing client issues a releasing 
\fIXAllowDeviceEvents\f1 
call or until the device grab is released.
Actual device changes are not lost while the device is frozen; 
they are simply queued in the server for later processing.
.P
If other_devices_mode is 
\fIGrabModeAsync\f1 ,
processing of events from other devices is unaffected
by activation of the grab.  
If other_devices_mode is 
\fIGrabModeSync\f1,
the state of all devices except the grabbed device
 (as seen by client applications) appears to freeze, 
and the X server generates no further events from those devices
until the grabbing client issues a releasing 
\fIXAllowDeviceEvents\f1 
call or until the device grab is released.
Actual events are not lost while the devices are frozen; 
they are simply queued in the server for later processing.
.P
If the device is actively grabbed by some other client,
\f3XGrabDevice\f1
fails and returns
\fIAlreadyGrabbed\f1.
If grab_window is not viewable,
it fails and returns
\fIGrabNotViewable\f1.
If the device is frozen by an active grab of another client,
it fails and returns
\fIGrabFrozen\f1.
If the specified time is earlier than the last-device-grab time 
or later than the current X server time,
it fails and returns
\fIGrabInvalidTime\f1.
Otherwise, the last-device-grab time is set to the specified time
\fI( CurrentTime\f1 
is replaced by the current X server time).
.LP
If a grabbed device is closed by a client while an active grab by that
client is in effect, the active grab is released.  If the device is
frozen only by an active grab of the requesting client, it is thawed.
.P
\f3XGrabDevice\f1
can generate
\fIBadClass\f1, \fIBadDevice\f1, \fIBadValue\f1, and \fIBadWindow\f1 
errors.
.P
The
\fIXUngrabDevice\f1
request
releases the device and any queued events if this client has it actively 
grabbed from either
\f3XGrabDevice\f1
or
\fIXGrabDeviceKey\f1.
If other devices are frozen by the grab, \fIXUngrabDevice\f1 thaws them.
\fIXUngrabDevice\f1
does not release the device and any queued events
if the specified time is earlier than
the last-device-grab time or is later than the current X server time.
It also generates
\fIDeviceFocusIn\f1 
and 
\fIDeviceFocusOut\f1 
events.
The X server automatically performs an 
\fIUngrabDevice\f1 
request if the event window for an
active device grab becomes not viewable.
.LP
\fIXUngrabDevice\f1
can generate a \fIBadDevice\f1 error.
.SH DIAGNOSTICS
.TP 12
\fIBadDevice\f1
An invalid device was specified.  The specified device does not exist or has 
not been opened by this client via \fIXOpenInputDevice\f1.  This error may
also occur if the specified device is the X keyboard or X pointer device.
.TP 12
\fIBadValue\f1
Some numeric value falls outside the range of values accepted by the request.
Unless a specific range is specified for an argument, the full range defined
by the argument's type is accepted.  Any argument defined as a set of
alternatives can generate this error.
.TP 12
\fIBadWindow\f1
A value for a Window argument does not name a defined Window.
.SH "SEE ALSO"
XAllowDeviceEvents(3X),
XGrabDeviceButton(3X),
XGrabDeviceKey(3X),
.br
\fI\*(xL\f1

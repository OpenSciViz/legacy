.ds xL Programming With Xlib
.TH XGetFeedbackControl 3X11 "Release 5" "X Version 11" "X FUNCTIONS"
.SH NAME
XGetFeedbackControl, XChangeFeedbackControl \- query and change input device feedbacks
.SH SYNTAX
XFeedbackState *
XGetFeedbackControl\^(\^\fIdisplay\f1, \fIdevice\f1\^, \fInum_feedbacks\f1\^)
.br
      Display *\fIdisplay\f1\^;
.br
      XDevice *\fIdevice\f1\^; 
.br
      int *\fInum_feedbacks\f1\^; 
.br
.sp 
int XChangeFeedbackControl\^(\^\fIdisplay\f1, \fIdevice\f1\^, \fImask\f1\^, \fIcontrol\f1\^)
.br
      Display *\fIdisplay\f1\^;
.br
      XDevice *\fIdevice\f1\^; 
.br
      Mask \fImask\f1\^; 
.br
      XFeedbackControl *\fIcontrol\f1\^; 
.SH ARGUMENTS
.TP 15
.I display
Specifies the connection to the X server.
.TP 15
.I device
Specifies the device whose feedbacks are to be queried or modified.
.TP 15
.I num_feedbacks
Specifies an address into which the number of feedbacks supported by the
device is to be returned.
.TP 15
.I mask 
Specifies a mask specific to each type of feedback that describes how 
the feedback is to be modified.  
.TP 15 
.I control
Specifies the address of an \fIXFeedbackControl\f1 structure that contains
the new values for the feedback.
.SH DESCRIPTION
These requests are provided to manipulate those input devices that
support feedbacks.  A \fIBadMatch\f1 error will be generated if the
requested device does not support feedbacks.  Whether or not a given
device supports feedbacks can be determined by examining the information
returned by the \fIXOpenDevice\f1 request.  For those devices that
support feedbacks, \fIXOpenDevice\f1 will return an
\fIXInputClassInfo\f1 structure with the input_class field
equal to the constant \fIFeedbackClass\f1 (defined in the file \fIXI.h\f1).
.P
The \f3XGetFeedbackControl\f1 request returns a pointer to a list of 
\fIXFeedbackState\f1 structures.  Each item in this list describes one
of the feedbacks supported by the device.  The items are variable length,
so each contains its length to allow traversal to the next item in the
list.
.P
The feedback classes that are currently defined are:  \fIKbdFeedbackClass\f1,
\fIPtrFeedbackClass\f1, \fIStringFeedbackClass\f1, \fIIntegerFeedbackClass\f1,
\fILedFeedbackClass\f1, and \fIBellFeedbackClass\f1.  These constants are
defined in the file \fIXI.h\f1.  An input device may
support zero or more classes of feedback, and may support multiple feedbacks
of the same class.  Each feedback contains a class identifier and an id
that is unique within that class for that input device.  The id is used
to identify the feedback when making an \fIXChangeFeedbackControl\f1 request.
.P
\f3XGetFeedbackControl\f1 can generate a \fIBadDevice\f1 or
\fIBadMatch\f1 error.
.P
The \fIXChangeFeedbackControl\f1 request modifies the values of one 
feedback on the specified device.  The feedback is identified by the id
field of the \fIXFeedbackControl\f1 structure that is passed with the
request.  The fields of the feedback that are to be modified are identified
by the bits of the mask that is passed with the request.
.P
\fIXChangeFeedbackControl\f1 can generate a \fIBadDevice\f1,
\fIBadMatch\f1, or \fIBadValue\f1  error.
.SH STRUCTURES
Each class of feedback is described by a structure specific to that class.
These structures are defined in the file \fIXInput.h\f1.
\fIXFeedbackState\f1 and \fIXFeedbackControl\f1 are generic 
structures that contain three fields that are at the beginning of each class
of feedback:
.P
.DS
typedef struct {
.br
	XID class;                         
.br
	int length;                                      
.br
	XID id;
.br
} XFeedbackState, XFeedbackControl;
.DE
.P
The \fIXKbdFeedbackState\f1 structure defines the attributes that are
returned for feedbacks equivalent to those on the X keyboard.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     click;
	int     percent;
	int     pitch;
	int     duration;
	int     led_mask;
	int     global_auto_repeat;
	char    auto_repeats[32];
} XKbdFeedbackState;
.fi
.DE
.P
The \fIXPtrFeedbackState\f1 structure defines the attributes that are
returned for feedbacks equivalent to those on the the X pointer.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     accelNum;
	int     accelDenom;
	int     threshold;
} XPtrFeedbackState;
.fi
.DE
.P
The \fIXIntegerFeedbackState\f1 structure defines attributes that are 
returned for integer feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     resolution;
	int     minVal;
	int     maxVal;
} XIntegerFeedbackState;
.fi
.DE
.P
The \fIXStringFeedbackState\f1 structure defines the attributes that are
returned for string feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     max_symbols;
	int     num_syms_supported;
	KeySym  *syms_supported;
} XStringFeedbackState;
.fi
.DE
.P
The \fIXBellFeedbackState\f1 structure defines the attributes that are
returned for bell feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     percent;
	int     pitch;
	int     duration;
} XBellFeedbackState;
.fi
.DE
.P
The \fIXLedFeedbackState\f1 structure defines the attributes that are
returned for LED feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     led_values;
} XLedFeedbackState;
.fi
.DE
.P
The \fIXPrtFeedbackControl\f1 structure defines the attributes that can be
controlled for pointer feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     accelNum;
	int     accelDenom;
	int     threshold;
} XPtrFeedbackControl;
.fi
.DE
.P
The \fIXKbdFeedbackControl\f1 structure defines the attributes that can be
controlled for keyboard feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     click;
	int     percent;
	int     pitch;
	int     duration;
	int     led_mask;
	int     led_value;
	int     key;
	int     auto_repeat_mode;
} XKbdFeedbackControl;
.fi
.DE
.P
The \fIXStringFeedbackControl\f1 structure defines the attributes that can be
controlled for string feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     num_keysyms;
	KeySym  *syms_to_display;
} XStringFeedbackControl;
.fi
.DE
.P
The \fIXIntegerFeedbackControl\f1 structure defines the attributes that can
be controlled for integer feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     int_to_display;
} XIntegerFeedbackControl;
.fi
.DE
.P
The \fIXBellFeedbackControl\f1 structure defines the attributes that can be
controlled for bell feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     percent;
	int     pitch;
	int     duration;
} XBellFeedbackControl;
.fi
.DE
.P
The \fIXLedFeedbackControl\f1 structure defines the attributes that can be
controlled for LED feedbacks.
.P
.DS
.nf
typedef struct {
	XID     class;
	int     length;
	XID     id;
	int     led_mask;
	int     led_values;
} XLedFeedbackControl;
.fi
.DE
.SH DIAGNOSTICS
.TP 12
\fIBadDevice\f1
An invalid device was specified.  The specified device does not exist or has 
not been opened by this client via \fIXOpenInputDevice\f1.  This error may
also occur if some other client has caused the specified device to become
the X keyboard or X pointer device via the \fIXChangeKeyboardDevice\f1 or
\fIXChangePointerDevice\f1 requests.
.TP 12
\fIBadMatch\f1
This error may occur if an \f3XGetFeedbackControl\f1 request was made specifying
a device that has no feedbacks, or an \fIXChangeFeedbackControl\f1 request was
made with an \fIXFeedbackControl\f1 structure that contains an invalid feedback
type.  It may also occur if an invalid combination of mask bits is specified
(\fIDvKey\f1 but no \fIDvAutoRepeatMode\f1 for keyboard feedbacks), or if an 
invalid KeySym is specified for a string feedback.
.TP 12
\fIBadValue\f1
Some numeric value falls outside the range of values accepted by the 
\fIXChangeFeedbackControl\f1 request.
Unless a specific range is specified for an argument, the full range defined
by the argument's type is accepted.  Any argument defined as a set of
alternatives can generate this error.
.SH "SEE ALSO"
.br
\fI\*(xL\f1

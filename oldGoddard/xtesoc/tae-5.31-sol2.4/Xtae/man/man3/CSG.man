This section describes the widget's resources.
Unless otherwise noted, each resource is "CSG",
where 'C' means the resource may be set during
widget creation, 'S' means the resource may be
set via XtSetValues, and 'G' means
the resource may be obtained via XtGetValues.

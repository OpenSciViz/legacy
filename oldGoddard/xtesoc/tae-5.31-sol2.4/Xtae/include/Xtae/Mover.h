/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              Mover.h: Public header file for MoverGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 03-jun-93	Add extern C for C++...kbs
 * 08-jun-93	Add superclass header for application's convenience (so app.
 *         	includes Mover.h but doesn't need to include DDO.h)...kbs
 */

#ifndef _XtaeMoverG_h
#define _XtaeMoverG_h

#include <Xtae/Xtae.h>
#include <Xtae/DDO.h>	/* superclass */

        /* Declare class record constants */

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeMoverGadgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeMoverGadgetClassRec 	* XtaeMoverGadgetClass;
typedef struct _XtaeMoverGadgetRec      	* XtaeMoverGadget; 

	/* Subclass test macro */

#ifndef XtaeIsMover
#define XtaeIsMover(w)  	(XtIsSubclass (w, xtaeMoverGadgetClass))
#define XtaeIsMoverGadget(w)	(XtaeIsMover(w))
#endif

	/*  Creation entry points */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern Widget XtaeCreateMover _TAE_PROTO(( Widget d, String name,
                                         ArgList args, Cardinal n)) ;

        /* Class-specific method declarations, if any */
        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeMoverG_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              Xtae.h: Public header file for all TAE Plus widgets
 *
 ************************************************************************/

/*      Change Log:
 *
 * 30-apr-93    Initial...kbs
 * 13-may-93    XtaeEvertical and related additions...kbs
 * 03-jun-93    New name for prototype header, copied here by installtae...kbs
 * 07-jun-93    Moved typedef of TIVHandle to TIV.h; add stretchDirection...kbs
 * 08-jun-93    Moved some constants to TIVconst.h (new file)...kbs
 * 24-jun-93    Cookbook for adding/changing a resource...palm, kbs
 * 24-jun-93    Added XtaeCR_DDOAREA_INPUT...kbs
 * 25-jun-93    Remove XmNactivateCallback; minor cleanup...kbs
 * 28-jun-93    Rotator resources added...kbs
 * 30-jun-93    DiscretePictures resources added; XtaeRMaximumAngle del'd...kbs
 * 10-aug-93    X11R5 port: Motif 1.2 Xm.h header reorganized...kbs
 * 04-apr-94    Changed reference of XtaeStringDefs.h to XtaeStrDefs.h...dag
 * 15-apr-94    Concurrent:  Changed C++ comment to C...dgf
 *
 */


/*** N O T E:   UPDATE THESE: XtaeVERSION, XtaeREVISION, XtaeUPDATE_LEVEL ***/


/************************************************************************
 *
 *
 *		COOKBOOK for ADDING or CHANGING a RESOURCE  
 *
 *		    [cookbook last modified: 6/24/93]
 *
 *
 *   When you add or change a resource for any on the Xtae widgets,
 *   please review this list of files to (potentially) change.
 *
 *   After first delivery of a particular widget, changing a resource 
 *   (e.g, its representation type, its name, its default value, etc.), 
 *   should be strongly discouraged.
 *
 *   We have intentionally ignored the widget writer convention that the 
 *   field name of a resource such as XmNborderWidth is "border_width".
 *   Instead, the field name is always IDENTICAL to the resource name
 *   with the "XtaeN" prefix omitted. Thus the field name for the resource
 *   "XtaeNvalueDisplayFormat" is "valueDisplayFormat". (This resource 
 *   is a good example to trace in the cookbook below.)
 *
 *   [Unless stated otherwise, all .c files are located in $TAEXTAE and 
 *   all .h files are in $TAEXTAEINC = $TAEX/include/Xtae.]
 *
 *	1) Changes to <widget>.c source module:
 *
 *		- resources[] table
 *
 *		- Initialize method should copy resources passed by value 
 *		  (e.g., strings)
 *
 *		- SetValues method should check for new resource; this may
 *		  involve adding another changeMask (TIV_*_MASK) to TIV.h
 *		  (e.g., TIVVAL_DISP_FORMAT_MASK)
 *
 *		- Destroy method must free anything dynamically allocated 
 *		  (usually in Initialize or setValues), including subtle 
 *		  things like callback lists.
 *
 *	2) Add to the <widget>.h PUBLIC module the resource name and class, 
 *	   with the appropriate case distinction conventions exemplified below:
 *
 *		#define XtaeNvalueDisplayFormat         "valueDisplayFormat"
 *		#define XtaeCValueDisplayFormat         "ValueDisplayFormat"
 *
 *	3) Add to the <widget>P.h PRIVATE module the field in the widget data 
 *	   structure which holds the value of the new resource, e.g., in DDOP.h:
 *	
 *		typedef struct _XtaeDDOGadgetPart {
 *			String          valueDisplayFormat;
 *		} XtaeDDOGadgetPart;
 *
 *	4) If the resource has a default value other than NULL or 0, add a
 *	   constant to <widget>P.h [private] which is then referenced in 
 *	   <widget>.c in the resources[] table and whenever the default is 
 *	   needed, e.g., in DDOP.h:
 *
 *		#define DEFAULT_VALUE_DISPLAY_FORMAT    "%g"
 *
 *	   and in DDO.c in the resources[] table:
 *	
 *	      	{
 *   		XtaeNvalueDisplayFormat, XtaeCValueDisplayFormat, XtaeRString,
 *   		sizeof( String ),
 *   		offset ( ddo.valueDisplayFormat),
 *   		XtaeRImmediate, (XtPointer) DEFAULT_VALUE_DISPLAY_FORMAT
 * 		},
 *
 *
 *	5) If resource is associated w/a new representation type (data type), 
 *	   a converter must be written and registered. This function should be
 *	   named "xtaeCvtStringTo<your_new_type>". This involves changes to:
 *
 *		- <widget>.h to specify the constant for the new representation
 *		  type, e.g., in DDO.h:
 *
 *			#define XtaeRDouble                  "Double"
 *
 *		   Some new representation type are also in Xtae.h. 
 *	[TBD if this is acceptable]
 *
 *		- ResConvert.h to specify the function prototype
 *		  (e.g., xtaeCvtStringToDouble _TAE_PROTO ((Display *dpy...)); )
 *
 *		  NOTE: The return type of this conversion function MUST be
 *		        Boolean (since we use XtSetTypeConverter).
 *
 *		- ResConvert.c will contain the function definition; it should
 *		  generally use the ConversionDone macro (returns Boolean).
 *
 *		- ClassInitialize method of <widget>.c should call
 *		  XtSetTypeConverter with the name of the converter. 
 *		  Note that DDO.c registers xtaeCvtStringToDouble since all 
 *		  subclasses need this, whereas Mover.c registers 
 *		  xtaeCvtStringToMovementDirection because only XtaeMover needs
 *		  this.
 *
 *	6) $TAEPERLLIB/uilGen.pl will need new logic to set the resource in uil.
 *	   See "sub <widget>_Presentation".
 *
 *	7) $TAEX/uil/<widget>.uil may need the resource declared (if a uil
 *	   user might be inclined to use the resource), e.g. in DDO.uil:
 *
 *		XtaeNvalueDisplayFormat: argument('valueDisplayFormat',string);
 *
 *	8) $TAEX/man/man3/Xtae<widget>.3 must be updated.
 *
 *	9) Update various test programs which have test cases for setting
 *	   and getting DDO resources (e.g., Phil's taeuildisp, Ken's ddo-test.c).
 *	   Update corresponding test plans to indicate expected results.
 *	   Inform QA group of new resource; have them review man page and
 *	   test plan.
 *
 *	END COOKBOOK
 ************************************************************************/



	/* Make multiple includes of this file safe. */
#ifndef _Xtae_h
#define _Xtae_h

	/* Include superclass public header file.
 	 * The superclass is XmManager, but there is no Manager.h 
 	 * in Motif 1.1.* because Xm.h has the relevant info. 
	 * This header file organization changed in Motif 1.2.*.
 	 */

#include <Xm/Xm.h>
#if XmVERSION >= 1 && XmREVISION >= 2
/* Concurrent C++ 2.0 chokes on these files */
#ifndef masscomp
#include    <Xm/XmP.h>
#include    <Xm/ManagerP.h>
#include    <Xm/GadgetP.h>
#endif
#endif

#include <Xtae/taeproto.h>	/* function prototypes */

	/* Redefine resources in <X11/StringDefs.h> using our "Xtae" prefix */

#include <Xtae/XtaeStrDefs.h>

/************************************************************************
 *  RESERVED FOR INTERNAL USE
 ************************************************************************/

#define XtaeNdebug			"debug"
#define XtaeCDebug			"Debug"


#define XtaeVERSION        5
#define XtaeREVISION       3
#define XtaeUPDATE_LEVEL   0
#define XtaeVersion        (XtaeVERSION * 1000 + XtaeREVISION)
#define XtaeVERSION_STRING "@(#)Century Computing, Inc/TAE Plus Version 5.3"

extern int XtaeUseVersion;

/************************************************************************
 *  END - RESERVED FOR INTERNAL USE
 ************************************************************************/


/************************************************************************
 *  Global TAE Plus specific resources, not in XtaeStrDefs.h
 ************************************************************************/

/************************************************************************
 *  XtaeMover Resources and define values 
 *  (with Class and Representation Type forms)
 ************************************************************************/

#define XtaeNmovementDirection		"movementDirection"
#define XtaeCMovementDirection		"MovementDirection"
#define XtaeRMovementDirection		"MovementDirection"

/************************************************************************
 *  XtaeStretcher Resources and define values 
 *  (with Class and Representation Type forms)
 ************************************************************************/

#define XtaeNstretchDirection		"stretchDirection"
#define XtaeCStretchDirection		"StretchDirection"
#define XtaeRStretchDirection		"StretchDirection"

/************************************************************************
 *  XtaeRotator Resources and define values 
 *  (with Class and Representation Type forms)
 ************************************************************************/

#define XtaeNrotationDirection		"rotationDirection"
#define XtaeCRotationDirection		"RotationDirection"
#define XtaeRRotationDirection		"RotationDirection"

#define XtaeNpivotPoint			"pivotPoint"
#define XtaeCPivotPoint			"PivotPoint"
#define XtaeRPivotPoint			"PivotPoint"

#define XtaeNmaximumAngle		"maximumAngle"
#define XtaeCMaximumAngle		"MaximumAngle"
	/* Note: representation type is XtaeRDouble */

/************************************************************************
 *  XtaeDiscretePictures Resources and define values 
 *  (with Class and Representation Type forms)
 ************************************************************************/

#define XtaeNdiscreteThresholdTable	"discreteThresholdTable"
#define XtaeCDiscreteThresholdTable	"DiscreteThresholdTable"
#define XtaeRDiscreteThresholdTable	"DiscreteThresholdTable"

#define XtaeNdiscreteThresholdTableStr	"discreteThresholdTableStr"
#define XtaeCDiscreteThresholdTableStr	"DiscreteThresholdTableStr"
	/* Note: representation type is String */


/****************************************************************************
 *
 *  General Constants
 *
 ****************************************************************************/
/* 
 * TIVconst.h contains constants such as XtaeVERTICAL needed by:
 *	a) widget applications
 *	b) widget implementation
 *	c) UIL Mrm support code
 *	d) Wpt applications
 *	e) TIV (TAE-to-interViews interface) implementation
 */

#include <Xtae/TIVconst.h>

/****************************************************************************
 *
 *  Input Processing / Event Handling
 *
 ****************************************************************************/

	/* TBD - names not discussed */

/* when event handler is called */
#define XtaeREPORT_NONE         	0
#define XtaeREPORT_AT_END       	1
#define XtaeREPORT_CONTINUOUSLY 	2
#define XtaeREPORT_ON_SELECTION 	3

/* user interaction ; see also Callback reasons */
#define XtaeUI_NONE                     0
#define XtaeUI_SELECT                   1
#define XtaeUI_HORIZONTAL_MOVE          2
#define XtaeUI_VERTICAL_MOVE            3
#define XtaeUI_TWO_D_MOVE               4
#define XtaeUI_ROTATE                   5       /* TBD: need clockwise/counter*/
#define XtaeUI_HORIZONTAL_SCALE         6
#define XtaeUI_VERTICAL_SCALE           7
#define XtaeUI_TWO_D_SCALE              8
#define XtaeUI_ANIMATE                  9
#define XtaeUI_ANIMATE_FORWARD          10
#define XtaeUI_ANIMATE_BACKWARD         11

/* visual feedback when user interaction occurs */

        /* OR: XtaeFEEDBACK_NONE, FEEDBACK_RUBBERBAND, etc */

#define XtaeNO_FEEDBACK         	0
#define XtaeRUBBERBAND_FEEDBACK 	1
#define XtaeDYNAMIC_FEEDBACK    	2
#define XtaeTRANSFORMATION_FEEDBACK     3

/****************************************************************************
 *
 *  Callback reasons
 *
 ****************************************************************************/

#define XtaeCR_DDOAREA_INPUT            0       /* anywhere within DDOArea */
#define XtaeCR_SELECT                   1
#define XtaeCR_HORIZONTAL_MOVE          2
#define XtaeCR_VERTICAL_MOVE            3
#define XtaeCR_TWO_D_MOVE               4
#define XtaeCR_ROTATE                   5       /* TBD: need clockwise/counter*/
#define XtaeCR_HORIZONTAL_SCALE         6
#define XtaeCR_VERTICAL_SCALE           7
#define XtaeCR_TWO_D_SCALE              8
#define XtaeCR_ANIMATE                  9
#define XtaeCR_ANIMATE_FORWARD          10
#define XtaeCR_ANIMATE_BACKWARD         11
#define XtaeCR_VALUE_CHANGED            12       /* by application */

	/* New type definitions, if any */
	/* TBD */

/************************************************************************
 *
 *  Callback structures 
 *
 ************************************************************************/

/* NOTE: In v5.3, only XtaeDDOAreaCallbackStruct is supported */

#ifndef BLOCK_INPUT

#define XtaeNinputCallback        "inputCallback"

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
} XtaeDDOAreaCallbackStruct;

#endif	/* BLOCK_INPUT */

/***** OTHERS VERY TBD  including names *****/

typedef struct
{
    int     reason;
    XEvent  *event;
} XtaeAnyCallbackStruct;

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
    XtPointer 	objectName;
    double 	*values;		/* TBD */
} XtaeDDOSelectCallbackStruct;

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
    XtPointer 	objectName;
    double 	*values;		/* TBD */
} XtaeDDOMoveCallbackStruct;

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
    XtPointer 	objectName;
    double 	*values;		/* TBD */
} XtaeDDORotateCallbackStruct;

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
    XtPointer 	objectName;
    double 	*values;		/* TBD */
} XtaeDDOScaleCallbackStruct;

typedef struct
{
    int     	reason;
    XEvent  	*event;
    Window  	window;
    XtPointer 	objectName;
    double 	*values;		/* TBD */
} XtaeDDOAnimateCallbackStruct;

/*        TBD - we could add Inherit defines such as:
 *
 *	#define XtaeInheritResize  ((XtProc) _XtInherit)
 *
 */ 

/*        TBD - FOR FUTURE
 *
 *  We would add names of callbacks to:
 *  (a) correspond to Motif counterparts (Xt-only in XtaeStrDefs.h)
 *  (b) describe DDO-only callback capabilities
 *
 * 	#define XtaeNactivateCallback     	"activateCallback" 
 * 	#define XtaeNrotateCallback     	"rotateCallback" 
 *
 */

/************************************************************************
 *  Dynamic Object defines
 *
 *	NOTE:
 *
 *	Resources and defines for 
 *	Mover, Rotator, Stretcher, StripChart, and Discrete Pictures 
 *	are in their respective header files.
 *  
 ************************************************************************/

#endif /* _Xtae_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


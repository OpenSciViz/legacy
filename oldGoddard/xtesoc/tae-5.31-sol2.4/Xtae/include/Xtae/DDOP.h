/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              DDOP.h: Private header file for DDOGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 24-jun-93	Change BLOCK_INPUT_GADGET to BLOCK_INPUT a la DDOArea...kbs
 * 14-jul-93	Added undocumented resource userInput...kbs
 * 04-apr-94	Changed XtaeStringDefs.h to XtaeStrDefs.h (<= 14 chars)...dag
 * 17-apr-94	Concurrent port: XmGadgetClass is in Xm/GadgetP.h...dgf
 */

#ifndef _XtaeDDOGadgetP_h
#define _XtaeDDOGadgetP_h

#include <Xtae/Xtae.h>

/* Include public header for this class */
#include <Xtae/DDO.h>

/* Include private header for superclass Gadget, unfortunately in XmP.h */
#include <Xm/XmP.h>	/* for XmGadgetClass */

/* It looks like Motif pulled XmGadgetClass out of Xm/XmP.h and put
   it in Xm/GadgetP.h in Motif 1.2 */
#ifdef masscomp
#include <Xm/GadgetP.h>
#endif

/* Specific defines for this class and subclasses */

#define DEFAULT_CURRENT_VALUE	0.0
#define DEFAULT_UPDATE_DELTA	0.0
#define DEFAULT_MINIMUM_VALUE	0.0
#define DEFAULT_MAXIMUM_VALUE	100.0
#define DEFAULT_VALUE_DISPLAY_FORMAT    "%g"
#define XTAE_MAX_THRESHOLDS	1000

/* Define unique representation types not found in XtaeStrDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
/* TBD */

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the DDOGadget widget class record  */

typedef struct
{
   int  	TBD_for_now;

   XtPointer	extension;	/* X11R4 and up */

} XtaeDDOGadgetClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeDDOGadgetClassRec
{
	RectObjClassPart		rect_class;
	XmGadgetClassPart		gadget_class;	
	XtaeDDOGadgetClassPart		ddo_class;

} XtaeDDOGadgetClassRec;


/* External definition for class record 
 * (note UPPERCASE "DDO", lowercase "xtae")
 */

externalref XtaeDDOGadgetClassRec xtaeDDOGadgetClassRec;


/* New fields for the DDOGadget instance part */

typedef struct _XtaeDDOGadgetPart
{
	/* New resource fields */

	String 		pictureName;
	double		currentValue;	/* see currentValue_ptr */
	double		updateDelta;	/* see updateDelta_ptr */
	double		minimum;	/* see minimum_ptr */
	double		maximum;	/* see maximum_ptr */
	int		numThresholds;
	XtaeThresholdTable thresholdTable;
	String		thresholdTableStr;
	Boolean		valueDisplayEnable;
	String		valueDisplayFormat;
	
#ifndef BLOCK_INPUT
   	XtCallbackList 	activate_callback;
#endif

	/* private */
	/* New internal fields -- DO NOT REFERENCE FROM APPLICATIONS */

	TIVHandle	DynamicHandle;
                                /* opaque pointer to InterViews info */
                                /* needed for some TIV_ funcs */

	unsigned long	changeMask;
				/* for communicating SetValues results
				 * to subclasses
				 */

	double		* currentValue_ptr;
	double		* updateDelta_ptr;
	double		* minimum_ptr;
	double		* maximum_ptr;

	int 		userInput;	/* RESERVED FOR INTERNAL USE */
	int 		debug;		/* RESERVED FOR INTERNAL USE */

} XtaeDDOGadgetPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeDDOGadgetRec
{
	ObjectPart			object;
	RectObjPart			rectangle;  /* NOTE name */
	XmGadgetPart			gadget;     /* TBD */
	XtaeDDOGadgetPart		ddo;	    /* ddo_gadget */

} XtaeDDOGadgetRec;


#endif /* _XtaeDDOGadgetP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 *      MrmUtils.h -
 *
 *	function prototypes and constants for "enhanced" Mrm support,
 *	to provide functionality closer to Wpt, beyond what Motif provides.
 *
 *	USER NOTE:
 *
 *	Only the TAE Plus Mrm support functions declared in this file are
 *	intended for public use; no other such functions are supported.
 */

/*      Change Log:
 *
 * 07-aug-93	Initial...kbs,chc
 * 09-aug-93	Change type for Init functions to return line count, add
 *		MAXARG, change main_window_widget to ContainerWidget...chc
 */


#ifndef	_MrmUtils_h
#define	_MrmUtils_h

#include <Xtae/Xtae.h>		/* for _TAE_PROTO */

#define TU_MAXARG	   200
#define TU_MAX_TEXTLINES   512
#define TU_TEXT_BLOCKSIZE  256

#ifndef TU_FSPECSIZ
#ifdef  _POSIX_PATH_MAX
#define TU_FSPECSIZ 	_POSIX_PATH_MAX	
#else
#define TU_FSPECSIZ	255
#endif
#endif	/* TU_FSPECSIZ */

#ifndef TU_SUCCESS
#define TU_SUCCESS 	1 
#define TU_FAIL 	0
#define TU_EOS 		0
#endif	/* TU_SUCCESS */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

/* initializes a toplevel shell given an application shell */
extern void 	XtaeNewPanel		_TAE_PROTO ((
						Display *DisplayId,
						MrmHierarchy *S_MrmHierarchy,
						char    *PanelWidgetName,
						Widget  *TopLevelWidget,
						Widget  *ContainerWidget 
						));

/* sets list choices based on contents of filename */
extern int 	XtaeSelectionListInit	_TAE_PROTO ((
						Widget  widget,
						char    *filename
						));

/* sets widget's displayed text with contents of filename */
extern int 	XtaeTextDisplayInit	_TAE_PROTO ((
						Widget  widget,
						char    *filename
						));

/* declared for future use; functions are just stubs for now */
extern void 	XtaeFileSelectionSetColors _TAE_PROTO (( ));
extern void 	XtaeUnamageButtons 	   _TAE_PROTO (( ));

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

/* DO NOT PUT ANYTHING AFTER NEXT endif */
#endif	/* _MrmUtils_h */

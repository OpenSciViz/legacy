/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*************************************************************************
 *
 *  DiscretePictures.h: Public header file for DiscretePicturesGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 30-jun-93	Initial based on Mover.h...kbs
 */

#ifndef _XtaeDiscretePicturesG_h
#define _XtaeDiscretePicturesG_h

#include <Xtae/Xtae.h>
#include <Xtae/DDO.h>	/* superclass */

        /* Declare class record constants */

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeDiscretePicturesGadgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeDiscretePicturesGadgetClassRec 	* XtaeDiscretePicturesGadgetClass;
typedef struct _XtaeDiscretePicturesGadgetRec      	* XtaeDiscretePicturesGadget; 

	/* Subclass test macro */

#ifndef XtaeIsDiscretePictures
#define XtaeIsDiscretePictures(w)  	(XtIsSubclass (w, xtaeDiscretePicturesGadgetClass))
#define XtaeIsDiscretePicturesGadget(w)	(XtaeIsDiscretePictures(w))
#endif

	/*  Creation entry points */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern Widget XtaeCreateDiscretePictures _TAE_PROTO(( Widget d, String name,
                                         ArgList args, Cardinal n)) ;

        /* Class-specific method declarations, if any */
        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeDiscretePicturesG_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


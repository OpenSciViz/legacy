/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 07-jun-93	Add Boolean useDefaultColor to XtaeThreshold...kbs,palm
 */

#ifndef _Thresholds_h
#define _Thresholds_h

/************************************************************************
 *  New data types strictly for use with DDOs
 ************************************************************************/

	/* threshold structure for all subclasses except DiscretePictures */

typedef struct {
	double  value;
	Pixel   color;
	Boolean useDefaultColor; /* use dynamic as colored in idraw pix file */
} XtaeThreshold, *XtaeThresholdTable;

	/* TBD - maybe move to Discrete.h */
typedef struct {
	double  value;
	char	*pictureName;
} XtaeDiscreteThreshold, *XtaeDiscreteThresholdTable;

#define XtaeNdiscreteThresholdsTable	"discreteThresholdsTable"
#define XtaeNdiscreteThresholdsTableStr	"discreteThresholdsTableStr"
#define XtaeRDiscreteThresholdsTable	"DiscreteThresholdsTable"
	/* END TBD - maybe move to Discrete.h */

#endif	/* _Thresholds_h */

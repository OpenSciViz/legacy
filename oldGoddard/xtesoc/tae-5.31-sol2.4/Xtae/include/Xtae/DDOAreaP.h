/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              DDOAreaP.h: Private header file for DDOAreaWidgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 03-jun-93	Add extern C for C++...kbs
 * 08-jun-93	Add numPictures (private, internal resource)...kbs
 * 19-aug-93    X11R5 port: Motif 1.2 Xm.h header reorganized, so remove
 *         	XmP.h but add a few macros...kbs,rt
 * 04-apr-94	Changed XtaeStringDefs.h to XtaeStrDefs.h (<= 14 chars)...dag
 * 23-may-94	Concurrent Port: Added Xm/ManagerP.h for XmManagerClass...dgf
 */

#ifndef _XtaeDDOAreaP_h
#define _XtaeDDOAreaP_h


/* Include superclass private header file and our class public header file.
 * The superclass is Manager, but there is no ManagerP.h in Motif 1.1.*
 * because XmP.h has the relevant info.
 */

#ifdef masscomp
#include <Xm/ManagerP.h>
#endif

#if XmVERSION <= 1 && XmREVISION < 2
#include <Xm/XmP.h>

#else	/* Motif 1.2 */

#ifndef XtWidth
#define XtWidth(w)	((w)->core.width)
#endif
#ifndef XtHeight
#define XtHeight(w)	((w)->core.height)
#endif
#ifndef GMode
#define GMode(g)	((g)->request_mode)
#endif
#ifndef IsWidth
#define IsWidth(g)	(GMode (g) & CWWidth)
#endif
#ifndef IsHeight
#define IsHeight(g)	(GMode (g) & CWHeight)
#endif
#ifndef Max
#define Max(x, y)	(((x) > (y)) ? (x) : (y))
#endif

#endif	/* pre- Motif 1.2 */

#include <Xtae/Xtae.h>

/* Include public header for this class */
#include <Xtae/DDOArea.h>


/* Define unique representation types not found in XtaeStrDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
#define DEFAULT_AREA_BORDERWIDTH	0
#define DEFAULT_AREA_WIDTH		100
#define DEFAULT_AREA_HEIGHT		100

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the DDOArea widget class record  */

typedef struct
{

   int		TBD_for_now;

   XtPointer    extension;      /* X11R4 and up */

} XtaeDDOAreaClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeDDOAreaClassRec
{
	CoreClassPart			core_class;
	CompositeClassPart		composite_class;
	ConstraintClassPart		constraint_class;
	XmManagerClassPart		manager_class;
	XtaeDDOAreaClassPart		ddo_area_class;

} XtaeDDOAreaClassRec;


/* External definition for class record (note lowercase "ddo") */

externalref XtaeDDOAreaClassRec XtaeddoAreaClassRec;


/* New fields for the DDOArea instance part */

typedef struct
{
	/* New resource fields */

   char		*pictureFileName;	/* taeidraw file */
   char		*areaTitle;		/* title of the DDOArea */

	/* New internal fields -- DO NOT REFERENCE FROM APPLICATIONS */
	/* private */
   TIVHandle	AreaHandle;	/* opaque pointer to InterViews info */
				/* This handle is needed by all TIV_ funcs */
   TIVDimension IVwidth;	/* natural size from InterViews */
   TIVDimension IVheight;	/* natural size from InterViews */

   char         **pictureNames; /* dynam alloc'd by TIV_GetPictureNames */
   int		numPictures;	/* returned by TIV_GetPictureNames */

   int          debug;  /* RESERVED FOR INTERNAL USE */

#ifndef BLOCK_INPUT
   XtCallbackList input_callback;	/* TBD */
#endif

   XtPointer    extension;      /* X11R4 and up */

} XtaeDDOAreaPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeDDOAreaRec
{
	CorePart			core;
	CompositePart			composite;
	ConstraintPart			constraint;
	XmManagerPart			manager;
	XtaeDDOAreaPart			ddo_area;

} XtaeDDOAreaRec;

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern void _XtaeDDOAreaInput _TAE_PROTO(( XtaeDDOAreaWidget ddoa, 
					   XEvent * event));

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeDDOAreaP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

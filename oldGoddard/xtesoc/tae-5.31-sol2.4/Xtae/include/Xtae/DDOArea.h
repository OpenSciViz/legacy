/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              DDOArea.h: Public header file for DDOAreaWidgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 03-jun-93	Add extern C for C++...kbs
 */

#ifndef _XtaeDDOArea_h
#define _XtaeDDOArea_h

#include <Xtae/Xtae.h>
#include <Xtae/TIV.h>


/************************************************************************
 *  DDOArea Resources and define values
 ************************************************************************/

#define XtaeNpictureFileName	"pictureFileName"
#define XtaeCPictureFileName	"PictureFileName"

#define XtaeNareaTitle		"areaTitle"
#define XtaeCAreaTitle		"AreaTitle"


        /* Declare class record constants */

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeDDOAreaWidgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeDDOAreaClassRec 	* XtaeDDOAreaWidgetClass;
typedef struct _XtaeDDOAreaRec      	* XtaeDDOAreaWidget;

	/* Subclass test macro */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

#ifndef XtaeIsDDOArea
#define XtaeIsDDOArea(w)  (XtIsSubclass (w, xtaeDDOAreaWidgetClass))
#endif

	/*  Creation entry points */

extern Widget XtaeCreateDDOArea _TAE_PROTO (( Widget d, String name, 
					     ArgList args, Cardinal n)) ;


        /* Class-specific method declarations, if any */
        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeDDOArea_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


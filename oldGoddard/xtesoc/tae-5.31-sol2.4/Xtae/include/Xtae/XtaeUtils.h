/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 *      TaeUtils.h -
 *
 *      Constants/externs for Non-Xt utility functions used internally 
 *      by TAE Plus widgets.
 *
 *      CAUTION: THESE FUNCTIONS ARE NOT INTENDED FOR PUBLIC USE.
 *               NO API IS PROVIDED OR INTENDED.
 *		 DO NOT CALL THESE FUNCTIONS FROM NON-TAE WIDGET CODE.
 *
 *	TAE definitions: the idea is avoid refs
 *	to the TAE main tree for objects and 
 *	for includes. Note the TU_ prefix to avoid #undef.
 */

/*      Change Log:
 *
 * 30-apr-93	Initial...palm, kbs
 * 03-may-93	Added _xtae_string_changed for SetValues...kbs
 * 13-may-93	Added _xtae_lowercase; fixed prototypes...kbs
 * 04-jun-93	Add extern C for C++...kbs
 * 08-jun-93	Add _xtae_print_IVmsg (requires including TIV.h)...kbs
 * 18-jun-93	Add function arg to _xtae_print_IVmsg...kbs
 * 28-jun-93	Added _xtae_get_AreaHandle for Wpt access to DDOArea...kbs,krw
 * 28-jun-93	Remove input msg arg from _xtae_print_IVmsg...palm,kbs
 * 18-jul-93	Pass widget name to _xtae_print_IVmsg...kbs,palm
 */


#ifndef	_XtaeUtils_h
#define	_XtaeUtils_h

#include <Xtae/Xtae.h>		/* for _TAE_PROTO */
#include <Xtae/DDOArea.h>	/* for DDOArea access */
#include <Xtae/DDOAreaP.h>	/* for DDOArea private field access */
#include <Xtae/TIV.h>		/* for TIV data types and constants */

#define TU_SUCCESS 	1 
#define TU_FAIL 	0
#define TU_EOS 		0

#ifdef  _POSIX_PATH_MAX
#define TU_FSPECSIZ 	_POSIX_PATH_MAX	
#else
#define TU_FSPECSIZ	255
#endif

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

/* expands environment variables (if any) in a filespec */
extern int  	_xtae_f_subst 		_TAE_PROTO (( char *fspec,  
					char *subspec));

/* checks whether strings have changed; frees/copies as needed */
extern Boolean 	_xtae_string_changed 	_TAE_PROTO ((String *old, String *new));

/* converts "from" to lowercase, resulting in "to"; optionally strips
 * "Xtae" when returning the "to" string.
 */
extern Boolean 	_xtae_lowercase 	_TAE_PROTO (( String from, String to,
					int maxsize, int stripPrefix ));

/* display TIV error message */
void 		_xtae_print_IVmsg 	_TAE_PROTO (( char *widgetName,
					char *function,
					TIVStatus status));

/* return private DDOAreaHandle */
TIVHandle	 _xtae_get_AreaHandle 	_TAE_PROTO (( XtaeDDOAreaWidget ddoa ));

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif	/* _XtaeUtils_h */

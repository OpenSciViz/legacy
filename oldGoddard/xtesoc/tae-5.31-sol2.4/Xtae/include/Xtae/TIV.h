/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 *	TIV.h:	Xtae to InterViews Interface typedefs and constants
 *
 */

/*      Change Log:
 *
 * 30-apr-93	Initial...kbs,krw
 * 08-jun-93	Add TIVAnyStruct and TIVPIVOT_POINT_MASK...kbs,krw
 * 18-jun-93	Add XtAPPContext to Stripchart...kbs,krw
 * 30-jun-93	Removed TIV_PRELOAD_MASK and changed masks which followed; 
 *         	also deleted preload from TIVDiscretePixStruct...kbs
 * 09-jul-93	Remove SKIP_TIV branch...kbs
 * 14-jul-93	Added TIV_INPUTCALLBACK for TIV_RegisterInputCallback...krw,kbs
 * 15-jul-93	Added include of taeproto.h...krw,kbs
 * 11-aug-93    Removed updatePolicy from TIVStripchartStruct and
 *              removed TIVUPDATE_POLICY_MASK...krw
 * 09-sep-93	PR2283: Follow Xt convention: TIVPosition=Xt Position...kbs,palm
 */

#ifndef _TIV_h
#define _TIV_h

#include <Xtae/taeproto.h>

/* Data types */

typedef char *		TIVHandle;	/* opaque pointer to InterViews */
typedef unsigned int	TIVStatus; 	/* return codes from TIV functions */
typedef double		TIVReal;	/* avoid float vs. double name */
typedef unsigned int	TIVDimension;	/* width and height, bw */
typedef short		TIVPosition;	/* x, y */

typedef struct {			
	TIVPosition x;
	TIVPosition y;
	TIVDimension width;
	TIVDimension height;
	TIVDimension bw;	
} TIVGeom ;				/* returned by TIV_GetGeometry */

typedef void (*TIV_INPUTCALLBACK) _TAE_PROTO (( TIVHandle, XEvent *,
                                   TIVReal *, void * ));
				        /* Function pointer used with 
					 * TIV_RegisterInputCallback 
					 */
#include <Xtae/TIVconst.h>
#include <Xtae/Thresholds.h>

/* 
 * A TIVTransformStruct holds all resources/attributes which are unique
 * to a transform type for passing to TIV_UpdateTransform function.
 * Thresholds are included only because of the DiscretePictures asymmetry.
 */

typedef struct {
	int			numThresholds;
	XtaeThresholdTable	thresholdTable;
} TIVAnyStruct ;	/* Note: Don't use this for Discretes */

typedef struct {
	int			numThresholds;
	XtaeThresholdTable	thresholdTable;
	int			direction;	/* movementDirection */
} TIVMoverStruct ;

typedef struct {
	int			numThresholds;
	XtaeThresholdTable	thresholdTable;
	int			direction;	/* rotationDirection */
	TIVReal			maximumAngle;
	int			pivotPoint;	/* 9 constants */
} TIVRotatorStruct ;

typedef struct {
	int			numThresholds;
	XtaeThresholdTable	thresholdTable;
	int			direction;	/* stretchDirection */
} TIVStretcherStruct ;

typedef struct {
	int				numThresholds;
	XtaeDiscreteThresholdTable	thresholdTable; /* note type */
} TIVDiscretePixStruct ;

typedef struct {
	int			numThresholds;
	XtaeThresholdTable	thresholdTable;
	int			updateInterval;
	int			numSegments;
	XtAppContext		appContext;
} TIVStripchartStruct ;

typedef union _TIVTransformStruct {
	TIVAnyStruct 		any;
	TIVMoverStruct 		mover;
	TIVRotatorStruct 	rotator;
	TIVStretcherStruct 	stretcher;
	TIVDiscretePixStruct 	discrete;
	TIVStripchartStruct 	stripchart;
} TIVTransformStruct ;


/* TBD - possible structure for passing to TIV_Realize and 
 * TIV_UpdateDDOArea
 */

typedef struct {
	TIVPosition	x;	      /* location application requested */
	TIVPosition	y;
	TIVDimension	width;        /* size application requested */
	TIVDimension	height;
	TIVDimension	borderWidth;
	Pixel		fg;
	Pixel		bg;
	char 		*areaTitle;
} TIVDDOAreaStruct ;

/*-------------------------------------------------------------------------*/


/* Return codes and corresponding err msgs for TIV functions */

#define	TIVSUCCESS		0	/* NOTE : if ( !SUCCESS ) ... */
#define	TIVFAIL			1	/* NOTE */
#define	TIVNO_FILE		2	
#define	TIVBAD_FORMAT		3
#define	TIVNO_PICS		4	
#define	TIVBAD_HANDLE		5
#define	TIVBAD_PIC_NAME		6
#define	TIVDUP_PIC_NAME		7
#define	TIVDUP_TRANSFORM	8

	/**** TBD ----- add others *****/

/* A function will map codes to messages */
/*
 * status = TIV_StatusToMsg ( TIVStatus in_stat, char ** out_msg )
 *
 */

/* Bitmasks needed for TIV_UpdateDDOArea to signal what changed */

#define TIVNO_CHANGE_MASK        0L
#define TIVALL_CHANGE_MASK       (0xFFFF)

#define TIVPOSITION_MASK         (1L<<0)
#define TIVSIZE_MASK             (1L<<1)
#define TIVBORDER_MASK           (1L<<2)
#define TIVFG_MASK               (1L<<3)
#define TIVBG_MASK               (1L<<4)
#define TIVAREA_TITLE_MASK       (1L<<7)

/* Bitmasks needed for TIV_UpdateTransform to signal what changed */
/* Also use TIVNO_CHANGE_MASK, TIVALL_CHANGE_MASK above */

	/* for all DDO subclasses */
#define TIVCURRENT_VALUE_MASK    (1L<<0)
#define TIVUPDATE_DELTA_MASK     (1L<<1)
#define TIVMINIMUM_MASK          (1L<<2)
#define TIVMAXIMUM_MASK          (1L<<3)
#define TIVVAL_DISP_ENABLE_MASK  (1L<<4)
#define TIVVAL_DISP_FORMAT_MASK  (1L<<5)

	/* next is for either thresholdTable or discreteThresholdTable */
#define TIVTHRESHOLD_MASK       (1L<<6)
#define TIVTHRESHOLD_STR_MASK   (1L<<7)

	/* for Mover only */
#define TIVMOVEMENT_DIR_MASK     (1L<<8)

	/* for Rotator only */
#define TIVROTATION_DIR_MASK     (1L<<9)
#define TIVMAXIMUM_ANGLE_MASK    (1L<<10)
#define TIVPIVOT_POINT_MASK  	 (1L<<11)

	/* for Stretcher only */
#define TIVSTRETCH_DIR_MASK      (1L<<12)

	/* for DiscretePictures: no special mask; use *THRESHOLD*MASK */

	/* for Stripchart only */
#define TIVUPDATE_INTERVAL_MASK  (1L<<13)
#define TIVNUM_SEGMENTS_MASK     (1L<<14)


/* Transform types used by TIV_CreateTransform */
/* Direction is spec'd by TIVTransformStruct for Mover,Stretcher,Rotator */

#define TIVNULL_TRANSFORM			0
#define TIVMOVE_TRANSFORM			1
#define TIVSTRETCH_TRANSFORM			2
#define TIVROTATE_TRANSFORM			3
#define TIVDISCRETE_PIX_TRANSFORM		4
#define TIVSTRIPCHART_TRANSFORM			5

#endif /* _TIV_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


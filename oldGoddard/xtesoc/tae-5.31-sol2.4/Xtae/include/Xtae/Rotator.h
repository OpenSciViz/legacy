/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              Rotator.h: Public header file for RotatorGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 28-jun-93	Initial based on Mover.h...kbs
 */

#ifndef _XtaeRotatorG_h
#define _XtaeRotatorG_h

#include <Xtae/Xtae.h>
#include <Xtae/DDO.h>	/* superclass */

        /* Declare class record constants */

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeRotatorGadgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeRotatorGadgetClassRec 	* XtaeRotatorGadgetClass;
typedef struct _XtaeRotatorGadgetRec      	* XtaeRotatorGadget; 

	/* Subclass test macro */

#ifndef XtaeIsRotator
#define XtaeIsRotator(w)  	(XtIsSubclass (w, xtaeRotatorGadgetClass))
#define XtaeIsRotatorGadget(w)	(XtaeIsRotator(w))
#endif

	/*  Creation entry points */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern Widget XtaeCreateRotator _TAE_PROTO(( Widget d, String name,
                                         ArgList args, Cardinal n)) ;

        /* Class-specific method declarations, if any */
        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeRotatorG_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


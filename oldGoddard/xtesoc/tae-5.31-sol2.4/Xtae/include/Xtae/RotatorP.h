/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              RotatorP.h: Private header file for RotatorGadgetClass
 *
 ************************************************************************/

/*	Change Log:
 *
 * 28-jun-93	Initial based on MoverP.h and DDOP.h...kbs
 * 04-aug-93	PR2113: Change DEFAULT_MAXIMUM_ANGLE from 180 to 0...kbs,palm
 * 06-aug-93	PR2134: Change DEFAULT_PIVOT_POINT from XtaeCENTER_RIGHT...kbs
 * 04-apr-94    Changed XtaeStringDefs.h to XtaeStrDefs.h (<= 14 chars)...dag
 *
 */

#ifndef _XtaeRotatorGP_h
#define _XtaeRotatorGP_h

#include <Xtae/Xtae.h>

/* Include superclass private header file and our class public header file.
 * The superclass is XtaeDDOGadget.
 */

#include <Xtae/DDOP.h>

/* Include public header for this class */
#include <Xtae/Rotator.h>

/* Specific defines for this class and subclasses */

#define DEFAULT_ROTATION_DIRECTION 	XtaeCLOCKWISE
#define DEFAULT_PIVOT_POINT		XtaeCENTER_CENTER
#define DEFAULT_MAXIMUM_ANGLE		0.0


/* Define unique representation types not found in XtaeStrDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
/* TBD */

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the RotatorGadget widget class record  */

typedef struct
{
   int 		TBD;

   XtPointer	extension;	/* X11R4 and up */

} XtaeRotatorGadgetClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeRotatorGadgetClassRec
{
	RectObjClassPart		rect_class;
	XmGadgetClassPart		gadget_class;	
	XtaeDDOGadgetClassPart		ddo_class;
	XtaeRotatorGadgetClassPart	rotator_class;

} XtaeRotatorGadgetClassRec;


/* External definition for class record 
 * (note UPPERCASE "Rotator", lowercase "xtae")
 */

externalref XtaeRotatorGadgetClassRec xtaeRotatorGadgetClassRec;


/* New fields for the RotatorGadget instance part */

typedef struct _XtaeRotatorGadgetPart
{
	/* New resource fields */

	int		rotationDirection;
	int		pivotPoint;
	double		maximumAngle;

	/* New PRIVATE fields -- DO NOT REFERENCE FROM APPLICATIONS */

	TIVHandle	TransformHandle;
                                /*  
				 * Opaque pointer to InterViews info 
                                 * needed by most TIV_ functions.
				 */

	TIVTransformStruct Transform;
				/*
				 * Info needed by TIV_CreateTransform 
				 * and TIV_UpdateTransform.
				 *              
				 *              NOTE: 
				 * Transform contains a field called 
				 * thresholdTable which is a COPY of the
				 * pointer to the thresholdTable resource.
				 * It is NOT necessary to free this copy
				 * of the pointer since the original 
				 * thresholdTable is freed in DDO.c:Destroy.
				 */

        double          * maximumAngle_ptr;

	int		debug;  /* RESERVED FOR INTERNAL USE */

} XtaeRotatorGadgetPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeRotatorGadgetRec
{
	ObjectPart			object;
	RectObjPart			rectangle;  /* NOTE name */
	XmGadgetPart			gadget;	    /* TBD */
	XtaeDDOGadgetPart		ddo;        
	XtaeRotatorGadgetPart		rotator;      

} XtaeRotatorGadgetRec;


#endif /* _XtaeRotatorGP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

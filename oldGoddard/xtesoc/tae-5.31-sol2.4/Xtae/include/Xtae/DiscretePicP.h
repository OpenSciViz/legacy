/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/***************************************************************************
 *
 *  DiscretePicturesP.h: Private header file for DiscretePicturesGadgetClass
 *
 ***************************************************************************/

/*	Change Log:
 *
 * 30-jun-93	Initial based on MoverP.h and DDOP.h (thresholds)...kbs
 * 04-jul-93	Shorten class part from discrete_pictures to discrete...kbs
 * 04-apr-94	Changed XtaeStringDefs.h to XtaeStrDefs.h (<=14 chars)...dag
 *
 */

#ifndef _XtaeDiscretePicturesGP_h
#define _XtaeDiscretePicturesGP_h

#include <Xtae/Xtae.h>

/* Include superclass private header file and our class public header file.
 * The superclass is XtaeDDOGadget.
 */

#include <Xtae/DDOP.h>

/* Include public header for this class */
#include <Xtae/DiscretePic.h>

/* Specific defines for this class and subclasses */
/* TBD */

/* Define unique representation types not found in XtaeStrDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
/* TBD */

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the DiscretePicturesGadget widget class record  */

typedef struct
{
   int 		TBD;

   XtPointer	extension;	/* X11R4 and up */

} XtaeDiscretePicturesGadgetClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeDiscretePicturesGadgetClassRec
{
	RectObjClassPart			rect_class;
	XmGadgetClassPart			gadget_class;	
	XtaeDDOGadgetClassPart			ddo_class;
	XtaeDiscretePicturesGadgetClassPart	discrete_class;

} XtaeDiscretePicturesGadgetClassRec;


/* External definition for class record 
 * (note UPPERCASE "DiscretePictures", lowercase "xtae")
 */

externalref XtaeDiscretePicturesGadgetClassRec xtaeDiscretePicturesGadgetClassRec;


/* New fields for the DiscretePicturesGadget instance part */

typedef struct _XtaeDiscretePicturesGadgetPart
{
	/* New resource fields */

	/* Note type: XtaeDiscreteThresholdTable != XtaeThresholdTable */
        XtaeDiscreteThresholdTable 	discreteThresholdTable;
        String          		discreteThresholdTableStr;

	/* New PRIVATE fields -- DO NOT REFERENCE FROM APPLICATIONS */

	TIVHandle	TransformHandle;
                                /*  
				 * Opaque pointer to InterViews info 
                                 * needed by most TIV_ functions.
				 */

	TIVTransformStruct Transform;
				/*
				 * Info needed by TIV_CreateTransform 
				 * and TIV_UpdateTransform.
				 *              
				 *              NOTE: 
				 * Transform contains a field called 
				 * thresholdTable which is a COPY of the
				 * pointer to the thresholdTable resource.
				 * It is NOT necessary to free this copy
				 * of the pointer since the original 
				 * thresholdTable is freed in DDO.c:Destroy.
				 */

	int		debug;  /* RESERVED FOR INTERNAL USE */

} XtaeDiscretePicturesGadgetPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeDiscretePicturesGadgetRec
{
	ObjectPart			object;
	RectObjPart			rectangle;  /* NOTE name */
	XmGadgetPart			gadget;	    /* TBD */
	XtaeDDOGadgetPart		ddo;        
	XtaeDiscretePicturesGadgetPart	discrete;      

} XtaeDiscretePicturesGadgetRec;


#endif /* _XtaeDiscretePicturesGP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

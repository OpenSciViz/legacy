/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              Stretcher.h: Public header file for StretcherGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 25-jun-93	Initial based on Mover.h...kbs
 */

#ifndef _XtaeStretcherG_h
#define _XtaeStretcherG_h

#include <Xtae/Xtae.h>
#include <Xtae/DDO.h>	/* superclass */

        /* Declare class record constants */

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeStretcherGadgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeStretcherGadgetClassRec 	* XtaeStretcherGadgetClass;
typedef struct _XtaeStretcherGadgetRec      	* XtaeStretcherGadget; 

	/* Subclass test macro */

#ifndef XtaeIsStretcher
#define XtaeIsStretcher(w)  	(XtIsSubclass (w, xtaeStretcherGadgetClass))
#define XtaeIsStretcherGadget(w)	(XtaeIsStretcher(w))
#endif

	/*  Creation entry points */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern Widget XtaeCreateStretcher _TAE_PROTO(( Widget d, String name,
                                         ArgList args, Cardinal n)) ;

        /* Class-specific method declarations, if any */
        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeStretcherG_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


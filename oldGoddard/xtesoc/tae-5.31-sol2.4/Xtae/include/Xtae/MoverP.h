/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              MoverP.h: Private header file for MoverGadgetClass
 *
 ************************************************************************/

/*	Change Log:
 *
 * 04-may-93	Initial...kbs
 * 11-may-93	Changed datatype of XtaeNMovementDirection...kbs
 * 18-may-93	Again changed type of XtaeNMovementDirection and DEFAULT...kbs
 * 23-jun-93	Add transform to private part of widget structure...palm,kbs
 * 04-apr-94	Changed XtaeStringDefs.h to XtaeStrDefs.h (<= 14 chars)...dag
 *
 */

#ifndef _XtaeMoverGP_h
#define _XtaeMoverGP_h

#include <Xtae/Xtae.h>

/* Include superclass private header file and our class public header file.
 * The superclass is XtaeDDOGadget.
 */

#include <Xtae/DDOP.h>

/* Include public header for this class */
#include <Xtae/Mover.h>

/* Specific defines for this class and subclasses */

#define DEFAULT_MOVEMENT_DIRECTION 	XtaeVERTICAL


/* Define unique representation types not found in XtaeStrDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
/* TBD */

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the MoverGadget widget class record  */

typedef struct
{
   int 		TBD;

   XtPointer	extension;	/* X11R4 and up */

} XtaeMoverGadgetClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeMoverGadgetClassRec
{
	RectObjClassPart		rect_class;
	XmGadgetClassPart		gadget_class;	
	XtaeDDOGadgetClassPart		ddo_class;
	XtaeMoverGadgetClassPart	mover_class;

} XtaeMoverGadgetClassRec;


/* External definition for class record 
 * (note UPPERCASE "Mover", lowercase "xtae")
 */

externalref XtaeMoverGadgetClassRec xtaeMoverGadgetClassRec;


/* New fields for the MoverGadget instance part */

typedef struct _XtaeMoverGadgetPart
{
	/* New resource fields */

	int		movementDirection;

	/* New PRIVATE fields -- DO NOT REFERENCE FROM APPLICATIONS */

	TIVHandle	TransformHandle;
                                /*  
				 * Opaque pointer to InterViews info 
                                 * needed by most TIV_ functions.
				 */

	TIVTransformStruct Transform;
				/*
				 * Info needed by TIV_CreateTransform 
				 * and TIV_UpdateTransform.
				 *              
				 *              NOTE: 
				 * Transform contains a field called 
				 * thresholdTable which is a COPY of the
				 * pointer to the thresholdTable resource.
				 * It is NOT necessary to free this copy
				 * of the pointer since the original 
				 * thresholdTable is freed in DDO.c:Destroy.
				 */

	int		debug;  /* RESERVED FOR INTERNAL USE */

} XtaeMoverGadgetPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeMoverGadgetRec
{
	ObjectPart			object;
	RectObjPart			rectangle;  /* NOTE name */
	XmGadgetPart			gadget;	    /* TBD */
	XtaeDDOGadgetPart		ddo;        
	XtaeMoverGadgetPart		mover;      

} XtaeMoverGadgetRec;


#endif /* _XtaeMoverGP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

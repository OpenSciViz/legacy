/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              StretcherP.h: Private header file for StretcherGadgetClass
 *
 ************************************************************************/

/*	Change Log:
 *
 * 25-jun-93	Initial based on MoverP.h...kbs
 *
 */

#ifndef _XtaeStretcherGP_h
#define _XtaeStretcherGP_h

#include <Xtae/Xtae.h>

/* Include superclass private header file and our class public header file.
 * The superclass is XtaeDDOGadget.
 */

#include <Xtae/DDOP.h>

/* Include public header for this class */
#include <Xtae/Stretcher.h>

/* Specific defines for this class and subclasses */

#define DEFAULT_STRETCH_DIRECTION 	XtaeVERTICAL


/* Define unique representation types not found in XtaeStringDefs.h, if any */
/* TBD */

/* Inheritance constant(s) for class-specific methods, if any */
/* TBD */

/* Define new type for class methods, if any */
/* TBD */


/*  New fields for the StretcherGadget widget class record  */

typedef struct
{
   int 		TBD;

   XtPointer	extension;	/* X11R4 and up */

} XtaeStretcherGadgetClassPart;


/****************************************************************
 *
 * Full class record declaration
 *
 ***************************************************************/

typedef struct _XtaeStretcherGadgetClassRec
{
	RectObjClassPart		rect_class;
	XmGadgetClassPart		gadget_class;	
	XtaeDDOGadgetClassPart		ddo_class;
	XtaeStretcherGadgetClassPart	stretcher_class;

} XtaeStretcherGadgetClassRec;


/* External definition for class record 
 * (note UPPERCASE "Stretcher", lowercase "xtae")
 */

externalref XtaeStretcherGadgetClassRec xtaeStretcherGadgetClassRec;


/* New fields for the StretcherGadget instance part */

typedef struct _XtaeStretcherGadgetPart
{
	/* New resource fields */

	int		stretchDirection;

	/* New PRIVATE fields -- DO NOT REFERENCE FROM APPLICATIONS */

	TIVHandle	TransformHandle;
                                /*  
				 * Opaque pointer to InterViews info 
                                 * needed by most TIV_ functions.
				 */

	TIVTransformStruct Transform;
				/*
				 * Info needed by TIV_CreateTransform 
				 * and TIV_UpdateTransform.
				 *              
				 *              NOTE: 
				 * Transform contains a field called 
				 * thresholdTable which is a COPY of the
				 * pointer to the thresholdTable resource.
				 * It is NOT necessary to free this copy
				 * of the pointer since the original 
				 * thresholdTable is freed in DDO.c:Destroy.
				 */

	int		debug;  /* RESERVED FOR INTERNAL USE */

} XtaeStretcherGadgetPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ***************************************************************/

typedef struct _XtaeStretcherGadgetRec
{
	ObjectPart			object;
	RectObjPart			rectangle;  /* NOTE name */
	XmGadgetPart			gadget;	    /* TBD */
	XtaeDDOGadgetPart		ddo;        
	XtaeStretcherGadgetPart		stretcher;      

} XtaeStretcherGadgetRec;


#endif /* _XtaeStretcherGP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

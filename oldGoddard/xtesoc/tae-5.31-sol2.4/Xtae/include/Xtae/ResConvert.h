/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


#ifndef _Xtae_ResConvert_h
#define _Xtae_ResConvert_h

/*      Change Log:
 *
 * 30-apr-93    Initial...kbs
 * 11-may-93	XtaeOrientation and related additions...kbs
 * 18-may-93	Removed XtaeOrientation and related enums; replaced by
 *         	XtaeConversionTables such as MovementCvtTable; name change of
 *          	*CvtStringToOrientation to *CvtStringToMovementDirection...kbs
 * 03-jun-93	Add extern C for C++...kbs
 * 07-jun-93	Added xtaeCvtStringToStretchDirection and StretchCvtTable...kbs
 * 21-jun-93	Declare xtaeCvt* functions to return Boolean...palm,kbs
 * 28-jun-93	Added xtaeCvtStringToRotationDirection...kbs
 * 29-jun-93	Added xtaeCvtStringToPivotPoint...kbs
 * 30-jun-93	Added _XtaeCopyDiscreteThresholdTable prototype...kbs
 * 08-jul-93	Move *CvtTable to ResConvert.c...palm,kbs
 */

#include <Xtae/Xtae.h>
#include <Xtae/Thresholds.h>

#define MAX_ORIENT_SIZE 25


/************************************************************************
 *  Resource Conversion tables for DDOs
 ************************************************************************/

/* 
 * XtaeConversionTables are used to map strings to #defined integers
 * for various resources.
 *
 * Call _xtae_lowercase to convert a search key to lowercase AND
 * to cause any "Xtae" prefix to be skipped. ( XtaeBBB = BBB )
 *
 * Call searchConversionTable to find a key in a given table.
 *
 * NOTE: For user convenience, the following forms of all table-driven
 *       resources are acceptable IN ANY CASE COMBINATION.
 *
 *   		XtaeAAA_BBB, AAA_BBB, AAABBB
 *
 * The "AAABBB" (without underscore) form requires a specific entry in the 
 * table to match the "AAA_BBB" entry. For example, see RotationCvtTable.
 * (Tables are in ResConvert.c)
 */

typedef struct _XtaeConversionTable {
	char * searchKey;	/* MUST BE LOWERCASE in the table */
	int    value;		/* #defines from Xtae.h */
} XtaeConversionTable ;


/************************************************************************
 *  New conversion related functions for DDOs
 ************************************************************************/

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

extern Boolean	xtaeCvtStringToDouble _TAE_PROTO ((Display *display,
					XrmValuePtr args,
					Cardinal *num_args,
					XrmValuePtr fromVal,
					XrmValuePtr toVal,
					XtPointer *destructor_data));

extern Boolean	xtaeCvtStringToMovementDirection _TAE_PROTO ((Display *display,
					XrmValuePtr args,
					Cardinal *num_args,
					XrmValuePtr fromVal,
					XrmValuePtr toVal,
					XtPointer *destructor_data));

extern Boolean	xtaeCvtStringToStretchDirection _TAE_PROTO ((Display *display,
					XrmValuePtr args,
					Cardinal *num_args,
					XrmValuePtr fromVal,
					XrmValuePtr toVal,
					XtPointer *destructor_data));

extern Boolean	xtaeCvtStringToRotationDirection _TAE_PROTO ((Display *display,
					XrmValuePtr args,
					Cardinal *num_args,
					XrmValuePtr fromVal,
					XrmValuePtr toVal,
					XtPointer *destructor_data));

extern Boolean	xtaeCvtStringToPivotPoint _TAE_PROTO ((Display *display,
					XrmValuePtr args,
					Cardinal *num_args,
					XrmValuePtr fromVal,
					XrmValuePtr toVal,
					XtPointer *destructor_data));

/************************************************************************
 *  Ancillary functions to support conversions
 ************************************************************************/

/* All XtaeDDOGadget subclasses except XtaeDiscretePictures */

extern XtaeThresholdTable	_XtaeCopyThresholdTable 
					_TAE_PROTO ((XtaeThresholdTable table,
				        int count));

extern void 	_XtaeStrToThresholdTable _TAE_PROTO ((Widget w, 
					char *tableString, 
					int *countArg, 
					XtaeThresholdTable *tableArg));

extern void 	_XtaeFreeThresholdTable 
					_TAE_PROTO ((XtaeThresholdTable table,
				        int count));

/* XtaeDiscretePictures only */

extern XtaeDiscreteThresholdTable	_XtaeCopyDiscreteThresholdTable 
					_TAE_PROTO ((XtaeDiscreteThresholdTable
					table, int count));

extern void 	_XtaeStrToDiscreteThresholdTable _TAE_PROTO ((Widget widget, 
					char *tableString, int *countArg, 
					XtaeDiscreteThresholdTable *tableArg));

extern void 	_XtaeFreeDiscreteThresholdTable 
					_TAE_PROTO ((XtaeDiscreteThresholdTable 					table, int count));

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _Xtae_ResConvert_h */

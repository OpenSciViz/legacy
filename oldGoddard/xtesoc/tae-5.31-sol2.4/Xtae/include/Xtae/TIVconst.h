/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/****************************************************************************
 *
 *  TIVconst.h - General Constants
 *
 ****************************************************************************/


/*      Change Log:
 *
 * 08-jun-93    Initial...kbs,krw
 * 06-jul-93    Add ifndef _TIVconst_h and what string...kbs
 */

/*
 * TIVconst.h contains constants such as XtaeVERTICAL needed by:
 *      a) widget applications
 *      b) widget implementation
 *      c) UIL Mrm support code
 *      d) Wpt applications
 *      e) TIV (TAE-to-interViews interface) implementation
 */

#ifndef _TIVconst_h
#define _TIVconst_h

	/* Mover and Stretcher */
#define XtaeHORIZONTAL         	0
#define XtaeVERTICAL         	1

	/* Rotator */
#define XtaeCLOCKWISE		0
#define XtaeCOUNTER_CLOCKWISE	1

	/* Rotator pivotPoint */
#define XtaeTOP_LEFT        	1
#define XtaeTOP_CENTER      	2
#define XtaeTOP_RIGHT        	3
#define XtaeCENTER_LEFT      	4
#define XtaeCENTER_CENTER    	5
#define XtaeCENTER		(XtaeCENTER_CENTER)
#define XtaeCENTER_RIGHT     	6
#define XtaeBOTTOM_LEFT     	7
#define XtaeBOTTOM_CENTER   	8
#define XtaeBOTTOM_RIGHT   	9

#endif	/*  _TIVconst_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */

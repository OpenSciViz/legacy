/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 *	TIVfunc.h:	TAE to InterViews interface function prototypes
 */

/*	Change Log:
 *
 * 30-apr-93	Initial...kbs,krw
 * 07-may-93	TIV_LoadPictureFile changed to TIV_CreateDDOArea...kbs,krw
 * 14-may-93	Added TIV_HandleEvent...kbs,krw
 * 03-jun-93	New name for prototype header...kbs
 * 04-jun-93	Add extern C for C++...kbs
 * 08-jun-93	Many TIV interface changes: Added borderWidth to TIV_Resize,
 *         	deleted transformType from TIV_UpdateTransform,
 *         	added new functions: TIV_FlushDynamic, TIV_FlushArea,
 *         	TIV_MapDynamic, TIV_UnmapDynamic; add TIV_PreRealize with
 *         	arg list formerly in TIV_Realize...kbs,krw
 * 09-jun-93	Move parentWindow from TIV_PreRealize to TIV_Realize...kbs,krw
 * 14-jun-93	TIV_ResetDynamic not needed due to TIV_FlushDynamic...kbs,krw
 * 02-jul-93    Some syntax errors with some parameter definitions...krw
 * 02-jul-93    Removed transformType from TIV_UpdateTransform. Not needed
 *              because the transformHandle knows what type it is...krw
 * 14-jul-93    Added prototpyes for TIV_EnableInput, TIV_DisableInput, and
 *         	TIV_RegisterInputCallback; removed geometry arg from 
 *              TIV_DoTransform (use TIV_GetGeometry instead)...krw,kbs
 */



#ifndef _TIVfuncs_h
#define _TIVfuncs_h

#include <Xtae/TIV.h> 
#include <Xtae/taeproto.h>   // really from $TAEINC/taeproto.h, this is a copy

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

/*-------------------------------------------------------------------------*/
                   /* BEGIN DDOArea IV interface */

extern TIVStatus	TIV_StatusToMsg _TAE_PROTO (( 
					TIVStatus IVstatus, 
					char **msg /* returned */
					));

		  /* TIV_StatusToMsg can be used at all levels */


extern TIVStatus	TIV_Initialize _TAE_PROTO (( /* no args */ ));

extern TIVStatus	TIV_CreateDDOArea _TAE_PROTO (( 
					Display *display,
					char *filename,	        /* expanded */
					TIVHandle    *AreaHandle, /* ret */
					TIVDimension *IVwidth,  /* ret */
					TIVDimension *IVheight  /* ret */
					));

extern TIVStatus	TIV_GetPictureNames _TAE_PROTO (( 
					TIVHandle AreaHandle,
					int * numPictures,	/* ret */
					char *** pictureNames 	/* ret */
					));

extern TIVStatus	TIV_PreRealize _TAE_PROTO (( 
					TIVHandle AreaHandle,
					TIVPosition x,	      /* app req */
					TIVPosition y,
					TIVDimension width,   /* app req */
					TIVDimension height,
					TIVDimension borderWidth,
					Pixel fg,
					Pixel bg,
					char   *areaTitle
					));

extern TIVStatus	TIV_Realize _TAE_PROTO (( 
					TIVHandle AreaHandle,
					Window	parentWindow, /* panel win */
					Window *newWindow    /* ret */
					));

extern TIVStatus	TIV_Resize _TAE_PROTO (( 
					TIVHandle AreaHandle,
					TIVPosition x,     
					TIVPosition y,
					TIVDimension width, 
					TIVDimension height,
					TIVDimension borderWidth
					));

extern TIVStatus	TIV_Redisplay _TAE_PROTO ((
					TIVHandle AreaHandle,
					XEvent *event, 
					Region region
                                        ));

	/* 
 	 * TIV_FlushArea - tells DDOArea to visually update the display since
 	 * all dynamics and transform changes have been defined.
 	 */
extern TIVStatus	TIV_FlushArea   _TAE_PROTO (( 
					TIVHandle AreaHandle
					));

extern TIVStatus	TIV_UpdateDDOArea _TAE_PROTO (( 
					TIVHandle AreaHandle,
                                	unsigned long changeMask,
                                	TIVPosition x,     
					TIVPosition y,
                                	TIVDimension width, 
					TIVDimension height,
                                	TIVDimension borderWidth,
                                	Pixel fg,
					Pixel bg,
                                	char *areaTitle     
                                	));

	/* TIV_Destroy is also used by DDOGadget and subclasses */
	/* Pass AreaHandle, DynamicHandle, or TransformHandle */

extern TIVStatus	TIV_Destroy _TAE_PROTO (( 
					TIVHandle AreaHandle 
					));


	/* TIV_HandleEvent is intended to receive the following
	 * events which InterViews does an XSelectInput on:
	 *
	 * KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask|
	 * PointerMotionMask | PointerMotionHintMask |
	 * EnterWindowMask | LeaveWindowMask |
	 * ExposureMask |
	 * StructureNotifyMask | FocusChangeMask | OwnerGrabButtonMask
	 */

extern TIVStatus	TIV_HandleEvent _TAE_PROTO ((
					TIVHandle AreaHandle, 
					XEvent *event 
					));


/*-------------------------------------------------------------------------*/
                   /* BEGIN DDOGadget IV interface */

extern TIVStatus	TIV_CreateDynamic _TAE_PROTO ((
                        		TIVHandle AreaHandle, /*instance par*/
					char *pictureName,
                        		TIVHandle *DynamicHandle /* ret */
                        		));

	/* 
 	 * TIV_FlushDynamic - Apply all transforms to dynamic and draw it
 	 * (into off-screen pixmap). Called after one or more calls to
	 * TIV_UpdateTransform. See also TIV_FlushArea.
 	 */
extern TIVStatus	TIV_FlushDynamic _TAE_PROTO (( 
					TIVHandle DynamicHandle
					));
	/* 
	 * Note: 
	 * TIV_MapDynamic and TIV_UnmapDynamic are not presently used by 
	 * widget code.
	 */

	/* 
 	 * TIV_MapDynamic - Enable the dynamic to be displayed.
 	 * This is the default, but the function is called after a
 	 * prior TIV_UnmapDynamic.
 	 */
extern TIVStatus	TIV_MapDynamic  _TAE_PROTO (( 
					TIVHandle DynamicHandle
					));

	/* 
 	 * TIV_UnmapDynamic - Remove the dynamic from the display.
 	 * The dynamic is "removed" from the DDO, but any auxiliary
 	 * pictures (i.e., minLabel, maxLabel) will still be displayed.
 	 */
extern TIVStatus	TIV_UnmapDynamic _TAE_PROTO (( 
					TIVHandle DynamicHandle
					));
extern TIVStatus	TIV_GetGeometry _TAE_PROTO (( 
					TIVHandle DynamicHandle,
					TIVGeom   *geometry	/* ret */
					));

extern TIVStatus	TIV_EnableInput _TAE_PROTO (( 
					TIVHandle DynamicHandle
					));

extern TIVStatus	TIV_DisableInput _TAE_PROTO (( 
					TIVHandle DynamicHandle
					));

extern TIVStatus	TIV_RegisterInputCallback _TAE_PROTO (( 
                                        TIVHandle DynamicHandle,
                                        unsigned long callbackTypeMask,
                                        TIV_INPUTCALLBACK callbackFunc,
                                        void *client_data
                                        ));
	/*
         * TIV_RegisterInputCallback - Adds an input callback.
	 * DynamicHandle is the handle to the dynamic to register a
	 * callback for.
         *
         * callbackTypeMask is an OR of (from X.h)
         *          ButtonPressMask, ButtonReleaseMask, PointerMotionMask
         *
         * callbackFunc is the callback function pointer. If this
         *          parameter is null, then the existing callback of
         *          that type is removed (if any).
         *
         * client_data  - for widgets, this should be the widgetId, for
         *             Wpt, this should be the DDOA object.
         *
         * Returns:  TIVSUCCESS or  TIVBAD_HANDLE
         *
         * A Callback function will look like:
         *
         *	TIV_INPUTCALLBACK myCallback (TIVHandle dynamicHandle,
         *       			XEvent *xevent,
         *       			TIVReal *dynamicValues,
         *       			void *client_data);
         *
         * The dynamicValues array will be freed after you return from this
         * function, so you must copy the data to save it.
         *
	 */

		   /* Dynamic also uses TIV_Destroy */

/*-------------------------------------------------------------------------*/
                   /* BEGIN MoverGadget (& other subclasses) IV interface */

extern TIVStatus	TIV_CreateTransform _TAE_PROTO ((
                        		TIVHandle DynamicHandle, /*class parent*/
                                	TIVReal   currentValue, /*TBD if need*/
                                	TIVReal   updateDelta,
                                	TIVReal   minimum,
                                	TIVReal   maximum,
                                	int       valueDisplayEnable,
                                	char *    valueDisplayFormat,
                                	int       transformType,
                                	TIVTransformStruct *transform,
                        		TIVHandle *TransformHandle /*ret*/
                        		));

extern TIVStatus	TIV_UpdateTransform _TAE_PROTO (( 
					TIVHandle TransformHandle,
					unsigned long changeMask, 
					TIVReal   currentValue, /* TBD if need*/
    					TIVReal	  updateDelta,
    					TIVReal	  minimum,
    					TIVReal	  maximum,
                                	int       valueDisplayEnable,
    					char *	  valueDisplayFormat,
					TIVTransformStruct *transform
					));

extern TIVStatus	TIV_DoTransform _TAE_PROTO (( 
					TIVHandle TransformHandle,
					TIVReal   currentValue
					));

/*
 *  The logic for handling MULTIPLE transforms for a dynamic 
 *  (not presently supported by the Xtae widget set) is:
 *
 *
 *        Loop (for each Dynamic)
 *		TIV_UpdateTransform  or  TIV_DoTransform
 *		TIV_FlushDynamic
 *        	TIV_GetGeometry (widget code only)
 *        End-loop
 *
 *        TIV_FlushArea (to force DDOArea to update on the display)
 *
 */

		   /* also uses TIV_Destroy */
/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _TIVfunc_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


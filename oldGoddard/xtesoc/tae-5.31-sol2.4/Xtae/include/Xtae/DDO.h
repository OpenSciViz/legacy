/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/************************************************************************
 *
 *              DDO.h: Public header file for DDOGadgetClass
 *
 ************************************************************************/

/*      Change Log:
 *
 * 04-may-93	Initial...kbs
 * 03-jun-93	Add extern C for C++...kbs
 * 24-jun-93	Change BLOCK_INPUT_GADGET to BLOCK_INPUT a la DDOArea...kbs
 * 08-jul-93	Remove include of XmP.h from public header...kbs,palm
 */

#ifndef _XtaeDDOGadget_h
#define _XtaeDDOGadget_h

#include <Xtae/Xtae.h>
#include <Xtae/TIV.h>

#include <Xm/Xm.h>	/* for XmGadgetClass */


/************************************************************************
 *  New data types and converters strictly for use with DDOs
 ************************************************************************/

#include <Xtae/Thresholds.h>
#include <Xtae/ResConvert.h>

/************************************************************************
 *  DDOGadget Resources and define values
 ************************************************************************/

#define XtaeNpictureName        	"pictureName"
#define XtaeNcurrentValue		"currentValue"	
#define XtaeNupdateDelta		"updateDelta"
#define XtaeNnumThresholds		"numThresholds"
#define XtaeNthresholdTable		"thresholdTable"
#define XtaeNthresholdTableStr		"thresholdTableStr"
#define XtaeNminimum			"minimum"
#define XtaeNmaximum			"maximum"
#define XtaeNvalueDisplayEnable		"valueDisplayEnable"
#define XtaeNvalueDisplayFormat		"valueDisplayFormat"

#ifndef BLOCK_INPUT
#define XtaeNactivateCallback		"activateCallback"
#endif

/************************************************************************
 *  DDOGadget Resource Classes
 ************************************************************************/

#define XtaeCPictureName        	"PictureName"
#define XtaeCCurrentValue		"CurrentValue"	
#define XtaeCUpdateDelta		"UpdateDelta"
#define XtaeCNumThresholds		"NumThresholds"
#define XtaeCThresholdTable		"ThresholdTable"
#define XtaeCThresholdTableStr		"ThresholdTableStr"
#define XtaeCMinimum			"Minimum"
#define XtaeCMaximum			"Maximum"
#define XtaeCValueDisplayEnable		"ValueDisplayEnable"
#define XtaeCValueDisplayFormat		"ValueDisplayFormat"

/************************************************************************
 *  DDOGadget (New) Resource Representation Types
 ************************************************************************/

#define XtaeRDouble        		"Double"
#define XtaeRThresholdTable		"ThresholdTable"


/************************************************************************
 *  Declare class record constants
 ************************************************************************/

        /* External reference to the class record pointer */

externalref WidgetClass 		xtaeDDOGadgetClass;

        /* Reference to class and instance datatypes */

typedef struct _XtaeDDOGadgetClassRec 	* XtaeDDOGadgetClass;
typedef struct _XtaeDDOGadgetRec      	* XtaeDDOGadget;

	/* Subclass test macro */
	/* TBD - note second macro XtaeIsDDOGadget added */

#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif

#ifndef XtaeIsDDO
#define XtaeIsDDO(w)		(XtIsSubclass (w, xtaeDDOGadgetClass))
#define XtaeIsDDOGadget(w)	(XtaeIsDDO(w))
#endif

	/*  Creation entry points */

extern Widget XtaeCreateDDO _TAE_PROTO(( Widget d, String name, 
					 ArgList args, Cardinal n)) ;

/************************************************************************
 *  Class-specific method declarations, if any
 ************************************************************************/

        /* TBD */

#ifdef __cplusplus
}                       // end the extern "C" block
#endif

#endif /* _XtaeDDOGadget_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */


/*
 * Change Log:
 *      xx-xxx-91 Initial...Margi Klemp and Eric Hillis
 *      28-jun-93 Add XtaeCreateXterm and function prototype support...kbs
 */

#include <Xtae/Xtae.h>	/* for _TAE_PROTO, etc. */

#ifndef INSIDEXTERM
typedef struct _XtermWidgetRec *XtermWidget;
extern WidgetClass xtermWidgetClass;
#endif /* INSIDEXTERM */


#ifdef __cplusplus
extern "C" {            // Tell C++ not to overload these functions.
#endif
       /*  UIL Creation entry points */

extern Widget XtaeCreateXterm _TAE_PROTO(( Widget d, String name,
                                         ArgList args, Cardinal n)) ;

#ifdef __cplusplus
}                       // end the extern
#endif

/************************************************************************
 *  Xterm (color logger) Resources 
 ************************************************************************/

#define XmNalwaysHighlight	"alwaysHighlight"
#define	XmNboldFont		"boldFont"
#define	XmNc132			"c132"
#define XmNcharClass		"charClass"
#define	XmNcurses		"curses"
#define	XmNcursorColor		"cursorColor"
#define XmNcolor0		"color0"
#define XmNcolor1		"color1"
#define XmNcolor2		"color2"
#define XmNcolor3		"color3"
#define XmNcolor4		"color4"
#define XmNcolor5		"color5"
#define XmNcolor6		"color6"
#define XmNcolor7		"color7"
#define XmNcutNewline		"cutNewline"
#define XmNcutToBeginningOfLine	"cutToBeginningOfLine"
#define XmNeightBitInput	"eightBitInput"
#define XmNeightBitOutput	"eightBitOutput"
#define XmNgeometry		"geometry"
#define	XmNinternalBorder	"internalBorder"
#define	XmNjumpScroll		"jumpScroll"
#define	XmNbltScroll		"bltScroll"
#define	XmNlogFile		"logFile"
#define	XmNlogging		"logging"
#define	XmNlogInhibit		"logInhibit"
#define	XmNloginShell		"loginShell"
#define	XmNmarginBell		"marginBell"
#define	XmNpointerColor		"pointerColor"
#define XmNpointerColorBackground "pointerColorBackground"
#define	XmNpointerShape		"pointerShape"
#define XmNmultiClickTime	"multiClickTime"
#define	XmNmultiScroll		"multiScroll"
#define	XmNnMarginBell		"nMarginBell"
#define	XmNreverseWrap		"reverseWrap"
#define	XmNautoWrap		"autoWrap"
#define	XmNsaveLines		"saveLines"
#define	XmNscrollBar		"scrollBar"
#define XmNscrollTtyOutput	"scrollTtyOutput"
#define	XmNscrollKey		"scrollKey"
#define XmNscrollLines		"scrollLines"
#define XmNscrollPos    	"scrollPos"
#define XmNselectedLine		"selectedLine"
#define	XmNsignalInhibit	"signalInhibit"
#define XmNtiteInhibit		"titeInhibit"
#define	XmNvisualBell		"visualBell"
#define XmNallowSendEvents	"allowSendEvents"

#define XmCAlwaysHighlight	"AlwaysHighlight"
#define	XmCC132			"C132"
#define XmCCharClass		"CharClass"
#define	XmCCurses		"Curses"
#define XmCCutNewline		"CutNewline"
#define XmCCutToBeginningOfLine	"CutToBeginningOfLine"
#define XmCEightBitInput	"EightBitInput"
#define XmCEightBitOutput	"EightBitOutput"
#define XmCGeometry		"Geometry"
#define	XmCJumpScroll		"JumpScroll"
#define	XmCBltScroll		"BltScroll"
#define	XmCLogfile		"Logfile"
#define	XmCLogging		"Logging"
#define	XmCLogInhibit		"LogInhibit"
#define	XmCLoginShell		"LoginShell"
#define	XmCMarginBell		"MarginBell"
#define XmCMultiClickTime	"MultiClickTime"
#define	XmCMultiScroll		"MultiScroll"
#define	XmCColumn		"Column"
#define	XmCReverseWrap		"ReverseWrap"
#define XmCAutoWrap		"AutoWrap"
#define XmCSaveLines		"SaveLines"
#define	XmCScrollBar		"ScrollBar"
#define XmCScrollLines		"ScrollLines"
#define XmCScrollPos     	"ScrollPos"
#define	XmCScrollCond		"ScrollCond"
#define XmCSelectedLine		"SelectedLine"
#define	XmCSignalInhibit	"SignalInhibit"
#define XmCTiteInhibit		"TiteInhibit"
#define	XmCVisualBell		"VisualBell"
#define XmCAllowSendEvents	"AllowSendEvents"

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/

/* reslister - a resource file lister.
 * 	displays all panels with their items and presentation types
 *
 *		USAGE:
 *
 *	UNIX:	reslister [-a [-p panelname] [-d display [-f]] ] resfile
 *	VMS:	mcr []reslister.exe [-a [-p panelname] [-d display [-f]] ] resfile
 *
 *	-a           (dump ALL resfile info)
 *      -p panelname (dump ONLY the panel "panelname")
 *	-d display   (X display to check colors against)
 *	-f           (check fonts against display)
 *	
 *	Notes:
 *	Default (no options) is to produce SHORT resfile listing.
 *	Use of -d implies -a.
 *	Use of -f requires -d.
 *	-f slows this program down considerably.
 *	
 *	Keyword 'WARNING' signifies POSSIBLE resfile
 *	portability problems, such as with colors or fonts.
 *	
 *
 * Change Log:
 *	06-20-90	Initial creation - krw, Century Computing, Inc.
 *	10-29-90	print all strings in StringVec...kbs, Century Computing
 *	04-15-91	add -d display & -f options to check color,fonts...kbs
 *	04-15-91	also need new Makefile to match new v5.1 tree...kbs
 *	04-16-91	add DDO, dyntext, icon, pulldown special cases...kbs
 *	02-12-92	add -p panelname option...kbs
 *	06-15-92	add item/panel Title to output..kbs,cew,krw
 *	08-27-93	PR2221: remove Co_, Vm_ function declarations...kbs
 */

/*		ASSUMPTION:
 *
 *	This utility must be updated when new font or color VmResources
 *	are added with names OTHER THAN "font", "fg", and "bg".  See
 *	code for (pulldown) "menu" example.  Similar updates may be necessary
 *	when new DDO's are added.
 */

#include 	"taeconf.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 

#ifndef VMS
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#else
#include <stdio.h>
#include <X.h>
#include <Xlib.h>
#include <Xutil.h>
#endif

  
/* X11 global variables */
Display *dpy ;
Colormap cmap ;
XColor exact_color, screen_color;  /* return values */
XFontStruct *fontinfo;
TEXT 	display[STRINGSIZ+1];   /* X display connection */

TEXT 	*currentPanel ;		/* needed by printAttributes */
TEXT 	*currentPresType ;	/* needed by printAttributes */
static int numWarnings = 0;
BOOL fullList = FALSE;
BOOL checkDisplay = FALSE;	/* true iff -d display given */
BOOL checkFonts = FALSE;	/* true iff -f & -d display given */
TEXT 	requestedPanel[STRINGSIZ+1];	/* for -panel option */
BOOL onePanel = FALSE;
BOOL onePanelFound = FALSE ;



/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
 
/*	Data Types */
TEXT *dataTypes[] =
	{
	"",			/* skip first since integer = 1 */
	"INTEGER",
	"REAL",
	"STRING",
	"NAME"};
 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
    TEXT 	filename[STRINGSIZ+1];
    TEXT	**names; 
    Id		*ids;
    COUNT	i, len;
    CODE 	code, printItems();
    int         tlines, tcols, ttype;

    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower(FALSE);
    vmCollection = Co_New (P_ABORT);

/* get name of resource file to list. Can either be on the command line
 * or requested
 */
    if (argc == 1)
	{
	printf ("Please enter a resource file name : ");
	scanf("%s", filename);
	}

    if ( argc > 2 && argv[1][0] != '-' )
    	{
	fprintf (stderr, "%s: First argument must be an option.\n", argv[0]);
	usage ( argv );
    	}
    else if ( argc == 2 && argv[1][0] != '-' )
	{
	s_copy(argv[1], filename);
	}

    for ( i = 1 ; i < argc ; i++ )
    {
	char *next_arg = argv[i] ;
	int len ;

#ifdef DEBUG
printf ("DEBUG: next_arg (%d) = %s \n", i, next_arg );
#endif

	if ( next_arg[0] != '-' )
	    {
	    s_copy(argv[i], filename);
	    continue;
	    }

	len = strlen ( next_arg );

	switch ( next_arg[1] )
	{
	case 'd':	/* -display $DISPLAY */
		if ((next_arg[2] && 
		     strncmp ( "-display", next_arg, len) != 0) 
		     		|| (++i >= argc))
					usage ( argv ) ;
		if ( argv[i][0] == '-' )
		{
			fprintf (stderr, 
				"\n-display option requires host:display.\n" );
			usage ( argv );
		}

            	/* want to check resfile colors for given display */
		s_copy(argv[i], display);
            	checkDisplay = TRUE;
		fullList = TRUE;	/* -d implies -a */
            	if ( !(dpy = XOpenDisplay ( display ) ))
                	{
                	fprintf(stderr, "%s:  unable to open display \"%s\"\n",
                         	argv[0], argv[i] );
			usage (argv);
                	}
		else
			printf ("\nChecking resources against display %s\n", 
				display );

		cmap = DefaultColormap( dpy, DefaultScreen(dpy)) ;
		if ( !cmap )
                	{
                	fprintf(stderr, 
				"%s: no default colormap for display \"%s\"\n",
                         	argv[0], argv[i] );
			exit (1);
                	}
		continue ;

	case 'a':	/* -all : give complete res file info */
		if ((next_arg[2] && 
		     strncmp ( "-all", next_arg, len) != 0) )
					usage ( argv ) ;
		fullList = TRUE;
		continue ;

	case 'f':	/* -font : requires display */
		if ((next_arg[2] && 
		     strncmp ( "-font", next_arg, len) != 0) )
					usage ( argv ) ;
		checkFonts = TRUE;
		fullList = TRUE;
		continue ;

	case 'p':	/* -panel panelname */
		if ((next_arg[2] && 
		     strncmp ( "-panel", next_arg, len) != 0) 
		     		|| (++i >= argc))
					usage ( argv ) ;
		if ( argv[i][0] == '-' )
		{
			fprintf (stderr, 
				"\n-panel option requires a panelname.\n" );
			usage ( argv );
		}
		else
		{
		    s_copy (argv[i], requestedPanel);
		    printf ("\n...Dumping ONLY info for panel {%s}\n\n",
		            requestedPanel );
		    /* ASSUMPTION: we only look at the view */
		    s_append ("_v", requestedPanel);
		}
		onePanel = TRUE;
		continue ;

	default :
		fprintf (stderr, "%s: Unknown option: %s\n", 
			argv[0], next_arg );
		usage ( argv );
		break ;
	} /* switch */
    } /* for */

    if ( checkFonts && !checkDisplay )
    	{
   	printf ("Sorry, must supply -d display with -f option.\n");
	usage (argv);
    	}

    if ( !filename || NULLSTR ( filename ) )
    	{
    	printf ("Please enter a resource file name : ");
    	scanf("%s", filename);
    	}

/* Note: Co_ReadFile uses a default extention of ".res" */
    printf ("Reading resource file (%s)\n", filename);
    code = Co_ReadFile (vmCollection, filename, P_ABORT);
    if (code != SUCCESS)
	{
	printf("Could not read the file, code = %d\n", code);
	exit(0);
	}

/* Get the list of names and ids for each parblock */
    names = (TEXT **)Co_NameVector(vmCollection);
    ids = (Id *)Co_IdVector(vmCollection);
    for (i=0; names[i] != NULL; ++i)
	{
	/* only look at panel views (end with "_v") */
	len = s_length(names[i]);

	/* if -p option, skip all but requested panel */
	if ( onePanel && !s_equal(requestedPanel, names[i]) )
		continue;

	if (s_equal("_v", names[i]+(len-2)))	/* -2 for the "_v" */
	    {
	    TEXT targetName[16];
	    Id targetPanelId;
	    COUNT j;

	    onePanelFound = TRUE ;

	    *(names[i]+(len-2)) = EOS;		/* truncate name of panel */
	    printf("\nPanel = (%s)\n", names[i]);
	    currentPanel = names[i];	/* save current panel name */
	    s_copy(names[i], targetName);
	    s_append("_t", targetName);
	    for (j=0; !s_equal(names[j], targetName); j++); 
	    Vm_ForEach(ids[i], printItems, ids[j]);
	    }
	}

     if ( onePanel && !onePanelFound )
	{
	*(requestedPanel+(s_length(requestedPanel)-2)) = EOS;	/* truncate name*/
	fprintf (stdout, 
	"\nCould not find requested panel: {%s}\n",
	requestedPanel );
	fprintf (stdout, "To see panel names, do:\n\t'ar tv %s | more'\n",
	filename);
	fprintf (stdout, "Then supply a name without the '_v.par'.\n");
	}

     if (checkDisplay)
	fprintf (stdout, 
	"\nChecked resources against display %s.  Number of warnings: %d\n", 
		display, numWarnings );
} /* main */

FUNCTION CODE printItems(v, targetPanelId)
struct VARIABLE *v;
struct SYMTAB *targetPanelId;
{
void printAttributes();
struct VARIABLE *targetItem;
struct VARIABLE *Vm_FindStVar(), *Vm_FindVar();

/* Get presentation type qualifier */
struct VARIABLE *type = Vm_FindStVar(&v->v_qualst, "_type");

targetItem = Vm_FindVar(targetPanelId, v->v_name); 
if (type)
    {
    currentPresType = SVAL(*type,0);	/* save for printAttributes */
    printf ("  item (%s) : Presentation Type (%s), Target Data Type (%s)\n\
    Generates Events  = (%s)\n", v->v_name, 
		SVAL(*type,0), 
		(targetItem ? dataTypes[targetItem->v_type] : "NONE"),
		(targetItem && targetItem->v_event ? "TRUE" : "FALSE"));
    }
else
    {
    printf ("  item (%s) : Presentation Type (UNKNOWN), Target Data Type (%s)\n\
    Generates Events  = (%s)\n", v->v_name, 
		(targetItem ? dataTypes[targetItem->v_type] : "NONE"),
		(targetItem && targetItem->v_event ? "TRUE" : "FALSE"));
    }

/* Item's title is value of the view */
/** printf ("    (%-15s) %13s \"%s\"\n", "Title", "=",  SVAL(*v, 0)); **/
printf ("    (%s %-15s) %4s \"%s\"\n", "Title of", v->v_name, "=",  SVAL(*v, 0));

if (fullList && v->v_qualst.link != NULL)
    printAttributes ( v, &v->v_qualst);
printf("\n");
return(NULL);		/* always ok */
}


FUNCTION void printAttributes (v, qualst)
struct VARIABLE *v;			/* current item */
struct SYMTAB *qualst;			/* List of view qualifiers */
{
    struct VARIABLE *qual;

    Status status ;	/* from XLookupColor */

for (qual = qualst->link; qual != NULL; qual = qual->v_link)
    {
    if (qual->v_name[0] != '_')
	{
	COUNT index;

        /** printf ("    (%s) : (%s) = (", qual->v_name, **/
        printf ("    (%-15s) : (%7s) = (", 
		qual->v_name, 
		dataTypes[qual->v_type]);

	switch (qual->v_type)
	   {
	   case V_INTEGER:
		for(index=0; index < qual->v_count; ++index)
		    printf("%d ", IVAL(*qual, index)); 
		break;

	   case V_REAL:
		for(index=0; index < qual->v_count; ++index)
		    printf("%f ", RVAL(*qual, index)); 
		break;

	   case V_STRING:
		if (  qual->v_count > 1 )	/* multi-valued qualifier */
		    {
		    printf("....\n");
		    for(index=0; index < qual->v_count; ++index)
			{
    	    	    	if ( SVAL(*qual, index) )
		            printf("\t'%s'\n", SVAL(*qual, index)); 
    	    	    	else
		            printf("\t'NULL'\n"); 

			if ( checkDisplay ) /* only if -d display option */
			    {
			    /* CHECK DDO COLOR (except for Discretes) */
			    /* rangeval -> color for DDO (except Discretes)
			     * colorList -> color for dyntext (dynlabel)
			     */
			    if ( ( s_equal (qual->v_name, "rangeval")  &&
			     ! (s_equal (currentPresType, "Discrete")) &&
			     ! (s_equal (SVAL(*qual,index), "parent")) &&
			     ! (s_equal (SVAL(*qual,index), "")) &&
			     ! (s_equal (SVAL(*qual,index), "DEFAULT")) )
			     || ( s_equal (qual->v_name, "colorList")  &&
			     ! (s_equal(SVAL(*qual,index), "Item Foreground"))))
			        {
			       status = XLookupColor (dpy, cmap, 
				SVAL(*qual,index),    /* color name */
				&exact_color, &screen_color );
			       if ( status != 1 )
				   {
				   printf(")\n");
				   ++numWarnings;
				   printf ("\nWARNING: unknown %s \
color (%s) : %s item (%s), panel (%s)\n",
(s_equal (currentPresType, "dynlabel") ? "fg" : "DDO" ),
					SVAL(*qual,index), 
					currentPresType,
					v->v_name, 
					currentPanel );
				   }
			      } /* fg */
			   } /* checkDisplay */
			} /* for */
		    printf("\t....");
		    }
		else	/* single-valued qualifier */
		    {
    	    	    if ( SVAL(*qual, 0) )
		        printf("%s", SVAL(*qual, 0)); 
    	    	    else
		        printf( "NULL" );

		    /* special fg/bg color check; skip refs to "parent" */
		    if ( checkDisplay ) /* only if -d display option */
			{
			/* CHECK FOREGROUND COLOR */
		    	if (( s_equal (qual->v_name, "fg")  ||
		    	     s_equal (qual->v_name, "menu_fg")  ||
		    	     s_equal (qual->v_name, "labelfg")  )  &&
			     ( ! (s_equal (SVAL(*qual,0), "parent")) &&
			     ! (s_equal (SVAL(*qual,0), "Panel Foreground")) &&
			     ! (s_equal (SVAL(*qual,0), "Icon Foreground")) ))
			     {
			     status = XLookupColor (dpy, cmap, 
				SVAL(*qual,0),    /* color name */
				&exact_color, &screen_color );
			     if ( status != 1 )
				{
				printf(")\n");
				++numWarnings;
				if (s_equal (v->v_name, "_panel") &&
				   s_equal (currentPresType, "panel") )
				     printf ("\nWARNING: unknown panel fg \
color (%s) : panel (%s)\n",
					SVAL(*qual,0), 
					currentPanel );
				else 
				     printf ("\nWARNING: unknown fg \
color (%s) : %s item (%s), panel (%s)\n",
					SVAL(*qual,0), 
					currentPresType,
					v->v_name, 
					currentPanel );
				}
			     } /* fg */

			/* CHECK BACKGROUND COLOR */
		    	else if ( ( s_equal (qual->v_name, "bg")  ||
		    	     s_equal (qual->v_name, "menu_bg")  ||
		    	     s_equal (qual->v_name, "labelbg")  ) &&
			     ( ! (s_equal (SVAL(*qual,0), "parent")) &&
			     ! (s_equal (SVAL(*qual,0), "Panel Background")) &&
			     ! (s_equal (SVAL(*qual,0), "Icon Background")) ))
			     {
			     status = XLookupColor (dpy, cmap, 
				SVAL(*qual,0),    /* color name */
				&exact_color, &screen_color );
			     if ( status != 1 )
				{
				printf(")\n");
				++numWarnings;
				if (s_equal (v->v_name, "_panel") &&
				   s_equal (currentPresType, "panel") )
				     printf ("\nWARNING: unknown panel bg \
color (%s) : panel (%s)\n",
					SVAL(*qual,0), 
					currentPanel );
				else 
				     printf ("\nWARNING: unknown bg \
color (%s) : %s item (%s), panel (%s)\n",
					SVAL(*qual,0), 
					currentPresType,
					v->v_name, 
					currentPanel );
				}
			     } /* bg */
			} /* if checkDisplay */

/* Font checking often slows this program down since there is no
 * way to check the font without loading it and then unloading it.
 */
			if (checkFonts)
			{
			/* Note that pulldown has menu_font as well
			 * as "font", but icon only has "font".
			 */
		    	if (( s_equal (qual->v_name, "font")  ||
		    	     s_equal (qual->v_name, "menu_font") )  &&
			     ! (s_equal (SVAL(*qual,0), "parent")) )
			     {
			     if ( ( fontinfo = XLoadQueryFont (dpy,
				    SVAL(*qual,0) )) == NULL )
				{
				printf(")\n");
				++numWarnings;
				if (s_equal (v->v_name, "_panel") &&
				   s_equal (currentPresType, "panel") )
				     printf ("\nWARNING: unknown panel font \
(%s) : panel (%s)\n",
					SVAL(*qual,0), 
					currentPanel );
				else 
				     printf ("\nWARNING: unknown font \
(%s) : %s item (%s), panel (%s)\n",
					SVAL(*qual,0), 
					currentPresType,
					v->v_name, 
					currentPanel );
				}
			     else /* free X resource */
				XUnloadFont (dpy, fontinfo->fid );
			     } /* font */
			 } /* checkFonts */
		     } /* else single value string */
		break; /* case V_STRING */

	   default:
		break;
	   } /* switch */
	printf(")\n");
	}
    }
return;
} /* printAttributes */

usage ( argv )
char **argv ;
{
     fprintf (stderr, "usage:\n");
     fprintf (stderr, "%s [-a [-p panelname] [-d display [-f]] ] resfile\n", 
		argv[0] );
     fprintf (stderr, "-a           (dump all resfile info)\n");
     fprintf (stderr, "-p panelname (dump ONLY the panel \"panelname\")\n");
     fprintf (stderr, "-d display   (X display to check colors against)\n");
     fprintf (stderr, "-f           (check fonts against display)\n");
     fprintf (stderr, "\nNotes: \n");
     fprintf (stderr, "Default (no options) is to produce SHORT resfile listing.\n");
     fprintf (stderr, "Use of -d implies -a.\n");
     fprintf (stderr, "Use of -f requires -d.\n");
     fprintf (stderr, "-f slows this program down considerably.\n");
     fprintf (stderr, "\nKeyword 'WARNING' signifies POSSIBLE resfile \n");
     fprintf (stderr, "portability problems, such as with colors or fonts.\n");
     exit (1);
} /* usage */

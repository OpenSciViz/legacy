#
# Simple makefile for reslister
# June 20, 1990 Century Computing, Inc.
# May  08, 1991 Added -lX11 and changed libs to match v5.1 tree...kbs
# Aug  27, 1993 PR2221: Added -D_NO_PROTO, removed -O4...kbs,krw
#
# To make this program just type:
#	make -f reslister.mak
#
reslister: reslister.o
	cc reslister.o  \
	-I$(TAEINC) \
	-L$(TAELIB) \
	-o reslister \
	-lX11 -ltaec -ltae  \
	-ltermlib
#
reslister.o: reslister.c
	cc -D_NO_PROTO -I$(TAEINC)  -c reslister.c

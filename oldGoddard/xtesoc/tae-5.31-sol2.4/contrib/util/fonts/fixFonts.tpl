# ************************************************************
#
#  fixFonts.tpl >>> Replace wild-card res font names w/ specific res font names
#  Version: @(#)fixFonts.tpl	33.1 8/26/94 18:19:52
#
#	Usage:
#	% taeperl $TAE/contrib/util/fonts/fixFonts.tpl file1 file2 file3 ...
#
#  To run this script, your current working directory must be the 
#  directory holding the files to be processed.  
#
#  A new subdirectory in the CWD's parent ($PWD/../ff_dir) is created 
#  and the modified files are written to it.
#
#  Typical usage might look like:
#	% cd myPicFileDir			# cd to your picture file dir
#	% taeperl $TAE/contrib/util/fonts/fixFonts.tpl *.ps
#						# run script on all pic files
#	% cd ..					# cd to the parent directory
#	% mv myPicFileDir myPicFileDir_safety	# mv old pic files to bkp
#	% mv ff_dir myPicFileDir		# mv new pic files to pic dir
#	(test your applications)		# make sure everything works
#	% rm -rf myPicFileDir_safety		# delete the old picture files
#
#  
#  CHANGE LOG
#  17-feb-94	Initial...swd
#  18-feb-94	Only scan @FontName vector if line starts with "%I f" but not
#		"%I f u"...cew
#  18-feb-94	Fixed typo in font name, added code to quit search-and-
#		replace loop after first match...swd
#
# ***********************************************************

    # ----- Assign @oldFontName vector.  The first string in each pair
    # -----    is the old font name.  The second is the new font name.
    # ----- To add new font names for replacement, it is necessary only
    # -----    to add new pairs here.  No other change need be made.
    # ----- The first string in each pair will be treated as a regular 
    # -----    expression in the egrep sense, so to match a "*", we 
    # -----    must precede it with two backslashes.  The first
    # -----    backslash forces the second backslash to be stored in the
    # -----    variable.  The second backslash (the only one remaining at
    # -----    match time) causes the * to be matched literally.
    @FontName = (

	"\\*-courier-bold-o-\\*-140-75-75-\\*",
	"-adobe-courier-bold-o-*-*-14-*-75-75-*-*-*-*",

	"\\*-courier-bold-r-\\*-120-\\*",
	"-adobe-courier-bold-r-*-*-12-*-75-75-*-*-*-*",

	"\\*-courier-medium-r-\\*-100-\\*",
	"-adobe-courier-medium-r-*-*-10-*-75-75-*-*-*-*",

	"\\*-courier-medium-r-\\*-80-\\*",
	"-adobe-courier-medium-r-*-*-8-*-75-75-*-*-*-*",

	"\\*-helvetica-bold-r-\\*-140-\\*",
	"-adobe-helvetica-bold-r-*-*-14-*-75-75-*-*-*-*",

        "\\*-helvetica-medium-o-\\*-140-\\*",
	"-adobe-helvetica-medium-o-*-*-14-*-75-75-*-*-*-*",

        "\\*-helvetica-medium-r-\\*-120-\\*",
	"-adobe-helvetica-medium-r-*-*-12-*-75-75-*-*-*-*",

        "\\*-helvetica-medium-r-\\*-140-\\*",
	"-adobe-helvetica-medium-r-*-*-14-*-75-75-*-*-*-*",

        "\\*-times-bold-r-\\*-140-\\*",
	"-adobe-times-bold-r-*-*-14-*-75-75-*-*-*-*",

        "\\*-times-bold-r-\\*-240-\\*",
	"-adobe-times-bold-r-*-*-24-*-75-75-*-*-*-*",

        "\\*-times-medium-i-\\*-140-\\*",
	"-adobe-times-medium-i-*-*-14-*-75-75-*-*-*-*",

        "\\*-times-medium-r-\\*-120-\\*",
	"-adobe-times-medium-r-*-*-12-*-75-75-*-*-*-*",

        "\\*-times-medium-r-\\*-140-\\*",
	"-adobe-times-medium-r-*-*-14-*-75-75-*-*-*-*",

        "\\*-times-medium-r-\\*-180-\\*",
	"-adobe-times-medium-r-*-*-18-*-75-75-*-*-*-*",

        "-\\*-courier-bold-r-normal-\\*-12-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-courier-bold-r-*-*-12-*-75-75-*-*-*-*",

        "-\\*-courier-medium-r-normal-\\*-10-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-courier-medium-r-*-*-10-*-75-75-*-*-*-*",

        "-\\*-courier-medium-r-normal-\\*-8-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-courier-medium-r-*-*-8-*-75-75-*-*-*-*",

        "-\\*-helvetica-bold-r-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-helvetica-bold-r-*-*-14-*-75-75-*-*-*-*",

        "-\\*-helvetica-medium-o-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-helvetica-medium-o-*-*-14-*-75-75-*-*-*-*",

        "-\\*-helvetica-medium-r-normal-\\*-12-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-helvetica-medium-r-*-*-12-*-75-75-*-*-*-*",

        "-\\*-helvetica-medium-r-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-helvetica-medium-r-*-*-14-*-75-75-*-*-*-*",

        "-\\*-times-bold-r-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-times-bold-r-*-*-14-*-75-75-*-*-*-*",

        "-\\*-times-medium-i-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-times-medium-i-*-*-14-*-75-75-*-*-*-*",

        "-\\*-times-medium-r-normal-\\*-12-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-times-medium-r-*-*-12-*-75-75-*-*-*-*",

        "-\\*-times-medium-r-normal-\\*-14-\\*-\\*-\\*-\\*-\\*-\\*-\\*",
	"-adobe-times-medium-r-*-*-14-*-75-75-*-*-*-*"
    );

    # ----- Get the total number of font names.
    $nFonts = $#FontName + 1;

    # ----- Make the temporary directory.
    $dirname = "../ff_dir";
    $retStat = system ("mkdir $dirname") / 256;
    if (0 != $retStat) {
	print 
	    "\nfixFont: Failed creating directory.  mkdir returned $retStat\n";
	exit (1);
    }

    # ----- For each file specified on the command line...
    foreach $file (@ARGV) {
	print ("Processing $file \n");
	open (INFILE, $file) || die "fixFonts: Failed opening $file for input";
	open (OUTFILE, ">$dirname/$file") || 
	    die "fixFonts: Failed opening file $file for output";
	# ----- As long as there input available from this file...
	while ($_ = <INFILE>) {
	    if (index($_, "%I f") == 0 && index($_, "%I f u") != 0) {
		for ($i = 0; $i < $nFonts; $i += 2) {
		    last if (s/$FontName[$i]/$FontName[$i + 1]/g);
		}
	    }
	    print OUTFILE  $_;
	}
	close (INFILE);
	close (OUTFILE);
    }

    print ("Done.\n");


To use the user contributed presentation type, Color Logger (internal
name "scroller"), refer to the files "scroller.doc" and
"scroller.txt".

In addition, the file "scroller_wpt.doc" documents three Wpt calls
that are specific to the Color Logger.

They are:

Wpt_GetLoggerSelectedLineNumber - Return the number of the currently
					selected Color Logger line

Wpt_GetLoggerLine - get the text string at the specified line

Wpt_SetLoggerLine - replace the text in the buffer at the specified
			line with a new text string


There is an unbuilt demo, "scroller", which demonstrates the use of
these calls.  There is a source file "scroller.c", an rfg file
"scroller.rfg", and a help file "scroller.hlp".  On Unix platforms,
these files are in $TAECONTRIB/demo/scroller.  On VMS platforms, these
files are in the SCROLLER subdirectory under the DEMO subdirectory of
$TAECONTRIB.


/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/* *** TAE Plus Code Generator version V5.2 *** */
/* *** File:        pan_speed.c *** */
/* *** Generated:   Nov 13 11:12:48 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  speed
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_speed.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   speed
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   quit,            speedDynText
 *   
 * CHANGE LOG:
 * 13-Nov-92    Initially generated...TAE
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_speed.h"

/* One "include" for each connected panel */


/* MOD + crb */
#include		"sockcl.h"
extern T_SOCKCL_DATA	*socketData;
/* - */

Id speedTarget, speedView, speedId;
/* speedDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID speed_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    Id  Co_Find ();
    
    speedView = Co_Find (vmCollection, "speed_v");
    speedTarget = Co_Find (vmCollection, "speed_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID speed_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (speedId)
        printf ("Panel (speed) is already displayed.\n");
    else
        speedId = Wpt_NewPanel (Default_Display, speedTarget, speedView,
                                relativeWindow, speedDispatch, flags);
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID speed_Destroy_Panel ()
    {
    Wpt_PanelErase(speedId);
    speedId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID speed_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (speedId)
        Wpt_SetPanelState (speedId, flags);
    else
        speed_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  quit
 */
EVENT_HANDLER quit_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* MOD + crb */
    sockcl_send (socketData, "quit");
    /* - */

    /* Begin generated code for Connection */
    
    do_prog_exit ();
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  speedDynText
 */
EVENT_HANDLER speedDynText_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    }



struct DISPATCH speedDispatch[] = {
    {"quit", quit_Event},
    {"speedDynText", speedDynText_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

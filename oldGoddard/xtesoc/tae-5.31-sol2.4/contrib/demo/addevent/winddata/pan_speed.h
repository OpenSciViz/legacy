/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/* *** TAE Plus Code Generator version V5.2 *** */
/* *** File:        pan_speed.h *** */
/* *** Generated:   Nov 13 11:12:48 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Header file for panel:  speed
 *
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   speed
 * 
 * CHANGE LOG:
 * 13-Nov-92    Initially generated...TAE
 * **************************************************************************
 */

#ifndef I_PAN_speed                         /* prevent double include */
#define I_PAN_speed      0

/* Vm objects and panel Id.   */
extern Id speedTarget, speedView, speedId;

/* Dispatch table (global for calls to Wpt_NewPanel) */
extern struct DISPATCH speedDispatch[];

/* Initialize speedTarget and speedView */
extern VOID speed_Initialize_Panel ();

/* Create this panel and display it on the screen */
extern VOID speed_Create_Panel ();

/* Destroy this panel and erase it from the screen */
extern VOID speed_Destroy_Panel ();

/* Connect to this panel.  Create it or change it's state */
extern VOID speed_Connect_Panel ();
#endif


/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

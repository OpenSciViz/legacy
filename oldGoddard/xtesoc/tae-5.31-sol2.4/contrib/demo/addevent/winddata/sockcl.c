/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * Project:     TAE Plus
 * Facility:    Socket Demo
 * File:        sockcl.c
 *
 *
 * sockcl is the package that handles sockets from the client end.
 *
 * CHANGE LOG:
 *
 * 06-nov-92    initial version...crb
 *
 */

static char *c_file_version = "@(#)sockcl.c	33.1 8/26/94";

/* include files */
#include	<stdio.h>
#include	<errno.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<sys/un.h>
#include	"taeconf.inp"
#include	"sockcl.h"

/* sockcl_setup: create the socket */
FUNCTION T_SOCKCL_DATA	*sockcl_setup (name)
    char		*name;
    {
    char		*modname = "sockcl_setup";
    T_SOCKCL_DATA	*sockclData;
    int			len, code;

    /* obtain a new socket data packet */
    sockclData = (T_SOCKCL_DATA *)malloc (sizeof (T_SOCKCL_DATA));
    memset (sockclData, 0, sizeof (T_SOCKCL_DATA));
    
    /* open the socket */
    sockclData->socketId = socket (AF_UNIX, SOCK_STREAM, 0);
    if (sockclData->socketId < 0)
	{
	fprintf (stderr, "%s: socket failed (err=%d).\n", modname, errno);
	free (sockclData);
	return (NULL);
	}

    /* record the name */
    s_bcopy (name, sockclData->socketName, sizeof (sockclData->socketName)-1);
    
    /* declare ourselves done */
    sockclData->setup = TRUE;
    return (sockclData);
    }

/* connect to the server socket */
FUNCTION int		sockcl_connect (sockclData)
    T_SOCKCL_DATA	*sockclData;
    {
    char	*modname = "sockcl_connect";
    struct 	sockaddr_un saun;
    int		len, code;

    if (!sockclData->setup)
	{
	fprintf (stderr, "%s: sockcl not set up.\n", modname);
	return (FAIL);
	}
    
    /* create the connection address */
    saun.sun_family = AF_UNIX;
    strcpy (saun.sun_path, sockclData->socketName);

    /* attempt a connection */
    len  = sizeof (saun.sun_family) + s_length (sockclData->socketName);
    code = connect (sockclData->socketId, &saun, len);
    if (code < 0)
	{
	fprintf (stderr, "%s: connect failed (err=%d).\n", modname, errno);
	return (FAIL);
	}

    sockclData->connected = TRUE;
    return (SUCCESS);
    }

/* send some data out to the socket */
FUNCTION int		sockcl_send (sockclData, data)
    T_SOCKCL_DATA	*sockclData;
    char		*data;
    {
    char	*modname = "sockcl_send";
    int		code;
    
    if (!sockclData->setup)
	{
	fprintf (stderr, "%s: sockcl not set up.\n", modname);
	return (FAIL);
	}

    if (!sockclData->connected)
	{
	code = sockcl_connect ();
	if (code != SUCCESS)
	    return (FAIL);
	}
    code = send (sockclData->socketId, data, s_length(data)+1, 0);
    if (code < 0)
	{
	fprintf (stderr, "%s: send failed (err=%d)\n", modname, errno);
	return (FAIL);
	}
    
    return (SUCCESS);
    }

/* close the socket */
FUNCTION int		sockcl_close (sockclData)
    T_SOCKCL_DATA	*sockclData;
    {
    close (sockclData->socketId);
    free (sockclData);
    return (SUCCESS);
    }

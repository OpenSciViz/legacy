/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * Project:     TAE Plus
 * Facility:    Socket Demo
 * File:        sockcl.h
 *
 *
 * sockcl is the package that handles sockets from the client end.
 * sockcl.h contains package definitions.
 *
 * CHANGE LOG:
 *
 * 06-nov-92    initial version...crb
 *
 */

#ifndef SOCKCL_H
#define SOCKCL_H

/* client socket info structure */
typedef struct	S_SOCKCL_DATA
    {
    BOOL	setup;
    BOOL	connected;
    int		socketId;
    char	socketName[STRINGSIZ];
    } T_SOCKCL_DATA;

/* function prototypes */
FUNCTION T_SOCKCL_DATA	*sockcl_setup ();
FUNCTION int		sockcl_connect ();
FUNCTION int		sockcl_send ();
FUNCTION int		sockcl_close ();

#endif	/* SOCKCL_H */

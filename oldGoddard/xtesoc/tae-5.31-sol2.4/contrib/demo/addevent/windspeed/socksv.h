/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * Project:     TAE Plus
 * Facility:    Socket Demo
 * File:        socksv.h
 *
 *
 * socksv is the package that handles sockets from the client end.
 * socksv.h contains package definitions.
 *
 * CHANGE LOG:
 *
 * 06-nov-92    initial version...crb
 *
 */

#ifndef SOCKSV_H
#define SOCKSV_H

#define SOCK_BACKLOG	(4)	/* max number of simultaneous connections */

/* include files */
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<sys/un.h>
#include	<sys/filio.h>

/* socket info structure */
typedef struct	S_SOCKSV_DATA
    {
    BOOL	setup;
    int		socketId;
    int		commSockId;			/* make array for conns > 1 */
    struct	sockaddr_un commSockSaun;	/* make array for conns > 1 */
    int		currCommIndex;			/* only for conns > 1 */
    char	socketName[STRINGSIZ];
    } T_SOCKSV_DATA;

/* function prototypes */
FUNCTION T_SOCKSV_DATA	*socksv_setup ();
FUNCTION int		socksv_accept ();
FUNCTION void		socksv_read ();
FUNCTION int		socksv_close ();

#endif	/* SOCKSV_H */

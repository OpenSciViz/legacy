/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/* *** TAE Plus Code Generator version V5.2 *** */
/* *** File:        windspeed_init_pan.c *** */
/* *** Generated:   Nov  4 17:38:01 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Initialize all panels in the resource file.
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     A panel is deleted
 *     A new panel is added
 *     A panel's name is changed (not title)
 * For the panels:
 *   wind
 * 
 * CHANGE LOG:
 *  4-Nov-92    Initially generated...TAE
 * ************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "symtab.inc"
#include        "global.h"                  /* Application globals */

/* One "include" for each panel in resource file */
#include        "pan_wind.h"


FUNCTION VOID windspeed_Initialize_All_Panels (resfileSpec)
    TEXT        *resfileSpec;
    {
    extern Id   Co_Find ();
    extern Id   Co_New ();
    Id vmCollection ;
    
    /* read resource file */
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, resfileSpec, P_ABORT);
    
    /* initialize view and target Vm objects for each panel */
    wind_Initialize_Panel (vmCollection);
    }
    

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

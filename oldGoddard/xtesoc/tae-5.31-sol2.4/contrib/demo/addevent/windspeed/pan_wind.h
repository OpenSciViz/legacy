/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/* *** TAE Plus Code Generator version V5.2 *** */
/* *** File:        pan_wind.h *** */
/* *** Generated:   Nov  4 17:39:11 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Header file for panel:  wind
 *
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   wind
 * 
 * CHANGE LOG:
 *  4-Nov-92    Initially generated...TAE
 * **************************************************************************
 */

#ifndef I_PAN_wind                          /* prevent double include */
#define I_PAN_wind      0

/* Vm objects and panel Id.   */
extern Id windTarget, windView, windId;

/* Dispatch table (global for calls to Wpt_NewPanel) */
extern struct DISPATCH windDispatch[];

/* Initialize windTarget and windView */
extern VOID wind_Initialize_Panel ();

/* Create this panel and display it on the screen */
extern VOID wind_Create_Panel ();

/* Destroy this panel and erase it from the screen */
extern VOID wind_Destroy_Panel ();

/* Connect to this panel.  Create it or change it's state */
extern VOID wind_Connect_Panel ();
#endif


/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

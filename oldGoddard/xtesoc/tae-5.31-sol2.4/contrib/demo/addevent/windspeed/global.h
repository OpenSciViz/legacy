/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/* *** TAE Plus Code Generator version V5.2 *** */
/* *** File:        global.h *** */
/* *** Generated:   Nov  4 17:39:11 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This global header file is automatically "#include"d in each panel
 * file.  You can insert references to global variables here.
 *
 * REGENERATED:
 * This file is generated only once.
 * 
 * CHANGE LOG:
 *  4-Nov-92    Initially generated...TAE
 * ************************************************************************
 */

#ifndef I_GLOBAL                            /* prevent double include */
#define I_GLOBAL        0

/*      macros for access to parameter values 
 *
 * These macros obtain parameter values given the name of 
 * a Vm object and the name string of the parameter. 
 * The Vm objects are created by the Initialize_All_Panels
 * function for a resource file.
 *  
 * Reference scalar parameters as follows: 
 *    
 *      StringParm(myPanelTarget, "s")    -- string pointer   
 *      IntParm(myPanelTarget, "i")       -- integer value   
 *      RealParm(myPanelTarget, "r")      -- real value   
 *    
 *  For vector parameters, do the following: 
 *    
 *      TAEINT  *ival; 
 *      ival = &IntParm(myPanelTarget, "i"); 
 *      printf ("%d %d %d", ival[0], ival[1], ival[2]); 
 *  
 */ 
 
struct VARIABLE *Vm_Find ();  
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  

/*      Dispatch Table typedef          */ 
 
typedef VOID (*FUNCTION_PTR) (); 
typedef struct DISPATCH
    { 
    TEXT                *parmName; 
    FUNCTION_PTR        eventFunction; 
    } ;
    
#define EVENT_HANDLER static VOID           /* a flag for documentation */

/*      Display Id for use by direct Xlib calls: */
 
extern Display   *Default_Display;


/*      Externally define wptEvent so event handlers have access to it */

extern WptEvent  wptEvent;  /* event structure returned by Wpt_NextEvent */


#define SET_APPLICATION_DONE \
    { \
    extern BOOL Application_Done; \
    Application_Done = TRUE; \
    }

#endif

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

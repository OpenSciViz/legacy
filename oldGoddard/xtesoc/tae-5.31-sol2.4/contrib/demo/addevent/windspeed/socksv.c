/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * Project:     TAE Plus
 * Facility:    Socket Demo
 * File:        socksv.c
 *
 *
 * socksv is the package that handles sockets from the server end.
 *
 * CHANGE LOG:
 *
 * 06-nov-92    initial version...crb
 *
 */

static char *c_file_version = "@(#)socksv.c	33.1 8/26/94";

/* include files */
#include	<stdio.h>
#include	<errno.h>
#include	"taeconf.inp"
#include	"socksv.h"

/* socksv_setup: create the socket */
FUNCTION T_SOCKSV_DATA	*socksv_setup (name)
    char		*name;
    {
    char		*modname = "socksv_setup";
    T_SOCKSV_DATA	*socksvData;
    struct 		sockaddr_un saun;
    int			len, code;

    /* obtain a new socket data packet */
    socksvData = (T_SOCKSV_DATA *)malloc (sizeof (T_SOCKSV_DATA));
    memset (socksvData, 0, sizeof (T_SOCKSV_DATA));
    
    /* open the socket */
    socksvData->socketId = socket (AF_UNIX, SOCK_STREAM, 0);
    if (socksvData->socketId < 0)
	{
	fprintf (stderr, "%s: socket failed (err=%d).\n", modname, errno);
	return (FAIL);
	}

    /* record the name */
    s_bcopy (name, socksvData->socketName, sizeof (socksvData->socketName)-1);

    /* bind the name to this socket */
    saun.sun_family = AF_UNIX;
    strcpy (saun.sun_path, socksvData->socketName);
    unlink (socksvData->socketName);
    len  = sizeof (saun.sun_family) + s_length (socksvData->socketName);
    code = bind (socksvData->socketId, &saun, len);
    if (code < 0)
	{
	fprintf (stderr, "%s: bind failed (err=%d).\n", modname, errno);
	free (socksvData);
	return (NULL);
	}
    
    /* listen on the socket */
    code = listen (socksvData->socketId, SOCK_BACKLOG);
    if (code < 0)
	{
	fprintf (stderr, "%s: listen failed (err=%d).\n", modname, errno);
	free (socksvData);
	return (NULL);
	}

    /* declare ourselves done */
    socksvData->setup = TRUE;
    return (socksvData);
    }


/* accept a connection to this socket */
FUNCTION int		socksv_accept (socksvData)
    T_SOCKSV_DATA	*socksvData;
    {
    char	*modname = "socksv_send";
    int		newLen;
    int		dontblock = 1;
    int		code;
    
    if (!socksvData->setup)
	{
	fprintf (stderr, "%s: socksv not set up.\n", modname);
	return (FAIL);
	}

    newLen = sizeof (socksvData->commSockSaun);
    socksvData->commSockId = accept (socksvData->socketId,
				     &(socksvData->commSockSaun), &newLen);
    if (socksvData->commSockId < 0)
	{
	fprintf (stderr, "%s: accept failed (err=%d)\n", modname, errno);
	return (FAIL);
	}

    /* set non-blocking IO on the socket */
    ioctl (socksvData->commSockId, FIONBIO, &dontblock);

    return (socksvData->commSockId);
    }

/* read incoming data on the socket */
FUNCTION void		socksv_read (socksvData, buf, size)
    T_SOCKSV_DATA	*socksvData;
    char		*buf;
    int			size;
    {
    char	*modname = "socksv_read";
    char	mybuf[STRINGSIZ+1];
    
    if (!socksvData->setup)
	{
	fprintf (stderr, "%s: socksv not set up.\n", modname);
	s_copy ("", buf);
	return;
	}

    /* receive the data */
    recv (socksvData->commSockId, mybuf, sizeof(mybuf-1), 0);
    if (errno == EWOULDBLOCK)
	{
	/* nothing to read on the socket; zap the buffer */
	s_copy ("", buf);
	}
    else
	{
	/* copy data into user-supplied buffer */
	s_bcopy (mybuf, buf, size);
	}
    return;
    }

/* close the socket */
FUNCTION int		socksv_close (socksvData)
    T_SOCKSV_DATA	*socksvData;
    {
    /* close and zap the socket */
    close (socksvData->socketId);
    unlink (socksvData->socketName);
    free (socksvData);
    return (SUCCESS);
    }

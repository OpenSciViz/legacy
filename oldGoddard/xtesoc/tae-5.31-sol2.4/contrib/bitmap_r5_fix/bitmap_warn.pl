#################################################################
#
# bitmap_warn.pl
#
# Used to create a $HOME/Bitmap app-defaults file for machines that don't
# have all of the X11r5 bitmaps (used by the "bitmap" program) installed.
# "bitmap" will still work without these bitmaps, but the warnings are
# annoying.
#
# If a user already has a $HOME/Bitmap app-defaults file, these resources
# can be appended to the file.  This app-defaults file is really only needed
# by users of the WorkBench who are editing icon items, menubar icons, or
# panel icons.
#
# Change Log:
# 18-may-94	PR2723: Created...cew

$prog_name = "bitmap_warn.pl";
$tae = $ENV{'TAE'};

$resource_file = "$ENV{'HOME'}/Bitmap";
$resource_data = "*bitmapFilePath:  $tae/contrib/bitmap_r5_fix";

if (-e $resource_file)
	{
	print "$prog_name: $resource_file already exists.\n";
	print "[O]verwrite, [A]ppend, [C]ancel ? ";
	$action = getc;
	if ($action !~ /^[OoAa]/)
		{
		print "Operation cancelled: no file written.\n";
		exit 0;
		}
	}

if ($action =~ /^[Oo]/)
	{
	open(FILEOUT, ">$resource_file") || 
		die "$prog_name: Can't open $resource_file for writing";
	}
else
	{
	open(FILEOUT, ">>$resource_file") || 
		die "$prog_name: Can't open $resource_file for writing";
	}

print(FILEOUT "$resource_data\n") || die "Can't write to $resource_file";

print "$resource_data\n";
print "...successfully written to $resource_file.\n"

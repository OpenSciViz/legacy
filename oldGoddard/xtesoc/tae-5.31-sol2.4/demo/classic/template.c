/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* 
 *	TAE C template
 *
 *
 *	This program first receives initial values of 
 * 	variables from the user. Then it reconstructs 
 *	parameter block to transmit back to TAE for 
 *	dynamic parameters. 
 *	Finally it receives dynamic parameter values 
 *	by receiving parameter block from TM.
 *
 *
 *	The PDF for this program is:
 *
 *	process		help=*
 *	parm file	type=file 	default="default-file"
 *	parm integer	type=integer	default=1
 *	parm real	type=real	default=1.0
 *	parm string	type=(string,20) default="default-string"
 *	end-proc
 *
 *
 *	template.c and template.pdf files are in the tae$demo
 *	directory for VMS and $TAEDEMOCLASSIC directory for UNIX
 *
 */

#include 	"taeconf.inp"
#include	"parblk.inc"
#include	"terminc.inc"
#include	<stdio.h>

	FUNCTION main()
	{
	Id	vmid, Vm_New();
	CODE	Vm_ReadFromTM();
	FILE	*fp, *Vm_OpenStdout();
	struct	VARIABLE *v;
	struct	VARIABLE *Vm_Find();
	TEXT	taefile[FSPECSIZ + 1];
	TEXT	*vtaefile[1];	/* a string value vector for taefile */
	TAEINT	vintg;		
	TAEINT	vvintg[1];	/* an integer value vector for vintg */
	TAEFLOAT vreal;
	TAEFLOAT vvreal[1];	/* a real value vector for vreal     */
	TEXT	vstring[132];
	TEXT	*vvstring[1];	/* a string  value vector for vstring */


	/* Receive initial values of variables from the user.	*/

	/* Create a new patameter block and use it to get	*/
	/* parameter values from TM.				*/
	vmid = Vm_New (P_ABORT);

	/*Receive a block of data from TM.			*/
	Vm_ReadFromTM (vmid);

	/* Open the current standard output device.		*/	
	fp = Vm_OpenStdout (vmid);

	/* Retrieve a value of a string variable from  dynamic  */
	/* memory parameter object.				*/

	v = Vm_Find (vmid, "FILE");
	s_copy (SVAL(*v, 0), taefile);
	vtaefile[0] = taefile;		/* for Vm_SetString	*/

	/* Retrieve a value of an integer variable.		*/
	v = Vm_Find (vmid, "INTEGER");
	vintg = IVAL(*v, 0);
	vvintg[0] = vintg;		/* for Vm_SetIntg	*/

	/* Retrieve a value of a real variable.			*/
	v = Vm_Find (vmid, "REAL");
	vreal = RVAL(*v, 0);
	vvreal[0] = vreal;		/* for Vm_SetReal	*/

	/* Retrieve a value of a string variable.		*/
	v = Vm_Find (vmid, "STRING");
	s_copy (SVAL(*v, 0), vstring);	
	vvstring[0] = vstring;		/* for Vm_SetString 	*/


	/* Reconstruct the parameter block to transmit back to  */
	/* TAE for dynamic parameters.		       		*/

	/* Add a file, an integer, a real, and a string    	*/
	/* variable name and their values to the parameter block*/
	
	Vm_SetString (vmid, "FILE", 1, vtaefile, P_UPDATE);
	Vm_SetIntg (vmid, "INTEGER", 1, vvintg, P_UPDATE); 
	Vm_SetReal (vmid, "REAL", 1, vvreal, P_UPDATE);
	Vm_SetString (vmid, "STRING", 1, vvstring, P_UPDATE);

	/* Send the parameter block to the TM with a set of    	*/
	/* initial values for a dynamic tutor session  		*/
	Vm_DynTutor (vmid, "",M_FULLPDF);

	/* Receive parameter values by receiving parameter 	*/
	/* block from TM */
	Vm_ReadFromTM (vmid);
	}

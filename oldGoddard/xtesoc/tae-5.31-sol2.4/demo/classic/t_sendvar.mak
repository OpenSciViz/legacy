.SUFFIXES: .pdf .pdf~
.pdf~.pdf:
	$(GET) -G$*.pdf $(GFLAGS) $<
#
CFLAGS = -I$(TINC) -O
SHELL = /bin/sh
#
t_sendvar: t_sendvar.o t_sendvar.pdf $(TAELIB)/libtae.a
	cc -o t_sendvar t_sendvar.o $(TAELIB)/libtaec.a $(TAELIB)/libtae.a \
		-ltermlib 

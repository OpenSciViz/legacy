/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*	TEST_SENDVAR: Demonstration program for ASYNC-PROCESS
 *
 *	This loops forever doing the following:
 *	Receives a PARBLK, and alters every value in the PARBLK:
 *
 *		All integers and reals are decremented by one.
 *		Strings are decreased by one character in length.
 *
 *	An output PARBLK is produced with the new values.
 *	The PARBLK is then returned to the JOB that sent the block.
 *
 *	The history of the _JOB variable:  
 *
 *	1. PARBLKs received here have a special string variable, _JOB, that
 *	identifies the job name of the sender.   The job name is the name
 *	of the mailbox for returning messages to the sender.  If the
 *	sender is the TAE Monitor, the _JOB variable is created automatically
 *	by TM (i.e., does not have to be in the SENDVAR variable list).
 *
 *	2. Likewise, when we return a block to the sender, we include
 *	a _JOB variable to identify ourself.
 *	
 * 	TBD: do something so that p_ and q_ errors don't hang.
 *		(For now we use P_CONT mode to get our own errors.)
 */

#include	"TAECONF"
#include	"PARBLK"		/* VBLOCK definitions     */
#include	"COMMINC"		/* mailbox c_ subroutines */
#include	<stdio.h>

static FILE 	*sysout = NULL;
static COUNT 	get_string();
static TAEINT  	get_integer();
static CODE	send_parblk();
struct VARIABLE *p_find();

VOID 	terminate();
CODE	emit();
#define	TERM	0x1000
#define STATUS  0x1001


main ()
{
struct PATH in_path;			/* input path control block    */
struct VARIABLE *v;
struct PARBLK in_parblk, out_parblk;	/* VBLOCKs		       */
CODE 	code;
COUNT	i, l, parblk_size;
TEXT	*s;
TEXT	sender_job[STRINGSIZ+1], this_job[STRINGSIZ+1], parent_job[STRINGSIZ+1];
TEXT	errmsg[STRINGSIZ+1];
COUNT	mb_size;			/* size of mailbox to create	*/
FILE	*z_init();
COUNT	messages = 0;			/* number processed		*/

sysout = z_init (&in_parblk, P_CONT);
if (sysout == NULL)
    terminate (in_parblk.hostcode, "z_init failure");
emit (0, "alive", STATUS);
get_string (&in_parblk, "_CHILD",  this_job);
get_string (&in_parblk, "_PARENT", parent_job);	
fprintf (sysout, "Job '%s' running under parent '%s'.\n", this_job, parent_job);
mb_size = get_integer (&in_parblk, "MB_SIZE");
code = c_crepath (&in_path, mb_size, this_job, TMPMBX);
if (code != SUCCESS)
    {
    fprintf (sysout, "Cannot create input mailbox. %s\n", in_path.errmsg);
    terminate (in_path.host_code, "c_crepath error");
    }
q_init (&out_parblk, P_CONT);
send_parblk (&out_parblk, this_job, parent_job);		/* tell parent we're alive */
while (FOREVER)	
    {
    parblk_size = mb_size;
    code = c_getmsg (&in_path, (GENPTR)&in_parblk, &parblk_size);
    makeabs (&in_parblk.symtab, in_parblk.pool);		/* make ptrs absolute */
    in_parblk.mode = P_CONT;					/* return errors */
    get_string(&in_parblk, "_JOB", sender_job);
    q_init (&out_parblk, P_BYTES, P_CONT);
    for (v=in_parblk.symtab.link; v != NULL; v=(*v).v_link)	/* for each variable  */
	{
	if (s_equal ((*v).v_name, "_JOB"))			/* ignore _JOB */
    	    continue;
	for (i=0; i < (*v).v_count; i++)			/* for each component */
	    if ((*v).v_type == V_INTEGER)
		IVAL(*v,i) --;
	    else if ((*v).v_type == V_REAL)
		RVAL(*v,i) = RVAL(*v,i) - 1.0;
	    else if ((*v).v_type == V_STRING)
		{
		s = SVAL(*v,i);					/* point to string  */
		l = s_length (s);				/* get length and   */
		if (l > 0)
		    s[l-1] = 0;					/* delete last char */
		}	    	    
	/*    place the current variable in out_parblk: 	*/

        if ((*v).v_type == V_INTEGER)
            q_intg (&out_parblk, (*v).v_name, (*v).v_count, (*v).v_cvp, P_ADD);
        else if ((*v).v_type == V_REAL)
	    q_real (&out_parblk, (*v).v_name, (*v).v_count, (*v).v_cvp, P_ADD);
        else if ((*v).v_type == V_STRING)
	    q_string (&out_parblk, (*v).v_name, (*v).v_count, (*v).v_cvp, P_ADD);
	}
    send_parblk (&out_parblk, this_job, sender_job);
    messages ++;
    emit (messages, "running", STATUS);
    }
}

    FUNCTION static TAEINT get_integer (parblk, name)

    struct PARBLK *parblk;	/* in: VBLOCK			*/
    TEXT	  name[];	/* in: name of variable		*/

    {
    struct VARIABLE *v;

    v = p_find (parblk, name);
    if (v == NULL)
    	{
    	fprintf (sysout, "Cannot find variable '%s'.", name);
        terminate (-1, "No variable.");
        }
    return (IVAL(*v,0));
    }




    FUNCTION static COUNT get_string (parblk, name, output)

    struct PARBLK *parblk;	/* in: VBLOCK			*/
    TEXT	  name[];	/* in: name of variable		*/
    TEXT	output[];	/* out: string value		*/

    {
    struct VARIABLE *v;

    v = p_find (parblk, name);
    if (v == NULL)
    	{
    	fprintf (sysout, "Cannot find variable '%s'.", name);
        terminate (-1, "No variable.");
        }
    return (s_copy (SVAL(*v,0), output));	/* return string length */
    }



    FUNCTION static CODE send_parblk (parblk, this_job, target_job)
    
    struct PARBLK	*parblk;	/* in: parblk to send            */
    TEXT		this_job[];	/* in: name of current job 	 */
    TEXT		target_job[];	/* in: name of job to send it to */
    
    {
    IMPORT GENPTR	r_top();	/* current top of allocated area */
    TEXT		*valvec[1]; 	/* string value vector for _JOB  */
    TEXT		errmsg[STRINGSIZ+1];
    CODE		code;

    valvec[0] = this_job;
    q_string (parblk, "_JOB", 1, valvec, P_ADD);	/* tell target who we are */
    (*parblk).blksiz = r_top ((*parblk).pool) - (GENPTR)parblk;    
    makerel (&(*parblk).symtab, (*parblk).pool);	/* make ptrs relative */
    code = c_pmwcd (target_job, (GENPTR)parblk, (*parblk).blksiz, errmsg);
    if (code != SUCCESS)
        fprintf (sysout, "Cannot send to '%s'.  %s.\n", target_job, errmsg);
    return (code);
    }

/*	emit.   Emit status to parent.  This is process analogy of
 *	the TCL EMIT command.
 */

#include	"ASYNCINC"

    FUNCTION CODE emit (sfi, skey, type)

    FUNINT	sfi;			/* in: $SFI to report		*/
    TEXT	skey[];			/* in: $SKEY to report		*/
    FUNINT	type;			/* in: STATUS or TERM		*/

    {
    struct MON_MSG	msg;
    CODE		code;

    msg.type = (type==TERM) ? MO_TERM : MO_STATUS;
    msg.m_sfi = sfi;
    s_bcopy (skey, msg.m_skey, sizeof(msg.m_skey) - 1);
#ifdef UNIX
    msg.m_childpid = getpid();
#endif
    code = c_snpa ((GENPTR)&msg, sizeof (msg));
    return (code);
    }



    FUNCTION VOID terminate (sfi, skey)

    FUNINT	sfi;			/* in: final $SFI status	*/
    TEXT	skey[];			/* in: final $SKEY string	*/
    
    {
    emit (sfi, skey, TERM);
    exit (0);
    }

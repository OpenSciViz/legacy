/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/




	/******************************
	* PARAMETER DEFINITIONS	      *
	******************************/


#ifndef sun
#ifndef VAX_VMS
#define         EPSILON  0.00000001
#endif
#endif
#define		forever  1	/* GOOD FOR INFINITE LOOPING */
#define		dimproc  20	/* CDIM FOR $PROCESS	     */
#define		intdim   3	/* CDIM FOR MULINT */
#define		rldim    3	/* DIM  FOR MULREA */
#define		strdim   3	/* DIM  FOR MULSTR */
#define		fildim   2	/* DIM  FOR MULFIL */
#define		oplun    6	/* OPERATOR LUN		     */
#define		pcprevloglun 1  /* LOG FILE LUN 	     */
#define		loglun   2      /* LOG FILE LUN OUTPUT LOG   */
#define		pdellun  3	/* LUN FOR FILE DELETE       */
#define		numds    4	/* NUMBER OF DATASETS        */
#define		parlun   3	/* LUN FOR DISK BLOCK ACCESS */
#define		imchan   7      /* NUMBER OF CHANNELS IN IMAGE FILE*/
#define		imline  10    	/* NUMBER OF LINES TO TEST   */
#define		linsiz  100   	/* NUMBER OF PIXELS PER IMAGE LINE*/
#define		imlabl   7    	/* NUMBER OF LABEL RECORDS IN IMG FIL*/
#define		imlun    7    	/* IMAGE FILE LOGICAL UNIT   */
#define		labsiz  20	/* NUMBER OF BYTES IN LABEL REC*/
#define		waitdelta 5    	/* POLL WAIT TIME BETWEEN OPENS*/
#define		filefound 0    	/* I/O STATUS CODE	     */
#define		filenotfound 29	/* I/O STATUS CODE	     */	
#define		badfilespec 43 	/* I/O STATUS CODE	     */
static  char    batchrt[]="BATCH"; /* RUNTYPE VALUE FOR BATCH   */


/***************************
* APPTEST ERROR CODES	   *
***************************/

#define		tnosub  -2
#define		tnolog  -3
#define		tnorcv  -4
#define		tnoblk  -5
#define		tnoatt  -6
#define		tnosnd  -7
#define		tnoadd  -8
#define		tnoupd  -9
#define		timwrt  -10
#define		tdata   -11
#define		tmatch  -12
#define		topen   -13 
#define		tread   -14
#define		tilacc  -15
#define		tattct  -16
#define		tnokil  -17
#define		tterm   -18
#define		tbqdyn  -19
#define         tilblw  -20
#define         tilblr  -21
#define         tilblm  -22

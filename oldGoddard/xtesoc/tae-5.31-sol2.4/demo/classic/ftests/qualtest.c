/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



C  ***** DO NOT USE TABS OR FORMFEEDS IN ANY UNIX FORTRAN FILES *****
C
        PROGRAM QUALFOR
        IMPLICIT INTEGER(A-Z)
C
C       QUALFOR if designed to verify the operations of the TAE
C       parameter passing mechanism for qualifiers.
C

#include "pgminc.fin"

        INTEGER LUN
        INTEGER VBLOCK(XPRDIM)
        INTEGER IVAL(3) 
        INTEGER PWQ1(3)
        INTEGER LEN(5)
        CHARACTER*10 STRVAL(5) 
        CHARACTER*10 P2QAL2(5)
        REAL PWQ3 
        DATA IVAL /2,4,8/
        DATA STRVAL /'for','do','loop','while','if'/ 
        DATA PWQ3 /6.5/

        LUN = 4
        CALL XRINIM (VBLOCK, XPRDIM, XABORT, ISTAT)
        CALL XRINTG (VBLOCK, 'PWQ2.QUAL1', 1, P2QAL1, COUNT, ISTAT)
        IF (P2QAL1 .NE. 1812) THEN
        WRITE (LUN, 100) P2QAL1
100     FORMAT (' pwq2.qual1 = ',I4,' in QUALFOR.')
        CALL XMPUT('Incorrect value passed into QUALFOR',
     +     'TSTQ-QUALFAIL',ISTAT)
        STOP
        ENDIF
        CALL XRSTR (VBLOCK, 'PWQ2.QUAL2', 5, P2QAL2, LEN, COUNT, ISTAT)
        DO 200 I=1,COUNT
        IF (P2QAL2(I) .NE. STRVAL(I)) THEN
        WRITE (LUN, 110) I, P2QAL2(I)
110     FORMAT (' pwq2.qual2(',I1,') = ',A,' in QUALFOR.')
        CALL XMPUT('Incorrect value passed into QUALFOR', 
     +     'TSTQ-QUALFAIL',ISTAT)
        STOP
        ENDIF
200     CONTINUE
        CALL XRINTG (VBLOCK, 'PWQ1', 3, PWQ1, COUNT, ISTAT)
        DO 300 I=1,COUNT
        IF (PWQ1(I) .NE. IVAL(I)) THEN
        WRITE (LUN, 210) I, PWQ1(I)
210     FORMAT (' pwq1(',I1,') = ',I3,' in QUALFOR.')
        CALL XMPUT('Incorrect value passed into QUALFOR', 
     +      'TSTQ-QUALFAIL',ISTAT)
        STOP
        ENDIF
300     CONTINUE
        CALL XQREAL (VBLOCK, 'PWQ3', 1, PWQ3, xupdat, ISTAT)
        P2QAL2(1) = 'ten'
        P2QAL2(2) = 'twenty'
        CALL XQSTR (VBLOCK, 'PWQ2.QUAL2', 2, P2QAL2, xupdat, ISTAT)
        CALL XQOUT (VBLOCK, ISTAT)
        STOP
        END

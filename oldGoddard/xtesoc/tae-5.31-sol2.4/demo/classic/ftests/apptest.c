/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



C   ***** DO NOT USE TABS OR FORMFEEDS IN ANY UNIX FORTRAN FILES *****
C
        IMPLICIT INTEGER (A-Z)
C
C       APPTEST is designed to verify the operations of the TAE parameter
C       passing mechanism between the TAE monitor and TAE applications.
C
C        APPTEST exercises all of the
C       subroutines available to an application program from the TAE
C       object module library. The subroutine packages utilized include:
C       
C               PACKAGE         TITLE
C               -----------     -------------------------------
C               XQ/XR           Parameter/Variable Manipulation
C               XT              Terminal I/O
C               XM              Message Logging
C               XI              Image File Access
C               XZ              Miscellaneous   
C
C
C       NOTE: The parameter passing routines are actually utilized in most of
C       the tests.
C
C  CHANGE LOG:
C
C       11-may-83       Changed DYNTURN subcommand to check parameter
C                       values against the set specified by DSETNO
C                       AFTER the dynamic tutor session...tjm
C       07-JUN-83       Check FILE type passing in IMAGE subcommand...peb
C       22-JUN-83       Fix for XRFORP buffer too small error...dm
C       18-JUL-83       Fix XRINTG call for $SWITCH param...peb
C       26-JUL-83       Add a new param, MULFIL for testing FILE parms...jtm
C       25-OCT-83       Change xr calling sequences...dm
C       26-OCT-83       Fix PROCNAM bug, declare VERS as character string...dm
C       31-oct-83       Fxi xqwrtb call...palm
C       14-nov-83       Fix 6-character variable names for FORTRAN-77
C                       compliance...palm
C       14-nov-83       Fix to allow in batch; of course, some subcommands
C                       require interactive session, but we want to be
C                       able to test what batch does when these subcommands
C                       execute...palm
C       14-nov-83       Fix version number COMMON and display...peb
C       16-nov-83       Replace "DO WHILE" with GOTO for portability...palm
C       18-nov-83       Fix bug in NAMED wherein an error code is always
C                       returned...nhe
C       01-dec-83       Unix related fixes for portability...dm
C                       Also delete image file access calls...dm
C       08-dec-83       Hard-coded version number (temporary)...dm
C       09-dec-83       Fix minor bugs related to subcommand TERMNL...dm
C       11-dec-83       Add 'STOP' statement at the end of main program.
C                       Also remove program name declaration ...dm
C       19-dec-83       Replace image file test calls ...dm
C       09-may-84       Fix bugs in pcheck routine...dm
C       10-may-84       Change APPTEST.DAT file name to lower case...dm
C       19-jan-87       PR1181: make file names lower case...palm
C       02-apr-88       Use C preprocessor now; DELETE ALL TABS, FORMFEEDS..ljn
C       27-mar-91       Call f_force_lower()...ljn
C
C
        COMMON /TTST21/IPRC             
        INTEGER VERLEN                  
        CHARACTER*31 VERS               
        COMMON /VERS/ VERLEN, VERS      

#include  "tstinc.fin"

C  LOCAL VARIABLES:

        INTEGER STATUS

        CHARACTER*132   STRING          
        CHARACTER*20    SUBCMD          
        CHARACTER*20    SESS            
        CHARACTER*20    RUNTYP          
        CHARACTER*80    PROCNM          
        CHARACTER*80    STDOUT(2)
        INTEGER         SOLEN(2)        
        INTEGER         SWITCH          
        CHARACTER*8     NAME
        INTEGER         PARAMS(xprdim)  
        CHARACTER*132   DUMMY
C
#include "datinc.fin"

C
C               *********************************************************
C               * PRINT VERSION, OPEN THE LOG FILE, GET TEST CONTROL    *
C               *                      PARAMETERS                       *
C               *********************************************************
C               *       CHECKS THE FOLLOWING ROUTINES:                  *
C               *                                                       *
C               *********************************************************
C

        WRITE (oplun,100) VERS(1:VERLEN)
100     FORMAT(//,' TAE APPLICATION SUBROUTINE PACKAGE TEST. VERSION ',
     1      A)

        CALL F_FORCE_LOWER (.TRUE.)

C
C               *************************************************
C               * INITIALIZE PARAMETER BLOCK (I.E. GET IT FROM  *
C               * TM, AND CHECK IMPLICIT PARAMETERS TO          *
C               * DETERMINE THE TEST TO EXECUTE.                *
C               *                                               *
C               *************************************************
C
        CALL XZINIT (PARAMS, xprdim, oplun,
     1  xcont, STATUS)                  
        CALL XTINIT (CRTTYP, LINES, COLS)       

        IF (STATUS .NE. xsucc) GO TO 1100
        NAME='_PROC'
        CALL XRSTR (PARAMS, NAME, 1, PROCNM,
     1      PROCLN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        NAME = '_STDOUT'
        CALL XRSTR (PARAMS, NAME, 2, STDOUT,
     1      SOLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        NAME = '_SUBCMD'
        CALL XRSTR (PARAMS, NAME, 1, SUBCMD,
     1      SUBLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        NAME = '$SESSION'
        CALL XRSTR (PARAMS, NAME, 1, SESS,
     1      SESLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        NAME = '$RUNTYPE'
        CALL XRSTR (PARAMS, NAME, 1, RUNTYP,
     1      RTLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        NAME = '$SWITCH'
        CALL XRINTG (PARAMS, NAME, 1, SWITCH,
     1      COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000

C
        IF (SUBCMD .EQ. 'START') THEN
C               ********************************************
C               *       OPEN DATA FILE AND WRITE VERSION   *
C               *       NUMBER TO IT.                      *
C               ********************************************
C
            OPEN (UNIT=loglun, FILE='apptest.dat',
     1    STATUS = 'NEW', ERR= 850)
            WRITE (loglun,100) VERS             
            CALL XZEXIT (xsucc, 'TST-SUCCESS')
        ELSE IF (SUBCMD .EQ. 'END') THEN
        
            CALL TSTCLS                                 
            CALL XZEXIT (xsucc, 'TST-SUCCESS')
        ENDIF

C               *************************************************
C               * IF NOT INTIALIZATION OR TERMINATION RUN, OPEN *
C               * LOG FILE AND POSITION TO THE END FOR ADDING   *
C               * MORE ENTRIES.                                 *
C               *************************************************

        OPEN (UNIT=loglun, FILE='apptest.dat',
     1     STATUS = 'OLD', ERR= 850)
99901   CONTINUE                                
            READ (UNIT=loglun, FMT=101, END = 150) DUMMY
101         FORMAT (A)  
        GOTO 99901
150     CONTINUE
C
C

        IF (SUBCMD .EQ. 'CHECK') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1, DSETNO, COUNT, STATUS) 
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 500) DSETNO
                WRITE (oplun, 500) DSETNO
500             FORMAT (1X,'PARAMETER ACQUISITION TEST ON DATASET ',I2)
                CALL PCHECK (PARAMS, DSETNO, oplun)
        ELSE IF (SUBCMD .EQ. 'FORMAT') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1, DSETNO, COUNT, STATUS) 
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 550) DSETNO
                WRITE (oplun, 550) DSETNO
550             FORMAT (1X,'PARAMETER FORMATTING TEST ON DATASET ',I2)
                CALL FORMAT (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'DYNKILL') THEN
                WRITE (loglun, 600)
                WRITE (oplun, 600)
600             FORMAT (1X, 'DYNAMIC TUTOR KILL TEST.' )
                CALL DNKILL (PARAMS)
        ELSE IF (SUBCMD .EQ. 'DYNTURN') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1,           
     1       DSETNO, COUNT, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 650) DSETNO
                WRITE (oplun, 650) DSETNO
650             FORMAT (1X,'DYNAMIC TUTOR TURNAROUND TEST ON DATASET ',
     1          I2)
                CALL DNTURN (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'SUBSET') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1,           
     1       DSETNO, COUNT, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 700) DSETNO
                WRITE (oplun, 700) DSETNO
700             FORMAT (1X,'DYNAMIC TUTOR SUBSETTING TEST ON DATASET ',
     1          I2)
                CALL DNSUB (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'BUILD') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1,           
     1       DSETNO, COUNT, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 725) DSETNO
                WRITE (oplun, 725) DSETNO
725             FORMAT (1X, 'DYNAMIC TUTOR BUILD TEST ON DATASET ',
     1          I2)
                CALL DNBLD (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'READ') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1,           
     1       DSETNO, COUNT, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 750) DSETNO
                WRITE (oplun, 750) DSETNO
750             FORMAT (1X,'PARAMETER SET DISK READ TEST ON DATASET ',
     1          I2)
                CALL DISKRD (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'UPDATE') THEN
                NAME = 'DSETNO'                 
                CALL XRINTG (PARAMS, NAME, 1,           
     1       DSETNO, COUNT, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                WRITE (loglun, 775) DSETNO
                WRITE (oplun, 775) DSETNO
775             FORMAT (1X,'PARAMETER SET DISK UPDATE TEST ON DATASET ',
     1          I2)
                CALL DISKUP (PARAMS, DSETNO)
        ELSE IF (SUBCMD .EQ. 'NAME') THEN
                WRITE (loglun, 780)
                WRITE (oplun, 780)
780             FORMAT (1X, 'NAMED PARAMETER ACQUISITION TEST.')
                CALL NAMED (PARAMS)
        ELSE IF (SUBCMD .EQ. 'IMAGE') THEN
                WRITE (loglun, 785)
                WRITE (oplun, 785)
785             FORMAT (1X, 'IMAGE FILE ACCESS TEST.')
                CALL IMAGE (PARAMS)
        ELSE IF (SUBCMD .EQ. 'TERMNL') THEN
                WRITE (loglun, 795)
                WRITE (oplun, 795)
795             FORMAT (1X, 'TERMINAL ROUTINE TEST.')
                CALL TERMNL
        ELSE
            WRITE (loglun, 200) SUBCMD
            WRITE (oplun, 200) SUBCMD
 200        FORMAT (1x, '***** ERROR: Unrecognized subcommand:',A)
            CALL XZEXIT (tbdsub, 'TST-BADSUB')
        ENDIF
        CALL XZEXIT (xsucc, 'TST-SUCCESS')

C
C                       ******************
C                       * ERROR HANDLING *
C                       ******************
C
C  NOTE:  When adding error messages, make sure the text contains
C         asterisks (*) in columns 1 - 5
C
C         for later text search by TSTCLS routine.
C
850     CONTINUE
        WRITE (oplun,851)
851     FORMAT(1X,'UNABLE TO OPEN OUTPUT LOG FILE')
        CALL XMPUT ('UNABLE TO OPEN OUTPUT LOG FILE', 
     1      'TST-NOLOGACC',CODE)
        CALL XZEXIT (tnolog, 'TST-NOLOGACC')                    
1000    CONTINUE
        CALL XRHERR (PARAMS, HOSTC)
        WRITE (oplun,1001) NAME, STATUS
        WRITE (loglun, 1001) NAME, STATUS,HOSTC
1001    FORMAT (1X, '***** UNABLE TO RECEIVE PARAMETER ',A,'. ERROR = ',
     1  I4/1x, 'HOST ERROR CODE = ',I6)
        CALL XZEXIT (tnorcv, 'TST-RCVFAIL')
1100    CONTINUE
        WRITE (oplun, 1101) STATUS
        WRITE (loglun, 1101) STATUS
1101    FORMAT (1X, '***** UNABLE TO RECEIVE PARM BLOCK FROM TM. ',
     1  'STATUS = ',I4)
        CALL XZEXIT (tnoblk, 'TST-BRCVFAIL')
        STOP
        END

        SUBROUTINE BADINT(PNAME,BADI,EXPINT,NUMINT)

#include "tstinc.fin"

C
C  PRINT ERROR MESSAGE FOR UNEXPECTED INTEGER VALUES.
C
C Arguments:
        CHARACTER*(*)   PNAME           
        INTEGER         BADI(1)         
        INTEGER         EXPINT(1)       
        INTEGER         NUMINT          

#include  "datinc.fin"

        WRITE (loglun,100) PNAME        
100     FORMAT (/,' ***** UNEXPECTED VALUE FOR ''',
     1  A, '''.')
        WRITE (loglun,200) (BADI(I),I=1,NUMINT)
200     FORMAT (' RECEIVED:  ',(6I10))
        WRITE (loglun,300) (EXPINT(I),I=1,NUMINT)
300     FORMAT (' EXPECTED:  ',(6I10))
        RETURN
        END

        SUBROUTINE BADREA(PNAME,BADR,EXPREA,NUMREA)

#include  "tstinc.fin"

C
C  PRINT ERROR MESSAGE FOR UNEXPECTED REAL VALUES.
C
C Arguments:
        CHARACTER*(*)   PNAME(1)        
        REAL            BADR(1)         
        REAL            EXPREA(1)       
        INTEGER         NUMREA          
C
C Local Variables:
C
#include  "datinc.fin"

        WRITE (loglun,100) PNAME        
100     FORMAT (/, ' ***** UNEXPECTED VALUE FOR ''',
     1  A,'''.')
        WRITE (loglun,200) (BADR(I),I=1,NUMREA)
200     FORMAT (' RECEIVED:  ',(4(2X,E15.8)))
        WRITE (loglun,300) (EXPREA(I),I=1,NUMREA)
300     FORMAT (' EXPECTED:  ',(4(X,E15.8)))
        RETURN
        END

        SUBROUTINE BADSTR(PNAME,BADS,EXPSTR,NUMSTR)

#include  "tstinc.fin"

C
C  PRINT AN ERROR MESSAGE FOR AN UNEXPECTED STRING VALUE.
C
C
C Arguments:
        CHARACTER *(*)  PNAME           
        CHARACTER*(*)   BADS (1)        
        CHARACTER*(*)   EXPSTR (1)      
        INTEGER         NUMSTR          

C
#include  "datinc.fin"
C
        WRITE (loglun,100) PNAME
100     FORMAT(/,' ***** UNEXPECTED VALUE FOR ''',
     1  A, '''.')
        WRITE (loglun,200)
200     FORMAT (' RECEIVED:')
        DO 301 I=1, NUMSTR
            WRITE (loglun,300) BADS(I)
300         FORMAT(8X,A)
301     CONTINUE
        WRITE (loglun,400)
400     FORMAT (' EXPECTED:')
        DO 501 I=1, NUMSTR
            WRITE (loglun,300) EXPSTR(I)
501     CONTINUE
        RETURN
        END

        SUBROUTINE CHKATR (PARAMS, PNAME, EXPCNT, EXPTYP)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"
C
C       This routine will obtain the attributes of the specified
C       parameter and compare them against the expected values.
C
C       *TBD: Currently, only COUNT and TYPE are checked by CHKATR
C       
C  Calling arguments:
        
        INTEGER PARAMS(1)
        CHARACTER*(*) PNAME
        INTEGER EXPCNT
        INTEGER EXPTYP
C
C  Local variables:
C
        LOGICAL DEFALT
        INTEGER TYPE

#include  "datinc.fin"
        
        CALL XRATTR (PARAMS,PNAME,TYPE,COUNT,DEFALT,ACCESS,STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 100) PNAME, STATUS
100         FORMAT (1X,
     1      '***** Unable to obtain attributes of parameter ''',
     1      A, '''. Status = ', I4)
            CALL XZEXIT (tnoatt, 'TST-NOATTR')
        ENDIF
        IF (COUNT .NE. EXPCNT) THEN
            WRITE (loglun, 200) COUNT, EXPCNT, PNAME
200         FORMAT (1X, '***** No. of values from ''XRATTR''(',
     1      I2,') doesn''t match expected (',I2,') for ''',A,'''.')
            CALL XZEXIT (tattct, 'TST-BADCOUNT')
        ENDIF
        IF (TYPE .NE. EXPTYP) THEN
            WRITE (loglun, 300) TYPE, EXPTYP, PNAME
300         FORMAT (1X, '***** type attribute from ''XRATTR''(',
     1      I2,') doesn''t match expected (',I2,') for ''',A,'''.')
            CALL XZEXIT (tattct, 'TST-BADTYPE')
        ENDIF
        RETURN
        END

        SUBROUTINE DNBLD (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)
C
C       This routine initializes a parameter block with nothing in it,
C       sends the block to TM and waits for a new block back. The new block
C       is then compared against the specified data set. So, in this test,
C       the block is built from scratch in dynamic tutor.
C
C  Calling arguments:

        INTEGER PARAMS (1)
        INTEGER DSETNO

        CALL XQINI (PARAMS, xprdim, xabort)
        CALL XQDYNP ('apptest-BUILD', PARAMS, xfulpd, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 150) STATUS
150         FORMAT (1X,
     1      '***** Attempt to send parameter block to TM failed.',
     1      I4)
            CALL XZEXIT (tnosnd, 'TST-BSNDFAIL')
        ENDIF
        CALL XRINIM (PARAMS, xprdim, xcont, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 200) STATUS
200         FORMAT (1X,'***** Unable to receive parm block from TM.',
     1  ' Status = ',
     1      I4)
            CALL XZEXIT (tnorcv, 'TST-BRCVFAIL')
        ENDIF
        CALL PCHECK (PARAMS, DSETNO)
        RETURN
        END

        SUBROUTINE DNKILL (PARAMS)
        IMPLICIT INTEGER (A-Z)
C
C       This routine is called when the 'DYNKILL' subcommand
C       is found in a parameter block rec'd from TM. A block of
C       parameters is created, sent to TM and a kill command is
C       expected.
C
C  Calling arguments:

        INTEGER PARAMS (1)

#include  "tstinc.fin"

        CALL XQINI (PARAMS, xprdim, xabort)
        CALL XQDYNP ('apptest-DYNKILL', PARAMS, xfulpd, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 150) STATUS
150         FORMAT (1X,
     1      '***** Attempt to send parameter block to TM failed.',I4)
            CALL XZEXIT (tnosnd, 'TST-BSNDFAIL')
        ENDIF
        CALL XRINIM (PARAMS, xprdim, xcont, STATUS)
        IF (STATUS .NE. xkilld) THEN
            WRITE (loglun, 200) STATUS
200         FORMAT (1X,'***** Expected ''xkilld'' status code from ',
     1  'XRINIM. Received: ',
     1      I4)
            CALL XZEXIT (tnokil, 'TST-NOKILL')
        ENDIF
        RETURN
        END

        SUBROUTINE DNSUB (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

C
C       This routine is called when the 'SUBSET' subcommand
C       is rec'd in a parameter block from TM. The processing involves
C       building a parameter block from scratch, filling it with
C       a subset of the parameters defined for the process.
C       The block is sent to TM, dynamic tutor is entered, and the block
C       is sent back unchanged. The values in the block are then verified.
C
C  Calling arguments:
        INTEGER PARAMS (1)
        INTEGER DSETNO
C
C  Local variables
C
        CHARACTER*8  PNAME
        INTEGER STRLEN(strdim)  

#include  "datinc.fin"
                

        CALL XQINI (PARAMS, xprdim, xcont)
        CALL XQREAL (PARAMS, 'MULREAL', rldim, EXMULR (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 200) STATUS
200         FORMAT (1X, '***** Call to ''XQREAL'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDREAL')
        ENDIF
        CALL XQSTR (PARAMS, 'MULSTRNG', strdim, EXMULS (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 300) STATUS
300         FORMAT (1X, '***** Call to ''XQSTR'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDSTR')
        ENDIF
        CALL XQINTG (PARAMS, 'DSETNO', 1, DSETNO,
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 400) STATUS
400         FORMAT (1X, '***** Call to ''XQINTG'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDINT')
        ENDIF   
        CALL XQDYNP ('apptest-SUBSET', PARAMS, xsubpd, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 500) STATUS
500         FORMAT (1X,
     1      '***** Attempt to send parameter block to TM failed.',I4)
            CALL XZEXIT (tnosnd, 'TST-BSNDFAIL')
        ENDIF   
        CALL XRINIM (PARAMS, xprdim, xabort, STATUS)

C               * NOW CHECK VALUES RETURNED *
C
        PNAME = 'MULREAL'
        CALL CHKATR (PARAMS, PNAME, rldim, xreal)
        CALL XRREAL(PARAMS, PNAME, rldim, MULREA,
     1      COUNT, STATUS)                              
        IF (STATUS .NE. xsucc) GO TO 100

        DO 55 I=1, rldim
            IF (MULREA(I).NE.EXMULR(I,DSETNO)) THEN
                CALL BADREA('MULREAL', MULREA, EXMULR(1,DSETNO),
     1              rldim)
                GO TO 60
            ENDIF
55      CONTINUE
60      CONTINUE

        PNAME = 'MULSTRNG'
        CALL CHKATR (PARAMS, PNAME, strdim, xstrng)
        CALL XRSTR (PARAMS, PNAME, strdim,
     1       MULSTR, STRLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 100
        DO 65 I=1, strdim                                       
            IF (MULSTR(I) .NE. EXMULS(I,DSETNO)) THEN
                CALL BADSTR('MULSTRNG',MULSTR (I),
     1  EXMULS(I,DSETNO), strdim)
                GO TO 70
            ENDIF
65      CONTINUE
 70     CONTINUE
        RETURN
100     CONTINUE
        WRITE (loglun, 1000) PNAME, STATUS
1000    FORMAT (1X, '***** Unable to receive parameter ''',A,
     1  '''. Error = ', I4)
        RETURN
        END

        SUBROUTINE DISKRD (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

C       This routine reads a parameter block from disk and
C       compares it to an expected set of values (DSETNO).
C
C  Calling arguments:
        INTEGER PARAMS (1)
        INTEGER DSETNO

#include  "datinc.fin"

        CALL XRRDB (SAVBLK, parlun, PARAMS, xprdim, xabort, STATUS)
        CALL PCHECK (PARAMS, DSETNO)
        RETURN
        END

        SUBROUTINE DISKUP (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"
C       This routine reads a parameter block from disk, updates
C       values in the block, and writes the block back to disk.
C
C  Calling arguments:

        INTEGER PARAMS (1)
        INTEGER DSETNO

#include  "datinc.fin"

        CALL XRRDB (SAVBLK, parlun, PARAMS, xprdim, xabort, STATUS)

        CALL XQINTG (PARAMS, 'MULINT', intdim, EXMULI (1, DSETNO),
     1       xupdat, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 100) STATUS
100         FORMAT (1X, '***** Call to ''XQINTG'' to update a ',
     1      'parameter failed. Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDINT')
        ENDIF
        CALL XQREAL (PARAMS, 'MULREAL', rldim, EXMULR (1, DSETNO),
     1       xupdat, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 200) STATUS
200         FORMAT (1X, '***** Call to ''XQREAL'' to update a ',
     1      'parameter failed. Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDREAL')
        ENDIF
        CALL XQSTR (PARAMS, 'MULSTRNG', strdim, EXMULS (1, DSETNO),
     1       xupdat, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 300) STATUS
300         FORMAT (1X, '***** Call to ''XQSTR'' to update a ',
     1      'parameter failed. Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDSTR')
        ENDIF
        CALL XQINTG (PARAMS, 'DSETNO', 1, DSETNO,
     1       xupdat, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 400) STATUS
400         FORMAT (1X, '***** Call to ''XQINTG'' to update a ',
     1      'parameter failed. Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDSTR')
        ENDIF   
        CALL XQWRTB (SAVBLK, parlun, PARAMS, STATUS)
        RETURN
        END


        SUBROUTINE DNTURN (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

C
C       This routine is called when the 'DYNTURN' subcommand
C       is rec'd in a parameter block from TM. The processing involves
C       building a parameter block from scratch, filling it with
C       values consistent with the DSETNO value rec'd from TM.
C       The block is sent to TM, dynamic tutor is entered, and the block
C       is sent back to APPTEST. The values in the block are then compared
C       against the value of DSETNO received back from TM.
C
C  Calling arguments:
        INTEGER PARAMS (1)
        INTEGER DSETNO

        CHARACTER*8 NAME

#include  "datinc.fin"

        CALL XQINI (PARAMS, xprdim, xcont)
        CALL XQINTG (PARAMS, 'MULINT', intdim, EXMULI (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 100) STATUS
100         FORMAT (1X, '***** Call to ''XQINTG'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDINT')
        ENDIF
        CALL XQREAL (PARAMS, 'MULREAL', rldim, EXMULR (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 200) STATUS
200         FORMAT (1X, '***** Call to ''XQREAL'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDREAL')
        ENDIF
        CALL XQSTR (PARAMS, 'MULSTRNG', strdim, EXMULS (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 300) STATUS
300         FORMAT (1X, '***** Call to ''XQSTR'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDSTR')
        ENDIF
        CALL XQFILE (PARAMS, 'MULFIL', fildim, EXMULF (1, DSETNO),
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 350) STATUS
350         FORMAT (1X, '***** Call to ''XQFILE'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDFILE')
        ENDIF
        CALL XQINTG (PARAMS, 'DSETNO', 1, DSETNO,
     1       xadd, STATUS)
        IF (STATUS .NE. xsucc)  THEN
            WRITE (loglun, 400) STATUS
400         FORMAT (1X, '***** Call to ''XQINTG'' to add to a ',
     1      'parameter block failed. Status = ',I4)
            CALL XZEXIT (tnoadd, 'TST-ADDINT')
        ENDIF   
        CALL XQDYNP ('apptest-DYNTURN', PARAMS, xsubpd, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 500) STATUS
500         FORMAT (1X,
     1      '***** Attempt to send parameter block to TM failed.',I4)
            CALL XZEXIT (tnosnd, 'TST-BSNDFAIL')
        ENDIF   
        CALL XRINIM (PARAMS, xprdim, xabort, STATUS)
        NAME = 'DSETNO'
        CALL XRINTG (PARAMS, NAME, 1,   
     1       DSETNO, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 1000
        CALL PCHECK (PARAMS, DSETNO)
        RETURN
1000    CONTINUE
        WRITE (oplun,1001) NAME, STATUS
        WRITE (loglun, 1001) NAME, STATUS
1001    FORMAT (1X,'***** UNABLE TO RECEIVE PARAMETER ',A,'. ERROR = ',
     1  I4)
        CALL XZEXIT (tnorcv, 'TST-RCVFAIL')
        END

        SUBROUTINE IMAGE (PARAMS)
        IMPLICIT INTEGER (A-Z)
        
#include  "tstinc.fin"
C
C Calling arguments:
C
        INTEGER PARAMS(1)

C
C Local variables:
C
        INTEGER IFCB (xifdim)
        INTEGER BUFFER (linsiz/4)       
        CHARACTER*8 SKEY
        CHARACTER*9 PNAME
        CHARACTER*80 TAEFIL, HFILE

#include  "datinc.fin"

        PNAME = 'IMGFILE'
        CALL XRFILE (PARAMS, PNAME, 1, TAEFIL, TAELEN,
     1          HFILE, HLEN, NUMSTR, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 50)
            WRITE (oplun, 50)
50          FORMAT (1X, 'Unable to access image file name parameter')
            CALL XZEXIT (timwrt, 'TST-IWRTFAIL')
        ENDIF
        CALL XIOPOU (IFCB, LUN, HFILE, xcs,
     1  imchan, imline, linsiz, imlabl, labsiz, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 100) STATUS
            WRITE (oplun, 100) STATUS
100         FORMAT (1X, '***** Can''t open image file for output.',
     1      ' Status = ',I4)
            CALL XZEXIT (timwrt, 'TST-IWRTFAIL')
        ENDIF
C
C       ************************************************************
C       * WRITE A 7 BAND/ 10 LINE PER BAND IMAGE TO THE IMAGE FILE *
C       ************************************************************
C
        CALL XIWRLB (IFCB, EXPLAB, 1, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 150) STATUS
            WRITE (oplun, 150) STATUS
150         FORMAT (1X, '***** Can''t write image file label.',
     1      ' Status = ',I4)
            CALL XZEXIT (tilblw, 'TST-ILABWRIT')
        ENDIF
        DO 170 CHAN=1,7
            DO 160 LINE=1,10
                BUFFER(1) = CHAN
                BUFFER(2) = LINE
                BUFFER(linsiz/8) = LINE
                BUFFER(linsiz/4) = LINE
                CALL XIWRIT (IFCB, BUFFER, CHAN, LINE, 1, STATUS)
                IF (STATUS .NE. xsucc) GO TO 1000
                CALL XIWAIT (IFCB, STATUS)
                IF (STATUS .NE. xsucc) GOTO 1100
160         CONTINUE
170     CONTINUE
        CALL XICLSE (IFCB, xsave)
C
C       **********************************************
C       * NOW OPEN THE FILE AND READ ALL THE RECORDS *
C       **********************************************
C
        CALL XIOPIN (IFCB, imglun, HFILE, xinput, STATUS)
        CALL XIRDLB (IFCB, LABBUF, 1, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 175) STATUS
            WRITE (oplun, 175) STATUS
175         FORMAT (1X, '***** Can''t read image file label.',
     1      ' Status = ',I4)
            CALL XZEXIT (tilblw, 'TST-ILABREAD')
        ENDIF
        IF (LABBUF .NE. EXPLAB) THEN
            WRITE (loglun, 200) STATUS
            WRITE (oplun, 200) STATUS
200         FORMAT (1X, '***** Image file label mismatch.')
            CALL XZEXIT (tilblw, 'TST-ILABMTCH')
        ENDIF
        DO 190 CHAN=1,7
            DO 180 LINE=1,10
                CALL XIREAD (IFCB, BUFFER, CHAN, LINE, 1, STATUS)
                IF (STATUS .NE. xsucc) GOTO 1200
                CALL XIWAIT (IFCB, STATUS)
                IF (STATUS .NE. xsucc) GOTO 1300
                IF ((BUFFER(1) .NE. CHAN) .OR.
     1      (BUFFER(2) .NE. LINE) .OR.
     1      (BUFFER(linsiz/8) .NE. LINE) .OR.
     2      (BUFFER(linsiz/4) .NE. LINE)) THEN
                   GOTO 1400
                ENDIF
180         CONTINUE
190     CONTINUE
        CALL XICLSE (IFCB, xdel)
        RETURN
1000    CONTINUE
        WRITE (loglun, 1001)
        WRITE (oplun, 1001)
1001    FORMAT (1X,
     1   '***** Error writing an image line to image file (XIWRIT).')
        SKEY = 'TST-IMGWRITE'
        GOTO 2000
1100    CONTINUE
        WRITE (loglun, 1101)
        WRITE (oplun, 1101)
1101    FORMAT (1X,
     1   '***** Error waiting for image file write (XIWRIT).')
        SKEY = 'TST-IMGWWAIT'
        GOTO 2000
1200    CONTINUE
        WRITE (loglun, 1201)
        WRITE (oplun, 1201)
1201    FORMAT (1X, '***** Error reading image file (XIREAD).')
        SKEY = 'TST-IMGREAD'
        GOTO 2000
1300    CONTINUE
        WRITE (loglun, 1301)
        WRITE (oplun, 1301)
1301    FORMAT (1X,'***** Error waiting for image file read (XIWAIT).')
        SKEY = 'TST-IMGRWAIT'
        GOTO 2000
1400    CONTINUE
        WRITE (loglun, 1401)
        WRITE (oplun, 1401)
1401    FORMAT (1X, 
     1  '***** Image data written doesn''t match data read.')
        CALL XZEXIT (tdata, 'TST-NOMATCH')
2000    CONTINUE
        CALL XIHERR (IFCB, HCODE)
        WRITE (loglun, 2001) HCODE
2001    FORMAT (1X, '   Host error code = ',I5)
        CALL XZEXIT (tilacc, SKEY)
        END

        SUBROUTINE NAMED (PARAMS, STDOUT)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

C       This routine is called when the 'NAME' subcommand is encountered.
C       Named will read a parameter from the parameter block which is a
C       named parameter. THe value of the parameter is compared against an
C       expected value and then set to a new value. The block is then sent
C       back to TM.
C
C  Calling arguments:
        INTEGER PARAMS (1)
C
C
        CHARACTER*40 NAMEST

#include  "datinc.fin"

        CALL XRSTR (PARAMS, 'S', 1, NAMEST, STRLEN, STRCNT, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 100) STATUS
            WRITE (oplun, 100) STATUS
100         FORMAT (1X, '***** Unable to receive parameter ''S''.',
     1   ' Error = ',I4)
            CALL XZEXIT (tnorcv, 'TST-RCVFAIL')
        ENDIF
        IF (NAMEST .NE. EXNAME) THEN
            CALL BADSTR ('S',NAMEST, EXNAME, 1)
            CALL XZEXIT (tdata, 'TST-NAMEMTCH')
        ENDIF
        CALL XQSTR (PARAMS, 'S', 1, RETSTR, xupdat, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 200) STATUS
            WRITE (oplun, 200) STATUS
200         FORMAT (1X,'***** Can''t update parameter ''S'' in block.',
     1   ' Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDSTR')
        ENDIF
        CALL XQINTG (PARAMS, '$SWITCH', 1, SWVAL, xupdat, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 250) STATUS
            WRITE (oplun, 250) STATUS
250         FORMAT (1X, 
     1      '***** Can''t update global ''$SWITCH'' in block.',
     1      ' Status = ',I4)
            CALL XZEXIT (tnoupd, 'TST-UPDSWTCH')
        ENDIF
        CALL XQOUT (PARAMS, STATUS)
        IF (STATUS .NE. xsucc) THEN
            WRITE (loglun, 300) STATUS
            WRITE (oplun, 300) STATUS
300         FORMAT (1X, '***** Attempt to send parameter block ',
     1   'to TM failed.',
     1   ' Status = ',I4)
            CALL XZEXIT (tnosnd, 'TST-BSNDFAIL')
        ENDIF
        RETURN
        END

        SUBROUTINE PCHECK (PARAMS, EXDSET)
        IMPLICIT INTEGER (A-Z)
        
#include  "tstinc.fin"

C
C       This routine is called during any test which requires a
C       comparison of the parameters received from TM and the
C       hardcoded expected values (see below).
C
C  Calling arguments:
        
        INTEGER PARAMS(1)               
        INTEGER EXDSET
C
C  Local arguments:
C
        INTEGER STRLEN(strdim)  
        CHARACTER*8  PNAME
        CHARACTER*30    HOSTF (fildim)
        INTEGER FILLEN(fildim), HLEN(fildim)
#include  "datinc.fin"
C
C               *************************************************
C               *    GET AND CHECK EACH EXPECTED PARAMETER      *
C               *               - MULINT, a multivalue          *
C               *                 integer value.                *
C               *               - MULREAL, A multivalue real    *
C               *                 parameter.                    *
C               *               - MULSTRNG, a multivalue string *
C               *                 parameter.                    *
C               *               - MULFIL, a multivalue filespec *
C               *                                               *
C               *************************************************
C
C

        PNAME = 'DSETNO'
        CALL CHKATR (PARAMS, PNAME, 1, xintg)
        CALL XRINTG (PARAMS, PNAME, 1,
     1       DSETNO, COUNT, STATUS)

        IF (STATUS .NE. xsucc) GO TO 100
        IF (EXDSET .NE. DSETNO) THEN
            CALL BADINT ('DSETNO', DSETNO, EXDSET, 1)
            GOTO 50
        ENDIF
        PNAME = 'MULINT'
        CALL CHKATR (PARAMS, PNAME, intdim, xintg)
        CALL XRINTG (PARAMS, PNAME, intdim,
     1       MULINT, COUNT, STATUS)             
        IF (STATUS .NE. xsucc) GO TO 100

        DO 45 I = 1, intdim
            IF (MULINT(I).NE.EXMULI(I,DSETNO)) THEN
                CALL BADINT('MULINT',MULINT,EXMULI(1,DSETNO), intdim)
                GO TO 50
            ENDIF
 45     CONTINUE
 50     CONTINUE

        PNAME = 'MULREAL'
        CALL CHKATR (PARAMS, PNAME, rldim, xreal)
        CALL XRREAL(PARAMS, PNAME, rldim, MULREA,
     1      COUNT, STATUS)                              
        IF (STATUS .NE. xsucc) GO TO 100

        DO 55 I=1, rldim
            IF (MULREA(I).NE.EXMULR(I,DSETNO)) THEN
                CALL BADREA('MULREAL', MULREA, EXMULR(1,DSETNO),
     1              rldim)
                GO TO 60
            ENDIF
 55     CONTINUE
 60     CONTINUE

        PNAME = 'MULSTRNG'
        CALL CHKATR (PARAMS, PNAME, strdim, xstrng)
        CALL XRSTR (PARAMS, PNAME, strdim,
     1       MULSTR, STRLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 100
        DO 65 I=1, strdim                                       
            IF (MULSTR(I) .NE. EXMULS(I,DSETNO)) THEN
                CALL BADSTR('MULSTRNG',MULSTR (1),
     1          EXMULS (1,DSETNO), strdim)
                GO TO 70
            ENDIF
 65     CONTINUE
 70     CONTINUE
        PNAME = 'MULFIL'
        CALL CHKATR (PARAMS, PNAME, fildim, xfile)
        CALL XRFILE (PARAMS, PNAME, fildim,
     1       MULFIL, FILLEN, HOSTF, HLEN, COUNT, STATUS)
        IF (STATUS .NE. xsucc) GO TO 100
        DO 75 I=1, fildim                                       
            IF (MULFIL(I) .NE. EXMULF(I,DSETNO)) THEN
                CALL BADSTR('MULFIL',MULFIL (1),
     1          EXMULF (1,DSETNO), fildim)
                GO TO 80
            ENDIF
 75     CONTINUE

 80     CONTINUE
        RETURN
100     CONTINUE
        WRITE (loglun, 1000) PNAME, STATUS
1000    FORMAT (1X, '*****Unable to access parameter ''',A, 
     1          '''.  Error = ', I4)
        RETURN
        END

        SUBROUTINE FORMAT (PARAMS, DSETNO)
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"
C
C       This routine verifies the correct operation of the
C       TAE FORTRAN callable parameter formatting routines.
C
C  Calling arguments:

        INTEGER PARAMS(1)               
        INTEGER DSETNO

C
C  Local Variables
C
        LOGICAL EOF
        CHARACTER*75 PLINE

#include  "datinc.fin"

        CALL XRFINI (PARAMS)                    
        STATUS = xsucc
99902   IF(.NOT.(STATUS .EQ. xsucc)) GOTO 99903 
            CALL XRFORP (PARAMS, 74, PLINE, STATUS)
            IF (STATUS .EQ.xsucc) THEN
                WRITE (loglun, 100) PLINE
100             FORMAT (5X, A)
            ELSE IF (STATUS .EQ. xeof) THEN
                RETURN
            ELSE IF (STATUS .EQ. xnull) THEN
                WRITE (loglun, 200)
200             FORMAT (5x, '***** Parameter or global or local ',
     2  'variable too long',
     1  ' to format into displayable form.')
                CALL XZEXIT (tplong, 'TST-PTOOLONG')
            ELSE
                WRITE (loglun, 300) STATUS
300             FORMAT (1X,
     1  '***** Unrecognized error code from ''XRFORP''',
     1  '. Status = ', I4)
                CALL XZEXIT (tplong, 'TST-XRFORPER')
                RETURN
            ENDIF
        GOTO 99902
99903   CONTINUE
        RETURN
        END

        SUBROUTINE TERMNL
C
C       This routine exercises the TAE terminal access routines.
C
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

        CALL XTINIT (TYPE, LINES, COLS) 
        CALL XTCLR 
        CALL XTOUT (8, 30 , ' THIS  IS  A')
        CALL XTOUT (9, 30,  ' TEST OF THE')
        CALL XTOUT (10, 30, ' TAE TERMINAL')
        CALL XTOUT (11, 30, '   ROUTINES')
        CALL XTBELL
        DO 20 I=27,45
            CALL XTPOS (6, I)
            CALL XTWRIT ('X', xccnul)
 20     CONTINUE
        CALL XTBELL
        DO 30 I=27,45
            CALL XTPOS (13, I)
            CALL XTWRIT ('X', xccnul)
 30     CONTINUE
        CALL XTBELL
        DO 40 I=7,12
            CALL XTPOS (I, 27)
            CALL XTWRIT ('XX', xccnul)
 40     CONTINUE
        CALL XTBELL
        DO 50 I=7,12
            CALL XTPOS (I, 44)
            CALL xtwrit ('XX', xccnul)
 50     CONTINUE
        CALL XTBELL
        CALL XTBELL
        RETURN
        END

        SUBROUTINE TSTCLS
        IMPLICIT INTEGER (A-Z)

#include  "tstinc.fin"

C
C  Note on the screen and in the log, whether the Parameter Check
C  Self Test failed or passed.  This routine is meant to be called
C  after all iterations of the test are completed.
C  This routine does a text search for certain keywords indicating
C  failure.
C
C  Local variables:

        LOGICAL         FAILED                  
        CHARACTER*132   STRING                  
        INTEGER         STATUS
        CHARACTER*20    TOKEN

#include  "datinc.fin"

        OPEN (UNIT=loglun, FILE='apptest.dat', STATUS = 'OLD',
     1    ERR= 800)
        REWIND(loglun)

        FAILED = .FALSE.                        
99904   CONTINUE                                
            READ (UNIT=loglun, FMT = 300, IOSTAT=STATUS,
     1    ERR=850, END=50) STRING       
300         FORMAT (A)
            IF (STRING (2:4) .EQ. '***') THEN
                FAILED = .TRUE.
                GO TO 50
            ENDIF
        GOTO 99904
 50     CONTINUE
        CALL XTBELL                             
        IF (FAILED) THEN                        
            CALL XTBELL
            WRITE (oplun,100)
100         FORMAT(//,1X,'*** TAETEST UNSUCCESSFUL ***')
        ELSE
            WRITE (oplun,200)
200         FORMAT(//,1X,'TAETEST COMPLETED SUCCESSFULLY')
        ENDIF
        RETURN

C
C  Error handling.
C
800     CONTINUE
        WRITE (oplun,801)
801     FORMAT (1X, 'COULDN''T OPEN LOG FILE WHILE CLOSING OUT TEST')
        CALL XZEXIT (topen, 'TST-LOGOPEN')
850     CONTINUE
        WRITE (oplun,851)
851     FORMAT(1X,'LOG FILE READ ERROR WHILE CLOSING OUT TEST')
        CALL XZEXIT (tread, 'TST-LOGREAD')
        END

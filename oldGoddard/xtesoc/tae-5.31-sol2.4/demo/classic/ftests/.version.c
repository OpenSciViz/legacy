/*
 *	This module provides the version number for fortran programs.
 */

#include "forstr.inp"

	struct  {
		int	verlen;
		char	versn[32];
		} FOR_NAME(vers) = {sizeof(VERSION)-1, VERSION};


/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* CAPPTEST.C      (C version of APPTEST)
 *
 * APPTEST IS DESIGNED TO VERIFY THE OPERATIONS OF THE TAE PARAMETER PASSING
 * MECHANISM BETWEEN THE TAE MONITOR AND TAE APPLICATIONS.
 *
 * APPTEST EXERCISES ALL OF THE SUBROUTINES AVAILABLE TO AN APPLICATION
 * PROGRAM FROM THE TAE OBJECT MODULE LIBRARY. THE SUBROUTINE PACKAGES
 * UTILIZED INCLUDE: 
 *
 *
 *	PACKAGE		TITLE
 *	-------		-----
 * 	p_/q_		PARAMETER/VARIABLE MANIPULATION
 *	t_		TERMINAL I/O
 *	m_		MESSAGE LOGGING
 *	z_		MISCELLANEOUS
 *
 * 
 * NOTE:  THE PARAMETER PASSING ROUTINES ARE ACTUALLY UTILIZED IN MOST
 *      OF THE TESTS. 
 *
 *   Change Log:
 *
 *      12-aug-86    Initial...nci  (JR did most of the conversion in '85,
 *                                   I just debugged it)
 *	30-jan-87    Fixed p_inim calling seq for dimension...peb
 *	10-nov-88    #ifdef sun for epsilon checking...ljn
 *	26-nov-91    For UNIX, no return status from wait_hold;
 *		     added \n to a message; need bigger MAXSTRING...ljn
 *	29-oct-92    Added include of math.h do get fabs...krW
 *	15-dec-93    Updated vrsion[] to an IMPORT TEXT; eliminate empty
 *		     declaration warnings...dag
 */

#include	"taeconf.inp"
#include	"parblk.inc"
#include        "terminc.inc"
#include	"tstinc.inc"
#include	"datinc.inc"
#include	<stdio.h>
#include	<math.h>	/* get the definition of fabs */

IMPORT TEXT	vrsion[];	/* program version number */
FILE		*fp,*fp1;	/* pointer to standard output file */
FILE            *z_init();


int		status;
char		string[132];	/* string buffer    		   */
struct VARIABLE	*p_find(),	
		*subcmd,	/* _SUBCMD implicit parameter pointer       */
		*sess,		/* $SESSION implicit parameter pointer 	    */
		*runtyp,	/* $RUNTYPE implicit parameter pointer      */
		*procnm,	/* _PROC implicit parameter pointer         */
		*std_out,	/* _STDOUT implicit parameter pointer  	    */
		*usestd,	/* usestd keyword parameter pointer         */
		*switch_wd,	/* $SWITCH implicit parameter pointer	    */
		*dsetno,	/* DSETNO implicit parameter pointer	    */
		*mulint,	/* MULINT parameter pointer		    */
		*mulreal,	/* MULREAL parameter pointer		    */
		*mulstr,	/* MULSTRNG parameter pointer		    */
		*mulfil;	/* MULFIL parameter pointer		    */


FUNINT		lun;		/* Logical unit number			    */
	

int		solen[1];
int		uselen;
char		name[8];
struct	PARBLK	params;	        /* space for parameter block	   */
char		dummy[132];
CODE		*hcode;		/* host error code		   */
int		iswitch;	/* $SWITCH implicit parameter	   */


	/*********************************************************
	* INITIALIZE PARAMETER BLOCK (I.E. GET IT FROM TM), AND  *
	* CHECK IMPLICIT PARAMETERS TO DETERMINE THE TEST TO     *
	* EXECUTE. 						 *
	*********************************************************/


FUNCTION main()
{
    f_force_lower(TRUE);
    fp=z_init(&params, P_CONT); /* initializes p_ and t_ packages*/
    if (fp==NULL) goto L3; /* abort if error in initializing */
    fprintf(fp,
                "\n\tTAE APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s\n\n",
		vrsion);	

    procnm = p_find (&params,"_PROC");
    strcpy (name, "_PROC");
    if (procnm==NULL) goto L2;
    std_out = p_find (&params,"_STDOUT");
    strcpy (name, "_STDOUT");
    if (std_out==NULL) goto L2;
    subcmd = p_find (&params,"_SUBCMD");
    strcpy (name, "_SUBCMD");
    if (subcmd==NULL) goto L2;
    sess = p_find (&params,"$SESSION");
    strcpy (name, "$SESSION");
    if (sess==NULL) goto L2;
    runtyp = p_find (&params,"$RUNTYPE");
    strcpy (name, "$RUNTYPE");
    if (runtyp==NULL) goto L2;
    switch_wd = p_find (&params,"$SWITCH");
    strcpy (name, "$SWITCH");
    if (switch_wd==NULL) goto L2;
    usestd = p_find (&params,"USESTD");
    strcpy (name, "USESTD");
    if (usestd==NULL) goto L2;
	
    if(strcmp ("START",SVAL(*subcmd,0)) == 0) 
	{
	        
	/********************************************************
        *	OPEN DATA FILE AND WRITE VERSION NUMBER		*
	*	TO IT.						*
	*********************************************************/

	fp1=fopen("CAPPTEST.DAT", "w");
	if (fp1==NULL) goto L1;
        fprintf(fp1,
		"\n\tTAE APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s",
	        vrsion);
	
	fprintf (fp1,"\n+++++ CAPPTEST BEGIN"); /* Write begin sentinel */
        fprintf (fp1,"\n----- CAPPTEST EXIT"); /* Write exit sentinel */
       	z_exit (1,"");
	}

    else if (strcmp ("END",SVAL(*subcmd,0)) == 0) 
	{
        /* No begin or exit sentinels for -END */    	
	    
	tstcls();	/* Perform test completion processing */
	z_exit (1,"");
	}
    else if (strcmp ("POLL",SVAL(*subcmd,0)) ==0) 
	{
	/* No begin or exit sentinels for -POLL */    
	tstexs();    /* Wait for file to exist */
    	z_exit (1,"");
	}

	/********************************************************
	* IF NOT INITIALIZATION OR TERMINATION RUN, OPEN LOG	*
	* FILE AND POSITION TO THE END FOR ADDING MORE ENTRIES. *
	********************************************************/

	fp1=fopen ("CAPPTEST.DAT", "a"); /*open and position to the end of file */
	if (fp1==NULL) goto L1;

	fprintf (fp1,
		"\n\tTAE APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s\n\n",
		vrsion);
	fprintf (fp1,
		"\n+++++ CAPPTEST BEGIN"); /* WRITE BEGIN SENTINEL TO FILE */	
	fprintf (fp,
		"\n+++++ CAPPTEST BEGIN"); /* WRITE BEGIN SENTINEL */	
 
	if (strcmp("CHECK",SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find (&params,"DSETNO"); /* First get dataset number */
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
	    	    "\nPARAMETER ACQUISITION TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    fprintf (fp,
	    	    "\nPARAMETER ACQUISITION TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    pcheck (--IVAL(*dsetno,0));
	}
	else if (strcmp("FORMAT",SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER FORMATTING TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER FORMATTING TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    format(--IVAL(*dsetno,0));
	}
	else if (strcmp("DYNKILL", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,"\nDYNAMIC TUTOR KILL TEST");
	    fprintf (fp,"\nDYNAMIC TUTOR KILL TEST");
	    dnkill();
	}
	else if (strcmp("DYNTURN", SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First get dataset number*/
            strcpy (name, "DSETNO");
            if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR TURNAROUND TEST ON DATATEST %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR TURNAROUND TEST ON DATATEST %d",
		    IVAL(*dsetno,0));
	    dnturn(--IVAL(*dsetno,0));
	}
	else if (strcmp("SUBSET", SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR SUBSETTING TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR SUBSETTING TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    dnsub(--IVAL(*dsetno,0));
	}
	else if (strcmp("BUILD", SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR BUILD TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR BUILD TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    dnbld(--IVAL(*dsetno,0), SVAL(*runtyp,0));
	}
	else if (strcmp("READ", SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER SET DISK READ TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER SET DISK READ TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    diskrd(--IVAL(*dsetno,0));
	}
	else if (strcmp("UPDATE", SVAL(*subcmd,0)) == 0) {
	    dsetno = p_find(&params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER SET DISK UPDATE TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER SET DISK UPDATE TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    diskup(--IVAL(*dsetno,0));
	}
	else if (strcmp("NAME", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nNAMED PARAMETER ACQUISITION TEST.");
	    fprintf (fp,
		    "\nNAMED PARAMETER ACQUISITION TEST.");
	    named();
	}
	else if (strcmp("IMAGE", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nIMAGE FILE ACCESS TEST.");
	    fprintf (fp,
		    "\nIMAGE FILE ACCESS TEST.");
	    image();
	}	    
	else if (strcmp("TERMNL", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nTERMINAL ROUTINE TEST.");
	    fprintf (fp,
		    "\nTERMINAL ROUTINE TEST.");
	    termnl();
	}
	else {
	    fprintf (fp1,
		    "\n*****-> ERROR: Unrecognized subcommand: %s",
		    SVAL(*subcmd,0));	
	    fprintf (fp,
		    "\n*****-> ERROR: Unrecognized subcommand: %s",
		    SVAL(*subcmd,0));	
	    z_exit(-1,"TST-BADSUB");
	}

	fprintf(fp1,"\n----- CAPPTEST EXIT");
	fprintf(fp,"\n----- CAPPTEST EXIT");
        z_exit(1,"");

	
			/*****************
			* ERROR HANDLING *
			*****************/

	/* NOTE:  WHEN ADDING ERROR MESSAGES, MAKE SURE THE TEXT CONTAINS
		  ASTERISKS (*) IN COLUMNS 1 - 5 FOR LATER TEXT SEARCH
		  BY TSTCLS ROUTINE.					*/

L1:	fprintf (fp, "\n*****-> UNABLE TO OPEN OUTPUT LOG FILE.");
	m_msg ("UNABLE TO OPEN OUTPUT LOG FILE.", "TST-NOLOGACC");
	z_exit (tnolog, "TST-NOLOGACC");
L2: p_herr (&params, hcode);
    fprintf (fp1,
		"\n*****-> UNABLE TO RECEIVE PARAMETER %s, HOST ERROR CODE = %d",
		name,hcode);
    fprintf (fp,
		"\n*****-> UNABLE TO RECEIVE PARAMETER %s, HOST ERROR CODE = %d",
		name,hcode);
    z_exit (tnorcv, "TST-RCVFAIL");
    exit();
L3: p_herr (&params, hcode);
    fprintf (fp1,
	  "\n*****-> UNABLE TO RECEIVE PARM BLOCK FROM TM. HOST ERROR CODE = %d ",
    	   hcode);
    fprintf (fp,
   	  "\n*****-> UNABLE TO RECEIVE PARM BLOCK FROM TM. HOST ERROR CODE = %d ",
    	   hcode);
    z_exit (tnoblk, "TST-BRCVFAIL");
}


FUNCTION VOID badint(pname, badi, expint, numint)

    /* PRINT ERROR MESSAGE FOR UNEXPECTED int VALUES. */
    /*       Arguments:			              */

	char	pname[];	/* PARAMETER NAME     */
        struct VARIABLE	*badi;	/* THE BAD VALUE(S)   */
	int expint[];	        /* EXPECTED VALUE(S)  */
	int numint;		/* NUMBER OF intS     */	
	
    {
    int i;

        /* TYPE PARAMETER NAME */

	fprintf (fp1, "\n*****-> UNEXPECTED VALUE FOR '%s'.\n",pname);
	fprintf (fp, "\n*****-> UNEXPECTED VALUE FOR '%s'.\n",pname);
        for (i=0; i<=numint-1; i++) {
	    fprintf(fp1,"\nRECEIVED:  %d",IVAL(*badi,i));
	    fprintf(fp,"\nRECEIVED:  %d",IVAL(*badi,i));
     	    }
	for (i=0; i<=numint-1; i++) {
	    fprintf(fp1,"\nEXPECTED:  %d",expint[i]);
	    fprintf(fp,"\nEXPECTED:  %d",expint[i]);
	    }
	return;
    }




FUNCTION VOID badrea(pname,badr,exprea,numrea)

    /* PRINT ERROR MESSAGE FOR UNEXPECTED REAL VALUES */
    /* ARGUMENTS:				      */

    char        	pname[];	/* PARAMETER NAME     */
    struct VARIABLE	*badr;    	/* THE BAD VALUE(S)   */
    TAEFLOAT            exprea[];	/* EXPECTED VALUE(S)  */
    int            	numrea;		/* NUMBER OF REALS    */
    {

    int i;    
    
    fprintf (fp1,"\n*****-> UNEXPECTED VALUE FOR '%s'.\n", pname);
    fprintf (fp,"\n*****-> UNEXPECTED VALUE FOR '%s'.\n", pname);
    for (i=0; i<=numrea-1; i++) {
	fprintf(fp1,"\nRECEIVED: %f",RVAL(*badr,i));
	fprintf(fp,"\nRECEIVED: %f",RVAL(*badr,i));
            }
    for (i=0; i<=numrea-1; i++) {
	fprintf(fp1,"\nEXPECTED: %f",exprea[i]);
	fprintf(fp,"\nEXPECTED: %f",exprea[i]);
        }
    return;
    } 



FUNCTION VOID badstr(pname,bads,expstr,numstr)

    /* PRINT AN ERROR MESSAGE FOR AN UNEXPECTED STRING VALUE.  
       ARGUMENTS:					      */

    char	pname[];	/* PARAMETER NAME     */
    struct VARIABLE	*bads;	/* THE BAD STRING(S)  */
    char*	expstr[];	/* EXPECTED STRING(S) */
    int    	numstr;		/* NUMBER OF STRINGS  */

    {

    int i;
    
    fprintf(fp1,"\n*****-> UNEXPECTED VALUE FOR '%s'",pname);
    fprintf(fp,"\n*****-> UNEXPECTED VALUE FOR '%s'",pname);
    for (i=0; i<=numstr-1; i++) {
        fprintf(fp1,"\nRECEIVED: %s", SVAL(*bads,i));
        fprintf(fp,"\nRECEIVED: %s", SVAL(*bads,i)); 
        }
    for (i=0; i<=numstr-1; i++) {
        fprintf(fp1,"\nEXPECTED: %s", expstr[i]);
        fprintf(fp,"\nEXPECTED: %s", expstr[i]);
        }
    return;
    }






FUNCTION VOID chkatr(pname, paddr, expcnt, exptyp)

/* THIS ROUTINE WILL OBTAIN THE ATTRIBUTES OF THE SPECIFIED
   PARAMETER AND COMPARE THEM AGAINST THE EXPECTED VALUES.

   CURRENTLY, ONLY "COUNT" AND "TYPE" ARE CHECKED BY CHKATR */


char	pname[];   /* PARAMETER NAME */
struct  VARIABLE *paddr;
int	expcnt;    /* THE NUMBER OF VALUES WE EXPECT THE PARAMETER TO CONTAIN*/
int 	exptyp;    /* THE TYPE OF VARIABLE WE EXPECT THE PARAMETER TO BE*/

{

if (((*paddr).v_count) != expcnt)  
    {
    fprintf(fp, "\n*****-> ERROR IN COUNT FOR PARAMETER %s.\n ", pname);
    fprintf(fp, "EXPECTED %d BUT COUNTED %d", expcnt, (*paddr).v_count);       
    fprintf(fp1, "\n*****-> ERROR IN COUNT FOR PARAMETER %s. ", pname);
    fprintf(fp1, "EXPECTED %d BUT COUNTED %d", expcnt, (*paddr).v_count);      
    z_exit (tattct, "TST_BADCOUNT");
    }


if  (((*paddr).v_type) != exptyp) 
    if ((((*paddr).v_type) == 3) && (((*paddr).v_file) != TRUE))
        {
        fprintf(fp, "\n*****-> ERROR IN TYPE FOR PARAMETER %s.\n ",pname); 
        fprintf(fp, "EXPECTED %d BUT FOUND %d", exptyp, (*paddr).v_type);
        fprintf(fp1, "\n*****-> ERROR IN TYPE FOR PARAMETER %s. ",pname);
        fprintf(fp1, "EXPECTED %d BUT FOUND %d", exptyp, (*paddr).v_type);
        z_exit (tattct, "TST_BADTYPE");
        }
}









	
FUNCTION VOID diskup (DSETNO)

    /* Calling arguments */
    int DSETNO;
    {
    /* This routine reads a parameter block from disk, updates
       values in the block, and writes the block back to disk */
    
    /* Local variables: */
    int  status;

    p_rdb (savblk, parlun, &params, P_BYTES, P_ABORT);
    status = q_intg (&params, "MULINT", intdim, &exmuli[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_intg' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_intg' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDINT");
	};
    status = q_real (&params, "MULREAL",rldim, &exmulr[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
 	{
	fprintf(fp, "\n*****-> Call to 'q_real' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_real' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDREAL");
	};
    status= q_string(&params,"MULSTRNG",strdim,&exmuls[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_string' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_string' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDSTR");
	};
    status = q_intg (&params, "DSETNO", 1, &DSETNO, P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_intg' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_intg' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDINT");
	};
    q_wrtb (savblk, 0, &params);
    }






FUNCTION VOID diskrd (DSETNO)
    
    /* Calling arguments: */
    int DSETNO;
    
    /* This routine reads a parameter block from disk and
       compares it to an expected set of values (DSETNO) */
    {
    p_rdb (savblk, parlun, &params,  P_BYTES, P_ABORT);
    strcpy (name, "DSETNO");
    dsetno = p_find (&params, "DSETNO");
    if (dsetno == NULL)
	{
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	z_exit (tnorcv, "TST-RCVFAIL");
	}
    pcheck (DSETNO);
    }






FUNCTION VOID dnbld (DSETNO, RUNTYP)

	/* Calling arguments: */
	int	DSETNO;
	char	RUNTYP[20];

	{
	/* Local arguments:	   */
	int 	status;

	q_init (&params, P_BYTES, P_CONT);
	status = q_dynp (&params, "CAPPTEST-BUILD", M_FULLPDF);
	if (status != SUCCESS)
		{
		fprintf (fp, "\n*****-> Attempt to send parameter block to");
		fprintf (fp, " TM failed. Status = %d", status);
		fprintf (fp1, "\n*****-> Attempt to send parameter block to");
		fprintf (fp1, " TM failed. Status = %d", status);
		z_exit (tnosnd, "TST-BSNDFAIL");
		}
	status = p_inim (&params, sizeof(struct PARBLK), P_CONT);
	if (strcmp (RUNTYP, batchrt) != 0)
	    {
	    if (status != SUCCESS)
		{
		fprintf (fp,"\n*****-> Unable to receive parm block");
		fprintf (fp," from TM. Status = %d", status);
		fprintf (fp1,"\n*****-> Unable to receive parm block");
		fprintf (fp1," from TM. Status = %d", status);
		z_exit (tnorcv, "TST-BRCVFAIL");
		}
	    pcheck (DSETNO);
	    }
	else if (strcmp (RUNTYP, batchrt) == 0)
		{
		fprintf (fp, "\nBATCH - Checking p_inim Status...");
		if (status == SUCCESS)
		    {
		    fprintf (fp, "\n*****-> p_inim passed SUCCESS");
		    fprintf (fp, " in batch, status = %d", status);
		    fprintf (fp1, "\n*****-> p_inim passed SUCCESS");
		    fprintf (fp1, " in batch, status = %d", status);
		    z_exit (tbqdyn, "TST-DYNOFAIL");
		    }
		else{
		    fprintf (fp, "\n Expected p_inim failure");
		    fprintf (fp, " from batch, status: %d", status);
		    fprintf (fp1, "\n Expected p_inim failure");
		    fprintf (fp1, " from batch, status: %d", status);
		    }
		}
	}








FUNCTION VOID dnkill ()

    /* This routine is called when the "DYNKILL" subcommand
       is found in a parameter block received from TM. A block
       of parameters is created, sent to TM and a kill command
       is expected */

    /* Calling arguments: none */
    {
    /* Local arguments:        */
    int status;

    q_init (&params, P_BYTES, P_ABORT);    
    status = q_dynp (&params, "CAPPTEST-DYNKILL", M_FULLPDF);
    if (status != SUCCESS)
  	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed. %d",
			status);
	fprintf(fp1,"\n*****-> Attempt to send parameter block to TM failed. %d",
			status);
    	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    status = p_inim (&params, sizeof(struct PARBLK), P_CONT);
    if (status != P_KILLED)
	{
	fprintf(fp, "\n*****-> Expected P_KILLED status code from p_inim. ");
	fprintf(fp, "Received: %d", status);
	fprintf(fp1, "\n*****-> Expected P_KILLED status code from p_inim. ");
	fprintf(fp1, "Received: %d", status);
    	z_exit (tnokil, "TST-NOKILL");
	}
    }











FUNCTION VOID dnsub (DSETNO)

    /* Calling arguments: */
    int DSETNO;

    /* This routine is called when the 'SUBSET' subcommand is
       received in a parameter block from TM. The processing involves
       building a parameter block from scratch, filling it with a
       subset of the parameters defined for the process. The block is
       sent to TM, dynamic tutor is entered, and the block is sent
       back unchanged. The values in the block are then verified. */
    {
    /* Local Variables: */
    TAEINT i;
    TEXT  pname[8];

    q_init (&params, P_BYTES, P_CONT);
    status=q_real (&params, "MULREAL", rldim, &exmulr[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_real' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_real' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDREAL");
	}
    status=q_string(&params, "MULSTRNG", strdim, &exmuls[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_string' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_string' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDSTR");
	}
    status=q_intg (&params, "DSETNO", 1, &DSETNO, P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'q_intg' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'q_intg' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDINT");
	};
    status=q_dynp (&params, "CAPPTEST-SUBSET", M_SUBPDF);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed.");
	fprintf(fp, "Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Attempt to send parameter block to TM failed.");
	fprintf(fp1,"Status = %4d\n", status);
	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    p_inim (&params, sizeof(struct PARBLK), P_ABORT);

    /* Now check values returned */
    strcpy (pname, "MULREAL");
    mulreal = p_find (&params, "MULREAL");
    if (mulreal == NULL) goto LDNSUB1;
    chkatr (pname, mulreal, rldim, V_REAL);
    for (i=0; i<rldim; i++)
        if ( !checkreal ((float)RVAL(*mulreal,i),(float)exmulr[DSETNO][i]) )
           {
	    badrea ("MULREAL", mulreal, exmulr[DSETNO], rldim);
            break;
           }
    strcpy(pname, "MULSTRNG");
    mulstr = p_find (&params, pname);
    if (mulstr == NULL) goto LDNSUB1;
    chkatr (pname, mulstr, strdim, V_STRING);
    for (i=0; i<strdim; i++)
	if (strcmp(SVAL(*mulstr,i), exmuls[DSETNO][i]) != 0)
	    {
	    badstr ("MULSTRNG", mulstr, exmuls[DSETNO], strdim);
	    break;
	    }
    return;

LDNSUB1:fprintf(fp, "\n*****-> Unable to receive parameter %s. Error=%4d\n",
			pname, status);

	fprintf(fp1,"\n*****-> Unable to receive parameter %s. Error=%4d\n",
			pname, status);
    }









FUNCTION BOOL checkreal ( r1, r2 )

    float r1;
    float r2;
{
#ifdef sun
   	if ( r1 != r2 )
           return ( FALSE );
#else
#ifdef VAX_VMS 
   	if ( r1 != r2 )
           return ( FALSE );
#else
        if ( fabs ( r1 - r2 ) > EPSILON )
           return ( FALSE );
#endif
#endif
        return (TRUE);
}

FUNCTION VOID dnturn (DSETNO)

    /* This routine is called when the DYNTURN subommand is
       received in a parameter block from TM. The processing
       involves building a parameter block from scratch, filling
       it with values consistent with the DSETNO value received
       from TM. The block is sent to TM, dynamic tutor is entered,
       and the block is sent back to CAPPTEST. The values in the block
       are then compared against the value of DSETNO received back from TM.*/

    /* Calling arguments: */
    int	 DSETNO; 	/* dataset number */
    {
    /* Local arguments:   */

    q_init (&params, P_BYTES, P_CONT);
    status = q_intg (&params, "MULINT", intdim, &exmuli[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to q_intg to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to q_intg to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDINT");
	}
    status = q_real (&params, "MULREAL", rldim, &exmulr[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to q_real to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to q_real to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDREAL");
	}
    status = q_string (&params, "MULSTRNG", strdim, &exmuls[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to q_string to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to q_string to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDSTR");
	}
    status = q_string (&params, "MULFIL", fildim, &exmulf[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to q_string to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to q_string to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDFILE");
	}
    DSETNO++;  /* because we decremented before we called dnturn */
    status = q_intg (&params, "DSETNO", 1, &DSETNO, P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to q_intg to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to q_intg to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDINT");
	}
    status = q_dynp (&params, "CAPPTEST-DYNTURN", M_SUBPDF);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed. %d",
				status);
	fprintf(fp1, "\n*****-> Attempt to send parameter block to TM failed. %d",
				status);
 	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    p_inim (&params, sizeof(struct PARBLK), P_ABORT);
    strcpy (name, "DSETNO");
    dsetno = p_find (&params, "DSETNO");
    if (dsetno == NULL)
	{
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	z_exit (tnorcv, "TST-RCVFAIL");
	}
    pcheck (--IVAL(*dsetno,0));
    }









FUNCTION VOID format (dummy) /* dummy isn't needed; just for temporary compat.*/

    /* Calling arguments */
    int	  dummy;
    {

    /* This routine verifies the correct operation of the
       TAE C callable parameter formatting routines. */

    /* Local Variables: */
    TEXT  pline[75];

    p_inifor(&params);	/* initialize for p_forp calls */
    status = SUCCESS;
    while (status == SUCCESS)
	{
	status = p_forp (&params, pline, 74);
	if (status == SUCCESS)
	    {
	    fprintf(fp, "\n     %s", pline);
	    fprintf(fp1,"\n     %s", pline);
	    }
	else if (status == FP_EOP) return;
	else if (status == FP_NULL)
	    {
	    fprintf(fp, "\n*****-> Parameter or global or local ");
	    fprintf(fp, "variable too long to format into displayable form\n");
	    fprintf(fp1,"\n*****-> Parameter or global or local ");
	    fprintf(fp1,"variable too long to format into displayable form\n");
	    z_exit(-1, "TST-PTOOLONG");
	    }
	else{
	    fprintf(fp, "\n*****-> Unrecognized error code from 'p_forp'. ");
	    fprintf(fp, "status = %4d\n", status);
	    fprintf(fp1,"\n*****-> Unrecognized error code from 'p_forp'. ");
	    fprintf(fp1,"status = %4d\n", status);
	    z_exit(-1, "TST-PFORPER");
	    }
	}
    }




FUNCTION VOID image()
 
{
/* LOCAL VARIABLES */

struct	IFCB  ifcb;             /* IMAGE FILE CONTROL BLOCK */
struct  VARIABLE *imgfile;  
TEXT    labbuf[labsiz+1];	/* RECEIVE DATA BUFFER */	
TEXT    explab[labsiz+1];       /* A DATA BUFFER */
char	buffer[100];            /* A DATA BUFFER */
int	line, chan;
TEXT	hfile [FSPECSIZ+1];     /* ARRAY TO RECEIVE TRANSLATED HOST FILE*/
TEXT	taefile [FSPECSIZ];     /* FILESPEC TO BE TRANSLATED */
int     stat;

    strcpy (explab, "Test image label.");
    imgfile = p_find(&params, "IMGFILE");
    if(((*imgfile).v_file != TRUE) || (imgfile == NULL))
	{
	fprintf (fp,"\n*****->UNABLE TO ACCESS IMAGE FILE NAME PARAMETER");
	fprintf (fp1,"\n*****->UNABLE TO ACCESS IMAGE FILE NAME PARAMETER");
        z_exit(timwrt, "TST-IWRTFAIL");
	}

    strcpy (taefile, SVAL(*imgfile,0));
    p_file (taefile, (*imgfile).v_filemode, hfile);
    stat = i_opou(&ifcb, lun, hfile, 2, imchan, imline, linsiz, imlabl,labsiz);
    if (stat != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T OPEN IMAGE FILE FOR OUTPUT.");
	fprintf (fp1,"\n*****->CAN'T OPEN IMAGE FILE FOR OUTPUT.");
        z_exit(timwrt, "TST-IWRTFAIL");
        }


    /********************************************************************
    * WRITE A 7 BAND/10 LINE PER BAND IMAGE TO THE IMAGE FILE           *
    ********************************************************************/

    
    if (i_wrlb (&ifcb, explab, 1) != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T WRITE IMAGE FILE LABEL RECORD.");
	fprintf (fp1,"\n*****->CAN'T WRITE IMAGE FILE LABEL RECORD.");
	z_exit(tilblw, "TST-ILABWRIT");
	}
    for (chan = 1; chan <= 7; ++chan)
	{
	for (line = 1; line <= 10; ++line)
	    {
	    buffer[0] = chan;
	    buffer[1] = line;
	    buffer[linsiz/2-1] = line;
	    buffer[linsiz-1] = line;
	    stat = i_write (&ifcb, buffer, chan, line, 1);
	    if (stat != SUCCESS)
		{
		fprintf (fp, "\n*****-> Error writing an image line ");
		fprintf (fp, "to image file (i_write).");
		fprintf (fp1, "\n*****-> Error writing an image line ");
		fprintf (fp1, "to image file (i_write).");
		fprintf (fp, "\n*****->Host error code = %d", i_herr (&ifcb));
		fprintf (fp1, "\n*****->Host error code = %d", i_herr (&ifcb));
		z_exit (tilacc, "TST-IMGWRITE");
		}
	    if (i_wait (&ifcb) != SUCCESS) goto ERR3;
 	    }
 	}
    i_clse (&ifcb, I_SAVE);

    /*****************************************************************
    *          NOW OPEN THE FILE AND READ ALL THE RECORDS            *
    *****************************************************************/

    stat = i_opin (&ifcb, lun, hfile, I_INPUT);
    if (stat != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T OPEN IMAGE FILE FOR INPUT.");
	fprintf (fp1,"\n*****->CAN'T OPEN IMAGE FILE FOR INPUT.");
        z_exit(tilblr, "TST-ILABREAD");
        }
    if (i_rdlb (&ifcb, labbuf, 1) != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T READ IMAGE FILE LABEL");
	fprintf (fp1,"\n*****->CAN'T READ IMAGE FILE LABEL");
	z_exit (tilblr, "TST-ILABREAD");
	}
    if (strcmp (labbuf ,explab) != 0)
	{
	fprintf (fp,"\n*****->IMAGE FILE LABEL MISMATCH."); 
	fprintf (fp1,"\n*****->IMAGE FILE LABEL MISMATCH."); 
	z_exit (tilblm, "TST-ILABMTCH");
	}
    for (chan = 1; chan <= 7; ++chan)
	{
	for (line = 1; line <= 10; ++line)
	    {
	    stat = i_read (&ifcb, buffer, chan, line, 1);
	    if (stat != SUCCESS)
		{
		fprintf (fp, "\n*****->Error reading image file(i_read).");
		fprintf (fp1, "\n*****->Error reading image file(i_read).");
		fprintf (fp,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
		fprintf (fp1,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
		z_exit (tilacc, "TST-IMGREAD");
		}		
     	    if (i_wait (&ifcb) != SUCCESS)   goto ERR1;
	    if ( (buffer[0] != (char)chan) 
	      || (buffer[1] != (char)line)
	      || (buffer[linsiz/2-1] != (char)line) 
	      || (buffer[linsiz-1] != (char)line) )
		goto ERR2;
	    }
	}
    i_clse (&ifcb, I_DEL);
    return;

    /* ERROR HANDLING */
    
ERR1:	fprintf (fp,"\n*****->ERROR WAITING FOR IMAGE FILE READ (I_WAIT).");
	fprintf (fp1,"\n*****->ERROR WAITING FOR IMAGE FILE READ (I_WAIT).");
	fprintf (fp,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
	fprintf (fp1,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
	z_exit (tilacc, "TST-IMGRWAIT");

ERR2:   fprintf (fp,"\n*****->IMAGE DATA WRITTEN DOES NOT MATCH DATA READ.");
        fprintf (fp1,"\n*****->IMAGE DATA WRITTEN DOES NOT MATCH DATA READ.");
        z_exit (tdata, "TST-NOMATCH");

ERR3:   fprintf (fp, "\n*****-> ERROR WAITING FOR IMAGE FILE WRITE(I_WRITE).");
	fprintf (fp1,"\n*****-> ERROR WAITING FOR IMAGE FILE WRITE(I_WRITE).");
	fprintf (fp, "\n*****->HOST ERROR CODE = %d", i_herr (&ifcb));
	fprintf (fp1, "\n*****->HOST ERROR CODE = %d", i_herr (&ifcb));
	z_exit (tilacc, "TST-IMGWWAIT");
	}





FUNCTION VOID named()
    /* THIS ROUTINE IS CALLED WHEN THE "NAME" SUBCOMMAND IS ENCOUNTERED.
       NAMED WILL READ A PARAMETER FROM THE PARAMETER BLOCK WHICH IS A 
       NAMED PARAMETER.  THE VALUE OF THE PARAMETER IS COMPARED AGAINST 
       AN EXPECTED VALUE AND THEN SET TO A NEW VALUE.  THE BLOCK IS THEN
       SENT BACK TO TM.                                                 */

{
    /* LOCAL ARGUMENTS */

struct VARIABLE *s;
int    status;
TEXT   namest[40];

    s = p_find(&params, "S");
    if(s == NULL) 
        {
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER 'S'. ");
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER 'S'. ");
        z_exit (tnorcv, "TST-NAMEMTCH");
        }

    strcpy (namest, SVAL(*s,0));
    if (strcmp (namest, exname[0]) != 0)
	{
	badstr ("S", s, exname, 1);
	z_exit (tdata, "TST-NAMEMTCH");
	}
    status = q_string(&params, "S", 1, &retstr[0], P_UPDATE);
    if (status != SUCCESS)
        {
        fprintf (fp1,"\n*****-> CAN'T UPDATE PARAMETER 'S' IN BLOCK.");
	fprintf (fp1," STATUS = %d ", status);
        fprintf (fp,"\n*****-> CAN'T UPDATE PARAMETER 'S' IN BLOCK.");
	fprintf (fp," STATUS = %d ", status);
        z_exit (tnoupd, "TST-UPDSTR");
        }

    status = q_intg(&params, "$SWITCH", 1, &swval, P_UPDATE);
    if (status != SUCCESS)
	{
        fprintf (fp1,"\n*****->CAN'T UPDATE GLOBAL '$SWITCH' IN BLOCK.");
	fprintf (fp1," STATUS = %d ", status);
        fprintf (fp,"\n*****->CAN'T UPDATE GLOBAL '$SWITCH' IN BLOCK.");
	fprintf (fp," STATUS = %d ", status);
        z_exit (tnoupd, "TST-UPDSWTCH");
	}

    status = q_out(&params);
    if (status != SUCCESS)
   	{
	fprintf (fp1,"\n*****->ATTEMPT TO SEND PARAMETER BLOCK ");
	fprintf (fp1,"TO TM FAILED. HOST ERROR = %d", status);
	fprintf (fp,"\n*****->ATTEMPT TO SEND PARAMETER BLOCK ");
	fprintf (fp,"TO TM FAILED. HOST ERROR = %d", status);
	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    }   




FUNCTION VOID pcheck (exdset)
    int exdset;

    {
    /* THIS ROUTINE IS CALLED DURING ANY TEST WHICH REQUIRES A
       COMPARISON OF THE PARAMETERS RECEIVED FROM TM AND THE
       HARDCODED EXPECTED VALUES.				*/

    char 	pname[10];
    int 	i;
    TEXT        hfile [FSPECSIZ+1];

    /******************************************************
    *     GET AND CHECK EACH EXPECTED PARAMETER		  *
    *		- MULINT, a multivalue int value      *
    *		- MULREAL, a multivalue REAL parameter    *
    *		- MULSTRNG, a multivalue string parameter *
    *		- MULFIL, a multivalue filespec           *
    ******************************************************/


    strcpy(pname, "DSETNO");
    dsetno = p_find(&params,"DSETNO");
    if(dsetno == NULL) goto L100;
    chkatr(pname, dsetno, 1, V_INTEGER);
    if(exdset != IVAL(*dsetno,0)) 
	{
	badint ("DSETNO", dsetno, &exdset, 1);
 	goto L50;
    	}
    strcpy(pname, "MULINT");
    mulint = p_find(&params,"MULINT");
    if(mulint == NULL) goto L100;
    chkatr(pname, mulint, intdim, V_INTEGER);
    for (i=0; i< intdim; i++) 
	if (IVAL(*mulint,i) != exmuli [IVAL(*dsetno,0)][i]) 
	    {
	    badint ("MULINT", mulint, exmuli[IVAL(*dsetno,0)], intdim);
       	    break;
	    };
L50:  
    strcpy(pname,"MULREAL");
    mulreal = p_find (&params, "MULREAL");
    if (mulreal == NULL) goto L100;
    chkatr (pname, mulreal, rldim, V_REAL);
    for (i=0; i< rldim; i++)                                        
        {
   	if ( !checkreal ( (float)RVAL(*mulreal,i) , 
                          (float)exmulr[IVAL(*dsetno,0)][i] ) )
	    {
	    badrea ("MULREAL", mulreal, exmulr[IVAL(*dsetno,0)], rldim);
	    break;
	    };
         }
    strcpy(pname, "MULSTRNG");
    mulstr = p_find (&params, "MULSTRNG");
    if (mulstr == NULL) goto L100;
    chkatr (pname, mulstr, strdim, V_STRING);
    for (i=0; i< strdim; i++)
	if (strcmp(SVAL(*mulstr,i),exmuls [IVAL(*dsetno,0)][i]) != 0) 
	    {
	    badstr ("MULSTRNG", mulstr, exmuls [IVAL(*dsetno,0)], strdim);
	    break;
	    };
    strcpy(pname, "MULFIL");
    mulfil = p_find (&params, "MULFIL");
    if (mulfil == NULL) goto L100;
    chkatr (pname, mulfil, fildim, V_STRING);
    for (i=0; i< fildim; i++)
        {
        p_file (SVAL(*mulfil,i), (*mulfil).v_filemode, hfile);
	if (strcmp (hfile, exmulf [IVAL(*dsetno,0)][i]) != 0)
	    {
	    badstr ("MULFIL", mulfil, exmulf [IVAL(*dsetno,0)], fildim);
	    break;
	    };
        }
    return;
L100: fprintf (fp1,"\n*****->UNABLE TO ACCESS PARAMETER %s", pname);
      fprintf (fp,"\n*****->UNABLE TO ACCESS PARAMETER %s", pname);
      return;
    }




#define MAXSTRING 200
FUNCTION VOID tstcls ()
{
    /*  Note on the screen and in the log, whether the Parameter Check
	Self Test failed or passed. This routine is meant to be called
	after all iterations of the test are completed.
	This routine does a text search for certain keywords indicating
	failure. Now enhanced to look for pairs of '+++++' and '-----'.
	If these symbols do not occur in pairs, first '+++++' then
	'-----', an CAPPTEST run has aborted or exited on an '*****' message.*/

    /* Local variables:  */
    int		failed=FALSE;	/* TRUE if test failed */
				/* start by assuming test didn't fail */
    char	string[MAXSTRING+1];	/* Text line buffer    */
    char	token[21];
    int		plus=FALSE;	/* '+++++' tracker 	*/
    int		minus=TRUE;	/* '-----' tracker	*/
    char	pname[10];
    char	stdfil[80];	/* File name		*/
    int		flag;		/* dummy flag		*/
    int		fd;
    struct VARIABLE *stdfile;	/* STDFILE implicit parameter pointer    */

    stdfile=p_find(&params,"STDFILE");
    if (stdfile==NULL) goto L810;
    fd = open (SVAL(*stdfile, 0), 0);               /* 0 is read mode    */
    if (fd < 0) goto L800;
    while ( (flag = getline(fd,string,MAXSTRING)) != 0) /* get next line */
	{
	if (flag<0) goto L850;                          /* read error    */
	if (strncmp(string,"*****->",7) == 0)
	    {
	    failed=TRUE;
	    break;
	    }
 	/* Initial plus ok, but should always see a minus before another
	   plus. Also we do not check for the case of no +++++ or -----;
	   this is so that we can still use this for old CAPPTEST.DATs and
	   other sundry files such s CTCLTESTB.LOG. Can't write them until
	   we open anyway so we have a WINDOW OF VULNERABILITY anyway*/
	if (strncmp(string,"+++",3) == 0)
	    {
	    if (plus || (!minus))
		{
		failed=TRUE;
		break;
		};
	    plus=TRUE;
	    minus=FALSE;
	    };
	if (strncmp(string,"---",3) == 0)
	    {
	    if (minus || (!plus))
		{
		failed=TRUE;
		break;
		}
	    plus=FALSE;
	    minus=TRUE;
	    }
	} /* end while */
        close (fd);
	t_bell();
	if (failed)
	    {
	    t_bell();
	    fprintf(fp, "\n*** CTAETEST UNSUCCESSFUL\n");
	    }
	else{
	    fprintf(fp, "\nCTAETEST COMPLETED SUCCESSFULLY\n");
	    }
	return;
/* Error Handling */
L800:	fprintf(fp,"\nCOULDN'T OPEN LOG FILE WHILE CLOSING OUT TEST\n");
	z_exit(topen, "TST-LOGOPEN");
L810:	fprintf(fp, "\nLOG FILE NAME PARM ERROR WHILE CLOSING OUT TEST\n");
	z_exit(topen, "TST-LOGPARM");
L850:	fprintf(fp, "\nLOG FILE READ ERROR WHILE CLOSING OUT TEST\n");
	z_exit(tread, "TST-LOGREAD");
}






FUNCTION VOID termnl()
    /* This routine exercises the TAE terminal access routines */
    /* Calling arguments: none */
    {
    /* Local   arguments: */
    int i;
    
    t_clear  ();
    t_output (8, 30, "THIS IS A");	/* write line to screen */
    t_output (9, 30, "TEST OF THE");
    t_output (10, 30, "TAE TERMINAL");
    t_output (11, 30, "  ROUTINES");
    t_bell ();
    for (i=27; i<=45; i++)
	{
	t_pos (6, i);
 	t_write ("X", T_NULL);
	}
    t_bell ();
    for (i=27; i<=45; i++)
	{
	t_pos (13, i);
	t_write ("X", T_NULL);
	}
    t_bell ();
    for (i=7; i<=12; i++)
	{
	t_pos (i, 27);
	t_write ("XX", T_NULL);
	}
    t_bell ();
    for (i=7; i<=12; i++)
	{
	t_pos (i, 44);
	t_write ("XX", T_NULL);
	}
    t_bell ();
    t_bell ();
    }








FUNCTION VOID tstexs ()

	/* Calling arguments: none */

	/* THIS ROUTINE TO BE USED FOR BATCH TESTING. IT IS USED
	   TO "SYNCHRONIZE" BATCH SUBMISSIONS TO AVOID FILE ACCESS
	   COLLISIONS. 

	   WITHIN A LOOP WE WAIT 5 SECONDS AND OPEN THE FILE NAMED
	   BY THE PARAMETER checkf. THE LOOP IS EXECUTED UP TO limit
	   NUMBER OF TIMES OR
		FOR look = "NOEXIST"
		    UNTIL checkf DOES NOT EXIST.
		FOR look = "EXIST"
		    UNTIL checkf DOES EXIST.

	   AFTER WE HAVE TRIED limit NUMBER OF TIMES, WE "TIME OUT". */

	{
	
	/* Local arguments:        */
	struct VARIABLE *checkf, *limit, *look;
        FILE    *fp2;
	int     i;
	char	pname[80];

	strcpy (pname, "CHECKFIL");
	checkf = p_find (&params, "CHECKFIL");
	if (checkf == NULL) goto ERR6;
	strcpy (pname, "LIMIT");
	limit = p_find  (&params, "LIMIT");
	if (limit ==  NULL) goto ERR6;
	strcpy (pname, "LOOK");
	look  = p_find  (&params, "LOOK");
	if (look == NULL) goto ERR6;
	
	fprintf (fp, "\n\nWAITING FOR FILE: %s", SVAL(*checkf,0));
	fprintf (fp, "\nLOOP LIMIT: %d", IVAL(*limit,0));
	fprintf (fp, ". FILE MUST %s\n", SVAL(*look,0));
	for (i=1; i<=IVAL(*limit,0); i++)
	    {
	    apwait (waitdelta);  /* WAIT A LITTLE WHILE */
	    fp2 = fopen (SVAL(*checkf,0) , "r");
	    if ((fp2==NULL) && (strcmp(SVAL(*look,0),"NOEXIST")==0)) return;
	    if  (fp2==NULL) continue;     /* keep checking til timeout */

/* successfully opened the file, was it supposed to be there ? */
	    if (strcmp (SVAL(*look,0), "EXIST") == 0)    /* success */
		{
		fclose (fp2);
		return;
		}
	    else if (strcmp(SVAL(*look,0),"NOEXIST") == 0) 
                {
                fclose (fp2);    /* file not supposed to exist, so close */
                continue;        /*   and hope it goes away soon...      */
                }
            else                        /* neither EXIST nor NOEXIST... */
                {
        	strcpy (pname, "LOOK");
                goto ERR6;
                }
	    }
	fprintf (fp, "\n*****-> TIME OUT");
	m_msg ("CAPPTEST-POLL timed out", "TST-POLLTIMO");
	z_exit (topen, "TST-POLLTIMO");

ERR6:	fprintf (fp, "\n*****-> %s: PARM ERROR IN POLL", pname);
	z_exit (tnorcv, "TST-POLLPARM");
	}



FUNCTION VOID apwait (delta)    /* WAIT delta seconds */
    int delta;
    {
    int status;
#ifdef VMS
    status = wait_hold (delta*1000);    /* wait_hold expects milliseconds */
    if (status != SUCCESS)
        {
	fprintf (fp, "\n*****-> wait_hold failed with status=%d\n", status);
	z_exit (topen, "TST-POLLTIMO");
        }
#else
    wait_hold (delta*1000);    /* wait_hold expects milliseconds */
#endif
    return;
    }


/* looks a lot like read, but stops at a newline boundary */

FUNCTION static int getline (fd, buf, maxsiz)
   int       fd;
   GENPTR    buf;
   int       maxsiz;
   {
   int i=0;
   int j;
   while ( (j = read (fd, &buf[i], 1)) > 0 )
      {
      if (buf[i] == '\n') { buf[i+1] = EOS; return (i+1); }
      i++;
      if (i >= maxsiz) { printf ("internal size error\n"); return (-1); }
      }
   return (j);
   }

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* VMAPPTEST.C      (VM version of APPTEST)
 *
 * VMAPPTEST IS DESIGNED TO VERIFY THE OPERATIONS OF THE TAE PARAMETER PASSING
 * MECHANISM BETWEEN THE TAE MONITOR AND TAE APPLICATIONS.
 *
 * VMAPPTEST EXERCISES ALL OF THE SUBROUTINES AVAILABLE TO AN APPLICATION
 * PROGRAM FROM THE TAE OBJECT MODULE LIBRARY. THE SUBROUTINE PACKAGES
 * UTILIZED INCLUDE: 
 *
 *
 *	PACKAGE		TITLE
 *	-------		-----
 * 	Vm_		PARAMETER/VARIABLE MANIPULATION
 *	z_		MISCELLANEOUS
 *
 * 
 * NOTE:  THE PARAMETER PASSING ROUTINES ARE ACTUALLY UTILIZED IN MOST
 *      OF THE TESTS. 
 *
 *   Change Log:
 *
 *              19-Sep-1987 Created...tpl
 *              16-Dec-1987 Converted to UNIX...tpl
 *		10-nov-88   #ifdef sun for epsilon checking...ljn
 *		08-apr-91   pr943 added call to f_force_lower...tpl
 *		29-oct-92   Added include of math.h (for fabs)...krw
 *		15-dec-93   Updated vrsion[] to an IMPORT TEXT; eliminate empty
 *			    declaration warnings...dag
 */

#include	"taeconf.inp"
#include	"parblk.inc"
#include        "terminc.inc"
#include	"tstinc.inc"
#include	"vmdatinc.inc"
#include	<stdio.h>
#include	<math.h>	/* get definition of fabs */

IMPORT TEXT	vrsion[];	/* program version number */
FILE		*fp,*fp1;	/* pointer to standard output file */
FILE            *Vm_OpenStdout();
Id              Vm_New();

int		status;
char		string[132];	/* string buffer    		   */
struct VARIABLE	*Vm_Find(),	
		*subcmd,	/* _SUBCMD implicit parameter pointer       */
		*sess,		/* $SESSION implicit parameter pointer 	    */
		*runtyp,	/* $RUNTYPE implicit parameter pointer      */
		*procnm,	/* _PROC implicit parameter pointer         */
		*std_out,	/* _STDOUT implicit parameter pointer  	    */
		*usestd,	/* usestd keyword parameter pointer         */
		*switch_wd,	/* $SWITCH implicit parameter pointer	    */
		*dsetno,	/* DSETNO implicit parameter pointer	    */
		*mulint,	/* MULINT parameter pointer		    */
		*mulreal,	/* MULREAL parameter pointer		    */
		*mulstr,	/* MULSTRNG parameter pointer		    */
		*mulfil;	/* MULFIL parameter pointer		    */


FUNINT		lun;		/* Logical unit number			    */
	

int		solen[1];
int		uselen;
char		name[NAMESIZ+1];
Id        	params;	        /* space for parameter block	   */
char		dummy[132];
CODE            code;
CODE		*hcode;		/* host error code		   */
int		iswitch;	/* $SWITCH implicit parameter	   */


	/*********************************************************
	* INITIALIZE PARAMETER BLOCK (I.E. GET IT FROM TM), AND  *
	* CHECK IMPLICIT PARAMETERS TO DETERMINE THE TEST TO     *
	* EXECUTE. 						 *
	*********************************************************/


FUNCTION main()
{
    f_force_lower(TRUE);
    params = Vm_New(P_CONT);
    code = Vm_ReadFromTM ( params );    
    fp=Vm_OpenStdout( params); /* get stdout */
    if (fp==NULL) goto L3; /* abort if error in initializing */
    fprintf(fp,
                "\n\tVm_ APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s\n\n",
		vrsion);	

    procnm = Vm_Find (params,"_PROC");
    strcpy (name, "_PROC");
    if (procnm==NULL) goto L2;
    std_out = Vm_Find (params,"_STDOUT");
    strcpy (name, "_STDOUT");
    if (std_out==NULL) goto L2;
    subcmd = Vm_Find (params,"_SUBCMD");
    strcpy (name, "_SUBCMD");
    if (subcmd==NULL) goto L2;
    sess = Vm_Find (params,"$SESSION");
    strcpy (name, "$SESSION");
    if (sess==NULL) goto L2;
    runtyp = Vm_Find (params,"$RUNTYPE");
    strcpy (name, "$RUNTYPE");
    if (runtyp==NULL) goto L2;
    switch_wd = Vm_Find (params,"$SWITCH");
    strcpy (name, "$SWITCH");
    if (switch_wd==NULL) goto L2;
    usestd = Vm_Find (params,"USESTD");
    strcpy (name, "USESTD");
    if (usestd==NULL) goto L2;
	
    if(strcmp ("START",SVAL(*subcmd,0)) == 0) 
	{
	        
	/********************************************************
        *	OPEN DATA FILE AND WRITE VERSION NUMBER		*
	*	TO IT.						*
	*********************************************************/

	fp1=fopen("VMAPPTEST.DAT", "w");
	if (fp1==NULL) goto L1;
        fprintf(fp1,
		"\n\tVm_ APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s",
	        vrsion);
	
	fprintf (fp1,"\n+++++ VMAPPTEST BEGIN"); /* Write begin sentinel */
        fprintf (fp1,"\n----- VMAPPTEST EXIT"); /* Write exit sentinel */
       	z_exit (1,"");
	}

    else if (strcmp ("END",SVAL(*subcmd,0)) == 0) 
	{
        /* No begin or exit sentinels for -END */    	
	    
	tstcls();	/* Perform test completion processing */
	z_exit (1,"");
	}
    else if (strcmp ("POLL",SVAL(*subcmd,0)) ==0) 
	{
	/* No begin or exit sentinels for -POLL */    
	tstexs();    /* Wait for file to exist */
    	z_exit (1,"");
	}

	/********************************************************
	* IF NOT INITIALIZATION OR TERMINATION RUN, OPEN LOG	*
	* FILE AND POSITION TO THE END FOR ADDING MORE ENTRIES. *
	********************************************************/

	fp1=fopen ("VMAPPTEST.DAT", "a"); /*open and position to the end of file */
	if (fp1==NULL) goto L1;

	fprintf (fp1,
		"\n\tVm_ APPLICATION SUBROUTINE PACKAGE TEST. VERSION %s\n\n",
		vrsion);
	fprintf (fp1,
		"\n+++++ VMAPPTEST BEGIN"); /* WRITE BEGIN SENTINEL TO FILE */	
	fprintf (fp,
		"\n+++++ VMAPPTEST BEGIN"); /* WRITE BEGIN SENTINEL */	
 
	if (strcmp("CHECK",SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find (params,"DSETNO"); /* First get dataset number */
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
	    	    "\nPARAMETER ACQUISITION TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    fprintf (fp,
	    	    "\nPARAMETER ACQUISITION TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    pcheck (--IVAL(*dsetno,0));
	}
	else if (strcmp("FORMAT",SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER FORMATTING TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER FORMATTING TEST ON DATASET %d",
		     IVAL(*dsetno,0));
	    format(--IVAL(*dsetno,0));
	}
	else if (strcmp("DYNKILL", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,"\nDYNAMIC TUTOR KILL TEST");
	    fprintf (fp,"\nDYNAMIC TUTOR KILL TEST");
	    dnkill();
	}
	else if (strcmp("DYNTURN", SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First get dataset number*/
            strcpy (name, "DSETNO");
            if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR TURNAROUND TEST ON DATATEST %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR TURNAROUND TEST ON DATATEST %d",
		    IVAL(*dsetno,0));
	    dnturn(--IVAL(*dsetno,0));
	}
	else if (strcmp("SUBSET", SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR SUBSETTING TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR SUBSETTING TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    dnsub(--IVAL(*dsetno,0));
	}
	else if (strcmp("BUILD", SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nDYNAMIC TUTOR BUILD TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nDYNAMIC TUTOR BUILD TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    dnbld(--IVAL(*dsetno,0), SVAL(*runtyp,0));
	}
	else if (strcmp("READ", SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER SET DISK READ TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER SET DISK READ TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    diskrd(--IVAL(*dsetno,0));
	}
	else if (strcmp("UPDATE", SVAL(*subcmd,0)) == 0) {
	    dsetno = Vm_Find(params,"DSETNO"); /* First, get dataset number*/
            strcpy (name, "DSETNO");
	    if (dsetno == NULL) goto L2;
	    fprintf (fp1,
		    "\nPARAMETER SET DISK UPDATE TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    fprintf (fp,
		    "\nPARAMETER SET DISK UPDATE TEST ON DATASET %d",
		    IVAL(*dsetno,0));
	    diskup(--IVAL(*dsetno,0));
	}
	else if (strcmp("NAME", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nNAMED PARAMETER ACQUISITION TEST.");
	    fprintf (fp,
		    "\nNAMED PARAMETER ACQUISITION TEST.");
	    named();
	}
	else if (strcmp("IMAGE", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nIMAGE FILE ACCESS TEST.");
	    fprintf (fp,
		    "\nIMAGE FILE ACCESS TEST.");
	    image();
	}	    
	else if (strcmp("TERMNL", SVAL(*subcmd,0)) == 0) {
	    fprintf (fp1,
		    "\nTERMINAL ROUTINE TEST.");
	    fprintf (fp,
		    "\nTERMINAL ROUTINE TEST.");
	    termnl();
	}
	else {
	    fprintf (fp1,
		    "\n*****-> ERROR: Unrecognized subcommand: %s",
		    SVAL(*subcmd,0));	
	    fprintf (fp,
		    "\n*****-> ERROR: Unrecognized subcommand: %s",
		    SVAL(*subcmd,0));	
	    z_exit(-1,"TST-BADSUB");
	}

	fprintf(fp1,"\n----- VMAPPTEST EXIT");
	fprintf(fp,"\n----- VMAPPTEST EXIT");
        z_exit(1,"");

	
			/*****************
			* ERROR HANDLING *
			*****************/

	/* NOTE:  WHEN ADDING ERROR MESSAGES, MAKE SURE THE TEXT CONTAINS
		  ASTERISKS (*) IN COLUMNS 1 - 5 FOR LATER TEXT SEARCH
		  BY TSTCLS ROUTINE.					*/

L1:	fprintf (fp, "\n*****-> UNABLE TO OPEN OUTPUT LOG FILE.");
	m_msg ("UNABLE TO OPEN OUTPUT LOG FILE.", "TST-NOLOGACC");
	z_exit (tnolog, "TST-NOLOGACC");
L2:     fprintf (fp1,
		"\n*****-> UNABLE TO RECEIVE PARAMETER %s, HOST ERROR CODE = %d",
		name,hcode);
    fprintf (fp,
		"\n*****-> UNABLE TO RECEIVE PARAMETER %s, HOST ERROR CODE = %d",
		name,hcode);
    z_exit (tnorcv, "TST-RCVFAIL");
    exit();
L3:     fprintf (fp1,
	  "\n*****-> UNABLE TO RECEIVE PARM BLOCK FROM TM. HOST ERROR CODE = %d ",
    	   hcode);
    fprintf (fp,
   	  "\n*****-> UNABLE TO RECEIVE PARM BLOCK FROM TM. HOST ERROR CODE = %d ",
    	   hcode);
    z_exit (tnoblk, "TST-BRCVFAIL");
}


FUNCTION VOID badint(pname, badi, expint, numint)

    /* PRINT ERROR MESSAGE FOR UNEXPECTED int VALUES. */
    /*       Arguments:			              */

	char	pname[];	/* PARAMETER NAME     */
        struct VARIABLE	*badi;	/* THE BAD VALUE(S)   */
	int expint[];	        /* EXPECTED VALUE(S)  */
	int numint;		/* NUMBER OF intS     */	
	
    {
    int i;

        /* TYPE PARAMETER NAME */

	fprintf (fp1, "\n*****-> UNEXPECTED VALUE FOR '%s'.\n",pname);
	fprintf (fp, "\n*****-> UNEXPECTED VALUE FOR '%s'.\n",pname);
        for (i=0; i<=numint-1; i++) {
	    fprintf(fp1,"\nRECEIVED:  %d",IVAL(*badi,i));
	    fprintf(fp,"\nRECEIVED:  %d",IVAL(*badi,i));
     	    }
	for (i=0; i<=numint-1; i++) {
	    fprintf(fp1,"\nEXPECTED:  %d",expint[i]);
	    fprintf(fp,"\nEXPECTED:  %d",expint[i]);
	    }
	return;
    }




FUNCTION VOID badrea(pname,badr,exprea,numrea)

    /* PRINT ERROR MESSAGE FOR UNEXPECTED REAL VALUES */
    /* ARGUMENTS:				      */

    char        	pname[];	/* PARAMETER NAME     */
    struct VARIABLE	*badr;    	/* THE BAD VALUE(S)   */
    TAEFLOAT            exprea[];	/* EXPECTED VALUE(S)  */
    int            	numrea;		/* NUMBER OF REALS    */
    {

    int i;    
    
    fprintf (fp1,"\n*****-> UNEXPECTED VALUE FOR '%s'.\n", pname);
    fprintf (fp,"\n*****-> UNEXPECTED VALUE FOR '%s'.\n", pname);
    for (i=0; i<=numrea-1; i++) {
	fprintf(fp1,"\nRECEIVED: %f",RVAL(*badr,i));
	fprintf(fp,"\nRECEIVED: %f",RVAL(*badr,i));
            }
    for (i=0; i<=numrea-1; i++) {
	fprintf(fp1,"\nEXPECTED: %f",exprea[i]);
	fprintf(fp,"\nEXPECTED: %f",exprea[i]);
        }
    return;
    } 



FUNCTION VOID badstr(pname,bads,expstr,numstr)

    /* PRINT AN ERROR MESSAGE FOR AN UNEXPECTED STRING VALUE.  
       ARGUMENTS:					      */

    char	pname[];	/* PARAMETER NAME     */
    struct VARIABLE	*bads;	/* THE BAD STRING(S)  */
    char*	expstr[];	/* EXPECTED STRING(S) */
    int    	numstr;		/* NUMBER OF STRINGS  */

    {

    int i;
    
    fprintf(fp1,"\n*****-> UNEXPECTED VALUE FOR '%s'",pname);
    fprintf(fp,"\n*****-> UNEXPECTED VALUE FOR '%s'",pname);
    for (i=0; i<=numstr-1; i++) {
        fprintf(fp1,"\nRECEIVED: %s", SVAL(*bads,i));
        fprintf(fp,"\nRECEIVED: %s", SVAL(*bads,i)); 
        }
    for (i=0; i<=numstr-1; i++) {
        fprintf(fp1,"\nEXPECTED: %s", expstr[i]);
        fprintf(fp,"\nEXPECTED: %s", expstr[i]);
        }
    return;
    }






FUNCTION VOID chkatr(pname, paddr, expcnt, exptyp)

/* THIS ROUTINE WILL OBTAIN THE ATTRIBUTES OF THE SPECIFIED
   PARAMETER AND COMPARE THEM AGAINST THE EXPECTED VALUES.

   CURRENTLY, ONLY "COUNT" AND "TYPE" ARE CHECKED BY CHKATR */


char	pname[];   /* PARAMETER NAME */
struct  VARIABLE *paddr;
int	expcnt;    /* THE NUMBER OF VALUES WE EXPECT THE PARAMETER TO CONTAIN*/
int 	exptyp;    /* THE TYPE OF VARIABLE WE EXPECT THE PARAMETER TO BE*/

{

if (((*paddr).v_count) != expcnt)  
    {
    fprintf(fp, "\n*****-> ERROR IN COUNT FOR PARAMETER %s.\n ", pname);
    fprintf(fp, "EXPECTED %d BUT COUNTED %d", expcnt, (*paddr).v_count);       
    fprintf(fp1, "\n*****-> ERROR IN COUNT FOR PARAMETER %s. ", pname);
    fprintf(fp1, "EXPECTED %d BUT COUNTED %d", expcnt, (*paddr).v_count);      
    z_exit (tattct, "TST_BADCOUNT");
    }


if  (((*paddr).v_type) != exptyp) 
    if ((((*paddr).v_type) == 3) && (((*paddr).v_file) != TRUE))
        {
        fprintf(fp, "\n*****-> ERROR IN TYPE FOR PARAMETER %s.\n ",pname); 
        fprintf(fp, "EXPECTED %d BUT FOUND %d", exptyp, (*paddr).v_type);
        fprintf(fp1, "\n*****-> ERROR IN TYPE FOR PARAMETER %s. ",pname);
        fprintf(fp1, "EXPECTED %d BUT FOUND %d", exptyp, (*paddr).v_type);
        z_exit (tattct, "TST_BADTYPE");
        }
}









	
FUNCTION VOID diskup (DSETNO)

    /* Calling arguments */
    int DSETNO;
    {
    /* This routine reads a parameter block from disk, updates
       values in the block, and writes the block back to disk */
    
    /* Local variables: */
    int  status;

    Vm_ReadFromDisk (params, savblk);
    status = Vm_SetIntg (params, "MULINT", intdim, &exmuli[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetIntg' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetIntg' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDINT");
	};
    status = Vm_SetReal (params, "MULREAL",rldim, &exmulr[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
 	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetReal' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetReal' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDREAL");
	};
    status= Vm_SetString(params,"MULSTRNG",strdim,&exmuls[DSETNO][0],P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetString' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetString' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDSTR");
	};
    status = Vm_SetIntg (params, "DSETNO", 1, &DSETNO, P_UPDATE);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetIntg' to update a ");
	fprintf(fp, "parameter failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetIntg' to update a ");
	fprintf(fp1,"parameter failed. Status = %4d\n", status);
	z_exit(tnoupd, "TST-UPDINT");
	};
    Vm_WriteToDisk ( params, savblk );
    }






FUNCTION VOID diskrd (DSETNO)
    
    /* Calling arguments: */
    int DSETNO;
    
    /* This routine reads a parameter block from disk and
       compares it to an expected set of values (DSETNO) */
    {
    Vm_ReadFromDisk (params, savblk);
    strcpy (name, "DSETNO");
    dsetno = Vm_Find (params, "DSETNO");
    if (dsetno == NULL)
	{
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	z_exit (tnorcv, "TST-RCVFAIL");
	}
    pcheck (DSETNO);
    }






FUNCTION VOID dnbld (DSETNO, RUNTYP)

	/* Calling arguments: */
	int	DSETNO;
	char	RUNTYP[20];

	{
	/* Local arguments:	   */
	int 	status;

	params = Vm_New ( P_CONT );
	status = Vm_DynTutor (params, "VMAPPTEST-BUILD", M_FULLPDF);
	if (status != SUCCESS)
		{
		fprintf (fp, "\n*****-> Attempt to send parameter block to");
		fprintf (fp, " TM failed. Status = %d", status);
		fprintf (fp1, "\n*****-> Attempt to send parameter block to");
		fprintf (fp1, " TM failed. Status = %d", status);
		z_exit (tnosnd, "TST-BSNDFAIL");
		}
	status = Vm_ReadFromTM (params);
	if (strcmp (RUNTYP, batchrt) != 0)
	    {
	    if (status != SUCCESS)
		{
		fprintf (fp,"\n*****-> Unable to receive parm block");
		fprintf (fp," from TM. Status = %d", status);
		fprintf (fp1,"\n*****-> Unable to receive parm block");
		fprintf (fp1," from TM. Status = %d", status);
		z_exit (tnorcv, "TST-BRCVFAIL");
		}
	    pcheck (DSETNO);
	    }
	else if (strcmp (RUNTYP, batchrt) == 0)
		{
		fprintf (fp, "\nBATCH - Checking Vm_ReadFromTM Status...");
		if (status == SUCCESS)
		    {
		    fprintf (fp, "\n*****-> Vm_ReadFromTM passed SUCCESS");
		    fprintf (fp, " in batch, status = %d", status);
		    fprintf (fp1, "\n*****-> Vm_ReadFromTM passed SUCCESS");
		    fprintf (fp1, " in batch, status = %d", status);
		    z_exit (tbqdyn, "TST-DYNOFAIL");
		    }
		else{
		    fprintf (fp, "\n Expected Vm_ReadFromTM failure");
		    fprintf (fp, " from batch, status: %d", status);
		    fprintf (fp1, "\n Expected Vm_ReadFromTM failure");
		    fprintf (fp1, " from batch, status: %d", status);
		    }
		}
	}








FUNCTION VOID dnkill ()

    /* This routine is called when the "DYNKILL" subcommand
       is found in a parameter block received from TM. A block
       of parameters is created, sent to TM and a kill command
       is expected */

    /* Calling arguments: none */
    {
    /* Local arguments:        */
    int status;

    params = Vm_New ( P_CONT );    
    status = Vm_DynTutor (params, "VMAPPTEST-DYNKILL", M_FULLPDF);
    if (status != SUCCESS)
  	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed. %d",
			status);
	fprintf(fp1,"\n*****-> Attempt to send parameter block to TM failed. %d",
			status);
    	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    status = Vm_ReadFromTM (params);
    if (status != P_KILLED)
	{
	fprintf(fp, "\n*****-> Expected P_KILLED status code from Vm_ReadFromTM. ");
	fprintf(fp, "Received: %d", status);
	fprintf(fp1, "\n*****-> Expected P_KILLED status code from Vm_ReadFromTM. ");
	fprintf(fp1, "Received: %d", status);
    	z_exit (tnokil, "TST-NOKILL");
	}
    }











FUNCTION VOID dnsub (DSETNO)

    /* Calling arguments: */
    int DSETNO;

    /* This routine is called when the 'SUBSET' subcommand is
       received in a parameter block from TM. The processing involves
       building a parameter block from scratch, filling it with a
       subset of the parameters defined for the process. The block is
       sent to TM, dynamic tutor is entered, and the block is sent
       back unchanged. The values in the block are then verified. */
    {
    /* Local Variables: */
    TAEINT i;
    TEXT  pname[8];

    params = Vm_New ( P_CONT);
    status=Vm_SetReal (params, "MULREAL", rldim, &exmulr[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetReal' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetReal' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDREAL");
	}
    status=Vm_SetString(params, "MULSTRNG", strdim, &exmuls[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetString' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetString' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDSTR");
	}
    status=Vm_SetIntg (params, "DSETNO", 1, &DSETNO, P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to 'Vm_SetIntg' to add to a ");
	fprintf(fp, "parameter block failed. Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Call to 'Vm_SetIntg' to add to a ");
	fprintf(fp1,"parameter block failed. Status = %4d\n", status);
	z_exit (tnoadd, "TST-ADDINT");
	};
    status=Vm_DynTutor (params, "VMAPPTEST-SUBSET", M_SUBPDF);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed.");
	fprintf(fp, "Status = %4d\n", status);
	fprintf(fp1,"\n*****-> Attempt to send parameter block to TM failed.");
	fprintf(fp1,"Status = %4d\n", status);
	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    Vm_ReadFromTM(params );

    /* Now check values returned */
    strcpy (pname, "MULREAL");
    mulreal = Vm_Find (params, "MULREAL");
    if (mulreal == NULL) goto LDNSUB1;
    chkatr (pname, mulreal, rldim, V_REAL);
    for (i=0; i<rldim; i++)
        if ( !checkreal ((float)RVAL(*mulreal,i),(float)exmulr[DSETNO][i]) )
           {
	    badrea ("MULREAL", mulreal, exmulr[DSETNO], rldim);
            break;
           }
    strcpy(pname, "MULSTRNG");
    mulstr = Vm_Find (params, pname);
    if (mulstr == NULL) goto LDNSUB1;
    chkatr (pname, mulstr, strdim, V_STRING);
    for (i=0; i<strdim; i++)
	if (strcmp(SVAL(*mulstr,i), exmuls[DSETNO][i]) != 0)
	    {
	    badstr ("MULSTRNG", mulstr, exmuls[DSETNO], strdim);
	    break;
	    }
    return;

LDNSUB1:fprintf(fp, "\n*****-> Unable to receive parameter %s. Error=%4d\n",
			pname, status);

	fprintf(fp1,"\n*****-> Unable to receive parameter %s. Error=%4d\n",
			pname, status);
    }








FUNCTION BOOL checkreal ( r1, r2 )

    float r1;
    float r2;
{
#ifdef VAX_VMS 
   	if ( r1 != r2 )
           return ( FALSE );
#else
#ifdef sun
   	if ( r1 != r2 )
           return ( FALSE );
#else
        if ( fabs ( r1 - r2 ) > EPSILON )
           return ( FALSE );
#endif
#endif
	return (TRUE);
}

FUNCTION VOID dnturn (DSETNO)

    /* This routine is called when the DYNTURN subommand is
       received in a parameter block from TM. The processing
       involves building a parameter block from scratch, filling
       it with values consistent with the DSETNO value received
       from TM. The block is sent to TM, dynamic tutor is entered,
       and the block is sent back to VMAPPTEST. The values in the block
       are then compared against the value of DSETNO received back from TM.*/

    /* Calling arguments: */
    int	 DSETNO; 	/* dataset number */
    {
    /* Local arguments:   */

    params = Vm_New (P_CONT);
    status = Vm_SetIntg (params, "MULINT", intdim, &exmuli[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to Vm_SetIntg to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to Vm_SetIntg to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDINT");
	}
    status = Vm_SetReal (params, "MULREAL", rldim, &exmulr[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to Vm_SetReal to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to Vm_SetReal to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDREAL");
	}
    status = Vm_SetString (params, "MULSTRNG", strdim, &exmuls[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to Vm_SetString to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to Vm_SetString to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDSTR");
	}
    status = Vm_SetString (params, "MULFIL", fildim, &exmulf[DSETNO][0], P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to Vm_SetString to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to Vm_SetString to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDFILE");
	}
    DSETNO++;  /* because we decremented before we called dnturn */
    status = Vm_SetIntg (params, "DSETNO", 1, &DSETNO, P_ADD);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Call to Vm_SetIntg to add to a parameter ");
	fprintf(fp, "block failed. Status = %d", status);
	fprintf(fp1, "\n*****-> Call to Vm_SetIntg to add to a parameter ");
	fprintf(fp1, "block failed. Status = %d", status);
	z_exit (tnoadd, "TST-ADDINT");
	}
    status = Vm_DynTutor (params, "VMAPPTEST-DYNTURN", M_SUBPDF);
    if (status != SUCCESS)
	{
	fprintf(fp, "\n*****-> Attempt to send parameter block to TM failed. %d",
				status);
	fprintf(fp1, "\n*****-> Attempt to send parameter block to TM failed. %d",
				status);
 	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    Vm_ReadFromTM (params);
    strcpy (name, "DSETNO");
    dsetno = Vm_Find (params, "DSETNO");
    if (dsetno == NULL)
	{
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER %s. ERROR = %d",
				name, dsetno);
	z_exit (tnorcv, "TST-RCVFAIL");
	}
    pcheck (--IVAL(*dsetno,0));
    }









FUNCTION VOID format (dummy) /* dummy isn't needed; just for temporary compat.*/

    /* Calling arguments */
    int	  dummy;
    {

    /* This routine verifies the correct operation of the
       TAE C callable parameter formatting routines. */

    /* Local Variables: */
    TEXT  pline[75];

    Vm_InitFormat(params);	/* initialize for Vm_FormatVar calls */
    status = SUCCESS;
    while (status == SUCCESS)
	{
	status = Vm_FormatVar (params, pline, 74);
	if (status == SUCCESS)
	    {
	    fprintf(fp, "\n     %s", pline);
	    fprintf(fp1,"\n     %s", pline);
	    }
	else if (status == FP_EOP) return;
	else if (status == FP_NULL)
	    {
	    fprintf(fp, "\n*****-> Parameter or global or local ");
	    fprintf(fp, "variable too long to format into displayable form\n");
	    fprintf(fp1,"\n*****-> Parameter or global or local ");
	    fprintf(fp1,"variable too long to format into displayable form\n");
	    z_exit(-1, "TST-PTOOLONG");
	    }
	else{
	    fprintf(fp, "\n*****-> Unrecognized error code from 'Vm_FormatVar'. ");
	    fprintf(fp, "status = %4d\n", status);
	    fprintf(fp1,"\n*****-> Unrecognized error code from 'Vm_FormatVar'. ");
	    fprintf(fp1,"status = %4d\n", status);
	    z_exit(-1, "TST-PFORPER");
	    }
	}
    }




FUNCTION VOID image()
 
{
/* LOCAL VARIABLES */

struct	IFCB  ifcb;             /* IMAGE FILE CONTROL BLOCK */
struct  VARIABLE *imgfile;  
TEXT    labbuf[labsiz+1];	/* RECEIVE DATA BUFFER */	
TEXT    explab[labsiz+1];       /* A DATA BUFFER */
char	buffer[100];            /* A DATA BUFFER */
int	line, chan;
TEXT	hfile [FSPECSIZ+1];     /* ARRAY TO RECEIVE TRANSLATED HOST FILE*/
TEXT	taefile [FSPECSIZ];     /* FILESPEC TO BE TRANSLATED */
int     stat;

    strcpy (explab, "Test image label.");
    imgfile = Vm_Find(params, "IMGFILE");
    if(((*imgfile).v_file != TRUE) || (imgfile == NULL))
	{
	fprintf (fp,"\n*****->UNABLE TO ACCESS IMAGE FILE NAME PARAMETER");
	fprintf (fp1,"\n*****->UNABLE TO ACCESS IMAGE FILE NAME PARAMETER");
        z_exit(timwrt, "TST-IWRTFAIL");
	}

    strcpy (taefile, SVAL(*imgfile,0));
    p_file (taefile, (*imgfile).v_filemode, hfile);
    stat = i_opou(&ifcb, lun, hfile, 2, imchan, imline, linsiz, imlabl,labsiz);
    if (stat != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T OPEN IMAGE FILE FOR OUTPUT.");
	fprintf (fp1,"\n*****->CAN'T OPEN IMAGE FILE FOR OUTPUT.");
        z_exit(timwrt, "TST-IWRTFAIL");
        }


    /********************************************************************
    * WRITE A 7 BAND/10 LINE PER BAND IMAGE TO THE IMAGE FILE           *
    ********************************************************************/

    
    if (i_wrlb (&ifcb, explab, 1) != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T WRITE IMAGE FILE LABEL RECORD.");
	fprintf (fp1,"\n*****->CAN'T WRITE IMAGE FILE LABEL RECORD.");
	z_exit(tilblw, "TST-ILABWRIT");
	}
    for (chan = 1; chan <= 7; ++chan)
	{
	for (line = 1; line <= 10; ++line)
	    {
	    buffer[0] = chan;
	    buffer[1] = line;
	    buffer[linsiz/2-1] = line;
	    buffer[linsiz-1] = line;
	    stat = i_write (&ifcb, buffer, chan, line, 1);
	    if (stat != SUCCESS)
		{
		fprintf (fp, "\n*****-> Error writing an image line ");
		fprintf (fp, "to image file (i_write).");
		fprintf (fp1, "\n*****-> Error writing an image line ");
		fprintf (fp1, "to image file (i_write).");
		fprintf (fp, "\n*****->Host error code = %d", i_herr (&ifcb));
		fprintf (fp1, "\n*****->Host error code = %d", i_herr (&ifcb));
		z_exit (tilacc, "TST-IMGWRITE");
		}
	    if (i_wait (&ifcb) != SUCCESS) goto ERR3;
 	    }
 	}
    i_clse (&ifcb, I_SAVE);

    /*****************************************************************
    *          NOW OPEN THE FILE AND READ ALL THE RECORDS            *
    *****************************************************************/

    stat = i_opin (&ifcb, lun, hfile, I_INPUT);
    if (stat != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T OPEN IMAGE FILE FOR INPUT.");
	fprintf (fp1,"\n*****->CAN'T OPEN IMAGE FILE FOR INPUT.");
        z_exit(tilblr, "TST-ILABREAD");
        }
    if (i_rdlb (&ifcb, labbuf, 1) != SUCCESS)
	{
	fprintf (fp,"\n*****->CAN'T READ IMAGE FILE LABEL");
	fprintf (fp1,"\n*****->CAN'T READ IMAGE FILE LABEL");
	z_exit (tilblr, "TST-ILABREAD");
	}
    if (strcmp (labbuf ,explab) != 0)
	{
	fprintf (fp,"\n*****->IMAGE FILE LABEL MISMATCH."); 
	fprintf (fp1,"\n*****->IMAGE FILE LABEL MISMATCH."); 
	z_exit (tilblm, "TST-ILABMTCH");
	}
    for (chan = 1; chan <= 7; ++chan)
	{
	for (line = 1; line <= 10; ++line)
	    {
	    stat = i_read (&ifcb, buffer, chan, line, 1);
	    if (stat != SUCCESS)
		{
		fprintf (fp, "\n*****->Error reading image file(i_read).");
		fprintf (fp1, "\n*****->Error reading image file(i_read).");
		fprintf (fp,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
		fprintf (fp1,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
		z_exit (tilacc, "TST-IMGREAD");
		}		
     	    if (i_wait (&ifcb) != SUCCESS)   goto ERR1;
	    if ( (buffer[0] != (char)chan) 
	      || (buffer[1] != (char)line)
	      || (buffer[linsiz/2-1] != (char)line) 
	      || (buffer[linsiz-1] != (char)line) )
		goto ERR2;
	    }
	}
    i_clse (&ifcb, I_DEL);
    return;

    /* ERROR HANDLING */
    
ERR1:	fprintf (fp,"\n*****->ERROR WAITING FOR IMAGE FILE READ (I_WAIT).");
	fprintf (fp1,"\n*****->ERROR WAITING FOR IMAGE FILE READ (I_WAIT).");
	fprintf (fp,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
	fprintf (fp1,"\n*****->HOST ERROR CODE = %d",i_herr (&ifcb));
	z_exit (tilacc, "TST-IMGRWAIT");

ERR2:   fprintf (fp,"\n*****->IMAGE DATA WRITTEN DOES NOT MATCH DATA READ.");
        fprintf (fp1,"\n*****->IMAGE DATA WRITTEN DOES NOT MATCH DATA READ.");
        z_exit (tdata, "TST-NOMATCH");

ERR3:   fprintf (fp, "\n*****-> ERROR WAITING FOR IMAGE FILE WRITE(I_WRITE).");
	fprintf (fp1,"\n*****-> ERROR WAITING FOR IMAGE FILE WRITE(I_WRITE).");
	fprintf (fp, "\n*****->HOST ERROR CODE = %d", i_herr (&ifcb));
	fprintf (fp1, "\n*****->HOST ERROR CODE = %d", i_herr (&ifcb));
	z_exit (tilacc, "TST-IMGWWAIT");
	}





FUNCTION VOID named()
    /* THIS ROUTINE IS CALLED WHEN THE "NAME" SUBCOMMAND IS ENCOUNTERED.
       NAMED WILL READ A PARAMETER FROM THE PARAMETER BLOCK WHICH IS A 
       NAMED PARAMETER.  THE VALUE OF THE PARAMETER IS COMPARED AGAINST 
       AN EXPECTED VALUE AND THEN SET TO A NEW VALUE.  THE BLOCK IS THEN
       SENT BACK TO TM.                                                 */

{
    /* LOCAL ARGUMENTS */

struct VARIABLE *s;
int    status;
TEXT   namest[40];

    s = Vm_Find(params, "S");
    if(s == NULL) 
        {
	fprintf (fp1,"\n*****-> UNABLE TO RECEIVE PARAMETER 'S'. ");
	fprintf (fp,"\n*****-> UNABLE TO RECEIVE PARAMETER 'S'. ");
        z_exit (tnorcv, "TST-NAMEMTCH");
        }

    strcpy (namest, SVAL(*s,0));
    if (strcmp (namest, exname[0]) != 0)
	{
	badstr ("S", s, exname, 1);
	z_exit (tdata, "TST-NAMEMTCH");
	}
    status = Vm_SetString(params, "S", 1, &retstr[0], P_UPDATE);
    if (status != SUCCESS)
        {
        fprintf (fp1,"\n*****-> CAN'T UPDATE PARAMETER 'S' IN BLOCK.");
	fprintf (fp1," STATUS = %d ", status);
        fprintf (fp,"\n*****-> CAN'T UPDATE PARAMETER 'S' IN BLOCK.");
	fprintf (fp," STATUS = %d ", status);
        z_exit (tnoupd, "TST-UPDSTR");
        }

    status = Vm_SetIntg(params, "$SWITCH", 1, &swval, P_UPDATE);
    if (status != SUCCESS)
	{
        fprintf (fp1,"\n*****->CAN'T UPDATE GLOBAL '$SWITCH' IN BLOCK.");
	fprintf (fp1," STATUS = %d ", status);
        fprintf (fp,"\n*****->CAN'T UPDATE GLOBAL '$SWITCH' IN BLOCK.");
	fprintf (fp," STATUS = %d ", status);
        z_exit (tnoupd, "TST-UPDSWTCH");
	}

    status = Vm_SetTCLVar(params);
    if (status != SUCCESS)
   	{
	fprintf (fp1,"\n*****->ATTEMPT TO SEND PARAMETER BLOCK ");
	fprintf (fp1,"TO TM FAILED. HOST ERROR = %d", status);
	fprintf (fp,"\n*****->ATTEMPT TO SEND PARAMETER BLOCK ");
	fprintf (fp,"TO TM FAILED. HOST ERROR = %d", status);
	z_exit (tnosnd, "TST-BSNDFAIL");
	}
    }   




FUNCTION VOID pcheck (exdset)
    int exdset;

    {
    /* THIS ROUTINE IS CALLED DURING ANY TEST WHICH REQUIRES A
       COMPARISON OF THE PARAMETERS RECEIVED FROM TM AND THE
       HARDCODED EXPECTED VALUES.				*/

    char 	pname[10];
    int 	i;
    TEXT        hfile [FSPECSIZ+1];

    /******************************************************
    *     GET AND CHECK EACH EXPECTED PARAMETER		  *
    *		- MULINT, a multivalue int value      *
    *		- MULREAL, a multivalue REAL parameter    *
    *		- MULSTRNG, a multivalue string parameter *
    *		- MULFIL, a multivalue filespec           *
    ******************************************************/


    strcpy(pname, "DSETNO");
    dsetno = Vm_Find(params,"DSETNO");
    if(dsetno == NULL) goto L100;
    chkatr(pname, dsetno, 1, V_INTEGER);
    if(exdset != IVAL(*dsetno,0)) 
	{
	badint ("DSETNO", dsetno, &exdset, 1);
 	goto L50;
    	}
    strcpy(pname, "MULINT");
    mulint = Vm_Find(params,"MULINT");
    if(mulint == NULL) goto L100;
    chkatr(pname, mulint, intdim, V_INTEGER);
    for (i=0; i< intdim; i++) 
	if (IVAL(*mulint,i) != exmuli [IVAL(*dsetno,0)][i]) 
	    {
	    badint ("MULINT", mulint, exmuli[IVAL(*dsetno,0)], intdim);
       	    break;
	    };
L50:  
    strcpy(pname,"MULREAL");
    mulreal = Vm_Find (params, "MULREAL");
    if (mulreal == NULL) goto L100;
    chkatr (pname, mulreal, rldim, V_REAL);
    for (i=0; i< rldim; i++)                                        
   	if ( !checkreal ( (float)RVAL(*mulreal,i) , 
                          (float)exmulr[IVAL(*dsetno,0)][i] ) )
	    {
	    badrea ("MULREAL", mulreal, exmulr[IVAL(*dsetno,0)], rldim);
	    break;
	    };
    strcpy(pname, "MULSTRNG");
    mulstr = Vm_Find (params, "MULSTRNG");
    if (mulstr == NULL) goto L100;
    chkatr (pname, mulstr, strdim, V_STRING);
    for (i=0; i< strdim; i++)
	if (strcmp(SVAL(*mulstr,i),exmuls [IVAL(*dsetno,0)][i]) != 0) 
	    {
	    badstr ("MULSTRNG", mulstr, exmuls [IVAL(*dsetno,0)], strdim);
	    break;
	    };
    strcpy(pname, "MULFIL");
    mulfil = Vm_Find (params, "MULFIL");
    if (mulfil == NULL) goto L100;
    chkatr (pname, mulfil, fildim, V_STRING);
    for (i=0; i< fildim; i++)
        {
        p_file (SVAL(*mulfil,i), (*mulfil).v_filemode, hfile);
	if (strcmp (hfile, exmulf [IVAL(*dsetno,0)][i]) != 0)
	    {
	    badstr ("MULFIL", mulfil, exmulf [IVAL(*dsetno,0)], fildim);
	    break;
	    };
        }
    return;
L100: fprintf (fp1,"\n*****->UNABLE TO ACCESS PARAMETER %s", pname);
      fprintf (fp,"\n*****->UNABLE TO ACCESS PARAMETER %s", pname);
      return;
    }




FUNCTION VOID tstcls ()
{
    /*  Note on the screen and in the log, whether the Parameter Check
	Self Test failed or passed. This routine is meant to be called
	after all iterations of the test are completed.
	This routine does a text search for certain keywords indicating
	failure. Now enhanced to look for pairs of '+++++' and '-----'.
	If these symbols do not occur in pairs, first '+++++' then
	'-----', an VMAPPTEST run has aborted or exited on an '*****' message.*/

    /* Local variables:  */
    int		failed=FALSE;	/* TRUE if test failed */
				/* start by assuming test didn't fail */
    char	string[MAXSTRSIZ+1];	/* Text line buffer    */
    char	token[21];
    int		plus=FALSE;	/* '+++++' tracker 	*/
    int		minus=TRUE;	/* '-----' tracker	*/
    char	pname[10];
    char	stdfil[80];	/* File name		*/
    int		flag;		/* dummy flag		*/
    int		fd;
    struct VARIABLE *stdfile;	/* STDFILE implicit parameter pointer    */

    stdfile=Vm_Find(params,"STDFILE");
    if (stdfile==NULL) goto L810;
    fd = open (SVAL(*stdfile, 0), 0);               /* 0 is read mode    */
    if (fd < 0) goto L800;
    while ( (flag = getline(fd,string,MAXSTRSIZ)) != 0) /* get next line */
	{
	if (flag<0) goto L850;                          /* read error    */
	if (strncmp(string,"*****->",7) == 0)
	    {
	    failed=TRUE;
	    break;
	    }
 	/* Initial plus ok, but should always see a minus before another
	   plus. Also we do not check for the case of no +++++ or -----;
	   this is so that we can still use this for old VMAPPTEST.DATs and
	   other sundry files such s CTCLTESTB.LOG. Can't write them until
	   we open anyway so we have a WINDOW OF VULNERABILITY anyway*/
	if (strncmp(string,"+++",3) == 0)
	    {
	    if (plus || (!minus))
		{
		failed=TRUE;
		break;
		};
	    plus=TRUE;
	    minus=FALSE;
	    };
	if (strncmp(string,"---",3) == 0)
	    {
	    if (minus || (!plus))
		{
		failed=TRUE;
		break;
		}
	    plus=FALSE;
	    minus=TRUE;
	    }
	} /* end while */
        close (fd);
	t_bell();
	if (failed)
	    {
	    t_bell();
	    fprintf(fp, "\n*** Vm_ PACKAGE TEST UNSUCCESSFUL\n");
	    }
	else{
	    fprintf(fp, "\nVm_ PACKAGE TEST COMPLETED SUCCESSFULLY\n");
	    }
	return;
/* Error Handling */
L800:	fprintf(fp,"\nCOULDN'T OPEN LOG FILE WHILE CLOSING OUT TEST\n");
	z_exit(topen, "TST-LOGOPEN");
L810:	fprintf(fp, "\nLOG FILE NAME PARM ERROR WHILE CLOSING OUT TEST\n");
	z_exit(topen, "TST-LOGPARM");
L850:	fprintf(fp, "\nLOG FILE READ ERROR WHILE CLOSING OUT TEST\n");
	z_exit(tread, "TST-LOGREAD");
}






FUNCTION VOID termnl()
    /* This routine exercises the TAE terminal access routines */
    /* Calling arguments: none */
    {
    /* Local   arguments: */
    int i;
    
    t_clear  ();
    t_output (8, 30, "THIS IS A");	/* write line to screen */
    t_output (9, 30, "TEST OF THE");
    t_output (10, 30, "TAE TERMINAL");
    t_output (11, 30, "  ROUTINES");
    t_bell ();
    for (i=27; i<=45; i++)
	{
	t_pos (6, i);
 	t_write ("X", T_NULL);
	}
    t_bell ();
    for (i=27; i<=45; i++)
	{
	t_pos (13, i);
	t_write ("X", T_NULL);
	}
    t_bell ();
    for (i=7; i<=12; i++)
	{
	t_pos (i, 27);
	t_write ("XX", T_NULL);
	}
    t_bell ();
    for (i=7; i<=12; i++)
	{
	t_pos (i, 44);
	t_write ("XX", T_NULL);
	}
    t_bell ();
    t_bell ();
    }








FUNCTION VOID tstexs ()

	/* Calling arguments: none */

	/* THIS ROUTINE TO BE USED FOR BATCH TESTING. IT IS USED
	   TO "SYNCHRONIZE" BATCH SUBMISSIONS TO AVOID FILE ACCESS
	   COLLISIONS. 

	   WITHIN A LOOP WE WAIT 5 SECONDS AND OPEN THE FILE NAMED
	   BY THE PARAMETER checkf. THE LOOP IS EXECUTED UP TO limit
	   NUMBER OF TIMES OR
		FOR look = "NOEXIST"
		    UNTIL checkf DOES NOT EXIST.
		FOR look = "EXIST"
		    UNTIL checkf DOES EXIST.

	   AFTER WE HAVE TRIED limit NUMBER OF TIMES, WE "TIME OUT". */

	{
	
	/* Local arguments:        */
	struct VARIABLE *checkf, *limit, *look;
        FILE    *fp2;
	int     i;
	char	pname[80];

	strcpy (pname, "CHECKFIL");
	checkf = Vm_Find (params, "CHECKFIL");
	if (checkf == NULL) goto ERR6;
	strcpy (pname, "LIMIT");
	limit = Vm_Find  (params, "LIMIT");
	if (limit ==  NULL) goto ERR6;
	strcpy (pname, "LOOK");
	look  = Vm_Find  (params, "LOOK");
	if (look == NULL) goto ERR6;
	
	fprintf (fp, "\n\nWAITING FOR FILE: %s", SVAL(*checkf,0));
	fprintf (fp, "\nLOOP LIMIT: %d", IVAL(*limit,0));
	fprintf (fp, ". FILE MUST %s", SVAL(*look,0));
	for (i=1; i<=IVAL(*limit,0); i++)
	    {
	    apwait (waitdelta);  /* WAIT A LITTLE WHILE */
	    fp2 = fopen (SVAL(*checkf,0) , "r");
	    if ((fp2==NULL) && (strcmp(SVAL(*look,0),"NOEXIST")==0)) return;
	    if  (fp2==NULL) continue;     /* keep checking til timeout */

/* successfully opened the file, was it supposed to be there ? */
	    if (strcmp (SVAL(*look,0), "EXIST") == 0)    /* success */
		{
		fclose (fp2);
		return;
		}
	    else if (strcmp(SVAL(*look,0),"NOEXIST") == 0) 
                {
                fclose (fp2);    /* file not supposed to exist, so close */
                continue;        /*   and hope it goes away soon...      */
                }
            else                        /* neither EXIST nor NOEXIST... */
                {
        	strcpy (pname, "LOOK");
                goto ERR6;
                }
	    }
	fprintf (fp, "\n*****-> TIME OUT");
	m_msg ("VMAPPTEST-POLL timed out", "TST-POLLTIMO");
	z_exit (topen, "TST-POLLTIMO");

ERR6:	fprintf (fp, "\n*****-> %s: PARM ERROR IN POLL", pname);
	z_exit (tnorcv, "TST-POLLPARM");
	}



FUNCTION VOID apwait (delta)    /* WAIT delta seconds */
    int delta;
    {
    int status;
    status = wait_hold (delta*1000);    /* wait_hold expects milliseconds */
    if (status != SUCCESS)
        {
	fprintf (fp, "\n*****-> wait_hold failed with status=%d\n", status);
	z_exit (topen, "TST-POLLTIMO");
        }
    return;
    }


/* looks a lot like read, but stops at a newline boundary */

FUNCTION static int getline (fd, buf, maxsiz)
   int       fd;
   GENPTR    buf;
   int       maxsiz;
   {
   int i=0;
   int j;
   while ( (j = read (fd, &buf[i], 1)) > 0 )
      {
      if (buf[i] == '\n') { buf[i+1] = EOS; return (i+1); }
      i++;
      if (i >= maxsiz) { printf ("internal size error\n"); return (-1); }
      }
   return (j);
   }

C
C	TAE FORTRAN template
C
C 	This program first receives initial values of 
C  	variables from the user. Then it reconstructs 
C	V-block to transmit back to TAE for dynamic parameters. 
C	Finally it receives parameter values by receiving 
C	V-block back from TM.
C
C
C 	The PDF for this program is:
C 
C	process		help=*
C	parm file	type=file 	default="default-file"
C 	parm integer	type=integer	default=1
C 	parm real	type=real	default=1.0
C 	parm string	type=(string,20) default="default-string"
C 	end-proc
C
C
C	template.for and template.pdf files are in the tae$tools 
C	directory for VMS and $TTOOLS directory for UNIX
C
C

	IMPLICIT INTEGER (A-Z)
	INCLUDE '$TINC/PGMINC.FIN'
	INTEGER STATUS
	INTEGER VBLOCK(xprdim)
	CHARACTER*132 STRING
	CHARACTER*132 TAEFILE, HOSTFILE
	INTEGER  FCOUNT, SCOUNT, ICOUNT, RCOUNT
	INTEGER  INTG
	REAL	 REAL

C	Receive initial values of variables from the user through TM 

C	Initialize for the application program	
C	to write to the standard output device
	CALL XZINIT (VBLOCK, xprdim, 6, xabort, STATUS)

C	Translate the user-entered file name to a host file name
	CALL XRFILE (VBLOCK, 'FILE', 1, TAEFILE, TAELEN, 
     1		     HOSTFILE, HOSTLEN, FCOUNT, STATUS)
    
C	Retrieve integer variables from a V-block
	CALL XRINTG (VBLOCK, 'INTEGER', 1, INTG, ICOUNT, STATUS)

C	Retrieve real variables from a V-block
	CALL XRREAL (VBLOCK, 'REAL', 1, REAL, RCOUNT, STATUS)

C	Get values of string or keyword
	CALL XRSTR  (VBLOCK, 'STRING', 1, STRING, STRLEN, SCOUNT,
     1  	     STATUS)
	
C
C
C	Reconstruct V-block to transmit back to TAE 
C	for dynamic parameters

C	Initialize a V-block in preparation for other XQ calls.
	CALL XQINI (VBLOCK, xprdim, xabort)

C	Set file name string into the V-block.
	CALL XQFILE (VBLOCK, 'FILE', 1, TAEFILE, xadd, STATUS)

C	Add an integer variable name to the V-block
	CALL XQINTG (VBLOCK, 'INTEGER', 1, INTG, xadd, STATUS)

C	Add a real variable to the V-block
	CALL XQREAL (VBLOCK, 'REAL', 1, REAL, xadd, STATUS)

C	Add a string variable to the V-block
	CALL XQSTR  (VBLOCK, 'STRING', 1, STRING, xadd, STATUS)

C	Send the V-block to the TM with a set of initial values 
C	for a dynamic tutor session
	CALL XQDYNP ('TEMPLATE', VBLOCK, xfulpd, STATUS)

C	Receive parameter values by receiving VBLOCK from TM
	CALL XRINIM (VBLOCK, xprdim, xabort, STATUS)

	END

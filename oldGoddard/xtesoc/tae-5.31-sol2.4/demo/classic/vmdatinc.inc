/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



	/******************************************** 
	*  vmapptest.C DATA DECLARATION INCLUDE FILE  *
        ********************************************/


static TAEINT	exmuli [numds][intdim] ={ 
					{1,2,3},
					{9,8,7},
					{341,10041,9999},
					{-5,456,-9999}
					};  /* MULINT */


static TAEFLOAT	exmulr [numds][rldim] ={
					{.1,.2,.3},
					{.9,.8,.7},
					{17E6,0.0006,4E-5},
					{-17.5,12E12,5.}
				        }; /* MULREAL */


static TEXT	*exmuls [numds][strdim] ={
					 {"s1","s2","s3"}, /* DATASET 1 */
					 {"s4","s5","s6"}, /* DATASET 2 */
					 {"ST","UV","WX"}, /* DATASET 3 */
					 {".:","?!","()"}  /* DATASET 4 */
					}; /* MULSTR */


static TEXT	*exmulf [numds][fildim] ={
					 {"cappfile1.tmp","cappfile2.tmp"},
					 {"cappfile3.tmp","cappfile4.tmp"},
					 {"cappfile5.tmp","cappfile6.tmp"},
					 {"cappfile7.tmp","cappfile8.tmp"}
					};



static char	*exname [] = {"HELLO, TAETEST SCRIPT FILE HERE..."};
static char	*retstr [] = {"HELLO TAETEST SCRIPT, vmapptest EXE HERE..."};
static char	savblk [] = "vmapptest.PAR";
static int  swval     = 1234;

/*THE ABOVE VALUES ARE USED BY vmapptest FOR PARAMETER VALIDATION.
  IF ANY OF THESE VALUES ARE CHANGED, THEN vmapptest.SCR SHOULD ALSO
  BE CHANGED. THE VALUES ARE IN LOCAL ARRAYS WHICH ARE USED DURING
  EACH ACTIVATION BY ALL vmapptest ROUTINES.			   */




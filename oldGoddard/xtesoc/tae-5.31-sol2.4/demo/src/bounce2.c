/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/**********************************************************************/
/*  Program:  bounce2						      */
/*  To run:							      */
/*    1. Make sure that you are set to a directory with the files:    */
/*       bounce2, bounce.res, bounce.hlp#####			      */
/*       These files (or files to make them from) are all in $TDEMO   */
/*    2. At the shell prompt in an xterm window enter the program     */
/*       name: bounce2						      */
/*								      */
/*  Description:  This is an example program.  Its purpose is to      */
/*  illustrate various features of the TAE Plus WPT library.  It      */
/*  is the second in a series of three examples; each example in      */
/*  the series builds on the previous example.			      */
/*								      */
/*  These examples are all somewhat complex.  For examples of the     */
/*  simplest possible programs see the WPT release notes or the       */
/*  example program "minwpt".					      */
/*								      */
/*  The example scenario used for this program is a ball bouncing     */
/*  around inside a rectangle.  This second program in the series     */
/*  adds a paddle in the rectangle.  The paddle may be moved about    */
/*  by use of the mouse.  In addition to the features shown by	      */
/*  bounce1, this program illustrates the following features of WPT,  */
/*  the X library:						      */
/*    1. Processing mouse events from a WPT workspace.		      */
/*    2. Using mouse events for direct manipulation of a graphic      */
/*    object (the paddle).					      */
/* 								      */
/* Change Log:
 *
 *	19-jul-89	Removed include of /sys/time.h...ljn
 *	 8-Dec-90	changed TDEMO to TAEDEMORES for res file...baw
 *	11-Jun-91 PR883	add t_pinit...nci
 *      13-jul-93       Remove all function declarations for Vm_, Wpt_, Co_, 
 *                      and t_ routines since these declarations are now
 *                      available in TAE header files.  Some declarations 
 *                      in this demo (i.e. t_pinit) didn't match those in the 
 *                      header file(s) thus causing compilation problems...ws
 *
 */
/**********************************************************************/

#include 	"taeconf.inp"  
#include 	"wptinc.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 
  
#ifdef VMS
#define BOUNCE_RES "$TAEDEMORES:bounce.res"
#else
#define BOUNCE_RES "$TAEDEMORES/bounce.res"
#endif

#define EVENT_HANDLER 		/* a flag for documentation */  
 
/*	macros for access to parameter values 
 
These macros obtain parameter values given the name of 
a Vm object and the name string of the parameter. 
The Vm objects are created by the initializePanels function 
below. 
 
*/ 
  
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vm, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vm, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vm, name), 0))  
  
  
/* 
 
Reference scalar parameters as follows: 
   
 	StringParm(myPanelTarget, "s")    -- string pointer   
 	IntParm(myPanelTarget, "i")	  -- integer value   
 	RealParm(myPanelTarget, "r")	  -- real value   
   
 For vector parameters, do the following: 
   
 	TAEINT	*ival; 
 	ival = &IntParm(myPanelTarget, "i"); 
 	printf ("%d %d %d", ival[0], ival[1], ival[2]); 
 
*/  
 
/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
static Id bounceTarget, bounceView, bounceId; 
EVENT_HANDLER bounce_uball (); 
EVENT_HANDLER bounce_vball (); 
EVENT_HANDLER bounce_grav (); 
EVENT_HANDLER bounce_elas (); 
EVENT_HANDLER bounce_box (); 
EVENT_HANDLER bounce_run (); 
EVENT_HANDLER bounce_stop (); 
EVENT_HANDLER bounce_reset (); 
EVENT_HANDLER bounce_quit (); 

/*	Dispatch Table: one per panel 	*/ 
 
typedef VOID (*FUNCTION_PTR) (); 
static struct DISPATCH  
    { 
    TEXT		*parmName; 
    FUNCTION_PTR 	eventFunction; 
    } ;
 
struct DISPATCH bounceDispatch[] = {
{"uball", bounce_uball}, 
{"vball", bounce_vball}, 
{"grav", bounce_grav}, 
{"elas", bounce_elas}, 
{"box", bounce_box}, 
{"run", bounce_run}, 
{"stop", bounce_stop}, 
{"reset", bounce_reset}, 
{"quit", bounce_quit}, 
 
    {NULL, NULL}		/* terminator entry */ 
    }; 

#define	line( x, x2, y2, x1, y1 )	\
((y2)-(y1)) * ((x)-(x1)) / ((x2)-(x1)) + (y1)
#define	PERIOD	500		/* Update period in milliseconds.   */
#define	BALLSIZE	4
#define	PSIZE		34

WptPanelEvent		wptEvent;	/* Event information from WPT */

Window		box_W;

TAEFLOAT	xball, yball, uball, vball, grav, elas;
TAEINT		xsize, ysize;
int		xpaddle, ypaddle;
int		run = FALSE;	/* Flag to indicate start/stop of     */
                                /* moving ball.			      */
int		grabpaddle = FALSE;  /* Flag TRUE when moving paddle  */


XPoint	 	ball[] = {  
{           0,           0 },
{ -BALLSIZE/2, -BALLSIZE/2 },
{           0,    BALLSIZE },
{    BALLSIZE,           0 },
{           0,   -BALLSIZE },
{   -BALLSIZE,           0} };


XPoint 	   	paddle[] = {
{ 	0,      	0 },
{ 	0, 	  PSIZE/2 },
{ 	0,  	  -PSIZE  } };


/*  Graphic Contexts  */
GC	gcContext1;			/* with Black foreground  */
GC	gcContext2;			/* with White foreground  */


/* Display information  */
Display	  *display;		/* current display 	  */
int	  screen;		/* default Screen  number */

 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
struct DISPATCH          *dp;           /* working dispatch pointer  */ 
struct VARIABLE          *parmv;	/* pointer to event VARIABLE */ 
CODE                     code; 
 
    XEvent		*p_xevent;
    TEXT		*parmName;
    int			tlines,tcols,ttype;


    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower (FALSE);	/* permit upper/lowercase file names */

/*  Initialize X and Wpt */

    display = Wpt_Init(NULL);
    screen = DefaultScreen(display);

/* initialize graphic contexts we will be needing */
    initGC();

    initializePanels (); 
 
/*  Initialize the workspace display.				      */

    initball();
    plotball();
    initpaddle();
    plotpaddle();

    /*	main event loop */ 
 
/*  A primary characteristic of a WPT (or any interactive) program is */
/*  an event loop.  In an event loop there is one place that receives */
/*  (almost) all events, classifies them, and calls the appropriate   */
/*  processing function for each event.  The events received by the   */
/*  Wpt_NextEvent call maybe raw X events or WPT events.  WPT 	      */
/*  events are actually subtypes of X events.			      */

    Wpt_SetTimeOut(PERIOD);			/* specify timeout period */
    while( TRUE )
        {
/*      Wait for, then read next event.				      */

        Wpt_NextEvent(&wptEvent);		/* wait with timeout */ 

/*      Process timer time-out.					      */
	if (wptEvent.eventType == WPT_TIMEOUT_EVENT) 
	    {
	    if( run )
                {
                unplotball();
                moveball();
                plotball();
                plotpaddle(); 
                }
            }

	else if( wptEvent.eventType != WPT_PARM_EVENT )
               continue;
	else
	    {
/*          Process WPT panel events.				      */
            if( wptEvent.p_panelId == bounceId )
                {
                parmName = wptEvent.parmName;


/*              Process events from the workspace "box".	    */
/*              Note that the X events from the workspace window    */
/*              are returned as a component of the WPT Panel event. */

                if( s_equal( parmName, "box" ) )
                    {
                    p_xevent = &(wptEvent.p_xEvent);
                    if( (*p_xevent).type == ButtonPress )
                        {
                        grabpaddle = TRUE;
                        unplotpaddle();
                        xpaddle = (*(XButtonPressedEvent *)p_xevent).x;
                        ypaddle = (*(XButtonPressedEvent *)p_xevent).y;
                        plotpaddle();
                        }
                    else if( (*p_xevent).type == ButtonRelease )
                        grabpaddle = FALSE;
                    else if( (*p_xevent).type == LeaveNotify )
                        grabpaddle = FALSE;
                    else if( (*p_xevent).type == MotionNotify && grabpaddle )
                        {
                        unplotpaddle();
                        xpaddle = (*(XPointerMovedEvent *)p_xevent).x;
                        ypaddle = (*(XPointerMovedEvent *)p_xevent).y;
                        plotpaddle();
                        }
                    else if( (*p_xevent).type == Expose )
                        {
                        plotball();
                        plotpaddle();
                        }
                    }

	/*	Panel event has occurred.  Lookup the event in the 
	 *	dispatch table and call the associated  
 	 *	event handler function. 
 	 */ 
 
                else
                    {
                    dp = (struct DISPATCH *) wptEvent.p_userContext;  
                    for (;  (*dp).parmName != NULL;   dp++)
                        if (s_equal ((*dp).parmName, wptEvent.parmName))  
		        { 
	                parmv = Vm_Find (wptEvent.p_dataVm, 
				wptEvent.parmName); 
		        (*(*dp).eventFunction)  
			    ((*parmv).v_cvp, (*parmv).v_count); 
		        break; 
		        }
                    }
                }
            }
        }
} 

FUNCTION VOID initializePanels ()
  
{
vmCollection = Co_New (P_ABORT);
Co_ReadFile (vmCollection, BOUNCE_RES, P_ABORT);
bounceView = Co_Find (vmCollection, "bounce_v");
bounceTarget = Co_Find (vmCollection, "bounce_t");
bounceId = Wpt_NewPanel ("", bounceTarget, 
		bounceView, NULL, bounceDispatch, 0);

/*  Get ID of workspace subwindow with PARM name "box".		      */

    box_W   = Wpt_ItemWindow( bounceId, "box" );
    xsize   = IVAL( *Vm_Find( bounceView, "box.size" ), 0 );
    ysize   = IVAL( *Vm_Find( bounceView, "box.size" ), 1 );

/*  Specify X window events to be received from the workspace.	    */
/*  Note that the workspace is an X window.			    */

    XSelectInput( display, box_W, ButtonPressMask | ButtonReleaseMask
                  | ButtonMotionMask | LeaveWindowMask | ExposureMask ); 
}
 
/*	event handlers */ 
 
EVENT_HANDLER bounce_uball (value, count)
 
	TAEFLOAT	value[];	/* real vector          */
	FUNINT		count;		/* number of reals      */
{
/*  Set new value for velocity components.		      */

    uball = RVAL( *Vm_Find( bounceTarget, "uball" ), 0 );
}


EVENT_HANDLER bounce_vball (value, count)
 
	TAEFLOAT	value[];	/* real vector          */
	FUNINT		count;		/* number of reals      */
{
    vball = RVAL( *Vm_Find( bounceTarget, "vball" ), 0 );
}


EVENT_HANDLER bounce_grav (value, count)
 
	TAEFLOAT	value[];	/* real vector          */
	FUNINT		count;		/* number of reals      */
{
/*              Set new value for gravity.			      */

    grav = RVAL( *Vm_Find( bounceTarget, "grav" ), 0 );
}


EVENT_HANDLER bounce_elas (value, count)
 
	TAEFLOAT	value[];	/* real vector          */
	FUNINT		count;		/* number of reals      */
{
/*  Set new value for elasticity.			      */
/*  Note that elas must be reset after the reject	      */

    elas = RVAL( *Vm_Find( bounceTarget, "elas" ), 0 );
     if( elas < 0.85 || 1.05 < elas )
        Wpt_ParmReject( bounceId, "elas",
                   "Invalid elasticity.  Must be 0.85 <= elas <= 1.05\n" );
    elas = RVAL( *Vm_Find( bounceTarget, "elas" ), 0 );
}


EVENT_HANDLER bounce_box (value, count)
 
	TAEINT	value[];	/* integer vector	*/
	FUNINT	count;		/* number of integers	*/
{
/*              Process events from the workspace "box".	      */

    plotball();
}


EVENT_HANDLER bounce_run (value, count)
 
	TAEINT	value[];	/* integer vector	*/
	FUNINT	count;		/* number of integers	*/
{
    run = TRUE;
}


EVENT_HANDLER bounce_stop (value, count)
 
	TAEINT	value[];	/* integer vector	*/
	FUNINT	count;		/* number of integers	*/
{
    Vm_SetReal( bounceTarget, "uball", 1, &uball, P_UPDATE );
    Vm_SetReal( bounceTarget, "vball", 1, &vball, P_UPDATE );
    Wpt_ParmUpdate( bounceId, "uball" );
    Wpt_ParmUpdate( bounceId, "vball" );
    run = FALSE;
}


EVENT_HANDLER bounce_reset (value, count)
 
	TAEINT	value[];	/* integer vector	*/
	FUNINT	count;		/* number of integers	*/
{
    Wpt_PanelReset( bounceId );
    unplotball();
    initball();
    plotball();
    run = FALSE;
}


EVENT_HANDLER bounce_quit (value, count)
 
	TAEINT	value[];	/* integer vector	*/
	FUNINT	count;		/* number of integers	*/
{
    exit( 0 );
}



/**********************************************************************/
/*  Function: initball						      */
/*  Description: This function computes the initial position and      */
/*  and velocity of the ball.					      */
/**********************************************************************/

FUNCTION initball( )
    {
    xball = 0;
    yball = ysize/2;
    uball = RVAL( *Vm_Find( bounceTarget, "uball" ), 0 );
    vball = RVAL( *Vm_Find( bounceTarget, "vball" ), 0 );
    grav  = RVAL( *Vm_Find( bounceTarget, "grav"  ), 0 );
    elas  = RVAL( *Vm_Find( bounceTarget, "elas"  ), 0 );
    return;
    }

/************************************************************************/
/*   Function: initGC.  Initialize the requited graphic contexts 	*/
/*									*/
/************************************************************************/

    FUNCTION  initGC()
    {
    XGCValues   values;
    unsigned  long  valueMask;

    valueMask = GCForeground | GCLineWidth | GCJoinStyle;
    values.line_width = 2;
    values.join_style = JoinRound;

    values.foreground = BlackPixel(display, screen);
    gcContext1 = XCreateGC(display, RootWindow(display, screen),
		 valueMask, &values);

    values.foreground = WhitePixel(display, screen);
    gcContext2 = XCreateGC(display, RootWindow(display, screen),
		 valueMask, &values);

    return;
    }    
/**********************************************************************/
/*  Function: moveball						      */
/*  Description: This function computes a new position and velocity   */
/*  for the ball.						      */
/*  Note: In the bounce2 version this function must account for       */
/*  bounces off the paddle as well as off the walls.		      */
/**********************************************************************/

FUNCTION moveball( )
    {
    float	xball0 = xball, yball0 = yball, yc;


/*  Increment ball position by velocity*deltatime.		      */

    xball += uball;
    yball += vball;

/*  Compute new horizontal velocity.  Velocity is reversed if ball    */
/*  bounces off vertical wall (left, right sides) or off paddle.      */
/*  If ball bounces off paddle, it must be moved back to the side of  */
/*  the paddle that it was coming from.				      */

    yc = line( xpaddle, xball, yball, xball0, yball0 );
    if( xball < 0 || xball > xsize )
        uball = -uball;
    else if( (xpaddle - xball0) * (xball - xpaddle) >= 0
    && (yc - (ypaddle - PSIZE/2)) * ((ypaddle + PSIZE/2) - yc) >= 0 )
        {
        uball = -uball;
        xball = xball0;
        }

/*  Compute new vertical velocity.  Velocity is reversed if ball      */
/*  bounces off horizontal wall (top or bottom).  Otherwise, velocity */
/*  is incremented by gravity.					      */

    if( yball < 0 || yball > ysize ) 
        vball = -vball*elas;
    else
        vball += grav*PERIOD/1000000;
    return;
    }


/**********************************************************************/
/*  Function: plotball						      */
/*  Description: This function plots the ball.			      */
/**********************************************************************/

FUNCTION plotball( )
    {
    ball[0].x = (short)xball;
    ball[0].y = (short)yball;
    XDrawLines(display, box_W, gcContext1, ball, 6, CoordModePrevious); 
    return;
    }

/**********************************************************************/
/*  Function: unplotball					      */
/*  Description:  This function erases the ball.		      */
/**********************************************************************/

FUNCTION unplotball( )
    {
    XDrawLines(display, box_W, gcContext2, ball, 6, CoordModePrevious); 
    return;
    }

/**********************************************************************/
/*  Function: initpaddle					      */
/*  Description: This function computes the initial position of the   */
/*  paddle.							      */
/**********************************************************************/

FUNCTION initpaddle( )
    {
    xpaddle = xsize/2;
    ypaddle = ysize/2;
    return;
    }

/**********************************************************************/
/*  Function: movepaddle					      */
/*  Description: This function computes a new position for the paddle.*/
/**********************************************************************/

FUNCTION movepaddle( )
    {
    xpaddle += 0;
    ypaddle += 0;
    return;
    }

/**********************************************************************/
/*  Function: plotpaddle					      */
/*  Description: This function plots the paddle.		      */
/**********************************************************************/

FUNCTION plotpaddle( )
    {
    paddle[0].x = (short)xpaddle;
    paddle[0].y = (short)ypaddle;
    XDrawLines(display, box_W, gcContext1, paddle, 3, CoordModePrevious);
    return;
    }

/**********************************************************************/
/*  Function: unplotpaddle					      */
/*  Description:  This function erases the paddle.		      */
/**********************************************************************/

FUNCTION unplotpaddle( )
    {
    XDrawLines(display, box_W, gcContext2, paddle, 3, CoordModePrevious);
    return;
    }

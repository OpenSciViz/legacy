/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/******************************************************************************
 * Change log:
 *
 * 02-Nov-89 initial...???
 * 11-Jun-91 PR883	add t_pinit...nci
 * 13-jul-93    Remove all function declarations for Vm_, Wpt_, Co_,
 *              and t_ routines since these declarations are now
 *              available in TAE header files.  Some declarations
 *              in this demo (i.e. t_pinit) didn't match those in the 
 *              header file(s) thus causing compilation problems...ws
 *****************************************************************************/

/******************************************************************************
This is a demo program that demonstrates how to get and set view
attributes, such a foreground color, background color, size, and
title, of TAE Plus items.  The item used in this demo is a radio
button item, although the logic applies to any item.  For details,
see the event handlers "only_set" and "only_get" in this file.
*****************************************************************************/

/*** WorkBench version Wed Oct 25 13:55:53 EDT 1989 ***/

/***************************************** 
 
A Program generated by the TAE Plus WorkBench:  
 
To turn this into a real application, do the following: 
 
1.	For each parameter that you have defined to be 
"event-generating", there is an event handler function below. 
Each handler has a name that is the concatenation of 
the panel name and the parameter name.   Add 
application-dependent logic to each event handler. 
(As generated by the WorkBench, each event handler 
simply logs the occurrence of the event.) 
 
If you don't see any functions below flagged with 
'EVENT_HANDLER', then you did not have any 
parameters defined as event generating. 
 
 
Additional notes: 
 
1.	Each event handler has two arguments: (a) the  
value vector associated with the parameter and (b) the number  
of components.   Note that for scalar values, we  
pass the value as if it were a vector with count 1.   
    
Though it's unlikely that you are interested in  
the value of a button event parameter, the values are  
always passed to the event handler for consistency. 
 
2.	You gain access to non-event parameters by calling 
the Vm package using the targetId Vm objects 
that are created in initializePanels.   
There are macros defined below to assist in accessing 
values in Vm objects. 
 
*********************************************/ 
  
#include 	"taeconf.inp"  
#include 	"wptinc.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 
  
#ifdef VMS
#define SETGET_RES "$TAEDEMORES:setget.res"
#else
#define SETGET_RES "$TAEDEMORES/setget.res"
#endif

#define EVENT_HANDLER 		/* a flag for documentation */  
 
/*	macros for access to parameter values 
 
These macros obtain parameter values given the name of 
a Vm object and the name string of the parameter. 
The Vm objects are created by the initializePanels function 
below. 
 
*/ 
  
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  
  
/* 
 
Reference scalar parameters as follows: 
   
 	StringParm(myPanelTarget, "s")    -- string pointer   
 	IntParm(myPanelTarget, "i")	  -- integer value   
 	RealParm(myPanelTarget, "r")	  -- real value   
   
 For vector parameters, do the following: 
   
 	TAEINT	*ival; 
 	ival = &IntParm(myPanelTarget, "i"); 
 	printf ("%d %d %d", ival[0], ival[1], ival[2]); 
 
*/  
 
/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
static Id onlyTarget, onlyView, onlyId; 
EVENT_HANDLER only_set (); 
EVENT_HANDLER only_get (); 
EVENT_HANDLER only_quit (); 

/*	Display Id for use by direct Xlib calls: */
 
Display   *theDisplay;
 
 
/*	Dispatch Table: one per panel 	*/ 
 
typedef VOID (*FUNCTION_PTR) (); 
static struct DISPATCH  
    { 
    TEXT		*parmName; 
    FUNCTION_PTR 	eventFunction; 
    } ;
 
struct DISPATCH onlyDispatch[] = {
    {"set", only_set}, 
    {"get", only_get}, 
    {"quit", only_quit}, 
    {NULL, NULL}	/* terminator entry */
    };
 
 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
struct DISPATCH         *dp;           	/* working dispatch pointer  */ 
struct VARIABLE         *parmv;		/* pointer to event VARIABLE */ 
WptEvent		wptEvent;     	/* event data		     */
CODE                    code; 
int			tlines,tcols,ttype;

    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower (FALSE);		/* permit upper/lowercase file names */
    theDisplay  = Wpt_Init (NULL); 
    initializePanels (); 
 
    /*	main event loop */ 
 
    while (FOREVER) 
	{ 
    	code = Wpt_NextEvent (&wptEvent);	  /* get next WPT event  */ 
    	if (code < 0)  
    	    continue; 
	else if (code != WPT_PARM_EVENT) 
           { 
           if (code == WPT_FILE_EVENT)           /* from external source */ 
               printf ("No EVENT_HANDLER for event from external source.\n"); 
           continue; 
           } 
 
	/*	Event has occurred from a Panel Parm.  Lookup the event 
	 *	in the dispatch table and call the associated 
 	 *	event handler function. 
 	 */ 
 
       dp = (struct DISPATCH *) wptEvent.p_userContext;  
       for (;  (*dp).parmName != NULL;   dp++)
	    if (strcmp ((*dp).parmName, wptEvent.parmName) == 0)  
		{ 
		parmv = Vm_Find (wptEvent.p_dataVm, wptEvent.parmName); 
		(*(*dp).eventFunction)  
			((*parmv).v_cvp, (*parmv).v_count); 
		break; 
		} 
        } 
} 

FUNCTION VOID initializePanels ()
      
    {
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, SETGET_RES, P_ABORT);
    onlyView = Co_Find (vmCollection, "only_v");
    onlyTarget = Co_Find (vmCollection, "only_t");
    onlyId = Wpt_NewPanel ("", onlyTarget, 
    		onlyView, NULL, onlyDispatch, 0);
    }
 
/*	event handlers */ 
 
EVENT_HANDLER only_set (value, count)

/*  Depending on the title of a button called "set", this routine
    sets one of a number of view attributes of a radio button item
    called "myradio".  The attributes that are set are:  background
    color, foreground color, size, and title.  After this is done,
    the title of the button "set" is changed to reflect the next
    attribute to be changed.  Running this program gives a clear
    idea of what's going on. */

    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    TEXT        *str_vector[2];
    TAEINT      int_vector[2];

    printf ("Panel only, parm set: value = %s\n",
    		count>0 ? value[0] : "none");

    /* set view attributes */
    /* NOTE:  The following attributes cannot be set:
              font, presentation type, origin, columns */
    if ( strcmp(StringParm(onlyView, "set"),"Set bg") == 0 )
       {
       str_vector[0] = "blue";
       Vm_SetString (onlyView, "myradio.bg", 1, str_vector, P_UPDATE);
       Wpt_ViewUpdate (onlyId, "myradio", onlyView, "myradio");
       set_button ("Set fg");
       }
    else if ( strcmp(StringParm(onlyView, "set"),"Set fg") == 0 )
       {
       str_vector[0] = "yellow";
       Vm_SetString (onlyView, "myradio.fg", 1, str_vector, P_UPDATE);
       Wpt_ViewUpdate (onlyId, "myradio", onlyView, "myradio");
       set_button("Set size");
       }
    else if ( strcmp(StringParm(onlyView, "set"),"Set size") == 0 )
       {
       int_vector[0] = 170;
       int_vector[1] = 170;
       Vm_SetIntg (onlyView, "myradio.size", 2, int_vector, P_UPDATE);
       Wpt_ViewUpdate (onlyId, "myradio", onlyView, "myradio");
       set_button("Set title");
       }
    else if ( strcmp(StringParm(onlyView, "set"),"Set title") == 0 )
       {
       str_vector[0] = "Your Radio";
       Vm_SetString (onlyView, "myradio", 1, str_vector, P_UPDATE);
       Wpt_ViewUpdate (onlyId, "myradio", onlyView, "myradio");
       set_button("Set bg");
       }
    }
    
EVENT_HANDLER only_get (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel only, parm get: value = %s\n",
    		count>0 ? value[0] : "none");

    printf ("item type is        -> %s\n",
            StringParm(onlyView, "myradio._TYPE") );
    printf ("foreground color is -> %s\n",
            StringParm(onlyView, "myradio.fg") );
    printf ("background color is -> %s\n",
            StringParm(onlyView, "myradio.bg") );
    printf ("origin (x value) is -> %d\n",
            IVAL(*Vm_Find(onlyView,"myradio.origin"),0) );
    printf ("origin (y value) is -> %d\n",
            IVAL(*Vm_Find(onlyView,"myradio.origin"),1) );
    printf ("font is             -> %s\n",
            StringParm(onlyView, "myradio.font") );
    printf ("size (x value) is   -> %d\n",
            IVAL(*Vm_Find(onlyView,"myradio.size"),0) );
    printf ("size (y value) is   -> %d\n",
            IVAL(*Vm_Find(onlyView,"myradio.size"),1) );
    printf ("# of columns is     -> %d\n",
            IntParm(onlyView, "myradio.columns") );
    printf ("title is            -> %s\n",
            StringParm(onlyView, "myradio") );
    }
    
EVENT_HANDLER only_quit (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel only, parm quit: value = %s\n",
    		count>0 ? value[0] : "none");
    Wpt_PanelErase (onlyId);
    exit(0);
    }

FUNCTION VOID set_button (title)
    TEXT        *title;
    {
    TEXT        *str_vector[1];
    str_vector[0] = title;
    Vm_SetString (onlyView, "set", 1, str_vector, P_UPDATE);
    Wpt_ViewUpdate (onlyId, "set", onlyView, "set");
    }

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*
 * This program is a simple example of Wpt use.
 *
 * Change log:
 *
 * 5-20-88 	Assume the interface is a resource file...nhe
 * 12-15-88 	Read resource file from $TUTIL/minwpt.res...palm
 * 17-jul-89	Convert to new (Xt based) WPT architecture...dm
 * 04-oct-89    VMS port...ljn 
 *  8-Dec-90	changed TDEMO to TAEDEMORES for res file...baw
 * 17-apr-91	Logical MINWPT_RES...ljn
 * 11-Jun-91 PR883 add t_pinit...nci
 * 13-jul-93    Remove all function declarations for Vm_, Wpt_, Co_,
 *              and t_ routines since these declarations are now
 *              available in TAE header files.  Some declarations
 *              in this demo (i.e. t_pinit) didn't match those in the 
 *              header file(s) thus causing compilation problems...ws
 */

#include	"taeconf.inp"
#include 	"wptinc.inp"

#ifdef VMS
#define MINWPT_RES "$TAEDEMORES:minwpt.res"
#else
#define MINWPT_RES "$TAEDEMORES/minwpt.res"
#endif


    main()
    {
/*  DECLARATIONS */

    
    TEXT        *parmName;      /* parameter name               */
    Id          view;           /* Vm object for view data      */
    Id          target;         /* Vm object for target data    */
    Id          panelId;        /* current panel                */
    struct VARIABLE *echoval;   /* TAE variable for echoparm value */
    Id          vmCollection;
    CODE        code;
    WptEvent    wptEvent;       /* Event returned by Wpt        */
    int                 tlines,tcols,ttype;


/* INITIALIZE */
    t_pinit (&tlines, &tcols, &ttype);
    Wpt_Init (NULL);		/* if err, msg to stderr and pgm exits */

/* Create Panel using WPT */

    vmCollection = Co_New(0);	/* Read the resource file and crack it	      */
    code = Co_ReadFile (vmCollection, MINWPT_RES, P_ABORT);
    view = Co_Find (vmCollection, "minwpt_v");    	/* get view data      */
    target = Co_Find (vmCollection, "minwpt_t");  	/* target data        */
    panelId = Wpt_NewPanel ("", target, view, NULL, target, 0);


/* Loop, waiting on events, screening out events of no interest to us */
    while (FOREVER) 
        {
    	Wpt_NextEvent (&wptEvent);	        /* get next Wpt event  */
    	if (wptEvent.p_panelId != panelId) 	/* should not happen   */
    	    continue;
	parmName = wptEvent.parmName;
	/*	dispatch on parm name...	*/

	if (s_equal (parmName, "echoparm"))
	    {
    	    echoval = Vm_Find (target, "echoparm");	/* pt to parm	 */
    	    printf ("Parameter value is %s\n", SVAL(*echoval,0));
	    }
        else if (s_equal (parmName, "quit"))
    	    {
    	    printf ("Quitting\n");
    	    exit (0);
    	    }
        else 
            {
    	    printf ("Unexpected parameter name: %s\n ", parmName);
            }
	}    
    }

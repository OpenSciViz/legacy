/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_pselect.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  pselect
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_pselect.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pselect
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   checkbox,        icon,            menubar,         option, 
 *   pulldown,        pushbutton,      radio,           scale, 
 *   selectionlist
 *   
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * 24-Aug-92    Update the dynamic text with scale's current value...dgf
 * 21-Sep-92    Print the count and value(s) of each item...dgf
 * 26-oct-92	PR1705: some printf statements indicate the wrong itemname...krw
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 *
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_pselect.h"

/* (+) ADDED dgf */
#include        "pan_ptext.h"
/* (-) ADDED */

/* One "include" for each connected panel */

Id pselectTarget, pselectView, pselectId;
/* pselectDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID pselect_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    pselectView = Co_Find (vmCollection, "pselect_v");
    pselectTarget = Co_Find (vmCollection, "pselect_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID pselect_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pselectId)
        printf ("Panel (pselect) is already displayed.\n");
    else
        pselectId = Wpt_NewPanel (Default_Display, pselectTarget, pselectView,
                                  relativeWindow, pselectDispatch, flags);
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID pselect_Destroy_Panel ()
    {
    Wpt_PanelErase(pselectId);
    pselectId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID pselect_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pselectId)
        Wpt_SetPanelState (pselectId, flags);
    else
        pselect_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  checkbox
 */
EVENT_HANDLER checkbox_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm checkbox: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  icon
 */
EVENT_HANDLER icon_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm icon: "); 
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  menubar
 */
EVENT_HANDLER menubar_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
	/* (+) ADDED dgf */
    printf ("Panel pselect, parm menubar: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  option
 */
EVENT_HANDLER option_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm option: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  pulldown
 */
EVENT_HANDLER pulldown_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm pulldown: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  pushbutton
 */
EVENT_HANDLER pushbutton_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm pushbutton: ");
    Print_IntegerValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  radio
 */
EVENT_HANDLER radio_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm radio: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


/* ************************************************************************
 * Handle event from parameter:  scale
 */
EVENT_HANDLER scale_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm scale: ");
    Print_IntegerValues (value, count);
    /* (-) ADDED */

    /* (+) ADDED dgf */
    /* Update the dynamic text with scale's current value */
    Wpt_SetIntg ( ptextId, "dynlabel", value[0] );
    /* (-) ADDED */

    }


/* ************************************************************************
 * Handle event from parameter:  selectionlist
 */
EVENT_HANDLER selectionlist_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm selectionlist: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }



struct DISPATCH pselectDispatch[] = {
    {"checkbox", checkbox_Event},
    {"icon", icon_Event},
    {"menubar", menubar_Event},
    {"option", option_Event},
    {"pulldown", pulldown_Event},
    {"pushbutton", pushbutton_Event},
    {"radio", radio_Event},
    {"scale", scale_Event},
    {"selectionlist", selectionlist_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

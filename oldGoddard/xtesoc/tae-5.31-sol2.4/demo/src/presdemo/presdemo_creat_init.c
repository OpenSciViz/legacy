/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        presdemo_creat_init.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Displays all panels in the initial panel set of this resource file
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     A panel is added to the initial panel set
 *     A panel is deleted from the initial panel set
 * For the set of initial panels:
 *   pcontrol
 * 
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * ************************************************************************
 */
#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */

/* One include for each panel in initial panel set */
#include        "pan_pcontrol.h"


FUNCTION VOID presdemo_Create_Initial_Panels ()
    {
    /* Show panels */

    pcontrol_Create_Panel (NULL, WPT_PREFERRED);
    }

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

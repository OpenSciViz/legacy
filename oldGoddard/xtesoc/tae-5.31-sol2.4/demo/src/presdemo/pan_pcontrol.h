/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_pcontrol.h *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Header file for panel:  pcontrol
 *
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pcontrol
 * 
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 *  14-Aug-92   Added prototypes for Update_Color, Is_ManPage, Display_ManPage,
 *	        	ConnectAll functions...dgf
 *  21-Sep-92	Added prototype for Get_EntrySensitivity, 						 *				Toggle_EntrySensitivity and ConnectAll functions...dgf
 * **************************************************************************
 */

#ifndef I_PAN_pcontrol                      /* prevent double include */
#define I_PAN_pcontrol      0

/* Vm objects and panel Id.   */
extern Id pcontrolTarget, pcontrolView, pcontrolId;

/* Dispatch table (global for calls to Wpt_NewPanel) */
extern struct DISPATCH pcontrolDispatch[];

/* Initialize pcontrolTarget and pcontrolView */
extern VOID pcontrol_Initialize_Panel ();

/* Create this panel and display it on the screen */
extern VOID pcontrol_Create_Panel ();

/* Destroy this panel and erase it from the screen */
extern VOID pcontrol_Destroy_Panel ();

/* Connect to this panel.  Create it or change it's state */
extern VOID pcontrol_Connect_Panel ();
/* (+) ADDED...dgf */

/* Change a TAE presentation item to a given color. */
FUNCTION void Update_Color ();

/* Retrieves all subcascadEs of manpage entry and determines if value is
 * an entry of these subcascades */
FUNCTION CODE Is_ManPage ();

/* Display a given man page in an xterm. */
FUNCTION void Display_ManPage ();

/* Connect all presentation type panels.  Toggle the displayall and 
 * hideall entry */ 
FUNCTION void ConnectAll ();

/* Hide all presentation type panels.  Bring up ddodemo.  When ddodemo
 * is terminated, return all presentation type panels to original state. */
FUNCTION void Do_DdoDemo ();

/* Toggle the sensitivity of a menubar entry */
FUNCTION int Toggle_EntrySensitivity ();

/* Return the sensitivity of a non-cascade menu entry. */
FUNCTION BOOL Get_EntrySensitivity ();

/* (-) ADDED...dgf */
#endif


/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

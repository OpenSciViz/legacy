/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        global.h *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This global header file is automatically "#include"d in each panel
 * file.  You can insert references to global variables here.
 *
 * REGENERATED:
 * This file is generated only once.
 * 
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * 21-Sep-92    Added global functions  Print_StringValues,
                                        Print_RealValues, and
                                        Print_IntegerValues...dgf
 * 15-Jul-93    Remove unnecessary declaration of Vm_Find; the declaration
 *              is now provided in a TAE header file...ws
 * 14-dec-93	Fix compiler warning (typedef declares no type name)...dag
 * 
 * ************************************************************************
 */

#ifndef I_GLOBAL                            /* prevent double include */
#define I_GLOBAL        0

/*      macros for access to parameter values 
 *
 * These macros obtain parameter values given the name of 
 * a Vm object and the name string of the parameter. 
 * The Vm objects are created by the Initialize_All_Panels
 * function for a resource file.
 *  
 * Reference scalar parameters as follows: 
 *    
 *      StringParm(myPanelTarget, "s")    -- string pointer   
 *      IntParm(myPanelTarget, "i")       -- integer value   
 *      RealParm(myPanelTarget, "r")      -- real value   
 *    
 *  For vector parameters, do the following: 
 *    
 *      TAEINT  *ival; 
 *      ival = &IntParm(myPanelTarget, "i"); 
 *      printf ("%d %d %d", ival[0], ival[1], ival[2]); 
 *  
 */ 
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  

/*      Dispatch Table typedef          */ 
 
typedef VOID (*FUNCTION_PTR) (); 
typedef struct DISPATCH
    { 
    TEXT                *parmName; 
    FUNCTION_PTR        eventFunction; 
    } Dispatch;
    
#define EVENT_HANDLER static VOID           /* a flag for documentation */

/*      Display Id for use by direct Xlib calls: */
 
extern Display   *Default_Display;

#define SET_APPLICATION_DONE \
    { \
    extern BOOL Application_Done; \
    Application_Done = TRUE; \
    }
/* ADDED (+) dgf */
/* global functions */

/* Print the count and string value(s) returned to an Event Handler. */
extern FUNCTION void Print_StringValues ();

/* Print the count and real value(s) returned to an Event Handler. */
extern FUNCTION void Print_RealValues ();

/* Print the count and  integer value(s) returned to an Event Handler. */
extern FUNCTION void Print_IntegerValues ();

/* (-) ADDED */

#endif

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

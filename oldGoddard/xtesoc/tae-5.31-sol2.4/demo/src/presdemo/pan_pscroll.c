/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_pscroll.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  pscroll
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_pscroll.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pscroll
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   scroller
 *   
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 *  3-Aug-92	Added code to display data in scroller item...dgf
 * 21-Sep-92    Print the count and value(s) of each item...dgf
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 *
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_pscroll.h"

/* One "include" for each connected panel */

Id pscrollTarget, pscrollView, pscrollId;
/* pscrollDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID pscroll_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    pscrollView = Co_Find (vmCollection, "pscroll_v");
    pscrollTarget = Co_Find (vmCollection, "pscroll_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID pscroll_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    /* (+) ADDED dgf */
#define MAX_LOGGER_MSGS 8
    TEXT * svec[MAX_LOGGER_MSGS];

    /* Color Logger presentation type in User Contributed category */

    /* embedded escape sequences denote color index set in WorkBench */
    svec[0] = "\033[32m This is message zero.\033[0m";
    svec[1] = "\033[46m Connection established.\033[0m";
    svec[2] = " Data transmitted. \033[0m"; 
    svec[3] = " Data received.\033[0m";
    svec[4] = "\033[47m Error: Connection lost!\033[0m";
    svec[5] = "\033[37m Warning: Fuel low.\033[0m";
    svec[6] = " Logging out.\033[0m";
    svec[7] = " Many different \033[30mc\033[0m\033[31mo\033[0m\033[32ml\033[0m\033[34mo\033[0m\033[35mr\033[0m\033[36ms\033[0m\033[37m.\033[0m";

    /* panel must be created before values can be placed in scroller item */ 
    if (pscrollId)
    	printf ("Panel (pscroll) is already displayed.\n");
    else
    	pscrollId = Wpt_NewPanel (Default_Display, pscrollTarget, pscrollView,
    	    	relativeWindow, pscrollDispatch, flags);

    /* set the item's target to the strings with escape sequences */
    Vm_SetString (pscrollTarget, "scroller", MAX_LOGGER_MSGS, svec, P_UPDATE);
    Wpt_ParmUpdate (pscrollId, "scroller");
    /* (-) ADDED dgf */
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID pscroll_Destroy_Panel ()
    {
    Wpt_PanelErase (pscrollId);
    pscrollId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID pscroll_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pscrollId)
        Wpt_SetPanelState (pscrollId, flags);
    else
        pscroll_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  scroller
 */
EVENT_HANDLER scroller_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm scroller: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }



struct DISPATCH pscrollDispatch[] = {
    {"scroller", scroller_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

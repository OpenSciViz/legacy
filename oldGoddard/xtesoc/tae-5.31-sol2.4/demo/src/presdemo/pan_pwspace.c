/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_pwspace.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  pwspace
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_pwspace.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pwspace
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   workspace
 *   
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * 21-Sep-92    Print the count and value(s) of each item...dgf
 * 27-Sep-92    Added Set_Window_Background function for workspace...kbs,krw
 * 28-Sep-92    Added automatic resizing of Work Region to fit bitmap...kbs,cew
 * 07-Oct-92    Wrong panel name in printf of event handler...kbs
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 *
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_pwspace.h"

/* One "include" for each connected panel */

/* (+) ADDED kbs */
#define BITMAPFILE	"/einstein.xbm"

void Set_Window_Background();
/* (-) ADDED kbs */

/* (+) ADDED dgf */
/* To be used to store the window Id of a TAE item */
Window picwin;
/* (-) ADDED dgf */

Id pwspaceTarget, pwspaceView, pwspaceId;
/* pwspaceDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID pwspace_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    pwspaceView = Co_Find (vmCollection, "pwspace_v");
    pwspaceTarget = Co_Find (vmCollection, "pwspace_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID pwspace_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    /* (+) ADDED kbs */
    TEXT 	*getenv();
    TEXT 	*dirname;
    TEXT 	filename[STRINGSIZ];
    TAEINT	size[2];		/* Work Region size */
    int  	width, height;		/* returned size of bitmap */
    /* (-) ADDED kbs */

    if (pwspaceId)
        printf ("Panel (pwspace) is already displayed.\n");
    else
        pwspaceId = Wpt_NewPanel (Default_Display, pwspaceTarget, pwspaceView,
                                  relativeWindow, pwspaceDispatch, flags);
    /* (+) ADDED dgf */
    /* Retrieve the window Id of the workspace item */
    picwin = Wpt_ItemWindow (pwspaceId, "workspace");

    /* If your application needs the widget id of the workspace, 
     * use XtWindowToWidget(). For example:
     *        Widget workspaceWidget;
     *        workspaceWidget = XtWindowToWidget (Default_Display, picwin);
     */
    /* (-) ADDED dgf */

    /* (+) ADDED kbs */
	dirname = getenv ( "TAEBITMAPS" );
	strcpy ( filename, dirname );
	strcat ( filename, BITMAPFILE );

	/* read bitmapfile into workspace window background */
	/* (Note that bitmap width and height are returned.) */
	Set_Window_Background ( picwin, filename, "SaddleBrown", "OldLace",
	                        &width, &height );

	/* Resize X workspace Work Region ("workwinSize") to match bitmap size.
	 * This allows the same workspace to be used for any bitmap whose
	 * size is greater than or equal to that defined in the WorkBench.
	 * This does not alter the ITEM size of the scrolled workspace.
	 *
	 * NOTE: If you would rather TILE a small bitmap in a larger workspace
	 *       window, simply block the Vm_SetIntg/Wpt_ViewUpdate code.
	 */
	size[0] = width;
	size[1] = height;
	Vm_SetIntg ( pwspaceView, "workspace.workwinSize", 2, size, P_UPDATE);
	Wpt_ViewUpdate ( pwspaceId, "workspace", pwspaceView, "workspace" );

    /* (-) ADDED kbs */
    }

/* ************************************************************************
 * Function to read bitmap file into a Pixmap structure.
 * Accepts colornames to change the fg and bg color of the pixmap.
 * (If you want the bitmap's normal black/white, pass "black", "white".)
 * This function should only be called once (e.g., not from event handler).
 * Since it needs the window id of the workspace, call it after Wpt_NewPanel
 * to guarantee the workspace item has been created (valid window id).
 * Actually, this function will work on ANY window id passed in, including
 * the panel's window id (obtained by using Wpt_PanelWindow).
 * 
 */
 /* (+) ADDED kbs,krw */
FUNCTION void Set_Window_Background ( win, bitmapfile, fgname, bgname,
                                      wreturn, hreturn )
    Window win ;	    /* X window id of workspace item */
    char *bitmapfile ;	    /* name of file containing bitmap */
    char *fgname, *bgname;  /* foreground and background colors to use */
    int  *wreturn,*hreturn; /* return bitmap width & height to caller */
{

   Pixmap bitmap, pixmap;	/* 1 plane and N-plane */
   XColor fg_color, bg_color;
   GC gc;
   XGCValues values;
   unsigned  long  valueMask;
   Display *dpy;
   Drawable drawable ;
   Colormap cmap;
   int screen;
   unsigned long fg, bg ;
   unsigned int width, height; 	/* width and height of bitmap */
   int status ;
   int depth ;			/* 1 = monochrome, >1 = grayscale or color */
   int x_hot, y_hot ;

	drawable = win ;
	dpy      = Default_Display;
	screen   = DefaultScreen (dpy);
	depth    = DisplayPlanes (dpy, screen);
	cmap     = DefaultColormap (dpy, screen) ;

        /* allocate foreground and background color pixels */
        bg = ( XParseColor ( dpy, cmap, bgname, &bg_color)
                &&  XAllocColor (dpy, cmap, &bg_color) )
             ? bg_color.pixel : WhitePixel (dpy, screen) ;

        fg = ( XParseColor ( dpy, cmap, fgname, &fg_color)
                &&  XAllocColor (dpy, cmap, &fg_color) )
             ? fg_color.pixel : BlackPixel (dpy, screen) ;

	/* setup graphic context with requested colors */
    	valueMask = GCForeground | GCBackground ;
    	values.foreground = fg ;
    	values.background = bg ;
	gc = XCreateGC (dpy, drawable, valueMask, &values);

	/* create single plane pixmap (called a bitmap) */
	/* fill it with data from interpreted bitmap file */
        status = XReadBitmapFile (dpy, drawable, bitmapfile, 
				&width, &height, &bitmap, &x_hot, &y_hot);

        if (status != BitmapSuccess)
	    {
            fprintf (stderr, "ERROR: Couldn't read bitmap: %s\n", bitmapfile);
	    return;
	    }

	printf ("\tRead bitmapfile = %s, \n\twidth = %d, height = %d\n", 
			bitmapfile, width, height);

	/* return width and height to caller, who may choose to ignore this */
	*wreturn = width ;
	*hreturn = height ;

	/* create another pixmap (of the same width and height as read bitmap) 
         * of the correct depth for the current screen 
         */
	pixmap = XCreatePixmap (dpy, drawable, width, height, depth);

	/* copy bitmap to all planes of pixmap using requested colors in gc */
	XCopyPlane (dpy, bitmap, pixmap, gc, 
			/* copy all of source... */ 0, 0, width, height,
			/* to destination's upper left corner... */ 0, 0,
			/* using plane mask */ 1L);

        /* change the window's background to a tiling of N-plane pixmap */
        XSetWindowBackgroundPixmap (dpy, win, pixmap) ;

	/* force exposure (needed for some X displays) */
	XClearWindow ( dpy, win );

} /* Set_Window_Background */
 /* (-) ADDED kbs,krw */

/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID pwspace_Destroy_Panel ()
    {
    Wpt_PanelErase(pwspaceId);
    pwspaceId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID pwspace_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pwspaceId)
        Wpt_SetPanelState (pwspaceId, flags);
    else
        pwspace_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  workspace
 */
EVENT_HANDLER workspace_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pwspace, parm workspace: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }


struct DISPATCH pwspaceDispatch[] = {
    {"workspace", workspace_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */


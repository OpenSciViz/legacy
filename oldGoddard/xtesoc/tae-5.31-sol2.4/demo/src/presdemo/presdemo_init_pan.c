/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        presdemo_init_pan.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Initialize all panels in the resource file.
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     A panel is deleted
 *     A new panel is added
 *     A panel's name is changed (not title)
 * For the panels:
 *   pcontrol, pdialog1, pdialog2, pscroll,  pselect,  ptext,    pwspace, 
 * 
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * 15-Jul-93    Remove unnecessary declaration of Co functions; the 
 *              declarations are now provided in a TAE header file...ws
 *
 * ************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "symtab.inc"
#include        "global.h"                  /* Application globals */

/* One "include" for each panel in resource file */
#include        "pan_pcontrol.h"
#include        "pan_pdialog1.h"
#include        "pan_pdialog2.h"
#include        "pan_pscroll.h"
#include        "pan_pselect.h"
#include        "pan_ptext.h"
#include        "pan_pwspace.h"


FUNCTION VOID presdemo_Initialize_All_Panels (resfileSpec)
    TEXT        *resfileSpec;
    {
    Id vmCollection ;
    
    /* read resource file */
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, resfileSpec, P_ABORT);
    
    /* initialize view and target Vm objects for each panel */
    pcontrol_Initialize_Panel (vmCollection);
    pdialog1_Initialize_Panel (vmCollection);
    pdialog2_Initialize_Panel (vmCollection);
    pscroll_Initialize_Panel (vmCollection);
    pselect_Initialize_Panel (vmCollection);
    ptext_Initialize_Panel (vmCollection);
    pwspace_Initialize_Panel (vmCollection);
    }
    

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

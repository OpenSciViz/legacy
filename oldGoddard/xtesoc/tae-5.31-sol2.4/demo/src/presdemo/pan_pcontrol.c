/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE PLUS CODE GENERATOR VERSION v5.2 BETA *** */
/* *** File:        pan_pcontrol.c *** */
/* *** Generated:   Aug  3 08:23:20 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  pcontrol
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_pcontrol.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pcontrol
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   controlMenubar,  demo_item
 *   
 */
/*
 * **************************************************************************
 * CHANGE LOG:
 *  03-Aug-92   Initially generated...TAE
 *  03-Aug-92   Display bitmap in workspace, change demo_item fg and bg color,
 *         	include presentation panel header files...dgf
 *  10-Aug-92 	Update title, update value, dim, hide demo_item...dgf 
 *  14-Aug-92   Display man pages...dgf
 *  16-Sep-92	Added Do_DDO_Demo to allow access ddodemo from presdemo...dgf
 *  18-Sep-92   Added Get_EntrySensitivity and Toggle_EntrySensisitivity...dgf 
 *  18-Sep-92   Added ConnectAll for all presentation panels...dgf 
 *  30-Sep-92   Background the xterm for man pages, use man page title for 
 *              titlebar and icon name of man page window, shorten sh script 
 *              name for old SysV, declare Do_DDO_Demo, cosmetic changes...kbs
 *  02-Oct-92	Added event handler for demo_item...kbs
 *  06-Oct-92	Added Get_Values function here and in Edit menu; added 
 *          	constraints and errormsg to demo_item...kbs
 *  07-Oct-92	Execute ddodemo in bg; VMS ddodemo conditional; Get_Values 
 *          	after Wpt_SetNoValue could dump core on some platforms...kbs
 *  09-Oct-92	Get target count, data type and constraints in Get_Values...kbs
 *  09-Nov-92	SGI port: remove panel state code from Do_DDO_Demo...kbs
 *  10-Nov-92	Longer cmd line for xterm due to extra options...kbs
 *  16-Nov-92	Incorrect use of tertiary operator..kbs
 *  24-jun-93	Replace exit(0) w/ SET_APPLICATION_DONE in quit evt hndlr...swd
 *  15-Jul-93   Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 *  28-nov-94	PR2919: Ultrix dxterm has different options...kbs
 * 
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_pcontrol.h"

/* One "include" for each connected panel */
#include        "pan_pselect.h"

/* (-) ADDED dgf */
/* include headers for all panels */
#include        "pan_pdialog2.h"
#include        "pan_pwspace.h"
#include        "pan_pdialog1.h"
#include        "pan_pscroll.h"
#include        "pan_ptext.h"
/* (-) ADDED */

/* (+) ADDED dgf */
#define FLUNK -999  /* flag for returning failure of a function */ 
/* (-) ADDED */

/* (+) ADDED kbs */
FUNCTION void Do_DDO_Demo ();
FUNCTION void Get_Values ();
/* (-) ADDED kbs */

Id pcontrolTarget, pcontrolView, pcontrolId;
/* pcontrolDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID pcontrol_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    pcontrolView = Co_Find (vmCollection, "pcontrol_v");
    pcontrolTarget = Co_Find (vmCollection, "pcontrol_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID pcontrol_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {

    if (pcontrolId)
    	printf ("Panel (pcontrol) is already displayed.\n");
    else 
    	pcontrolId = Wpt_NewPanel (Default_Display, pcontrolTarget,
    	    pcontrolView, relativeWindow, pcontrolDispatch, flags);
    /* (+) ADDED dgf */
    /* initially make hideall entry insensitive */
    Toggle_EntrySensitivity ("hideall"); 
    /* (-) ADDED */
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID pcontrol_Destroy_Panel ()
    {

    Wpt_PanelErase(pcontrolId);
    pcontrolId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID pcontrol_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {

    if (pcontrolId)
        Wpt_SetPanelState (pcontrolId, flags);
    else
    	pcontrol_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  controlMenubar
 */
EVENT_HANDLER controlMenubar_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
   {

    /* (+) ADDED dgf */
    TEXT *svec[1];
    static TAEINT stitle,svalue;

    /* (-) ADDED */
    /* Begin generated code for Connection */
    if (count <= 0)
    	;                                   /* null value or no value */
    else if ( s_equal (value[0], "quit"))
    	    { 
    	    /* exit application */
    	    /* (+) ADDED dgf */
	    SET_APPLICATION_DONE
    	    /* (-) ADDED */
    	    }
    else if ( (s_equal (value[0], "blackfg")) ||
    	      (s_equal (value[0], "bluefg"))  ||
    	      (s_equal (value[0], "greenfg")) ||  
    	      (s_equal (value[0], "redfg"))   ||
    	      (s_equal (value[0], "whitefg")) ||
    	      (s_equal (value[0], "yellowfg")) )
    	    {
    	    /* update the foreground color of the demo item */
    	    Update_Color ("fg", value[0]);
    	    }
    else if ( (s_equal (value[0], "blackbg")) ||
    	      (s_equal (value[0], "bluebg"))  ||
    	      (s_equal (value[0], "greenbg")) ||
    	      (s_equal (value[0], "redbg"))   ||
    	      (s_equal (value[0], "whitebg")) ||
    	      (s_equal (value[0], "yellowbg")) )
    	    {
    	    /* update the background color of the demo item */
    	    Update_Color ("bg", value[0]);
    	    }
    else if (s_equal (value[0], "updatevalue"))
    	    { 
    	    /* toggle the value of the demo item */
    	    /* (+) ADDED dgf */
    	    if (svalue == 0)
    	    	{
    	    	Wpt_SetString (pcontrolId, "demo_item", "one new value");
    	    	svalue++;
    	    	}
    	    else
    	    	{
    	    	Wpt_SetString (pcontrolId, "demo_item", "another new value");
    	    	svalue--; 
    	    	}
    	    /* (-) ADDED */
    	    }
    else if (s_equal (value[0], "setnovalue"))
    	    {
    	    /* set the value of the demo item to NULL */
    	    /* (+) ADDED dgf*/
    	    Wpt_SetNoValue (pcontrolId, "demo_item");
    	    /* (-) ADDED */
    	    }
    else if (s_equal (value[0], "updatetitle"))
    	    {
    	    /* toggle title of the demo item */
    	    /* (+) ADDED dgf */
    	    if (stitle == 0)
    	    	{
    	    	svec[0] = "New title";
    	    	Vm_SetString (pcontrolView, "demo_item", 1, svec, P_UPDATE);
    	    	Wpt_ViewUpdate (pcontrolId, "demo_item", pcontrolView,
    	    	    "demo_item"); 
    	    	stitle++;
    	    }
    	    else
    	    {
    	    svec[0] = "Name";
    	    Vm_SetString (pcontrolView, "demo_item", 1, svec, P_UPDATE);
    	    Wpt_ViewUpdate (pcontrolId, "demo_item", pcontrolView, "demo_item");
    	    stitle--;
    	    }
    	    /* (-) ADDED */
    	    }
    else if (s_equal (value[0], "dimitem"))
    	    {
    	    /* enable/disable the demo item */
    	    if (s_equal (value[1], "yes")) 
    	    	{
    	    	/* (+) ADDED dgf */
    	    	/* turn sensitivity off */
    	    	Wpt_SetItemSensitivity (pcontrolId, "demo_item", 0); 
    	    	/* (-) ADDED */
    	    	}
    	    else
    	    	{
    	    	/* (+) ADDED dgf */
    	    	/* turn sensitivity on */
    	    	Wpt_SetItemSensitivity (pcontrolId, "demo_item", 1);
    	    	/* (-) ADDED */
    	    	}
    	    }
    else if (s_equal (value[0], "hideitems"))
    	    {
    	    /* hide/show the demo item */
    	    if (s_equal (value[1], "yes"))
        	{
    	    	/* (+) ADDED dgf */ 
    	    	Wpt_HideItem (pcontrolId, "demo_item");
    	    	/* (-) ADDED */
    	    	}
    	    else
    	    	{
    	    	/* (+) ADDED dgf */
    	    	Wpt_ShowItem (pcontrolId, "demo_item");
    	    	/* (-) ADDED */
    	    	}
    	    }
    else if (s_equal (value[0], "reset"))
            {
    	    /* reset the value of demo_item to it's original value 
    	     * read in from the resource file */ 
    	    /* (+) ADDED dgf */
    	    Wpt_PanelReset (pcontrolId);
    	    /* (-) ADDED */
    	    }
    else if (s_equal (value[0], "get_values"))
            {
    	    /* retrieve view and target values of the demo_item */
    	    /* (+) ADDED kbs */
    	    Get_Values ( );
    	    /* (-) ADDED */
	    }
    else if (s_equal (value[0], "displayall"))
            {
    	    /* display all of the presentation panels */
    	    /* (+) ADDED dgf */
    	    ConnectAll (WPT_PREFERRED);
    	    /* (-) ADDED */
            }
    else if (s_equal (value[0], "hideall"))
            {
    	    /* hide all of the presentation panels */
    	    /* (+) ADDED dgf */
    	    ConnectAll (WPT_INVISIBLE);
    	    /* (-) ADDED */
            }
    else if ( Is_ManPage (value[0]) )
            {
    	    /* display man page */
    	    /* (+) ADDED dgf */
#ifndef VMS
    	    Display_ManPage (value[0]);
#else 
    	    Wpt_PanelMessage (pcontrolId, 
    	    	"Man Page option not implemented in VMS.");
#endif
    	    /* (-) ADDED */
            }
    else if (s_equal (value[0], "ddodemo")) 
            {
    	    /* execute ddodemo demonstration application */
    	    Do_DDO_Demo ();
    	    /* (-) ADDED */
            } 
    /* End generated code for Connection */
    }

/* ************************************************************************
 * Handle event from parameter:  demo_item
 */
EVENT_HANDLER demo_item_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED kbs */
    printf ("Panel pcontrol, parm demo_item: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }

/* Retrieve target and view info from the demo_item.
 *
 * Except for the "presentation specific view resources" portion,
 * this function could be generalized by passing in panelId, targetId, and
 * viewId, and TEXT *itemname, and using strcat functions to build the 
 * qualifier names.
 *
 */

FUNCTION void Get_Values ( )
    {
    TEXT *targetValue;
    TEXT **constraints;
    TAEINT numConstraints;
    COUNT targetCount, i;
    CODE data_type;
    BOOL dummy_default;            /* irrelevant return values */
    CODE dummy_access;  

    /* Target value: */

    /* demo_item is string (TEXT *) data type and returns a count of 1,
     * so use StringParm. If count were greater than one, we'd use SVAL. 
     * Note that an item's current value is the value of the TARGET 
     * Vm object without any qualifiers. Testing the return from
     * StringParm is necessary because if Wpt_SetNoValue was called for
     * the item, StringParm would return NULL.
     */
    targetValue = StringParm (pcontrolTarget, "demo_item");
    printf ("\n-------- Target Information --------\n");
    printf ("current value     = %s\n",
            (targetValue) ? targetValue : "(NO VALUE)" );

    /* Get count of target Vm object.
     * For a keyin, this is always 1, except if Wpt_SetNoValue was called,
     * then the count should be 0.
     * Note that we are passing the target and the item name.
     */
    Vm_GetAttribute (pcontrolTarget, "demo_item",  &data_type,
                    &targetCount, &dummy_default, &dummy_access);
    printf ("target count      = %d\n", targetCount);
    printf ("target data type  = %s\n", 
		((data_type == V_STRING)  ? "String" :
		 (data_type == V_INTEGER) ? "Integer" :
		 (data_type == V_REAL)    ? "Real" :
		 (data_type == V_NAME)    ? "Name" : "Unknown data type"));

    /* Get valid values for the item */
    Wpt_GetStringConstraints (pcontrolId, "demo_item",
                              &numConstraints, &constraints);
    printf ("Item has %d constraints (valids) defined:\n", numConstraints);
    for (i = 0; i < numConstraints; i++)
	{
	printf ("                    %s\n", constraints[i]);
	}

    /* View values: */

    /* Common view resources for all presentation types */
    /* Note that title is value of the view without qualifiers. */
    printf ("\n-------- Common View Resources --------\n");
    printf ("title             = %s\n",
            StringParm (pcontrolView, "demo_item") );
    printf ("presentation type = %s\n",
            StringParm (pcontrolView, "demo_item._TYPE") );
    printf ("foreground color  = %s\n",
            StringParm (pcontrolView, "demo_item.fg") );
    printf ("background color  = %s\n",
            StringParm (pcontrolView, "demo_item.bg") );
    printf ("font              = %s\n",
            StringParm (pcontrolView, "demo_item.font") );
    printf ("origin (x value)  = %d\n",
            IVAL (*Vm_Find (pcontrolView, "demo_item.origin"), 0) );
    printf ("origin (y value)  = %d\n",
            IVAL (*Vm_Find (pcontrolView, "demo_item.origin"), 1) );
    printf ("size   (width)    = %d\n",
            IVAL (*Vm_Find (pcontrolView, "demo_item.size"), 0) );
    printf ("size   (height)   = %d\n",
            IVAL (*Vm_Find (pcontrolView, "demo_item.size"), 1) );
    printf ("border            = %d\n",
            IntParm (pcontrolView, "demo_item.border") );
    printf ("shadow            = %d\n",
            IntParm (pcontrolView, "demo_item.shadow") );
    printf ("traversal         = %d\n",
            IntParm (pcontrolView, "demo_item.traversal") );

    /* Presentation specific view resources */
    printf ("\n-------- Presentation Specific View Resources --------\n");
    printf ("justify           = %s\n",
            StringParm (pcontrolView, "demo_item.justify") );

    /* Since "errormsg" is multi-dimensional, need to get count first. */
        {
        struct VARIABLE *v;            /* returned by Vm_Find */
        TAEINT i;                      /* loop control */
        COUNT newcount;                /* # of strings currently in item */
        CODE dummy_type, dummy_access; /* irrelevant return values */
        BOOL dummy_default;            /* irrelevant return values */

        /* Note that we are passing the view and the "errormsg" qualifier
	 * of the item.
	 */
        Vm_GetAttribute (pcontrolView, "demo_item.errormsg", 
	     		 &dummy_type, &newcount, 
			 &dummy_default, &dummy_access);
        /* access the view TAE Variable */
        v = Vm_Find (pcontrolView, "demo_item.errormsg");
        /* retrieve the string vector */
        for ( i = 0; i < newcount; i++ )
            printf ("errormsg[%d]       = %s\n", i, SVAL (*v, i) );

        }
    }

/* Change a TAE presentation item to a given color using it's entry title. */
FUNCTION void Update_Color (fg_or_bg, color_entry_name)
    TEXT *fg_or_bg;
    TEXT *color_entry_name;
    {
    TEXT path[STRINGSIZ+1];
    TEXT qual_to_update[80];	/* qualifier to be updated */
    TEXT *color[80];		/* actual color name, e.g., "blue" */

    /* "demo_item" is name of item to be updated */
    /* Note the '.' separator needed to build the qualifier */
    strcpy (qual_to_update, "demo_item.");

    /* append "fg" or "bg" to complete the qualifier */
    strcat (qual_to_update, fg_or_bg); 

    /* retrieve fully qualified path of named entry to be used to directly
     * access the entry view */
    Wpt_GetMenuEntryPath (pcontrolView, "controlMenubar", 
    color_entry_name, path);

    /* retrieve title of entry for actual color name because entry names
     * must be unique */
    color[0] = StringParm (pcontrolView, path);

    /* set the color in the view object */
    Vm_SetString (pcontrolView, qual_to_update, 1, color, P_UPDATE);
    Wpt_ViewUpdate (pcontrolId, "demo_item", pcontrolView, "demo_item");
    }

/* Retrieves all subcascades of manpage entry and determines if value is
 * an entry of these subcascades */
/* Note: Code changes will not be required if additonal presentation
 * categories and/or presentation types are added to the man page entries. */
FUNCTION CODE Is_ManPage (entry_name)
    TEXT *entry_name; 
    {
    WPT_MENUENTRY *entries, *subentries;
    COUNT menucount, submenucount;
    TAEINT i,j;

    /* retrieve all entries for manpage cascade */
    Wpt_GetMenu (pcontrolTarget, pcontrolView, "controlMenubar", "manpage",
    	&menucount, &entries);
    for (i=0; i<menucount; i++)
    	{
    	/* retrieve all entries for each subcascade */
    	Wpt_GetMenu (pcontrolTarget, pcontrolView, "controlMenubar", 
    	    entries[i].name, &submenucount, &subentries);

    	/* test each entry */
    	for (j=0; j<submenucount; j++)
    	    {
    	    if (s_equal(entry_name,subentries[j].name))
    	    	/* value is an entry in a subcascade of "manpage" */ 
    	    	return SUCCESS; 
    	    }
    	}
    /* value is not an entry in a subcascade of "manpage" */
    return FAIL;
    }

/* Display a given man page in an xterm. */
FUNCTION void Display_ManPage (entry_name)
    TEXT *entry_name;
    {
    TEXT cmd[160];     /* command string for xterm command */ 
    TEXT manpage[19];  /* entry name max plus TAE_ */ 

    strcpy (manpage, "TAE_");
    /* concatenate "TAE_" with entry name to from man page title */
    strcat (manpage, entry_name);

    /* Invoke xterm in the background to execute shell script
     * which displays one man page. Option -title controls the
     * xterm titlebar and -n (NOT -name) controls the icon name
     * without changing the application class name of this instance
     * of xterm.
     */
#if defined(vax) || defined(MIPSEL)
    /* PR2919: Ultrix-specific binary and options */
    sprintf (cmd, "dxterm -xrm 'DXterm.title: %s' -xrm 'DXterm.iconName: %s' -e presdemoman %s &", 
	     manpage, manpage, manpage);
#else
    sprintf (cmd, "xterm -sb -title %s -n %s -e presdemoman %s &", 
	     manpage, manpage, manpage);
#endif
    /* execute man page script */
    system (cmd);
    }

/* Connect all presentation type panels with a given state.  
 * Toggle the displayall and hideall entry */
FUNCTION void ConnectAll (state)
    BOOL state;
    {

    /* (+) ADDED dgf */
    pselect_Connect_Panel (NULL, state);
    pdialog1_Connect_Panel (NULL, state);
    ptext_Connect_Panel (NULL, state);
    pscroll_Connect_Panel (NULL, state);
    pdialog2_Connect_Panel (NULL, state);
    pwspace_Connect_Panel (NULL, state);
    Toggle_EntrySensitivity ("displayall");	
    Toggle_EntrySensitivity ("hideall");
    /* (-) added */
    }


/* Hide all presentation type panels.  Bring up ddodemo.  When ddodemo
 * is terminated, return all presentation type panels to original state. */
FUNCTION void Do_DDO_Demo ()
{
    /* execute the ddodemo demo application */
#ifdef VMS
    /* spawn/nowait doesn't work- why? */
    system ("ddodemo");
#else
    system ("ddodemo &");
#endif
}

/* Return the sensitivity of a non-cascade menu entry. */
FUNCTION BOOL Get_EntrySensitivity (entry)
    TEXT *entry;
{
    WPT_MENUENTRY *entries;
    COUNT entryCnt;
    CODE code;
    int i;
    BOOL state;

    code = Wpt_GetMenu (pcontrolTarget, pcontrolView , "controlMenubar", 
                        entry, &entryCnt, &entries);
	if (code == SUCCESS)
    	{
    	for (i=0; i<entryCnt; i++)
    	    {
    	    if (s_equal(entries[i].name,entry))
    	    	{
    	    	state = entries[i].state;
    	    	tae_free (entries);
    	    	return state;
    	    	}
    	   	}
    	tae_free (entries);    	/* remember to free the menu info */
    	return FLUNK;    	    /* not in menu,  must be a cascade */
    	/* use FLUNK because FAIL will imply insensitive */
    	}
    else 
    	return FLUNK;	/* menu level or entry not returned */
    	/* use FLUNK because FAIL will imply insensitive */
}

/* Toggle the sensitivity of a menubar entry */ 
FUNCTION int Toggle_EntrySensitivity (entry)
    TEXT *entry;
{
	BOOL newstate[1];

    newstate[0] = (Get_EntrySensitivity (entry));
    if (!(newstate[0] == FLUNK))
    	{
    	newstate[0] = (!newstate[0]);
    	Wpt_SetMenuSensitivity (pcontrolView, "controlMenubar", 1, &entry, 
                                newstate);
    	Wpt_MenuUpdate (pcontrolId, pcontrolTarget, pcontrolView,
			"controlMenubar"); 		
    	return newstate[0];
    	}
    else
    	return FAIL;
}

struct DISPATCH pcontrolDispatch[] = {
    {"controlMenubar", controlMenubar_Event},
    {"demo_item", demo_item_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_pdialog1.c *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  pdialog1
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_pdialog1.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   pdialog1
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   filesel
 *   
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * 21-Sep-92    Print the count and value(s) of each item...dgf
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 *
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_pdialog1.h"

/* One "include" for each connected panel */

Id pdialog1Target, pdialog1View, pdialog1Id;
/* pdialog1Dispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID pdialog1_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    pdialog1View = Co_Find (vmCollection, "pdialog1_v");
    pdialog1Target = Co_Find (vmCollection, "pdialog1_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID pdialog1_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pdialog1Id)
        printf ("Panel (pdialog1) is already displayed.\n");
    else
        pdialog1Id = Wpt_NewPanel (Default_Display, pdialog1Target, pdialog1View,
                                   relativeWindow, pdialog1Dispatch, flags);
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID pdialog1_Destroy_Panel ()
    {
    Wpt_PanelErase(pdialog1Id);
    pdialog1Id=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID pdialog1_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (pdialog1Id)
        Wpt_SetPanelState (pdialog1Id, flags);
    else
        pdialog1_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  filesel
 */
EVENT_HANDLER filesel_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* (+) ADDED dgf */
    printf ("Panel pselect, parm filesel: ");
    Print_StringValues (value, count);
    /* (-) ADDED */
    }

struct DISPATCH pdialog1Dispatch[] = {
    {"filesel", filesel_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

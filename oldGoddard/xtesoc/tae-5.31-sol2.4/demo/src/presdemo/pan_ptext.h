/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.2 Beta *** */
/* *** File:        pan_ptext.h *** */
/* *** Generated:   Aug  3 08:16:51 1992 *** */
/* ************************************************************************
 * PURPOSE:
 * Header file for panel:  ptext
 *
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   ptext
 * 
 * CHANGE LOG:
 *  3-Aug-92    Initially generated...TAE
 * **************************************************************************
 */

#ifndef I_PAN_ptext                         /* prevent double include */
#define I_PAN_ptext      0

/* Vm objects and panel Id.   */
extern Id ptextTarget, ptextView, ptextId;

/* Dispatch table (global for calls to Wpt_NewPanel) */
extern struct DISPATCH ptextDispatch[];

/* Initialize ptextTarget and ptextView */
extern VOID ptext_Initialize_Panel ();

/* Create this panel and display it on the screen */
extern VOID ptext_Create_Panel ();

/* Destroy this panel and erase it from the screen */
extern VOID ptext_Destroy_Panel ();

/* Connect to this panel.  Create it or change it's state */
extern VOID ptext_Connect_Panel ();
#endif


/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/******************************************************************************
 * Change log:
 *
 * 11-Jun-91 PR883	add t_pinit...nci
 * 13-jul-93    Remove all function declarations for Vm_, Wpt_, Co_,
 *              and t_ routines since these declarations are now
 *              available in TAE header files.  Some declarations
 *              in this demo (i.e. t_pinit) didn't match those in the 
 *              header file(s) thus causing compilation problems...ws
 ******************************************************************************/

/*** WorkBench version V4.0 ***/
/***************************************** 
 
A Program generated by the TAE Plus WorkBench:  
 
To turn this into a real application, do the following: 
 
1.	For each parameter that you have defined to be 
"event-generating", there is an event handler function below. 
Each handler has a name that is the concatenation of 
the panel name and the parameter name.   Add 
application-dependent logic to each event handler. 
(As generated by the WorkBench, each event handler 
simply logs the occurrence of the event.) 
 
If you don't see any functions below flagged with 
'EVENT_HANDLER', then you did not have any 
parameters defined as event generating. 
 
 
Additional notes: 
 
1.	Each event handler has two arguments: (a) the  
value vector associated with the parameter and (b) the number  
of components.   Note that for scalar values, we  
pass the value as if it were a vector with count 1.   
    
Though it's unlikely that you are interested in  
the value of a button event parameter, the values are  
always passed to the event handler for consistency. 
 
2.	You gain access to non-event parameters by calling 
the Vm package using the targetId Vm objects 
that are created in initializePanels.   
There are macros defined below to assist in accessing 
values in Vm objects. 
 
*********************************************/ 
  
#include 	"taeconf.inp"  
#include 	"wptinc.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 
  
#ifdef VMS
#define STATES_RES "$TAEDEMORES:states.res"
#else
#define STATES_RES "$TAEDEMORES/states.res"
#endif

#define EVENT_HANDLER 		/* a flag for documentation */  
 
/*	macros for access to parameter values 
 
These macros obtain parameter values given the name of 
a Vm object and the name string of the parameter. 
The Vm objects are created by the initializePanels function 
below. 
 
*/ 
  
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  
  
/* 
 
Reference scalar parameters as follows: 
   
 	StringParm(myPanelTarget, "s")    -- string pointer   
 	IntParm(myPanelTarget, "i")	  -- integer value   
 	RealParm(myPanelTarget, "r")	  -- real value   
   
 For vector parameters, do the following: 
   
 	TAEINT	*ival; 
 	ival = &IntParm(myPanelTarget, "i"); 
 	printf ("%d %d %d", ival[0], ival[1], ival[2]); 
 
*/  
 
/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
static Id StatesTarget, StatesView, StatesId; 
static Id astateTarget, astateView, astateId; 
EVENT_HANDLER States_squit (); 
EVENT_HANDLER States_slist (); 
EVENT_HANDLER astate_sok (); 
EVENT_HANDLER astate_name (); 
EVENT_HANDLER astate_pop (); 

/*	Display Id for use by direct Xlib calls: */
 
Display   *theDisplay;
 
 
/*	Dispatch Table: one per panel 	*/ 
 
typedef VOID (*FUNCTION_PTR) (); 
static struct DISPATCH  
    { 
    TEXT		*parmName; 
    FUNCTION_PTR 	eventFunction; 
    } ;
 
struct DISPATCH StatesDispatch[] = {
    {"squit", States_squit}, 
    {"slist", States_slist}, 
    {NULL, NULL}	/* terminator entry */
    };
 
struct DISPATCH astateDispatch[] = {
    {"sok", astate_sok}, 
    {"name", astate_name}, 
    {"pop", astate_pop}, 
    {NULL, NULL}	/* terminator entry */
    };
 
 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
struct DISPATCH         *dp;           	/* working dispatch pointer  */ 
struct VARIABLE         *parmv;		/* pointer to event VARIABLE */ 
WptEvent		wptEvent;     	/* event data		     */
CODE                    code; 
int			tlines,tcols,ttype;

    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower (FALSE);	/* permit upper/lowercase file names */
    theDisplay  = Wpt_Init (NULL); 
    initializePanels (); 
 
    /*	main event loop */ 
 
    while (FOREVER) 
	{ 
    	code = Wpt_NextEvent (&wptEvent);	  /* get next WPT event  */ 
    	if (code < 0)  
    	    continue; 
	else if (code != WPT_PARM_EVENT) 
           { 
           if (code == WPT_FILE_EVENT)           /* from external source */ 
               printf ("No EVENT_HANDLER for event from external source.\n"); 
           continue; 
           } 
 
	/*	Event has occurred from a Panel Parm.  Lookup the event 
	 *	in the dispatch table and call the associated 
 	 *	event handler function. 
 	 */ 
 
       dp = (struct DISPATCH *) wptEvent.p_userContext;  
       for (;  (*dp).parmName != NULL;   dp++)
	    if (strcmp ((*dp).parmName, wptEvent.parmName) == 0)  
		{ 
		parmv = Vm_Find (wptEvent.p_dataVm, wptEvent.parmName); 
		(*(*dp).eventFunction)  
			((*parmv).v_cvp, (*parmv).v_count); 
		break; 
		} 
        } 
} 

FUNCTION VOID initializePanels ()
      
    {
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, STATES_RES, P_ABORT);
    StatesView = Co_Find (vmCollection, "States_v");
    StatesTarget = Co_Find (vmCollection, "States_t");
    astateView = Co_Find (vmCollection, "astate_v");
    astateTarget = Co_Find (vmCollection, "astate_t");
    StatesId = Wpt_NewPanel ("", StatesTarget, 
    		StatesView, NULL, StatesDispatch, 0);
    }
 
/*	event handlers */ 
 
EVENT_HANDLER States_squit (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel States, parm squit: value = %s\n",
    		count>0 ? value[0] : "none");
    Wpt_PanelErase(StatesId);
    StatesId=0;
    exit(0);
    /*  TCL: quit  */
    }
    
EVENT_HANDLER States_slist (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel States, parm slist: value = %s\n",
    		count>0 ? value[0] : "none");
    if (count <= 0)
        ;		/* null value or no value */
    else if (strcmp (value[0], "Maryland") == 0)
        {
        astateId = Wpt_NewPanel ("", astateTarget, 
        		astateView, NULL, astateDispatch, 0);
        Wpt_PanelErase(StatesId);
        StatesId=0;
        }
    else if (strcmp (value[0], "Virginia") == 0)
        {
        astateId = Wpt_NewPanel ("", astateTarget, 
        		astateView, NULL, astateDispatch, 0);
        Wpt_PanelErase(StatesId);
        StatesId=0;
        }
    else if (strcmp (value[0], "California") == 0)
        {
        astateId = Wpt_NewPanel ("", astateTarget, 
        		astateView, NULL, astateDispatch, 0);
        Wpt_PanelErase(StatesId);
        StatesId=0;
        }
    }
    
EVENT_HANDLER astate_sok (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel astate, parm sok: value = %s\n",
    		count>0 ? value[0] : "none");
    StatesId = Wpt_NewPanel ("", StatesTarget, 
    		StatesView, NULL, StatesDispatch, 0);
    Wpt_PanelErase(astateId);
    astateId=0;
    }
    
EVENT_HANDLER astate_name (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel astate, parm name: value = %s\n",
    		count>0 ? value[0] : "none");
    }
    
EVENT_HANDLER astate_pop (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel astate, parm pop: value = %s\n",
    		count>0 ? value[0] : "none");
    }
    

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/******************************************************************************
 * Change log:
 *
 *	06-Dec-89 initial...???
 *	11-Jun-91 PR883	add t_pinit...nci
 * 13-jul-93    Remove all function declarations for Vm_, Wpt_, Co_,
 *              and t_ routines since these declarations are now
 *              available in TAE header files.  Some declarations
 *              in this demo (i.e. t_pinit) didn't match those in the 
 *              header file(s) thus causing compilation problems...ws
 ******************************************************************************/
  
#include 	"taeconf.inp"  
#include 	"wptinc.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 

#ifdef VMS
#define MODALTEST_RES "$TAEDEMORES:modaltest.res"
#else
#define MODALTEST_RES "$TAEDEMORES/modaltest.res"
#endif
  
#define EVENT_HANDLER 		/* a flag for documentation */  
 
/*	macros for access to parameter values 
 
These macros obtain parameter values given the name of 
a Vm object and the name string of the parameter. 
The Vm objects are created by the initializePanels function 
below. 
 
*/ 
  
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  
  
/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
static Id modal1Target, modal1View, modal1Id; 
static Id panel2Target, panel2View, panel2Id; 
EVENT_HANDLER modal1_item1 (); 
EVENT_HANDLER modal1_NoName01 (); 
EVENT_HANDLER panel2_item2 (); 
EVENT_HANDLER panel2_outobj (); 
EVENT_HANDLER panel2_qqqqq (); 

/*	Display Id for use by direct Xlib calls: */
 
Display   *theDisplay;
 
 
/*	Dispatch Table: one per panel 	*/ 
 
typedef VOID (*FUNCTION_PTR) (); 
static struct DISPATCH  
    { 
    TEXT		*parmName; 
    FUNCTION_PTR 	eventFunction; 
    } ;
 
struct DISPATCH modal1Dispatch[] = {
    {"item1", modal1_item1}, 
    {"NoName01", modal1_NoName01}, 
    {NULL, NULL}	/* terminator entry */
    };
 
struct DISPATCH panel2Dispatch[] = {
    {"item2", panel2_item2}, 
    {"outobj", panel2_outobj}, 
    {"qqqqq", panel2_qqqqq}, 
    {NULL, NULL}	/* terminator entry */
    };
 
 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
struct DISPATCH          *dp;           /* working dispatch pointer  */ 
struct VARIABLE          *parmv;	/* pointer to event VARIABLE */ 
WptEvent		  wptEvent;     /* event data		     */
CODE                     code; 
int			tlines,tcols,ttype;

    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower (FALSE);	/* permit upper/lowercase file names */
    theDisplay  = Wpt_Init (NULL); 
    initializePanels (); 
 
    /*	main event loop */ 
 
    while (FOREVER) 
	{ 
    	code = Wpt_NextEvent (&wptEvent);	  /* get next WPT event  */ 
    	if (code < 0)  
    	    continue; 
	else if (code != WPT_PARM_EVENT) 
           { 
           if (code == WPT_FILE_EVENT)           /* from external source */ 
               printf ("No EVENT_HANDLER for event from external source.\n"); 
           continue; 
           } 
 
	/*	Event has occurred from a Panel Parm.  Lookup the event 
	 *	in the dispatch table and call the associated 
 	 *	event handler function. 
 	 */ 
 
       dp = (struct DISPATCH *) wptEvent.p_userContext;  
       for (;  (*dp).parmName != NULL;   dp++)
	    if (strcmp ((*dp).parmName, wptEvent.parmName) == 0)  
		{ 
		parmv = Vm_Find (wptEvent.p_dataVm, wptEvent.parmName); 
		(*(*dp).eventFunction)  
			((*parmv).v_cvp, (*parmv).v_count); 
		break; 
		} 
        } 
} 

FUNCTION VOID initializePanels ()
      
    {
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, MODALTEST_RES, P_ABORT);
    modal1View = Co_Find (vmCollection, "modal1_v");
    modal1Target = Co_Find (vmCollection, "modal1_t");
    panel2View = Co_Find (vmCollection, "panel2_v");
    panel2Target = Co_Find (vmCollection, "panel2_t");
    modal1Id = Wpt_NewPanel ("", modal1Target, 
    		modal1View, NULL, modal1Dispatch, WPT_MODALPANEL);
    panel2Id = Wpt_NewPanel ("", panel2Target,
                panel2View, NULL, panel2Dispatch, 0);

    }
 
/*	event handlers */ 
 
EVENT_HANDLER modal1_item1 (value, count)
    TAEFLOAT 	value[];     /* real vector     */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel modal1, parm item1: value = %f\n",
    		count>0 ? value[0] : 0.0);
    Wpt_SetReal ( panel2Id,"outobj",value[0] );
    }
    
EVENT_HANDLER modal1_NoName01 (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel modal1, parm NoName01: value = %s\n",
    		count>0 ? value[0] : "none");
    Wpt_PanelErase(modal1Id);
    }
    
EVENT_HANDLER panel2_item2 (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel panel2, parm item2: value = %s\n",
    		count>0 ? value[0] : "none");
    }
    
EVENT_HANDLER panel2_outobj (value, count)
    TAEFLOAT 	value[];     /* real vector     */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel panel2, parm outobj: value = %f\n",
    		count>0 ? value[0] : 0.0);
    }
    
EVENT_HANDLER panel2_qqqqq (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel panel2, parm qqqqq: value = %s\n",
    		count>0 ? value[0] : "none");
    Wpt_PanelErase(panel2Id);
    exit(1);
    }
    

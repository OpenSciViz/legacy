/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.0 (Beta) *** */
/* *** File:        pan_main.c *** */
/* *** Generated:   Jan 23 13:29:03 1991 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  main
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_main.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   main
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   quit,            show_panel2,     show_panel1,     icon, 
 *   intKeyin,        selectionList,   show2_iconify,   radioButtons, 
 *   pulldownMenu
 *   
 * CHANGE LOG:
 * 23-Jan-91    Initially generated...TAE
 * 23-Jan-91	Add SET_APPLICATION_DONE to quit event handler
 * 15-Sep-92	PR1633 Change first arg of Wpt_NewPanel to Default_Display...kbs
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_main.h"

/* One "include" for each connected panel */
#include        "pan_panel2.h"
#include        "pan_panel1.h"

Id mainTarget, mainView, mainId;
/* mainDispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID main_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    mainView = Co_Find (vmCollection, "main_v");
    mainTarget = Co_Find (vmCollection, "main_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID main_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (mainId)
        printf ("Panel (main) is already displayed.\n");
    else
        mainId = Wpt_NewPanel (Default_Display, mainTarget, mainView,
                               relativeWindow, mainDispatch, flags);
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID main_Destroy_Panel ()
    {
    Wpt_PanelErase(mainId);
    mainId=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID main_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (mainId)
        Wpt_SetPanelState (mainId, flags);
    else
        main_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  quit
 */
EVENT_HANDLER quit_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm quit: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    main_Destroy_Panel ();
    /*  TCL: quit  */
    
    /* End generated code for Connection */

    /* ADDED (+) cew */
    SET_APPLICATION_DONE;
    /* (-) */
    }


/* ************************************************************************
 * Handle event from parameter:  show_panel2
 */
EVENT_HANDLER show_panel2_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm show_panel2: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    panel2_Connect_Panel (NULL, WPT_PREFERRED);
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  show_panel1
 */
EVENT_HANDLER show_panel1_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm show_panel1: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    panel1_Connect_Panel (NULL, WPT_PREFERRED);
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  icon
 */
EVENT_HANDLER icon_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm icon: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  intKeyin
 */
EVENT_HANDLER intKeyin_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm intKeyin: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  selectionList
 */
EVENT_HANDLER selectionList_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm selectionList: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  show2_iconify
 */
EVENT_HANDLER show2_iconify_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm show2_iconify: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    main_Connect_Panel (NULL, WPT_ICONIC);
    panel2_Connect_Panel (NULL, WPT_INVISIBLE);
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  radioButtons
 */
EVENT_HANDLER radioButtons_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm radioButtons: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    if (count <= 0)
        ;                                   /* null value or no value */
    else if (s_equal (value[0], "No Change"))
        {
        }
    else if (s_equal (value[0], "Preferred"))
        {
        panel1_Connect_Panel (NULL, WPT_PREFERRED);
        }
    else if (s_equal (value[0], "Deleted"))
        {
        panel1_Destroy_Panel ();
        }
    else if (s_equal (value[0], "Iconic"))
        {
        panel1_Connect_Panel (NULL, WPT_ICONIC);
        }
    else if (s_equal (value[0], "Visible"))
        {
        panel1_Connect_Panel (NULL, WPT_VISIBLE);
        }
    else if (s_equal (value[0], "Invisible"))
        {
        panel1_Connect_Panel (NULL, WPT_INVISIBLE);
        }
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  pulldownMenu
 */
EVENT_HANDLER pulldownMenu_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel main, parm pulldownMenu: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    }



struct DISPATCH mainDispatch[] = {
    {"quit", quit_Event},
    {"show_panel2", show_panel2_Event},
    {"show_panel1", show_panel1_Event},
    {"icon", icon_Event},
    {"intKeyin", intKeyin_Event},
    {"selectionList", selectionList_Event},
    {"show2_iconify", show2_iconify_Event},
    {"radioButtons", radioButtons_Event},
    {"pulldownMenu", pulldownMenu_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

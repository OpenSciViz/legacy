/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.0 (Beta) *** */
/* *** File:        pan_panel2.c *** */
/* *** Generated:   Jan 23 13:29:03 1991 *** */
/* ************************************************************************
 * PURPOSE:
 * This file encapsulates the TAE Plus panel:  panel2
 * These routines enable panel initialization, creation, and destruction.
 * Access to these routines from other files is enabled by inserting
 * '#include "pan_panel2.h"'.  For more advanced manipulation of the panel
 * using the TAE routines, the panel's Id, Target, and View are provided.
 * 
 * NOTES:
 * For each parameter that you have defined to be "event-generating" in
 * this panel, there is an event handler procedure below.  Each handler
 * has a name that is a concatenation of the parameter name and "_Event".
 * Add application-dependent logic to each event handler.  (As generated
 * by the WorkBench, each event handler simply logs the occurrence of the
 * event.)
 * 
 * You may want to flag any changes you make to this file so that if you
 * regenerate this file, you can more easily cut and paste your
 * modifications back in.  For example:
 * 
 *   generated code ...
 *   /* (+) ADDED yourinitials * /
 *   your code
 *   /* (-) ADDED * /
 *   more generated code ...
 * 
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   panel2
 * 
 * The following WorkBench operations will also cause regeneration:
 *     An item is deleted
 *     A new item is added to this panel
 *     An item's name is changed (not title)
 *     An item's data type is changed
 *     An item's generates events flag is changed
 *     An item's valids changed (if item is type string and connected)
 *     An item's connection information is changed
 * For the panel items:
 *   close,           realKeyin,       workspace,       stringKeyin, 
 *   multilineEdit
 *   
 * CHANGE LOG:
 * 23-Jan-91    Initially generated...TAE
 * 15-Sep-92	PR1633 Change first arg of Wpt_NewPanel to Default_Display...kbs
 * 15-Jul-93    Remove unnecessary declaration of Co_Find; the declaration
 *              is now provided in a TAE header file...ws
 * **************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "global.h"                  /* Application globals */
#include        "pan_panel2.h"

/* One "include" for each connected panel */

Id panel2Target, panel2View, panel2Id;
/* panel2Dispatch is defined at the end of this file */

/* ************************************************************************
 * Initialize the view and target of this panel.
 */
FUNCTION VOID panel2_Initialize_Panel (vmCollection)
    Id vmCollection;
    {
    
    panel2View = Co_Find (vmCollection, "panel2_v");
    panel2Target = Co_Find (vmCollection, "panel2_t");
    }


/* ************************************************************************
 * Create the panel object and display it on the screen.
 */
FUNCTION VOID panel2_Create_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (panel2Id)
        printf ("Panel (panel2) is already displayed.\n");
    else
        panel2Id = Wpt_NewPanel (Default_Display, panel2Target, panel2View,
                                 relativeWindow, panel2Dispatch, flags);
    }


/* ************************************************************************
 * Erases a panel from the screen and de-allocate the associated panel
 * object.
 */
FUNCTION VOID panel2_Destroy_Panel ()
    {
    Wpt_PanelErase(panel2Id);
    panel2Id=0;
    }


/* ************************************************************************
 * Connect to this panel.  Create it or change it's state.
 */
FUNCTION VOID panel2_Connect_Panel (relativeWindow, flags)
    Window      relativeWindow;
    COUNT       flags;
    {
    if (panel2Id)
        Wpt_SetPanelState (panel2Id, flags);
    else
        panel2_Create_Panel (relativeWindow, flags);
    }


/* ************************************************************************
 * Handle event from parameter:  close
 */
EVENT_HANDLER close_Event (value, count)
    TAEINT      value[];                    /* integer vector  */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel panel2, parm close: value = %d\n",
            count>0 ? value[0] : 0);
    
    /* End default generated code */
    
    /* Begin generated code for Connection */
    
    panel2_Destroy_Panel ();
    
    /* End generated code for Connection */
    }


/* ************************************************************************
 * Handle event from parameter:  realKeyin
 */
EVENT_HANDLER realKeyin_Event (value, count)
    TAEFLOAT    value[];                    /* real vector     */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel panel2, parm realKeyin: value = %f\n",
            count>0 ? value[0] : 0.0);
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  workspace
 */
EVENT_HANDLER workspace_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel panel2, parm workspace: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  stringKeyin
 */
EVENT_HANDLER stringKeyin_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel panel2, parm stringKeyin: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    }


/* ************************************************************************
 * Handle event from parameter:  multilineEdit
 */
EVENT_HANDLER multilineEdit_Event (value, count)
    TEXT        *value[];                   /* string pointers */
    FUNINT      count;                      /* num of values   */
    {
    /* Begin default generated code */
    
    printf ("Panel panel2, parm multilineEdit: value = %s\n",
            count>0 ? value[0] : "none");
    
    /* End default generated code */
    }



struct DISPATCH panel2Dispatch[] = {
    {"close", close_Event},
    {"realKeyin", realKeyin_Event},
    {"workspace", workspace_Event},
    {"stringKeyin", stringKeyin_Event},
    {"multilineEdit", multilineEdit_Event},
    {NULL, NULL}                            /* terminator entry */
};

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

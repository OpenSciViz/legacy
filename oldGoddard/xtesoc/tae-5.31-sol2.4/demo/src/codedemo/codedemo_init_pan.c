/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.0 (Beta) *** */
/* *** File:        codedemo_init_pan.c *** */
/* *** Generated:   Jan 23 13:29:03 1991 *** */
/* ************************************************************************
 * PURPOSE:
 * Initialize all panels in the resource file.
 * 
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     A panel is deleted
 *     A new panel is added
 *     A panel's name is changed (not title)
 * For the panels:
 *   main,     panel1,   panel2
 * 
 * CHANGE LOG:
 * 23-Jan-91    Initially generated...TAE
 * 15-Jul-93    Remove unnecessary declarations of Co functions; the 
 *              declarations are now provided in a TAE header file...ws
 * ************************************************************************
 */

#include        "taeconf.inp"
#include        "wptinc.inp"
#include        "symtab.inc"
#include        "global.h"                  /* Application globals */

/* One "include" for each panel in resource file */
#include        "pan_main.h"
#include        "pan_panel1.h"
#include        "pan_panel2.h"


FUNCTION VOID codedemo_Initialize_All_Panels (resfileSpec)
    TEXT        *resfileSpec;
    {
    Id vmCollection ;
    
    /* read resource file */
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, resfileSpec, P_ABORT);
    
    /* initialize view and target Vm objects for each panel */
    main_Initialize_Panel (vmCollection);
    panel1_Initialize_Panel (vmCollection);
    panel2_Initialize_Panel (vmCollection);
    }
    

/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

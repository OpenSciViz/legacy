/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/* *** TAE Plus Code Generator version V5.0 (Beta) *** */
/* *** File:        pan_panel2.h *** */
/* *** Generated:   Jan 23 13:29:03 1991 *** */
/* ************************************************************************
 * PURPOSE:
 * Header file for panel:  panel2
 *
 * REGENERATED:
 * The following WorkBench operations will cause regeneration of this file:
 *     The panel's name is changed (not title)
 * For panel:
 *   panel2
 * 
 * CHANGE LOG:
 * 23-Jan-91    Initially generated...TAE
 * **************************************************************************
 */

#ifndef I_PAN_panel2                        /* prevent double include */
#define I_PAN_panel2      0

/* Vm objects and panel Id.   */
extern Id panel2Target, panel2View, panel2Id;

/* Dispatch table (global for calls to Wpt_NewPanel) */
extern struct DISPATCH panel2Dispatch[];

/* Initialize panel2Target and panel2View */
extern VOID panel2_Initialize_Panel ();

/* Create this panel and display it on the screen */
extern VOID panel2_Create_Panel ();

/* Destroy this panel and erase it from the screen */
extern VOID panel2_Destroy_Panel ();

/* Connect to this panel.  Create it or change it's state */
extern VOID panel2_Connect_Panel ();
#endif


/* Automatic TAE-style indenting for Emacs users */
/* *** Local Variables:                         *** */
/* *** mode:                            c       *** */
/* *** c-indent-level:                  0       *** */
/* *** c-continued-statement-offset:    4       *** */
/* *** c-brace-offset:                  4       *** */
/* *** c-brace-imaginary-offset:        4       *** */
/* *** c-argdecl-indent:                4       *** */
/* *** c-label-offset:                  -4      *** */
/* *** c-continued-brace-offset:        -4      *** */
/* *** comment-column:                  45      *** */
/* *** comment-multi-line:              nil     *** */
/* *** End:                                     *** */

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/




/*** WorkBench version V3.11 ***/
/*** MODIFIED for WorkBench version V4.1 ***/
/*** MODIFIED for WorkBench version V4.2 ***/
/*** MODIFIED for WorkBench version V5.0 ***/

/**********************************************************************
 *      CHANGE LOG:
 *      12/20/89 initial (new demo showing use of Workspace item)..kbs
 *      03/12/90 redesigned .res and logic; now tracks ButtonPresses...kbs
 *      03/13/90 added help file...kbs
 *      03/16/90 ifdef's for VMS...kbs
 *      08/14/90 added switch & do_* handlers from xev.c, & dump_wpt_info...kbs
 *      09/04/90 verbose flag for dump_wpt_info...kbs
 *      08-dec-90 changed TDEMO to TAEDEMORES for res file...baw
 *      30-apr-91 VMS port: XWORKSPACE_RES, header file change...ljn
 *      11-jun-91 PR883 add t_pinit...nci
 *      14-feb-92 Different way of checking for xexpose.count...kbs
 *      13-jul-93 Remove all function declarations for Vm_, Wpt_, Co_,
 *                and t_ routines since these declarations are now
 *                available in TAE header files.  Some declarations
 *                in this demo (i.e. t_pinit) didn't match those in the 
 *                header file(s) thus causing compilation problems...ws
 *	08-dec-93 Solaris port (backported dag from aug-93)...cob
 **********************************************************************
 *
 *      This example uses 2 TAE workspace items and illustrates
 *      2 different ways to call Xlib directly.  It also shows
 *	how to use the WptEvent structure to access information
 *	about the events. 
 *
 *	Try moving the mouse in and out of the larger workspace (with-
 *	out clicking on the icon), or press a key or mouse button 
 *	inside the larger workspace.  Notice that the event name is 
 *	displayed in the window from which you invoked this program.
 *	Try iconifying the panel and then de-iconifying it, obscuring
 *	it with another window, and then exposing (raising) it.
 *
 *	The example does not attempt to handle all of the
 *	possible Wpt_NextEvent event types (although it will name 
 *	any event which occurs).  Also, exposure events
 *	(which should generate re-draws) are not handled very
 *	elegantly.  Resize of window is not handled at all.
 *
 *	When working with the TAE workspace, these steps should be followed:
 *		1. Use Wpt_ItemWindow to obtain the X window id.
 *		2. Create a Graphics Context (gc).
 *		3. Decide which event(s) will trigger your drawing handler.
 *		4. Decide what to redraw if Expose event occurs.
 *		5. Note that TAE Plus filters out most of the 33 X events;
 *		   only the following events are currerntly directly available
 *		   to applications without adding input handlers (list subject
 *		   to expansion):
 *		   KeyPress, ButtonPress, ButtonRelease, Expose, 
 *		   EnterNotify, LeaveNotify, and MotionNotify.
 *
 *	==> Search for "XLIB" throughout this file.
 *
 ****************************************************************************/ 
  
#include 	"taeconf.inp"  
#include 	"wptinc.inp"  
#include	"symtab.inc"  
#include	"parblk.inc" 

#ifdef VMS
#define XWORKSPACE_RES "$TAEDEMORES:xworkspace.res"
#else
#define XWORKSPACE_RES "$TAEDEMORES/xworkspace.res"
#endif

/* XLIB - added from xev.c; 
 * Don't need Xproto.h which includes problematic Xmd.h 
 */
#include <X11/Xos.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <ctype.h>

char *Yes = "YES" ;	/* added from xev.c */
char *No  = "NO" ;	/* added from xev.c */
  
#define EVENT_HANDLER VOID	/* a flag for documentation */  

/*	macros for access to parameter values 
 
These macros obtain parameter values given the name of 
a Vm object and the name string of the parameter. 
The Vm objects are created by the initializePanels function 
below. 
 
*/ 
  
 
  
#define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name),0))  
#define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
#define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
  
  
/*	Vm objects and panel Ids.   */ 
 
Id vmCollection ;
static Id pan1Target, pan1View, pan1Id; 
EVENT_HANDLER pan1_iquit (); 
EVENT_HANDLER pan1_update_barchart (); 
EVENT_HANDLER pan1_but_clear (); 
EVENT_HANDLER pan1_ws_barchart (); 
EVENT_HANDLER pan1_ws_buttonpress (); 

/*	Display Id for use by direct Xlib calls: */
 
Display   *theDisplay;
 
 
/*	Dispatch Table: one per panel 	*/ 
 
typedef VOID (*FUNCTION_PTR) (); 
static struct DISPATCH  
    { 
    TEXT		*parmName; 
    FUNCTION_PTR 	eventFunction; 
    } ;
 
struct DISPATCH pan1Dispatch[] = {
    {"iquit", pan1_iquit}, 
    {"update_barchart", pan1_update_barchart}, 
    {"but_clear", pan1_but_clear}, 
    {"ws_barchart", pan1_ws_barchart}, 
    {"ws_buttonpress", pan1_ws_buttonpress}, 
    {NULL, NULL}	/* terminator entry */
    };
 
 

/* XLIB constants and global declarations */
#define POINT_WIDTH	2	/* dimensions of pt around buttonpress */
#define POINT_HEIGHT	2	/* dimensions of pt around buttonpress */
#define BC_WIDTH	1	/* width of barchart lines */
#define XINC		5	/* gap for barchart */
#define YINC		5	/* gap for barchart */
#define NLINES		100	/* number of lines per draw of barchart */
#define BP_WIDTH 	3	/* width of line drawn upon buttonpress */

int screen ;
XGCValues   values;
unsigned  long  valueMask;
int	verbose = 0;

	/* X window id's returned by Wpt_ItemWindow() */
Window win_ws_buttonpress ;
Window win_ws_barchart ;

	/* Graphics Contexts require above win id as argument */
GC gc_ws_buttonpress ;
GC gc_ws_barchart ;

/* TAE specifics */
WptEvent    wptEvent;       /* (made global) Event returned by Wpt */
TEXT	*parmName ;


/* XLIB: Table for mapping XEvent numbers to names; for TAE workspace */
static char * EventTable[] = {	/***** TABLE BASED ON <X11/X.h> *******/
	"not_used",		/*0 */
	"not_used",  		/*1 */
	"KeyPress",		/*2 */
	"KeyRelease",		/*3 */
	"ButtonPress",		/*4 */
	"ButtonRelease",	/*5 */
	"MotionNotify",		/*6 */
	"EnterNotify",		/*7 */
	"LeaveNotify",		/*8 */
	"FocusIn",		/*9 */
	"FocusOut",		/*10 */
	"KeymapNotify",		/*11 */
	"Expose",		/*12 */
	"GraphicsExpose",	/*13 */
	"NoExpose",		/*14 */
	"VisibilityNotify",	/*15 */
	"CreateNotify",		/*16 */
	"DestroyNotify",	/*17 */
	"UnmapNotify",		/*18 */
	"MapNotify",		/*19 */
	"MapRequest",		/*20 */
	"ReparentNotify",	/*21 */
	"ConfigureNotify",	/*22 */
	"ConfigureRequest",	/*23 */
	"GravityNotify",	/*24 */
	"ResizeRequest",	/*25 */
	"CirculateNotify",	/*26 */
	"CirculateRequest",	/*27 */
	"PropertyNotify",	/*28 */
	"SelectionClear",	/*29 */
	"SelectionRequest",	/*30 */
	"SelectionNotify",	/*31 */
	"ColormapNotify",	/*32 */
	"ClientMessage",	/*33 */
	"MappingNotify"		/*34 */
} ;	/* end EventTable */

 
main (argc, argv) 
 
    FUNINT	argc; 
    TEXT	*argv[]; 
 
{ 
struct DISPATCH          *dp;           /* working dispatch pointer  */ 
struct VARIABLE          *parmv;	/* pointer to event VARIABLE */ 
/** Made global: WptEvent		  wptEvent;     /* event data */
CODE                     code; 
int			tlines,tcols,ttype;

    t_pinit (&tlines, &tcols, &ttype);
    f_force_lower (FALSE);	/* permit upper/lowercase file names */
    theDisplay  = Wpt_Init (NULL); 
    initializePanels (); 
 
	if (argc > 1 )
	{
		if ( argv[1][0] == '-' && argv[1][1] == 'v' )
			verbose = 1;
		else
		{
			printf ("\nUnknown option: %s.\n", argv[1] );
			printf ("For verbose WPT info, invoke with -v.\n");
		}
	}
	else
		printf ("\n\tFor verbose WPT info, invoke with -v.\n");

    /*	main event loop */ 
 
    while (FOREVER) 
	{ 
    	code = Wpt_NextEvent (&wptEvent);	  /* get next WPT event  */ 
    	if (code < 0)  
    	    continue; 
	else if (code != WPT_PARM_EVENT) 
           { 
           if (code == WPT_FILE_EVENT)           /* from external source */ 
               printf ("No EVENT_HANDLER for event from external source.\n"); 
           continue; 
           } 
 
	/* XLIB */
	printf ("-----------------------------------------------------------------------\n");

	/* Note that barchart drawing (and re-drawing) is handled here,
	 * rather than in event handler, mainly to illustrate response
	 * to exposure events.  Therefore, the barchart will be re-drawn
	 * but the connection of buttonpresses will NOT be re-drawn.
	 */

        parmName = wptEvent.parmName ;

	/* VERTICAL BAR GRAPH */
	if ( s_equal ( parmName, "ws_barchart" ))
	{
		/* Initialize graphics context for barchart, if necessary. */
	    if ( !( gc_ws_barchart ))
	    {

		/* get X window id associated with workspace */
		win_ws_barchart = Wpt_ItemWindow ( pan1Id, "ws_barchart" );
		printf ( "\n===> X window id for ws_barchart is hex: %x\n", 
			win_ws_barchart );

		/* one-time initialization of graphics context */

    		valueMask = GCBackground | GCForeground | GCLineWidth ;
    		values.line_width = BC_WIDTH ;
    		values.foreground = WhitePixel(  theDisplay, screen);
    		values.background = BlackPixel(  theDisplay, screen);

    		gc_ws_barchart = XCreateGC ( theDisplay, win_ws_barchart, 
						valueMask, &values);
	    }
		
		/* Map event number to event name (string) */
		/* NOTE: EventTable isn't generated by TAE workbench */

#ifndef SILENT
        printf ("\nparmName %s, X Event = %s, type %d\n",
                wptEvent.parmName,
                EventTable[ wptEvent.p_xEvent.type ],
                wptEvent.p_xEvent.type);       /* same data as in wptEvent */

		do_switch ( wptEvent.p_xEvent );   /* based on xev.c */
#endif

	    if (  wptEvent.p_xEvent.type == Expose )	/* Expose */
            {
		/* note access of member of REAL XEvent struct */

		/* Unlike most widgets, the X Workspace (based on the
		 * XmDrawingArea widget), does NOT compress exposure
		 * events, so do it yourself.  
		 *
		 * The field xexpose.count will be 0:
		 *    - if a SINGLE Expose event is sent; or
		 *    - if this Expose event is the LAST in a series of
		 *      such events for the same window.
		 *
		 * Therefore, the check below avoids unnecessary redraws.
		 */
		if (  wptEvent.p_xEvent.xexpose.count == 0 )
		{
		    printf ("\tFORCING (RE)DRAW due to Expose event...\n");
		    XClearWindow ( theDisplay, win_ws_barchart );

		    /* Bogus args passed to EVENT_HANDLER since
		     * this example ignores them anyway.
		     */
	            pan1_update_barchart ( "", 0 );	/* do the drawing */
		}
		/* else NO-OP: skip intermediate exposures */

	    } /* end Expose */

	    if (verbose) 
			dump_wpt_info ( wptEvent );  /* window and widgetId's */

	} /* end parm ws_barchart */
	/* end XLIB */

	/*	Event has occurred from a Panel Parm.  Lookup the event 
	 *	in the dispatch table and call the associated 
 	 *	event handler function. 
 	 */ 
 
       dp = (struct DISPATCH *) wptEvent.p_userContext;  
       for (;  (*dp).parmName != NULL;   dp++)
	    if (strcmp ((*dp).parmName, wptEvent.parmName) == 0)  
		{ 
		parmv = Vm_Find (wptEvent.p_dataVm, wptEvent.parmName); 
		(*(*dp).eventFunction)  
			((*parmv).v_cvp, (*parmv).v_count); 
		break; 
		} 
        } 
} 

FUNCTION VOID initializePanels ()
      
    {
    vmCollection = Co_New (P_ABORT);
    Co_ReadFile (vmCollection, XWORKSPACE_RES, P_ABORT);
    pan1View = Co_Find (vmCollection, "pan1_v");
    pan1Target = Co_Find (vmCollection, "pan1_t");
    if (!pan1Id)
        pan1Id = Wpt_NewPanel ("", pan1Target, 
        	pan1View, NULL, pan1Dispatch, 0);
    else
        printf ("Panel (pan1) is already displayed.\n");
    }
 
/*	event handlers */ 
 
EVENT_HANDLER pan1_iquit (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("exiting...\n");
    Wpt_PanelErase(pan1Id);
    pan1Id=0;
	if (verbose == 0) 
		printf ("\n\tFor verbose WPT info, invoke with -v.\n");
    exit(0);
    }
    
EVENT_HANDLER pan1_update_barchart (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {

	/* XLIB */

   XWindowAttributes win_attr ;
   static int x = 0, y = 0 ;
   int x_end, y_end ;
   int width, height ;
   int window_width, window_height ;

   unsigned int seed ;
   void srand ( ) ;
   int rand ( ) ;
   int i ;

	/* seed random number generator */
	seed = (unsigned int) time(0) ;
	srand (seed) ;
	
	/* start busy indicator which DISCARDS events occuring
	 * between here and Wpt_EndWait.  Try to press the QUIT
	 * button while the bar graph is being drawn and you'll
	 * see that the ButtonPress on QUIT is never seen by
	 * the application.
	 */
	(void) Wpt_BeginWait (pan1Id );

	for ( i = 0; i < NLINES; i++ )
	{

		/* get possibly changed window dimensions */
		XGetWindowAttributes ( theDisplay, win_ws_barchart, &win_attr );
		window_width = win_attr.width ;
		window_height = win_attr.height ;

		/* draw next line of bar graph from bottom up 
	 	* (DECREASING y) and wraparound window width.
	 	*/
		x += XINC ;
		if ( x >= window_width )
		{
			x = XINC ;
			XClearWindow ( theDisplay, win_ws_barchart );
		}
		y = window_height ;	
		x_end = x ;
		y_end = y - rand() % window_height ;

		XDrawLine ( theDisplay, win_ws_barchart, gc_ws_barchart,
				x, y, x_end, y_end );

	} /* for */

	/* turn off busy indicator ; stop discarding events */
	(void) Wpt_EndWait (pan1Id );
    }
    
EVENT_HANDLER pan1_but_clear (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
	/* XLIB */
	XClearWindow ( theDisplay, win_ws_buttonpress );
	XClearWindow ( theDisplay, win_ws_barchart );
    }
    
EVENT_HANDLER pan1_ws_barchart (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {
    printf ("Panel pan1, parm ws_barchart: value = %s\n",
    		count>0 ? value[0] : "none");
    }
    
EVENT_HANDLER pan1_ws_buttonpress (value, count)
    TEXT	*value[];    /* string pointers */
    FUNINT	count;	     /* num of values   */
    {

	/* XLIB */

	/* This handler uses Wpt_ItemWindow as the hook to X, initializes
	 * a graphics context, and draws a line wherever the mouse button
	 * is pressed.
	 */


    static int first_time = 1 ;
    static int xprevious, yprevious ; /* save coords of last ButtonPress */

	if ( first_time )	/* do one-time initialization */
	{
		first_time = 0 ;
		xprevious = yprevious = 0 ; /* upper left corner of workspace */

		/* get X window id associated with workspace */
		win_ws_buttonpress = Wpt_ItemWindow ( pan1Id, "ws_buttonpress" );
		printf ( "\n===> X window id for ws_buttonpress is hex: %x\n", 
			win_ws_buttonpress );

		/* one-time initialization of graphics context */

    		valueMask = GCBackground | GCForeground 
				| GCLineWidth | GCLineStyle 
				| GCFillRule | GCFillStyle ;
    		values.line_width = BP_WIDTH ;
    		values.line_style = LineSolid ; 
				/* or LineOnOffDash, LineDoubleDash */
    		values.fill_style = FillSolid ;
    		values.fill_rule  = EvenOddRule ;
    		values.foreground = WhitePixel(  theDisplay, screen);
    		values.background = BlackPixel(  theDisplay, screen);

    		gc_ws_buttonpress = XCreateGC ( theDisplay, win_ws_buttonpress, 
						valueMask, &values);
	}

#ifndef SILENT
        printf ("\nparmName %s, X Event = %s, type %d\n",
                wptEvent.parmName,
                EventTable[ wptEvent.p_xEvent.type ],
                wptEvent.p_xEvent.type);       /* same data as in wptEvent */

		do_switch ( wptEvent.p_xEvent );   /* based on xev.c */
#endif

	/* only draw line if ButtonPress */
	if ( wptEvent.p_xEvent.type == ButtonPress ) /* constants from X.h */
	{
#ifndef SILENT
	printf ("\tButtonPress event: button = %d, x = %d, y = %d\n",
		wptEvent.p_xEvent.xbutton.button, 
		wptEvent.p_xEvent.xbutton.x, 
		wptEvent.p_xEvent.xbutton.y);
#endif

		/* Draw small rectangle around buttonpress.
		 * For most resolutions, this will look like a dot.
		 * Could use XDrawPoints instead with 9 points:
		 *	o o o
		 *	o x o	where 'x' is buttonpress coordinate.
		 *	o o o
		 */
		XDrawRectangle ( theDisplay, win_ws_buttonpress, 
				gc_ws_buttonpress, 
                        	wptEvent.p_xEvent.xbutton.x -1,
                        	wptEvent.p_xEvent.xbutton.y -1,
				POINT_WIDTH, POINT_HEIGHT );

		/* Draw line connected to previous buttonpress */
		XDrawLine ( theDisplay, win_ws_buttonpress, gc_ws_buttonpress, 
			xprevious, yprevious,
			wptEvent.p_xEvent.xbutton.x,
			wptEvent.p_xEvent.xbutton.y );

		/* Save last ButtonPress coordinate */
		xprevious = wptEvent.p_xEvent.xbutton.x ;
		yprevious = wptEvent.p_xEvent.xbutton.y ;


	}
	if (verbose)
		dump_wpt_info ( wptEvent );  /* window and widgetId's */
} /* end pan1_ws_buttonpress */
/***************************************************************************/
dump_wpt_info ( wptEvent )
WptEvent wptEvent ;
{

		/* Get some of the many details stored in the
		 * WptEvent structure. For details, see the
		 * "TAE C Programmer's Manual, Volume 2",
		 * especially section 4.4.
		 */
	    printf ("\n\tInfo directly from WptEvent structure:\n");
	    printf ( "panelId = %x, windowId = %x, eventWindow = %x,\n",
			wptEvent.p_panelId,
			wptEvent.p_windowId,
			wptEvent.p_eventWindow );
	    printf ( "panelWidgetId = %x, relativeWindow = %x\n",
			wptEvent.p_panelWidgetId,
			wptEvent.p_relativeWindow );

		/* Verify event struct info by calling Wpt_ functions */
	    printf ("\tInfo from Wpt_ calls:\n");
	    printf ("Wpt_PanelWindow = %x, Wpt_PanelTopWindow = %x, \n",
			Wpt_PanelWindow (wptEvent.p_panelId),
			Wpt_PanelTopWindow (wptEvent.p_panelId) );
	    printf ("Wpt_PanelWidgetId = %x, Wpt_ItemWindow = %x\n",
			Wpt_PanelWidgetId (wptEvent.p_panelId),
			Wpt_ItemWindow (wptEvent.p_panelId, wptEvent.parmName) );
}
/***************************************************************************/

	/* from here to EOF borrowed from MIT's xev.c by Jim Fulton */

do_switch ( event )
XEvent	event ;		/* raw X event */
{
	switch (event.type) {
	  case KeyPress:
	    prologue (&event, "KeyPress");
	    do_KeyPress (&event);
	    break;
	  case KeyRelease:
	    prologue (&event, "KeyRelease");
	    do_KeyRelease (&event);
	    break;
	  case ButtonPress:
	    prologue (&event, "ButtonPress");
	    do_ButtonPress (&event);
	    break;
	  case ButtonRelease:
	    prologue (&event, "ButtonRelease");
	    do_ButtonRelease (&event);
	    break;
	  case MotionNotify:
	    prologue (&event, "MotionNotify");
	    do_MotionNotify (&event);
	    break;
	  case EnterNotify:
	    prologue (&event, "EnterNotify");
	    do_EnterNotify (&event);
	    break;
	  case LeaveNotify:
	    prologue (&event, "LeaveNotify");
	    do_LeaveNotify (&event);
	    break;
	  case Expose:
	    prologue (&event, "Expose");
	    do_Expose (&event);
	    break;
	  case ConfigureNotify:
	    prologue (&event, "ConfigureNotify");
	    do_ConfigureNotify (&event);
	    break;
	  case MappingNotify:
	    prologue (&event, "MappingNotify");
	    do_MappingNotify (&event);
	    break;
	  default:
	    printf ("Unknown event type %d\n", event.type);
	    break;
	} /* switch */
} /* do_switch */

prologue (eventp, event_name)
    XEvent *eventp;
    char *event_name;
{
    XAnyEvent *e = (XAnyEvent *) eventp;

    printf ("\n%s event, serial %ld, synthetic %s, window 0x%lx,\n",
	    event_name, e->serial, e->send_event ? Yes : No, e->window);
    return;
}


do_KeyPress (eventp)
    XEvent *eventp;
{
    XKeyEvent *e = (XKeyEvent *) eventp;
    KeySym ks;
    char *ksname;
    int nbytes;
    char str[256+1];

    nbytes = XLookupString (e, str, 256, &ks, NULL);
    if (ks == NoSymbol)
	ksname = "NoSymbol";
    else if (!(ksname = XKeysymToString (ks)))
	ksname = "(no name)";
    printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
	    e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
    printf ("    state 0x%x, keycode %u (keysym 0x%x, %s), same_screen %s,\n",
	    e->state, e->keycode, ks, ksname, e->same_screen ? Yes : No);
    if (nbytes < 0) nbytes = 0;
    if (nbytes > 256) nbytes = 256;
    str[nbytes] = '\0';
    printf ("    XLookupString gives %d characters:  \"%s\"\n", nbytes, str);

    return;
}

do_KeyRelease (eventp)
    XEvent *eventp;
{
    do_KeyPress (eventp);		/* since it has the same info */
    return;
}

do_ButtonPress (eventp)
    XEvent *eventp;
{
    XButtonEvent *e = (XButtonEvent *) eventp;

    printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
	    e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
    printf ("    state 0x%x, button %u, same_screen %s\n",
	    e->state, e->button, e->same_screen ? Yes : No);

    return;
}

do_ButtonRelease (eventp)
    XEvent *eventp;
{
    do_ButtonPress (eventp);		/* since it has the same info */
    return;
}

do_MotionNotify (eventp)
    XEvent *eventp;
{
    XMotionEvent *e = (XMotionEvent *) eventp;

    printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
	    e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
    printf ("    state 0x%x, is_hint %u, same_screen %s\n",
	    e->state, e->is_hint, e->same_screen ? Yes : No);

    return;
}

do_EnterNotify (eventp)
    XEvent *eventp;
{
    XCrossingEvent *e = (XCrossingEvent *) eventp;
    char *mode, *detail;
    char dmode[10], ddetail[10];

    switch (e->mode) {
      case NotifyNormal:  mode = "NotifyNormal"; break;
      case NotifyGrab:  mode = "NotifyGrab"; break;
      case NotifyUngrab:  mode = "NotifyUngrab"; break;
      case NotifyWhileGrabbed:  mode = "NotifyWhileGrabbed"; break;
      default:  mode = dmode, sprintf (dmode, "%u", e->mode); break;
    }

    switch (e->detail) {
      case NotifyAncestor:  detail = "NotifyAncestor"; break;
      case NotifyVirtual:  detail = "NotifyVirtual"; break;
      case NotifyInferior:  detail = "NotifyInferior"; break;
      case NotifyNonlinear:  detail = "NotifyNonlinear"; break;
      case NotifyNonlinearVirtual:  detail = "NotifyNonlinearVirtual"; break;
      case NotifyPointer:  detail = "NotifyPointer"; break;
      case NotifyPointerRoot:  detail = "NotifyPointerRoot"; break;
      case NotifyDetailNone:  detail = "NotifyDetailNone"; break;
      default:  detail = ddetail; sprintf (ddetail, "%u", e->detail); break;
    }

    printf ("    root 0x%lx, subw 0x%lx, time %lu, (%d,%d), root:(%d,%d),\n",
	    e->root, e->subwindow, e->time, e->x, e->y, e->x_root, e->y_root);
    printf ("    mode %s, detail %s, same_screen %s,\n",
	    mode, detail, e->same_screen ? Yes : No);
    printf ("    focus %s, state %u\n", e->focus ? Yes : No, e->state);

    return;
}

do_LeaveNotify (eventp)
    XEvent *eventp;
{
    do_EnterNotify (eventp);		/* since it has same information */
    return;
}


do_Expose (eventp)
    XEvent *eventp;
{
    XExposeEvent *e = (XExposeEvent *) eventp;

    printf ("    (%d,%d), width %d, height %d, count %d\n",
	    e->x, e->y, e->width, e->height, e->count);
    return;
}


do_ConfigureNotify (eventp)
    XEvent *eventp;
{
    XConfigureEvent *e = (XConfigureEvent *) eventp;

    printf ("    event 0x%lx, window 0x%lx, (%d,%d), width %d, height %d,\n",
	    e->event, e->window, e->x, e->y, e->width, e->height);
    printf ("    border_width %d, above 0x%lx, override %s\n",
	    e->border_width, e->above, e->override_redirect ? Yes : No);
    return;
}

do_MappingNotify (eventp)
    XEvent *eventp;
{
    XMappingEvent *e = (XMappingEvent *) eventp;
    char *r;
    char rdummy[10];

    switch (e->request) {
      case MappingModifier:  r = "MappingModifier"; break;
      case MappingKeyboard:  r = "MappingKeyboard"; break;
      case MappingPointer:  r = "MappingPointer"; break;
      default:  r = rdummy; sprintf (rdummy, "%d", e->request); break;
    }

    printf ("    request %s, first_keycode %d, count %d\n",
	    r, e->first_keycode, e->count);
    return;
}


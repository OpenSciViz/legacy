# Begin TAE Plus sample .profile (v5.3)
TAE=location_of_TAE_software
# e.g., /home/tae/v53
export TAE
. $TAE/bin/sh/taesetup

# $TAEDEMOBIN is needed only if you frequently run demos.
PATH=$PATH:$TAEBIN:$TAEDEMOBIN

# $TAE/Xtae/man is needed only if you use DDO widgets
MANPATH=$MANPATH:$TAE/man:$TAE/Xtae/man
export MANPATH
# End TAE Plus sample .profile

# Begin TAE Plus sample .cshrc (v5.3)
if ($?TAE == 0) then
    setenv TAE location_of_TAE_software
    # e.g., /home/tae/v53
    #
    source $TAE/bin/csh/taesetup
    #
    # Call this ALSO if you modify TAE Plus source code:
    # source $TAE/bin/csh/taesetupsrc
    # Call this ALSO if you use TAE Classic:
    # source $TAE/bin/csh/taesetupclassic
endif
# End TAE Plus sample .cshrc

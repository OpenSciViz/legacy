# Begin TAE Plus sample .login (v5.3)
# It is assumed that your $path also has 
# /usr/bin/X11 (or wherever X11 binaries are installed).
# Users of res2uil also need to have the `uil' compiler 
# in their search path.
#
set path = ($path $TAEBIN)

# Optionally use this instead of above if 
# you would like to use the TAE Plus demos.
# set path = ($path $TAEBIN $TAEDEMOBIN)

# Manual pages for TAE API (Wpt, Vm, Co routines).
#
# NOTE: If you plan to use UIL for DDO widgets
# (or write an Xt application with DDO widgets),
# then append ":$TAE/Xtae/man" to each "setenv" line below.
#
if ($?MANPATH == 0) then
    setenv MANPATH /usr/man:$TAE/man
    # OR: setenv MANPATH /usr/man:$TAE/man:$TAE/Xtae/man
else
    setenv MANPATH ${MANPATH}:$TAE/man
    # OR: setenv MANPATH ${MANPATH}:$TAE/man:$TAE/Xtae/man
endif
# End TAE Plus sample .login

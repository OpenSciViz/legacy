/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/******************************************************************************/
// Header file for WPT presentation type File Selection.
//
// CHANGE LOG:
// 01-mar-92	Initial version...rt
// 03-mar-92	Sun C++ doesn't like prevtarget/button = NULL...rt
// 29-sep-92	Added includes needed by keep environment variable feature...rt
// 05-oct-92	Fix C++ anachronisms...rt
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw
/******************************************************************************/
#ifndef I_FILESEL
#define I_FILESEL 0

#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/List.h>
#include <Xm/FileSB.h>
#endif
#include <sys/param.h>

// For FileselA class
#define  FILESEL_SENTINEL  	0xFF1215FF	

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "file selection"
class FileselP : public Presentation {
public:

	int TypeOk (int type) 
		{return type == V_STRING ;} // no V_REAL or V_INTEGER
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "filesel";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};

/************************************************************************/

//WidgetAgent representing a File Selection Box Widget (as a WptItem)

class FileselA : public WptItemDialog {
protected:
    TEXT   *prevbutton;
    TEXT   *prevtarget;
    TEXT   *origfilter;
    void   Check() 
	{if (sentinel != FILESEL_SENTINEL) abort(); }

    XtCallbackProc   selectCallbackPtr;  
    void             *selectCallbackData;

    int BldWptArgList (Symbol *targetSym, Symbol *viewSym, Widget parentWidget);

public:
    FileselA (TEXT *name, WptPanel *panelId, Symbol *targetSym,
	      Symbol *viewSym);	
    ~FileselA();   

    void FileselSelect (void *callData);	
    void SetAppCallback (TEXT *callbackType, XtCallbackProc appCallbackProc,
			 void *appCallbackData);
    void DispatchCallback (Widget widgetId, TEXT *callbackType, void *callData);
    int DisplayNewValue (int newTarget);
    int DefaultArgs (Widget, XtkArg **);
    int UpdateView (Symbol *newView ); 	

    int SetState (int newState) {return SetStatePropagate(newState);}
    int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState) 
    	{return WptItem::GetState(currentState);}
   
    Boolean DefaultTraversalState() {return True;};
    };
#endif

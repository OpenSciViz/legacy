/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type TEXT
//
// 23-aug-90 Added setstate and active...tpl
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92	PR1257: Use <> for #include filenames...ljn
// 21-jan-92    Added DefaultTraversalState...tpl
// 03-mar-92	Added UpdateView to update constraint error msg...cew
//
#ifndef I_TEXTENTRY
#define I_TEXTENTRY 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <TextEdit.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/TextEdit.h>
#endif


// For TextA class 
#define  TEXT_SENTINEL  0xFF1203FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "text"

class TextP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}		// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "text";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a TextWidget (as a WptItem)

class TextA : public WptItem {
protected:
    String  current;
    String  previous;
    void    Check() 
	{if (sentinel != TEXT_SENTINEL) abort(); }
    XtCallbackProc   executeCallbackPtr;  
    void *  executeCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
public:
    TextA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~TextA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int  DisplayNewValue(int newTarget);	// display new value in target
    void *ConvertValue(TEXT *stringVal, 
	TEXT *errorMsg, TEXT *errorKey);	// convert string to data type
    void SetLastValInTarget();
    Boolean DefaultTraversalState()
                                {return True;}
    int UpdateView (Symbol *newView);
    };
#endif


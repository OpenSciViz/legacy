/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//
//	WidgetAgent: Encapsulates a Widget at Wpt panel or item level.  
//
// Change Log:
//
// 06-apr-89	Initial Release...dm
// 10-apr-89	Add panel CloseItems() and item CloseInput()...dm
// 28-apr-89	Changes to Protected function BldWptArgList()...dm
// 12-may-89	Cleanup, Add SetSelection() and currentSelection...dm
// 19-jun-89 	Make const "static", else g++ makes them global...ljn	
// 19-jun-89	Update  for help  implementation...dm
// 22-aug-89	Support for multiselection...krw
// 15-dec-89    remove const from declarations to remove warning...tpl
// 08-feb-90    AT&T C++ 2.0 conversion; add GetWidgetId(), GetManipulate()..ljn
// 22-oct-90	"Fast" panel support: Finish() and XtkArg info fields...ljn
// 23-oct-90	declare DoICCCM...ljn,kbs
// 11-nov-90	added GetState()...tpl
// 11-nov-90	Save preferred panel creation state...ljn
// 12-nov-90	Remove #ifdef MOTIF's and some non-MOTIF fields...ljn
// 15-nov-90    Added DefaultItem...tpl
// 16-nov-90	Added DefaultItemName...krw
// 18-nov-90    Changed SetState to a non on-line function...tpl
// 20-nov-90	GetPreferredState no longer in-line...ljn
// 27-oct-90    Major overhaul of size and positioning. Added x/yOffSet..krw
// 01-feb-91    added method ChildPanel in wptpanel...tpl
// 21-feb-91	hp port for 5.0...ljn
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 10-feb-92	Added autoSize to WptItem...cew
// 18-feb-92	Add SetStateNoPropagate and SetStatePropagate to WptItem.
//		Default SetState to SetStateNoPropagate...cew
// 22-feb-92	Implementation of class WptItemDialog and other Dialog stuff;
// 		Removed methods UpdateComponent & DisplayNewComponentVal...ljn
// 24-feb-92	New parameter to Manipulate to support item relocation...crb
// 03-mar-92    Added SetScrollbar/WidgetColors to WptItem class...rt
// 03-mar-92	Added customizable constraint error message...cew
// 05-mar-92	Sun C++ 2.1 port...ljn
// 18-mar-92	Added MessageNoBlock...cew
// 00-mar-92	Added Finish to WptItem for scrolled workspace...kbs,krw
// 27-mar-92	Added ValidateSize to WptItem for scrolled workspace...kbs,cew
// 30-mar-92	Multiple Display support.  Added panelId to WptItem ctor.
//		Removed panelId from DoSetUp.  Added screen private data.
//		Added WptItemDialog ctor...cew
// 01-apr-92    Provide capability to customize help style...ljn
// 02-apr-92	Declaration of Lower()...crb
// 03-apr-92    Declared some TEXT as const TEXT...tpl
// 22-apr-92    Scrolled Panel support: WidgetAgent additions: GetSWwidgetId(),
//              IsScrollablePanel(); WptPanel additions: PanelScrollbars,
//         	Get_ScrWinPadding(), SWwidgetId, ScrWinPad_*, WorkWin*, 
//         	HighlightWindow()...kbs
// 11-may-92    Added PanelScrollbarsPolicy method for panel...tpl
// 10-jul-92	PR 1421: Add Manage,Unmanage methods so that HideItem removes
//		item from traversal list...cew
// 14-sep-92	PR 1591 et al: new "whichHandle" parm to MakeRubberBand
//		in support of window-type selection handles...crb
// 05-oct-92	Fix C++ anachronisms...rt
// 10-feb-93	new CreateScriptedWptEvent..palm
// 22-jun-93	set active=TRUE in WptItem ctor...palm
// 01-jul-93	Extricate IV2.6.  Use WptRubberRect instead of IV RubberRect.
//		Remove WBCanvas and Painter...cew
// 02-aug-93	PR852: Add SetVec_SELECT to set _SELECT as a vector...cew
// 20-oct-93	PR2421: Removed "&" from SetOrigin, SetOffset, SetSize 
//		parameters. The reference was unecessary...krw
// 21-feb-95	SunCC 4 port: fixed "name hides WidgetAgent::name" warning...dag
/*****************************************************************************/

#ifndef I_WIDGETAGENT
#define I_WIDGETAGENT 0

#include	<taeconf.inp>
#include	<slist.h>
#include	<mousemode.h>
#include        <symbol.h>
// WPT specific definitions
#include	<wptdefs.h>		
// function declarations
#include	<declare.h>		
// Vm to Xt resource conversion
#include	<resconvert.h>		

class	WptRubberRect;
class   WptPanel;
class   WptItem;
class   WptHelpPanel;


class WidgetAgent {
protected:
	int	sentinel;		// Unique id for class/subclass
    	TEXT	name[NAMESIZ+1];	// object name
    	Widget	widgetId;		// widget Id returned by Xt
    	Window	thisw;			// widget window Id
    	Window	shellw;			// top level shell window id 
    	WptPanel  *parentPanel;		// parent panel, if any
    	Coord	border;			// border width
    	Coord	xOrigin, yOrigin;	// relative to parent
    	Coord	xSize, ySize;		// dimension
	Coord	xOffSet, yOffSet;	// start of object body rel. to origin
	int	managed;		// TRUE if currently managed
    	int	mapped;			// TRUE if currently mapped
    	int	manipulate;		// Is manipulation allowed ?
    	int	edge;			// stretch type
	WptArg  **wptArgs;	   	// Wpt types args from Symbol to Xtk
	XtkArg  traArgs[3];	   	// Wpt types args from Symbol to Xtk 
        slist   helpPanelList;          // help panels attached to this object
	int	currentSelection;	// True if item is currently selected
        Boolean traversalOn;        	// True or False
	Screen	*screen;		// screen pointer
friend WptHelpPanel;

// Internal functions:
    virtual void    Check() 		// Integrity Check
	{if (sentinel != WIDGET_SENTINEL) abort(); }
    virtual int  ConstraintArgs(Widget, XtkArg **);   	// parent constraints
    virtual int  DefaultArgs(Widget, XtkArg **);   //Wpt default Args for Widget

    virtual void   Redraw() 
	{Xt_RedrawWidget(widgetId);}

    int BldXtkArgList(Widget parentWidget, XtkArg** argList);
    virtual void  BldTraversalArgs(Symbol *view, WptPanel *, 
					Boolean DefaultState);
    void FreeArgList(XtkArg *argList) 
	{if (argList != NULL) tae_free(argList); argList = NULL; }
    void  FreeWptArgList();
    int NumElements(void * list[]);	   // number of elements in a vector

public: 				
// External functions: 
    WidgetAgent ();			   // subclasses do the work
    ~WidgetAgent (); 
    virtual void ModeSetup (int mode) {mode=mode;} // special setup given a mode

    void GetOrigin (Coord& x, Coord& y)
        {x = xOrigin; y = yOrigin;}
    void SetOrigin (Coord x, Coord y)		// needed by WB
        {xOrigin = x; yOrigin = y;}
    void GetOffset(Coord& xoff, Coord& yoff)
        {xoff = xOffSet; yoff = yOffSet;}
    void SetOffset(Coord xoff, Coord yoff)	// needed by WB
        {xOffSet = xoff; yOffSet = yoff;}
    void SetSize (Coord width, Coord height)	// needed by WB
	{xSize = width; ySize = height;}
    Coord GetBorder () {return border;}

    void GetSize (Coord& width, Coord& height) 
	{width = xSize; height = ySize;}

    Window WindowId () {return thisw;}		// give window Id
    Window ShellWindow () {return shellw;}	// give top level window Id
    WptPanel * ParentPanel () 
	{return parentPanel;}			// NULL for WPT panels
    
    void UnMap ();			// for easy/fast hiding
    void Map (); 			// for easy/fast un-hiding
    void Raise ();
    void Lower ();
    void UnManage ();
    void Manage ();

    TEXT *Name () {return name;}	// Pointer to object name
    WptRubberRect * MakeRubberband (Coord ex, Coord ey,
		WidgetAgent **theList, int,
		WptRubberRect **frameRubber, int whichHandle);
					// Move/Resize/Select the object
    int Manipulate (XEvent & xEvent, WidgetAgent **theList,
		    int WAs, Window *winId);
    void SetSelection(int selectFlag);	// select/unselect the object
    Widget GetWidgetId() {return widgetId;}
    int GetManipulate() {return manipulate;}
    Boolean GetTraversalOn() {return traversalOn;}
    void SetTraversalOn(Boolean state ) {traversalOn = state;}

    // scrolled panel support
    Bool            IsScrollablePanel ();
    virtual Widget  GetSWwidgetId() {return nil;} // widget id of scrolled win

// virtual functions: 
   virtual Boolean DefaultTraversalState()
				{return False;}; 
    virtual Window XHandle() {return thisw;}	// window for manipulation 
    virtual Widget WidgetHandle() { return NULL; }
    virtual void Highlight ()  {}
    virtual void UnHighlight () {}
    virtual int  GetMode() {return nil;}	// in subclass
    virtual void Move (Coord x, Coord y) {x=x; y=y;}
    virtual void Resize (Coord x, Coord y, Coord width, Coord height)
			{x=x; y=y; width=width; height=height;}
    virtual void GetBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{x1 = xOrigin+xOffSet; y1 = yOrigin+yOffSet;
	 x2 = x1+xSize-1; y2 = y1+ySize-1;}

    virtual void GetFullSize (Coord& fullWidth, Coord& fullHeight)
	{ fullWidth = xSize+2*xOffSet; fullHeight = ySize+xOffSet+yOffSet;}

    virtual void GetFullBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{Coord fx, fy; GetFullSize(fx, fy); 
	 x1 = xOrigin; y1 = yOrigin;
	 x2 = x1+fx-1; y2 = y1+fy-1;}

    virtual void MapToRoot(Coord x1, Coord y, Coord *rootx, Coord *rooty)
			{*rootx = x1; *rooty=y;}

// Help functions
    virtual void Helper() {}			// help display function
    void DisplayHelp(Boolean panelHelp, TEXT *helpSpec,
		WidgetAgent *helpObject, WptPanel *reqPanel);
    
};

//
//	WptPanels are widgets used to display/manipulate a composite
//	widget into which item level widgets may be placed.
//
//	The Boolean member "manipulate" indicates whether
//	manipulation functions are allowed for a panel instance.
//	(It is required for panels which are being designed, not allowed
//	for panels which are used to provide input to application.) 
//	 


class WptPanel : public WidgetAgent {
protected:
 	Widget		toplevel;		// top level shell widget
        Widget          titleW;                 // panel title widget
 	int		wptFlags;		// flags to use for Wp_NewPanel
 	Window		relativeWindow;		// relative window Id
	void		*userContext;		// user given association data
    	SymbolTable     *targetSt;		// Target Symbol table for panel
    	SymbolTable  	*viewSt;		// Symbol table for view 
    	SymbolTable     *initialTarget;		// Initial target Symbol table 
	slist		itemList;		// list of items (WptItem ptrs)
    	int		modeFlag;		// current mode of operation
	XtkArg		*xtkArgList;		// Xt args for panel creation
	int		numXtkArgs;		// number of Xt args

	WptItem		*helpItemId;		// Id of selected help button
        WptItem         *defaultItem;           // Id of default item
	TEXT 		defaultItemName[2*NAMESIZ+2]; // Could include comp name
// scrolled panel
        int	 	PanelScrollbars ; 	// possible values are:
                                        	// WPT_SCROLLBARS_NONE,
                                        	// WPT_SCROLLBARS_AS_NEEDED, 
                                        	// WPT_SCROLLBARS_ALWAYS
	Widget		SWwidgetId ;    	// NOTE: 
						// scrwin != widgetId (box)
        					// Constrain shell size to 
						// workSize + ScrWinPad_*
        Dimension       ScrWinPad_Width, ScrWinPad_Height; // scrollbar dimen
        TAEINT          WorkWinWidth, WorkWinHeight; // work region size
						// Given requested size, 
						// return max allowed size.
	void 		ValidateSize (Coord width, Coord height, 
                                      Coord *validWidth, Coord *validHeight);

	Window		HighlightWindow()	// which win gets red dots
				{
	    			if ( PanelScrollbars ) 
		 			return ( XtWindow ( SWwidgetId ) );
	    			else return ( thisw ); // normal case
				}
// end scrolled panel

// Internal functions:
    void    Check() 			// Integrity Check
	{if (sentinel != PANEL_SENTINEL) abort(); }
    int  	BldWptArgList(Symbol *target, 		
			Symbol *view, Widget parentWidget, Display *dpy); 
    int  	DefaultArgs(Widget, XtkArg **);   //default Args for panelWidget
    //int  	ConstraintArgs(Widget, XtkArg **);   	// parent constraints
    void    	PostRealize();
    TEXT**	GetItemNames(SymbolTable* viewSt);
    void	FreeItemNames(TEXT** itemNames);
friend WptHelpPanel;


public: 
    WptPanel (TEXT* name, SymbolTable* target, SymbolTable* view, 
		Window parentW, void * userContext, int flags,
		Display *display);
    ~WptPanel () ;		
    virtual 	Boolean  ValidPanel() {return (sentinel == PANEL_SENTINEL); }
    WptItem*    CreateItem(TEXT* name, Symbol *target, 
	     		Symbol *view, Boolean& helpAgent);  // create an item
    WptItem*    AddNewItem(const TEXT* name, Symbol *target, 
	     		Symbol *view);		  // Add new item to a panel
    int		EraseItem(WptItem * itemId);	  // erase item from panel
    void	AddItemInfo(WptItem *itemId);	  // add a new item's info
    void 	InsertInList(WptItem * itemId);

    int ItemCount() { return itemList.count(); }
    Window XHandle() {return shellw;}  	// use shell window for manipulation 
    Widget WidgetHandle() {return toplevel;}
    SymbolTable* TargetSt()  {return targetSt;}
    SymbolTable* ViewSt()  {return viewSt;}	   // only at new WptPanel time
    void* UserContext() { return userContext; }	   // user's association data
    Window RelativeWindow() { return relativeWindow; }

    void Highlight ();
    void UnHighlight ();
    WptItem *GetItemId(TEXT *itemName);
    COUNT GetPreferredState ();
    void DoICCCM(Display *dpy);	// Handles frame, decorations, icon, etc.
    void Finish();	// Part 2 of WptPanel ctor ("fast" panel support)
    int DefaultItem ( const TEXT *name);
    void DefaultItemName ( const TEXT *itemName)
	{s_bcopy (itemName, defaultItemName, 2*NAMESIZ+1);}
    WptItem *GetDefaultItemId () {return defaultItem;}
    const TEXT *GetDefaultItemName () {return defaultItemName;}
    TEXT *GetDefaultItemComp ()
	{int period = s_index (defaultItemName, '.');
	 return (period >= 0)? &defaultItemName[period+1] : nil;} 
    Boolean  ChildPanel()			// item generates event?
	{return ( wptFlags & WPT_CHILD ); } 

    // Scrolled panel: get relevant size of H and V scrollbars
    void Get_ScrWinPadding (Dimension& padWidth, Dimension& padHeight)
	{ padWidth = ScrWinPad_Width; padHeight = ScrWinPad_Height; }
    Widget 	GetSWwidgetId() {return SWwidgetId;} // override virtual
    int   PanelScrollbarsPolicy() { return PanelScrollbars;}

// Virtual Functions from widgetagent
    void Move (Coord x, Coord y);
    void Resize (Coord x, Coord y, Coord width, Coord height);
    void GetBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{x1 = xOrigin+xOffSet; y1 = yOrigin+yOffSet;
	 x2 = x1+xSize-1; y2 = y1+ySize-1;}

    void GetFullSize (Coord& fullWidth, Coord& fullHeight)
	{ fullWidth = xSize+2*xOffSet; fullHeight = ySize+xOffSet+yOffSet;}

    void GetFullBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{Coord fx, fy; GetFullSize(fx, fy); 
	 x1 = xOrigin; y1 = yOrigin;
	 x2 = x1+fx-1; y2 = y1+fy-1;}

    void MapToRoot(Coord x, Coord y, Coord *rootx, Coord *rooty)
	{*rootx = xOrigin + xOffSet + x; *rooty = yOrigin +yOffSet + y;}

//  Functions to enable/disable the panel into "OPERATE" mode 
    void SetInitialMode (int flag)		// Initial mode setup
	{manipulate = flag;
	 modeFlag = manipulate ? MANIPULATE : OPERATE; 
	 SetMode(modeFlag);}
    void SetMode(int mode);	 	// Set Panel mode: OPERATE/HELP etc.
    int  GetMode() {return modeFlag;}
    void ChangePanelCursor(XCursor cursor) {Xt_DefineCursor(toplevel, cursor);}


//  Functions needed from application level
    void Reset();
    void Message (const TEXT *message);
    void MessageNoBlock ( const TEXT *message, void * userContext, BOOL returnToApp);
    void BeginWait (XCursor cursorId = nil);
    void EndWait ();
    void CloseItems();		   	// Terminate active items 

// Wpt Help related functions
    void Helper();				// Panel help requestor 
    void BeginHelpMode(WptItem *helpbtn);	// Mark panel is in help mode
    void EndHelpMode();				// Reset panel to original mode
};
    

// Meta Class WptItem is the presentation of a target item

class WptItem : public WidgetAgent {
protected:
    Symbol  *targetSym;			// Item's target  symbol
    Symbol  *viewSym;			// Item's view  symbol or nil 
    Boolean  noLastValue;		// No previous value yet
    int	    active;
    BOOL    autoSize;           	// True if can automatically resize
    TEXT    *err_str;			// custom constraint error msg

//Internal functions:
    void    Check() 
	{if (sentinel != ITEM_SENTINEL) abort(); }
    int  BldWptArgList(Symbol *target, 		// Build own Arglist 
		Symbol *view, Widget parentWidget); 
    void    DoSetUp (TEXT * name, Symbol* target, Symbol *view) ;
    int  ConstraintArgs(Widget, XtkArg **);   	// parent constraints
    Boolean  GenerateEvent()			// item generates event?
	{return (targetSym != NULL && targetSym->Event()); } 
    void SetScrollbarWidgetColors (Widget, Widget);
    void SetChildWidgetColors (Widget, Widget);
    void SetErrorString (Symbol *view);		// parse errormsg into err_str

public:
    WptItem (WptPanel *panelId)
	{sentinel = ITEM_SENTINEL; autoSize = FALSE; err_str = nil;
	 active = TRUE;		// subclass ctor may override 
	 parentPanel = panelId;
	 screen = XtScreen(panelId->GetWidgetId());};
    virtual ~WptItem ();
    void    PostRealize(); 
    virtual void Finish () {}; // Needed by subclass scrolled workspace. 
                               // Finish is called by PostRealize.
                               // Subclass workspace will override ValidateSize.
    // Override this ValidateSize if your presentation needs to verify
    // any WorkBench resizing (eg, agentWb.cc::Resize).
    virtual void ValidateSize (Coord width, Coord height, 
			Coord *validWidth, Coord *validHeight) 
			{ *validWidth = width;  *validHeight = height;};
    Widget  WidgetHandle() {return widgetId;}
    Symbol  *TargetSym() {return targetSym;}
    Symbol  *ViewSym() {return viewSym;}

    void    CreateWptEvent (XEvent *event = NULL, 
		int vIndex = 0, int selectionOnly = FALSE);
    void    CreateScriptedWptEvent (int fd, XEvent *event = NULL, 
		int vIndex = 0, int selectionOnly = FALSE);
    int	    CurrentTargetSelection();	   // currently selected vald, or -1 
    void    SetVec_SELECT();		   // set _SELECT as a vector
    TEXT    **CurrentTargetStrings();	   // current target values, as strings
    virtual void SetAppCallback(TEXT* callbackType, 
	XtCallbackProc  appCallbackProc, void * appCallbackData) 
	{callbackType=callbackType; 
	 appCallbackProc=appCallbackProc;
	 appCallbackData=appCallbackData;}

    void Helper();				// Item help requestor 
    void Highlight ();
    void UnHighlight ();

// Virtual Functions from widgetagent
    void Move (Coord x, Coord y);
    void Resize (Coord x, Coord y, Coord width, Coord height);

    void GetBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{x1 = xOrigin+xOffSet+border; y1 = yOrigin+yOffSet+border;
	 x2 = x1+xSize-1; y2 = y1+ySize-1;}

    void GetFullSize (Coord& fullWidth, Coord& fullHeight)
	{fullWidth = xSize+2*(border+xOffSet);
	 fullHeight = ySize+2*(border+yOffSet);}

    void GetFullBox (Coord &x1, Coord &y1, Coord &x2, Coord &y2)
	{Coord fx, fy; GetFullSize(fx, fy); 
	 x1 = xOrigin; y1 = yOrigin;
	 x2 = x1+fx-1; y2 = y1+fy-1;}

    void MapToRoot(Coord x, Coord y, Coord *rootx, Coord *rooty)
	{parentPanel->MapToRoot(xOrigin+x, yOrigin+y, rootx, rooty);}

// Functions dealing with updating item's  value/view etc.
    int	    SetTargetValue(int validIndex);	// value from valid index
    int	    SetTargetValue(void *valuePtr, int element = 0); 
    int	    UpdateItem(Symbol *source = NULL);	// update selection & display
    int     CheckValid(TEXT *stringVal, int *selectIndex);
    int     ResetLast();			// Reset to last value
    virtual void SetLastValInTarget() {}  	// in subclass

    virtual int  DisplayNewValue (int newT)
                { newT=newT; return 1;}           // update displayed value
    virtual void CloseInput() {}	  	    // Terminate user input
    virtual int  UpdateView (Symbol *view);         // update item's view
    int     CommonViewArgs(Symbol *view,            // common items for update
                XtkArg *argList, TEXT *titleRes = nil);

// Functions overriden by Dialog items.
    virtual Boolean HasComponents() {return False;}
    virtual Widget GetComponentWidgetId (TEXT *comp) {comp=comp; return nil;}

//  Functions to enable/disable the item into "Oparate" mode 
    int  GetMode() { return parentPanel->GetMode(); }
    void EnableOperate(Boolean  flag) 	// allow/disallow subwidget events
	{Xt_AllowSubEvents(widgetId, TRUE, flag, 0); }  
					
//  Check a value for validity.  Should be in Symbol ???
    Boolean CheckValid(int intVal, TEXT *errormsg, TEXT *errorKey);
    Boolean CheckValid(TAEFLOAT realVal, TEXT *errormsg, TEXT *errorKey);
    Boolean CheckValid(TEXT *stringVal, TEXT *errormsg, TEXT *errorKey,
			Boolean copyExact = FALSE);

// Miscellaneous functions.
    void ReverseColor ();			// Reverse fg/bg color of item
    virtual void ReverseLabel(int) {}		// Highlight/Unhighlight label 
    virtual int ReverseField(int index, int flag) 	// Same for component
		{index=index; flag=flag; return 1;}

    virtual int  SetStateNoPropagate(int state); // set XmNsensitive
    virtual int  SetStatePropagate(int state);   // call XtSetSensitive

    virtual int  SetState(int state)
	{SetStateNoPropagate(state); return 0;}
    virtual int  SetState(int index, int state)
	{index=index; state=state; return 0;}   // Set state of value component
    virtual int GetState (int *currentState)
	{ *currentState = active; return 0;}
    virtual int  GetState(int index, int *state) 
	{index=index; state=state; return 0;}   // Get state of value component 

    virtual int DefaultShadowThickness () 
				 { return 0; }  // Default shadow thickness
    virtual int DefaultBorderWidth() 
				 { return 0; }  // Default border thickness
    virtual int DefaultItemLook(int state, TEXT *comp) 
		{state=state; comp=comp; return 0; } // Default look
};

// Meta Class WptItemDialog is the presentation of a target dialog item

class WptItemDialog : public WptItem {
protected:

// Widget Id's for Dialog item components.
    Widget okWidgetId;
    Widget applyWidgetId;
    Widget cancelWidgetId;
    Widget helpWidgetId;
    Widget separatorWidgetId;
    Widget filterWidgetId;
    Widget selectionWidgetId;
    Widget dirlistWidgetId;
    Widget filelistWidgetId;

public:
    WptItemDialog (WptPanel *panelId) : WptItem(panelId) {};
    Boolean HasComponents() {return True;}
    Widget GetComponentWidgetId (TEXT *component)
        {if (s_equal (component, "ok")) return okWidgetId;
         else if (s_equal (component, "apply")) return applyWidgetId;
         else if (s_equal (component, "cancel")) return cancelWidgetId;
         else if (s_equal (component, "help")) return helpWidgetId;
	else return (Widget)NULL;}
    };

//	Return flags from Edge:
static const TOP_EDGE = 	0x01;
static const RIGHT_EDGE = 	0x02;
static const LEFT_EDGE  =	0x04;
static const BOTTOM_EDGE = 	0x08;
static const CORNER = 		0x10;

//	Retrurn code from Manipulate
static const SELECT_ONLY =  	0x01;
static const MOVE_ONLY = 	0x02;
static const RESIZE_OBJECT =	0x04;

#endif

/*
 * Change Log 
 * 29-nov-94	This file contains a fix for the DEC C++ v1.2a
 *		includes under Ultrix 4.4...shj
 */

#if defined(__DECCXX)
struct _XDisplay;
struct _XFreeFuncs;
struct _XSQEvent;
struct _XrmHashBucketRec;
struct _XExten;
struct _XKeytrans;
struct _XDisplayAtoms;
struct _XContextDB;
struct _XIMFilter;
struct _XrmHashBucketRec; 
#endif

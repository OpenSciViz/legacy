/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type TEXTLIST
// CHANGE LOG:
// 06-jul-89	Force STRING data type only...krw
// 10-sep-89	VMS port...ljn
// 24-aug-90    added setstate and active...tpl
// 26-oct-90    added methods for handling multiselection...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 21-jan-92    Added DefaultTraversalState...tpl
// 03-mar-92	Added UpdateView to update constraint error msg...cew
// 15-apr-92	Added boolean fromafile to indicate file or constraints...tpl
// 22-apr-92    Set ValidsRequired to FALSE...tpl

#ifndef I_TEXTLIST
#define I_TEXTLIST 0

#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <TextList.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/TextList.h>
#endif

// For TextlistA widgetagents
#define TEXTLIST_SENTINEL  0xFF1204FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "textlist"
class TextlistP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==V_STRING;}
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "textlist";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class TextlistA : public WptItem {
protected:
    String * current;
    String * previous;
    int previousCount ;
    int currentCount ;
    Boolean  fromafile;

    void    Check() 
	{if (sentinel != TEXTLIST_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
public:
    TextlistA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~TextlistA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int  DisplayNewValue(int newTarget);	// update selection on display
    void SetLastValInTarget();
    void SetTarget(void * valuePtr, TAEINT count );
    void CloseInput();
    Boolean DefaultTraversalState()
                                {return True;}
    int UpdateView (Symbol *newView);
    };
#endif


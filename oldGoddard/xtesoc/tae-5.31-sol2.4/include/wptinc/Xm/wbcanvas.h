/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//	WorkBench form of canvas
//	We vary from the real InterViews form to avoid
//	all of the (WPT conflicting) InterViews code.

//	Canvas encapsulates the window Id and the
//	height, providing a bridge between the InterViews'
//	rubberband classes and X.   The existence of
//	a canvas is usually transient because the height
//	is only obtained during creation.  (In fact, that's
//	the whole idea, to avoid a roundtrip server call
//	(XQueryWindow) every time a coordinate must be
//	converted from InterViews to X.
//	24-feb-89	Renamed to wbcanvas.h. Changed Canvas to
//			WBCanvas to avoid conflict with InterViews...krw
//			Made it a subclass of Canvas
//	03-may-89	Add Display as argument to X_ calls...dm
//	10-sep-89	VMS port...ljn
//	25-jan-90	g++ 1.36 port...ljn
//	10-apr-92	Canvas ctor needs display arg...ga tech, cew
//	12-oct-92	gcc 2.2.2 does not like XorRect...rt
//	07-dec-93	Solaris port (backported dag from aug-93)...cob


#ifndef	I_WBCANVAS
#define	I_WBCANVAS 1

#ifdef VMS
//InterViews Canvas class
#include "inc$interviews:canvas.h"	
#else
//InterViews Canvas class
#include "InterViews/canvas.h"	
#endif

extern "C"
{
void X_GetInfo (Display *,Window,int *,int *,int *,int *, int *);
void X_XorLine (Display *,Window,int,int,int,int); 
#if ((__GNUC__) == (2))) || (defined(sun) && OSMajorVersion >= 5)
/* Do nothing for gcc 2.2.2 */
#else
void X_XorRect (Display *,Window,int,int,int,int);
#endif
}


class WBCanvas : public Canvas{
	Window	window;		// X window id
	Coord	width;		// width as time of canvas creation
	Coord	height;		// height at time of canvas creation
	Coord	x0;		// origin...
	Coord	y0;		
	Coord	border;		// border width
public:
	WBCanvas (Display *dpy, Window id)  : Canvas(dpy, (void *) id) /* dual */
	   {window=id; X_GetInfo (dpy, id, &width, &height, &x0, &y0, &border);}
	
	//   convert to InterViews vertical coordinate from X form

	IvCoord (Coord y) {return height-y;}		
	
	//    convert to X vertical coordinate from InterViews form
	//	(yes, both conversions have the same formula because
	//	height = y_iv + y_x)

	XCoord (Coord y) {return height-y;}

	//	The following take X Coorindates:

	Window GetWindow () {return window;}
	void GetInfo (Coord &xOrigin, Coord &yOrigin, Coord &xSize,
		       Coord &ySize, Coord & theBorder)
	    {xOrigin = x0; yOrigin = y0; xSize = width;
	     ySize = height; theBorder = border;}

	// 	The following take InterViews coordinates:

	void XorLine (Display *dpy, Coord x0, Coord y0, Coord x1, Coord y1)
		{X_XorLine (dpy, window, x0, height-y0, x1, height-y1);}
#if ((__GNUC__) == (2))) || (defined(sun) && OSMajorVersion >= 5)
/* Do nothing for gcc 2.2.2 */
#else
	void XorRect (Display *dpy, Coord x0, Coord y0, Coord x1, Coord y1)
		{X_XorRect (dpy, window, x0, height-y0, x1, height-y1);}
#endif
};
#endif

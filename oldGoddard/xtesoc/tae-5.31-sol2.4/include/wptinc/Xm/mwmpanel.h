/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//      mwmpanel.h
//
// Constants needed only for MWM under OSF/Motif.
//
// These constants relate to panel-level details, such as
// mwmDecorations and mwmFunctions.
// Files agentRt.cc and xtkintf.cc need these constants.

// Change Log:
//
// 16-aug-90 Initial version...kbs
// 04-sep-90 revised for second panel details design...kbs
// 17-oct-90 removed TAE_BITMAP_DEFAULT...kbs
// 25-oct-90 added TAE_MWM_DECOR_TBAR* for FlatBorder with Titlebar...kbs

/////////////////////////////    CHANGES TBD    ///////////////////////////


/* Depends on <Xm/MwmUtil.h> for all MWM_* constants */

/*******         REPLACE MWM_* with:    #include <Xm/MwmUtil.h> ****/

// Unfortunately, Xm/MwmUtil.h includes X11/Xmd.h which has a
// "typedef unsigned char BOOL;" which conflicts with the TAE typedef
// "typedef int BOOL, VOID;" in $TINC/stdh.inp.
// Therefore, we have grabbed about a dozen constants from Xm/MwmUtil.h
// which we realize is not desirable.

// BEGIN from <Xm/MwmUtil.h>
/* bit definitions for MwmHints.functions */
#define MWM_FUNC_ALL            (1L << 0)
#define MWM_FUNC_RESIZE         (1L << 1)
#define MWM_FUNC_MOVE           (1L << 2)
#define MWM_FUNC_MINIMIZE       (1L << 3)
#define MWM_FUNC_MAXIMIZE       (1L << 4)
#define MWM_FUNC_CLOSE          (1L << 5)

/* bit definitions for MwmHints.decorations */
#define MWM_DECOR_ALL           (1L << 0)
#define MWM_DECOR_BORDER        (1L << 1)
#define MWM_DECOR_RESIZEH       (1L << 2)
#define MWM_DECOR_TITLE         (1L << 3)
#define MWM_DECOR_MENU          (1L << 4)
#define MWM_DECOR_MINIMIZE      (1L << 5)
#define MWM_DECOR_MAXIMIZE      (1L << 6)
// END from <Xm/MwmUtil.h>

/* MWM decoration flag masks for XmNmwmDecorations */
/* Not all are currently used. */
#define TAE_MWM_DECOR_NONE        0
#define TAE_MWM_DECOR_SYSTEM      (MWM_DECOR_TITLE | MWM_DECOR_MENU)
#define TAE_MWM_DECOR_MINIMIZE    (MWM_DECOR_TITLE | MWM_DECOR_MINIMIZE)
#define TAE_MWM_DECOR_MAXIMIZE    (MWM_DECOR_TITLE | MWM_DECOR_MAXIMIZE)
#define TAE_MWM_DECOR_TITLEBAR    (TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE |\
                                      TAE_MWM_DECOR_MAXIMIZE)
#define TAE_MWM_DECOR_FRAME       (MWM_DECOR_BORDER)
#define TAE_MWM_DECOR_RESIZE      (MWM_DECOR_BORDER | MWM_DECOR_RESIZEH)

#define TAE_MWM_DECOR_TITLEBAR_NO_RESIZE    (TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE |\
                                      TAE_MWM_DECOR_MAXIMIZE |\
                                      TAE_MWM_DECOR_FRAME)

#define TAE_MWM_DECOR_TITLEBAR_NO_MAXIMIZE    (TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE |\
                                      TAE_MWM_DECOR_RESIZE |\
                                      TAE_MWM_DECOR_FRAME)

// Next 2 constants for FrameStyle of FlatBorder and TitlebarStyle != None
// Next is TAE_MWM_DECOR_ALL - FRAME - RESIZE
#define TAE_MWM_DECOR_TBAR_MOTIF_BORDER    (TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE |\
                                      TAE_MWM_DECOR_MAXIMIZE )

// Next is TAE_MWM_DECOR_ALL - FRAME - MAX - RESIZE
#define TAE_MWM_DECOR_TBAR_NO_MAXIMIZE_BORDER    (TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE )

#define TAE_MWM_DECOR_ALL         (TAE_MWM_DECOR_TITLEBAR |\
                                      TAE_MWM_DECOR_RESIZE)

// Default TAE panel decorations under Motif:
#define TAE_MWM_DECOR_DEF         (MWM_DECOR_BORDER |\
                                      TAE_MWM_DECOR_SYSTEM |\
                                      TAE_MWM_DECOR_MINIMIZE )

/* MWM function flag masks for XmNmwmFunctions */
/* Not all are currently used. */
#define TAE_MWM_FUNC_NONE         0
#define TAE_MWM_FUNC_ALL          (MWM_FUNC_RESIZE | MWM_FUNC_MOVE |\
                                      MWM_FUNC_MINIMIZE |\
                                      MWM_FUNC_MAXIMIZE |\
                                      MWM_FUNC_CLOSE)

// Default TAE panel functions under Motif:
#define TAE_MWM_FUNC_DEF          (MWM_FUNC_MOVE | MWM_FUNC_MINIMIZE )

// All MWM functions except resize
#define TAE_MWM_FUNC_ALL_BUT_RESIZE   (MWM_FUNC_MOVE |\
                                      MWM_FUNC_MINIMIZE |\
                                      MWM_FUNC_MAXIMIZE |\
                                      MWM_FUNC_CLOSE)
#define TAE_MWM_FUNC_RESIZE           (MWM_FUNC_RESIZE)
#define TAE_MWM_FUNC_MIN_MOVE_RESIZE           (TAE_MWM_FUNC_DEF |\
                                                MWM_FUNC_RESIZE)

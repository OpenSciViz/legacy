/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//	slist collections.
//	CHANGE LOG:
//	17-nov-88	new ForEachInSlist...palm
//	22-aug-89	Added first, last and ability to get any entry in 
//			a list (via an index) without removing it from the
//			list...krw
//	29-jan-92	new slist_iterator function "index" and associated
//			logic to maintain index of entries during an slist
//			iteration; new slist::insertBefore and slist::entryptr
//			functions...crb

#ifndef H_SLIST
#define H_SLIST


// 7.3.1
// This header file declares:-

class slist;
class slist_iterator;

typedef void* ent;

class slink {
friend class slist;
friend class slist_iterator;
    slink* next;
    ent e;
    slink(ent a, slink* p) { e=a; next=p; }
};

class slist {
friend class slist_iterator;
    int	   entries;			// number of entries
    slink* last;

private:
    slink *entryptr(int index);		// return ptr to slink entry

public:
    int count() {return entries;}
    int insert(ent a);
    int insertBefore(int index, ent newa);
    int append(ent a);
    ent get();
    void clear();
    void remove (ent a);		// remove from list
    ent entry(int index);
    ent head() {return entry(0);}
    ent tail()  {return entry(count()-1);}

    slist()      { entries=0; last=0; }
    slist(ent a) { entries=1; last=new slink(a,0); last->next=last; }
    ~slist()     { clear(); }
};    

// 7.3.2

//	Fixed from OASIS delivery to match the book 
//	(and to deliver entries from the beginning rather
//	the end)

class slist_iterator {
    slink* ce;
    slist* cs;
    int    listindex;
public:
    slist_iterator(slist& s) { cs = &s; ce = cs->last; listindex = -1;}

    ent operator()() {
        listindex++;
	ent ret = ce ? (ce=ce->next)->e : 0;
	if (ce == cs->last) ce = 0;
	return ret;
    }
    int index() { return listindex; }
};

//	macro for iterating.

#define ForEachInSlist(item, type, aSlist)	\
    {						\
    slist_iterator NAME2(item,_iter)(aSlist);	\
    type item;					\
    while (item = (type) NAME2(item,_iter)())	\
	{

#define EndForEachInSlist(item)			\
	}					\
    }
#endif

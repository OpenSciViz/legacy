/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// CHANGE Log:
// 17-jun-89	Removed declaration of XtkArg as struct...ljn
// 10-sep-89	VMS port...ljn
// 24-jan-90    Add 'extern "C"' for g++ 1.36 port...ljn
// 12-oct-90	Added WptArg->SetValue to fix monochrome fg/bg clash...kbs,krw
// 06-nov-90    Added MinValueCount...rt
// 12-dec-90	Removed #include of Xw.h...ljn
// 28-jan-91    Added XM_ALLOCATED and MULTI_ALLOCATED...tpl
// 21-feb-91	HP port for 5.0...ljn
// 15-mar-91	Added font cache structure and utility routine prototypes...krw
// 28-mar-91	Header files for VMS...ljn
// 10-may-91	Added color cache structure and utility routine prototypes...krw
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 30-mar-92	Multiple Display support.  Added display arg to findFont funcs.
//		Added screen arg to findColor func.  Changed display to screen
//		in CCache.  Changed from display to screen the arg in Convert
//		functions, ColorArg, FontArg, and FontListArg ctors...cew
// 19-apr-92	Added AlignConvert, KeySymConvert and AccelConvert...krw
// 28-nov-94	Ultrix Port: Included decXlibfix.h for a workaround
//		to a DEC C++ include file bug...shj
// 22-feb-95	SunCC 4 port: removed default parm from ConvertFunc...dag
//

#ifndef I_XTRESOURCE
#define I_XTRESOURCE 0

#ifdef VMS
#include <Xatom.h>
extern "C"
{
#include <Intrinsic.h>
}
#include <wptdefs.h>
#include <symbol.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#else
#include <X11/Xatom.h>
#ifdef __ultrix
#include <wptinc/Xm/decXlibfix.h>
#endif
extern "C"
{
#include <X11/Intrinsic.h>
}
#include <wptdefs.h>
#include <symbol.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#endif

#define TAE_ALLOCATED  1
#define X_ALLOCATED    2
#define XM_ALLOCATED    3
#define MULTI_ALLOCATED    4

// Font Caching information
typedef struct fcache
    {
    TEXT *fontName;                             // name of font
    int accessCnt;                              // # of objects using font
    Display *theDisplay;                        // Server with the font
    XFontStruct *fontStruct;
    XmFontList  xmFontList;
    } *FCache;

FCache findFontByFL(Display *dpy, XmFontList xmfontlist);
FCache findFontByFS(Display *dpy, XFontStruct *fontstruct);
FCache findFontByFN(Display *dpy, TEXT *fontName);
// End of Font Caching

// Color Caching information
typedef struct ccache
    {
    TEXT *colorName;				// name of color
    int accessCnt;                              // # of objects using color
    Screen *theScreen;				// Server with the color
    XColor	xColor;				// XColor from server
    } *CCache;

CCache findColorByCN(Screen *screen, TEXT *colorName);
// End of Color Caching

class   Symbol;
struct  VmResource;			// defined below
class   VmToWptArg;			// defined below
class	WptArg;				// defined below 

typedef Arg XtkArg;			// same as used by XToolkit
typedef WptArg* (*ConvertFunc) 
  (VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *s);

struct	VmResource {
	TEXT	*symbolName;		// name in symbol table
	int	index;			// index to value 
};

class VmToWptArg {
public: 
	ConvertFunc	converter;		// conversion function
	TEXT		*xtName;		// Xt resource name
	VmResource	*vmResource;		// Vm resources 	
	int		vmResCount;		// no. of Vm resources 
	void *		defaultVal;		// WPT default value

    TEXT *XtkName() {return xtName; }
    TEXT *SymbolName()  {return vmResource[0].symbolName; }
    int Index() {return vmResource[0].index; }
    void *  Default() {return defaultVal; }
    TEXT *VmName(int i)  {return vmResource[i].symbolName; }
    int  VmIndex(int i) {return vmResource[i].index; }
};

class  WptArg {
protected:
	XtkArg		xtkArg;			// Xt Arg for widget
	Boolean		allocated;		// True if valuePtr allocated
public: 
     WptArg(VmToWptArg *entry)
		{xtkArg.name = entry->XtkName(), 
		allocated = 0; }  		// rest in subclass
     WptArg(TEXT *xtname, XtArgVal xtvalue, Boolean alloc) 
		{xtkArg.name = xtname, xtkArg.value = xtvalue,
		allocated = alloc; }		
    ~WptArg();
    TEXT *Name() {return xtkArg.name; }
    XtArgVal Value() {return xtkArg.value; }
    void SetValue ( XtArgVal newValue )  { xtkArg.value = newValue; }
};

/************* Generalized resources from view block ***************/

// Any resource of type integer (single valued)
class IntgArg : public WptArg {
public: 
    IntgArg(VmToWptArg *entry, Symbol *sym);
    ~IntgArg() {}
};

// Any resource of type real (single valued)
class RealArg: public WptArg {
public: 
    RealArg(VmToWptArg *entry, Symbol *sym);
    ~RealArg() {}
};

// Any resource of type string (single valued) 
class StringArg: public WptArg {
public: 
    StringArg(VmToWptArg *entry, Symbol *sym);
    ~StringArg() {}
};

// Any resource value
class ViewValueArg : public WptArg {
public: 
    ViewValueArg(VmToWptArg *entry, Symbol *sym);
    ~ViewValueArg() {}
};



/*******************  Special type of resources ***************/

class ColorArg : public WptArg {
public: 
    ColorArg(VmToWptArg *entry, Symbol *sym, Widget aWidget, Screen *screen); 
    ~ColorArg() {}
};

class FontArg : public WptArg {
public: 
    FontArg(VmToWptArg *entry, Symbol *sym, Widget aWidget, Screen *screen); 
    ~FontArg() {}
};
class FontListArg : public WptArg {
public: 
    FontListArg(VmToWptArg *entry, Symbol *sym, Widget aWidget, Screen *screen); 
    ~FontListArg() {}
};

// Selection strings from target
class SelectionString : public WptArg {
public: 
    SelectionString(VmToWptArg *entry, Symbol *sym);
    ~SelectionString() {} 
};
//String count from target
class SelectionCount : public WptArg {
public: 
    SelectionCount(VmToWptArg *entry, Symbol *sym);
    ~SelectionCount() {}
};

//String size from target
class MaxStringSize: public WptArg {
public: 
    MaxStringSize(VmToWptArg *entry, Symbol *sym);
    ~MaxStringSize() {}
};

//Index to initial value string from target
class InitialIndex: public WptArg {
public: 
    InitialIndex(VmToWptArg *entry, Symbol *sym);
    ~InitialIndex() {}
};

//value vector from target
class MultiValues : public WptArg {
public: 
    MultiValues(VmToWptArg *entry, Symbol *sym);
    ~MultiValues() {}
};

//value vector from target
class SingleValue : public WptArg {
public: 
    SingleValue(VmToWptArg *entry, Symbol *sym);
    ~SingleValue() {}
};

//value vector from target
class Values : public WptArg {
public: 
    Values(VmToWptArg *entry, Symbol *sym);
    ~Values() {}
};

//maximum value count from target
class MaxValueCount : public WptArg {
public: 
    MaxValueCount(VmToWptArg *entry, Symbol *sym);
    ~MaxValueCount() {}
};

//minimum value count from target
class MinValueCount : public WptArg {
public: 
    MinValueCount(VmToWptArg *entry, Symbol *sym);
    ~MinValueCount() {}
};

//value count from target
class ValueCount : public WptArg {
public: 
    ValueCount(VmToWptArg *entry, Symbol *sym);
    ~ValueCount() {}
};

// item title from view block
class TitleString : public WptArg {
public: 
    TitleString(VmToWptArg *entry, Symbol *sym);
    ~TitleString() {}
};
// XmListConvert
class XmListArg : public WptArg {
public:
    XmListArg(VmToWptArg *entry, Symbol *sym);
    ~XmListArg() {}
};

// XmStringConvert 
class XmStringArg : public WptArg {
public:
    XmStringArg(VmToWptArg *entry, Symbol *sym);
    void FindSlashN(char *, char *);
    ~XmStringArg() {}
};

// Icon creation and pixmap Id 
class PixmapArg : public WptArg {
public: 
    PixmapArg(VmToWptArg *entry, Symbol *sym);
    ~PixmapArg() {}
};

// Alignment Resource
class AlignArg : public WptArg {
public: 
    AlignArg(VmToWptArg *entry, Symbol *sym);
    ~AlignArg() {}
};

// KeySym Resource
class KeySymArg : public WptArg {
public: 
    KeySymArg(VmToWptArg *entry, Symbol *sym);
    ~KeySymArg() {}
};

// Accelerator Resource
class AccelArg : public WptArg {
protected:
TEXT accelString[STRINGSIZ+1];
public: 
    AccelArg(VmToWptArg *entry, Symbol *sym);
    ~AccelArg() {}
};

// Args provided by WPT as defaults (no user option)
class DefaultArg : public WptArg {
public: 
    DefaultArg(VmToWptArg *entry, Symbol *sym );
    ~DefaultArg() {}
};

/**********************************************************************/

extern WptArg* IntgConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* RealConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* StringConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* ViewValueConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* TitleConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* XmListConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* XmStringConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* XmStringQualConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* ColorConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* FontConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* FontListConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* SelectConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* CountConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* MaxCountConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* MinCountConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* ValCountConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* InitialConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* SizeConvert 
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* ValueConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* SingleValueConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* MultiValuesConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* PixmapConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* AlignConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* KeySymConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
extern WptArg* AccelConvert
	(VmToWptArg *e, Symbol *v, Symbol *t, Widget w, Screen *screen);
#endif

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//Include file for WPT item presentation type RADIO (RadioButtons)
// CHANGE LOG:
// 06-jul-89	Force Radio Buttons to be STRING only...krw
// 10-sep-89	VMS port...ljn
// 24-aug-90    added setstate and active...tpl
// 02-nov-90    SetState with index is now a real function...tpl
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 21-jan-92    Added DefaultTraversalState...tpl
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_RADIOBUTTON
#define I_RADIOBUTTON 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <Radio.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Radio.h>
#endif


// for RadioButtonA  class 
#define  RADIO_SENTINEL  0xFF1202FF	

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "radio"
class RadioP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==V_STRING;}
	int ValidsRequired ()
		{return TRUE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "radio";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class RadioA : public WptItem {
protected:
    int	 lastIndex;			// initial index to labels
    int  currentIndex;			// current index to labels

    void    Check() 
	{if (sentinel != RADIO_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
public:
    RadioA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~RadioA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int	 DisplayNewValue(int newTarget);	// update selection on display
    int	 UpdateView(Symbol *newView);	// update selection on display
    void SetLastValInTarget()
	{SetTargetValue(lastIndex); noLastValue = TRUE;}
    virtual int SetState (int newState)
	{return WptItem::SetState(newState);}
    virtual int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState)
	{ return WptItem::GetState(currentState);}
    Boolean DefaultTraversalState()
                                {return True;}
    };
#endif


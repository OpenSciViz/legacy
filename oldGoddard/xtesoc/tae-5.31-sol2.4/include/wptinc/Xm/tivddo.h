/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * tivddo.h
 *
 * This file defines the internal class objects for use within tiv.cc.
 *
 * The Classes are:
 *
 *     DDOWindow        - represents the InterViews Window of the DDO
 *
 *     DDOPicture       - represents one picture object from an idraw
 *                        picture file.
 *
 *     DDOAggregate     - used to contain a drawable collection of like objects
 *
 *     DDOObject        - a general base class for all subsequent DDOAreas
 *                        DDODynamics and DDOTransforms
 *
 *     DDOArea		- represents one high level DDO. In the widget side
 *			  of TAE Plus, this maps to a DDOArea widget. On the
 *			  Wpt side, this maps to one DDO item.
 *
 *     DDODynamic	- represents one Dynamic object in the DDO sense.
 *
 *     DDOTransform	- maps to one transformation type for a Dynamic
 *			  object.
 *
 *     DDOMover		- subclass of DDOTransform. Defines a transform
 *			  of type mover.
 *
 *     DDORotator	- subclass of DDOTransform. Defines a transform
 *			  of type rotator.
 *
 *     DDOStretcher	- subclass of DDOTransform. Defines a transform
 *			  of type stretcher.
 *
 *     DDODiscrete	- subclass of DDOTransform. Defines a transform
 *			  of type discrete.
 *
 *     DDOStripchart	- subclass of DDOTransform. Defines a transform
 *			  of type stripchart.
 *
 * Change Log:
 * 30-apr-93	Initial Creation...krw
 * 17-jul-93    Need to remember original maximum angle for rotators..krw
 * 23-jul-93    PR1964, PR1979: We can't have IV calling XDestroyWindow on the
 *              DDO's main window (Widgets don't like this). Defined Subclass
 *              of DDOWindow to support this...krw
 * 29-jul-93    PR2035: the WorkBench could not switch back/forth between
 *              stretcher and Discrete. Added origDynGraphic_ to 
 *              DDODiscrete so we could restore the dynamic...krw
 * 03-aug-93    PR2045: added displayCacheEntry_ to support multiple 
 *              consoles...krw
 * 07-aug-93    Added Stripchart...krw
 * 23-aug-93    Minor changes suggested by code review...krw
 * 25-aug-93    PR2199 added fields to DDODiscrete to support scaling of
 *              pictures to fit within the area defined by the dynamic...krw
 * 30-aug-93    PR2232 added name_ to DDODynamic...krw
 * 07-sep-93	PR2230 added DDOTransform::dynVisibilityChg and
 *		DDOArea::anyNamedPictures...krw
 * 11-sep-93	PR2153 To support DDOArea::update better we remember the
 *		titlePicture rather than the TextObj...krw
 * 24-sep-93	PR2339 Added DDOArea::naturalGeometry()...krw
 * 11-dec-93	Solaris port: include <sys/time.h>...dag
 * 25-jan-94	PR2595 added dirtyDynamic...krw
 * 04-apr-94	SCO port: include <sys/time.h>...swd
 * 06-apr-94	Included <Xtae/TIVconst.h> and ripped out Xtae #defines...dag
 * 08-jul-94	Intergraph port: include <sys/time.h>...dag
 * 15-mar-95    AT&T Port: include the proper time header...dgf
 */
#ifndef _TIVDDO_H
#define _TIVDDO_H

#include <InterViews/coord.h>
#include <InterViews/resource.h>
#include <InterViews/glyph.h>
#include <InterViews/window.h>
#include <InterViews/aggr.h>
#include <InterViews/display.h>
//#include <IV-X11/Xdefs.h>
#include "figure.h"
#include <Xtae/TIVconst.h>

// Get the proper Time header for Stripcharts
#ifdef VMS
#include <time.h>
#else
#ifdef macII
#include <sys/time.h>
#endif
#ifdef SYSV
#if defined(mipseb) || defined(sgi) || (defined(sun) && OSMajorVersion >=5) || defined(sco) || defined(__clipper__) || defined(__NCR)
#include <sys/time.h>
#else
#include <time.h>
#endif
#else
#if defined(MIPSEL) || defined(ultrix)
#include <time.h>
#else
#include <sys/time.h>
#endif
#endif
#endif


// from <X11/Intrinsic.h> Note: this must be before TIV.h
// use "char" for Boolean because that is the way Motif defines it.
typedef unsigned long      Pixel;
typedef char Boolean;			// used in Threshold lists
typedef void *XtAppContext;             // from Intrinsic.c

#include <Xtae/TIV.h>
// TEMPORARY ********************
//typedef void (*TIV_INPUTCALLBACK)(TIVHandle, XEvent *, TIVReal *, void *);
// TEMPORARY ********************

#include <wptinc/Xm/slist.h>

extern TIVStatus tivStatus;		// package internal global error status


// ***************************************************************************
// Macro for checking change masks

#define MASK(a) ((changeMask & a))


class DisplayEntry;
class DDOWindow;
class DDOPicture;
class DDOArea;
class DDODynamic;
class DDOTransform;
class DDOMover;
class DDOStretcher;
class DDORotator;
class DDODiscrete;
class DDOStripchart;

	// from InterViews
// class XEvent;
class Graphic;
class PolyGraphic;
class PolyGlyph;
class Transformer;
class Color;
class Background;


// ***************************************************************************
// D D O W i n d o w
// ***************************************************************************
class DDOWindow : public PopupWindow {
public:
    DDOWindow(Display *dpy, XWindow parent,
	      PixelCoord x, PixelCoord y,
	      PixelCoord width, PixelCoord height,
	      Glyph* glyph, const Color *fgColor, const Color *bgColor,
	      PixelCoord bw);
    ~DDOWindow();

    virtual void set_attributes();
					// new upper left coord
    virtual void moveWindow(PixelCoord left, PixelCoord top);

    virtual void preunbind();          // Fool Window::unbind()

    void setFgColor(const Color *fgColor);
    void setBgColor(const Color *bgColor);
    void setBorderWidth(PixelCoord bw);

protected:
friend DDOArea;				// let DDOArea have access to these
    const Color	*fgColor_;
    const Color	*bgColor_;
    PixelCoord	bw_;			// border width

private:
    XWindow	parent_;
};






// ***************************************************************************
// D D O O b j e c t
// ***************************************************************************
//	Base class. Used to give some commonality to all the DDO objects.
//	This makes it easier to do things like Destroy.
class DDOObject : public Resource {
public:
    DDOObject();
    virtual ~DDOObject();

    virtual void destroy();
    virtual int getPictureNames(char ***pictureNames);
protected:
    Graphic     *root_;                 // the root graphic for this object
};

inline void DDOObject::destroy() { Resource::unref(this);}


// ***************************************************************************
// D D O P i c t u r e
// ***************************************************************************
// There is one DDOPicture object corresponding to each first level
// picture to be used as a dynamic. 
class DDOPicture : public Resource {
public:
    DDOPicture(Graphic *g);
    ~DDOPicture();

    Graphic *graphic() const;	// the original graphic w/o transformers applied
    PolyGraphic *handle();	// original graphic with translation and scale	
    const String& pictureName();
    Transformer &xForm();	
    const Color *origColor();	// return the original color of the graphic

	// apply a new translation & scaling and store in baseXform
    void reScale(Coord dx, Coord dy, Coord sx, Coord sy);

    DDOPicture *clone();	// create new instance of this

private:
    Graphic	*graphic_;	// original Graphic comprising this picture
    const Color	*origColor_;	// original fg color of graphic
    Transformer baseXform_;	// transform with translation and scale applied
    PolyGraphic	*tSetter_;	// Holds the graphic_ and can
				// apply the baseXform_ to it.
				// Note: class TransformSetter does
				// NOT work here.
    DDOPicture	*original_;	// if I'm a clone, here is my original
    slist	myClones_;	// list of clones of me
};


// *************************************************************************
inline
const String& DDOPicture::pictureName() {
    return graphic_->GetName();
}

// *************************************************************************
inline
Graphic *DDOPicture::graphic() const {
    return graphic_;
}


// *************************************************************************
inline
Transformer &DDOPicture::xForm() {
    return baseXform_;
}

// *************************************************************************
inline
const Color *DDOPicture::origColor() {
    return origColor_;
}

// *************************************************************************
// Other classes grab me by my handle.
inline 
PolyGraphic *DDOPicture::handle() {
    return tSetter_;
}



// ***************************************************************************
// D D O A g g r e g a t e
// ***************************************************************************
class DDOAggregate : public Aggregate {
public:
    DDOAggregate(GlyphIndex size = 4);

    //override, original caused problems with dynamics disappearing
    virtual void draw(Canvas*, const Allocation&) const; 

    GlyphIndex index(const Glyph* compGlyph);
    virtual void remove(const Glyph* compGlyph);// remove given Glyph*
    virtual void remove(GlyphIndex);		// remove given index
};


// ***************************************************************************
// D D O A r e a
// ***************************************************************************
class DDOArea : public DDOObject {
public:
    DDOArea (XDisplay *d, const char *filename, 
	     const char *bgPictureName);
    ~DDOArea();
    TIVStatus preRealize(TIVPosition x, TIVPosition y,
		TIVDimension width, TIVDimension height, TIVDimension bw,
		Pixel fg, Pixel bg,
		const char *titlePictureName, const char *titleText);

    TIVStatus realize(XWindow w);

        // calculate natural size of the ddo, and the offset to apply to 
        // each picture. This is performed once at ctor time.

    void determineNatSize();

        // KLUDGE to handle V5.2 and earlier Discretes, see
        // DDODiscrete::getDiscretePictures

    void setNatDimensions(Coord x, Coord y, Coord w, Coord h);

	// natural width and height (in pixels) based on static picture size.
    void naturalSize (TIVDimension&, TIVDimension&);

        // natural geometry info for the background.
        // This is the set of values used for translating and scaling of the
        // dynamics.

    void naturalGeometry(Coord& x, Coord& y, Coord& natW, Coord& natH);

	// return the current size (in pixels)
    void pSize(PixelCoord &w, PixelCoord &h);

	// return the current size (in Coords)
    void size (Coord &w, Coord &h);

	// dimensions have changed so rescale all objects
    TIVStatus resize(TIVPosition x, TIVPosition y,
		     TIVDimension width, TIVDimension height, 
		     TIVDimension borderWidth);

    TIVStatus update(unsigned long changeMask,
		     TIVPosition x, TIVPosition y,
		     TIVDimension width, TIVDimension height,
		     TIVDimension borderWidth,
		     Pixel fg, Pixel bg, char *areaTitle);

    Display *display();			// return my display

    DDOWindow *displayWindow();	        // return the InterViews displayWindow
    XWindow displayXWindow();		// return the XWindow id of my window

    DDOPicture *findPicture(const char *name, GlyphIndex defaultIndex);

    void handleXEvent (XEvent *);

    TIVStatus exposure(XEvent *ex, Region region);	// from Redisplay

	// Finalize all dynamic and transform changes

    TIVStatus flushArea();

	// restore entire Background
    void restoreBg ();

	// Restore just the specific portion of the background
    void restoreBg (PixelCoord l, PixelCoord b, PixelCoord t, PixelCoord r);

	// Add a new DDODynamic to me
    void registerDynamic (DDODynamic *);

	// register a new DDOTransform with me
    void registerTransform (DDOTransform *);

	// Remove a DDODynamic from me
    void removeDynamic (DDODynamic *);

	// Remove a DDOTransform from me
    void removeTransform (DDOTransform *);

	// Tell the area that one or more min/max labels have changed
    void labelChanged(Extension &ex);	// ex is BB of damaged area

	// background picture...needed by some DDOTransforms
    DDOPicture *bgPicture();

	// Return the current scaling of the original background.
    void getBgScale (Coord &scaleX, Coord &scaleY);

	// Return a reasonable fallback index for a new discrete
    GlyphIndex getFallbackIndex ();

	// scale/translate all pictures relative to the bgPicture
    void reScale (PixelCoord newW, PixelCoord newH);

	// true if at least one picture in subpicts has a non-null name
    Boolean anyNamedPictures();

protected:
    void buildPictures();			// build all the DDOPictures

private:
    Display		*dpy_;		// display this DDOArea is on
    DisplayEntry        *displayCacheEntry_; //Display cache entry for this dpy
    GraphicMaster	*gm_;		// graphic for file read in
    DDOAggregate	*allFg_;	// Contains allDyns_ and valueLabels
    DDOAggregate	*allBg_;	// the background,title, min/max labels
    DDOAggregate	*allMinLabels_;	// all minLabel Pictures
    DDOAggregate	*allMaxLabels_;	// all maxLabel Pictures
    DDOAggregate	*allDyns_;	// all the Dynamic Pictures
    DDOAggregate	*allValueLabels_;// all valueLabel Pictures
    Background		*background_;	// background color object
    DDOPicture		*titlePicture_; // Picture for the title
    Graphic		*bgGraphic_;	// Graphic to use for the background
    GlyphIndex		subpictcnt_;	// count of names 
    DDOPicture		**subpicts_;	// list of immediate subpictures
    DDOWindow	        *displayWindow_;// my display window
    DDOWindow		*bgWindow_;	// bg window (offscreen)
    DDOPicture		*bgPicture_;	// picture for the background
    Coord		natW_, natH_;	// natural dimensions in Coords
    PixelCoord		bgpw_, bgph_;	// bg Natural w, h in pixels
    Coord		bgX_, bgY_;	// original origin of background
    Coord		sx_, sy_;	// current scale of background	
    TIVPosition		x_, y_;		// current window origin
    TIVDimension	w_, h_;		// current size in pixels
    TIVDimension	bw_;		// current border width
    const Color		*fgColor_;	// foreground (Window border) color
    const Color		*bgColor_;	// background fill color
    slist		dynList_;	// list of current DDODynamics
    DDODynamic          *activeDyn_;    // which Dynamic currently input active
    Boolean		namedPictures_;	// true if at least one pic has a name
};


// *************************************************************************
// return the display I am on
inline
Display *DDOArea::display() {return dpy_;}

// *************************************************************************
// return the InterViews Window id of my window
inline
DDOWindow *DDOArea::displayWindow() {
    return displayWindow_;
}


// *************************************************************************
// return the XWindow id of my window
inline
XWindow DDOArea::displayXWindow() {
    return (displayWindow_) ? displayWindow_->rep()->xwindow_ : (XWindow) nil;
}


// *************************************************************************
// return the bgPicture...some DDOTransforms may need this 
inline
DDOPicture *DDOArea::bgPicture() {
    return bgPicture_;
}


// *************************************************************************
// Return the current scaling of the original background.
inline
void DDOArea::getBgScale (Coord &scaleX, Coord &scaleY) {
    scaleX = sx_; scaleY = sy_;
}


// *************************************************************************
// natural geometry info for the background.
// This is the set of values used for translating and scaling of the
// dynamics.
inline
void DDOArea::naturalGeometry(Coord& x, Coord& y, Coord& natW, Coord& natH) {
    x = bgX_; y = bgY_;
    natW = natW_; natH = natH_;
    }

// *************************************************************************
// Return a reasonable fallback index for a new discrete
// The value is simply the number of currently defined dynamics plus 1.
inline
GlyphIndex DDOArea::getFallbackIndex () { return allDyns_->count() + 1;}


// *************************************************************************
// return the current size (in pixels)
inline
void DDOArea::pSize(PixelCoord &w, PixelCoord &h) {
	w = (PixelCoord) w_;
	h = (PixelCoord) h_;
}


// *************************************************************************
// return the current size (in Coords)
inline
void DDOArea::size (Coord &w, Coord &h) {
    w = dpy_->to_coord((PixelCoord) w_);
    h = dpy_->to_coord((PixelCoord) h_);
}

// *************************************************************************
// true if at least one picture in subpicts has a non-null name
//
inline
Boolean DDOArea::anyNamedPictures() {return namedPictures_;}


// ***************************************************************************
// D D O D y n a m i c
// ***************************************************************************
class DDODynamic : public DDOObject {
public:
    DDODynamic(DDOArea *parent, const char *name);
    ~DDODynamic();

    TIVStatus resetDynamic();		// reset totalXform to identity
    TIVStatus flushDynamic();		// apply all xforms and redisplay
    TIVStatus getGeometry(TIVGeom *geometry);
 
    void draw();			// draw this dynamic in the DDOArea
    void undraw();			// remove this dynamic from the DDOArea

    void map();				// draw and enable the display of this
    void unmap();			// remove and disable display of this

	// register a new DDOTransform with me
    void registerTransform (DDOTransform *);

	// apply a transformer to the totalXform_
    void applyTransformer (const Transformer&);

	// remove a DDOTransform from me
    void removeTransform (DDOTransform *);

        // true if dynamic is currently visible (mapped)
    boolean mapped();

        // true if user dynamic input is enabled
    boolean inputEnabled();

        // define the types of input for this dynamic
    void defineInput(boolean state);

        // Register an input callback with this dynamic
    void RegisterInputCallback (unsigned long callbackTypeMask,
				TIV_INPUTCALLBACK callbackFunc,
				void *client_data );

	// enable drawing of the dynamic

    void enableDisplay();

	// disable drawing of the dynamic

    void disableDisplay();

	// Set the threshold foreground color.

    void setFGColor(Pixel color, boolean useDefault);

	// Get the DDOArea we are in

    DDOArea *parent();

	// get the Coord bounds of the dynamic
    void getBounds(Coord &l, Coord &b, Coord &r, Coord &t);

	// the parent DDOArea has resize, so I must tell my transformers
    void areaResized();

	// Change theDynamic_'s Picture...really support for discretes
    void changePicture (DDOPicture *newPicture);

    // Handle buttonPress events on me
    void pressEvent (Event &event);

    // Handle buttonRelease events on me
    void releaseEvent (Event &event);

    // Handle motion events on me
    void motionEvent (Event &event);

    // Signal that this dynamic should be declared damaged and should
    // be redrawn
    void dirtyDynamic();

    DDOPicture	*dynPicture_;		// idraw picture this dynamic based on
    PolyGraphic	*theDynamic_;		// combines the totalXform_ with
					// the dynamic picture 

    NullTerminatedString name_;         // name used for this dynamic
    NullTerminatedString suffix_;	// pictureName suffix
    Coord	l_, b_, r_, t_;		// Current Dynamic bounds

protected:
    TIVReal *createValueArray();        // return list of values from xforms

friend DDOArea;
friend DDOTransform;

private:
    void getPictureNameSuffix();	// get the integer suffix from name_

    DDOArea	*parent_;		// parent DDOArea
    slist	xFormList_;		// list of DDOTransforms for this dyn
    Transformer	totalXform_;		// total transformation matrix. made
					// from sequentially applying xforms
					// in xFormList_ to the identity matrix
    Transformer lastXform_;		// previous transformer
    boolean	pictureChanged_;	// was picture changed (via discrete)?
    boolean	mapped_;		// true if dynamic is visible (mapped)

        // The following variables support user input
    boolean	inputEnabled_;		// true if dynamic will accept input
    TIV_INPUTCALLBACK pressCallback;
    TIV_INPUTCALLBACK releaseCallback;
    TIV_INPUTCALLBACK motionCallback;
    void        *pressClientData;
    void        *releaseClientData;
    void        *motionClientData;
};


// ***************************************************************************
// What is the current visibility state of the dynamic?
inline
boolean DDODynamic::mapped() {return mapped_;}

// ***************************************************************************
// Does this dynamic accept user input?
inline
boolean DDODynamic::inputEnabled() {return inputEnabled_;}

// ***************************************************************************
// Define the current input state for this dynamic
inline
void DDODynamic::defineInput(boolean state)  {inputEnabled_ = state;}

// ***************************************************************************
// Get the DDOArea we are in
inline
DDOArea *DDODynamic::parent() { return parent_;}


// ***************************************************************************
// Get the Coord bounds of the dynamic
inline
void DDODynamic::getBounds(Coord &l, Coord &b, Coord &r, Coord &t) {
    l = l_; b = b_; r = r_; t = t_; 
    }

// ***************************************************************************
// Signal that this dynamic should be declared damaged and should be redrawn
inline
void DDODynamic::dirtyDynamic() { pictureChanged_ = true; }


// ***************************************************************************
// D D O T r a n s f o r m
// ***************************************************************************
class DDOTransform : public DDOObject {
public:
    DDOTransform (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);
    virtual ~DDOTransform();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

    DDODynamic *parent();
    const Transformer &xForm();
    TIVReal value();                    // return current displayed value

        // return a value normalized between the minimum and maximum
    float normalizeValue(TIVReal value);

        // inverse of normalizeValue...return application value
    TIVReal applicationValue(float normVal);

    DDOPicture *getOneAuxPicture(const char *baseName, boolean textOnly);
    virtual void updateValueDisplay(TIVReal newValue);
    virtual void doThreshold(); 	// based on currentValue

    virtual TIVStatus doTransform(TIVReal newValue);
    virtual TIVStatus calculateTransform (TIVReal newValue);
    virtual void getAuxPictures();
    virtual int transformType();
    virtual void areaResized();

    // The dynamic is notifying me of a visibility change
    virtual void dynVisibilityChg(Boolean dynVisible);

    // Handle buttonPress events
    virtual void pressEvent (Event &event);

    // Handle buttonRelease events
    virtual void releaseEvent (Event &event);

    // Handle motion events on me
    virtual void motionEvent (Event &event);

protected:
friend DDOArea;

    DDODynamic	*dynamic_;		// parent DDODynamic
    TIVReal	currentValue_;		// current value displayed
    TIVReal	lastXformedValue_;	// last value used for transformation
    TIVReal	delta_;			// update delta;
    TIVReal	inputDelta_;		// input update delta;
    TIVReal	minValue_, maxValue_;	// minimum and maximum values
    TIVReal	*thresholdValue_;	// array of threshold values
    int		thresholdCnt_;		// count of thresholds
    Pixel	*thresholdColor_;	// threshold Colors (except Discrete)
    Boolean	*useDefaultColor_;	// true means use defaultColor
    DDOPicture	*valuePicture_;		// picture used to show current value
    DDOPicture	*minPicture_;		// minLabel picture
    DDOPicture	*maxPicture_;		// maxLabel picture
    CopyString	valueFormat_;		// "C" format to use for value display
    CopyString	minFormat_;		// format of min value display
    CopyString	maxFormat_;		// format of max value display
    Transformer	xForm_;			// current transformer matrix 
    boolean	valueEnabled_;		// true if value display is enabled

    // The following values are here to support DDO Input
    Coord       normalValue_;           // last normalized value used on
                                        // in a doTransform
    Coord       lastNormalValue_;       // normalized value at lastCoord_
                                        // Typically saved at buttonPress time

private:
	// Change the text of one of the text object pictures
    void updateTextPicture(DDOPicture *picture, const String &format, 
			   TIVReal value, Extension &damagedArea);
};

// ***************************************************************************
// What DDODynamic am I registered with?
inline
DDODynamic *DDOTransform::parent() { return dynamic_;}

// ***************************************************************************
// What is my transform?
inline
const Transformer &DDOTransform::xForm() { return xForm_;}

// ***************************************************************************
// What is my value?
inline
TIVReal DDOTransform::value() { return currentValue_;}


// ***************************************************************************
// What type of DDOTransform am I?
inline
int DDOTransform::transformType() {return TIVNULL_TRANSFORM;}

// ***************************************************************************
// Common normalization calculation. Typically used in calculateTransform.
inline
float DDOTransform::normalizeValue(TIVReal value) {
    return (Math::min(Math::max(value, minValue_), maxValue_) - minValue_) / 
		(maxValue_ - minValue_);
}


// ***************************************************************************
// Inverse of normalizeValue. This routine is typically called by the 
// motionEvent methods to determine a new current value in application units.
// The input parameter is a normalizedValue.
inline
TIVReal DDOTransform::applicationValue(float normVal) {
    return (Math::max( Math::min(
	(TIVReal)normVal * (maxValue_ - minValue_) + minValue_, maxValue_),
		      minValue_));
}


// ***************************************************************************
// D D O M o v e r
// ***************************************************************************
class DDOMover : public DDOTransform {
public:
    DDOMover (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);
    virtual ~DDOMover();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

    virtual TIVStatus calculateTransform (TIVReal newValue);
    virtual void getAuxPictures();
    virtual int transformType();
    virtual void areaResized();

    // Handle buttonPress events
    virtual void pressEvent (Event &event);

    // Handle buttonRelease events
    virtual void releaseEvent (Event &event);

    // Handle motion events on me
    virtual void motionEvent (Event &event);

private:
    DDOPicture	*stopPicture_;		// stop  picture (if any)
    int		direction_;
    Coord	naturalRange_;		// natural range without scaling
    Coord	fullRange_;       // natural range with DDOArea scaling applied

    // added to support input
    Coord       lastCoord_;             // last Input coord in proper direction

    Coord calculateNaturalRange();// calculate the natural range of the mover
    Coord calculateFullRange();	// calculate the fullRange for data mapping
}; 


// ***************************************************************************
inline
DDOMover::transformType() { return TIVMOVE_TRANSFORM;}



// ***************************************************************************
// D D O R o t a t o r
// ***************************************************************************
class DDORotator : public DDOTransform {
public:
    DDORotator (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);

    virtual ~DDORotator();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

    virtual TIVStatus calculateTransform (TIVReal newValue);
    virtual void getAuxPictures();
    virtual int transformType();
    virtual void areaResized();

    // Handle buttonPress events
    virtual void pressEvent (Event &event);

    // Handle buttonRelease events
    virtual void releaseEvent (Event &event);

    // Handle motion events on me
    virtual void motionEvent (Event &event);

private:
        // All angles below are in degrees in the Cartesian coordinate system

    DDOPicture	*startPicture_;		// start picture (if any)
    DDOPicture	*stopPicture_;		// stop  picture (if any)
    DDOPicture	*pivotPicture_;		// pivot  picture (if any)
    int		direction_;
    int		pivotPoint_;
    Coord	myL_, myB_, myR_, myT_;	// my dynamic's bounds
    Coord	myCx_, myCy_;		// my dynamic's center
    Coord	origMaxAngle_;	        // orig maximum angle from application
    Coord	maximumAngle_;		// maximum total angle of rotation

    void  determinePivotPt();		// determine the pivot point(what else?)
    void  calculateMaxAngle();		// calculate maximum angle of rotation

	// calculate angle from x-axis to pt (x,y) with (px, py) as origin
    Coord calculateOneAngle(Coord x, Coord y, Coord px, Coord py);

    Coord xPivot_, yPivot_;        	// rotation pivot

        // The following instance variables support DDO input
    Coord minAngle_;                    // angle to min (start) point
    Coord maxAngle_;                    // angle to max (stop) point
    Coord curAngle_;                    // current angle of rotation
    Coord lastAngle_;                   // angle defined by lastX_, lastY_
    Coord lastX_, lastY_;

}; 

// ***************************************************************************
inline
DDORotator::transformType() { return TIVROTATE_TRANSFORM;}


// ***************************************************************************
// D D O S t r e t c h e r
// ***************************************************************************
class DDOStretcher : public DDOTransform {
public:
    DDOStretcher (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);
    virtual ~DDOStretcher();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

    virtual TIVStatus calculateTransform (TIVReal newValue);
    virtual void getAuxPictures();
    virtual int transformType();
    virtual void areaResized();

    // Handle buttonPress events
    virtual void pressEvent (Event &event);

    // Handle buttonRelease events
    virtual void releaseEvent (Event &event);

    // Handle motion events on me
    virtual void motionEvent (Event &event);

private:
    int		direction_;

    // The following values are here to support DDO Input
    Coord       fullRange_;             // The full scaled size of the dynamic
                                        // in the proper direction
    Coord       lastCoord_;             // last Input coord in proper direction
}; 

// ***************************************************************************
inline
DDOStretcher::transformType() { return TIVSTRETCH_TRANSFORM;}



// ***************************************************************************
// D D O D i s c r e t e
// ***************************************************************************
class DDODiscrete : public DDOTransform {
public:
    DDODiscrete (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);
    virtual ~DDODiscrete();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

	// change picture based on value. This method does the real work
    virtual void doThreshold();

    virtual TIVStatus calculateTransform (TIVReal newValue);
    virtual void getDiscretePictures(TIVDiscretePixStruct &discrete);
    virtual void scalePictures ();  // scale the discrete pictures if necessary
    virtual int transformType();
    virtual void areaResized();

    // The dynamic is notifying me of a visibility change
    virtual void dynVisibilityChg(Boolean dynVisible);

private:
    DDOPicture	**thresholdPicts_;	// list of threshold pictures
					// there are thresholdCnt_ of them
    GlyphIndex	currentIndex_;		// current picture displayed
    Graphic	*origDynGraphic_;	// dynamic's original picture
    boolean	staticBG_;              // true if there is a static BG picture
    Coord	maxW_, maxH_;	       // max width/height of all discrete pics
    Coord	scaleX_, scaleY_;	// scaling for v5.3 discrete picts.
    Boolean	missingPic_;		// true if current picture is missing
}; 


// ***************************************************************************
inline
DDODiscrete::transformType() { return TIVDISCRETE_PIX_TRANSFORM;}


// ***************************************************************************
// D D O S t r i p c h a r t
// ***************************************************************************
class StripchartTimer;

class DDOStripchart : public DDOTransform {
public:
    DDOStripchart (DDODynamic *ddoDynammic, TIVReal currentValue,
		  TIVReal updateDelta, TIVReal minimum, TIVReal maximum,
		  int valueDisplayEnable, char * valueDisplayFormat,
		  int transformtype, TIVTransformStruct *transformStruct);
    virtual ~DDOStripchart();

    virtual TIVStatus updateTransform(unsigned long changeMask,
		TIVReal currentValue, TIVReal updateDelta, 
		TIVReal minimum, TIVReal maximum, 
		int valueDisplayEnable, char *valueDisplayFormat,
		TIVTransformStruct *transformStruct);

    void initializeLines();             // setup the lineSegments_
    void addToTimer();                  // Add this Stripchart to a timer

    virtual void doThreshold();         // change color of individual lines

    virtual TIVStatus calculateTransform (TIVReal newValue);

    // override doTransform...need special handling for on demand
    virtual TIVStatus doTransform(TIVReal newValue);

                 // scroll the graph one seg and plot next segment
    void graphUpdate();

    virtual int transformType();
    virtual void areaResized();

       // set the current segment's color
    void setFGColor(Pixel color, boolean useDefault); 

private:

    int		 maxSegments_;		// max number of segents to draw
    unsigned int interval_;             // update interval
    XtAppContext appContext_;           // Xt application context
    StripchartTimer *timer_;            // the timer I'm attached to
    DDOPicture   *scPicture_;           // The stripchart picture to display
    DDOPicture   *origPicture_;         // original dynamic picture
    Graphic      *origDynGraphic_;      // original dynamic graphic.
    Coord        origL_, origB_, origR_, origT_; // original dynamic dimensions
    Coord        origWidth_, origHeight_; // original dynamic size
    PolyGraphic  *lineSegments_;        // all stripchart line segments
    boolean      firstValue_;           // indicates this graphUpdate is for
                                        // the first data value
#ifdef VMS
    timeb_t lastTime;
#else
    struct timeval lastTime;	        //Time of last update
#endif
}; 

// ***************************************************************************
inline
DDOStripchart::transformType() { return TIVSTRIPCHART_TRANSFORM;}

//_TIVDDO_H 
#endif 

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type PULLDOWN
// CHANGE LOG:
// 06-jul-89	Force STRING data type only...krw
// 10-sep-89	VMS port...ljn
// 24-aug-90    added setstate and active...tpl
// 02-nov-90    added getstate ...tpl
// 12-nov-90    added DefaultShadowThickness...tpl
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 21-jan-92	Added DefaultTraversalState...tpl
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_PULLDOWN
#define I_PULLDOWN 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <Pulldown.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Pulldown.h>
#endif

// For PulldownA class
#define  PULLDOWN_SENTINEL  0xFF120AFF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "pulldown"
class PulldownP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==V_STRING;}
	int ValidsRequired ()
		{return TRUE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "pulldown";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class PulldownA : public WptItem {
protected:
    int	 lastIndex;			// initial index to labels
    int  currentIndex;			// current index to labels

    void    Check() 
	{if (sentinel != PULLDOWN_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);


public:
    PulldownA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~PulldownA();
    void ModeSetup(int mode);	// special setup in each mode 

    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int  DisplayNewValue(int newTarget);
    void SetLastValInTarget()
	{SetTargetValue(lastIndex);  noLastValue = TRUE; }
    virtual int SetState (int newState)
	{return WptItem::SetState(newState);}
    virtual int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState)
	{ return WptItem::GetState(currentState);}
    int DefaultShadowThickness() { return 2; }
    int DefaultBorderWidth() { return 1; }
    Boolean DefaultTraversalState()
                                {return True;}

    UpdateView(Symbol *newView);

    };
#endif


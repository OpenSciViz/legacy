/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/**************************************************************************
 *
 *	Include file for Wpt provided Help panel class.
 *
 **************************************************************************/

#ifndef I_WPTHELP
#define I_WPTHELP	0

#include   <widgetagent.h>

class WptHelpPanel;


class WptHelpPanel {
protected: 
	WidgetAgent	*helpObject;	// object for which help is requested
        WptPanel    	*requestPanel;	// panel from which help is requested
    	WptPanel    	*helpPanelId;	// panel displaying help info
	SymbolTableList *helpPanelTables;    // help panel resource tables
        SymbolTable 	*helpTarget;	// target for help panel;
        SymbolTable	*helpView;	// view block for help panel

        int BuildSymbolTables(WptPanel *reqPanel,  WidgetAgent *helpObj,
		SymbolTable **targetSt, SymbolTable **viewSt);
	int ReadHelpInfo(TEXT *name, SymbolTable *viewSt,
		TEXT  *helpSpec, Boolean panelHelp);

public:
    WptHelpPanel(WptPanel *requestPanel, WidgetAgent *helpObject,
			Boolean panelHelp, TEXT *helpSpec);  
    ~WptHelpPanel() ;
    WptPanel *HelpPanelId() {return helpPanelId;}
};

#endif


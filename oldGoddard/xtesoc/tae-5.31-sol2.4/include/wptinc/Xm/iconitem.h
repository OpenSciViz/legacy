/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Header file for WPT presentation type ICON.

//CHANGE LOG:
// 25-sep-89	Fixed workbench icon rotation (needed TAEFLOAT)...krw
// 13-nov-90    Added DefaultShadowThickness...tpl
// 16-nov-90    Added DefaultItemLook...tpl
// 22-mar-91	Removed DefaultShadowThickness pr897...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 21-jan-92    Added DefaultTraversalState...tpl
// 22-feb-92	New arg to DefaultItemLook() (for items with components)...ljn
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_ICON
#define I_ICON 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <Icon.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Icon.h>
#endif

// for Icon widget agents
#define ICON_SENTINEL  0xFF1206FF	
// for Icon help agents
#define HELP_ICON_SENTINEL  0xFF1206FF	

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "icon"
class IconP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "icon";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);

	WidgetAgent * MakeHelpAgent(TEXT * name, WptPanel* panelId,
                        Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a iconWidget (as a WptItem)

class IconA : public WptItem {
protected:
    void    Check() 
	{if (sentinel != ICON_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int bitHeight;
    int bitWidth;
    char *bitPattern;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    int origshadow;
    int origdefaultshadow;
public:
    IconA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~IconA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    UpdateView(Symbol *newView);
    void RotateBitmap(char *bits, int width, int height, 
                         TAEFLOAT angle, int sameSize, 
                         char **newBits, int *newWidth, int *newHeight);
    virtual int  SetState(int state)
	{return WptItem::SetState(state);}
    virtual int SetState (int index, int newState)
                {index=index; newState=newState; return 1;}
    int DefaultItemLook (int state, TEXT *component);
    Boolean DefaultTraversalState()
                                {return True;}
    };



class HelpIconA : public IconA {
protected:
    void    Check()
        {if (sentinel != HELP_ICON_SENTINEL) abort(); }
public:
    HelpIconA (TEXT * name, WptPanel *panelId, Symbol* targetSym,
                        Symbol *viewSym);
    ~HelpIconA();

    // Use override for subclasses of Icons (help icons)
    void SetAppCallback(TEXT* callbackType, XtCallbackProc 
		appCallbackProc, void * appCallbackData) 
    	{callbackType=callbackType; appCallbackProc=appCallbackProc;
	 appCallbackData=appCallbackData; }
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData) 
	{widgetId=widgetId; callbackType=callbackType; callData=callData; }
    };

#endif

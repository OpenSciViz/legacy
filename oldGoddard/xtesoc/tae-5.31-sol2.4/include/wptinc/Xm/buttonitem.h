/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for a WPT item presentation type BUTTON 
//
// 13-nov-90 	Added DefaultShadowThickness...tpl
// 14-nov-90 	Removed DefaultBorderWidth...tpl
// 18-nov-90 	Removed SetState...tpl
// 28-mar-91 	VMS port...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 21-jan-92  	Added DefaultTraversalState...tpl
// 22-feb-92	New arg to DefaultItemLook() (for items w/ components)...ljn
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_ACTIONBUTTON
#define I_ACTIONBUTTON 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <PushB.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/PushB.h>
#endif

// For ButtonA class
#define  BUTTON_SENTINEL  0xFF1201FF		
// For HelpButtonA class
#define  HELP_BUTTON_SENTINEL  0xFF12011F	

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "button"
class ButtonP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "button";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);

	WidgetAgent * MakeHelpAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class ButtonA : public WptItem {
protected:

    void    Check() 
	{if (sentinel != BUTTON_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    int DefaultArgs(Widget, XtkArg **);
    int origshadow;
    int origdefaultshadow;

public:
    ButtonA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~ButtonA();   

    // Use Use override for subclasses of buttons (help buttons)
    virtual void ButtonSelect(void *callData);	
    virtual void SetAppCallback(TEXT* callbackType, XtCallbackProc 
		appCallbackProc, void * appCallbackData);
    virtual void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    virtual int  SetState(int state)
	{return WptItem::SetState(state);}
    virtual int SetState (int index, int newState)
		{index=index; newState=newState; return 1;}
    virtual int DefaultShadowThickness () { return 2; }
    virtual int DefaultItemLook (int state, TEXT *component);
    virtual Boolean DefaultTraversalState()
                                {return True;};
    };




class HelpButtonA : public ButtonA {
protected:
    void    Check() 
	{if (sentinel != HELP_BUTTON_SENTINEL) abort(); }
public:
    HelpButtonA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
			Symbol *viewSym); 
    ~HelpButtonA();   

    // Use override for subclasses of buttons (help buttons)
    void SetAppCallback(TEXT* callbackType, XtCallbackProc 
		appCallbackProc, void * appCallbackData) 
    	{callbackType=callbackType; appCallbackProc=appCallbackProc;
	 appCallbackData=appCallbackData; }
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData) 
	{widgetId=widgetId; callbackType=callbackType; callData=callData; }
    };
#endif


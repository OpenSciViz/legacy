/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


#ifndef idraw_h
#define idraw_h

#include <InterViews/enter-scope.h>
#include <OS/enter-scope.h>

class InputFile;
class GraphicMaster;

class IdrawReader {
public:
//    static GraphicMaster* load(InputFile*, Display* dpy);
    static GraphicMaster* load(const char*, Display* dpy);
};
#endif

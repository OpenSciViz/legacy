/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//	Symbol manipulation.

//	Change Log:

//	11-jul-88	Make SetSize external....palm
//	10-nov-88	Comments on which NOT to use.
//			New iterator macros.
//			Allow integer/real to be fetched without error.	
//	18-nov-88	New Merge function for SymbolTables...palm
//	21-nov-88	New symbol ctor...palm
//	22-nov-88	New saveDebug members...palm
//	22-nov-88	New Set (Symbol *) for SymbolTables...palm
//	23-feb-89	Added indexed String, Real, Intg...krw
//	13-mar-89	Vm_ routines return CODE not void...krw
//      14-dec-89       fixed lazy prototype stuff(...)...tpl
//	24-jan-90	Add 'extern "C"' for g++ 1.36 port...ljn
//	02-feb-89	Vm_Alloc was missing strsiz...krw
//	09-feb-90	DeleteIf() added to SymbolTable...ljn
//	12-feb-90	Added Vm_FreeCA()...ljn
//	14-sep-90	Added Vm_GetValidString/Real/Intg()...rt
//	18-oct-90	Fixed return type of s_bcopy...cew
//      06-feb-91	tae_free() decl removed; defined in declare.h...ljn
//      08-may-91       PR869 change setvalid to return code...tpl
//      09-may-91       PR869 changes part 2...tpl
//	02-jan-92	PR1257: Go to #include <>...ljn
//	24-feb-92	Removed saveDebug no one used it...krw
//      03-apr-92	Used const TEXT as much as possible...tpl
//	30-apr-92	Added Symbol::Remove to delete a qualifier from
//			a symbol. 
//			Added Symbol::Set(qualName, Symbol*).
//			Added SymbolTable::Insert()
//			Added Symbol::Insert().
//			Added Symbol::Replace() and SymbolTable::Replace...krw
//      11-may-92       Used const TEXT for SymbolTabel []...tpl
//      14-may-92       Used const TEXT ** for Vm_SetValidString, Vm_SetString,
//                      and SetValid.  Removed x_error declaration  (more Sun
//                      C++ changes)...tpl/rt
//	09-mar-93	PR1855: Added Vm_FreeVarCA to support Symbol dtor..krw
//			
#ifndef I_SYMBOL
#define I_SYMBOL 0

#include	<taeconf.inp>
#include	<symtab.inc>
#include	<parblk.inc>
#include	<vminc.inc>

#ifndef nil
#define nil 	NULL
#endif

class Symbol;				// defined below
class SymbolTable;			// defined below

typedef CODE (*SymbolCallback) (Symbol *s, void *context);
typedef CODE (*StCallback) 
		(SymbolTable  *st,  void *context,  const TEXT *name,  int type);


extern "C"
{
Id	Vm_New (FUNINT);
void	Vm_FreeCA (Id);
void	Vm_NewCA (Id, FUNINT);
void	Vm_Free(Id);
VARIABLE *Vm_Find (Id, const TEXT *);
VARIABLE *Vm_FindVar (Id, const TEXT *);
CODE Vm_ReadFromDisk (Id, const TEXT *);
CODE Vm_WriteToDisk (Id, const TEXT *);
CODE Vm_ForEach (Id, SymbolCallback, void *context); 
CODE Vm_SetIntg (Id, const TEXT * name, int count, TAEINT *intg, int mode);
CODE Vm_SetReal (Id, const TEXT *name, int count, TAEFLOAT *real, int mode);
CODE Vm_SetString (Id, const TEXT *name, int count, const TEXT **vv, int mode);
CODE Vm_SetValidIntg (Id, const TEXT *name, int count, TAEINT *low, TAEINT *high);
CODE Vm_SetValidReal (Id, const TEXT *name, int count, TAEFLOAT *low, TAEFLOAT *high);
CODE Vm_SetValidString (Id, const TEXT *name, int count, const TEXT **vv);
CODE Vm_GetValidString (Id, const TEXT *name, int *count, TEXT ***vv);
CODE Vm_GetValidIntg (Id, const TEXT *name, FUNINT *count, TAEINT **low,
		      TAEINT **high);
CODE Vm_GetValidReal (Id, const TEXT *name, FUNINT *count, TAEFLOAT **low,
		      TAEFLOAT **high);
CODE Vm_CopyVarToVm (Id, VARIABLE *vin);
CODE Vm_MoveSymtab(SYMTAB *st1, SYMTAB *st2);
CODE Vm_FreeVar(SYMTAB *head, VARIABLE *v);
CODE Vm_FreeVarCA(SYMTAB *head, VARIABLE *v);
CODE Vm_FreeValue (GENPTR vv, int type, int count);
Symbol *Vm_AllocVar(SYMTAB *head);
GENPTR Vm_AllocValue (VARIABLE *);
CODE Vm_CopyVar (VARIABLE *vin, VARIABLE *vout);
Symbol *Vm_Alloc (SYMTAB *head, const TEXT *name, int type, int count, int strsiz);
CODE Vm_CopyValue (int type, GENPTR in, GENPTR out, int count);

COUNT s_bcopy (const TEXT *from, TEXT *to, int maxChars);

Id      Co_New( CODE );
VOID    Co_Add( Id, SymbolTable *, const TEXT *, CODE);
VOID    Co_Free( Id ...  );
VOID    Co_Free_Nocall ( Id );
BOOL    Co_Empty ( Id );
COUNT   Co_Count(Id);
Id     *Co_IdVector ( Id );
TEXT  **Co_NameVector ( Id );
Id      Co_Remove ( Id,  const TEXT * );
SymbolTable *Co_Find ( Id, const TEXT * );
CODE    Co_ReadFile (Id, const TEXT *, CODE);
CODE    Co_WriteFile(Id, const TEXT *);
CODE    Co_ForEach (Id c, StCallback f, void *context);

}

/*********************************

User Guide:

A Symbol is a TCL variable.  A SymbolTable
is a Vm object, i.e., a collection of 
Symbols.  A SymbolTable on disk is a TAE
standard PAR file as written by the TCL
SAVE command or the TUTOR SAVE command.
A SymbolTableList is a collection of Symbol
Tables.  A SymbolTableList on disk is
(a) one file, the concatenation of several
PAR files, or (b) a UNIX archive of PAR files.

To access a Symbol in a SymbolTable,
do something like:
	
    printf ("value of x is %d\n", (*st)["x"].Intg());

but beware that the above crashes if "x" 
does not exist in st.   To check existence, do:

    Symbol *s = (*st).GetSymbol("x");
    if (s)
        printf ("value of x is %d\n", (*s).Intg());
	
Sometimes references to Symbols make for better
reading:

    Symbol &s = (*st)["x"];
    if (&s)
	printf ("value of x is %d\n", s.Intg());

You may reference a numeric value as either Real
or Intg and the functions do a conversion for you.
To Set the value of something, your type must be
correct, however.

The Set functions for SymbolTable will create the
variable for you if it doesn't exist.  Likewise,
the Set functions for qualifiers will create the
variable for you.  The Set functions for Symbols,
of course, do not create for you (because an
object must exist if you want to operate on it).

Many functions below are marked "DO NOT USE" because
they are not well-designed.  They are retained
here because some existing code references them.

To stream over elements in a collection, see
the ForEach macros below.

For examples, of all of this, see the test case
that is conditionalized in the symbol.c module.

*****************************/


//	A Symbol is an agent for the traditional TAE VARIABLE
//	structure.  

extern "C" VARIABLE *p_lookup (struct SYMTAB *, const TEXT *);


class Symbol {
	VARIABLE v;

public:
	Symbol ();
	Symbol (const char *name, int type, int minc=1, int maxc=1);
	~Symbol ();
	int  Count () 
		{return v.v_count;}
 	int  Type () 
		{return v.v_type;}
	TEXT *Name () 
		{return v.v_name;}
 	int Nullable () 
		{return v.v_nullable;}
        int Event() 
		{return v.v_event;}
	int MaxCount ()
		{return v.v_maxc;}
	int MinCount ()
		{return v.v_minc;}
	int Size ()
		{return v.v_size;}
	GENPTR Value ()				// DO NOT USE
		{return v.v_cvp;}

	//  do not use ForEach.  The ForEachSymbol macro is much cleaner
	CODE ForEach (SymbolCallback f, void *context);	// for each qualifier

	Symbol **PointerVector ();		// used by ForEachSymbol macro
	void ReType (int type);
	void SetMaxCount (int maxc);
	void SetMinCount (int minc);
	void SetSize (int size) ;
	void SetNullable (int nullable) ;
	void SetName (const TEXT *newName)
	   {s_bcopy (newName, v.v_name, NAMESIZ);}
	void SetEvent (int state) {v.v_event = state;}
	int Valids ();					// number of valids 
	TEXT *Valid (int index)				// get nth valid 
	     {return (*(S_VALID *)v.v_valid).slist[index].string;}
	void Valid (int index, TAEINT &low, TAEINT &high)
	     {low = (*(I_VALID *)v.v_valid).range[index].low;
	     high = (*(I_VALID *)v.v_valid).range[index].high;}
	void Valid (int index, TAEFLOAT &low, TAEFLOAT &high)
	     {low = (*(R_VALID *)v.v_valid).range[index].low;
	     high = (*(R_VALID *)v.v_valid).range[index].high;}
        CODE SetValid (int count, int low[], int high[]);
        CODE SetValid (int count, TAEFLOAT low[], TAEFLOAT high[]);
        CODE SetValid (int count, const TEXT **valid);
        void GetValid (FUNINT *count, TAEINT *low[], TAEINT *high[]);
        void GetValid (FUNINT *count, TAEFLOAT *low[], TAEFLOAT *high[]);
        void GetValid (FUNINT *count, TEXT ***valid);
	Symbol *Qualifier (const TEXT *name) 
	    {return (Symbol *)p_lookup(&v.v_qualst, name);}
  	Symbol & operator[] (const TEXT *name)
	    {return *Qualifier(name);}
	Symbol * Clone (const TEXT *newName = nil);
        void Remove (Symbol *);			// remove a qualifier
        void Remove (TEXT *name) {Remove (Qualifier(name));}
	void Insert (Symbol *sym, Symbol *relSym=NULL); // insert a qualifier
	void Insert (Symbol *sym, TEXT *relName=NULL); // insert a qualifier
	void InsertAfter (Symbol *newSym);	// insert newSym after this sym
	void Replace (Symbol *oldQual, Symbol *newQual);// replace a qualifier
	void Replace (TEXT *oldQual, Symbol *newQual)
		{Replace (Qualifier(oldQual), newQual);}

	void SetNoValue ();
	void SetNullValue ();
	int CountOk (int count);		// is count compatible?
	void Set (Symbol *source);		// copy value

	//  Set for scalars

	void Set (const TEXT *string);
	void Set (TAEINT intg);
	void Set (TAEFLOAT real);

	//  Set for vectors

	void Set (int count, const TEXT **svv);
	void Set (int count, TAEINT *ivv);
	void Set (int count, TAEFLOAT *fvv);

	//   Set one element of a vector

        void SetElement (const TEXT *string, int index); 
        void SetElement (TAEINT value, int index);
        void SetElement (TAEFLOAT value, int index);

    //		Scalar Values
    //		As a convenience, we allow that a Real may
    //		be fetched as an Intg and vice versa.  This
    //		freedom is only available on scalar fetches.

    TEXT *     String ()  ; 
    TAEFLOAT   Real ()    ;
    TAEINT     Intg ()    ;

    //		Specificly index value of value vector

    TEXT *     String (int index) {return SVAL(v,index);} 
    TAEFLOAT   Real (int index)   {return RVAL(v,index);} 
    TAEINT     Intg (int index)   {return IVAL(v,index);} 

    //		Vector Values
    //		BEWARE: no nice conversions as for scalars !!

    TEXT **    StringVec ()   {return &SVAL(v,0);}
    TAEFLOAT * RealVec ()     {return &RVAL(v,0);} 
    TAEINT   * IntgVec ()     {return &IVAL(v,0);} 


//	Qualifiers:	
//	Note that if the qualifier does not exist,
//	it is created.

	//  Qualifier Set from another Symbol

    void Set (const TEXT *qualifier, Symbol *symbol);

	//  Qualifier Set for scalars

    void Set (const TEXT *qualifier, TEXT *string);
    void Set (const TEXT *qualifier, TAEINT intg);
    void Set (const TEXT *qualifier, TAEFLOAT real);

	//  Qualifier Set for vectors

    void Set (const TEXT *qualifier, int count, const TEXT **svv);
    void Set (const TEXT *qualifier, int count, TAEINT *ivv);
    void Set (const TEXT *qualifier, int count, TAEFLOAT *fvv);

    //		Qualifier Get Scalar Values
    //		DO NOT USE.  use s["name"].String(), etc.

    TEXT *     String (const TEXT *qualifier)   
	{return Qualifier(qualifier) -> String();}
    TAEFLOAT  Real (const TEXT *qualifier)     
	{return Qualifier(qualifier) -> Real();} 
    TAEINT    Intg (const TEXT *qualifier)     
	{return Qualifier(qualifier) -> Intg ();} 

    //		Vector Values for Qualifiers
    //		DO NOT USE. use s["name"].StringVec(), etc.

    TEXT **    StringVec (const TEXT *qualifier)   
	{return &SVAL(*(VARIABLE*) Qualifier(qualifier),0);}
    TAEFLOAT * RealVec (const TEXT *qualifier)     
	{return &RVAL(*(VARIABLE*) Qualifier(qualifier),0);} 
    TAEINT   * IntgVec (const TEXT *qualifier)     
	{return &IVAL(*(VARIABLE*) Qualifier(qualifier),0);} 


    //		Set specific element of qualifier
    //		DO NOT USE.  use s["name"].SetElement() instead

    void SetElement (const TEXT *qual, const TEXT *string, int index); 
    void SetElement (const TEXT *qual, TAEINT value, int index);
    void SetElement (const TEXT *qual, TAEFLOAT value, int index);
};



class SymbolTable {
	VM_STRUCT	vm;

friend Symbol;
public:
    SymbolTable (const TEXT *suffix = nil);
    ~SymbolTable (); 
    void DeleteIf();
    int RefCount ()				// number of "users"
	{return vm.npblk.numvar;}
    void Reference ()				// bump number of "users"
	{vm.npblk.numvar++;}
    TEXT *Suffix ()
	{return vm.npblk.ctxptr;}
    void SetSuffix (const TEXT *newName);		// capture _t, _tar, etc.
    VARIABLE * Variable (const TEXT *name) 		// DO NOT USE 
	{return Vm_FindVar ((Id) &vm, name);}

    //	The best way to get a Symbol reference from a SymbolTable is
    //	st["name"].    You may double index and get a qualifier
    //	from a table:  st["name"]["qual"].

    Symbol * GetSymbol (const TEXT *name) 
	{return (Symbol *)Variable(name);}
    Symbol & operator[] (const TEXT *name) 
	{return *GetSymbol(name);}

    void ReadFile (TEXT *fileSpec) 
	{Vm_ReadFromDisk ((Id) &vm, fileSpec);} 
    void WriteFile (TEXT *fileSpec) 
	{Vm_WriteToDisk ((Id) &vm, fileSpec);}
    void Remove (Symbol *);
    void Remove (TEXT *name) {Remove (GetSymbol(name));}
    void CopyIn (VARIABLE *v) ;				// DO NOT USE
    void CopyIn (Symbol *s) {CopyIn ((VARIABLE *) s);}
    void Add (Symbol *s) ;
    void Merge (SymbolTable *source);			// merge source to this
    void Insert (Symbol *sym, Symbol *relSym=NULL);// insert sym after relSym
    void Insert (Symbol *sym, TEXT *relName=NULL);
    void Replace (Symbol *oldSym, Symbol *newSym); // replace a symbol
    void Replace (TEXT *oldSym, Symbol *newSym)
	    {Replace (GetSymbol(oldSym), newSym);}

    //	do not use ForEach.  The ForEachSymbol macro is much cleaner.
    CODE ForEach (SymbolCallback f, void *context)
	{return Vm_ForEach ((Id) &vm, f, context);}

    Symbol **PointerVector ();			// used in ForEachSymbol macro
    SymbolTable * Clone ();
    void Set (const TEXT *name, Symbol *s);     // copy symbol value (or create)
    void Set (const TEXT *name, TAEINT value);
    void Set (const TEXT *name, int count, TAEINT *value);  
    void Set (const TEXT *name, const TEXT *value);  
    void Set (const TEXT *name, int count, const TEXT **value); 
    void Set (const TEXT *name,  TAEFLOAT value);
    void Set (const TEXT *name, int count, TAEFLOAT *value);
    CODE SetValid (const TEXT *name, int count, int low[], int high[])
	{return (Vm_SetValidIntg ((Id) &vm, name, count, low, high) );} 
    CODE SetValid (const TEXT *name, int count, TAEFLOAT low[], TAEFLOAT high[])
	{return (Vm_SetValidReal ((Id) &vm, name, count, low, high) );}
    CODE SetValid (const TEXT *name, int count, const TEXT **valid)
	{return (Vm_SetValidString ((Id) &vm, name, count, valid) );}

    void GetValid (const TEXT *name, FUNINT *count, TAEINT *low[], TAEINT *high[])
	{Vm_GetValidIntg ((Id) &vm, name, count, low, high);}
    void GetValid (const TEXT *name, FUNINT *count, TAEFLOAT *low[], TAEFLOAT *high[])
	{Vm_GetValidReal ((Id) &vm, name, count, low, high);}
    void GetValid (const TEXT *name, FUNINT *count, TEXT **valid[])
	{Vm_GetValidString ((Id) &vm, name, count, valid);}

    //	    Scalar values.
    //	    These give you nice conversions between Real/Intg,
    //	    e.g., you can ask for the value of an Integer
    //	    symbol as a Real. The preferred way to get a value 
    //      is to use indexing:   myTable["name"].Real()

    //	    Beware: if the symbol doesn't exist, you crash.

    TEXT * String (const TEXT *name)    			// DO NOT USE
	{return GetSymbol(name)->String();} 
    TAEFLOAT   Real ( const TEXT *name)  			// DO NOT USE
	{return GetSymbol(name)->Real();} 
    TAEINT     Intg ( const TEXT *name)  			// DO NOT USE
       {return GetSymbol(name)->Intg();}

    //       Vector values
    //	     Beware: if the symbol doesn't exist, you crash.

    TEXT ** StringVec (const TEXT *name)   			// DO NOT USE
       {return GetSymbol(name)->StringVec();} 
    TAEFLOAT * RealVec (const TEXT *name)  			// DO NOT USE
	{return GetSymbol(name)->RealVec();} 
    TAEINT   * IntgVec (const TEXT *name)  			// DO NOT USE
	{return GetSymbol(name)->IntgVec();}

    Id GetVmId () {return (Id) &vm;}		// don't use this 
};


//	A SymbolTableList is a collection of SymbolTables.
//	This is the C++ form of the traditional Co_ package.

class SymbolTableList {
	Id	collection;
	TEXT	errorMsg [STRINGSIZ+1];
	int	count;
public:
	SymbolTableList ();
	SymbolTableList (const TEXT *fileSpec);
	~SymbolTableList ();
	void WriteFile (const TEXT *fileSpec);
	TEXT * Error () ;
	SymbolTable * Find (const TEXT *memberName);// nil if not present
	SymbolTable & operator[] (const TEXT *memberName)
	    {return *Find (memberName);}

	//   DO NOT USE.  The ForEach macros are better.
	CODE ForEach (StCallback f, void *context)
	    {return Co_ForEach (collection, f, context);}

	int Count ()
	    {return count;}
	SymbolTable ** PointerVector ();		// caller must delete
 	TEXT ** NameVector ();				// caller must delete
							// (but not the names)
	void Add (SymbolTable *, const TEXT *memberName);
	void Remove ( const TEXT *memberName);
	void Free();
};





//	Macros for iteration
//	The "iter" variable is the "iteration variable" 
//	that is sucessively set to values in the collection.	
//	The whole loop is "protected" by a {} bracket so
//	you can re-use the iter variable later on and not
//	get a double def.

//	Beware: the pointer vector used here is "outside"
//	the object that delivered it.  So, if while streaming,
//	you delete some element of the collection, the
//	streaming will malfunction if the deleted object is
//	eventually encountered in the PointerVector.

/************
	ForEachSymbolTable in a SymbolTableList.   Example:

	ForEachSymbolTable(aTable, WbSymbolTableList)
	    printf ("The suffix for this table is %s\n", (*aTable).Suffix());
	EndForEach(aTable)

**************/


#define ForEachSymbolTable(iter,symbolTableList)	\
	{						\
	SymbolTable *iter;				\
	SymbolTable **NAME2(iter,__pv) = (symbolTableList).PointerVector();\
	SymbolTable **NAME2(iter,__org) = NAME2(iter,__pv);\
	for (iter=*NAME2(iter,__pv)++; iter; iter=*NAME2(iter,__pv)++)\
	    {

#define EndForEach(iter)			\
	    }					\
	tae_free (NAME2(iter,__org));		\
	}


/*************

	ForEachSymbol in a SymbolTable or qualifiers of a Symbol.  Examples:

	ForEachSymbol(s, aSymbolTable)
	    printf ("Name is %s\n", (*s).Name());
	EndForEach(v)


	ForEachSymbol(q, aSymbol)
	    printf ("A qualifier named %s\n", (*q).Name());
	EndForEach(q)

**************/


#define ForEachSymbol(iter,symbolTable)		\
	{					\
	Symbol	*iter;				\
	Symbol	**NAME2(iter,__pv) = (symbolTable).PointerVector();	\
	Symbol	**NAME2(iter,__org) = NAME2(iter,__pv);			\
	for (iter=*NAME2(iter,__pv)++; iter; iter=*NAME2(iter,__pv)++)	\
	    {						

#endif

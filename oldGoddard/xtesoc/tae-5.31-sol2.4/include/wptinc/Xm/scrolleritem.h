/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


// Include file for WPT item presentation type SCROLLER
// CHANGE LOG:
// 27-feb-92	Integration of contributed files; changed includes;
//         	added DefaultTraversalState(); NOTE: no UpdateView...kbs
// 14-oct-94	Ultrix Port: #endif cannot have anything following it...shj 

/* This file implements the SCROLLER (NoPump) OBJECT, as interfaced from
   TAE programs and the Workbench. */

#ifndef I_SCROLLERITEM
#define I_SCROLLERITEM 0

#include <widgetagent.h>
#include <presentation.h>

#define  SCROLLER_SENTINEL 0xFF1221FF /* Scroller Object item */

// Presentation of type "scroller"

class ScrollerP : public Presentation {
public:
	int TypeOk (int type) 
		{return (type == V_STRING);}
	char * TypeName () 
		{return "scroller";}
	int ValidsRequired() {return FALSE;}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};

/************************************************************************/

// Base of All Scroller Object classes
//WidgetAgent representing all Scroller items (as a WptItem)
class ScrollerA : public WptItem {
protected:
  String current;
  void    Check() 
    {if (sentinel != SCROLLER_SENTINEL) abort(); }
  XtCallbackProc   selectCallbackPtr;  
  void *  selectCallbackData;
  int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		    Widget parentWidget);
public:
  ScrollerA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
	   Symbol *viewSym);
  virtual ~ScrollerA();
  int DisplayNewValue(int flag);

  void SetAppCallback(TEXT* callbackType,
		      XtCallbackProc appCallbackProc, void * appCallbackData);

  void DispatchCallback(Widget widgetId, TEXT * callbackType,
			void * callData) ;

  //int UpdateView (Symbol *newView);

  Boolean DefaultTraversalState() {return False;}
};


#endif 

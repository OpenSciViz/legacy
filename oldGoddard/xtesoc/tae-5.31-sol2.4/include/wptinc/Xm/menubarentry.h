/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// One of the header files for a WPT item presentation type Menubar 
// This file defines the various menu entry type classes which may appear in 
// the cascading menu system.
//
// Change Log:
//
// 19-apr-92	Initial Creation...krw
// 31-jul-92    PR1466: If we are running the WorkBench, we must disable
//              MenuIcon caching. Otherwise, the designer may not see his
//              changes to the bitmap file...krw
// 05-oct-92	Fix C++ anachronisms...rt
// 16-oct-92	PR1695: some methods needed to return void rather than not
//		saying anything...krw
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw
//
#ifndef I_MENUBARENTRY
#define I_MENUBARENTRY 0

#include <widgetagent.h>
#include <presentation.h>

#ifdef VMS
#include "xm.h"
#include <RowColumn.h>
#include <Label.h>
#include <CascadeB.h>
#include <Separator.h>
#include <PushB.h>
#include <ToggleB.h>
#else
#include <Xm/Xm.h>
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/CascadeB.h>
#include <Xm/Separator.h>
#include <Xm/PushB.h>
#include <Xm/ToggleB.h>
#endif

class MenubarA;
class CascadeEntry;

class MenuEntry {
protected:
friend class MenubarA;
    Widget mainWidget;			// the primary Motif widget
    CascadeEntry *parentCascade;	// the entry's parent cascade entry
    WptArg **WptEntryArgs;
    COUNT  argCnt;
    TEXT   *name;

    int BldCommonEntryArgs (Symbol *viewSym, Symbol *targetSym,
                Widget parentWidget);

    int BldXtkArgList (Symbol *viewSym, Symbol *targetSym, 
			Widget parent, XtkArg ** xtkArgs);

    void FreeCommonArgList();

		// Update "_select" based on index of entry name in valids
    void Load_Select(Symbol *target);

	// Virtual Methods
			// Build any subclass specific Xtk Args
    virtual void BldSpecificXtkArgs ( Symbol *viewSym, Symbol *targetSym, 
		Widget parent, XtkArg * xtkArgs) 
    	{viewSym=viewSym; targetSym=targetSym; 
	 parent=parent; xtkArgs=xtkArgs;}	// by default none

    virtual int SpecificArgCnt() {return 0;}	// number of specific args
			// after creating/updating entry, anything else to do?

    virtual void Finish(Symbol *targetSym, Symbol *viewSym) 
    	{targetSym=targetSym; viewSym=viewSym;}

public:
    MenuEntry (Widget parent, CascadeEntry* cascade,
		Symbol* viewSym, Symbol* targetSym);
    virtual ~MenuEntry ();

    Widget MainWidget() {return mainWidget;}
    TEXT *Name() {return name;}

			// Set the target Symbol's value from this entry
    virtual void LoadTargetValue(Symbol *target);

			// Update the view for this entry. The Item's
			// UpdateView method only knows about items.
    virtual void UpdateEntryView(Widget parent, Symbol* viewSym, 
		Symbol* targetSym);
};

class CascadeEntry : public MenuEntry {
protected:
    Widget pulldownWidget;
public:
    XtArgVal	fg, bg, font;		// need for inheritance
    CascadeEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
    ~CascadeEntry ();// only necessary if something special?
    Widget MenuWidget() {return pulldownWidget;}
    void UpdateEntryView(Widget parent, Symbol* viewSym, 
		Symbol* targetSym);
};

class HelpEntry : public CascadeEntry {
public:
    HelpEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
};

class LabelEntry : public MenuEntry {
public:
    LabelEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
};

class IconEntry : public MenuEntry {
    Pixmap	pixmap;			// Icon's pixmap
protected:
    void BldSpecificXtkArgs (Symbol *viewSym, Symbol *targetSym,
			Widget parent, XtkArg * xtkArgs);
    int SpecificArgCnt() {return 2;}
public:
    IconEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
    ~IconEntry();
    void FreePixmap();			// Destroy the pixmap.
};

class PushButtonEntry : public MenuEntry {
public:
    PushButtonEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
};

class SeparatorEntry : public MenuEntry {
protected:
    void BldSpecificXtkArgs (Symbol *viewSym, Symbol *targetSym,
			Widget parent, XtkArg * xtkArgs);
    int SpecificArgCnt() {return 2;}
public:
    SeparatorEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
};

class CheckboxEntry : public MenuEntry {
protected:
    void Finish(Symbol *targetSym, Symbol *viewSym);
public:
    CheckboxEntry (Widget parent, CascadeEntry *parentCascade,
		Symbol* viewSym, Symbol* targetSym);
    void LoadTargetValue(Symbol *target);	// override default
    void SetCBState(TEXT *cbstate) {		// Set the Checkbox State
	XmToggleButtonSetState(mainWidget, // True means On, False means Off
	STR_EQUAL(cbstate, "yes"), False);}
};

#endif


/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


// Change Log:
// 
// 14-oct-94	PR2875: GetErrorMessage improperly declared...shj

extern "C" void _wptReqErrorMessage () ;
extern "C" int _wptGetErrorMessage (TEXT *receiver) ;

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type OPTION
// CHANGE LOG:
// 02-jan-92  	Initial implementation based on pulldown...dar 
// 18-feb-92  	Define SetState to use SetStatePropagate...cew
// 04-mar-92	ForceSize() should return void...ljn
// 05-oct-92	Fix C++ anachronisms...rt
// 06-oct-92	Typo caused this not to compile...rt
// 08-may-93    PR1915 Override BldTraversalArgs...tpl
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_OPTION
#define I_OPTION 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <RowColumn.h>
#include <PushBG.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/RowColumn.h>
#include <Xm/PushBG.h>
#endif

// For OptionA class
#define  OPTION_SENTINEL  0xFF1216FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "option"
class OptionP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==V_STRING;}
	int ValidsRequired ()
		{return TRUE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "option";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class OptionA : public WptItem {
protected:
    int		lastIndex;		// initial index to labels
    int		currentIndex;		// current index to labels
    Widget	menuWidget;		// Pulldown Menu widget
    Widget	optButton;		// Button Gadget
    Widget	optLabel;		// Label Gadget
    slist	buttons;		// list of pushbutton widgets
    XtkArg	*btnArgList;		// Button Xtk args..for Parm updates
    int		numBtnArgs;		// Number of button Xtk args.

    void    Check() 
	{if (sentinel != OPTION_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldPulldownWptArgList(Symbol *targetSym, Symbol *viewSym,
	Widget parentWidget);
    int BldButtonWptArgList(Symbol *targetSym, Symbol *viewSym,
	Widget parentWidget);
    int BldOptionWptArgList(Symbol *targetSym, Symbol *viewSym,
	Widget parentWidget, Widget PulldownWidget, Widget HistoryButton);
#if XmVERSION >= 1 && XmREVISION >= 2
    virtual void  BldTraversalArgs(Symbol *view, WptPanel *,
                                        Boolean DefaultState);
#endif
    void ForceSize();		// force the size to width, height in res file

public:
    OptionA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~OptionA();

    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int  DisplayNewValue(int newTarget);
    void SetLastValInTarget()
	{SetTargetValue(lastIndex);  noLastValue = TRUE; }
    virtual int SetState (int newState)
	{return SetStatePropagate(newState);}
    virtual int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState)
	{ return WptItem::GetState(currentState);}
    int DefaultShadowThickness() { return 2; }
    int DefaultBorderWidth() { return 0; }
    Boolean DefaultTraversalState()
                                {return True;};


    UpdateView(Symbol *newView);

    };
#endif


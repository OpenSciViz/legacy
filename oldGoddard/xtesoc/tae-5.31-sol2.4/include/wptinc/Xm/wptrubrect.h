/*
 * Copyright (c) 1987, 1988, 1989 Stanford University
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Stanford not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Stanford makes no representations about
 * the suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * STANFORD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL STANFORD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * Rubberbanding for rectangles.
 */
// Change Log:
// 30-jun-93	Copied from IV 2.6...cew

#ifndef wptrubrect_h
#define wptrubrect_h

#include <wptrubband.h>

class WptRubberRect : public WptRubberband {
public:
    WptRubberRect(
        Display*, Screen*, int x0, int y0, int x1, int y1,
	int offx = 0, int offy = 0
    );
    virtual void GetOriginal(int& x0, int& y0, int& x1, int& y1);
    virtual void GetCurrent(int& x0, int& y0, int& x1, int& y1);
    virtual void Draw();
protected:
    int fixedx, fixedy;
    int movingx, movingy;
};

class WptSlidingRect : public WptRubberRect {
public:
    WptSlidingRect(
        Display*, Screen*, int x0, int y0, int x1, int y1, 
	int rfx, int rfy, int offx = 0, int offy = 0
    );
    virtual void GetCurrent(int& x0, int& y0, int& x1, int& y1);
protected:
    int refx;
    int refy;
};

#endif

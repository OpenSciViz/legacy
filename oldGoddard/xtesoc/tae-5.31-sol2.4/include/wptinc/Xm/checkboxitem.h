/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for a WPT item presentation type CHECKBOX

// CHANGE LOG
// 13-aug-90	removed unnecessary HELP code..kbs
// 18-nov-90    removed set state which is being handled by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 08-may-91    PR863 added setlasetvalintarget...tpl
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 10-jan-92	Removed DefaultArgs...tpl
// 21-jan-92    Added DefaultTraversalState...tpl
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw

#ifndef I_ACTIONCHECKBOX
#define I_ACTIONCHECKBOX 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <ToggleB.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/ToggleB.h>		
#endif

// For CheckboxA class
#define  CHECKBOX_SENTINEL  0xFF1211FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "checkbox"
class CheckboxP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "checkbox";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a checkboxWidget (as a WptItem)

class CheckboxA : public WptItem {
protected:

    void    Check() 
	{if (sentinel != CHECKBOX_SENTINEL) abort(); }

    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;

    XtCallbackProc   releaseCallbackPtr;  
    void *  releaseCallbackData;

    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);

public:
    CheckboxA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~CheckboxA();   

    // Use override for subclasses of checkboxs (help checkboxs)
    virtual void CheckboxSelect(void *callData);	
    virtual void CheckboxRelease(void *callData);	

    virtual void SetAppCallback(TEXT* callbackType, XtCallbackProc 
		appCallbackProc, void * appCallbackData);
    virtual void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);

    virtual int  SetState(int state)
	{return WptItem::SetState(state);}
    virtual int SetState (int index, int newState)
		{index=index; newState=newState; return 1;}
    virtual int  DisplayNewValue(int newTarget); // display new value in target
    virtual void SetLastValInTarget();
    virtual Boolean DefaultTraversalState()
                                {return True;};
    };
#endif

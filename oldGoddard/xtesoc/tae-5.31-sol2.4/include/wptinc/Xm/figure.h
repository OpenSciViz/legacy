/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * planar figures
 */
/*
 * Change Log:
 * 18-jul-93  Removed Tool stuff, and cleaned up debug code...krw
 * 28-jul-93  PR2024 added bg and pattern to the Graphic class so we
 *            can dither the fill color whenever the stroke changes...krw
 * 23-aug-93  Minor changes suggested by code review...krw
 * 18-sep-93  PR2233 Removed GlyphFigure::request, ::allocate and moved them
 *	      to TextObj class only...krw
 */
#ifndef figure_h
#define figure_h

#include <InterViews/glyph.h>
#include <InterViews/transformer.h>
#include <InterViews/pattern.h>
#include "globals.h"

class BoxObjList;
class Brush;
class Color;
class Font;
class GraphicList;
class PolyGlyph;
class String;
class Transformer;

#include <InterViews/raster.h>
#include <InterViews/image.h>
#include <InterViews/bitmap.h>
#include <InterViews/stencil.h>

const int buf_size = 10;

// ****************************************************************************
// G R A P H I C   C L A S S
// ****************************************************************************
//
class Graphic : public Glyph {
public:
    Graphic(Graphic* gr = nil);
    virtual ~Graphic ();

    virtual void request(Requisition&) const;
    virtual void allocate(Canvas*, const Allocation&, Extension&);
    virtual void draw(Canvas*, const Allocation&) const;
    virtual void drawit(Canvas*);
    virtual void drawclipped(Canvas*, Coord, Coord, Coord, Coord);

    virtual Glyph* clone() const;
    virtual void flush();

    virtual Transformer* transformer();
    virtual void transformer(Transformer*);
    void eqv_transformer(Transformer&);

    virtual void brush(const Brush*);
    virtual const Brush* brush();
    virtual void stroke(const Color*);
    virtual const Color* stroke();
    virtual void fill(const Color*);

    virtual const Color* bg();
    virtual void bg(const Color*);
    virtual const Pattern* pattern();
    virtual void pattern(const Pattern*);

    virtual const Color* fill();
    virtual void font(const Font*);
    virtual const Font* font();
    virtual void closed(boolean);
    virtual boolean closed();
    virtual void curved(boolean);
    virtual boolean curved();
    virtual int ctrlpts(Coord*&, Coord*&) const;
    virtual void ctrlpts(Coord*, Coord*, int);
    virtual Graphic* parent();
    virtual void parent(Graphic*);
    virtual Graphic* root();

    void translate(Coord dx, Coord dy);
    void scale(Coord sx, Coord sy, Coord ctrx = 0.0, Coord ctry = 0.0);
    void rotate(float angle, Coord ctrx = 0.0, Coord ctry = 0.0);
    void align(Alignment, Graphic*, Alignment);

    virtual void recompute_shape();
    virtual void getbounds(Coord&, Coord&, Coord&, Coord&);
    virtual void getcenter(Coord&, Coord&);    
    virtual boolean contains(PointObj&);
    virtual boolean intersects(BoxObj&);

    virtual void undraw();
    virtual void append_(Graphic*);
    virtual void prepend_(Graphic*);
    virtual void insert_(GlyphIndex, Graphic*);
    virtual void remove_(GlyphIndex);
    virtual void replace_(GlyphIndex, Graphic*);
    virtual void change_(GlyphIndex);

    virtual GlyphIndex count_() const;
    virtual Graphic* component_(GlyphIndex) const;
    virtual void modified_(GlyphIndex);

    virtual Graphic* first_containing(PointObj&);
    virtual Graphic* last_containing(PointObj&);

    virtual Graphic* first_intersecting(BoxObj&);
    virtual Graphic* last_intersecting(BoxObj&);

    virtual Graphic* first_within(BoxObj&);
    virtual Graphic* last_within(BoxObj&);

    virtual Graphic& operator = (Graphic&);
    
    virtual boolean textObject();// for TAE Plus, is this object a textObject?

    void get_original(const Coord*&, const Coord*&);
    void add_point (Coord x, Coord y);
    void add_curve (Coord x, Coord y, Coord x1, Coord y1, Coord x2, Coord y2);
    void Bspline_move_to (
        Coord x, Coord y, Coord x1, Coord y1, Coord x2, Coord y2
    );
    void Bspline_curve_to (
        Coord x, Coord y, Coord x1, Coord y1, Coord x2, Coord y2
    );

    void enableDrawing();
    void disableDrawing();
    boolean drawingEnabled();

protected:
    Graphic (
        const Brush* brush, const Color* stroke, const Color* fill,
        const Font* font, boolean closed, boolean curved, int coords, 
        Transformer*
    );

    virtual void draw_gs(Canvas*, Graphic*);
    virtual void drawclipped_gs(
        Canvas*, Coord, Coord, Coord, Coord, Graphic*
    );
    virtual void getextent_gs(
        Coord&, Coord&, Coord&, Coord& ,Coord& ,Graphic* gs
    );
    virtual boolean contains_gs(PointObj&, Graphic* gs);
    virtual boolean intersects_gs(BoxObj&, Graphic* gs);
    virtual void getbounds_gs(Coord&, Coord&, Coord&, Coord&, Graphic* gs);

    virtual void total_gs (Graphic& gs);
    void parentXform(Transformer& t);
    
    virtual void concat_gs(Graphic* a, Graphic* b, Graphic* dest);
    virtual void concatXform(
        Transformer* a, Transformer* b, Transformer* dest
    );
    virtual void concat(Graphic* a, Graphic* b, Graphic* dest);

/*   Helpers   */

    virtual boolean contains_(Graphic*, PointObj&, Graphic* gs);
    virtual boolean intersects_(Graphic*, BoxObj&, Graphic* gs);
    virtual void getbounds_(
        Graphic*, Coord&, Coord&, Coord&, Coord&, Graphic* gs
    );
    void total_gs_(Graphic*, Graphic& gs);
    void concatgs_(Graphic*, Graphic*, Graphic*, Graphic*);
    void concatXform_(Graphic*, Transformer*, Transformer*, Transformer*);
    void concat_(Graphic*, Graphic*, Graphic*, Graphic*);
    void getextent_(Graphic*, Coord&, Coord&, Coord&, Coord&, Coord&,Graphic*);

    void draw_(Graphic*, Canvas*, Graphic*);
    void drawclipped_(Graphic*, Canvas*, Coord, Coord, Coord, Coord, Graphic*);
    void transform_(Coord, Coord, Coord&, Coord&, Graphic*);
protected:
    const Brush* _brush;
    const Color* _stroke;
    const Color* _fill;
    const Color* _bg;                    // background (not fill color)
    const Font* _font;
    Transformer*  _t;
    const Pattern* _pattern;             // pattern for fill color dithering

    boolean _closed;
    boolean _curved;
    int _ctrlpts;
    int _buf_size;
    Coord* _x;
    Coord* _y;

    Coord _xmin;
    Coord _xmax;
    Coord _ymin;
    Coord _ymax;
    Graphic* _parent;
    boolean	_drawEnabled;		// true if drawing enabled (default)
};

inline					// by default I am not a TextObj
boolean Graphic::textObject() { return false;}


inline
void Graphic::enableDrawing() {_drawEnabled = true;}

inline
void Graphic::disableDrawing() {_drawEnabled = false;}

inline
boolean Graphic::drawingEnabled() {return _drawEnabled;}


class PolyGraphic : public Graphic {
public:
    PolyGraphic(Graphic* = nil) ;
    virtual ~PolyGraphic();
    virtual void request(Requisition&) const;
    virtual void allocate(Canvas*, const Allocation&, Extension&);

    virtual void undraw();

    virtual void append_(Graphic*);
    virtual void prepend_(Graphic*);
    virtual void insert_(GlyphIndex, Graphic*);
    virtual void remove_(GlyphIndex);
    virtual void replace_(GlyphIndex, Graphic*);
    virtual void change_(GlyphIndex);

    virtual GlyphIndex count_() const;
    virtual Graphic* component_(GlyphIndex) const;

    virtual void modified_(GlyphIndex);
    virtual void flush();
    virtual Glyph* clone() const;

    virtual Graphic* first_containing(PointObj&);
    virtual Graphic* last_containing(PointObj&);

    virtual Graphic* first_intersecting(BoxObj&);
    virtual Graphic* last_intersecting(BoxObj&);

    virtual Graphic* first_within(BoxObj&);
    virtual Graphic* last_within(BoxObj&);

protected:
    virtual void draw_gs(Canvas*, Graphic*);
    virtual void drawclipped_gs(
        Canvas*, Coord, Coord, Coord, Coord, Graphic*
    );
    virtual boolean contains_gs(PointObj&, Graphic*);
    virtual boolean intersects_gs(BoxObj&, Graphic*);
    virtual void getextent_gs(
        Coord&, Coord&, Coord&, Coord&, Coord&, Graphic* gs
    );

protected:
    PolyGlyph* _body;
};



class GraphicMaster : public PolyGraphic {
public:
    GraphicMaster(Graphic* = nil, const Color* bg = nil);
    virtual ~GraphicMaster();

    virtual void request(Requisition&) const;
    virtual void allocate(Canvas*, const Allocation&, Extension&);
    virtual Glyph* clone() const;
protected:
    virtual void drawclipped_gs(
        Canvas*, Coord, Coord, Coord, Coord, Graphic*
    );
protected:
    Allocation _a;
};


class Line : public Graphic {
public:
    Line (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord x1, Coord y1, Coord x2, Coord y2, Transformer*
    );
    virtual Glyph* clone() const;
    
protected:
    virtual ~Line ();
};

class Rectangle : public Graphic {
public:
    Rectangle (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord l, Coord b, Coord r, Coord t, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    virtual ~Rectangle ();
};

class Circle : public Graphic {
public:
    Circle (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord x, Coord y, Coord r, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    virtual ~Circle ();
};

class Ellipse : public Graphic {
public:
    Ellipse (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord x, Coord y, Coord rx, Coord ry, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    virtual ~Ellipse ();
};

class Open_BSpline : public Graphic {
public:
    Open_BSpline (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord* x, Coord* y, int ctrlpts, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    Open_BSpline(Open_BSpline*);
    virtual ~Open_BSpline ();
};

class Closed_BSpline : public Graphic {
public:
    Closed_BSpline (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord* x, Coord* y, int ctrlpts, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    Closed_BSpline(Closed_BSpline*);
    virtual ~Closed_BSpline ();
};

class Polyline : public Graphic {
public:
    Polyline (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord* x, Coord* y, int ctrlpts, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    virtual ~Polyline ();
};

class Polygon : public Graphic {
public:
    Polygon (
        const Brush* brush, const Color* stroke, const Color* fill,
        Coord* x, Coord* y, int ctrlpts, Transformer*
    );
    virtual Glyph* clone() const;

protected:
    virtual ~Polygon ();
};

// ****************************************************************************
// intermediate class for Glyph Figure type objects. These objects are not
// graphical objects in the sense of Lines and Polygons. The body of a
// Glyph figure is a Glyph object such as an Image or Stencil.
// This is here to support common methods.
class GlyphFigure : public Graphic {
public:
    GlyphFigure();
    GlyphFigure(Glyph *newBody);
    void setBody(Glyph *newBody);
    virtual void draw(Canvas*, const Allocation&) const;

protected:
    virtual ~GlyphFigure();
    
    virtual void getextent_gs(Coord&, Coord&, Coord&, Coord&, Coord&,Graphic*);
    virtual boolean contains_gs(PointObj&, Graphic*);
    virtual boolean intersects_gs(BoxObj&, Graphic*);
    virtual void draw_gs(Canvas*, Graphic*);
protected:
    Allocation _a;
    Glyph* _body;
};

inline GlyphFigure::GlyphFigure() {_body = nil;}
inline GlyphFigure::GlyphFigure(Glyph *newBody) {
    _body = newBody; 
    Resource::ref(_body);
}
inline void GlyphFigure::setBody(Glyph *newBody) {
    Resource::unref(_body);
    _body = newBody;
    Resource::ref(_body);
}


class TextObj : public GlyphFigure {
public:
    TextObj (
        const Font* font, const Color* stroke, const char*, Transformer*
    );
    virtual void request(Requisition&) const;
    virtual void allocate(Canvas*, const Allocation&, Extension&);
    virtual void text(const char*);
    virtual const char* text();
    virtual Glyph* clone() const;

    virtual boolean textObject();// for TAE Plus, is this object a textObject?

protected:
    virtual ~TextObj();
    
    void init();
protected:
    String* _text;
};

inline
boolean TextObj::textObject() { return true;}

// Added by KRW
class RastObj : public GlyphFigure {
public:
    RastObj (const Raster* raster, Transformer *t);
    virtual Glyph* clone() const;
protected:
    virtual ~RastObj();

    const Raster* _raster;
};


class StenObj : public GlyphFigure {
public:
    StenObj(const Bitmap*, const Color*, Transformer *t);
    virtual Glyph* clone() const;
protected:
    virtual ~StenObj();

    const Bitmap *_bitmap;
    Stencil *_stencil;
};
#endif

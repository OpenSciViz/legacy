/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for a WPT item presentation type Menubar 
// Specifically these are the class definitions for MenubarA
// Class definitions for the various menubar entry types are in menubarentry.h
//
// Change Log:
// 
// 19-apr-92	Initial Creation...krw
// 08-may-93    PR1904 Overrid virtual function BldTraversalArgs...tpl
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw
// 11-apr-94	RS/6000 port: renamed MenuEntry() to GetMenuEntry()...dag
//
#ifndef I_MENUBAR
#define I_MENUBAR 0


#include <widgetagent.h>
#include <presentation.h>
#include <menubarentry.h>

#ifdef VMS
#include "xm.h"
#include <RowColumn.h>
#else
#include <Xm/Xm.h>
#include <Xm/RowColumn.h>
#endif

// For MenubarA class
#define  MENUBAR_SENTINEL  0xFF1218FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "menubar"
class MenubarP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==V_STRING;}
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "menubar";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/
// These MBENTRY objects are inserted into the entries slist in the MenubarA
// object. There will be one MBENTRY object per menu entry (of course)
// This class was created to make it easier for the MenubarA class to manipulate
// 
class MBENTRY {
protected:
    TEXT *name;
    TEXT *type;
    MenuEntry *menuEntry;

public:
    MBENTRY(TEXT *Ename, TEXT *Etype, MenuEntry *EmenuEntry)
	{name = s_save(Ename); type = s_save(Etype); menuEntry = EmenuEntry;}
    ~MBENTRY() {tae_free(name); tae_free(type); 
		delete menuEntry;}		// TBD should we destroy here?
    TEXT *Name() {return name;}
    TEXT *Type() {return type;}
    MenuEntry *GetMenuEntry() {return menuEntry;}
    BOOL SameName(TEXT *otherName) 	// return TRUE if entry has same name
	{return (STR_EQUAL(name, otherName));}
    BOOL SameType(TEXT *otherType)	// return TRUE if entry is same Type
	{return (STR_EQUAL(type, otherType));}
    BOOL SameWidget(Widget widget) 	// return TRUE if entry is same widget
	{return (menuEntry->MainWidget() == widget);}
    void LoadTargetValue(Symbol *target)// load entry's value into target symbol	
	{menuEntry->LoadTargetValue(target);}
    };

//WidgetAgent representing a menubar Widget (as a WptItem)


class MenubarA : public WptItem {
    int  lastIndex;                     // index of "last" or previous value
    int  currentIndex;                  // index of "current" value
    slist entries;			// list of all entries in this menubar
#ifdef DEBUGOUT
    Window helpWindow;			// Window for help mode
#endif

    void    Check() 
	{if (sentinel != MENUBAR_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    void BldEntry(Widget parent, CascadeEntry *parentCascade,
	Symbol* viewSym, Symbol* targetSym);
    MBENTRY *FindEntry(Widget widgetId);
    MBENTRY *FindEntry(int index){
	if (index < 0 || index >= entries.count()) return NULL;
	else return ((MBENTRY *)entries.entry(index));}
    MBENTRY *FindEntry(TEXT *entryName);

#if XmVERSION >= 1 && XmREVISION >= 2
    virtual void  BldTraversalArgs(Symbol *view, WptPanel *,
                                        Boolean DefaultState);
#endif

public:
    MenubarA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~MenubarA();   

    void ModeSetup(int mode);   // special setup in each mode

    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
                void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
                void * callData);
    int  DisplayNewValue(int newTarget);
    void SetLastValInTarget();
    virtual int SetState(int state) {return WptItem::SetState(state);}
    virtual int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState)
	{ return WptItem::GetState(currentState);}
    int DefaultShadowThickness() { return 0; }
    int DefaultBorderWidth() { return 0; }
    Boolean DefaultTraversalState() {return True;}
    Boolean HasComponents() {return True;}

    UpdateView(Symbol *newView);
    };
#endif

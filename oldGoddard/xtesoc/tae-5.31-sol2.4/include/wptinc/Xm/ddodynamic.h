/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// CHANGE LOG:
// 18-jul-93    Initial implementation to support V5.3 DDOs..krw
// 07-aug-93    Added DDOAStripchartXform class...krw
// 24-aug-93    Minor changes as suggested by code review...krw
// 12-oct-93    PR2353: DDOA is ambiguous in the place used...rt
// 11-apr-94  RS/6000 port: renamed DDODynamic::DDOA() to GetDDOA()...dag
//
#ifndef I_DDODYNAMIC
#define I_DDODYNAMIC 0

#include <widgetagent.h>
#include <presentation.h>

#include <Xtae/TIV.h>

/***************************************************************************/
// D D O A D y n a m i c   C l a s s
/***************************************************************************/
// This class is the logical embodiment of a ddo Dynamic.
// A DDODynamic will have one or more DDOATransforms associated with it.
// The idea is that for Homogenous DDOs (v5.2 type) each DDOADynamic will
// have the same Type of DDOATransform associated with it. 2-D movers will
// have two DDOAMover transforms.
//
class DDOA;
class DDOATransform;

class DDOADynamic {
public:
    DDOADynamic (DDOA *ddoa, TEXT *pictureName);

    ~DDOADynamic();

    void AddTransform (DDOATransform *xForm);

    void RemoveAllTransforms ();

    // Define the types of user input to report back to me.
    // The TIV callback is also registered.
    void DefineUserInput (Symbol *viewSym);
    void DefineUserInput (int inputEnable, int reportEventMask);

    void NewWptTargetValues (TAEFLOAT *valueVec); // new values from Wpt

    // new values from user, must update target. This routine is called in
    // response to DDO direct manipulation user input

    void UpdateTarget (TAEFLOAT *valueVec);

    // Return a my list of DDOATransforms
    slist &TransformList ();

    // TRUE if user input  event should be reported back to the WPT application
    virtual Boolean ReportEvent (int xeventType);

    // Simple TIV methods

    TIVStatus Flush();
    TIVStatus Map();
    TIVStatus Unmap();
    TIVStatus EnableInput();
    TIVStatus DisableInput();


    TEXT *Picture();                   // what picture was used for me?
    TIVHandle Handle();                // whats my TIV handle?
    DDOA *GetDDOA();                      // whats my DDOA?
    int InputEnableFlag();
    int ReportEventMask();

private:

    TEXT        *pictureName_;         // saved for debug purposes
    TIVHandle   dynHandle_;            // TIV dynamic handle
    class DDOA  *ddoa_;                // parent DDO containing this dynamic
    slist       xFormList_;            // list of DDOATransforms for this dyn.

        // user input related values    

    int         inputEnable_;
     // OR of ButtonPressMask, ButtonReleaseMask, and/or PointerMotionMask
     // to define which events to report back to user
    long        reportEventMask_;
    };

inline
slist &DDOADynamic::TransformList() {return xFormList_;}

inline
TIVStatus DDOADynamic::Flush() {return TIV_FlushDynamic(dynHandle_);}

inline
TIVStatus DDOADynamic::Map(){return TIV_MapDynamic(dynHandle_);}

inline
TIVStatus DDOADynamic::Unmap(){return TIV_UnmapDynamic(dynHandle_);}

inline
TIVStatus DDOADynamic::EnableInput(){return TIV_EnableInput(dynHandle_);}

inline
TIVStatus DDOADynamic::DisableInput(){return TIV_DisableInput(dynHandle_);}

inline
TEXT *DDOADynamic::Picture() {return pictureName_;}

inline
TIVHandle DDOADynamic::Handle() {return dynHandle_;}

inline
DDOA *DDOADynamic::GetDDOA() {return ddoa_;}

inline
int DDOADynamic::InputEnableFlag() {return inputEnable_;}

inline
int DDOADynamic::ReportEventMask() {return reportEventMask_;}


/***************************************************************************/
// D D O A T r a n s f o r m   C l a s s
/***************************************************************************/
// This is a base class for the various DDO transform types
// Users create one of the subclass instances, not this type.
// We have different subclasses because many of the resources are different.
//
class DDOATransform {
public:
    ~DDOATransform ();

    TIVStatus DoTransform(TAEFLOAT *valueVec);

    TIVHandle Handle();
    DDOADynamic *Dynamic();
    int Index();
    Symbol *ViewSym();

    TAEFLOAT GetTargetValue();

        // replace my value in target with this one

    void SetTargetValue(TAEFLOAT newValue);  

        // Virtual methods

    virtual int xformType();               // what kind of fool am I?

        // UpdateView methods return the revised changeMask.

    unsigned long UpdateView (Symbol *newView);  // new view info from Wpt 

        // for use by homogenous DDOs
    void UpdateView (DDOATransform *origXform, unsigned long changeMask);

        // return the current tivStatus for this transform
    TIVStatus GetStatus();

protected:
    TIVHandle    transformHandle_;
    DDOADynamic  *dynamic_;
    int          targetIndex_;
    TIVStatus    tivStatus_;
    Symbol       *viewSym_;
    unsigned long changeMask_;

    // Common resource values

    TIVReal  minimum_;                           // minimum value
    TIVReal  maximum_;                           // maximum value
    TIVReal  updateDelta_;                       // update Delta
    Boolean  venableFlag_;                       // venableFlag
    char     *venableStr_;                       // value display enable string
    char     *vDisplayFormat_;                   // value display format string

    // transform structure, used by subclasses to store thresholds
    // and other Transform specific stuff. Typically, the subclasses
    // will fill this in their ctors.

    TIVTransformStruct xformStruct_;

    // Constructors

    DDOATransform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym);
    DDOATransform (DDOATransform *origXform, int targetIndex, 
		   DDOADynamic *dyn);            // a clone

    
        // create color thresholds in xformStruct_ from theView 
        // (usually viewSym_)

    void buildColorThresholds(Symbol *theView);

        // copy color thresholds from one structure to my xformStruct_

    void copyColorThresholds(const TIVTransformStruct &original);

        // see if thresholds have changed. Works for both Color
        // and Picture name thresholds

    void checkThresholds (Symbol *newView);

    void checkStdResources (Symbol *newView);

        // copy the standard resources from one transform to another

    void copyStdResources (DDOATransform *origXform);

        // On a UpdateView each subclass can add specific resource checking by 
        // redefining this method. The subclass must update the transform
        // struct and changeMask_ accordingly

    virtual void checkSubclassResources(Symbol *newView);

        // subclass should update its specific resources and transforms

    virtual void copySubclassResources(DDOATransform *origXform);

        // Actually create the TIV Transform. 
 
    void createTIVTransform ();

        // Update the transform based on the current Transform values and
        // changeMask.

    void updateTIVTransform ();

    void checkStatus (TEXT *msgText);
    };

    
inline
TIVStatus DDOATransform::DoTransform(TAEFLOAT *valueVec) {
    return TIV_DoTransform(transformHandle_, valueVec[targetIndex_]);}

inline
TIVHandle DDOATransform::Handle() {return transformHandle_;}

inline
DDOADynamic *DDOATransform::Dynamic() {return dynamic_;}

inline
int DDOATransform::Index() {return targetIndex_;}

inline
Symbol *DDOATransform::ViewSym() {return viewSym_;}

inline
TIVStatus DDOATransform::GetStatus() {return tivStatus_;}

/***************************************************************************/
// D D O A M o v e r X f o r m   C l a s s
/***************************************************************************/
// Embodies a mover specific transformation
//
class DDOAMoverXform : public DDOATransform {
public:
    DDOAMoverXform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym,
		    int directionOverride = -1);

    // The following ctor is useful for Homogenous DDOs
    // The ctor uses the parameter values rather than digging into the
    // viewSym for the values. Essentially this is a clone.
    DDOAMoverXform (DDOAMoverXform *original, int targetIndex,
		    DDOADynamic *dyn, int directionOverride = -1); 

    virtual int xformType();

        // On a UpdateView the mover can check  specific resources.
        // The mover  must update the transform struct and changeMask_

    virtual void checkSubclassResources(Symbol *newView);

        // copy mover specific resources

    virtual void copySubclassResources(DDOATransform *origXform);

protected:
    };

/***************************************************************************/
// D D O A S t r e t c h e r X f o r m   C l a s s
/***************************************************************************/
// Embodies a stretcher specific transformation
//
class DDOAStretcherXform : public DDOATransform {
public:
    DDOAStretcherXform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym);

    // The following ctor is useful for Homogenous DDOs
    // The ctor uses the parameter values rather than digging into the
    // viewSym for the values. Essentially this is a clone.
    DDOAStretcherXform (DDOAStretcherXform *original, int targetIndex,
			DDOADynamic *dyn);

    virtual int xformType();

        // On a UpdateView the stretcher can check  specific resources.
        // The stretcher must update the transform struct and changeMask_

    virtual void checkSubclassResources(Symbol *newView);

        // copy stretcher specific resources

    virtual void copySubclassResources(DDOATransform *origXform);

protected:
    };
    
/***************************************************************************/
// D D O A R o t a t o r X f o r m   C l a s s
/***************************************************************************/
// Embodies a rotator specific transformation
//
class DDOARotatorXform : public DDOATransform {
public:
    DDOARotatorXform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym);

    // The following ctor is useful for Homogenous DDOs
    // The ctor uses the parameter values rather than digging into the
    // viewSym for the values. Essentially this is a clone.
    DDOARotatorXform (DDOARotatorXform *original, int targetIndex,
		      DDOADynamic *dyn);

    virtual int xformType();

        // On a UpdateView the rotator can check  specific resources.
        // The rotator must update the transform struct and changeMask_

    virtual void checkSubclassResources(Symbol *newView);

        // copy rotator specific resources

    virtual void copySubclassResources(DDOATransform *origXform);

protected:
    // interpret the Wpt resource values for pivot point into the proper
    // constant required by the TIV package.
    int     getPivotPoint (TEXT *hrotpt, TEXT *vrotpt);

    };

/***************************************************************************/
// D D O A D i s c r e t e X f o r m   C l a s s
/***************************************************************************/
// Embodies a discrete specific transformation
//
class DDOADiscreteXform : public DDOATransform {
public:
    DDOADiscreteXform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym);

    // The following ctor is useful for Homogenous DDOs
    // The ctor uses the parameter values rather than digging into the
    // viewSym for the values. Essentially this is a clone.
    DDOADiscreteXform (DDOADiscreteXform *original, int targetIndex,
		      DDOADynamic *dyn);

    virtual int xformType();

        // On a UpdateView the discrete can check  specific resources.
        // The discrete must update the transform struct and changeMask_

    virtual void checkSubclassResources(Symbol *newView);

        // copy discrete specific resources

    virtual void copySubclassResources(DDOATransform *origXform);

protected:
    // create picture threshs in xformStruct_ from theView (usually viewSym_)
    void buildPictureThresholds(Symbol *theView);

    // copy picture thresholds from one structure to my xformStruct_
    void copyPictureThresholds(const TIVTransformStruct &original);
    };

/***************************************************************************/
// D D O A S t r i p c h a r t X f o r m   C l a s s
/***************************************************************************/
// Embodies a stripchart specific transformation
//
class DDOAStripchartXform : public DDOATransform {
public:
    DDOAStripchartXform (DDOADynamic *dyn, int targetIndex, Symbol *viewSym);

    // The following ctor is useful for Homogenous DDOs
    // The ctor uses the parameter values rather than digging into the
    // viewSym for the values. Essentially this is a clone.
    DDOAStripchartXform (DDOAStripchartXform *original, int targetIndex,
			DDOADynamic *dyn);

    virtual int xformType();

        // On a UpdateView the stripchart can check  specific resources.
        // The stripchart must update the transform struct and changeMask_

    virtual void checkSubclassResources(Symbol *newView);

        // copy stripchart specific resources

    virtual void copySubclassResources(DDOATransform *origXform);

protected:
    };

#endif





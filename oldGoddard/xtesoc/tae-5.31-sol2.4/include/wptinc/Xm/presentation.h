/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


//
// 12-jul-91    PR1134 Changed MakeHelpAgent to call MakeWidgetAgent
//              if not an help item...tl,kw,ks 
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 09-dec-93	PR2518: Added Initialize method to initialize presentation
//              types...kbs,krw

#ifndef H_PRESENTATION
#define H_PRESENTATION

#include <widgetagent.h>

/*****************

Presentation objects for Wpt package. 

For each item presentation type, there is one instance of a
Presentation object that supplies services
for the presentation type, e.g., aPres->TypeOk(type)
tells the caller whether the target symbol "type" is ok for
the presentation.   Another important service of
a Presentation object is the actual creation of
WidgetAgent corresponding to the presentation.

The classes are named RadioP, ButtonP, ...
and the corresponding (single) instances are TheRadioPres,
TheButtonPres, ...

The instances are static objects--they are created
at Wpt initialization time (by Wpt_Init() call), or by directly invoking
the function wpt_entry().  During creation,  the instances
are linked together so that the mere existence of
an object module with a Presentation object causes
the presentation to be available.
The use of static objects avoids having a common 
"table" module that points to all of the presentation
objects.

The GetPresentation function returns a pointer to one
of the presentation instances given the name string
of the presentation, e.g., GetPresentation("Radio")
returns a pointer to TheRadioPres.

******************/


class Presentation;

Presentation *GetPresentation (char * presentationName);

class SymbolTable;
class WidgetAgent;

class Presentation 
{
	Presentation	*link;		// chain of static The's

public:
	Presentation();
	~Presentation () 		        // should never happen
	    {}			
	Presentation *Next()			// next pres type
	    {return link;}
	virtual char * TypeName() 
	    {abort(); return 0;}		// delegated to sub-class 
	virtual int ValidsRequired () 		
	    {return TRUE;}			// delegated to sub-class
	virtual int CountOk (int minc, int maxc) // is target's count ok ?
	    {minc=maxc; return TRUE;}
	virtual int TypeOk (int type)		// is type compatible?
	    {return type==type;}
	virtual int TargetRequired ()		// target PARM required?
	    {return TRUE;}
	virtual WidgetAgent * MakeWidgetAgent(   // Create an instance of 
		TEXT *name, WptPanel* panelId,   // the widgetagent
		Symbol *target, Symbol *view);
	virtual WidgetAgent * MakeHelpAgent(     // Create an instance of 
		TEXT *name, WptPanel* panelId,   // the widgetagent for help
		Symbol *target, Symbol *view)    // if not a help item do
						 // regular makewidgetagent
	    { return MakeWidgetAgent( name, panelId, target, view);}

//      NOTE: For "Adding a Presentation Type", the following function 
//	defined in $TAEWPT/presentRt.cc must be updated to call 
//	your "Init{presentationType}P" function (which is defined in 
//	your {presentationType}Rt.cc file).

	static void Initialize();		// Inits all presentations
};

#endif

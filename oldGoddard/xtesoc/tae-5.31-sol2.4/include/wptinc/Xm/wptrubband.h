/*
 * Copyright (c) 1987, 1988, 1989 Stanford University
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Stanford not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Stanford makes no representations about
 * the suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * STANFORD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL STANFORD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * Rubber - rubberbanding graphical objects
 */
// Change Log:
// 30-jun-93	Copied from IV 2.6.  Remove fillgc...cew
// 28-nov-94	Ultrix Port: Had to include decXlibfix.h for DEC C++ 
//		bug workaround...shj

#ifndef wptrubband_h
#define wptrubband_h

#ifdef __ultrix
#include <wptinc/Xm/decXlibfix.h>
#endif

#include <X11/Xlib.h>

enum Side { LeftSide, RightSide, BottomSide, TopSide };

class WptRubberband {
public:
    WptRubberband(Display*, Screen*, int offx, int offy);
    virtual ~WptRubberband();

    virtual void Draw();
    virtual void Redraw();
    virtual void Erase();
    virtual void Track(int x, int y);

    // from Painter
    void SetOverwrite(Bool children);
protected:
    float Angle(int, int, int, int);	// angle line makes w/horiz
    float Distance(int, int, int, int); // distance between 2 points
protected:
    Bool drawn;
    int trackx, offx;
    int tracky, offy;
    Display *display;
    Screen *screen;
    Window root;
    GC dashgc;

    // from Painter
    Bool overwrite;
};

#endif

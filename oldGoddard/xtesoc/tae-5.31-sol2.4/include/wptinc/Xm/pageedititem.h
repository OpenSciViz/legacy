/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Includfe file for WPT item presentation type PAGEEDIT

/****************************************************************************/
// 
// Change Log:
//
// 06-apr-89	Initial Release...dm
// 10-apr-89	Add CloseInput()...dm
// 10-sep-89	VMS port...ljn
// 24-aug-90    added setstate and sensitive...tpl
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92	PR1257: Use <> for #include files...ljn
// 21-jan-92    Added DefaultTraversalState...tpl
// 03-mar-92	Added UpdateView to update constraint error msg...cew
/*****************************************************************************/

#ifndef I_PAGEEDIT
#define I_PAGEEDIT 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <PageEdit.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/PageEdit.h>
#endif

// For PageEditA class
#define  PAGEEDIT_SENTINEL 0xFF1209FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "pageedit"

class PageeditP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}		// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "pageedit";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a PageeditWidget (as a WptItem)

class PageeditA : public WptItem {
protected:
    String * current;
    String * previous;
    int previousCount ;
    int currentCount ;
    void    Check() 
	{if (sentinel != PAGEEDIT_SENTINEL) abort(); }
    XtCallbackProc   executeCallbackPtr;  
    void *  executeCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
public:
    PageeditA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~PageeditA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int  DisplayNewValue(int newTarget);	// update value on display
    void *ConvertValue(TEXT **stringVal, TAEINT count,
	TEXT *errorMsg, TEXT *errorKey);	// convert string to data type
    void SetLastValInTarget();
    void SetTarget(void * valuePtr, TAEINT count );
    void CloseInput();
    Boolean DefaultTraversalState()
                                {return True;}
    int UpdateView (Symbol *newView);
    };
#endif


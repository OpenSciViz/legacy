/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*****************************************************************************/
// Header file for WPT presentation type Message.
//
// CHANGE LOG:
//
// 22-feb-92	Initial version...ljn
// 05-mar-92	Sun C++ 2.1 port...ljn
// 14-sep-92    PR1558: Message text no longer comes from title...ljn
// 20-oct-93	PR2421: Repaired a few compilation warnings...krw
/****************************************************************************/

#ifndef I_ACTIONMESSAGE
#define I_ACTIONMESSAGE 0

#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <MessageB.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/MessageB.h>		
#endif

// For MessageA class
#define  MESSAGE_SENTINEL  	0xFF1217FF 	

class   WptPanel;
class   WptItem;
class	Presentation;

// Presentation of type "message"
class MessageP : public Presentation {
public:
        int TypeOk (int type)
                {return type == V_STRING ;} // no V_REAL or V_INTEGER
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "message";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};

/************************************************************************/

//WidgetAgent representing a messageWidget (as a WptItem)

class MessageA : public WptItemDialog {
protected:

    void    Check() 
	{if (sentinel != MESSAGE_SENTINEL) abort(); }

    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;

    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    XmString textXmString;
    int origshadow;
    int origdefaultshadow;

public:
    MessageA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~MessageA();   
    void MessageSelect(void *callData);
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,  
		void * callData);	
    int DefaultArgs(Widget, XtkArg **);
    int UpdateView(Symbol *newView);
    virtual int SetState (int newState) 
    	{SetStatePropagate(newState); return 0;}
    virtual int SetState (int index, int newState);
    virtual int GetState (int index, int *newState);
    virtual int GetState (int *currentState)
	{ return WptItem::GetState(currentState);}
    int DefaultItemLook (int state, TEXT *component);
    Boolean DefaultTraversalState() {return True;} 
    };
#endif

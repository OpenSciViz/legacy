/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// CHANGE LOG:
// 25-jun-93    Initial implementation to support V5.3 DDOs. Originally
//              this was ooitem.h...krw
// 18-jul-93    Complete redesign and implementation of the Wpt side of 
//              DDOs...krw
// 06-aug-93    Added the Stripchart...krw
// 24-aug-93    Added extern of tivStatus for better error checking...krw
//              Removed valueVec_ from DDOA, unused...krw
// 09-sep-93	PR2153 improved UpdateView support for Wpt...krw
#ifndef I_DDOITEM
#define I_DDOITEM 0

#include <widgetagent.h>
#include <presentation.h>

#include <Xtae/TIV.h>

// include ddodynamic.h here because it MUST come BEFORE the rest of ddoitem.h
#include <ddodynamic.h>

extern TIVStatus tivStatus;   // TIV package internal global error status

/***************************************************************************/
// D D O   P R E S E N T A T I O N   S E N T I N E L S
/***************************************************************************/
// Discrete DDO item
#define  DISCRETE_SENTINEL  0xFF120BFF   
// Rotator DDO item
#define  ROTATOR_SENTINEL   0xFF120CFF   
// Stretcher DDO item
#define  STRETCHER_SENTINEL 0xFF120DFF   
// Mover DDO item
#define  MOVER_SENTINEL     0xFF120EFF   
// Stripchart DDO item
#define  STRIPCHART_SENTINEL 0xFF120FFF  
// Composite DDO item
#define  COMPDDO_SENTINEL   0xFF1210FF   

/*****************************************************************************/
// D D O A   c l a s s
/*****************************************************************************/
// Base of All Data Driven Object WptItem classes
// WidgetAgent representing all DDO items (as a WptItem)
// This class represents the DDOArea. It provides a number of common
// methods for use by the DDO type specific subclasses.
//
class DDOA : public WptItem {
public:
    TIVHandle areaHandle();

    void SetAppCallback(TEXT* callbackType,
                XtCallbackProc appCallbackProc, void * appCallbackData);

    // support v5.2 (and earlier) style input behaviour
    void DispatchCallback(Widget widgetId, TEXT * callbackType,
                void * callData) ;

    // used by threshold procesing to convert a color name to a pixel
    Pixel convertColor (Symbol *theView, int colorIndex);

    void checkStatus (TIVStatus status, TEXT *msgFormat);
    
    void flush();                               // flush the area.

//	Virtual Functions
    virtual ~DDOA();

    virtual int DisplayNewValue(int newTargetFlag);

    virtual int UpdateView (Symbol *newView);

    // Routine to handle user input on a dynamic
    virtual void FinishUserInput (DDOADynamic *dyn, XEvent *xevent);

protected:
    TIVHandle    areaHandle_;
    DDOADynamic  **dynamic_;                    // list of DDOADynamics
    int          numDynamics_;                  // number of dynamics
    TIVStatus    tivStatus_;                    // last reported status
    int          numPictures_;
    char         **pictureNames_;               // list of pictures in file

    unsigned long changeMask_;

    // KLUDGE to prevent double reporting of ButtonRelease events
    // Idea is to prevent DispatchCallback from processing if last
    // event a release on a dynamic.
    Boolean      lastWasUserInput_;             

    // one doesn't create a DDOA directly, use subclasses
    DDOA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);

    void cleanupDDO();				// prepare for rebuild or dtor
    void buildDDO(Symbol *view);		// to build/rebuild DDOArea

    void checkStatus (TEXT *msgFormat);           // use internal status

        // loop & create all dyns
    void buildDynamics(int dynamicsToBuild);

    // Specify the same usr input style for ALL dynamics.
    // typically called in a Homogenous DDO ctor
    void DefineUserInput();

private:
    XtCallbackProc appInputCallbackPtr;     // application callbacks if any
    void *appInputCallbackData;

    int BldWptArgList(Symbol *targetSym, Symbol *viewSym, Widget parentWidget);

    void DispatchToApp (XEvent *xevent);
};

inline
TIVHandle DDOA::areaHandle() {return areaHandle_;}

// General purpose check on and report errors from TIV calls
inline
void DDOA::checkStatus(TIVStatus status, TEXT *msgFormat) {
    tivStatus_ = status;
    checkStatus(msgFormat);
    }

inline
void DDOA::flush() { tivStatus_ = TIV_FlushArea (areaHandle_); }

// ***************************************************************************
//                         M O V E R A
// WidgetAgent representing a Homogenous mover DDO item (as a WptItem)
//
class MoverA : public DDOA {
public:
    MoverA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	

    virtual ~MoverA();

    // change the view of a MoverA. 
    virtual int UpdateView (Symbol *newView);

protected:

    void    Check(); 

    virtual void buildTransforms();

    // Build a Mover DDO, assumes buildDDO has been previously executed.
    virtual void buildMover();

private:
    Boolean mover2D_;                       // true if this is a 2D mover
    };

inline
void MoverA::Check() {if (sentinel != MOVER_SENTINEL) abort();}


// ***************************************************************************
//                    R O T A T O R
//WidgetAgent representing a rotator Homogenous DDO item (as a WptItem)
class RotatorA : public DDOA {
public:
    RotatorA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~RotatorA();

    // change the view of a RotatorA. 
    virtual int UpdateView (Symbol *newView);

protected:

    void    Check(); 

    virtual void buildTransforms();

    // Build a Rotator DDO, assumes buildDDO has been previously executed.
    virtual void buildRotator();
    };

inline
void RotatorA::Check() {if (sentinel != ROTATOR_SENTINEL) abort(); }

// ***************************************************************************
//                   S T R E T C H E R 
//WidgetAgent representing a stretcher Homogenous DDO item (as a WptItem)
class StretcherA : public DDOA {
public:
    StretcherA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~StretcherA();

    // change the view of a StretcherA. 
    virtual int UpdateView (Symbol *newView);

protected:

    void    Check(); 

    virtual void buildTransforms();

    // Build a Stretcher DDO, assumes buildDDO has been previously executed.
    virtual void buildStretcher();
    };

inline
void StretcherA::Check() {if (sentinel != STRETCHER_SENTINEL) abort(); }

/*****************************************************************************/
//                   D I S C R E T E
//WidgetAgent representing a discrete Homogenous DDO item (as a WptItem)
class DiscreteA : public DDOA {
public:
    DiscreteA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~DiscreteA();

    // change the view of a DiscreteA. 
    virtual int UpdateView (Symbol *newView);

protected:

    void    Check(); 

    virtual void buildTransforms();

    // Build a Discrete DDO, assumes buildDDO has been previously executed.
    virtual void buildDiscrete();
    };

inline
void DiscreteA::Check() {if (sentinel != DISCRETE_SENTINEL) abort(); }





/*****************************************************************************/
//                   C O M P D D O
//WidgetAgent representing a Composite Heterogenous DDO item (as a WptItem)
class CompddoA : public DDOA {
public:
    CompddoA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~CompddoA();

    // change the view of a CompddoA. 
    virtual int UpdateView (Symbol *newView);

protected:
    Symbol **dynViewSym_;              // array of views matched to dynamics

    void    Check(); 

    void findViewSyms(Symbol *newView); // locate the views for each dynamic

    void DefineCompositeUserInput(); // determine user input attr. for each dyn

    virtual void buildTransforms();

    // Build a Compddo DDO, assumes buildDDO has been previously executed.
    virtual void buildCompddo();
    };

inline
void CompddoA::Check() {if (sentinel != COMPDDO_SENTINEL) abort(); }


// ***************************************************************************
//                   S T R I P C H A R T
//WidgetAgent representing a stripchart Homogenous DDO item (as a WptItem)
class StripchartA : public DDOA {
public:
    StripchartA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~StripchartA();

    // change the view of a StripchartA. 
    virtual int UpdateView (Symbol *newView);

protected:

    void    Check(); 

    virtual void buildTransforms();

    // Build a Stripchart DDO, assumes buildDDO has been previously executed.
    virtual void buildStripchart();
    };

inline
void StripchartA::Check() {if (sentinel != STRIPCHART_SENTINEL) abort(); }

#endif





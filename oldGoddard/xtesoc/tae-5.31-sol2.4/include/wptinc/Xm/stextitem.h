/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type STATIC (Static Text)
//
// Change Log:
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92	PR1257: Use <> for #include file names...ljn

#ifndef I_STATICTEXT
#define I_STATICTEXT 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <Label.h>
#include <LabelG.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#endif

// For StaticTextA class
#define  STEXT_SENTINEL  0xFF1207FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "static"
class StaticTextP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "static";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a Static text Widget (as a WptItem)

class StaticTextA : public WptItem {
protected:
    void    Check() 
	{if (sentinel != STEXT_SENTINEL) abort(); }
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    int DefaultArgs(Widget, XtkArg **);

public:
    StaticTextA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~StaticTextA();
    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
		void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);
    int UpdateView(Symbol *newView);
    };
#endif


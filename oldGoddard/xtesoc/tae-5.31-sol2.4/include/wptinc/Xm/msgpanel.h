/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/********************************************************************
 *
 * msgpanel.h	Defines class structure for dialog and msg boxes.
 *
 *  CHANGE LOG:  
 *
 *  18-mar-92	Provide option to MessageBox to busy wait (pre v5.2 behavior),
 *		or not busy wait (and possibly send WPT_MESSAGE_EVENT to app)
 *  20-mar-92	Changed MessageBox() arg from VOID to void...ljn
 *  30-mar-92	Multiple Display support.  Added ctor to create MessageBox
 *		relative to root window....cew
 *  03-apr-92   Changed from TEXT to const TEXT for message arguments...tpl
 *
 ********************************************************************/

#ifndef I_MSGPANEL
#define I_MSGPANEL  0


// WPT specific definitions
#include	<wptdefs.h>		
#include	<taeconf.inp>
// function declarations
#include	<declare.h>		


class MessageBox{
protected:
    int		sentinel;
    Widget	topWidget;		// shell Widget Id
    Widget	msgboxWidget;		// dialog widget Id
    Widget	relativeWidget;		// put msg panel relative to this
    Widget	contextWidget;		// use this for color inheritence
    Screen	*screen;		// on this screen
    Display	*display;		// on this display

//private 
    void    Check() 		// Integrity Check
	{if (sentinel != MSGBOX_SENTINEL) abort(); }
    int   BldShellArgList(XtkArg *argList);
    int  BldMsgboxArgList(const TEXT * message, XtkArg *argList);
    void CreateMsgPanel (const TEXT *message, BOOL wait, void *noWaitData,
			 BOOL returnToApp);
public: 				
// External functions: 
    // relative to a widget
    MessageBox ( const TEXT *message, Widget contextWidget = nil, 
		Widget relativeWidget = nil,
		BOOL wait = TRUE,	      // TRUE = busy wait
		void *noWaitData = nil,	      // data to return, if not wait
		BOOL returnToApp = FALSE);    // send WPT_MESSAGE_EVENT to app
    // relative to the root window of a screen
    MessageBox ( const TEXT *message, Screen *scr,
		BOOL wait = TRUE,	      // TRUE = busy wait
		void *noWaitData = nil,	      // data to return, if not wait
		BOOL returnToApp = FALSE);    // send WPT_MESSAGE_EVENT to app
    ~MessageBox ();
    Widget WidgetHandle() {return topWidget;}
};
#endif


/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT item presentation type TEXTDISP. 

//
// Change log:
//
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92	PR1257: Use <> for #include file names...ljn

#ifndef I_TEXTDISPLAY
#define I_TEXTDISPLAY 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <PageEdit.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/PageEdit.h>
#endif

//For TextDispA class
#define TEXTDISP_SENTINEL  0xFF1208FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "textdisp"
class TextdispP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return FALSE;}
	TEXT * TypeName () 
		{return "textdisp";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a buttonWidget (as a WptItem)

class TextdispA : public WptItem {
protected:
    XtCallbackProc	selectCallbackPtr;
    void		*selectCallbackData;

    void    Check() 
	{if (sentinel != TEXTDISP_SENTINEL) abort(); }
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);
    int DefaultArgs(Widget, XtkArg **);

public:
    TextdispA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~TextdispA();

    void SetAppCallback(TEXT* callbackType, XtCallbackProc appCallbackProc,
                void * appCallbackData);
    void DispatchCallback(Widget widgetId, TEXT* callbackType,
                void * callData);
    int UpdateView (Symbol *newView);
    };
#endif


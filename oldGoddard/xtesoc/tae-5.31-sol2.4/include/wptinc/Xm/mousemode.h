/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//	Encapsulation of Mouse Mode flag.  
//	Only one instance expected (declared below).
// Change Log:
// 21-jun-89	GNU defaults to treating 'const' as 'extern const'...ljn
// 10-dec-93	PR2518: Changed use of Mousemode to pointer...krw/rt
//

#ifndef mousemode_h
#define mousemode_h 0


static const CONNECTION = 1;
static const MANIPULATE = 2;
static const OPERATE =    3;
static const HELP = 4;

class MouseModeClass  {
	int  modeFlag;
public:
	MouseModeClass (int iniMode = OPERATE) {modeFlag = iniMode;}
	int Connection () {return modeFlag == CONNECTION;}
        int Manipulate () {return modeFlag == MANIPULATE;}
	int Operate ()    {return modeFlag == OPERATE;}
	void SetConnection () {modeFlag = CONNECTION;}
	void SetManipulate () {modeFlag = MANIPULATE;}
	void SetOperate ()    {modeFlag = OPERATE;}
};


extern MouseModeClass *MouseMode;
#endif

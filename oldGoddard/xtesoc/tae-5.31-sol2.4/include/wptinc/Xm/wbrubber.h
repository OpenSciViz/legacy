/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// CHANGE LOG:
// 19-jun-89	To satisfy g++, made WbRect(), GetCurrent() public...ljn
// 10-sep-89	VMS port...ln
// 01-jul-93	Extricate IV2.6.  Use WptRubberband instead of IV rubberband.
//		Remove Canvas, Painter (substitute with Display and Screen).
//		Change Coord to int...cew

#ifndef I_WBRUBBER 
#define I_WBRUBBER 

#include	<wptrubband.h>
#include	<wptrubrect.h>

//	WB's own version of Stetching
//	(to get reference point)

class WbStretch : public WptRubberRect {
protected:
    Side side;
    int rfx, rfy;		// original event point
public:
    WbStretch (
        Display *, Screen *, int x0, int y0, int x1, int y1, Side s,
	int rfx, int rfy);
    void GetCurrent(int& x0, int& y0, int& x1, int& y1);
};


//	WB's own version of RubberRect
//	(to get reference point)

class WbRect : public WptRubberRect {
	int	rfx, rfy;
public:
    WbRect (Display *d, Screen *, int x0, int y0, int x1, int y1,
	int rfx, int rfy);
    void GetCurrent (int &x0, int &y0, int &x1, int &y1);
};

#endif

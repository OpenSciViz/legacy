/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// 
// Function declaration for functions referenced by WPT and Workbench
//
// Change Log:
//
// 24-jan-90	Add 'extern "C"', stdlib.h for g++ 1.36 port...ljn
// 12-feb-90	Add s_index, s_lseq, and strpqu declarations...ljn
// 07-may-90    Add WptAppWaitForEvent (formerly, XtAppWaitForEvent())...ljn
// 07-may-90    No more Xtinc directory; use system X header files...ljn
// 19-sep-90	Added s_substring(), f_* routines...ljn
// 18-oct-90    Added f_libr, f_opnblk.  Fixed return type of s_bcopy...cew
// 22-oct-90	"Fast" panel support: Xt_CreatePanelShellWidget() pulled from
//		Xt_CreatePanelWidget(); also moved some X_*() decl's here...ljn
// 16-dec-90	Added border to Xt_ResizeWidget call, now consistent with
//		real widget routine...krw
// 06-feb-91	If doing FAST_MALLOC, don't prototype tae_alloc...ljn
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 06-jan-92	Use ... for x_error so error.cc compile does not complain...ljn
// 04-mar-92	Fix previous fix...ljn
// 30-mar-92	Multiple Display support.  Added WptDisplay, FindWptDisplay,
//		WptPanelError, WptAllowedPanelEvent.  Added Screen * arg to
//		WptError, and Xt_CreatePanelShellWidget...cew
// 02-apr-92	New Xt_LowerWidget declaration...crb
// 06-apr-92    Moved most function prototype to wptinc.inp and included
//		wptinc.inp...tpl
// 10-apr-92	Add aPainter, aState, world to WptDisplay for Multiple
//		Display support for DDOs...cew
// 22-apr-92	Args added to Xt_CreatePanelWidget for scrolled panel...kbs
// 03-oct-92	PR 1626: New args to Xt_CreateSubPanelWidget for scrolled
//		child panels...crb
// 05-oct-92	Fix C++ anachronisms...rt
// 01-jul-93	Extricate IV2.6.  Remove painter, state, worldrep includes.
//		Use ivdpyInfo (handled by oo.cc) to insulate WptDisplay from
//		IV.  #define XCursor...cew

#ifndef I_DECLARE
#define I_DECLARE  0

#include	<taeconf.inp>
#include	<wptinc.inp>

#ifdef __cplusplus
// InterViews-supplied std lib decl's
#include 	<stdlib.h>		
extern "C" 
{
#ifdef VMS
#include 	<Intrinsic.h>
#else
extern "C"
{
#include 	<X11/Intrinsic.h>	
}
#endif
}
#include	<resconvert.h>
#include	<slist.h>

#ifndef Coord
#    define Coord int
#endif
#ifndef nil
#define nil	NULL	
#endif

#define XCursor Cursor

typedef struct
    {
    Display	*dpy;
    Widget	modalWidgetId;		   // Id of modal widget
    slist	wptMessageList;		   // List of non-blocking msgs
    XCursor	defWaitCursor;		   // wait cursor
    void	*ivdpyInfo;		   // information needed for IV
    } WptDisplay;

WptDisplay *FindWptDisplay (Display *display);

// TAE library functions:
extern "C"
{
        void x_error (int mode, TEXT *msg, TEXT *key,
                        void *a1=nil, void *a2=nil, void *a3=nil);
    	void  bytmov(void *, void *, int);
	CODE f_write (SFILE *, TEXT *);
	CODE f_opnspc (SFILE *,TAEINT,TEXT *,TEXT *, TEXT *, TEXT *,TAEINT);
	CODE f_close (SFILE *, TAEINT);
	CODE f_ophf( SFILE *,TAEINT,TEXT *,TAEINT);
	CODE f_opnblk( SFILE *,TAEINT, FSBLOCK *, TAEINT);
	CODE f_crack(TEXT *,TEXT *,TEXT *,TEXT *, FSBLOCK *, TEXT *);
	COUNT f_spec (FSBLOCK *, TEXT *);
	CODE f_read (SFILE *, TEXT *);
}

	class Symbol;
	class SymbolTable;


	void GetMasterFontsColors (SymbolTable *st);
	void CopyToQual (Symbol *source, Symbol *dest);
	void CopyToSt (Symbol *s, SymbolTable *st);
        inline int Abs(int a) {return (a < 0) ? -a : a;}
	inline void concat(TEXT* a, TEXT* b, TEXT* c) 
			{s_copy(a, c); s_append(b, c);}

extern "C"
{
// XToolkit  Specific Wpt functions

    void Xt_RedrawWidget(Widget);
    void Xt_AllowSubEvents(Widget widget, Boolean top, 
		Boolean allow, EventMask mask);
    EventMask Xt_RequestMotion(Widget widget);

    Widget Xt_CreatePanelShellWidget(TEXT* name, Window parentWindow,
                Screen *screen, XtkArg*  argList, int numArgs);
    // Create TAE Box widget with or without scrolled window as parent.
    Widget Xt_CreatePanelWidget(TEXT* name, XtkArg* argList,
                int numArgs, Widget toplevel, 
		Widget    *pSWwidget, int PanelScrollbars,
                Dimension *ScrWinPad_Width, Dimension *ScrWinPad_Height,
                TAEINT WorkWinWidth, TAEINT WorkWinHeight );
    Widget Xt_CreateSubPanelWidget(TEXT* name, Widget parentWidget,
		XtkArg *argList, int numArgs, 
		Widget *toplevel,
		Widget *pSWwidget, int PanelScrollbars,
                Dimension *ScrWinPad_Width, Dimension *ScrWinPad_Height,
                TAEINT WorkWinWidth, TAEINT WorkWinHeight );
    Widget Xt_CreateTitleWidget(TEXT *name, Widget panelWidget,
		XtkArg *titleArgs, int numTitleArgs);

    void Xt_ChangeOffsets(XtkArg, int, int, int);
    void Xt_MoveWidget(Widget widget, Coord x, Coord y, Boolean top);
    void Xt_ResizeWidget(Widget widget, Coord x, Coord y,
		Coord width, Coord height, Coord border, Boolean top);
    int  Xt_SetWidgetSize(Widget widget, XtkArg *argList, int numArgs);
    int  Xt_GetResource(XtkArg *argList, int numArgs,
                TEXT *resName, XtArgVal *value);
    void Xt_DestroyWidget(Widget widget);
    void Xt_LowerWidget(Widget widget);
    void Xt_RaiseWidget(Widget widget);
    void Xt_ReparentWidget(Widget widget, Window parentWindow);
    int  Xt_DefineCursor(Widget widget, XCursor cursor = nil);
    XCursor  X_CreateCursor (Screen *, int, int,
                     char [], char [], int, int, int);

    void X_DiscardCurEvents(Display *display);
    int  X_LookupKeyMap(XKeyEvent *keyEvent, TEXT *buffer,
                int bytes, int *keyCode);
    void X_PanelHighlight(Display *display, Window window);
    void X_PanelUnHighlight(Display *display, Window window);
    void X_ItemHighlight(Display *display, Window window);
    void X_ItemUnHighlight(Display *display, Window window);
    void X_RequestMotion (Display *, Window);
    void X_XorLine(Display *, Window, int, int, int, int);
    void RedrawExposedWidgets(Display *display);
    void SetSolidXor (Display *);

    CODE    WptAppWaitForEvent(XtAppContext appContext,
                XEvent *event, unsigned long *intervalPtr,
                Boolean noInterrupt, int *eventSource, int *eventMask);
    void WptError(Screen *, CODE, TEXT *, TEXT * ...);
    void WptPanelError(Id, CODE, TEXT *, TEXT * ...);
    BOOL WptAllowedPanelEvent (XEvent xEvent);
}
#endif
/**********************/


/*	fast, case insensitive string compare...  */

extern TEXT s_table[];			/* upper case conversion table */

#ifndef STR_EQUAL
#define STR_EQUAL(s1, s2) \
	(s_table[*(s1)] == s_table[*(s2)] ? s_equal(s1, s2) : FALSE)
#endif
#endif

/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// Include file for WPT presentation type SCALE (SLIDER).
//
// CHANGE LOG:
// 04/20/90	created based on checkboxitem.h...kbs
// 04/26/90	added declaration for UpdateView...kbs
// 09-jul-90	removed OSFX11/ ...ljn
// 13-aug-90	removed HELP and SetState code...kbs
// 19-sep-90	added SetState back for dimming (so sue me!)...kbs
// 13-nov-90    added DefaultShadowThickness...tpl
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 10-jan-92	Removed DefaultArgs...tpl
// 21-jan-92    Added DefaultTraversalState...tpl
// 18-feb-92    Define SetState to use SetStatePropagate...cew
// 05-oct-92	Fix C++ anachronisms...rt
// 06-oct-92	Typo caused this not co compile...rt

#ifndef I_ACTIONSCALE
#define I_ACTIONSCALE 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <Scale.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Scale.h>		
#endif

// For ScaleA class
#define  SCALE_SENTINEL  	0xFF1213FF	

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "scale"
class ScaleP : public Presentation {
public:
	int TypeOk (int type) 
		{return (type == V_INTEGER || type == V_REAL) ;}  // no V_STRING

// TBD: do we want min & max to come from Scale Pres. panel or Constraints?
// If Scale Presentation panel, ValidsRequired = False.
	int ValidsRequired ()
		{return FALSE;}

	int TargetRequired ()
		{return TRUE;}

	TEXT * TypeName () 
		{return "scale";}

	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a scaleWidget (as a WptItem)

class ScaleA : public WptItem {
protected:

    void    Check() 
	{if (sentinel != SCALE_SENTINEL) abort(); }

    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;

    XtCallbackProc   releaseCallbackPtr;  
    void *  releaseCallbackData;

    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);


    // Handle scale-specific resources not handled by CommonViewArgs()
    int UpdateView (Symbol *newView ); 	

public:
    ScaleA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    ~ScaleA();   

    // Use override for subclasses of scales (help scales)
    virtual void ScaleSelect(void *callData);	
    virtual void ScaleRelease(void *callData);	

    virtual void SetAppCallback(TEXT* callbackType, XtCallbackProc 
		appCallbackProc, void * appCallbackData);

    virtual void DispatchCallback(Widget widgetId, TEXT* callbackType,
		void * callData);

    int SetState (int newState)
	{return SetStatePropagate(newState);}
    int SetState (int index, int newState)
                {index=index; newState=newState; return 1;}

    int  DisplayNewValue(int newTarget);        // display new value in target

    int DefaultShadowThickness () { return 2; }
    Boolean DefaultTraversalState()
                                {return True;}
    };
#endif

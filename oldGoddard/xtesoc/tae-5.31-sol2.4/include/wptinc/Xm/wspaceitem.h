/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//Include file for WPT item presentation type WORKSPACE
//Change Log:
// 08-jun-89	Needed the WorkspaceA dtor to be virtual...krw
// 10-sep-89	VMS port...ljn
// 12-jan-90	Added releaseCallbackPtr & releaseCallbackData for ButtonRelease...kbs
// 31-aug-90	Added enterCallbackPtr & enterCallbackData for MOTIF...kbs
// 08-oct-90	Added resizeCallbackPtr & resizeCallbackData for WB...krw
// 18-nov-90    Removed SetState which is handle by agentRt now...tpl
// 29-nov-90    Removed all non-MOTIF ifdefs...kbs
// 29-nov-90	Virtual function SpecifyCallbacks added to base class;
//         	also made SetAppCallback & DispatchCallback virtuals..kbs,krw
// 28-mar-91	Header files for VMS...ljn
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 00-feb-92    Added ScrolledW.h, WorkspaceScrollbars, SetScrollbarColors,
//              UpdateView, BldScrolledWinWptArgList, BldWorkspaceWptArgList;
//              Added Finish and ValidateSize (override WptItem);
//              Added ScrWinPad_Width, Height to compute max size...kbs

#ifndef I_WORKSPACE
#define I_WORKSPACE 0


#include <widgetagent.h>
#include <presentation.h>
#ifdef VMS
#include <Xatom.h>
#include <StringDefs.h>
#include <Shell.h>
#include "xm.h"
#include <DrawingA.h>
#include <ScrolledW.h>
#else
#include <X11/Xatom.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/DrawingA.h>
// need if scrollable workspace
#include <Xm/ScrolledW.h>	
#endif

// For WorkspaceA class
#define  WORKSPACE_SENTINEL  0xFF1205FF		

class   WptPanel;
class   WptItem;
class	Presentation;


// Presentation of type "workspace"
class WorkSpaceP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;}			// anything goes
	int ValidsRequired ()
		{return FALSE;}
	int TargetRequired ()
		{return TRUE;}
	TEXT * TypeName () 
		{return "workspace";}
	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};


/************************************************************************/

//WidgetAgent representing a workspace item (as a WptItem)

class WorkSpaceA : public WptItem {
protected:
    void    Check() 
	{if (sentinel != WORKSPACE_SENTINEL) abort(); }

//  callback pointers and data from application (future)
    XtCallbackProc   selectCallbackPtr;  
    void *  selectCallbackData;
    XtCallbackProc   releaseCallbackPtr;  
    void *  releaseCallbackData;
    XtCallbackProc   exposeCallbackPtr;  
    void *  exposeCallbackData;
    XtCallbackProc   keyDownCallbackPtr;  
    void *  keyDownCallbackData;
    XtCallbackProc   motionCallbackPtr;  
    void *  motionCallbackData;
    XtCallbackProc   leaveCallbackPtr;  
    void *  leaveCallbackData;

//  Added enterCallbackPtr and resizeCallbackPtr for MOTIF
    XtCallbackProc   enterCallbackPtr;  
    void *  enterCallbackData;
    XtCallbackProc   resizeCallbackPtr;  
    void *  resizeCallbackData;

//  BldWptArgList is the v5.1 Workspace-only version 
//  and v5.2 and later non-scrollable workspaces
    int BldWptArgList(Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);

//  BldScrolledWinWptArgList is for the scrolled win parent of workspace
    int BldScrolledWinWptArgList (Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);

//  BldWorkspaceWptArgList is for the child workspace of scrolledwin
    int BldWorkspaceWptArgList (Symbol *targetSym, Symbol *viewSym,
		Widget parentWidget);

//  Handle scrolled wspace-specific resources not handled by CommonViewArgs()
    int UpdateView (Symbol *newView );

//  Set by constructor indicating whether workspace is scrollable
//  (scroll None, Always or As Needed)
    int WorkspaceScrollbars ; //WPT_SCROLLBARS_NONE, WPT_SCROOLLBARS_ALWAYS, WPT_SCROOLLBARS_AS_NEEDED

//  Changes scrollbar background based on scrolled win background.
    void SetScrollbarColors ( Widget scrwin );

//  Constrain max item size to workwinSize + ScrWinPad_*
//  See ValidateSize and CalcScrWinPadding 
    Dimension       ScrWinPad_Width, ScrWinPad_Height;

//  Extra widget for parent of workspace iff scrollable
    Widget SWwidgetId ;
//  DrawingArea widget 
    Widget DAwidgetId ;

public:
    WorkSpaceA (TEXT * name, WptPanel *panelId, Symbol* targetSym, 
		Symbol *viewSym);	
    virtual ~WorkSpaceA();

//  Subclasses such as OutputObject and non-DDO Dynamic Text will override
//  the workspace's SpecifyCallbacks (and related) routines

    virtual void SpecifyCallbacks ( );

    virtual void SetAppCallback(TEXT* callbackType, 
		XtCallbackProc appCallbackProc, void * appCallbackData);

    virtual void DispatchCallback(Widget widgetId, TEXT * callbackType, 
		void * callData);

    virtual void Finish (); // override WptItem's

//  agentWb.cc:Resize calls ValidateSize
    virtual void ValidateSize (Coord width, Coord height, /* requested size */
        Coord *validWidth, Coord *validHeight /* max allowed size */ );

    int DefaultBorderWidth() { return 1; }
    };
#endif


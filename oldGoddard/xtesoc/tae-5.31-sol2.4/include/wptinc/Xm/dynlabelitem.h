/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



// CHANGE LOG:
// 15-oct-90	created based on ooitem.h...kbs
// 16-oct-90	subclass of OutputObject rather than WorkSpace...kbs,krw
// 23-oct-90	major rewrite of class definition...kbs,krw
// 26-oct-90	expanded ctor and split into modules...kbs
// 17-nov-90	prevent V_STRING datatype for now...kbs,krw
// 18-nov-90    Removed SetState which is handle by widgetagent now..tpl
// 15-mar-91	Make dynlabel more compatible with CommonViewArgs...krw
// 14-may-91	Added PreviousFGPixel, FGcolorST, FGcolorViewSym...kbs
// 15-may-91	Added SetState to override virtual for dimming...kbs
// 03-jan-92    PR1257: Use <> for #include file names...ljn
// 20-aug-92	PR1575: enable String type for dynamic text...cew
// 01-jul-93	Extricate IV2.6.  Don't look for math.h in IV...cew
// 13-jul-93	Since OutputObjectA is being reimplemented for DDOs, change
//         	base class to WorkspaceA; make more methods virtual...kbs
// 14-jul-93	Remove InterViews defines; include wspaceitem.h, 
//         	not ooitem.h...kbs,cew

#ifndef I_DYNLABELITEM
#define I_DYNLABELITEM 0
#endif

// superclass
#include <wspaceitem.h>			
#include <widgetagent.h>
#include <presentation.h>

// for abs, fabs
#include <math.h>			

// Dynamic Text (NOT DDO "dyntext")
#define  DYNLABEL_SENTINEL   0xFF1214FF  
                                         // For DynLabelA class

#define stipple_width 16           /* for dimming; see stipple_bits */
#define stipple_height 16

class   WptPanel;
class   WptItem;


// Dynlabel is subclass of the class WorkSpaceA 

class DynLabelP : public Presentation {
public:
	int TypeOk (int type) 
		{return type==type;} // all OK: V_REAL, V_INTEGER, V_STRING

	char * TypeName () 
		{return "dynlabel";}		// not DDO "dyntext"

	int ValidsRequired() {return FALSE;}	//    CHANGE ???

	int TargetRequired() {return TRUE;}  

	WidgetAgent * MakeWidgetAgent(TEXT * name, WptPanel* panelId, 
			Symbol *target, Symbol *view);
};

/************************************************************************/

// WidgetAgent representing dynlabel as a WptItem

class DynLabelA : public WorkSpaceA {

protected:

    // Special X info for DynLabel (needed for redrawing)
    Window	DynLabel_win ;		// X Window id
    GC		DynLabel_gc ;		// Graphic Context
    Display	*DynLabel_dpy ;		// display
    XFontStruct *DynLabel_fontinfo ;	// not fontname
    // Colormap    DynLabel_cmap ;		// colormap

    Position	xBox, yBox ;		// coords/size of bounding box 
    Dimension   wBox, hBox ;		// which is winsize - shadow
    Dimension WSwidth, WSheight;	// total workspace size

    int		shadow ;		// shadowThickness
    Position	xTextPos, yTextPos ;	// text position in workspace
    int		sLength ;		// char length of text to display
    unsigned long PreviousFGPixel;	// remember last pixel in ChangeFGColor

    // end Special X info for DynLabel

    TAEINT	TargetType ;	// UpdateView needs to know data type
    SymbolTable *FGcolorST;		// for ChangeFGColor
    Symbol	*FGcolorViewSym;	// for ChangeFGColor

    // from item spec panel viewSym or Details panel
    unsigned long defaultFGColor;	// fg pixel color
    unsigned long defaultBGColor;	// bg pixel color
    XFontStruct *defaultFont;
    TAEINT	justification;	// 0=Left, 1=Center, 2=Right
    TAEINT	margin;		// in pixels, for Left or Right justif

    // Currently displayed value (data type dependent).
    // Needed to compare against delta to decide whether to update screen.

    TAEFLOAT	rdelta;		// real update delta (int/real target only)
    TAEINT	idelta;		// int update delta (int/real target only)
    BOOL	HAS_A_VALUE;	// don't reference *Current if flag is false

    TAEINT	intCurrent;
    TAEFLOAT	realCurrent;
    // don't need stringCurrent since delta meaningless
    // TAEINT	indexCurrent;	// array index of *Current, or current tag

    // parallel arrays (lists); based on Vector Values in symbol.h
    TAEINT	numElts ;	        // actual size of these arrays
    TEXT	**colorList ;		// colornames for diff thresholds
    TEXT	**displayStrings ;	// strings to display for threshold

    // formattedString is displayString[i] with current target value inserted;
    // This is what is actually displayed in the DynLabelA workspace.
    TEXT	formattedString[STRINGSIZ+1]; 

    TAEINT 	*intThresholds ;	// if type=V_INTEGER
    TAEFLOAT 	*realThresholds ;	// if type=V_REAL
    TEXT 	**stringTags ;		// if type=V_STRING

    // current string to display with formating and value
    char	formatString[STRINGSIZ+1];

    void	Check()
    		{if (sentinel != DYNLABEL_SENTINEL) abort(); }

    // SPECIAL SUBCLASS FUNCTIONS

    void Get_TAE_Resources (TEXT *name, WptPanel *panelId, 
			Symbol *targetSym, Symbol *viewSym);
    void Set_Xlib_Env  (TEXT *name, WptPanel *panelId, 
			Symbol *targetSym, Symbol *viewSym);

    void ChangeFGColor ( TEXT * FGcolor );                 // changes only fg 
    void ChangeFGColor ( unsigned long FGcolor );          // changes only fg 
    void ChangeColors  ( unsigned long FGcolor, 
			 unsigned long BGcolor );  // changes fg  & bg

    void PositionText ( TEXT* dynamictext );

    void UpdateTarget (Symbol * newTargetSym );  // called by Wpt_ParmUpdate
    void DisplayValue (int newTarget);        // display new value in target
    void DisplayValue (TAEFLOAT newTarget);   // display new value in target
    void DisplayValue (TEXT * newTarget);     // display new value in target

#ifdef EXTRA_DEBUG
    void DumpViewInfo ( TEXT* itemName, int targetType );  // DEBUG
#endif

    // Inherited: 
    // int BldWptArgList (Symbol *targetSym, Symbol *viewSym, ...
    // int DefaultArgs(Widget, XtkArg **);


public:
    DynLabelA (TEXT *name, WptPanel *panelId, Symbol *targetSym, 
		Symbol *viewSym);

    virtual ~DynLabelA();

    // Instead of DisplayNewValue, Wpt_ParmUpdate calls UpdateTarget
    // which calls the appropriate DisplayValue.
    virtual int DisplayNewValue(int flag) 
	{
	flag=flag; 
	Symbol * newTargetSym = TargetSym();
	UpdateTarget ( newTargetSym ); 
	return 1;
	}

    // Handle dynlabel-specific resources not handled by CommonViewArgs()
    virtual int UpdateView (Symbol *newView );

    //  Subclasses of workspace must override
    //  the workspace's SpecifyCallbacks (and related) routines

    void SpecifyCallbacks ( ) ;

    void SetAppCallback(TEXT* callbackType,
                XtCallbackProc appCallbackProc, void * appCallbackData);

    void DispatchCallback(Widget widgetId, TEXT * callbackType,
                void * callData) ;

    // SPECIAL SUBCLASS FUNCTIONS

    virtual void ReExpose();	// redraw item with possibly different color

    // override virtual; need to do our own dimming since not a widget
    int SetState (int newState);
    int SetState (int index, int newState)
                {index=index; newState=newState; return 1;}
};

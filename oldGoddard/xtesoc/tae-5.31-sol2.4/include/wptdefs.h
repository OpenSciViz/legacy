/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/***************************************************************************
 *
 *	Include file for TAE_PLUS Window related WPT definitions. 
 *
 ****************************************************************************
 *
 *  CHANGE LOG:
 * 
 * 17-dec-88	Initial Release, for XToolkit architecture of WPT 
 * 04-may-88	Added Mover Sentinel...krw
 * 20-may-89	Move each WptItem subclass sentinel to their own header 
 *			files (for ease of adding new presentations)...dm
 * 22-may-89	Add WPT_SUBPANEL and other symbols for facelift...dm
 * 03-oct-89       Add WPT_MODALPANEL...tl 
 * 07-may-90	c_plusplus is now __cplusplus...ljn
 * 01-oct-90	WPT_ICONICPANEL, WPT_FASTPANEL...ljn
 * 23-oct-90	WPT_VISIBLEPANEL, WPT_INVISIBLEPANEL, WPT_DEFAULT...ljn
 * 09-nov-90	Dropped `PANEL' part of panel constants; added 
 *			WPT_PREFERRED, WPT_DELETED...ljn
 * 13-nov-90	Make WPT_FAST -> WPT_FASTICONIC...ljn
 * 19-feb-92	Removed PANEL_SCROLL (Facelift)...kbs
 * 22-feb-92	Defines for new Message pres and general ones for Dialogs...ljn
 * 18-mar-92	Added MESSAGEEVENT_SENTINEL...cew
 * 25-mar-92	Added WPT_SCROLLBAR* constants; removed many unused constants:
 *         	BUTTON_BORDER_WIDTH
 *         	DEF_WPT_WSHEIGHT
 *         	DEF_WPT_WSWIDTH
 *         	HIGHLIGHT_SIZE
 *         	MAXTITLE_STRINGS
 *         	PARM_EVENT (as opposed to WPT_PARM_EVENT)
 *         	REALFORMAT
 *              SCROLLPANEL_SENTINEL
 *         	TITLEBAR_LEN
 *         	WPT_ERRABORT
 *         	WPT_HELP_ITEM
 *         	WPT_HELP_PANEL
 *         	WPT_INPUT_ITEM
 *         	WPT_MAXHELPPNUM
 *         	WPT_MAXSUBPNUM
 *         	WPT_NOPADDING
 *         	WPT_NULL_EVENT
 *         	WPT_OUTPUT_ITEM
 *         	WPT_PANEL_EVENT...kbs
 * 29-apr-92	Addition of declarations for WPT_ACCEL, WPT_SEP and 
 *		WPT_MENUENTRY structures and constants...krw
 * 31-jul-92	PR1567: Added WPT_MENU_STATICLABEL_TYPE and declared
 *		WPT_MENU_LABEL_TYPE to be obsolete...krw
 * 29-sep-92	PR1115: Add TIMEREVENT_SENTINEL for WPT_TIMER_EVENT
 ******************************************************************************/

#ifndef  I_WPTDEFS
#define  I_WPTDEFS  0

#include <taeconf.inp>

#define  WPT_FATALERROR 2		/* quit due to unrecoverable error */ 
#define  WPT_WARNING    3		/* warning for recoverable error  */ 

/* Wpt panel creation flags  */ 
#define  WPT_PREFERRED  0x0000		/* use `preferred state' resfile info */
#define  WPT_CHILD      0x0001		/* a child of a given window */	   
#define  WPT_ACTION     0x0002		/* events for all target parms */     
#define  WPT_MANIPULATE 0x0004		/* panel in manipulation mode      */
#define  WPT_SUBPANEL   0x0008		/* panel as a subpanel of another */
#define  WPT_NOSHOW     0x0010
#define  WPT_MODALPANEL 0x0020
#define  WPT_ICONIC 	0x0040		/* iconified state*/
#define  WPT_FASTICONIC 0x0080		/* minimally built, iconified panel */
#define  WPT_INVISIBLE  WPT_NOSHOW	/* withdrawn state*/
#define  WPT_VISIBLE 	0x0100		/* normal state, raised */
#define  WPT_MODAL	WPT_MODALPANEL  /* normal state, raised, modal */
#define  WPT_DELETED	0x0200		/* delete on event (for code gen) */

#define  WPT_NOPADDING  0x1000  	/* no padding for the panel */  
#define  WPT_HELP_PANEL 0x2000		/* panel is for help, used by WPT  */

#define  WPT_ACTIVE     1		/* Item active (may receive input) */
#define  WPT_INACTIVE   2		/* Item inactive (cannot recv. input) */

/* Dialog stuff */
#define  OK_COMPONENT		0x0001
#define  APPLY_COMPONENT	0x0002
#define  CANCEL_COMPONENT	0x0004
#define  HELP_COMPONENT		0x0008
#define  SEPARATOR_COMPONENT	0x0010

/* Message type (controls icon) */
#define  WPT_MESSAGE_MESSAGE		0
#define  WPT_MESSAGE_INFORMATION	1
#define  WPT_MESSAGE_WARNING		2
#define  WPT_MESSAGE_ERROR 		3
#define  WPT_MESSAGE_QUESTION		4
#define  WPT_MESSAGE_WORKING		5

/* Scrolled Workspace and Scrolled Panel */
#define  WPT_SCROLLBARS_NONE		0
#define  WPT_SCROLLBARS_AS_NEEDED	1
#define  WPT_SCROLLBARS_ALWAYS		2

/******************************************************************************/
/* WPT_MENU entry types */
#define  WPT_MENU_CASCADE_TYPE		"cascade"
#define  WPT_MENU_HELPCASCADE_TYPE	"help cascade"
#define  WPT_MENU_PUSHBUTTON_TYPE	"pushbutton"
#define  WPT_MENU_STATICLABEL_TYPE	"static label"
#define  WPT_MENU_LABEL_TYPE		WPT_MENU_STATICLABEL_TYPE /*obsolete*/
#define  WPT_MENU_MENUICON_TYPE		"menuicon"
#define  WPT_MENU_SEPARATOR_TYPE	"separator"
#define  WPT_MENU_CHECKBOX_TYPE		"checkbox"
#define  WPT_MENU_TAEHELP_TYPE		"taehelp"

/* WPT_ACCEL accelerator masks, these can be ORed together*/
#define  WPT_ACCEL_NONE			0
#define  WPT_ACCEL_ALT			1
#define  WPT_ACCEL_CTRL			(WPT_ACCEL_ALT<<1)
#define  WPT_ACCEL_SHIFT		(WPT_ACCEL_CTRL<<1)

/* WPT_MENU separator styles */
/* NOTE: these integers are also indexes into the separator table in */
/*	 menucalls.cc. They MUST agree with that table */
#define	 WPT_SEP_SINGLE_LINE		0
#define	 WPT_SEP_DOUBLE_LINE		1
#define	 WPT_SEP_SINGLE_DASHED_LINE	2
#define	 WPT_SEP_DOUBLE_DASHED_LINE	3
#define	 WPT_SEP_SHADOW_ETCHED_IN	4
#define	 WPT_SEP_SHADOW_ETCHED_OUT	5
#define	 WPT_SEP_NO_LINE		6

/* WPT_MENU structure definitions */
typedef struct {
    TAEINT	modifier;		/* modifier mask */
    TEXT	key[STRINGSIZ+1];	/* name of accelerator key */
    } WPT_ACCEL;

typedef struct {
    TAEINT	style;			/* separator style  (WPT_SEP_xxx)*/
    TAEINT	thickness;		/* thickness if shadow separator */
    } WPT_SEP;

typedef struct {
    TEXT	type[NAMESIZ+1];	/* one of WPT_MENU_xxx_TYPE constants */
    TEXT 	name[NAMESIZ+1];	/* unique entry name */
    TEXT	title[STRINGSIZ+1];	/* title of entry (displayed in menu) */
    TEXT	mnemonic[2];		/* only 1st char significant */
    BOOL	state;			/* TRUE = active, FALSE = inactive */
    TEXT	fg[STRINGSIZ+1];	/* foreground color ("" = default) */
    TEXT	bg[STRINGSIZ+1];	/* background color ("" = default) */
    TEXT	font[STRINGSIZ+1];	/* font ("" = default) */
    WPT_ACCEL	accelerator;
    TEXT	checkboxVal[sizeof("yes")];/* value of checkbox if any */
    TEXT	iconFileName[STRINGSIZ+1];/* icon file name if any */
    WPT_SEP	separator;		/* separator info if any */

    CODE	status;			/* status from Wpt_Setx for this entry*/
    } WPT_MENUENTRY;
/******************************************************************************/
    

#ifdef  __cplusplus			/**** needed by WPT only  ****/
// size of key-in field for various parameter types 
// decimal field size		  
#define  DECIMALSIZE	12		
// Hex field size		  
#define  HEXSIZE	8		
// real field size		  
#define  REALSIZE	20		

// panel related function handled directly by Wpt. 
#define  PARM_EVENT	     21

// default background color
#define WPT_DEF_BG  "white"		
// default foreground color
#define WPT_DEF_FG  "black"		
// default WPTfont
#define WPT_DEF_FONT "fixed"		

// Sentinel for  WidgetAgent object
#define  WIDGET_SENTINEL    0xFF1FFFFF
// Sentinel for  Wpt panel object
#define  PANEL_SENTINEL     0xFF11FFFF
// Sentinel for  Wpt item object
#define  ITEM_SENTINEL      0xFF120FFF
// Dialog box
#define  DIALOG_SENTINEL    0xFF13FFFF

//sentinels for various WPT related event classes
#define	 WPTEVENT_SENTINEL 0xFF40FFFF
#define	 PANELEVENT_SENTINEL 0xFF41FFFF
#define  PARMEVENT_SENTINEL 0xFF42FFFF
#define  FILEEVENT_SENTINEL 0xFF43FFFF
#define  WINDOWEVENT_SENTINEL 0xFF44FFFF
#define  MESSAGEEVENT_SENTINEL 0xFF45FFFF
#define  TIMEREVENT_SENTINEL 0xFF46FFFF

#define  MSGBOX_SENTINEL  0xFF5FFFFF
#endif		/* __cplusplus */

#endif

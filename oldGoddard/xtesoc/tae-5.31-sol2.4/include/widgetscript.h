/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*****************************************************************************
 *
 *  widgetscript.h >>>  Widget level scripting support
 *
 *
 *	CHANGE LOG:
 *
 *  10-mar-93	Copied from palm's directory...swd
 *  23-apr-93	Added extern "C" block, include kr_ansi.h...swd
 *  03-jun-93	Changed kr_ansi.h to TAEProto.h, ARGS to _TAE_PROTO...kbs
 *  21-jun-93	new function names that don't clash with application...palm
 *  14-aug-93	PR2143: new function for popup children...palm
 *
 ****************************************************************************/
#ifndef WIDGETSCRIPT_H_DEFINED
#define WIDGETSCRIPT_H_DEFINED 0

#include <taeproto.h>	/* Only for _TAE_PROTO macro */

/* return codes */
#define WS_OK       0
#define WS_NOACTION 1
#define WS_NOWIDGET 2

#ifdef __cplusplus
extern "C" {		// Tell C++ not to overload these functions
#endif

int wptWidgetScript
   _TAE_PROTO ((Widget topLevel, char *widgetName, XEvent *ev, char *actionName, char *arg));
int wptWidgetSetValues
   _TAE_PROTO ((Widget topLevel, char *widgetName,  char *resourceName, 
   void *value, int type));
char *wptWidgetClassName _TAE_PROTO ((Widget w));

char *wptWidgetName _TAE_PROTO ((Widget w));

Widget *wptWidgetChildren _TAE_PROTO ((Widget w));
Widget *wptWidgetPopups   _TAE_PROTO ((Widget w));

int wptWidgetNumChildren _TAE_PROTO ((Widget w));
int wptWidgetNumPopups   _TAE_PROTO ((Widget w));

int wptWidgetWindow _TAE_PROTO((Widget w));

#ifdef GOOD_STUFF_BUT_EXPERIMENTAL
void wptWidgetHookActions();
void wptWidgetUnHookActions();
#endif

#ifdef __cplusplus
}		// End extern "C" block
#endif

#endif 


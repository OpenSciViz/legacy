/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/******************************************************************************
 *
 *  wptscript.h >>> Header for wptscript.cc
 *
 ******************************************************************************
 *
 *	CHANGE LOG:
 *
 *  10-mar-93	Installed from palm's directory...swd
 *  19-apr-93	Added ARGS macro wrapper on C-callable function protos...swd
 *  23-apr-93	Moved function protos to wptinp.inc...swd
 *  05-may-93	Changed name: Wpt_SetLearn to Wpt_SetRecord...swd
 *  23-may-94   PR2710: objectList and messageList are now pointers to slists
 *              instead of slists because static initializers are created
 *              unless the lists are pointers to slists...rt
 *
 *****************************************************************************/


#ifndef WptScriptRcvr_H
#define WptScriptRcvr_H 0

#include   <taeconf.inp>
#include   <wptevent.h>
#include	<slist.h>


class SymbolTable;

class WptScriptRcvr
    {
protected:
    int		fd;			// fd of script command
    XtInputId   reqId;			// request id for socket callback
    int		sequence;		// most recent msg sequence number
public:
    WptScriptRcvr (char *name);		// registration
    ~WptScriptRcvr ();			// de-registration

    void SetFd(int scriptFd) 
	{fd = scriptFd;}
    void SetReqId (XtInputId requestId)
	{reqId = requestId;}
    void SetSequence (int seq)
	{sequence = seq;}
    //   CAUTION: the SymbolTable returned by the Action
    //   function is deleted after being transmitted to
    //   the script process.
    virtual SymbolTable *Action (SymbolTable *message) 
	{return message;} 		// pure virtual: a dummy

    SymbolTable *makeSt
	(int code, char *message="", void *p1=NULL, void *p2=NULL);
    SymbolTable *noPanelMsg (char *panelName)
	{return makeSt (FAIL, "cannot find panel '%s'", panelName);}
    static SymbolTable *HandleMessage 
	(SymbolTable * input, int fd, XtInputId reqId);
    static void AddMessage (const char *aMessage);
    static int MessagesEnabled() {return messagesEnabled;}
protected:
    char	*name;			// object name
    static slist *objectList;		// instance list of all WptScriptRcvr's
    static slist *messageList;
    static int   messagesEnabled;
    };

class WptScriptEvent : public WptScriptRcvr
    {
public:
    WptScriptEvent () : WptScriptRcvr ("TAE_event") {}
    SymbolTable *Action (SymbolTable *input);
    void SendResponse (int scriptFd);

    // this is static so that we can encapsulate all
    // script object pointers in this module. 

    static void EventFinished(WptEvent *wptEvent);
    static void EventDelivery(WptEvent *wptEvent);
    };


class WptScriptGetTargetVm : public WptScriptRcvr
    {
public:
    WptScriptGetTargetVm () : WptScriptRcvr ("TAE_getTargetVm") {}
    SymbolTable * Action (SymbolTable *input);
    };

class WptScriptGetViewVm : public WptScriptRcvr
    {
public:
    WptScriptGetViewVm () : WptScriptRcvr ("TAE_getViewVm") {}
    SymbolTable * Action (SymbolTable *input);
    };

class WptScriptNop : public WptScriptRcvr
    {
public:
    WptScriptNop () : WptScriptRcvr ("TAE_nop") {} 
    SymbolTable *Action (SymbolTable *input) ;
    };

class WptScriptBeginWait : public WptScriptRcvr
    {
public:
    WptScriptBeginWait () : WptScriptRcvr ("TAE_beginWait") {} 
    SymbolTable *Action (SymbolTable *input) ;
    };

class WptScriptEndWait : public WptScriptRcvr
    {
public:
    WptScriptEndWait () : WptScriptRcvr ("TAE_endWait") {} 
    SymbolTable *Action (SymbolTable *input) ;
    };


class WptScriptMessagesOn : public WptScriptRcvr	// allow modal messages
    {
public:
    WptScriptMessagesOn () : WptScriptRcvr ("TAE_messagesOn") {} 
    SymbolTable *Action (SymbolTable *input) ;
    };

class WptScriptMessagesOff : public WptScriptRcvr	// dis-allow modal msgs 
						        // (and printf them)
    {
public:
    WptScriptMessagesOff () : WptScriptRcvr ("TAE_messagesOff") {}
    SymbolTable *Action (SymbolTable *input) ;
    };


//	general-purpose for C callers

class WptScriptFunction : public WptScriptRcvr
    {
    T_ScriptFunction 	function; 
    void		*context;
public:
    WptScriptFunction (char *name, T_ScriptFunction aFunction, void *context);
    SymbolTable *Action (SymbolTable *input);
    };


class WptScriptBreak : public WptScriptRcvr
    {
public:
    WptScriptBreak () : WptScriptRcvr ("TAE_break") {}
    SymbolTable *Action (SymbolTable *input);
    };


class Symbol;

class WptScriptWait : public WptScriptRcvr
    {
    Symbol	*panelNameS;	// values are names of panels; NULL=all
    Symbol	*parmNameS;	// values are names of parms; NULL=all
    int		waitActive;
public:
    WptScriptWait ();
    SymbolTable *Action (SymbolTable *msg);
    void SatisfyWait(char *panelName, char *parmName, Id targetVm);
    int Matches (char *parmName, char *panelName);
    void SetSensitivity ();
    };


class WptScriptGetWindowId : public WptScriptRcvr
    {
public:
    WptScriptGetWindowId () : WptScriptRcvr ("TAE_getWindowId") {}
    SymbolTable *Action (SymbolTable *msg);
    };

class WptScriptSetItemSensitivity : public WptScriptRcvr
    {
public:
    WptScriptSetItemSensitivity () : WptScriptRcvr ("TAE_SetItemSensitivity") {}
    SymbolTable *Action (SymbolTable *msg);
    };

//
//	this allows the script to announce its pid
//	so that the application process can kill the script 
//
//	also the script sends
//	a pid of zero to indicate termination.
//
class WptScriptControl : public WptScriptRcvr
    {
    int	pid;
public:
    WptScriptControl () : WptScriptRcvr ("TAE_scriptControl") 
	{pid = 0;}
    SymbolTable *Action (SymbolTable *msg);
    int Pid() 
	{return pid;}
    void ClearPid()  
	{pid = 0;}
    void Kill();
    
    virtual void ScriptBegin(SymbolTable *msg) {msg=msg;}	// default: do nothing
    virtual void ScriptEnd(SymbolTable *msg)   {msg=msg;}	// default: do nothing
    };

class WptScriptStdout : public WptScriptRcvr
    {
public:
    WptScriptStdout () : WptScriptRcvr ("TAE_stdout") {}
    SymbolTable *Action (SymbolTable *msg);
    };


class WptPanelMessage: public WptScriptRcvr
    {
public:
    WptPanelMessage() : WptScriptRcvr ("TAE_panelMessage") {}
    SymbolTable *Action (SymbolTable *msg);
    };


class WptSetRecord: public WptScriptRcvr
    {
public:
    WptSetRecord() : WptScriptRcvr ("TAE_setLearn") {}
    SymbolTable *Action (SymbolTable *msg);
    };

class WptScriptSetValues : public WptScriptRcvr
    {
    SymbolTable 		*values;
    T_ScriptedValuesFunction	callback;
    void			*context;
public:
    WptScriptSetValues();
    ~WptScriptSetValues();
    void SetCallback (T_ScriptedValuesFunction theCallback, void *theContext)
	{callback = theCallback; context = theContext;}
    SymbolTable *Action (SymbolTable *msg);
    SymbolTable *GetSymbolTable();
    Symbol *GetValue (const char *name);
    const char *GetString (const char *name, const char *defaultValue);
    int GetIntg (const char *name, int defaultValue);
    double GetReal (const char *name, double defaultValue);
    };

class WptScriptWidgetAction : public WptScriptRcvr
    {
public:
    WptScriptWidgetAction () : WptScriptRcvr ("TAE_WidgetAction") {}
    SymbolTable *Action (SymbolTable *input);
    };

class WptScriptWidgetTree : public WptScriptRcvr
    {
public:
    WptScriptWidgetTree () : WptScriptRcvr ("TAE_WidgetTree") {}
    SymbolTable *Action (SymbolTable *input);
    };

class WptScriptWidgetSetValues : public WptScriptRcvr
    {
public:
    WptScriptWidgetSetValues () : WptScriptRcvr ("TAE_WidgetSetValues") {}
    SymbolTable *Action (SymbolTable *input);
    };

#endif

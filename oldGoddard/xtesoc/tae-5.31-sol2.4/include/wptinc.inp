/**************************************************************************n
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/****************************************************************************
 *
 * This file contains all the constants and function declarations
 * needed by WPT programs.
 *
 ****************************************************************************
 *
 * CHANGE LOG: 
 *
 *	22-may-89	Declare _appPanelList external symbol...dm
 *	19-jun-89	First arg in Wpt_NewPanel() is now 'const'...ljn
 *	13-jul-89	Add Wpt_PanelTopWindow() declaration...dm
 *	30-aug-89	Wpt_SetReal and Wpt_SetInt use TAEFLOAT, TAEINT
 *			respectively...krw
 *	10-sep-89	VMS port...ljn
 *      15-dec-89       removed const in declaration...tpl
 *	24-jan-90	Add 'extern "C"' for g++ 1.36...ljn
 *	07-may-90	c_plusplus is now __cplusplus...ljn
 *	08-oct-90	Added Wpt_ Constraints declarations...rt
 *	05-nov-90	Added Wpt_SetSensitivity and 
 *			Wpt_SetConstraintSensitivity ...krw
 *	06-nov-90	Wpt_SetPanelState and Wpt_GetPanelState...ljn
 *      11-nov-90	Added Wpt_Showitem & Wpt_HideItem...rt
 *	31-mar-91	Header files for VMS...ljn
 *	21-aug-91	Declare Wpt_CloseDisplay()...ljn
 *	03-jan-92	PR1257: Use <> for #include file names...ljn
 *	28-jan-92  	Remove include of symbol.h and widgetagent.h...tpl
 *      18-mar-92       Added Wpt_MessageNoBlock, and Wpt_RejectNoBlock.
 *                      Both are currently unsupported...cew
 *      30-mar-92       Multiple Display support.  Added Wpt_OpenDisplay,
 *                      Wpt_PanelDisplayId, Wpt_Finish, changed first argument
 *                      of Wpt_NewPanel to Display *...cew
 *	03-apr-92	Added declaration of GetItemSensitivity 
 *			and many others.. ...tpl
 *	30-apr-92	Added function prototype of Wpt_KeyinCheckForNULLInput
 *			PR1109, pr908...tpl
 *	01-may-92	Added declarations for Wpt_Menuxxx routines...krw
 *			Wpt_GetHelpStyle was improperly defined for C,
 *			it should have been TEXT * not CODE.
 *			Wpt_MessageNoBlock was improperly defined as
 *			Wpt_PanelMessageNoBlock.
 *	08-may-92	Revision to Menubar API, removed
 *			Wpt_FreeMenuEntries..krw
 *      11-may-92       Used const TEXT for item name in menu routines
 *		        and declared free properly...tpl
 *	27-may-92       ifdef GNU for free function prototype PR1444...tpl
 *      29-sep-92	PR1115: Added Wpt_Add/RemoveTimer ...cew
 *	15-oct-92	PR1684: Various C++ compilers had problems with
 *			the prototype for free(). This has been moved to
 *			taeconf.inp...krw
 *	22-oct-92	PR1699: Function declaration for Wpt_Rehearse added..krw
 *	23-apr-93	Added scripting related functions...swd
 *	05-may-93	Changed name: Wpt_SetLearn to Wpt_SetRecord...swd
 *	03-jun-93	Removed inc of kr_ansi.h since taeconf includes it;
 *	        	changed ARGS to _TAE_PROTO...kbs
 *	09-jul-93	Use of _TAE_PROTO for prototypes...rt
 *	11-may-94	SCO port: Remove comments from pre-processor lines...swd
 *	27-jul-94	PR2815: Don't undef NULL if ANSI; some ANSIs squawk..dag
 *
 **************************************************************************/

#ifndef I_WPTINC
#define	I_WPTINC  0

#include <taeconf.inp>			/* Make sure this is first */
#include <wptdefs.h>			/* all WPT defines	*/


/*********************************************************************/

#ifndef  __cplusplus			/* for regular 'C' modules   */
#include	<wptevent.h>		/* all wpt related events    */
#ifdef VMS
#include	<Intrinsic.h>		/* Widget definitions etc.   */
#undef  NULL                            /* redefined by X same value */
#include        <Xlib.h>
#else
#include	<X11/Intrinsic.h>	/* Widget definitions etc.   */
#ifndef __STDC__			/* NULL is part of ANSI std. */
#undef  NULL                            /* redefined by X same value */
#endif
#include        <X11/Xlib.h>
#endif

#ifndef  NULL                           /* in case not included again */
#define  NULL  0
#endif

/** Define entry points for upward compatibility  */

#define  Wp_NewPanel	Wpt_NewPanel
#define  Wp_PanelErase	Wpt_PanelErase 
#define  Wp_PanelReset	Wpt_PanelReset
#define  Wp_ParmReject	Wpt_ParmReject
#define  Wp_PanelWindow	Wpt_PanelWindow
#define  Wp_PanelXrId	Wpt_PanelXrId
#define  Wp_ItemWindow	Wpt_ItemWindow
#define  Wp_PanelMessage Wpt_PanelMessage
#define  Wp_MissingVal	Wpt_MissingVal
#define  Wp_ParmUpdate	Wpt_ParmUpdate
#define  Wp_QueryItem	Wpt_QueryItem
#define  Wp_BeginWait	Wpt_BeginWait
#define  Wp_EndWait	Wpt_EndWait
#define  Wp_ViewUpdate	Wpt_ViewUpdate
#define  Wp_CloseItems	Wpt_CloseItems
#define  Wp_NextEvent	Wpt_NextEvent
#define  Wp_Init	Wpt_Init
#define  Wp_ConvertName Wpt_ConvertName
#define  Wp_ConvertNameP Wpt_ConvertNameP
#define  wp_PanelEvent	wpt_PanelEvent

#define  xrPanel*	Widget			/* XToolkit handle to panel */ 
#define  Wpt_PanelXrId  Wpt_PanelWidgetId	/* new XToolkit  version */

/************************************************
 * WPT related entry points    			*
 * Old entry points are defined to be the same  * 
 * as the new ones (common to c and c++). 	*
 ***********************************************/

Display*    Wpt_Init _TAE_PROTO ((const TEXT *displayName));
Display*    Wpt_CCInit _TAE_PROTO ((const TEXT *displayName));
CODE        Wpt_Finish();
Display*    Wpt_OpenDisplay _TAE_PROTO ((TEXT *displayName));
Id      Wpt_NewPanel _TAE_PROTO ((Display *display, Id dataVm, Id viewVm, 
                                  Window relativeWindow, void* userContext, 
                                  int wptFlags));
CODE    Wpt_PanelErase _TAE_PROTO ((Id panelId));
Id      Wpt_NewItem _TAE_PROTO ((Id panelId, const TEXT* name, 
                                 Id dataVm, Id viewVm));
CODE    Wpt_ItemErase _TAE_PROTO ((Id panelId, const TEXT* name));
CODE    Wpt_PanelReset _TAE_PROTO ((Id panelId));
CODE  	Wpt_ParmReject _TAE_PROTO ((Id panelId, const TEXT* parmName,
                                    TEXT *message));
CODE    Wpt_RejectNoBlock _TAE_PROTO ((Id panelId, const TEXT* parmName,
                                       TEXT *message));
Window  Wpt_PanelWindow _TAE_PROTO ((Id panelId));
Window  Wpt_PanelTopWindow _TAE_PROTO ((Id panelId));
Widget	Wpt_PanelWidgetId _TAE_PROTO ((Id panelId));
CODE    Wpt_PanelDisplayId _TAE_PROTO ((Id panelId, Display **displayId,
                                        COUNT *screen_number));
Window 	Wpt_ItemWindow _TAE_PROTO ((Id panelId, const TEXT* parmName));
CODE    Wpt_PanelMessage _TAE_PROTO ((Id panelId, const TEXT* message));
CODE    Wpt_MessageNoBlock _TAE_PROTO ((Id panelId, const TEXT* message,
                                        void *userContext,
                                        BOOL returnToApp));
BOOL  	Wpt_MissingVal _TAE_PROTO ((Id dataVm, TEXT* message));
CODE    Wpt_ParmUpdate _TAE_PROTO ((Id panelId, const TEXT* parmName));

CODE	Wpt_BeginWait _TAE_PROTO ((Id panelId));
CODE	Wpt_EndWait _TAE_PROTO ((Id panelId));
CODE    Wpt_ViewUpdate _TAE_PROTO ((Id panelId, const TEXT* parmName, 
                                    Id view, const TEXT* viewParm));
CODE	Wpt_CloseDisplay _TAE_PROTO ((Display *theDisplay));
CODE	Wpt_CloseItems _TAE_PROTO ((Id panelId));
void    Wpt_SetTimeOut _TAE_PROTO ((int interval));
Id      Wpt_AddEvent _TAE_PROTO ((int eventSource, int eventTypeMask));
CODE    Wpt_RemoveEvent _TAE_PROTO ((Id eventId));
Id	Wpt_AddTimer _TAE_PROTO ((TAEINT interval, BOOL repeat,
                                  void* userContext));
void	Wpt_RemoveTimer _TAE_PROTO ((Id timerId));
CODE	Wpt_NextEvent _TAE_PROTO ((WptEvent *));
BOOL    Wpt_CheckEvent();
BOOL    Wpt_Pending();
void	Wpt_EnableInterrupt _TAE_PROTO ((Boolean flag));
Window	Wpt_ConvertName _TAE_PROTO ((const TEXT* name));
Window  Wpt_ConvertNameP _TAE_PROTO ((const TEXT* name, Window window));
CODE    Wpt_SetReal _TAE_PROTO ((Id panelId, const TEXT* parmName,
                                 TAEFLOAT real));
CODE    Wpt_SetIntg _TAE_PROTO ((Id panelId, const TEXT* parmName,
                                 TAEINT intg));
CODE    Wpt_SetString _TAE_PROTO ((Id panelId, const TEXT* parmName, 
                                   const TEXT *string));
CODE	Wpt_SetRealConstraints _TAE_PROTO ((Id thePanel, const TEXT *parmName,
                                            FUNINT count, TAEFLOAT low[],
                                            TAEFLOAT high[]));
CODE	Wpt_SetIntgConstraints _TAE_PROTO ((Id thePanel, const TEXT *parmName,
                                            FUNINT count, TAEINT low[],
                                            TAEINT high[]));
CODE	Wpt_SetStringConstraints _TAE_PROTO ((Id thePanel,
                                              const TEXT *parmName,
                                              FUNINT count,
                                              const TEXT *vector[]));
CODE	Wpt_GetRealConstraints _TAE_PROTO ((Id thePanel,const  TEXT *parmName,
                                            FUNINT *count, TAEFLOAT *low[],
                                            TAEFLOAT *high[]));
CODE	Wpt_GetIntgConstraints _TAE_PROTO ((Id thePanel, const TEXT *parmName,
                                            FUNINT *count, TAEINT *low[],
                                            TAEINT *high[]));
CODE	Wpt_GetStringConstraints _TAE_PROTO ((Id thePanel,
                                              const TEXT *parmName,
                                              FUNINT *count, TEXT **vector[]));
CODE    Wpt_SetNoValue _TAE_PROTO ((Id panelId, const TEXT* parmName));

CODE	Wpt_GetItemSensitivity _TAE_PROTO ((Id thePanel, const TEXT *parmName,
                                            BOOL *state));
CODE	Wpt_SetItemSensitivity _TAE_PROTO ((Id thePanel, const TEXT *parmName,
                                            BOOL state));
CODE	Wpt_SetConstraintSensitivity _TAE_PROTO ((Id thePanel,
                                                  const TEXT *parmName,
                                                  FUNINT num,
                                                  const TEXT **stringvec,
                                                  BOOL *state));
CODE	Wpt_GetConstraintSensitivity _TAE_PROTO ((Id thePanel,
                                                  const TEXT *parmName,
                                                  FUNINT num,
                                                  TEXT **stringvec,
                                                  BOOL *state));
CODE    Wpt_SetPanelState _TAE_PROTO ((Id panelId, COUNT state));
CODE    Wpt_GetPanelState _TAE_PROTO ((Id panelId, COUNT *state));
CODE	Wpt_HideItem _TAE_PROTO ((Id thePanel, const TEXT *parmName));
CODE	Wpt_ShowItem _TAE_PROTO ((Id thePanel, const TEXT *parmName));
CODE	Wpt_SetHelpStyle _TAE_PROTO (( const TEXT *));
TEXT    *Wpt_GetHelpStyle();

void 	Wpt_Rehearse _TAE_PROTO ((TAEINT theInterval, const TEXT *theOrder,
                                  const TEXT *theCycle ));

void    Wpt_KeyinCheckForNULLInput _TAE_PROTO (( BOOL state ));

		/* Wpt_Menu routines */
CODE	Wpt_GetMenu _TAE_PROTO ((Id targetVm, Id viewVm, const TEXT *itemName,
                                 TEXT *entryName, COUNT *count,
                                  WPT_MENUENTRY **entries));
CODE	Wpt_SetMenu _TAE_PROTO ((Id targetVm, Id viewVm, const TEXT *itemName,
                                 TEXT *entryName, FUNINT count,
                                 WPT_MENUENTRY *entries));
CODE	Wpt_MenuUpdate _TAE_PROTO ((Id thePanel, Id targetVm, Id viewVm,
                                    const TEXT *itemName));
CODE	Wpt_InsertMenuEntries _TAE_PROTO ((Id targetVm, Id viewVm,
                                           const TEXT *itemName,
                                           TEXT *entryName, FUNINT count,
                                           WPT_MENUENTRY *entries));
CODE	Wpt_RemoveMenuEntries _TAE_PROTO ((Id targetVm, Id viewVm,
                                           const TEXT *itemName, 
                                           FUNINT count, TEXT **entryNames));
CODE	Wpt_SetMenuSensitivity _TAE_PROTO ((Id viewVm, const TEXT *itemName,
                                            FUNINT count, TEXT **entryNames,
                                            BOOL *state));
CODE 	Wpt_GetMenuEntryPath _TAE_PROTO ((Id viewVm, const TEXT *itemName,
                                          TEXT *entryName, TEXT *entryPath));

/* Wpt_ Scripting - related functions.	*/
extern 	CODE 	Wpt_ScriptInit _TAE_PROTO ((TEXT *socketName));
extern 	BOOL 	Wpt_IsScripted();
extern 	BOOL 	Wpt_SetRecord _TAE_PROTO ((char *recordFileName));

/* Wpt_ Scripting -- Unsupported functions.	*/
 extern VOID Wpt_KillScript();
 extern TEXT *Wpt_GetScriptedString _TAE_PROTO ((char *name,
                                                 char *defaultValue));
 extern TAEINT Wpt_GetScriptedIntg _TAE_PROTO ((char *name, int defaultValue));
 extern TAEFLOAT Wpt_GetScriptedReal _TAE_PROTO ((char *name,
                                                  double defaultValue));
 extern Id   Wpt_GetScriptedVm ();
 extern Id   Wpt_RegisterForScript
    _TAE_PROTO ((TEXT *name, T_ScriptFunction funct, VOID *context));
 extern VOID Wpt_DeRegisterForScript _TAE_PROTO ((Id theObjectFromRegister));
 extern VOID Wpt_BeginWaitAll();
 extern VOID Wpt_EndWaitAll();

int  s_copy _TAE_PROTO ((const char * source, char * dest));
int  s_append _TAE_PROTO ((const char * source, char * dest));
void s_blank _TAE_PROTO ((const TEXT *string, int length));
COUNT s_bcopy _TAE_PROTO ((const TEXT *from, TEXT *to, int length));
int  s_equal _TAE_PROTO ((const char *, const char *));
int  s_index _TAE_PROTO ((const TEXT *, FUNINT));
int  s_lseq _TAE_PROTO ((const TEXT *, const TEXT *));
void strpqu _TAE_PROTO ((const TEXT *));
int  s_length _TAE_PROTO ((const char *));
TEXT *s_substring _TAE_PROTO ((TEXT *pattern, TEXT *fullstring));
TEXT  *s_save _TAE_PROTO ((const TEXT *));
void  s_free _TAE_PROTO ((TEXT *));
void s_strip _TAE_PROTO (( TEXT *));
void s_shift _TAE_PROTO (( TEXT *, FUNINT));
int  s_i2s _TAE_PROTO ((TAEINT intg, TEXT *string));
int  s_r2s _TAE_PROTO ((TAEFLOAT real, TEXT *string));
int  s_s2i _TAE_PROTO ((TEXT *string, TAEINT *intg));
int  s_s2r _TAE_PROTO ((TEXT *string, TAEFLOAT *real));

#ifndef FAST_ALLOC
void  * tae_alloc _TAE_PROTO ((int size, int num));
#endif
void  bytmov _TAE_PROTO ((void *, void *, int));
CODE f_libr _TAE_PROTO ((TEXT *, TEXT*));
CODE f_name _TAE_PROTO ((TEXT *, TEXT*));
CODE f_type _TAE_PROTO ((TEXT *, TEXT*));
BOOL f_force_lower _TAE_PROTO ((BOOL));
BOOL f_exists _TAE_PROTO ((const TEXT *, TEXT *, TEXT *, TEXT *));

VOID t_pinit _TAE_PROTO (( int *, int *, int *));

/***************************************************************************/

#else		/* #ifndef  __cplusplus */
		/* THIS SEGMENT FOR C++ PROGRAMS  */	
   
//	in this section we use C++ commenting to remind editors
//	that they are in C++


// all wpt related events
#include	<wptevent.h>		
#ifdef VMS
// Widget definitions etc.
#include	<Intrinsic.h>		
#else
// Widget definitions etc.
#include	<X11/Intrinsic.h>	
#endif

// These comprise the C application interface entry points. The extern C
// ensures they don't get overloaded.

extern "C"
{
Display*    Wpt_Init(const TEXT *displayName);
Display*    Wpt_CCInit(const TEXT *displayName);
CODE        Wpt_Finish();
Display*    Wpt_OpenDisplay(TEXT *displayName);
Id      Wpt_NewPanel (Display *display, Id dataVm, Id viewVm, 
			Window relativeWindow, void* userContext, 
			int wptFlags);
CODE    Wpt_PanelErase (Id panelId);
Id      Wpt_NewItem (Id panelId, const TEXT* name, 
			Id dataVm, Id viewVm); 
CODE    Wpt_ItemErase (Id panelId, const TEXT* name);
CODE    Wpt_PanelReset(Id panelId);
CODE  	Wpt_ParmReject(Id panelId, const TEXT* parmName, TEXT *message);
CODE    Wpt_RejectNoBlock(Id panelId, const TEXT* parmName, TEXT *message);
Window  Wpt_PanelWindow (Id panelId);
Window  Wpt_PanelTopWindow (Id panelId);
Widget	Wpt_PanelWidgetId (Id panelId);
CODE    Wpt_PanelDisplayId (Id panelId, Display **displayId,
                                    COUNT *screen_number);
Window 	Wpt_ItemWindow(Id panelId, const TEXT* parmName);
CODE    Wpt_PanelMessage(Id panelId, const TEXT* message);
CODE    Wpt_MessageNoBlock(Id panelId, const TEXT* message,
                                      void *userContext,
                                      BOOL returnToApp);
BOOL  	Wpt_MissingVal(Id dataVm, TEXT* message);
CODE    Wpt_ParmUpdate(Id panelId, const TEXT* parmName);

//   extern CODE  Wpt_QueryItem(panelId, parmName, rectangle) 

CODE	Wpt_BeginWait(Id panelId);
CODE	Wpt_EndWait(Id panelId);
CODE    Wpt_ViewUpdate(Id panelId, const TEXT* parmName, 
			    Id view, const TEXT* viewParm);
CODE	Wpt_CloseDisplay(Display *theDisplay);
CODE	Wpt_CloseItems(Id panelId);
void    Wpt_SetTimeOut(int interval);
Id      Wpt_AddEvent(int eventSource, int eventTypeMask);
CODE    Wpt_RemoveEvent(Id eventId);
Id	Wpt_AddTimer(TAEINT interval, BOOL repeat, void* userContext);
void	Wpt_RemoveTimer(Id timerId);
CODE	Wpt_NextEvent(WptEvent *);
BOOL    Wpt_CheckEvent();
BOOL    Wpt_Pending();
void	Wpt_EnableInterrupt(Boolean flag);
Window	Wpt_ConvertName(const TEXT* name);
Window  Wpt_ConvertNameP(const TEXT* name, Window window);
CODE    Wpt_SetReal(Id panelId, const TEXT* parmName, TAEFLOAT real);
CODE    Wpt_SetIntg(Id panelId, const TEXT* parmName, TAEINT intg);
CODE    Wpt_SetString(Id panelId, const TEXT* parmName, 
			const TEXT *string);
CODE	Wpt_SetRealConstraints(Id thePanel, const TEXT *parmName,
					FUNINT count, TAEFLOAT low[],
					TAEFLOAT high[]);
CODE	Wpt_SetIntgConstraints(Id thePanel, const TEXT *parmName,
					FUNINT count, TAEINT low[],
					TAEINT high[]);
CODE	Wpt_SetStringConstraints(Id thePanel, const TEXT *parmName,
					  FUNINT count, const TEXT *vector[]);
CODE	Wpt_GetRealConstraints(Id thePanel,const  TEXT *parmName,
					FUNINT *count, TAEFLOAT *low[],
					TAEFLOAT *high[]);
CODE	Wpt_GetIntgConstraints(Id thePanel, const TEXT *parmName,
					FUNINT *count, TAEINT *low[],
					TAEINT *high[]);
CODE	Wpt_GetStringConstraints(Id thePanel, const TEXT *parmName,
					  FUNINT *count, TEXT **vector[]);
CODE    Wpt_SetNoValue(Id panelId, const TEXT* parmName);

CODE	Wpt_GetItemSensitivity (Id thePanel, const TEXT *parmName,
					BOOL *state);
CODE	Wpt_SetItemSensitivity (Id thePanel, const TEXT *parmName,
					BOOL state);
CODE	Wpt_SetConstraintSensitivity (Id thePanel, const TEXT *parmName,
				FUNINT num, const TEXT **stringvec, BOOL *state);
CODE	Wpt_GetConstraintSensitivity (Id thePanel, const TEXT *parmName,
				FUNINT num, TEXT **stringvec, BOOL *state);
CODE    Wpt_SetPanelState(Id panelId, COUNT state);
CODE    Wpt_GetPanelState(Id panelId, COUNT *state);
CODE	Wpt_HideItem(Id thePanel, const TEXT *parmName);
CODE	Wpt_ShowItem(Id thePanel, const TEXT *parmName);
Id	Wpt_ItemId ( Id thePanel, const TEXT *parmName );
Widget	Wpt_ItemWidgetId ( Id itemId );
CODE	Wpt_SetHelpStyle( const TEXT *);
TEXT    *Wpt_GetHelpStyle();

void 	Wpt_Rehearse (TAEINT theInterval, 
                      const TEXT *theOrder = "min-max-min",
                      const TEXT *theCycle = "repeated");

void    Wpt_KeyinCheckForNULLInput ( BOOL state );

		/* Wpt_Menu routines */
CODE	Wpt_GetMenu (Id targetVm, Id viewVm, const TEXT *itemName, TEXT *entryName,
			COUNT *count, WPT_MENUENTRY **entries);
CODE	Wpt_SetMenu (Id targetVm, Id viewVm, const TEXT *itemName, TEXT *entryName,
			FUNINT count, WPT_MENUENTRY *entries);
CODE	Wpt_MenuUpdate (Id thePanel, Id targetVm, Id viewVm, const TEXT *itemName);
CODE	Wpt_InsertMenuEntries (Id targetVm, Id viewVm, const TEXT *itemName, 
			TEXT *entryName, FUNINT count, WPT_MENUENTRY *entries);
CODE	Wpt_RemoveMenuEntries (Id targetVm, Id viewVm, const TEXT *itemName, 
			FUNINT count, TEXT **entryNames); 
CODE	Wpt_SetMenuSensitivity (Id viewVm, const TEXT *itemName, FUNINT count,
			TEXT **entryNames, BOOL *state);
CODE 	Wpt_GetMenuEntryPath (Id viewVm, const TEXT *itemName, TEXT *entryName,
			TEXT *entryPath);

// Wpt_ Scripting - related functions.
CODE Wpt_ScriptInit (char *socketName);
BOOL Wpt_IsScripted();
BOOL  Wpt_SetRecord (char *recordFileName);

// Wpt_ Unsupported Scripting functions
 void Wpt_KillScript();
 char *Wpt_GetScriptedString (char *name, char *defaultValue);
 int  Wpt_GetScriptedIntg (char *name, int defaultValue);
 double Wpt_GetScriptedReal (char *name, double defaultValue);
 Id   Wpt_GetScriptedVm ();
 Id   Wpt_RegisterForScript (char *name, T_ScriptFunction funct, void *context);
 void Wpt_DeRegisterForScript (Id theObjectFromRegister);
 void Wpt_BeginWaitAll();
 void Wpt_EndWaitAll();


int  s_copy (const char * source, char * dest);
int  s_append (const char * source, char * dest);
void s_blank (const TEXT *string, int length);
COUNT s_bcopy (const TEXT *from, TEXT *to, int length);
int  s_equal (const char *, const char *);
int  s_index (const TEXT *, FUNINT);
int  s_lseq (const TEXT *, const TEXT *);
void strpqu (const TEXT *);
int  s_length (const char *);
TEXT *s_substring (TEXT *pattern, TEXT *fullstring);
TEXT  *s_save (const TEXT *);
void  s_free (TEXT *);
void s_strip( TEXT *);
void s_shift( TEXT *, FUNINT);
int  s_i2s(TAEINT intg, TEXT *string);
int  s_r2s(TAEFLOAT real, TEXT *string);
int  s_s2i(TEXT *string, TAEINT *intg);
int  s_s2r(TEXT *string, TAEFLOAT *real);

#ifndef FAST_ALLOC
void  * tae_alloc(int size, int num);
#endif
void  bytmov(void *, void *, int);
CODE f_libr (TEXT *, TEXT*);
CODE f_name (TEXT *, TEXT*);
CODE f_type (TEXT *, TEXT*);
BOOL f_force_lower (BOOL);
BOOL f_exists (const TEXT *, TEXT *, TEXT *, TEXT *);

VOID t_pinit( int *, int *, int *);
}

#endif		/* 	#ifndef  __cplusplus	*/

#endif	


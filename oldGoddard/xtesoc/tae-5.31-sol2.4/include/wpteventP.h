/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*
 * include file for the internal WptEvent structures, 
 *
 * Also defines the Wptlib class WptEventClass.
 *
 **********************************************************************/
// CHANGE LOG:  
// 
// 18-mar-92 created from old wptevent.h...tpl
// 29-sep-92 PR1115: Added TimerEventClass...cew
// 16-mar-93 Copied from palm's GEM directory.  Scripting stuff added...swd
//

#ifndef I_WPTINTEVENT
#define I_WPTINTEVENT  0

#include <wptevent.h>

#ifdef __cplusplus
#ifdef VMS
#include     "$taeincxm:symbol.h"
#include     "$taeincxm:widgetagent.h"
#else
#include     <symbol.h>
#include     <widgetagent.h>
#endif

// Class structures used by WPT for Wpt events 

class WptEventClass;
class PanelEventClass;
class ParmEventClass;
class FileEventClass;
class WindowEventClass;
class MessageEventClass;
class TimerEventClass;

class WptEventClass {
protected:
    int		     sentinel;
    WptEvent         wptEvent;		// event data
   
    // internal functions
    void 	Check()  
	{if (sentinel != WPTEVENT_SENTINEL) abort(); }

public:
    //external functions
    WptEventClass() {sentinel = WPTEVENT_SENTINEL; }
    ~WptEventClass () { sentinel = 0; }
    virtual void CopyEvent(WptEvent *event)  {event = event;}
};

class  PanelEventClass : public WptEventClass {
protected:
    void 	Check()  
	{if (sentinel != PANELEVENT_SENTINEL) abort(); }
public:
    PanelEventClass(WptPanel *panelId, XEvent *xEvent);
    ~PanelEventClass() {sentinel = 0;}
};


// Event from a Wpt Item (parm)
class  ParmEventClass : public WptEventClass {
protected:
    void 	Check()  
	{if (sentinel != PARMEVENT_SENTINEL) abort(); }
public:
    ParmEventClass(WptItem *itemId, XEvent *xEvent = nil, 
	int valueIndex = 0, Boolean selectionOnly = FALSE); 
    ~ParmEventClass() { sentinel = 0;}
    void CopyEvent(WptEvent *callerEvent);
    void SetScripted(int scriptFd) 
	{wptEvent.p_scripted = 1; wptEvent.p_scriptFd = scriptFd;}
};


// Event notifying a X Window (non-widget) event
class  WindowEventClass : public WptEventClass {
protected:
    void 	Check()  
	{if (sentinel != WINDOWEVENT_SENTINEL) abort(); }
public:
    WindowEventClass(XEvent *xEvent);
    ~WindowEventClass() { sentinel = 0; }
    void CopyEvent(WptEvent *callerEvent);
};


// Event from a file
class  FileEventClass : public WptEventClass {
protected:
    void 	Check()  
	{if (sentinel != FILEEVENT_SENTINEL) abort(); }
public:
    FileEventClass(int source, int mask);
    ~FileEventClass() { sentinel = 0; }
    void CopyEvent(WptEvent *callerEvent);
};

// Event from a Non-blocking Wpt Message
class  MessageEventClass : public WptEventClass {
protected:
    void      Check()
      {if (sentinel != MESSAGEEVENT_SENTINEL) abort(); }
public:
    MessageEventClass(void *userContext);
    ~MessageEventClass() { sentinel = 0;}
    void CopyEvent(WptEvent *callerEvent);
};

// Event from a timer
class  TimerEventClass : public WptEventClass {
protected:
    void      Check()
      {if (sentinel != TIMEREVENT_SENTINEL) abort(); }
public:
    TimerEventClass(Id timerId, BOOL repeat, 
		    TAEINT interval, void *userContext);
    ~TimerEventClass() { sentinel = 0;}
    void CopyEvent(WptEvent *callerEvent);
};

#endif

#endif

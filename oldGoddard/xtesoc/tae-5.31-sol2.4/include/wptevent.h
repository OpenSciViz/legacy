/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 * include file for the struct WptEvent, representing a Wpt type of
 * event created by WPT, to be returned to the caller via a
 * Wpt_NextEvent() call.
 *
 * Also defines the Wptlib class WptEventClass.
 *
 ************************************************************************
 *
 *  CHANGE LOG:  
 *
 *  27-may-89	Add fields p_valueIndex, p_subtype to ParmEventClass...dm
 *  07-may-90   c_plusplus is now __cplusplus...ljn
 *  13-jun-90   Merge in motif changes...tpl
 *  15-mar-91	Removed #ifdef MOTIF...ljn
 *  08-apr-91	#ifdef VMS for header files; 30-apr VMS logical change...ljn
 *  30-jan-92	PR1257: Use <> for #include filenames...ljn
 *  18-mar-92 Added MessageEventClass...cew
 *  04-apr-92   Split this file into wptevent.h and wpteventp.h...tpl
 *  19-aug-92	PR435: Add WPT_UPDATE_READMASK...crb
 *  29-sep-92	PR1115: Added WptTimerEvent struct...cew
 *  09-feb-93   add new scripted member, carved out of dummy...palm
 *  09-jul-93   ANSI C requires no tokens after an endif...rt
 *  25-mar-94   PR2663: Centerline compiler bug workaround concerning
 *		unistd.h and stdio.h...rt
 **********************************************************************/

#ifndef I_WPTEVENT
#define I_WPTEVENT  0

/* Types of events for Wpt panel event */
#define  WPT_HELP_EVENT    1
#define  WPT_PARM_EVENT    2
#define  WPT_FILE_EVENT    3
#define  WPT_WINDOW_EVENT  4
#define  WPT_TIMEOUT_EVENT 5
#define	 WPT_INTERRUPT_EVENT 6
#define  WPT_MESSAGE_EVENT 7
#define  WPT_TIMER_EVENT   8

#define  WPT_VALUE	   0x1
#define	 WPT_SELECTION	   0x2

#define  WptPanelEvent	  WptEvent		/* backward compatibility */
#define	 wpt_PanelEvent   wpt_Event		/*	,,		*/


#define  p_userContext    AnyEvent.ParmEvent.userContext
#define	 p_dataVm	  AnyEvent.ParmEvent.dataVm	
#define  p_variable	  AnyEvent.ParmEvent.variable
#define	 p_valueIndex	  AnyEvent.ParmEvent.valueIndex
#define	 p_subtype	  AnyEvent.ParmEvent.subtype
#define  p_panelId	  AnyEvent.ParmEvent.panelId
#define  p_windowId       AnyEvent.ParmEvent.windowId
#define  p_panelWidgetId  AnyEvent.ParmEvent.panelWidgetId
#define  p_relativeWindow AnyEvent.ParmEvent.relativeWindow
#define  p_eventWindow	  AnyEvent.ParmEvent.eventWindow	
#define  p_xEvent         AnyEvent.ParmEvent.xEvent
#define  p_scripted	  AnyEvent.ParmEvent.scripted
#define  p_scriptFd	  AnyEvent.ParmEvent.scriptFd

#define  f_fileSource     AnyEvent.FileEvent.fileSource
#define  f_fileEventType  AnyEvent.FileEvent.fileEventType

#define  w_windowId       AnyEvent.WindowEvent.windowId
#define  w_xEvent         AnyEvent.WindowEvent.xEvent

#define  m_userContext    AnyEvent.MessageEvent.userContext

#define  t_id             AnyEvent.TimerEvent.id
#define  t_repeat         AnyEvent.TimerEvent.repeat
#define  t_interval       AnyEvent.TimerEvent.interval
#define  t_userContext    AnyEvent.TimerEvent.userContext

/*   For the convenience of application programs  */

#define	 WPT_READMASK  	  	XtInputReadMask
#define	 WPT_WRITEMASK    	XtInputWriteMask
#define	 WPT_EXCEPTMASK   	XtInputExceptMask
#define	 WPT_UPDATE_READMASK	(1<<3)

#include 	<taeconf.inp>


/***********************************************************************
 *
 * WptEvent related data structure used by Wpt and by applications.
 *
 ****************  WARNING: coordinate changes to this with the
 ****************  equialent _cplusplus structure below !!
 *
 *  NOTES: 
 *
 *      The field valueIndex, meaningful for multi-component parameters, 
 * 	indicates the value component that changed to generate this event.
 *	A value of -1 indicates any component (one or more) has changed,
 *	as happens for a PageEdit editor. 
 *
 *	The field 'subtype' indicates whether the event is generated due 
 *	to a change in the items value (WPT_VALUE), or just due to its 
 *	selection (WPT_SELECTION). The former is the default case.
 *	The later is specific to non-selection items (such as PageEdit,
 *	and MultiText), and needed for special WPT applications, that is,
 *	the workbench and Facelift Tutor (TM).
 *
 *********************************************************************/


#ifndef  __cplusplus				/* for C programs */
#include 	<vminc.inc>
#ifdef VMS
#include	<Xlib.h>
#include	<Intrinsic.h>
#else
#include	<X11/Xlib.h>
#include	<X11/Intrinsic.h>
#endif

    typedef  struct
	{
	GENPTR  	userContext;	/* user given wpt panel context */
	Id		dataVm;		/* parameter data block 	*/
	struct VARIABLE *variable;	/* ptr to parm in data block	*/
	int		valueIndex;	/* index to value vector 	*/
   	int		subtype;	/* event subtype: value/selection  */
	Id		panelId;	/* Id of correponding wpt panel */ 
	Window		windowId;	/* panels's window Id		*/
	Id	        panelWidgetId;  /* panel Widget Id		*/
	Window		relativeWindow; /* panel created relative to:   */	
	Window		eventWindow;	/* X window where event occured */ 
	XEvent		xEvent;		/* associated X event, for WS only */
	short		scripted;	/* true if scripted event       */
	short		scriptFd;	/* fd of script command         */
        int		dummy[3];	/* for future			*/
	} WptParmEvent;

    typedef  struct
      {
      GENPTR          userContext;    /* user given context           */
        int           dummy[4];       /* for future                   */
      } WptMessageEvent;

    typedef  struct
      {
      Id            id;
      BOOL	    repeat;
      TAEINT	    interval;
      GENPTR        userContext;    /* user given context           */
      int           dummy[4];       /* for future                   */
      } WptTimerEvent;


#else 					/* in __cplusplus */
#include     <wptdefs.h>

// Intrinsic.h eventually includes unistd.h and stdio.h.
// The Centerline compiler has a problem when either unistd.h or stdio.h
// is included in an extern "C" {} block and the other is not included
// in an extern "C" {} block.
#ifdef __CLCC__
#include <X11/Intrinsic.h>
#else
extern "C"
{
#include <X11/Intrinsic.h>
}
#endif

/*
#ifdef VMS
#include     "$taeincxm:symbol.h"
#include     "$taeincxm:widgetagent.h"	
#else
#include     <symbol.h>
#include     <widgetagent.h>	
#endif
*/

   typedef struct
	{
	void 		*userContext;	// user given wpt panel context 
	Id		dataVm;		// parameter data block
	Id		variable;	// ptr to parm in data block	
	int		valueIndex;	// index to value vector 
   	int		subtype;	// event subtype: value/selection 
	Id		panelId;	// Id of correponding wpt panel
	Window		windowId;	// panels's window Id
	Widget		panelWidgetId;	// panel Widget Id	
	Window		relativeWindow; // panel created relative to:   
	Window		eventWindow;	// X window where event occured
	XEvent		xEvent;		// associated X event, for WS only 
	short		scripted;	// true if scripted event
	short		scriptFd;	// fd of script command
        int		dummy[3];	// for future		
	} WptParmEvent;
    
    typedef struct
      {
      void            *userContext;   // user given context
      int           dummy[4];       // for future
      } WptMessageEvent;

    typedef  struct
      {
      Id            id;
      BOOL	    repeat;
      TAEINT	    interval;
      void          *userContext;    // user given context
      int           dummy[4];        // for future
      } WptTimerEvent;

#endif   
/**********************************************************************/

   

   typedef struct
	{
	Window		windowId;	/* window Id of X window 	*/
 	XEvent		xEvent;		/* event received from X	*/
	} WptWindowEvent;


   typedef struct
	{
 	int		fileSource;	/* file descriptor 		*/	
	int	 	fileEventType;	/* Read/Write/Exception		*/	
	} WptFileEvent;

   typedef union
	{
	WptParmEvent 	ParmEvent;
	WptWindowEvent  WindowEvent;
	WptFileEvent	FileEvent;
        WptMessageEvent MessageEvent;
        WptTimerEvent   TimerEvent;
	} WptAnyEvent;

   typedef struct
	{
	CODE		eventType;	/* WPT_PARM_EVENT		*/
   	TEXT		*parmName;	/* ptr to parameter name	*/
	WptAnyEvent	AnyEvent;	/* union - data for event type	*/
	} WptEvent;

/***********************************************************************/



#endif

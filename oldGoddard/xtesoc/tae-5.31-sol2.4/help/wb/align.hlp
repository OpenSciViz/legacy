.help
                       CURRENT SELECTION ALIGNMENT

PURPOSE:  The Current Selection Alignment panel is used to define the 
type of alignment to be used on a set of selected objects (items or 
panels). Use the WorkBench's Multiple Selection capability to select the 
objects, and then use the options on this panel to specify the 
alignment.  Once specified, a particular type of alignment can be reused 
on different sets of objects via the Align operation.

EXAMPLE:  If you have a number of push buttons in a row that you would 
like evenly distributed in a straight line, select all the push buttons, 
then choose "Distribute" for the Horizontal and "Center" for the 
Vertical alignment values.  Pressing Apply will perform the alignment.

HINT:  For most alignments, the object first selected is used as an 
anchor.  Other objects are aligned to it.  For "distribute" alignment, 
the two outermost objects are used as anchors, and the other objects are 
equally spaced between them.

SEE ALSO:  Align operation under the Arrange pulldown menu.

.help line1
                       CURRENT SELECTION ALIGNMENT

PURPOSE:  The Current Selection Alignment panel is used to define the 
type of alignment to be used on a set of selected objects (items or 
panels). Use the WorkBench's Multiple Selection capability to select the 
objects, and then use the options on this panel to specify the 
alignment.  Once specified, a particular type of alignment can be reused 
on different sets of objects via the Align operation.

EXAMPLE:  If you have a number of push buttons in a row that you would 
like evenly distributed in a straight line, select all the push buttons, 
then choose "Distribute" for the Horizontal and "Center" for the 
Vertical alignment values.  Pressing Apply will perform the alignment.

HINT:  For most alignments, the object first selected is used as an 
anchor.  Other objects are aligned to it.  For "distribute" alignment, 
the two outermost objects are used as anchors, and the other objects are 
equally spaced between them.

SEE ALSO:  Align operation under the Arrange pulldown menu.

.help showalign
                           ALIGNMENT WORKSPACE

PURPOSE:  This area is used to illustrate the alignment resulting from 
the vertical and horizontal selections.  This representation of the 
alignment may be used to determine if the correct combination has been 
selected prior to clicking on the Apply push button.  

.help apply
                                  APPLY

PURPOSE:  Applies the specified alignment to the current selection. The 
Current Selection Alignment panel remains on the display so that the 
designer may try other changes.

.help halign
                           HORIZONTAL ALIGNMENT

PURPOSE:  Used to specify how the currently selected objects will be 
aligned along the horizontal dimension.  

SELECTIONS:

     none          No alignment along the horizontal dimension.

     left          The left sides of selected objects are aligned.

     center        The horizontal centers of selected objects are 
                   aligned.

     right         The right sides of selected objects are aligned.

     distribute    The selected objects are equally spaced between the 
                   left-most and right-most objects.

.help cancel
                                   CLOSE

PURPOSE:  Removes the panel from the display. All selections are saved 
and may be applied by choosing the Align operation under the Arrange 
pulldown menu on the main Workbench panel.

.help valign
                             VERTICAL ALIGNMENT

PURPOSE:  Used to specify how the currently selected objects will be 
aligned along the vertical dimension.  

SELECTIONS:

     none         No alignment along the vertical dimension.

     top          The tops of selected objects are aligned.

     center       The vertical centers of selected objects are aligned.

     bottom       The bottoms of selected objects are aligned.

     distribute   The selected objects are equally spaced between the 
                  top-most and bottom-most objects.

.help ok
                                   OK

PURPOSE:  Applies the specified alignment and closes the Current 
Selection Alignment panel.

.help
                        SCALE (SLIDER) PRESENTATION PANEL

PURPOSE:  The Scale Presentation panel allows the WorkBench user to 
control the detailed appearance of the Scale item.  Select HELP for 
individual items in this panel for further details.

.help NoName01
                        SCALE (SLIDER) PRESENTATION PANEL

PURPOSE:  The Scale Presentation panel allows the WorkBench user to 
control the detailed appearance of the Scale item.  Select HELP for 
individual items in this panel for further details.

.help decimalPts
                               DECIMAL PLACES

PURPOSE:  Specifies the number of decimal places to the right of the 
decimal point used in displaying the value of a REAL-valued Scale.  This 
attribute is ignored for INTEGER-valued Scale items.

EXAMPLE:  If your Scale is integer data type, this value must be 0. If 
your Scale is real data type, you may select a meaningful number of 
decimal places. 

NOTE:  Changing this value does not effect the precision of the Scale's 
actual value.

DEFAULT:  0.

.help showValue
                            DISPLAY NUMERIC VALUE

PURPOSE:  Controls whether or not the Scale's numeric value is displayed 
with the Scale and automatically updated as the Scale is adjusted.

DEFAULT:  Yes.

.help direction
                              PROCESSING DIRECTION

PURPOSE:  Specifies the direction in which numbers will increase or 
decrease.

SELECTIONS:  The two options are:

     Ascending     Horizontal Scales will INCREASE when moved toward the 
                   RIGHT, and Vertical Scales will INCREASE when moved 
                   toward the TOP.

     Descending    Horizontal Scales will DECREASE when moved toward the 
                   RIGHT, and vertical Scales will DECREASE when moved 
                   toward the TOP.

DEFAULT:  Ascending.

.help orientation
                             SCALE ORIENTATION

PURPOSE:  Specifies the orientation of the Scale (Slider).

SELECTIONS: 
          
     Vertical      The Scale's slider will be moved vertically. 

     Horizontal    The Scale's slider will be moved horizontally.

EXAMPLE:  Once you have selected the Scale Orientation, use the 
Processing Direction to specify the direction in which numbers will
increase or decrease.

DEFAULT:  Vertical.

.help continued_event
                          EVENT REPORTING

PURPOSE:  Specifies when item events will be reported to the 
application.

SELECTIONS:  

   Continuously            Events are generated continuously as the 
                           Scale is being moved.

   Only on Button Release  An event is generated only after Scale 
                           movement has stopped and the button released.

DEFAULT:  Only on Button Release.

HINT:  Using "Continuously" puts a heavy processing load on the 
application.  Only use this option when necessary.

.help range_label
                     SCALE RANGE MINIMUM AND MAXIMUM

PURPOSE:  Used to specify the Scale's minimum and maximum range. Negative 
numbers are acceptable.

DEFAULTS:  0 and 100.

.help sliderThickness
                            SLIDER THICKNESS

PURPOSE:  Specifies the thickness of the slider (in pixels).

DEFAULT:  20 pixels.

.help quit
                                 CLOSE

PURPOSE:  Removes the Scale Presentation panel from the display.

.help minimum
                           SCALE RANGE MINIMUM

PURPOSE:  Specifies the minimum value of the Scale's range. Negative 
numbers are acceptable.  

DEFAULT:  0.

.help maximum
                           SCALE RANGE MAXIMUM

PURPOSE:  Specifies the maximum value of the Scale's range. Negative 
numbers are acceptable, provided that maximum is greater than minimum.

DEFAULT:  100.

.help ScaleMultiple
                           MULTIPLE (INCREMENT)

PURPOSE:  Specifies the increment (or decrement) in value that results 
from a click on the slider or from arrow key usage.  

DEFAULT: 10.


.help
                              ICON PRESENTATION PANEL

PURPOSE:  The Icon Presentation panel is used to specify additional item 
characteristics for an Icon.  

SELECTIONS:  The following Icon characteristics can be specified: 

   File Name       The name of a bitmap file containing a picture in X 
                   bitmap form, which can optionally be used to 
                   initialize the picture portion of the Icon item.

   Help Item       When this is enabled, the Icon item will behave as a 
                   "Help" icon.

   Icon Rotation   The angle of rotation to apply to the Icon, in 
                   degrees.

   Icon Width      The width of the Icon picture in pixels.

   Icon Height     The height of the Icon picture in pixels.

.help line1
.reference .help

.help quit
                                   CLOSE

PURPOSE:  Removes the Icon Presentation panel from the display without 
further changes to the item.

.help iconwidth 
                                 ICON WIDTH

PURPOSE:  Specifies the width for the Icon picture (bitmap) in pixels. 
The value must be a multiple of 16; however, Width*Height must be <= 
9600.  This setting cannot be used to change the width of an existing 
bitmap.  If a bitmap is loaded from a file, the icon width is 
automatically set to match that of the loaded bitmap.

DEFAULT:  48 pixels.

.help iconheight
                                ICON HEIGHT

PURPOSE:  Specifies the height for the Icon picture (bitmap) in pixels. 
The value must be a multiple of 16; however, Width*Height must be <= 
9600.  This setting cannot be used to change the height of an existing 
bitmap.  If a bitmap is loaded from a file, the icon height is 
automatically set to match that of the loaded bitmap.

DEFAULT:  48 pixels.

.help editfile
                                   EDIT 

PURPOSE:  Used to create or edit an Icon picture.  When selected, the 
"bitmap" editor is displayed, and you are presented with either: 1) a 
blank bitmap of your specified height and width for you to create a new 
icon, 2) the current Icon picture for you to edit (if no File Name is 
specified), or 3) a bitmap loaded from the specified File Name for you 
to edit or use as is for your Icon Picture.


.help helpsem
                                HELP ITEM

PURPOSE:  Specifies whether or not the Icon item will be used as a "Help" 
Item. When selected, a "Help" Item icon causes the application to enter 
help mode. The user may then ask for help on any other item within the 
panel containing this icon (or on the panel itself).

DEFAULT:  Not a "Help" Item.

NOTE:  An application will not receive events from an icon that is 
defined to be a "Help" Item. 

SEE ALSO:  The Edit Help File push button in the middle of the Panel 
Details panel provides information on editing a Help File.

.help iconfile
                                FILE NAME

PURPOSE:  Specifies the name of an associated bitmap file. This file can 
be used to load an existing bitmap or to save one you create using the 
bitmap editor.

SEE ALSO:  The Edit push button will provide you with additional 
information about existing and new File Names.


.help rotation
                              ICON ROTATION

PURPOSE:  Specifies an angle of rotation (in degrees) to apply to the 
icon. This value can be changed dynamically by the application program 
to obtain various animation effects with the icon. 

EXAMPLE:  If you enter 90 degrees, the icon is oriented a quarter turn 
clockwise.

DEFAULT:  0.0 degrees.


.help fileL
.reference .help iconfile

.help labelfg
                          TITLE FOREGROUND COLOR

PURPOSE:  Specifies the color to use for the icon title text.  Enter the 
desired color into the keyin.  A value of "parent" indicates use of the panel's 
foreground color.  A value of "Icon Foreground" indicates use of the 
icon's foreground color as specified on the Item Specification panel.

DEFAULT:  Icon Foreground. 

.help fgL
.reference .help labelfg

.help fglist
.include $TAEHELP/wb/fg_pref1.hlp

     parent              Foreground color of the panel.

     Icon Foreground	 Foreground color of the icon as specified on the
			 Item Specification panel.

.include $TAEHELP/wb/fg_pref2.hlp

.help labelbg
                          TITLE BACKGROUND COLOR

PURPOSE:  Specifies the background color to use for the small 
rectangular area surrounding the title.  Enter the desired color 
into the keyin. A value of "parent" indicates use of the panel's background 
color.  A value of "Icon Background" indicates use of the icon's background 
color as specified on the Item Specification panel.

DEFAULT:  Icon Background. 
 
.help bgL
.reference .help labelbg

.help bglist
.include $TAEHELP/wb/bg_pref1.hlp

     parent              Background color of the panel.

     Icon Background	 Background color of the icon as specified on the
			 Item Specification panel.

.include $TAEHELP/wb/bg_pref2.hlp

.help select
				SELECT

PURPOSE:  Displays a file selection dialog box to aid in specifying
the name of the file containing the bitmap for the Icon.

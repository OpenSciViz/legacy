.help
                          COMPOSITE PRESENTATION PANEL

PURPOSE:  The Composite Presentation Panel is used to specify the unique
details of the Composite DDO and to access the Graphics Editor for drawing
the DDO pictures.

EXAMPLE:  The Composite DDO may be used to represent an entire control
panel of dials, counters, status indicators, or anything else you could
represent with any other the other DDOs.

SELECTIONS:

  Item Level:  These resources apply to the Composite DDO item, not to
               the individual dynamics which comprise it.

     Picture File Name     The name of the Graphics Editor file
                           containing DDO pictures for the Composite.

     Number of Dynamics    Defines the maximum number of dynamics this
                           DDO will support.  The associated DDO picture
                           file should have less than or equal to this
                           number of dynamic picture objects.

     Edit                  Used to call up the Graphics Editor to
                           create or edit Composite DDO pictures.

  Common per Dynamic:  These resource are present in each dynamic in the
                       Composite DDO item, but they are defined independently
                       of the other dynamics in the Composite DDO.

     Range Minimum         Minimum display value for the dynamic.  Values
                           less than this will be displayed as if the
                           value were equal to the minimum.

     Range Maximum         Maximum display value for the dynamic.  Values
                           more than this will be displayed as if the
                           value were equal to the maximum.

     Initial Value         The dynamic's values when the DDO is first
                           displayed.  Can also be used to see the dynamic set
                           to different values during design in the
                           WorkBench.  In the "Set Item Default" WorkBench
                           mode, initial values can also be set via direct
                           manipulation of an input-enabled dynamic.

     Value Display         When enabled, the Value picture is displayed
                           with the dynamic.

     Value Format          Overrides the format used for the value picture.

     Update Delta          The change in value required before the dynamic
                           display is updated.  During direct manipulation
                           of an input-enabled dynamic, this resource controls
                           how much the user has to move the mouse pointer
                           before the dynamic will move.  The target will not
                           change (and an event will not be reported) until the
                           user has moved the dynamic in excess of the
                           update delta.

     Threshold Value       Used to specify the values at which a specified
                           color or picture will be used for the Dynamic
                           DDO picture.  Does not apply to 2D mover dynamics.

  Dynamic Specific:  These resource are present only for the dynamic types
                     to which they apply.

     Mover Direction       The orientation of movement made by a Mover
                           dynamic (either horizontal, vertical, or both).

     Maximum Rotation      Overrides the maximum rotation angle defined by
     Angle                 the Stop picture.  Applies only to Rotator dynamics.

     Horizontal Rotation   Together, these define a default point around
     Point and Vertical    which the dynamic will rotate.  This default is
     Rotation Point        used only if no Pivot picture is defined for the
                           dynamic.  Applies only to Rotator dynamics.

     Rotation Direction    The direction in which the dynamic
                           will rotate (clockwise or counter-clockwise).
                           Applies only to Rotator dynamics.

     Stretcher Direction   The direction in which a Stretcher dynamic
                           will stretch.  Horizontal = left to right;
                           Vertical = bottom to top.

     Interval              The time delay to wait before plotting the next
                           chart segment.  Applies only to Stripchart dynamics.

     Segments per Line     Number of line segments to be drawn across the
                           width of the plotting area.  Applies only to
                           Stripchart dynamics.

PICTURE NAMES:  A Composite DDO is made up of the following DDO pictures:

  Required DDO Picture Names:

     static          The fixed (background) portion of the Composite DDO item.

     dynamic(1-n)    The dynamic portion(s) of the Composite DDO item.
                     If the Number of Dynamics is greater than one, then
                     additional dynamics may be specified.  The number
                     at the end of the dynamic name is the index of
                     the dynamic.  For example, dynamic1 (or dynamic)
                     is the first dynamic, dynamic2 is the second...
                     and so on.  If the dynamic index is greater than
                     the Number of Dynamics, then the excess will be ignored.

  Optional DDO Picture Names:


     start(1-n)      The position of the a Rotator dynamic corresponding to
                     the dynamic's Range Minimum.  The Start and Stop pictures
                     together define the angle of rotation for the Rotator
                     dynamic, provided no explicit Maximum Rotation Angle is
                     specified.  This picture only applies to Rotator dynamics.

     stop(1-n)       Maximum location of the dynamic.
                     DEFAULT:  Top or right side.

     pivot(1-n)      The pivot point about which a Rotator dynamic
                     will rotate.  If no Pivot picture is specified, the
                     Default Vertical and Horizontal Rotation points
                     will be used.  Does not apply to other dynamic types.

     title           The location, font, and color for the title
                     specified in the Item Specification panel.

     minlabel(1-n)   The location, font, color and format of the label
                     for the dynamic's minimum value.

     maxlabel(1-n)   The location, font, color and format of the label
                     for the dynamic's maximum value.

     value(1-n)      The location, font, color  and format for the
                     dynamic's current value.  If Value Format is set for
                     the dynamic, then this format is overridden.

NOTE 1:  The start point for a Mover dynamic is always at the left side (for
horizontal), top (for vertical), or lower left (for 2D movers).

NOTE 2:  A Rotator dynamic must be drawn in a position corresponding to the
dynamic's Range Minimum value.

NOTE 3:  There are two ways to define the angle of rotation for a Rotator
dynamic: 1) by explicitly setting Maximum Rotation Angle, and 2) by leaving
the angle = 0.0 and defining Start and Stop pictures for the Rotator dynamic.

.help pictureFileB
.reference .help
.help mappingB
.reference .help
.help inputPropB
.reference .help
.help commonFB
.reference .help
.help specificFB
.reference .help

.help pictureFileL
.reference .help file
.help pictureNameL
.reference .help file

.help file
                             PICTURE FILE NAME

PURPOSE:  Specifies the name of the Graphics Editor file containing the
DDO pictures for the Composite.

SEE ALSO:  Help on the Edit push button and the panel background will provide
you with a list of the DDO pictures that make up a Composite DDO.

.help edit
                                 EDIT

PURPOSE: The Edit push button is used to create or edit the individual
pictures that make up the DDO.  When you click on this push button, the
Graphics Editor is displayed.  As part of defining the DDO in the
Graphics Editor, you must both draw the individual pictures needed
(required and optional), and then assign them the appropriate picture
names.

PICTURE NAMES:  A Composite DDO is made up of the following DDO pictures:

  Required DDO Picture Names:

     static          The fixed (background) portion of the Composite DDO item.

     dynamic(1-n)    The dynamic portion(s) of the Composite DDO item.
                     If the Number of Dynamics is greater than one, then
                     additional dynamics may be specified.  The number
                     at the end of the dynamic name is the index of
                     the dynamic.  For example, dynamic1 (or dynamic)
                     is the first dynamic.  dynamic2 is the second...
                     and so on.  If the dynamic index is greater than
                     the Number of Dynamics, then the excess will be ignored.

      start(1-n)     The position of the a Rotator dynamic corresponding to
                     the dynamic's Range Minimum.  The Start and Stop pictures
                     together define the angle of rotation for the Rotator
                     dynamic, provided no explicit Maximum Rotation Angle is
                     specified.  This picture is required only for Rotator
                     dynamics, and does not apply to all other dynamic types.

  Optional DDO Picture Names:

     stop(1-n)       Maximum location of the dynamic.
                     DEFAULT:  Top or right side.

     pivot(1-n)      The pivot point about which a Rotator dynamic
                     will rotate.  If no Pivot picture is specified, the
                     Default Vertical and Horizontal Rotation points
                     will be used.  Does not apply to other dynamic types.

     title           The location, font, and color for the title
                     specified in the Item Specification panel.

     minlabel(1-n)   The location, font, color and format of the label
                     for the dynamic's minimum value.

     maxlabel(1-n)   The location, font, color and format of the label
                     for the dynamic's maximum value.

     value(1-n)      The location, font, color  and format for the
                     dynamic's current value.  If Value Format is set for
                     the dynamic, then this format is overridden.

NOTE 1:  The start point for a Mover dynamic is always at the left side (for
horizontal), bottom (for vertical), or lower left (for 2D movers).

NOTE 2:  A Rotator dynamic must be drawn in a position corresponding to the
dynamic's Range Minimum value.

NOTE 3:  There are two ways to define the angle of rotation for a Rotator
dynamic: 1) by explicitly setting Maximum Rotation Angle, and 2) by leaving
the angle = 0.0 and defining Start and Stop pictures for the Rotator dynamic.

.help select
                           SELECT

PURPOSE:  Displays a file selection dialog box to aid in specifying
the name of the Graphics Editor file containing the DDO pictures
for the Composite.

SEE ALSO:  Help on the Edit push button will provide you with a list of
the DDO picture objects that make up a Composite DDO.

.help mappingL
.reference .help mapping
.help objectNameL
.reference .help mapping
.help udoTypeL
.reference .help mapping

.help mapping
                           DYNAMIC MAPPING

PURPOSE:  Shows the current mapping of all dynamics with their associated
types.  Whenever you change the type of a dynamic with the Dynamic Type
option menu, this list is updated to reflect the new type of the currently
selected dynamic.  If no type has been assigned, or if you change the type
to <None>, then nothing will appear in the Type column of this list.  When
you make a selection in this list, the values of the selected dynamic are
loaded into the fields of the Composite Presentation panel.  Also, items
in the dynamic-specific fields region appear or disappear according to the
type of the selected dynamic.  When you change the number of dynamics (as
specified in the Number of Dynamics keyin), this list is updated to reflect
the new number of dynamics.  If you change the number of dynamics to be
greater than what it was before (e.g., change from 3 to 5), then additional
dynamics (with no assigned type) will be appended to the Dynamic Mapping list.
If you change the number of dynamics to be less than what it was before (e.g.,
change from 5 to 3), then dynamics will be deleted from the end of the Dynamic
Mapping list.  This operation can be reversed by pressing the Undo button on
the WorkBench main panel.

.help numDynamicsL
.reference .help numDynamics

.help numDynamics
                        NUMBER OF DYNAMICS

PURPOSE:  Defines the maximum number of dynamics (of any type) that this
DDO will support.  The associated DDO picture file should have less than
or equal to this number of dynamic picture objects.  When you change the
number of dynamics of the Composite DDO, the Dynamic Mapping list will
be updated to reflect the new number of dynamics.  If you change this
number to be greater than what it was before (e.g., change from 3 to 5), then
additional dynamics (with no assigned type) will be appended to the Dynamic
Mapping list.  If you change the number of dynamics to be less than what
it was before (e.g., change from 5 to 3), then dynamics will be deleted from
the end of the Dynamic Mapping list.  This operation can be reversed by
pressing the Undo button on the WorkBench main panel.

.help dynamicTypes
                            DYNAMIC TYPE

PURPOSE:  Specifies the type of the dynamic currently selected in the
Dynamic Mapping selection list.  The type of the currently selected
dynamic appears in the Type column of the Dynamic Mapping list.  Also,
items in the dynamic-specific fields region appear or disappear according
to the newly assigned type of the current dynamic.

SELECTIONS:

     <None>               No type defined.  Before a type is defined, you
                          cannot set any other characteristics for the
                          current dynamic.

     Discrete Pictures    The selected dynamic is a Discrete Pictures dynamic.

     Mover                The selected dynamic is a Mover.

     Rotator              The selected dynamic is a Rotator.

     Stretcher            The selected dynamic is a Stretcher.

     Stripchart           The selected dynamic is a Stripchart.

DEFAULT:  <None>.

.help setType
PURPOSE:  Sets the type of the currently selected dynamic.  Currently not used.

.help inputL
.reference .help input_enable

.help input_enable
                    INPUT ENABLED FOR DYNAMIC

PURPOSE:  Determines whether or not direct manipulation can be used
to change the position of the dynamic, both in the WorkBench and
in an application.  When this checkbox is on, the dynamic will respond
to mouse input from both the interface designer and the end-user.
Also, the During Drag checkbox will be enabled.  When this checkbox
is off, the dynamic will be insensitive to input, and the During Drag
checkbox will be dimmed.

.help reportL
                    REPORT EVENTS

PURPOSE:  These three checkboxes specify the event-reporting
behavior of the dynamic.  They are only enabled if the Composite DDO
item is event-generating (set from the Item Specification Panel).

SEE ALSO:  Help on the individual event-reporting checkboxes
will provide specific information.

.help report_begin
                        AT BEGINNING

PURPOSE:  When this checkbox is on, an event will be reported when
the user clicks the mouse button down on this dynamic.  This checkbox
is only enabled if the Composite DDO item is event-generating (set
from the Item Specification Panel).

DEFAULT: Do not report events at beginning.

.help report_drag
                        DURING DRAG

PURPOSE:  Determines whether or not events are reported as the
user drags the mouse after initially clicking on this dynamic
and holding down the mouse button.  If this checkbox is on, then
an event will be reported during a drag each time the end-user
drags the dynamic past its Update Delta.  This checkbox is only enabled
if the dynamic is input-enabled (set from the Presentation Panel), and
the Composite DDO item is event-generating (set from the Item
Specification Panel).

DEFAULT: Do not report events during drag.

.help report_end
                        AT END

PURPOSE:  When this checkbox is on, an event will be reported when
the user releases the mouse button after having clicked on this
dynamic.  This checkbox is only enabled if the Composite DDO item
is event-generating (set from the Item Specification Panel).

DEFAULT: Report events at end.

.help commonFL
                            COMMON FIELDS

PURPOSE:  The resources defined by the fields in this group are present in
each dynamic in the Composite DDO item, but they are defined independently
of the other dynamics in the Composite DDO.  For instance, each dynamic
can have a different Update Delta.  This is not the case in a simple
(non-Composite) DDO, where these resources (except Initial Value) are
shared by each dynamic in the item.

NOTE:  The fields in this group do not change when you change the Dynamic
Type of a dynamic.  Of course, their values change if you select a new
dynamic from the Dynamic Mapping list.

.help min
                             RANGE MINIMUM

PURPOSE:  Specifies the minimum displayed value for this dynamic.  Values
less than this are displayed as the minimum.  This value, combined with the
Range Maximum, defines the display range for this dynamic.

SEE ALSO: Help on the Color Thresholds list will provide you with an
example of how the data values affect the picture being displayed.  For
a discrete pictures dynamic type, refer to the Picture Thresholds list.

DEFAULT:  0.0.

.help max
                            RANGE MAXIMUM

PURPOSE: Specifies the maximum displayed value for this dynamic.  Values
more than this are displayed as the maximum.  This value, combined with the
Range Minimum, defines the display range for this dynamic.

SEE ALSO: Help on the Color Thresholds list will provide you with an
example of how the data values affect the picture being displayed.  For
a discrete pictures dynamic type, refer to the Picture Thresholds list.

DEFAULT:  100.0.

.help delta
                              UPDATE DELTA

PURPOSE:  Specifies the change in the item value required before the
Mover display will be updated, whether by an application or by the end-user.

EXAMPLE:  If you specify 1.0 in the Update Delta of a mover dynamic type,
the dynamic will only be updated when the its data value has changed by
at least 1.0.  This could be used to display a graphical counter where the
user only sees changes of one full tick-mark, and to constrain the end-user
to move the dynamic only along the tick-marks.

.help initialL
.reference .help dynamic_initial

.help dynamic_initial
                            INITIAL VALUE(S)

PURPOSE:  Specifies the default or initial value(s) for the dynamic when
displayed in the application.  It is also useful for viewing the effects
of a data value change on the dynamic during design in the WorkBench.
The number or numbers to the right of this field represent the target
index of the initial value directly to its left.  See help on Target
Index for additional information.

EXAMPLE:  To view the effect of a specific value from within the
WorkBench, enter that value in the Initial Value(s) field, and then type
the Escape key.  The dynamic will automatically update to reflect the new
value, including any color or picture change associated with defined Color
or Picture Thresholds.

NOTE:  The Initial Value field is unique in that it is not necessary to
press the Apply button on the Item Specification panel to update the
DDO item.  However, for all other attributes specified on this panel, you
must press Apply for them to take effect.  Specifically, after updating
Thresholds, be sure to press Apply before experimenting with different
Initial Values.

INPUT NOTE:  The Initial Value(s) field can be set on an input-enabled
dynamic by using direct manipulation on the dynamic while in "Set Item
Default" mode.  Note that you will have to close the Item Specification
panel to set the initial value in this manner.

.help targetIndex2
.reference .help targetL
.help targetIndex1
.reference .help targetL

.help targetL
                              TARGET INDEX

PURPOSE:  The number or numbers to the right of the Initial Value(s) field
represent the target index of the initial value directly to its left.
Target index 0 corresponds to the zeroth value stored in the target object,
often referenced by value[0].  Target index 1 corresponds to value[1], and
so on.  Although this information is not important to the interface designer,
it is included here for the programmer.  Only one target index is shown for
all dynamics except for 2D mover dynamics, which have two target values apiece.

.help venable
                       VALUE DISPLAY ENABLED/DISABLED

PURPOSE:  Indicates whether or not a text string representing the
dynamic's value should be displayed.  To display the value, you must
create a Value picture in the Graphics Editor and select the Enable button.

SELECTIONS:

     Enabled      If a Value picture has been defined for the dynamic,
                  then it will be displayed.

     Disabled     No value will be displayed.

DEFAULT:  Enabled.

SEE ALSO:  Help on the Value Format keyin will provide information on
how to specify the format of the Value picture.

.help displayL
.reference .help vformat

.help vformat
                             VALUE FORMAT

PURPOSE:  Specifies the format to use for displaying the Value picture.
If a Value picture has been defined for this dynamic, then the format of
the value, as specified in the idraw picture file, is overridden by this
format.

EXAMPLE:  The format is expressed as a "C" language printf format.  Here
are some examples:

          Format                    Desired Value Display
          ------                    ---------------------
          Value = %.0f              Value = 25
          $%.2f                     $25.13
          AACN+%.4f V               AACN+5.1250 V

.help specificFL
                          DYNAMIC SPECIFIC FIELDS

PURPOSE:  The resources defined by the fields in this group are present
only in the dynamic types to which they apply.  The fields on the Composite
Presentation panel appear or disappear depending on the type of the
dynamic selected in the Dynamic Mapping list.  When you change a
dynamic's type or select a new dynamic with a different type, the fields
in this group will change to match the type of the current dynamic.
No fields will be present before you have specified a dynamic type. 

.help mdir
                              MOVER DIRECTION

PURPOSE:  Specifies the orientation of movement for the dynamic.

SELECTIONS:

     Horizontal          Moves horizontally (left and right).

     Vertical            Moves vertically (up and down).

     Both (2D)           Moves in both directions.

DEFAULT:  Vertical.

.help sdir
                          STRETCHER DIRECTION

PURPOSE:  Defines which way the dynamic will stretch.

SELECTIONS:

     Vertical       Stretch vertically

     Horizontal     Stretch horizontally

DEFAULT:  Vertical.

.help pictlst
                              PICTURE LIST

PURPOSE:  The Picture List is a list of named pictures found in the file
specified by the Picture File Name keyin at the top of this panel. This
list is used to assign a particular picture to a range of data values.

EXAMPLE:  You have created and named a number of pictures using idraw,
and their names are now displayed in the Picture List.  To have a
specific picture displayed when the item's data value is 5.0 or greater,
enter 5.0 in the Threshold Value keyin, select the picture's name from
the Picture List, and then click on the Add/Replace push button.

.help ondemand
                        UPDATE ON DEMAND

PURPOSE:  Indicates to TAE the type of stripchart updating to perform.

When this checkbox is on, the interval is set to zero. This indicates to
TAE that this stripchart will only update (scroll) whenever the program
changes the target value(s). Typically, this is done with a Wpt_SetReal call.

When the checkbox is off, the stripchart will automatically update and
scroll whenever the number of milliseconds in the interval field expire.
The application should update the target values whenever the value changes.

SEE ALSO: Help on Interval.

.help interval
                               INTERVAL

PURPOSE:  Specifies the time delay (in milliseconds) to wait before
plotting the next chart segment. If the interval is set to zero (0)
the stripchart will only update on demand. See Help for "Update On Demand".

HINT:  Use values greater than 500 milliseconds.  Values less than 500
(1/2 second) may become somewhat unreliable due to system load. Gaps
in the stripchart may result because two or more timeouts may occur
before the stripchart has had a chance to obtain the values.

.help segmentsL
.reference .help segments

.help segments
                            NUMBER OF LINE SEGMENTS

PURPOSE:  Specifies the number of line segments to be drawn across the
width of the plotting area of this stripchart dynamic.

DEFAULT:  32.

.help coordL
.reference .help rotL2

.help rotL2
                        DEFAULT ROTATION POINT

PURPOSE:  Used to specify a default point about which the dynamic
will rotate.  This default is used only if a Pivot picture is
not defined for the dynamic.

SEE ALSO:  Help on the Horizontal and Vertical Rotation Point radio
buttons will provide information on how to specify the default rotation
point.

.help hrotpt
                         HORIZONTAL ROTATION POINT

PURPOSE:  In conjunction with the Vertical Rotation Point, defines the
horizontal location of a default rotation point about which the dynamic
rotates.  This default point is only used if no Pivot point picture is
defined for the dynamic.

SELECTIONS:

     Left       Default rotation point is at the left of the dynamic.

     Center     Default rotation point is at the center of the dynamic.

     Right      Default rotation point is at the right of the dynamic.

EXAMPLE:  If you want the default rotation point to be in the center
of the dynamic, select Center for both the Vertical and the Horizontal
Rotation Point.

DEFAULT:  Center.

NOTE:  The rotation point does not need to lie within the dynamic
picture.

.help vrotpt
                         VERTICAL ROTATION POINT

PURPOSE:  In conjunction with the Horizontal Rotation Point, defines the
vertical location of a default rotation point about which the dynamic
rotates.  This default point is only used if no Pivot point picture is
defined for the dynamic.

SELECTIONS:

     Top        Default rotation point is at the top of the dynamic.

     Center     Default rotation point is at the center of the dynamic.

     Bottom     Default rotation point is at the bottom of the dynamic.

EXAMPLE:  If you want the default rotation point to be in the center
of the Dynamic, select Center for both the Vertical and the Horizontal
Rotation Point.

DEFAULT:  Center.

NOTE:  The rotation point does not need to lie within the dynamic
picture.

.help maxrotL
.reference .help rotang

.help rotang
                             MAXIMUM ROTATION ANGLE

PURPOSE:  The Maximum Rotation Angle keyin is used to override the
maximum rotation angle of a rotator dynamic.  This number specifies
the rotation angle in degrees.

EXAMPLE:  If the value is 0.0, the rotation angle defined by the dynamic's
start and stop pictures (created in idraw) will specify the rotation angle.
An angle greater than 360 degrees will be reduced to less than 360 with a
modulo function.

DEFAULT:  The default value for this keyin is 0.0.

.help rdir
                           ROTATION DIRECTION

PURPOSE:  Specifies the direction of rotation for the dynamic.

SELECTIONS:

     Clockwise           Rotates clockwise.

     CounterClockwise    Rotates counter-clockwise.

DEFAULT:  Clockwise.

.help threshLabel
.reference .help threshes

.help threshes
                          COLOR/PICTURE THRESHOLDS

PURPOSE:  Displays a list of value thresholds in ascending order with
associated colors (or pictures).  As the dynamic value changes and crosses
the defined thresholds, the dynamic changes color or picture to match that
defined for the threshold.  Does not apply to 2D mover dynamics.

EXAMPLE:  The value associated with each color (or picture) represents
the minimum for that range number, and the next value (or the Range
Maximum if no more thresholds exist) represents the upper value for
that range.  Note that the minimum value is included in the range while
the upper value is not.

For the following Color Thresholds list:

                          0.00000 (yellow)
                         10.00000 (green)
                         15.00000 (red)

If the data value = 0.0 up to 9.9, then yellow is used for the Dynamic
picture.
If the data value = 10.0 up to 14.9 then green is used.
If the data value = 15.0 (or greater) then red is used.

For the following Picture Thresholds list:

                          0.00000 (pict1)
                         10.00000 (pict2)
                         15.00000 (pict3)

If the item value = 0.0 up to 9.9,  pict1 is displayed.
If the item value = 10.0 up to 14.9, pict2 is displayed.
If the item value = 15.0 (or greater), pict3 is displayed.

DEFAULT:  0.0 ().  In the case of color thresholds, this means that the default
picture (as created in taeidraw) will be displayed, in whatever color is
specified in the picture file.  In the case of picture thresholds, all
values result in the same picture being displayed.  Since no picture name
is given, the first one created is used.

NOTE:  Remember that you can define a different set of thresholds for each
dynamic in the Composite DDO.

.help addL
.reference .help add

.help add
                             ADD/REPLACE

PURPOSE:  Adds or Replaces an entry in the Color (or Picture) Thresholds
list. The particular entry to add or replace is specified in the Threshold
Value field.

EXAMPLE:  The Add/Replace button works as follows:

     If the Threshold Value matches an entry in the Color (or Picture)
     Threshold list, that value in the list is REPLACED.

     If the Threshold Value does NOT match an entry in the Color (or
     Picture) Threshold list, that value is ADDED to the list.

.help delthrL
.reference .help delete

.help delete
                                DELETE

PURPOSE:  Removes the currently selected entry from the Color (or Picture)
Thresholds list.

.help threshvL
.reference .help threshld

.help threshld
                            THRESHOLD VALUE

PURPOSE:  Specifies a threshold value of interest and in conjunction
with the ADD/REPLACE button, is used to build the Color (or Picture)
Thresholds list.

EXAMPLE:  To have blue displayed when the dynamic's data value is 5.0 or
greater, enter 5.0 in the Threshold Value, select the color blue, and
then click on the Add/Replace button.  For a Discrete Pictures dynamic,
an analogous procedure can be followed to have the 'pic1' picture displayed
when the dynamic's data value is 5.0 or greater.

DEFAULT:  0.0.

.help fglist
                              COLOR LIST

PURPOSE:  (1) Shows the currently selected color.
          (2) Provides a quick selection of your most common
              colors and any special color names that this
              resource supports (i.e. "parent").
          (3) Invokes the Color Editor

SELECTIONS:

     Color Editor        Invokes the Color Editor.  If the color
                         editor is already active, the current color
                         is loaded.

     DEFAULT             Display the dynamic using whatever color was
                         initially used to create it.

     Your current list   Colors can be added to this list using the Color
     of Foreground       Editor.  Colors can be added to, deleted from, or
     Preferences.        repositioned within this list using the Color/Font
                         Preferences panel (accessible from the WorkBench
                         Preferences panel.

.help colorL
.reference .help color

.help color
                                COLOR

PURPOSE:  The Color keyin is used to specify the particular color choice
to be assigned to a data value range.  Does not apply to 2D movers dynamics.

EXAMPLE:  To have blue used when the dynamic's data value is 5.0 or
greater, enter 5.0 for the Threshold Value, enter "blue" in the Color keyin
and then click on the Add/Replace push button.  Color changes only
affect the foreground color of the dynamic.

DEFAULT:  DEFAULT - this means display the dynamic using whatever color
was initially used to create it.

.help quit
                                CLOSE

PURPOSE:  Removes the Composite Presentation panel from the display
without further updates to the DDO.


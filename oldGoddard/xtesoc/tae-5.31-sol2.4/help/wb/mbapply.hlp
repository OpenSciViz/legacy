.help
			 MENUBAR APPLY PANEL

PURPOSE: This panel allows you to specify whether you wish to save the
changes you have made to the menubar details panel prior to closing the
item specification panel or quitting the WorkBench.  It appears whenever
you make changes to the menubar details panel and then attempt to either
close the item specification panel or quit the WorkBench without applying
your changes.

Since the menubar details panel can be quite complicated, this panel helps
prevent you from closing the item specification panel by accident and losing
all of your changes.  

This panel appears only when you have changed the details of a menubar item;
it will not appear with any other item type.  Closing the item specification
panel or quitting the WorkBench without applying changes to any other item
type will cause your changes to be discarded.

.help mb_label
	 APPLY MENUBAR DETAILS PANEL CHANGES BEFORE CLOSING?

PURPOSE:  Press Apply if you want to apply menubar details panel changes
before closing the item specification panel or quitting the WorkBench.  Press
Don't Apply if you want to continue without applying your changes.  Press 
Cancel if you don't want to close the item specification panel or quit
the WorkBench at this time.

.help cancel
				CANCEL

PURPOSE:  Pressing Cancel will cancel closing the item specification panel
or quitting the WorkBench, and will close this panel.

.help OK
				APPLY

PURPOSE:  Pressing Apply will cause changes you have made to the menubar
details panel to be applied before the item specification panel is closed
or the WorkBench is exited.

.help line1
.reference .help

.help dontapply
			     DON'T APPLY

PURPOSE: Pressing Don't Apply will cause whatever changes you have made
to the menubar details panel to be discarded before the item specification
panel is closed or the WorkBench is exited.


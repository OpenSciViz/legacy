.help
                         KEYIN PRESENTATION PANEL

PURPOSE:  The Keyin Presentation panel is used to specify those 
characteristics particular to the Keyin item.  

SELECTIONS:

   String Length          The maximum number of characters accepted by the 
                          keyin. This setting controls the default size of 
                          the keyin field.

   Fixed Title Width      Indicates whether or not to use a fixed character 
                          spacing between the left edge of the title and 
                          the left edge of the keyin box.

   Specify Error Message  Permits customization of runtime error message.


HINT:  Settings only take effect when the Apply or OK button on the Item 
Specification panel is pressed.

.help line1
.reference .help
.help quit
                                    CLOSE

PURPOSE:  Removes the Keyin Presentation panel from the display.

.help justify
                                FIXED TITLE WIDTH

PURPOSE:  Specifies where the keyin box should be placed in relationship 
to the title.

SELECTIONS:

     checked         The keyin box is placed at a fixed distance from 
                     the START of the title. This is useful for lining 
                     up a number of keyin items with variable length 
                     titles.

     not checked     The keyin box is placed at a fixed distance from 
                     the END of the title. The Keyin will be of minimum 
                     size with this option.

DEFAULT:  not checked.

.help strsize
                                 STRING LENGTH

PURPOSE:  Specifies the length of the keyin box by setting the maximum 
number of characters that can be entered.

DEFAULT:  20 characters.  

.help errormsg
                            SPECIFY ERROR MESSAGE

PURPOSE:  Allows designer to specify a customized (application specific)
error message which will replace all internal TAE Plus error messages
for this item.  (This customized error message can also be changed
at runtime by setting the "errormsg" qualifier and then calling
Wpt_ViewUpdate.) If this field is empty, the TAE default messages will
be used.

HINT:     The customized error message will be displayed when the end user:

            o  enters the wrong data type;
            o  enters an invalid string constraint;
            o  enters an invalid integer or real for the constraint 
               range; or
            o  erases the value from a non-nullable keyin.


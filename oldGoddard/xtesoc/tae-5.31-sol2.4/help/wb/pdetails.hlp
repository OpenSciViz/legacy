.help
                            PANEL DETAILS PANEL

PURPOSE:  The Panel Details Panel is used to specify detailed attributes 
for an application panel.  From this panel, you can:

     Designate frame characteristics, such as user resizability.

     Specify titlebar function, such as visibility and resizing.

     Specify a Preferred panel state.

     Develop and edit panel Help files.

     Design and edit Panel Icons.

HINT:  Press Apply on the Panel Specification panel to see changes made
on the Panel Details panel take effect.

.help NoName01
.reference .help
.help line1
.reference .help
.help NoName03
.reference .help
.help NoName04
.reference .help

.help border
                           FRAME BORDER WIDTH

PURPOSE:  If a flat border is selected, this parameter specifies its 
thickness. A value of 0 means no border will be displayed.

DEFAULT:  0 pixels.

HINT:  If an mwm frame is selected, Border Width is ignored.

.help edithelp
                            HELP FILE EDIT
          
PURPOSE:  Used to create or edit help text for an application panel. If 
the specified help file does not exist, a new skeleton help file will be 
created. This skeleton help file will contain place holders telling you 
where to place the help text for each item currently defined on the panel.

In order for the application user to access online help for an 
application panel, the panel must include a push button or icon defined 
as a help item.  The user selects the help item to get a ? cursor, and 
then clicks on the an item or the panel background to get the panel help
text.

NOTE:  For UNIX systems the help file is opened using vi, on VMS EDT is 
used.  These defaults can be overridden using the Xdefaults or 
DECW$XDEFAULTS file as necessary.

HINT:  It is generally recommended that you wait until all your item's
have been defined before you create the panel's help file.  This will
ensure that the help file skeleton has all of the necessary place holders. 

.help iconL
                               PANEL ICON

PURPOSE:  The Panel Icon section of this panel is used to specify an 
icon to be displayed whenever the panel is iconified, and to display the 
bitmap editor to create the icon bitmap.  The panel icon is stored in a 
bitmap file, which is read when the panel is used at runtime.  The icon 
can be changed by changing the bitmap file.

NOTE:  Panel icons are not stored in the resource file, so if they are
used in an application it will be necessary to deliver the panel icon file
along with the application. 

.help iconfileL
.reference .help iconfile

.help editicon
                                EDIT ICON

PURPOSE:  The Edit push button in the Panel Icon section of this panel 
is used to display the bitmap editor for creating or editing icons.

.help labelL
.reference .help iconlabel

.help panelL
                             PANEL ATTRIBUTES

PURPOSE:  The Panel Attributes section of this panel is used to specify 
the panel style and to display a second panel to specify panel 
dimensions.


.help iconfile
                              ICON FILE NAME

PURPOSE:  Specifies the name of the bitmap file where the panel icon 
bitmap is stored.

HINT:  Several different panels can reference the same bitmap file for 
their panel icon.

.help traversal
                            KEYBOARD TRAVERSAL

PURPOSE:  To enable traversal for the panel.

DEFAULT:  Disabled 

.help styleL
                                 STYLE

PURPOSE:  Style specifies which of several panel styles to use.  The 
style relates to frame characteristics and titlebar functionality.  The 
selected style is used to display the panel both in the WorkBench and in 
the application.  In this way, the designer sees the panels in the same 
way as the end user.  The designer can always resize and move panels in 
the WorkBench independent of the panel style.

HINT:  To create a panel which the application user cannot move or 
resize, you must use the following panel style:

     Frame        Flat border or no border (i.e., flat with width = 0)

     Titlebar     None

.help framestyle
                                 FRAME

PURPOSE:  Specifies the type of frame to use for the panel.  

SELECTIONS:  Options include:

     MWM - No Resize       A 3-D style, Motif frame with no resize 
                           handles.  With this style, the end user 
                           CANNOT resize the panel.

     MWM - With Resize     A 3-D style, Motif frame with resize handles.
                           With this style, the end user CAN resize the 
                           panel.

     Flat Border           A flat, 2-D frame of the specified width, or 
                           no frame (if width = 0).  With this style, 
                           the end user CANNOT resize the panel.

DEFAULT:  MWM - No Resize.

NOTE:  Control over whether or not the application user can move the 
panel is controlled by the Titlebar style.

HINT:  To create a panel which the application user cannot move or 
resize, you must use the following panel style:

     Frame        Flat border or no border (i.e., flat with width = 0)

     Titlebar     None

.help titlebarstyle
                         TITLEBAR FUNCTIONALITY

PURPOSE:  Used to specify the type of titlebar to be displayed with the 
panel. Note that a titlebar must be present for the end user to be able 
to move the panel during application execution.

SELECTIONS:  Options include:

     TAE Default    The titlebar includes only two icons: minimize 
                    (iconify) and activate mwm (system) menu.  The mwm 
                    menu is modified to exclude the maximize, size, and 
                    close operations.

     MWM Default    The titlebar is the default, full-featured mwm 
                    titlebar, which includes the maximize icon and menu 
                    activation with all menu choices available.

     None           No Title bar will be displayed with the panel.  In 
                    this case, the application user cannot move the 
                    panel.

DEFAULT:  TAE Default.

NOTE:  Control over whether or not the application user can resize the 
panel is controlled by the Frame style.

HINT:  To create a panel which the application user cannot move or 
resize, you must use the following panel style:

     Frame        Flat border or no border (i.e., flat with width = 0)

     Titlebar     None

.help iconlabel
                                ICON TITLE

PURPOSE:  Specifies the string that will serve as a label for the panel 
icon.  This label appears centered and below the bitmap picture.

.help iconwidth
                               ICON WIDTH 

PURPOSE:  Specifies the width of the icon bitmap in pixels, between 16
and 128.

DEFAULT:  50 pixels.

.help iconheight
                               ICON HEIGHT

PURPOSE:  Specifies the height of the icon bitmap in pixels, between 16 
and 128.

DEFAULT:  50 pixels.

.help state
                           PREFERRED PANEL STATE

PURPOSE:  Specifies a preferred display state for the panel.  This state 
is used under the following circumstances:

     -  The panel is displayed automatically as an initial panel for an 
        application.

     -  The panel is displayed as a result of a connection, where the 
        specified state for this panel is "Preferred".

     -  The panel is displayed via an explicit Wpt_NewPanel call that 
        specifies a state of "WPT_PREFERRED". 

SELECTIONS:

     Visible             The panel is fully built and displayed on the
                         screen.

     Visible and Modal   The panel is a "modal" panel (i.e., it must be
                         closed before interacting with any other panels
                         in the running application).  Modal panels are
                         always visible. Modal panels are only APPLICATION
                         modal as opposed to SYSTEM modal.

     Invisible           The panel is fully built but not displayed.

     Iconic              The panel is fully built and collapsed to a 
                         panel icon.

     Fast Iconic         The panel is only partially built and collapsed to
                         an icon.  The panel will be fully built the first
                         time it is made visible.

DEFAULT:  Visible.

.help helpfile
                          HELP FILE SPECIFICATION

PURPOSE:  Specifies the name of the help file for the panel. Pressing 
the "Edit..." button attempts to open and edit the named file. By 
default, the help file name is derived from the panel name.

EXAMPLE:  In order for the user to have access to the help text, one or 
more push buttons or icons on the panel MUST be defined as a "Help 
Item". 

     To define a push button or icon as a "Help Item":

     1) Modify or create a button or icon.

     2) Bring up the "Details" panel for that item. (Select the 
        "Details..." button on the Item Specification Panel)

     3) Check the "Help" Item checkbox.
        

Whenever the user selects the help button or icon, the standard TAE Plus 
Help mode is invoked.

.help helpfileL
.reference .help helpfile

.help helpL
.reference .help helpfile

.help close
                                 CLOSE

PURPOSE:  Removes the Panel Details panel and any sub-panels from the 
display.

.help selecticon
				SELECT

PURPOSE:  Displays a file selection dialog box to aid in specifying
the name of the file containing the icon box bitmap for the panel.

.help selecthelp
				SELECT

PURPOSE:  Displays a file selection dialog box to aid in specifying
the name of the file containing the help file for the panel.

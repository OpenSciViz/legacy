.help
                       MESSAGE PRESENTATION PANEL

PURPOSE:  The Message Presentation panel is used to specify attributes
specific to the Message item. 

Most selections are done in the shaded box at the top of the Message
Presentation Panel. This is the template area. It represents the Message
item under design.

Where attributes are not selectable in the template, they may be specified
in the area below the template.

SELECTIONS: 

     Button Size             Make your buttons all one size by selecting
                             "Uniform" in this field. Selecting "Compressed"
                             causes your buttons to shrink to exactly fit
                             their individual titles.

     Button Titles           Button titles can be entered by typing into
                             the keyin area of the relevant button in the
                             template.

     Components              A Message can be configured to include one or
                             more components from the following list:

                                 OK Button
                                 Cancel Button
                                 Help Button
                                 Separator

                             Click in the checkbox for the relevant component
                             to include or exclude it.
   
     Icon                    To specify the type of icon desired, click
                             on the icon in the template. This brings up a
                             palette of icons from which you can select one.

     Justification           Use the Justification field in the template to
                             specify desired message placement (left, right or
                             center).

     Message Text            Enter your message by typing it into the Message
                             Text field.

     TAE Help                If you want your Help button to behave like TAE
                             Help, turn this on. Turning this off makes your
                             Help button behave similar to the other Message
                             buttons.


.help align
			MESSAGE JUSTIFICATION

PURPOSE: Align the message in the message field.

SELECTIONS:

     Left             Place the message on the left side of the message field.

     Center           Center the message in the message field.

     Right            Place the message on the right side of the message field.

DEFAULT:  Left

.help alignL
.reference .help align


.help buttonsize

                               BUTTON SIZE

PURPOSE: Specify whether buttons should be of uniform size or compressed
         to fit individual button titles.

SELECTIONS:

     Uniform               Make all buttons the size of the button with the
                           largest title.

     Compressed            Individually size each button to fit its own title.

DEFAULT: Uniform

.help cancelC
                          DISPLAY CANCEL BUTTON

PURPOSE: Indicate whether the Cancel button should be displayed.

SELECTIONS:

     ON                       Display the Cancel button.

     OFF                      Hide the Cancel button.

DEFAULT: ON

.help canceltitle
                          CANCEL BUTTON TITLE

PURPOSE: Specify the title for the Cancel button by typing it into this field.

DEFAULT: Cancel

.help helpC
                          DISPLAY HELP BUTTON

PURPOSE: Indicate whether the Help button should be displayed.

SELECTIONS:

     ON                       Display the Help button.

     OFF                      Hide the Help button.

DEFAULT: ON

.help helptitle
                           HELP BUTTON TITLE

PURPOSE: Specify the title for the Help button by typing it into this field.

DEFAULT: Help

.help okC
                            DISPLAY OK BUTTON

PURPOSE: Indicate whether the OK button should be displayed.

SELECTIONS:

     ON                       Display the OK button.

     OFF                      Hide the OK button.

DEFAULT: ON

.help oktitle
                              OK BUTTON TITLE

PURPOSE: Specify the title for the OK button by typing it into this field.

DEFAULT: OK

.help separatorC
                              DISPLAY SEPARATOR

PURPOSE: Indicate whether the Message separator should be displayed.

SELECTIONS:

     ON                       Display the separator.

     OFF                      Hide the separator.

DEFAULT: ON

.help icon
                         ICON SELECTION

PURPOSE:  Specify the desired icon to appear in the Message.

To specify an icon, click on the icon in the template and then choose your
icon from the displayed palette.

Five type of icons are provided: Information, Warning, Error, Question
and Working.

DEFAULT: No icon.

.help boxcancel
.reference .help canceltitle

.help boxhelp
.reference .help helptitle

.help boxmessage
                            MESSAGE TEMPLATE AREA

PURPOSE: Allow the designer to specify Message attributes in an intuitive way.

The shaded area in the Message Presentation Panel is used to enter most of your
attribute specifications. 

By clicking or typing in the appropriate areas in the template, you can:
(a) include or exclude Message item components, (b) customize button
titles, (c) choose an icon to display and (d) align your message as
desired in the message field.

.help boxok
.reference .help oktitle

.help boxtitle
.reference .help title

.help line
.reference .help

.help message
.reference .help icon

.help information
.reference .help icon

.help warning
.reference .help icon

.help error
.reference .help icon

.help question
.reference .help icon

.help working
.reference .help icon

.help helpsem
                                TAE HELP

PURPOSE: Specify the behavior of the Message Help button.

SELECTIONS:

     ON                       Make the Help button a TAE Help button.

     OFF                      Make the Help button an application-defined
                              button which behaves similarly to the other
                              Message buttons.

DEFAULT:  ON

.help quit
                                  CLOSE

PURPOSE:  Removes the Message Presentation Panel from the display.

.help separator
.reference  .help boxmessage

.help text

                                  MESSAGE

PURPOSE: Specify the message for the Message item.

Enter your message by typing it into this field.

DEFAULT: No message.

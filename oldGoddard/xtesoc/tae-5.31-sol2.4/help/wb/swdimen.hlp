.help
                 DIMENSION SPECIFICATION PANEL (Scrollable)

PURPOSE:  The Dimension Specification panel is used to directly specify 
the origin and size of an object (item or panel).  This panel is 
accessed from either the Item Specification or Panel Details panels.  
This panel provides an alternative method to direct manipulation for 
precisely controlling the size or location of an object, and can be used 
to determine current size and location of an object precisely.

For scrollable panels or items, the Dimension Specification panel is
the only way to specify the Work Region width or height. (See Help on
Work Region Size.)

HINT:  This panel can be used to create different objects of the exact 
same size.  Press the Apply push button at the bottom of the Item or 
Panel Specification panel to indicate when changes should take effect.

.help originL
                                 ORIGIN

PURPOSE:  Specifies the location of the panel on the display or the item 
within the panel in screen pixel coordinates.  The origin for a panel is 
located at the upper left corner of the panel, including any frame or 
titlebar.  The origin for an item is located at the upper left corner of 
the item's rectangular bounding box. Item origin values are relative to 
the upper left corner of the panel, excluding the frame or titlebar.

DEFAULT:  The default location for the panel's origin is at the center 
of the screen. The default location for an item is at the center of its 
parent panel.

NOTE:  There is no way to specify the Work Region origin since it is
always at (x=0, y=0) with respect to the panel or item.

.help x
                                ORIGIN X

PURPOSE:  Specifies the X coordinate for the item or panel origin.  The 
origin for a panel is located at the upper left corner of the panel, 
including any frame or titlebar.  The origin for an item is located at 
the upper left corner of the item's rectangular bounding box.  Item 
origin values are relative to the upper left corner of the panel, 
excluding the frame or titlebar.


.help y
                                ORIGIN Y

PURPOSE:  Specifies the Y coordinate for the item or panel origin.  The 
origin for a panel is located at the upper left corner of the panel, 
including any frame or titlebar.  The origin for an item is located at 
the upper left corner of the item's rectangular bounding box.  Item 
origin values are relative to the upper left corner of the panel, 
excluding the frame or titlebar.

.help panelsizeL
                                   SIZE

PURPOSE:  Used to specify an item or panel's width and height in pixels.  

DEFAULT:  The default panel size is 400 x 400 pixels. Default item sizes 
depend on presentation type.  For example, the default size of an 
X Workspace is 200 x 200 pixels.


.help width
                                 WIDTH

PURPOSE:  Specifies the panel or item's width in pixels. 

DEFAULT:  The default panel width is 400 pixels.  The default item width 
depends on presentation type. 

.help height 

                                 HEIGHT 

PURPOSE:  Specifies the panel or item's height in pixels.

DEFAULT:  The default panel height is 400 pixels. The default item 
height depends on presentation type. 

.help workregionL
                            WORK REGION SIZE

PURPOSE:  Used to specify an item or panel's Work Region width and height
in pixels.  The Work Region is typically larger than the item or panel
and can even be larger than the physical screen. The Work Region icon provides
a graphical representation of the relationship of Work Region to Viewport.
The larger, heavily outlined box represents the Work Region and the smaller
box represents the Viewport. The Viewport can be thought of as what the
end-user sees while the Work Region is what the application sees.

A large Work Region is particularly useful for:

        (a) displaying huge graphics such as a map or graph in a
            limited amount of screen space (in an X Workspace); or

        (b) displaying a large panel containing many items, some of which
            are not initially accessible to the user; when the user 
            needs to access the other items, he simply scrolls the panel.


DEFAULT:  The default Work Region Size for a panel is the panel's size;
the default Work Region Size for an item is the item's size.

HINT:  The maximum Work Region width or height is 2000000000 pixels.
The scrollbar choice of "As Needed" is useful if you want scrollbars
to dynamically appear/disappear as appropriate when the panel or item
is resized in the WorkBench, and, for scrollable panels only, when
the panel is resized by the end user.

.help workregionSizeL
.reference .help workregionL
.help workWidth

                            WORK REGION WIDTH 

PURPOSE:  Specifies the panel or item's Work Region width in pixels.
In general, the Work Region width should be greater than the panel
or item's width.

DEFAULT:  The default panel or item Work Region width is the same as
the width of the panel or item itself.  Since the scrollbars take extra
space, by default the horizontal scrollbar will not be fully extended.

HINT:  Note that a fully extended scrollbar for items is not useful since 
the end user cannot use the window manager to resize items as he can 
resize panels.  If you want the scrollbar to be fully extended when a 
panel first appears, use the resize handles to directly increase the 
panel's width.

.help workHeight

                            WORK REGION HEIGHT

PURPOSE:  Specifies the panel or item's Work Region height in pixels.
In general, the Work Region height should be greater than the panel
or item's height.

DEFAULT:  The default panel or item Work Region height is the same as
the height of the panel or item itself.  Since the scrollbars take extra
space, by default the vertical scrollbar will not be fully extended.

HINT:  Note that a fully extended scrollbar for items is not useful since 
the end user cannot use the window manager to resize items as he can 
resize panels.  If you want the scrollbar to be fully extended when a 
panel first appears, use the resize handles to directly increase the 
panel's height.

.help close
                                  CLOSE

PURPOSE:  Removes the Dimension Specification panel from the display.  
All settings are saved and may be applied by clicking on the Apply push 
button at the bottom of the Item or Panel Specification panel.

EXAMPLE:  If you enter values for origin and/or size, and then click on 
the Close button, those values will be saved.  If you want to see the 
effect of the values, click on the Apply push button on the Item or 
Panel Specification panel.

.help help
.reference .help

.help vertLine
.reference .help

.help horizLine
.reference .help

.help line1
.reference .help

.help line2
.reference .help

.help viewportL
                        (VIEWPORT) ORIGIN AND SIZE

PURPOSE:  Used to specify an item or panel's origin and (Viewport) size
in pixels. Its origin is the panel's/item's origin and its width and height 
are the panel's/item's width and height. Typically, the Viewport displays a 
subset of the underlying Work Region. The user may scroll the Viewport to see 
other sections of the Work Region. Think of the Viewport as what the end user 
will see and think of the Work Region as what the application code will see.

The Viewport icon is simply a graphical representation of the Viewport to Work
Region relationship. In this icon, the Viewport is represented by the smaller
thick bordered box, while the Work Region is much larger and surrounds the
Viewport. 

Viewports and Work Regions are useful for:

        (a) displaying huge graphics such as a map or graph in a
            limited amount of screen space (in an X Workspace); or

        (b) displaying a large panel containing many items, some of which
            are not initially accessible to the user; when the user 
            needs to access the other items, he simply scrolls the panel.


DEFAULT:  The default Viewport size is 400x400 pixels. The Work Region must
be at least as big as the Viewport or larger.

.help viewportIcon
.reference .help viewportL

.help workregionIcon
.reference .help workregionL

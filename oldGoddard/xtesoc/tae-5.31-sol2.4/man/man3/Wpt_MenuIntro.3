.\"
.\" @(#)Wpt_MenuIntro.3	33.1 8/26/94 
.\"
.TH Wpt_MenuIntro 3 "9 Sep 1993" "TAE Plus v5.3"
.SH NAME
Wpt_MenuIntro - introduction to menubar functions

.SH INTRODUCTION
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading

TAE Plus provides a set of menubar convenience routines to assist
in the programmatic manipulation of the complex \fImenubar\fP
presentation type.
These routines are designed
to handle \fIlarge scale\fP changes to an existing menubar item.
Smaller and more precise changes should be performed with the other
TAE Plus Vm and Wpt routines. Examples of both types of changes appear
in the section entitled \fICoding Examples.\fP
For a discussion of the value passed to the application, see the section 
entitled \fIWPT_PARM_EVENT.\fP

.SH MENUBAR ROUTINES, CONSTANTS, AND STRUCTURES
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading

The following is a set of simple Wpt routines which support
the menubar presentation type. The overall idea is to provide a
simplified API interface to the manipulation of menu items in Wpt.
One of the primary goals is to provide an easy way to modify
an existing menubar given choices retrieved from a data base or some
other application-specific area.

An \fIentry\fP is one definable unit of the menubar. 
To visualize an entry, think of a menubar with a row of
pulldown menus (each is an entry). Each choice on each menu in a menubar 
is also an entry. In other words, the menubar is a hierarchical collection of
entries in which \fIeach entry name must be unique.\fP (However, \fItitles\fP
of entries need not be unique.)

\fBMenubar API Routine Summary\fP

.nf
Wpt_MenuIntro		- Introduction to Menubar API
Wpt_GetMenu		- Return One Menu Level or Entry of Menubar
Wpt_GetMenuEntryPath	- Get Fully Qualified Path to Named Entry
Wpt_InsertMenuEntries	- Insert Menubar Entries After Specified Entry
Wpt_MenuUpdate	- Update Specified Menu's View and Target
Wpt_RemoveMenuEntries	- Remove Entries from Menubar
Wpt_SetMenu		- Define One Menu Level of Menubar or Cascade
Wpt_SetMenuSensitivity	- Sets the Input Sensitivity State of Menubar Entries
.fi

Each of these routines appears as a separate man page.

To facilitate parameter passing between the application code and the Wpt
support routines, the following C language defines (constants)
and structures must be used.
They can be found in $TAEINC/wptdefs.h. Ada programmers have a complimentary
set of definitions.

.nf
/* WPT_MENU entry types */

    WPT_MENU_CASCADE_TYPE
    WPT_MENU_HELPCASCADE_TYPE
    WPT_MENU_PUSHBUTTON_TYPE
    WPT_MENU_STATICLABEL_TYPE
    WPT_MENU_MENUICON_TYPE
    WPT_MENU_SEPARATOR_TYPE
    WPT_MENU_CHECKBOX_TYPE
    WPT_MENU_TAEHELP_TYPE

.bp
/* Accelerator modifier Key masks */

    WPT_ACCEL_NONE
    WPT_ACCEL_ALT
    WPT_ACCEL_CTRL
    WPT_ACCEL_SHIFT

/* WPT_MENU separator styles */

    WPT_SEP_SINGLE_LINE
    WPT_SEP_DOUBLE_LINE
    WPT_SEP_SINGLE_DASHED_LINE
    WPT_SEP_DOUBLE_DASHED_LINE
    WPT_SEP_SHADOW_ETCHED_IN
    WPT_SEP_SHADOW_ETCHED_OUT
    WPT_SEP_NO_LINE

/* WPT_MENU structure definitions */

typedef struct {
    TAEINT      modifier;		/* modifier mask */
    TEXT        key[STRINGSIZ+1];	/* name of accelerator key */
    } WPT_ACCEL;

typedef struct {
    TAEINT      style;			/* separator style  (WPT_SEP_xxx)*/
    TAEINT      thickness;		/* thickness if shadow separator */
    } WPT_SEP;

typedef struct {
    TEXT        type[NAMESIZ+1];	/* one of WPT_MENU_xxx_TYPE constants */

    TEXT        name[NAMESIZ+1];	/* unique entry name per menubar */
    /* NOTE: It is \fIname\fP which is passed to the application as value[0]. */

    TEXT        title[STRINGSIZ+1];	/* title of entry (displayed in menu) */
    TEXT        mnemonic[2];		/* only 1st char significant; "" = none */
    BOOL        state;			/* TRUE = active, FALSE = inactive */
					/* This is for input sensitivity (dimming) */
    TEXT        fg[STRINGSIZ+1];	/* foreground color ("" = use item's default) */
    TEXT        bg[STRINGSIZ+1];	/* background color ("" = use item's default) */
    TEXT        font[STRINGSIZ+1];	/* font ("" = use item's default) */
    WPT_ACCEL   accelerator;
    TEXT        checkboxVal[sizeof("yes")];	/* value of checkbox if any */
    TEXT        iconFileName[STRINGSIZ+1];	/* icon file name if any */
    WPT_SEP     separator;		/* separator info if any */
    CODE        status;			/* status from Wpt_Setx for this entry*/
    } WPT_MENUENTRY;
.fi

.bp
.SH CODE EXAMPLES
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading

In addition to the examples in this section,
the directory $TAEDEMOSRC/presdemo has a 
working example of menubar manipulation. 

Assume for all of the following examples that you have a menubar arranged as
follows:
.nf

---------------------------------------------------------------
File          Edit                 Arrange           Help
---------------------------------------------------------------
Open          Insert---> Before    Up                on context
Close         Delete     After     Down
Save          Modify               Forward
Quit          Duplicate            Backward

Note that the "Insert" choice under the "Edit" menu has two
two subchoices (before and after.)

Throughout the following code examples, assume: 

   1) The menubar's item name is "mymenubar".
   
   2) Each entry has the same name as its title, except the first letter
      of the entry name is lowercase.
 
   3) The panel Id, panel view and panel target are called
      \fIpanel1Id, panel1View\fP, and \fIpanel1Target\fP respectively.

.fi

.SH EXAMPLE 1

\fIQUESTION: What are the names of the top level cascade menus?\fP

.nf
{
COUNT i, ecount;
WPT_MENUENTRY *entries;

Wpt_GetMenu (panel1Target, panel1View, "mymenubar", NULL, 
            &ecount, &entries);

for (i=0; i<ecount; ++i)
    {
    printf ("entries[%d] = %s\\n", i, entries[i].name);
    }
tae_free (entries);		/* remember to free the entries */
}


OUTPUT:

entries[0] = file
entries[1] = edit
entries[2] = arrange
entries[3] = help

.fi

.SH EXAMPLE 2

\fIQUESTION: What are the names of the entries under insert?\fP

.nf
{
COUNT i, ecount;
WPT_MENUENTRY *entries;

Wpt_GetMenu (panel1Target, panel1View, "mymenubar", "insert",
            &ecount, &entries);

for (i=0; i<ecount; ++i)
    {
    printf ("entries[%d] = %s\\n", i, entries[i].name);
    }
tae_free (entries);		/* remember to free the entries */
}


OUTPUT:

entries[0] = before
entries[1] = after
.fi

.SH EXAMPLE 3

\fIQUESTION: How do I disable "open" and delete "forward" and "backward"?\fP

Disable using \fIWpt_SetMenuSensitivity\fP and delete 
using \fIWpt_RemoveMenuEntries\fP.
However, the changes do not take affect until \fBWpt_MenuUpdate\fP is called.

.nf
{
TEXT *disableNames[1];
BOOL state[1];

TEXT *removeNames[2];

disableNames[0] = "open";
state[0]        = FALSE;

Wpt_SetMenuSensitivity (panel1View, "mymenubar", 1, 
    disableNames, state);

removeNames[0]  = "forward";
removeNames[1]  = "backward";

Wpt_RemoveMenuEntries (panel1Target, panel1View, "mymenubar", 
    2, removeNames);

	/* finalize the changes */
Wpt_MenuUpdate (panel1Id, panel1Target, panel1View, "mymenubar");
}
.fi

.SH EXAMPLE 4

\fIQUESTION: How do I insert a new pulldown menu between the "Arrange"
and "Help" menus?\fP

The technique is to insert a cascade entry, then populate it with
new choices. Finally, update the menubar with the changes.

.nf
{
WPT_MENUENTRY newCascade[1];
WPT_MENUENTRY newChoices[2];

strcpy (newCascade[0].type, WPT_MENU_CASCADE_TYPE);
strcpy (newCascade[0].name, "auxiliary");  /* name passed to application */
strcpy (newCascade[0].title, "Auxiliary");
strcpy (newCascade[0].mnemonic, "A");
strcpy (newCascade[0].fg, "parent cascade"); /* inherit from the menubar */
strcpy (newCascade[0].bg, "parent cascade");
strcpy (newCascade[0].font, "parent cascade");
newCascade[0].state = TRUE;

         /* insert new auxiliary cascade */
Wpt_InsertMenuEntries (panel1Target, panel1View,
    "mymenubar", "arrange", 1, newCascade);

strcpy (newChoices[0].type, WPT_MENU_PUSHBUTTON_TYPE);
strcpy (newChoices[0].name, "choice1");
strcpy (newChoices[0].title, "Choice 1");
strcpy (newChoices[0].mnemonic, "1");
	/* set Ctrl + 1 to be the accelerator combination */
newChoices[0].accelerator.modifier = WPT_ACCEL_CTRL;
strcpy (newChoices[0].accelerator.key, "1");
strcpy (newChoices[0].fg, "red");
strcpy (newChoices[0].bg, "white");
strcpy (newChoices[0].font, "parent cascade");
newChoices[0].state = TRUE;

strcpy (newChoices[1].type, WPT_MENU_PUSHBUTTON_TYPE);
strcpy (newChoices[1].name, "choice2");
strcpy (newChoices[1].title, "Choice 2");
strcpy (newChoices[1].mnemonic, "2");
newChoices[1].accelerator.modifier = WPT_ACCEL_CTRL;
strcpy (newChoices[1].accelerator.key, "2");
strcpy (newChoices[1].fg, "white");
strcpy (newChoices[1].bg, "red");
strcpy (newChoices[1].font, "parent cascade");
newChoices[1].state = TRUE;

         /* add two choices to the new cascade */
Wpt_SetMenu (panel1Target, panel1View, "mymenubar", 
    "auxiliary", 2, newChoices);

         /* finally update the menubar with all changes */
Wpt_MenuUpdate (panel1Id, panel1Target, panel1View, "mymenubar");
}

.fi

.SH EXAMPLE 5 (using Vm_SetString and Wpt_ViewUpdate)

\fIQUESTION: How do I change just the foreground color of the "delete" entry
to "red"?\fP

\fIThis type of change requires the precision provided by the Vm 
(Variable Manipulation) library.\fP
The \fIWpt_GetMenuEntryPath\fP routine 
returns the Vm path to the specific entry.
We then append ".fg" to it to specify that the foreground attribute (qualifier)
is to be updated.

.nf
{
TEXT *colorVec[1];
TEXT deletePath[STRINGSIZ+1];

Wpt_GetMenuEntryPath(panel1View, "mymenubar", "delete", deletePath);

strcat (deletePath, ".fg");		/* just update the fg color */
colorVec[0] = "red";

Vm_SetString(panel1View, deletePath, 1, colorVec, P_UPDATE);
Wpt_ViewUpdate (panel1Id, "mymenubar", panel1View, "mymenubar");
}

.fi

This example assumes only \fIone attribute\fP is to be updated.
If several attributes of the same menubar entry
need to be updated simultaneously, you would
need to \fIstrcpy\fP the "deletePath" to another string, \fIstrcat\fP
the qualifier, and call \fIVm_SetString\fP.  Repeat these three steps
for each attribute which is to be changed.  Then call \fIWpt_ViewUpdate\fP
to update the menubar item on the screen.

.SH EXAMPLE 6 (using Vm_SetString and Wpt_ViewUpdate)

\fIQUESTION: How do I change the titles of the choices under the "insert" entry?\fP

Again, this type of change requires Vm library precision. 
The \fIWpt_GetMenuEntryPath\fP returns the Vm path to the specific entries.
The titles are the \fIvalues\fP of the entries in the view Vm object.
Here we are changing the titles "Before" and "After" to "Prefix"
and "Postfix", respectively. (Note that the entry \fInames\fP are "before"
and "after".  These names aren't changed; only the titles are changed.)

.nf
{
TEXT *titleVec[1];
TEXT path[STRINGSIZ+1];	/* assuming long enough for returned path */

Wpt_GetMenuEntryPath(panel1View, "mymenubar", "before", path);
titleVec[0] = "Prefix";
Vm_SetString(panel1View, path, 1, titleVec, P_UPDATE);

Wpt_GetMenuEntryPath(panel1View, "mymenubar", "after", path);
titleVec[0] = "Postfix";
Vm_SetString(panel1View, path, 1, titleVec, P_UPDATE);

Wpt_ViewUpdate (panel1Id, "mymenubar", panel1View, "mymenubar");
}

.fi

.bp
.SH COMMON ENTRY ATTRIBUTES
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading
.nf
All menu entry types have the following common attributes (qualifiers).

    _type       - Entry type (WPT_MENU_*_TYPE)
    fg          - Foreground color
    bg          - Background color
    font        - Font
    justification - Left, Center, Right
    state       - TRUE (active), FALSE (inactive/dimmed)
.fi

.SH SPECIAL ENTRY ATTRIBUTES
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading
.nf
In addition, some entry types have other qualifiers.

WPT_MENU_CASCADE_TYPE and WPT_MENU_HELP_CASCADE_TYPE have:

    mnemonic    - one character mnemonic


WPT_MENU_CHECKBOX_TYPE, WPT_MENU_PUSHBUTTON_TYPE, 
and WPT_MENU_TAEHELP_TYPE have:

    mnemonic    - one character mnemonic
    accelerator - accelerator key sequence
.fi

The \fIcheckbox\fP target value is stored as a STRING qualifier
of the target variable. The qualifier must have the
same name as the checkbox entry.  See the section 
entitled \fIWPT_PARM_EVENT\fP.

.nf
WPT_MENU_MENUICON_TYPE has:

    mnemonic    - one character mnemonic
    accelerator - accelerator key sequence
    filename    - filename for the icon to display


WPT_MENU_SEPARATOR_TYPE has:

    septhickness - thickness of separator
    The separator type is the value of the view qualifier.
.fi

.SH WPT_PARM_EVENT
.\" Trick to force a blank line after a heading
.IP

.LP
.\" END Trick to force a blank line after a heading

All menubar items are of data type STRING. \fIThe\fP \fBvalue\fP \fIof
a menubar item will be the\fP \fBname\fP \fIof the selected entry,
passed as value[0] to the application's event handler.\fP
If and only if the entry selected was a \fIcheckbox\fP, then its `yes'/`no'
value will also be passed in the next value, value[1].

For example, if the `toprinter' entry was selected from
the `print' cascade on menubar `mainMenubar'. The value passed would be:
.nf

    value[0] = "toprinter"	/* entry name, rather than title */

    the item's count would be 1.
.fi

If the chosen entry was a checkbox, an additional value is passed 
indicating the entry's state. 
If the `toprinter' entry above was 
a checkbox (entry type = WPT_MENU_CHECKBOX_TYPE). Then the values
passed would be:
.nf

    value[0] = "toprinter"
    value[1] = "yes"           /* the value of the checkbox */

    the item's count would be 2.
.fi
 
Of course, the value passed to the application's event handler may 
also be obtained from the panel target. For example, for a menubar
named `mainMenubar', you can retrieve its value by using the macro
\fIStringParm\fP.  (Be sure to check that count > 0 before calling this macro.)
.nf

    TEXT *value;
    value = StringParm(targetId, "mainMenubar");

.fi

Checkboxes in a menubar may also have a value of their own. 
The value of any checkbox
in the menubar can be retrieved by appending ".\fI checkboxName\fP" to the
name of the menubar item, \fIregardless of its position in 
the menu hierarchy.\fP (This is because the \fItarget\fP value of a
menubar item only has a depth of one regardless of the depth of the menu
hierarchy described by the item's \fIview\fP.)
For example, suppose that somewhere on our menubar we have a checkbox
called `cb1'. Then to extract the current value of
the checkbox, use:
.nf

    TEXT *value;
    value = StringParm(targetId, "mainMenubar.cb1");
    /* value == "yes" or "no", depending on current user input */

.fi

The advantage of this implementation is that 
the application programmer only needs to know the entry names
of the checkboxes, rather than be concerned with their
position in the menubar hierarchy.

.SH SEE ALSO

Vm, Vm_SetString, Wpt, Wpt_ViewUpdate,
Wpt_GetMenu, 
Wpt_GetMenuEntryPath,
Wpt_InsertMenuEntries,
Wpt_MenuUpdate,
Wpt_RemoveMenuEntries,
Wpt_SetMenu,
Wpt_SetMenuSensitivity,
TAE_menubar

.so man1/../COPYRIGHT
.so man1/../AUTHOR

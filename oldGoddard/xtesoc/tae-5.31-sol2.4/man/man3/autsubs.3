.\"
.\" 	@(#)autsubs.3	33.1 8/26/94
.\"
.TH autsubs 3 "30 Aug 1993"  "TAE Plus v5.3"
.SH NAME
autsubs \- taeperl subroutines for scripting an application

.SH INTRODUCTION
.IP

.LP
This man page describes \fItaeperl(1)\fP subroutines for
driving an application from a script.   The target
application process is called the "Application under
Test" (AUT).  taeperl scripts that call
these subroutines are used for testing TAE
Plus applications.

Scripts are executed by the \fItaeperl(1)\fP interpreter
and use the subroutines described below to send
script commands to the AUT.  Script commands are
"interlocked" with the application being
scripted, that is, the script sends a message to the AUT and
then waits for a response message from the AUT before
proceeding to the next command.  See \fItaeperl(1)\fP
for more information.

A script may control any application that has
called \fIWpt_ScriptInit(3)\fP.
Wpt_ScriptInit establishes
the application as a "server" for script commands.
The application listens for both X and script commands,
that is, the user interface continues to be active
while scripting is in progress.   Programmers
writing test scripts often use Aut'BeginWait (see below) 
to lock out the real user interface in order to minimize 
the probability that someone accidentally corrupts a scripted test. 
.ig xx
X .SH C-CALLABLE FUNCTIONS
X Generally, your application logic should not care whether it
X is being scripted or not.  For special cases, however, you
X may call IsScripted() to determine if the logic currently
X executing was initiated by a script command.
.xx

The source code for the subroutines resides in autsubs.pl in the
$TAEPERLLIB directory, and is made available to the script
by means of the perl "require" directive (see below).
$TAEPERLLIB is included among the directories taeperl
searches for "require" files, so that the "-I" command line
option is not needed.  
Thus, to run
a script, the command line is:

    % taeperl myScript.pl

.SH COMMON SYNOPSIS
.IP
.LP
    require 'autsubs.pl';

.SH DESCRIPTIONS and SPECIFIC SYNOPSES
.IP
.LP
In the subroutine descriptions below, the return
code named "$dodie" means that a non-zero return
value indicates that the subroutine executed correctly,
and that a zero return value indicates an error 
condition.  This type of return value supports
perl's "do or die" paradigm, for example:

.nf
    &Aut'BeginWait($skt, "myPanel") || die "BeginWait failure";
.fi

For simplicity, many script programmers ignore
the $dodie return codes because the Aut
subroutines automatically print error messages
when subroutines fail.  You may, however, suppress the
automatic error output with the &Aut'SetPrintErr
subroutine. 

.LP

\fB&Aut'BeginWait -\fP
.in +5

$dodie = &Aut'BeginWait ($skt, $panelName)

Sets the named panel to hourglass state.  In this state the
panel will not accept user input (see
\fIWpt_BeginWait(1)\fP).  This might be used in a test
script to ensure that the test is not disrupted by actual user
input.  Terminate the hourglass state by calling Aut'EndWait.
.in -5

\fB&Aut'Close -\fP
.in +5

$dodie = &Aut'Close ($socketHandle)

Closes the connection specified by \fI$socketHandle\fP.
.in -5

\fB&Aut'Connect -\fP
.in +5

$socketHandle = &Aut'Connect ($socketName)

Opens a connection to the AUT.  \fI$socketName\fP is the socket
name specified by the AUT in the Wpt_ScriptInit call.
If the Wpt_ScriptInit argument is a simple string without
explicit UNIX path names,  then the script must be running
with the same working directory as the AUT.  Many applications
use a socketName of the form "/tmp/myScriptSocket" to make
the AUT more easily accessible and to remove the "same directory"
restriction.

It is assumed that the AUT is already running at the
time of Aut'Connect.   \fI$socketHandle\fP is used in subsequent
calls to specify the AUT of interest.  In the subroutine
descriptions here, the first argument ($skt) is the socket handle. 

Aut'Connect dies and causes the script to exit to the shell
if the connection is not successful.  
.in -5

\fB&Aut'EndWait -\fP
.in +5

$dodie = &Aut'EndWait($skt, $panelName)

Resets the hourglass state for the named panel.  See
&Aut'BeginWait.
.in -5

\fB&Aut'GetWindowId -\fP
.in +5

$windowId = &Aut'GetWindowId ($skt, $panelName)

Returns the window Id of the named panel.
This subroutine allows a perl script to dump the window
(associated with a TAE Plus panel) using X utilities.
.in -5

\fB&Aut'Messages -\fP
.in +5

$dodie = &Aut'Messages ($skt, $enable)

Turns on (\fI$enable\fP is 1) or off 
(\fI$enable\fP is 0) the popup messages produced by
\fIWpt_PanelMessage(1)\fP.  When
turned off, the messages are written to the
stdout of the AUT.  The purpose here is to
allow construction of unattended tests that
capture the errors produced.
.in -5

\fB&Aut'PanelMessage -\fP
.in +5

$dodie = &Aut'PanelMessage ($skt, $panelName, $message)

Generates a 
.I Wpt_PanelMessage(3)
call within the AUT using the indicated panel
and the message string.   If messages are disabled
(see Aut'Messages), then the popup will not appear (but the message 
will be logged to stdout).
.in -5

\fB&Aut'SetAllSensitivity -\fP
.in +5

$dodie = &Aut'SetAllSensitivity ($skt, $panelName, $sensitive)

Sets (\fI$sensitive\fP is 1) or resets 
(\fI$sensitive\fP is 0) item sensitivity for all items on the 
named panel.
.in -5

\fB&Aut'SetItemSensitivity -\fP
.in +5

$dodie = &Aut'SetItemSensitivity ($skt, $panelName, $parmName, $sensitive)

Sets (\fI$sensitivity\fP is 1) or resets 
(\fI$sensitivity\fP is 0) item sensitivity.
.in -5

\fB&Aut'SetPace -\fP
.in +5

&Aut'SetPace ($delaySecs)

Establishes an implicit delay between Aut
UserEvent subroutines.  Following a SetPace call, Aut
UserEvent subroutines sleep for \fI$delaySecs\fP seconds
before proceeding.  This avoids having
to place explicit sleep calls following each
script command.    Delays are typically necessary
so that a human may observe the progress of a test.
(Without delays, popup panels might be created,
scripted, and erased without ever being visible.)
.in -5

\fB&Aut'SetPrintErr -\fP
.in +5

&Aut'SetPrintErr ($enable)

Turns on (\fI$enable\fP is 1) or off
(\fI$enable\fP is 0) automatic error printing by
the Aut subroutines.  By default, printing is
enabled.  When error printing is disabled, the
calling script must examine all return codes and
perform its own error handling.
.in -5

\fB&Aut'SetSingle-\fP
.in +5

&Aut'SetSingle ($enable)

Turns on (\fI$enable\fP is 1) or off
(\fI$enable\fP is 0) single stepping of simulated
user events.  By default, single stepping is
disabled.   Single stepping pauses the taeperl
script and prompts for RETURN before sending
a user event to the AUT; the panel, item, and value
are identified in the prompt.
.in -5

\fB&Aut'Print -\fP
.in +5

$dodie = &Aut'Print ($skt, $string)

Sends the string to be printed on the stdout of the
AUT.  This subroutine allows a script to annotate test output.
.in -5

\fB&Aut'SetRecord -\fP
.in +5

$dodie = &Aut'SetRecord ($skt, $recordFile)

Enables and disables record mode in the AUT.   If the \fI$recordFile\fP
argument is present and not an empty string, then record mode 
is enabled to the file.  If record mode was already enabled,
then the old recordFile is closed and the AUT records
subsequent events to the new recordFile.  If the two files
are identical then the previous recording will be lost.

If \fI$recordFile\fP is absent or is the empty string, the
recordFile is closed and recording is disabled.

This subroutine is used by \fItaerecord(1)\fP.
.in -5

\fB&Aut'SetTrace -\fP
.in +5

&Aut'SetTrace ($enable)

Enables (\fI$enable\fP is 1) and disables 
(\fI$enable\fP is 0) tracing.
When tracing is enabled, Aut subroutines
log the panel name, item name, and
value being scripted to taeperl's stdout.
.in -5
.ig xx
X .SS "$proceed = &Aut'Alarm ($prty, $type, $message);"
X Directs the process to display a popup message using
X the Alm_AlarmText function.  This blocks until the user
X hits one of the buttons on the popup.  $prty is
X &ALM_NOTIFICATION, &ALM_INFO, or &ALM_CAUTION.
X $type is &ALM_NOTED or &ALM_ABORT_CONTINUE, selecting
X the buttons to be displayed in the panel.
X The return value is True if the user hit NOTED or 
X CONTINUE and False otherwise.  See alarms(n) for details.    
.xx
.ig xx
X .SS "&Aut'Begin;"
X Announces the pid of the taeperl script to the process
X identified by $View/$Process.   This allows the application 
X to have a "kill script" button.    Application processes
X can call "KillScript()" to kill the currently registered script.
.xx
.ig xx
X .SS "&Aut'BeginWaitAll($skt);"
X Sets all visible panels to hourglass state with Wpt_BeginWait.
.xx
.ig xx
X .SS "&Aut'End;"
X Announces the termination of the taeperl script to the
X process identified by $View/$Process.   This allows the
X application to adjust the user context (e.g., changing the
X kill button to run).   
.xx
.ig xx
X .SS "&Aut'EndWaitAll($skt);"
X Resets the hourglass state for all visible panels. 
.xx

\fB&Aut'UserEvent -\fP
.in +5

$dodie = &Aut'UserEvent ($skt, $panelName, $parmName [,$value,...])

Sends a simulated user event to the AUT identified by
the socket handle \fI$skt\fP. The event simulates user interaction
with the item named \fI$parmName\fP on the panel named 
\fI$panelName\fP.

The optional 
.I $value 
argument is for those items
that have associated values, for example, a text keyin
field.  
.I $value 
may be a scalar or a list of
component values when the item is a vector.  

The type of the item may be integer or string and
is inferred from the type
of the value.  Thus, if you pass a string $s with value "1" 
and desire it to be an integer, you must pass it as 0 + $s.
Use UserRealEvent, UserStringEvent, or UserIntgEvent 
to force the correct type.

Examples:

.nf
    #
    #    press the ok button on the main panel
    #
    &Aut'UserEvent ($skt, "main", "ok");  
    #
    #    enter "hello there" into the item named "greeting" 
    #
    &Aut'UserEvent ($skt, "main", "greeting", "hello there");
.fi

You may simulate an event for any item on any existing panel. 
Simulated events are subject to the constraints of
a real end user, that is, Aut'UserEvent generates an error message
if you attempt to simulate an event for (a) an
insensitive item, (b) an item on a panel whose input is disabled
by Wpt_BeginWait, or (c) an item on a panel whose input is
disabled by some other modal panel.   For various technical reasons,
Aut'UserEvent cannot properly test whether the panel being
scripted is visible, so you are allowed to simulate an event
for an invisible panel.

Aut'UserEvent does not support simulating the use of a help
button to obtain TAE Plus context-sensitive help.  If you 
use Aut'UserEvent to press a help button, the application event
handler is called, but the special Wpt help logic is not invoked.

Aut'UserEvent may be used to script an event which terminates the
AUT, for example, from a script, you can operate a "quit" button which
exits the process.   Such scripting works much better if the 
application returns from the event handler with a flag to direct 
that the main event loop call Wpt_Finish and then exit.   In
code generated by the WorkBench (using the "multiple file" style),
there is the SET_APPLICATION_DONE macro that sets such a flag. 
The other types of generated code provide similar facilities.
See "Scripting a Quit Event" below.

does a formal "SET_APPLICATION_DONE" rather than 
a direct call to the "exit" function; SET_APPLICATION_DONE, 
defined in the generated global.h module, causes the main 
loop to terminate and then call Wpt_Finish.
.in -5

\fB&Aut'UserIntgEvent -\fP
.in +5

$dodie = &Aut'UserIntgEvent ($skt, $panelName, $parmName, value, ...)

Same as UserEvent but forces the type of the item to integer.
See UserEvent.
.in -5

\fB&Aut'UserRealEvent -\fP
.in +5

$dodie = &Aut'UserRealEvent  ($skt, $panelName, $parmName, value,...)

Same as UserEvent but forces the type of the item to real.
See UserEvent.
.in -5

\fB&Aut'UserStringEvent -\fP
.in +5

$dodie = &Aut'UserStringEvent ($skt, $panelName, $parmName, value, ...)

Same as UserEvent but forces the type of the item to string.
See UserEvent.
.in -5
.ig xx
X Using &Aut'SetValues and SetScriptedValuesCallback, an
X application author can combine several simple functions
X into one callback by casing on one of the pooled values.
X The symbol "cmd" is reserved as are all symbols that start
X with underscore.
.xx
.ig xx
X .SS "&Aut'SimpleAlarm ($message);"
X Equivalent to "&Aut'Alarm (&ALM_INFO, &ALM_NOTED, $message);".
X This is painless way to display a message.  This blocks
X until the user hits NOTED. 
.xx

\fB&Aut'WalkChoices -\fP
.in +5
$dodie = &Aut'WalkChoices ($skt, $panelName, $choiceListName)

This walks through the choices (i.e., the constraints) for
a TAE Plus selection item (for example, radio button
bank, pulldown menu, option menu), setting each
choice to the value of the item.  This is
an easy way to exercise menu options without
hard-coding the list of choices.   For
some cases, you might want to pull apart the
(simple) logic of this subroutine and incorporate
the technique into your script.  An example is nested
walks, where you walk through one item nested
within the walkthrough of another.
.in -5

\fB&Aut'WidgetAction -\fP
.in +5

$dodie = &Aut'WidgetAction ($skt, $panelName, $widgetName," $action, $arg, [$name, $value, ...])

Allows you to script directly to widgets,
bypassing TAE Plus.  The 
.I $widgetName 
argument is
in XtNameToWidget format, that is, "*" and "."
characters may be used as in "*list.button".  If
the widget name is an empty string, then the
shell widget associated with the panel is used.
The widget must be somewhere in the widget tree
starting at the shell associated with the TAE
Plus panel.   
.I $action 
is the name of an
XtActionProc associated with the widget.  
.I $arg 
is
an optional string argument to pass to the
action.

You may control the XEvent passed to the action function by
adding name/value pairs following the 
.I $arg 
argument.
By default, the XEvent is XButtonEvent with most fields set to zero.
You can set the fields "type", "time", "x", "y", "x_root", "y_root",
"state", "button", and "same_screen".   You can use an associative
array as a convenient way to construct name/value pairs\(emsee
WidgetSetValues for an example.  Aut'WidgetAction example:

.nf 
    &Aut'WidgetAction ($skt, "proto",  "*myButton",  "Activate", "", 
        "x", 50, "y", 50);
.fi

WidgetAction has considerable power.  To help you pick widget
names, you may dump the widget trees of a process with:  

.nf
    % taeperl $TAEPERLLIB/widgetTree.pl socketName 
.fi 

Several TAE Plus items are constructed from gadgets, which 
do not have action functions. 

For TAE Plus panels, the name of the shell widget
is panelName_shell.  The next widget down is the
TAE Plus panel widget, with class name "Box" and
widget name equal to the panel name.  In the call
to XtNameToWidget, the script handler passes the
shell widget as the widget argument.  Example:
the push button named "ok" on the panel "test"
has the full widget name of
"test_shell.test.ok".  Typically, the "ok" button
is scripted with:

.nf
    &Aut'WidgetAction ($skt, "test", "*ok", ...);
.fi 

Note that "test" here is the TAE Plus panel name (whose widget
name is "test_shell").  If you want to avoid uncertainty with 
the asterisk in "*ok", you may use:
 
.nf
    &Aut'WidgetAction ($skt, "test", "test.ok", ...);
.fi 

The "test" in "test.ok" here refers to TAE's Box widget (i.e., the
panel widget).
.in -5

\fB&Aut'WidgetSetValues -\fP
.in +5

$dodie = &Aut'WidgetSetValues ($skt, $panelName, $widgetName, $rName, $rValue,...)

Sets resource values for a widget with
XtSetValues.  See WidgetAction for notes on
identifying the widget.  
.I $rName 
and
.I $rValue
represent any number of name/value pairs of
resources.  Resource names should omit the "XmN"
prefix.  Values may be string or integer.  This
capability was inspired by the \fIeditres\fP utility in
X11R5.  This subroutine has considerable power;
for example, while a script is running, you could
change the background color of the panel, 
change a label string, or move the shell window. 

A similar subroutine named Aut'WidgetGetValues is
much desired; however, as the editres
documentation explains, many "resource-to-string"
resource converters would be needed in the
application process.

The name/value pairs may come from an associative array
as follows:

.nf
    $res{"x"} = 100;
    $res{"y"} = 200;
    $res{"fg"} = "white";
    &Aut'WidgetSetValues ($skt, "MainPanel", "*Cancel", %res);
.fi
.in -5
.ig xx
X &Aut'StringEvent has special handling for items that
X are validated strings (e.g., pulldown, radio button, etc.):
X if the scripted value is invalid, ScriptStringEvent produces
X an alarm popup\-the user has the option to terminate the
X script with the "Cancel" button.  The error message contains 
X the bad value and the title of the item.   The displayed title is
X the title string associated with the item; if the item has
X no title,  the title is obtained by looking for a label
X in the same panel with the original item name suffixed 
X with "L" or "_T".
X 
X The script is blocked while the popup is active.
X 
X If the user selects "Cancel" to terminate the script, ScriptStringEvent
X calls the subroutine named by the $ExitHandler variable; if
X no such variable is defined, ScriptStringEvent exits directly.
.xx

.SH LIMITATIONS
.IP
.LP
When autsubs.pl is used to generate simulated
user events, the application's event handler
should not examine the XEvent substructure of the
wptEvent structure.   Simulated user events
short-circuit the X server and there is no
associated X event data.  Generally, this is not
a problem because TAE Plus applications seldom
use the XEvent information.   For applications
that do examine the XEvent information, you
should conditionalize the logic on the return
value of Wpt_IsScripted(3).


.SH UNSUPPORTED SUBROUTINES
.IP
.LP
The experimental subroutines described in this 
section are subject to change (in name, calling sequence,
or behavior) in future deliveries.


\fB&Aut'DumpVm -\fP
.in +5

&Aut'DumpVm ($vmId)
Dumps the Vm object to stdout.  Useful for debugging a script.
.in -5

\fB&Aut'FunctionCall -\fP
.in +5

$receivedVmId = &Aut'FunctionCall ($skt, $functionName [,$vmId])

Returns the Vm object from calling the registered script
function identified by \fI$functionName\fP.  The function must 
have been
registered in the AUT by calling 
\fIWpt_RegisterForScript(3)\fP.
If $vmId is specified, the 
corresponding Vm object is transmitted to the registered function.

The caller must free the returned Vm object with &Vm_Free;
.in -5

\fB&Aut'GetTargetVm -\fP
.in +5

$vmId = &Aut'GetTargetVm ($skt, $panelName)

Returns the target Vm object associated with the named panel.
The caller should eventually free the Vm object with &Vm_Free.
.in -5

\fB&Aut'GetViewVm -\fP
.in +5

$vmId = &Aut'GetViewVm ($skt, $panelName)

Returns the view Vm object associated with the named panel.
The caller should eventually free the Vm object with &Vm_Free.
.in -5


\fB&Aut'SetValues -\fP
.in +5

$dodie = &Aut'SetValues ($skt, $name, $value, $name, $value, ...)

This subroutine allows the script to set
general-purpose flags in the application
process.  SetValues sets values into a Vm
object of "current script values" contained in the AUT.
The AUT can call 
.I Wpt_GetScriptedVm(3)
to query the "value pool".

The SetValues arguments consist of name/value pairs, for example,
the following sets "mode" to 24 and "scriptName" to "bugFreeScript":

.nf
    &Aut'SetValues ($skt, "mode", 24, "scriptName", "bugFreeScript");
.fi

The names and types of the symbols are agreed upon by the
script author and the application author.  SetValues does not 
currently support Real values.    Name/value pairs can be
conveniently constructed from a perl associative array.
See
Aut'WidgetSetValues for an example.
.in -5


\fB&Aut'Wait -\fP
.in +5

$responseVm = &Aut'Wait ($skt, $panelName, $parmName1, $parmName2, ...)

Delays until the user operates one of the named
items on the panel named \fI$panelName\fP.  If no
parmName's are supplied, then the script delays
until any item on the named panel is operated.
The returned Vm is the target Vm of the panel and
contains two additional symbols, "_panelName" and
"_parmName", to tell you what condition satisfied
the wait.

The parmName argument may be a list, allowing
you to wait on a specific list of items.
Example:

.nf
    &Aut'Wait ("mainPanel", "ok", "cancel");  
.fi

Note: The caller must free the returned Vm object with &Vm_Free;
.in -5
.ig xx
X .SS "$responseVm = &Aut'WaitWithVectors ($skt, *pan, *parm, $setSens);"
X Similar to ScriptWait, but you can supply two vectors by reference,
X the first with a list of panel names and the second with
X a list of parm names.   Either vector can be empty in which case
X the "wild card" rules above apply.   Example:
X 
X .nf
X     @panels = ("mainPanel", "auxPanel");
X     @parms = ("ok", "cancel");
X     &Aut'WaitWithVectors (*panels, *parms, 0);  
X .fi
X 
X The $setSensitive argument is TRUE to indicate that the
X items being waited on should be set sensitive.  This allows
X you to use "ScriptSetAllSensitivity" to set all items on
X a panel insensitive, then use ScriptWaitWithVectors to enable
X (specifically) the items you want to wait for. 
X 
.xx


\fB&Vm_Free -\fP
.in +5

$code = &Vm_Free ($vmId)

Frees a Vm object.  
The returned code is always true.  
Calls to Vm_Free typically ignore the return code.



.SH NOTES ON WRITING SCRIPTS
.IP
.LP
.SS "Interrupt Handlers"
.IP

.LP
If you call &Aut'BeginWait, then you should have
a SIGINT handler to clean things up before
exiting, otherwise you may leave the panel in an
unusable state.   Example:

.nf
    $skt = &Aut'Open("theApplication");
    $SIG{"INT"} = "interrupt";
    ...
    &Aut'BeginWait ($skt, "mainPanel");
    ...
    &Aut'EndWait ($skt, "mainPanel");
    &Aut'Close($skt);
    exit(0);


    sub interrupt 
        {
        &Aut'EndWait($skt, "mainPanel");
        &Aut'Close($skt);
        exit(0);
        }
.fi

Use of &Aut'SetItemSensitivity requires similar attention. 

.SS "Scripting a Quit Event"
.IP

.LP
If you script some TAE Plus user event whose event handler directly 
exits, the script will hang.    This is because 
the script does not proceed until the TAE Plus event
handler returns (in order to keep the script synchronized
with the event handling).  If the event handler never
returns, then the script hangs.   The best approach
to this problem is to avoid calling exit from a TAE Plus
event handler and use the "application done" flag (provided
in code generated by the WorkBench) instead.

.SH EXAMPLES
.IP
.LP
\fItaeperl(1)\fP has examples.
Another source of examples is to use 
\fItaerecord(1)\fP and \fIrec2pl(1)\fP to generate a script for your own 
application.  

.SH SEE ALSO
.IP
.LP
\fItaeperl(1)\fP, \fItaerecord(1)\fP,
\fIWpt_ScriptInit(3)\fP, \fIWpt_IsScripted(3)\fP, \fIWpt_SetRecord(3)\fP,
\fIWpt_ScriptUnsup(3)\fP

.I Programming perl
by Larry Wall and Randal Schwartz (O'Reilly & Associates, Sebastopol,
CA) is the standard reference on the perl language.

A typical Aut subroutine is a small number (about
10 lines) of readable code, so one of the best
sources of information is the autsubs.pl file
itself.


.so man1/../COPYRIGHT
.so man1/../AUTHOR

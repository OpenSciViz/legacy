'\" t
.\ Above is to invoke "tbl". Disable hyphenation below.
.nh
.TH StringParm 3 "29 Oct 1992" "TAE Plus v5.3"
.SH NAME
StringParm, IntParm, RealParm, SVAL, IVAL, RVAL - macros to retrieve values of TAE Variables

.SH SYNOPSIS
.IP
.LP
StringParm(vmId, name)
.br
IntParm(vmId, name)
.br
RealParm(vmId, name)

SVAL (*v, i)
.br
IVAL (*v, i)
.br
RVAL (*v, i)
.br

C Parameters are described in the *Parm DESCRIPTION and *VAL DESCRIPTION
sections.
Ada programmers: See ADA PROGRAMMER NOTES section.
C++ programmers: See C++ PROGRAMMER NOTES section.

.SH *Parm DESCRIPTION
.IP
.LP
Arguments:

.nr L 0 1
.IP \n+L.
vmid: input, Id   vmid;

Id of a Vm Object; may be target or view Vm Object returned by Co_Find.
.IP \n+L.
name: input, TEXT   name[ ];

Name of the TAE Variable.  The name may be qualified to any number of levels,
e.g., "myKeyin" (to obtain the target or title), "myButton.fg", "myMenubar.edit.copy.font", etc.
.LP

The *Parm macros (which are \fIonly\fP defined in generated C code)
are used to access either target or view information.
They reinforce the object-oriented access to TAE Variables in that
the programmer passes the Vm Object (vmid) and the name of the item
(possibly with qualifiers to access specific attributes).

Use the *Parm macros (instead of the *VAL macros) when:
.IP
\(bu you have the target or view Vm Object (vmid); and
.sp
\(bu you are accessing a single-dimensioned attribute (target of count 1,
title, fg, bg, font, etc., but not for origin or size)
.LP

The application can call the appropriate *Parm macro:
StringParm, IntParm, or RealParm, as per the target datatype. 
These macros extract the values of a TAE variable in the Vm Object 
(either target or view vmIds). The macros are defined as:

.nf
    #define StringParm(vmId, name)    (SVAL(*Vm_Find(vmId, name), 0))  
    #define IntParm(vmId, name)       (IVAL(*Vm_Find(vmId, name), 0))  
    #define RealParm(vmId, name)      (RVAL(*Vm_Find(vmId, name), 0))  
.fi

.SH TARGET ACCESS
.IP
.LP
The generic form for using each of these macros to extract
an item's current target value follows. 
(Use the form which matches the item's data type.)

.nf
    TEXT *currentString;
    currentString = StringParm (panelnameTarget, "itemname");

    TAEINT currentInt;
    currentInt = IntParm (panelnameTarget, "itemname");

    TAEFLOAT currentReal;
    currentReal = RealParm (panelnameTarget, "itemname");
.fi


Note that use of StringParm, IntParm, and RealParm all assume that 
the target is single-dimensioned (since they extract the 0th element
in the vector). 

For items which have multi-dimensioned values 
(count greater than one) such as the Multi-Line Edit presentation, 
the programmer should instead use 
the corresponding SVAL, IVAL, or RVAL macros 
(on which the *Parm macros are based). 
This may require use of Vm_GetAttribute to first determine the 
current count (dimension) of the value vector.
See the CODE EXAMPLES section.

.SH TITLE ACCESS
.IP
.LP
The title of a TAE Plus item is the \fIvalue of the view\fP TAE Variable.
Therefore, to access an item's title, we use the view Vm Object (vmid):

.nf
    TEXT *title;
    title = StringParm (panelnameView, "itemname");
.fi

.SH VIEW ACCESS
.IP
.LP
To access a view attribute other than the item's title, 
the generic form is similar to that of target access,
except the view vmid is used and a qualifier is added.
(Use the form which matches the \fIqualifier's\fP data type.)

.nf
    TEXT *stringAttribute;
    stringAttribute = StringParm (panelnameView, "itemname.qualifier");

    TAEINT intAttribute;
    intAttribute = IntParm (panelnameView, "itemname.qualifier");

    TAEFLOAT realAttribute;
    realAttribute = RealParm (panelnameView, "itemname.qualifier");
.fi


.SH *VAL DESCRIPTION
.IP
.LP
Arguments:

.nr L 0 1
.IP \n+L.
v: input, struct VARIABLE *v;

Pointer to a VARIABLE structure, typically in the Vm Object.

.IP \n+L.
i: input, COUNT   i;

Zero relative index of the multi-dimensional vector.
.LP

The *VAL macros are also used to access either target or view information.

Use the *VAL macros (instead of the *Parm macros) when:
.IP
\(bu you have the pointer to the "struct VARIABLE" (obtained 
by Vm_Find); or
.sp
\(bu you are accessing a multi-dimensioned attribute (target count greater
than one, or view attributes such as origin or size)
.LP

.nf
SVAL (*v, i): access the i-th string in the string variable accessed through v.

IVAL (*v, i): access the i-th integer in the integer variable accessed through v.

RVAL (*v, i): access the i-th real in the real variable accessed through v.
.fi

.SH TARGET ACCESS CODE EXAMPLES 
.IP 
.LP 
Assume we have a panel named "simple" which contains a string Keyin 
named "entry".  We could extract the current target value of the Keyin
from any point in the application using the StringParm macro as follows:

.nf
    Id simpleTarget;    /* panel target Vm Object */
     ....
    TEXT *currentValue;
    currentValue = StringParm (simpleTarget, "entry" );
    printf ("value of item is: %s\\n", currentValue);
.fi


Now assume the item "entry" is a Multi-Line Edit presentation with
a count greater than one. Use Vm_GetAttribute to obtain the count
of the item. Use SVAL to index individual strings.

.\" indirect access to Vm_GetAttribute/Wpt_CloseItems/SVAL example.
.\"	.so man5/wptcloseitems.5
.\" DO NOT DELETE THIS FILE.
.\" IT IS INCLUDED INTO SEVERAL MAN PAGES.

Both the Multi-Line Edit and the \fImultiple\fP Selection List
presentation types have to be "closed" before the text entered or
selected by the user is returned to the application.  
Either presentation type will be closed when the user presses the <Escape> key, 
or when the application calls \fIWpt_CloseItems\fP.
If the user terminates the input, the text is retrieved from the value
vector passed to the event handler.
If the application closes the item, the text is retrieved using the SVAL macro.

The following example closes all the items on the panel, 
uses Vm_GetAttribute to obtain the current number of values of the item,
and prints each of the values.
Assume that the panel name is "simple" and the item name is "entry".

.nf
    Id simpleTarget;               /* panel target Vm Object */
     ....
    struct VARIABLE *v;            /* returned by Vm_Find */
    TAEINT i;                      /* loop control */
    COUNT newcount;                /* # of strings currently in item */
    CODE dummy_type, dummy_access; /* irrelevant return values */
    BOOL dummy_default;            /* irrelevant return values */

    Wpt_CloseItems (simpleId);     /* terminate input prior to <Escape> */
    Vm_GetAttribute (simpleTarget, "entry", &dummy_type,
                    &newcount, &dummy_default, &dummy_access);
    /* access the target TAE Variable */
    v = Vm_Find (simpleTarget, "entry");
    /* retrieve the string vector */
    for ( i = 0; i < newcount; i++ )
        printf ("line[%d] = %s\\n", i, SVAL (*v, i) );
.fi


.SH VIEW ACCESS CODE EXAMPLES
.IP
.LP
Assume we have a panel named "simple" which contains a Keyin named
"entry".  View attributes are accessed using the view Vm Object (vmid).

To retrieve the item's font (a single-dimensioned attribute), do:
.nf
    Id simpleView;    /* panel view Vm Object */
     ....
    TEXT *font;
    font = StringParm (simpleView, "entry.font");
    printf ("font of item is: %d %d\\n", font);
.fi


We could determine the location (origin) of the item on the panel
for the view Vm Object (view vmid) using either IntParm or IVAL.
The origin qualifier is a multi-dimensioned attribute of the item.
     
.nf
    Id simpleView;    /* panel view Vm Object */
     ....
    TAEINT  *origin; 
    origin = &IntParm (simpleView, "entry.origin");
    printf ("location of item is: %d %d\\n", origin[0], origin[1]); 
.fi

The most efficient use of IVAL would be:
.nf

    Id simpleView;    /* panel view Vm Object */
     ....
    TAEINT  x, y;
    struct VARIABLE *v ;
    v = Vm_Find (simpleView,"entry.origin");
    x = IVAL (*v, 0);
    y = IVAL (*v, 1);
    printf ("location of item is: %d %d\\n", x, y);
.fi

.ig INEFFICIENT
or, if you prefer:
.nf

    Id simpleView;    /* panel view Vm Object */
     ....
    TAEINT  x, y;
    x = IVAL (*Vm_Find (simpleView,"entry.origin"), 0) );
    y = IVAL (*Vm_Find (simpleView,"entry.origin"), 1) );
    printf ("location of item is: %d %d\\n", x, y);
.fi
.INEFFICIENT

.SH ADA PROGRAMMER NOTES
.IP
.LP
Instead of the above six C macros, use the tae_vm.Vm_Extract_* procedures:

.nf
Vm_Extract_SVAL 
               (vptr : address; index : in taecint;  sval : address)

Vm_Extract_IVAL 
               (vptr : address; index : in taecint;  ival : address)

Vm_Extract_RVAL 
               (vptr : address; index : in taecint;  rval : address)

.fi

Instead of Vm_GetAttribute, use tae_vm.Vm_Extract_COUNT:

.nf
Vm_Extract_COUNT 
               (vptr : address; count : address)
.fi

Use tae_vm.Vm_Find to gain access to either target or view variables.
The vmid argument will be panelname_info.target or panelname_info.view.

.nf
Vm_Find
               (vmid : in vmobject_ptr; name : in string; 
                vout : out variable_ptr)

.SH ADA CODE EXAMPLE
.IP
.LP

The following event handler for an item named "button" extracts
a multiple selection list's current choices.

.nf
procedure adasel_button (info : in tae_wpt.event_context_ptr) is
    value : array (1..1) of string (1..tae_taeconf.STRINGSIZE);
    count : taeint;

    var_selection : variable_ptr;   -- ptr to selection list target variable
    current_count : taeint ;        -- current number of selections
                                -- current selection list values
    selections : array (1..1) of string (1..tae_taeconf.STRINGSIZE);

        begin
-- deleted default generated code for button

-- Since selection list needs <Escape>, 
-- signal termination of input for all items on panel.
        tae_wpt.Wpt_CloseItems (adasel_info.panel_id);

-- Find the target variable associated with item named "selection"
        tae_vm.Vm_Find (adasel_info.target, "selection", var_selection);

-- Determine current number of selections
        tae_vm.Vm_Extract_Count (var_selection, current_count);
        text_io.put ("button checking selection list's current_count = ");
        text_io.put (taeint'image(current_count));
        text_io.new_line;

-- Print current selections
        for J in 1 .. current_count loop
                -- extract next selection from target
                tae_vm.Vm_Extract_SVAL (var_selection, J, selections(1));
                text_io.put ("selection #");
                text_io.put (taeint'image(J));
                text_io.put (" is: ");
                text_io.put_line (selections(1));
        end loop ;
    end adasel_button;
.fi


.SH C++ PROGRAMMER NOTES
.IP
.LP
The C++ bindings define the TaePanel and TaeItem base classes.
Access to class data is through class member functions as illustrated below.

Assume you have the pointer to the TaePanel, call it "MyPanel":

.nf
    TEXT *itemVal =    MyPanel->String(itemName);
.fi
or:
.nf
    TEXT *itemVal =    (*MyPanel)[itemName].String();

for the Intg and Real versions, replace "TEXT *" with the appropriate
data type and "String" with Intg or Real.


Similarly, if you have the pointer to the TaeItem, call it "MyItem":

.nf
    TEXT *itemVal = MyItem->String();  // or Intg or Real
.fi

For vectors:

.nf
    TEXT **itemVals = MyPanel->StringVec(itemName);
.fi
or:
.nf
    TEXT **itemVals = (*MyPanel)[itemName].StringVec();
.fi

The Intg and Real versions are similar.

To access the i'th value, the SVAL, IVAL and RVAL functionality
is achieved by:

.nf
    TEXT *itemVal = MyPanel->StringVec(itemName)[i];
.fi
or:
.nf
    TEXT *itemVal = (*MyPanel)[itemName].StringVec()[i];
.fi

Similarly, if you have the pointer to the TaeItem, call it "MyItem":

.nf
    TEXT *itemVal = MyItem->StringVec()[i];  // or IntgVec or RealVec
.fi

To extract the value of a view qualifier of an item, simply add the qualifier
name as a second parameter to the example calls above. For example, given
the pointer to the TaePanel "MyPanel", the foreground color of an item on
that panel can be extracted with:

.nf
    TEXT *itemFgVal = MyPanel->String(itemName, "fg");
.fi


.SH SEE ALSO
.IP
.LP
Vm_Find, Vm_GetAttribute, Wpt_CloseItems

.so man1/../COPYRIGHT
.so man1/../AUTHOR

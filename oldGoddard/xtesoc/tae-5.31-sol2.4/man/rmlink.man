#!/bin/sh
#
#     ULTRIX SPECIFIC 
#
# Remove links to v5.2 $TAEMAN pages. 
#
# Change Log
# 28-Nov-94	PR2917: Initial version...kbs

MANDIR_LOCAL=/usr/man/manl

if test $TAEPLAT && test "$TAEPLAT" != mipsel	# LATER, MIPSEL
then
    echo "This script is only needed for Ultrix on DECstation."
    echo "Your platform is: $TAEPLAT"
    exit 1
fi

if [ ! -d $MANDIR_LOCAL ] ; then
    echo "Can't find local man dir: $MANDIR_LOCAL."
    echo "If you installed v5.2 TAE man pages someplace else, modify the string"    echo "\"MANDIR_LOCAL=/usr/man/manl\" in $0 accordingly."
    exit
fi

# In v5.2, man pages were installed (linked) in the single LOCAL 'manl' dir.

cd $MANDIR_LOCAL
for FILENAME in "\
TAE_tools.1 \
TAE_demos.1 \
taecg.1 \
Co.3 \
Co_Add.3 \
Co_Find.3 \
Co_ForEach.3 \
Co_Free.3 \
Co_New.3 \
Co_ReadFile.3 \
Co_Remove.3 \
Co_WriteFile.3 \
StringParm.3 \
Vm.3 \
Vm_Call.3 \
Vm_Copy.3 \
Vm_DynTutor.3 \
Vm_Find.3 \
Vm_FindVar.3 \
Vm_ForEach.3 \
Vm_FormatVar.3 \
Vm_Free.3 \
Vm_GetAttribute.3 \
Vm_GetHostError.3 \
Vm_GetValidIntg.3 \
Vm_GetValidReal.3 \
Vm_GetValidString.3 \
Vm_InitFormat.3 \
Vm_New.3 \
Vm_OpenStdout.3 \
Vm_ReadFromDisk.3 \
Vm_ReadFromTM.3 \
Vm_SetIntg.3 \
Vm_SetMax.3 \
Vm_SetMin.3 \
Vm_SetNextMenu.3 \
Vm_SetParmPage.3 \
Vm_SetReal.3 \
Vm_SetString.3 \
Vm_SetStringLength.3 \
Vm_SetTCLVar.3 \
Vm_SetValidIntg.3 \
Vm_SetValidReal.3 \
Vm_SetValidString.3 \
Vm_WriteToDisk.3 \
Wpt.3 \
Wpt_AddEvent.3 \
Wpt_AddTimer.3 \
Wpt_BeginWait.3 \
Wpt_CloseDisplay.3 \
Wpt_CloseItems.3 \
Wpt_EndWait.3 \
Wpt_Finish.3 \
Wpt_GetConstraintSensitivity.3 \
Wpt_GetIntgConstraints.3 \
Wpt_GetItemSensitivity.3 \
Wpt_GetMenu.3 \
Wpt_GetMenuEntryPath.3 \
Wpt_GetPanelState.3 \
Wpt_GetRealConstraints.3 \
Wpt_GetStringConstraints.3 \
Wpt_HideItem.3 \
Wpt_Init.3 \
Wpt_InsertMenuEntries.3 \
Wpt_ItemWindow.3 \
Wpt_KeyinCheckForNULLInput.3 \
Wpt_MenuIntro.3 \
Wpt_MenuUpdate.3 \
Wpt_MissingVal.3 \
Wpt_NewPanel.3 \
Wpt_NextEvent.3 \
Wpt_OpenDisplay.3 \
Wpt_PanelDisplayId.3 \
Wpt_PanelErase.3 \
Wpt_PanelMessage.3 \
Wpt_PanelReset.3 \
Wpt_PanelTopWindow.3 \
Wpt_PanelWidgetId.3 \
Wpt_PanelWindow.3 \
Wpt_ParmReject.3 \
Wpt_ParmUpdate.3 \
Wpt_Pending.3 \
Wpt_Rehearse.3 \
Wpt_RemoveEvent.3 \
Wpt_RemoveMenuEntries.3 \
Wpt_SetConstraintSensitivity.3 \
Wpt_SetHelpStyle.3 \
Wpt_SetIntg.3 \
Wpt_SetIntgConstraints.3 \
Wpt_SetItemSensitivity.3 \
Wpt_SetMenu.3 \
Wpt_SetMenuSensitivity.3 \
Wpt_SetNoValue.3 \
Wpt_SetPanelState.3 \
Wpt_SetReal.3 \
Wpt_SetRealConstraints.3 \
Wpt_SetString.3 \
Wpt_SetStringConstraints.3 \
Wpt_SetTimeOut.3 \
Wpt_ShowItem.3 \
Wpt_ViewUpdate.3 \
s_equal.3 \
TAE_intro.5 \
TAE_checkbox.5 \
TAE_color_logger.5 \
TAE_ddo_intro.5 \
TAE_discrete_pictures.5 \
TAE_dynamic_text.5 \
TAE_file_selection.5 \
TAE_icon.5 \
TAE_keyin.5 \
TAE_label.5 \
TAE_menubar.5 \
TAE_message.5 \
TAE_mover.5 \
TAE_multi_line_edit.5 \
TAE_option_menu.5 \
TAE_panel.5 \
TAE_pulldown_menu.5 \
TAE_push_button.5 \
TAE_radio_buttons.5 \
TAE_rotator.5 \
TAE_scale.5 \
TAE_selection_list.5 \
TAE_stretcher.5 \
TAE_stripchart.5 \
TAE_text_display.5 \
TAE_X_workspace.5 \
"
do
    echo "rm $FILENAME"
    rm $FILENAME
done


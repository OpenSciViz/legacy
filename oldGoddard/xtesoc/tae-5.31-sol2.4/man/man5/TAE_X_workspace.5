'\" t
.\" Above is to invoke "tbl". MUST BE FIRST LINE OF FILE.
.\" Disable hyphenation below.
.nh
.\"
.TH TAE_X_workspace 5 "20 Sep 1993" "TAE Plus v5.3"
.SH NAME
TAE_workspace  - to facilitate low level access to Xlib or X Toolkit

.SH DESCRIPTION
.\" force a blank line after a heading by .IP/.LP
.IP

.LP
X Workspace:  A general purpose rectangular subwindow.  It is
intended for use by sophisticated applications to draw and maintain
graphics.  The workspace is actually a subwindow with a name
attached to it.  The item can be any data type.

.SH INTERNAL NAME ("_TYPE") 
.IP

.LP
workspace

.SH PRESENTATION CATEGORY
.IP

.LP
X Workspace category

.SH TARGET DATA TYPE(S) SUPPORTED
.IP

.LP
String (TEXT *), Integer (TAEINT), and Real (TAEFLOAT).
However, the data type is not meaningful since no value
is returned to the application.


.SH TARGET COUNT
.IP

.LP
The count returned to an event handler for this presentation type is:
(-1). This means the target value is meaningless.

.SH EVENT GENERATION
.IP

.LP
When the user generates
any one of several types of X events
anywhere within the bounding box of the
X Workspace (except scrollbars, if any),
the application's event handler for this item is invoked.
The X events which trigger the event handler are:
.sp
.TS
center expand;
C i C i w(4.5i)
L L w(4.5i).
X Event	Occurs When...
.sp
Expose	T{
obscured portion of item becomes visible,
or item first becomes visible
T}
.sp
EnterNotify	user moves mouse into item
.sp
LeaveNotify	user moves mouse out of item
.sp
MotionNotify	user moves mouse within item
.sp
ButtonPress	user presses any mouse button
.sp
ButtonRelease	user releases any mouse button
.sp
KeyPress	user presses a keyboard key
.sp
KeyRelease	user releases a keyboard key
.sp
.TE

However, since the count
is (-1), there is no meaningful value returned to the application; do
not access the value[] vector.

Applications using the X Workspace are more likely to be
interested in X Window System information accessible through the
WptEvent associated with the X event. See the demo in
$TAEDEMOSRC called xworkspace.c.


.\" indirect .SH GETTING and SETTING TARGET VALUES
.so man5/novalueinfo.5

.SH CONSTRAINTS (VALIDS)
.IP

.LP
Constraints are not relevant to this presentation type.

.bp
.SH PRESENTATION SPECIFIC RESOURCES
.IP

.LP
The following resources, initially set at design time via the 
X Workspace Presentation panel in the TAE Plus WorkBench,
are specific to this presentation type.  
Each resource has a default value if not
explicitly set by the designer.
Each of these resources is stored in the item's \fIview\fP.

To change a presentation-specific resource at 
runtime using \fIWpt_ViewUpdate\fP, 
the resource must have a "Y" (yes) in the "Set" column of the following table.
Resources with an "N" (no) can only be controlled at design time in the
WorkBench, or at runtime \fIbefore\fP the item's panel is created, such as by
using the \fIVm_Set*\fP functions. 

The method for changing resources at runtime using 
\fIWpt_ViewUpdate\fP is illustrated in the CODE EXAMPLES section.
The method involving \fIVm_Set*\fP functions prior to panel creation
is discussed in the \fBTAE Plus Programmer's Manual.\fP

To retrieve the current setting of a view resource, use the appropriate
macro \fIStringParm\fP, \fIIntParm\fP, or \fPRealParm\fP
(defined in generated code),
depending on the data type of the qualifier given in the table.
(The macros \fISVAL\fP, \fIIVAL\fP, or \fIRVAL\fP should be used for
multi-dimensional resources, 
such as those declared as of type TAEINT[2] or TEXT**.)

.\" BEGIN PRESENTATION SPECIFIC TABLE
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Scrollbars
T}	sbDisplayPolicy	TEXT*	No	T{
controls whether or not the X Workspace will be scrollable;
choices are: "None", "As Needed",
or "Always"; WorkBench default: "None";
while in the WorkBench, scrollbars will disappear
if the designer resizes item larger than the Work Region
T}

T{
Work Region Width and Height
T}	workwinSize	TAEINT[2]	Yes	T{
specifies the item's  drawing area, which is typically larger than the
item's size qualifier in one or both dimensions;  the visible
portion (viewport) is controlled by the item's size qualifier;
WorkBench default: 200 x 200
T}
.TE
.\" END of PRESENTATION SPECIFIC TABLE

.sp 3
.SH COMMON RESOURCES
.IP

.LP
This group of resources is defined by using the Item Specification
panel in the WorkBench.
Each of these resources is stored in the item's \fIview\fP.

Note: The actual resource name to use with \fIWpt_ViewUpdate\fP appears
in the "Qualifier" column.

See \fITAE_intro\fP for a complete description of these Common Resources.
The table below highlights any presentation specific information about
these resources in the "Notes" column.

.\" BEGIN COMMON RESOURCES TABLE
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i) .
Resource	Qualifier	Type	Set	Notes
.sp
T{
Background Color
T}	bg	TEXT *	Yes	T{
applies to color inside the bounding box of the item, except for text; 
WorkBench default: panel bg
T}

T{
Border Width
T}	border	TAEINT	Yes	T{
specifies the thickness of a border outside the item; 
WorkBench default: 1
T}

T{
Font
T}	font	TEXT *	N/A	T{
font is ignored by this presentation type;
WorkBench default: panel font 
T}

T{
Foreground Color
T}	fg	TEXT *	Yes	T{
generally ignored by this presentation type;
only applies to the item's border color (if Border Width is positive); 
WorkBench default: panel fg
T}

T{
Keyboard Traversal
T}	traversal	TAEINT	No	T{
WorkBench default: 0; 
valid values: 0 (off; traversal disabled) or 1 (on; traversal enabled)
T}

T{
Location
T}	origin	TAEINT[2]	Yes	T{
origin is the upper left corner of the item's rectangular bounding box, 
relative to the upper left corner of the panel; 
WorkBench default: (0, 0) if using Specify Dimensions, dynamic otherwise
T}

T{
Shadow Thickness
T}	shadow	TAEINT	No	T{
currently unsupported due to Motif implementation details; 
see also Border Width; 
WorkBench default: 0 
T}

T{
Title
T}	T{
value of the view
T}	TEXT *	N/A	T{
title is ignored for the X Workspace;
WorkBench default: null string
T}

T{
Width and Height
T}	size	TAEINT[2]	Yes	T{
used to specify an item's width and height in pixels; 
size[0] is the width, size[1] is the height; 
WorkBench default: 200 x 200
T}
.TE
.\" END of COMMON RESOURCES TABLE


.SH CODE EXAMPLES
.IP

.LP
\fIAccess to X Library or X Toolkit functionality:\fP

In the following example, assume the application has a panel named
"panelname" and an item named "itemname".
If the application intends to draw into the X Workspace using X library (Xlib)
functions such as XDrawLine, XFillRectangle, etc., it is necessary to
retrieve the \fIwindow\fP id of the item using \fIWpt_ItemWindow\fP:

.in +3
.nf
Window drawWin;            /* X Window id */
Id panelnameId;            /* panel Id */

drawWin = Wpt_ItemWindow ( panelnameId, "itemname" );
.fi
.in -3

Most Xlib functions require a Display pointer
and a Window (or Drawable) as their
first two arguments. For example, the following fragment
draws a line in the X Workspace (given the above):

.in +3
.nf
GC gc;                        /* graphics context */
Drawable drawable;            /* either a Window or a Pixmap */
int x, y, xEnd, yEnd;         /* coordinates of endpoints of line */
static BOOL firstTime = TRUE; /* only want to create GC once */

if ( firstTime )
    {
    drawable = (Drawable) drawWin;
    gc = XCreateGC (Default_Display, drawable, 0, NULL);
    firstTime = FALSE;
    }
x = 10;
y = 100;
xEnd = 300;
yend = 150;
XDrawLine (Default_Display, drawWin, gc, x, y, xEnd, yEnd);
.fi
.in -3

The application can also obtain the \fIwidget\fP id for use with X Toolkit (Xt)
or Motif (Xm) function calls, after obtaining the X Window id:

.in +3
.nf
Widget workspaceWidget;    /* Xt widget id */
Window drawWin;            /* X Window id (obtained above) */
Display *Default_Display;  /* returned from Wpt_Init */

workspaceWidget = XtWindowToWidget (Default_Display, drawWin);
.fi
.in -3

Most Xt or Xm functions require a widget id as the first argument.

One possible use of the X Workspace at the X Toolkit (rather than Xlib) level
is to create a widget as the child of the widget associated with the
X Workspace. (The advantage of creating the widget as a child is that
its coordinates will always be relative to its parent; the X Workspace
can then be re-positioned in the WorkBench and its child will still maintain
the same relative position. Furthermore, the X Workspace can be used as a
container or subpanel containing a number of widgets.)

The following fragment illustrates this technique.
Note that the created widget need not be related to any existing
TAE Plus presentation type.

.in +3
.nf
#include <Xm/Xm.h>
#include <Xm/ArrowB.h>  /* for XmArrowButton */
Widget workspaceWidget;    /* Xt widget id */
Dimension width, height;
Position xloc, yloc;    /* coordinates relative to X Workspace */
Arg args[4];
int i = 0;

xloc   = 10;
yloc   = 10;
width  = 50;
height = 100;
XtSetArg (args[i], XmNx, xloc ); i++;
XtSetArg (args[i], XmNy, yloc ); i++;
XtSetArg (args[i], XmNwidth, width ); i++;
XtSetArg (args[i], XmNheight, height ); i++;

/* Create an ArrowButton as the child of the X Workspace. */
/* Assumes "workspaceWidget" has been set as per above fragment. */
XtCreateManagedWidget ( "Arrow", xmArrowButtonWidgetClass,
                        workspaceWidget, args, i );
.fi
.in -3


\fIResizing the Work Region of a Scrolled Workspace:\fP

\fINote:\fP This code example only applies if the X Workspace has scrollbars
as designated in the X Workspace Presentation panel of the WorkBench.

Some applications need to display an image or drawing whose
size cannot be determined until runtime.  A scrolled X Workspace
is ideally suited for this situation since the Work Region 
can be dynamically resized.

The relevant qualifier found in the Presentation Specific Resources table
is "workwinSize". Assume we have an item named "itemname" 
on a panel named "panelname". 

The following fragment changes the Work Region size
but does not affect the item size. 
The scrollbars will adjust to reflect
the new proportion of item size to Work Region size.
Note that Work Region is not
limited to the size of the screen; the maximum is 2000000000 
(2 billion) pixels in either dimension, 
which is roughly 2 million times the size of the screen in each dimension.

.in +3
.nf
Id panelnameId;         /* panel Id */
Id panelnameView;       /* Id of view Vm Object */
TAEINT WRsize[2];       /* desired Work Region size; use vector */

WRsize[0] = 9000;       /* width in pixels */
WRsize[1] = 22000;      /* height in pixels */
Vm_SetIntg (panelnameView, "itemname.workwinSize", 2, WRsize, P_UPDATE);
Wpt_ViewUpdate (panelnameId, "itemname", panelnameView, "itemname");
.fi
.in -3

Note that \fIpresdemo\fP uses this technique to resize the Work Region
to fit the size of the displayed bitmap which is larger than 
the WorkBench-defined size of the Work Region.  

.SH X WORKSPACE DEMOS
.IP

.LP
Additional code examples using the X Workspace can be found in $TAEDEMOSRC:

.TS
center ;
L i L w(4.0i).
xworkspace.c	T{
illustrates basic line  drawing
and ButtonPress coordinate access at the
X library level, how  to  access  the  X
Window id of a TAE Plus item, and \fIhow to
reference the X  event  structure\fP;  also
can  be  used to report X events such as
Expose,    MotionNotify,    EnterNotify,
LeaveNotify,  KeyPress, KeyRelease, 
ButtonPress and ButtonRelease events
T}
.sp
presdemo	T{
displays a large bitmap in a scrollable X Workspace;
resizes Work Region to fit bitmap
(see pan_pwspace.c in the directory $TAEDEMOSRC/presdemo)
T}
.sp
bounce2.c	T{
an unbuilt demo which illustrates use of the X Workspace
T}
.TE

.SH SEE ALSO
TAE_intro (for Common Resources), 
StringParm, IntParm, or RealParm (for accessing target value and view attributes), 
Wpt_ViewUpdate, Wpt_ParmUpdate, 
Vm_Set*, Vm_Get*, Wpt_Set*, Wpt_Get*,
Wpt_ItemWindow

.so man1/../COPYRIGHT
.so man1/../AUTHOR

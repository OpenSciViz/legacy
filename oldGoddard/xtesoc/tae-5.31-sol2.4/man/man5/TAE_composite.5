'\" t
.\" Above is to invoke "tbl". MUST BE FIRST LINE OF FILE.
.\" Disable hyphenation below.
.nh
.\"
.TH TAE_composite 5 "23 Sep 1993" "TAE Plus v5.3"
.SH NAME
TAE_composite - DDO which contains multiple independent dynamics
of different types, all on a common static background

.SH DESCRIPTION
.\" force a blank line after a heading by .IP/.LP
.IP

.LP
Composite: (DDO) A container DDO to place a heterogeneous mixture
of DDO types on a common static background.  Each dynamic is
specified and manipulated as an independent DDO with its own
target and view characteristics (including value, thresholds, and
event-reporting behavior), but is contained in one TAE Plus item and
is drawn in a common \fItaeidraw\fP picture file.

A \fIComposite DDO\fP consists of one or more dynamics which may 
be of \fIdifferent\fP types.
For example, a Composite DDO may consist of four dynamics:
one mover dynamic, one rotator dynamic, and two stretcher dynamics.
In contrast, if a \fIhomogeneous DDO\fP contains multiple dynamics, 
the dynamics must all be the same type 
(e.g., a rotator may consist of three rotator dynamics).
See TAE_mover, TAE_rotator, TAE_stretcher, TAE_stripchart, 
and TAE_discrete_pictures man pages
for descriptions of the homogeneous DDOs.

Note: Alternatively, a Composite DDO may consist of multiple dynamics 
all of the \fIsame\fP type (e.g., three rotator dynamics).  However,
unlike a homogeneous rotator with three dynamics, the rotators in the
Composite DDO can have separate thresholds, event-reporting behavior,
minimum value, etc.

.SH INTERNAL NAME ("_TYPE")
.IP

.LP
The internal name of the presentation type (at the item level) is "compddo".
However, the internal names of the 
individual dynamics (at the qualifier level) depend upon
the DDO type of each dynamic; the possible names are:
"compmover", "comprotator", "compstretcher", 
"compstripchart" and "compdiscrete".
(The internal names for dynamics correspond to the 
choices in the "Dynamic Type" option menu in the 
Composite Presentation panel in the WorkBench.)

See the CODE EXAMPLES section for an example of retrieving
the type of the dynamic.


.SH PRESENTATION CATEGORY
.IP

.LP
Dynamic Data Object category

.SH TARGET DATA TYPE(S) SUPPORTED
.IP

.LP
Real (TAEFLOAT)

.SH TARGET COUNT
.IP

.LP
The count returned to an event handler for this presentation type is
equal to "numDynamics", the number of dynamics defined.
For example, if the Composite DDO consists of one mover dynamic, one rotator
dynamic, and two stretchers dynamics, the target count is four,
contained in value[0] through value[3].

\fIException:\fP
Each 2D mover dynamic requires two target values (corresponding to
the mover's X and Y coordinates).  Consequently, when a Composite DDO
contains at least one 2D mover dynamic, the usual one-to-one
mapping of dynamic n to value[n-1] is not applicable.
For example, consider a Composite DDO consisting of three dynamics.  
Suppose dynamic1 is a rotator, dynamic2 is a 2D mover,
and dynamic3 is a stretcher.  
In this case, value[0] corresponds to the rotator,
value[1] and value[2] correspond to the 2D mover, 
and value[3] corresponds to the stretcher.

.\" indirect .SH EVENT GENERATION
.so man5/ddoevent.5

.\" indirect .SH GETTING and SETTING TARGET VALUES
.so man5/realinfo.5

.SH CONSTRAINTS (VALIDS)
.IP

.LP
Constraints are not relevant to this presentation type.


.SH REQUIRED PICTURE NAMES
.IP

.LP

When a Composite DDO is created, the following picture names are mandatory.
The designer must assign these names to individual picture
objects using the "Specify Name" choice from the
Structure menu of \fItaeidraw\fP.

.\" BEGIN REQUIRED PICTURE TABLE
.TS
center ;
C b  C b
L w(1.8)  L w(4.8i) .
Picture Name	Description

static	T{
The fixed (background) portion of the DDO.
T}

dynamic(1-n)	T{
The dynamic portion of the DDO.  If the
Number of Dynamics is greater than one, additional
dynamics may be specified. The number at the end
of the dynamic name is the index of the dynamic.
For example, dynamic1 (or dynamic) is the  first
dynamic,  dynamic2  is the second, and so on. If
the dynamic index is greater than the Number of
Dynamics, the excess will be ignored.
T}
.TE

Picture dynamic1 is actually the
zeroth dynamic corresponding to target value[0],
dynamic2 corresponds to target value[1], etc.

Once again, 2D movers are an exception, 
as explained in the TARGET COUNT section above.

.SH OPTIONAL PICTURE NAMES
.IP

.LP

Each type of dynamic may have one or more optional pictures names
defined, such as "minlabel", "maxlabel", "start", "stop", "pivot", etc.
For the names of valid optional picture names for a particular Dynamic Type,
see the corresponding man page 
(TAE_mover, TAE_rotator, TAE_stretcher, TAE_stripchart, 
or TAE_discrete_pictures).

.bp
.SH PRESENTATION SPECIFIC RESOURCES (Item Level)
.IP

.LP

The two resources shown in the table below are defined at the item level
via the Composite Presentation panel. These resources pertain to the
\fIentire\fP Composite DDO item, rather than to each dynamic separately.
Each resource has a default value if not explicitly set by the
designer.  Each of these resources is stored in the item's \fIview\fP.

.so man5/viewinfo.5

.\" BEGIN PRESENTATION SPECIFIC TABLE
.TS
center ;
C b  C b  C b  C b  C b
l w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Picture File
T}	file	TEXT*	Yes	T{
pathname to  DDO picture created  with \fItaeidraw\fP;
this picture file contains all dynamics and any
additional (optional) named pictures 
T}

T{
Number of Dynamics
T}	numDynamics	TAEINT	Yes	T{
number of dynamics in this DDO
T}
.TE

.bp
.SH DYNAMIC-SPECIFIC RESOURCES
.IP

.LP
The following resources, initially set at design time via the 
Composite Presentation panel in the TAE Plus WorkBench,
are specific to each particular "Dynamic Type" (Mover, Rotator, etc.).

See the CODE EXAMPLES section for examples of
setting and getting values for these resources.


.ce 1
\fIMOVER\fP
.sp
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Mover Direction
T}	mdir	TEXT*	Yes/No	T{
"Vertical", "Horizontal", or "Both (2D)":
direction(s) the mover can move;
mdir can not be dynamically changed to or
from "Both (2D)"; it can be changed to or
from "Vertical" and "Horizontal";
WorkBench default: "Vertical".
T}
.TE

.ce 1
\fIROTATOR\fP
.sp
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Default Rotation Point (Horizontal)
T}	hrotpt	TEXT*	Yes	T{
if no Pivot picture is defined, this resource and the
corresponding Vertical resource specify a default
point about  which the dynamic portion of the DDO
will rotate;  choices are: "Left", "Center", and "Right";
WorkBench default: "Center"
T}

T{
Default Rotation Point (Vertical)
T}	vrotpt	TEXT*	Yes	T{
if no Pivot  picture is defined, this resource and
the corresponding Horizontal resource specify a
default  point about which the dynamic
will rotate;  choices are: "Top", "Center",
and "Bottom"; WorkBench default: "Center"
T}

T{
Rotation Direction
T}	rdir	TEXT*	Yes	T{
specifies the direction of motion of the dynamic;
choices are: "Clockwise" or "CounterClockwise";
WorkBench default: "Clockwise"
T}

T{
Maximum Rotation Angle (Override)
T}	rotang	TAEFLOAT	Yes	T{
overrides the maximum rotation angle defined by
the Stop picture;  in degrees;  WorkBench
default: 0.0 degrees
T}
.TE

.ce 1
\fISTRETCHER\fP
.sp
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Stretcher Direction
T}	sdir	TEXT*	Yes	T{
specifies the direction of motion of the dynamic;  choices
are:  "Vertical" or "Horizontal"; WorkBench default: "Vertical"
T}
.TE

.ce 1
\fISTRIPCHART\fP
.sp
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Interval (Milliseconds)
T}	interval	TAEINT	Yes	T{
specifies the time delay in milliseconds to wait before
plotting the next chart segment;  if the interval is set
to zero, the stripchart will only update on demand;
WorkBench default:  1000 milliseconds
T}

T{
Segments per  Line
T}	segments	TAEINT	Yes	T{
number of line segments to draw across the width of the plotting area;
WorkBench default:  32
T}
.TE

.ce 1 
\fIDISCRETE PICTURES\fP
.sp
.ce 1
No special resources.

.bp
.SH COMMON DYNAMIC RESOURCES
.IP

.sp 1
The following resources, initially set at design time via the 
Composite Presentation panel in the TAE Plus WorkBench,
may be controlled on a per dynamic basis.
Each resource is available for each Dynamic Type, unless noted otherwise.

.\" BEGIN COMMON DYNAMIC TABLE
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i).
Resource	Qualifier	Type	Set	Description
.sp
T{
Color Thresholds
T}	rangeval	TEXT**	Yes	T{
list of colornames in the same order as the Threshold Values list;
\fIdoes not apply to discrete pictures and 2D mover dynamics\fP
T}

T{
Picture Thresholds
T}	rangeval	TEXT**	Yes	T{
list of picture names in the same order as the Threshold Values list;
\fIapplies only to discrete pictures dynamics\fP
T}

T{
Range Maximum
T}	max	TAEFLOAT	Yes	T{
maximum value for this dynamic;
WorkBench default: 100.0
T}

T{
Range Minimum
T}	min	TAEFLOAT	Yes	T{
minimum value for this dynamic;
WorkBench default: 0.0
T}

T{
Threshold Values
T}	ranges	TAEFLOAT*	Yes	T{
list of thresholds in the same order as the Color Thresholds list 
(or Picture Thresholds, in the case of a discrete pictures dynamic);
does not apply to 2D mover dynamics
T}

T{
Update Delta
T}	delta	TAEFLOAT	Yes	T{
minimum change in target value necessary before
object is visually updated.  During direct manipulation
of an input-enabled dynamic, this resource controls how
much the user has to move the mouse pointer before
the dynamic will move.  The target will not change
(and an event will not be reported) until
the user has moved the dynamic in excess of the update delta.
WorkBench default: 0.0
T}

T{
Value Display
T}	venable	TEXT*	Yes	T{
"Enabled" or "Disabled" displaying of dynamic's
numeric value;  WorkBench default: "Enabled"
T}

T{
Value Format
T}	vformat	TEXT*	Yes	T{
C format string for displaying value;
this overrides the picture file's value format (if any)
T}

T{
Input Enabled for Dynamics
T}	input_enable	TAEINT	Yes	T{
indicates whether or not direct manipulation can be used to change the
target value; \fIdoes not apply to discrete pictures and stripchart
dynamics\fP; valid values: 0 (off; direct manipulation disabled) or 1
(on; direct manipulation enabled); WorkBench default: 0.
T}

T{
At Beginning
T}	report_begin	TAEINT	Yes	T{
indicates whether or not the item's event handler is called when the
user presses the mouse button down; valid values: 0 (off) or 1 (on);
WorkBench default: 0.
T}

T{
During Drag
T}	report_drag	TAEINT	Yes	T{
indicates whether or not the item's event handler is called as the
user drags the mouse after clicking on an input-enabled dynamic;
valid values: 0 (off) or 1 (on); WorkBench default: 0.
T}

T{
At End
T}	report_end	TAEINT	Yes	T{
indicates whether or not the item's event handler is called when the
user releases the mouse button; valid values: 0 (off) or 1 (on);
WorkBench default: 1.
T}
.TE
.\" END of COMMON DYNAMIC TABLE

.sp 3
.SH COMMON RESOURCES (Item Level)
.IP

.LP
This group of resources is defined by using the Item Specification
panel in the WorkBench.
Each of these resources is stored in the item's \fIview\fP.

Note: The actual resource name to use with \fIVm_SetReal\fP appears
in the "Qualifier" column.

See \fITAE_intro\fP for a complete description of these Common Resources.
The table below highlights any presentation specific information about
these resources in the "Notes" column.

.\" BEGIN COMMON RESOURCES TABLE
.TS
center ;
C b  C b  C b  C b  C b
L w(1.2i)  L  L  L  L w(2.8i) .
Resource	Qualifier	Type	Set	Notes
.sp
T{
Background Color
T}	bg	TEXT *	Yes	T{
applies to the fill area within the item's bounding box 
where no DDO picture object appears;
WorkBench default: panel bg
T}

T{
Border Width
T}	border	TAEINT	Yes	T{
specifies the thickness of a border outside the item; 
WorkBench default: 0
T}

T{
Font
T}	font	TEXT *	No	T{
font is ignored by this item; 
use taeidraw to create text picture objects
such as title and value
T}

T{
Foreground Color
T}	fg	TEXT *	Yes	T{
applies only to the item's border color (if Border Width is positive); 
WorkBench default: panel fg
T}

T{
Keyboard Traversal
T}	traversal	TAEINT	No	T{
WorkBench default: 0; 
valid values: 0 (off; traversal disabled) or 1 (on; traversal enabled)
T}

T{
Location
T}	origin	TAEINT[2]	Yes	T{
origin is the upper left corner of the item's rectangular bounding box, 
relative to the upper left corner of the panel; 
WorkBench default: (0, 0) if using Specify Dimensions, dynamic otherwise
T}

T{
Shadow Thickness
T}	shadow	TAEINT	No	T{
currently unsupported for DDOs; 
see Border Width; WorkBench default: 0
T}

T{
Title
T}	T{
\(dg
T}	TEXT *	Yes	T{
displayed at the location of the \fItitle\fP picture object, if any;
if no such picture is defined, this resource is ignored;
WorkBench default: null string
T}

T{
Width and Height
T}	size	TAEINT[2]	Yes	T{
used to specify an item's width and height in pixels; 
size[0] is the width, size[1] is the height; 
WorkBench default: dynamic
T}
.TE
.\" END of COMMON RESOURCES TABLE

\(dg Title is referenced by the \fIitem name\fP without
any specific qualifier. 

.SH CODE EXAMPLES
.IP

.LP
\fINOTE:\fP
Unlike homogeneous DDOs, each dynamic in a Composite DDO has its
own View Object, referenced by the number of the dynamic.  When
changing a view qualifier of a dynamic, refer to the dynamic as:

.ce 1
\fIitemName.dynamicNum.qualifier\fP

(where "itemName" is the name defined in the Item Specification
Panel and "dynamicNum" is the dynamic name described in the
\fIComposite Details Panel\fP.
For example, to access the movement direction of dynamic2 (a mover)
of Composite DDO "myComposite", use:

.ce 1
\fImyComposite.dynamic2.mdir\fP

.so man5/compositecode.5

.SH SEE ALSO
taeidraw, TAE_ddo_intro,
TAE_mover, TAE_rotator, TAE_stretcher, TAE_stripchart, 
TAE_discrete_pictures,
TAE_intro (for Common Resources), 
StringParm, IntParm, or RealParm (for accessing target value and view attributes), 
Wpt_ViewUpdate, Wpt_ParmUpdate, 
Vm_Set*, Vm_Get*, Wpt_Set*, Wpt_Get* 

.so man1/../COPYRIGHT
.so man1/../AUTHOR

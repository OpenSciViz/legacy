\fIUpdating a Composite DDO's Value (Multiple Dynamics):\fP

Assume we have a Composite DDO named "itemname" containing 
three dynamics:  "dynamic1" is a mover, "dynamic2" is a 2D mover,
and "dynamic3" is a rotator.

Note:  We are passing four values even though we only have three
dynamics.  This is because each 2D mover requires two target values.
See "Retrieving the Target Count of a Composite DDO".

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
Id panelnameTarget;     /* defined in the panel's header file */
TAEFLOAT values[4];
values[0] = 70.0;       /* value of dynamic1 */
values[1] = 20.0;       /* x value of dynamic2 */
values[2] = 67.0;       /* y value of dynamic2 */
values[3] = 35.0;       /* value of dynamic3 */
Vm_SetReal (panelnameTarget, "itemname",
            4, values, P_UPDATE);
Wpt_ParmUpdate (panelnameId, "itemname");
.fi
.in -3

Note that we must pass all the values even if we wish to change
only a single dynamic.  See "Retrieving the Value of a Composite 
DDO (multiple target values)".

\fIUpdating a Composite DDO's Value (Single Dynamic):\fP

Assume we have a Composite DDO item named "itemname"
containing only one dynamic on a panel named "panelname". 
NOTE:  This case is unlikely, because Composite DDOs almost
always have more than one dynamic.

The following fragment changes the Composite DDO's value to 98.6.  

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
TAEFLOAT value1;         /* NOT a vector */

value1 = 98.6;
/* Use Wpt_SetReal only for single valued (scalar) values */
Wpt_SetReal (panelnameId, "itemname", value1);
.fi
.in -3

\fIRetrieving the Target Count of a Composite DDO:\fP

\fIVm_GetAttribute\fP returns the count of an item's
value from a given view or target variable.  The following
example stores the count of the item's target in the
variable "targetCount".

Note:  As you will see in a later example, the target
count is needed to retrieve the value of any DDO with a target
count greater than one.


.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
COUNT targetCount;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;

Vm_GetAttribute (panelnameTarget, "itemname",
                 &dataType,            /* data type of ddo */
                 &targetCount,         /* number of values in target of ddo */
                 &dummyDefault,
                 &dummyAccess);
.fi
.in -3

\fIRetrieving the Value of a Composite DDO (multiple target values):\fP

Note: MAXVAL, defined in $TAEINC/taeconf.inp, is the maximum number of
values for a multiple count target.

.in +3
.nf
Id panelnameTarget;   /* defined in the panel's header file */
TAEFLOAT targetValue[MAXVAL];   /* array of values */
COUNT targetCount, i;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;   /* irrelevant return values */
struct VARIABLE *targetVariable;

Vm_GetAttribute (panelnameTarget, "itemname",
      &dataType,            /* data type of DDO */
      &targetCount,         /* number of values in target of DDO */
      &dummyDefault,
      &dummyAccess);

targetVariable = Vm_Find(panelnameTarget, "itemname");

for (i = 0; i < targetCount; i++)
    targetValue[i] = (RVAL(*targetVariable, i));
.fi
.in -3

\fIRetrieving the Value of a Composite DDO (single target value):\fP

NOTE:  The case of a Composite having only one target value is unlikely,
because Composite DDOs almost always have more than one dynamic.

.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
TAEFLOAT value1;	/* scalar value */

value1 =  RealParm(panelnameTarget, "itemname");
.fi
.in -3

\fIChanging the File which Describes a Composite DDO:\fP

In this example we are changing the picture file that the 
Composite DDO "itemname" is using to display the graphical pictures.
The relevant qualifier found in the Presentation Specific Resources table
is called "file".

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
Id panelnameView;       /* defined in the panel's header file */
TEXT *fileName[1];	/* vector value */

fileName[0] = "pict.ps";
Vm_SetString(panelnameView, "itemname.file", 1, fileName, P_UPDATE);
Wpt_ViewUpdate(panelnameId, "itemname", panelnameView, "itemname");
.fi
.in -3

\fIChanging the Color or Picture Thresholds of a Composite DDO\fP

Certain applications may need to dynamically assign new threshold
colors.  The relevant qualifier found in the Presentation
Specific Resources table is called "rangeval".  Assume we 
wish to change all five colors assigned to "dynamic1" of
Composite DDO "itemname". 

Note:  The number of values in the "rangeval" qualifier must always
be equivalent to the number of values in the "ranges" qualifier.

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
Id panelnameView;       /* defined in the panel's header file */
TEXT *colors[5];        /* must use vectors */

colors[0] = "green";
colors[1] = "yellow";
colors[2] = "brown";
colors[3] = "magenta";
colors[4] = "violet";
Vm_SetString (panelnameView, "itemname.dynamic1.rangeval",
              5, colors, P_UPDATE);
Wpt_ViewUpdate (panelnameId, "itemname",
                panelnameView, "itemname");
.fi
.in -3

\fIRetrieving View Resources for a Composite DDO:\fP

Two levels of information exist in the View object of a Composite
DDO.  The first level contains information pertaining to item
specific information.  The second level contains information
specific to a particular dynamic.  

\fIItem Level Information\fP

The following code retrieves the number of dynamics in the
Composite DDO "itemname".

.in +3
.nf
Id panelnameView;       /* defined in the panel's header file */
TAEINT numOfDyn;

numOfDyn   = IntParm(panelnameView, "itemname.numDynamics");
.fi
.in -3

\fIDynamic Level Information\fP

In this example we are retrieving view information specific to "dynamic1"
in Composite DDO "itemname".

Note:  This code is specific to "dynamic1". You would need to change this
qualifier to reflect the dynamic you are interested in. 

.in +3
.nf
Id panelnameView;       /* defined in the panel's header file */
TEXT *dynPresType, *moverDirection;

dynPresType  = StringParm(panelnameView, "itemname.dynamic1._TYPE");
moverDirection = StringParm(panelnameView,"itemname.dynamic1.mdir");
.fi
.in -3

\fIRetrieving the Last Selected Dynamic's Value of a Composite DDO:\fP

A programmer might be interested in knowing what is the last selected
dynamic Neither the Target or View object stores this information.  
The following code retrieves the last selected dynamic from the
Composite DDO "itemname" using the _SELECT qualifier.
(_SELECT[0] is the target index of the last selected dynamic.)

NOTE: If the last selected dynamic is a 2D mover, _SELECT will
be dimensioned to two.

.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
TAEFLOAT selectedValue[2];
TAEINT selectedIndex;
COUNT indexCount, i;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;
struct VARIABLE *targetVariable;

Vm_GetAttribute (panelnameTarget, "itemname._SELECT",
              &dataType,            /* data type of ddo */
              &indexCount,         /* number of values in target of ddo */
              &dummyDefault,
              &dummyAccess);
/* Retrieve the index value of last selected dynamic */
selectedIndex  = IntParm(panelnameTarget, "itemname._SELECT");
/* retrieve the target variable to be used for target value extraction */
targetVariable = Vm_Find(panelnameTarget, "itemname");
/* Retrieve last selected dynamic's value */
selectedValue[0] = (RVAL(*targetVariable, selectedIndex));
if (indexCount > 1)         /* 2D mover */
{
   /* Retrieve the second value for the last selected dynamic */
    selectedValue[1] = (RVAL(*targetVariable, selectedIndex+1));
    printf("Dynamic [2D Mover] value = (%f,%f)\n", selectedValue[0], selectedValue[1]);
}
else
    printf("Dynamic value = %f \n", selectedValue[0]);
.fi
.in -3

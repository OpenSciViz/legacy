
\fIUpdating a DDO's Value (single target value):\fP

The DDO's target value, 
in conjunction with threshold colors and ranges defined in the WorkBench, 
controls what is actually displayed 
at any point during the application's execution.
Assume we have a DDO item named "itemname" containing only one 
dynamic (picture name "dynamic" or "dynamic1")
on a panel named "panelname". 

The following fragment changes the DDO's value to 98.6.
The dynamic picture object will be displayed in the color assigned
to the range in which the value 98.6 falls.

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
TAEFLOAT value1;         /* NOT a vector */

value1 = 98.6;
/* Use Wpt_SetReal only for single valued (scalar) values */
Wpt_SetReal (panelnameId, "itemname", value1);
.fi
.in -3


\fIUpdating a DDO's Value (multiple target values):\fP

Consider a similar situation as in the above example, but suppose
the DDO ("itemname") was defined in the WorkBench (via \fItaeidraw\fP) to
consists of three dynamics. 
Assume we want to set the first dynamic to 100.0,
the second to 200.0, and the third to 300.0.
Use \fIVm_SetReal\fP and \fIWpt_ParmUpdate\fP instead
of \fIWpt_SetReal\fP (since \fIWpt_SetReal\fP is only for scalar
values; multiple dynamics requires a vector).
.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
Id panelnameId;         /* defined in the panel's header file */
TAEFLOAT values[3];     /* must use vectors */

values[0] = 100.0;
values[1] = 200.0;
values[2] = 300.0;
Vm_SetReal (panelnameTarget, "itemname", 3, values, P_UPDATE);
Wpt_ParmUpdate (panelnameId, "itemname");
.fi
.in -3

\fI Retrieving the Target Count of a DDO:\fP

\fIVm_GetAttribute\fP returns the count of an item's
value from a given view or target variable.  The following 
example stores the count of the item's target in the
variable "targetCount".  

Note:  As you will see in a later example, the target
count is needed to retrieve the value of any DDO with a target
count greater than one.

.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
COUNT targetCount;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;	

Vm_GetAttribute (panelnameTarget, "itemname",
                 &dataType,            /* data type of DDO */
                 &targetCount,         /* number of values in target of DDO */
                 &dummyDefault,
                 &dummyAccess);
.fi
.in -3

\fIRetrieving the Value of a DDO (single target value):\fP

The next example retrieves the target value of "itemname".

Note: This code is intended for DDOs with a single target value. If 
the dynamic type is ever changed to a 2D mover or the presentation type
is ever changed to a composite, the could code  fail.  A later
example showing the retrieval of multiple target values will work
in all cases (even single target values).  


.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
TAEFLOAT value1;	/* single value */

value1 =  RealParm(panelnameTarget, "itemname");
.fi
.in -3


\fIRetrieving the Values of a DDO (multiple target values):\fP

This example retrieves the target values of "itemname" and
stores them in the array "targetValue". 

Note: MAXVAL, defined in $TAEINC/taeconf.inp, is the maximum number of
values for a multiple count target.

.in +3
.nf
Id panelnameTarget;   /* defined in the panel's header file */
TAEFLOAT targetValue[MAXVAL];   /* array of values */
COUNT targetCount, i;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;
struct VARIABLE *targetVariable;

Vm_GetAttribute (panelnameTarget, "itemname",
      &dataType,            /* data type of DDO */
      &targetCount,         /* number of values in target of DDO */
      &dummyDefault,
      &dummyAccess);

targetVariable = Vm_Find(panelnameTarget, "itemname");

for (i = 0; i < targetCount; i++)
    targetValue[i] = (RVAL(*targetVariable, i));
.fi
.in -3

\fIChanging the File which Describes the DDO:\fP

The following fragment dynamically changes the picture file from which 
the DDO is obtained.
The relevant qualifier found in the Presentation Specific Resources table
is called "file".

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
Id panelnameView;       /* defined in the panel's header file */
TEXT *fileName[1];	/* vector value */

fileName[0] = "pict.ps";
Vm_SetString(panelnameView, "itemname.file", 1, fileName, P_UPDATE);
Wpt_ViewUpdate(panelnameId, "itemname", panelnameView, "itemname");
.fi
.in -3

\fIChanging the Color and Ranges of Thresholds of a DDO:\fP

Certain applications may need to dynamically assign new threshold
colors and/or ranges.  The relevant qualifiers found in the Presentation 
Specific Resources table are called "rangeval" and "ranges", respectively.  
Assume we wish to change all five colors and ranges assigned to this item. 

Note:  The number of values in the "rangeval" and "ranges" qualifiers
should always be equivalent to the number of thresholds specified in
the WorkBench.

.in +3
.nf
Id panelnameId;         /* defined in the panel's header file */
Id panelnameView;       /* defined in the panel's header file */
TEXT *colors[5];        /* must use vectors */
TAEFLOAT ranges[5];

colors[0] = "green";
colors[1] = "yellow";
colors[2] = "red";
colors[3] = "magenta";
colors[4] = "violet";
ranges[0] = 0.0;
ranges[1] = 20.0;
ranges[2] = 50.0;
ranges[3] = 80.0;
ranges[4] = 90.0;

Vm_SetReal (panelnameView, "itemname.ranges",
            5, ranges, P_UPDATE);
Vm_SetString (panelnameView, "itemname.rangeval",
              5, colors, P_UPDATE);
Wpt_ViewUpdate (panelnameId, "itemname",
                panelnameView, "itemname");
.fi
.in -3

\fIRetrieving View Resources:\fP

The following example retrieves the picture file, presentation type,
origin, size, and traversal status from the view object of the
DDO "itemname". 

Note: This is only a small subset of view resources for this
presentation type.  See PRESENTATION SPECIFIC RESOURCES and 
COMMON RESOURCES for a complete list.

.in +3
.nf
Id panelnameView;       /* defined in the panel's header file */
TEXT *file, *presType;
TAEINT origin[2], size[2], traversal;

file       = StringParm(panelnameView, "itemname.file");
presType   = StringParm(panelnameView, "itemname._TYPE");
origin[0]  = IVAL (*Vm_Find (panelnameView, "itemname.origin"), 0);
origin[1]  = IVAL (*Vm_Find (panelnameView, "itemname.origin"), 1);
size[0]    = IVAL (*Vm_Find (panelnameView, "itemname.size"), 0);
size[1]    = IVAL (*Vm_Find (panelnameView, "itemname.size"), 1);
traversal  = IntParm (panelnameView, "itemname.traversal");
.fi
.in -3

\fIRetrieving the Value of the Last Selected Dynamic:\fP

A programmer might be interested in knowing what is the last selected
dynamic, but neither the Target nor the View object stores this information.
The following code retrieves the last selected dynamic from the
the DDO "itemname" using the _SELECT qualifier.
(_SELECT[0] is the target index of the last selected dynamic.)

NOTE: If the last selected dynamic is a 2D mover, _SELECT will
be dimensioned to two.

NOTE 2: This example does not apply to stripcharts because _SELECT
is meaningless for stripchart dynamics.

.in +3
.nf
Id panelnameTarget;     /* defined in the panel's header file */
TAEFLOAT selectedValue[2];
TAEINT selectedIndex;
COUNT indexCount, i;
BOOL dummyDefault;            /* irrelevant return values */
CODE dataType, dummyAccess;
struct VARIABLE *targetVariable;

Vm_GetAttribute (panelnameTarget, "itemname._SELECT",
                 &dataType,            /* data type of DDO */
                 &indexCount,         /* number of values in target of DDO */
                 &dummyDefault,
                 &dummyAccess);
/* Retrieve the index value of last selected dynamic */
selectedIndex  = IntParm(panelnameTarget, "itemname._SELECT");
/* Retrieve the target variable to be used for target value extraction */
targetVariable = Vm_Find(panelnameTarget, "itemname");
/* Retrieve last selected dynamic's value */
selectedValue[0] = (RVAL(*targetVariable, selectedIndex));
if (indexCount > 1)         /* 2D mover */
{
    /* Retrieve the second value for the last selected dynamic */
    selectedValue[1] = (RVAL(*targetVariable, selectedIndex+1));
    printf("Dynamic [2D Mover] value = (%f,%f)\\n", selectedValue[0], selectedValue[1]);
}
else
    printf("Dynamic value = %f \\n", selectedValue[0]);
.fi
.in -3

.\" 
.\" 
.\"	@(#)taerecord.1	33.1 8/26/94
.\"
.TH taerecord 1 "20 Sep 1993"  "TAE Plus v5.3"
.SH NAME
taerecord, rec2pl, taeplayback - Utilities for Wpt scripting (to record and playback application sessions)


.SH INTRODUCTION
.IP

.LP
The three commands described in this man page are scripts for 
recording and playing back user events to a TAE 
Plus application.  Using the 
.I taerecord 
command, you may turn on "record"
mode in a TAE Plus application.  The application will then record
user events (button clicks, text entry, and so forth) to a file.

You may use 
.I taeplayback 
to play the session back.  The taeplayback utility
emulates a user, and reads the record file to deliver to the application 
exactly the sequence of
events that was recorded by the taerecord command.

.I rec2pl 
translates the record file to a \fItaeperl(1)\fP script.  You may 
edit the script to add control structures or any of the other
capabilities of taeperl.  

The commands and functions described in this man 
page and in the man pages listed in the SEE ALSO section
provide a facility for preparing test scripts that can thoroughly 
and repeatably exercise most TAE Plus applications.  

.SH CONCEPT
.IP

.LP
In the context of this man page, a TAE Plus application
is known as the "Application under Test" (AUT).
An AUT becomes "scriptable" when it calls Wpt_ScriptInit(3)
during initialization.   Wpt_ScriptInit creates a socket
to receive inter-process messages from taeperl scripts.
A scriptable application is still usable interactively, and it is 
common for operational applications to have
scripting unconditionally enabled. 

taeperl is the interpreter for a procedural, C-like language
with features for sending messages to AUTs.
The Wpt library has logic to receive and process messages
received from taeperl scripts.   The types of messages
that Wpt processes are:

.IP \(bu 
simulated user events: a script may operate
any item on any panel of the AUT.
.IP \(bu 
record  mode control: a script may enable or disable 
record mode.
.IP \(bu 
other: a script may request the window Id
of a panel, the target or view Vm associated with a panel,
the widget tree of a panel, etc.  (These particular Wpt services
are currently unsupported.)
.LP

\fIautsubs(3)\fP has the complete list of the services 
that Wpt offers to taeperl scripts. 

taerecord is implemented as a taeperl script. The
taerecord script sends the message "enable record
mode" to the AUT; the message includes the file
specification of the record file.  In response to
the receipt of this message, Wpt opens the file
and sets a flag so that subsequent user actions
will be captured in the file.   As the user
operates the AUT, Wpt writes one line to the
record file for each TAE Plus user event.  Note
that taerecord itself is trivial\(emthe real work
of recording is done by the Wpt library executing
in the context of the AUT.

To terminate recording, use taerecord's
"-stop" option.   This option causes taerecord
to send a message to the AUT requesting that the
record file be closed and that the AUT terminate 
record mode.  Again, taerecord's role is
minor\(emthe real work is done by Wpt.
Throughout the life of the AUT, you may enable
and disable record mode any number of times.

To "playback" the user actions captured in the
record file, you execute taeplayback.  taeplayback
converts the record file to a taeperl script and executes the
script.  The script sends "simulated user events" to
the AUT. 

To generate a test case written in taeperl, you should
run rec2pl against your record file to obtain a raw taeperl script.
You then edit the script to add loops, conditionals, error checking,
logging, pauses, etc.   The looping capability is especially
powerful:  you could, for example, place a series of commands
in a loop and let the test script run (unattended) for many hours.

The \fItaeperl(1)\fP man page has additional information 
concerning the relationship of the script and the Application
Under Test. 


.SH DESCRIPTIONS and SYNOPSES
.IP

.LP
\fBtaerecord -\fP
.in +5

% taerecord [-start | -stop] socketName [recordFileName]

The taerecord utility starts or stops record mode
in a TAE Plus application which has executed a
call to \fIWpt_ScriptInit(3)\fP.  When enabled,
record mode produces a record file with one line
for each TAE Plus application user event.  taerecord
sends a message\(em"disable" or "enable"\(emto the
application and then terminates; the work of writing
the record file is done by the application.

The \fIsocketName\fP argument is required.  It must be the same
socketName as specified in the Wpt_ScriptInit(3) call.
Most commonly, the
socketName is the name of the application.  See the example below.
If your current directory is different than the current directory of
the TAE Plus application, you may have to specify a directory
as part of the socketName; see Wpt_ScriptInit(3).

If neither \fI-start\fP nor
\fI-stop\fP is specified, \fI-start\fP is assumed.
If no 
.I recordFileName 
is given, user events will be recorded in the file
<\fIsocketName\fP>.rec.  If \fIrecordFileName\fP is specified without an
extension, an extension of ".rec" will be appended to it.
A \fIrecordFileName\fP provided with an extension will be used exactly
as specified.  If the record file already exists, taerecord will solicit and
await confirmation before overwriting the existing file.

If
.I recordFileName
is specified as "-", then the user-event records will be sent to the
application's (not taerecord's) stdout.

Note:
.I recordFileName
is opened and written in the context of the
application.  Therefore, if the 
.I recordFileName
specification has no explicit directory,
then the file is created in the directory where the application is running.

If record mode is already enabled when taerecord is called with the
\fI-start\fP option, the existing record file will be closed and
recording will continue to the record file specified in the new
taerecord command line.  

If 
.I -stop
is specified, the record file will be closed and recording will stop.
If \fIrecordFileName\fP is provided it will be ignored.

An application can itself initiate recording by calling
\fIWpt_SetRecord(3)\fP.
.in -5

\fBrec2pl -\fP
.in +5

% rec2pl recordFileName > taeperlScript.pl

rec2pl reads \fIrecordFileName\fP, which must be a file in the format
produced by a TAE Plus application in record mode, and writes a 
.I taeperl(1)
script to standard output.  Use rec2pl to translate your
record file to a taeperl script if you wish to edit the
session before playing it back.  If you do not want to edit
the session, you may invoke taeplayback immediately after
recording your session.  In that case taeplayback will
invoke rec2pl for you.

If 
.I recordFileName
has no extension, an extension of ".rec" will be appended to it.

The generated taeperl script runs with a "pace"
value of one second, i.e., the script delays one
second before each simulated user action.  You
may edit the script and set the pace to your
liking by changing the argument to the
&Aut'SetPace subroutine call.
.in -5



\fBtaeplayback -\fP
.in +5

% taeplayback recSessionFile

taeplayback plays back the user session stored in \fIrecSessionFile\fP.
If 
.I recSessionFile 
has an extension of ".rec" it is assumed to be a
record file in the format generated by a TAE Plus application in record
mode.  
If 
.I recSessionFile 
has no extension, an extension of ".rec" will be
appended, and it will be assumed to be in record file format.  If 
.I recSessionFile
has an extension other than ".rec", it
is assumed to be a taeperl script.

The
.I recSessionFile 
specifies a socket name, and the target
application (which must have called Wpt_ScriptInit to make a 
connection to an identically named socket) must be up
and running when taeplayback is invoked.  

Also, when taeplayback is started, the
application must be in a state such that the events to be played back
are permissible.
The playback will not work, for example, if events
for items on nonexistent panels are specified in the \fPrecSessionFile\fP.

taeplayback is a shell script that invokes taeperl (after converting
the record file to a taeperl script, if necessary).  If you need to
use options on the taeperl command line, you can play your session back
using taeperl directly\(emsee the EXAMPLE SCENARIO below.
.in -5


.SH RECORD FILE FORMAT
.IP

.LP
The record file is a text file with fields
delimited by the ASCII FS (field separator) control
character.  (This character is usually rendered as "^\\" or
"ctrl-backslash".  Its character code is octal 034, hex 0x1c, 
or decimal 28.)

There is
a header line followed by one line per
user event.  The header line has the 
following fields:
.IP \(bu 
"header" to identify the line.
.IP \(bu 
current system time (from \fItime(3)\fP) expressed as a decimal number.
.IP \(bu 
the name of the application's socket, that is, 
the string passed to Wpt_ScriptInit.
.IP \(bu 
current system time in text form (from \fIasctime(3)\fP).

.LP

An event line has the following fields:
.IP \(bu 
the time of the event, expressed as the number of seconds
since record mode was turned on, that is, the delta from the header time.
.IP \(bu 
"IntgEvent", "StrEvent", or "RealEvent", indicating
the type of item that the user operated.
.IP \(bu 
the panel name of the event.
.IP \(bu 
the item name of the event.
.IP \(bu 
the value of the item enclosed in parentheses; if the
item has no value, then "()" appears in this field.
If the item has multiple values, the values are separated
by parentheses.  For string values, quotes are escaped with
backslash.
.LP

Here is a sample record file:
\fR
.nf
header^\\734363812^\\my_app^\\Fri Apr  9 09:56:52 1993^\\
5^\\StrEvent^\\my_pan^\\CONTINUE^\\()^\\
8^\\StrEvent^\\my_pan^\\CONTINUE^\\()^\\
17^\\StrEvent^\\my_pan^\\QUIT^\\()^\\
.fi
\fP

.SH EXAMPLE SCENARIO
.IP

.LP
.nr L 0 1
.IP \n+L. 
Use the WorkBench to create a resource file and generate code for 
an application called "my_app".  

.IP \n+L. 
Edit the generated code to enable the call to Wpt_ScriptInit. 

.IP \n+L. 
Build the application and start it
in the background.  If you now get a listing of the files in your
working directory, you will see that Wpt_ScriptInit has created a socket
entry, my_app.sk.

.IP \n+L. 
Invoke taerecord:

    % taerecord -start my_app 

taerecord will send a message to my_app through the socket my_app.sk.
Since you did not specify a record file, taerecord will tell the
application to record to my_app.rec.

.IP \n+L. 
Interact with the interface by clicking on buttons, making menu
selections, entering text, and so on.

.IP \n+L. 
Disable record mode:

    % taerecord -stop my_app

You now have a record file (my_app.rec) in which your actions are
stored.  Ensure that my_app is still running, and that it is in the same
state it was in when you turned on record mode.  That is, make sure that
the same panels are displayed, the same items are sensitive, and so on.
If you choose, you may terminate and restart my_app.

.IP \n+L. 
Now, play the session back:

    % taeplayback my_app

An extension of ".rec" will be appended to my_app, and
taeplayback will invoke rec2pl to convert my_app.rec to
a taeperl script called my_app.pl.  taeperl will then be
invoked to do the playback.

.LP

You may, if you choose, convert the record file to a script file:

    % rec2pl my_app > my_app.pl

and then edit my_app.pl to add additional logic.  
Make sure that my_app is running and in the proper state.  You can then play
back the edited session:

    % taeplayback my_app.pl

In this case, the ".pl" extension tells taeplayback that the
input file is a taeperl script, so no attempt at translation
will be made.

You could play back the edited taeperl script using taeperl directly:

    % taeperl my_app.pl

This approach is usually preferred over using taeplayback to execute
the script because taeperl supports the \fIperl(1)\fP command line options.

.SH LIMITATIONS

.IP \(bu 
Events from Help panels cannot be recorded or played back.
.IP \(bu 
Individual selection events from a multiple selection list cannot be
recorded or played back.  However, the final
event (generated when ESC is pressed or when the maximum number of
selections is made) can be recorded.
.IP \(bu 
Recording and playing back events from an item of type "scroller"
(color logger)
produces the following results: for playback, the lines
selected during record are scrolled to the logger.   This happens
because the logger does not have the symmetry of input and output
that other TAE Plus presentation types have.  The application
supporting the logger does, however, receive the same events
during playback that were generated by the user during record. 
.IP \(bu 
events from an X Workspace cannot be recorded or played back.
(The X Workspace may, however, be scripted using the Aut'WidgetAction
subroutine described in autsubs(3); you must code such calls manually.)
.IP \(bu 
Events delivered to window manager frame decorations or
icons cannot be recorded or played
back.  Thus, if a user iconifies and then restores a panel, the sequence
cannot be recorded or reproduced.  Also, if the application iconifies a
panel (or brings it up in iconic form), the user's act of clicking on
the icon to restore the panel cannot be recorded or played back.

.SH SEE ALSO
\fItaeperl(1)\fP, \fIautsubs(3)\fP,
\fIWpt_ScriptInit(3)\fP, \fIWpt_IsScripted(3)\fP, \fIWpt_SetRecord(3)\fP, 
\fIWpt_ScriptUnsup(3)\fP

.so man1/../COPYRIGHT
.so man1/../AUTHOR

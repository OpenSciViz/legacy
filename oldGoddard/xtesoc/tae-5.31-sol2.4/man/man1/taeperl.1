.\" t
.\" 
.\"  @(#)taeperl.1	33.1 8/26/94
.\"
.TH taeperl 1 "2 Sep 1993"  "TAE Plus v5.3"
.SH NAME
taeperl \- perl interpreter with TAE Plus subroutines to support Wpt scripting

.SH SYNOPSIS
.IP

.LP
taeperl [options] scriptFile

Note: the environment variable TAEPERLLIB must be defined
before running taeperl; see \fISetting Up Your TAE Plus
Environment\fP.

.SH DESCRIPTION
.IP

.LP
.SS General
.IP

.LP
taeperl is the standard perl language with TAE Plus
functions available as "built-in" subroutines. 
Using the high-level, interpreted taeperl language, you may 
write test scripts to simulate the user input to an
application.  This capability provides an automatic
testing facility. 

In addition to testing, there are experimental
(and currently unsupported) features in taeperl
that allow you to:

.IP \(bu 
write application programs that have a TAE Plus
graphical user interface.   taeperl is ideal as a
prototyping language.  It is also possible to
write polished, responsive, operational
applications in taeperl.  When writing in taeperl,
the TAE Plus WorkBench is used to design the
graphical appearance of the application just as
if you were using the C language to implement the
application.
.IP \(bu 
write "listening" programs that periodically observe
the system and notify an application.  For example,
a taeperl script could wake up every minute, look for
high priority mail, and alert the application via
an inter-process message.  In response to the message, the
application could alert the user via a popup.  
.IP \(bu 
write resource file utilities.   Using the 
taeperl language,  you may read, analyze, or update 
TAE Plus resource files.  For example,  with about
15 lines of taeperl, you can generate a report "show
the titles of all pulldown menus".   See EXAMPLES below.
.LP

.SS "What is perl?"
.IP

.LP
perl is a high-level procedural language released by the
Free Software Foundation and packaged with
TAE Plus.  perl is designed as a replacement for
(and unification of) the classic UNIX utilities
grep, sed, and awk; the language is described
in
.I Programming perl
by Wall and Schwartz.

taeperl consists of extensions to perl using the
standard perl hooks for "user-defined
subroutines".  Other than the replacement of the
usersub.c module, taeperl does not impose changes
to the standard perl delivery. 

.SS Testing Concept
.IP

.LP
A TAE Plus application process being tested is called 
the Application under Test (AUT).

The test scenario typically  consists of
two processes: (a) the taeperl interpreter running a
script, and (b) the AUT.  The script executes
subroutines that build and send Vm objects to the
AUT via an Interprocess Communication (IPC) mechanism. 
The AUT listens for both X events and the
receipt of Vm objects from the script.  When script Vm
objects arrive at the AUT, TAE Plus Wpt 
logic receives each message, analyzes message
contents, and calls an EVENT_HANDLER for the
simulated user input.  The following considerations apply:

.IP \(bu 
Vm objects are TAE Plus "symbol tables" as described in 
\fITAE Plus Programmer's Manual\fP. 

.IP \(bu 
The AUT must enable the receipt of script messages
by calling Wpt_ScriptInit(3) during initialization.

.IP \(bu 
The script is interlocked with the AUT, i.e., the
script is blocked until the AUT's event handler returns
to Wpt. 

.IP \(bu 
In the simple case there are two processes, taeperl and
the AUT.   There is, however, no restriction on the
number of AUTs that can be controlled from one script.
.LP


.sp 
The interaction between taeperl and the AUT is
inspired by "remote procedure call" (RPC) technology:  

.IP \(bu 
the script sends a message to the AUT
directing what AUT library function should be
called; the message also contains parameters
for the function.

.IP \(bu
Wpt, running in the context of the AUT, receives
the message and calls the function identified in 
the message.

.IP \(bu 
the function executes and returns to Wpt;
Wpt builds a response message and sends
the message back to the script.

.IP \(bu 
the script continues execution, with the results
of the RPC available as script variables.
.LP

The desired effect is that the script appears to be 
calling functions in the AUT, i.e. the taeperl process
is "pulling the strings" of the AUT.

The following is a simple script that drives the
AUT named "xyz".  This script presses the "run"
button on the "demo" panel, delays 10 seconds,
then presses "cancel";  the sequence is repeated
1000 times.

.nf
\fB
  #    test.pl
  #
  #    Simulate user input for application "xyz". Shell command:
  #
  #        % taeperl test.pl
  #
  require 'autSubs.pl';    # get subroutines from $TAEPERLLIB
  $socketHandle = &Aut'Connect ("xyz");   # connect to AUT
  &Aut'SetPace(2);                        # 2 secs between events 
  &Aut'SetTrace(1);                       # log simulated events
  foreach (1..1000)                       # 1000 times:
      {
      &Aut'UserEvent ($socketHandle, "demo", "run");    # press "run" 
      sleep 10;
      &Aut'UserEvent ($socketHandle, "demo", "cancel"); # press "cancel"
      }
  &Aut'Close ($socketHandle);
\fP
.fi

\fIautsubs(3)\fP describes the subroutines called in the
above example.
\fItaerecord(1)\fP has a description of the record/playback
capability that you may use to generate scripts automatically.


.SH OPTIONS
.IP

.LP
The taeperl command line options are the same as
for \fIperl(1)\fP.  The most commonly used command line
option is "-I", which specifies the directory for
included (via the perl \fIrequire\fP statement)
scripts.  Note that "-I$TAEPERLLIB" is implicitly
part of the taeperl command line.

The -P option (that requests the C pre-processor
be run against the script before execution) may not
work on your system unless you have re-installed perl as described
under "CONFIGURING PERL" below.


.SH CONFIGURING PERL
.IP

.LP
The full perl source delivery is packaged with
TAE Plus in the file $TAE/taeperl/perl.tar.Z.  
A partial installation of perl has already been
performed by Century and the perl man page has
been placed in $TAEMAN/man1/perl.1.  After you
complete the standard TAE Plus installation
procedure, taeperl is ready for use.

You may wish to re-install perl in order to (a)
use different installation options or (b) obtain
the complete perl configuration.  It is
recommended that you re-install perl if your site
is a heavy user of perl or taeperl.  The
advantages of re-installing are:

.IP "perl utilities" 
perl and the perl utilities (e.g., h2ph, and a2p) get installed
in a standard directory for general use.

.IP "perl library subroutines"
The standard perl library packages (e.g, ctime.pl) get
installed in a standard directory for general use.

.IP "correct configuration"
With the perl and taeperl pre-installed 
by Century, there are certain default path names
that may not exactly match your system.  The
effects of these path conflicts are generally minor; the
most prominent problem is that perl and taeperl
attempt to find the cpp processor in
"/usr/lang/cpp" and this path may not be correct
for your system.  The effect of this particular conflict is
that the -P command line option ("run the script through the C
pre-processor before execution") may not work. 

.LP

To re-install perl,  follow the  
instructions in the file $TAE/taeperl/README.


.SH FILES
.IP

.LP
By convention, taeperl script files use the .pl extension.

.SH SEE ALSO
.IP

.LP
\fIProgramming perl\fP 
(Wall and Schwartz,
O'Reilly and Associates, Inc., Sebastopol, CA)
describes the perl language itself.  This book
and the perl technology is highly recommended for
anyone who programs under UNIX (graphical user interfaces 
or otherwise).

\fIperl(1)\fP
is a terse (but complete) 120-page description of the perl language.

\fIautsubs(3)\fP
describes taeperl subroutines (themselves written in taeperl)
that provide access to an AUT (Application Under Test).

\fIWpt_ScriptInit(3)\fP
explains how to initialize the Wpt scripting mechanism.

\fIWpt_IsScripted(3)\fP
can be used to determine whether Wpt scripting is enabled.

\fIWpt_SetRecord(3)\fP
enables or disables record mode for Wpt scripting.

\fIWpt_ScriptUnsup(3)\fP
describes how to write an application-resident "registered"
function that may be called from a taeperl script.   For 
many applications, the standard "registered" functions
for simulating user input are sufficient.

\fItaerecord(1)\fP
describes how to start and stop record mode for an AUT.
This man page also describes 
\fItaeplayback\fP, used to playback the session,
and \fIrec2pl\fP, which translates a record file to a \fItaeperl\fP
script that can be refined for greater application control.

.IP "\fITAE Plus Programmer's Manual\fP"
Provides descriptions of TAE Plus applications
and the C-callable versions of the Vm and Wpt functions.


.SH WARNING
.IP

.LP
perl (and consequently taeperl) is known to have
some memory leaks.   If you plan to have a script
that runs over a period of many hours, you should
monitor the virtual size of the taeperl process.


.SH TAEPERL SUBROUTINES (UNSUPPORTED)
.IP

.LP
The taeperl subroutines in this section are used
to implement the higher-level subroutines
described in autsubs(3).  The calling sequences,
names, and behavior of the subroutines described
here are subject to change in future releases.
\fIWhen you write scripts, you should use the
fully-supported subroutines described in autsubs(3)\fP.

Generally, the TAE Plus subroutines in taeperl have
the same arguments as the corresponding
C functions in TAE Plus.  Exceptions are noted
below.  Note that not all TAE Plus functions have 
a corresponding taeperl subroutine. 

The calling sequences are shown in perl syntax.

.SS Return Value Philosophy
A TAE Plus function that, in C,  returns a
success/fail code will, in taeperl, return zero for
failure and non-zero for success.  This
philosophy is consistent with the perl "do or
die" paradigm:

.nf
\fB
    &Wpt_Init ("xt4:0.0") || die "cannot open display";
\fP
.fi

In the subroutine prototypes below, subroutines
suitable for "do or die" are shown as 

.nf
\fB
    $code = &subroutine(args);
\fP
.fi

taeperl subroutines that return data, e.g., Vm_Get,
return the perl "undefined" value when the
data cannot be obtained. 

.SS Object Id Checking
When an object Id (i.e., "handle") is passed to a taeperl
subroutine, taeperl checks the Id and aborts if the
Id is invalid.   This object checking is
performed for Vm IDs, Co IDs, and panel IDs.


.SS IPC Subroutines

These perl subroutines provide bindings to an
Interprocess Communication (IPC) package for
scripting a TAE Plus application.  Generally,
programmers do not call these directly, but
instead use the higher level subroutines in
autsubs(3).

.IP "$skt = &Ipc_Connect ($socketName);"
Establish a connection to the application that has
called Wpt_ScriptInit with the $socketName string.
$skt is a handle for use in subsequent Ipc calls.
$skt is less than zero if the connection
cannot be made.

.IP "&Ipc_Close ($skt);"
Closes the connection.  

.IP "$bool = &Ipc_Send ($skt, $vmId);"
Sends the Vm object to the server.   Vm objects are constructed
using Vm_New, Vm_Set and the other subroutines described below.
Returns TRUE if successful.

.IP "$vmId = &Ipc_Receive ($skt);"
Reads the next message from the connection and converts the message
into a Vm object.  The caller must free the returned object
with Vm_Free.   Ipc_Receive blocks until a message is received. 
Returns undef if a message cannot be read, i.e., some error with
the connection.

.IP "$vmId = &Ipc_SendReceive ($skt, $vmId);" 
Sends the Vm object, waits for a response, and returns the 
received Vm object.   $vmId is undef if an error occurs.


.LP


.SS Co Subroutines

See \fITAE Plus Programmer's Manual\fP 
for a description of TAE's "Collection (Co)" 
package.   The following taeperl subroutines enable
access to Co from scripts:


.IP "$code = &Co_Add($collectionId, $objectId, $name);"
Adds an object to a collection.  

.IP "$vmId = &Co_Find($collectionId, $name);"
Zero means not found and non-zero is the Id of
the named Vm object.

.IP "$code = &Co_Free($coId);"
The returned code is always true; calls to to 
&Co_Free typically ignore the return code.

.IP "@nameVector = &Co_Names($collectionId);"
Returns a vector of object names in the collection.
For a TAE Plus resource file, this is a list
of the Vm names.   This subroutine is typically
used to get a list for iteration over all Vm
objects in a TAE Plus resource file; see "EXAMPLES" below.

.IP "$collectionId = &Co_New;"
Creates a new, empty collection and returns the Id.

.IP "$code = &Co_ReadFile($collectionId, $fileName, [$flags]);"
If the $flags argument is omitted, then P_CONT is
assumed and no message is generated when an error
is found--the returned value is false.  P_ABORT
requests termination (with error message) upon
error.  You may code the P_ABORT constant as a perl subroutine
call, e.g., "&Co_ReadFile($coId, $fileName, &P_ABORT);".

.IP "$code = &Co_WriteFile($collectionId, $fileName);"


.SS Vm Subroutines

See \fITAE Plus Programmer's Manual\fP 
for a description of TAE's "Variable Manipulation (Vm)"
package.    Most of the Vm subroutines have the
same calling sequence as the C counterparts, though
there are exceptions.    There are some new Vm
subroutines here that have no C counterpart;
most notable in this category are Vm_Set and Vm_Get, which
provide a "typeless" perl-like calling sequence. 

.IP "%assoc = &Vm_Assoc ($vmId [,$name]);"
Creates an associative array from a Vm object; if
$name is present, then Vm_Assoc creates the
associative array from the qualifiers of the
named symbol.  The "value" returned by Vm_Assoc
is actually a vector of name/value pairs which,
when assigned to an associative array, creates
named values in the array.   All symbols are
treated as scalars, i.e., only the first element
is selected from a multi-valued symbol.  Symbols that
have no value are not returned by Vm_Assoc; thus, 
referencing such a symbol name in the associative array
produces undef.

Examples:

.nf
\fB
    print "values in the Vm:\\n";
    %assoc = &Vm_Assoc ($vmId);             # symbols from the Vm
    while (($name, $value) = each %assoc)
        {
        print "$name=$value\\n";
        }

    print "qualifiers of x:\\n";
    %assoc = &Vm_Assoc ($vmId, "x");        # qualifiers of "x"
    while (($name, $value) = each %assoc)
        {
        print "$name=$value\\n";
        }

\fP
.fi

If you want to iterate over the symbols in a Vm
or the qualifiers of a symbol, you might also try
Vm_Names, which does not limit you to scalar symbols (as
Vm_Assoc does).


.ig xx
.IP "$binary = &Vm_AsPerl($vmId);"
X (This subroutine is experimental.) Returns a
X string of binary bytes that represent the Vm
X object as a relativized parblk.  Note that
X strings in perl are not null-terminated and may
X contain the transparent data needed for a
X parblk.  The $binary string may be written to
X disk, sent to a pipe, etc.  Returns undef if
X $vmId is not a valid Id.
.xx

.IP "$newVmId = &Vm_Clone($vmId [,$flags);"
Creates a copy of the Vm object identified by
$vmId.  $flags is the same as for &Vm_New (below)
and defaults to P_CONT.   This subroutine is nice
when writing GUI applications in taeperl where you want
multiple instances of a panel: every time you create
the panel, clone a copy of the target Vm object to
pass to Wpt_NewPanel.

.IP "$count = &Vm_Count($vmId, $name);"
Returns the vector count of the variable.
Returns undef if the variable cannot be found. 

.IP "$bool = &Vm_Event ($vmId $name);"
Returns 1 if the variable is event-generating, 0 otherwise.
Returns undef if the variable does not exist.

.IP "$bool = &Vm_Exists ($vmId [,$name]);"
Returns true if the variable exists, false otherwise.
If $name is omitted, then Vm_Exists returns true
if the $vmId is a valid Vm object. 
This subroutine is convenient when working with Vm objects
created with mode P_ABORT: you may verify that
a variable exists before attempting to access it with
&Vm_Get;  without such a check, you would abort
on the Vm_Get call.

.IP "$code = &Vm_Free($vmId);"
The returned code is always true; calls to Vm_Free typically
ignore the return code.

.IP "[@$]value = &Vm_Get($vmId, $name);"
Returns the value of the variable.  If the value is
a vector, then Vm_Get returns a vector.  If the variable
vector count is less than one ("null value") or if the variable
does not exist, then perl's 'undef' (undefined value)
is returned for scalar context and an empty vector for
vector context.

If a variable has a count greater than one and you
specify a scalar for the return value, you receive the first
element of the vector.

With Vm_Get, it is not possible to distinguish
between a non-existent variable and a variable
with a null value;   when such a distinction is
required, use Vm_Exists first.

If the $vmId is not the ID of a valid Vm object,
taeperl aborts with a diagnostic.  You may
determine the validity of a Vm Id 
with Vm_Exists.

.IP "@valids = &Vm_GetValidString($vmId, $name);"
Returns a vector of valid strings ("constraints")
for the named variable.  The variable must be 
of type string.  Returns an empty vector if the
variable does not exist or if the variable is not
a string.

.ig xx
X .IP "$vmId = &Vm_Make($asPerlVm);"
X (This subroutine is experimental.) 
X Returns the Vm Id of a Vm object constructed from the
X parblk passed as argument.  A parblk may be constructed
X by Vm_AsPerl. 
.xx

.IP "@names = &Vm_Names($vmId [,$name]);"
With one argument, returns a vector of symbol names within the
Vm object.  If $name is present, returns a vector of names
that are qualifiers of the named variable.   This subroutine
is typically used to iterate over (a) the variables in a
Vm object or (b) the qualifiers of a variable;  see "EXAMPLES" below.

.IP "$vmId = &Vm_New([$flags, $name, $value, ...]);"
If $flags is omitted, P_CONT is assumed.  When
using the flags argument, you may code the P_CONT
and P_ABORT constants as subroutine calls, e.g.,
"&Vm_New(&P_ABORT);".  P_CONT means that errors
in Vm will continue without an error message;
P_ABORT generates an automatic abort when errors occur
in manipulating the Vm object.

The flags argument may be followed by name/value
pairs; for each pair, a symbol is created in the
Vm object, either a string or an integer
depending upon whether the form of the value.  As
with Vm_Set, it is not possible to create a Real
symbol in this way.  Rather than passing specific
name/value pairs, you may pass an associative
array which, in an argument context, becomes a
list of name/value pairs.   Example:

.nf
\fB
    $a{"x"} = 100;
    $a{"y"} = "a string";
    $a{"z"} = 500;
    $vmId = &Vm_New (&P_CONT, %a);  # vm gets symbols x, y, and z
\fP
.fi


.IP "$code = &Vm_Set($vmId, $name, $value, ...);"
Sets or creates the value of a variable within the Vm
object.  If the variable does not exist, it is
created and the type (integer or string) of the
variable is inferred by the apparent type of the
first $value.   If your value is a string of the form
"99999", you may force Vm_Set to create an
integer Vm variable by adding zero to the value, i.e.,  0+$value.

To set the values of a vector, you may pass any number
of $value arguments.  You may also pass a vector as
an array argument, e.g., "@vectorValues".

To create real variables, you must use Vm_SetReal.
You may update real variables with Vm_Set or Vm_SetReal.

If you pass no values, Vm_Set creates a string
variable with a null value.  If you want to force
the type of your null variable,  
use Vm_SetIntg, Vm_SetReal, or Vm_SetString.

.IP "$code = &Vm_FreeVar ($vmId, $name);"
Removes a variable from a Vm object.  The variable
name may be qualified (an enhancement to the 
C-callable Vm_FreeVar).  The returned value is
always true and calls to Vm_FreeVar typically ignore
the return code.

.IP "$code = &Vm_SetIntg ($vmId, $name, $value,...);"
Updates or creates an integer value.  As with Vm_Set, there
may be several $value arguments or a vector of values.

If the variable already exists, it will be updated with
the new value.  If the type is other than integer, then
taeperl attempts to convert the values to the proper type;
in this conversion, a string that is not numeric
will become a zero.

.IP "$code = &Vm_SetString ($vmId, $name, $value,...);"
Updates or creates a string value.  As with Vm_Set, there
may be several $value arguments or a vector of values.

If the variable already exists, it will be updated with
the new value.  If the type is other than string, then
taeperl converts the numeric values to strings.

.IP "$code = &Vm_SetReal($vmId, $name, $value,...);"
Updates or creates a real value.  As with Vm_Set, there
may be several $value arguments or a vector of values.

If the variable already exists, it will be updated with
the new value.  If the type is other than real, then
taeperl attempts to convert the values to the proper type.

.IP "$code = &Vm_SetValidIntg($vmId, $name, $count, $low(s), $high(s));"
$count  is the number of ranges.  $low consists of one element
for each $count; $high is similar.

.IP "$code = &Vm_SetValidReal($vmId, $name, $count, $lo, $hi);"
Similar to Vm_SetValidIntg.

.IP "$code = &Vm_SetValidString ($vmId, $name, $count, $string, ...);"
Each $string is used as a valid ("constraint").  $count is
ignored if passed as a zero (and the number of
strings in the calling sequence is used as the
count).  As with all perl calls, you may pass an
array rather than multiple $string arguments.

.IP "$type = &Vm_Type($vmId, $name);"
Returns the type of the variable; 
returns undef if the variable does not exist.
The returned type may be compared (numerically) against
&V_INTEGER, &V_REAL, or &V_STRING.

.SS Wpt Subroutines

See \fITAE Plus Programmer's Manual\fP
for a description of TAE's Window Programming 
Tools (Wpt) function library.    
The following subroutines enable access to Wpt for
the purpose of writing interactive TAE Plus applications in taeperl.

.IP "$code = &Wpt_AddEvent($eventSource, $eventMask);"

.IP "$timerId = &Wpt_AddTimer ($interval, $repeat, $userContext);"

.IP "$code = &Wpt_BeginWait($panelId);"

.IP "$bool = &Wpt_CheckEvent;"
Returns true if event(s) are waiting.

.IP "$code = &Wpt_CloseDisplay($displayId);"

.IP "$code = &Wpt_CloseItems($panelId);"

.IP "&Wpt_EnableInterrupt($flag);"

.IP "$code = &Wpt_EndWait($panelId);"

.ig xx
X .IP "$string = &Wpt_Errno;"
X Returns the error string describing with the most recent Wpt error. 
.xx
.IP "$code = &Wpt_GetItemSensitivity($panelId, $parmName);"

.ig xx
X .IP "$code = &Wpt_GetPanelState($panelId);"
X Not supported.
.xx
.IP "$code = &Wpt_Init([$display]);"
If the argument is omitted, the environment variable DISPLAY
is used.  If the $display string is empty, then 
DISPLAY is also used.

.IP "$code = &Wpt_ItemErase($panelId, $parmName);"

.IP "$bool = &Wpt_MissingVal($dataVm, $message);"
Returns true if any of the Vm symbols is missing a value.

.ig xx
.IP "$code = &Wpt_Msleep ($milliseconds);"
Sleeps for the number of milliseconds indicated. 
This uses the usleep system service and is not available
on all systems.
.xx
.IP "$itemId = &Wpt_NewItem($panelId, $parmName, $targetId, $viewId);"

.IP "$panelId = &Wpt_NewPanel($dummy, $targetId, $viewId,  "

.IP "$parentWindow, $context, $flags);"
Returns the panelId as function value.  The $flags argument may
be coded using &WPT_CHILD, &WPT_INVISIBLE, &WPT_VISIBLE,
&WPT_FASTICONIC, &WPT_ICONIC, &WPT_PREFERRED, or &WPT_MODAL.

.IP "($eventType, @eventData) = &Wpt_NextEvent;"
See "EVENT LOOPS" below for suggestions
on how to use Wpt_NextEvent.
$eventType is a string equal to 
"WPT_PARM_EVENT", "WPT_FILE_EVENT",
"WPT_WINDOW_EVENT", or "WPT_TIMEOUT_EVENT".
@eventData consists of additional data depending
upon the event type:

WPT_PARM_EVENT:
($panelName, $itemName, $itemValue, $panelId,
$dataVm, $windowId, $userContext, $xEvent).
$panelName is the panel name and $itemName is the
item name; $itemValue is a perl scalar value containing (a) the
first element of the item's current value or (b)
undef if the item has no current value;  $panelId
is the Id of the panel.  $dataVm is the Vm object
Id;  $windowId is the window Id of the panel;
$userContext is the context argument from the
panel's Wpt_NewPanel creation call;  $xEvent is
one perl value containing the XEvent structure
that generated the event--to be useful, $xEvent
must be exploded with perl's unpack function.

The $itemValue element is a convenience; if you 
need the full value of the item, then use the
Vm subroutines and the $dataVm value.

WPT_FILE_EVENT:
($fileSource, $fileEventType).   $fileSource is the
file descriptor of the event and $fileEventType is
the read/write/exception indicator.

WPT_WINDOW_EVENT:
($windowId, $xEvent).  $windowId is the window Id that generated
the event and $xEvent is the XEvent structure (which must
be unpacked to be useful).

WPT_TIMEOUT_EVENT:
This event type has no associated parameters.

WPT_TIMER_EVENT:
($timerId, $repeat, $interval, $userContext).
The $timerId is the Id of the timer assigned by Wpt_AddTimer.
The other values are from the Wpt_AddTimer arguments.


.IP "$code = &Wpt_PanelErase($panelId);"

.IP "$code = &Wpt_PanelMessage($panelId, $message);"

.IP "$code = &Wpt_PanelReset($panelId);"

.IP "$windowId = &Wpt_PanelTopWindow($panelId);"

.IP "$widgetId = &Wpt_PanelWidgetId($panelId);"

.IP "$windowId = &Wpt_PanelWindow($panelId);"

.IP "$code = &Wpt_ParmReject($panelId, $parmName, $message);"

.IP "$code = &Wpt_ParmUpdate($panelId, $parmName);"

.IP "$bool = &Wpt_Pending;"

.ig xx
X .IP "$code = &Wpt_QueryItem($panelId, $parmName);"
X Not supported.
.xx
.IP "$code = &Wpt_RemoveEvent($eventId);"

.IP "$code = &Wpt_RemoveTimer ($timerId);"

.IP "$code = &Wpt_ScriptInit ($socketName);"
Note that an application written in taeperl may be
scripted, i.e., play the role of an Application under
Test.  

.IP "$code = &Wpt_SetIntg($panelId, $parmName, $value);"

.IP "$code = &Wpt_SetItemSensitivity($panelId, $parmName, $sensitive);"

.IP "$code = &Wpt_SetNoValue($panelId, $parmName);"

.IP "$code = &Wpt_SetPanelState($panelId, $state);"

.IP "&Wpt_Rehearse ($interval, $order, $cycle);"

.IP "$code = &Wpt_SetReal($panelId, $parmName, $value);"

.IP "$code = &Wpt_SetString($panelId, $parmName, $value);"

.IP "$code = &Wpt_SetTimeOut($timeInterval);"

.IP "$code = &Wpt_ViewUpdate($panelId, $parmName, $viewId, $viewParm);"

.SH EVENT LOOPS  (UNSUPPORTED)
.IP

.LP
The (unsupported) Wpt_NextEvent function delivers more event data than most
taeperl programs need.  The following simple event loop is
recommended for programs that only care about
WPT_PARM_EVENTs:

.nf
\fB
while (1)
    {
    ($eventType, $panelName, $itemName, $itemValue, $panelId, $dataVm)
        = &Wpt_NextEvent;
    next unless $eventType eq "WPT_PARM_EVENT";
    eval "&${panelName}_${itemName} (\\$itemValue, \\$panelId, \\$dataVm);";
    if ($@)
        {print "cannot find event handler ${panelName}_${itemName}\\n";
    }

\fP
.fi

The eval statement calls a subroutine whose name
is constructed dynamically from the panel name
and the name of the item generating the event.
Using this event loop, you must write an event
handler subroutine for each event-generating item
on each of the application's panels.  The
following example is an event handler for item
"fileName" on panel "main":

.nf
\fB
sub main_fileName
    {
    local($itemValue, $panelId, $dataVm) = @_;
    print "item file on panel main: value = $itemValue\\n";
    }
\fP
.fi

The file $TAEPERLLIB/WptSubs.pl contains a
subroutine, Wpt_EventLoop, that implements a more
sophisticated event loop, enabling access to all
event types.  Writing WPT_PARM_EVENT event
handlers for use with Wpt_EventLoop is the same
as with the simple event loop above.

There are many other techniques for dispatching events.   
For example, you could have a perl package for each
panel and dispatch to subroutines within the package;
in this case the eval statement above would appear:

.nf
\fB

    eval "&${panelName}'${itemName} (\\$itemValue, \\$panelId, \\$dataVm);";

\fP
.fi

.SH Writing Simple GUI Programs
.IP

.LP
The file $TAEPERLLIB/WptSubs.pl contains a subroutine, &Wpt_Program,
that initializes Wpt, reads the resource file,
extracts Vm objects, creates initial panels, and
enters the event loop subroutine &Wpt_EventLoop.
With Wpt_Program, you may quickly write simple
GUI applications.

The first argument to &Wpt_Program is the name of
the resource file; subsequent arguments are the
names of the initial panels to be created.  If
you need to extract additional Vm objects from the
resource file, Wpt_Program creates a global
variable, $Co, which is the collection Id of the
resource file.  

Wpt_Program also creates global
variables for the target and view Vm of each
initial panel.  The target and view variables
are named according to the panel name, e.g.,
"MainTarget", and "MainView" for the panel named
"Main".   The panel Id is also available as a global
variable, e.g., "MainId" for the panel named "Main".
(These variables are "dynamically named" using eval.)

See "Simple Application using Wpt_Program" below
for an example using Wpt_Program.

.SH EXAMPLES (UNSUPPORTED)
.IP

.LP
The examples in this section use unsupported
taeperl subroutines scripts whose calling sequence is
subject to change in future releases of TAE
Plus.

.SS "widgetTree.pl"
.sp
The source for this example is provided in $TAEPERLLIB/widgetTree.pl.
This script prints the current widget tree of a TAE Plus
application.  Invoke it as follows:

.in +3
 taeperl $TAEPERLLIB/widgetTree.pl socketName [panelName]
.in -3

In order for widgetTree to work, the application must already
be running and must have called
Wpt_ScriptInit(3) during initialization.  The
socketName on the widgeTree command line must
match the name passed to Wpt_ScriptInit.

For each widget, widgetTree prints the name,
the class, the widget address, and the
widget window Id.  

If panelName is present, widgetTree displays the
current widget tree for that panel.  If panelName
is omitted, then widgetTree displays the tree for
each extant TAE Plus panel.

.SS "taeTrace.pl"
.sp
The source for this example is provided in $TAEPERLLIB/taeTrace.pl.
This script provides an event trace on standard
output for TAE Plus user events generated in
a TAE Plus application process.  Invoke it as follows:

.in +3
taeperl $TAEPERLLIB/taeTrace.pl socketName 
.in -3

In order for taeTrace to work, the application
must already be running and must have called
Wpt_ScriptInit(3) during initialization.  The
socketName in the taeTrace command line must
match the name passed to Wpt_ScriptInit.

On a heavily loaded system it is possible to miss an event
because the taeTrace logic is "request next event, wait
for response, request next event,...".  There is a small 
window where an event could occur without an active wait request
from taeTrace.

.LP

.bp
.SS "Simple Application in taeperl" 
.sp
The following script uses unsupported subroutines
to implement a simple graphical application.

.nf
\fB
#       simpleApp.pl
#
#       TAE Plus application written in taeperl.
#       The window has two buttons: "quit" and
#       "run". To execute: % taeperl simpleApp.pl
#
&Wpt_Init || die "cannot connect to X server"; 
$co = &Co_New;
&Co_ReadFile ($co, 'taePlusResourceFile.res',  0);

# create main panel 
$tar = &Co_Find ($co, "demo_t") 
    || die "cannot find demo panel";
$vu  = &Co_Find ($co, "demo_v") 
    || die "cannot find demo panel";
$mainId = &Wpt_NewPanel (0, $tar, $vu, 0, 0, 0) 
    || die "cannot create new panel";

#     main event loop

while (1) 
    {
    ($eventType, $panelName, $itemName, $itemValue, $panelId) 
	= &Wpt_NextEvent;
    next unless $eventType eq "WPT_PARM_EVENT"; 
    eval "&${panelName}_${itemName} (\\$itemValue, \\$panelId, \\$dataVm);";
    if ($@)
        {print "no event handler for panel $panelName, item $parmName\\n";}
    }

#
#   event handler for button "quit" on panel "demo";
#   called from event loop above.
#
sub demo_quit 
    {
    print "you hit the quit button\\n";
    exit;
    }
#
#   event handler for button "run" on panel "demo"
#   called from event loop above.
#
sub demo_run
    {
    print "you hit the run button\\n";
    }

\fP
.fi

.bp
.SS "Simple Application using Wpt_Program" 
.sp
This is the same program as above, but the
Wpt_Program subroutine is used, which performs
the tedious initialization for you.  Wpt_Program
also calls the event loop, Wpt_EventLoop. 
This example uses unsupported subroutines.

.nf
\fB


#       simpleApp.pl
#
#       TAE Plus application written in taeperl.
#       The window has two buttons: "quit" and
#       "run".    To execute:
#
#	    % taeperl simpleApp.pl
#

require 'WptSubs.pl';		# $TAEPERLLIB must be set to get this
&Wpt_Program ('taePlusResourceFile.res', 'demo');   # init plus event loop

#
#   Note: the panel Id is now available as $demoId,
#   the target Vm as $demoTarget and the view Vm as
#   $demoView.  &Wpt_Program establishes these variables
#   for you.
#

#
#   event handler for button "quit" on panel "demo";
#   called from event loop above.
#
sub demo_quit 
    {
    print "you hit the quit button\\n";
    exit;
    }
#
#   event handler for button "run" on panel "demo"
#   called from event loop above.
#
sub demo_run
    {
    print "you hit the run button\\n";
    }

\fP
.fi



.bp
.SS "Resource File Utility" 
.sp
This example uses unsupported taeperl subroutines
to generate a report from a TAE Plus resource
file.   The report shows the title of each pulldown 
presentation item.

.nf
\fB


#
#       reslist.pl
#
#
#      taeperl script to display names and titles 
#      of all pulldown menus. Shell command: 
#
#          % taeperl reslist.pl taePlusResourceFileName
#
$co = &Co_New;
&Co_ReadFile ($co, $ARGV[0],  0);      # read resource file
foreach $vmName (sort &Co_Names($co)) 
    {
    next unless $vmName =~ /_v$/;      # look at views only
    $panel = $vmName;                  # panel name == view name
    print "$panel:\\n";
    $vuVm = &Co_Find ($co, $vmName);    # find view vm
    foreach $parmName (sort &Vm_Names($vuVm))  # for each item in view
       {
       $type = &Vm_Get ($vuVm, "${parmName}._type");
       next unless ($type eq "pulldown");
       $title = &Vm_Get ($vuVm, $parmName);
       print "\\t$parmName: $type, '$title'\\n"; 
       }
    }


\fP
.fi

.SH REGISTERING APPLICATION FUNCTIONS (UNSUPPORTED)
.IP

.LP
The function registration capability in this 
section is unsupported and subject to change.

Application programmers may "register" their own
functions for access from scripts.  Sophisticated
testing often requires such functions, for example,
a script might need an AUT function "wait for
image to complete loading" before proceeding with
scripted analysis of the image.  The function registration
mechanism is described in \fITAE_ScriptUnsup(3)\fP.

By default, TAE Plus provides many powerful
script-callable library functions in AUTs.  In
addition to the basic "simulated user input",
there are functions for obtaining window IDs and
target Vm objects associated with selected
panels.  A script may also obtain (and dump) the
widget tree associated with an AUT or set
resources into any AUT-resident widget.
\fIautsubs(3)\fP describes how to access the standard
script-callable AUT functions.



.so man1/../COPYRIGHT

The terms under which perl is distributed with
TAE Plus, and the terms under which recipients
may further distribute perl, are described in the
Gnu General Public License.  There is a copy of
this license in the file
$TAE/taeperl/perl/Copying.  If you intend to
distribute perl, you must comply with this
license.  Neither perl's authors nor the Free
Software Foundation, Inc. are responsible for
bugs introduced as a result of Century's
extensions and do not offer any warranty
whatsoever on perl or Century's extensions.

.so man1/../AUTHOR

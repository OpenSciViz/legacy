#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# taelearn.pl Version: @(#)taelearn.pl	33.1 8/26/94 18:24:27
#****************************************************************************
#
#  taelearn.pl >>> Turn learn mode on/off
#
#
#	Usage:
# 		taeperl taelearn.pl socketName [filename]
#	    $1 = socketName of application process
#	    $2 = learn file name to turn on learn mode 
#
#	    If filename present, turn on learn mode, dumping learned events
#	    to file.  If filename absent, turn off learn mode.
#
#
#      CHANGE LOG:
#
#  29-mar-93   Copied from palm's directory...swd
#
#***************************************************************************/

require 'autsubs.pl';

$socketName = $ARGV[0];
$learnFileName = $ARGV[1];

if (!$socketName)
    {die "usage: 'taeperl taelearn.pl socketName [learnFileName]'";}

$skt = &Aut'Connect($socketName);	# dies if no connection
$resp = &Aut'sendReceive ($skt, 
    "function", "TAE_setLearn", 
    "learnFile", $learnFileName);
&Aut'printError ($resp);
&Aut'Close($skt);

#############################################################################
#
#      Copyright (c) 1993, 1994
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# 	@(#)uilGen.pl	33.1 8/26/94
#
#	Generate UIL and supporting C code from TAE Plus resource file
#
#	$ARGV[0] = (optional) code style - "basic" <default> or "enhanced"
#	   (following arg positions decrement by one if ARGV[0] omitted)
#	$ARGV[1] = input resource file name
#	$ARGV[2] = output uil file name
#	$ARGV[3] = output C file name
#

#############################################################################
#  CHANGE LOG:
# 00-mid-92	Original design and implementation...palm
# 00-sum-93	Expansion and refinements...chc,palm,kbs
# 06-aug-93	PR-2100 : Implement basic Mrm code generation and condition
#			  code generation style on "enhanced" or "basic"
#			  passed as ARGV[0]. Rearrange ARGV list to accomodate
#			  new arg, and make symbolic...chc
#			  Condition output of #defines in Imakefile on
#			  codeStye and DDO/Color logger use...chc
#		PR-2105 : Disable panel "default item" handling pending
#			  further investigation...chc
# 10-aug-93	PR-2100 : Changed all main_window_widget references
#			  to ContainerWidget. Added panel icon name, functions,
#			  and decorations to top level resource constants..chc
# 11-aug-93	PR-2097 : Expand environment vars in panel icon filename
#			  for basic mode (pushed env var expansion into new
#			  sub resolveFilePathname). For enhanced mode,
#			  expansion is deferred to run time.
#			  Add code to createNewPanel for reading panel icon
#			  bitmap files...chc
# 13-aug-93	PR-2106 : Fix file selection box callback names, and add
#			  XmFileSelectionDoSearch call to create callback..chc
# 13-aug-93	PR-2162 : Add APP_CINCLUDES = $(ENHANCED_UIL_INCLUDES) to
#			  generate Imakefile to fix makefile problem...chc
# 13-aug-93	PR-2162 : Make APP_CINCLUDES conditional on enhanced code style
#			  ...chc
# 13-aug-93	PR-2162 : Removed APP_CINCLUDES since TAEmake.tmpl now sets
#         	          XTAE_INCLUDES...kbs
# 16-aug-93	PR-2182 : Set $UilBasename and use it instead of $ProgBasename
#         	          wherever .uil or .uid file is referenced...kbs
# 17-aug-93	PR-2106	: Added XmNdirMask resource to filesel pres type,
#			  generate Mrm code frag to handle XmFILE_ANY_TYPE
#			  value for XmNfileTypeMask (UIL bug), and
#			  added expansion of env vbls in initialfilter.
#			: deleted font height references and XmNvisibelItems.
#			  Widget should size list properly based on widget size.
#		PR-2083 : Added callbacks to file selection box buttons
# 		PR-2100	: Changed $res2uil to res2uil in all error messages.
#			...chc
# 26-aug-93	PR-2102 : Add font and color handling for menubar pres type
#			  'parent','Panel Font/fg/bg', etc. Minor cleanup of
#			  menubar UIL indentation...chc
#		PR-2106 : Change handling of XmNfileTypeMask to use a
#			  UIL identifier 'XM_FILE_ANY_TYPE : 3' in place of
#			  using XtSetValue in the filesel create callback...chc
# 27-aug-93    	          Change '!' comment flag to '#' for comments moved 
#         	          from UIL to perl code...cc
#	       PR-2092 : Eliminate explicit setting of XmNnavigiationType  for
#			 XmNtraversal = false. Add F10 fix from menubarRt.cc.
#			 ...chc
# 27-aug-93     PR-2096: Change icon implementation to XmForm. Add label to
#			 special handling in parseColor.
# 30-aug-93	PR-2220: Remove dependency on ctime...chc
#		PR-2092: change $navigation assignment from comound_string to
#			 string in panel_Presentation. Remove 'chop' from
#			 time processing for C, UIL time stamps. 
#		PR-2194: Add logic to detect missing _directory object, and to
#			 use first panel encountered as initial panel when no
#			 initial panels are defined.
#			-change wording of error message for missing sboption,
#			 selpolicy in Textlist pres type.
#			 (per 8/28 test report L.Neve)...chc
# 31-aug-93	PR-2220: cosmetic fix-added newline to UIL header. Replaced
#			 $date with $time in Imakefile header.
#		PR-2103, Finish conversion of icon pres type to XmForm. Added
#		PR-2096	 left/right filler and tweaked attachments, shadow,
#			 and highlight thickness to handle icons with and
#			 without labels.
#		PR-2106: Add default case for file select box buttons with no
#			 title specified.
#			 ...chc
#  1-sep-93	PR-2094: Checkpoint changes to generateCallback and 
#			 panel_Presentation. Now supports all connection
#			 states for icons and pushbuttons EXCEPT for preferred.
#			 Implementation is not complete...chc
#  1-sep-93	PR-2255: Report unsupported DDO features such as input_enabled
#         	         and 2D Mover. Revised error messages using $ERR,
#         	         $WARN, $INFO. Some cosmetic changes...kbs
#		PR-2194: Some res files have initial panels set to ""...kbs
#  2-sep-93	PR-2256: Treat taehelp like help cascade, but warn user of 
#         	         this substitution; cosmetic err msg changes;
#          	         added sub TAEhelp for button, icon, filesel,msg;
#               PR-2274: Revise Co_ReadFile handling to catch resfile with
#          	         obsolete format (v5.1 or earlier); handle icon pres
#          	         with file that doesn't exist (use integers)...kbs
#          	         better err handling if pres sub fails...palm,kbs
#  3-sep-93	PR-2103: Fix icon label fg/bg...cc,kbs
#  7-sep-93	PR-2255: Revise translateEnum; cosmetic msg changes...kbs
#  9-sep-93	PR-2096: Change text, pageedit & textdisp container widgets from
#			 XmRowColumn to XmForm.
#			 Change textlist container widget from XmBulletinBoard
#			 to XmForm.
#			 Fix keyin callback procedure name, add value changed
#			 callback...chc
#  9-sep-93		 Fix shadow/border on keyin text entry area...chc
# 10-sep-93	PR-2146  Allow textdisp and pageedit to handle lines greater
#			 than 132 characters. Does not handle special characters
#			 correctly (\t, \n, etc). NOTE: taewb truncates lines
#			 at 132 characters...chc
# 10-sep-93	PR-2098  Delete commenting of code fragments in
#			 generateCallbacks.
#			 Minor cleanup of icon, adding XmN*Offset resources.
#			 ...chc
# 10-sep-93	PR-2094	 Use XIconifyWindow, XWithdrawWindow, XMapWindow for
#			 Iconic, Invisible and Visible state transitions
#			 instead of XtMapWidget with resource manipulation...chc
# 10-sep-93	PR-2102  Add entry callback to pulldown of each cascade. Fix
#			 sensitivity for pushbutton, checkbox, etc...chc
# 13-sep-93	PR-none  Change callback suffix for scroller and radio;
#         	         removed all "_cb" from callback suffixes;
#          	         fix pulldown choice alignment (and defaults);
#          	         fix option menu alignment defaults...kbs
# 13-sep-93	PR-2294  Added accelerators/mnemonics for MenuEntryPushbutton
#          	         a la MenuEntryCheckbox (both are leaf nodes)...kbs
# 16-sep-93	PR-2314  Changed MenuEntryCheckbox and MenuEntryCascadeArgs
#          	         to support XmNset for checkboxes in menubar...kbs
# 16-sep-93	PR-2313  Added parseAccelerator for more flexible conversion
#         	         of acceleratorText to accelerators; also cosmetic
#         	         alignment of menubar UIL...kbs
# 18-sep-93	PR-none	 Change call_data argument type from XtPointer to
#			 Xm<widgetClass>Callbackstruct in generated code
#			 for callbacks...chc
# 20-sep-93	PR-none  Make all callbacks uniform in declaration of
#                        call_data; always print callback reason...kbs
# 23-sep-93	PR-2337  Strange link problem related to wraparound line in
#			 generated Imakefile after the word "small"...kbs
# 28-sep-93	PR-2350	 Add 'if' test to generateCallback to prevent reference
#			 to nil pointer...chc
# 03-nov-93	PR-2455  (HP Port) Imakefile can't have tab in assignment...kbs
# 17-nov-93	PR-2480  (HP Port) Extend color inheritance in parseColor; 
#          	         icon's label colors set to item bg/fg, not mom bg/fg;
#                        DO check ColorVarAssoc before adding new entry to
#                        avoid clobbering old value (icon label problem); add
#                        XmNleftOffset, XmNrightOffset for icon$label...kbs,chc
#                        don't set XmNselectColor for checkbox...kbs
# 17-nov-93     PR-2430  (HP Port) Fix color inherit. for option/pulldown menu;
#                        fix right side of option menu...kbs
# 22-dec-93	PR-2490  If .res contains XLFD, generate one-up varname...kbs
# 22-dec-93	PR-2535  Map font alias to XLFD using added XLFD_TABLE which
#                        is based on $TAEWPT/fontalias.h...kbs
# 07-feb-94	PR-2614  (SGI Port) Imakefile requires tab in rules lines...kbs
# 19-feb-94	PR-2623  Map Lucida font aliases on non Solaris platforms
#			 using XLFD_NON_OW_TABLE...cew
#			 Minor fixups with XLFD_TABLE.  charB08 not expanded
#			 correctly.  decw$ fonts need '\' before $....cew
# 05-may-94     PR-2713  Removed "#include <stdlib.h>" since Concurrent does
#                        not have this and Sun4 doesn't need it...kbs
# 05-may-94     PR-2714  Use DiscPict.uil rather than DiscretePictures.uil for
#                        Concurrent due to 14 character filename limit...kbs
# 17-jun-94	PR-2783  $MrmInit{"widgetname"} must match assumptions in
#         	         $TAEXTAE/mrm.pl; DiscPict.uil is now linked to
#          	         DiscretePictures.uil to avoid upward compatibility
#         	         problems on most platforms...kbs
# 15-jul-94	PR-2787  Fix $skip computation for textdisp and pageedit to
#         	         avoid problem with 48-character lines which resulted
#          	         in XmNvalue compilation problem...kbs,chc
# 21-dec-94     PR-2281  Revise calculation of labelOffset and textOffset for
#                        text_Presentation...kbs,cew
# 22-dec-94     PR-2941  Set more resources to better control radio buttons 
#                        placement (compressed still not working)...kbs,cew

#############################################################################
# ARGUMENTS

$argvStyle = 0;		# $ARGV[0] = Mrm code style ("basic" or "enhanced")
   $codeStyleEnhanced = "enhanced";
   $codeStyleBasic = "basic";

$argvResFile = 1;	# $ARGV[1] = input resource file name
$argvUilFile = 2;       # $ARGV[2] = output uil file name
$argvCFile = 3;		# $ARGV[3] = output C file name

# GLOBALS

$TAEversion="5.3" ;	###### THIS SHOULD CHANGE WITH EACH RELEASE ###

$MotifVersion="v1.1" ;	###### TBD: THIS MAY CHANGE to 1.2

# For constructing standard messages.
$Res2Uil = "res2uil";
$MsgInd  = "        ";		# indentation: one more space than $Res2Uil
$ERR     = "$Res2Uil: ERROR";	# Can't continue or MAJOR feature loss
$WARN    = "$Res2Uil: WARNING"; # unsupported resources/features
$INFO    = "$Res2Uil: INFO";	# minor, doesn't break anything

$FontVarAssoc { "fixed" } = 
    "\tfont_alias_fixed:         font('fixed');" ;
$FontVarAssoc { "variable" } = 
    "\tfont_alias_variable:         font('variable');" ;

$userScript = "uilInsertions.pl";	# user enhancements to std uilGen.pl 
do $userScript if (-r $userScript);	# execute user code if present

# GLOBAL associative array to reference CommonViewArgs (CVA)
# for the current item being processed.

	$CVA{"title"} ;
	$CVA{"font"} ;
	$CVA{"x"} ;
	$CVA{"y"} ;
	$CVA{"width"} ;
	$CVA{"height"} ;
	$CVA{"fg"} ;
	$CVA{"bg"} ;
	$CVA{"border"} ;
	$CVA{"shadow"} ;
	$CVA{"traversal"} ;
	$CVA{"navigation"} ;
	$CVA{"highlight"} ;
	$CVA{"borderColor"}; # not used since TAE assumes this is fg color

# index of next XLFD found in resource file
        $XLFD_INDEX=0;

#------------------- BEGIN XLFD_TABLE
# 		NOTE:
#
# This XLFD_TABLE (X Logical Font Description) is based on $TAEWPT/fontalias.h
# and will need to be updated in that file changes.
# GLOBAL associative array.
#
    $XLFD_TABLE{"ncenB08"} = "-adobe-new century schoolbook-bold-r-normal--8-80-75-75-p-56-iso8859-1" ;
    $XLFD_TABLE{"ncenB10"} = "-adobe-new century schoolbook-bold-r-normal--10-100-75-75-p-66-iso8859-1" ;
    $XLFD_TABLE{"ncenB12"} = "-adobe-new century schoolbook-bold-r-normal--12-120-75-75-p-77-iso8859-1" ;
    $XLFD_TABLE{"ncenB14"} = "-adobe-new century schoolbook-bold-r-normal--14-140-75-75-p-87-iso8859-1" ;
    $XLFD_TABLE{"ncenB18"} = "-adobe-new century schoolbook-bold-r-normal--18-180-75-75-p-113-iso8859-1" ;
    $XLFD_TABLE{"ncenB24"} = "-adobe-new century schoolbook-bold-r-normal--24-240-75-75-p-149-iso8859-1" ;
    $XLFD_TABLE{"ncenBI08"} = "-adobe-new century schoolbook-bold-i-normal--8-80-75-75-p-56-iso8859-1" ;
    $XLFD_TABLE{"ncenBI10"} = "-adobe-new century schoolbook-bold-i-normal--10-100-75-75-p-66-iso8859-1" ;
    $XLFD_TABLE{"ncenBI12"} = "-adobe-new century schoolbook-bold-i-normal--12-120-75-75-p-76-iso8859-1" ;
    $XLFD_TABLE{"ncenBI14"} = "-adobe-new century schoolbook-bold-i-normal--14-140-75-75-p-88-iso8859-1" ;
    $XLFD_TABLE{"ncenBI18"} = "-adobe-new century schoolbook-bold-i-normal--18-180-75-75-p-111-iso8859-1" ;
    $XLFD_TABLE{"ncenBI24"} = "-adobe-new century schoolbook-bold-i-normal--24-240-75-75-p-148-iso8859-1" ;
    $XLFD_TABLE{"ncenI08"} = "-adobe-new century schoolbook-medium-i-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"ncenI10"} = "-adobe-new century schoolbook-medium-i-normal--10-100-75-75-p-60-iso8859-1" ;
    $XLFD_TABLE{"ncenI12"} = "-adobe-new century schoolbook-medium-i-normal--12-120-75-75-p-70-iso8859-1" ;
    $XLFD_TABLE{"ncenI14"} = "-adobe-new century schoolbook-medium-i-normal--14-140-75-75-p-81-iso8859-1" ;
    $XLFD_TABLE{"ncenI18"} = "-adobe-new century schoolbook-medium-i-normal--18-180-75-75-p-104-iso8859-1" ;
    $XLFD_TABLE{"ncenI24"} = "-adobe-new century schoolbook-medium-i-normal--24-240-75-75-p-136-iso8859-1" ;
    $XLFD_TABLE{"ncenR08"} = "-adobe-new century schoolbook-medium-r-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"ncenR10"} = "-adobe-new century schoolbook-medium-r-normal--10-100-75-75-p-60-iso8859-1" ;
    $XLFD_TABLE{"ncenR12"} = "-adobe-new century schoolbook-medium-r-normal--12-120-75-75-p-70-iso8859-1" ;
    $XLFD_TABLE{"ncenR14"} = "-adobe-new century schoolbook-medium-r-normal--14-140-75-75-p-82-iso8859-1" ;
    $XLFD_TABLE{"ncenR18"} = "-adobe-new century schoolbook-medium-r-normal--18-180-75-75-p-103-iso8859-1" ;
    $XLFD_TABLE{"ncenR24"} = "-adobe-new century schoolbook-medium-r-normal--24-240-75-75-p-137-iso8859-1" ;
    $XLFD_TABLE{"courB08"} = "-adobe-courier-bold-r-normal--8-80-75-75-m-50-iso8859-1" ;
    $XLFD_TABLE{"courB10"} = "-adobe-courier-bold-r-normal--10-100-75-75-m-60-iso8859-1" ;
    $XLFD_TABLE{"courB12"} = "-adobe-courier-bold-r-normal--12-120-75-75-m-70-iso8859-1" ;
    $XLFD_TABLE{"courB14"} = "-adobe-courier-bold-r-normal--14-140-75-75-m-90-iso8859-1" ;
    $XLFD_TABLE{"courB18"} = "-adobe-courier-bold-r-normal--18-180-75-75-m-110-iso8859-1" ;
    $XLFD_TABLE{"courB24"} = "-adobe-courier-bold-r-normal--24-240-75-75-m-150-iso8859-1" ;
    $XLFD_TABLE{"courBO08"} = "-adobe-courier-bold-o-normal--8-80-75-75-m-50-iso8859-1" ;
    $XLFD_TABLE{"courBO10"} = "-adobe-courier-bold-o-normal--10-100-75-75-m-60-iso8859-1" ;
    $XLFD_TABLE{"courBO12"} = "-adobe-courier-bold-o-normal--12-120-75-75-m-70-iso8859-1" ;
    $XLFD_TABLE{"courBO14"} = "-adobe-courier-bold-o-normal--14-140-75-75-m-90-iso8859-1" ;
    $XLFD_TABLE{"courBO18"} = "-adobe-courier-bold-o-normal--18-180-75-75-m-110-iso8859-1" ;
    $XLFD_TABLE{"courBO24"} = "-adobe-courier-bold-o-normal--24-240-75-75-m-150-iso8859-1" ;
    $XLFD_TABLE{"courO08"} = "-adobe-courier-medium-o-normal--8-80-75-75-m-50-iso8859-1" ;
    $XLFD_TABLE{"courO10"} = "-adobe-courier-medium-o-normal--10-100-75-75-m-60-iso8859-1" ;
    $XLFD_TABLE{"courO12"} = "-adobe-courier-medium-o-normal--12-120-75-75-m-70-iso8859-1" ;
    $XLFD_TABLE{"courO14"} = "-adobe-courier-medium-o-normal--14-140-75-75-m-90-iso8859-1" ;
    $XLFD_TABLE{"courO18"} = "-adobe-courier-medium-o-normal--18-180-75-75-m-110-iso8859-1" ;
    $XLFD_TABLE{"courO24"} = "-adobe-courier-medium-o-normal--24-240-75-75-m-150-iso8859-1" ;
    $XLFD_TABLE{"courR08"} = "-adobe-courier-medium-r-normal--8-80-75-75-m-50-iso8859-1" ;
    $XLFD_TABLE{"courR10"} = "-adobe-courier-medium-r-normal--10-100-75-75-m-60-iso8859-1" ;
    $XLFD_TABLE{"courR12"} = "-adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1" ;
    $XLFD_TABLE{"courR14"} = "-adobe-courier-medium-r-normal--14-140-75-75-m-90-iso8859-1" ;
    $XLFD_TABLE{"courR18"} = "-adobe-courier-medium-r-normal--18-180-75-75-m-110-iso8859-1" ;
    $XLFD_TABLE{"courR24"} = "-adobe-courier-medium-r-normal--24-240-75-75-m-150-iso8859-1" ;
    $XLFD_TABLE{"helvB08"} = "-adobe-helvetica-bold-r-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"helvB10"} = "-adobe-helvetica-bold-r-normal--10-100-75-75-p-60-iso8859-1" ;
    $XLFD_TABLE{"helvB12"} = "-adobe-helvetica-bold-r-normal--12-120-75-75-p-70-iso8859-1" ;
    $XLFD_TABLE{"helvB14"} = "-adobe-helvetica-bold-r-normal--14-140-75-75-p-82-iso8859-1" ;
    $XLFD_TABLE{"helvB18"} = "-adobe-helvetica-bold-r-normal--18-180-75-75-p-103-iso8859-1" ;
    $XLFD_TABLE{"helvB24"} = "-adobe-helvetica-bold-r-normal--24-240-75-75-p-138-iso8859-1" ;
    $XLFD_TABLE{"helvBO08"} = "-adobe-helvetica-bold-o-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"helvBO10"} = "-adobe-helvetica-bold-o-normal--10-100-75-75-p-60-iso8859-1" ;
    $XLFD_TABLE{"helvBO12"} = "-adobe-helvetica-bold-o-normal--12-120-75-75-p-69-iso8859-1" ;
    $XLFD_TABLE{"helvBO14"} = "-adobe-helvetica-bold-o-normal--14-140-75-75-p-82-iso8859-1" ;
    $XLFD_TABLE{"helvBO18"} = "-adobe-helvetica-bold-o-normal--18-180-75-75-p-104-iso8859-1" ;
    $XLFD_TABLE{"helvBO24"} = "-adobe-helvetica-bold-o-normal--24-240-75-75-p-138-iso8859-1" ;
    $XLFD_TABLE{"helvO08"} = "-adobe-helvetica-medium-o-normal--8-80-75-75-p-47-iso8859-1" ;
    $XLFD_TABLE{"helvO10"} = "-adobe-helvetica-medium-o-normal--10-100-75-75-p-57-iso8859-1" ;
    $XLFD_TABLE{"helvO12"} = "-adobe-helvetica-medium-o-normal--12-120-75-75-p-67-iso8859-1" ;
    $XLFD_TABLE{"helvO14"} = "-adobe-helvetica-medium-o-normal--14-140-75-75-p-78-iso8859-1" ;
    $XLFD_TABLE{"helvO18"} = "-adobe-helvetica-medium-o-normal--18-180-75-75-p-98-iso8859-1" ;
    $XLFD_TABLE{"helvO24"} = "-adobe-helvetica-medium-o-normal--24-240-75-75-p-130-iso8859-1" ;
    $XLFD_TABLE{"helvR08"} = "-adobe-helvetica-medium-r-normal--8-80-75-75-p-46-iso8859-1" ;
    $XLFD_TABLE{"helvR10"} = "-adobe-helvetica-medium-r-normal--10-100-75-75-p-56-iso8859-1" ;
    $XLFD_TABLE{"helvR12"} = "-adobe-helvetica-medium-r-normal--12-120-75-75-p-67-iso8859-1" ;
    $XLFD_TABLE{"helvR14"} = "-adobe-helvetica-medium-r-normal--14-140-75-75-p-77-iso8859-1" ;
    $XLFD_TABLE{"helvR18"} = "-adobe-helvetica-medium-r-normal--18-180-75-75-p-98-iso8859-1" ;
    $XLFD_TABLE{"helvR24"} = "-adobe-helvetica-medium-r-normal--24-240-75-75-p-130-iso8859-1" ;
    $XLFD_TABLE{"timB08"} = "-adobe-times-bold-r-normal--8-80-75-75-p-47-iso8859-1" ;
    $XLFD_TABLE{"timB10"} = "-adobe-times-bold-r-normal--10-100-75-75-p-57-iso8859-1" ;
    $XLFD_TABLE{"timB12"} = "-adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1" ;
    $XLFD_TABLE{"timB14"} = "-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1" ;
    $XLFD_TABLE{"timB18"} = "-adobe-times-bold-r-normal--18-180-75-75-p-99-iso8859-1" ;
    $XLFD_TABLE{"timB24"} = "-adobe-times-bold-r-normal--24-240-75-75-p-132-iso8859-1" ;
    $XLFD_TABLE{"timBI08"} = "-adobe-times-bold-i-normal--8-80-75-75-p-47-iso8859-1" ;
    $XLFD_TABLE{"timBI10"} = "-adobe-times-bold-i-normal--10-100-75-75-p-57-iso8859-1" ;
    $XLFD_TABLE{"timBI12"} = "-adobe-times-bold-i-normal--12-120-75-75-p-68-iso8859-1" ;
    $XLFD_TABLE{"timBI14"} = "-adobe-times-bold-i-normal--14-140-75-75-p-77-iso8859-1" ;
    $XLFD_TABLE{"timBI18"} = "-adobe-times-bold-i-normal--18-180-75-75-p-98-iso8859-1" ;
    $XLFD_TABLE{"timBI24"} = "-adobe-times-bold-i-normal--24-240-75-75-p-128-iso8859-1" ;
    $XLFD_TABLE{"timI08"} = "-adobe-times-medium-i-normal--8-80-75-75-p-42-iso8859-1" ;
    $XLFD_TABLE{"timI10"} = "-adobe-times-medium-i-normal--10-100-75-75-p-52-iso8859-1" ;
    $XLFD_TABLE{"timI12"} = "-adobe-times-medium-i-normal--12-120-75-75-p-63-iso8859-1" ;
    $XLFD_TABLE{"timI14"} = "-adobe-times-medium-i-normal--14-140-75-75-p-73-iso8859-1" ;
    $XLFD_TABLE{"timI18"} = "-adobe-times-medium-i-normal--18-180-75-75-p-94-iso8859-1" ;
    $XLFD_TABLE{"timI24"} = "-adobe-times-medium-i-normal--24-240-75-75-p-125-iso8859-1" ;
    $XLFD_TABLE{"timR08"} = "-adobe-times-medium-r-normal--8-80-75-75-p-44-iso8859-1" ;
    $XLFD_TABLE{"timR10"} = "-adobe-times-medium-r-normal--10-100-75-75-p-54-iso8859-1" ;
    $XLFD_TABLE{"timR12"} = "-adobe-times-medium-r-normal--12-120-75-75-p-64-iso8859-1" ;
    $XLFD_TABLE{"timR14"} = "-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1" ;
    $XLFD_TABLE{"timR18"} = "-adobe-times-medium-r-normal--18-180-75-75-p-94-iso8859-1" ;
    $XLFD_TABLE{"timR24"} = "-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1" ;
    $XLFD_TABLE{"5x8"} = "-misc-fixed-medium-r-normal--8-80-75-75-c-50-iso8859-1" ;
    $XLFD_TABLE{"6x10"} = "-misc-fixed-medium-r-normal--10-100-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"6x12"} = "-misc-fixed-medium-r-semicondensed--12-110-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"6x13"} = "-misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"6x13B"} = "-misc-fixed-bold-r-semicondensed--13-120-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"6x9"} = "-misc-fixed-medium-r-normal--9-90-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"7x13"} = "-misc-fixed-medium-r-normal--13-120-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"7x13B"} = "-misc-fixed-bold-r-normal--13-120-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"7x14"} = "-misc-fixed-medium-r-normal--14-130-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"7x14rk"} = "-misc-fixed-medium-r-normal--14-130-75-75-c-70-jisx0201.1976-0" ;
    $XLFD_TABLE{"8x13"} = "-misc-fixed-medium-r-normal--13-120-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"8x13B"} = "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"8x16"} = "-sony-fixed-medium-r-normal--16-150-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"8x16rk"} = "-sony-fixed-medium-r-normal--16-150-75-75-c-80-jisx0201.1976-0" ;
    $XLFD_TABLE{"9x15"} = "-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-1" ;
    $XLFD_TABLE{"9x15B"} = "-misc-fixed-bold-r-normal--15-140-75-75-c-90-iso8859-1" ;
    $XLFD_TABLE{"10x20"} = "-misc-fixed-medium-r-normal--20-200-75-75-c-100-iso8859-1" ;
    $XLFD_TABLE{"12x24"} = "-sony-fixed-medium-r-normal--24-230-75-75-c-120-iso8859-1" ;
    $XLFD_TABLE{"12x24rk"} = "-sony-fixed-medium-r-normal--24-230-75-75-c-120-jisx0201.1976-0" ;
    $XLFD_TABLE{"charB08"} = "-bitstream-charter-bold-r-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"charB10"} = "-bitstream-charter-bold-r-normal--10-100-75-75-p-63-iso8859-1" ;
    $XLFD_TABLE{"charB12"} = "-bitstream-charter-bold-r-normal--12-120-75-75-p-75-iso8859-1" ;
    $XLFD_TABLE{"charB14"} = "-bitstream-charter-bold-r-normal--15-140-75-75-p-94-iso8859-1" ;
    $XLFD_TABLE{"charB18"} = "-bitstream-charter-bold-r-normal--19-180-75-75-p-119-iso8859-1" ;
    $XLFD_TABLE{"charB24"} = "-bitstream-charter-bold-r-normal--25-240-75-75-p-157-iso8859-1" ;
    $XLFD_TABLE{"charBI08"} = "-bitstream-charter-bold-i-normal--8-80-75-75-p-50-iso8859-1" ;
    $XLFD_TABLE{"charBI10"} = "-bitstream-charter-bold-i-normal--10-100-75-75-p-62-iso8859-1" ;
    $XLFD_TABLE{"charBI12"} = "-bitstream-charter-bold-i-normal--12-120-75-75-p-74-iso8859-1" ;
    $XLFD_TABLE{"charBI14"} = "-bitstream-charter-bold-i-normal--15-140-75-75-p-93-iso8859-1" ;
    $XLFD_TABLE{"charBI18"} = "-bitstream-charter-bold-i-normal--19-180-75-75-p-117-iso8859-1" ;
    $XLFD_TABLE{"charBI24"} = "-bitstream-charter-bold-i-normal--25-240-75-75-p-154-iso8859-1" ;
    $XLFD_TABLE{"charI08"} = "-bitstream-charter-medium-i-normal--8-80-75-75-p-44-iso8859-1" ;
    $XLFD_TABLE{"charI10"} = "-bitstream-charter-medium-i-normal--10-100-75-75-p-55-iso8859-1" ;
    $XLFD_TABLE{"charI12"} = "-bitstream-charter-medium-i-normal--12-120-75-75-p-65-iso8859-1" ;
    $XLFD_TABLE{"charI14"} = "-bitstream-charter-medium-i-normal--15-140-75-75-p-82-iso8859-1" ;
    $XLFD_TABLE{"charI18"} = "-bitstream-charter-medium-i-normal--19-180-75-75-p-103-iso8859-1" ;
    $XLFD_TABLE{"charI24"} = "-bitstream-charter-medium-i-normal--25-240-75-75-p-136-iso8859-1" ;
    $XLFD_TABLE{"charR08"} = "-bitstream-charter-medium-r-normal--8-80-75-75-p-45-iso8859-1" ;
    $XLFD_TABLE{"charR10"} = "-bitstream-charter-medium-r-normal--10-100-75-75-p-56-iso8859-1" ;
    $XLFD_TABLE{"charR12"} = "-bitstream-charter-medium-r-normal--12-120-75-75-p-67-iso8859-1" ;
    $XLFD_TABLE{"charR14"} = "-bitstream-charter-medium-r-normal--15-140-75-75-p-84-iso8859-1" ;
    $XLFD_TABLE{"charR18"} = "-bitstream-charter-medium-r-normal--19-180-75-75-p-106-iso8859-1" ;
    $XLFD_TABLE{"charR24"} = "-bitstream-charter-medium-r-normal--25-240-75-75-p-139-iso8859-1" ;
    $XLFD_TABLE{"clB6x10"} = "-schumacher-clean-bold-r-normal--10-100-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clB6x12"} = "-schumacher-clean-bold-r-normal--12-120-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clB8x10"} = "-schumacher-clean-bold-r-normal--10-100-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB8x12"} = "-schumacher-clean-bold-r-normal--12-120-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB8x13"} = "-schumacher-clean-bold-r-normal--13-130-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB8x14"} = "-schumacher-clean-bold-r-normal--14-140-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB8x16"} = "-schumacher-clean-bold-r-normal--16-160-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB8x8"} = "-schumacher-clean-bold-r-normal--8-80-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clB9x15"} = "-schumacher-clean-bold-r-normal--15-150-75-75-c-90-iso8859-1" ;
    $XLFD_TABLE{"clI6x12"} = "-schumacher-clean-medium-i-normal--12-120-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clI8x8"} = "-schumacher-clean-medium-i-normal--8-80-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR4x6"} = "-schumacher-clean-medium-r-normal--6-60-75-75-c-40-iso8859-1" ;
    $XLFD_TABLE{"clR5x10"} = "-schumacher-clean-medium-r-normal--10-100-75-75-c-50-iso8859-1" ;
    $XLFD_TABLE{"clR5x6"} = "-schumacher-clean-medium-r-normal--6-60-75-75-c-50-iso8859-1" ;
    $XLFD_TABLE{"clR5x8"} = "-schumacher-clean-medium-r-normal--8-80-75-75-c-50-iso8859-1" ;
    $XLFD_TABLE{"clR6x10"} = "-schumacher-clean-medium-r-normal--10-100-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clR6x12"} = "-schumacher-clean-medium-r-normal--12-120-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clR6x13"} = "-schumacher-clean-medium-r-normal--13-130-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clR6x6"} = "-schumacher-clean-medium-r-normal--6-60-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clR6x8"} = "-schumacher-clean-medium-r-normal--8-80-75-75-c-60-iso8859-1" ;
    $XLFD_TABLE{"clR7x10"} = "-schumacher-clean-medium-r-normal--10-100-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"clR7x12"} = "-schumacher-clean-medium-r-normal--12-120-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"clR7x14"} = "-schumacher-clean-medium-r-normal--14-140-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"clR7x8"} = "-schumacher-clean-medium-r-normal--8-80-75-75-c-70-iso8859-1" ;
    $XLFD_TABLE{"clR8x10"} = "-schumacher-clean-medium-r-normal--10-100-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR8x12"} = "-schumacher-clean-medium-r-normal--12-120-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR8x13"} = "-schumacher-clean-medium-r-normal--13-130-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR8x14"} = "-schumacher-clean-medium-r-normal--14-140-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR8x16"} = "-schumacher-clean-medium-r-normal--16-160-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR8x8"} = "-schumacher-clean-medium-r-normal--8-80-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"clR9x15"} = "-schumacher-clean-medium-r-normal--15-150-75-75-c-90-iso8859-1" ;
    $XLFD_TABLE{"deccurs"} = "decw\$cursor" ;
    $XLFD_TABLE{"decsess"} = "decw\$session" ;
    $XLFD_TABLE{"olcursor"} = "-sun-open look cursor-*-*-*-*-12-120-*-*-*-*-*-*" ;
    $XLFD_TABLE{"olgl10"} = "-sun-open look glyph-*-*-*-*-10-100-*-*-*-*-*-*" ;
    $XLFD_TABLE{"olgl12"} = "-sun-open look glyph-*-*-*-*-12-120-*-*-*-*-*-*" ;
    $XLFD_TABLE{"olgl14"} = "-sun-open look glyph-*-*-*-*-14-140-*-*-*-*-*-*" ;
    $XLFD_TABLE{"olgl19"} = "-sun-open look glyph-*-*-*-*-19-190-*-*-*-*-*-*" ;
    $XLFD_TABLE{"symb08"} = "-adobe-symbol-medium-r-normal--8-80-75-75-p-51-adobe-fontspecific" ;
    $XLFD_TABLE{"symb10"} = "-adobe-symbol-medium-r-normal--10-100-75-75-p-61-adobe-fontspecific" ;
    $XLFD_TABLE{"symb12"} = "-adobe-symbol-medium-r-normal--12-120-75-75-p-74-adobe-fontspecific" ;
    $XLFD_TABLE{"symb14"} = "-adobe-symbol-medium-r-normal--14-140-75-75-p-85-adobe-fontspecific" ;
    $XLFD_TABLE{"symb18"} = "-adobe-symbol-medium-r-normal--18-180-75-75-p-107-adobe-fontspecific" ;
    $XLFD_TABLE{"symb24"} = "-adobe-symbol-medium-r-normal--24-240-75-75-p-142-adobe-fontspecific" ;
    $XLFD_TABLE{"tech14"} = "-dec-terminal-medium-r-normal--14-140-75-75-c-80-dec-dectech" ;
    $XLFD_TABLE{"techB14"} = "-dec-terminal-bold-r-normal--14-140-75-75-c-80-dec-dectech" ;
    $XLFD_TABLE{"term14"} = "-dec-terminal-medium-r-normal--14-140-75-75-c-80-iso8859-1" ;
    $XLFD_TABLE{"termB14"} = "-dec-terminal-bold-r-normal--14-140-75-75-c-80-iso8859-1" ;

##------------------- END XLFD_TABLE

#------------------- BEGIN XLFD_NON_OW_TABLE
#
# The following fonts should not be expanded on OpenWindows servers.  Since
# we can't determine the SERVER, we will just not expand these fonts on
# Solaris platforms.
#
    $XLFD_NON_OW_TABLE{"k14"} = "-misc-fixed-medium-r-normal--14-130-75-75-c-70-jisx0201.1976-0" ;
    $XLFD_NON_OW_TABLE{"lubB08"} = "-b&h-lucidabright-demibold-r-normal--8-80-75-75-p-47-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB10"} = "-b&h-lucidabright-demibold-r-normal--10-100-75-75-p-59-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB12"} = "-b&h-lucidabright-demibold-r-normal--12-120-75-75-p-71-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB14"} = "-b&h-lucidabright-demibold-r-normal--14-140-75-75-p-84-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB18"} = "-b&h-lucidabright-demibold-r-normal--18-180-75-75-p-107-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB19"} = "-b&h-lucidabright-demibold-r-normal--19-190-75-75-p-114-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubB24"} = "-b&h-lucidabright-demibold-r-normal--24-240-75-75-p-143-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI08"} = "-b&h-lucidabright-demibold-i-normal--8-80-75-75-p-48-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI10"} = "-b&h-lucidabright-demibold-i-normal--10-100-75-75-p-59-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI12"} = "-b&h-lucidabright-demibold-i-normal--12-120-75-75-p-72-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI14"} = "-b&h-lucidabright-demibold-i-normal--14-140-75-75-p-84-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI18"} = "-b&h-lucidabright-demibold-i-normal--18-180-75-75-p-107-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI19"} = "-b&h-lucidabright-demibold-i-normal--19-190-75-75-p-114-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubBI24"} = "-b&h-lucidabright-demibold-i-normal--24-240-75-75-p-143-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI08"} = "-b&h-lucidabright-medium-i-normal--8-80-75-75-p-45-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI10"} = "-b&h-lucidabright-medium-i-normal--10-100-75-75-p-57-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI12"} = "-b&h-lucidabright-medium-i-normal--12-120-75-75-p-67-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI14"} = "-b&h-lucidabright-medium-i-normal--14-140-75-75-p-80-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI18"} = "-b&h-lucidabright-medium-i-normal--18-180-75-75-p-102-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI19"} = "-b&h-lucidabright-medium-i-normal--19-190-75-75-p-109-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubI24"} = "-b&h-lucidabright-medium-i-normal--24-240-75-75-p-136-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS08"} = "-b&h-lucida-bold-i-normal-sans-8-80-75-75-p-49-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS10"} = "-b&h-lucida-bold-i-normal-sans-10-100-75-75-p-67-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS12"} = "-b&h-lucida-bold-i-normal-sans-12-120-75-75-p-79-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS14"} = "-b&h-lucida-bold-i-normal-sans-14-140-75-75-p-92-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS18"} = "-b&h-lucida-bold-i-normal-sans-18-180-75-75-p-119-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS19"} = "-b&h-lucida-bold-i-normal-sans-19-190-75-75-p-122-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBIS24"} = "-b&h-lucida-bold-i-normal-sans-24-240-75-75-p-151-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR08"} = "-b&h-lucidabright-medium-r-normal--8-80-75-75-p-45-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR10"} = "-b&h-lucidabright-medium-r-normal--10-100-75-75-p-56-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR12"} = "-b&h-lucidabright-medium-r-normal--12-120-75-75-p-68-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR14"} = "-b&h-lucidabright-medium-r-normal--14-140-75-75-p-80-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR18"} = "-b&h-lucidabright-medium-r-normal--18-180-75-75-p-103-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR19"} = "-b&h-lucidabright-medium-r-normal--19-190-75-75-p-109-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lubR24"} = "-b&h-lucidabright-medium-r-normal--24-240-75-75-p-137-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS08"} = "-b&h-lucida-bold-r-normal-sans-8-80-75-75-p-50-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS10"} = "-b&h-lucida-bold-r-normal-sans-10-100-75-75-p-66-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS12"} = "-b&h-lucida-bold-r-normal-sans-12-120-75-75-p-79-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS14"} = "-b&h-lucida-bold-r-normal-sans-14-140-75-75-p-92-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS18"} = "-b&h-lucida-bold-r-normal-sans-18-180-75-75-p-120-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS19"} = "-b&h-lucida-bold-r-normal-sans-19-190-75-75-p-122-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luBS24"} = "-b&h-lucida-bold-r-normal-sans-24-240-75-75-p-152-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS08"} = "-b&h-lucida-medium-i-normal-sans-8-80-75-75-p-45-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS10"} = "-b&h-lucida-medium-i-normal-sans-10-100-75-75-p-59-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS12"} = "-b&h-lucida-medium-i-normal-sans-12-120-75-75-p-71-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS14"} = "-b&h-lucida-medium-i-normal-sans-14-140-75-75-p-82-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS18"} = "-b&h-lucida-medium-i-normal-sans-18-180-75-75-p-105-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS19"} = "-b&h-lucida-medium-i-normal-sans-19-190-75-75-p-108-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luIS24"} = "-b&h-lucida-medium-i-normal-sans-24-240-75-75-p-136-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS08"} = "-b&h-lucida-medium-r-normal-sans-8-80-75-75-p-45-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS10"} = "-b&h-lucida-medium-r-normal-sans-10-100-75-75-p-58-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS12"} = "-b&h-lucida-medium-r-normal-sans-12-120-75-75-p-71-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS14"} = "-b&h-lucida-medium-r-normal-sans-14-140-75-75-p-81-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS18"} = "-b&h-lucida-medium-r-normal-sans-18-180-75-75-p-106-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS19"} = "-b&h-lucida-medium-r-normal-sans-19-190-75-75-p-108-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"luRS24"} = "-b&h-lucida-medium-r-normal-sans-24-240-75-75-p-136-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS08"} = "-b&h-lucidatypewriter-bold-r-normal-sans-8-80-75-75-m-50-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS10"} = "-b&h-lucidatypewriter-bold-r-normal-sans-10-100-75-75-m-60-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS12"} = "-b&h-lucidatypewriter-bold-r-normal-sans-12-120-75-75-m-70-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS14"} = "-b&h-lucidatypewriter-bold-r-normal-sans-14-140-75-75-m-90-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS18"} = "-b&h-lucidatypewriter-bold-r-normal-sans-18-180-75-75-m-110-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS19"} = "-b&h-lucidatypewriter-bold-r-normal-sans-19-190-75-75-m-110-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutBS24"} = "-b&h-lucidatypewriter-bold-r-normal-sans-24-240-75-75-m-140-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS08"} = "-b&h-lucidatypewriter-medium-r-normal-sans-8-80-75-75-m-50-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS10"} = "-b&h-lucidatypewriter-medium-r-normal-sans-10-100-75-75-m-60-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS12"} = "-b&h-lucidatypewriter-medium-r-normal-sans-12-120-75-75-m-70-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS14"} = "-b&h-lucidatypewriter-medium-r-normal-sans-14-140-75-75-m-90-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS18"} = "-b&h-lucidatypewriter-medium-r-normal-sans-18-180-75-75-m-110-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS19"} = "-b&h-lucidatypewriter-medium-r-normal-sans-19-190-75-75-m-110-iso8859-1" ;
    $XLFD_NON_OW_TABLE{"lutRS24"} = "-b&h-lucidatypewriter-medium-r-normal-sans-24-240-75-75-m-140-iso8859-1" ;

##------------------- END XLFD_NON_OW_TABLE

$menuiconCount = 0; # Global ident suffix for menuicon pixmaps

##
## MWM function flag masks for XmNmwmFunctions 
##
$TAE_MWM_FUNC_NONE = '(MWM_FUNC_RESIZE | MWM_FUNC_MOVE |
		   MWM_FUNC_MINIMIZE | MWM_FUNC_MAXIMIZE |
		   MWM_FUNC_CLOSE | MWM_FUNC_ALL)';

$TAE_MWM_FUNC_ALL = '(MWM_FUNC_RESIZE | MWM_FUNC_MOVE |
		   MWM_FUNC_MINIMIZE | MWM_FUNC_MAXIMIZE |
		   MWM_FUNC_CLOSE)';

#$TAE_MWM_FUNC_DEF = '(MWM_FUNC_MOVE | MWM_FUNC_MINIMIZE )';
$TAE_MWM_FUNC_DEF =  '(MWM_FUNC_MAXIMIZE | MWM_FUNC_CLOSE | MWM_FUNC_ALL )';

$TAE_MWM_FUNC_ALL_BUT_RESIZE = '(MWM_FUNC_MOVE | MWM_FUNC_MINIMIZE |
		   MWM_FUNC_MAXIMIZE | MWM_FUNC_CLOSE)';

$TAE_MWM_FUNC_RESIZE = '(MWM_FUNC_RESIZE)';

$TAE_MWM_FUNC_MIN_MOVE_RESIZE='(MWM_FUNC_MOVE | MWM_FUNC_MINIMIZE | MWM_FUNC_RESIZE)';

##
## MWM decoration flag masks for XmNmwmDecorations 
##
$TAE_MWM_DECOR_FRAME = '(MWM_DECOR_BORDER)';

$TAE_MWM_DECOR_RESIZE = '(MWM_DECOR_BORDER | MWM_DECOR_RESIZEH)';

$TAE_MWM_DECOR_TITLEBAR_NO_RESIZE = '(MWM_DECOR_TITLE | MWM_DECOR_MENU |
		    MWM_DECOR_MINIMIZE | MWM_DECOR_MAXIMIZE |
		    MWM_DECOR_BORDER)';

$TAE_MWM_DECOR_TITLEBAR_NO_MAXIMIZE = '(MWM_DECOR_TITLE | MWM_DECOR_MENU |
		    MWM_DECOR_MINIMIZE | MWM_DECOR_RESIZEH |
		    MWM_DECOR_BORDER)';

$TAE_MWM_DECOR_TBAR_MOTIF_BORDER = '(MWM_DECOR_TITLE | MWM_DECOR_MENU |
		    MWM_DECOR_MINIMIZE | MWM_DECOR_MAXIMIZE )';

$TAE_MWM_DECOR_TBAR_NO_MAXIMIZE_BORDER= '(MWM_DECOR_MENU | MWM_DECOR_TITLE |
		    MWM_DECOR_MINIMIZE )';

$TAE_MWM_DECOR_ALL= '(MWM_DECOR_ALL)';

$TAE_MWM_DECOR_DEF='(MWM_DECOR_MENU | MWM_DECOR_TITLE |
		    MWM_DECOR_BORDER | MWM_DECOR_MINIMIZE )';

$TAE_MWM_DECOR_NONE='(MWM_DECOR_MENU | MWM_DECOR_BORDER |
		   MWM_DECOR_TITLE | MWM_DECOR_MINIMIZE |
		   MWM_DECOR_MAXIMIZE | MWM_DECOR_RESIZEH |
		   MWM_DECOR_ALL)';

##
## GLOBAL tables to store mwm function and decoration masks
##
   ### FUNCTION MASKS

$TAE_MWM_FUNC_MASKS{'MWM - No Resize','TAE Default'} = $TAE_MWM_FUNC_DEF;
$TAE_MWM_FUNC_MASKS{'MWM - No Resize','MWM Default'} =
						 $TAE_MWM_FUNC_ALL_BUT_RESIZE;
$TAE_MWM_FUNC_MASKS{'MWM - No Resize','None'} = $TAE_MWM_FUNC_NONE;
$TAE_MWM_FUNC_MASKS{'MWM - With Resize','TAE Default'} =
						 $TAE_MWM_FUNC_MIN_MOVE_RESIZE;
$TAE_MWM_FUNC_MASKS{'MWM - With Resize','MWM Default'} = $TAE_MWM_FUNC_ALL;
$TAE_MWM_FUNC_MASKS{'MWM - With Resize','None'} = $TAE_MWM_FUNC_RESIZE;
$TAE_MWM_FUNC_MASKS{'Flat Border','TAE Default'} = $TAE_MWM_FUNC_DEF;
$TAE_MWM_FUNC_MASKS{'Flat Border','MWM Default'} = $TAE_MWM_FUNC_ALL_BUT_RESIZE;
$TAE_MWM_FUNC_MASKS{'Flat Border','None'} = $TAE_MWM_FUNC_NONE;

   ### DECORATION MASKS

$TAE_MWM_DECOR_MASKS{'MWM - No Resize','TAE Default'} = $TAE_MWM_DECOR_DEF;
$TAE_MWM_DECOR_MASKS{'MWM - No Resize','MWM Default'} =
					 $TAE_MWM_DECOR_TITLEBAR_NO_RESIZE;
$TAE_MWM_DECOR_MASKS{'MWM - No Resize','None'} = $TAE_MWM_DECOR_FRAME;
$TAE_MWM_DECOR_MASKS{'MWM - With Resize','TAE Default'}=
					 $TAE_MWM_DECOR_TITLEBAR_NO_MAXIMIZE;
$TAE_MWM_DECOR_MASKS{'MWM - With Resize','MWM Default'} = $TAE_MWM_DECOR_ALL;
$TAE_MWM_DECOR_MASKS{'MWM - With Resize','None'} = $TAE_MWM_DECOR_RESIZE;
$TAE_MWM_DECOR_MASKS{'Flat Border','TAE Default'} =
					 $TAE_MWM_DECOR_TBAR_NO_MAXIMIZE_BORDER;
$TAE_MWM_DECOR_MASKS{'Flat Border','MWM Default'} =
					 $TAE_MWM_DECOR_TBAR_MOTIF_BORDER;
$TAE_MWM_DECOR_MASKS{'Flat Border','None'} =$TAE_MWM_DECOR_NONE;

#
# Mwm input mode codes -- corresponds to #define's in Xm/MwmUtil.h
#
$MWM_INPUT_MODELESS = 0;
$MWM_INPUT_PRIMARY_APPLICATION_MODAL = 1;
$MWM_INPUT_SYSTEM_MODAL = 2;
$MWM_INPUT_FULL_APPLICATION_MODAL = 3;

# Maximum charater count for UIL string literal
$MAX_LITERAL = 2000;
#
   # Get code style argument. Default to "basic" if not present, and
   # decrement remaining args by one.
if ($ARGV[$argvStyle] ne $codeStyleEnhanced && 
    $ARGV[$argvStyle] ne $codeStyleBasic){
   $argvResFile -= 1;
   $argvUilFile -= 1;
   $argvCFile -= 1;
   $codeStyle = $codeStyleBasic;
   }
else {
   $codeStyle = $ARGV[$argvStyle];
   }

# Allocate a collection and read the resource file.
$co = &Co_New(0);
$ResFile = $ARGV[$argvResFile]; 

# Resource file known to exist and be non-empty at this point.
# But, just in case..
$exists = (stat ($ResFile))[0];
defined ( $exists ) || die "$ERR: Resource file '$ResFile' does not exist.\n";

# Co_ReadFile will fail if resfile is obsolete format (eg, v5.1).
# Can't use P_ABORT due to taeperl bug (ABORT gets propogated). 
$code = &Co_ReadFile ($co, $ResFile, &P_CONT) ;
if ($code != 1)
    {
    # not reachable if P_ABORT passed to Co_ReadFile
    print "$ERR: File '$ResFile' has obsolete format.\n";
    print "$MsgInd Use \$TAEBIN/resupgrade.\n";
    ## Don't use "die".
    exit 1;
    }
$ResFileTime = &getResFileTime ($ResFile);
# Get names of each object in the collection.
@coNames = &Co_Names ($co);

# Get directory object and names of initial panels
$dirVm = &Co_Find ($co, "_directory");
if ($dirVm ne 0)
    {
    @dirNames = &Vm_Names ($dirVm);
    @initialPanels = &Vm_Get ($dirVm, "initial");
    $initialPanelCount = @initialPanels;

    ## Some old res files do have _directory but initial = "".
    if ( $initialPanels[0] eq "" )
	{
        print "$INFO: Null initial panel in resource file.\n";
        $initialPanelCount = 0;
	}
    }
else
    {
    print "$INFO: _directory not found in resource file.\n";
    $initialPanelCount = 0;
    }

# Create lookup table for initial panel generation
local($flag)=1;
foreach $ipanel (@initialPanels) {
   $initialPanelTable{$ipanel}=$flag;
   }

# open output files for writing
open (UIL, ">$ARGV[$argvUilFile]") ||
     die "$ERR: Cannot create UIL file '$ARGV[$argvUilFile]'";
open (C, ">$ARGV[$argvCFile]")   || 
     die "$ERR: Cannot create C file '$ARGV[$argvCFile]'"; 
open (IMAKEFILE, ">Imakefile") || die "$ERR: Cannot create Imakefile"; 

# determine basename of user application and .uil file (for code/Imakefile)
#
# ASSUMPTION - This enforces a .c and .uil suffix convention which
#              res2uil.sh also enforces.
#
$ProgBasename = $ARGV[$argvCFile];
$ProgBasename =~ s/.c$//;	# strip .c
$UilBasename  = $ARGV[$argvUilFile];
$UilBasename  =~ s/.uil$//;	# strip .uil
##print "	ProgBasename = $ProgBasename, UilBasename = $UilBasename\n";

&printHeader; # top of UIL file

# Iterate over panels and items.
$firstPanel = 1; 
foreach $panel (@coNames) {
    next unless ($panel =~ /_v$/);		# views only!!
    $viewVm = &Co_Find ($co, $panel);
    $panel =~ s/_v$//;				# isolate panel name (strip _v)
    $targetVm = &Co_Find ($co, "${panel}_t");	# get targetVm

    # Get parent font, fg, and bg to pass to pres type specific subroutines.
    $PanelFont = &Vm_Get ($viewVm, "_panel.font"); 
    $PanelFG   = &Vm_Get ($viewVm, "_panel.fg"); 
    $PanelBG   = &Vm_Get ($viewVm, "_panel.bg"); 

    @vmNames = &Vm_Names ($viewVm);		# for each item
    foreach $itemName (@vmNames) {
	next if ($itemName =~ /^_/);		# ignore _panel, etc.
	$type = &Vm_Get ($viewVm, "${itemName}._type");

	# see if this item has a font not seen in previous items
	$font = &Vm_Get ($viewVm, "${itemName}.font");
	$font = &parseFont ($font, $PanelFont, $type);
	$ItemFont = $font;

	# see if this item has a color not seen in previous items
	$fg = &Vm_Get ($viewVm, "${itemName}.fg");
	$fg = &parseColor ($fg, $PanelFG, $type);
	$ItemForeground = $fg;

	$bg = &Vm_Get ($viewVm, "${itemName}.bg");
	$bg = &parseColor ($bg, $PanelBG, $type);
	$ItemBackground = $bg;

	$ExtraArgs  =  &cleanInsert($Insertion{"$panel.$itemName.args"});
	$ExtraCons  =  &cleanInsert($Insertion{"$panel.$itemName.cons"});
	$ExtraCalls =  &cleanInsert($Insertion{"$panel.$itemName.calls"});
	$PanelItem = "${panel}_${itemName}";		# coding convenience

	# Call the presentation type specific subroutine
	eval "&${type}_Presentation (\$targetVm, \$viewVm, \$panel, \
		\$itemName, \$PanelFont, \$PanelFG, \$PanelBG); ";
	if ($@)
	    {
	    if ( $@ =~ /defined sub/ )	# parse error msg
		{
		# We don't have a subroutine defined for this pres type.
	        # TBD whether this is an $ERR or a $WARN.
	        # Try to avoid line wrap (if 15 char item, 8 char panel).
	        print "$ERR: Presentation type {$type} is unsupported:\n";
	        print "$MsgInd item $itemName, panel $panel. \n";
	        print "$MsgInd UIL will not be generated for this item.\n";
		}
	    else # A defined subroutine bombed; print perl's error result.
		{
		print "$ERR: Processing item $itemName, panel $panel: \n";
		print "$@\n";	
		}
	    }
    }
    # Panel processed after items so that controls can be known
    $ExtraArgs = &cleanInsert($Insertion{"$panel.args"});
    $ExtraCalls = &cleanInsert($Insertion{"$panel.calls"});
    $ExtraCons = &cleanInsert($Insertion{"$panel.cons"});
    &panel_Presentation ($viewVm, $panel);	# put out bulletin board
}

&printTopLevelValues;	# literals for panel top level resources
&printColorsAndFonts;	# colornames and font aliases
&printPixmaps;		# bitmap files and pixmaps
print UIL $Insertion{"prefix"};
&printProcs;    	# print on $ProcedureList
&printItems;		# print on $Items
&printPanels;   	# print on $Panels
print UIL $Insertion{"suffix"};
&printModuleEnd;

&generateCModule;
&generateImakefile;
exit(0);


# 
#
# 	panel
#

sub panel_Presentation {

    local($viewVm, $name) = @_;

	##
	## Set up top level panel shell function and increment panel counter
	##
    $PanelWidgetName     = "${panel}Panel";
    if ($firstPanel)
	{
	$firstPanel = 0;
	$PanelCount = 0;
	$AppShellPanel = $panel;
	}
    else
	{
	$PanelCount += 1;
	}

    $PanelTitle          = &Vm_Get ($viewVm, "_panel");
    $PanelNamesList .= "#define ${panel}_INDEX ${PanelCount}\n";

    ($PanelX, $PanelY)          = &Vm_Get ($viewVm, "_panel.origin"); 
    $PanelX = 0 if (!defined $PanelX);
    $PanelY = 0 if (!defined $PanelY);
    ($PanelWidth, $PanelHeight) = &Vm_Get ($viewVm, "_panel.size");
    $PanelWidth =  200 if (!defined $PanelWidth);
    $PanelHeight = 200 if (!defined $PanelHeight);
    $PanelBorder                = &Vm_Get ($viewVm, "_panel.border"); 
    $PanelBorder = 0 if (!defined $PanelBorder);
    $PanelIconFilename 		= &Vm_Get ($viewVm, "_panel.iconfile"); 
        ##
	## Don't expand environment vars in enhanced mode, since
	## XtaeNewPanel will handle environment vars. If basic mode,
	## expand any environment vars here.
    if ($codeStyle eq $codeStyleBasic)
 	{$PanelIconFilename = &resolveFilePathname($PanelIconFilename);}
    $PanelIconName 		= &Vm_Get ($viewVm, "_panel.iconlabel"); 

	# Note: for panel, momFG, momBG, and PanelFont are the same
	#       as fg, bg, and font
    local($fg) = &Vm_Get ($viewVm, "_panel.fg");
          $fg = &parseColor ($fg, $fg, "_panel");

    local($bg) = &Vm_Get ($viewVm, "_panel.bg");
          $bg = &parseColor ($bg, $bg, "_panel");

	# Can't control font of titlebar this way.
    local($font) = &Vm_Get ($viewVm, "_panel.font");
          $font = &parseFont ($font, $font, "_panel");

	# traversal handling is similar to agentRt.cc
    local($traversal) = &Vm_Get ($viewVm, "_panel.traversal");
    local($navigation, $highlight);
    if ( $traversal == 1 )
	{
	$traversal  = "true" ;	# Motif wants Boolean not integer
	$navigation = "XmNnavigationType     = XmTAB_GROUP ;";
	$highlight  = 2 ;
	}
    else ## if $traversal == 0 OR if traversal is undefined
	{
	$traversal  = "false" ;	# Motif wants Boolean not integer
	# $navigation = XmNONE ;  # NOTE: This differs from WPT.
	$navigation = '';
	$highlight  = 0 ;
	}

	##
	## Set up panel functions and decorations
	##
    local($framestyle) 		= &Vm_Get ($viewVm, "_panel.framestyle"); 
    local($titlebarstyle) 	= &Vm_Get ($viewVm, "_panel.titlebarstyle"); 

    $PanelModal = $MWM_INPUT_MODELESS;
    $PanelIconic = 'false';
    $PanelVisible = 'true';
    local($state) 		= &Vm_Get ($viewVm, "_panel.state"); 
    if ($state eq "Visible")
	{$PanelVisible = 'true'; $PanelIconic = 'false';}
    elsif ($state eq "Invisible")
	{$PanelVisible = 'false'; $PanelIconic = 'false';}
    elsif ($state eq "Iconic")
	{$PanelVisible = 'true'; $PanelIconic = 'true';}
    elsif ($state eq "Modal")
	{$PanelVisible = 'true'; $PanelIconic = 'false';
	 $PanelModal = $MWM_INPUT_PRIMARY_APPLICATION_MODAL;}

	## Set defaults if framestyle or titlebarstyle undefined
    if (defined($framestyle) && defined($titlebarstyle)) 
	{
	$PanelFunc=$TAE_MWM_FUNC_MASKS{$framestyle,$titlebarstyle};
	$PanelFunc = $TAE_MWM_FUNC_DEF if (!defined $PanelFunc);
	$PanelDecor=$TAE_MWM_DECOR_MASKS{$framestyle,$titlebarstyle};
	$PanelDecor = $TAE_MWM_DECOR_DEF if (!defined $PanelDecor);
	}
    else
	{
	$PanelFunc = $TAE_MWM_FUNC_DEF;
	$PanelDecor = $TAE_MWM_DECOR_DEF;
	}


    local($defaultitem) 	= &Vm_Get ($viewVm, "_panel.defaultitem"); 


	# Resources to control whether panel is scrollable and the
	# extent of its Work Region (of the Scrolled Window)
    local($sbDisplayPolicy) 	= &Vm_Get ($viewVm, "_panel.sbDisplayPolicy"); 
    local($WWWidth, $WWHeight) 	= &Vm_Get ($viewVm, "_panel.workwinSize"); 
    local($scrolledWin);
    
    if (!defined($sbDisplayPolicy) || $sbDisplayPolicy eq "None")
	{
	$scrolledWin = 'FALSE';
	$PanelBBname = 'Panel';
	$PanelBBx = "k_${PanelWidgetName}_x";
	$PanelBBy = "k_${PanelWidgetName}_y";
	$PanelBBwidth = $PanelWidth;
	$PanelBBheight = $PanelHeight;
	}
    else
	{
	$scrolledWin = 'TRUE';
	$PanelBBname = '_PanelBB';
	$PanelBBx = 0;
	$PanelBBy = 0;
	$PanelBBwidth = $WWWidth;
	$PanelBBheight = $WWHeight;
	if ($sbDisplayPolicy eq "Always")
	    {$PanelScrollPolicy = 'XmSTATIC';}
	else
	    {$PanelScrollPolicy = 'XmAS_NEEDED';}
	}
#    print "PANEL: sb = $sbDisplayPolicy, Scroll = $PanelScrollPolicy\n";

	##
	## Define literals for top level resources
	##
	## CAUTION! Any changes to the literals must be accompanied by
	##	    corresponding changes to the resourceName and
	##	    indexSuffix vars in generateCModule.
	##
    $TopLevelResourceValues .=<<end;
	! TopLevelShell resource for panel $panel
	k_${PanelWidgetName}_title : exported \"$PanelTitle\";
	k_${PanelWidgetName}_x : exported $PanelX;
	k_${PanelWidgetName}_y : exported $PanelY;
	k_${PanelWidgetName}_w : exported $PanelWidth;
	k_${PanelWidgetName}_h : exported $PanelHeight;
	k_${PanelWidgetName}_b : exported $PanelBorder;
	k_${PanelWidgetName}_vsbl : exported $PanelVisible;
	k_${PanelWidgetName}_iconic : exported $PanelIconic;
	k_${PanelWidgetName}_mode : exported $PanelModal;
	k_${PanelWidgetName}_func : exported $PanelFunc;
	k_${PanelWidgetName}_decor : exported $PanelDecor;
	k_${PanelWidgetName}_iconName : exported \"$PanelIconName\";
	k_${PanelWidgetName}_iconFile : exported \"$PanelIconFilename\";

end
	## Replace x,y values with identifiers
    $PanelX = "k_${PanelWidgetName}_x";
    $PanelY = "k_${PanelWidgetName}_y";
    $PanelWidth = "k_${PanelWidgetName}_w";
    $PanelHeight = "k_${PanelWidgetName}_h";
    $PanelBorder = "k_${PanelWidgetName}_b";

	##
	## If this is an initial panel, generate the panel creation call
	## append it to the InitialPanelList.
	##
    if ( defined( $initialPanelTable{"$panel"} ) ||
	 ( ($initialPanelCount eq 0) && ($PanelCount eq 0)) ) 
       {
	##
	## for initial panels, put the creation call on the initial
	## panels list.
	##
       $InitialPanelList .= &generateNewPanelCode($panel);
       if ($initialPanelCount eq 0)
	 {
	 print "$INFO: No initial panel specified; using ${panel}.\n";
	 }
       }

	##
	##  Now generate create/destroy callbacks for the panel.
	##  Generate pointer cleanup code for the destroy callback.
    local($createCallback)  = "${panel}Panel_create";
    local($destroyCallback) = "${panel}Panel_destroy";
    local($cleanupCode) =
    "TopLevelWidget[${panel}_INDEX] = ContainerWidget[${panel}_INDEX] = NULL;";
    &generateCallback ($createCallback);
    &generateCallback ($destroyCallback,'','',$cleanupCode);

	##
	## Append the panel UIL to the panel list
	##
    $Panels .= <<end;


object ${panel}$PanelBBname : XmBulletinBoard	! panel presentation
    {
    arguments
	{
	XmNx		      = $PanelBBx;
	XmNy		      = $PanelBBy;
	!XmNwidth	      = $PanelBBwidth;   (reference info)
	!XmNheight	      = $PanelBBheight;   (reference info)
	XmNforeground         = color_$fg ;
	XmNbackground         = color_$bg ;
	XmNborderColor        = color_$fg ;
	XmNborderWidth        = $PanelBorder;
	XmNmarginWidth	      = 0;
	XmNmarginHeight	      = 0;
	XmNresizePolicy       = XmRESIZE_GROW;
	XmNtraversalOn        = $traversal;
	$navigation

	$ExtraArgs
	};
    controls 
	{
$PanelItemList
	$ExtraCons
	};
    callbacks
	{
	MrmNcreateCallback = procedure $createCallback;
	XmNdestroyCallback = procedure $destroyCallback;
	};
    };
end

if ($scrolledWin eq "TRUE")
	{
        $Panels .= <<end;

object ${panel}Panel : XmScrolledWindow
    {
     arguments
	{
	XmNx                  = $PanelX;
	XmNy                  = $PanelY;
	XmNwidth              = $PanelWidth;
	XmNheight             = $PanelHeight;
	XmNforeground         = color_$fg ;
	XmNbackground         = color_$bg ;
	XmNborderColor        = color_$fg ;
	XmNborderWidth        = $PanelBorder;

	XmNtraversalOn        = $traversal;
	$navigation

	XmNscrollBarDisplayPolicy = $PanelScrollPolicy;
	XmNscrollingPolicy = XmAUTOMATIC;
	XmNscrolledWindowMarginWidth = 2;
	XmNscrolledWindowMarginHeight = 2;

	$ExtraArgs
	};
     controls
	{
	XmBulletinBoard ${panel}$PanelBBname;
	$ExtraCons
	};
     };
end
	}
	# reset for next panel (if any)
    $PanelItemList = "";
}

sub generateCallback {
    local($callbackName,$callbackStructType,$callbackArgType,$codeFrag,
	  $newPanel,$nextState,$curState) = @_; 

	##
	## Generate connection management code
	##
    if ( $ENV{DEBUG_CONNECTIONS} == 1) 
	{
	print "generateCallback: newPanel = \'$newPanel\', next = \'$nextState\', cur = \'$curState\'\n";
	}
    local($connectionCode);
    if ( (!defined $newPanel || $newPanel eq "") && ($curState ne "") )
	{
	$connectionCode = &changePanelState($panel,$curState);
	}
    elsif ($newPanel ne "")
	{
	$connectionCode = &generatePanelConnection($newPanel,$nextState,
						  $panel,$curState);
	}
    if  ($ENV{DEBUG_CONNECTIONS} == 1) 
	{
	print "generateCallback: connection code\n";
	print $connectionCode;
	print "\n";
	}
    $ProcedureList .= "\t$callbackName(${callbackArgType});\n";
    $FunctionNameList .= qq!{"$callbackName", (XtPointer)$callbackName},\n!;

    local($callDataType, $callDataCast, $callbackPrintf);

    $callDataType = "XtPointer";
    $callbackPrintf = "if ( cb )\n\tprintf (\"callback: $callbackName, reason = %d\\n\", cb->reason);\n    else\n\tprintf (\"callback: $callbackName\\n\");";

    if ($callbackStructType eq "")
	{
	$callDataCast = "XmAnyCallbackStruct *cb = (XmAnyCallbackStruct *) call_data;";
	}
    else
	{
	$callDataCast = "Xm${callbackStructType}CallbackStruct *cb = (Xm${callbackStructType}CallbackStruct *) call_data;";
	}

    $FunctionDclList .= <<end;
void $callbackName (widget, client_data, call_data)
        Widget  widget;
        XtPointer client_data;
        $callDataType call_data;
    {
    $callDataCast
    $callbackPrintf
    $connectionCode
    $codeFrag 
    }

end
}


sub changePanelState{
    local($panelName,$panelState) = @_;
    if  ($ENV{DEBUG_CONNECTIONS} == 1) 
	{
	print "changePanelState: $panelName, $panelState\n";
	}

    local($panelCode);
    if ($panelState eq "Preferred"){
	print "$INFO: Preferred state for panel (${panelName}) not supported.\n";
	}

    elsif ($panelState eq "Iconic"){
        $panelCode .= <<end;

    XIconifyWindow(TheDisplay,
                   XtWindow(TopLevelWidget[${panelName}_INDEX]),
                   TheScreen);
end
	}

    elsif ($panelState eq "Deleted"){
	$panelCode .= <<end;

    if (TopLevelWidget[${panelName}_INDEX] == NULL)
	return;
    XtDestroyWidget(TopLevelWidget[${panelName}_INDEX]);
end
	}

    elsif ($panelState eq "Invisible"){
        $panelCode = "XtUnmapWidget(TopLevelWidget[${panelName}_INDEX]);";
	}

    elsif ($panelState eq "No Change"){
	##
	## No code generated for no change
	##
	}
    else{
	print "$WARN: Invalid current state (${panelState}) for panel ${panelName} connection\n";
	}

    return $panelCode;
    }
#
# generatePanelConnections
#
#	Generate code to manage connection state changes for panel-to-panel
#	connections. Calls changePanelState() to process current panel change.
#
sub generatePanelConnection{
    local($nextPanel,$nextState,$curPanel,$curState) =@_;
    if  ($ENV{DEBUG_CONNECTIONS} == 1) 
	{
	print "generatePanelConnection: $nextPanel, $nextState, $curPanel, $curState\n";
	}

	##
	## Generate code to process the current panel
	##
    local($curpanelCode) = &changePanelState($curPanel,$curState);

	##
	## Generate code to process the next panel
	##
    local($panelCode);
    if ($nextState eq "Iconic" || $nextState eq "Fast Iconic"){
        $panelCode .= <<end;

    if (TopLevelWidget[${nextPanel}_INDEX] == NULL)
	return;

    XIconifyWindow(TheDisplay,
                   XtWindow(TopLevelWidget[${nextPanel}_INDEX]),
                   TheScreen);

end
	}

    elsif ($nextState eq "Deleted"){
	$panelCode .= <<end;

    if (TopLevelWidget[${nextPanel}_INDEX] == NULL)
	return;
    XtDestroyWidget(TopLevelWidget[${nextPanel}_INDEX]);
end
	}

    elsif ($nextState eq "Invisible"){
        $panelCode .=  <<end;

        if (TopLevelWidget[${nextPanel}_INDEX] == NULL)
	    return;
	XWithdrawWindow(TheDisplay,
			XtWindow(TopLevelWidget[${nextPanel}_INDEX]),
			TheScreen);
end
	}

    elsif ($nextState eq "Visible" || $nextState eq "Preferred"){
	$panelCode .= <<end;
/* If top level pointer still valid, just re-map widget */
	if (TopLevelWidget[${nextPanel}_INDEX] != NULL)
    	    {
	    XMapWindow(TheDisplay,
		       XtWindow(TopLevelWidget[${nextPanel}_INDEX]));
	    return;
	    }

end
	$panelCode .= &generateNewPanelCode($nextPanel);
	}
    else{
	print "$WARN: Invalid next state (${nextState}) for panel ${curPanel} to panel ${nextPanel} connection\n";
	}

    $panelCode .= $curpanelCode;
    return $panelCode;
}

sub generateNewPanelCode{
    local($newpanel)=@_;

    local($newpanelCode,$PanelWidgetName);
    $PanelWidgetName = "${newpanel}Panel";


    if ($codeStyle eq $codeStyleEnhanced )
	{
	## 
	## For enhanced codeSytle, generate the Xtae panel creation call
	##
	$newpanelCode .=<<end;

    XtaeNewPanel( TheDisplay, &S_MrmHierarchy, \"$PanelWidgetName\",
		  &TopLevelWidget[${newpanel}_INDEX],
	 	  &ContainerWidget[${newpanel}_INDEX]);
end
	}
    else
	{
	##
	## for basic code style generate the local Mrm new panel code
	##
	$newpanelCode .=<<end;

    _createNewPanel( TheDisplay, &S_MrmHierarchy, \"$PanelWidgetName\",
		  &TopLevelWidget[${newpanel}_INDEX],
	 	  &ContainerWidget[${newpanel}_INDEX]);

end
	}
	##
	## Finally, return the generated code to caller
	##
    return $newpanelCode;
}

#
#	map TAE Plus view resource value to uil resource value. 
#
#	args:
#		$viewVm = vm id of view
#		$itemName = tae plus item name
#		$qualName = tae plus qualifier name (e.g., "alignment")
#		@mappingPairs = taePlusValue, uilValue,
#				taePlusValue, uilValue, ...
#
#	The first pair in mappingPairs is the default. 
#	Comparisons are case insensitive. 
#
sub translateEnum
    {
    local($viewVm, $itemName, $qualName, @mappingPairs) = @_;
    local($i); 
    for ($i=0; $i <= $#mappingPairs; $i+=2)	# for every other element:
	{
	$mappingPairs[$i] =~ tr/A-Z/a-z/;	# make "keys" lowercase
	}
    local(%mapping) = @mappingPairs;		# create associative array

    # Some resources are the value of the view (w/out qualifier)....
    local($value);
    if ( $qualName eq "NO_QUAL" )
	{ $value = &Vm_Get($viewVm, "${itemName}"); }
    else # ... but most have quals
	{ $value = &Vm_Get($viewVm, "${itemName}.${qualName}"); }

    $value =~ tr/A-Z/a-z/;			# to lower case for ...
    local($mappedValue) = $mapping{$value};	# perform mapping
    $mappedValue = $mappingPairs[1] if (! defined $mappedValue);
    return $mappedValue;
    }

#
#	Add item name to list of items for the
#	current panel.   The list is built in reverse
#	order to get the wb-defined stacking order.
#	(Note that Wpt creates items in viewSt 
#	order, but gets stacking order correct
#	by realizing the panel FIRST.)
#
sub addToPanel 
    {
    local($type, $name) = @_;
    $PanelItemList = "\t$type $name;\n" . $PanelItemList;
    }


# 
#
#	button (pushbutton)
#
sub button_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs
    &TAEhelp ($viewVm, $itemName, $panel);

    local($labelalign) = &translateEnum ($viewVm, $itemName, "labalign",
	"Center", "XmALIGNMENT_CENTER",		# default !!
	"Left",   "XmALIGNMENT_BEGINNING",
	"Right",  "XmALIGNMENT_END"
	);

	# Add to list of items for this panel
    &addToPanel ("XmPushButton", $PanelItem);

	#Extract connection info
    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    local($newPanel)= &Vm_Get ($targetVm, "${itemName}._newPan");
    local($nextState) = &Vm_Get ($targetVm, "${itemName}._nextState");
    local($curState) = &Vm_Get ($targetVm, "${itemName}._currState");
    local($codeFrag) = &Vm_Get ($targetVm, "${itemName}._code");

	# Generate callback if necess. (must be event generating to use
	# 				connections!)
    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_act";
    	&generateCallback ($procedureName,"PushButton",
			   '',$codeFrag,
			   $newPanel,$nextState,$curState) ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNactivateCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

$Items .= <<end;

object $PanelItem : XmPushButton  ! button presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNlabelString     = $CVA{"title"};
	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};
	XmNhighlightThickness = $CVA{"highlight"};

	! Presentation Panel resources
	XmNalignment = $labelalign;
	! invariant resources; 
	$ExtraArgs
	};
    $callback
    };
end
}

# 
#	message (message box)
#
sub message_Presentation {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

        # Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName,
                        $momFont, $momFG, $momBG );

        # SpecialArgs
    &TAEhelp ($viewVm, $itemName, $panel);

        # Deferred: components (removing buttons: OK, Help, Cancel
        # We define a bitmask in $TAEINC/wptdefs.h; See "Dialog stuff"
        # From Motif1.2.2 periodic.uil, it appears that generating uil like:
        # controls {
        #         Xm_OK unmanaged { };
        #         Xm_Help unmanaged { };
        #     };
        # should do the trick, but can't I get this to work with 1.1.4 uil.
        # When I put the 1.2.2 bin dir in my path first, it does NOT complain
        # about the above.
        # NOTE - this "components" bit applies to filesel prestype also.
    local($components) = &Vm_Get ($viewVm, "${itemName}.components");

    local($helptitle) = &Vm_Get ($viewVm, "${itemName}.helptitle");
    $helptitle = "Help" if (!defined $helptitle);
    local($canceltitle) = &Vm_Get ($viewVm, "${itemName}.canceltitle");
    $canceltitle = "Cancel" if (!defined $canceltitle);
    local($oktitle) = &Vm_Get ($viewVm, "${itemName}.oktitle");
    $oktitle = "OK" if (!defined $oktitle);
    local($text);
    if (&Vm_Get ($viewVm, "$itemName.text"))	# new style resource file
        {$text = &getTitle ($viewVm, $itemName, "text");}
    else					# old style resource file
	{$text = &getTitle ($viewVm, $itemName);} 
    local($minimizeButtons) = &translateEnum ($viewVm, $itemName, "buttonsize",
	"Uniform", "false",
	"Compressed", "true"
	);
    local($align) = &translateEnum ($viewVm, $itemName, "align",
	"Center", "XmALIGNMENT_CENTER",		# default !!
	"Left",   "XmALIGNMENT_BEGINNING",
	"Right",  "XmALIGNMENT_END"
	);
    local($dialogType) = &translateEnum ($viewVm, $itemName, "msgtype",
	"0","XmDIALOG_MESSAGE",
	"1","XmDIALOG_INFORMATION",
	"2","XmDIALOG_WARNING",
	"3","XmDIALOG_ERROR",
	"4","XmDIALOG_QUESTION",
	"5","XmDIALOG_WORKING");

	# Add to list of items for this panel and generate callback if necess.
    &addToPanel ("XmMessageBox", $PanelItem);

    &generateCallback ("${PanelItem}_create");
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        &generateCallback ("${PanelItem}_ok") ;
        &generateCallback ("${PanelItem}_cancel") ;
        &generateCallback ("${PanelItem}_help") ;
	}

$Items .= <<end;

object $PanelItem : XmMessageBox	! message presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};


	! Presentation Panel resources
	XmNdialogType        	= $dialogType; 
	XmNmessageAlignment	= $align;		!XmALIGNMENT_CENTER;
	XmNmessageString    	= $text;
	XmNminimizeButtons      = $minimizeButtons;	! false;
	XmNdefaultButtonType	= 0;

	XmNlabelFontList        = font_alias_$CVA{"font"};
	XmNbuttonFontList       = font_alias_$CVA{"font"};
	XmNtextFontList         = font_alias_$CVA{"font"};

	XmNhelpLabelString      = "$helptitle";	
	XmNcancelLabelString    = "$canceltitle";
	XmNokLabelString        = "$oktitle";	

	! invariant resources; 
	XmNnoResize 		= false;
	XmNresizePolicy       = XmRESIZE_NONE;
	$ExtraArgs
	};
    callbacks
	{
	XmNokCallback = procedure ${PanelItem}_ok();
	XmNcancelCallback = procedure ${PanelItem}_cancel();
	XmNhelpCallback = procedure ${PanelItem}_help();
	$ExtraCalls
	};
    };
end
}
#
# 
#
#	filesel (file selection box)
#
sub filesel_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs
    &TAEhelp ($viewVm, $itemName, $panel);

	# Deferred: components (removing buttons: OK, Help, Cancel, Filter
	# We define a bitmask in $TAEINC/wptdefs.h; See "Dialog stuff"
	# From Motif1.2.2 periodic.uil, it appears that generating uil like:
	# controls {
        #         Xm_OK unmanaged { };
        #         Xm_Help unmanaged { };
        #     };
	# should do the trick, but can't I get this to work with 1.1.4 uil.
	# When I put the 1.2.2 bin dir in my path first, it does NOT complain
	# about the above.
	# NOTE - this "components" bit applies to Message prestype also.

    local($components) = &Vm_Get ($viewVm, "${itemName}.components");

    local($helptitle) = &Vm_Get ($viewVm, "${itemName}.helptitle");
    $helptitle = "Help" if (!defined $helptitle);
    local($canceltitle) = &Vm_Get ($viewVm, "${itemName}.canceltitle");
    $canceltitle = "Cancel" if (!defined $canceltitle);
    local($applytitle) = &Vm_Get ($viewVm, "${itemName}.applytitle");
    $applytitle = "Apply" if (!defined $applytitle);
    local($oktitle) = &Vm_Get ($viewVm, "${itemName}.oktitle");
    $oktitle = "OK" if (!defined $oktitle);

    local($filtertitle) = &Vm_Get ($viewVm, "${itemName}.filtertitle");
    $filtertitle = "Filter" if (!defined $filtertitle);
    local($dirstitle) = &Vm_Get ($viewVm, "${itemName}.dirstitle");
    $dirstitle = "Directories" if (!defined $dirstitle);
    local($filestitle) = &Vm_Get ($viewVm, "${itemName}.filestitle");
    $filestitle = "Files" if (!defined $filestitle);
    local($selectiontitle) = &Vm_Get ($viewVm, "${itemName}.selectiontitle");
    $selectiontitle = "Selection" if (!defined $selectiontitle);
    local($filetype) = &translateEnum ($viewVm, $itemName, "fileType",
	"Regular Files Only",   "XmFILE_REGULAR",
	"Directory Files Only", "XmFILE_DIRECTORY",
	"All Files", "XmFILE_ANY_TYPE" 
	); 
	##
	## Set fileTypeMask resource line
	##
    local($fileTypeMask,$fixFileTypeMask);
    if ($filetype eq "XmFILE_ANY_TYPE"){
	$fileTypeMask = " XmNfileTypeMask	= XM_FILE_ANY_TYPE;";
	   # NOTE: Motif 1.1.4 does not recognize XmFILE_ANY_TYPE in UIL.
	   #       The identifier XM_FILE_ANY_TYPE is defened in the
	   #	   generated UIL with value 3 (Neither the '+' nor '|'
	   #	   operators applied to XmFILE_REGULAR and XmFILE_DIRECTORY
	   #	   give the desired result.
	}
    else{
	$fileTypeMask = "XmNfileTypeMask	= ${filetype};";
	}

	# Get initialfilter and expand env variable (if any) 
    local($initialfilter) = &Vm_Get ($viewVm, "${itemName}.initialfilter");
    $initialfilter = &resolveFilePathname($initialfilter);

    local($minimizeButtons) = &translateEnum ($viewVm, $itemName, "buttonsize",
	"Uniform", "false",
	"Compressed", "true"
	);

    &addToPanel ("XmFileSelectionBox", $PanelItem);

    &generateCallback ("${PanelItem}_create", '','',$fixFileTypeMask);
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        &generateCallback ("${PanelItem}_ok","FileSelectionBox") ;
        &generateCallback ("${PanelItem}_cancel","FileSelectionBox") ;
        &generateCallback ("${PanelItem}_help","FileSelectionBox") ;
	}

$Items .= <<end;

object $PanelItem : XmFileSelectionBox	! filesel presentation
    {
    arguments
	{
	! QUALIFIERS with no equivalent resource:
	!	 XmNlabelString = "$title";
	! 	 XmNfontList = font_alias_$font;
	! Item Specification Panel resources
	!	XmNlabelString     = $CVA{"title"};
	!	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources

	! XmNlistVisibleItemCount  = $numvis; !not used. depends on font size
	$fileTypeMask
	XmNminimizeButtons      = $minimizeButtons;

	XmNlabelFontList        = font_alias_$CVA{"font"} ;
	XmNbuttonFontList       = font_alias_$CVA{"font"} ;
	XmNtextFontList         = font_alias_$CVA{"font"} ;

	XmNhelpLabelString      = "$helptitle" ;
	XmNcancelLabelString    = "$canceltitle" ;
	XmNapplyLabelString     = "$applytitle" ;
	XmNokLabelString        = "$oktitle" ;

	XmNdirListLabelString   = "$dirstitle" ;
	XmNfileListLabelString  = "$filestitle" ;
	XmNfilterLabelString    = "$filtertitle" ;
	XmNselectionLabelString = "$selectiontitle" ;

	XmNdirMask = "$initialfilter";
	! invariant resources; 
	XmNnoResize	= false;
	XmNresizePolicy	= XmRESIZE_NONE;
	$ExtraArgs
	};
    callbacks
	{
	MrmNcreateCallback = procedure ${PanelItem}_create();
	XmNokCallback = procedure ${PanelItem}_ok();
	XmNcancelCallback = procedure ${PanelItem}_cancel();
	XmNhelpCallback = procedure ${PanelItem}_help();
	$ExtraCalls
	};
    };
end
}


# 
#
#	textlist (selection list)
#
sub textlist_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# Compute offset for label placement
    local($labelOffset) = $CVA{"border"} + $CVA{"shadow"};

	# SpecialArgs
    ## NOTE: maxallow resource is obsolete; do not process
    ##       Maximum Selections Allowed comes from the target

    ## Not supported: errmsg
    local($errormsg) = &Vm_Get ($viewVm, "${itemName}.errormsg");

    # Get Choices from:
    local($fromafile) = &Vm_Get ($viewVm, "${itemName}.fromafile"); 
    local(@valids, $listItems, $itemCount);
    local($fileCallback,$filecbProcName);
    local($filecbInitCode);
    if ($codeStyle eq $codeStyleEnhanced ) {
	$filecbInitCode = "XtaeSelectionListInit(widget,(char *)client_data);";
	}
    if ($fromafile eq "File") 
	{ 
        local($filename) = &Vm_Get ($viewVm, "${itemName}.filename");
	$filecbProcName = "${panel}_${itemName}_create";
    	&generateCallback ($filecbProcName, '', 'string', $filecbInitCode);
	$fileCallback = "MrmNcreateCallback = procedure ${filecbProcName}(\"${filename}\");";
	$listItems="\"\"";
	$itemCount = 0;
	}
    else		# note: default is "Constraints"
	{ 
	## Currently use one entry callback for all children
	## Alternate approach: generate callbacks for each child 

	# Get list choices
	@valids = &Vm_GetValidString ($targetVm, $itemName); 
	grep (s/"/\\"/g, @valids);		# fix interior quotes
	# Convert list choices to format that "string_table" accepts.
	$quote = "\"" ;
	# insert quote before first item
	$valids[0] = $quote . $valids[0] ;
	# append quote after last item
	$valids[$#valids] .= $quote ;
	# separate each item by quote-comma-newline-tabs-quote
	$listItems = join ("\",\n\t\t\t\t\"", @valids) ;
	$itemCount = $#valids + 1;
	if (length($listItems) > $MAX_LITERAL)
	    {
	    print "$ERR: textlist valids exceed maximum allowable length ($MAX_LITERAL) for string literal\n";
	    print "${MsgInd} item = ${PanelItem}\n";
	    }
	
	## print "\ntextlist = @valids [COUNT = $itemCount]\n";
	## print "\nlistItems = $listItems\n";
	}

    # Scrollbar valids
    local($raw_sboption) = &Vm_Get ($viewVm, "${itemName}.sboption"); 
    local($listSizePolicy, $scrollBarDisplayPolicy);
    if ($raw_sboption eq "Vertical and Horizontal Always") 
	{ 
	$listSizePolicy         = XmCONSTANT ;
	$scrollBarDisplayPolicy = XmSTATIC ;
	}
    elsif ($raw_sboption eq "Only Vertical,  As Needed") 
	{ 
	$listSizePolicy         = XmVARIABLE ;
	$scrollBarDisplayPolicy = XmAS_NEEDED ;
	}
    elsif ($raw_sboption eq "Only Vertical,  Always") 
	{ 
	$listSizePolicy         = XmVARIABLE ;
	$scrollBarDisplayPolicy = XmSTATIC ;
	}
    elsif ($raw_sboption eq "Vertical and Horizontal As Needed") 
	{ 
	$listSizePolicy         = XmCONSTANT ;
	$scrollBarDisplayPolicy = XmAS_NEEDED ;
	}
    else 
	{ 
	print "$WARN: Invalid or missing qualifier \'sboption\' for \'${itemName}\'; using default.\n";
	$listSizePolicy         = XmCONSTANT ;
	$scrollBarDisplayPolicy = XmAS_NEEDED ;
	}

    # Selection policy
    local($raw_selpolicy) = &Vm_Get ($viewVm, "${itemName}.selpolicy"); 
    local($selpolicy) = XmBROWSE_SELECT ;	# default values
    local($callbackReason) = XmNbrowseSelectionCallback ;
    local($cbProcSuffix)="selbrw";
    if ($raw_selpolicy eq "Single") 
	{ 
	$selpolicy = XmBROWSE_SELECT ;
	$callbackReason = XmNbrowseSelectionCallback ;
	}
    elsif ($raw_selpolicy eq "Multiple") 
	{
	$selpolicy = XmMULTIPLE_SELECT ;
	$callbackReason = XmNmultipleSelectionCallback ;
	$cbProcSuffix = "selmul";
	}
    elsif ($raw_selpolicy eq "Multiple Ranges") 
	{
	$selpolicy = XmEXTENDED_SELECT ;
	$callbackReason = XmNextendedSelectionCallback ;
	$cbProcSuffix = "selext";
	}
    else 
	{
	print "$WARN: Invalid or missing qualifier \'selpolicy\' for \'${itemName}\'; using default.\n";
	$callbackReason = XmNbrowseSelectionCallback ;
	}

    &addToPanel ("XmForm", $PanelItem);

	# Convert selected choices to format that "string_table" accepts.
    local(@selecteds) = &Vm_Get ($targetVm, $itemName); 
    grep (s/"/\\"/g, @selecteds);			# fix interior quotes
    local($selectedItems, $selectedItemCount);
    $quote = "\"" ;
    # insert quote before first item
    $selecteds[0] = $quote . $selecteds[0] ;
    # append quote after last item
    $selecteds[$#selecteds] .= $quote ;
    # separate each item by quote-comma-newline-tabs-quote
    $selectedItems = join ("\",\n\t\t\t\t\"", @selecteds) ;
    $selectedItemCount = $#selecteds + 1;
## print "\ntextlist = @selecteds [COUNT = $selectedItemCount]\n";
## print "\nselectedItems = $selectedItems\n";

    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_${cbProcSuffix}";
    	&generateCallback ($procedureName,"List") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	$fileCallback
	$callbackReason = procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

	#
	# Append the uil object declaration to the items list
	#
$Items .= <<end;

object $PanelItem : XmForm	! textlist presentation
    {
    arguments
	{
	! Item Specification Panel resources
	!	XmNlabelString     = $CVA{"title"};
	!	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources
	XmNmarginHeight = 0;
	XmNmarginWidth = 0;
	};
    controls
	{
    	${PanelItem}\$label : XmLabel
            {
            arguments
                {
		XmNtopAttachment   = XmATTACH_FORM;
		XmNtopOffset	   = $labelOffset;
		XmNleftAttachment   = XmATTACH_FORM;
		XmNleftOffset	   = $labelOffset;
		XmNrightAttachment   = XmATTACH_FORM;
		XmNrightOffset	   = $labelOffset;
		XmNfontList        = font_alias_$CVA{"font"};
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNborderColor     = color_$CVA{"fg"};
		XmNshadowThickness = $CVA{"shadow"};

               	XmNlabelString  = 'Selection List';
               	XmNalignment    = XmALIGNMENT_BEGINNING;
               ! XmNhighlightThickness = 0;
                };
            };
	${PanelItem}\$list : XmScrolledList
	    {
	    arguments
		{
		XmNtopAttachment   = XmATTACH_WIDGET;
		XmNtopWidget	   = ${PanelItem}\$label;
		XmNleftAttachment   = XmATTACH_FORM;
		XmNrightAttachment   = XmATTACH_FORM;
		XmNbottomAttachment   = XmATTACH_FORM;
		XmNfontList        = font_alias_$CVA{"font"};
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNborderColor     = color_$CVA{"fg"};

		! the following sets XmNitemCount automatically
		XmNitems = string_table( $listItems ); 

		! the following sets XmNselectedItemCount automatically
		XmNselectedItems   = string_table( $selectedItems );

		XmNselectionPolicy = $selpolicy;
		XmNlistSizePolicy = $listSizePolicy ;
		XmNscrollBarDisplayPolicy = $scrollBarDisplayPolicy ;

		! invariant resources; 
		XmNhighlightThickness   = 0;
		XmNlistMarginHeight     = 3;
		XmNlistMarginWidth      = 3;
		XmNlistSpacing          = 3;
		$ExtraArgs
		};
	   $callback
	   };
       };
    };
end
}

# 
#
#	scale
#
sub scale_Presentation {

local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

# Handle common resources (fg,bg,title,font,size, etc.)
&CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs
    local($defaultValue)   = &Vm_Get ($targetVm, $itemName);
    #	Note: XmScale takes only integer values:
    $defaultValue = (defined $defaultValue) ? int($defaultValue) : 0;
    local($decimalPts)     = &Vm_Get ($viewVm, "${itemName}.decimalPts");

    local($raw_showValue)  = &Vm_Get ($viewVm, "${itemName}.ShowValue");
    local($showValue);
    $showValue = ($raw_showValue == 1) ? "true" : "false" ;

	# NOTE: the "sensitive" resource is obsolete

	# set resources that depend on whether scale is horizontal or vertical
    local($raw_orientation) = &Vm_Get ($viewVm, "${itemName}.orientation");
    local($raw_direction)   = &Vm_Get ($viewVm, "${itemName}.direction");
    local($sliderThickness) = &Vm_Get ($viewVm, "${itemName}.sliderThickness");
    local($orientation, $direction, $scaleWidth, $scaleHeight);

    if ($raw_orientation eq "Vertical")
	{
    	$orientation = XmVERTICAL ;
        $direction = XmMAX_ON_TOP; 		# default
        $direction = XmMAX_ON_TOP    if ( $raw_direction eq "Ascending");
        $direction = XmMAX_ON_BOTTOM if ( $raw_direction eq "Descending");
	$scaleWidth = $sliderThickness ;
	$scaleHeight = $CVA{"height"} ;
	}
    else
	{
    	$orientation = XmHORIZONTAL ;
        $direction = XmMAX_ON_RIGHT; 		# default
        $direction = XmMAX_ON_RIGHT   if ( $raw_direction eq "Ascending");
        $direction = XmMAX_ON_LEFT    if ( $raw_direction eq "Descending");
	$scaleWidth = $CVA{"width"} ;
	$scaleHeight = $sliderThickness ;
	}

## print "raw_orientation = $raw_orientation, raw_direction = $raw_direction\n";
## print "orientation = $orientation, direction = $direction\n";


# TBD -fix for EventHandler

    local($continued_event) = &Vm_Get ($viewVm, "${itemName}.continued_event");
    local($callback,$dragCallback);

    $callback = '! No callbacks generated';
    $dragCallback = '';

    local($minimum) = &Vm_Get ($viewVm, "${itemName}.minimum");
    $minimum = (defined $minimum) ? int($minimum) : 0;
    local($maximum) = &Vm_Get ($viewVm, "${itemName}.maximum");
    $maximum = (defined $maximum) ? int($maximum) : 0;
    local($scaleMultiple) = &Vm_Get ($viewVm, "${itemName}.scaleMultiple");
    $scaleMultiple = (defined $scaleMultiple) ? int($scaleMultiple) : 0;
    &addToPanel ("XmScale", $PanelItem);

    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_change";
        if ( $continued_event eq "Continuously")
	    {
            $procedureName = "${PanelItem}_chgdrg";
	    $dragCallback = "XmNdragCallback = procedure ${procedureName}();";
	    }
    	&generateCallback ($procedureName,"Scale") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNvalueChangedCallback=procedure ${procedureName}();
	$dragCallback
	$ExtraCalls
	};
end
	}

$Items .= <<end;

object $PanelItem : XmScale	! scale presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNtitleString     = $CVA{"title"};
	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};
	XmNhighlightThickness = $CVA{"highlight"};

	! Presentation Panel resources
	XmNorientation = $orientation;
	XmNprocessingDirection = $direction;
	XmNscaleHeight = $scaleHeight;
        XmNscaleWidth = $scaleWidth;
! decimalpts = $decimalPts;
	XmNminimum = $minimum;
	XmNmaximum = $maximum;
	XmNscaleMultiple = $scaleMultiple;
	XmNshowValue = $showValue;
	XmNvalue = $defaultValue;
	! invariant resources; 
	$ExtraArgs
	};
    $callback
    };
end
}

# 
#
#	checkbox
#
sub checkbox_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs
    local($labelsp) = &Vm_Get ($viewVm, "${itemName}.labelsp");
    $labelsp = 4 if (!defined $labelsp); 

	# determine whether checkbox is on/off
    local($type)  = &Vm_Type ($targetVm, $itemName);
    local($defaultValue) = &Vm_Get ($targetVm, $itemName);
    local($state) = "false";
    if ($type == 1 && $defaultValue == 1 )    {$state = "true";} ; # V_INTEGER
    if ($type == 2 && $defaultValue == 1.0 )  {$state = "true";} ; # V_REAL
    if ($type == 3 && $defaultValue eq "yes") {$state = "true";} ; # V_STRING

    &addToPanel ("XmToggleButton", $PanelItem);

    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_change";
    	&generateCallback ($procedureName,"ToggleButton") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNvalueChangedCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

$Items .= <<end;

object $PanelItem : XmToggleButton	! checkbox presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNlabelString     = $CVA{"title"};
	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	!NOTE: $TAEWPT/checkboxRt.cc appears to force highlightThickness to 0
	XmNhighlightThickness = 0;

	! Presentation Panel resources
	XmNspacing = $labelsp;
	XmNset = $state;

	! invariant resources; 
	XmNalignment    = XmALIGNMENT_BEGINNING;
	! XmNselectColor = color_black; ! NOTE: this is a best guess
	$ExtraArgs
	};
    $callback
    };
end
}



# 
#
#	scroller (aka color logger)
#
sub scroller_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;
    $usingLogger = 1;
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );
    $MrmInit{"Xterm"}++;
    &addToPanel ("user_defined", $PanelItem);
    #
    #	color processing
    #
    local(@color, $i, $c);
    for $i (0..7)
	{
	$c = &Vm_Get ($viewVm, "$itemName.color$i");
	$c = $momFG if (!defined($c) || $c eq "parent");
	$c = &parseColor ($c, $momFG, "scroller");
	@color[$i] = "color_$c";
	}
    #
    #	other type-specific resources
    # 
    local($jumpScroll) = &Vm_Get($viewVm, "$itemName.jumpscr");
    $jumpScroll = &mapBoolean($jumpScroll); 
    local($logFile) = &Vm_Get ($viewVm, "$itemName.logfile");
    if (defined $logFile)
	{$logFile = "XmNlogFile = '$logFile';"}
    else
	{$logFile = "";}
    local($logging) = &Vm_Get ($viewVm, "$itemName.logging");
    $logging = &mapBoolean($logging); 
    local($autoWrap) = &Vm_Get ($viewVm, "$itemName.wrap");
    $autoWrap = &mapBoolean($autoWrap); 
    local($saveLines) = &Vm_Get ($viewVm, "$itemName.savelns");
    $saveLines = 64 if (!defined $saveLines);
    local($scrollBar) = &Vm_Get ($viewVm, "$itemName.scrollb");
    $scrollBar = &mapBoolean($scrollBar); 
    local($scrollTtyOutput) = &Vm_Get ($viewVm, "$itemName.scrtty");
    $scrollTtyOutput = &mapBoolean($scrollTtyOutput); 
    local($visualBell) = &Vm_Get ($viewVm, "$itemName.visbell");
    $visualBell = &mapBoolean($visualBell); 

    local($callback); 
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_act";
    	&generateCallback ($procedureName,'','',$newPanel,$nextState,$curState) ;
	$callback = <<end;
callbacks{
	XmNactivateCallback = procedure $procedureName();
	};
end
	}

$Items .= <<end;

${XtermInclude}include file 'Xterm.uil';
object $PanelItem : user_defined procedure XtaeCreateXterm  ! scroller 
    {
    arguments
	{
	! Item Specification Panel resources
	XmNfont            = "$CVA{'font'}";	! a 'font name' not a font
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNcolor0          = $color[0]; 
	XmNcolor1          = $color[1];
	XmNcolor2          = $color[2];
	XmNcolor3          = $color[3];
	XmNcolor4          = $color[4];
	XmNcolor5          = $color[5];
	XmNcolor6          = $color[6];
	XmNcolor7          = $color[7];
	XmNjumpScroll      = $jumpScroll;
	$logFile
	XmNlogging         = $logging;
	XmNautoWrap        = $autoWrap;
	XmNsaveLines       = $saveLines;
	XmNscrollBar       = $scrollBar;
	XmNscrollTtyOutput = $scrollTtyOutput;
	XmNvisualBell      = $visualBell;
	$ExtraArgs
	};
    $callback
    $ExtraCalls
    };
end
$XtermInclude = "!"		# include one time only
}



# 
#
#	radio button bank 
#
#	TBD: arrow traversal not working
#
#
sub radio_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs
    local($defaultValue) = &Vm_Get($targetVm, $itemName);
	# Get choices
    local(@valids) = &Vm_GetValidString ($targetVm, $itemName); 
    grep (s/"/\\"/g, @valids);		# fix interior quotes
    local($validsList,$set,$num_valids,$rows,$orientation,$rowcol,$boxY);

    $num_valids = @valids;
    local($columns) = &Vm_Get ($viewVm, "${itemName}.columns");
    $columns = 1 if (!defined $columns || $columns <= 0);
    $rows = 1 + int(($num_valids - 1)/$columns);

    local($itemmargin) = &Vm_Get ($viewVm, "${itemName}.itemmargin");
    local($rowmargin) = &Vm_Get ($viewVm, "${itemName}.rowmargin");
    $itemmargin = 4 if (!defined $itemmargin) ;
    $rowmargin =  0 if (!defined $rowmargin) ; 

    # PR2941 - radio button spacing improved but not perfect.
    # "Compressed Columns" (layout qualifier) choice isn't handled well.
    local ($marginRight, $marginTop, $marginBottom, $highlight);
    $marginRight = $itemmargin ;
    # Must truncate division else float result is possible.
    $marginTop   = $marginBottom = int ( $rowmargin / 2); 
    # Highlight should be either 0 or 1 (depending on traversal), not 0 or 2.
    $highlight = 0;
    $highlight = 1 if ( $CVA{"traversal"} eq "true" );
    
    #
    #	With XmRadioBox, we cannot get exactly the TAE Plus
    #   arrangement (because we must go horizontal to get the
    #   same ordering).  If we use XmPACK_TIGHT packing, then 
    #	XmNnumColumns is ignored.
    #
    # Look for substring since variations possible for old .res files!
    local($raw_layout) = &Vm_Get ($viewVm, "${itemName}.layout");
    local($layout);
    $raw_layout =~ tr/A-Z/a-z/;		# for case insensitive compare...
    if ( index ( $raw_layout, "equally spaced" ) >= 0 )
		{ $layout = XmPACK_COLUMN ;}
    elsif ($rows == 1 && index ( $raw_layout, "compressed" ) >= 0 )
		{ $layout = XmPACK_TIGHT ;}
    else {
	 ## print "radio_Presentation: unexpected layout = $raw_layout\n";
	 $layout = XmPACK_COLUMN;
	 }
    &addToPanel ("XmForm", $PanelItem);
    local($callbackProc) = "${PanelItem}_entry"; # or _change ?
    &generateCallback ($callbackProc,"RowColumn") ;

    local($i) = 0;
    foreach $opt (@valids) {
	$i++;
	$set = ($opt eq $defaultValue) ? 
			"XmNset          = true" : "XmNset          = false" ;
        $validsList .=  <<end;
		${PanelItem}\$b$i : XmToggleButton 
		    {
		    arguments 
			{
			XmNlabelString  = "$opt";
			XmNfontList     = font_alias_$CVA{"font"};
			$set;
			XmNforeground   = color_$CVA{"fg"};
			XmNbackground   = color_$CVA{"bg"};
			XmNmarginRight  = $marginRight ;
			XmNmarginTop    = $marginTop ;
			XmNmarginBottom = $marginBottom ;
			XmNmarginHeight = 0 ;
			XmNmarginWidth  = 0 ;
			XmNhighlightThickness = $highlight ;
			};
		    };
end
	}
$validsList = &cleanInsert($validsList);
$Items .= <<end;

object $PanelItem : XmForm 		! radio presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	XmNmarginHeight = $CVA{"shadow"};
	XmNmarginWidth = $CVA{"shadow"};

	! Presentation Panel resources
	$ExtraArgs
	};
    controls
	{
	${PanelItem}\$label : XmLabel 
	    {
	    arguments 
		{
		XmNalignment       = XmALIGNMENT_BEGINNING ;
		XmNleftAttachment  = XmATTACH_FORM;
		XmNrightAttachment = XmATTACH_FORM;
		XmNtopAttachment   = XmATTACH_FORM;
		XmNlabelString     = $CVA{"title"}; 
		XmNfontList        = font_alias_$CVA{"font"};
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		};
	    };
	${PanelItem}\$radio : XmRadioBox
	    {
	    arguments
		{
		XmNtopAttachment   = XmATTACH_WIDGET;
		XmNtopWidget       = ${PanelItem}\$label;
		XmNbottomAttachment= XmATTACH_FORM;
		XmNleftAttachment  = XmATTACH_FORM;
		XmNrightAttachment = XmATTACH_FORM;

	    	XmNforeground      = color_$CVA{"fg"};
	    	XmNbackground      = color_$CVA{"bg"};
	    	XmNborderColor     = color_$CVA{"fg"};
	    	XmNborderWidth     = 0;
	    	XmNshadowThickness = 0;

		XmNnumColumns      = $rows;        ! note: "number of rows" here
		XmNpacking         = $layout; 
		XmNspacing         = 0;
		XmNorientation	   = XmHORIZONTAL; 
		XmNmarginHeight    = 0;
                XmNmarginWidth     = 0;
	    	};
	    controls
		{
		$validsList
		};
	    callbacks
		{
	        XmNentryCallback = procedure $callbackProc(); 
		};
	    };
	};
    };
end
}

# 
#
#	pulldown	(pulldown menu)
#
sub pulldown_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs

	# Title default is XmALIGNMENT_CENTER in the WorkBench
    local($mlabelalign) = &translateEnum ($viewVm, $itemName, "mlabelalign",
	"Center", "XmALIGNMENT_CENTER",		# TAE default for title
	"Left",   "XmALIGNMENT_BEGINNING",	
	"Right",  "XmALIGNMENT_END"
	);

	# Choice default is XmALIGNMENT_BEGINNING in the WorkBench
    local($choicealign) = &translateEnum ($viewVm, $itemName, "choicealign",
	"Left",   "XmALIGNMENT_BEGINNING",	# TAE default for choice
	"Center", "XmALIGNMENT_CENTER",		
	"Right",  "XmALIGNMENT_END"
	);

	# Get menu fg, bg, font 
	# NOTE use of $ItemForeground, $ItemBackground, not $momfg, $mombg.
    local($menu_fg) = &Vm_Get ($viewVm, "${itemName}.menu_fg");
          $menu_fg = $PanelFG if ( $menu_fg eq "Panel Foreground");
          $menu_fg = &parseColor ($menu_fg, $ItemForeground, $type);
    local($menu_bg) = &Vm_Get ($viewVm, "${itemName}.menu_bg");
          $menu_bg = $PanelBG if ( $menu_bg eq "Panel Background");
          $menu_bg = &parseColor ($menu_bg, $ItemBackground, $type);
    local($menu_font) = &Vm_Get ($viewVm, "${itemName}.menu_font");
          $menu_font = &parseFont ($menu_font, $momFont, $type);
    
    &addToPanel ("XmMenuBar", $PanelItem);

    local($callback) = '!No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_entry";
    	&generateCallback ($procedureName, "RowColumn") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNentryCallback=procedure ${procedureName}();
	};
end
	}

	# Get choices
    local(@valids) = &Vm_GetValidString ($targetVm, $itemName); 
    grep (s/"/\\"/g, @valids);		# fix interior quotes
    local($validsList) = "\n" ;

       ## Generate UIL objects for each choice pushbutton. Note that
       ## callbacks for each button could be generated here (for instance, to
       ## support connections), although we have opted to use a single
       ## XmNentryCallback for the entire pulldown.  
    foreach $opt (@valids) {
        $validsList .= 
"\t\tXmPushButton {arguments { \
\t\t\tXmNlabelString = \"$opt\"; \
\t\t\tXmNfontList    = font_alias_$menu_font ; \
\t\t\tXmNforeground  = color_$menu_fg ; \
\t\t\tXmNbackground  = color_$menu_bg ; \
\t\t\tXmNalignment   = $choicealign;  };};\n";
	}

$Items .= <<end;

object $PanelItem : XmMenuBar	! pulldown presentation
    {
    arguments
	{
	! Item Specification Panel resources
	! XmNlabelString     = \$CVA{"title"}; 		!! No UIL counterpart
	! XmNfontList        = font_alias_$CVA{"font"}; !! No UIL counterpart
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNmarginWidth	   = 0;
	XmNmarginHeight	   = 0;
	
	XmNmenuAccelerator = "<Key>Dummy";
	XmNpopupEnabled    = false;

	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources

	! invariant resources; 
	$ExtraArgs
	};
    controls
	{
	${PanelItem}\$button : XmCascadeButton 
	    { 
	    arguments
		{
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNlabelString     = $CVA{"title"};
		XmNfontList        = font_alias_$CVA{"font"};
		XmNalignment       = $mlabelalign;

		};
	    controls
		{
		XmPulldownMenu 
		    {
		    arguments
			{
			XmNentryAlignment = $choicealign;
			! None
			};
		    controls
			{
			$validsList
			};
		    $callback
		    };
		};
	    };
        };
    };
end
}


# 
#
#	option menu	
#
sub option_Presentation {

    local($taretVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# SpecialArgs

	# Adjust option menu widtha - TBD not exactly right, but better
	$CVA{"width"} += 2 * $CVA{"shadow"} + 2 * $CVA{"border"};

    local($choicealign) = &translateEnum ($viewVm, $itemName, "choicealign",
	"Left",   "XmALIGNMENT_BEGINNING",	# TAE default
	"Center", "XmALIGNMENT_CENTER",		
	"Right",  "XmALIGNMENT_END"
	);

	# TBD: $selalign is not used in generated UIL since XmOptionMenu 
        # doesn't appear to support alignment for the "current selection"
    local($selalign) = &translateEnum ($viewVm, $itemName, "selalign",
	"Left",   "XmALIGNMENT_BEGINNING",	# TAE default
	"Center", "XmALIGNMENT_CENTER",		
	"Right",  "XmALIGNMENT_END"
	);

	# Get menu fg, bg, font 
	# NOTE use of $ItemForeground, $ItemBackground, not $momfg, $mombg.
    local($menu_fg) = &Vm_Get ($viewVm, "${itemName}.menu_fg");
          $menu_fg = $PanelFG if ( $menu_fg eq "Panel Foreground");
          $menu_fg = &parseColor ($menu_fg, $ItemForeground, $type);
    local($menu_bg) = &Vm_Get ($viewVm, "${itemName}.menu_bg");
          $menu_bg = $PanelBG if ( $menu_bg eq "Panel Background");
          $menu_bg = &parseColor ($menu_bg, $ItemBackground, $type);
    local($menu_font) = &Vm_Get ($viewVm, "${itemName}.menu_font");
          $menu_font = &parseFont ($menu_font, $momFont, $type);
    
    &addToPanel ("XmBulletinBoard", $PanelItem);

    local($callback) = '!No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_entry";
    	&generateCallback ($procedureName, "RowColumn") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNentryCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

	# Get choices
    local(@valids) = &Vm_GetValidString ($targetVm, $itemName); 
    grep (s/"/\\"/g, @valids);		# fix interior quotes
    local($validsList) = "\n";

       ## Generate UIL objects for each pushbutton. Note that
       ## callbacks for each button could be generated here (for instance, to
       ## support connections), although we have opted to use a single
       ## XmNentryCallback for the entire pulldown.  
    foreach $opt (@valids) {
        $validsList .= 
"\t\tXmPushButton {arguments { \
\t\t\tXmNlabelString = \"$opt\"; \
\t\t\tXmNfontList    = font_alias_$menu_font ; \
\t\t\tXmNforeground  = color_$menu_fg ; \
\t\t\tXmNbackground  = color_$menu_bg ; \
\t\t\tXmNalignment   = $choicealign;  };};\n";
	}

$Items .= <<end;

object $PanelItem : XmBulletinBoard	! option presentation
    {
    arguments
	{
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNmarginWidth	   = 0;
	XmNmarginHeight	   = 0;
	XmNlabelFontList   = font_alias_$CVA{"font"};
	XmNbuttonFontList  = font_alias_$menu_font;
	};
    controls
	{
	${PanelItem}\$opt : XmOptionMenu
	    {
	    arguments
		{
		! Item Specification Panel resources
		XmNlabelString     = $CVA{"title"};
		XmNx               = 0;
		XmNy               = 0;
		XmNwidth           = $CVA{"width"};
		XmNheight          = $CVA{"height"};
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNborderColor     = color_$CVA{"fg"};
		XmNborderWidth     = $CVA{"border"};
		XmNshadowThickness = $CVA{"shadow"};
		XmNtraversalOn     = $CVA{"traversal"};
		XmNnavigationType  = $CVA{"navigation"};

		! invariant resources; TBD
		$ExtraArgs
		};
	    controls
		{
		XmPulldownMenu 
		  {
		  arguments
		    {
		    XmNforeground      = color_$menu_fg;
		    XmNbackground      = color_$menu_fg;
		    XmNborderWidth     = $CVA{"border"};
		    XmNshadowThickness = $CVA{"shadow"};
		    XmNlabelString     = "$valids[0]";
		    !XmNfontList        = font_alias_$CVA{"font"}; Not supported
		    !XmNalignment       = $selalign;  Not supported
		    };
		  controls
		    {
		    $validsList
		    };
		  $callback
		  };
		};
	    };
	};
    };
end
}
# 
#
#	text	(keyin)
#
sub text_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

	# Compute offsets for text component
    # PR2281
    local($textOffset) = $CVA{"shadow"};
    local($labelOffset) = $textOffset ;


	# SpecialArgs
	# set length based on default value
    local($type)  = &Vm_Type ($targetVm, $itemName);
    local($defaultValue) = &Vm_Get ($targetVm, $itemName);
    $defaultValue =~ s/"/\\"/g;		# fix interior quotes for UIL
    local($length);
    $length = length ("9999999999")   if ($type == 1);	# "integer"
    $length = length ("9999999.9999") if ($type == 2);	# "real"
    $length = length ($defaultValue)  if ($type == 3);	# "string"
    $length = 20                      if ($length == 0);

    &addToPanel ("XmForm", $PanelItem);

    local($callback) = '!No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($ProcedureName) = "${PanelItem}_text";
    	&generateCallback ($ProcedureName) ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNactivateCallback=procedure ${ProcedureName}();
	XmNlosingFocusCallback=procedure ${ProcedureName}();
	$ExtraCalls
	};
end
	}


$Items .= <<end;

object $PanelItem : XmForm	! text presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources
	};
    controls
	{
	${PanelItem}\$label : XmLabel
	    {
	    arguments
		{
		XmNtopAttachment = XmATTACH_FORM;
		XmNtopOffset	 = $labelOffset;
		XmNleftAttachment = XmATTACH_FORM;
		XmNleftOffset	 = $labelOffset;
		XmNlabelString = $CVA{"title"};
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNborderColor     = color_$CVA{"fg"};
		XmNfontList = font_alias_$CVA{"font"};
		};
	    };

	${PanelItem}\$text : XmText 
	    { 
	    arguments
		{
		XmNtopAttachment  = XmATTACH_FORM;
		XmNtopOffset	   = $textOffset;
		XmNleftAttachment  = XmATTACH_WIDGET;
		XmNleftWidget	   = ${PanelItem}\$label;
		XmNleftOffset      = 1;
		XmNrightAttachment = XmATTACH_FORM;
		XmNrightOffset     = $textOffset;
		XmNbottomAttachment = XmATTACH_FORM;
		XmNbottomOffset    = $textOffset;
		XmNforeground      = color_$CVA{"fg"};
		XmNbackground      = color_$CVA{"bg"};
		XmNborderColor     = color_$CVA{"fg"};
		XmNfontList = font_alias_$CVA{"font"};
		XmNhighlightThickness = $CVA{"highlight"};
		XmNmarginHeight	   = 1;

		XmNeditable = true;
		XmNcolumns = $length;
		XmNvalue = "$defaultValue";
		$ExtraArgs
		};
	    $callback
	    };
	};
    };
end
}

# 
#
#	workspace 
#
sub workspace_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );


    # Resources to control whether panel is scrollable and the
    # extent of its Work Region (of the Scrolled Window)
    local($sbDisplayPolicy) 	= &Vm_Get ($viewVm, "${itemName}.sbDisplayPolicy"); 
    local($WWWidth, $WWHeight) 	= &Vm_Get ($viewVm, "${itemName}.workwinSize"); 
    local($scrolledWin);
    
    if (!defined($sbDisplayPolicy) || $sbDisplayPolicy eq "None")
	{
	$scrolledWin = 'FALSE';
	$wspaceDAname = $itemName;
	$wspaceDAx = $CVA{"x"};
	$wspaceDAy = $CVA{"y"};
	$wspaceDAwidth = $CVA{"width"};
	$wspaceDAheight = $CVA{"height"};
        &addToPanel ("XmDrawingArea", $PanelItem);
	}
    else
	{
	$scrolledWin = 'TRUE';
	$wspaceDAname = "${itemName}_wspaceDA";
	$wspaceDAx = 0;
	$wspaceDAy = 0;
	$wspaceDAwidth = $WWWidth;
	$wspaceDAheight = $WWHeight;
        &addToPanel ("XmScrolledWindow", $PanelItem);
	if ($sbDisplayPolicy eq "Always")
	    {$itemScrollPolicy = 'XmSTATIC';}
	else
	    {$itemScrollPolicy = 'XmAS_NEEDED';}
	}

    # generate callback info
    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_input";
    	&generateCallback ($procedureName,"DrawingArea") ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNinputCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

$Items .= <<end;

object ${panel}_${wspaceDAname} : XmDrawingArea	! workspace presentation
    {
    arguments
	{
	! Item Specification Panel resources
	!	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $wspaceDAx;
	XmNy               = $wspaceDAy;
	XmNwidth           = $wspaceDAwidth;
	XmNheight          = $wspaceDAheight;
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources
	XmNresizePolicy = XmRESIZE_NONE;

	! invariant resources; 
	$ExtraArgs
	};
    $callback
    };
end

    if ($scrolledWin eq "TRUE")
	{
	$Items .= <<end;

object $PanelItem : XmScrolledWindow
    {
    arguments
        {
        XmNx               = $CVA{"x"};
        XmNy               = $CVA{"y"};
        XmNwidth           = $CVA{"width"};
        XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation panel scrolling resources
        XmNscrollBarDisplayPolicy = $itemScrollPolicy;
        XmNscrollingPolicy = XmAUTOMATIC;
        XmNscrolledWindowMarginWidth = 2;
        XmNscrolledWindowMarginHeight = 2;
        };
    controls
        {
        XmDrawingArea ${panel}_${wspaceDAname};
	$ExtraCons
	};
    };
end
	}
}


# 
#
#	dynamic text (label)
#
sub dynlabel_Presentation {

    local( $targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG)= @_;

    $Items .= <<end;
!   NOTE: dynlabel presentation is implemented as a static label
end
    &static_Presentation($targetVm, $viewVm,
			 $panel, $itemName, $momFont, $momFG, $momBG);
}
# 
#
#	static text (label)
#
sub static_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );

    ## Note that TAE Plus label must be event sensitive, so use 
    ## XmPushButton rather than XmLabel.

    ## NOTE - however if button, can't prevent reverse video on clicking
    ##       but if label, can't generate events


    local($widgetClass, $callback);
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_act";
    	&generateCallback ($procedureName,"PushButton") ;
	$widgetClass = "XmPushButton";
	$callback = "XmNactivateCallback = procedure ${procedureName}();";
	}
    else
	{
	$widgetClass = "XmLabel";
	$callback = "";
	}
    &addToPanel ($widgetClass, $PanelItem);

$Items .= <<end;

object $PanelItem : $widgetClass		! static presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNlabelString     = $CVA{"title"};
	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};
	XmNhighlightThickness = $CVA{"highlight"};

	! Presentation Panel resources; this pres. has none

	! invariant setting for TAE label
	XmNalignment          = XmALIGNMENT_BEGINNING ;
	$ExtraArgs
	};
    callbacks
	{
	$callback
	};
    };
end
}

# 
#
#	pageedit (Multi-Line Edit)
#
sub pageedit_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );
	# Compute offset for label placement
    local($labelOffset) = $CVA{"border"} + $CVA{"shadow"};

	# SpecialArgs

	# Add to list of items for this panel and generate callback if necess.
    &addToPanel ("XmForm", $PanelItem);

        ## NOTE: could optionally generate callbacks for each possible
        ##	     reason. Currently, only XmNmodifyVerify is used.
    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_txtedt";
    	&generateCallback ($procedureName) ;
	$callback = '';
	$callback .= <<end;
callbacks{
	XmNlosingFocusCallback=procedure ${procedureName}();
	XmNactivateCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}

	# Get strings to display (from view)
    local(@textstrs) = &Vm_Get ($targetVm, "${itemName}" ); 
	# Escape special characters
	# NOTE: Need to escape backslash too!
    grep (s#\n#\\\\n#g, @textstrs);
    grep (s#\r#\\\\r#g, @textstrs);
    grep (s#\t#\\\\t#g, @textstrs);
    grep (s#\f#\\\\f#g, @textstrs);
    grep (s/"/\\"/g, @textstrs);


	# join strings, inserting newlines
    local($textString) = join("\\n", @textstrs);
    if (length($textString) > $MAX_LITERAL)
        {
        print "$ERR: pageedit valids exceed maximum allowable length ($MAX_LITERAL) for string literal\n";
        print "${MsgInd} item = ${PanelItem}\n";
        }

	# parse into 48 character lines
    local($MAX_STRING_LINE) = 48;
    local(@shortstrs,$len,$skip);
    $len = length($textString);
    local ($ibegin, $iend);
    $ibegin = 0;
    while ($ibegin < $len) {
    	$iend = $ibegin + $MAX_STRING_LINE - 1;
	# check for escaped characters at end of line
        if (substr($textString,$iend,1) eq "\\")
	     {$skip = $MAX_STRING_LINE + 1;}
	else
	     {$skip = $MAX_STRING_LINE ;}

        push(@shortstrs, substr($textString, $ibegin, $skip));
        } continue {$ibegin += $skip;}


	# Convert to format that "XmNValue" accepts.
    local($textLines, $itemCount);
    $quote = "\"" ;
    $nl = "\\n";
    # insert quote before first item
    $shortstrs[0] = $quote . $shortstrs[0];
    # append quote after last item
    $shortstrs[$#shortstrs] .= $nl. $quote ;
    $textLines = join ("\"\n\t\t\t\&\"", @shortstrs) ;
    $itemCount = $#shortstrs + 1;
	

## print "\npageedit = @textstrs [COUNT = $itemCount]\n";
## print "\ntextLines = $textLines\n";

$Items .= <<end;

object $PanelItem : XmForm	! pageedit presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources
	};
    controls
	{
	${PanelItem}\$label : XmLabel
	    {
	    arguments
		{
		XmNtopAttachment = XmATTACH_FORM;
		XmNtopOffset	 = $labelOffset;
		XmNleftAttachment = XmATTACH_FORM;
		XmNleftOffset	 = $labelOffset;
		XmNrightAttachment = XmATTACH_FORM;
		XmNrightOffset	 = $labelOffset;
		XmNlabelString = $CVA{"title"};
		XmNfontList = font_alias_$CVA{"font"};
		XmNforeground = color_$CVA{"fg"};
		XmNbackground = color_$CVA{"bg"};
               	XmNalignment    = XmALIGNMENT_BEGINNING;
		};
	    };
	${PanelItem}\$text : XmScrolledText 
	    { 
	    arguments
		{
		XmNtopAttachment = XmATTACH_WIDGET;
		XmNtopWidget = ${PanelItem}\$label;
		XmNleftAttachment = XmATTACH_FORM;
		XmNrightAttachment = XmATTACH_FORM;
		XmNbottomAttachment = XmATTACH_FORM;
		XmNfontList = font_alias_$CVA{"font"};
		XmNforeground = color_$CVA{"fg"};
		XmNbackground = color_$CVA{"bg"};
		XmNvalue = $textLines;

		! invariant resources; 
		XmNeditable        = true;
		XmNeditMode        = XmMULTI_LINE_EDIT;
		$ExtraArgs
		};
	    $callback
	    };
	};
    };
end
}

# 
#
#	textdisp (text display)
#
sub textdisp_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );
	# Compute offset for label placement
    local($labelOffset) = $CVA{"border"} + $CVA{"shadow"};

	# SpecialArgs

	# Add to list of items for this panel
    &addToPanel ("XmForm", $PanelItem);

	# Determine text source and set strings or filespec
    local($textLines, $itemCount);
    local($filecbProcName,$fileCallback);
    local($filecbInitCode);
    if ($codeStyle eq $codeStyleEnhanced ) {
	$filecbInitCode = "XtaeTextDisplayInit(widget,(char *)client_data);";
	}

    local($fl_or_tx) = &Vm_Get ($viewVm, "${itemName}.fl_or_tx");
    if ($fl_or_tx eq "File") 
	{ 
        local($filename) = &Vm_Get ($viewVm, "${itemName}.filespec");
	$filecbProcName = "${panel}_${itemName}_create";
    	&generateCallback ($filecbProcName,'','string',$filecbInitCode);
	$fileCallback =
	 "MrmNcreateCallback = procedure ${filecbProcName}(\"${filename}\");";
	$textLines="\"\"";
	$itemCount = 0;
	}
    else
	{    
	   # Get strings to display (from view)
	local(@textstrs) = &Vm_Get ($viewVm, "${itemName}.textstrs" ); 

	   # Escape special characters
	   # NOTE: Need to escape backslash too!
	grep (s#\n#\\\\n#g, @textstrs);
	grep (s#\r#\\\\r#g, @textstrs);
	grep (s#\t#\\\\t#g, @textstrs);
	grep (s#\f#\\\\f#g, @textstrs);
	grep (s/"/\\"/g, @textstrs);


	   # join strings, inserting newlines
	local($textString) = join("\\n", @textstrs);
	if (length($textString) > $MAX_LITERAL)
	   {
	   print "$ERR: textdisp valids exceed maximum allowable length ($MAX_LITERAL) for string literal\n";
	   print "${MsgInd} item = ${PanelItem}\n";
	   }

	   # parse into 48 character lines
    	local($MAX_STRING_LINE) = 48;
    	local(@shortstrs,$len,$skip);
    	$len = length($textString);
    	local ($ibegin, $iend);
    	$ibegin = 0;
    	while ($ibegin < $len) {
            $iend = $ibegin + $MAX_STRING_LINE - 1;
            # check for escaped characters at end of line
            if (substr($textString,$iend,1) eq "\\")
                 {$skip = $MAX_STRING_LINE + 1;}
            else
                 {$skip = $MAX_STRING_LINE ;}

            push(@shortstrs, substr($textString, $ibegin, $skip));
            } continue {$ibegin += $skip;}

	   # Convert to format that "XmNValue" accepts.
	$quote = "\"" ;
	$nl = "\\n";
	   # insert quote before first item
	$shortstrs[0] = $quote . $shortstrs[0];
	   # append quote after last item
	$shortstrs[$#shortstrs] .= $nl. $quote ;
	$textLines = join ("\"\n\t\t\t\&\"", @shortstrs) ;
	$itemCount = $#shortstrs + 1;

	} # end text from textstrs

       # Generate callback if necess.
    local($labelCallback) = '! No callbacks generated';
    local($textCallback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_txtdsp";
    	&generateCallback ($procedureName) ;
#	$labelCallback = '';
#	$labelCallback .= <<end;
#callbacks{
#	XmNactivateCallback=procedure ${procedureName}();
#	};
#end
	$textCallback = '';
	$textCallback .= <<end;
callbacks{
	$fileCallback
	XmNfocusCallback=procedure ${procedureName}();
	$ExtraCalls
	};
end
	}


$Items .= <<end;

object $PanelItem : XmForm	! textdisp presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn        = $CVA{"traversal"};
	XmNnavigationType     = $CVA{"navigation"};

	! Presentation Panel resources
	};
    controls
	{
	${PanelItem}\$label : XmPushButton
	    {
	    arguments
		{
		XmNtopAttachment = XmATTACH_FORM;
		XmNtopOffset	 = $labelOffset;
		XmNleftAttachment = XmATTACH_FORM;
		XmNleftOffset	 = $labelOffset;
		XmNrightAttachment = XmATTACH_FORM;
		XmNrightOffset	 = $labelOffset;
		XmNlabelString = $CVA{"title"};
		XmNfontList = font_alias_$CVA{"font"};
		XmNforeground = color_$CVA{"fg"};
		XmNbackground = color_$CVA{"bg"};
		XmNhighlightThickness = 0;
		XmNshadowThickness = 0;
               	XmNalignment    = XmALIGNMENT_BEGINNING;
		};
	    };
	${PanelItem}\$text : XmScrolledText 
	    { 
	    arguments
		{
		XmNtopAttachment = XmATTACH_WIDGET;
		XmNtopWidget = ${PanelItem}\$label;
		XmNleftAttachment = XmATTACH_FORM;
		XmNrightAttachment = XmATTACH_FORM;
		XmNbottomAttachment = XmATTACH_FORM;
		XmNfontList = font_alias_$CVA{"font"};
		XmNforeground = color_$CVA{"fg"};
		XmNbackground = color_$CVA{"bg"};
		XmNvalue = $textLines;
		! invariant resources; 
		XmNeditable        = false;
		XmNeditMode        = XmMULTI_LINE_EDIT;
		$ExtraArgs
		};
	    $textCallback
	    };
	};
    };
end
}

# 
#
#	menubar
#
sub menubar_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    # Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName,
                        $momFont, $momFG, $momBG );

    # horizontal spacing between toplevel menu cascade buttons
    local($spacing) = &Vm_Get ($viewVm, "${itemName}.spacing");
    $spacing = 0 if (!defined $spacing);

    &addToPanel ("XmMenuBar", $PanelItem);

    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    local($MenubarCallback) = '! No callbacks generated';
    ## generate callbacks 
    if ( $eventGenerating ) 
	{
        local($procedureName) = "${PanelItem}_entry";
    	&generateCallback ($procedureName,"RowColumn") ;
	$MenubarCallback = '';
	$MenubarCallback .= <<end;
callbacks{ XmNentryCallback=procedure ${procedureName}(); };
end
	}

    $helpCascade = "";
    $MENUDEPTH = 0;
    $MENUTABBING = '';
       ### Recursively generate UIL for menu entries, adding
       ### sub-widgets to list as they are encountered
    &MenuEntryCascadeArgs(
		$targetVm, $viewVm, $panel, $itemName,
		$CVA{"font"}, $CVA{"fg"}, $CVA{"bg"},
		$MENUTABBING,$MENUDEPTH
		);

    ### Append the opening UIL for the top level widget
    &menubarUILopen($helpCascade);

    $Items .= $MenuEntryItems;
	
       ### Append the closing UIL for the top level widget
    &menubarUILclose($callback);

       ## Clear menu entry items list
    $MenuEntryItems = "";
}

#
sub icon_Presentation {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

    	# Handle common resources (fg,bg,title,font,size, etc.)
    &CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
			$momFont, $momFG, $momBG );
    local($iconOffset) = $CVA{"border"} + $CVA{"shadow"};

    &TAEhelp ($viewVm, $itemName, $panel);

	# SpecialArgs: icon size, location and pixmap file, label
	#TBD: Compute icon boundaries and placement, label placement
	#TBD: and icon pixmap file name
	local($iconwidth, $iconheight)= &Vm_Get ($viewVm, "${itemName}.mapsize");
	local($iconrotate) = &Vm_Get ($viewVm, "${itemName}.rotate");

	   # Center the icon horizontally
	local($iconX) =
	     int( ($CVA{"width"} - $CVA{"shadow"} - $CVA{"highlight"} -
		  $iconwidth + 1)/2 );

        if ($ENV{DEBUG_ICONS} == 1)
	    {
	    print "icon_Presentation: iconX  = $iconX computed from\n";
	    print "\twidth =",$CVA{"width"},"\n";
	    print "\tborder =",$CVA{"border"},"\n";
	    print "\tshadow =",$CVA{"shadow"},"\n";
	    print "\thighlight =",$CVA{"highlight"},"\n";
	    print "\ticon width = $iconwidth\n";
	    }
	   ## Extract the label info and compute vertical position
	local($labelfg,$labelbg);

	# NOTE use of $ItemForeground, $ItemBackground, not $momfg, $mombg.
	$labelfg = &Vm_Get ($viewVm, "${itemName}.labelfg");
	$labelfg = $CVA{"fg"} if ( !defined ($labelfg) );
	$labelfg = &parseColor ($labelfg, $ItemForeground, "icon");

	$labelbg = &Vm_Get ($viewVm, "${itemName}.labelbg");
	$labelbg = $CVA{"bg"} if ( !defined ($labelbg) );
	$labelbg = &parseColor ($labelbg, $ItemBackground, "icon");

	   #TBD: change to subroutine call
	   ##
	   ## Save expanded bitmap file name, or if no file name,
	   ## convert bitmap array to local pixmap and color table.
	   ##
	# &getPixmap($viewVm, ${itemName}.iconfile","${panel}_${itemName}");
	# Get bitmap file name and add it to the pixmap associative array
	local($iconLabelPixmap,$iconPixmapName);
	local($iconBitmapFG,$iconBitmapBG);
	local($iconfile) = &Vm_Get ($viewVm, "${itemName}.iconfile");
	local ($exists) ;

	# Does the (possibly expanded) file exist?
	if (defined($iconfile) && $iconfile ne "")
	    {
	    # stat() expands env var.
	    # Any info from stat implies existence.
	    $exists = (stat ($iconfile))[0];   
	    }

        if ( defined ( $exists ) )
	    {
	    ### Expand initial environment variable if necces.
	    ### $iconfile = &resolveFilePathname($iconfile);
	    $iconPixmapName = "pixmap_${panel}_${itemName}";
            $PixmapVarAssoc { $iconPixmapName } =
		 "\t${iconPixmapName}:  xbitmapfile('$iconfile');" ;
	    }
	else ## No file, so use integers stored in resfile
	    {
	    local($iconColorTableName) = "clrtbl_${panel}_${itemName}";
	    local($iconBitmapColorTable) =
	     $iconColorTableName.": color_table(color_$fg='o',\t\t\t\t\tcolor_$bg=' ');";
	    local($iconBitmap) = &parseIconBitmap($viewVm,$itemName);
	    $iconPixmapName = "bitmap_${panel}_${itemName}";
	    $IconBitmapVarAssoc { $iconPixmapName } .= <<end;
	$iconBitmapColorTable
	${iconPixmapName}:icon(color_table=${iconColorTableName},${iconBitmap});
end
	     }

    &addToPanel ("XmForm", $PanelItem);

	# Extract connection info
    local($newPanel)= &Vm_Get ($targetVm, "${itemName}._newPan");
    local($nextState) = &Vm_Get ($targetVm, "${itemName}._nextState");
    local($curState) = &Vm_Get ($targetVm, "${itemName}._currState");
    local($codeFrag) = &Vm_Get ($targetVm, "${itemName}._code");

    local($callback) = '! No callbacks generated';
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);
    if ( $eventGenerating ) 
        {
        local($procedureName) = "${PanelItem}_act";
    	&generateCallback ($procedureName,"PushButton",'',$codeFrag,
			   $newPanel,$nextState,$curState) ;
	$callback = '';
	$callback .= <<end;
callbacks{
	        XmNactivateCallback=procedure ${procedureName}();
	        $ExtraCalls
	        };
end
        }

    if ($ENV{DEBUG_ICONS} == 1)
	{
	 print "icon_Presentation: title = \'",$CVA{"title"},"\'\n";
	 print "		   length = ",length($CVA{"title"}),"\n";
	}

	##
	## If icon has a label, generate a pushbutton widget for the label,
	## and suppress the highlight, shadow and bottom attachment for the
	## icon widget. If no label, allow icon shadow and highlight, and
	## attach the icon bottom to the form.
	##
    local($labelButton);
    local($pictureBottomAttachment);
    local($iconHighlightThickness,$iconShadowThickness);
    if ( length($CVA{"title"}) > 2) {
	$iconHighlightThickness = 0;
	$iconShadowThickness = 0;
        $labelButton .= <<end;
	${PanelItem}\$label : XmPushButton
            {
            arguments
               {
		XmNtopAttachment      = XmATTACH_WIDGET;
		XmNtopWidget          = ${PanelItem}\$icon;
		XmNleftAttachment     = XmATTACH_FORM;
		XmNrightAttachment    = XmATTACH_FORM;
		XmNbottomAttachment   = XmATTACH_FORM;
		XmNbottomOffset       = $iconOffset;
		XmNleftOffset         = $iconOffset;
		XmNrightOffset        = $iconOffset;
		XmNfontList           = font_alias_$CVA{"font"};
		XmNforeground         = color_$labelfg;
		XmNbackground         = color_$labelbg;
		XmNborderColor        = color_$labelbg;

		XmNlabelType          = XmSTRING;
		XmNlabelString        = $CVA{"title"};
		XmNalignment          = XmALIGNMENT_CENTER;
		XmNborderWidth        = 0;
		XmNshadowThickness    = 0;
		XmNhighlightThickness = 0;
                };
	    $callback
            };
end
	}
    else{
	$pictureBottomAttachment = "XmNbottomAttachment  = XmATTACH_FORM;";
	$pictureBottomAttachment .= "\n\t\tXmNbottomOffset = ${iconOffset};";
	$iconHighlightThickness = $CVA{"highlight"};
	$iconShadowThickness = $CVA{"shadow"};
	}

	##
	## If icon does not take up the full width, generate left and
	## right filler pushbuttons to maintain the container widget
	## background (otherwise the icon background color would be
	## used.
	##
    local($iconRightAttachment,$iconLeftAttachment);
    local($leftFillerButton, $rightFillerButton);
    if ($iconX > 5 && length($CVA{"title"}) > 2) {
	   # generate left filler
	$leftFillerButton .= <<end;
	${PanelItem}\$lfill: XmPushButton
	    {
	    arguments
		{
		XmNwidth              = $iconX;
		XmNheight             = $iconheight;
		XmNforeground         = color_$labelbg;	!color_$CVA{"fg"}; 
		XmNbackground         = color_$labelbg;	!color_$CVA{"fg"}; 
		XmNtopAttachment      = XmATTACH_FORM;
		XmNtopOffset	      = $iconOffset;
		XmNleftAttachment     = XmATTACH_FORM;
		XmNleftOffset	      = $iconOffset;
		$pictureBottomAttachment
		XmNborderWidth        = 0;
		XmNshadowThickness    = 0;
		XmNhighlightThickness = 0;
		};
	    $callback
	    };
end
	   # set icon left attachment, let icon right attachment float
        $iconLeftAttachment = "XmNleftAttachment  = XmATTACH_WIDGET;\n";
	$iconLeftAttachment .= "\t\tXmNleftWidget = ${PanelItem}\$lfill;\n";

	   # generate right filler
	$rightFillerButton .= <<end;
	${PanelItem}\$rfill: XmPushButton
	    {
	    arguments
		{
		XmNwidth              = $iconX;
		XmNheight             = $iconheight;
		XmNforeground         = color_$labelbg;	!color_$CVA{"fg"}; 
		XmNbackground         = color_$labelbg;	!color_$CVA{"fg"}; 
		XmNtopAttachment      = XmATTACH_FORM;
		XmNtopOffset          = $iconOffset;
		XmNleftAttachment     = XmATTACH_WIDGET;
		XmNleftWidget         = ${PanelItem}\$icon;
		XmNrightAttachment    = XmATTACH_FORM;
		XmNrightOffset        = $iconOffset;
		$pictureBottomAttachment
		XmNborderWidth        = 0;
		XmNshadowThickness    = 0;
		XmNhighlightThickness = 0;
		};
	    $callback
	    };
end
	}
    else {
	$iconLeftAttachment ="XmNleftAttachment  = XmATTACH_FORM;";
	$iconLeftAttachment .= "\n\t\tXmNleftOffset = $iconOffset;";
	$iconRightAttachment ="XmNrightAttachment  = XmATTACH_FORM;";
	$iconRightAttachment .= "\n\t\tXmNrightOffset = $iconOffset;";
	}

	##
	## Now that the components are defined, generate the UIL for the
	## icon presentation.
	##
$Items .= <<end;
object $PanelItem : XmForm	! icon presentation
    {
    arguments
	{
	! Item Specification Panel resources
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$labelfg;
	XmNbackground      = color_$labelbg;
	XmNborderColor     = color_$labelfg;
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};
	XmNtraversalOn     = $CVA{"traversal"};
	XmNnavigationType  = $CVA{"navigation"};

	! Presentation Panel resources
	XmNresizePolicy = XmRESIZE_GROW;
	};
    controls
	{
$leftFillerButton
	${PanelItem}\$icon : XmPushButton
	    {
	    arguments
		{
		XmNtopAttachment      = XmATTACH_FORM;
		XmNtopOffset          = $iconOffset;
		$iconLeftAttachment
		$iconRightAttachment 
		$pictureBottomAttachment
		XmNforeground         = color_$CVA{"fg"};
		XmNbackground         = color_$CVA{"bg"};
		XmNborderColor        = color_$labelbg;	!color_$CVA{"fg"}; 
		XmNlabelType          = XmPIXMAP;
		XmNlabelPixmap        = $iconPixmapName;
		XmNalignment          = XmALIGNMENT_CENTER;
		XmNborderWidth        = 0;
		XmNshadowThickness    = $iconShadowThickness;
		XmNhighlightThickness = $iconHighlightThickness;
		XmNmarginHeight       = 0;
		XmNmarginWidth        = 0;
		$ExtraArgs
		};
	    $callback
	    };
$rightFillerButton
$labelButton
	};
   };
end
}

#
#	discretePictures: one DDOArea and N DiscretePictures sub-widgets 
#
sub discrete_Presentation 
    {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) 		= @_;
    $usingDDO = 1;
    $MrmInit{"DiscretePictures"}++;
    &ddoAreaCommon(@_);		# make DDOArea widget; and now sub-widgets:
    local($procName);
    #
    #	note: preLoad no longer a resource for the discrete DDO.
    #
    local($preload) = &translateEnum ($viewVm, $itemName, "preload",
	"At Startup", "XtaeLOAD_AT_STARTUP",
	"As Needed", "XtaeLOAD_AS_NEEDED"
	);
    local($discreteThresholdTableStr) = 
	&discreteThresholdStr ($viewVm, $itemName);
    local(@values) = &Vm_Get($targetVm, $itemName);
    local($d);

# PR2714 Shorter Discrete filename on Concurrent due to 14 character limit.
# Note that DiscretePictures.uil is a symbolic link to DiscPict.uil.
    if ( $ENV{'TAEPLAT'} eq "rtu" )
        {
        $Items .=
        "${DiscreteIncludeFirstTime}include file 'DiscPict.uil';\n";
        }
    else
        {
        $Items .=
        "${DiscreteIncludeFirstTime}include file 'DiscretePictures.uil';\n";
        }


    local($callback, $ddoArgs, $extraCalls, $name); 
    foreach $d (1..$#values+1)		# one gadget for each target value
	{
        $ddoArgs = &ddoCommon ($targetVm, $viewVm, $itemName, $d); 
	$name = "${PanelItem}\$d${d}";
	$extraCalls = &cleanInsert($Insertion{"$panel.$name.calls"});
	$Items .= <<end

object $name : user_defined procedure XtaeCreateDiscretePictures
    {
    arguments
	{
	$ddoArgs
	XtaeNdiscreteThresholdTableStr = $discreteThresholdTableStr
	    ;
	};
    callbacks
	{
	$callback
	$extraCalls
	};
    };
end
	}

$DiscreteIncludeFirstTime = "!";		# commented from now on
}

#
#	mover: one DDOArea and N Mover sub-widgets 
#
sub mover_Presentation 
    {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) 		= @_;
    $usingDDO = 1;
    $MrmInit{"Mover"}++;
    &ddoAreaCommon(@_);		# make DDOArea widget; and now sub-widgets:
    local($procName) ;

    # NOTE: 2D mover is unsupported in v5.3 (doc'd in res2uil man page)
    local($direction) = &translateEnum ($viewVm, $itemName, "mdir",
	"Vertical",   "XtaeVERTICAL",
	"Horizontal", "XtaeHORIZONTAL",
	"Both (2D)", "UNSUPPORTED"
	);
    if ( $direction eq "UNSUPPORTED" )
	{
    	print "$WARN: 2D Mover is unsupported: item $itemName, panel $panel.\n";
	print "$MsgInd Setting mover direction to default: XtaeVERTICAL.\n";
	$direction = XtaeVERTICAL;
	}

    local(@values) = &Vm_Get($targetVm, $itemName);
    local($d);
    $Items .= "${MoverIncludeFirstTime}include file 'Mover.uil';\n";
    local($callback, $ddoArgs, $extraCalls, $name); 
    foreach $d (1..$#values+1)		# one gadget for each target value
	{
        $ddoArgs = &ddoCommon ($targetVm, $viewVm, $itemName, $d); 
	$name = "${PanelItem}\$d${d}";
	$extraCalls = &cleanInsert($Insertion{"$panel.$name.calls"});
	$Items .= <<end

object $name : user_defined procedure XtaeCreateMover
    {
    arguments
	{
	$ddoArgs
	XtaeNmovementDirection = $direction;
	};
    callbacks
	{
	$callback
	$extraCalls
	};
    };
end
	}

$MoverIncludeFirstTime = "!";		# commented from now on
}

#
#	rotator : one DDOArea and N Rotator sub-widgets 
#
sub rotator_Presentation 
    {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) 		= @_;
    $usingDDO = 1;
    $MrmInit{"Rotator"}++;
    &ddoAreaCommon(@_);		# make DDOArea widget; and now sub-widgets:
    local($procName) ;
    local($direction) = &translateEnum ($viewVm, $itemName, "rdir",
	"Clockwise",        "XtaeCLOCKWISE",
	"CounterClockwise", "XtaeCOUNTER_CLOCKWISE"
	);
    local($maximumAngle) = &Vm_Get($viewVm, "${itemName}.rotang");

    local($hrotpt) = &translateEnum ($viewVm, $itemName, "hrotpt", 
	"Right",  "RIGHT",
	"Left",   "LEFT",
	"Center", "CENTER"
	);
    local($vrotpt) = &translateEnum ($viewVm, $itemName, "vrotpt", 
	"Center",  "CENTER",
	"Top",     "TOP",
	"Bottom",  "BOTTOM"
	);
    local($pivotPoint) = "Xtae${vrotpt}_${hrotpt}";

    local(@values) = &Vm_Get($targetVm, $itemName);
    local($d);
    $Items .= "${RotatorIncludeFirstTime}include file 'Rotator.uil';\n";
    local($callback, $ddoArgs, $extraCalls, $name); 
    foreach $d (1..$#values+1)		# one gadget for each target value
	{
        $ddoArgs = &ddoCommon ($targetVm, $viewVm, $itemName, $d); 
	$name = "${PanelItem}\$d${d}";
	$extraCalls = &cleanInsert($Insertion{"$panel.$name.calls"});
	$Items .= <<end

object $name : user_defined procedure XtaeCreateRotator
    {
    arguments
	{
	$ddoArgs
	XtaeNmaximumAngle = float($maximumAngle);
	XtaeNpivotPoint = $pivotPoint; 
	XtaeNrotationDirection = $direction;
	};
    callbacks
	{
	$callback
	$extraCalls
	};
    };
end
	}

$RotatorIncludeFirstTime = "!";		# commented from now on
}

#
#	stretcher: one DDOArea and N Stretcher sub-widgets 
#
sub stretcher_Presentation 
    {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) 		= @_;
    $usingDDO = 1;
    $MrmInit{"Stretcher"}++;
    &ddoAreaCommon(@_);		# make DDOArea widget; and now sub-widgets:
    local($procName) ;
    local($direction) = &translateEnum ($viewVm, $itemName, "sdir",
	"Vertical",   "XtaeVERTICAL",
	"Horizontal", "XtaeHORIZONTAL"
	);
    local(@values) = &Vm_Get($targetVm, $itemName);
    local($d);
    $Items .= "${StretcherIncludeFirstTime}include file 'Stretcher.uil';\n";
    local($callback, $ddoArgs, $extraCalls, $name); 
    foreach $d (1..$#values+1)		# one gadget for each target value
	{
        $ddoArgs = &ddoCommon ($targetVm, $viewVm, $itemName, $d); 
	$name = "${PanelItem}\$d${d}";
	$extraCalls = &cleanInsert($Insertion{"$panel.$name.calls"});
	$Items .= <<end

object $name : user_defined procedure XtaeCreateStretcher
    {
    arguments
	{
	$ddoArgs
	XtaeNstretchDirection = $direction;
	};
    callbacks
	{
	$callback
	$extraCalls
	};
    };
end
	}

$StretcherIncludeFirstTime = "!";		# commented from now on
}

#
#	stripchart: one DDOArea and N Stripchart sub-widgets 
#
#	NOTE: "X" here disables the "stripchart" presentation type.
#
sub Xstripchart_Presentation 
    {
    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) 		= @_;
    $usingDDO = 1;
    $MrmInit{"Stripchart"}++;
    &ddoAreaCommon(@_);		# make DDOArea widget; and now sub-widgets:
    local($procName) ;
    local($updateInterval) = &Vm_Get($viewVm, "${itemName}.interval");
    local($updatePolicy) = &Vm_Get($viewVm, "${itemName}.on");
    $updatePolicy = "??TBD??";
    local($numSegments) = &Vm_Get($viewVm, "${itemName}.segments");
    local(@values) = &Vm_Get($targetVm, $itemName);
    local($d);
    $Items .= "${StripchartIncludeFirstTime}include file 'Stripchart.uil';\n";
    local($callback, $ddoArgs, $extraCalls, $name); 
    foreach $d (1..$#values+1)		# one gadget for each target value
	{
        $ddoArgs = &ddoCommon ($targetVm, $viewVm, $itemName, $d); 
	$name = "${PanelItem}\$d${d}";
	$extraCalls = &cleanInsert($Insertion{"$panel.$name.calls"});
	$Items .= <<end

object $name : user_defined procedure XtaeCreateStripchart
    {
    arguments
	{
	$ddoArgs
	! TBD: XtaeNupdatePolicy = $updatePolicy;
	XtaeNnumSegments = $numSegments;
	XtaeNupdateInterval = $updateInterval;
	};
    callbacks
	{
	$callback
	$extraCalls
	};
    };
end
	}

$StripchartIncludeFirstTime = "!";		# commented from now on
}

#
#	generate object for DDOArea 
#
sub ddoAreaCommon
{
local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;
&CommonViewArgs ($targetVm, $viewVm, $panel, $itemName, 
		    $momFont, $momFG, $momBG );
&addToPanel ("user_defined", $PanelItem);
local($pictureFileName) = &Vm_Get($viewVm, "${itemName}.file");
local(@values) = &Vm_Get($targetVm, $itemName);
local($d);
local($controlsList);		# list of sub-widgets (DDOGadget's)
foreach $d (1..$#values+1)	# one DDOGadget for each target value:
    {
    $controlsList .= "\tuser_defined ${PanelItem}\$d$d;\n";
    }
$controlsList = &cleanInsert($controlsList);	# clean up for insert 

local($callback) = 
    &makeCallback ($targetVm, $panel, $itemName, "XmNinputCallback", "input");

$Items .= <<end;

${DDOAreaIncludeFirstTime}include file 'DDOArea.uil';
${DDOIncludeFirstTime}include file 'DDO.uil';

object $PanelItem : user_defined procedure XtaeCreateDDOArea
    {
    arguments
	{
	! Item Specification Panel resources
	XmNfontList        = font_alias_$CVA{"font"};
	XmNx               = $CVA{"x"};
	XmNy               = $CVA{"y"};
	XmNwidth           = $CVA{"width"};
	XmNheight          = $CVA{"height"};
	XmNforeground      = color_$CVA{"fg"};
	XmNbackground      = color_$CVA{"bg"};
	XmNborderColor     = color_$CVA{"fg"};
	XmNborderWidth     = $CVA{"border"};
	XmNshadowThickness = $CVA{"shadow"};

        XtaeNpictureFileName = "$pictureFileName";
        XtaeNareaTitle = $CVA{"title"} ;
	$ExtraArgs
	};
    controls
	{
	$controlsList
	$ExtraCons
	};
    callbacks
	{
	$callback
	$ExtraCalls
	};
    };
end

$DDOAreaIncludeFirstTime = "!";		# commented out from now on
$DDOIncludeFirstTime = "!";		# commented out from now on
}


#
#	common handling for ddo resources.
#	returns string for args section. 
#
#	Note: if only one value, then there's only
#	one dynamic and we set its name to "dynamic"
#	rather than "dynamic1".
#
#
sub ddoCommon
{
local($tarVm, $viewVm, $itemName, $number) = @_;
$MrmInit{"DDOArea"}++;
$MrmInit{"DDO"}++;
local($type) = &Vm_Get($viewVm, "${itemName}._TYPE");
local(@values) = &Vm_Get($tarVm, $itemName);
local($currentValue) = $values[$number-1];
local($updateDelta) = &Vm_Get($viewVm, "${itemName}.delta");
local($minimum) = &Vm_Get($viewVm, "${itemName}.min");
local($maximum) = &Vm_Get($viewVm, "${itemName}.max");

#
#	Currently, there are no corresponding TAE Plus resouces
#	so we don't set the maxFormat and minFormat uil values.
#	This code might be used in the future. 
#
local($maxFormat) = &Vm_Get($viewVm, "${itemName}.maxLabel");
local($minFormat) = &Vm_Get($viewVm, "${itemName}.minLabel");
$maxFormat = "" if (! $maxFormat);
$minFormat = "" if (! $minFormat);

local($valueDisplayEnable) = 
    (&Vm_Get($viewVm, "${itemName}.venable") eq "Enabled") ? "true" : "false";
local($valueDisplayFormat) = &Vm_Get($viewVm, "${itemName}.vformat");
local($pictureName) = ($#values == $[) ? "dynamic" : "dynamic$number";
local($thresholdTableStr, $thresholdSpec); 
if ($type eq "discrete")
    { 
    $thresholdSpec = "";		# discrete has its own table format
    }
else
    {
    $thresholdTableStr = &thresholdStr($viewVm, $itemName);
    $thresholdSpec = &cleanInsert(<<end); 
	XtaeNthresholdTableStr = $thresholdTableStr
	    ;
end
    # print "$itemName: thresholdSpec = $thresholdSpec\n";
    }

# Unsupported features in v5.3 (documented as RESTRICTIONS in res2uil man page)
# We could check for report_events too, but no necessary.
local($inputEnabled) = &Vm_Get($viewVm, "${itemName}.input_enable");
if (defined ($inputEnabled) && $inputEnabled )
    {
    print "$WARN: DDO input is unsupported: item $itemName, panel $panel.\n";
    }

return &cleanInsert(<<end);
	XtaeNcurrentValue = float($currentValue);
	XtaeNmaximum = float($maximum);
	XtaeNminimum = float($minimum);
	XtaeNpictureName = "$pictureName";
	$thresholdSpec
	XtaeNupdateDelta = float($updateDelta);
	XtaeNvalueDisplayEnable = $valueDisplayEnable;
	XtaeNvalueDisplayFormat = "$valueDisplayFormat";
end
}


#
#	generate threshold string from
#	rangeval and ranges qualifiers
#
sub thresholdStr
    {
    local($viewVm, $itemName) = @_;
    local($momColor) = &Vm_Get ($viewVm, "_panel.fg");
    local(@colors) = &Vm_Get($viewVm, "${itemName}.rangeval");
    local(@values) = &Vm_Get($viewVm, "${itemName}.ranges");
    # print "thresoldStr for $itemName high index = $#values\n";
    return "''" if ($#colors < $[ || $#values < $[);	# empty vectors? 
    local($str) = "\n";
    local($separator) = " ";
    local($i, $c, $v);
    foreach $i ($[..$#values)
	{
	$c = $colors[$i] ? $colors[$i] : "black" ;    # in case uneven vectors
	$c = &parseDDOColor ($c, $momColor);          # convert to "#" if nec
	$v = $values[$i] ? $values[$i] : 0.0;
	$str .= qq!\t $separator "$v '$c' "\n!;
	$separator = "&";
	}
    chop $str;			# remove last newline
    return $str;
    }

#
#	generate discrete threshold string from
#	qualifiers
#
sub discreteThresholdStr
    {
    local($viewVm, $itemName) = @_;
    # print "discreteThresoldStr for $itemName\n";
    local(@pictures) = &Vm_Get($viewVm, "${itemName}.rangeval");
    local(@values) = &Vm_Get($viewVm, "${itemName}.ranges");
    return "''" if ($#pictures < $[ || $#values < $[);	# empty vectors? 
    local($str) = "\n";
    local($separator) = " ";
    local($i, $p, $v);
    foreach $i ($[..$#values)
	{
	$p = $pictures[$i] ? $pictures[$i] : "DEFAULT" ;    # in case uneven 
	$v = $values[$i] ? $values[$i] : 0.0;
	$str .= qq!\t $separator "$v '$p' "\n!;
	$separator = "&";
	}
    chop $str;			# remove last newline
    return $str;
    }
#
#	The MrmInit associative array has keys that correspondg
#	to Mrm calls that must be made (once) in the generated C
#	module.   Here we produce the actual C code and return
#	the string.
#
# NOTE: $MrmInit{"widgetname"} must match assumptions in $TAEXTAE/mrm.pl.
#
sub MrmInitCalls
    {
    local($key, $mrmCalls);
    foreach $key (sort keys %MrmInit)
	{$mrmCalls .= "    Xtae${key}MrmInit();\n";}
    return &cleanInsert($mrmCalls);
    }

#
#
sub mapBoolean
    {
    local($bool) = @_;
    return $bool ? "true" : "false"; 	# note: undefined goes to "false"
    }

#
#	remove leading whitespace and final newline
#	so that substituting in a template is pretty
#	(i.e., leading indentation and final newline
#	already supplied).
#
sub cleanInsert 
    {
    local($str) = @_;
    $str =~ s/^\s+//;			# remove leading whitespace
    local($lastc) = chop $str;		# remove final newline
    $str .= $lastc if ($lastc ne "\n");	# whoops, maybe not newline
    return $str;
    }

sub getResFileTime
    {
    local($resFile) = @_;
    local($mtime) = (stat($resFile))[9];       # time file last modified
    local($sec,$min,$hour,$mday,$mon,$year) = localtime($mtime);
    $mon++;
    return "$mon/$mday/$year $hour:$min.";
    }

#
#	clean all values in the Insertion associative array. 
#
sub cleanUserInsertions
    {
    local($key);
    foreach $key (keys %Insertion)
	{
	$Insertion{$key} = &cleanInsert($Insertion{$key});
	}
    }

#	getTitle:
#
#	Converts a title (typically for XmNlabelString) 
#	to something suitable for UIL, i.e., 
#	newlines and interior quotes are handled.
#	The returned string has surrounding quotes,
#	ready for substituting in UIL argument spec.
#
#	If qualName is undef, then we use the $itemName
#	unqualified.
#
sub getTitle 
    {
    local($viewVm, $itemName, $qualName) = @_;
    local($name) = (defined $qualName) ? "$itemName.$qualName" : $itemName;
    local(@raw_title_vec) = &Vm_Get ($viewVm, $name);
    local($raw_title) = join ("\\n", @raw_title_vec);	# each element is a line
    $raw_title =~ s/"/\\"/g;				# escape interior quotes
    local(@multiLineTitle) = split (/\\n/, $raw_title );
    local($line, $title);
    local($lastLine) = pop(@multiLineTitle);		# special handling 
    for $line (@multiLineTitle)				# all except last line
	{
	$title .= <<end;
		compound_string("$line", separate=true) &
end
	}
    if ($title)						# multi-line title?
	{
    $title .= <<end;
		compound_string("$lastLine", separate=false)
end
	}
    else
	{$title = '"' . $lastLine .  '"';}		# single-line title
    # print "title = '$title'\n";
    return &cleanInsert($title);
    }

#
# getPixmap -- gets pixmap for from file or integer array and 
#	       appends to pixmap/bitmap associative array.
#	       returns pixmap identifier.
#	       
sub getPixmap{
 #TBD
}

#
#	TAEhelp:
#
#	Handles presentations which have the "helpsem" (help semantics)
#	qualifier. [v5.3 set: button, icon, filesel, and message].
#	Menubar also has a "taehelp" entry, but the handling is different.
#
#	NOTE: TAE Help mechanism is unsupported (doc'd in res2uil man page)
#
sub TAEhelp
    {
    local($viewVm, $itemName, $panel) = @_;
    local($helpsem) = &Vm_Get ($viewVm, "${itemName}.helpsem");
    if (defined ($helpsem) &&  $helpsem =~ /^[yY]/ )
	{
	print "$WARN: TAE Help mechanism is unsupported:\n";
	print "$MsgInd item $itemName, panel $panel.\n";
	}

    }
#
#   contructs the procedure name, declares the procedure,
#   and returns the generated uil for the callback (if any)
#
sub makeCallback
    {
    local ($tarVm, $panel, $itemName, $resource, $suffix) = @_;
    if (&Vm_Event ($tarVm, $itemName))
	{
        local($procName) = "${panel}_${itemName}_${suffix}";
	&generateCallback ($procName);
	return "$resource = procedure $procName();";
	}
    else
	{
	return "";			# no callbacks
	}
    }


#
#
# CommonViewArgs handles all common resources (Item Specification panel).
# Stores results in global associative array so sub xxx_Presentation can 
# retrieve values from $CVA{}.
#

sub CommonViewArgs {

    local($targetVm, $viewVm, $panel, $itemName, $momFont, $momFG, $momBG) = @_;

	# Get presentation in case there's some special case treatment needed.
    local($type) = &Vm_Get ($viewVm, "${itemName}._type");
    local($eventGenerating) = &Vm_Event ($targetVm, $itemName);

    local($title) = &getTitle ($viewVm, $itemName);
    local($x, $y) = &Vm_Get ($viewVm, "${itemName}.origin");
    local($width, $height) = &Vm_Get ($viewVm, "${itemName}.size");
    local($border) = &Vm_Get ($viewVm, "${itemName}.border");
    local($shadow) = &Vm_Get ($viewVm, "${itemName}.shadow");

    local($fg) = &Vm_Get ($viewVm, "${itemName}.fg");
          $fg = &parseColor ($fg, $momFG, $type);
          $fg = $momFG if ( $fg eq "parent");

    local($bg) = &Vm_Get ($viewVm, "${itemName}.bg");
          $bg = &parseColor ($bg, $momBG, $type);
          $bg = $momBG if ( $bg eq "parent");

    local($font) = &Vm_Get ($viewVm, "${itemName}.font");
	  if ($font)		# otherwise defaults below
	      {
              $font = &parseFont ($font, $momFont, $type);
              $font = $momFont if ( $font eq "parent");
	      }
	# traversal handling is similar to agentRt.cc
    local($traversal) = &Vm_Get ($viewVm, "${itemName}.traversal");
    local($navigation, $highlight);
    if ( $traversal == 1 )
	{
	$traversal  = "true" ;	# Motif wants Boolean not integer
	$navigation = XmTAB_GROUP;
	$highlight  = 2 ;
	}
    else ## if $traversal == 0 OR if traversal is undefined
	{
	$traversal  = "false" ;	# Motif wants Boolean not integer
	$navigation = XmNONE ;  # NOTE: This differs from WPT.
	$highlight  = 0 ;
	}

	# Save in global associative array.

    $CVA{"title"} 	= $title ;
    $CVA{"font"} 	= (defined $font) ? $font : "font('fixed')" ;
    $CVA{"x"} 		= (defined $x) ? $x : 0 ;
    $CVA{"y"} 		= (defined $y) ? $y : 0 ;
    $CVA{"width"} 	= (defined $width) ?  $width : 0;
    $CVA{"height"} 	= (defined $height) ? $height : 0;
    $CVA{"fg"} 		= (defined $fg) ? $fg : "color('black')" ;
    $CVA{"bg"} 		= (defined $bg) ? $bg : "color('white')" ;
    $CVA{"border"} 	= (defined $border) ? $border : 0;
    $CVA{"shadow"} 	= (defined $shadow) ? $shadow : 2 ;
    $CVA{"traversal"} 	= $traversal ;		
    $CVA{"navigation"}  = $navigation ;
    $CVA{"highlight"}   = $highlight ;
    ## $CVA{"borderColor"}; # not used; same value as $CVA{"fg"}
}

#
# menubarUILopen - generates opening UIL for top level widget of menubar
#
sub menubarUILopen{
    local($menuHelpCascade)=@_;
    local($helpMenuWidget);
    if ($menuHelpCascade ne "") {
	$helpMenuWidget = "XmNmenuHelpWidget = ${menuHelpCascade};";
	}
      ## Add the top level widget to the items list
    $Items .= <<end;

object $PanelItem : XmMenuBar	! menubar presentation
  {
  arguments
    {
    ! Item Specification Panel resources
    XmNx               = $CVA{"x"};
    XmNy               = $CVA{"y"};
    XmNwidth           = $CVA{"width"};
    XmNheight          = $CVA{"height"};
    XmNforeground      = color_$CVA{"fg"};
    XmNbackground      = color_$CVA{"bg"};
    XmNborderColor     = color_$CVA{"fg"};
    XmNborderWidth     = $CVA{"border"};
    XmNmarginWidth     = 0;
    XmNmarginHeight    = 0;
    XmNshadowThickness = $CVA{"shadow"};

    XmNmenuAccelerator = "<Key>Dummy";
    XmNpopupEnabled    = false;

    XmNtraversalOn     = $CVA{"traversal"};
    XmNnavigationType  = $CVA{"navigation"};
    ! XmNhighlightThickness = $CVA{"highlight"}; NO UIL COUNTERPART 

    ! Presentation Panel resources
    XmNspacing          = $spacing;
    XmNresizeWidth      = false;
    XmNresizeHeight     = false;

    ! Help menu resource goes here when required
    $helpMenuWidget
    $ExtraArgs
    };

  controls
    {
end
}
#
# menubarUILclose - generates closing UIL for top level widget of menubar
#
sub menubarUILclose{
	local($callback)=@_;

$Items .= <<end;
    };
  $callback
  };
end
}

#
# MenuEntryCascadeArgs	  handles recursive traversal of menubar cascade buttons
#

sub MenuEntryCascadeArgs {
    local($targetVm, $viewVm, $panel, $itemName,
	  $momFont, $momFG, $momBG, $menuTabs,$menuDepth) = @_;

	##
	## Track menu depth for help cascades and tabbing
	##
    local($tabLevel)=$menuTabs;
    local($depth)=$menuDepth;
    local($cascadeTag)="";

	##
	## Save local copies of mom Font, FG and BG
	##
    local($entryMomFont)=$momFont;
    local($entryMomFG)=$momFG;
    local($entryMomBG)=$momBG;

    local ( $item, @vmNames, $type, %assoc );

    local ( @qualList, $qualName );
        # LOCAL associative array to reference MenuEntryCommonViewArgs (MCVA)
    local(
    	$MCVA{"title"} ,
    	$MCVA{"font"} ,
    	$MCVA{"fg"} ,
    	$MCVA{"bg"} ,
    	$MCVA{"justification"} ,
    	$MCVA{"state"} ,	   # for XmNsensitive (dimming)
    	$MCVA{"mnemonic"} ,
    	$MCVA{"acceleratorText"} , # eg: "Ctrl+A"
    	$MCVA{"accelerator"}      # eg: "Ctrl<Key>A"
	);

     @qualList = &Vm_Names($viewVm, $itemName);
     foreach $qual (@qualList)
	{
	$qualName = "${itemName}.${qual}";
	if ($ENV{DEBUGTABBING}==1){
	    print "${tabLevel}>$qualName\n";
	    }
	$type = &Vm_Get ($viewVm, "${qualName}._type");
	if (!defined($type))
		 {next;}
	else {
	     if ($ENV{UILMENUDEBUG}==1)
	        {print "   ${qualName} , TYPE = ${type}\n";}
	     }

           ## Get common menu entry resources (fg,bg,title,font,size, etc.)
        &MenuEntryCommonViewArgs ($targetVm, $viewVm, $panel, 
				  $qualName,
                        	  $entryMomFont, $entryMomFG, $entryMomBG,
				  *MCVA );
	if ($ENV{DEBUG_FONTS} == 1)
	   {print "MenuEntryCascadeArgs: returned font =",$MCVA{"font"},"\n";}
	#if ( $ENV{'TAEPERL_DEBUG'} == 1 )
	#	{ &printMenuCommonViewArgs ( *MCVA ); }

	if ($type eq "cascade" || $type eq "help cascade" || $type eq "taehelp")
	    {
		### Not terminal node
		###
		### If help cascade, format object ident to be referenced
		### in XmNmenuHelpWidget resource of menubar
		###
	    $cascadeTag = "";
	    if ($type eq "help cascade" || $type eq "taehelp" )
		{
		# Note that "taehelp" is treated same as "help cascade" for now.
	        if ( $type eq "taehelp" )
		    {
		    print "$WARN: Menubar entry type 'taehelp' is unsupported.\n";
		    printf "$MsgInd Item $itemName, panel $panel will become a 'help cascade'.\n";
		    }
	        if ($helpCascade eq "" && $depth eq 0)
		    {
		    $helpCascade = "${PanelItem}\$help";
		    $cascadeTag = ${helpCascade}." : ";
	            if ($ENV{DEBUGMENUHELP}==1)
		       {print "   found helpCascade = ${helpCascade}\n";}
		    } 
	        elsif ($depth ne 0)
		    {
		    print "$WARN: ${PanelItem}.${qualName} depth=${depth}: \n";
		    print "$MsgInd Help cascade not supported below top level.\n";
		    }
	        elsif ($helpCascade ne "")
		    {
		    print "$WARN: ${PanelItem}.${qualName} : \n";
		    print "$MsgInd Help cascade already encountered for this item.\n";
		    }
		}

	    if ($ENV{DEBUGMENUHELP}==1)
	       {print "   cascadeTag = ${cascadeTag}\n";}

		###
		### Output opening UIL for cascade button
		###
	    &MenuEntryCascade_uilopen (*MCVA, $cascadeTag, $tablevel);

		###
		### Descend to process cascade item
		###
	    &MenuEntryCascadeArgs(
    			 $targetVm, $viewVm, $panel, $qualName,
			 $MCVA{"font"}, $MCVA{"fg"}, $MCVA{"bg"},
			 $tabLevel.'    ',$depth+1);
		###
		### Output closing UIL for cascade button
		###
	    &MenuEntryCascade_uilclose (*MCVA, $MenubarCallback, $tablevel);
	    } # end not terminal node

	### Following strings for $type are from $TAEWPT/menubarRt.cc.
	### Note that types "cascade", "help cascade" and "taehelp" are
	### handled above.

	elsif ( $type eq "pushbutton" )
	    { &MenuEntryPushbutton(*MCVA,$tabLevel); }
	elsif ( $type eq "separator" )
	    { &MenuEntrySeparator($viewVm,*MCVA,$tabLevel); }
	elsif ( $type eq "checkbox" )
	    { 
	    ### Checkbox entries have a target value always at the level
	    ### "itemName.checkboxEntryName", regardless of depth.
	    ###
	    ### ASSUMPTION: 
	    ### WorkBench enforces uniqueness of checkbox entry names
	    ### within each menubar.
	    ###
	    ### Get the first and last components of the checkbox name. 
	    local(@parts) = split (/\./, $qualName );
	    local($head) = $parts[0];
	    local($tail) = $parts[$#parts];
	    local($set) = &Vm_Get ($targetVm, "${head}.${tail}" );
	    if ($ENV{DEBUGMENU_CHECKBOX} == 1)
		{
		print "Checkbox qualName = $qualName, set = $set\n";
		print "head.tail = $head.$tail \n";
		}
	    local($state) = "false";	# for XmNset resource
   	    if ($set eq "yes") {$state = "true";} ; 
	    # Note last arg to MenuEntryCheckbox.
	    &MenuEntryCheckbox( *MCVA,$tabLevel, $state ); 
	    }
	elsif ( $type eq "label" )
	    { &MenuEntryLabel ( *MCVA,$tabLevel ); }
	elsif ( $type eq "menuicon" )
	    { &MenuEntryMenuicon ($qualName, *MCVA,$tabLevel ); }
	else
	    { print "$ERR: MenuEntryCascadeArgs: UNKNOWN type = $type \n"; }
	} # outer foreach
} # end MenuEntryCascadeArgs
#
sub MenuEntryPushbutton { local(*MCVA,$tabLevel) = @_;
 if ($ENV{UILMENUDEBUG} == 1) {print "DEBUG: entering MenuEntryPushbutton\n";}

    local ($accel, $accelTxt, $mnem);
    $accel = length($MCVA{"accelerator"}) != 0 ? "" : "!";
    $accelTxt = length($MCVA{"acceleratorText"}) != 0 ? "" : "!";
    $mnem = length($MCVA{"mnemonic"}) != 0 ? "" : "!";

	$MenuEntryItems .= <<end
$tabLevel    XmPushButton{ arguments {
$tabLevel      XmNlabelString     = $MCVA{"title"};
$tabLevel      XmNfontList        = font_alias_$MCVA{"font"};
$tabLevel      XmNforeground      = color_$MCVA{"fg"};
$tabLevel      XmNbackground      = color_$MCVA{"bg"};
$tabLevel      XmNalignment       = $MCVA{"justification"};
$tabLevel      XmNsensitive       = $MCVA{"state"} ;
$tabLevel      ${accelTxt}XmNacceleratorText = "$MCVA{"acceleratorText"}";
$tabLevel      ${accel}XmNaccelerator     = "$MCVA{"accelerator"}";
$tabLevel      ${mnem}XmNmnemonic        = keysym( "$MCVA{"mnemonic"}");
$tabLevel      };};

end
}
#
sub MenuEntrySeparator { local($viewVm, *MCVA,$tabLevel) = @_;

    if ($ENV{UILMENUDEBUG} == 1) {print "DEBUG: entering MenuEntrySeparator\n";}

    # separator type is value of view of entry of type "separator"
    local($raw_sep) = &Vm_Get ($viewVm, "${itemName}");

    # Note: "NO_QUAL" is a special tag for translateEnum.
    local($sep) = &translateEnum ($viewVm, $itemName, "NO_QUAL",
	    "Single Line","XmSINGLE_LINE",	# DEFAULT!
	    "Double Line","XmDOUBLE_LINE",
	    "Single Dashed Line","XmSINGLE_DASHED_LINE",
	    "Double Dashed Line","XmDOUBLE_DASHED_LINE",
	    "Shadow Etched In","XmSHADOW_ETCHED_IN",
	    "Shadow Etched Out","XmSHADOW_ETCHED_OUT");

if ( ($ENV{UILMENUDEBUG} == 1) || ($ENV{SEPARATOR_DEBUG} == 1) )
{ print "	raw_sep = $raw_sep, sep = $sep, itemName = $itemName\n";}

	$MenuEntryItems .= <<end
$tabLevel    XmSeparator {arguments {
$tabLevel      XmNseparatorType   =  $sep ;
$tabLevel      };};

end
}
#
# Note last arg.
sub MenuEntryCheckbox { local ( *MCVA,$tabLevel, $state ) = @_;

 if ($ENV{UILMENUDEBUG} == 1) {print "DEBUG: entering MenuEntryCheckbox\n";}

    if ($ENV{UILMENUDEBUG} == 1) {
	print "TBD: MenuEntryCheckbox XmNspacing forced to 10\n";
	}

    local ($accel, $accelTxt, $mnem);
    $accel = length($MCVA{"accelerator"}) != 0 ? "" : "!";
    $accelTxt = length($MCVA{"acceleratorText"}) != 0 ? "" : "!";
    $mnem = length($MCVA{"mnemonic"}) != 0 ? "" : "!";

    $MenuEntryItems .= <<end
$tabLevel    XmToggleButton {arguments {
$tabLevel      XmNlabelString     = $MCVA{"title"};
$tabLevel      XmNspacing         = 10; ! TBD: Pending PR-2102
$tabLevel      XmNfontList        = font_alias_$MCVA{"font"};
$tabLevel      XmNforeground      = color_$MCVA{"fg"};
$tabLevel      XmNbackground      = color_$MCVA{"bg"};
$tabLevel      XmNalignment       = $MCVA{"justification"};
$tabLevel      XmNsensitive       = $MCVA{"state"} ;
$tabLevel      XmNset             = $state;
$tabLevel      ${accelTxt}XmNacceleratorText = "$MCVA{"acceleratorText"}";
$tabLevel      ${accel}XmNaccelerator     = "$MCVA{"accelerator"}";
$tabLevel      ${mnem}XmNmnemonic        = keysym( "$MCVA{"mnemonic"}");
$tabLevel      };};

end
}
#
sub MenuEntryLabel { local ( *MCVA,$tabLevel ) = @_;

  if ($ENV{UILMENUDEBUG} == 1) {print "DEBUG: entering MenuEntryLabel\n";}

  local ($mnem);
  $mnem = length($MCVA{"mnemonic"}) != 0 ? "" : "!";

  $MenuEntryItems .= <<end;
$tabLevel    XmLabel {arguments {
$tabLevel      XmNlabelString     = $MCVA{"title"};
$tabLevel      XmNfontList        = font_alias_$MCVA{"font"};
$tabLevel      XmNforeground      = color_$MCVA{"fg"};
$tabLevel      XmNbackground      = color_$MCVA{"bg"};
$tabLevel      XmNalignment       = $MCVA{"justification"};
$tabLevel      ${mnem}XmNmnemonic        = keysym("$MCVA{"mnemonic"}");
$tabLevel      XmNsensitive = $MCVA{"state"} ;
$tabLevel      };};

end
}
#
sub MenuEntryMenuicon { local ($entryName, *MCVA ,$tabLevel) = @_;

 if ($ENV{UILMENUDEBUG} == 1) {print "DEBUG: entering MenuEntryMenuicon\n";}

 # TBD - XmNacceleratorText but NO XmNmnemonic
 $menuiconCount += 1;
 local ($accel, $accelTxt);
 $accel = length($MCVA{"accelerator"}) != 0 ? "" : "!";
 $accelTxt = length($MCVA{"acceleratorText"}) != 0 ? "" : "!";
 local($pixmapFileName) = 
	&Vm_Get($viewVm, "${entryName}.filename");
 local($pixmapIdent)="mnupxmap_".$menuiconCount;

 if ($ENV{UILMENUDEBUG} == 1)
   {
   print "DEBUG: MenuEntryMenuicon : pixmapFileName = $pixmapFileName\n";
   print "			     pixmapIdent = $pixmapIdent\n";
   }

 if (defined($pixmapFileName) && $pixmapFileName ne "")
    {
       # Expand initial environment variable if necces.
    $pixmapFileName=&resolveFilePathname($pixmapFileName);
    $PixmapVarAssoc { $pixmapIdent } =
	 "\t${pixmapIdent}:  xbitmapfile('$pixmapFileName');" ;
    if ($ENV{UILMENUDEBUG} == 1)
      {print "DEBUG: MenuEntryMenuicon : pixmapFileName = $pixmapFileName\n";}
    }
 else
    {
    print "$ERR: No pixmap defined for menuicon $panel.$entryName\n";
    }

    $MenuEntryItems .= <<end
$tabLevel    XmPushButton {arguments {
$tabLevel      XmNlabelType       = XmPIXMAP ;
$tabLevel      XmNlabelPixmap     = ${pixmapIdent} ; 
$tabLevel      XmNfontList        = font_alias_$MCVA{"font"};
$tabLevel      XmNforeground      = color_$MCVA{"fg"};
$tabLevel      XmNbackground      = color_$MCVA{"bg"};
$tabLevel      XmNalignment       = $MCVA{"justification"};
$tabLevel      ${accelTxt}XmNacceleratorText = "$MCVA{"acceleratorText"}";
$tabLevel      ${accel}XmNaccelerator     = "$MCVA{"accelerator"}";
$tabLevel      };};

end
}
#
sub MenuEntryCascade_uilopen { local ( *MCVA, $cascadeTag,$tablevel ) = @_;

     local ($mnem);
     $mnem = length($MCVA{"mnemonic"}) != 0 ? "" : "!";

$MenuEntryItems .= <<end;
$tablevel    !<<<<<<< open $MCVA{"title"} Cascade
$tablevel    $cascadeTag XmCascadeButton { arguments {
$tablevel       XmNlabelString     = $MCVA{"title"};
$tablevel       XmNfontList        = font_alias_$MCVA{"font"};
$tablevel       XmNforeground      = color_$MCVA{"fg"};
$tablevel       XmNbackground      = color_$MCVA{"bg"};
$tablevel       ${mnem}XmNmnemonic        = keysym( "$MCVA{"mnemonic"}" ); 
$tablevel       ! XmNalignment = $mlabelalign;
$tablevel       };
$tablevel     controls {
$tablevel       XmPulldownMenu {
$tablevel         arguments{ };
$tablevel         controls
$tablevel           {
end
}
#
sub MenuEntryCascade_uilclose { local ( *MCVA, $callback, $tablevel ) = @_;

    $MenuEntryItems .= <<end;
$tablevel           };
$tablevel	  $callback
$tablevel         };
$tablevel       };
$tablevel     };
$tablevel    ! close $MCVA{"title"} Cascade >>>>>>>

end
}
#
#
# printMenuCommonViewArgs debug print of all common menu entry resources.
#
sub printMenuCommonViewArgs {
	local(*MCVA) = @_ ;

	print "\n\ttitle = ", $MCVA{"title"} ;
	print "\n\tfont = ", $MCVA{"font"}  ;
	print "\n\tfg = ", $MCVA{"fg"}    ;
	print "\n\tbg = ", $MCVA{"bg"}    ;
	print "\n\tjustification = ", $MCVA{"justification"};
	print "\n\tstate = ", $MCVA{"state"};
	print "\n\tmnemonic = ", $MCVA{"mnemonic"};
	print "\n\tacceleratorText = ", $MCVA{"acceleratorText"};
	print "\n\taccelerator = ", $MCVA{"accelerator"};
	print "\n\n";
}
#
#
# MenuEntryCommonViewArgs handles all common menu entry resources.
#
sub MenuEntryCommonViewArgs {

   local($targetVm, $viewVm, $panel, $entryName, $momFont, $momFG, $momBG, *MCVA) = @_;
    ##                        NOTE:   ^^^^^^^^^^

	# Get presentation for determining special case treatment
    local($type) = &Vm_Get ($viewVm, "${entryName}._type");

    # Placeholder: use when implementing menubar button/icon connections
    ## local($eventGenerating) = &Vm_Event ($targetVm, $entryName);
    ## print "item = $entryName, event gen = $eventGenerating\n";

    local($title) = &getTitle ($viewVm, $entryName); 
    local($fg) = &Vm_Get ($viewVm, "${entryName}.fg");
	##
	## Check for foreground references, and substitute appropriate
	## foreground color if necessary/
	##
          $fg = $ItemForeground if ( $fg eq "item foreground");
          $fg = $PanelFG if ( $fg eq "Panel Foreground");
          $fg = $PanelFG if ( $fg eq "parent panel");
          $fg = $momFG if ( $fg eq "parent cascade");

          $fg = &parseColor ($fg, $momFG, $type);

    local($bg) = &Vm_Get ($viewVm, "${entryName}.bg");
	##
	## Check for background references, and substitute appropriate
	## background color if necessary/
	##
          $bg = $ItemBackground if ( $bg eq "item background");
          $bg = $PanelBG if ( $bg eq "Panel Background");
          $bg = $PanelBG if ( $bg eq "parent panel");
          $bg = $momBG if ( $bg eq "parent cascade");

          $bg = &parseColor ($bg, $momBG, $type);

    local($font) = &Vm_Get ($viewVm, "${entryName}.font");
	##
	## Check for font references, and substitute appropriate
	## font if necessary/
	##
          $font = $ItemFont if ( $font eq "item font");
          $font = $PanelFont if ( $font eq "Panel Font");
          $font = $PanelFont if ( $font eq "parent panel");
          $font = $momFont if ( $font eq "parent cascade");
	
	  $font = &parseFont ($font, $momFont, $type);

    local($justification) = 
	&translateEnum ($viewVm, $entryName, "justification",
	    "center", "XmALIGNMENT_CENTER",		# default !!
	    "left",   "XmALIGNMENT_BEGINNING",
	    "right",  "XmALIGNMENT_END"
	    );

	# State is input sensitivity (false = dimmed)
    local($raw_state, $state);
    $raw_state     = &Vm_Get ($viewVm, "${entryName}.state");
    if ( $raw_state == 0 ) { $state = "false" ; }
    else { $state = "true" ; }

    local($mnemonic)      = &Vm_Get ($viewVm, "${entryName}.mnemonic");

# cascades can't have accelerators
# TAE stores ONLY the acceleratorText (eg: "Ctrl+A"), but
#       we also need to map this to accelerator  (eg: "Ctrl<Key>A").
#       For algorithm, see $TAEWPT/wptresconv.cc: AccelArg::AccelArg

    local($acceleratorText)   = &Vm_Get ($viewVm, "${entryName}.accelerator");
    local($accelerator);

    # map acceleratorText to accelerator 
    if ( $acceleratorText ne "" )  # can't use "defined"
	{
        $accelerator = &parseAccelerator ($acceleratorText);
	}

	# Save in associative array.

    $MCVA{"title"} 	= $title ;
    $MCVA{"font"} 	= (defined $font) ? $font : "font('fixed')" ;
    $MCVA{"fg"} 	= (defined $fg) ? $fg : "color('black')" ;
    $MCVA{"bg"} 	= (defined $bg) ? $bg : "color('white')" ;
    $MCVA{"justification"}  =
	 defined($justification) ? $justification : XmALIGNMENT_LEFT;
    $MCVA{"state"}          =
	 defined($state) ? $state : "false"; # for XmNsensitive (dimming)

# TBD - accelerators and mnemonics may be NULL  - UIL generation subs must
#	check and either suppress resource or assign default.
    $MCVA{"mnemonic"}       = $mnemonic;
    $MCVA{"acceleratorText"}= $acceleratorText;
    $MCVA{"accelerator"}    = $accelerator;
}

#
#
#
sub printHeader {
# put version and datestamp into UIL file for future reference
local($time) = `date`;
print UIL "! UIL generated by TAE Plus $TAEversion: $time";
print UIL "! From $ResFile dated $ResFileTime\n";
print UIL "! @(#)uilGen.pl	33.1 8/26/94 \n";
print UIL <<end;

module main
    version = '$MotifVersion'
    names = case_sensitive
    objects = 
	{
	XmSeparator = gadget;
	}

    value

	! Used in place of XmFILE_ANY_TYPE for the XmNfileTypeMask resource
	! since UIL doesn't recognize the latter, and neither
	!     XmNfileTypeMask = (XmFILE_REGULAR + XmFILE_DIRECTORY) nor
	!     XmNfileTypeMask = (XmFILE_REGULAR | XmFILE_DIRECTORY) 
	! yield the desired file search result.
	!
	XM_FILE_ANY_TYPE	: 3;

end
}
#
#
#
sub resolveFilePathname{
   local($filename)=@_;
   local($fullPathName);
   if (index($filename,"$")==0)
      {
      local($path)=substr($filename,1,index($filename,"/")-1);
      $path = $ENV{$path};
      $fullPathName = $path.substr($filename,index($filename,"/"));
      }
   else{
      $fullPathName = $filename;}

   return $fullPathName;
}

#
#
#
sub parseFont {
    local ($font, $momFont, $type) = @_;

    if ($ENV{DEBUG_FONTS} == 1)
	{
	print "parseFont: font=$font momFont=$momFont\n";
	print "           PanelFont=$PanelFont\n";
	}

    # replace parent font with name if necessary
    $font = $momFont if ( $font eq "parent");

	# Some pres types permit special strings such as "Panel Font"
    if ( index ( $font, " " ) >= 0 )
	{
        if ( $type eq "pulldown" || $type eq "option" )
            {
            $font = $momFont if ( $font eq "Panel Font") ;
            }
        else
            {
            $font = "fixed";            # avoid uil compilation error
            print "$ERR: Unexpected exception in parseFont, type = $type, font = $font\n";
           }

	} # end if embedded space in $font

	# PR-2490 
    elsif ( index ( $font, "-" ) >= 0 )  # XLFD (X logical font description)
	{
	# In this case, $font is an XLFD.
	++$XLFD_INDEX ;
        local ($font_xlfd);
        $font_xlfd = "xlfd_" . $XLFD_INDEX;

        # Add font to associative array (don't check whether already there).
        # NOTE: We're stuck with the "font_alias_" prefix for XLFD because
        #       later code uses that prefix.
        $FontVarAssoc { $font_xlfd } = "\tfont_alias_$font_xlfd:         font('$font');" ;
        $font = $font_xlfd;     # TBD - for return
	}
    else	# normal case: font alias
	{
        # Add font to associative array (don't check whether already there).
	#
	# PR-2535 - Map font alias to XLFD
	$XLFD = $XLFD_TABLE{$font} ;
	if ( length ( $XLFD ) > 0 )
	    {
            $FontVarAssoc {$font} = "\tfont_alias_$font:         font('$XLFD');" ;
	    }
	else # not found in $XLFD_TABLE
	    {
	    # See pr-2623.  Only search next table if not on solaris.
	    if ($ENV{TAEPLAT} ne "solaris")
		{
		$XLFD = $XLFD_NON_OW_TABLE{$font} ;
		if ( length ( $XLFD ) > 0 )
		    {
		    $FontVarAssoc {$font} = "\tfont_alias_$font:         font('$XLFD');" ;
		    }
		}
	    }
	if ( length ( $XLFD ) <= 0 )
	    # Can't map alias to XLFD so assume alias is known to server.
	    # Examples of this case on Solaris are: k14, luB*, luI*.
	    {
            $FontVarAssoc {$font} = "\tfont_alias_$font:         font('$font');" ;
	    }
	}

    # Force return of font value (possibly converted).
    $ret = $font ;
    }
#
#
#	dig components from TAE Plus' rgb color string format.
#
sub getRGB
    {
    local($color) = @_;
    local($numeric, $r, $g, $b);
    $numeric = '[0-9]*\.[0-9]*|[0-9]+' ;	# nnn.nnn or nnn
    $color =~ /[rR]\s*=\s*($numeric)/;
    $r =  int($1 * 0xFFFF) ;
    $color =~ /[gG]\s*=\s*($numeric)/;
    $g =  int($1 * 0xFFFF);
    $color =~ /[bB]\s*=\s*($numeric)/;
    $b =  int($1 * 0xFFFF);
    return ($r, $g, $b);
    }
#
#   color parsing for DDOs; the resulting color string
#   becomes part of a thresholdString (and consequently the "r=..." 
#   syntax is not allowed).
#
sub parseDDOColor 
    {
    local ($color, $momColor) = @_;
    if ($color =~ /=/)			# TAE Plus component format?
	{
	local($r, $g, $b) = &getRGB ($color);
	$color = sprintf ("#%04x%04x%04x", $r, $g, $b);	# use hex format
	}
    elsif ($color eq "parent" || $color eq "PARENT")
	{
	$color = $momColor;				# copy from parent
	}
    return $color;			# suitable for thresholdTableString
    }

#
#    colors for non-DDO items.
#
#	NOTE: Code for hex and rgb parsing is repeated; re-do as subs (TBD)
#
#	TBD: Handling new X11R5 colorspec such as this subset of cases: 
#		rgb:ffff/0/0
#		rgbi:<red>/<green>/<blue>
#		CIEXYZ:<X>/<Y>/<Z>
#		TekHVC:<H>/<V>/<C> 
#
sub parseColor {
    local ($color, $momColor, $type) = @_;
    local ($rColor,$gColor,$bColor,$rgb, $o, $l, $argColor);

    # replace parent color with name if necessary
    $argColor = $color;
    $color = $momColor if ( $color eq "parent");

    if ($color =~ /^#/)			# hex format?
	{
	$color =~ s/^#//;		# remove leading hex indicator
	local($compSize) = length($color)/3;	# chars/component
	local($zeroPad) = "0" x (4 - $compSize);# right-hand padding
	local(@components);			# vector of rgb values
	local($i, $hex);
	for $i (0..2)				# for each component
	    {
	    $hex = substr($color, $i*$compSize, $compSize) . $zeroPad;
	    eval "\$components[\$i] = 0x$hex";	# convert to decimal
	    }
	local($r, $g, $b) = @components;	# give them names
	$rgb = "rgb($r, $g, $b)";		# rgb specification
	}

    # check for "(r=%f, g=%f, b=%f)" color notation (v5.2 and later)
    elsif ($color =~ /=/)		# TAE Plus component format
	{
	($rColor, $gColor, $bColor) = &getRGB($color);
	$rgb = "rgb($rColor, $gColor, $bColor)";
	$color = "r" . $rColor . "g" . $gColor . "b" . $bColor;
	}
      # Some pres types permit special strings such as "Panel Foreground"
    elsif ($color =~ / /)		# embedded blank
	{
        if ( $type eq "pulldown" || $type eq "option" )
            {
            if ( $color eq "Panel Background" || $color eq "Panel Foreground")
                {
                $color = $momColor ;

		# Catch possible inheritance of hex format
                if ($color =~ /^#/)                 # hex format?
                    {
                    $color =~ s/^#//;               # remove leading hex indicator
                    local($compSize) = length($color)/3;    # chars/component
                    local($zeroPad) = "0" x (4 - $compSize);# right-hand padding
                    local(@components);                     # vector of rgb values
                    local($i, $hex);
                    for $i (0..2)                           # for each component
                        {
                        $hex = substr($color, $i*$compSize, $compSize) . $zeroPad;
                        eval "\$components[\$i] = 0x$hex";  # convert to decimal
                        }
                    local($r, $g, $b) = @components;        # give them names
                    $rgb = "rgb($r, $g, $b)";               # rgb specification
                    }

		# Catch possible inheritance of RGB
    		elsif ($color =~ /=/)		# TAE Plus component format
		    {
		    ($rColor, $gColor, $bColor) = &getRGB($color);
		    $rgb = "rgb($rColor, $gColor, $bColor)";
		    $color = "r" . $rColor . "g" . $gColor . "b" . $bColor;
		    }
                }
            }
        if ( $type eq "icon" )
            {
            if ( $color eq "Icon Background" || $color eq "Icon Foreground")
                {
                $color = $momColor ;

		# Catch possible inheritance of hex format
                if ($color =~ /^#/)                 # hex format?
                    {
                    $color =~ s/^#//;               # remove leading hex indicator
                    local($compSize) = length($color)/3;    # chars/component
                    local($zeroPad) = "0" x (4 - $compSize);# right-hand padding
                    local(@components);                     # vector of rgb values
                    local($i, $hex);
                    for $i (0..2)                           # for each component
                        {
                        $hex = substr($color, $i*$compSize, $compSize) . $zeroPad;
                        eval "\$components[\$i] = 0x$hex";  # convert to decimal
                        }
                    local($r, $g, $b) = @components;        # give them names
                    $rgb = "rgb($r, $g, $b)";               # rgb specification
                    }

		# Catch possible inheritance of RGB
    		elsif ($color =~ /=/)		# TAE Plus component format
		    {
		    ($rColor, $gColor, $bColor) = &getRGB($color);
		    $rgb = "rgb($rColor, $gColor, $bColor)";
		    $color = "r" . $rColor . "g" . $gColor . "b" . $bColor;
		    }
                }
            }
        else
            {
            $color = "black";           # avoid UIL compilation error
            print "$ERR: Unexpected exception in parseColor, type = $type, color = '$argColor', forced to $color\n";
	    }
	} # end if embedded space in $color

    # Add color to associative array 
    if ( length ( $ColorVarAssoc{ $color } ) == 0 ) # not already there
    {
      if ((defined $rgb) && ($rgb ne ""))
	{
	$ColorVarAssoc { $color } = "\tcolor_$color:\t$rgb;" ;
	}
      else
	{
        $ColorVarAssoc { $color } = "\tcolor_$color:\tcolor('$color');" ; 
	}
    }
    
    # Force return of color name (possibly converted).
    $ret = $color ;
    }

#
# parseAccelerator - Convert XmNacceleratorText to XmNaccelerator
#
#	Examples:
#		Shift+A		  ==>  Shift<Key>A
#		Ctrl+Shift+A	  ==>  Ctrl Shift<Key>A
#		Alt+Ctrl+Shift+A  ==>  Atl Ctrl Shift<Key>A
#		Shift+F9  	  ==>  Shift<Key>F9
#		KP_F2		  ==>  <Key>KP_F2
#		c		  ==>  <Key>c
#		Execute		  ==>  <Key>Execute
#
# 	Note: Order of modifiers is not significant in Wpt. Therefore,
#             "Alt+Ctrl+Shift" = "Shift+Ctrl+Alt" (behave the same).
#
sub parseAccelerator {
    local ($acceleratorText) = @_;   # input string

    local ($accelerator);	# result of parsing (returned)
    local (@parts);		# plus-separated components now space-separated
    local ($tail);		# last elt. of @parts = key to be 
				# possibly modified
    local ($modifiers);		# all of @parts except for $tail 
				# (eg: Ctrl Shift)

    $accelerator = $acceleratorText ;

    # Change + to space
    @parts       = split (/\+/, $accelerator );

    # Separate last element (tail) from all modifiers.
    $tail        = pop (@parts);
    $modifiers   = join ( " ", @parts );

    # Put together the pieces. Note that "<Key>" occurs only before last elt.
    $accelerator = $modifiers . "<Key>" . $tail ;

    if ( $ENV{DEBUG_ACCELERATORS} == 1) 
	{
    	print "acceleratorText = $acceleratorText \n";
    	print "modifiers       = $modifiers, tail = $tail\n";
    	print "accelerator     = $accelerator \n";
	}
    return ($accelerator);
}

#
# parseIconBitMap -- Convert stored bitmap to UIL pixmap format
#
sub parseIconBitmap{ 
    local ($viewVm,$itemName)=@_;
    local($mapwidth,$mapheight) = &Vm_Get($viewVm,"${itemName}.mapsize");
    local($bitmap) = pack("i*",&Vm_Get($viewVm,"${itemName}.bitmap"));
    local(@array)= unpack("c*",$bitmap);
    local($mask,$temp,$pixmap,$count);
    $temp = "'";
    $count = 0;
## target for exit of loop at end of line
ITEM:
    foreach $item (@array)
	{
	$mask = 1;
	for (1..8)
	    {
	    if ($item & $mask)
		{ $temp .= 'o';}
	    else
		{ $temp .= ' ';}

	    $count += 1;
	    if ($count == $mapwidth)
	        { 
	        $temp .= "',";
	        $pixmap .= $temp;
	        $temp = "'";
	        $count = 0;
		## last byte is padded with 0's, so start the next line fresh
		next ITEM;
	        }

	    $mask *= 2;
	    }
	}
		
    chop($pixmap);
    $ret = $pixmap;
}

#
# Add to UIL file all pixmap declarations
#
sub printPixmaps {
# Following lines are needed to handle icons.

	printf (UIL "\n");
	printf (UIL "\t!icon setup\n");
#        printf (UIL "\tblack: %30s;\n", "color('black')");
#        printf (UIL "\twhite: %30s;\n", "color('white')");
#        printf (UIL "\ticon_color_table: %30s;\n",
#		"color_table(black='o', white=' ')");

	local(@bitmapLines);
	foreach $bitmapVariable (sort keys %IconBitmapVarAssoc)
	    {	
	    @bitmapLines = split(/,/,$IconBitmapVarAssoc { $bitmapVariable } );
	    print UIL  join(",\n",@bitmapLines),"\n";
	    printf (UIL "\n");
	    }
	printf (UIL "\n");

	printf (UIL "\t!END icon setup\n");
	printf (UIL "\n");

foreach $pixmapVariable ( sort keys %PixmapVarAssoc )
	{
	print  UIL $PixmapVarAssoc { $pixmapVariable };
	}
}

#
# Add to UIL file all top level resource value literals
#
sub printTopLevelValues {
    print UIL <<end;

	! Function and Decoration constants from MwmUtil.h
        MWM_FUNC_ALL            : 1;
        MWM_FUNC_RESIZE         : 2;
        MWM_FUNC_MOVE           : 4;
        MWM_FUNC_MINIMIZE       : 8;
        MWM_FUNC_MAXIMIZE       : 16;
        MWM_FUNC_CLOSE          : 32;

        MWM_DECOR_ALL           : 1;
        MWM_DECOR_BORDER        : 2;
        MWM_DECOR_RESIZEH       : 4;
        MWM_DECOR_TITLE         : 8;
        MWM_DECOR_MENU          : 16;
        MWM_DECOR_MINIMIZE      : 32;
        MWM_DECOR_MAXIMIZE      : 64;


	! Literals for top level shell resources

$TopLevelResourceValues

end
}
#
# Add to UIL file all color and font declarations in sorted order.
#
sub printColorsAndFonts {

$\ = "\n";
foreach $colorVariable ( sort keys %ColorVarAssoc )
        {
        print  UIL $ColorVarAssoc { $colorVariable };
        }
printf (UIL "\n");

foreach $fontVariable ( sort keys %FontVarAssoc )
        {
        print  UIL $FontVarAssoc { $fontVariable };
        }
print UIL "\f";			# end of section
}

#
#
sub printProcs {
    return if (!$ProcedureList);
    print UIL <<end;
procedure
$ProcedureList

end
    }

sub printItems {
    print UIL <<end;
$Items

end
    }

sub printPanels {
    print UIL <<end;
$Panels

end
    }

sub printModuleEnd {
    print UIL "\nend module;\n";
}



# 
#
#  generate C CODE
#

sub generateCModule {
local ($time) = `date`;
chop($time);
local ($mrmInitCalls) = &MrmInitCalls;
local ($xtaeinc);

if ($codeStyle eq $codeStyleEnhanced )
    {
    $styleDecls ="#include <Xtae/MrmUtils.h>	/* for libXtae utility functions */";
    }
else
    {
    $styleDecls ="#define MAX_ARGLIST	64";
    }

#Set panel count to number of panels
$PanelCount += 1;
print C <<end;

/* *** TAE Plus Mrm Code Generator version $TAEversion, Style: $codeStyle *** */
/* *** Generated:   $time *** */
/* *** From $ResFile *** */
/* *** Dated $ResFileTime *** */

#include <stdio.h>
#include <malloc.h>
#include <Mrm/MrmPublic.h>
#include <Xm/Xm.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <Xm/MwmUtil.h>         /* for MWM_DECOR_* and MWM_FUNC_* */

$styleDecls


#define NUM_PANELS $PanelCount
$PanelNamesList


/*
** Globals for Xt and Xm toolkit functions
*/
Display 	*TheDisplay;
int		TheScreen;
XtAppContext 	AppContext;
Widget		ContainerWidget[NUM_PANELS];
Widget 	        TopLevelWidget[NUM_PANELS];
MrmHierarchy	S_MrmHierarchy;

/*
** Main program for application $ProgBasename.c and $UilBasename.uid
*/
int main (argc, argv)
        int  argc;
        char **argv;
   {
    int n;
    unsigned long	decorMask;
    unsigned long	funcMask;

    static char *db_filename_vec[] = {"$UilBasename.uid"};
    static int db_filename_num =
	(sizeof db_filename_vec / sizeof db_filename_vec [0]);

    /* ContainerWidget must be NULL before use */
    for (n=0; n < NUM_PANELS; n++) ContainerWidget[n] = NULL;

    MrmInitialize ();

    $mrmInitCalls

    /*
    ** Initialize the application shell for the first initial panel
    */
    TopLevelWidget[${AppShellPanel}_INDEX] =
                  XtAppInitialize(
                                 &AppContext,
                                 argv[0],       /* app class */
                                 NULL, 0,       /* options, # */
                                 &argc, argv,
                                 NULL,          /* fallback resources */
                                 NULL,NULL);    /* args,numargs */

    if (TopLevelWidget[${AppShellPanel}_INDEX] == NULL) {
        fprintf (stderr, "%s:  Can't create application shell\\n", argv[0]);
        exit (1);
        }
    else
	{
        /* Get the display id for later use */
        TheDisplay=XtDisplay(TopLevelWidget[${AppShellPanel}_INDEX]);
        TheScreen = XScreenNumberOfScreen(XtScreen(TopLevelWidget[${AppShellPanel}_INDEX]));
	}


    /*
    ** Open the Mrm widget hierarchy. Abort if unable to open.
    */
    if (MrmOpenHierarchy (db_filename_num, /* Number of files. */
          db_filename_vec,                 /* Array of file names.  */
          NULL,                            /* Default OS extenstion. */
          &S_MrmHierarchy)                 /* Pointer to returned MRM ID */
          !=MrmSUCCESS)
	{
        fprintf (stderr, "can't open hierarchy\\n");
	exit (1);
	}

    RegisterCallbacks ();


$InitialPanelList

    XtAppMainLoop   (AppContext);
   } /* main */


$FunctionDclList

/*  list of functions to register */

static MrmRegisterArg RegList[] = 
    {
        $FunctionNameList
        {"", 0}		/* dummy last entry */ 
    };
#define NRegList (sizeof(RegList)/sizeof(RegList[0]) - 1)

int RegisterCallbacks () 
   {
   int code;

   code = MrmRegisterNames (RegList, NRegList);
   if (code != MrmSUCCESS)
       {
       printf ("Cannot register callbacks\\n");
       return;
       }
   } /* RegisterCallbacks */
end
	##
	## CAUTION !!! The body of _createNewPanel is derived from
	##	       XtaeNewPanel in $TAEXTAE/MrmUtils.c. Any changes made
	##	       to either XtaeNewPanel or _createNewPanel
	##	       should be made to both modules.
	##
if ($codeStyle eq $codeStyleBasic) {
print C <<end;
int _createNewPanel(
    	  DisplayId, S_MrmHierarchy, PanelWidgetName,
	  TopLevelWidget,
 	  ContainerWidget)
   Display	*DisplayId;
   MrmHierarchy	*S_MrmHierarchy;
   char		*PanelWidgetName;
   Widget	*TopLevelWidget;
   Widget	*ContainerWidget;
   {
    /* Variables for top level resource retrieval */
    MrmType     dummy_class;
    int n;
    Arg arglist[MAX_ARGLIST];
    char uilIndex[32][MAX_ARGLIST];
    static char *indexSuffix[]=
        {"_title",
         "_x","_y","_w","_h","_b",
         "_vsbl","_iconic",
	 "_iconName",
	 "_mode","_func","_decor",
	 NULL};
    static String  resourceName[] =
        {XmNtitle,
         XmNx,XmNy,XmNwidth,XmNheight,XmNborderWidth,
         XmNmappedWhenManaged,XmNiconic,XmNiconName,
	 XmNmwmInputMode,XmNmwmFunctions,XmNmwmDecorations
        };

    /* Variables for Bitmap file retrieval */
    Drawable drawable;
    Pixmap bitmap ;
    MrmType dummy_type;
    int w, h, xhot, yhot;
    char  iconFileIndex[132];
    String  iconFilename;
    int i,fileStart;


    /*
    ** Create panel shell widget if not already created
    */
    if (*TopLevelWidget == NULL)
        *TopLevelWidget = XtAppCreateShell (PanelWidgetName, NULL,
                              topLevelShellWidgetClass,/* PanelWidgetClass, */
                              DisplayId, NULL, 0);
    /*
    ** Fetch panel container widget from hierarchy
    */
    if (MrmFetchWidget (
          *S_MrmHierarchy,
          PanelWidgetName,      /* uil name of panel */
          *TopLevelWidget,      /* top level shell widget for panel */
          ContainerWidget,   /* top level container widget for panel */
          &dummy_class)         /* placeholder for returned class */
          != MrmSUCCESS)
        {
        fprintf (stderr, "can't fetch %d window\\n",PanelWidgetName);
        exit (1);
        }

    /*
    ** Set shell resources from UID file
    */
    n=0;
    while (indexSuffix[n] != NULL)
        {
        sprintf(uilIndex[n],"k_%s%s", PanelWidgetName, indexSuffix[n]);
        XtSetArg (arglist[n],resourceName[n],uilIndex[n]);
        n++;
        }

    MrmFetchSetValues ( *S_MrmHierarchy,
                        *TopLevelWidget,
                        arglist,n);

    XtManageChild   (*ContainerWidget);
    XtRealizeWidget (*TopLevelWidget);

    /* Format index for Icon Filename literal and fetch from UID file */
    sprintf(iconFileIndex,"k_%s_iconFile",PanelWidgetName);
    MrmFetchLiteral(*S_MrmHierarchy,
                   (String)(&iconFileIndex[0]),
                   DisplayId,
                   &iconFilename,&dummy_type);

   /* If filename is null, return (uses Mwm default) */
   if (strlen(iconFilename) == 0)
      return;

   /*
   ** Get drawable for XReadBitmapFile and read in the bitmap
   */
   drawable = (Drawable)XtWindow ( *TopLevelWidget );
   if ( (n = XReadBitmapFile (DisplayId,
                             drawable, iconFilename,
                             &w, &h, &bitmap, &xhot, &yhot))
         == BitmapSuccess)
      {
      XtSetArg (arglist[0], XmNiconPixmap, bitmap );
      XtSetValues(*TopLevelWidget,arglist,1);
      }
   else
      fprintf(stderr,"SetPanelIcon: Error %d reading bitmap file %s\\n",
              n,iconFilename);
   }
end
   }

} # end generateCModule 

# 
#
#  generate Imakefile
#	TBD - expand this for multiple SRCS
#

sub generateImakefile {

local ($defineLibs,$defineLogger);
if ($usingLogger) {
     $defineLogger = "#define UseColorLogger";}
if ($usingDDO || $codeStyle eq $codeStyleEnhanced) {
     $defineLibs = "#define UseEnhancedLibs";}

local($time) = `date`;
chop($time);
print IMAKEFILE "# Imakefile generated by TAE Plus $TAEversion: $time";
print IMAKEFILE "#";
print IMAKEFILE "# To create a Makefile,     type: 'tae.mkmf'.";
print IMAKEFILE "# To build the application, type: 'make'.";
print IMAKEFILE "# To compile the uil file,  type: 'make $UilBasename.uid'.\n";

print IMAKEFILE <<end;

# Standard Motif and X libs are defined in \$TAE/config/TAEmake.tmpl.
# If your Motif installation is non-default, see the symbols for
# MOTIFLIBS and MRMLIBS on which UIL_LINKLIBS is based.
# Make changes in \$TAE/config/site.def rather than \$TAE/config/TAEmake.tmpl.
# (UIL_LINKLIBS also includes some X Window System libs such as -lXt and -lX11.)

#                                  NOTE:
#
# UIL_LINKLIBS changes based on whether "UseEnhancedLibs" and "UseColorLogger"
# are defined. By default, NEITHER of these are defined. This indicates that
# your application does not need to link with any TAE Plus libraries.
#
# However, if your application uses the Xtae DDO widgets, OR if you invoked
# res2uil with the "-e" (enhanced) option, the line: "#define UseEnhancedLibs"
# will appear below, indicating that linking with several TAE Plus libraries
# is necessary.

$defineLibs
 
# Similarly, if your application uses the Xtae Color Logger widget, the line:
# "#define UseColorLogger" will appear below, indicating that a different, small
# TAE Plus library is needed.

$defineLogger

         LINKLIBS = \$(UIL_LINKLIBS)

             SRCS = $ProgBasename.c
             OBJS = $ProgBasename.o 

SimpleCProgramTarget($ProgBasename,\$(OBJS))

# Note that the uil compiler, \$(UIL), is assumed to be in your search path.
# Several relevant UIL make variables are defined in \$TAE/config/TAEmake.tmpl.

$UilBasename.uid:: $UilBasename.uil \$(DEPUIL)
	\$(RM) \$@
	\$(UIL) \$(UILFLAGS) \$(LOCAL_UILFLAGS) -o \$@ $UilBasename.uil \$(ALL_UIL_INCLUDES)
end
}

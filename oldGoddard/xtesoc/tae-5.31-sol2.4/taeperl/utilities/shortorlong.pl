#
# shortorlong.pl [dir]
#
# 	Sense if a given directory contains short or long filenames
#	Exits with the following status:
#		0 : neither long or short
#		1 : short
#		2 : long
#		3 : argument is not a directory
#
# Algorithm to determine if there are long or short files in a
# given directory.
# 	If there are pan_*.* files then it is assumed to be long files
# 	If there are p_*.* files then it is assumed to be short files
# 	If there are both, then it is assumed that there are extra user
#	     files in the directory that are not related to panels that
#	     happened to be named p_ or pan_.  In this case, we check
#	     for *_ci.* files.  If they exist, we assume short files.
#	     Note, a *_ci.* file exists in all languages for short file apps.
#
# Change Log:
# ??-jul-94	PR2781: created...cew

if ($#ARGV >= 0) {
    # determine directory
    $dir = $ARGV[0];
    if (! -d $dir) {
	print "ERROR: Argument $dir is not a directory\n";
	exit 3;
    }
} else {
    $dir = '.';
}

$long = $short = 0;
@longlist = <$dir/pan_*.*>;
@shortlist = <$dir/p_*.*>;
if ($#longlist >= 0) { $long = 1; }
if ($#shortlist >= 0) { $short = 1; }
if ($long && $short) {
    # confused about whether this is short or long.
    # look for a short version of Create Initial panels (which is common
    # to all languages.
    @shortlist = <$dir/*_ci.*>;
    if ($#shortlist >= 0) { $long = 0; } else { $short = 0; }
}

if ($long) {exit 2;}
if ($short) {exit 1;}
exit 0;

#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

#  WptSubs.pl Version: @(#)WptSubs.pl	33.1 8/26/94 18:24:22
#****************************************************************************
#
#	WptSubs.pl >>> Subroutines to assist in writing GUI programs in taeperl.
#
#
#
#	CHANGE LOG:
#
#  30-mar-93	Copied from palm's directory...swd
#
#**************************************************************************
#
#       To use this, write one event handler
#	for each parm on each panel of the form:
#
#	sub panelName_parmName
#	    {
#	    local($parmValue, $panelId, $dataVm) = @_;
#	    --- your logic here --- 
#	    }

#
#	Main Event loop 
#
sub Wpt_EventLoop
    {
    while (1) 
	{
	($eventType, @eventData)  = &Wpt_NextEvent;
	eval "&handle_$eventType (\@eventData);";     # dispatch to type handler
	if ($@)
	    {print "no handler for $eventType\n";}
	}
    }

#
#	WPT_PARM_EVENT handler.  Most programs need only this
#	type of handler.
#

sub handle_WPT_PARM_EVENT
    {
    local($panelName, $parmName, $parmValue, $panelId, 
	$dataVm, $windowId, $userContext, $xEvent) = @_;
    # print "$panelName, $parmName, $parmValue, $panelId, 
    #    $dataVm, $windowId, $userContext\n";
    eval "&${panelName}_${parmName} (\$parmValue, \$panelId, \$dataVm)";
    if ($@)
	{
	print "cannot find event handler named ${panelName}_${parmName}\n";
	$@ = "";
	}
    }
#
#	You will probably want your own versions of these.
#	If you want these defined, set $DefineNonParmHandlers	
#	before the require of WptSubs.pl.
#
eval <<'end' if ($DefineNonParmHandlers);

    sub handle_WPT_TIMEOUT_EVENT
	{
	print "timeout event\n";
	}

    sub handle_WPT_WINDOW_EVENT
	{
	local($windowId, $xEvent) = @_;
	## bad idea when TAE terminating: print "WPT_WINDOW_EVENT: $windowId\n";
	}

    sub handle_WPT_FILE_EVENT
	{
	local ($fileSource, $fileEventType) = @_;
	print "WPT_FILE_EVENT: $fileSource $fileEventType\n";
	}

    sub handle_WPT_TIMER_EVENT
	{
	local ($timerId, $repeat, $interval, $userContext) = @_;
	print "WPT_TIMER_EVENT: $timerId, $repeat, $interval, $userContext\n";
	}
end

#
#	For writing quickie GUI.  Calling: 
#
#       &Wpt_Program($resFileName, $initialPanelName1, $initialPanelName2, ...);
#
#	This does all of the Wpt initialization required.
#	You must supply the event handlers described above.
#	A typical program might appear:
#
#
#		&Wpt_Program ("myApp.res", "mainPanel");
#
#		sub mainPanel_quit
#		    {
#		    exit;
#		    }
#		...
#
sub Wpt_Program
    {
    local($resFile, @panels) = @_;
    local($tar, $vu, $pid);
    &Wpt_Init || die "cannot connect to X server";
    if ($ScriptSocket)					# caller-settable
        {
	&Wpt_ScriptInit ($ScriptSocket) 
	    || print "Warning: cannot open script socket.\n"; 
	}
    $Co = &Co_New;
    &Co_ReadFile ($Co, $resFile) || die "cannot access $resFile" ;
    foreach $panel (@panels)
	{
	$tar = &Co_Find ($Co, $panel . "_t") || die "cannot find panel $panel";
	$vu = &Co_Find ($Co, $panel . "_v")  || die "cannot find panel $panel";
	$pid = &Wpt_NewPanel (0, $tar, $vu, 0, $panel, 0);
	die "cannot create panel $panel" if ($pid == 0);
	#
	#	Make the target, view, and panelId global
	#	variables named according to $panel.
	#	So, for panel "MainPanel", there are variables
	#	$MainPanelId, $MainPanelTarget, $MainPanelView.
	#
	#
	eval "\$${panel}Id = \$pid";
	eval "\$${panel}Target = \$tar";
	eval "\$${panel}View = \$vu";
	}
    &Wpt_EventLoop;
    }


1;

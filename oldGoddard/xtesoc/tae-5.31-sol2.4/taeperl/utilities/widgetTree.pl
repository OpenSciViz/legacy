#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# widgetTree.pl Version: @(#)widgetTree.pl	33.1 8/26/94 18:24:19
#****************************************************************************
#
#  widgetTree.pl >>> Remotely monitor TAE Plus parm events, display names/values
#
#
#	Usage:
# 		taeperl widgetTree.pl socketName [panelName]
#	    If panelName is provided, that is the root of the tree.  Otherwise
#	    all panels are treed.
#
#
#      CHANGE LOG:
#
#  29-mar-93   Copied from palm's directory...swd
#  21-jun-93   for compatibility with new Vm scheme delivered by 
#	       wptscript.cc's addWidget function; this allows
#	       widgetTree to show the tree when there are duplicate
#	       widget names...palm
#
#***************************************************************************/

require 'autsubs.pl';
if (!$ARGV[0])
    {
    print "usage: 'taeperl widgetTree.pl socketName [panel]'\n";
    exit;
    }
$SocketName = $ARGV[0];
$panelName = $ARGV[1] ? $ARGV[1] : "";

$s = &Aut'Connect($SocketName);
&Aut'SetDebug(0);
$response = &Aut'sendReceive ($s, 
		"function", "TAE_WidgetTree",
		"panelName", $panelName);
##&DumpVm ($response);
$retStr = &Vm_Get ($response, "_retStr");
if ($retStr)
    {print "error: $retStr\n";}			# error from aut 
else
    {&printLevel ($response, "tree");}		# display tree
&Aut'Close($s);
#
#	The application has delivered to us a Vm object that
#	specifies the widget tree.   There is one first level
#	symbol named "tree".  Each qualifier of "tree" 
#	represents a widget; qualifiers thereof represent the
#	next level of widgets, and so on.  The value of each 
#	qualifier is a string with with information about the 
#	widget (name, class, address, window, etc.)
#
sub printLevel {
    local($vm, $name) = @_;
    #print "$vm, $name\n";
    return if (!defined &Vm_Get ($vm, $name));	# return if problems
    $indent .= "  ";				# one level deeper
    local(@quals) = &Vm_Names ($vm, $name);
    foreach $q (@quals)				# for each qualifier:
	{
	$qname = "$name.$q";			# construct full qualifier name
	$info = &Vm_Get ($vm, $qname);		# get qualifier value
	print "${indent}$info\n";		# show widget info
	&printLevel ($vm, $qname);		# print next level
	}
    $indent =~ s/  //;				# one level less deep
    }

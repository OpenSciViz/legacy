#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# autsubs.pl Version: @(#)autsubs.pl	33.1 8/26/94
#****************************************************************************
#
#  autsubs.pl >>>
#
#
#      CHANGE LOG:
#
#  29-mar-93	Copied from palm's directory...swd
#  05-may-93	Name change:  SetLearn to SetRecord...swd
#  21-jun-93	return "do or die" error code for UserEvent(s)...palm
#  16-mar-94	pr-2644: Add main' prefix to Vm_Set call in FunctionCall...swd
#
#***************************************************************************/

#	Return code philosophy:
#
#	Most Aut subroutines return TRUE if things went okay,
#	and FALSE otherwise.   This allows the caller to use
#	the "do or die" paradigm.
#
#	Some Aut subroutines return a Vm object Id for the
#	Vm object returned from the AUT.   For such functions,
#	the caller must look inside the Vm and examine the
#	_retInt variable, which is SUCCESS (== 1) or 
#	otherwise (!= 1).   The &RetIntOk subroutines simplifies
#	the analysis of such a Vm.
#

package Aut;
#
#	package-global variables:
#
$Trace = 0;			# 0=do not trace script command
$Pace  = 1;			# secs between script commands
$Single = 0;			# 0=do not single cycle script commands
$Debug = 0;
$Sequence = 1;			# message sequence (gets loopbacked) 
$PrintErr = 1;			# print returned Vm errors to stdout

#
#	Make a Vm from pairs of "name, value" arguments.
#
sub makeVm {
    local(@vmData) = @_;
    local($vmId) = &main'Vm_New (P_CONT, @vmData);
    return $vmId;
    }

sub SetTrace		# 1 turns on trace mode 
    {
    ($Trace) = @_;
    }
sub SetPace		# arg = seconds to delay between script commands
    {
    ($Pace) = @_;
    }
sub SetSingle		# 1 causes single cycle
    {
    ($Single) = @_;
    }
sub SetDebug 
    { 
    ($Debug) = @_;
    ($Trace) = @_;
    }
sub SetPrintErr		# TRUE causes errors to be printed (default)
    {
    ($PrintErr) = @_;
    }

sub scriptStd		# standard stuff for a script command
    {
    local($panel, $parm, $value) = @_;   	# showing $value[0] only here
    local($response);
    local($p, $f, $l);
    if ($Pace)
	{
	sleep $Pace;
	}
    if ($Single)
	{
	### ($p, $f, $l) = caller;
	### print "package=$p, file=$f, line=$l\n";
	local($val) = (defined $value) ?  "to '$value'" : "";
	print "press RETURN to set $panel/$parm $val > ";
	$response = <STDIN>;
	}
    if ($Trace)
	{
	print "panel $panel, item $parm, value = '$value'\n";
	}
    }

sub SUCCESS {1;}

#
#	Print _retStr Vm variable if not empty.
#	This honors $PrintErr, which is Settable.
#
#	This returns TRUE if _retInt == SUCCESS.
#	Thus, callers can use the perl "do or die" paradigm.
#
#
sub printError
    {
    local($vmId) = @_;
    local($retStr) = &main'Vm_Get ($vmId, "_retStr");
    print "*****error: $retStr\n" if ($retStr && $PrintErr);
    local($retInt) = &main'Vm_Get ($vmId, "_retInt");
    return $retInt == &SUCCESS;		# TRUE if Wpt code is SUCCESS
    }

#
#	returns TRUE if _retInt variable has SUCCESS.
#
sub RetIntOk
    {
    local($vmId) = @_;
    local($retInt) = &main'Vm_Get ($vmId, "_retInt");
    return $retInt == &SUCCESS;
    }

sub DumpVm
    {				
    #print "\nvm dump: ";
    local($vmId) = @_;
    foreach (&main'Vm_Names($vmId)) 
	{
	print "\t$_: ",  
	      join (' ', &main'Vm_Get($vmId, $_)) . "\n" ;
        }
    }

#
#	sendReceive: send Vm to Aut; receive response from Aut.
#	arguments: ($socketId, itemName, itemValue, itemName, itemValue,...)
#	TBD: in trouble with floating values!!!
#	return: vmId of response; caller must free.
#
sub sendReceive 		
    {				
    local ($skt, @vmData) = @_;  		# ($socketId, vmPairs)
    local($vmId) = &makeVm (@vmData);		# construct output Vm
    if ($Debug)
	{
        print "sending:\n" if ($Debug);
        &DumpVm($vmId) if ($Debug);
	}
    &main'Vm_Set ($vmId, "_sequence", $Sequence);
    &main'Ipc_Send ($skt, $vmId) || die "cannot write to Aut. $!";
    &main'Vm_Free($vmId);				# free the output Vm
    ($vmId = &main'Ipc_Receive ($skt)) || die "cannot read from Aut. $!";
    die "Bad sequence number returned from AUT."
	if (&main'Vm_Get($vmId, "_sequence") != $Sequence);
    $Sequence++;
    if ($Debug)
	{
        print "received:\n" if ($Debug);
        &DumpVm($vmId) if ($Debug);
	}
    return $vmId;				# caller must free
    }
#
#	like sendReceive but caller has constructed the output Vm.
#	The caller is responsible for freeing the output and input Vm.
#
sub sendReceiveFromVm
    {
    local ($skt, $vmId) = @_;  			# ($socketId, $vmId)
    if ($Debug)
	{
        print "sending: \n" if ($Debug);
        &DumpVm($vmId) if ($Debug);
	}
    &main'Vm_Set ($vmId, "_sequence", $Sequence);
    &main'Ipc_Send ($skt, $vmId) || die "cannot write to Aut. $!";
    (local($resp) = &main'Ipc_Receive ($skt)) || die "cannot read from Aut. $!";
    die "Bad sequence number returned from AUT."
	if (&main'Vm_Get($vmId, "_sequence") != $Sequence);
    $Sequence++;
    if ($Debug)
	{
        print "receiving: \n" if ($Debug);
        &DumpVm($resp) if ($Debug);
	}
    return $resp;				# caller must free
    }

sub Connect
    {
    local($socketName) = @_;
    local($skt) = &main'Ipc_Connect ($socketName);
    die "cannot connect to $socketName. $!" if ($skt < 0);
    return $skt;
    }

sub Close
    {
    local($skt) = @_;
    &main'Ipc_Close ($skt);
    return 1;
    }

sub SetRecord 		# empty file name string disables record mode
    {
    local($skt, $file) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_setLearn",
		       "learnFile", $file);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub PanelMessage
    {
    local($skt, $panel, $message) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_panelMessage",
		       "panelName", $panel,
		       "message", $message);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

#
#	tell target process our pid (so it can kill us)
#
sub Begin {
    local($skt) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_scriptControl",
		       "pid", $$);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }
#
#	announce completion of script (zero the pid)
#
sub End 
    {
    local($skt) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_scriptControl",
		       "pid", 0);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }
#
#	send string to stdout of the scripted process	
#	for annotating test output
#
sub Print {
    local($skt, $string) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_stdout",
		       "string", $string);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }


#
#	Common processing for UserEvent, UserIntgEvent, ...;
#	$type may be "intg", "real", "string", or "infer"
#
sub userEventCommon 
    { 
    local($type, $skt, $panelName, $parmName, @value) = @_;
    &scriptStd ($panelName, $parmName, @value);
    local($vmId) = &main'Vm_New;
    &main'Vm_Set ($vmId, "function", "TAE_event");
    &main'Vm_Set ($vmId, "panelName", $panelName);
    &main'Vm_Set ($vmId, "parmName", $parmName);
    if ($type eq "string")				# TBD: record case tech
	{&main'Vm_SetString ($vmId, "value", @value);} 
    elsif ($type eq "intg")
	{&main'Vm_SetIntg ($vmId, "value", @value);} 
    elsif ($type eq "real")
	{&main'Vm_SetReal ($vmId, "value", @value);} 
    else 
	{&main'Vm_Set ($vmId, "value", @value);} 
    local($resp) = &sendReceiveFromVm($skt, $vmId);
    &main'Vm_Free($vmId);
    local($ret) = &printError($resp) ;
    &main'Vm_Free($resp) ;
    return $ret;
    }

sub UserEvent 
    {
    return &userEventCommon ("infer", @_);
    }

sub UserRealEvent {
    return &userEventCommon("real", @_);
    }

sub UserIntgEvent {
    return &userEventCommon("intg", @_);
    }

sub UserStringEvent {
    return &userEventCommon("string", @_);
    }

sub Nop
    {
    local($skt) = @_;
    local($resp) = &sendReceive ($skt, 
			"function", "TAE_nop");
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub GetTargetVm 
    {					# caller must free returned Vm object
    local($skt, $panelName) = @_;
    local($resp) = &sendReceive ($skt, 
			"function", "TAE_getTargetVm",
			"panelName", $panelName);
    &printError($resp);
    return $resp; 			# note: might have _retInt == FAIL !!
    }
#
#	note: caller must free the returned Vm object
#
sub FunctionCall
    {
    local($skt, $functionName, $vmId) = @_;
    local($resp);
    if ($vmId)			# vm specified?
	{
	&main'Vm_Set($vmId, "function", $functionName);
        $resp = &sendReceiveFromVm ($skt, $vmId);
	}
    else
	{
	$resp = &sendReceive ($skt,
			"function", $functionName);
	}
    &printError($resp);
    return $resp;			# note: might have _retInt == FAIL !!
    }

sub GetViewVm 
    {					# caller must free returned Vm object
    local($skt, $panelName) = @_;
    local($resp) = &sendReceive ($skt, 
			"function", "TAE_getViewVm",
			"panelName", $panelName);
    &printError($resp);
    return $resp; 			# note: might have _retInt == FAIL
    }

sub BeginWait {
    local($skt, $panelName) = @_;
    local($resp) = &sendReceive ($skt,
			"function", "TAE_beginWait",
			"panelName", $panelName);
    local($ret) = &printError ($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub EndWait {
    local($skt, $panelName) = @_;
    local($resp) = &sendReceive ($skt,
			"function", "TAE_endWait",
			"panelName", $panelName);
    local($ret) = &printError ($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub BeginWaitAll {return &BeginWait ("");}  # empty string means "all panels"

sub EndWaitAll   {return &EndWait ("");}    # empty string means "all panels"

sub SetItemSensitivity {
    local($skt, $panelName, $parmName, $sensitive) = @_;
    local($resp) = &sendReceive ($skt, 
		   "function", "TAE_SetItemSensitivity",
		   "panelName", $panelName, 
		   "parmName", $parmName,
		   "sensitive", $sensitive
		   ); 
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub SetAllSensitivity 	# for all items in the panel
    {
    local($skt, $panelName, $sensitive) = @_;
    local($resp) = &sendReceive ($skt, 
		   "function", "TAE_SetItemSensitivity",
		   "panelName", $panelName, 
		   "sensitive", $sensitive
		   ); 
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }


sub Messages 
    {
    local($skt, $enable) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", 
			   $enable ? "TAE_messagesOn" : "TAE_messagesOff" 
		   ); 
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub GetWindowId {
    local($skt, $panelName) = @_;
    local($resp) = &sendReceive ($skt, 
		 	"function", "TAE_getWindowId",
			"panelName", $panelName);
    local($windowId) = &main'Vm_Get ($resp, "Id");
    &printError($resp);
    &main'Vm_Free($resp);
    return $windowId; 			# might be undef (if unkown panel) 
    }
#
#	Note: caller must free the returned Vm.
#	TBD: the whole wait thing needs thought.
#	TBD: for example, waiting on one item on one panel
#		shouldn't have to free the returned value
#
sub Wait {			 	 # one panel, many parms, sensitivity
    local($skt, $panelName, @parmName) = @_; # note: multiple parm names allowed
    local(@panelVector) = ($panelName);  # make single element into a vector
    return &WaitWithVectors ($skt, *panelVector, *parmName, 0);
    }

#	Note: caller must free the returned Vm.

sub WaitSetSensitive {		 	 # one panel, many parms, sensitivity 
    local($skt, $panelName, @parmName) = @_; # note: multiple parm names allowed
    @panelVector = ($panelName);	 # make single element into a vector
    return &WaitWithVectors ($skt, *panelVector, *parmName, 1);
    }
#
#	wait on event(s) and return vm object;
#	(with name of item and panel that satisfied the wait);
#
#	Note: caller must free the returned Vm.
#
sub WaitWithVectors {		 # most functional form of ScriptWait 
    local($skt, *panelName, *parmName, $setSensitive) = @_;	
    local($vmId) = &main'Vm_New;
    &main'Vm_Set ($vmId, "function", "TAE_wait");
    &main'Vm_Set ($vmId, "panelName", @panelName); 	
    &main'Vm_Set ($vmId, "parmName", @parmName); 	
    &main'Vm_Set ($vmId, "setSensitive", $setSensitive); 	
    local($resp) = &sendReceiveFromVm($skt, $vmId);
    &main'Vm_Free ($vmId);
    &printError($resp);
    return $resp;   		# note: caller must free; _retInt may be FAIL 
    }

sub Break  
    {
    local($skt) = @_;
    local($resp) = &sendReceive ($skt, 
		       "function", "TAE_break"
		       );
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub WalkChoices  
    {
    local ($skt, $panel, $menu) = @_;
    local($vmId) = &GetTargetVm ($skt, $panel);         # get target Vm
    local($choiceCount) = 
	&main'Vm_GetValidString ($vmId, $menu);		# get valids count
    @choices =     
	&main'Vm_GetValidString ($vmId, $menu);		# get valids
    # print "choiceCount = $choiceCount\n";
    # print "choices=", @choices; print "\n";
    local($ret) = 1;					# assume good return
    local($localRet);
    foreach $choice (@choices) 
	{
	$localRet = &UserEvent ($skt, $panel, $menu, $choice) if ($choice);
	$ret = 0 if (!$localRet);
	sleep $Pace if ($Pace);
	}
    &main'Vm_Free ($vmId);
    return  $ret;			# note: FALSE if any one item failed
    }
#
#	here, caller owns and frees the Vm
#
sub SetValuesFromVm {
    local($skt, $vmId) = @_;
    &main'Vm_Set ($vmId, "function", "TAE_setValues");
    local($resp) = &sendReceiveFromVm ($skt, $vmId);
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }
#
#	here, caller supplies data via name/value pairs.
#
sub SetValues {
    local($skt, @pairs) = @_;		# name, value pairs; no reals !!!
    local($vmId) = &makeVm (@pairs); 
    local($ret) = &SetValuesFromVm ($vmId);
    &main'Vm_Free ($vmId);
    return $ret;
    }

#
#	X event types for scripting TAE Plus workspaces 
#	with WidgetAction
#	
sub KeyPress      {2;}
sub KeyRelease    {3;}
sub ButtonPress   {4;}
sub ButtonRelease {5;}
sub MotionNotify  {6;}
sub Button1 {1;}
sub Button2 {2;}
sub Button2 {3;}
#
#	X event state masks: 
#
sub ShiftMask     {1<<0;}
sub LockMask      {1<<1;}
sub ControlMask   {1<<2;}
#
#	scripting subroutines for workspaces 
#	(in Motif terms: "DA" = "drawing areas")
#

sub DAButton {
    local($event, $panelName, $widgetName, $x, $y) = @_;
    return &WidgetAction (
	$panelName, 
	$widgetName, 			# widget name
	"DrawingAreaInput", 		# drawing area action
	"",				# action arg
	"type", $event,			# XEvent stuff
	"button", &Button1,		# left button
	"x", $x,
	"y", $y
	);
    }

#
#	widget level scripting; 	
#	@eventData consists of pairs of "name" and value
#	representing members of the simulated XButtonEvent.
#
sub WidgetAction {
    local ($skt, $panelName, $widgetName, $action, $arg, @eventData) = @_;  
    local($resp) = &sendReceive($skt,
		"function",   "TAE_WidgetAction",
		"panelName",  $panelName,          # panel containing the widget
		"widgetName", $widgetName,         # in XtNameToWidget format
		"actionName", $action,
		"actionArg",  $arg ? $arg : "",
		@eventData);                       # name/value event pairs
    local($ret) = &printError($resp);
    &main'Vm_Free($resp);
    return $ret;
    }

sub WidgetSetValues {
    local ($skt, $panelName, $widgetName, @resources) = @_;  	
    local ($vm) = &makeVm (
	"function", "TAE_WidgetSetValues",
	"panelName",  $panelName,		# panel containing the widget
	"widgetName", $widgetName);		# in XtNameToWidget format
    &main'Vm_Set ($vm, "resource", 0);		# parent container for... 
    local($i);
    for ($i = 0; $i <  $#resources; )		# for each name/value pair
	{
	#	Make qualifier to the "resource" symbol:
	#	"resource.1" is the resource name,
	#       "resource.1.value" is the resource value
	#	and so on.
	#
	&main'Vm_Set ($vm, "resource.$i", $resources[$i]);         # res name 
	&main'Vm_Set ($vm, "resource.$i.value", $resources[$i+1]); # res value
	$i = $i + 2;
	}
    local($resp) = &sendReceiveFromVm ($skt, $vm);
    &main'Vm_Free($vm);
    local($ret) = &printError ($resp);
    &main'Vm_Free($resp);
    return $ret;
    }
1;

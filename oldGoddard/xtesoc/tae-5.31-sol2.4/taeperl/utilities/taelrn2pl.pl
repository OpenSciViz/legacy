#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# taelrn2pl.pl Version: @(#)taelrn2pl.pl	33.1 8/26/94 18:24:26
#****************************************************************************
#
#  taelrn2pl.pl >>> Convert record (learn) file to script
#
#
#	Usage:
# 		taeperl taelrn2pl.pl recordFileName > ScriptFileName
#
#
#      CHANGE LOG:
#
#  29-mar-93   Copied from palm's directory...swd
#  24-aug-93   Change "learn" references to "record"; fix typo;
#              added recordFile to output header...kbs
#  26-aug-93   fixed the arg to Aut'Close...palm
#
#***************************************************************************/

$recordFile = $ARGV[0];
&setTypeMap;
open (IN, $recordFile) || die "cannot open record file";
$hdr = <IN>;
($sentinel, $decTime, $socketName, $time) = split ("\034", $hdr);
&writePrefix;
while (<IN>)
    {
    ($secs, $type, $panel, $item, $value) = split ("\034");
    print qq!$typeMap{$type} (\$skt, "$panel", "$item", $value);\n!
	if ($typeMap{$type});
    }
&writeSuffix;

sub writePrefix
    {
    print <<end;
#
# script generated from record file $recordFile dated $time
#
require 'autsubs.pl';                # AUT subroutine package
&Aut'SetPace(1);		     # one second delay between events
&Aut'SetTrace(0);		     # turn off stdout trace of events
&Aut'SetSingle(0);		     # turn off single cycle option
\$skt = &Aut'Connect ("$socketName");# open connection to application 

end
    }


sub writeSuffix
    {
    print <<end;
    
&Aut'Close (\$skt);			# close connection to application
end
    }

#
#	maps script subroutine name to event type 
#
sub setTypeMap
    {
    $typeMap{"IntgEvent"} = "&Aut'UserIntgEvent";
    $typeMap{"StrEvent"} =  "&Aut'UserStringEvent";
    $typeMap{"RealEvent"} = "&Aut'UserRealEvent";
    }

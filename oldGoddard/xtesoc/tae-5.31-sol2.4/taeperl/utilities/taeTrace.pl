#############################################################################
#
#      Copyright (c) 1993
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

# taeTrace.pl Version: @(#)taeTrace.pl	33.1 8/26/94 18:24:28
#****************************************************************************
#
#  taeTrace.pl >>> Remotely monitor TAE Plus parm events, display names/values
#
#
#	Usage:
# 		taeperl taetrace.pl socketName
#
#
#      CHANGE LOG:
#
#  29-mar-93   Copied from palm's directory...swd
#
#***************************************************************************/

require 'autsubs.pl';

$socketName = $ARGV[0]; 				# view to monitor
die "usage: 'taeperl taetrace.pl socketName'" if (!$socketName);
$skt = &Aut'Connect ($socketName);			# connect to cs
print "tracing user events for '$socketName'...\n";

while (1) {
    $vmId = &Aut'WaitWithVectors ($skt, @wildPanel, @wildParm); # wait for any 
    $panelName = &Vm_Get ($vmId, "_panelName");
    die "invalid response from application." if (!$panelName);
    $parmName = &Vm_Get ($vmId, "_parmName");
    die "invalid response from application." if (!$parmName);
    @value = &Vm_Get ($vmId, $parmName);
    print "panel $panelName, item $parmName";
    if ($#value < 0)				# no value
	{print "\n";}
    elsif ($#value == $[)			# one value
	{print qq!, value="$value[0]"\n!;}
    elsif ($#value > $[)			# multi values
        {
        print ", values:\n";
        for ($i=0; $i <= $#value; $i++)
	    {print qq!\t"$value[$i]"\n!;}
        }
    &Vm_Free($vmId);
    }

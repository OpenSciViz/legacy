
#############################################################################
#
#      Copyright (c) 1994
#      Century Computing, Inc.
#      ALL RIGHTS RESERVED
#
#      The software (programs, data bases and/or documentation) on or in
#      any media can not be reproduced, disclosed, or used except under
#      the terms of the License Agreement between Century Computing, Inc. 
#      and your organization.
#
#############################################################################

#
# perl script to convert TAE Plus function name to truncated filename.
# Invoked by $TAEBIN/taeman.
#
# NOTE: This script is ONLY needed for systems which impose a 14-character
#       limit on filenames. 
#
# CHANGE LOG:
# 07-apr-94 Initially created...kbs
# 11-apr-94 Revised the 97 "#trunc" (truncated) names...kbs
# 21-apr-94 Concurrent has brain damaged "man" cmd, so do "man -";
#           added "-m" option to dump man page mapping table;
#           add details to usage info...kbs
# 20-jun-94 Due to problems with tables, simply more the cat versions
#           rather than using the man command...kbs,df

#
# Index table by longnames; right side is shortened names (or exact same).
# Keep this table in sorted order. Note that aliases must be included.
#
#
#       CAUTION:
#               If you change anything in the following table,
#               you must ALSO change $TAETOOLS/truncman.sh
#               which has a similar list (but with filenames, not API names).
#               This is ugly, but seemingly necessary.
#               Note that compositecode.5 and wptcloseitems.5 are 2
#               special case which must appear in truncman.sh but are not 
#               needed here because they are included files which
#               have no public API. (eg, we say "man Wpt_CloseItems", not
#               "man wptcloseitems").

# Left = Name user supplies	      # Right = (Possibly) Truncated filename
#
$MANPAGE_TABLE{"Co"}				= "Co";
$MANPAGE_TABLE{"Co_"}				= "Co_";
$MANPAGE_TABLE{"Co_Add"}			= "Co_Add";
$MANPAGE_TABLE{"Co_Find"}			= "Co_Find";
$MANPAGE_TABLE{"Co_ForEach"}			= "Co_ForEach";
$MANPAGE_TABLE{"Co_Free"}			= "Co_Free";
$MANPAGE_TABLE{"Co_Intro"}			= "Co_Intro";
$MANPAGE_TABLE{"Co_New"}			= "Co_New";
$MANPAGE_TABLE{"Co_ReadFile"}			= "Co_ReadFile";
$MANPAGE_TABLE{"Co_Remove"}			= "Co_Remove";
$MANPAGE_TABLE{"Co_WriteFile"}			= "Co_WriteFile";
$MANPAGE_TABLE{"Co_intro"}			= "Co_intro";
$MANPAGE_TABLE{"IVAL"}				= "IVAL";
$MANPAGE_TABLE{"IntParm"}			= "IntParm";
$MANPAGE_TABLE{"RVAL"}				= "RVAL";
$MANPAGE_TABLE{"RealParm"}			= "RealParm";
$MANPAGE_TABLE{"SVAL"}				= "SVAL";
$MANPAGE_TABLE{"StringParm"}			= "StringParm";
$MANPAGE_TABLE{"TAE"}				= "TAE";
$MANPAGE_TABLE{"TAE_DDO"}			= "TAE_DDO";
$MANPAGE_TABLE{"TAE_DDO_intro"}			= "TAEDDOintro"; #trunc
$MANPAGE_TABLE{"TAE_X_workspace"}		= "TAEXworkspac"; #trunc
$MANPAGE_TABLE{"TAE_button"}			= "TAE_button";
$MANPAGE_TABLE{"TAE_checkbox"}			= "TAE_checkbox";
$MANPAGE_TABLE{"TAE_color_logger"}		= "TAEcolorLog"; #trunc
$MANPAGE_TABLE{"TAE_compDDO"}			= "TAE_compDDO"; 
$MANPAGE_TABLE{"TAE_compddo"}			= "TAE_compddo";
$MANPAGE_TABLE{"TAE_composite"}			= "TAEcomposite"; #trunc
$MANPAGE_TABLE{"TAE_composite_DDO"}		= "TAEcompDDO"; #trunc
$MANPAGE_TABLE{"TAE_composite_ddo"}		= "TAEcompddo"; #trunc
$MANPAGE_TABLE{"TAE_data_driven_object"}	= "TAEdataDO"; #trunc
$MANPAGE_TABLE{"TAE_data_driven_objects"}	= "TAEdataDOs"; #trunc
$MANPAGE_TABLE{"TAE_ddo"}			= "TAE_ddo";
$MANPAGE_TABLE{"TAE_ddo_intro"}			= "TAEddointro"; #trunc
$MANPAGE_TABLE{"TAE_ddos"}			= "TAE_ddos";
$MANPAGE_TABLE{"TAE_demos"}			= "TAE_demos";
$MANPAGE_TABLE{"TAE_discrete"}			= "TAE_discrete";
$MANPAGE_TABLE{"TAE_discrete_pictures"}		= "TAEdiscPict"; #trunc
$MANPAGE_TABLE{"TAE_dynamic_data_object"}	= "TAEDDO"; #trunc
$MANPAGE_TABLE{"TAE_dynamic_data_objects"}	= "TAEDDOs"; #trunc
$MANPAGE_TABLE{"TAE_dynamic_text"}		= "TAEdynText"; #trunc
$MANPAGE_TABLE{"TAE_dynlabel"}			= "TAE_dynlabel";
$MANPAGE_TABLE{"TAE_dyntext"}			= "TAE_dyntext";
$MANPAGE_TABLE{"TAE_file_selection"}		= "TAEfileSelec"; #trunc
$MANPAGE_TABLE{"TAE_filesel"}			= "TAE_filesel";
$MANPAGE_TABLE{"TAE_icon"}			= "TAE_icon";
$MANPAGE_TABLE{"TAE_intro"}			= "TAE_intro";
$MANPAGE_TABLE{"TAE_keyin"}			= "TAE_keyin";
$MANPAGE_TABLE{"TAE_label"}			= "TAE_label";
$MANPAGE_TABLE{"TAE_list"}			= "TAE_list";
$MANPAGE_TABLE{"TAE_menubar"}			= "TAE_menubar";
$MANPAGE_TABLE{"TAE_message"}			= "TAE_message";
$MANPAGE_TABLE{"TAE_mover"}			= "TAE_mover";
$MANPAGE_TABLE{"TAE_multi_line_edit"}		= "TAEmultiLinE"; #trunc
$MANPAGE_TABLE{"TAE_multiline_edit"}		= "TAEmultiline"; #trunc
$MANPAGE_TABLE{"TAE_option"}			= "TAE_option";
$MANPAGE_TABLE{"TAE_option_menu"}		= "TAEoptionMen"; #trunc
$MANPAGE_TABLE{"TAE_page_edit"}			= "TAEpageEdit"; #trunc
$MANPAGE_TABLE{"TAE_pageedit"}			= "TAE_pageedit";
$MANPAGE_TABLE{"TAE_panel"}			= "TAE_panel";
$MANPAGE_TABLE{"TAE_pulldown"}			= "TAE_pulldown";
$MANPAGE_TABLE{"TAE_pulldown_menu"}		= "TAEpulldownM"; #trunc
$MANPAGE_TABLE{"TAE_push_button"}		= "TAEpushBtn"; #trunc
$MANPAGE_TABLE{"TAE_radio"}			= "TAE_radio";
$MANPAGE_TABLE{"TAE_radio_button"}		= "TAEradioBtn"; #trunc
$MANPAGE_TABLE{"TAE_radio_buttons"}		= "TAEradioBtns"; #trunc
$MANPAGE_TABLE{"TAE_rotator"}			= "TAE_rotator";
$MANPAGE_TABLE{"TAE_scale"}			= "TAE_scale";
$MANPAGE_TABLE{"TAE_scroller"}			= "TAE_scroller";
$MANPAGE_TABLE{"TAE_selection_list"}		= "TAEselecList"; #trunc
$MANPAGE_TABLE{"TAE_slider"}			= "TAE_slider";
$MANPAGE_TABLE{"TAE_static"}			= "TAE_static";
$MANPAGE_TABLE{"TAE_stretcher"}			= "TAEstretcher"; #trunc
$MANPAGE_TABLE{"TAE_strip_chart"}		= "TAEstripChar"; #trunc
$MANPAGE_TABLE{"TAE_stripchart"}		= "TAEstripchar"; #trunc
$MANPAGE_TABLE{"TAE_text"}			= "TAE_text";
$MANPAGE_TABLE{"TAE_text_display"}		= "TAEtextDisp"; #trunc
$MANPAGE_TABLE{"TAE_text_list"}			= "TAEtextList"; #trunc
$MANPAGE_TABLE{"TAE_textdisp"}			= "TAE_textdisp";
$MANPAGE_TABLE{"TAE_tools"}			= "TAE_tools";
$MANPAGE_TABLE{"TAE_workspace"}			= "TAEworkspace"; #trunc
$MANPAGE_TABLE{"TAE_x_workspace"}		= "TAExWorkspac"; #trunc
$MANPAGE_TABLE{"Vm"}				= "Vm";
$MANPAGE_TABLE{"Vm_"}				= "Vm_";
$MANPAGE_TABLE{"Vm_Call"}			= "Vm_Call";
$MANPAGE_TABLE{"Vm_Copy"}			= "Vm_Copy";
$MANPAGE_TABLE{"Vm_DynTutor"}			= "Vm_DynTutor";
$MANPAGE_TABLE{"Vm_Find"}			= "Vm_Find";
$MANPAGE_TABLE{"Vm_FindVar"}			= "Vm_FindVar";
$MANPAGE_TABLE{"Vm_ForEach"}			= "Vm_ForEach";
$MANPAGE_TABLE{"Vm_FormatVar"}			= "Vm_FormatVar";
$MANPAGE_TABLE{"Vm_Free"}			= "Vm_Free";
$MANPAGE_TABLE{"Vm_GetAttribute"}		= "VmGetAttrib"; #trunc
$MANPAGE_TABLE{"Vm_GetHostError"}		= "VmGetHostErr"; #trunc
$MANPAGE_TABLE{"Vm_GetValidIntg"}		= "VmGetValidI"; #trunc
$MANPAGE_TABLE{"Vm_GetValidReal"}		= "VmGetValidR"; #trunc
$MANPAGE_TABLE{"Vm_GetValidString"}		= "VmGetValidS"; #trunc
$MANPAGE_TABLE{"Vm_InitFormat"}			= "VmInitForm"; #trunc
$MANPAGE_TABLE{"Vm_Intro"}			= "Vm_Intro";
$MANPAGE_TABLE{"Vm_New"}			= "Vm_New";
$MANPAGE_TABLE{"Vm_OpenStdout"}			= "VmOpenStdout"; #trunc
$MANPAGE_TABLE{"Vm_ReadFromDisk"}		= "VmReadFromDk"; #trunc
$MANPAGE_TABLE{"Vm_ReadFromTM"}			= "VmReadFromTM"; #trunc
$MANPAGE_TABLE{"Vm_SetIntg"}			= "Vm_SetIntg";
$MANPAGE_TABLE{"Vm_SetMax"}			= "Vm_SetMax";
$MANPAGE_TABLE{"Vm_SetMin"}			= "Vm_SetMin";
$MANPAGE_TABLE{"Vm_SetNextMenu"}		= "VmSetNextMen"; #trunc
$MANPAGE_TABLE{"Vm_SetParmPage"}		= "VmSetParmPag"; #trunc
$MANPAGE_TABLE{"Vm_SetReal"}			= "Vm_SetReal";
$MANPAGE_TABLE{"Vm_SetString"}			= "Vm_SetString";
$MANPAGE_TABLE{"Vm_SetStringLength"}		= "VmSetStringL"; #trunc
$MANPAGE_TABLE{"Vm_SetTCLVar"}			= "Vm_SetTCLVar";
$MANPAGE_TABLE{"Vm_SetValidIntg"}		= "VmSetValidI"; #trunc
$MANPAGE_TABLE{"Vm_SetValidReal"}		= "VmSetValidR"; #trunc
$MANPAGE_TABLE{"Vm_SetValidString"}		= "VmSetValidS"; #trunc
$MANPAGE_TABLE{"Vm_WriteToDisk"}		= "VmWriteToDis"; #trunc
$MANPAGE_TABLE{"Vm_intro"}			= "Vm_intro";
$MANPAGE_TABLE{"Wpt"}				= "Wpt";
$MANPAGE_TABLE{"Wpt_"}				= "Wpt_";
$MANPAGE_TABLE{"Wpt_AddEvent"}			= "Wpt_AddEvent";
$MANPAGE_TABLE{"Wpt_AddTimer"}			= "Wpt_AddTimer";
$MANPAGE_TABLE{"Wpt_BeginWait"}			= "WptBeginWait"; #trunc
$MANPAGE_TABLE{"Wpt_CloseDisplay"}		= "WptCloseDisp"; #trunc
$MANPAGE_TABLE{"Wpt_CloseItems"}		= "WptCloseItem"; #trunc
$MANPAGE_TABLE{"Wpt_EndWait"}			= "Wpt_EndWait";
$MANPAGE_TABLE{"Wpt_Finish"}			= "Wpt_Finish";
$MANPAGE_TABLE{"Wpt_GetConstraintSensitivity"}	= "WptGetConSen"; #trunc
$MANPAGE_TABLE{"Wpt_GetIntgConstraints"}	= "WptGetIntCon"; #trunc
$MANPAGE_TABLE{"Wpt_GetItemSensitivity"}	= "WptGetItmSen"; #trunc
$MANPAGE_TABLE{"Wpt_GetMenu"}			= "Wpt_GetMenu";
$MANPAGE_TABLE{"Wpt_GetMenuEntryPath"}		= "WptGetMenuEP"; #trunc
$MANPAGE_TABLE{"Wpt_GetPanelState"}		= "WptGetPanelS"; #trunc
$MANPAGE_TABLE{"Wpt_GetRealConstraints"}	= "WptGetReaCon"; #trunc
$MANPAGE_TABLE{"Wpt_GetStringConstraints"}	= "WptGetStrCon"; #trunc
$MANPAGE_TABLE{"Wpt_HideItem"}			= "Wpt_HideItem";
$MANPAGE_TABLE{"Wpt_Init"}			= "Wpt_Init";
$MANPAGE_TABLE{"Wpt_InsertMenuEntries"}		= "WptInsMenuE"; #trunc
$MANPAGE_TABLE{"Wpt_Intro"}			= "Wpt_Intro";
$MANPAGE_TABLE{"Wpt_IsScripted"}		= "WptIsScript"; #trunc
$MANPAGE_TABLE{"Wpt_ItemWindow"}		= "WptItemWin"; #trunc
$MANPAGE_TABLE{"Wpt_KeyinCheckForNULLInput"}	= "WptKeyinCFNI"; #trunc
$MANPAGE_TABLE{"Wpt_MenuIntro"}			= "WptMenuIntro"; #trunc
$MANPAGE_TABLE{"Wpt_MenuUpdate"}		= "WptMenuUpdat"; #trunc
$MANPAGE_TABLE{"Wpt_MissingVal"}		= "WptMissingVa"; #trunc
$MANPAGE_TABLE{"Wpt_NewPanel"}			= "Wpt_NewPanel";
$MANPAGE_TABLE{"Wpt_NextEvent"}			= "WptNextEvent"; #trunc
$MANPAGE_TABLE{"Wpt_OpenDisplay"}		= "WptOpenDisp"; #trunc
$MANPAGE_TABLE{"Wpt_PanelDisplayId"}		= "WptPanDispId"; #trunc
$MANPAGE_TABLE{"Wpt_PanelErase"}		= "WptPanErase"; #trunc
$MANPAGE_TABLE{"Wpt_PanelMessage"}		= "WptPanMsg"; #trunc
$MANPAGE_TABLE{"Wpt_PanelReset"}		= "WptPanReset"; #trunc
$MANPAGE_TABLE{"Wpt_PanelTopWindow"}		= "WptPanTopWin"; #trunc
$MANPAGE_TABLE{"Wpt_PanelWidgetId"}		= "WptPanWidgId"; #trunc
$MANPAGE_TABLE{"Wpt_PanelWindow"}		= "WptPanWindow"; #trunc
$MANPAGE_TABLE{"Wpt_ParmReject"}		= "WptParmRejec"; #trunc
$MANPAGE_TABLE{"Wpt_ParmUpdate"}		= "WptParmUpdat"; #trunc
$MANPAGE_TABLE{"Wpt_Pending"}			= "Wpt_Pending";
$MANPAGE_TABLE{"Wpt_Rehearse"}			= "Wpt_Rehearse";
$MANPAGE_TABLE{"Wpt_RemoveEvent"}		= "WptRemEvent"; #trunc
$MANPAGE_TABLE{"Wpt_RemoveMenuEntries"}		= "WptRemMenuEn"; #trunc
$MANPAGE_TABLE{"Wpt_RemoveTimer"}		= "WptRemTimer"; #trunc
$MANPAGE_TABLE{"Wpt_ScriptInit"}		= "WptScriptIni"; #trunc
$MANPAGE_TABLE{"Wpt_ScriptUnsup"}		= "WptScriptUns"; #trunc
$MANPAGE_TABLE{"Wpt_SetConstraintSensitivity"}	= "WptSetConSen"; #trunc
$MANPAGE_TABLE{"Wpt_SetHelpStyle"}		= "WptSetHelpSt"; #trunc
$MANPAGE_TABLE{"Wpt_SetIntg"}			= "Wpt_SetIntg";
$MANPAGE_TABLE{"Wpt_SetIntgConstraints"}	= "WptSetIntCon"; #trunc
$MANPAGE_TABLE{"Wpt_SetItemSensitivity"}	= "WptSetItmSen"; #trunc
$MANPAGE_TABLE{"Wpt_SetMenu"}			= "Wpt_SetMenu";
$MANPAGE_TABLE{"Wpt_SetMenuSensitivity"}	= "WptSetMenuSe"; #trunc
$MANPAGE_TABLE{"Wpt_SetNoValue"}		= "WptSetNoValu"; #trunc
$MANPAGE_TABLE{"Wpt_SetPanelState"}		= "WptSetPanelS"; #trunc
$MANPAGE_TABLE{"Wpt_SetReal"}			= "Wpt_SetReal";
$MANPAGE_TABLE{"Wpt_SetRealConstraints"}	= "WptSetReaCon"; #trunc
$MANPAGE_TABLE{"Wpt_SetRecord"}			= "WptSetRecord"; #trunc
$MANPAGE_TABLE{"Wpt_SetString"}			= "WptSetString"; #trunc
$MANPAGE_TABLE{"Wpt_SetStringConstraints"}	= "WptSetStrCon"; #trunc
$MANPAGE_TABLE{"Wpt_SetTimeOut"}		= "WptSetTimeOu"; #trunc
$MANPAGE_TABLE{"Wpt_ShowItem"}			= "Wpt_ShowItem";
$MANPAGE_TABLE{"Wpt_ViewUpdate"}		= "WptViewUpdat"; #trunc
$MANPAGE_TABLE{"Wpt_intro"}			= "Wpt_intro";
$MANPAGE_TABLE{"Xtae"}				= "Xtae";
$MANPAGE_TABLE{"XtaeDDO"}			= "XtaeDDO";
$MANPAGE_TABLE{"XtaeDDOArea"}			= "XtaeDDOArea";
$MANPAGE_TABLE{"XtaeDDO_intro"}			= "XtaeDDOintro"; #trunc
$MANPAGE_TABLE{"XtaeDiscretePictures"}		= "XtaeDiscrete"; #trunc
$MANPAGE_TABLE{"XtaeMover"}			= "XtaeMover";
$MANPAGE_TABLE{"XtaeRotator"}			= "XtaeRotator";
$MANPAGE_TABLE{"XtaeStretcher"}			= "XtaeStretch"; #trunc
$MANPAGE_TABLE{"XtaeStripchart"}		= "XtaeStrip"; #trunc
$MANPAGE_TABLE{"Xtae_intro"}			= "Xtae_intro";
$MANPAGE_TABLE{"autsubs"}			= "autsubs";
$MANPAGE_TABLE{"f_force_lower"}			= "fforcelower"; #trunc
$MANPAGE_TABLE{"fontalias"}			= "fontalias";
$MANPAGE_TABLE{"genupgrade"}			= "genupgrade";
$MANPAGE_TABLE{"iconconvert"}			= "iconconvert";
$MANPAGE_TABLE{"par2pdf"}			= "par2pdf";
$MANPAGE_TABLE{"parupgrade"}			= "parupgrade";
$MANPAGE_TABLE{"rec2pl"}			= "rec2pl";
$MANPAGE_TABLE{"res2rfg"}			= "res2rfg";
$MANPAGE_TABLE{"res2uil"}			= "res2uil";
$MANPAGE_TABLE{"resupgrade"}			= "resupgrade";
$MANPAGE_TABLE{"rfg2res"}			= "rfg2res";
$MANPAGE_TABLE{"s_equal"}			= "s_equal";
$MANPAGE_TABLE{"t_pinit"}			= "t_pinit";
$MANPAGE_TABLE{"tae_alloc"}			= "tae_alloc";
$MANPAGE_TABLE{"tae_free"}			= "tae_free";
$MANPAGE_TABLE{"taec++one"}			= "taec++one";
$MANPAGE_TABLE{"taeccone"}			= "taeccone";
$MANPAGE_TABLE{"taecg"}				= "taecg";
$MANPAGE_TABLE{"taeidraw"}			= "taeidraw";
$MANPAGE_TABLE{"taeperl"}			= "taeperl";
$MANPAGE_TABLE{"taeplayback"}			= "taeplayback";
$MANPAGE_TABLE{"taerecord"}			= "taerecord";
$MANPAGE_TABLE{"taesetup"}			= "taesetup";
$MANPAGE_TABLE{"taetm"}				= "taetm";
$MANPAGE_TABLE{"taewb"}				= "taewb";

	### TBD these 3 (can't hurt)
$MANPAGE_TABLE{"imake"}				= "imake";
$MANPAGE_TABLE{"patch"}				= "patch";
$MANPAGE_TABLE{"perl"}				= "perl";

####---------------------- END MANPAGE_TABLE -------------------------------

$PROG="taeman"; # intentionally omit the .pl

if ( $ARGV[0] eq "-k" )
    {
    system ( "man -k $ARGV[1]" );
    exit ;
    }
elsif ( $ARGV[0] eq "-m" )
    {
    print "           Table of TAE Plus Man Pages\n";
    print "           ===========================\n\n";
    printf "Note:        The table contains aliases for certain man pages. \n";
    printf "For example, Wpt, Wpt_, wpt_Intro, and Wpt_intro each invoke the same man page.\n\n";
    printf "%-30s %-15s\n", "Documented API Name", "Actual Man Page" ;
    printf "%-30s %-15s\n", "-------------------", "---------------" ;
    ## Iterate thru table
    ## while ( ($key, $value) = each %MANPAGE_TABLE )
    # Sort and then print (normal iteration doesn't preverse table order :-( )
    foreach $key ( sort keys %MANPAGE_TABLE )
        {
	# formatted output
        printf "%-30s %-15s\n", $key, $MANPAGE_TABLE{$key} ;
	## Use next instead of above if using 'while' iteration.
        ## printf "%-30s %-15s\n", $key, $value ;
        }
    exit ;
    }
elsif ( $ARGV[0] =~ /^-/) # reject other options
    {
    print "$PROG: Valid options are:\n\n";
    print "-k     display one-line description\n";
    print "       e.g.:         'taeman -k Wpt_NewPanel'\n";
    print "       or display description of similarly named pages\n";
    print "       e.g.:         'taeman -k Wpt_Get'\n\n";
    print "-m     show mapping of man page names\n";
    exit ;
    }

# Lookup name in table.
$MANPAGE = $MANPAGE_TABLE{ $ARGV[0] } ;

if ( length ( $MANPAGE ) > 0 )	# found in table
    {
    $TAE = $ENV{TAE};
    $platform = $ENV{TAEPLAT};
    if (!defined ($platform)  )
	{
	print "\$TAEPLAT is not defined. \nSee 'Getting Started with TAE Plus' to setup your environment.\n";
	exit;
	}
    
    if ( $platform eq "rtu" ) # Concurrent/Masscomp
	{	
	## Don't use "man" on Concurrent due to problems with tables.
	## Just cat the "cat" versions.
	## system ( "man - $MANPAGE" ); 
	if ( $MANPAGE  =~ /^Xtae/ )
	    {
	    $Filename=$TAE . "/Xtae/man/cat3/" . $MANPAGE . ".3" ;
	    system ( "more $Filename" ); 
	    }
	else 
        # It is in $TAE/man in one of 3 directories: cat1, cat3, or cat5.
	# Rather than checking for filename patterns, if the file exists,
	# we must have found the correct directory.
	    {
	    $Filename=$TAE . "/man/cat1/" . $MANPAGE . ".1" ;
	    if ( ! -s $Filename )
	        {
	        $Filename=$TAE . "/man/cat3/" . $MANPAGE . ".3" ;
	        if ( ! -s $Filename )
	            {
	            $Filename=$TAE . "/man/cat5/" . $MANPAGE . ".5" ;
		    }
		}
	    system ("more $Filename") ;
	    } # regular $TAE/man
	} # Concurrent
    else # Every other reasonable UNIX platform we've encountered.
	{ 
	system ( "man $MANPAGE" ); 
	}
    }
else
    {
    print "$PROG: Unrecognized TAE Plus man page name: '$ARGV[0]'.\n";
    print "        Check spelling or try 'taeman -k Some_Substring'.\n";
    }
exit ;

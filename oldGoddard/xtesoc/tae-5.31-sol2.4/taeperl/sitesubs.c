/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


static char sitesubs_cVersion[] = "@(#)sitesubs.c	34.1 8/31/94";
/****************************************************************************
*****************************************************************************
**
**  sitesubs.c >>> Stub userinit1().  Replace w/ site-specific subroutines
**
**
**  REVISIONS:
**  29-jan-93  Copied from /net/fox/home1/pm/tae/script/lib ...swd
**  28-jul-93  Copied from $TAESRC/taeperl to $TAE/taeperl...swd
**  
*****************************************************************************
****************************************************************************/
void userinit1()
{
/*	placeholder for site-specific subroutine registration.
 *	Sites replace this module with their own additions
 *	to tperl.  Follow instructions that come with perl. 
 */

}

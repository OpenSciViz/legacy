/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


//
// This file contains TaeItem Class defintions
//
// 28-feb-92 Created...tpl
// 22-apr-92 Capitalized name and parent...tpl
// 11-may-92 Added menubar methods...tpl
// 15-jun-92 PR1464 React method moved to protected...tpl
// 18-aug-92 PR1512, PR1513 Revised C++ bindings to make programmer's life 
//	     easier, more productive and have good family values...krw
// 12-oct-92 Class name may not be left out unless there is exactly one
//	     immediate base class.  C++ anachronism...rt
// 14-oct-92 SGI does not like some functions inline...rt
// 19-oct-92 Sun C++ requires TargetTable() and ViewTable() member function
//	     declarations to be declared inline...rt
// 26-aug-93 PR2157 Display a warning if the view or target of a TaeItem could
//           not be found during construction. Also save a copy of the item
//           name rather than extracting the name from the view...krw
// 08-apr-94 IBM RS/6000 port: cast 'values' to (const TEXT **) in one of the
//         TaeItem::Update() methods...dag
//           
#ifndef I_TAEITEM
#define I_TAEITEM 0

#include <taepanel.h>
#include <slist.h>

//
// TaeItem Class definition
//
class TaeItem;

typedef  void (*IREACTFUNC)(TaeItem *, WptEvent*); // item React Func pointer

// Default react function 
extern void TaeItem_DefaultReact(TaeItem *, WptEvent*); 

class TaeItem
{
friend class TaePanel;

public:
    TaeItem ( TaePanel *panel, const TEXT *name, 
	IREACTFUNC theReactFunc = &TaeItem_DefaultReact);
    virtual ~TaeItem();

//      Methods                    		Wpt functions
//      _______                    		_____________

//
// Sensitivity methods
//
    CODE GetSensitivity (BOOL *state);		//Wpt_GetItemSensitivity

                            			//Wpt_GetConstraintSensitivity
    CODE GetSensitivity (FUNINT num,TEXT **stringvec, BOOL *state);

    CODE SetSensitivity (BOOL state);		//Wpt_SetItemSensitivity

						//Wpt_SetConstraintSensitivity
    CODE SetSensitivity (FUNINT num,const TEXT **stringvec, BOOL *state);

//
// Constraints methods
//
                            			//Wpt_GetIntgConstraints
    CODE GetConstraints (FUNINT *count,TAEINT **low,TAEINT **high);

                            			//Wpt_GetRealConstraints
    CODE GetConstraints (FUNINT *count, TAEFLOAT **low,TAEFLOAT **high);

                            			//Wpt_GetStringConstraints
    CODE GetConstraints (FUNINT *count, TEXT **vector[]);

                            			//Wpt_SetIntgConstraints
    CODE SetConstraints (FUNINT count,TAEINT low[],TAEINT high[]);

                                                //Wpt_SetRealConstraints
    CODE SetConstraints (FUNINT count, TAEFLOAT low[],TAEFLOAT high[]);

                                                //Wpt_SetStringConstraints
    CODE SetConstraints (FUNINT count, const TEXT *vector[]);

//
// hide, show, windowid, widgetid
//
    CODE Hide ();                       	//Wpt_HideItem
    CODE Show ();                        	//Wpt_ShowItem

    Window WindowId ();                  	//Wpt_ItemWindow
    Widget WidgetId();		              	//Wpt_ItemWidgetId    

//
//              UPDATE METHODS
//
// These are heavily overloaded methods. The primary purpose is to make
// it easy to do simple things, such as, to change the foreground color of
// a button TaeItem.
//
// Another objective was to help the programmer from worrying about whether
// to update the target or the view. Most of the update routines automatically
// handle the appropriate update of the target or view, as required. This
// latter objective was not totally achieved. The exceptions are the
// "UpdateTarget" methods which are used for updating the value of a qualifier
// of the target TaeVar.
//
// All the rest of the Update methods have variations on their parameters to
// differenciate them.
//
// NOTE: When specifying a qualifier in the following methods, use the standard
//       TAE naming conventions without the leading item name. For example:
//       To change the foreground color of and item, called "button1" use:
//
//              MyTaeItem->Update("fg", "green");
//
//       To change the foreground color of a menubar entry two cascades down
//       you can use: (besides using the Menu routines)
//
//              MyTaeItem->Update("cascade1.cascade2.button1.fg", "green");

//
// 			TARGET METHODS
//
    CODE Update ();	                 	//Wpt_ParmUpdate
    CODE Update ( TAEINT intg );	        //Wpt_SetIntg
    CODE Update ( TAEFLOAT real);		//Wpt_SetReal
    CODE Update ( const TEXT *string);		//Wpt_SetString

//              Multivalued Target Update
    CODE Update ( COUNT count, TAEINT *intg );
    CODE Update ( COUNT count, TAEFLOAT *real);
    CODE Update ( COUNT count, const TEXT **string);

//
// reject and setnovalue
//
    CODE Reject (TEXT *message); 		//Wpt_ParmReject
    CODE SetNoValue();				//Wpt_SetNoValue


//			TARGET Qualifier Updates

// 		update scalar TAEINT target qualifier
    CODE UpdateTarget (const TEXT *qualSpec, TAEINT intg);

// 		update scalar TAEFLOAT target qualifier
    CODE UpdateTarget (const TEXT *qualSpec, TAEFLOAT real);

// 		update scalar TEXT * target qualifier
    CODE UpdateTarget (const TEXT *qualSpec, const TEXT *string);

// 		update a multivalued TAEINT target qualifier array
    CODE UpdateTarget (const TEXT *qualSpec, COUNT count, TAEINT *intg);

// 		update a multivalued TAEFLOAT target qualifier
    CODE UpdateTarget (const TEXT *qualSpec, COUNT count, TAEFLOAT *real);

// 		update a multivalued TEXT * target qualifier
    CODE UpdateTarget (const TEXT *qualSpec, COUNT count, const TEXT **strings);


// 		VIEW METHODS
						//Wpt_ViewUpdate
    CODE Update ( TaeVarTable *newviewTable,  const TEXT *viewParm);
    CODE UpdateView ();

// 		Update a named qualifier with a new TAEINT value
    CODE Update (const TEXT *qualSpec, TAEINT value);

// 		Update a named qualifier with multiple new TAEINT values
    CODE Update (const TEXT *qualSpec, TAEINT count, TAEINT *values);

// 		Update a named qualifier with a new TAEFLOAT value
    CODE Update (const TEXT *qualSpec, TAEFLOAT value);

// 		Update a named qualifier with multiple new TAEFLOAT values
    CODE Update (const TEXT *qualSpec, TAEINT count, TAEFLOAT *values);

// 		Update a named qualifier with a new TEXT value
    CODE Update (const TEXT *qualSpec, TEXT *value);

// 		Update the title of an item
    CODE UpdateTitle (TEXT *value);

// 		Update a named qualifier with multiple new TEXT values
    CODE Update (const TEXT *qualSpec, TAEINT count, TEXT **values);

//
//   More complex (read flexible) View update methods
//

// Update a named qualifier in a different view with a new TAEINT value
    CODE Update ( TaeVarTable *newviewTable, const TEXT *viewParm, 
	          const TEXT *qualSpec, TAEINT intg ); 

// Update a named qualifier in a different view with a new TAEFLOAT value
    CODE Update ( TaeVarTable *newviewTable, const TEXT *viewParm,
       	          const TEXT *qualSpec, TAEFLOAT real);

// Update a named qualifier in a different view with a new TEXT value
    CODE Update ( TaeVarTable *newviewTable, const TEXT *viewParm,
              	  const TEXT *qualSpec, const TEXT *string);
//
// Methods to return the current value of items or attributes of items
// Note: These methods imply the caller knows the data type of the item or
//       qualifier.
// Note2: if the qualifier does not exist then:
//      0 will be returned for TAEINT
//      0.0 for TAEFLOAT
//      NULL for TEXT *
//      NULL for all arrays
//
//	Target Values
    TAEINT      Intg ();
    TAEFLOAT    Real ();
    TEXT *      String ();	 	// returns ptr, not copy of string

    TAEINT *    IntgVec ();
    TAEFLOAT *  RealVec ();
    TEXT **     StringVec ();

    COUNT       Count();    			// vector Count

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string target
// target data type
    int         Type();    			// target data type

//      For Qualifiers (in the view)

    TAEINT      Intg (const TEXT *qualSpec);
    TAEFLOAT    Real (const TEXT *qualSpec);
    TEXT *      String (const TEXT *qualSpec);

    TEXT *	Title ();			// return item's title

    TAEINT *    IntgVec (const TEXT *qualSpec);
    TAEFLOAT *  RealVec (const TEXT *qualSpec);
    TEXT **     StringVec (const TEXT *qualSpec);

    COUNT       Count(const TEXT *qualSpec);

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string target
// view qualifier data type
    int         Type(const TEXT *qualSpec);	// qualifier data type

//      For Qualifiers (in the target)

    TAEINT      IntgTarget (const TEXT *qualSpec);
    TAEFLOAT    RealTarget (const TEXT *qualSpec);
    TEXT *      StringTarget (const TEXT *qualSpec);

    TAEINT *    IntgVecTarget (const TEXT *qualSpec);
    TAEFLOAT *  RealVecTarget (const TEXT *qualSpec);
    TEXT **     StringVecTarget (const TEXT *qualSpec);

    COUNT       CountTarget(const TEXT *qualSpec);

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string target
// target qualifier data type
    int         TypeTarget(const TEXT *qualSpec);// target qualifier data type

//
// Menu Methods
//
                                                //Wpt_GetMenu
    CODE GetMenu (TEXT *entryName, FUNINT *count, WPT_MENUENTRY **entries);

                                                //Wpt_SetMenu
    CODE SetMenu (TEXT *entryName, FUNINT count, WPT_MENUENTRY *entries);

    CODE MenuUpdate (); 			//Wpt_MenuUpdate

                                                //Wpt_InsertMenuEntries
    CODE InsertMenuEntries ( TEXT *entryName, FUNINT count, 
			     WPT_MENUENTRY *entries);

                                                //Wpt_RemoveMenuEntries
    CODE RemoveMenuEntries ( FUNINT count, TEXT **entryNames);

                                                //Wpt_SetMenuSensitivity
    CODE SetMenuSensitivity ( FUNINT count, TEXT **entryNames, BOOL *state);

                                                //Wpt_GetMenuEntryPath
    CODE GetMenuEntryPath ( TEXT *entryName, TEXT *entryPath);

//
// React Functions
//	(note: these can not be defined as post-inline functions
//	 the compiler (g++) gets very confused)
//
    IREACTFUNC ReactCB()	 		// get current React callback
    	{ return reactFunc; }

    IREACTFUNC ReactCB(IREACTFUNC newFunct)	// sets a new react function
    	{
    	IREACTFUNC rtn = reactFunc;
    	reactFunc = newFunct; 
    	return rtn;				// return the previous function
    	}


//Others:
    virtual void PrintValue(COUNT n = 1); 	// print first "n" values

    const TEXT *Name();

    TaePanel *Parent();

    inline TaeVarTable * TargetTable();
    inline TaeVarTable * ViewTable();

    TaeVar * GetTargetVar ();

    TaeVar * GetTargetVar ( WptEvent *event );

    TaeVar * GetViewVar ( );

    TaeVar * GetViewVar ( const TEXT *vqualname );

    TaeVar & operator[] ( const TEXT *vqualname );

	// React must be public because TaePanel_DefaultReact, a non-class
	// function, must be able to call it
    virtual void React (WptEvent *wptEvent);

	// PROTECTED methods and data
protected:
	// PRIVATE methods and data
private:
    TEXT 	*itemName;			// item's name
    TaePanel 	*parentPanel;          		// parent TaePanel object
    TaeVar	*targetVar;			// item's target variable
    TaeVar	*viewVar;			// item's view variable
    IREACTFUNC	reactFunc;	// Pointer to this instance's react function

// The following methods prevent symantically incorrect duplication of 
// a TaeItem. 
    TaeItem (const TaeItem& copy);
    TaeItem& operator=(const TaeItem& copy);

};		// end of class TaeItem


//
// ************* INLINE Functions
//  

//
// create an instance of a TaeItem and register with the parentPanel.
// Constructor:
//
inline 
TaeItem::TaeItem ( TaePanel *panel, const TEXT *name , IREACTFUNC ReactFunct)
    {
    parentPanel = panel;
    panel->RegisterItem(this);
    targetVar = &(*TargetTable())[name]; 
    viewVar = &(*ViewTable())[name]; 
    itemName = s_save(name);
    if (!viewVar || !targetVar) 
	printf("TAE Warning: Item (%s) could not be found in panel (%s)\n",
	       itemName, panel->Name());
    reactFunc = ReactFunct;
    }

//
//  Destructor
//
inline
TaeItem::~TaeItem()
    {
    parentPanel->DeregisterItem(this);
    if (itemName) s_free(itemName);
    }


//      Methods                    		Wpt functions
//      _______                    		_____________
// Sensitivity methods
//
inline 
CODE TaeItem::GetSensitivity (BOOL *state) 	//Wpt_GetItemSensitivity
    { return parentPanel->GetSensitivity ( itemName, state );}

inline 						//Wpt_GetConstraintSensitivity
CODE TaeItem::GetSensitivity (FUNINT num,TEXT **stringvec, BOOL *state)
    { 
    return parentPanel->GetSensitivity(itemName, num,  stringvec, state);
    }

inline 
CODE TaeItem::SetSensitivity (BOOL state)	//Wpt_SetItemSensitivity
    { 
    return parentPanel->SetSensitivity ( itemName, state);
    }

inline 						//Wpt_SetConstraintSensitivity
CODE TaeItem::SetSensitivity (FUNINT num,const TEXT **stringvec,
                            BOOL *state)
    { 
    return parentPanel->SetSensitivity (itemName, num, stringvec, state);
    }

//
// Constraints methods
//

inline 						//Wpt_GetIntgConstraints
CODE TaeItem::GetConstraints (FUNINT *count,TAEINT **low,TAEINT **high)
    { 
    return parentPanel->GetConstraints ( itemName, count, low, high); 
    }

inline 						//Wpt_GetRealConstraints
CODE TaeItem::GetConstraints (FUNINT *count, TAEFLOAT **low, TAEFLOAT **high)
    { 
    return parentPanel->GetConstraints ( itemName, count, low, high); 
    }

inline 						//Wpt_GetStringConstraints
CODE TaeItem::GetConstraints (FUNINT *count, TEXT **vector[])
    { 
    return parentPanel->GetConstraints ( itemName, count, vector); 
    }

inline 						//Wpt_SetIntgConstraints
CODE TaeItem::SetConstraints (FUNINT count,TAEINT low[],TAEINT high[])
    { 
    return parentPanel->SetConstraints ( itemName, count,  low, high); 
    }

inline 						//Wpt_SetRealConstraints
CODE TaeItem::SetConstraints (FUNINT count, TAEFLOAT low[], TAEFLOAT high[])
    { 
    return parentPanel->SetConstraints (itemName, count, low, high );
    }

inline 						//Wpt_SetStringConstraints
CODE TaeItem::SetConstraints (FUNINT count, const TEXT *vector[])
    { 
    return parentPanel->SetConstraints (itemName, count, vector ); 
    }

//
// hide, show, windowid, widgetid
//
inline 
CODE TaeItem::Hide ()                   	//Wpt_HideItem
    { 
    return parentPanel->Hide ( itemName ); 
    }

inline 
CODE TaeItem::Show ()                   	//Wpt_ShowItem
    { 
    return parentPanel->Show(itemName); 
    }

inline 
Window TaeItem::WindowId ()             	//Wpt_ItemWindow
    { 
    return parentPanel->WindowId (itemName); 
    }

inline 
Widget TaeItem::WidgetId()              	//Wpt_ItemWidgetId    
    { 
    return parentPanel->WidgetId ( itemName ); 
    }


//
// Update methods
//
	// TARGET oriented Updates
inline 
CODE TaeItem::Update ()                 	//Wpt_ParmUpdate
    { 
    return parentPanel->Update(itemName); 
    }


inline 
CODE TaeItem::Update ( TAEINT intg )		//Wpt_SetIntg
    { 
    return parentPanel->Update ( itemName, intg ); 
    }

inline 
CODE TaeItem::Update ( TAEFLOAT real)		//Wpt_SetReal
    { 
    return parentPanel->Update ( itemName, real); 
    }

inline 
CODE TaeItem::Update ( const TEXT *string)	//Wpt_SetString
    { 
    return parentPanel->Update ( itemName, string); 
    }

//              Multivalued Target Update
inline
CODE TaeItem::Update ( COUNT count, TAEINT *intg )
    { 
    return parentPanel->Update ( itemName, count, intg); 
    }

inline
CODE TaeItem::Update ( COUNT count, TAEFLOAT *real)
    { 
    return parentPanel->Update ( itemName, count, real); 
    }

inline
CODE TaeItem::Update ( COUNT count, const TEXT **string)
    { 
    return parentPanel->Update ( itemName, count, string); 
    }

	// Update a target Qualifier
inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, TAEINT intg)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, intg);
    }

inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, TAEFLOAT real)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, real);
    }

inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, const TEXT *string)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, string);
    }

inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, COUNT count, TAEINT *intg)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, count, intg);
    }

inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, COUNT count, TAEFLOAT *real)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, count, real);
    }

inline
CODE TaeItem::UpdateTarget (const TEXT *qualSpec, COUNT count, 
			    const TEXT **strings)
    {
    return parentPanel->UpdateTarget (itemName, qualSpec, count, strings);
    }


	// VIEW oriented Updates
inline 						//Wpt_ViewUpdate
CODE TaeItem::Update ( TaeVarTable *newviewTable,  
    	               const TEXT *viewParm) 
    { 
    return parentPanel->Update (itemName, newviewTable, viewParm); 
    }

inline
CODE TaeItem::UpdateView () 			//Wpt_ViewUpdate
    { 
    return parentPanel->UpdateView (itemName); 
    }


// Update a named qualifier in a different view with a new TAEINT value
inline 
CODE TaeItem::Update ( TaeVarTable *newviewTable, const TEXT *viewParm,
              	       const TEXT *qualSpec, TAEINT intg ) 
    { 
    return parentPanel->Update ( itemName, newviewTable, viewParm,
                       		 qualSpec, intg); 
    }

// Update a named qualifier in a different view with a new TAEFLOAT value
inline 
CODE TaeItem::Update ( TaeVarTable *newviewTable, const TEXT *viewParm,
              	       const TEXT *qualSpec, TAEFLOAT real) 
    { 
    return parentPanel->Update ( itemName, newviewTable, viewParm,
                                 qualSpec, real );
    }

// Update a named qualifier in a different view with a new TEXT value
inline 
CODE TaeItem::Update ( TaeVarTable *newviewTable, const TEXT *viewParm,
              	       const TEXT *qualSpec, const TEXT *string) 
    { 
    return parentPanel->Update ( itemName, newviewTable, viewParm,
                                 qualSpec, string);
    }

// Update a named qualifier with a new TAEINT value
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TAEINT value)
    {
    return parentPanel->Update (itemName, qualSpec, value); 
    }

// Update a named qualifier with multiple new TAEINT values
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TAEINT count, TAEINT *values)
    {
    return parentPanel->Update (itemName, qualSpec, count, values); 
    }

// Update a named qualifier with a new TAEFLOAT value
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TAEFLOAT value)
    {
    return parentPanel->Update (itemName, qualSpec, value); 
    }

// Update a named qualifier with multiple new TAEFLOAT values
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TAEINT count, TAEFLOAT *values)
    {
    return parentPanel->Update (itemName, qualSpec, count, values); 
    }

// Update a named qualifier with a new TEXT value
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TEXT *value)
    {
    return parentPanel->Update (itemName, qualSpec, value); 
    }

// Update the item's title
inline
CODE TaeItem::UpdateTitle (TEXT *newTitle)
    {
    return parentPanel->Update (itemName, "", newTitle);
    }

// Update a named qualifier with multiple new TEXT values
inline 
CODE TaeItem::Update (const TEXT *qualSpec, TAEINT count, TEXT **values)
    {
    return parentPanel->Update (itemName, qualSpec, count, (const TEXT**)values); 
    }


//
// reject and setnovalue
//
inline 
CODE TaeItem::Reject (TEXT *message) 		//Wpt_ParmReject
    { 
    return parentPanel->Reject ( itemName, message ); 
    }

inline 
CODE TaeItem::SetNoValue()			//Wpt_SetNoValue
    { 
    return parentPanel->SetNoValue(itemName) ; 
    }

//
// Value Methods
//
#define GET_ITEM_VALUE(TYPE, METHOD) \
inline \
TYPE  TaeItem::METHOD () \
    { \
    return parentPanel->METHOD(itemName); \
    } 

GET_ITEM_VALUE(TAEINT, Intg)
GET_ITEM_VALUE(TAEFLOAT, Real)
GET_ITEM_VALUE(TEXT*, String)

GET_ITEM_VALUE(TAEINT*, IntgVec)
GET_ITEM_VALUE(TAEFLOAT*, RealVec)
GET_ITEM_VALUE(TEXT**, StringVec)

GET_ITEM_VALUE(COUNT, Count)
GET_ITEM_VALUE(int, Type)
#undef GET_ITEM_VALUE

//      For Qualifiers (in the view)
#define GET_ITEM_QUAL_VALUE(TYPE, METHOD) \
inline \
TYPE  TaeItem::METHOD (const TEXT *qualSpec) \
    { \
    return parentPanel->METHOD(itemName, qualSpec); \
    } 

GET_ITEM_QUAL_VALUE(TAEINT, Intg)
GET_ITEM_QUAL_VALUE(TAEFLOAT, Real)
GET_ITEM_QUAL_VALUE(TEXT*, String)

inline 						// return item's title
TEXT *TaeItem::Title () 
    { 
    return TaeItem::String("");
    }

GET_ITEM_QUAL_VALUE(TAEINT*, IntgVec)
GET_ITEM_QUAL_VALUE(TAEFLOAT*, RealVec)
GET_ITEM_QUAL_VALUE(TEXT**, StringVec)

GET_ITEM_QUAL_VALUE(COUNT, Count)
GET_ITEM_QUAL_VALUE(int, Type)

//      For Qualifiers (in the target)

GET_ITEM_QUAL_VALUE(TAEINT, IntgTarget)
GET_ITEM_QUAL_VALUE(TAEFLOAT, RealTarget)
GET_ITEM_QUAL_VALUE(TEXT*, StringTarget)

GET_ITEM_QUAL_VALUE(TAEINT*, IntgVecTarget)
GET_ITEM_QUAL_VALUE(TAEFLOAT*, RealVecTarget)
GET_ITEM_QUAL_VALUE(TEXT**, StringVecTarget)

GET_ITEM_QUAL_VALUE(COUNT, CountTarget)
GET_ITEM_QUAL_VALUE(int, TypeTarget)
#undef GET_ITEM_QUAL_VALUE

//
// Menu Methods
//
                                                //Wpt_GetMenu
inline 
CODE TaeItem::GetMenu (TEXT *entryName, COUNT *count,
                     WPT_MENUENTRY **entries)
    {
    return parentPanel->GetMenu (itemName, entryName, count, entries);
    }
                                                //Wpt_SetMenu
inline 
CODE TaeItem::SetMenu (TEXT *entryName, FUNINT count,
                     WPT_MENUENTRY *entries)
    {
    return parentPanel->SetMenu (itemName, entryName, count, entries);
    }
inline 
CODE TaeItem::MenuUpdate () 			//Wpt_MenuUpdate
    {
    return parentPanel->MenuUpdate (itemName);
    }

inline 						//Wpt_InsertMenuEntries
CODE TaeItem::InsertMenuEntries ( TEXT *entryName, FUNINT count,
                                WPT_MENUENTRY *entries)
    {
    return parentPanel->InsertMenuEntries ( itemName, entryName, count,
					    entries);
    }

inline 						//Wpt_RemoveMenuEntries
CODE TaeItem::RemoveMenuEntries ( FUNINT count, TEXT **entryNames)
    {
    return parentPanel->RemoveMenuEntries ( itemName, count, entryNames);
    }

inline 						//Wpt_SetMenuSensitivity
CODE TaeItem::SetMenuSensitivity ( FUNINT count, TEXT **entryNames,
                                   BOOL *state)
    {
    return parentPanel->SetMenuSensitivity ( itemName, count, entryNames,
					     state);
    }

inline 						//Wpt_GetMenuEntryPath
CODE TaeItem::GetMenuEntryPath ( TEXT *entryName, TEXT *entryPath)
    {
    return parentPanel->GetMenuEntryPath ( itemName, entryName, entryPath);
    }

//
// Other methods
//

inline
const TEXT *TaeItem::Name() 
    { return itemName; }

inline
TaePanel *TaeItem::Parent()
    {return parentPanel;}

#ifdef sgi
TaeVarTable * TaeItem::TargetTable()
#else
inline
TaeVarTable * TaeItem::TargetTable()
#endif
    { return parentPanel->TargetTable(); }

#ifdef sgi
TaeVarTable * TaeItem::ViewTable()
#else
inline
TaeVarTable * TaeItem::ViewTable()
#endif
    { return parentPanel->ViewTable(); }

inline
TaeVar * TaeItem::GetTargetVar () 
    { return targetVar; }

inline
TaeVar * TaeItem::GetTargetVar ( WptEvent *event )
    { return &(*TargetTable())[event->parmName]; } 

inline
TaeVar * TaeItem::GetViewVar ( ) 
    { return viewVar; }

inline
TaeVar * TaeItem::GetViewVar ( const TEXT *vqualname ) 
    { return (viewVar) ?  &(*viewVar)[vqualname] : (TaeVar *)NULL; }


inline
TaeVar & TaeItem::operator[] ( const TEXT *vqualname )
    { if (!viewVar)
	  printf ("TAE Warning: the viewVar for item (%s) does not exist and\n\
you are about to ask for Qualifier (%s). Expect a core dump.\n", 
		  itemName, vqualname);
      return (*viewVar)[vqualname]; }

inline
void TaeItem::React (WptEvent *wptEvent) 
    {
    if (reactFunc) 
	(*reactFunc)(this, wptEvent);
    }

#endif

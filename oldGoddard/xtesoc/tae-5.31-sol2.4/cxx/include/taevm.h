/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



//
//
// This include file contains class definition for the following classes
//
//      TaeVar
//    TaeVarTable
//    TaeCollection
//
// 17-mar-92 Created...tpl
// 15-jul-92 PR1471: TaeVar::ForEach was not returning a CODE...krw
// 28-aug-92 Possible memory leak in the TaeCollection dtor. Need to free
//	     anything read in with Co_ReadFile...krw
//	     PR1512: some classes need virtual dtors...krw
// 14-oct-92 SGI does not like some functions inline and does not like
//           function definitions and declarations having default parameters
//           simultaneously...rt
// 16-oct-92 PR1677: Call to Co_ReadFile should pass P_ABORT so the appl.
//	     aborts if the collection file can not be found. This is consistent
//	     with our generated C code...krw
//	     PR1694: DupFunction had an unused parameter and was not returning
//	     a value...krw
//	     PR1695:Also defined the inline functions better. Some compilers 
//	     appear to be one pass, so they need functions fully defined 
//	     early...krw
// 16-dec-92 PR1773: Removed declaration of virtual dtors for TaeVar and 
//	     TaeVarTable. These classes must NEVER have any virtual methods or 
//	     any class specific instance data. The same for any subclasses 
//	     there from. See the PR writeup for the reason...krw
// 20-sep-93 PR2320 Deleting a TaeCollection could cause a core dump...krw
//

#ifndef I_TAEVM
#define I_TAEVM 0

#include    <taeconf.inp>
#include    <symtab.inc>
#include    <parblk.inc>
#include    <vminc.inc>
#include    <wptinc.inp>
#include    <symbol.h>
#include    <slist.h>

#ifndef nil
#define nil     NULL
#endif

class TaeVar;                // defined below
class TaeVarTable;            // defined below
class TaeCollection;            // defined below

typedef CODE (*TaeVarCallback) (TaeVar *s, void *context);
typedef CODE (*TaeVarTableCallback)
             (TaeVarTable  *st,  void *context,  const TEXT *name,  int type);

//
// declare Vm function prototypes
//



/*********************************

User Guide:

A TaeVar is a TAE variable.  A TaeVarTable
is a Vm object, i.e., a collection of 
TaeVars.  A TaeVarTable on disk is a TAE
standard PAR file as written by the TCL
SAVE command or the TUTOR SAVE command.
A TaeCollection is a collection of TaeVar
Tables.  A TaeCollection on disk is
(a) one file, the concatenation of several
PAR files, or (b) a UNIX archive of PAR files.

To access a TaeVar in a TaeVarTable,
do something like:
    
    printf ("value of x is %d\n", (*st)["x"].Intg());

but beware that the above crashes if "x" 
does not exist in st.   To check existence, do:

    TaeVar *s = (*st).GetTaeVar("x");
    if (s)
        printf ("value of x is %d\n", (*s).Intg());
    
Sometimes references to TaeVars make for better
reading:

    TaeVar &s = (*st)["x"];
    if (&s)
    printf ("value of x is %d\n", s.Intg());

You may reference a numeric value as either Real
or Intg and the functions do a conversion for you.
To Set the value of something, your type must be
correct, however.

The Set functions for TaeVarTable will create the
variable for you if it doesn't exist.  Likewise,
the Set functions for qualifiers will create the
variable for you.  The Set functions for TaeVars,
of course, do not create for you (because an
object must exist if you want to operate on it).


*****************************/


//    A TaeVar is an agent for the traditional TAE VARIABLE
//    structure.  

class TaeVar : public Symbol {

public:
    TaeVar (){}

    ~TaeVar (){}

    inline TaeVar   *QualVar (const TEXT *name);

    inline TaeVar   & operator[] (const TEXT *name);

    inline CODE ForEach (TaeVarCallback f, void *context);

    inline TaeVar *Clone(const TEXT *newName = nil);

private:
// The following methods prevent symantically incorrect duplication of
// a TaeVar. Use Clone instead.
    TaeVar (const TaeVar& copy);
    TaeVar& operator=(const TaeVar& copy);

};

inline
TaeVar *TaeVar::QualVar (const TEXT *name)
    { 
    return (TaeVar *) Qualifier( name ); 
    }

inline
TaeVar & TaeVar::operator[] (const TEXT *name)
    {
    return *QualVar (name);
    }

inline
CODE TaeVar::ForEach (TaeVarCallback f, void *context)
    { 
    SymbolCallback sf = (SymbolCallback) f;
    Symbol * sym = (Symbol *) this;
    return sym->ForEach ( sf, context );
    }

inline
TaeVar *TaeVar::Clone(const TEXT *newName) 
    {
    return (TaeVar *) Symbol::Clone(newName);
    }



class TaeVarTable : public SymbolTable {

friend TaeVar;

public:

    TaeVarTable (){}

    ~TaeVarTable (){}

    inline TaeVar * GetTaeVar (const TEXT *name);

    inline TaeVar & operator[] (const TEXT *name);

    inline TEXT * String (const TEXT *name);

    inline TAEFLOAT Real (const TEXT *name);

    inline TAEINT Intg (const TEXT *name);

    inline TaeVarTable *Clone();
};

inline
TaeVar * TaeVarTable::GetTaeVar (const TEXT *name)
    { 
    return ( TaeVar *)Variable(name);
    }

inline
TaeVar & TaeVarTable::operator[] (const TEXT *name)
    { 
    return *GetTaeVar(name);
    }


inline
TEXT * TaeVarTable::String (const TEXT *name)
    { 
    return GetTaeVar(name)->String();
    }


inline
TAEFLOAT TaeVarTable::Real (const TEXT *name)
    { 
    return GetTaeVar(name)->Real();
    }


inline
TAEINT TaeVarTable::Intg (const TEXT *name)
    { 
    return GetTaeVar(name)->Intg();
    }


inline
TaeVarTable *TaeVarTable::Clone() 
    { 
    return (TaeVarTable *) SymbolTable::Clone();
    }   


//    A TaeCollection is a collection of TaeVarTables.
//    This is the C++ form of the traditional Co_ package.

class TaeCollection {
    Id    collection;
    BOOL  collectionRead;		// TRUE if Co_ReadFile called
    TEXT  filename[STRINGSIZ+1];
    CODE  code;
    int   count;
public:
    inline TaeCollection ();

    inline TaeCollection (const TEXT *fileSpec);

    inline ~TaeCollection ();		// if superstructure still around:

    inline CODE WriteFile (const TEXT *fileSpec);

    inline TaeVarTable *Find (const TEXT *memberName);  // nil if not present

    inline int Count ();

    inline void Remove (const TEXT *memberName);

    inline const TEXT * ResFileName();

    inline CODE status();

    inline CODE ForEach (TaeVarTableCallback f, void *context);

    inline TaeCollection *Clone ();

// The following methods are fully defined here because other inline methods
// need to call them. Many compilers must have them fully defined before they
// are used in order to make them inline.

    inline void Add (TaeVarTable * vt, const TEXT *memberName)
    	{ 
    	Co_Add (collection, (SymbolTable *)vt, memberName, 0); 
    	}

    inline void Free()
    	{
    	Co_Free (collection, Vm_Free);
    	collection = 0;
    	collectionRead = FALSE;
    	}

private:
static CODE DupFunction (TaeVarTable *st, void *context,
	const TEXT *name, int type)
    {
    type = type;			// avoid compiler warnings
    TaeCollection *newCollection = (TaeCollection *)context;
    TaeVarTable *newTable = st->Clone();
    if (!newTable) 
	return (CODE) !NULL;		// non-zero means stop looping
    else
	{
        newCollection->Add(newTable, name);
    	return (CODE) NULL;		// success
	}
    }

// The following methods prevent symantically incorrect duplication of
// a TaeCollection. Use Clone instead.
    TaeCollection (const TaeCollection& copy);
    TaeCollection& operator=(const TaeCollection& copy);
};

		// INLINE methods
//
//  constructor
//
inline
TaeCollection::TaeCollection ()
    { 
    collection = Co_New(0);
    collectionRead = FALSE;
    code = SUCCESS;
    }

inline
TaeCollection::TaeCollection (const TEXT *fileSpec)
    {
    collection = Co_New(0);
    code = Co_ReadFile (collection, fileSpec, P_ABORT);
    if (code == SUCCESS) collectionRead = TRUE;
    s_copy (fileSpec, filename);
    }

//
//  destructor
//
inline
TaeCollection::~TaeCollection ()	// if superstructure still around:
    { if (collection) 
        {
        if (collectionRead)
	    Free();
        else
	    Co_Free (collection, nil);
        }
    }

inline 
CODE TaeCollection::WriteFile (const TEXT *fileSpec)
    {
    code = Co_WriteFile (collection, fileSpec);
    return code;
    }

					// nil if not present
inline 
TaeVarTable *TaeCollection::Find (const TEXT *memberName)  
    { 
    return ((TaeVarTable *)Co_Find (collection, memberName)); 
    }

inline
int TaeCollection::Count () 
    { 
    return count; 
    }


inline 
void TaeCollection::Remove (const TEXT *memberName)
    { 
    Co_Remove ( collection, memberName ); 
    }

inline
const TEXT * TaeCollection::ResFileName()
    { 
    return (const TEXT *)filename; 
    }

inline
CODE TaeCollection::status() 
    { 
    return code; 
    }

inline 
CODE TaeCollection::ForEach (TaeVarTableCallback f, void *context)
    { 
    StCallback sf = (StCallback) f;
    SymbolTableList *sym = (SymbolTableList *) this;
    return sym->ForEach ( sf, context ); 
    }
	
inline
TaeCollection *TaeCollection::Clone () 
    { 
    TaeCollection *newCollection = new TaeCollection;	
    ForEach ((TaeVarTableCallback)&TaeCollection::DupFunction, 
		(void *)newCollection);
    return newCollection;
    }

#endif

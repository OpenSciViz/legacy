/****************************************************************************
 *      Copyright (c) 1993, 1994
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/


/*
 *      This file contains class definitions of 
 *        TaeResource,
 *        TaePanel, 
 *        TaeEventHandler, 
 *        TaePanelWindow, 
 *        TaePanelFile, 
 *        TaePanelTimeout,
 *	  TaePanelTimer
 */
//
// 28-jan-92 Created...tpl
// 06-apr-92 Added TaeResource class...tpl
// 21-apr-92 Streamlined Display and removed connect...tpl
// 29-apr-92 PR1345 Changed Display method to Show...tpl
// 11-may-92  Added menubar methods...tpl
// 15-jun-92 PR1464 React method moved to protected...tpl
// 09-sep-92 PR1513:Numerous changes based on user comments...krw
//	     PR1512: some classes need virtual dtors...krw
//	     PR1500: the private virtual functions need to be protected...krw
// 01-oct-92 PR1115: Add TaePanelTimer class;
// 12-oct-92 Class name may not be left out unless there is exactly one
//           immediate base class.  C++ anachronism...rt
// 16-oct-92 PR1695: Centerline C++ complained about TaePanelTimeout not
//	     having a constructor. Also TaePanel::Initialize was not 
//	     returning a value...krw
// 10-nov-92 PR1745: Removed TaeEventHandler(displayId) ctor, EventDisplay and
//	     TaePanel::AddItem methods. They all were useless...krw
// 13-aug-93 PR2176 MissingVal must not have a const parameter...krw
// 14-oct-93 PR2353: Moved TaeResource::~TaeResource to taepanel.cc...rt
//
#ifndef I_TAEPANEL
#define I_TAEPANEL

#include <wptinc.inp>
#include <taevm.h>
#include <slist.h>

class TaeEventHandler;
class TaePanelWindow;
class TaePanelFile;
class TaePanelTimeout;
class TaePanelTimer;
class TaeItem;

//
// ****************** TaeResource **************************
//
class TaeResource
{
public:
    TaeResource (const TEXT *resfileName );

    virtual ~TaeResource ();

    virtual void Initialize_All_Panels () {};
    virtual void Create_Initial_Panels ( const TaeEventHandler& ) {};
  
    TaeCollection *Collection();

private:
    TaeCollection *theCollection;

// The following methods prevent symantically incorrect duplication of
// a TaeResource.
    TaeResource (const TaeResource& copy);
    TaeResource& operator=(const TaeResource& copy);
};

inline
TaeResource::TaeResource (const TEXT *resfileName )
    {
    theCollection = new TaeCollection ( resfileName );
    }

inline
TaeCollection *TaeResource::Collection() 
    {
    return theCollection;
    }

//
// ****************** TaePanel **************************
//
// TaePanel class encapsulates TAE panels and WPT functions
//
class TaePanel;

typedef  void (*PREACTFUNC)(TaePanel *, WptEvent*); // Panel React Func pointer

class TaePanel
{
    friend class TaeItem;
    friend class TaeEventHandler;
public:

//  Constructor:

    	//read the resfile and get the view and target from the resfile
    TaePanel ( const TEXT * panelname, const TEXT * resfilename );

    	//create a TaePanel instance using view and target.
	// Set alloced to TRUE if target and view are newly allocated
	// If alloced is TRUE, the target and view TaeVarTables will be
	// deleted via the TaePanel dtor.
    TaePanel ( const TEXT * panelname, TaeVarTable *target, 
	       TaeVarTable *view, BOOL alloced = FALSE);

    	//get panel view and target from the collection
    TaePanel ( const TEXT * panelname, TaeCollection *vmCollection );

    	//read the res file with the same name
    TaePanel ( const TEXT *panelname );

	// create new TaePanel from this one
    virtual TaePanel *Clone(const TEXT *uniqueName);

//  Destructor

    virtual ~TaePanel();               		//destructor will erase panel
                         			//if panel has not been erased

//        Methods                		Wpt functions
//        _______                		_____________

    CODE BeginWait();            		//Wpt_BeginWait
    CODE CloseItems();            		//Wpt_CloseItems
    CODE EndWait();                		//Wpt_EndWait
                        			//Wpt_GetItemSensitivity
    CODE GetSensitivity (const TEXT *itemName, BOOL *state);

                            			//Wpt_GetConstraintSensitivity
    CODE GetSensitivity (const TEXT *itemName,FUNINT num,
                         TEXT **stringvec, BOOL *state);

                        			//Wpt_SetItemSensitivity
    CODE SetSensitivity (const TEXT *itemName, BOOL state);

                            			//Wpt_SetConstraintSensitivity
    CODE SetSensitivity (const TEXT *itemName,FUNINT num,
                         const TEXT **stringvec, BOOL *state);

                            			//Wpt_GetIntgConstraints
    CODE GetConstraints (const TEXT *itemName,FUNINT *count,
                 	 TAEINT **low,TAEINT **high);

                            			//Wpt_GetRealConstraints
    CODE GetConstraints (const TEXT *itemName, FUNINT *count,
                         TAEFLOAT **low,TAEFLOAT **high);

                            			//Wpt_GetStringConstraints
    CODE GetConstraints (const TEXT *itemName, FUNINT *count,
                         TEXT **vector[]);

                            			//Wpt_SetIntgConstraints
    CODE SetConstraints (const TEXT *itemName, FUNINT count,
                         TAEINT low[],TAEINT high[]);

                                                //Wpt_SetRealConstraints
    CODE SetConstraints (const TEXT *itemName, FUNINT count,
                        TAEFLOAT low[],TAEFLOAT high[]);


                                                //Wpt_SetStringConstraints
    CODE SetConstraints (const TEXT *itemName, FUNINT count,
                        const TEXT *vector[]);


    CODE GetState (TAEINT *state);		//Wpt_GetPanelState

    CODE SetState (TAEINT state);		//Wpt_SetPanelState

    CODE Hide ();				//Wpt_HidePanel
    CODE Hide (const TEXT *itemName);		//Wpt_HideItem

//  			Show:
// calls Wpt_NewPanel to create and display the panel (or Wpt_SetPanelState
//      to display the panel if the panel has already been created).
// if no display is specified default display is used
// if no flag is specified WPT_PREFERRED is used as flag.
// if no window (relativeWindow) is specified NULL (Root Window) is used.

    CODE Show(const TaeEventHandler& e, const CODE flags=WPT_PREFERRED,
		 Window w=NULL);

						// Show on another X display
    CODE Show(Display *display, const TaeEventHandler& e,
                 const CODE flags=WPT_PREFERRED, Window w=NULL);


    CODE Show (); 				//Wpt_ShowPanel
    CODE Show (const TEXT *itemName); 		//Wpt_ShowItem

    CODE Erase();              			//Wpt_PanelErase

//
// Miscellaneous methods
//
    Window WindowId(const TEXT *itemName);      //Wpt_ItemWindow
    Window WindowId ();                  	//Wpt_PanelWindow

    Widget WidgetId();            		//Wpt_PanelWidgetId
    Widget WidgetId(const TEXT *itemName); //widgetId of item, no wpt equivalent

    BOOL MissingVal ( TEXT *message );	        //Wpt_MissingVal
    CODE Message ( const TEXT *message );	//Wpt_PanelMessage
    CODE Reset();				//Wpt_PanelReset
    Window TopWindowId ();    			//Wpt_PanelTopWindow 

    CODE Reject (const TEXT *itemName,TEXT *message); //Wpt_ParmReject

//
// 		UPDATE METHODS
//
// These are heavily overloaded methods. The primary purpose is to make
// it easy to do simple things, such as, to change the foreground color of
// a button on the TaePanel. 
//
// Another objective was to help the programmer from worrying about whether
// to update the target or the view. Most of the update routines automatically 
// handle the appropriate update of the target or view, as required. This
// latter objective was not totally achieved. The exceptions are the 
// "UpdateTarget" methods which are used for updating the value of a qualifier 
// of the target TaeVar. 
//
// All the rest of the Update methods have variations on their parameters to
// differenciate them.
//
// NOTE: When specifying a qualifier in the following methods, use the standard
//       TAE naming conventions without the leading item name. For example:
//	 To change the foreground color of and item, called "button1" use:
//
//		MyTaePanel->Update("button1", "fg", "green");
//
//	 To change the foreground color of a menubar entry two cascades down
//	 you can use: (besides using the Menu routines)
//
//		MyTaePanel->Update("menubar1", "cascade1.cascade2.button1.fg",
//			           "green"); 

//
//		Target Methods
//
    CODE Update ( const TEXT *itemName);		//Wpt_ParmUpdate
    CODE SetNoValue(const TEXT *itemName); 		//Wpt_SetNoValue

    CODE Update ( const TEXT *itemName, TAEINT intg );		//Wpt_SetIntg
    CODE Update ( const TEXT *itemName, TAEFLOAT real);		//Wpt_SetReal
    CODE Update ( const TEXT *itemName, const TEXT *string);	//Wpt_SetString

//		Multivalued Target Update
    CODE Update ( const TEXT *itemName, COUNT count, TAEINT *intg );
    CODE Update ( const TEXT *itemName, COUNT count, TAEFLOAT *real);
    CODE Update ( const TEXT *itemName, COUNT count, const TEXT **string);

// 		Target Qualifier Update

//		update scalar TAEINT target qualifier
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, TAEINT intg);

// 		update scalar TAEFLOAT target qualifier
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, 
		       TAEFLOAT real);

// 		update scalar TEXT * target qualifier
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, 
		       const TEXT *string);

// 		update a multivalued TAEINT target qualifier array
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, 
		       int count, TAEINT *intg);

// 		update a multivalued TAEFLOAT target qualifier
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, 
		       int count, TAEFLOAT *real);

// 		update a multivalued TEXT * target qualifier
    CODE UpdateTarget (const TEXT *itemName, const TEXT *qualSpec, 
		       int count, const TEXT **strings);

//
//		View Methods
//
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable, 
		  const TEXT *viewParm);		//Wpt_ViewUpdate

    CODE UpdateView (const TEXT *itemName);		// alternate ViewUpdate
//
//		View Qualifier Update
//
// 		update scalar TAEINT view qualifier
    CODE Update (const TEXT *itemName, const TEXT *qualSpec, TAEINT intg);

// 		update scalar TAEFLOAT view qualifier
    CODE Update (const TEXT *itemName, const TEXT *qualSpec, TAEFLOAT real);

// 		update scalar TEXT * view qualifier
//  Note: if the qualSpec is NULL or "", you will be updating the item's title

    CODE Update (const TEXT *itemName, const TEXT *qualSpec, 
		 const TEXT *string);

//		update an item's title
    CODE UpdateTitle (const TEXT *itemName, const TEXT *string);

// 		update a multivalued TAEINT view qualifier
    CODE Update (const TEXT *itemName, const TEXT *qualSpec, 
		 int count, TAEINT *intg);

// 		update a multivalued TAEFLOAT view qualifier
    CODE Update (const TEXT *itemName, const TEXT *qualSpec, 
		 int count, TAEFLOAT *real);

// 		update a multivalued TEXT * view qualifier
    CODE Update (const TEXT *itemName, const TEXT *qualSpec, 
		 int count, const TEXT **strings);

//
//   More complex (read flexible) View update methods
//
// 		update a scaler TAEINT qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec, TAEINT intg );

// 		update a scaler TAEFLOAT qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec, TAEFLOAT real);

// 		update a scaler TEXT * qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec, 
		  const TEXT *string);

//		For arrays
// 	update a multivalued TAEINT qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec,
		  TAEINT count, TAEINT *intg );

// 	update a multivalued TAEFLOAT qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec,
	          TAEINT count, TAEFLOAT *real);

// 	update a multivalued TEXT * qualifier from another view TaeVarTable
    CODE Update ( const TEXT *itemName, TaeVarTable *newViewTable,
                  const TEXT *viewParm, const TEXT *qualSpec,
		  TAEINT count, const TEXT **string);

//
// Methods to return the current value of items or attributes of items
// Note: These methods imply the caller knows the data type of the item or
//       qualifier.
// Note2: the item MUST exist in the panel. If not:
//	0 will be returned for TAEINT
//	0.0 for TAEFLOAT
//	NULL for TEXT *
//	NULL for all arrays 
//
    TAEINT	Intg (const TEXT *itemName);
    TAEFLOAT	Real (const TEXT *itemName);
    TEXT *	String (const TEXT *itemName); //returns ptr, not copy of string

    TAEINT *	IntgVec (const TEXT *itemName);
    TAEFLOAT *	RealVec (const TEXT *itemName);
    TEXT **	StringVec (const TEXT *itemName);

    COUNT	Count(const TEXT *itemName);		// vector Count

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string target 
// data type
    int		Type(const TEXT *itemName);		// target Data type

    TEXT *	Title(const TEXT *itemName);		// return Item's title

//	For Qualifiers (in the view)

    TAEINT	Intg (const TEXT *itemName, const TEXT *qualSpec);
    TAEFLOAT	Real (const TEXT *itemName, const TEXT *qualSpec);
    TEXT *	String (const TEXT *itemName, const TEXT *qualSpec); 

    TAEINT *	IntgVec (const TEXT *itemName, const TEXT *qualSpec);
    TAEFLOAT *	RealVec (const TEXT *itemName, const TEXT *qualSpec);
    TEXT **	StringVec (const TEXT *itemName, const TEXT *qualSpec);

    COUNT	Count(const TEXT *itemName, const TEXT *qualSpec);

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string view 
// qualifier data type
    int		Type(const TEXT *itemName, const TEXT *qualSpec);

// 	For Qualifiers (in the target)

    TAEINT	IntgTarget (const TEXT *itemName, const TEXT *qualSpec);
    TAEFLOAT	RealTarget (const TEXT *itemName, const TEXT *qualSpec);
    TEXT *	StringTarget (const TEXT *itemName, const TEXT *qualSpec); 

    TAEINT *	IntgVecTarget (const TEXT *itemName, const TEXT *qualSpec);
    TAEFLOAT *	RealVecTarget (const TEXT *itemName, const TEXT *qualSpec);
    TEXT **	StringVecTarget (const TEXT *itemName, const TEXT *qualSpec);

    COUNT	CountTarget(const TEXT *itemName, const TEXT *qualSpec);

// returns V_INTEGER, V_REAL, V_STRING for integer, real and string target 
// qualifier data type
    int		TypeTarget(const TEXT *itemName, const TEXT *qualSpec);

//
//  Menu Methods
//
    CODE GetMenu (const TEXT *itemName, TEXT *entryName, //Wpt_GetMenu
                  COUNT *count, WPT_MENUENTRY **entries);

    CODE SetMenu (const TEXT *itemName, TEXT *entryName, //Wpt_SetMenu
                  FUNINT count, WPT_MENUENTRY *entries);

    CODE MenuUpdate ( const TEXT *itemName); 		 //Wpt_MenuUpdate

							 //Wpt_InsertMenuEntries
    CODE InsertMenuEntries (const TEXT *itemName, TEXT *entryName,
                            FUNINT count, WPT_MENUENTRY *entries);

							 //Wpt_RemoveMenuEntries
    CODE RemoveMenuEntries (const TEXT *itemName, FUNINT count, 
			    TEXT **entryNames);

							//Wpt_SetMenuSensitivity
    CODE SetMenuSensitivity (const TEXT *itemName, FUNINT count,
                             TEXT **entryNames, BOOL *state);

							//Wpt_GetMenuEntryPath
    CODE GetMenuEntryPath (const TEXT *itemName, TEXT *entryName,
                           TEXT *entryPath);

//  Others:

    TaeVarTable *TargetTable ();
    TaeVarTable *ViewTable();
    TaeCollection *Collection();

    TaeEventHandler* Handler() const;

    void RegisterItem ( TaeItem * );
    void DeregisterItem ( TaeItem * );

    CODE status();

    virtual void HandleEvent ( WptEvent *);

    Id PanelId();
    TaeItem *GetItem(const TEXT *itemName);    // get a registered item, by name

    TaeItem & operator[] (const TEXT *itemName);

    const TEXT *Name();

    slist *ItemList ();				// get list of registered items

//
// React Functions
//
    PREACTFUNC  ReactCB();			// get current React callback
    PREACTFUNC  ReactCB(PREACTFUNC newFunct);	// sets a new react function

		// PROTECTED
protected:
    virtual void React(WptEvent* e);           // event dispatcher
    
		// PRIVATE
private:
    TaeVarTable *viewTable;
    TaeVarTable *targetTable;

    slist itemlist;               	       // items registed with this panel

    CODE code;
    BOOL view_target_allocated;

    TaeCollection *collection;

    TEXT panelName[STRINGSIZ+1];
    TEXT resourceFile[STRINGSIZ+1];

    void Initialize ( const TEXT * panelName, const TEXT * resfilename );

    CODE FindTable ( TaeCollection *collect, TEXT *suffix, TaeVarTable **table);

    Id panelId;                  			// tae panel id
    const TaeEventHandler* eventHandler;  		// event handler
    WptEvent lastEvent;              			// backward comp.
    PREACTFUNC  reactFunc;      // Pointer to this instance's react function

// The following methods prevent symantically incorrect duplication of
// a TaePanel. Use Clone instead.
    TaePanel (const TaePanel& copy);
    TaePanel& operator=(const TaePanel& copy);
};

inline						 // alternate ViewUpdate
CODE TaePanel::UpdateView (const TEXT *itemName)
    {
    return Update (itemName, viewTable, itemName);
    }

inline
CODE TaePanel::UpdateTitle (const TEXT *itemName, const TEXT *string)
    {
    return Update (itemName, "", string);
    }

inline
TEXT *	TaePanel::Title(const TEXT *itemName)		// return Item's title
    {
    return TaePanel::String(itemName, "");
    }

//  Others:

inline
TaeVarTable *TaePanel::TargetTable () 
    { 
    return targetTable;
    }

inline
TaeVarTable *TaePanel::ViewTable() 
    { 
    return viewTable;
    }

inline
TaeCollection *TaePanel::Collection() 
    { 
    return collection; 
    }


inline
TaeEventHandler* TaePanel::Handler() const
    { 
    return (TaeEventHandler *) eventHandler; 
    }


inline
CODE TaePanel::status() 
    { 
    return code; 
    }

inline
Id TaePanel::PanelId()
    {
    return panelId;
    }


inline
TaeItem & TaePanel::operator[] (const TEXT *itemName)
    {
    return *GetItem(itemName);
    }


inline
const TEXT *TaePanel::Name()
    {
    return panelName;
    }


inline						// get list of registered items
slist *TaePanel::ItemList () 
    { 
    return &itemlist;
    }

//
// React Functions
//
inline						// get current React callback
PREACTFUNC  TaePanel::ReactCB() 
    {
    return reactFunc;
    }	


inline						// sets a new react function
PREACTFUNC  TaePanel::ReactCB(PREACTFUNC newFunct)
    {
    PREACTFUNC rtn = reactFunc;
    reactFunc = newFunct;
    return rtn;
    }

//
// ****************************** TaePanelWindow ***********************
//
//  TaePanelWindow class is for creating objects that receive and
//  process X events.
//

class TaePanelWindow
{
    friend class TaeEventHandler;
public:
    TaePanelWindow();
    TaePanelWindow(Window w);

    virtual ~TaePanelWindow() {};
    Window WindowId() const;

	// PROTECTED
protected:

    Window SetWindow(Window);
    virtual int HandleEvent(const TaeEventHandler&,const XEvent&);

	// PRIVATE
private:
    Window window;

// The following methods prevent symantically incorrect duplication of
// a TaePanelWindow.
    TaePanelWindow (const TaePanelWindow& copy);
    TaePanelWindow& operator=(const TaePanelWindow& copy);
};

inline
TaePanelWindow::TaePanelWindow() 
    { 
    window = 0; 
    }

inline
TaePanelWindow::TaePanelWindow(Window w)
    { 
    window = w; 
    }

inline
Window TaePanelWindow::WindowId() const 
    { 
    return window; 
    }

inline
int TaePanelWindow::HandleEvent(const TaeEventHandler&,const XEvent&)
    {
    return 0;
    }

//
// ****************************** TaePanelFile ***********************
//
//    TaePanelFile objects receives and process file or sockets events
//

class TaePanelFile
{
    friend class TaeEventHandler;

public:
    TaePanelFile(int d);
    virtual ~TaePanelFile(){};

    int Descriptor() const;

protected: 
    virtual int ReadEvent(const TaeEventHandler&);
    virtual int WriteEvent(const TaeEventHandler&);
    virtual int ExceptEvent(const TaeEventHandler&);
    virtual int HandleEvent(const TaeEventHandler&);

    int descriptor;
    Id sourceId;
};

inline
TaePanelFile::TaePanelFile(int d)
    { 
    descriptor = d; 
    sourceId = 0; 
    }

inline
int TaePanelFile::Descriptor() const 
    { 
    return descriptor; 
    }

inline
int TaePanelFile::ReadEvent(const TaeEventHandler&)
    { 
    return 0; 
    }

inline
int TaePanelFile::WriteEvent(const TaeEventHandler&)
    { 
    return 0; 
    }

inline
int TaePanelFile::ExceptEvent(const TaeEventHandler&)
    { 
    return 0; 
    }

inline
int TaePanelFile::HandleEvent(const TaeEventHandler&)
    { 
    return 0; 
    }

//
// ****************************** TaePanelTimeout ***********************
//
//    TaePanelTimeout object receive and process Timeout event
//

class TaePanelTimeout
{
    friend class TaeEventHandler;

public:
    TaePanelTimeout() {};
    virtual ~TaePanelTimeout () {};

protected:
    virtual void Timeout(const TaeEventHandler& ){}
    virtual void Done(const TaeEventHandler& ){}
};



//
// ****************************** TaePanelTimer ***********************
//
//    TaePanelTimer objects receives and process timer events
//

class TaePanelTimer
{
    friend class TaeEventHandler;

public:
    TaePanelTimer();
    virtual ~TaePanelTimer(){};

protected:
    virtual int HandleEvent( const TaeEventHandler& );

    // Get information from the event
    void *GetUserContext ();
    int GetInterval ();
    BOOL GetRepeat ();

private:
    Id timerId;

    // the following fields are set when an event occurs
    void *userContext;
    BOOL repeat;
    int  interval;

    void SetEventFields (void*, int, BOOL);
};


inline
TaePanelTimer::TaePanelTimer()
    {
    timerId = 0;
    userContext = nil;
    repeat = FALSE;
    interval = -1;
    }

inline
int TaePanelTimer::HandleEvent(const TaeEventHandler&)
    {
    return 0;
    }

inline void TaePanelTimer::SetEventFields (void *uc, int i, BOOL r)
    {
    userContext = uc;
    interval = i;
    repeat = r;
    }

inline void *TaePanelTimer::GetUserContext () {return (userContext);}
inline int TaePanelTimer::GetInterval () {return (interval);}
inline BOOL TaePanelTimer::GetRepeat () {return (repeat);}


//
// ****************************** TaeEventHandler ***********************
//
// TaeEventHandler object contains the main event loops. It gets and
// distributes events to the appropriate TaePanel, TaePanelFile,
// TaePanelTimeout, or TaePanelWindow objects.
//

class TaeEventHandler
{
public:

    TaeEventHandler();

    virtual ~TaeEventHandler(){}

    void Register ( TaePanelWindow* );
    void Deregister ( TaePanelWindow* );

    void Register ( TaePanelFile*, int );
    void Deregister ( TaePanelFile* );

    void Register(TaePanelTimeout*,int,int=-1);
    void Deregister ();

    void Register(TaePanelTimer* panelTimer, 
		  int interval,
		  BOOL repeat,
		  void *client_data);
    void Deregister (TaePanelTimer *);

    void DeregisterAllWindows();
    void DeregisterAllFiles();

//
//  the next 3 methods are for backward comp.
//
    TaePanelWindow * SetDefaultTaePanelWindow ( TaePanelWindow * );

    TaePanelFile * SetDefaultTaePanelFile ( TaePanelFile * );

    virtual void ProcessEvents();
    virtual void EndProcessEvents ();

protected:

    void DeliverWindowEvent(WptEvent&);
    void DeliverFileEvent(WptEvent&);
    void DeliverTimeoutEvent();
    void DeliverTimerEvent(WptEvent&);

private:
    BOOL TaeEventForever;
    slist panelWindows;
    TaePanelWindow* defaultPanelWindow;

    slist panelFiles;
    TaePanelFile* defaultPanelFile;

    TaePanelTimeout* panelTimeout;
    int interval;
    int count;

    slist panelTimers;

// The following methods prevent symantically incorrect duplication of
// a TaeEventHandler.
    TaeEventHandler (const TaeEventHandler& copy);
    TaeEventHandler& operator=(const TaeEventHandler& copy);
};


	// INLINE methods
inline
TaeEventHandler::TaeEventHandler()
    {
    panelTimeout = NULL;
    interval = 0; count = 0;
    defaultPanelWindow = NULL;
    defaultPanelFile = NULL;
    TaeEventForever = TRUE;
    }

inline
void TaeEventHandler::EndProcessEvents () 
    { 
    TaeEventForever = FALSE; 
    }
#endif

NOOP=noop	#force Bourne Shell
#
#  Script to compile and link single-file TAE Plus c++ application.
#
# CHANGE LOG
#  06-apr-92 Created...tpl
# 18-nov-92	PR 1718: On some platforms (i.e. Ultrix and SGI), the suffix
#		supplied for basename is a regular expression.  Thus
#		"basename minwptc .c" will result in "minwp".  Need to
#		backslash the ".".  Also need to put the suffix in quotes
#		for the SGI...cew
# 25-jan-93	PR1807: Add filename extension for IBM; check for env var
#          	$TAECG_LANG_SUFFIX on all platforms; edit generated makefile
#         	before running make if env var is set...kbs
# 25-may-93	$TAEBIN/{all,csh} changed to $TAE/bin/{all,csh}...kbs
# 03-may-94	PR2700: (Concurrent) Handle .C extension...kbs
# 06-jul-94	PR2798: (SCO) Must extend PR2700 fix to sco...swd
# 05-aug-94	PR2819: (Intergraph) Handle .C extension...dag
# 06-dec-94	Ultrix Port: Handle .C extension also...shj
# 07-dec-94	Changed ".cc" to "C++ source" in error message...dag

if [ $#  != 1 ]
then
  echo "$0: Single argument expected (name of C++ source file)."
  exit 1
fi

if [ -z "$TAEPLAT" ] ; then
    echo "\$TAEPLAT (TAE platform) is not set in your environment."
    echo "Type:  source $TAE/bin/csh/taesetup"
    echo "Then run $0 again."
    exit 1
fi

if test "$TAEPLAT" = sgi
then
    if [ -z "$TAECG_LANG_SUFFIX" ] ; then
        prog=`basename $1 '\.cc'`		#in case .cc extension specified
    else
        prog=`basename $1 '\.$TAECG_LANG_SUFFIX'`		
    fi

elif test "$TAEPLAT" = rs6000 || test "$TAEPLAT" = rtu || test "$TAEPLAT" = sco || test "$TAEPLAT" = ingr || test "$TAEPLAT" = mipsel
then		# IBM, Concurrent, SCO, Intergraph, and Ultrix
    if [ -z "$TAECG_LANG_SUFFIX" ] ; then
        prog=`basename $1 .C`		
    else
        prog=`basename $1 .$TAECG_LANG_SUFFIX`		
    fi

else # sun4, etc.
    if [ -z "$TAECG_LANG_SUFFIX" ] ; then
        prog=`basename $1 .cc`		
    else
        prog=`basename $1 .$TAECG_LANG_SUFFIX`		
    fi
fi

IMAKEFILE=$TAE/bin/all/taec++one.imak
MAKEFILE=$prog.mak

echo "     Beginning single source file build."

#If $prog.mak already exists, back it up and produce a warning.
if [ -f ${prog}.mak ] ; then
    echo "     Backing up existing file ${prog}.mak to ${prog}.mak.bak."
    test -f ${prog}.mak.bak && rm -f ${prog}.mak.bak
    mv ${prog}.mak ${prog}.mak.bak
fi

#Generate prog_name.mak file to be used to build program.
echo "     Generating file ${prog}.mak."
imake -TTAEmake.tmpl -I$TAE/config -DPROG_NAME=$prog -f $IMAKEFILE -s $MAKEFILE

# Have to edit the newly generated makefile on the fly before running make
# if env var is set. (Edit commands must be in column one.)
if [ ! -z "$TAECG_LANG_SUFFIX" ] ; then
ed $MAKEFILE 1>/dev/null 2>&1 << !
/EXTRA_C++FLAGS =/s//EXTRA_C++FLAGS = $(C++SUFFIX_FLAGS)/
/\.SUFFIXES: \.cc \.C/s//\.SUFFIXES: \.cc \.C \.$TAECG_LANG_SUFFIX/
a
.$TAECG_LANG_SUFFIX.o:
	$(C++) -c $(C++FLAGS) $<
.
w
q
!
fi

if test $TAEPLAT && test "$TAEPLAT" = apollo
then
echo "     make -P -f ${MAKEFILE}"
make -P -f $MAKEFILE 
else
echo "     make -f ${MAKEFILE}"
make -f $MAKEFILE 
fi
exit 0

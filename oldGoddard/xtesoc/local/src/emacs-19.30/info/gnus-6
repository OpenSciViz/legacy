This is Info file ../info/gnus, produced by Makeinfo-1.55 from the
input file gnus.texi.

   This file documents Gnus, the GNU Emacs newsreader.

   Copyright (C) 1995 Free Software Foundation, Inc.

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided also
that the entire resulting derived work is distributed under the terms
of a permission notice identical to this one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions.


File: gnus,  Node: Optional Backend Functions,  Prev: Required Backend Functions,  Up: Backend Interface

Optional Backend Functions
--------------------------

`(nnchoke-retrieve-groups GROUPS &optional SERVER)'
     GROUPS is a list of groups, and this function should request data
     on all those groups.  How it does it is of no concern to Gnus, but
     it should attempt to do this in a speedy fashion.

     The return value of this function can be either `active' or
     `group', which says what the format of the result data is.  The
     former is in the same format as the data from
     `nnchoke-request-list', while the latter is a buffer full of lines
     in the same format as `nnchoke-request-group' gives.

          group-buffer = *active-line / *group-status

`(nnchoke-request-update-info GROUP INFO &optional SERVER)'
     A Gnus group info (*note Group Info::.) is handed to the backend
     for alterations.  This comes in handy if the backend really
     carries all the information (as is the case with virtual an imap
     groups).  This function may alter the info in any manner it sees
     fit, and should return the (altered) group info.  This function
     may alter the group info destructively, so no copying is needed
     before boogying.

     There should be no result data from this function.

`(nnchoke-request-scan &optional GROUP SERVER)'
     This function may be called at any time (by Gnus or anything else)
     to request that the backend check for incoming articles, in one
     way or another.  A mail backend will typically read the spool file
     or query the POP server when this function is invoked.  The GROUP
     doesn't have to be heeded - if the backend decides that it is too
     much work just scanning for a single group, it may do a total scan
     of all groups.  It would be nice, however, to keep things local if
     that's practical.

     There should be no result data from this function.

`(nnchoke-request-asynchronous GROUP &optional SERVER ARTICLES)'
     This is a request to fetch articles asynchronously later.
     ARTICLES is an alist of (ARTICLE-NUMBER LINE-NUMBER).  One would
     generally expect that if one later fetches article number 4, for
     instance, some sort of asynchronous fetching of the articles after
     4 (which might be 5, 6, 7 or 11, 3, 909 depending on the order in
     that alist) would be fetched asynchronouly, but that is left up to
     the backend.  Gnus doesn't care.

     There should be no result data from this function.

`(nnchoke-request-group-description GROUP &optional SERVER)'
     The result data from this function should be a description of
     GROUP.

          description-line = name <TAB> description eol
          name             = <string>
          description      = <text>

`(nnchoke-request-list-newsgroups &optional SERVER)'
     The result data from this function should be the description of all
     groups available on the server.

          description-buffer = *description-line

`(nnchoke-request-newgroups DATE &optional SERVER)'
     The result data from this function should be all groups that were
     created after `date', which is in normal human-readable date
     format.  The data should be in the active buffer format.

`(nnchoke-request-create-groups GROUP &optional SERVER)'
     This function should create an empty group with name GROUP.

     There should be no return data.

`(nnchoke-request-expire-articles ARTICLES &optional GROUP SERVER FORCE)'
     This function should run the expiry process on all articles in the
     ARTICLES range (which is currently a simple list of article
     numbers.)  It is left up to the backend to decide how old articles
     should be before they are removed by this function.  If FORCE is
     non-`nil', all ARTICLES should be deleted, no matter how new they
     are.

     This function should return a list of articles that it did not/was
     not able to delete.

     There should be no result data returned.

`(nnchoke-request-move-article ARTICLE GROUP SERVER ACCEPT-FORM'
     &optional LAST)

     This function should move ARTICLE (which is a number) from GROUP
     by calling ACCEPT-FORM.

     This function should ready the article in question for moving by
     removing any header lines it has added to the article, and
     generally should "tidy up" the article.  Then it should `eval'
     ACCEPT-FORM in the buffer where the "tidy" article is.  This will
     do the actual copying.  If this `eval' returns a non-`nil' value,
     the article should be removed.

     If LAST is `nil', that means that there is a high likelihood that
     there will be more requests issued shortly, so that allows some
     optimizations.

     There should be no data returned.

`(nnchoke-request-accept-article GROUP &optional LAST)'
     This function takes the current buffer and inserts it into GROUP.
     If LAST in `nil', that means that there will be more calls to this
     function in short order.

     There should be no data returned.

`(nnchoke-request-replace-article ARTICLE GROUP BUFFER)'
     This function should remove ARTICLE (which is a number) from GROUP
     and insert BUFFER there instead.

     There should be no data returned.


File: gnus,  Node: Score File Syntax,  Next: Headers,  Prev: Backend Interface,  Up: Appendix

Score File Syntax
=================

   Score files are meant to be easily parsable, but yet extremely
mallable.   It was decided that something that had the same read syntax
as an Emacs Lisp list would fit that spec.

   Here's a typical score file:

     (("summary"
       ("win95" -10000 nil s)
       ("Gnus"))
      ("from"
       ("Lars" -1000))
      (mark -100))

   BNF definition of a score file:

     score-file       = "" / "(" *element ")"
     element          = rule / atom
     rule             = string-rule / number-rule / date-rule
     string-rule      = "(" quote string-header quote space *string-match ")"
     number-rule      = "(" quote number-header quote space *number-match ")"
     date-rule        = "(" quote date-header quote space *date-match ")"
     quote            = <ascii 34>
     string-header    = "subject" / "from" / "references" / "message-id" /
                        "xref" / "body" / "head" / "all" / "followup"
     number-header    = "lines" / "chars"
     date-header      = "date"
     string-match     = "(" quote <string> quote [ "" / [ space score [ "" /
                        space date [ "" / [ space string-match-t ] ] ] ] ] ")"
     score            = "nil" / <integer>
     date             = "nil" / <natural number>
     string-match-t   = "nil" / "s" / "substring" / "S" / "Substring" /
                        "r" / "regex" / "R" / "Regex" /
                        "e" / "exact" / "E" / "Exact" /
                        "f" / "fuzzy" / "F" / "Fuzzy"
     number-match     = "(" <integer> [ "" / [ space score [ "" /
                        space date [ "" / [ space number-match-t ] ] ] ] ] ")"
     number-match-t   = "nil" / "=" / "<" / ">" / ">=" / "<="
     date-match       = "(" quote <string> quote [ "" / [ space score [ "" /
                        space date [ "" / [ space date-match-t ] ] ] ] ")"
     date-match-t     = "nil" / "at" / "before" / "after"
     atom             = "(" [ required-atom / optional-atom ] ")"
     required-atom    = mark / expunge / mark-and-expunge / files /
                        exclude-files / read-only / touched
     optional-atom    = adapt / local / eval
     mark             = "mark" space nil-or-number
     nil-or-t         = "nil" / <integer>
     expunge          = "expunge" space nil-or-number
     mark-and-expunge = "mark-and-expunge" space nil-or-number
     files            = "files" *[ space <string> ]
     exclude-files    = "exclude-files" *[ space <string> ]
     read-only        = "read-only" [ space "nil" / space "t" ]
     adapt            = "adapt" [ space "nil" / space "t" / space adapt-rule ]
     adapt-rule       = "(" *[ <string> *[ "(" <string> <integer> ")" ] ")"
     local            = "local" *[ space "(" <string> space <form> ")" ]
     eval             = "eval" space <form>
     space            = *[ " " / <TAB> / <NEWLINE> ]

   Any unrecognized elements in a score file should be ignored, but not
discarded.

   As you can see, white space is needed, but the type and amount of
white space is irrelevant.  This means that formatting of the score
file is left up to the programmer - if it's simpler to just spew it all
out on one looong line, then that's ok.

   The meaning of the various atoms are explained elsewhere in this
manual.


File: gnus,  Node: Headers,  Next: Ranges,  Prev: Score File Syntax,  Up: Appendix

Headers
=======

   Gnus uses internally a format for storing article headers that
corresponds to the NOV format in a mysterious fashion.  One could
almost suspect that the author looked at the NOV specification and just
shamelessly *stole* the entire thing, and one would be right.

   "Header" is a severly overloaded term.  "Header" is used in RFC1036
to talk about lines in the head of an article (eg., `From').  It is
used by many people as a synonym for "head" - "the header and the
body".  (That should be avoided, in my opinion.)  And Gnus uses a format
interanally that it calls "header", which is what I'm talking about
here.  This is a 9-element vector, basically, with each header (ouch)
having one slot.

   These slots are, in order: `number', `subject', `from', `date',
`id', `references', `chars', `lines', `xref'.  There are macros for
accessing and setting these slots - they all have predicatable names
beginning with `mail-header-' and `mail-header-set-', respectively.

   The `xref' slot is really a `misc' slot.  Any extra info will be put
in there.


File: gnus,  Node: Ranges,  Next: Group Info,  Prev: Headers,  Up: Appendix

Ranges
======

   GNUS introduced a concept that I found so useful that I've started
using it a lot and have elaborated on it greatly.

   The question is simple: If you have a large amount of objects that
are identified by numbers (say, articles, to take a *wild* example)
that you want to callify as being "included", a normal sequence isn't
very useful.  (A 200,000 length sequence is a bit long-winded.)

   The solution is as simple as the question: You just collapse the
sequence.

     (1 2 3 4 5 6 10 11 12)

   is transformed into

     ((1 . 6) (10 . 12))

   To avoid having those nasty `(13 . 13)' elements to denote a
lonesome object, a `13' is a valid element:

     ((1 . 6) 7 (10 . 12))

   This means that comparing two ranges to find out whether they are
equal is slightly tricky:

     ((1 . 6) 7 8 (10 . 12))

   and

     ((1 . 5) (7 . 8) (10 . 12))

   are equal.  In fact, any non-descending list is a range:

     (1 2 3 4 5)

   is a perfectly valid range, although a pretty longwinded one.  This
is also legal:

     (1 . 5)

   and is equal to the previous range.

   Here's a BNF definition of ranges.  Of course, one must remember the
semantic requirement that the numbers are non-descending.  (Any number
of repetition of the same number is allowed, but apt to disappear in
range handling.)

     range           = simple-range / normal-range
     simple-range    = "(" number " . " number ")"
     normal-range    = "(" start-contents ")"
     contents        = "" / simple-range *[ " " contents ] /
                       number *[ " " contents ]

   Gnus currently uses ranges to keep track of read articles and article
marks.  I plan on implementing a number of range operators in C if The
Powers That Be are willing to let me.  (I haven't asked yet, because I
need to do some more thinking on what operators I need to make life
totally range-based without ever having to convert back to normal
sequences.)


File: gnus,  Node: Group Info,  Prev: Ranges,  Up: Appendix

Group Info
==========

   Gnus stores all permanent info on groups in a "group info" list.
This list is from three to six elements (or more) long and exhaustively
describes the group.

   Here are two example group infos; one is a very simple group while
the second is a more complex one:

     ("no.group" 5 (1 . 54324))
     
     ("nnml:my.mail" 3 ((1 . 5) 9 (20 . 55))
                     ((tick (15 . 19)) (replied 3 6 (19 . 23)))
                     (nnml "")
                     (auto-expire (to-address "ding@ifi.uio.no")))

   The first element is the group name as Gnus knows the group; the
second is the group level; the third is the read articles in range
format; the fourth is a list of article marks lists; the fifth is the
select method; and the sixth contains the group parameters.

   Here's a BNF definition of the group info format:

     info          = "(" group space level space read
                     [ "" / [ space marks-list [ "" / [ space method [ "" /
                     space parameters ] ] ] ] ] ")"
     group         = quote <string> quote
     level         = <integer in the range of 1 to inf>
     read          = range
     marks-lists   = nil / "(" *marks ")"
     marks         = "(" <string> range ")"
     method        = "(" <string> *elisp-forms ")"
     parameters    = "(" *elisp-forms ")"

   Actually that `marks' rule is a fib.  A `marks' is a `<string>'
consed on to a `range', but that's a bitch to say in pseudo-BNF.


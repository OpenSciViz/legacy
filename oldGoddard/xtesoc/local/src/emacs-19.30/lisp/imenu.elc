;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 24 22:57:05 1995
;;; from file /home/fsf/rms/e19/lisp/imenu.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`imenu.el' was compiled for Emacs 19.29 or later"))


#@57 *Non-nil means use a keymap when making the mouse menu.
(defvar imenu-use-keymap-menu nil (#$ . -468))
#@56 *Non-nil means Imenu should always rescan the buffers.
(defvar imenu-auto-rescan nil (#$ . -577))
#@87 * auto-rescan is disabled in buffers larger than this.
This variable is buffer-local.
(defvar imenu-auto-rescan-maxout 60000 (#$ . -681))
#@287 *Set this to non-nil for displaying the index in a completion buffer.

Non-nil means always display the index in a completion buffer.
Nil means display the index as a mouse menu when the mouse was
used to invoke `imenu'.
`never' means never automatically display a listing of any kind.
(defvar imenu-always-use-completion-buffer-p nil (#$ . -826))
#@501 *The function to use for sorting the index mouse-menu.

Affects only the mouse index menu.

Set this to nil if you don't want any sorting (faster).
The items in the menu are then presented in the order they were found
in the buffer.

Set it to `imenu--sort-by-name' if you want alphabetic sorting.

The function should take two arguments and return T if the first
element should come before the second.  The arguments are cons cells;
(NAME . POSITION).  Look at `imenu--sort-by-name' for an example.
(defvar imenu-sort-function nil (#$ . -1181))
#@53 *Maximum number of elements in an index mouse-menu.
(defvar imenu-max-items 25 (#$ . -1733))
#@231 *Progress message during the index scanning of the buffer.
If non-nil, user gets a message during the scanning of the buffer

Relevant only if the mode-specific function that creates the buffer
index use `imenu-progress-message'.
(defvar imenu-scanning-message "Scanning buffer for index (%3d%%)" (#$ . -1833))
#@140 *The replacement string for spaces in index names.
Used when presenting the index in a completion-buffer to make the
names work as tokens.
(defvar imenu-space-replacement "^" (#$ . -2151))
#@150 *The separator between index names of different levels.
Used for making mouse-menu titles and for flattening nested indexes
with name concatenation.
(defvar imenu-level-separator ":" (#$ . -2347))
#@40 *The format for making a submenu name.
(defvar imenu-submenu-name-format "%s..." (#$ . -2550))
#@1065 The regex pattern to use for creating a buffer index.

If non-nil this pattern is passed to `imenu-create-index-with-pattern'
to create a buffer index.

It is an alist with elements that look like this: (MENU-TITLE
REGEXP INDEX). 

MENU-TITLE is a string used as the title for the submenu or nil if the
entries are not nested.

REGEXP is a regexp that should match a construct in the buffer that is
to be displayed in the menu; i.e., function or variable definitions,
etc.  It contains a substring which is the name to appear in the
menu.  See the info section on Regexps for more information.

INDEX points to the substring in REGEXP that contains the name (of the
function, variable or type) that is to appear in the menu.

For emacs-lisp-mode for example PATTERN would look like:

'((nil "^\\s-*(def\\(un\\|subst\\|macro\\|advice\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2)
  ("*Vars*" "^\\s-*(def\\(var\\|const\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2)
  ("*Types*" "^\\s-*(def\\(type\\|struct\\|class\\|ine-condition\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2))

The variable is buffer-local.
(defvar imenu-generic-expression nil (#$ . 2653))
(make-variable-buffer-local (quote imenu-generic-expression))
#@370 The function to use for creating a buffer index.

It should be a function that takes no arguments and returns an index
of the current buffer as an alist.  The elements in the alist look
like: (INDEX-NAME . INDEX-POSITION).  You may also nest index list like
(INDEX-NAME . INDEX-ALIST).

This function is called within a `save-excursion'.

The variable is buffer-local.
(defvar imenu-create-index-function (quote imenu-default-create-index-function) (#$ . 3836))
(make-variable-buffer-local (quote imenu-create-index-function))
#@374 Function for finding the next index position.

If `imenu-create-index-function' is set to
`imenu-default-create-index-function', then you must set this variable
to a function that will find the next index, looking backwards in the
file.

The function should leave point at the place to be connected to the
index and it should return nil when it doesn't find another index.
(defvar imenu-prev-index-position-function (quote beginning-of-defun) (#$ . 4370))
(make-variable-buffer-local (quote imenu-prev-index-position-function))
#@138 Function for extracting the index name.

This function is called after the function pointed out by
`imenu-prev-index-position-function'.
(defvar imenu-extract-index-name-function nil (#$ . 4905))
(make-variable-buffer-local (quote imenu-extract-index-name-function))
(defalias 'imenu-progress-message '(macro . #[(prevpos &optional relpos reverse) "����\f�\f \f� �DDC�\f� Ȃ# ����\fEE���\f�BBEEEE�" [and imenu-scanning-message let pos relpos imenu--relative-position reverse if t > + 5 prevpos progn (message imenu-scanning-message pos) setq (pos)] 11]))
(defalias 'imenu-example--name-and-position #[nil "���!�`� �`� Ɠ�\f{,B�" [forward-sexp -1 make-marker marker end beg nil] 3])
(defalias 'imenu-example--lisp-extract-index-name #[nil "� ��!� ��Ǐ*�" [match-data match-data ((store-match-data match-data)) looking-at "(def" nil (byte-code "��!���!�`��!�`�{*�" [down-list 1 forward-sexp 2 -1 end beg] 4) ((error))] 3])
(defalias 'imenu-example--create-lisp-index #[nil "�����db��  ��\"�)� �� �B ��!�	�\\V�A �\"�)� Ў��!�� ���!���!�e ��!�� \fB�� ��!�w ��!�� B�� ��!�� ��!�`Sf�U�� ��!���!���!�� B�� ��!�� \nB)*�  �� ��\"�)\f�� ��!\fBB�� ��!BB\n�� ��!\nBB-�" [nil prev-pos index-unknown-alist index-type-alist index-var-alist index-alist imenu-scanning-message 0 pos message beginning-of-defun imenu--relative-position t 5 match-data match-data ((store-match-data match-data)) looking-at "(def" down-list 1 "def\\(var\\|const\\)" forward-sexp 2 imenu-example--name-and-position "def\\(un\\|subst\\|macro\\|advice\\)" "def\\(type\\|struct\\|class\\|ine-condition\\)" 41 -1 100 imenu-create-submenu-name "Variables" "Types" "Syntax-unknown"] 6])
(byte-code "��!� �É�Ű�B��" [boundp imenu-example--function-name-regexp-c "^[a-zA-Z0-9]+[ 	]?" "\\([a-zA-Z0-9_*]+[ 	]+\\)?" "\\([*&]+[ 	]*\\)?" "\\([a-zA-Z0-9_*]+\\)[ 	]*(" current-load-list] 5)
(defalias 'imenu-example--create-c-index #[(&optional regexp) "���eb�\f� ��\f\"�)� 	ʎ�\f�( ��#�e \f�I ��!�\n�\\V�H �\f\"�)��!���`�\"b�g)	�=�  � B�  *\f�w ��\f\"�)�+�" [nil char prev-pos index-alist imenu-scanning-message 0 pos message match-data match-data ((store-match-data match-data)) re-search-forward regexp imenu-example--function-name-regexp-c t imenu--relative-position 5 backward-up-list 1 scan-sexps 59 imenu-example--name-and-position 100] 5])
(byte-code "��\nB��!� ��\nB��!���!� ��\nBŇ" [("*Rescan*" . -99) imenu--rescan-item current-load-list boundp imenu--index-alist nil make-variable-buffer-local imenu--history-list] 2)
(defalias 'imenu--sort-by-name #[(item1 item2) "@	@��" [item1 item2] 2])
(defalias 'imenu--relative-position #[(&optional reverse) "`� � 	\nZ	�V� \nS	ť�]��& \nS�_	�]�*�" [buffer-size total pos reverse 50000 100 1] 3])
(defalias 'imenu-create-submenu-name #[(name) "�	\n\"�" [format imenu-submenu-name-format name] 3])
(defalias 'imenu--split #[(list n) "����. �A@\fBT�U�	 \f�B���	 \f�7 \f�B�,�" [list nil 0 i sublist result remain n] 5])
(defalias 'imenu--split-menu #[(menulist title) "����\f\"\"B�" ["Index menu" mapcar #[(menu) "��\n\"B�" [format "(%s)" title menu] 3] imenu--split menulist imenu-max-items] 6])
(defalias 'imenu--make-index-alist #[nil "� 	� 	� � V� �\f )�  ��!�B�" [imenu--index-alist imenu-auto-rescan buffer-size imenu-auto-rescan-maxout imenu-create-index-function error "No items suitable for an index found in this buffer" imenu--rescan-item] 2])
(defalias 'imenu--cleanup #[(&optional alist) "� 	� ��\"� ć" [alist imenu--index-alist mapcar #[(item) "�	A!� 	A��	A:� �	A!�" [markerp item nil imenu--cleanup] 3] t] 3])
(defalias 'imenu--create-keymap-2 #[(alist counter) "���\f\")�" [nil map mapcar #[(item) "A<� �\nT�@�@F�A\n�\\\"\"���B@@)BB�" [item append counter keymap imenu--create-keymap-2 10 (nil) t end] 6] alist] 3])
(defalias 'imenu--create-keymap-1 #[(title alist) "��\nD�\f�\"\"�" [append keymap title imenu--create-keymap-2 alist 0] 5])
#@65 Check whether the string STR is contained in multi-level ALIST.
(defalias 'imenu--in-alist #[(str alist) "������= @�AA\f@��( �\f�\n \n<�\n �\n\"��\n ��� 	,�" [nil res tail head elt alist str imenu--in-alist] 5 (#$ . 8959)])
#@223 *Wrapper for index searching functions.

Moves point to end of buffer and then repeatedly calls
`imenu-prev-index-position-function' and `imenu-extract-index-name-function'.
Their results are gathered into an index alist.
(defalias 'imenu-default-create-index-function #[nil "�	!�w �\n!�w É�db��) �	�	\"�	)	 �` �K ��!�	�\\V�J �	\"�	)�\n )\f;�) \f`BB�) �s �	�	\"�	)+��� �!���\"�" [fboundp imenu-prev-index-position-function imenu-extract-index-name-function nil name prev-pos index-alist imenu-scanning-message 0 pos message imenu--relative-position t 5 100 imenu-generic-expression imenu--generic-function error "The mode \"%s\" does not take full advantage of imenu.el yet." mode-name] 4 (#$ . -9212)])
(defalias 'imenu--replace-spaces #[(name replacement) "��\n�#�" [mapconcat #[(ch) "�	�\"�	 ��	!�" [char-equal ch 32 replacement char-to-string] 3] name ""] 4])
(defalias 'imenu--flatten-index-alist #[(index-alist &optional concat-names prefix) "��\n\"�" [mapcan #[(item) "@A� \f� \f	Q� 	�\n!�# \n��+ \nBC�0 �\n\"+�" [item name pos concat-names prefix imenu-level-separator new-prefix markerp imenu--flatten-index-alist] 3] index-alist] 3])
#@1184 Return an index of the current buffer as an alist.

PATTERN is an alist with elements that look like this: (MENU-TITLE
REGEXP INDEX).

MENU-TITLE is a string used as the title for the submenu or nil if the
entries are not nested.

REGEXP is a regexp that should match a construct in the buffer that is
to be displayed in the menu; i.e., function or variable definitions,
etc.  It contains a substring which is the name to appear in the
menu.  See the info section on Regexps for more information.

INDEX points to the substring in REGEXP that contains the name (of the
function, variable or type) that is to appear in the menu.

For emacs-lisp-mode for example PATTERN would look like:

'((nil "^\\s-*(def\\(un\\|subst\\|macro\\|advice\\)\\s-+\\([-A-Za-z0-9]+\\)" 2)
  ("*Vars*" "^\\s-*(def\\(var\\|const\\)\\s-+\\([-A-Za-z0-9]+\\)" 2)
  ("*Types*" "^\\s-*(def\\(type\\|struct\\|class\\|ine-condition\\)\\s-+\\([-A-Za-z0-9]+\\)" 2))'

Returns an index of the current buffer as an alist.  The elements in
the alist look like: (INDEX-NAME . INDEX-POSITION).  They may also be
nested index lists like (INDEX-NAME . INDEX-ALIST) depending on
pattern.

(imenu--generic-function PATTERN).
(defalias 'imenu--generic-function #[(patterns) "�C�����#�Q�	\ndb�\f�+ ��\f\"�)� Ҏ�	��#�j \f�Y ��!��\\V�X �\f\"�)�\n�͔b���\"�)�1 *\f� ��\f\"�)��\",�" [dummy nil "\\(" mapconcat #[(pattern) "A@�" [pattern] 1] patterns "\\)\\|\\(" "\\)" prev-pos global-regexp found index-alist imenu-scanning-message 0 pos message match-data match-data ((store-match-data match-data)) re-search-backward t imenu--relative-position 5 mapcar #[(pat) "@A@AA@\f?�V �\n!�V 	�	��{B	;�5 \n�K ��!\n\"�K �!�B\nB�\n@�	AB�,+�" [pat index regexp menu-title found looking-at end beg t --arg1--89602 index-alist assoc imenu-create-submenu-name nil G89600] 4] 100 delete] 8 (#$ . 10423)])
#@129 Let the user select from INDEX-ALIST in a completion buffer with PROMPT.

Returns t for rescan and otherwise a position number.
(defalias 'imenu--completion-buffer #[(index-alist &optional prompt) "����\"�=�$ �\n� �\f����&�' ΋�;�1 ��T @��> ̂T �\f\"�A<�S �A\n\"�T +�" [nil mapcar #[(item) "�	@\n\"	AB�" [imenu--replace-spaces item imenu-space-replacement] 3] index-alist prepared-index-alist choice name imenu-always-use-completion-buffer-p never completing-read prompt "Index item: " t imenu--history-list ((byte-code "�����\f\"!����� �\f����&)ʇ" ["*Completions*" display-completion-list all-completions "" prepared-index-alist #[nil "p��q��*�" [buffer "*Completions*" completion-reference-buffer] 2] minibuffer-setup-hook completing-read prompt "Index item: " nil t imenu--history-list name] 7)) imenu--rescan-item assoc imenu--completion-buffer] 8 (#$ . 12349)])
#@176 Let the user select from a buffer index from a mouse menu.

INDEX-ALIST is the buffer index and EVENT is a mouse event.

Returns t for rescan and otherwise a position number.
(defalias 'imenu--mouse-menu #[(index-alist event &optional title) "�	�+ ��\f:� �A@B� ���*	\"�, \f�3 � \"	�\n�Y �	@�	AGW�Q 	A�V 	A@A\"	�	\"\n�� \n<�� \n@��� \nGS\n8;�� \nGS\n8\n�� \n@;�� \nA�� \n@\n\n�=�� \n�\n<�� �\n�� �\n\f\"@Q�� �\n\f\"@#�\n;�� \n@���\n\f\"�\nAU�� \n;�\n@��Ղ�\n\f\"*�" [imenu--split-menu imenu-sort-function sort nil index-alist oldlist res title buffer-name menu position imenu-use-keymap-menu imenu--create-keymap-1 1 x-popup-menu event imenu--mouse-menu imenu-level-separator rassq imenu--rescan-item imenu--in-alist t] 8 (#$ . 13259)])
#@619 Let the user select from a buffer index and return the chosen index.

If the user originally activated this function with the mouse, a mouse
menu is used.  Otherwise a completion buffer is used and the user is
prompted with PROMPT.

If you call this function with index alist ALIST, then it lets the user
select from ALIST.

With no index alist ALIST, it calls `imenu--make-index-alist' to
create the index alist.

If `imenu-always-use-completion-buffer-p' is non-nil, then the
completion buffer is always used, no matter if the mouse was used or
not.

The returned value is on the form (INDEX-NAME . INDEX-POSITION).
(defalias 'imenu-choose-buffer-index #[(&optional prompt alist) "�	<�\f�1 	ƚ�1 	�A@)�@)	�	!�0 	�0 �	!�)�=�k \f�A \f�C � \f�T �T �	\"�Y �\"��=�1 � �1 ��1 +�" [nil last-nonmenu-event t result mouse-triggered index-alist (menu-bar) event position window framep select-window alist imenu--make-index-alist imenu-always-use-completion-buffer-p imenu--mouse-menu imenu--completion-buffer prompt imenu--cleanup imenu--index-alist] 4 (#$ . 14088)])
#@150 Adds an "imenu" entry to the menubar for the current local keymap.
NAME is the string naming the menu to be added.
See 'imenu' for more information.
(defalias 'imenu-add-to-menubar #[(name) "�\f �� �\f�B#�" [window-system define-key current-local-map [menu-bar index] name imenu] 5 (#$ . 15180) "sMenu name: "])
#@127 Jump to a place in the buffer chosen using a buffer menu or mouse menu.
See `imenu-choose-buffer-index' for more information.
(defalias 'imenu #[(index-item) "�; � ��A!�( �A!eV�  �A!dW�\" ~��A!b�AeV�6 AdW�8 ~�Ab�" [index-item push-mark markerp marker-position] 2 (#$ . 15500) (list (save-restriction (widen) (imenu-choose-buffer-index)))])
(provide (quote imenu))

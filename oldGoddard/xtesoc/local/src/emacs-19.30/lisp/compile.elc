;ELC   
;;; compiled by roland@snoopy.gnu.ai.mit.edu on Mon Nov 13 23:50:25 1995
;;; from file /usr/src/emacs-build/lisp/compile.el
;;; emacs version 19.29.91.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`compile.el' was compiled for Emacs 19.29 or later"))


#@70 *List of hook functions run by `compilation-mode' (see `run-hooks').
(defvar compilation-mode-hook nil (#$ . -480))
#@71 *Number of lines in a compilation window.  If nil, use Emacs default.
(defvar compilation-window-height nil (#$ . -602))
#@743 List of error message descriptors for visiting erring functions.
Each error descriptor is a cons (or nil).  Its car is a marker pointing to
an error message.  If its cdr is a marker, it points to the text of the
line the message is about.  If its cdr is a cons, it is a list
((DIRECTORY . FILE) LINE [COLUMN]).  Or its cdr may be nil if that
error is not interesting.

The value may be t instead of a list; this means that the buffer of
error messages should be reparsed the next time the list of errors is wanted.

Some other commands (like `diff') use this list to control the error
message tracking facilites; if you change its structure, you should make
sure you also change those packages.  Perhaps it is better not to change
it at all.
(defvar compilation-error-list nil (#$ . 730))
#@61 Value of `compilation-error-list' after errors were parsed.
(defvar compilation-old-error-list nil (#$ . 1525))
#@440 Function to call to parse error messages from a compilation.
It takes args LIMIT-SEARCH and FIND-AT-LEAST.
If LIMIT-SEARCH is non-nil, don't bother parsing past that location.
If FIND-AT-LEAST is non-nil, don't bother parsing after finding that 
many new errors.
It should read in the source files which have errors and set
`compilation-error-list' to a list with an element for each error message
found.  See that variable for more info.
(defvar compilation-parse-errors-function (quote compilation-parse-errors) (#$ . 1644))
#@247 Function to compute the name of a compilation buffer.
The function receives one argument, the name of the major mode of the
compilation buffer.  It should return a string.
nil means compute the name with `(concat "*" (downcase major-mode) "*")'.
(defvar compilation-buffer-name-function nil (#$ . 2178))
#@163 *Function to call when a compilation process finishes.
It is called with two arguments: the compilation buffer, and a string
describing how the process finished.
(defvar compilation-finish-function nil (#$ . -2489))
#@162 The most recent compilation buffer.
A buffer becomes most recent when its compilation is started
or when it is used with \[next-error] or \[compile-goto-error].
(defvar compilation-last-buffer nil (#$ . 2712))
#@44 List of compilation processes now running.
(defvar compilation-in-progress nil (#$ . 2928))
(byte-code "�	��\n �	B��" [compilation-in-progress minor-mode-alist (compilation-in-progress " Compiling")] 2)
#@65 Position of end of buffer when last error messages were parsed.
(defvar compilation-parsing-end nil (#$ . 3138))
#@50 Message to print when no more matches are found.
(defvar compilation-error-message "No more errors" (#$ . 3257))
(byte-code "�	B��" [compilation-num-errors-found current-load-list] 2)
#@526 Alist that specifies how to match errors in compiler output.
Each elt has the form (REGEXP FILE-IDX LINE-IDX [COLUMN-IDX FILE-FORMAT...])
If REGEXP matches, the FILE-IDX'th subexpression gives the file name, and
the LINE-IDX'th subexpression gives the line number.  If COLUMN-IDX is
given, the COLUMN-IDX'th subexpression gives the column number on that line.
If any FILE-FORMAT is given, each is a format string to produce a file name to
try; %s in the string is replaced by the text matching the FILE-IDX'th
subexpression.
(defvar compilation-error-regexp-alist (quote (("\n\\([^:( 	\n]+\\)[:(][ 	]*\\([0-9]+\\)\\([) 	]\\|:\\([^0-9\n]\\|\\([0-9]+:\\)\\)\\)" 1 2 5) ("\n\\(Error\\|Warning\\) \\([^:( 	\n]+\\) \\([0-9]+\\)\\([) 	]\\|:[^0-9\n]\\)" 2 3) ("[ 	:]\\([^:( 	\n]+\\)[:(](+[ 	]*\\([0-9]+\\))[:) 	]*$" 1 2) ("([ 	]*\\([^:( 	\n]+\\)[:(][ 	]*\\([0-9]+\\))" 1 2) ("\n\\(cfe\\|fort\\): [^:\n]*: \\([^ \n]*\\), line \\([0-9]+\\):" 2 3) ("\\(\n\\|on \\)[Ll]ine[ 	]+\\([0-9]+\\)[ 	]+of[ 	]+\"?\\([^\":\n]+\\)\"?:" 3 2) ("\"\\([^,\" \n	]+\\)\", lines? \\([0-9]+\\)[:., (-]" 1 2) ("rror: \\([^,\" \n	]+\\)[,:] \\(line \\)?\\([0-9]+\\):" 1 3) ("in line \\([0-9]+\\) of file \\([^ \n]+[^. \n]\\)\\.? " 2 1) ("\n[EW], \\([^(\n]*\\)(\\([0-9]+\\),[ 	]*\\([0-9]+\\)" 1 2 3) ("\n[^0-9 \n	:]+:[ 	]*\\([^ \n	:]+\\):\\([0-9]+\\):\\(\\([0-9]+\\)[: 	]\\)?" 1 2 4) ("\n\\(cc\\| cft\\)-[0-9]+ c\\(c\\|f77\\): ERROR \\([^,\n]+, \\)* File = \\([^,\n]+\\), Line = \\([0-9]+\\)" 4 5) ("\n\\([^( \n	]+\\)(\\([0-9]+\\):\\([0-9]+\\)) : " 1 2 3) ("\n\\([^, ]+\\), line \\([0-9]+\\), char \\([0-9]+\\)[:., (-]" 1 2 3))) (#$ . 3449))
#@128 If not nil, M-x compile reads the compilation command to use.
Otherwise, M-x compile just uses the value of `compile-command'.
(defvar compilation-read-command t (#$ . 5064))
#@127 If not nil, M-x compile asks which buffers to save before compiling.
Otherwise, it saves all modified buffers without asking.
(defvar compilation-ask-about-save t (#$ . 5246))
#@72 Regexp used to match grep hits.  See `compilation-error-regexp-alist'.
(defvar grep-regexp-alist (quote (("^\\([^:( 	\n]+\\)[:( 	]+\\([0-9]+\\)[:) 	]" 1 2))) (#$ . 5428))
#@59 Last grep command used in \[grep]; default for next grep.
(defvar grep-command "grep -n " (#$ . 5605))
#@195 *List of directories to search for source files named in error messages.
Elements should be directory names, not file names of directories.
nil as an element means to try the default directory.
(defvar compilation-search-path (quote (nil)) (#$ . -5715))
#@467 Last shell command used to do a compilation; default for next compilation.

Sometimes it is useful for files to supply local values for this variable.
You might also use mode hooks to specify it in certain modes, like this:

    (setq c-mode-hook
      '(lambda () (or (file-exists-p "makefile") (file-exists-p "Makefile")
		      (progn (make-local-variable 'compile-command)
			     (setq compile-command
				    (concat "make -k "
					    buffer-file-name))))))
(defvar compile-command "make -k " (#$ . 5976))
#@208 Regular expression matching lines that indicate a new current directory.
This must contain one \(, \) pair around the directory name.

The default value matches lines printed by the `-w' option of GNU Make.
(defvar compilation-enter-directory-regexp ": Entering directory `\\(.*\\)'$" (#$ . 6497))
#@349 Regular expression matching lines that indicate restoring current directory.
This may contain one \(, \) pair around the name of the directory
being moved from.  If it does not, the last directory entered (by a
line matching `compilation-enter-directory-regexp') is assumed.

The default value matches lines printed by the `-w' option of GNU Make.
(defvar compilation-leave-directory-regexp ": Leaving directory `\\(.*\\)'$" (#$ . 6802))
#@139 Stack of previous directories for `compilation-leave-directory-regexp'.
The head element is the directory the compilation was started in.
(defvar compilation-directory-stack nil (#$ . 7247))
#@304 If non-nil, called when a compilation process dies to return a status message.
This should be a function a two arguments as passed to a process sentinel
(see `set-process-sentinel); it returns a cons (MESSAGE . MODELINE) of the
strings to write into the compilation buffer, and to put in its mode line.
(defvar compilation-exit-message-function nil (#$ . 7445))
(byte-code "��!� ��B��!� ��B" [boundp compile-history nil current-load-list grep-history] 2)
#@58 Additional expressions to highlight in Compilation mode.
(defvar compilation-mode-font-lock-keywords (quote (("^\\([^ \n:]*:\\([0-9]+:\\)+\\)\\(.*\\)$" 1 font-lock-function-name-face))) (#$ . 7916))
#@799 Compile the program including the current buffer.  Default: run `make'.
Runs COMMAND, a shell command, in a separate process asynchronously
with output going to the buffer `*compilation*'.

You can then use the command \[next-error] to find the next error message
and move to the source code that caused it.

Interactively, prompts for the command if `compilation-read-command' is
non-nil; otherwise uses `compile-command'.  With prefix arg, always prompts.

To run more than one compilation at once, start one and rename the
`*compilation*' buffer to some other name with \[rename-buffer].
Then start the next one.

The name used for the buffer is actually whatever is returned by
the function in `compilation-buffer-name-function', so you can set that
to a function that generates a unique name.
(defalias 'compile #[(command) "�?�\"��	�\"�" [command compile-command save-some-buffers compilation-ask-about-save nil compile-internal "No more errors"] 3 (#$ . 8122) (byte-code "� 	� ��\fŉ�%C�\fC�" [compilation-read-command current-prefix-arg read-from-minibuffer "Compile command: " compile-command nil (compile-history . 1)] 6)])
#@54 Re-compile the program including the current buffer.
(defalias 'recompile #[nil "�	?�\"��\f�\"�" [save-some-buffers compilation-ask-about-save nil compile-internal compile-command "No more errors"] 3 (#$ . 9268) nil])
#@25 The system null device.
(defvar grep-null-device "/dev/null" (#$ . 9492))
#@283 Run grep, with user-specified args, and collect output in a buffer.
While grep runs asynchronously, you can use the \[next-error] command
to find the text that grep hits refer to.

This command uses a special history list for its arguments, so you can
easily repeat a grep command.
(defalias 'grep #[(command-args) "�	�Q���%�q���!�L*�" [compile-internal command-args " " grep-null-device "No more grep hits" "grep" nil grep-regexp-alist buf make-local-variable compilation-exit-message-function #[(proc msg) "�	!�	!�=�' \n�U� Ƃ+ \n�U�  Ȃ+ 	\nB�+ 	\nB)�" [process-exit-status proc code process-status exit 0 ("finished (matches found)\n" . "matched") 1 ("finished with no matches found\n" . "no match") msg] 2]] 6 (#$ . 9573) (list (read-from-minibuffer "Run grep (like this): " grep-command nil nil (quote grep-history)))])
#@734 Run compilation command COMMAND (low level interface).
ERROR-MESSAGE is a string to print if the user asks to see another error
and there are no more errors.  Third argument NAME-OF-MODE is the name
to display as the major mode in the compilation buffer.

Fourth arg PARSER is the error parser function (nil means the default).  Fifth
arg REGEXP-ALIST is the error message regexp alist to use (nil means the
default).  Sixth arg NAME-FUNCTION is a function called to name the buffer (nil
means the default).  The defaults for these variables are the global values of
`compilation-parse-errors-function', `compilation-error-regexp-alist', and
`compilation-buffer-name-function', respectively.

Returns the compilation buffer created.
(defalias 'compile-internal #[(command error-message &optional name-of-mode parser regexp-alist name-function) "��\n�	 ��� � �\n!!�q��p!�	�D �	!�=�6 ���\n\"!�> ��Џ��D ��� \"�)� �)�P �W ��	q���p!�� ��p!���\"����!�)	p=�� db��	!�	q�� ���!L���!(L���!L��C)�e\"�\n+� =�� �e\"��!���!�\n�2B2�+�	\"#4�4�\"��4�\"��4!`	��4:B:*�.���\"\"!���!��@�	��A \"&��́B \"\"!�-	�C)�" [nil outbuf name-of-mode "Compilation" get-buffer-create name-function compilation-buffer-name-function #[(mode) "�	��Q�" ["*" mode] 3] get-buffer-process comp-proc process-status run yes-or-no-p format "A %s process is running; kill it? " (byte-code "�	!���!��	!�" [interrupt-process comp-proc sit-for 1 delete-process] 2) ((error)) error "Cannot have two processes in `%s' at once" buffer-name kill-all-local-variables regexp-alist compilation-error-regexp-alist parser compilation-parse-errors-function default-directory outwin thisdir buffer-read-only buffer-disable-undo erase-buffer buffer-enable-undo "cd " "\n" command set-buffer-modified-p display-buffer compilation-mode make-local-variable compilation-error-message error-message compilation-directory-stack set-window-start mode-name selected-window set-window-point compilation-set-window-height fboundp start-process "EMACS=t" process-environment start-process-shell-command proc set-process-sentinel compilation-sentinel set-process-filter compilation-filter process-mark compilation-in-progress message "Executing `%s'..." sit-for 0 call-process shell-file-name "-c" "Executing `%s'...done" compilation-last-buffer] 8 (#$ . 10417)])
(defalias 'compilation-set-window-height #[(window) "�- �\n!��\n!!U�- \n��\n!!=?�- �� Ȏ�\n!��� Z!+�" [compilation-window-height window-width window frame-width window-frame frame-root-window selected-window w ((select-window w)) select-window enlarge-window window-height] 4])
#@38 Keymap for `compilation-minor-mode'.
(defvar compilation-minor-mode-map (byte-code "� �	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#�	)�" [make-sparse-keymap map define-key [mouse-2] compile-mouse-goto-error "" compile-goto-error "" "" kill-compilation "�" compilation-next-error "�" compilation-previous-error "�" compilation-previous-file "�" compilation-next-file] 4) (#$ . 13078))
#@84 Keymap for compilation log buffers.
`compilation-minor-mode-map' is a cdr of this.
(defvar compilation-mode-map (byte-code "�	B�\n��#��\n��#��\n����!B#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#�\n)�" [keymap compilation-minor-mode-map map define-key " " scroll-up "" scroll-down [menu-bar compilation-menu] "Compile" make-sparse-keymap [menu-bar compilation-menu compilation-mode-kill-compilation] ("Stop Compilation" . kill-compilation) [menu-bar compilation-menu compilation-mode-separator2] ("----") [menu-bar compilation-menu compilation-mode-first-error] ("First Error" . first-error) [menu-bar compilation-menu compilation-mode-previous-error] ("Previous Error" . previous-error) [menu-bar compilation-menu compilation-mode-next-error] ("Next Error" . next-error) [menu-bar compilation-menu compilation-separator2] ("----") [menu-bar compilation-menu compilation-mode-grep] ("Grep" . grep) [menu-bar compilation-menu compilation-mode-recompile] ("Recompile" . recompile) [menu-bar compilation-menu compilation-mode-compile] ("Compile" . compile)] 6) (#$ . 13476))
#@291 Major mode for compilation log buffers.
\<compilation-mode-map>To visit the source for a line-numbered error,
move point to the error message line and type \[compile-goto-error].
To kill the compilation, type \[kill-compilation].

Runs `compilation-mode-hook' with `run-hooks' (which see).
(defalias 'compilation-mode #[nil "� ��\n!���� ���!�L���!�" [kill-all-local-variables use-local-map compilation-mode-map compilation-mode major-mode "Compilation" mode-name compilation-setup make-local-variable font-lock-defaults (compilation-mode-font-lock-keywords t) run-hooks compilation-mode-hook] 2 (#$ . 14576) nil])
(defalias 'compilation-setup #[nil "���!�L���!�L���!�L���!�L�p�	�" [(":%s") mode-line-process make-local-variable compilation-error-list nil compilation-old-error-list compilation-parsing-end 1 compilation-directory-stack compilation-last-buffer] 2])
#@137 Non-nil when in compilation-minor-mode.
In this minor mode, all the error-parsing commands of the
Compilation major mode are available.
(defvar compilation-minor-mode nil (#$ . 15453))
(byte-code "��!��\n�� �\nB�\f�� �B\fB��" [make-variable-buffer-local compilation-minor-mode minor-mode-alist (compilation-minor-mode " Compilation") minor-mode-map-alist compilation-minor-mode-map] 2)
#@123 Toggle compilation minor mode.
With arg, turn compilation mode on if and only if arg is positive.
See `compilation-mode'.
(defalias 'compilation-minor-mode #[(&optional arg) "�	 	?� �!�V�� � �" [arg compilation-minor-mode prefix-numeric-value 0 compilation-setup] 2 (#$ . 15851) "P"])
#@35 Sentinel for compilation buffers.
(defalias 'compilation-sentinel #[(proc msg) "�	!�	!�>�� �\n!� �	�\"��� pǉ	\nˎ\nq��\f�7 \f	\"�= �	!Bd	`	b���@���u��� ��O���u����	!A#�	!�� �*�� 	W�� b��� \n\"�,�	!\"�!)�" [process-buffer proc buffer process-status (signal exit) buffer-name set-process-buffer nil opoint omax obuf ((set-buffer obuf)) compilation-exit-message-function msg process-exit-status status buffer-read-only 10 mode-name " " -1 " at " current-time-string 0 19 1 format ":%s [%s]" mode-line-process delete-process force-mode-line-update compilation-finish-function delq compilation-in-progress] 4 (#$ . 16147)])
#@98 Process filter for compilation buffers.
Just inserts the text, but uses `insert-before-markers'.
(defalias 'compilation-filter #[(proc string) "��\n!!�\" ��\n!q����\n!b��!��\n!`Ó+�" [buffer-name process-buffer proc nil buffer-read-only process-mark insert-before-markers string] 3 (#$ . 16818)])
(defalias 'compile-error-at-point #[nil "��`\"�\n�� `@@V� A�� )�" [compile-reinitialize-errors nil compilation-old-error-list errors] 4])
(defalias 'compilation-buffer-p #[(buffer) "�q�	� \n�=)�" [buffer compilation-minor-mode major-mode compilation-mode] 2])
(put (quote compilation-buffer-p) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@107 Move point to the next error in the compilation buffer.
Does NOT find the source line like \[next-error].
(defalias 'compilation-next-error #[(n) "�p!�\n ��!�p� �W�M �	\n	=�2 \nT\n	A�	� [\nV�A ��!�I \n\\8*�j A\f�Ή#�\f�f S\f8�i ��!)@b)�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer compile-error-at-point errors n 0 compilation-old-error-list e i "Moved back past first error" compilation-error-list compile-reinitialize-errors nil "Moved past last error"] 5 (#$ . 17488) "p"])
#@111 Move point to the previous error in the compilation buffer.
Does NOT find the source line like \[next-error].
(defalias 'compilation-previous-error #[(n) "�	[!�" [compilation-next-error n] 2 (#$ . 18043) "p"])
(defalias 'compilation-error-filedata #[(data) "A�!� �!�@�" [data markerp marker-buffer] 2])
(put (quote compilation-error-filedata) (quote byte-optimizer) (quote byte-compile-inline-expand))
(defalias 'compilation-error-filedata-file-name #[(filedata) "�	!�\n �	!�	@�" [bufferp filedata buffer-file-name] 2])
(put (quote compilation-error-filedata-file-name) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@73 Move point to the next error for a different file than the current one.
(defalias 'compilation-next-file #[(n) "�p!�\n ��!�p\f�WƉ�	�( � �# ��!�M ��`\"��!\f[�M `@@W�M A��; \f�V�� @�A�!�j �!�m @)�� 	�� ���!�� �!�� @)\"�� ��Ɖ�#��)�� ���!�� �!�� @)\"@�A�!�� �!�� @)��� A�p \fS��N �	�� ��!���Ɖ�#�)@@b+�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer n 0 nil filedata errors reversed compile-error-at-point "Moved past last error" compile-reinitialize-errors reverse compilation-old-error-list data markerp marker-buffer "%s the first erring file" bufferp buffer-file-name compilation-error-list 2 "%s is the last erring file" "This is the first erring file" 1] 6 (#$ . 18684) "p"])
#@77 Move point to the previous error for a different file than the current one.
(defalias 'compilation-previous-file #[(n) "�	[!�" [compilation-next-file n] 2 (#$ . 19499) "p"])
#@50 Kill the process made by the \[compile] command.
(defalias 'kill-compilation #[nil "� �	!� ��	!!� ��!)�" [compilation-find-buffer buffer get-buffer-process interrupt-process error "The compilation process is not running."] 3 (#$ . 19679) nil])
(defalias 'compile-reinitialize-errors #[(reparse &optional limit-search find-at-least) "�q�	�=� � � �	�+ �  V�+ ?�+ 	GY?�\\ ��!�dW�\\ 	\n�J 	GZ\"�\f	�\f\n�[ \n�))�" [compilation-last-buffer compilation-error-list t reparse compilation-forget-errors limit-search compilation-parsing-end find-at-least set-buffer-modified-p nil error-list-pos compilation-parse-errors-function compilation-old-error-list] 4])
(defalias 'compile-mouse-goto-error #[(event) "��	�	8:�  �	8)�@)!q�	�	8:�' ( �	8)�A@:�: \fA@@�= \fA@)b��p!�J ��!�p��`\"��y�\f��o `@@V�o A��] �x ��!�)�	�	8:�� � �	8)�@)!�� �� ��!�q�� ���!�" [window-buffer event 2 1 position compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer compile-reinitialize-errors nil 0 compilation-old-error-list compilation-error-list "No error to go to" select-window one-window-p other-window -1 push-mark next-error] 4 nil "e"])
#@265 Visit the source for the error message point is on.
Use this command in a compilation log buffer.  Sets the mark at point there.
\[universal-argument] as a prefix arg means to reparse the buffer's error messages first;
other kinds of prefix arguments are ignored.
(defalias 'compile-goto-error #[(&optional argp) "�p!�\n ��!�p�:`\"��y���/ `@@V�/ A�� � �; ��!�q�� ���!�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer compile-reinitialize-errors argp 0 compilation-old-error-list compilation-error-list one-window-p other-window -1 push-mark next-error 1] 4 (#$ . 20883) "P"])
(defalias 'compilation-find-buffer #[(&optional other-buffer) "� p�	q�\n� �=*� p��- �!�- �+ p=�- �� ��] @�	q�\n�D �=*�T �] @p=�] A��5 �h @�� �� p�	q�\n�y �=*�� �� ��!�p�� ��!)�" [other-buffer buffer compilation-minor-mode major-mode compilation-mode compilation-last-buffer buffer-name buffer-list buffers message "This is the only compilation buffer." error "No compilation started!"] 3])
#@855 Visit next compilation error message and corresponding source code.
This operates on the output from the \[compile] command.
If all preparsed error messages have been processed,
the error message buffer is checked for new ones.

A prefix arg specifies how many error messages to move;
negative means move back to previous error messages.
Just C-u as a prefix means reparse the error message buffer
and start at the first error.

\[next-error] normally applies to the most recent compilation started,
but as long as you are in the middle of parsing errors from one compilation
output buffer, you stay with that compilation output buffer.

Use \[next-error] in a compilation output buffer to switch to
processing errors from that compilation.

See variables `compilation-parse-errors-function' and
`compilation-error-regexp-alist' for customization ideas.
(defalias 'next-error #[(&optional argp) "� ��\f:?� �\f!\f:\"!�" [compilation-find-buffer compilation-last-buffer compilation-goto-locus compilation-next-error-locus argp prefix-numeric-value] 4 (#$ . 21938) "P"])
#@130 Visit previous compilation error message and corresponding source code.
This operates on the output from the \[compile] command.
(defalias 'previous-error #[nil "��!�" [next-error -1] 2 (#$ . 23015) nil])
#@153 Reparse the error message buffer and start at the first error
Visit corresponding source code.
This operates on the output from the \[compile] command.
(defalias 'first-error #[nil "��!�" [next-error (4)] 2 (#$ . 23227) nil])
#@886 Visit next compilation error and return locus in corresponding source code.
This operates on the output from the \[compile] command.
If all preparsed error messages have been processed,
the error message buffer is checked for new ones.

Returns a cons (ERROR . SOURCE) of two markers: ERROR is a marker at the
location of the error message in the compilation buffer, and SOURCE is a
marker at the location in the source code indicated by the error message.

Optional first arg MOVE says how many error messages to move forwards (or
backwards, if negative); default is 1.  Optional second arg REPARSE, if
non-nil, says to reparse the error message buffer and reset to the first
error (plus MOVE - 1).  If optional third argument SILENT is non-nil, return 
nil instead of raising an error if there are no more errors.

The current buffer should be the desired compilation output buffer.
(defalias 'compilation-next-error-locus #[(&optional move reparse silent) "� ���?� �W� ł S#�ĉ�ɍ�\n�? \n@AA��? \nA�\n�+ *�" [move 1 compile-reinitialize-errors reparse nil 0 next-error next-errors no-next-error (byte-code "�q�	�V� 	S��C 	S�\f=�, TA�� [V�; ��!�B \\\f�+�\n@��� 	�b 	�U�b �	�V�_ ��\"��p!�r ��p!!�=�u � ��� ���\"����p!�� ��p!!�=�� �P!��\nAA���A!��A@��@#��� \f�\n��\n@A:�� \n@A@��� \n@@Չ��\n@ա�\nA�\n�� \fAA@�A8 q���~��!��$�V�$�S!��'�y�� �� �� @A:�� @A@��� @AA@$� @A8$Z%&�=��%�W�v����%[$�������%$���%y����V���S!����y�$ @� ��+ A� �2-*�A!���A!?���\n@��J )Շ" [compilation-last-buffer move 0 compilation-error-list compilation-old-error-list e i n error "Moved back past first error" next-errors next-error 1 "Moved past last error" get-buffer-process process-status run compilation-forget-errors silent throw no-next-error nil compilation-error-message " yet" markerp fileinfo apply compilation-find-file buffer 2 column last-line errors goto-line move-to-column point-marker this lines selective-display t re-search-backward "[\n]" end re-search-forward marker-buffer] 6) compilation-error-list] 6 (#$ . 23460)])
#@205 Jump to an error locus returned by `compilation-next-error-locus'.
Takes one argument, a cons (ERROR . SOURCE) of two markers.
Selects a window with point at SOURCE, with another window displaying ERROR.
(defalias 'compilation-goto-locus #[(next-error) "�� !� � � =� ��A!!��  ��A!!�Ab�`�A!U�3 ~�Ab��	��@!�\"�F ��@!!�@\"��@\"��!*�" [window-dedicated-p selected-window frame-root-window switch-to-buffer-other-frame marker-buffer next-error switch-to-buffer marker-position t pop-up-windows get-buffer-window visible display-buffer w set-window-point set-window-start compilation-set-window-height] 3 (#$ . 25641)])
(defalias 'compilation-find-file #[(marker filename dir &rest formats) "� �\nÉ���\n!�1 ��\n!!\n�\n!B�\n!\n�v �v @�C ��m �m ��@\n\"\"�\f!�d �\f!A��K A��6 �� ���!!�\"��\"�����\n\"\n�$!�\f!�� �\n\f\"�\f!�� �\f!+-�" [formats ("%s") compilation-search-path nil name fmts thisdir buffer dirs file-name-absolute-p filename abbreviate-file-name expand-file-name file-name-directory file-name-nondirectory dir format file-exists-p find-file-noselect t pop-up-windows display-buffer marker-buffer marker w set-window-point set-window-start read-file-name "Find this error in: (default %s) " file-directory-p] 7])
(defalias 'compilation-forget-errors #[nil "�\" @�@���	A!� 	A��)A�� ��Ɖ�" [compilation-old-error-list next-error nil markerp compilation-error-list compilation-directory-stack 1 compilation-parsing-end] 4])
#@64 Return the number of \( ... \) groupings in REGEXP (a string).
(defalias 'count-regexp-groupings #[(regexp) "�	G��\fW�S 	\fH\fT�U�2 \fW�\n 	\fH�U�\n \fT�� �U�\n \fW�\n 	\fH\fT�U�\n T�\n ,�" [0 regexp nil c i len groupings 91 93 92 41] 5 (#$ . 27179)])
#@138 Parse the current buffer as grep, cc or lint error messages.
See variable `compilation-parse-errors-function' for the interface it uses.
(defalias 'compilation-parse-errors #[(limit-search find-at-least) "���!��������������	\n\fb�o�: �y��u���������#ݰ	�\f��!\\�\\��!\\�\\\n�m ��!	\nT	�� �	@A@\\�	@8\\�	@8�� �	@8\\FB�	@@!\\�\\	A�	�y �$!��$!!���\"!�����#��\f��\"��\f�\\�\f�\\�{!!+�+$�+-B-�+!�+$)/��`/Y���������\\�-0�1�i��1�\\�{!!+�+$+0�h0@+��h0A�0�V)0A�-@�0�z0$*/��`/Y�����\n����	��	@@���	A�	��	��	@	����!�Ĕ	A@�	A@�{��	8��	8�{!�	8���	8�����	8��	8�{!4567��!��6!�96P66$�	�BB6�7b�l�(ނ)�y�� 654EB�=A	@A��H=	BT*>�U>V�a/�}`/Y�}	A�}	@A@	A@A@��}	A7b��,����!�@ �A `_d�#�/�� `/Y�� ��� ��`��d.	�B !�" [nil compilation-error-list message "Parsing error messages..." 0 compilation-num-errors-found found-desired error-regexp-groups subexpr alist error-group leave-group enter-group regexp parent-expanded orig-expanded orig text-buffer compilation-parsing-end 2 -1 "\\(" compilation-enter-directory-regexp "\\)\\|" compilation-leave-directory-regexp mapconcat #[(elt) "�	@�Q�" ["\\(" elt "\\)"] 3] compilation-error-regexp-alist "\\|" "\\)" 1 count-regexp-groupings error "compilation-error-regexp-alist is empty!" 3 abbreviate-file-name default-directory file-truename expand-file-name "../" re-search-forward t file-name-as-directory dir compile-abbreviate-directory compilation-directory-stack file-directory-p limit-search stack beg "compilation-parse-errors: impossible regexp match!" string-to-int column linenum filename beginning-of-match boundp comint-file-name-prefix file-name-absolute-p 4 point-marker this find-at-least "compilation-parse-errors: known groups didn't match!" "Parsing error messages...%d (%.0f%% of buffer)" 100.0 "Parsing error messages...done"] 14 (#$ . 27461)])
(defalias 'compile-abbreviate-directory #[(dir orig orig-expanded parent-expanded) "�	!��!� 	P	G\fGV�) \f	�\fGO��) 	\fG�OP	GGV�L 	�GO��L ��!!	G�OP	�" [abbreviate-file-name dir boundp comint-file-name-prefix orig-expanded 0 orig nil parent-expanded file-name-directory directory-file-name] 4])
(provide (quote compile))

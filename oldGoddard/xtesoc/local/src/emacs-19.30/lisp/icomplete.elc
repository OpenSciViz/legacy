;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Mon Oct 30 13:21:31 1995
;;; from file /home/fsf/rms/e19/lisp/icomplete.el
;;; emacs version 19.29.1.2.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`icomplete.el' was compiled for Emacs 19.29 or later"))


(provide (quote icomplete))
#@531 *Icomplete-specific customization of minibuffer setup.

This hook is run during minibuffer setup iff icomplete will be active.
It is intended for use in customizing icomplete for interoperation
with other packages.  For instance:

  (add-hook 'icomplete-minibuffer-setup-hook 
	    (function
	     (lambda ()
	       (make-local-variable 'resize-minibuffer-window-max-height)
	       (setq resize-minibuffer-window-max-height 3))))

will constrain rsz-mini to a maximum minibuffer height of 3 lines when
icompletion is occurring.
(defvar icomplete-minibuffer-setup-hook nil (#$ . -507))
#@105 Non-nil enables incremental minibuffer completion, once
`\[icomplete-mode]' function has set things up.
(defvar icomplete-mode t (#$ . 1101))
#@63 Point where minibuffer input ends and completion info begins.
(defvar icomplete-eoinput 1 (#$ . 1249))
(make-variable-buffer-local (quote icomplete-eoinput))
#@218 Incremental-minibuffer-completion pre-command-hook.

Is run in minibuffer before user input when `icomplete-mode' is non-nil.
Use `icomplete-mode' function to set it up properly for incremental
minibuffer completion.
(defvar icomplete-pre-command-hook nil (#$ . 1414))
(add-hook (quote icomplete-pre-command-hook) (quote icomplete-tidy))
#@218 Incremental-minibuffer-completion post-command-hook.

Is run in minibuffer after user input when `icomplete-mode' is non-nil.
Use `icomplete-mode' function to set it up properly for incremental
minibuffer completion.
(defvar icomplete-post-command-hook nil (#$ . 1759))
(add-hook (quote icomplete-post-command-hook) (quote icomplete-exhibit))
#@108 Activate incremental minibuffer completion for this emacs session,
or deactivate with negative prefix arg.
(defalias 'icomplete-mode #[(&optional prefix) "� ��Y� ����\"�ǉ�" [prefix 0 t icomplete-mode add-hook minibuffer-setup-hook icomplete-minibuffer-setup nil] 3 (#$ . 2109) "p"])
#@364 Non-nil if current window is minibuffer that's doing simple completion.

Conditions are:
   the selected window is a minibuffer,
   and not in the middle of macro execution,
   and minibuffer-completion-table is not a symbol (which would
       indicate some non-standard, non-simple completion mechansm,
       like file-name and other custom-func completions).
(defalias 'icomplete-simple-completing-p #[nil "�� !� \n?� 9?�" [window-minibuffer-p selected-window executing-macro minibuffer-completion-table] 2 (#$ . 2407)])
#@123 Run in minibuffer on activation to establish incremental completion.
Usually run by inclusion in `minibuffer-setup-hook'.
(defalias 'icomplete-minibuffer-setup #[nil "�\" � �\" ��!������$���!������$���!�" [icomplete-mode icomplete-simple-completing-p make-local-hook pre-command-hook add-hook #[nil "��!�" [run-hooks icomplete-pre-command-hook] 2] nil t post-command-hook #[nil "��!�" [run-hooks icomplete-post-command-hook] 2] run-hooks icomplete-minibuffer-setup-hook] 5 (#$ . 2942)])
#@168 Remove completions display (if any) prior to new user input.
Should be run in on the minibuffer `pre-command-hook'.  See `icomplete-mode'
and `minibuffer-setup-hook'.
(defalias 'icomplete-tidy #[nil "� �' ��!�  \n�  \ndV� d��\nd|)���!�ŉ�" [icomplete-simple-completing-p boundp icomplete-eoinput buffer-undo-list make-local-variable 1] 2 (#$ . 3437)])
#@140 Insert icomplete completions display.
Should be run via minibuffer `post-command-hook'.  See `icomplete-mode'
and `minibuffer-setup-hook'.
(defalias 'icomplete-exhibit #[nil "� �. ed{��db���!� ��!�`d�V�- ��\n\f?$!+�" [icomplete-simple-completing-p t buffer-undo-list contents boundp icomplete-eoinput make-local-variable 1 insert-string icomplete-completions minibuffer-completion-table minibuffer-completion-predicate minibuffer-completion-confirm] 6 (#$ . 3801)])
#@699 Identify prospective candidates for minibuffer completion.

The display is updated with each minibuffer keystroke during
minibuffer completion.

Prospective completion suffixes (if any) are displayed, bracketed by
one of (), [], or {} pairs.  The choice of brackets is as follows:

  (...) - a single prospect is identified and matching is enforced,
  [...] - a single prospect is identified but matching is optional, or
  {...} - multiple prospects, separated by commas, are indicated, and
          further input is required to distingish a single one.

The displays for disambiguous matches have ` [Matched]' appended
(whether complete or not), or ` [No matches]', if no eligible
matches exist.
(defalias 'icomplete-completions #[(name candidates predicate require-match) "�	\n#\f� ł �\f� ǂ ���\f��1 ��#�� A�V @G	GV�P @	G�OQ�Q ��P�� �	\n#�G�������\"\"A\"	GV�� 	G�OQ\f�� �P�� R,-�" [all-completions name candidates predicate require-match "(" "[" ")" "]" "{" "}" close-bracket-prospects open-bracket-prospects close-bracket-determined open-bracket-determined comps format " %sNo matches%s" nil "" " [Matched]" try-completion most most-len most-is-exact apply concat nconc mapcar #[(com) "G	U� �ć�	�OD�" [com most-len t most-is-exact nil ","] 4] alternatives ","] 8 (#$ . 4283)])
(byte-code "� � ���" [icomplete-mode] 1)

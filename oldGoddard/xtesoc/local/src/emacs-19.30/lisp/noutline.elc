;ELC   
;;; compiled by roland@churchy.gnu.ai.mit.edu on Wed Oct 25 12:58:28 1995
;;; from file /gd/gnu/emacs/19.0/lisp/noutline.el
;;; emacs version 19.29.1.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`noutline.el' was compiled for Emacs 19.29 or later"))


#@266 *Regular expression to match the beginning of a heading.
Any line whose beginning matches this regexp is considered to start a heading.
The recommended way to set this is with a Local Variables: list
in the file it applies to.  See also outline-heading-end-regexp.
(defvar outline-regexp nil (#$ . -484))
(byte-code "��!� ���\"���" [default-value outline-regexp set-default "[*\f]+"] 3)
#@286 *Regular expression to match the end of a heading line.
You can assume that point is at the beginning of a heading when this
regexp is searched for.  The heading ends at the end of the match.
The recommended way to set this is with a `Local Variables:' list
in the file it applies to.
(defvar outline-heading-end-regexp "\n" (#$ . -879))
(byte-code "��!� ��B	�s � �	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#���!�| �&�B&�6� &�&����!B#��&��#��&��#��&��#��&��#��&��#��&��#��&����!B#��&��#��&��#��&��#��&��#��&��@ #��&�A �B āB !B#��&�C �D #��&�E �F #��&�G �H #��&�I �J #��&�K �L #�" [boundp outline-mode-prefix-map nil current-load-list make-sparse-keymap define-key "" outline-next-visible-heading "" outline-previous-visible-heading "	" show-children "" show-subtree "" hide-subtree "" outline-up-heading "" outline-forward-same-level "" outline-backward-same-level "" hide-body "" show-all "" hide-entry "" show-entry "\f" hide-leaves "" show-branches "" hide-sublevels "" hide-other outline-mode-menu-bar-map [hide] "Hide" [hide hide-other] ("Hide Other" . hide-other) [hide hide-sublevels] ("Hide Sublevels" . hide-sublevels) [hide hide-subtree] ("Hide Subtree" . hide-subtree) [hide hide-entry] ("Hide Entry" . hide-entry) [hide hide-body] ("Hide Body" . hide-body) [hide hide-leaves] ("Hide Leaves" . hide-leaves) [show] "Show" [show show-subtree] ("Show Subtree" . show-subtree) [show show-children] ("Show Children" . show-children) [show show-branches] ("Show Branches" . show-branches) [show show-entry] ("Show Entry" . show-entry) [show show-all] ("Show All" . show-all) [headings] "Headings" [headings outline-backward-same-level] ("Previous Same Level" . outline-backward-same-level) [headings outline-forward-same-level] ("Next Same Level" . outline-forward-same-level) [headings outline-previous-visible-heading] ("Previous" . outline-previous-visible-heading) [headings outline-next-visible-heading] ("Next" . outline-next-visible-heading) [headings outline-up-heading] ("Up" . outline-up-heading)] 6)
#@2 
(defvar outline-mode-map nil (#$ . 2998))
(byte-code "� � \n���#���#���" [outline-mode-map make-sparse-keymap text-mode-map define-key "" outline-mode-prefix-map [menu-bar] outline-mode-menu-bar-map] 4)
#@67 Non-nil if using Outline mode as a minor mode of some other mode.
(defvar outline-minor-mode nil (#$ . 3217))
(byte-code "��!�����#���� ��C\"��" [make-variable-buffer-local outline-minor-mode put permanent-local t minor-mode-alist append (outline-minor-mode " Outl")] 4)
#@54 Additional expressions to highlight in Outline mode.
(defvar outline-font-lock-keywords (quote (("^\\(\\*+\\)[ 	]*\\(.+\\)?[ 	]*$" (1 font-lock-string-face) (2 (let ((len (- (match-end 1) (match-beginning 1)))) (or (cdr (assq len (quote ((1 . font-lock-function-name-face) (2 . font-lock-keyword-face) (3 . font-lock-comment-face))))) font-lock-variable-name-face)) nil t)) ("\\[\\([A-Z][A-Za-z]+\\)*[0-9]+\\]" . font-lock-type-face))) (#$ . 3498))
#@57 Normal hook to be run after outline visibility changes.
(defvar outline-view-change-hook nil (#$ . 3953))
#@2063 Set major mode for editing outlines with selective display.
Headings are lines which start with asterisks: one for major headings,
two for subheadings, etc.  Lines not starting with asterisks are body lines. 

Body text or subheadings under a heading can be made temporarily
invisible, or visible again.  Invisible lines are attached to the end 
of the heading, so they move with it, if the line is killed and yanked
back.  A heading with text hidden under it is marked with an ellipsis (...).

Commands:\<outline-mode-map>
\[outline-next-visible-heading]   outline-next-visible-heading      move by visible headings
\[outline-previous-visible-heading]   outline-previous-visible-heading
\[outline-forward-same-level]   outline-forward-same-level        similar but skip subheadings
\[outline-backward-same-level]   outline-backward-same-level
\[outline-up-heading]   outline-up-heading		    move from subheading to heading

\[hide-body]	make all text invisible (not headings).
\[show-all]	make everything in buffer visible.

The remaining commands are used when point is on a heading line.
They apply to some of the body or subheadings of that heading.
\[hide-subtree]   hide-subtree	make body and subheadings invisible.
\[show-subtree]   show-subtree	make body and subheadings visible.
\[show-children]   show-children	make direct subheadings visible.
		 No effect on body, or subheadings 2 or more levels down.
		 With arg N, affects subheadings N levels down.
\[hide-entry]	   make immediately following body invisible.
\[show-entry]	   make it visible.
\[hide-leaves]	   make body under heading and under its subheadings invisible.
		     The subheadings remain visible.
\[show-branches]  make all subheadings at all levels visible.

The variable `outline-regexp' can be changed to control what is a heading.
A line is a heading if `outline-regexp' matches something at the
beginning of the line.  The longer the match, the deeper the level.

Turning on outline mode calls the value of `text-mode-hook' and then of
`outline-mode-hook', if they are non-nil.
(defalias 'outline-mode #[nil "� ��\n!������\"�\n�\f!���!�����!���R��!���!���R��!����!����\"����\"�" [kill-all-local-variables use-local-map outline-mode-map "Outline" mode-name outline-mode major-mode define-abbrev-table text-mode-abbrev-table nil local-abbrev-table set-syntax-table text-mode-syntax-table make-local-variable line-move-ignore-invisible t ((t . t)) buffer-invisibility-spec paragraph-start "\\|\\(" outline-regexp "\\)" auto-fill-inhibit-regexp paragraph-separate font-lock-defaults (outline-font-lock-keywords t) change-major-mode-hook add-hook show-all run-hooks text-mode-hook outline-mode-hook] 4 (#$ . 4067) nil])
#@202 *Prefix key to use for Outline commands in Outline minor mode.
The value of this variable is checked as part of loading Outline mode.
After that, changing the prefix key requires manipulating keymaps.
(defvar outline-minor-mode-prefix "@" (#$ . -6802))
(byte-code "��!� ��B	�\" � �	�#��		#����1 �	BB" [boundp outline-minor-mode-map nil current-load-list make-sparse-keymap define-key [menu-bar] outline-mode-menu-bar-map outline-minor-mode-prefix outline-mode-prefix-map outline-minor-mode minor-mode-map-alist] 4)
#@166 Toggle Outline minor mode.
With arg, turn Outline minor mode on if arg is positive, off otherwise.
See the command `outline-mode' for more information on this mode.
(defalias 'outline-minor-mode #[(&optional arg) "�	 	?� �!�V��# ��!�����!��( ��	�/ � �� �" [arg outline-minor-mode prefix-numeric-value 0 make-local-variable line-move-ignore-invisible t ((t . t)) buffer-invisibility-spec run-hooks outline-minor-mode-hook nil show-all force-mode-line-update] 3 (#$ . 7343) "P"])
#@130 Function of no args to compute a header's nesting level in an outline.
It can assume point is at the beginning of a header line.
(defvar outline-level (quote outline-level) (#$ . 7838))
#@188 Return the depth to which a statement is nested in the outline.
Point must be at the beginning of a header line.  This is actually
the number of characters that `outline-regexp' matches.
(defalias 'outline-level #[nil "��	!�Z)�" [looking-at outline-regexp 0] 2 (#$ . 8031)])
#@140 Skip forward to just before the next heading line.
If there's no following heading line, stop before the newline
at the end of the buffer.
(defalias 'outline-next-preface #[nil "��\n�Q��#� Ɣb�n� �u�" [re-search-forward "\n\\(" outline-regexp "\\)" nil move 0 -1] 4 (#$ . 8317)])
#@53 Move to the next (possibly invisible) heading line.
(defalias 'outline-next-heading #[nil "��\n�Q��#� ƔTb�" [re-search-forward "\n\\(" outline-regexp "\\)" nil move 0] 4 (#$ . 8605) nil])
#@50 Non-nil if the character after point is visible.
(defalias 'outline-visible #[nil "�`�\"?�" [get-char-property invisible] 3 (#$ . 8801)])
(put (quote outline-visible) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@114 Move to previous heading line, or beg of this line if it's a heading.
Only visible heading lines are considered.
(defalias 'outline-back-to-heading #[nil "�y�� �( ����Q��#� �`�\"?��\n )�( ��!�" [0 outline-on-heading-p nil found re-search-backward "^\\(" outline-regexp "\\)" t get-char-property invisible error "before first heading"] 5 (#$ . 9034)])
#@51 Return t if point is on a (visible) heading line.
(defalias 'outline-on-heading-p #[nil "��y�n� �`�\"?� �\f!)�" [0 get-char-property invisible looking-at outline-regexp] 3 (#$ . 9397)])
(defalias 'outline-end-of-heading #[nil "�	��#�\n �u�" [re-search-forward outline-heading-end-regexp nil move -1] 4])
#@178 Move to the next visible heading line.
With argument, repeats or can move backward if negative.
A heading line is one that starts with a `*' (or that
`outline-regexp' matches).
(defalias 'outline-next-visible-heading #[(arg) "�W�\f �y�� ��o�5 �W�5 o�/ ���Q��#�/ �`�\"� T� m�[ �V�[ m�U ���Q��#�U �`�\"�? S�5 �y�" [arg 0 nil re-search-backward "^\\(" outline-regexp "\\)" move get-char-property invisible re-search-forward] 4 (#$ . 9710) "p"])
#@173 Move to the previous heading line.
With argument, repeats or can move forward if negative.
A heading line is one that starts with a `*' (or that
`outline-regexp' matches).
(defalias 'outline-previous-visible-heading #[(arg) "�	[!�" [outline-next-visible-heading arg] 2 (#$ . 10174) "p"])
#@132 Hides or shows lines from FROM to TO, according to FLAG.
If FLAG is nil then text is shown, while if FLAG is t the text is hidden.
(defalias 'outline-flag-region #[(from to flag) "��\nb����`�#��* �`\"	�	�#��	��#�)*��!�" [t inhibit-read-only from nil outline-discard-overlays to outline flag make-overlay o overlay-put invisible run-hooks outline-view-change-hook] 4 (#$ . 10469)])
(defalias 'outline-discard-overlays #[(beg end prop) "	W�\n 	�	b�`W�z �`!��q @�\f\"�i �\f!	W�R �\f!V�i �\f!\n�\n�\n!	#��\f�\f!	#�)�i �\f!V�e �\f�\f!#��i �\f!�)A�� )�`!b�� )�" [end beg overlays-at overlays o overlay-get prop overlay-start overlay-end outline-copy-overlay o1 move-overlay delete-overlay next-overlay-change] 6])
(defalias 'outline-copy-overlay #[(o) "��\n!�\n!�\n!#�\n!�- �@A@#�AA�� *�" [make-overlay overlay-start o overlay-end overlay-buffer overlay-properties props o1 overlay-put] 6])
#@48 Hide the body directly following this heading.
(defalias 'hide-entry #[nil "� �� ���`� �`�#)�" [outline-back-to-heading outline-end-of-heading outline-flag-region outline-next-preface t] 4 (#$ . 11410) nil])
#@48 Show the body directly following this heading.
(defalias 'show-entry #[nil "��`� �`�#)�" [outline-flag-region outline-next-preface nil] 4 (#$ . 11624) nil])
#@37 Hide all of buffer except headings.
(defalias 'hide-body #[nil "�ed\"�" [hide-region-body] 3 (#$ . 11787) nil])
#@54 Hide all body lines in the region, but not headings.
(defalias 'hide-region-body #[(start end) "��	}�eb�� � � �m?�6 �`� �`�#�m� ��!�- ɂ. �u�� �� *�" [start end outline-on-heading-p outline-end-of-heading outline-flag-region outline-next-preface t looking-at "\n\n" 2 1] 4 (#$ . 11905)])
#@37 Show all of the text in the buffer.
(defalias 'show-all #[nil "�ed�#�" [outline-flag-region nil] 4 (#$ . 12203) nil])
#@54 Hide everything after this heading at deeper levels.
(defalias 'hide-subtree #[nil "��!�" [outline-flag-subtree t] 2 (#$ . 12327) nil])
#@52 Hide all body after this heading at deeper levels.
(defalias 'hide-leaves #[nil "� �� ��`� �`\"�" [outline-back-to-heading outline-end-of-heading hide-region-body outline-end-of-subtree] 3 (#$ . 12469) nil])
#@54 Show everything after this heading at deeper levels.
(defalias 'show-subtree #[nil "��!�" [outline-flag-subtree nil] 2 (#$ . 12683) nil])
#@72 Hide everything but the top LEVELS levels of headers, in whole buffer.
(defalias 'hide-sublevels #[(levels) "�W�\n ��!�S�eb�o� � � � �@ �� �`)�`�#��V�8 �!�b�)� )�" [levels 1 error "Must keep at least one level of headers" outline-on-heading-p outline-next-heading outline-end-of-subtree end outline-flag-region t 0 show-children] 4 (#$ . 12827) "p"])
#@70 Hide everything except for the current body and the parent headings.
(defalias 'hide-other #[nil "��!�``��� �`�\"??)�7 ��y�`=�. � ��`�#��3 � �`)� *�" [hide-sublevels 1 pos last 0 get-char-property invisible outline-next-heading outline-flag-region nil show-children] 4 (#$ . 13198) nil])
(defalias 'outline-flag-subtree #[(flag) "�� �� ��`� �`\f#)�" [outline-back-to-heading outline-end-of-heading outline-flag-region outline-end-of-subtree flag] 4])
(defalias 'outline-end-of-subtree #[nil "� �`�\n m�! \f� \n V�! �� ��\n n�. �u�n�. �u+�" [outline-back-to-heading t outline-level level first opoint nil outline-next-heading -1] 3])
#@61 Show all subheadings of this heading, but not their bodies.
(defalias 'show-branches #[nil "��!�" [show-children 1000] 2 (#$ . 13851) nil])
#@183 Show all direct subheadings of this heading.
Prefix arg LEVEL is how many levels below the current level should be shown.
Default is enough to cause the following heading to appear.
(defalias 'show-children #[(&optional level) "�\n �!�# �� � � �m� Ƃ\" � \fZ]*��� � \\`� �m�: d�< `T}�eb�m?�o � �m?�o  X�A �Ȋ�u�n�b �u�`)� �`�#�)�A *�" [level prefix-numeric-value outline-back-to-heading outline-level start-level outline-next-heading 1 outline-end-of-subtree outline-flag-region -1 outline-end-of-heading nil] 4 (#$ . 13998) "P"])
#@104 Move to the heading line of which the present line is a subheading.
With argument, move up ARG levels.
(defalias 'outline-up-heading #[(arg) "� �	 �=� ��!�	 �V�; �V�; o?�; 	 	 W�3 ��!��$ �Z)� �" [outline-back-to-heading outline-level 1 error "" arg 0 present-level outline-previous-visible-heading] 2 (#$ . 14548) "p"])
#@128 Move forward to the ARG'th subheading at same level as this one.
Stop at the first and last subheadings of a superior heading.
(defalias 'outline-forward-same-level #[(arg) "� �	�V�% �� )�� \fb�	S�! ���!�)� �" [outline-back-to-heading arg 0 outline-get-next-sibling point-to-move-to error ""] 3 (#$ . 14884) "p"])
#@74 Move to next heading of the same level, and return point or nil if none.
(defalias 'outline-get-next-sibling #[nil " ��!� 	V� m� ��!��  	W?�\" `)�" [outline-level level outline-next-visible-heading 1] 2 (#$ . 15209)])
#@129 Move backward to the ARG'th subheading at same level as this one.
Stop at the first and last subheadings of a superior heading.
(defalias 'outline-backward-same-level #[(arg) "� �	�V�% �� )�� \fb�	S�! ���!�)� �" [outline-back-to-heading arg 0 outline-get-last-sibling point-to-move-to error ""] 3 (#$ . 15441) "p"])
#@74 Move to next heading of the same level, and return point or nil if none.
(defalias 'outline-get-last-sibling #[nil " ��!� 	V� o� ��!��  	W?�\" `)�" [outline-level level outline-previous-visible-heading 1] 2 (#$ . 15768)])
(byte-code "��!���!�" [provide outline noutline] 2)

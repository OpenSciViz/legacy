;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Sat Nov 11 00:05:58 1995
;;; from file /home/fsf/rms/e19/lisp/pc-select.el
;;; emacs version 19.29.1.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`pc-select.el' was compiled for Emacs 19.29 or later"))


(provide (quote pc-select))
#@257 Save the region as if killed; but don't kill it; deactivate mark.
If `interprogram-cut-function' is non-nil, also save the text for a window
system cut and paste.

Deactivating mark is to avoid confusion with delete-selection-mode
and transient-mark-mode.
(defalias 'copy-region-as-kill-nomark #[(beg end) "�	\n\"����!�" [copy-region-as-kill beg end nil mark-active message "Region saved"] 3 (#$ . 507) "r"])
(defalias 'ensure-mark #[nil "?� ��!�" [mark-active set-mark-command nil] 2])
#@130 Ensure mark is active; move point right ARG characters (left if ARG negative).
On reaching end of buffer, stop and signal error.
(defalias 'forward-char-mark #[(&optional arg) "� �	u�" [ensure-mark arg] 1 (#$ . 1004) "p"])
#@180 Ensure mark is active; move point right ARG words (backward if ARG is negative).
Normally returns t.
If an edge of the buffer is reached, point is left there
and nil is returned.
(defalias 'forward-word-mark #[(&optional arg) "� �	v�" [ensure-mark arg] 1 (#$ . 1234) "p"])
#@433 Ensure mark is active; move forward to end of paragraph.
With arg N, do it N times; negative arg -N means move backward N paragraphs.

A line which `paragraph-start' matches either separates paragraphs
(if `paragraph-separate' matches it also) or is the first line of a paragraph.
A paragraph end is the beginning of a line which is not part of the paragraph
to which the end of the previous line belongs, or the end of the buffer.
(defalias 'forward-paragraph-mark #[(&optional arg) "� ��\n!�" [ensure-mark forward-paragraph arg] 2 (#$ . 1514) "p"])
#@826 Ensure mark is active; move cursor vertically down ARG lines.
If there is no character in the target line exactly under the current column,
the cursor is positioned after the character in that line which spans this
column, or at the end of the line if it is not long enough.
If there is no line in the buffer after this one, behavior depends on the
value of `next-line-add-newlines'.  If non-nil, it inserts a newline character
to create a line, and moves the cursor to that line.  Otherwise it moves the
cursor to the end of the buffer (if already at the end of the buffer, an error
is signaled).

The command C-x C-n can be used to create
a semipermanent goal column to which this command always moves.
Then it does not try to move vertically.  This goal column is stored
in `goal-column', which is nil when there is none.
(defalias 'next-line-mark #[(&optional arg) "� ��\n!�" [ensure-mark next-line arg] 2 (#$ . 2072) "p"])
#@182 Ensure mark is active; move point to end of current line.
With argument ARG not nil or 1, move forward ARG - 1 lines first.
If scan reaches end of buffer, stop there without error.
(defalias 'end-of-line-mark #[(&optional arg) "� �	�" [ensure-mark arg] 1 (#$ . 3007) "p"])
#@252 Ensure mark is active; scroll down ARG lines; or near full screen if no ARG.
A near full screen is `next-screen-context-lines' less than a full screen.
Negative ARG means scroll upward.
When calling from a program, supply a number as argument or nil.
(defalias 'scroll-down-mark #[(&optional arg) "� ��\n!�" [ensure-mark scroll-down arg] 2 (#$ . 3288) "P"])
#@328 Ensure mark is active; move point to the end of the buffer.
With arg N, put point N/10 of the way from the end.

If the buffer is narrowed, this command uses the beginning and size
of the accessible part of the buffer.

Don't use this command in Lisp programs!
(goto-char (point-max)) is faster and avoids clobbering the mark.
(defalias 'end-of-buffer-mark #[(&optional arg) "� �deZ\n�' d	�V� �\n!	ť_�# 	�\n!_ťZ�( db�)\n�2 �y�`�� b��� !�`W*�N �`!���!�" [ensure-mark size arg 10000 prefix-numeric-value 10 1 old-point window-start vertical-motion window-height overlay-recenter recenter -3] 4 (#$ . 3653) "P"])
#@124 Deactivate mark; move point right ARG characters (left if ARG negative).
On reaching end of buffer, stop and signal error.
(defalias 'forward-char-nomark #[(&optional arg) "�\nu�" [nil mark-active arg] 1 (#$ . 4278) "p"])
#@174 Deactivate mark; move point right ARG words (backward if ARG is negative).
Normally returns t.
If an edge of the buffer is reached, point is left there
and nil is returned.
(defalias 'forward-word-nomark #[(&optional arg) "�\nv�" [nil mark-active arg] 1 (#$ . 4508) "p"])
#@427 Deactivate mark; move forward to end of paragraph.
With arg N, do it N times; negative arg -N means move backward N paragraphs.

A line which `paragraph-start' matches either separates paragraphs
(if `paragraph-separate' matches it also) or is the first line of a paragraph.
A paragraph end is the beginning of a line which is not part of the paragraph
to which the end of the previous line belongs, or the end of the buffer.
(defalias 'forward-paragraph-nomark #[(&optional arg) "��!�" [nil mark-active forward-paragraph arg] 2 (#$ . 4788) "p"])
#@820 Deactivate mark; move cursor vertically down ARG lines.
If there is no character in the target line exactly under the current column,
the cursor is positioned after the character in that line which spans this
column, or at the end of the line if it is not long enough.
If there is no line in the buffer after this one, behavior depends on the
value of `next-line-add-newlines'.  If non-nil, it inserts a newline character
to create a line, and moves the cursor to that line.  Otherwise it moves the
cursor to the end of the buffer (if already at the end of the buffer, an error
is signaled).

The command C-x C-n can be used to create
a semipermanent goal column to which this command always moves.
Then it does not try to move vertically.  This goal column is stored
in `goal-column', which is nil when there is none.
(defalias 'next-line-nomark #[(&optional arg) "��!�" [nil mark-active next-line arg] 2 (#$ . 5344) "p"])
#@176 Deactivate mark; move point to end of current line.
With argument ARG not nil or 1, move forward ARG - 1 lines first.
If scan reaches end of buffer, stop there without error.
(defalias 'end-of-line-nomark #[(&optional arg) "�\n�" [nil mark-active arg] 1 (#$ . 6277) "p"])
#@246 Deactivate mark; scroll down ARG lines; or near full screen if no ARG.
A near full screen is `next-screen-context-lines' less than a full screen.
Negative ARG means scroll upward.
When calling from a program, supply a number as argument or nil.
(defalias 'scroll-down-nomark #[(&optional arg) "��!�" [nil mark-active scroll-down arg] 2 (#$ . 6558) "P"])
#@322 Deactivate mark; move point to the end of the buffer.
With arg N, put point N/10 of the way from the end.

If the buffer is narrowed, this command uses the beginning and size
of the accessible part of the buffer.

Don't use this command in Lisp programs!
(goto-char (point-max)) is faster and avoids clobbering the mark.
(defalias 'end-of-buffer-nomark #[(&optional arg) "�deZ�& d\n�V� �!\nƥ_�\" \n�!_ƥZ�' db�)�1 �y�`�� b��� !�`W*�M �`!���!�" [nil mark-active size arg 10000 prefix-numeric-value 10 1 old-point window-start vertical-motion window-height overlay-recenter recenter -3] 4 (#$ . 6921) "P"])
#@150 Ensure mark is active; move point left ARG characters (right if ARG negative).
On attempt to pass beginning or end of buffer, stop and signal error.
(defalias 'backward-char-mark #[(&optional arg) "� ��\n!�" [ensure-mark backward-char arg] 2 (#$ . 7545) "p"])
#@116 Ensure mark is active; move backward until encountering the end of a word.
With argument, do this that many times.
(defalias 'backward-word-mark #[(&optional arg) "� ��\n!�" [ensure-mark backward-word arg] 2 (#$ . 7812) "p"])
#@450 Ensure mark is active; move backward to start of paragraph.
With arg N, do it N times; negative arg -N means move forward N paragraphs.

A paragraph start is the beginning of a line which is a
`first-line-of-paragraph' or which is ordinary text and follows a
paragraph-separating line; except: if the first real line of a
paragraph is preceded by a blank line, the paragraph starts at that
blank line.

See `forward-paragraph' for more information.
(defalias 'backward-paragraph-mark #[(&optional arg) "� ��\n!�" [ensure-mark backward-paragraph arg] 2 (#$ . 8045) "p"])
#@624 Ensure mark is active; move cursor vertically up ARG lines.
If there is no character in the target line exactly over the current column,
the cursor is positioned after the character in that line which spans this
column, or at the end of the line if it is not long enough.

The command C-x C-n can be used to create
a semipermanent goal column to which this command always moves.
Then it does not try to move vertically.

If you are thinking of using this in a Lisp program, consider using
`forward-line' with a negative argument instead.  It is usually easier
to use and more reliable (no dependence on goal column, etc.).
(defalias 'previous-line-mark #[(&optional arg) "� ��\n!�" [ensure-mark previous-line arg] 2 (#$ . 8622) "p"])
#@188 Ensure mark is active; move point to beginning of current line.
With argument ARG not nil or 1, move forward ARG - 1 lines first.
If scan reaches end of buffer, stop there without error.
(defalias 'beginning-of-line-mark #[(&optional arg) "� ��\n!�" [ensure-mark beginning-of-line arg] 2 (#$ . 9363) "p"])
#@256 Ensure mark is active; scroll upward ARG lines; or near full screen if no ARG.
A near full screen is `next-screen-context-lines' less than a full screen.
Negative ARG means scroll downward.
When calling from a program, supply a number as argument or nil.
(defalias 'scroll-up-mark #[(&optional arg) "� ��\n!�" [ensure-mark scroll-up arg] 2 (#$ . 9676) "P"])
#@340 Ensure mark is active; move point to the beginning of the buffer.
With arg N, put point N/10 of the way from the beginning.

If the buffer is narrowed, this command uses the beginning and size
of the accessible part of the buffer.

Don't use this command in Lisp programs!
(goto-char (point-min)) is faster and avoids clobbering the mark.
(defalias 'beginning-of-buffer-mark #[(&optional arg) "� �deZ\n�) e	�V� �\n!	ť_�% 	�\n!_�\\ť\\�* eb�)\n�3 �y�" [ensure-mark size arg 10000 prefix-numeric-value 10 1] 4 (#$ . 10041) "P"])
#@144 Deactivate mark; move point left ARG characters (right if ARG negative).
On attempt to pass beginning or end of buffer, stop and signal error.
(defalias 'backward-char-nomark #[(&optional arg) "��!�" [nil mark-active backward-char arg] 2 (#$ . 10578) "p"])
#@110 Deactivate mark; move backward until encountering the end of a word.
With argument, do this that many times.
(defalias 'backward-word-nomark #[(&optional arg) "��!�" [nil mark-active backward-word arg] 2 (#$ . 10844) "p"])
#@444 Deactivate mark; move backward to start of paragraph.
With arg N, do it N times; negative arg -N means move forward N paragraphs.

A paragraph start is the beginning of a line which is a
`first-line-of-paragraph' or which is ordinary text and follows a
paragraph-separating line; except: if the first real line of a
paragraph is preceded by a blank line, the paragraph starts at that
blank line.

See `forward-paragraph' for more information.
(defalias 'backward-paragraph-nomark #[(&optional arg) "��!�" [nil mark-active backward-paragraph arg] 2 (#$ . 11076) "p"])
#@415 Deactivate mark; move cursor vertically up ARG lines.
If there is no character in the target line exactly over the current column,
the cursor is positioned after the character in that line which spans this
column, or at the end of the line if it is not long enough.

The command C-x C-n can be used to create
a semipermanent goal column to which this command always moves.
Then it does not try to move vertically.
(defalias 'previous-line-nomark #[(&optional arg) "��!�" [nil mark-active previous-line arg] 2 (#$ . 11652) "p"])
#@182 Deactivate mark; move point to beginning of current line.
With argument ARG not nil or 1, move forward ARG - 1 lines first.
If scan reaches end of buffer, stop there without error.
(defalias 'beginning-of-line-nomark #[(&optional arg) "��!�" [nil mark-active beginning-of-line arg] 2 (#$ . 12189) "p"])
#@250 Deactivate mark; scroll upward ARG lines; or near full screen if no ARG.
A near full screen is `next-screen-context-lines' less than a full screen.
Negative ARG means scroll downward.
When calling from a program, supply a number as argument or nil.
(defalias 'scroll-up-nomark #[(&optional arg) "��!�" [nil mark-active scroll-up arg] 2 (#$ . 12501) "P"])
#@334 Deactivate mark; move point to the beginning of the buffer.
With arg N, put point N/10 of the way from the beginning.

If the buffer is narrowed, this command uses the beginning and size
of the accessible part of the buffer.

Don't use this command in Lisp programs!
(goto-char (point-min)) is faster and avoids clobbering the mark.
(defalias 'beginning-of-buffer-nomark #[(&optional arg) "�deZ�( e\n�V� �!\nƥ_�$ \n�!_�\\ƥ\\�) eb�)�2 �y�" [nil mark-active size arg 10000 prefix-numeric-value 10 1] 4 (#$ . 12865) "P"])
#@429 Change mark behaviour to emulate motif, MAC or MS-Windows cut and paste style.

This mode will switch on delete-selection-mode and
transient-mark-mode.

The cursor keys (and others) are bound to new functions
which will modify the status of the mark. It will be
possible to select regions with shift-cursorkeys. All this
tries to emulate the look-and-feel of GUIs like motif,
the MAC GUI or MS-Windows (sorry for the last one).
(defalias 'pc-selection-mode #[nil "�	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#����\"����\"��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#����\"����\"��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#����\"�ԁ@ �A \"�ԁB �C \"�ԁD �E \"�ԁF �G \"�ԁH �I \"�ԁJ �K \"�ԁL �M \"�ԁN �O \"��P�Q �R #�ԁS �T \"�ԁU �V \"�ԁW �X \"��Y Z�Y [�\\ �] !�" [define-key global-map "�" copy-region-as-kill-nomark [S-right] forward-char-mark [right] forward-char-nomark [C-S-right] forward-word-mark [C-right] forward-word-nomark [S-down] next-line-mark [down] next-line-nomark [S-end] end-of-line-mark [end] end-of-line-nomark global-set-key [S-C-end] end-of-buffer-mark [C-end] end-of-buffer-nomark [S-next] scroll-up-mark [next] scroll-up-nomark [S-left] backward-char-mark [left] backward-char-nomark [C-S-left] backward-word-mark [C-left] backward-word-nomark [S-up] previous-line-mark [up] previous-line-nomark [S-home] beginning-of-line-mark [home] beginning-of-line-nomark [S-C-home] beginning-of-buffer-mark [C-home] beginning-of-buffer-nomark [S-prior] scroll-down-mark [prior] scroll-down-nomark [S-insert] yank [C-insert] copy-region-as-kill [S-delete] kill-region [f16] [f18] [f20] [f1] help [f6] other-window [delete] delete-char [C-delete] kill-line [M-backspace] undo [C-down] forward-paragraph-nomark [C-up] backward-paragraph-nomark [S-C-down] forward-paragraph-mark [S-C-up] backward-paragraph-mark function-key-map [M-delete] [-134217628] [C-M-delete] kill-sexp [C-backspace] backward-kill-word [C-escape] list-buffers t transient-mark-mode mark-even-if-inactive delete-selection-mode 1] 4 (#$ . 13400) nil])

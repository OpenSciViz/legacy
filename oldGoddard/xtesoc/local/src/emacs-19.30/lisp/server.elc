;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Sat Nov 11 00:07:35 1995
;;; from file /home/fsf/rms/e19/lisp/server.el
;;; emacs version 19.29.1.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`server.el' was compiled for Emacs 19.29 or later"))


#@41 *The program to use as the edit server.
(defvar server-program (expand-file-name "emacsserver" exec-directory) (#$ . -472))
#@67 *List of hooks to call when visiting a file for the Emacs server.
(defvar server-visit-hook nil (#$ . -602))
#@73 *List of hooks to call when switching to a buffer for the Emacs server.
(defvar server-switch-hook nil (#$ . -717))
#@73 *List of hooks to call when done editing a buffer for the Emacs server.
(defvar server-done-hook nil (#$ . -839))
#@28 the current server process
(defvar server-process nil (#$ . 959))
(byte-code "��!� ��B��" [boundp server-previous-string "" current-load-list] 2)
#@225 List of current server clients.
Each element is (CLIENTID BUFFERS...) where CLIENTID is a string
that can be given to the server process to identify a client.
When a buffer is marked as "done", it is removed from this list.
(defvar server-clients nil (#$ . 1116))
#@69 List of clientids for clients requesting editing of current buffer.
(defvar server-buffer-clients nil (#$ . 1386))
(byte-code "��!�����#�" [make-variable-buffer-local server-buffer-clients put permanent-local t] 4)
#@140 *The window to use for selecting Emacs server buffers.
If nil, use the selected window.
If it is a frame, use the frame's selected window.
(defvar server-window nil (#$ . -1608))
#@148 *Regexp which should match filenames of temporary files
which are deleted and reused after each edit
by the programs that invoke the emacs server.
(defvar server-temp-file-regexp "^/tmp/Re\\|/draft$" (#$ . -1794))
(byte-code "�	��\n �	B��" [server-buffer-clients minor-mode-alist (server-buffer-clients " Server")] 2)
(defalias 'server-log #[(string) "��!� ��q�db�� �\f��n� � )�" [get-buffer "*server*" current-time-string " " string newline] 3])
(defalias 'server-sentinel #[(proc msg) "�	!�=� ���!!��	!�=� ���!!�" [process-status proc exit server-log message "Server subprocess exited" signal "Server subprocess killed"] 3])
#@371 Allow this Emacs process to be a server for client processes.
This starts a server communications subprocess through which
client "editors" can send your editing commands to this Emacs job.
To use the server, set up the program `emacsclient' in the
Emacs distribution as your standard "editor".

Prefix arg means just kill any existing server communications subprocess.
(defalias 'server-start #[(&optional leave-dead) "� ��\"���ď���Ə�� ��\"��͏��- ��Ϗ�*�C @A@�!�)�. ?�k �S ���!!�����#)��\"���\"��!�" [server-process set-process-sentinel nil (delete-process server-process) ((error)) (delete-file "~/.emacs_server") ((error)) system-name sysname string-match "\\." dot-index (byte-code "���� \f#!�" [delete-file format "/tmp/esrv%d-%s" user-uid sysname] 5) ((error)) (byte-code "���� \f�O#!�" [delete-file format "/tmp/esrv%d-%s" user-uid sysname 0 dot-index] 7) ((error)) server-clients buffer server-buffer-done leave-dead server-log message "Restarting server" process-connection-type start-process "server" server-program server-sentinel set-process-filter server-process-filter process-kill-without-query] 4 (#$ . 2434) "P"])
(defalias 'server-process-filter #[(proc string) "�	!�\n	P��	\"�� 	ŉ�OƉ�	\n	ŕ�O��\"�; ��ŕ�OP!��� ��\"�� ŕ�O����\"OC\nŕ�O��\"�� ŔŕSOŕ�O��\"�� ���O!�� D	B	�)�^ �	\n\"�\nB�\nA@!���!����!!�,� 	��" [server-log string server-previous-string string-match "\n" 0 nil 1 lineno files client request "^Error: " message "Server error: " "^Client: " " " "[^ ]+ " arg "\\`\\+[0-9]+\\'" read server-visit-files server-clients server-switch-buffer run-hooks server-switch-hook substitute-command-keys "When done with a buffer, type \\[server-edit]"] 6])
#@127 Finds FILES and returns the list CLIENT with the buffers nconc'd.
FILES is an alist whose elements are (FILENAME LINENUMBER).
(defalias 'server-visit-files #[(files client) "��pŎ�v @@�!��N \nq�N �!�< �\n!�4 �\n!�X ���\"��X ���Q!�X �!��X �!q���!�*�@A@!�@Bp\fBA�� )\f�+�" [nil t obuf last-nonmenu-event client-record ((set-buffer obuf)) files filen get-file-buffer file-exists-p verify-visited-file-modtime buffer-modified-p revert-buffer y-or-n-p "File no longer exists: " ", write buffer to file? " write-file find-file-noselect run-hooks server-visit-hook goto-line client server-buffer-clients] 5 (#$ . 4213)])
#@270 Mark BUFFER as "done" for its client(s).
This buries the buffer, then returns a list of the form (NEXT-BUFFER KILLED).
NEXT-BUFFER is another server buffer, as a suggestion for what to select next,
or nil.  KILLED is t if we killed BUFFER (because it was a temp file).
(defalias 'server-buffer-done #[(buffer) "�	!�=É�	\n�� @	�+ \f>A@	�\f\"���[ �@!�R �@!�R �@\"�A��: )A�� \n�� �p ��!���	��@\"\"����@\"!��\")A�� �\f!�� �\f!�� �\fq����!�)�\f!�� �\f!���� �\f!�	-D�" [process-status server-process run nil t server-clients old-clients first killed next-buffer running client buffer delq tail bufferp buffer-name sit-for 1 send-string format "Close: %s Done\n" server-log server-buffer-clients run-hooks server-done-hook server-temp-file-p kill-buffer bury-buffer] 6 (#$ . 4870)])
#@258 Return non-nil if BUFFER contains a file considered temporary.
These are files whose names suggest they are repeatedly
reused to pass information to another program.

The variable `server-temp-file-regexp' controls which filenames
are considered temporary.
(defalias 'server-temp-file-p #[(buffer) "�	!�\f ��	!\"�" [buffer-file-name buffer string-match server-temp-file-regexp] 4 (#$ . 5726)])
#@294 Offer to save current buffer, mark it as "done" for clients.
This buries the buffer, then returns a list of the form (NEXT-BUFFER KILLED).
NEXT-BUFFER is another server buffer, as a suggestion for what to select next,
or nil.  KILLED is t if we killed the BUFFER (because it was a temp file).
(defalias 'server-done #[nil "p	�- �!� É� �*�* � �* ��\n�Q!�* �!��!)�" [buffer server-buffer-clients server-temp-file-p nil buffer-backed-up version-control save-buffer buffer-modified-p y-or-n-p "Save file " buffer-file-name "? " server-buffer-done] 4 (#$ . 6128)])
(defalias 'server-kill-buffer-query-function #[nil "?� ����p!\"!�" [server-buffer-clients yes-or-no-p format "Buffer `%s' still has clients; kill it? " buffer-name] 5])
(add-hook (quote kill-buffer-query-functions) (quote server-kill-buffer-query-function))
(defalias 'server-kill-emacs-query-function #[nil "�	\n� �����\n@A\"\">� �\nA�� ?�' ��!*�" [nil server-clients tail live-client t mapcar stringp buffer-name yes-or-no-p "Server buffers still have clients; exit anyway? "] 7])
(add-hook (quote kill-emacs-query-functions) (quote server-kill-emacs-query-function))
#@695 Switch to next server editing buffer; say "Done" for current buffer.
If a server buffer is current, it is marked "done" and optionally saved.
When all of a client's buffers are marked as "done", the client is notified.

Temporary files such as MH <draft> files are always saved and backed up,
no questions asked.  (The variable `make-backup-files', if nil, still
inhibits a backup; you can set it locally in a particular buffer to
prevent a backup for it.)  The variable `server-temp-file-regexp' controls
which filenames are considered temporary.

If invoked with a prefix argument, or if there is no server process running, 
starts server process and that is all.  Invoked by \[server-edit].
(defalias 'server-edit #[(&optional arg) "� 	� �	!�>� ��!���� \"�" [arg server-process process-status (signal exit) server-start nil apply server-switch-buffer server-done] 3 (#$ . 7284) "P"])
#@126 Switch to another buffer, preferably one that has a client.
Arg NEXT-BUFFER is a suggestion; if it is a live buffer, use it.
(defalias 'server-switch-buffer #[(&optional next-buffer killed-one) "�	!� �	!� �	!��( �	!�( �	!�\" � ��	!!��� !�7 �����#!�����#�� !�X � =�X �����#!��> )�� �\"�� �!�w �!�w �!����!\"��� �@A@\"�?�� �� !�" [windowp server-window window-live-p select-window framep frame-live-p make-frame frame-selected-window window-minibuffer-p selected-window next-window nil nomini 0 previous-window last-window window-dedicated-p set-window-dedicated-p next-buffer bufferp buffer-name switch-to-buffer apply server-switch-buffer server-buffer-done server-clients killed-one other-buffer] 5 (#$ . 8182)])
(byte-code "���\"���!�" [global-set-key "#" server-edit provide server] 3)

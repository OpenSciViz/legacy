;ELC   
;;; compiled by roland@snoopy.gnu.ai.mit.edu on Mon Nov 13 23:51:06 1995
;;; from file /usr/src/emacs-build/lisp/vc-hooks.el
;;; emacs version 19.29.91.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`vc-hooks.el' was compiled for Emacs 19.29 or later"))


#@133 *Back-end actually used by this interface; may be SCCS or RCS.
The value is only computed when needed to avoid an expensive search.
(defvar vc-default-back-end nil (#$ . -483))
#@93 *If non-nil, use VC for files managed with CVS.
If it is nil, don't use VC for those files.
(defvar vc-handle-cvs t (#$ . -666))
#@194 *Indicates whether rcsdiff understands the --brief option.
The value is either `yes', `no', or nil.  If it is nil, VC tries
to use --brief and sets this variable to remember whether it worked.
(defvar vc-rcsdiff-knows-brief nil (#$ . -802))
#@68 *List of extra directories to search for version control commands.
(defvar vc-path (byte-code "��!� " [file-directory-p "/usr/sccs" ("/usr/sccs")] 2) (#$ . -1049))
#@148 *Where to look for version-control master files.
The first pair corresponding to a given back end is used as a template
when creating new masters.
(defvar vc-master-templates (quote (("%sRCS/%s,v" . RCS) ("%s%s,v" . RCS) ("%sRCS/%s" . RCS) ("%sSCCS/s.%s" . SCCS) ("%ss.%s" . SCCS) vc-find-cvs-master)) (#$ . -1223))
#@146 *If non-nil, backups of registered files are made as with other files.
If nil (the default), files covered by version control don't get backups.
(defvar vc-make-backup-files nil (#$ . -1546))
#@93 *If non-nil, display revision number and lock status in modeline.
Otherwise, not displayed.
(defvar vc-display-status t (#$ . -1744))
#@56 *Identify work files by searching for version headers.
(defvar vc-consult-headers t (#$ . -1884))
#@157 *If non-nil, don't delete working files after registering changes.
If the back-end is CVS, workfiles are always kept, regardless of the
value of this flag.
(defvar vc-keep-workfiles t (#$ . -1989))
#@76 *Don't assume that permissions and ownership track version-control status.
(defvar vc-mistrust-permissions nil (#$ . -2193))
(defalias 'vc-mistrust-permissions #[(file) "�=� � �!!�" [vc-mistrust-permissions t vc-backend-subdirectory-name file] 3])
(byte-code "��\n\"� �\nB��!�����#�" [assoc vc-mode minor-mode-alist (vc-mode vc-mode) make-variable-buffer-local put permanent-local t] 4)
(defalias 'vc-error-occurred '(macro . #[(&rest body) "����\f�\"B�F�" [condition-case nil progn append body (nil) (error t)] 6]))
#@34 Obarray for per-file properties.
(defvar vc-file-prop-obarray [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] (#$ . 2724))
(byte-code "��!� ��B��!�" [boundp vc-buffer-backend t current-load-list make-variable-buffer-local] 2)
(defalias 'vc-file-setprop #[(file property value) "��\n\"\f#�" [put intern file vc-file-prop-obarray property value] 4])
(defalias 'vc-file-getprop #[(file property) "�	\n\"N�" [intern file vc-file-prop-obarray property] 3])
(defalias 'vc-file-clearprops #[(file) "��\n\"�\"�" [setplist intern file vc-file-prop-obarray nil] 4])
(defalias 'vc-match-substring #[(bn) "��{�" [bn] 2])
(defalias 'vc-lock-file #[(file) "�	!�� ��\n\"� \nŔŕO�\nǔǕOQ)�" [vc-name file master string-match "\\(.*/\\)s\\.\\(.*\\)" 1 "p." 2] 6])
(defalias 'vc-parse-buffer #[(patterns &optional file properties) "��\n\"�" [mapcar #[(p) "eb�G�=�1 ��@��#� �ǜ!�. �\n@#�\nA\n)�G�=�| ���@��#�e ���!��a �ǜ!)�> �y �\n@#�\nA\n*�" [p 2 nil value re-search-forward t vc-match-substring 1 file vc-file-setprop properties 3 "" latest-val latest-date date] 4] patterns] 3])
(defalias 'vc-insert-file #[(file &optional limit blocksize) "� ��\n!�U �G \f� �Ɖ��C � �Tb��\n��\f\\$A@�U�= �y����#�� *�K �\n!���!���!�͇Ƈ" [erase-buffer file-exists-p file limit blocksize 8192 nil s found buffer-size insert-file-contents 0 re-search-forward t set-buffer-modified-p auto-save-mode] 7])
(defalias 'vc-parse-locks #[(file locks) "�\n �\n��#���ǉ�	\n\f�\n!�=�M ��#�� єѕO	ҔҕO�\n	BC\"\nƕ�! �\n!�=�� ��#�� ҔҕO	єѕO�\n	BC\"\nƕ�U ��#�� �\n��#��� �\n��#��\n�\n�� �#-�" [locks vc-file-setprop file vc-master-locks none t 0 nil user version master-locks index found vc-backend SCCS string-match "^\\([0-9.]+\\) [0-9.]+ \\([^ ]+\\) .*\n?" 1 2 append RCS "[ 	\n]*\\([^:]+\\):\\([0-9.]+\\)" ";[ 	\n]+strict;" vc-checkout-model manual implicit] 5])
(defalias 'vc-simple-command #[(okstatus command file &rest args) "���!q�� �)�\f\"����!\n��\f\n#RB������C\"&�V�N ��!!����!!���!�+�" [get-buffer-create "*vc-info*" erase-buffer append vc-path exec-path nil "PATH=" getenv "PATH" path-separator mapconcat identity process-environment exec-status apply call-process command args file okstatus switch-to-buffer get-file-buffer shrink-window-if-larger-than-buffer display-buffer error "Couldn't find version control information"] 10])
(defalias 'vc-fetch-master-properties #[(file) "��	!�=�? ��!q���	!!� �	� \"��% �	��#���	!�\"������ !�Q�DD	�#��A�	!�=�� ��!q���	!�\"�����E	�#��	�\"��m ߘ�y �	��	�\"#��� ��\"�� �	�#��� ��	!�\"����!�Q��EC	�#�)�	�	�\"\"��A�	!�=�A��	!+����	!�$�)��!q���	�#��	�\"5��5\"�� �	��#��	���	!8#��@�	���5\"���>��5\"���>��5\"��@ �>�A 5\"�+�B �>�C 5\"�;�D �>�E #�*��!�N�F ��!!)�" [vc-backend file SCCS get-buffer-create "*vc-info*" vc-insert-file vc-lock-file vc-parse-locks buffer-string vc-file-setprop vc-master-locks none vc-name "^e" vc-parse-buffer ("^d D \\([^ ]+\\)" 1) "^d D \\([^ ]+\\) .* " regexp-quote user-login-name " " 1 (vc-latest-version vc-your-latest-version) RCS "^[0-9]" ("^head[ 	\n]+\\([^;]+\\);" 1) ("^branch[ 	\n]+\\([^;]+\\);" 1) ("^locks[ 	\n]*\\([^;]*;\\([ 	\n]*strict;\\)?\\)" 1) (vc-head-version vc-default-branch vc-master-locks) vc-file-getprop vc-default-branch default-branch "" vc-master-workfile-version vc-head-version string-match "^[0-9]+\\.[0-9]+\\(\\.[0-9]+\\.[0-9]+\\)*$" "^desc" "^\\(" "\\.[0-9]+\\)\ndate[ 	]+\\([0-9.]+\\);" 2 (vc-master-workfile-version) CVS file-name-directory default-directory vc-simple-command 0 "cvs" file-name-nondirectory "status" get-buffer (("\\(RCS Version\\|RCS Revision\\|Repository revision\\):[	 ]+\\([0-9.]+\\)" 2) ("^File: [^ 	]+[ 	]+Status: \\(.*\\)" 1)) (vc-latest-version vc-cvs-status) vc-cvs-status status "Up-to-date" up-to-date vc-checkout-time 5 file-attributes "Locally Modified" locally-modified "Needs Merge" needs-merge "Needs \\(Checkout\\|Patch\\)" needs-checkout "Unresolved Conflict" unresolved-conflict "Locally Added" locally-added unknown kill-buffer] 7])
(defalias 'vc-consult-rcs-headers #[(file) "�\n �\n!�\f ÇÉ���\n!q�eb�����#�* ��!�; eb�����#�r ��!�r ͕b�ΔΕ{����Q!�� ͕b���!�` ���� ��!�� ΔΕ{��� ���P��#�� ΔΕ{eb����P��#�� ��!�� ΔΕ{��� ��!�� ���� ���� �)�� �\n�#��=��\n!�=��\n�\f#��\n!��\n!�����\n!8\"�� �\n��#���\n��#�+�" [vc-consult-headers get-file-buffer file nil locking-user version status search-forward "$Id: " t looking-at "[^ ]+ \\([0-9.]+\\) " "$Header: " 0 1 "[0-9]+[/-][01][0-9][/-][0-3][0-9] " "[0-2][0-9]:[0-5][0-9]+:[0-6][0-9]+\\([+-][0-9:]+\\)? " "[^ ]+ [^ ]+ " "\\$" none rev-and-lock "\\([^ ]+\\) \\$" re-search-forward "Revision: \\([0-9.]+\\) \\$" "Locker:" " \\([^ ]+\\) \\$" " *\\$" rev vc-file-setprop vc-workfile-version vc-backend RCS vc-locking-user vc-mistrust-permissions string-match ".r-..-..-." 8 file-attributes vc-checkout-model manual implicit] 5])
(defalias 'vc-backend-subdirectory-name #[(&optional file) "�	� �	!� � ��!� Ƃ ǉ!�" [symbol-name file vc-backend vc-default-back-end vc-find-binary "rcs" RCS SCCS] 3])
#@115 Return the master name of a file, nil if it is not registered.
For CVS, the full name of CVS/Entries is returned.
(defalias 'vc-name #[(file) "�	�\"� �	!�� �	�\fA#��	�\f@#)�" [vc-file-getprop file vc-name vc-registered name-and-type vc-file-setprop vc-backend] 5 (#$ . 7905)])
#@73 Return the version-control type of a file, nil if it is not registered.
(defalias 'vc-backend #[(file) "�! ��\"�! �!��  ��\f@#���\fA#)�" [file vc-file-getprop vc-backend vc-registered name-and-type vc-file-setprop vc-name] 5 (#$ . 8191)])
(defalias 'vc-checkout-model #[(file) "�	�\"�H �	!�=� �	��#��	!�=�1 �	!��	�\"�H �	!��	�\"��	!�=�H �	���!�F ƂG �#�" [vc-file-getprop file vc-checkout-model vc-backend SCCS vc-file-setprop manual RCS vc-consult-rcs-headers vc-fetch-master-properties CVS getenv "CVSREAD" implicit] 5])
(defalias 'vc-cvs-status #[(file) "�	�\"� �	!��	�\"�" [vc-file-getprop file vc-cvs-status vc-fetch-master-properties] 3])
(defalias 'vc-master-locks #[(file) "�	�\"� �	!��	�\"�" [vc-file-getprop file vc-master-locks vc-fetch-master-properties] 3])
(defalias 'vc-master-locking-user #[(file) "�	!���=� ł! ��	!\f\"��  A�! �*�" [vc-master-locks file nil lock master-locks none assoc vc-workfile-version] 4])
(defalias 'vc-lock-from-permissions #[(file) "�	!�	!?�4 ���\n8\"� �	��#�4 �\n8� U�4 ���\n8\"�4 �	�� #)�" [file-attributes file attributes vc-mistrust-permissions string-match ".r-..-..-." 8 vc-file-setprop vc-locking-user none 2 user-uid ".rw..-..-." user-login-name] 4])
(defalias 'vc-file-owner #[(file) "��\n!8�� U� � � )�" [2 file-attributes file uid user-uid user-login-name] 4])
(defalias 'vc-rcs-lock-from-diff #[(file) "��\n!P\f�=� ���\n$� ���\n�%��=�< \f�5 ����\n$�B ��!��B \f�B ��U�Q �\n��#�X �\n��\n!#*�" ["-r" vc-workfile-version file version vc-rcsdiff-knows-brief no vc-simple-command 1 "rcsdiff" 2 "--brief" status error "rcsdiff failed." yes 0 vc-file-setprop vc-locking-user none vc-file-owner] 7])
(defalias 'vc-locking-user #[(file) "�	�\"�� �=?�� �� �	!�=�] �	!�=�* �	!�� �	�\"��	!8��? �	��#�� �	!�	�;�R �W ��\"#�)�� �	!�=�� ��	!�=�� �	!��� �=�� �	��#�� �	!�=�� �	��	!#��	�\"�=�� �	!�=�� �	!�)�� �	!�=�� �	!�� �	��	!#��	�\"��=?�� )�" [vc-file-getprop file vc-locking-user locking-user none vc-backend CVS vc-checkout-model manual vc-lock-from-permissions vc-checkout-time 5 file-attributes vc-file-setprop vc-file-owner locker format "%d" RCS nil p-lock vc-consult-rcs-headers rev-and-lock vc-master-locking-user implicit vc-rcs-lock-from-diff SCCS] 7])
(defalias 'vc-latest-version #[(file) "�	�\"� �	!��	�\"�" [vc-file-getprop file vc-latest-version vc-fetch-properties] 3])
(defalias 'vc-your-latest-version #[(file) "�	�\"� �	!��	�\"�" [vc-file-getprop file vc-your-latest-version vc-fetch-properties] 3])
(defalias 'vc-master-workfile-version #[(file) "�	�\"� �	!��	�\"�" [vc-file-getprop file vc-master-workfile-version vc-fetch-master-properties] 3])
(defalias 'vc-fetch-properties #[(file) "�	!�=�6 ���!q���	!�\"�������� !ϰ��ED	�#���!�4 ���!!)��	!�" [vc-backend file RCS get-buffer-create "*vc-info*" vc-insert-file vc-name "^desc" vc-parse-buffer ("^\\([0-9]+\\.[0-9.]+\\)\ndate[ 	]+\\([0-9.]+\\);" 1 2) "^\\([0-9]+\\.[0-9.]+\\)\n" "date[ 	]+\\([0-9.]+\\);[ 	]+" "author[ 	]+" regexp-quote user-login-name ";" 1 2 (vc-latest-version vc-your-latest-version) get-buffer kill-buffer vc-fetch-master-properties] 7])
(defalias 'vc-workfile-version #[(file) "�	�\"�[ �	!�=� �	!��	!�=�@ �	!�& �	�\"��	!�3 �	!�3 �\n�	�\n#�\n)��	!�=�[ �	!�S �	�\"��΍��	�\"�" [vc-file-getprop file vc-workfile-version vc-backend SCCS vc-latest-version RCS vc-consult-rcs-headers vc-master-workfile-version nil rev vc-file-setprop CVS found (byte-code "��\n!�\n!\"�" [vc-find-cvs-master file-name-directory file file-name-nondirectory] 4)] 4])
(defalias 'vc-registered #[(file) "����!� ��\"\n� \n�\"�0 �!�$ ��!\f�΍**�" [nil handlers handler boundp file-name-handler-alist find-file-name-handler file vc-registered file-name-directory "" file-name-nondirectory basename dirname found (byte-code "��\n\"�Ç" [mapcar #[(s) ":�\n 	\n\"��@	\n#�\f!�2 	�\f!��+ �!�\f!�?�2 ��\fAB\")�" [s dirname basename format trial file-exists-p file-name-directory file-attributes file throw found] 4] vc-master-templates nil] 3)] 3])
(defalias 'vc-utc-string #[(timeval) "�	@	A@�	!@\fZ��V�\" T\f�ZD�4 \f�W�1 S\f�\\D�4 \fD�!,�" [nil timeval current-time-zone offset low high utc 65535 65536 0 current-time-string] 6])
(defalias 'vc-find-cvs-master #[(dirname basename) "�� �\n�P!�� �\n�P!�� Ɖ\nP	\n\f͎���!q\f�\n�P!�eb�����!�Q��#�� \n�	���!#���	!8��!�!��r �	�#��y �	��#�)��\n�P�B\"�� \n�.�" [vc-handle-cvs file-directory-p dirname "CVS/" file-readable-p "CVS/Entries" nil case-fold-search basename file fold time buffer ((kill-buffer buffer)) get-buffer-create "*vc-info*" vc-insert-file re-search-forward "^/" regexp-quote "/\\([^/]*\\)/\\([^/]*\\)/" t vc-file-setprop vc-workfile-version match-string 1 5 file-attributes mtime 2 vc-utc-string vc-checkout-time 0 throw found CVS] 5])
#@70 Return the version-control type of the visited file, or nil if none.
(defalias 'vc-buffer-backend #[nil "�=� �� !���" [vc-buffer-backend t vc-backend buffer-file-name] 2 (#$ . 13026)])
#@275 Change read-only status of current buffer, perhaps via version control.
If the buffer is visiting a file registered with version control,
then check the file in or out.  Otherwise, just change the read-only flag
of the buffer.  With prefix argument, ask for version number.
(defalias 'vc-toggle-read-only #[(&optional verbose) "�� !� �!�� �" [vc-backend buffer-file-name vc-next-action verbose toggle-read-only] 2 (#$ . 13222) "P"])
(define-key global-map "" (quote vc-toggle-read-only))
(defalias 'vc-after-save #[nil "� �	�\"�F �	�\"��	!8�� �	��#��	!?�F �	!�=�F �	�� #�F �	!�=�C �	��#��	!)�" [buffer-file-name file vc-file-getprop vc-backend vc-checkout-time 5 file-attributes vc-file-setprop nil vc-locking-user vc-checkout-model implicit user-login-name CVS vc-cvs-status vc-mode-line] 4])
#@217 Set `vc-mode' to display type of version control for FILE.
The value is set in the current buffer, which should be the buffer
visiting FILE.  Second optional arg LABEL is put in place of version
control system name.
(defalias 'vc-mode-line #[(file &optional label) "�	!�� �\f� �\n!� �	!Q\n�8 	� ��8 �	!�8 � �	!��8 �� �\n)�" [vc-backend file vc-type " " label symbol-name vc-display-status vc-status vc-mode buffer-file-name vc-locking-user user-login-name t buffer-read-only force-mode-line-update] 5 (#$ . 14029) (list buffer-file-name nil)])
(defalias 'vc-status #[(file) "�	!�	!Ř� Ƃ: \f� �P�: \f;�( \f� ��, \f� U�5 �P�: �\f�R*�" [vc-locking-user file vc-workfile-version rev locker "0" " @@" "-" user-login-name user-uid ":"] 4])
(defalias 'vc-find-file-hook #[nil "�9 �!��!�  �!�\f?�9 ��!�ǉ��!�	�- �	!�\n�8 ��\n\"*�" [buffer-file-name vc-file-clearprops vc-backend vc-mode-line vc-make-backup-files make-local-variable backup-inhibited t file-symlink-p link link-type message "Warning: symbolic link to %s-controlled source file"] 4])
(add-hook (quote find-file-hooks) (quote vc-find-file-hook))
#@116 When file is not found, try to check it out from RCS or SCCS.
Returns t if checkout was successful, nil otherwise.
(defalias 'vc-file-not-found-hook #[nil "�	!� ���!��� !��ȏ)?�" [vc-backend buffer-file-name require vc file-name-directory default-directory nil (byte-code "�	!�" [vc-checkout buffer-file-name nil] 2) ((error t))] 3 (#$ . 15168)])
(add-hook (quote find-file-not-found-hooks) (quote vc-file-not-found-hook))
(defalias 'vc-kill-buffer-hook #[nil "� ;� �� !���!�" [buffer-file-name vc-file-clearprops kill-local-variable vc-buffer-backend] 2])
(byte-code "�	�\"�!�\\ � �	�#����#����#����#����#����#����#����#����#����#����#����#����#���!�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�� ��#�����@ #�����@ #�����@ #�����A #�����@ #�����@ #�����@ #�����@ #�����B #���C ��D #�����E #��F �G !�" [lookup-key global-map "v" vc-prefix-map keymapp make-sparse-keymap define-key "a" vc-update-change-log "c" vc-cancel-version "d" vc-directory "h" vc-insert-headers "i" vc-register "l" vc-print-log "r" vc-retrieve-snapshot "s" vc-create-snapshot "u" vc-revert-buffer "v" vc-next-action "=" vc-diff "~" vc-version-other-window boundp vc-menu-map [vc-directory] ("Show Locked Files" . vc-directory) [separator1] ("----") [vc-rename-file] ("Rename File" . vc-rename-file) [vc-version-other-window] ("Show Other Version" . vc-version-other-window) [vc-diff] ("Compare with Last Version" . vc-diff) [vc-update-change-log] ("Update ChangeLog" . vc-update-change-log) [vc-print-log] ("Show History" . vc-print-log) [separator2] ("----") [undo] ("Undo Last Check-In" . vc-cancel-version) [vc-revert-buffer] ("Revert to Last Version" . vc-revert-buffer) [vc-insert-header] ("Insert Header" . vc-insert-headers) [vc-menu-check-in] ("Check In" . vc-next-action) [vc-check-out] ("Check Out" . vc-toggle-read-only) [vc-register] ("Register" . vc-register) put vc-rename-file menu-enable vc-mode (eq (vc-buffer-backend) (quote RCS)) (and vc-mode (not buffer-read-only)) vc-toggle-read-only (and vc-mode buffer-read-only) (and buffer-file-name (not vc-mode)) provide vc-hooks] 4)

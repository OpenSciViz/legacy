;ELC   
;;; compiled by roland@whiz-bang.gnu.ai.mit.edu on Sat Sep  9 03:22:09 1995
;;; from file /gd/gnu/emacs/19.0/lisp/register.el
;;; emacs version 19.29.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`register.el' was compiled for Emacs 19.29 or later"))


#@272 Alist of elements (NAME . CONTENTS), one for each Emacs register.
NAME is a character (a number).  CONTENTS is a string, number,
frame configuration, mark or list.
A list of strings represents a rectangle.
A list of the form (file . NAME) represents the file named NAME.
(defvar register-alist nil (#$ . 481))
#@62 Return contents of Emacs register named REG, or nil if none.
(defalias 'get-register #[(reg) "	�A�" [reg register-alist] 2 (#$ . 797)])
#@148 Set contents of Emacs register named REGISTER to VALUE.  Returns VALUE.
See the documentation of the variable `register-alist' for possible VALUE.
(defalias 'set-register #[(register value) "	��� \n��� B�	B)�" [register register-alist aelt value] 3 (#$ . 941)])
#@236 Store current location of point in register REGISTER.
With prefix argument, store current frame configuration.
Use \[jump-to-register] to go to that location or restore that configuration.
Argument is a character, naming the register.
(defalias 'point-to-register #[(register &optional arg) "�	\n� � � � \"�" [set-register register arg current-frame-configuration point-marker] 3 (#$ . 1220) "cPoint to register: \nP"])
#@176 Store the window configuration of the selected frame in register REGISTER.
Use \[jump-to-register] to restore the configuration.
Argument is a character, naming the register.
(defalias 'window-configuration-to-register #[(register &optional arg) "�	� \"�" [set-register register current-window-configuration] 3 (#$ . 1649) "cWindow configuration to register: \nP"])
#@168 Store the window configuration of all frames in register REGISTER.
Use \[jump-to-register] to restore the configuration.
Argument is a character, naming the register.
(defalias 'frame-configuration-to-register #[(register &optional arg) "�	� \"�" [set-register register current-frame-configuration] 3 (#$ . 2022) "cFrame configuration to register: \nP"])
(defalias (quote register-to-point) (quote jump-to-register))
#@546 Move point to location stored in a register.
If the register contains a file name, find that file.
 (To put a file name in a register, you must use `set-register'.)
If the register contains a window configuration (one frame) or a frame
configuration (all frames), restore that frame or all frames accordingly.
First argument is a character, naming the register.
Optional second arg non-nil (interactively, prefix argument) says to
delete any existing frames that the frame configuration doesn't mention.
(Otherwise, these frames are iconified.)
(defalias 'jump-to-register #[(register &optional delete) "�	!��!� �\n!� �\n?\"�V �\n!�% �\n!�V �\n!�@ �\n!�5 ��!���\n!!�\nb�V \n:�S \n@�=�S �\nA!�V ��!)�" [get-register register val fboundp frame-configuration-p set-frame-configuration delete window-configuration-p set-window-configuration markerp marker-buffer error "That register's buffer no longer exists" switch-to-buffer file find-file "Register doesn't contain a buffer position or configuration"] 3 (#$ . 2446) "cJump to register: \nP"])
#@95 Display what is contained in register named REGISTER.
The Lisp value REGISTER is a character.
(defalias 'view-register #[(register) "�	!�� ���	!\"�� Ɛ��!���	!!���!�\n��, �\n!�� �\n!�Y �\n!�\f�A ��!�U ��!���\f!!���!���\n!!)�� �\n!�e ��!�� �\n!�q ��!�� \n:�� \n@�=�� ��!��\nA!���!�� \n:�� ��!�\n�� �\n@!�� �\nA��� \n;�� ��!��\n!�� ��!��\n!�)�" [get-register register val message "Register %s is empty" single-key-description "*Output*" princ "Register " " contains " markerp marker-buffer buf "a marker in no buffer" "a buffer position:\nbuffer " buffer-name ", position " marker-position window-configuration-p "a window configuration." frame-configuration-p "a frame configuration." file "the file " prin1 "." "the rectangle:\n" terpri "the text:\n" "Garbage:\n"] 5 (#$ . 3500) "cView register: "])
#@261 Insert contents of register REGISTER.  (REGISTER is a character.)
Normally puts point before and mark after the inserted text.
If optional second arg is non-nil, puts mark before and point after.
Interactively, second arg is non-nil if prefix arg is supplied.
(defalias 'insert-register #[(register &optional arg) "� ��\n!�:� �!��E ;� c��E ��+ �p\"��E �!�A �!�A ��!p\"��E ��!�)\n?�N � �" [push-mark get-register register val insert-rectangle princ markerp marker-position error "Register does not contain text" arg exchange-point-and-mark] 4 (#$ . 4312) "*cInsert register: \nP"])
#@207 Copy region into register REGISTER.  With prefix arg, delete as well.
Called from program, takes four args: REGISTER, START, END and DELETE-FLAG.
START and END are buffer positions indicating what to copy.
(defalias 'copy-to-register #[(register start end &optional delete-flag) "�	\n{\"�\f� \n|�" [set-register register start end delete-flag] 4 (#$ . 4912) "cCopy to register: \nr\nP"])
#@216 Append region to text in register REGISTER.
With prefix arg, delete as well.
Called from program, takes four args: REGISTER, START, END and DELETE-FLAG.
START and END are buffer positions indicating what to append.
(defalias 'append-to-register #[(register start end &optional delete-flag) "�	!;� ��!��	�	!{P\"��  |�" [get-register register error "Register does not contain text" set-register start end delete-flag] 5 (#$ . 5310) "cAppend to register: \nr\nP"])
#@218 Prepend region to text in register REGISTER.
With prefix arg, delete as well.
Called from program, takes four args: REGISTER, START, END and DELETE-FLAG.
START and END are buffer positions indicating what to prepend.
(defalias 'prepend-to-register #[(register start end &optional delete-flag) "�	!;� ��!��	{�	!P\"��  |�" [get-register register error "Register does not contain text" set-register start end delete-flag] 5 (#$ . 5788) "cPrepend to register: \nr\nP"])
#@226 Copy rectangular region into register REGISTER.
With prefix arg, delete as well.
Called from program, takes four args: REGISTER, START, END and DELETE-FLAG.
START and END are buffer positions giving two corners of rectangle.
(defalias 'copy-rectangle-to-register #[(register start end &optional delete-flag) "�	\n� �\f\"� �\f\"\"�" [set-register register delete-flag delete-extract-rectangle start end extract-rectangle] 5 (#$ . 6270) "cCopy rectangle to register: \nr\nP"])

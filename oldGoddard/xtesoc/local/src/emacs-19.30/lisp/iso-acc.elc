;ELC   
;;; compiled by roland@churchy.gnu.ai.mit.edu on Wed Oct 25 12:55:18 1995
;;; from file /gd/gnu/emacs/19.0/lisp/iso-acc.el
;;; emacs version 19.29.1.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`iso-acc.el' was compiled for Emacs 19.29 or later"))


(provide (quote iso-acc))
#@399 List of language-specific customizations for the ISO Accents mode.

Each element of the list is of the form (LANGUAGE ENABLE LIST).

LANGUAGE is a string naming the language.

ENABLE is a list of characters that will be used as accent prefixes.
It will be the value of the iso-accents-enable variable.

LIST is a list of accent translations.  It will be the value of the
iso-accents-list variable.
(defvar iso-languages (quote (("portuguese" (39 96 94 34 126) (((39 65) 193) ((39 69) 201) ((39 73) 205) ((39 79) 211) ((39 85) 218) ((39 67) 199) ((39 97) 225) ((39 101) 233) ((39 105) 237) ((39 111) 243) ((39 117) 250) ((39 99) 231) ((39 32) 39) ((96 65) 192) ((96 97) 224) ((96 32) 96) ((94 65) 194) ((94 69) 202) ((94 79) 212) ((94 97) 226) ((94 101) 234) ((94 111) 244) ((94 32) 94) ((34 85) 220) ((34 117) 252) ((34 32) 34) ((126 65) 195) ((126 79) 213) ((126 97) 227) ((126 111) 245) ((126 32) 126))) ("french" (39 96 94 34 126) (((39 65) 193) ((39 69) 201) ((39 73) 205) ((39 79) 211) ((39 85) 218) ((39 67) 199) ((39 97) 225) ((39 101) 233) ((39 105) 237) ((39 111) 243) ((39 117) 250) ((39 99) 231) ((39 32) 39) ((96 65) 192) ((96 69) 200) ((96 97) 224) ((96 101) 232) ((96 32) 96) ((94 65) 194) ((94 69) 202) ((94 73) 206) ((94 79) 212) ((94 85) 219) ((94 97) 226) ((94 101) 234) ((94 105) 238) ((94 111) 244) ((94 117) 251) ((94 32) 94) ((34 85) 220) ((34 117) 252) ((34 32) 34) ((126 65) 195) ((126 79) 213) ((126 97) 227) ((126 111) 245) ((126 32) 126))) ("default" (39 96 94 34 126 47) (((39 65) 193) ((39 69) 201) ((39 73) 205) ((39 79) 211) ((39 85) 218) ((39 89) 221) ((39 97) 225) ((39 101) 233) ((39 105) 237) ((39 111) 243) ((39 117) 250) ((39 121) 253) ((39 39) 180) ((39 32) 39) ((96 65) 192) ((96 69) 200) ((96 73) 204) ((96 79) 210) ((96 85) 217) ((96 97) 224) ((96 101) 232) ((96 105) 236) ((96 111) 242) ((96 117) 249) ((96 32) 96) ((96 96) 96) ((94 65) 194) ((94 69) 202) ((94 73) 206) ((94 79) 212) ((94 85) 219) ((94 97) 226) ((94 101) 234) ((94 105) 238) ((94 111) 244) ((94 117) 251) ((94 32) 94) ((94 94) 94) ((34 65) 196) ((34 69) 203) ((34 73) 207) ((34 79) 214) ((34 85) 220) ((34 97) 228) ((34 101) 235) ((34 105) 239) ((34 111) 246) ((34 115) 223) ((34 117) 252) ((34 121) 255) ((34 32) 34) ((34 34) 168) ((126 65) 195) ((126 67) 199) ((126 68) 208) ((126 78) 209) ((126 79) 213) ((126 84) 222) ((126 97) 227) ((126 99) 231) ((126 100) 240) ((126 110) 241) ((126 111) 245) ((126 116) 254) ((126 62) 187) ((126 60) 171) ((126 32) 126) ((126 126) 184) ((47 65) 197) ((47 69) 198) ((47 79) 216) ((47 97) 229) ((47 101) 230) ((47 111) 248) ((47 32) 47) ((47 47) 176))))) (#$ . 508))
#@115 Language for which ISO Accents mode is currently customized.
Change it with the `iso-accents-customize' function.
(defvar iso-language nil (#$ . 3130))
#@72 Association list for ISO accent combinations, for the chosen language.
(defvar iso-accents-list nil (#$ . 3288))
#@133 *Non-nil enables ISO Accents mode.
Setting this variable makes it local to the current buffer.
See the function `iso-accents-mode'.
(defvar iso-accents-mode nil (#$ . -3408))
(make-variable-buffer-local (quote iso-accents-mode))
#@59 Modify the following character by adding an accent to it.
(defalias 'iso-accents-accent-key #[(prompt) "� �\n!��\f!�" [iso-accents-mode iso-accents-compose prompt char-to-string last-input-char] 2 (#$ . 3643)])
#@59 Modify the following character by adding an accent to it.
(defalias 'iso-accents-compose-key #[(prompt) "�	!� ����\n�H!�\f@!#�)\n)�" [iso-accents-compose prompt combined unread-command-events unread nil error "Characters %s and %s cannot be composed" single-key-description 0] 5 (#$ . 3862)])
(defalias 'iso-accents-compose #[(prompt) "\n� ��!�=� �\" ��\n� �	#�� �, 	c�� `S`|��	D\"��F A@C��N C�	!+�" [last-input-char first-char prompt key-binding "a" self-insert-command this-command message "%s%c" "Compose with " read-event second-char assoc iso-accents-list entry unread-command-events vector] 5])
#@248 *List of accent keys that become prefixes in ISO Accents mode.
The default is (?' ?` ?^ ?" ?~ ?/), which contains all the supported
accent keys.  For certain languages, you might want to remove some of
those characters that are not actually used.
(defvar iso-accents-enable nil (#$ . -4498))
#@902 Toggle ISO Accents mode, in which accents modify the following letter.
This permits easy insertion of accented characters according to ISO-8859-1.
When Iso-accents mode is enabled, accent character keys
(`, ', ", ^, / and ~) do not self-insert; instead, they modify the following
letter key so that it inserts an ISO accented letter.

The variable `iso-accents-enable' specifies the list of characters to
enable as accents.  If you don't need all of them, remove the ones you
don't need from that list.

Special combinations: ~c gives a c with cedilla,
~d gives an Icelandic eth (d with dash).
~t gives an Icelandic thorn.
"s gives German sharp s.
/a gives a with ring.
/e gives an a-e ligature.
~< and ~> give guillemets.
~! gives an inverted exclamation mark.
~? gives an inverted question mark.

With an argument, a positive argument enables ISO Accents mode, 
and a negative argument disables it.
(defalias 'iso-accents-mode #[(&optional arg) "�\f �!�X� � ĉ�ŉ�" [arg prefix-numeric-value 0 iso-accents-mode nil t] 2 (#$ . 4797) "P"])
#@154 Customize the ISO accents machinery for a particular language.
It selects the customization based on the specifications in the
`iso-languages' variable.
(defalias 'iso-accents-customize #[(language) "�	\n\"��� ��!�K 	A@	AA@\n�. ���#��2 � 	��K ��\f@!�#�\fA��9 �*�" [assoc language iso-languages nil c table error "Unknown language" iso-language iso-accents-enable iso-accents-list key-translation-map substitute-key-definition iso-accents-accent-key make-sparse-keymap define-key char-to-string] 5 (#$ . 5851) (list (completing-read "Language: " iso-languages nil t))])
#@198 Convert two-character sequences in region into accented characters.
Noninteractively, this operates on text from START to END.
This uses the same conversion that ISO Accents mode uses for type-in.
(defalias 'iso-accentuate #[(start end) "��	}�b��u��`	W�J h>�D �gX�D g�X�D �hgD	\"��D �u���!�\fA@c�	S� �u�� +�" [start end 1 nil entry iso-accents-enable 65 122 assoc iso-accents-list -1 delete-char 2] 3 (#$ . 6447) "r"])
(defalias 'iso-accent-rassoc-unit #[(value alist) "� @A@	=� A�� � @�" [alist value] 3])
#@211 Convert accented characters in the region into two-character sequences.
Noninteractively, this operates on text from START to END.
This uses the opposite of the conversion done by ISO Accents mode for type-in.
(defalias 'iso-unaccentuate #[(start end) "��	}�b��`	W�; g�V�5 �g\"��5 ��!�@@@A@��	T� �u�� +�" [start end nil entry 127 iso-accent-rassoc-unit iso-accents-list delete-char 1] 3 (#$ . 6980) "r"])
#@130 Convert accented characters in the region into unaccented characters.
Noninteractively, this operates on text from START to END.
(defalias 'iso-deaccentuate #[(start end) "��	}�b��`	W�4 g�V�. �g\"��. ��!�@A@c�� �u�� +�" [start end nil entry 127 iso-accent-rassoc-unit iso-accents-list delete-char 1] 3 (#$ . 7404) "r"])
(iso-accents-customize "default")

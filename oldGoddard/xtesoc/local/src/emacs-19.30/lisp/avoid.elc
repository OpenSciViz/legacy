;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Mon Oct 30 13:04:42 1995
;;; from file /home/fsf/rms/e19/lisp/avoid.el
;;; emacs version 19.29.1.2.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`avoid.el' was compiled for Emacs 19.29 or later"))


(provide (quote avoid))
#@223 Value is t or a symbol if the mouse pointer should avoid the cursor.
See function `mouse-avoidance-mode' for possible values.  Changing this
variable is NOT the recommended way to change modes; use that function 
instead.
(defvar mouse-avoidance-mode nil (#$ . 495))
#@204 *Average distance that mouse will be moved when approached by cursor.
Only applies in mouse-avoidance-mode `jump' and its derivatives.
For best results make this larger than `mouse-avoidance-threshold'.
(defvar mouse-avoidance-nudge-dist 15 (#$ . -769))
#@59 *Variability of `mouse-avoidance-nudge-dist' (which see).
(defvar mouse-avoidance-nudge-var 10 (#$ . -1029))
#@44 Delay between animation steps, in seconds.
(defvar mouse-avoidance-animation-delay .01 (#$ . 1144))
#@166 *Mouse-pointer's flight distance.
If the cursor gets closer than this, the mouse pointer will move away.
Only applies in mouse-avoidance-modes `animate' and `jump'.
(defvar mouse-avoidance-threshold 5 (#$ . -1251))
(byte-code "��!� ��B��!� ��B��!�  ��B" [boundp mouse-avoidance-state nil current-load-list mouse-avoidance-pointer-shapes mouse-avoidance-n-pointer-shapes 0] 2)
#@46 Set the shape of the mouse pointer to SHAPE.
(defalias 'mouse-avoidance-set-pointer-shape #[(shape) "��!�" [shape x-pointer-shape set-mouse-color nil] 2 (#$ . 1646)])
(put (quote mouse-avoidance-set-pointer-shape) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@77 Return the position of point as (FRAME X . Y).
Analogous to mouse-position.
(defalias 'mouse-avoidance-point-position #[nil "� �	!��	!e]�`� � B� S�	!�B� &� @A@\\A@AA@\\+BB�" [selected-window w window-edges edges compute-motion window-start (0 . 0) window-width window-height window-hscroll 0 list selected-frame] 8 (#$ . 1927)])
(defalias 'mouse-avoidance-set-mouse-position #[(pos) "� �	!��	\f@\fA#�)Ň" [selected-frame f raise-frame set-mouse-position pos t] 4])
(defalias 'mouse-avoidance-too-close-p #[(mouse) "� \n@	@=�* \nA@�* �\nA@	A@Z!\fW�* �\nAA	AAZ!\fW)�" [mouse-avoidance-point-position point mouse abs mouse-avoidance-threshold] 3])
#@146 The position to which mouse-avoidance-mode `banish' moves the mouse.
You can redefine this if you want the mouse banished to a different corner.
(defalias 'mouse-avoidance-banish-destination #[nil "� S�B�" [frame-width 0] 2 (#$ . 2592)])
(defalias 'mouse-avoidance-banish-mouse #[nil "�� !�" [mouse-avoidance-set-mouse-position mouse-avoidance-banish-destination] 2])
(defalias 'mouse-avoidance-delta #[(cur delta dist var min max) "	Z\n[[\\]\n[\\\n[\\\f	Z\n\\^	ZW�. ��\f	ZV�< ���N \nW�N �� �` \nW�` \n�� �q \n�W�q �� �� \nW�� �� �� \nW�� \n�� �� �� �� �,�" [min cur dist var max R2 L2 R1 L1 nil delta 0] 6])
(put (quote mouse-avoidance-delta) (quote byte-optimizer) (quote byte-compile-inline-expand))
(defalias 'mouse-avoidance-nudge-mouse #[nil "� �@	A�@�!@Z�� \n\f	Z[\f[\\][\f\\\f[\\\n	Z\f\\^	ZW�] ��\n	ZV�m ��� W� �� �� W�� �� �� �W�� �� �� W�� �� �� W�� �� �� �� �� �.\nA�!AZ�� \n\f	Z[\f[\\][\f\\\f[\\\n	Z\f\\^	ZW�.��\n	ZV�>���PW�P���bW�b���s�W�s����W������W�����������.\n@\\A\\B�=���=���X��@�_!\\A�_!\\B!����]\\�=�� �\"#��!�)�&!���)�#�	A@\\	AA\\B!-�" [mouse-position cur cur-frame cur-pos random mouse-avoidance-nudge-var mouse-avoidance-state mouse-avoidance-nudge-dist 0 frame-width max min var dist delta R2 L2 R1 L1 nil deltax frame-height deltay mouse-avoidance-mode animate proteus 0.0 i 1 mouse-avoidance-set-mouse-position round .1 1.0 mouse-avoidance-random-shape shape x-pointer-shape set-mouse-color sit-for mouse-avoidance-animation-delay] 7])
#@213 Return a random cursor shape.
This assumes that any variable whose name begins with x-pointer- and
has an integer value is a valid cursor shape.  You might want to
redefine this function to suit your own tastes.
(defalias 'mouse-avoidance-random-shape #[nil "� �����#\"�G�!8�" [mouse-avoidance-pointer-shapes mapcar #[(x) "�	!J�" [intern x] 2] all-completions "x-pointer-" obarray (lambda (x) (and (boundp x) (integerp (symbol-value x)))) mouse-avoidance-n-pointer-shapes random] 7 (#$ . 4396)])
(defalias 'mouse-avoidance-banish-hook #[nil "?� �� !� � �" [executing-kbd-macro mouse-avoidance-kbd-command this-command-keys mouse-avoidance-banish-mouse] 2])
(defalias 'mouse-avoidance-exile-hook #[nil "?�D �� !�D � �  �\f!�  \f� �C �C �!?�C \f@� =�@ \fA� ��@ �A!�ˉ)�" [executing-kbd-macro mouse-avoidance-kbd-command this-command-keys mouse-position mp mouse-avoidance-state mouse-avoidance-too-close-p mouse-avoidance-banish-mouse selected-frame mouse-avoidance-banish-destination mouse-avoidance-set-mouse-position nil] 2])
(defalias 'mouse-avoidance-fancy-hook #[nil "?�' �� !�' �� !�' � � �� @=?�& ��\")�" [executing-kbd-macro mouse-avoidance-kbd-command this-command-keys mouse-avoidance-too-close-p mouse-position old-pos mouse-avoidance-nudge-mouse selected-frame apply set-mouse-position] 3])
#@120 Return t if the KEYSEQENCE is composed of keyboard events only.
Return nil if there are any lists in the key sequence.
(defalias 'mouse-avoidance-kbd-command #[(key) "� ���!� Ç�ō�" [key nil vectorp t done (byte-code "�	G�\nW� 	H<� ���\"�T�� *Ǉ" [0 key l i throw done nil t] 4)] 2 (#$ . 5729)])
#@1122 Set cursor avoidance mode to MODE.
MODE should be one of the symbols `banish', `exile', `jump', `animate',
`cat-and-mouse', `proteus', or `none'.

If MODE is nil, toggle mouse avoidance between `none` and `banish'
modes.  Positive numbers and symbols other than the above are treated
as equivalent to `banish'; negative numbers and `-' are equivalent to `none'.

Effects of the different modes: 
 * banish: Move the mouse to the upper-right corner on any keypress.
 * exile: Move the mouse to the corner only if the cursor gets too close,
     and allow it to return once the cursor is out of the way.
 * jump: If the cursor gets too close to the mouse, displace the mouse
     a random distance & direction.
 * animate: As `jump', but shows steps along the way for illusion of motion.
 * cat-and-mouse: Same as `animate'.
 * proteus: As `animate', but changes the shape of the mouse pointer too.

Whenever the mouse is moved, the frame is also raised.

(see `mouse-avoidance-threshold' for definition of "too close",
and `mouse-avoidance-nudge-dist' and `mouse-avoidance-nudge-var' for
definition of "random distance".)
(defalias 'mouse-avoidance-mode #[(&optional mode) "�=� ����\"����\"����\"��=�# �\n�� �=�5 �=�5 �=�E ���\"�\nΉB�� �=�Y ���\"�\n��� �=�z �=�z �n \n�z �� �!�V�� ���\"��\n�� �\n� �" [mode cat-and-mouse animate remove-hook post-command-idle-hook mouse-avoidance-banish-hook mouse-avoidance-exile-hook mouse-avoidance-fancy-hook none nil mouse-avoidance-mode jump proteus add-hook 0 mouse-avoidance-state exile banish t prefix-numeric-value force-mode-line-update] 3 (#$ . 6046) (list (intern (completing-read "Select cursor avoidance technique (SPACE for list): " (quote (("banish") ("exile") ("jump") ("animate") ("cat-and-mouse") ("proteus") ("none"))) nil t)))])
(byte-code "�	��\n �	B��" [mouse-avoidance-mode minor-mode-alist (mouse-avoidance-mode " Avoid")] 2)

# Generated automatically from Makefile.in by configure.
# Makefile for GNU Emacs.
# Copyright (C) 1985, 87, 88, 93, 94, 95 Free Software Foundation, Inc.

# This file is part of GNU Emacs.

# GNU Emacs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# GNU Emacs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GNU Emacs; see the file COPYING.  If not, write to
# the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

# Here are the things that we expect ../configure to edit.
srcdir=/usr/local/src/emacs-19.30/src
VPATH=/usr/local/src/emacs-19.30/src
CC=gcc
CPP=gcc -E
CFLAGS= -g -O 
LN_S=ln -s
# Substitute an assignment for the MAKE variable, because
# BSD doesn't have it as a default.


# On Xenix and the IBM RS6000, double-dot gets screwed up.
dot = .
lispsource = ${srcdir}/$(dot)$(dot)/lisp/
libsrc = $(dot)$(dot)/lib-src/
etc = $(dot)$(dot)/etc/
shortnamesdir = $(dot)$(dot)/shortnames/
cppdir = $(dot)$(dot)/cpp/
oldXMenudir = $(dot)$(dot)/oldXMenu/
lwlibdir = $(dot)$(dot)/lwlib/

# Configuration files for .o files to depend on.
M_FILE = ${srcdir}/m/sparc.h
S_FILE = ${srcdir}/s/sol2-4.h
config_h = config.h $(M_FILE) $(S_FILE)

# ========================== start of cpp stuff ======================= */
/* From here on, comments must be done in C syntax.  */

CPPFLAGS=
LDFLAGS=
C_SWITCH_SYSTEM=

/* just to be sure the sh is used */
SHELL=/bin/sh

#define NO_SHORTNAMES
#define NOT_C_CODE
#include "config.h"

/* We won't really call alloca;
   don't let the file name alloca.c get messed up.  */
#ifdef alloca
#undef alloca
#endif

/* Use HAVE_X11 as an alias for X11 in this file
   to avoid problems with X11 as a subdirectory name
   in -I and other such options which pass through this file. */

#ifdef X11
#define HAVE_X11
#undef X11
#endif

/* On some machines #define register is done in config;
   don't let it interfere with this file.  */
#undef register

/* On some systems we may not be able to use the system make command. */
#ifdef MAKE_COMMAND
MAKE = MAKE_COMMAND
#endif

#ifdef C_COMPILER
CC = C_COMPILER
#endif

/* GNU libc requires ORDINARY_LINK so that its own crt0 is used.
   Linux is an exception because it uses a funny variant of GNU libc.  */
#ifdef __GNU_LIBRARY__
#ifndef LINUX
#define ORDINARY_LINK
#endif
#endif

/* Some machines don't find the standard C libraries in the usual place.  */
#ifndef ORDINARY_LINK
#ifndef LIB_STANDARD
#define LIB_STANDARD -lc
#endif
#else
#ifndef LIB_STANDARD
#define LIB_STANDARD
#endif
#endif

/* Unless inhibited or changed, use -lg to link for debugging.  */
#ifndef LIBS_DEBUG
#define LIBS_DEBUG -lg
#endif

/* Some s/*.h files define this to request special libraries.  */
#ifndef LIBS_SYSTEM
#define LIBS_SYSTEM
#endif

/* Some m/*.h files define this to request special libraries.  */
#ifndef LIBS_MACHINE
#define LIBS_MACHINE
#endif

#ifndef LIB_MATH
# ifdef LISP_FLOAT_TYPE
#  define LIB_MATH -lm
# else /* ! defined (LISP_FLOAT_TYPE) */
#  define LIB_MATH
# endif /* ! defined (LISP_FLOAT_TYPE) */
#endif /* LIB_MATH */

/* Some s/*.h files define this to request special switches in ld.  */
#ifndef LD_SWITCH_SYSTEM
#if !defined (__GNUC__) && (defined(COFF_ENCAPSULATE) || (defined (BSD) && !defined (COFF)))
#define LD_SWITCH_SYSTEM -X
#else /* ! defined(COFF_ENCAPSULATE) || (defined (BSD) && !defined (COFF)) */
#define LD_SWITCH_SYSTEM
#endif /* ! defined(COFF_ENCAPSULATE) || (defined (BSD) && !defined (COFF)) */
#endif /* LD_SWITCH_SYSTEM */

/* Some m/*.h files define this to request special switches in ld.  */
#ifndef LD_SWITCH_MACHINE
#define LD_SWITCH_MACHINE
#endif

/* Some m/*.h files define this to request special switches in cc.  */
#ifndef C_SWITCH_MACHINE
#define C_SWITCH_MACHINE
#endif

/* Some s/*.h files define this to request special switches in cc.  */
#ifndef C_SWITCH_SYSTEM
#define C_SWITCH_SYSTEM
#endif

/* These macros are for switches specifically related to X Windows.  */
#ifndef C_SWITCH_X_MACHINE
#define C_SWITCH_X_MACHINE
#endif

#ifndef C_SWITCH_X_SYSTEM
#define C_SWITCH_X_SYSTEM
#endif

#ifndef C_SWITCH_X_SITE
#define C_SWITCH_X_SITE
#endif

#ifndef LD_SWITCH_X_SITE
#define LD_SWITCH_X_SITE
#endif

#ifndef LD_SWITCH_X_DEFAULT
#define LD_SWITCH_X_DEFAULT
#endif

/* These can be passed in from config.h to define special load and
   compile switches needed by individual sites */
#ifndef LD_SWITCH_SITE
#define LD_SWITCH_SITE
#endif

#ifndef C_SWITCH_SITE
#define C_SWITCH_SITE
#endif

#ifndef ORDINARY_LINK

#ifndef CRT0_COMPILE
#define CRT0_COMPILE $(CC) -c $(ALL_CFLAGS) C_SWITCH_ASM 
#endif

#ifndef START_FILES
#ifdef NO_REMAP
#ifdef COFF_ENCAPSULATE
#define START_FILES pre-crt0.o /usr/local/lib/gcc-crt0.o
#else /* ! defined (COFF_ENCAPSULATE) */
#define START_FILES pre-crt0.o /lib/crt0.o
#endif /* ! defined (COFF_ENCAPSULATE) */
#else /* ! defined (NO_REMAP) */
#define START_FILES ecrt0.o
#endif /* ! defined (NO_REMAP) */
#endif /* START_FILES */
STARTFILES = START_FILES

#else /* ORDINARY_LINK */

/* config.h might want to force START_FILES anyway */
#ifdef START_FILES
STARTFILES = START_FILES
#endif /* START_FILES */

#endif /* not ORDINARY_LINK */


/* cc switches needed to make `asm' keyword work.
   Nothing special needed on most machines.  */
#ifndef C_SWITCH_ASM
#define C_SWITCH_ASM
#endif

/* Figure out whether the system cpp can handle long names.
   Do it by testing it right now.
   If it loses, arrange to use the GNU cpp.  */

#define LONGNAMEBBBFOOX
#ifdef LONGNAMEBBBARFOOX
/* Installed cpp fails to distinguish those names!  */
/* Arrange to compile the GNU cpp later on */
#define NEED_CPP
/* Cause cc to invoke the cpp that comes with Emacs,
   which will be in a file named localcpp.  */
MYCPPFLAG= -Blocal
/* LOCALCPP is the local one or nothing.
   CPP is the local one or the standardone.  */
LOCALCPP= localcpp
#endif /* ! defined (LONGNAMEBBBARFOOX) */

#ifdef SHORTNAMES
SHORT= shortnames
#endif

#ifdef USE_X_TOOLKIT
#define USE_MOTIF
TOOLKIT_DEFINES = -DUSE_MOTIF
#else
TOOLKIT_DEFINES =
#endif

/* DO NOT use -R.  There is a special hack described in lastfile.c
   which is used instead.  Some initialized data areas are modified
   at initial startup, then labeled as part of the text area when
   Emacs is dumped for the first time, and never changed again. */

/* -Demacs is needed to make some files produce the correct version
   for use in Emacs.

   -DHAVE_CONFIG_H is needed for some other files to take advantage of
   the information in `config.h'.  */

/* C_SWITCH_X_SITE must come before C_SWITCH_X_MACHINE and C_SWITCH_X_SYSTEM
   since it may have -I options that should override those two.  */
ALL_CFLAGS=-Demacs -DHAVE_CONFIG_H $(TOOLKIT_DEFINES) $(MYCPPFLAG) -I. -I${srcdir} C_SWITCH_MACHINE C_SWITCH_SYSTEM C_SWITCH_SITE C_SWITCH_X_SITE C_SWITCH_X_MACHINE C_SWITCH_X_SYSTEM ${CFLAGS}
.c.o:
	$(CC) -c $(CPPFLAGS) $(ALL_CFLAGS) $<

#ifndef LIBX10_MACHINE
#define LIBX10_MACHINE
#endif

#ifndef LIBX11_MACHINE
#define LIBX11_MACHINE
#endif

#ifndef LIBX10_SYSTEM
#define LIBX10_SYSTEM
#endif

#ifndef LIBX11_SYSTEM
#define LIBX11_SYSTEM
#endif

#ifndef LIB_X11_LIB
#define LIB_X11_LIB -lX11
#endif

#ifdef HAVE_X_WINDOWS
#ifdef HAVE_X_MENU

/* Include xmenu.o in the list of X object files.  */
XOBJ= xterm.o xfns.o xfaces.o xmenu.o xselect.o xrdb.o

/* The X Menu stuff is present in the X10 distribution, but missing
   from X11.  If we have X10, just use the installed library;
   otherwise, use our own copy.  */
#ifdef HAVE_X11
#ifdef USE_X_TOOLKIT
OLDXMENU=${lwlibdir}liblw.a
LIBXMENU= $(OLDXMENU)
#else /* not USE_X_TOOLKIT */
OLDXMENU= ${oldXMenudir}libXMenu11.a
LIBXMENU= $(OLDXMENU)
#endif /* not USE_X_TOOLKIT */
#else /* not HAVE_X11 */
LIBXMENU= -lXMenu
#endif /* not HAVE_X11 */

#else /* not HAVE_X_MENU */

/* Otherwise, omit xmenu.o from the list of X object files, and
   don't worry about the menu library at all.  */
XOBJ= xterm.o xfns.o xfaces.o xselect.o xrdb.o
LIBXMENU=
#endif /* not HAVE_X_MENU */

#ifdef USE_X_TOOLKIT
#define MOTIF
#if defined (LUCID) || defined (ATHENA)
LIBW= -lXaw
#endif
#ifdef MOTIF
#ifdef LIB_MOTIF
LIBW= LIB_MOTIF
#else
LIBW= -lXm
#endif
#endif
#ifdef OPEN_LOOK
LIBW= -lXol
#endif

#ifdef HAVE_X11XTR6
#ifdef NEED_LIBW
LIBXTR6 = -lSM -lICE -lw
#else
LIBXTR6 = -lSM -lICE
#endif
#endif

#ifndef LIBXMU
#define LIBXMU -lXmu
#endif

LIBXT= $(LIBW) LIBXMU -lXt $(LIBXTR6) -lXext
#else
LIBXT=
#endif

#ifdef HAVE_X11
/* LD_SWITCH_X_DEFAULT comes after everything else that specifies
   options for where to find X libraries, but before those libraries.  */
X11_LDFLAGS = LD_SWITCH_X_SITE LD_SWITCH_X_DEFAULT
LIBX= $(LIBXMENU) $(X11_LDFLAGS) $(LIBXT) LIB_X11_LIB LIBX11_MACHINE LIBX11_SYSTEM
#else /* not HAVE_X11 */
LIBX= $(LIBXMENU) LD_SWITCH_X_SITE -lX10 LIBX10_MACHINE LIBX10_SYSTEM
#endif /* not HAVE_X11 */
#endif /* not HAVE_X_WINDOWS */

#ifndef ORDINARY_LINK
/* Fix linking if compiled with GCC.  */
#ifdef __GNUC__

#if __GNUC__ > 1

#ifdef LINKER
#define LINKER_WAS_SPECIFIED
#endif

/* Versions of GCC >= 2.0 put their library, libgcc.a, in obscure
   places that are difficult to figure out at make time.  Fortunately,
   these same versions allow you to pass arbitrary flags on to the
   linker, so there's no reason not to use it as a linker.

   Well, it's not quite perfect.  The `-nostdlib' keeps GCC from
   searching for libraries in its internal directories, so we have to
   ask GCC explicitly where to find libgcc.a.  */

#ifndef LINKER
#define LINKER $(CC) -nostdlib
#endif

#ifndef LIB_GCC
/* Ask GCC where to find libgcc.a.  */
#define LIB_GCC `$(CC) -print-libgcc-file-name`
#endif /* not LIB_GCC */

GNULIB_VAR = LIB_GCC

#ifndef LINKER_WAS_SPECIFIED
/* GCC passes any argument prefixed with -Xlinker directly to the
   linker.  See prefix-args.c for an explanation of why we don't do
   this with the shell's `for' construct.
   Note that some people don't have '.'  in their paths, so we must
   use ./prefix-args.  */
#define YMF_PASS_LDFLAGS(flags) `./prefix-args -Xlinker flags`
#else
#define YMF_PASS_LDFLAGS(flags) flags
#endif

#else /* __GNUC__ < 2 */

#ifndef LIB_GCC
#define LIB_GCC /usr/local/lib/gcc-gnulib
#endif /* not LIB_GCC */
GNULIB_VAR = `if [ -f LIB_GCC ] ; then echo LIB_GCC; else echo; fi`
#endif /* __GNUC__ < 2 */
#else /* not __GNUC__ */
GNULIB_VAR = 

#endif /* not __GNUC__ */
#endif /* not ORDINARY_LINK */

/* Specify address for ld to start loading at,
   if requested by configuration.  */
#ifdef LD_TEXT_START_ADDR
STARTFLAGS = -T LD_TEXT_START_ADDR -e __start
#endif

#ifdef ORDINARY_LINK
LD = $(CC)
#else
#ifdef COFF_ENCAPSULATE
LD=$(CC) -nostdlib
#else /* not ORDINARY_LINK */
#ifdef LINKER
LD=LINKER
#else /* not LINKER */
LD=ld
#endif /* not LINKER */
#endif /* not COFF_ENCAPSULATE */
#endif /* not ORDINARY_LINK */

ALL_LDFLAGS = LD_SWITCH_SYSTEM LD_SWITCH_MACHINE LD_SWITCH_SITE $(LDFLAGS)

/* A macro which other sections of Makefile can redefine to munge the
   flags before they're passed to LD.  This is helpful if you have
   redefined LD to something odd, like "gcc".
   (The YMF prefix is a holdover from the old name "ymakefile".)
  */
#ifndef YMF_PASS_LDFLAGS
#define YMF_PASS_LDFLAGS(flags) flags
#endif

/* Allow config.h to specify a replacement file for unexec.c.  */
#ifndef UNEXEC
#define UNEXEC unexec.o
#endif
#ifndef UNEXEC_SRC
#define UNEXEC_SRC unexec.c
#endif

#ifdef USE_TEXT_PROPERTIES
#define INTERVAL_SRC intervals.h
#define INTERVAL_OBJ intervals.o textprop.o
#else
#define INTERVAL_SRC
#define INTERVAL_OBJ
#endif

#ifdef HAVE_GETLOADAVG
#define GETLOADAVG_OBJ
#else
#define GETLOADAVG_OBJ getloadavg.o
#endif

#ifdef HAVE_MKTIME
#define MKTIME_OBJ
#else
#define MKTIME_OBJ mktime.o
#endif

#ifdef MSDOS
#ifdef HAVE_X_WINDOWS
#define MSDOS_OBJ dosfns.o msdos.o
#else
#define MSDOS_OBJ dosfns.o msdos.o xfaces.o xmenu.o
#endif
#else
#define MSDOS_OBJ
#endif


/* lastfile must follow all files
   whose initialized data areas should be dumped as pure by dump-emacs. */
obj=    dispnew.o frame.o scroll.o xdisp.o window.o \
	cm.o term.o $(XOBJ) \
	emacs.o keyboard.o macros.o keymap.o sysdep.o \
	buffer.o filelock.o insdel.o marker.o INTERVAL_OBJ \
	minibuf.o fileio.o dired.o filemode.o \
	cmds.o casetab.o casefiddle.o indent.o search.o regex.o undo.o \
	alloc.o data.o doc.o editfns.o callint.o \
	eval.o floatfns.o fns.o print.o lread.o \
	abbrev.o syntax.o UNEXEC mocklisp.o bytecode.o \
	process.o callproc.o \
	region-cache.o \
	doprnt.o strftime.o MKTIME_OBJ GETLOADAVG_OBJ MSDOS_OBJ

/* Object files used on some machine or other.
   These go in the DOC file on all machines
   in case they are needed there.  */
SOME_MACHINE_OBJECTS = sunfns.o dosfns.o msdos.o intervals.o textprop.o \
  xterm.o xfns.o xfaces.o xmenu.o xselect.o xrdb.o


#ifdef TERMINFO
/* Used to be -ltermcap here.  If your machine needs that,
   define LIBS_TERMCAP in the m/*.h file.  */
#ifndef LIBS_TERMCAP
#define LIBS_TERMCAP -lcurses
#endif /* LIBS_TERMCAP */
termcapobj = terminfo.o
#else /* ! defined (TERMINFO) */
#ifndef LIBS_TERMCAP
#define LIBS_TERMCAP
termcapobj = termcap.o tparam.o
#else /* LIBS_TERMCAP */
termcapobj = tparam.o
#endif /* LIBS_TERMCAP */
#endif /* ! defined (TERMINFO) */


#ifndef SYSTEM_MALLOC

#ifdef GNU_MALLOC  /* New GNU malloc */
#ifdef REL_ALLOC
mallocobj = gmalloc.o ralloc.o vm-limit.o
#else /* ! defined (REL_ALLOC) */
mallocobj = gmalloc.o vm-limit.o
#endif /* ! defined (REL_ALLOC) */
#else /* Old GNU malloc */
mallocobj = malloc.o
#endif /* Old GNU malloc */

#endif /* SYSTEM_MALLOC */


#ifndef HAVE_ALLOCA
allocaobj = alloca.o
#else
allocaobj =
#endif

#ifdef USE_X_TOOLKIT
widgetobj= widget.o
#else /* not USE_X_TOOLKIT */
widgetobj=
#endif /* not USE_X_TOOLKIT */


/* define otherobj as list of object files that make-docfile
   should not be told about.  */
otherobj= $(termcapobj) lastfile.o $(mallocobj) $(allocaobj) $(widgetobj)

#ifdef HAVE_FACES
#define FACE_SUPPORT ${lispsource}faces.elc ${lispsource}facemenu.elc
#else
#define FACE_SUPPORT
#endif

#ifdef LISP_FLOAT_TYPE
#define FLOAT_SUPPORT ${lispsource}float-sup.elc
#else
#define FLOAT_SUPPORT
#endif

#ifdef MULTI_FRAME
#define FRAME_SUPPORT ${lispsource}frame.elc
#else
#define FRAME_SUPPORT
#endif

#ifdef HAVE_MOUSE
#define MOUSE_SUPPORT ${lispsource}menu-bar.elc ${lispsource}mouse.elc \
  ${lispsource}select.elc ${lispsource}scroll-bar.elc
#else
#define MOUSE_SUPPORT
#endif

#ifdef HAVE_X_WINDOWS
#define X_WINDOWS_SUPPORT
#else
#define X_WINDOWS_SUPPORT
#endif

#ifdef VMS
#define VMS_SUPPORT ${lispsource}vmsproc.elc ${lispsource}vms-patch.elc
#else
#define VMS_SUPPORT
#endif

#ifdef MSDOS
#define MSDOS_SUPPORT ${lispsource}ls-lisp.elc ${lispsource}disp-tab.elc ${lispsource}dos-fns.elc
#else
#define MSDOS_SUPPORT
#endif

#ifdef WINDOWSNT
#define WINNT_SUPPORT ${lispsource}ls-lisp.elc ${lispsource}winnt.elc
#else
#define WINNT_SUPPORT
#endif

/* List of Lisp files loaded into the dumped Emacs.  It's arranged
   like this because it's easier to generate it semi-mechanically from
   loadup.el this way.

   Note that this list should not include lisp files which might not
   be present, like site-load.el and site-init.el; this makefile
   expects them all to be either present or buildable.  */
lisp= \
	${lispsource}abbrev.elc \
	${lispsource}buff-menu.elc \
	${lispsource}byte-run.elc \
	${lispsource}c-mode.elc \
	${lispsource}files.elc \
	${lispsource}fill.elc \
	${lispsource}format.elc \
	FACE_SUPPORT \
	MOUSE_SUPPORT \
	FLOAT_SUPPORT \
	FRAME_SUPPORT \
	X_WINDOWS_SUPPORT \
	${lispsource}help.elc \
	${lispsource}indent.elc \
	${lispsource}isearch.elc \
	${lispsource}lisp-mode.elc \
	${lispsource}lisp.elc \
	${lispsource}loadup.el \
	${lispsource}loaddefs.el \
	${lispsource}map-ynp.elc \
	${lispsource}page.elc \
	${lispsource}paragraphs.elc \
	${lispsource}paths.el \
	${lispsource}register.elc \
	${lispsource}replace.elc \
	${lispsource}simple.elc \
	${lispsource}startup.elc \
	${lispsource}subr.elc \
	${lispsource}text-mode.elc \
	${lispsource}vc-hooks.elc \
	${lispsource}ediff-hook.elc \
	VMS_SUPPORT \
	MSDOS_SUPPORT \
	WINNT_SUPPORT \
	${lispsource}window.elc \
	${lispsource}version.el

/* Lisp files that may or may not be used.
   We must unconditionally put them in the DOC file.  */
SOME_MACHINE_LISP =  ${lispsource}faces.elc ${lispsource}facemenu.elc \
  ${lispsource}float-sup.elc ${lispsource}frame.elc \
  ${lispsource}menu-bar.elc ${lispsource}mouse.elc \
  ${lispsource}select.elc ${lispsource}scroll-bar.elc \
  ${lispsource}vmsproc.elc ${lispsource}vms-patch.elc \
  ${lispsource}ls-lisp.elc ${lispsource}dos-fns.elc \
  ${lispsource}winnt.elc

/* Construct full set of libraries to be linked.
   Note that SunOS needs -lm to come before -lc; otherwise, you get
   duplicated symbols.  If the standard libraries were compiled
   with GCC, we might need gnulib again after them.  */
LIBES = $(LIBX) LIBS_SYSTEM LIBS_MACHINE LIBS_TERMCAP \
   LIBS_DEBUG $(GNULIB_VAR) LIB_MATH LIB_STANDARD $(GNULIB_VAR)

/* Enable recompilation of certain other files depending on system type.  */

#ifndef OTHER_FILES
#define OTHER_FILES
#endif

/* Enable inclusion of object files in temacs depending on system type.  */
#ifndef OBJECTS_SYSTEM
#define OBJECTS_SYSTEM
#endif

#ifndef OBJECTS_MACHINE
#define OBJECTS_MACHINE
#endif

all: emacs OTHER_FILES

emacs: temacs ${etc}DOC ${lisp}
#ifdef CANNOT_DUMP
	rm -f emacs
	ln temacs emacs
#else
#ifdef HAVE_SHM
	./temacs -nl -batch -l loadup dump
#else /* ! defined (HAVE_SHM) */
	./temacs -batch -l loadup dump
#endif /* ! defined (HAVE_SHM) */
#endif /* ! defined (CANNOT_DUMP) */

/* We run make-docfile twice because the command line may get too long
   on some systems.  */
/* ${SOME_MACHINE_OBJECTS} comes before ${obj} because some files may
   or may not be included in ${obj}, but they are always included in
   ${SOME_MACHINE_OBJECTS}.  Since a file is processed when it is mentioned
   for the first time, this prevents any variation between configurations
   in the contents of the DOC file.
   Likewise for ${SOME_MACHINE_LISP}.  */
${etc}DOC: ${libsrc}make-docfile ${obj} ${lisp}
	-rm -f ${etc}DOC
	${libsrc}make-docfile -d ${srcdir} ${SOME_MACHINE_OBJECTS} ${obj} > ${etc}DOC
	${libsrc}make-docfile -a ${etc}DOC -d ${srcdir} ${SOME_MACHINE_LISP} ${lisp}

${libsrc}make-docfile:
	cd ${libsrc}; ${MAKE} ${MFLAGS} make-docfile

/* Some systems define this to cause parallel Make-ing.  */
#ifndef MAKE_PARALLEL
#define MAKE_PARALLEL
#endif

temacs: MAKE_PARALLEL $(LOCALCPP) $(SHORT) $(STARTFILES) $(OLDXMENU) ${obj} ${otherobj} OBJECTS_SYSTEM OBJECTS_MACHINE prefix-args
	$(LD) YMF_PASS_LDFLAGS (${STARTFLAGS} ${ALL_LDFLAGS}) \
    -o temacs ${STARTFILES} ${obj} ${otherobj}  \
    OBJECTS_SYSTEM OBJECTS_MACHINE ${LIBES}

/* We don't use ALL_LDFLAGS because LD_SWITCH_SYSTEM and LD_SWITCH_MACHINE
   often contain options that have to do with using Emacs's crt0,
   which are only good with temacs.  */
prefix-args: prefix-args.c $(config_h)
	$(CC) $(ALL_CFLAGS) $(LDFLAGS) ${srcdir}/prefix-args.c -o prefix-args

/* These are needed for C compilation, on the systems that need them */
#ifdef NEED_CPP
CPP = ./localcpp
localcpp:
	cd ${cppdir}; ${MAKE} ${MFLAGS} EMACS=-DEMACS
	ln ${cppdir}cpp localcpp  /* Name where ALL_CFLAGS will refer to it */
/* cc appears to be cretinous and require all of these to exist
   if -B is specified -- we can't use one local pass and let the
   others be the standard ones.  What a loser.
   We can't even use ln, since they are probably
   on different disks.  */
	cp /lib/ccom localccom
	-cp /lib/optim localoptim
	-cp /lib/c2 localc2
	cp /bin/as localas
#else /* ! defined (NEED_CPP) */
CPP = $(CC) -E
#endif /* ! defined (NEED_CPP) */

#ifdef SHORTNAMES
shortnames:
	cd ${shortnamesdir}; ${MAKE} ${MFLAGS}
#endif

/* Don't lose if this was not defined.  */
#ifndef OLDXMENU_OPTIONS
#define OLDXMENU_OPTIONS
#endif

/* Don't lose if this was not defined.  */
#ifndef LWLIB_OPTIONS
#define LWLIB_OPTIONS
#endif

#ifdef HAVE_X_WINDOWS
#ifdef HAVE_X11
#ifdef USE_X_TOOLKIT
$(OLDXMENU): really-lwlib

/* Encode the values of these two macros in Make variables,
   so we can use $(...) to substitute their values within "...".  */
C_SWITCH_MACHINE_1 = C_SWITCH_MACHINE
C_SWITCH_SYSTEM_1 = C_SWITCH_SYSTEM
C_SWITCH_SITE_1 = C_SWITCH_SITE
C_SWITCH_X_SITE_1 = C_SWITCH_X_SITE
C_SWITCH_X_MACHINE_1 = C_SWITCH_X_MACHINE
C_SWITCH_X_SYSTEM_1 = C_SWITCH_X_SYSTEM
really-lwlib:
	cd ${lwlibdir}; ${MAKE} ${MFLAGS} LWLIB_OPTIONS \
    CC='${CC}' CFLAGS='${CFLAGS}' MAKE='${MAKE}' \
    "C_SWITCH_X_SITE=$(C_SWITCH_X_SITE_1)" \
    "C_SWITCH_X_MACHINE=$(C_SWITCH_X_MACHINE_1)" \
    "C_SWITCH_X_SYSTEM=$(C_SWITCH_X_SYSTEM_1)" \
    "C_SWITCH_SITE=$(C_SWITCH_SITE_1)" \
    "C_SWITCH_MACHINE=$(C_SWITCH_MACHINE_1)" \
    "C_SWITCH_SYSTEM=$(C_SWITCH_SYSTEM_1)"
	@true  /* make -t should not create really-lwlib.  */
.PHONY: really-lwlib
#else /* not USE_X_TOOLKIT */
#ifdef HAVE_X_MENU
$(OLDXMENU): really-oldXMenu

/* Encode the values of these two macros in Make variables,
   so we can use $(...) to substitute their values within "...".  */
C_SWITCH_MACHINE_1 = C_SWITCH_MACHINE
C_SWITCH_SYSTEM_1 = C_SWITCH_SYSTEM
C_SWITCH_SITE_1 = C_SWITCH_SITE
C_SWITCH_X_SITE_1 = C_SWITCH_X_SITE
C_SWITCH_X_MACHINE_1 = C_SWITCH_X_MACHINE
C_SWITCH_X_SYSTEM_1 = C_SWITCH_X_SYSTEM
really-oldXMenu:
	cd ${oldXMenudir}; ${MAKE} ${MFLAGS} OLDXMENU_OPTIONS \
    CC='${CC}' CFLAGS='${CFLAGS}' MAKE='${MAKE}' \
    "C_SWITCH_X_SITE=$(C_SWITCH_X_SITE_1)" \
    "C_SWITCH_X_MACHINE=$(C_SWITCH_X_MACHINE_1)" \
    "C_SWITCH_X_SYSTEM=$(C_SWITCH_X_SYSTEM_1)" \
    "C_SWITCH_SITE=$(C_SWITCH_SITE_1)" \
    "C_SWITCH_MACHINE=$(C_SWITCH_MACHINE_1)" \
    "C_SWITCH_SYSTEM=$(C_SWITCH_SYSTEM_1)"
	@true  /* make -t should not create really-oldXMenu.  */
.PHONY: really-oldXMenu
#endif /* HAVE_X_MENU */
#endif /* not USE_X_TOOLKIT */
#endif /* HAVE_X11 */
#endif /* HAVE_X_WINDOWS */

paths.h: paths.in
	@echo "The file paths.h needs to be set up from paths.in."
	@echo "Consult the file \`INSTALL' for instructions for building Emacs."
	exit 1

config.h: config.in
	@echo "The file config.h needs to be set up from config.in."
	@echo "Consult the file \`INSTALL' for instructions for building Emacs."
	exit 1

/* Some machines have alloca built-in.
   They should define HAVE_ALLOCA, or may just let alloca.s
   be used but generate no code.
   Some have it written in assembler in alloca.s.
   Some use the C version in alloca.c (these define C_ALLOCA in config.h).
   */

#ifdef C_ALLOCA
/* We could put something in alloca.c to #define free and malloc
   whenever emacs was #defined, but that's not appropriate for all
   users of alloca in Emacs.  Check out ../lib-src/getopt.c.  */
alloca.o : alloca.c
	$(CC) -c $(CPPFLAGS) -Dfree=xfree -Dmalloc=xmalloc \
	$(ALL_CFLAGS) ${srcdir}/alloca.c
#else
#ifndef HAVE_ALLOCA
alloca.o : alloca.s $(config_h)
/* $(CPP) is cc -E, which may get confused by filenames
   that do not end in .c.  So copy file to a safe name.  */
	-rm -f allocatem.c
	cp ${srcdir}/alloca.s allocatem.c
/* Remove any ^L, blank lines, and preprocessor comments,
   since some assemblers barf on them.  Use a different basename for the
   output file, since some stupid compilers (Green Hill's) use that
   name for the intermediate assembler file. */
	$(CPP) $(CPPFLAGS) $(ALL_CFLAGS) allocatem.c | \
	sed -e 's///' -e 's/^#.*//' | \
	sed -n -e '/^..*$$/p' > allocax.s
	-rm -f alloca.o
/* Xenix, in particular, needs to run assembler via cc.  */
	$(CC) -c allocax.s
	mv allocax.o alloca.o
	-rm -f allocax.s allocatem.c
#endif /* HAVE_ALLOCA */
#endif /* ! defined (C_ALLOCA) */

/* Nearly all the following files depend on lisp.h,
   but it is not included as a dependency because
   it is so often changed in ways that do not require any recompilation
   and so rarely changed in ways that do require any.  */

abbrev.o: abbrev.c buffer.h commands.h $(config_h) 
buffer.o: buffer.c buffer.h region-cache.h commands.h window.h \
   INTERVAL_SRC blockinput.h $(config_h) 
callint.o: callint.c window.h commands.h buffer.h mocklisp.h \
   keyboard.h $(config_h)
callproc.o: callproc.c paths.h buffer.h commands.h $(config_h) \
	process.h systty.h syssignal.h
casefiddle.o: casefiddle.c syntax.h commands.h buffer.h $(config_h) 
casetab.o: casetab.c buffer.h $(config_h)
cm.o: cm.c cm.h termhooks.h $(config_h)
cmds.o: cmds.c syntax.h buffer.h commands.h window.h $(config_h) 
pre-crt0.o: pre-crt0.c
ecrt0.o: ecrt0.c $(config_h)
	CRT0_COMPILE ${srcdir}/ecrt0.c
dired.o: dired.c commands.h buffer.h $(config_h) regex.h
dispnew.o: dispnew.c commands.h frame.h window.h buffer.h dispextern.h \
   termchar.h termopts.h termhooks.h cm.h disptab.h systty.h systime.h \
   xterm.h $(config_h)
doc.o: doc.c $(config_h) paths.h buffer.h keyboard.h
doprnt.o: doprnt.c $(config_h)
dosfns.o: buffer.h termchar.h termhooks.h frame.h msdos.h dosfns.h $(config_h)
editfns.o: editfns.c window.h buffer.h systime.h INTERVAL_SRC $(config_h) 
emacs.o: emacs.c commands.h systty.h syssignal.h process.h INTERVAL_SRC $(config_h) 
fileio.o: fileio.c window.h buffer.h systime.h INTERVAL_SRC $(config_h) 
filelock.o: filelock.c buffer.h paths.h $(config_h)
filemode.o: filemode.c  $(config_h)
getloadavg.o: getloadavg.c $(config_h)
indent.o: indent.c frame.h window.h indent.h buffer.h $(config_h) termchar.h \
   termopts.h disptab.h region-cache.h
insdel.o: insdel.c window.h buffer.h INTERVAL_SRC blockinput.h $(config_h)
keyboard.o: keyboard.c termchar.h termhooks.h termopts.h buffer.h \
   commands.h frame.h window.h macros.h disptab.h keyboard.h syssignal.h \
   systty.h systime.h dispextern.h intervals.h blockinput.h xterm.h $(config_h)
keymap.o: keymap.c buffer.h commands.h keyboard.h termhooks.h blockinput.h \
   $(config_h) 
lastfile.o: lastfile.c  $(config_h)
macros.o: macros.c window.h buffer.h commands.h macros.h keyboard.h $(config_h)
malloc.o: malloc.c $(config_h)
msdos.o: msdos.c msdos.h dosfns.h systime.h termhooks.h dispextern.h \
   termopts.h frame.h window.h $(config_h)
mktime.o: mktime.c $(config_h)
gmalloc.o: gmalloc.c $(config_h)
ralloc.o: ralloc.c $(config_h)
vm-limit.o: vm-limit.c mem-limits.h $(config_h) 
marker.o: marker.c buffer.h $(config_h) 
minibuf.o: minibuf.c syntax.h dispextern.h frame.h window.h \
   buffer.h commands.h $(config_h) 
mocklisp.o: mocklisp.c buffer.h $(config_h)
process.o: process.c process.h buffer.h window.h termhooks.h termopts.h \
   commands.h syssignal.h systime.h systty.h syswait.h frame.h $(config_h) 
regex.o: regex.c syntax.h buffer.h $(config_h) regex.h 
region-cache.o: region-cache.c buffer.h region-cache.h
frame.o: frame.c xterm.h window.h frame.h termhooks.h commands.h keyboard.h \
   buffer.h $(config_h)
scroll.o: scroll.c termchar.h dispextern.h frame.h $(config_h)
search.o: search.c regex.h commands.h buffer.h region-cache.h syntax.h \
   blockinput.h $(config_h) 
strftime.o: strftime.c $(config_h)
	$(CC) -c $(CPPFLAGS) $(ALL_CFLAGS) -Dstrftime=emacs_strftime $<
syntax.o: syntax.c syntax.h buffer.h commands.h $(config_h) 
sysdep.o: sysdep.c $(config_h) dispextern.h termhooks.h termchar.h termopts.h \
   frame.h syssignal.h systty.h systime.h syswait.h blockinput.h window.h
term.o: term.c termchar.h termhooks.h termopts.h $(config_h) cm.h frame.h \
   disptab.h keyboard.h
termcap.o: termcap.c $(config_h)
terminfo.o: terminfo.c $(config_h)
tparam.o: tparam.c $(config_h)
undo.o: undo.c buffer.h commands.h $(config_h) 
/* This hack is to discard any space that cpp might put at the beginning
   of UNEXEC when substituting it in.  */
UNEXEC_ALIAS=UNEXEC
$(UNEXEC_ALIAS): UNEXEC_SRC $(config_h)
widget.o: widget.c xterm.h frame.h dispextern.h widgetprv.h $(config_h)
window.o: window.c indent.h commands.h frame.h window.h buffer.h termchar.h \
   termhooks.h disptab.h keyboard.h $(config_h)
xdisp.o: xdisp.c macros.h commands.h indent.h buffer.h dispextern.h \
   termchar.h frame.h window.h disptab.h termhooks.h $(config_h) 
xfaces.o: xfaces.c dispextern.h frame.h xterm.h buffer.h blockinput.h \
   window.h $(config_h) 
xfns.o: xfns.c buffer.h frame.h window.h keyboard.h xterm.h \
   blockinput.h paths.h $(config_h)
xmenu.o: xmenu.c xterm.h window.h dispextern.h frame.h keyboard.h \
   blockinput.h puresize.h msdos.h $(config_h)
xterm.o: xterm.c xterm.h termhooks.h termopts.h termchar.h window.h \
  dispextern.h frame.h disptab.h blockinput.h systime.h syssignal.h \
  keyboard.h gnu.h sink.h sinkmask.h $(config_h)
xselect.o: xselect.c dispextern.h frame.h xterm.h blockinput.h $(config_h) 
xrdb.o: xrdb.c $(config_h)
hftctl.o: hftctl.c $(config_h)

/* The files of Lisp proper */

alloc.o: alloc.c frame.h window.h buffer.h  puresize.h syssignal.h keyboard.h \
 blockinput.h $(config_h) INTERVAL_SRC
bytecode.o: bytecode.c buffer.h syntax.h $(config_h)
data.o: data.c buffer.h puresize.h syssignal.h keyboard.h $(config_h)
eval.o: eval.c commands.h keyboard.h blockinput.h $(config_h)
floatfns.o: floatfns.c $(config_h)
fns.o: fns.c commands.h $(config_h) frame.h buffer.h keyboard.h INTERVAL_SRC
print.o: print.c process.h frame.h window.h buffer.h keyboard.h $(config_h)
lread.o: lread.c commands.h keyboard.h buffer.h paths.h $(config_h) \
 termhooks.h

/* Text properties support */
textprop.o: textprop.c buffer.h intervals.h $(config_h)
intervals.o: intervals.c buffer.h intervals.h keyboard.h puresize.h $(config_h)

/* System-specific programs to be made.
   OTHER_FILES, OBJECTS_SYSTEM and OBJECTS_MACHINE
   select which of these should be compiled.  */

sunfns.o: sunfns.c buffer.h $(config_h)

${libsrc}emacstool: ${libsrc}emacstool.c
	cd ${libsrc}; ${MAKE} ${MFLAGS} emacstool
mostlyclean:
	rm -f temacs prefix-args core \#* *.o libXMenu11.a liblw.a
	rm -f ../etc/DOC
clean: mostlyclean
	rm -f emacs-* emacs
/**/# This is used in making a distribution.
/**/# Do not use it on development directories!
distclean: clean
	rm -f paths.h config.h Makefile Makefile.c ../etc/DOC-*
maintainer-clean: distclean
	@echo "This command is intended for maintainers to use;"
	@echo "it deletes files that may require special tools to rebuild."
	rm -f TAGS
versionclean:
	-rm -f emacs emacs-* ../etc/DOC*
extraclean: distclean
	-rm -f *~ \#* m/?*~ s/?*~

/* The rule for the [sm] files has to be written a little funny to
   avoid looking like a C comment to CPP. */
SOURCES = *.[ch] [sm]/?* COPYING Makefile.in \
	config.in paths.in README COPYING ChangeLog vms.pp-trans
unlock:
	chmod u+w $(SOURCES)

relock:
	chmod -w $(SOURCES)
	chmod +w paths.h

/* Arrange to make a tags table in ../lisp, and another in this dir
   which includes ../lisp/TAGS by reference.  */
ctagsfiles = [a-zA-Z]*.[hc]
lisptagsfiles = ../lisp/[a-zA-Z]*.el
TAGS: $(ctagsfiles)
	export DIR; DIR=`pwd`; cd ${srcdir}; \
	$$DIR/../lib-src/etags --include=../lisp/TAGS \
	--regex='/[ 	]*DEFVAR_[A-Z_ 	(]+"\([^"]+\)"/' $(ctagsfiles)
${lispsource}TAGS: ${lispsource}$(lisptagsfiles)
	cd ${lispsource}; $(MAKE) TAGS
tags: TAGS ${lispsource}TAGS
.PHONY: tags

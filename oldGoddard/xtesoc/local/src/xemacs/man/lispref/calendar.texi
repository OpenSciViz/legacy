@c -*-texinfo-*-
@c This is part of the GNU Emacs Lisp Reference Manual.
@c Copyright (C) 1990, 1991, 1992, 1993 Free Software Foundation, Inc. 
@c See the file elisp.texi for copying conditions.
@node Calendar, Tips, Display, Top
@chapter Customizing the Calendar and Diary

  There are many customizations that you can use to make the calendar and
diary suit your personal tastes.

@menu
* Calendar Customizing::   Defaults you can set.
* Holiday Customizing::    Defining your own holidays.
* Date Display Format::    Changing the format.
* Time Display Format::    Changing the format.
* Daylight Savings::       Changing the default.
* Diary Customizing::      Defaults you can set.
* Hebrew/Islamic Entries:: How to obtain them.
* Fancy Diary Display::    Enhancing the diary display, sorting entries, 
                             using included diary files.
* Sexp Diary Entries::     Fancy things you can do.
* Appt Customizing::	   Customizing appointment reminders.
@end menu

@node Calendar Customizing
@section Customizing the Calendar
@vindex view-diary-entries-initially

  If you set the variable @code{view-diary-entries-initially} to
@code{t}, calling up the calendar automatically displays the diary
entries for the current date as well.  The diary dates appear only if
the current date is visible.  If you add both of the following lines to
your @file{.emacs} file:@refill

@example
(setq view-diary-entries-initially t)
(calendar)
@end example

@noindent
this displays both the calendar and diary windows whenever you start Emacs.

@vindex view-calendar-holidays-initially
  Similarly, if you set the variable
@code{view-calendar-holidays-initially} to @code{t}, entering the
calendar automatically displays a list of holidays for the current
three-month period.  The holiday list appears in a separate
window.

@vindex mark-diary-entries-in-calendar
  You can set the variable @code{mark-diary-entries-in-calendar} to
@code{t} in order to mark any dates with diary entries.  This takes
effect whenever the calendar window contents are recomputed.  There are
two ways of marking these dates: by changing the face (@pxref{Faces}),
if the display supports that, or by placing a plus sign (@samp{+})
beside the date otherwise.

@vindex mark-holidays-in-calendar
  Similarly, setting the variable @code{mark-holidays-in-calendar} to
@code{t} marks holiday dates, either with a change of face or with an
asterisk (@samp{*}).

@vindex calendar-holiday-marker
@vindex diary-entry-marker
  The variable @code{calendar-holiday-marker} specifies how to mark a
date as being a holiday.  Its value may be a character to insert next to
the date, or a face name to use for displaying the date.  Likewise, the
variable @code{diary-entry-marker} specifies how to mark a date that has
diary entries.  The calendar creates faces named @code{holiday-face} and
@code{diary-face} for these purposes; those symbols are the default
values of these variables, when Emacs supports multiple faces on your
terminal.

@vindex calendar-load-hook
  The variable @code{calendar-load-hook} is a normal hook run when the
calendar package is first loaded (before actually starting to display
the calendar).

@vindex initial-calendar-window-hook
  Starting the calendar runs the normal hook
@code{initial-calendar-window-hook}.  Recomputation of the calendar
display does not run this hook.  But if you leave the calendar with the
@kbd{q} command and reenter it, the hook runs again.@refill

@vindex today-visible-calendar-hook
  The variable @code{today-visible-calendar-hook} is a normal hook run
after the calendar buffer has been prepared with the calendar when the
current date is visible in the window.  One use of this hook is to
replace today's date with asterisks; to do that, use the hook function
@code{calendar-star-date}.

@findex calendar-star-date
@example
(add-hook 'today-visible-calendar-hook 'calendar-star-date)
@end example

@noindent
Another standard hook function marks the current date, either by
changing its face or by adding an asterisk.  Here's how to use it:

@findex calendar-mark-today
@example
(add-hook 'today-visible-calendar-hook 'calendar-mark-today)
@end example

@noindent
@vindex calendar-today-marker
The variable @code{calendar-today-marker} specifies how to mark today's
date.  Its value should be a character to insert next to the date or a
face name to use for displaying the date.  A face named
@code{calendar-today-face} is provided for this purpose; that symbol is
the default for this variable when Emacs supports multiple faces on your
terminal.

@vindex today-invisible-calendar-hook
@noindent
  A similar normal hook, @code{today-invisible-calendar-hook} is run if
the current date is @emph{not} visible in the window.

@node Holiday Customizing
@section Customizing the Holidays

@vindex calendar-holidays
@vindex christian-holidays
@vindex hebrew-holidays
@vindex islamic-holidays
  Emacs knows about holidays defined by entries on one of several lists.
You can customize these lists of holidays to your own needs, adding or
deleting holidays.  The lists of holidays that Emacs uses are for
general holidays (@code{general-holidays}), local holidays
(@code{local-holidays}), Christian holidays (@code{christian-holidays}),
Hebrew (Jewish) holidays (@code{hebrew-holidays}), Islamic (Moslem)
holidays (@code{islamic-holidays}), and other holidays
(@code{other-holidays}).

@vindex general-holidays
  The general holidays are, by default, holidays common throughout the
United States.  To eliminate these holidays, set @code{general-holidays}
to @code{nil}.

@vindex local-holidays
  There are no default local holidays (but sites may supply some).  You
can set the variable @code{local-holidays} to any list of holidays, as
described below.

@vindex all-christian-calendar-holidays
@vindex all-hebrew-calendar-holidays
@vindex all-islamic-calendar-holidays
  By default, Emacs does not include all the holidays of the religions
that it knows, only those commonly found in secular calendars.  For a
more extensive collection of religious holidays, you can set any (or
all) of the variables @code{all-christian-calendar-holidays},
@code{all-hebrew-calendar-holidays}, or
@code{all-islamic-calendar-holidays} to @code{t}.  If you want to
eliminate the religious holidays, set any or all of the corresponding
variables @code{christian-holidays}, @code{hebrew-holidays}, and
@code{islamic-holidays} to @code{nil}.@refill

@vindex other-holidays
  You can set the variable @code{other-holidays} to any list of
holidays.  This list, normally empty, is intended for individual use.

@cindex holiday forms
  Each of the lists (@code{general-holidays}, @code{local-holidays},
@code{christian-holidays}, @code{hebrew-holidays},
@code{islamic-holidays}, and @code{other-holidays}) is a list of
@dfn{holiday forms}, each holiday form describing a holiday (or
sometimes a list of holidays).

  Here is a table of the possible kinds of holiday form.  Day numbers
and month numbers count starting from 1, but ``dayname'' numbers
count Sunday as 0.  The element @var{string} is always the
name of the holiday, as a string.

@table @code
@item (holiday-fixed @var{month} @var{day} @var{string})
A fixed date on the Gregorian calendar.

@item (holiday-float @var{month} @var{dayname} @var{k} @var{string})
The @var{k}th @var{dayname} in @var{month} on the Gregorian calendar
(@var{dayname}=0 for Sunday, and so on); negative @var{k} means count back
from the end of the month.

@item (holiday-hebrew @var{month} @var{day} @var{string})
A fixed date on the Hebrew calendar.

@item (holiday-islamic @var{month} @var{day} @var{string})
A fixed date on the Islamic calendar.

@item (holiday-julian @var{month} @var{day} @var{string})
A fixed date on the Julian calendar.

@item (holiday-sexp @var{sexp} @var{string})
A date calculated by the Lisp expression @var{sexp}.  The expression
should use the variable @code{year} to compute and return the date of a
holiday, or @code{nil} if the holiday doesn't happen this year.  The
value of @var{sexp} must represent the date as a list of the form
@code{(@var{month} @var{day} @var{year})}.

@item (if @var{condition} @var{holiday-form})
A holiday that happens only if @var{condition} is true.

@item (@var{function} @r{[}@var{args}@r{]})
A list of dates calculated by the function @var{function}, called with
arguments @var{args}.
@end table

  For example, suppose you want to add Bastille Day, celebrated in
France on July 14.  You can do this as follows:

@smallexample
(setq other-holidays '((holiday-fixed 7 14 "Bastille Day")))
@end smallexample

@noindent
The holiday form @code{(holiday-fixed 7 14 "Bastille Day")} specifies the
fourteenth day of the seventh month (July).

  Many holidays occur on a specific day of the week, at a specific time
of month.  Here is a holiday form describing Hurricane Supplication Day,
celebrated in the Virgin Islands on the fourth Monday in August:

@smallexample
(holiday-float 8 1 4 "Hurricane Supplication Day")
@end smallexample

@noindent
Here the 8 specifies August, the 1 specifies Monday (Sunday is 0,
Tuesday is 2, and so on), and the 4 specifies the fourth occurrence in
the month (1 specifies the first occurrence, 2 the second occurrence,
@minus{}1 the last occurrence, @minus{}2 the second-to-last occurrence, and
so on).

  You can specify holidays that occur on fixed days of the Hebrew,
Islamic, and Julian calendars too.  For example,

@smallexample
(setq other-holidays
      '((holiday-hebrew 10 2 "Last day of Hanukkah")
        (holiday-islamic 3 12 "Mohammed's Birthday")
        (holiday-julian 4 2 "Jefferson's Birthday")))
@end smallexample

@noindent
adds the last day of Hanukkah (since the Hebrew months are numbered with
1 starting from Nisan), the Islamic feast celebrating Mohammed's
birthday (since the Islamic months are numbered from 1 starting with
Muharram), and Thomas Jefferson's birthday, which is 2 April 1743 on the
Julian calendar.

  To include a holiday conditionally, use either Emacs Lisp's @code{if} or the
@code{holiday-sexp} form.  For example, American presidential elections
occur on the first Tuesday after the first Monday in November of years
divisible by 4:

@smallexample
(holiday-sexp (if (= 0 (% year 4))
                   (calendar-gregorian-from-absolute
                    (1+ (calendar-dayname-on-or-before
                          1 (+ 6 (calendar-absolute-from-gregorian
                                  (list 11 1 year))))))
              "US Presidential Election"))
@end smallexample

@noindent
or

@smallexample
(if (= 0 (% displayed-year 4))
    (fixed 11
           (extract-calendar-day
             (calendar-gregorian-from-absolute
               (1+ (calendar-dayname-on-or-before
                     1 (+ 6 (calendar-absolute-from-gregorian
                              (list 11 1 displayed-year)))))))
           "US Presidential Election"))
@end smallexample

  Some holidays just don't fit into any of these forms because special
calculations are involved in their determination.  In such cases you
must write a Lisp function to do the calculation.  To include eclipses,
for example, add @code{(eclipses)} to @code{other-holidays}
and write an Emacs Lisp function @code{eclipses} that returns a
(possibly empty) list of the relevant Gregorian dates among the range
visible in the calendar window, with descriptive strings, like this:

@smallexample
(((6 27 1991) "Lunar Eclipse") ((7 11 1991) "Solar Eclipse") ... )
@end smallexample

@node Date Display Format
@section Date Display Format
@vindex calendar-date-display-form

  You can customize the manner of displaying dates in the diary, in mode
lines, and in messages by setting @code{calendar-date-display-form}.
This variable holds a list of expressions that can involve the variables
@code{month}, @code{day}, and @code{year}, which are all numbers in
string form, and @code{monthname} and @code{dayname}, which are both
alphabetic strings.  In the American style, the default value of this
list is as follows:

@smallexample
((if dayname (concat dayname ", ")) monthname " " day ", " year)
@end smallexample

@noindent
while in the European style this value is the default:

@smallexample
((if dayname (concat dayname ", ")) day " " monthname " " year)
@end smallexample

@noindent
The ISO standard date representation is this:

@smallexample
(year "-" month "-" day)
@end smallexample

@noindent
This specifies a typical American format:

@smallexample
(month "/" day "/" (substring year -2))
@end smallexample

@node Time Display Format
@section Time Display Format
@vindex calendar-time-display-form

  The calendar and diary by default display times of day in the
conventional American style with the hours from 1 through 12, minutes,
and either @samp{am} or @samp{pm}.  If you prefer the European style,
also known in the US as military, in which the hours go from 00 to 23,
you can alter the variable @code{calendar-time-display-form}.  This
variable is a list of expressions that can involve the variables
@code{12-hours}, @code{24-hours}, and @code{minutes}, which are all
numbers in string form, and @code{am-pm} and @code{time-zone}, which are
both alphabetic strings.  The default value of
@code{calendar-time-display-form} is as follows:

@smallexample
(12-hours ":" minutes am-pm
          (if time-zone " (") time-zone (if time-zone ")"))
@end smallexample

@noindent
Here is a value that provides European style times:

@smallexample
(24-hours ":" minutes
          (if time-zone " (") time-zone (if time-zone ")"))
@end smallexample

@node Daylight Savings
@section Daylight Savings Time
@cindex daylight savings time

  Emacs understands the difference between standard time and daylight
savings time---the times given for sunrise, sunset, solstices,
equinoxes, and the phases of the moon take that into account.  The rules
for daylight savings time vary from place to place and have also varied
historically from year to year.  To do the job properly, Emacs needs to
know which rules to use.

  Some operating systems keep track of the rules that apply to the place
where you are; on these systems, Emacs gets the information it needs
from the system automatically.  If some or all of this information is
missing, Emacs fills in the gaps with the rules currently used in
Cambridge, Massachusetts, which is the center of GNU's world.


@vindex calendar-daylight-savings-starts
@vindex calendar-daylight-savings-ends
  If the default choice of rules is not appropriate for your location,
you can tell Emacs the rules to use by setting the variables
@code{calendar-daylight-savings-starts} and
@code{calendar-daylight-savings-ends}.  Their values should be Lisp
expressions that refer to the variable @code{year}, and evaluate to the
Gregorian date on which daylight savings time starts or (respectively)
ends, in the form of a list @code{(@var{month} @var{day} @var{year})}.
The values should be @code{nil} if your area does not use daylight
savings time.

  Emacs uses these expressions to determine the start and end dates of
daylight savings time as holidays and for correcting times of day in the
solar and lunar calculations.

  The values for Cambridge, Massachusetts are as follows:

@example
@group
(calendar-nth-named-day 1 0 4 year)
(calendar-nth-named-day -1 0 10 year)
@end group
@end example

@noindent
i.e., the first 0th day (Sunday) of the fourth month (April) in
the year specified by @code{year}, and the last Sunday of the tenth month
(October) of that year.  If daylight savings time were
changed to start on October 1, you would set
@code{calendar-daylight-savings-starts} to this:

@example
(list 10 1 year)
@end example

  For a more complex example, suppose daylight savings time begins on
the first of Nisan on the Hebrew calendar.  You should set
@code{calendar-daylight-savings-starts} to this value:

@example
(calendar-gregorian-from-absolute
  (calendar-absolute-from-hebrew
    (list 1 1 (+ year 3760))))
@end example

@noindent
because Nisan is the first month in the Hebrew calendar and the Hebrew
year differs from the Gregorian year by 3760 at Nisan.

  If there is no daylight savings time at your location, or if you want
all times in standard time, set @code{calendar-daylight-savings-starts}
and @code{calendar-daylight-savings-ends} to @code{nil}.

@vindex calendar-daylight-time-offset
  The variable @code{calendar-daylight-time-offset} specifies the
difference between daylight savings time and standard time, measured in
minutes.  The value for Cambridge is 60.

@vindex calendar-daylight-savings-starts-time
@vindex calendar-daylight-savings-ends-time
  The variable @code{calendar-daylight-savings-starts-time} and the
variable @code{calendar-daylight-savings-ends-time} specify the number
of minutes after midnight local time when the transition to and from
daylight savings time should occur.  For Cambridge, both variables'
values are 120.

@node Diary Customizing
@section Customizing the Diary

@vindex holidays-in-diary-buffer
  Ordinarily, the mode line of the diary buffer window indicates any
holidays that fall on the date of the diary entries.  The process of
checking for holidays can take several seconds, so including holiday
information delays the display of the diary buffer noticeably.  If you'd
prefer to have a faster display of the diary buffer but without the
holiday information, set the variable @code{holidays-in-diary-buffer} to
@code{nil}.@refill

@vindex number-of-diary-entries
  The variable @code{number-of-diary-entries} controls the number of
days of diary entries to be displayed at one time.  It affects the
initial display when @code{view-diary-entries-initially} is @code{t}, as
well as the command @kbd{M-x diary}.  For example, the default value is
1, which says to display only the current day's diary entries.  If the
value is 2, both the current day's and the next day's entries are
displayed.  The value can also be a vector of seven elements: for
example, if the value is @code{[0 2 2 2 2 4 1]} then no diary entries
appear on Sunday, the current date's and the next day's diary entries
appear Monday through Thursday, Friday through Monday's entries appear
on Friday, while on Saturday only that day's entries appear.

@vindex print-diary-entries-hook
@findex print-diary-entries
  The variable @code{print-diary-entries-hook} is a normal hook run
after preparation of a temporary buffer containing just the diary
entries currently visible in the diary buffer.  (The other, irrelevant
diary entries are really absent from the temporary buffer; in the diary
buffer, they are merely hidden.)  The default value of this hook does
the printing with the command @code{lpr-buffer}.  If you want to use a
different command to do the printing, just change the value of this
hook.  Other uses might include, for example, rearranging the lines into
order by day and time.

@vindex diary-date-forms
  You can customize the form of dates in your diary file, if neither the
standard American nor European styles suits your needs, by setting the
variable @code{diary-date-forms}.  This variable is a list of patterns
for recognizing a date.  Each date pattern is a list whose elements may
be regular expressions (@pxref{Regular Expressions}) or the symbols
@code{month}, @code{day}, @code{year}, @code{monthname}, and
@code{dayname}.  All these elements serve as patterns that match certain
kinds of text in the diary file.  In order for the date pattern, as a
whole, to match, all of its elements must match consecutively.

  A regular expression in a date pattern matches in its usual fashion,
using the standard syntax table altered so that @samp{*} is a word
constituent.

  The symbols @code{month}, @code{day}, @code{year}, @code{monthname},
and @code{dayname} match the month number, day number, year number,
month name, and day name of the date being considered.  The symbols that
match numbers allow leading zeros; those that match names allow
three-letter abbreviations and capitalization.  All the symbols can
match @samp{*}; since @samp{*} in a diary entry means ``any day'', ``any
month'', and so on, it should match regardless of the date being
considered.

  The default value of @code{diary-date-forms} in the American style is
this:

@example
((month "/" day "[^/0-9]")
 (month "/" day "/" year "[^0-9]")
 (monthname " *" day "[^,0-9]")
 (monthname " *" day ", *" year "[^0-9]")
 (dayname "\\W"))
@end example

  The date patterns in the list must be @emph{mutually exclusive} and
must not match any portion of the diary entry itself, just the date and
one character of whitespace.  If, to be mutually exclusive, the pattern
must match a portion of the diary entry text---beyond the whitespace
that ends the date---then the first element of the date pattern
@emph{must} be @code{backup}.  This causes the date recognizer to back
up to the beginning of the current word of the diary entry, after
finishing the match.  Even if you use @code{backup}, the date pattern
must absolutely not match more than a portion of the first word of the
diary entry.  The default value of @code{diary-date-forms} in the
European style is this list:

@example
((day "/" month "[^/0-9]")
 (day "/" month "/" year "[^0-9]")
 (backup day " *" monthname "\\W+\\<[^*0-9]")
 (day " *" monthname " *" year "[^0-9]")
 (dayname "\\W"))
@end example

@noindent
Notice the use of @code{backup} in the third pattern, because it needs
to match part of a word beyond the date itself to distinguish it from
the fourth pattern.

@node Hebrew/Islamic Entries
@section Hebrew- and Islamic-Date Diary Entries

  Your diary file can have entries based on Hebrew or Islamic dates, as
well as entries based on the world-standard Gregorian calendar.
However, because recognition of such entries is time-consuming and most
people don't use them, you must explicitly enable their use.  If you
want the diary to recognize Hebrew-date diary entries, for example, 
you must do this:

@vindex nongregorian-diary-listing-hook
@vindex nongregorian-diary-marking-hook
@findex list-hebrew-diary-entries
@findex mark-hebrew-diary-entries
@smallexample
(add-hook 'nongregorian-diary-listing-hook 'list-hebrew-diary-entries)
(add-hook 'nongregorian-diary-marking-hook 'mark-hebrew-diary-entries)
@end smallexample

@noindent
If you want Islamic-date entries, do this:

@findex list-islamic-diary-entries
@findex mark-islamic-diary-entries
@smallexample
(add-hook 'nongregorian-diary-listing-hook 'list-islamic-diary-entries)
(add-hook 'nongregorian-diary-marking-hook 'mark-islamic-diary-entries)
@end smallexample

  Hebrew- and Islamic-date diary entries have the same formats as
Gregorian-date diary entries, except that @samp{H} precedes a Hebrew
date and @samp{I} precedes an Islamic date.  Moreover, because the
Hebrew and Islamic month names are not uniquely specified by the first
three letters, you may not abbreviate them.  For example, a diary entry
for the Hebrew date Heshvan 25 could look like this:

@smallexample
HHeshvan 25 Happy Hebrew birthday!
@end smallexample

@noindent
and would appear in the diary for any date that corresponds to Heshvan 25
on the Hebrew calendar.  And here is  Islamic-date diary entry  that matches
Dhu al-Qada 25:

@smallexample
IDhu al-Qada 25 Happy Islamic birthday!
@end smallexample

  As with Gregorian-date diary entries, Hebrew- and Islamic-date entries
are nonmarking if they are preceded with an ampersand (@samp{&}).

  Here is a table of commands used in the calendar to create diary entries
that match the selected date and other dates that are similar in the Hebrew
or Islamic calendar:

@table @kbd
@item i h d
Add a diary entry for the Hebrew date corresponding to the selected date
(@code{insert-hebrew-diary-entry}).
@item i h m
Add a diary entry for the day of the Hebrew month corresponding to the
selected date (@code{insert-monthly-hebrew-diary-entry}).  This diary
entry matches any date that has the same Hebrew day-within-month as the
selected date.
@item i h y
Add a diary entry for the day of the Hebrew year corresponding to the
selected date (@code{insert-yearly-hebrew-diary-entry}).  This diary 
entry matches any date which has the same Hebrew month and day-within-month
as the selected date.
@item i i d
Add a diary entry for the Islamic date corresponding to the selected date
(@code{insert-islamic-diary-entry}).
@item i i m
Add a diary entry for the day of the Islamic month corresponding to the
selected date (@code{insert-monthly-islamic-diary-entry}).
@item i i y
Add a diary entry for the day of the Islamic year corresponding to the
selected date (@code{insert-yearly-islamic-diary-entry}).
@end table

@findex insert-hebrew-diary-entry
@findex insert-monthly-hebrew-diary-entry
@findex insert-yearly-hebrew-diary-entry
@findex insert-islamic-diary-entry
@findex insert-monthly-islamic-diary-entry
@findex insert-yearly-islamic-diary-entry
  These commands work much like the corresponding commands for ordinary
diary entries: they apply to the date that point is on in the calendar
window, and what they do is insert just the date portion of a diary entry
at the end of your diary file.  You must then insert the rest of the 
diary entry.

@node Fancy Diary Display
@section Fancy Diary Display
@vindex diary-display-hook
@findex simple-diary-display

  Diary display works by preparing the diary buffer and then running the
hook @code{diary-display-hook}.  The default value of this hook
(@code{simple-diary-display}) hides the irrelevant diary entries and
then displays the buffer.  However, if you specify the hook as follows,

@cindex diary buffer
@findex fancy-diary-display
@example
(add-hook 'diary-display-hook 'fancy-diary-display)
@end example

@noindent
this enables fancy diary display.  It displays diary entries and
holidays by copying them into a special buffer that exists only for the
sake of display.  Copying to a separate buffer provides an opportunity
to change the displayed text to make it prettier---for example, to sort
the entries by the dates they apply to.

  As with simple diary display, you can print a hard copy of the buffer
with @code{print-diary-entries}.  To print a hard copy of a day-by-day
diary for a week by positioning point on Sunday of that week, type
@kbd{7 d} and then do @kbd{M-x print-diary-entries}.  As usual, the
inclusion of the holidays slows down the display slightly; you can speed
things up by setting the variable @code{holidays-in-diary-buffer} to
@code{nil}.

@vindex diary-list-include-blanks
  Ordinarily, the fancy diary buffer does not show days for which there are
no diary entries, even if that day is a holiday.  If you want such days to be
shown in the fancy diary buffer, set the variable
@code{diary-list-include-blanks} to @code{t}.@refill

@cindex sorting diary entries
  If you use the fancy diary display, you can use the normal hook
@code{list-diary-entries-hook} to sort each day's diary entries by their
time of day.  Here's how

@findex sort-diary-entries
@example
(add-hook 'list-diary-entries-hook 'sort-diary-entries t)
@end example

@noindent
For each day, this sorts diary entries that begin with a recognizable
time of day according to their times.  Diary entries without times come
first within each day.

  Fancy diary display also has the ability to process included diary
files.  This permits a group of people to share a diary file for events
that apply to all of them.  Lines in the diary file of this form:

@smallexample
#include "@var{filename}"
@end smallexample

@noindent
includes the diary entries from the file @var{filename} in the fancy
diary buffer.  The include mechanism is recursive, so that included files
can include other files, and so on; you must be careful not to have a
cycle of inclusions, of course.  Here is how to enable the include
facility:

@vindex list-diary-entries-hook
@vindex mark-diary-entries-hook
@findex include-other-diary-files
@findex mark-included-diary-files
@smallexample
(add-hook 'list-diary-entries-hook 'include-other-diary-files)
(add-hook 'mark-diary-entries-hook 'mark-included-diary-files)
@end smallexample

The include mechanism works only with the fancy diary display, because
ordinary diary display shows the entries directly from your diary file.

@node Sexp Diary Entries
@section Sexp Entries and the Fancy Diary Display
@cindex sexp diary entries

  Sexp diary entries allow you to do more than just have complicated
conditions under which a diary entry applies.  If you use the fancy
diary display, sexp entries can generate the text of the entry depending
on the date itself.  For example, an anniversary diary entry can insert
the number of years since the anniversary date into the text of the
diary entry.  Thus the @samp{%d} in this dairy entry:

@findex diary-anniversary
@smallexample
%%(diary-anniversary 10 31 1948) Arthur's birthday (%d years old)
@end smallexample

@noindent
gets replaced by the age, so on October 31, 1990 the entry appears in
the fancy diary buffer like this:

@smallexample
Arthur's birthday (42 years old)
@end smallexample

@noindent
If the diary file instead contains this entry:

@smallexample
%%(diary-anniversary 10 31 1948) Arthur's %d%s birthday
@end smallexample

@noindent
the entry in the fancy diary buffer for October 31, 1990 appears like this:

@smallexample
Arthur's 42nd birthday
@end smallexample

  Similarly, cyclic diary entries can interpolate the number of repetitions
that have occurred:

@findex diary-cyclic
@smallexample
%%(diary-cyclic 50 1 1 1990) Renew medication (%d%s time)
@end smallexample

@noindent
looks like this:

@smallexample
Renew medication (5th time)
@end smallexample

@noindent
in the fancy diary display on September 8, 1990.

  The generality of sexp diary entries lets you specify any diary entry
that you can describe algorithmically.  A sexp diary entry contains an
expression that computes whether the entry applies to any given date.
If its value is non-@code{nil}, the entry applies to that date;
otherwise, it does not.  The expression can use the variable  @code{date}
to find the date being considered; its value is a list (@var{month}
@var{day} @var{year}) that refers to the Gregorian calendar.

  Suppose you get paid on the 21st of the month if it is a weekday, and
on the Friday before if the 21st is on a weekend.  Here is how to write
a sexp diary entry that matches those dates:

@smallexample
&%%(let ((dayname (calendar-day-of-week date))
         (day (car (cdr date))))
      (or (and (= day 21) (memq dayname '(1 2 3 4 5)))
          (and (memq day '(19 20)) (= dayname 5)))
         ) Pay check deposited
@end smallexample

  The following sexp diary entries take advantage of the ability (in the fancy
diary display) to concoct diary entries whose text varies based on the date:

@findex diary-sunrise-sunset
@findex diary-phases-of-moon
@findex diary-day-of-year
@findex diary-iso-date
@findex diary-julian-date
@findex diary-astro-day-number
@findex diary-hebrew-date
@findex diary-islamic-date
@findex diary-french-date
@findex diary-mayan-date
@table @code
@item %%(diary-sunrise-sunset)
Make a diary entry for the local times of today's sunrise and sunset.
@item %%(diary-phases-of-moon)
Make a diary entry for the phases (quarters) of the moon.
@item %%(diary-day-of-year)
Make a diary entry with today's day number in the current year and the number
of days remaining in the current year.
@item %%(diary-iso-date)
Make a diary entry with today's equivalent ISO commercial date.
@item %%(diary-julian-date)
Make a diary entry with today's equivalent date on the Julian calendar.
@item %%(diary-astro-day-number)
Make a diary entry with today's equivalent astronomical (Julian) day number.
@item %%(diary-hebrew-date)
Make a diary entry with today's equivalent date on the Hebrew calendar.
@item %%(diary-islamic-date)
Make a diary entry with today's equivalent date on the Islamic calendar.
@item %%(diary-french-date)
Make a diary entry with today's equivalent date on the French Revolutionary
calendar.
@item %%(diary-mayan-date)
Make a diary entry with today's equivalent date on the Mayan calendar.
@end table

@noindent
Thus including the diary entry

@example
&%%(diary-hebrew-date)
@end example

@noindent
causes every day's diary display to contain the equivalent date on the
Hebrew calendar, if you are using the fancy diary display.  (With simple
diary display, the line @samp{&%%(diary-hebrew-date)} appears in the
diary for any date, but does nothing particularly useful.)

  These functions can be used to construct sexp diary entries based on
the Hebrew calendar in certain standard ways:

@cindex rosh hodesh
@findex diary-rosh-hodesh
@cindex parasha, weekly
@findex diary-parasha
@cindex candle lighting times
@findex diary-sabbath-candles
@cindex omer count
@findex diary-omer
@cindex yahrzeits
@findex diary-yahrzeit
@table @code
@item %%(diary-rosh-hodesh)
Make a diary entry that tells the occurrence and ritual announcement of each
new Hebrew month.
@item %%(diary-parasha)
Make a Saturday diary entry that tells the weekly synagogue scripture reading.
@item %%(diary-sabbath-candles)
Make a Friday diary entry that tells the @emph{local time} of Sabbath
candle lighting.
@item %%(diary-omer)
Make a diary entry that gives the omer count, when appropriate.
@item %%(diary-yahrzeit @var{month} @var{day} @var{year}) @var{name}
Make a diary entry marking the anniversary of a date of death.  The date
is the @emph{Gregorian} (civil) date of death.  The diary entry appears
on the proper Hebrew calendar anniversary and on the day before.  (In
the European style, the order of the parameters is changed to @var{day},
@var{month}, @var{year}.)
@end table

@node Appt Customizing
@section Customizing Appointment Reminders

  You can specify exactly how Emacs reminds you of an appointment, and
how far in advance it begins doing so, by setting these variables:

@vindex appt-message-warning-time
@vindex appt-audible
@vindex appt-visible
@vindex appt-display-mode-line
@vindex appt-msg-window
@vindex appt-display-duration
@vindex appt-disp-window-function
@vindex appt-delete-window-function
@table @code
@item appt-message-warning-time
The time in minutes before an appointment that the reminder begins.  The
default is 10 minutes.
@item appt-audible
If this is non-@code{nil}, Emacs rings the
terminal bell for appointment reminders.  The default is @code{t}.
@item appt-visible
If this is non-@code{nil}, Emacs displays the appointment
message in the echo area.  The default is @code{t}.
@item appt-display-mode-line
If this is non-@code{nil}, Emacs displays the number of minutes
to the appointment on the mode line.  The default is @code{t}.
@item appt-msg-window
If this is non-@code{nil}, Emacs displays the appointment
message in another window.  The default is @code{t}.
@item appt-disp-window-function
This variable holds a function to use to create the other window
for the appointment message.
@item appt-delete-window-function
This variable holds a function to use to get rid of the appointment
message window, when its time is up.
@item appt-display-duration
The number of seconds to display an appointment message.  The default
is 5 seconds.
@end table

#
# SUMMARY:      Summary of OO-Browser features
#
# AUTHOR:       Bob Weiner
#
# ORIG-DATE:    15-Oct-90
# LAST-MOD:     23-Aug-95 at 15:56:13 by Bob Weiner
#
# Copyright (C) 1990-1995  Free Software Foundation, Inc.
# See the file BR-COPY for license information.
#
# This file is part of the OO-Browser.
     
OO-Browser Features
*******************

The OO-Browser is a multi-windowed, interactive object-oriented class browser
similar in use to the well-known Smalltalk browsers.  It is unique in a
number of respects foremost of which is that it works well with a
multiplicity of object-oriented languages.  It provides both textual
views within an editor and graphical views under the X window system.


* Support for all popular OO Languages on UNIX
----------------------------------------------

  ** Contains support for Eiffel, C++, Objective-C, Common Lisp and its
     Object System (CLOS), Java, and Smalltalk class browsing.  Additionally,
     support for browsing large amounts of material in Info format by node
     name (a popular online documentation format with cross references and
     hierarchical structure) is included.

  ** Method browsing is supported for C++, Objective-C, Eiffel, Java and
     CLOS.  CLOS supports browsing all elements defined with (def*
     constructs.  In-source feature browsing is also supported for all of
     these languages.  One simply selects a feature name to jump to its
     corresponding source.  Method name overloading in C++ and inherited
     feature renaming in Eiffel are fully supported.

  ** C code browsing is supported for C++, Objective-C and C source code.

  ** Objective-C category and formal protocol browsing are supported.

  ** C++ parameterized template classes and methods are supported.

  ** Immediate switching among languages is allowed.  One can switch
     from Eiffel browsing to C++ browsing in an instant, if so desired.
     Or simply run two OO-Browsers side by side.

  ** Multiple inheritance support is built-in, where applicable.

  ** Statistics on classes and Environments may be displayed.

  ** Language-specific class information may be shown.  Presently this
     feature is supported only for Eiffel.  A listing of class parents,
     attributes, routines and best guess (highly accurate) list of
     routine calls may be displayed.  Outputs from the Eiffel 'short'
     and 'flat' commands may also be shown.


* For Serious Use
----------------------

  ** Library (stable) and System (in development) classes may be
     maintained and listed separately or together.  Any number of
     Libraries and Systems may be combined for listing in a single
     Environment.  There are no fixed limits on the number of classes per
     Environment nor on the number of Environments that may be browsed.

  **  All source code, over 400 kilobytes, is included and is heavily
      documented.


* Ease of Use
-------------

  ** Machine-independent mouse support is included along with an extremely
     intuitive point and click interface that uses just two mouse keys.  The
     OO-Browser is pre-configured for use with the X window system, NEXTSTEP,
     Sunview or Apollo's DM window system under InfoDock, Emacs V19, XEmacs,
     Epoch, and Emacs V18.  Online mouse usage help is always one key away.

  ** Popup and pulldown command menus are available under InfoDock, Emacs V19
     and XEmacs.

  ** The OO-Browser help display gives short descriptions of all of
     the commands and key bindings available in the browser.  By clicking
     on any such selection, the corresponding command is executed.

  ** One may also click on class names to see ancestors, descendants,
     or the class itself.  Just select a class name and the OO-Browser
     immediately will display or edit the class' source.  Once a class
     file has been loaded, one can quickly switch to it by selection
     from a menu of such files. 

  ** For a number of languages, one may also select a feature
     (method) name or declaration and move directly to the definition
     of the feature.  The browser accounts for the complexities of member name
     overloading in C++ and unlimited feature renaming in Eiffel so that you
     need not.  Just click on a declaration and watch the browser display jump
     to the proper defintion.

  ** Jump back to a previously visited class or feature by selecting from a
     list of recently visited ones.

  ** OO-Browser commands may also be invoked from the keyboard, allowing
     unrestricted use via standard terminal interfaces.

* Speed
-------

  ** Building Environments is fast compared to many other tools and browser
     startup once an Environment has been built is very fast.  Response times
     on workstations are excellent; for example, in one test case, less than
     two real seconds were required to display a set of complex inheritance
     graphs involving over 400 classes.


* Flexibility
-------------

  ** X and NEXTSTEP hierarchy display browsers are included.  They provide
     views of class inheritance structure and lexically included elements,
     which allows for quick random access to entire Environments.  A click on
     a class or element name immediately jumps to it in the editor, providing
     rapid, visual browsing.  One can pop up several graphical browsers to
     gain several views of classes in the same or in multiple environments.
     All graphical browsers can communicate with a single textual browser, so
     one can quickly display and edit classes from different environments
     (even different languages).

  ** The OO-Browser uses class source code only, hence no compiler is
     necessary for proper browser operation.  This allows one to explore
     class libraries without the need for additional tools.

  ** Class inheritance networks may be displayed.  Either a single
     inheritance level (parents or children) or the entire inheritance
     network (ancestors or descendants) for a set of classes may be shown.

  ** Class files may be added, replaced or deleted one at a time or as a
     group by specifying a root directory below which all class files are
     found, including those in subdirectories.

  ** A menu of class files can be displayed for random access to specific
     code modules.

  ** On startup, the OO-Browser lists all currently known classes within a
     particular Environment.  Any desired classes may be found by searching
     or by matching a regular expression or string to the set of class names.
     This may be done repeatedly to achieve an "and"-type relational query
     effect.

  ** The number of listing windows is limited only by the screen width and
     the width setting used for listing windows.

  ** The OO-Browser is adaptable to any class-based object-oriented language.


* Editor Integration
--------------------

  ** The OO-Browser works with the powerful, freely distributable, GNU
     Emacs editor; it works on any UNIX system display supported by Emacs.
     It is also possible to use alternative editors to view or to edit source
     code displayed by the browser.

  ** All OO-Browser outputs are text which may be edited as desired or
     saved to files.

  ** OO-Browser functions may be used standalone within the editor
     without using the multi-windowed browser interface.  One useful
     example is to point to a class name such as a parent class in the
     text of another class and have the parent's source appear in an
     editable fashion.

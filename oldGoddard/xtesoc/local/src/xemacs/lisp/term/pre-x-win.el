;;; pre-x-win.el --- first phase of runtime initialization for X windows
;; Copyright (C) 1990, 1993, 1994 Free Software Foundation, Inc.
;; Copyright (C) 1995 Board of Trustees, University of Illinois.

;; Author: FSF
;; Keywords: terminals

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; pre-x-win.el: this file is loaded either from ../term/x-win.el or
;; by make-device when it creates the first X device.

;;; Code:


;;; If you want to change this variable, this is the place you must do it.
;;; Do not set it to a string containing periods.  X doesn't like that.
;(setq x-emacs-application-class "Emacs")

(require 'x-iso8859-1)

(setq mouse-motion-handler 'x-track-pointer)
(setq character-set-property 'x-iso8859/1) ; see x-iso8859-1.el

(setq initial-frame-alist (if initial-frame-unmapped-p
			       '((initially-unmapped . t))
			     nil))

;; This should really be in x11/x-toolbar.el but we need it before we
;; create the first X device and we can't load x-toolbar.el until
;; after we create the first X device.
;; #### Someone tell me I'm missing something and there is a better
;; #### way to do this.
(defun x-init-toolbar-from-resources (locale)
  (x-init-specifier-from-resources
   top-toolbar-height 'natnum locale
   '("topToolBarHeight" . "TopToolBarHeight"))
  (x-init-specifier-from-resources
   bottom-toolbar-height 'natnum locale
   '("bottomToolBarHeight" . "BottomToolBarHeight"))
  (x-init-specifier-from-resources
   left-toolbar-width 'natnum locale
   '("leftToolBarWidth" . "LeftToolBarWidth"))
  (x-init-specifier-from-resources
   right-toolbar-width 'natnum locale
   '("rightToolBarWidth" . "RightToolBarWidth")))

(provide 'pre-x-win)

;;; pre-x-win.el ends here

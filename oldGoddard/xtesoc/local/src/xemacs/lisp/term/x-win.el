;;; x-win.el --- runtime initialization for X windows if initial window system
;; Copyright (C) 1990, 1993, 1994 Free Software Foundation, Inc.
;; Copyright (C) 1995 Board of Trustees, University of Illinois.

;; Author: FSF
;; Keywords: terminals

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; x-win.el: this file is loaded from ../lisp/startup.el when it
;; recognizes that the initial window system is to be X.  An X device
;; is created and hooks are set for popping up the initial window.

;;; Code:


;;; If you want to change this variable, this is the place you must do it.
;;; Do not set it to a string containing periods.  X doesn't like that.
;(setq x-emacs-application-class "Emacs")

(require 'pre-x-win)

;; Open the X display when this file is loaded
;; (Note that the first frame is created later.)
(setq command-line-args-left
      (cdr (device-x-argv-list
	    (make-x-device nil (cons (car command-line-args)
				     command-line-args-left)))))

(require 'post-x-win)

;;; x-win.el ends here

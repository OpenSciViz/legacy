;;; ns-win.el --- runtime initialization for the NeXTstep window system
;; Copyright (C) 1995 NO ONE YET

;; Author: XEmacs
;; Keywords: terminals

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; ns-win.el: this file is loaded from ../lisp/startup.el when it
;; recognizes that NeXTstep is to be used.  The NeXTstep display is
;; opened and hooks are set for popping up the initial window.

;; startup.el will then examine startup files, and eventually call the
;; hooks which create the first window (s).

;;; Code:

;;; ns-win.el ends here

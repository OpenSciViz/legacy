;;; tty-win.el --- runtime initialization for tty's if initial window system
;; Copyright (C) 1994 Free Software Foundation, Inc.

;;; This file is part of XEmacs.
;;;
;;; XEmacs is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2, or (at your option)
;;; any later version.
;;;
;;; XEmacs is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with XEmacs; see the file COPYING.  If not, write to
;;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(require 'pre-tty-win)

;; #### This is all that should really be in the middle but we can't
;; do that until we have device-local variables.
;; (make-tty-device nil nil)

;; #### make-device needs to be doing the initialization of
;; function-key-map.

(let (newdevice
      (function-key-map (make-sparse-keymap)))
  (setq newdevice (make-tty-device nil nil))
  (load "term/vt100.el" nil t)
  (set-device-function-key-map newdevice function-key-map))

(require 'post-tty-win)

;;; tty-win.el ends here

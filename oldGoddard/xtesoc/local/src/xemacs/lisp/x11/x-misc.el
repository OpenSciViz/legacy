;;; x-misc.el --- miscellaneous X functions.

;;; Copyright (C) 1995 Sun Microsystems.

;; Author: Ben Wing <wing@netcom.com>

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defun x-bogosity-check-resource (name class type)
  "Check for a bogus resource specification."
  (let ((bogus (x-get-resource
		(concat "__no-such-friggin-locale__." name)
		(concat "__No-such-friggin-widget__." class)
		type 'global nil t)))
    (if bogus
	(display-warning
	 'resource
	 (format "Bad resource specification encountered: something like
     Emacs*%s: %s
You should replace the * with a . in order to get proper behavior when
you use the specifier and/or `set-face-*' functions." name bogus)))))

(defun x-init-specifier-from-resources (specifier type locale
						  &rest resource-list)
  "Initialize a specifier from the resource database.
LOCALE specifies the locale that is to be initialized and should be
a frame, a device, or 'global.  TYPE is the type of the resource and
should be one of 'string, 'boolean, 'integer, or 'natnum.  The
remaining args should be conses of names and classes of resources
to be examined.  The first resource with a value specified becomes
the spec for SPECIFIER in LOCALE. (However, if SPECIFIER already
has a spec in LOCALE, nothing is done.) Finally, if LOCALE is 'global,
a check is done for bogus resource specifications."
  (if (eq locale 'global)
      (mapcar #'(lambda (x)
		  (x-bogosity-check-resource (car x) (cdr x) type))
	      resource-list))
  (if (not (specifier-spec-list specifier locale))
      (catch 'done
	(while resource-list
	  (let* ((name (caar resource-list))
		 (class (cdar resource-list))
		 (resource
		  (x-get-resource name class type locale nil t)))
	    (if resource
		(progn
		  (add-spec-to-specifier specifier resource locale)
		  (throw 'done t))))
	  (setq resource-list (cdr resource-list))))))

(defun x-get-resource-and-bogosity-check (name class type &optional locale)
  (x-bogosity-check-resource name class type)
  (x-get-resource name class type locale nil t))

;; #### this function is not necessary.
(defun x-get-resource-and-maybe-bogosity-check (name class type &optional
						     locale)
  (if (eq locale 'global)
      (x-bogosity-check-resource name class type))
  (x-get-resource name class type locale nil t))

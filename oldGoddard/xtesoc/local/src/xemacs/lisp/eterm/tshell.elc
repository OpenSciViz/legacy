;ELC   
;;; compiled by cthomp@willow.cs.uiuc.edu on Tue Aug 29 14:00:42 1995
;;; from file /xemacs/xemacs-19.13-release/editor/lisp/eterm/tshell.el
;;; emacs version 19.13 XEmacs Lucid.
;;; bytecomp version 2.25; 1-Sep-94.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19")))
    (error "This file was compiled for Emacs 19."))

(require 'term)
(defvar tshell-prompt-pattern "^[^#$%>\n]*[#$%>] *" "\
Regexp to match prompts in the inferior shell.
Defaults to \"^[^#$%>\\n]*[#$%>] *\", which works pretty well.
This variable is used to initialise `term-prompt-regexp' in the 
shell buffer.

The pattern should probably not match more than one line.  If it does,
tshell-mode may become confused trying to distinguish prompt from input
on lines which don't start with a prompt.

This is a fine thing to set in your `.emacs' file.")
(defvar tshell-completion-fignore nil "\
*List of suffixes to be disregarded during file/command completion.
This variable is used to initialize `term-completion-fignore' in the shell
buffer.  The default is nil, for compatibility with most shells.
Some people like (\"~\" \"#\" \"%\").

This is a fine thing to set in your `.emacs' file.")
(defvar tshell-delimiter-argument-list '(124 38 60 62 40 41 59) "\
List of characters to recognise as separate arguments.
This variable is used to initialize `term-delimiter-argument-list' in the
shell buffer.  The default is (?\\| ?& ?< ?> ?\\( ?\\) ?\\;).

This is a fine thing to set in your `.emacs' file.")
(defvar tshell-dynamic-complete-functions '(term-replace-by-expanded-history tshell-dynamic-complete-environment-variable tshell-dynamic-complete-command tshell-replace-by-expanded-directory term-dynamic-complete-filename) "\
List of functions called to perform completion.
This variable is used to initialise `term-dynamic-complete-functions' in the
shell buffer.

This is a fine thing to set in your `.emacs' file.")
(defvar shell-command-regexp "[^;&|\n]+" "\
*Regexp to match a single command within a pipeline.
This is used for directory tracking and does not do a perfect job.")
(defvar shell-completion-execonly t "\
*If non-nil, use executable files only for completion candidates.
This mirrors the optional behavior of tcsh.

Detecting executability of files may slow command completion considerably.")
(defvar shell-popd-regexp "popd" "\
*Regexp to match subshell commands equivalent to popd.")
(defvar shell-pushd-regexp "pushd" "\
*Regexp to match subshell commands equivalent to pushd.")
(defvar shell-pushd-tohome nil "\
*If non-nil, make pushd with no arg behave as \"pushd ~\" (like cd).
This mirrors the optional behavior of tcsh.")
(defvar shell-pushd-dextract nil "\
*If non-nil, make \"pushd +n\" pop the nth dir to the stack top.
This mirrors the optional behavior of tcsh.")
(defvar shell-pushd-dunique nil "\
*If non-nil, make pushd only add unique directories to the stack.
This mirrors the optional behavior of tcsh.")
(defvar shell-cd-regexp "cd" "\
*Regexp to match subshell commands equivalent to cd.")
(defvar explicit-csh-args (byte-code "�=��Ç" [system-type hpux ("-i" "-T") ("-i")] 2) "\
*Args passed to inferior shell by M-x tshell, if the shell is csh.
Value is a list of strings, which may be nil.")
(defvar tshell-input-autoexpand 'history "\
*If non-nil, expand input command history references on completion.
This mirrors the optional behavior of tcsh (its autoexpand and histlit).

If the value is `input', then the expansion is seen on input.
If the value is `history', then the expansion is only when inserting
into the buffer's input ring.  See also `term-magic-space' and
`term-dynamic-complete'.

This variable supplies a default for `term-input-autoexpand',
for Tshell mode only.")
(defvar tshell-dirstack nil "\
List of directories saved by pushd in this buffer's shell.
Thus, this does not include the shell's current directory.")
(defvar tshell-dirtrackp t "\
Non-nil in a shell buffer means directory tracking is enabled.")
(defvar tshell-last-dir nil "\
Keep track of last directory for ksh `cd -' command.")
(defvar tshell-dirstack-query nil "\
Command used by `tshell-resync-dir' to query the shell.")
(byte-code "��!���	���!�	��#��	��#��	��#��	��#���	�\"���$���	�\"���$�" [boundp tshell-mode-map nil copy-keymap term-mode-map define-key "" tshell-forward-command "" tshell-backward-command "	" term-dynamic-complete "�" term-dynamic-list-filename-completions define-key-after lookup-key [menu-bar completion] [complete-env-variable] ("Complete Env. Variable Name" . tshell-dynamic-complete-environment-variable) complete-file [menu-bar completion] [expand-directory] ("Expand Directory Reference" . tshell-replace-by-expanded-directory) complete-expand] 5)
(defvar tshell-mode-hook nil "\
*Hook for customising Tshell mode.")
(fset 'tshell-mode #[nil "� ����!�	\n��!���!�����!�����\"����p!!@!��!��ᘫ�⪊㘫�䪁�&皫��&��\"��ꪁ�,)��!���!�" [term-mode tshell-mode major-mode "Shell" mode-name use-local-map tshell-mode-map tshell-prompt-pattern term-prompt-regexp tshell-completion-fignore term-completion-fignore tshell-delimiter-argument-list term-delimiter-argument-list tshell-dynamic-complete-functions term-dynamic-complete-functions make-local-variable paragraph-start tshell-dirstack nil tshell-last-dir tshell-dirtrackp t add-hook term-input-filter-functions tshell-directory-tracker tshell-input-autoexpand term-input-autoexpand file-name-nondirectory process-command get-buffer-process shell getenv "HISTFILE" "bash" "~/.bash_history" "ksh" "~/.sh_history" "~/.history" term-input-ring-file-name "/dev/null" string-match "^k?sh$" "pwd" "dirs" tshell-dirstack-query run-hooks tshell-mode-hook term-read-input-ring] 5 "\
Major mode for interacting with an inferior shell.
Return after the end of the process' output sends the text from the 
    end of process to the end of the current line.
Return before end of process output copies the current line (except
    for the prompt) to the end of the buffer and sends it.
M-x term-send-invisible reads a line of text without echoing it,
    and sends it to the shell.  This is useful for entering passwords.

If you accidentally suspend your process, use \\[term-continue-subjob]
to continue it.

cd, pushd and popd commands given to the shell are watched by Emacs to keep
this buffer's default directory the same as the shell's working directory.
M-x dirs queries the shell and resyncs Emacs' idea of what the current 
    directory stack is.
M-x dirtrack-toggle turns directory tracking on and off.

\\{tshell-mode-map}
Customization: Entry to this mode runs the hooks on `term-mode-hook' and
`tshell-mode-hook' (in that order).  Before each input, the hooks on
`term-input-filter-functions' are run.

Variables `shell-cd-regexp', `shell-pushd-regexp' and `shell-popd-regexp'
are used to match their respective commands, while `shell-pushd-tohome',
`shell-pushd-dextract' and `shell-pushd-dunique' control the behavior of the
relevant command.

Variables `term-completion-autolist', `term-completion-addsuffix',
`term-completion-recexact' and `term-completion-fignore' control the
behavior of file name, command name and variable name completion.  Variable
`shell-completion-execonly' controls the behavior of command name completion.
Variable `tshell-completion-fignore' is used to initialise the value of
`term-completion-fignore'.

Variables `term-input-ring-file-name' and `term-input-autoexpand' control
the initialisation of the input ring history, and history expansion.

Variables `term-output-filter-functions', a hook, and
`term-scroll-to-bottom-on-input' and `term-scroll-to-bottom-on-output'
control whether input and output cause the window to scroll to the end of the
buffer." nil])
(fset 'tshell #[nil "��!��\n����!����!����!	�	P��	�Q!����!�����!��J���%q�� �,��!�" [term-check-proc "*shell*" explicit-shell-file-name getenv "ESHELL" "SHELL" "/bin/sh" prog file-name-nondirectory name "~/.emacs_" startfile intern-soft "explicit-" "-args" xargs-name apply make-term "shell" file-exists-p boundp ("-i") tshell-mode switch-to-buffer] 7 "\
Run an inferior shell, with I/O through buffer *shell*.
If buffer exists but shell process is not running, make new shell.
If buffer exists and shell process is running, just switch to buffer `*shell*'.
Program used comes from variable `explicit-shell-file-name',
 or (if that is nil) from the ESHELL environment variable,
 or else from SHELL if there is no ESHELL.
If a file `~/.emacs_SHELLNAME' exists, it is given as initial input
 (Note that this may lose due to a timing error if the shell
  discards input when it starts up.)
The buffer is put in Tshell mode, giving commands for sending input
and controlling the subjobs of the shell.  See `tshell-mode'.
See also the variable `tshell-prompt-pattern'.

The shell file name (sans directories) is used to make a symbol name
such as `explicit-csh-args'.  If that symbol is a variable,
its value is used as a list of arguments when invoking the shell.
Otherwise, one argument `-i' is passed to the shell.

(Type \\[describe-mode] in the shell buffer for a list of commands.)" nil])
(fset 'tshell-directory-tracker #[(str) "����Ï�" [tshell-dirtrackp chdir-failure (byte-code "��\n\"�Õĉ��	\n#��Õ�\nOÉ#�\nOˉ#�\"�=����!!����\"�=����!!����\"�=����!!���\n#�Õ�,�" [string-match "^[;\\s ]*" str 0 nil arg1 cmd end start shell-command-regexp term-arguments 1 shell-popd-regexp tshell-process-popd substitute-in-file-name shell-pushd-regexp tshell-process-pushd shell-cd-regexp tshell-process-cd "[;\\s ]*"] 4) ((error "Couldn't cd"))] 3 "\
Tracks cd, pushd and popd commands issued to the shell.
This function is called on each input passed to the shell.
It watches for cd, pushd and popd commands and sets the buffer's
default directory to track these commands.

You may toggle this tracking on and off with M-x dirtrack-toggle.
If emacs gets confused, you can resync with the shell with M-x dirs.

See variables `shell-cd-regexp', `shell-pushd-regexp', and `shell-popd-regexp',
while `shell-pushd-tohome', `shell-pushd-dextract' and `shell-pushd-dunique'
control the behavior of the relevant command.

Environment variables are expanded, see function `substitute-in-file-name'."])
(byte-code "��M���M���M���M���M�" [tshell-process-popd #[(arg) "�	!�����U�����@!�A� �����V��GX���BS��	�AA��A� *����!)�" [tshell-extract-num arg 0 num tshell-dirstack cd tshell-dirstack-message nil ds cell error "Couldn't popd"] 3] tshell-prefixed-directory-name #[(dir) "G�U��\n��\n!��\nP��\n!�" [term-file-name-prefix 0 dir file-name-absolute-p expand-file-name] 2] tshell-process-cd #[(arg) "G�U��\n�P��Ę�����!�!�� )�" [arg 0 term-file-name-prefix "~" "-" tshell-last-dir tshell-prefixed-directory-name new-dir default-directory cd tshell-dirstack-message] 2] tshell-process-pushd #[(arg) "�	!	G�U������P!�� ��	\n�@!�\nAB� )�� ��!�� \n���\nGV����!��\n�U�����!!����\nS8�	!��	!��!�� )��	B�G\n��\nZ�!�!�\"�@!�A� -��	��	!!������B� ))�" [tshell-extract-num arg num 0 shell-pushd-tohome shell-process-pushd term-file-name-prefix "~" tshell-dirstack default-directory old cd tshell-dirstack-message message "Directory stack empty." "Directory stack not that deep." error "Couldn't cd." shell-pushd-dextract dir tshell-process-popd tshell-process-pushd ds dslen front reverse back append new-ds old-wd tshell-prefixed-directory-name shell-pushd-dunique] 5] tshell-extract-num #[(str) "��\n\"���\n!�" [string-match "^\\+[1-9][0-9]*$" str string-to-int] 3]] 2)
(fset 'tshell-dirtrack-toggle #[nil "?����ê��\"�" [tshell-dirtrackp message "Directory tracking %s" "ON" "OFF"] 3 "\
Turn directory tracking on and off in a shell buffer." nil])
(defalias 'dirtrack-toggle 'tshell-dirtrack-toggle)
(fset 'tshell-resync-dirs #[nil "�p!�	!�b�c��c���!��	\"��	�\"�`ɓ�`\n�c���!���!���	!�\nb��q)b���!�ǔǕS{�G���W����#�̔̕OPBǕ��]���ُ.�" [get-buffer-process proc process-mark pmark tshell-dirstack-query "\n" sit-for 0 term-send-string nil pt backward-char 1 looking-at ".+\n" accept-process-output delete-char dl dl-len ds i string-match "\\s *\\(\\S +\\)\\s *" term-file-name-prefix (byte-code "�	@!�	A� �" [cd ds tshell-dirstack tshell-dirstack-message] 2) ((error (message "Couldn't cd.")))] 5 "\
Resync the buffer's idea of the current directory stack.
This command queries the shell with the command bound to 
`tshell-dirstack-query' (default \"dirs\"), reads the next
line output and parses it to form the new directory stack.
DON'T issue this command unless the buffer is at a shell prompt.
Also, note that if some other subprocess decides to do output
immediately after the query, its output will be taken as the
new directory stack -- you lose. If this happens, just do the
command again." nil])
(byte-code "���\"���M�" [defalias dirs tshell-resync-dirs tshell-dirstack-message #[nil "�\nB��P!�G	��@�\nG	Y��\n�	O����\n	�OP\n��\nGGY��\n�GO���\nG�O�\n��\n��	�\n!�QA)�%�	!,�" ["" msg default-directory tshell-dirstack ds expand-file-name term-file-name-prefix "~/" home homelen dir 0 nil directory-file-name " " message] 5]] 3)
(fset 'tshell-forward-command #[(&optional arg) "���`)��P	�$����!)�" [nil limit re-search-forward shell-command-regexp "\\([;&|][	 ]*\\)+" move arg skip-syntax-backward " "] 5 "\
Move forward across ARG shell command(s).  Does not cross lines.
See `shell-command-regexp'." "p"])
(fset 'tshell-backward-command #[(&optional arg) "���!�`)�`V����y�`)��\n\"����	\"\n�$��̔b���w)�" [term-bol nil limit 0 skip-syntax-backward " " re-search-backward format "[;&|]+[	 ]*\\(%s\\)" shell-command-regexp move arg 1 ";&|"] 6 "\
Move backward across ARG shell command(s).  Does not cross lines.
See `shell-command-regexp'." "p"])
(fset 'tshell-dynamic-complete-command #[nil "� ���� Ď��	\"*?��ǔ���!�`)=����!�� )�" [term-match-partial-filename filename match-data _match_data_ ((store-match-data _match_data_)) string-match "[~/]" 0 tshell-backward-command 1 message "Completing command name..." tshell-dynamic-complete-as-command] 4 "\
Dynamically complete the command at point.
This function is similar to `term-dynamic-complete-filename', except that it
searches `exec-path' (minus the trailing emacs library path) for completion
candidates.  Note that this may not be the same as the shell's idea of the
path.

Completion is dependent on the value of `shell-completion-execonly', plus
those that effect file completion.  See `tshell-dynamic-complete-as-command'.

Returns t if successful." nil])
(fset 'tshell-dynamic-complete-as-command #[nil "� ����\n!�!A��\n!!�����#���������@���!!�!���\"���@P������\"������!�����!��BA��;A����\")�!�>������ !���c�!.�" [term-match-partial-filename "" filename file-name-nondirectory pathnondir reverse exec-path paths file-name-as-directory expand-file-name default-directory cwd term-completion-fignore mapconcat #[(x) "�	!�P�" [regexp-quote x "$"] 2] "\\|" ignored-extensions path nil comps-in-path file filepath completions term-directory "." file-accessible-directory-p file-name-all-completions string-match file-directory-p shell-completion-execonly file-executable-p term-completion-addsuffix term-dynamic-simple-complete success (sole shortest) " "] 5 "\
Dynamically complete at point as a command.
See `tshell-dynamic-complete-filename'.  Returns t if successful."])
(fset 'tshell-match-partial-variable #[nil "�`����#����!���u�l����!?����\"�˔˕{*�" [limit re-search-backward "[^A-Za-z0-9_{}]" nil move looking-at "\\$" 1 "[^A-Za-z0-9_{}$]" re-search-forward "\\$?{?[A-Za-z0-9_]*}?" 0] 4 "\
Return the variable at point, or nil if non is found."])
(fset 'tshell-dynamic-complete-environment-variable #[nil "� �����	\"����!�� )�" [tshell-match-partial-variable variable string-match "^\\$" message "Completing variable name..." tshell-dynamic-complete-as-environment-variable] 4 "\
Dynamically complete the environment variable at point.
Completes if after a variable, i.e., if it starts with a \"$\".
See `tshell-dynamic-complete-as-environment-variable'.

This function is similar to `term-dynamic-complete-filename', except that it
searches `process-environment' for completion candidates.  Note that this may
not be the same as the interpreter's idea of variable names.  The main problem
with this type of completion is that `process-environment' is the environment
which Emacs started with.  Emacs does not track changes to the environment made
by the interpreter.  Perhaps it would be more accurate if this function was
called `tshell-dynamic-complete-process-environment-variable'.

Returns non-nil if successful." nil])
(fset 'tshell-dynamic-complete-as-environment-variable #[nil "� ������\n\"����O��\n\"��\"��>��� ���\n\"�O��\n\"��Ӫ���\n\"��ժ����������!!!��ڪ����,.�" [tshell-match-partial-variable "" var string-match "[^$({]\\|$" 0 nil variable mapcar #[(x) "���\"O�" [x 0 string-match "="] 5] process-environment variables term-completion-addsuffix addsuffix term-dynamic-simple-complete success (sole shortest) "[^$({]" "{" "}" "(" ")" protection file-directory-p term-directory getenv "/" " " suffix] 5 "\
Dynamically complete at point as an environment variable.
Used by `tshell-dynamic-complete-environment-variable'.
Uses `term-dynamic-simple-complete'."])
(fset 'tshell-replace-by-expanded-directory #[nil "� �Ɋ��b�\nB��!��G����!���Ȕȕ{!	\n	��˪�	\nGY����!����	\n8!Љ#���	\"��+�" [term-match-partial-filename 0 default-directory tshell-dirstack looking-at "=-/?" "=\\([0-9]+\\)" string-to-number 1 index stack nil error "Directory stack not that deep." replace-match file-name-as-directory t message "Directory item: %d"] 4 "\
Expand directory stack reference before point.
Directory stack references are of the form \"=digit\" or \"=-\".
See `default-directory' and `tshell-dirstack'.

Returns t if successful." nil])
(provide 'tshell)

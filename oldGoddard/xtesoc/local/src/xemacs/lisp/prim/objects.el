;;; objects.el --- Lisp interface to C window-system objects
;; Keywords: faces internal

;; Copyright (C) 1994 Board of Trustees, University of Illinois
;; Copyright (C) 1995 Ben Wing

;; Author: Chuck Thompson <cthomp@cs.uiuc.edu>,
;;         Ben Wing <wing@netcom.com>

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defun object-property-1 (function object domain)
  (let ((instance (specifier-instance object domain)))
    (and instance (funcall function instance))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; font specifiers

(defun make-font-specifier (spec-list)
  "Create a new `font' specifier object with the given specification list.
SPEC-LIST can be a list of specifications (each of which is a cons of a
locale and a list of instantiators), a single instantiator, or a list
of instantiators.  See `make-specifier' for more information about
specifiers."
  (make-specifier-and-init 'font spec-list))

(defun font-name (font &optional domain)
  "Return the name of the FONT in the specified DOMAIN, if any.
FONT should be a font specifier object and DOMAIN is normally a window
and defaults to the selected window if omitted.  This is equivalent
to using `specifier-instance' and applying `font-instance-name' to
the result.  See `make-specifier' for more information about specifiers."
  (object-property-1 'font-instance-name font domain))

(defun font-properties (font &optional domain)
  "Return the properties of the FONT in the specified DOMAIN, if any.
FONT should be a font specifier object and DOMAIN is normally a window
and defaults to the selected window if omitted.  This is equivalent
to using `specifier-instance' and applying `font-instance-properties'
to the result.  See `make-specifier' for more information about specifiers."
  (object-property-1 'font-instance-properties font domain))

(defun font-truename (font &optional domain)
  "Return the truename of the FONT in the specified DOMAIN, if any.
FONT should be a font specifier object and DOMAIN is normally a window
and defaults to the selected window if omitted.  This is equivalent
to using `specifier-instance' and applying `font-instance-truename'
to the result.  See `make-specifier' for more information about specifiers."
  (object-property-1 'font-instance-truename font domain))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; color specifiers

(defun make-color-specifier (spec-list)
  "Create a new `color' specifier object with the given specification list.
SPEC-LIST can be a list of specifications (each of which is a cons of a
locale and a list of instantiators), a single instantiator, or a list
of instantiators.  See `make-specifier' for a detailed description of
how specifiers work."
  (make-specifier-and-init 'color spec-list))

(defun color-name (color &optional domain)
  "Return the name of the COLOR in the specified DOMAIN, if any.
COLOR should be a color specifier object and DOMAIN is normally a window
and defaults to the selected window if omitted.  This is equivalent
to using `specifier-instance' and applying `color-instance-name' to
the result.  See `make-specifier' for more information about specifiers."
  (object-property-1 'color-instance-name color domain))

(defun color-rgb-components (color &optional domain)
  "Return the RGB components of the COLOR in the specified DOMAIN, if any.
COLOR should be a color specifier object and DOMAIN is normally a window
and defaults to the selected window if omitted.  This is equivalent
to using `specifier-instance' and applying `color-instance-rgb-components'
to the result.  See `make-specifier' for more information about specifiers."
  (object-property-1 'color-instance-rgb-components color domain))

;;; objects.el ends here.


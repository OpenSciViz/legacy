;;; extents.el --- miscellaneous extent functions not written in C

;;;; Copyright (C) 1993, 1994 Free Software Foundation, Inc.

;; Keywords: internal

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;; an alternative to map-extents.
(defun mapcar-extents (function &optional predicate buffer from to flags
				property value)
  "Applies FUNCTION to all extents which overlap a region in BUFFER.
The region is is delimited by FROM and TO.  A list of the values returned
by FUNCTION is returned.  An optional PREDICATE may be used to further
limit the extents over which FUNCTION is mapped.  The optional arguments
FLAGS, PROPERTY, and VALUE may also be used to control the extents passed
to PREDICATE or FUNCTION.  See also `map-extents'."
  (let (*result*)
    (map-extents (if predicate
                     #'(lambda (ex junk)
                         (and (funcall predicate ex)
                              (setq *result* (cons (funcall function ex)
                                                   *result*)))
                         nil)
                   #'(lambda (ex junk)
                         (setq *result* (cons (funcall function ex)
                                              *result*))
                         nil))
                 buffer from to nil flags property value)
    (nreverse *result*)))

(defun extent-string (extent)
  "Return the string delimited by the bounds of EXTENT."
  ;; by Stig@hackvan.com
  (buffer-substring (extent-start-position extent)
		    (extent-end-position extent)
		    (extent-buffer extent)))

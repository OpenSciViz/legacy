;;; window.el --- XEmacs window commands aside from those written in C.
;; Keywords: extensions

;; Copyright (C) 1985, 1989, 1993, 1994 Free Software Foundation, Inc.

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Synched up with: FSF 19.28.

;;;; Window tree functions.

(defun one-window-p (&optional nomini all-frames)
  "Returns non-nil if the selected window is the only window (in its frame).
Optional arg NOMINI non-nil means don't count the minibuffer
even if it is active.

The optional arg ALL-FRAMES t means count windows on all frames.
If it is `visible', count windows on all visible frames.
ALL-FRAMES nil or omitted means count only the selected frame, 
plus the minibuffer it uses (which may be on another frame).
ALL-FRAMES = 0 means count windows on all visible and iconified frames.
If ALL-FRAMES is any other value, count only the selected frame."
  (let ((base-window (selected-window)))
    (if (and nomini (eq base-window (minibuffer-window)))
	(setq base-window (next-window base-window)))
    (eq base-window
	(next-window base-window (if nomini 'arg) all-frames))))

(defun walk-windows (proc &optional minibuf all-frames)
  "Cycle through all visible windows, calling PROC for each one.
PROC is called with a window as argument.
Optional second arg MINIBUF t means count the minibuffer window
even if not active.  If MINIBUF is neither t nor nil it means
not to count the minibuffer even if it is active.

Optional third arg ALL-FRAMES t means include all windows in all frames;
otherwise cycle within the selected frame."
  ;; If we start from the minibuffer window, don't fail to come back to it.
  (if (window-minibuffer-p (selected-window))
      (setq minibuf t))
  ;; Note that, like next-window & previous-window, this behaves a little 
  ;; strangely if the selected window is on an invisible frame: it hits
  ;; some of the windows on that frame, and all windows on visible frames.
  (let* ((walk-windows-history nil)
	 (walk-windows-current (selected-window)))
    (while (progn
	     (setq walk-windows-current
		   (next-window walk-windows-current minibuf all-frames))
	   q  (not (memq walk-windows-current walk-windows-history)))
      (setq walk-windows-history (cons walk-windows-current
				       walk-windows-history))
      (funcall proc walk-windows-current))))


(defun count-windows (&optional minibuf)
   "Returns the number of visible windows.
Optional arg NO-MINI non-nil means don't count the minibuffer
even if it is active."
   (let ((count 0))
     (walk-windows (function (lambda (w)
			       (setq count (+ count 1))))
		   minibuf)
     count))

(defun balance-windows ()
  "Makes all visible windows the same height (approximately)."
  (interactive)
  (let ((count -1) levels newsizes size)
    ;; Find all the different vpos's at which windows start,
    ;; then count them.  But ignore levels that differ by only 1.
    (save-window-excursion
      (let (tops (prev-top -2))
	(walk-windows (function (lambda (w)
                        (setq tops (cons (nth 1 (window-pixel-edges w))
                                         tops))))
		      'nomini)
	(setq tops (sort tops '<))
	(while tops
	  (if (> (car tops) (1+ prev-top))
	      (setq prev-top (car tops)
		    count (1+ count)))
	  (setq levels (cons (cons (car tops) count) levels))
	  (setq tops (cdr tops)))
	(setq count (1+ count))))
    ;; Subdivide the frame into that many vertical levels.
    (setq size (/ (frame-pixel-height) count))
    (walk-windows (function (lambda (w)
                    (select-window w)
                    (let ((newtop (cdr (assq (nth 1 (window-pixel-edges))
                                             levels)))
                          (newbot (or (cdr (assq
					    (+ (window-pixel-height)
					       (nth 1 (window-pixel-edges)))
					    levels))
                                      count)))
                      (setq newsizes
                            (cons (cons w (* size (- newbot newtop)))
                                  newsizes))))))
    (walk-windows (function (lambda (w)
			      (select-window w)
			      (let ((newsize (cdr (assq w newsizes))))
				(enlarge-window
				 (/ (- newsize (window-pixel-height))
				    (face-height 'default))))))
                  'nomini)))

(defvar split-window-keep-point t
  "*If non-nil, split windows keeps the original point in both children.
This is often more convenient for editing.
If nil, adjust point in each of the two windows to minimize redisplay.
This is convenient on slow terminals, but point can move strangely.")

(defun split-window-vertically (&optional arg)
  "Split current window into two windows, one above the other.
The uppermost window gets ARG lines and the other gets the rest.
Negative arg means select the size of the lowermost window instead.
With no argument, split equally or close to it.
Both windows display the same buffer now current.

If the variable split-window-keep-point is non-nil, both new windows
will get the same value of point as the current window.  This is often
more convenient for editing.

Otherwise, we chose window starts so as to minimize the amount of
redisplay; this is convenient on slow terminals.  The new selected
window is the one that the current value of point appears in.  The
value of point can change if the text around point is hidden by the
new modeline.

Programs should probably use split-window instead of this."
  (interactive "P")
  (let ((old-w (selected-window))
	(old-point (point))
	(size (and arg (prefix-numeric-value arg)))
	new-w bottom)
    (and size (< size 0) (setq size (+ (window-height) size)))
    (setq new-w (split-window nil size))
    (or split-window-keep-point
	(progn
	  (save-excursion
	    (set-buffer (window-buffer))
	    (goto-char (window-start))
	    (vertical-motion (window-height))
	    (set-window-start new-w (point))
	    (if (> (point) (window-point new-w))
		(set-window-point new-w (point)))
	    (vertical-motion -1)
	    (setq bottom (point)))
	  (if (<= bottom (point))
	      (set-window-point old-w (1- bottom)))
	  (if (< (window-start new-w) old-point)
	      (progn
		(set-window-point new-w old-point)
		(select-window new-w)))))
    new-w))

(defun split-window-horizontally (&optional arg)
  "Split current window into two windows side by side.
This window becomes the leftmost of the two, and gets ARG columns.
Negative arg means select the size of the rightmost window instead.
No arg means split equally."
  (interactive "P")
  (let ((size (and arg (prefix-numeric-value arg))))
    (and size (< size 0)
	 (setq size (+ (window-width) size)))
    (split-window nil size t)))

(defun enlarge-window-horizontally (arg)
  "Make current window ARG columns wider."
  (interactive "p")
  (enlarge-window arg t))

(defun shrink-window-horizontally (arg)
  "Make current window ARG columns narrower."
  (interactive "p")
  (shrink-window arg t))

(defun shrink-window-if-larger-than-buffer (&optional window)
  "Shrink the WINDOW to be as small as possible to display its contents.
Do not shrink to less than `window-min-height' lines.
Do nothing if the buffer contains more lines than the present window height,
or if some of the window's contents are scrolled out of view,
or if the window is not the full width of the frame,
or if the window is the only window of its frame."
  (interactive)
  (save-excursion
    (set-buffer (window-buffer window))
    (let ((w (selected-window)) ;save-window-excursion can't win
	  (buffer-file-name buffer-file-name)
	  (p (point))
	  (n 0)
	  (ignore-final-newline
	   ;; If buffer ends with a newline, ignore it when counting height
	   ;; unless point is after it.
	   (and (not (eobp))
		(eq ?\n (char-after (1- (point-max))))))
	  (buffer-read-only nil)
	  (modified (buffer-modified-p))
	  (buffer (current-buffer)))
      (if (and (< 1 (count-windows))
	       ;; check to make sure that we don't have horizontally
	       ;; split windows
	       (eq (frame-highest-window (selected-frame) 0)
		   (frame-highest-window (selected-frame) -1))
	       (pos-visible-in-window-p (point-min) window)
	       ;; #### there needs to be a check here for a global
	       ;; minibuffer
	       )
	  (unwind-protect
	      (progn
		(select-window (or window w))
		(goto-char (point-min))
		(while (pos-visible-in-window-p
			(- (point-max)
			   (if ignore-final-newline 1 0)))
		  ;; defeat file locking... don't try this at home, kids!
		  (setq buffer-file-name nil)
		  (insert ?\n) (setq n (1+ n)))
		(if (> n 0)
		    (shrink-window (min (1- n)
					(- (window-height)
					   window-min-height)))))
	    (delete-region (point-min) (point))
	    (set-buffer-modified-p modified)
	    (goto-char p)
	    (select-window w)
	    ;; Make sure we unbind buffer-read-only
	    ;; with the proper current buffer.
	    (set-buffer buffer))))))

(defun backward-other-window (arg &optional all-frames invisible-too)
  "Select the ARG'th different window on this frame, going backwards.
This is just like calling `other-window' with the arg negated."
  (interactive "p")
  (other-window (- arg) all-frames invisible-too))

(defun windows-of-buffer (&optional buffer)
  "Returns a list of windows that have BUFFER in them.
If BUFFER is not specified, the current buffer will be used."
  (or (bufferp buffer)
      (if (stringp buffer)
	  (setq buffer (or (get-buffer buffer)
			   (get-file-buffer buffer)))
	(setq buffer (current-buffer))))
  (let* ((firstwin (next-window nil nil t))
	 (wind firstwin) 
	 (done nil)
	 window-list)
    (while (not done)
      (if (eq (window-buffer wind) buffer)
	  (setq window-list (append window-list (list wind))))
      (setq wind (next-window wind nil t))
      (setq done (eq wind firstwin)))
    window-list))

(defun buffer-in-multiple-windows-p (&optional buffer)
  "Returns t if BUFFER is in multiple windows.
If BUFFER is not specified, the current buffer will be used."
  (setq buffer (or buffer
		   (get-buffer buffer)
		   (get-file-buffer buffer)
		   (current-buffer)))
  (> (length (windows-of-buffer buffer)) 1))

(defun window-list (&optional frame minibuf window)
  "Return a list of windows on FRAME, beginning with WINDOW.
FRAME and WINDOW default to the selected ones.  
Optional second arg MINIBUF t means count the minibuffer window
even if not active.  If MINIBUF is neither t nor nil it means
not to count the minibuffer even if it is active."
  (setq window (or window (selected-window))
	frame (or frame (selected-frame)))
  (if (not (eq (window-frame window) frame))
      (error "Window must be on frame."))
  (let ((current-frame (selected-frame))
	list)
    (unwind-protect
	(save-window-excursion
	  (select-frame frame)
	  (walk-windows
	   (function (lambda (cur-window)
		       (if (not (eq window cur-window))
			   (setq list (cons cur-window list)))))
	   minibuf)
	  (setq list (cons window list)))
      (select-frame current-frame))))

;; FSF compatibility function
(defun set-window-dedicated-p (window arg)
  "Control whether WINDOW is dedicated to the buffer it displays.
If it is dedicated, Emacs will not automatically change
which buffer appears in it.
The second argument is the new value for the dedication flag;
non-nil means yes."
  (let ((buffer (if arg (window-buffer window) nil)))
    (set-window-buffer-dedicated window buffer)))

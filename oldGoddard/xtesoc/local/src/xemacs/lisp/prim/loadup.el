;;; loadup.el --- load up standardly loaded Lisp files for XEmacs.
;; Keywords: internal

;; It is not a good idea to edit this file.  Use site-init.el or site-load.el
;; instead.
;;
;; This is loaded into a bare XEmacs to make a dumpable one.
;; Copyright (C) 1985, 1986, 1992, 1993, 1994 Free Software Foundation, Inc.

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(if (fboundp 'error)
    (error "loadup.el already loaded!"))

(call-with-condition-handler
      ;; This is awfully damn early to be getting an error, right?
      'really-early-error-handler
 #'(lambda ()
     ;; We don't want to have any undo records in the dumped XEmacs.
     (buffer-disable-undo (get-buffer "*scratch*"))

     ;; lread.c (or src/Makefile.in.in) has prepended "${srcdir}/../lisp/prim"
     ;; to load-path, which is how this file has been found.  At this point,
     ;; enough of emacs has been initialized that we can call directory-files
     ;; and get the rest of the dirs (so that we can dump stuff from modes/
     ;; and packages/.)
     ;;
     (let ((temp-path (expand-file-name ".." (car load-path))))
       (setq load-path (nconc (directory-files temp-path t "^[^-.]"
					       nil 'dirs-only)
			      (cons temp-path load-path))))

     (setq load-warn-when-source-newer t ; set to nil at the end
	   load-warn-when-source-only t)

     (load "version.el")	;Ignore compiled-by-mistake version.elc
     ;; garbage collect after loading every file in an attempt to
     ;; minimize the size of the dumped image (if we don't do this,
     ;; there will be lots of extra space in the data segment filled
     ;; with garbage-collected junk)
     (garbage-collect)
     (load "backquote") ; needed for defsubst etc.
     (garbage-collect)
     (load "bytecomp-runtime")	; define defsubst
     (garbage-collect)
     (load "subr") ;; now load the most basic Lisp functions
     (garbage-collect)
     (load "cmdloop")
     (or (fboundp 'recursive-edit) (load "cmdloop1"))
     (garbage-collect)
     (load "keymap")
     (garbage-collect)
     (load "syntax")
     (garbage-collect)
     (load "minibuf")
     (garbage-collect)
     (load "specifier")
     (garbage-collect)
     (load "faces")		; must be loaded before any make-face call
     (garbage-collect)
     (load "glyphs")
     (garbage-collect)
     (load "objects")
     (garbage-collect)
     (load "extents")
     (garbage-collect)
     (load "process")
     (garbage-collect)
     (load "device")
     (garbage-collect)
     (load "obsolete")
     (garbage-collect)
     (load "keydefs.el")	; Before loaddefs so that keymap vars exist.
     (garbage-collect)
     ;; If SparcWorks support is included some additional packages are
     ;; dumped which would normally have autoloads.  To avoid
     ;; duplicate doc string warnings, SparcWorks uses a separate
     ;; autoloads file with the dumped packages removed.
     (if (featurep 'sparcworks)
	 (load "eos/loaddefs-eos.el")
       (load "loaddefs.el"))	; don't get confused if loaddefs compiled.
     (garbage-collect)
     (load "simple")
     (garbage-collect)
     (load "misc")
     (garbage-collect)
     (load "help")
     (garbage-collect)
     ;; (load "hyper-apropos")  Soon...
     ;; (garbage-collect)
     (load "files")
     (garbage-collect)
     (load "indent")
     (garbage-collect)
     (load "window")
     (garbage-collect)
     (load "paths.el")		; don't get confused if paths compiled.
     (garbage-collect)
     (load "startup")
     (garbage-collect)
     (load "lisp")
     (garbage-collect)
     (load "page")
     (garbage-collect)
     (load "register")
     (garbage-collect)
     (load "iso8859-1")		; This must be before any modes
				; (sets standard syntax table.)
     (garbage-collect)
     (load "paragraphs")
     (garbage-collect)
     (load "lisp-mode")
     (garbage-collect)
     (load "text-mode")
     (garbage-collect)
     (load "fill")
     (garbage-collect)
     (load "isearch-mode")
     (garbage-collect)
     (load "cc-mode")
     (garbage-collect)
     (load "vc-hooks")
     (garbage-collect)
     (load "ediff-hook")
     (garbage-collect)
     (load "replace")
     (garbage-collect)
     (if (eq system-type 'vax-vms)
	 (progn
	   (load "vmsproc")
	   (garbage-collect)))
     (load "abbrev")
     (garbage-collect)
     (load "buff-menu")
     (garbage-collect)
     (if (eq system-type 'vax-vms)
	 (progn
	   (load "vms-patch")
	   (garbage-collect)))
     (if (featurep 'lisp-float-type)	; preload some constants and 
	 (progn				; floating pt. functions if 
	   (load "float-sup")		; we have float support.
	   (garbage-collect)))

     (load "itimer")	; for vars auto-save-timeout and auto-gc-threshold
     (garbage-collect)
     (load "frame")
     (garbage-collect)
     (load "toolbar")
     (garbage-collect)
     (load "scrollbar")
     (garbage-collect)
     ;; At some point we hope that this will not be necessary.
     (if (featurep 'x)
	 (progn
	   (load "menubar")
	   (garbage-collect)))
     (if (featurep 'x)
	 ;; preload the X code, for faster startup.
	 (progn
	   (load "x-menubar")
	   (garbage-collect)
;;; autoload this.
;;;            (load "x-font-menu")
;;;            (garbage-collect)
	   (if (featurep 'dialog)
	       (progn
		 (load "dialog")
		 (garbage-collect)))
	   (load "x-faces")
	   (garbage-collect)
	   (load "x-iso8859-1")
	   (garbage-collect)
	   (load "x-mouse")
	   (garbage-collect)
	   (load "x-select")
	   (garbage-collect)
	   (load "x-scrollbar")
	   (garbage-collect)
	   (load "x-misc")
	   (garbage-collect)
	   ))

     (if (featurep 'tooltalk)
	 (progn
	   (load "tooltalk/tooltalk-load")
	   (garbage-collect)))

     (if (featurep 'energize)
	 (progn
	   (load "energize/energize-load.el")
	   (garbage-collect)))

     (if (featurep 'sparcworks)
	 (progn
	   (load "sunpro/sunpro-load.el")
	   (garbage-collect)))

     )) ;; end of call-with-condition-handler


(setq load-warn-when-source-newer nil ; set to t at top of file
      load-warn-when-source-only nil)

(setq debugger 'debug)

(if (or (equal (nth 4 command-line-args) "no-site-file")
	(equal (nth 5 command-line-args) "no-site-file"))
    (setq site-start-file nil))

;;; If you want additional libraries to be preloaded and their
;;; doc strings kept in the DOC file rather than in core,
;;; you may load them with a "site-load.el" file.
;;; But you must also cause them to be scanned when the DOC file
;;; is generated.  For VMS, you must edit ../../vms/makedoc.com.
;;; For other systems, you must edit ../../src/Makefile.in.in.
(if (load "site-load" t)
    (garbage-collect))

;;; Note: all compiled Lisp files loaded above this point
;;; must be among the ones parsed by make-docfile
;;; to construct DOC.  Any that are not processed
;;; for DOC will not have doc strings in the dumped XEmacs.

;;; Don't bother with these if we're running temacs, i.e. if we're
;;; just debugging don't waste time finding doc strings.

(if (or (equal (nth 3 command-line-args) "dump")
	(equal (nth 4 command-line-args) "dump"))
    (progn
      (message "Finding pointers to doc strings...")
      (if (fboundp 'dump-emacs)
	  (let ((name emacs-version))
 	    (string-match " Lucid" name)
 	    (setq name (concat (substring name 0 (match-beginning 0))
 			       (substring name (match-end 0))))
	    (while (string-match "[^-+_.a-zA-Z0-9]+" name)
	      (setq name (concat
			  (downcase (substring name 0 (match-beginning 0)))
			  "-"
			  (substring name (match-end 0)))))
	    (if (string-match "-+\\'" name)
		(setq name (substring name 0 (match-beginning 0))))
	    (copy-file (expand-file-name "DOC" "../lib-src")
		       (expand-file-name (concat "DOC-" name) "../lib-src")
		       t)
	    (Snarf-documentation (concat "DOC-" name)))
	(Snarf-documentation "DOC"))
      (message "Finding pointers to doc strings...done")
      (Verify-documentation)
      ))

;;; Note: You can cause additional libraries to be preloaded
;;; by writing a site-init.el that loads them.
;;; See also "site-load" above.
(if (stringp site-start-file)
    (load "site-init" t))

;;; At this point, we're ready to resume undo recording for scratch.
(buffer-enable-undo "*scratch*")

(if (and (eq system-type 'vax-vms)
	 (or (equal (nth 3 command-line-args) "dump")
	     (equal (nth 4 command-line-args) "dump")))
    (progn
      (setq command-line-args nil)
      (message "Dumping data as file temacs.dump")
      (dump-emacs "temacs.dump" "temacs")
      (kill-emacs)))

(if (or (equal (nth 3 command-line-args) "dump")
	(equal (nth 4 command-line-args) "dump"))
    (progn
      (let ((name (concat "emacs-" emacs-version)))
 	(string-match " Lucid" name)
 	(setq name (concat (substring name 0 (match-beginning 0))
 			   (substring name (match-end 0))))
	(while (string-match "[^-+_.a-zA-Z0-9]+" name)
	  (setq name (concat (downcase (substring name 0 (match-beginning 0)))
			     "-"
			     (substring name (match-end 0)))))
	(if (string-match "-+\\'" name)
	    (setq name (substring name 0 (match-beginning 0))))
	(message "Dumping under names xemacs and %s" name))
      (condition-case ()
	  (delete-file "xemacs")
	(file-error nil))
      (if (fboundp 'really-free)
	  (really-free))
      (dump-emacs "xemacs" "temacs")
      ;; Recompute NAME now, so that it isn't set when we dump.
      (let ((name (concat "emacs-" emacs-version)))
 	(string-match " Lucid" name)
 	(setq name (concat (substring name 0 (match-beginning 0))
 			   (substring name (match-end 0))))
	(while (string-match "[^-+_.a-zA-Z0-9]+" name)
	  (setq name (concat (downcase (substring name 0 (match-beginning 0)))
			     "-"
			     (substring name (match-end 0)))))
	(if (string-match "-+\\'" name)
	    (setq name (substring name 0 (match-beginning 0))))
	(add-name-to-file "xemacs" name t))
      (kill-emacs)))

(if (or (equal (nth 3 command-line-args) "run-temacs")
	(equal (nth 4 command-line-args) "run-temacs"))
    (progn
      ;; purify-flag is nil if called from loadup-el.el.
      (if purify-flag
	  (progn
	    (message "\nSnarfing doc...")
	    (Snarf-documentation "DOC")
	    (Verify-documentation)))
      (message "\nBootstrapping from temacs...")
      (setq purify-flag nil)
      (apply #'run-emacs-from-temacs
	     (nthcdr (if (equal (nth 3 command-line-args) "run-temacs")
			 4 5)
		     command-line-args))
      ;; run-emacs-from-temacs doesn't actually return anyway.
      (kill-emacs)))

;;; Avoid error if user loads some more libraries now.
(setq purify-flag nil)

;;; If you are using 'recompile', then you should have used -l loadup-el.el
;;; so that the .el files always get loaded (the .elc files may be out-of-
;;; date or bad).
(if (or (equal (nth 3 command-line-args) "recompile")
	(equal (nth 4 command-line-args) "recompile"))
    (progn
      (let ((command-line-args-left
	     (nthcdr (if (equal (nth 3 command-line-args) "recompile")
			 4 5)
		     command-line-args)))
	(batch-byte-recompile-directory)
	(kill-emacs))))


;;; For machines with CANNOT_DUMP defined in config.h,
;;; this file must be loaded each time XEmacs is run.
;;; So run the startup code now.

(or (fboundp 'dump-emacs)
    (eval top-level))

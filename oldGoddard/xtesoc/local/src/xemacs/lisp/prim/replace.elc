;ELC   
;;; compiled by cthomp@willow.cs.uiuc.edu on Tue Aug 29 14:22:01 1995
;;; from file /xemacs/xemacs-19.13-release/editor/lisp/prim/replace.el
;;; emacs version 19.13 XEmacs Lucid.
;;; bytecomp version 2.25; 1-Sep-94.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19")))
    (error "This file was compiled for Emacs 19."))

(defvar case-replace t "\
*Non-nil means `query-replace' should preserve case in replacements.
What this means is that `query-replace' will change the case of the
replacement text so that it matches the text that was replaced.
If this variable is nil, the replacement text will be inserted
exactly as it was specified by the user, irrespective of the case
of the text that was replaced.

Note that this flag has no effect if `case-fold-search' is nil,
or if the replacement text has any uppercase letters in it.")
(byte-code "��!�����M�" [boundp query-replace-history nil query-replace-read-args #[(string) "������!\"����%����!\n#����%\n	\nE*�" [nil to from read-from-minibuffer format "%s: " gettext string query-replace-history "%s %s with: " current-prefix-arg] 6]] 2)
(fset 'query-replace #[(from-string to-string &optional arg) "�	\n��%�����!�" [perform-replace from-string to-string t nil arg unread-command-event message "Done"] 6 "\
Replace some occurrences of FROM-STRING with TO-STRING.
As each match is found, the user must type a character saying
what to do with it.  For directions, type \\[help-command] at that time.

Preserves case in each replacement if `case-replace' and `case-fold-search'
are non-nil and FROM-STRING has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.

To customize possible responses, change the \"bindings\" in `query-replace-map'." (query-replace-read-args "Query replace")])
(fset 'query-replace-regexp #[(regexp to-string &optional arg) "�	\nÉ%�����!�" [perform-replace regexp to-string t arg unread-command-event message "Done"] 6 "\
Replace some things after point matching REGEXP with TO-STRING.
As each match is found, the user must type a character saying
what to do with it.  For directions, type \\[help-command] at that time.

Preserves case in each replacement if `case-replace' and `case-fold-search'
are non-nil and REGEXP has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.
In TO-STRING, `\\&' stands for whatever matched the whole of REGEXP,
and `\\=\\N' (where N is a digit) stands for
 whatever what matched the Nth `\\(...\\)' in REGEXP." (query-replace-read-args "Query replace regexp")])
(fset 'map-query-replace-regexp #[(regexp to-strings &optional arg) "�\n<��\n��\nG�U����\n\"���	\n���\n\"OC\"\n��\n\"T�O��]�	\nC\"ǉ�R�		ʉ�&�)����!�" [nil replacements to-strings 0 string-match " " append "" perform-replace regexp t arg unread-command-event message "Done"] 8 "\
Replace some matches for REGEXP with various strings, in rotation.
The second argument TO-STRINGS contains the replacement strings, separated
by spaces.  This command works like `query-replace-regexp' except
that each successive replacement uses the next successive replacement string,
wrapping around from the last such string to the first.

Non-interactively, TO-STRINGS may be a list of replacement strings.

A prefix argument N says to use each replacement string N times
before rotating to the next." (byte-code "��������%���\n\"����%\n	E*�" [nil to from read-from-minibuffer "Map query replace (regexp): " query-replace-history format "Query replace %s with (space-separated strings): " current-prefix-arg] 6)])
(fset 'replace-string #[(from-string to-string &optional delimited) "�	\nÉ%�����!�" [perform-replace from-string to-string nil delimited unread-command-event message "Done"] 6 "\
Replace occurrences of FROM-STRING with TO-STRING.
Preserve case in each match if `case-replace' and `case-fold-search'
are non-nil and FROM-STRING has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.

This function is usually the wrong thing to use in a Lisp program.
What you probably want is a loop like this:
  (while (search-forward FROM-STRING nil t)
    (replace-match TO-STRING nil t))
which will run faster and will not set the mark or print anything." (query-replace-read-args "Replace string")])
(fset 'replace-regexp #[(regexp to-string &optional delimited) "�	\n��%�����!�" [perform-replace regexp to-string nil t delimited unread-command-event message "Done"] 6 "\
Replace things after point matching REGEXP with TO-STRING.
Preserve case in each match if `case-replace' and `case-fold-search'
are non-nil and REGEXP has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.
In TO-STRING, `\\&' stands for whatever matched the whole of REGEXP,
and `\\=\\N' (where N is a digit) stands for
 whatever what matched the Nth `\\(...\\)' in REGEXP.

This function is usually the wrong thing to use in a Lisp program.
What you probably want is a loop like this:
  (while (re-search-forward REGEXP nil t)
    (replace-match TO-STRING nil nil))
which will run faster and will not set the mark or print anything." (query-replace-read-args "Replace regexp")])
(defvar regexp-history nil "\
History list for some commands that read regular expressions.")
(define-function 'keep-lines 'delete-non-matching-lines)
(fset 'delete-non-matching-lines #[(regexp) "�n���y�`m?�����#��	d|����Ɣb��y�`)	W��	|�)��y�`)m�MƔƕU�F�u��A*�" [1 start re-search-forward regexp nil move 0 end] 4 "\
Delete all lines except those containing matches for REGEXP.
A match split across lines preserves all the lines it lies in.
Applies to all lines after point." (byte-code "����%C�" [read-from-minibuffer "Keep lines (containing match for regexp): " nil regexp-history] 6)])
(define-function 'flush-lines 'delete-matching-lines)
(fset 'delete-matching-lines #[(regexp) "�m?���	��#���Ĕb��y�`)�y�`|��c)�" [re-search-forward regexp nil t 0 1] 4 "\
Delete lines containing matches for REGEXP.
If a match is split across lines, all the lines it lies in are deleted.
Applies to lines after point." (byte-code "����%C�" [read-from-minibuffer "Flush lines (containing match for regexp): " nil regexp-history] 6)])
(define-function 'how-many 'count-matches)
(fset 'count-matches #[(regexp) "���m��`���#��\n`U���u��jT�e��\"+�" [0 nil opoint count re-search-forward regexp t 1 message "%d occurrences"] 4 "\
Print number of matches for REGEXP following point." (byte-code "����%C�" [read-from-minibuffer "How many matches for (regexp): " nil regexp-history] 6)])
(byte-code "��!���	��� �	�\"��	��#��	��#���!���\n��!�����!���" [boundp occur-mode-map nil make-sparse-keymap set-keymap-name define-key "" occur-mode-goto-occurrence button2 occur-mode-mouse-goto occur-buffer occur-nlines occur-pos-list] 4)
(fset 'occur-mode #[nil "� ��\n!����!��!���!���!���!����!�" [kill-all-local-variables use-local-map occur-mode-map occur-mode major-mode gettext "Occur" mode-name make-local-variable occur-buffer occur-nlines occur-pos-list require mode-motion mode-motion-highlight-line mode-motion-hook run-hooks occur-mode-hook] 2 "\
Major mode for output from \\[occur].
Move point to one of the occurrences in this buffer,
then use \\[occur-mode-goto-occurrence] to go to the same occurrence
Or click \\<occur-mode-map>\\[occur-mode-mouse-goto] on an occurrence line.
in the buffer that the occurrences were found in.
\\{occur-mode-map}"])
(fset 'occur-mode-mouse-goto #[(e) "� � Ď�!�� +�" [selected-window selected-frame frame-save window-save ((byte-code "�	!��!�" [select-frame frame-save select-window window-save] 2)) mouse-set-point e occur-mode-goto-occurrence] 2 "\
go to occurence highlighted by mouse.  This function is only reasonable when bound
to a mouse key in the occur buffer" "e"])
(fset 'occur-mode-goto-occurrence #[nil "���!������!��e��y�`)\"��y�S	�W���	Z��	�V��	�\\�\\��˥)�8��\"�W����!�����!����!����!!��q���!����\"b��\"-�" [occur-buffer buffer-name nil occur-pos-list error "Buffer in which occurrences were found is deleted" count-lines 0 line-count occur-nlines 2 1 occur-number pos get-buffer-window t window occur-source-buffer "No occurrence on this line" window-live-p frame-visible-p window-frame pop-to-buffer set-window-point] 4 "\
Go to the occurrence the current line describes." nil])
(defvar list-matching-lines-default-context-lines 0 "\
*Default number of context lines to include around a `list-matching-lines'
match.  A negative number means to include that many lines before the match.
A positive number means to include that many lines both before and after.")
(defvar list-matching-lines-whole-buffer t "\
If t, occur operates on whole buffer, otherwise occur starts from point.
default is nil.")
(define-function 'occur 'list-matching-lines)
(fset 'list-matching-lines #[(regexp &optional nlines) "������!����!���p�e� \n����y��e`\"T`)Ґ�q����!#c�� ��)=��db����� ����\"!�dU�����#��Дb��y�� !��`\"\\*`Еb��Дb��W����[y�`)#�Еb��V��Ty����y�`)$��\"&�&G�\")�*�� �*`ړ�q�*B���!���c���#$#��$#Z!��W��[���*�V��)���y�*S�*�k0\n`ЕДZ\\ړ�`\nW��&����0\"&&���&�y�0T0�\\)*W��)���y�*T�*�k)�y�-�o q��t����G\")�-�" [regexp "" error "Must pass non-empty regexp to `list-matching-lines'" nlines prefix-numeric-value list-matching-lines-default-context-lines t 1 make-marker final-context-start prevpos linenum buffer first list-matching-lines-whole-buffer 0 count-lines "*Occur*" standard-output format "Lines matching %s in buffer %s.\n" buffer-name occur-mode occur-buffer occur-nlines nil occur-pos-list beginning-of-buffer message "Searching for %s ..." re-search-forward match-data _match_data_ ((store-match-data _match_data_)) start end "%3d" tag make-string 32 empty tem zerop "--------\n" insert-buffer-substring backward-char 58 this-linenum "%d matching lines."] 7 "\
Show all lines in the current buffer containing a match for REGEXP.

If a match spreads across multiple lines, all those lines are shown.

If variable `list-matching-lines-whole-buffer' is non-nil, the entire buffer is
searched, otherwise search begins at point.

Each line is displayed with NLINES lines before and after,
or -NLINES before if NLINES is negative.
NLINES defaults to `list-matching-lines-default-context-lines'.
Interactively it is the prefix arg.

The lines are shown in a buffer named `*Occur*'.
It serves as a menu to find any of the occurrences in this buffer.
\\[describe-mode] in that buffer will explain how." (byte-code "� ��	��	@�\n�����\n\"ȉ��%����ȉ��%�\n˚��\n��\n\n	\n��	�	@	A\"��\n+D�" [symbol-near-point regexp-history default 0 minibuffer-history-minimum-string-length read-from-minibuffer format "List lines matching regexp (default `%s'): " nil "List lines matching regexp: " input "" delete current-prefix-arg] 7)])
(defvar query-replace-help (purecopy "Type Space or `y' to replace one match, Delete or `n' to skip to next,\nRET or `q' to exit, Period to replace one match and exit,\nComma to replace but not move point immediately,\nC-r to enter recursive edit (\\[exit-recursive-edit] to get out again),\nC-w to delete match and recursive edit,\nC-l to clear the frame, redisplay, and offer same replacement again,\n! to replace all remaining matches with no more questions,\n^ to move point back to previous match.") "\
Help message while in query-replace")
(defvar query-replace-map nil "\
Keymap that defines the responses to questions in `query-replace'.
The \"bindings\" in this map are not commands; they are answers.
The valid answers include `act', `skip', `act-and-show',
`exit', `act-and-exit', `edit', `delete-and-edit', `recenter',
`automatic', `backup', and `help'.")
(byte-code "�� � �\n�\"��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#��\n��#�\n)���\"���M�" [query-replace-map make-sparse-keymap map set-keymap-name define-key " " act "" skip [delete] [backspace] "y" "n" "," act-and-show [escape] exit "q" [return] "." act-and-exit "" edit "" delete-and-edit "" recenter "!" automatic "^" backup [(control h)] help "?" "" quit "" autoload isearch-highlight "isearch" perform-replace-next-event #[(event) "���Î�Ĕĕ\"��!�ǉ*��!�" [isearch-highlight t aborted ((isearch-dehighlight aborted)) 0 next-command-event event nil] 3]] 4)
(fset 'perform-replace #[(from-string replacements query-flag regexp-flag delimited-flag &optional repeat-count map) "��	� �������??��Ǫ��ɉ���ʉ������!;�������!���������!�Q� �� ���m����#��`=����ʔ=����m���u���#��˃�� ���!�!�<��U��\\8TG����!��#�T��� ��ɉ�+,-.-���#��!���!\"+�!�+�=����!����������$�:!P!���=+�=���ˉ-�0+�=����@�=@b�=A�=�,���=A!�A)���!���@ !��A �!��+�B =��,���#��-�,�+�C =��,���#���-�,�+�D =��,��#��,�+�E =��,���#��-��,�+�F =��ˉ-�+�G =���G �!��+�H =���� ��I  �)!���!�+�J =��ʔʕ|��� ��I  �)!��,��Kˉ-�`,���L �M � \"BB,��T,`�z ��.�" [map query-replace-map allocate-event case-fold-search case-replace from-string regexp-flag re-search-forward search-forward nil 0 t query-flag substitute-command-keys "Query replacing %s with %s: (\\<query-replace-map>\\[help] for help) " message match-again lastrepl replace-count next-rotate-count stack keep-going replacement-index next-replacement real-match-data search-string search-function literal nocasify event replacements repeat-count 1 delimited-flag "\\b" regexp-quote push-mark undo-boundary match-data looking-at store-match-data replace-match (concat (format "Query replacing %s%s with %s.\n\n" (if regexp-flag (gettext "regexp ") "") from-string next-replacement) (substitute-command-keys query-replace-help)) def replaced done help-form perform-replace-next-event lookup-key vector help gettext "*Help*" princ format "Query replacing %s%s with %s.\n\n" "regexp " "" query-replace-help exit backup elt "No previous match" ding no-terminate sit-for act act-and-exit act-and-show automatic skip recenter edit recursive-edit delete-and-edit unread-command-event mapcar #[(elt) "�	!���	!	É���	�" [markerp elt marker-position nil] 4]] 17 "\
Subroutine of `query-replace'.  Its complexity handles interactive queries.
Don't use this in your own program unless you want to query and set the mark
just as `query-replace' does.  Instead, write a simple loop like this:
  (while (re-search-forward \"foo[ 	]+bar\" nil t)
    (replace-match \"foobar\" nil nil))
which will run faster and probably do exactly what you want."])
(fset 'match-string '(macro . #[(n &optional string) "��\nD����\nD�\nDF����\nD�\nDEE�" [and match-beginning n string substring match-end buffer-substring] 7 "\
Returns the Nth subexpression matched by the last regular expression
search.  The second argument, STRING, must be specified if the last
regular expression search was done with `string-match'."]))
(fset 'save-match-data '(macro . #[(&rest forms) "����B�EE�" [let ((_match_data_ (match-data))) unwind-protect progn forms (store-match-data _match_data_)] 5 "\
Execute FORMS, restoring the global value of the match data."]))

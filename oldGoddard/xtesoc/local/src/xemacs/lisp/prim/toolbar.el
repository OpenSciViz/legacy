;; Toolbar support.
;; Copyright (C) 1995 Board of Trustees, University of Illinois

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defvar toolbar-help-enabled t
  "If non-nil help is echoed for toolbar buttons.")

(defvar toolbar-icon-directory nil
  "Location of standard toolbar icon bitmaps.")

(defun toolbar-make-button-list (up &optional down disabled cap-up cap-down cap-disabled)
  "Calls make-glyph on each arg and returns a list of the results."
  (if (featurep 'x)
      (let ((up-glyph (make-glyph up))
	    (down-glyph (and down (make-glyph down)))
	    (disabled-glyph (and disabled (make-glyph disabled)))
	    (cap-up-glyph (and cap-up (make-glyph cap-up)))
	    (cap-down-glyph (and cap-down (make-glyph cap-down)))
	    (cap-disabled-glyph (and cap-disabled (make-glyph cap-disabled))))
	(if cap-disabled
	    (list up-glyph down-glyph disabled-glyph
		  cap-up-glyph cap-down-glyph cap-disabled-glyph)
	  (if cap-down
	    (list up-glyph down-glyph disabled-glyph
		  cap-up-glyph cap-down-glyph)
	    (if cap-up
		(list up-glyph down-glyph disabled-glyph cap-up-glyph)
	      (if disabled-glyph
		  (list up-glyph down-glyph disabled-glyph)
		(if down-glyph
		    (list up-glyph down-glyph)
		  (list up-glyph)))))))
    nil))

(defun init-toolbar-location ()
  (if (not toolbar-icon-directory)
      (setq toolbar-icon-directory
	    (file-name-as-directory
	     (expand-file-name "toolbar" data-directory)))))

(defun init-toolbar-from-resources (locale)
  (if (and (featurep 'x)
	   (or (eq locale 'global)
	       (eq 'x (device-or-frame-type locale)))
	   (x-init-toolbar-from-resources locale))))


(if (featurep 'x)
    (provide 'toolbar))

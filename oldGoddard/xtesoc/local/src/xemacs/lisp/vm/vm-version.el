(provide 'vm)

(provide 'vm-version)

(defconst vm-version "5.95 (beta)"
  "Version number of VM.")

(defun vm-version ()
  "Returns the value of the variable vm-version."
  vm-version)

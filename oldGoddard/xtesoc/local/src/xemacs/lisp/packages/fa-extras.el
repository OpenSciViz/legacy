;; fa-extras.el has been obsoleted by the latest version of
;; filladapt.el.  We keep this stub here in order to prevent breaking
;; .emacs which have a (require 'fa-extras) in them.

(require 'filladapt)
(provide 'fa-extras)

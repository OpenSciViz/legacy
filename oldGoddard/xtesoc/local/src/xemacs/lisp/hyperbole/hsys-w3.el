;;!emacs
;;
;; FILE:         hsys-w3.el
;; SUMMARY:      Hyperbole support for Emacs W3 World-Wide Web (WWW) browsing.
;; USAGE:        GNU Emacs Lisp Library
;; KEYWORDS:     comm, help, hypermedia
;;
;; AUTHOR:       Bob Weiner
;; ORG:          Motorola Inc.
;;
;; ORIG-DATE:     7-Apr-94 at 17:17:39 by Bob Weiner
;; LAST-MOD:     14-Apr-95 at 16:08:59 by Bob Weiner
;;
;; This file is part of Hyperbole.
;; Available for use and distribution under the same terms as GNU Emacs.
;;
;; Copyright (C) 1994-1995, Free Software Foundation, Inc.
;; Developed with support from Motorola Inc.
;;
;; DESCRIPTION:  
;;
;;   This module defines an implicit button type and associated action and
;;   help types.  A press of the Action Key on a unified resource locator
;;   (URL) displays the referent for the URL.  A press of the Help Key on a
;;   URL displays a history list of previously browsed WWW documents.  Press
;;   the Action Key on any item from the history list to display it.
;;
;;   This requires the Emacs W3 World-Wide-Web browser available from:
;;     "/anonymous@cs.indiana.edu:/pub/elisp/w3".
;;
;;   It assumes that you have set up to have w3 auto-loaded according to the
;;   setup instructions included with W3.  Specifically,
;;   'w3-follow-url-at-point' should be autoloaded.
;;
;; DESCRIP-END.

;;; ************************************************************************
;;; Other required Elisp libraries
;;; ************************************************************************

;;; Requires 'w3' libraries.

;;; ************************************************************************
;;; Public variables
;;; ************************************************************************

(defib hw3:link-follow ()
  "When not in a w3 buffer, follow any url (link) at point."
  (if (not (eq major-mode 'w3-mode))
      (let ((link-and-pos (hpath:www-at-p t)))
	(if link-and-pos
	    (progn (ibut:label-set link-and-pos)
		   (hact 'hw3:link-follow (car link-and-pos)))))))

(defact hw3:link-follow (link-label)
  "Follows a link given by LINK-LABEL."
  (interactive "sURL to follow: ")
  (or (stringp link-label)
      (error "(hw3:link-follow): Link label must be given as a string."))
  (w3-fetch link-label))

(defun hw3:link-follow:help (&optional but)
  "Displays history list of www nodes previously visited."
  (interactive)
  (if (fboundp 'w3-show-history-list)
      (hact 'w3-show-history-list)
    (hact 'error "(hw3:link-follow:help): W3 must be loaded to display WWW history")))

(provide 'hsys-w3)

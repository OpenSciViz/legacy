;;; eos.el --- Intereactively loads the XEmacs/SPARCworks interface
;;; this file is an alias for sun-eos.el

;; Copyright (C) 13 Jun 1995  Sun Microsystems, Inc.

;; Maintainer:	Eduardo Pelegri-Llopart <eduardo.pelegri-llopart@Eng.Sun.COM>
;; Author:      Eduardo Pelegri-Llopart <eduardo.pelegri-llopart@Eng.Sun.COM>
;; Version:	1.1
;; Header:	@(#) eos.el: v1.1 95/06/15 19:02:21

;; Keywords:	SPARCworks EOS Era on SPARCworks load

;;; Commentary:

;; If manual loading is desired...
;; Please send feedback to eduardo.pelegri-llopart@eng.sun.com

;;; Code:

(load "sun-eos-load.el")
(eos::start)

;;; sun-eos-eos.el ends here

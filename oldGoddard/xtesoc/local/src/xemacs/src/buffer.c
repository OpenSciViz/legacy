/* Buffer manipulation primitives for XEmacs.
   Copyright (C) 1985-1989, 1992-1994 Free Software Foundation, Inc.
   Copyright (C) 1995 Sun Microsystems.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Mule 2.0, FSF 19.28. */

/* This file contains functions that work with buffer objects.
   Functions that manipulate a buffer's text, however, are not
   in this file:

   1) The low-level functions that actually know about the
      implementation of a buffer's text are located in insdel.c.
   2) The higher-level (mostly Lisp) functions that manipulate a
      buffer's text are in editfns.c.
   3) The highest-level Lisp commands are in cmds.c.

   However:

   -- Functions that know about syntax tables (forward-word,
      scan-sexps, etc.) are in syntax.c, as are functions
      that manipulate syntax tables.
   -- Functions that know about case tables (upcase, downcase,
      etc.) are in casefiddle.c.  Functions that manipulate
      case tables (case-table-p, set-case-table, etc.) are
      in casetab.c.
   -- Functions that do searching and replacing are in
      search.c.  The low-level functions that implement
      regular expressions are in regex.c.

   Also:

   -- Some file and process functions (in fileio.c and process.c)
      copy text from or insert text into a buffer; they call
      low-level functions in insdel.c to do this.
   -- insdel.c calls low-level functions in undo.c and extents.c
      to record buffer modifications for undoing and to handle
      extent adjustment and extent-data creation and insertion.

*/

#include <config.h>
#include "lisp.h"

#include "buffer.h"
#include "commands.h"
#include "extents.h"
#include "faces.h"
#include "frame.h"
#include "insdel.h"
#include "process.h"            /* for kill_buffer_processes */
#include "symeval.h"
#include "syntax.h"
#include "sysdep.h"	/* for getwd */
#include "window.h"

#include "sysfile.h"

struct buffer *current_buffer;	/* the current buffer */

/* This structure holds the default values of the buffer-local variables
   defined with DEFVAR_BUFFER_LOCAL, that have special slots in each buffer.
   The default value occupies the same slot in this structure
   as an individual buffer's value occupies in that buffer.
   Setting the default value also goes through the alist of buffers
   and stores into each buffer that does not say it has a local value.  */
Lisp_Object Vbuffer_defaults;

/* This structure marks which slots in a buffer have corresponding
   default values in buffer_defaults.
   Each such slot has a nonzero value in this structure.
   The value has only one nonzero bit.

   When a buffer has its own local value for a slot,
   the bit for that slot (found in the same slot in this structure)
   is turned on in the buffer's local_var_flags slot.

   If a slot in this structure is 0, then there is a DEFVAR_BUFFER_LOCAL
   for the slot, but there is no default value for it; the corresponding
   slot in buffer_defaults is not used except to initialize newly-created
   buffers.

   If a slot is -1, then there is a DEFVAR_BUFFER_LOCAL for it
   as well as a default value which is used to initialize newly-created
   buffers and as a reset-value when local-vars are killed.

   If a slot is -2, there is no DEFVAR_BUFFER_LOCAL for it. 
   (The slot is always local, but there's no lisp variable for it.)
   The default value is only used to initialize newly-creation buffers. 
   
   If a slot is -3, then there is no DEFVAR_BUFFER_LOCAL for it but
   there is a default which is used to initialize newly-creation
   buffers and as a reset-value when local-vars are killed.

   
   */
struct buffer buffer_local_flags;

/* This structure holds the names of symbols whose values may be
   buffer-local.  It is indexed and accessed in the same way as the above. */
static Lisp_Object Vbuffer_local_symbols;

/* Alist of all buffer names vs the buffers. */
/* This used to be a variable, but is no longer,
   to prevent lossage due to user rplac'ing this alist or its elements.
   Note that there is a per-frame copy of this as well; the frame slot
   and the global variable contain the same data, but possibly in different
   orders, so that the buffer ordering can be per-frame.
  */
Lisp_Object Vbuffer_alist;

/* Functions to call before and after each text change. */
Lisp_Object Qbefore_change_functions;
Lisp_Object Qafter_change_functions;
Lisp_Object Vbefore_change_functions;
Lisp_Object Vafter_change_functions;

/* #### Obsolete, for compatibility */
Lisp_Object Qbefore_change_function;
Lisp_Object Qafter_change_function;
Lisp_Object Vbefore_change_function;
Lisp_Object Vafter_change_function;

#if 0 /* FSFmacs */
Lisp_Object Vtransient_mark_mode;
#endif

/* t means ignore all read-only text properties.
   A list means ignore such a property if its value is a member of the list.
   Any non-nil value means ignore buffer-read-only.  */
Lisp_Object Vinhibit_read_only;

/* List of functions to call that can query about killing a buffer.
   If any of these functions returns nil, we don't kill it.  */
Lisp_Object Vkill_buffer_query_functions;

/* Non-nil means delete a buffer's auto-save file when the buffer is saved. */
Lisp_Object Vdelete_auto_save_files;

Lisp_Object Qbuffer_live_p;
Lisp_Object Qbuffer_or_string_p;

/* List of functions to call before changing an unmodified buffer.  */
Lisp_Object Vfirst_change_hook;
Lisp_Object Qfirst_change_hook;

Lisp_Object Qfundamental_mode;
Lisp_Object Qmode_class;
Lisp_Object Qpermanent_local;

Lisp_Object Qprotected_field;

Lisp_Object QSFundamental;	/* A string "Fundamental" */
Lisp_Object QSscratch;          /* "*scratch*" */
Lisp_Object Qdefault_directory;

Lisp_Object Qkill_buffer_hook;
Lisp_Object Qbuffer_file_name, Qbuffer_undo_list;

Lisp_Object Qrename_auto_save_file;

Lisp_Object Qget_file_buffer;
Lisp_Object Qchange_major_mode_hook, Vchange_major_mode_hook;

/* Two thresholds controlling how much undo information to keep.  */
int undo_threshold;
int undo_high_threshold;

int find_file_compare_truenames;
int find_file_use_truenames;


static void reset_buffer_local_variables (struct buffer *);
static Lisp_Object mark_buffer (Lisp_Object, void (*) (Lisp_Object));
static void print_buffer (Lisp_Object, Lisp_Object, int);
DEFINE_LRECORD_IMPLEMENTATION ("buffer", buffer,
                               mark_buffer, print_buffer, 0, 0, 0,
			       struct buffer);

#ifdef ENERGIZE
extern void mark_energize_buffer_data (struct buffer *b,
				       void (*markobj) (Lisp_Object));
#endif

Lisp_Object
make_buffer (struct buffer *buf)
{
  Lisp_Object obj;
  XSETBUFFER (obj, buf);
  return obj;
}

static Lisp_Object
mark_buffer (Lisp_Object obj, void (*markobj) (Lisp_Object))
{
  struct buffer *buf = XBUFFER (obj);

  /* Truncate undo information. */
  buf->undo_list = truncate_undo_list (buf->undo_list,
                                       undo_threshold,
                                       undo_high_threshold);

#define MARKED_SLOT(x) ((markobj) (buf->x));
#include "bufslots.h"
#undef MARKED_SLOT

  mark_buffer_extents (buf, markobj);

#ifdef ENERGIZE
  mark_energize_buffer_data (XBUFFER (obj), markobj);
#endif

  return (Qnil);
}

static void
print_buffer (Lisp_Object obj, Lisp_Object printcharfun, int escapeflag)
{
  struct buffer *b = XBUFFER (obj);

  if (print_readably) 
    {
      if (!BUFFER_LIVE_P (b))
	error ("printing unreadable object #<killed buffer>");
      else
	error ("printing unreadable object #<buffer %s>", 
	       string_data (XSTRING (b->name)));
    }
  else if (!BUFFER_LIVE_P (b))
    write_c_string ("#<killed buffer>", printcharfun);
  else if (escapeflag)
    {
      write_c_string ("#<buffer ", printcharfun);
      print_internal (b->name, printcharfun, 1);
      write_c_string (">", printcharfun);
    }
  else
    {
      print_internal (b->name, printcharfun, 0);
    }
}
   
  

DEFUN ("bufferp", Fbufferp, Sbufferp, 1, 1, 0,
  "T if OBJECT is an editor buffer.")
  (object)
     Lisp_Object object;
{
  if (BUFFERP (object))
    return Qt;
  return Qnil;
}

DEFUN ("buffer-live-p", Fbuffer_live_p, Sbuffer_live_p, 1, 1, 0,
  "T if OBJECT is an editor buffer that has not been deleted.")
  (object)
     Lisp_Object object;
{
  if (BUFFERP (object) && BUFFER_LIVE_P (XBUFFER (object)))
    return Qt;
  return Qnil;
}

static void
nsberror (Lisp_Object spec)
{
  if (STRINGP (spec))
    error ("No buffer named %s", string_data (XSTRING (spec)));
  signal_simple_error ("Invalid buffer argument", spec);
}

DEFUN ("buffer-list", Fbuffer_list, Sbuffer_list, 0, 1, 0,
  "Return a list of all existing live buffers.\n\
The order is specific to the selected frame; if the optional FRAME\n\
argument is provided, the ordering for that frame is returned instead.\n\
If the FRAME argument is t, then the global (non-frame) ordering is\n\
returned instead.")
  (frame)
  Lisp_Object frame;
{
  Lisp_Object list;
  if (EQ (frame, Qt))
    list = Vbuffer_alist;
  else
    list = get_frame (frame)->buffer_alist;
  return Fmapcar (Qcdr, list);
}

Lisp_Object
get_buffer (Lisp_Object name, int error_if_deleted_or_does_not_exist)
{
  Lisp_Object buf;

  if (BUFFERP (name))
    {
      if (!BUFFER_LIVE_P (XBUFFER (name)))
        {
          if (error_if_deleted_or_does_not_exist)
            nsberror (name);
          return (Qnil);
        }
      return name;
    }
  else
    {
      struct gcpro gcpro1;

      CHECK_STRING (name, 0);
      name = LISP_GETTEXT (name); /* I18N3 */
      GCPRO1 (name);
      buf = Fcdr (Fassoc (name, Vbuffer_alist));
      UNGCPRO;
      if (NILP (buf) && error_if_deleted_or_does_not_exist)
	nsberror (name);
      return (buf);
    }
}

struct buffer *
decode_buffer (Lisp_Object buffer, int allow_string)
{
  if (NILP (buffer))
    {
      return current_buffer;
    }
  else if (STRINGP (buffer))
    {
      if (allow_string)
	return XBUFFER (get_buffer (buffer, 1));
      else
	CHECK_BUFFER (buffer, 0);    /* This will cause a wrong-type error. */
    }

  CHECK_LIVE_BUFFER (buffer, 0);
  return XBUFFER (buffer);
}

DEFUN ("decode-buffer", Fdecode_buffer, Sdecode_buffer, 1, 1, 0,
  "Validate BUFFER or if BUFFER is nil, return the current buffer.\n\
If BUFFER is a valid buffer or a string representing a valid buffer,\n\
the corresponding buffer object will be returned.  Otherwise an error\n\
will be signaled.")
  (buffer)
  Lisp_Object buffer;
{
  struct buffer *b = decode_buffer (buffer, 1);
  XSETBUFFER (buffer, b);
  return buffer;
}

DEFUN ("get-buffer", Fget_buffer, Sget_buffer, 1, 1, 0,
  "Return the buffer named NAME (a string).\n\
If there is no live buffer named NAME, return nil.\n\
NAME may also be a buffer; if so, the value is that buffer.")
  (name)
     Lisp_Object name;
{
#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif

  /* #### This might return a dead buffer.  This is gross.  This is
     called FSF compatibility. */
  if (BUFFERP (name))
    return name;
  return (get_buffer (name, 0));
}


DEFUN ("get-file-buffer", Fget_file_buffer, Sget_file_buffer, 1, 1, 0,
  "Return the buffer visiting file FILENAME (a string).\n\
If there is no such live buffer, return nil.")
  (filename)
     Lisp_Object filename;
{
  /* This function can GC */
  REGISTER Lisp_Object tail, buf, tem;
  struct gcpro gcpro1;

#ifdef I18N3
  /* DO NOT translate the filename. */
#endif
  GCPRO1 (filename);
  CHECK_STRING (filename, 0);
  filename = Fexpand_file_name (filename, Qnil);
  {
    /* If the file name has special constructs in it,
       call the corresponding file handler.  */
    Lisp_Object handler = Ffind_file_name_handler (filename, Qget_file_buffer);
    if (!NILP (handler))
      {
	UNGCPRO;
	return call2 (handler, Qget_file_buffer, filename);
      }
  }
  UNGCPRO;

  if (find_file_compare_truenames || find_file_use_truenames)
    {
      struct gcpro gcpro1, gcpro2, gcpro3;
      Lisp_Object fn = Qnil;
      Lisp_Object dn = Qnil;

      GCPRO3 (fn, dn, filename);
      fn = Ffile_truename (filename, Qnil);
      if (NILP (fn))
	{
	  dn = Ffile_name_directory (filename);
	  fn = Ffile_truename (dn, Qnil);
	  if (! NILP (fn)) dn = fn;
	  fn = Fexpand_file_name (Ffile_name_nondirectory (filename),
				  dn);
	}
      filename = fn;
      UNGCPRO;
    }

  for (tail = Vbuffer_alist; CONSP (tail); tail = XCDR (tail))
    {
      buf = Fcdr (XCAR (tail));
      if (!BUFFERP (buf)) continue;
      if (!STRINGP (XBUFFER (buf)->filename)) continue;
      tem = Fstring_equal (filename,
			   (find_file_compare_truenames
			    ? XBUFFER (buf)->truename
			    : XBUFFER (buf)->filename));
      if (!NILP (tem))
	return buf;
    }
  return Qnil;
}


static void
push_buffer_alist (Lisp_Object name, Lisp_Object buf)
{
  Lisp_Object cons = Fcons (name, buf);
  Lisp_Object rest, dev;
  Vbuffer_alist = nconc2 (Vbuffer_alist, Fcons (cons, Qnil));
  DEVICE_AND_FRAME_LOOP (dev, rest)
    {
      struct frame *f;
      f = XFRAME (XCAR (rest));
      f->buffer_alist = nconc2 (f->buffer_alist, Fcons (cons, Qnil));
    }
}

static void
delete_from_buffer_alist (Lisp_Object buf)
{
  Lisp_Object cons = Frassq (buf, Vbuffer_alist);
  Lisp_Object rest, dev;
  if (NILP (cons))
    return; /* abort() ? */
  Vbuffer_alist = delq_no_quit (cons, Vbuffer_alist);
  DEVICE_AND_FRAME_LOOP (dev, rest)
    {
      struct frame *f;
      f = XFRAME (XCAR (rest));
      f->buffer_alist = delq_no_quit (cons, f->buffer_alist);
    }
}


DEFUN ("get-buffer-create", Fget_buffer_create, Sget_buffer_create, 1, 1, 0,
  "Return the buffer named NAME, or create such a buffer and return it.\n\
A new buffer is created if there is no live buffer named NAME.\n\
If NAME starts with a space, the new buffer does not keep undo information.\n\
If NAME is a buffer instead of a string, then it is the value returned.\n\
The value is never nil.")  
  (name)
     Lisp_Object name;
{
  /* This function can GC */
  Lisp_Object buf;
  REGISTER Lisp_Object function, tem;
  int speccount = specpdl_depth ();
  REGISTER struct buffer *b;

#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif

  name = LISP_GETTEXT (name);
  buf = Fget_buffer (name);
  if (!NILP (buf))
    return buf;

  if (string_length (XSTRING (name)) == 0)
    error ("Empty string for buffer name is not allowed");

  b = alloc_lcrecord (sizeof (struct buffer), lrecord_buffer);
  XSETBUFFER (buf, b);
  {
    /* Default everything to Vbuffer_defaults,
       remembering to preserve allocation/GC info */
    struct lcrecord_header save;
    save = b->header;
    *b = *XBUFFER (Vbuffer_defaults);
    b->header = save;
  }
  reset_buffer_local_variables (b);
  b->directory = ((current_buffer) ? current_buffer->directory : Qnil);

  init_buffer_text (b);
  BUF_MODIFF (b) = 1;
  BUF_FACECHANGE (b) = 1;

  b->last_window_start = 1;

  b->name = name;
  if (string_byte (XSTRING (name), 0) != ' ')
    b->undo_list = Qnil;
  else
    b->undo_list = Qt;

  /* initialize the extent list */
  init_buffer_extents (b);

  /* Put this in the alist of all live buffers.  */
  push_buffer_alist (name, buf);

  b->mark = Fmake_marker ();
  b->markers = 0;
  b->point_marker = Fmake_marker ();
  Fset_marker (b->point_marker, make_number (1), buf);

  function = XBUFFER (Vbuffer_defaults)->major_mode;
  if (NILP (function))
    {
      tem = Fget (current_buffer->major_mode, Qmode_class, Qnil);
      if (EQ (tem, Qnil))
	function = current_buffer->major_mode;
    }

  if (NILP (function) || EQ (function, Qfundamental_mode))
    return buf;

  /* To select a nonfundamental mode,
     select the buffer temporarily and then call the mode function. */

  record_unwind_protect (Fset_buffer, Fcurrent_buffer ());

  Fset_buffer (buf);
  call0 (function);

  return unbind_to (speccount, buf);
}


static void
reset_buffer_local_variables (struct buffer *b)
{
  struct buffer *def = XBUFFER (Vbuffer_defaults);

  b->local_var_flags = 0;
  /* For each slot that has a default value,
     copy that into the slot.  */
#define MARKED_SLOT(slot) \
  { int mask = XINT (buffer_local_flags.slot); \
    if (mask > 0 || mask == -1 || mask == -3) \
      b->slot = def->slot; \
  }
#include "bufslots.h"
#undef MARKED_SLOT
}


/* We split this away from generate-new-buffer, because rename-buffer
   and set-visited-file-name ought to be able to use this to really
   rename the buffer properly.  */

DEFUN ("generate-new-buffer-name", Fgenerate_new_buffer_name, Sgenerate_new_buffer_name,
  1, 2, 0,
  "Return a string that is the name of no existing buffer based on NAME.\n\
If there is no live buffer named NAME, then return NAME.\n\
Otherwise modify name by appending `<NUMBER>', incrementing NUMBER\n\
until an unused name is found, and then return that name.\n\
Optional second argument IGNORE specifies a name that is okay to use\n\
\(if it is in the sequence to be tried)\n\
even if a buffer with that name exists.")
 (name, ignore)
     Lisp_Object name, ignore;
{
  REGISTER Lisp_Object gentemp, tem;
  int count;
  char number[10];

  CHECK_STRING (name, 0);

  name = LISP_GETTEXT (name);
#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif

  tem = Fget_buffer (name);
  if (NILP (tem))
    return (name);

  count = 1;
  while (1)
    {
      sprintf (number, "<%d>", ++count);
      gentemp = concat2 (name, build_string (number));
      if (!NILP (ignore))
        {
          tem = Fstring_equal (gentemp, ignore);
          if (!NILP (tem))
            return gentemp;
        }
      tem = Fget_buffer (gentemp);
      if (NILP (tem))
	return (gentemp);
    }
}


DEFUN ("buffer-name", Fbuffer_name, Sbuffer_name, 0, 1, 0,
  "Return the name of BUFFER, as a string.\n\
With no argument or nil as argument, return the name of the current buffer.")
  (buffer)
     Lisp_Object buffer;
{
  if (NILP (buffer))
    return current_buffer->name;
  CHECK_BUFFER (buffer, 0);
  return XBUFFER (buffer)->name;
}

DEFUN ("buffer-file-name", Fbuffer_file_name, Sbuffer_file_name, 0, 1, 0,
  "Return name of file BUFFER is visiting, or nil if none.\n\
No argument or nil as argument means use the current buffer.")
  (buffer)
     Lisp_Object buffer;
{
  if (NILP (buffer))
    return current_buffer->filename;
  CHECK_BUFFER (buffer, 0);
  return XBUFFER (buffer)->filename;
}

DEFUN ("buffer-local-variables", Fbuffer_local_variables,
  Sbuffer_local_variables, 0, 1, 0,
  "Return an alist of variables that are buffer-local in BUFFER.\n\
Most elements look like (SYMBOL . VALUE), describing one variable.\n\
For a symbol that is locally unbound, just the symbol appears in the value.\n\
Note that storing new VALUEs in these elements doesn't change the variables.\n\
No argument or nil as argument means use current buffer as BUFFER.")
  (buffer)
     Lisp_Object buffer;
{
  struct buffer *buf = decode_buffer (buffer, 0);
  Lisp_Object result = Qnil;

  {
    /* Reference each variable in the alist in our current buffer.
       If inquiring about the current buffer, this gets the current values,
       so store them into the alist so the alist is up to date.
       If inquiring about some other buffer, this swaps out any values
       for that buffer, making the alist up to date automatically.  */
    Lisp_Object tail;
    for (tail = buf->local_var_alist; CONSP (tail); tail = XCDR (tail))
      {
        Lisp_Object elt = XCAR (tail);
        Lisp_Object val = ((buf == current_buffer)
                           ? find_symbol_value (XCAR (elt))
                           : XCDR (elt));

	/* If symbol is unbound, put just the symbol in the list.  */
	if (EQ (val, Qunbound))
	  result = Fcons (XCAR (elt), result);
	/* Otherwise, put (symbol . value) in the list.  */
	else
	  result = Fcons (Fcons (XCAR (elt), val), result);
      }
  }

  /* Add on all the variables stored in special slots.  */
  {
    struct buffer *syms = XBUFFER (Vbuffer_local_symbols);
#define MARKED_SLOT(slot) \
    { int mask = XINT (buffer_local_flags.slot); \
      if (mask == 0 || mask == -1 \
	  || ((mask > 0) && (buf->local_var_flags & mask))) \
        result = Fcons (Fcons (syms->slot, buf->slot), result); \
    }
#include "bufslots.h"
#undef MARKED_SLOT
  }
  return (result);
}

DEFUN ("buffer-dedicated-frame", Fbuffer_dedicated_frame, Sbuffer_dedicated_frame,
       0, 1, 0,
       "Return the frame dedicated to this BUFFER, or nil if there is none.\n\
No argument or nil as argument means use current buffer as BUFFER.")
  (buffer)
     Lisp_Object buffer;
{
  struct buffer *buf = decode_buffer (buffer, 0);

  /* XEmacs addition: if the frame is dead, silently make it go away. */
  if (!NILP (buf->dedicated_frame) &&
      !FRAME_LIVE_P (XFRAME (buf->dedicated_frame)))
    buf->dedicated_frame = Qnil;
    
  return buf->dedicated_frame;
}

DEFUN ("set-buffer-dedicated-frame", Fset_buffer_dedicated_frame,
       Sset_buffer_dedicated_frame,
       2, 2, 0,
       "For this BUFFER, set the FRAME dedicated to it.\n\
FRAME must be a frame or nil.")
  (buffer, frame)
     Lisp_Object buffer, frame;
{
  struct buffer *buf = decode_buffer (buffer, 0);

  if (!NILP (frame))
    CHECK_LIVE_FRAME (frame, 0); /* XEmacs change */

  return buf->dedicated_frame = frame;
}



DEFUN ("buffer-modified-p", Fbuffer_modified_p, Sbuffer_modified_p,
  0, 1, 0,
  "Return t if BUFFER was modified since its file was last read or saved.\n\
No argument or nil as argument means use current buffer as BUFFER.")
  (buffer)
     Lisp_Object buffer;
{
  struct buffer *buf = decode_buffer (buffer, 0);

  return buf->save_modified < BUF_MODIFF (buf) ? Qt : Qnil;
}

DEFUN ("set-buffer-modified-p", Fset_buffer_modified_p, Sset_buffer_modified_p,
  1, 2, 0,
  "Mark BUFFER as modified or unmodified according to FLAG.\n\
A non-nil FLAG means mark the buffer modified.  No argument or nil\n\
as BUFFER means use current buffer.")
  (flag, buffer)
     Lisp_Object flag, buffer;
{
  /* This function can GC */
  Lisp_Object fn;
  struct buffer *buf = decode_buffer (buffer, 0);

#ifdef ENERGIZE
  Lisp_Object starting_flag = 
    (buf->save_modified < BUF_MODIFF (buf)) ? Qt : Qnil;
  Lisp_Object argument_flag = (NILP (flag)) ? Qnil : Qt;
#endif  

#ifdef CLASH_DETECTION
  /* If buffer becoming modified, lock the file.
     If buffer becoming unmodified, unlock the file.  */

  fn = buf->filename;
  if (!NILP (fn))
    {
      int already = buf->save_modified < BUF_MODIFF (buf);
      if (already == NILP (flag))
	{
	  int count = specpdl_depth ();
	  /* lock_file() and unlock_file() currently use current_buffer */
	  record_unwind_protect (Fset_buffer, Fcurrent_buffer ());
	  set_buffer_internal (buf);
	  if (!already && !NILP (flag))
	    lock_file (fn);
	  else if (already && NILP (flag))
	    unlock_file (fn);
	  unbind_to (count, Qnil);
	}
    }
#endif                          /* CLASH_DETECTION */

  /* This is often called when the buffer contents are altered but we
     don't want to treat the changes that way (e.g. selective
     display).  We still need to make sure redisplay realizes that the
     contents have potentially altered and it needs to do some
     work. */
  BUF_MODIFF (buf)++;
  buf->save_modified = NILP (flag) ? BUF_MODIFF (buf) : 0;
  MARK_MODELINE_CHANGED;

#ifdef ENERGIZE
  /* don't send any notification if we are "setting" the modification bit
     to be the same as it already was */
  if (!EQ (starting_flag, argument_flag))
    {
      extern Lisp_Object Qenergize_buffer_modified_hook;
      int count = specpdl_depth ();
      record_unwind_protect (Fset_buffer, Fcurrent_buffer ());
      set_buffer_internal (buf);
      run_hook_with_args (Qenergize_buffer_modified_hook, 3,
			  flag, make_number (BUF_BEG (buf)),
			  make_number (BUF_Z (buf)));
      unbind_to (count, Qnil);
    }
#endif /* ENERGIZE */

  return flag;
}

DEFUN ("buffer-modified-tick", Fbuffer_modified_tick, Sbuffer_modified_tick,
  0, 1, 0,
  "Return BUFFER's tick counter, incremented for each change in text.\n\
Each buffer has a tick counter which is incremented each time the text in\n\
that buffer is changed.  It wraps around occasionally.\n\
No argument or nil as argument means use current buffer as BUFFER.")
  (buffer)
     Lisp_Object buffer;
{
  struct buffer *buf = decode_buffer (buffer, 0);

  return make_number (BUF_MODIFF (buf));
}

DEFUN ("rename-buffer", Frename_buffer, Srename_buffer, 1, 2,
       "sRename buffer (to new name): \nP",
  "Change current buffer's name to NAME (a string).\n\
If second arg UNIQUE is nil or omitted, it is an error if a\n\
buffer named NAME already exists.\n\
If UNIQUE is non-nil, come up with a new name using\n\
`generate-new-buffer-name'.\n\
Interactively, one can set UNIQUE with a prefix argument.\n\
Returns the name we actually gave the buffer.\n\
This does not change the name of the visited file (if any).")
  (name, unique)
     Lisp_Object name, unique;
{
  /* This function can GC */
  Lisp_Object tem, buf;

#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif
  CHECK_STRING (name, 0);
  name = LISP_GETTEXT (name);

  if (string_length (XSTRING (name)) == 0)
    error ("Empty string is invalid as a buffer name");

  tem = Fget_buffer (name);
  /* Don't short-circuit if UNIQUE is t.  That is a useful way to rename
     the buffer automatically so you can create another with the original name.
     It makes UNIQUE equivalent to
     (rename-buffer (generate-new-buffer-name NAME)).  */
  /* XEmacs change: added check for nil */
  if (NILP (unique) && !NILP (tem) && XBUFFER (tem) == current_buffer)
    return current_buffer->name;
  if (!NILP (tem))
    {
      if (!NILP (unique))
	name = Fgenerate_new_buffer_name (name, current_buffer->name);
      else
	error ("Buffer name \"%s\" is in use",
	       string_data (XSTRING (name)));
    }

  current_buffer->name = name;

  /* Catch redisplay's attention.  Unless we do this, the modelines for
     any windows displaying current_buffer will stay unchanged.  */
  MARK_MODELINE_CHANGED;

  buf = Fcurrent_buffer ();

  /* The aconses in the Vbuffer_alist are shared with frame->buffer_alist,
     so this will change it in the per-frame ordering as well. */
  Fsetcar (Frassq (buf, Vbuffer_alist), name);
  if (NILP (current_buffer->filename)
      && !NILP (current_buffer->auto_save_file_name))
    call0 (Qrename_auto_save_file);
  /* refetch since that last call may have done GC */
  /* (hypothetical relocating GC) */
  return current_buffer->name;
}

DEFUN ("other-buffer", Fother_buffer, Sother_buffer, 0, 3, 0,
  "Return most recently selected buffer other than BUFFER.\n\
Buffers not visible in windows are preferred to visible buffers,\n\
unless optional third argument VISIBLE-OK is non-nil.\n\
If no other buffer exists, the buffer `*scratch*' is returned.\n\
If BUFFER is omitted or nil, some interesting buffer is returned.\n\
\n\
The ordering is for this frame; If second optional argument FRAME\n\
is provided, then the ordering is for that frame.  If the second arg\n\
is t, then the global ordering is returned.\n\
\n\
Note: In FSF Emacs, this function takes two arguments: BUFFER and\n\
VISIBLE-OK.")
  (buffer, frame, visible_ok)
     Lisp_Object buffer, frame, visible_ok;
{
  /* This function can GC */
  Lisp_Object tail, buf, notsogood, tem;
  Lisp_Object alist;

  notsogood = Qnil;

  if (EQ (frame, Qt))
    alist = Vbuffer_alist;
  else
    alist = get_frame (frame)->buffer_alist;

  for (tail = alist; !NILP (tail); tail = Fcdr (tail))
    {
      buf = Fcdr (Fcar (tail));
      if (EQ (buf, buffer))
	continue;
      if (string_byte (XSTRING (XBUFFER (buf)->name), 0) == ' ')
	continue;
      if (NILP (visible_ok))
	{
	  /* get-buffer-window will handle nil or t frame */
	  tem = Fget_buffer_window (buf, frame, Qnil);
	}
      else
	tem = Qnil;
      if (NILP (tem))
	return buf;
      if (NILP (notsogood))
	notsogood = buf;
    }
  if (!NILP (notsogood))
    return notsogood;
  return Fget_buffer_create (QSscratch);
}

/* XEmacs change: Make this argument required because this is a dangerous
   function. */
DEFUN ("buffer-disable-undo", Fbuffer_disable_undo, Sbuffer_disable_undo, 1, 1, 0,
  "Make BUFFER stop keeping undo information.\n\
Any undo records it already has are discarded.")
  (buffer)
     Lisp_Object buffer;
{
  /* Allowing nil is an RMSism */
  struct buffer *real_buf = decode_buffer (buffer, 1);
  real_buf->undo_list = Qt;
  return Qnil;
}

DEFUN ("buffer-enable-undo", Fbuffer_enable_undo, Sbuffer_enable_undo,
       0, 1, "",
  "Start keeping undo information for buffer BUFFER.\n\
No argument or nil as argument means do this for the current buffer.")
  (buffer)
     Lisp_Object buffer;
{
  /* Allowing nil is an RMSism */
  struct buffer *real_buf = decode_buffer (buffer, 1);
  if (EQ (real_buf->undo_list, Qt))
    real_buf->undo_list = Qnil;

  return Qnil;
}

DEFUN ("kill-buffer", Fkill_buffer, Skill_buffer, 1, 1, "bKill buffer: ",
  "Kill the buffer BUFNAME.\n\
The argument may be a buffer or may be the name of a buffer.\n\
An argument of nil means kill the current buffer.\n\n\
Value is t if the buffer is actually killed, nil if user says no.\n\n\
The value of `kill-buffer-hook' (which may be local to that buffer),\n\
if not void, is a list of functions to be called, with no arguments,\n\
before the buffer is actually killed.  The buffer to be killed is current\n\
when the hook functions are called.\n\
\n\
Any processes that have this buffer as the `process-buffer' are killed\n\
with `delete-process'.")
  (bufname)
     Lisp_Object bufname;
{
  /* This function can GC */
  Lisp_Object buf;
  REGISTER struct buffer *b;
  struct gcpro gcpro1, gcpro2;

  if (NILP (bufname))
    buf = Fcurrent_buffer ();
  else if (BUFFERP (bufname))
    buf = bufname;
  else
    {
      buf = get_buffer (bufname, 0);
      if (NILP (buf)) nsberror (bufname);
    }

  b = XBUFFER (buf);

  /* OK to delete an already-deleted buffer. */
  if (!BUFFER_LIVE_P (b))
    return Qnil;

  /* Don't kill the minibuffer now current.  */
  if (EQ (buf, Vminibuffer_zero))
    return Qnil;

  if (EQ (buf, Vecho_area_buffer))
    return Qnil;

  /* Query if the buffer is still modified.  */
  if (INTERACTIVE && !NILP (b->filename)
      && BUF_MODIFF (b) > b->save_modified)
    {
      Lisp_Object killp;
      GCPRO2 (buf, bufname);
      killp = call1
	(Qyes_or_no_p,
	 (emacs_doprnt_string_c
	  ((CONST Bufbyte *) GETTEXT ("Buffer %s modified; kill anyway? "),
	   Qnil, -1, string_data (XSTRING (b->name)))));
      UNGCPRO;
      if (NILP (killp))
	return Qnil;
      b = XBUFFER (buf);        /* Hypothetical relocating GC. */
    }

  /* Run hooks with the buffer to be killed temporarily selected,
     unless the buffer is already dead (could have been deleted
     in the question above).
   */
  if (BUFFER_LIVE_P (b))
    {
      int speccount = specpdl_depth ();
      Lisp_Object tail = Qnil;

      GCPRO2 (buf, tail);
      record_unwind_protect (save_excursion_restore, save_excursion_save ());
      Fset_buffer (buf);

      /* First run the query functions; if any query is answered no,
         don't kill the buffer.  */
      for (tail = Vkill_buffer_query_functions;
           !NILP (tail);
           tail = Fcdr (tail))
      {
	if (NILP (call0 (Fcar (tail))))
        {
          UNGCPRO;
	  return unbind_to (speccount, Qnil);
        }
      }

      if (!NILP (Vrun_hooks))
        call1 (Vrun_hooks, Qkill_buffer_hook);
#ifdef HAVE_X_WINDOWS
      /* If an X selection was in this buffer, disown it.
	 We could have done this by simply adding this function to the
	 kill-buffer-hook, but the user might mess that up.
	 */
      if (EQ (Vwindow_system, Qx))
	call0 (intern ("xselect-kill-buffer-hook"));
      /* #### generalise me! */
#endif
      unbind_to (speccount, Qnil);
      UNGCPRO;
      b = XBUFFER (buf);        /* Hypothetical relocating GC. */
  }

  /* We have no more questions to ask.  Verify that it is valid
     to kill the buffer.  This must be done after the questions
     since anything can happen within yes-or-no-p.  */

  /* Don't kill the minibuffer now current.  */
  if (EQ (buf, XWINDOW (minibuf_window)->buffer))
    return Qnil;

  /* Might have been deleted during the last question above */
  if (!BUFFER_LIVE_P (b))
    return Qnil;

  /* Make this buffer not be current.
     In the process, notice if this is the sole visible buffer
     and give up if so.  */
  if (b == current_buffer)
    {
      Fset_buffer (Fother_buffer (buf, Qnil, Qnil));
      if (b == current_buffer)
	return Qnil;
    }

  /* Now there is no question: we can kill the buffer.  */

#ifdef CLASH_DETECTION
  /* Unlock this buffer's file, if it is locked.  */
  unlock_buffer (b);
#endif /* CLASH_DETECTION */

  {
    int speccount = specpdl_depth ();
    specbind (Qinhibit_quit, Qt);

    kill_buffer_processes (buf);

    /* #### This is a problem if this buffer is in a dedicated window.
       Need to undedicate any windows of this buffer first (and delete them?)
       */
    Freplace_buffer_in_windows (buf);

    delete_from_buffer_alist (buf);

    font_lock_buffer_was_killed (b);

    /* Delete any auto-save file, if we saved it in this session.  */
    if (STRINGP (b->auto_save_file_name)
	&& b->auto_save_modified != 0)
      {
	if (!NILP (Vdelete_auto_save_files) &&
	    b->save_modified < b->auto_save_modified)
	  unlink ((char *) string_data (XSTRING (b->auto_save_file_name)));
      }

    /* Unchain all markers of this buffer
       and leave them pointing nowhere.  */
    {
      REGISTER struct Lisp_Marker *m, *next;
      for (m = b->markers; m; m = next)
	{
	  m->buffer = 0;
	  next = marker_next (m);
	  marker_next (m) = 0;
	}
      b->markers = 0;
    }
    b->name = Qnil;
    uninit_buffer_text (b);
    b->undo_list = Qnil;
    uninit_buffer_extents (b);
    unbind_to (speccount, Qnil);
  }
  return Qt;
}

/* Put the element for buffer BUF at the front of buffer-alist.
   This is done when a buffer is selected "visibly".
   It keeps buffer-alist in the order of recency of selection
   so that other_buffer will return something nice.  */

void
record_buffer (Lisp_Object buf)
{
  REGISTER Lisp_Object link, prev;

  prev = Qnil;
  for (link = Vbuffer_alist; CONSP (link); link = XCDR (link))
    {
      if (EQ (XCDR (XCAR (link)), buf))
	break;
      prev = link;
    }
  /* Effectively do Vbuffer_alist = delq_no_quit (link, Vbuffer_alist) */
  if (NILP (prev))
    Vbuffer_alist = XCDR (Vbuffer_alist);
  else
    XCDR (prev) = XCDR (XCDR (prev));
  XCDR (link) = Vbuffer_alist;
  Vbuffer_alist = link;

  /* That was the global one.  Now do the same thing for the
     per-frame buffer-alist. */
  prev = Qnil;
  for (link = selected_frame ()->buffer_alist; CONSP (link);
       link = XCDR (link))
    {
      if (EQ (XCDR (XCAR (link)), buf))
	break;
      prev = link;
    }
  /* Effectively do f->buffer_alist = delq_no_quit (link, f->buffer_alist) */
  if (NILP (prev))
    selected_frame ()->buffer_alist =
      XCDR (selected_frame ()->buffer_alist);
  else
    XCDR (prev) = XCDR (XCDR (prev));
  XCDR (link) = selected_frame ()->buffer_alist;
  selected_frame ()->buffer_alist = link;
}

DEFUN ("switch-to-buffer", Fswitch_to_buffer, Sswitch_to_buffer, 1, 2, "BSwitch to buffer: ",
  "Select buffer BUFNAME in the current window.\n\
BUFNAME may be a buffer or a buffer name.\n\
Optional second arg NORECORD non-nil means\n\
do not put this buffer at the front of the list of recently selected ones.\n\
\n\
WARNING: This is NOT the way to work on another buffer temporarily\n\
within a Lisp program!  Use `set-buffer' instead.  That avoids messing with\n\
the window-buffer correspondences.")
  (bufname, norecord)
     Lisp_Object bufname, norecord;
{
  /* This function can GC */
  Lisp_Object buf;
  Lisp_Object tem;

#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif
  if (EQ (minibuf_window, Fselected_window (Qnil)))
    error ("Cannot switch buffers in minibuffer window");
  tem = Fwindow_dedicated_p (Fselected_window (Qnil));
  if (!NILP (tem))
    error ("Cannot switch buffers in a dedicated window");

  if (NILP (bufname))
    buf = Fother_buffer (Fcurrent_buffer (), Qnil, Qnil);
  else
    buf = Fget_buffer_create (bufname);
  Fset_buffer (buf);
  if (NILP (norecord))
    record_buffer (buf);

  Fset_window_buffer (EQ (Fselected_window (Qnil), minibuf_window)
		      ? Fnext_window (minibuf_window, Qnil, Qnil, Qnil)
		      : Fselected_window (Qnil),
		      buf);

  return buf;
}

DEFUN ("pop-to-buffer", Fpop_to_buffer, Spop_to_buffer, 1, 3, 0,
  "Select buffer BUFNAME in some window, preferably a different one.\n\
If BUFNAME is nil, then some other buffer is chosen.\n\
If `pop-up-windows' is non-nil, windows can be split to do this.\n\
If optional second arg NOT-THIS-WINDOW-P is non-nil, insist on finding\n\
another window even if BUFNAME is already visible in the selected window.\n\
If optional third arg is non-nil, it is the frame to pop to this\n\
buffer on.")
  (bufname, not_this_window_p, on_frame)
     Lisp_Object bufname, not_this_window_p, on_frame;
{
  /* This function can GC */
#ifdef I18N3
  /* #### Doc string should indicate that the buffer name will get
     translated. */
#endif
  Lisp_Object buf, window, frame;
  if (NILP (bufname))
    buf = Fother_buffer (Fcurrent_buffer (), Qnil, Qnil);
  else
    buf = Fget_buffer_create (bufname);
  Fset_buffer (buf);
  window = Fdisplay_buffer (buf, not_this_window_p, on_frame);
  frame = Fwindow_frame (window);
  /* if the display-buffer hook decided to show this buffer in another
     frame, then select that frame. */
  if (!EQ (frame, Fselected_frame (Qnil)))
    Fselect_frame (frame);
  record_buffer (buf);
  Fselect_window (window);
  return buf;
}

DEFUN ("current-buffer", Fcurrent_buffer, Scurrent_buffer, 0, 0, 0,
  "Return the current buffer as a Lisp object.")
  ()
{
  Lisp_Object buf;
  XSETBUFFER (buf, current_buffer);
  return buf;
}

/* Set the current buffer to b */

void
set_buffer_internal (struct buffer *b)
{
  REGISTER struct buffer *old_buf;
  REGISTER Lisp_Object tail;

  if (current_buffer == b)
    return;

  INVALIDATE_PIXEL_TO_GLYPH_CACHE;

  old_buf = current_buffer;
  current_buffer = b;
  invalidate_current_column ();   /* invalidate indentation cache */

#ifdef HAVE_FEP
  if (!noninteractive && initialized)
    {
      extern Lisp_Object Ffep_force_on (), Ffep_force_off (), Ffep_get_mode ();

      old_buf->fep_mode = Ffep_get_mode ();
      
      if (!NILP (current_buffer->fep_mode))
	Ffep_force_on ();
      else
	Ffep_force_off ();
  }
#endif

  /* Look down buffer's list of local Lisp variables
     to find and update any that forward into C variables. */

  LIST_LOOP (tail, b->local_var_alist)
    {
      Lisp_Object sym = XCAR (XCAR (tail));
      Lisp_Object valcontents = XSYMBOL (sym)->value;
      if (SYMBOL_VALUE_MAGIC_P (valcontents))
	{
	  /* Just reference the variable
	     to cause it to become set for this buffer.  */
	  (void) find_symbol_value (sym);
	}
    }

  /* Do the same with any others that were local to the previous buffer */

  if (old_buf)
    {
      for (tail = old_buf->local_var_alist; 
           !NILP (tail);
           tail = XCDR (tail))
	{
	  Lisp_Object sym = XCAR (XCAR (tail));
	  Lisp_Object valcontents = XSYMBOL (sym)->value;

	  if (SYMBOL_VALUE_MAGIC_P (valcontents))
	    {
	      /* Just reference the variable
		 to cause it to become set for this buffer.  */
	      (void) find_symbol_value (sym);
	    }
	}
    }
}

DEFUN ("set-buffer", Fset_buffer, Sset_buffer, 1, 1, 0,
  "Make the buffer BUFNAME current for editing operations.\n\
BUFNAME may be a buffer or the name of an existing buffer.\n\
See also `save-excursion' when you want to make a buffer current temporarily.\n\
This function does not display the buffer, so its effect ends\n\
when the current command terminates.\n\
Use `switch-to-buffer' or `pop-to-buffer' to switch buffers permanently.")
  (bufname)
     Lisp_Object bufname;
{
  Lisp_Object buffer;
  buffer = get_buffer (bufname, 0);
  if (NILP (buffer))
    error ("Selecting deleted buffer");
  set_buffer_internal (XBUFFER (buffer));
  return buffer;
}


DEFUN ("barf-if-buffer-read-only", Fbarf_if_buffer_read_only,
				   Sbarf_if_buffer_read_only, 0, 3, 0,
  "Signal a `buffer-read-only' error if the buffer is read-only.\n\
Optional argument BUFFER defaults to the current buffer.\n\
\n\
If optional argument START is non-nil, all extents in the buffer\n\
which overlap that part of the buffer are checked to ensure none has a\n\
`read-only' property. (Extents that lie completely within the range,\n\
however, are not checked.) END defaults to the value of START.\n\
\n\
If START and END are equal, the range checked is [START, END] (i.e.\n\
closed on both ends); otherwise, the range checked is (START, END)\n\
(open on both ends), except that extents that lie completely within\n\
[START, END] are not checked.  See `extent-in-region-p' for a fuller\n\
discussion.")
  (buffer, start, end)
  Lisp_Object buffer, start, end;
{
  struct buffer *b = decode_buffer (buffer, 0);
  Bufpos s, e;

  if (NILP (start))
    s = e = -1;
  else
    {
      if (NILP (end))
	end = start;
      get_bufrange (b, start, end, &s, &e, 0);
    }
  barf_if_buffer_read_only (b, s, e);

  return (Qnil);
}

static void
bury_buffer_1 (Lisp_Object buffer, Lisp_Object before,
	       Lisp_Object *buffer_alist)
{
  Lisp_Object aelt = rassq_no_quit (buffer, *buffer_alist);
  Lisp_Object link = memq_no_quit (aelt, *buffer_alist);
  Lisp_Object iter, before_before;

  *buffer_alist = delq_no_quit (aelt, *buffer_alist);
  for (before_before = Qnil, iter = *buffer_alist;
       !NILP (iter) && !EQ (XCDR (XCAR (iter)), before);
       before_before = iter, iter = XCDR (iter))
    ;
  XCDR (link) = iter;
  if (!NILP (before_before))
    XCDR (before_before) = link;
  else
    *buffer_alist = link;
}

DEFUN ("bury-buffer", Fbury_buffer, Sbury_buffer, 0, 2, "",
  "Put BUFFER at the end of the list of all buffers.\n\
There it is the least likely candidate for `other-buffer' to return;\n\
thus, the least likely buffer for \\[switch-to-buffer] to select by default.\n\
If BUFFER is nil or omitted, bury the current buffer.\n\
Also, if BUFFER is nil or omitted, remove the current buffer from the\n\
selected window if it is displayed there.\n\
If BEFORE is non-nil, it specifies a buffer before which BUFFER\n\
will be placed, instead of being placed at the end.")
  (buffer, before)
     Lisp_Object buffer, before;
{
  /* This function can GC */
  struct buffer *buf = decode_buffer (buffer, 1);
  /* If we're burying the current buffer, unshow it.  */
  /* Note that the behavior of (bury-buffer nil) and
     (bury-buffer (current-buffer)) is not the same.
     This is illogical but is historical.  Changing it
     breaks mh-e and TeX and such packages. */
  if (NILP (buffer))
    Fswitch_to_buffer (Fother_buffer (Fcurrent_buffer (), Qnil, Qnil), Qnil);
  XSETBUFFER (buffer, buf);

  if (!NILP (before))
    before = get_buffer (before, 1);

  if (EQ (before, buffer))
    error ("Cannot place a buffer before itself");

  bury_buffer_1 (buffer, before, &Vbuffer_alist);
  bury_buffer_1 (buffer, before, &selected_frame ()->buffer_alist);

  return Qnil;
}


DEFUN ("erase-buffer", Ferase_buffer, Serase_buffer, 0, 1, "*",
  "Delete the entire contents of the BUFFER.\n\
Any clipping restriction in effect (see `narrow-to-region') is removed,\n\
so the buffer is truly empty after this.\n\
BUFFER defaults to the current buffer if omitted.")
     (buffer)
     Lisp_Object buffer;
{
  /* This function can GC */
  struct buffer *b = decode_buffer (buffer, 1);
  /* #### yuck yuck yuck.  This is gross.  The old echo-area code,
     however, was the only place that called erase_buffer() with a
     non-zero NO_CLIP argument.

     Someone needs to fix up the redisplay code so it is smarter
     about this, so that the NO_CLIP junk isn't necessary. */
  int no_clip = (b == XBUFFER (Vecho_area_buffer));

  INVALIDATE_PIXEL_TO_GLYPH_CACHE;

  widen_buffer (b, no_clip);
  buffer_delete_range (b, BUF_BEG (b), BUF_Z (b), 0);
  b->last_window_start = 1;

  /* Prevent warnings, or suspension of auto saving, that would happen
     if future size is less than past size.  Use of erase-buffer
     implies that the future text is not really related to the past text.  */
  b->save_length = Qzero;

  zmacs_region_stays = 0;
  return Qnil;
}



DEFUN ("kill-all-local-variables", Fkill_all_local_variables, Skill_all_local_variables,
  0, 0, 0,
  "Switch to Fundamental mode by killing current buffer's local variables.\n\
Most local variable bindings are eliminated so that the default values\n\
become effective once more.  Also, the syntax table is set from\n\
`standard-syntax-table', the local keymap is set to nil,\n\
and the abbrev table from `fundamental-mode-abbrev-table'.\n\
This function also forces redisplay of the modeline.\n\
\n\
Every function to select a new major mode starts by\n\
calling this function.\n\n\
As a special exception, local variables whose names have\n\
a non-nil `permanent-local' property are not eliminated by this function.\n\
\n\
The first thing this function does is run\n\
the normal hook `change-major-mode-hook'.")
  ()
{
  /* This function can GC */
  if (!NILP (Vrun_hooks))
  {
    call1 (Vrun_hooks, Qchange_major_mode_hook);
  }

  reset_buffer_local_variables (current_buffer);

  kill_buffer_local_variables (current_buffer);

  /* Force modeline redisplay.  Useful here because all major mode
     commands call this function.  */
  MARK_MODELINE_CHANGED;

  return Qnil;
}

void
syms_of_buffer (void)
{
  defsymbol (&Qbuffer_live_p, "buffer-live-p");
  defsymbol (&Qbuffer_or_string_p, "buffer-or-string-p");
  defsymbol (&Qmode_class, "mode-class");
  defsymbol (&Qrename_auto_save_file, "rename-auto-save-file");
  defsymbol (&Qkill_buffer_hook, "kill-buffer-hook");
  defsymbol (&Qpermanent_local, "permanent-local");

  defsymbol (&Qfirst_change_hook, "first-change-hook");
  defsymbol (&Qbefore_change_functions, "before-change-functions");
  defsymbol (&Qafter_change_functions, "after-change-functions");

  /* #### Obsolete, for compatibility */
  defsymbol (&Qbefore_change_function, "before-change-function");
  defsymbol (&Qafter_change_function, "after-change-function");

  defsymbol (&Qbuffer_file_name, "buffer-file-name");
  defsymbol (&Qbuffer_undo_list, "buffer-undo-list");
  defsymbol (&Qdefault_directory, "default-directory");

  defsymbol (&Qget_file_buffer, "get-file-buffer");
  defsymbol (&Qchange_major_mode_hook, "change-major-mode-hook");

  defsymbol (&Qfundamental_mode, "fundamental-mode");

  defsubr (&Sbufferp);
  defsubr (&Sbuffer_live_p);
  defsubr (&Sbuffer_list);
  defsubr (&Sdecode_buffer);
  defsubr (&Sget_buffer);
  defsubr (&Sget_file_buffer);
  defsubr (&Sget_buffer_create);

  defsubr (&Sgenerate_new_buffer_name);
  defsubr (&Sbuffer_name);
  defsubr (&Sbuffer_file_name);
  defsubr (&Sbuffer_local_variables);
  defsubr (&Sbuffer_dedicated_frame);
  defsubr (&Sset_buffer_dedicated_frame);
  defsubr (&Sbuffer_modified_p);
  defsubr (&Sset_buffer_modified_p);
  defsubr (&Sbuffer_modified_tick);
  defsubr (&Srename_buffer);
  defsubr (&Sother_buffer);
  defsubr (&Sbuffer_disable_undo);
  defsubr (&Sbuffer_enable_undo);
  defsubr (&Skill_buffer);
  defsubr (&Serase_buffer);
  defsubr (&Sswitch_to_buffer);
  defsubr (&Spop_to_buffer);
  defsubr (&Scurrent_buffer);
  defsubr (&Sset_buffer);
  defsubr (&Sbarf_if_buffer_read_only);
  defsubr (&Sbury_buffer);
  defsubr (&Skill_all_local_variables);

  deferror (&Qprotected_field, "protected-field",
	    "Attempt to modify a protected field", 1);
}

/* initialize the buffer routines */
void
vars_of_buffer (void)
{
  /* This function can GC */
  staticpro (&QSFundamental);
  staticpro (&QSscratch);
  staticpro (&Vbuffer_alist);

  QSFundamental = Fpurecopy (build_string ("Fundamental"));
  QSscratch = Fpurecopy (build_string (DEFER_GETTEXT ("*scratch*")));

  Vbuffer_alist = Qnil;
  current_buffer = 0;

  DEFVAR_LISP ("change-major-mode-hook", &Vchange_major_mode_hook,
    "List of hooks to be run before killing local variables in a buffer.\n\
This should be used by by any mode that temporarily alters the contents or\n\
the read-only state of the buffer.  See also `kill-all-local-variables'.");
  Vchange_major_mode_hook = Qnil;

  DEFVAR_BOOL ("find-file-compare-truenames", &find_file_compare_truenames,
    "If this is true, then the find-file command will check the truenames\n\
of all visited files when deciding whether a given file is already in\n\
a buffer, instead of just the buffer-file-name.  This means that if you\n\
attempt to visit another file which is a symbolic-link to a file which is\n\
already in a buffer, the existing buffer will be found instead of a newly-\n\
created one.  This works if any component of the pathname (including a non-\n\
terminal component) is a symbolic link as well, but doesn't work with hard\n\
links (nothing does).\n\
\n\
See also the variable find-file-use-truenames.");
  find_file_compare_truenames = 0;

  DEFVAR_BOOL ("find-file-use-truenames", &find_file_use_truenames,
    "If this is true, then a buffer's visited file-name will always be\n\
chased back to the real file; it will never be a symbolic link, and there\n\
will never be a symbolic link anywhere in its directory path.\n\
That is, the buffer-file-name and buffer-file-truename will be equal.\n\
This doesn't work with hard links.\n\
\n\
See also the variable find-file-compare-truenames.");
  find_file_use_truenames = 0;

  DEFVAR_BOOL ("find-file-visit-truename", &find_file_use_truenames,
    "An alias for `find-file-use-truenames'.\n\
This variable is present for compatibility with FSF Emacs.");

  DEFVAR_LISP ("before-change-functions", &Vbefore_change_functions,
	       "List of functions to call before each text change.\n\
Two arguments are passed to each function: the positions of\n\
the beginning and end of the range of old text to be changed.\n\
\(For an insertion, the beginning and end are at the same place.)\n\
No information is given about the length of the text after the change.\n\
\n\
Buffer changes made while executing the `before-change-functions'\n\
don't call any before-change or after-change functions.");
  Vbefore_change_functions = Qnil;

  /* FSF Emacs has the following additional doc at the end of
     before-change-functions and after-change-functions:

That's because these variables are temporarily set to nil.\n\
As a result, a hook function cannot straightforwardly alter the value of\n\
these variables.  See the Emacs Lisp manual for a way of\n\
accomplishing an equivalent result by using other variables.

     But this doesn't apply under XEmacs because things are
     handled better. */

  DEFVAR_LISP ("after-change-functions", &Vafter_change_functions,
	       "List of functions to call after each text change.\n\
Three arguments are passed to each function: the positions of\n\
the beginning and end of the range of changed text,\n\
and the length of the pre-change text replaced by that range.\n\
\(For an insertion, the pre-change length is zero;\n\
for a deletion, that length is the number of characters deleted,\n\
and the post-change beginning and end are at the same place.)\n\
\n\
Buffer changes made while executing `after-change-functions'\n\
don't call any before-change or after-change functions.");
  Vafter_change_functions = Qnil;

  DEFVAR_LISP ("before-change-function", &Vbefore_change_function,
	       ""); /* obsoleteness will be documented */
  Vbefore_change_function = Qnil;

  DEFVAR_LISP ("after-change-function", &Vafter_change_function,
	       ""); /* obsoleteness will be documented */
  Vafter_change_function = Qnil;

  DEFVAR_LISP ("first-change-hook", &Vfirst_change_hook,
  "A list of functions to call before changing a buffer which is unmodified.\n\
The functions are run using the `run-hooks' function.");
  Vfirst_change_hook = Qnil;

#if 0 /* FSFmacs */
  xxDEFVAR_LISP ("transient-mark-mode", &Vtransient_mark_mode,
    "*Non-nil means deactivate the mark when the buffer contents change.");
  Vtransient_mark_mode = Qnil;
#endif

  DEFVAR_INT ("undo-threshold", &undo_threshold,
    "Keep no more undo information once it exceeds this size.\n\
This threshold is applied when garbage collection happens.\n\
The size is counted as the number of bytes occupied,\n\
which includes both saved text and other data.");
  undo_threshold = 20000;

  DEFVAR_INT ("undo-high-threshold", &undo_high_threshold,
    "Don't keep more than this much size of undo information.\n\
A command which pushes past this size is itself forgotten.\n\
This threshold is applied when garbage collection happens.\n\
The size is counted as the number of bytes occupied,\n\
which includes both saved text and other data.");
  undo_high_threshold = 30000;

  DEFVAR_LISP ("inhibit-read-only", &Vinhibit_read_only,
    "*Non-nil means disregard read-only status of buffers or characters.\n\
If the value is t, disregard `buffer-read-only' and all `read-only'\n\
text properties.  If the value is a list, disregard `buffer-read-only'\n\
and disregard a `read-only' text property if the property value\n\
is a member of the list.");
  Vinhibit_read_only = Qnil;

  DEFVAR_LISP ("kill-buffer-query-functions", &Vkill_buffer_query_functions,
    "List of functions called with no args to query before killing a buffer.");
  Vkill_buffer_query_functions = Qnil;

  DEFVAR_LISP ("delete-auto-save-files", &Vdelete_auto_save_files,
  "*Non-nil means delete auto-save file when a buffer is saved or killed.");
  Vdelete_auto_save_files = Qt;
}

/* DOC is ignored because it is snagged and recorded externally 
 *  by make-docfile */
/* Renamed from DEFVAR_PER_BUFFER because FSFmacs D_P_B takes
 *  a bogus extra arg, which confuses an otherwise identical make-docfile.c */
/* Declaring this stuff as const produces 'Cannot reinitialize' messages
   from SunPro C's fix-and-continue feature (a way neato feature that
   makes debugging unbelievably more bearable) */
#define DEFVAR_BUFFER_LOCAL(lname, field_name, doc)			\
 do { static CONST_IF_NOT_DEBUG struct symbol_value_forward I_hate_C	\
       = { { { { lrecord_symbol_value_forward },			\
               (void *) &(buffer_local_flags.field_name), 69 },		\
             SYMVAL_CURRENT_BUFFER_FORWARD }, 0 };			\
      defvar_buffer_local ((lname), &I_hate_C);				\
 } while (0)

#define DEFVAR_BUFFER_LOCAL_MAGIC(lname, field_name, doc, magicfun)	\
 do { static CONST_IF_NOT_DEBUG struct symbol_value_forward I_hate_C	\
       = { { { { lrecord_symbol_value_forward },			\
               (void *) &(buffer_local_flags.field_name), 69 },		\
             SYMVAL_CURRENT_BUFFER_FORWARD }, magicfun };		\
      defvar_buffer_local ((lname), &I_hate_C);				\
 } while (0)

static void
defvar_buffer_local (CONST char *namestring, 
                     CONST struct symbol_value_forward *m)
{
  int offset = ((char *)symbol_value_forward_forward (m)
                - (char *)&buffer_local_flags);

  defvar_mumble (namestring, m, sizeof (*m));

  *((Lisp_Object *)(offset + (char *)XBUFFER (Vbuffer_local_symbols))) 
    = intern (namestring);
}

/* DOC is ignored because it is snagged and recorded externally 
 *  by make-docfile */
#define DEFVAR_BUFFER_DEFAULTS(lname, field_name, doc)			\
 do { static CONST_IF_NOT_DEBUG struct symbol_value_forward I_hate_C	\
       = { { { { lrecord_symbol_value_forward },			\
               (void *) &(buffer_local_flags.field_name), 69 },		\
             SYMVAL_DEFAULT_BUFFER_FORWARD }, 0 };			\
      defvar_mumble ((lname), &I_hate_C, sizeof (I_hate_C));		\
 } while (0)

#define DEFVAR_BUFFER_DEFAULTS_MAGIC(lname, field_name, doc, magicfun)	\
 do { static CONST_IF_NOT_DEBUG struct symbol_value_forward I_hate_C	\
       = { { { { lrecord_symbol_value_forward },			\
               (void *) &(buffer_local_flags.field_name), 69 },		\
             SYMVAL_DEFAULT_BUFFER_FORWARD }, magicfun };		\
      defvar_mumble ((lname), &I_hate_C, sizeof (I_hate_C));		\
 } while (0)

static void
nuke_all_buffer_slots (struct buffer *b, Lisp_Object zap)
{
  struct lcrecord_header save;

  save = b->header;
  memset (b, 0, sizeof (*b));
  b->header = save;

#define MARKED_SLOT(x)	b->x = (zap);
#include "bufslots.h"
#undef MARKED_SLOT
}

void
complex_vars_of_buffer (void)
{
  /* Make sure all markable slots in buffer_defaults
     are initialized reasonably, so mark_buffer won't choke.
   */
  struct buffer *defs = alloc_lcrecord (sizeof (struct buffer),
					lrecord_buffer);
  struct buffer *syms = alloc_lcrecord (sizeof (struct buffer),
					lrecord_buffer);

  staticpro (&Vbuffer_defaults);
  staticpro (&Vbuffer_local_symbols);
  XSETBUFFER (Vbuffer_defaults, defs);
  XSETBUFFER (Vbuffer_local_symbols, syms);
  
  nuke_all_buffer_slots (syms, Qnil);
  nuke_all_buffer_slots (defs, Qnil);
  
  
  /* Set up the non-nil default values of various buffer slots.
     Must do these before making the first buffer.
     */
  defs->major_mode = Qfundamental_mode;
  defs->mode_name = QSFundamental;
  defs->abbrev_table = Qnil;    /* real default setup by Lisp code */
  defs->downcase_table = Vascii_downcase_table;
  defs->upcase_table = Vascii_upcase_table;
  defs->case_canon_table = Vascii_canon_table;
  defs->case_eqv_table = Vascii_eqv_table;
  defs->syntax_table = Vstandard_syntax_table;
  defs->modeline_format = build_string ("%-");  /* reset in loaddefs.el */
  defs->case_fold_search = Qt;
  defs->selective_display_ellipses = Qt;
  defs->tab_width = make_number (8);
  defs->ctl_arrow = Qt;
  defs->fill_column = make_number (70);
  defs->left_margin = Qzero;
  defs->save_length = Qzero;       /* lisp code wants int-or-nil */
  defs->modtime = 0;
  defs->save_modified = 1;
  defs->auto_save_modified = 0;
  defs->auto_save_failure_time = -1;
  
  {
    /*  0 means var is always local.  Default used only at creation.
     * -1 means var is always local.  Default used only at reset and
     *    creation.
     * -2 means there's no lisp variable corresponding to this slot
     *    and the default is only used at creation.
     * -3 means no Lisp variable.  Default used only at reset and creation.
     * >0 is mask.  Var is local if ((buffer->local_var_flags & mask) != 0)
     *              Otherwise default is used.
     */
    Lisp_Object always_local_no_default = make_number (0);
    Lisp_Object always_local_resettable = make_number (-1);
    Lisp_Object resettable = make_number (-3);
    
    /* Assign the local-flags to the slots that have default values.
       The local flag is a bit that is used in the buffer
       to say that it has its own local value for the slot.
       The local flag bits are in the local_var_flags slot of the
       buffer.  */
    
    nuke_all_buffer_slots (&buffer_local_flags, make_number (-2));
    buffer_local_flags.filename = always_local_no_default;
    buffer_local_flags.truename = always_local_no_default;
    buffer_local_flags.directory = always_local_no_default;
    buffer_local_flags.backed_up = always_local_no_default;
    buffer_local_flags.save_length = always_local_no_default;
    buffer_local_flags.auto_save_file_name = always_local_no_default;
    buffer_local_flags.read_only = always_local_no_default;
    
    buffer_local_flags.major_mode = always_local_resettable;
    buffer_local_flags.mode_name = always_local_resettable;
    buffer_local_flags.undo_list = always_local_no_default;
#if 0 /* FSFmacs */
    buffer_local_flags.mark_active = always_local_resettable;
#endif
    
    buffer_local_flags.keymap = resettable;
    buffer_local_flags.downcase_table = resettable;
    buffer_local_flags.upcase_table = resettable;
    buffer_local_flags.case_canon_table = resettable;
    buffer_local_flags.case_eqv_table = resettable;
    buffer_local_flags.syntax_table = resettable;
    
    buffer_local_flags.modeline_format = make_number (1);
    buffer_local_flags.abbrev_mode = make_number (2);
    buffer_local_flags.overwrite_mode = make_number (4);
    buffer_local_flags.case_fold_search = make_number (8);
    buffer_local_flags.auto_fill_function = make_number (0x10);
    buffer_local_flags.selective_display = make_number (0x20);
    buffer_local_flags.selective_display_ellipses = make_number (0x40);
    buffer_local_flags.tab_width = make_number (0x80);
    buffer_local_flags.truncate_lines = make_number (0x100);
    buffer_local_flags.ctl_arrow = make_number (0x200);
    buffer_local_flags.fill_column = make_number (0x400);
    buffer_local_flags.left_margin = make_number (0x800);
    buffer_local_flags.abbrev_table = make_number (0x1000);
#ifdef MSDOS
    buffer_local_flags.buffer_file_type = make_number (0x2000);
#endif
    
#ifdef MULE_REGEXP
    buffer_local_flags.category_table = make_number (0x4000);
#endif
#ifdef HAVE_FEP
    buffer_local_flags.fep_mode = make_number (0x8000);
#endif
    /* #### Warning, 0x4000000 (that's six zeroes) is the largest number
       currently allowable due to the XINT() handling of this value.
       With some rearrangement you can get 4 more bits. */
  }

  DEFVAR_BUFFER_DEFAULTS ("default-modeline-format", modeline_format,
    "Default value of `modeline-format' for buffers that don't override it.\n\
This is the same as (default-value 'modeline-format).");

  DEFVAR_BUFFER_DEFAULTS ("default-abbrev-mode", abbrev_mode,
    "Default value of `abbrev-mode' for buffers that do not override it.\n\
This is the same as (default-value 'abbrev-mode).");

  DEFVAR_BUFFER_DEFAULTS ("default-ctl-arrow", ctl_arrow,
    "Default value of `ctl-arrow' for buffers that do not override it.\n\
This is the same as (default-value 'ctl-arrow).");

#if 0 /* #### make this a specifier! */
  DEFVAR_BUFFER_DEFAULTS ("default-display-direction", display_direction,
    "Default display-direction for buffers that do not override it.\n\
This is the same as (default-value 'display-direction).\n\
Note: This is not yet implemented.");
#endif

  DEFVAR_BUFFER_DEFAULTS ("default-truncate-lines", truncate_lines,
    "Default value of `truncate-lines' for buffers that do not override it.\n\
This is the same as (default-value 'truncate-lines).");

  DEFVAR_BUFFER_DEFAULTS ("default-fill-column", fill_column,
    "Default value of `fill-column' for buffers that do not override it.\n\
This is the same as (default-value 'fill-column).");

  DEFVAR_BUFFER_DEFAULTS ("default-left-margin", left_margin,
    "Default value of `left-margin' for buffers that do not override it.\n\
This is the same as (default-value 'left-margin).");

  DEFVAR_BUFFER_DEFAULTS ("default-tab-width", tab_width,
    "Default value of `tab-width' for buffers that do not override it.\n\
This is the same as (default-value 'tab-width).");

  DEFVAR_BUFFER_DEFAULTS ("default-case-fold-search", case_fold_search,
    "Default value of `case-fold-search' for buffers that don't override it.\n\
This is the same as (default-value 'case-fold-search).");

#ifdef MSDOS
  DEFVAR_BUFFER_DEFAULTS ("default-buffer-file-type", buffer_file_type,
    "Default file type for buffers that do not override it.\n\
This is the same as (default-value 'buffer-file-type).\n\
The file type is nil for text, t for binary.");
#endif

  DEFVAR_BUFFER_LOCAL ("modeline-format", modeline_format,
    "Template for displaying modeline for current buffer.\n\
Each buffer has its own value of this variable.\n\
Value may be a string, a symbol or a list or cons cell.\n\
For a symbol, its value is used (but it is ignored if t or nil).\n\
 A string appearing directly as the value of a symbol is processed verbatim\n\
 in that the %-constructs below are not recognized.\n\
For a list whose car is a symbol, the symbol's value is taken,\n\
 and if that is non-nil, the cadr of the list is processed recursively.\n\
 Otherwise, the caddr of the list (if there is one) is processed.\n\
For a list whose car is a string or list, each element is processed\n\
 recursively and the results are effectively concatenated.\n\
For a list whose car is an integer, the cdr of the list is processed\n\
  and padded (if the number is positive) or truncated (if negative)\n\
  to the width specified by that number.\n\
A string is printed verbatim in the modeline except for %-constructs:\n\
  (%-constructs are allowed when the string is the entire modeline-format\n\
   or when it is found in a cons-cell or a list)\n\
  %b -- print buffer name.      %c -- print the current column number.\n\
  %f -- print visited file name.\n\
  %* -- print %, * or hyphen.   %+ -- print *, % or hyphen.\n\
	% means buffer is read-only and * means it is modified.\n\
	For a modified read-only buffer, %* gives % and %+ gives *.\n\
  %s -- print process status.   %l -- print the current line number.\n\
  %S -- print name of selected frame (only meaningful under X Windows).\n\
  %p -- print percent of buffer above top of window, or Top, Bot or All.\n\
  %P -- print percent of buffer above bottom of window, perhaps plus Top,\n\
        or print Bottom or All.\n\
  %n -- print Narrow if appropriate.\n\
  %t -- Under MS-DOS, print T if files is text, B if binary.\n\
  %[ -- print one [ for each recursive editing level.  %] similar.\n\
  %% -- print %.                %- -- print infinitely many dashes.\n\
Decimal digits after the % specify field width to which to pad.");

  DEFVAR_BUFFER_DEFAULTS ("default-major-mode", major_mode,
    "*Major mode for new buffers.  Defaults to `fundamental-mode'.\n\
nil here means use current buffer's major mode.");

  DEFVAR_BUFFER_DEFAULTS ("fundamental-mode-abbrev-table", abbrev_table,
    "The abbrev table of mode-specific abbrevs for Fundamental Mode.");

  DEFVAR_BUFFER_LOCAL ("major-mode", major_mode,
    "Symbol for current buffer's major mode.");

  DEFVAR_BUFFER_LOCAL ("mode-name", mode_name,
    "Pretty name of current buffer's major mode (a string).");

  DEFVAR_BUFFER_LOCAL ("abbrev-mode", abbrev_mode,
    "Non-nil turns on automatic expansion of abbrevs as they are inserted.\n\
Automatically becomes buffer-local when set in any fashion.");

  DEFVAR_BUFFER_LOCAL ("case-fold-search", case_fold_search,
    "*Non-nil if searches should ignore case.\n\
Automatically becomes buffer-local when set in any fashion.");

  DEFVAR_BUFFER_LOCAL ("fill-column", fill_column,
    "*Column beyond which automatic line-wrapping should happen.\n\
Automatically becomes buffer-local when set in any fashion.");

  DEFVAR_BUFFER_LOCAL ("left-margin", left_margin,
    "*Column for the default indent-line-function to indent to.\n\
Linefeed indents to this column in Fundamental mode.\n\
Automatically becomes buffer-local when set in any fashion.\n\
Do not confuse this with the specifier `left-margin-width';\n\
that controls the size of a margin that is displayed outside\n\
of the text area.");

  DEFVAR_BUFFER_LOCAL ("tab-width", tab_width,
    "*Distance between tab stops (for display of tab characters), in columns.\n\
Automatically becomes buffer-local when set in any fashion.");

  DEFVAR_BUFFER_LOCAL ("ctl-arrow", ctl_arrow,
    "*Non-nil means display control chars with uparrow.\n\
Nil means use backslash and octal digits.\n\
An integer means characters >= ctl-arrow are assumed to be printable, and\n\
will be displayed as a single glyph.\n\
Any other value is the same as 160 - the code SPC with the high bit on.\n\
\n\
The interpretation of this variable is likely to change in the future.\n\
\n\
Automatically becomes buffer-local when set in any fashion.\n\
This variable does not apply to characters whose display is specified\n\
in the current display table (if there is one).");

#if 0 /* #### Make this a specifier! */
  DEFVAR_BUFFER_LOCAL ("display-direction", display_direction,
    "*Non-nil means lines in the buffer are displayed right to left.\n\
Nil means left to right. (Not yet implemented.)");
#endif

  DEFVAR_BUFFER_LOCAL_MAGIC ("truncate-lines", truncate_lines,
    "*Non-nil means do not display continuation lines;\n\
give each line of text one frame line.\n\
Automatically becomes buffer-local when set in any fashion.\n\
\n\
Note that this is overridden by the variable\n\
`truncate-partial-width-windows' if that variable is non-nil\n\
and this buffer is not full-frame width.",
			     redisplay_variable_changed);

  DEFVAR_BUFFER_LOCAL ("default-directory", directory,
    "Name of default directory of current buffer.  Should end with slash.\n\
Each buffer has its own value of this variable.");

#ifdef HAVE_FEP
  DEFVAR_BUFFER_LOCAL ("fep-mode", fep_mode,
    "*Document me.");
#endif

#ifdef MSDOS
  DEFVAR_BUFFER_LOCAL ("buffer-file-type", buffer_file_type,
    "*If visited file is text, nil; otherwise, t.");
#endif

  DEFVAR_BUFFER_LOCAL ("auto-fill-function", auto_fill_function,
    "Function called (if non-nil) to perform auto-fill.\n\
It is called after self-inserting a space at a column beyond `fill-column'.\n\
Each buffer has its own value of this variable.\n\
NOTE: This variable is not an ordinary hook;\n\
It may not be a list of functions.");

  DEFVAR_BUFFER_LOCAL ("buffer-file-name", filename,
    "Name of file visited in current buffer, or nil if not visiting a file.\n\
Each buffer has its own value of this variable.");

  DEFVAR_BUFFER_LOCAL ("buffer-file-truename", truename,
    "The real name of the file visited in the current buffer, \n\
or nil if not visiting a file.  This is the result of passing \n\
buffer-file-name to the `truename' function.  Every buffer has \n\
its own value of this variable.  This variable is automatically \n\
maintained by the functions that change the file name associated \n\
with a buffer.");

  DEFVAR_BUFFER_LOCAL ("buffer-auto-save-file-name", auto_save_file_name,
    "Name of file for auto-saving current buffer,\n\
or nil if buffer should not be auto-saved.\n\
Each buffer has its own value of this variable.");

  DEFVAR_BUFFER_LOCAL ("buffer-read-only", read_only,
    "Non-nil if this buffer is read-only.\n\
Each buffer has its own value of this variable.");

  DEFVAR_BUFFER_LOCAL ("buffer-backed-up", backed_up,
    "Non-nil if this buffer's file has been backed up.\n\
Backing up is done before the first time the file is saved.\n\
Each buffer has its own value of this variable.");

  DEFVAR_BUFFER_LOCAL ("buffer-saved-size", save_length,
    "Length of current buffer when last read in, saved or auto-saved.\n\
0 initially.\n\
Each buffer has its own value of this variable.");

  DEFVAR_BUFFER_LOCAL ("selective-display", selective_display,
    "Non-nil enables selective display:\n\
Integer N as value means display only lines\n\
 that start with less than n columns of space.\n\
A value of t means, after a ^M, all the rest of the line is invisible.\n\
 Then ^M's in the file are written into files as newlines.\n\n\
Automatically becomes buffer-local when set in any fashion.");

#ifndef old
  DEFVAR_BUFFER_LOCAL ("selective-display-ellipses", selective_display_ellipses,
    "t means display ... on previous line when a line is invisible.\n\
Automatically becomes buffer-local when set in any fashion.");
#endif

  DEFVAR_BUFFER_LOCAL ("local-abbrev-table", abbrev_table,
    "Local (mode-specific) abbrev table of current buffer.");

  DEFVAR_BUFFER_LOCAL ("overwrite-mode", overwrite_mode,
    "Non-nil if self-insertion should replace existing text.\n\
If non-nil and not `overwrite-mode-binary', self-insertion still\n\
inserts at the end of a line, and inserts when point is before a tab,\n\
until the tab is filled in.\n\
If `overwrite-mode-binary', self-insertion replaces newlines and tabs too.\n\
Automatically becomes buffer-local when set in any fashion.");

#if 0 /* FSFmacs */
  /* Adds the following to the doc string for buffer-undo-list:

An entry (nil PROP VAL BEG . END) indicates that a text property
was modified between BEG and END.  PROP is the property name,
and VAL is the old value.
*/
#endif

  DEFVAR_BUFFER_LOCAL ("buffer-undo-list", undo_list,
    "List of undo entries in current buffer.\n\
Recent changes come first; older changes follow newer.\n\
\n\
An entry (START . END) represents an insertion which begins at\n\
position START and ends at position END.\n\
\n\
An entry (TEXT . POSITION) represents the deletion of the string TEXT\n\
from (abs POSITION).  If POSITION is positive, point was at the front\n\
of the text being deleted; if negative, point was at the end.\n\
\n\
An entry (t HIGHWORD LOWWORD) indicates that the buffer had been\n\
previously unmodified.  HIGHWORD and LOWWORD are the high and low\n\
16-bit words of the buffer's modification count at the time.  If the\n\
modification count of the most recent save is different, this entry is\n\
obsolete.\n\
\n\
An entry of the form POSITION indicates that point was at the buffer\n\
location given by the integer.  Undoing an entry of this form places\n\
point at POSITION.\n\
\n\
nil marks undo boundaries.  The undo command treats the changes\n\
between two undo boundaries as a single step to be undone.\n\
\n\
If the value of the variable is t, undo information is not recorded.");

#if 0 /* FSFmacs */
  xxDEFVAR_BUFFER_LOCAL ("mark-active", mark_active, 
    "Non-nil means the mark and region are currently active in this buffer.\n\
Automatically local in all buffers.");
#endif

  /* Check for DEFVAR_BUFFER_LOCAL without initializing the corresponding
     slot of buffer_local_flags and vice-versa.  Must be done after all
     DEFVAR_BUFFER_LOCAL() calls. */
#define MARKED_SLOT(slot)					\
  if ((XINT (buffer_local_flags.slot) != -2 &&			\
         XINT (buffer_local_flags.slot) != -3)			\
      != !(NILP (XBUFFER (Vbuffer_local_symbols)->slot)))	\
  abort ()
#include "bufslots.h"
#undef MARKED_SLOT

  Vprin1_to_string_buffer
    = Fget_buffer_create (Fpurecopy (build_string (" prin1")));
  /* Reset Vbuffer_alist again so that the above buf is magically
     invisible */
  Vbuffer_alist = Qnil;
  /* Want no undo records for prin1_to_string_buffer */
  Fbuffer_disable_undo (Vprin1_to_string_buffer);
  
  {
    Lisp_Object scratch =
      Fset_buffer (Fget_buffer_create (QSscratch));
    /* Want no undo records for *scratch* until after Emacs is dumped */
    Fbuffer_disable_undo (scratch);
  }
}

void
init_buffer (void)
{
  /* This function can GC */
  char buf[MAXPATHLEN+1];

  buf[0] = 0;

  Fset_buffer (Fget_buffer_create (QSscratch));

#ifndef VMS /* Need filename-syntax-independent absolute-pathname-p ! */
  {
    /* If PWD is accurate, use it instead of calling getwd.  This is faster
       when PWD is right, and may avoid a fatal error.  */
    struct stat dotstat, pwdstat;
    char *pwd = getenv ("PWD");

    if (pwd && pwd[0] == '/'
        && stat (pwd, &pwdstat) == 0
        && stat (".", &dotstat) == 0
        && dotstat.st_ino == pwdstat.st_ino
        && dotstat.st_dev == pwdstat.st_dev
        && (int) strlen (pwd) < MAXPATHLEN)
      strcpy (buf, pwd);
  }
#endif /* not VMS */
  if (buf[0] == 0)
    {
      /* PWD env var didn't work for us */
      if (getwd (buf) == 0)
	fatal ("`getwd' failed: %s.", buf);
    }

#ifndef VMS
  /* Maybe this should really use some standard subroutine
     whose definition is filename syntax dependent.  */
  if (buf[strlen (buf) - 1] != '/')
    strcat (buf, "/");
#endif /* not VMS */
  current_buffer->directory = build_string (buf);

#if 0 /* FSFmacs */
  /* #### is this correct? */
  temp = get_minibuffer (0);
  XBUFFER (temp)->directory = current_buffer->directory;
#endif
}

/* Emacs shell widget external header file.
   Copyright (C) 1994 Sun Microsystems, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing, May, 1994. */

#ifndef _EmacsShell_h
#define _EmacsShell_h

#ifndef XtNwidthCells
#define XtNwidthCells "widthCells"
#endif
#ifndef XtCWidthCells
#define XtCWidthCells "WidthCells"
#endif

#ifndef XtNheightCells
#define XtNheightCells "heightCells"
#endif
#ifndef XtCHeightCells
#define XtCHeightCells "HeightCells"
#endif

#ifndef XtNminWidthCells
#define XtNminWidthCells "minWidthCells"
#endif
#ifndef XtCMinWidthCells
#define XtCMinWidthCells "MinWidthCells"
#endif

#ifndef XtNminHeightCells
#define XtNminHeightCells "minHeightCells"
#endif
#ifndef XtCMinHeightCells
#define XtCMinHeightCells "MinHeightCells"
#endif

typedef struct _TopLevelEmacsShellClassRec *TopLevelEmacsShellWidgetClass;
typedef struct _TopLevelEmacsShellRec *TopLevelEmacsShellWidget;
extern WidgetClass topLevelEmacsShellWidgetClass;

typedef struct _TransientEmacsShellClassRec *TransientEmacsShellWidgetClass;
typedef struct _TransientEmacsShellRec *TransientEmacsShellWidget;
extern WidgetClass transientEmacsShellWidgetClass;

void EmacsShellUpdateSizeHints (Widget gw);
void TopLevelEmacsShellUpdateSizeHints (Widget gw);
void TransientEmacsShellUpdateSizeHints (Widget gw);
void EmacsShellSetSizeUserSpecified (Widget gw);
void EmacsShellSetPositionUserSpecified (Widget gw);
void EmacsShellSmashIconicHint (Widget shell, int iconic_p);

#endif /* _EmacsShell_h */

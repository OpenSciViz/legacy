/* Commonly-used symbols
   Copyright (C) 1995 Sun Microsystems

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* The purpose of this file is as a central place to stick symbols
   that don't have any obvious connection to any particular module
   and might be used in many different contexts.

   #### More should be put here.
   */

#include <config.h>
#include "lisp.h"

Lisp_Object Qall;
Lisp_Object Qboolean;
Lisp_Object Qbuffer;
Lisp_Object Qbytes;
Lisp_Object Qcategory;
Lisp_Object Qcase;
Lisp_Object Qchars;
Lisp_Object Qcolor;
Lisp_Object Qcommand;
Lisp_Object Qcritical;
Lisp_Object Qdefault;
Lisp_Object Qdevice;
Lisp_Object Qdisplay;
Lisp_Object Qdoc_string;
Lisp_Object Qeq;
Lisp_Object Qfont;
Lisp_Object Qframe;
Lisp_Object Qgeometry;
Lisp_Object Qglobal;
Lisp_Object Qhighlight;
Lisp_Object Qinteger;
Lisp_Object Qkeyboard;
Lisp_Object Qmax;
Lisp_Object Qmemory;
Lisp_Object Qmessage;
Lisp_Object Qminus;
Lisp_Object Qname;
Lisp_Object Qnotice;
Lisp_Object Qpath;
Lisp_Object Qprint;
Lisp_Object Qresource;
Lisp_Object Qreverse;
Lisp_Object Qstring;
Lisp_Object Qsymbol;
Lisp_Object Qsyntax;
Lisp_Object Qtext;
Lisp_Object Qunimplemented;
Lisp_Object Qvector;
Lisp_Object Qwarning;
Lisp_Object Qwindow;

void
syms_of_general (void)
{
  defsymbol (&Qminus, "-");
  defsymbol (&Qall, "all");
  defsymbol (&Qboolean, "boolean");
  defsymbol (&Qbuffer, "buffer");
  defsymbol (&Qbytes, "bytes");
  defsymbol (&Qcase, "case");
  defsymbol (&Qcategory, "category");
  defsymbol (&Qchars, "chars");
  defsymbol (&Qcolor, "color");
  defsymbol (&Qcommand, "command");
  defsymbol (&Qcritical, "critical");
  defsymbol (&Qdefault, "default");
  defsymbol (&Qdevice, "device");
  defsymbol (&Qdisplay, "display");
  defsymbol (&Qdoc_string, "doc-string");
  defsymbol (&Qeq, "eq");
  defsymbol (&Qfont, "font");
  defsymbol (&Qframe, "frame");
  defsymbol (&Qgeometry, "geometry");
  defsymbol (&Qglobal, "global");
  defsymbol (&Qhighlight, "highlight");
  defsymbol (&Qinteger, "integer");
  defsymbol (&Qkeyboard, "keyboard");
  defsymbol (&Qmax, "max");
  defsymbol (&Qmemory, "memory");
  defsymbol (&Qmessage, "message");
  defsymbol (&Qminus, "-");
  defsymbol (&Qname, "name");
  defsymbol (&Qnotice, "notice");
  defsymbol (&Qpath, "path");
  defsymbol (&Qprint, "print");
  defsymbol (&Qresource, "resource");
  defsymbol (&Qreverse, "reverse");
  defsymbol (&Qstring, "string");
  defsymbol (&Qsymbol, "symbol");
  defsymbol (&Qsyntax, "syntax");
  defsymbol (&Qtext, "text");
  defsymbol (&Qunimplemented, "unimplemented");
  defsymbol (&Qvector, "vector");
  defsymbol (&Qwarning, "warning");
  defsymbol (&Qwindow, "window");
}

/* Interface from Emacs to terminfo.
   Copyright (C) 1985, 1986, 1993 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: FSF 19.28. */

#include <config.h>

#include <string.h>

/* Define these variables that serve as global parameters to termcap,
   so that we do not need to conditionalize the places in Emacs
   that set them.  */

char *UP, *BC, PC;
#if defined (HAVE_TERMIOS) && !(defined (USG) && (defined (sparc) || defined(INTEL386)))
#include <termios.h>
speed_t ospeed;
#else
short ospeed;
#endif

#ifdef AIX
#include <termio.h>
#endif /* AIX */

/* Interface to curses/terminfo library.
   Turns out that all of the terminfo-level routines look
   like their termcap counterparts except for tparm, which replaces
   tgoto.  Not only is the calling sequence different, but the string
   format is different too.
*/

#include <curses.h>
#include <term.h>

extern void *xmalloc (int size);

char * tparam (CONST char *string, char *outstring, int len, int arg1,
	       int arg2, int arg3, int arg4, int arg5, int arg6, int arg7,
	       int arg8, int arg9);
char *
tparam (CONST char *string, char *outstring, int len, int arg1, int arg2,
	int arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9)
{
  char *temp;

  temp = (char *) tparm (string, arg1, arg2, arg3, arg4, arg5, arg6, arg7,
			 arg8, arg9);
  if (outstring == 0)
    outstring = ((char *) (xmalloc ((strlen (temp)) + 1)));
  strcpy (outstring, temp);
  return outstring;
}

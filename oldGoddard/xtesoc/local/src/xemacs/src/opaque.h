/* Opaque Lisp objects.
   Copyright (C) 1993 Sun Microsystems, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing, October 1993. */

#ifndef _XEMACS_OPAQUE_H_
#define _XEMACS_OPAQUE_H_

struct Lisp_Opaque
{
  struct lcrecord_header header;
  Lisp_Object (*markfun) (Lisp_Object obj, void (*markobj) (Lisp_Object));
  long size;
  char data[1];
};

DECLARE_LRECORD (opaque, struct Lisp_Opaque);
#define XOPAQUE(x) XRECORD (x, opaque, struct Lisp_Opaque)
#define XSETOPAQUE(x, p) XSETRECORD (x, p, opaque)
#define OPAQUEP(x) RECORDP (x, opaque)
/* #define CHECK_OPAQUE(x, i) CHECK_RECORD (x, opaque)
   Opaque pointers should never escape to the Lisp level, so
   functions should not be doing this. */

extern Lisp_Object make_opaque (int size, void *data);
extern Lisp_Object make_opaque_ptr (void *val);
extern Lisp_Object make_opaque_long (long val);

#define OPAQUE_SIZE(op) ((op)->size)
#define OPAQUE_DATA(op) ((op)->data)
#define OPAQUE_MARKFUN(op) ((op)->markfun)
#define XOPAQUE_SIZE(op) OPAQUE_SIZE (XOPAQUE (op))
#define XOPAQUE_DATA(op) OPAQUE_DATA (XOPAQUE (op))
#define XOPAQUE_MARKFUN(op) OPAQUE_MARKFUN (XOPAQUE (op))

#define get_opaque_ptr(op) (* (void **) XOPAQUE_DATA (op))
#define set_opaque_ptr(op, ptr) (get_opaque_ptr (op) = ptr)
#define get_opaque_long(op) (* (long *) XOPAQUE_DATA (op))
#define set_opaque_long(op, ptr) (get_opaque_long (op) = ptr)
#define set_opaque_markfun(op, fun) (XOPAQUE_MARKFUN (op) = fun)

#endif /* _XEMACS_OPAQUE_H_ */

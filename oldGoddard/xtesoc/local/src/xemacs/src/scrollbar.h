/* Define scrollbar instance.
   Copyright (C) 1994, 1995 Board of Trustees, University of Illinois

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_SCROLLBAR_H_
#define _XEMACS_SCROLLBAR_H_

struct scrollbar_instance
{
  /* Used by the frame caches. */
  struct scrollbar_instance *next;

  /* Pointer back to the mirror structure attached to. */
  struct window_mirror *mirror;

  /* This flag indicates if the scrollbar is currently in use. */
  char scrollbar_is_active;

  /* This flag indicates if a data parameter has changed. */
  char scrollbar_instance_changed;

  /* A structure of auxiliary data specific to the device type.
     struct x_scrollbar_data is used for X window frames; defined in
     scrollbar-x.h */
  void *scrollbar_data;
};

#define SCROLLBAR_INSTANCE_FRAME(inst) (inst->mirror->frame)

extern void init_frame_scrollbars (struct frame *f);
extern void init_device_scrollbars (struct device *d);
extern void init_global_scrollbars (struct device *d);
extern void free_frame_scrollbars (struct frame *f);
extern void release_window_mirror_scrollbars (struct window_mirror *mir);
extern void update_window_scrollbars (struct window *w,
				      struct window_mirror *mirror,
				      int active, int horiz_only);

extern Lisp_Object Vscrollbar_width, Vscrollbar_height;

extern Lisp_Object Qscrollbar_line_up;
extern Lisp_Object Qscrollbar_line_down;
extern Lisp_Object Qscrollbar_page_up;
extern Lisp_Object Qscrollbar_page_down;
extern Lisp_Object Qscrollbar_to_top;
extern Lisp_Object Qscrollbar_to_bottom;
extern Lisp_Object Qscrollbar_vertical_drag;

extern Lisp_Object Qscrollbar_char_left;
extern Lisp_Object Qscrollbar_char_right;
extern Lisp_Object Qscrollbar_page_left;
extern Lisp_Object Qscrollbar_page_right;
extern Lisp_Object Qscrollbar_to_left;
extern Lisp_Object Qscrollbar_to_right;
extern Lisp_Object Qscrollbar_horizontal_drag;

#endif /* _XEMACS_SCROLLBAR_H_ */

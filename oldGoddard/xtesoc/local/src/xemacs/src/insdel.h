/* Buffer insertion/deletion and gap motion for XEmacs.
   Copyright (C) 1985-1993 Free Software Foundation, Inc.
   Copyright (C) 1994 Amdahl Corporation.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_INSDEL_H_
#define _XEMACS_INSDEL_H_

extern Memind do_marker_adjustment (Memind mpos, Memind from,
				    Memind to, int amount);

extern int begin_multiple_change (struct buffer *buf, Bufpos start,
				  Bufpos end);
extern void end_multiple_change (struct buffer *buf, int count);

extern void fixup_internal_substring (CONST Bufbyte *nonreloc,
				      Lisp_Object reloc,
				      int offset, int *len);

/* In font-lock.c */
extern void font_lock_maybe_update_syntactic_caches (struct buffer *buf,
						     Bufpos start,
						     Bufpos orig_end,
						     Bufpos new_end);
extern void font_lock_buffer_was_killed (struct buffer *buf);

/* flags for functions below */

#define INSDEL_BEFORE_MARKERS 1
#define INSDEL_NO_LOCKING 2

extern void buffer_insert_string_1 (struct buffer *buf, Bufpos pos,
				    CONST Bufbyte *nonreloc, Lisp_Object reloc,
				    Bytecount offset, Bytecount length,
				    int flags);
extern void buffer_insert_raw_string_1 (struct buffer *buf, Bufpos pos,
					CONST Bufbyte *nonreloc,
					Bytecount length, int flags);
extern void buffer_insert_lisp_string_1 (struct buffer *buf, Bufpos pos,
					 Lisp_Object str, int flags);
extern void buffer_insert_c_string_1 (struct buffer *buf, Bufpos pos,
				      CONST char *s, int flags);
extern void buffer_insert_emacs_char_1 (struct buffer *buf, Bufpos pos,
					Emchar ch, int flags);
extern void buffer_insert_c_char_1 (struct buffer *buf, Bufpos pos, char c,
				    int flags);
extern void buffer_insert_from_buffer_1 (struct buffer *buf, Bufpos pos,
					 struct buffer *buf2, Bufpos pos2,
					 Charcount length, int flags);

/* Macros for insertion functions that insert at point after markers.
   All of these can GC. */

#define buffer_insert_string(buf, nonreloc, reloc, offset, length) \
  buffer_insert_string_1 (buf, -1, nonreloc, reloc, offset, length, 0)
#define buffer_insert_raw_string(buf, string, length) \
  buffer_insert_raw_string_1 (buf, -1, string, length, 0)
#define buffer_insert_c_string(buf, s) \
  buffer_insert_c_string_1 (buf, -1, s, 0)
#define buffer_insert_lisp_string(buf, str) \
  buffer_insert_lisp_string_1 (buf, -1, str, 0)
#define buffer_insert_c_char(buf, c) \
  buffer_insert_c_char_1 (buf, -1, c, 0)
#define buffer_insert_emacs_char(buf, ch) \
  buffer_insert_emacs_char_1 (buf, -1, ch, 0)
#define buffer_insert_from_buffer(buf, b, index, length) \
  buffer_insert_from_buffer_1 (buf, -1, b, index, length, 0)

extern void buffer_delete_range (struct buffer *buf, Bufpos from, Bufpos to,
				 int flags);
extern void buffer_replace_char (struct buffer *b, Bufpos pos, Emchar ch,
				 int not_real_change, int force_lock_check);
extern void barf_if_buffer_read_only (struct buffer *buf, Bufpos from,
				      Bufpos to);

extern Lisp_Object make_string_from_buffer (struct buffer *buf, Bufpos pos,
					    Charcount length);
extern void init_buffer_text (struct buffer *b);
extern void uninit_buffer_text (struct buffer *b);

#ifdef MULE
MAC_DECLARE_EXTERN (Emchar, mactemp_lstream_emchar)
MAC_DECLARE_EXTERN (int, mactemp_lstream_emcint)
extern Emchar Lstream_get_emchar_1 (Lstream *stream, int first_char);
extern int Lstream_fput_emchar (Lstream *stream, Emchar ch);
extern void Lstream_funget_emchar (Lstream *stream, Emchar ch);
#endif

#ifdef MULE
# define Lstream_get_emchar(stream)					\
MAC_BEGIN								\
  MAC_DECLARE (int, mactemp_lstream_emcint, Lstream_getc (stream))	\
  mactemp_lstream_emcint < 0x80 ? (Emchar) mactemp_lstream_emcint :	\
    Lstream_get_emchar_1 (stream, mactemp_lstream_emcint)		\
MAC_END
# define Lstream_put_emchar(stream, ch)				\
MAC_BEGIN							\
  MAC_DECLARE (Emchar, mactemp_lstream_emchar, ch)		\
  CHAR_ASCII_P (mactemp_lstream_emchar) ?			\
    Lstream_putc (stream, mactemp_lstream_emchar) :		\
    Lstream_fput_emchar (stream, mactemp_lstream_emchar)	\
MAC_END
# define Lstream_unget_emchar(stream, ch)			\
MAC_BEGIN							\
  MAC_DECLARE (Emchar, mactemp_lstream_emchar, ch)		\
  CHAR_ASCII_P (mactemp_lstream_emchar) ?			\
    Lstream_ungetc (stream, mactemp_lstream_emchar) :		\
    Lstream_funget_emchar (stream, mactemp_lstream_emchar)	\
MAC_END
#else
# define Lstream_get_emchar(stream) Lstream_getc (stream)
# define Lstream_put_emchar(stream, ch) Lstream_putc (stream, ch)
# define Lstream_unget_emchar(stream, ch) Lstream_ungetc (stream, ch)
#endif

#ifdef MULE

struct buffer_mule_bufpos_data
{
  Bytind bibeg, biend;
  Bufpos beg, end;
  int charsize;
};

#endif

struct buffer_change_data
{
  /* multiple change stuff */
  int in_multiple_change;
  Bufpos mc_begin, mc_orig_end, mc_new_end;
  int mc_begin_signaled;
  Charcount begin_unchanged, end_unchanged;
  /* redisplay needs to know if a newline was deleted so its
     incremental-redisplay algorithm will fail */
  int newline_was_deleted;
  Charcount begin_extent_unchanged, end_extent_unchanged;
};

/* Number of characters at the beginning and end of the buffer that
   have not changed since the last call to buffer_reset_changes().
   If no changes have occurred since then, both values will be -1.

   "Changed" means that the text has changed. */

#define BUF_BEGIN_UNCHANGED(buf) ((buf)->changes->begin_unchanged)
#define BUF_END_UNCHANGED(buf) ((buf)->changes->end_unchanged)

/* Number of characters at the beginning and end of the buffer that
   have not had a covering extent change since the last call to
   buffer_reset_changes ().  If no changes have occurred since then,
   both values will be -1.

   "Changed" means that the extents covering the text have changed. */

#define BUF_EXTENT_BEGIN_UNCHANGED(buf) \
  ((buf)->changes->begin_extent_unchanged)
#define BUF_EXTENT_END_UNCHANGED(buf) ((buf)->changes->end_extent_unchanged)

#define BUF_NEWLINE_WAS_DELETED(buf) \
  ((buf)->changes->newline_was_deleted)

extern void buffer_extent_signal_changed_region (struct buffer *buf,
						 Bufpos start,
						 Bufpos end);
extern void buffer_reset_changes (struct buffer *buf);

#endif /* _XEMACS_INSDEL_H_ */

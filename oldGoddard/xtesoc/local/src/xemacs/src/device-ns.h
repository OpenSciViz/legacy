/* Define NeXTstep specific device and frame object for XEmacs.
   Copyright (C) 1995 Carl Edman

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written by Carl Edman. */

#ifndef _XEMACS_DEVICE_NS_H_
#define _XEMACS_DEVICE_NS_H_

#ifdef HAVE_NEXTSTEP
#endif /* HAVE_NEXTSTEP */

#endif /* _XEMACS_DEVICE_NS_H_ */

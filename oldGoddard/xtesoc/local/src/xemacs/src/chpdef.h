/* This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: FSF 19.28. */

#define	CHP$_END	0
#define	CHP$_ACCESS	1
#define	CHP$_FLAGS	2
#define	CHP$_PRIV	3
#define	CHP$_ACMODE	4
#define	CHP$_ACCLASS	5
#define	CHP$_RIGHTS	6
#define	CHP$_ADDRIGHTS	7
#define	CHP$_MODE	8
#define	CHP$_MODES	9
#define	CHP$_MINCLASS	10
#define	CHP$_MAXCLASS	11
#define	CHP$_OWNER	12
#define	CHP$_PROT	13
#define	CHP$_ACL	14
#define	CHP$_AUDITNAME	15
#define	CHP$_ALARMNAME	16
#define	CHP$_MATCHEDACE	17
#define	CHP$_PRIVUSED	18
#define	CHP$_MAX_CODE	19
#define	CHP$M_SYSPRV	1
#define	CHP$M_BYPASS	2
#define	CHP$M_UPGRADE	4
#define	CHP$M_DOWNGRADE	8
#define	CHP$M_GRPPRV	16
#define	CHP$M_READALL	32
#define	CHP$V_SYSPRV	0
#define	CHP$V_BYPASS	1
#define	CHP$V_UPGRADE	2
#define	CHP$V_DOWNGRADE	3
#define	CHP$V_GRPPRV	4
#define	CHP$V_READALL	5
#define	CHP$M_READ	1
#define	CHP$M_WRITE	2
#define	CHP$M_USEREADALL	4
#define	CHP$V_READ	0
#define	CHP$V_WRITE	1
#define	CHP$V_USEREADALL	2

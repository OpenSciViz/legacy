/* Various function declarations for XEmacs.
   Used to be part of lisp.h
   Copyright (C) 1985-1987, 1992-1994 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Mule 2.0.  Divergent from FSF. */

#ifndef _EMACSFNS_H_
#define _EMACSFNS_H_


/* Defined in abbrev.c */
extern Lisp_Object Vfundamental_mode_abbrev_table;
extern Lisp_Object Fexpand_abbrev (void);


/* Defined in alloc.c */
extern void release_breathing_space (void);
extern Lisp_Object Fcons (Lisp_Object car, Lisp_Object cdr);
extern Lisp_Object Flist (int nargs, Lisp_Object *args);
extern Lisp_Object Fmake_list (Lisp_Object length, Lisp_Object init);
extern Lisp_Object Fmake_vector (Lisp_Object length, Lisp_Object init);
extern Lisp_Object make_vector (int length, Lisp_Object init);
extern Lisp_Object Fvector (int nargs, Lisp_Object *args);
extern Lisp_Object vector1 (Lisp_Object);
extern Lisp_Object vector2 (Lisp_Object, Lisp_Object);
extern Lisp_Object vector3 (Lisp_Object, Lisp_Object, Lisp_Object);
extern Lisp_Object vector4 (Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object);
extern Lisp_Object vector5 (Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object, Lisp_Object);
extern Lisp_Object vector6 (Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object, Lisp_Object, Lisp_Object);
extern Lisp_Object vector7 (Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object);
extern Lisp_Object vector8 (Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object, Lisp_Object, Lisp_Object,
			    Lisp_Object, Lisp_Object);
extern Lisp_Object Fmake_symbol (Lisp_Object name);
extern Lisp_Object Fmake_marker (void);
extern Lisp_Object Fmake_string (Lisp_Object length, Lisp_Object init);
extern void garbage_collect_1 (void);
extern Lisp_Object Fgarbage_collect (void);
extern Lisp_Object list1 (Lisp_Object);
extern Lisp_Object list2 (Lisp_Object, Lisp_Object);
extern Lisp_Object list3 (Lisp_Object, Lisp_Object, Lisp_Object);
extern Lisp_Object list4 (Lisp_Object, Lisp_Object, Lisp_Object, Lisp_Object);
extern Lisp_Object list5 (Lisp_Object, Lisp_Object, Lisp_Object, Lisp_Object,
			  Lisp_Object);
extern Lisp_Object list6 (Lisp_Object, Lisp_Object, Lisp_Object, Lisp_Object,
			  Lisp_Object, Lisp_Object);

extern void malloc_warning (CONST char *);
extern DOESNT_RETURN memory_full (void);
extern void *xmalloc (int size);
extern void *xmalloc_and_zero (int size);
extern void *xrealloc (void *, int size);
#ifdef ERROR_CHECK_MALLOC
extern void xfree_1 (void *);
#else
extern void xfree (void *);
#endif
extern char *xstrdup (CONST char *);
extern void disksave_object_finalization (void);
extern int purify_flag;
extern int gc_in_progress;
extern int gc_currently_forbidden;
extern Lisp_Object restore_gc_inhibit (Lisp_Object);
extern int gc_generation_number[1];
extern int purified (Lisp_Object);

extern Lisp_Object build_string (CONST char *);
extern Lisp_Object build_ext_string (CONST char *);
extern Lisp_Object build_translated_string (CONST char *);
extern Lisp_Object make_string (CONST Bufbyte *, Bytecount);
extern Lisp_Object make_ext_string (CONST char *, int);
extern Lisp_Object make_uninit_string (int length);

extern Lisp_Object make_float (double float_value);

extern Lisp_Object Fmake_byte_code (int nargs, Lisp_Object *args);

extern Lisp_Object Fpurecopy (Lisp_Object);
extern void report_pure_usage (int report_impurities,
                               int die_if_pure_storage_exceeded);
extern Lisp_Object make_pure_string (CONST Bufbyte *, Bytecount len,
				     int nocopy);
extern Lisp_Object make_pure_pname (CONST Bufbyte *, Bytecount len,
				    int nocopy);
extern Lisp_Object pure_cons (Lisp_Object, Lisp_Object);
extern Lisp_Object make_pure_vector (int len, Lisp_Object init);

extern void free_cons (struct Lisp_Cons *ptr);
extern void free_list (Lisp_Object list);
extern void free_alist (Lisp_Object alist);

#ifdef LISP_FLOAT_TYPE
extern Lisp_Object make_pure_float (double float_value);
/* extern void free_float (struct Lisp_Float *); */
#endif


/* Defined in buffer.c */
extern Lisp_Object make_buffer (struct buffer *buf);
extern Lisp_Object Fset_buffer_left_margin_width (Lisp_Object width,
						 Lisp_Object buffer);
extern Lisp_Object Fset_buffer_right_margin_width (Lisp_Object width,
						  Lisp_Object buffer);
extern Lisp_Object Fbuffer_left_margin_width (Lisp_Object buffer);
extern Lisp_Object Fbuffer_right_margin_width (Lisp_Object buffer);
extern Lisp_Object Ferase_buffer (Lisp_Object buffer);
extern Lisp_Object Fbuffer_disable_undo (Lisp_Object buffer);
extern Lisp_Object Fkill_buffer (Lisp_Object buffer);
extern Lisp_Object Fbuffer_name (Lisp_Object buffer);
extern Lisp_Object Fget_buffer (Lisp_Object name);
extern Lisp_Object Fget_buffer_create (Lisp_Object name);
extern Lisp_Object Fget_file_buffer (Lisp_Object fn);
extern Lisp_Object Fset_buffer (Lisp_Object buffer);
extern Lisp_Object Fbarf_if_buffer_read_only (Lisp_Object buffer,
                                              Lisp_Object s, Lisp_Object e);
extern Lisp_Object Fcurrent_buffer (void);
extern Lisp_Object Fswitch_to_buffer (Lisp_Object buf, Lisp_Object norecord);
extern Lisp_Object Fpop_to_buffer (Lisp_Object bufname, Lisp_Object other,
                                   Lisp_Object same_frame);
extern Lisp_Object Fother_buffer (Lisp_Object buffer, Lisp_Object frame,
				  Lisp_Object visible_ok);
extern Lisp_Object Fbuffer_list (Lisp_Object frame);
extern Lisp_Object Fset_buffer_modified_p (Lisp_Object flag,
					   Lisp_Object buffer);
extern Lisp_Object QSscratch;   /* "*scratch*" */
extern Lisp_Object Qbuffer_file_name, Qbuffer_undo_list;
extern Lisp_Object Qdefault_directory;
extern int find_file_compare_truenames;
extern int find_file_use_truenames;
extern Lisp_Object Fbuffer_modified_p (Lisp_Object buffer);
extern Lisp_Object Fgenerate_new_buffer_name (Lisp_Object name,
                                              Lisp_Object ignore);
extern Lisp_Object Frename_buffer (Lisp_Object name, Lisp_Object unique);

/* Functions to call before and after each text change. */
extern Lisp_Object Vbefore_change_functions;
extern Lisp_Object Qbefore_change_functions;
extern Lisp_Object Vafter_change_functions;
extern Lisp_Object Qafter_change_functions;

/* #### Obsolete, for compatibility */
extern Lisp_Object Vbefore_change_function;
extern Lisp_Object Qbefore_change_function;
extern Lisp_Object Vafter_change_function;
extern Lisp_Object Qafter_change_function;

extern Lisp_Object Vfirst_change_hook;
extern Lisp_Object Qfirst_change_hook;
extern Lisp_Object Vinhibit_read_only;

extern Lisp_Object Qpermanent_local, Qprotected_field;


/* Defined in bytecode.c */
extern Lisp_Object Qbytecode;
extern Lisp_Object Fbyte_code (Lisp_Object bytestr, 
                               Lisp_Object constants_vector, 
                               Lisp_Object maxdepth);


/* Defined in callint.c */
extern Lisp_Object Vcommand_history;
extern Lisp_Object Qcall_interactively;
extern Lisp_Object Fcall_interactively (Lisp_Object fn, Lisp_Object record);
extern Lisp_Object Fprefix_numeric_value (Lisp_Object prefix);
extern Lisp_Object Qread_from_minibuffer;
extern Lisp_Object Qenable_recursive_minibuffers;
extern Lisp_Object Qcompleting_read;
extern Lisp_Object Qread_file_name;
extern Lisp_Object Qread_directory_name;
extern Lisp_Object Qread_buffer;


/* Defined in callproc.c */
extern Lisp_Object Vexec_path, Vexec_directory, Vdata_directory,
		   Vdoc_directory;


/* Defined in casefiddle.c */
extern Lisp_Object Fupcase (Lisp_Object obj, Lisp_Object buffer);
extern Lisp_Object Fdowncase (Lisp_Object obj, Lisp_Object buffer);
extern Lisp_Object Fcapitalize (Lisp_Object obj, Lisp_Object buffer);
extern Lisp_Object Fupcase_region (Lisp_Object b, Lisp_Object e,
				   Lisp_Object buffer);
extern Lisp_Object Fdowncase_region (Lisp_Object b, Lisp_Object e,
				     Lisp_Object buffer);
extern Lisp_Object Fcapitalize_region (Lisp_Object b, Lisp_Object e,
				       Lisp_Object buffer);
extern Lisp_Object upcase_initials_region (struct buffer *buf, Lisp_Object b,
					   Lisp_Object e);
extern Lisp_Object Fupcase_word (Lisp_Object arg, Lisp_Object buffer);
extern Lisp_Object Fdowncase_word (Lisp_Object arg, Lisp_Object buffer);
extern Lisp_Object Fcapitalize_word (Lisp_Object arg, Lisp_Object buffer);

extern Lisp_Object Vascii_downcase_table, Vascii_upcase_table;
extern Lisp_Object Vascii_canon_table, Vascii_eqv_table;


/* Defined in cmds.c */
extern Lisp_Object Fforward_char (Lisp_Object n, Lisp_Object buffer);
extern Lisp_Object Fforward_line (Lisp_Object n, Lisp_Object buffer);
extern Lisp_Object Fend_of_line (Lisp_Object n, Lisp_Object buffer);
extern Lisp_Object Fbeginning_of_line (Lisp_Object n, Lisp_Object buffer);
extern Lisp_Object Qself_insert_command;


/* Defined in data.c */
extern Lisp_Object Qnil, Qt, Qquote, Qlambda, Qfunction, Qunbound;
extern Lisp_Object Qerror_conditions, Qerror_message, Qtop_level, Qsignal;
extern Lisp_Object Qerror, Qquit, Qwrong_type_argument, Qargs_out_of_range;
extern Lisp_Object Qvoid_function, Qvoid_variable;
extern Lisp_Object Qcyclic_function_indirection, Qcyclic_variable_indirection;
extern Lisp_Object Qsetting_constant, Qinvalid_read_syntax;
extern Lisp_Object Qinvalid_function, Qwrong_number_of_arguments, Qno_catch;
extern Lisp_Object Qend_of_file, Qarith_error;
extern Lisp_Object Qrange_error, Qdomain_error, Qsingularity_error;
extern Lisp_Object Qsingularity_error, Qoverflow_error, Qunderflow_error;
extern Lisp_Object Qbeginning_of_buffer, Qend_of_buffer, Qbuffer_read_only;

extern Lisp_Object Qintegerp, Qnatnump, Qsymbolp, Qlistp, Qconsp;
extern Lisp_Object Qstringp, Qarrayp, Qsequencep, Qbufferp, Qsubrp;
extern Lisp_Object Qcharacterp, Qchar_or_string_p, Qmarkerp, Qvectorp;
extern Lisp_Object Qinteger_or_marker_p, Qboundp, Qfboundp;
extern Lisp_Object Qcons;
extern Lisp_Object Qcdr, Qignore;

extern Lisp_Object Qnumberp, Qnumber_or_marker_p;

extern Lisp_Object Qvalues;
extern Lisp_Object Qprogn, Qeval;
extern Lisp_Object Qstring_or_buffer_p;

extern Lisp_Object Qfloatp;
extern Lisp_Object Ftruncate (Lisp_Object n);

extern Lisp_Object Flistp (Lisp_Object x);

extern Lisp_Object Fcar (Lisp_Object cons), Fcar_safe (Lisp_Object cons);
extern Lisp_Object Fcdr (Lisp_Object cons), Fcdr_safe (Lisp_Object cons);
extern Lisp_Object Fsetcar (Lisp_Object cons, Lisp_Object val);
extern Lisp_Object Fsetcdr (Lisp_Object cons, Lisp_Object val);
extern Lisp_Object Faref (Lisp_Object array, Lisp_Object idx);
extern Lisp_Object Faset (Lisp_Object array, Lisp_Object idx, Lisp_Object x);
extern Lisp_Object Farray_length (Lisp_Object array);
extern Lisp_Object Felt (Lisp_Object seq, Lisp_Object idx);

extern Lisp_Object Fzerop (Lisp_Object);
extern Lisp_Object Fnumber_to_string (Lisp_Object num);
extern Lisp_Object Fstring_to_number (Lisp_Object str);

#ifndef make_number
extern Lisp_Object make_number (LISP_WORD_TYPE);
#endif
extern DOESNT_RETURN pure_write_error (void);
extern DOESNT_RETURN args_out_of_range (Lisp_Object, Lisp_Object);
extern DOESNT_RETURN args_out_of_range_3 (Lisp_Object, Lisp_Object, Lisp_Object);
extern Lisp_Object wrong_type_argument (Lisp_Object pred, Lisp_Object value);
extern void check_int_range (int val, int min, int max);

extern Lisp_Object word_to_lisp (unsigned int);
extern unsigned int lisp_to_word (Lisp_Object);

extern Lisp_Object Fcompiled_function_instructions (Lisp_Object function);
extern Lisp_Object Fcompiled_function_constants (Lisp_Object function);
extern Lisp_Object Fcompiled_function_stack_depth (Lisp_Object function);
extern Lisp_Object Fcompiled_function_arglist (Lisp_Object function);
extern Lisp_Object Fcompiled_function_interactive (Lisp_Object function);
extern Lisp_Object Fcompiled_function_domain (Lisp_Object function);

extern Lisp_Object Fquo (int nargs, Lisp_Object *args);
extern Lisp_Object Fsub1 (Lisp_Object num);
extern Lisp_Object Fadd1 (Lisp_Object num);
extern Lisp_Object Fgtr (Lisp_Object num1, Lisp_Object num2);
extern Lisp_Object Flss (Lisp_Object num1, Lisp_Object num2);
extern Lisp_Object Fleq (Lisp_Object num1, Lisp_Object num2);
extern Lisp_Object Fgeq (Lisp_Object num1, Lisp_Object num2);
extern Lisp_Object Fminus (int nargs, Lisp_Object *args);
extern Lisp_Object Fplus (int nargs, Lisp_Object *args);
extern Lisp_Object Fmin (int nargs, Lisp_Object *args);
extern Lisp_Object Fmax (int nargs, Lisp_Object *args);
extern Lisp_Object Ftimes (int nargs, Lisp_Object *args);
extern Lisp_Object Frem (Lisp_Object num1, Lisp_Object num2);


/* Defined in device.c */
extern Lisp_Object Fmake_device (Lisp_Object type, Lisp_Object device_data);
extern Lisp_Object Fselect_device (Lisp_Object device);
extern Lisp_Object Fdevice_enable_input (Lisp_Object device);
extern Lisp_Object Fdevice_disable_input (Lisp_Object device);
extern Lisp_Object Qgrayscale, Qmono;


/* Defined in device-stream.c */
extern Lisp_Object Qstream;


/* Defined in device-tty.c */
extern Lisp_Object Qtty;


#ifdef HAVE_X_WINDOWS
/* Defined in device-x.c */
extern Lisp_Object Fx_display_grayscale_p (Lisp_Object device);
extern Lisp_Object Fx_display_color_p (Lisp_Object device);
extern Lisp_Object Fx_valid_color_name_p (Lisp_Object name,
					  Lisp_Object device);
extern Lisp_Object Qx;
#endif


#ifdef HAVE_NEXTSTEP
/* Defined in device-ns.m */
extern Lisp_Object Fns_display_grayscale_p (Lisp_Object device);
extern Lisp_Object Fns_display_color_p (Lisp_Object device);
extern Lisp_Object Fns_valid_color_name_p (Lisp_Object name,
					   Lisp_Object device);
extern Lisp_Object Qns;
#endif


/* Defined in dired.c */
extern Lisp_Object make_directory_hash_table (char *path);


/* Defined in doc.c */
extern Lisp_Object Vdoc_file_name;
extern Lisp_Object Fsubstitute_command_keys (Lisp_Object string);
extern Lisp_Object Fdocumentation (Lisp_Object fun, Lisp_Object raw);
extern Lisp_Object Fdocumentation_property (Lisp_Object sym, Lisp_Object prop,
                                            Lisp_Object raw);


/* Defined in doprnt.c */
extern Bytecount emacs_doprnt_c (Lisp_Object stream,
				 CONST Bufbyte *format_nonreloc,
				 Lisp_Object format_reloc,
				 Bytecount format_length,
				 ...);
extern Bytecount emacs_doprnt_va (Lisp_Object stream,
				  CONST Bufbyte *format_nonreloc,
				  Lisp_Object format_reloc,
				  Bytecount format_length,
				  va_list vargs);
extern Bytecount emacs_doprnt_lisp (Lisp_Object stream,
				    CONST Bufbyte *format_nonreloc,
				    Lisp_Object format_reloc,
				    Bytecount format_length,
				    int nargs, CONST Lisp_Object *largs);
extern Lisp_Object emacs_doprnt_string_c (CONST Bufbyte *format_nonreloc,
					  Lisp_Object format_reloc,
					  Bytecount format_length,
					  ...);
extern Lisp_Object emacs_doprnt_string_va (CONST Bufbyte *format_nonreloc,
					   Lisp_Object format_reloc,
					   Bytecount format_length,
					   va_list vargs);
extern Lisp_Object emacs_doprnt_string_lisp (CONST Bufbyte *format_nonreloc,
					     Lisp_Object format_reloc,
					     Bytecount format_length,
					     int nargs,
					     CONST Lisp_Object *largs);


/* Defined in editfns.c */
extern Bufpos bufpos_clip_to_bounds (Bufpos lower, Bufpos num, Bufpos upper);
extern Bytind bytind_clip_to_bounds (Bytind lower, Bytind num, Bytind upper);
extern Lisp_Object time_to_lisp (time_t the_time);
extern int lisp_to_time (Lisp_Object specified_time, time_t *result);
extern Lisp_Object Fwiden (Lisp_Object buffer);
extern Lisp_Object Fnarrow_to_region (Lisp_Object b, Lisp_Object e,
				      Lisp_Object buffer);
extern Lisp_Object Vprefix_arg, Vcurrent_prefix_arg;
extern Lisp_Object Qcurrent_prefix_arg;
extern Lisp_Object Fgoto_char (Lisp_Object pos, Lisp_Object buffer);
extern Lisp_Object Fpoint_min_marker (Lisp_Object buffer);
extern Lisp_Object Fpoint_max_marker (Lisp_Object buffer);
extern Lisp_Object Fpoint_min (Lisp_Object buffer);
extern Lisp_Object Fpoint_max (Lisp_Object buffer);
extern Lisp_Object Fpoint (Lisp_Object buffer);
extern Lisp_Object Fpoint_marker (Lisp_Object dont_copy_p, Lisp_Object buffer);
extern Lisp_Object Fmark_marker (Lisp_Object inactive_p, Lisp_Object buffer);
extern Lisp_Object Ffollowing_char (Lisp_Object buffer);
extern Lisp_Object Fprevious_char (Lisp_Object buffer);
extern Lisp_Object Fchar_after (Lisp_Object pos, Lisp_Object buffer);
extern Lisp_Object Finsert (int nargs, Lisp_Object *args);
extern Lisp_Object Finsert_char (Lisp_Object ch, Lisp_Object count,
				 Lisp_Object ignored);
extern void buffer_insert1 (struct buffer *buf, Lisp_Object arg);
extern Lisp_Object Finsert_before_markers (int nargs, Lisp_Object *args);
extern Lisp_Object Finsert_buffer_substring (Lisp_Object buffer, 
                                             Lisp_Object b, Lisp_Object e);
extern Lisp_Object Fdelete_region (Lisp_Object b, Lisp_Object e,
				   Lisp_Object buffer);
extern Lisp_Object Feolp (Lisp_Object buffer);
extern Lisp_Object Feobp (Lisp_Object buffer);
extern Lisp_Object Fbolp (Lisp_Object buffer);
extern Lisp_Object Fbobp (Lisp_Object buffer);
extern Lisp_Object Fformat (int nargs, Lisp_Object *args);
extern Lisp_Object Fbuffer_substring (Lisp_Object start, Lisp_Object end,
				      Lisp_Object buffer);
extern Lisp_Object make_string_from_buffer (struct buffer *buf,
                                            int pos, int length);
extern Lisp_Object save_excursion_save (void), save_restriction_save (void);
extern Lisp_Object save_excursion_restore (Lisp_Object info);
extern Lisp_Object save_restriction_restore (Lisp_Object info);
extern Lisp_Object Fchar_to_string (Lisp_Object ch);
extern Lisp_Object Fcurrent_time_seconds (Lisp_Object cons);
extern Lisp_Object Fgetenv (Lisp_Object var, Lisp_Object interactivep);
extern Lisp_Object Qpoint, Qmark, Qregion_beginning, Qregion_end;
extern Lisp_Object Qformat;


/* Defined in elhash.c */
extern Lisp_Object Fhashtablep (Lisp_Object obj);
extern Lisp_Object Fmake_hashtable (Lisp_Object size);
extern Lisp_Object Fcopy_hashtable (Lisp_Object old_table);
extern Lisp_Object Fgethash (Lisp_Object key, Lisp_Object table,
                             Lisp_Object def);
extern Lisp_Object Fremhash (Lisp_Object key, Lisp_Object table);
extern Lisp_Object Fputhash (Lisp_Object key, Lisp_Object val,
                             Lisp_Object table);
extern Lisp_Object Fclrhash (Lisp_Object table);
extern Lisp_Object Fhashtable_fullness (Lisp_Object table);
extern Lisp_Object Fmaphash (Lisp_Object function, Lisp_Object table);

extern Lisp_Object Vcharacter_set_property;


/* Defined in emacs.c */
extern DOESNT_RETURN fatal (CONST char *fmt, ...); /* ... printf args */
extern DOESNT_RETURN error (CONST char *fmt, ...); /* ... printf args */
extern int stderr_out (CONST char *fmt, ...);      /* ... printf args */
extern int stdout_out (CONST char *fmt, ...);      /* ... printf args */
extern Lisp_Object continuable_error (CONST char *fmt, ...); /* printf args */
extern SIGTYPE fatal_error_signal (int sig);
extern Lisp_Object make_arg_list (int argc, char **argv);
extern void make_argc_argv (Lisp_Object argv_list, int *argc, char ***argv);
extern void free_argc_argv (char **argv);
extern Lisp_Object decode_env_path (CONST char *evarname, CONST char *def);
/* Nonzero means don't do interactive redisplay and don't change tty modes */
extern int noninteractive;
extern Lisp_Object Fkill_emacs (Lisp_Object arg);

extern Lisp_Object Vcommand_line_args;
extern Lisp_Object Vinvocation_name;
extern Lisp_Object Vinvocation_directory;

extern int emacs_priority;
extern int suppress_early_backtrace;

extern Lisp_Object Qsave_buffers_kill_emacs;
extern Lisp_Object Qkill_emacs_hook;


/* Defined in eval.c */
extern Lisp_Object Qautoload, Qexit, Qinteractive, Qcommandp, Qdefun, Qmacro;
extern Lisp_Object Vinhibit_quit, Vquit_flag, Qinhibit_quit;
extern Lisp_Object Vautoload_queue;
extern Lisp_Object Vrun_hooks;
extern Lisp_Object Fuser_variable_p (Lisp_Object);
extern Lisp_Object Finteractive_p (void);
extern DOESNT_RETURN signal_error (Lisp_Object sig, Lisp_Object data);
extern DOESNT_RETURN signal_simple_error (CONST char *, Lisp_Object);
extern DOESNT_RETURN signal_simple_error_2 (CONST char *,
					    Lisp_Object, Lisp_Object);
extern Lisp_Object signal_simple_continuable_error (CONST char *,
						      Lisp_Object);
extern Lisp_Object signal_simple_continuable_error_2 (CONST char *,
						      Lisp_Object,
						      Lisp_Object);
extern Lisp_Object Fprogn (Lisp_Object args);
extern Lisp_Object Fcommandp (Lisp_Object obj);
extern Lisp_Object Feval (Lisp_Object form);
extern Lisp_Object Fapply (int nargs, Lisp_Object *args);
extern Lisp_Object Ffuncall (int nargs, Lisp_Object *args);
extern Lisp_Object Fbacktrace (Lisp_Object stream, Lisp_Object detailed);
extern Lisp_Object apply1 (Lisp_Object fn, Lisp_Object args);
extern Lisp_Object call0 (Lisp_Object fn);
extern Lisp_Object call1 (Lisp_Object fn, Lisp_Object a0);
extern Lisp_Object call2 (Lisp_Object fn, Lisp_Object a0, Lisp_Object a1);
extern Lisp_Object call3 (Lisp_Object fn,
                          Lisp_Object a0, Lisp_Object a1, Lisp_Object a2);
extern Lisp_Object call4 (Lisp_Object fn,
                          Lisp_Object a0, Lisp_Object a1, Lisp_Object a2,
                          Lisp_Object a3);
extern Lisp_Object call5 (Lisp_Object fn,
                          Lisp_Object a0, Lisp_Object a1, Lisp_Object a2,
                          Lisp_Object a3, Lisp_Object a4);
extern Lisp_Object call6 (Lisp_Object fn,
                          Lisp_Object a0, Lisp_Object a1, Lisp_Object a2,
                          Lisp_Object a3, Lisp_Object a4, Lisp_Object a5);
extern Lisp_Object call0_in_buffer (struct buffer *buf, Lisp_Object fn);
extern Lisp_Object call1_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0);
extern Lisp_Object call2_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0, Lisp_Object a1);
extern Lisp_Object call3_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0, Lisp_Object a1,
				    Lisp_Object a2);
extern Lisp_Object call4_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0, Lisp_Object a1,
				    Lisp_Object a2, Lisp_Object a3);
extern Lisp_Object call5_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0, Lisp_Object a1,
				    Lisp_Object a2, Lisp_Object a3,
				    Lisp_Object a4);
extern Lisp_Object call6_in_buffer (struct buffer *buf, Lisp_Object fn,
				    Lisp_Object a0, Lisp_Object a1,
				    Lisp_Object a2, Lisp_Object a3,
				    Lisp_Object a4, Lisp_Object a5);
extern Lisp_Object eval_in_buffer (struct buffer *buf, Lisp_Object form);
extern Lisp_Object call0_with_handler (Lisp_Object handler, Lisp_Object fn);
extern Lisp_Object call1_with_handler (Lisp_Object handler, Lisp_Object fn,
				       Lisp_Object a0);
extern Lisp_Object eval_in_buffer_trapping_errors (char *warning_string,
						   struct buffer *buf,
						   Lisp_Object form);
extern Lisp_Object run_hook_trapping_errors (char *warning_string,
					     Lisp_Object hook_symbol);
extern Lisp_Object call0_trapping_errors (char *warning_string,
					  Lisp_Object function);
extern Lisp_Object call1_trapping_errors (char *warning_string,
					  Lisp_Object function,
					  Lisp_Object object);
extern Lisp_Object Fsignal (Lisp_Object signame, Lisp_Object data);
/* C Code should be using internal_catch, record_unwind_p, condition_case_1 */
/* extern Lisp_Object Fcatch (Lisp_Object args); */
/* extern Lisp_Object Funwind_protect (Lisp_Object args); */
/* extern Lisp_Object Fcondition_case (Lisp_Object args); */
extern Lisp_Object Fthrow (Lisp_Object tag, Lisp_Object val);
extern Lisp_Object internal_catch (Lisp_Object tag, 
                                   Lisp_Object (*func) (Lisp_Object arg),
                                   Lisp_Object arg,
                                   int *threw);
extern Lisp_Object condition_case_1 (Lisp_Object handlers,
                                     Lisp_Object (*bfun) (Lisp_Object barg),
                                     Lisp_Object barg,
                                     Lisp_Object (*hfun) (Lisp_Object val,
                                                          Lisp_Object harg),
                                     Lisp_Object harg);
extern Lisp_Object Fcondition_case_3 (Lisp_Object bodyform, 
                                      Lisp_Object var, 
                                      Lisp_Object handlers);
extern Lisp_Object unbind_to (int n, Lisp_Object val);
extern void specbind (Lisp_Object symbol, Lisp_Object value);
extern void record_unwind_protect (Lisp_Object (*function) (Lisp_Object arg),
                                   Lisp_Object arg);
extern void do_autoload (Lisp_Object fundef, Lisp_Object funname);
extern Lisp_Object un_autoload (Lisp_Object oldqueue);
extern void warn_when_safe_lispstr (Lisp_Object class, Lisp_Object level,
				    Lisp_Object str);
extern void warn_when_safe (Lisp_Object class, Lisp_Object level,
			    CONST char *fmt, ...);


/* Defined in event-stream.c */
extern Lisp_Object Qemacs_handle_focus_change;
extern Lisp_Object Fread_key_sequence (Lisp_Object prompt);
extern Lisp_Object Fsit_for (Lisp_Object seconds, Lisp_Object nodisp);
extern Lisp_Object Fsleep_for (Lisp_Object seconds);
extern Lisp_Object Faccept_process_output (Lisp_Object process,
					   Lisp_Object timeout_secs,
					   Lisp_Object timeout_msecs);
extern Lisp_Object Fnext_event (Lisp_Object event, Lisp_Object prompt);
extern Lisp_Object Fnext_command_event (Lisp_Object event, Lisp_Object prompt);
extern Lisp_Object Fdispatch_event (Lisp_Object event);
extern void wait_delaying_user_input (int (*predicate) (void *arg),
                                      void *predicate_arg);
extern int detect_input_pending (void);
extern void enqueue_command_event (Lisp_Object event);
extern Lisp_Object Femacs_handle_focus_change (Lisp_Object frame_inp_and_dev);
extern Lisp_Object dequeue_command_event (void);
extern void enqueue_event (Lisp_Object event, Lisp_Object *head,
			   struct Lisp_Event **tail);
extern Lisp_Object dequeue_event (Lisp_Object *head,
				  struct Lisp_Event **tail);
extern Lisp_Object Fadd_timeout (Lisp_Object secs, 
                                 Lisp_Object function, Lisp_Object object, 
                                 Lisp_Object resignal);
extern Lisp_Object Fdisable_timeout (Lisp_Object id); 
extern Lisp_Object reset_this_command_keys (Lisp_Object reset_key_echo);
extern Lisp_Object Fenqueue_eval_event (Lisp_Object function,
					Lisp_Object object);
extern Lisp_Object enqueue_misc_user_event (Lisp_Object function,
					    Lisp_Object object);


/* Defined in event-Xt.c */

extern void signal_special_Xt_user_event (Lisp_Object function,
					  Lisp_Object object);


/* Defined in events.c */
extern void clear_event_resource (void);
extern Lisp_Object Fallocate_event (void);
extern Lisp_Object Fdeallocate_event (Lisp_Object event);
extern Lisp_Object Fcopy_event (Lisp_Object from, Lisp_Object to);
extern Lisp_Object make_event (void);
extern int event_to_character (struct Lisp_Event *event,
			       int allow_extra_modifiers,
			       int allow_meta,
			       int allow_non_ascii);
extern Lisp_Object Fcharacter_to_event (Lisp_Object ch, Lisp_Object event,
					Lisp_Object device);
extern Lisp_Object Fevent_to_character (Lisp_Object e,
					Lisp_Object allow_extra_modifiers,
					Lisp_Object allow_meta,
					Lisp_Object allow_non_ascii);
extern Lisp_Object Fevent_over_text_area_p (Lisp_Object event);
extern Lisp_Object Fevent_over_modeline_p (Lisp_Object event);
extern Lisp_Object Fevent_over_border_p (Lisp_Object event);
extern Lisp_Object Fevent_over_toolbar_p (Lisp_Object event);
extern Lisp_Object Fevent_window (Lisp_Object event);
extern Lisp_Object Fevent_buffer (Lisp_Object event);
extern Lisp_Object Fevent_frame (Lisp_Object event);
extern Lisp_Object Fevent_button (Lisp_Object event);
extern Lisp_Object Fevent_function (Lisp_Object event);
extern Lisp_Object Fevent_glyph_extent (Lisp_Object event);
extern Lisp_Object Fevent_key (Lisp_Object event);
extern Lisp_Object Fevent_modifiers (Lisp_Object event);
extern Lisp_Object Fevent_modifier_bits (Lisp_Object event);
extern Lisp_Object Fevent_object (Lisp_Object event);
extern Lisp_Object Fevent_point (Lisp_Object event);
extern Lisp_Object Fevent_process (Lisp_Object event);
extern Lisp_Object Fevent_timestamp (Lisp_Object event);
extern Lisp_Object Fevent_x (Lisp_Object event);
extern Lisp_Object Fevent_y (Lisp_Object event);
extern Lisp_Object Fevent_x_pixel (Lisp_Object event);
extern Lisp_Object Fevent_y_pixel (Lisp_Object event);

extern Lisp_Object QKbackspace, QKtab, QKlinefeed, QKreturn;
extern Lisp_Object QKescape, QKspace, QKdelete, QKnosymbol;


/* Defined in extents.c */
extern Lisp_Object Fextentp (Lisp_Object obj);
extern Lisp_Object Fextent_object (Lisp_Object ext);
extern Lisp_Object Fextent_start_position (Lisp_Object ext);
extern Lisp_Object Fextent_end_position (Lisp_Object ext);
extern Lisp_Object Fextent_length (Lisp_Object ext);
extern Lisp_Object Fmake_extent (Lisp_Object from, Lisp_Object to,
                                 Lisp_Object buffer);
extern Lisp_Object Fset_extent_endpoints (Lisp_Object,
					  Lisp_Object, Lisp_Object);
extern Lisp_Object Fdelete_extent (Lisp_Object extent);
extern Lisp_Object Fdetach_extent (Lisp_Object);
extern Lisp_Object Fmap_extents (Lisp_Object function, 
                                 Lisp_Object buffer, 
                                 Lisp_Object from, Lisp_Object to,
                                 Lisp_Object maparg,
                                 Lisp_Object flags,
				 Lisp_Object property,
				 Lisp_Object value);
extern Lisp_Object Fextent_at (Lisp_Object pos, Lisp_Object buffer, 
                               Lisp_Object flag, Lisp_Object before);
extern Lisp_Object Fset_extent_begin_glyph (Lisp_Object extent,
					    Lisp_Object begin_glyph,
					    Lisp_Object layout);
extern Lisp_Object Fset_extent_end_glyph (Lisp_Object extent_obj,
					  Lisp_Object glyph,
					  Lisp_Object layout);
extern Lisp_Object Fset_extent_begin_glyph_layout (Lisp_Object extent,
						   Lisp_Object layout);
extern Lisp_Object Fset_extent_end_glyph_layout (Lisp_Object extent,
						   Lisp_Object layout);
extern Lisp_Object Fextent_begin_glyph_layout (Lisp_Object extent);
extern Lisp_Object Fextent_end_glyph_layout (Lisp_Object extent);
extern Lisp_Object Fset_extent_priority (Lisp_Object extent, Lisp_Object pri);
extern Lisp_Object Fset_extent_property (Lisp_Object,Lisp_Object,Lisp_Object);
extern Lisp_Object Fextent_property (Lisp_Object extent, Lisp_Object);
extern Lisp_Object Fforce_highlight_extent (Lisp_Object extent,
                                            Lisp_Object flag);
extern Lisp_Object Fhighlight_extent (Lisp_Object extent, Lisp_Object flag);
extern Lisp_Object Fextent_replica_p (Lisp_Object object);
extern Lisp_Object Fextent_replica_extent (Lisp_Object extent_replica);
extern Lisp_Object Fextent_replica_start (Lisp_Object extent_replica);
extern Lisp_Object Fextent_replica_end (Lisp_Object extent_replica);

extern Lisp_Object Fnext_single_property_change (Lisp_Object pos,
						 Lisp_Object prop,
						 Lisp_Object buffer,
						 Lisp_Object limit);
extern Lisp_Object Fprevious_single_property_change (Lisp_Object pos,
						     Lisp_Object prop,
						     Lisp_Object buffer,
						     Lisp_Object limit);
extern Lisp_Object Qdetached, Qdestroyed, Qbegin_glyph, Qend_glyph;
extern Lisp_Object Qstart_open, Qend_open, Qread_only;
extern Lisp_Object Qunique, Qduplicable, Qinvisible;
extern Lisp_Object Qoutside_margin, Qinside_margin, Qwhitespace;
extern Lisp_Object Qglyph_invisible;

extern Lisp_Object Qdup_list;


/* Defined in faces.c */
extern Lisp_Object Qface, Qextent_face, Qset_extent_face;
extern Lisp_Object Fextent_face (Lisp_Object);
extern Lisp_Object Fset_extent_face (Lisp_Object, Lisp_Object);


/* Defined in fileio.c */
extern Lisp_Object Qfile_name_handler_alist;
extern Lisp_Object Qfile_error;
extern Lisp_Object Ffile_name_as_directory (Lisp_Object fn);
extern Lisp_Object Fexpand_file_name (Lisp_Object fn, Lisp_Object def);
extern Lisp_Object Ffile_name_nondirectory (Lisp_Object fn);
extern Lisp_Object Fsubstitute_in_file_name (Lisp_Object fn);
extern Lisp_Object Ffile_symlink_p (Lisp_Object fn);
extern Lisp_Object Ffile_truename (Lisp_Object name, Lisp_Object def);
extern Lisp_Object Ffile_name_nondirectory (Lisp_Object fn);
extern Lisp_Object Ffile_name_directory (Lisp_Object fn);
extern Lisp_Object Fdirectory_file_name (Lisp_Object fn);
extern Lisp_Object Ffile_directory_p (Lisp_Object fn);
extern Lisp_Object Ffile_readable_p (Lisp_Object fn);
extern Lisp_Object Ffile_name_absolute_p (Lisp_Object fn);
extern Lisp_Object Ffile_exists_p (Lisp_Object fn);
extern Lisp_Object Ffile_executable_p (Lisp_Object filename);
extern Lisp_Object Ffile_accessible_directory_p (Lisp_Object fn);
extern void record_auto_save (void);
extern void force_auto_save_soon (void);
extern Lisp_Object Ffind_file_name_handler (Lisp_Object filename,
                                            Lisp_Object operation);
extern DOESNT_RETURN report_file_error (CONST char *string, Lisp_Object data);
extern DOESNT_RETURN signal_file_error (CONST char *string, Lisp_Object data);
extern DOESNT_RETURN signal_double_file_error (CONST char *string1,
					       CONST char *string2,
					       Lisp_Object data);
extern DOESNT_RETURN signal_double_file_error_2 (CONST char *string1,
						 CONST char *string2,
						 Lisp_Object data1,
						 Lisp_Object data2);
extern Lisp_Object expand_and_dir_to_file (Lisp_Object fn, Lisp_Object def);
extern Lisp_Object Finsert_file_contents_internal (Lisp_Object filename,
						   Lisp_Object visit,
						   Lisp_Object beg,
						   Lisp_Object end,
						   Lisp_Object replace);
extern Lisp_Object Fwrite_region_intrenal (Lisp_Object start, Lisp_Object end,
					   Lisp_Object filename, 
					   Lisp_Object append,
					   Lisp_Object visit);
extern Lisp_Object Fdo_auto_save (Lisp_Object nomsg, Lisp_Object current_only);
extern Lisp_Object Fverify_visited_file_modtime (Lisp_Object buffer);
extern Lisp_Object Funhandled_file_name_directory (Lisp_Object filename);
extern Lisp_Object Fset_buffer_modtime (Lisp_Object buf, Lisp_Object in_time);
extern int read_allowing_quit (int fildes, void *buf, unsigned int nbyte);
extern int write_allowing_quit (int fildes, CONST void *buf,
				unsigned int nbyte);


/* Defined in filelock.c */
extern void lock_file (Lisp_Object fn);
extern void unlock_file (Lisp_Object fn);
extern void unlock_all_files (void);
extern Lisp_Object Flock_buffer (Lisp_Object fn);
extern Lisp_Object Funlock_buffer (void);
extern void unlock_buffer (struct buffer *buffer);
extern Lisp_Object Ffile_locked_p (Lisp_Object fn);


/* Defined in filemode.c */
extern void filemodestring (struct stat *statp, char *str);


/* Defined in floatfns.c */
extern double extract_float (Lisp_Object);
extern Lisp_Object Ffloat (Lisp_Object n);


/* Defined in fns.c */
extern Lisp_Object list_sort (Lisp_Object list, 
                              Lisp_Object lisp_arg,
                              int (*pred_fn) (Lisp_Object first,
                                              Lisp_Object second,
                                              Lisp_Object lisp_arg));
extern Lisp_Object Fsort (Lisp_Object list, 
                          Lisp_Object pred);
extern Lisp_Object merge (Lisp_Object org_l1, Lisp_Object org_l2,
                          Lisp_Object pred);

extern void run_hook_with_args (Lisp_Object hook_var, int nargs, ...);

extern Lisp_Object Qstring_lessp, Qidentity, Qyes_or_no_p;
extern Lisp_Object Vfeatures;
extern Lisp_Object Fidentity (Lisp_Object x);
extern Lisp_Object Frandom (Lisp_Object arg);
extern Lisp_Object Flength (Lisp_Object seq);
extern Lisp_Object Fstring_equal (Lisp_Object s1, Lisp_Object s2);
extern Lisp_Object Fstring_lessp (Lisp_Object s1, Lisp_Object s2);
extern Lisp_Object string_getprop (struct Lisp_String *s,
				   Lisp_Object property,
				   Lisp_Object defalt);
extern void string_putprop (struct Lisp_String *s, Lisp_Object property,
			    Lisp_Object value);
void bump_string_modiff (Lisp_Object str);
extern Lisp_Object Fappend (int nargs, Lisp_Object *args);
extern Lisp_Object Fconcat (int nargs, Lisp_Object *args);
extern Lisp_Object Fvconcat (int nargs, Lisp_Object *args);
extern Lisp_Object Fcopy_sequence (Lisp_Object seq);
extern Lisp_Object Fsubstring (Lisp_Object str, Lisp_Object s, Lisp_Object e);
extern Bytecount get_string_range (Lisp_Object string,
				       Lisp_Object from, Lisp_Object to,
				       Bytecount *from_out,
				       Bytecount *to_out);
extern Bytecount get_string_bytepos (Lisp_Object string, Lisp_Object pos);
extern Lisp_Object Fnthcdr (Lisp_Object n, Lisp_Object list);
extern Lisp_Object Fnth (Lisp_Object n, Lisp_Object list);
extern Lisp_Object Fmember (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object Fmemq (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object memq_no_quit (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object Fassoc (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object assoc_no_quit (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Fassq (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object assq_no_quit (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Frassoc (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Frassq (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object rassq_no_quit (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Fdelete (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object Fdelq (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object delq_no_quit (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object Fremassoc (Lisp_Object elt, Lisp_Object list);
extern Lisp_Object remassoc_no_quit (Lisp_Object key, Lisp_Object list);
extern Lisp_Object Fremassq (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object remassq_no_quit (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Fremrassoc (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Fremrassq (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object remrassq_no_quit (Lisp_Object key, Lisp_Object alist);
extern Lisp_Object Freverse (Lisp_Object list), Fnreverse (Lisp_Object list);
extern Lisp_Object Fget (Lisp_Object sym, Lisp_Object prop, Lisp_Object def);
extern Lisp_Object Fput (Lisp_Object sym, Lisp_Object prop, Lisp_Object val);

extern void pure_put (Lisp_Object sym, Lisp_Object prop, Lisp_Object val);
extern Lisp_Object Fremprop (Lisp_Object sym, Lisp_Object prop);
extern int plists_differ (Lisp_Object a, Lisp_Object b, int depth);
extern int internal_getf (Lisp_Object plist, Lisp_Object property,
			  Lisp_Object *value_out);
extern void internal_putf (Lisp_Object *plist, Lisp_Object property,
			   Lisp_Object value);
extern int internal_remprop (Lisp_Object *plist, Lisp_Object property);
extern Lisp_Object Fequal (Lisp_Object one, Lisp_Object two);
extern int internal_equal (Lisp_Object, Lisp_Object, int depth);
extern Lisp_Object Ffillarray (Lisp_Object array, Lisp_Object init);
extern Lisp_Object Fnconc (int nargs, Lisp_Object *args);
extern Lisp_Object Fmapcar (Lisp_Object fn, Lisp_Object seq);
extern Lisp_Object Ffeaturep (Lisp_Object name);
extern Lisp_Object Frequire (Lisp_Object name, Lisp_Object filename);
extern Lisp_Object Fprovide (Lisp_Object name);
extern Lisp_Object concat2 (Lisp_Object s1, Lisp_Object s2);
extern Lisp_Object vconcat2 (Lisp_Object s1, Lisp_Object s2);
extern Lisp_Object nconc2 (Lisp_Object l1, Lisp_Object l2);
extern Lisp_Object Fcopy_alist (Lisp_Object alist);
extern Lisp_Object Fcopy_tree (Lisp_Object arg, Lisp_Object vecp);


/* Defined in font-lock.c */
extern Lisp_Object Qcomment, Qblock_comment;
extern Lisp_Object Qbeginning_of_defun, Qend_of_defun;


/* Defined in frame.c */
extern Lisp_Object Fframep (Lisp_Object obj);
extern Lisp_Object Fframe_live_p (Lisp_Object obj);
extern Lisp_Object Fselect_frame (Lisp_Object scr);
extern Lisp_Object Fselected_frame (Lisp_Object device);
extern Lisp_Object Fmake_frame (Lisp_Object parameters, Lisp_Object device);
extern Lisp_Object Fwindow_frame (Lisp_Object window);
extern Lisp_Object Fframe_root_window (Lisp_Object frame);
extern Lisp_Object Fframe_selected_window (Lisp_Object frame);
extern Lisp_Object Fframe_lowest_window (Lisp_Object frame,
					 Lisp_Object offset);
extern Lisp_Object Fframe_highest_window (Lisp_Object frame,
					  Lisp_Object offset);
extern Lisp_Object Fframe_list (Lisp_Object device);
extern Lisp_Object Fframe_name (Lisp_Object frame);
extern Lisp_Object Fnext_frame (Lisp_Object frame, 
                                 Lisp_Object miniframe);
extern Lisp_Object Fmouse_position (Lisp_Object device);
extern Lisp_Object Fmouse_pixel_position (Lisp_Object device);
extern Lisp_Object Fset_mouse_position (Lisp_Object window,
                                        Lisp_Object x, Lisp_Object y);
extern Lisp_Object Fset_mouse_pixel_position (Lisp_Object window,
					      Lisp_Object x, Lisp_Object y);
extern Lisp_Object Fmake_frame_visible (Lisp_Object frame);
extern Lisp_Object Fmake_frame_invisible (Lisp_Object frame,
					  Lisp_Object ignored);
extern Lisp_Object Ficonify_frame (Lisp_Object frame);
extern Lisp_Object Fdeiconify_frame (Lisp_Object frame);
extern Lisp_Object Fframe_visible_p (Lisp_Object frame);
extern Lisp_Object Fframe_iconified_p (Lisp_Object frame);
extern Lisp_Object Fvisible_frame_list (Lisp_Object device);
extern Lisp_Object Fframe_parameters (Lisp_Object frame);
extern Lisp_Object Fmodify_frame_parameters (Lisp_Object frame,
                                              Lisp_Object alist);
extern Lisp_Object Fframe_height (Lisp_Object frame);
extern Lisp_Object Fframe_width (Lisp_Object frame);
extern Lisp_Object Fset_frame_height (Lisp_Object frame,
                                       Lisp_Object rows, Lisp_Object pretend);
extern Lisp_Object Fset_frame_width  (Lisp_Object frame,
                                       Lisp_Object cols, Lisp_Object pretend);
extern Lisp_Object Fset_frame_size (Lisp_Object frame, 
                                     Lisp_Object cols, Lisp_Object rows, 
                                     Lisp_Object pretend);
extern Lisp_Object Fset_frame_position (Lisp_Object frame,
                                         Lisp_Object xoffset, 
                                         Lisp_Object yoffset);
extern Lisp_Object Vdefault_minibuffer_frame;

extern Lisp_Object Vcreate_frame_hook, Qcreate_frame_hook;
extern Lisp_Object Vmouse_enter_frame_hook, Qmouse_enter_frame_hook;
extern Lisp_Object Vmouse_leave_frame_hook, Qmouse_leave_frame_hook;
extern Lisp_Object Vmap_frame_hook, Qmap_frame_hook;
extern Lisp_Object Vunmap_frame_hook, Qunmap_frame_hook;
extern Lisp_Object Vmouse_motion_handler;
extern Lisp_Object Vsynchronize_minibuffers;

extern Lisp_Object Qframep, Qframe_live_p, Qdelete_frame;
extern Lisp_Object Qselect_frame_hook, Qdeselect_frame_hook;


/* Defined in frame-x.c */
extern Lisp_Object Fx_set_scrollbar_pointer (Lisp_Object frame,
					     Lisp_Object cursor);
extern Lisp_Object Qpointer;


/* Defined in general.c */
extern Lisp_Object Qall;
extern Lisp_Object Qboolean;
extern Lisp_Object Qbuffer;
extern Lisp_Object Qbytes;
extern Lisp_Object Qcategory;
extern Lisp_Object Qcase;
extern Lisp_Object Qchars;
extern Lisp_Object Qcolor;
extern Lisp_Object Qcommand;
extern Lisp_Object Qcritical;
extern Lisp_Object Qdefault;
extern Lisp_Object Qdevice;
extern Lisp_Object Qdisplay;
extern Lisp_Object Qdoc_string;
extern Lisp_Object Qeq;
extern Lisp_Object Qfont;
extern Lisp_Object Qframe;
extern Lisp_Object Qgeometry;
extern Lisp_Object Qglobal;
extern Lisp_Object Qhighlight;
extern Lisp_Object Qinteger;
extern Lisp_Object Qkeyboard;
extern Lisp_Object Qmax;
extern Lisp_Object Qmemory;
extern Lisp_Object Qmessage;
extern Lisp_Object Qminus;
extern Lisp_Object Qname;
extern Lisp_Object Qnotice;
extern Lisp_Object Qpath;
extern Lisp_Object Qprint;
extern Lisp_Object Qresource;
extern Lisp_Object Qreverse;
extern Lisp_Object Qstring;
extern Lisp_Object Qsymbol;
extern Lisp_Object Qsyntax;
extern Lisp_Object Qtext;
extern Lisp_Object Qunimplemented;
extern Lisp_Object Qvector;
extern Lisp_Object Qwarning;
extern Lisp_Object Qwindow;


/* Defined in getloadavg.c */
extern int getloadavg (double loadavg[], int nelem);


/* Defined in glyphs.c */
extern Lisp_Object Fmake_glyph_internal (Lisp_Object type);
extern Lisp_Object Fmake_image_instance (Lisp_Object data,
					  Lisp_Object device,
					  Lisp_Object force_mono,
					  Lisp_Object no_error);
extern Lisp_Object Fimage_instance_p (Lisp_Object obj);
extern Lisp_Object Fimage_instance_type (Lisp_Object image_instance);
extern Lisp_Object Fglyph_type (Lisp_Object glyph);
extern Lisp_Object Qnothing, Qmono_pixmap, Qcolor_pixmap;
extern Lisp_Object Qcursor, Qsubwindow;
extern Lisp_Object Qformatted_string;
extern Lisp_Object Qicon;


/* Defined in glyphs-x.c */
extern Lisp_Object Fcolorize_image_instance (Lisp_Object, Lisp_Object,
					      Lisp_Object);
extern Lisp_Object Fmake_cursor (Lisp_Object name, Lisp_Object fg,
				 Lisp_Object bg, Lisp_Object device);
extern Lisp_Object Fcursorp (Lisp_Object obj);
extern Lisp_Object Fcursor_name (Lisp_Object obj);
extern Lisp_Object Fcursor_foreground (Lisp_Object obj);
extern Lisp_Object Fcursor_background (Lisp_Object obj);


/* Defined in indent.c */
extern Lisp_Object Fvertical_motion (Lisp_Object lines, Lisp_Object window);
extern Lisp_Object Findent_to (Lisp_Object col, Lisp_Object mincol,
			       Lisp_Object buffer);
extern Lisp_Object Fcurrent_column (Lisp_Object buffer); 
extern int spaces_at_point (struct buffer *b, Bufpos pos);
extern int column_at_point (struct buffer *buf, Bufpos pos, int cur_col);
extern int current_column (struct buffer *buf);
extern void invalidate_current_column (void);
extern Bufpos vmotion (struct window *w, Bufpos orig, int vtarget,
		       int *ret_vpos);


/* Defined in intl.c */
extern Lisp_Object Fignore_defer_gettext (Lisp_Object obj);
extern Lisp_Object Fgettext (Lisp_Object string);
extern Lisp_Object Fdgettext (Lisp_Object domain, Lisp_Object string);
extern Lisp_Object Fdomain (Lisp_Object);
extern Lisp_Object Qdefer_gettext;
#ifdef I18N3
extern Lisp_Object Vfile_domain;
#endif


/* Defined in keyboard.c */
extern void init_interrupts_late (void);
extern Lisp_Object Fdiscard_input (void), Frecursive_edit (void);
extern Lisp_Object Finput_pending_p (void);
extern Lisp_Object Qdisabled, Qtop_level;
extern Lisp_Object Vdisabled_command_hook;
extern void stuff_buffered_input (Lisp_Object stuffstring);
extern Lisp_Object Fset_input_mode (Lisp_Object interrupt,
                                    Lisp_Object flow, 
                                    Lisp_Object meta,
				    Lisp_Object quit,
				    Lisp_Object device);
extern int command_loop_level;
extern Lisp_Object Qreally_early_error_handler;


/* Defined in keymap.c */
extern Lisp_Object Fcurrent_local_map (Lisp_Object buffer);
extern Lisp_Object Fkeymapp (Lisp_Object);
extern Lisp_Object Fmake_sparse_keymap (void);
extern Lisp_Object Fkeymap_fullness (Lisp_Object keymap);
extern Lisp_Object Fkey_description (Lisp_Object key);
extern Lisp_Object Fsingle_key_description (Lisp_Object key);
extern Lisp_Object Ftext_char_description (Lisp_Object c);
extern Lisp_Object Qmodeline_map;
extern Lisp_Object Vsingle_space_string;
extern Lisp_Object Qcontrol, Qctrl, Qmeta, Qsuper, Qhyper, Qalt, Qshift;
extern Lisp_Object Qkeymap, Qkeymapp;
extern void where_is_to_char (Lisp_Object definition, char *buffer);


/* Defined in lread.c */
extern Lisp_Object Qvariable_documentation, Qstandard_input, Qread_char;
extern Lisp_Object Qvariable_domain; /* I18N3 */
extern Lisp_Object Qload;
extern Lisp_Object Vstandard_input;
extern Lisp_Object Vvalues;
extern Lisp_Object Fread (Lisp_Object readcharfun);
extern Lisp_Object Fread_from_string (Lisp_Object string, 
                                      Lisp_Object start, Lisp_Object end);
extern Lisp_Object Fload_internal (Lisp_Object filename,
				   Lisp_Object missing_ok,
				   Lisp_Object nomessage,
				   Lisp_Object nosuffix);
extern void close_load_descs (void);
 extern int locate_file (Lisp_Object path, 
                        Lisp_Object str, CONST char *suffix, 
                        Lisp_Object *storeptr, int mode);
extern Lisp_Object Flocate_file_clear_hashing (Lisp_Object path);
extern int isfloat_string (CONST char *);

#ifdef LOADHIST /* this is just a stupid idea */
#define LOADHIST_ATTACH(x) \
 do { if (initialized) Vcurrent_load_list = Fcons (x, Vcurrent_load_list); } \
 while (0)
extern Lisp_Object Vcurrent_load_list;
extern Lisp_Object Vload_history;
#else /*! LOADHIST */
# define LOADHIST_ATTACH(x)
#endif /*! LOADHIST */


/* Defined in macros.c */
extern Lisp_Object Fexecute_kbd_macro (Lisp_Object macro, 
                                       Lisp_Object prefixarg);


/* Defined in marker.c */
extern Bufpos marker_position (Lisp_Object marker);
extern void set_marker_position (Lisp_Object marker, Bufpos pos);
extern void unchain_marker (Lisp_Object marker);
extern Lisp_Object Fset_marker (Lisp_Object marker, 
                                Lisp_Object pos, Lisp_Object buffer);
extern Lisp_Object Fmarker_position (Lisp_Object m);
extern Lisp_Object Fmarker_buffer (Lisp_Object m);
extern Lisp_Object Fcopy_marker (Lisp_Object m);
extern Lisp_Object set_marker_restricted (Lisp_Object marker,
                                          Lisp_Object pos, Lisp_Object buf);


/* Defined in menubar.c */
int initialize_frame_menubar (struct frame *f);
extern Lisp_Object Fpopup_dialog_box (Lisp_Object dbox_desc);
extern Lisp_Object Fpopup_menu (Lisp_Object menu_desc, Lisp_Object event);
extern void update_frame_menubars (struct frame *f);
extern Lisp_Object Qcurrent_menubar;
extern Lisp_Object Qactivate_menubar_hook;
extern Lisp_Object Vactivate_menubar_hook;
extern int popup_menu_up_p;
extern Lisp_Object Qmouse_event_p; /* events.c */
extern Lisp_Object Q_active, Q_suffix, Q_keys, Q_style, Q_selected;
extern Lisp_Object Q_filter, Q_config, Q_included;
extern Lisp_Object Qtoggle, Qradio, Qbutton;


/* Defined in minibuf.c */
extern int minibuf_level;
extern Charcount scmp_1 (CONST Bufbyte *s1, CONST Bufbyte *s2, Charcount len,
			 int ignore_case);
#define scmp(s1, s2, len) scmp_1 (s1, s2, len, completion_ignore_case)
extern Lisp_Object Fread_from_minibuffer (Lisp_Object prompt, 
                                          Lisp_Object init,
                                          Lisp_Object keymap,
                                          Lisp_Object read_crock,
                                          Lisp_Object hist);
extern int completion_ignore_case;
extern Lisp_Object Qcompletion_ignore_case;
extern Lisp_Object Vcompletion_regexp_list;
extern int regexp_ignore_completion_p (CONST Bufbyte *nonreloc,
				       Lisp_Object reloc, Bytecount offset,
				       Bytecount length);

extern Lisp_Object Vminibuffer_zero;

extern Lisp_Object Vecho_area_buffer;
extern Lisp_Object clear_echo_area (struct frame *f, Lisp_Object label, 
				    int no_restore);
extern Lisp_Object clear_echo_area_from_print (struct frame *f, 
					       Lisp_Object label, 
					       int no_restore);
extern void echo_area_append (struct frame *f, CONST Bufbyte *nonreloc,
			      Lisp_Object reloc, Bytecount offset,
			      Bytecount length, Lisp_Object type);
extern void echo_area_message (struct frame *f, CONST Bufbyte *nonreloc,
			       Lisp_Object reloc, Bytecount offset,
			       Bytecount length, Lisp_Object type);
extern Lisp_Object echo_area_status (struct frame *f);
extern int echo_area_active (struct frame *f);
extern Lisp_Object echo_area_contents (struct frame *f);
void message_internal (CONST Bufbyte *nonreloc, Lisp_Object reloc,
		       Bytecount offset, Bytecount length);
extern void message (CONST char *fmt, ...);              /* ... printf args */
extern void message_no_translate (CONST char *fmt, ...); /* ... printf args */


/* Defined in mocklisp.c */
extern Lisp_Object Vmocklisp_arguments, Qmocklisp, Qmocklisp_arguments;
extern Lisp_Object ml_apply (Lisp_Object function, Lisp_Object args);


/* Defined in mule-*.c */
extern Lisp_Object Fread_coding_system (Lisp_Object prompt);
extern Lisp_Object Fread_non_nil_coding_system (Lisp_Object prompt);


/* Defined in print.c */
extern void write_string_to_stdio_stream (FILE *stream, CONST Bufbyte *str,
					  Bytecount offset, Bytecount len);
extern Lisp_Object Vprin1_to_string_buffer;
extern Lisp_Object Fprin1 (Lisp_Object obj, Lisp_Object printcharfun);
extern Lisp_Object Fprinc (Lisp_Object obj, Lisp_Object printcharfun);
extern Lisp_Object Fprint (Lisp_Object obj, Lisp_Object printcharfun);
extern Lisp_Object Fprin1_to_string (Lisp_Object obj, Lisp_Object noescape);
extern Lisp_Object Fterpri (Lisp_Object printcharfun);
extern Lisp_Object Vstandard_output, Qstandard_output;
extern Lisp_Object Qexternal_debugging_output;
extern void temp_output_buffer_setup (CONST char *bufname);
extern void temp_output_buffer_show (Lisp_Object buf, Lisp_Object same_scrn);
/* NOTE: Do not call this with the data of a Lisp_String.  Use princ.
 * Note: stream should be defaulted before calling
 *  (eg Qnil means stdout, not Vstandard_output, etc) */
extern void write_c_string (CONST char *s, Lisp_Object printcharfun);
/* Same goes for this function. */
extern void write_string_1 (CONST Bufbyte *s, Bytecount size, 
                            Lisp_Object printcharfun);
extern void print_internal (Lisp_Object obj, 
                            Lisp_Object printcharfun, 
                            int escapeflag);
extern Lisp_Object Vprint_level;
extern Lisp_Object Vprint_length;
extern int print_escape_newlines;
extern int print_readably;
extern Lisp_Object Qprint_escape_newlines;
extern Lisp_Object internal_with_output_to_temp_buffer
  (CONST char *bufname, 
   Lisp_Object (*function) (Lisp_Object args),
   Lisp_Object args,
   Lisp_Object same_frame);
extern void float_to_string (char *buf, double data);
extern void print_symbol (Lisp_Object, Lisp_Object stream, int escapeflag);
extern void print_bytecode (Lisp_Object, Lisp_Object stream, int escapeflag);
extern void print_float (Lisp_Object, Lisp_Object stream, int escapeflag);
extern Lisp_Object Qprint_length, Qprint_string_length;


/* Defined in process.c */
extern Lisp_Object Fprocess_send_eof (Lisp_Object process);


/* Defined in redisplay.c */
extern Lisp_Object Fredraw_display (Lisp_Object device);
extern Lisp_Object Fredraw_frame (Lisp_Object frame);
extern Lisp_Object Fmessage_displayed_p (Lisp_Object return_string);
extern Lisp_Object Voverlay_arrow_position, Voverlay_arrow_string;
extern Lisp_Object Vbar_cursor;
extern Lisp_Object Vwindow_system;
extern Lisp_Object Vwindow_system_version;
extern Lisp_Object Qcursor_in_echo_area;
/* Nonzero means reading single-character input with prompt
   so put cursor on minibuffer after the prompt.  */
extern int cursor_in_echo_area;


/* Defined in search.c */
struct re_pattern_buffer;
struct re_registers;
extern Lisp_Object Fstring_match (Lisp_Object regexp,
                                  Lisp_Object string, Lisp_Object start);
extern Lisp_Object Fmatch_beginning (Lisp_Object n);
extern Lisp_Object Fmatch_end (Lisp_Object n);
extern Lisp_Object Fskip_chars_forward (Lisp_Object string, Lisp_Object lim,
					Lisp_Object buffer);
extern Lisp_Object Fskip_chars_backward (Lisp_Object string, Lisp_Object lim,
					 Lisp_Object buffer);
extern Bufpos scan_buffer (struct buffer *buf, Emchar target, Bufpos start,
			   Bufpos Limit, int cnt, int *shortage,
			   int allow_quit);
extern Bufpos find_next_newline (struct buffer *buf, Bufpos from, int cnt);
extern Bufpos find_next_newline_no_quit (struct buffer *buf, Bufpos from,
					 int cnt);
extern int compile_pattern (Lisp_Object pattern, 
			    struct re_pattern_buffer *bufp, 
			    struct re_registers *regp,
			    char *translate, int backward,
			    int no_error);
Bytecount fast_string_match (Lisp_Object regexp,  CONST Bufbyte *nonreloc,
			     Lisp_Object reloc, Bytecount offset,
			     Bytecount length, int no_error, int no_quit);
extern Lisp_Object Fre_search_forward (Lisp_Object string, Lisp_Object bound,
                                       Lisp_Object no_error, Lisp_Object count);


/* Defined in sound.c */
extern Lisp_Object Fplay_sound (Lisp_Object sound, Lisp_Object volume,
				Lisp_Object device);
extern Lisp_Object Fding (Lisp_Object arg, Lisp_Object sound,
			  Lisp_Object device);
extern void init_device_sound (struct device *d);
extern Lisp_Object Qnas;


/* Defined in specifier.c */
extern Lisp_Object Fspecifier_instance (Lisp_Object specifier,
					Lisp_Object locale,
					Lisp_Object defalt,
					Lisp_Object no_fallback);
extern Lisp_Object Fspecifier_specs (Lisp_Object specifier,
				     Lisp_Object locale,
				     Lisp_Object tag_set,
				     Lisp_Object exact_p);
extern Lisp_Object Fspecifier_spec_list (Lisp_Object specifier,
					 Lisp_Object locale,
					 Lisp_Object tag_set,
					 Lisp_Object exact_p);
extern Lisp_Object Fmake_specifier (Lisp_Object type);
extern Lisp_Object Fadd_spec_to_specifier (Lisp_Object specifier,
					   Lisp_Object locale,
					   Lisp_Object instantiator,
					   Lisp_Object tag,
					   Lisp_Object how_to_add);
extern Lisp_Object Fvalid_specifier_locale_p (Lisp_Object locale);
extern Lisp_Object Fcopy_specifier (Lisp_Object specifier,
				    Lisp_Object dest,
				    Lisp_Object locale,
				    Lisp_Object tag_set,
				    Lisp_Object exact_p,
				    Lisp_Object how_to_add);
extern Lisp_Object specifier_instance (Lisp_Object specifier,
				       Lisp_Object domain,
				       int no_error_or_quit,
				       int no_fallback);
extern Lisp_Object specifier_instance_no_quit (Lisp_Object specifier,
					       Lisp_Object domain,
					       int no_fallback);
extern Lisp_Object Qfallback;
extern Lisp_Object Qgeneric, Qnatnum;


/* Defined in symbols.c */
extern Lisp_Object Vobarray;
extern Lisp_Object Fintern (Lisp_Object str, Lisp_Object obarray);
extern Lisp_Object Fintern_soft (Lisp_Object str, Lisp_Object obarray);
extern int hash_string (CONST Bufbyte *, Bytecount len);
extern Lisp_Object intern (CONST char *);
extern Lisp_Object oblookup (Lisp_Object obarray,
			     CONST Bufbyte *, Bytecount len);
extern void map_obarray (Lisp_Object obarray, 
                         void (*fn) (Lisp_Object sym, Lisp_Object arg),
                         Lisp_Object arg);
extern Lisp_Object Fboundp (Lisp_Object sym);
extern Lisp_Object Ffboundp (Lisp_Object);
extern Lisp_Object Ffset (Lisp_Object sym, Lisp_Object val);
extern Lisp_Object Fsymbol_plist (Lisp_Object sym);
extern Lisp_Object Fsetplist (Lisp_Object sym, Lisp_Object val);
extern Lisp_Object Fsymbol_function (Lisp_Object sym);
extern Lisp_Object Fsymbol_value (Lisp_Object sym);
extern Lisp_Object Fdefault_value (Lisp_Object sym);
extern Lisp_Object Fdefault_boundp (Lisp_Object sym);
extern Lisp_Object Fset (Lisp_Object sym, Lisp_Object val);
extern Lisp_Object Fset_default (Lisp_Object sym, Lisp_Object val);
extern Lisp_Object Fsymbol_name (Lisp_Object sym);
extern Lisp_Object Findirect_function (Lisp_Object object);
extern Lisp_Object indirect_function (Lisp_Object object, int errorp);
extern Lisp_Object symbol_value_in_buffer (Lisp_Object sym, Lisp_Object buf);
extern void kill_buffer_local_variables (struct buffer *buf);
extern Lisp_Object Fmake_local_variable (Lisp_Object object);
extern int symbol_value_buffer_local_info (Lisp_Object symbol,
                                           struct buffer *);
extern Lisp_Object find_symbol_value (Lisp_Object symbol);
extern Lisp_Object top_level_value (Lisp_Object symbol);
extern Lisp_Object Fkill_local_variable (Lisp_Object symbol);
extern Lisp_Object Fmake_variable_buffer_local (Lisp_Object variable);

/* Defined in syntax.c */
extern int scan_words (struct buffer *buf, int from, int count);
extern Lisp_Object Fforward_word (Lisp_Object n, Lisp_Object buffer);


/* Defined in sysdep.c, also declared in sysdep.h.
   (These may be called via the open, etc macros instead...)
 */
#ifdef ENCAPSULATE_OPEN
extern int sys_open (CONST char *path, int oflag, ...);
#endif
#ifdef ENCAPSULATE_CLOSE
extern int sys_close (int fd);
#endif
#ifdef ENCAPSULATE_READ
extern int sys_read (int fildes, void *buf, unsigned int nbyte);
#endif
#ifdef ENCAPSULATE_WRITE
extern int sys_write (int fildes, CONST void *buf, unsigned int nbyte);
#endif

#ifdef ENCAPSULATE_FOPEN
extern FILE *sys_fopen (CONST char *path, CONST char *type);
#endif
#ifdef ENCAPSULATE_FCLOSE
extern int sys_fclose (FILE *stream);
#endif
#ifdef ENCAPSULATE_FREAD
extern size_t sys_fread (void *ptr, size_t size, size_t nitem, FILE *stream);
#endif
#ifdef ENCAPSULATE_FWRITE
extern size_t sys_fwrite (CONST void *ptr, size_t size, size_t nitem,
			  FILE *stream);
#endif

unsigned int sys_getuid (void);

extern char *egetenv (CONST char *);
/* extern char *getenv (CONST char *); */


/* Defined in undo.c */
extern Lisp_Object Fundo_boundary (void);
extern Lisp_Object truncate_undo_list (Lisp_Object list, int min, int max);
extern void record_extent (Lisp_Object extent, int attached);
extern void record_insert (struct buffer *b, Bufpos beg, Charcount length);
extern void record_delete (struct buffer *b, Bufpos beg, Charcount length);
extern void record_change (struct buffer *b, Bufpos beg, Charcount length);


/* Defined in unex*.c */
extern int unexec (char *new_name, char *a_name,
                   unsigned int data_start, 
                   unsigned int bss_start, 
                   unsigned int entry_address);
#ifdef RUN_TIME_REMAP
extern int run_time_remap (char *);
#endif


/* Defined in vm-limit.c */
extern void memory_warnings (void *start, void (*warnfun) (CONST char *));


/* Defined in window.c */
extern Lisp_Object Qvisible;
extern Lisp_Object Qscroll_up, Qscroll_down;
extern Lisp_Object Fselected_window (Lisp_Object device);
extern Lisp_Object Fwindow_buffer (Lisp_Object window);
extern Lisp_Object Fwindow_lowest_p (Lisp_Object window);
extern Lisp_Object Fwindow_highest_p (Lisp_Object window);
extern Lisp_Object Fget_buffer_window (Lisp_Object buffer, 
                                       Lisp_Object frame,
                                       Lisp_Object ignored);
extern Lisp_Object Fsave_window_excursion (Lisp_Object body);
extern Lisp_Object Fset_window_configuration (Lisp_Object config);
extern Lisp_Object Fcurrent_window_configuration (Lisp_Object frame);
extern Lisp_Object Fdisplay_buffer (Lisp_Object buffer, 
                                    Lisp_Object notthiswindow, 
                                    Lisp_Object overrideframe);
extern Lisp_Object Freplace_buffer_in_windows (Lisp_Object buffer);
extern Lisp_Object Fwindow_dedicated_p (Lisp_Object window);
extern Lisp_Object Fnext_window (Lisp_Object window, 
                                 Lisp_Object minibuf, 
                                 Lisp_Object all_frames,
                                 Lisp_Object ignored);
extern Lisp_Object Fdelete_window (Lisp_Object window);
extern Lisp_Object Fselect_window (Lisp_Object window);
extern Lisp_Object Fset_window_buffer (Lisp_Object window, 
                                       Lisp_Object buffer);
extern Lisp_Object Fsplit_window (Lisp_Object window, 
                                  Lisp_Object chsize, 
                                  Lisp_Object horflag);
extern Lisp_Object Frecenter (Lisp_Object arg, Lisp_Object window);
extern Lisp_Object Fmove_to_window_line (Lisp_Object arg, Lisp_Object window);
extern Lisp_Object Fbuffer_left_margin_pixwidth (Lisp_Object buffer);
extern Lisp_Object Fbuffer_right_margin_pixwidth (Lisp_Object buffer);
extern Lisp_Object Fset_window_hscroll (Lisp_Object window, Lisp_Object ncol);
extern Lisp_Object Fwindow_point (Lisp_Object window);
extern Lisp_Object Fset_window_point (Lisp_Object window, Lisp_Object pos);
extern Lisp_Object Fset_window_start (Lisp_Object window, Lisp_Object pos,
				      Lisp_Object noforce);
extern Lisp_Object Fwindow_start (Lisp_Object window);
extern Lisp_Object Fwindow_end (Lisp_Object window, Lisp_Object guarantee);

#endif /* _EMACSFNS_H_ */

/* This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */


#ifndef _XEMACS_ELHASH_H_
#define _XEMACS_ELHASH_H_

enum hashtable_type
{
  HASHTABLE_NONWEAK,
  HASHTABLE_KEY_WEAK,
  HASHTABLE_VALUE_WEAK,
  HASHTABLE_WEAK
};

extern Lisp_Object Fmake_hashtable (Lisp_Object size);
extern Lisp_Object Fcopy_hashtable (Lisp_Object old_table);
extern Lisp_Object Fgethash (Lisp_Object obj, Lisp_Object table, 
                             Lisp_Object defalt);
extern Lisp_Object Fputhash (Lisp_Object obj, Lisp_Object val, 
                             Lisp_Object table);
extern Lisp_Object Fremhash (Lisp_Object obj, Lisp_Object table);
extern Lisp_Object Fhashtable_fullness (Lisp_Object table);

extern Lisp_Object make_lisp_hashtable (int size,
	 int (*test_function) (CONST void*, CONST void*),
	 unsigned long (*hash_function) (CONST void*),
	 enum hashtable_type type);

extern void elisp_maphash (void (*fn) (CONST void *key, void *contents,
				       void *extra_arg),
                           Lisp_Object table, 
                           void *extra_arg);

extern void elisp_map_remhash (int (*fn) (CONST void *key,
					  CONST void *contents,
					  void *extra_arg),
                           Lisp_Object table, 
                           void *extra_arg);

extern int finish_marking_weak_hashtables (int (*obj_marked_p) (Lisp_Object),
					   void (*markobj) (Lisp_Object));
extern void prune_weak_hashtables (int (*obj_marked_p) (Lisp_Object));

extern char *elisp_hvector_malloc (unsigned int, Lisp_Object);
extern void elisp_hvector_free (void *ptr, Lisp_Object table);

extern int lisp_string_equal (CONST void *x1, CONST void *x2);
extern unsigned long lisp_string_hash (CONST void *x);

#endif /* _XEMACS_ELHASH_H_ */

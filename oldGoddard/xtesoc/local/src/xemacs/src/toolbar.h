/* Define toolbar instance.
   Copyright (C) 1995 Board of Trustees, University of Illinois

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_TOOLBAR_H_
#define _XEMACS_TOOLBAR_H_

#include "specifier.h"

enum toolbar_pos
{
  TOP_TOOLBAR,
  BOTTOM_TOOLBAR,
  LEFT_TOOLBAR,
  RIGHT_TOOLBAR
};

/* There are 4 of these per frame.  They don't really need to be an
   lrecord (they're not lisp-accessible) but it makes marking slightly
   more modular.  */
struct toolbar_data
{
  struct lcrecord_header header;

  /* The last buffer for which the toolbars were displayed. */
  Lisp_Object last_toolbar_buffer;

  /* The actual buttons are chained through this. */
  Lisp_Object toolbar_buttons;
};

DECLARE_LRECORD (toolbar_data, struct toolbar_data);
#define XTOOLBAR_DATA(x) XRECORD (x, toolbar_data, struct toolbar_data)
#define XSETTOOLBAR_DATA(x, p) XSETRECORD (x, p, toolbar_data)
#define TOOLBAR_DATAP(x) RECORDP (x, toolbar_data)
#define CHECK_TOOLBAR_DATA(x, i) CHECK_RECORD (x, toolbar_data)

#define FRAME_TOOLBAR_DATA(frame, position)				\
  (XTOOLBAR_DATA ((frame)->toolbar_data[position]))
#define FRAME_TOOLBAR_BUFFER(frame, position)	\
  (XTOOLBAR_DATA ((frame)->toolbar_data[position])->last_toolbar_buffer)

/* These are chained together through toolbar_buttons in struct
   toolbar_data.  These don't need to be an lrecord either, but again,
   it makes marking easier. */
struct toolbar_button
{
  struct lcrecord_header header;

  Lisp_Object next;
  Lisp_Object frame;

  Lisp_Object up_glyph;
  Lisp_Object down_glyph;
  Lisp_Object disabled_glyph;

  Lisp_Object cap_up_glyph;
  Lisp_Object cap_down_glyph;
  Lisp_Object cap_disabled_glyph;

  Lisp_Object callback;
  Lisp_Object enabled_p;
  Lisp_Object help_string;

  char enabled;
  char down;
  char pushright;
  char blank;

  int x, y;
  int width, height;
  int dirty;
};

DECLARE_LRECORD (toolbar_button, struct toolbar_button);
#define XTOOLBAR_BUTTON(x) XRECORD (x, toolbar_button, struct toolbar_button)
#define XSETTOOLBAR_BUTTON(x, p) XSETRECORD (x, p, toolbar_button)
#define TOOLBAR_BUTTONP(x) RECORDP (x, toolbar_button)
#define CHECK_TOOLBAR_BUTTON(x, i) CHECK_RECORD (x, toolbar_button)

extern void get_toolbar_coords (struct frame *f, enum toolbar_pos pos, int *x,
				int *y, int *width, int *height, int *vert,
				int for_layout);
extern Lisp_Object toolbar_button_at_pixpos (struct frame *f, int x_coord,
					     int y_coord);
DECLARE_SPECIFIER_TYPE (toolbar);
extern Lisp_Object Qtoolbar;
#define XTOOLBAR_SPECIFIER(x) XSPECIFIER_TYPE (x, toolbar)
#define XSETTOOLBAR_SPECIFIER(x, p) XSETSPECIFIER_TYPE (x, p, toolbar)
#define TOOLBAR_SPECIFIERP(x) SPECIFIER_TYPEP (x, toolbar)
#define CHECK_TOOLBAR_SPECIFIER(x, i) CHECK_SPECIFIER_TYPE (x, i, toolbar)

#define DEFAULT_TOP_TOOLBAR_HEIGHT	37
#define DEFAULT_BOTTOM_TOOLBAR_HEIGHT	0
#define DEFAULT_LEFT_TOOLBAR_WIDTH	0
#define DEFAULT_RIGHT_TOOLBAR_WIDTH	0

#define DEFAULT_TOOLBAR_BLANK_SIZE	8
#define MINIMUM_SHADOW_THICKNESS	2

extern Lisp_Object Vtop_toolbar_height, Vbottom_toolbar_height;
extern Lisp_Object Vleft_toolbar_width, Vright_toolbar_width;

extern void update_frame_toolbars (struct frame *f);
extern void init_frame_toolbars (struct frame *f);
extern void init_device_toolbars (struct device *d);
extern void init_global_toolbars (struct device *d);
extern void free_frame_toolbars (struct frame *f);


#ifdef HAVE_X_WINDOWS

extern void x_redraw_exposed_toolbars (struct frame *f, int x, int y,
				       int width, int height);

#endif /* HAVE_X_WINDOWS */
#endif /* _XEMACS_TOOLBAR_H_ */

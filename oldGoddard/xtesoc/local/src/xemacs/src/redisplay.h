/* Redisplay data structures.
   Copyright (C) 1994, 1995 Board of Trustees, University of Illinois

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifndef _XEMACS_REDISPLAY_H_
#define _XEMACS_REDISPLAY_H_

/* Redisplay DASSERT types */
#define DB_DISP_POS			1
#define DB_DISP_TEXT_LAYOUT		2
#define DB_DISP_REDISPLAY		4

/* These are the possible return values from pixel_to_glyph_translation. */
#define OVER_MODELINE		0
#define OVER_TEXT		1
#define OVER_OUTSIDE		2
#define OVER_NOTHING		3
#define OVER_BORDER		4
#define OVER_TOOLBAR		5

#define NO_BLOCK	-1

struct window_mirror;

/* Imagine that the text in the buffer is displayed on a piece of paper
   the width of the frame and very very tall.  The line start cache is
   an array of struct line_start_cache's, describing the start and
   end buffer positions for a contiguous set of lines on that piece
   of paper. */

struct line_start_cache
{
  Bufpos start, end;
  int height;
};

typedef struct line_start_cache_dynarr_type
{
  Dynarr_declare (struct line_start_cache);
} line_start_cache_dynarr;

enum rune_type
{
  BLANK,
  CHAR,
  DGLYPH,
  HLINE,
  VLINE			/* #### not implemented -- future possibility */
};

enum cursor_type
{
  CURSOR_ON,
  CURSOR_OFF,
  NO_CURSOR,
  NEXT_CURSOR,
  IGNORE_CURSOR
};

#define DEFAULT_INDEX	(face_index) 0
#define MODELINE_INDEX	(face_index) 1

/* A rune is a single display element, such as a printable character
   or pixmap.  Any single character in a buffer has one or more runes
   (or zero, if the character is invisible) corresponding to it. */
   
struct rune
{
  face_index findex;		/* face rune is displayed with */
  Lisp_Object extent;		/* extent rune is attached to, if any */

  short xpos;			/* horizontal starting position in pixels */
  short width;			/* pixel width of rune */

  enum cursor_type cursor_type;	/* is this rune covered by the cursor? */

  Bufpos bufpos;		/* buffer position this rune is displaying */
  Bufpos endpos;		/* if set this rune covers a range of pos */
  enum rune_type type;		/* type of rune object */
  union
  {
    /* DGLYPH */
    struct
    {
      Lisp_Object glyph;
      int xoffset;
    } dglyph;

    /* CHAR */
    Emchar ch;

    /* HLINE */
    struct
    {
      int thickness;		/* how thick to make hline */
      int yoffset;		/* how far down from top of line to put top */
    } hline;
  } object;			/* actual rune object */
};

typedef struct rune_dynarr_type
{
  Dynarr_declare (struct rune);
} rune_dynarr;

/* These must have distinct values.  Note that the ordering actually
   represents priority levels.  TEXT has the lowest priority level. */
enum display_type
{
  TEXT,
  LEFT_OUTSIDE_MARGIN,
  LEFT_INSIDE_MARGIN,
  RIGHT_INSIDE_MARGIN,
  RIGHT_OUTSIDE_MARGIN,
  OVERWRITE
};

struct display_block
{
  enum display_type type;	/* type of display block */

  int start_pos;		/* starting pixel position of block */
  int end_pos;			/* ending pixel position of block */

  rune_dynarr *runes;		/* Dynamic array of runes */
};

typedef struct display_block_dynarr_type
{
  Dynarr_declare (struct display_block);
} display_block_dynarr;

typedef struct layout_bounds_type
{
  int left_out;
  int left_in;
  int left_white;
  int right_white;
  int right_in;
  int right_out;
} layout_bounds;

struct glyph_block
{
  Lisp_Object glyph;
  Lisp_Object extent;
  /* The rest are only used by margin routines. */
  face_index findex;
  int active;
  int width;
};

typedef struct glyph_block_dynarr_type
{
  Dynarr_declare (struct glyph_block);
} glyph_block_dynarr;

struct display_line
{
  short ypos;				/* vertical position in pixels */
  unsigned short ascent, descent;	/* maximum values for this line */
  unsigned short clip;			/* amount of bottom of line to clip */
  Bufpos bufpos;			/* first buffer position on line */
  Bufpos end_bufpos;			/* last buffer position on line */
  Charcount offset;			/* adjustment to bufpos vals */
  Charcount num_chars;			/* # of chars on line
					   including expansion of tabs
					   and control chars */
  int cursor_elt;			/* rune block of TEXT display
					   block cursor is at or -1 */
  char used_prop_data;			/* can't incrementally updated if line
					   used propogation data */

  layout_bounds bounds;			/* line boundary positions */

  char modeline;			/* t if this line is a modeline */

  /* Dynamic array of display blocks */
  display_block_dynarr *display_blocks;

  /* Dynamic arrays of left and right glyph blocks */
  glyph_block_dynarr *left_glyphs;
  glyph_block_dynarr *right_glyphs;
};

typedef struct display_line_dynarr_type
{
  Dynarr_declare (struct display_line);
} display_line_dynarr;

enum prop_type
{
  PROP_STRING,
  PROP_CHAR,
  PROP_MINIBUF_PROMPT,
  PROP_BLANK
};

/* Data that should be propagated to the next line.  Either a single
   Emchar or a string of Bufbyte's. */

struct prop_block
{
  enum prop_type type;

  union data
  {
    struct
    {
      Bufbyte *str;
      Bytecount len;
      Bytecount pos;
    } p_string;

    struct
    {
      Emchar ch;
      Bufpos cursor_bufpos;
      enum cursor_type cursor_type;
    } p_char;

    struct
    {
      int width;
      face_index findex;
    } p_blank;
  } data;
};

typedef struct prop_block_dynarr_type
{
  Dynarr_declare (struct prop_block);
} prop_block_dynarr;

/* It could be argued that the following two structs belong in
   extents.h, but they're only used by redisplay and it simplifies
   the header files to put them here. */

typedef struct extent_dynarr_type
{
  Dynarr_declare (struct extent *);
} extent_dynarr;

/* NOTE NOTE NOTE: Currently the positions in an extent fragment
   structure are Bytind's, not Bufpos's.  This could change. */

struct extent_fragment
{
  struct buffer *buf;
  struct frame *frm;
  Bytind pos, end;
  extent_dynarr *extents;
  glyph_block_dynarr *begin_glyphs, *end_glyphs;
  int invisible;
};

struct font_metric_info
{
  int width;
  int height;			/* always ascent + descent; for convenience */
  int ascent;
  int descent;

  char proportional;
};

typedef struct position_redisplay_data_type
{
  /* This information is normally filled in by the create_*_block
     routines and is used by the add_*_rune routines. */
  Lisp_Object window;
  struct device *d;
  struct display_block *db;
  struct display_line *dl;
  Emchar ch;
  Bufpos bufpos;
  Bufpos endpos;
  int pixpos;
  int max_pixpos;
  int width;
  Bufpos cursor_bufpos;	/* This stores the buffer position of the cursor. */
  enum cursor_type cursor_type;
  int cursor_x;		/* rune block cursor is at */
  int start_col;
  int start_col_enabled;

  /* Information about the face the text should be displayed in and
     any begin-glyphs and end-glyphs. */
  struct extent_fragment *ef;
  face_index findex;
  struct font_metric_info fm;

  /* The height of a pixmap may either be predetermined if the user
     has set a baseline value, or it may be depedent on whatever the
     line ascent and descent values end up being based just on font
     information.  In the first case we can immediately update the
     values, thus there inclusion here.  In the last case we cannot
     determine the actual contribution to the line height until we
     have finished laying out all text on the line.  Thus we propagate
     the max height of such pixmaps and do a final calculation after
     all text has been added to the line. */
  int new_ascent;
  int new_descent;
  int max_pixmap_height;

} pos_data;

/* If non-zero, a window-system was specified on the command line.
   Defined in emacs.c. */
extern int display_arg;

/* Type of display specified.  Defined in emacs.c. */
extern char *display_use;

extern emchar_dynarr *mode_string_buffer;
extern bufbyte_dynarr *mode_spec;
extern Lisp_Object Vglobal_mode_string;

extern void redisplay (void);
extern struct display_block *get_display_block_from_line (struct display_line *dl, enum display_type type);
extern layout_bounds calculate_display_line_boundaries (struct window *w,
							int modeline);
extern Bufpos point_at_center (struct window *w, int type, int regen,
			       Bufpos start, Bufpos point);
extern int line_at_center (struct window *w, int type);
extern int window_half_pixpos (struct window *w);
extern void redisplay_echo_area (void);
extern int generate_modeline_string (struct window *w, int pos, int min_pos,
				     int max_pos, Lisp_Object elt, int depth,
				     int max_pixsize, face_index findex,
				     int type);
extern void free_display_structs (struct window_mirror *mir);
extern int pixel_to_glyph_translation (struct frame *f, int x_coord,
				       int y_coord, int *col, int *row,
				       int *obj_x, int *obj_y,
				       struct window **w, Bufpos *bufpos,
				       Bufpos *closest, Lisp_Object *class);
extern void glyph_to_pixel_translation (struct window *w, int char_x,
					int char_y, int *pix_x, int *pix_y);
extern void mark_redisplay (void (*) (Lisp_Object));
extern int point_in_line_start_cache (struct window *w, Bufpos point,
				      int min_past);
extern int point_would_be_visible (struct window *w, Bufpos startp,
				   Bufpos point);
extern Bufpos start_of_last_line (struct window *w, Bufpos startp);
extern Bufpos end_of_last_line (struct window *w, Bufpos startp);
extern Bufpos start_with_line_at_pixpos (struct window *w, Bufpos point,
					 int pixpos);
extern Bufpos start_with_point_on_display_line (struct window *w, Bufpos point,
						int line);
extern redisplay_variable_changed (Lisp_Object sym, Lisp_Object *val,
				   struct buffer *b, int flags);


/* defined in redisplay-output.c */
extern int get_next_display_block (layout_bounds bounds,
				   display_block_dynarr *dba, int start_pos,
				   int *next_start);
extern void redisplay_clear_bottom_of_window (struct window *w,
					      display_line_dynarr *ddla,
					      int min_start, int max_end);
extern void redisplay_update_line (struct window *w, int first_line,
				   int last_line, int update_values);
extern void redisplay_output_window (struct window *w);
extern int redisplay_move_cursor (struct window *w, Bufpos new_point,
				  int no_output_end);
extern void redisplay_redraw_cursor (struct frame *f, int run_begin_end_meths);
extern void output_display_line (struct window *w, display_line_dynarr *cdla,
				 display_line_dynarr *ddla, int line,
				 int force_start, int force_end);

/*
 * Extern's for redisplay defined global variables.
 */

/* #### organize this crap */

/* nil or a symbol naming the window system
   under which emacs is running
   ('x is the only current possibility) */
extern Lisp_Object Vwindow_system;

/* Version number of X windows: 10, 11 or nil.  */
extern Lisp_Object Vwindow_system_version;

/* Nonzero means truncate lines in all windows less wide than the frame */
extern int truncate_partial_width_windows;

extern int in_display;

/* Nonzero means no need to redraw the entire frame on resuming
   a suspended Emacs.  This is useful on terminals with multiple pages,
   where one page is used for Emacs and another for all else. */
extern int no_redraw_on_reenter;

/* Nonzero means flash the frame instead of ringing the bell.  */
extern int visible_bell;

/* Thickness of shadow border around 3D modelines. */
extern Lisp_Object Vmodeline_shadow_thickness;

/* Scroll if point lands on the bottom line and that line is partially
   clipped. */
extern int scroll_on_clipped_lines;

/* Quick flags to signal redisplay.  redisplay() sets them all to 0
   when it finishes.  If none of them are set when it starts, it
   assumes that nothing needs to be done.  Functions that make a change
   that is (potentially) visible on the screen should set the
   appropriate flag.

   If any of these flags are set, redisplay will look more carefully
   to see if anything has really changed. */

/* non-nil if the contents of a buffer have changed since the last time
   redisplay completed */
extern int buffers_changed;
extern int buffers_changed_set;

/* Nonzero if head_clip or tail_clip of a buffer has changed
 since last redisplay that finished */
extern int clip_changed;
extern int clip_changed_set;

/* non-nil if any extent has changed since the last time redisplay completed */
extern int extents_changed;
extern int extents_changed_set;

/* non-nil if any face has changed since the last time redisplay completed */
extern int faces_changed;

/* Nonzero means one or more frames have been marked as garbaged */
extern int frame_changed;

/* True if a menubar is in need of updating somewhere. */
extern int menubar_changed;
extern int menubar_changed_set;

/* true iff we should redraw the modelines on the next redisplay */
extern int modeline_changed;
extern int modeline_changed_set;

/* non-nil if point has changed in some buffer since the last time
   redisplay completed */
extern int point_changed;
extern int point_changed_set;

/* non-nil if some frame has changed its size */
extern int size_changed;

/* non-nil if some device has signaled that it wants to change size */
extern int asynch_device_change_pending;

/* non-nil if any toolbar has changed */
extern int toolbar_changed;
extern int toolbar_changed_set;

/* non-nil if any window has changed since the last time redisplay completed */
extern int windows_changed;

/* non-nil if any frame's window structure has changed since the last
   time redisplay completed */
extern int windows_structure_changed;

/* These macros can be relatively expensive.  Since they are often
   called numerous times between each call to redisplay, we keep track
   if each has already been called and don't bother doing most of the
   work if it is currently set. */

#define MARK_TYPE_CHANGED(object) do {					\
  if (!object##_changed_set) {						\
    Lisp_Object dev;							\
    DEVICE_LOOP (dev)							\
      {									\
        Lisp_Object frm;						\
        struct device *d = XDEVICE (XCONS (dev)->car);			\
        FRAME_LOOP (frm, d)						\
	  XFRAME (XCONS (frm)->car)->object##_changed = 1;		\
        d->object##_changed = 1;					\
      }									\
    object##_changed = 1;						\
    object##_changed_set = 1; }						\
  }  while (0)

#define MARK_BUFFERS_CHANGED MARK_TYPE_CHANGED(buffers)
#define MARK_CLIP_CHANGED MARK_TYPE_CHANGED(clip)
#define MARK_EXTENTS_CHANGED MARK_TYPE_CHANGED(extents)
#define MARK_MENUBAR_CHANGED MARK_TYPE_CHANGED(menubar)
#define MARK_MODELINE_CHANGED MARK_TYPE_CHANGED(modeline)
#define MARK_POINT_CHANGED MARK_TYPE_CHANGED(point)
#define MARK_TOOLBAR_CHANGED MARK_TYPE_CHANGED(toolbar)

/* Anytime a device or frame is added or deleted we need to reset
   these flags. */
#define RESET_CHANGED_SET_FLAGS						\
  do {									\
    buffers_changed_set = 0;						\
    clip_changed_set = 0;						\
    extents_changed_set = 0;						\
    menubar_changed_set = 0;						\
    modeline_changed_set = 0;						\
    point_changed_set = 0;						\
    toolbar_changed_set = 0;						\
  } while (0)

#endif /* _XEMACS_REDISPLAY_H_ */

/* Copyright (C) 1995 Amdahl Corporation.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing. */

#ifndef _EmacsManagerP_h
#define _EmacsManagerP_h


#include "xintrinsicp.h"
#ifdef LWLIB_USES_MOTIF
#include "xmmanagerp.h"
#endif
#include "EmacsManager.h"

typedef struct {		/* new fields for EmacsManager class */
  int dummy;
} EmacsManagerClassPart;

typedef struct _EmacsManagerClassRec {	/* full class record declaration */
  CoreClassPart core_class;
  CompositeClassPart composite_class;
#ifdef LWLIB_USES_MOTIF
  ConstraintClassPart constraint_class;
  XmManagerClassPart manager_class;
#endif
  EmacsManagerClassPart emacs_manager_class;
} EmacsManagerClassRec;

typedef struct {		/* new fields for EmacsManager widget */
  XtCallbackList resize_callback;
  XtCallbackList query_geometry_callback;
  XtPointer user_data;
} EmacsManagerPart;

typedef struct _EmacsManagerRec {	/* full instance record */
    CorePart core;
    CompositePart composite;
#ifdef LWLIB_USES_MOTIF
    ConstraintPart constraint;
    XmManagerPart manager;
#endif
    EmacsManagerPart emacs_manager;
} EmacsManagerRec;

extern EmacsManagerClassRec emacsManagerClassRec;	 /* class pointer */

#endif /* _EmacsManagerP_h */

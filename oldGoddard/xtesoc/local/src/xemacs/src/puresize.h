/* Definition of PURESIZE.
   Copyright (C) 1986, 1988, 1992, 1993, 1994 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: */

#ifndef PURESIZE_H
#define PURESIZE_H

/* # bytes of pure Lisp code to leave space for.
   Note that s/msdos.h, s/vms.h, m/alpha.h, m/altos.h, m/ns32000.h,
   m/sparc.h, m/sun2.h, m/sun386.h and m/ustation.h may override this
   default.  */

/* If PURESIZE is already defined then either the user overrode it at
   configure time, or the above happened. */
#ifndef PURESIZE

#define DEFAULT_PURESIZE 483000

#if defined(ENERGIZE) && !defined(PURESIZE)
# define PURESIZE (DEFAULT_PURESIZE + 360000)
#endif

#if defined(SUNPRO) && !defined(PURESIZE)
# define PURESIZE (DEFAULT_PURESIZE + 174000)
#endif

#if defined(TOOLTALK) && !defined(PURESIZE)
# define PURESIZE (DEFAULT_PURESIZE + 69000)
#endif

#ifndef PURESIZE
# define PURESIZE DEFAULT_PURESIZE
#endif

#endif /* !PURESIZE */

#endif /* PURESIZE_H */

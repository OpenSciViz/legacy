/* Fixed-size block allocator -- include file.
   Copyright (C) 1994 Amdahl Corporation.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

Written by Ben Wing, December 1994.

*/

#ifndef _XEMACS_BLOCKTYPE_H_
#define _XEMACS_BLOCKTYPE_H_

#define Blocktype_declare(type)                                      \
  type *free;                                                        \
  int elsize;                                                        \
  type *tempel

void *Blocktype_newf (int elsize);
void Blocktype_allocf (void *b);
void Blocktype_free (void *bbb, void *el);

#define Blocktype_new(structype) \
  (structype *) Blocktype_newf (sizeof(*(((structype *) NULL)->free)))
#define Blocktype_alloc(b) (Blocktype_allocf (b), (b)->tempel)

#endif /* _XEMACS_BLOCKTYPE_H_ */

/* This file is part of XEmacs.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with this library; if not, write to the Free
Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

/* Synched up with: Not in FSF. */

#ifndef _EXTW_XT_H_
#define _EXTW_XT_H_

#include "extw-Xlib.h"

#ifndef XtCXtToolkitError
#define XtCXtToolkitError "XtToolkitError"
#endif

#ifndef DEFAULT_WM_TIMEOUT
#define DEFAULT_WM_TIMEOUT 5000
#endif

void extw_send_geometry_value(Display *display, Window win, Atom property,
			      en_extw_notify type, XtWidgetGeometry *xwg,
			      long data0);
void extw_get_geometry_value(Display *display, Window win, Atom property,
			     XtWidgetGeometry *xwg);
Bool extw_wait_for_response(Widget w, XEvent *event, unsigned long request_num,
			    en_extw_notify type, unsigned long timeout);


#endif /* _EXTW_XT_H_ */

/* TTY frame functions.
   Copyright (C) 1995 Amdahl Corporation

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing. */

/* #### This file is just a stub.  It should be possible to have more
   than one frame on a tty, with only one frame being "active" (displayed)
   at a time. */

#include <config.h>
#include "lisp.h"

#include "device-tty.h"
#include "frame.h"

Lisp_Object Vdefault_tty_frame_alist;

static void
tty_init_frame (struct frame *f, Lisp_Object frame_data)
{
  struct device *d = XDEVICE (FRAME_DEVICE (f));
  if (!NILP (DEVICE_FRAME_LIST (d)))
    error ("Only one frame allowed on TTY devices");

  f->name = build_string ("emacs");
  f->height = DEVICE_TTY_DATA (d)->height;
  f->width = DEVICE_TTY_DATA (d)->width;
  f->visible = 1;
  f->scrollbar_on_left = 1;
  f->scrollbar_on_top = 0;
  SET_FRAME_CLEAR (f);
}


/************************************************************************/
/*                            initialization                            */
/************************************************************************/

void
device_type_create_frame_tty (void)
{
  /* frame methods */
  DEVICE_HAS_METHOD (tty, init_frame);
}

void
vars_of_frame_tty (void)
{
  DEFVAR_LISP ("default-tty-frame-alist", &Vdefault_tty_frame_alist,
    "Alist of default frame-creation parameters for tty frames.\n\
These are in addition to and override what is specified in\n\
`default-frame-alist', but are overridden by the arguments to the\n\
particular call to `make-frame'.");
  Vdefault_tty_frame_alist = Qnil;

  tty_device_methods->device_specific_frame_params = &Vdefault_tty_frame_alist;
}

/* prototypes for keymap-hacking
   Copyright (C) 1992, 1993, 1994 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */


#ifndef _XEMACS_KEYMAP_H_
#define _XEMACS_KEYMAP_H_

DECLARE_LRECORD (keymap, struct keymap);
#define XKEYMAP(x) XRECORD (x, keymap, struct keymap)
#define XSETKEYMAP(x, p) XSETRECORD (x, p, keymap)
#define KEYMAPP(x) RECORDP (x, keymap)
#define CHECK_KEYMAP(x, i) CHECK_RECORD (x, keymap)

extern Lisp_Object get_keymap (Lisp_Object object, int errorp, int autoload);
extern Lisp_Object event_binding (Lisp_Object event0, int accept_default);
extern Lisp_Object function_key_map_event_binding (Lisp_Object event0);

extern Lisp_Object Fkey_description (Lisp_Object keys);
extern Lisp_Object Fsingle_key_description (Lisp_Object key);
extern Lisp_Object Fwhere_is_internal (Lisp_Object definition, 
                                       Lisp_Object keymaps,
                                       Lisp_Object firstonly,
                                       Lisp_Object noindirect,
				       Lisp_Object event_or_keys);

extern Lisp_Object Fkeymap_name (Lisp_Object keymap);
extern Lisp_Object Fset_keymap_name (Lisp_Object keymap, Lisp_Object name);
extern Lisp_Object Fkeymap_prompt (Lisp_Object keymap, Lisp_Object inherit);
extern Lisp_Object Fset_keymap_prompt (Lisp_Object keymap, Lisp_Object prompt);

extern int relevant_keymaps_to_search (Lisp_Object keys,
                                       int max_maps, Lisp_Object maps[]);
extern void describe_map_tree (Lisp_Object startmap, int partial,
                               Lisp_Object shadow, Lisp_Object prefix,
                               int mice_only_p);

extern void key_desc_list_to_event (Lisp_Object list, Lisp_Object event,
				    int allow_menu_events);

extern Lisp_Object Vmeta_prefix_char;
extern int event_matches_key_specifier_p (struct Lisp_Event *event,
					  Lisp_Object key_specifier);

#endif /* _XEMACS_KEYMAP_H_ */

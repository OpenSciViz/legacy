/*
   Copyright (C) 1995 Amdahl Corporation.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifdef SYSV_SYSTEM_DIR
# include <dirent.h>
#elif defined (NONSYSTEM_DIR_LIBRARY)
# include "ndir.h"
#elif defined (MSDOS)
# include <dirent.h>
#else
# include <sys/dir.h>
#endif /* not NONSYSTEM_DIR_LIBRARY */

#ifdef SYSV_SYSTEM_DIR
# define DIRENTRY struct dirent
#else /* not SYSV_SYSTEM_DIR */
# define DIRENTRY struct direct
#endif

/* The d_nameln member of a struct dirent includes the '\0' character
   on some systems, but not on others.  What's worse, you can't tell
   at compile-time which one it will be, since it really depends on
   the sort of system providing the filesystem you're reading from,
   not the system you are running on.  Paul Eggert
   <eggert@bi.twinsun.com> says this occurs when Emacs is running on a
   SunOS 4.1.2 host, reading a directory that is remote-mounted from a
   Solaris 2.1 host and is in a native Solaris 2.1 filesystem.

   Since applying strlen to the name always works, we'll just do that.  */
#define NAMLEN(p) strlen (p->d_name)

#ifdef MSDOS
#define DIRENTRY_NONEMPTY(p) ((p)->d_name[0] != 0)
#else
#define DIRENTRY_NONEMPTY(p) ((p)->d_ino)
#endif

/* encapsulation: directory calls */

#ifdef ENCAPSULATE_CHDIR
extern int sys_chdir (CONST char *path);
#endif
#if defined (ENCAPSULATE_CHDIR) && !defined (DONT_ENCAPSULATE)
# undef chdir
# define chdir sys_chdir
#endif
#if !defined (ENCAPSULATE_CHDIR) && defined (DONT_ENCAPSULATE)
# define sys_chdir chdir
#endif

#ifdef ENCAPSULATE_MKDIR
extern int sys_mkdir (CONST char *path, int mode);
#endif
#if defined (ENCAPSULATE_MKDIR) && !defined (DONT_ENCAPSULATE)
# undef mkdir
# define mkdir sys_mkdir
#endif
#if !defined (ENCAPSULATE_MKDIR) && defined (DONT_ENCAPSULATE)
# define sys_mkdir mkdir
#endif

#ifdef ENCAPSULATE_OPENDIR
extern DIR *sys_opendir (CONST char *filename);
#endif
#if defined (ENCAPSULATE_OPENDIR) && !defined (DONT_ENCAPSULATE)
# undef opendir
# define opendir sys_opendir
#endif
#if !defined (ENCAPSULATE_OPENDIR) && defined (DONT_ENCAPSULATE)
# define sys_opendir opendir
#endif

#ifdef ENCAPSULATE_READDIR
extern DIRENTRY *sys_readdir (DIR *dirp);
#endif
#if defined (ENCAPSULATE_READDIR) && !defined (DONT_ENCAPSULATE)
# undef readdir
# define readdir sys_readdir
#endif
#if !defined (ENCAPSULATE_READDIR) && defined (DONT_ENCAPSULATE)
# define sys_readdir readdir
#endif

#ifdef ENCAPSULATE_RMDIR
extern int sys_rmdir (CONST char *path);
#endif
#if defined (ENCAPSULATE_RMDIR) && !defined (DONT_ENCAPSULATE)
# undef rmdir
# define rmdir sys_rmdir
#endif
#if !defined (ENCAPSULATE_RMDIR) && defined (DONT_ENCAPSULATE)
# define sys_rmdir rmdir
#endif


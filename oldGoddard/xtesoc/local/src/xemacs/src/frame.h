/* Define frame-object for XEmacs.
   Copyright (C) 1988, 1992, 1993, 1994 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: FSF 19.28. */

#ifndef _XEMACS_FRAME_H_
#define _XEMACS_FRAME_H_

#include "scrollbar.h"
#include "toolbar.h"
#include "device.h"

struct frame
{
  struct lcrecord_header header;

  /* device frame belongs to. */
  Lisp_Object device;

  /* Name of this frame: a Lisp string.  */
  Lisp_Object name;

  /* Size of this frame, in units of characters.  */
  int height;
  int width;

  /* Size of this frame, in units of pixels. */
  int pixheight;
  int pixwidth;

  /* Width of the internal border.  This is a line of background color
     just inside the window's border.  It is normally only non-zero on
     X frames, but we put it here to avoid introducing window system
     dependencies. */
  int internal_border_width;

  /* New height and width for pending size change.  0 if no change pending.  */
  int new_height, new_width;

  /* This frame's root window.  Every frame has one.
     If the frame has only a minibuffer window, this is it.
     Otherwise, if the frame has a minibuffer window, this is its sibling.  */
  Lisp_Object root_window;

  /* This frame's selected window.
     Each frame has its own window hierarchy
     and one of the windows in it is selected within the frame.
     The selected window of the selected frame is Emacs's selected window.  */
  Lisp_Object selected_window;

  /* This frame's minibuffer window.
     Most frames have their own minibuffer windows,
     but only the selected frame's minibuffer window
     can actually appear to exist.  */
  Lisp_Object minibuffer_window;

  /* This frame's root window mirror.  This structure exactly mirrors
     the frame's window structure but contains only pointers to the
     display structures. */
  struct window_mirror *root_mirror;
  
  /* A copy of the global Vbuffer_list, to maintain a per-frame buffer
     ordering.  The Vbuffer_list variable and the buffer_list slot of each
     frame contain exactly the same data, just in different orders.  */
  Lisp_Object buffer_alist;

  /* #### serious overhaulage needs to be applied to the following. */
  Lisp_Object pointer;
  Lisp_Object scrollbar_pointer;

  /* Vector representing the menubar currently displayed.  See menubar.c. */
  Lisp_Object menubar_data;

  /* Data representing each currently displayed toolbar.  See
     toolbar.c */
  Lisp_Object toolbar_data[4];

  /* frame-local scrollbar information.  See scrollbar.c. */
  int scrollbar_y_offset;
  int scrollbar_on_left;
  int scrollbar_on_top;

  /* cache of created scrollbars */
  struct scrollbar_instance *sb_vcache;
  struct scrollbar_instance *sb_hcache;

  /* A structure of auxiliary data specific to the device type.
     struct x_frame is used for X window frames; defined in frame-x.h */
  void *frame_data;

  /* specifier values cached in the struct frame: */

  /* Width and height of the scrollbars. */
  Lisp_Object scrollbar_width, scrollbar_height;

  /* Size of the toolbars.  The frame-local toolbar space is
     subtracted before the windows are arranged.  Window and buffer
     local toolbars overlay their windows. */
  Lisp_Object toolbar_size[4];

  /* Possible frame-local default for outside margin widths. */
  Lisp_Object left_margin_width;
  Lisp_Object right_margin_width;

  /* one-bit flags: */

  /* Is frame marked for deletion?  This is used in XSetErrorHandler().  */
  unsigned int being_deleted :1;

  /* Nonzero if this frame has been destroyed. */
  unsigned int dead :1;

  /* Nonzero if last attempt at redisplay on this frame was preempted.  */
  unsigned int display_preempted :1;

  /* Nonzero if frame is currently displayed.  */
  unsigned int visible :1;

  /* Nonzero if window is currently iconified.
     This and visible are mutually exclusive.  */
  unsigned int iconified :1;

  /* Nonzero if this frame should be cleared and then redrawn.
     Setting this will also effectively set frame_changed. */
  unsigned int clear :1;

  /* True if frame actually has a  minibuffer window on it.
     0 if using a minibuffer window that isn't on this frame.  */
  unsigned int has_minibuffer :1;
     
  /* True if frame's root window can't be split.  */
  unsigned int no_split :1;

  unsigned int top_toolbar_was_visible :1;
  unsigned int bottom_toolbar_was_visible :1;
  unsigned int left_toolbar_was_visible :1;
  unsigned int right_toolbar_was_visible :1;

  /* redisplay flags */
  unsigned int buffers_changed :1;
  unsigned int clip_changed :1;
  unsigned int extents_changed :1;
  unsigned int faces_changed :1;
  unsigned int frame_changed :1;
  unsigned int menubar_changed :1;
  unsigned int modeline_changed :1;
  unsigned int point_changed :1;
  unsigned int size_changed :1;
  unsigned int toolbar_changed :1;
  unsigned int windows_changed :1;
  unsigned int windows_structure_changed :1;
  unsigned int window_face_cache_reset :1;	/* used by expose handler */

  unsigned int size_change_pending :1;
  unsigned int mirror_dirty :1;

  /* flag indicating if any window on this frame is displaying a subwindow */
  int subwindows_being_displayed :1;
};

/* If this is non-nil, it is the frame that make-frame is currently
   creating.  We can't set the current frame to this in case the
   debugger goes off because it would try and display to it.  However,
   there are some places which need to reference it which have no
   other way of getting it if it isn't the selected frame. */
extern Lisp_Object Vframe_being_created;

#ifdef emacs

DECLARE_LRECORD (frame, struct frame);
#define XFRAME(x) XRECORD (x, frame, struct frame)
#define XSETFRAME(x, p) XSETRECORD (x, p, frame)
#define FRAMEP(x) RECORDP (x, frame)
#define CHECK_FRAME(x, i) CHECK_RECORD (x, frame)

#define CHECK_LIVE_FRAME(x, i)						\
  do { CHECK_FRAME (x, i);						\
       if (! FRAMEP (x)							\
	   || ! FRAME_LIVE_P (XFRAME (x)))				\
         x = wrong_type_argument (Qframe_live_p, (x)); } while (0)
#define CHECK_X_FRAME(x, i)						\
  do { CHECK_FRAME (x, i);						\
       if (! FRAMEP (x)							\
	   || ! FRAME_IS_X (XFRAME (x)))				\
         x = wrong_type_argument (Qframe_x_p, (x)); } while (0)
#define CHECK_NS_FRAME(x, i)						\
  do { CHECK_FRAME (x, i);						\
       if (! FRAMEP (x)							\
	   || ! FRAME_IS_NS (XFRAME (x)))				\
         x = wrong_type_argument (Qframe_ns_p, (x)); } while (0)
#define CHECK_TTY_FRAME(x, i)						\
  do { CHECK_FRAME (x, i);						\
       if (! FRAMEP (x)							\
	   || ! FRAME_IS_TTY (XFRAME (x)))				\
         x = wrong_type_argument (Qframe_tty_p, (x)); } while (0)

typedef struct frame *FRAME_PTR;
extern Lisp_Object Qframe_live_p;
extern Lisp_Object Qframe_x_p, Qframe_tty_p;
extern Lisp_Object Vframe_title_format, Vframe_icon_title_format;

extern int frame_changed;

#define MARK_FRAME_FACES_CHANGED(f) do {				\
  (f)->faces_changed = 1;						\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_FACES_CHANGED (XDEVICE ((f)->device));			\
  else									\
    faces_changed = 1; } while (0)

#define MARK_FRAME_TOOLBARS_CHANGED(f) do {				\
  (f)->toolbar_changed = 1;						\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_TOOLBARS_CHANGED (XDEVICE ((f)->device));		\
  else									\
    toolbar_changed = 1; } while (0)

#define MARK_FRAME_SIZE_CHANGED(f) do {					\
  (f)->size_changed = 1;						\
  (f)->size_change_pending = 1;						\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_SIZE_CHANGED (XDEVICE ((f)->device));			\
  else									\
    size_changed = 1; } while (0)

#define MARK_FRAME_CHANGED(f) do {					\
  (f)->frame_changed = 1;						\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_FRAME_CHANGED (XDEVICE ((f)->device));			\
  else									\
    frame_changed = 1; } while (0)

#define MARK_FRAME_WINDOWS_CHANGED(f) do {				\
  (f)->windows_changed = 1;						\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_WINDOWS_CHANGED (XDEVICE ((f)->device));		\
  else									\
    windows_changed = 1; } while (0)

#define MARK_FRAME_WINDOWS_STRUCTURE_CHANGED(f) do {			\
  (f)->windows_structure_changed = 1;					\
  if (!NILP ((f)->device))						\
    MARK_DEVICE_WINDOWS_STRUCTURE_CHANGED (XDEVICE ((f)->device));	\
  else									\
    windows_structure_changed = 1; } while (0)


#define SET_FRAME_CLEAR(f) MARK_FRAME_CHANGED(f); (f)->clear = 1
#define FRAME_DEVICE(f) ((f)->device)
#define FRAME_LIVE_P(f) (!(f)->dead)
#define FRAME_IS_TTY(f) (!(f)->dead && DEVICE_IS_TTY (XDEVICE ((f)->device)))

#define FRAME_IS_X(f) (!(f)->dead && DEVICE_IS_X (XDEVICE ((f)->device)))
#define FRAME_IS_NS(f) (!(f)->dead && DEVICE_IS_NS (XDEVICE ((f)->device)))
#if defined (HAVE_X_WINDOWS) && defined (HAVE_NEXTSTEP)
#define FRAME_IS_WIN(f) (FRAME_IS_X (f) || FRAME_IS_NS (f))
#elif defined (HAVE_X_WINDOWS)
#define FRAME_IS_WIN(f) FRAME_IS_X (f)
#elif defined (HAVE_NEXTSTEP)
#define FRAME_IS_WIN(f) FRAME_IS_NS (f)
#else
#define FRAME_IS_WIN(f) 0
#endif

#define FRAME_MINIBUF_ONLY_P(f) \
  EQ (FRAME_ROOT_WINDOW (f), FRAME_MINIBUF_WINDOW (f))
#define FRAME_HAS_MINIBUF_P(f) ((f)->has_minibuffer)
#define FRAME_HEIGHT(f) ((f)->height)
#define FRAME_WIDTH(f) ((f)->width)
#define FRAME_PIXHEIGHT(f) ((f)->pixheight)
#define FRAME_PIXWIDTH(f) ((f)->pixwidth)
#define FRAME_SCROLLBAR_WIDTH(f) XINT ((f)->scrollbar_width)
#define FRAME_SCROLLBAR_HEIGHT(f) XINT ((f)->scrollbar_height)
#define FRAME_TOP_TOOLBAR_HEIGHT(f) XINT ((f)->toolbar_size[TOP_TOOLBAR])
#define FRAME_BOTTOM_TOOLBAR_HEIGHT(f) XINT ((f)->toolbar_size[BOTTOM_TOOLBAR])
#define FRAME_LEFT_TOOLBAR_WIDTH(f) XINT ((f)->toolbar_size[LEFT_TOOLBAR])
#define FRAME_RIGHT_TOOLBAR_WIDTH(f) XINT ((f)->toolbar_size[RIGHT_TOOLBAR])
#define FRAME_NEW_HEIGHT(f) ((f)->new_height)
#define FRAME_NEW_WIDTH(f) ((f)->new_width)
#define FRAME_CURSOR_X(f) ((f)->cursor_x)
#define FRAME_CURSOR_Y(f) ((f)->cursor_y)
#define FRAME_VISIBLE_P(f) ((f)->visible)
#define FRAME_NO_SPLIT_P(f) ((f)->no_split)
#define FRAME_ICONIFIED_P(f) ((f)->iconified)
#define FRAME_MINIBUF_WINDOW(f) ((f)->minibuffer_window)
#define FRAME_ROOT_WINDOW(f) ((f)->root_window)
#define FRAME_SELECTED_WINDOW(f) ((f)->selected_window)
#define FRAME_SB_VCACHE(f) ((f)->sb_vcache)
#define FRAME_SB_HCACHE(f) ((f)->sb_hcache)

#define FRAME_TOP_TOOLBAR_VISIBLE(f)					\
 (!NILP ((f)->toolbar_data[TOP_TOOLBAR])		\
  && !NILP (FRAME_TOOLBAR_DATA ((f), TOP_TOOLBAR)->toolbar_buttons))
#define FRAME_BOTTOM_TOOLBAR_VISIBLE(f)					\
 (!NILP ((f)->toolbar_data[BOTTOM_TOOLBAR])		\
  && !NILP (FRAME_TOOLBAR_DATA ((f), BOTTOM_TOOLBAR)->toolbar_buttons))
#define FRAME_LEFT_TOOLBAR_VISIBLE(f)					\
 (!NILP ((f)->toolbar_data[LEFT_TOOLBAR])		\
  && !NILP (FRAME_TOOLBAR_DATA ((f), LEFT_TOOLBAR)->toolbar_buttons))
#define FRAME_RIGHT_TOOLBAR_VISIBLE(f)					\
 (!NILP ((f)->toolbar_data[RIGHT_TOOLBAR])		\
  && !NILP (FRAME_TOOLBAR_DATA ((f), RIGHT_TOOLBAR)->toolbar_buttons))

#define FRAME_BORDER_WIDTH(f) ((f)->internal_border_width)
#define FRAME_BORDER_HEIGHT(f) ((f)->internal_border_width)

#define FRAME_REAL_TOP_TOOLBAR_HEIGHT(f)				\
     (FRAME_TOP_TOOLBAR_VISIBLE (f) ? FRAME_TOP_TOOLBAR_HEIGHT (f) : 0)
#define FRAME_REAL_BOTTOM_TOOLBAR_HEIGHT(f)				\
     (FRAME_BOTTOM_TOOLBAR_VISIBLE (f) ? FRAME_BOTTOM_TOOLBAR_HEIGHT (f) : 0)
#define FRAME_REAL_LEFT_TOOLBAR_WIDTH(f)				\
     (FRAME_LEFT_TOOLBAR_VISIBLE (f) ? FRAME_LEFT_TOOLBAR_WIDTH (f) : 0)
#define FRAME_REAL_RIGHT_TOOLBAR_WIDTH(f)				\
     (FRAME_RIGHT_TOOLBAR_VISIBLE (f) ? FRAME_RIGHT_TOOLBAR_WIDTH (f) : 0)

#define FRAME_TOP_BORDER_START(f) (FRAME_REAL_TOP_TOOLBAR_HEIGHT (f))
#define FRAME_TOP_BORDER_END(f)						\
  (FRAME_TOP_BORDER_START (f) + FRAME_BORDER_HEIGHT (f))

#define FRAME_BOTTOM_BORDER_START(f)					\
  (FRAME_PIXHEIGHT (f) - FRAME_BORDER_HEIGHT (f) -			\
   FRAME_REAL_BOTTOM_TOOLBAR_HEIGHT (f))
#define FRAME_BOTTOM_BORDER_END(f)					\
  (FRAME_PIXHEIGHT (f) - FRAME_REAL_BOTTOM_TOOLBAR_HEIGHT (f))

#define FRAME_LEFT_BORDER_START(f) (FRAME_REAL_LEFT_TOOLBAR_WIDTH (f))
#define FRAME_LEFT_BORDER_END(f)					\
  (FRAME_LEFT_BORDER_START (f) + FRAME_BORDER_WIDTH (f))

#define FRAME_RIGHT_BORDER_START(f)					\
  (FRAME_PIXWIDTH (f) - FRAME_BORDER_WIDTH (f) -			\
   FRAME_REAL_RIGHT_TOOLBAR_WIDTH (f))
#define FRAME_RIGHT_BORDER_END(f)					\
  (FRAME_PIXWIDTH (f) - FRAME_REAL_RIGHT_TOOLBAR_WIDTH (f))


#define HAS_FRAMEMETH_P(f, m) HAS_DEVMETH_P (XDEVICE (FRAME_DEVICE (f)), m)
#define FRAMEMETH(f,m, args) DEVMETH (XDEVICE (FRAME_DEVICE (f)), m, args)
#define MAYBE_FRAMEMETH(f, m, args) \
  MAYBE_DEVMETH (XDEVICE (FRAME_DEVICE (f)), m, args)
#define FRAMEMETH_OR_GIVEN(f, m, args, given) \
  DEVMETH_OR_GIVEN (XDEVICE (FRAME_DEVICE (f)), m, args, given)

#define FRAME_LOOP(consvar, d) LIST_LOOP (consvar, DEVICE_FRAME_LIST (d))

#define DEVICE_AND_FRAME_LOOP(devcons, frmcons)				\
  DEVICE_LOOP (devcons)							\
    FRAME_LOOP (frmcons, XDEVICE (XCAR (devcons)))

extern Lisp_Object Vdefault_frame_name;

extern void update_frame_title (struct frame *f);
extern struct frame *choose_minibuf_frame (void);
extern Lisp_Object next_frame (Lisp_Object f, Lisp_Object mini);
extern Lisp_Object prev_frame (Lisp_Object f, Lisp_Object mini);
extern void store_in_alist (Lisp_Object *alistptr, 
                            CONST char *propname, 
                            Lisp_Object val);
extern void do_pending_frame_size_change (void);
extern void change_frame_size (struct frame *frame,
                                int newlength, int newwidth, 
                                int pretend, int delay);
extern void delete_frame_internal (Lisp_Object frame,
					  int ok_to_delete_last);
extern void hold_frame_size_changes (void);
extern void unhold_frame_size_changes (struct frame *f);
extern void select_frame_1 (Lisp_Object frame);
extern struct frame *selected_frame (void);
extern struct frame *device_selected_frame (struct device *d);
extern struct frame *get_frame (Lisp_Object frame);
extern int other_visible_frames (struct frame *f);

#endif /* emacs */

#endif /* _XEMACS_FRAME_H_ */

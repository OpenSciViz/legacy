/* Generic glyph data structures + display tables
   Copyright (C) 1994 Board of Trustees, University of Illinois
   Copyright (C) 1995 Ben Wing

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_GLYPHS_H_
#define _XEMACS_GLYPHS_H_

#include "specifier.h"

/*****************************************************************************
 *                            Image Instantiators                            *
 *****************************************************************************/

struct image_instantiator_methods;

/* Note that image-instance types are not the same as image-instantiator types.
   Here's an approximate mapping:

  image type				image-instantiator type
  ----------				-----------------------
  nothing				nothing
  string				text
  formatted-string			text
  xbm					mono-pixmap or cursor
  xpm					color-pixmap, mono-pixmap, or cursor
  xface					mono-pixmap or cursor
  cursor-font				cursor
  subwindow				subwindow

*/

enum image_instance_type
{
  IMAGE_UNKNOWN,
  IMAGE_NOTHING,
  IMAGE_TEXT,
  IMAGE_MONO_PIXMAP,
  IMAGE_COLOR_PIXMAP,
  IMAGE_CURSOR,
  IMAGE_SUBWINDOW
};

#define IMAGE_NOTHING_MASK (1 << 0)
#define IMAGE_TEXT_MASK (1 << 1)
#define IMAGE_MONO_PIXMAP_MASK (1 << 2)
#define IMAGE_COLOR_PIXMAP_MASK (1 << 3)
#define IMAGE_CURSOR_MASK (1 << 4)
#define IMAGE_SUBWINDOW_MASK (1 << 5)

typedef struct ii_keyword_entry_dynarr_type
{
  Dynarr_declare (struct ii_keyword_entry);
} Ii_keyword_entry_dynarr;

/* These are methods specific to a particular type of image instantiator
   (e.g. xpm, string, etc.). */

struct image_instantiator_methods
{
  Lisp_Object symbol;

  Ii_keyword_entry_dynarr *keywords;
  /* Implementation specific methods: */

  /* Validate method: Given an instantiator vector, return non-zero if
     it's valid for this image-instantiator type, zero otherwise.
     Note that this validation only occurs after all the keyword-
     specific validation has already been performed.  This is chiefly
     useful for making sure that certain required keywords are
     present.  If NO_ERROR is zero, the validation method can signal
     an error if the instantiator is invalid. (This is so that a more
     explanatory error message can be issued.) Even if NO_ERROR is
     zero, however, this function can go ahead and return 0 for an
     invalid instantiator. (In that case, the caller gets a generic
     error message "Invalid instantiator".). */
  int (*validate_method) (Lisp_Object instantiator, int no_error);

  /* Normalize method: Given an instantiator, convert it to the form
     that should be used in a glyph, for devices of type DEVICE_TYPE.
     Return Qnil if the conversion fails. */
  Lisp_Object (*normalize_method) (Lisp_Object instantiator,
				   Lisp_Object device_type,
				   int no_error);

  /* Instantiate method: Given an instantiator and a partially
     filled-in image instance, complete the filling-in.  Return
     non-zero if the instantiation succeeds, 0 if it fails.
     This must be present. */
  int (*instantiate_method) (Lisp_Object image_instance,
			     Lisp_Object instantiator,
			     int dest_mask,
			     int no_error);
};

struct ii_keyword_entry
{
  Lisp_Object keyword;
  int (*validate) (Lisp_Object data, int no_error);
  int multiple_p;
};

/***** Calling an image-instantiator method *****/

#define HAS_IITYPE_METH_P(mstruc, m) ((mstruc)->m##_method)
#define IITYPE_METH(mstruc, m, args) (((mstruc)->m##_method) args)

/* Call a void-returning specifier method, if it exists */
#define MAYBE_IITYPE_METH(mstruc, m, args)				   \
do {									   \
  struct image_instantiator_methods *_maybe_iitype_meth_mstruc = (mstruc); \
  if (HAS_IITYPE_METH_P (_maybe_iitype_meth_mstruc, m))			   \
    IITYPE_METH (_maybe_iitype_meth_mstruc, m, args);			   \
} while (0)

MAC_DECLARE_EXTERN (struct image_instantiator_methods *,
		    mactemp_iitype_meth_or_given)

/* Call a specifier method, if it exists; otherwise return
   the specified value */

#define IITYPE_METH_OR_GIVEN(mstruc, m, args, given)			\
MAC_BEGIN								\
  MAC_DECLARE (struct image_instantiator_methods *,			\
	       mactemp_iitype_meth_or_given, mstruc)			\
  HAS_IITYPE_METH_P (mactemp_iitype_meth_or_given, m) ?			\
    IITYPE_METH (mactemp_iitype_meth_or_given, m, args) : (given)	\
MAC_END

/***** Defining new image-instantiator types *****/

#define DECLARE_IMAGE_INSTANTIATOR_TYPE(type)				     \
extern struct image_instantiator_methods * type##_image_instantiator_methods

#define DEFINE_IMAGE_INSTANTIATOR_TYPE(type)				\
struct image_instantiator_methods * type##_image_instantiator_methods

#define INITIALIZE_IMAGE_INSTANTIATOR_TYPE(type, obj_name)	\
  do {								\
    type##_image_instantiator_methods =				\
      malloc_type_and_zero (struct image_instantiator_methods);	\
    defsymbol (&Q##type, obj_name);				\
    type##_image_instantiator_methods->symbol = Q##type;	\
    type##_image_instantiator_methods->keywords =		\
      Dynarr_new (struct ii_keyword_entry);			\
    add_entry_to_image_instantiator_type_list			\
      (Q##type, type##_image_instantiator_methods);		\
  } while (0)

/* Declare that image-instantiator type TYPE has method M; used in
   initialization routines */
#define IITYPE_HAS_METHOD(type, m) \
  (type##_image_instantiator_methods->m##_method = type##_##m)

/* Declare that KEYW is a valid keyword for image-instantiator type
   TYPE.  VALIDATE_FUN if a function that returns whether the data
   is valid.  The keyword may not appear more than once. */
#define IITYPE_VALID_KEYWORD(type, keyw, validate_fun)		\
  do {								\
    struct ii_keyword_entry entry;				\
								\
    entry.keyword = keyw;					\
    entry.validate = validate_fun;				\
    entry.multiple_p = 0;					\
    Dynarr_add (type##_image_instantiator_methods->keywords,	\
		entry);						\
  } while (0)

/* Same as IITYPE_VALID_KEYWORD except that the keyword may
   appear multiple times. */
#define IITYPE_VALID_MULTI_KEYWORD(type, keyword, validate_fun)	\
  do {								\
    struct ii_keyword_entry entry;				\
								\
    entry.keyword = keyword;					\
    entry.validate = validate_fun;				\
    entry.multiple_p = 1;					\
    Dynarr_add (type##_image_instantiator_methods->keywords,	\
		entry);						\
  } while (0)

extern Lisp_Object Q_data, Q_file, Qautodetect;

extern void add_entry_to_image_instantiator_type_list (Lisp_Object symbol,
			struct image_instantiator_methods *meths);
extern Lisp_Object find_keyword_in_vector (Lisp_Object vector,
					   Lisp_Object keyword);
extern Lisp_Object find_keyword_in_vector_or_given (Lisp_Object vector,
						    Lisp_Object keyword,
						    Lisp_Object defalt);
extern int valid_string_p (Lisp_Object data, int no_error);
extern int valid_int_p (Lisp_Object data, int no_error);

/*****************************************************************************
 *                          Image Specifier Object                           *
 *****************************************************************************/

DECLARE_SPECIFIER_TYPE (image);
extern Lisp_Object Qimage;
#define XIMAGE_SPECIFIER(x) XSPECIFIER_TYPE (x, image)
#define XSETIMAGE_SPECIFIER(x, p) XSETSPECIFIER_TYPE (x, p, image)
#define IMAGE_SPECIFIERP(x) SPECIFIER_TYPEP (x, image)
#define CHECK_IMAGE_SPECIFIER(x, i) CHECK_SPECIFIER_TYPE (x, i, image)

extern void set_image_attached_to (Lisp_Object obj, Lisp_Object face,
				   Lisp_Object property);

struct image_specifier
{
  int allowed;
  Lisp_Object face;		/* face this is attached to, or nil */
  Lisp_Object face_property;	/* property of that face */
};

#define IMAGE_SPECIFIER_DATA(g) (SPECIFIER_TYPE_DATA (g, image))
#define IMAGE_SPECIFIER_ALLOWED(g) (IMAGE_SPECIFIER_DATA (g)->allowed)
#define IMAGE_SPECIFIER_FACE(g) (IMAGE_SPECIFIER_DATA (g)->face)
#define IMAGE_SPECIFIER_FACE_PROPERTY(g) \
  (IMAGE_SPECIFIER_DATA (g)->face_property)

#define XIMAGE_SPECIFIER_ALLOWED(g) \
  IMAGE_SPECIFIER_ALLOWED (XIMAGE_SPECIFIER (g))
/*****************************************************************************
 *                           Image Instance Object                           *
 *****************************************************************************/

DECLARE_LRECORD (image_instance, struct Lisp_Image_Instance);
#define XIMAGE_INSTANCE(x) \
  XRECORD (x, image_instance, struct Lisp_Image_Instance)
#define XSETIMAGE_INSTANCE(x, p) XSETRECORD (x, p, image_instance)
#define IMAGE_INSTANCEP(x) RECORDP (x, image_instance)
#define CHECK_IMAGE_INSTANCE(x, i) CHECK_RECORD (x, image_instance)

struct Lisp_Image_Instance
{
  struct lcrecord_header header;
  Lisp_Object device;
  Lisp_Object name;
  enum image_instance_type type;
  union
    {
      struct
	{
	  Lisp_Object string;
	} text;
      struct
	{
	  int width, height, depth;
	  Lisp_Object hotspot_x, hotspot_y; /* integer or Qnil */
	  Lisp_Object filename; /* string or Qnil */
	  Lisp_Object mask_filename; /* string or Qnil */
	} pixmap;
      struct
	{
	  int dummy; /* #### fill in this structure */
	} subwindow;
    } u;

  /* device-type- and image-type-specific data */
  void *data;
};

#define IMAGE_INSTANCE_DEVICE(i) ((i)->device)
#define IMAGE_INSTANCE_NAME(i) ((i)->name)
#define IMAGE_INSTANCE_TYPE(i) ((i)->type)
#define IMAGE_INSTANCE_PIXMAP_TYPE_P(i)					\
 ((IMAGE_INSTANCE_TYPE (i) == IMAGE_MONO_PIXMAP)			\
  || (IMAGE_INSTANCE_TYPE (i) == IMAGE_COLOR_PIXMAP))

#define IMAGE_INSTANCE_TEXT_STRING(i) ((i)->u.text.string)

#define IMAGE_INSTANCE_PIXMAP_WIDTH(i) ((i)->u.pixmap.width)
#define IMAGE_INSTANCE_PIXMAP_HEIGHT(i) ((i)->u.pixmap.height)
#define IMAGE_INSTANCE_PIXMAP_DEPTH(i) ((i)->u.pixmap.depth)
#define IMAGE_INSTANCE_PIXMAP_FILENAME(i) ((i)->u.pixmap.filename)
#define IMAGE_INSTANCE_PIXMAP_MASK_FILENAME(i) ((i)->u.pixmap.mask_filename)
#define IMAGE_INSTANCE_PIXMAP_HOTSPOT_X(i) ((i)->u.pixmap.hotspot_x)
#define IMAGE_INSTANCE_PIXMAP_HOTSPOT_Y(i) ((i)->u.pixmap.hotspot_y)

#define XIMAGE_INSTANCE_DEVICE(i) \
  IMAGE_INSTANCE_DEVICE (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_NAME(i) \
  IMAGE_INSTANCE_NAME (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_TYPE(i) \
  IMAGE_INSTANCE_TYPE (XIMAGE_INSTANCE (i))

#define XIMAGE_INSTANCE_TEXT_STRING(i) \
  IMAGE_INSTANCE_TEXT_STRING (XIMAGE_INSTANCE (i))

#define XIMAGE_INSTANCE_PIXMAP_WIDTH(i) \
  IMAGE_INSTANCE_PIXMAP_WIDTH (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_HEIGHT(i) \
  IMAGE_INSTANCE_PIXMAP_HEIGHT (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_DEPTH(i) \
  IMAGE_INSTANCE_PIXMAP_DEPTH (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_FILENAME(i) \
  IMAGE_INSTANCE_PIXMAP_FILENAME (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_MASK_FILENAME(i) \
  IMAGE_INSTANCE_PIXMAP_MASK_FILENAME (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_HOTSPOT_X(i) \
  IMAGE_INSTANCE_PIXMAP_HOTSPOT_X (XIMAGE_INSTANCE (i))
#define XIMAGE_INSTANCE_PIXMAP_HOTSPOT_Y(i) \
  IMAGE_INSTANCE_PIXMAP_HOTSPOT_Y (XIMAGE_INSTANCE (i))

/*****************************************************************************
 *                              Glyph Object                                 *
 *****************************************************************************/

enum glyph_type
{
  GLYPH_UNKNOWN,
  GLYPH_BUFFER,
  GLYPH_CURSOR,
  GLYPH_ICON
};

struct Lisp_Glyph
{
  struct lcrecord_header header;

  enum glyph_type type;

  /* specifiers: */
  Lisp_Object image;		/* the actual image */
  Lisp_Object contrib_p;	/* whether to figure into line height */
  Lisp_Object baseline;		/* percent above baseline */

  Lisp_Object face;		/* if non-nil, face to use when displaying */

  Lisp_Object plist;
};

DECLARE_LRECORD (glyph, struct Lisp_Glyph);
#define XGLYPH(x) XRECORD (x, glyph, struct Lisp_Glyph)
#define XSETGLYPH(x, p) XSETRECORD (x, p, glyph)
#define GLYPHP(x) RECORDP (x, glyph)
#define CHECK_GLYPH(x, i) CHECK_RECORD (x, glyph)

#define GLYPH_TYPE(g) ((g)->type)
#define GLYPH_IMAGE(g) ((g)->image)
#define GLYPH_CONTRIB_P(g) ((g)->contrib_p)
#define GLYPH_BASELINE(g) ((g)->baseline)
#define GLYPH_FACE(g) ((g)->face)

#define XGLYPH_TYPE(g) GLYPH_TYPE (XGLYPH (g))
#define XGLYPH_IMAGE(g) GLYPH_IMAGE (XGLYPH (g))
#define XGLYPH_CONTRIB_P(g) GLYPH_CONTRIB_P (XGLYPH (g))
#define XGLYPH_BASELINE(g) GLYPH_BASELINE (XGLYPH (g))
#define XGLYPH_FACE(g) GLYPH_FACE (XGLYPH (g))

extern Lisp_Object Vtruncation_glyph, Vcontinuation_glyph, Voctal_escape_glyph;
extern Lisp_Object Vcontrol_arrow_glyph, Vinvisible_text_glyph, Vhscroll_glyph;
extern Lisp_Object Vxemacs_logo;

extern unsigned short glyph_width (Lisp_Object glyph, face_index findex,
				   int framep, Lisp_Object window);
extern unsigned short glyph_ascent (Lisp_Object glyph, face_index findex,
				    int framep, Lisp_Object window);
extern unsigned short glyph_descent (Lisp_Object glyph, face_index findex,
				     int framep, Lisp_Object window);
extern unsigned short glyph_height (Lisp_Object glyph, face_index findex,
				    int framep, Lisp_Object window);
extern Lisp_Object glyph_baseline (Lisp_Object glyph, Lisp_Object domain);
extern Lisp_Object glyph_face (Lisp_Object glyph, Lisp_Object domain);
extern int glyph_contrib_p (Lisp_Object glyph, Lisp_Object domain);
extern Lisp_Object glyph_image_instance (Lisp_Object glyph,
					 Lisp_Object domain,
					 int no_error_or_quit);
extern int file_or_data_must_be_present (Lisp_Object instantiator,
					 int no_error);
extern int data_must_be_present (Lisp_Object instantiator, int no_error);
extern Lisp_Object make_string_from_file (Lisp_Object file);
extern Lisp_Object tagged_vector_to_alist (Lisp_Object vector);
extern Lisp_Object alist_to_tagged_vector (Lisp_Object tag, Lisp_Object alist);

/*****************************************************************************
 *                          Glyph Cache Elements                             *
 *****************************************************************************/

struct glyph_cache_element
{
  Lisp_Object glyph;

  unsigned int updated :1;
  unsigned short width;
  unsigned short ascent;
  unsigned short descent;
};

#define CONT_GLYPH_INDEX	(glyph_index) 0
#define TRUN_GLYPH_INDEX	(glyph_index) 1
#define HSCROLL_GLYPH_INDEX	(glyph_index) 2
#define CONTROL_GLYPH_INDEX	(glyph_index) 3
#define OCT_ESC_GLYPH_INDEX	(glyph_index) 4
#define INVIS_GLYPH_INDEX	(glyph_index) 5

#define GLYPH_CACHE_ELEMENT(window, index)				\
  Dynarr_atp (window->glyph_cache_elements, index)
#define GLYPH_CACHE_ELEMENT_GLYPH(window, index)			\
  Dynarr_atp (window->glyph_cache_elements, index)->glyph
#define GLYPH_CACHE_ELEMENT_WIDTH(window, index)			\
  Dynarr_atp (window->glyph_cache_elements, index)->width
#define GLYPH_CACHE_ELEMENT_ASCENT(window, index)			\
  Dynarr_atp (window->glyph_cache_elements, index)->ascent
#define GLYPH_CACHE_ELEMENT_DESCENT(window, index)			\
  Dynarr_atp (window->glyph_cache_elements, index)->descent

extern void mark_glyph_cache_elements (glyph_cache_element_dynarr *elements,
				       void (*markobj) (Lisp_Object));
extern void mark_glyph_cache_elements_as_not_updated (struct window *w);
extern void reset_glyph_cache_elements (struct window *w);

/*****************************************************************************
 *                             Display Tables                                *
 *****************************************************************************/

#define DISP_TABLE_SIZE	256
#define DISP_CHAR_ENTRY(dp, c)	((dp)->contents[c])

extern struct Lisp_Vector *get_display_table (struct window *, face_index);

#endif /* _XEMACS_GLYPHS_H_ */

/* Define X specific frame object for XEmacs.
   Copyright (C) 1989, 1992, 1993 1994 Free Software Foundation, Inc.
   Copyright (C) 1994, 1995 Board of Trustees, University of Illinois
   Copyright (C) 1994, 1995 Amdahl Corporation

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_FRAME_X_H_
#define _XEMACS_FRAME_X_H_

#ifdef HAVE_X_WINDOWS

#include "frame.h"

/* The maximum number of widgets that can be displayed above the text
   area at one time.  Currently no more than 3 will ever actually be
   displayed (menubar, psheet, debugger panel). */
#define MAX_CONCURRENT_TOP_WIDGETS 8

/* Each X frame object points to its own struct x_frame object
   in the display.x field.  The x_frame structure contains all
   the information that is specific to X windows.  */

struct x_frame
{
  /* The widget of this frame.  This is an EmacsShell or an
     ExternalShell. */
  Widget widget;

  /* The parent of the EmacsFrame, the menubar, and the scrollbars.
     This is an EmacsManager. */
  Widget container;

  /* The widget of the menubar, of whatever widget class it happens to be. */
  Widget menubar_widget;

  /* The widget of the edit portion of this frame; this is an EmacsFrame,
     and the window of this widget is what the redisplay code draws on. */
  Widget edit_widget;

  /* Lists the widgets above the text area, in the proper order.
     Used by the EmacsManager. */
  Widget top_widgets[MAX_CONCURRENT_TOP_WIDGETS];
  int num_top_widgets;

#ifdef ENERGIZE
  /* The Energize property-sheets.  The current_ slots are the ones which are
     actually on the frame.  The desired_ slots are the ones which should
     be there.  Redisplay synchs these.
   */
  int *current_psheets;
  int *desired_psheets;
  int current_psheet_count;
  int desired_psheet_count;
  Lisp_Object current_psheet_buffer;
  Lisp_Object desired_psheet_buffer;
#endif

  /*************************** Miscellaneous **************************/

  /* The icon pixmaps; these are Lisp_Image_Instance objects, or Qnil. */
  Lisp_Object icon_pixmap;
  Lisp_Object icon_pixmap_mask;

  /* We don't provide a mechanism for changing these are they are
     initialized so we might as well keep pointers to them and avoid
     lots of expensive calls to gc_cache_lookup. */
  GC toolbar_top_shadow_gc;
  GC toolbar_bottom_shadow_gc;
  GC toolbar_blank_background_gc;
  GC toolbar_pixmap_background_gc;

  /* geometry string that ought to be freed. */
  char *geom_free_me_please;

#ifdef I18N4
  XIC input_context;
#endif 

  /* 1 if the frame is completely visible on the display, 0 otherwise.
     if 0 the frame may have been iconified or may be totally
     or partially hidden by another X window */
  int totally_visible_p :1;

  /* NB: Both of the following flags are derivable from the 'shell'
     field above, but it's easier if we also have them separately here. */

  /* Are we a top-level frame?  This means that our shell is a
     TopLevelShell, and we should do certain things to interact with
     the window manager. */
  int top_level_frame_p :1;

#ifdef EXTERNAL_WIDGET
  /* Are we using somebody else's window for our shell window?  This
     means that our shell is an ExternalShell.  If this flag is set, then
     `top_level_frame_p' will never be set. */
  int external_window_p :1;
#endif /* EXTERNAL_WIDGET */
};

#define FRAME_X_DATA(f) ((struct x_frame *) ((f)->frame_data))

#define FRAME_X_SHELL_WIDGET(f) (FRAME_X_DATA (f)->widget)
#define FRAME_X_CONTAINER_WIDGET(f) (FRAME_X_DATA (f)->container)
#define FRAME_X_MENUBAR_WIDGET(f) (FRAME_X_DATA (f)->menubar_widget)
#define FRAME_X_TEXT_WIDGET(f) (FRAME_X_DATA (f)->edit_widget)
#define FRAME_X_TOP_WIDGETS(f) (FRAME_X_DATA (f)->top_widgets)
#define FRAME_X_NUM_TOP_WIDGETS(f) (FRAME_X_DATA (f)->num_top_widgets)

#ifdef ENERGIZE
#define FRAME_X_CURRENT_PSHEETS(f) (FRAME_X_DATA (f)->current_psheets)
#define FRAME_X_DESIRED_PSHEETS(f) (FRAME_X_DATA (f)->desired_psheets)
#define FRAME_X_CURRENT_PSHEET_COUNT(f) \
  (FRAME_X_DATA (f)->current_psheet_count)
#define FRAME_X_DESIRED_PSHEET_COUNT(f) \
  (FRAME_X_DATA (f)->desired_psheet_count)
#define FRAME_X_CURRENT_PSHEET_BUFFER(f) \
  (FRAME_X_DATA (f)->current_psheet_buffer)
#define FRAME_X_DESIRED_PSHEET_BUFFER(f) \
  (FRAME_X_DATA (f)->desired_psheet_buffer)
#endif

#define FRAME_X_ICON_PIXMAP(f) (FRAME_X_DATA (f)->icon_pixmap)
#define FRAME_X_ICON_PIXMAP_MASK(f) (FRAME_X_DATA (f)->icon_pixmap_mask)

#define FRAME_X_TOOLBAR_TOP_SHADOW_GC(f) \
  (FRAME_X_DATA (f)->toolbar_top_shadow_gc)
#define FRAME_X_TOOLBAR_BOTTOM_SHADOW_GC(f) \
  (FRAME_X_DATA (f)->toolbar_bottom_shadow_gc)
#define FRAME_X_TOOLBAR_BLANK_BACKGROUND_GC(f) \
  (FRAME_X_DATA (f)->toolbar_blank_background_gc)
#define FRAME_X_TOOLBAR_PIXMAP_BACKGROUND_GC(f) \
  (FRAME_X_DATA (f)->toolbar_pixmap_background_gc)

#define FRAME_X_GEOM_FREE_ME_PLEASE(f) (FRAME_X_DATA (f)->geom_free_me_please)

#define FRAME_X_TOTALLY_VISIBLE_P(f) (FRAME_X_DATA (f)->totally_visible_p)
#define FRAME_X_TOP_LEVEL_FRAME_P(f) (FRAME_X_DATA (f)->top_level_frame_p)

#ifdef EXTERNAL_WIDGET
#define FRAME_X_EXTERNAL_WINDOW_P(f) (FRAME_X_DATA (f)->external_window_p)
#endif

#ifdef I18N4
#define FRAME_X_INPUT_CONTEXT(f) (FRAME_X_DATA (f)->input_context)
#endif

#endif /* HAVE_X_WINDOWS */
#endif /* _XEMACS_FRAME_X_H_ */

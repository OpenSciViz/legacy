/* Define X-specific scrollbar instance.
   Copyright (C) 1994, 1995 Board of Trustees, University of Illinois

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

#ifndef _XEMACS_SCROLLBAR_X_H_
#define _XEMACS_SCROLLBAR_X_H_

#ifdef HAVE_X_WINDOWS

#include "device.h"
#include "xintrinsic.h"
#include "lwlib.h"
#include "scrollbar.h"

struct x_scrollbar_data
{
  /* Unique scrollbar identifier and name. */
  unsigned int id;
  char *name;

  /* Is set if we have already set the backing_store attribute correctly */
  char backing_store_initialized;

  /* Positioning and sizing information for scrollbar and slider. */
  scrollbar_values pos_data;

  /* Pointer to the scrollbar widget this structure describes. */
  Widget widget;

#if defined (LWLIB_SCROLLBARS_MOTIF) || defined (LWLIB_SCROLLBARS_LUCID)
  /* Recorded starting position for Motif-like scrollbar drags. */
  int vdrag_orig_value;
  Bufpos vdrag_orig_window_start;
#endif
};

#define SCROLLBAR_X_DATA(i) ((struct x_scrollbar_data *) ((i)->scrollbar_data))

#define SCROLLBAR_X_ID(i) (SCROLLBAR_X_DATA (i)->id)
#define SCROLLBAR_X_NAME(i) (SCROLLBAR_X_DATA (i)->name)
#define SCROLLBAR_X_BACKING_STORE_INITIALIZED(i) \
  (SCROLLBAR_X_DATA (i)->backing_store_initialized)
#define SCROLLBAR_X_POS_DATA(i) (SCROLLBAR_X_DATA (i)->pos_data)
#define SCROLLBAR_X_WIDGET(i) (SCROLLBAR_X_DATA (i)->widget)

#if defined (LWLIB_SCROLLBARS_MOTIF) || defined (LWLIB_SCROLLBARS_LUCID)
#define SCROLLBAR_X_VDRAG_ORIG_VALUE(i) \
  (SCROLLBAR_X_DATA (i)->vdrag_orig_value)
#define SCROLLBAR_X_VDRAG_ORIG_WINDOW_START(i) \
  (SCROLLBAR_X_DATA (i)->vdrag_orig_window_start)
#endif

extern void x_update_frame_scrollbars (struct frame *f);
extern void x_set_scrollbar_pointer (struct frame *f, Lisp_Object cursor);
extern int x_window_is_scrollbar (struct frame *f, Window win);

#endif /* HAVE_X_WINDOWS */
#endif /* _XEMACS_SCROLLBAR_H_ */

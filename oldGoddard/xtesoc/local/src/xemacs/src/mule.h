/* Basic Mule header file.
   Copyright (C) 1995 Amdahl Corporation.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Written March 1995 by Ben Wing. */

#ifndef _XEMACS_MULE_H_
#define _XEMACS_MULE_H_

/* This file can be included when we have no Mule support, but will
   have no effect. */

#ifdef MULE

#include "mule/mule-category.h"
#include "mule/mule-coding.h"

struct Lisp_Coding_System *maybe_decode_coding_system (Lisp_Object codesys);
struct Lisp_Coding_System *decode_coding_system (Lisp_Object codesys);
struct Lisp_Coding_System *decode_coding_system_variable (Lisp_Object codesys);

extern Lisp_Object Vkeyboard_coding_system, Vdisplay_coding_system;
extern Lisp_Object Vinput_coding_system, Voutput_coding_system;
extern Lisp_Object Vused_coding_system;
extern Lisp_Object Vprocess_input_coding_system, Vprocess_output_coding_system;
extern Lisp_Object Vpathname_coding_system;
extern Lisp_Object Vdefault_coding_system;

#endif /* MULE */

#endif /* _XEMACS_MULE_H_ */

/* XEmacs routines to deal with case tables.
   Copyright (C) 1987, 1992, 1993, 1994 Free Software Foundation, Inc.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: FSF 19.28. */

/* Written by Howard Gayle.  See chartab.c for details. */

#include <config.h>
#include "lisp.h"
#include "buffer.h"

Lisp_Object Qcase_table_p;
Lisp_Object Vascii_downcase_table, Vascii_upcase_table;
Lisp_Object Vascii_canon_table, Vascii_eqv_table;

static void compute_trt_inverse (Lisp_Object trt, Lisp_Object inverse);

DEFUN ("case-table-p", Fcase_table_p, Scase_table_p, 1, 1, 0,
  "Return t iff ARG is a case table.\n\
See `set-case-table' for more information on these data structures.")
  (table)
     Lisp_Object table;
{
  Lisp_Object down, up, canon, eqv;
  down = Fcar_safe (table);
  up = Fcar_safe (Fcdr_safe (table));
  canon = Fcar_safe (Fcdr_safe (Fcdr_safe (table)));
  eqv = Fcar_safe (Fcdr_safe (Fcdr_safe (Fcdr_safe (table))));

#define STRING256_P(obj) \
  (STRINGP (obj) && string_char_length (XSTRING (obj)) == 256)

  return (STRING256_P (down)
	  && (NILP (up) || STRING256_P (up))
	  && ((NILP (canon) && NILP (eqv))
	      || (STRING256_P (canon)
                  && (NILP (eqv) || STRING256_P (eqv))))
	  ? Qt : Qnil);
}

static Lisp_Object
check_case_table (Lisp_Object obj)
{
  REGISTER Lisp_Object tem;

  while (tem = Fcase_table_p (obj), NILP (tem))
    obj = wrong_type_argument (Qcase_table_p, obj);
  return (obj);
}   

DEFUN ("current-case-table", Fcurrent_case_table, Scurrent_case_table, 0, 0, 0,
  "Return the case table of the current buffer.")
  ()
{
  Lisp_Object down, up, canon, eqv;
  struct buffer *buf = current_buffer;
  
  down = buf->downcase_table;
  up = buf->upcase_table;
  canon = buf->case_canon_table;
  eqv = buf->case_eqv_table;

  return Fcons (down, Fcons (up, Fcons (canon, Fcons (eqv, Qnil))));
}

DEFUN ("standard-case-table", Fstandard_case_table,
  Sstandard_case_table, 0, 0, 0,
  "Return the standard case table.\n\
This is the one used for new buffers.")
  ()
{
  return Fcons (Vascii_downcase_table,
		Fcons (Vascii_upcase_table,
		       Fcons (Vascii_canon_table,
			      Fcons (Vascii_eqv_table, Qnil))));
}

static Lisp_Object set_case_table (Lisp_Object table, int standard);


DEFUN ("set-case-table", Fset_case_table, Sset_case_table, 1, 1, 0,
  "Select a new case table for the current buffer.\n\
A case table is a list (DOWNCASE UPCASE CANONICALIZE EQUIVALENCES)\n\
 where each element is either nil or a string of length 256.\n\
DOWNCASE maps each character to its lower-case equivalent.\n\
UPCASE maps each character to its upper-case equivalent;\n\
 if lower and upper case characters are in 1-1 correspondence,\n\
 you may use nil and the upcase table will be deduced from DOWNCASE.\n\
CANONICALIZE maps each character to a canonical equivalent;\n\
 any two characters that are related by case-conversion have the same\n\
 canonical equivalent character; it may be nil, in which case it is\n\
 deduced from DOWNCASE and UPCASE.\n\
EQUIVALENCES is a map that cyclicly permutes each equivalence class\n\
 (of characters with the same canonical equivalent); it may be nil,\n\
 in which case it is deduced from CANONICALIZE.")
  (table)
     Lisp_Object table;
{
  return set_case_table (table, 0);
}

DEFUN ("set-standard-case-table",
       Fset_standard_case_table, Sset_standard_case_table, 1, 1, 0,
  "Select a new standard case table for new buffers.\n\
See `set-case-table' for more info on case tables.")
  (table)
     Lisp_Object table;
{
  return set_case_table (table, 1);
}

static Lisp_Object
set_case_table (Lisp_Object table, int standard)
{
  Lisp_Object down, up, canon, eqv;
  struct buffer *buf = current_buffer;

  check_case_table (table);

  down = Fcar_safe (table);
  up = Fcar_safe (Fcdr_safe (table));
  canon = Fcar_safe (Fcdr_safe (Fcdr_safe (table)));
  eqv = Fcar_safe (Fcdr_safe (Fcdr_safe (Fcdr_safe (table))));

  if (NILP (up))
    {
      up = Fmake_string (make_number (256), make_number (0));
      compute_trt_inverse (down, up);
    }

  if (NILP (canon))
    {
      REGISTER Charcount i;

      canon = Fmake_string (make_number (256), make_number (0));

      /* Set up the CANON vector; for each character,
	 this sequence of upcasing and downcasing ought to
	 get the "preferred" lowercase equivalent.  */
      for (i = 0; i < 256; i++)
	set_string_char (XSTRING (canon), i,
			 string_char
			 (XSTRING (down),
			  (Charcount) string_char
			  (XSTRING (up),
			   (Charcount) string_char (XSTRING (down), i))));
    }

  if (NILP (eqv))
    {
      eqv = Fmake_string (make_number (256), make_number (0));

      compute_trt_inverse (canon, eqv);
    }

  if (standard)
    {
      Vascii_downcase_table = down;
      Vascii_upcase_table = up;
      Vascii_canon_table = canon;
      Vascii_eqv_table = eqv;
    }
  else
    {
      buf->downcase_table = down;
      buf->upcase_table = up;
      buf->case_canon_table = canon;
      buf->case_eqv_table = eqv;
    }
  return table;
}

/* Given a translate table TRT, store the inverse mapping into INVERSE.
   Since TRT is not one-to-one, INVERSE is not a simple mapping.
   Instead, it divides the space of characters into equivalence classes.
   All characters in a given class form one circular list, chained through
   the elements of INVERSE.  */

static void
compute_trt_inverse (Lisp_Object trt, Lisp_Object inverse)
{
  Charcount i = 0400;
  Emchar c, q;

  while (--i)
    set_string_char (XSTRING (inverse), i, (Emchar) i);
  i = 0400;
  while (--i)
    {
      if ((q = string_char (XSTRING (trt), i)) != (Emchar) i)
	{
	  c = string_char (XSTRING (inverse), (Charcount) q);
	  set_string_char (XSTRING (inverse), (Charcount) q, (Emchar) i);
	  set_string_char (XSTRING (inverse), i, c);
	}
    }
}


void
syms_of_casetab (void)
{
  defsymbol (&Qcase_table_p, "case-table-p");

  defsubr (&Scase_table_p);
  defsubr (&Scurrent_case_table);
  defsubr (&Sstandard_case_table);
  defsubr (&Sset_case_table);
  defsubr (&Sset_standard_case_table);
}

void
vars_of_casetab (void)
{
  REGISTER Emchar i;
  Lisp_Object tem;

  staticpro (&Vascii_downcase_table);
  staticpro (&Vascii_upcase_table);
  staticpro (&Vascii_canon_table);
  staticpro (&Vascii_eqv_table);

#if 0
  DEFVAR_LISP ("ascii-downcase-table", &Vascii_downcase_table,
	       "String mapping ASCII characters to lowercase equivalents.");
  DEFVAR_LISP ("ascii-upcase-table", &Vascii_upcase_table,
	       "String mapping ASCII characters to uppercase equivalents.");
#endif

  tem = Fmake_string (make_number (256), make_number (0));
  Vascii_downcase_table = tem;
  Vascii_canon_table = tem;
  
  for (i = 0; i < 256; i++)
    set_string_char (XSTRING (tem), (Charcount) i, tolower (i));
  
  tem = Fmake_string (make_number (256), make_number (0));
  Vascii_upcase_table = tem;
  Vascii_eqv_table = tem;
  
  for (i = 0; i < 256; i++)
    set_string_char (XSTRING (tem), (Charcount) i, (isupper (i)
						    ? tolower (i)
						    : (islower (i)
						       ? toupper (i)
						       : i)));
}

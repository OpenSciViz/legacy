/* Synched up with: FSF 19.29. */

#include "aix3-2-5.h"

#define AIX4

/* AIX 4 does not have HFT any more.  */
#undef AIXHFT

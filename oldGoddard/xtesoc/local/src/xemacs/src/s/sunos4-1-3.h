/* Synched up with: FSF 19.29. */

/* XEmacs change: FSFmacs changes to include sunos4shr.h in 19.29.
   We handle shared libraries differently. */
#include "sunos4-1.h"

/* TERMIOS is broken under SunOS??

   Someone says: This causes failure in process_send_signal (tcgetattr
   loses) and may also cause hanging at Emacs startup when parent is
   not a job control shell.  */
/* murray@chemical-eng.edinburgh.ac.uk says this works, and avoids
   the problem of spurious ^M in subprocess output.  */
#undef HAVE_TERMIOS
#if 0
/* This enables some #undefs in systty.h.  */
#define BSD_TERMIOS
#endif

#if 0
/* jik@gza.com says this works now.  */
/* The bug that corrupts GNU malloc's memory pool is fixed in SunOS 4.1.3. */

#undef SYSTEM_MALLOC

/* barrie@calvin.demon.co.uk says memmove is missing.  */
#ifndef SYSTEM_MALLOC
#define MEMMOVE_MISSING
#endif
#endif /* 0 */

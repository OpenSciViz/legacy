/* Synched up with: FSF 19.29. */

/* System description file for hpux version 9.  */

#include "hpux8.h"

#define HPUX9

/* If Emacs doesn't seem to work when built to use GNU malloc, you
   probably need to get the latest patches to the HP/UX compiler.
   See `etc/MACHINES' for more information.  */
#if 0
#define SYSTEM_MALLOC 1
#undef GNU_MALLOC
#undef REL_ALLOC
#endif

#ifndef __GNUC__
/* Make room for enough symbols, so dispnew.c does not fail.  */
/* XEmacs: cognot@ensg.u-nancy.fr: C_SWITCH_SYSTEM already defined in hpux8.h,
                           -D_BSD makes hp CC choke on process.c
#define C_SWITCH_SYSTEM -Wp,-H200000 -D_BSD
*/
#undef C_SWITCH_SYSTEM
#define C_SWITCH_SYSTEM -Ae -Wp,-H100000
/* XEmacs: commented out
#else
#define C_SWITCH_SYSTEM -D_BSD
*/
#endif

/* neal@ctd.comsat.com */
#define NO_TERMIO

/* According to ngorelic@speclab.cr.usgs.gov,
   references to the X11R4 directoriess in these variables
   (inherited from hpux8.h)
   cause the wrong libraries to be found,
   and the options to specify the X11R5 directories are unnecessary
   since the R5 files are found without them.  */
#undef LIB_X11_LIB
#undef C_SWITCH_X_SYSTEM
#undef LD_SWITCH_X_SYSTEM
/* However, HPUX 9 has Motif includes in a strange place.
   So search that place.  */
/* XEmacs change: add X11R5, change LD_SWITCH_X_DEFAULT to
   LD_SWITCH_X_SYSTEM.  #### Why do we need to make these changes? */
#define C_SWITCH_X_SYSTEM -I/usr/include/X11R5 -I/usr/include/Motif1.2
#define LD_SWITCH_X_SYSTEM -L/usr/lib/X11R5 -L/usr/lib/Motif1.2

/* XEmacs: apparently rint() is totally broken in HPUX 9. */
#undef HAVE_RINT


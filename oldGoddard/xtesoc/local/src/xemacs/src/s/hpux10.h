/* Synched up with: FSF 19.29. */

/* System description file for hpux version 9.  */

#include "hpux9.h"

#define HPUX10

#undef random
#undef srandom

/* New paths for hpux 10 */
#undef C_SWITCH_X_SYSTEM
#define C_SWITCH_X_SYSTEM -I/usr/include/X11R5 -I/usr/include/Motif1.2 -I/opt/audio/include
#undef LD_SWITCH_X_SYSTEM
#define LD_SWITCH_X_SYSTEM -L/usr/lib/X11R5 -L/usr/lib/Motif1.2 -L/opt/audio/lib

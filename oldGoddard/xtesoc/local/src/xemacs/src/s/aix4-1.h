/* Synched up with: FSF 19.29. */

#define AIX4_1 

#include "aix4.h"

#if 0 /* Tomotake FURUHATA <furuhata@trl.ibm.co.jp> says this is needed
	 in Mule, but we don't know why.  Anyway, it's not needed now.  */
#define SYSTEM_MALLOC
#endif

/* XEmacs change suggested by Butch Anton <butch@taligent.com> */
#define SIGNALS_VIA_CHARACTERS

/* System description file for SCO OpenServer Release 5
   Copyright (C) 1993, 1994 Free Software Foundation, Inc.

This file is part of GNU Emacs.

GNU Emacs is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU Emacs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* Synched up with: Not in FSF. */

/* Changes for SCO OpenServer 5 by Robert Lipe, robertl@arnet.com */

#include "sco4.h"

#undef static         /* This would terrorize a lot of our headers */

#undef LIB_X11_LIB
#define LIB_X11_LIB -lX11 -lgen

#ifndef MAXPATHLEN
#  define MAXPATHLEN PATHSIZE
#endif
#define ANSICPP 1

#undef ADDR_CORRECT
#define ADDR_CORRECT(x) (int)((char *)(x) - (char*)0)

#undef START_FILES
#define START_FILES pre-crt0.o

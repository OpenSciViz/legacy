/* Synched up with: FSF 19.29. */

#include "irix5-3.h"

/* Irix 6 tries to do 64 bits, but doesn't do it fully,
   so inhibit that.  */
#define IRIX_FORCE_32_BITS

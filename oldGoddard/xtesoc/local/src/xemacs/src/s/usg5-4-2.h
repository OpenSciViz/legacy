/* Synched up with: FSF 19.29. */

/* s/ file for System V release 4.2.  */

#include "usg5-4.h"

/* Motif needs -lgen.  */
#undef LIBS_SYSTEM
#define LIBS_SYSTEM -lsocket -lnsl -lelf -lgen

/* Use libw.a along with X11R6 Xt.  */
#define NEED_LIBW

/* XEmacs change (since getwd is auto-determined) */
#undef HAVE_GETWD  /* (appears to be buggy on SVR4.2) */

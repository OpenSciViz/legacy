/* Synched up with: Not in FSF. */

#include "decosf1-3.h"

#ifdef THIS_IS_YMAKEFILE
/* This to get rid of the def in decosf1-3 forcing dynamic linking. */
#undef LD_SWITCH_SYSTEM
#define LD_SWITCH_SYSTEM -non_shared
#endif

/* Various initialization function prototypes.
   Copyright (C) 1995 Board of Trustees, University of Illinois

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifndef _XEMACS_SYMSINIT_H_
#define _XEMACS_SYMSINIT_H_

/* Earliest environment initializations (dump-time and run-time). */

extern void init_data_very_early (void);
extern void init_floatfns_very_early (void);
extern void init_free_hook (void);
extern void init_intl_very_early (void);
extern void init_ralloc (void);
extern void init_signals_very_early (void);

/* Early Lisp-engine initialization (dump-time only). */

extern void init_alloc_once_early (void);
extern void init_symbols_once_early (void);
extern void init_errors_once_early (void);

/* Declare the built-in symbols and primitives (dump-time only). */

extern void syms_of_abbrev (void);
extern void syms_of_alloc (void);
extern void syms_of_buffer (void);
extern void syms_of_bytecode (void);
extern void syms_of_callint (void);
extern void syms_of_callproc (void);
extern void syms_of_casefiddle (void);
extern void syms_of_casetab (void);
extern void syms_of_cmds (void);
extern void syms_of_data (void);
extern void syms_of_debug (void);
extern void syms_of_device_tty (void);
extern void syms_of_device_x (void);
extern void syms_of_device (void);
extern void syms_of_dired (void);
extern void syms_of_doc (void);
extern void syms_of_editfns (void);
extern void syms_of_elhash (void);
extern void syms_of_emacs (void);
extern void syms_of_energize (void);
extern void syms_of_epoch (void);
extern void syms_of_eval (void);
extern void syms_of_event_stream (void);
extern void syms_of_event_Xt (void);
extern void syms_of_events (void);
extern void syms_of_extents (void);
extern void syms_of_faces (void);
extern void syms_of_fileio (void);
extern void syms_of_filelock (void);
extern void syms_of_floatfns (void);
extern void syms_of_fns (void);
extern void syms_of_font_lock (void);
extern void syms_of_frame_x (void);
extern void syms_of_frame (void);
extern void syms_of_free_hook (void);
extern void syms_of_general (void);
extern void syms_of_glyphs_x (void);
extern void syms_of_glyphs (void);
extern void syms_of_indent (void);
extern void syms_of_intl (void);
extern void syms_of_keyboard (void);
extern void syms_of_keymap (void);
extern void syms_of_lread (void);
extern void syms_of_lstream (void);
extern void syms_of_macros (void);
extern void syms_of_marker (void);
extern void syms_of_md5 (void);
extern void syms_of_menubar_x (void);
extern void syms_of_menubar (void);
extern void syms_of_minibuf (void);
extern void syms_of_mocklisp (void);
extern void syms_of_objects_tty (void);
extern void syms_of_objects_x (void);
extern void syms_of_objects (void);
extern void syms_of_print (void);
extern void syms_of_process (void);
extern void syms_of_ralloc (void);
extern void syms_of_redisplay (void);
extern void syms_of_scrollbar (void);
extern void syms_of_search (void);
extern void syms_of_signal (void);
extern void syms_of_sound (void);
extern void syms_of_specifier (void);
extern void syms_of_sunpro (void);
extern void syms_of_symbols (void);
extern void syms_of_syntax (void);
extern void syms_of_toolbar (void);
extern void syms_of_tooltalk (void);
extern void syms_of_undo (void);
extern void syms_of_vmsfns (void);
extern void syms_of_vmsproc (void);
extern void syms_of_window (void);
extern void syms_of_xselect (void);

/* Initialize the device types (dump-time only). */

extern void device_type_create (void);
extern void device_type_create_stream (void);
extern void device_type_create_tty (void);
extern void device_type_create_frame_tty (void);
extern void device_type_create_objects_tty (void);
extern void device_type_create_redisplay_tty (void);
extern void device_type_create_x (void);
extern void device_type_create_frame_x (void);
extern void device_type_create_glyphs_x (void);
extern void device_type_create_menubar_x (void);
extern void device_type_create_objects_x (void);
extern void device_type_create_redisplay_x (void);
extern void device_type_create_scrollbar_x (void);
extern void device_type_create_toolbar_x (void);

/* Initialize the specifier types (dump-time only). */

extern void specifier_type_create (void);
extern void specifier_type_create_image (void);
extern void specifier_type_create_objects (void);
extern void specifier_type_create_toolbar (void);

/* Initialize the image instantiator types (dump-time only). */

extern void image_instantiator_type_create (void);
extern void image_instantiator_type_create_glyphs_x (void);

/* Allow for Fprovide() (dump-time only). */

extern void init_provide_once (void);

/* Initialize most variables (dump-time only). */

extern void vars_of_abbrev (void);
extern void vars_of_alloc (void);
extern void vars_of_buffer (void);
extern void vars_of_bytecode (void);
extern void vars_of_callint (void);
extern void vars_of_callproc (void);
extern void vars_of_casetab (void);
extern void vars_of_cmds (void);
extern void vars_of_debug (void);
extern void vars_of_device_stream (void);
extern void vars_of_device_tty (void);
extern void vars_of_device (void);
extern void vars_of_dired (void);
extern void vars_of_doc (void);
extern void vars_of_editfns (void);
extern void vars_of_elhash (void);
extern void vars_of_emacs (void);
extern void vars_of_eval (void);
extern void vars_of_event_stream (void);
extern void vars_of_event_tty (void);
extern void vars_of_events (void);
extern void vars_of_extents (void);
extern void vars_of_faces (void);
extern void vars_of_fileio (void);
extern void vars_of_filelock (void);
extern void vars_of_floatfns (void);
extern void vars_of_font_lock (void);
extern void vars_of_frame_tty (void);
extern void vars_of_frame (void);
extern void vars_of_glyphs (void);
extern void vars_of_indent (void);
extern void vars_of_insdel (void);
extern void vars_of_intl (void);
extern void vars_of_keyboard (void);
extern void vars_of_keymap (void);
extern void vars_of_lread (void);
extern void vars_of_macros (void);
extern void vars_of_md5 (void);
extern void vars_of_menubar (void);
extern void vars_of_minibuf (void);
extern void vars_of_mocklisp (void);
extern void vars_of_print (void);
extern void vars_of_process (void);
extern void vars_of_ralloc (void);
extern void vars_of_redisplay (void);
extern void vars_of_search (void);
extern void vars_of_sound (void);
extern void vars_of_specifier (void);
extern void vars_of_symbols (void);
extern void vars_of_syntax (void);
extern void vars_of_toolbar (void);
extern void vars_of_undo (void);
extern void vars_of_window (void);
extern void vars_of_device_x (void);
extern void vars_of_event_Xt (void);
extern void vars_of_frame_x (void);
extern void vars_of_glyphs_x (void);
extern void vars_of_menubar_x (void);
extern void vars_of_objects_x (void);
extern void vars_of_xselect (void);
extern void vars_of_epoch (void);
extern void vars_of_scrollbar_x (void);
extern void vars_of_energize (void);
extern void vars_of_tooltalk (void);
extern void vars_of_sunpro (void);

/* Initialize specifier variables (dump-time only). */

extern void specifier_vars_of_glyphs (void);
extern void specifier_vars_of_menubar (void);
extern void specifier_vars_of_redisplay (void);
extern void specifier_vars_of_scrollbar (void);
extern void specifier_vars_of_toolbar (void);
extern void specifier_vars_of_window (void);

/* Initialize variables with complex dependencies
   on other variables (dump-time only). */

extern void complex_vars_of_glyphs (void);
extern void complex_vars_of_glyphs_x (void);
extern void complex_vars_of_energize (void);
extern void complex_vars_of_syntax (void);
extern void complex_vars_of_buffer (void);
extern void complex_vars_of_minibuf (void);
extern void complex_vars_of_event_stream (void);
extern void complex_vars_of_callproc (void);
extern void complex_vars_of_filelock (void);
extern void complex_vars_of_keymap (void);

/* Reset the Lisp engine (run-time only). */

extern void reinit_alloc (void);
extern void reinit_eval (void);
extern void reinit_mule_category (void);

/* Late initialization -- stuff pertaining only to interactive usage,
   I/O, or Lisp reading. (Dump-time and run-time.) */

extern void init_buffer (void);
extern void init_callproc (void);
extern void init_device_stream (void);
extern void init_device_tty (void);
extern void init_dosfns (void);
extern void init_editfns (void);
extern void init_environment (void);
extern void init_event_Xt_late (void);
extern void init_event_stream (void);
extern void init_event_tty_late (void);
extern void init_faces (void);
extern void init_keyboard (void);
extern void init_lread (void);
extern void init_macros (void);
/* Not named init_process in order to avoid conflict with NS 3.3 */
extern void init_xemacs_process (void);
extern void init_redisplay (void);
extern void init_sunpro (void);
extern void init_vms_input (void);
extern void init_vmsfns (void);
extern void init_vmsproc (void);

#endif /* _XEMACS_SYMSINIT_H_ */

This is Info file ../../info/lispref.info, produced by Makeinfo-1.63
from the input file lispref.texi.

   Edition History:

   GNU Emacs Lisp Reference Manual Second Edition (v2.01), May 1993 GNU
Emacs Lisp Reference Manual Further Revised (v2.02), August 1993 Lucid
Emacs Lisp Reference Manual (for 19.10) First Edition, March 1994
XEmacs Lisp Programmer's Manual (for 19.12) Second Edition, April 1995
GNU Emacs Lisp Reference Manual v2.4, June 1995 XEmacs Lisp
Programmer's Manual (for 19.13) Third Edition, July 1995

   Copyright (C) 1990, 1991, 1992, 1993, 1994, 1995 Free Software
Foundation, Inc.  Copyright (C) 1994, 1995 Sun Microsystems, Inc.
Copyright (C) 1995 Amdahl Corporation.  Copyright (C) 1995 Ben Wing.

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided that the
entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Foundation.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided also
that the section entitled "GNU General Public License" is included
exactly as in the original, and provided that the entire resulting
derived work is distributed under the terms of a permission notice
identical to this one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that the section entitled "GNU General Public License"
may be included in a translation approved by the Free Software
Foundation instead of in the original English.


Indirect:
lispref.info-1: 2070
lispref.info-2: 43774
lispref.info-3: 89826
lispref.info-4: 136920
lispref.info-5: 186781
lispref.info-6: 230636
lispref.info-7: 277107
lispref.info-8: 321069
lispref.info-9: 368958
lispref.info-10: 418919
lispref.info-11: 464774
lispref.info-12: 513367
lispref.info-13: 562874
lispref.info-14: 611287
lispref.info-15: 659980
lispref.info-16: 706047
lispref.info-17: 755506
lispref.info-18: 805370
lispref.info-19: 848703
lispref.info-20: 896751
lispref.info-21: 942180
lispref.info-22: 987519
lispref.info-23: 1036815
lispref.info-24: 1086333
lispref.info-25: 1134908
lispref.info-26: 1182366
lispref.info-27: 1223614
lispref.info-28: 1273610
lispref.info-29: 1321524
lispref.info-30: 1371177
lispref.info-31: 1420101
lispref.info-32: 1469314
lispref.info-33: 1518868
lispref.info-34: 1565714
lispref.info-35: 1610947
lispref.info-36: 1660766
lispref.info-37: 1707555
lispref.info-38: 1742639

Tag Table:
(Indirect)
Node: Top2070
Node: Copying43774
Node: Introduction62932
Node: Caveats64514
Node: Lisp History66165
Node: Conventions67419
Node: Some Terms68234
Node: nil and t68955
Node: Evaluation Notation70631
Node: Printing Notation71558
Node: Error Messages72432
Node: Buffer Text Notation72873
Node: Format of Descriptions73748
Node: A Sample Function Description74605
Node: A Sample Variable Description78590
Node: Acknowledgements79498
Node: Lisp Data Types81284
Node: Printed Representation83657
Node: Comments85698
Node: Programming Types86597
Node: Integer Type88200
Node: Floating Point Type89207
Node: Character Type89826
Node: Symbol Type96826
Node: Sequence Type99524
Node: Cons Cell Type100955
Node: Dotted Pair Notation105438
Node: Association List Type107562
Node: Array Type108445
Node: String Type109811
Node: Vector Type112927
Node: Function Type113697
Node: Macro Type114809
Node: Primitive Function Type115506
Node: Byte-Code Type117025
Node: Autoload Type117642
Node: Editing Types118624
Node: Buffer Type119553
Node: Marker Type121547
Node: Window Type122271
Node: Frame Type123479
Node: Window Configuration Type124166
Node: Process Type124710
Node: Stream Type125669
Node: Keymap Type126794
Node: Syntax Table Type127254
Node: Display Table Type128227
Node: Extent Type128587
Node: Window-System Types129189
Node: Font Type129529
Node: Color Type129635
Node: Pixmap Type129763
Node: Type Predicates129875
Node: Equality Predicates136920
Node: Numbers140319
Node: Integer Basics141635
Node: Float Basics143993
Node: Predicates on Numbers145733
Node: Comparison of Numbers147369
Node: Numeric Conversions150760
Node: Arithmetic Operations152226
Node: Rounding Operations157536
Node: Bitwise Operations158641
Node: Math Functions167574
Node: Random Numbers169519
Node: Strings and Characters171284
Node: String Basics172385
Node: Predicates for Strings175113
Node: Creating Strings175673
Node: Text Comparison180447
Node: String Conversion183504
Node: Formatting Strings186781
Node: Character Case192675
Node: Case Table195570
Node: Lists199448
Node: Cons Cells200409
Node: Lists as Boxes201745
Node: List-related Predicates204387
Node: List Elements206089
Node: Building Lists209110
Node: Modifying Lists215105
Node: Setcar215917
Node: Setcdr218338
Node: Rearrangement220848
Node: Sets And Lists226407
Node: Association Lists230636
Node: Sequences Arrays Vectors238210
Node: Sequence Functions240380
Node: Arrays243633
Node: Array Functions246031
Node: Vectors248217
Node: Vector Functions249737
Node: Symbols252242
Node: Symbol Components253297
Node: Definitions257471
Node: Creating Symbols259697
Node: Property Lists266731
Node: Plists and Alists268149
Node: Symbol Plists269896
Node: Other Plists271703
Node: Evaluation272811
Node: Intro Eval273615
Node: Eval277107
Node: Forms281509
Node: Self-Evaluating Forms282666
Node: Symbol Forms284183
Node: Classifying Lists285100
Node: Function Indirection285856
Node: Function Forms288971
Node: Macro Forms289970
Node: Special Forms291569
Node: Autoloading293801
Node: Quoting294299
Node: Control Structures295662
Node: Sequencing297283
Node: Conditionals300148
Node: Combining Conditions303570
Node: Iteration306840
Node: Nonlocal Exits308624
Node: Catch and Throw309324
Node: Examples of Catch313167
Node: Errors315185
Node: Signaling Errors316673
Node: Processing of Errors319739
Node: Handling Errors321069
Node: Error Symbols327975
Node: Cleanups331421
Node: Variables335201
Node: Global Variables336909
Node: Constant Variables337985
Node: Local Variables338508
Node: Void Variables343451
Node: Defining Variables346962
Node: Accessing Variables354126
Node: Setting Variables355552
Node: Variable Scoping360070
Node: Scope361667
Node: Extent363190
Node: Impl of Scope364665
Node: Using Scoping366626
Node: Buffer-Local Variables368148
Node: Intro to Buffer-Local368958
Node: Creating Buffer-Local371501
Node: Default Value376713
Node: Functions379857
Node: What Is a Function380951
Node: Lambda Expressions384961
Node: Lambda Components385870
Node: Simple Lambda387702
Node: Argument List389361
Node: Function Documentation393088
Node: Function Names395030
Node: Defining Functions397604
Node: Calling Functions400540
Node: Mapping Functions404389
Node: Anonymous Functions406741
Node: Function Cells409985
Node: Inline Functions414772
Node: Related Topics416582
Node: Macros417635
Node: Simple Macro418919
Node: Expansion419654
Node: Compiling Macros422628
Node: Defining Macros424466
Node: Backquote425784
Node: Problems with Macros428127
Node: Argument Evaluation428822
Node: Surprising Local Vars431726
Node: Eval During Expansion433794
Node: Repeated Expansion435488
Node: Loading437399
Node: How Programs Do Loading439072
Node: Autoload445250
Node: Repeated Loading451344
Node: Named Features453461
Node: Unloading458107
Node: Hooks for Loading460244
Node: Byte Compilation461912
Node: Speed of Byte-Code463689
Node: Compilation Functions464774
Node: Docs and Compilation471013
Node: Dynamic Loading473590
Node: Eval During Compile475955
Node: Byte-Code Objects477212
Node: Disassembly479695
Node: Debugging488142
Node: Debugger489551
Node: Error Debugging490696
Node: Infinite Loops492698
Node: Function Debugging493942
Node: Explicit Debug496725
Node: Using Debugger497497
Node: Debugger Commands499360
Node: Invoking the Debugger503677
Node: Internals of Debugger507592
Node: Syntax Errors512119
Node: Excess Open513367
Node: Excess Close515238
Node: Compilation Errors516655
Node: Edebug517943
Node: Using Edebug519925
Node: Instrumenting522886
Node: Edebug Execution Modes525270
Node: Jumping528162
Node: Edebug Misc531151
Node: Breakpoints532538
Node: Global Break Condition535277
Node: Persistent Breakpoints536234
Node: Views537137
Node: Edebug Eval538728
Node: Eval List539490
Node: Printing542899
Node: Coverage Testing543858
Node: The Outside Context545609
Node: Just Checking546317
Node: Outside Window Configuration546800
Node: Edebug Recursive Edit548851
Node: Side Effects549609
Node: Macro Calls550791
Node: Specification List552563
Node: Backtracking561066
Node: Specification Examples562874
Node: Edebug Options565946
Node: Read and Print569954
Node: Streams Intro570931
Node: Input Streams572949
Node: Input Functions578039
Node: Output Streams580099
Node: Output Functions584146
Node: Output Variables588446
Node: Minibuffers590388
Node: Intro to Minibuffers591473
Node: Text from Minibuffer593881
Node: Object from Minibuffer598536
Node: Minibuffer History601731
Node: Completion604307
Node: Basic Completion606280
Node: Minibuffer Completion611287
Node: Completion Commands614512
Node: High-Level Completion619148
Node: Reading File Names623162
Node: Programmed Completion626842
Node: Yes-or-No Queries629052
Node: Multiple Queries634733
Node: Minibuffer Misc638792
Node: Command Loop643294
Node: Command Overview644640
Node: Defining Commands647924
Node: Using Interactive648672
Node: Interactive Codes653130
Node: Interactive Examples658666
Node: Interactive Call659980
Node: Command Loop Info665384
Node: Events670171
Node: Event Types671631
Node: Event Contents673530
Node: Event Predicates676907
Node: Accessing Mouse Event Positions678109
Node: Frame-Level Event Position Info678785
Node: Window-Level Event Position Info679894
Node: Event Text Position Info681603
Node: Event Glyph Position Info684031
Node: Event Toolbar Position Info685311
Node: Other Event Position Info685961
Node: Accessing Other Event Info686358
Node: Working With Events687966
Node: Converting Events689384
Node: Reading Input692315
Node: Key Sequence Input693317
Node: Reading One Event695273
Node: Dispatching an Event698034
Node: Quoted Character Input698485
Node: Peeking and Discarding699835
Node: Waiting703739
Node: Quitting706047
Node: Prefix Command Arguments710455
Node: Recursive Editing715542
Node: Disabling Commands720339
Node: Command History722406
Node: Keyboard Macros724144
Node: Keymaps726361
Node: Keymap Terminology727896
Node: Format of Keymaps730824
Node: Creating Keymaps731235
Node: Inheritance and Keymaps732566
Node: Prefix Keys734211
Node: Active Keymaps737802
Node: Key Lookup744680
Node: Functions for Key Lookup750208
Node: Changing Key Bindings755506
Node: Key Binding Commands762878
Node: Scanning Keymaps764939
Node: Menus769226
Node: Menu Format769702
Node: Menubar Format777698
Node: Menubar778323
Node: Modifying Menus781437
Node: Menu Filters786455
Node: Pop-Up Menus788345
Node: Buffers Menu790551
Node: Dialog Boxes791840
Node: Dialog Box Format792007
Node: Dialog Box Functions793377
Node: Toolbar793775
Node: Toolbar Intro794089
Node: Toolbar Descriptor Format794236
Node: Specifying the Toolbar797978
Node: Other Toolbar Variables800692
Node: Scrollbars801382
Node: Modes801510
Node: Major Modes802458
Node: Major Mode Conventions805370
Node: Example Major Modes811325
Node: Auto Major Mode819293
Node: Mode Help826742
Node: Derived Modes827843
Node: Minor Modes830034
Node: Minor Mode Conventions831336
Node: Keymaps and Minor Modes834200
Node: Modeline Format835035
Node: Modeline Data836804
Node: Modeline Variables841076
Node: %-Constructs845792
Node: Hooks848703
Node: Documentation855517
Node: Documentation Basics856877
Node: Accessing Documentation859722
Node: Keys in Documentation865308
Node: Describing Characters867740
Node: Help Functions870014
Node: Files877948
Node: Visiting Files879872
Node: Visiting Functions881377
Node: Subroutines of Visiting886425
Node: Saving Buffers888501
Node: Reading from Files894594
Node: Writing to Files896751
Node: File Locks899428
Node: Information about Files902477
Node: Testing Accessibility903238
Node: Kinds of Files906978
Node: Truenames908659
Node: File Attributes909661
Node: Changing File Attributes914772
Node: File Names920178
Node: File Name Components921713
Node: Directory Names924814
Node: Relative File Names928526
Node: File Name Expansion929604
Node: Unique File Names933989
Node: File Name Completion935130
Node: Contents of Directories937762
Node: Create/Delete Dirs941075
Node: Magic File Names942180
Node: Partial Files947810
Node: Intro to Partial Files948038
Node: Creating a Partial File949278
Node: Detached Partial Files950213
Node: Format Conversion951335
Node: Files and MS-DOS957845
Node: Backups and Auto-Saving959909
Node: Backup Files960584
Node: Making Backups961981
Node: Rename or Copy964730
Node: Numbered Backups967223
Node: Backup Names969468
Node: Auto-Saving972751
Node: Reverting980893
Node: Buffers984051
Node: Buffer Basics985466
Node: Current Buffer987519
Node: Buffer Names992207
Node: Buffer File Name995412
Node: Buffer Modification999532
Node: Modification Time1001728
Node: Read Only Buffers1005103
Node: The Buffer List1007521
Node: Creating Buffers1012026
Node: Killing Buffers1014175
Node: Indirect Buffers1017908
Node: Windows1019735
Node: Basic Windows1021213
Node: Splitting Windows1024310
Node: Deleting Windows1031184
Node: Selecting Windows1033865
Node: Cyclic Window Ordering1036815
Node: Buffers and Windows1041439
Node: Displaying Buffers1043283
Node: Choosing Window1047888
Node: Window Point1055606
Node: Window Start1057652
Node: Vertical Scrolling1062142
Node: Horizontal Scrolling1067849
Node: Size of Window1071358
Node: Position of Window1075003
Node: Resizing Windows1076835
Node: Window Configurations1081360
Node: Frames1084635
Node: Creating Frames1086333
Node: Frame Parameters1087492
Node: Parameter Access1088339
Node: Initial Parameters1089038
Node: X Frame Parameters1091166
Node: Size and Position1095416
Node: Frame Name1097413
Node: Frame Titles1098327
Node: Deleting Frames1100149
Node: Finding All Frames1100749
Node: Frames and Windows1102743
Node: Minibuffers and Frames1104448
Node: Input Focus1105427
Node: Visibility of Frames1107490
Node: Raising and Lowering1109403
Node: Frame Hooks1111771
Node: Devices1113578
Node: Basic Device Functions1115260
Node: Device Types and Classes1115923
Node: Connecting to a Device1118070
Node: The Selected Device1120039
Node: Device Input1120372
Node: Positions1121108
Node: Point1122064
Node: Motion1124828
Node: Character Motion1125595
Node: Word Motion1127645
Node: Buffer End Motion1129005
Node: Text Lines1130502
Node: Screen Lines1134908
Node: List Motion1137916
Node: Skipping Characters1140746
Node: Excursions1142905
Node: Narrowing1145127
Node: Markers1150326
Node: Overview of Markers1151228
Node: Predicates on Markers1154115
Node: Creating Markers1154987
Node: Information from Markers1158003
Node: Changing Markers1159101
Node: The Mark1160479
Node: The Region1167377
Node: Text1168547
Node: Near Point1171204
Node: Buffer Contents1174940
Node: Comparing Text1177437
Node: Insertion1178845
Node: Commands for Insertion1182366
Node: Deletion1185324
Node: User-Level Deletion1188861
Node: The Kill Ring1192991
Node: Kill Ring Concepts1195165
Node: Kill Functions1196219
Node: Yank Commands1198124
Node: Low-Level Kill Ring1199995
Node: Internals of Kill Ring1202781
Node: Undo1205561
Node: Maintaining Undo1209890
Node: Filling1212510
Node: Margins1218505
Node: Auto Filling1222433
Node: Sorting1223614
Node: Columns1232915
Node: Indentation1235431
Node: Primitive Indent1236210
Node: Mode-Specific Indent1237454
Node: Region Indent1239951
Node: Relative Indent1242899
Node: Indent Tabs1245281
Node: Motion by Indent1246601
Node: Case Changes1247380
Node: Text Properties1250630
Node: Examining Properties1252576
Node: Changing Properties1254593
Node: Property Search1258168
Node: Special Properties1262864
Node: Saving Properties1263145
Node: Substitution1266287
Node: Registers1268161
Node: Transposition1270704
Node: Change Hooks1271598
Node: Searching and Matching1273610
Node: String Search1274741
Node: Regular Expressions1279465
Node: Syntax of Regexps1280048
Node: Regexp Example1292492
Node: Regexp Search1294662
Node: POSIX Regexps1299871
Node: Search and Replace1301706
Node: Match Data1305071
Node: Simple Match Data1306201
Node: Replacing Match1310465
Node: Entire Match Data1312799
Node: Saving Match Data1314792
Node: Searching and Case1316176
Node: Standard Regexps1318210
Node: Syntax Tables1320408
Node: Syntax Basics1321524
Node: Syntax Descriptors1324090
Node: Syntax Class Table1325940
Node: Syntax Flags1331976
Node: Syntax Table Functions1335193
Node: Motion and Syntax1339057
Node: Parsing Expressions1340509
Node: Standard Syntax Tables1346578
Node: Syntax Table Internals1347422
Node: Abbrevs1348449
Node: Abbrev Mode1350253
Node: Abbrev Tables1350973
Node: Defining Abbrevs1352506
Node: Abbrev Files1354411
Node: Abbrev Expansion1356185
Node: Standard Abbrev Tables1360816
Node: Extents1361975
Node: Intro to Extents1363130
Node: Creating and Modifying Extents1366656
Node: Extent Endpoints1368111
Node: Finding Extents1371177
Node: Extent Properties1374418
Node: Detached Extents1381076
Node: Duplicable Extents1382838
Node: Extent Replicas1387478
Node: Extents and Events1389261
Node: Atomic Extents1391125
Node: Specifiers1391572
Node: Introduction to Specifiers1393375
Node: Specifiers In-Depth1395685
Node: Specifier Instancing1400586
Node: Specifier Types1403855
Node: Adding Specifications1408511
Node: Retrieving Specifications1416366
Node: Specifier Tag Functions1420101
Node: Specifier Instancing Functions1423335
Node: Specifier Example1426877
Node: Creating Specifiers1429982
Node: Specifier Validation Functions1432231
Node: Other Specification Functions1434615
Node: Faces and Window-System Objects1438434
Node: Faces1438758
Node: Merging Faces1440376
Node: Basic Face Functions1442336
Node: Face Properties1444435
Node: Face Convenience Functions1453994
Node: Other Face Display Functions1457127
Node: Fonts1457940
Node: Font Specifiers1458641
Node: Font Instances1458882
Node: Font Instance Names1459849
Node: Font Instance Size1460690
Node: Font Instance Characteristics1461976
Node: Font Convenience Functions1463145
Node: Colors1464435
Node: Color Specifiers1464875
Node: Color Instances1465102
Node: Color Instance Properties1465846
Node: Color Convenience Functions1466472
Node: Glyphs1467525
Node: Glyph Functions1468189
Node: Creating Glyphs1468524
Node: Glyph Properties1469314
Node: Glyph Convenience Functions1478023
Node: Images1479002
Node: Image Specifiers1479447
Node: Image Instantiator Conversion1483810
Node: Image Instances1485167
Node: Cursors1487935
Node: Annotations1488104
Node: Annotation Basics1488581
Node: Annotation Primitives1491725
Node: Margin Primitives1495123
Node: Annotation Hooks1497074
Node: Display1497844
Node: Refresh Screen1498775
Node: Truncation1500885
Node: The Echo Area1503610
Node: Invisible Text1506683
Node: Selective Display1509266
Node: Overlay Arrow1513393
Node: Temporary Displays1514747
Node: Blinking1518868
Node: Usual Display1521052
Node: Display Tables1523602
Node: Display Table Format1524406
Node: Active Display Table1525850
Node: Character Descriptors1527042
Node: Beeping1527800
Node: Processes1532556
Node: Subprocess Creation1534777
Node: Synchronous Processes1538070
Node: MS-DOS Subprocesses1544768
Node: Asynchronous Processes1545842
Node: Deleting Processes1549546
Node: Process Information1551417
Node: Input to Processes1555345
Node: Signals to Processes1557614
Node: Output from Processes1562034
Node: Process Buffers1562846
Node: Filter Functions1565714
Node: Accepting Output1571281
Node: Sentinels1572808
Node: Process Window Size1576301
Node: Transaction Queues1576650
Node: Network1578348
Node: System Interface1580181
Node: Starting Up1581451
Node: Start-up Summary1582045
Node: Init File1585599
Node: Terminal-Specific1587981
Node: Command Line Arguments1591139
Node: Getting Out1594627
Node: Killing XEmacs1595196
Node: Suspending XEmacs1596865
Node: System Environment1600183
Node: User Identification1606266
Node: Time of Day1608152
Node: Time Conversion1610947
Node: Timers1615920
Node: Terminal Input1618093
Node: Input Modes1618596
Node: Translating Input1621009
Node: Recording Input1623255
Node: Terminal Output1624556
Node: Flow Control1627417
Node: Batch Mode1631210
Node: X-Windows1632299
Node: X Selections1633087
Node: X Server1635297
Node: Resources1635698
Node: Server Data1640565
Node: Grabs1641772
Node: X Miscellaneous1643353
Node: ToolTalk Support1645738
Node: XEmacs ToolTalk API Summary1645963
Node: Sending Messages1647262
Node: Example of Sending Messages1647513
Node: Elisp Interface for Sending Messages1648575
Node: Receiving Messages1654966
Node: Example of Receiving Messages1655189
Node: Elisp Interface for Receiving Messages1656025
Node: Internationalization1659850
Node: I18N Levels 1 and 21660060
Node: I18N Level 31660766
Node: Level 3 Basics1661047
Node: Level 3 Primitives1661880
Node: Dynamic Messaging1663485
Node: Domain Specification1663948
Node: Documentation String Extraction1665854
Node: I18N Level 41666772
Node: Tips1666914
Node: Style Tips1667549
Node: Compilation Tips1677057
Node: Documentation Tips1678972
Node: Comment Tips1684481
Node: Library Headers1687485
Node: XEmacs Internals1691457
Node: Building XEmacs1692158
Node: Pure Storage1696812
Node: Garbage Collection1699534
Node: Writing XEmacs Primitives1707555
Node: Object Internals1717770
Node: Buffer Internals1719106
Node: Window Internals1722252
Node: Process Internals1725692
Node: Standard Errors1727675
Node: Standard Buffer-Local Variables1731903
Node: Standard Keymaps1734579
Node: Standard Hooks1738311
Node: Index1742639

End Tag Table

This is Info file ../../info/new-users-guide.info, produced by
Makeinfo-1.63 from the input file new-users-guide.texi.

   This file will serve as the User's Manual for the XEmacs editor.

   Copyright (C) 1985, 1986, 1988 Richard M. Stallman.  Copyright (C)
1991, 1992, 1993, 1994 Lucid, Inc.  Copyright (C) 1993, 1994 Sun
Microsystems, Inc.

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.


File: new-users-guide.info,  Node: Command Index,  Next: Variable Index,  Prev: Key Index,  Up: Top

Command and Function Index
**************************

* Menu:

* add-menu-item:                        Customizing Menus.
* append-to-buffer:                     Accumulating text.
* append-to-file:                       Accumulating text.
* auto-fill-mode <1>:                   Insert.
* auto-fill-mode:                       Minor Modes.
* backward-char:                        Cursor Position.
* backward-kill-word:                   Erase.
* backward-word:                        Cursor Position.
* beginning-of-buffer:                  Cursor Position.
* beginning-of-line:                    Cursor Position.
* copy-to-buffer:                       Accumulating text.
* delete-backward-char:                 Erase.
* delete-char:                          Erase.
* delete-menu-item:                     Customizing Menus.
* delete-other-windows <1>:             XEmacs Window.
* delete-other-windows:                 Windows and Menus.
* delete-window <1>:                    XEmacs Window.
* delete-window:                        Windows and Menus.
* describe-variable:                    Setting Variables.
* dired-other-window:                   XEmacs Window.
* disable-menu-item:                    Customizing Menus.
* edit-options:                         Setting Variables.
* enable-menu-item:                     Customizing Menus.
* end-of-buffer:                        Cursor Position.
* end-of-line:                          Cursor Position.
* eval-expression:                      Init File.
* eval-region:                          Customization Basics.
* exchange-point-and-mark:              Selecting Text.
* find-alternate-file:                  Visiting.
* find-file:                            Visiting.
* find-file-other-frame:                Visiting.
* find-file-other-window:               XEmacs Window.
* forward-char:                         Cursor Position.
* forward-word:                         Cursor Position.
* goto-char:                            Cursor Position.
* goto-line:                            Cursor Position.
* help-with-tutorial:                   Edit.
* isearch-backward:                     Search and Replace.
* isearch-forward:                      Search and Replace.
* kill-line:                            Erase.
* kill-sentence:                        Erase.
* kill-word:                            Erase.
* list-options:                         Setting Variables.
* mail-other-window:                    XEmacs Window.
* make-directory:                       File Names.
* make-symbolic-link:                   Customizing key Bindings.
* mark-beginning-of-buffer:             Selecting Text.
* mark-end-of-buffer:                   Selecting Text.
* next-line:                            Cursor Position.
* prepend-to-buffer:                    Accumulating text.
* previous-line:                        Cursor Position.
* print-region:                         Region Operation.
* relabel-menu-items:                   Customizing Menus.
* remove-directory:                     File Names.
* replace-string:                       Search and Replace.
* save-buffer:                          Saving Files.
* save-buffers-kill-emacs:              Exiting.
* save-some-buffers:                    Saving Files.
* scroll-other-window <1>:              Windows and Menus.
* scroll-other-window:                  XEmacs Window.
* set-mark-command:                     Selecting Text.
* set-variable:                         Setting Variables.
* split-window-horizontally:            XEmacs Window.
* split-window-vertically:              XEmacs Window.
* suspend-emacs:                        Exiting.
* switch-to-buffer-other-window:        XEmacs Window.
* transpose-chars:                      Cursor Position.
* write file:                           Saving Files.
* yank:                                 Moving Text.
* zap-to-char:                          Erase.


File: new-users-guide.info,  Node: Variable Index,  Next: Concept Index,  Prev: Command Index,  Up: Top

Variable Index
**************

* Menu:

* buffers-menu-max-size:                Init File.
* case-fold-search:                     Search and Replace.
* default-directory:                    File Names.
* display-time:                         Init File.
* fill-column:                          Init File.
* frame-title-format:                   Init File.
* make-backup-files:                    Saving Files.
* zmacs-regions:                        Init File.


File: new-users-guide.info,  Node: Concept Index,  Next: Entering,  Prev: Variable Index,  Up: Top

Concept Index
*************

* Menu:

* .emacs:                               Customization Basics.
* abbrev-modex:                         Minor Modes.
* accumulating text:                    Accumulating text.
* add menus:                            Customizing Menus.
* asm-mode:                             Major Modes.
* Auto Delete Selection menu item:      Options Menu.
* auto saving:                          Saving Files.
* auto-save-mode:                       Minor Modes.
* binding keys:                         Customizing key Bindings.
* blink-paren:                          Minor Modes.
* buffer:                               Entering.
* Buffers menu:                         Buffers Menu.
* Buffers Menu Length... menu item:     Options Menu.
* Buffers Sub-Menus menu item:          Options Menu.
* c-mode:                               Major Modes.
* Case Sensitive Search menu item:      Options Menu.
* Clear menu item:                      Edit menu.
* clipboard selection:                  Mouse.
* Copy menu item:                       Edit menu.
* copying text:                         Accumulating text.
* creating-directories:                 File Names.
* cursor control:                       Cursor Position.
* cursor position:                      Cursor Position.
* cursor shapes:                        Mouse.
* customize <1>:                        Customization Basics.
* customize:                            Other Customizations.
* customize menus:                      Customizing Menus.
* Cut menu item:                        Edit menu.
* Delete Frame menu item:               File menu.
* delete menus:                         Customizing Menus.
* deleting:                             Erase.
* deleting menu items:                  Customizing Menus.
* deletion:                             Insert.
* digit argument:                       Numeric Argument.
* disable menus:                        Customizing Menus.
* disabling menu items:                 Customizing Menus.
* displaying time:                      Init File.
* echo area:                            Echo Area.
* edit-picture:                         Major Modes.
* enabling menu items:                  Customizing Menus.
* End Macro Recording menu item:        Edit menu.
* entering Emacs:                       Enter.
* entering XEmacs:                      Enter.
* erasing:                              Erase.
* Execute Last Macro menu item:         Edit menu.
* Exit Emacs menu item:                 File menu.
* exiting:                              Exiting.
* file:                                 Entering.
* File menu:                            File menu.
* file names:                           File Names.
* files:                                Files.
* Font menu item:                       Options Menu.
* font-lock-mode <1>:                   Other Customizations.
* font-lock-mode:                       Minor Modes.
* fortran-mode:                         Major Modes.
* fundamental-mode:                     Major Modes.
* goto-line:                            Cursor Position.
* help <1>:                             The Help Menu.
* help:                                 Help.
* Help menu:                            Help menu.
* hook:                                 Other Customizations.
* init file examples:                   Init File.
* Insert File... menu item:             File menu.
* insertion:                            Insert.
* key bindings:                         Customizing key Bindings.
* keystrokes:                           Customizing key Bindings.
* Kill Buffer menu item:                File menu.
* kill ring:                            Moving Text.
* killing:                              Moving Text.
* killing Emacs:                        Exiting.
* line-number-mode:                     Minor Modes.
* lisp-mode:                            Major Modes.
* major modes:                          Major Modes.
* mark:                                 Select and Move.
* menus:                                XEmacs Window.
* minor modes:                          Minor Modes.
* mistakes, correcting:                 Undo.
* mode line:                            Mode Line.
* modes:                                Modes.
* mouse selection:                      Mouse.
* moving text:                          Moving Text.
* negative argument:                    Numeric Argument.
* New Frame menu item:                  File menu.
* newline:                              Insert.
* nroff-mode:                           Major Modes.
* numeric argument:                     Numeric Argument.
* Open ... menu item:                   File menu.
* open another file:                    Frame.
* Open in New Frame... menu item:       File menu.
* Options menu:                         Options Menu.
* outline-mode:                         Major Modes.
* overstrike:                           Insert.
* Overstrike menu item:                 Options Menu.
* overwrite-mode:                       Minor Modes.
* Paren Highlighting menu item:         Options Menu.
* Paste menu item:                      Edit menu.
* pasting:                              Moving Text.
* primary selection:                    Mouse.
* Print Buffer menu item:               File menu.
* pull-down-menus:                      XEmacs Window.
* Read Only menu item:                  Options Menu.
* rectangle commands:                   Accumulating text.
* region:                               Select and Move.
* registers:                            Accumulating text.
* relabelling menu items:               Customizing Menus.
* removing-directories:                 File Names.
* replace:                              Search and Replace.
* Revert Buffer menu item:              File menu.
* Save Buffer As ... menu item:         File menu.
* Save Buffer menu item:                File menu.
* Save Options:                         Options Menu.
* saving files:                         Saving Files.
* searching:                            Search and Replace.
* selected window:                      Windows and Menus.
* setting variables:                    Setting Variables.
* shrinking XEmacs frame:               Exiting.
* simultaneous editing:                 Saving Files.
* Size menu item:                       Options Menu.
* Split Frame:                          File menu.
* Start Macro Recording menu item:      Edit menu.
* suspending:                           Exiting.
* Syntax Highlighting menu item:        Options Menu.
* Teach Extended Commands menu item:    Options Menu.
* temporary storage:                    Accumulating text.
* tex-mode:                             Major Modes.
* texinfo-mode:                         Major Modes.
* top level:                            Mode Line.
* Un-split (Keep Others):               File menu.
* Un-split (Keep This):                 File menu.
* undo:                                 Undo.
* Undo menu item:                       Edit menu.
* visiting files:                       Visiting.
* Weight menu item:                     Options Menu.
* windows <1>:                          XEmacs Window.
* windows <1>:                          Entering.
* windows:                              Windows and Menus.
* yanking:                              Moving Text.



SHELL = /bin/sh
CC=gcc
CFLAGS= -g -O 
ALLOCA=
YACC=bison -y
LN_S=ln -s
version=19.13
configuration=sparc-sun-solaris2.4
prefix=/usr/local
exec_prefix=${prefix}
bindir=${exec_prefix}/bin
libdir=${exec_prefix}/lib
srcdir=/usr/local/src/xemacs/lib-src
VPATH=/usr/local/src/xemacs/lib-src
archlibdir=${libdir}/xemacs-${version}/${configuration}
INSTALL = /usr/local/src/xemacs/install.sh -c
INSTALL_PROGRAM = ${INSTALL}
INSTALL_DATA = ${INSTALL} -m 644
INSTALLABLES = etags ctags emacsclient b2m gnuclient gnudoit
INSTALLABLE_SCRIPTS = rcs-checkin
UTILITIES= make-path wakeup profile make-docfile digest-doc 	sorted-doc movemail cvtmail fakemail yow emacsserver hexl 	gnuserv
SCRIPTS= rcs2log vcdiff
EXECUTABLES= ${UTILITIES} ${INSTALLABLES} ${SCRIPTS} ${INSTALLABLE_SCRIPTS}
SOURCES = COPYING ChangeLog Makefile.in.in README aixcc.lex emacs.csh 	makedoc.com *.[chy] rcs2log vcdiff
LOADLIBES= -lsocket -lnsl -lintl -lelf -lkvm -lgen -ldl     
LOAD_X_LIBES= -L/usr/openwin/lib -R/usr/openwin/lib     
C_SWITCH_X= -I/usr/openwin/include  -I/usr/dt/include   
ALL_CFLAGS =     -Demacs -DHAVE_CONFIG_H    -DSTDC_HEADERS -I. -I../src -I${srcdir} -I${srcdir}/../src    ${LDFLAGS} ${CPPFLAGS} ${CFLAGS}
LINK_CFLAGS =     -Demacs -DHAVE_CONFIG_H    -I. -I../src -I${srcdir} -I${srcdir}/../src ${LDFLAGS} ${CFLAGS}
CPP_CFLAGS =     -Demacs -DHAVE_CONFIG_H    -I. -I../src -I${srcdir} -I${srcdir}/../src ${CPPFLAGS} ${CFLAGS}
ALLOCA_CFLAGS =     -Demacs -DHAVE_CONFIG_H    -I. -I../src -I${srcdir} -I${srcdir}/../src ${CPPFLAGS} ${CFLAGS}
.c.o:
	${CC} -c ${CPP_CFLAGS} $<
all: ${UTILITIES} ${INSTALLABLES} ${SCRIPTS}
rcs2log: force
	@if [ ! -f rcs2log ] ; then 				  ${LN_S} ${srcdir}/rcs2log . ;				fi
vcdiff: force
	@if [ ! -f vcdiff ] ; then				  ${LN_S} ${srcdir}/vcdiff . ;				fi
force:
blessmail:
	../src/emacs -batch -l ../lisp/blessmail.el
	chmod +x blessmail
maybe-blessmail: blessmail 
	@if [ `wc -l <blessmail` != 2 ] ; then 	  dir=`sed -n -e 's/echo mail directory = \(.*\)/\1/p' blessmail`; 	  echo Assuming $$dir is really the mail spool directory, you should; 	  echo run  lib-src/blessmail ${archlibdir}/movemail; 	  echo as root, to give  movemail  appropriate permissions.; 	  echo Do that after running  make install.; 	fi
${archlibdir}: all
	@echo
	@echo "Installing utilities run internally by XEmacs."
	./make-path ${archlibdir}
	if [ `(cd ${archlibdir} && /bin/pwd)` != `/bin/pwd` ]; then 	  for file in ${UTILITIES}; do 	    (cd ..; $(INSTALL_PROGRAM) lib-src/$$file ${archlibdir}/$$file) ; 	  done ; 	fi
	if [ `(cd ${archlibdir} && /bin/pwd)` 	     != `(cd ${srcdir} && /bin/pwd)` ]; then 	  for file in ${SCRIPTS}; do 	    (cd ..; $(INSTALL_PROGRAM) ${srcdir}/$$file ${archlibdir}/$$file); 	  done ; 	fi
install: ${archlibdir}
	@echo
	@echo "Installing utilities for users to run."
	for file in ${INSTALLABLES} ; do 	  (cd ..; $(INSTALL_PROGRAM) lib-src/$${file} ${bindir}/$${file}) ; 	done
	for file in ${INSTALLABLE_SCRIPTS} ; do 	  (cd ..; $(INSTALL_PROGRAM) ${srcdir}/$${file} ${bindir}/$${file}) ; 	done
uninstall:
	(cd ${bindir}; 	 rm -f ${INSTALLABLES} ${INSTALLABLE_SCRIPTS})
	(cd ${archlibdir}; 	 rm -f ${UTILITIES} ${INSTALLABLES} ${SCRIPTS} ${INSTALLABLE_SCRIPTS})
mostlyclean:
	-rm -f core *.o
clean: mostlyclean
	-rm -f ${INSTALLABLES} ${UTILITIES}
distclean: clean
	-rm -f ../etc/DOC* *.tab.c *.tab.h aixcc.c TAGS
	-rm -f Makefile Makefile.in blessmail
realclean: distclean
	true
extraclean: realclean
	-rm -f *~ \#*
unlock:
	chmod u+w $(SOURCES)
relock:
	chmod u-w $(SOURCES)
check:
	@echo "We don't have any tests for XEmacs yet."
TAGS: etags
	etags *.[ch]
GETOPTOBJS = getopt.o getopt1.o $(ALLOCA)
GETOPTDEPS = $(GETOPTOBJS) ${srcdir}/getopt.h
getopt.o: ${srcdir}/getopt.c ${srcdir}/getopt.h
	${CC} -c ${CPP_CFLAGS} ${srcdir}/getopt.c
getopt1.o: ${srcdir}/getopt1.c ${srcdir}/getopt.h
	${CC} -c ${CPP_CFLAGS} ${srcdir}/getopt1.c
alloca.o: ${srcdir}/../src/alloca.c
	${CC} -c ${ALLOCA_CFLAGS} ${srcdir}/../src/alloca.c
etags: ${srcdir}/etags.c $(GETOPTDEPS) ../src/config.h
	$(CC) ${ALL_CFLAGS} -DVERSION="\"${version}\"" ${srcdir}/etags.c $(GETOPTOBJS) $(LOADLIBES) -o etags
ctags: ${srcdir}/etags.c $(GETOPTDEPS) etags
	$(CC) ${ALL_CFLAGS} -DCTAGS -DVERSION="\"${version}\"" ${srcdir}/etags.c $(GETOPTOBJS) $(LOADLIBES) -o ctags
wakeup: ${srcdir}/wakeup.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/wakeup.c $(LOADLIBES) -o wakeup
profile: ${srcdir}/profile.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/profile.c $(LOADLIBES) -o profile
make-docfile: ${srcdir}/make-docfile.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/make-docfile.c $(LOADLIBES) -o make-docfile
digest-doc: ${srcdir}/digest-doc.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/digest-doc.c $(LOADLIBES) -o digest-doc 
sorted-doc: ${srcdir}/sorted-doc.c ${ALLOCA}
	$(CC) ${ALL_CFLAGS} ${srcdir}/sorted-doc.c ${ALLOCA} $(LOADLIBES) -o sorted-doc
b2m: ${srcdir}/b2m.c ../src/config.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/b2m.c $(LOADLIBES) -o b2m 
movemail: ${srcdir}/movemail.c ../src/config.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/movemail.c $(LOADLIBES) -o movemail
cvtmail: ${srcdir}/cvtmail.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/cvtmail.c $(LOADLIBES) -o cvtmail
fakemail: ${srcdir}/fakemail.c ../src/config.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/fakemail.c $(LOADLIBES) -o fakemail
yow: ${srcdir}/yow.c ../src/paths.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/yow.c $(LOADLIBES) -o yow
emacsserver: ${srcdir}/emacsserver.c ../src/config.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/emacsserver.c $(LOADLIBES) -o emacsserver
emacsclient: ${srcdir}/emacsclient.c ../src/config.h
	$(CC) ${ALL_CFLAGS} ${srcdir}/emacsclient.c $(LOADLIBES) -o emacsclient
hexl: ${srcdir}/hexl.c
	$(CC) ${ALL_CFLAGS} ${srcdir}/hexl.c $(LOADLIBES) -o hexl
make-msgfile: ${srcdir}/make-msgfile.c
	$(CC) ${CPP_CFLAGS} ${srcdir}/make-msgfile.c $(LOADLIBES) -o make-msgfile
make-po: ${srcdir}/make-po.c
	$(CC) ${CPP_CFLAGS} ${srcdir}/make-po.c $(LOADLIBES) -o make-po
gnuslib.o: ${srcdir}/gnuslib.c ${srcdir}/gnuserv.h ../src/config.h
	$(CC) -c ${CPP_CFLAGS} ${C_SWITCH_X} ${srcdir}/gnuslib.c
gnuclient: ${srcdir}/gnuclient.c gnuslib.o ${srcdir}/gnuserv.h 
	$(CC) ${ALL_CFLAGS} -o gnuclient ${srcdir}/gnuclient.c gnuslib.o $(LOADLIBES) $(LOAD_X_LIBES) -lXau
gnudoit: ${srcdir}/gnudoit.c gnuslib.o ${srcdir}/gnuserv.h 
	$(CC) ${ALL_CFLAGS} -o gnudoit ${srcdir}/gnudoit.c gnuslib.o $(LOADLIBES) $(LOAD_X_LIBES) -lXau
gnuserv: ${srcdir}/gnuserv.c gnuslib.o ${srcdir}/gnuserv.h
	$(CC) ${ALL_CFLAGS} ${C_SWITCH_X} -o gnuserv ${srcdir}/gnuserv.c gnuslib.o $(LOADLIBES) $(LOAD_X_LIBES) -lXau
make-path: ${srcdir}/make-path.c ../src/config.h
	$(CC) $(ALL_CFLAGS) ${srcdir}/make-path.c -o make-path
emacstool: ${srcdir}/emacstool.c
	$(CC) ${srcdir}/emacstool.c -o emacstool ${ALL_CFLAGS} 	  -lsuntool -lsunwindow -lpixrect $(LOADLIBES)
nemacstool: ${srcdir}/emacstool.c
	$(CC) -o nemacstool -DJLE ${ALL_CFLAGS} ${srcdir}/emacstool.c 	  -lsuntool -lmle -lsunwindow -lpixrect $(LOADLIBES)
xvetool: ${srcdir}/emacstool.c
	$(CC) -o xvetool -DXVIEW ${ALL_CFLAGS} ${srcdir}/emacstool.c 	  -lxview -lX -I$(OPENWINHOME)/include -L$(OPENWINHOME)/lib 	  $(LOADLIBES)
xveterm: ${srcdir}/emacstool.c
	$(CC) -o xveterm -DXVIEW -DTTERM ${ALL_CFLAGS} ${srcdir}/emacstool.c 	  -lxview -lolgx -lX  -I$(OPENWINHOME)/include -L$(OPENWINHOME)/lib 	  $(LOADLIBES)
aixcc: ${srcdir}/aixcc.c
	$(CC) $(ALL_CFLAGS) -o aixcc ${srcdir}/aixcc.c
aixcc.c: ${srcdir}/aixcc.lex
	lex ${srcdir}/aixcc.lex
	mv lex.yy.c aixcc.c

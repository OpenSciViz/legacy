/* Flags indicating how lwlib was compiled.
   Copyright (C) 1994 Lucid, Inc.

This file is part of the Lucid Widget Library.

The Lucid Widget Library is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

The Lucid Widget Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  */


/* This is a kludge to make sure emacs can only link against a version of
   lwlib that was compiled in the right way.  Emacs references symbols which
   correspond to the way it thinks lwlib was compiled, and if lwlib wasn't
   compiled in that way, then somewhat meaningful link errors will result.
   The alternatives to this range from obscure link errors, to obscure
   runtime errors that look a lot like bugs.
 */

#include "lwlib.h"

#include <X11/Xlib.h>	/* to get XlibSpecificationRelease */
#ifdef NEED_MOTIF
#include <Xm/Xm.h>	/* to get XmVersion */
#endif

#if (XlibSpecificationRelease == 4)
int lwlib_uses_x11r4;
#elif (XlibSpecificationRelease == 5)
int lwlib_uses_x11r5;
#elif (XlibSpecificationRelease == 6)
int lwlib_uses_x11r6;
#else
int lwlib_uses_unknown_x11;
#endif

#ifdef NEED_MOTIF
int lwlib_uses_motif;
#else
int lwlib_does_not_use_motif;
#endif

#if (XmVersion >= 1002)
int lwlib_uses_motif_1_2;
#else
int lwlib_does_not_use_motif_1_2;
#endif

#ifdef MENUBARS_LUCID
int lwlib_menubars_lucid;
#else
# ifdef MENUBARS_MOTIF
int lwlib_menubars_motif;
# else
Error!  Unknown toolkit for menubars.
# endif
#endif

#ifdef SCROLLBARS_LUCID
int lwlib_scrollbars_lucid;
#else
# ifdef SCROLLBARS_MOTIF
int lwlib_scrollbars_motif;
# else
#  ifdef SCROLLBARS_ATHENA
int lwlib_scrollbars_athena;
#  else
Error!  Unknown toolkit for scrollbars.
#  endif
# endif
#endif

#ifdef DIALOGS_MOTIF
int lwlib_dialogs_motif;
#else
# ifdef DIALOGS_ATHENA
int lwlib_dialogs_athena;
# else
Error!  Unknown toolkit for dialogs.
# endif
#endif

#ifdef ENERGIZE
int lwlib_uses_energize;
#else
int lwlib_does_not_use_energize;
#endif

;; Copyright (c) Harlequin Limited, 1987 - 1993.

;; Lispworks resources for X11R2

(in-package tools)

(clue::define-resources
  (* font) "8x13"
  (* title-font)  "9x15"
  (* item-title-font) "8x13bold"
  (* message-font) "6x13")

(clue::define-resources
  (* title-widget title-font) "8x13"
  (* text-item    title-font) "8x13")

(clue::define-resources
  (* top-level * scroll-pane * menu  font) "8x13")

(clue::define-resources
  (* plain-pane font)  "8x13"
  (* editor-pane bold-font)  "8x13bold"
  (* editor-pane italic-font)  "8x13bold"
  (* editor-pane bold-italic-font)  "8x13bold")


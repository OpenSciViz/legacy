;;----------------------------------------------------------------------------
;;
;; Default colour resources for lispworks.
;;
;; If you add anything in here make sure that you add a
;; corresponding entry in the r-mono file otherwise there
;; may be problems if you swap servers.
;;
;;----------------------------------------------------------------------------
;; Copyright (c) Harlequin Limited, 1987--1993.
;;----------------------------------------------------------------------------


(in-package tools)


;;----------------------------------------------------------------------------
;; The LispWorks color aliases... redefining each of these will be
;; sufficient to change all of the colors in LispWorks. The colors themselves
;; are defined below.
;;----------------------------------------------------------------------------

(dolist (color
         '(
           (:color-foreground                  :navy)
           (:color-background                  :lispworks-blue)
           (:color-highlight                   :red)

           (:color-scroll-bar-foreground       :dark-blue)

           (:color-list-pane-background        :medium-yellow)
           (:color-display-pane-background     :medium-yellow)
           (:color-editor-pane-background      :light-brown)
           (:color-graph-pane-background       :beige)
           (:color-listener-background         :medium-green)
           (:color-shell-background            :medium-brown)
           (:color-collector-background        :medium-yellow)
           (:color-book-page-background        :medium-yellow)

           (:color-system-grapher-system-color :color-highlight)
           (:color-system-grapher-file-color   :color-foreground)

           (:color-classworks-foreground       :black)
           (:color-classworks-background       :white)
           (:color-classworks-highlight-color  :yellow)
           
           (:color-confirmer-foreground        :black)
           (:color-confirmer-background        :highlight-red)
           (:color-confirmer-border            :firebrick)
           ))
  (color:define-color-alias (first color) (second color)))


;;----------------------------------------------------------------------------
;; Some useful colors
;;----------------------------------------------------------------------------

(color:define-color-alias :lispworks-blue (color:make-rgb 0.70 0.90 0.99))
(color:define-color-alias :light-red      (color:make-rgb 0.99 0.85 0.92))
(color:define-color-alias :highlight-red  (color:make-rgb 0.99 0.55 0.55))
(color:define-color-alias :light-blue     (color:make-rgb 0.92 0.97 0.99))
(color:define-color-alias :medium-blue    (color:make-rgb 0.88 0.96 0.99))
(color:define-color-alias :dark-blue      (color:make-rgb 0.00 0.60 0.80))
(color:define-color-alias :gray-blue      (color:make-rgb 0.15 0.90 0.90))
(color:define-color-alias :light-brown    (color:make-rgb 0.99 0.97 0.95))
(color:define-color-alias :medium-brown   (color:make-rgb 0.99 0.95 0.85))
(color:define-color-alias :medium-green   (color:make-rgb 0.85 0.99 0.94))
(color:define-color-alias :medium-yellow  (color:make-rgb 0.99 0.99 0.75))

;;; Y 16/9/92
(color:define-color-alias :foreground :black)


;;----------------------------------------------------------------------------
;; The default color resources
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * foreground) :color-foreground
  (lispworks color-root * background) :color-background)

(define-resources
  (lispworks color-root * top-level border) :black
  (lispworks color-root * top-level inactive-colour) :color-highlight
  )


;;----------------------------------------------------------------------------
;; Scroll bar colors
;;----------------------------------------------------------------------------

;; Open Look scroll bar colors

(define-resources
  (lispworks color-root * scroll-bar slug-foreground) :dark-blue
  (lispworks color-root * scroll-bar background-colour) :light-blue
  (lispworks color-root * scroll-bar background-gray) :light-blue
  (lispworks color-root * scroll-bar slug-gray) '100%gray
  (lispworks color-root * scroll-bar background) :light-red
  (lispworks color-root * scroll-bar * background) :light-red
  (lispworks color-root * scroll-bar foreground) :dark-blue
  (lispworks color-root * scroll-bar * foreground) :dark-blue
  )

;; Motif scroll bar colors

(define-resources
  (lispworks color-root * simple-scroll-bar foreground)
  :color-scroll-bar-foreground
  )


;;----------------------------------------------------------------------------
;; Shell background's
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * shell-window interactive-stream
             background) :color-shell-background
  (lispworks color-root * top-level * shell * interactive-stream
             background) :color-shell-background)


;;----------------------------------------------------------------------------
;; Make popups uniformly colored
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * simple-popup * foreground) :color-foreground
  (lispworks color-root * simple-popup * background) :color-background
  (lispworks color-root * simple-popup * simple-scroll-bar foreground)
  :color-scroll-bar-foreground
  )


;;----------------------------------------------------------------------------
;; Make confirmers uniformly black on red
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * confirmer * foreground) :color-confirmer-foreground
  (lispworks color-root * confirmer * background) :color-confirmer-background
  (lispworks color-root * confirmer border) :color-confirmer-border)


;;----------------------------------------------------------------------------
;; Miscellaneous colors
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * text-menu * background) :color-list-pane-background
  (lispworks color-root * fixed-text-menu * background) :color-list-pane-background
  (lispworks color-root * display-pane * background) :color-display-pane-background
  (lispworks color-root * icon-foreground) :color-background
  (lispworks color-root * colour-editor * background) 'gray90
  (lispworks color-root * resource-browser * message background) :medium-blue
  (lispworks color-root * resource-browser * complete-pane background) 
  :medium-blue
  (lispworks color-root * viewer-window * book-page background)
  :color-book-page-background
  (lispworks color-root * page-viewer * book-page background)
  :color-book-page-background
  )

(define-resources
  (lispworks color-root * top-level * scroll-pane * scroll-bar
             background) :light-red
  (lispworks color-root * top-level * scroll-pane * scroll-bar
             foreground) :dark-blue
  )

(define-resources
  (lispworks color-root * popup-menu background) :light-red
  (lispworks color-root * popup-menu * background) :light-red
  )

(define-resources
  (lispworks color-root * collector background) :color-collector-background
  (lispworks color-root * editor-pane background) :color-editor-pane-background)

(define-resources
  (lispworks color-root * top-level * listener background)
  :color-listener-background
  (lispworks color-root * listener-pane background)
  :color-listener-background
  (lispworks color-root * workspace * reader background)
  :color-listener-background
  (lispworks color-root * workspace * listener background)
  :color-collector-background)

(define-resources
  (lispworks color-root  * lispworks-top-level icon)
  `(new-lispworks-logo :color-foreground :color-background)
  )
  

;;----------------------------------------------------------------------------
;; Colors for Graphics Ports.
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * top-level * graphics-port foreground) :color-foreground
  (lispworks color-root * graphics-port foreground) :color-foreground
  (lispworks color-root * graph-pane * background) :color-graph-pane-background)

(color:define-color-alias :motif-bevel-highlight-color :white)
(color:define-color-alias :motif-bevel-lowlight-color  :navy)


;;----------------------------------------------------------------------------
;; ClassWorks color aliases
;;----------------------------------------------------------------------------

(color:define-color-alias :drag-select-foreground :color-classworks-highlight-color)
(color:define-color-alias :drag-select-background :color-classworks-background)
(color:define-color-alias :cw-cursor-foreground   :color-classworks-foreground)
(color:define-color-alias :cw-cursor-background   :color-classworks-highlight-color)
(color:define-color-alias :cw-handle-foreground   :color-classworks-foreground)
(color:define-color-alias :cw-handle-fill         :color-classworks-background)
(color:define-color-alias :cw-object-foreground   :color-classworks-foreground)
(color:define-color-alias :cw-object-background   :color-classworks-background)

(define-resources
  (lispworks color-root * top-level * master background) :color-classworks-background)


;;----------------------------------------------------------------------------
;; System grapher colors
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * system-grapher system-color)
  :color-system-grapher-system-color
  (lispworks color-root * system-grapher file-color)
  :color-system-grapher-file-color)


;;----------------------------------------------------------------------------
;; CAPI colors
;;----------------------------------------------------------------------------

(define-resources
  (lispworks color-root * notifier list-panel background)
  :color-list-pane-background
  (lispworks color-root * notifier editor-pane background)
  :color-editor-pane-background
  (lispworks color-root * tree-pane background)
  :color-graph-pane-background
  (lispworks color-root * collector-pane background)
  :color-collector-background
  (lispworks color-root * shell interactive-stream background)
  :color-shell-background
  (lispworks color-root * capi-top-level * scroll-bar background)
  :color-background)

;; Copyright (c) Harlequin Limited, 1987 - 1993.


;; Lispworks resources for X11R3 and R4

(in-package tools)

;; default font is variable helvetica - except where it is important to 
;; have fixed font
(define-resources
    (* font) "*helvetica-medium-r-normal--12*"
  (* top-level * text-menu * font)  "*courier-medium-r-normal--12*"
  (* top-level * fixed-text-menu * font) "*courier-medium-r-normal--12*"
  (* plain-pane font)  "*courier-medium-r-normal--12*")

;; specific font resources for the editor - these should be the bold and
;; italic versions of the above plain pane font

(define-resources
  (* editor-pane bold-font)  "*courier-bold-r-normal--12*"
  (* editor-pane italic-font)  "*courier-medium-o-normal--12*"
  (* editor-pane bold-italic-font)  "*courier-bold-o-normal--12*")


(define-resources
  (* title-font)  "*helvetica-bold-r-normal--14*"
  (* item-title-font) "*helvetica-bold-r-normal--12*"
  (* message-font) "*helvetica-medium-r-normal--10*")

(define-resources
  (* title-widget title-font) "*helvetica-bold-r-normal--12*"
  (* text-item    title-font) "*helvetica-bold-r-normal--12*")

(define-resources
  (lispworks * spiral-hyper-book tab-font) "*helvetica-bold-r-normal--10*")

;; Moved from book/resources.lisp MLA

(define-resources
  (lispworks * spiral-hyper-book font)
  "*helvetica-bold-r-normal--10*")

(define-resources (lispworks * book-page font-translations)
  '(
    ((:tiny :normal) . "*helvetica-medium-r-normal--10*")
    ((:small :normal) . "*helvetica-medium-r-normal--12*")
    ((:large :normal) . "*helvetica-medium-r-normal--18*")
    ((:big :normal) . "*helvetica-medium-r-normal--18*")
    ((:huge :normal) . "*helvetica-medium-r-normal--24*")
    ((:tiny :bold) . "*helvetica-bold-r-normal--10*")
    ((:small :bold) . "*helvetica-bold-r-normal--12*")
    ((:large :bold) .  "*helvetica-bold-r-normal--18*")
    ((:big :bold) .  "*helvetica-bold-r-normal--18*")
    ((:huge :bold) . "*helvetica-bold-r-normal--24*")
    ((:tiny :italic) . "*helvetica-medium-o-normal--10*")
    ((:small :italic) . "*helvetica-medium-o-normal--12*")
    ((:large :italic) . "*helvetica-medium-o-normal--18*")
    ((:big :italic) . "*helvetica-medium-o-normal--18*")
    ((:typewriter) . "*courier-medium-r-normal--12-*")
    ((:symbol20) . "*-symbol-*--20-*")
    ((:symbol14) . "*-symbol-*--14-*")
    ))


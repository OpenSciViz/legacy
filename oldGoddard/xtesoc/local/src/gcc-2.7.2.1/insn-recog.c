/* Generated automatically by the program `genrecog'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "insn-config.h"
#include "recog.h"
#include "real.h"
#include "output.h"
#include "flags.h"

extern rtx gen_split_120 ();
extern rtx gen_split_258 ();
extern rtx gen_split_266 ();
extern rtx gen_split_274 ();
extern rtx gen_split_275 ();
extern rtx gen_split_376 ();
extern rtx gen_split_377 ();
extern rtx gen_split_378 ();
extern rtx gen_split_379 ();
extern rtx gen_split_380 ();
extern rtx gen_split_381 ();
extern rtx gen_split_382 ();
extern rtx gen_split_383 ();
extern rtx gen_split_384 ();
extern rtx gen_split_385 ();
extern rtx gen_split_386 ();
extern rtx gen_split_387 ();
extern rtx gen_split_388 ();
extern rtx gen_split_389 ();

/* `recog' contains a decision tree
   that recognizes whether the rtx X0 is a valid instruction.

   recog returns -1 if the rtx is not valid.
   If the rtx is valid, recog returns a nonnegative number
   which is the insn code number for the pattern that matched.
   This is the same as the order in the machine description of
   the entry that matched.  This number can be used as an index into
   entry that matched.  This number can be used as an index into various
   insn_* tables, such as insn_templates, insn_outfun, and insn_n_operands
   (found in insn-output.c).

   The third argument to recog is an optional pointer to an int.
   If present, recog will accept a pattern if it matches except for
   missing CLOBBER expressions at the end.  In that case, the value
   pointed to by the optional pointer will be set to the number of
   CLOBBERs that need to be added (it should be initialized to zero by
   the caller).  If it is set nonzero, the caller should allocate a
   PARALLEL of the appropriate size, copy the initial entries, and call
   add_clobbers (found in insn-emit.c) to fill in the CLOBBERs.

   The function split_insns returns 0 if the rtl could not
   be split or the split rtl in a SEQUENCE if it can be.*/

rtx recog_operand[MAX_RECOG_OPERANDS];

rtx *recog_operand_loc[MAX_RECOG_OPERANDS];

rtx *recog_dup_loc[MAX_DUP_OPERANDS];

char recog_dup_num[MAX_DUP_OPERANDS];

#define operands recog_operand

int
recog_1 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case SImode:
      switch (GET_CODE (x1))
	{
	case NE:
	  goto L81;
	case NEG:
	  goto L95;
	case EQ:
	  goto L137;
	case PLUS:
	  goto L195;
	case MINUS:
	  goto L264;
	case LTU:
	  goto L253;
	case GEU:
	  goto L280;
	}
    }
  L345:
  if (noov_compare_op (x1, SImode))
    {
      ro[1] = x1;
      goto L346;
    }
  L427:
  if (GET_MODE (x1) != SImode)
    goto ret0;
  switch (GET_CODE (x1))
    {
    case LO_SUM:
      goto L428;
    case HIGH:
      goto L439;
    }
  goto ret0;

  L81:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L82;
    }
  goto L345;

  L82:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 37;
    }
  goto L345;

  L95:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != SImode)
    {
      goto L345;
    }
  switch (GET_CODE (x2))
    {
    case NE:
      goto L96;
    case EQ:
      goto L152;
    case LTU:
      goto L259;
    case PLUS:
      goto L273;
    case GEU:
      goto L286;
    }
  goto L345;

  L96:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L97;
    }
  goto L345;

  L97:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 38;
    }
  goto L345;

  L152:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L153;
    }
  goto L345;

  L153:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 42;
    }
  goto L345;

  L259:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L260;
  goto L345;

  L260:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    return 50;
  goto L345;

  L273:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == LTU && 1)
    goto L274;
  goto L345;

  L274:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == CCmode && GET_CODE (x4) == REG && XINT (x4, 0) == 0 && 1)
    goto L275;
  goto L345;

  L275:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L276;
  goto L345;

  L276:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      return 52;
    }
  goto L345;

  L286:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L287;
  goto L345;

  L287:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    return 54;
  goto L345;

  L137:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L138;
    }
  goto L345;

  L138:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 41;
    }
  goto L345;

  L195:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != SImode)
    {
      goto L345;
    }
  switch (GET_CODE (x2))
    {
    case NE:
      goto L196;
    case EQ:
      goto L230;
    case LTU:
      goto L292;
    case GEU:
      goto L333;
    }
  goto L345;

  L196:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L197;
    }
  goto L345;

  L197:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L198;
  goto L345;

  L198:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && register_operand (x2, SImode))
    {
      ro[2] = x2;
      *pnum_clobbers = 1;
      return 45;
    }
  goto L345;

  L230:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L231;
    }
  goto L345;

  L231:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L232;
  goto L345;

  L232:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && register_operand (x2, SImode))
    {
      ro[2] = x2;
      *pnum_clobbers = 1;
      return 47;
    }
  goto L345;

  L292:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L293;
  goto L345;

  L293:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L294;
  goto L345;

  L294:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      return 55;
    }
  L301:
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == PLUS && 1)
    goto L302;
  goto L345;

  L302:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L303;
    }
  goto L345;

  L303:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      return 56;
    }
  goto L345;

  L333:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L334;
  goto L345;

  L334:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L335;
  goto L345;

  L335:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      return 60;
    }
  goto L345;

  L264:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != SImode)
    {
      goto L345;
    }
  switch (GET_CODE (x2))
    {
    case NEG:
      goto L265;
    case MINUS:
      goto L315;
    case SUBREG:
    case REG:
      if (register_operand (x2, SImode))
	{
	  ro[2] = x2;
	  goto L213;
	}
    }
  L307:
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L308;
    }
  goto L345;

  L265:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == LTU && 1)
    goto L266;
  goto L345;

  L266:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == CCmode && GET_CODE (x4) == REG && XINT (x4, 0) == 0 && 1)
    goto L267;
  goto L345;

  L267:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L268;
  goto L345;

  L268:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      return 51;
    }
  goto L345;

  L315:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L316;
    }
  goto L345;

  L316:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L317;
    }
  goto L345;

  L317:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == LTU && 1)
    goto L318;
  goto L345;

  L318:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L319;
  goto L345;

  L319:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    return 58;
  goto L345;

  L213:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) != SImode)
    {
      x2 = XEXP (x1, 0);
      goto L307;
    }
  switch (GET_CODE (x2))
    {
    case NE:
      goto L214;
    case EQ:
      goto L248;
    }
  x2 = XEXP (x1, 0);
  goto L307;

  L214:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L215;
    }
  x2 = XEXP (x1, 0);
  goto L307;

  L215:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 46;
    }
  x2 = XEXP (x1, 0);
  goto L307;

  L248:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L249;
    }
  x2 = XEXP (x1, 0);
  goto L307;

  L249:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    {
      *pnum_clobbers = 1;
      return 48;
    }
  x2 = XEXP (x1, 0);
  goto L307;

  L308:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) != SImode)
    {
      goto L345;
    }
  switch (GET_CODE (x2))
    {
    case LTU:
      goto L309;
    case PLUS:
      goto L325;
    case GEU:
      goto L341;
    }
  goto L345;

  L309:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L310;
  goto L345;

  L310:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    return 57;
  goto L345;

  L325:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == LTU && 1)
    goto L326;
  goto L345;

  L326:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == CCmode && GET_CODE (x4) == REG && XINT (x4, 0) == 0 && 1)
    goto L327;
  goto L345;

  L327:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L328;
  goto L345;

  L328:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      return 59;
    }
  goto L345;

  L341:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == CCmode && GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L342;
  goto L345;

  L342:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    return 61;
  goto L345;

  L253:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L254;
  goto L345;

  L254:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 49;
  goto L345;

  L280:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L281;
  goto L345;

  L281:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 53;
  goto L345;

  L346:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L347;
  goto L427;

  L347:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 62;
  goto L427;

  L428:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L434;
    }
  goto ret0;

  L434:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == UNSPEC && XINT (x2, 1) == 0 && XVECLEN (x2, 0) == 1 && 1)
    goto L435;
  if (immediate_operand (x2, SImode))
    {
      ro[2] = x2;
      return 82;
    }
  goto ret0;

  L435:
  x3 = XVECEXP (x2, 0, 0);
  if (immediate_operand (x3, SImode))
    {
      ro[2] = x3;
      return 83;
    }
  goto ret0;

  L439:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == UNSPEC && XINT (x2, 1) == 0 && XVECLEN (x2, 0) == 1 && 1)
    goto L440;
  L444:
  ro[1] = x2;
  if (check_pic (1))
    return 85;
  goto ret0;

  L440:
  x3 = XVECEXP (x2, 0, 0);
  ro[1] = x3;
  if (check_pic (1))
    return 84;
  goto L444;
 ret0: return -1;
}

int
recog_2 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case SImode:
      switch (GET_CODE (x1))
	{
	case ZERO_EXTEND:
	  goto L865;
	case SIGN_EXTEND:
	  goto L922;
	case FIX:
	  goto L1037;
	case PLUS:
	  goto L1117;
	case MINUS:
	  goto L1182;
	case MULT:
	  goto L1227;
	case TRUNCATE:
	  goto L1263;
	case DIV:
	  goto L1322;
	case UDIV:
	  goto L1360;
	case AND:
	  goto L1395;
	case IOR:
	  goto L1440;
	case XOR:
	  goto L1485;
	case NOT:
	  goto L1519;
	case NEG:
	  goto L1664;
	case ASHIFT:
	  goto L1858;
	case ASHIFTRT:
	  goto L1888;
	case LSHIFTRT:
	  goto L1898;
	}
    }
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L665;
  goto ret0;

  L865:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case HImode:
      if (memory_operand (x2, HImode))
	{
	  ro[1] = x2;
	  return 158;
	}
      break;
    case QImode:
      if (sparc_operand (x2, QImode))
	{
	  ro[1] = x2;
	  if (GET_CODE (operands[1]) != CONST_INT)
	    return 162;
	  }
    }
  goto ret0;

  L922:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case HImode:
      if (memory_operand (x2, HImode))
	{
	  ro[1] = x2;
	  return 174;
	}
      break;
    case QImode:
      if (memory_operand (x2, QImode))
	{
	  ro[1] = x2;
	  return 178;
	}
    }
  goto ret0;

  L1037:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) != FIX)
    goto ret0;
  switch (GET_MODE (x2))
    {
    case SFmode:
      goto L1038;
    case DFmode:
      goto L1043;
    case TFmode:
      goto L1048;
    }
  goto ret0;

  L1038:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SFmode))
    {
      ro[1] = x3;
      if (TARGET_FPU)
	return 205;
      }
  goto ret0;

  L1043:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DFmode))
    {
      ro[1] = x3;
      if (TARGET_FPU)
	return 206;
      }
  goto ret0;

  L1048:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, TFmode))
    {
      ro[1] = x3;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 207;
      }
  goto ret0;

  L1117:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1118;
    }
  goto ret0;

  L1118:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 220;
    }
  goto ret0;

  L1182:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1183;
    }
  goto ret0;

  L1183:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 228;
    }
  goto ret0;

  L1227:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1228;
    }
  goto ret0;

  L1228:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V8 || TARGET_SPARCLITE)
	return 233;
      }
  goto ret0;

  L1263:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == LSHIFTRT && 1)
    goto L1264;
  goto ret0;

  L1264:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == DImode && GET_CODE (x3) == MULT && 1)
    goto L1265;
  goto ret0;

  L1265:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) != DImode)
    goto ret0;
  switch (GET_CODE (x4))
    {
    case SIGN_EXTEND:
      goto L1266;
    case ZERO_EXTEND:
      goto L1298;
    }
  goto ret0;

  L1266:
  x5 = XEXP (x4, 0);
  if (register_operand (x5, SImode))
    {
      ro[1] = x5;
      goto L1267;
    }
  goto ret0;

  L1267:
  x4 = XEXP (x3, 1);
  switch (GET_MODE (x4))
    {
    case DImode:
      if (GET_CODE (x4) == SIGN_EXTEND && 1)
	goto L1268;
      break;
    case SImode:
      if (register_operand (x4, SImode))
	{
	  ro[2] = x4;
	  goto L1278;
	}
    }
  goto ret0;

  L1268:
  x5 = XEXP (x4, 0);
  if (register_operand (x5, SImode))
    {
      ro[2] = x5;
      goto L1269;
    }
  goto ret0;

  L1269:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 32 && 1)
    if (TARGET_V8 || TARGET_SPARCLITE)
      return 240;
  goto ret0;

  L1278:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 32 && 1)
    if (TARGET_V8 || TARGET_SPARCLITE)
      return 241;
  goto ret0;

  L1298:
  x5 = XEXP (x4, 0);
  if (register_operand (x5, SImode))
    {
      ro[1] = x5;
      goto L1299;
    }
  goto ret0;

  L1299:
  x4 = XEXP (x3, 1);
  if (GET_MODE (x4) == DImode && GET_CODE (x4) == ZERO_EXTEND && 1)
    goto L1300;
  L1309:
  if (uns_small_int (x4, SImode))
    {
      ro[2] = x4;
      goto L1310;
    }
  goto ret0;

  L1300:
  x5 = XEXP (x4, 0);
  if (register_operand (x5, SImode))
    {
      ro[2] = x5;
      goto L1301;
    }
  goto L1309;

  L1301:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 32 && 1)
    if (TARGET_V8 || TARGET_SPARCLITE)
      return 246;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 1);
  goto L1309;

  L1310:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 32 && 1)
    if (TARGET_V8 || TARGET_SPARCLITE)
      return 247;
  goto ret0;

  L1322:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1323;
    }
  goto ret0;

  L1323:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V8)
	{
	  *pnum_clobbers = 1;
	  return 248;
	}
      }
  goto ret0;

  L1360:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1361;
    }
  goto ret0;

  L1361:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V8)
	return 251;
      }
  goto ret0;

  L1395:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1396;
    }
  L1422:
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NOT && 1)
    goto L1423;
  goto ret0;

  L1396:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 257;
    }
  x2 = XEXP (x1, 0);
  goto L1422;

  L1423:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1424;
    }
  goto ret0;

  L1424:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SImode))
    {
      ro[2] = x2;
      return 261;
    }
  goto ret0;

  L1440:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1441;
    }
  L1467:
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NOT && 1)
    goto L1468;
  goto ret0;

  L1441:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 265;
    }
  x2 = XEXP (x1, 0);
  goto L1467;

  L1468:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1469;
    }
  goto ret0;

  L1469:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SImode))
    {
      ro[2] = x2;
      return 269;
    }
  goto ret0;

  L1485:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1486;
    }
  goto ret0;

  L1486:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 273;
    }
  goto ret0;

  L1519:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == XOR && 1)
    goto L1520;
  L1710:
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      return 302;
    }
  goto ret0;

  L1520:
  x3 = XEXP (x2, 0);
  if (reg_or_0_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1521;
    }
  goto L1710;

  L1521:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      return 278;
    }
  goto L1710;

  L1664:
  x2 = XEXP (x1, 0);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      return 294;
    }
  goto ret0;

  L1858:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1859;
    }
  goto ret0;

  L1859:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 330;
    }
  goto ret0;

  L1888:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1889;
    }
  goto ret0;

  L1889:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 334;
    }
  goto ret0;

  L1898:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L1899;
    }
  goto ret0;

  L1899:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      return 336;
    }
  goto ret0;

  L665:
  x2 = XEXP (x1, 0);
  if (comparison_operator (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L666;
    }
  L729:
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L730;
    }
  goto ret0;

  L666:
  x3 = XEXP (x2, 0);
  switch (GET_MODE (x3))
    {
    case CCmode:
      switch (GET_CODE (x3))
	{
	case REG:
	  if (XINT (x3, 0) == 0 && 1)
	    goto L667;
	}
      break;
    case CCXmode:
      if (GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
	goto L683;
    }
  L698:
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[2] = x3;
      goto L699;
    }
  L706:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[2] = x3;
      goto L707;
    }
  goto L729;

  L667:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L668;
  x3 = XEXP (x2, 0);
  goto L698;

  L668:
  x2 = XEXP (x1, 1);
  if (arith11_operand (x2, SImode))
    {
      ro[2] = x2;
      goto L669;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L698;

  L669:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (TARGET_V9)
	return 132;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L698;

  L683:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L684;
  x3 = XEXP (x2, 0);
  goto L698;

  L684:
  x2 = XEXP (x1, 1);
  if (arith11_operand (x2, SImode))
    {
      ro[2] = x2;
      goto L685;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L698;

  L685:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (TARGET_V9)
	return 134;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L698;

  L699:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L700;
  x3 = XEXP (x2, 0);
  goto L706;

  L700:
  x2 = XEXP (x1, 1);
  if (arith11_operand (x2, SImode))
    {
      ro[3] = x2;
      goto L701;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L706;

  L701:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 136;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L706;

  L707:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L708;
  goto L729;

  L708:
  x2 = XEXP (x1, 1);
  if (arith11_operand (x2, SImode))
    {
      ro[3] = x2;
      goto L709;
    }
  x2 = XEXP (x1, 0);
  goto L729;

  L709:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 137;
      }
  x2 = XEXP (x1, 0);
  goto L729;

  L730:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L731;
    }
  goto ret0;

  L731:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L732;
  goto ret0;

  L732:
  x2 = XEXP (x1, 1);
  if (arith10_operand (x2, SImode))
    {
      ro[3] = x2;
      goto L733;
    }
  goto ret0;

  L733:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 140;
      }
  goto ret0;
 ret0: return -1;
}

int
recog_3 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case DImode:
      switch (GET_CODE (x1))
	{
	case ZERO_EXTEND:
	  goto L877;
	case SIGN_EXTEND:
	  goto L934;
	case FIX:
	  goto L1082;
	case PLUS:
	  goto L1105;
	case MINUS:
	  goto L1170;
	case MULT:
	  goto L1232;
	case DIV:
	  goto L1327;
	case UDIV:
	  goto L1365;
	case AND:
	  goto L1383;
	case IOR:
	  goto L1428;
	case XOR:
	  goto L1473;
	case NOT:
	  goto L1507;
	case NEG:
	  goto L1654;
	case ASHIFT:
	  goto L1863;
	case ASHIFTRT:
	  goto L1893;
	case LSHIFTRT:
	  goto L1903;
	}
    }
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L673;
  goto ret0;

  L877:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case QImode:
      if (sparc_operand (x2, QImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9 && GET_CODE (operands[1]) != CONST_INT)
	    return 164;
	  }
      break;
    case HImode:
      if (memory_operand (x2, HImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9)
	    return 166;
	  }
      break;
    case SImode:
      if (sparc_operand (x2, SImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9 && GET_CODE (operands[1]) != CONST_INT)
	    return 168;
	  }
    }
  goto ret0;

  L934:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case QImode:
      if (memory_operand (x2, QImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9)
	    return 180;
	  }
      break;
    case HImode:
      if (memory_operand (x2, HImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9)
	    return 182;
	  }
      break;
    case SImode:
      if (sparc_operand (x2, SImode))
	{
	  ro[1] = x2;
	  if (TARGET_V9)
	    return 184;
	  }
    }
  goto ret0;

  L1082:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) != FIX)
    goto ret0;
  switch (GET_MODE (x2))
    {
    case SFmode:
      goto L1083;
    case DFmode:
      goto L1088;
    case TFmode:
      goto L1093;
    }
  goto ret0;

  L1083:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SFmode))
    {
      ro[1] = x3;
      if (0 && TARGET_V9 && TARGET_FPU)
	return 214;
      }
  goto ret0;

  L1088:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DFmode))
    {
      ro[1] = x3;
      if (0 && TARGET_V9 && TARGET_FPU)
	return 215;
      }
  goto ret0;

  L1093:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, TFmode))
    {
      ro[1] = x3;
      if (0 && TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 216;
      }
  goto ret0;

  L1105:
  x2 = XEXP (x1, 0);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1106;
    }
  goto ret0;

  L1106:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    goto L1112;
  goto ret0;

  L1112:
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (! TARGET_V9)
	{
	  *pnum_clobbers = 1;
	  return 218;
	}
      }
  L1113:
  ro[2] = x2;
  if (TARGET_V9)
    return 219;
  goto ret0;

  L1170:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1171;
    }
  goto ret0;

  L1171:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    goto L1177;
  goto ret0;

  L1177:
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (! TARGET_V9)
	{
	  *pnum_clobbers = 1;
	  return 226;
	}
      }
  L1178:
  ro[2] = x2;
  if (TARGET_V9)
    return 227;
  goto ret0;

  L1232:
  x2 = XEXP (x1, 0);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1233;
    }
  L1250:
  if (GET_MODE (x2) != DImode)
    goto ret0;
  switch (GET_CODE (x2))
    {
    case SIGN_EXTEND:
      goto L1251;
    case ZERO_EXTEND:
      goto L1283;
    }
  goto ret0;

  L1233:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 234;
      }
  x2 = XEXP (x1, 0);
  goto L1250;

  L1251:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1252;
    }
  goto ret0;

  L1252:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == SIGN_EXTEND && 1)
    goto L1253;
  L1259:
  if (small_int (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V8 || TARGET_SPARCLITE)
	return 238;
      }
  goto ret0;

  L1253:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      if (TARGET_V8 || TARGET_SPARCLITE)
	return 237;
      }
  goto L1259;

  L1283:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1284;
    }
  goto ret0;

  L1284:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == ZERO_EXTEND && 1)
    goto L1285;
  L1291:
  if (uns_small_int (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V8 || TARGET_SPARCLITE)
	return 244;
      }
  goto ret0;

  L1285:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      if (TARGET_V8 || TARGET_SPARCLITE)
	return 243;
      }
  goto L1291;

  L1327:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1328;
    }
  goto ret0;

  L1328:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 249;
      }
  goto ret0;

  L1365:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1366;
    }
  goto ret0;

  L1366:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 252;
      }
  goto ret0;

  L1383:
  x2 = XEXP (x1, 0);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1384;
    }
  L1408:
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == NOT && 1)
    goto L1409;
  goto ret0;

  L1384:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    goto L1390;
  x2 = XEXP (x1, 0);
  goto L1408;

  L1390:
  ro[2] = x2;
  if (! TARGET_V9)
    return 255;
  L1391:
  ro[2] = x2;
  if (TARGET_V9)
    return 256;
  x2 = XEXP (x1, 0);
  goto L1408;

  L1409:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1410;
    }
  goto ret0;

  L1410:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DImode))
    goto L1417;
  goto ret0;

  L1417:
  ro[2] = x2;
  if (! TARGET_V9)
    return 259;
  L1418:
  ro[2] = x2;
  if (TARGET_V9)
    return 260;
  goto ret0;

  L1428:
  x2 = XEXP (x1, 0);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1429;
    }
  L1453:
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == NOT && 1)
    goto L1454;
  goto ret0;

  L1429:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    goto L1435;
  x2 = XEXP (x1, 0);
  goto L1453;

  L1435:
  ro[2] = x2;
  if (! TARGET_V9)
    return 263;
  L1436:
  ro[2] = x2;
  if (TARGET_V9)
    return 264;
  x2 = XEXP (x1, 0);
  goto L1453;

  L1454:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1455;
    }
  goto ret0;

  L1455:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DImode))
    goto L1462;
  goto ret0;

  L1462:
  ro[2] = x2;
  if (! TARGET_V9)
    return 267;
  L1463:
  ro[2] = x2;
  if (TARGET_V9)
    return 268;
  goto ret0;

  L1473:
  x2 = XEXP (x1, 0);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1474;
    }
  goto ret0;

  L1474:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    goto L1480;
  goto ret0;

  L1480:
  ro[2] = x2;
  if (! TARGET_V9)
    return 271;
  L1481:
  ro[2] = x2;
  if (TARGET_V9)
    return 272;
  goto ret0;

  L1507:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case DImode:
      if (GET_CODE (x2) == XOR && 1)
	goto L1508;
      if (register_operand (x2, DImode))
	{
	  ro[1] = x2;
	  if (! TARGET_V9)
	    return 300;
	  }
    }
  L1706:
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      if (TARGET_V9)
	return 301;
      }
  goto ret0;

  L1508:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1509;
    }
  L1514:
  if (reg_or_0_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1515;
    }
  goto L1706;

  L1509:
  x3 = XEXP (x2, 1);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      if (! TARGET_V9)
	return 276;
      }
  x3 = XEXP (x2, 0);
  goto L1514;

  L1515:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[2] = x3;
      if (TARGET_V9)
	return 277;
      }
  goto L1706;

  L1654:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    goto L1659;
  goto ret0;

  L1659:
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x2;
      if (! TARGET_V9)
	{
	  *pnum_clobbers = 1;
	  return 292;
	}
      }
  L1660:
  ro[1] = x2;
  if (TARGET_V9)
    return 293;
  goto ret0;

  L1863:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1864;
    }
  goto ret0;

  L1864:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 331;
      }
  goto ret0;

  L1893:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1894;
    }
  goto ret0;

  L1894:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 335;
      }
  goto ret0;

  L1903:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L1904;
    }
  goto ret0;

  L1904:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 337;
      }
  goto ret0;

  L673:
  x2 = XEXP (x1, 0);
  if (comparison_operator (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L674;
    }
  L737:
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L738;
    }
  goto ret0;

  L674:
  x3 = XEXP (x2, 0);
  switch (GET_MODE (x3))
    {
    case CCmode:
      switch (GET_CODE (x3))
	{
	case REG:
	  if (XINT (x3, 0) == 0 && 1)
	    goto L675;
	}
      break;
    case CCXmode:
      if (GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
	goto L691;
    }
  L714:
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[2] = x3;
      goto L715;
    }
  L722:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[2] = x3;
      goto L723;
    }
  goto L737;

  L675:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L676;
  x3 = XEXP (x2, 0);
  goto L714;

  L676:
  x2 = XEXP (x1, 1);
  if (arith11_double_operand (x2, DImode))
    {
      ro[2] = x2;
      goto L677;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L714;

  L677:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9)
	return 133;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L714;

  L691:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L692;
  x3 = XEXP (x2, 0);
  goto L714;

  L692:
  x2 = XEXP (x1, 1);
  if (arith11_double_operand (x2, DImode))
    {
      ro[2] = x2;
      goto L693;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L714;

  L693:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9)
	return 135;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L714;

  L715:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L716;
  x3 = XEXP (x2, 0);
  goto L722;

  L716:
  x2 = XEXP (x1, 1);
  if (arith11_double_operand (x2, DImode))
    {
      ro[3] = x2;
      goto L717;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L722;

  L717:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 138;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L722;

  L723:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L724;
  goto L737;

  L724:
  x2 = XEXP (x1, 1);
  if (arith11_double_operand (x2, DImode))
    {
      ro[3] = x2;
      goto L725;
    }
  x2 = XEXP (x1, 0);
  goto L737;

  L725:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 139;
      }
  x2 = XEXP (x1, 0);
  goto L737;

  L738:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L739;
    }
  goto ret0;

  L739:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L740;
  goto ret0;

  L740:
  x2 = XEXP (x1, 1);
  if (arith10_double_operand (x2, DImode))
    {
      ro[3] = x2;
      goto L741;
    }
  goto ret0;

  L741:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DImode))
    {
      ro[4] = x2;
      if (TARGET_V9)
	return 141;
      }
  goto ret0;
 ret0: return -1;
}

int
recog_4 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case DFmode:
      switch (GET_CODE (x1))
	{
	case FLOAT_EXTEND:
	  goto L962;
	case FLOAT_TRUNCATE:
	  goto L982;
	case FLOAT:
	  goto L990;
	case PLUS:
	  goto L1753;
	case MINUS:
	  goto L1768;
	case MULT:
	  goto L1793;
	case DIV:
	  goto L1812;
	case NEG:
	  goto L1826;
	case ABS:
	  goto L1838;
	case SQRT:
	  goto L1850;
	}
    }
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L753;
  goto ret0;

  L962:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 187;
      }
  goto ret0;

  L982:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 192;
      }
  goto ret0;

  L990:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU)
	    return 194;
	  }
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[1] = x2;
	  if (0 && TARGET_V9 && TARGET_FPU)
	    return 203;
	  }
    }
  goto ret0;

  L1753:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      goto L1754;
    }
  goto ret0;

  L1754:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 308;
      }
  goto ret0;

  L1768:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      goto L1769;
    }
  goto ret0;

  L1769:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 311;
      }
  goto ret0;

  L1793:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != DFmode)
    goto ret0;
  if (GET_CODE (x2) == FLOAT_EXTEND && 1)
    goto L1794;
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      goto L1784;
    }
  goto ret0;

  L1794:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SFmode))
    {
      ro[1] = x3;
      goto L1795;
    }
  goto ret0;

  L1795:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DFmode && GET_CODE (x2) == FLOAT_EXTEND && 1)
    goto L1796;
  goto ret0;

  L1796:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SFmode))
    {
      ro[2] = x3;
      if ((TARGET_V8 || TARGET_V9) && TARGET_FPU)
	return 316;
      }
  goto ret0;

  L1784:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 314;
      }
  goto ret0;

  L1812:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      goto L1813;
    }
  goto ret0;

  L1813:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 319;
      }
  goto ret0;

  L1826:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 322;
      }
  goto ret0;

  L1838:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 325;
      }
  goto ret0;

  L1850:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 328;
      }
  goto ret0;

  L753:
  x2 = XEXP (x1, 0);
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L754;
    }
  L785:
  if (comparison_operator (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L786;
    }
  goto ret0;

  L754:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L755;
    }
  goto L785;

  L755:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L756;
  goto L785;

  L756:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[3] = x2;
      goto L757;
    }
  x2 = XEXP (x1, 0);
  goto L785;

  L757:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 143;
      }
  x2 = XEXP (x1, 0);
  goto L785;

  L786:
  x3 = XEXP (x2, 0);
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[2] = x3;
      goto L787;
    }
  L794:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[2] = x3;
      goto L795;
    }
  L826:
  if (GET_CODE (x3) != REG)
    goto ret0;
  switch (GET_MODE (x3))
    {
    case CCmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L827;
      break;
    case CCXmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L851;
    }
  goto ret0;

  L787:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L788;
  x3 = XEXP (x2, 0);
  goto L794;

  L788:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[3] = x2;
      goto L789;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L794;

  L789:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 147;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L794;

  L795:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L796;
  x3 = XEXP (x2, 0);
  goto L826;

  L796:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[3] = x2;
      goto L797;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L826;

  L797:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 148;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L826;

  L827:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L828;
  goto ret0;

  L828:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L829;
    }
  goto ret0;

  L829:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 152;
      }
  goto ret0;

  L851:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L852;
  goto ret0;

  L852:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L853;
    }
  goto ret0;

  L853:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, DFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 155;
      }
  goto ret0;
 ret0: return -1;
}

int
recog_5 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case TFmode:
      switch (GET_CODE (x1))
	{
	case FLOAT_EXTEND:
	  goto L966;
	case FLOAT:
	  goto L994;
	case PLUS:
	  goto L1748;
	case MINUS:
	  goto L1763;
	case MULT:
	  goto L1800;
	case DIV:
	  goto L1807;
	case NEG:
	  goto L1822;
	case ABS:
	  goto L1834;
	case SQRT:
	  goto L1846;
	}
    }
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L761;
  goto ret0;

  L966:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU && TARGET_HARD_QUAD)
	    return 188;
	  }
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU && TARGET_HARD_QUAD)
	    return 189;
	  }
    }
  goto ret0;

  L994:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU && TARGET_HARD_QUAD)
	    return 195;
	  }
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[1] = x2;
	  if (0 && TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	    return 204;
	  }
    }
  goto ret0;

  L1748:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      goto L1749;
    }
  goto ret0;

  L1749:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 307;
      }
  goto ret0;

  L1763:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      goto L1764;
    }
  goto ret0;

  L1764:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 310;
      }
  goto ret0;

  L1800:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != TFmode)
    goto ret0;
  if (GET_CODE (x2) == FLOAT_EXTEND && 1)
    goto L1801;
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      goto L1779;
    }
  goto ret0;

  L1801:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DFmode))
    {
      ro[1] = x3;
      goto L1802;
    }
  goto ret0;

  L1802:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == TFmode && GET_CODE (x2) == FLOAT_EXTEND && 1)
    goto L1803;
  goto ret0;

  L1803:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DFmode))
    {
      ro[2] = x3;
      if ((TARGET_V8 || TARGET_V9) && TARGET_FPU)
	return 317;
      }
  goto ret0;

  L1779:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 313;
      }
  goto ret0;

  L1807:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      goto L1808;
    }
  goto ret0;

  L1808:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 318;
      }
  goto ret0;

  L1822:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 321;
      }
  goto ret0;

  L1834:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 324;
      }
  goto ret0;

  L1846:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU && TARGET_HARD_QUAD)
	return 327;
      }
  goto ret0;

  L761:
  x2 = XEXP (x1, 0);
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L762;
    }
  L801:
  if (comparison_operator (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L802;
    }
  goto ret0;

  L762:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L763;
    }
  goto L801;

  L763:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L764;
  goto L801;

  L764:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[3] = x2;
      goto L765;
    }
  x2 = XEXP (x1, 0);
  goto L801;

  L765:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, TFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 144;
      }
  x2 = XEXP (x1, 0);
  goto L801;

  L802:
  x3 = XEXP (x2, 0);
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[2] = x3;
      goto L803;
    }
  L810:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[2] = x3;
      goto L811;
    }
  L834:
  if (GET_CODE (x3) != REG)
    goto ret0;
  switch (GET_MODE (x3))
    {
    case CCmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L835;
      break;
    case CCXmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L859;
    }
  goto ret0;

  L803:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L804;
  x3 = XEXP (x2, 0);
  goto L810;

  L804:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[3] = x2;
      goto L805;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L810;

  L805:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, TFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 149;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L810;

  L811:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L812;
  x3 = XEXP (x2, 0);
  goto L834;

  L812:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[3] = x2;
      goto L813;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L834;

  L813:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, TFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 150;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L834;

  L835:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L836;
  goto ret0;

  L836:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      goto L837;
    }
  goto ret0;

  L837:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, TFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 153;
      }
  goto ret0;

  L859:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L860;
  goto ret0;

  L860:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      goto L861;
    }
  goto ret0;

  L861:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, TFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 156;
      }
  goto ret0;
 ret0: return -1;
}

int
recog_6 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XEXP (x0, 0);
  switch (GET_MODE (x1))
    {
    case CCmode:
      switch (GET_CODE (x1))
	{
	case REG:
	  if (XINT (x1, 0) == 0 && 1)
	    goto L2;
	}
      break;
    case CCFPEmode:
      if (GET_CODE (x1) == REG && XINT (x1, 0) == 0 && 1)
	goto L7;
    L41:
      if (ccfp_reg_operand (x1, CCFPEmode))
	{
	  ro[0] = x1;
	  goto L42;
	}
      break;
    case CCFPmode:
      if (GET_CODE (x1) == REG && XINT (x1, 0) == 0 && 1)
	goto L22;
    L56:
      if (ccfp_reg_operand (x1, CCFPmode))
	{
	  ro[0] = x1;
	  goto L57;
	}
      break;
    case CCXmode:
      if (GET_CODE (x1) == REG && XINT (x1, 0) == 0 && 1)
	goto L37;
      break;
    case SImode:
      if (register_operand (x1, SImode))
	{
	  ro[0] = x1;
	  goto L80;
	}
      break;
    case DImode:
      if (register_operand (x1, DImode))
	{
	  ro[0] = x1;
	  goto L108;
	}
    L588:
      if (reg_or_nonsymb_mem_operand (x1, DImode))
	{
	  ro[0] = x1;
	  goto L589;
	}
      break;
    case HImode:
      if (register_operand (x1, HImode))
	{
	  ro[0] = x1;
	  goto L447;
	}
    }
  L354:
  if (GET_CODE (x1) == PC && 1)
    goto L355;
  L535:
  switch (GET_MODE (x1))
    {
    case QImode:
      if (reg_or_nonsymb_mem_operand (x1, QImode))
	{
	  ro[0] = x1;
	  goto L536;
	}
    L538:
      if (register_operand (x1, QImode))
	{
	  ro[0] = x1;
	  goto L539;
	}
      if (GET_CODE (x1) == MEM && 1)
	goto L552;
      break;
    case HImode:
    L555:
      if (reg_or_nonsymb_mem_operand (x1, HImode))
	{
	  ro[0] = x1;
	  goto L556;
	}
    L558:
      if (register_operand (x1, HImode))
	{
	  ro[0] = x1;
	  goto L559;
	}
      if (GET_CODE (x1) == MEM && 1)
	goto L571;
      break;
    case SImode:
    L574:
      if (reg_or_nonsymb_mem_operand (x1, SImode))
	{
	  ro[0] = x1;
	  goto L575;
	}
    L663:
      if (register_operand (x1, SImode))
	{
	  ro[0] = x1;
	  goto L864;
	}
      if (GET_CODE (x1) == MEM && 1)
	goto L585;
      break;
    case DImode:
      if (reg_or_nonsymb_mem_operand (x1, DImode))
	{
	  ro[0] = x1;
	  goto L592;
	}
    L671:
      if (register_operand (x1, DImode))
	{
	  ro[0] = x1;
	  goto L876;
	}
      break;
    case SFmode:
      if (general_operand (x1, SFmode))
	{
	  ro[0] = x1;
	  goto L595;
	}
    L597:
      if (reg_or_nonsymb_mem_operand (x1, SFmode))
	{
	  ro[0] = x1;
	  goto L598;
	}
    L743:
      if (register_operand (x1, SFmode))
	{
	  ro[0] = x1;
	  goto L973;
	}
      if (GET_CODE (x1) == MEM && 1)
	goto L613;
      break;
    case DFmode:
      if (general_operand (x1, DFmode))
	{
	  ro[0] = x1;
	  goto L617;
	}
    L619:
      if (reg_or_nonsymb_mem_operand (x1, DFmode))
	{
	  ro[0] = x1;
	  goto L620;
	}
    L751:
      if (register_operand (x1, DFmode))
	{
	  ro[0] = x1;
	  goto L961;
	}
      if (GET_CODE (x1) == MEM && 1)
	goto L638;
      break;
    case TFmode:
      if (general_operand (x1, TFmode))
	{
	  ro[0] = x1;
	  goto L642;
	}
    L644:
      if (reg_or_nonsymb_mem_operand (x1, TFmode))
	{
	  ro[0] = x1;
	  goto L645;
	}
    L759:
      if (register_operand (x1, TFmode))
	{
	  ro[0] = x1;
	  goto L965;
	}
      switch (GET_CODE (x1))
	{
	case MEM:
	  goto L660;
	}
      break;
    case CC_NOOVmode:
      switch (GET_CODE (x1))
	{
	case REG:
	  if (XINT (x1, 0) == 0 && 1)
	    goto L1121;
	}
      break;
    case CCX_NOOVmode:
      switch (GET_CODE (x1))
	{
	case REG:
	  if (XINT (x1, 0) == 0 && 1)
	    goto L1128;
	}
    }
  if (GET_CODE (x1) == PC && 1)
    goto L1907;
  L2069:
  ro[0] = x1;
  goto L2070;
  L2127:
  if (GET_CODE (x1) == PC && 1)
    goto L2128;
  switch (GET_MODE (x1))
    {
    case SImode:
      if (register_operand (x1, SImode))
	{
	  ro[0] = x1;
	  goto L2147;
	}
      break;
    case DImode:
      if (register_operand (x1, DImode))
	{
	  ro[0] = x1;
	  goto L2158;
	}
    }
  goto ret0;

  L2:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCmode && GET_CODE (x1) == COMPARE && 1)
    goto L889;
  x1 = XEXP (x0, 0);
  goto L2069;

  L889:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SImode:
      switch (GET_CODE (x2))
	{
	case ZERO_EXTEND:
	  goto L890;
	case ZERO_EXTRACT:
	  goto L947;
	case SUBREG:
	case REG:
	  if (register_operand (x2, SImode))
	    {
	      ro[0] = x2;
	      goto L4;
	    }
	}
    L1525:
      if (cc_arithop (x2, SImode))
	{
	  ro[2] = x2;
	  goto L1526;
	}
    L1607:
      if (cc_arithopn (x2, SImode))
	{
	  ro[2] = x2;
	  goto L1608;
	}
      break;
    case QImode:
      switch (GET_CODE (x2))
	{
	case SUBREG:
	  if (XINT (x2, 1) == 0 && 1)
	    goto L907;
	}
    }
  L1561:
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NOT && 1)
    goto L1562;
  x1 = XEXP (x0, 0);
  goto L2069;

  L890:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, QImode))
    {
      ro[0] = x3;
      goto L891;
    }
  goto L1525;

  L891:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 169;
  x2 = XEXP (x1, 0);
  goto L1525;

  L947:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L948;
    }
  goto L1525;

  L948:
  x3 = XEXP (x2, 1);
  if (small_int (x3, SImode))
    {
      ro[1] = x3;
      goto L949;
    }
  goto L1525;

  L949:
  x3 = XEXP (x2, 2);
  if (small_int (x3, SImode))
    {
      ro[2] = x3;
      goto L950;
    }
  goto L1525;

  L950:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (INTVAL (operands[2]) > 19)
      return 185;
  x2 = XEXP (x1, 0);
  goto L1525;

  L4:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      return 23;
    }
  x2 = XEXP (x1, 0);
  goto L1525;

  L1526:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1527;
    }
  goto L1607;

  L1527:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1528;
    }
  goto L1607;

  L1528:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 279;
  x2 = XEXP (x1, 0);
  goto L1607;

  L1608:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == NOT && 1)
    goto L1609;
  goto L1561;

  L1609:
  x4 = XEXP (x3, 0);
  if (arith_operand (x4, SImode))
    {
      ro[0] = x4;
      goto L1610;
    }
  goto L1561;

  L1610:
  x3 = XEXP (x2, 1);
  if (reg_or_0_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1611;
    }
  goto L1561;

  L1611:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 287;
  x2 = XEXP (x1, 0);
  goto L1561;

  L907:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L908;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L908:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 171;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1562:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == XOR && 1)
    goto L1563;
  L1715:
  if (arith_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1716;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1563:
  x4 = XEXP (x3, 0);
  if (reg_or_0_operand (x4, SImode))
    {
      ro[0] = x4;
      goto L1564;
    }
  goto L1715;

  L1564:
  x4 = XEXP (x3, 1);
  if (arith_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1565;
    }
  goto L1715;

  L1565:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 283;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1715;

  L1716:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 303;
  x1 = XEXP (x0, 0);
  goto L2069;

  L7:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCFPEmode && GET_CODE (x1) == COMPARE && 1)
    goto L8;
  x1 = XEXP (x0, 0);
  goto L41;

  L8:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[0] = x2;
	  goto L9;
	}
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[0] = x2;
	  goto L14;
	}
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[0] = x2;
	  goto L19;
	}
    }
  x1 = XEXP (x0, 0);
  goto L41;

  L9:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU)
	return 24;
      }
  x1 = XEXP (x0, 0);
  goto L41;

  L14:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU)
	return 25;
      }
  x1 = XEXP (x0, 0);
  goto L41;

  L19:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 26;
      }
  x1 = XEXP (x0, 0);
  goto L41;

  L42:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCFPEmode && GET_CODE (x1) == COMPARE && 1)
    goto L43;
  x1 = XEXP (x0, 0);
  goto L354;

  L43:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[1] = x2;
	  goto L44;
	}
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[1] = x2;
	  goto L49;
	}
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[1] = x2;
	  goto L54;
	}
    }
  x1 = XEXP (x0, 0);
  goto L354;

  L44:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 31;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L49:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 32;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L54:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 33;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L22:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCFPmode && GET_CODE (x1) == COMPARE && 1)
    goto L23;
  x1 = XEXP (x0, 0);
  goto L56;

  L23:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[0] = x2;
	  goto L24;
	}
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[0] = x2;
	  goto L29;
	}
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[0] = x2;
	  goto L34;
	}
    }
  x1 = XEXP (x0, 0);
  goto L56;

  L24:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU)
	return 27;
      }
  x1 = XEXP (x0, 0);
  goto L56;

  L29:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU)
	return 28;
      }
  x1 = XEXP (x0, 0);
  goto L56;

  L34:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[1] = x2;
      if (! TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 29;
      }
  x1 = XEXP (x0, 0);
  goto L56;

  L57:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCFPmode && GET_CODE (x1) == COMPARE && 1)
    goto L58;
  x1 = XEXP (x0, 0);
  goto L354;

  L58:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[1] = x2;
	  goto L59;
	}
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[1] = x2;
	  goto L64;
	}
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[1] = x2;
	  goto L69;
	}
    }
  x1 = XEXP (x0, 0);
  goto L354;

  L59:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 34;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L64:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 35;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L69:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, TFmode))
    {
      ro[2] = x2;
      if (TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 36;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L37:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCXmode && GET_CODE (x1) == COMPARE && 1)
    goto L954;
  x1 = XEXP (x0, 0);
  goto L2069;

  L954:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != DImode)
    {
      x1 = XEXP (x0, 0);
      goto L2069;
    }
  if (GET_CODE (x2) == ZERO_EXTRACT && 1)
    goto L955;
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L39;
    }
  L1532:
  if (cc_arithop (x2, DImode))
    {
      ro[2] = x2;
      goto L1533;
    }
  L1615:
  if (cc_arithopn (x2, DImode))
    {
      ro[2] = x2;
      goto L1616;
    }
  L1569:
  if (GET_CODE (x2) == NOT && 1)
    goto L1570;
  x1 = XEXP (x0, 0);
  goto L2069;

  L955:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L956;
    }
  goto L1532;

  L956:
  x3 = XEXP (x2, 1);
  if (small_int (x3, SImode))
    {
      ro[1] = x3;
      goto L957;
    }
  goto L1532;

  L957:
  x3 = XEXP (x2, 2);
  if (small_int (x3, SImode))
    {
      ro[2] = x3;
      goto L958;
    }
  goto L1532;

  L958:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9 && INTVAL (operands[2]) > 51)
      return 186;
  x2 = XEXP (x1, 0);
  goto L1532;

  L39:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      if (TARGET_V9)
	return 30;
      }
  x2 = XEXP (x1, 0);
  goto L1532;

  L1533:
  x3 = XEXP (x2, 0);
  if (arith_double_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1534;
    }
  goto L1615;

  L1534:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1535;
    }
  goto L1615;

  L1535:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 280;
  x2 = XEXP (x1, 0);
  goto L1615;

  L1616:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == DImode && GET_CODE (x3) == NOT && 1)
    goto L1617;
  goto L1569;

  L1617:
  x4 = XEXP (x3, 0);
  if (arith_double_operand (x4, DImode))
    {
      ro[0] = x4;
      goto L1618;
    }
  goto L1569;

  L1618:
  x3 = XEXP (x2, 1);
  if (reg_or_0_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1619;
    }
  goto L1569;

  L1619:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 288;
  x2 = XEXP (x1, 0);
  goto L1569;

  L1570:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == DImode && GET_CODE (x3) == XOR && 1)
    goto L1571;
  L1721:
  if (arith_double_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1722;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1571:
  x4 = XEXP (x3, 0);
  if (reg_or_0_operand (x4, DImode))
    {
      ro[0] = x4;
      goto L1572;
    }
  goto L1721;

  L1572:
  x4 = XEXP (x3, 1);
  if (arith_double_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1573;
    }
  goto L1721;

  L1573:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 284;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1721;

  L1722:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 304;
  x1 = XEXP (x0, 0);
  goto L2069;
 L80:
  tem = recog_1 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x1 = XEXP (x0, 0);
  goto L574;

  L108:
  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case DImode:
      switch (GET_CODE (x1))
	{
	case NE:
	  goto L109;
	case NEG:
	  goto L123;
	case EQ:
	  goto L165;
	}
    }
  L350:
  if (noov_compare_op (x1, DImode))
    {
      ro[1] = x1;
      goto L351;
    }
  L465:
  if (GET_MODE (x1) != DImode)
    {
      x1 = XEXP (x0, 0);
      goto L588;
    }
  switch (GET_CODE (x1))
    {
    case LO_SUM:
      goto L466;
    case HIGH:
      goto L478;
    }
  x1 = XEXP (x0, 0);
  goto L588;

  L109:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L110;
    }
  goto L350;

  L110:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && pnum_clobbers != 0 && 1)
    if (TARGET_V9)
      {
	*pnum_clobbers = 1;
	return 39;
      }
  goto L350;

  L123:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != DImode)
    {
      goto L350;
    }
  switch (GET_CODE (x2))
    {
    case NE:
      goto L124;
    case EQ:
      goto L180;
    }
  goto L350;

  L124:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L125;
    }
  goto L350;

  L125:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    if (TARGET_V9)
      {
	*pnum_clobbers = 1;
	return 40;
      }
  goto L350;

  L180:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L181;
    }
  goto L350;

  L181:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    if (TARGET_V9)
      {
	*pnum_clobbers = 1;
	return 44;
      }
  goto L350;

  L165:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L166;
    }
  goto L350;

  L166:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && pnum_clobbers != 0 && 1)
    if (TARGET_V9)
      {
	*pnum_clobbers = 1;
	return 43;
      }
  goto L350;

  L351:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L352;
  goto L465;

  L352:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 63;
  goto L465;

  L466:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L467;
    }
  x1 = XEXP (x0, 0);
  goto L588;

  L467:
  x2 = XEXP (x1, 1);
  if (immediate_operand (x2, DImode))
    goto L473;
  x1 = XEXP (x0, 0);
  goto L588;

  L473:
  ro[2] = x2;
  if (! TARGET_V9)
    return 89;
  L474:
  ro[2] = x2;
  if (TARGET_V9)
    return 90;
  x1 = XEXP (x0, 0);
  goto L588;

  L478:
  x2 = XEXP (x1, 0);
  ro[1] = x2;
  if (! TARGET_V9 && check_pic (1))
    return 91;
  L489:
  if (pnum_clobbers != 0 && const_double_operand (x2, VOIDmode))
    {
      ro[1] = x2;
      if (TARGET_V9 && check_pic (1))
	{
	  *pnum_clobbers = 1;
	  return 92;
	}
      }
  L500:
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x2;
      if (TARGET_MEDLOW && check_pic (1))
	{
	  *pnum_clobbers = 1;
	  return 93;
	}
      }
  L511:
  if (pnum_clobbers != 0 && data_segment_operand (x2, VOIDmode))
    {
      ro[1] = x2;
      if (TARGET_MEDANY && check_pic (1))
	{
	  *pnum_clobbers = 1;
	  return 94;
	}
      }
  L522:
  if (pnum_clobbers != 0 && text_segment_operand (x2, VOIDmode))
    {
      ro[1] = x2;
      if (TARGET_MEDANY && check_pic (1))
	{
	  *pnum_clobbers = 1;
	  return 95;
	}
      }
  L533:
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x2;
      if (TARGET_FULLANY && check_pic (1))
	{
	  *pnum_clobbers = 1;
	  return 96;
	}
      }
  x1 = XEXP (x0, 0);
  goto L588;

  L589:
  x1 = XEXP (x0, 1);
  if (general_operand (x1, DImode))
    {
      ro[1] = x1;
      if (! TARGET_V9
   && (register_operand (operands[0], DImode)
       || register_operand (operands[1], DImode)
       || operands[1] == const0_rtx))
	return 109;
      }
  x1 = XEXP (x0, 0);
  goto L354;

  L447:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == HImode && GET_CODE (x1) == HIGH && 1)
    goto L448;
  x1 = XEXP (x0, 0);
  goto L555;

  L448:
  x2 = XEXP (x1, 0);
  ro[1] = x2;
  if (check_pic (1))
    return 86;
  x1 = XEXP (x0, 0);
  goto L555;

  L355:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L356;
  x1 = XEXP (x0, 0);
  goto L535;

  L356:
  x2 = XEXP (x1, 0);
  if (noov_compare_op (x2, VOIDmode))
    {
      ro[0] = x2;
      goto L357;
    }
  L374:
  if (comparison_operator (x2, VOIDmode))
    {
      ro[0] = x2;
      goto L375;
    }
  L410:
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[0] = x2;
      goto L411;
    }
  x1 = XEXP (x0, 0);
  goto L535;

  L357:
  x3 = XEXP (x2, 0);
  if (GET_CODE (x3) == REG && XINT (x3, 0) == 0 && 1)
    goto L358;
  goto L374;

  L358:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L359;
  goto L374;

  L359:
  x2 = XEXP (x1, 1);
  switch (GET_CODE (x2))
    {
    case LABEL_REF:
      goto L360;
    case PC:
      goto L369;
    }
  x2 = XEXP (x1, 0);
  goto L374;

  L360:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  goto L361;

  L361:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == PC && 1)
    return 74;
  x2 = XEXP (x1, 0);
  goto L374;

  L369:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L370;
  x2 = XEXP (x1, 0);
  goto L374;

  L370:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  return 75;

  L375:
  x3 = XEXP (x2, 0);
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[1] = x3;
      goto L376;
    }
  L393:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[1] = x3;
      goto L394;
    }
  goto L410;

  L376:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L377;
  x3 = XEXP (x2, 0);
  goto L393;

  L377:
  x2 = XEXP (x1, 1);
  switch (GET_CODE (x2))
    {
    case LABEL_REF:
      goto L378;
    case PC:
      goto L387;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L393;

  L378:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  goto L379;

  L379:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == PC && 1)
    if (TARGET_V9)
      return 76;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L393;

  L387:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L388;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L393;

  L388:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  if (TARGET_V9)
    return 77;
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L393;

  L394:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L395;
  goto L410;

  L395:
  x2 = XEXP (x1, 1);
  switch (GET_CODE (x2))
    {
    case LABEL_REF:
      goto L396;
    case PC:
      goto L405;
    }
  x2 = XEXP (x1, 0);
  goto L410;

  L396:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  goto L397;

  L397:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == PC && 1)
    if (TARGET_V9)
      return 78;
  x2 = XEXP (x1, 0);
  goto L410;

  L405:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L406;
  x2 = XEXP (x1, 0);
  goto L410;

  L406:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  if (TARGET_V9)
    return 79;
  x2 = XEXP (x1, 0);
  goto L410;

  L411:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L412;
    }
  x1 = XEXP (x0, 0);
  goto L535;

  L412:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L413;
  x1 = XEXP (x0, 0);
  goto L535;

  L413:
  x2 = XEXP (x1, 1);
  switch (GET_CODE (x2))
    {
    case LABEL_REF:
      goto L414;
    case PC:
      goto L423;
    }
  x1 = XEXP (x0, 0);
  goto L535;

  L414:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  goto L415;

  L415:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == PC && 1)
    if (TARGET_V9)
      return 80;
  x1 = XEXP (x0, 0);
  goto L535;

  L423:
  x2 = XEXP (x1, 2);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L424;
  x1 = XEXP (x0, 0);
  goto L535;

  L424:
  x3 = XEXP (x2, 0);
  ro[2] = x3;
  if (TARGET_V9)
    return 81;
  x1 = XEXP (x0, 0);
  goto L535;

  L536:
  x1 = XEXP (x0, 1);
  if (move_operand (x1, QImode))
    {
      ro[1] = x1;
      if (register_operand (operands[0], QImode)
   || register_operand (operands[1], QImode)
   || operands[1] == const0_rtx)
	return 98;
      }
  x1 = XEXP (x0, 0);
  goto L538;

  L539:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == QImode && GET_CODE (x1) == SUBREG && XINT (x1, 1) == 0 && 1)
    goto L540;
  x1 = XEXP (x0, 0);
  goto L2069;

  L540:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == LO_SUM && 1)
    goto L541;
  x1 = XEXP (x0, 0);
  goto L2069;

  L541:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, QImode))
    {
      ro[1] = x3;
      goto L542;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L542:
  x3 = XEXP (x2, 1);
  if (immediate_operand (x3, VOIDmode))
    {
      ro[2] = x3;
      return 99;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L552:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L553;
    }
  goto L2069;

  L553:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, QImode))
    {
      ro[1] = x1;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 100;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L556:
  x1 = XEXP (x0, 1);
  if (move_operand (x1, HImode))
    {
      ro[1] = x1;
      if (register_operand (operands[0], HImode)
   || register_operand (operands[1], HImode)
   || operands[1] == const0_rtx)
	return 102;
      }
  x1 = XEXP (x0, 0);
  goto L558;

  L559:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) != HImode)
    {
      x1 = XEXP (x0, 0);
      goto L2069;
    }
  switch (GET_CODE (x1))
    {
    case LO_SUM:
      goto L560;
    case ZERO_EXTEND:
      goto L869;
    case SIGN_EXTEND:
      goto L926;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L560:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, HImode))
    {
      ro[1] = x2;
      goto L561;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L561:
  x2 = XEXP (x1, 1);
  if (immediate_operand (x2, VOIDmode))
    {
      ro[2] = x2;
      return 103;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L869:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == QImode && sparc_operand (x2, QImode))
    {
      ro[1] = x2;
      if (GET_CODE (operands[1]) != CONST_INT)
	return 160;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L926:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, QImode))
    {
      ro[1] = x2;
      return 176;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L571:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L572;
    }
  goto L2069;

  L572:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, HImode))
    {
      ro[1] = x1;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 104;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L575:
  x1 = XEXP (x0, 1);
  if (move_operand (x1, SImode))
    {
      ro[1] = x1;
      if (register_operand (operands[0], SImode)
   || register_operand (operands[1], SImode)
   || operands[1] == const0_rtx)
	return 106;
      }
  x1 = XEXP (x0, 0);
  goto L663;
 L864:
  tem = recog_2 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x1 = XEXP (x0, 0);
  goto L2069;

  L585:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L586;
    }
  goto L2069;

  L586:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, SImode))
    {
      ro[1] = x1;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 107;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L592:
  x1 = XEXP (x0, 1);
  if (move_operand (x1, DImode))
    {
      ro[1] = x1;
      if (TARGET_V9
   && (register_operand (operands[0], DImode)
       || register_operand (operands[1], DImode)
       || operands[1] == const0_rtx))
	return 110;
      }
  x1 = XEXP (x0, 0);
  goto L671;
 L876:
  tem = recog_3 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x1 = XEXP (x0, 0);
  goto L2069;

  L595:
  x1 = XEXP (x0, 1);
  ro[1] = x1;
  if (TARGET_FPU && GET_CODE (operands[1]) == CONST_DOUBLE)
    return 111;
  x1 = XEXP (x0, 0);
  goto L597;

  L598:
  x1 = XEXP (x0, 1);
  if (reg_or_nonsymb_mem_operand (x1, SFmode))
    goto L602;
  x1 = XEXP (x0, 0);
  goto L743;

  L602:
  ro[1] = x1;
  if (TARGET_FPU
   && (register_operand (operands[0], SFmode)
       || register_operand (operands[1], SFmode)))
    return 113;
  L603:
  ro[1] = x1;
  if (! TARGET_FPU
   && (register_operand (operands[0], SFmode)
       || register_operand (operands[1], SFmode)))
    return 114;
  x1 = XEXP (x0, 0);
  goto L743;

  L973:
  x1 = XEXP (x0, 1);
  switch (GET_MODE (x1))
    {
    case SFmode:
      switch (GET_CODE (x1))
	{
	case FLOAT_TRUNCATE:
	  goto L974;
	case FLOAT:
	  goto L986;
	case PLUS:
	  goto L1758;
	case MINUS:
	  goto L1773;
	case MULT:
	  goto L1788;
	case DIV:
	  goto L1817;
	case NEG:
	  goto L1830;
	case ABS:
	  goto L1842;
	case SQRT:
	  goto L1854;
	}
    }
  if (GET_CODE (x1) == IF_THEN_ELSE && 1)
    goto L745;
  x1 = XEXP (x0, 0);
  goto L2069;

  L974:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU)
	    return 190;
	  }
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU && TARGET_HARD_QUAD)
	    return 191;
	  }
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L986:
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[1] = x2;
	  if (TARGET_FPU)
	    return 193;
	  }
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[1] = x2;
	  if (0 && TARGET_V9 && TARGET_FPU)
	    return 202;
	  }
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1758:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L1759;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1759:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 309;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1773:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L1774;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1774:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 312;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1788:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L1789;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1789:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 315;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1817:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L1818;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1818:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      if (TARGET_FPU)
	return 320;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1830:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 323;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1842:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 326;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1854:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      if (TARGET_FPU)
	return 329;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L745:
  x2 = XEXP (x1, 0);
  if (v9_regcmp_op (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L746;
    }
  L769:
  if (comparison_operator (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L770;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L746:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L747;
    }
  goto L769;

  L747:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L748;
  goto L769;

  L748:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[3] = x2;
      goto L749;
    }
  x2 = XEXP (x1, 0);
  goto L769;

  L749:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 142;
      }
  x2 = XEXP (x1, 0);
  goto L769;

  L770:
  x3 = XEXP (x2, 0);
  if (ccfp_reg_operand (x3, CCFPmode))
    {
      ro[2] = x3;
      goto L771;
    }
  L778:
  if (ccfp_reg_operand (x3, CCFPEmode))
    {
      ro[2] = x3;
      goto L779;
    }
  L818:
  if (GET_CODE (x3) != REG)
    {
      x1 = XEXP (x0, 0);
    goto L2069;
    }
  switch (GET_MODE (x3))
    {
    case CCmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L819;
      break;
    case CCXmode:
      if (XINT (x3, 0) == 0 && 1)
	goto L843;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L771:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L772;
  x3 = XEXP (x2, 0);
  goto L778;

  L772:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[3] = x2;
      goto L773;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L778;

  L773:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 145;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L778;

  L779:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L780;
  x3 = XEXP (x2, 0);
  goto L818;

  L780:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[3] = x2;
      goto L781;
    }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L818;

  L781:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SFmode))
    {
      ro[4] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 146;
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L818;

  L819:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L820;
  x1 = XEXP (x0, 0);
  goto L2069;

  L820:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      goto L821;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L821:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 151;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L843:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L844;
  x1 = XEXP (x0, 0);
  goto L2069;

  L844:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[2] = x2;
      goto L845;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L845:
  x2 = XEXP (x1, 2);
  if (register_operand (x2, SFmode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 154;
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L613:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L614;
    }
  goto L2069;

  L614:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, SFmode))
    {
      ro[1] = x1;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 115;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L617:
  x1 = XEXP (x0, 1);
  ro[1] = x1;
  if (TARGET_FPU && GET_CODE (operands[1]) == CONST_DOUBLE)
    return 116;
  x1 = XEXP (x0, 0);
  goto L619;

  L620:
  x1 = XEXP (x0, 1);
  if (reg_or_nonsymb_mem_operand (x1, DFmode))
    goto L624;
  x1 = XEXP (x0, 0);
  goto L751;

  L624:
  ro[1] = x1;
  if (TARGET_FPU
   && (register_operand (operands[0], DFmode)
       || register_operand (operands[1], DFmode)))
    return 118;
  L625:
  ro[1] = x1;
  if (! TARGET_FPU
   && (register_operand (operands[0], DFmode)
       || register_operand (operands[1], DFmode)))
    return 119;
  x1 = XEXP (x0, 0);
  goto L751;
 L961:
  tem = recog_4 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x1 = XEXP (x0, 0);
  goto L2069;

  L638:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L639;
    }
  goto L2069;

  L639:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, DFmode))
    {
      ro[1] = x1;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 121;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L642:
  x1 = XEXP (x0, 1);
  ro[1] = x1;
  if (TARGET_FPU && GET_CODE (operands[1]) == CONST_DOUBLE)
    return 122;
  x1 = XEXP (x0, 0);
  goto L644;

  L645:
  x1 = XEXP (x0, 1);
  if (reg_or_nonsymb_mem_operand (x1, TFmode))
    goto L649;
  x1 = XEXP (x0, 0);
  goto L759;

  L649:
  ro[1] = x1;
  if (TARGET_FPU
   && (register_operand (operands[0], TFmode)
       || register_operand (operands[1], TFmode)))
    return 124;
  L650:
  ro[1] = x1;
  if (! TARGET_FPU
   && (register_operand (operands[0], TFmode)
       || register_operand (operands[1], TFmode)))
    return 125;
  x1 = XEXP (x0, 0);
  goto L759;
 L965:
  tem = recog_5 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x1 = XEXP (x0, 0);
  goto L2069;

  L660:
  x2 = XEXP (x1, 0);
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L661;
    }
  goto L2069;

  L661:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && reg_or_0_operand (x1, TFmode))
    {
      ro[1] = x1;
      if (0 && (reload_completed || reload_in_progress) && ! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 126;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1121:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CC_NOOVmode && GET_CODE (x1) == COMPARE && 1)
    goto L1122;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1122:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != SImode)
    {
      x1 = XEXP (x0, 0);
      goto L2069;
    }
  switch (GET_CODE (x2))
    {
    case PLUS:
      goto L1123;
    case MINUS:
      goto L1188;
    case NEG:
      goto L1669;
    case ASHIFT:
      goto L1869;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1123:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1124;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1124:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1125;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1125:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 221;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1188:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1189;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1189:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1190;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1190:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 229;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1669:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1670;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1670:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 295;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1869:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1870;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1870:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 1 && 1)
    goto L1871;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1871:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    return 332;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1128:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == CCX_NOOVmode && GET_CODE (x1) == COMPARE && 1)
    goto L1129;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1129:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) != DImode)
    {
      x1 = XEXP (x0, 0);
      goto L2069;
    }
  switch (GET_CODE (x2))
    {
    case PLUS:
      goto L1130;
    case MINUS:
      goto L1195;
    case NEG:
      goto L1675;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1130:
  x3 = XEXP (x2, 0);
  if (arith_double_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1131;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1131:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1132;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1132:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 222;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1195:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1196;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1196:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1197;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1197:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 230;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1675:
  x3 = XEXP (x2, 0);
  if (arith_double_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1676;
    }
  x1 = XEXP (x0, 0);
  goto L2069;

  L1676:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CONST_INT && XWINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 296;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1907:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == LABEL_REF && 1)
    goto L1908;
  x1 = XEXP (x0, 0);
  goto L2069;

  L1908:
  x2 = XEXP (x1, 0);
  ro[0] = x2;
  return 338;

  L2070:
  x1 = XEXP (x0, 1);
  if (GET_CODE (x1) == CALL && 1)
    goto L2071;
  x1 = XEXP (x0, 0);
  goto L2127;

  L2071:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == MEM && 1)
    goto L2072;
  x1 = XEXP (x0, 0);
  goto L2127;

  L2072:
  x3 = XEXP (x2, 0);
  if (address_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2073;
    }
  L2087:
  if (symbolic_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2088;
    }
  L2102:
  if (address_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L2103;
    }
  L2117:
  if (symbolic_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L2118;
    }
  x1 = XEXP (x0, 0);
  goto L2127;

  L2073:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 357;
	}
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2087;

  L2088:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 358;
	}
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2102;

  L2103:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 359;
	}
      }
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2117;

  L2118:
  x2 = XEXP (x1, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[2] = x2;
      if (TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 360;
	}
      }
  x1 = XEXP (x0, 0);
  goto L2127;

  L2128:
  x1 = XEXP (x0, 1);
  if (address_operand (x1, SImode))
    {
      ro[0] = x1;
      if (! TARGET_PTR64)
	return 368;
      }
  L2131:
  if (address_operand (x1, DImode))
    {
      ro[0] = x1;
      if (TARGET_PTR64)
	return 369;
      }
  goto ret0;

  L2147:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == SImode && GET_CODE (x1) == FFS && 1)
    goto L2148;
  goto ret0;

  L2148:
  x2 = XEXP (x1, 0);
  if (pnum_clobbers != 0 && register_operand (x2, SImode))
    {
      ro[1] = x2;
      if (TARGET_SPARCLITE)
	{
	  *pnum_clobbers = 1;
	  return 374;
	}
      }
  goto ret0;

  L2158:
  x1 = XEXP (x0, 1);
  if (GET_MODE (x1) == DImode && GET_CODE (x1) == FFS && 1)
    goto L2159;
  goto ret0;

  L2159:
  x2 = XEXP (x1, 0);
  if (pnum_clobbers != 0 && register_operand (x2, DImode))
    {
      ro[1] = x2;
      if (TARGET_V9)
	{
	  *pnum_clobbers = 1;
	  return 375;
	}
      }
  goto ret0;
 ret0: return -1;
}

int
recog_7 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  switch (GET_MODE (x2))
    {
    case SImode:
      switch (GET_CODE (x2))
	{
	case NE:
	  goto L74;
	case NEG:
	  goto L87;
	case EQ:
	  goto L130;
	case PLUS:
	  goto L186;
	case MINUS:
	  goto L203;
	case MULT:
	  goto L1238;
	case DIV:
	  goto L1315;
	case UDIV:
	  goto L1371;
	}
    }
  L452:
  if (move_pic_label (x2, SImode))
    {
      ro[1] = x2;
      goto L453;
    }
  goto ret0;

  L74:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L75;
    }
  goto L452;

  L75:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L76;
  goto L452;

  L76:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L77;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L77:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 37;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L87:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != SImode)
    {
      goto L452;
    }
  switch (GET_CODE (x3))
    {
    case NE:
      goto L88;
    case EQ:
      goto L144;
    }
  goto L452;

  L88:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L89;
    }
  goto L452;

  L89:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L90;
  goto L452;

  L90:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L91;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L91:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 38;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L144:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L145;
    }
  goto L452;

  L145:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L146;
  goto L452;

  L146:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L147;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L147:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 42;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L130:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L131;
    }
  goto L452;

  L131:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L132;
  goto L452;

  L132:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L133;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L133:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 41;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L186:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != SImode)
    {
      goto L452;
    }
  switch (GET_CODE (x3))
    {
    case NE:
      goto L187;
    case EQ:
      goto L221;
    }
  goto L452;

  L187:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L188;
    }
  goto L452;

  L188:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L189;
  goto L452;

  L189:
  x3 = XEXP (x2, 1);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L190;
    }
  goto L452;

  L190:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L191;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L191:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 45;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L221:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L222;
    }
  goto L452;

  L222:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L223;
  goto L452;

  L223:
  x3 = XEXP (x2, 1);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L224;
    }
  goto L452;

  L224:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L225;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L225:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 47;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L203:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L204;
    }
  goto L452;

  L204:
  x3 = XEXP (x2, 1);
  if (GET_MODE (x3) != SImode)
    {
      goto L452;
    }
  switch (GET_CODE (x3))
    {
    case NE:
      goto L205;
    case EQ:
      goto L239;
    }
  goto L452;

  L205:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L206;
    }
  goto L452;

  L206:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L207;
  goto L452;

  L207:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L208;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L208:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 46;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L239:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L240;
    }
  goto L452;

  L240:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L241;
  goto L452;

  L241:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L242;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L242:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return 48;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1238:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1239;
    }
  goto L452;

  L1239:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L1240;
    }
  goto L452;

  L1240:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1241;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1241:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CC_NOOVmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L1242;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1242:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CC_NOOVmode && GET_CODE (x2) == COMPARE && 1)
    goto L1243;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1243:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == MULT && 1)
    goto L1244;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1244:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1245;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1245:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    goto L1246;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1246:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    if (TARGET_V8 || TARGET_SPARCLITE)
      return 235;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1315:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1316;
    }
  goto L452;

  L1316:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L1317;
    }
  goto L452;

  L1317:
  x1 = XVECEXP (x0, 0, 1);
  switch (GET_CODE (x1))
    {
    case CLOBBER:
      goto L1318;
    case SET:
      goto L1351;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1318:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[3] = x2;
      if (TARGET_V8)
	return 248;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1351:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L1352;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1352:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == COMPARE && 1)
    goto L1353;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1353:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == DIV && 1)
    goto L1354;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1354:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1355;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1355:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    goto L1356;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1356:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && pnum_clobbers != 0 && 1)
    if (TARGET_V8)
      {
	*pnum_clobbers = 1;
	return 250;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1371:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1372;
    }
  goto L452;

  L1372:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L1373;
    }
  goto L452;

  L1373:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1374;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1374:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L1375;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1375:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == COMPARE && 1)
    goto L1376;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1376:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == UDIV && 1)
    goto L1377;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1377:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1378;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1378:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    goto L1379;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L1379:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    if (TARGET_V8)
      return 253;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L452;

  L453:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L454;
  goto ret0;

  L454:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    goto L455;
  goto ret0;

  L455:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PC && 1)
    return 87;
  goto ret0;
 ret0: return -1;
}

int
recog_8 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  switch (GET_MODE (x2))
    {
    case DImode:
      switch (GET_CODE (x2))
	{
	case NE:
	  goto L102;
	case NEG:
	  goto L115;
	case EQ:
	  goto L158;
	case HIGH:
	  goto L483;
	}
    }
  L459:
  if (move_pic_label (x2, DImode))
    {
      ro[1] = x2;
      goto L460;
    }
  L493:
  if (GET_MODE (x2) != DImode)
    goto ret0;
  switch (GET_CODE (x2))
    {
    case HIGH:
      goto L494;
    case PLUS:
      goto L1098;
    case MINUS:
      goto L1163;
    }
  goto ret0;

  L102:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L103;
    }
  goto L459;

  L103:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L104;
  goto L459;

  L104:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L105;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L105:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCXmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 39;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L115:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != DImode)
    {
      goto L459;
    }
  switch (GET_CODE (x3))
    {
    case NE:
      goto L116;
    case EQ:
      goto L172;
    case SUBREG:
    case REG:
      if (register_operand (x3, DImode))
	{
	  ro[1] = x3;
	  goto L1649;
	}
    }
  goto L459;

  L116:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L117;
    }
  goto L459;

  L117:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L118;
  goto L459;

  L118:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L119;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L119:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCXmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 40;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L172:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L173;
    }
  goto L459;

  L173:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L174;
  goto L459;

  L174:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L175;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L175:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCXmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 44;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L1649:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1650;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L1650:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (! TARGET_V9)
      return 292;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L158:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L159;
    }
  goto L459;

  L159:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L160;
  goto L459;

  L160:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L161;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L161:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCXmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (TARGET_V9)
      return 43;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L483:
  x3 = XEXP (x2, 0);
  if (const_double_operand (x3, VOIDmode))
    {
      ro[1] = x3;
      goto L484;
    }
  goto L459;

  L484:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L485;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L485:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    if (TARGET_V9 && check_pic (1))
      return 92;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L459;

  L460:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L461;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L493;

  L461:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    goto L462;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L493;

  L462:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == PC && 1)
    if (TARGET_V9)
      return 88;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L493;

  L494:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  goto L495;
  L505:
  if (data_segment_operand (x3, VOIDmode))
    {
      ro[1] = x3;
      goto L506;
    }
  L516:
  if (text_segment_operand (x3, VOIDmode))
    {
      ro[1] = x3;
      goto L517;
    }
  L527:
  ro[1] = x3;
  goto L528;

  L495:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L496;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L505;

  L496:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    if (TARGET_MEDLOW && check_pic (1))
      return 93;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L505;

  L506:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L507;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L516;

  L507:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    if (TARGET_MEDANY && check_pic (1))
      return 94;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L516;

  L517:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L518;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L527;

  L518:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    if (TARGET_MEDANY && check_pic (1))
      return 95;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L527;

  L528:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L529;
  goto ret0;

  L529:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 1 && 1)
    if (TARGET_FULLANY && check_pic (1))
      return 96;
  goto ret0;

  L1098:
  x3 = XEXP (x2, 0);
  if (arith_double_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1099;
    }
  goto ret0;

  L1099:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L1100;
    }
  goto ret0;

  L1100:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1101;
  goto ret0;

  L1101:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (! TARGET_V9)
      return 218;
  goto ret0;

  L1163:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1164;
    }
  goto ret0;

  L1164:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L1165;
    }
  goto ret0;

  L1165:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1166;
  goto ret0;

  L1166:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    if (! TARGET_V9)
      return 226;
  goto ret0;
 ret0: return -1;
}

int
recog_9 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[0] = x2;
	  goto L73;
	}
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[0] = x2;
	  goto L101;
	}
      break;
    case QImode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L546;
	}
      break;
    case HImode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L565;
	}
    }
  switch (GET_MODE (x2))
    {
    case SImode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L579;
	}
      break;
    case SFmode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L607;
	}
      break;
    case DFmode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L632;
	}
      break;
    case TFmode:
      switch (GET_CODE (x2))
	{
	case MEM:
	  goto L654;
	}
      break;
    case CCmode:
      switch (GET_CODE (x2))
	{
	case REG:
	  if (XINT (x2, 0) == 0 && 1)
	    goto L895;
	}
      break;
    case CC_NOOVmode:
      switch (GET_CODE (x2))
	{
	case REG:
	  if (XINT (x2, 0) == 0 && 1)
	    goto L1136;
	}
      break;
    case CCX_NOOVmode:
      switch (GET_CODE (x2))
	{
	case REG:
	  if (XINT (x2, 0) == 0 && 1)
	    goto L1149;
	}
      break;
    case CCXmode:
      switch (GET_CODE (x2))
	{
	case REG:
	  if (XINT (x2, 0) == 0 && 1)
	    goto L1550;
	}
    }
  if (GET_CODE (x2) == PC && 1)
    goto L1930;
  L2061:
  ro[0] = x2;
  goto L2062;
  L2140:
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[0] = x2;
	  goto L2141;
	}
    L2273:
      if (restore_operand (x2, SImode))
	{
	  ro[0] = x2;
	  goto L2274;
	}
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[0] = x2;
	  goto L2152;
	}
    L2290:
      if (restore_operand (x2, DImode))
	{
	  ro[0] = x2;
	  goto L2291;
	}
      break;
    case QImode:
      if (restore_operand (x2, QImode))
	{
	  ro[0] = x2;
	  goto L2264;
	}
      break;
    case HImode:
      if (restore_operand (x2, HImode))
	{
	  ro[0] = x2;
	  goto L2269;
	}
      break;
    case SFmode:
      if (restore_operand (x2, SFmode))
	{
	  ro[0] = x2;
	  goto L2279;
	}
    L2302:
      if (GET_CODE (x2) == REG && XINT (x2, 0) == 32 && 1)
	goto L2303;
    }
  goto ret0;
 L73:
  tem = recog_7 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x2 = XEXP (x1, 0);
  goto L2061;
 L101:
  tem = recog_8 (x0, insn, pnum_clobbers);
  if (tem >= 0) return tem;
  x2 = XEXP (x1, 0);
  goto L2061;

  L546:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L547;
    }
  goto L2061;

  L547:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, QImode))
    {
      ro[1] = x2;
      goto L548;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L548:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L549;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L549:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 100;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L565:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L566;
    }
  goto L2061;

  L566:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, HImode))
    {
      ro[1] = x2;
      goto L567;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L567:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L568;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L568:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 104;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L579:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L580;
    }
  goto L2061;

  L580:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L581;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L581:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L582;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L582:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 107;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L607:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L608;
    }
  goto L2061;

  L608:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L609;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L609:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L610;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L610:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 115;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L632:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L633;
    }
  goto L2061;

  L633:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, DFmode))
    {
      ro[1] = x2;
      goto L634;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L634:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L635;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L635:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if ((reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 121;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L654:
  x3 = XEXP (x2, 0);
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L655;
    }
  goto L2061;

  L655:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, TFmode))
    {
      ro[1] = x2;
      goto L656;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L656:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L657;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L657:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if (0 && (reload_completed || reload_in_progress) && ! TARGET_PTR64)
	return 126;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L895:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == COMPARE && 1)
    goto L896;
  x2 = XEXP (x1, 0);
  goto L2061;

  L896:
  x3 = XEXP (x2, 0);
  switch (GET_MODE (x3))
    {
    case SImode:
      switch (GET_CODE (x3))
	{
	case ZERO_EXTEND:
	  goto L897;
	case NOT:
	  goto L1579;
	}
    L1540:
      if (cc_arithop (x3, SImode))
	{
	  ro[3] = x3;
	  goto L1541;
	}
    L1624:
      if (cc_arithopn (x3, SImode))
	{
	  ro[3] = x3;
	  goto L1625;
	}
      break;
    case QImode:
      if (GET_CODE (x3) == SUBREG && XINT (x3, 1) == 0 && 1)
	goto L914;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L897:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, QImode))
    {
      ro[1] = x4;
      goto L898;
    }
  goto L1540;

  L898:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L899;
  x3 = XEXP (x2, 0);
  goto L1540;

  L899:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L900;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L900:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L901;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L901:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == ZERO_EXTEND && 1)
    goto L902;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L902:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    return 170;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L1579:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == SImode && GET_CODE (x4) == XOR && 1)
    goto L1580;
  L1728:
  if (arith_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1729;
    }
  goto L1540;

  L1580:
  x5 = XEXP (x4, 0);
  if (reg_or_0_operand (x5, SImode))
    {
      ro[1] = x5;
      goto L1581;
    }
  goto L1728;

  L1581:
  x5 = XEXP (x4, 1);
  if (arith_operand (x5, SImode))
    {
      ro[2] = x5;
      goto L1582;
    }
  goto L1728;

  L1582:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1583;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1583:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1584;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1584:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1585;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1585:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NOT && 1)
    goto L1586;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1586:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == XOR && 1)
    goto L1587;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1587:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1588;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1588:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    return 285;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1728;

  L1729:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1730;
  x3 = XEXP (x2, 0);
  goto L1540;

  L1730:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1731;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L1731:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1732;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L1732:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NOT && 1)
    goto L1733;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L1733:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    return 305;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1540;

  L1541:
  x4 = XEXP (x3, 0);
  if (arith_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1542;
    }
  goto L1624;

  L1542:
  x4 = XEXP (x3, 1);
  if (arith_operand (x4, SImode))
    {
      ro[2] = x4;
      goto L1543;
    }
  goto L1624;

  L1543:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1544;
  x3 = XEXP (x2, 0);
  goto L1624;

  L1544:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1545;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1624;

  L1545:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1546;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1624;

  L1546:
  x2 = XEXP (x1, 1);
  if (rtx_equal_p (x2, ro[3]) && 1)
    return 281;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1624;

  L1625:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == SImode && GET_CODE (x4) == NOT && 1)
    goto L1626;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1626:
  x5 = XEXP (x4, 0);
  if (arith_operand (x5, SImode))
    {
      ro[1] = x5;
      goto L1627;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1627:
  x4 = XEXP (x3, 1);
  if (reg_or_0_operand (x4, SImode))
    {
      ro[2] = x4;
      goto L1628;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1628:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1629;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1629:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1630;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1630:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1631;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1631:
  x2 = XEXP (x1, 1);
  if (rtx_equal_p (x2, ro[3]) && 1)
    return 289;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L914:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L915;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L915:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L916;
  x2 = XEXP (x1, 0);
  goto L2061;

  L916:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L917;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L917:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, QImode))
    {
      ro[0] = x2;
      goto L918;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L918:
  x2 = XEXP (x1, 1);
  if (rtx_equal_p (x2, ro[1]) && 1)
    return 172;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1136:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CC_NOOVmode && GET_CODE (x2) == COMPARE && 1)
    goto L1137;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1137:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != SImode)
    {
      x2 = XEXP (x1, 0);
      goto L2061;
    }
  switch (GET_CODE (x3))
    {
    case PLUS:
      goto L1138;
    case MINUS:
      goto L1203;
    case NEG:
      goto L1682;
    case ASHIFT:
      goto L1877;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1138:
  x4 = XEXP (x3, 0);
  if (arith_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1139;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1139:
  x4 = XEXP (x3, 1);
  if (arith_operand (x4, SImode))
    {
      ro[2] = x4;
      goto L1140;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1140:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1141;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1141:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1142;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1142:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1143;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1143:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == PLUS && 1)
    goto L1144;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1144:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    goto L1145;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1145:
  x3 = XEXP (x2, 1);
  if (rtx_equal_p (x3, ro[2]) && 1)
    return 223;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1203:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1204;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1204:
  x4 = XEXP (x3, 1);
  if (arith_operand (x4, SImode))
    {
      ro[2] = x4;
      goto L1205;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1205:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1206;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1206:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1207;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1207:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1208;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1208:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == MINUS && 1)
    goto L1209;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1209:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    goto L1210;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1210:
  x3 = XEXP (x2, 1);
  if (rtx_equal_p (x3, ro[2]) && 1)
    return 231;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1682:
  x4 = XEXP (x3, 0);
  if (arith_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1683;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1683:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1684;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1684:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1685;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1685:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1686;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1686:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == NEG && 1)
    goto L1687;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1687:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    return 297;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1877:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1878;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1878:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 1 && 1)
    goto L1879;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1879:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1880;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1880:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1881;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1881:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1882;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1882:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == ASHIFT && 1)
    goto L1883;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1883:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    goto L1884;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1884:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 1 && 1)
    return 333;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1149:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCX_NOOVmode && GET_CODE (x2) == COMPARE && 1)
    goto L1150;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1150:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != DImode)
    {
      x2 = XEXP (x1, 0);
      goto L2061;
    }
  switch (GET_CODE (x3))
    {
    case PLUS:
      goto L1151;
    case MINUS:
      goto L1216;
    case NEG:
      goto L1693;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1151:
  x4 = XEXP (x3, 0);
  if (arith_double_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1152;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1152:
  x4 = XEXP (x3, 1);
  if (arith_double_operand (x4, DImode))
    {
      ro[2] = x4;
      goto L1153;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1153:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1154;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1154:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1155;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1155:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1156;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1156:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == PLUS && 1)
    goto L1157;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1157:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    goto L1158;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1158:
  x3 = XEXP (x2, 1);
  if (rtx_equal_p (x3, ro[2]) && 1)
    if (TARGET_V9)
      return 224;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1216:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1217;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1217:
  x4 = XEXP (x3, 1);
  if (arith_double_operand (x4, DImode))
    {
      ro[2] = x4;
      goto L1218;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1218:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1219;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1219:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1220;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1220:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1221;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1221:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == MINUS && 1)
    goto L1222;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1222:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    goto L1223;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1223:
  x3 = XEXP (x2, 1);
  if (rtx_equal_p (x3, ro[2]) && 1)
    if (TARGET_V9)
      return 232;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1693:
  x4 = XEXP (x3, 0);
  if (arith_double_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1694;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1694:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1695;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1695:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1696;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1696:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1697;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1697:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == NEG && 1)
    goto L1698;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1698:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    if (TARGET_V9)
      return 298;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1550:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCXmode && GET_CODE (x2) == COMPARE && 1)
    goto L1593;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1593:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != DImode)
    {
      x2 = XEXP (x1, 0);
      goto L2061;
    }
  if (GET_CODE (x3) == NOT && 1)
    goto L1594;
  L1551:
  if (cc_arithop (x3, DImode))
    {
      ro[3] = x3;
      goto L1552;
    }
  L1636:
  if (cc_arithopn (x3, DImode))
    {
      ro[3] = x3;
      goto L1637;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1594:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == DImode && GET_CODE (x4) == XOR && 1)
    goto L1595;
  L1739:
  if (arith_double_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1740;
    }
  goto L1551;

  L1595:
  x5 = XEXP (x4, 0);
  if (reg_or_0_operand (x5, DImode))
    {
      ro[1] = x5;
      goto L1596;
    }
  goto L1739;

  L1596:
  x5 = XEXP (x4, 1);
  if (arith_double_operand (x5, DImode))
    {
      ro[2] = x5;
      goto L1597;
    }
  goto L1739;

  L1597:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1598;
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1598:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1599;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1599:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1600;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1600:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == NOT && 1)
    goto L1601;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1601:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == DImode && GET_CODE (x3) == XOR && 1)
    goto L1602;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1602:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1603;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1603:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    if (TARGET_V9)
      return 286;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L1739;

  L1740:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1741;
  x3 = XEXP (x2, 0);
  goto L1551;

  L1741:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1742;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1551;

  L1742:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1743;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1551;

  L1743:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == NOT && 1)
    goto L1744;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1551;

  L1744:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[1]) && 1)
    if (TARGET_V9)
      return 306;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1551;

  L1552:
  x4 = XEXP (x3, 0);
  if (arith_double_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L1553;
    }
  goto L1636;

  L1553:
  x4 = XEXP (x3, 1);
  if (arith_double_operand (x4, DImode))
    {
      ro[2] = x4;
      goto L1554;
    }
  goto L1636;

  L1554:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1555;
  x3 = XEXP (x2, 0);
  goto L1636;

  L1555:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1556;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1636;

  L1556:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1557;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1636;

  L1557:
  x2 = XEXP (x1, 1);
  if (rtx_equal_p (x2, ro[3]) && 1)
    if (TARGET_V9)
      return 282;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  goto L1636;

  L1637:
  x4 = XEXP (x3, 0);
  if (GET_MODE (x4) == DImode && GET_CODE (x4) == NOT && 1)
    goto L1638;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1638:
  x5 = XEXP (x4, 0);
  if (arith_double_operand (x5, DImode))
    {
      ro[1] = x5;
      goto L1639;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1639:
  x4 = XEXP (x3, 1);
  if (reg_or_0_operand (x4, DImode))
    {
      ro[2] = x4;
      goto L1640;
    }
  x2 = XEXP (x1, 0);
  goto L2061;

  L1640:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1641;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1641:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1642;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1642:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1643;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1643:
  x2 = XEXP (x1, 1);
  if (rtx_equal_p (x2, ro[3]) && 1)
    if (TARGET_V9)
      return 290;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1930:
  x2 = XEXP (x1, 1);
  if (address_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1931;
    }
  L1937:
  if (address_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1938;
    }
  L1944:
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1945;
  x2 = XEXP (x1, 0);
  goto L2061;

  L1931:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == USE && 1)
    goto L1932;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1937;

  L1932:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1933;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1937;

  L1933:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  if (! TARGET_PTR64)
    return 342;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1937;

  L1938:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == USE && 1)
    goto L1939;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1944;

  L1939:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1940;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1944;

  L1940:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  if (TARGET_PTR64)
    return 343;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L1944;

  L1945:
  x3 = XEXP (x2, 0);
  ro[0] = x3;
  goto L1946;

  L1946:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1947;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1947:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) != REG)
    {
      x1 = XVECEXP (x0, 0, 0);
      x2 = XEXP (x1, 0);
    goto L2061;
    }
  switch (GET_MODE (x2))
    {
    case SImode:
      if (XINT (x2, 0) == 15 && 1)
	goto L1948;
      break;
    case DImode:
      if (XINT (x2, 0) == 15 && 1)
	goto L1957;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1948:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1949;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1949:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[0]) && 1)
    if (! TARGET_PTR64)
      return 344;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1957:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1958;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L1958:
  x3 = XEXP (x2, 0);
  if (rtx_equal_p (x3, ro[0]) && 1)
    if (TARGET_PTR64)
      return 345;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2061;

  L2062:
  x2 = XEXP (x1, 1);
  if (GET_CODE (x2) == CALL && 1)
    goto L2063;
  x2 = XEXP (x1, 0);
  goto L2140;

  L2063:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == MEM && 1)
    goto L2064;
  x2 = XEXP (x1, 0);
  goto L2140;

  L2064:
  x4 = XEXP (x3, 0);
  if (address_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2065;
    }
  L2079:
  if (symbolic_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2080;
    }
  L2094:
  if (address_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L2095;
    }
  L2109:
  if (symbolic_operand (x4, DImode))
    {
      ro[1] = x4;
      goto L2110;
    }
  x2 = XEXP (x1, 0);
  goto L2140;

  L2065:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L2066;

  L2066:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2067;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2079;

  L2067:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_PTR64)
      return 357;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2079;

  L2080:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L2081;

  L2081:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2082;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2094;

  L2082:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_PTR64)
      return 358;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2094;

  L2095:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L2096;

  L2096:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2097;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2109;

  L2097:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (TARGET_PTR64)
      return 359;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  x3 = XEXP (x2, 0);
  x4 = XEXP (x3, 0);
  goto L2109;

  L2110:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L2111;

  L2111:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2112;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2140;

  L2112:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (TARGET_PTR64)
      return 360;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2140;

  L2141:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == FFS && 1)
    goto L2142;
  x2 = XEXP (x1, 0);
  goto L2273;

  L2142:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2143;
    }
  x2 = XEXP (x1, 0);
  goto L2273;

  L2143:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2144;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2273;

  L2144:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[2] = x2;
      if (TARGET_SPARCLITE)
	return 374;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2273;

  L2274:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, SImode))
    {
      ro[1] = x2;
      goto L2275;
    }
  L2284:
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == PLUS && 1)
    goto L2285;
  goto ret0;

  L2275:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_EPILOGUE)
      return 408;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L2284;

  L2285:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2286;
    }
  goto ret0;

  L2286:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L2287;
    }
  goto ret0;

  L2287:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_EPILOGUE)
      return 410;
  goto ret0;

  L2152:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == FFS && 1)
    goto L2153;
  x2 = XEXP (x1, 0);
  goto L2290;

  L2153:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L2154;
    }
  x2 = XEXP (x1, 0);
  goto L2290;

  L2154:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2155;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2290;

  L2155:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, DImode))
    {
      ro[2] = x2;
      if (TARGET_V9)
	return 375;
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2290;

  L2291:
  x2 = XEXP (x1, 1);
  if (arith_double_operand (x2, DImode))
    {
      ro[1] = x2;
      goto L2292;
    }
  L2296:
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == PLUS && 1)
    goto L2297;
  goto ret0;

  L2292:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (TARGET_V9 && ! TARGET_EPILOGUE)
      return 411;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  goto L2296;

  L2297:
  x3 = XEXP (x2, 0);
  if (arith_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L2298;
    }
  goto ret0;

  L2298:
  x3 = XEXP (x2, 1);
  if (arith_double_operand (x3, DImode))
    {
      ro[2] = x3;
      goto L2299;
    }
  goto ret0;

  L2299:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (TARGET_V9 && ! TARGET_EPILOGUE)
      return 412;
  goto ret0;

  L2264:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, QImode))
    {
      ro[1] = x2;
      goto L2265;
    }
  goto ret0;

  L2265:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_EPILOGUE)
      return 406;
  goto ret0;

  L2269:
  x2 = XEXP (x1, 1);
  if (arith_operand (x2, HImode))
    {
      ro[1] = x2;
      goto L2270;
    }
  goto ret0;

  L2270:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_EPILOGUE)
      return 407;
  goto ret0;

  L2279:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[1] = x2;
      goto L2280;
    }
  x2 = XEXP (x1, 0);
  goto L2302;

  L2280:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_FPU && ! TARGET_EPILOGUE)
      return 409;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2302;

  L2303:
  x2 = XEXP (x1, 1);
  if (register_operand (x2, SFmode))
    {
      ro[0] = x2;
      goto L2304;
    }
  goto ret0;

  L2304:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == RETURN && 1)
    if (! TARGET_EPILOGUE)
      return 413;
  goto ret0;
 ret0: return -1;
}

int
recog_10 (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  switch (GET_MODE (x2))
    {
    case SFmode:
      if (register_operand (x2, SFmode))
	{
	  ro[0] = x2;
	  goto L998;
	}
      break;
    case DFmode:
      if (register_operand (x2, DFmode))
	{
	  ro[0] = x2;
	  goto L1007;
	}
      break;
    case TFmode:
      if (register_operand (x2, TFmode))
	{
	  ro[0] = x2;
	  goto L1016;
	}
      break;
    case DImode:
      if (general_operand (x2, DImode))
	{
	  ro[0] = x2;
	  goto L1052;
	}
      break;
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[0] = x2;
	  goto L1332;
	}
    }
  if (GET_CODE (x2) == PC && 1)
    goto L1912;
  goto ret0;

  L998:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SFmode && GET_CODE (x2) == FLOAT && 1)
    goto L999;
  goto ret0;

  L999:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1000;
    }
  goto ret0;

  L1000:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1001;
  goto ret0;

  L1001:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1002;
    }
  goto ret0;

  L1002:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1003;
  goto ret0;

  L1003:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 199;
      }
  goto ret0;

  L1007:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DFmode && GET_CODE (x2) == FLOAT && 1)
    goto L1008;
  goto ret0;

  L1008:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1009;
    }
  goto ret0;

  L1009:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1010;
  goto ret0;

  L1010:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1011;
    }
  goto ret0;

  L1011:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1012;
  goto ret0;

  L1012:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 200;
      }
  goto ret0;

  L1016:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == TFmode && GET_CODE (x2) == FLOAT && 1)
    goto L1017;
  goto ret0;

  L1017:
  x3 = XEXP (x2, 0);
  if (general_operand (x3, DImode))
    {
      ro[1] = x3;
      goto L1018;
    }
  goto ret0;

  L1018:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1019;
  goto ret0;

  L1019:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1020;
    }
  goto ret0;

  L1020:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1021;
  goto ret0;

  L1021:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 201;
      }
  goto ret0;

  L1052:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == FIX && 1)
    goto L1053;
  goto ret0;

  L1053:
  x3 = XEXP (x2, 0);
  if (GET_CODE (x3) != FIX)
    goto ret0;
  switch (GET_MODE (x3))
    {
    case SFmode:
      goto L1054;
    case DFmode:
      goto L1064;
    case TFmode:
      goto L1074;
    }
  goto ret0;

  L1054:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SFmode))
    {
      ro[1] = x4;
      goto L1055;
    }
  goto ret0;

  L1055:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1056;
  goto ret0;

  L1056:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1057;
    }
  goto ret0;

  L1057:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1058;
  goto ret0;

  L1058:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 211;
      }
  goto ret0;

  L1064:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, DFmode))
    {
      ro[1] = x4;
      goto L1065;
    }
  goto ret0;

  L1065:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1066;
  goto ret0;

  L1066:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1067;
    }
  goto ret0;

  L1067:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1068;
  goto ret0;

  L1068:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU)
	return 212;
      }
  goto ret0;

  L1074:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, TFmode))
    {
      ro[1] = x4;
      goto L1075;
    }
  goto ret0;

  L1075:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1076;
  goto ret0;

  L1076:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, DFmode))
    {
      ro[2] = x2;
      goto L1077;
    }
  goto ret0;

  L1077:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1078;
  goto ret0;

  L1078:
  x2 = XEXP (x1, 0);
  if (memory_operand (x2, DImode))
    {
      ro[3] = x2;
      if (TARGET_V9 && TARGET_FPU && TARGET_HARD_QUAD)
	return 213;
      }
  goto ret0;

  L1332:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == DIV && 1)
    goto L1333;
  goto ret0;

  L1333:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1334;
    }
  goto ret0;

  L1334:
  x3 = XEXP (x2, 1);
  if (arith_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L1335;
    }
  goto ret0;

  L1335:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == SET && 1)
    goto L1336;
  goto ret0;

  L1336:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    goto L1337;
  goto ret0;

  L1337:
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == COMPARE && 1)
    goto L1338;
  goto ret0;

  L1338:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == DIV && 1)
    goto L1339;
  goto ret0;

  L1339:
  x4 = XEXP (x3, 0);
  if (rtx_equal_p (x4, ro[1]) && 1)
    goto L1340;
  goto ret0;

  L1340:
  x4 = XEXP (x3, 1);
  if (rtx_equal_p (x4, ro[2]) && 1)
    goto L1341;
  goto ret0;

  L1341:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L1342;
  goto ret0;

  L1342:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1343;
  goto ret0;

  L1343:
  x2 = XEXP (x1, 0);
  if (scratch_operand (x2, SImode))
    {
      ro[3] = x2;
      if (TARGET_V8)
	return 250;
      }
  goto ret0;

  L1912:
  x2 = XEXP (x1, 1);
  switch (GET_MODE (x2))
    {
    case SImode:
      if (register_operand (x2, SImode))
	{
	  ro[0] = x2;
	  goto L1913;
	}
      break;
    case DImode:
      if (register_operand (x2, DImode))
	{
	  ro[0] = x2;
	  goto L1922;
	}
    }
  goto ret0;

  L1913:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == USE && 1)
    goto L1914;
  goto ret0;

  L1914:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1915;
  goto ret0;

  L1915:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  goto L1916;

  L1916:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == USE && 1)
    goto L1917;
  goto ret0;

  L1917:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_PTR64)
      return 340;
  goto ret0;

  L1922:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == USE && 1)
    goto L1923;
  goto ret0;

  L1923:
  x2 = XEXP (x1, 0);
  if (GET_CODE (x2) == LABEL_REF && 1)
    goto L1924;
  goto ret0;

  L1924:
  x3 = XEXP (x2, 0);
  ro[1] = x3;
  goto L1925;

  L1925:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == USE && 1)
    goto L1926;
  goto ret0;

  L1926:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (TARGET_PTR64)
      return 341;
  goto ret0;
 ret0: return -1;
}

int
recog (x0, insn, pnum_clobbers)
     register rtx x0;
     rtx insn;
     int *pnum_clobbers;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  int tem;

  L2121:
  switch (GET_CODE (x0))
    {
    case UNSPEC:
      if (GET_MODE (x0) == SImode && XINT (x0, 1) == 0 && XVECLEN (x0, 0) == 2 && 1)
	goto L2122;
      break;
    case SET:
      goto L1;
    case PARALLEL:
      if (XVECLEN (x0, 0) == 2 && 1)
	goto L71;
      if (XVECLEN (x0, 0) == 3 && 1)
	goto L996;
      break;
    case CALL:
      goto L1967;
    case UNSPEC_VOLATILE:
      if (XINT (x0, 1) == 0 && XVECLEN (x0, 0) == 1 && 1)
	goto L2120;
      if (XINT (x0, 1) == 1 && XVECLEN (x0, 0) == 1 && 1)
	goto L2133;
      if (XINT (x0, 1) == 2 && XVECLEN (x0, 0) == 1 && 1)
	goto L2135;
      if (XINT (x0, 1) == 3 && XVECLEN (x0, 0) == 1 && 1)
	goto L2137;
      break;
    case RETURN:
      if (! TARGET_EPILOGUE)
	return 365;
      break;
    case CONST_INT:
      if (XWINT (x0, 0) == 0 && 1)
	return 366;
    }
  goto ret0;

  L2122:
  x1 = XVECEXP (x0, 0, 0);
  if (register_operand (x1, SImode))
    {
      ro[0] = x1;
      goto L2123;
    }
  goto ret0;

  L2123:
  x1 = XVECEXP (x0, 0, 1);
  if (register_operand (x1, SImode))
    {
      ro[1] = x1;
      if (! TARGET_V9)
	return 364;
      }
  goto ret0;
 L1:
  return recog_6 (x0, insn, pnum_clobbers);

  L71:
  x1 = XVECEXP (x0, 0, 0);
  switch (GET_CODE (x1))
    {
    case SET:
      goto L72;
    case CALL:
      goto L1961;
    }
  goto ret0;
 L72:
  return recog_9 (x0, insn, pnum_clobbers);

  L1961:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == MEM && 1)
    goto L1962;
  goto ret0;

  L1962:
  x3 = XEXP (x2, 0);
  if (address_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1963;
    }
  L1973:
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L1974;
    }
  L1984:
  if (address_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1985;
    }
  L1995:
  if (symbolic_operand (x3, DImode))
    {
      ro[0] = x3;
      goto L1996;
    }
  L2014:
  if (address_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2015;
    }
  L2028:
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2029;
    }
  L2042:
  if (address_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2043;
    }
  L2056:
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2057;
    }
  goto ret0;

  L1963:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L1964;

  L1964:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1965;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1973;

  L1965:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_PTR64)
      return 348;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1973;

  L1974:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L1975;

  L1975:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1976;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1984;

  L1976:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_PTR64)
      return 349;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1984;

  L1985:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L1986;

  L1986:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1987;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1995;

  L1987:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (TARGET_PTR64)
      return 350;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L1995;

  L1996:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L1997;

  L1997:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1998;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2014;

  L1998:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == DImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (TARGET_PTR64)
      return 351;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2014;

  L2015:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2016;

  L2016:
  x1 = XVECEXP (x0, 0, 1);
  if (pnum_clobbers != 0 && immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) > 0)
	{
	  *pnum_clobbers = 1;
	  return 352;
	}
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2028;

  L2029:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2030;

  L2030:
  x1 = XVECEXP (x0, 0, 1);
  if (pnum_clobbers != 0 && immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) > 0)
	{
	  *pnum_clobbers = 1;
	  return 353;
	}
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2042;

  L2043:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2044;

  L2044:
  x1 = XVECEXP (x0, 0, 1);
  if (pnum_clobbers != 0 && immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) < 0)
	{
	  *pnum_clobbers = 1;
	  return 354;
	}
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2056;

  L2057:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2058;

  L2058:
  x1 = XVECEXP (x0, 0, 1);
  if (pnum_clobbers != 0 && immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) < 0)
	{
	  *pnum_clobbers = 1;
	  return 355;
	}
      }
  goto ret0;

  L996:
  x1 = XVECEXP (x0, 0, 0);
  switch (GET_CODE (x1))
    {
    case SET:
      goto L997;
    case CALL:
      goto L2005;
    }
  goto ret0;
 L997:
  return recog_10 (x0, insn, pnum_clobbers);

  L2005:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == MEM && 1)
    goto L2006;
  goto ret0;

  L2006:
  x3 = XEXP (x2, 0);
  if (address_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2007;
    }
  L2020:
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2021;
    }
  L2034:
  if (address_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2035;
    }
  L2048:
  if (symbolic_operand (x3, SImode))
    {
      ro[0] = x3;
      goto L2049;
    }
  goto ret0;

  L2007:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2008;

  L2008:
  x1 = XVECEXP (x0, 0, 1);
  if (immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      goto L2009;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2020;

  L2009:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2010;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2020;

  L2010:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) > 0)
      return 352;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2020;

  L2021:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2022;

  L2022:
  x1 = XVECEXP (x0, 0, 1);
  if (immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      goto L2023;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2034;

  L2023:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2024;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2034;

  L2024:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) > 0)
      return 353;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2034;

  L2035:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2036;

  L2036:
  x1 = XVECEXP (x0, 0, 1);
  if (immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      goto L2037;
    }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2048;

  L2037:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2038;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2048;

  L2038:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) < 0)
      return 354;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  x3 = XEXP (x2, 0);
  goto L2048;

  L2049:
  x2 = XEXP (x1, 1);
  ro[1] = x2;
  goto L2050;

  L2050:
  x1 = XVECEXP (x0, 0, 1);
  if (immediate_operand (x1, VOIDmode))
    {
      ro[2] = x1;
      goto L2051;
    }
  goto ret0;

  L2051:
  x1 = XVECEXP (x0, 0, 2);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2052;
  goto ret0;

  L2052:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == SImode && GET_CODE (x2) == REG && XINT (x2, 0) == 15 && 1)
    if (! TARGET_V9 && GET_CODE (operands[2]) == CONST_INT && INTVAL (operands[2]) < 0)
      return 355;
  goto ret0;

  L1967:
  x1 = XEXP (x0, 0);
  if (GET_MODE (x1) == SImode && GET_CODE (x1) == MEM && 1)
    goto L1968;
  goto ret0;

  L1968:
  x2 = XEXP (x1, 0);
  if (address_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1969;
    }
  L1979:
  if (symbolic_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1980;
    }
  L1990:
  if (address_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L1991;
    }
  L2001:
  if (symbolic_operand (x2, DImode))
    {
      ro[0] = x2;
      goto L2002;
    }
  goto ret0;

  L1969:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x1;
      if (! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 348;
	}
      }
  x1 = XEXP (x0, 0);
  x2 = XEXP (x1, 0);
  goto L1979;

  L1980:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x1;
      if (! TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 349;
	}
      }
  x1 = XEXP (x0, 0);
  x2 = XEXP (x1, 0);
  goto L1990;

  L1991:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x1;
      if (TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 350;
	}
      }
  x1 = XEXP (x0, 0);
  x2 = XEXP (x1, 0);
  goto L2001;

  L2002:
  x1 = XEXP (x0, 1);
  if (pnum_clobbers != 0 && 1)
    {
      ro[1] = x1;
      if (TARGET_PTR64)
	{
	  *pnum_clobbers = 1;
	  return 351;
	}
      }
  goto ret0;

  L2120:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == CONST_INT && XWINT (x1, 0) == 0 && 1)
    return 362;
  goto ret0;

  L2133:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == CONST_INT && XWINT (x1, 0) == 0 && 1)
    return 371;
  goto ret0;

  L2135:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == CONST_INT && XWINT (x1, 0) == 0 && 1)
    return 372;
  goto ret0;

  L2137:
  x1 = XVECEXP (x0, 0, 0);
  if (memory_operand (x1, VOIDmode))
    {
      ro[0] = x1;
      return 373;
    }
  goto ret0;
 ret0: return -1;
}

rtx
split_1 (x0, insn)
     register rtx x0;
     rtx insn;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  rtx tem;

  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 1);
  if (GET_MODE (x2) != SImode)
    goto ret0;
  switch (GET_CODE (x2))
    {
    case AND:
      goto L1401;
    case IOR:
      goto L1446;
    case XOR:
      goto L1491;
    case NOT:
      goto L1499;
    case NE:
      goto L2191;
    case NEG:
      goto L2199;
    case EQ:
      goto L2208;
    case PLUS:
      goto L2225;
    case MINUS:
      goto L2235;
    }
  goto ret0;

  L1401:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1402;
    }
  goto ret0;

  L1402:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L1403;

  L1403:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1404;
  goto ret0;

  L1404:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (GET_CODE (operands[2]) == CONST_INT
   && !SMALL_INT (operands[2])
   && (INTVAL (operands[2]) & 0x3ff) == 0x3ff)
	return gen_split_258 (operands);
      }
  goto ret0;

  L1446:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1447;
    }
  goto ret0;

  L1447:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L1448;

  L1448:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1449;
  goto ret0;

  L1449:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (GET_CODE (operands[2]) == CONST_INT
   && !SMALL_INT (operands[2])
   && (INTVAL (operands[2]) & 0x3ff) == 0x3ff)
	return gen_split_266 (operands);
      }
  goto ret0;

  L1491:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L1492;
    }
  goto ret0;

  L1492:
  x3 = XEXP (x2, 1);
  ro[2] = x3;
  goto L1493;

  L1493:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1494;
  goto ret0;

  L1494:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (GET_CODE (operands[2]) == CONST_INT
   && !SMALL_INT (operands[2])
   && (INTVAL (operands[2]) & 0x3ff) == 0x3ff)
	return gen_split_274 (operands);
      }
  goto ret0;

  L1499:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) == SImode && GET_CODE (x3) == XOR && 1)
    goto L1500;
  goto ret0;

  L1500:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L1501;
    }
  goto ret0;

  L1501:
  x4 = XEXP (x3, 1);
  ro[2] = x4;
  goto L1502;

  L1502:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L1503;
  goto ret0;

  L1503:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[3] = x2;
      if (GET_CODE (operands[2]) == CONST_INT
   && !SMALL_INT (operands[2])
   && (INTVAL (operands[2]) & 0x3ff) == 0x3ff)
	return gen_split_275 (operands);
      }
  goto ret0;

  L2191:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2192;
    }
  goto ret0;

  L2192:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L2193;
  goto ret0;

  L2193:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2194;
  goto ret0;

  L2194:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_382 (operands);
  goto ret0;

  L2199:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != SImode)
    goto ret0;
  switch (GET_CODE (x3))
    {
    case NE:
      goto L2200;
    case EQ:
      goto L2217;
    }
  goto ret0;

  L2200:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2201;
    }
  goto ret0;

  L2201:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2202;
  goto ret0;

  L2202:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2203;
  goto ret0;

  L2203:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_383 (operands);
  goto ret0;

  L2217:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2218;
    }
  goto ret0;

  L2218:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2219;
  goto ret0;

  L2219:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2220;
  goto ret0;

  L2220:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_385 (operands);
  goto ret0;

  L2208:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[1] = x3;
      goto L2209;
    }
  goto ret0;

  L2209:
  x3 = XEXP (x2, 1);
  if (GET_CODE (x3) == CONST_INT && XWINT (x3, 0) == 0 && 1)
    goto L2210;
  goto ret0;

  L2210:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2211;
  goto ret0;

  L2211:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_384 (operands);
  goto ret0;

  L2225:
  x3 = XEXP (x2, 0);
  if (GET_MODE (x3) != SImode)
    goto ret0;
  switch (GET_CODE (x3))
    {
    case NE:
      goto L2226;
    case EQ:
      goto L2246;
    }
  goto ret0;

  L2226:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2227;
    }
  goto ret0;

  L2227:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2228;
  goto ret0;

  L2228:
  x3 = XEXP (x2, 1);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L2229;
    }
  goto ret0;

  L2229:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2230;
  goto ret0;

  L2230:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_386 (operands);
  goto ret0;

  L2246:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2247;
    }
  goto ret0;

  L2247:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2248;
  goto ret0;

  L2248:
  x3 = XEXP (x2, 1);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L2249;
    }
  goto ret0;

  L2249:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2250;
  goto ret0;

  L2250:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_388 (operands);
  goto ret0;

  L2235:
  x3 = XEXP (x2, 0);
  if (register_operand (x3, SImode))
    {
      ro[2] = x3;
      goto L2236;
    }
  goto ret0;

  L2236:
  x3 = XEXP (x2, 1);
  if (GET_MODE (x3) != SImode)
    goto ret0;
  switch (GET_CODE (x3))
    {
    case NE:
      goto L2237;
    case EQ:
      goto L2257;
    }
  goto ret0;

  L2237:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2238;
    }
  goto ret0;

  L2238:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2239;
  goto ret0;

  L2239:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2240;
  goto ret0;

  L2240:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_387 (operands);
  goto ret0;

  L2257:
  x4 = XEXP (x3, 0);
  if (register_operand (x4, SImode))
    {
      ro[1] = x4;
      goto L2258;
    }
  goto ret0;

  L2258:
  x4 = XEXP (x3, 1);
  if (GET_CODE (x4) == CONST_INT && XWINT (x4, 0) == 0 && 1)
    goto L2259;
  goto ret0;

  L2259:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2260;
  goto ret0;

  L2260:
  x2 = XEXP (x1, 0);
  if (GET_MODE (x2) == CCmode && GET_CODE (x2) == REG && XINT (x2, 0) == 0 && 1)
    return gen_split_389 (operands);
  goto ret0;
 ret0: return 0;
}

rtx
split_insns (x0, insn)
     register rtx x0;
     rtx insn;
{
  register rtx *ro = &recog_operand[0];
  register rtx x1, x2, x3, x4, x5;
  rtx tem;

  L626:
  switch (GET_CODE (x0))
    {
    case SET:
      goto L627;
    case PARALLEL:
      if (XVECLEN (x0, 0) == 2 && 1)
	goto L1398;
    }
  goto ret0;

  L627:
  x1 = XEXP (x0, 0);
  if (register_operand (x1, DFmode))
    {
      ro[0] = x1;
      goto L628;
    }
  L2173:
  if (register_operand (x1, VOIDmode))
    {
      ro[0] = x1;
      goto L2174;
    }
  L2180:
  if (register_operand (x1, SImode))
    {
      ro[0] = x1;
      goto L2181;
    }
  goto ret0;

  L628:
  x1 = XEXP (x0, 1);
  if (register_operand (x1, DFmode))
    {
      ro[1] = x1;
      if (! TARGET_V9 && reload_completed)
	return gen_split_120 (operands);
      }
  x1 = XEXP (x0, 0);
  goto L2173;

  L2174:
  x1 = XEXP (x0, 1);
  if (splittable_immediate_memory_operand (x1, VOIDmode))
    {
      ro[1] = x1;
      if (flag_pic)
	return gen_split_378 (operands);
      }
  L2177:
  if (extend_op (x1, VOIDmode))
    {
      ro[1] = x1;
      goto L2178;
    }
  x1 = XEXP (x0, 0);
  goto L2180;

  L2178:
  x2 = XEXP (x1, 0);
  if (splittable_immediate_memory_operand (x2, VOIDmode))
    {
      ro[2] = x2;
      if (flag_pic)
	return gen_split_379 (operands);
      }
  x1 = XEXP (x0, 0);
  goto L2180;

  L2181:
  x1 = XEXP (x0, 1);
  if (immediate_operand (x1, SImode))
    goto L2185;
  goto ret0;

  L2185:
  ro[1] = x1;
  if (! flag_pic && (GET_CODE (operands[1]) == SYMBOL_REF
		  || GET_CODE (operands[1]) == CONST
		  || GET_CODE (operands[1]) == LABEL_REF))
    return gen_split_380 (operands);
  L2186:
  ro[1] = x1;
  if (flag_pic && (GET_CODE (operands[1]) == SYMBOL_REF
		|| GET_CODE (operands[1]) == CONST))
    return gen_split_381 (operands);
  goto ret0;

  L1398:
  x1 = XVECEXP (x0, 0, 0);
  if (GET_CODE (x1) == SET && 1)
    goto L1399;
  goto ret0;

  L1399:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[0] = x2;
      goto L1400;
    }
  L2162:
  if (splittable_symbolic_memory_operand (x2, VOIDmode))
    {
      ro[0] = x2;
      goto L2163;
    }
  L2168:
  if (splittable_immediate_memory_operand (x2, VOIDmode))
    {
      ro[0] = x2;
      goto L2169;
    }
  goto ret0;
 L1400:
  tem = split_1 (x0, insn);
  if (tem != 0) return tem;
  x2 = XEXP (x1, 0);
  goto L2162;

  L2163:
  x2 = XEXP (x1, 1);
  if (reg_or_0_operand (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L2164;
    }
  x2 = XEXP (x1, 0);
  goto L2168;

  L2164:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2165;
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2168;

  L2165:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[2] = x2;
      if (! flag_pic)
	return gen_split_376 (operands);
      }
  x1 = XVECEXP (x0, 0, 0);
  x2 = XEXP (x1, 0);
  goto L2168;

  L2169:
  x2 = XEXP (x1, 1);
  if (general_operand (x2, VOIDmode))
    {
      ro[1] = x2;
      goto L2170;
    }
  goto ret0;

  L2170:
  x1 = XVECEXP (x0, 0, 1);
  if (GET_CODE (x1) == CLOBBER && 1)
    goto L2171;
  goto ret0;

  L2171:
  x2 = XEXP (x1, 0);
  if (register_operand (x2, SImode))
    {
      ro[2] = x2;
      if (flag_pic)
	return gen_split_377 (operands);
      }
  goto ret0;
 ret0: return 0;
}


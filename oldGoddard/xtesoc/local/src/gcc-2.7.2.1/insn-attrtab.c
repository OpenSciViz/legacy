/* Generated automatically by the program `genattrtab'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "insn-config.h"
#include "recog.h"
#include "regs.h"
#include "real.h"
#include "output.h"
#include "insn-attr.h"

#define operands recog_operand

int
insn_current_length (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 0;

    }
}

int
insn_variable_length_p (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 0;

    }
}

int
insn_default_length (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      insn_extract (insn);
      if (symbolic_memory_operand (operands[1], VOIDmode))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return 4;
        }
      else if ((which_alternative == 2) || ((which_alternative == 3) || (which_alternative == 4)))
        {
	  return 5;
        }
      else
        {
	  return 5;
        }

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return 1;
        }
      else if (which_alternative == 2)
        {
	  return 2;
        }
      else if (which_alternative == 3)
        {
	  return 3;
        }
      else
        {
	  return 3;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return 1;
        }
      else if ((which_alternative == 2) || (which_alternative == 3))
        {
	  return 2;
        }
      else if ((which_alternative == 4) || ((which_alternative == 5) || (which_alternative == 6)))
        {
	  return 3;
        }
      else
        {
	  return 3;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 1)
        {
	  if (symbolic_memory_operand (operands[1], VOIDmode))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }
      else if (which_alternative != 0)
        {
	  if (symbolic_memory_operand (operands[0], VOIDmode))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }
      else
        {
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 2;
	    }
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 3) || (which_alternative == 2))
        {
	  if (symbolic_memory_operand (operands[1], VOIDmode))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }
      else if ((which_alternative != 0) && (which_alternative != 1))
        {
	  if (symbolic_memory_operand (operands[0], VOIDmode))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }
      else if (which_alternative == 1)
        {
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 2;
	    }
        }
      else
        {
	  return 1;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 1;
        }
      else if (which_alternative == 1)
        {
	  return 2;
        }
      else if ((which_alternative == 2) || ((which_alternative == 3) || ((which_alternative == 4) || (which_alternative == 5))))
        {
	  return 1;
        }
      else
        {
	  return 1;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 2;
        }
      else if ((which_alternative == 1) || (which_alternative == 2))
        {
	  return 1;
        }
      else if ((which_alternative == 3) || ((which_alternative == 4) || (which_alternative == 5)))
        {
	  return 3;
        }
      else if (which_alternative == 6)
        {
	  return 2;
        }
      else if (which_alternative == 7)
        {
	  return 3;
        }
      else
        {
	  return 3;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  if ((which_alternative == 0) || (which_alternative == 2))
	    {
	      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	        {
		  return 1;
	        }
	      else
	        {
		  return 2;
	        }
	    }
	  else
	    {
	      return 1;
	    }
        }
      else if (which_alternative == 2)
        {
	  return 1;
        }
      else
        {
	  if ((which_alternative == 3) || (which_alternative == 4))
	    {
	      if (symbolic_memory_operand (operands[1], VOIDmode))
	        {
		  return 2;
	        }
	      else
	        {
		  return 1;
	        }
	    }
	  else
	    {
	      if (symbolic_memory_operand (operands[0], VOIDmode))
	        {
		  return 2;
	        }
	      else
	        {
		  return 1;
	        }
	    }
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 2;
	    }
        }
      else if (which_alternative == 1)
        {
	  return 1;
        }
      else if (which_alternative == 2)
        {
	  if (symbolic_memory_operand (operands[1], VOIDmode))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }
      else
        {
	  return 1;
        }

    case 418:
    case 417:
    case 416:
    case 415:
    case 414:
    case 405:
    case 404:
    case 403:
    case 402:
    case 401:
    case 400:
    case 399:
    case 398:
    case 397:
    case 396:
    case 395:
    case 394:
    case 393:
    case 392:
    case 391:
    case 390:
    case 366:
    case 362:
    case 337:
    case 335:
    case 333:
    case 331:
    case 290:
    case 289:
    case 286:
    case 285:
    case 282:
    case 281:
    case 277:
    case 272:
    case 268:
    case 264:
    case 260:
    case 256:
    case 252:
    case 249:
    case 234:
    case 232:
    case 231:
    case 227:
    case 224:
    case 223:
    case 219:
    case 186:
    case 185:
    case 59:
    case 58:
    case 56:
      insn_extract (insn);
      if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 306:
    case 305:
    case 302:
    case 301:
    case 298:
    case 297:
    case 294:
    case 172:
    case 170:
    case 61:
    case 60:
    case 57:
    case 55:
    case 52:
    case 51:
      insn_extract (insn);
      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
        {
	  return 1;
        }
      else
        {
	  return 2;
        }

    case 375:
    case 253:
    case 251:
    case 126:
    case 122:
    case 96:
    case 92:
      return 5;

    case 374:
      return 8;

    case 413:
    case 412:
    case 411:
    case 410:
    case 409:
    case 408:
    case 407:
    case 406:
    case 365:
    case 364:
    case 372:
    case 300:
    case 292:
    case 276:
    case 271:
    case 267:
    case 263:
    case 259:
    case 255:
    case 247:
    case 246:
    case 244:
    case 243:
    case 241:
    case 240:
    case 238:
    case 237:
    case 226:
    case 218:
    case 115:
    case 107:
    case 104:
    case 100:
    case 94:
    case 91:
    case 48:
    case 47:
    case 46:
    case 45:
    case 44:
    case 43:
    case 42:
    case 41:
    case 40:
    case 39:
    case 38:
    case 37:
      return 2;

    case 325:
    case 322:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 1;
        }
      else
        {
	  if (((sparc_arch_type) == (ARCH_ARCH32BIT)))
	    {
	      return 2;
	    }
	  else
	    {
	      return 1;
	    }
        }

    case 324:
    case 321:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 1;
        }
      else
        {
	  if (((sparc_arch_type) == (ARCH_ARCH32BIT)))
	    {
	      return 4;
	    }
	  else
	    {
	      return 1;
	    }
        }

    case 250:
    case 248:
      return 6;

    case 213:
    case 212:
    case 211:
    case 201:
    case 200:
    case 199:
    case 121:
    case 116:
    case 63:
    case 62:
      return 3;

    case 125:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 4;
        }
      else if (which_alternative == 1)
        {
	  return 5;
        }
      else
        {
	  return 5;
        }

    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return 2;
        }
      else if (which_alternative == 1)
        {
	  return 1;
        }
      else
        {
	  return 1;
        }

    case 88:
    case 87:
      return 4;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 1;

    }
}

int
result_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 329:
    case 328:
    case 327:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 63 /* 0x3f */;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 36 /* 0x24 */;
        }
      else
        {
	  return 1;
        }

    case 320:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 37 /* 0x25 */;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 18 /* 0x12 */;
        }
      else
        {
	  return 1;
        }

    case 319:
    case 318:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 37 /* 0x25 */;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 27 /* 0x1b */;
        }
      else
        {
	  return 1;
        }

    case 317:
    case 316:
    case 315:
    case 314:
    case 313:
      if ((TARGET_SUPERSPARC))
        {
	  return 9;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return 7;
        }
      else
        {
	  return 1;
        }

    case 235:
    case 233:
      if ((TARGET_SUPERSPARC))
        {
	  return 12 /* 0xc */;
        }
      else
        {
	  return 1;
        }

    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
      if ((TARGET_SUPERSPARC))
        {
	  return 9;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return 5;
        }
      else
        {
	  return 1;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      if ((TARGET_SUPERSPARC))
        {
	  return 3;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 0) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && (which_alternative != 3)))) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if ((((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && ((which_alternative != 4) && ((which_alternative != 5) && (which_alternative != 6))))))) || ((which_alternative == 1) || (which_alternative == 6))) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 184:
    case 168:
    case 164:
    case 162:
    case 160:
    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 0) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative == 3) || (which_alternative == 2)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 122:
    case 116:
    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative == 0) || (which_alternative == 1)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 4) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative == 2) || (which_alternative == 5)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 6) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 6) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if (((which_alternative == 2) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if ((((which_alternative == 2) || (which_alternative == 4)) || (which_alternative == 7)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return 9;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 125:
    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 3;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 1;
        }

    case 36:
    case 35:
    case 34:
    case 33:
    case 32:
    case 31:
    case 29:
    case 28:
    case 27:
    case 26:
    case 25:
    case 24:
      if ((TARGET_SUPERSPARC))
        {
	  return 9;
        }
      else
        {
	  return 1;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 1;

    }
}

int
iwport_unit_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 1;

    }
}

int
shift_unit_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 1;

    }
}

int
fp_mds_unit_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 329:
    case 328:
    case 327:
      if ((TARGET_SUPERSPARC))
        {
	  return 36 /* 0x24 */;
        }
      else
        {
	  return 63 /* 0x3f */;
        }

    case 320:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 37 /* 0x25 */;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 18 /* 0x12 */;
        }
      else
        {
	  return 63 /* 0x3f */;
        }

    case 319:
    case 318:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 37 /* 0x25 */;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 27 /* 0x1b */;
        }
      else
        {
	  return 63 /* 0x3f */;
        }

    case 317:
    case 316:
    case 315:
    case 314:
    case 313:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 7;
        }
      else if ((TARGET_SUPERSPARC))
        {
	  return 9;
        }
      else
        {
	  return 63 /* 0x3f */;
        }

    case 235:
    case 233:
      if ((TARGET_SUPERSPARC))
        {
	  return 12 /* 0xc */;
        }
      else
        {
	  return 63 /* 0x3f */;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 63 /* 0x3f */;

    }
}

unsigned int
fp_mds_unit_blockage_range (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 196671 /* 0x3003f */;

    }
}

int
fp_alu_unit_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case 124:
    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 4) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 6) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 5;
        }
      else
        {
	  return 9;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 9;

    }
}

unsigned int
fp_alu_unit_blockage_range (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case 124:
    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 6) && ((TARGET_SUPERSPARC)))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
    case 36:
    case 35:
    case 34:
    case 33:
    case 32:
    case 31:
    case 29:
    case 28:
    case 27:
    case 26:
    case 25:
    case 24:
      if ((TARGET_SUPERSPARC))
        {
	  return 196613 /* 0x30005 */;
        }
      else
        {
	  return 327685 /* 0x50005 */;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 327685 /* 0x50005 */;

    }
}

int
memory_unit_ready_cost (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 184:
    case 168:
    case 164:
    case 162:
    case 160:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else
        {
	  return 3;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return 2;
        }
      else
        {
	  return 3;
        }

    case 125:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 5) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if ((which_alternative == 4) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 2) && ((TARGET_SUPERSPARC))) || ((which_alternative == 3) && ((TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 0) || (which_alternative == 3)) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 7) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 1) || (which_alternative == 6)) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if (((which_alternative == 1) || (which_alternative == 6)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((((which_alternative == 0) || (which_alternative == 4)) && ((TARGET_SUPERSPARC))) || ((which_alternative == 5) && ((TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 3) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 4) && ((TARGET_SUPERSPARC))) || ((which_alternative == 5) && ((TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 122:
    case 116:
    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if ((which_alternative == 5) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 6) && ((TARGET_SUPERSPARC))) || ((which_alternative == 3) && ((TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 2) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else if ((which_alternative == 7) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 8) && ((TARGET_SUPERSPARC))) || (((which_alternative == 1) || (which_alternative == 3)) && ((TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 3) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if (((which_alternative == 5) || (which_alternative == 6)) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 126:
    case 121:
    case 115:
    case 107:
    case 104:
    case 100:
      if ((TARGET_SUPERSPARC))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 2;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return 1;
        }
      else
        {
	  return 3;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 3;

    }
}

unsigned int
memory_unit_blockage_range (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 131075 /* 0x20003 */;

    }
}

int
function_units_used (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 336:
    case 334:
    case 330:
      if ((TARGET_SUPERSPARC))
        {
	  return -25 /* 0xffffffe7 */;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 329:
    case 328:
    case 327:
    case 320:
    case 319:
    case 318:
    case 317:
    case 316:
    case 315:
    case 314:
    case 313:
      return 2;

    case 235:
    case 233:
      if ((! (TARGET_SUPERSPARC)))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 278:
    case 273:
    case 269:
    case 265:
    case 261:
    case 257:
    case 228:
    case 220:
      if ((TARGET_SUPERSPARC))
        {
	  return 4;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
      if (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC))))
        {
	  return 1;
        }
      else
        {
	  return 2;
        }

    case 184:
    case 168:
    case 164:
    case 162:
    case 160:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      if ((TARGET_SUPERSPARC))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else
        {
	  return 2;
        }

    case 125:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 3) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 4) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 5) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if ((which_alternative == 1) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) || (which_alternative == 3)) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative == 0) || (which_alternative == 3)) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((((which_alternative != 0) && (which_alternative != 3)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else if (((which_alternative == 0) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 5) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if (((which_alternative == 1) || (which_alternative == 6)) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 7) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 2) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if ((which_alternative == 3) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 1) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else if ((which_alternative == 4) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 5) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 2) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 3) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if ((which_alternative == 1) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 122:
    case 116:
    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 1) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 0) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else
        {
	  return 2;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 6) && ((TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else if ((which_alternative == 6) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 3) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 5) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 4) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if (((which_alternative != 6) && ((which_alternative != 3) && ((which_alternative != 5) && ((which_alternative != 2) && (which_alternative != 4))))) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 8) && ((TARGET_SUPERSPARC)))
        {
	  return 0;
        }
      else if ((which_alternative == 8) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if (((which_alternative == 1) || (which_alternative == 3)) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative == 1) || (which_alternative == 3)) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 7) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 0;
        }
      else if (((which_alternative == 2) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative == 2) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 6) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if (((which_alternative != 8) && (((which_alternative != 1) && (which_alternative != 3)) && ((which_alternative != 7) && (((which_alternative != 2) && (which_alternative != 4)) && (which_alternative != 6))))) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 5) || (which_alternative == 6)) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative == 5) || (which_alternative == 6)) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if ((which_alternative == 1) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return 1;
        }
      else if ((((which_alternative != 5) && (which_alternative != 6)) && (((which_alternative != 3) && (which_alternative != 4)) && (which_alternative != 1))) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 126:
    case 121:
    case 115:
    case 107:
    case 104:
    case 100:
      if ((TARGET_SUPERSPARC))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 3) && ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  return -18 /* 0xffffffee */;
        }
      else if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  return 0;
        }
      else if (((which_alternative != 3) && (which_alternative != 2)) && (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC)))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case 36:
    case 35:
    case 34:
    case 33:
    case 32:
    case 31:
    case 29:
    case 28:
    case 27:
    case 26:
    case 25:
    case 24:
      if ((TARGET_SUPERSPARC))
        {
	  return 1;
        }
      else if ((! (TARGET_SUPERSPARC)))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      if (((TARGET_SUPERSPARC)) || ((! (TARGET_SUPERSPARC))))
        {
	  return -1 /* 0xffffffff */;
        }
      else
        {
	  return 2;
        }

    }
}

int
num_delay_slots (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 369:
    case 368:
    case 360:
    case 359:
    case 358:
    case 357:
    case 351:
    case 350:
    case 349:
    case 348:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
      return 1;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return 0;

    }
}

enum attr_arch
get_attr_arch ()
{
}

enum attr_cpu
get_attr_cpu ()
{
}

enum attr_in_annul_branch_delay
get_attr_in_annul_branch_delay (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 325:
    case 324:
    case 322:
    case 321:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      insn_extract (insn);
      if (! (symbolic_memory_operand (operands[1], VOIDmode)))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 119:
    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 0)
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 1)
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) || (which_alternative == 2))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 418:
    case 417:
    case 416:
    case 415:
    case 414:
    case 405:
    case 404:
    case 403:
    case 402:
    case 401:
    case 400:
    case 399:
    case 398:
    case 397:
    case 396:
    case 395:
    case 394:
    case 393:
    case 392:
    case 391:
    case 390:
    case 366:
    case 362:
    case 337:
    case 335:
    case 333:
    case 331:
    case 290:
    case 289:
    case 286:
    case 285:
    case 282:
    case 281:
    case 277:
    case 272:
    case 268:
    case 264:
    case 260:
    case 256:
    case 252:
    case 249:
    case 234:
    case 232:
    case 231:
    case 227:
    case 224:
    case 223:
    case 219:
    case 186:
    case 185:
    case 59:
    case 58:
    case 56:
      insn_extract (insn);
      if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case 306:
    case 305:
    case 302:
    case 301:
    case 298:
    case 297:
    case 294:
    case 172:
    case 170:
    case 61:
    case 60:
    case 57:
    case 55:
    case 52:
    case 51:
      insn_extract (insn);
      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
        {
	  return IN_ANNUL_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_ANNUL_BRANCH_DELAY_FALSE;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    case 413:
    case 412:
    case 411:
    case 410:
    case 409:
    case 408:
    case 407:
    case 406:
    case 375:
    case 374:
    case 372:
    case 369:
    case 368:
    case 365:
    case 364:
    case 360:
    case 359:
    case 358:
    case 357:
    case 355:
    case 354:
    case 353:
    case 352:
    case 351:
    case 350:
    case 349:
    case 348:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
    case 300:
    case 292:
    case 276:
    case 271:
    case 267:
    case 263:
    case 259:
    case 255:
    case 253:
    case 251:
    case 250:
    case 248:
    case 247:
    case 246:
    case 244:
    case 243:
    case 241:
    case 240:
    case 238:
    case 237:
    case 226:
    case 218:
    case 213:
    case 212:
    case 211:
    case 201:
    case 200:
    case 199:
    case 126:
    case 125:
    case 124:
    case 122:
    case 121:
    case 116:
    case 115:
    case 107:
    case 104:
    case 100:
    case 96:
    case 94:
    case 92:
    case 91:
    case 88:
    case 87:
    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
    case 63:
    case 62:
    case 48:
    case 47:
    case 46:
    case 45:
    case 44:
    case 43:
    case 42:
    case 41:
    case 40:
    case 39:
    case 38:
    case 37:
      return IN_ANNUL_BRANCH_DELAY_FALSE;

    default:
      return IN_ANNUL_BRANCH_DELAY_TRUE;

    }
}

enum attr_in_uncond_branch_delay
get_attr_in_uncond_branch_delay (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 325:
    case 324:
    case 322:
    case 321:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      insn_extract (insn);
      if (! (symbolic_memory_operand (operands[1], VOIDmode)))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 119:
    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 0)
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 1)
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) || (which_alternative == 2))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 418:
    case 417:
    case 416:
    case 415:
    case 414:
    case 405:
    case 404:
    case 403:
    case 402:
    case 401:
    case 400:
    case 399:
    case 398:
    case 397:
    case 396:
    case 395:
    case 394:
    case 393:
    case 392:
    case 391:
    case 390:
    case 366:
    case 362:
    case 337:
    case 335:
    case 333:
    case 331:
    case 290:
    case 289:
    case 286:
    case 285:
    case 282:
    case 281:
    case 277:
    case 272:
    case 268:
    case 264:
    case 260:
    case 256:
    case 252:
    case 249:
    case 234:
    case 232:
    case 231:
    case 227:
    case 224:
    case 223:
    case 219:
    case 186:
    case 185:
    case 59:
    case 58:
    case 56:
      insn_extract (insn);
      if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case 306:
    case 305:
    case 302:
    case 301:
    case 298:
    case 297:
    case 294:
    case 172:
    case 170:
    case 61:
    case 60:
    case 57:
    case 55:
    case 52:
    case 51:
      insn_extract (insn);
      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
        {
	  return IN_UNCOND_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_UNCOND_BRANCH_DELAY_FALSE;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    case 413:
    case 412:
    case 411:
    case 410:
    case 409:
    case 408:
    case 407:
    case 406:
    case 375:
    case 374:
    case 372:
    case 369:
    case 368:
    case 365:
    case 364:
    case 360:
    case 359:
    case 358:
    case 357:
    case 355:
    case 354:
    case 353:
    case 352:
    case 351:
    case 350:
    case 349:
    case 348:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
    case 300:
    case 292:
    case 276:
    case 271:
    case 267:
    case 263:
    case 259:
    case 255:
    case 253:
    case 251:
    case 250:
    case 248:
    case 247:
    case 246:
    case 244:
    case 243:
    case 241:
    case 240:
    case 238:
    case 237:
    case 226:
    case 218:
    case 213:
    case 212:
    case 211:
    case 201:
    case 200:
    case 199:
    case 126:
    case 125:
    case 124:
    case 122:
    case 121:
    case 116:
    case 115:
    case 107:
    case 104:
    case 100:
    case 96:
    case 94:
    case 92:
    case 91:
    case 88:
    case 87:
    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
    case 63:
    case 62:
    case 48:
    case 47:
    case 46:
    case 45:
    case 44:
    case 43:
    case 42:
    case 41:
    case 40:
    case 39:
    case 38:
    case 37:
      return IN_UNCOND_BRANCH_DELAY_FALSE;

    default:
      return IN_UNCOND_BRANCH_DELAY_TRUE;

    }
}

enum attr_in_branch_delay
get_attr_in_branch_delay (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 325:
    case 324:
    case 322:
    case 321:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      insn_extract (insn);
      if (! (symbolic_memory_operand (operands[1], VOIDmode)))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 119:
    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 0)
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 1)
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 1) || (which_alternative == 2))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 418:
    case 417:
    case 416:
    case 415:
    case 414:
    case 405:
    case 404:
    case 403:
    case 402:
    case 401:
    case 400:
    case 399:
    case 398:
    case 397:
    case 396:
    case 395:
    case 394:
    case 393:
    case 392:
    case 391:
    case 390:
    case 366:
    case 362:
    case 337:
    case 335:
    case 333:
    case 331:
    case 290:
    case 289:
    case 286:
    case 285:
    case 282:
    case 281:
    case 277:
    case 272:
    case 268:
    case 264:
    case 260:
    case 256:
    case 252:
    case 249:
    case 234:
    case 232:
    case 231:
    case 227:
    case 224:
    case 223:
    case 219:
    case 186:
    case 185:
    case 59:
    case 58:
    case 56:
      insn_extract (insn);
      if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case 306:
    case 305:
    case 302:
    case 301:
    case 298:
    case 297:
    case 294:
    case 172:
    case 170:
    case 61:
    case 60:
    case 57:
    case 55:
    case 52:
    case 51:
      insn_extract (insn);
      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
        {
	  return IN_BRANCH_DELAY_TRUE;
        }
      else
        {
	  return IN_BRANCH_DELAY_FALSE;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    case 413:
    case 412:
    case 411:
    case 410:
    case 409:
    case 408:
    case 407:
    case 406:
    case 375:
    case 374:
    case 372:
    case 369:
    case 368:
    case 365:
    case 364:
    case 360:
    case 359:
    case 358:
    case 357:
    case 355:
    case 354:
    case 353:
    case 352:
    case 351:
    case 350:
    case 349:
    case 348:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
    case 300:
    case 292:
    case 276:
    case 271:
    case 267:
    case 263:
    case 259:
    case 255:
    case 253:
    case 251:
    case 250:
    case 248:
    case 247:
    case 246:
    case 244:
    case 243:
    case 241:
    case 240:
    case 238:
    case 237:
    case 226:
    case 218:
    case 213:
    case 212:
    case 211:
    case 201:
    case 200:
    case 199:
    case 126:
    case 125:
    case 124:
    case 122:
    case 121:
    case 116:
    case 115:
    case 107:
    case 104:
    case 100:
    case 96:
    case 94:
    case 92:
    case 91:
    case 88:
    case 87:
    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
    case 63:
    case 62:
    case 48:
    case 47:
    case 46:
    case 45:
    case 44:
    case 43:
    case 42:
    case 41:
    case 40:
    case 39:
    case 38:
    case 37:
      return IN_BRANCH_DELAY_FALSE;

    default:
      return IN_BRANCH_DELAY_TRUE;

    }
}

enum attr_in_call_delay
get_attr_in_call_delay (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 325:
    case 324:
    case 322:
    case 321:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      insn_extract (insn);
      if (! (symbolic_memory_operand (operands[1], VOIDmode)))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 119:
    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 0)
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative != 1)
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 5)
        {
	  return IN_CALL_DELAY_FALSE;
        }
      else
        {
	  if ((which_alternative == 1) || (which_alternative == 2))
	    {
	      return IN_CALL_DELAY_TRUE;
	    }
	  else
	    {
	      return IN_CALL_DELAY_FALSE;
	    }
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 418:
    case 417:
    case 416:
    case 415:
    case 414:
    case 405:
    case 404:
    case 403:
    case 402:
    case 401:
    case 400:
    case 399:
    case 398:
    case 397:
    case 396:
    case 395:
    case 394:
    case 393:
    case 392:
    case 391:
    case 390:
    case 366:
    case 362:
    case 337:
    case 335:
    case 333:
    case 331:
    case 290:
    case 289:
    case 286:
    case 285:
    case 282:
    case 281:
    case 277:
    case 272:
    case 268:
    case 264:
    case 260:
    case 256:
    case 252:
    case 249:
    case 234:
    case 232:
    case 231:
    case 227:
    case 224:
    case 223:
    case 219:
    case 186:
    case 185:
    case 59:
    case 58:
    case 56:
      insn_extract (insn);
      if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case 306:
    case 305:
    case 302:
    case 301:
    case 298:
    case 297:
    case 294:
    case 172:
    case 170:
    case 61:
    case 60:
    case 57:
    case 55:
    case 52:
    case 51:
      insn_extract (insn);
      if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
        {
	  return IN_CALL_DELAY_TRUE;
        }
      else
        {
	  return IN_CALL_DELAY_FALSE;
        }

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    case 413:
    case 412:
    case 411:
    case 410:
    case 409:
    case 408:
    case 407:
    case 406:
    case 375:
    case 374:
    case 372:
    case 369:
    case 368:
    case 365:
    case 364:
    case 360:
    case 359:
    case 358:
    case 357:
    case 355:
    case 354:
    case 353:
    case 352:
    case 351:
    case 350:
    case 349:
    case 348:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
    case 300:
    case 292:
    case 276:
    case 271:
    case 267:
    case 263:
    case 259:
    case 255:
    case 253:
    case 251:
    case 250:
    case 248:
    case 247:
    case 246:
    case 244:
    case 243:
    case 241:
    case 240:
    case 238:
    case 237:
    case 226:
    case 218:
    case 213:
    case 212:
    case 211:
    case 201:
    case 200:
    case 199:
    case 126:
    case 125:
    case 124:
    case 122:
    case 121:
    case 116:
    case 115:
    case 107:
    case 104:
    case 100:
    case 96:
    case 94:
    case 92:
    case 91:
    case 88:
    case 87:
    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
    case 63:
    case 62:
    case 48:
    case 47:
    case 46:
    case 45:
    case 44:
    case 43:
    case 42:
    case 41:
    case 40:
    case 39:
    case 38:
    case 37:
      return IN_CALL_DELAY_FALSE;

    default:
      return IN_CALL_DELAY_TRUE;

    }
}

enum attr_type
get_attr_type (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 4)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 5)
        {
	  return TYPE_FPLOAD;
        }
      else
        {
	  return TYPE_FPSTORE;
        }

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_MOVE;
        }
      else if ((which_alternative == 3) || (which_alternative == 4))
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 5)
        {
	  return TYPE_STORE;
        }
      else
        {
	  return TYPE_STORE;
        }

    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 0) || (which_alternative == 1))
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_LOAD;
        }
      else
        {
	  return TYPE_STORE;
        }

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 4)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 5)
        {
	  return TYPE_MULTI;
        }
      else if (which_alternative == 6)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 7)
        {
	  return TYPE_FPLOAD;
        }
      else
        {
	  return TYPE_FPSTORE;
        }

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_FPLOAD;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 4)
        {
	  return TYPE_FPSTORE;
        }
      else
        {
	  return TYPE_STORE;
        }

    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_LOAD;
        }
      else
        {
	  return TYPE_STORE;
        }

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_FPSTORE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_FPLOAD;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 4)
        {
	  return TYPE_FPSTORE;
        }
      else if (which_alternative == 5)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 6)
        {
	  return TYPE_FPLOAD;
        }
      else
        {
	  return TYPE_LOAD;
        }

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_STORE;
        }
      else
        {
	  return TYPE_LOAD;
        }

    case 111:
    case 116:
    case 122:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_LOAD;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_FPLOAD;
        }
      else
        {
	  return TYPE_STORE;
        }

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_FP;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 2)
        {
	  return TYPE_FPSTORE;
        }
      else if (which_alternative == 3)
        {
	  return TYPE_STORE;
        }
      else if (which_alternative == 4)
        {
	  return TYPE_FPLOAD;
        }
      else
        {
	  return TYPE_LOAD;
        }

    case 125:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_MOVE;
        }
      else if (which_alternative == 1)
        {
	  return TYPE_STORE;
        }
      else
        {
	  return TYPE_LOAD;
        }

    case 160:
    case 162:
    case 164:
    case 168:
    case 184:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (which_alternative == 0)
        {
	  return TYPE_UNARY;
        }
      else
        {
	  return TYPE_LOAD;
        }

    case 49:
    case 50:
    case 53:
    case 54:
    case 371:
    case 372:
    case 373:
      return TYPE_MISC;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    case 62:
    case 63:
    case 87:
    case 88:
    case 364:
    case 365:
    case 374:
    case 375:
    case 406:
    case 407:
    case 408:
    case 409:
    case 410:
    case 411:
    case 412:
    case 413:
      return TYPE_MULTI;

    case 132:
    case 133:
    case 134:
    case 135:
    case 136:
    case 137:
    case 138:
    case 139:
    case 140:
    case 141:
    case 142:
    case 143:
    case 144:
    case 145:
    case 146:
    case 147:
    case 148:
    case 149:
    case 150:
    case 151:
    case 152:
    case 153:
    case 154:
    case 155:
    case 156:
      return TYPE_CMOVE;

    case 327:
    case 328:
    case 329:
      return TYPE_FPSQRT;

    case 318:
    case 319:
      return TYPE_FPDIVD;

    case 320:
      return TYPE_FPDIVS;

    case 313:
    case 314:
    case 315:
    case 316:
    case 317:
      return TYPE_FPMUL;

    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 31:
    case 32:
    case 33:
    case 34:
    case 35:
    case 36:
      return TYPE_FPCMP;

    case 187:
    case 188:
    case 189:
    case 190:
    case 191:
    case 192:
    case 193:
    case 194:
    case 195:
    case 199:
    case 200:
    case 201:
    case 202:
    case 203:
    case 204:
    case 205:
    case 206:
    case 207:
    case 211:
    case 212:
    case 213:
    case 214:
    case 215:
    case 216:
    case 307:
    case 308:
    case 309:
    case 310:
    case 311:
    case 312:
    case 321:
    case 322:
    case 323:
    case 324:
    case 325:
    case 326:
      return TYPE_FP;

    case 233:
    case 235:
      return TYPE_IMUL;

    case 352:
    case 353:
    case 354:
    case 355:
      return TYPE_CALL_NO_DELAY_SLOT;

    case 348:
    case 349:
    case 350:
    case 351:
    case 357:
    case 358:
    case 359:
    case 360:
      return TYPE_CALL;

    case 74:
    case 75:
    case 76:
    case 77:
    case 78:
    case 79:
    case 80:
    case 81:
      return TYPE_BRANCH;

    case 338:
    case 340:
    case 341:
    case 342:
    case 343:
    case 344:
    case 345:
    case 368:
    case 369:
      return TYPE_UNCOND_BRANCH;

    case 330:
    case 334:
    case 336:
      return TYPE_SHIFT;

    case 220:
    case 228:
    case 257:
    case 261:
    case 265:
    case 269:
    case 273:
    case 278:
      return TYPE_IALU;

    case 100:
    case 104:
    case 107:
    case 115:
    case 121:
    case 126:
      return TYPE_STORE;

    case 158:
    case 166:
    case 174:
    case 176:
    case 178:
    case 180:
    case 182:
      return TYPE_LOAD;

    case 23:
    case 30:
    case 169:
    case 171:
    case 221:
    case 222:
    case 229:
    case 230:
    case 279:
    case 280:
    case 283:
    case 284:
    case 287:
    case 288:
    case 295:
    case 296:
    case 303:
    case 304:
    case 332:
      return TYPE_COMPARE;

    case 37:
    case 38:
    case 39:
    case 40:
    case 41:
    case 42:
    case 43:
    case 44:
    case 51:
    case 52:
    case 55:
    case 57:
    case 60:
    case 61:
    case 170:
    case 172:
    case 292:
    case 293:
    case 294:
    case 297:
    case 298:
    case 300:
    case 301:
    case 302:
    case 305:
    case 306:
      return TYPE_UNARY;

    case 84:
    case 85:
    case 86:
    case 91:
    case 92:
    case 93:
    case 94:
    case 95:
    case 96:
      return TYPE_MOVE;

    default:
      return TYPE_BINARY;

    }
}

enum attr_use_clobbered
get_attr_use_clobbered (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      return USE_CLOBBERED_FALSE;

    }
}

int
eligible_for_delay (delay_insn, slot, candidate_insn, flags)
     rtx delay_insn;
     int slot;
     rtx candidate_insn;
     int flags;
{
  rtx insn;

  if (slot >= 1)
    abort ();

  insn = delay_insn;
  switch (recog_memoized (insn))
    {
    case 360:
    case 359:
    case 358:
    case 357:
    case 351:
    case 350:
    case 349:
    case 348:
      slot += 1 * 1;
      break;
      break;

    case 369:
    case 368:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
      slot += 3 * 1;
      break;
      break;

    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
      slot += 2 * 1;
      break;
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      slot += 0 * 1;
      break;
      break;

    }

  if (slot < 1)
    abort ();

  insn = candidate_insn;
  switch (slot)
    {
    case 3:
      switch (recog_memoized (insn))
	{
        case 325:
        case 324:
        case 322:
        case 321:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 182:
        case 180:
        case 178:
        case 176:
        case 174:
        case 166:
        case 158:
	  insn_extract (insn);
	  if (! (symbolic_memory_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 119:
        case 118:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 0) || (which_alternative == 1))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 114:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 113:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 111:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 0)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 110:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 1)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 109:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 1) || (which_alternative == 2))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 106:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 102:
        case 98:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 418:
        case 417:
        case 416:
        case 415:
        case 414:
        case 405:
        case 404:
        case 403:
        case 402:
        case 401:
        case 400:
        case 399:
        case 398:
        case 397:
        case 396:
        case 395:
        case 394:
        case 393:
        case 392:
        case 391:
        case 390:
        case 366:
        case 362:
        case 337:
        case 335:
        case 333:
        case 331:
        case 290:
        case 289:
        case 286:
        case 285:
        case 282:
        case 281:
        case 277:
        case 272:
        case 268:
        case 264:
        case 260:
        case 256:
        case 252:
        case 249:
        case 234:
        case 232:
        case 231:
        case 227:
        case 224:
        case 223:
        case 219:
        case 186:
        case 185:
        case 59:
        case 58:
        case 56:
	  insn_extract (insn);
	  if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 306:
        case 305:
        case 302:
        case 301:
        case 298:
        case 297:
        case 294:
        case 172:
        case 170:
        case 61:
        case 60:
        case 57:
        case 55:
        case 52:
        case 51:
	  insn_extract (insn);
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        case 413:
        case 412:
        case 411:
        case 410:
        case 409:
        case 408:
        case 407:
        case 406:
        case 375:
        case 374:
        case 372:
        case 369:
        case 368:
        case 365:
        case 364:
        case 360:
        case 359:
        case 358:
        case 357:
        case 355:
        case 354:
        case 353:
        case 352:
        case 351:
        case 350:
        case 349:
        case 348:
        case 345:
        case 344:
        case 343:
        case 342:
        case 341:
        case 340:
        case 338:
        case 300:
        case 292:
        case 276:
        case 271:
        case 267:
        case 263:
        case 259:
        case 255:
        case 253:
        case 251:
        case 250:
        case 248:
        case 247:
        case 246:
        case 244:
        case 243:
        case 241:
        case 240:
        case 238:
        case 237:
        case 226:
        case 218:
        case 213:
        case 212:
        case 211:
        case 201:
        case 200:
        case 199:
        case 126:
        case 125:
        case 124:
        case 122:
        case 121:
        case 116:
        case 115:
        case 107:
        case 104:
        case 100:
        case 96:
        case 94:
        case 92:
        case 91:
        case 88:
        case 87:
        case 81:
        case 80:
        case 79:
        case 78:
        case 77:
        case 76:
        case 75:
        case 74:
        case 63:
        case 62:
        case 48:
        case 47:
        case 46:
        case 45:
        case 44:
        case 43:
        case 42:
        case 41:
        case 40:
        case 39:
        case 38:
        case 37:
	  return 0;

        default:
	  return 1;

      }
    case 2:
      switch (recog_memoized (insn))
	{
        case 325:
        case 324:
        case 322:
        case 321:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 182:
        case 180:
        case 178:
        case 176:
        case 174:
        case 166:
        case 158:
	  insn_extract (insn);
	  if (! (symbolic_memory_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 119:
        case 118:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 0) || (which_alternative == 1))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 114:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 113:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 111:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 0)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 110:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 1)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 109:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 1) || (which_alternative == 2))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 106:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 102:
        case 98:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 418:
        case 417:
        case 416:
        case 415:
        case 414:
        case 405:
        case 404:
        case 403:
        case 402:
        case 401:
        case 400:
        case 399:
        case 398:
        case 397:
        case 396:
        case 395:
        case 394:
        case 393:
        case 392:
        case 391:
        case 390:
        case 366:
        case 362:
        case 337:
        case 335:
        case 333:
        case 331:
        case 290:
        case 289:
        case 286:
        case 285:
        case 282:
        case 281:
        case 277:
        case 272:
        case 268:
        case 264:
        case 260:
        case 256:
        case 252:
        case 249:
        case 234:
        case 232:
        case 231:
        case 227:
        case 224:
        case 223:
        case 219:
        case 186:
        case 185:
        case 59:
        case 58:
        case 56:
	  insn_extract (insn);
	  if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 306:
        case 305:
        case 302:
        case 301:
        case 298:
        case 297:
        case 294:
        case 172:
        case 170:
        case 61:
        case 60:
        case 57:
        case 55:
        case 52:
        case 51:
	  insn_extract (insn);
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        case 413:
        case 412:
        case 411:
        case 410:
        case 409:
        case 408:
        case 407:
        case 406:
        case 375:
        case 374:
        case 372:
        case 369:
        case 368:
        case 365:
        case 364:
        case 360:
        case 359:
        case 358:
        case 357:
        case 355:
        case 354:
        case 353:
        case 352:
        case 351:
        case 350:
        case 349:
        case 348:
        case 345:
        case 344:
        case 343:
        case 342:
        case 341:
        case 340:
        case 338:
        case 300:
        case 292:
        case 276:
        case 271:
        case 267:
        case 263:
        case 259:
        case 255:
        case 253:
        case 251:
        case 250:
        case 248:
        case 247:
        case 246:
        case 244:
        case 243:
        case 241:
        case 240:
        case 238:
        case 237:
        case 226:
        case 218:
        case 213:
        case 212:
        case 211:
        case 201:
        case 200:
        case 199:
        case 126:
        case 125:
        case 124:
        case 122:
        case 121:
        case 116:
        case 115:
        case 107:
        case 104:
        case 100:
        case 96:
        case 94:
        case 92:
        case 91:
        case 88:
        case 87:
        case 81:
        case 80:
        case 79:
        case 78:
        case 77:
        case 76:
        case 75:
        case 74:
        case 63:
        case 62:
        case 48:
        case 47:
        case 46:
        case 45:
        case 44:
        case 43:
        case 42:
        case 41:
        case 40:
        case 39:
        case 38:
        case 37:
	  return 0;

        default:
	  return 1;

      }
    case 1:
      switch (recog_memoized (insn))
	{
        case 325:
        case 324:
        case 322:
        case 321:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 182:
        case 180:
        case 178:
        case 176:
        case 174:
        case 166:
        case 158:
	  insn_extract (insn);
	  if (! (symbolic_memory_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 119:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 1) || (which_alternative == 0))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 118:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 0) || (which_alternative == 1))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 114:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 113:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative != 0) && ((which_alternative != 1) && ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && (((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode))))) || ((which_alternative == 2) || (which_alternative == 3))))))) || (((which_alternative == 0) || (which_alternative == 1)) && ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 111:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 0)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 110:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 1)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 109:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 1) || (which_alternative == 2))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 106:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((((which_alternative == 3) || (which_alternative == 4)) && ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))))) || ((((which_alternative == 5) || (which_alternative == 6)) && ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || (((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 4)) && ((which_alternative != 5) && (which_alternative != 6))) && (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))))))))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 102:
        case 98:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((which_alternative == 3) || (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (which_alternative == 1))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 418:
        case 417:
        case 416:
        case 415:
        case 414:
        case 405:
        case 404:
        case 403:
        case 402:
        case 401:
        case 400:
        case 399:
        case 398:
        case 397:
        case 396:
        case 395:
        case 394:
        case 393:
        case 392:
        case 391:
        case 390:
        case 366:
        case 362:
        case 337:
        case 335:
        case 333:
        case 331:
        case 290:
        case 289:
        case 286:
        case 285:
        case 282:
        case 281:
        case 277:
        case 272:
        case 268:
        case 264:
        case 260:
        case 256:
        case 252:
        case 249:
        case 234:
        case 232:
        case 231:
        case 227:
        case 224:
        case 223:
        case 219:
        case 186:
        case 185:
        case 59:
        case 58:
        case 56:
	  insn_extract (insn);
	  if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 306:
        case 305:
        case 302:
        case 301:
        case 298:
        case 297:
        case 294:
        case 172:
        case 170:
        case 61:
        case 60:
        case 57:
        case 55:
        case 52:
        case 51:
	  insn_extract (insn);
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        case 413:
        case 412:
        case 411:
        case 410:
        case 409:
        case 408:
        case 407:
        case 406:
        case 375:
        case 374:
        case 372:
        case 369:
        case 368:
        case 365:
        case 364:
        case 360:
        case 359:
        case 358:
        case 357:
        case 355:
        case 354:
        case 353:
        case 352:
        case 351:
        case 350:
        case 349:
        case 348:
        case 345:
        case 344:
        case 343:
        case 342:
        case 341:
        case 340:
        case 338:
        case 300:
        case 292:
        case 276:
        case 271:
        case 267:
        case 263:
        case 259:
        case 255:
        case 253:
        case 251:
        case 250:
        case 248:
        case 247:
        case 246:
        case 244:
        case 243:
        case 241:
        case 240:
        case 238:
        case 237:
        case 226:
        case 218:
        case 213:
        case 212:
        case 211:
        case 201:
        case 200:
        case 199:
        case 126:
        case 125:
        case 124:
        case 122:
        case 121:
        case 116:
        case 115:
        case 107:
        case 104:
        case 100:
        case 96:
        case 94:
        case 92:
        case 91:
        case 88:
        case 87:
        case 81:
        case 80:
        case 79:
        case 78:
        case 77:
        case 76:
        case 75:
        case 74:
        case 63:
        case 62:
        case 48:
        case 47:
        case 46:
        case 45:
        case 44:
        case 43:
        case 42:
        case 41:
        case 40:
        case 39:
        case 38:
        case 37:
	  return 0;

        default:
	  return 1;

      }
    default:
      abort ();
    }
}

int
eligible_for_annul_false (delay_insn, slot, candidate_insn, flags)
     rtx delay_insn;
     int slot;
     rtx candidate_insn;
     int flags;
{
  rtx insn;

  if (slot >= 1)
    abort ();

  insn = delay_insn;
  switch (recog_memoized (insn))
    {
    case 360:
    case 359:
    case 358:
    case 357:
    case 351:
    case 350:
    case 349:
    case 348:
      slot += 1 * 1;
      break;
      break;

    case 369:
    case 368:
    case 345:
    case 344:
    case 343:
    case 342:
    case 341:
    case 340:
    case 338:
      slot += 3 * 1;
      break;
      break;

    case 81:
    case 80:
    case 79:
    case 78:
    case 77:
    case 76:
    case 75:
    case 74:
      slot += 2 * 1;
      break;
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      slot += 0 * 1;
      break;
      break;

    }

  if (slot < 1)
    abort ();

  insn = candidate_insn;
  switch (slot)
    {
    case 3:
      switch (recog_memoized (insn))
	{
        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        default:
	  return 0;

      }
    case 2:
      switch (recog_memoized (insn))
	{
        case 325:
        case 324:
        case 322:
        case 321:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative != 1) || (! (((sparc_arch_type) == (ARCH_ARCH32BIT)))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 182:
        case 180:
        case 178:
        case 176:
        case 174:
        case 166:
        case 158:
	  insn_extract (insn);
	  if (! (symbolic_memory_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 119:
        case 118:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 0) || (which_alternative == 1))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 114:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 1) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 113:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((((which_alternative == 3) || (which_alternative == 2)) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || ((((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && (! (symbolic_memory_operand (operands[0], VOIDmode)))))))) || (((which_alternative == 1) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || (((which_alternative != 3) && (which_alternative != 2)) && ((which_alternative == 0) || ((which_alternative == 2) || (which_alternative == 3)))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 111:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 0)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 110:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (which_alternative != 1)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 109:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((which_alternative == 1) || (which_alternative == 2))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 106:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || ((which_alternative == 2) || (((which_alternative == 3) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 4) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (((which_alternative == 5) && (! (symbolic_memory_operand (operands[0], VOIDmode)))) || ((which_alternative == 6) && (! (symbolic_memory_operand (operands[0], VOIDmode))))))))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 102:
        case 98:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if (((which_alternative == 0) && ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))) || ((which_alternative == 1) || (((which_alternative == 2) && (! (symbolic_memory_operand (operands[1], VOIDmode)))) || (which_alternative == 3))))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 418:
        case 417:
        case 416:
        case 415:
        case 414:
        case 405:
        case 404:
        case 403:
        case 402:
        case 401:
        case 400:
        case 399:
        case 398:
        case 397:
        case 396:
        case 395:
        case 394:
        case 393:
        case 392:
        case 391:
        case 390:
        case 366:
        case 362:
        case 337:
        case 335:
        case 333:
        case 331:
        case 290:
        case 289:
        case 286:
        case 285:
        case 282:
        case 281:
        case 277:
        case 272:
        case 268:
        case 264:
        case 260:
        case 256:
        case 252:
        case 249:
        case 234:
        case 232:
        case 231:
        case 227:
        case 224:
        case 223:
        case 219:
        case 186:
        case 185:
        case 59:
        case 58:
        case 56:
	  insn_extract (insn);
	  if ((arith_operand (operands[2], VOIDmode)) || (arith_double_operand (operands[2], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case 306:
        case 305:
        case 302:
        case 301:
        case 298:
        case 297:
        case 294:
        case 172:
        case 170:
        case 61:
        case 60:
        case 57:
        case 55:
        case 52:
        case 51:
	  insn_extract (insn);
	  if ((arith_operand (operands[1], VOIDmode)) || (arith_double_operand (operands[1], VOIDmode)))
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }

        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        case 413:
        case 412:
        case 411:
        case 410:
        case 409:
        case 408:
        case 407:
        case 406:
        case 375:
        case 374:
        case 372:
        case 369:
        case 368:
        case 365:
        case 364:
        case 360:
        case 359:
        case 358:
        case 357:
        case 355:
        case 354:
        case 353:
        case 352:
        case 351:
        case 350:
        case 349:
        case 348:
        case 345:
        case 344:
        case 343:
        case 342:
        case 341:
        case 340:
        case 338:
        case 300:
        case 292:
        case 276:
        case 271:
        case 267:
        case 263:
        case 259:
        case 255:
        case 253:
        case 251:
        case 250:
        case 248:
        case 247:
        case 246:
        case 244:
        case 243:
        case 241:
        case 240:
        case 238:
        case 237:
        case 226:
        case 218:
        case 213:
        case 212:
        case 211:
        case 201:
        case 200:
        case 199:
        case 126:
        case 125:
        case 124:
        case 122:
        case 121:
        case 116:
        case 115:
        case 107:
        case 104:
        case 100:
        case 96:
        case 94:
        case 92:
        case 91:
        case 88:
        case 87:
        case 81:
        case 80:
        case 79:
        case 78:
        case 77:
        case 76:
        case 75:
        case 74:
        case 63:
        case 62:
        case 48:
        case 47:
        case 46:
        case 45:
        case 44:
        case 43:
        case 42:
        case 41:
        case 40:
        case 39:
        case 38:
        case 37:
	  return 0;

        default:
	  return 1;

      }
    case 1:
      switch (recog_memoized (insn))
	{
        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        default:
	  return 0;

      }
    default:
      abort ();
    }
}

static int
fp_mds_unit_blockage (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 329:
    case 328:
    case 327:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 6;
        }
      break;

    case 320:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 4;
        }
      break;

    case 319:
    case 318:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 5;
        }
      break;

    case 317:
    case 316:
    case 315:
    case 314:
    case 313:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 7;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 7;

    case 1:
      return 37 /* 0x25 */;

    case 2:
      return 63 /* 0x3f */;

    case 3:
      return 3;

    case 4:
      return 12 /* 0xc */;

    case 5:
      return 21 /* 0x15 */;

    case 6:
      return 30 /* 0x1e */;

    case 7:
      return 12 /* 0xc */;

    }
}

static int
fp_mds_unit_conflict_cost (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 329:
    case 328:
    case 327:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 6;
        }
      break;

    case 320:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 4;
        }
      break;

    case 319:
    case 318:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 5;
        }
      break;

    case 317:
    case 316:
    case 315:
    case 314:
    case 313:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 7;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 7;

    case 1:
      return 37 /* 0x25 */;

    case 2:
      return 63 /* 0x3f */;

    case 3:
      return 3;

    case 4:
      return 12 /* 0xc */;

    case 5:
      return 21 /* 0x15 */;

    case 6:
      return 30 /* 0x1e */;

    case 7:
      return 12 /* 0xc */;

    }
}

static int
fp_alu_unit_blockage (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 124:
    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 1;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 5;

    case 1:
      switch (recog_memoized (insn))
	{
        case 118:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case 124:
        case 113:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case 110:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case 109:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case 106:
	  insn_extract (insn);
	  if (! constrain_operands (INSN_CODE (insn), reload_completed))
	    fatal_insn_not_found (insn);
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case 326:
        case 325:
        case 324:
        case 323:
        case 322:
        case 321:
        case 312:
        case 311:
        case 310:
        case 309:
        case 308:
        case 307:
        case 216:
        case 215:
        case 214:
        case 213:
        case 212:
        case 211:
        case 207:
        case 206:
        case 205:
        case 204:
        case 203:
        case 202:
        case 201:
        case 200:
        case 199:
        case 195:
        case 194:
        case 193:
        case 192:
        case 191:
        case 190:
        case 189:
        case 188:
        case 187:
        case 36:
        case 35:
        case 34:
        case 33:
        case 32:
        case 31:
        case 29:
        case 28:
        case 27:
        case 26:
        case 25:
        case 24:
	  if ((TARGET_SUPERSPARC))
	    {
	      return 3;
	    }
	  else
	    {
	      return 5;
	    }

        case -1:
	  if (GET_CODE (PATTERN (insn)) != ASM_INPUT
	      && asm_noperands (PATTERN (insn)) < 0)
	    fatal_insn_not_found (insn);
        default:
	  return 5;

      }

    }
}

static int
fp_alu_unit_conflict_cost (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 326:
    case 325:
    case 324:
    case 323:
    case 322:
    case 321:
    case 312:
    case 311:
    case 310:
    case 309:
    case 308:
    case 307:
    case 216:
    case 215:
    case 214:
    case 213:
    case 212:
    case 211:
    case 207:
    case 206:
    case 205:
    case 204:
    case 203:
    case 202:
    case 201:
    case 200:
    case 199:
    case 195:
    case 194:
    case 193:
    case 192:
    case 191:
    case 190:
    case 189:
    case 188:
    case 187:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 124:
    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 1;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 5;

    case 1:
      return 3;

    }
}

static int
memory_unit_blockage (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && (which_alternative != 3)))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && ((which_alternative != 4) && ((which_alternative != 5) && (which_alternative != 6))))))) || ((which_alternative == 1) || (which_alternative == 6))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if (((which_alternative == 1) || (which_alternative == 6)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 184:
    case 168:
    case 164:
    case 162:
    case 160:
    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 3) || (which_alternative == 2)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 122:
    case 116:
    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) || (which_alternative == 1)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 2) || (which_alternative == 5)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 2) || (which_alternative == 4)) || (which_alternative == 7)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative == 2) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 3) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 125:
    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 3;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 2;

    case 1:
      return 3;

    case 2:
      return 3;

    case 3:
      return 3;

    }
}

static int
memory_unit_conflict_cost (executing_insn, candidate_insn)
     rtx executing_insn;
     rtx candidate_insn;
{
  rtx insn;
  int casenum;

  insn = executing_insn;
  switch (recog_memoized (insn))
    {
    case 182:
    case 180:
    case 178:
    case 176:
    case 174:
    case 166:
    case 158:
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 124:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && (which_alternative != 3)))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 4) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 119:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative != 0) && ((which_alternative != 2) && (which_alternative != 3))) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 118:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative != 0) && ((which_alternative != 1) && ((which_alternative != 2) && ((which_alternative != 3) && ((which_alternative != 4) && ((which_alternative != 5) && (which_alternative != 6))))))) || ((which_alternative == 1) || (which_alternative == 6))) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if (((which_alternative == 1) || (which_alternative == 6)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 184:
    case 168:
    case 164:
    case 162:
    case 160:
    case 114:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((! (TARGET_SUPERSPARC)))
        {
	  casenum = 0;
        }
      else
        {
	  casenum = 1;
        }
      break;

    case 113:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 3) || (which_alternative == 2)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 3) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 122:
    case 116:
    case 111:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 0) || (which_alternative == 1)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 0) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 1) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 110:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 2) || (which_alternative == 5)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 5) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 109:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((((which_alternative == 2) || (which_alternative == 4)) || (which_alternative == 7)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative == 2) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else if ((which_alternative == 7) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 2;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 106:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if (((which_alternative == 3) || (which_alternative == 4)) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if (((which_alternative == 3) || (which_alternative == 4)) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case 125:
    case 102:
    case 98:
      insn_extract (insn);
      if (! constrain_operands (INSN_CODE (insn), reload_completed))
        fatal_insn_not_found (insn);
      if ((which_alternative == 2) && ((! (TARGET_SUPERSPARC))))
        {
	  casenum = 0;
        }
      else if ((which_alternative == 2) && ((TARGET_SUPERSPARC)))
        {
	  casenum = 1;
        }
      else
        {
	  casenum = 3;
        }
      break;

    case -1:
      if (GET_CODE (PATTERN (insn)) != ASM_INPUT
          && asm_noperands (PATTERN (insn)) < 0)
        fatal_insn_not_found (insn);
    default:
      casenum = 3;
      break;

    }

  insn = candidate_insn;
  switch (casenum)
    {
    case 0:
      return 2;

    case 1:
      return 3;

    case 2:
      return 3;

    case 3:
      return 3;

    }
}

struct function_unit_desc function_units[] = {
  {"memory", 1, 1, 0, 0, 3, memory_unit_ready_cost, memory_unit_conflict_cost, 3, memory_unit_blockage_range, memory_unit_blockage}, 
  {"fp_alu", 2, 1, 0, 0, 5, fp_alu_unit_ready_cost, fp_alu_unit_conflict_cost, 5, fp_alu_unit_blockage_range, fp_alu_unit_blockage}, 
  {"fp_mds", 4, 1, 0, 0, 63, fp_mds_unit_ready_cost, fp_mds_unit_conflict_cost, 63, fp_mds_unit_blockage_range, fp_mds_unit_blockage}, 
  {"shift", 8, 1, 0, 3, 3, shift_unit_ready_cost, 0, 3, 0, 0}, 
  {"iwport", 16, 2, 0, 3, 3, iwport_unit_ready_cost, 0, 3, 0, 0}, 
};

int
const_num_delay_slots (insn)
     rtx insn;
{
  switch (recog_memoized (insn))
    {
    default:
      return 1;
    }
}

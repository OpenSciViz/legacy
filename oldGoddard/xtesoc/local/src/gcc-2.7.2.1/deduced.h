#define clock_t long
#define dev_t unsigned long
#define fpos_t long
#define gid_t long
#define ino_t unsigned long
#define mode_t unsigned long
#define nlink_t unsigned long
#define off_t long
#define pid_t long
#define ptrdiff_t __PTRDIFF_TYPE__
#define size_t __SIZE_TYPE__
#define time_t long
#define uid_t long
#define wchar_t __WCHAR_TYPE__
#define int32_t int /* default */
#define uint32_t unsigned int /* default */
#define wait_arg_t void
#define ssize_t int
#define va_list void *
#define chtype int
#define box32 box
#define initscr32 initscr
#define w32addch waddch
#define w32insch winsch

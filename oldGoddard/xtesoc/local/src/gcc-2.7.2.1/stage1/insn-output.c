/* Generated automatically by the program `genoutput'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "regs.h"
#include "hard-reg-set.h"
#include "real.h"
#include "insn-config.h"

#include "conditions.h"
#include "insn-flags.h"
#include "insn-attr.h"

#include "insn-codes.h"

#include "recog.h"

#include <stdio.h>
#include "output.h"

static char *
output_62 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return output_scc_insn (operands, insn); 
}

static char *
output_63 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return output_scc_insn (operands, insn); 
}

static char *
output_74 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], 0, 1, 0,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_75 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], 0, 1, 1,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_76 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], operands[1], 2, 0,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_77 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], operands[1], 2, 1,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_78 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], operands[1], 2, 0,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_79 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_cbranch (operands[0], operands[1], 2, 1,
			 final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			 ! final_sequence);
}
}

static char *
output_80 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_v9branch (operands[0], 1, 2, 0,
			  final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			  ! final_sequence);
}
}

static char *
output_81 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return output_v9branch (operands[0], 1, 2, 1,
			  final_sequence && INSN_ANNULLED_BRANCH_P (insn),
			  ! final_sequence);
}
}

static char *
output_82 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return TARGET_V9 ? "add %1,%%lo(%a2),%0" : "or %1,%%lo(%a2),%0";
}

static char *
output_83 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return TARGET_V9 ? "add %1,%%lo(%a2),%0" : "or %1,%%lo(%a2),%0";
}

static char *
output_89 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  /* Don't output a 64 bit constant, since we can't trust the assembler to
     handle it correctly.  */
  if (GET_CODE (operands[2]) == CONST_DOUBLE)
    operands[2] = gen_rtx (CONST_INT, VOIDmode, CONST_DOUBLE_LOW (operands[2]));
  return "or %R1,%%lo(%a2),%R0";
}
}

static char *
output_90 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  /* Don't output a 64 bit constant, since we can't trust the assembler to
     handle it correctly.  */
  if (GET_CODE (operands[2]) == CONST_DOUBLE)
    operands[2] = gen_rtx (CONST_INT, VOIDmode, CONST_DOUBLE_LOW (operands[2]));
  /* Note that we use add here.  This is important because Medium/Anywhere
     code model support depends on it.  */
  return "add %1,%%lo(%a2),%0";
}
}

static char *
output_91 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op0 = operands[0];
  rtx op1 = operands[1];

  if (GET_CODE (op1) == CONST_INT)
    {
      operands[0] = operand_subword (op0, 1, 0, DImode);
      output_asm_insn ("sethi %%hi(%a1),%0", operands);

      operands[0] = operand_subword (op0, 0, 0, DImode);
      if (INTVAL (op1) < 0)
	return "mov -1,%0";
      else
	return "mov 0,%0";
    }
  else if (GET_CODE (op1) == CONST_DOUBLE)
    {
      operands[0] = operand_subword (op0, 1, 0, DImode);
      operands[1] = gen_rtx (CONST_INT, VOIDmode, CONST_DOUBLE_LOW (op1));
      output_asm_insn ("sethi %%hi(%a1),%0", operands);

      operands[0] = operand_subword (op0, 0, 0, DImode);
      operands[1] = gen_rtx (CONST_INT, VOIDmode, CONST_DOUBLE_HIGH (op1));
      return singlemove_string (operands);
    }
  else
    abort ();
  return "";
}
}

static char *
output_92 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx high, low;
  
  split_double (operands[1], &high, &low);

  if (high == const0_rtx)
    {
      operands[1] = low;
      output_asm_insn ("sethi %%hi(%a1),%0", operands);
    }
  else
    {
      operands[1] = high;
      output_asm_insn (singlemove_string (operands), operands);

      operands[1] = low;
      output_asm_insn ("sllx %0,32,%0", operands);
      if (low != const0_rtx)
	output_asm_insn ("sethi %%hi(%a1),%%g1; or %0,%%g1,%0", operands);
    }

  return "";
}
}

static char *
output_98 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_98[] = {
    "mov %1,%0",
    "sethi %%hi(%a1),%0",
    "ldub %1,%0",
    "stb %r1,%0",
  };
  return strings_98[which_alternative];
}

static char *
output_102 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_102[] = {
    "mov %1,%0",
    "sethi %%hi(%a1),%0",
    "lduh %1,%0",
    "sth %r1,%0",
  };
  return strings_102[which_alternative];
}

static char *
output_106 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_106[] = {
    "mov %1,%0",
    "fmovs %1,%0",
    "sethi %%hi(%a1),%0",
    "ld %1,%0",
    "ld %1,%0",
    "st %r1,%0",
    "st %r1,%0",
  };
  return strings_106[which_alternative];
}

static char *
output_109 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (FP_REG_P (operands[0]) || FP_REG_P (operands[1]))
    return output_fp_move_double (operands);
  return output_move_double (operands);
}
}

static char *
output_110 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  switch (which_alternative)
    {
    case 0:
      return "mov %1,%0";
    case 1:
      /* Sethi does not sign extend, so we must use a little trickery
	 to use it for negative numbers.  Invert the constant before
	 loading it in, then use a xor immediate to invert the loaded bits
	 (along with the upper 32 bits) to the desired constant.  This
	 works because the sethi and immediate fields overlap.  */

      if ((INTVAL (operands[1]) & 0x80000000) == 0)
	return "sethi %%hi(%a1),%0";
      else
	{
	  operands[1] = gen_rtx (CONST_INT, VOIDmode,
				 ~ INTVAL (operands[1]));
	  output_asm_insn ("sethi %%hi(%a1),%0", operands);
	  /* The low 10 bits are already zero, but invert the rest.
	     Assemblers don't accept 0x1c00, so use -0x400 instead.  */
	  return "xor %0,-0x400,%0";
	}
    case 2:
      return "ldx %1,%0";
    case 3:
      return "stx %r1,%0";
    case 4:
      return "mov %1,%0";
    case 5:
      return "ldd %1,%0";
    case 6:
      return "std %1,%0";
    }
}
}

static char *
output_111 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  switch (which_alternative)
    {
    case 0:
      return singlemove_string (operands);
    case 1:
      return "ld %1,%0";
    case 2:
      return "st %%g0,%0";
    }
}
}

static char *
output_113 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_113[] = {
    "fmovs %1,%0",
    "mov %1,%0",
    "ld %1,%0",
    "ld %1,%0",
    "st %r1,%0",
    "st %r1,%0",
  };
  return strings_113[which_alternative];
}

static char *
output_114 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_114[] = {
    "mov %1,%0",
    "ld %1,%0",
    "st %r1,%0",
  };
  return strings_114[which_alternative];
}

static char *
output_116 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  switch (which_alternative)
    {
    case 0:
      return output_move_double (operands);
    case 1:
      return output_fp_move_double (operands);
    case 2:
      if (TARGET_V9)
	{
	  return "stx %%g0,%0";
	}
      else
	{
	  operands[1] = adj_offsettable_operand (operands[0], 4);
	  return "st %%g0,%0\n\tst %%g0,%1";
	}
    }
}
}

static char *
output_118 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (FP_REG_P (operands[0]) || FP_REG_P (operands[1]))
    return output_fp_move_double (operands);
  return output_move_double (operands);
}
}

static char *
output_119 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return output_move_double (operands);
}

static char *
output_121 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  output_asm_insn ("sethi %%hi(%a0),%2", operands);
  if (which_alternative == 0)
    return "std %1,[%2+%%lo(%a0)]";
  else
    return "st %%g0,[%2+%%lo(%a0)]\n\tst %%g0,[%2+%%lo(%a0+4)]";
}
}

static char *
output_122 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  switch (which_alternative)
    {
    case 0:
      return output_move_quad (operands);
    case 1:
      return output_fp_move_quad (operands);
    case 2:
      if (TARGET_V9)
	{
	  operands[1] = adj_offsettable_operand (operands[0], 8);
	  return "stx %%g0,%0\n\tstx %%g0,%1";
	}
      else
	{
	  /* ??? Do we run off the end of the array here? */
	  operands[1] = adj_offsettable_operand (operands[0], 4);
	  operands[2] = adj_offsettable_operand (operands[0], 8);
	  operands[3] = adj_offsettable_operand (operands[0], 12);
	  return "st %%g0,%0\n\tst %%g0,%1\n\tst %%g0,%2\n\tst %%g0,%3";
	}
    }
}
}

static char *
output_124 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (FP_REG_P (operands[0]) || FP_REG_P (operands[1]))
    return output_fp_move_quad (operands);
  return output_move_quad (operands);
}
}

static char *
output_125 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (FP_REG_P (operands[0]) || FP_REG_P (operands[1]))
    return output_fp_move_quad (operands);
  return output_move_quad (operands);
}
}

static char *
output_126 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  output_asm_insn ("sethi %%hi(%a0),%2", operands);
  if (which_alternative == 0)
    return "std %1,[%2+%%lo(%a0)]\n\tstd %S1,[%2+%%lo(%a0+8)]";
  else
    return "st %%g0,[%2+%%lo(%a0)]\n\tst %%g0,[%2+%%lo(%a0+4)]\n\t st %%g0,[%2+%%lo(%a0+8)]\n\tst %%g0,[%2+%%lo(%a0+12)]";
}
}

static char *
output_160 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_160[] = {
    "and %1,0xff,%0",
    "ldub %1,%0",
  };
  return strings_160[which_alternative];
}

static char *
output_162 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_162[] = {
    "and %1,0xff,%0",
    "ldub %1,%0",
  };
  return strings_162[which_alternative];
}

static char *
output_164 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_164[] = {
    "and %1,0xff,%0",
    "ldub %1,%0",
  };
  return strings_164[which_alternative];
}

static char *
output_168 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_168[] = {
    "srl %1,0,%0",
    "lduw %1,%0",
  };
  return strings_168[which_alternative];
}

static char *
output_184 (operands, insn)
     rtx *operands;
     rtx insn;
{
  static /*const*/ char *const strings_184[] = {
    "sra %1,0,%0",
    "ldsw %1,%0",
  };
  return strings_184[which_alternative];
}

static char *
output_185 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  int len = INTVAL (operands[1]);
  int pos = 32 - INTVAL (operands[2]) - len;
  unsigned mask = ((1 << len) - 1) << pos;

  operands[1] = gen_rtx (CONST_INT, VOIDmode, mask);
  return "andcc %0,%1,%%g0";
}
}

static char *
output_186 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  int len = INTVAL (operands[1]);
  int pos = 64 - INTVAL (operands[2]) - len;
  unsigned mask = ((1 << len) - 1) << pos;

  operands[1] = gen_rtx (CONST_INT, VOIDmode, mask);
  return "andcc %0,%1,%%g0";
}
}

static char *
output_199 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[1]) == MEM)
    output_asm_insn ("ldd %1,%2", operands);
  else
    output_asm_insn ("stx %1,%3\n\tldd %3,%2", operands);
  return "fxtos %2,%0";
}
}

static char *
output_200 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[1]) == MEM)
    output_asm_insn ("ldd %1,%2", operands);
  else
    output_asm_insn ("stx %1,%3\n\tldd %3,%2", operands);
  return "fxtod %2,%0";
}
}

static char *
output_201 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[1]) == MEM)
    output_asm_insn ("ldd %1,%2", operands);
  else
    output_asm_insn ("stx %1,%3\n\tldd %3,%2", operands);
  return "fxtoq %2,%0";
}
}

static char *
output_211 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  output_asm_insn ("fstox %1,%2", operands);
  if (GET_CODE (operands[0]) == MEM)
    return "std %2,%0";
  else
    return "std %2,%3\n\tldx %3,%0";
}
}

static char *
output_212 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  output_asm_insn ("fdtox %1,%2", operands);
  if (GET_CODE (operands[0]) == MEM)
    return "std %2,%0";
  else
    return "std %2,%3\n\tldx %3,%0";
}
}

static char *
output_213 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  output_asm_insn ("fqtox %1,%2", operands);
  if (GET_CODE (operands[0]) == MEM)
    return "std %2,%0";
  else
    return "std %2,%3\n\tldx %3,%0";
}
}

static char *
output_218 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op2 = operands[2];

  /* If constant is positive, upper bits zeroed, otherwise unchanged.
     Give the assembler a chance to pick the move instruction. */
  if (GET_CODE (op2) == CONST_INT)
    {
      int sign = INTVAL (op2);
      if (sign < 0)
	return "addcc %R1,%2,%R0\n\taddx %1,-1,%0";
      return "addcc %R1,%2,%R0\n\taddx %1,0,%0";
    }
  else if (GET_CODE (op2) == CONST_DOUBLE)
    {
      rtx xoperands[4];
      xoperands[0] = operands[0];
      xoperands[1] = operands[1];
      xoperands[2] = GEN_INT (CONST_DOUBLE_LOW (op2));
      xoperands[3] = GEN_INT (CONST_DOUBLE_HIGH (op2));
      if (xoperands[2] == const0_rtx && xoperands[0] == xoperands[1])
	output_asm_insn ("add %1,%3,%0", xoperands);
      else
	output_asm_insn ("addcc %R1,%2,%R0\n\taddx %1,%3,%0", xoperands);
      return "";
    }
  return "addcc %R1,%R2,%R0\n\taddx %1,%2,%0";
}
}

static char *
output_226 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op2 = operands[2];

  /* If constant is positive, upper bits zeroed, otherwise unchanged.
     Give the assembler a chance to pick the move instruction. */
  if (GET_CODE (op2) == CONST_INT)
    {
      int sign = INTVAL (op2);
      if (sign < 0)
	return "subcc %R1,%2,%R0\n\tsubx %1,-1,%0";
      return "subcc %R1,%2,%R0\n\tsubx %1,0,%0";
    }
  else if (GET_CODE (op2) == CONST_DOUBLE)
    {
      rtx xoperands[4];
      xoperands[0] = operands[0];
      xoperands[1] = operands[1];
      xoperands[2] = GEN_INT (CONST_DOUBLE_LOW (op2));
      xoperands[3] = GEN_INT (CONST_DOUBLE_HIGH (op2));
      if (xoperands[2] == const0_rtx && xoperands[0] == xoperands[1])
	output_asm_insn ("sub %1,%3,%0", xoperands);
      else
	output_asm_insn ("subcc %R1,%2,%R0\n\tsubx %1,%3,%0", xoperands);
      return "";
    }
  return "subcc %R1,%R2,%R0\n\tsubx %1,%2,%0";
}
}

static char *
output_255 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op2 = operands[2];

  /* If constant is positive, upper bits zeroed, otherwise unchanged.
     Give the assembler a chance to pick the move instruction. */
  if (GET_CODE (op2) == CONST_INT)
    {
      int sign = INTVAL (op2);
      if (sign < 0)
	return "mov %1,%0\n\tand %R1,%2,%R0";
      return "mov 0,%0\n\tand %R1,%2,%R0";
    }
  else if (GET_CODE (op2) == CONST_DOUBLE)
    {
      rtx xoperands[4];
      xoperands[0] = operands[0];
      xoperands[1] = operands[1];
      xoperands[2] = GEN_INT (CONST_DOUBLE_LOW (op2));
      xoperands[3] = GEN_INT (CONST_DOUBLE_HIGH (op2));
      /* We could optimize then operands[1] == operands[0]
	 and either half of the constant is -1.  */
      output_asm_insn ("and %R1,%2,%R0\n\tand %1,%3,%0", xoperands);
      return "";
    }
  return "and %1,%2,%0\n\tand %R1,%R2,%R0";
}
}

static char *
output_263 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op2 = operands[2];

  /* If constant is positive, upper bits zeroed, otherwise unchanged.
     Give the assembler a chance to pick the move instruction. */
  if (GET_CODE (op2) == CONST_INT)
    {
      int sign = INTVAL (op2);
      if (sign < 0)
	return "mov -1,%0\n\tor %R1,%2,%R0";
      return "mov %1,%0\n\tor %R1,%2,%R0";
    }
  else if (GET_CODE (op2) == CONST_DOUBLE)
    {
      rtx xoperands[4];
      xoperands[0] = operands[0];
      xoperands[1] = operands[1];
      xoperands[2] = GEN_INT (CONST_DOUBLE_LOW (op2));
      xoperands[3] = GEN_INT (CONST_DOUBLE_HIGH (op2));
      /* We could optimize then operands[1] == operands[0]
	 and either half of the constant is 0.  */
      output_asm_insn ("or %R1,%2,%R0\n\tor %1,%3,%0", xoperands);
      return "";
    }
  return "or %1,%2,%0\n\tor %R1,%R2,%R0";
}
}

static char *
output_271 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  rtx op2 = operands[2];

  /* If constant is positive, upper bits zeroed, otherwise unchanged.
     Give the assembler a chance to pick the move instruction. */
  if (GET_CODE (op2) == CONST_INT)
    {
      int sign = INTVAL (op2);
      if (sign < 0)
	return "xor %1,-1,%0\n\txor %R1,%2,%R0";
      return "mov %1,%0\n\txor %R1,%2,%R0";
    }
  else if (GET_CODE (op2) == CONST_DOUBLE)
    {
      rtx xoperands[4];
      xoperands[0] = operands[0];
      xoperands[1] = operands[1];
      xoperands[2] = GEN_INT (CONST_DOUBLE_LOW (op2));
      xoperands[3] = GEN_INT (CONST_DOUBLE_HIGH (op2));
      /* We could optimize then operands[1] == operands[0]
	 and either half of the constant is 0.  */
      output_asm_insn ("xor %R1,%2,%R0\n\txor %1,%3,%0", xoperands);
      return "";
    }
  return "xor %1,%2,%0\n\txor %R1,%R2,%R0";
}
}

static char *
output_321 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (TARGET_V9)
    return "fnegd %1,%0"; /* Can't use fnegs, won't work with upper regs.  */
  else if (which_alternative == 0)
   return "fnegs %0,%0";
  else
   return "fnegs %1,%0\n\tfmovs %R1,%R0\n\tfmovs %S1,%S0\n\tfmovs %T1,%T0";
}
}

static char *
output_322 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (TARGET_V9)
    return "fnegd %1,%0";
  else if (which_alternative == 0)
   return "fnegs %0,%0";
  else
   return "fnegs %1,%0\n\tfmovs %R1,%R0";
}
}

static char *
output_324 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (TARGET_V9)
    return "fabsd %1,%0"; /* Can't use fabss, won't work with upper regs.  */
  else if (which_alternative == 0)
    return "fabss %0,%0";
  else
    return "fabss %1,%0\n\tfmovs %R1,%R0\n\tfmovs %S1,%S0\n\tfmovs %T1,%T0";
}
}

static char *
output_325 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (TARGET_V9)
    return "fabsd %1,%0";
  else if (which_alternative == 0)
    return "fabss %0,%0";
  else
    return "fabss %1,%0\n\tfmovs %R1,%R0";
}
}

static char *
output_330 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 31)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x1f);

  return "sll %1,%2,%0";
}
}

static char *
output_331 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 63)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x3f);

  return "sllx %1,%2,%0";
}
}

static char *
output_334 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 31)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x1f);

  return "sra %1,%2,%0";
}
}

static char *
output_335 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 63)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x3f);

  return "srax %1,%2,%0";
}
}

static char *
output_336 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 31)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x1f);

  return "srl %1,%2,%0";
}
}

static char *
output_337 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (GET_CODE (operands[2]) == CONST_INT
      && (unsigned) INTVAL (operands[2]) > 63)
    operands[2] = GEN_INT (INTVAL (operands[2]) & 0x3f);

  return "srlx %1,%2,%0";
}
}

static char *
output_365 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return output_return (operands);
}

static char *
output_371 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return TARGET_V9 ? "flushw" : "ta 3";
}

static char *
output_373 (operands, insn)
     rtx *operands;
     rtx insn;
{
 return TARGET_V9 ? "flush %f0" : "iflush %f0";
}

static char *
output_404 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  /* Go by way of output_move_double in case the register in operand 2
     is not properly aligned for ldd.  */
  operands[1] = gen_rtx (MEM, DFmode,
			 gen_rtx (LO_SUM, SImode, operands[0], operands[1]));
  operands[0] = operands[2];
  return output_move_double (operands);
}
}

static char *
output_406 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (! TARGET_V9 && current_function_returns_struct)
    return "jmp %%i7+12\n\trestore %%g0,%1,%Y0";
  else
    return "ret\n\trestore %%g0,%1,%Y0";
}
}

static char *
output_407 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (! TARGET_V9 && current_function_returns_struct)
    return "jmp %%i7+12\n\trestore %%g0,%1,%Y0";
  else
    return "ret\n\trestore %%g0,%1,%Y0";
}
}

static char *
output_408 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (! TARGET_V9 && current_function_returns_struct)
    return "jmp %%i7+12\n\trestore %%g0,%1,%Y0";
  else
    return "ret\n\trestore %%g0,%1,%Y0";
}
}

static char *
output_409 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (! TARGET_V9 && current_function_returns_struct)
    return "jmp %%i7+12\n\trestore %%g0,%1,%Y0";
  else
    return "ret\n\trestore %%g0,%1,%Y0";
}
}

static char *
output_410 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  if (! TARGET_V9 && current_function_returns_struct)
    return "jmp %%i7+12\n\trestore %r1,%2,%Y0";
  else
    return "ret\n\trestore %r1,%2,%Y0";
}
}

static char *
output_415 (operands, insn)
     rtx *operands;
     rtx insn;
{

{
  return "call %a0,%1\n\tadd %%o7,(%l2-.-4),%%o7";
}
}

char * const insn_template[] =
  {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "cmp %r0,%1",
    "fcmpes %0,%1",
    "fcmped %0,%1",
    "fcmpeq %0,%1",
    "fcmps %0,%1",
    "fcmpd %0,%1",
    "fcmpq %0,%1",
    "cmp %r0,%1",
    "fcmpes %0,%1,%2",
    "fcmped %0,%1,%2",
    "fcmpeq %0,%1,%2",
    "fcmps %0,%1,%2",
    "fcmpd %0,%1,%2",
    "fcmpq %0,%1,%2",
    "subcc %%g0,%1,%%g0\n\taddx %%g0,0,%0",
    "subcc %%g0,%1,%%g0\n\tsubx %%g0,0,%0",
    "mov 0,%0\n\tmovrnz %1,1,%0",
    "mov 0,%0\n\tmovrnz %1,-1,%0",
    "subcc %%g0,%1,%%g0\n\tsubx %%g0,-1,%0",
    "subcc %%g0,%1,%%g0\n\taddx %%g0,-1,%0",
    "mov 0,%0\n\tmovrz %1,1,%0",
    "mov 0,%0\n\tmovrz %1,-1,%0",
    "subcc %%g0,%1,%%g0\n\taddx %2,0,%0",
    "subcc %%g0,%1,%%g0\n\tsubx %2,0,%0",
    "subcc %%g0,%1,%%g0\n\tsubx %2,-1,%0",
    "subcc %%g0,%1,%%g0\n\taddx %2,-1,%0",
    "addx %%g0,0,%0",
    "subx %%g0,0,%0",
    "subx %%g0,%1,%0",
    "subx %%g0,%1,%0",
    "subx %%g0,-1,%0",
    "addx %%g0,-1,%0",
    "addx %%g0,%1,%0",
    "addx %1,%2,%0",
    "subx %1,0,%0",
    "subx %1,%2,%0",
    "subx %1,%2,%0",
    "subx %1,-1,%0",
    "addx %1,-1,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "sethi %%hi(%a1),%0",
    "sethi %%hi(%a1),%0",
    "sethi %%hi(%a1),%0",
    "\n1:\n\tcall 2f\n\tsethi %%hi(%l1-1b),%0\n2:\tor %0,%%lo(%l1-1b),%0\n\tadd %0,%%o7,%0",
    "\n1:\n\tcall 2f\n\tsethi %%hi(%l1-1b),%0\n2:\tor %0,%%lo(%l1-1b),%0\n\tadd %0,%%o7,%0",
    0,
    0,
    0,
    0,
    "sethi %%hi(%a1),%0",
    "sethi %%hi(%a1),%0; add %0,%%g4,%0",
    "sethi %%hi(%a1),%0",
    "sethi %%uhi(%a1),%%g1; or %%g1,%%ulo(%a1),%%g1; sllx %%g1,32,%%g1; sethi %%hi(%a1),%0; or %0,%%g1,%0",
    0,
    0,
    "or %1,%%lo(%a2),%0",
    "sethi %%hi(%a0),%2\n\tstb %r1,[%2+%%lo(%a0)]",
    0,
    0,
    "or %1,%%lo(%a2),%0",
    "sethi %%hi(%a0),%2\n\tsth %r1,[%2+%%lo(%a0)]",
    0,
    0,
    "sethi %%hi(%a0),%2\n\tst %r1,[%2+%%lo(%a0)]",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "sethi %%hi(%a0),%2\n\tst %r1,[%2+%%lo(%a0)]",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "mov%C1 %%icc,%2,%0",
    "mov%C1 %%icc,%2,%0",
    "mov%C1 %%xcc,%2,%0",
    "mov%C1 %%xcc,%2,%0",
    "mov%C1 %2,%3,%0",
    "mov%C1 %2,%3,%0",
    "mov%C1 %2,%3,%0",
    "mov%C1 %2,%3,%0",
    "movr%D1 %2,%r3,%0",
    "movr%D1 %2,%r3,%0",
    "fmovrs%D1 %2,%r3,%0",
    "fmovrd%D1 %2,%r3,%0",
    "fmovrq%D1 %2,%r3,%0",
    "fmovs%C1 %2,%3,%0",
    "fmovs%C1 %2,%3,%0",
    "fmovd%C1 %2,%3,%0",
    "fmovd%C1 %2,%3,%0",
    "fmovq%C1 %2,%3,%0",
    "fmovq%C1 %2,%3,%0",
    "fmovs%C1 %%icc,%2,%0",
    "fmovd%C1 %%icc,%2,%0",
    "fmovq%C1 %%icc,%2,%0",
    "fmovs%C1 %%xcc,%2,%0",
    "fmovd%C1 %%xcc,%2,%0",
    "fmovq%C1 %%xcc,%2,%0",
    0,
    "lduh %1,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "lduh %1,%0",
    0,
    0,
    "andcc %0,0xff,%%g0",
    "andcc %1,0xff,%0",
    "andcc %0,0xff,%%g0",
    "andcc %1,0xff,%0",
    0,
    "ldsh %1,%0",
    0,
    "ldsb %1,%0",
    0,
    "ldsb %1,%0",
    0,
    "ldsb %1,%0",
    0,
    "ldsh %1,%0",
    0,
    0,
    0,
    0,
    "fstod %1,%0",
    "fstoq %1,%0",
    "fdtoq %1,%0",
    "fdtos %1,%0",
    "fqtos %1,%0",
    "fqtod %1,%0",
    "fitos %1,%0",
    "fitod %1,%0",
    "fitoq %1,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    "fxtos %1,%0",
    "fxtod %1,%0",
    "fxtoq %1,%0",
    "fstoi %1,%0",
    "fdtoi %1,%0",
    "fqtoi %1,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    "fstox %1,%0",
    "fdtox %1,%0",
    "fqtox %1,%0",
    0,
    0,
    "add %1,%2,%0",
    "add %1,%2,%0",
    "addcc %0,%1,%%g0",
    "addcc %0,%1,%%g0",
    "addcc %1,%2,%0",
    "addcc %1,%2,%0",
    0,
    0,
    "sub %1,%2,%0",
    "sub %1,%2,%0",
    "subcc %0,%1,%%g0",
    "subcc %0,%1,%%g0",
    "subcc %1,%2,%0",
    "subcc %1,%2,%0",
    "smul %1,%2,%0",
    "mulx %1,%2,%0",
    "smulcc %1,%2,%0",
    0,
    "smul %1,%2,%R0\n\trd %%y,%0",
    "smul %1,%2,%R0\n\trd %%y,%0",
    0,
    "smul %1,%2,%%g0\n\trd %%y,%0",
    "smul %1,%2,%%g0\n\trd %%y,%0",
    0,
    "umul %1,%2,%R0\n\trd %%y,%0",
    "umul %1,%2,%R0\n\trd %%y,%0",
    0,
    "umul %1,%2,%%g0\n\trd %%y,%0",
    "umul %1,%2,%%g0\n\trd %%y,%0",
    "sra %1,31,%3\n\twr %%g0,%3,%%y\n\tnop\n\tnop\n\tnop\n\tsdiv %1,%2,%0",
    "sdivx %1,%2,%0",
    "sra %1,31,%3\n\twr %%g0,%3,%%y\n\tnop\n\tnop\n\tnop\n\tsdivcc %1,%2,%0",
    "wr %%g0,%%g0,%%y\n\tnop\n\tnop\n\tnop\n\tudiv %1,%2,%0",
    "udivx %1,%2,%0",
    "wr %%g0,%%g0,%%y\n\tnop\n\tnop\n\tnop\n\tudivcc %1,%2,%0",
    0,
    0,
    "and %1,%2,%0",
    "and %1,%2,%0",
    0,
    "andn %2,%1,%0\n\tandn %R2,%R1,%R0",
    "andn %2,%1,%0",
    "andn %2,%1,%0",
    0,
    0,
    "or %1,%2,%0",
    "or %1,%2,%0",
    0,
    "orn %2,%1,%0\n\torn %R2,%R1,%R0",
    "orn %2,%1,%0",
    "orn %2,%1,%0",
    0,
    0,
    "xor %r1,%2,%0",
    "xor %r1,%2,%0",
    0,
    0,
    "xnor %1,%2,%0\n\txnor %R1,%R2,%R0",
    "xnor %r1,%2,%0",
    "xnor %r1,%2,%0",
    "%A2cc %0,%1,%%g0",
    "%A2cc %0,%1,%%g0",
    "%A3cc %1,%2,%0",
    "%A3cc %1,%2,%0",
    "xnorcc %r0,%1,%%g0",
    "xnorcc %r0,%1,%%g0",
    "xnorcc %r1,%2,%0",
    "xnorcc %r1,%2,%0",
    "%B2cc %r1,%0,%%g0",
    "%B2cc %r1,%0,%%g0",
    "%B3cc %r2,%1,%0",
    "%B3cc %r2,%1,%0",
    0,
    "subcc %%g0,%R1,%R0\n\tsubx %%g0,%1,%0",
    "sub %%g0,%1,%0",
    "sub %%g0,%1,%0",
    "subcc %%g0,%0,%%g0",
    "subcc %%g0,%0,%%g0",
    "subcc %%g0,%1,%0",
    "subcc %%g0,%1,%0",
    0,
    "xnor %%g0,%1,%0\n\txnor %%g0,%R1,%R0",
    "xnor %%g0,%1,%0",
    "xnor %%g0,%1,%0",
    "xnorcc %%g0,%0,%%g0",
    "xnorcc %%g0,%0,%%g0",
    "xnorcc %%g0,%1,%0",
    "xnorcc %%g0,%1,%0",
    "faddq %1,%2,%0",
    "faddd %1,%2,%0",
    "fadds %1,%2,%0",
    "fsubq %1,%2,%0",
    "fsubd %1,%2,%0",
    "fsubs %1,%2,%0",
    "fmulq %1,%2,%0",
    "fmuld %1,%2,%0",
    "fmuls %1,%2,%0",
    "fsmuld %1,%2,%0",
    "fdmulq %1,%2,%0",
    "fdivq %1,%2,%0",
    "fdivd %1,%2,%0",
    "fdivs %1,%2,%0",
    0,
    0,
    "fnegs %1,%0",
    0,
    0,
    "fabss %1,%0",
    "fsqrtq %1,%0",
    "fsqrtd %1,%0",
    "fsqrts %1,%0",
    0,
    0,
    "addcc %0,%0,%%g0",
    "addcc %1,%1,%0",
    0,
    0,
    0,
    0,
    "b%* %l0%(",
    0,
    "jmp %%o7+%0%#",
    "jmp %%o7+%0%#",
    "jmp %a0%#",
    "jmp %a0%#",
    "call %l0%#",
    "call %l0%#",
    0,
    0,
    "call %a0,%1%#",
    "call %a0,%1%#",
    "call %a0,%1%#",
    "call %a0,%1%#",
    "call %a0,%1\n\tnop\n\tunimp %2",
    "call %a0,%1\n\tnop\n\tunimp %2",
    "call %a0,%1\n\tnop\n\tnop",
    "call %a0,%1\n\tnop\n\tnop",
    0,
    "call %a1,%2%#",
    "call %a1,%2%#",
    "call %a1,%2%#",
    "call %a1,%2%#",
    0,
    "",
    0,
    "cmp %1,0\n\tbe,a .+8\n\tadd %0,4,%0",
    0,
    "nop",
    0,
    "jmp %a0%#",
    "jmp %a0%#",
    0,
    0,
    "jmp %%o0+0\n\trestore",
    0,
    "sub %%g0,%1,%0\n\tand %0,%1,%0\n\tscan %0,0,%0\n\tmov 32,%2\n\tsub %2,%0,%0\n\tsra %0,31,%2\n\tand %2,31,%2\n\tadd %2,%0,%0",
    "neg %1,%2\n\tnot %2,%2\n\txor %1,%2,%2\n\tpopc %2,%0\n\tmovrz %1,%%g0,%0",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "ldd %1,%0",
    "std %1,%0",
    "ldd %1,%0",
    "std %1,%0",
    "ldd %3,%2",
    "std %3,%2",
    "ldd %3,%2",
    "std %3,%2",
    "orcc %1,%%g0,%0",
    "orcc %1,%%g0,%0",
    "ldsh %1,%0\n\torcc %0,%%g0,%2",
    "ldsh %1,%0\n\torcc %0,%%g0,%2",
    "ldsb %1,%0\n\torcc %0,%%g0,%2",
    "ldsb %1,%0\n\torcc %0,%%g0,%2",
    0,
    "ld [%0+%%lo(%a1)],%2",
    0,
    0,
    0,
    0,
    0,
    "ret\n\trestore %%g0,%1,%Y0",
    "ret\n\trestore %r1,%2,%Y0",
    "ret\n\tfmovs %0,%%f0",
    "call %a1,%2\n\tadd %%o7,(%l3-.-4),%%o7",
    0,
    "call %a1,%2\n\tadd %%o7,(%l3-.-4),%%o7",
    "call %a0,%1\n\tadd %%o7,(%l2-.-4),%%o7",
    "subxcc %r1,0,%0",
  };

char *(*const insn_outfun[])() =
  {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_62,
    output_63,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_74,
    output_75,
    output_76,
    output_77,
    output_78,
    output_79,
    output_80,
    output_81,
    output_82,
    output_83,
    0,
    0,
    0,
    0,
    0,
    output_89,
    output_90,
    output_91,
    output_92,
    0,
    0,
    0,
    0,
    0,
    output_98,
    0,
    0,
    0,
    output_102,
    0,
    0,
    0,
    output_106,
    0,
    0,
    output_109,
    output_110,
    output_111,
    0,
    output_113,
    output_114,
    0,
    output_116,
    0,
    output_118,
    output_119,
    0,
    output_121,
    output_122,
    0,
    output_124,
    output_125,
    output_126,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_160,
    0,
    output_162,
    0,
    output_164,
    0,
    0,
    0,
    output_168,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_184,
    output_185,
    output_186,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_199,
    output_200,
    output_201,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_211,
    output_212,
    output_213,
    0,
    0,
    0,
    0,
    output_218,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_226,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_255,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_263,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_271,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_321,
    output_322,
    0,
    output_324,
    output_325,
    0,
    0,
    0,
    0,
    output_330,
    output_331,
    0,
    0,
    output_334,
    output_335,
    output_336,
    output_337,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_365,
    0,
    0,
    0,
    0,
    0,
    output_371,
    0,
    output_373,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    output_404,
    0,
    output_406,
    output_407,
    output_408,
    output_409,
    output_410,
    0,
    0,
    0,
    0,
    output_415,
    0,
    0,
    0,
  };

rtx (*const insn_gen_function[]) () =
  {
    gen_cmpsi,
    gen_cmpdi,
    gen_cmpsf,
    gen_cmpdf,
    gen_cmptf,
    gen_seqsi_special,
    gen_seqdi_special,
    gen_snesi_special,
    gen_snedi_special,
    gen_seqdi_special_trunc,
    gen_snedi_special_trunc,
    gen_seqsi_special_extend,
    gen_snesi_special_extend,
    gen_seq,
    gen_sne,
    gen_sgt,
    gen_slt,
    gen_sge,
    gen_sle,
    gen_sgtu,
    gen_sltu,
    gen_sgeu,
    gen_sleu,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_beq,
    gen_bne,
    gen_bgt,
    gen_bgtu,
    gen_blt,
    gen_bltu,
    gen_bge,
    gen_bgeu,
    gen_ble,
    gen_bleu,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_movqi,
    0,
    0,
    0,
    gen_movhi,
    0,
    0,
    0,
    gen_movsi,
    0,
    0,
    gen_movdi,
    0,
    0,
    0,
    gen_movsf,
    0,
    0,
    0,
    0,
    gen_movdf,
    0,
    0,
    0,
    0,
    0,
    gen_movtf,
    0,
    0,
    0,
    gen_movsicc,
    gen_movdicc,
    gen_movsfcc,
    gen_movdfcc,
    gen_movtfcc,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_zero_extendhisi2,
    0,
    gen_zero_extendqihi2,
    0,
    gen_zero_extendqisi2,
    0,
    gen_zero_extendqidi2,
    0,
    gen_zero_extendhidi2,
    0,
    gen_zero_extendsidi2,
    0,
    0,
    0,
    0,
    0,
    gen_extendhisi2,
    0,
    gen_extendqihi2,
    0,
    gen_extendqisi2,
    0,
    gen_extendqidi2,
    0,
    gen_extendhidi2,
    0,
    gen_extendsidi2,
    0,
    0,
    0,
    gen_extendsfdf2,
    gen_extendsftf2,
    gen_extenddftf2,
    gen_truncdfsf2,
    gen_trunctfsf2,
    gen_trunctfdf2,
    gen_floatsisf2,
    gen_floatsidf2,
    gen_floatsitf2,
    gen_floatdisf2,
    gen_floatdidf2,
    gen_floatditf2,
    0,
    0,
    0,
    gen_floatdisf2_sp64,
    gen_floatdidf2_sp64,
    gen_floatditf2_sp64,
    gen_fix_truncsfsi2,
    gen_fix_truncdfsi2,
    gen_fix_trunctfsi2,
    gen_fix_truncsfdi2,
    gen_fix_truncdfdi2,
    gen_fix_trunctfdi2,
    0,
    0,
    0,
    gen_fix_truncsfdi2_sp64,
    gen_fix_truncdfdi2_sp64,
    gen_fix_trunctfdi2_sp64,
    gen_adddi3,
    0,
    0,
    gen_addsi3,
    0,
    0,
    0,
    0,
    gen_subdi3,
    0,
    0,
    gen_subsi3,
    0,
    0,
    0,
    0,
    gen_mulsi3,
    gen_muldi3,
    0,
    gen_mulsidi3,
    0,
    gen_const_mulsidi3,
    gen_smulsi3_highpart,
    0,
    gen_const_smulsi3_highpart,
    gen_umulsidi3,
    0,
    gen_const_umulsidi3,
    gen_umulsi3_highpart,
    0,
    gen_const_umulsi3_highpart,
    gen_divsi3,
    gen_divdi3,
    0,
    gen_udivsi3,
    gen_udivdi3,
    0,
    gen_anddi3,
    0,
    0,
    gen_andsi3,
    0,
    0,
    0,
    0,
    gen_iordi3,
    0,
    0,
    gen_iorsi3,
    0,
    0,
    0,
    0,
    gen_xordi3,
    0,
    0,
    gen_xorsi3,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_negdi2,
    0,
    0,
    gen_negsi2,
    0,
    0,
    0,
    0,
    gen_one_cmpldi2,
    0,
    0,
    gen_one_cmplsi2,
    0,
    0,
    0,
    0,
    gen_addtf3,
    gen_adddf3,
    gen_addsf3,
    gen_subtf3,
    gen_subdf3,
    gen_subsf3,
    gen_multf3,
    gen_muldf3,
    gen_mulsf3,
    0,
    0,
    gen_divtf3,
    gen_divdf3,
    gen_divsf3,
    gen_negtf2,
    gen_negdf2,
    gen_negsf2,
    gen_abstf2,
    gen_absdf2,
    gen_abssf2,
    gen_sqrttf2,
    gen_sqrtdf2,
    gen_sqrtsf2,
    gen_ashlsi3,
    gen_ashldi3,
    0,
    0,
    gen_ashrsi3,
    gen_ashrdi3,
    gen_lshrsi3,
    gen_lshrdi3,
    gen_jump,
    gen_tablejump,
    gen_pic_tablejump_32,
    gen_pic_tablejump_64,
    0,
    0,
    0,
    0,
    gen_casesi,
    gen_call,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    gen_call_value,
    0,
    0,
    0,
    0,
    gen_untyped_call,
    gen_blockage,
    gen_untyped_return,
    gen_update_return,
    gen_return,
    gen_nop,
    gen_indirect_jump,
    0,
    0,
    gen_nonlocal_goto,
    gen_flush_register_windows,
    gen_goto_handler_and_restore,
    gen_flush,
    gen_ffssi2,
    gen_ffsdi2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  };

char *insn_name[] =
  {
    "cmpsi",
    "cmpdi",
    "cmpsf",
    "cmpdf",
    "cmptf",
    "seqsi_special",
    "seqdi_special",
    "snesi_special",
    "snedi_special",
    "seqdi_special_trunc",
    "snedi_special_trunc",
    "seqsi_special_extend",
    "snesi_special_extend",
    "seq",
    "sne",
    "sgt",
    "slt",
    "sge",
    "sle",
    "sgtu",
    "sltu",
    "sgeu",
    "sleu",
    "*cmpsi_insn",
    "*cmpsf_fpe_sp32",
    "*cmpdf_fpe_sp32",
    "*cmptf_fpe_sp32",
    "*cmpsf_fp_sp32",
    "*cmpdf_fp_sp32",
    "*cmptf_fp_sp32",
    "*cmpdi_sp64",
    "*cmpsf_fpe_sp64",
    "*cmpdf_fpe_sp64",
    "*cmptf_fpe_sp64",
    "*cmpsf_fp_sp64",
    "*cmpdf_fp_sp64",
    "*cmptf_fp_sp64",
    "*snesi_zero",
    "*neg_snesi_zero",
    "*snedi_zero",
    "*neg_snedi_zero",
    "*seqsi_zero",
    "*neg_seqsi_zero",
    "*seqdi_zero",
    "*neg_seqdi_zero",
    "*x_plus_i_ne_0",
    "*x_minus_i_ne_0",
    "*x_plus_i_eq_0",
    "*x_minus_i_eq_0",
    "*sltu_insn",
    "*neg_sltu_insn",
    "*neg_sltu_minus_x",
    "*neg_sltu_plus_x",
    "*sgeu_insn",
    "*neg_sgeu_insn",
    "*sltu_plus_x",
    "*sltu_plus_x_plus_y",
    "*x_minus_sltu",
    "*x_minus_y_minus_sltu",
    "*x_minus_sltu_plus_y",
    "*sgeu_plus_x",
    "*x_minus_sgeu",
    "*scc_si",
    "*scc_di",
    "beq",
    "bne",
    "bgt",
    "bgtu",
    "blt",
    "bltu",
    "bge",
    "bgeu",
    "ble",
    "bleu",
    "*normal_branch",
    "*inverted_branch",
    "*normal_fp_branch_sp64",
    "*inverted_fp_branch_sp64",
    "*normal_fpe_branch_sp64",
    "*inverted_fpe_branch_sp64",
    "*normal_int_branch_sp64",
    "*inverted_int_branch_sp64",
    "*lo_sum_si",
    "*pic_lo_sum_si",
    "*pic_sethi_si",
    "*sethi_si",
    "*sethi_hi",
    "*move_pic_label_si",
    "*move_pic_label_di",
    "*lo_sum_di_sp32",
    "*lo_sum_di_sp64",
    "*sethi_di_sp32",
    "*sethi_di_sp64",
    "*sethi_di_medlow",
    "*sethi_di_medany_data",
    "*sethi_di_medany_text",
    "*sethi_di_fullany",
    "movqi",
    "*movqi_insn",
    "*lo_sum_qi",
    "*store_qi",
    "movhi",
    "*movhi_insn",
    "*lo_sum_hi",
    "*store_hi",
    "movsi",
    "*movsi_insn",
    "*store_si",
    "movdi",
    "*movdi_sp32_insn",
    "*movdi_sp64_insn",
    "*movsf_const_insn",
    "movsf",
    "*movsf_insn",
    "*movsf_no_f_insn",
    "*store_sf",
    "*movdf_const_insn",
    "movdf",
    "*movdf_insn",
    "*movdf_no_e_insn",
    "*movdf_no_e_insn+1",
    "*store_df",
    "*movtf_const_insn",
    "movtf",
    "*movtf_insn",
    "*movtf_no_e_insn",
    "*store_tf",
    "movsicc",
    "movdicc",
    "movsfcc",
    "movdfcc",
    "movtfcc",
    "*movsi_cc_sp64",
    "*movdi_cc_sp64",
    "*movsi_ccx_sp64",
    "*movdi_ccx_sp64",
    "*movsi_ccfp_sp64",
    "*movsi_ccfpe_sp64",
    "*movdi_ccfp_sp64",
    "*movdi_ccfpe_sp64",
    "*movsi_cc_reg_sp64",
    "*movdi_cc_reg_sp64",
    "*movsf_cc_reg_sp64",
    "*movdf_cc_reg_sp64",
    "*movtf_cc_reg_sp64",
    "*movsf_ccfp_sp64",
    "*movsf_ccfpe_sp64",
    "*movdf_ccfp_sp64",
    "*movdf_ccfpe_sp64",
    "*movtf_ccfp_sp64",
    "*movtf_ccfpe_sp64",
    "*movsf_cc_sp64",
    "*movdf_cc_sp64",
    "*movtf_cc_sp64",
    "*movsf_ccx_sp64",
    "*movdf_ccx_sp64",
    "*movtf_ccx_sp64",
    "zero_extendhisi2",
    "*zero_extendhisi2_insn",
    "zero_extendqihi2",
    "*zero_extendqihi2_insn",
    "zero_extendqisi2",
    "*zero_extendqisi2_insn",
    "zero_extendqidi2",
    "*zero_extendqidi2_insn",
    "zero_extendhidi2",
    "*zero_extendhidi2_insn",
    "zero_extendsidi2",
    "*zero_extendsidi2_insn",
    "*cmp_zero_extendqisi2",
    "*cmp_zero_extendqisi2_set",
    "*cmp_siqi_trunc",
    "*cmp_siqi_trunc_set",
    "extendhisi2",
    "*sign_extendhisi2_insn",
    "extendqihi2",
    "*sign_extendqihi2_insn",
    "extendqisi2",
    "*sign_extendqisi2_insn",
    "extendqidi2",
    "*sign_extendqidi2_insn",
    "extendhidi2",
    "*sign_extendhidi2_insn",
    "extendsidi2",
    "*sign_extendsidi2_insn",
    "*cmp_zero_extract",
    "*cmp_zero_extract_sp64",
    "extendsfdf2",
    "extendsftf2",
    "extenddftf2",
    "truncdfsf2",
    "trunctfsf2",
    "trunctfdf2",
    "floatsisf2",
    "floatsidf2",
    "floatsitf2",
    "floatdisf2",
    "floatdidf2",
    "floatditf2",
    "*floatdisf2_insn",
    "*floatdidf2_insn",
    "*floatditf2_insn",
    "floatdisf2_sp64",
    "floatdidf2_sp64",
    "floatditf2_sp64",
    "fix_truncsfsi2",
    "fix_truncdfsi2",
    "fix_trunctfsi2",
    "fix_truncsfdi2",
    "fix_truncdfdi2",
    "fix_trunctfdi2",
    "*fix_truncsfdi2_insn",
    "*fix_truncdfdi2_insn",
    "*fix_trunctfdi2_insn",
    "fix_truncsfdi2_sp64",
    "fix_truncdfdi2_sp64",
    "fix_trunctfdi2_sp64",
    "adddi3",
    "*adddi3_sp32",
    "*adddi3_sp64",
    "addsi3",
    "*cmp_cc_plus",
    "*cmp_ccx_plus",
    "*cmp_cc_plus_set",
    "*cmp_ccx_plus_set",
    "subdi3",
    "*subdi3_sp32",
    "*subdi3_sp64",
    "subsi3",
    "*cmp_minus_cc",
    "*cmp_minus_ccx",
    "*cmp_minus_cc_set",
    "*cmp_minus_ccx_set",
    "mulsi3",
    "muldi3",
    "*cmp_mul_set",
    "mulsidi3",
    "*mulsidi3_sp32",
    "const_mulsidi3",
    "smulsi3_highpart",
    "*smulsidi3_highpart_sp32",
    "const_smulsi3_highpart",
    "umulsidi3",
    "*umulsidi3_sp32",
    "const_umulsidi3",
    "umulsi3_highpart",
    "*umulsidi3_highpart_sp32",
    "const_umulsi3_highpart",
    "divsi3",
    "divdi3",
    "*cmp_sdiv_cc_set",
    "udivsi3",
    "udivdi3",
    "*cmp_udiv_cc_set",
    "anddi3",
    "*anddi3_sp32",
    "*anddi3_sp64",
    "andsi3",
    "andsi3+1",
    "*and_not_di_sp32",
    "*and_not_di_sp64",
    "*and_not_si",
    "iordi3",
    "*iordi3_sp32",
    "*iordi3_sp64",
    "iorsi3",
    "iorsi3+1",
    "*or_not_di_sp32",
    "*or_not_di_sp64",
    "*or_not_si",
    "xordi3",
    "*xorsi3_sp32",
    "*xordi3_sp64",
    "xorsi3",
    "xorsi3+1",
    "*xor_not_di_sp32-1",
    "*xor_not_di_sp32",
    "*xor_not_di_sp64",
    "*xor_not_si",
    "*cmp_cc_arith_op",
    "*cmp_ccx_arith_op",
    "*cmp_cc_arith_op_set",
    "*cmp_ccx_arith_op_set",
    "*cmp_cc_xor_not",
    "*cmp_ccx_xor_not",
    "*cmp_cc_xor_not_set",
    "*cmp_ccx_xor_not_set",
    "*cmp_cc_arith_op_not",
    "*cmp_ccx_arith_op_not",
    "*cmp_cc_arith_op_not_set",
    "*cmp_ccx_arith_op_not_set",
    "negdi2",
    "*negdi2_sp32",
    "*negdi2_sp64",
    "negsi2",
    "*cmp_cc_neg",
    "*cmp_ccx_neg",
    "*cmp_cc_set_neg",
    "*cmp_ccx_set_neg",
    "one_cmpldi2",
    "*one_cmpldi2_sp32",
    "*one_cmpldi2_sp64",
    "one_cmplsi2",
    "*cmp_cc_not",
    "*cmp_ccx_not",
    "*cmp_cc_set_not",
    "*cmp_ccx_set_not",
    "addtf3",
    "adddf3",
    "addsf3",
    "subtf3",
    "subdf3",
    "subsf3",
    "multf3",
    "muldf3",
    "mulsf3",
    "*muldf3_extend",
    "*multf3_extend",
    "divtf3",
    "divdf3",
    "divsf3",
    "negtf2",
    "negdf2",
    "negsf2",
    "abstf2",
    "absdf2",
    "abssf2",
    "sqrttf2",
    "sqrtdf2",
    "sqrtsf2",
    "ashlsi3",
    "ashldi3",
    "*cmp_cc_ashift_1",
    "*cmp_cc_set_ashift_1",
    "ashrsi3",
    "ashrdi3",
    "lshrsi3",
    "lshrdi3",
    "jump",
    "tablejump",
    "pic_tablejump_32",
    "pic_tablejump_64",
    "*tablejump_sp32",
    "*tablejump_sp64",
    "*get_pc_sp32",
    "*get_pc_sp64",
    "casesi",
    "call",
    "*call_address_sp32",
    "*call_symbolic_sp32",
    "*call_address_sp64",
    "*call_symbolic_sp64",
    "*call_address_struct_value_sp32",
    "*call_symbolic_struct_value_sp32",
    "*call_address_untyped_struct_value_sp32",
    "*call_symbolic_untyped_struct_value_sp32",
    "call_value",
    "*call_value_address_sp32",
    "*call_value_symbolic_sp32",
    "*call_value_address_sp64",
    "*call_value_symbolic_sp64",
    "untyped_call",
    "blockage",
    "untyped_return",
    "update_return",
    "return",
    "nop",
    "indirect_jump",
    "*branch_sp32",
    "*branch_sp64",
    "nonlocal_goto",
    "flush_register_windows",
    "goto_handler_and_restore",
    "flush",
    "ffssi2",
    "ffsdi2",
    "ffsdi2+1",
    "ffsdi2+2",
    "ffsdi2+3",
    "ffsdi2+4",
    "ffsdi2+5",
    "ffsdi2+6",
    "ffsdi2+7",
    "ffsdi2+8",
    "ffsdi2+9",
    "ffsdi2+10",
    "ffsdi2+11",
    "ffsdi2+12",
    "ffsdi2+13",
    "ffsdi2+14",
    "ffsdi2+15",
    "*return_qi-15",
    "*return_qi-14",
    "*return_qi-13",
    "*return_qi-12",
    "*return_qi-11",
    "*return_qi-10",
    "*return_qi-9",
    "*return_qi-8",
    "*return_qi-7",
    "*return_qi-6",
    "*return_qi-5",
    "*return_qi-4",
    "*return_qi-3",
    "*return_qi-2",
    "*return_qi-1",
    "*return_qi",
    "*return_hi",
    "*return_si",
    "*return_sf_no_fpu",
    "*return_addsi",
    "*return_di",
    "*return_adddi",
    "*return_sf",
    "*return_sf+1",
    "*return_sf+2",
    "*return_sf+3",
    "*return_sf+4",
    "*return_sf+5",
  };
char **insn_name_ptr = insn_name;

const int insn_n_operands[] =
  {
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    1,
    1,
    2,
    2,
    1,
    1,
    2,
    3,
    2,
    3,
    3,
    2,
    2,
    2,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    2,
    2,
    3,
    3,
    2,
    2,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    2,
    2,
    2,
    2,
    2,
    3,
    2,
    2,
    2,
    2,
    3,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    5,
    4,
    4,
    4,
    4,
    4,
    4,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    1,
    2,
    1,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    4,
    4,
    4,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    4,
    4,
    4,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    4,
    3,
    4,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    4,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    4,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    4,
    4,
    3,
    3,
    3,
    3,
    3,
    4,
    4,
    2,
    2,
    3,
    3,
    3,
    3,
    4,
    4,
    2,
    2,
    2,
    2,
    1,
    1,
    2,
    2,
    2,
    2,
    2,
    2,
    1,
    1,
    2,
    2,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    1,
    2,
    3,
    3,
    3,
    3,
    0,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    3,
    4,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    5,
    3,
    3,
    3,
    3,
    3,
    0,
    2,
    2,
    0,
    0,
    1,
    1,
    1,
    4,
    0,
    0,
    1,
    3,
    3,
    3,
    3,
    2,
    3,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    3,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    4,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    2,
    2,
    2,
    2,
    3,
    2,
    3,
    1,
    3,
    2,
    3,
    2,
    2,
  };

const int insn_n_dups[] =
  {
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    0,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    0,
    0,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    2,
    0,
    0,
    2,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    0,
    0,
    2,
    2,
    0,
    0,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    15,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  };

char *const insn_operand_constraint[][MAX_RECOG_OPERANDS] =
  {
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "", },
    { "r", "rI", },
    { "f", "f", },
    { "e", "e", },
    { "e", "e", },
    { "f", "f", },
    { "e", "e", },
    { "e", "e", },
    { "r", "rHI", },
    { "=c", "f", "f", },
    { "=c", "e", "e", },
    { "=c", "e", "e", },
    { "=c", "f", "f", },
    { "=c", "e", "e", },
    { "=c", "e", "e", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "=r", },
    { "=r", },
    { "=r", "rI", },
    { "=r", "rI", },
    { "=r", },
    { "=r", },
    { "=r", "rI", },
    { "=r", "%r", "rI", },
    { "=r", "r", },
    { "=r", "r", "rI", },
    { "=r", "r", "rI", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "", },
    { "=r", "", },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { "", },
    { "", },
    { "", "c", },
    { "", "c", },
    { "", "c", },
    { "", "c", },
    { "", "r", },
    { "", "r", },
    { "=r", "r", "in", },
    { "=r", "r", "in", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "i", },
    { "=r", "i", },
    { "=r", "0", "in", },
    { "=r", "0", "in", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "", },
    { "=r", "", },
    { "", "", },
    { "=r,r,r,Q", "rI,K,Q,rJ", },
    { "=r", "r", "in", },
    { "", "rJ", "=&r", },
    { "", "", },
    { "=r,r,r,Q", "rI,K,Q,rJ", },
    { "=r", "r", "in", },
    { "", "rJ", "=&r", },
    { "", "", },
    { "=r,f,r,r,f,Q,Q", "rI,!f,K,Q,!Q,rJ,!f", },
    { "", "rJ", "=&r", },
    { "", "", },
    { "=r,T,U,Q,r,r,?f,?f,?Q", "r,U,T,r,Q,i,f,Q,f", },
    { "=r,r,r,Q,?f,?f,?Q", "rI,K,Q,rJ,f,Q,f", },
    { "=?r,f,m", "?F,m,G", },
    { "", "", },
    { "=f,r,f,r,Q,Q", "f,r,Q,Q,f,r", },
    { "=r,r,Q", "r,Q,r", },
    { "i", "rfG", "=&r", },
    { "=?r,e,o", "?F,m,G", },
    { "", "", },
    { "=T,U,e,r,Q,Q,e,r", "U,T,e,r,e,r,Q,Q", },
    { "=T,U,r,Q,&r", "U,T,r,r,Q", },
    { "", "", },
    { "i,i", "re,G", "=&r,&r", },
    { "=?r,e,o", "?F,m,G", },
    { "", "", },
    { "=e,r,Q,Q,e,&r", "e,r,e,r,Q,Q", },
    { "=r,Q,&r", "r,r,Q", },
    { "i,i", "re,G", "=&r,&r", },
    { "", "", "", "", },
    { "", "", "", "", },
    { "", "", "", "", },
    { "", "", "", "", },
    { "", "", "", "", },
    { "=r", "", "ri", "0", },
    { "=r", "", "rHI", "0", },
    { "=r", "", "ri", "0", },
    { "=r", "", "rHI", "0", },
    { "=r", "", "c", "ri", "0", },
    { "=r", "", "c", "ri", "0", },
    { "=r", "", "c", "rHI", "0", },
    { "=r", "", "c", "rHI", "0", },
    { "=r", "", "r", "ri", "0", },
    { "=r", "", "r", "ri", "0", },
    { "=f", "", "r", "f", "0", },
    { "=e", "", "r", "e", "0", },
    { "=e", "", "r", "e", "0", },
    { "=f", "", "c", "f", "0", },
    { "=f", "", "c", "f", "0", },
    { "=e", "", "c", "e", "0", },
    { "=e", "", "c", "e", "0", },
    { "=e", "", "c", "e", "0", },
    { "=e", "", "c", "e", "0", },
    { "=f", "", "f", "0", },
    { "=e", "", "e", "0", },
    { "=e", "", "e", "0", },
    { "=f", "", "f", "0", },
    { "=e", "", "e", "0", },
    { "=e", "", "e", "0", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r,r", "r,Q", },
    { "", "", },
    { "=r,r", "r,Q", },
    { "", "", },
    { "=r,r", "r,Q", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r,r", "r,Q", },
    { "r", },
    { "=r", "r", },
    { "r", },
    { "=r", "r", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r", "m", },
    { "", "", },
    { "=r,r", "r,Q", },
    { "r", "n", "n", },
    { "r", "n", "n", },
    { "=e", "f", },
    { "=e", "f", },
    { "=e", "e", },
    { "=f", "e", },
    { "=f", "e", },
    { "=e", "e", },
    { "=f", "f", },
    { "=e", "f", },
    { "=e", "f", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "=f", "rm", "=&e", "m", },
    { "=e", "rm", "=&e", "m", },
    { "=e", "rm", "=&e", "m", },
    { "=f", "e", },
    { "=e", "e", },
    { "=e", "e", },
    { "=f", "f", },
    { "=f", "e", },
    { "=f", "e", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "=rm", "f", "=&e", "m", },
    { "=rm", "e", "=&e", "m", },
    { "=rm", "e", "=&e", "m", },
    { "=e", "f", },
    { "=e", "e", },
    { "=e", "e", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rI", },
    { "%r", "rI", },
    { "%r", "rHI", },
    { "=r", "%r", "rI", },
    { "=r", "%r", "rHI", },
    { "=r", "r", "rHI", },
    { "=r", "r", "rHI", },
    { "=r", "r", "rHI", },
    { "=r", "r", "rI", },
    { "r", "rI", },
    { "r", "rHI", },
    { "=r", "r", "rI", },
    { "=r", "r", "rHI", },
    { "=r", "%r", "rI", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rI", },
    { "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "I", },
    { "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "", },
    { "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "", },
    { "=r", "r", "rI", "=&r", },
    { "=r", "r", "rHI", },
    { "=r", "r", "rI", "=&r", },
    { "=r", "r", "rI", },
    { "=r", "r", "rHI", },
    { "=r", "r", "rI", },
    { "", "", "", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rI", },
    { "", "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "", "", "", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rHI", },
    { "=r", "%r", "rI", },
    { "", "", "", "", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "", "", "", },
    { "=r", "%r", "rHI", },
    { "=r", "%rJ", "rHI", },
    { "=r", "%rJ", "rI", },
    { "", "", "", "", },
    { "", "", "", "", },
    { "=r", "r", "r", },
    { "=r", "rJ", "rHI", },
    { "=r", "rJ", "rI", },
    { "%r", "rI", "", },
    { "%r", "rHI", "", },
    { "=r", "%r", "rI", "", },
    { "=r", "%r", "rHI", "", },
    { "%rJ", "rI", },
    { "%rJ", "rHI", },
    { "=r", "%rJ", "rI", },
    { "=r", "%rJ", "rHI", },
    { "rI", "rJ", "", },
    { "rHI", "rJ", "", },
    { "=r", "rI", "rJ", "", },
    { "=r", "rHI", "rJ", "", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "r", },
    { "=r", "rI", },
    { "rI", },
    { "rHI", },
    { "=r", "rI", },
    { "=r", "rHI", },
    { "", "", },
    { "=r", "r", },
    { "=r", "rHI", },
    { "=r", "rI", },
    { "rI", },
    { "rHI", },
    { "=r", "rI", },
    { "=r", "rHI", },
    { "=e", "e", "e", },
    { "=e", "e", "e", },
    { "=f", "f", "f", },
    { "=e", "e", "e", },
    { "=e", "e", "e", },
    { "=f", "f", "f", },
    { "=e", "e", "e", },
    { "=e", "e", "e", },
    { "=f", "f", "f", },
    { "=e", "f", "f", },
    { "=e", "e", "e", },
    { "=e", "e", "e", },
    { "=e", "e", "e", },
    { "=f", "f", "f", },
    { "=e,e", "0,e", },
    { "=e,e", "0,e", },
    { "=f", "f", },
    { "=e,e", "0,e", },
    { "=e,e", "0,e", },
    { "=f", "f", },
    { "=e", "e", },
    { "=e", "e", },
    { "=f", "f", },
    { "=r", "r", "rI", },
    { "=r", "r", "rI", },
    { "r", },
    { "=r", "r", },
    { "=r", "r", "rI", },
    { "=r", "r", "rI", },
    { "=r", "r", "rI", },
    { "=r", "r", "rI", },
    { 0 },
    { "r", },
    { "r", },
    { "r", },
    { "p", },
    { "p", },
    { 0 },
    { 0 },
    { "", "", "", },
    { "", "", "", "i", },
    { "p", "", },
    { "s", "", },
    { "p", "", },
    { "s", "", },
    { "p", "", "", },
    { "s", "", "", },
    { "p", "", "", },
    { "s", "", "", },
    { "=rf", "", "", "", "", },
    { "=rf", "p", "", },
    { "=rf", "s", "", },
    { "=rf", "p", "", },
    { "=rf", "s", "", },
    { "", "", "", },
    { 0 },
    { "", "", },
    { "r", "r", },
    { 0 },
    { 0 },
    { "p", },
    { "p", },
    { "p", },
    { "", "", "", "", },
    { 0 },
    { 0 },
    { "m", },
    { "=&r", "r", "=&r", },
    { "=&r", "r", "=&r", },
    { "", "", "", },
    { "", "", "", },
    { "", "", },
    { "", "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "=rf", "", "=rf", "", },
    { "", "rf", "", "rf", },
    { "=fr", "", "=fr", "", },
    { "", "fr", "", "fr", },
    { "=rf", "", "=rf", "", },
    { "", "rf", "", "rf", },
    { "=fr", "", "=fr", "", },
    { "", "fr", "", "fr", },
    { "=r", "r", "r", },
    { "=r", "r", "r", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "", "", "", },
    { "=r", "i", "=er", },
    { "=r", "i", "=fr", },
    { "", "rI", },
    { "", "rI", },
    { "", "rI", },
    { "r", "r", },
    { "", "%r", "rI", },
    { "", "rHI", },
    { "", "%r", "rHI", },
    { "f", },
    { "", "ps", "", },
    { "ps", "", },
    { "", "ps", "", },
    { "ps", "", },
    { "=r", "rJ", },
  };

const enum machine_mode insn_operand_mode[][MAX_RECOG_OPERANDS] =
  {
    { SImode, SImode, },
    { DImode, DImode, },
    { SFmode, SFmode, },
    { DFmode, DFmode, },
    { TFmode, TFmode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, DImode, DImode, },
    { SImode, DImode, DImode, },
    { DImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, },
    { SImode, SImode, },
    { SFmode, SFmode, },
    { DFmode, DFmode, },
    { TFmode, TFmode, },
    { SFmode, SFmode, },
    { DFmode, DFmode, },
    { TFmode, TFmode, },
    { DImode, DImode, },
    { CCFPEmode, SFmode, SFmode, },
    { CCFPEmode, DFmode, DFmode, },
    { CCFPEmode, TFmode, TFmode, },
    { CCFPmode, SFmode, SFmode, },
    { CCFPmode, DFmode, DFmode, },
    { CCFPmode, TFmode, TFmode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, },
    { SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, },
    { SImode, },
    { SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode, },
    { VOIDmode, },
    { VOIDmode, CCFPmode, },
    { VOIDmode, CCFPmode, },
    { VOIDmode, CCFPEmode, },
    { VOIDmode, CCFPEmode, },
    { VOIDmode, DImode, },
    { VOIDmode, DImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, VOIDmode, },
    { SImode, VOIDmode, },
    { HImode, VOIDmode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { QImode, QImode, },
    { QImode, QImode, },
    { QImode, QImode, VOIDmode, },
    { SImode, QImode, SImode, },
    { HImode, HImode, },
    { HImode, HImode, },
    { HImode, HImode, VOIDmode, },
    { SImode, HImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { SFmode, SFmode, },
    { SFmode, SFmode, },
    { SFmode, SFmode, },
    { SFmode, SFmode, },
    { SImode, SFmode, SImode, },
    { DFmode, DFmode, },
    { DFmode, DFmode, },
    { DFmode, DFmode, },
    { DFmode, DFmode, },
    { VOIDmode, VOIDmode, },
    { SImode, DFmode, SImode, },
    { TFmode, TFmode, },
    { TFmode, TFmode, },
    { TFmode, TFmode, },
    { TFmode, TFmode, },
    { SImode, TFmode, SImode, },
    { SImode, VOIDmode, SImode, SImode, },
    { DImode, VOIDmode, DImode, DImode, },
    { SFmode, VOIDmode, SFmode, SFmode, },
    { DFmode, VOIDmode, DFmode, DFmode, },
    { TFmode, VOIDmode, TFmode, TFmode, },
    { SImode, VOIDmode, SImode, SImode, },
    { DImode, VOIDmode, DImode, DImode, },
    { SImode, VOIDmode, SImode, SImode, },
    { DImode, VOIDmode, DImode, DImode, },
    { SImode, VOIDmode, CCFPmode, SImode, SImode, },
    { SImode, VOIDmode, CCFPEmode, SImode, SImode, },
    { DImode, VOIDmode, CCFPmode, DImode, DImode, },
    { DImode, VOIDmode, CCFPEmode, DImode, DImode, },
    { SImode, VOIDmode, DImode, SImode, SImode, },
    { DImode, VOIDmode, DImode, DImode, DImode, },
    { SFmode, VOIDmode, DImode, SFmode, SFmode, },
    { DFmode, VOIDmode, DImode, DFmode, DFmode, },
    { TFmode, VOIDmode, DImode, TFmode, TFmode, },
    { SFmode, VOIDmode, CCFPmode, SFmode, SFmode, },
    { SFmode, VOIDmode, CCFPEmode, SFmode, SFmode, },
    { DFmode, VOIDmode, CCFPmode, DFmode, DFmode, },
    { DFmode, VOIDmode, CCFPEmode, DFmode, DFmode, },
    { TFmode, VOIDmode, CCFPmode, TFmode, TFmode, },
    { TFmode, VOIDmode, CCFPEmode, TFmode, TFmode, },
    { SFmode, VOIDmode, SFmode, SFmode, },
    { DFmode, VOIDmode, DFmode, DFmode, },
    { TFmode, VOIDmode, TFmode, TFmode, },
    { SFmode, VOIDmode, SFmode, SFmode, },
    { DFmode, VOIDmode, DFmode, DFmode, },
    { TFmode, VOIDmode, TFmode, TFmode, },
    { SImode, HImode, },
    { SImode, HImode, },
    { HImode, QImode, },
    { HImode, QImode, },
    { SImode, QImode, },
    { SImode, QImode, },
    { DImode, QImode, },
    { DImode, QImode, },
    { DImode, HImode, },
    { DImode, HImode, },
    { DImode, SImode, },
    { DImode, SImode, },
    { QImode, },
    { SImode, QImode, },
    { SImode, },
    { QImode, SImode, },
    { SImode, HImode, },
    { SImode, HImode, },
    { HImode, QImode, },
    { HImode, QImode, },
    { SImode, QImode, },
    { SImode, QImode, },
    { DImode, QImode, },
    { DImode, QImode, },
    { DImode, HImode, },
    { DImode, HImode, },
    { DImode, SImode, },
    { DImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { DFmode, SFmode, },
    { TFmode, SFmode, },
    { TFmode, DFmode, },
    { SFmode, DFmode, },
    { SFmode, TFmode, },
    { DFmode, TFmode, },
    { SFmode, SImode, },
    { DFmode, SImode, },
    { TFmode, SImode, },
    { SFmode, DImode, },
    { DFmode, DImode, },
    { TFmode, DImode, },
    { SFmode, DImode, DFmode, DImode, },
    { DFmode, DImode, DFmode, DImode, },
    { TFmode, DImode, DFmode, DImode, },
    { SFmode, DImode, },
    { DFmode, DImode, },
    { TFmode, DImode, },
    { SImode, SFmode, },
    { SImode, DFmode, },
    { SImode, TFmode, },
    { DImode, SFmode, },
    { DImode, DFmode, },
    { DImode, TFmode, },
    { DImode, SFmode, DFmode, DImode, },
    { DImode, DFmode, DFmode, DImode, },
    { DImode, TFmode, DFmode, DImode, },
    { DImode, SFmode, },
    { DImode, DFmode, },
    { DImode, TFmode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { DImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { DImode, DImode, DImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, SImode, },
    { DImode, DImode, DImode, DImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { SImode, SImode, SImode, SImode, },
    { DImode, DImode, DImode, DImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { SImode, SImode, },
    { SImode, },
    { DImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { DImode, DImode, },
    { SImode, SImode, },
    { SImode, },
    { DImode, },
    { SImode, SImode, },
    { DImode, DImode, },
    { TFmode, TFmode, TFmode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { TFmode, TFmode, TFmode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { TFmode, TFmode, TFmode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { DFmode, SFmode, SFmode, },
    { TFmode, DFmode, DFmode, },
    { TFmode, TFmode, TFmode, },
    { DFmode, DFmode, DFmode, },
    { SFmode, SFmode, SFmode, },
    { TFmode, TFmode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { TFmode, TFmode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { TFmode, TFmode, },
    { DFmode, DFmode, },
    { SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, SImode, },
    { SImode, },
    { SImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, SImode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, SImode, },
    { VOIDmode },
    { VOIDmode, },
    { SImode, },
    { DImode, },
    { SImode, },
    { DImode, },
    { VOIDmode },
    { VOIDmode },
    { SImode, SImode, SImode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { SImode, VOIDmode, },
    { SImode, VOIDmode, },
    { DImode, VOIDmode, },
    { DImode, VOIDmode, },
    { SImode, VOIDmode, VOIDmode, },
    { SImode, VOIDmode, VOIDmode, },
    { SImode, VOIDmode, VOIDmode, },
    { SImode, VOIDmode, VOIDmode, },
    { VOIDmode, SImode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, SImode, VOIDmode, },
    { VOIDmode, SImode, VOIDmode, },
    { VOIDmode, DImode, VOIDmode, },
    { VOIDmode, DImode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode },
    { BLKmode, VOIDmode, },
    { SImode, SImode, },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode, },
    { SImode, },
    { DImode, },
    { SImode, SImode, SImode, SImode, },
    { VOIDmode },
    { VOIDmode },
    { VOIDmode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, DImode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { QImode, QImode, },
    { HImode, HImode, },
    { SImode, SImode, },
    { SFmode, SFmode, },
    { SImode, SImode, SImode, },
    { DImode, DImode, },
    { DImode, DImode, DImode, },
    { SFmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
    { VOIDmode, VOIDmode, },
  };

const char insn_operand_strict_low[][MAX_RECOG_OPERANDS] =
  {
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, },
    { 0, 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0 },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0, },
    { 0 },
    { 0 },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0 },
    { 0, 0, },
    { 0, 0, },
    { 0 },
    { 0 },
    { 0, },
    { 0, },
    { 0, },
    { 0, 0, 0, 0, },
    { 0 },
    { 0 },
    { 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
  };

extern int register_operand ();
extern int arith_operand ();
extern int arith_double_operand ();
extern int intreg_operand ();
extern int ccfp_reg_operand ();
extern int noov_compare_op ();
extern int comparison_operator ();
extern int v9_regcmp_op ();
extern int immediate_operand ();
extern int move_pic_label ();
extern int const_double_operand ();
extern int data_segment_operand ();
extern int text_segment_operand ();
extern int general_operand ();
extern int reg_or_nonsymb_mem_operand ();
extern int move_operand ();
extern int symbolic_operand ();
extern int reg_or_0_operand ();
extern int scratch_operand ();
extern int arith10_operand ();
extern int arith11_operand ();
extern int arith11_double_operand ();
extern int arith10_double_operand ();
extern int memory_operand ();
extern int sparc_operand ();
extern int small_int ();
extern int uns_arith_operand ();
extern int uns_small_int ();
extern int cc_arithop ();
extern int cc_arithopn ();
extern int address_operand ();
extern int nonmemory_operand ();
extern int call_operand ();
extern int restore_operand ();

int (*const insn_operand_predicate[][MAX_RECOG_OPERANDS])() =
  {
    { register_operand, arith_operand, },
    { register_operand, arith_double_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { intreg_operand, },
    { register_operand, arith_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, arith_double_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, },
    { register_operand, },
    { register_operand, arith_operand, },
    { register_operand, arith_operand, },
    { register_operand, },
    { register_operand, },
    { register_operand, arith_operand, },
    { register_operand, arith_operand, arith_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, noov_compare_op, },
    { register_operand, noov_compare_op, },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { 0 },
    { noov_compare_op, },
    { noov_compare_op, },
    { comparison_operator, ccfp_reg_operand, },
    { comparison_operator, ccfp_reg_operand, },
    { comparison_operator, ccfp_reg_operand, },
    { comparison_operator, ccfp_reg_operand, },
    { v9_regcmp_op, register_operand, },
    { v9_regcmp_op, register_operand, },
    { register_operand, register_operand, immediate_operand, },
    { register_operand, register_operand, immediate_operand, },
    { register_operand, 0, },
    { register_operand, 0, },
    { register_operand, 0, },
    { register_operand, move_pic_label, },
    { register_operand, move_pic_label, },
    { register_operand, register_operand, immediate_operand, },
    { register_operand, register_operand, immediate_operand, },
    { register_operand, 0, },
    { register_operand, const_double_operand, },
    { register_operand, 0, },
    { register_operand, data_segment_operand, },
    { register_operand, text_segment_operand, },
    { register_operand, 0, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, move_operand, },
    { register_operand, register_operand, immediate_operand, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, move_operand, },
    { register_operand, register_operand, immediate_operand, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, move_operand, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { reg_or_nonsymb_mem_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, move_operand, },
    { general_operand, 0, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { general_operand, 0, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { 0, 0, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { general_operand, 0, },
    { general_operand, general_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { reg_or_nonsymb_mem_operand, reg_or_nonsymb_mem_operand, },
    { symbolic_operand, reg_or_0_operand, scratch_operand, },
    { register_operand, comparison_operator, arith10_operand, register_operand, },
    { register_operand, comparison_operator, arith10_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, arith11_operand, register_operand, },
    { register_operand, comparison_operator, arith11_double_operand, register_operand, },
    { register_operand, comparison_operator, arith11_operand, register_operand, },
    { register_operand, comparison_operator, arith11_double_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, arith11_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, arith11_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, arith11_double_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, arith11_double_operand, register_operand, },
    { register_operand, v9_regcmp_op, register_operand, arith10_operand, register_operand, },
    { register_operand, v9_regcmp_op, register_operand, arith10_double_operand, register_operand, },
    { register_operand, v9_regcmp_op, register_operand, register_operand, register_operand, },
    { register_operand, v9_regcmp_op, register_operand, register_operand, register_operand, },
    { register_operand, v9_regcmp_op, register_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, ccfp_reg_operand, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, comparison_operator, register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, sparc_operand, },
    { register_operand, register_operand, },
    { register_operand, sparc_operand, },
    { register_operand, register_operand, },
    { register_operand, sparc_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, sparc_operand, },
    { register_operand, },
    { register_operand, register_operand, },
    { register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, sparc_operand, },
    { register_operand, small_int, small_int, },
    { register_operand, small_int, small_int, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, general_operand, },
    { register_operand, general_operand, },
    { register_operand, general_operand, },
    { register_operand, general_operand, register_operand, memory_operand, },
    { register_operand, general_operand, register_operand, memory_operand, },
    { register_operand, general_operand, register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { general_operand, register_operand, },
    { general_operand, register_operand, },
    { general_operand, register_operand, },
    { general_operand, register_operand, register_operand, memory_operand, },
    { general_operand, register_operand, register_operand, memory_operand, },
    { general_operand, register_operand, register_operand, memory_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { arith_operand, arith_operand, },
    { arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, arith_operand, },
    { register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, small_int, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, uns_arith_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, uns_small_int, },
    { register_operand, register_operand, uns_arith_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, uns_small_int, },
    { register_operand, register_operand, arith_operand, scratch_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_operand, scratch_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_double_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { 0, 0, 0, 0, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { 0, 0, 0, 0, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_double_operand, arith_double_operand, },
    { register_operand, arith_operand, arith_operand, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { register_operand, register_operand, register_operand, },
    { register_operand, reg_or_0_operand, arith_double_operand, },
    { register_operand, reg_or_0_operand, arith_operand, },
    { arith_operand, arith_operand, cc_arithop, },
    { arith_double_operand, arith_double_operand, cc_arithop, },
    { register_operand, arith_operand, arith_operand, cc_arithop, },
    { register_operand, arith_double_operand, arith_double_operand, cc_arithop, },
    { reg_or_0_operand, arith_operand, },
    { reg_or_0_operand, arith_double_operand, },
    { register_operand, reg_or_0_operand, arith_operand, },
    { register_operand, reg_or_0_operand, arith_double_operand, },
    { arith_operand, reg_or_0_operand, cc_arithopn, },
    { arith_double_operand, reg_or_0_operand, cc_arithopn, },
    { register_operand, arith_operand, reg_or_0_operand, cc_arithopn, },
    { register_operand, arith_double_operand, reg_or_0_operand, cc_arithopn, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, arith_operand, },
    { arith_operand, },
    { arith_double_operand, },
    { register_operand, arith_operand, },
    { register_operand, arith_double_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, arith_double_operand, },
    { register_operand, arith_operand, },
    { arith_operand, },
    { arith_double_operand, },
    { register_operand, arith_operand, },
    { register_operand, arith_double_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, },
    { register_operand, register_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { register_operand, register_operand, arith_operand, },
    { 0 },
    { register_operand, },
    { register_operand, },
    { register_operand, },
    { address_operand, },
    { address_operand, },
    { 0 },
    { 0 },
    { register_operand, nonmemory_operand, nonmemory_operand, },
    { call_operand, 0, 0, 0, },
    { address_operand, 0, },
    { symbolic_operand, 0, },
    { address_operand, 0, },
    { symbolic_operand, 0, },
    { address_operand, 0, immediate_operand, },
    { symbolic_operand, 0, immediate_operand, },
    { address_operand, 0, immediate_operand, },
    { symbolic_operand, 0, immediate_operand, },
    { register_operand, 0, 0, 0, 0, },
    { 0, address_operand, 0, },
    { 0, symbolic_operand, 0, },
    { 0, address_operand, 0, },
    { 0, symbolic_operand, 0, },
    { 0, 0, 0, },
    { 0 },
    { memory_operand, 0, },
    { register_operand, register_operand, },
    { 0 },
    { 0 },
    { address_operand, },
    { address_operand, },
    { address_operand, },
    { general_operand, general_operand, general_operand, 0, },
    { 0 },
    { 0 },
    { memory_operand, },
    { register_operand, register_operand, scratch_operand, },
    { register_operand, register_operand, scratch_operand, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
    { restore_operand, arith_operand, },
    { restore_operand, arith_operand, },
    { restore_operand, arith_operand, },
    { restore_operand, register_operand, },
    { restore_operand, arith_operand, arith_operand, },
    { restore_operand, arith_double_operand, },
    { restore_operand, arith_operand, arith_double_operand, },
    { register_operand, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, 0, },
    { 0, 0, },
    { 0, 0, },
  };

const int insn_n_alternatives[] =
  {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    4,
    1,
    1,
    0,
    4,
    1,
    1,
    0,
    7,
    1,
    0,
    9,
    7,
    3,
    0,
    6,
    3,
    1,
    3,
    0,
    8,
    5,
    0,
    2,
    3,
    0,
    6,
    3,
    2,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    1,
    0,
    2,
    0,
    2,
    0,
    2,
    0,
    1,
    0,
    2,
    1,
    1,
    1,
    1,
    0,
    1,
    0,
    1,
    0,
    1,
    0,
    1,
    0,
    1,
    0,
    2,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    1,
    1,
    0,
    1,
    1,
    0,
    1,
    1,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    2,
    2,
    1,
    2,
    2,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    1,
    0,
    0,
    1,
    1,
    1,
    0,
    0,
    0,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
  };

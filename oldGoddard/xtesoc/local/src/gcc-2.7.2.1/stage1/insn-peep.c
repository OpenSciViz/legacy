/* Generated automatically by the program `genpeep'
from the machine description file `md'.  */

#include "config.h"
#include "rtl.h"
#include "regs.h"
#include "output.h"
#include "real.h"

extern rtx peep_operand[];

#define operands peep_operand

rtx
peephole (ins1)
     rtx ins1;
{
  rtx insn, x, pat;
  int i;

  if (NEXT_INSN (ins1)
      && GET_CODE (NEXT_INSN (ins1)) == BARRIER)
    return 0;

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L390;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L390;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, SImode)) goto L390;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L390; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L390;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L390;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SImode)) goto L390;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! memory_operand (x, SImode)) goto L390;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[0], operands[2]) 
   && ! MEM_VOLATILE_P (operands[1]) && ! MEM_VOLATILE_P (operands[3])
   && addrs_ok_for_ldd_peep (XEXP (operands[1], 0), XEXP (operands[3], 0)))) goto L390;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 390;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L390:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L391;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! memory_operand (x, SImode)) goto L391;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, SImode)) goto L391;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L391; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L391;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L391;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! memory_operand (x, SImode)) goto L391;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! register_operand (x, SImode)) goto L391;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[1], operands[3]) 
   && ! MEM_VOLATILE_P (operands[0]) && ! MEM_VOLATILE_P (operands[2])
   && addrs_ok_for_ldd_peep (XEXP (operands[0], 0), XEXP (operands[2], 0)))) goto L391;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 391;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L391:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L392;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SFmode)) goto L392;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, SFmode)) goto L392;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L392; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L392;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L392;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SFmode)) goto L392;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! memory_operand (x, SFmode)) goto L392;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[0], operands[2]) 
   && ! MEM_VOLATILE_P (operands[1]) && ! MEM_VOLATILE_P (operands[3])
   && addrs_ok_for_ldd_peep (XEXP (operands[1], 0), XEXP (operands[3], 0)))) goto L392;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 392;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L392:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L393;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! memory_operand (x, SFmode)) goto L393;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, SFmode)) goto L393;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L393; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L393;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L393;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! memory_operand (x, SFmode)) goto L393;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! register_operand (x, SFmode)) goto L393;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[1], operands[3]) 
   && ! MEM_VOLATILE_P (operands[0]) && ! MEM_VOLATILE_P (operands[2])
   && addrs_ok_for_ldd_peep (XEXP (operands[0], 0), XEXP (operands[2], 0)))) goto L393;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 393;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L393:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L394;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L394;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, SImode)) goto L394;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L394; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L394;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L394;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SImode)) goto L394;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! memory_operand (x, SImode)) goto L394;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[2], operands[0]) 
   && ! MEM_VOLATILE_P (operands[3]) && ! MEM_VOLATILE_P (operands[1])
   && addrs_ok_for_ldd_peep (XEXP (operands[3], 0), XEXP (operands[1], 0)))) goto L394;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 394;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L394:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L395;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! memory_operand (x, SImode)) goto L395;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, SImode)) goto L395;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L395; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L395;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L395;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! memory_operand (x, SImode)) goto L395;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! register_operand (x, SImode)) goto L395;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[3], operands[1]) 
   && ! MEM_VOLATILE_P (operands[2]) && ! MEM_VOLATILE_P (operands[0])
   && addrs_ok_for_ldd_peep (XEXP (operands[2], 0), XEXP (operands[0], 0)))) goto L395;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 395;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L395:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L396;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SFmode)) goto L396;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, SFmode)) goto L396;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L396; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L396;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L396;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SFmode)) goto L396;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! memory_operand (x, SFmode)) goto L396;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[2], operands[0]) 
   && ! MEM_VOLATILE_P (operands[3]) && ! MEM_VOLATILE_P (operands[1])
   && addrs_ok_for_ldd_peep (XEXP (operands[3], 0), XEXP (operands[1], 0)))) goto L396;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 396;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L396:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L397;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! memory_operand (x, SFmode)) goto L397;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, SFmode)) goto L397;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L397; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L397;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L397;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! memory_operand (x, SFmode)) goto L397;
  x = XEXP (pat, 1);
  operands[3] = x;
  if (! register_operand (x, SFmode)) goto L397;
  if (! (! TARGET_V9
   && registers_ok_for_ldd_peep (operands[3], operands[1]) 
   && ! MEM_VOLATILE_P (operands[2]) && ! MEM_VOLATILE_P (operands[0])
   && addrs_ok_for_ldd_peep (XEXP (operands[2], 0), XEXP (operands[0], 0)))) goto L397;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 397;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L397:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L398;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L398;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, SImode)) goto L398;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L398; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L398;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L398;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L398;
  if (GET_MODE (x) != CCmode) goto L398;
  if (XINT (x, 0) != 0) goto L398;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L398;
  if (GET_MODE (x) != CCmode) goto L398;
  x = XEXP (XEXP (pat, 1), 0);
  operands[2] = x;
  if (! register_operand (x, SImode)) goto L398;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L398;
  if (XWINT (x, 0) != 0) goto L398;
  if (! ((rtx_equal_p (operands[2], operands[0])
    || rtx_equal_p (operands[2], operands[1]))
   && ! FP_REG_P (operands[0]) && ! FP_REG_P (operands[1]))) goto L398;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 398;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L398:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L399;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, DImode)) goto L399;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! register_operand (x, DImode)) goto L399;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L399; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L399;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L399;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L399;
  if (GET_MODE (x) != CCXmode) goto L399;
  if (XINT (x, 0) != 0) goto L399;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L399;
  if (GET_MODE (x) != CCXmode) goto L399;
  x = XEXP (XEXP (pat, 1), 0);
  operands[2] = x;
  if (! register_operand (x, DImode)) goto L399;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L399;
  if (XWINT (x, 0) != 0) goto L399;
  if (! (TARGET_V9
   && (rtx_equal_p (operands[2], operands[0])
       || rtx_equal_p (operands[2], operands[1]))
   && ! FP_REG_P (operands[0]) && ! FP_REG_P (operands[1]))) goto L399;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 399;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L399:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L400;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, HImode)) goto L400;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, HImode)) goto L400;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L400; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L400;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L400;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SImode)) goto L400;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != SIGN_EXTEND) goto L400;
  if (GET_MODE (x) != SImode) goto L400;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L400;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L400; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L400;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L400;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L400;
  if (GET_MODE (x) != CCmode) goto L400;
  if (XINT (x, 0) != 0) goto L400;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L400;
  if (GET_MODE (x) != CCmode) goto L400;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[2], x)) goto L400;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L400;
  if (XWINT (x, 0) != 0) goto L400;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 400;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L400:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L401;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, HImode)) goto L401;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, HImode)) goto L401;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L401; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L401;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L401;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, DImode)) goto L401;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != SIGN_EXTEND) goto L401;
  if (GET_MODE (x) != DImode) goto L401;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L401;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L401; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L401;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L401;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L401;
  if (GET_MODE (x) != CCXmode) goto L401;
  if (XINT (x, 0) != 0) goto L401;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L401;
  if (GET_MODE (x) != CCXmode) goto L401;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[2], x)) goto L401;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L401;
  if (XWINT (x, 0) != 0) goto L401;
  if (! (TARGET_V9)) goto L401;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 401;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L401:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L402;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, QImode)) goto L402;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, QImode)) goto L402;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L402; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L402;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L402;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SImode)) goto L402;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != SIGN_EXTEND) goto L402;
  if (GET_MODE (x) != SImode) goto L402;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L402;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L402; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L402;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L402;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L402;
  if (GET_MODE (x) != CCmode) goto L402;
  if (XINT (x, 0) != 0) goto L402;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L402;
  if (GET_MODE (x) != CCmode) goto L402;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[2], x)) goto L402;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L402;
  if (XWINT (x, 0) != 0) goto L402;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 402;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L402:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L403;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, QImode)) goto L403;
  x = XEXP (pat, 1);
  operands[1] = x;
  if (! memory_operand (x, QImode)) goto L403;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L403; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L403;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L403;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, DImode)) goto L403;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != SIGN_EXTEND) goto L403;
  if (GET_MODE (x) != DImode) goto L403;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L403;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L403; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L403;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L403;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L403;
  if (GET_MODE (x) != CCXmode) goto L403;
  if (XINT (x, 0) != 0) goto L403;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L403;
  if (GET_MODE (x) != CCXmode) goto L403;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[2], x)) goto L403;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L403;
  if (XWINT (x, 0) != 0) goto L403;
  if (! (TARGET_V9)) goto L403;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 403;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L403:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L404;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L404;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LO_SUM) goto L404;
  if (GET_MODE (x) != SImode) goto L404;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L404;
  x = XEXP (XEXP (pat, 1), 1);
  operands[1] = x;
  if (! immediate_operand (x, SImode)) goto L404;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L404; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L404;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L404;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, DFmode)) goto L404;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != MEM) goto L404;
  if (GET_MODE (x) != DFmode) goto L404;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L404;
  if (! (RTX_UNCHANGING_P (operands[1]) && reg_unused_after (operands[0], insn))) goto L404;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 404;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L404:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L405;
  x = XEXP (pat, 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L405;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LO_SUM) goto L405;
  if (GET_MODE (x) != SImode) goto L405;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L405;
  x = XEXP (XEXP (pat, 1), 1);
  operands[1] = x;
  if (! immediate_operand (x, SImode)) goto L405;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L405; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L405;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L405;
  x = XEXP (pat, 0);
  operands[2] = x;
  if (! register_operand (x, SFmode)) goto L405;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != MEM) goto L405;
  if (GET_MODE (x) != SFmode) goto L405;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L405;
  if (! (RTX_UNCHANGING_P (operands[1]) && reg_unused_after (operands[0], insn))) goto L405;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 405;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L405:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != PARALLEL) goto L414;
  if (XVECLEN (x, 0) != 2) goto L414;
  x = XVECEXP (pat, 0, 0);
  if (GET_CODE (x) != SET) goto L414;
  x = XEXP (XVECEXP (pat, 0, 0), 0);
  operands[0] = x;
  x = XEXP (XVECEXP (pat, 0, 0), 1);
  if (GET_CODE (x) != CALL) goto L414;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0);
  if (GET_CODE (x) != MEM) goto L414;
  if (GET_MODE (x) != SImode) goto L414;
  x = XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0);
  operands[1] = x;
  if (! call_operand_address (x, SImode)) goto L414;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
  operands[2] = x;
  x = XVECEXP (pat, 0, 1);
  if (GET_CODE (x) != CLOBBER) goto L414;
  x = XEXP (XVECEXP (pat, 0, 1), 0);
  if (GET_CODE (x) != REG) goto L414;
  if (GET_MODE (x) != SImode) goto L414;
  if (XINT (x, 0) != 15) goto L414;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L414; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L414;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L414;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != PC) goto L414;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LABEL_REF) goto L414;
  x = XEXP (XEXP (pat, 1), 0);
  operands[3] = x;
  if (! (short_branch (INSN_UID (insn), INSN_UID (operands[3])))) goto L414;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 414;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L414:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != PARALLEL) goto L415;
  if (XVECLEN (x, 0) != 2) goto L415;
  x = XVECEXP (pat, 0, 0);
  if (GET_CODE (x) != CALL) goto L415;
  x = XEXP (XVECEXP (pat, 0, 0), 0);
  if (GET_CODE (x) != MEM) goto L415;
  if (GET_MODE (x) != SImode) goto L415;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0);
  operands[0] = x;
  if (! call_operand_address (x, SImode)) goto L415;
  x = XEXP (XVECEXP (pat, 0, 0), 1);
  operands[1] = x;
  x = XVECEXP (pat, 0, 1);
  if (GET_CODE (x) != CLOBBER) goto L415;
  x = XEXP (XVECEXP (pat, 0, 1), 0);
  if (GET_CODE (x) != REG) goto L415;
  if (GET_MODE (x) != SImode) goto L415;
  if (XINT (x, 0) != 15) goto L415;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L415; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L415;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L415;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != PC) goto L415;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LABEL_REF) goto L415;
  x = XEXP (XEXP (pat, 1), 0);
  operands[2] = x;
  if (! (short_branch (INSN_UID (insn), INSN_UID (operands[2])))) goto L415;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 415;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L415:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != PARALLEL) goto L416;
  if (XVECLEN (x, 0) != 2) goto L416;
  x = XVECEXP (pat, 0, 0);
  if (GET_CODE (x) != SET) goto L416;
  x = XEXP (XVECEXP (pat, 0, 0), 0);
  operands[0] = x;
  x = XEXP (XVECEXP (pat, 0, 0), 1);
  if (GET_CODE (x) != CALL) goto L416;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0);
  if (GET_CODE (x) != MEM) goto L416;
  if (GET_MODE (x) != SImode) goto L416;
  x = XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0);
  operands[1] = x;
  if (! call_operand_address (x, DImode)) goto L416;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
  operands[2] = x;
  x = XVECEXP (pat, 0, 1);
  if (GET_CODE (x) != CLOBBER) goto L416;
  x = XEXP (XVECEXP (pat, 0, 1), 0);
  if (GET_CODE (x) != REG) goto L416;
  if (GET_MODE (x) != DImode) goto L416;
  if (XINT (x, 0) != 15) goto L416;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L416; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L416;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L416;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != PC) goto L416;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LABEL_REF) goto L416;
  x = XEXP (XEXP (pat, 1), 0);
  operands[3] = x;
  if (! (TARGET_V9 && short_branch (INSN_UID (insn), INSN_UID (operands[3])))) goto L416;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (4, operands));
  INSN_CODE (ins1) = 416;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L416:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != PARALLEL) goto L417;
  if (XVECLEN (x, 0) != 2) goto L417;
  x = XVECEXP (pat, 0, 0);
  if (GET_CODE (x) != CALL) goto L417;
  x = XEXP (XVECEXP (pat, 0, 0), 0);
  if (GET_CODE (x) != MEM) goto L417;
  if (GET_MODE (x) != SImode) goto L417;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0);
  operands[0] = x;
  if (! call_operand_address (x, DImode)) goto L417;
  x = XEXP (XVECEXP (pat, 0, 0), 1);
  operands[1] = x;
  x = XVECEXP (pat, 0, 1);
  if (GET_CODE (x) != CLOBBER) goto L417;
  x = XEXP (XVECEXP (pat, 0, 1), 0);
  if (GET_CODE (x) != REG) goto L417;
  if (GET_MODE (x) != DImode) goto L417;
  if (XINT (x, 0) != 15) goto L417;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L417; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L417;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L417;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != PC) goto L417;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != LABEL_REF) goto L417;
  x = XEXP (XEXP (pat, 1), 0);
  operands[2] = x;
  if (! (TARGET_V9 && short_branch (INSN_UID (insn), INSN_UID (operands[2])))) goto L417;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (3, operands));
  INSN_CODE (ins1) = 417;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L417:

  insn = ins1;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != PARALLEL) goto L418;
  if (XVECLEN (x, 0) != 2) goto L418;
  x = XVECEXP (pat, 0, 0);
  if (GET_CODE (x) != SET) goto L418;
  x = XEXP (XVECEXP (pat, 0, 0), 0);
  operands[0] = x;
  if (! register_operand (x, SImode)) goto L418;
  x = XEXP (XVECEXP (pat, 0, 0), 1);
  if (GET_CODE (x) != MINUS) goto L418;
  if (GET_MODE (x) != SImode) goto L418;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0);
  operands[1] = x;
  if (! reg_or_0_operand (x, SImode)) goto L418;
  x = XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
  if (GET_CODE (x) != REG) goto L418;
  if (GET_MODE (x) != SImode) goto L418;
  if (XINT (x, 0) != 0) goto L418;
  x = XVECEXP (pat, 0, 1);
  if (GET_CODE (x) != CLOBBER) goto L418;
  x = XEXP (XVECEXP (pat, 0, 1), 0);
  if (GET_CODE (x) != REG) goto L418;
  if (GET_MODE (x) != CCmode) goto L418;
  if (XINT (x, 0) != 0) goto L418;
  do { insn = NEXT_INSN (insn);
       if (insn == 0) goto L418; }
  while (GET_CODE (insn) == NOTE
	 || (GET_CODE (insn) == INSN
	     && (GET_CODE (PATTERN (insn)) == USE
		 || GET_CODE (PATTERN (insn)) == CLOBBER)));
  if (GET_CODE (insn) == CODE_LABEL
      || GET_CODE (insn) == BARRIER)
    goto L418;
  pat = PATTERN (insn);
  x = pat;
  if (GET_CODE (x) != SET) goto L418;
  x = XEXP (pat, 0);
  if (GET_CODE (x) != REG) goto L418;
  if (GET_MODE (x) != CCmode) goto L418;
  if (XINT (x, 0) != 0) goto L418;
  x = XEXP (pat, 1);
  if (GET_CODE (x) != COMPARE) goto L418;
  x = XEXP (XEXP (pat, 1), 0);
  if (!rtx_equal_p (operands[0], x)) goto L418;
  x = XEXP (XEXP (pat, 1), 1);
  if (GET_CODE (x) != CONST_INT) goto L418;
  if (XWINT (x, 0) != 0) goto L418;
  PATTERN (ins1) = gen_rtx (PARALLEL, VOIDmode, gen_rtvec_v (2, operands));
  INSN_CODE (ins1) = 418;
  delete_for_peephole (NEXT_INSN (ins1), insn);
  return NEXT_INSN (insn);
 L418:

  return 0;
}

rtx peep_operand[4];

#ifndef XCOMM
#define XCOMM #
#endif

XCOMM ImageMagick Imakefile info.  This contains any special redefinitions.
XCOMM
XCOMM Copyright 1995 E. I. du Pont de Nemours & Company
XCOMM
XCOMM Permission to use, copy, modify, distribute, and sell this software and
XCOMM its documentation for any purpose is hereby granted without fee,
XCOMM provided that the above Copyright notice appear in all copies and that
XCOMM both that Copyright notice and this permission notice appear in
XCOMM supporting documentation, and that the name of E. I. du Pont de Nemours
XCOMM and Company not be used in advertising or publicity pertaining to
XCOMM distribution of the software without specific, written prior
XCOMM permission.  E. I. du Pont de Nemours & Company makes no representations
XCOMM about the suitability of this software for any purpose.  It is provided
XCOMM "as is" without express or implied warranty.
XCOMM
XCOMM E. I. du Pont de Nemours & Company disclaims all warranties with regard
XCOMM to this software, including all implied warranties of merchantability
XCOMM and fitness, in no event shall E. I. du Pont de Nemours & Company be
XCOMM liable for any special, indirect or consequential damages or any
XCOMM damages whatsoever resulting from loss of use, data or profits, whether
XCOMM in an action of contract, negligence or other tortious action, arising
XCOMM out of or in connection with the use or performance of this software.

XCOMM If your system does not have /usr/include/dirent.h, define:
XCOMM
XCOMM DIRENT_DEFINES= -DNoDIRENT

XCOMM Define HasShape if your X server supports the Shape Extension and
XCOMM HasSharedMemory for the Shared Memory Extension.
XCOMM
XCOMM EXTENSIONS_DEFINES= -DHasShape -DHasSharedMemory

XCOMM Set DoSharedLib to HasSharedLibraries to build ImageMagick with shared
XCOMM libraries.
XCOMM
XCOMM #define DoSharedLib  HasSharedLibraries

XCOMM By default, the maximum color value is 255.  To increase the maximum to
XCOMM 65535 define:
XCOMM
XCOMM QUANTUM_DEFINES= -DQuantumLeap

XCOMM Define HasDPS if your X server supports the Display Postscript
XCOMM extension.  If you have the HDF, JBIG, JPEG, MPEG, PNG, TIFF, or XPM
XCOMM libraries define HasHDF, HasJBIG, HasJPEG, HasMPEG, HasPNG, HasTIFF,
XCOMM or HasXPM respectively.  Make sure the path names are correct.  See
XCOMM README for more details.
XCOMM
XCOMM #define HasDPS
XCOMM #define HasHDF
XCOMM #define HasJBIG
XCOMM #define HasJPEG
XCOMM #define HasMPEG
XCOMM #define HasPNG
XCOMM #define HasTIFF
XCOMM #define HasXPM

#ifdef HasDPS
DPS_DEFINES= -DHasDPS
DPS_INCLUDES= -I/usr/openwin/include/X11
DPS_LIBRARIES= -L/usr/openwin/lib -ldpstk -ldps
#endif

#ifdef HasHDF
HDF_DEFINES= -DHasHDF
HDF_INCLUDES= -I$(TOP)/hdf
HDF_LIBRARIES= -L$(TOP)/hdf -ldf
#endif

#ifdef HasJBIG
JBIG_DEFINES= -DHasJBIG
JBIG_INCLUDES= -I$(TOP)/jbig/libjbig
JBIG_LIBRARIES= -L$(TOP)/jbig/libjbig -ljbig
#endif

#ifdef HasJPEG
JPEG_DEFINES= -DHasJPEG
JPEG_INCLUDES= -I$(TOP)/jpeg
JPEG_LIBRARIES= -L$(TOP)/jpeg -ljpeg
#endif

#ifdef HasMPEG
MPEG_DEFINES= -DHasMPEG
MPEG_INCLUDES= -I$(TOP)/mpeg
MPEG_LIBRARIES= -L$(TOP)/mpeg -lmpeg
#endif

#ifdef HasPNG
PNG_DEFINES= -DHasPNG
PNG_INCLUDES= -I$(TOP)/png -I$(TOP)/zlib
PNG_LIBRARIES= -L$(TOP)/png -lpng -L$(TOP)/zlib -lz
#endif

#ifdef HasTIFF
TIFF_DEFINES= -DHasTIFF
TIFF_INCLUDES= -I$(TOP)/tiff/libtiff
TIFF_LIBRARIES= -L$(TOP)/tiff/libtiff -ltiff
#endif

#ifdef HasXPM
XPM_DEFINES= -DHasXPM
XPM_INCLUDES= -I$(TOP)/xpm/lib
XPM_LIBRARIES= -L$(TOP)/xpm/lib -lXpm
#endif

XCOMM Might need extra libraries for xtp to link correctly.
XCOMM
XCOMM  EXTRA_LOAD_FLAGS= -lnsl

#ifdef HPArchitecture
CCOPTIONS= -Aa -Dhpux
#endif

XCOMM ImageMagick has an include file named X.h.  Do not set LOCALINC to
XCOMM the top level X11 include directory or X11/X.h will be overwritten.

LOCALDIR= /usr/local/bin
LOCALINC= /usr/local/include
LOCALLIB= /usr/local/lib
LOCALMAN= /usr/local/man/mann

MAGICK_INCLUDES= -I$(TOP)/magick
MAGICK_LIBRARIES= -L$(TOP)/magick -lMagick

DEFINES= $(MAGICK_INCLUDES) $(DIRENT_DEFINES) $(EXTENSIONS_DEFINES) \
  $(QUANTUM_DEFINES) $(DPS_DEFINES) $(DPS_INCLUDES) \
  $(HDF_DEFINES) $(HDF_INCLUDES) $(JBIG_DEFINES) $(JBIG_INCLUDES) \
  $(JPEG_DEFINES) $(JPEG_INCLUDES) $(MPEG_DEFINES) $(MPEG_INCLUDES) \
  $(PNG_DEFINES) $(PNG_INCLUDES) $(TIFF_DEFINES) $(TIFF_INCLUDES) \
  $(XPM_DEFINES) $(XPM_INCLUDES)
LOCAL_LIBRARIES= $(MAGICK_LIBRARIES) $(DPS_LIBRARIES) $(JBIG_LIBRARIES) \
  $(JPEG_LIBRARIES) $(MPEG_LIBRARIES) $(HDF_LIBRARIES) $(PNG_LIBRARIES) \
  $(TIFF_LIBRARIES) $(XPM_LIBRARIES)
REQUIRED_LIBRARIES= $(DPS_LIBRARIES) $(HDF_LIBRARIES) $(JBIG_LIBRARIES) \
  $(JPEG_LIBRARIES) $(MPEG_LIBRARIES) $(PNG_LIBRARIES) $(TIFF_LIBRARIES) \
  $(XPM_LIBRARIES)


#ifdef uxpArchitecture
#define ForceSubdirs(dirs)                                              @@\
dirs::                                                                  @@\
        @cd $@ ; $(MAKE) $(MFLAGS) PassCDebugFlags all

INSTALL=/usr/ucb/install -g sys
#endif

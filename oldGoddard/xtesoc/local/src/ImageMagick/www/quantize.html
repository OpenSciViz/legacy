<HTML VERSION="2.0">
<HEAD>
<TITLE>ImageMagick's color reduction algorithm</TITLE>
</HEAD>
<BODY BACKGROUND="../images/background.gif">
<CENTER>
<H1>ImageMagick's color reduction algorithm</H1>
</CENTER>
<pre>

</pre>
<DL>
<DD>This document describes how <B>ImageMagick</B> performs color
reduction on an image.  To fully understand this document,
you should have a knowledge of basic imaging techniques and
the tree data structure and terminology.
<P>
<DD>For purposes of color allocation, an image is a set of <I>n</I>
pixels, where each pixel is a point in <B>RGB</B> space.  <B>RGB</B> space
is a 3-dimensional vector space, and each pixel, <I>p(i)</I>,  is
defined by an ordered triple of red, green, and blue
coordinates, (<I>r(i)</I>,<I>g(i)</I>,<I>b(i)</I>).
<P>
<DD>Each primary color component (<I>red</I>, <I>green</I>, or <I>blue</I>)
represents an intensity which varies linearly from 0 to a
maximum value, <I>Cmax</I>, which corresponds to full saturation of
that color.  Color allocation is defined over a domain
consisting of the cube in <B>RGB</B> space with opposite vertices
at (0,0,0) and (<I>Cmax</I>,<I>Cmax</I>,<I>Cmax</I>).  <B>ImageMagick</B>
requires <I>Cmax</I>= <I>255</I>.
<P>
<DD>The algorithm maps this domain onto a tree in which each
node represents a cube within that domain.  In the following
discussion, these cubes are defined by the coordinate of two
opposite vertices: The vertex nearest the origin in <B>RGB</B>
space and the vertex farthest from the origin.
<P>
<DD>The tree's root node represents the the entire domain,
(0,0,0) through (<I>Cmax</I>,<I>Cmax</I>,<I>Cmax</I>).  Each lower level in the
tree is generated by subdividing one node's cube into eight
smaller cubes of equal size.  This corresponds to bisecting
the parent cube with planes passing through the midpoints of
each edge.
<P>
<DD>The basic algorithm operates in three phases:
<P>
<UL>
<LI> <B>Classification,</B>
<LI> <B>Reduction</B>, and
<LI> <B>Assignment</B>. 
</UL>
<P>
<B>Classification</B>
builds a color description tree for the image.  <B>Reduction</B>
collapses the tree until the number it represents, at most,
is the number of colors desired in the output image.
<B>Assignment</B> defines the output image's color map and sets
each pixel's color by reclassification in the reduced tree.
<I>Our goal is to minimize the numerical discrepancies between
the original colors and quantized colors</I>.  To learn more
about quantization error, see <B>Measuring Color Reduction
Error</B> later in this document.
<P>
<DD><B>Classification</B> begins by initializing a color description
tree of sufficient depth to represent each possible input
color in a leaf.  However, it is impractical to generate a
fully-formed color description tree in the classification
phase for realistic values of <I>Cmax</I>.  If color
components in
the input image are quantized to <I>k</I>-bit precision, so that
<I>Cmax</I> = <I>2^k-1</I>, the tree would need <I>k</I> levels below the root
node to allow representing each possible input color in a
leaf.  This becomes <B>prohibitive</B> because the tree's:
<PRE>
      total number of nodes = 1+Sum(8^i), i=1,k

      For k=8, 
      Number of nodes= 1 + (8^1+8^2+....+8^8)
                               8^8 - 1 
                     = 1 + 8.-----------
                               8 - 1
                     = 19,173,961 
</PRE>
<P>
<DD>Therefore, to avoid building a fully populated
tree, <B>ImageMagick</B>: 
<OL>
<LI> Initializes data structures for nodes
only as they are needed; 
<LI> Chooses a maximum depth for the
tree as a function of the desired number of colors in the
output image (<B>currently based-two logarithm of <I>Cmax</I></B>). 
</OL>
<PRE>
      For Cmax=255, 
                          
      Maximum tree depth = log (255)  
                              2

                         = log (255) / log (2)
                              e           e

                         =7.99 ~= 8
</PRE>
</DL>
<DL>
<DD>A tree of
this depth generally allows the best representation of the
source image with the fastest computational speed and the
least amount of memory.  However, the default depth is
inappropriate for some images.  Therefore, the caller can
request a specific tree depth.
<P>
<DD>For each pixel in the input image, classification scans
downward from the root of the color description tree.  At
each level of the tree, it identifies the single node which
represents a cube in <B>RGB</B> space containing the pixel's color.
It updates the following data for each such node:
</DL>
<P>
<DL>
<DL>
<DL>
<DT><B>n1:</B>
<DD>Number of pixels whose color is contained in the <B>RGB</B>
          cube which this node represents;
<P>
<DT><B>n2:</B>
<DD>Number of pixels whose color is not represented in a
          node at lower depth in the tree;  initially, <B>n2=0</B>
          for all nodes except leaves of the tree.
<P>
<DT><B>Sr,Sg,Sb:</B>
<DD>Sums of the <I>red</I>, <I>green</I>, and <I>blue</I> component values for
all pixels not classified at a lower depth.  The
combination of these sums and <I>n2</I> will ultimately
characterize the mean color of a set of pixels
represented by this node.
<P>
<DT><B>E:</B>
<DD>The distance squared in <B>RGB</B> space between each pixel
contained within a node and the nodes' center.  This
represents the quantization error for a node.
</DL>
</DL>
</DL>
<P>
<DL>
<DD><B>Reduction</B> repeatedly prunes the tree until the number of
nodes with <I>n2</I> &gt; <I>0</I> is less than or equal to the maximum
number of colors allowed in the output image.  On any given iteration over the
tree, it selects those nodes whose <I>E</I> 
value is minimal for pruning and merges their color
statistics upward.  It uses a pruning threshold, <I>Ep</I>, to
govern node selection as follows:
<PRE>
      Ep = 0
      while number of nodes with (n2 &gt; 0) &gt; required maximum number of colors
         prune all nodes such that E &lt;= Ep
         Set Ep  to minimum E in remaining nodes
</PRE>
<P>
<DD>This has the effect of minimizing any quantization error
when merging two nodes together.
</DL>
<P>
<DL>
<DD>When a node to be pruned has offspring, the pruning
procedure invokes itself recursively in order to prune the
tree from the leaves upward.  The values of <I>n2</I>,<I>Sr</I>, <I>Sg</I>  and
<I>Sb</I> in a node being pruned are always added to the
corresponding data in that node's parent.  This retains the
pruned node's color characteristics for later averaging.
<P>
<DD>For each node,  <I>n2</I> pixels exist for which that node
represents the smallest volume in <B>RGB</B> space containing those
pixel's colors.  When <I>n2</I> &gt; <I>0</I> the node will uniquely define
a color in the output image.  At the beginning of reduction,
<I>n2</I> = <I>0</I>  for all nodes except the leaves of the tree which
represent colors present in the input image.
<P>
<DD>The other pixel count, <I>n1</I>,  indicates the total number of
colors within the cubic volume which the node represents.
This includes <I>n1</I> - <I>n2</I> pixels whose colors should be defined
by nodes at a lower level in the tree.
<P>
<DD><B>Assignment</B> generates the output image from the pruned tree.
The output image consists of two parts: 
<OL>
<LI>A color map,
which is an array of color descriptions (<B>RGB</B> triples) for
each color present in the output image. 
<LI>A pixel array,
which represents each pixel as an index into the color map
array.
</OL>
<P>
<DD>First, the assignment phase makes one pass over the pruned
color description tree to establish the image's color map.
For each node with <I>n2</I> &gt; <I>0</I>, it divides <I>Sr</I>,
<I>Sg</I>, and <I>Sb</I> by <I>n2</I>.
This produces the mean color of all pixels that classify no
lower than this node.  Each of these colors becomes an entry
in the color map.
<P>
<DD>Finally, the assignment phase reclassifies each pixel in the
pruned tree to identify the deepest node containing the
pixel's color.  The pixel's value in the pixel array becomes
the index of this node's mean color in the color map.
<P>
<DD>Empirical evidence suggests that the distances in color spaces
     such as <B>YUV</B>, or <B>YIQ</B> correspond to perceptual color
     differences more closely than do distances in <B>RGB</B> space.
     These color spaces may give better results when color
     reducing an image.  Here the algorithm is as described
     except each pixel is a point in the alternate color space.
     For convenience, the color components are normalized to the
     range 0 to a maximum value, <I>Cmax</I>.  The color reduction can
     then proceed as described.
</DL>
<DL>
<pre>

</pre>
<DT><H2>Measuring Color Reduction Error</H2>
<DD>Depending on the image, the color reduction error may be
obvious or invisible.  Images with high spatial frequencies
(such as hair or grass) will show error much less than
pictures with large smoothly shaded areas (such as faces).
This is because the high-frequency contour edges introduced
by the color reduction process are masked by the high
frequencies in the image.
<P>
To measure the difference between the original and color
reduced images (the total color reduction error),
<B>ImageMagick</B> sums over all pixels in an image the distance
squared in <B>RGB</B> space between each original pixel value and
its color reduced value. <B>ImageMagick</B> prints several error
measurements including the mean error per pixel, the
normalized mean error, and the normalized maximum error.
<P>
The normalized error measurement can be used to compare
images.  In general, the closer the mean error is to zero
the more the quantized image resembles the source image.
Ideally, the error should be perceptually-based, since the
human eye is the final judge of quantization quality.
<P>
These errors are measured and printed when <B>-verbose</B> and
<B>-colors</B> <I>are specified on the command line</I>:
<P>
<DL>
<DL>
<DT><B>mean error per pixel:</B>
<DD>is the mean error for any single pixel in the image.
<P>
<DT><B>normalized mean square error:</B>
<DD>is the normalized mean square quantization error for
any single pixel in the image.
<P>
<DD>This distance measure is normalized to a range between
0 and 1.  It is independent of the range of red, green,
and blue values in the image.
<P>
<DT><B>normalized maximum square error:</B>
<DD>is the largest normalized square quantization error for
any single pixel in the image.
<P>
<DD>This distance measure is normalized to a range between and blue values
in the image.
</DL>
</DL>
</DL>
<DL>
<pre>

</pre>
<DT><H2>Copyright</H2>
<B>
<DD>Copyright 1995 E. I. du Pont de Nemours and Company
Permission to use, copy, modify, distribute, and sell this
software and its documentation for any purpose is hereby
granted without fee, provided that the above copyright
notice appear in all copies and that both that copyright
notice and this permission notice appear in supporting
documentation, and that the name of E. I. du Pont de
Nemours and Company not be used in advertising or
publicity pertaining to distribution of the software
without specific, written prior permission.  E. I. du Pont
de Nemours and Company makes no representations about the
suitability of this software for any purpose.  It is
provided "as is" without express or implied warranty.
<P>
<DD>E. I. du Pont de Nemours and Company disclaims all
warranties with regard to this software, including all
implied warranties of merchantability and fitness, in no
event shall E. I. du Pont de Nemours and Company be liable
for any special, indirect or consequential damages or any
damages whatsoever resulting from loss of use, data or
profits, whether in an action of contract, negligence or
other tortuous action, arising out of or in connection
with the use or performance of this software.
</B>
</DL>
<DL>
<pre>

</pre>
<DT><H2>Acknowledgements</H2>
<DD><I>Paul Raveling</I>, <B>USC Information Sciences Institute</B>, for the
original idea of using space subdivision for the color
reduction algorithm.  With Paul's permission, this document
is an adaptation from a document he wrote.
</DL>
<DL>
<DT><H2>Authors</H2>
<DD><I>John Cristy</I>, <A HREF="mailto:cristy@dupont.com">
 <I>cristy@dupont.com</I>,</A> <B>E.I. du Pont de Nemours and Company
 Incorporated</B>.
</DL>
<P>
<HR>
<P>

<a href="../ImageMagick.html"><img alt="[home page]"
                src="../images/home.gif"></a></BODY>
</HTML>

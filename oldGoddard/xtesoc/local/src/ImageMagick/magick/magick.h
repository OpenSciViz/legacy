/*
  Include declarations.
*/
#ifdef __hpux
#define _HPUX_SOURCE  1
#endif
#include <stdio.h>
#if defined(__STDC__) || defined(sgi) || defined(_AIX)
#include <stdlib.h>
#include <unistd.h>
#else
#ifdef vms
#include <stdlib.h>
#else
#include <malloc.h>
#include <memory.h>
#endif
#endif
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <time.h>
#ifndef __MWERKS__
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <SIOUX.h>
#include <console.h>
#include <unix.h>
#include <types.h>
#include <stat.h>
#endif
#undef index
#undef assert

/*
  ImageMagick include declarations.
*/
#if defined(__cplusplus) || defined(c_plusplus)
#define class  c_class
#endif
#include "image.h"
#include "compress.h"
#include "utility.h"
#include "monitor.h"
#include "error.h"
#include "X.h"
#include "widget.h"
#include "PreRvIcccm.h"

/*
  Define declarations.
*/
#define DownShift(x) (((int) ((x)+(1L << 13))) >> 14)
#define False  0
#define Max(x,y)  (((x) > (y)) ? (x) : (y))
#define Min(x,y)  (((x) < (y)) ? (x) : (y))
#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif
#define QuantumTick(i,image) \
  (((i+1) == image->packets) || ((i % image->rows) == 0))
#ifndef STDIN_FILENO
#define STDIN_FILENO  0
#endif
#define True  1
#define UpShift(x) ((int) (x) << 14)
#define UpShifted(x) ((int) ((x)*(1L << 14)+0.5))

/*
  Review these definitions and change them to suit your local requirements.
*/
#define CompressCommand  "|compress -c > %s"
#define DefaultFont  "Helvetica"
#define DefaultPointSize  "18"
#define DocumentationURL  \
  "http://www.wizards.dupont.com/cristy/ImageMagick.html"
#define GunzipCommand  "|gzip -cdfq %s"
#define GzipCommand  "|gzip -cf > %s"
#define DefaultImageQuality  "75"
#define LaunchCommand  "xpaint %s"
#define PICTCommand  "|picttoppm %s"
#define PostscriptColorDevice  "pnmraw"
#define PostscriptMonoDevice  "pbmraw"
#define ReadBinaryType  "rb"
#define WriteBinaryType  "wb"
#define UncompressCommand  "|uncompress -c %s"
#define UndoCache  "16"
#define WWWCommand  "/usr/local/bin/get -q %s:%s > %s"

#ifndef vms
#define ApplicationDefaults  "/usr/lib/X11/app-defaults/"
#define BrowseCommand  "netscape %s &"
#define EditorCommand  "xterm -title \"Edit Image Comment\" -e vi %s"
#define HistogramCommand \
  "display -immutable -window_group 0x%lx -title \"Histogram of %s\" tmp:%s &"
#define PostscriptCommand \
  "gs -sDEVICE=%s -q -dNOPAUSE -dSAFER %s -sOutputFile=%s -- %s < /dev/null > /dev/null"
#define PreferencesDefaults  "~/."
#define PrintCommand  "lp -c %s"
#define RGBColorDatabase  "/usr/lib/X11/rgb.txt"
#else
#define ApplicationDefaults  "decw$system_defaults:"
#define BrowseCommand "spawn/nowait mosaic %s"
#define EditorCommand  "cre/term/wait edit/tpu %s"
#define HistogramCommand \
  "display -immutable -window_group 0x%lx -title \"Histogram of %s\" tmp:%s"
#define PostscriptCommand \
  "gs \"-sDEVICE=%s\" -q \"-dNOPAUSE\" \"-dSAFER\" \"%s\" \"-sOutputFile=%s\" -- \"%s\""
#define PreferencesDefaults  "decw$user_defaults:"
#define PrintCommand  "print/delete %s"
#define RGBColorDatabase  "sys$common:[sysmgr]decw$rgb.dat"
#endif

#ifndef __MWERKS__
#define ReadCommandlLine(argc,argv)
#else
#define HasJPEG
#define HasPNG
#define HasTIFF
#define ReadCommandlLine(argc,argv)  argc=ccommand(argv); puts(Version);
#endif

/*
  Image colorspaces:
*/
#define UndefinedColorspace  0
#define RGBColorspace  1
#define GRAYColorspace 2
#define TransparentColorspace 3
#define OHTAColorspace  4
#define XYZColorspace  5
#define YCbCrColorspace  6
#define YCCColorspace  7
#define YIQColorspace  8
#define YPbPrColorspace  9
#define YUVColorspace  10
/*
  Image compression algorithms:
*/
#define UndefinedCompression  0
#define NoCompression  1
#define RunlengthEncodedCompression  2
#define ZipCompression  3
/*
  Image interlace:
*/
#define UndefinedInterlace  0
#define NoneInterlace  1
#define LineInterlace  2
#define PlaneInterlace  3
#define DefaultInterlace  PlaneInterlace
/*
  Image compositing operations:
*/
#define UndefinedCompositeOp  0
#define OverCompositeOp  1
#define InCompositeOp  2
#define OutCompositeOp  3
#define AtopCompositeOp  4
#define XorCompositeOp  5
#define PlusCompositeOp  6
#define MinusCompositeOp  7
#define AddCompositeOp  8
#define SubtractCompositeOp  9
#define DifferenceCompositeOp  10
#define BumpmapCompositeOp  11
#define ReplaceCompositeOp  12
#define MatteReplaceCompositeOp  13
#define BlendCompositeOp  14
/*
  Image color matching algorithms:
*/
#define PointMethodOp  0
#define ReplaceMethodOp  1
#define FloodfillMethodOp  2
#define ResetMethodOp  3
/*
  Page geometries:
*/
#define PCLPageGeometry  "612x792+43+43"
#define PCLDensityGeometry  "75x75"
#define PSDensityGeometry  "72x72"
#define PSPageGeometry  "612x792+43+43"
#define TextPageGeometry  "612x792+43+43"
/*
  3D effects:
*/
#define HighlightModulate  UpScale(125)
#define ShadowModulate  UpScale(135)
#define DepthModulate  UpScale(185)
#define TroughModulate  UpScale(110)

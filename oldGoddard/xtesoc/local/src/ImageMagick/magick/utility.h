#ifndef vms
#ifndef NoDIRENT
#include <dirent.h>
#else
#include <sys/dir.h>
#ifndef dirent
#define dirent direct
#endif
#endif
#include <pwd.h>
#else
#include "vms.h"
#endif

#ifndef S_ISDIR
#define S_ISDIR(mode) (((mode) & S_IFMT) == S_IFDIR)
#endif

/*
  Utility define declarations.
*/
#ifndef vms
#define IsGlob(text) \
  ((strchr(text,'*') != (char *) NULL) || \
   (strchr(text,'?') != (char *) NULL) || \
   (strchr(text,'{') != (char *) NULL) || \
   (strchr(text,'}') != (char *) NULL) || \
   (strchr(text,'[') != (char *) NULL) || \
   (strchr(text,']') != (char *) NULL))
#define BasenameSeparator  "/"
#define DirectorySeparator  "/"
#define SystemCommand(command)  system(command)
#define TemporaryDirectory  "/tmp"
#define TemporaryTemplate  "%s/magickXXXXXX"
#else
#define IsGlob(text) \
  ((strchr(text,'*') != (char *) NULL) || \
   (strchr(text,'?') != (char *) NULL) || \
   (strchr(text,'{') != (char *) NULL) || \
   (strchr(text,'}') != (char *) NULL))
#define BasenameSeparator  "]"
#define DirectorySeparator  ""
#define SystemCommand(command)  (!system(command))
#define TemporaryDirectory  "SYS$Disk:[]"
#define TemporaryTemplate  "%smagickXXXXXX.tmp"
#endif

/*
  Utilities routines.
*/
extern char
  *ClientName(const char *),
  **ListColors(const char *,int *),
  **ListFiles(char *,const char *,int *),
  *PostscriptGeometry(const char *),
  **StringToList(char *);

extern int
  GlobExpression(char *,const char *),
  ReadDataBlock(char *,FILE *);

extern unsigned int
  IsAccessible(const char *),
  ReadData(char *,const unsigned int,const unsigned int,FILE *);

extern unsigned long
  LSBFirstReadLong(FILE *),
  MSBFirstReadLong(FILE *);

extern unsigned short
  LSBFirstReadShort(FILE *),
  MSBFirstReadShort(FILE *);

extern void
  AppendImageFormat(const char *,char *),
  ExpandFilename(char *),
  ExpandFilenames(int *,char ***),
  LocaleFilename(char *),
  LSBFirstWriteLong(const unsigned long,FILE *),
  LSBFirstWriteShort(const unsigned int,FILE *),
  MSBFirstOrderLong(char *,const unsigned int),
  MSBFirstOrderShort(char *,const unsigned int),
  MSBFirstWriteLong(const unsigned long,FILE *),
  MSBFirstWriteShort(const unsigned int,FILE *),
  TemporaryFilename(char *);

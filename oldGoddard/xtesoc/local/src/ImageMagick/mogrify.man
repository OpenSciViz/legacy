.ad l
.nh
.TH mogrify 1 "1 May 1994" "ImageMagick"
.SH NAME
mogrify - transform an image or sequence of images
.SH SYNOPSIS
.B "mogrify" [ \fIoptions\fP ...] \fIfile\fP
[ [ \fIoptions\fP ...] \fIfile\fP ...]
.SH DESCRIPTION
\fBmogrify\fP transforms an image or a sequence of images.  These
transforms include image scaling, image rotation, color reduction, and
others.  The transmogrified image overwrites the original image.
.SH EXAMPLES
To convert all the TIFF files in a particular directory to JPEG, use:
.PP
.B
    mogrify -format jpeg *.tiff
.PP
To scale an image of a cockatoo to exactly 640 pixels in width and 480
pixels in height, use:
.PP
.B
    mogrify -geometry 640x480! cockatoo.miff
.PP
To create a single colormap for a sequence of bird images, use:
.PP
.B
    mogrify -colors 256 scenes/birds.*
.SH OPTIONS
.TP
.B "-blur \fIfactor\fP"
blurs an image.  Specify \fIfactor\fP as the percent enhancement
(0.0 - 99.9%).
.TP
.B "-border \fI<width>x<height>\fP"
surround the image with a border or color.  See \fBX(1)\fP for details
about the geometry specification.

The color of the border is obtained from the X server and is defined as
\fBbordercolor\fP (class \fBborderColor\fP).  See \fBX(1)\fP for details.
.TP
.B "-box \fIcolor\fP"
set the color of the annotation bounding box.  See \fB-draw\fP or
for further details.
 
See \fBX(1)\fP for details about the color specification.
.TP
.B "-colors \fIvalue\fP"
preferred number of colors in the image.

The actual number of colors in the image may be less than your request,
but never more.  Note, this is a color reduction option.  Images with
less unique colors than specified with this option will remain unchanged.
Refer to \fBquantize(9)\fP for more details.

If more than one image is specified on the command line, a single
colormap is created and saved with each image.

Note, options \fB-colormap\fP, \fB-dither\fP, \fB-colorspace\fP, and
\fB-treedepth\fP affect the color reduction algorithm.
.TP
.B "-colorspace \fIvalue\fP"
the type of colorspace: \fBGRAY\fP, \fBOHTA\fP, \fBRGB\fP,
\fBTransparent\fP, \fBXYZ\fP, \fBYCbCr\fP, \fBYIQ\fP, \fBYPbPr\fP, or
\fBYUV\fP.

Color reduction, by default, takes place in the RGB color space.
Empirical evidence suggests that distances in color spaces such as YUV
or YIQ correspond to perceptual color differences more closely
than do distances in RGB space.  These color spaces may give better
results when color reducing an image.  Refer to \fBquantize(9)\fP for
more details.
 
The \fBTransparent\fP color space behaves uniquely in that it perserves
the matte channel of the image if it exists.

The \fB-colors\fP or \fB-monochrome\fP option is required
for this option to take effect.
.TP
.B "-comment \fIstring\fP"
annotate an image with a comment.

By default, each image is commented with its file name.  Use this
option to assign a specific comment to the image.  Optionally you can
include the image filename, type, width, height, or scene number by
embedding special format characters.  Embed \fB%f\fP for filename,
\fB%m\fP for magick, \fB%w\fP for width, \fB%h\fP for height, \fB%s\fP
for scene number, \fB%b\fP for file size in kilobytes, or \fB\\n\fP for
newline.  For example,

.nf
     -comment "%m:%f %wx%h"
.fi

produces an image comment of \fBMIFF:bird.miff 512x480\fP for an image
titled \fBbird.miff\fP and whose width is 512 and height is 480.

If the first character of \fIstring\fP is \fB@\fP, the image comment is read
from a file titled by the remaining characters in the string.
.TP
.B "-compress \fItype\fP"
the type of image compression: \fIZlib\fP or \fIRunlengthEncoded\fP.

Specify \fB\+compress\fP to store the binary image in an uncompressed format.
The default is the compression type of the specified image file.
.TP
.B "-contrast"
enhance or reduce the image contrast.

This option enhances the intensity differences between the
lighter and darker elements of the image.  Use \fB-contrast\fP to
enhance the image or \fB+contrast\fP to reduce the image contrast.
.TP
.B "-crop \fI<width>{%}x<height>{%}{\+-}<x offset>{\+-}<y offset>\fP"
preferred size and location of the cropped image.  See \fBX(1)\fP for details
about the geometry specification.

To specify a percentage width or height instead, append \fB%\fP.  For example
to crop the image by ten percent on all sides of the image, use \fB-crop 10%\fP.

Use cropping to apply image processing options, or transmogrify, only a
particular area of an image.   Use \fB-crop 0x0\fP to remove edges that are
the background color.   Omit the x and y offset
to generate one or more subimages of a uniform size.
.TP
.B "-density \fI<width>x<height>\fP
vertical and horizontal resolution in pixels of the image.

This option specifies an image density when decoding a Postscript or
Portable Document page.  The default is 72 pixels per inch in the horizontal
and vertical direction.
.TP
.B "-despeckle"
reduce the speckles within an image.
.TP
.B "-display \fIhost:display[.screen]\fP"
specifies the X server to contact; see \fBX(1)\fP.
.TP
.B "-dispose \fImethod\fP"
GIF disposal method.

Graphics Interchange Format (GIF) Specification 89a of July 31, 1990 for
details.
.TP
.B "-dither"
apply Floyd/Steinberg error diffusion to the image.

The basic strategy of dithering is to trade intensity resolution for
spatial resolution by averaging the intensities of several neighboring
pixels.  Images which suffer from severe contouring when reducing colors
can be improved with this option.

The \fB-colors\fP or \fB-monochrome\fP option is required for this option
to take effect.
.TP
.B "-draw \fIstring\fP"
annotate an image with one or more graphic primitives.

Use this option to annotate an image with one or more graphic primitives.
The primitives include

.nf
  rectangle
  circle
  polygon
  color
  matte
  text
  image
.fi

\fBRectangle\fP, \fBtext\fP, and \fBimage\fP require an upper left and lower
right coordinate.  \fBCircle\fP requires the center coordinate and a
coordinate on the outer edge.  Finally, \fBpolygon\fP requires three or more
coordinates defining its boundaries.  Coordinates are integers separated by
an optional comma.  For example, to define a circle centered at 100,100 that
extends to 150,150 use:

.nf
  -draw 'circle 100,100 150,150'
.fi

Use \fBcolor\fP to change the color of a pixel.  Follow the
pixel coordinate with a method:

.nf
  point
  replace
  floodfill
  reset
.fi

Consider the target pixel as that specified by your coordinate.  The
\fBpoint\fP method recolors the target pixel.  The \fBreplace\fP method
recolors any pixel that matches the color of the target pixel.
\fBFloodfill\fP recolors any pixel that matches the color of the target
pixel and is a neighbor.  Finally, \fBreset\fP recolors all pixels.

Use \fBmatte\fP to the change the pixel matte value to transparent.
Follow the pixel coordinate with a method (see the \fBcolor\fB
primitive for a description of methods).  The \fBpoint\fP method
changes the matte value of the target pixel.  The \fBreplace\fP method
changes the matte value of any pixel that matches the color of the
target pixel.  \fBFloodfill\fP changes the matte value of any pixel
that matches the color of the target pixel and is a neighbor. Finally
\fBreset\fP changes the matte value of all pixels.

Use \fBtext\fP to annotate an image with text.  Follow the
text coordinates with a string.  If the string has embedded spaces,
enclose it in double quotes.  Optionally you can
include the image filename, type, width, height, or scene number by
embedding special format characters.  Embed \fB%f\fP for filename,
\fB%m\fP for magick, \fB%w\fP for width, \fB%h\fP for height, \fB%s\fP
for scene number, or \fB\\n\fP for newline.  For example,

.nf
     -draw 'text 100,100 "%m:%f %wx%h"'
.fi

annotates the image with \fBMIFF:bird.miff 512x480\fP for an image
titled \fBbird.miff\fP and whose width is 512 and height is 480.

If the first character of the string is \fB@\fP, the text is read
from a file titled by the remaining characters in the string.

Use \fBimage\fP to composite an image with another image.  Follow the
image coordinates with the filename of an image.

If the first character of \fIstring\fP is \fB@\fP, the text is read
from a file titled by the remaining characters in the string.

You can set the primitive color, font color, and font bounding box color with
\fB-pen\fP, \fB-font\fP, and \fB-box\fP respectively.  Options are
processed in command line order so be sure to use \fB-pen\fP
\fIbefore\fP the \fB-draw\fP option.
.TP
.B "-edge \fIfactor\fP"
detect edges with an image.  Specify \fIfactor\fP as the percent enhancement
(0.0 - 99.9%).
.TP
.B "-emboss"
emboss the image.
.TP
.B "-enhance"
apply a digital filter to enhance a noisy image.
.TP
.B "-equalize"
perform histogram equalization to the image.
.TP
.B "-flip"
create a "mirror image" by reflecting the image scanlines in the vertical
direction.
.TP
.B "-flop"
create a "mirror image" by reflecting the image scanlines in the horizontal
direction.
.TP
.B "-format \fItype\fP"
the image format type.

This option will convert any image to the image format you specify.
See \fBconvert(1)\fP for a list of image format types supported by
\fBImageMagick\fP.

By default the file is written to its original name.  However, if the
filename extension matches a supported format, the extension is replaced
with the image format type specified with \fp-format\fP.  For example,
if you specify \fItiff\fP as the format type and the input image
filename is \fIimage.gif\fP, the output image filename becomes
\fIimage.tiff\fP.
.TP
.B "-font \fIname\fP"
use this font when annotating the image with text.

\fBMogrify\fP contacts an X server to obtain the font.  If an X server is not
available, a Postscript font is used.  You can set the pointsize with
\fB-pointsize\fP.
.TP
.B "-frame \fI<width>x<height>+<outer bevel width>+<inner bevel width>\fP"
surround the image with an an ornamental border.  See \fBX(1)\fP for details
about the geometry specification.

The color of the border is specified with the \fB-mattecolor\fP command line
option.
.TP
.B "-gamma \fIvalue\fP"
level of gamma correction.

The same color image displayed on two different workstations may look
different due to differences in the display monitor.  Use gamma
correction to adjust for this color difference.  Reasonable values
extend from 0.8 to 2.3.

You can apply separate gamma values to the red, green, and blue
channels of the image with a gamma value list delineated with commas
(i.e. 1.7,2.3,1.2).
.TP
.B "-geometry \fI<width>{%}x<height>{%}{!}{<}{>}"
preferred width and height of the image.  See \fBX(1)\fP for details
about the geometry specification.

By default, the width and height are maximum values.  That is, the
image is expanded or contracted to fit the width and height value while
maintaining the aspect ratio of the image.  Append an exclamation point
to the geometry to force the image size to exactly the size you
specify.  For example, if you specify \fB640x480!\fP the image width is
set to 640 pixels and height to 480.  If only one factor is
specified, both the width and height assume the value.

To specify a percentage width or height instead, append \fB%\fP.  The
image size is multiplied by the width and height percentages to obtain
the final image dimensions.  To increase the size of an image, use a
value greater than 100 (e.g. 125%).  To decrease an image's size, use a
percentage less than 100.

Use \fB<\fP to change the dimensions of the image \fIonly\fP
if its size exceeds the geometry specification.  \fB>\fP resizes
the image \fIonly\fP if its dimensions is less than the geometry
specification.  For example, if you specify \fB640x480>\fP and the
image size is 512x512, the image size does not change.  However, if
the image is 1024x1024, it is resized to 640x480.
.TP
.B "-implode \fIfactor\fP"
implode image pixels about the center. Specify \fIfactor\fP as the percent
implosion (0 - 99.9 %) or explosion (-99.9 - 0)
.TP
.B "-interlace \fItype\fP"
the type of interlacing scheme: \fBNONE\fP, \fBLINE\fP, or \fBPLANE\fP.

This option is used to specify the type of interlacing scheme for raw
image formats such as \fBRGB\fP or \fBYUV\fP.  \fBNONE\fP means do not
interlace (RGBRGBRGBRGBRGBRGB...), \fBLINE\fP uses scanline
interlacing (RRR...GGG...BBB...RRR...GGG...BBB...), and \fBPLANE\fP uses
plane interlacing (RRRRRR...GGGGGG...BBBBBB...).

Use \fBLINE\fP, or \fBPLANE\fP to create an interlaced GIF or progressive
JPEG image.
.B "-label \fIname\fP"
assign a label to an image.

Use this option to assign a specific label to the image.  Optionally
you can include the image filename, type, width, height, or scene
number in the label by embedding special format characters.   Embed
\fB%f\fP for filename, \fB%m\fP for magick, \fB%w\fP for width,
\fB%h\fP for height, or \fB%s\fP for scene number, or \fB\\n\fP for newline.
For example,
.nf
     -label "%m:%f %wx%h"
.fi
produces an image label of \fBMIFF:bird.miff 512x480\fP for an image
titled \fBbird.miff\fP and whose width is 512 and height is 480.

If the first character of \fIstring\fP is \fB@\fP, the image label is read
from a file titled by the remaining characters in the string.

When converting to Postscript, use this option to specify a header string
to print above the image.
.TP
.B "-map \fIfilename\fP"
choose a particular set of colors from this image.

By default, color reduction chooses an optimal set of colors that
best represent the original image.  Alternatively, you can choose a
particular set of colors with this option.  This is useful when
you want to create a sequence of images with one particular set of
colors for each image.
.TP
.B "-modulate \fIvalue\fP"
vary the brightness, saturation, and hue of an image.

Specify the percent change in brightness, the color saturation, and the hue
separated by commas.  For example, to increase the color brightness
by 20% and decrease the color saturation by 10% and leave the hue
unchanged, use: \fB-modulate 20,-10\fP.
.TP
.B "-monochrome"
transform the image to black and white.
.TP
.B "-negate"
apply color inversion to image.

The red, green, and blue intensities of an image are negated.
.TP
.B "-noise"
reduce the noise in an image with a noise peak elimination filter.

The principal function of noise peak elimination filter is to smooth
the objects within an image without losing edge information and without
creating undesired structures.  The central idea of the algorithm is to
replace a pixel with its next neighbor in value within a 3 x 3 window,
if this pixel has been found to be noise.  A pixel is defined as noise
if and only if this pixel is a maximum or minimum within the 3 x 3 window.
.TP
.B "-normalize"
transform image to span the full range of color values.

This is a contrast enhancement technique.
.TP
.B "-opaque \fIcolor\fP"
change this color to the pen color within the image.  See \fB-pen\fP for
more details.
.TP
.B "-page \fI<width>x<height>{\+-}<x offset>{\+-}<y offset>\fP"
preferred size and location of the Postscript page.

Use this option to specify the dimensions of the Postscript page in
pixels per inch or a TEXT page in pixels.  The default for a Postscript page is
to center the image on a letter page 612 by 792 pixels. The
margins are 1/2" (i.e.  612x792+42+42).  Other common sizes are:

    Letter      612x 792
    Tabloid     792x1224
    Ledger     1224x 792
    Legal       612x1008
    Statement   396x 612
    Executive   540x 720
    A3          842x1190
    A4          595x 842
    A5          420x 595
    B4          729x1032
    B5          516x 729
    Folio       612x 936
    Quarto      610x 780
    10x14       720x1008
.PP
For convenience you can specify the page size by media (e.g.
A4, Ledger, etc.).
.PP
To place a Postscript image with a given size on a given location on a
page, use -page +HOFFSET+VOFFSET -geometry WIDTHxHEIGHT (fill in
numbers). Note: this is only for generating Postscript, not Encapsulated
Postscript.
.PP
To position a GIF image, use -page +LEFT+TOP (e.g. -page +100+200).
.PP
The default page dimensions for a TEXT image is 612x792.
.TP
.B "-paint \fIradius\fP"
paint the image.

Each pixel is replaced by the most frequent color in a circular neighborhood
whose width is specified with \fIradius\fP.
.TP
.B "-pen \fIcolor\fP"
set the color of the font or opaque color.  See \fB-annotate\fP or
\fB-draw\fP for further details.
 
See \fBX(1)\fP for details about the color specification.
.TP
.B "-pointsize \fIvalue\fP"
pointsize of the Postscript font.
.TP
.B "-quality \fIvalue\fP"
JPEG quality setting.

Quality is 0 (worst) to 100 (best). The default is 75.
.TP
.B "-raise \fI<bevel width>\fP"
lighten or darken image edges to create a 3-D effect.
 
\fIBevel width\fP is the width of an edge.  Use \fB-raise\fP to create a
raised effect, otherwise use \fB+raise\fP.
.TP
.B "-region \fI<width>x<height>{\+-}<x offset>{\+-}<y offset>\fP"
apply options to a portion of the image.
 
By default, any command line options are applied to the entire image.  Use
\fB-region\fP to restrict operations to a particular area of the image.
.TP
.B "-roll \fI{\+-}<x offset>{\+-}<y offset>\fP"
roll an image vertically or horizontally.  See \fBX(1)\fP for details
about the geometry specification.

A negative \fIx offset\fP rolls the image left-to-right.  A negative
\fIy offset\fP rolls the image top-to-bottom.
.TP
.B "-rotate \fIdegrees\fP"
apply Paeth image rotation to the image.

Empty triangles left over from rotating the image are filled with
the color defined as \fBbordercolor\fP (class \fBborderColor\fP).
See \fBX(1)\fP for details.
.TP
.B "-sample \fIgeometry\fP"
scale image with pixel sampling.
.TP
.B "-scene \fIvalue\fP"
image scene number.
.TP
.B "-segment \fIvalue\fP"
eliminate clusters that are insignificant.

The number of pixels in each cluster must exceed the
the cluster threshold to be considered valid.
See \fBIMAGE SEGMENTATION\fB for details.
.TP
.B "-sharpen \fIfactor\fP"
sharpen an image.  Specify \fIfactor\fP as the percent enhancement
(0.0 - 99.9%).
.TP
.B "-shear \fI<x degrees>x<y degrees>\fP"
shear the image along the X or Y axis by a positive or negative shear angle.

Shearing slides one edge of an image along the X or Y axis, creating a
parallelogram.  An X direction shear slides an edge along the X axis,
while a Y direction shear slides an edge along the Y axis.  The amount
of the shear is controlled by a shear angle.  For X direction shears,
\fIx degrees>\fP is measured relative to the Y axis, and similarly, for
Y direction shears \fIy degrees\fP is measured relative to the X axis.

Empty triangles left over from shearing the image are filled with
the color defined as \fBbordercolor\fP (class \fBborderColor\fP).
See \fBX(1)\fP for details.
.TP
.B "-size \fI<width>{%}x<height>{%}+<offset>\fP"
width and height of the image.

Use this option to specify the width and height of raw images whose
dimensions are unknown such as \fBGRAY\fP, \fBRGB\fP, or \fBCMYK\fP.
In addition to width and height, use \fB-size\fP to skip any header
information in the image or tell the number of colors in a \fBMAP\fP
image file, (e.g. -size 640x512+256).
 
For Photo CD images, choose from these sizes:
.PP
     192x128
     384x256
     768x512
    1536x1024
    3072x2048
 
Finally, use this option to choose a particular resolution layer of a JBIG
image (e.g. -size 1024x768).
.TP
.B "-solarize \fIthreshold\fP"
negate all pixels above the threshold level.    Specify \fIfactor\fP as the
percent threshold of the intensity (0 - 99.9%).

This option produces a \fBsolarization\fB effect seen when exposing
a photographic film to light during the development process.
.TP
.B "-spread \fIamount\fP"
displace image pixels by a random amount.
 
\fIAmount\fP defines the size of the neighborhood around each pixel to
choose a candidate pixel to swap.
.TP
.B "-swirl \fIdegrees\fP"
swirl image pixels about the center.
 
\fIDegrees\fP defines the tightness of the swirl.
.TP
.B "-texture \fIfilename\fP"
name of texture to tile onto the image background.
.TP
.B "-transparency \fIcolor\fP"
make this color transparent within the image.
.TP
.B "-treedepth \fIvalue\fP"
Normally, this integer value is zero or one.  A zero or one tells
\fBmogrify\fP to choose a optimal tree depth for the color reduction
algorithm.

An optimal depth generally allows the best representation of the source
image with the fastest computational speed and the least amount of
memory.  However, the default depth is inappropriate for some images.
To assure the best representation, try values between 2 and 8 for this
parameter.  Refer to \fBquantize(9)\fP for more details.

The \fB-colors\fP or \fB-monochrome\fP option is required for this option
to take effect.
.TP
.B "-undercolor \fI<undercolor factor>x<black-generation factor>\fP"
control undercolor removal and black generation on CMYK images.

This option enables you to perform undercolor removal and black
generation on CMYK images-- images to be printed on a four-color
printing system. You can control how much cyan, magenta, and yellow
to remove from your image and how much black to add to it.
The standard undercolor removal is \fB1.0x1.0\fP.  You'll
frequently get better results, though, if the percentage of black you
add to your image is slightly higher than the percentage of C, M, and Y
you remove from it.  For example you might try \fB0.5x0.7\fP.
.TP
.B -verbose
print detailed information about the image.

This information is printed: image scene number;  image name;  image
size; the image class (\fIDirectClass\fP or \fIPseudoClass\fP); the total
number of unique colors (if known);  and the number of seconds to read and
transform the image.  Refer to \fBmiff(5)\fP for a description of
the image class.

If \fB-colors\fP is also specified, the total unique colors in the image
and color reduction error values are printed.  Refer to \fBquantize(9)\fP
for a description of these values.
.PP
Options are processed in command line order.
Any option you specify on the command line remains in effect until it is
explicitly changed by specifying the option again with a different effect.
For example, to mogrify two images, the first with 32 colors and the
second with only 16 colors, use:
.PP
     mogrify -colors 32 cockatoo.miff -colors 16 macaw.miff
.PP
Change \fI-\fP to \fI\+\fP in any option above to reverse its effect.
For example, specify \fB\+compress\fP to store the binary image in an
uncompressed format.
.PP
By default, the image format is determined by its magic number. To
specify a particular image format, precede the filename with an image
format name and a colon (i.e. ps:image) or specify the image type as
the filename suffix (i.e. image.ps).  See \fBconvert(1)\fP for a list
of valid image formats.
.PP
Specify \fIfile\fP as \fI-\fP for standard input and output.  If
\fIfile\fP has the extension \fB.Z\fP or \fB.gz\fP, the file is
uncompressed with \fBuncompress\fP or \fBgunzip\fP respectively and
subsequently compressed using with \fBcompress\fP or \fBgzip\fP.
Finally, precede the image file name with \fI|\fP to pipe to or from a
system command.
.PP
Use an optional index enclosed in brackets after a file name to specify
a desired subimage of a multi-resolution image format like Photo CD
(e.g. img0001.pcd[4]) or a range for MPEG images (e.g. video.mpg[50-75]).
For raw images, specify a subimage with a geometry (e.g.  -size 640x512
image.rgb[320x256+50+50]).
.SH IMAGE SEGMENTATION
Use \fB-segment\fP to segment an image by analyzing the histograms of the color
components and identifying units that are homogeneous with the fuzzy c-means
technique.  The scale-space filter analyzes the histograms of the three
color components of the image and identifies a set of classes.  The
extents of each class is used to coarsely segment the image with
thresholding.  The color associated with each class is determined by
the mean color of all pixels within the extents of a particular class.
Finally, any unclassified pixels are assigned to the closest class with
the fuzzy c-means technique.
.PP
The fuzzy c-Means algorithm can be summarized as follows:
.RS
.LP
o Build a histogram, one for each color component of the image.
.LP
o For each histogram, successively apply the scale-space filter and
build an interval tree of zero crossings in the second derivative at
each scale.  Analyze this scale-space ``fingerprint'' to determine
which peaks or valleys in the histogram are most predominant.
.LP
o The fingerprint defines intervals on the axis of the histogram.  Each
interval contains either a minima or a maxima in the original signal.
If each color component lies within the maxima interval, that pixel is
considered ``classified'' and is assigned an unique class number.
.LP
o Any pixel that fails to be classified in the above thresholding pass is
classified using the fuzzy c-Means technique.  It is assigned to one
of the classes discovered in the histogram analysis phase.
.RE
.PP
The fuzzy c-Means technique attempts to cluster a pixel by finding the local
minima of the generalized within group sum of squared error objective
function.  A pixel is assigned to the closest class of which the fuzzy
membership has a maximum value.

For additional information see
.IP
Young Won Lim, Sang Uk Lee, "On The Color Image Segmentation Algorithm Based
on the Thresholding and the Fuzzy c-Means Techniques", Pattern Recognition,
Volume 23, Number 9, pages 935-952, 1990.
.SH SEE ALSO
.B
display(1), animate(1), import(1), montage(1), convert(1),
combine(1), xtp(1)
.SH COPYRIGHT
Copyright 1996 E. I. du Pont de Nemours and Company
.PP
Permission to use, copy, modify, distribute, and sell this software and
its documentation for any purpose is hereby granted without fee,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of E. I. du Pont de Nemours
and Company not be used in advertising or publicity pertaining to
distribution of the software without specific, written prior
permission.  E. I. du Pont de Nemours and Company makes no representations
about the suitability of this software for any purpose.  It is provided
"as is" without express or implied warranty.
.PP
E. I. du Pont de Nemours and Company disclaims all warranties with regard
to this software, including all implied warranties of merchantability
and fitness, in no event shall E. I. du Pont de Nemours and Company be
liable for any special, indirect or consequential damages or any
damages whatsoever resulting from loss of use, data or profits, whether
in an action of contract, negligence or other tortuous action, arising
out of or in connection with the use or performance of this software.
.SH ACKNOWLEDGEMENTS
Michael Halle, Spatial Imaging Group at MIT, for the initial
implementation of Alan Paeth's image rotation algorithm.
.PP
David Pensak, E. I. du Pont de Nemours and Company, for providing a
computing environment that made this program possible.
.PP
Paul Raveling, USC Information Sciences Institute, for the original
idea of using space subdivision for the color reduction algorithm.
.SH AUTHORS
John Cristy, E.I. du Pont de Nemours and Company Incorporated

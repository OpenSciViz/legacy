/*
  State declarations.
*/
#define AutoReverseAnimationState 0x0001
#define DefaultState  0x0000
#define ExitState  0x0002
#define ForwardAnimationState 0x0004
#define HighlightState  0x0008
#define PlayAnimationState 0x0010
#define RepeatAnimationState 0x0020
#define StepAnimationState 0x0040

/*
  Static declarations.
*/
static char
  *ImageMagickHelp[]=
  {
    "BUTTONS",
    "",
    "  Press any button to map or unmap the Command widget.",
    "",
    "COMMAND WIDGET",
    "  The Command widget lists a number of sub-menus and commands.",
    "  They are",
    "",
    "    Animate",
    "      Play",
    "      Step",
    "      Repeat",
    "      Auto Reverse",
    "    Speed",
    "      Slower",
    "      Faster",
    "    Direction",
    "      Forward",
    "      Reverse",
    "    Image Info",
    "    Help",
    "    Quit",
    "",
    "  Menu items with a indented triangle have a sub-menu.  They",
    "  are represented above as the indented items.  To access a",
    "  sub-menu item, move the pointer to the appropriate menu and",
    "  press a button and drag.  When you find the desired sub-menu",
    "  item, release the button and the command is executed.  Move",
    "  the pointer away from the sub-menu if you decide not to",
    "  execute a particular command.",
    "",
    "KEYBOARD ACCELERATORS",
    "  p    Press to animate the sequence of images.",
    "",
    "  s    Press to display the next image in the sequence.",
    "",
    "  .    Press to continually display the sequence of images.",
    "",
    "  a    Press to automatically reverse the sequence of images.",
    "",
    "  <    Press to slow the display of the images.  Refer to",
    "       -delay for more information.",
    "",
    "  >    Press to speed-up the display of the images.  Refer to",
    "       -delay for more information.",
    "",
    "  f    Press to animate in the forward direction.",
    "",
    "  r    Press to animate in the reverse direction.",
    "",
    "  i    Press to display information about the image.  Press",
    "       any key or button to erase the information.",
    "",
    "       This information is printed: image name;  image size;",
    "       and the total number of unique colors in the image.",
    "",
    "  h    Press to display helpful information about animate(1).",
    "",
    "       Function keys HELP or F1 are synonymous with the h key.",
    "",
    "  q    Press to discard all images and exit program.",
    (char *) NULL
  };

/*
  Function prototypes.
*/
static int
  SceneCompare(const void *,const void *);

static void
  XAnimateBackgroundImage (Display *,XResourceInfo *,Image **,
    const unsigned int),
  XAnimateImage(Display *,XResourceInfo *,char **,const int,Image **,
    const unsigned int),
  XMagickCommand(Display *,XResourceInfo *,XWindows *,const KeySym,Image **,
    unsigned int *),
  Usage(const unsigned int);

/*
  Include declarations
*/
#include <stdio.h>
#if defined(__STDC__) || defined(sgi) || defined(_AIX)
#include <stdlib.h>
#else
#ifdef vms
#include <stdlib.h>
#else
#include <malloc.h>
#include <memory.h>
#endif
#endif
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#undef index
#undef assert

/*
  Define declarations for the Display program.
*/
#if __STDC__ || defined(sgi) || defined(_AIX)
#define _Declare(formal_parameters) formal_parameters
#else
#define const 
#define _Declare(formal_parameters) ()
#endif
#define False  0
#define IsGlob(text) \
  ((strchr(text,'*') != (char *) NULL) || \
   (strchr(text,'?') != (char *) NULL) || \
   (strchr(text,'{') != (char *) NULL) || \
   (strchr(text,'}') != (char *) NULL))
#define Max(x,y)  (((x) > (y)) ? (x) : (y))
#define MaxTextLength  2048
#define True  1
#define Warning(message,qualifier)  \
{  \
  (void) fprintf(stderr,"%s: %s",client_name,message);  \
  if (qualifier != (char *) NULL)  \
    (void) fprintf(stderr," (%s)",qualifier);  \
  (void) fprintf(stderr,".\n");  \
}

#ifndef lint
static char 
  Version[]="@(#)ImageMagick 2.1 92/10/10 cristy@dupont.com";
#endif

.ad l
.nh
.TH XTP 1 "10 December 1994"
.SH NAME
xtp - file transfer program
.SH SYNOPSIS
.B "xtp"
[ \fI-options\fP ... ] \fI<uniform resource locator>\fP
.SH DESCRIPTION
.PP
.I Xtp
is a utility for retrieving, listing, or printing files from a remote
network site, or sending files to a remote network site.
.I xtp
performs most of the same functions as the \fBftp(1)\fP program, but does
not require any interactive commands.  You simply specify the file transfer
task on the command line and \fBxtp\fP performs the task automatically.
.SH EXAMPLES
.PP
To retrieve the file \fBbird.jpg\fP in directory \fBimages\fP from  host
\fBwizard.dupont.com\fP, use:
.PP
.B
     xtp ftp://wizard.dupont.com/images/bird.jpg
.PP
To retrieve all the files from directory \fBimages\fP as user
\fBcristy\fP from host \fBwizard.dupont.com\fP, use:
.PP
     xtp -retrieve ftp://cristy@wizard.dupont.com/images
.PP
You will be prompted for a password.
.PP
To retrieve all the files from directory \fBimages\fP as user
\fBcristy\fP and password \fBmagick\fP from host \fBwizard.dupont.com\fP, use:
.PP
     xtp -retrieve ftp://cristy:magick@wizard.dupont.com/images
.SH OPTIONS
.TP
.B "-account \fIpassword\fP"
Supply a supplemental password required by a remote system for access
to resources.
.TP
.B "-binary"
retrieve files as binary.  This is the default.  Use \fB+binary\fP to
retrieve files as text.
.TP
.B "-directory 
list the names of files and their attributes that match the filename
component of the uniform resource locator.  The filename component is
processed as a regular expression.
.TP
.B "-exclude \fIexpression\fP
exclude files that match the regular \fIexpression\fP.

This option applies to the \fB-directory\fP, \fB-print\fP, or
\fB-retrieve\fP options.
.TP
.B "-file \fIname\fP"
store the file with this name.

Refer to the \fB-get\fP and \fB-put\fP option for more details.
.TP
.B "-get 
get files that match the filename component of the uniform resource locator.
The filename component is expanded by passing it to \fBcsh(1)\fP.

This option is equivalent to using the \fBftp get\fP command.  However,
if the filename contains globbing characters this option is equivalent
to the \fBftp mget\fP command.  Without globbing characters,
you can store the file locally with a different name by using the
\fB-file\fP option.
.TP
.B "-port \fInumber\fP"
If no port number is specified, \fBxtp\fP attempts to contact a FTP server
at the default port.  Otherwise, the specified port number is used.
.TP
.B "-proxy \fIhostname\fP"
access the remote host via a proxy \fBftpd\fP client running on this host.

The default value of this option can be set with the environment variable
\fBxtp_proxy\fP.  See \fBENVIRONMENT\fP for more details.  Use \fB+proxy\fP
to prevent proxy connections.
.TP
.B "-print
print files that match the filename component of the uniform resource locator.
The filename component is processed as a regular expression.
.TP
.B "-prune"
process files in the remote directory specified by the directory component
of the uniform resource locator.  Do not recursively search for files.
.TP
.B "-put 
put files that match the filename component of the uniform resource locator.
The filename component is expanded by passing it to \fBcsh(1)\fP.

This option is equivalent to using the \fBftp put\fP command.  However,
if the filename contains globbing characters this option is equivalent
to the \fBftp mput\fP command.  Without globbing characters,
you can store the file remotely with a different name by using the
\fB-file\fP option.
.TP
.B "-retrieve 
retrieve files that match the filename component of the uniform resource
locator.  The filename component is processed as a regular expression.

Retrieved files are stored on your local host directory as the full
name of the retrieved file.  For example, if the retrieved file is
named \fBdocuments/xtp.man\fP on the remote FTP server, it will appear
in your remote directory as \fBdocuments/xtp.man\fP.
.TP
.B "-timeout \fIseconds\fP"
specifies the maximum seconds to complete your remote FTP server request.
If this time expires, the program terminates.  The program also terminates if
one tenth of this value is exceeded while logging onto the remote FTP
server.
.TP
.TP
.B "-type \fIname\fP"
identify the remote system type: UNIX, VMS, or other.

The system type is determined automatically, however, you can override
the system type with this option.
.TP
.B "-verbose"
show all responses from the remote server.
.PP
If only the program name is specified on the command line, the program command
syntax and options are listed.
.PP
If neither \fB-directory\fP, \fB-print\fP, \fB-put\fP, or \fB-retrieve\fP
are specified on the command line, the file or files specified by the
uniform resource locator is retrieved from the remote network host (as if
\fB-get\fP was specified).
.PP
The uniform resource locator is as described in the World Wide Web
specification with one exception.  The filename part of the locator is
expanded with \fBcsh(1)\fP for options \fB-get\fP or \fB-put\fP otherwise
it is processed as a regular expression (described in the next
section).  For convenience, the protocol component of the uniform resource
locator (\fBftp://\fP) may be omitted.
.PP
\fBXtp\fP retrieves files from the remote directory for \fB-get\fP and
puts files in the remote directory for \fB-put\fP.  Otherwise, \fBxtp\fP
looks for a file of the form \fBls-lls-l([Rt])+([Rt])*\fP and assumes it
contains a recursive directory listing.  If none is found, \fBxtp\fP
recursively descends the directory hierarchy from the remote directory.
Some remote hosts may have thousands of files causing a significant
delay satisfying your request.  This can be wasteful if the files you
are interested in reside in a known directory.  You can reduce the
searching required by specifying \fI<remote directory>\fP on the command
line.  This limits the filename search to the specified directory and
any of its subdirectories.  Alternatively, \fB-prune\fP restricts the
search to the remote directory only.
.SH REGULAR EXPRESSIONS
A \fIregular expression\fP is zero or more branches, separated by
\fB|\fP.  It matches anything that matches one of the branches.
.PP
A branch is zero or more pieces, concatenated.  It matches a match for
the first, followed by a match for the second, etc.
.PP
A piece is an atom possibly followed by \fB*\fP, \fB+\fP, or \fB?\fP.
An atom followed by \fB*\fP matches a sequence of 0 or more matches of
the atom.  An atom followed by \fB+\fP matches a sequence of 1 or more
matches of the atom.  An atom followed by \fB?\fP matches a match of
the atom, or the null pattern.
.PP
An atom is a \fIregular expression\fP in parentheses (matching a match
for the \fIregular expression\fP), a range (see below), \fB.\fP
(matching any single character), \fB^\fP (matching the null pattern at
the beginning of the input pattern), \fB$\fP (matching the null pattern
at the end of the input pattern), a \fB\'\fP followed by a single
character (matching that character), or a single character with no
other significance (matching that character).
.PP
A range is a sequence of characters enclosed in \fB[]\fP.  It normally
matches any single character from the sequence.  If the sequence begins
with \fB^\fP, it matches any single character not from the rest of the
sequence.  If two characters in the sequence are separated by \fB-\fP,
this is shorthand for the full list of ASCII characters between them
(e.g.  \fB[0-9]\fP matches any decimal digit). To include a literal
\fB]\fP in the sequence, make it the first character (following a
possible \fB^\fP).  To include a literal \fB-\fP, make it the first or
last character.
.SH ENVIRONMENT
.TP
.B "xtp_proxy"
Specifies that the remote site should be contacted by proxy.  See \fB-proxy\fP.
.SH SEE ALSO
.B
ftp(1C), Mosaic(1)
.SH COPYRIGHT
Copyright 1995 E. I. Dupont de Nemours and Company
.PP
Permission to use, copy, modify, distribute, and sell this software and
its documentation for any purpose is hereby granted without fee,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of E. I. Dupont de Nemours
and Company not be used in advertising or publicity pertaining to
distribution of the software without specific, written prior
permission.  E. I. Dupont de Nemours and Company makes no representations
about the suitability of this software for any purpose.  It is provided
"as is" without express or implied warranty.
.PP
E. I. Dupont de Nemours and Company disclaims all warranties with regard
to this software, including all implied warranties of merchantability
and fitness, in no event shall E. I. Dupont de Nemours and Company be
liable for any special, indirect or consequential damages or any
damages whatsoever resulting from loss of use, data or profits, whether
in an action of contract, negligence or other tortious action, arising
out of or in connection with the use or performance of this software.
.SH ACKNOWLEDGEMENTS
Steve Singles, University of Delaware, for the initial implementation of
this program.
.PP
Henry Spencer, University of Toronto, for the implementation of the
\fIregular expression\fP interpreter and the text in \fBREGULAR
EXPRESSIONS\fP.
.SH AUTHOR
John Cristy, E.I. DuPont De Nemours and Company Incorporated

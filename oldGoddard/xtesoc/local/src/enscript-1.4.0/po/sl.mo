   /   `  X  �  �����           ��������   *   ?          Q   u����      x   ���������   �   �         �  
����      �  3��������   �  K      	  %  �����   
  /  ���������  Z  �        �  �����     �  <��������  �  g        �  �����       ���������  #  �        M  �����     |  ��������  �  6        �  H����     �  r��������    �      #  "  �����     H  ���������  v  2        �  f����       ���������  6  �         W  ����     x  ;��������  �  M      !  �  l����   "  �  ���������  �  �      )    �����   %  9  	��������  P     $   '  v  ;����   (  �  h��������  �  �   &   ,  �  �����   +  	  ���������  +     *   -  Y  9����   .    p��������  �  �      G  �  �����   1  �  ���������  #  7   0   3  L  _����   4  k  |��������  �  �   2   8  �  �����   7  �  	��������    	?   6   9  @  	l����   :  e  	���������  |  	�   5   A  �  �����   =  �  ���������  E  �   <   ?  j  ����   @  �  3��������  �  b   >   D  �  �����   C  !  ���������  K     B   E  d  *����   F  �  S��������  �  z   ;   S  �  �����   I  �  ���������    �   H   K  I  ����   L  o  )��������  �  E   J   P  �  o����   O  �  ���������  �  �   N   Q  2  �����   R  O  ��������  a  #   M   Y  }  :����   U  �  p��������  �  �   T   W  �  �����   X  '  ��������  Q  Q   V   \  y  �����   [  �  ���������  �  �   Z   ^  �  ���������  �  I   ]   _    j��������  W  �xrealloc(): couldn't reallocate %d bytes
 xmalloc(): couldn't allocate %d bytes
 xcalloc(): couldn't allocate %d bytes
 wrapped warning: didn't process following options from environment variable %s:
 unknown special escape: %s unknown encoding: %s unexpected EOF while scanning ^@epsf escape truncated too long file name for ^@epsf escape:
%.*s too long argument for %s escape:
%.*s syntax error in option string %s="%s":
missing end of quotation: %c reading AFM info for font "%s"
 processing file "%s"...
 printer passing through %s file "%s"
 non-printable character codes (decimal):
 must print at least one line per each page: %s missing character codes (decimal):
 malformed underlay position: %s malformed font spec: %s malformed font spec for ^@font escape: %s malformed float dimension: "%s" malformed ^@epsf escape: no '{' found malformed ^@epsf escape: no ']' after options malformed %s escape: no '{' found known media:
name             width	height	llx	lly	urx	ury
------------------------------------------------------------
 invalid value for ^@shade escape: %s invalid value "%s" for option %s illegal value "%s" for option %s illegal underlay style: %s illegal page label format "%s" illegal option: %s illegal option %c for ^@epsf escape illegal non-printable format "%s" epsf: couldn't open pipe to command "%s": %s
 downloading font "%s"
 do not know anything about media "%s" couldn't stat input file "%s": %s couldn't rewind divert file: %s couldn't open printer `%s': %s couldn't open input filter "%s" for file "%s": %s couldn't open input file "%s": %s couldn't open font description file "%s": %s
 couldn't open config file "%s/%s": %s couldn't open EPS file "%s": %s
 couldn't open AFM library: %s couldn't open AFM file for the default font: %s couldn't open AFM file for font "%s", using default
 couldn't get passwd entry for uid=%d: %s couldn't find prolog "%s": %s
 couldn't find header definition file "%s.hdr": %s
 couldn't find header definition file "%s.hdr" couldn't find encoding file "%s.enc": %s
 couldn't create output file "%s": %s couldn't create divert file name: %s couldn't create divert file "%s": %s [ %d pages * %d copy ] Usage: %s [OPTION]... [FILE]...
Mandatory arguments to long options are mandatory for short options too.
  -1                         same as --columns=1
  -2                         same as --columns=2
      --columns=NUM          specify the number of columns per page
  -a, --pages=PAGES          specify which pages are printed
  -b, --header=HEADER        set page header
  -B, --no-header            no page headers
  -c, --truncate-lines       cut long lines (default is to wrap)
  -C, --line-numbers         precede each line with its line number
  -d                         an alias for option --printer
  -D, --setpagedevice=KEY[:VALUE]
                             pass a page device definition to output
  -e, --escapes[=CHAR]       enable special escape interpretation
 Try `%s --help' for more information.
 Long-only options:
  --download-font=NAME       download font NAME
  --filter-stdin=NAME        specify how stdin is shown to the input filter
  --help                     print this help and exit
  --highlight-bar-gray=NUM   print highlight bars with gray NUM (0 - 1)
  --list-media               list names of all known media
  --list-options             list all options and their values
  --non-printable-format=FMT specify how non-printable chars are printed
  --page-label-format=FMT    set page label format to FMT
  --printer-options=OPTIONS  pass extra options to the printer command
  --ul-angle=ANGLE           set underlay text's angle to ANGLE
  --ul-font=NAME             print underlays with font NAME
  --ul-gray=NUM              print underlays with gray value NUM
  --ul-position=POS          set underlay's starting position to POS
  --ul-style=STYLE           print underlays with style STYLE
 EPS file "%s" is too large for page
 EPS file "%s" is not a valid EPS file
 EPS file "%s" does not start with "%%!" magic
 EPS file "%s" contains malformed %%%%BoundingBox row:
"%.*s"
 %s:%d: %%HeaderHeight: no argument %s:%d: %%Format: too long name, maxlen=%d %s:%d: %%Format: no name %s:%d: %%Format: name "%s" is already defined %s:%d: %%FooterHeight: no argument %s: unrecognized option `--%s'
 %s: unrecognized option `%c%s'
 %s: option requires an argument -- %c
 %s: option `--%s' doesn't allow an argument
 %s: option `%s' requires an argument
 %s: option `%s' is ambiguous
 %s: option `%c%s' doesn't allow an argument
 %s: invalid option -- %c
 %s: illegal option -- %c
 %s: illegal newline character specifier: '%s': expected 'n' or 'r'
 %d non-printable characters
 %d lines were %s
 %d characters were missing
 %%Format: unknown `%%' escape `%c' (%d) %%Format: unknown `$' escape `%c' (%d) %%Format: too long variable name for $() escape %%Format: too long format for %%D{} escape %%Format: too long format for $D{} escape %%Format: no closing ')' for $() escape  sent to %s
  left in %s
   option %d = "%s"
   -s, --baselineskip=NUM     set baselineskip to NUM
  -S, --statusdict=KEY[:VALUE]
                             pass a statusdict definition to the output
  -t, --title=TITLE          set banner page's job title to TITLE.  Option
                             sets also the name of the input file stdin.
  -T, --tabsize=NUM          set tabulator size to NUM
  -u, --underlay[=TEXT]      print TEXT under every page
  -v, --verbose              tell what we are doing
  -V, --version              print version number
  -X, --encoding=NAME        use input encoding NAME
  -z, --no-formfeed          do not interpret form feed characters
  -Z, --pass-through         pass through PostScript and PCL files
                             without any modifications
   -f, --font=NAME            use font NAME for body text
  -F, --header-font=NAME     use font NAME for header texts
  -g, --print-anyway         nothing (compatibility option)
  -G                         same as --fancy-header
      --fancy-header[=NAME]  select fancy page header
  -h, --no-job-header        suppress the job header page
  -H, --highlight-bars=NUM   specify how high highlight bars are
  -i, --indent=NUM           set line indent to NUM characters
  -I, --filter=CMD           read input files through input filter CMD
  -j, --borders              print borders around columns
  -k, --page-prefeed         enable page prefeed
  -K, --no-page-prefeed      disable page prefeed
  -l, --lineprinter          simulate lineprinter, this is an alias for:
                               --lines-per-page=66, --no-header, --portrait,
                               --columns=1
   -L, --lines-per-page=NUM   specify how many lines are printed on each page
  -m, --mail                 send mail upon completion
  -M, --media=NAME           use output media NAME
  -n, --copies=NUM           print NUM copies of each page
  -N, --newline=NL           select the newline character.  Possible
                             values for NL are: n (`\n') and r (`\r').
  -o                         an alias for option --output
  -O, --missing-characters   list missing characters
  -p, --output=FILE          leave output to file FILE.  If FILE is `-',
                             leave output to stdout.
  -P, --printer=NAME         print output to printer NAME
  -q, --quiet, --silent      be really quiet
  -r, --landscape            print in landscape mode
  -R, --portrait             print in portrait mode
  xrealloc(): ni mogo�e ponovno rezervirati %d bytov pomnilnika
 xmalloc(): ni mogo�e rezervirati %d bytov pomnilnika
 xcalloc(): ni mogo�e rezervirati %d bytov pomnilnika
 nadaljevanih v naslednji vrstici pozor: naslednje izbire spremenljivke %s niso bile obdelane:
 neprepoznana posebna ube�na sekvenca: %s neznani kodni nabor: %s nepri�akovan znak EOF med branjem ube�ne sekvence ^@epsf porezanih predolgo ime datoteke za ube�no sekvenco ^@epsf:
%.*s predolgo ime datoteke za ube�no sekvenco ^@epsf:
%.*s sintakti�na napaka pri izbiri %s="%s":
manjka desni narekovaj: %c beremo metri�no datoteko AFM za font "%s"
 obdelujemo datoteko "%s"...
 tiskalnik posredujemo %s datoteko "%s"
 kode znakov, ki se jih ne da natisniti (decimalno):
 na vsaki strani mora biti vsaj ena vrstica: %s kode manjkajo�ih znakov (decimalno):
 slabo formulirana slabo postavljena specifikacija fonta: %s slabo dolo�en font za ube�no sekvenco ^@font: %s neustrezne dimenzije: "%s" slabo definirana ube�na sekvenco ^@epsf: manjka ,{' slabo definirana ube�na sekvenco ^@epsf: manjka ,]' za izbirami slabo definirana ube�na sekvenco ^@epsf: manjka ,{' znani formati papirja:
ime              �irina	vi�ina	LLX	LLY	URX	URY
------------------------------------------------------------
  vrednost "%s" ni dovoljena za izbiro %s vrednost "%s" ni dovoljena za izbiro %s slabo formulirana nedovoljena oznaka strani "%s" nedovoljena izbira: %s nedovoljena izbira %c za ube�no sekvenco ^@epsf nedovoljen non-printable format "%s" epsf: cevi ni mogo�e speljati do ukaza "%s": %s
 nalagamo font "%s"
 format papirja "%s" je neznan ni mo� priti do podatkov o datoteki "%s": %s vhodne datoteke "%s" ni mogo�e odpreti: %s tiskalnik ,%s' ni dostopen: %s vhodne datoteke "%s" ni mogo�e odpreti: %s vhodne datoteke "%s" ni mogo�e odpreti: %s datoteke z opisom fonta "%s" ni mo� odpreti: %s
 konfiguracijske datoteke "%s/%s" ni mogo�e odpreti: %s datoteke "%s" v obliki EPS ni mogo�e odpreti: %s
 knji�nice AFM ni mogo�e odpreti: %s datoteke AFM za privzeti font ni mo� odpreti: %s datoteke AFM za font "%s" ni mo� odpreti, uporabljamo privzeto
 ni mogo�e priti do passwd za UID=%d: %s uvoda "%s" ni mo� najti: %s
 datoteke z definicijo glave "%s.hdr" ni mo� najti: %s
 datoteke "%s.hdr" z definicijo glave ni mogo�e najti kodne datoteke "%s.enc" ni mo� najti: %s
 izhodne datoteke "%s" ni mogo�e zapisati: %s izhodne datoteke "%s" ni mogo�e zapisati: %s izhodne datoteke "%s" ni mogo�e zapisati: %s [ %d strani * %d kopij ] Uporaba: %s [izbire] [datoteka] ...
Izbire:
  -1, -2, --columns=NUM		�tevilo stolpcev na stran
  -b GLAVA, --page-header=GLAVA
				naslovna glava na strani
  -B, --no-header		brez naslovne glave
  -c, --truncate-lines		predolge vrstice porezane
				(privzeta izbira je nadaljevanje v naslednji vrstici)
  -C, --line-numbers		vsaka vrstica ozna�ena s �tevilko vrstice
  -d TISKALNIK			odtis na tiskalnik TISKALNIK
  -D KEY[:VALUE], --setpagedevice=KEY[:VALUE]
				pass a page device definition to output
  -e, --special-escapes		omogo�enje posebnih ube�nih sekvenc
    --download-font=IME		nalo�imo font IME
  --list-media			izpis vseh znanih formatov papirja
  --list-options		izpis nastavitev programa
  --non-printable-format=FMT	dolo�imo FMT za non-printable format
  --page-label-format=FMT	dolo�imo FMT za oznako strani
  --ul-angle=KOT		KOT, pod katerim naj bo izpisano besedilo v podlagi
  --ul-font=IME		font, v katerem naj bo izpisano besedilo v podlagi
  --ul-gray=NUM			stopnja sivine za besedilo v podlagi (0 - 1)
  --ul-position=POS		podlaga naj se za�ne na mestu POS
 EPS datoteka "%s" je prevelika za stran
 Datoteka "%s" ni v obliki EPS datoteka "%s" v obliki EPS se ne za�ne z "%%!" Datoteka "%s" v obliki EPS vsebuje nepravilno vrstico %%%%BoundingBox:
"%.*s"
 %s:%d: %%HeaderHeight: manjka argument %s:%d: %%Format: ime je predolgo, najve�ja dol�ina %d %s:%d: %%Format: manjka ime %s:%d: %%Format: ime "%s" je �e dolo�eno %s:%d: %%FooterHeight: manjka argument %s: neprepoznana izbira ,--%s'
 %s: neprepoznana izbira ,%c%s'
 %s: izbira zahteva argument -- %c
 %s: izbira ,--%s' ne dovoljuje argumenta
 %s: izbira ,%s' zahteva argument
 %s: izbira ,%s' je dvoumna
 %s: izbira ,%c%s' ne dovoljuje argumenta
 %s: neveljavna izbira -- %c
 %s: nedovoljena izbira -- %c
 %s: ,%s' ni dovoljeni znak za novo vrstico: ,n' ali ,r' sta
 %d znakov, ki se jih ne da natisniti
 %d vrstic je bilo %s
 %d znakov je manjkalo
 %%Format: neprepoznana ,%%' ube�na sekvenca `%c' (%d) %%Format: neprepoznana ,$' ube�na sekvenca `%c' (%d) %%Format: predolgo formatno dolo�ilo za ube�no sekvenco $D{} %%Format: predolg format za ube�no sekvenco %%D{} %%Format: predolgo formatno dolo�ilo za ube�no sekvenco $D{} %%Format: predolgo formatno dolo�ilo za ube�no sekvenco $D{}  poslano na %s
  ostalo na %s
   izbira %d = "%s"
   -s NUM, --baselineskip=NUM	razmik med vrsticami naj bo NUM (v 1/72")
  -S KEY[:VALUE], --statusdict=KEY[:VALUE]
				uporaba definicij v "statusdict"
  -t NASLOV, --title=NASLOV	nadnaslov, izpisan na vsaki strani
  -T NUM, --tabsize=NUM		tabulator na NUM znakov (privzeto 8)
  -u [TEKST], --underlay[=TEKST]	besedilo, izpisano na podlogi
  -v, --verbose			s komentarjem med potekom
  -V, --version			verzija programa %s
  -X IME, --encoding=IME	IME kodnega nabora vhodnega besedila
  -z, --no-formfeed		ignoriraj znake za skok na novo stran v besedilu
  -Z, --pass-through-ps-files	datoteke v formatu PostScript ali PCL
				odtisni brez sprememb
   -f IME, --font=IME		uporabimo font IME za glavno besedilo
  -F IME, --header-font=IME	uporabimo font IME za glavo
  -g, --print-anyway		ni� (zaradi zdru�ljivosti)
  -G, --fancy-header[=IME]	izbira izumetni�ene glave
  -h, --help			ta navodila
  -i NUM, --indent=NUM		NUM znakov zamika na levem robu
  -l, --lineprinter		imitacija vrsti�nega tiskalnika:
			  	  --lines-per-page=65, --no-header,
				  --portrait, --columns=1
  -L NUM, --lines-per-page=NUM	koliko vrstic besedila na stran
  -m, --mail			sporo�ilo o opravljenem poslu po po�ti
   -M IME, --media=IME  	format papirja IME
  -n NUM, --copies=NUM		�tevilo kopij
  -N NL, --newline=NL		izbira znaka za konec vrstice.  Mo�nosti za NL
 				sta: n (`\n') in r (`\r').
  -o DATOTEKA			okraj�ava za izbiro `-p', `--output'
  -O, --list-missing-characters	kode manjkajo�ih znakov
  -p DATOTEKA, --output=DATOTEKA	izhod na datoteko z imenom DATOTEKA.
				�e je ime ,-', izpis na standardni izhod.
  -P IME, --printer=IME	odtis na tiskalnik IME
  -q, --quiet, --silent		brez obvestil
  -r, --landscape		podol�ni odtis
  -R, --portrait		pokon�ni odtis (privzeta izbira)
 Project-Id-Version: enscript 1.3.0
POT-Creation-Date: 1996-06-30 10:16+0300
PO-Revision-Date: 1996-05-29 09:34
Last-Translator: Primoz Peterlin <primoz.peterlin@biofiz.mf.uni-lj.si>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-2
Content-Transfer-Encoding: 8-bit
 
   /   `  X  Z  �����           ��������   *   2          Q   Z����      x   ���������   �   �         �   �����      �   ���������   �        	  %  /����   
  /  ;��������  Z  l        �  �����     �  ���������  �  �        �  �����       ���������  #           M  ����     |  <��������  �  j        �  k����     �  ���������    �      #  "  �����     H  ��������  v  \        �  �����       ���������  6  �         W  �����     x  ��������  �        !  �  .����   "  �  E��������  �  q      )    r����   %  9  ���������  P  �   $   '  v  �����   (  �  ��������  �  M   &   ,  �  u����   +  	  ���������  +  �   *   -  Y  �����   .    ,��������  �  R      G  �  v����   1  �  ���������  #  �   0   3  L  /����   4  k  W��������  �  ~   2   8  �  �����   7  �  ���������    �   6   9  @  '����   :  e  U��������  |  o   5   A  �  p����   =  �  ���������  E  �   <   ?  j  �����   @  �  ���������  �  	)   >   D  �  	l����   C  !  	���������  K  	�   B   E  d  	�����   F  �  
��������  �  
:   ;   S  �  
W����   I  �  
t��������    
�   H   K  I  
�����   L  o  
���������  �     J   P  �  &����   O  �  D��������  �  b   N   Q  2  �����   R  O  ���������  a  �   M   Y  }  �����   U  �  ���������  �  %   T   W  �  Y����   X  '  ���������  Q  �   V   \  y  �����   [  �  ��������  �  "   Z   ^  �  5��������  �  6   ]   _    7��������  W  8xrealloc(): couldn't reallocate %d bytes
 xmalloc(): couldn't allocate %d bytes
 xcalloc(): couldn't allocate %d bytes
 wrapped warning: didn't process following options from environment variable %s:
 unknown special escape: %s unknown encoding: %s unexpected EOF while scanning ^@epsf escape truncated too long file name for ^@epsf escape:
%.*s too long argument for %s escape:
%.*s syntax error in option string %s="%s":
missing end of quotation: %c reading AFM info for font "%s"
 processing file "%s"...
 printer passing through %s file "%s"
 non-printable character codes (decimal):
 must print at least one line per each page: %s missing character codes (decimal):
 malformed underlay position: %s malformed font spec: %s malformed font spec for ^@font escape: %s malformed float dimension: "%s" malformed ^@epsf escape: no '{' found malformed ^@epsf escape: no ']' after options malformed %s escape: no '{' found known media:
name             width	height	llx	lly	urx	ury
------------------------------------------------------------
 invalid value for ^@shade escape: %s invalid value "%s" for option %s illegal value "%s" for option %s illegal underlay style: %s illegal page label format "%s" illegal option: %s illegal option %c for ^@epsf escape illegal non-printable format "%s" epsf: couldn't open pipe to command "%s": %s
 downloading font "%s"
 do not know anything about media "%s" couldn't stat input file "%s": %s couldn't rewind divert file: %s couldn't open printer `%s': %s couldn't open input filter "%s" for file "%s": %s couldn't open input file "%s": %s couldn't open font description file "%s": %s
 couldn't open config file "%s/%s": %s couldn't open EPS file "%s": %s
 couldn't open AFM library: %s couldn't open AFM file for the default font: %s couldn't open AFM file for font "%s", using default
 couldn't get passwd entry for uid=%d: %s couldn't find prolog "%s": %s
 couldn't find header definition file "%s.hdr": %s
 couldn't find header definition file "%s.hdr" couldn't find encoding file "%s.enc": %s
 couldn't create output file "%s": %s couldn't create divert file name: %s couldn't create divert file "%s": %s [ %d pages * %d copy ] Usage: %s [OPTION]... [FILE]...
Mandatory arguments to long options are mandatory for short options too.
  -1                         same as --columns=1
  -2                         same as --columns=2
      --columns=NUM          specify the number of columns per page
  -a, --pages=PAGES          specify which pages are printed
  -b, --header=HEADER        set page header
  -B, --no-header            no page headers
  -c, --truncate-lines       cut long lines (default is to wrap)
  -C, --line-numbers         precede each line with its line number
  -d                         an alias for option --printer
  -D, --setpagedevice=KEY[:VALUE]
                             pass a page device definition to output
  -e, --escapes[=CHAR]       enable special escape interpretation
 Try `%s --help' for more information.
 Long-only options:
  --download-font=NAME       download font NAME
  --filter-stdin=NAME        specify how stdin is shown to the input filter
  --help                     print this help and exit
  --highlight-bar-gray=NUM   print highlight bars with gray NUM (0 - 1)
  --list-media               list names of all known media
  --list-options             list all options and their values
  --non-printable-format=FMT specify how non-printable chars are printed
  --page-label-format=FMT    set page label format to FMT
  --printer-options=OPTIONS  pass extra options to the printer command
  --ul-angle=ANGLE           set underlay text's angle to ANGLE
  --ul-font=NAME             print underlays with font NAME
  --ul-gray=NUM              print underlays with gray value NUM
  --ul-position=POS          set underlay's starting position to POS
  --ul-style=STYLE           print underlays with style STYLE
 EPS file "%s" is too large for page
 EPS file "%s" is not a valid EPS file
 EPS file "%s" does not start with "%%!" magic
 EPS file "%s" contains malformed %%%%BoundingBox row:
"%.*s"
 %s:%d: %%HeaderHeight: no argument %s:%d: %%Format: too long name, maxlen=%d %s:%d: %%Format: no name %s:%d: %%Format: name "%s" is already defined %s:%d: %%FooterHeight: no argument %s: unrecognized option `--%s'
 %s: unrecognized option `%c%s'
 %s: option requires an argument -- %c
 %s: option `--%s' doesn't allow an argument
 %s: option `%s' requires an argument
 %s: option `%s' is ambiguous
 %s: option `%c%s' doesn't allow an argument
 %s: invalid option -- %c
 %s: illegal option -- %c
 %s: illegal newline character specifier: '%s': expected 'n' or 'r'
 %d non-printable characters
 %d lines were %s
 %d characters were missing
 %%Format: unknown `%%' escape `%c' (%d) %%Format: unknown `$' escape `%c' (%d) %%Format: too long variable name for $() escape %%Format: too long format for %%D{} escape %%Format: too long format for $D{} escape %%Format: no closing ')' for $() escape  sent to %s
  left in %s
   option %d = "%s"
   -s, --baselineskip=NUM     set baselineskip to NUM
  -S, --statusdict=KEY[:VALUE]
                             pass a statusdict definition to the output
  -t, --title=TITLE          set banner page's job title to TITLE.  Option
                             sets also the name of the input file stdin.
  -T, --tabsize=NUM          set tabulator size to NUM
  -u, --underlay[=TEXT]      print TEXT under every page
  -v, --verbose              tell what we are doing
  -V, --version              print version number
  -X, --encoding=NAME        use input encoding NAME
  -z, --no-formfeed          do not interpret form feed characters
  -Z, --pass-through         pass through PostScript and PCL files
                             without any modifications
   -f, --font=NAME            use font NAME for body text
  -F, --header-font=NAME     use font NAME for header texts
  -g, --print-anyway         nothing (compatibility option)
  -G                         same as --fancy-header
      --fancy-header[=NAME]  select fancy page header
  -h, --no-job-header        suppress the job header page
  -H, --highlight-bars=NUM   specify how high highlight bars are
  -i, --indent=NUM           set line indent to NUM characters
  -I, --filter=CMD           read input files through input filter CMD
  -j, --borders              print borders around columns
  -k, --page-prefeed         enable page prefeed
  -K, --no-page-prefeed      disable page prefeed
  -l, --lineprinter          simulate lineprinter, this is an alias for:
                               --lines-per-page=66, --no-header, --portrait,
                               --columns=1
   -L, --lines-per-page=NUM   specify how many lines are printed on each page
  -m, --mail                 send mail upon completion
  -M, --media=NAME           use output media NAME
  -n, --copies=NUM           print NUM copies of each page
  -N, --newline=NL           select the newline character.  Possible
                             values for NL are: n (`\n') and r (`\r').
  -o                         an alias for option --output
  -O, --missing-characters   list missing characters
  -p, --output=FILE          leave output to file FILE.  If FILE is `-',
                             leave output to stdout.
  -P, --printer=NAME         print output to printer NAME
  -q, --quiet, --silent      be really quiet
  -r, --landscape            print in landscape mode
  -R, --portrait             print in portrait mode
  xrealloc(): %d tavun uudelleenvaraus ep�onnistui
 xmalloc(): %d tavun varaus ep�onnistui
 xcalloc(): %d tavun varaus ep�onnistui
 rivitettiin varoitus: seuraavat optiot j�iv�t k�sittelem�tt� ymp�rist�muuttujasta %s:
 tuntematon komento: %s tuntematon merkist�: %s tiedosto loppui kesken ^@epsf komennon katkaistiin liian pitk� tiedostonimi ^@epsf komennolle:
%.*s liian pitk� argumentti ^@shade komennolle:
%.*s  luetaan AFM-tietoa kirjasimelle "%s"
 tiedosto "%s"...
 kirjoittimelle sivuutan %s-tiedoston "%s"
  jokaiselle sivulle t�ytyy tulostaa v�hint��n yksi rivi: %s puuttuvat merkkikoodit (desimaalinumeroina):
  virheellinen kirjasinm��ritys: %s virheellinen kirjasinm��ritys ^@font komennolle: %s virheellinen liukulukuyksikk�: "%s" virheellinen ^@epsf komento: merkki� '{' ei l�ytynyt virheellinen ^@epsf komento: merkki ']' puuttuu optioiden per�st� virheellinen ^@epsf komento: merkki� '{' ei l�ytynyt  virheellinen arvo ^@shade komennolle: %s virheellinen arvo "%s" optiolle %s virheellinen arvo "%s" optiolle %s  tuntematon sivun otsikon tulostusmuoto "%s" virheellinen optio: %s virheellinen optio %c for ^@epsf komennolle  epsf: putken avaus komennolle "%s" ep�onnistui: %s
 sis�llytet��n kirjasin "%s" tulosteeseen
 tuntematon tulostusmedia "%s" l�hdetiedoston "%s" tarkastelu ep�onnistui: %s siirtyminen tilap�istiedostossa ep�onnistui: %s kirjoittimen `%s' avaus ep�onnistui: %s l�hdetiedoston "%s" avaus ep�onnistui: %s l�hdetiedoston "%s" avaus ep�onnistui: %s kirjasintiedoston "%s" avaus ep�onnistui: %s
 konfiguraatiotiedoston "%s/%s" avaus ep�onnistui: %s EPS-kuvan "%s" avaus ep�onnistui: %s
 AFM kirjaston avaus ep�onnistui: %s AFM-tiedoston avaus ep�onnistui oletuskirjasimelle: %s AFM-tiedoston avaus ep�onnistui kirjasimelle "%s", k�ytet��n oletuskirjasinta
 k�ytt�j�tietoja ei l�ytynyt k�ytt�j�lle uid=%d: %s PostScript-tiedostoa "%s" ei l�ydy: %s
 otsaketiedostoa "%s.hdr" ei l�ydy: %s
 otsikkotiedostoa "%s.hdr" ei l�ytynyt merkist�tiedostoa "%s.enc" ei l�ydy: %s
 tulostiedoston "%s" luonti ep�onnistui: %s tilap�istiedoston nimen luonti ep�onnistui: %s tilap�istiedoston "%s" luonti ep�onnistui: %s [ %d sivua * %d kopiota ]  Komento `%s --help' antaa lis�� tietoa.
  EPS tiedosto "%s" on liian iso mahtuakseen sivulle
 EPS-tiedosto "%s" ei ole validi EPS-tiedosto
 EPS-tiedosto "%s" ei ala "%%!" tunnisteella
 EPS-tiedostossa "%s" on virheellinen %%%%BoundingBox rivi:
"%.*s"
 %s:%d: %%HeaderHeight: argumentti puuttuu %s:%d: %%Format: liian pitk� nimi, maksimipituus=%d %s:%d: %%Format: ei nime� %s:%d: %%Format: nimi "%s" on jo m��ritelty %s:%d: %%FooterHeight: argumentti puuttuu %s: tuntematon optio `--%s'
 %s: tuntematon optio `%c%s'
 %s: optio vaatii argumentin -- %c
 %s: optio `--%s' ei ota argumenttia
 %s: optio `%s' vaatii argumentin
 %s: optio `%s' on monik�sitteinen
 %s: optio `%c%s' ei ota argumenttia
 %s: virheellinen optio -- %c
 %s: virheellinen optio -- %c
 %s: virheellinen rivinvaihtomerkin m��re: '%s': pit�isi olla 'n' tai 'r'
  %d rivi� %s
 %d merkki� puuttui
 %%Format: tuntematon `%%' komento `%c' (%d) %%Format: tuntematon `$' komento `%c' (%d) %%Format: liian pitk� muuttujan nimi $()-komennolle %%Format: liian pitk� muotomerkkijono %%D{}-komennolle %%Format: liian pitk� muotomerkkijono $D{}-komennolle %%Format: ei sulkevaa ')' merkki� $()-komennolle  tulostettu %s
  tulostettu tiedostoon %s
   optio %d = "%s"
    Project-Id-Version: GNU enscript 1.3.2
POT-Creation-Date: 1996-06-30 10:16+0300
PO-Revision-Date: 1996-05-24 08:27
Last-Translator: Markku Rossi <mtr@iki.fi>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8-bit
 
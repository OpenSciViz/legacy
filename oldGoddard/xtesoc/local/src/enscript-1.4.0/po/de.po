# German messages for GNU enscript.
# Copyright (C) 1996 Free Software Foundation, Inc.
# Karl Eichwalder <ke@ke.central.de>, 1996
#
msgid ""
msgstr ""
"Project-Id-Version: GNU enscript 1.3.2\n"
"POT-Creation-Date: 1996-06-30 10:16+0300\n"
"PO-Revision-Date: 1996-04-29 17:56 MET DST\n"
"Last-Translator: Karl Eichwalder <ke@ke.Central.DE>\n"
"Language-Team: German <de@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8-bit\n"

# Alles in diesen Stil �ndern:
#
# msgid "%%Format: no closing ')' for $() escape"
# msgstr "%%Format: Keine schlie�ende ')' f�r das escape-Zeichen $()"
#
#: compat/getopt.c:519
#, c-format
msgid "%s: option `%s' is ambiguous\n"
msgstr "%s: Option '%s' ist zweideutig\n"

#: compat/getopt.c:542
#, c-format
msgid "%s: option `--%s' doesn't allow an argument\n"
msgstr "%s: Option '--%s' erlaubt keinen Parameter\n"

#: compat/getopt.c:547
#, c-format
msgid "%s: option `%c%s' doesn't allow an argument\n"
msgstr "%s: Option '%c%s' erlaubt keinen Parameter\n"

#: compat/getopt.c:562
#, c-format
msgid "%s: option `%s' requires an argument\n"
msgstr "%s: Option '%s' verlangt einen Parameter\n"

#. --option
#: compat/getopt.c:590
#, c-format
msgid "%s: unrecognized option `--%s'\n"
msgstr "%s: Nicht erkannte Option '--%s'\n"

#. +option or -option
#: compat/getopt.c:594
#, c-format
msgid "%s: unrecognized option `%c%s'\n"
msgstr "%s: Nicht erkannte Option '%c%s'\n"

#. 1003.2 specifies the format of this message.
#: compat/getopt.c:619
#, c-format
msgid "%s: illegal option -- %c\n"
msgstr "%s: illegal option -- %c\n"

#: compat/getopt.c:622
#, c-format
msgid "%s: invalid option -- %c\n"
msgstr "%s: invalid option -- %c\n"

#: compat/getopt.c:658
#, c-format
msgid "%s: option requires an argument -- %c\n"
msgstr "%s: option requires an argument -- %c\n"

#: src/main.c:656
#, c-format
msgid "couldn't get passwd entry for uid=%d: %s"
msgstr "kein Passwort-Eintrag f�r UID=%d zu bekommen: %s"

#. No luck, report error from the original config file.
#: src/main.c:703
#, c-format
msgid "couldn't open config file \"%s/%s\": %s"
msgstr "kann Konfigurationsdatei \"%s/%s\" nicht �ffnen: %s"

#: src/main.c:756
#, c-format
msgid "unknown encoding: %s"
msgstr "unbekannte Kodierung: %s"

#: src/main.c:774
#, c-format
msgid "couldn't open AFM library: %s"
msgstr "kann AFM-Bibliothek nicht �ffnen: %s"

# M�ssen die x-, y-Sachen auch �bersetzt werden?
#: src/main.c:805
msgid ""
"known media:\n"
"name             width\theight\tllx\tlly\turx\tury\n"
"------------------------------------------------------------\n"
msgstr ""
"unbekanntes Medium:\n"
"Name             Weite  H�he    llx     lly     urx     ury\n"
"------------------------------------------------------------\n"

#: src/main.c:824
#, c-format
msgid "do not know anything about media \"%s\""
msgstr "Nichts von dem Medium \"%s\" bekannt"

#: src/main.c:832
#, c-format
msgid "illegal page label format \"%s\""
msgstr "ung�ltiges Format f�r das Seiten-Label (?) \"%s\""

#: src/main.c:844
#, c-format
msgid "illegal non-printable format \"%s\""
msgstr "ung�ltiges, nicht auszugebendes Format (`non-printable') \"%s\""

#: src/main.c:876
#, c-format
msgid "malformed underlay position: %s"
msgstr "fehlformatierte `underlay' Position: %s"

#: src/main.c:899
#, c-format
msgid "illegal underlay style: %s"
msgstr ""

#: src/main.c:945
#, c-format
msgid "couldn't open printer `%s': %s"
msgstr "kann nicht auf Drucker '%s' zugreifen: %s"

#: src/main.c:953
#, c-format
msgid "couldn't create output file \"%s\": %s"
msgstr "kann Ausgabedatei \"%s\" nicht anlegen: %s"

#: src/main.c:991
#, c-format
msgid "couldn't stat input file \"%s\": %s"
msgstr "kann Eingabedatei \"%s\" nicht feststellen (`stat'): %s"

#. Tell how things went.
#: src/main.c:1016
#, c-format
msgid "[ %d pages * %d copy ]"
msgstr "[ %d Seiten * %d Kopie ]"

#: src/main.c:1018
#, c-format
msgid " sent to %s\n"
msgstr " geschickt zu: %s\n"

#: src/main.c:1018
msgid "printer"
msgstr "Drucker"

#: src/main.c:1020
#, c-format
msgid " left in %s\n"
msgstr " abgelegt in: %s\n"

#: src/main.c:1023
#, c-format
msgid "%d lines were %s\n"
msgstr "%d Zeile(n) wurde(n) %s\n"

#: src/main.c:1024
msgid "truncated"
msgstr "abgeschnitten (`truncated')"

#: src/main.c:1024
msgid "wrapped"
msgstr "zusammengedr�ngt (`wrapped')"

#: src/main.c:1028
#, c-format
msgid "%d characters were missing\n"
msgstr "%d Zeichen fehlen\n"

#: src/main.c:1031
msgid "missing character codes (decimal):\n"
msgstr "fehlende Zeichencodes (dezimal):\n"

#: src/main.c:1038
#, c-format
msgid "%d non-printable characters\n"
msgstr "%d nicht druckbare Zeichen\n"

#: src/main.c:1041
msgid "non-printable character codes (decimal):\n"
msgstr "nicht druckbare Zeichencodes (dezimal):\n"

#: src/main.c:1107
#, c-format
msgid ""
"syntax error in option string %s=\"%s\":\n"
"missing end of quotation: %c"
msgstr ""
"Syntaxfehler in der Option %s=\"%s\":\n"
"fehlendes Ende des Zitats (`quotation'): %c"

#: src/main.c:1138
#, c-format
msgid ""
"warning: didn't process following options from environment variable %s:\n"
msgstr ""
"ACHTUNG: folgende Optionen von der Umgebungsvariablen %s nicht ausgef�hrt:\n"

#: src/main.c:1142
#, c-format
msgid "  option %d = \"%s\"\n"
msgstr "  Option %d = \"%s\"\n"

#: src/main.c:1264 src/main.c:1270 src/main.c:1436 src/util.c:400
#, c-format
msgid "malformed font spec: %s"
msgstr "unpassende (`malformed') Fontspezifikation: %s"

#: src/main.c:1285
#, c-format
msgid "couldn't find header definition file \"%s.hdr\""
msgstr "kann Definitionsdatei \"%s.hdr\" f�r die Kopfangaben nicht finden"

#: src/main.c:1330
#, c-format
msgid "must print at least one line per each page: %s"
msgstr "muss wenigstens eine Zeile pro Seite drucken: %s"

#: src/main.c:1349
#, c-format
msgid "%s: illegal newline character specifier: '%s': expected 'n' or 'r'\n"
msgstr ""
"%s: ung�ltiger Bezeichner f�r \"Neue Zeile\": '%s': erwartet 'n' oder 'r'\n"

#: src/main.c:1489
#, c-format
msgid "Try `%s --help' for more information.\n"
msgstr ""

#: src/main.c:1601
#, c-format
msgid ""
"Usage: %s [OPTION]... [FILE]...\n"
"Mandatory arguments to long options are mandatory for short options too.\n"
"  -1                         same as --columns=1\n"
"  -2                         same as --columns=2\n"
"      --columns=NUM          specify the number of columns per page\n"
"  -a, --pages=PAGES          specify which pages are printed\n"
"  -b, --header=HEADER        set page header\n"
"  -B, --no-header            no page headers\n"
"  -c, --truncate-lines       cut long lines (default is to wrap)\n"
"  -C, --line-numbers         precede each line with its line number\n"
"  -d                         an alias for option --printer\n"
"  -D, --setpagedevice=KEY[:VALUE]\n"
"                             pass a page device definition to output\n"
"  -e, --escapes[=CHAR]       enable special escape interpretation\n"
msgstr ""

#: src/main.c:1618
msgid ""
"  -f, --font=NAME            use font NAME for body text\n"
"  -F, --header-font=NAME     use font NAME for header texts\n"
"  -g, --print-anyway         nothing (compatibility option)\n"
"  -G                         same as --fancy-header\n"
"      --fancy-header[=NAME]  select fancy page header\n"
"  -h, --no-job-header        suppress the job header page\n"
"  -H, --highlight-bars=NUM   specify how high highlight bars are\n"
"  -i, --indent=NUM           set line indent to NUM characters\n"
"  -I, --filter=CMD           read input files through input filter CMD\n"
"  -j, --borders              print borders around columns\n"
"  -k, --page-prefeed         enable page prefeed\n"
"  -K, --no-page-prefeed      disable page prefeed\n"
"  -l, --lineprinter          simulate lineprinter, this is an alias for:\n"
"                               --lines-per-page=66, --no-header, "
"--portrait,\n"
"                               --columns=1\n"
msgstr ""

#: src/main.c:1635
msgid ""
"  -L, --lines-per-page=NUM   specify how many lines are printed on each "
"page\n"
"  -m, --mail                 send mail upon completion\n"
"  -M, --media=NAME           use output media NAME\n"
"  -n, --copies=NUM           print NUM copies of each page\n"
"  -N, --newline=NL           select the newline character.  Possible\n"
"                             values for NL are: n (`\\n') and r (`\\r').\n"
"  -o                         an alias for option --output\n"
"  -O, --missing-characters   list missing characters\n"
"  -p, --output=FILE          leave output to file FILE.  If FILE is `-',\n"
"                             leave output to stdout.\n"
"  -P, --printer=NAME         print output to printer NAME\n"
"  -q, --quiet, --silent      be really quiet\n"
"  -r, --landscape            print in landscape mode\n"
"  -R, --portrait             print in portrait mode\n"
msgstr ""

#: src/main.c:1651
msgid ""
"  -s, --baselineskip=NUM     set baselineskip to NUM\n"
"  -S, --statusdict=KEY[:VALUE]\n"
"                             pass a statusdict definition to the output\n"
"  -t, --title=TITLE          set banner page's job title to TITLE.  Option\n"
"                             sets also the name of the input file stdin.\n"
"  -T, --tabsize=NUM          set tabulator size to NUM\n"
"  -u, --underlay[=TEXT]      print TEXT under every page\n"
"  -v, --verbose              tell what we are doing\n"
"  -V, --version              print version number\n"
"  -X, --encoding=NAME        use input encoding NAME\n"
"  -z, --no-formfeed          do not interpret form feed characters\n"
"  -Z, --pass-through         pass through PostScript and PCL files\n"
"                             without any modifications\n"
msgstr ""

#: src/main.c:1666
msgid ""
"Long-only options:\n"
"  --download-font=NAME       download font NAME\n"
"  --filter-stdin=NAME        specify how stdin is shown to the input "
"filter\n"
"  --help                     print this help and exit\n"
"  --highlight-bar-gray=NUM   print highlight bars with gray NUM (0 - 1)\n"
"  --list-media               list names of all known media\n"
"  --list-options             list all options and their values\n"
"  --non-printable-format=FMT specify how non-printable chars are printed\n"
"  --page-label-format=FMT    set page label format to FMT\n"
"  --printer-options=OPTIONS  pass extra options to the printer command\n"
"  --ul-angle=ANGLE           set underlay text's angle to ANGLE\n"
"  --ul-font=NAME             print underlays with font NAME\n"
"  --ul-gray=NUM              print underlays with gray value NUM\n"
"  --ul-position=POS          set underlay's starting position to POS\n"
"  --ul-style=STYLE           print underlays with style STYLE\n"
msgstr ""

#: src/psgen.c:273
#, c-format
msgid "couldn't find prolog \"%s\": %s\n"
msgstr "kann Vorspann (`prolog') \"%s\" nicht finden: %s\n"

#: src/psgen.c:281
#, c-format
msgid "couldn't find encoding file \"%s.enc\": %s\n"
msgstr "kann Kodierungsdatei \"%s.enc\" nicht finden: %s\n"

#: src/psgen.c:403
#, c-format
msgid "couldn't find header definition file \"%s.hdr\": %s\n"
msgstr "kann Datei \"%s.hdr\" f�r die Kopfangabe nicht finden: %s\n"

#: src/psgen.c:514
#, c-format
msgid "processing file \"%s\"...\n"
msgstr "bearbeite Datei \"%s\"...\n"

#.
#. * At the beginning of the column, warn user
#. * and print image.
#.
#: src/psgen.c:711
#, c-format
msgid "EPS file \"%s\" is too large for page\n"
msgstr "EPS-Datei \"%s\" ist zu gro� f�r Seite\n"

#: src/psgen.c:895
#, c-format
msgid "unknown special escape: %s"
msgstr "unbekanntes Spezil-escape-Zeichen: %s"

#: src/psgen.c:1021
#, c-format
msgid "illegal option %c for ^@epsf escape"
msgstr "ung�ltige Option %c f�r das escape-Zeichen ^@epsf"

#: src/psgen.c:1025
msgid "malformed ^@epsf escape: no ']' after options"
msgstr "fehlgebildetes escape-Zeichen ^@epsf: kein ']' nach den Optionen"

#: src/psgen.c:1036
#, c-format
msgid ""
"too long file name for ^@epsf escape:\n"
"%.*s"
msgstr ""
"zu langer Dateiname f�r das escape-Zeichen ^@epsf:\n"
"%.*s"

#: src/psgen.c:1040
msgid "unexpected EOF while scanning ^@epsf escape"
msgstr ""
"unerwartetes EOF w�hrend Durchsicht (`scanning') des escape-Zeichens ^@epsf"

#: src/psgen.c:1046
msgid "malformed ^@epsf escape: no '{' found"
msgstr "fehlformatiertes escape-Zeichen ^@epsf: keine '{' gefunden"

#: src/psgen.c:1104
#, fuzzy, c-format
msgid "malformed %s escape: no '{' found"
msgstr "fehlformatiertes escape-Zeichen ^@epsf: keine '{' gefunden"

#: src/psgen.c:1117
#, fuzzy, c-format
msgid ""
"too long argument for %s escape:\n"
"%.*s"
msgstr ""
"zu langer Dateiname f�r das escape-Zeichen ^@epsf:\n"
"%.*s"

#: src/psgen.c:1135
#, c-format
msgid "malformed font spec for ^@font escape: %s"
msgstr "fehlformatierte Fontspezifikation f�r das escape-Zeichen ^@font: %s"

#: src/psgen.c:1147
#, c-format
msgid "invalid value for ^@shade escape: %s"
msgstr ""

#: src/psgen.c:1647
#, c-format
msgid "epsf: couldn't open pipe to command \"%s\": %s\n"
msgstr "epsf: kann Pipe zum Befehl \"%s\" nicht �ffnen: %s\n"

#: src/psgen.c:1673
#, c-format
msgid "couldn't open EPS file \"%s\": %s\n"
msgstr "kann EPS-Datei \"%s\" nicht �ffnen: %s\n"

#: src/psgen.c:1709
msgid "EPS file \"%s\" does not start with \"%%!\" magic\n"
msgstr "EPS-Datei \"%s\" beginnt nicht mit Kennung (`magic') %%!\n"

#. No, this BoundingBox comment is corrupted.
#: src/psgen.c:1734
msgid ""
"EPS file \"%s\" contains malformed %%%%BoundingBox row:\n"
"\"%.*s\"\n"
msgstr ""
"EPS-Datei \"%s\" enth�lt fehlgebildete %%%%BoundingBox-Zeile:\n"
"\"%.*s\"\n"

#: src/psgen.c:1759
#, c-format
msgid "EPS file \"%s\" is not a valid EPS file\n"
msgstr "EPS-Datei \"%s\" ist keine g�ltige EPS-Datei\n"

#: src/psgen.c:1942
#, c-format
msgid "passing through %s file \"%s\"\n"
msgstr "durch %s die Datei \"%s\" weiterreichen\n"

# Mir ist unklar, was diese und die folgenden Meldungen meinen... -ke-
#: src/psgen.c:2044
#, c-format
msgid "couldn't create divert file name: %s"
msgstr "Datei mit abgeleitetem Namen kann nicht angelegt werden: %s"

#: src/psgen.c:2050
#, c-format
msgid "couldn't create divert file \"%s\": %s"
msgstr "Datei mit abgeleitetem Namen kann nicht angelegt werden \"%s\": %s"

#: src/psgen.c:2071
#, c-format
msgid "couldn't rewind divert file: %s"
msgstr "Datei mit abgeleitetem Namen kann nicht \"zur�ckgespuhlt\" werden: %s"

#: src/util.c:246 src/util.c:274
#, c-format
msgid "illegal value \"%s\" for option %s"
msgstr "ung�ltiger Wert \"%s\" f�r Option %s"

#: src/util.c:262
#, c-format
msgid "invalid value \"%s\" for option %s"
msgstr ""

#: src/util.c:424
#, c-format
msgid "illegal option: %s"
msgstr "ung�ltige Option: %s"

#: src/util.c:587
msgid "%s:%d: %%Format: no name"
msgstr "%s:%d: %%Format: keine Name"

#: src/util.c:597
msgid "%s:%d: %%Format: too long name, maxlen=%d"
msgstr "%s:%d: %%Format: zu langer Name, maxlen=%d"

#: src/util.c:619
msgid "%s:%d: %%Format: name \"%s\" is already defined"
msgstr "%s:%d: %%Format: Name \"%s\" ist schon definiert"

#: src/util.c:640
msgid "%s:%d: %%HeaderHeight: no argument"
msgstr "%s:%d: %%HeaderHeight: kein Parameter"

#: src/util.c:661
msgid "%s:%d: %%FooterHeight: no argument"
msgstr "%s:%d: %%FooterHeight: kein Parameter"

#: src/util.c:725
#, c-format
msgid "reading AFM info for font \"%s\"\n"
msgstr "lese AFM-Information f�r Font \"%s\"\n"

#: src/util.c:746
#, c-format
msgid "couldn't open AFM file for font \"%s\", using default\n"
msgstr ""
"kann AFM-Datei f�r Font \"%s\" nicht �ffnen, nehme Vorgabe (`default')\n"

#: src/util.c:752
#, c-format
msgid "couldn't open AFM file for the default font: %s"
msgstr "kann AFM-Datei f�r den Vorgabe-Font nicht �ffnen: %s"

#. Ok, fine.  Font was found.
#: src/util.c:869
#, c-format
msgid "downloading font \"%s\"\n"
msgstr "lade Font \"%s\"\n"

#: src/util.c:873
#, c-format
msgid "couldn't open font description file \"%s\": %s\n"
msgstr "kann Datei f�r Font-Beschreibung \"%s\" nicht �ffnen: %s\n"

#: src/util.c:1118
msgid "%%Format: too long format for %%D{} escape"
msgstr "%%Format: zu langes Format f�r escape-Zeichen %%D{}"

# ### was ist gemeint???
#: src/util.c:1202
msgid "%%Format: unknown `%%' escape `%c' (%d)"
msgstr "%%Format: unbekanntes `%%' escape-Zeichen '%c' (%d)"

#: src/util.c:1232
msgid "%%Format: no closing ')' for $() escape"
msgstr "%%Format: Keine schlie�ende ')' f�r das escape-Zeichen $()"

#: src/util.c:1234
msgid "%%Format: too long variable name for $() escape"
msgstr "%%Format: zu langer Variablen-Name f�r das escape-Zeichen $()"

#: src/util.c:1259
msgid "%%Format: too long format for $D{} escape"
msgstr "%%Format: zu langes Format f�r das escape-Zeichen $D{}"

#: src/util.c:1314
msgid "%%Format: unknown `$' escape `%c' (%d)"
msgstr "%%Format: unbekanntes `$' escape-Zeichen '%c' (%d)"

#: src/util.c:1480
#, c-format
msgid "malformed float dimension: \"%s\""
msgstr "fehlformatierte `float dimension': \"%s\""

#: src/util.c:1594
#, fuzzy, c-format
msgid "couldn't open input filter \"%s\" for file \"%s\": %s"
msgstr "kann Eingabedatei \"%s\" nicht �ffnen: %s"

#: src/util.c:1611
#, c-format
msgid "couldn't open input file \"%s\": %s"
msgstr "kann Eingabedatei \"%s\" nicht �ffnen: %s"

#: src/xalloc.c:40
#, c-format
msgid "xmalloc(): couldn't allocate %d bytes\n"
msgstr "xmalloc(): kann %d Bytes nicht unterbringen\n"

#: src/xalloc.c:53
#, c-format
msgid "xcalloc(): couldn't allocate %d bytes\n"
msgstr "xmalloc(): kann %d Bytes nicht unterbringen\n"

#: src/xalloc.c:69
#, c-format
msgid "xrealloc(): couldn't reallocate %d bytes\n"
msgstr "xrealloc(): kann %d Bytes nicht erneut unterbringen\n"

#~ msgid "couldn't unlink divert file \"%s\": %s"
#~ msgstr ""
#~ "Datei mit abgeleitetem Namen kann nicht abgel�st (`unlink') werden: %s"

#~ msgid ""
#~ "too long font name for ^@font escape:\n"
#~ "%.*s"
#~ msgstr ""
#~ "zu langer Fontname f�r das escape-Zeichen ^@font:\n"
#~ "%.*s"

#~ msgid "malformed ^@font escape: no '{' found"
#~ msgstr "fehlformatiertes escape-Zeichen ^@font: keine '{' gefunden"

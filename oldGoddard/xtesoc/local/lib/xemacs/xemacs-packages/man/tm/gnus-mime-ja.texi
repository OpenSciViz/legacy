\input texinfo.tex
@setfilename gnus-mime-ja.info
@settitle{gnus-mime 0.10 $B@bL@=q!JF|K\8lHG!K(B}
@direntry
* Gnus-MIME-Ja::           MIME extensions for Gnus ($BF|K\8lHG(B)
@end direntry
@titlepage
@title gnus-mime 0.10 $B@bL@=q!JF|K\8lHG!K(B
@author $B<i2,(B $BCNI'(B <morioka@@jaist.ac.jp>
@subtitle 1996/10/21
@end titlepage
@node Top, Introduction, (dir), (dir)
@top gnus-mime 0.10 $B@bL@=q!JF|K\8lHG!K(B

@ifinfo

This file documents gnus-mime, a MIME extension for Gnus.@refill

tm $B$r;H$C$F(B Gnus $B$N(B MIME $B5!G=$r6/2=$9$k$?$a$N(B package $B$G$"$k(B `gnus-mime' 
$B$K$D$$$F@bL@$7$^$9!#(B
@end ifinfo

@menu
* Introduction::                gnus-mime $B$C$F2?!)(B
* Automatic MIME Preview::      MIME message $B$N(B inline $BI=<((B
* mule::                        $B9q:]2=(B
* MIME-Edit::                   MIME message $B$N:n@.(B
* Concept Index::               $B35G0:w0z(B
* Function Index::              $B4X?t:w0z(B
* Variable Index::              $BJQ?t:w0z(B
@end menu

@node Introduction, Automatic MIME Preview, Top, Top
@chapter gnus-mime $B$C$F2?!)(B
@cindex gnus-mime

@strong{gnus-mime} $B$O(B tm (@ref{(tm-ja)tm-kernel}) $B$r;H$C$F(B Gnus
(@ref{(gnus)}) $B$N(B MIME (@ref{(tm-ja)MIME}) $B5!G=$r6/2=$9$k$?$a$N(B package 
$B$G$9!#(B

@noindent
@strong{[$BCm0U(B]}
@quotation

gnus-mime $B$O(B Gnus $B@lMQ$G$9!#(BGNUS $B$K$O(B tm-gnus (@ref{(tm-gnus_ja)}) $B$r;H(B
$B$$$^$9!#(B
@end quotation


gnus-mime $B$r;H$&$H(B tm-view (@ref{(tm-view-ja)}) $B$rMQ$$$F(B 
@code{"*Article*"} buffer $B$G(B MIME preview $B$r9T$&$3$H$,$G$-$^$9!#FC$K!"(B
XEmacs $B$G$O!"(Btext $B$HF1MM$K!"3($b(B inline $BI=<($r$9$k$3$H$,$G$-$^$9!#(B

$B$^$?!"(BMULE (@ref{(tm-ja)mule}), XEmacs/mule, mule $BE}9gHG(B Emacs $B$G$OB?8@(B
$B8l!&9q:]2=5!G=$,(B support $B$5$l!"(BMIME message $B$KBP$9$k(B charset $B$N(B support, 
$BHs(B MIME message $B$KBP$9$k(B Newsgroup $BKh$N(B default-mime-charset $B$N;XDj$r9T(B
$B$&$3$H$,$G$-$^$9!#(B@refill

$B$^$?!"(Boriginal $B$N(B Gnus $B$G$O(B iso-8859-1 (@ref{(tm-ja)iso-8859-1}) $B$N(B Q
encoding $B$N(B encoded-word (@ref{(tm-ja)encoded-word}) $B$N(B decode $B$7$+(B 
support $B$5$l$^$;$s$,!"(Bgnus-mime $B$G$O!"(Biso-8859-2
(@ref{(tm-ja)iso-8859-2}), iso-8859-3 (@ref{(tm-ja)iso-8859-3}), ...,
iso-2022-jp (@ref{(tm-ja)iso-2022-jp}), euc-kr (@ref{(tm-ja)euc-kr}),
iso-2022-kr (@ref{(tm-ja)iso-2022-kr}), hz-gb-2312
(@ref{(tm-ja)hz-gb-2312}), cn-big5 (@ref{(tm-ja)cn-big5}) $B$J$I!"(Bmule $B$G(B
$BMxMQ2DG=$J$5$^$6$^$J(B charset $B$N(B B $B$J$i$S$K(B Q encoding $B$N(B encoded-word $B$r(B
$B07$&$3$H$,$G$-$^$9!#$^$?!"(BGnus $B$h$j$b(B RFC 1522 $B$KCi<B$K=hM}$7$F$$$^$9!#(B


@node Automatic MIME Preview, mule, Introduction, Top
@chapter MIME message $B$N(B inline $BI=<((B
@cindex XEmacs
@cindex automatic MIME preview

gnus-mime $B$G$O(B @strong{automatic MIME preview} $B5!G=$rMxMQ$9$k$3$H$,$G$-(B
$B$^$9!#$3$N5!G=$r;H$&$H(B Summary mode $B$G5-;v$rFI$`;~!"DL>oI=<($5$l$k(B 
Article buffer $B$NBe$o$j$K(B tm-view $B$G=hM}$5$l$?(B preview buffer $B$rI=<($7$^(B
$B$9!#(B(cf. @ref{(tm-view-ja)}) @refill

$B$3$N$?$a!"(BBase64 (@ref{(tm-ja)Base64}) $B$d(B Quoted-Printable
(@ref{(tm-ja)Quoted-Printable}) $B$G(B encode $B$5$l$?5-;v$r(B decode $B$7$FI=<($9(B
$B$k$3$H$,$G$-$^$9!#$"$k$$$O!"(Btext/enriched (@ref{(tm-ja)text/enriched}) 
$B7A<0$N$h$&$J=qBN$dAHHG$K4X$9$k>pJs$r;}$C$?5-;v$r@07A$7$FI=<($G$-$^$9!#Ev(B
$BA3$N$3$H$J$,$i!"(Bmultipart (@ref{(tm-ja)multipart}) $B$N5-;v$b@5$7$/=hM}$G(B
$B$-$^$9!#(B@refill

$B$^$?!"(B@strong{XEmacs} $B$G$O(B text $B$HF1MM$K3($b(B preview buffer $BFb$GI=<($9$k(B
$B$3$H$,$G$-$^$9!#(B@refill

metamail $B$r;H$C$?>l9g$H0[$J$j!"2;$NF~$C$?5-;v$rFI$s$@$H$?$s!"$$$-$J$j!"(B
$B2;$,LD$j=P$7$?$j!"(Bvideo $B$NF~$C$?5-;v$rFI$`$?$S$K(B video $B$,:F@8$5$l$?$j!"(B
anonymous ftp (@ref{(tm-ja)ftp}) $B$d(B mail-server $B$rMxMQ$7$?(B 
external-message $B$NF~$C$?5-;v$rFI$`$?$S$K(B anonymous ftp $B$r<B9T$7$?$j!"(B
mail $B$rAw$C$?$j$9$k$3$H$,KI$2$^$9!#$3$&$7$?(B content $B$O(B preview buffer $B$N(B
$BCf$KF~$C$F:F@8(B command $B$r<B9T$7$?;~$N$_:F@8$5$l$^$9!#(B@refill

$B$7$+$7$J$,$i!"CY$$(B machine $B$r;H$C$F$$$F!"(BMIME $B=hM}$N(B overhead $B$,5$$K$J(B
$B$k>l9g!"(Bautomatic MIME preview $B$rM^;_$9$k$3$H$,$G$-$^$9!#(B


@menu
* MIME processing::             MIME $B=hM}$N;EAH$_(B
* Two buffers for an article::  Automatic MIME preview $B$N;EAH$_(B
@end menu

@node MIME processing, Two buffers for an article, Automatic MIME Preview, Automatic MIME Preview
@section MIME $B=hM}$N;EAH$_(B

@code{gnus-show-mime} $B$,(B @code{nil} $B$G$J$/!"$+$D!"(Barticle $B$,(B 
Mime-Version field $B$r;}$C$F$$$k>l9g!"B($A!"$=$N(B article $B$,(B MIME message 
$B$N>l9g!"(B@code{gnus-show-mime-method} $B$,8F$P$l$F(B MIME $B=hM}$,9T$J$o$l$^$9!#(B
@refill

@code{gnus-strict-mime} $B$,(B @code{nil} $B$N>l9g!"(B@code{gnus-show-mime} $B$,(B 
@code{nil} $B$G$J$1$l$P>o$K(B @code{gnus-show-mime-method} $B$,8F$P$l$F(B MIME 
$B=hM}$,9T$J$o$l$^$9!#(B@refill

Gnus 5.2 $B0J9_$G$O!"(B@code{gnus-strict-mime} $B$,(B @code{nil} 
$B$G$J$/!"$+$D!"(Barticle $B$K(B Mime-Version field $B$,$J$$>l9g!"(B
@code{gnus-show-mime-method} $B$,8F$P$l$kBe$o$j$K(B 
@code{gnus-decode-encoded-word-method} $B$,8F$P$l$^$9!#$G$b!"8E$$(B 
Gnus $B$G$OM-8z$G$O$J$$$N$G!"$3$N>l9g!"(B@code{gnus-strict-mime} $B$O(B 
@code{nil} $B$K$7$F$*$$$?J}$,NI$$$G$7$g$&!#(B


@defvar gnus-show-mime

$B$3$NJQ?t$,(B @code{nil} $B$G$J$$$H$-!"(Bautomatic MIME preview $B$,9T$J$o$l$^$9!#(B
$B4{DjCM$O(B @code{t} $B$G$9!#(B@refill

$B$3$NJQ?t$O(B Gnus $B$NJQ?t$J$N$G!"(Bgnus-mime $B$,(B load $B$5$l$kA0$K(B gnus $B$,(B 
load $B$5$l$?>l9g!"4{DjCM$OJ]>Z$5$l$^$;$s!#(B

(cf. @ref{(gnus)Using MIME})
@end defvar


@defvar gnus-show-mime-method

MIME $B=hM}$r9T$J$&;~$K!"$3$NJQ?t$KB+G{$5$l$?4X?t$,8F$P$l$^$9!#4{DjCM$O(B 
@code{gnus-article-preview-mime-message} $B$G$9!#(B@refill

$B$3$NJQ?t$O(B Gnus $B$NJQ?t$J$N$G!"(Bgnus-mime $B$,(B load $B$5$l$kA0$K(B gnus $B$,(B 
load $B$5$l$?>l9g!"4{DjCM$OJ]>Z$5$l$^$;$s!#(B

(cf. @ref{(gnus)Using MIME})
@end defvar


@defvar gnus-decode-encoded-word-method

@code{gnus-show-mime} $B$H(B @code{gnus-strict-mime} $B$,$H$b$K(B @code{nil} $B$G(B
$B$J$$>l9g$K!"(BMime-Version field $B$,$J$$(B article $B$KBP$7$F$3$NJQ?t$KB+G{$5$l(B
$B$?4X?t$,8F$P$l$^$9!#4{DjCM$O!"(B@code{gnus-article-decode-encoded-word} $B$G(B
$B$9!#(B@refill

$B$3$NJQ?t$O(B Gnus $B$NJQ?t$J$N$G!"(Bgnus-mime $B$,(B load $B$5$l$kA0$K(B gnus $B$,(B load 
$B$5$l$?>l9g!"4{DjCM$OJ]>Z$5$l$^$;$s!#(B@refill

$B$^$?!"(BGnus 5.0.* $B$*$h$S(B Gnus 5.1 $B$G$OL58z$G$9!#(B
@end defvar


@noindent
@strong{[$BCm0U(B]}
@quotation

gnus-mime $B$O(B Gnus $B$NJQ?t$rMQ$$!"$3$l$r(B gnus-mime $B$N=i4|CM$G(B@code{defvar} 
$B$7$F$$$^$9!#Nc$($P!"(B@code{gnus-show-mime} $B$O(B @code{nil}$B$G$O$J$/(B @code{t} 
$B$G!"(B@code{gnus-show-mime-method} $B$O(B @code{metamail-buffer} $B$G$O$J$/(B
@code{gnus-article-preview-mime-message} $B$G(B @code{defvar} $B$7$F$$$^$9!#(B
(cf. @ref{(gnus)Using MIME}) @refill

@code{setq} $B$G$O$J$/(B @code{defvar} $B$rMQ$$$F$$$k$N$O(B gnus-mime $B$,(B load$B$5(B
$B$l$kA0$K(B @file{~/.emacs} $BEy$G(B user $B$,9T$C$?@_Dj$rB:=E$9$k$?$a$G$9$,!"$3(B
$B$N$3$H$OF1;~$K!"$b$7(B gnus-mime $B$,(B load $B$5$l$kA0$K(B gnus $B$,(B load $B$5$l$l$P(B 
gnus-mime $B$N5!G=$OI8=`$G$OF/$+$J$$$3$H$r0UL#$7$F$$$^$9!#$h$C$F!"(B
@file{~/.gnus} $BEy$G(B gnus-mime $B$d(B mime-setup $B$r(B load $B$9$k$3$H$K$OLdBj$,$"(B
$B$j$^$9!#$I$&$7$F$b$=$&$7$?$$>l9g$O!"(B@code{gnus-show-mime} $B$d(B
@code{gnus-show-mime-method} $B$N@_Dj$r9T$C$F$/$@$5$$!#(B
@end quotation



@node Two buffers for an article,  , MIME processing, Automatic MIME Preview
@section Automatic MIME preview $B$N;EAH$_(B
@cindex preview buffer
@cindex raw article buffer

Gnus 5.2 $B0J9_$G$O(B Gnus $B$O#1$D$N(B article $B$KBP$7$F(B 
@code{gnus-original-article-buffer} (@code{" *Original Article*"}) $B$H(B
@code{gnus-article-buffer} (@code{"*Article*"}) $B$H$$$&#2$D$N(B buffer $B$r;}$C(B
$B$F$$$^$9!#$3$3$G$OA0<T$r(B @strong{raw article buffer}, $B8e<T$r(B 
@strong{preview buffer} $B$H8F$V$3$H$K$7$^$9!#(B@refill

raw article buffer $B$K$O2?$b=hM}$r$5$l$F$$$J$$!X@8$N(B article$B!Y$,F~$C$F$$(B
$B$^$9!#FC$K!"(Bmule (@ref{(tm-ja)mule}) $B$G$O(B code $BJQ49$r9T$J$o$J$$>uBV$N5-(B
$B;v$,F~$j$^$9!#(B@refill

preview buffer $B$O(B raw article buffer $B$r85$K(B MIME $B=hM}$r9T$J$C$F@07A$5$l(B
$B$?5-;v$,F~$j$^$9!#FC$K!"(Bmule $B$N>l9g!"$3$NCf$G!"(Bcharset $B$b$7$/$O(B 
newsgroup $BKh$K;XDj$5$l$?J8;z(B code $B$G$N(B code $BJQ49$,9T$J$o$l$^$9!#(B@refill

Gnus 5.0.* $B$*$h$S(B Gnus 5.1 $B$G$O(B raw article buffer $B$H(B preview buffer $B$N(B
$B6hJL$O$"$j$^$;$s$,(B gnus-mime $B$,L5M}LpM}(B raw article buffer $B$r:n$j=P$7$F(B
$B$$$^$9!#$h$C$F!"FC$KM}M3$,$J$1$l$P(B Gnus 5.2 $B0J9_$r;H$o$l$k$3$H$r$*4+$a$7(B
$B$^$9!#(B@refill

(cf. @ref{(tm-view-ja)Two buffers for an article})



@node mule, MIME-Edit, Automatic MIME Preview, Top
@chapter $B9q:]2=(B

MULE (@ref{(tm-ja)mule}), XEmacs/mule, $B$*$h$S!"(Bmule $BE}9gHG(B Emacs (*1) $B$J(B
$B$I$N(B mule $B5!G=$r;}$C$?(B emacs $B$G$O(B MIME charset (@ref{(tm-ja)MIME charset}) $B$*$h$S!"Hs(B MIME message/part $B$KBP$9$k(B newsgroup $BKh$K;XDj$5$l$?(B 
@code{default-mime-charset} $B$G$N(B code $BJQ49$,9T$o$l$^$9!#(B

@noindent
@strong{[Memo]}
@quotation
(*1) 1996 $BG/(B 10 $B7n8=:_!"H>ED(B $B7u0l(B $B;a$H(B RMS $B;a$,3+H/Cf$N(B Emacs $B$G!"(BMULE
$B$N5!G=$N$&$A!"B?8@8l2=$H9q:]2=$K4X$9$k5!G=$,(B merge $B$5$l$?$b$N$r;X$7$F(B
$B$$$^$9!#(B
@end quotation


$B6qBNE*$K$O!"(Bcode $BJQ49$O<!$N$h$&$K9T$o$l$^$9!'(B

@enumerate
@item
$B$"$k(B newsgroup $B$rA*Br$9$k$H!"$=$N(B newsgroup $B$KBP1~$9$k(B default $B$N(B MIME
charset $B$r(B @code{gnus-newsgroup-default-charset-alist} $B$+$iA\$7!"(B
$B$b$7!"8+IU$+$l$P!"(BSummary Buffer $B$N(B buffer local $BJQ?t(B
@code{default-mime-charset} $B$K$=$l$r@_Dj$9$k!#$b$7!"$J$1$l$P!"(B
@code{default-mime-charset} $B$NBg0hCM$,MQ$$$i$l$k!#(B

@item
Summary Buffer $BCf$NHs(B ASCII $BJ8;z$O(B @code{default-mime-charset} 
$B$N(B Summary Buffer $B>e$G$NCM$G(B code $BJQ49$5$l$k!#(B

@item
encoded-word (@ref{(tm-ja)encoded-word}) $B$N>l9g!"@k8@$5$l$?(B MIME charset
(@ref{(tm-ja)MIME charset}) $B$G(B code $BJQ49$r9T$&!#(B

@item
article $B$N(B code $BJQ49$O!'(B

@enumerate
@item
$B$b$7!"$"$k(B MIME message $B$N$"$k(B part $B$,(B Content-Type field
(@ref{(tm-ja)Content-Type field}) $B$K(B charset parameter $B$r;}$C$F$$$l$P!"(B
$B$=$N(B MIME charset $B$rMQ$$$F(B code $BJQ49$r9T$&!#(B

@item
message header $B$dHs(B MIME message $B$J$I!"(Bcharset $B$,@k8@$5$l$F$$$J$$>l9g!"(B
Summary Buffer $B>e$N(B @code{default-mime-charset} $B$NCM$G(B code $BJQ49(B
$B$5$l$k!#(B
@end enumerate
@end enumerate


@defvar gnus-newsgroup-default-charset-alist

newsgroup $B$KBP1~$9$k@55,I=8=$H(B default MIME charset $B$rI=$9(B symbol $B$NBP$+(B
$B$i$J$kO"A[(B list.@refill

$B4{DjCM$G$O!"(Balt.chinese.* $B$G$O(B hz-gb-2312 (@ref{(tm-ja)hz-gb-2312}),
alt.chinese.text.big5, hk.*, hkstar.*, tw.* $B$G$O(B cn-big5
(@ref{(tm-ja)cn-big5}), fj.* $B$G$O(B iso-2022-jp-2
(@ref{(tm-ja)iso-2022-jp-2}), han.* $B$G$O(B euc-kr (@ref{(tm-ja)euc-kr}),
relcom.* $B$G$O(B koi8-r (@ref{(tm-ja)koi8-r}) $B$,;XDj$5$l$F$$$^$9!#(B@refill

$B$"$k(B newsgroup $B$KBP1~$9$k(B MIME charset $B$,B8:_$7$J$$>l9g$O(B
@code{default-mime-charset} $B$,MQ$$$i$l$^$9!#(B
@end defvar


@defun gnus-set-newsgroup-default-charsetnewsgroup charset

@var{newsgroup} $B$KBP$9$k(B default MIME charset $B$r(B @var{charset} $B$K@_Dj$9(B
$B$k!#(B@refill

@var{newsgroup} $B$O(B newsgroup $BL>$+$=$N0lIt$+$i$J$kJ8;zNs!#!JNc!'(B
@code{"gnu.emacs.gnus"}, @code{"gnu.emacs"}, @code{"gnu"}$B!K(B@refill

@var{charset} $B$O(B MIME charset $B$rI=$9(B symbol. $B!JNc!'(B@code{iso-8859-1},
@code{iso-2022-jp}$B!K(B
@end defun



@node MIME-Edit, Concept Index, mule, Top
@chapter MIME message $B$N:n@.(B

mime-setup (@ref{(tm-ja)mime-setup}) $B$r;H$C$F$$$k>l9g!"(Btm-edit
(@ref{(tm-edit-ja)}) $B$rMQ$$$?(B MIME message $B$N:n@.$r9T$&$3$H$,$G$-$^$9!#(B

@noindent
@strong{[$BCm0U(B]}
@quotation

mime-setup $B$NI8=`@_Dj$G$O(B signature $B$N<+F0A^F~$,M^@)$5$l$^$9!#(B

(cf. @ref{(tm-ja)mime-setup})
@end quotation


@kbd{C-c C-x C-m} (@code{mime-editor/insert-mail}) $B$KBP$9$k<BAu$OI8=`$G(B
$B$OM?$($i$l$^$;$s!#$3$l$O!"(BGnus $B$HAH$_9g$o$;$k$Y$-(B mail reader $B$r$I$&$9$l(B
$B$PNI$$$+$,H=$i$J$$$+$i$G$9!#$^$?!"BgDq$N>l9g!"(B@kbd{C-c C-x C-y}
(@code{mime-editor/insert-message}) $B$G;vB-$j$k!JB($A!"A^F~$7$?$$(Bmessage 
$B$rA*Br$7$F$=$N(B @code{"*Article*"} buffer $B$rI=<($9$l$P$=$N(Bmessage $B$r(B 
@kbd{C-c C-x C-y} $B$GA^F~$9$k$3$H$,$G$-$^$9!K$+$i$G$9!#(B@refill

$B$7$+$7$J$,$i!"(B@code{mime-editor/mail-inserter-alist} $B$N(B
@code{message-mode} $B$NCM$K@_Dj$7$?$$<BAu$r;XDj$9$k$3$H$G(B 
@kbd{C-c C-x C-m} $B$rMxMQ$9$k$3$H$,$G$-$^$9!#(B


@section $BNc!'(B@kbd{C-c C-x C-m} $B$HF1$8F0:n$r$5$;$k>l9g(B

@lisp
(set-alist 'mime-editor/mail-inserter-alist
	   'message-mode (function message-mime-insert-article))
@end lisp



@section $BNc!'(BMH $B$N(B folder $B$+$i<h$j9~$`>l9g(B

@lisp
(autoload 'tm-mh-e/insert-mail "tm-mh-e")

(set-alist 'mime-editor/mail-inserter-alist
	   'message-mode (function tm-mh-e/insert-mail))
@end lisp



@node Concept Index, Function Index, MIME-Edit, Top
@chapter $B35G0:w0z(B

@printindex cp

@node Function Index, Variable Index, Concept Index, Top
@chapter $B4X?t:w0z(B

@printindex fn

@node Variable Index,  , Function Index, Top
@chapter $BJQ?t:w0z(B

@printindex vr
@bye

(goto-char (point-min))
(when (search-forward "**end of header")
  (forward-line 1)
  (beginning-of-line)
  (insert "@direntry\n")
  (insert "* Mew: (mew.jis)                  Messaging in the Emacs World($BF|K\8lHG(B)\n")
  (insert "@end direntry\n"))

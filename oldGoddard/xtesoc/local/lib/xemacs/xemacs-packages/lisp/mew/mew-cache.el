;;; mew-cache.el --- Cache management for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar 23, 1997
;; Revised: Aug 26, 1997

;;; Code:

(defconst mew-cache-version "mew-cache.el version 0.05")

(require 'mew)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Prepare new message --- caching
;;

(defmacro mew-cache-decode-syntax (buf)
  (` (save-excursion
       (set-buffer (, buf))
       mew-decode-syntax)))

(defmacro mew-cache-decode-error (buf)
  (` (save-excursion
       (set-buffer (, buf))
       mew-decode-error)))

(defvar mew-cache nil
      "Mail cache. (old ... new) order alist with association
 ((\"+folder\" . \"message\") . cache-buffer)"
      )

(defmacro mew-cache-buffer-get (entry)
  (` (cdr (, entry))))

(defmacro mew-cache-folder-get (entry)
  (` (car (car (, entry)))))

(defmacro mew-cache-message-get (entry)
  (` (cdr (car (, entry)))))

(defmacro mew-cache-entry-make (fld-msg buf)
  (` (cons (, fld-msg) (, buf))))

(defmacro mew-cache-hit (fld-msg)
  "Return value assosiated with key."
  (` (mew-cache-buffer-get (assoc (, fld-msg) mew-cache))))

(defun mew-cache-sort (entry)
  (let* ((pointer (cons nil mew-cache))
	 (top pointer))
    (while (cdr pointer)
      (if (equal (car (cdr pointer)) entry)
	  (setcdr pointer (cdr (cdr pointer)))
	(setq pointer (cdr pointer))))
    (setcdr pointer (list entry))
    (setq mew-cache (cdr top))))

(defun mew-cache-add (fld-msg)
  "Rutern associating buffer"
  (let ((len (length mew-cache))
	(buf nil))
    (if (>= len mew-cache-size)
	(prog1
	    (setq buf (mew-cache-buffer-get (car mew-cache)))
	  (setq mew-cache (nconc (cdr mew-cache)
				  (list (mew-cache-entry-make fld-msg buf))))
	  )
      (prog1
	  (setq buf (get-buffer-create 
		     (concat mew-buffer-cache (int-to-string len))))
	(setq mew-cache (nconc mew-cache
				(list (mew-cache-entry-make fld-msg buf))))
	))
    ))

(defun mew-cache-delete ()
  "Delete the most recent cache entry."
  (setq mew-cache (nreverse (cdr (nreverse mew-cache)))))

(defun mew-cache-attribute-get (file)
  ;; 5th is modified time
  ;; 7th is file size
  (let ((attr (file-attributes file)))
    (list (nth 5 attr)
	  (nth 7 attr))))
       
(defun mew-cache-message (fld msg)
  (let ((hit (mew-cache-hit (cons fld msg)))
	(decode nil))
    (if hit
	(save-excursion
	  (mew-cache-sort (mew-cache-entry-make (cons fld msg) hit))
	  (set-buffer hit)
	  (if (and (not (mew-folder-newsp fld))
		   (not (equal mew-cache-attribute ;; buffer-local 
			       (mew-cache-attribute-get
				(mew-expand-folder fld msg)))))
	      ;; cache is invalid
	      (setq decode t)))
      (setq hit (mew-cache-add (cons fld msg)))
      (setq decode t))
    (if decode
	(condition-case nil
	    (save-excursion
	      (set-buffer hit)
	      ;; in cache buffer
	      (mew-decode fld msg))
	  (quit
	   (mew-cache-delete)
	   (error "MIME decoding is quitted."))))
    hit ;; retrun value
    ))

(defun mew-cache-flush ()
  "A function to flush all decoded messages in cache list."
  (interactive)
  (mew-decode-syntax-delete)
  (let ((n 0))
    (while (< n mew-cache-size)
      (mew-kill-buffer (concat mew-buffer-cache (int-to-string n)))
      (setq n (1+ n))
      ))
  (mew-current-set 'cache nil)
  (mew-current-set 'message nil)
  (mew-current-set 'part nil)
  (setq mew-cache nil)
  )

(provide 'mew-cache)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-cache.el ends here

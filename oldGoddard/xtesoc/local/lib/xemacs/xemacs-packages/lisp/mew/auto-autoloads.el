;;; DO NOT MODIFY THIS FILE
(if (featurep 'mew-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "mew/_pkg.el")

(package-provide 'mew :version 1.08 :type 'regular)

;;;***

(provide 'mew-autoloads)

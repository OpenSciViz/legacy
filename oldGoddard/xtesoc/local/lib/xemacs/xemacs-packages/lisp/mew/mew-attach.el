;;; mew-attach.el --- attachments for Mew Draft mode

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Oct  2, 1996
;; Revised: Sep 18, 1997

;;; Code:

(defconst mew-attach-version "mew-attach.el version 0.16")

(require 'mew)

(defvar mew-draft-attach-map nil
  "A region map for attachments in Draft mode")

(if mew-draft-attach-map
    ()
  (setq mew-draft-attach-map (make-keymap))
  (let ((begin ?\0) (end ?\177))
    (while (<= begin end)
      (define-key mew-draft-attach-map 
	(char-to-string begin) 'mew-draft-null-function)
      (setq begin (1+ begin))))
  (define-key mew-draft-attach-map "\C-f" 'mew-attach-forward)
  (define-key mew-draft-attach-map "\C-b" 'mew-attach-backforward)
  (define-key mew-draft-attach-map "\C-n" 'mew-attach-next)
  (define-key mew-draft-attach-map "\C-p" 'mew-attach-previous)
  (define-key mew-draft-attach-map "a"    'mew-attach-audio)
  (define-key mew-draft-attach-map "c"    'mew-attach-copy)
  (define-key mew-draft-attach-map "C"    'mew-attach-charset)
  (define-key mew-draft-attach-map "d"    'mew-attach-delete)
  (define-key mew-draft-attach-map "D"    'mew-attach-description)
  (define-key mew-draft-attach-map "P"    'mew-attach-disposition)
  (define-key mew-draft-attach-map "e"    'mew-attach-external-body)
  (define-key mew-draft-attach-map "f"    'mew-attach-find-file)
  (define-key mew-draft-attach-map "F"    'mew-attach-find-new-file)
  (define-key mew-draft-attach-map "l"    'mew-attach-link)
  (define-key mew-draft-attach-map "m"    'mew-attach-multipart)
  (define-key mew-draft-attach-map "T"    'mew-attach-type)
  (define-key mew-draft-attach-map "G"    'mew-attach-gzip64)
  (define-key mew-draft-attach-map "B"    'mew-attach-base64)
  (define-key mew-draft-attach-map "Q"    'mew-attach-quoted-printable)
  (define-key mew-draft-attach-map "S"    'mew-attach-pgp-sign)
  (define-key mew-draft-attach-map "E"    'mew-attach-pgp-enc)
  (define-key mew-draft-attach-map "p"    'mew-attach-pgp-public-key)
  (define-key mew-draft-attach-map "U"    'mew-attach-undo)
  (define-key mew-draft-attach-map "\C-y" 'mew-attach-dummy)
  )

(defconst mew-draft-attach-boundary-begin
  "------------------------------ attachments ------------------------------")
(defconst mew-draft-attach-boundary-end
  "--------0-1-2-3-4-5-6-7-8-9----------------------------------------------")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; goodies
;;

(defmacro mew-attach-p ()
  (` (markerp mew-draft-buffer-attach))
  )

(defun mew-draft-prepare-attachments ()
  "Prepare an attachment region in the bottom of the draft.
To compose a multipart message, you should execute this command first."
  (interactive)
  (let* ((mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	 (cp (expand-file-name mew-draft-coverpage mimedir))
	 (dir (file-name-nondirectory mimedir)))
    (if (mew-attach-p)
	(message "Attachments already exist.")
      (mew-attach-insert-boundary)
      (if (not (file-exists-p mimedir)) (mew-make-directory mimedir))
      (if (not (file-exists-p cp)) 
	  (write-region 1 1 cp 'append 'no-msg)) ;; touch
      (if (not mew-encode-syntax)
	  (setq mew-encode-syntax (mew-encode-syntax-initial dir)))
      (mew-encode-syntax-print mew-encode-syntax)
      (mew-attach-goto-number 'here '(2))
      (if (not (pos-visible-in-window-p (point-max)
					(get-buffer-window (current-buffer))))
	  (scroll-up 1)) ;; xxx
      (setq buffer-undo-list nil))))

(defun mew-attach-insert-boundary ()
   (goto-char (point-max))
   (if (null (bolp))(insert "\n"))
   (if (mew-attach-p)
       (set-marker mew-draft-buffer-attach (point))
     (setq mew-draft-buffer-attach (point-marker)))
   (insert mew-draft-attach-boundary-begin)
   (insert "\n")
   (insert mew-draft-attach-boundary-end)
   (insert "\n")
   (beginning-of-line)
   )

(defun mew-attach-nums ()
  (let ((nums (mew-syntax-number)))
    (if nums
	(mew-split-number nums)
      nil)
    ))

(defun mew-attach-expand-path (syntax nums)
  "ignore the first dir"
  (let ((path ""))
    (while (cdr nums)
      (setq syntax (aref syntax (+ mew-syntax-magic (1- (car nums)))))
      (setq path (concat path (mew-syntax-get-file syntax)))
      (setq nums (cdr nums))
      )
    path
    ))

(defun mew-attach-line ()
  "Return the line number for the cursor from the beginning of the 
attachments region. 
0 if on the beginning boundary. 
-1 if on the ending boundary."
  (let (ret max)
    (save-excursion
      (beginning-of-line)
      (setq ret (count-lines (marker-position mew-draft-buffer-attach)(point)))
      (goto-char (point-max))
      (beginning-of-line)
      (setq max (count-lines (marker-position mew-draft-buffer-attach)(point)))
      (if (or (= ret max) (= ret (1- max))) -1 ret)
      )
    ))

(defun mew-attach-move-onto-file ()
  (end-of-line)
  (re-search-backward " [^ ]")
  (forward-char))

(defun mew-attach-move-onto-body ()
  (goto-char (marker-position mew-draft-buffer-attach))
  (forward-char -1))

(defun mew-attach-directory-p ()
  (save-excursion
    (mew-attach-move-onto-file)
    (looking-at (concat "[^ ]+" mew-path-separator))))

(defun mew-attach-dot-p ()
  (save-excursion
    (mew-attach-move-onto-file)
    (looking-at "\\.$")))

;; cursor only
(defun mew-attach-line0-1 ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(or (= line 0) (= line -1)))))

;; cursor only
(defun mew-attach-line01-1 ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(or (= line 0) (= line 1) (= line -1)))))

;; insert commands
(defun mew-attach-not-line012-1 ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(not (or (= line 0) (= line 1) (= line 2) (= line -1))))))

;; find commands
(defun mew-attach-not-line012-1-dot ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(not (or (mew-attach-dot-p)
		 (= line 0) (= line 1) (= line 2) (= line -1))))))


;; delete commands
 
(defun mew-attach-not-line02-1-dot ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(not (or (mew-attach-dot-p) (= line 0) (= line 2) (= line -1))))))


;; modify commands
(defun mew-attach-not-line0-1-dot ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(not (or (mew-attach-dot-p) (= line 0) (= line -1))))))

;; corsor
(defun mew-attach-line1-dot ()
  (if (mew-attach-p)
      (let ((line (mew-attach-line)))
	(or (mew-attach-dot-p) (= line 1)))))

(defun mew-attach-goto-number (direction nums)
  (let (numreg)
    (setq nums (nreverse nums))
    (cond
     ((equal direction 'next)
      (setq nums (cons (1+ (car nums)) (cdr nums)))
      )
     ((equal direction 'prev)
      (setq nums (cons (1- (car nums)) (cdr nums)))
      )
     ((equal direction 'up)
      (setq nums (cdr nums))
      )
     ((equal direction 'down)
      (setq nums (cons 1 nums))
      )
     (t ())
     )
    (if (null nums)
	(progn
	  (goto-char (marker-position mew-draft-buffer-attach))
	  (re-search-forward "^..... ")
	  (mew-attach-move-onto-file))
      (setq numreg (int-to-string (car nums)))
      (setq nums (cdr nums))
      (while nums
	(setq numreg (concat (int-to-string (car nums)) "." numreg))
	(setq nums (cdr nums))
	)
      (setq numreg (regexp-quote numreg))
      (goto-char (marker-position mew-draft-buffer-attach))
      (re-search-forward (concat "^....." numreg " "))
      (mew-attach-move-onto-file)
      )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; commands
;;

;;
;; cursor commands
;;

(defun mew-attach-forward ()
  "Go to the first subdirectory in attachments."
  (interactive) 
  (if (mew-attach-line0-1)
      (mew-attach-goto-number 'here nil)
    (mew-attach-move-onto-file)
    (if (mew-attach-directory-p)
	(mew-attach-goto-number 'down (mew-attach-nums))
      (mew-attach-goto-number 'here (mew-attach-nums)))
    ))

(defun mew-attach-backforward ()
  "Go to the parent directory in attachments."
  (interactive)
  (if (mew-attach-line0-1)
      (mew-attach-move-onto-body)
    (let ((nums (mew-attach-nums)))
      (if (= (length nums) 1)
	  (mew-attach-goto-number 'here nil)
	(mew-attach-goto-number 'up (mew-attach-nums))))))

(defun mew-attach-previous ()
  "Go to the previous file in the current directory in attachments."
  (interactive)
  (if (mew-attach-line01-1)
      (mew-attach-move-onto-body)
  (let* ((nums (mew-attach-nums))
	 (last (nth (1- (length nums)) nums)))
    (if (= last 1)
	(mew-attach-goto-number 'here (mew-attach-nums))
      (mew-attach-goto-number 'prev (mew-attach-nums))))))


(defun mew-attach-next ()
  "Go to the next file in the current directory in attachments."
  (interactive)
  (cond
   ((mew-attach-line0-1)
    (mew-attach-goto-number 'here nil))
   ((mew-attach-line1-dot)
    (mew-attach-goto-number 'here (mew-attach-nums)))
   (t
    (mew-attach-goto-number 'next (mew-attach-nums)))
   ))

;;
;; delete commands
;;

(defun mew-attach-delete ()
  "Delete this file or this directory in attachments."
  (interactive)
  (if (not (mew-attach-not-line02-1-dot))
      (message "Can't delete here.")
    (if (equal (mew-attach-line) 1)
	(if (y-or-n-p "Delete entire attachments? ")
	    (progn
	      (mew-delete-directory-recursively
	       (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	      (setq mew-encode-syntax nil)
	      (goto-char (marker-position mew-draft-buffer-attach))
	      (delete-region (point) (point-max))
	      (setq mew-draft-buffer-attach nil)
	      (message "attachments were deleted"))
	  (message "attachments were not deleted"))
      (let* ((nums (mew-attach-nums))
	     (subdir (mew-attach-expand-path mew-encode-syntax nums))
	     (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	     (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	     (name (mew-syntax-get-file syntax))
	     (ename (if (equal subdir "") name (concat subdir name)))
	     (fullname (expand-file-name ename mimedir))
	     (dirp (string-match (concat mew-path-separator "$") name))
	     (msg (if dirp
		      "Delete %s with contained files? "
		    "Delete %s? ")))
	;; mimedir / {subdir/} name
	(if (not (y-or-n-p (format msg ename)))
	    ()
	  (message "Deleting %s ... " ename)
          (if (file-exists-p fullname)
              (if dirp
                  (mew-delete-directory-recursively fullname)
                (delete-file fullname)))
	  (message "Deleting %s ... done" ename)
	  (setq mew-encode-syntax
		(mew-syntax-remove-entry mew-encode-syntax nums))
	  (mew-encode-syntax-print mew-encode-syntax)
	  ))
      )))

;;
;; insert commands
;;

(defun mew-attach-multipart ()
  "Create a subdirectory(i.e. multipart) on \".\" in attachments."
  (interactive)
  (if (not (mew-attach-not-line012-1))
      (message "Can't create a sub-multipart here.")
    (let* ((nums (mew-attach-nums))
	   (subdir (mew-attach-expand-path mew-encode-syntax nums))
	   (insert-default-directory nil)
	   (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	   dir dirpath)
      ;; mimedir / {subdir/} dir
      (if (not (equal subdir "")) 
	  (setq mimedir (expand-file-name subdir mimedir)))
      ;; mimedir / dir
      (setq dirpath (mew-random-filename mimedir))
      (if (file-exists-p dirpath)
	  (message "Could not make a directory, sorry")
	(setq dir (file-name-nondirectory dirpath))
	(mew-make-directory dirpath)
	(setq mew-encode-syntax
	      (mew-syntax-insert-entry
	       mew-encode-syntax 
	       nums
	       (mew-encode-syntax-multi dir mew-type-mlm)))
	(mew-encode-syntax-print mew-encode-syntax))
      )))

(defun mew-attach-duplicate (func action setmode &optional from to)
  (interactive)
  (if (not (mew-attach-not-line012-1))
      (message "Can't %s here." action)
    (let* ((nums (mew-attach-nums))
	   (subdir (mew-attach-expand-path mew-encode-syntax nums))
	   (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	   (frompath from)
	   (efile (and to (concat subdir to)))
	   (tofile to))
      (while (or (null frompath)
		 (file-directory-p (file-chase-links frompath)))
	(setq frompath (mew-input-file-name
			(concat (capitalize action) " from : "))))
      (while (or (null efile)
		 (file-exists-p (expand-file-name efile mimedir)))
	(setq tofile (mew-input-string 
		      (concat (capitalize action) " to %s(%s): ")
		      subdir
		      (file-name-nondirectory frompath)))
	(setq efile (concat subdir tofile)))
      (funcall func frompath (expand-file-name efile mimedir))
      (if setmode (set-file-modes (expand-file-name efile mimedir)
				  mew-file-mode))
      (setq mew-encode-syntax
	    (mew-syntax-insert-entry
	     mew-encode-syntax 
	     nums
	     (mew-encode-syntax-single tofile nil nil nil nil 'cdp)))
      (mew-encode-syntax-print mew-encode-syntax)
      )))

(defun mew-attach-link (&optional from to)
  "Link a file with a symbolic link on \".\" in attachments.
FROM must be absolute path. TO must be a file name."
  (interactive)
  (mew-attach-duplicate (function mew-symbolic-link) "link" nil from to))

(defun mew-attach-copy (&optional from to)
  "Copy a file (via networks) on \".\" in attachments.
FROM must be absolute path. TO must be a file name.
To copy a remote file, use the \"/[user@]hostname:/filepath\" syntax."
  (interactive)
  (mew-attach-duplicate (function copy-file) "copy" t from to))

(defun mew-attach-find-new-file ()
  "Open a new file into a buffer on \".\" in attachments."
  (interactive)
  (if (not (mew-attach-not-line012-1))
      (message "Can't find a new file here.")
    (let* ((nums (mew-attach-nums))
	   (subdir (mew-attach-expand-path mew-encode-syntax nums))
	   (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	   file filepath)
      ;; mimedir / {subdir/} dir
      (if (not (equal subdir "")) 
	  (setq mimedir (expand-file-name subdir mimedir)))
      ;; mimedir / file
      (setq filepath (mew-random-filename mimedir mew-text-suffix))
      (if (null filepath)
	  (message "Could not make a text file, sorry.")
	(setq file (file-name-nondirectory filepath))
	(setq mew-encode-syntax
	      (mew-syntax-insert-entry
	       mew-encode-syntax 
	       nums
	       (mew-encode-syntax-single file (list mew-ct-txt) ;; for charset
					 nil nil nil nil)))
	(mew-encode-syntax-print mew-encode-syntax)
	;;
	(find-file filepath)
	(text-mode)
	;; buffer switched
	(local-set-key "\C-c\C-q" 'mew-kill-buffer)
	))
    ))

(defun mew-attach-audio ()
  "Sampling voice and insert as audio file on \".\" in attachments"
  (interactive)
  (if (not (mew-attach-not-line012-1))
      (message "Can't attach audio data here.")
    (let* ((nums (mew-attach-nums))
	   (subdir (mew-attach-expand-path mew-encode-syntax nums))
	   (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	   file filepath)
      ;; mimedir / {subdir/} dir
      (if (not (equal subdir "")) 
	  (setq mimedir (expand-file-name subdir mimedir)))
      ;; mimedir / file
      (setq filepath (mew-random-filename mimedir mew-audio-suffix))
      (if (null filepath)
	  (message "Could not make a file for audio, sorry.")
	(setq file (file-name-nondirectory filepath))
	(save-excursion
	  (mew-set-buffer-tmp)
	  (condition-case nil
	      (progn
		(while (not (y-or-n-p "Are you ready? ")) ())
		(message "Type C-g to finish recording...")
		(mew-plet
		 (apply (function call-process) mew-prog-audio)
		 ))
	    (quit (message "Type C-g to finish recording... done.")))
	  (mew-flet
	   (write-region (point-min) (point-max) filepath nil 'no-msg)))
	(setq mew-encode-syntax
	      (mew-syntax-insert-entry
	       mew-encode-syntax 
	       nums
	       (mew-encode-syntax-single file mew-type-ado
					 nil nil nil nil 'cdp)))
	(mew-encode-syntax-print mew-encode-syntax)
	))
    ))

;;
;; modify commands
;;

(defun mew-attach-undo ()
  "Unmark. The original mark appears."
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't undo encoding here")
    (mew-syntax-clear-marks mew-encode-syntax (mew-attach-nums))
    (mew-encode-syntax-print mew-encode-syntax)
    )
  )

(defun mew-attach-type ()
  "Change the data type(Content-Type:) in attachments."
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't change %s here" mew-ct:)
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (file (mew-syntax-get-file syntax))
	   (ctl (mew-syntax-get-ct syntax))
	   (ct (capitalize (car ctl)))
	   cte)
      (setq ct (mew-input-type "Type for %s (%s): " file ct
			       (if (mew-syntax-multipart-p syntax)
				   mew-mime-content-type-multipart-list
				 mew-mime-content-type-list)))
      (setq ctl (list ct))
      (mew-syntax-set-ct syntax ctl)
      (setq cte (mew-attr-get-cte (mew-attr-by-ct ct)))
      (mew-syntax-set-cte syntax cte)
      (mew-encode-syntax-print mew-encode-syntax)
      )))

(defun mew-attach-charset ()
  "Specify charset for a Text/* object in attachments."
  (interactive)
  (if (not mew-mule-p)
      (message "This commands cannot be used on this Emacs 19")
    (if (not (mew-attach-not-line0-1-dot))
	(message "Can't specify character set here")
      (let* ((nums (mew-attach-nums))
	     (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	     (file (mew-syntax-get-file syntax))
	     (ctl (mew-syntax-get-ct syntax))
	     (ct (car ctl))
	     (char (mew-syntax-get-member ctl "charset"))
	     charset
	     (case-fold-search t))
	(if (not (string-match "text/" ct))
	    (message "Can't specify character set to %s" ct)
	  (if (null char) (setq char "guess"))
	  (setq charset (mew-input-type "Charset for %s (%s): "
					file char mew-charset-list))
	  (if (equal charset "guess")
	      (setq ctl (list ct))
	    (setq ctl (list ct (format "charset=%s" charset))))
	  (mew-syntax-set-ct syntax ctl)
	  (mew-encode-syntax-print mew-encode-syntax)
	  )))
    ))

(defun mew-attach-description (&optional cd)
  "Input a description(Content-Description:) in attachments."
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't describe here")
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (ocd (or (mew-syntax-get-cd syntax) (mew-syntax-get-file syntax))))
      (if (null cd)
	  (setq cd (read-string "Description: " ocd)))
      (if (equal cd "")
	  (mew-syntax-set-cd syntax nil)
	(mew-syntax-set-cd syntax cd))
      (mew-encode-syntax-print mew-encode-syntax)
      )
    ))

(defun mew-attach-disposition (&optional cdp)
  "Input Content-Disposition: in attachments."
  (interactive)
  (if (not (mew-attach-not-line012-1-dot))
      (message "Can't set disposition here")
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (ocdp (or (mew-syntax-get-member (mew-syntax-get-cdp syntax)
					    "filename")
		     (mew-syntax-get-file syntax))))
      (if (null cdp)
	  (setq cdp (read-string "Filename: " ocdp)))
      (if (equal cdp "") (setq cdp nil)) ;; to clear
      (mew-syntax-set-cdp syntax (mew-syntax-cdp-format cdp))
      (mew-encode-syntax-print mew-encode-syntax)
      )
    ))

(defun mew-attach-base64 ()
  "Put the 'B' mark to encode with Base64 in attachments."
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't encode with base64 here")
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (ctl (mew-syntax-get-ct syntax))
	   (ct (car ctl))
	   (case-fold-search t))
	(if (or (string-match "^Message/" ct) (string-match "^Multipart/" ct))
	    (message "Can't encode with base64 here")
	  (mew-syntax-set-cte syntax mew-b64)
	  (mew-encode-syntax-print mew-encode-syntax)
	  ))
    ))

(defun mew-attach-quoted-printable ()
  "Put the 'Q' mark to encode with Quoted-Printable in attachments"
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't encode with quoted-printable here")
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (ctl (mew-syntax-get-ct syntax))
	   (ct (car ctl))
	   (case-fold-search t))
	(if (or (string-match "^Message/" ct) (string-match "^Multipart/" ct))
	    (message "Can't encode with quoted-printable here")
	  (mew-syntax-set-cte syntax mew-qp)
	  (mew-encode-syntax-print mew-encode-syntax)
	  ))
    ))

(defun mew-attach-gzip64 ()
  "Put the 'G' mark to encode with Gzip64 in attachments. This is 
applicable only to Text/Plain and Application/Postscript since compression 
is not effective other objects. For example, JPEG is already compressed."
  (interactive)
  (if (not (mew-attach-not-line0-1-dot))
      (message "Can't apply gzip here")
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (ctl (mew-syntax-get-ct syntax))
	   (ct (car ctl))
	   (case-fold-search t))
      (if (null (mew-member-case-equal ct mew-mime-content-type-text-list))
	  ;; never use compress to types other than text/plain or 
	  ;; application/postscript. Indeed, compression is not
	  ;; effective to compressed data such as jpeg.
	  (message "Can't apply gzip to %s" ct)
	(mew-syntax-set-cte syntax mew-xg)
	(mew-encode-syntax-print mew-encode-syntax)
	))
    ))

(defun mew-attach-pgp-sign ()
  "Put the 'PS' mark to sign with PGP in attachments."
  (interactive)
  (cond
   ((not (mew-attach-not-line0-1-dot))
    (message "Can't PGP sign here"))
   ((null mew-pgp-ver)
    (message "PGP doesn't exist"))
   (t
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (privacy (mew-syntax-get-privacy syntax))
	   (pgp-sign (list mew-ct-mls mew-ct-pgs)))
      (cond
       ((>= (length privacy) 2)
	(message "Too many marks"))
       ((mew-member pgp-sign privacy)
	(message "Already marked."))
       (t
	(mew-syntax-set-privacy 
	 syntax (append privacy (list pgp-sign)))
	(mew-encode-syntax-print mew-encode-syntax))
       )))
   ))

(defun mew-attach-pgp-enc ()
  "Put the 'PE' mark to encrypt with PGP in attachment. 
Input decryptors' addresses."
  (interactive)
  (cond
   ((not (mew-attach-not-line0-1-dot))
    (message "Can't PGP encrypt here"))
   ((null mew-pgp-ver)
    (message "PGP doesn't exist"))
   (t
    (let* ((nums (mew-attach-nums))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))
	   (privacy (mew-syntax-get-privacy syntax))
	   (towhom (car (mew-header-address-collect '("To:"))))
	   (pgp-enc (list mew-ct-mle mew-ct-pge)))
      (cond
       ((>= (length privacy) 2)
	(message "Too many marks"))
       ((mew-member pgp-enc privacy)
	(message "Already marked."))
       (t
	;; ask towhom before set privacy for C-g
	(if towhom
	    (setq towhom (mew-input-address2 "To (%s): " towhom))
	  (setq towhom (mew-input-address "To:")))
	(mew-syntax-set-privacy 
	 syntax (append privacy (list pgp-enc)))
	(mew-syntax-set-decrypters syntax towhom)
	(mew-encode-syntax-print mew-encode-syntax))
       )))
   ))

;;
;; find commands
;;

(defun mew-attach-find-file ()
  "Open this file into a buffer in attachments."
  (interactive)
  (if (not (mew-attach-not-line012-1-dot))
      (message "Can't find a file here.")
    (let* ((nums (mew-attach-nums))
	   (subdir (mew-attach-expand-path mew-encode-syntax nums))
	   (mimedir (mew-expand-folder (mew-draft-to-mime (buffer-name))))
	   (syntax (mew-syntax-get-entry mew-encode-syntax nums))	   
	   (file (mew-syntax-get-file syntax))
	   efile)
      (setq efile (if (equal subdir "") file (concat subdir file)))
      (find-file (expand-file-name efile mimedir))
      ;; buffer switched
      (local-set-key "\C-c\C-q" 'mew-kill-buffer)
      )))

;;

(defun mew-attach-dummy ()
  "A dummy function to clear side-effect in attachments."
  (interactive)
  ()
  )

(provide 'mew-attach)

;;; Copyright Notice:

;; Copyright (C) 1996, 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-attach.el ends here

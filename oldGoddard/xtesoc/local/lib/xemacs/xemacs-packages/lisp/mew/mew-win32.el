;;; mew-win32.el --- Settings for Mew on Win32

;; Author:  Shuichi Kitaguchi <kit@Mew.org>
;; Created: Dec 05, 1997
;; Revised: Jun 01, 1998

;;; Code:

(defconst mew-win32-version "mew-win32.el 0.25")


;;
;;  ~/.emacs settings
;;

;;; for PRINTING
;;    mew-w32-prog-print     ...  print command
;;    mew-w32-prog-print-arg ...  print command argument
;;    mew-w32-cs-print       ...  coding-system for printing
;;    define-process-argument-editing  ...  for argument-editing
;;
;;; example
;;   (setq mew-w32-prog-print     "notepad.exe")
;;   (setq mew-w32-prog-print-arg "/p")
;;   (setq mew-w32-cs-print       '*sjis*dos)      ;; Mule for Win32
;;   (setq mew-w32-cs-print       'shift_jis-dos)  ;; Meadow
;;   (define-process-argument-editing "/notepad\\.exe$"
;;     (lambda (x)
;;       (general-process-argument-editing-function x nil t)))

;;; for BROWSE-URL
;;
;; (define-key global-map [S-mouse-2] 'browse-url-at-mouse)
;;
;;   browse-mew-noask                 ... ask or not when browse
;;   browse-mew-append-file           ... URL collection file name
;;   browse-mew-append-always-file    ... always, append URL to file (for dial-up)
;;   browse-mew-append-always-mailto  ... always, URL is mailto: (for emacs19.28)
;;
;;; example
;;   (setq browse-mew-noask nil)
;;   (setq browse-mew-append-file "~/.browse")
;;   (setq browse-mew-append-always-file nil)
;;   (setq browse-mew-append-always-mailto nil)



;; Win32 programs.
(defvar mew-w32-exec           "fiber.exe")
(defvar mew-w32-prog-print     "notepad.exe")
(defvar mew-w32-prog-print-arg nil)


;; Emacs version dependent variables.
(cond
;; ((featurep 'xemacs)	; XEmacs?
;;  (setq mew-prog-shell-arg  shell-command-switch)
  
;;  (setq mew-cs-noconv-eol 'raw-text))

 ((featurep 'meadow)		; Meadow
  (setq mew-prog-shell-arg  shell-command-switch)
  
  (setq mew-cs-noconv-eol 'raw-text)
  
  (defvar mew-w32-cs-print 'no-conversion)
  
  (require 'mw32script)
  (mw32script-init))
 (t					; Mule for Win32
  (setq mew-prog-shell-arg  shell-command-option)
  
  (make-coding-system
   '*crlf2lf* 4
   ?= "No conversion." nil
   '([1 23 47 346 13 18 250 10 18 1881 5 4421 19 10
	47 772 5892 49 58 0 0 14 772 49 22] .
	[1 4 14 775 22]))
  (setq mew-cs-noconv-eol *crlf2lf*)

  (defvar mew-w32-cs-print *noconv*)
  
  (require 'win32-script)
  (define-process-argument-editing
    "/[^./]+$"
    'script-process-argument-editing-function 'last)))


;; common programs.
(setq mew-prog-mime-encode "mewencode.exe")
(setq mew-prog-mime-decode "mewdecode.exe")
(setq mew-prog-tar         "tar.exe")
(setq mew-prog-compress    "compress.exe")
(setq mew-prog-gzip        "gzip.exe")
(setq mew-prog-shell       shell-file-name)

(setq mew-prog-pgp         "pgp.exe")

(setq mew-prog-utime       "utime.exe")
(setq mew-touch-folder-p   t)

(setq mew-prog-uncompface  "uncompface.exe")

(setq mew-prog-text/html     mew-w32-exec)
(setq mew-prog-text/html-arg nil)
(setq mew-ext-prog-url       mew-w32-exec)
(setq mew-ext-prog-url-args  nil)


;; browse-url.
(defvar browse-mew-button [mouse-2]) ; mouse button

(setq browse-url-regexp "\\(\\b\\(s?https?\\|ftp\\|file\\|gopher\\|news\\|telnet\\|wais\\|mailto\\):\\(//[-a-zA-Z0-9_.]+:[0-9]*\\)?[-a-zA-Z0-9_=?#$@~`%&*+|\\/.,]*[-a-zA-Z0-9_=#$@~`%&*+|\\/]+\\)\\|\\(\\([^-A-Za-z0-9!_.%]\\|^\\)[-A-Za-z0-9._!%]+@[A-Za-z0-9][-A-Za-z0-9._!]+[A-Za-z0-9]\\)")

(defvar browse-mew-noask t)                   ;; ask or not when browse.
(defvar browse-mew-append-file nil)           ;; URL collection file
(defvar browse-mew-append-always-file nil)    ;; for dialup user
(defvar browse-mew-append-always-mailto nil)  ;; for emacs19.28

(defun browse-url-or-mew (url)
  "browse URL or mew-send."
  (interactive)
  (let* ((append-buffer (and browse-mew-append-file
			     (string= buffer-file-name
				      (expand-file-name browse-mew-append-file))))
	 (append-nil (or append-buffer (not browse-mew-append-file)))
	 (append-all (and (not append-nil) browse-mew-append-always-file))
	 (append-ask (and (not append-nil) (not browse-mew-append-always-file)))
	 (browse-all (or append-buffer browse-mew-noask))
	 (browse-ask (and (not append-buffer) (not browse-mew-noask))))
    (string-match "\\([a-zA-Z0-9][-a-zA-Z0-9!_=?#$@~`%&*+|\\/.,:]+\\)" url)
    (setq url (substring url (match-beginning 0) (match-end 0)))
    (if (not (string-match ":" url))    ;; emacs19.28 only
	(if (and (not browse-mew-append-always-mailto)
		 (not (y-or-n-p (format "mailto:%s(y) or ftp://%s(n)? " url url))))
	    (setq url (concat "ftp://" url))
	  (setq url (concat "mailto:" url))))
    (cond
     ((and append-all browse-all)
      (browse-url-mew-append url)
      (browse-url-mew-start url))
     ((and append-ask browse-all)
      (if (y-or-n-p (format "Append %s? " url))
	  (browse-url-mew-append url))
      (browse-url-mew-start url))
     ((and append-nil browse-all)
      (browse-url-mew-start url))
     ((and append-all browse-ask)
      (browse-url-mew-append url)
      (if (y-or-n-p (format "Browse %s? " url))
	  (browse-url-mew-start url)))
     ((and append-nil browse-ask)
      (if (y-or-n-p (format "Browse %s? " url))
	  (browse-url-mew-start url)))
     (t ;; (and append-ask browse-ask)
      (if (y-or-n-p (format "Browse %s(y) or Append(n)? " url))
	  (browse-url-mew-start url)
	(browse-url-mew-append url)))
     )))

(defun browse-url-mew-append (url)
  (let ((file (expand-file-name browse-mew-append-file))
	(beg))
    (save-excursion
      (find-file file)
      (set-buffer (current-buffer))
      (goto-char (point-min))
      (while (search-forward url nil t)
	(progn
	  (beginning-of-line)
	  (setq beg (point))
	  (forward-line 1)
	  (delete-region beg (point))))
      (goto-char (point-max))
      (insert url "\n")
      (write-file file)
      (kill-buffer (current-buffer))
      (message "Append %s to %s done." url file)
      )))

(defun browse-url-mew-start (url)
  (message "Browse %s." url)
  (cond
   ((string-match "^mailto:" url)
    (mew-send (substring url 7 nil)))
   (t
    (apply (function start-process)
	   (format "*mew %s*" mew-ext-prog-url)
	   mew-buffer-tmp mew-ext-prog-url
	   (append mew-ext-prog-url-args (list url))))))

(setq browse-url-browser-function 'browse-url-or-mew)
(add-hook 'mew-init-hook
	  (lambda ()
	    (progn 
	      (define-key mew-message-mode-map browse-mew-button 'browse-url-at-mouse)
	      )))


;; printing
(defun mew-w32-print-buffer ()
  (interactive)
  (setq tempfile (mew-make-temp-name))
  (mew-frwlet mew-cs-noconv mew-w32-cs-print
	      (write-region (point-min) (point-max) tempfile nil nil))
  (setq w32-start-process-show-window t)
  (cond
   ((eq mew-w32-prog-print-arg nil)
    (call-process mew-w32-prog-print nil nil nil tempfile))
   (t
    (call-process mew-w32-prog-print nil nil nil mew-w32-prog-print-arg tempfile)))
  (setq w32-start-process-show-window nil)
  (delete-file tempfile))
(setq mew-print-function (function mew-w32-print-buffer))


;; MIME setting
(defvar mew-prog-plain '(mew-mime-text/plain () nil))
(defvar mew-prog-html '(mew-mime-text/html () nil))
(defvar mew-prog-text '(mew-mime-text/plain () nil))
(defvar mew-prog-audio (list mew-w32-exec () t))
(defvar mew-prog-gif (list mew-w32-exec () t))
(defvar mew-prog-jpeg (list mew-w32-exec () t))
(defvar mew-prog-xwd (list mew-w32-exec () t))
(defvar mew-prog-xbm (list mew-w32-exec () t))
(defvar mew-prog-xpm (list mew-w32-exec () t))
(defvar mew-prog-png (list mew-w32-exec () t))
(defvar mew-prog-bmp (list mew-w32-exec () t))
(defvar mew-prog-image (list mew-w32-exec () t))
(defvar mew-prog-mpeg (list mew-w32-exec () t))
(defvar mew-prog-rfc822 '(mew-mime-message/rfc822 () nil))
(defvar mew-prog-external-body '(mew-mime-external-body () nil))
(defvar mew-prog-delivery-status '(mew-mime-text/plain () nil))
(defvar mew-prog-postscript (list mew-w32-exec () t))
(defvar mew-prog-pgp-keys '(mew-mime-pgp-keys () nil))
(defvar mew-prog-octet-stream (list mew-w32-exec () t))

(provide 'mew-win32)

;;; Copyright Notice:

;; Copyright (C) 1996, 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-win32.el ends here

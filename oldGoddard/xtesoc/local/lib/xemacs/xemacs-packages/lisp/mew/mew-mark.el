;;; mew-mark.el --- Marking for Mew Summary and Virtual mode

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar  2, 1997
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-mark-version "mew-mark.el version 0.10")

(require 'mew)

;; level 1: *, @
;; level 2: D
;; level 3: o

(defvar mew-mark-delete-switch
  (list 
   (cons mew-mark-multi  'mew-mark-keep-line)
   (cons mew-mark-review 'mew-mark-keep-line)
   (cons mew-mark-delete 'mew-mark-delete-line)
   (cons mew-mark-refile 'mew-mark-delete-line)
   )
  )

(defvar mew-mark-undo-switch
  (list 
   (cons mew-mark-multi  'mew-mark-unmark)
   (cons mew-mark-review 'mew-mark-unmark)
   (cons mew-mark-delete 'mew-mark-unmark)
   (cons mew-mark-refile 'mew-mark-unrefile)
   )
  )

;;
;; for mew-mark-delete-switch
;;

(defun mew-mark-delete-line ()
  (beginning-of-line)
  (let ((start (point)))
    (forward-line)
    (delete-region start (point))
    ))

(defun mew-mark-keep-line ()
  () ;; do nothing
  )

;;
;; for mew-mark-undo-switch
;;

(defun mew-mark-unmark ()
  "Delete the mark on this message anyway"
  (save-excursion
    (let ((buffer-read-only nil))
      (beginning-of-line)
      (re-search-forward mew-summary-message-regex)
      (delete-char 1)
      (insert " ")
      (mew-highlight-unmark-line)
      )
    ))

(defun mew-mark-unrefile ()
  "Delete refile state and delete the mark"
  (mew-refile-reset (mew-summary-message-number))
  (mew-mark-unmark)
  )

;;
;; Basic functions for mark
;;

(defun mew-summary-marked-p ()
  "See if this message is marked"
  (save-excursion
    (beginning-of-line)
    (cond 
     ((looking-at (concat mew-summary-message-regex " \\|^$")) nil)
     (t t)
     )
    ))

(defun mew-summary-get-mark ()
  (let (mark)
    (save-excursion
      (beginning-of-line)
      (if (not (looking-at (concat mew-summary-message-regex "\\(.\\)")))
	  nil
	(setq mark (mew-match 2)) ;; regex includes \( \)
	(cond 
	 ((string-equal " " mark) nil)
	 (t (string-to-char mark))
	 )
	)
      )))

(defun mew-summary-mark-as (mark &optional force)
  "Mark this message if possible"
  (let ((buffer-read-only nil))
    (cond 
     ((or force (null (mew-summary-marked-p)))
      (beginning-of-line)
      (re-search-forward mew-summary-message-regex)
      (delete-char 1)
      (insert (char-to-string mark))
      (mew-highlight-mark-line mark))
     (t (message "Already marked")))
    ))

;;
;; Entire buffer
;;

(defun mew-summary-mark-exist-p (marklist)
  "See if this Summary mode has one or more marked messages"
  (let ((regex (concat
		mew-summary-message-regex
		"["
		(mapconcat (function char-to-string) marklist nil)
		"]")))
    (save-excursion
      (goto-char (point-min))
      (re-search-forward regex nil t)
      )
    ))

(defun mew-summary-mark-collect (mark &optional begin end)
  (save-excursion
    (let ((regex (concat mew-summary-message-regex
		      (regexp-quote (char-to-string mark))))
	  (msglist nil))
      (goto-char (if begin begin (point-min)))
      (while (re-search-forward regex end t)
	(setq msglist (cons (mew-summary-message-number) msglist)))
      (nreverse msglist))))

(defun mew-summary-mark-collect2 (mark)
  (save-excursion
    (let ((regex (concat mew-summary-message-regex
			 (regexp-quote (char-to-string mark))))
	  (msglist nil))
      (goto-char (point-min))
      (while (re-search-forward regex nil t)
	(setq msglist (cons 
		       (cons
			(mew-summary-folder-name)
			(mew-summary-message-number))
		       msglist)))
      (nreverse msglist) ;; return
      )))

(defun mew-summary-mark-collect3 (mark)
  (save-excursion
    (let ((regex (concat mew-summary-message-regex
			 (regexp-quote (char-to-string mark))))
	  (i 1)
	  ret)
      (goto-char (point-min))
      (while (not (eobp))
	(if (looking-at regex)
	    (setq ret (cons i ret)))
	(forward-line)
	(setq i (1+ i)))
      (nreverse ret))))

;;
;; Weak marks
;;

(defun mew-summary-mark-weak (mark)
  (save-excursion
    (mew-summary-goto-message)
    (let ((cmark (mew-summary-get-mark))
	  (msg (mew-summary-message-number)))
      (cond 
       ((null msg) (message "No message"))
       ((or (equal cmark mew-mark-refile) (equal cmark mew-mark-delete))
	(message "Already strongly marked as %s" (char-to-string cmark)))
       ((equal cmark mark)
	(message "Already marked as %s" (char-to-string cmark)))
       (t ;; no mark
	(save-excursion
	  (if cmark (mew-summary-undo-one))
	  (mew-summary-mark-as mark)))
       ))
    (set-buffer-modified-p nil)
    ))

;; "@" in Summary mode
(defun mew-summary-multi ()
  "Put the multi the '@' mark on this message for 'F', 'M-s', 
and 'M-t'. It can overlay the '*' mark. The cursor stays always."
  (interactive)
  (mew-summary-mark-weak mew-mark-multi)
  )

;; "*" in Summary mode
(defun mew-summary-review ()
  "Put the review the '*' mark on this message. 
Use 'N' or 'P' to jump to a message marked with '*'.
It can overlay '@'. The cursor stays always.
See also 'mo', 'md', 'mr', and 'ma'."
  (interactive)
  (mew-summary-mark-weak mew-mark-review)
  )

(defun mew-summary-down-mark (mark)
  (interactive)
  (forward-line)
  (cond 
   ((re-search-forward (concat mew-summary-message-regex
			       (regexp-quote (char-to-string mark)))
		       nil t nil)
    (beginning-of-line)
    t)
   (t 
    (forward-line -1)
    (message "No more marked message")
    nil)))

(defun mew-summary-display-review-down ()
  "Jump to the message marked with '*' below."
  (interactive)
  (if (mew-summary-down-mark mew-mark-review)
      (if mew-summary-buffer-disp-msg ;; just seed up!
	  (mew-summary-display))))

(defun mew-summary-up-mark (mark)
  (interactive)
  (cond 
   ((re-search-backward (concat mew-summary-message-regex
			       (regexp-quote (char-to-string mark)))
			nil t nil)
    t)
   (t 
    (message "No more marked message")
    nil)
   ))

(defun mew-summary-display-review-up ()
  "Jump to the message marked with '*' above."
  (interactive)
  (if (mew-summary-up-mark mew-mark-review)
      (if mew-summary-buffer-disp-msg ;; just seed up!
	  (mew-summary-display))))

;;
;; Strong marks
;;

;; Remove

(defun mew-summary-delete (count)
  "Put the delete mark(default is 'D') on this message.
This can overlay other marks. When it overlays, the cursor stays
on the message. If it marks newly, displays the next message."
  (interactive "P")
  (mew-summary-goto-message)
  (mew-decode-syntax-delete)
  (if (not (numberp count))
      (mew-summary-delete-one)
    (while (> count 0)
      (mew-summary-mark-as mew-mark-delete)
      (mew-decode-syntax-delete)
      (forward-line)
      (setq count (1- count)))
    (while (< count 0)
      (mew-summary-mark-as mew-mark-delete)
      (mew-decode-syntax-delete)
      (forward-line -1)
      (setq count (1+ count)))
    ))

(defun mew-summary-delete-one ()
  (let ((mark (mew-summary-get-mark))
	(msg (mew-summary-message-number))
	(zmacs-regions nil))
    (cond
     ((null msg) (message "No message"))
     ((equal mark mew-mark-delete) (message "Already marked as delete"))
     ((equal mark mew-mark-refile)
      (if (y-or-n-p "Already marked as refile. Delete it? ")
	  (progn
	    (mew-summary-undo-one)
	    (save-excursion (mew-summary-mark-as mew-mark-delete))
	    )))
     ((or (equal mark mew-mark-review) (equal mark mew-mark-multi))
      (mew-summary-undo-one)
      (save-excursion (mew-summary-mark-as mew-mark-delete)))
     (t ;; no mark
      (save-excursion (mew-summary-mark-as mew-mark-delete))
      ;; for C-x C-x
      (beginning-of-line)
      (push-mark (point) t t)
      (mew-summary-display-next)
      )
     )))

;;
;; "mx" extensions
;;

(defun mew-summary-mark-all (&optional arg)
  "Put the '*' mark onto all messages which are not marked."
  (interactive "P")
  (if arg
      (mew-summary-mark-region (region-beginning) (region-end))
    (mew-summary-mark-region (point-min) (point-max))))

(defun mew-summary-mark-region (beg end)
  (interactive "r")
  (let ((regex (concat mew-summary-message-regex " ")))
    (save-excursion
      (goto-char beg)
      (while (re-search-forward regex end t)
	(mew-summary-mark-as mew-mark-review)
	))
    ))

(defun mew-summary-mark-regexp (regex)
  "Put the '*' mark onto Mall messages matched to a regular expression."
  (interactive "sRegexp: ")
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward regex nil t)
      (if (not (mew-summary-marked-p))
	  (mew-summary-mark-as mew-mark-review))
      )
    ))

;;
;; Exchange
;;

(defun mew-summary-exchange-mark (old-mark new-mark)
  (let ((regex (concat mew-summary-message-regex
		       (regexp-quote (char-to-string old-mark)))))
    (save-excursion
      (goto-char (point-min))
      (if (not (re-search-forward regex nil t))
	  (message "No marked messages")
	(beginning-of-line)
	(let ((buffer-read-only nil))
	  (while (re-search-forward regex nil t)
	    (delete-backward-char 1)
	    (insert (char-to-string new-mark))
	    (mew-highlight-mark-line new-mark)))))))
   
(defun mew-summary-mark-delete ()	; * -> D
  "Put the delete mark onto all messages marked with '*'."
  (interactive)
  (mew-summary-exchange-mark mew-mark-review mew-mark-delete))

(defun mew-summary-mark-multi ()	; * -> @
  "Change the '*' mark into the '@' mark."
  (interactive)
  (mew-summary-exchange-mark mew-mark-review mew-mark-multi))

(defun mew-summary-mark-review ()	; @ -> *
  "Change the '@' mark into the '*' mark."
  (interactive)
  (mew-summary-exchange-mark mew-mark-multi mew-mark-review))

(defun mew-summary-mark-swap ()		; @ <-> *
  "Swap the '@' mark and the '*' mark."
  (interactive)
  (mew-summary-exchange-mark mew-mark-multi mew-mark-tmp)
  (mew-summary-exchange-mark mew-mark-review mew-mark-multi)
  (mew-summary-exchange-mark mew-mark-tmp mew-mark-review)
  )

;;
;; Undo
;;

(defun mew-summary-undo-one ()
  (interactive)
  (cond 
   ((mew-summary-marked-p)
    (beginning-of-line)
    (looking-at (concat mew-summary-message-regex "\\([^ +0-9]\\)"))
    (funcall (cdr (assoc (string-to-char (mew-match 2))
			 mew-mark-undo-switch)))
    )
   ((eobp) (message "No message"))
   (t (message "No mark"))
   )
  )

(defun mew-summary-undo (count)
  "Cancel the mark on this message."
  (interactive "P")
  (mew-summary-goto-message)
  (mew-decode-syntax-delete)
  (save-excursion
    (if (not (numberp count))
	(mew-summary-undo-one)
      (while (> count 0)
	(mew-summary-undo-one)
	(forward-line)
	(setq count (1- count)))
      (while (< count 0)
	(mew-summary-undo-one)
	(forward-line -1)
      (setq count (1+ count)))
      )
    )
  (set-buffer-modified-p nil)
  )

(defun mew-summary-undo-all ()
  "Cancel all marks according to what you input."
  (interactive)
  (let ((char nil) (ociea cursor-in-echo-area))
    (unwind-protect
	(progn
	  (message "Input mark : ")
	  (setq cursor-in-echo-area t)
	  (setq char (read-char))
	  (message "Input mark : %s" (char-to-string char))
	  )
      (setq cursor-in-echo-area ociea))
    (if (assoc char mew-mark-undo-switch)
	(mew-summary-batch-unmark (list char) 'msg)
      (message "Mark %s is not supported" (char-to-string char)))))


;;"mx" extensions
(defun mew-summary-mark-undo-all ()
  "Unmark all message marked with 'o' or 'D'."
  (interactive)
  (mew-summary-batch-unmark (list mew-mark-delete mew-mark-refile) 'msg)
  )

(defun mew-summary-batch-unmark (mark-list msg)
  (mew-decode-syntax-delete)
  (let* ((mark-str (mapconcat (function concat)
			      (mapcar (function char-to-string) mark-list) ""))
	 (regex (concat mew-summary-message-regex "[" mark-str "]")))
    (if msg (message "Unmarking %s ..." mark-str))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward regex nil t)
	(mew-summary-undo-one)))
    (if msg (message "Unmarking %s ... done" mark-str)))
  (set-buffer-modified-p nil)
  )

;;
;; Processing marks
;;

(defun mew-mark-delete-region (begin end)
  (let ((buffer-read-only nil)
	(m (make-marker)))
    (set-marker m end)
    (goto-char begin)
    (while (re-search-forward 
	    (concat mew-summary-message-regex "\\([^ +0-9]\\)") m t)
      (funcall (cdr (assoc (string-to-char (mew-match 2))
			   mew-mark-delete-switch)))
      )
    (set-buffer-modified-p nil)
    ))

(defun mew-summary-exec ()
  "Process marked messages. To cancel the '*' mark, use 'u' or 'U'."
  (interactive)
  (mew-summary-exec-region (point-min) (point-max))
  )

(defun mew-summary-exec-current ()
  (interactive)
  (mew-summary-goto-message)
  (mew-decode-syntax-delete)
  (let (beg end)
    (save-excursion
      (beginning-of-line)
      (setq beg (point))
      (end-of-line)
      (setq end (point))
      )
    (mew-summary-exec-region beg end)
    ))

(defvar mew-summary-exec-error-msg nil)

(defun mew-summary-exec-region (beg end)
  (interactive "r")
  (if (mew-summary-exclusive-p)
      (save-excursion
	(save-restriction
	  (narrow-to-region beg end)
	  (goto-char (point-min))
	  (message "Collecting marks ...")
	  (condition-case nil
	      (let ((msgs (mew-summary-mark-collect
			   mew-mark-refile (point-min) (point-max)))
		    (dels (mew-summary-mark-collect
			   mew-mark-delete (point-min) (point-max)))
		    (src (buffer-name))
		    err-folders)
		(if (and (null msgs) (null dels))
		    (message "No marks")
		  ;; opening...
		  (setq mew-summary-buffer-process t)
		  (message "Refiling and deleting ...")
		  (mew-window-configure (current-buffer) 'summary)
		  (mew-decode-syntax-delete)
		  (mew-current-set 'message nil)
		  ;; refiling and deleting...
		  (if (setq err-folders (mew-summary-exec-sanity-check msgs))
		      (progn
			(setq mew-summary-buffer-process nil)
			(ding)
			(message 
			 "Nothing proceeded. Folder(s) MUST be a file!: %s"
			 (mew-join "," err-folders)))
		    (mew-summary-exec-refile src msgs)
		    (mew-summary-exec-delete src dels)
		    ;; ending...
		    (mew-mark-delete-region (point-min) (point-max))
		    (if mew-summary-cache-use (mew-summary-folder-cache-save))
		    (mew-refile-guess-save)
		    (run-hooks 'mew-summary-exec-hook)
		    (set-buffer-modified-p nil)
		    (mew-summary-mode-line (current-buffer))
		    (message "Refiling and deleting ... done")
		    (setq mew-summary-buffer-process nil)
		    ))) ;; end of let
	    (quit
	     (set-buffer-modified-p nil)
	     (setq mew-summary-buffer-process nil))
	    )
	  ))))

(defun mew-summary-exec-sanity-check (msgs)
  (if mew-use-imap
      nil ;; no sanity check
    (let (msg dst dsts uniq-dsts udst err-dsts dir)
      (while msgs
	(setq msg (car msgs))
	(setq msgs (cdr msgs))
	(setq dsts (cdr (assoc msg mew-summary-buffer-refile)))
	(while dsts
	  (setq dst (car dsts))
	  (setq dsts (cdr dsts))
	  (if (not (mew-member dst uniq-dsts))
	      (setq uniq-dsts (cons dst uniq-dsts)))))
      (if (not (mew-member mew-trash-folder uniq-dsts))
	  (setq uniq-dsts (cons mew-trash-folder uniq-dsts)))
      (while uniq-dsts
	(setq udst (car uniq-dsts))
	(setq uniq-dsts (cdr uniq-dsts))
	(setq dir (mew-expand-folder udst))
	(if (file-exists-p dir)
	    (if (file-directory-p dir)
		(if (not (file-writable-p dir))
		    (set-file-modes dir mew-folder-mode))
	      (setq err-dsts (cons udst err-dsts))) ;; NG
	  (mew-make-directory dir)))
      err-dsts
      )))

(defun mew-summary-exec-refile (src msgs)
  (let (dsts tmp msg msg-dsts dsts-msgses)
    (while msgs
      (setq msg (car msgs))
      (setq msgs (cdr msgs))
      (setq msg-dsts (assoc msg mew-summary-buffer-refile))
      (setq dsts (cdr msg-dsts))
      (if dsts ;; sanity check
	  (if (setq tmp (assoc dsts dsts-msgses))
	      (setq dsts-msgses (cons (append tmp (list msg))
				      (delete tmp dsts-msgses)))
	    (setq dsts-msgses (cons (list dsts msg) dsts-msgses))
	    ))
      (setq mew-summary-buffer-refile
	    (delete msg-dsts mew-summary-buffer-refile)))
    ;; refile at once
    (while dsts-msgses
      (mew-summary-immv src (car dsts-msgses))
      (setq dsts-msgses (cdr dsts-msgses)))
    ))

(defun mew-summary-exec-delete (src dels)
  (if dels
      (let ((rm-it nil))
	(cond
	 ((equal mew-msg-rm-policy 'always)
	  (setq rm-it t))
	 ((equal mew-msg-rm-policy 'trashonly)
	  (if (equal src mew-trash-folder)
	      (setq rm-it t)))
	 ((equal mew-msg-rm-policy 'uselist)
	  (if (mew-member-match src mew-msg-rm-folder-list)
	      (setq rm-it t)))
	 )
	(if rm-it
	    (mew-summary-imclean src dels)
	  (if (equal src mew-trash-folder)
	      (let ((regex (concat
			    mew-summary-message-regex
			    (regexp-quote (char-to-string
					   mew-mark-delete)))))
		(goto-char (point-min))
		(while (re-search-forward regex nil t)
		  (mew-summary-undo-one)))
	    (mew-summary-imrm src dels)
	    )))))

(defun mew-summary-immv (src dsts-msgs)
  (let* ((dsts (car dsts-msgs)) ;; (+foo +bar)
	 (msgs (cdr dsts-msgs)) ;; (1 2 3)
	 (myselfp (mew-folder-member src dsts))
	 msgs- msg srcfile dstfile dst num)
    (if mew-use-imap
	(mew-summary-start-im mew-prog-immv src dsts msgs)
      (if myselfp
	  ;; msg stays in the src folder with the same number
	  (progn
	    (setq dsts (delete src dsts))
	    (while msgs
	      (setq msg (car msgs))
	      (setq msgs (cdr msgs))
	      (if (mew-file-regular-p (mew-expand-folder src msg))
		  (setq msgs- (cons msg msgs-))))
	    (setq msgs- (nreverse msgs-))
	    (setq msgs msgs-))
	(setq dst (car dsts)) 
	(setq dsts (cdr dsts))
	(setq num (string-to-int (mew-folder-new-message dst t)))
	(while msgs
	  (setq srcfile (mew-expand-folder src (car msgs)))
	  (setq msgs (cdr msgs))
	  (if (not (and (file-exists-p srcfile) (file-writable-p srcfile)))
	      ()
	    (setq msgs- (cons (int-to-string num) msgs-))
	    (setq dstfile (mew-expand-folder dst (int-to-string num)))
	    (setq num (1+ num))
	    (rename-file srcfile dstfile)))
	(setq msgs- (nreverse msgs-))
	(setq src dst)
	)
      (while dsts
	(setq dst (car dsts)) 
	(setq dsts (cdr dsts))
	(setq num (string-to-int (mew-folder-new-message dst t)))
	(setq msgs msgs-)
	(while msgs
	  (setq srcfile (mew-expand-folder src (car msgs)))
	  (setq msgs (cdr msgs))
	  (setq dstfile (mew-expand-folder dst (int-to-string num)))
	  (setq num (1+ num))
	  (mew-link srcfile dstfile)))
      (if mew-touch-folder-p (mew-touch-folder dst))
      )
    (if (not myselfp)
	()
      (goto-char (point-min))
      (setq msgs msgs-) ;; anyway
      (while msgs
	;; illegal marks remain and the lines will be deleted.
	(if (re-search-forward
	     (format "^ *%s%s" (car msgs)
		     (regexp-quote (char-to-string mew-mark-refile)))
	     nil t)
	    (mew-mark-unmark))
	(setq msgs (cdr msgs))))
    ))

(defun mew-summary-imrm (src dels)
  (if mew-use-imap
      (mew-summary-start-im mew-prog-imrm src nil dels)
    (let* (num srcfile dstfile)
      (setq num (string-to-int (mew-folder-new-message mew-trash-folder t)))
      ;; must be here after ensuring that +trash exists.
      (while dels
	(setq srcfile (mew-expand-folder src (car dels)))
	(setq dels (cdr dels))
	(setq dstfile (mew-expand-folder mew-trash-folder
					 (int-to-string num)))
	(setq num (1+ num))
	(if (mew-file-regular-p srcfile)
	    ;; if not, the marked line will be deleted anyway.
	    (rename-file srcfile dstfile)))
      (if mew-touch-folder-p (mew-touch-folder mew-trash-folder)))))

(defun mew-summary-imclean (src dels)
  (if mew-use-imap
      (mew-summary-start-im mew-prog-imclean src nil dels "--quiet=yes")
    (let (file)
      (while dels
	(setq file (mew-expand-folder src (car dels)))
	(setq dels (cdr dels))
	(if (mew-file-regular-p file)
	    ;; if not, the marked line will be deleted anyway.
	    (delete-file file))))))

(defun mew-summary-start-im (imprg src dsts msgs &optional arg)
  (let ((imarg (if arg (cons arg mew-prog-im-arg) mew-prog-im-arg)))
    (or (listp dsts) (setq dsts (list dsts)))
    (or (listp msgs) (setq msgs (list msgs)))
    (setq mew-summary-imap-string nil)
    (setq mew-summary-imap-process (apply (function start-process)
					    imprg nil imprg
					    (format "--src=%s" src)
					    (append imarg dsts msgs)))
    (set-process-filter 
     mew-summary-imap-process 'mew-summary-imap-filter)
    (set-process-sentinel
     mew-summary-imap-process 'mew-summary-imap-sentinel)
    (while mew-summary-imap-process
      (if mew-xemacs-p
	  (accept-process-output)
	(sit-for 1)
	;; accept-process-output or sleep-for is not enough
	(discard-input)))))

(defun mew-summary-imap-filter (process string)
  (save-excursion
    (setq mew-summary-imap-string
	  (concat mew-summary-imap-string string))
    (if (string-match "^Password for \\(.+\\):"
		      mew-summary-imap-string)
	(let ((userhost (mew-match 1 mew-summary-imap-string)))
	  (setq mew-summary-imap-string "")
	  (process-send-string
	   process
	   (format "%s\n"
		   (mew-input-passwd
		    (format "Enter password (%s) : " userhost))))))))

(defun mew-summary-imap-sentinel (process event)
  (let ((prog (process-name process)))
    (and mew-summary-imap-string
	 (string-match (format "^%s: \\([^\n]*\\)" prog)
		       mew-summary-imap-string)
	 (message "Refile error: %s"
		  (mew-match 1 mew-summary-imap-string)))
    (setq mew-summary-imap-process nil)
    (setq mew-summary-imap-string nil)))

;;;
;;; Clean up marks!
;;;

(defun mew-mark-clean-up (&optional sum-buf)
  "Process marked messages for this folder"
  (if sum-buf
      (set-buffer sum-buf)
    (setq sum-buf (buffer-name)))
  (if (mew-summary-mark-exist-p (list mew-mark-delete mew-mark-refile))
      (if (y-or-n-p (format "Marks exist in %s. Process them? " sum-buf))
	  (mew-summary-exec))))

(defun mew-mark-clean-up-all ()
  "Process marked messages for all Summary modes.
Typically called by kill-emacs-hook."
  (mew-decode-syntax-delete)
  (let ((bufs mew-clean-up-buffers) buf)
    (save-excursion
      (while bufs
	(setq buf (car bufs))
	(setq bufs (cdr bufs))
	(if (bufferp (get-buffer buf))
	    (mew-mark-clean-up buf))))))
;; Saving marks is a really bad idea.
;; First because there is no way to fill the gap if the folder is newer
;; than the cache at quitting.
;; Even if the cache is newer, saving marks faces dilemma if 
;; multiple Emacses run.

(provide 'mew-mark)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-mark.el ends here

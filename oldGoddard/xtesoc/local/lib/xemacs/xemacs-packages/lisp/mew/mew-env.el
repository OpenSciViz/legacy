;;; mew-env.el --- Environment setup for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar  6, 1997
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-env-version "mew-env.el version 0.12")

(require 'mew)

(defvar mew-temacs-p nil)
(defvar mew-xemacs-p nil)
(defvar mew-mule-p   nil)
(defvar mew-icon-p   nil)
(defvar mew-mule-version 0)

(if (featurep 'mule) (setq mew-mule-p t))

(cond
 ((string-match "XEmacs" emacs-version)
  (setq mew-temacs-p nil)
  (setq mew-xemacs-p t)
  (require 'mew-xemacs)
  (if (and (featurep 'xpm) (eq (console-type) 'x))
      (setq mew-icon-p t))
  (if (string-match "^19" emacs-version)
      (require 'mew-mule0)
    (if (null mew-mule-p)
	(require 'mew-mule0)
      (require 'mew-mule3)
      (setq mew-mule-version 3)))
  )
 (t
  (setq mew-temacs-p t)
  (setq mew-xemacs-p nil)
  (require 'mew-temacs)
  (if (null mew-mule-p)
    (require 'mew-mule0)
    (if (= (string-to-int mule-version) 3)
      (progn
        (require 'mew-mule3)
        (setq mew-mule-version 3)
      )
      (require 'mew-mule2)
      (setq mew-mule-version 2)
    )
   )
  )
 )

(cond 
 (mew-xemacs-p
  (or (find-face 'underline)
      (progn (make-face 'underline)
	     (set-face-underline-p 'underline t)))
  (fset 'mew-overlay-p (symbol-function 'extentp))
  (fset 'mew-overlay-make (symbol-function 'make-extent))
  (defun mew-overlay-move (overlay beg end &optional buffer)
    (set-extent-endpoints overlay beg end))
  (defun mew-overlay-put (overlay prop value)
    (set-extent-property overlay prop value))
  (fset 'mew-buffer-substring (symbol-function 'buffer-substring))
  (defun mew-mark () (mark t))
  (defun mew-pop-to-buffer (buf)
    (select-window (display-buffer buf nil (selected-frame)))
    (set-buffer buf))
  )
 (mew-temacs-p
  (if window-system (require 'faces))
  (fset 'mew-overlay-p (symbol-function 'overlayp))
  (fset 'mew-overlay-make (symbol-function 'make-overlay))
  (fset 'mew-overlay-move (symbol-function 'move-overlay))
  (fset 'mew-overlay-put (symbol-function 'overlay-put))
  (require 'easymenu)
  (if (fboundp 'buffer-substring-no-properties)
      (fset 'mew-buffer-substring
	    (symbol-function 'buffer-substring-no-properties))
    (defun mew-buffer-substring (beg end)
      "Return the text from BEG to END, without text properties, as a string."
      (let ((string (buffer-substring beg end)))
	(set-text-properties 0 (length string) nil string)
	string)))
  (defun mew-mark () (marker-position (mark-marker)))
  (fset 'mew-pop-to-buffer (symbol-function 'pop-to-buffer))
  )
 )

(if (fboundp 'characterp)
    (fset 'mew-characterp (symbol-function 'characterp))
  (fset 'mew-characterp (symbol-function 'integerp)))

(if (fboundp 'string-width)
    (fset 'mew-string-width (symbol-function 'string-width))
  (fset 'mew-string-width (symbol-function 'length)))

;; to avoid competition with mh-e.el, sigh.
(if (rassq 'mh-letter-mode auto-mode-alist)
    (setq auto-mode-alist
	  (delete (rassq 'mh-letter-mode auto-mode-alist)
		  auto-mode-alist)))

(defvar mew-connection-type1 nil
  "Connection type for many processes. t means PTY and nil means PIPE.
PIPE is usually recommended for speed but some OSes such as Linux 
requires PTY.")

(defvar mew-connection-type2 t 
  "Connection type for processes that requires a password. 
This must be PTY.")

(cond
 ((fboundp 'make-symbolic-link)
  (defun mew-symbolic-link (filename newname &optional OK-IF-ALREADY-EXISTS)
    (if (file-directory-p (file-chase-links filename))
	(error "Can't make a symbolic link to directory")
      (make-symbolic-link filename newname OK-IF-ALREADY-EXISTS)))
  (defun mew-link (filename newname &optional OK-IF-ALREADY-EXISTS)
    (if (file-directory-p (file-chase-links filename))
	(error "Can't make a link to directory")
      (add-name-to-file filename newname OK-IF-ALREADY-EXISTS)))
  )
 (t
  (defun mew-symbolic-link (filename newname &optional OK-IF-ALREADY-EXISTS)
    (if (file-directory-p filename)
	(error "Can't make a copy of directory")
      (copy-file filename newname OK-IF-ALREADY-EXISTS 'keepdate)))
  (defun mew-link (filename newname &optional OK-IF-ALREADY-EXISTS)
    (if (file-directory-p filename)
	(error "Can't make a copy of directory")
      (copy-file filename newname OK-IF-ALREADY-EXISTS 'keepdate)))
  )
 )

(cond
 ((fboundp 'string-to-char-list)
  (defalias 'mew-string-to-list 'string-to-char-list))
 ((fboundp 'string-to-list)
  (defalias 'mew-string-to-list 'string-to-list))
 )

(provide 'mew-env)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-env.el ends here

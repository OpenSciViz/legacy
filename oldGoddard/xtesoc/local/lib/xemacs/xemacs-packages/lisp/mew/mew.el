;;; mew.el --- Messaging in the Emacs World

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar 22, 1994
;; Revised: Jun 08, 1998

;;; Commentary:

;; The updated version is available from:
;;	ftp://ftp.Mew.org/pub/Mew/mew-current.tar.gz
;;	http://www.Mew.org/
;;
;; Minimum setup:
;;	(autoload 'mew "mew" nil t)
;;	(autoload 'mew-send "mew" nil t)
;;	(setq mew-mail-domain-list '("your mail domain"))
;;	(setq mew-icon-directory "icon directory")
;;

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Mew version
;;;

(defconst mew-version "Mew version 1.93b38")
(provide 'mew)
(require 'mew-vars)
(require 'mew-func)

(defun mew-version-show ()
  "Show mew-version in minibuffer."
  (interactive)
  (message "%s" mew-version))

(defvar mew-x-mailer
  (concat mew-version " on "
          (if (string-match "XEmacs" emacs-version) "XEmacs" "Emacs")
          (format " %d.%d " emacs-major-version emacs-minor-version)
          (and (boundp 'xemacs-codename) (concat "(" xemacs-codename ")"))
          (and (boundp 'mule-version) (concat "/ Mule " mule-version)))
  "*A value inserted into X-Mailer: field in Draft mode if non-nil.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; For developers
;;;

(defvar mew-debug nil)
;(setq mew-debug nil)
;(setq mew-debug t)

(defun mew-buffer-message ()
  (if window-system
      (concat
       mew-buffer-message
       (int-to-string
	(mew-member-case-equal 
	 (cdr (assq
	       'window-id
	       (frame-parameters (window-frame (selected-window)))))
	 (sort
	  (mapcar
	   (function (lambda (frame)
		       (cdr (assq
			     'window-id
			     (frame-parameters frame)))))
	   (frame-list))
	  (function string<)))))
    mew-buffer-message
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Initialize global variables
;;;

(defvar mew-mail-path nil)
(defvar mew-news-path nil)
(defvar mew-queue-path nil)
(defvar mew-inbox-folder nil)
(defvar mew-draft-folder nil)
(defvar mew-trash-folder nil)
(defvar mew-imap-account nil)
(defvar mew-config-cases nil)

(defvar mew-msg-rm-policy 'trashonly
  "Set remove policy. You can set one of the followings:
'totrash   : Refile to the +trash folder if not in the +trash folder.
             Just unmark the 'D' mark if in the +trash folder.
'always    : Really remove messages marked with 'D' always anyway.
'trashonly : Really remove messages marked with 'D' if in the +trash folder.
             In other folders, refile to the +trash folder.
'uselist   : Really remove messages marked with 'D' if in a folder found
	     in @samp{mew-msg-rm-folder-list}. In other folders, refile 
             to the +trash folder.
otherwise  : considered as 'totrash.
")

(defvar mew-msg-rm-folder-list nil
  "Folder list to remove message really.")

(defvar mew-petname-file nil)
(defvar mew-draft-mime-folder nil
  "A directory where attachments are contained.")
(defvar mew-path-alist
  '(("MailPath" . mew-mail-path)
    ("NewsPath" . mew-news-path)
    ("QueuePath" . mew-queue-path)
    ("InboxFolder" . mew-inbox-folder)
    ("DraftFolder" . mew-draft-folder)
    ("TrashFolder" . mew-trash-folder)
    ("PetnameFile" . mew-petname-file)
    ("ImapAccount" . mew-imap-account)
    ("ConfigCases" . mew-config-cases))
  )

(setplist 'mew-current-state 
	  (list
	   'message    nil 
	   'cache      nil
	   'part       nil
	   'window     nil
	   ))

(defun mew-current-get (key)
  (get 'mew-current-state key))

(defun mew-current-set (key value)
  (put 'mew-current-state key value))

(setq mew-alias-alist  ())
(setq mew-folder-list  ())
(setq mew-folder-alist ())
(setq mew-petname-alist ())
(setq mew-clean-up-buffers ())

(setq mew-message-citation nil)
(setq mew-message-citation-buffer nil) ;; should be nil

(setq mew-process-file-alist nil)

;; See mew-temp-file-initial.
(setq mew-temp-dir nil)  ;; the default is "/tmp/user_name_uniq"
(setq mew-temp-file nil) ;; the default is "/tmp/user_name_uniq/mew"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Window configuration stack
;;;

(defvar mew-window-stack nil)

(defun mew-window-push ()
  (let ((frame (selected-frame))
	(config (current-window-configuration)))
    (setq mew-window-stack (cons (cons frame config) mew-window-stack))
    )
  )

(defun mew-window-pop ()
  (let* ((frame (selected-frame))
	 (assoc (assoc frame mew-window-stack)))
    (if (and assoc (window-configuration-p (cdr assoc)))
	(set-window-configuration (cdr assoc))
      (switch-to-buffer (get-buffer-create mew-window-home-buffer)))
    (setq mew-window-stack (delete assoc mew-window-stack))
    )
  )

;;;
;;; Window configuration
;;;

(defun mew-window-configure (nbuf action)
;;; action : summary, message, draft or list
;;; list for action (1 0)  for Summary only
;;; list for action (3 10) for Summary and Message
  (if (equal action 'summary)
      (mew-current-set 'message nil))
  (let* ((windows
	  (if (listp action) 
	      action
	    (car (cdr (assq action mew-window-configuration)))))
	 (obufwin (get-buffer-window (current-buffer)))
	 (msgwin  (get-buffer-window (mew-buffer-message)))
	 (height nil) (winsum nil) (sum-height 0) (msg-height 0))
    (setq height (+ (if obufwin (window-height obufwin) 0)
		    (if msgwin  (window-height msgwin)  0)))
    (if (or mew-window-use-full
	    (<= height (* 2 window-min-height)))
	(progn
	 ;; Delete other windows and use full emacs window.
	 (delete-other-windows)
	 (setq height (window-height (selected-window)))))
    (if (get-buffer (mew-buffer-message))
	(delete-windows-on (mew-buffer-message))
      (save-excursion
	(set-buffer (get-buffer-create (mew-buffer-message)))
	;; "truncate?" is asked in Message mode.
	;; so set the same toolbar as Sumamry mode
	(if mew-icon-p
	    (set-specifier default-toolbar
			   (cons (current-buffer) mew-summary-toolbar)))
	(mew-message-mode)))
    (setq winsum (apply (function +) windows))
    (if (not (zerop (nth 0 windows)))
	(setq sum-height (max window-min-height
			     (/ (* height (nth 0 windows)) winsum))))
    (if (and (equal action 'summary) (equal (% sum-height 2) 1)) 
	(setq sum-height (1+ sum-height)))
    (if (not (zerop (nth 1 windows)))
	(setq msg-height (max window-min-height
			     (- height sum-height))))
    (setq height (+ sum-height msg-height))
    (if (null (zerop sum-height))
	(switch-to-buffer nbuf 'norecord))
    (if (zerop msg-height)
	()
      (split-window nil sum-height)
      (other-window 1)
      (switch-to-buffer (mew-buffer-message) 'norecord))
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Bootstrap
;;;

(defun mew (&optional arg)
  "Execute Mew. If 'mew-auto-get' is 't', messages stored in your
spool are fetched to the +inbox folder and messages in the +inbox
folder are listed up in Summary mode. If 'mew-auto-get' is 'nil', list
up messages in the inbox folder. If 'C-u' is specified, perform this
function thinking that 'mew-auto-get' is reversed."
  (interactive "P")
  (let ((auto (if arg (not mew-auto-get) mew-auto-get)))
    (mew-window-push)
    (if (null mew-mail-path) (mew-init))
    (if auto
	(mew-summary-get)
      (mew-summary-goto-folder t mew-inbox-folder)
      )))

(defun mew-send (&optional to cc subject)
  (interactive)
  (mew-current-set 'window (current-window-configuration))
  (if (null mew-mail-path) (mew-init))
  (mew-summary-send t to cc subject)
  )

;;;
;;; Functions for boot time
;;;

(defun mew-init ()
  (mew-hello)
  (message "Setting Mew world ...")
  (mew-set-environment)
  (if mew-use-bbdb (require 'bbdb-com)) ;; bbdb is implicitly required
  (run-hooks 'mew-init-hook)
  (mew-status-update t)
  (message "Setting Mew world ... done")
  )

(defun mew-set-environment ()
  (let (error-message dir dirs)
    (condition-case nil
	(progn
	  (if (string-match "^18" emacs-version)
	      (progn
		(setq error-message "Not support Emacs 18 nor Mule 1\n")
		(error "")
		))
	  ;;
	  ;; initialize IM variables
	  ;;
	  (if (setq error-message (mew-read-config))
	      (error ""))
	  ;;
	  (setq mew-clean-up-buffers (list mew-inbox-folder)) ; for quiting
	  ;;
	  ;; mew-temp-file must be local and readable for the user only
	  ;; for privacy/speed reasons.
	  ;;
	  (setq mew-temp-dir (make-temp-name mew-temp-file-initial))
	  (mew-make-directory mew-temp-dir)
	  (set-file-modes mew-temp-dir mew-folder-mode)
	  (setq mew-temp-file (expand-file-name "mew" mew-temp-dir))
	  ;;
	  (add-hook 'kill-emacs-hook (function mew-mark-clean-up-all))
	  (add-hook 'kill-emacs-hook (function mew-temp-dir-clean-up))
	  ;;
	  (setq dirs (list mew-mail-path mew-news-path))
	  (while dirs
	    (setq dir (file-chase-links (car dirs)))
	    (setq dirs (cdr dirs))
	    (if (file-exists-p dir)
		(if (/= mew-folder-mode (mew-get-file-modes dir))
		    (set-file-modes dir mew-folder-mode))))
	  )
      (error
       (set-buffer mew-buffer-hello)
       (goto-char (point-max))
       (insert "\n\nMew errors:\n\n")
       (and error-message (insert error-message))
       (set-buffer-modified-p nil)
       (setq buffer-read-only t)
       (error "Mew found some errors above.")) ;; cause an error again
      )
    ))

(defun mew-status-update (arg)
  "Update the list of aliases. If you type 'C-u Z' the list of folders
are also updated in addition to that of aliases.  If 
'mew-use-folders-file-p' is t, the list of folders is stored in
\"~/Mail/.folders\"."
  (interactive "P")
  (message "Updating status ... ")
  (mew-highlight-make-keywords-regex)
  (setq mew-alias-alist (mew-alias-make-alist))
  (setq mew-petname-alist (mew-petname-make-alist))
  (mew-pgp-set-version)
  (if (not arg)
      ()
    (setq mew-folder-list (mew-folder-make-list (interactive-p)))
    (setq mew-folder-alist (mew-folder-make-alist mew-folder-list)))
  (message "Updating status ...   done"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Config
;;;

(defun mew-read-config ()
  (mew-set-buffer-tmp)
  (if (not (mew-which mew-prog-impath exec-path))
      (format "%s is not found in 'exec-path'" mew-prog-impath)
    (mew-im-call-process nil mew-prog-impath "--path=yes")
    (goto-char (point-min))
    (let (key value pair)
      (while (not (eobp))
	(if (looking-at "^\\(.+\\)=\\(.+\\)$")
	    ;; Petname may be null string.
	    (progn
	      (setq key (mew-match 1))
	      (setq value (mew-match 2))
	      (if (setq pair (mew-assoc-match2 key mew-path-alist 0))
		  (set (cdr pair) value))
	      ))
	(forward-line)
	))
    (if mew-config-cases
	(setq mew-config-list (mew-split "," mew-config-cases)))
    (let (errmsg)
      (if mew-mail-path
	  ()
	(setq errmsg (concat errmsg "\tMailDir was NOT found in Config.\n")))
      (if mew-news-path
	  ()
	(setq errmsg (concat errmsg "\tNewsDir was NOT found in Config.\n")))
      (if mew-inbox-folder
	  ()
	(setq errmsg (concat errmsg "\tInboxFolder was NOT found in Config.\n")))
      (if mew-draft-folder
	  ;; xxx ugh! what a hard coding... +draft/mime
	  (setq mew-draft-mime-folder
		(concat (file-name-as-directory mew-draft-folder) "mime"))
	(setq errmsg (concat errmsg "\tDraftFolder was NOT found in Config.\n")))
      (if mew-trash-folder
	  ()
	(setq errmsg (concat errmsg "\tTrashDir was NOT found in Config.\n")))
      (if errmsg
	  errmsg ;; return value
	(let ((folders (list mew-inbox-folder mew-draft-folder mew-trash-folder))
	      target)
	  (while folders
	    (setq target (mew-expand-folder (car folders)))
	    (if (file-exists-p target)
		()
	      (mew-make-directory target)
	      (message "%s was created" target))
	    (setq folders (cdr folders))
	    ))
	nil) ;; return value
      )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; e-mail address alias
;;;

(defun mew-alias-make-alist ()
  "Make alias alist with association of (alias . expantion) from
IM \"imali\" command. Currently, only \"user: user@domain\" syntax
is supported."
  (save-excursion
    (let ((case-fold-search t)
	  (alias nil)
	  (expn nil)
	  (alist nil))
      (mew-set-buffer-tmp)
      (mew-im-call-process nil mew-prog-imali)
      ;; concat separated lines by comma
      (goto-char (point-min))
      (while (re-search-forward ",$" nil t)
	(end-of-line)
	(forward-char 1)
 	(delete-backward-char 1))
      ;;
      (goto-char (point-min))
      (while (re-search-forward "^\\([^:]+\\):[ \t]*\\(.*\\)$" nil t)
	(setq alias (mew-match 1)
	      expn (mew-match 2))
	;; append for first assoc comes first
	(setq alist (cons (cons alias expn) alist)))
      ;; load from mew-refile-from-alist
      (setq alist (mew-refile-alist-append-alias alist))
      (nreverse alist) ; return value
      )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Folders
;;;

(defun mew-folder-make-list (updatep)
  (save-excursion
    (let ((case-fold-search t)
	  (folders ())
	  (folder nil)
	  (start nil)
	  (file (expand-file-name mew-folders-file mew-mail-path)))
      (mew-set-buffer-tmp)
      (cond
       ((and (not updatep)
	     mew-use-folders-file-p
	     (file-readable-p file))
	(insert-file-contents file))
       (t
	(mapcar (lambda (x) (insert x "\n"))
		(nconc (funcall mew-folder-list-function "+")
		       (funcall mew-folder-list-function "=")))
	(if mew-use-folders-file-p
	    (write-region (point-min) (point-max) file nil 'no-msg)))
       )
      (goto-char (point-min))
      (while (not (eobp))
	(setq start (point))
	(if (not (looking-at "[+=]"))
	    (forward-line)
	  (forward-line)
	  (setq folder (mew-buffer-substring start (1- (point))))
	  (if (and (car folders)
		   (string-match (concat "^" (regexp-quote 
					      (file-name-as-directory
					       (car folders))))
				 folder))
	      ;; regexp-quote is not necessary since not "+".
	      (setq folders 
		    (cons folder 
			  (cons (file-name-as-directory (car folders))
				(cdr folders))))
	    (setq folders (cons folder folders))))
	)
      folders ;; return value
      )))

(defmacro mew-folder-make-alist (list)
  (` (mapcar (function mew-folder-pair) (, list)))
  )

(defun mew-folder-pair (folder)
  (let* ((dir (directory-file-name (mew-folder-to-dir folder)))
	 ;; foo/bar  -> foo/bar
	 ;; foo/bar/ -> foo/bar
	 (subdir (file-name-nondirectory dir)))
	 ;; foo/bar -> bar 
	 ;; foo -> foo
    (if (mew-folders-ignore-p folder)
	(list folder nil)
      (list folder subdir)
      )
    ))

(defun mew-folders-ignore-p (folder)
  (let ((ignores mew-folders-ignore))
    (catch 'ignore
      ;; while always returns nil
      (while ignores
	(if (string-match (concat "^" (car ignores)) folder)
	    (throw 'ignore t))
	(setq ignores (cdr ignores))
	))
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Petnames
;;;

(defun mew-petname-make-alist ()
  (if (and mew-use-petname mew-petname-file (file-readable-p mew-petname-file))
      (save-excursion
	(let (ret)
	  (mew-set-buffer-tmp)
	  (insert-file-contents mew-petname-file)
	  (goto-char (point-min))
	  (while (not (eobp))
	    (if (looking-at "^\\([^ \t]+\\)[ \t]+\"\\(.*\\)\"$")
		(setq ret (cons (cons (mew-match 1) (mew-match 2)) ret)))
	    (forward-line))
	  ret))
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Teer down
;;;

(defun mew-temp-dir-clean-up ()
  "A function to remove Mew's temporary directory recursively. 
It is typically called by kill-emacs-hook."
  (if (and mew-temp-dir (file-exists-p mew-temp-dir))
      (mew-delete-directory-recursively mew-temp-dir))
  )

(defun mew-summary-suspend ()
  "Suspend Mew then switch to another buffer. All buffers of 
Mew retain, so you can resume with buffer operations."
  (interactive)
  (mew-window-pop)
  ;; bury buffers
  (if window-system
      (let ((x (buffer-list))
	    (regexp (concat "^" (regexp-quote mew-buffer-message))))
	(while x
	  (if (and (buffer-name (car x))
		   (string-match regexp (buffer-name (car x))))
	      (bury-buffer (car x)))
	  (setq x (cdr x))))
    (bury-buffer (mew-buffer-message)))
  (let ((x mew-clean-up-buffers))
    (while x
      (if (get-buffer (car x))
	  (bury-buffer (car x)))
      (setq x (cdr x))))
  (set-buffer (car (buffer-list)))
  (run-hooks 'mew-suspend-hook)
  )

(defun mew-kill-buffer (&optional buf)
  "Erase the current mode(buffer)."
  (interactive)
  (if buf
      (if (get-buffer buf) (kill-buffer buf))
    (kill-buffer (current-buffer)))
  )

(defun mew-summary-quit ()
  "Quit Mew. All buffers of Mew are erased."
  (interactive)
  (if (not (y-or-n-p "Quit Mew? "))
      ()
    (mew-window-pop)
    ;; killing buffers
    ;; message buffers
    (if (and window-system)
	(let ((x (buffer-list))
	      (regexp (concat "^" mew-buffer-message)))
	  (while x
	    (if (and (buffer-name (car x))
		     (string-match regexp (buffer-name (car x))))
		(mew-kill-buffer (car x)))
	    (setq x (cdr x))
	    ))
      (mew-kill-buffer (mew-buffer-message)))
    ;; cache buffers
    (let ((x (buffer-list))
	  (regex (concat "^" (regexp-quote mew-buffer-cache))))
      (while x
	(if (and (buffer-name (car x))
		 (string-match regex (buffer-name (car x))))
	    (mew-kill-buffer (car x)))
	(setq x (cdr x))
	))
    ;;
    (mew-kill-buffer mew-buffer-hello)
    (mew-kill-buffer mew-buffer-mime)
    (mew-kill-buffer mew-buffer-tmp)
    ;; xxx summary & draft buffers...
    ;;
    (remove-hook 'kill-emacs-hook (function mew-temp-dir-clean-up))
    (mew-temp-dir-clean-up)
    (setq mew-temp-dir nil mew-temp-file nil)
    ;;
    (remove-hook 'kill-emacs-hook (function mew-mark-clean-up-all))
    (mew-mark-clean-up-all)
    (while mew-clean-up-buffers
      (mew-kill-buffer (car mew-clean-up-buffers))
      (setq mew-clean-up-buffers (cdr mew-clean-up-buffers))
      )
    (setq mew-clean-up-buffers (list mew-inbox-folder))
    ;;
    (run-hooks 'mew-quit-hook)
    ;; lastly, clean up variables
    (setq mew-mail-path nil)
    (setq mew-news-path nil)
    (setq mew-window-stack nil)
    (setq mew-draft-folder nil)
    (setq mew-alias-alist ())
    (setq mew-folder-list ())
    (setq mew-folder-alist ())
    (setq mew-cache nil)
    (message "") ;; flush minibuffer
    )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Load Mew libraries
;;;
(require 'mew-complete)
(require 'mew-minibuf)
(require 'mew-cache)
(require 'mew-encode)
(require 'mew-decode)
(require 'mew-mime)
(require 'mew-mark)
(require 'mew-header)
(require 'mew-pgp)
(require 'mew-bq)
(require 'mew-syntax)
(require 'mew-scan)
(require 'mew-pick)
(require 'mew-summary)
(require 'mew-virtual)
(require 'mew-message)
(require 'mew-draft)
(require 'mew-attach)
(require 'mew-demo)
(require 'mew-refile)
(require 'mew-ext)
(require 'mew-fib)
(require 'mew-sort)
(require 'mew-highlight)

;;; Copyright Notice:

;; Copyright (C) 1994, 1995, 1996, 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew.el ends here

;;; mew-virtual.el --- Virtual mode for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Oct  2, 1996
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-virtual-version "mew-virtual.el version 0.16")

(require 'mew)

(defvar mew-virtual-mode-map    nil)

(if mew-virtual-mode-map
    ()
  (setq mew-virtual-mode-map (make-sparse-keymap))
  (define-key mew-virtual-mode-map " "    'mew-summary-show)
  (define-key mew-virtual-mode-map "."    'mew-summary-display)
  (define-key mew-virtual-mode-map "<"    'mew-summary-display-top)
  (define-key mew-virtual-mode-map ">"    'mew-summary-display-bottom)
  (define-key mew-virtual-mode-map "\177" 'mew-summary-prev-page)
  (define-key mew-virtual-mode-map "\r"   'mew-summary-scroll-up)
  (define-key mew-virtual-mode-map "-"    'mew-summary-scroll-down)
  (define-key mew-virtual-mode-map "\e\r" 'mew-summary-scroll-down)
  (define-key mew-virtual-mode-map "g"    'mew-summary-goto-folder)
  (define-key mew-virtual-mode-map "j"    'mew-summary-jump-message)
  (define-key mew-virtual-mode-map "i"    'mew-summary-get)
  (define-key mew-virtual-mode-map "a"    'mew-summary-reply)
  (define-key mew-virtual-mode-map "A"    'mew-summary-reply-with-citation)
  (define-key mew-virtual-mode-map "E"    'mew-summary-reedit)
  (define-key mew-virtual-mode-map "\ee"  'mew-summary-edit-again)
  (define-key mew-virtual-mode-map "f"    'mew-summary-forward)
  (define-key mew-virtual-mode-map "F"    'mew-summary-multi-forward)
  (define-key mew-virtual-mode-map "r"    'mew-summary-redist)
  (define-key mew-virtual-mode-map "@"    'mew-summary-multi)
  (define-key mew-virtual-mode-map "*"    'mew-summary-review)
  (define-key mew-virtual-mode-map "y"    'mew-summary-save)
  (define-key mew-virtual-mode-map "u"    'mew-summary-undo)
  (define-key mew-virtual-mode-map "U"    'mew-summary-undo-all)
  (define-key mew-virtual-mode-map "n"    'mew-summary-display-down)
  (define-key mew-virtual-mode-map "p"    'mew-summary-display-up)
  (define-key mew-virtual-mode-map "N"    'mew-summary-display-review-down)
  (define-key mew-virtual-mode-map "P"    'mew-summary-display-review-up)
  (define-key mew-virtual-mode-map "w"    'mew-summary-send)
  (define-key mew-virtual-mode-map "B"    'mew-summary-burst)
  (define-key mew-virtual-mode-map "Z"    'mew-status-update)
  (define-key mew-virtual-mode-map "#"    'mew-summary-print)
  (define-key mew-virtual-mode-map "|"    'mew-summary-pipe-message)
  (define-key mew-virtual-mode-map "q"    'mew-summary-suspend)
  (define-key mew-virtual-mode-map "Q"    'mew-summary-quit)
  (define-key mew-virtual-mode-map "\C-c\C-e" 'mew-summary-execute-external)
  (define-key mew-virtual-mode-map "\C-c\C-f" 'mew-pgp-fetch-key)
  (define-key mew-virtual-mode-map "\C-c\C-i" 'mew-summary-insert)
  (define-key mew-virtual-mode-map "\C-c\C-s" 'mew-summary-isearch-forward)
  (define-key mew-virtual-mode-map "\C-c\C-r" 'mew-summary-isearch-backward)
  (define-key mew-virtual-mode-map "\C-c\C-o" 
    'mew-summary-jump-to-draft-buffer)
  (define-key mew-virtual-mode-map "\el"  'mew-summary-recenter)
  (define-key mew-virtual-mode-map "\et"  'mew-summary-uudecode)
  (define-key mew-virtual-mode-map "\C-c\C-l" 'mew-summary-convert-local-cs)
  (define-key mew-virtual-mode-map "\es"  'mew-summary-unshar)
  (define-key mew-virtual-mode-map "v"    'mew-summary-toggle-disp-msg)
  (define-key mew-virtual-mode-map "\ea"  'mew-summary-toggle-analysis)
  (define-key mew-virtual-mode-map "\C-c\C-x" 'mew-summary-x-face)
  (define-key mew-virtual-mode-map "\C-c\C-q" 'mew-kill-buffer)
  (if mew-xemacs-p
      (define-key mew-virtual-mode-map 'button2 'mew-summary-mouse-show)
    (define-key mew-virtual-mode-map [mouse-2] 'mew-summary-mouse-show))
  )

;;;
;;; Virtual mode
;;;

(defun mew-virtual-mode ()
  "Major mode for reading messages.
The keys that are defined for this mode are:

SPC	Read through messages. That is, display a message, scroll it, 
	and move-then-display another message. 
	See 'mew-summary-show-direction' to set 'up, 'down, 
	'next(current direction) or 'stop. Default is 'down.
DEL	Back-scroll this message. Unnecessary header fields are hidden
	over the window. Type DEL to see them when a message is displayed.
.	Force to display this message. If without MIME analysis, force 
	to analyze this message.

RET	Make this message scroll up with one line.
M-RET	Make this message scroll down with one line.
-	Make this message scroll down with one line.

C-n	Go to the next line.
C-p	Go to the previous line.
n	Move to below then display. Targets includes parts, messages 
	marked with '*', and non-marked messages.
p	Move to above then display. Targets includes parts, messages 
	marked with '*', and non-marked messages.
j	Jump to a message according to the number which you input.

i	Get +inbox asynchronously.
g	Go to the folder which you input.

w	Write a message. A new draft is prepared in Draft mode.
a	Answer to this message. A new draft is prepared in Draft mode. 
	Mew automatically decides To: and Cc:.
A	Answer to this message. A new draft is prepared in Draft mode. 
	Mew automatically decides To: and Cc: and cites the body.
f	Forward this message to a third person. A new draft is prepared in 
	Draft mode and this message is automatically attached.
F	Forward messages marked with '@' to a third person. A new draft 
	is prepared in Draft mode and this message is automatically 
	attached. 

E	Edit this message again to send. Or edit this rfc822 part
	typically included MIME-encapsulated error message.
	In a draft folder, it just edits the message. Otherwise, 
	copy the message to draft folder, then edit.
M-e	Edit an old fashioned error message in which the original message 
	is encapsulated after \"----- Original message follows -----\".
r	Re-distribute this message with Resent-To:. It is strongly 
	discouraged to use this command since beginners are always 
	confused. Please use 'f' instead.

v	Toggle \"Virtual mode only\" and \"Virtual & Message mode\". If 
	you choose \"Virtual mode only\", you can quickly put the delete 
	marks since the next message is not displayed.
M-a	Toggle \"MIME analysis mode\" and \"non MIME analysis mode\". In 
	\"non MIME analysis mode\", the message is quickly displayed in 
	 Message mode because MIME analysis is skipped.
M-l	Make the current line to the center of Virtual mode.

*	Put the review the '*' mark on this message. 
	Use N or P to jump to a message marked with '*'.
	It can overlay '@'. The cursor stays always.
	See also 'mo', 'md', 'mr', and 'ma'.
N	Jump to the message marked with '*' below.
P	Jump to the message marked with '*' above.

@	Put the multi the '@' mark on this message for 'F', 'M-s', 
	and 'M-t'. It can overlay the '*' mark. The cursor stays always.
u	Cancel the mark on this message.
U	Cancel all marks according to what you input.

C-cC-s	Incremental search forward in Message mode.
C-cC-r	Incremental search backward in Message mode.

M-s	Apply \"unshar\" on messages marked with '@'.
M-t	Apply \"uudecode\" on messages marked with '@'.

y	Copy this message or save this part as the file name which 
	you input.
#	Print this message or this part.
|	Send this message via pipe.

S	Sort messages and list them up again.
O	Pack messages and list them up again.
B	De-capsulate embedded messages in MIME format.
Z	Update the list of aliases. If you type 'C-u Z' the list 
	of folders are also updated in addition to that of aliases. 

q	Suspend Mew then switch to another buffer. All buffers of 
	Mew retain, so you can resume with buffer operations.
Q	Quit Mew. All buffers of Mew are erased.
C-cC-q	Erase the current mode(buffer).

C-cC-l	Convert to character sets used locally.
C-cC-x	Display xface.
C-cC-f	PGP public key fetch.
"
  (interactive)
  (setq major-mode 'mew-virtual-mode)
  (setq mode-name "Virtual")
  (setq mode-line-buffer-identification mew-mode-line-id)
  (use-local-map mew-virtual-mode-map)
  (setq buffer-read-only t)
  (setq truncate-lines t)
  (setq selective-display t)
  (setq selective-display-ellipses nil)
  (if (equal (nth (- (length mode-line-format) 2) mode-line-format)
	     '(-3 . "%p"))
      (setq mode-line-format
	    (let ((mlf (copy-sequence mode-line-format))
		  (l (length mode-line-format)))
	      (setcdr (nthcdr (- l 3) mlf)
		      '("[" mew-summary-buffer-left-msgs " more]" "-%-"))
	      mlf)
	    ))
  (if mew-xemacs-p
      (progn
	(if mew-icon-p
	    (set-specifier default-toolbar
			   (cons (current-buffer) mew-summary-toolbar)))
        (set-specifier scrollbar-height (cons (current-buffer) 0))
        (set-buffer-menubar current-menubar)
        (mew-summary-make-popup-menu t)
        (add-submenu nil mew-summary-mode-menu-spec)
	))
  (mew-virtual-highlight-setup)
  (mew-highlight-cursor-line)
  (run-hooks 'mew-virtual-mode-hook)
  )

(defun mew-virtual-folder-message ()
  (interactive)
  (looking-at "^ *\\([0-9]+\\).*\r\\(.*\\)$")
  (message "%s" (mew-match 2))
  )

(defun mew-summary-virtual ()
  (interactive)
  (let ((folder (concat 
	       "++" 
	       (mew-input-string "Virtual folder name %s(%s): " 
				 "" ;; dummy
				 "virtual")))
	(folders (mew-input-folders (buffer-name)))
	(grep (mew-input-pick-pattern)))
    (mew-summary-scan-body mew-prog-imls
			   'mew-virtual-mode
			   folder
			   mew-cs-virtual
			   nil
			   folders
			   grep)
    )
  )

(provide 'mew-virtual)

;;; Copyright Notice:

;; Copyright (C) 1996, 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-virtual.el ends here

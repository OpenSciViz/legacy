;;; mew-vars.el --- Variables and Constants for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Nov 22, 1997
;; Revised: 

;;; Code:

(defconst mew-vars-version "mew-vars.el version 0.01")

(require 'mew-env)

;;;
;;; Global Constants
;;;

;; Must be capitalized!
(defconst mew-mv: "Mime-Version:")
(defconst mew-mv:-num "1.0")
(defconst mew-ct:  "Content-Type:")
(defconst mew-cte: "Content-Transfer-Encoding:")
(defconst mew-cd:  "Content-Description:")
(defconst mew-cid: "Content-ID:")
(defconst mew-cdp: "Content-Disposition:")

(defconst mew-x-mew: "X-Mew:")

(defconst mew-ct-txt "Text/Plain")
(defconst mew-ct-msg "Message/Rfc822")
(defconst mew-ct-ext "Message/External-Body")
(defconst mew-ct-mlm "Multipart/Mixed")
(defconst mew-ct-mls "Multipart/Signed")
(defconst mew-ct-mle "Multipart/Encrypted")
(defconst mew-ct-mld "Multipart/Digest")
(defconst mew-ct-ado "Audio/Basic")
(defconst mew-ct-apo "Application/Octet-Stream")
(defconst mew-ct-pgs "application/pgp-signature") ;; xxx case
(defconst mew-ct-pge "application/pgp-encrypted") ;; xxx case
(defconst mew-ct-apk "Application/Pgp-Keys")

(defconst mew-type-txt (list mew-ct-txt "charset=us-ascii"))
(defconst mew-type-msg (list mew-ct-msg))
(defconst mew-type-mlm (list mew-ct-mlm))
(defconst mew-type-ado (list mew-ct-ado))
(defconst mew-type-apo (list mew-ct-apo))
(defconst mew-type-apk (list mew-ct-apk))

(defconst mew-b64  "base64")
(defconst mew-qp   "quoted-printable")
(defconst mew-xg   "x-gzip64")
(defconst mew-7bit "7bit")
(defconst mew-8bit "8bit")
(defconst mew-bin  "binary")

(defconst mew-decode-value 
  (list mew-b64 mew-qp mew-xg mew-7bit mew-8bit mew-bin))

(defconst mew-mime-fields
  (list (list mew-ct:  'analyze)
	(list mew-cte: 'extract)
	(list mew-cd:  'decode)
	(list mew-cid: nil)
	(list mew-cdp: 'analyze)
	))
(defvar mew-address-fields
  '("To:" "Cc:" "From:"
    "Reply-To:" "In-Reply-To:"
    "Resent-To:" "Resent-Cc:" "Resent-From:")
  "*A list of RFC822 structured fields. If the field key is
a member of this list, its value is decoded as addr-spec.
Otherwise, its value is decoded as text.")

(defconst mew-syntax-magic 
  (+ (length [key beg end pri]) (length mew-mime-fields)))

(defconst mew-buffer-message     "*Mew message*")
(defconst mew-buffer-mime       " *mew mime*")
(defconst mew-buffer-cache      " *mew cache*")
(defconst mew-buffer-watch       "*Mew watch*")
(defconst mew-buffer-tmp        " *mew tmp*")
(defconst mew-buffer-burst      " *mew burst*")
(defconst mew-buffer-completions "*Mew completions*")
(defconst mew-buffer-whom        "*Mew whom*")
(defconst mew-buffer-ext        " *mew ext*")
(defconst mew-buffer-hello      " *mew hello*")

(defconst mew-home (file-name-as-directory "~"))

(defconst mew-ext-suffix       ".ext")
(defconst mew-text-suffix      ".txt")
(defconst mew-audio-suffix     ".au")
(defconst mew-pgp-key-suffix   ".pka")
(defconst mew-pgp-ascii-suffix ".asc")

;;;
;;; Buffer local variables
;;;

(defvar mew-summary-buffer-start-point nil)
(defvar mew-summary-buffer-process nil)
(defvar mew-summary-buffer-string nil)
(defvar mew-summary-buffer-reviews nil)
(defvar mew-summary-imap-process nil)
(defvar mew-summary-imap-string nil)
(defvar mew-summary-buffer-direction nil)
(defvar mew-summary-buffer-folder-cache-time nil)
(defvar mew-summary-buffer-refile nil)
(defvar mew-summary-buffer-disp-msg t)
(defvar mew-summary-buffer-left-msgs "-")
(defvar mew-watch-buffer-process nil)
(defvar mew-draft-buffer-header nil)
(defvar mew-draft-buffer-attach nil)
(defvar mew-cache-attribute nil)
(defvar mew-decode-syntax nil)
(defvar mew-decode-error nil)
(defvar mew-encode-syntax nil)
(defvar mew-overlay-cursor-line nil)
(defvar mew-complete-window-config nil)

(mapcar
 (function make-variable-buffer-local)
 (list 'mew-summary-buffer-start-point
       'mew-summary-buffer-process
       'mew-summary-buffer-string
       'mew-summary-buffer-reviews
       'mew-summary-imap-process
       'mew-summary-imap-string
       'mew-summary-buffer-direction
       'mew-summary-buffer-folder-cache-time
       'mew-summary-buffer-refile
       'mew-summary-buffer-disp-msg
       'mew-summary-buffer-left-msgs
       'mew-watch-buffer-process
       'mew-draft-buffer-header
       'mew-draft-buffer-attach
       'mew-cache-attribute
       'mew-encode-syntax
       'mew-decode-syntax
       'mew-decode-error
       'mew-overlay-cursor-line
       'mew-complete-window-config
       ))

;;;
;;; Global Variables
;;;

;;
;; Hooks
;;

(defvar mew-init-hook nil
  "*Hook called at initialize time.")
(defvar mew-summary-mode-hook nil
  "*Hook called in Summary mode.")
(defvar mew-virtual-mode-hook nil
  "*Hook called in Virtual mode.")
(defvar mew-draft-mode-hook nil
  "*Hook called in Draft mode.")
(defvar mew-message-mode-hook nil
  "*Hook called in Message mode.")
(defvar mew-message-hook nil
  "*Hook called whenever message displayed.")
(defvar mew-send-hook nil
  "*Hook called before sending a message")
(defvar mew-real-send-hook nil
  "*Hook called before really sending a message")  
(defvar mew-suspend-hook nil
  "*Hook called on suspend.")
(defvar mew-quit-hook nil
  "*Hook called on quit.")
(defvar mew-summary-inc-sentinel-hook nil
  "*Hook called when inc finished.")
(defvar mew-summary-scan-sentinel-hook nil
  "*Hook called when scan finished.")
(defvar mew-summary-exec-hook nil
  "*Hook called when mew-summary-exec finished.")

;;
;;
;; Sub-programs
;;

(defvar mew-prog-imget       "imget")
(defvar mew-prog-imls        "imls")
(defvar mew-prog-imcat       "imcat")
(defvar mew-prog-imclean     "imclean")
(defvar mew-prog-imali       "imali")
(defvar mew-prog-impath      "impath")
(defvar mew-prog-impack      "impack")
(defvar mew-prog-immv        "immv")
(defvar mew-prog-imrm        "imrm")
(defvar mew-prog-imput       "imput")
(defvar mew-prog-imsort      "imsort")
(defvar mew-prog-imstore     "imstore")
(defvar mew-prog-imgrep      "imgrep")
(defvar mew-prog-imjoin      "imjoin")
(defvar mew-prog-mime-encode "mewencode")
(defvar mew-prog-mime-decode "mewdecode")
(defvar mew-prog-unshar      "unshar")
(defvar mew-prog-uumerge     "uumerge")
(defvar mew-prog-tar         "tar")
(defvar mew-prog-compress    "compress")
(defvar mew-prog-gzip        "gzip")
(defvar mew-prog-utime       "touch")
(defvar mew-prog-pgp         "pgp")

(defvar mew-prog-uncompface  "uncompface")
(defvar mew-prog-shell       "/bin/sh")
;(defvar mew-prog-shell       "cmd.exe")

(defvar mew-prog-text/html        "netscape")
(defvar mew-prog-text/html-arg    nil)

(defvar mew-prog-shell-arg   "-c")

(defvar mew-prog-im-arg '("--help=no" "--debug=no"))

(defvar mew-prog-imget-arg-list nil
  "*Argument list for mew-prog-imget")
(defvar mew-prog-imls-arg-list nil
  "*Argument list for mew-prog-imls")

;;
;; MIME types
;;

(defvar mew-mime-content-type-text-list
  '("Text/Plain" "Text/Html"
    "Application/Postscript"
    "Message/Rfc822")
  "Content-Type: list for text which are treated as text when encoded"
  )

(defvar mew-mime-content-type-multipart-list
  '("Multipart/Mixed" "Multipart/Alternative"
    ;;"Multipart/Digest" "Multipart/Parallel"
    )
  "Candidate of 'Content-Type: Multipart' when CT: is changed in draft buffer."
  )

(defvar mew-mime-content-type-binary-list
  '("Application/Octet-Stream"
    "Image/Jpeg" "Image/Gif" "Image/x-xwd" "Image/x-xbm" "Image/x-xpm" "Image/x-bmp"
    "Image/Png" "Audio/Basic" "Video/Mpeg"
    )
  "Content-Type: list to save as binary file in mew-summary-save on Mule"
  )

(defvar mew-mime-content-type-others-list
  '("Message/Rfc822" "Message/External-body")
  )

(defvar mew-mime-content-type-list
  (append mew-mime-content-type-text-list
	  mew-mime-content-type-multipart-list
	  mew-mime-content-type-binary-list
	  mew-mime-content-type-others-list)
  "Candidate of Content-Type: when CT: is changed in draft buffer."
  )

(defvar mew-mime-content-type-ignore-cdp
  '("Message/.*" "Multipart/.*")
  "Content-Type: list to ignore for insertion Content-Disposition:"
  )

;;
;; Separators
;;

(defvar mew-path-separator "/")
(defvar mew-header-separator "----")
(defvar mew-eoh "^$")
(defvar mew-eoh2 (format "^\\(%s\\|\\)$" (regexp-quote mew-header-separator)))
(defvar mew-lwsp "^[ \t]")
(defvar mew-address-separator ":, \t\n")
(defvar mew-page-delimiter "^\^L")
(defvar mew-keyval "^\\([^ \t:]+:?\\)[ \t]*")
;; "^\\([^ \t:]+:?\\)[ \t]*\\(.*\\)$" faces a stupid error. 
;; "$" doesn't mean end-of-line if the second match is empty, sigh.

;;
;; Limits
;;

(defvar mew-file-max-size 100000
  "*The max size of messages. If the size of a message is greater
than mew-file-max-size, Mew skips MIME analysis.")

(defvar mew-header-max-length 100
  "*If the length of a header exceeds this value, 
it is not arranged nor MIME decoded.
See also mew-header-max-depth.")

(defvar mew-header-max-depth 20
  "*A value to decide loop depth for header field syntax analysis.
It was known as mew-loop-depth.
See also mew-header-max-length.")

;; 
;; Mail Addresses
;;

(defvar mew-mail-domain-list nil
  "*Your e-mail address domain list like 
(\"Mew.org\" \"iijlab.net\").
They are used for mail-domain completion in Draft mode(C-cTAB)")

(defvar mew-mail-domain (or (car mew-mail-domain-list) "")
  "*Your e-mail address domain.")

(defvar mew-mail-address-list nil
  "*The addresses included in this list never appear on the Cc:
field on a draft buffer.")

(cond 
 ((string= mew-mail-domain "")
  (defvar mew-mail-address (user-login-name)))
 (t
  (defvar mew-mail-address (concat (user-login-name) "@" mew-mail-domain)))
 )

;;
;; Window
;;

(defvar mew-window-configuration 
  '((summary (1  0))
    (message (8 31))
    (draft   (1  0)))
  "*Ratio of windows")

(defvar mew-window-use-full nil
  "*Dynamic window configuration when nil. This variable will be obsolated
because mew will support mew-window-percent(e.g 50, 100, etc.).")

;;
;; Appearance
;;

(defvar mew-mode-line-id '("Mew: %12b")
  "*A default value of mode-line-buffer-identification for each Mew mode.")

(defvar mew-multipart-icon-position 'right
  "*Position where multipart icons are displayed. 
If 'left, displayed at the left size of the default toolbar.
If 'right, displayed at the right size of the default toolbar.
Otherwise, not displayed.")


;; Low level
(defvar mew-folder-list-function 'mew-folder-list)


;;
;; Summary mode
;;

(defvar mew-analysis t
  "*MIME syntax analysis and cache when non-nil.
   Skip syntax analysis when nil.")

(defvar mew-summary-cache-use t
  "*If non-nil, insert mew-summary-cache-file into Summary mode buffer 
when the Summary mode is visited and it does not exist in buffer. 
mew-summary-cache-file is automatically saved on inc and scan.")

(defvar mew-cache-size 10
  "*Number of buffer for message cache.")

(defvar mew-summary-cache-file ".mew-cache"
  "*Cache file for Summary mode contents.")

(defvar mew-window-home-buffer "*scratch*"
  "*Buffer name to return if window stack is empty.")

(defvar mew-summary-show-direction 'down
  "*Direction for SPC at end of message. 
'up 'down 'next(current direction) 'stop")

(defvar mew-summary-trace-directory t
  "*If non-nil, trace directory when mew-summary-goto-folder.")

(defvar mew-summary-recenter-p t)

(defvar mew-summary-scan-width nil
  "*If non-nil, used as --width value.")

(defvar mew-save-dir mew-home
  "The default directory to save messages or parts in Summary mode.")

;;
;; Message mode
;;

(defvar mew-end-of-message-string nil
  "*A value inserted on the end of message buffer if non-nil.")

(defvar mew-end-of-part-string nil
  "*A value inserted on the end of message buffer if non-nil.")

(defvar mew-break-pages t)

(defvar mew-field-other-visible t
  "*If *non-nil*, fields which is not found in \"mew-field-spec\"
are displayed after visible fields. Otherwise they are
hidden before visible fields (and after invisible fields).")

;;
;; Draft mode
;;

(defvar mew-reply-to nil
  "*A value inserted into Reply-To: field in Draft mode if non-nil.")

(defvar mew-fcc nil
  "*A value inserted into Fcc: field in Draft mode if non-nil.")

(defvar mew-cc nil
  "*A value inserted into Cc: field in Draft mode if non-nil.")

(defvar mew-dcc nil
  "*A value inserted into Dcc: field in Draft mode if non-nil.")

(defvar mew-from-list nil
  "*A list of From: for circular completion in Draft mode.")

(defvar mew-from (car mew-from-list)
  "*A value inserted into From: field in Draft mode if non-nil.")

(defvar mew-header-alist nil
  "*Alist of key and value pair for header field to be inserted on draft.
'((\"X-fingerprint:\" . \"6B 63 38 88 67 5E 96 8E  CE A4 62 73 3F 11 64 94\")
  (\"X-URL:\" . \"http://www.Mew.org/~kazu/\"))"
  )

(defvar mew-draft-backup-file ".mew-backup")
(defvar mew-draft-syntax-file ".mew-syntax")
(defvar mew-draft-coverpage "CoverPage")

;;
;; IM Config
;;

(defvar mew-config-guess-alist nil
  "*If non-nil, this value is used to guess values of the Config: field.
The syntax is exactly the same as \"mew-refile-guess-alist\".")

(defvar mew-config-insert-when-prepared nil 
  "*If non-nil, the Config: field is inserted according to
\"mew-config-guess-alist\" when the draft is prepared.")

(defvar mew-config-insert-when-composed nil
  "*If non-nil, the Config: field is inserted according to
\"mew-config-guess-alist\" when the message is composed.")

(defconst mew-config-default "default"
  "The default value for IM Config to avoid hard cording.
Do not change this value.")

(defvar mew-config-list nil
  "A list of config cases appeared in .im/Config.
It is used for circular completion in Draft mode.
This variable is automatically set. Do NOT set this by yourself.")

(defvar mew-config-imget mew-config-default
  "A config value specified to imget. Use \"C\" in Summary mode
to change this value with mew-config-list.")

;;
;; Completion
;;

(defvar mew-fields
  '("To:" "Cc:" "Subject:" "Dcc:" "Fcc:" "Bcc:"
    "Reply-To:" "Followup-To:" "From:" "Newsgroups:"
    "Config:")
  "*Completion field list in Draft mode."
  )

(defvar mew-field-completion-switch
  '(("To:"       . mew-complete-address)
    ("Cc:"       . mew-complete-address)
    ("Dcc:"      . mew-complete-address)
    ("Bcc:"      . mew-complete-address)
    ("Reply-To:" . mew-complete-address)
    ("Fcc:"      . mew-complete-folder)
    ("Config:"   . mew-complete-config))
  "*Completion function alist concerned with the key."
  )

(defvar mew-field-circular-completion-switch
  '(("To:"       . mew-circular-complete-domain)
    ("Cc:"       . mew-circular-complete-domain)
    ("Dcc:"      . mew-circular-complete-domain)
    ("Bcc:"      . mew-circular-complete-domain)
    ("Reply-To:" . mew-circular-complete-domain)
    ("From:"     . mew-circular-complete-from)
    ("Config:"   . mew-circular-complete-config))
  "*Circular completion function alist concerned with the key."
  )

;;
;; Citation
;;

(defvar mew-cite-prefix "> "
  "*Prefix of citation.")
(defvar mew-cite-hook nil
  "*If you want to use super-cite, (setq mew-cite-hook 'sc-cite-original).")
(defvar mew-before-cite-hook nil
  "Called in mew-summary-reply-with-citation before citation.")

(defvar mew-cite-prefix-function nil
  "*Function called on citations. A good candidate is 
'mew-cite-prefix-username")

(defvar mew-cite-prefix-confirmp nil
  "*If non-nil, ciattion prefix (such as \"kazu> \") is
confirmed to be used.")

(defvar mew-cite-fields '("From:" "Subject:" "Date:")
  "*The fields that you want to extract as citation label. 
If you change this valuable, you must change mew-cite-format.
The value of the first field becomes the first argment for mew-cite-format.
(e.g. The default first argment is a value of From: field.)
The value of the second field becomes the second argment for mew-cite-format.
....")

(defvar mew-cite-format "From: %s\nSubject: %s\nDate: %s\n\n"
  "*Format for the citation label.")

;;
;; Signature
;; 

(defvar mew-signature-file "~/.signature"
  "*A signature file to be inserted in Draft mode. To support multiple
signature files, use \"c-sig.el\".")

(defvar mew-signature-insert-last nil 
  "*If non-nil, the signature file is inserted in the last of body. 
Otherwise, it is inserted at the currect point. If you created multipart
and mew-signature-as-lastpart is non-nil, this variable is ignored.")

(defvar mew-signature-as-lastpart nil
  "*If non-nil, the signature file is appended as the final part 
when you create multipart.")

(defvar mew-signature-description "My signature"
  "*This variable is used as a description if the signature is appended
as the final part.")

;;
;; Temporary solution to decide To: when reply
;;

(defvar mew-noreplyto-to-list '("From:")
  "*To: field list picked at mew-summary-reply if Reply-To: doesn't exist.")

(defvar mew-noreplyto-cc-list '("To:" "Cc:" "Apparently-To:")
  "Cc: field list picked at mew-summary-reply if Reply-To: doesn't exist.")

(defvar mew-replyto-to-list '("Reply-To:" "From:")
  "*To: field list picked at mew-summary-reply if Reply-To: exists.")

(defvar mew-replyto-cc-list '("To:" "Cc:" "Apparently-To:")
  "*Cc: field list picked at mew-summary-reply if Reply-To: exists.")

;;
;; Encoding and Decoding
;;

(defvar mew-encode-fields
   '(("^To:$" . mew-header-encode-address)
     ("^Cc:$" . mew-header-encode-address)
     ("^From:$" . mew-header-encode-address)
     ("^Resent-To:$" . mew-header-encode-address)
     ("^Resent-Cc:$" . mew-header-encode-address)
     ("^Resent-From:$" . mew-header-encode-address)
     ("^Reply-To:$" . mew-header-encode-address)
     ("^In-Reply-To:$" . mew-header-encode-address)
     (".*" . mew-header-encode-text))
   )

;;
;; Folders
;;

(defvar mew-folder-mode 448 ;; decimal for octal 0700
  "Secure file mode for folders. 448(0700 in octal) is STRONGLY recommended
for privacy reasons.")

(defvar mew-file-mode 384 ;; decimal for octal 0600
  "Secure file mode. 384(0600 in octal) is STRONGLY recommended
for privacy reasons.")

(defvar mew-use-folders-file-p nil
  "*If non-nil, Mew try to load mew-folders-file to make bootup faster.
And C-u Z saves folder list to mew-folders-file.")

(defvar mew-folders-file ".folders"
  "*A file to save folder list. It is used on start-up to
make start-up much faster.")

(defvar mew-folders-ignore '("+from" "=")
  "*Folder list to contain messages whose To: is you and From: is
personal, e.g. a friend. Directories under this folder is ignored
for refile guess.")

(defvar mew-folders-default-folder "+from"
  "*A default folder used by mew-guess-by-default.
mew-guess-by-default append the \"From:\" value to it.
e.g. \"+from/kazu\"")

(defvar mew-mime-compose-folder-delete 'delete
 "*Action to delete MIME draft directory after sending a multipart letter.
Proper value is 'ask', 'delete', and 'retain'.")

(defvar mew-folder-list-skip-pattern
  (if (and (eq system-type 'windows-nt)
	   (not (featurep 'meadow)))
      "^[0-9]+$"
    nil)
  "This value is used to make collecting folder faster.
If nil, Mew skips directories whose link count is 2, that is
they don't have subdirectories. Unfortunately, link count is
not available on some OSes. So, if a regular express pattern is 
set to this value, Mew skips directories whose name are matched
this value. A good example value is \"^[0-9]+$\",
especially for Windows (excluding WNT).")

;;
;; File
;;

(defvar mew-file-append-p nil
  "*If non-nil, a message or a part is appended to the existing file
on mew-summary-save(\"y\"). Otherwise overwrited.")

(defvar mew-temp-file-initial
  (expand-file-name (user-login-name) (or (getenv "TMP")
					  (getenv "TEMP")
					  "/tmp"))
  "*Hint to make a secure directory on the local file system.
On setup phase Mew make a secure directory from this variable
and set mew-temp-file a file name prefex contained the directory name.
The directory must be unreadable from others, otherwise it might become 
a big security hole. Mew never uses 'call-process-region' rather does use
'call-process' creating a temporary file with mew-temp-file by itself.
If 'call-process-region' is used, Emacs creates a temporary file
(probably in /tmp). So bad guys can wiretap the temporary file.")

;;
;; Demo
;;

(defvar mew-demo t
  "*Mew demo is displayed at boot time if non-nil.")

(defvar mew-demo-picture mew-xemacs-p
  "*A picture of cats is displayed if non-nil on XEmacs.")

(defvar mew-icon-mew "Mew.xpm")
(defvar	mew-icon-mew-mono "Mew.xbm")
(defvar	mew-icon-mew-mule-bitmap-image "Mew.img")

(defvar mew-logo nil)

;;
;; Ask?
;; 

(defvar mew-ask-cc t
  "*Prompt user for CC: field if non-nil")

(defvar mew-ask-subject nil
  "*Prompt user for Subject: field when send if non-nil")

(defvar mew-ask-range t
  "*If non-nil, ask for a range for scanning. If false, always
use default.")

(defvar mew-ask-newsgroups nil
  "*If non-nil, prompt user if he/she want to include Newsgroups: field")

(defvar mew-ask-config nil
  "*If non-nil, prompt user if he/she want to include/change Config: field")

(defvar mew-ask-cite-prefix nil
  "*If non-nil, ask citation prefix when cite a message")

(defvar mew-ask-pack t
  "*If non-nil, ask whether or not you really want to pack.")

(defvar mew-ask-send t
  "*If non-nil, ask whether or not you really want to send the message
which you composed without explicit C-cC-m.")

;;
;; Field Magic
;;

(defvar mew-reply-string "Re: ")
(defvar mew-reply-regex "^Re:")
(defvar mew-forward-string "Fw: ")
(defvar mew-forward-regex "^\\(Fw\\|Forward\\):")

;;
;; Use
;;

;;BBDB not fully supported
(defvar mew-use-bbdb nil
  "*Use Insidious Big Brother Database if t")

(defvar mew-use-petname nil)

(defvar mew-use-imap nil
  "If nil, both immv and imrm are called synchronously for speed when
\"x\" is typed. If non-nil, they are called asynchronously to catch 
user's password for IMAP operation.")

;;
;; X face
;;

(defvar mew-x-face-filter (list mew-prog-uncompface "ikon2xbm"))
(defvar mew-x-face-prog "xv")
(defvar mew-x-face-args nil)
(defvar mew-x-face-file "~/.xface"
  "*If non-nil and the file exists, X-Face: fields is inserted.")

;;
;; Auto
;;

(defvar mew-auto-get t
  "If non-nil mew-auto-get, Mew gets messages from your spool 
automatically. 'C-u M-x mew' forces to get messages even if
mew-auto-get is nil.")

(defvar mew-auto-flush-queue nil
  "If non-nil and if there are queued messages in  IM's QueueDir,
they are flushed at \"imget\"(i.e. 'i'). This idea saves money in 
dial up environment.")

;;
;; Pick and Sort
;;

(defvar mew-pick-default-field nil
  "*Default prefix string to be appeared when inputing a pick pattern.
A good example is \"from=\".")

(defvar mew-pick-field-list
  '("head=" "body=" "all="
    "to=" "cc=" "subject=" "dcc=" "fcc=" "bcc=" "date="
    "reply-to=" "followup-to=" "from=" "newsgroups=")
  "*A list of key for pick pattern.")

(defvar mew-sort-default-key "date"
  "*Default sort key when inputing a sort key. 
Its format is key:mode where more is found in \"mew-sort-modes\".")

(defvar mew-sort-key-alist
  '(("date" . "date") ("subject") ("from") ("to") ("newsgroups")
    ("posted" . "date") ("x-sequence" . "num") ("x-mail-count" . "num"))
  "*List of fields for `mew-summary-sort'.
Each element is (FIELD-NAME) or (FIELD-NAME . MODE).
MODE is one of \"date\" (sort by chronological order) or
               \"num\"  (sort by numerical order) or
               \"text\" (sort by alphabetical order) or
               \"ml\"   (modify subject).
(nil means \"text\").")

(defvar mew-sort-modes '("date" "num" "text" "ml"))

;;
;; Range
;;

(defvar mew-input-range-list
  '("update" "all" "first:" "prev:" "next:" "last:")
  "A list used by range completion.")

;; ugh! Hard coding... Setq by yourself if you don't use +draft.
(defvar mew-range-alist 
  '(("^+draft" . "all") ("^+trash" . "all") (".*" . "update"))
  "*Alist of folder and default range pair")

;;
;; PGP Public key fetch
;;

(defvar mew-pgp-keyserver-url-template
  "http://pgp5.ai.mit.edu:11371/pks/lookup?op=get&search=%s"
;;  "http://pgp.nic.ad.jp/cgi-bin/pgpsearchkey.pl?op=get&search=%s"
  )

(defvar mew-x-pgp-key-list
  '("x-pgp-key:" "x-pgp-key-url:" "x-pgp-public-key:" "x-pgp-public-key-url:"
    "x-pgp5-key:" "x-pgp5-key-url:" "x-pgp5-public-key:" "x-pgp5-public-key-url:"
    "x-public-key:"))
   
(defvar mew-pgp-fetch-key-process nil)

;;
;; Misc
;;

(defvar mew-print-function (function lpr-buffer))

;;
;; Marks
;;

(defvar mew-mark-multi  ?@)
(defvar mew-mark-review ?*)
(defvar mew-mark-delete ?D)
(defvar mew-mark-refile ?o) ;; do you like "^"?
(defvar mew-mark-tmp    ?%) ;; temporary use only.

;;
;; Highlight
;;

(defvar mew-use-highlight-cursor-line t
  "*Put underline on the current line in Summary mode.")
(defvar mew-use-highlight-mouse-line mew-xemacs-p
  "*Paint the line where mouse locates in Summary mode.")
(defvar mew-use-highlight-mark t
  "*Paint marked lines in Summary mode.")
(defvar mew-use-highlight-header t
  "*Paint header in Message and Draft mode.")
(defvar mew-use-highlight-body nil
  "*Paint body in Message and Draft mode.")
(defvar mew-use-highlight-url t
  "*Emphasize URL lines in Message mode.")
(defvar mew-use-highlight-url-regex
  "\\b\\(s?https?\\|ftp\\|file\\|gopher\\|news\\|telnet\\|wais\\|mailto\\):\\(//[-a-zA-Z0-9_.]+:[0-9]*\\)?[-a-zA-Z0-9_=?#$@~`%&*+|\\/.,]*[-a-zA-Z0-9_=#$@~`%&*+|\\/]"
  "*Regular expression to find URL.")

(defvar mew-use-highlight-x-face mew-xemacs-p
  "*Iconify X-Face: on XEmacs in Message mode.")

(defvar mew-highlight-body-max-line 200)

(defvar mew-highlight-url-max-size 3000
  "*Use bold font at url maximize size.
If nil, all contents are parsed.")

;;
;; Styles and colors
;;

(defvar mew-highlight-cursor-line-face 'underline
  "*Face to highlight the cursor line in Summary and Virtual mode")
(defvar mew-highlight-mouse-line-function
  (function mode-motion-highlight-line)
  "*A function to highlight the mouse line in Summary and Virtual mode")
(defvar mew-highlight-url-face         'bold
  "*Face to highlight URL in Mesasge mode")
(defvar mew-highlight-url-mouse-face   'highlight
  "*Mouse face to highlight URLin Mesasge mode")

(defvar mew-highlight-header-face-list
  '(mew-highlight-header-face-subject
    mew-highlight-header-face-from
    mew-highlight-header-face-to
    mew-highlight-header-face-key
    mew-highlight-header-face-private
    mew-highlight-header-face-important
    mew-highlight-header-face-marginal
    mew-highlight-header-face-xmew)
  "*A list of face symbol name to highlight header.
Each name should be 'mew-highlight-header-face-xxx'.
Each face will be created from 'mew-highlight-header-sytel-xxx' and
'mew-highlight-header-color-xxx'. These faces can be used in
'mew-field-spec'.")

(defvar mew-highlight-header-style-subject   'bold)
(defvar mew-highlight-header-style-from      'bold)
(defvar mew-highlight-header-style-to        'bold)
(defvar mew-highlight-header-style-key       'bold)
(defvar mew-highlight-header-style-private   'bold)
(defvar mew-highlight-header-style-important 'bold)
(defvar mew-highlight-header-style-marginal  'bold)
(defvar mew-highlight-header-style-xmew      'bold)

(defvar mew-highlight-header-color-subject   "firebrick")
(defvar mew-highlight-header-color-from      "Purple")
(defvar mew-highlight-header-color-to        "DarkOrange2")
(defvar mew-highlight-header-color-key       "ForestGreen")
(defvar mew-highlight-header-color-private
  (if (or mew-xemacs-p (fboundp 'frame-face-alist))
      (face-foreground 'default)
    nil))
(defvar mew-highlight-header-color-important "Blue")
(defvar mew-highlight-header-color-marginal  "gray50")
(defvar mew-highlight-header-color-xmew      "Red")

(defvar mew-highlight-body-face-list
  '(mew-highlight-body-face-quote
    mew-highlight-body-face-comment)
  "*A list of face symbol name to highlight body.
Each name should be 'mew-highlight-body-face-xxx'.
Each face will be created from 'mew-highlight-body-sytel-xxx' and
'mew-highlight-body-color-xxx'. These faces can be used in
'mew-highlight-body-keywords'.")

(defvar mew-highlight-body-style-quote   'default)
(defvar mew-highlight-body-style-comment 'default)

(defvar mew-highlight-body-color-quote   "ForestGreen")
(defvar mew-highlight-body-color-comment "gray50")

;; xxx hard coding...
(defvar mew-highlight-mark-folder-list '("+inbox")
  "*A folder list to highlight marked lines.")

(defvar mew-highlight-mark-face-list
  '(mew-highlight-mark-face-review
    mew-highlight-mark-face-multi
    mew-highlight-mark-face-delete
    mew-highlight-mark-face-refile)
  "*A list of face symbol name to highlight marked lines.
Each name should be 'mew-highlight-mark-face-xxx'.
Each face will be created from 'mew-highlight-mark-sytel-xxx' and
'mew-highlight-mark-color-xxx'. These faces can be used in
'mew-highlight-mark-keywords'.")

(defvar mew-highlight-mark-style-review 'default)
(defvar mew-highlight-mark-style-multi  'default)
(defvar mew-highlight-mark-style-delete 'default)
(defvar mew-highlight-mark-style-refile 'default)

(defvar mew-highlight-mark-color-review "Blue")
(defvar mew-highlight-mark-color-multi  "Purple")
(defvar mew-highlight-mark-color-delete "firebrick")
(defvar mew-highlight-mark-color-refile "ForestGreen")

(defvar mew-field-spec
  '(("^Subject:$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-subject)
    ("^From:$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-from)
    ("^\\(To\\|Apparently-To\\):$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-to)
    ("^Cc:$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-to)
    ("^Newsgroups:$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-to)
    ("^Date:$" t)
    ("^Reply-To:$" t)
    ("^X-Mailer:$" t)
    ("^X-Mew:$" t
     mew-highlight-header-face-important
     mew-highlight-header-face-xmew)
    ("^\\(Received\\|Return-Path\\|Sender\\|Errors-To\\):$" nil)
    ("^\\(Message-Id\\|Posted\\|In-Reply-To\\|References\\|Precedence\\):$" nil)
    ("^\\(Resent\\|Delivered\\|Content\\)-" nil)
    ("^\\(Mime-Version\\|Lines\\):$" nil)
    ("^From$" nil)
    ("^X-" nil
     mew-highlight-header-face-private
     mew-highlight-header-face-marginal))
  "*An alist of field spec for Message mode. Each spec
consists of field-regular-expression, visible-p, face-for-key, 
and face-for-value. Fields whose visible-p is t are displayed in 
Message mode in the defined order. Fields whose visible-p is nil are
hidden in Message mode. Type DEL to see them. Fields not matched
to field-regular-expressions are operated by the value of
\"mew-field-other-visible\". If face-for-key is omitted, 
\"mew-highlight-header-face-key\" is used. If face-for-value is not
present, mew-highlight-header-face-marginal is used.")

(defvar mew-highlight-body-keywords
  '(("^[ \t]*\\(\\w*[A-Za-z0-9'-]*[>|]+.*\\)"
     mew-highlight-body-face-quote)
    ("^#+.*"
     mew-highlight-body-face-comment))
  "*A list of (\"body-key-regex\" face-for-body).
This is used to highlight body.")

(defvar mew-highlight-body-keywords-regex nil)

(defvar mew-highlight-mark-keywords
  (list 
   (cons mew-mark-review 'mew-highlight-mark-face-review)
   (cons mew-mark-multi  'mew-highlight-mark-face-multi)
   (cons mew-mark-delete 'mew-highlight-mark-face-delete)
   (cons mew-mark-refile 'mew-highlight-mark-face-refile)
   )
  "A list of mark-face pair to used in Summary mode where its folder name
is found in 'mew-highlight-mark-folder-list'.")

;;
;; Error
;;

(defvar mew-error-unknown-charset "**UNKNOWN CHARSET**")
(defvar mew-error-illegal-base64 " **BASE64 ENCODING ERROR** ")

;;
;; MIME control
;;


(defvar mew-mime-content-type
  '(("multipart/*" nil         nil     nil            mew-icon-multipart)
    ("audio/basic" "\\.au$"    mew-b64 mew-prog-audio mew-icon-audio)
    ("image/gif"   "\\.gif$"   mew-b64 mew-prog-gif   mew-icon-image)
    ("image/jpeg"  "\\.jpe?g$" mew-b64 mew-prog-jpeg  mew-icon-image)
    ("image/png"   "\\.png$"   mew-b64 mew-prog-png   mew-icon-image)
    ("image/x-xwd" "\\.xwd$"   mew-b64 mew-prog-xwd   mew-icon-image)
    ("image/x-xbm" "\\.xbm$"   mew-b64 mew-prog-xbm   mew-icon-image)
    ("image/x-xpm" "\\.xpm$"   mew-b64 mew-prog-xpm   mew-icon-image)
    ("image/x-bmp" "\\.bmp$"   mew-b64 mew-prog-bmp   mew-icon-image)
    ("image/.*"    "^$"        mew-b64 mew-prog-image mew-icon-image)
    ("video/mpeg"  "\\.mpe?g$" mew-b64 mew-prog-mpeg  mew-icon-video)
    ("message/rfc822" "^[0-9]+$" nil mew-prog-rfc822 mew-icon-message/rfc822)
    ("message/external-body" "\\.ext$" nil mew-prog-external-body
				 mew-icon-message/external-body)
    ("message/delivery-status" "^$" nil mew-prog-delivery-status mew-icon-text)
    ("application/postscript" "\\.ps$" mew-qp mew-prog-postscript
				  mew-icon-application/postscript)
    ("application/pgp-keys" "\\.pka$" nil mew-prog-pgp-keys mew-icon-unknown)
    ("application/octet-stream"
      "\\.tar\\.?g?z?$\\|\\.tgz$\\|\\.gz$\\|\\.Z$\\|\\.lzh$\\|\\.zip$\\|\\.bin$\\|\\.pgp$"
      mew-b64 mew-prog-octet-stream mew-icon-application/octet-stream)
    ("text/html"   "\\.html$"  nil     mew-prog-html  mew-icon-text)
    ;; must be here
    ("text/plain"  ".*"        nil     mew-prog-plain mew-icon-text)
    ("text/.*"     "^$"        nil     mew-prog-text  mew-icon-text)
    (".*" "^$" nil mew-prog-octet-stream mew-icon-unknown))
  "(content-type filename <encoding> <prog> <icon>)"
   )

(cond
 ((memq system-type '(OS/2 emx))
  (require 'mew-os2))
 ((eq system-type 'windows-nt)
  (require 'mew-win32))
 (t
  (require 'mew-unix))
 )

(provide 'mew-vars)

;;; Copyright Notice:

;; Copyright (C) 1996, 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-vars.el ends here

;;; mew-mule0.el --- Environment of non-Mule for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar 20, 1997
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-mule0-version "mew-mule0.el version 0.07")

;; In the context of Mew, 'charset' means MIME charset.
;; 'cs' means the internal representation of Emacs (was known as Mule).

;;
;; User CS definitions
;;

(defvar mew-cs-noconv       nil)
(defvar mew-cs-noconv-eol   nil)
(defvar mew-cs-autoconv     nil)
(defvar mew-cs-7bit         nil)
(defvar mew-cs-7bit-crlf    nil)

(defvar mew-cs-mime-trans   nil)
(defvar mew-cs-rfc822-trans nil)
(defvar mew-cs-draft        nil)
(defvar mew-cs-scan         nil)
(defvar mew-cs-infile       nil)
(defvar mew-cs-outfile      nil)
(defvar mew-cs-virtual      nil)
(defvar mew-cs-pick         nil)

(defvar mew-cs-charset
  '(("us-ascii" nil)
    ("iso-8859-1" nil)))

(defvar mew-mule-character-set nil)

(defvar mew-cs-for-body
  '(("us-ascii"      (0)   nil "7bit")
    ("iso-8859-1"    (0 1) nil "quoted-printable"))
  )

(defvar mew-cs-for-header
  '(("us-ascii"    (0) nil nil)
    ("iso-8859-1"  (1) nil "Q"))
  )

(defvar mew-lc-ascii 0)

;;
;; Charset
;;

(defun mew-charset-guess-region (beg end)
  (interactive "r")
  "Guess minimum character set name."
  (save-excursion
    (goto-char beg)
    (if (re-search-forward "[\200-\377]" end t)
	"iso-8859-1"
      "us-ascii")
    ))

(defvar mew-charset-list nil)

(defun mew-charset-to-cs (charset)
  (if charset
      (nth 1 (mew-assoc-case-equal charset mew-cs-charset 0))
    ))

(defun mew-charset-to-cte (charset)
  (if charset
      ;; used for body
      (nth 3 (mew-assoc-case-equal charset mew-cs-for-body 0))
    ))

;;
;; CS
;;

(defun mew-find-cs-region (start end)
  (let (ret)
    (save-excursion
      (goto-char beg)
      (if (re-search-forward "[\000-\177]" end t)
	  (setq ret (cons 0 ret)))
      (goto-char beg)
      (if (re-search-forward "[\200-\377]" end t)
	  (setq ret (cons 1 ret)))
      )
    ret))

(defconst mew-cs-ascii '(0))

;; to internal
(defun mew-cs-decode-region (beg end cs)
  )

;; to extenal
(defun mew-cs-encode-region (beg end cs)
  )

;; to internal
(defun mew-cs-decode-string (str cs)
  str
  )

;; to external
(defun mew-cs-encode-string (str cs)
  str
  )

;;
;; Process environment
;;

(defun mew-set-process-cs (pro from-pro to-pro)
  )

(defmacro mew-plet (&rest body)
  (`(let ((call-process-hook nil))
      (,@ body))
   ))

(defmacro mew-piolet (input output &rest body)
  (`(let ((call-process-hook nil))
      (,@ body))
   ))

(defmacro mew-flet (&rest body)
  (`(let (jam-zcat-filename-list
	  jka-compr-compression-info-list)
      (,@ body))
   ))

(defmacro mew-frwlet (read write &rest body)
  (`(let (jam-zcat-filename-list
	  jka-compr-compression-info-list)
      (,@ body))
   ))

(provide 'mew-mule0)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-mule0.el ends here

;;; mew-mule3.el --- Environment of Mule version 3 for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar 20, 1997
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-mule3-version "mew-mule3.el version 0.12")

;; In the context of Mew, 'charset' means MIME charset.
;; 'cs' means the internal representation of Emacs (was known as Mule).

;;
;; User CS definitions
;;

;; iso-2022-7bit-ss2 is iso-2022-jp-2

(defvar mew-cs-noconv       'binary)
(defvar mew-cs-noconv-eol   'no-conversion)
(defvar mew-cs-autoconv     'undecided)
(defvar mew-cs-7bit         'iso-2022-7bit-ss2)
(defvar mew-cs-7bit-crlf    'iso-2022-7bit-ss2-dos)
(defvar mew-cs-mime-trans   'iso-2022-7bit-ss2)
(defvar mew-cs-rfc822-trans 'iso-2022-7bit-ss2)
(defvar mew-cs-draft        'iso-2022-7bit-ss2)
(defvar mew-cs-scan         'ctext)
(defvar mew-cs-infile       'undecided) ;; guess in +draft
(defvar mew-cs-outfile      'iso-2022-7bit-ss2)
(defvar mew-cs-virtual      'iso-2022-7bit-ss2-unix) ;; ^M as it is
(defvar mew-cs-pick         'euc-jp)

(defvar mew-cs-charset
  '(("us-ascii"   nil)
    ("iso-8859-1" iso-8859-1)
    ("iso-8859-2" iso-8859-2)
    ("iso-8859-3" iso-8859-3)
    ("iso-8859-4" iso-8859-4)
    ("koi8-r"	  koi8-r)
    ("iso-8859-5" iso-8859-5)
    ("iso-8859-6" iso-8859-6)
    ("iso-8859-7" iso-8859-7)
    ("iso-8859-8" iso-8859-8)
    ("iso-8859-9" iso-8859-9)
    ("iso-2022-jp" iso-2022-jp)
    ("euc-jp"      euc-japan)
    ("shift_jis"   shift_jis)
    ("iso-2022-kr" iso-2022-kr)
    ("euc-kr"      euc-korea)
    ("iso-2022-cn" iso-2022-cn)
    ("cn-gb"       cn-gb-2312)
    ("cn-big5"     chinese-big5)
    ("iso-2022-cn-ext" iso-2022-cn-ext)
    ("iso-2022-jp-2"   iso-2022-7bit-ss2)
    ("iso-2022-int-1"   iso-2022-int-1)
    ))

(defvar mew-cs-for-body
  '(("us-ascii" (ascii) nil "7bit")
    ("iso-8859-1" (ascii latin-iso8859-1)    iso-8859-1 "quoted-printable")
    ("iso-8859-2" (ascii latin-iso8859-2)    iso-8859-2 "quoted-printable")
    ("iso-8859-3" (ascii latin-iso8859-3)    iso-8859-3 "quoted-printable")
    ("iso-8859-4" (ascii latin-iso8859-4)    iso-8859-4 "quoted-printable")
    ("koi8-r"	  (ascii cyrillic-iso8859-5) koi8-r     "quoted-printable")
;    ("iso-8859-5" (ascii cyrillic-iso8859-5) iso-8859-5 "base64")
    ("iso-8859-6" (ascii arabic-iso8859-6)   iso-8859-6 "base64")
    ("iso-8859-7" (ascii greek-iso8859-7)    iso-8859-7 "base64")
    ("iso-8859-8" (ascii hebrew-iso8859-8)   iso-8859-8 "base64")
    ("iso-8859-9" (ascii latin-iso8859-9)    iso-8859-9 "quoted-printable")
    ("iso-2022-jp"
     (ascii latin-jisx0201 japanese-jisx0208 japanese-jisx0208-1978) 
     iso-2022-jp "7bit")
    ("iso-2022-kr" (ascii korean-ksc5601) iso-2022-kr "7bit")
    ("cn-gb" (ascii chinese-gb2312 chinese-sisheng) cn-gb-2312 "base64")
    ("iso-2022-jp-2" 
     (ascii 
      latin-jisx0201 japanese-jisx0208 japanese-jisx0208-1978 japanese-jisx0212
      chinese-gb2312 korean-ksc5601 latin-iso8859-1 greek-iso8859-7)
     iso-2022-7bit-ss2 "7bit")
    ))

(defvar mew-cs-for-header
  '(("us-ascii" (ascii) nil nil)
    ("iso-8859-1" (latin-iso8859-1)    iso-8859-1 "Q")
    ("iso-8859-2" (latin-iso8859-2)    iso-8859-2 "Q")
    ("iso-8859-3" (latin-iso8859-3)    iso-8859-3 "Q")
    ("iso-8859-4" (latin-iso8859-4)    iso-8859-4 "Q")
    ("koi8-r"	  (cyrillic-iso8859-5) koi8-r     "Q")
;    ("iso-8859-5" (cyrillic-iso8859-5) iso-8859-5 "B")
    ("iso-8859-6" (arabic-iso8859-6)   iso-8859-6 "B")
    ("iso-8859-7" (greek-iso8859-7)    iso-8859-7 "B")
    ("iso-8859-8" (hebrew-iso8859-8)   iso-8859-8 "B")
    ("iso-8859-9" (latin-iso8859-9)    iso-8859-9 "Q")
    ("iso-2022-jp" 
     (latin-jisx0201 japanese-jisx0208 japanese-jisx0208-1978) 
     iso-2022-jp "B")
    ("euc-kr" (korean-ksc5601) euc-korea "B")
    ("cn-gb" (chinese-gb2312 chinese-sisheng) cn-gb-2312 "B")
;    ("iso-2022-jp-2" 
;     (latin-jisx0201 japanese-jisx0208 japanese-jisx0208-1978 japanese-jisx0212
;     chinese-gb2312 korean-ksc5601 latin-iso8859-1 greek-iso8859-7)
;     iso-2022-7bit-ss2 "B")
    ))

;;
;;
;;

(defvar mew-lc-ascii 'ascii)
(defvar mew-lc-jp    'japanese-jisx0208)
(defvar mew-lc-kana  'katakana-jisx0201)
(defalias 'mew-make-char 'make-char)

;;
;; Charset
;;

(defun mew-charset-guess-region (beg end)
  "Guess minimum charset name."
  (interactive "r")
  (let* ((tcsl (mew-find-cs-region beg end))
	 (N (length tcsl))
	 (alst mew-cs-for-body)
	 i a acsl aret csl ret)
    (if (member mew-lc-kana tcsl)
	(progn
	  (require 'mew-lang-jp)
	  (mew-zenkaku-katakana-region beg end)
	  (message "Converted Hankaku Kana to Zenkaku Kana!!")
	  (ding)
	  (sit-for 1)
	  (setq tcsl (delete mew-lc-kana tcsl))
	  (if (not (member mew-lc-jp tcsl))
	      (setq tcsl (cons mew-lc-jp tcsl)))
	  (setq N (length tcsl))))
    (while alst
      (setq a (car alst))
      (setq acsl (nth 1 a))
      (setq aret (nth 0 a))
      (setq alst (cdr alst))
      (catch 'loop
	(setq i 0)
	(while (< i N)
	  (if (mew-member (nth i tcsl) acsl)
	      ()
	    (setq aret nil)
	    (setq acsl nil)
	    (throw 'loop nil))
	  (setq i (1+ i)))
	)
      (if (null ret)
	  (setq ret aret)
	(if (and acsl (< (length acsl) (length csl)))
	    (setq ret aret csl acsl))))
    (or ret "unknown")
    ))

(defvar mew-charset-list
  ;; used for body
  (mapcar (lambda (x) (nth 0 x)) mew-cs-for-body))

(defun mew-charset-to-cs (charset)
  (if charset
      (nth 1 (mew-assoc-case-equal charset mew-cs-charset 0))))

(defun mew-charset-to-cte (charset)
  (if charset
      ;; used for body
      (nth 3 (mew-assoc-case-equal charset mew-cs-for-body 0))))

;;
;; CS
;;

(defalias 'mew-find-cs-region 'find-charset-region)

(defconst mew-cs-ascii '(ascii))

;; to internal
(defun mew-cs-decode-region (beg end cs)
  (if cs (decode-coding-region beg end cs)))

;; to extenal
(defun mew-cs-encode-region (beg end cs)
  (if cs (encode-coding-region beg end cs)))

;; to internal
(defun mew-cs-decode-string (str cs)
  (if cs (decode-coding-string str cs)))

;; to external
(defun mew-cs-encode-string (str cs)
  (if cs (encode-coding-string str cs) str))

;;
;; Process environment
;;

(defun mew-set-process-cs (process read write)
  (set-process-coding-system process read write))

(defmacro mew-plet (&rest body)
  `(let ((call-process-hook nil)
	 (coding-system-for-read  'binary)
	 (coding-system-for-write 'binary))
     ,@body))

(defmacro mew-piolet (read write &rest body)
  `(let ((call-process-hook nil)
	 (coding-system-for-read  ,read)
	 (coding-system-for-write ,write))
     ,@body))

(defmacro mew-flet (&rest body)
  `(let ((coding-system-for-read  'binary)
	 (coding-system-for-write 'binary)
	  jam-zcat-filename-list
	  jka-compr-compression-info-list)
     ,@body))

(defmacro mew-frwlet (read write &rest body)
  `(let ((coding-system-for-read  ,read)
	 (coding-system-for-write ,write)
	  jam-zcat-filename-list
	  jka-compr-compression-info-list)
     ,@body))

(provide 'mew-mule3)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-mule3.el ends here

;;; mew-mule2.el --- Environment of Mule version 2 for Mew

;; Author:  Kazu Yamamoto <Kazu@Mew.org>
;; Created: Mar 20, 1997
;; Revised: Oct 25, 1997

;;; Code:

(defconst mew-mule2-version "mew-mule2.el version 0.07")

;; In the context of Mew, 'charset' means MIME charset.
;; 'cs' means the internal representation of Emacs (was known as Mule).

;;
;; User CS definitions
;;

(defvar mew-cs-noconv       '*noconv*)
(defvar mew-cs-noconv-eol   '*noconv*) ;; UNIX, for Win see mew.dot.emacs
(defvar mew-cs-autoconv     '*autoconv*)
(defvar mew-cs-7bit         '*iso-2022-ss2-7*)
(defvar mew-cs-7bit-crlf    '*iso-2022-ss2-7*dos)
(defvar mew-cs-mime-trans   '*iso-2022-ss2-7*)
(defvar mew-cs-rfc822-trans '*iso-2022-ss2-7*) 
(defvar mew-cs-draft        '*iso-2022-ss2-7*)
(defvar mew-cs-scan         '*ctext*)
(defvar mew-cs-infile       '*autoconv*)
(defvar mew-cs-outfile      '*iso-2022-ss2-7*)
(defvar mew-cs-virtual      '*iso-2022-ss2-7*unix) ;; ^M as it is
(defvar mew-cs-pick         '*euc-japan*)

(defvar mew-cs-charset
  '(("us-ascii"   nil)
    ("iso-8859-1" *iso-8859-1*)
    ("iso-8859-2" *iso-8859-2*)
    ("iso-8859-3" *iso-8859-3*)
    ("iso-8859-4" *iso-8859-4*)
    ("koi8-r"	  *koi8*)
    ("iso-8859-5" *iso-8859-5*)
    ("iso-8859-6" *iso-8859-6*)
    ("iso-8859-7" *iso-8859-7*)
    ("iso-8859-8" *iso-8859-8*)
    ("iso-8859-9" *iso-8859-9*)
    ("iso-2022-jp" *iso-2022-jp*)
    ("euc-jp"      *euc-japan*)
    ("shift_jis"   *sjis*)
    ("iso-2022-kr" *iso-2022-kr*)
    ("euc-kr"      *euc-korea*)
;    ("iso-2022-cn" *iso-2022-cn*)
    ("cn-gb"        *euc-china*)
    ("cn-big5"      *big5*)
;    ("iso-2022-cn-ext" iso-2022-cn-ext)
    ("iso-2022-jp-2"   *iso-2022-ss2-7*)
    ("iso-2022-int-1"  *iso-2022-int-1*)
    ))

(defvar mew-cs-for-body
  '(("us-ascii"      (0) nil "7bit")
    ("iso-8859-1"    (0 129) *iso-8859-1* "quoted-printable")
    ("iso-8859-2"    (0 130) *iso-8859-2* "quoted-printable")
    ("iso-8859-3"    (0 131) *iso-8859-3* "quoted-printable")
    ("iso-8859-4"    (0 132) *iso-8859-4* "quoted-printable")
    ("koi8-r"	     (0 140) *koi8*       "quoted-printable")
;    ("iso-8859-5"    (0 140) *iso-8859-5* "base64")
    ("iso-8859-6"    (0 135) *iso-8859-6* "base64")
    ("iso-8859-7"    (0 134) *iso-8859-7* "base64")
    ("iso-8859-8"    (0 136) *iso-8859-8* "base64")
    ("iso-8859-9"    (0 141) *iso-8859-9* "quoted-printable")
    ("iso-2022-jp"   (0 138 146 144) *iso-2022-jp* "7bit")
    ("iso-2022-kr"   (0 147)         *iso-2022-kr* "7bit")
    ("cn-gb"         (0 145 160)     *euc-china* "base64")
    ("iso-2022-jp-2" (0 138 146 144 145 147 148 129 134) 
                     *iso-2022-ss2-7* "7bit")
    ))

(defvar mew-cs-for-header
  '(("us-ascii"    (0) nil nil)
    ("iso-8859-1"  (129) *iso-8859-1* "Q")
    ("iso-8859-2"  (130) *iso-8859-2* "Q")
    ("iso-8859-3"  (131) *iso-8859-3* "Q")
    ("iso-8859-4"  (132) *iso-8859-4* "Q")
    ("koi8-r"	   (140) *koi8*       "Q")
;    ("iso-8859-5"  (140) *iso-8859-5* "B")
    ("iso-8859-6"  (135) *iso-8859-6* "B")
    ("iso-8859-7"  (134) *iso-8859-7* "B")
    ("iso-8859-8"  (136) *iso-8859-8* "B")
    ("iso-8859-9"  (141) *iso-8859-9* "Q")
    ("iso-2022-jp" (138 146 144) *iso-2022-jp* "B")
    ("euc-kr"      (147) *euc-kr*     "B")
    ("cn-gb"       (145 160)     *euc-china* "B")
;    ("iso-2022-jp-2" (138 146 144 145 147 148 129 134) 
;                     *iso-2022-ss2-7* "B")
    ))

;;
;;
;;

(defvar mew-lc-ascii 0)
(defvar mew-lc-kana  137)
(defvar mew-lc-jp    146)
(fset 'mew-make-char (symbol-function 'make-character))

;;
;; Charset
;;

(defun mew-charset-guess-region (beg end)
  "Guess minimum charset name."
  (interactive "r")
  (let* ((tcsl (mew-find-cs-region beg end))
	 (N (length tcsl))
	 (alst mew-cs-for-body)
	 i a acsl aret csl ret)
    (if (member mew-lc-kana tcsl)
	(progn
	  (require 'mew-lang-jp)
	  (mew-zenkaku-katakana-region beg end)
	  (message "Converted Hankaku Kana to Zenkaku Kana!!")
	  (ding)
	  (sit-for 1)
	  (setq tcsl (delete mew-lc-kana tcsl))
	  (if (not (member mew-lc-jp tcsl))
	      (setq tcsl (cons mew-lc-jp tcsl)))
	  (setq N (length tcsl))))
    (while alst
      (setq a (car alst))
      (setq acsl (nth 1 a))
      (setq aret (nth 0 a))
      (setq alst (cdr alst))
      (catch 'loop
	(setq i 0)
	(while (< i N)
	  (if (mew-member (nth i tcsl) acsl)
	      ()
	    (setq aret nil)
	    (setq acsl nil)
	    (throw 'loop nil))
	  (setq i (1+ i)))
	)
      (if (null ret)
	  (setq ret aret)
	(if (and acsl (< (length acsl) (length csl)))
	    (setq ret aret csl acsl))))
    (or ret "unknown")
    ))

(defvar mew-charset-list
  (mapcar (lambda (x) (nth 0 x)) mew-cs-for-body)
  )

(defun mew-charset-to-cs (charset)
  (if charset
      (nth 1 (mew-assoc-case-equal charset mew-cs-charset 0))
    ))

(defun mew-charset-to-cte (charset)
  (if charset
      ;; used for body
      (nth 3 (mew-assoc-case-equal charset mew-cs-for-body 0))))

;;
;; CS
;;

(defun mew-find-cs-region (beg end)
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ((re-official "[^\232-\235\240-\377")
	    (re-private "\\|[\232-\235][^\000-\237")
	    lclist lc mc-flag)
	(while (re-search-forward (concat re-official "]" re-private "]")
				  nil t)
	  (setq lc (preceding-char))
	  (cond
	   ((<= lc ?\177)
	    (setq lc 0)
	    (setq re-official (concat re-official "\000-\177")))
	   ((< lc ?\240)
	    (setq re-official (concat re-official (char-to-string lc))))
	   (t
	    (setq re-private (concat re-private (char-to-string lc))))
	   )
	  (setq lclist (cons lc lclist)))
	lclist))))

(defconst mew-cs-ascii '(0))

;; to internal
(defun mew-cs-decode-region (beg end cs)
  (if cs (code-convert-region beg end cs '*internal*)))

;; to extenal
(defun mew-cs-encode-region (beg end cs)
  (if cs (code-convert-region beg end '*internal* cs)))

;; to internal
(defun mew-cs-decode-string (str cs)
  (if cs (code-convert-string str cs '*internal*)))

;; to external
(defun mew-cs-encode-string (str cs)
  (if cs (code-convert-string str '*internal* cs) str))

;;
;; Process environment
;;

(defun mew-set-process-cs (pro from-pro to-pro)
  (set-process-coding-system pro from-pro to-pro))

(defmacro mew-plet (&rest body)
  (`(let ((call-process-hook nil)
	  (default-process-coding-system
	    (cons '*noconv* '*noconv*)))
      (,@ body))))

(defmacro mew-piolet (input output &rest body)
  (`(let ((call-process-hook nil)
	  (default-process-coding-system
	    (cons (, input) (, output))))
      (,@ body))))

(defmacro mew-flet (&rest body)
  (`(let ((file-coding-system          '*noconv*)
	  (file-coding-system-for-read '*noconv*)
	  jam-zcat-filename-list
	  jka-compr-compression-info-list)
      (,@ body))))

(defmacro mew-frwlet (read write &rest body)
  (`(let ((file-coding-system          (, write))
	  (file-coding-system-for-read (, read))
	  jam-zcat-filename-list
	  jka-compr-compression-info-list)
      (,@ body))))

(provide 'mew-mule2)

;;; Copyright Notice:

;; Copyright (C) 1997, 1998 Mew developing team.
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;; 
;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 2. Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 3. All advertising materials mentioning features or use of this software
;;    must display the following acknowledgement:
;;       This product includes software developed by 
;;       Mew developing team and its contributors.
;; 4. Neither the name of the team nor the names of its contributors
;;    may be used to endorse or promote products derived from this software
;;    without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;; PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;; OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
;; IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; mew-mule2.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'misc-games-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "misc-games/_pkg.el")

(package-provide 'misc-games :version 1.12 :type 'single-file)

;;;***

;;;### (autoloads (all-hail-emacs all-hail-xemacs praise-be-unto-emacs praise-be-unto-xemacs) "advocacy" "misc-games/advocacy.el")

(defvar xemacs-praise-sound-file "im_so_happy" "\
The name of an audio file containing something to play
when praising XEmacs")

(defvar xemacs-praise-message "All Hail XEmacs!\n" "\
What to praise XEmacs with")

(autoload 'praise-be-unto-xemacs "advocacy" "\
All Hail XEmacs!" t nil)

(autoload 'praise-be-unto-emacs "advocacy" nil t nil)

(autoload 'all-hail-xemacs "advocacy" "\
All Hail XEmacs!" t nil)

(autoload 'all-hail-emacs "advocacy" nil t nil)

;;;***

;;;### (autoloads (blackbox) "blackbox" "misc-games/blackbox.el")

(autoload 'blackbox "blackbox" "\
Play blackbox.  Optional prefix argument is the number of balls;
the default is 4.

What is blackbox?

Blackbox is a game of hide and seek played on an 8 by 8 grid (the
Blackbox).  Your opponent (Emacs, in this case) has hidden several
balls (usually 4) within this box.  By shooting rays into the box and
observing where they emerge it is possible to deduce the positions of
the hidden balls.  The fewer rays you use to find the balls, the lower
your score.

Overview of play:

\\<blackbox-mode-map>To play blackbox, type \\[blackbox].  An optional prefix argument
specifies the number of balls to be hidden in the box; the default is
four.

The cursor can be moved around the box with the standard cursor
movement keys.

To shoot a ray, move the cursor to the edge of the box and press SPC.
The result will be determined and the playfield updated.

You may place or remove balls in the box by moving the cursor into the
box and pressing \\[bb-romp].

When you think the configuration of balls you have placed is correct,
press \\[bb-done].  You will be informed whether you are correct or
not, and be given your score.  Your score is the number of letters and
numbers around the outside of the box plus five for each incorrectly
placed ball.  If you placed any balls incorrectly, they will be
indicated with `x', and their actual positions indicated with `o'.

Details:

There are three possible outcomes for each ray you send into the box:

	Detour: the ray is deflected and emerges somewhere other than
		where you sent it in.  On the playfield, detours are
		denoted by matching pairs of numbers -- one where the
		ray went in, and the other where it came out.

	Reflection: the ray is reflected and emerges in the same place
		it was sent in.  On the playfield, reflections are
		denoted by the letter `R'.

	Hit:	the ray strikes a ball directly and is absorbed.  It does
		not emerge from the box.  On the playfield, hits are
		denoted by the letter `H'.

The rules for how balls deflect rays are simple and are best shown by
example.

As a ray approaches a ball it is deflected ninety degrees.  Rays can
be deflected multiple times.  In the diagrams below, the dashes
represent empty box locations and the letter `O' represents a ball.
The entrance and exit points of each ray are marked with numbers as
described under \"Detour\" above.  Note that the entrance and exit
points are always interchangeable.  `*' denotes the path taken by the
ray.

Note carefully the relative positions of the ball and the ninety
degree deflection it causes.

    1                                            
  - * - - - - - -         - - - - - - - -         - - - - - - - -       
  - * - - - - - -         - - - - - - - -         - - - - - - - -       
1 * * - - - - - -         - - - - - - - -         - O - - - - O -       
  - - O - - - - -         - - O - - - - -         - - * * * * - -
  - - - - - - - -         - - - * * * * * 2     3 * * * - - * - -
  - - - - - - - -         - - - * - - - -         - - - O - * - -      
  - - - - - - - -         - - - * - - - -         - - - - * * - -       
  - - - - - - - -         - - - * - - - -         - - - - * - O -       
                                2                         3

As mentioned above, a reflection occurs when a ray emerges from the same point
it was sent in.  This can happen in several ways:

                                                                           
  - - - - - - - -         - - - - - - - -          - - - - - - - -
  - - - - O - - -         - - O - O - - -          - - - - - - - -
R * * * * - - - -         - - - * - - - -          O - - - - - - -
  - - - - O - - -         - - - * - - - -        R - - - - - - - -
  - - - - - - - -         - - - * - - - -          - - - - - - - -
  - - - - - - - -         - - - * - - - -          - - - - - - - -
  - - - - - - - -       R * * * * - - - -          - - - - - - - -
  - - - - - - - -         - - - - O - - -          - - - - - - - -

In the first example, the ray is deflected downwards by the upper
ball, then left by the lower ball, and finally retraces its path to
its point of origin.  The second example is similar.  The third
example is a bit anomalous but can be rationalized by realizing the
ray never gets a chance to get into the box.  Alternatively, the ray
can be thought of as being deflected downwards and immediately
emerging from the box.

A hit occurs when a ray runs straight into a ball:

  - - - - - - - -         - - - - - - - -          - - - - - - - -
  - - - - - - - -         - - - - - - - -          - - - - O - - -
  - - - - - - - -         - - - - O - - -        H * * * * - - - -
  - - - - - - - -       H * * * * O - - -          - - - * - - - -
  - - - - - - - -         - - - - O - - -          - - - O - - - -
H * * * O - - - -         - - - - - - - -          - - - - - - - -
  - - - - - - - -         - - - - - - - -          - - - - - - - -
  - - - - - - - -         - - - - - - - -          - - - - - - - -

Be sure to compare the second example of a hit with the first example of
a reflection." t nil)

;;;***

;;;### (autoloads (conx-load conx conx-region conx-buffer) "conx" "misc-games/conx.el")

(autoload 'conx-buffer "conx" "\
Absorb the text in the current buffer into the tree." t nil)

(autoload 'conx-region "conx" "\
Absorb the text in the current region into the tree." t nil)

(autoload 'conx "conx" "\
Generate some random sentences in the *conx* buffer." t nil)

(autoload 'conx-load "conx" "\
Load in a CONX database written by the \\[conx-save] command.
This clears the database currently in memory." t nil)

;;;***

;;;### (autoloads (decipher-mode decipher) "decipher" "misc-games/decipher.el")

(autoload 'decipher "decipher" "\
Format a buffer of ciphertext for cryptanalysis and enter Decipher mode." t nil)

(autoload 'decipher-mode "decipher" "\
Major mode for decrypting monoalphabetic substitution ciphers.
Lower-case letters enter plaintext.
Upper-case letters are commands.

The buffer is made read-only so that normal Emacs commands cannot
modify it.

The most useful commands are:
\\<decipher-mode-map>
\\[decipher-digram-list]  Display a list of all digrams & their frequency
\\[decipher-frequency-count]  Display the frequency of each ciphertext letter
\\[decipher-adjacency-list]  Show adjacency list for current letter (lists letters appearing next to it)
\\[decipher-make-checkpoint]  Save the current cipher alphabet (checkpoint)
\\[decipher-restore-checkpoint]  Restore a saved cipher alphabet (checkpoint)" t nil)

;;;***

;;;### (autoloads (dissociated-press) "dissociate" "misc-games/dissociate.el")

(autoload 'dissociated-press "dissociate" "\
Dissociate the text of the current buffer.
Output goes in buffer named *Dissociation*,
which is redisplayed each time text is added to it.
Every so often the user must say whether to continue.
If ARG is positive, require ARG chars of continuity.
If ARG is negative, require -ARG words of continuity.
Default is 2." t nil)

;;;***

;;;### (autoloads (doctor) "doctor" "misc-games/doctor.el")

(autoload 'doctor "doctor" "\
Switch to *doctor* buffer and start giving psychotherapy." t nil)

;;;***

;;;### (autoloads (dunnet) "dunnet" "misc-games/dunnet.el")

(autoload 'dunnet "dunnet" "\
Switch to *dungeon* buffer and start game." t nil)

;;;***

;;;### (autoloads (flame) "flame" "misc-games/flame.el")

(autoload 'flame "flame" "\
Generate ARG (default 1) sentences of half-crazed gibberish." t nil)

;;;***

;;;### (autoloads (gomoku) "gomoku" "misc-games/gomoku.el")

(autoload 'gomoku "gomoku" "\
Start a Gomoku game between you and Emacs.
If a game is in progress, this command allow you to resume it.
If optional arguments N and M are given, an N by M board is used.

You and Emacs play in turn by marking a free square. You mark it with X
and Emacs marks it with O. The winner is the first to get five contiguous
marks horizontally, vertically or in diagonal.
You play by moving the cursor over the square you choose and hitting
\\<gomoku-mode-map>\\[gomoku-human-plays].
Use \\[describe-mode] for more info." t nil)

;;;***

;;;### (autoloads (hanoi) "hanoi" "misc-games/hanoi.el")

(autoload 'hanoi "hanoi" "\
Towers of Hanoi diversion.  Argument is number of rings." t nil)

;;;***

;;;### (autoloads (life) "life" "misc-games/life.el")

(autoload 'life "life" "\
Run Conway's Life simulation.
The starting pattern is randomly selected.  Prefix arg (optional first
arg non-nil from a program) is the number of seconds to sleep between
generations (this defaults to 1)." t nil)

;;;***

;;;### (autoloads (mpuz) "mpuz" "misc-games/mpuz.el")

(autoload 'mpuz "mpuz" "\
Multiplication puzzle with GNU Emacs." t nil)

;;;***

;;;### (autoloads (toggle-rot13-mode rot13-other-window) "rot13" "misc-games/rot13.el")

(autoload 'rot13-other-window "rot13" "\
Display current buffer in rot 13 in another window.
To terminate the rot13 display, delete that window." t nil)

(autoload 'toggle-rot13-mode "rot13" "\
Toggle the use of rot 13 encoding for the current window." t nil)

;;;***

(provide 'misc-games-autoloads)

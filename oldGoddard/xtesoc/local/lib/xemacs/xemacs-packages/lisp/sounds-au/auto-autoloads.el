;;; DO NOT MODIFY THIS FILE
(if (featurep 'sounds-au-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "sounds-au/_pkg.el")

(package-provide 'sounds-au :version 1.08 :type 'regular)

;;;***

(provide 'sounds-au-autoloads)

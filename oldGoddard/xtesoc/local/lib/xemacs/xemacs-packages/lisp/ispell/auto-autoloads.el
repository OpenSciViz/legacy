;;; DO NOT MODIFY THIS FILE
(if (featurep 'ispell-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "ispell/_pkg.el")

(package-provide 'ispell :version 1.16 :type 'regular)

;;;***

;;;### (autoloads (ispell-message ispell-minor-mode ispell-complete-word-interior-frag ispell-complete-word ispell-continue ispell-buffer ispell-comments-and-strings ispell-region ispell-change-dictionary ispell-kill-ispell ispell-help ispell-word) "ispell" "ispell/ispell.el")

(defcustom ispell-personal-dictionary nil "*File name of your personal spelling dictionary, or nil.\nIf nil, the default personal dictionary, \"~/.ispell_DICTNAME\" is used,\nwhere DICTNAME is the name of your default dictionary." :type '(choice file (const :tag "default" nil)) :group 'ispell)

(defcustom ispell-local-dictionary-alist nil "*Contains local or customized dictionary definitions.\nSee `ispell-dictionary-alist'." :type '(repeat (list (choice :tag "Dictionary" (string :tag "Dictionary name") (const :tag "default" nil)) (regexp :tag "Case characters") (regexp :tag "Non case characters") (regexp :tag "Other characters") (boolean :tag "Many other characters") (repeat :tag "Ispell command line args" (string :tag "Arg")) (choice :tag "Extended character mode" (const "~tex") (const "~plaintex") (const "~nroff") (const "~list") (const "~latin1") (const "~latin3") (const :tag "default" nil)) (choice :tag "Character set" (const iso-8859-1) (const iso-8859-2)))) :group 'ispell)

(setq ispell-dictionary-alist-1 '((nil "[A-Za-z]" "[^A-Za-z]" "[']" nil ("-B") nil iso-8859-1) ("american" "[A-Za-z]" "[^A-Za-z]" "[']" nil ("-B") nil iso-8859-1) ("british" "[A-Za-z]" "[^A-Za-z]" "[']" nil ("-B" "-d" "british") nil iso-8859-1) ("castellano" "[A-Z�������a-z�������]" "[^A-Z�������a-z�������]" "[---]" nil ("-B" "-d" "castellano") "~tex" iso-8859-1) ("castellano8" "[A-Z�������a-z�������]" "[^A-Z�������a-z�������]" "[---]" nil ("-B" "-d" "castellano") "~latin1" iso-8859-1)))

(setq ispell-dictionary-alist-2 '(("czech" "[A-Za-z�������ݮ���ϫ���������������]" "[^A-Za-z�������ݮ���ϫ���������������]" "" nil ("-B" "-d" "czech") nil iso-8859-2) ("dansk" "[A-Z���a-z���]" "[^A-Z���a-z���]" "[']" nil ("-C") nil iso-8859-1) ("deutsch" "[a-zA-Z\"]" "[^a-zA-Z\"]" "[']" t ("-C") "~tex" iso-8859-1) ("deutsch8" "[a-zA-Z�������]" "[^a-zA-Z�������]" "[']" t ("-C" "-d" "deutsch") "~latin1" iso-8859-1) ("english" "[A-Za-z]" "[^A-Za-z]" "[']" nil ("-B") nil iso-8859-1)))

(setq ispell-dictionary-alist-3 '(("esperanto" "[A-Za-z������������]" "[^A-Za-z������������]" "[-']" t ("-C") "~latin3" iso-8859-1) ("esperanto-tex" "[A-Za-z^\\]" "[^A-Za-z^\\]" "[-'`\"]" t ("-C" "-d" "esperanto") "~tex" iso-8859-1) ("francais7" "[A-Za-z]" "[^A-Za-z]" "[`'^---]" t nil nil iso-8859-1) ("francais" "[A-Za-z���������������������������]" "[^A-Za-z���������������������������]" "[---']" t nil "~list" iso-8859-1)))

(setq ispell-dictionary-alist-4 '(("francais-tex" "[A-Za-z���������������������������\\]" "[^A-Za-z���������������������������\\]" "[---'^`\"]" t nil "~tex" iso-8859-1) ("nederlands" "[A-Za-z�-���-��-��-��-���-���-��-�]" "[^A-Za-z�-���-��-��-��-���-���-��-�]" "[']" t ("-C") nil iso-8859-1) ("nederlands8" "[A-Za-z�-���-��-��-��-���-���-��-�]" "[^A-Za-z�-���-��-��-��-���-���-��-�]" "[']" t ("-C") nil iso-8859-1)))

(setq ispell-dictionary-alist-5 '(("norsk" "[A-Za-z����������������]" "[^A-Za-z����������������]" "[\"]" nil ("-d" "norsk") "~list" iso-8859-1) ("norsk7-tex" "[A-Za-z{}\\'^`]" "[^A-Za-z{}\\'^`]" "[\"]" nil ("-d" "norsk") "~plaintex" iso-8859-1) ("polish" "[A-Za-z������������������]" "[^A-Za-z������������������]" "" nil ("-d" "polish") nil iso-8859-2)))

(setq ispell-dictionary-alist-6 '(("russian" "[�������������������������������������ţ��������������������������]" "[^�������������������������������������ţ��������������������������]" "[']" t ("-C" "-d" "russian") "~latin1" iso-8859-1) ("svenska" "[A-Za-z��������������������]" "[^A-Za-z��������������������]" "[']" nil ("-C") "~list" iso-8859-1)))

(defcustom ispell-dictionary-alist (append ispell-local-dictionary-alist ispell-dictionary-alist-1 ispell-dictionary-alist-2 ispell-dictionary-alist-3 ispell-dictionary-alist-4 ispell-dictionary-alist-5 ispell-dictionary-alist-6) "An alist of dictionaries and their associated parameters.\n\nEach element of this list is also a list:\n\n(DICTIONARY-NAME CASECHARS NOT-CASECHARS OTHERCHARS MANY-OTHERCHARS-P\n        ISPELL-ARGS EXTENDED-CHARACTER-MODE CHARACTER-SET)\n\nDICTIONARY-NAME is a possible string value of variable `ispell-dictionary',\nnil means the default dictionary.\n\nCASECHARS is a regular expression of valid characters that comprise a\nword.\n\nNOT-CASECHARS is the opposite regexp of CASECHARS.\n\nOTHERCHARS is a regexp of characters in the NOT-CASECHARS set but which can be\nused to construct words in some special way.  If OTHERCHARS characters follow\nand precede characters from CASECHARS, they are parsed as part of a word,\notherwise they become word-breaks.  As an example in English, assume the\nregular expression \"[']\" for OTHERCHARS.  Then \"they're\" and\n\"Steven's\" are parsed as single words including the \"'\" character, but\n\"Stevens'\" does not include the quote character as part of the word.\nIf you want OTHERCHARS to be empty, use the empty string.\nHint: regexp syntax requires the hyphen to be declared first here.\n\nMANY-OTHERCHARS-P is non-nil when multiple OTHERCHARS are allowed in a word.\nOtherwise only a single OTHERCHARS character is allowed to be part of any\nsingle word.\n\nISPELL-ARGS is a list of additional arguments passed to the ispell\nsubprocess.\n\nEXTENDED-CHARACTER-MODE should be used when dictionaries are used which\nhave been configured in an Ispell affix file.  (For example, umlauts\ncan be encoded as \\\"a, a\\\", \"a, ...)  Defaults are ~tex and ~nroff\nin English.  This has the same effect as the command-line `-T' option.\nThe buffer Major Mode controls Ispell's parsing in tex or nroff mode,\nbut the dictionary can control the extended character mode.\nBoth defaults can be overruled in a buffer-local fashion. See\n`ispell-parsing-keyword' for details on this.\n\nCHARACTER-SET used for languages with multibyte characters.\n\nNote that the CASECHARS and OTHERCHARS slots of the alist should\ncontain the same character set as casechars and otherchars in the\nLANGUAGE.aff file (e.g., english.aff)." :type '(repeat (list (choice :tag "Dictionary" (string :tag "Dictionary name") (const :tag "default" nil)) (regexp :tag "Case characters") (regexp :tag "Non case characters") (regexp :tag "Other characters") (boolean :tag "Many other characters") (repeat :tag "Ispell command line args" (string :tag "Arg")) (choice :tag "Extended character mode" (const "~tex") (const "~plaintex") (const "~nroff") (const "~list") (const "~latin1") (const "~latin3") (const :tag "default" nil)) (choice :tag "Character set" (const iso-8859-1) (const iso-8859-2)))) :group 'ispell)

(defvar ispell-menu-map nil "\
Key map for ispell menu.")

(defvar ispell-menu-xemacs nil "\
Spelling menu for XEmacs.
If nil when package is loaded, a standard menu will be set,
and added as a submenu of the \"Edit\" menu.")

(defconst ispell-menu-map-needed (and (not ispell-menu-map) (not (string-match "18\\.[0-9]+\\.[0-9]+" emacs-version)) (not (string-match "Lucid\\|XEmacs" emacs-version))))

(if ispell-menu-map-needed (let ((dicts (reverse (cons (cons "default" nil) ispell-dictionary-alist))) name) (setq ispell-menu-map (make-sparse-keymap "Spell")) (while dicts (setq name (car (car dicts)) dicts (cdr dicts)) (if (stringp name) (define-key ispell-menu-map (vector (intern name)) (cons (concat "Select " (capitalize name)) (list 'lambda nil '(interactive) (list 'ispell-change-dictionary name))))))))

(if ispell-menu-map-needed (progn (define-key ispell-menu-map [ispell-change-dictionary] '("Change Dictionary" . ispell-change-dictionary)) (define-key ispell-menu-map [ispell-kill-ispell] '("Kill Process" . ispell-kill-ispell)) (define-key ispell-menu-map [ispell-pdict-save] '("Save Dictionary" lambda nil (interactive) (ispell-pdict-save t t))) (define-key ispell-menu-map [ispell-complete-word] '("Complete Word" . ispell-complete-word)) (define-key ispell-menu-map [ispell-complete-word-interior-frag] '("Complete Word Frag" . ispell-complete-word-interior-frag))))

(if ispell-menu-map-needed (progn (define-key ispell-menu-map [ispell-continue] '("Continue Check" . ispell-continue)) (define-key ispell-menu-map [ispell-word] '("Check Word" . ispell-word)) (define-key ispell-menu-map [ispell-comments-and-strings] '("Check Comments" . ispell-comments-and-strings)) (define-key ispell-menu-map [ispell-region] '("Check Region" . ispell-region)) (define-key ispell-menu-map [ispell-buffer] '("Check Buffer" . ispell-buffer))))

(if ispell-menu-map-needed (progn (define-key ispell-menu-map [ispell-message] '("Check Message" . ispell-message)) (define-key ispell-menu-map [ispell-help] '("Help" lambda nil (interactive) (describe-function 'ispell-help))) (put 'ispell-region 'menu-enable 'mark-active) (fset 'ispell-menu-map (symbol-value 'ispell-menu-map))))

(defvar ispell-skip-region-alist '((ispell-words-keyword forward-line) (ispell-dictionary-keyword forward-line) (ispell-pdict-keyword forward-line) (ispell-parsing-keyword forward-line) ("^---*BEGIN PGP [A-Z ]*--*" . "^---*END PGP [A-Z ]*--*") ("^---* \\(Start of \\)?[Ff]orwarded [Mm]essage" . "^---* End of [Ff]orwarded [Mm]essage") ("\\(/\\|\\(\\(\\w\\|-\\)+[.:@]\\)\\)\\(\\w\\|-\\)*\\([.:/@]+\\(\\w\\|-\\|~\\)+\\)+")) "\
Alist expressing beginning and end of regions not to spell check.
The alist key must be a regular expression.
Valid forms include:
  (KEY) - just skip the key.
  (KEY . REGEXP) - skip to the end of REGEXP.  REGEXP may be string or symbol.
  (KEY REGEXP) - skip to end of REGEXP.  REGEXP must be a string.
  (KEY FUNCTION ARGS) - FUNCTION called with ARGS returns end of region.")

(defvar ispell-tex-skip-alists '((("\\\\addcontentsline" ispell-tex-arg-end 2) ("\\\\add\\(tocontents\\|vspace\\)" ispell-tex-arg-end) ("\\\\\\([aA]lph\\|arabic\\)" ispell-tex-arg-end) ("\\\\bibliographystyle" ispell-tex-arg-end) ("\\\\makebox" ispell-tex-arg-end 0) ("\\\\document\\(class\\|style\\)" . "\\\\begin[ 	\n]*{[ 	\n]*document[ 	\n]*}")) (("\\(figure\\|table\\)\\*?" ispell-tex-arg-end 0) ("list" ispell-tex-arg-end 2) ("program" . "\\\\end[ 	\n]*{[ 	\n]*program[ 	\n]*}") ("verbatim\\*?" . "\\\\end[ 	\n]*{[ 	\n]*verbatim\\*?[ 	\n]*}"))) "\
*Lists of regions to be skipped in TeX mode.
First list is used raw.
Second list has key placed inside \\begin{}.

Delete or add any regions you want to be automatically selected
for skipping in latex mode.")

(define-key esc-map "$" 'ispell-word)

(autoload 'ispell-word "ispell" "\
Check spelling of word under or before the cursor.
If the word is not found in dictionary, display possible corrections
in a window allowing you to choose one.

If optional argument FOLLOWING is non-nil or if `ispell-following-word'
is non-nil when called interactively, then the following word
\(rather than preceding) is checked when the cursor is not over a word.
When the optional argument QUIETLY is non-nil or `ispell-quietly' is non-nil
when called interactively, non-corrective messages are suppressed.

With a prefix argument (or if CONTINUE is non-nil),
resume interrupted spell-checking of a buffer or region.

Word syntax described by `ispell-dictionary-alist' (which see).

This will check or reload the dictionary.  Use \\[ispell-change-dictionary]
or \\[ispell-region] to update the Ispell process." t nil)

(autoload 'ispell-help "ispell" "\
Display a list of the options available when a misspelling is encountered.

Selections are:

DIGIT: Replace the word with a digit offered in the *Choices* buffer.
SPC:   Accept word this time.
`i':   Accept word and insert into private dictionary.
`a':   Accept word for this session.
`A':   Accept word and place in `buffer-local dictionary'.
`r':   Replace word with typed-in value.  Rechecked.
`R':   Replace word with typed-in value. Query-replaced in buffer. Rechecked.
`?':   Show these commands.
`x':   Exit spelling buffer.  Move cursor to original point.
`X':   Exit spelling buffer.  Leaves cursor at the current point, and permits
        the aborted check to be completed later.
`q':   Quit spelling session (Kills ispell process).
`l':   Look up typed-in replacement in alternate dictionary.  Wildcards okay.
`u':   Like `i', but the word is lower-cased first.
`m':   Place typed-in value in personal dictionary, then recheck current word.
`C-l':  redraws screen
`C-r':  recursive edit
`C-z':  suspend emacs or iconify frame" nil nil)

(autoload 'ispell-kill-ispell "ispell" "\
Kill current Ispell process (so that you may start a fresh one).
With NO-ERROR, just return non-nil if there was no Ispell running." t nil)

(autoload 'ispell-change-dictionary "ispell" "\
Change `ispell-dictionary' (q.v.) to DICT and kill old Ispell process.
A new one will be started as soon as necessary.

By just answering RET you can find out what the current dictionary is.

With prefix argument, set the default directory." t nil)

(autoload 'ispell-region "ispell" "\
Interactively check a region for spelling errors.
Return non-nil if spell session completed normally." t nil)

(autoload 'ispell-comments-and-strings "ispell" "\
Check comments and strings in the current buffer for spelling errors." t nil)

(autoload 'ispell-buffer "ispell" "\
Check the current buffer for spelling errors interactively." t nil)

(autoload 'ispell-continue "ispell" "\
Continue a halted spelling session beginning with the current word." t nil)

(autoload 'ispell-complete-word "ispell" "\
Try to complete the word before or under point (see `lookup-words')
If optional INTERIOR-FRAG is non-nil then the word may be a character
sequence inside of a word.

Standard ispell choices are then available." t nil)

(autoload 'ispell-complete-word-interior-frag "ispell" "\
Completes word matching character sequence inside a word." t nil)

(autoload 'ispell-minor-mode "ispell" "\
Toggle Ispell minor mode.
With prefix arg, turn Ispell minor mode on iff arg is positive.
 
In Ispell minor mode, pressing SPC or RET
warns you if the previous word is incorrectly spelled.

All the buffer-local variables and dictionaries are ignored -- to read
them into the running ispell process, type \\[ispell-word] SPC." t nil)

(autoload 'ispell-message "ispell" "\
Check the spelling of a mail message or news post.
Don't check spelling of message headers except the Subject field.
Don't check included messages.

To abort spell checking of a message region and send the message anyway,
use the `x' command.  (Any subsequent regions will be checked.)
The `X' command aborts the message send so that you can edit the buffer.

To spell-check whenever a message is sent, include the appropriate lines
in your .emacs file:
   (add-hook 'message-send-hook 'ispell-message)  ;; GNUS 5
   (add-hook 'news-inews-hook 'ispell-message)    ;; GNUS 4
   (add-hook 'mail-send-hook  'ispell-message)
   (add-hook 'mh-before-send-letter-hook 'ispell-message)

You can bind this to the key C-c i in GNUS or mail by adding to
`news-reply-mode-hook' or `mail-mode-hook' the following lambda expression:
   (function (lambda () (local-set-key \"\\C-ci\" 'ispell-message)))" t nil)

;;;***

(provide 'ispell-autoloads)

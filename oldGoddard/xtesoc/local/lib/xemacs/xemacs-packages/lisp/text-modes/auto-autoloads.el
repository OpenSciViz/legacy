;;; DO NOT MODIFY THIS FILE
(if (featurep 'text-modes-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "text-modes/_pkg.el")

(package-provide 'text-modes :version 1.21 :type 'single-file)

;;;***

;;;### (autoloads (define-auto-insert auto-insert) "autoinsert" "text-modes/autoinsert.el")

(autoload 'auto-insert "autoinsert" "\
Insert default contents into a new file if `auto-insert' is non-nil.
Matches the visited file name against the elements of `auto-insert-alist'." t nil)

(autoload 'define-auto-insert "autoinsert" "\
Associate CONDITION with (additional) ACTION in `auto-insert-alist'.
Optional AFTER means to insert action after all existing actions for CONDITION,
or if CONDITION had no actions, after all other CONDITIONs." nil nil)

;;;***

;;;### (autoloads (flyspell-mode-off global-flyspell-mode flyspell-mode) "flyspell" "text-modes/flyspell.el")

(defcustom flyspell-mode-line-string " Fly" "*String displayed on the modeline when flyspell is active.\nSet this to nil if you don't want a modeline indicator." :group 'flyspell :type 'string)

(defvar flyspell-mode nil)

(make-variable-buffer-local 'flyspell-mode)

(defvar flyspell-mode-map (make-sparse-keymap))

(autoload 'flyspell-mode "flyspell" "\
Minor mode performing on-the-fly spelling checking.
Ispell is automatically spawned on background for each entered words.
The default flyspell behavior is to highlight incorrect words.
With no argument, this command toggles Flyspell mode.
With a prefix argument ARG, turn Flyspell minor mode on iff ARG is positive.
  
Alternatively, you can use Global Flyspell mode to automagically turn on 
Flyspell in buffers whose major mode supports it and whose major mode is one
of `flyspell-global-modes'. For example, put in your ~/.emacs:

 (global-flyspell-mode t)

Bindings:
\\[ispell-word]: correct words (using Ispell).
\\[flyspell-auto-correct-word]: automatically correct word.
\\[flyspell-correct-word] (or mouse-2): popup correct words.

Hooks:
flyspell-mode-hook is run after flyspell is entered.

Remark:
`flyspell-mode' uses `ispell-mode'.  Thus all Ispell options are
valid.  For instance, a personal dictionary can be used by
invoking `ispell-change-dictionary'.

Consider using the `ispell-parser' to check your text.  For instance
consider adding:
\(add-hook 'tex-mode-hook (function (lambda () (setq ispell-parser 'tex))))
in your .emacs file.

flyspell-region checks all words inside a region.

flyspell-buffer checks the whole buffer." t nil)

(autoload 'global-flyspell-mode "flyspell" "\
Toggle Global Flyspell mode.
With prefix ARG, turn Global Flyspell mode on if and only if ARG is positive.
Displays a message saying whether the mode is on or off if MESSAGE is non-nil.
Returns the new status of Global Flyspell mode (non-nil means on).

When Global Flyspell mode is enabled, Flyspell mode is automagically
turned on in a buffer if its major mode is one of `flyspell-global-modes'." t nil)

(autoload 'flyspell-mode-off "flyspell" "\
Turn Flyspell mode off." nil nil)

(if (fboundp 'add-minor-mode) (add-minor-mode 'flyspell-mode flyspell-mode-line-string flyspell-mode-map nil 'flyspell-mode) (or (assoc 'flyspell-mode minor-mode-alist) (setq minor-mode-alist (cons '(flyspell-mode flyspell-mode-line-string) minor-mode-alist))) (or (assoc 'flyspell-mode minor-mode-map-alist) (setq minor-mode-map-alist (cons (cons 'flyspell-mode flyspell-mode-map) minor-mode-map-alist))))

;;;***

;;;### (autoloads (hexlify-buffer hexl-find-file hexl-mode) "hexl" "text-modes/hexl.el")

(autoload 'hexl-mode "hexl" "\
\\<hexl-mode-map>
A major mode for editing binary files in hex dump format.

This function automatically converts a buffer into the hexl format
using the function `hexlify-buffer'.

Each line in the buffer has an \"address\" (displayed in hexadecimal)
representing the offset into the file that the characters on this line
are at and 16 characters from the file (displayed as hexadecimal
values grouped every 16 bits) and as their ASCII values.

If any of the characters (displayed as ASCII characters) are
unprintable (control or meta characters) they will be replaced as
periods.

If `hexl-mode' is invoked with an argument the buffer is assumed to be
in hexl format.

A sample format:

  HEX ADDR: 0001 0203 0405 0607 0809 0a0b 0c0d 0e0f     ASCII-TEXT
  --------  ---- ---- ---- ---- ---- ---- ---- ----  ----------------
  00000000: 5468 6973 2069 7320 6865 786c 2d6d 6f64  This is hexl-mod
  00000010: 652e 2020 4561 6368 206c 696e 6520 7265  e.  Each line re
  00000020: 7072 6573 656e 7473 2031 3620 6279 7465  presents 16 byte
  00000030: 7320 6173 2068 6578 6164 6563 696d 616c  s as hexadecimal
  00000040: 2041 5343 4949 0a61 6e64 2070 7269 6e74   ASCII.and print
  00000050: 6162 6c65 2041 5343 4949 2063 6861 7261  able ASCII chara
  00000060: 6374 6572 732e 2020 416e 7920 636f 6e74  cters.  Any cont
  00000070: 726f 6c20 6f72 206e 6f6e 2d41 5343 4949  rol or non-ASCII
  00000080: 2063 6861 7261 6374 6572 730a 6172 6520   characters.are 
  00000090: 6469 7370 6c61 7965 6420 6173 2070 6572  displayed as per
  000000a0: 696f 6473 2069 6e20 7468 6520 7072 696e  iods in the prin
  000000b0: 7461 626c 6520 6368 6172 6163 7465 7220  table character 
  000000c0: 7265 6769 6f6e 2e0a                      region..

Movement is as simple as movement in a normal emacs text buffer.  Most
cursor movement bindings are the same (ie. Use \\[hexl-backward-char], \\[hexl-forward-char], \\[hexl-next-line], and \\[hexl-previous-line]
to move the cursor left, right, down, and up).

Advanced cursor movement commands (ala \\[hexl-beginning-of-line], \\[hexl-end-of-line], \\[hexl-beginning-of-buffer], and \\[hexl-end-of-buffer]) are
also supported.

There are several ways to change text in hexl mode:

ASCII characters (character between space (0x20) and tilde (0x7E)) are
bound to self-insert so you can simply type the character and it will
insert itself (actually overstrike) into the buffer.

\\[hexl-quoted-insert] followed by another keystroke allows you to insert the key even if
it isn't bound to self-insert.  An octal number can be supplied in place
of another key to insert the octal number's ASCII representation.

\\[hexl-insert-hex-char] will insert a given hexadecimal value (if it is between 0 and 0xFF)
into the buffer at the current point.

\\[hexl-insert-octal-char] will insert a given octal value (if it is between 0 and 0377)
into the buffer at the current point.

\\[hexl-insert-decimal-char] will insert a given decimal value (if it is between 0 and 255)
into the buffer at the current point.

\\[hexl-mode-exit] will exit hexl-mode.

Note: saving the file with any of the usual Emacs commands
will actually convert it back to binary format while saving.

You can use \\[hexl-find-file] to visit a file in hexl-mode.

\\[describe-bindings] for advanced commands." t nil)

(autoload 'hexl-find-file "hexl" "\
Edit file FILENAME in hexl-mode.
Switch to a buffer visiting file FILENAME, creating one in none exists." t nil)

(autoload 'hexlify-buffer "hexl" "\
Convert a binary buffer to hexl format.
This discards the buffer's undo information." t nil)

;;;***

;;;### (autoloads (htmlize-buffer) "htmlize" "text-modes/htmlize.el")

(autoload 'htmlize-buffer "htmlize" "\
HTML-ize BUFFER." t nil)

;;;***

;;;### (autoloads (image-decode-xpm image-decode-png image-decode-tiff image-decode-gif image-decode-jpeg image-mode) "image-mode" "text-modes/image-mode.el")

(autoload 'image-mode "image-mode" "\
\\{image-mode-map}" t nil)

(autoload 'image-decode-jpeg "image-mode" "\
Decode JPEG image between START and END." nil nil)

(autoload 'image-decode-gif "image-mode" "\
Decode GIF image between START and END." nil nil)

(autoload 'image-decode-tiff "image-mode" "\
Decode TIFF image between START and END." nil nil)

(autoload 'image-decode-png "image-mode" "\
Decode PNG image between START and END." nil nil)

(autoload 'image-decode-xpm "image-mode" "\
Decode XPM image between START and END." nil nil)

;;;***

;;;### (autoloads (iso-accents-mode) "iso-acc" "text-modes/iso-acc.el")

(autoload 'iso-accents-mode "iso-acc" "\
Toggle ISO Accents mode, in which accents modify the following letter.
This permits easy insertion of accented characters according to ISO-8859-1.
When Iso-accents mode is enabled, accent character keys
\(`, ', \", ^, / and ~) do not self-insert; instead, they modify the following
letter key so that it inserts an ISO accented letter.

You can customize ISO Accents mode to a particular language
with the command `iso-accents-customize'.

Special combinations: ~c gives a c with cedilla,
~d gives an Icelandic eth (d with dash).
~t gives an Icelandic thorn.
\"s gives German sharp s.
/a gives a with ring.
/e gives an a-e ligature.
~< and ~> give guillemots.
~! gives an inverted exclamation mark.
~? gives an inverted question mark.

With an argument, a positive argument enables ISO Accents mode, 
and a negative argument disables it." t nil)

;;;***

;;;### (autoloads (electric-nroff-mode nroff-mode) "nroff-mode" "text-modes/nroff-mode.el")

(autoload 'nroff-mode "nroff-mode" "\
Major mode for editing text intended for nroff to format.
\\{nroff-mode-map}
Turning on Nroff mode runs `text-mode-hook', then `nroff-mode-hook'.
Also, try `nroff-electric-mode', for automatically inserting
closing requests for requests that are used in matched pairs." t nil)

(autoload 'electric-nroff-mode "nroff-mode" "\
Toggle `nroff-electric-newline' minor mode.
`nroff-electric-newline' forces Emacs to check for an nroff request at the
beginning of the line, and insert the matching closing request if necessary.
This command toggles that mode (off->on, on->off), with an argument,
turns it on iff arg is positive, otherwise off." t nil)

(defvar nroff-electric-mode nil "\
Non-nil if in electric-nroff minor mode.")

(add-minor-mode 'nroff-electric-mode " Electric" nil nil 'electric-nroff-mode)

;;;***

;;;### (autoloads (scribe-mode) "scribe" "text-modes/scribe.el")

(autoload 'scribe-mode "scribe" "\
Major mode for editing files of Scribe (a text formatter) source.
Scribe-mode is similar text-mode, with a few extra commands added.
\\{scribe-mode-map}

Interesting variables:

scribe-fancy-paragraphs
  Non-nil makes Scribe mode use a different style of paragraph separation.

scribe-electric-quote
  Non-nil makes insert of double quote use `` or '' depending on context.

scribe-electric-parenthesis
  Non-nil makes an open-parenthesis char (one of `([<{')
  automatically insert its close if typed after an @Command form." t nil)

;;;***

;;;### (autoloads (tabify untabify) "tabify" "text-modes/tabify.el")

(autoload 'untabify "tabify" "\
Convert all tabs in region to multiple spaces, preserving columns.
Called non-interactively, the region is specified by arguments
START and END, rather than by the position of point and mark.
The variable `tab-width' controls the spacing of tab stops." t nil)

(autoload 'tabify "tabify" "\
Convert multiple spaces in region to tabs when possible.
A group of spaces is partially replaced by tabs
when this can be done without changing the column they end at.
Called non-interactively, the region is specified by arguments
START and END, rather than by the position of point and mark.
The variable `tab-width' controls the spacing of tab stops." t nil)

;;;***

;;;### (autoloads (ununderline-and-unoverstrike-region overstrike-region unoverstrike-region ununderline-region underline-region) "underline" "text-modes/underline.el")

(autoload 'underline-region "underline" "\
Underline all nonblank characters in the region.
Works by overstriking underscores.
Called from program, takes two arguments START and END
which specify the range to operate on." t nil)

(autoload 'ununderline-region "underline" "\
Remove all underlining (overstruck underscores) in the region.
Called from program, takes two arguments START and END
which specify the range to operate on." t nil)

(autoload 'unoverstrike-region "underline" "\
Remove all overstriking (character-backspace-character) in the region.
Called from program, takes two arguments START and END which specify the
range to operate on." t nil)

(autoload 'overstrike-region "underline" "\
Overstrike (character-backspace-character) all nonblank characters in
the region. Called from program, takes two arguments START and END which
specify the range to operate on." t nil)

(autoload 'ununderline-and-unoverstrike-region "underline" "\
Remove underlining and overstriking in the region.  Called from a program,
takes two arguments START and END which specify the range to operate on." t nil)

;;;***

;;;### (autoloads (whitespace-mode) "whitespace-mode" "text-modes/whitespace-mode.el")

(autoload 'whitespace-mode "whitespace-mode" "\
Toggle whitespace mode.
With arg, turn whitespace mode on iff arg is positive.
In whitespace mode the different whitespaces (tab and blank)
are highlighted with different faces. The faces are:
`whitespace-blank-face' and `whitespace-tab-face'.
Use the command `whitespace-show-faces' to show their values." t nil)

;;;***

;;;### (autoloads (winmgr-mode) "winmgr-mode" "text-modes/winmgr-mode.el")

(autoload 'winmgr-mode "winmgr-mode" "\
Major mode for editing winmgr config files." t nil)

;;;***

;;;### (autoloads (wordstar-mode) "ws-mode" "text-modes/ws-mode.el")

(autoload 'wordstar-mode "ws-mode" "\
Major mode with WordStar-like key bindings.

BUGS:
 - Help menus with WordStar commands (C-j just calls help-for-help)
   are not implemented
 - Options for search and replace
 - Show markers (C-k h) is somewhat strange
 - Search and replace (C-q a) is only available in forward direction

No key bindings beginning with ESC are installed, they will work
Emacs-like.

The key bindings are:

  C-a		backward-word
  C-b		fill-paragraph
  C-c		scroll-up-line
  C-d		forward-char
  C-e		previous-line
  C-f		forward-word
  C-g		delete-char
  C-h		backward-char
  C-i		indent-for-tab-command
  C-j		help-for-help
  C-k		ordstar-C-k-map
  C-l		ws-repeat-search
  C-n		open-line
  C-p		quoted-insert
  C-r		scroll-down-line
  C-s		backward-char
  C-t		kill-word
  C-u		keyboard-quit
  C-v		overwrite-mode
  C-w		scroll-down
  C-x		next-line
  C-y		kill-complete-line
  C-z		scroll-up

  C-k 0		ws-set-marker-0
  C-k 1		ws-set-marker-1
  C-k 2		ws-set-marker-2
  C-k 3		ws-set-marker-3
  C-k 4		ws-set-marker-4
  C-k 5		ws-set-marker-5
  C-k 6		ws-set-marker-6
  C-k 7		ws-set-marker-7
  C-k 8		ws-set-marker-8
  C-k 9		ws-set-marker-9
  C-k b		ws-begin-block
  C-k c		ws-copy-block
  C-k d		save-buffers-kill-emacs
  C-k f		find-file
  C-k h		ws-show-markers
  C-k i		ws-indent-block
  C-k k		ws-end-block
  C-k p		ws-print-block
  C-k q		kill-emacs
  C-k r		insert-file
  C-k s		save-some-buffers
  C-k t		ws-mark-word
  C-k u		ws-exdent-block
  C-k C-u	keyboard-quit
  C-k v		ws-move-block
  C-k w		ws-write-block
  C-k x		kill-emacs
  C-k y		ws-delete-block

  C-o c		wordstar-center-line
  C-o b		switch-to-buffer
  C-o j		justify-current-line
  C-o k		kill-buffer
  C-o l		list-buffers
  C-o m		auto-fill-mode
  C-o r		set-fill-column
  C-o C-u	keyboard-quit
  C-o wd	delete-other-windows
  C-o wh	split-window-horizontally
  C-o wo	other-window
  C-o wv	split-window-vertically

  C-q 0		ws-find-marker-0
  C-q 1		ws-find-marker-1
  C-q 2		ws-find-marker-2
  C-q 3		ws-find-marker-3
  C-q 4		ws-find-marker-4
  C-q 5		ws-find-marker-5
  C-q 6		ws-find-marker-6
  C-q 7		ws-find-marker-7
  C-q 8		ws-find-marker-8
  C-q 9		ws-find-marker-9
  C-q a		ws-query-replace
  C-q b		ws-to-block-begin
  C-q c		end-of-buffer
  C-q d		end-of-line
  C-q f		ws-search
  C-q k		ws-to-block-end
  C-q l		ws-undo
  C-q p		ws-last-cursorp
  C-q r		beginning-of-buffer
  C-q C-u	keyboard-quit
  C-q w		ws-last-error
  C-q y		ws-kill-eol
  C-q DEL	ws-kill-bol
" t nil)

;;;***

;;;### (autoloads (xpm-mode) "xpm-mode" "text-modes/xpm-mode.el")

(autoload 'xpm-mode "xpm-mode" "\
Treat the current buffer as an xpm file and colorize it.

  Shift-button-1 lets you paint by dragging the mouse.  Shift-button-1 on a
color definition line will change the current painting color to that line's
value.

  Characters inserted from the keyboard will NOT be colored properly yet.
Use the mouse, or do xpm-init (\\[xpm-init]) after making changes.

\\[xpm-add-color] Add a new color, prompting for character and value
\\[xpm-show-image] show the current image at the top of the buffer
\\[xpm-parse-color] parse the current line's color definition and add
   it to the color table.  Provided as a means of changing colors.
XPM minor mode bindings:
\\{xpm-mode-map}" t nil)

;;;***

;;;### (autoloads (xrdb-mode) "xrdb-mode" "text-modes/xrdb-mode.el")

(autoload 'xrdb-mode "xrdb-mode" "\
Major mode for editing xrdb config files" t nil)

;;;***

(provide 'text-modes-autoloads)

;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'filladapt '("filladapt"))
(custom-add-loads 'xrdb '("xrdb-mode"))
(custom-add-loads 'tools '("autoinsert" "folding"))
(custom-add-loads 'hypermedia '("htmlize"))
(custom-add-loads 'outlines '("whitespace-mode"))
(custom-add-loads 'crontab '("crontab"))
(custom-add-loads 'scribe '("scribe"))
(custom-add-loads 'auto-insert '("autoinsert"))
(custom-add-loads 'htmlize '("htmlize"))
(custom-add-loads 'flyspell '("flyspell"))
(custom-add-loads 'languages '("crontab" "winmgr-mode" "xrdb-mode"))
(custom-add-loads 'fill '("filladapt"))
(custom-add-loads 'folding '("folding"))
(custom-add-loads 'processes '("flyspell"))
(custom-add-loads 'wp '("scribe"))
(custom-add-loads 'whitespace '("whitespace-mode"))
(custom-add-loads 'matching '("whitespace-mode"))
(custom-add-loads 'winmgr '("winmgr-mode"))

;;; custom-load.el ends here

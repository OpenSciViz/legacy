;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 19:03:03 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/oa/text-modes/autoinsert.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`autoinsert.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


(custom-declare-group 'auto-insert nil "Automatic mode-dependent insertion of text into new files." :group 'tools)
(custom-declare-variable 'auto-insert ''not-modified "*Controls automatic insertion into newly found empty files:\n	nil	do nothing\n	t	insert if possible\n	other	insert if possible, but mark as unmodified.\nInsertion is possible when something appropriate is found in\n`auto-insert-alist'.  When the insertion is marked as unmodified, you can\nsave it with  \\[write-file] RET.\nThis variable is used when `auto-insert' is called as a function, e.g.\nwhen you do (add-hook 'find-file-hooks 'auto-insert).\nWith \\[auto-insert], this is always treated as if it were `t'." :type '(radio (const :tag "Do nothing" nil) (const :tag "Insert if possible" t) (sexp :format "%t\n" :tag "Insert if possible, but mark as unmodified" other)) :require 'autoinsert :group 'auto-insert)
(custom-declare-variable 'auto-insert-query ''function "*If non-`nil', ask user before auto-inserting.\nWhen this is `function', only ask when called non-interactively." :type '(choice (const :tag "Don't ask" nil) (const :tag "Ask when non-interactive" function) (sexp :format "%t\n" :tag "Ask" other)) :group 'auto-insert)
(custom-declare-variable 'auto-insert-prompt '"Perform %s auto-insertion? " "*Prompt to use when querying whether to auto-insert.\nIf this contains a %s, that will be replaced by the matching rule." :type 'string :group 'auto-insert)
#@686 A list specifying text to insert by default into a new file.
Elements look like (CONDITION . ACTION) or ((CONDITION . DESCRIPTION) . ACTION).
CONDITION maybe a regexp that must match the new file's name, or it may be
a symbol that must match the major mode for this element to apply.
Only the first matching element is effective.
Optional DESCRIPTION is a string for filling `auto-insert-prompt'.
ACTION may be a skeleton to insert (see `skeleton-insert'), an absolute
file-name or one relative to `auto-insert-directory' or a function to call.
ACTION may also be a vector containing several successive single actions as
described above, e.g. ["header.insert" date-and-author-update].
(defvar auto-insert-alist '((("\\.\\([Hh]\\|hh\\|hpp\\)\\'" . "C / C++ header") (upcase (concat (file-name-nondirectory (substring buffer-file-name 0 (match-beginning 0))) "_" (substring buffer-file-name (1+ (match-beginning 0))))) "#ifndef " str n "#define " str "\n\n" _ "\n\n#endif") (("\\.\\([Cc]\\|cc\\|cpp\\)\\'" . "C / C++ program") nil "#include \"" (and (fboundp 'c-companion-file) (file-name-nondirectory (c-companion-file 'name))) & ?\" | -10) ("[Mm]akefile\\'" . "makefile.inc") (html-mode lambda nil (sgml-tag "html")) (plain-tex-mode . "tex-insert.tex") (bibtex-mode . "tex-insert.tex") (latex-mode "options, RET: " "\\documentstyle[" str & ?\] | -1 ?\{ (read-string "class: ") "}\n" ("package, %s: " "\\usepackage[" (read-string "options, RET: ") & ?\] | -1 ?\{ str "}\n") _ "\n\\begin{document}\n" _ "\n\\end{document}") (("/bin/.*[^/]\\'" . "Shell-Script mode magic number") lambda nil (if (eq major-mode default-major-mode) (sh-mode))) (ada-mode . ada-header) (("\\.el\\'" . "Emacs Lisp header") "Short description: " ";;; " (file-name-nondirectory (buffer-file-name)) " --- " str "\n\n;; Copyright (C) " (substring (current-time-string) -4) " by " (getenv "ORGANIZATION") | "Free Software Foundation, Inc." "\n\n;; Author: " (user-full-name) '(if (search-backward "&" (save-excursion (beginning-of-line 1) (point)) t) (replace-match (capitalize (user-login-name)) t t)) '(end-of-line 1) " <" (user-login-name) ?\@ (system-name) ">\n;; Keywords: " '(require 'finder) '(setq v1 (mapcar (lambda (x) (list (symbol-name (car x)))) finder-known-keywords) v2 (mapconcat (lambda (x) (format "%10.0s:  %s" (car x) (cdr x))) finder-known-keywords "\n")) ((let ((minibuffer-help-form v2)) (completing-read "Keyword, C-h: " v1 nil t)) str ", ") & -2 "\n\n;; This file is part of XEmacs.\n\n;; XEmacs is free software; you can redistribute it and/or modify it\n;; under the terms of the GNU General Public License as published by\n;; the Free Software Foundation; either version 2, or (at your option)\n;; any later version.\n\n;; XEmacs is distributed in the hope that it will be useful, but\n;; WITHOUT ANY WARRANTY; without even the implied warranty of\n;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n;; General Public License for more details.\n\n;; You should have received a copy of the GNU General Public License\n;; along with XEmacs; see the file COPYING.  If not, write to the Free\n;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA\n;; 02111-1307, USA.\n\n;;; Synched up with: Not in FSF\n\n;;; Commentary:\n\n;; " _ "\n\n;;; Code:\n\n\n(provide '" (file-name-sans-extension (file-name-nondirectory (buffer-file-name))) ")\n\n;;; " (file-name-nondirectory (buffer-file-name)) " ends here\n")) (#$ . 2050))
#@54 *Directory from which auto-inserted files are taken.
(defvar auto-insert-directory "~/insert/" (#$ . -5507))
#@145 Insert default contents into a new file if `auto-insert' is non-nil.
Matches the visited file name against the elements of `auto-insert-alist'.
(defalias 'auto-insert #[nil "?¹ 	Âa¬\n¹ o¹ m¹ Ä	Êb	«Á	@@:¬ªA@9«aªÌ\"«	@AÄ	ªE	A	¬?«º;«ÎP!ªÐ«««Òa«	Âa¬ÓÔ\"!ªÐ«Ö×Ø!«ªÙ!\"Ú ­	Âa?­Û\nÐa!-" [buffer-read-only this-command auto-insert auto-insert-alist nil action desc cond case-fold-search alist 1 major-mode string-match buffer-file-name file-readable-p auto-insert-directory t auto-insert-query function y-or-n-p format auto-insert-prompt mapcar #[(action) ";«Á\nP!­Ã!Ä" [action file-readable-p auto-insert-directory insert-file-contents ((byte-code "Àp!	:«	@Âa¬Ã	!	 " [switch-to-buffer action lambda skeleton-insert] 2))] 3] vectorp vector buffer-modified-p set-buffer-modified-p] 6 (#$ . 5623) nil])
#@210 Associate CONDITION with (additional) ACTION in `auto-insert-alist'.
Optional AFTER means to insert action after all existing actions for CONDITION,
or if CONDITION had no actions, after all other CONDITIONs.
(defalias 'define-auto-insert #[(key action &optional after) "À	\n\"«»ÄA!«Å­AÄ!«ªÈ!?­A#ª«ÈA\"ªÈA\"¡ª«\n	BC¤ª	B\nB)" [assoc key auto-insert-alist elt vectorp vconcat after action vector] 6 (#$ . 6525)])
(provide 'autoinsert)

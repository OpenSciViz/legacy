;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'scheme '("scheme" "xscheme"))
(custom-add-loads 'languages '("xscheme"))

;;; custom-load.el ends here

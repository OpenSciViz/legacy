;;; DO NOT MODIFY THIS FILE
(if (featurep 'scheme-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "scheme/_pkg.el")

(package-provide 'scheme :version 1.09 :type 'regular)

;;;***

;;;### (autoloads nil "cmuscheme" "scheme/cmuscheme.el")

;;;***

;;;### (autoloads (run-scsh) "cmuscheme48" "scheme/cmuscheme48.el")

(defvar scsh-program-name "scsh" "\
The program name and arguments to be invoked for the `run-scsh'
command.")

(autoload 'run-scsh "cmuscheme48" "\
Run inferiour `scsh'.  See the documentation to `run-scheme' after
`cmuscheme' has been `require'd." t nil)

;;;***

;;;### (autoloads (dsssl-mode scheme-mode) "scheme" "scheme/scheme.el")

(autoload 'scheme-mode "scheme" "\
Major mode for editing Scheme code.
Editing commands are similar to those of lisp-mode.

In addition, if an inferior Scheme process is running, some additional
commands will be defined, for evaluating expressions and controlling
the interpreter, and the state of the process will be displayed in the
modeline of all Scheme buffers.  The names of commands that interact
with the Scheme process start with \"xscheme-\".  For more information
see the documentation for xscheme-interaction-mode.

Commands:
Delete converts tabs to spaces as it moves back.
Blank lines separate paragraphs.  Semicolons start comments.
\\{scheme-mode-map}
Entry to this mode calls the value of scheme-mode-hook
if that value is non-nil." t nil)

(autoload 'dsssl-mode "scheme" "\
Major mode for editing DSSSL code.
Editing commands are similar to those of lisp-mode.

Commands:
Delete converts tabs to spaces as it moves back.
Blank lines separate paragraphs.  Semicolons start comments.
\\{scheme-mode-map}
Entry to this mode calls the value of dsssl-mode-hook
if that value is non-nil and inserts the value of
`dsssl-sgml-declaration' if that variable's value is a string." t nil)

;;;***

;;;### (autoloads (run-scheme) "xscheme" "scheme/xscheme.el")

(autoload 'run-scheme "xscheme" "\
Run an inferior Scheme process.
Output goes to the buffer `*scheme*'.
With argument, asks for a command line." t nil)

;;;***

(provide 'scheme-autoloads)

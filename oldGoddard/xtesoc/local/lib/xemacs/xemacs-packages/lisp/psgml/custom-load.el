;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'psgml-dtd '("psgml"))
(custom-add-loads 'html '("psgml-html"))
(custom-add-loads 'psgml '("psgml-html" "psgml"))
(custom-add-loads 'psgml-html '("psgml-html"))
(custom-add-loads 'sgml '("psgml-html" "psgml"))
(custom-add-loads 'languages '("psgml"))
(custom-add-loads 'psgml-insert '("psgml"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'calc-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "calc/_pkg.el")

(package-provide 'calc :version 1.11 :type 'regular)

;;;***

;;;### (autoloads (calc-extensions) "calc-ext" "calc/calc-ext.el")

(autoload 'calc-extensions "calc-ext" "\
This function is part of the autoload linkage for parts of Calc." nil nil)

;;;***

;;;### (autoloads (calc-tutorial) "calc-misc" "calc/calc-misc.el")

(autoload 'calc-tutorial "calc-misc" "\
Run the Emacs Info system on the Calculator Tutorial." t nil)

;;;***

;;;### (autoloads (defmath calc-embedded-activate calc-embedded calc-grab-rectangle calc-grab-region full-calc-keypad calc-keypad calc-eval quick-calc full-calc calc calc-dispatch) "calc" "calc/calc.el")

(defvar calc-info-filename "calc.info" "\
*File name in which to look for the Calculator's Info documentation.")

(defvar calc-settings-file "~/.emacs" "\
*File in which to record permanent settings; default is \"~/.emacs\".")

(defvar calc-autoload-directory nil "\
Name of directory from which additional \".elc\" files for Calc should be
loaded.  Should include a trailing \"/\".
If nil, use original installation directory.
This can safely be nil as long as the Calc files are on the load-path.")

(defvar calc-gnuplot-name "gnuplot" "\
*Name of GNUPLOT program, for calc-graph features.")

(defvar calc-gnuplot-plot-command nil "\
*Name of command for displaying GNUPLOT output; %s = file name to print.")

(defvar calc-gnuplot-print-command "lp %s" "\
*Name of command for printing GNUPLOT output; %s = file name to print.")
(global-set-key "\e#" 'calc-dispatch)

(autoload 'calc-dispatch "calc" "\
Invoke the GNU Emacs Calculator.  See calc-dispatch-help for details." t nil)

(autoload 'calc "calc" "\
The Emacs Calculator.  Full documentation is listed under \"calc-mode\"." t nil)

(autoload 'full-calc "calc" "\
Invoke the Calculator and give it a full-sized window." t nil)

(autoload 'quick-calc "calc" "\
Do a quick calculation in the minibuffer without invoking full Calculator." t nil)

(autoload 'calc-eval "calc" "\
Do a quick calculation and return the result as a string.
Return value will either be the formatted result in string form,
or a list containing a character position and an error message in string form." nil nil)

(autoload 'calc-keypad "calc" "\
Invoke the Calculator in \"visual keypad\" mode.
This is most useful in the X window system.
In this mode, click on the Calc \"buttons\" using the left mouse button.
Or, position the cursor manually and do M-x calc-keypad-press." t nil)

(autoload 'full-calc-keypad "calc" "\
Invoke the Calculator in full-screen \"visual keypad\" mode.
See calc-keypad for details." t nil)

(autoload 'calc-grab-region "calc" "\
Parse the region as a vector of numbers and push it on the Calculator stack." t nil)

(autoload 'calc-grab-rectangle "calc" "\
Parse a rectangle as a matrix of numbers and push it on the Calculator stack." t nil)

(autoload 'calc-embedded "calc" "\
Start Calc Embedded mode on the formula surrounding point." t nil)

(autoload 'calc-embedded-activate "calc" "\
Scan the current editing buffer for all embedded := and => formulas.
Also looks for the equivalent TeX words, \\gets and \\evalto." t nil)

(autoload 'defmath "calc" nil nil 'macro)

;;;***

;;;### (autoloads (read-kbd-macro edit-kbd-macro edit-last-kbd-macro) "macedit" "calc/macedit.el")

(autoload 'edit-last-kbd-macro "macedit" "\
Edit the most recently defined keyboard macro." t nil)

(autoload 'edit-kbd-macro "macedit" "\
Edit a keyboard macro which has been assigned a name by name-last-kbd-macro.
\(See also edit-last-kbd-macro.)" t nil)

(autoload 'read-kbd-macro "macedit" "\
Read the region as a keyboard macro definition.
The region is interpreted as spelled-out keystrokes, e.g., `M-x abc RET'.
The resulting macro is installed as the \"current\" keyboard macro.

Symbols:  RET, SPC, TAB, DEL, LFD, NUL; C-key; M-key.  (Must be uppercase.)
          REM marks the rest of a line as a comment.
          Whitespace is ignored; other characters are copied into the macro." t nil)

;;;***

(provide 'calc-autoloads)

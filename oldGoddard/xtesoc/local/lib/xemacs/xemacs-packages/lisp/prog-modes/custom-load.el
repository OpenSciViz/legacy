;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'prolog '("prolog"))
(custom-add-loads 'pascal '("pascal"))
(custom-add-loads 'tcl '("tcl"))
(custom-add-loads 'icon '("icon"))
(custom-add-loads 'fortran-indent '("fortran"))
(custom-add-loads 'lisp-indent '("cl-indent"))
(custom-add-loads 'tools '("make-mode"))
(custom-add-loads 'lisp '("cl-indent"))
(custom-add-loads 'f90 '("f90"))
(custom-add-loads 'perl '("cperl-mode"))
(custom-add-loads 'asm '("asm-mode"))
(custom-add-loads 'fortran-comment '("fortran"))
(custom-add-loads 'cperl-electric '("cperl-mode"))
(custom-add-loads 'verilog '("verilog-mode"))
(custom-add-loads 'f90-indent '("f90"))
(custom-add-loads 'SQL '("sql"))
(custom-add-loads 'vrml '("vrml-mode"))
(custom-add-loads 'simula '("simula"))
(custom-add-loads 'fortran '("f90" "fortran"))
(custom-add-loads 'cperl-faces '("cperl-mode"))
(custom-add-loads 'languages '("asm-mode" "cperl-mode" "fortran" "icon" "pascal" "prolog" "python-mode" "rexx-mode" "simula" "tcl" "verilog-mode" "vrml-mode"))
(custom-add-loads 'cperl-indent '("cperl-mode"))
(custom-add-loads 'faces '("cperl-mode"))
(custom-add-loads 'processes '("sql"))
(custom-add-loads 'rexx '("rexx-mode"))
(custom-add-loads 'python '("python-mode"))
(custom-add-loads 'makefile-mode '("make-mode"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'viper-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "viper/_pkg.el")

(package-provide 'viper :version 1.17 :type 'regular)

;;;***

;;;### (autoloads (viper-mode toggle-viper-mode) "viper" "viper/viper.el")

(autoload 'toggle-viper-mode "viper" "\
Toggle Viper on/off.
If Viper is enabled, turn it off.  Otherwise, turn it on." t nil)

(autoload 'viper-mode "viper" "\
Turn on Viper emulation of Vi." t nil)

;;;***

(provide 'viper-autoloads)

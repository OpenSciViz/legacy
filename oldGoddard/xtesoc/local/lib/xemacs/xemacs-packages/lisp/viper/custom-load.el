;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'viper-mouse '("viper-mous"))
(custom-add-loads 'viper-ex '("viper-ex"))
(custom-add-loads 'viper-search '("viper-init"))
(custom-add-loads 'viper-highlighting '("viper-init"))
(custom-add-loads 'emulations '("viper"))
(custom-add-loads 'viper-misc '("viper-cmd" "viper-init" "viper"))
(custom-add-loads 'viper-hooks '("viper-init"))
(custom-add-loads 'viper '("viper-ex" "viper-init" "viper-keym" "viper-macs" "viper-mous" "viper-util" "viper"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'view-process-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "view-process/_pkg.el")

(package-provide 'view-process :version 1.07 :type 'regular)

;;;***

;;;### (autoloads (View-process-status) "view-process-mode" "view-process/view-process-mode.el")

(autoload 'View-process-status "view-process-mode" "\
Prints a list with processes in the buffer `View-process-buffer-name'.
COMMAND-SWITCHES is a string with the command switches (ie: -aux).
IF the optional argument REMOTE-HOST is given, then the command will
be executed on the REMOTE-HOST. If an prefix arg is given, then the 
function asks for the name of the remote host.
If USE-LAST-SORTER-AND-FILTER is t, then the last sorter and filter 
commands are used. Otherwise the sorter and filter from the list
'View-process-sorter-and-filter' are used." t nil)

(defalias 'ps 'View-process-status)

;;;***

(provide 'view-process-autoloads)

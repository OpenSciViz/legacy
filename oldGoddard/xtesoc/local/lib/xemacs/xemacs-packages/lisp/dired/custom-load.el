;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'dired-markers '("dired"))
(custom-add-loads 'environment '("dired"))
(custom-add-loads 'tools '("diff"))
(custom-add-loads 'dired-behavior '("dired"))
(custom-add-loads 'diff '("diff"))
(custom-add-loads 'dired-hooks '("dired"))
(custom-add-loads 'dired '("dired-faces" "dired"))
(custom-add-loads 'dired-programs '("dired"))
(custom-add-loads 'dired-file-names '("dired"))

;;; custom-load.el ends here

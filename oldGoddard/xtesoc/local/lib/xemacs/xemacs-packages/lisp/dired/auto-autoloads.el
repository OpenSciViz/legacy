;;; DO NOT MODIFY THIS FILE
(if (featurep 'dired-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "dired/_pkg.el")

(package-provide 'dired :version 1.06 :type 'regular)

;;;***

;;;### (autoloads (diff-backup diff) "diff" "dired/diff.el")

(defcustom diff-switches "-c" "*A list of switches (strings) to pass to the diff program." :type '(choice string (repeat string)) :group 'diff)

(autoload 'diff "diff" "\
Find and display the differences between OLD and NEW files.
Interactively you are prompted with the current buffer's file name for NEW
and what appears to be its backup for OLD." t nil)

(autoload 'diff-backup "diff" "\
Diff this file with its backup file or vice versa.
Uses the latest backup, if there are several numerical backups.
If this file is a backup, diff it with its original.
The backup file is the first file given to `diff'." t nil)

;;;***

;;;### (autoloads (dired-jump-back-other-frame dired-jump-back-other-window dired-jump-back dired-noselect dired-other-frame dired-other-window dired default-directory) "dired" "dired/dired.el")

(autoload 'default-directory "dired" "\
 Returns the default-directory for the current buffer.
Will use the variable default-directory-function if it non-nil." nil nil)

(defcustom dired-compression-method 'gzip "*Type of compression program to use.\nGive as a symbol.\nCurrently-recognized methods are: gzip pack compact compress.\nTo change this variable use \\[dired-do-compress] with a zero prefix." :type '(choice (const gzip) (const pack) (const pack) (const compact) (const compress)) :group 'dired-programs)

(defvar dired-compression-method-alist '((gzip ".gz" ("gzip") ("gzip" "-d") "-f") (compress ".Z" ("compress" "-f") ("compress" "-d") "-f") (pack ".z" ("pack" "-f") ("unpack")) (compact ".C" ("compact") ("uncompact"))) "\
*Association list of compression method descriptions.
 Each element of the table should be a list of the form
 
     (compress-type extension (compress-args) (decompress-args) force-flag)
 
 where 
   `compress-type' is a unique symbol in the alist to which
      `dired-compression-method' can be set;
   `extension' is the file extension (as a string) used by files compressed
      by this method;
   `compress-args' is a list of the path of the compression program and
      flags to pass as separate arguments;
   `decompress-args' is a list of the path of the decompression
      program and flags to pass as separate arguments.
   `force-flag' is the switch to pass to the command to force overwriting
      of existing files.
 
 For example:
 
   (setq dired-compression-method-alist
         (cons '(frobnicate \".frob\" (\"frob\") (\"frob\" \"-d\") \"-f\")
               dired-compression-method-alist))
   => ((frobnicate \".frob\" (\"frob\") (\"frob\" \"-d\")) 
       (gzip \".gz\" (\"gzip\") (\"gunzip\"))
       ...)
 
 See also: dired-compression-method <V>")

(defcustom dired-ls-program "ls" "*Absolute or relative name of the ls program used by dired." :type 'string :group 'dired-programs)

(defcustom dired-listing-switches "-al" "*Switches passed to ls for dired. MUST contain the `l' option.\nCan contain even `F', `b', `i' and `s'." :type '(choice string (repeat string)) :group 'dired-programs)

(defcustom dired-chown-program (if (memq system-type '(hpux dgux usg-unix-v linux)) "chown" "/etc/chown") "*Name of chown command (usually `chown' or `/etc/chown')." :type 'string :group 'dired-programs)

(defcustom dired-gnutar-program nil "*If non-nil, name of the GNU tar executable (e.g. \"tar\" or \"gnutar\").\nGNU tar's `z' switch is used for compressed tar files.\nIf you don't have GNU tar, set this to nil: a pipe using `zcat' is then used." :type '(choice string (const nil)) :group 'dired-programs)

(defcustom dired-unshar-program nil "*Set to the name of the unshar program, if you have it." :type '(choice string (const nil)) :group 'dired-programs)

(defcustom dired-local-variables-file ".dired" "*If non-nil, filename for local variables for Dired.\nIf Dired finds a file with that name in the current directory, it will\ntemporarily insert it into the dired buffer and run `hack-local-variables'.\n\nType \\[info] and `g' `(emacs)File Variables' `RET' for more info on\nlocal variables." :type 'string :group 'dired-behavior)

(defcustom dired-kept-versions 2 "*When cleaning directory, number of versions to keep." :type 'integer :group 'dired-behavior)

(defcustom dired-find-subdir nil "*Determines whether dired tries to lookup a subdir in existing buffers.\nIf non-nil, dired does not make a new buffer for a directory if it can be\nfound (perhaps as subdir) in some existing dired buffer. If there are several\ndired buffers for a directory, then the most recently used one is chosen.\n\nDired avoids switching to the current buffer, so that if you have\na normal and a wildcard buffer for the same directory, C-x d RET will\ntoggle between those two." :type 'boolean :group 'dired-behavior)

(defcustom dired-use-file-transformers t "*Determines whether dired uses file transformers.\nIf non-nil `dired-do-shell-command' will apply file transformers to file names.\nSee \\[describe-function] for dired-do-shell-command for more information." :type 'boolean :group 'dired-behavior)

(defcustom dired-dwim-target nil "*If non-nil, dired tries to guess a default target directory.\nThis means that if there is a dired buffer displayed in the next window,\nuse its current subdir, instead of the current subdir of this dired buffer.\nThe target is put in the prompt for file copy, rename, etc." :type 'boolean :group 'dired-behavior)

(defcustom dired-copy-preserve-time t "*If non-nil, Dired preserves the last-modified time in a file copy.\n(This works on only some systems.)\\<dired-mode-map>\nUse `\\[dired-do-copy]' with a zero prefix argument to toggle its value." :type 'boolean :group 'dired-behavior)

(defcustom dired-no-confirm nil "*If non-nil, a list of symbols for commands dired should not confirm.\nIt can be a sublist of\n\n  '(byte-compile chgrp chmod chown compress copy delete hardlink load\n    move print shell symlink uncompress recursive-delete kill-file-buffer\n    kill-dired-buffer patch create-top-dir revert-subdirs)\n\nThe meanings of most of the symbols are obvious.  A few exceptions:\n\n    'compress applies to compression or decompression by any of the \n     compression program in `dired-compression-method-alist'.\n\n    'kill-dired-buffer applies to offering to kill dired buffers for\n     directories which have been deleted.\n\n    'kill-file-buffer applies to offering to kill buffers visiting files\n     which have been deleted.\n\n    'recursive-delete applies to recursively deleting non-empty\n     directories, and all of their contents.\n\n    'create-top-dir applies to `dired-up-directory' creating a new top level\n     directory for the dired buffer.\n\n    'revert-subdirs applies to re-reading subdirectories which have \n     been modified on disk.\n\nNote that this list also applies to remote files accessed with efs\nor ange-ftp." :type '(set (byte-compile chgrp chmod chown compress copy delete hardlink load move print shell symlink uncompress recursive-delete kill-file-buffer kill-dired-buffer patch create-top-dir revert-subdirs)) :group 'dired-behavior)

(defcustom dired-backup-if-overwrite nil "*Non-nil if Dired should ask about making backups before overwriting files.\nSpecial value 'always suppresses confirmation." :type 'boolean :group 'dired-behavior)

(defcustom dired-omit-files nil "*If non-nil un-interesting files will be omitted from this dired buffer.\nUse \\[dired-omit-toggle] to see these files. (buffer local)" :type 'boolean :group 'dired-behavior)

(defcustom dired-mail-reader 'vm "*Mail reader used by dired for dired-read-mail (\\[dired-read-mail]).\nThe symbols 'rmail and 'vm are the only two allowed values." :type '(choice (const rmail) (const vm)) :group 'dired-behavior)

(defcustom dired-refresh-automatically t "*If non-nil, refresh dired buffers automatically after file operations." :type 'boolean :group 'dired-behavior)
(define-key ctl-x-map "d" 'dired)

(autoload 'dired "dired" "\
\"Edit\" directory DIRNAME--delete, rename, print, etc. some files in it.
Optional second argument SWITCHES specifies the `ls' options used.
\(Interactively, use a prefix argument to be able to specify SWITCHES.)
Dired displays a list of files in DIRNAME (which may also have
shell wildcards appended to select certain files).  If DIRNAME is a cons,
its first element is taken as the directory name and the resr as an explicit
list of files to make directory entries for.
\\<dired-mode-map>You can move around in it with the usual commands.
You can flag files for deletion with \\[dired-flag-file-deletion] and then
delete them by typing \\[dired-expunge-deletions].
Type \\[dired-describe-mode] after entering dired for more info.

If DIRNAME is already in a dired buffer, that buffer is used without refresh." t nil)
(define-key ctl-x-4-map "d" 'dired-other-window)

(autoload 'dired-other-window "dired" "\
\"Edit\" directory DIRNAME.  Like `dired' but selects in another window." t nil)
(define-key ctl-x-5-map "d" 'dired-other-frame)

(autoload 'dired-other-frame "dired" "\
\"Edit\" directory DIRNAME.  Like `dired' but makes a new frame." t nil)

(autoload 'dired-noselect "dired" "\
Like `dired' but returns the dired buffer as value, does not select it." nil nil)
(define-key ctl-x-map "\C-j" 'dired-jump-back)

(autoload 'dired-jump-back "dired" "\
Jump back to dired.
If in a file, dired the current directory and move to file's line.
If in dired already, pop up a level and goto old directory's line.
In case the proper dired file line cannot be found, refresh the dired
  buffer and try again." t nil)
(define-key ctl-x-4-map "\C-j" 'dired-jump-back-other-window)

(autoload 'dired-jump-back-other-window "dired" "\
Like \\[dired-jump-back], but to other window." t nil)
(define-key ctl-x-5-map "\C-j" 'dired-jump-back-other-frame)

(autoload 'dired-jump-back-other-frame "dired" "\
Like \\[dired-jump-back], but in another frame." t nil)

;;;***

(provide 'dired-autoloads)

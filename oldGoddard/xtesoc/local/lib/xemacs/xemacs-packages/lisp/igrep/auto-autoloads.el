;;; DO NOT MODIFY THIS FILE
(if (featurep 'igrep-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "igrep/_pkg.el")

(package-provide 'igrep :version 1.06 :type 'regular)

;;;***

;;;### (autoloads (Buffer-menu-igrep dired-do-igrep-find dired-do-igrep igrep-visited-files igrep-find igrep igrep-insinuate) "igrep" "igrep/igrep.el")

(autoload 'igrep-insinuate "igrep" "\
Define `grep' aliases for the corresponding `igrep' commands.
With a prefix arg, override the current `grep` command definitions." t nil)

(autoload 'igrep "igrep" "\
*Run `grep` PROGRAM to match EXPRESSION in FILES.
The output is displayed in the *igrep* buffer, which `\\[next-error]' and
`\\[compile-goto-error]' parse to find each line of matched text.

PROGRAM may be nil, in which case it defaults to `igrep-program'.

EXPRESSION is automatically quoted by `shell-quote-argument'.

FILES is either a file name pattern (expanded by the shell named by
`shell-file-name') or a list of file name patterns.

Optional OPTIONS is also passed to PROGRAM; it defaults to `igrep-options'.

If a prefix argument (`\\[universal-argument]') is given when called interactively,
or if `igrep-read-options' is set, OPTIONS is read from the minibuffer.

If two prefix arguments (`\\[universal-argument] \\[universal-argument]') are given when called interactively,
or if `igrep-read-multiple-files' is set, FILES is read from the minibuffer
multiple times.

If three prefix arguments (`\\[universal-argument] \\[universal-argument] \\[universal-argument]') are given when called interactively,
or if `igrep-read-options' and `igrep-read-multiple-files' are set,
OPTIONS is read and FILES is read multiple times.

If `igrep-find' is non-nil, the directory or directories
containing FILES is recursively searched for files whose name matches
the file name component of FILES (and whose contents match EXPRESSION)." t nil)

(autoload 'igrep-find "igrep" "\
*Run `grep` via `find`; see `igrep' and `igrep-find'.
All arguments (including prefix arguments, when called interactively)
are handled by `igrep'." t nil)

(autoload 'igrep-visited-files "igrep" "\
*Run `grep` PROGRAM to match EXPRESSION (with optional OPTIONS) on all visited files.
See `\\[igrep]'." t nil)

(autoload 'dired-do-igrep "igrep" "\
*Run `grep` on the marked (or next prefix ARG) files.
See `\\[igrep]'." t nil)

(autoload 'dired-do-igrep-find "igrep" "\
*Run `grep` on the marked (or next prefix ARG) directories.
See `\\[igrep]'." t nil)

(autoload 'Buffer-menu-igrep "igrep" "\
*Run `grep` on the files visited in buffers marked with '>'.
See `\\[igrep]'." t nil)

;;;***

(provide 'igrep-autoloads)

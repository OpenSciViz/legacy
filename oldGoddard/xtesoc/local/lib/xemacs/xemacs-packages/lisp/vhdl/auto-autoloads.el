;;; DO NOT MODIFY THIS FILE
(if (featurep 'vhdl-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "vhdl/_pkg.el")

(package-provide 'vhdl :version 1.08 :type 'regular)

;;;***

;;;### (autoloads (vhdl-mode) "vhdl-mode" "vhdl/vhdl-mode.el")

(autoload 'vhdl-mode "vhdl-mode" "\
Major mode for editing VHDL code.
vhdl-mode $Revision: 2.74 $
To submit a problem report, enter `\\[vhdl-submit-bug-report]' from a
vhdl-mode buffer.  This automatically sets up a mail buffer with version
information already added.  You just need to add a description of the
problem, including a reproducable test case and send the message.

Note that the details of configuring vhdl-mode will soon be moved to the
accompanying texinfo manual.  Until then, please read the README file
that came with the vhdl-mode distribution.

The hook variable `vhdl-mode-hook' is run with no args, if that value is
bound and has a non-nil value.

Key bindings:
\\{vhdl-mode-map}" t nil)

;;;***

(provide 'vhdl-autoloads)

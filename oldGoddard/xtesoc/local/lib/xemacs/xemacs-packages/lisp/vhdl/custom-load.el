;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'languages '("vhdl-mode"))
(custom-add-loads 'vhdl '("vhdl-mode"))

;;; custom-load.el ends here

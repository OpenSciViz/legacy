;;; DO NOT MODIFY THIS FILE
(if (featurep 'vc-cc-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "vc-cc/_pkg.el")

(package-provide 'vc-cc :version 1.12 :type 'regular)

;;;***

;;;### (autoloads (tq-create) "tq" "vc-cc/tq.el")

(autoload 'tq-create "tq" "\
Create and return a transaction queue communicating with PROCESS.
PROCESS should be a subprocess capable of sending and receiving
streams of bytes.  It may be a local process, or it may be connected
to a tcp server on another machine." nil nil)

;;;***

;;;### (autoloads (vc-cc-browse-versions vc-cc-what-rule vc-cc-view-handler vc-cc-file-handler vc-mkbrtype vc-graph-history vc-edit-config vc-cc-pwv vc-rename-this-file vc-latest-version vc-locking-user vc-update-change-log vc-rename-file vc-cancel-version vc-revert-buffer vc-print-log vc-retrieve-snapshot vc-assign-name vc-create-snapshot vc-directory vc-reformat-dired-buffer vc-dired-mode vc-insert-headers vc-version-other-window vc-version-diff vc-diff vc-checkout vc-register vc-next-action vc-file-status) "vc" "vc-cc/vc.el")

(defvar vc-checkin-hook nil "\
*List of functions called after a checkin is done.  See `run-hooks'.")

(defvar vc-before-checkin-hook nil "\
*List of functions called before a checkin is done.  See `run-hooks'.")

(autoload 'vc-file-status "vc" "\
Display the current status of the file being visited.
Currently, this is only defined for CVS and ClearCase.
The information provided in the modeline is generally sufficient for
RCS and SCCS." t nil)

(autoload 'vc-next-action "vc" "\
Do the next logical checkin or checkout operation on the current file.

For RCS and SCCS files:
   If the file is not already registered, this registers it for version
control and then retrieves a writable, locked copy for editing.
   If the file is registered and not locked by anyone, this checks out
a writable and locked file ready for editing.
   If the file is checked out and locked by the calling user, this
first checks to see if the file has changed since checkout.  If not,
it performs a revert.
   If the file has been changed, this pops up a buffer for entry
of a log message; when the message has been entered, it checks in the
resulting changes along with the log message as change commentary.  If
the variable `vc-keep-workfiles' is non-nil (which is its default), a
read-only copy of the changed file is left in place afterwards.
   If the file is registered and locked by someone else, you are given
the option to steal the lock.

For CVS files:
   If the file is not already registered, this registers it for version
control.  This does a \"cvs add\", but no \"cvs commit\".
   If the file is added but not committed, it is committed.
   If the file has not been changed, neither in your working area or
in the repository, a message is printed and nothing is done.
   If your working file is changed, but the repository file is
unchanged, this pops up a buffer for entry of a log message; when the
message has been entered, it checks in the resulting changes along
with the logmessage as change commentary.  A writable file is retained.
   If the repository file is changed, you are asked if you want to
merge in the changes into your working copy.

The following is true regardless of which version control system you
are using:

   If you call this from within a VC dired buffer with no files marked,
it will operate on the file in the current line.
   If you call this from within a VC dired buffer, and one or more
files are marked, it will accept a log message and then operate on
each one.  The log message will be used as a comment for any register
or checkin operations, but ignored when doing checkouts.  Attempted
lock steals will raise an error.

   For checkin, a prefix argument lets you specify the version number to use." t nil)

(autoload 'vc-register "vc" "\
Register the current file into your version-control system." t nil)

(autoload 'vc-checkout "vc" "\
Retrieve a copy of the latest version of the given file." nil nil)

(autoload 'vc-diff "vc" "\
Display diffs between file versions.
Normally this compares the current file and buffer with the most recent
checked in version of that file.  This uses no arguments.
With a prefix argument, it reads the file name to use
and two version designators specifying which versions to compare." t nil)

(autoload 'vc-version-diff "vc" "\
For FILE, report diffs between two stored versions REL1 and REL2 of it.
If FILE is a directory, generate diffs between versions for all registered
files in or below it." t nil)

(autoload 'vc-version-other-window "vc" "\
Visit version REV of the current buffer in another window.
If the current buffer is named `F', the version is named `F.~REV~'.
If `F.~REV~' already exists, it is used instead of being re-created.

ClearCase uses the normal version-extended naming convention instead." t nil)

(autoload 'vc-insert-headers "vc" "\
Insert headers in a file for use with your version-control system.
Headers desired are inserted at the start of the buffer, and are pulled from
the variable `vc-header-alist'." t nil)

(autoload 'vc-dired-mode "vc" "\
The augmented Dired minor mode used in VC directory buffers.
All Dired commands operate normally.  Users currently locking listed files
are listed in place of the file's owner and group.
Keystrokes bound to VC commands will execute as though they had been called
on a buffer attached to the file named in the current Dired buffer line." t nil)

(autoload 'vc-reformat-dired-buffer "vc" "\
Reformats the current dired buffer using the given file alist." nil nil)

(autoload 'vc-directory "vc" "\
Show version-control status of all files under the directory DIR.
With a single prefix argument, all registered files are listed.
With two prefix arguments, only files you have checked out are listed.

Files are listed in a dired buffer, in `vc-dired-mode'." t nil)

(autoload 'vc-create-snapshot "vc" "\
Make a snapshot called NAME.
The snapshot is made from all registered files at or below the current
directory.  For each file, the version level of its latest
version becomes part of the named configuration." t nil)

(autoload 'vc-assign-name "vc" "\
Assign a NAME to the current file version.
In vc-dired-mode all marked files are labeled." t nil)

(autoload 'vc-retrieve-snapshot "vc" "\
Retrieve the snapshot called NAME.
This function fails if any files are locked at or below the current directory
Otherwise, all registered files are checked out (unlocked) at their version
levels in the snapshot." t nil)

(autoload 'vc-print-log "vc" "\
List the change log of the current buffer in a window." t nil)

(autoload 'vc-revert-buffer "vc" "\
Revert the current buffer's file back to the latest checked-in version.
This asks for confirmation if the buffer contents are not identical
to that version.
If the back-end is CVS, this will give you the most recent revision of
the file on the branch you are editing." t nil)

(autoload 'vc-cancel-version "vc" "\
Get rid of most recently checked in version of this file.
A prefix argument means do not revert the buffer afterwards." t nil)

(autoload 'vc-rename-file "vc" "\
Rename file OLD to NEW, and rename its master file likewise." t nil)

(autoload 'vc-update-change-log "vc" "\
Find change log file and add entries from recent RCS logs.
The mark is left at the end of the text prepended to the change log.
With prefix arg of C-u, only find log entries for the current buffer's file.
With any numeric prefix arg, find log entries for all files currently visited.
Otherwise, find log entries for all registered files in the default directory.
>From a program, any arguments are passed to the `rcs2log' script." t nil)

(autoload 'vc-locking-user "vc" "\
Return the name of the person currently holding a lock on FILE.
Return nil if there is no such person.
Under CVS, a file is considered locked if it has been modified since it
was checked out...even though it may well be writable by you." nil nil)

(autoload 'vc-latest-version "vc" nil nil nil)

(autoload 'vc-rename-this-file "vc" "\
Rename the file of the current buffer.
It also renames the source control archive with it" t nil)

(autoload 'vc-cc-pwv "vc" nil nil nil)

(autoload 'vc-edit-config "vc" "\
Edit a ClearCase configuration specification" t nil)

(autoload 'vc-graph-history "vc" "\
Display a graph of a file's history" t nil)

(autoload 'vc-mkbrtype "vc" nil t nil)

(autoload 'vc-cc-file-handler "vc" nil nil nil)

(autoload 'vc-cc-view-handler "vc" nil nil nil)

(autoload 'vc-cc-what-rule "vc" nil t nil)

(autoload 'vc-cc-browse-versions "vc" nil t nil)

;;;***

(provide 'vc-cc-autoloads)

;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'shell '("net-utils"))
(custom-add-loads 'mail '("emacsbug" "feedmail" "metamail"))
(custom-add-loads 'tools '("rcompile"))
(custom-add-loads 'feedmail-header '("feedmail"))
(custom-add-loads 'hypermedia '("metamail"))
(custom-add-loads 'feedmail-spray '("feedmail"))
(custom-add-loads 'feedmail-queue '("feedmail"))
(custom-add-loads 'emacsbug '("emacsbug"))
(custom-add-loads 'remote-compile '("rcompile"))
(custom-add-loads 'metamail '("metamail"))
(custom-add-loads 'shadowfile '("shadowfile"))
(custom-add-loads 'files '("shadowfile"))
(custom-add-loads 'feedmail-misc '("feedmail"))
(custom-add-loads 'maint '("emacsbug"))
(custom-add-loads 'feedmail '("feedmail"))
(custom-add-loads 'processes '("metamail" "rcompile"))
(custom-add-loads 'net-utils '("net-utils"))
(custom-add-loads 'feedmail-headers '("feedmail"))

;;; custom-load.el ends here

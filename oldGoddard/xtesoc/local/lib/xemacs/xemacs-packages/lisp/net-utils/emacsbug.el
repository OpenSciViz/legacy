;;; emacsbug.el --- command to report Emacs bugs to appropriate mailing list.

;; Copyright (C) 1985, 1994, 1997, 1998 Free Software Foundation, Inc.

;; Author: K. Shane Hartman
;; Maintainer: XEmacs Development Team
;; Keywords: maint mail

;; Not fully installed because it can work only on Internet hosts.
;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Synched up with: FSF 20.2.

;;; Commentary:

;; `M-x report-emacs-bug ' starts an email note to the Emacs maintainers
;; describing a problem.  Here's how it's done...

;;; Code:

;; >> This should be an address which is accessible to your machine,
;; >> otherwise you can't use this file.  It will only work on the
;; >> internet with this address.

(require 'sendmail)

(defgroup emacsbug nil
  "Sending Emacs bug reports."
  :group 'maint
  :group 'mail)

;; XEmacs: changed address
(defcustom report-emacs-bug-address "xemacs@xemacs.org"
  "*Address of mailing list for XEmacs bugs."
  :group 'emacsbug
  :type 'string)

;; XEmacs: changed variable name
(defcustom report-emacs-bug-beta-address "xemacs-beta@xemacs.org"
  "*Address of mailing list for XEmacs beta bugs."
  :group 'emacsbug
  :type 'string)

(defvar report-emacs-bug-orig-text nil
  "The automatically-created initial text of bug report.")

(defcustom report-emacs-bug-no-confirmation nil
  "*If non-nil, suppress the confirmations asked for the sake of novice users."
  :group 'emacsbug
  :type 'boolean)

(defcustom report-emacs-bug-no-explanations nil
  "*If non-nil, suppress the explanations given for the sake of novice users."
  :group 'emacsbug
  :type 'boolean)

;; Lots of changes for XEmacs.

;;;###autoload
(defun report-emacs-bug (topic &optional recent-keys)
  "Report a bug in XEmacs.
Prompts for bug subject.  Leaves you in a mail buffer."
  ;; This strange form ensures that (recent-keys) is the value before
  ;; the bug subject string is read.
  (interactive (reverse (list (recent-keys) (read-string "Bug Subject: "))))
  (let (user-point)
    (compose-mail (if (string-match "(beta[0-9]+)" emacs-version)
		      report-emacs-bug-beta-address
		    report-emacs-bug-address)
		  topic)
    ;; The rest of this does not execute
    ;; if the user was asked to confirm and said no.
    ;; #### FSF
    ;(rfc822-goto-eoh)
    (goto-char (point-min))
    (re-search-forward (concat "^" (regexp-quote mail-header-separator) "$"))
    (forward-line 1)

    ;; FSF has weird code to delete the user's signature, and reinsert
    ;; it without multibyte characters, to "discourage users to write
    ;; non-English text."  I don't want to use that.
    (unless report-emacs-bug-no-explanations
      ;; Insert warnings for novice users.
      (insert "This bug report will be sent to the XEmacs Development Team,\n")
      (insert-face " not to your local site managers!!" 'italic)
      (insert "\nPlease write in ")
      (insert-face "English" 'italic)
      (insert ", because the XEmacs maintainers do not have
translators to read other languages for them.\n\n"))

    (insert "In " (emacs-version) "\n")
    (if (and system-configuration-options
	     (not (equal system-configuration-options "")))
	(insert "configured using `configure "
		system-configuration-options "'\n"))
    (insert "\n")
    (insert "Please describe exactly what actions triggered the bug\n"
	    "and the precise symptoms of the bug:\n\n") 
    (setq user-point (point))
    (insert "\n\n\n"
	    "Recent keystrokes:\n\n")
    (let ((before-keys (point)))
      (insert (key-description recent-keys))
      (save-restriction
	(narrow-to-region before-keys (point))
	(goto-char before-keys)
	(while (progn (move-to-column 50) (not (eobp)))
	  (search-forward " " nil t)
	  (insert "\n"))))
    (insert "\n\n\nRecent messages (most recent first):\n\n")
    (let ((standard-output (current-buffer)))
      (print-recent-messages 10))
    ;; This is so the user has to type something
    ;; in order to send easily.
    (use-local-map (let ((map (make-sparse-keymap)))
		     (set-keymap-parents map (current-local-map))
		     map))
    (define-key (current-local-map) "\C-c\C-i" 'report-emacs-bug-info)
    (unless report-emacs-bug-no-explanations
      (with-output-to-temp-buffer "*Bug Help*"
	(if (eq mail-user-agent 'sendmail-user-agent)
	    (princ (substitute-command-keys
		    "Type \\[mail-send-and-exit] to send the bug report.\n")))
	(princ (substitute-command-keys
		"Type \\[kill-buffer] RET to cancel (don't send it).\n"))
	(terpri)
	(princ (substitute-command-keys
		"\
Type \\[report-emacs-bug-info] to visit in Info the XEmacs Manual section
about when and how to write a bug report,
and what information to supply so that the bug can be fixed.
Type SPC to scroll through this section and its subsections."))))
    ;; Make it less likely people will send empty messages.
    (make-local-variable 'mail-send-hook)
    (add-hook 'mail-send-hook 'report-emacs-bug-hook)
    (save-excursion
      (goto-char (point-max))
      (skip-chars-backward " \t\n")
      (make-local-variable 'report-emacs-bug-orig-text)
      (setq report-emacs-bug-orig-text (buffer-substring (point-min) (point))))
    (goto-char user-point)))

;; XEmacs: for backwards compatibility
(defalias 'report-xemacs-bug 'report-emacs-bug)

(defun report-emacs-bug-info ()
  "Go to the Info node on reporting Emacs bugs."
  (interactive)
  (Info-goto-node "(xemacs)Top"))

(defun report-emacs-bug-hook ()
  (save-excursion
    (goto-char (point-max))
    (skip-chars-backward " \t\n")
    (if (and (= (- (point) (point-min))
		(length report-emacs-bug-orig-text))
	     (equal (buffer-substring (point-min) (point))
		    report-emacs-bug-orig-text))
	(error "No text entered in bug report"))

    ;; Check the buffer contents and reject non-English letters.
    (save-excursion
      (goto-char (point-min))
      (skip-chars-forward "\0-\177")
      (if (not (eobp))
	  (if (or report-emacs-bug-no-confirmation
		  (y-or-n-p "Convert non-ASCII letters to hexadecimal? "))
	      (while (progn (skip-chars-forward "\0-\177")
			    (not (eobp)))
		(let ((ch (following-char)))
		  (delete-char 1)
		  (insert (format "=%02x" ch)))))))

    ;; The last warning for novice users.
    (if (or report-emacs-bug-no-confirmation
	    (yes-or-no-p
	     "Send this bug report to the Emacs maintainers? "))
	;; Just send the current mail.
	nil
      (goto-char (point-min))
      (if (search-forward "To: ")
	  (let ((pos (point)))
	    (end-of-line)
	    (delete-region pos (point))))
      (kill-local-variable 'mail-send-hook)
      (with-output-to-temp-buffer "*Bug Help*"
	(princ (substitute-command-keys "\
You invoked the command M-x report-emacs-bug,
but you decided not to mail the bug report to the Emacs maintainers.

If you want to mail it to someone else instead,
please insert the proper e-mail address after \"To: \",
and send the mail again using \\[mail-send-and-exit].")))
      (error "M-x report-emacs-bug was cancelled, please read *Bug Help* buffer"))
    ))

(provide 'emacsbug)

;;; emacsbug.el ends here

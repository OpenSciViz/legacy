;;; DO NOT MODIFY THIS FILE
(if (featurep 'elib-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "elib/_pkg.el")

(package-provide 'elib :version 1.05 :type 'single-file)

;;;***

(provide 'elib-autoloads)

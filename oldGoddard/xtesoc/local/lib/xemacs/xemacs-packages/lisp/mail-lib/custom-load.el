;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'message '("sendmail"))
(custom-add-loads 'mail-abbrevs '("mail-abbrevs"))
(custom-add-loads 'mail '("highlight-headers" "mail-abbrevs" "mail-extr" "rmail-mini" "smtpmail"))
(custom-add-loads 'rmail-reply '("rmail-mini"))
(custom-add-loads 'hypermedia '("browse-url"))
(custom-add-loads 'highlight-headers-faces '("highlight-headers"))
(custom-add-loads 'smtpmail '("smtpmail"))
(custom-add-loads 'faces '("highlight-headers"))
(custom-add-loads 'browse-url '("browse-url"))
(custom-add-loads 'news '("highlight-headers"))
(custom-add-loads 'highlight-headers '("highlight-headers"))
(custom-add-loads 'rmail '("rmail-mini"))
(custom-add-loads 'mail-extr '("mail-extr"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'mail-lib-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "mail-lib/_pkg.el")

(package-provide 'mail-lib :version 1.24 :type 'regular)

;;;***

;;;### (autoloads (browse-url-kfm browse-url-lynx-emacs browse-url-lynx-xterm browse-url-w3 browse-url-iximosaic browse-url-cci browse-url-grail browse-url-mosaic browse-url-netscape) "browse-url" "mail-lib/browse-url.el")

(defcustom browse-url-browser-function 'browse-url-w3 "*Function to display the current buffer in a WWW browser.\nUsed by the `browse-url-at-point', `browse-url-at-mouse', and\n`browse-url-of-file' commands." :type '(radio (function-item browse-url-w3) (function-item browse-url-netscape) (function-item browse-url-mosaic) (function-item browse-url-cci) (function-item browse-url-iximosaic) (function-item browse-url-lynx-xterm) (function-item browse-url-lynx-emacs) (function-item browse-url-grail) (function-item browse-url-kfm) (function :tag "Other" nil)) :group 'browse-url)

(autoload 'browse-url-netscape "browse-url" "\
Ask the Netscape WWW browser to load URL.

Default to the URL around or before point.  The strings in variable
`browse-url-netscape-arguments' are also passed to Netscape.

When called interactively, if variable `browse-url-new-window-p' is
non-nil, load the document in a new Netscape window, otherwise use a
random existing one.  A non-nil interactive prefix argument reverses
the effect of browse-url-new-window-p.

When called non-interactively, optional second argument NEW-WINDOW is
used instead of browse-url-new-window-p." t nil)

(autoload 'browse-url-mosaic "browse-url" "\
Ask the XMosaic WWW browser to load URL.
Default to the URL around or before point." t nil)

(autoload 'browse-url-grail "browse-url" "\
Ask the Grail WWW browser to load URL.
Default to the URL around or before point.  Runs the program in the
variable `browse-url-grail'." t nil)

(autoload 'browse-url-cci "browse-url" "\
Ask the XMosaic WWW browser to load URL.
Default to the URL around or before point.

This function only works for XMosaic version 2.5 or later.  You must
select `CCI' from XMosaic's File menu, set the CCI Port Address to the
value of variable `browse-url-CCI-port', and enable `Accept requests'.

When called interactively, if variable `browse-url-new-window-p' is
non-nil, load the document in a new browser window, otherwise use a
random existing one.  A non-nil interactive prefix argument reverses
the effect of browse-url-new-window-p.

When called non-interactively, optional second argument NEW-WINDOW is
used instead of browse-url-new-window-p." t nil)

(autoload 'browse-url-iximosaic "browse-url" "\
Ask the IXIMosaic WWW browser to load URL.
Default to the URL around or before point." t nil)

(autoload 'browse-url-w3 "browse-url" "\
Ask the w3 WWW browser to load URL.
Default to the URL around or before point." t nil)

(autoload 'browse-url-lynx-xterm "browse-url" "\
Ask the Lynx WWW browser to load URL.
Default to the URL around or before point.  A new Lynx process is run
in an Xterm window." t nil)

(autoload 'browse-url-lynx-emacs "browse-url" "\
Ask the Lynx WWW browser to load URL.
Default to the URL around or before point.  Run a new Lynx process in
an Emacs buffer." t nil)

(autoload 'browse-url-kfm "browse-url" "\
Ask the kfm WWW browser to load URL.
Default to the URL around or before point." t nil)

;;;***

;;;### (autoloads (highlight-headers-follow-url highlight-headers-follow-url-mosaic highlight-headers-follow-url-kfm highlight-headers-follow-url-netscape highlight-headers) "highlight-headers" "mail-lib/highlight-headers.el")

(autoload 'highlight-headers "highlight-headers" "\
Highlight message headers between start and end.
Faces used:
  message-headers			the part before the colon
  message-header-contents		the part after the colon
  message-highlighted-header-contents	contents of \"special\" headers
  message-cited-text			quoted text from other messages
  message-url				URLs (WWW uniform resource locators)

Variables used:

  highlight-headers-regexp			what makes a \"special\" header
  highlight-headers-citation-regexp		matches lines of quoted text
  highlight-headers-citation-header-regexp	matches headers for quoted text

If HACK-SIG is true,then we search backward from END for something that
looks like the beginning of a signature block, and don't consider that a
part of the message (this is because signatures are often incorrectly
interpreted as cited text.)" nil nil)

(autoload 'highlight-headers-follow-url-netscape "highlight-headers" nil nil nil)

(autoload 'highlight-headers-follow-url-kfm "highlight-headers" nil nil nil)

(autoload 'highlight-headers-follow-url-mosaic "highlight-headers" nil nil nil)

(autoload 'highlight-headers-follow-url "highlight-headers" nil t nil)

;;;***

;;;### (autoloads (define-mail-alias build-mail-aliases mail-aliases-setup) "mail-abbrevs" "mail-lib/mail-abbrevs.el")

(defcustom mail-abbrev-mailrc-file nil "Name of file with mail aliases.   If nil, ~/.mailrc is used." :type '(choice (const :tag "Default" nil) file) :group 'mail-abbrevs)

(defvar mail-aliases nil "\
Word-abbrev table of mail address aliases.
If this is nil, it means the aliases have not yet been initialized and
should be read from the .mailrc file.  (This is distinct from there being
no aliases, which is represented by this being a table with no entries.)")

(autoload 'mail-aliases-setup "mail-abbrevs" nil nil nil)

(autoload 'build-mail-aliases "mail-abbrevs" "\
Read mail aliases from .mailrc and set mail-aliases." nil nil)

(autoload 'define-mail-alias "mail-abbrevs" "\
Define NAME as a mail-alias that translates to DEFINITION.
If DEFINITION contains multiple addresses, separate them with commas." t nil)

;;;***

;;;### (autoloads (what-domain mail-extract-address-components) "mail-extr" "mail-lib/mail-extr.el")

(autoload 'mail-extract-address-components "mail-extr" "\
Given an RFC-822 ADDRESS, extract full name and canonical address.
Returns a list of the form (FULL-NAME CANONICAL-ADDRESS).
If no name can be extracted, FULL-NAME will be nil.
ADDRESS may be a string or a buffer.  If it is a buffer, the visible 
 (narrowed) portion of the buffer will be interpreted as the address.
 (This feature exists so that the clever caller might be able to avoid
 consing a string.)
If ADDRESS contains more than one RFC-822 address, only the first is
 returned.  Some day this function may be extended to extract multiple
 addresses, or perhaps return the position at which parsing stopped." nil nil)

(autoload 'what-domain "mail-extr" "\
Prompts for a mail domain, and prints the country it corresponds to
in the minibuffer." t nil)

;;;***

;;;### (autoloads (mail-fetch-field mail-file-babyl-p) "mail-utils" "mail-lib/mail-utils.el")

(defvar mail-use-rfc822 nil "\
*If non-nil, use a full, hairy RFC822 parser on mail addresses.
Otherwise, (the default) use a smaller, somewhat faster, and
often correct parser.")

(autoload 'mail-file-babyl-p "mail-utils" nil nil nil)

(defvar rmail-dont-reply-to-names nil "\
*A regexp specifying names to prune of reply to messages.
A value of nil means exclude your own name only.")

(autoload 'mail-fetch-field "mail-utils" "\
Return the value of the header field FIELD-NAME.
The buffer is expected to be narrowed to just the headers of the message.
If second arg LAST is non-nil, use the last such field if there are several.
If third arg ALL is non-nil, concatenate all such fields with commas between." nil nil)

;;;***

;;;### (autoloads (pop3-movemail pop3-nnmail-movemail) "pop3" "mail-lib/pop3.el")

(autoload 'pop3-nnmail-movemail "pop3" "\
Function to move mail from INBOX on a pop3 server to file CRASHBOX." nil nil)

(autoload 'pop3-movemail "pop3" "\
Transfer contents of a maildrop to the specified CRASHBOX." nil nil)

;;;***

;;;### (autoloads (reporter-submit-bug-report) "reporter" "mail-lib/reporter.el")

(autoload 'reporter-submit-bug-report "reporter" nil nil nil)

;;;***

;;;### (autoloads nil "rmail-mini" "mail-lib/rmail-mini.el")

(defcustom rmail-dont-reply-to-names nil "*A regexp specifying names to prune of reply to messages.\nA value of nil means exclude your own name only." :type '(choice regexp (const :tag "Your Name" nil)) :group 'rmail-reply)

(defvar rmail-default-dont-reply-to-names "info-" "\
A regular expression specifying part of the value of the default value of
the variable `rmail-dont-reply-to-names', for when the user does not set
`rmail-dont-reply-to-names' explicitly.  (The other part of the default
value is the user's name.)
It is useful to set this variable in the site customization file.")

;;;***

;;;### (autoloads (rmail-file-p) "rmailout" "mail-lib/rmailout.el")

(autoload 'rmail-file-p "rmailout" nil nil nil)

;;;***

;;;### (autoloads (mail-other-frame mail-other-window mail mail-mode user-mail-address) "sendmail" "mail-lib/sendmail.el")

(defvar mail-from-style 'angles "\
*Specifies how \"From:\" fields look.

If `nil', they contain just the return address like:
	king@grassland.com
If `parens', they look like:
	king@grassland.com (Elvis Parsley)
If `angles', they look like:
	Elvis Parsley <king@grassland.com>")

(defvar mail-self-blind nil "\
Non-nil means insert BCC to self in messages to be sent.
This is done when the message is initialized,
so you can remove or alter the BCC field to override the default.")

(defvar mail-interactive nil "\
Non-nil means when sending a message wait for and display errors.
nil means let mailer mail back a message to report errors.")

(defvar mail-dir nil "\
*Default directory for saving messages.")

(defvar rmail-ignored-headers (purecopy (concat "^\\(" (mapconcat 'identity '("Sender:" "References:" "Return-Path:" "Received:" "[^: 	\n]*Message-ID:" "Errors-To:" "Path:" "Expires:" "Xref:" "Lines:" "Approved:" "Distribution:" "Content-Length:" "Mime-Version:" "Content-Type:" "Content-Transfer-Encoding:" "X400-Received:" "X400-Originator:" "X400-Mts-Identifier:" "X400-Content-Type:" "Content-Identifier:" "Status:" "Summary-Line:" "X-Attribution:" "Via:" "Sent-Via:" "Mail-From:" "Origin:" "Comments:" "Originator:" "NF-ID:" "NF-From:" "Posting-Version:" "Posted:" "Posted-Date:" "Date-Received:" "Relay-Version:" "Article-I\\.D\\.:" "NNTP-Version:" "NNTP-Posting-Host:" "X-Mailer:" "X-Newsreader:" "News-Software:" "X-Received:" "X-References:" "X-Envelope-To:" "X-VMS-" "Remailed-" "X-Plantation:" "X-Windows:" "X-Pgp-") "\\|") "\\)")) "\
*Gubbish header fields one would rather not see.")

(defvar mail-yank-ignored-headers (purecopy (concat rmail-ignored-headers "\\|" "^\\(" (mapconcat 'identity '("Resent-To:" "Resent-By:" "Resent-CC:" "To:" "Subject:" "In-Reply-To:") "\\|") "\\)")) "\
Delete these headers from old message when it's inserted in a reply.")

(defvar send-mail-function 'sendmail-send-it "\
Function to call to send the current buffer as mail.
The headers should be delimited by a line whose contents
match the variable `mail-header-separator'.")

(defvar mail-header-separator (purecopy "--text follows this line--") "\
*Line used to separate headers from text in messages being composed.")

(defvar mail-archive-file-name nil "\
*Name of file to write all outgoing messages in, or nil for none.
This can be an inbox file or an Rmail file.")

(defvar mail-default-reply-to nil "\
*Address to insert as default Reply-to field of outgoing messages.
If nil, it will be initialized from the REPLYTO environment variable
when you first send mail.")

(defvar mail-alias-file nil "\
*If non-nil, the name of a file to use instead of `/usr/lib/aliases'.
This file defines aliases to be expanded by the mailer; this is a different
feature from that of defining aliases in `.mailrc' to be expanded in Emacs.
This variable has no effect unless your system uses sendmail as its mailer.")

(defvar mail-yank-prefix "> " "\
*Prefix insert on lines of yanked message being replied to.
nil means use indentation.")

(defvar mail-signature nil "\
*Text inserted at end of mail buffer when a message is initialized.
If t, it means to insert the contents of the file `mail-signature-file'.")

(autoload 'user-mail-address "sendmail" "\
Query the user for his mail address, unless it is already known." t nil)

(autoload 'mail-mode "sendmail" "\
Major mode for editing mail to be sent.
Like Text Mode but with these additional commands:
C-c C-s  mail-send (send the message)    C-c C-c  mail-send-and-exit
C-c C-f  move to a header field (and create it if there isn't):
	 C-c C-f C-t  move to To:	C-c C-f C-s  move to Subj:
	 C-c C-f C-b  move to BCC:	C-c C-f C-c  move to CC:
	 C-c C-f C-f  move to FCC:	C-c C-f C-r  move to Reply-To:
C-c C-t  mail-text (move to beginning of message text).
C-c C-w  mail-signature (insert `mail-signature-file' file).
C-c C-y  mail-yank-original (insert current message, in Rmail).
C-c C-q  mail-fill-yanked-message (fill what was yanked).
C-c C-v  mail-sent-via (add a sent-via field for each To or CC)." t nil)

(autoload 'mail "sendmail" "\
Edit a message to be sent.  Prefix arg means resume editing (don't erase).
When this function returns, the buffer `*mail*' is selected.
The value is t if the message was newly initialized; otherwise, nil.

Optionally, the signature file `mail-signature-file' can be inserted at the
end; see the variable `mail-signature'.

\\<mail-mode-map>
While editing message, type \\[mail-send-and-exit] to send the message and exit.

Various special commands starting with C-c are available in sendmail mode
to move to message header fields:
\\{mail-mode-map}

The variable `mail-signature' controls whether the signature file
`mail-signature-file' is inserted immediately.

If `mail-signature' is nil, use \\[mail-signature] to insert the
signature in `mail-signature-file'.

If `mail-self-blind' is non-nil, a BCC to yourself is inserted
when the message is initialized.

If `mail-default-reply-to' is non-nil, it should be an address (a string);
a Reply-to: field with that address is inserted.

If `mail-archive-file-name' is non-nil, an FCC field with that file name
is inserted.

The normal hook `mail-setup-hook' is run after the message is
initialized.  It can add more default fields to the message.

When calling from a program, the first argument if non-nil says
not to erase the existing contents of the `*mail*' buffer.

The second through fifth arguments,
 TO, SUBJECT, IN-REPLY-TO and CC, specify if non-nil
 the initial contents of those header fields.
 These arguments should not have final newlines.
The sixth argument REPLYBUFFER is a buffer whose contents
 should be yanked if the user types C-c C-y.
The seventh argument ACTIONS is a list of actions to take
 if/when the message is sent.  Each action looks like (FUNCTION . ARGS);
 when the message is sent, we apply FUNCTION to ARGS.
 This is how Rmail arranges to mark messages `answered'." t nil)

(autoload 'mail-other-window "sendmail" "\
Like `mail' command, but display mail buffer in another window." t nil)

(autoload 'mail-other-frame "sendmail" "\
Like `mail' command, but display mail buffer in another frame." t nil)
(add-hook 'same-window-buffer-names "*mail*")

;;;***

;;;### (autoloads (smtpmail-send-it) "smtpmail" "mail-lib/smtpmail.el")

(autoload 'smtpmail-send-it "smtpmail" nil nil nil)

;;;***

(provide 'mail-lib-autoloads)

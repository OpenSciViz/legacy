;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'ps-print-face '("ps-print"))
(custom-add-loads 'ps-print-header '("ps-print"))
(custom-add-loads 'ps-print '("ps-print"))
(custom-add-loads 'ps-print-font '("ps-print"))
(custom-add-loads 'ps-print-horizontal '("ps-print"))
(custom-add-loads 'faces '("ps-print"))
(custom-add-loads 'wp '("ps-print"))
(custom-add-loads 'ps-print-vertical '("ps-print"))
(custom-add-loads 'ps-print-color '("ps-print"))

;;; custom-load.el ends here

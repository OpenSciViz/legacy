;;; ps-swapper.el --- landscape functions for ps-print

;; Copyright (C) 1999 Thomas E Deweese

;; Author: Thomas E Deweese  <deweese@kodak.com>
;; Keywords: hardware

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;; 02111-1307, USA.

;;; Synched up with: Not in FSF

;;; Commentary:

;; 

;;; Code:

(defvar ps-portrait-font-size '()
  "Font size for body when printing portrait, only used if non-nil.
Otherwise it uses, 1.2 * ps-font-size, or if that is unset, 8.5")
(defvar ps-header-portrait-font-size '()
  "Font size for Header when printing portrait, only used if non-nil
Otherwise it uses 1.2 * ps-header-font-size, or if that is unset, 12")
(defvar ps-header-title-portrait-font-size '()
  "Font size for Title (in header) when printing portrait, 
only used if non-nil.
Otherwise it uses 1.2 * ps-header-title-font-size, or if that is unset, 14")

(defvar ps-landscape-font-size '()
  "Font size for Body when printing landscape, only used if non-nil.
Otherwise it uses, ps-font-size/1.2, or if that is unset, 7")
(defvar ps-header-landscape-font-size '()
  "Font size for Header when printing landscape, only used if non-nil.
Otherwise it uses, ps-font-size/1.2, or if that is unset, 10")
(defvar ps-header-title-landscape-font-size '()
  "Font size for Title (in header) when printing landscape, 
only used if non-nil.
Otherwise it uses, ps-font-size/1.2, or if that is unset, 12")

(defun td-set-print-mode (landscape-p)
  "Sets up ps-print for landscape/portrait printing"
  (interactive (list (y-or-n-p "Print landscape")))
  (if (and ps-landscape-mode (not landscape-p))
      (setq 
       ps-landscape-mode    landscape-p
       ps-number-of-columns 1

       ps-font-size (cond 
		     ((boundp 'ps-portrait-font-size) 
		      ps-portrait-font-size)
		     ((boundp 'ps-font-size) (* ps-font-size 1.2))
		     (t 8.5))

       ps-header-font-size (cond
			    ((and (boundp 'ps-header-portrait-font-size) 
				  ps-header-portrait-font-size)
			     ps-header-portrait-font-size)
			    ((and (boundp 'ps-header-font-size)
				  ps-header-font-size)
			     (* ps-header-font-size 1.2))
			    (t 12))
       ps-header-title-font-size
       (cond ((boundp 'ps-header-title-portrait-font-size)
	      ps-header-title-portrait-font-size)
	     ((boundp 'ps-header-title-font-size)
	      (* ps-header-title-font-size 1.2))
	     (t 14))
       )
    )
  (if (and (not ps-landscape-mode) landscape-p)
      (setq 
       ps-landscape-mode    landscape-p
       ps-number-of-columns 2
       ps-font-size (cond 
		     ((boundp 'ps-landscape-font-size) 
		      ps-landscape-font-size)
		     ((boundp 'ps-font-size) (/ ps-font-size 1.2))
		     (t 7))

       ps-header-font-size (cond
			    ((boundp 'ps-header-landscape-font-size) 
			     ps-header-landscape-font-size)
			    ((boundp 'ps-header-font-size)
			     (/ ps-header-font-size 1.2))
			    (t 10))
       ps-header-title-font-size
       (cond ((boundp 'ps-header-title-landscape-font-size)
	      ps-header-title-landscape-font-size)
	     ((boundp 'ps-header-title-font-size)
	      (/ ps-header-title-font-size 1.2))
	     (t 12))
       )
    )
  )
	     
(defun td-ps-print-buffer-with-faces (landscape-p)
  "Queries user about printing landscape/portrait"
  (interactive (list (y-or-n-p "Print landscape")))
  (let ((old ps-landscape-mode))
    (td-set-print-mode landscape-p)
    (ps-print-buffer-with-faces)
    (td-set-print-mode old)
    )
  )

(defun td-ps-print-region-with-faces (landscape-p)
  "Queries user about printing landscape/portrait"
  (interactive '(list (y-or-n-p "Print landscape")))
  (let ((old ps-landscape-mode))
    (td-set-print-mode landscape-p)
    (ps-print-buffer-with-faces)
    (td-set-print-mode old)
    )
  )

(defun td-ps-spool-buffer-with-faces (landscape-p)
  "Queries user about printing landscape/portrait"
  (interactive (list (y-or-n-p "Print landscape")))
  (let ((old ps-landscape-mode))
    (td-set-print-mode landscape-p)
    (ps-spool-buffer-with-faces)
    (td-set-print-mode old)
    )
  )

(defun td-ps-spool-region-with-faces (landscape-p)
  "Queries user about printing landscape/portrait"
  (interactive '(list (y-or-n-p "Print landscape")))
  (let ((old ps-landscape-mode))
    (td-set-print-mode landscape-p)
    (ps-spool-buffer-with-faces)
    (td-set-print-mode old)
    )
  )

(defalias 'td-ps-despool 'ps-despool)

(provide 'ps-swapper)

;;; ps-swapper.el ends here

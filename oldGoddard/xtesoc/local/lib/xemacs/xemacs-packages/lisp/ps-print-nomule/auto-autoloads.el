;;; DO NOT MODIFY THIS FILE
(if (featurep 'ps-print-nomule-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "ps-print-nomule/_pkg.el")

(package-provide 'ps-print-nomule :version 1.02 :type 'regular)

;;;***

;;;### (autoloads (ps-setup ps-nb-pages-region ps-nb-pages-buffer ps-line-lengths ps-despool ps-spool-region-with-faces ps-spool-region ps-spool-buffer-with-faces ps-spool-buffer ps-print-region-with-faces ps-print-region ps-print-buffer-with-faces ps-print-buffer) "ps-print" "ps-print-nomule/ps-print.el")

(defcustom ps-paper-type 'letter "*Specifies the size of paper to format for.\nShould be one of the paper types defined in `ps-page-dimensions-database', for\nexample `letter', `legal' or `a4'." :type '(symbol :validate (lambda (wid) (if (assq (widget-value wid) ps-page-dimensions-database) nil (widget-put wid :error "Unknown paper size") wid))) :group 'ps-print)

(defcustom ps-print-color-p (or (fboundp 'x-color-values) (fboundp 'color-instance-rgb-components)) "*If non-nil, print the buffer's text in color." :type 'boolean :group 'ps-print-color)

(autoload 'ps-print-buffer "ps-print" "\
Generate and print a PostScript image of the buffer.

When called with a numeric prefix argument (C-u), prompts the user for
the name of a file to save the PostScript image in, instead of sending
it to the printer.

More specifically, the FILENAME argument is treated as follows: if it
is nil, send the image to the printer.  If FILENAME is a string, save
the PostScript image in a file with that name.  If FILENAME is a
number, prompt the user for the name of the file to save in." t nil)

(autoload 'ps-print-buffer-with-faces "ps-print" "\
Generate and print a PostScript image of the buffer.
Like `ps-print-buffer', but includes font, color, and underline
information in the generated image.  This command works only if you
are using a window system, so it has a way to determine color values." t nil)

(autoload 'ps-print-region "ps-print" "\
Generate and print a PostScript image of the region.
Like `ps-print-buffer', but prints just the current region." t nil)

(autoload 'ps-print-region-with-faces "ps-print" "\
Generate and print a PostScript image of the region.
Like `ps-print-region', but includes font, color, and underline
information in the generated image.  This command works only if you
are using a window system, so it has a way to determine color values." t nil)

(autoload 'ps-spool-buffer "ps-print" "\
Generate and spool a PostScript image of the buffer.
Like `ps-print-buffer' except that the PostScript image is saved in a
local buffer to be sent to the printer later.

Use the command `ps-despool' to send the spooled images to the printer." t nil)

(autoload 'ps-spool-buffer-with-faces "ps-print" "\
Generate and spool a PostScript image of the buffer.
Like `ps-spool-buffer', but includes font, color, and underline
information in the generated image.  This command works only if you
are using a window system, so it has a way to determine color values.

Use the command `ps-despool' to send the spooled images to the printer." t nil)

(autoload 'ps-spool-region "ps-print" "\
Generate a PostScript image of the region and spool locally.
Like `ps-spool-buffer', but spools just the current region.

Use the command `ps-despool' to send the spooled images to the printer." t nil)

(autoload 'ps-spool-region-with-faces "ps-print" "\
Generate a PostScript image of the region and spool locally.
Like `ps-spool-region', but includes font, color, and underline
information in the generated image.  This command works only if you
are using a window system, so it has a way to determine color values.

Use the command `ps-despool' to send the spooled images to the printer." t nil)

(autoload 'ps-despool "ps-print" "\
Send the spooled PostScript to the printer.

When called with a numeric prefix argument (C-u), prompt the user for
the name of a file to save the spooled PostScript in, instead of sending
it to the printer.

More specifically, the FILENAME argument is treated as follows: if it
is nil, send the image to the printer.  If FILENAME is a string, save
the PostScript image in a file with that name.  If FILENAME is a
number, prompt the user for the name of the file to save in." t nil)

(autoload 'ps-line-lengths "ps-print" "\
*Display the correspondence between a line length and a font size,
using the current ps-print setup.
Try: pr -t file | awk '{printf \"%3d %s
\", length($0), $0}' | sort -r | head" t nil)

(autoload 'ps-nb-pages-buffer "ps-print" "\
*Display an approximate correspondence between a font size and the number
of pages the current buffer would require to print
using the current ps-print setup." t nil)

(autoload 'ps-nb-pages-region "ps-print" "\
*Display an approximate correspondence between a font size and the number
of pages the current region would require to print
using the current ps-print setup." t nil)

(autoload 'ps-setup "ps-print" "\
*Return the current setup" nil nil)

;;;***

(provide 'ps-print-nomule-autoloads)

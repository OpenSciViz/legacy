;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'ssh '("ssh"))
(custom-add-loads 'tar '("tar-mode"))
(custom-add-loads 'telnet '("telnet"))
(custom-add-loads 'crypt '("crypt"))
(custom-add-loads 'comint '("telnet"))
(custom-add-loads 'lpr '("lpr"))
(custom-add-loads 'mchat '("mchat"))
(custom-add-loads 'jka-compr '("jka-compr"))
(custom-add-loads 'rlogin '("rlogin"))
(custom-add-loads 'data '("arc-mode" "jka-compr" "tar-mode"))
(custom-add-loads 'compression '("jka-compr"))
(custom-add-loads 'archive-lzh '("arc-mode"))
(custom-add-loads 'archive '("arc-mode"))
(custom-add-loads 'archive-arc '("arc-mode"))
(custom-add-loads 'archive-zoo '("arc-mode"))
(custom-add-loads 'archive-zip '("arc-mode"))
(custom-add-loads 'processes '("background" "rlogin" "ssh"))
(custom-add-loads 'wp '("lpr"))
(custom-add-loads 'background '("background"))
(custom-add-loads 'unix '("rlogin" "ssh" "tar-mode"))

;;; custom-load.el ends here

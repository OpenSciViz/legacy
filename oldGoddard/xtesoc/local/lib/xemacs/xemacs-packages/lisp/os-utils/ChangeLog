1999-07-20  Darryl Okahata  <darrylo@sr.hp.com>

	* tar-mode.el: `tar-regexp' must only match the names of
	uncompressed tar files.  If it matches the names of compressed tar
	files, tar-mode will attempt to parse the compressed tar file as
	an uncompressed tar file, causing an error.  Note that this will
	not affect either crypt.el nor jka-compr.el, as they will properly
	uncompress a buffer before entering tar-mode.

1999-07-15  SL Baur  <steve@beopen.com>

	* jka-compr.el (jka-compr-install): Only diddle with
	file-coding-system-alist in XEmacs with file-coding.
	(jka-compr-uninstall): Ditto.

1999-06-29  SL Baur  <steve@miho.m17n.org>

	* Makefile (ELCS): ps-print.el removed.

1999-03-19  Didier Verna  <didier@xemacs.org>

	* mchat.el: version 2.0 released.

1998-12-30  Martin Buchholz  <martin@xemacs.org>

	* tar-mode.el (tar-regexp): Add autoload cookie.
	(auto-mode-alist): Add autoload cookie.

1998-10-01  SL Baur  <steve@altair.xemacs.org>

	* ssh.el (ssh-send-Ctrl-C): send-string -> process-send-string.
	(ssh-send-Ctrl-D): Ditto.
	(ssh-send-Ctrl-Z): Ditto.
	(ssh-send-Ctrl-backslash): Ditto.
	* rlogin.el (rlogin-send-Ctrl-C): Ditto.
	(rlogin-send-Ctrl-D): Ditto.
	(rlogin-send-Ctrl-Z): Ditto.
	(rlogin-send-Ctrl-backslash): Ditto.

1998-09-30  SL Baur  <steve@altair.xemacs.org>

	* ftelnet.el: New file.
	* ssh.el: Sync to version 1.6
	* rlogin.el: Sync to version 1.43

1998-09-10  Noah Friedman  <friedman@splode.com>

	* rlogin.el (rlogin): If using make-local-hook, pass the 4th arg
	`t' to add-hook also to modify the new local value.
	* ssh.el (ssh): Here also.

1998-07-14  SL Baur  <steve@altair.xemacs.org>

	* rlogin.el: Global replace send-string -> process-send-string.
	* ssh.el: Ditto.

1998-06-07  Tomasz Cholewo  <tjchol01@aivo.spd.louisville.edu>

	* jka-compr.el (jka-compr-partial-uncompress): Fix integer in concat.

1998-06-13  SL Baur  <steve@altair.xemacs.org>

	* lpr.el (print-region-1): Remove references to `buffer-file-type'.

1998-04-17  SL Baur  <steve@altair.xemacs.org>

	* rlogin.el-1.40.

1998-04-06  Hrvoje Niksic  <hniksic@srce.hr>

	* tar-mode.el (tar-subfile-mode): Call `auto-save-mode' with a 0,
	argument, not nil.
	(tar-regexp): Include `.tar.gz' files.

1998-02-21  Aki Vehtari  <Aki.Vehtari@hut.fi>

	* tar-mode.el:  Add "q" for quit, and use "C" for copy and "R" for
	rename as in dired.

1998-02-15  SL Baur  <steve@altair.xemacs.org>

	* jka-compr.el: Reenable auto-compression-mode symbol.

1998-01-24  SL Baur  <steve@altair.xemacs.org>

	* Makefile (VERSION): Update to package standard 1.0.
	* package-info.in: Ditto.

1998-01-11  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Update to newer package interface.

1998-01-05  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Update to newer packages interface.

1997-12-27  Jens-Ulrik Holger Petersen  <petersen@kurims.kyoto-u.ac.jp>

	* tar-mode.el: Minimal synchage with Emacs 20.  Moved `tar-' to the
	front of all identifiers.  Some docstring improvements too.
	(tar-mode-map): Added keybindings for [up], [down] and [return].

1997-12-24  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Created.

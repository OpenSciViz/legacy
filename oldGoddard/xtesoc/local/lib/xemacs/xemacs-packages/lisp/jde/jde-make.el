;;; jde-make.el -- Integrated Development Environment for Java.
;; $Revision: 1.4 $ 

;; Author: Paul Kinnucan <paulk@mathworks.com>
;; Maintainer: Paul Kinnucan
;; Keywords: java, tools

;; Copyright (C) 1997, 1998 Paul Kinnucan.

;; GNU Emacs is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

(require 'compile)

(defcustom jde-make-program "make"
  "*Specifies name of make program."
 :group 'jde-project
 :type 'string)

(defcustom jde-make-args ""
  "*Specifies arguments to be passed to make program."
  :group 'jde-project
  :type 'string)

;;;###autoload
(defun jde-make (args)
  "Run the JDE make program."
  (interactive
   (list (if (string= jde-make-args "")
	     (read-from-minibuffer (concat jde-make-program " ")
				   (nth 0 minibuffer-history))
	   jde-make-args)))

  (compile 
   (concat
    jde-make-program
    " "
    (jde-run-make-arg-string
     (jde-run-parse-args args))
    "\n\n")))

(provide 'jde-make)

;; $Log: jde-make.el $
;; Revision 1.4  1998/11/27 09:38:23  paulk
;; Changed to use compile mode as suggested by Robert Grace <rmg2768@draper.com>.
;;
;; Revision 1.3  1998/05/29 01:46:39  paulk
;; Added dummy function for jde-make-mode to facilitate autoloading.
;;
;; Revision 1.2  1998/05/27 06:04:52  paulk
;; Added autoload comments.
;;
;; Revision 1.1  1998/03/27 04:44:36  kinnucan
;; Initial revision
;;

;; End of jde-make.el
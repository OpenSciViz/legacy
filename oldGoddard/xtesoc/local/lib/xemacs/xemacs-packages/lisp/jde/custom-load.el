;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'jde-project '("jde-db" "jde-make" "jde-run" "jde"))
(custom-add-loads 'jde-gen '("jde-gen"))
(custom-add-loads 'jde-db-options '("jde-db"))
(custom-add-loads 'tools '("jde"))
(custom-add-loads 'jde '("jde-compile" "jde-db" "jde-gen" "jde-run" "jde"))
(custom-add-loads 'jde-compile-options '("jde-compile"))
(custom-add-loads 'jde-run-options '("jde-run"))

;;; custom-load.el ends here

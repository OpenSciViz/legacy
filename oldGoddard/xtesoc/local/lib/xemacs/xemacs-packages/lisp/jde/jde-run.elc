;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:50:12 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/prog/jde/jde-run.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`jde-run.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


(custom-declare-variable 'jde-run-mode-hook 'nil "*List of hook functions run by `jde-run-mode' (see `run-hooks')." :group 'jde-project :type 'hook)
(custom-declare-variable 'jde-run-application-class '"" "*Name of the Java class to run. \nThis is the class that is run if you select JDE->Run App from the JDE\nmenu or type C-c C-v C-r. If this option is the empty string, the JDE\nruns the class corresponding to the source file in the current\nbuffer. Note that the specified class must have a static public main\nmethod." :group 'jde-project :type 'string)
(custom-declare-variable 'jde-run-working-directory '"" "*Path of the working directory for this application.\nIf you specify a path, the JDE launches the application from the\ndirectory specified by the path." :group 'jde-project :type 'string)
(custom-declare-variable 'jde-run-java-vm '"java" "*Specify Java interpreter for non-Windows platforms." :group 'jde-project :type 'string)
(custom-declare-variable 'jde-run-java-vm-w '"javaw" "*Specify Java interpreter for Windows platforms.\nOn NTEmacs, the JDE must use the NTEmacs show window option in order\nto run the Java interpreter. This in turn requires that the JDE use\nthe javaw version of the JDK Java interpreter to prevent a console\nwindow from appearing every time you run an application. Sound\nconfusing? It is, but it works." :group 'jde-project :type 'string)
(custom-declare-variable 'jde-run-read-vm-args 'nil "*Read vm arguments from the minibuffer.\nIf this variable is non-nil, the jde-run command reads vm arguments\nfrom the minibuffer and appends them to those specified by\nthe `jde-run-option' variable group." :group 'jde-project :type 'boolean)
#@40 Vm arguments read from the minibuffer.
(defvar jde-run-interactive-vm-args "" (#$ . 2279))
#@50 History of vm arguments read from the minibuffer
(defvar jde-run-interactive-vm-arg-history nil (#$ . 2376))
(custom-declare-variable 'jde-run-read-app-args 'nil "*Read arguments to be passed to application from the minibuffer." :group 'jde-project :type 'boolean)
#@49 Application arguments read from the minibuffer.
(defvar jde-run-interactive-app-args "" (#$ . 2647))
#@59 History of application arguments read from the minibuffer
(defvar jde-run-interactive-app-arg-history nil (#$ . 2754))
(custom-declare-group 'jde-run-options nil "JDE Interpreter Options" :group 'jde :prefix "jde-run-option-")
(custom-declare-variable 'jde-run-option-classpath 'nil "*Specify paths of classes required to run this application.\nThe JDE uses the specified paths to construct a -classpath\nargument to pass to the Java interpreter. This option overrides the\n`jde-global-classpath' option." :group 'jde-run-options :type '(repeat (file :tag "Path")))
(custom-declare-variable 'jde-run-option-verbose '(list nil nil nil) "*Print messages about the running process.\nThe messages are printed in the run buffer." :group 'jde-run-options :type '(list :indent 2 (checkbox :format "\n  %[%v%] %h \n" :doc "Print classes loaded.\nPrints a message in the run buffer each time a class is loaded.") (checkbox :format "%[%v%] %h \n" :doc "Print memory freed.\nPrints a message in the run buffer each time the garbage collector\nfrees memory.") (checkbox :format "%[%v%] %h \n" :doc "Print JNI info.\nPrints JNI-related messages including information about which native\nmethods have been linked and warnings about excessive creation of\nlocal references.")))
(custom-declare-variable 'jde-run-option-properties 'nil "*Specify property values.\nEnter the name of the property, for example, awt.button.color, in the\nProperty Name field; enter its value, for example, green, in the\nProperty Value field. You can specify as many properties as you like." :group 'jde-run-options :type '(repeat (cons (string :tag "Property Name") (string :tag "Property Value"))))
(custom-declare-variable 'jde-run-option-heap-size '(list (cons 1 "megabytes") (cons 16 "megabytes")) "*Specify the initial and maximum size of the interpreter heap." :group 'jde-run-options :type '(list (cons (integer :tag "Start") (radio-button-choice (const "bytes") (const "kilobytes") (const "megabytes"))) (cons (integer :tag "Max") (radio-button-choice (const "bytes") (const "kilobytes") (const "megabytes")))))
(custom-declare-variable 'jde-run-option-stack-size '(list (cons 128 "kilobytes") (cons 400 "kilobytes")) "*Specify size of the C and Java stacks." :group 'jde-run-options :type '(list (cons (integer :tag "C Stack") (radio-button-choice (const "bytes") (const "kilobytes") (const "megabytes"))) (cons (integer :tag "Java Stack") (radio-button-choice (const "bytes") (const "kilobytes") (const "megabytes")))))
(custom-declare-variable 'jde-run-option-garbage-collection '(list t t) "*Specify garbage collection options." :group 'jde-run-options :type '(list :indent 2 (checkbox :format "%[%v%] %t \n" :tag "Collect garbage asynchronously.") (checkbox :format "%[%v%] %t \n" :tag "Collect unused classes.")))
(custom-declare-variable 'jde-run-option-java-profile '(cons nil "./java.prof") "*Enable Java profiling." :group 'jde-run-options :type '(cons boolean (file :tag "File" :help-echo "Specify where to put profile results here.")))
(custom-declare-variable 'jde-run-option-heap-profile '(cons nil (list "./java.hprof" 5 20 "Allocation objects")) "*Output heap profiling data." :group 'jde-run-options :type '(cons boolean (list (string :tag "Ouput File Path") (integer :tag "Stack Trace Depth") (integer :tag "Allocation Sites") (radio-button-choice :format "%t \n%v" :tag "Sort output based on:" (const "Allocation objects") (const "Live objects")))))
(custom-declare-variable 'jde-run-option-verify '(list nil t) "*Verify classes." :group 'jde-run-options :type '(list :indent 2 (checkbox :format "%[%v%] %t \n" :tag "Executed code in all classes.") (checkbox :format "%[%v%] %t \n" :tag "Classes loaded by a classloader.")))
(custom-declare-variable 'jde-run-option-vm-args 'nil "*Specify arguments to be passed to the Java vm.\nThis option allows you to specify one or more arguments to be passed\nto the Java interpreter. It is an alternative to using JDE Run Option\nvariables, such as `jde-run-option-stack-size', to specify Java\ninterpreter options. Also, it makes it possible to use the JDE with\ninterpreters that accept command line arguments not supported by \nthe JDE Run Option variable set." :group 'jde-run-options :type '(repeat (string :tag "Argument")))
(custom-declare-variable 'jde-run-option-application-args 'nil "*Specify command-line arguments to pass to the application.\nThe JDE passes the specified arguments to the application on\nthe command line." :group 'jde-run-options :type '(repeat (string :tag "Argument")))
(custom-declare-variable 'jde-run-applet-viewer '"appletviewer" "*Specify name of viewer to use to display page containing the applet." :group 'jde-project :type 'string)
(custom-declare-variable 'jde-run-applet-doc '"index.html" "*Specify name of document containing applet to be viewed.\nIf no document is specified, JDE assumes that the document name is\nAPPLET.html, where APPLET is the name of the applet to be viewed. The\ndefault value is index.html." :group 'jde-project :type 'string)
#@69 Converts a string of command-line arguments to a list of arguments.
(defalias 'jde-run-parse-args #[(s) "��\n\"��������\n\"C\"Õ��\n#��h+�" [string-match "[a-zA-Z0-9\\.:=;$%+\\_/-]+" s 0 nil tokens i n append match-string] 6 (#$ . 7789)])
#@69 Converts a list of command-line arguments to a string of arguments.
(defalias 'jde-run-make-arg-string #[(args) "�	G�W�������P	8PT��g+�" ["" args 0 i n str " "] 4 (#$ . 8048)])
#@110 Specifies the Java interpreter used to run Java applications
on non-Windows platforms . The default is java.
(defalias 'jde-run-set-vm #[(vm) "��" [vm jde-run-java-vm] 2 (#$ . 8246) "sEnter name of Java interpreter: "])
#@107 Specifies the Java interpreter used to run Java applications
on Windows platforms . The default is javaw.
(defalias 'jde-run-set-vm-w #[(vm) "��" [vm jde-run-java-vm-w] 2 (#$ . 8475) "sEnter name of Java interpreter: "])
#@51 Specify the name of the application class to run.
(defalias 'jde-run-set-app #[(app) "��" [app jde-run-application-class] 2 (#$ . 8704) "sEnter application class: "])
#@181 Specify arguments to be passed to the Java vm.
This command serves as an alternative to using the JDE Run Options
panel to specify command-line arguments for the Java interpreter.
(defalias 'jde-run-set-args #[(args) "�	!��" [jde-run-parse-args args jde-run-option-vm-args] 2 (#$ . 8880) "sEnter arguments: "])
#@231 Specify the arguments to be passed to the Java application class.
This command provides an alternative to using the JDE Run Options panel
to specify command-line arguments to pass to the application when starting
the application.
(defalias 'jde-run-set-app-args #[(args) "�	!��" [jde-run-parse-args args jde-run-option-application-args] 2 (#$ . 9199) "sEnter arguments: "])
#@77 Sets the viewer to be used to view an applet. The default is 
appletviewer.
(defalias 'jde-run-set-applet-viewer #[(viewer) "��" [viewer jde-run-applet-viewer] 2 (#$ . 9580) "sEnter viewer name: "])
#@150 Specify the doc to be used to view an applet.
This command provides an alternative to using the JDE Options
panel to specifying the applet document.
(defalias 'jde-run-set-applet-doc #[(doc) "�������" [doc "" nil jde-run-applet-doc] 2 (#$ . 9788) "sEnter applet doc name: "])
#@41 Build a classpath from a list of paths.
(defalias 'jde-run-build-classpath-arg #[(path-list) "�	G�W���V��	�QT��i+�" ["" path-list 0 n len classpath jde-classpath-separator] 5 (#$ . 10076)])
#@150 Builds a command-line argument string to pass to the Java vm.
This function builds the string from the values of the JDE
Run Option panel variables.
(defalias 'jde-run-get-vm-args #[nil "���B��B��BE	����	!D������!D@A@�8����C���Ӥ��Ԥ+��G�W��8�@�ARC�)T��\\*@��@�A\"A#!A@\"��\"@�\"A\"A##!䘬��!PC�#昬��#PC�,(@)��)@�)A\"A#*(A@+��+@�+A\"A#,*혬��*PC�,��,PC�,1@?1A@?2�3����2����*6@6A7�8��7���������7PC�*<@8<A�=@7=A@>�=8?�@ =8ցA O�B8��7�C ���>�D k��?�E k��B�F ����G ���ށH 7>?B%C�.I@IA@J�K���L �K��J���M �*N��NG�OOW���P N8!�T��e**�" [nil "bytes" "" "kilobytes" "k" "megabytes" "m" memory-unit-abbrevs options jde-run-option-classpath "-classpath" jde-run-build-classpath-arg jde-global-classpath jde-run-option-verbose 2 print-jni-info print-memory-freed print-classes-loaded "-v" ("-verbosegc") ("-verbosejni") jde-run-option-properties 0 n count prop "-D" "=" jde-run-option-heap-size start-cons format "%d%s" assoc start-size max-cons max-size "1m" "-Xms" "16m" "-Xmx" jde-run-option-stack-size c-cons c-size java-cons java-size "128k" "-Xss" "400k" "-Xoss" jde-run-option-garbage-collection no-gc-classes no-gc-asynch ("-Xnoasyncgc") ("-Xnoclassgc") jde-run-option-java-profile file profilep "./java.prof" ("-Xprof") "-Xprof:" jde-run-option-heap-profile prof-options depth top 3 1 sort "./java.hprof" 5 20 "a" ("-Xhprof") "-Xhprof:file=%s,depth=%d,top=%d,sort=%s" jde-run-option-verify verify-remote verify-all ("-Xverify") ("-Xnoverify") jde-run-option-vm-args len jde-run-parse-args] 8 (#$ . 10287)])
#@159 Runs the Java program named by jde-run-application-class in
a buffer, piping output from the program to the buffer and 
input from the buffer to the program.
(defalias 'jde-run #[nil "���	��� ��� !!P�	!)�" [jde-run-application-class app-class "" jde-db-get-package file-name-sans-extension file-name-nondirectory buffer-file-name jde-run-internal] 5 (#$ . 12021) nil])
#@121 Saves the value of the w32-start-process-show-window variable
before evaluating body and restores the value afterwards.
(defalias 'save-w32-show-window '(macro . #[(&rest body) "���������\"BBB�����\"BBBFBBB�" [if (and (eq system-type 'windows-nt) (not jde-xemacsp)) (boundp 'win32-start-process-show-window) let ((save win32-start-process-show-window)) (setq win32-start-process-show-window t) append body ((setq win32-start-process-show-window save)) ((save w32-start-process-show-window)) (setq w32-start-process-show-window t) ((setq w32-start-process-show-window save))] 11 (#$ . 12402)]))
(defalias 'jde-run-unquote #[(string) "�H�a���GSO��" [string 0 ?\" 1] 3])
(defalias 'jde-run-internal #[(app-class) "�	�Q\nĘ��\n���!�� �!\n�a���Ϙ������ �����ى�%!	C�����ى�%!% �� !�R$�\nq�� ��!���Qc�$c�� �)�a��*����!��,-�,�\n	� %�-,)��0-�0�\n	� %�-0)���\n	� %��\n!��!-����	\"��!+�" ["*" app-class default-directory jde-run-working-directory "" working-directory source-directory run-buf-name comint-check-proc get-buffer-create run-buffer system-type windows-nt win32-p jde-run-java-vm "java" jde-run-java-vm-w prog append jde-run-get-vm-args jde-run-read-vm-args jde-run-parse-args read-from-minibuffer "Vm args: " jde-run-interactive-vm-args nil (jde-run-interactive-vm-arg-history . 1) jde-run-option-application-args jde-run-read-app-args "Application args: " jde-run-interactive-app-args (jde-run-interactive-app-arg-history . 1) prog-args " " jde-run-make-arg-string "\n\n" command-string erase-buffer cd "cd " "\n" jde-run-mode jde-xemacsp boundp win32-start-process-show-window save t comint-exec w32-start-process-show-window pop-to-buffer message "An instance of %s is running."] 13])
#@33 Mode for running Java programs.
(defalias 'jde-run-mode #[nil "� ����!�" [comint-mode jde-run-mode major-mode run-hooks jde-run-mode-hook] 2 (#$ . 14182) nil])
#@217 A version of comint-exec patched to start an applet viewer as
a command shell subprocess rather than as a subprocess of Emacs. This
is necessary to avoid displaying a DOS window when starting a viewer
under Windows.
(defalias 'jde-run-applet-exec #[(buffer name command startfile switches) "�q��!����\n!�)�$�\n�\"���!�db��\n!`Γ���!�*�" [buffer get-buffer-process proc delete-process jde-run-applet-exec-1 name command switches set-process-filter comint-output-filter make-local-variable comint-ptyp process-connection-type process-mark nil run-hooks comint-exec-hook] 6 (#$ . 14350)])
(defalias 'jde-run-applet-exec-1 #[(name buffer command switches) "��!��	������ \"D������ \"D��!?���C���!�������%*�" [boundp system-uses-terminfo "TERM=dumb" format "COLUMNS=%d" frame-width "TERM=emacs" "TERMCAP=emacs:co#%d:tc=unknown:" getenv "EMACS" "EMACS=t" process-environment file-directory-p default-directory "/" apply start-process-shell-command name buffer command switches] 6])
(defalias 'jde-run-applet-internal #[(doc) "��\n!!��Q�!���!	�a�q�� ���Qc��\n�Rc�� �)��\nC%��!+����\"��!*�" [file-name-sans-extension file-name-nondirectory doc doc-name "*" run-buf-name comint-check-proc get-buffer-create run-buffer system-type windows-nt win32-p jde-run-applet-viewer prog erase-buffer "cd " default-directory "\n" " " "\n\n" jde-run-mode jde-run-applet-exec nil pop-to-buffer message "An instance of %s is running." app-class] 6])
#@125 Run an applet, using the viewer specified by jde-run-applet-viewer
and the applet document specified by jde-run-applet-doc.
(defalias 'jde-run-applet #[(&optional doc) "����	��	���!�P���	\"����\nP!)�" [doc jde-run-applet-doc file-name-sans-extension buffer-file-name ".html" applet-doc jde-run-applet-internal string-match "appletviewer" jde-run-applet-viewer default-directory] 4 (#$ . 15845) "sEnter applet document name: "])
(defalias 'jde-run-menu-run-applet #[nil "�	!�" [jde-run-applet jde-run-applet-doc] 2 nil nil])
(provide 'jde-run)

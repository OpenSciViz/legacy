;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tex '("texnfo-tex"))
(custom-add-loads 'texinfo '("texinfo" "texnfo-tex"))
(custom-add-loads 'docs '("makeinfo" "texinfo"))
(custom-add-loads 'makeinfo '("makeinfo"))
(custom-add-loads 'texinfo-tex '("texnfo-tex"))

;;; custom-load.el ends here

1998-07-20  Christoph Wedler  <wedler@fmi.uni-passau.de>

	* tex-mode.el (tex-common-initialization): Use syntax class
	`escape' for \.  Syntax class `character quote' only works for
	`forward-sexp' and friends.

1998-07-14  SL Baur  <steve@altair.xemacs.org>

	* texnfo-tex.el: Global search and replace send-string ->
	process-send-string.

1998-07-01  Adrian Aichner  <aichner@ecf.teradyne.com>

	* texinfmt.el (texinfo-format-region): Removing trailing WS as
	first step in Info file generation.  No support for makeinfo-like
	"asis". 
	(texinfo-format-buffer-1): Ditto.  
	(texinfo-format-convert): Converting three hyphens to two at end
	of line too.

	* informat.el (Info-validate): RE-quoting search for "* Menu".
	Match of "\n\\* Menu;$" ends previous menu and starts new one.
	Improved matching for : and :: menu items.  Adding support for
	xrefs crossing line boundaries by using multi-line argument in
	(Info-extract-menu-node-name STR t).  Reversing lossages before
	printing.  Adding info file name and line number to lossage
	information.  Using compilation-minor-mode for lossages buffer.
	(Info-validate-node-name): Canonicalizing WS in node names to
	avoid unnecessary lossages.  Upgrading lossages list to provide
	filename and line number information to support compilation minor
	mode.  
	(Info-file-data): New function returning string including file
	name and line number.

1998-06-10  Adrian Aichner  <aichner@ecf.teradyne.com>

	* texinfmt.el: Added `texinfo-format-uref'.  Matching only files
	registered for texinfo-mode in auto-mode-alist.  This also fixes
	infinite recursion of this function when called on directories NB:
	(directory-files ...) returns "." and ".." as well.  Using
	(set-syntax-table texinfo-format-syntax-table) in
	texinfo-multitable-buffer-name so that scan-lists will do that
	according to the proper syntax.

1998-04-07  SL Baur  <steve@altair.xemacs.org>

	* Makefile (srckit): Use TAR/EXCLUDES variables from XEmacs.rules.

1998-03-03  SL Baur  <steve@altair.xemacs.org>

	* texinfmt.el: Synch with FSF Emacs 20.2.

1998-01-24  SL Baur  <steve@altair.xemacs.org>

	* Makefile (VERSION): Update to package standard 1.0.
	* package-info.in: Ditto.

1998-01-12  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Update to newer package interface.

1998-01-03  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Update to newer package interface.

1997-12-14  Hrvoje Niksic  <hniksic@srce.hr>

	* texnfo-upd.el: Synched with FSF 20.2.

	* texinfo.el: Synched with FSF 20.2.

	* texinfo.el (texinfo-mode-syntax-table): `@' is a
	symbol-constituent, not an escape.

1997-12-30  SL Baur  <steve@altair.xemacs.org>

	* Makefile (ELCS): Add informat.el.

1997-12-24  SL Baur  <steve@altair.xemacs.org>

	* Makefile: Created.

;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'holidays '("calendar"))
(custom-add-loads 'calendar-tex '("cal-tex"))
(custom-add-loads 'calendar '("calendar" "solar"))
(custom-add-loads 'applications '("calendar"))
(custom-add-loads 'local '("calendar"))
(custom-add-loads 'calendar-hooks '("calendar"))
(custom-add-loads 'diary '("calendar" "diary-lib"))
(custom-add-loads 'chinese-calendar '("cal-china"))
(custom-add-loads 'appt '("auto-autoloads" "appt"))

;;; custom-load.el ends here

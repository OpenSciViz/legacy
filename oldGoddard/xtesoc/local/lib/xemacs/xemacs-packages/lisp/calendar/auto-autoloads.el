;;; DO NOT MODIFY THIS FILE
(if (featurep 'calendar-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "calendar/_pkg.el")

(package-provide 'calendar :version 1.12 :type 'regular)

;;;***

;;;### (autoloads (appt-make-list appt-delete appt-add) "appt" "calendar/appt.el")

(defcustom appt-issue-message t "*Non-nil means check for appointments in the diary buffer.\nTo be detected, the diary entry must have the time\nas the first thing on a line." :type 'boolean :group 'appt)

(defcustom appt-audible t "*Controls whether appointment announcements should beep.\nAppt uses two sound-types for beeps: `appt' and `appt-final'.\nIf this is a number, then that many beeps will occur.\nIf this is a cons, the car is how many beeps, and the cdr is the\n  delay between them (a float, fraction of a second to sleep.)\nSee also the variable `appt-msg-countdown-list'" :type 'boolean :group 'appt)

(defcustom appt-display-mode-line t "*Non-nil means display minutes to appointment and time on the mode line." :type 'boolean :group 'appt)

(autoload 'appt-add "appt" "\
Add an appointment for the day at TIME and issue MESSAGE.
The time should be in either 24 hour format or am/pm format." t nil)

(autoload 'appt-delete "appt" "\
Delete an appointment from the list of appointments." t nil)

(autoload 'appt-make-list "appt" "\
Don't call this directly; call appt-initialize or appt-diary-entries." nil nil)

;;;***

;;;### (autoloads nil "cal-dst" "calendar/cal-dst.el")

(put 'calendar-daylight-savings-starts 'risky-local-variable t)

(put 'calendar-daylight-savings-ends 'risky-local-variable t)

;;;***

;;;### (autoloads (list-yahrzeit-dates) "cal-hebrew" "calendar/cal-hebrew.el")

(autoload 'list-yahrzeit-dates "cal-hebrew" "\
List Yahrzeit dates for *Gregorian* DEATH-DATE from START-YEAR to END-YEAR.
When called interactively from the calendar window, the date of death is taken
from the cursor position." t nil)

;;;***

;;;### (autoloads (calendar) "calendar" "calendar/calendar.el")

(defcustom calendar-week-start-day 0 "*The day of the week on which a week in the calendar begins.\n0 means Sunday (default), 1 means Monday, and so on." :type 'integer :group 'calendar)

(defcustom calendar-offset 0 "*The offset of the principal month from the center of the calendar window.\n0 means the principal month is in the center (default), -1 means on the left,\n+1 means on the right.  Larger (or smaller) values push the principal month off\nthe screen." :type 'integer :group 'calendar)

(defcustom view-diary-entries-initially nil "*Non-nil means display current date's diary entries on entry.\nThe diary is displayed in another window when the calendar is first displayed,\nif the current date is visible.  The number of days of diary entries displayed\nis governed by the variable `number-of-diary-entries'." :type 'boolean :group 'diary)

(defcustom number-of-diary-entries 1 "*Specifies how many days of diary entries are to be displayed initially.\nThis variable affects the diary display when the command M-x diary is used,\nor if the value of the variable `view-diary-entries-initially' is t.  For\nexample, if the default value 1 is used, then only the current day's diary\nentries will be displayed.  If the value 2 is used, then both the current\nday's and the next day's entries will be displayed.\n\nThe value can also be a vector such as [0 2 2 2 2 4 1]; this value\nsays to display no diary entries on Sunday, the display the entries\nfor the current date and the day after on Monday through Thursday,\ndisplay Friday through Monday's entries on Friday, and display only\nSaturday's entries on Saturday.\n\nThis variable does not affect the diary display with the `d' command\nfrom the calendar; in that case, the prefix argument controls the\nnumber of days of diary entries displayed." :type '(choice (integer :tag "Entries") (vector :value [0 0 0 0 0 0 0] (integer :tag "Sunday") (integer :tag "Monday") (integer :tag "Tuesday") (integer :tag "Wednesday") (integer :tag "Thursday") (integer :tag "Friday") (integer :tag "Saturday"))) :group 'diary)

(defcustom mark-diary-entries-in-calendar nil "*Non-nil means mark dates with diary entries, in the calendar window.\nThe marking symbol is specified by the variable `diary-entry-marker'." :type 'boolean :group 'diary)

(defcustom view-calendar-holidays-initially nil "*Non-nil means display holidays for current three month period on entry.\nThe holidays are displayed in another window when the calendar is first\ndisplayed." :type 'boolean :group 'holidays)

(defcustom mark-holidays-in-calendar nil "*Non-nil means mark dates of holidays in the calendar window.\nThe marking symbol is specified by the variable `calendar-holiday-marker'." :type 'boolean :group 'holidays)

(defcustom all-hebrew-calendar-holidays nil "*If nil, show only major holidays from the Hebrew calendar.\nThis means only those Jewish holidays that appear on secular calendars.\n\nIf t, show all the holidays that would appear in a complete Hebrew calendar." :type 'boolean :group 'holidays)

(defcustom all-christian-calendar-holidays nil "*If nil, show only major holidays from the Christian calendar.\nThis means only those Christian holidays that appear on secular calendars.\n\nIf t, show all the holidays that would appear in a complete Christian\ncalendar." :type 'boolean :group 'holidays)

(defcustom all-islamic-calendar-holidays nil "*If nil, show only major holidays from the Islamic calendar.\nThis means only those Islamic holidays that appear on secular calendars.\n\nIf t, show all the holidays that would appear in a complete Islamic\ncalendar." :type 'boolean :group 'holidays)

(defcustom calendar-load-hook nil "*List of functions to be called after the calendar is first loaded.\nThis is the place to add key bindings to `calendar-mode-map'." :type 'hook :group 'calendar-hooks)

(defcustom initial-calendar-window-hook nil "*List of functions to be called when the calendar window is first opened.\nThe functions invoked are called after the calendar window is opened, but\nonce opened is never called again.  Leaving the calendar with the `q' command\nand reentering it will cause these functions to be called again." :type 'hook :group 'calendar-hooks)

(defcustom today-visible-calendar-hook nil "*List of functions called whenever the current date is visible.\nThis can be used, for example, to replace today's date with asterisks; a\nfunction `calendar-star-date' is included for this purpose:\n    (setq today-visible-calendar-hook 'calendar-star-date)\nIt can also be used to mark the current date with `calendar-today-marker';\na function is also provided for this:\n    (setq today-visible-calendar-hook 'calendar-mark-today)\n\nThe corresponding variable `today-invisible-calendar-hook' is the list of\nfunctions called when the calendar function was called when the current\ndate is not visible in the window.\n\nOther than the use of the provided functions, the changing of any\ncharacters in the calendar buffer by the hooks may cause the failure of the\nfunctions that move by days and weeks." :type 'hook :group 'calendar-hooks)

(defcustom today-invisible-calendar-hook nil "*List of functions called whenever the current date is not visible.\n\nThe corresponding variable `today-visible-calendar-hook' is the list of\nfunctions called when the calendar function was called when the current\ndate is visible in the window.\n\nOther than the use of the provided functions, the changing of any\ncharacters in the calendar buffer by the hooks may cause the failure of the\nfunctions that move by days and weeks." :type 'hook :group 'calendar-hooks)

(defcustom diary-file "~/diary" "*Name of the file in which one's personal diary of dates is kept.\n\nThe file's entries are lines in any of the forms\n\n            MONTH/DAY\n            MONTH/DAY/YEAR\n            MONTHNAME DAY\n            MONTHNAME DAY, YEAR\n            DAYNAME\n\nat the beginning of the line; the remainder of the line is the diary entry\nstring for that date.  MONTH and DAY are one or two digit numbers, YEAR is\na number and may be written in full or abbreviated to the final two digits.\nIf the date does not contain a year, it is generic and applies to any year.\nDAYNAME entries apply to any date on which is on that day of the week.\nMONTHNAME and DAYNAME can be spelled in full, abbreviated to three\ncharacters (with or without a period), capitalized or not.  Any of DAY,\nMONTH, or MONTHNAME, YEAR can be `*' which matches any day, month, or year,\nrespectively.\n\nThe European style (in which the day precedes the month) can be used\ninstead, if you execute `european-calendar' when in the calendar, or set\n`european-calendar-style' to t in your .emacs file.  The European forms are\n\n            DAY/MONTH\n            DAY/MONTH/YEAR\n            DAY MONTHNAME\n            DAY MONTHNAME YEAR\n            DAYNAME\n\nTo revert to the default American style from the European style, execute\n`american-calendar' in the calendar.\n\nA diary entry can be preceded by the character\n`diary-nonmarking-symbol' (ordinarily `&') to make that entry\nnonmarking--that is, it will not be marked on dates in the calendar\nwindow but will appear in a diary window.\n\nMultiline diary entries are made by indenting lines after the first with\neither a TAB or one or more spaces.\n\nLines not in one the above formats are ignored.  Here are some sample diary\nentries (in the default American style):\n\n     12/22/1988 Twentieth wedding anniversary!!\n     &1/1. Happy New Year!\n     10/22 Ruth's birthday.\n     21: Payday\n     Tuesday--weekly meeting with grad students at 10am\n              Supowit, Shen, Bitner, and Kapoor to attend.\n     1/13/89 Friday the thirteenth!!\n     &thu 4pm squash game with Lloyd.\n     mar 16 Dad's birthday\n     April 15, 1989 Income tax due.\n     &* 15 time cards due.\n\nIf the first line of a diary entry consists only of the date or day name with\nno trailing blanks or punctuation, then that line is not displayed in the\ndiary window; only the continuation lines is shown.  For example, the\nsingle diary entry\n\n     02/11/1989\n      Bill Blattner visits Princeton today\n      2pm Cognitive Studies Committee meeting\n      2:30-5:30 Lizzie at Lawrenceville for `Group Initiative'\n      4:00pm Jamie Tappenden\n      7:30pm Dinner at George and Ed's for Alan Ryan\n      7:30-10:00pm dance at Stewart Country Day School\n\nwill appear in the diary window without the date line at the beginning.  This\nfacility allows the diary window to look neater, but can cause confusion if\nused with more than one day's entries displayed.\n\nDiary entries can be based on Lisp sexps.  For example, the diary entry\n\n      %%(diary-block 11 1 1990 11 10 1990) Vacation\n\ncauses the diary entry \"Vacation\" to appear from November 1 through November\n10, 1990.  Other functions available are `diary-float', `diary-anniversary',\n`diary-cyclic', `diary-day-of-year', `diary-iso-date', `diary-french-date',\n`diary-hebrew-date', `diary-islamic-date', `diary-mayan-date',\n`diary-chinese-date', `diary-coptic-date', `diary-ethiopic-date',\n`diary-persian-date', `diary-yahrzeit', `diary-sunrise-sunset',\n`diary-phases-of-moon', `diary-parasha', `diary-omer', `diary-rosh-hodesh',\nand `diary-sabbath-candles'.  See the documentation for the function\n`list-sexp-diary-entries' for more details.\n\nDiary entries based on the Hebrew and/or the Islamic calendar are also\npossible, but because these are somewhat slow, they are ignored\nunless you set the `nongregorian-diary-listing-hook' and the\n`nongregorian-diary-marking-hook' appropriately.  See the documentation\nfor these functions for details.\n\nDiary files can contain directives to include the contents of other files; for\ndetails, see the documentation for the variable `list-diary-entries-hook'." :type 'file :group 'diary)

(defcustom diary-nonmarking-symbol "&" "*Symbol indicating that a diary entry is not to be marked in the calendar." :type 'string :group 'diary)

(defcustom hebrew-diary-entry-symbol "H" "*Symbol indicating a diary entry according to the Hebrew calendar." :type 'string :group 'diary)

(defcustom islamic-diary-entry-symbol "I" "*Symbol indicating a diary entry according to the Islamic calendar." :type 'string :group 'diary)

(defcustom diary-include-string "#include" "*The string indicating inclusion of another file of diary entries.\nSee the documentation for the function `include-other-diary-files'." :type 'string :group 'diary)

(defcustom sexp-diary-entry-symbol "%%" "*The string used to indicate a sexp diary entry in diary-file.\nSee the documentation for the function `list-sexp-diary-entries'." :type 'string :group 'diary)

(defcustom abbreviated-calendar-year t "*Interpret a two-digit year DD in a diary entry as either 19DD or 20DD.\nFor the Gregorian calendar; similarly for the Hebrew and Islamic calendars.\nIf this variable is nil, years must be written in full." :type 'boolean :group 'diary)

(defcustom european-calendar-style nil "*Use the European style of dates in the diary and in any displays.\nIf this variable is t, a date 1/2/1990 would be interpreted as February 1,\n1990.  The accepted European date styles are\n\n            DAY/MONTH\n            DAY/MONTH/YEAR\n            DAY MONTHNAME\n            DAY MONTHNAME YEAR\n            DAYNAME\n\nNames can be capitalized or not, written in full, or abbreviated to three\ncharacters with or without a period." :type 'boolean :group 'diary)

(defcustom american-date-diary-pattern '((month "/" day "[^/0-9]") (month "/" day "/" year "[^0-9]") (monthname " *" day "[^,0-9]") (monthname " *" day ", *" year "[^0-9]") (dayname "\\W")) "*List of pseudo-patterns describing the American patterns of date used.\nSee the documentation of `diary-date-forms' for an explanation." :type '(repeat (choice (cons :tag "Backup" (const backup) (repeat (list :inline t :format "%v" (symbol :tag "Keyword") (choice symbol regexp)))) (repeat (list :inline t :format "%v" (symbol :tag "Keyword") (choice symbol regexp))))) :group 'diary)

(defcustom european-date-diary-pattern '((day "/" month "[^/0-9]") (day "/" month "/" year "[^0-9]") (backup day " *" monthname "\\W+\\<[^*0-9]") (day " *" monthname " *" year "[^0-9]") (dayname "\\W")) "*List of pseudo-patterns describing the European patterns of date used.\nSee the documentation of `diary-date-forms' for an explanation." :type '(repeat (choice (cons :tag "Backup" (const backup) (repeat (list :inline t :format "%v" (symbol :tag "Keyword") (choice symbol regexp)))) (repeat (list :inline t :format "%v" (symbol :tag "Keyword") (choice symbol regexp))))) :group 'diary)

(defcustom european-calendar-display-form '((if dayname (concat dayname ", ")) day " " monthname " " year) "*Pseudo-pattern governing the way a date appears in the European style.\nSee the documentation of calendar-date-display-form for an explanation." :type 'sexp :group 'calendar)

(defcustom american-calendar-display-form '((if dayname (concat dayname ", ")) monthname " " day ", " year) "*Pseudo-pattern governing the way a date appears in the American style.\nSee the documentation of `calendar-date-display-form' for an explanation." :type 'sexp :group 'calendar)

(defcustom print-diary-entries-hook 'lpr-buffer "*List of functions called after a temporary diary buffer is prepared.\nThe buffer shows only the diary entries currently visible in the diary\nbuffer.  The default just does the printing.  Other uses might include, for\nexample, rearranging the lines into order by day and time, saving the buffer\ninstead of deleting it, or changing the function used to do the printing." :type 'hook :group 'diary)

(defcustom list-diary-entries-hook nil "*List of functions called after diary file is culled for relevant entries.\nIt is to be used for diary entries that are not found in the diary file.\n\nA function `include-other-diary-files' is provided for use as the value of\nthis hook.  This function enables you to use shared diary files together\nwith your own.  The files included are specified in the diary file by lines\nof the form\n\n        #include \"filename\"\n\nThis is recursive; that is, #include directives in files thus included are\nobeyed.  You can change the \"#include\" to some other string by changing\nthe variable `diary-include-string'.  When you use `include-other-diary-files'\nas part of the list-diary-entries-hook, you will probably also want to use the\nfunction `mark-included-diary-files' as part of `mark-diary-entries-hook'.\n\nFor example, you could use\n\n     (setq list-diary-entries-hook\n       '(include-other-diary-files sort-diary-entries))\n     (setq diary-display-hook 'fancy-diary-display)\n\nin your `.emacs' file to cause the fancy diary buffer to be displayed with\ndiary entries from various included files, each day's entries sorted into\nlexicographic order." :type 'hook :group 'diary)

(defcustom diary-hook nil "*List of functions called after the display of the diary.\nCan be used for appointment notification." :type 'hook :group 'diary)

(defcustom diary-display-hook nil "*List of functions that handle the display of the diary.\nIf nil (the default), `simple-diary-display' is used.  Use `ignore' for no\ndiary display.\n\nOrdinarily, this just displays the diary buffer (with holidays indicated in\nthe mode line), if there are any relevant entries.  At the time these\nfunctions are called, the variable `diary-entries-list' is a list, in order\nby date, of all relevant diary entries in the form of ((MONTH DAY YEAR)\nSTRING), where string is the diary entry for the given date.  This can be\nused, for example, a different buffer for display (perhaps combined with\nholidays), or produce hard copy output.\n\nA function `fancy-diary-display' is provided as an alternative\nchoice for this hook; this function prepares a special noneditable diary\nbuffer with the relevant diary entries that has neat day-by-day arrangement\nwith headings.  The fancy diary buffer will show the holidays unless the\nvariable `holidays-in-diary-buffer' is set to nil.  Ordinarily, the fancy\ndiary buffer will not show days for which there are no diary entries, even\nif that day is a holiday; if you want such days to be shown in the fancy\ndiary buffer, set the variable `diary-list-include-blanks' to t." :type 'hook :group 'diary)

(defcustom nongregorian-diary-listing-hook nil "*List of functions called for listing diary file and included files.\nAs the files are processed for diary entries, these functions are used to cull\nrelevant entries.  You can use either or both of `list-hebrew-diary-entries'\nand `list-islamic-diary-entries'.  The documentation for these functions\ndescribes the style of such diary entries." :type 'hook :group 'diary)

(defcustom mark-diary-entries-hook nil "*List of functions called after marking diary entries in the calendar.\n\nA function `mark-included-diary-files' is also provided for use as the\nmark-diary-entries-hook; it enables you to use shared diary files together\nwith your own.  The files included are specified in the diary file by lines\nof the form\n        #include \"filename\"\nThis is recursive; that is, #include directives in files thus included are\nobeyed.  You can change the \"#include\" to some other string by changing the\nvariable `diary-include-string'.  When you use `mark-included-diary-files' as\npart of the mark-diary-entries-hook, you will probably also want to use the\nfunction `include-other-diary-files' as part of `list-diary-entries-hook'." :type 'hook :group 'diary)

(defcustom nongregorian-diary-marking-hook nil "*List of functions called for marking diary file and included files.\nAs the files are processed for diary entries, these functions are used to cull\nrelevant entries.  You can use either or both of `mark-hebrew-diary-entries'\nand `mark-islamic-diary-entries'.  The documentation for these functions\ndescribes the style of such diary entries." :type 'hook :group 'diary)

(defcustom diary-list-include-blanks nil "*If nil, do not include days with no diary entry in the list of diary entries.\nSuch days will then not be shown in the fancy diary buffer, even if they\nare holidays." :type 'boolean :group 'diary)

(defcustom holidays-in-diary-buffer t "*Non-nil means include holidays in the diary display.\nThe holidays appear in the mode line of the diary buffer, or in the\nfancy diary buffer next to the date.  This slows down the diary functions\nsomewhat; setting it to nil makes the diary display faster." :type 'boolean :group 'holidays)

(put 'general-holidays 'risky-local-variable t)

(defcustom general-holidays '((holiday-fixed 1 1 "New Year's Day") (holiday-float 1 1 3 "Martin Luther King Day") (holiday-fixed 2 2 "Groundhog Day") (holiday-fixed 2 14 "Valentine's Day") (holiday-float 2 1 3 "President's Day") (holiday-fixed 3 17 "St. Patrick's Day") (holiday-fixed 4 1 "April Fools' Day") (holiday-float 5 0 2 "Mother's Day") (holiday-float 5 1 -1 "Memorial Day") (holiday-fixed 6 14 "Flag Day") (holiday-float 6 0 3 "Father's Day") (holiday-fixed 7 4 "Independence Day") (holiday-float 9 1 1 "Labor Day") (holiday-float 10 1 2 "Columbus Day") (holiday-fixed 10 31 "Halloween") (holiday-fixed 11 11 "Veteran's Day") (holiday-float 11 4 4 "Thanksgiving")) "*General holidays.  Default value is for the United States.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'oriental-holidays 'risky-local-variable t)

(defcustom oriental-holidays '((if (fboundp 'atan) (holiday-chinese-new-year))) "*Oriental holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'local-holidays 'risky-local-variable t)

(defcustom local-holidays nil "*Local holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'local :group 'holidays)

(put 'other-holidays 'risky-local-variable t)

(defcustom other-holidays nil "*User defined holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'hebrew-holidays-1 'risky-local-variable t)

(defvar hebrew-holidays-1 '((holiday-rosh-hashanah-etc) (if all-hebrew-calendar-holidays (holiday-julian 11 (let* ((m displayed-month) (y displayed-year) (year)) (increment-calendar-month m y -1) (let ((year (extract-calendar-year (calendar-julian-from-absolute (calendar-absolute-from-gregorian (list m 1 y)))))) (if (zerop (% (1+ year) 4)) 22 21))) "\"Tal Umatar\" (evening)"))))

(put 'hebrew-holidays-2 'risky-local-variable t)

(defvar hebrew-holidays-2 '((if all-hebrew-calendar-holidays (holiday-hanukkah) (holiday-hebrew 9 25 "Hanukkah")) (if all-hebrew-calendar-holidays (holiday-hebrew 10 (let ((h-year (extract-calendar-year (calendar-hebrew-from-absolute (calendar-absolute-from-gregorian (list displayed-month 28 displayed-year)))))) (if (= (% (calendar-absolute-from-hebrew (list 10 10 h-year)) 7) 6) 11 10)) "Tzom Teveth")) (if all-hebrew-calendar-holidays (holiday-hebrew 11 15 "Tu B'Shevat"))))

(put 'hebrew-holidays-3 'risky-local-variable t)

(defvar hebrew-holidays-3 '((if all-hebrew-calendar-holidays (holiday-hebrew 11 (let ((m displayed-month) (y displayed-year)) (increment-calendar-month m y 1) (let* ((h-year (extract-calendar-year (calendar-hebrew-from-absolute (calendar-absolute-from-gregorian (list m (calendar-last-day-of-month m y) y))))) (s-s (calendar-hebrew-from-absolute (if (= (% (calendar-absolute-from-hebrew (list 7 1 h-year)) 7) 6) (calendar-dayname-on-or-before 6 (calendar-absolute-from-hebrew (list 11 17 h-year))) (calendar-dayname-on-or-before 6 (calendar-absolute-from-hebrew (list 11 16 h-year)))))) (day (extract-calendar-day s-s))) day)) "Shabbat Shirah"))))

(put 'hebrew-holidays-4 'risky-local-variable t)

(defvar hebrew-holidays-4 '((holiday-passover-etc) (if (and all-hebrew-calendar-holidays (let* ((m displayed-month) (y displayed-year) (year)) (increment-calendar-month m y -1) (let ((year (extract-calendar-year (calendar-julian-from-absolute (calendar-absolute-from-gregorian (list m 1 y)))))) (= 21 (% year 28))))) (holiday-julian 3 26 "Kiddush HaHamah")) (if all-hebrew-calendar-holidays (holiday-tisha-b-av-etc))))

(put 'hebrew-holidays 'risky-local-variable t)

(defcustom hebrew-holidays (append hebrew-holidays-1 hebrew-holidays-2 hebrew-holidays-3 hebrew-holidays-4) "*Jewish holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'christian-holidays 'risky-local-variable t)

(defcustom christian-holidays '((if all-christian-calendar-holidays (holiday-fixed 1 6 "Epiphany")) (holiday-easter-etc) (if all-christian-calendar-holidays (holiday-greek-orthodox-easter)) (if all-christian-calendar-holidays (holiday-fixed 8 15 "Assumption")) (if all-christian-calendar-holidays (holiday-advent)) (holiday-fixed 12 25 "Christmas") (if all-christian-calendar-holidays (holiday-julian 12 25 "Eastern Orthodox Christmas"))) "*Christian holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'islamic-holidays 'risky-local-variable t)

(defcustom islamic-holidays '((holiday-islamic 1 1 (format "Islamic New Year %d" (let ((m displayed-month) (y displayed-year)) (increment-calendar-month m y 1) (extract-calendar-year (calendar-islamic-from-absolute (calendar-absolute-from-gregorian (list m (calendar-last-day-of-month m y) y))))))) (if all-islamic-calendar-holidays (holiday-islamic 1 10 "Ashura")) (if all-islamic-calendar-holidays (holiday-islamic 3 12 "Mulad-al-Nabi")) (if all-islamic-calendar-holidays (holiday-islamic 7 26 "Shab-e-Mi'raj")) (if all-islamic-calendar-holidays (holiday-islamic 8 15 "Shab-e-Bara't")) (holiday-islamic 9 1 "Ramadan Begins") (if all-islamic-calendar-holidays (holiday-islamic 9 27 "Shab-e Qadr")) (if all-islamic-calendar-holidays (holiday-islamic 10 1 "Id-al-Fitr")) (if all-islamic-calendar-holidays (holiday-islamic 12 10 "Id-al-Adha"))) "*Islamic holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'solar-holidays 'risky-local-variable t)

(defcustom solar-holidays '((if (fboundp 'atan) (solar-equinoxes-solstices)) (if (progn (require 'cal-dst) t) (funcall 'holiday-sexp calendar-daylight-savings-starts '(format "Daylight Savings Time Begins %s" (if (fboundp 'atan) (solar-time-string (/ calendar-daylight-savings-starts-time (float 60)) calendar-standard-time-zone-name) "")))) (funcall 'holiday-sexp calendar-daylight-savings-ends '(format "Daylight Savings Time Ends %s" (if (fboundp 'atan) (solar-time-string (/ calendar-daylight-savings-ends-time (float 60)) calendar-daylight-time-zone-name) "")))) "*Sun-related holidays.\nSee the documentation for `calendar-holidays' for details." :type 'sexp :group 'holidays)

(put 'calendar-holidays 'risky-local-variable t)

(defvar calendar-setup nil "\
The frame set up of the calendar.
The choices are `one-frame' (calendar and diary together in one separate,
dedicated frame) or `two-frames' (calendar and diary in separate, dedicated
frames); with any other value the current frame is used.")

(autoload 'calendar "calendar" "\
Choose between the one frame, two frame, or basic calendar displays.
The original function `calendar' has been renamed `calendar-basic-setup'." t nil)

;;;***

;;;### (autoloads (diary-mail-entries list-diary-entries diary) "diary-lib" "calendar/diary-lib.el")

(autoload 'diary "diary-lib" "\
Generate the diary window for ARG days starting with the current date.
If no argument is provided, the number of days of diary entries is governed
by the variable `number-of-diary-entries'.  This function is suitable for
execution in a `.emacs' file." t nil)

(autoload 'list-diary-entries "diary-lib" "\
Create and display a buffer containing the relevant lines in diary-file.
The arguments are DATE and NUMBER; the entries selected are those
for NUMBER days starting with date DATE.  The other entries are hidden
using selective display.

Returns a list of all relevant diary entries found, if any, in order by date.
The list entries have the form ((month day year) string specifier) where
\(month day year) is the date of the entry, string is the entry text, and
specifier is the applicability.  If the variable `diary-list-include-blanks'
is t, this list includes a dummy diary entry consisting of the empty string)
for a date with no diary entries.

After the list is prepared, the hooks `nongregorian-diary-listing-hook',
`list-diary-entries-hook', `diary-display-hook', and `diary-hook' are run.
These hooks have the following distinct roles:

    `nongregorian-diary-listing-hook' can cull dates from the diary
        and each included file.  Usually used for Hebrew or Islamic
        diary entries in files.  Applied to *each* file.

    `list-diary-entries-hook' adds or manipulates diary entries from
        external sources.  Used, for example, to include diary entries
        from other files or to sort the diary entries.  Invoked *once* only,
        before the display hook is run.

    `diary-display-hook' does the actual display of information.  If this is
        nil, simple-diary-display will be used.  Use add-hook to set this to
        fancy-diary-display, if desired.  If you want no diary display, use
        add-hook to set this to ignore.

    `diary-hook' is run last.  This can be used for an appointment
        notification function." nil nil)

(autoload 'diary-mail-entries "diary-lib" "\
Send a mail message showing diary entries for next NDAYS days.
If no prefix argument is given, NDAYS is set to `diary-mail-days'.

You can call `diary-mail-entries' every night using an at/cron job.
For example, this script will run the program at 2am daily.  Since
`emacs -batch' does not load your `.emacs' file, you must ensure that
all relevant variables are set, as done here.

#!/bin/sh
# diary-rem.sh -- repeatedly run the Emacs diary-reminder
emacs -batch \\
-eval \"(setq diary-mail-days 3 \\
             european-calendar-style t \\
             diary-mail-addr \\\"user@host.name\\\" )\" \\
-l diary-lib -f diary-mail-entries 
at -f diary-rem.sh 0200 tomorrow

You may have to tweak the syntax of the `at' command to suit your
system.  Alternatively, you can specify a cron entry:
0 1 * * * diary-rem.sh
to run it every morning at 1am." t nil)

;;;***

;;;### (autoloads (check-calendar-holidays list-holidays holidays) "holidays" "calendar/holidays.el")

(autoload 'holidays "holidays" "\
Display the holidays for last month, this month, and next month.
If called with an optional prefix argument, prompts for month and year.

This function is suitable for execution in a .emacs file." t nil)

(autoload 'list-holidays "holidays" "\
Display holidays for years Y1 to Y2 (inclusive).

The optional list of holidays L defaults to `calendar-holidays'.  See the
documentation for that variable for a description of holiday lists.

The optional LABEL is used to label the buffer created." t nil)

(autoload 'check-calendar-holidays "holidays" "\
Check the list of holidays for any that occur on DATE.
The value returned is a list of strings of relevant holiday descriptions.
The holidays are those in the list calendar-holidays." nil nil)

;;;***

;;;### (autoloads (phases-of-moon) "lunar" "calendar/lunar.el")

(autoload 'phases-of-moon "lunar" "\
Display the quarters of the moon for last month, this month, and next month.
If called with an optional prefix argument, prompts for month and year.

This function is suitable for execution in a .emacs file." t nil)

;;;***

;;;### (autoloads (solar-equinoxes-solstices sunrise-sunset) "solar" "calendar/solar.el")

(defcustom calendar-time-display-form '(12-hours ":" minutes am-pm (if time-zone " (") time-zone (if time-zone ")")) "*The pseudo-pattern that governs the way a time of day is formatted.\n\nA pseudo-pattern is a list of expressions that can involve the keywords\n`12-hours', `24-hours', and `minutes',  all numbers in string form,\nand `am-pm' and `time-zone',  both alphabetic strings.\n\nFor example, the form\n\n  '(24-hours \":\" minutes\n    (if time-zone \" (\") time-zone (if time-zone \")\"))\n\nwould give military-style times like `21:07 (UTC)'." :type 'sexp :group 'calendar)

(defcustom calendar-latitude nil "*Latitude of `calendar-location-name' in degrees.\n\nThe value can be either a decimal fraction (one place of accuracy is\nsufficient), + north, - south, such as 40.7 for New York City, or the value\ncan be a vector [degrees minutes north/south] such as [40 50 north] for New\nYork City.\n\nThis variable should be set in `site-start'.el." :type '(choice (const nil) (number :tag "Exact") (vector :value [0 0 north] (integer :tag "Degrees") (integer :tag "Minutes") (choice :tag "Position" (const north) (const south)))) :group 'calendar)

(defcustom calendar-longitude nil "*Longitude of `calendar-location-name' in degrees.\n\nThe value can be either a decimal fraction (one place of accuracy is\nsufficient), + east, - west, such as -73.9 for New York City, or the value\ncan be a vector [degrees minutes east/west] such as [73 55 west] for New\nYork City.\n\nThis variable should be set in `site-start'.el." :type '(choice (const nil) (number :tag "Exact") (vector :value [0 0 west] (integer :tag "Degrees") (integer :tag "Minutes") (choice :tag "Position" (const east) (const west)))) :group 'calendar)

(defcustom calendar-location-name '(let ((float-output-format "%.1f")) (format "%s%s, %s%s" (if (numberp calendar-latitude) (abs calendar-latitude) (+ (aref calendar-latitude 0) (/ (aref calendar-latitude 1) 60.0))) (if (numberp calendar-latitude) (if (> calendar-latitude 0) "N" "S") (if (equal (aref calendar-latitude 2) 'north) "N" "S")) (if (numberp calendar-longitude) (abs calendar-longitude) (+ (aref calendar-longitude 0) (/ (aref calendar-longitude 1) 60.0))) (if (numberp calendar-longitude) (if (> calendar-longitude 0) "E" "W") (if (equal (aref calendar-longitude 2) 'east) "E" "W")))) "*Expression evaluating to name of `calendar-longitude', `calendar-latitude'.\nFor example, \"New York City\".  Default value is just the latitude, longitude\npair.\n\nThis variable should be set in `site-start'.el." :type 'sexp :group 'calendar)

(autoload 'sunrise-sunset "solar" "\
Local time of sunrise and sunset for today.  Accurate to a few seconds.
If called with an optional prefix argument, prompt for date.

If called with an optional double prefix argument, prompt for longitude,
latitude, time zone, and date, and always use standard time.

This function is suitable for execution in a .emacs file." t nil)

(autoload 'solar-equinoxes-solstices "solar" "\
*local* date and time of equinoxes and solstices, if visible in the calendar window.
Requires floating point." nil nil)

;;;***

(provide 'calendar-autoloads)

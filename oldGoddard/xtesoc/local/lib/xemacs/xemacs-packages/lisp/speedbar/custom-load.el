;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tools '("speedbar"))
(custom-add-loads 'speedbar '("speedbar"))
(custom-add-loads 'tags '("speedbar"))
(custom-add-loads 'faces '("speedbar"))
(custom-add-loads 'speedbar-faces '("speedbar"))
(custom-add-loads 'speedbar-vc '("speedbar"))

;;; custom-load.el ends here

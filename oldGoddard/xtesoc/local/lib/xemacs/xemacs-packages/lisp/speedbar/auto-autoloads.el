;;; DO NOT MODIFY THIS FILE
(if (featurep 'speedbar-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "speedbar/_pkg.el")

(package-provide 'speedbar :version 1.13 :type 'regular)

;;;***

;;;### (autoloads (speedbar-get-focus speedbar-frame-mode) "speedbar" "speedbar/speedbar.el")

(defalias 'speedbar 'speedbar-frame-mode)

(autoload 'speedbar-frame-mode "speedbar" "\
Enable or disable speedbar.  Positive ARG means turn on, negative turn off.
nil means toggle.  Once the speedbar frame is activated, a buffer in
`speedbar-mode' will be displayed.  Currently, only one speedbar is
supported at a time.
`speedbar-before-popup-hook' is called before popping up the speedbar frame.
`speedbar-before-delete-hook' is called before the frame is deleted." t nil)

(autoload 'speedbar-get-focus "speedbar" "\
Change frame focus to or from the speedbar frame.
If the selected frame is not speedbar, then speedbar frame is
selected.  If the speedbar frame is active, then select the attached frame." t nil)

;;;***

(provide 'speedbar-autoloads)

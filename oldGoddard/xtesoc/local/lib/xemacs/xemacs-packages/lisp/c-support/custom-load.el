;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tools '("hideshow"))
(custom-add-loads 'outlines '("hideshow"))
(custom-add-loads 'c-macro '("cmacexp"))
(custom-add-loads 'hideshow '("hideshow"))
(custom-add-loads 'c '("cmacexp"))

;;; custom-load.el ends here

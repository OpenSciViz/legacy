;;; DO NOT MODIFY THIS FILE
(if (featurep 'games-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "games/_pkg.el")

(package-provide 'games :version 1.1 :type 'regular)

;;;***

;;;### (autoloads (snake) "snake" "games/snake.el")

(autoload 'snake "snake" "\
Snake

Move the snake around without colliding with its tail or with the
border.

Eating dots causes the snake to get longer.

snake-mode keybindings:
   \\<snake-mode-map>
\\[snake-start-game]	Starts a new game of Snake
\\[snake-end-game]	Terminates the current game
\\[snake-pause-game]	Pauses (or resumes) the current game
\\[snake-move-left]	Makes the snake move left
\\[snake-move-right]	Makes the snake move right
\\[snake-move-up]	Makes the snake move up
\\[snake-move-down]	Makes the snake move down

" t nil)

;;;***

;;;### (autoloads (sokoban) "sokoban" "games/sokoban.el")

(autoload 'sokoban "sokoban" "\
Sokoban

Push the blocks onto the target squares.

sokoban-mode keybindings:
   \\<sokoban-mode-map>
\\[sokoban-start-game]	Starts a new game of Sokoban
\\[sokoban-restart-level]	Restarts the current level
\\[sokoban-goto-level]	Jumps to a specified level
\\[sokoban-move-left]	Move one square to the left
\\[sokoban-move-right]	Move one square to the right
\\[sokoban-move-up]	Move one square up
\\[sokoban-move-down]	Move one square down

" t nil)

;;;***

;;;### (autoloads (tetris) "tetris" "games/tetris.el")

(autoload 'tetris "tetris" "\
Tetris

Shapes drop from the top of the screen, and the user has to move and
rotate the shape to fit in with those at the bottom of the screen so
as to form complete rows.

tetris-mode keybindings:
   \\<tetris-mode-map>
\\[tetris-start-game]	Starts a new game of Tetris
\\[tetris-end-game]	Terminates the current game
\\[tetris-pause-game]	Pauses (or resumes) the current game
\\[tetris-move-left]	Moves the shape one square to the left
\\[tetris-move-right]	Moves the shape one square to the right
\\[tetris-rotate-prev]	Rotates the shape clockwise
\\[tetris-rotate-next]	Rotates the shape anticlockwise
\\[tetris-move-bottom]	Drops the shape to the bottom of the playing area

" t nil)

;;;***

(provide 'games-autoloads)

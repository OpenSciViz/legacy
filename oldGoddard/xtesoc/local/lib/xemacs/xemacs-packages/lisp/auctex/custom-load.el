;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tex '("tex"))
(custom-add-loads 'LaTeX-label '("latex"))
(custom-add-loads 'LaTeX-environment '("latex"))
(custom-add-loads 'LaTeX-macro '("latex"))
(custom-add-loads 'TeX-output '("tex-buf" "tex"))
(custom-add-loads 'LaTeX-math '("latex"))
(custom-add-loads 'TeX-parse '("tex"))
(custom-add-loads 'AUC-TeX '("tex"))
(custom-add-loads 'TeX-command '("tex"))
(custom-add-loads 'TeX-command-name '("tex"))
(custom-add-loads 'TeX-commands '("tex-buf"))
(custom-add-loads 'TeX '("tex"))
(custom-add-loads 'TeX-indentation '("latex" "tex"))
(custom-add-loads 'TeX-macro '("latex" "tex"))
(custom-add-loads 'TeX-quote '("tex"))
(custom-add-loads 'LaTeX-indentation '("latex"))
(custom-add-loads 'LaTeX '("latex" "tex"))
(custom-add-loads 'TeX-file-extension '("tex"))
(custom-add-loads 'TeX-file '("tex"))

;;; custom-load.el ends here

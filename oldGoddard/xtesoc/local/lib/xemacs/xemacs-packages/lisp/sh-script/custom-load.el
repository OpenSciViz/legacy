;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'languages '("sh-script"))
(custom-add-loads 'processes '("executable"))
(custom-add-loads 'executable '("executable"))
(custom-add-loads 'sh '("sh-script"))
(custom-add-loads 'unix '("sh-script"))

;;; custom-load.el ends here

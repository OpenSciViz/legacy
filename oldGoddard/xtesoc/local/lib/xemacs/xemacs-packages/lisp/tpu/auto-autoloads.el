;;; DO NOT MODIFY THIS FILE
(if (featurep 'tpu-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "tpu/_pkg.el")

(package-provide 'tpu :version 1.09 :type 'regular)

;;;***

;;;### (autoloads (tpu-edt-on) "tpu-edt" "tpu/tpu-edt.el")

(fset 'tpu-edt-mode 'tpu-edt-on)

(fset 'tpu-edt 'tpu-edt-on)

(autoload 'tpu-edt-on "tpu-edt" "\
Turn on TPU/edt emulation." t nil)

;;;***

;;;### (autoloads (tpu-set-cursor-bound tpu-set-cursor-free tpu-set-scroll-margins) "tpu-extras" "tpu/tpu-extras.el")

(autoload 'tpu-set-scroll-margins "tpu-extras" "\
Set scroll margins." t nil)

(autoload 'tpu-set-cursor-free "tpu-extras" "\
Allow the cursor to move freely about the screen." t nil)

(autoload 'tpu-set-cursor-bound "tpu-extras" "\
Constrain the cursor to the flow of the text." t nil)

;;;***

(provide 'tpu-autoloads)

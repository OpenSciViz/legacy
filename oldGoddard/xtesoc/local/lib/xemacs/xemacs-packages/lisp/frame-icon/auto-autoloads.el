;;; DO NOT MODIFY THIS FILE
(if (featurep 'frame-icon-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "frame-icon/_pkg.el")

(package-provide 'frame-icon :version 1.07 :type 'regular)

;;;***

(provide 'frame-icon-autoloads)

;;; DO NOT MODIFY THIS FILE
(if (featurep 'strokes-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "strokes/_pkg.el")

(package-provide 'strokes :version 1.05 :type 'regular)

;;;***

;;;### (autoloads (strokes-compose-complex-stroke strokes-decode-buffer strokes-mode strokes-list-strokes strokes-load-user-strokes strokes-help strokes-describe-stroke strokes-do-complex-stroke strokes-do-stroke strokes-read-stroke strokes-global-set-stroke) "strokes" "strokes/strokes.el")

(defcustom strokes-mode nil "Non-nil when `strokes' is globally enabled." :type 'boolean :set (lambda (symbol value) (strokes-mode (or value 0))) :initialize 'custom-initialize-default :require 'strokes :group 'strokes)

(autoload 'strokes-global-set-stroke "strokes" "\
Interactively give STROKE the global binding as COMMAND.
Operated just like `global-set-key', except for strokes.
COMMAND is a symbol naming an interactively-callable function.  STROKE
is a list of sampled positions on the stroke grid as described in the
documentation for the `strokes-define-stroke' function." t nil)

(defalias 'global-set-stroke 'strokes-global-set-stroke)

(autoload 'strokes-read-stroke "strokes" "\
Read a simple stroke (interactively) and return the stroke.
Optional PROMPT in minibuffer displays before and during stroke reading.
This function will display the stroke interactively as it is being
entered in the strokes buffer if the variable
`strokes-use-strokes-buffer' is non-nil.
Optional EVENT is acceptable as the starting event of the stroke" nil nil)

(autoload 'strokes-do-stroke "strokes" "\
Read a simple stroke from the user and then exectute its command.
This must be bound to a mouse event." t nil)

(autoload 'strokes-do-complex-stroke "strokes" "\
Read a complex stroke from the user and then exectute its command.
This must be bound to a mouse event." t nil)

(autoload 'strokes-describe-stroke "strokes" "\
Displays the command which STROKE maps to, reading STROKE interactively." t nil)

(defalias 'describe-stroke 'strokes-describe-stroke)

(autoload 'strokes-help "strokes" "\
Get instructional help on using the the `strokes' package." t nil)

(autoload 'strokes-load-user-strokes "strokes" "\
Load user-defined strokes from file named by `strokes-file'." t nil)

(defalias 'load-user-strokes 'strokes-load-user-strokes)

(autoload 'strokes-list-strokes "strokes" "\
Pop up a buffer containing an alphabetical listing of strokes in STROKES-MAP.
With CHRONOLOGICAL prefix arg (\\[universal-argument]) list strokes
chronologically by command name.
If STROKES-MAP is not given, `strokes-global-map' will be used instead." t nil)

(defalias 'list-strokes 'strokes-list-strokes)

(autoload 'strokes-mode "strokes" "\
Toggle strokes being enabled.
With ARG, turn strokes on if and only if ARG is positive or true.
Note that `strokes-mode' is a global mode.  Think of it as a minor
mode in all buffers when activated.
By default, strokes are invoked with mouse button-2.  You can define
new strokes with

> M-x global-set-stroke

To use strokes for pictographic editing, such as Chinese/Japanese, use
Sh-button-2, which draws strokes and inserts them.  Encode/decode your
strokes with

> M-x strokes-encode-buffer
> M-x strokes-decode-buffer" t nil)

(autoload 'strokes-decode-buffer "strokes" "\
Decode stroke strings in BUFFER and display their corresponding glyphs.
Optional BUFFER defaults to the current buffer.
Optional FORCE non-nil will ignore the buffer's read-only status." t nil)

(autoload 'strokes-compose-complex-stroke "strokes" "\
Read a complex stroke and insert its glyph into the current buffer." t nil)

;;;***

(provide 'strokes-autoloads)

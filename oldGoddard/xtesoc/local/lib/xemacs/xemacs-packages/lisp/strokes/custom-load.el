;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'extensions '("strokes"))
(custom-add-loads 'mouse '("strokes"))
(custom-add-loads 'lisp '("strokes"))
(custom-add-loads 'strokes '("strokes"))

;;; custom-load.el ends here

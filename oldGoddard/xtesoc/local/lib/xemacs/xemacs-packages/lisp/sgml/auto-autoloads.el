;;; DO NOT MODIFY THIS FILE
(if (featurep 'sgml-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "sgml/_pkg.el")

(package-provide 'sgml :version 1.05 :type 'regular)

;;;***

;;;### (autoloads (linuxdoc-sgml-mode) "linuxdoc-sgml" "sgml/linuxdoc-sgml.el")

(autoload 'linuxdoc-sgml-mode "linuxdoc-sgml" "\
Major mode based on SGML mode for editing linuxdoc-sgml documents.
See the documentation on sgml-mode for more info. This mode
understands the linuxdoc-sgml tags." t nil)

;;;***

;;;### (autoloads nil "sgml-mode" "sgml/sgml-mode.el")

;;;***

(provide 'sgml-autoloads)

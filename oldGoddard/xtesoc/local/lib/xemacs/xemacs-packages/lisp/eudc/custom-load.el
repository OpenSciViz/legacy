;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'eudc-bbdb '("eudc-custom-vars"))
(custom-add-loads 'mail '("eudc-custom-vars"))
(custom-add-loads 'comm '("eudc-custom-vars"))
(custom-add-loads 'eudc-ldap '("eudc-custom-vars"))
(custom-add-loads 'eudc '("eudc-custom-vars"))
(custom-add-loads 'eudc-ph '("eudc-custom-vars"))

;;; custom-load.el ends here

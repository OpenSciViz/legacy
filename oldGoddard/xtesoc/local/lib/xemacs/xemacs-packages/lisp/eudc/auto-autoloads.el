;;; DO NOT MODIFY THIS FILE
(if (featurep 'eudc-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "eudc/_pkg.el")

(package-provide 'eudc :version 1.29 :type 'regular)

;;;***

;;;### (autoloads (eudc-display-jpeg-as-button eudc-display-jpeg-inline eudc-display-sound eudc-display-url eudc-display-generic-binary) "eudc-bob" "eudc/eudc-bob.el")

(autoload 'eudc-display-generic-binary "eudc-bob" "\
Display a button for unidentified binary DATA." nil nil)

(autoload 'eudc-display-url "eudc-bob" "\
Display URL and make it clickable." nil nil)

(autoload 'eudc-display-sound "eudc-bob" "\
Display a button to play the sound DATA." nil nil)

(autoload 'eudc-display-jpeg-inline "eudc-bob" "\
Display the JPEG DATA inline at point if possible." nil nil)

(autoload 'eudc-display-jpeg-as-button "eudc-bob" "\
Display a button for the JPEG DATA." nil nil)

;;;***

;;;### (autoloads (eudc-edit-hotlist) "eudc-hotlist" "eudc/eudc-hotlist.el")

(autoload 'eudc-edit-hotlist "eudc-hotlist" "\
Edit the hotlist of directory servers in a specialized buffer" t nil)

;;;***

;;;### (autoloads (eudc-load-eudc eudc-query-form eudc-expand-inline eudc-get-phone eudc-get-email eudc-set-server) "eudc" "eudc/eudc.el")

(autoload 'eudc-set-server "eudc" "\
Set the directory server to SERVER using PROTOCOL.
Unless NO-SAVE is non-nil, the server is saved as the default 
server for future sessions." t nil)

(autoload 'eudc-get-email "eudc" "\
Get the email field of NAME from the directory server." t nil)

(autoload 'eudc-get-phone "eudc" "\
Get the phone field of NAME from the directory server." t nil)

(autoload 'eudc-expand-inline "eudc" "\
Query the directory server, and expand the query string before point.
The query string consists of the buffer substring from the point back to
the preceding comma, colon or beginning of line.  
The variable `eudc-inline-query-format' controls how to associate the 
individual inline query words with directory attribute names.
After querying the server for the given string, the expansion specified by 
`eudc-inline-expansion-format' is inserted in the buffer at point.
If REPLACE is non nil, then this expansion replaces the name in the buffer.
`eudc-expansion-overwrites-query' being non nil inverts the meaning of REPLACE.
Multiple servers can be tried with the same query until one finds a match, 
see `eudc-inline-expansion-servers'" t nil)

(autoload 'eudc-query-form "eudc" "\
Display a form to query the directory server.
If given a non-nil argument the function first queries the server 
for the existing fields and displays a corresponding form." t nil)

(autoload 'eudc-load-eudc "eudc" "\
Load the Emacs Unified Directory Client.
This does nothing except loading eudc by autoload side-effect." t nil)

(let ((menu '("Directory Search" ["Load Hotlist of Servers" eudc-load-eudc t] ["New Server" eudc-set-server t] ["---" nil nil] ["Query with Form" eudc-query-form t] ["Expand Inline Query" eudc-expand-inline t] ["---" nil nil] ["Get Email" eudc-get-email t] ["Get Phone" eudc-get-phone t]))) (if (not (featurep 'eudc-autoloads)) (if (string-match "XEmacs" emacs-version) (if (and (featurep 'menubar) (not (featurep 'infodock))) (add-submenu '("Tools") menu)) (require 'easymenu) (cond ((fboundp 'easy-menu-add-item) (easy-menu-add-item nil '("tools") (easy-menu-create-menu (car menu) (cdr menu)))) ((fboundp 'easy-menu-create-keymaps) (define-key global-map [menu-bar tools eudc] (cons "Directory Search" (easy-menu-create-keymaps "Directory Search" (cdr menu)))))))))

;;;***

(provide 'eudc-autoloads)

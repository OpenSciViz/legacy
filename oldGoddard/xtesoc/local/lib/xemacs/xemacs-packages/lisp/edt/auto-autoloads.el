;;; DO NOT MODIFY THIS FILE
(if (featurep 'edt-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "edt/_pkg.el")

(package-provide 'edt :version 1.08 :type 'regular)

;;;***

;;;### (autoloads (edt-emulation-on) "edt" "edt/edt.el")

(autoload 'edt-emulation-on "edt" "\
Turn on EDT Emulation." t nil)

;;;***

(provide 'edt-autoloads)

;;; DO NOT MODIFY THIS FILE
(if (featurep 'pc-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "pc/_pkg.el")

(package-provide 'pc :version 1.16 :type 'single-file)

;;;***

;;;### (autoloads (fusion-set-mode fusion-mode) "fusion" "pc/fusion.el")

(autoload 'fusion-mode "fusion" "\
Toggle fusion mode on and off.
See also `fusion-set-mode'" t nil)

(autoload 'fusion-set-mode "fusion" "\
Turn fusion mode off when ON is nil, on otherwise
See also `fusion-mode'" nil nil)

;;;***

;;;### (autoloads (pending-delete-mode turn-off-pending-delete turn-on-pending-delete) "pending-del" "pc/pending-del.el")

(defcustom pending-delete-mode nil "Non-nil when Pending Delete mode is enabled. In Pending Delete mode, typed\ntext replaces the selected region. Normally, you shouldn't modify this\nvariable by hand, but use the function `pending-delete-mode' instead. However, \nyou can customize the default value from the options menu (auto delete\nselection)." :type 'boolean :set (lambda (symbol value) (pending-delete-mode (or value 0))) :initialize 'custom-initialize-default :require 'pending-del :group 'keyboard)

(autoload 'turn-on-pending-delete "pending-del" "\
Turn on pending delete minor mode unconditionally." t nil)

(autoload 'turn-off-pending-delete "pending-del" "\
Turn off pending delete minor mode unconditionally." t nil)

(autoload 'pending-delete-mode "pending-del" "\
Toggle Pending Delete minor mode.
When the pending delete is on, typed text replaces the selection.
With a positive argument, turns it on.
With a non-positive argument, turns it off." t nil)

(define-obsolete-function-alias 'pending-delete-on 'turn-on-pending-delete)

(define-obsolete-function-alias 'pending-delete-off 'turn-off-pending-delete)

(define-compatible-function-alias 'delete-selection-mode 'pending-delete-mode)

(defalias 'pending-delete 'pending-delete-mode)

;;;***

;;;### (autoloads (s-region-bind-cua s-region-bind) "s-region" "pc/s-region.el")

(autoload 's-region-bind "s-region" "\
Bind shifted keys from KEYLIST to region marking commands.
Each key in KEYLIST is rebound to deactivate region if the last command
was one of shift marking commands.  Keys with added shift modifier are
bound to start or continue marking a region.  Optional argument KEYLIST
defaults to `s-region-key-list'.  Optional argument MAP specifies keymap
to add binding to, defaulting to global keymap." t nil)

(autoload 's-region-bind-cua "s-region" "\
Bind some of CUA keys in keymap MAP to kill and yank commands.
Optional argument MAP defaults to `global-map'.
New bindings:
  Sh-delete    kill-region
  Sh-insert    yank
  C-insert     copy-region-as-kill
  C-delete     kill-line
  C-backspace  backward-kill-word" t nil)

;;;***

(provide 'pc-autoloads)

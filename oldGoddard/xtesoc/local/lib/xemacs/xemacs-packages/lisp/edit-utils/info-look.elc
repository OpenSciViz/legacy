;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:56:25 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/oa/edit-utils/info-look.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`info-look.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


(require 'info)
#@171 *Symbol of the current buffer's help mode.
Provide help according to the buffer's major mode if value is nil.
Automatically becomes buffer local when set in any fashion.
(defvar info-lookup-mode nil (#$ . -619))
(make-variable-buffer-local 'info-lookup-mode)
#@58 *Non-nil means pop up the Info buffer in another window.
(defvar info-lookup-other-window-flag t (#$ . -884))
#@98 *Face for highlighting looked up help items.
Setting this variable to nil disables highlighting.
(defvar info-lookup-highlight-face 'highlight (#$ . -1000))
#@39 Overlay object used for highlighting.
(defvar info-lookup-highlight-overlay nil (#$ . 1163))
#@34 History of previous input lines.
(defvar info-lookup-history nil (#$ . 1262))
#@1512 *Alist of known help topics.
Cons cells are of the form

    (HELP-TOPIC . VARIABLE)

HELP-TOPIC is the symbol of a help topic.
VARIABLE is a variable storing HELP-TOPIC's public data.
 Value is an alist with elements of the form

    (HELP-MODE REGEXP IGNORE-CASE DOC-SPEC PARSE-RULE OTHER-MODES)

HELP-MODE is a mode's symbol.
REGEXP is a regular expression matching those help items whose
 documentation can be looked up via DOC-SPEC.
IGNORE-CASE is non-nil if help items are case insensitive.
DOC-SPEC is a list of documentation specifications of the form

    (INFO-NODE TRANS-FUNC PREFIX SUFFIX)

INFO-NODE is the name (including file name part) of an Info index.
TRANS-FUNC is a function translating index entries into help items;
 nil means add only those index entries matching REGEXP, a string
 means prepend string to the first word of all index entries.
PREFIX and SUFFIX are parts of a regular expression.  If one of
 them is non-nil then search the help item's Info node for the
 first occurrence of the regular expression `PREFIX ITEM SUFFIX'.
 ITEM will be highlighted with `info-lookup-highlight-face' if this
 variable is not nil.
PARSE-RULE is either the symbol name of a function or a regular
 expression for guessing the default help item at point.  Fuzzy
 regular expressions like "[_a-zA-Z0-9]+" do a better job if
 there are no clear delimiters; do not try to write too complex
 expressions.  PARSE-RULE defaults to REGEXP.
OTHER-MODES is a list of cross references to other help modes.
(defvar info-lookup-alist '((symbol . info-lookup-symbol-alist) (file . info-lookup-file-alist)) (#$ . -1348))
(defalias 'info-lookup->topic-value #[(topic) "�	\n\"AJ�" [assoc topic info-lookup-alist] 3])
(byte-code "��N�s�����\"�����#�" [info-lookup->topic-value byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->mode-value #[(topic mode) "�	\n�\n\"AJ)\"�" [assoc mode topic info-lookup-alist] 5])
(byte-code "��N�s�����\"�����#�" [info-lookup->mode-value byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->regexp #[(topic mode) "	�	�\"AJ)\"*A@�" [topic mode assoc info-lookup-alist] 5])
(byte-code "��N�s�����\"�����#�" [info-lookup->regexp byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->ignore-case #[(topic mode) "�	\n�\n	�	\"AJ)\"*8�" [2 topic mode assoc info-lookup-alist] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->ignore-case byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->doc-spec #[(topic mode) "�	\n�\n	�	\"AJ)\"*8�" [3 topic mode assoc info-lookup-alist] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->doc-spec byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->parse-rule #[(topic mode) "�	\n�\n	�	\"AJ)\"*8�" [4 topic mode assoc info-lookup-alist] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->parse-rule byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->other-modes #[(topic mode) "�	\n�\n	�	\"AJ)\"*8�" [5 topic mode assoc info-lookup-alist] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->other-modes byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
#@485 Cache storing data maintained automatically by the program.
Value is an alist with cons cell of the form

    (HELP-TOPIC . ((HELP-MODE INITIALIZED COMPLETIONS REFER-MODES) ...))

HELP-TOPIC is the symbol of a help topic.
HELP-MODE is a mode's symbol.
INITIALIZED is nil if HELP-MODE is uninitialized, t if
 HELP-MODE is initialized, and `0' means HELP-MODE is
 initialized but void.
COMPLETIONS is an alist of documented help items.
REFER-MODES is a list of other help modes to use.
(defvar info-lookup-cache nil (#$ . 5192))
(defalias 'info-lookup->cache #[(topic) "�	\n\"��	�B\nB�@�" [assoc topic info-lookup-cache nil] 3])
(byte-code "��N�s�����\"�����#�" [info-lookup->cache byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->topic-cache #[(topic) "�\n\"���B\nB�@)A�" [topic assoc info-lookup-cache nil] 3])
(byte-code "��N�s�����\"�����#�" [info-lookup->topic-cache byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->mode-cache #[(topic mode) "�	\n��\n\"��\n�BB�@)A)\"�" [assoc mode topic info-lookup-cache nil] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->mode-cache byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->initialized #[(topic mode) "	�	��\"���BB�@)A)\"*A@�" [topic mode assoc info-lookup-cache nil] 6])
(byte-code "��N�s�����\"�����#�" [info-lookup->initialized byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->completions #[(topic mode) "�	\n\"���	\n\"��	\n�\n	��	\"��	�BB�@)A)\"*8�" [info-lookup->initialized topic mode info-lookup-setup-mode 2 assoc info-lookup-cache nil] 7])
(byte-code "��N�s�����\"�����#�" [info-lookup->completions byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->refer-modes #[(topic mode) "�	\n\"���	\n\"��	\n�\n	��	\"��	�BB�@)A)\"*8�" [info-lookup->initialized topic mode info-lookup-setup-mode 3 assoc info-lookup-cache nil] 7])
(byte-code "��N�s�����\"�����#�" [info-lookup->refer-modes byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(defalias 'info-lookup->all-modes #[(topic mode) "	�	\"���	\"��	�	��	\"��	�BB�@)A)\"*8*B�" [mode topic info-lookup->initialized info-lookup-setup-mode 3 assoc info-lookup-cache nil] 8])
(byte-code "��N�s�����\"�����#�" [info-lookup->all-modes byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
#@125 *Alist of help specifications for symbol names.
See the documentation of the variable `info-lookup-alist' for more details.
(defvar info-lookup-symbol-alist '((autoconf-mode "A[CM]_[_A-Z0-9]+" nil (("(autoconf)Macro Index" "AC_" "^[ 	]+- \\(Macro\\|Variable\\): .*\\<" "\\>") ("(automake)Index" nil "^[ 	]*`" "'")) ignore (m4-mode)) (bison-mode "[:;|]\\|%\\([%{}]\\|[_a-z]+\\)\\|YY[_A-Z]+\\|yy[_a-z]+" nil (("(bison)Index" nil "`" "'")) "[:;|]\\|%\\([%{}]\\|[_a-zA-Z][_a-zA-Z0-9]*\\)" (c-mode)) (c-mode "\\(struct \\|union \\|enum \\)?[_a-zA-Z][_a-zA-Z0-9]*" nil (("(libc)Function Index" nil "^[ 	]+- \\(Function\\|Macro\\): .*\\<" "\\>") ("(libc)Variable Index" nil "^[ 	]+- \\(Variable\\|Macro\\): .*\\<" "\\>") ("(libc)Type Index" nil "^[ 	]+- Data Type: \\<" "\\>") ("(termcap)Var Index" nil "^[ 	]*`" "'")) info-lookup-guess-c-symbol) (m4-mode "[_a-zA-Z][_a-zA-Z0-9]*" nil (("(m4)Macro index")) "[_a-zA-Z0-9]+") (makefile-mode "\\$[^({]\\|\\.[_A-Z]*\\|[_a-zA-Z][_a-zA-Z0-9-]*" nil (("(make)Name Index" nil "^[ 	]*`" "'")) "\\$[^({]\\|\\.[_A-Z]*\\|[_a-zA-Z0-9-]+") (texinfo-mode "@\\([a-zA-Z]+\\|[^a-zA-Z]\\)" nil (("(texinfo)Command and Variable Index" (lambda (item) (if (string-match "^\\([a-zA-Z]+\\|[^a-zA-Z]\\)\\( .*\\)?$" item) (concat "@" (match-string 1 item)))) "`" "'")))) (#$ . -8228))
#@123 *Alist of help specifications for file names.
See the documentation of the variable `info-lookup-alist' for more details.
(defvar info-lookup-file-alist '((c-mode "[_a-zA-Z0-9./+-]+" nil (("(libc)File Index")))) (#$ . -9537))
#@187 Throw away all cached data.
This command is useful if the user wants to start at the beginning without
quitting Emacs, for example, after some Info documents were updated on the
system.
(defalias 'info-lookup-reset #[nil "���" [nil info-lookup-cache] 2 (#$ . 9770) nil])
#@269 Display the documentation of a symbol.
If called interactively, SYMBOL will be read from the mini-buffer.
Prefix argument means unconditionally insert the default symbol name
into the mini-buffer so that it can be edited.
The default symbol is the one found at point.
(defalias 'info-lookup-symbol #[(symbol &optional mode) "��	\n#�" [info-lookup symbol mode] 4 (#$ . 10049) (byte-code "��!�" [info-lookup-interactive-arguments symbol] 2)])
#@266 Display the documentation of a file.
If called interactively, FILE will be read from the mini-buffer.
Prefix argument means unconditionally insert the default file name
into the mini-buffer so that it can be edited.
The default file name is the one found at point.
(defalias 'info-lookup-file #[(file &optional mode) "��	\n#�" [info-lookup file mode] 4 (#$ . 10497) (byte-code "��!�" [info-lookup-interactive-arguments file] 2)])
#@58 Return default value and help mode for help topic TOPIC.
(defalias 'info-lookup-interactive-arguments #[(topic) "	��\n��\"AJ)\"*��	��\n���!�\"���\"�����\n\"���B\nB�\n@)A)\"*8*�\"���\"?�����\"AJ)\"*8*��������#����\"ˉ�&��k����.D�" [topic info-lookup-mode major-mode mode assoc info-lookup-alist info-lookup-change-mode info-lookup->initialized info-lookup-setup-mode 2 info-lookup-cache nil completions info-lookup-guess-default default current-prefix-arg input completion-ignore-case t enable-recursive-minibuffers completing-read format "Describe %s (default %s): " "Describe %s: " info-lookup-history value ""] 8 (#$ . 10933)])
(defalias 'info-lookup-change-mode #[(topic) "��\n�\n\"AJ)\"���\n\"����&�\"A�����\n\"��\n\"����\n#��*�" [mapcar #[(arg) "�	@!	@B�" [symbol-name arg] 2] topic assoc info-lookup-alist completions completing-read format "Use %s help mode: " nil t info-lookup-history mode error "No %s help available" info-lookup->mode-value "No %s help available for `%s'" info-lookup-mode] 7])
#@43 Display the documentation of a help item.
(defalias 'info-lookup #[(topic item mode) "��	��\n�\"����#�����	\"AJ)\"*8*��\n���\n�\"���\"�����\"���BB�@)A)\"*8*\"����\n���#�\"���\"�����\"���BB�@)A)\"*8,B� Ή������ ���݋���!�����@���	\"AJ)\"*8*����@@�@8�@8��ᏈA�XA�,�����!.�" [mode info-lookup-mode major-mode info-lookup->mode-value topic error "No %s help available for `%s'" assoc 2 info-lookup-alist item info-lookup->initialized info-lookup-setup-mode info-lookup-cache nil "Not documented as a %s: %s" "" 3 selected-window suffix prefix node doc-spec found window modes entry info-lookup-other-window-flag info ((info)) switch-to-buffer-other-window "*info*" (byte-code "�	!��A��!������	\n@	�		�		�	\"AJ)\"*8*�eb���!Q!�Ԕb�������!!��Ԕԕ�!���p$���\"�*����#�*χ" [Info-goto-node node Info-menu entry item t found prefix suffix topic modes mode 2 assoc info-lookup-alist nil buffer-read-only case-fold-search re-search-forward regexp-quote 0 window-system info-lookup-highlight-face end start overlayp info-lookup-highlight-overlay move-overlay make-overlay overlay-put face] 6) ((error)) select-window] 9 (#$ . 12050)])
#@41 Initialize the internal data structure.
(defalias 'info-lookup-setup-mode #[(topic mode) "	�	�	��\"���BB�@)A)\"*A@*�� ĉ�ĉ	\n	�	�\"AJ)\"*����	#�����	�	�	�\"AJ)\"*8*\"���	\"����\"\"��	�	��\"���BB�@)A)\"*\nE	\n���\"���BB�@)		B��\"���BB�@)A)B����\n	��-�" [topic mode assoc info-lookup-cache nil 0 refer-modes completions initialized data cell info-lookup-alist message "No %s help available for `%s'" mapcar #[(arg) "�	\n\"���	\n\"�	\n��	��	\"��	�BB�@)A)\"*A@*�a��\n	B�	�" [info-lookup->initialized topic arg info-lookup-setup-mode mode assoc info-lookup-cache nil t refer-modes] 7] 5 info-lookup-make-completions apply append #[(arg) "	�\n\"���\n\"��\n�\n��\"���BB�@)A)\"*8*�" [topic arg mode info-lookup->initialized info-lookup-setup-mode 2 assoc info-lookup-cache nil] 7] t] 9 (#$ . 13384)])
#@47 Create a unique alist from all index entries.
(defalias 'info-lookup-make-completions #[(topic mode) "���" [nil (byte-code "	�	�	�\"AJ)\"*8*�	�	�	�\"AJ)\"*A@*�Qǉ����	\nЋ�.�" [topic mode 3 assoc info-lookup-alist "^\\(" "\\)\\([ 	].*\\)?$" nil result prefix item entry trans node regexp doc-spec ((byte-code "� �	�� 	@@	@A@�a��Ī�	@A@;��	@A@ƪ�	@A@��\n\"��\n!�eb�����#������#����!!���\"AJ)\"*8*��������\"�/BB�#��\n\"�	A�� � �" [info doc-spec node nil #[(arg) "�	\n\"����\n\"�" [string-match regexp arg match-string 1] 3] prefix #[(arg) "��\n\"����\n\"P�" [string-match "^\\([^: 	\n]+\\)" arg prefix match-string 1] 4] trans message "Processing Info node \"%s\"..." Info-goto-node search-forward "\n* Menu:" t re-search-forward "\n\\* \\([^:	\n]*\\):" match-string 1 entry item topic mode 2 assoc info-lookup-alist result "Processing Info node \"%s\"... done" Info-directory] 7))] 9) ((error))] 3 (#$ . 14311)])
#@102 Pick up default item at point (with favor to look back).
Return nil if there is nothing appropriate.
(defalias 'info-lookup-guess-default #[(topic mode) "			�	\"���	\"��	�	��\"���BB�@)A)\"*8,B`ǉ	\n	�����@\"	A\nb��e	������	\"\"�,�" [topic mode info-lookup->initialized info-lookup-setup-mode 3 assoc info-lookup-cache nil whitespace guess start modes info-lookup-guess-default* delete mapcar #[(ch) "�	�\"���	�\"���	�\"��?����	�" [char-equal ch ?\  ?\t ?\n whitespace nil] 3]] 8 (#$ . 15331)])
(defalias 'info-lookup-guess-default* #[(topic mode) "	�	�	�\"AJ)\"*8*	�	�	�\"AJ)\"*8*��	�	�	�\"AJ)\"*A@*`Ɖ��	\n9�� ��:��@	A��	���x�`\n�	��#���	!��Ε\nY���!�c��b���w��	!���!.�" [topic mode 2 assoc info-lookup-alist 4 nil result subexp regexp end start rule case-fold-search 0 " 	\n" re-search-backward t looking-at match-string] 8])
#@28 Get the C symbol at point.
(defalias 'info-lookup-guess-c-symbol #[nil "���" [nil (byte-code "� �`����x�W����x�W����!����!�Pb���!����!��\nΘ��\nϘ��\nИ����!��\n�P��!��\n��\nP+�" [backward-sexp nil name prefix start " 	\n" 0 "_a-zA-Z0-9" looking-at "\\(struct\\|union\\|enum\\)\\s " match-string 1 " " "[_a-zA-Z][_a-zA-Z0-9]*" "struct" "union" "enum" "[a-z]+\\s +\\([_a-zA-Z][_a-zA-Z0-9]*\\)"] 3) ((error))] 3 (#$ . 16294)])
#@47 Perform completion on symbol preceding point.
(defalias 'info-complete-symbol #[(&optional mode) "��\n������\n�\"AJ)\"*��������!\"�" [info-complete symbol mode info-lookup-mode major-mode topic assoc info-lookup-alist info-lookup-change-mode] 7 (#$ . 16738) nil])
#@45 Perform completion on file preceding point.
(defalias 'info-complete-file #[(&optional mode) "��\n\"�" [info-complete file mode] 3 (#$ . 17019) (list (if (info-lookup->mode-value 'file (or info-lookup-mode major-mode)) (or info-lookup-mode major-mode) (info-lookup-change-mode 'file)))])
#@30 Try to complete a help item.
(defalias 'info-complete #[(topic mode) "� �	��\n���	\"����	#�			�	\"���	\"��	�	��\"���BB�@)A)\"*8,B`͉����@A�	\"b��c����\"��	�	\"���	\"��	�	��\"���BB�@)A)\"*8*\"���� ��;��GZ|�c,�" [barf-if-buffer-read-only mode info-lookup-mode major-mode info-lookup->mode-value topic error "No %s completion available for `%s'" info-lookup->initialized info-lookup-setup-mode 3 assoc info-lookup-cache nil completion try start modes info-lookup-guess-default* "Found no %s to complete" try-completion 2 ding] 10 (#$ . 17313)])
(provide 'info-look)

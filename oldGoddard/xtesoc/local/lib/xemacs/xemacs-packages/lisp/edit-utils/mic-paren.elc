;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:56:35 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/oa/edit-utils/mic-paren.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`mic-paren.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


#@389 *Defines the behaviour of mic-paren when point is between a closing and an
  opening parenthesis.

A value of 'close means highlight the parenthesis matching the
close-parenthesis before the point.

A value of 'open means highlight the parenthesis matching the open-parenthesis
after the point.

Any other value means highlight both parenthesis matching the parenthesis
beside the point.
(defvar paren-priority nil (#$ . -603))
#@138 *If nil only the matching parenthesis is highlighted.
If non-nil the whole s-expression between the matching parenthesis is
highlighted.
(defvar paren-sexp-mode nil (#$ . -1038))
#@155 *If non-nil and point is after a close parenthesis, both the close and
open parenthesis is highlighted. If nil, only the open parenthesis is
highlighted.
(defvar paren-highlight-at-point t (#$ . -1224))
#@366 *If non-nil stig-paren will highlight text which is not visible in the
current buffer.  

This is useful if you regularly display the current buffer in multiple windows
or frames. For instance if you use follow-mode (by andersl@csd.uu.se), however
it may slow down your Emacs.

(This variable is ignored (treated as non-nil) if you set paren-sexp-mode to
non-nil.)
(defvar paren-highlight-offscreen nil (#$ . -1434))
#@57 *Display message if matching parenthesis is off-screen.
(defvar paren-message-offscreen t (#$ . -1857))
#@55 *Display message if no matching parenthesis is found.
(defvar paren-message-no-match t (#$ . -1967))
#@158 *Make noise if the cursor is at an unmatched parenthesis or no matching
parenthesis is found.

Even if nil, typing an unmatched parenthesis produces a ding.
(defvar paren-ding-unmatched nil (#$ . -2075))
#@899 *This variable controls when highlighting is done.  The variable has
different meaning in different versions of Emacs.

In Emacs 19.29 and below: 
  This variable is ignored.

In Emacs 19.30:
  A value of nil will make highlighting happen immediately (this may slow down
  your Emacs if running on a slow system).  Any non-nil value will delay
  highlighting for the time specified by post-command-idle-delay.  

In Emacs 19.31 and above:
  A value of nil will make highlighting happen immediately (this may slow down
  your Emacs if running on a slow system).  If not nil, the value should be a
  number (possible a floating point number if your Emacs support floating point
  numbers).  The number is the delay before mic-paren performs highlighting.

If you change this variable when mic-paren is active you have to re-activate
(with M-x paren-activate) mic-paren for the change to take effect.
(defvar paren-delay nil (#$ . -2286))
#@195 *If non-nil mic-paren will not change the value of blink-matching-paren when
activated of deactivated.

If nil mic-paren turns of blinking when activated and turns on blinking when
deactivated.
(defvar paren-dont-touch-blink nil (#$ . -3229))
#@61 *If non-nil mic-paren will not activate itself when loaded.
(defvar paren-dont-activate-on-load t (#$ . -3478))
#@299 *If non-nil mic-paren will not try to load the timer-feature when loaded.

(I have no idea why Emacs user ever want to set this to non-nil but I hate
packages which loads/activates stuff I don't want to use so I provide this way
to prevent the loading if someone doesn't want timers to be loaded.)
(defvar paren-dont-load-timer (byte-code "��\n\"?�" [string-match "XEmacs\\|Lucid" emacs-version] 3) (#$ . -3597))
(byte-code "������%�������%�" [custom-declare-group mic-paren nil "Highlighting of (non)matching parentheses and expressions." :group matching custom-declare-face paren-face ((((class color) (background light)) (:background "lightgoldenrod")) (((class color) (background dark)) (:background "darkolivegreen")) (t (:underline t))) "*Face to use for showing the matching parenthesis."] 6)
#@94 Don't even think of using this.
The variable is obsolete, use the `paren-face' face instead.
(defvar paren-face 'paren-face (#$ . 4403))
(custom-declare-face 'paren-mismatch-face '((((class color) (background light)) (:background "DeepPink")) (((class color) (background dark)) (:background "DeepPink3")) (t (:inverse-video t))) "*Face to use when highlighting a mismatched parenthesis." :group 'mic-paren)
#@103 Don't even think of using this.
The variable is obsolete, use the `paren-mismatch-face' face instead.
(defvar paren-mismatch-face 'paren-mismatch-face (#$ . 4817))
(custom-declare-face 'paren-no-match-face '((((class color) (background light)) (:background "yellow")) (((class color) (background dark)) (:background "yellow4")) (t nil)) "*Face to use when highlighting an unmatched parenthesis." :group 'mic-paren)
#@103 Don't even think of using this.
The variable is obsolete, use the `paren-no-match-face' face instead.
(defvar paren-no-match-face 'paren-no-match-face (#$ . 5239))
(byte-code "��\n\"����M���M���M���M���M�����M���M���M���M���M���" [string-match "\\(Lucid\\|XEmacs\\)" emacs-version mic-make-overlay make-extent mic-delete-overlay delete-extent mic-overlay-put set-extent-property mic-cancel-timer #[(timer) "�	!�" [delete-itimer timer] 2] mic-run-with-idle-timer start-itimer make-overlay delete-overlay overlay-put cancel-timer run-with-idle-timer] 3)
#@411 Activates mic-paren parenthesis highlighting.
paren-activate deactivates the paren.el and stig-paren.el packages if they are
active 
Options:
  paren-priority
  paren-sexp-mode
  paren-highlight-at-point
  paren-highlight-offscreen
  paren-message-offscreen
  paren-message-no-match
  paren-ding-unmatched
  paren-delay
  paren-dont-touch-blink
  paren-dont-activate-on-load
  paren-face
  paren-mismatch-face
(defalias 'paren-activate #[nil "� ���!�����\"����\"���!�����!���!�����!���!�����\"����\"����\"����\"������!��������#�����\"���!����!�������\"����\"���!�����\"���!�" [paren-deactivate boundp post-command-idle-hook remove-hook show-paren-command-hook post-command-hook show-paren-overlay mic-delete-overlay show-paren-overlay-1 stig-paren-command-hook stig-paren-safe-command-hook pre-command-hook stig-paren-delete-overlay paren-dont-touch-blink nil blink-matching-paren featurep timer paren-delay mic-run-with-idle-timer t mic-paren-command-idle-hook mic-paren-idle-timer add-hook mic-paren-command-hook post-command-idle-delay error "Cannot activate mic-paren in this Emacs version"] 4 (#$ . 5798) nil])
#@48 Deactivates mic-paren parenthesis highlighting
(defalias 'paren-deactivate #[nil "��!�����\"����!����\"��	!��\n!��!���͉�" [boundp post-command-idle-hook remove-hook mic-paren-command-idle-hook mic-paren-idle-timer mic-cancel-timer post-command-hook mic-paren-command-hook mic-delete-overlay mic-paren-backw-overlay mic-paren-point-overlay mic-paren-forw-overlay paren-dont-touch-blink t blink-matching-paren] 3 (#$ . 6940) nil])
#@143 Overlay for the open-paren which matches the close-paren before
point. When in sexp-mode this is the overlay for the expression before point.
(defvar mic-paren-backw-overlay (byte-code "�ee\"�" [mic-make-overlay] 3) (#$ . 7387))
#@73 Overlay for the close-paren before point.
(Not used when is sexp-mode.)
(defvar mic-paren-point-overlay (byte-code "�ee\"�" [mic-make-overlay] 3) (#$ . 7622))
#@141 Overlay for the close-paren which matches the open-paren after
point. When in sexp-mode this is the overlay for the expression after point.
(defvar mic-paren-forw-overlay (byte-code "�ee\"�" [mic-make-overlay] 3) (#$ . 7788))
#@77 Idle-timer.  Used only in Emacs 19.31 and above (and if paren-delay is nil)
(defvar mic-paren-idle-timer nil (#$ . 8020))
(defalias 'mic-paren-command-hook #[nil "��� ����ď�" [executing-kbd-macro input-pending-p paren-error (mic-paren-highlight) ((error (byte-code "�� !?����\"�" [window-minibuffer-p selected-window message "mic-paren caught an error (please report): %s" paren-error] 3)))] 3])
(defalias 'mic-paren-command-idle-hook #[nil "���" [paren-error (mic-paren-highlight) ((error (byte-code "�� !?����\"�" [window-minibuffer-p selected-window message "mic-paren caught an error (please report): %s" paren-error] 3)))] 3])
#@88 The main-function of mic-paren. Does all highlighting, dinging, messages,
cleaning-up.
(defalias 'mic-paren-highlight #[nil "�	!��\n!��!�hz�a�gz�a���a���`S!!������e`Z]d}���͏�*�� �h!fa?�!���������`\"����#�����#�����T\"���`S`\"����#����\n�#�����#����\n�#������� !������ߪ���!#���\"��� �*���`S`\"��$#�%���� !����!�%��\"��� �)gz�a��hz�a���a?����`!!���'����ed`\\^}���鏈*'���g!'Sfa?�'!���������`'\"���	�#����	�#����'S'\"���	�#����	�#������� !������ߪ���'!#���\"��� *���``T\"�	�$#�%���� !����!�%��\"��� )�" [mic-delete-overlay mic-paren-forw-overlay mic-paren-point-overlay mic-paren-backw-overlay ?\) ?\( paren-priority open paren-evenp paren-backslashes-before-char nil blink-matching-paren-distance (byte-code "�`�\"��" [scan-sexps -1 open] 3) ((error)) matching-paren pos-visible-in-window-p visible mismatch paren-highlight-offscreen paren-sexp-mode mic-make-overlay mic-overlay-put face paren-mismatch-face paren-face paren-highlight-at-point paren-message-offscreen window-minibuffer-p selected-window message "%s %s" "MISMATCH:" "Matches" mic-paren-get-matching-open-text paren-ding-unmatched ding paren-no-match-face paren-message-no-match "No opening parenthesis found" close (byte-code "�`�\"��" [scan-sexps 1 close] 3) ((error)) mic-paren-get-matching-close-text "No closing parenthesis found"] 5 (#$ . 8664)])
#@54 Returns a string with the context around OPEN-paren.
(defalias 'mic-paren-get-matching-open-text #[(open) "�b����x�n)���y�`T{�P���u���w���`{)�" [open " 	" nil 0 "..." 1 "\n 	"] 2 (#$ . 10131)])
#@55 Returns a string with the context around CLOSE-paren.
(defalias 'mic-paren-get-matching-close-text #[(close) "�b��y�`{b���!��Ī��P)�" [close 0 looking-at "[ 	]*$" "" "..."] 3 (#$ . 10335)])
#@54 Returns t if NUMBER is an even number, nil otherwise
(defalias 'paren-evenp #[(number) "���a�" [number 2 0] 2 (#$ . 10534)])
(defalias 'paren-backslashes-before-char #[(pnt) "S�eY��fz�a��\nTS��l\n)�" [pnt 0 n ?\\] 3])
(byte-code "����!����ŏ���� ����\"���!���!�" [paren-dont-load-timer featurep timer nil (byte-code "��!�" [require timer] 2) ((error)) paren-dont-activate-on-load paren-activate add-hook window-setup-hook #[nil "��	?��� �" [window-system paren-dont-activate-on-load paren-activate] 1] provide mic-paren paren] 3)

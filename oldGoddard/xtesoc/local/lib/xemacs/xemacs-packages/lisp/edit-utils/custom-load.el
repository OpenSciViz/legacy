;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'permanent-buffers '("permanent-buffers"))
(custom-add-loads 'extensions '("iswitchb" "page-ext" "tempo"))
(custom-add-loads 'recent-files-menu '("recent-files"))
(custom-add-loads 'mouse '("avoid" "id-select" "outl-mouse"))
(custom-add-loads 'menu '("recent-files"))
(custom-add-loads 'minibuffer '("detached-minibuf" "icomplete" "savehist"))
(custom-add-loads 'environment '("popper"))
(custom-add-loads 'mic-paren '("mic-paren"))
(custom-add-loads 'man '("man"))
(custom-add-loads 'completion '("completion"))
(custom-add-loads 'tools '("func-menu" "lazy-shot"))
(custom-add-loads 'uniquify '("uniquify"))
(custom-add-loads 'recent-files '("recent-files"))
(custom-add-loads 'dabbrev '("dabbrev"))
(custom-add-loads 'save-place '("saveplace"))
(custom-add-loads 'lisp '("func-menu"))
(custom-add-loads 'applications '("uniquify"))
(custom-add-loads 'outlines '("outl-mouse"))
(custom-add-loads 'paren-matching '("paren"))
(custom-add-loads 'avoid '("avoid"))
(custom-add-loads 'help '("man"))
(custom-add-loads 'winring '("winring"))
(custom-add-loads 'data '("saveplace"))
(custom-add-loads 'backup '("backup-dir"))
(custom-add-loads 'id-select '("id-select"))
(custom-add-loads 'outl-mouse '("outl-mouse"))
(custom-add-loads 'frames '("balloon-help" "desktop" "detached-minibuf" "rsz-minibuf" "winring"))
(custom-add-loads 'bookmarks '("bookmark"))
(custom-add-loads 'desktop '("desktop"))
(custom-add-loads 'abbrev '("dabbrev"))
(custom-add-loads 'lazy-shot '("lazy-shot"))
(custom-add-loads 'icomplete '("icomplete"))
(custom-add-loads 'iswitchb '("iswitchb"))
(custom-add-loads 'fume '("func-menu"))
(custom-add-loads 'blink-cursor '("blink-cursor"))
(custom-add-loads 'files '("after-save-commands" "recent-files" "where-was-i-db"))
(custom-add-loads 'fast-lock '("fast-lock"))
(custom-add-loads 'where-was-i '("where-was-i-db"))
(custom-add-loads 'popper '("popper"))
(custom-add-loads 'tempo '("tempo"))
(custom-add-loads 'detached-minibuf '("detached-minibuf"))
(custom-add-loads 'resize-minibuffer '("rsz-minibuf"))
(custom-add-loads 'balloon-help '("balloon-help"))
(custom-add-loads 'faces '("fast-lock" "lazy-shot"))
(custom-add-loads 'pages '("page-ext"))
(custom-add-loads 'savehist '("savehist"))
(custom-add-loads 'editing '("bookmark" "permanent-buffers"))
(custom-add-loads 'matching '("paren" "completion" "id-select" "mic-paren"))
(custom-add-loads 'c '("func-menu"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'fsf-compat-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "fsf-compat/_pkg.el")

(package-provide 'fsf-compat :version 1.06 :type 'single-file)

;;;***

;;;### (autoloads (thing-at-point bounds-of-thing-at-point forward-thing) "thingatpt" "fsf-compat/thingatpt.el")

(autoload 'forward-thing "thingatpt" "\
Move forward to the end of the next THING." nil nil)

(autoload 'bounds-of-thing-at-point "thingatpt" "\
Determine the start and end buffer locations for the THING at point.
THING is a symbol which specifies the kind of syntactic entity you want.
Possibilities include `symbol', `list', `sexp', `defun', `filename', `url',
`word', `sentence', `whitespace', `line', `page' and others.

See the file `thingatpt.el' for documentation on how to define
a symbol as a valid THING.

The value is a cons cell (START . END) giving the start and end positions
of the textual entity that was found." nil nil)

(autoload 'thing-at-point "thingatpt" "\
Return the THING at point.
THING is a symbol which specifies the kind of syntactic entity you want.
Possibilities include `symbol', `list', `sexp', `defun', `filename', `url',
`word', `sentence', `whitespace', `line', `page' and others.

See the file `thingatpt.el' for documentation on how to define
a symbol as a valid THING." nil nil)

;;;***

;;;### (autoloads (with-timeout run-with-idle-timer run-with-timer run-at-time) "timer" "fsf-compat/timer.el")

(autoload 'run-at-time "timer" "\
Perform an action after a delay of SECS seconds.
Repeat the action every REPEAT seconds, if REPEAT is non-nil.
TIME should be a string like \"11:23pm\", nil meaning now, a number of seconds
from now, or a value from `encode-time'.
REPEAT may be an integer or floating point number.
The action is to call FUNCTION with arguments ARGS.

This function returns a timer object which you can use in `cancel-timer'." t nil)

(autoload 'run-with-timer "timer" "\
Perform an action after a delay of SECS seconds.
Repeat the action every REPEAT seconds, if REPEAT is non-nil.
SECS and REPEAT may be integers or floating point numbers.
The action is to call FUNCTION with arguments ARGS.

This function returns a timer object which you can use in `cancel-timer'." t nil)

(autoload 'run-with-idle-timer "timer" "\
Perform an action the next time Emacs is idle for SECS seconds.
If REPEAT is non-nil, do this each time Emacs is idle for SECS seconds.
SECS may be an integer or a floating point number.
The action is to call FUNCTION with arguments ARGS.

This function returns a timer object which you can use in `cancel-timer'." t nil)
(put 'with-timeout 'lisp-indent-function 1)

(autoload 'with-timeout "timer" "\
Run BODY, but if it doesn't finish in SECONDS seconds, give up.
If we give up, we run the TIMEOUT-FORMS and return the value of the last one.
The call should look like:
 (with-timeout (SECONDS TIMEOUT-FORMS...) BODY...)
The timeout is checked whenever Emacs waits for some kind of external
event (such as keyboard input, input from subprocesses, or a certain time);
if the program loops without waiting in any way, the timeout will not
be detected." nil 'macro)

;;;***

;;;### (autoloads (x-popup-menu) "x-popup-menu" "fsf-compat/x-popup-menu.el")

(autoload 'x-popup-menu "x-popup-menu" "\
Pop up menu for Mouse-2 for selected date in the calendar window." nil nil)

;;;***

(provide 'fsf-compat-autoloads)

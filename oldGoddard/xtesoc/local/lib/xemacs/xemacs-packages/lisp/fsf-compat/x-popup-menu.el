;;; x-popup-menu.el --- Mimic x-popup-menu in FSF Emacs

;; Copyright (C) 1998 by Free Software Foundation, Inc.

;; Author: Jeff Miller <jmiller@smart.net>
;; Keywords: frames

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;; 02111-1307, USA.

;;; Synched up with: Not in FSF

;;; Commentary:

;;; Code:

;;;###autoload
(defun x-popup-menu  (event menu)
  "Pop up menu for Mouse-2 for selected date in the calendar window."
  (save-excursion
    (let ((title (car menu))
	  ;; try to ignore just a "" string, XEmacs will typically add two
	  ;; horizontal lines after the title.  A "" just adds a third
	  (mb-items (if (string-match "" (car (car (cdr menu))))
			(cdr (car (cdr menu)))
		      ))
	  (selection))
      
      ;; pop up menu & get the selection 
      (setq selection (get-popup-menu-response 
		       (cons title (convert_fsf_popup mb-items)) event)) 

      ;; normally, we'll get a <#event (call-intercatively function)>
      ;; return, but if nothing was selected, we'll have <#event
      ;; (run-hooks menu-no-select-hook.  So, if something is selected,
      ;; return it, other run the hook
      (if (string-match (symbol-name (event-function selection))
			"call-interactively")   
		(setq selection (event-object selection))
	(funcall (event-function selection) (event-object selection))
	))))     


(defun convert_fsf_popup (menu)
  "Convert FSF style menu notation to the XEmacs format."
  ;; map over list, converting cons cells to vectors.  Strings will be
  ;; turned into vectors as well, just with a nil function
  	 (mapcar '(lambda (x) 
		    (cond (;; Solitary string
			   (and (stringp (car x))
				(not (cdr x)))  
			   (vector  (car x) nil))
			  (;; alist -> vector
			   (and (stringp (car x))
				(not (true-list-p  x)))
			   (vector (car x) (cdr x)))
			  (;; submenu
			   (and (stringp (car x))
				(true-list-p (cdr x)))
			   (cons (car x) (convert_fsf_popup (cdr x))))
			  )
		    )
		 menu))

(provide 'x-popup-menu)
;;; x-popup-menu.el ends here

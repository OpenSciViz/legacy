;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'bbdb-record-display '("bbdb"))
(custom-add-loads 'bbdb-utilities-ftp '("bbdb-ftp"))
(custom-add-loads 'bbdb-record-use '("bbdb"))
(custom-add-loads 'bbdb-mua-specific-gnus '("bbdb-gnus" "bbdb"))
(custom-add-loads 'bbdb-mua-specific-gnus-scoring '("bbdb-gnus"))
(custom-add-loads 'mail '("bbdb"))
(custom-add-loads 'bbdb-record-creation '("bbdb"))
(custom-add-loads 'bbdb-saving '("bbdb"))
(custom-add-loads 'bbdb-utilities-server '("bbdb-srv"))
(custom-add-loads 'bbdb-utilities-print '("bbdb-print"))
(custom-add-loads 'bbdb-save '("bbdb"))
(custom-add-loads 'bbdb-hooks '("bbdb"))
(custom-add-loads 'bbdb-phone-dialing '("bbdb-com"))
(custom-add-loads 'bbdb-utilities-finger '("bbdb-com"))
(custom-add-loads 'bbdb-utilities '("bbdb-whois" "bbdb"))
(custom-add-loads 'news '("bbdb"))
(custom-add-loads 'bbdb-utilities-supercite '("bbdb-sc"))
(custom-add-loads 'bbdb '("bbdb-com" "bbdb-hooks" "bbdb-snarf" "bbdb"))
(custom-add-loads 'bbdb-database '("bbdb"))
(custom-add-loads 'bbdb-noticing-records '("bbdb-com" "bbdb-hooks" "bbdb"))
(custom-add-loads 'bbdb-mua-specific '("bbdb"))

;;; custom-load.el ends here

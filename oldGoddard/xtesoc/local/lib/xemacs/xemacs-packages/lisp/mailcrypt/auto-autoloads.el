;;; DO NOT MODIFY THIS FILE
(if (featurep 'mailcrypt-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "mailcrypt/_pkg.el")

(package-provide 'mailcrypt :version 2.02 :type 'regular)

;;;***

;;;### (autoloads (with-expect) "expect" "mailcrypt/expect.el")

(autoload 'with-expect "expect" "\
Set things up for communication with PROGRAM.
FORMS will be evaluated in the normal manner.  To talk to the process,
use `expect' and `expect-send'.  See the manual for full documentation.
This macro returns nil.

If PROGRAM is a string, start that program.  If PROGRAM is a list, use
the first element of that list as the program and the remainder as the
parameters.  If PROGRAM is a process, talk to that process.

PROGRAM will be started up in a new, fresh temporary buffer.  The
buffer will be killed upon completion.  If PROGRAM is a process,
a new buffer won't be created, and the buffer won't be killed upon
completion." nil 'macro)

;;;***

(provide 'mailcrypt-autoloads)

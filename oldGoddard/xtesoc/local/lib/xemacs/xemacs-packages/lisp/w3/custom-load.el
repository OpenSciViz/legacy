;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'url '("url-gw" "url-irc" "url-news" "url-vars" "url"))
(custom-add-loads 'ssl '("ssl"))
(custom-add-loads 'url-cookie '("url-cookie" "url-vars"))
(custom-add-loads 'hypermedia '("url-vars" "w3-cus"))
(custom-add-loads 'w3-advanced '("w3-cus"))
(custom-add-loads 'w3-menus '("w3-cus" "w3-menu"))
(custom-add-loads 'url-gateway '("url-gw"))
(custom-add-loads 'socks '("socks"))
(custom-add-loads 'w3-files '("w3-cus"))
(custom-add-loads 'comm '("ssl"))
(custom-add-loads 'url-cache '("url-cache" "url-vars"))
(custom-add-loads 'w3-printing '("w3-cus"))
(custom-add-loads 'w3-images '("w3-cus" "w3-display"))
(custom-add-loads 'url-history '("url-vars"))
(custom-add-loads 'url-hairy '("url-vars"))
(custom-add-loads 'url-mime '("url-vars"))
(custom-add-loads 'faces '("font"))
(custom-add-loads 'processes '("socks"))
(custom-add-loads 'w3-hooks '("w3-cus"))
(custom-add-loads 'w3 '("url-vars" "w3-cus" "w3-script"))
(custom-add-loads 'url-file '("url-cache" "url-vars"))
(custom-add-loads 'url-news '("url-news"))
(custom-add-loads 'w3-display '("w3-cus"))
(custom-add-loads 'w3-parsing '("w3-cus"))
(custom-add-loads 'i18n '("url-vars"))
(custom-add-loads 'w3-scripting '("w3-script"))

;;; custom-load.el ends here

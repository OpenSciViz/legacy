;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:17:18 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/libs/xemacs-base/macros.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`macros.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


#@238 Assign a name to the last keyboard macro defined.
Argument SYMBOL is the name to define.
The symbol's function definition becomes the keyboard macro string.
Such a "function" cannot be called from Lisp, but it is a valid
editor command.
(defalias 'name-last-kbd-macro #[(symbol) "����!��!��K;���K!����\"�ǘ����!�M�" [last-kbd-macro error "No keyboard macro defined" fboundp symbol vectorp "Function %s is already defined and not a keyboard macro." "" "No command name given"] 3 (#$ . 600) "SName for last kbd macro: "])
#@584 Insert in buffer the definition of kbd macro NAME, as Lisp code.
Optional second arg KEYS means also record the keys it is on
(this is the prefix argument, when calling interactively).

This Lisp code will, when executed, define the kbd macro with the same
definition it has now.  If you say to record the keys, the Lisp code
will also rebind those keys to the macro.  Only global key bindings
are recorded since executing this Lisp code always makes global
bindings.

To save a kbd macro, visit a file of Lisp code such as your `~/.emacs',
use this command, and then save the file.
(defalias 'insert-kbd-macro #[(macroname &optional keys) "��!Ę��� �c����!��\"c�	G�V���c����c��	p\"��c����!����@#c��A��j))�" [nil definition symbol-name macroname "" format-kbd-macro "(setq last-kbd-macro" format "(defalias '%s" 50 " (read-kbd-macro\n" "\n  (read-kbd-macro " prin1 "))\n" keys where-is-internal "(global-set-key %S '%s)\n"] 4 (#$ . 1135) "CInsert kbd macro (name): \nP"])
#@592 Query user during kbd macro execution.
With prefix argument, enters recursive edit,
 reading keyboard commands even within a kbd macro.
 You can give different commands each time the macro executes.
Without prefix argument, asks whether to continue running the macro.
Your options are: \<query-replace-map>
\[act]	Finish this iteration normally and continue with the next.
\[skip]	Skip the rest of this iteration, and start the next.
\[exit]	Stop the macro entirely right now.
\[recenter]	Redisplay the frame, then ask again.
\[edit]	Enter recursive edit; ask again when you exit from that.
(defalias 'kbd-macro-query #[(flag) "��	����!���ŉ� *��� ���!\n�� ŉ��\n\"�� *��!�\"��a������a�������a�������a����!����a��ŉ� �*���a������a��� �ސ���!!��!q�� )��*�! *�" [executing-kbd-macro defining-kbd-macro error "Not defining or executing kbd macro" flag nil recursive-edit t substitute-command-keys "Proceed with macro?\\<query-replace-map> (\\[act], \\[skip], \\[exit], \\[recenter], \\[edit]) " msg loop message "%s" read-char-exclusive def key vector lookup-key query-replace-map act skip "" exit recenter edit quit quit-flag help ding "*Help*" princ "Specify how to proceed with keyboard macro execution.\nPossibilities: \\<query-replace-map>\n\\[act]	Finish this iteration normally and continue with the next.\n\\[skip]	Skip the rest of this iteration, and start the next.\n\\[exit]	Stop the macro entirely right now.\n\\[recenter]	Redisplay the frame, then ask again.\n\\[edit]	Enter recursive edit; ask again when you exit from that." standard-output help-mode] 5 (#$ . 2134) "P"])
#@1314 For each complete line between point and mark, move to the beginning
of the line, and run the last keyboard macro.

When called from lisp, this function takes two arguments TOP and
BOTTOM, describing the current region.  TOP must be before BOTTOM.
The optional third argument MACRO specifies a keyboard macro to
execute.

This is useful for quoting or unquoting included text, adding and
removing comments, or producing tables where the entries are regular.

For example, in Usenet articles, sections of text quoted from another
author are indented, or have each line start with `>'.  To quote a
section of text, define a keyboard macro which inserts `>', put point
and mark at opposite ends of the quoted section, and use
`\[apply-macro-to-region-lines]' to mark the entire section.

Suppose you wanted to build a keyword table in C where each entry
looked like this:

    { "foo", foo_data, foo_function }, 
    { "bar", bar_data, bar_function },
    { "baz", baz_data, baz_function },

You could enter the names in this format:

    foo
    bar
    baz

and write a macro to massage a word into a table entry:

    \C-x (
       \M-d { "\C-y", \C-y_data, \C-y_function },
    \C-x )

and then select the region of un-tablified names and use
`\[apply-macro-to-region-lines]' to build the table from the names.

(defalias 'apply-macro-to-region-lines #[(top bottom &optional macro) "��	����!�	�b��y�� �	\nb�n���y�� 	W��b���y�`Ǔ�)����	!�)�_	ǉ��ǉ�+�" [macro last-kbd-macro error "No keyboard macro has been defined." bottom 0 point-marker nil next-line-marker end-marker top 1 execute-kbd-macro] 3 (#$ . 3778) "r"])
(provide 'macros)

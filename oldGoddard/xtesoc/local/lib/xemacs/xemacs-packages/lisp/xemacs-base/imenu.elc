;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:17:15 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/libs/xemacs-base/imenu.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`imenu.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


(custom-declare-group 'imenu nil "Mode-specific buffer indexes." :group 'matching :group 'frames)
(custom-declare-variable 'imenu-use-markers 't "*Non-nil means use markers instead of integers for Imenu buffer positions.\nSetting this to nil makes Imenu work faster.\n\nThis might not yet be honored by all index-building functions." :type 'boolean :group 'imenu)
(custom-declare-variable 'imenu-max-item-length '60 "*If a number, truncate Imenu entries to that length." :type 'integer :group 'imenu)
(custom-declare-variable 'imenu-auto-rescan 'nil "*Non-nil means Imenu should always rescan the buffers." :type 'boolean :group 'imenu)
(custom-declare-variable 'imenu-auto-rescan-maxout '60000 "*Imenu auto-rescan is disabled in buffers larger than this size.\nThis variable is buffer-local." :type 'integer :group 'imenu)
(custom-declare-variable 'imenu-always-use-completion-buffer-p 'nil "*Set this to non-nil for displaying the index in a completion buffer.\n\n`never' means never automatically display a listing of any kind.\nA value of nil (the default) means display the index as a mouse menu\nif the mouse was used to invoke `imenu'.\nAnother non-nil value means always display the index in a completion buffer." :type '(choice (const :tag "On Mouse" nil) (const :tag "Never" never) (sexp :tag "Always" :format "%t\n" t)) :group 'imenu)
(custom-declare-variable 'imenu-sort-function 'nil "*The function to use for sorting the index mouse-menu.\n\nAffects only the mouse index menu.\n\nSet this to nil if you don't want any sorting (faster).\nThe items in the menu are then presented in the order they were found\nin the buffer.\n\nSet it to `imenu--sort-by-name' if you want alphabetic sorting.\n\nThe function should take two arguments and return T if the first\nelement should come before the second.  The arguments are cons cells;\n(NAME . POSITION).  Look at `imenu--sort-by-name' for an example." :type 'function :group 'imenu)
(custom-declare-variable 'imenu-max-items '25 "*Maximum number of elements in a mouse menu for Imenu." :type 'integer :group 'imenu)
(custom-declare-variable 'imenu-scanning-message '"Scanning buffer for index (%3d%%)" "*Progress message during the index scanning of the buffer.\nIf non-nil, user gets a message during the scanning of the buffer.\n\nRelevant only if the mode-specific function that creates the buffer\nindex use `imenu-progress-message'." :type 'string :group 'imenu)
(custom-declare-variable 'imenu-space-replacement '"^" "*The replacement string for spaces in index names.\nUsed when presenting the index in a completion-buffer to make the\nnames work as tokens." :type 'string :group 'imenu)
(custom-declare-variable 'imenu-level-separator '":" "*The separator between index names of different levels.\nUsed for making mouse-menu titles and for flattening nested indexes\nwith name concatenation." :type 'string :group 'imenu)
#@1417 The regex pattern to use for creating a buffer index.

If non-nil this pattern is passed to `imenu--generic-function'
to create a buffer index.

The value should be an alist with elements that look like this:
 (MENU-TITLE REGEXP INDEX)
or like this:
 (MENU-TITLE REGEXP INDEX FUNCTION ARGUMENTS...)
with zero or more ARGUMENTS.  The former format creates a simple element in
the index alist when it matches; the latter creates a special element
of the form  (NAME FUNCTION NAME POSITION-MARKER ARGUMENTS...)
with FUNCTION and ARGUMENTS beiong copied from `imenu-generic-expression'.

MENU-TITLE is a string used as the title for the submenu or nil if the
entries are not nested.

REGEXP is a regexp that should match a construct in the buffer that is
to be displayed in the menu; i.e., function or variable definitions,
etc.  It contains a substring which is the name to appear in the
menu.  See the info section on Regexps for more information.

INDEX points to the substring in REGEXP that contains the name (of the
function, variable or type) that is to appear in the menu.

For emacs-lisp-mode for example PATTERN would look like:

'((nil "^\\s-*(def\\(un\\|subst\\|macro\\|advice\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2)
  ("*Vars*" "^\\s-*(def\\(var\\|const\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2)
  ("*Types*" "^\\s-*(def\\(type\\|struct\\|class\\|ine-condition\\)\\s-+\\([-A-Za-z0-9+]+\\)" 2))

The variable is buffer-local.
(defvar imenu-generic-expression nil (#$ . 3488))
(make-variable-buffer-local 'imenu-generic-expression)
#@521 The function to use for creating a buffer index.

It should be a function that takes no arguments and returns an index
of the current buffer as an alist.

Simple elements in the alist look like (INDEX-NAME . INDEX-POSITION).
Special elements look like (INDEX-NAME FUNCTION ARGUMENTS...).
A nested sub-alist element looks like (INDEX-NAME SUB-ALIST).
The function `imenu--subalist-p' tests an element and returns t
 if it is a sub-alist.

This function is called within a `save-excursion'.

The variable is buffer-local.
(defvar imenu-create-index-function 'imenu-default-create-index-function (#$ . 5016))
(make-variable-buffer-local 'imenu-create-index-function)
#@374 Function for finding the next index position.

If `imenu-create-index-function' is set to
`imenu-default-create-index-function', then you must set this variable
to a function that will find the next index, looking backwards in the
file.

The function should leave point at the place to be connected to the
index and it should return nil when it doesn't find another index.
(defvar imenu-prev-index-position-function 'beginning-of-defun (#$ . 5687))
(make-variable-buffer-local 'imenu-prev-index-position-function)
#@138 Function for extracting the index name.

This function is called after the function pointed out by
`imenu-prev-index-position-function'.
(defvar imenu-extract-index-name-function nil (#$ . 6208))
(make-variable-buffer-local 'imenu-extract-index-name-function)
#@135 The default function called when selecting an Imenu item.
The function in this variable is called when selecting a normal index-item.
(defvar imenu-default-goto-function 'imenu-default-goto-function (#$ . 6475))
(make-variable-buffer-local 'imenu-default-goto-function)
(defalias 'imenu--subalist-p #[(item) "A:��A@<��A@@�a?�" [item lambda] 2])
(defalias 'imenu-progress-message '(macro . #[(prevpos &optional relpos reverse) "���������DDC���Ȫ�����EE����BBEEEE�" [and imenu-scanning-message let pos relpos imenu--relative-position reverse if t > + 5 prevpos progn (message imenu-scanning-message pos) setq (pos)] 11]))
(defalias 'imenu-example--name-and-position #[nil "���!�\n��� ��`� �`�{+B�" [forward-sexp -1 imenu-use-markers point-marker end beg] 3])
(defalias 'imenu-example--lisp-extract-index-name #[nil "� ��!����Ǐ*�" [match-data #1=#:match-data ((store-match-data #1#)) looking-at "(def" nil (byte-code "��!���!�`��!�`�{*�" [down-list 1 forward-sexp 2 -1 end beg] 4) ((error))] 3])
(defalias 'imenu-example--create-lisp-index #[nil "�����db�����\"�)� ������!�	�\\V���\"�)� Ў��!�Ҋ��!���!����!�� B����!����!�� B����!����!�`Sf�U����!���!���!�� B����!�� \nB)*�����\"�)���BB���BB\n���\nBB-�" [nil prev-pos index-unknown-alist index-type-alist index-var-alist index-alist imenu-scanning-message 0 pos message beginning-of-defun imenu--relative-position t 5 match-data #1=#:match-data ((store-match-data #1#)) looking-at "(def" down-list 1 "def\\(var\\|const\\|custom\\)" forward-sexp 2 imenu-example--name-and-position "def\\(un\\|subst\\|macro\\|advice\\)" "def\\(type\\|struct\\|class\\|ine-condition\\)" ?\) -1 100 "Variables" "Types" "Syntax-unknown"] 6])
(byte-code "��!���É�Ű�B��" [boundp imenu-example--function-name-regexp-c "^[a-zA-Z0-9]+[ 	]?" "\\([a-zA-Z0-9_*]+[ 	]+\\)?" "\\([*&]+[ 	]*\\)?" "\\([a-zA-Z0-9_*]+\\)[ 	]*(" current-load-list] 5)
(defalias 'imenu-example--create-c-index #[(&optional regexp) "���eb�����\"�)� 	ʎ�����#������!�\n�\\V���\"�)��!���`�\"b�g)	�a�H� B�A*����\"�)�+�" [nil char prev-pos index-alist imenu-scanning-message 0 pos message match-data #1=#:match-data ((store-match-data #1#)) re-search-forward regexp imenu-example--function-name-regexp-c t imenu--relative-position 5 backward-up-list 1 scan-sexps ?\; imenu-example--name-and-position 100] 5])
(byte-code "��\nB��!����\nB��!���!����\nB��!���!����\nB��!���	�\nB��!���\n�\nBŇ" [("*Rescan*" . -99) imenu--rescan-item current-load-list boundp imenu--index-alist nil make-variable-buffer-local imenu--last-menubar-index-alist imenu--history-list imenu--menu-callback-variable imenu--popup-menu-p] 2)
(defalias 'imenu--sort-by-name #[(item1 item2) "@	@��" [item1 item2] 2])
(defalias 'imenu--relative-position #[(&optional reverse) "`� ��	\nZ	�V��\nS	ť�]���\nS�_	�]�*�" [buffer-size total pos reverse 50000 100 1] 3])
(defalias 'imenu--split #[(list n) "������A@BT�U�i�B���^���B�,�" [list nil 0 i sublist result remain n] 5])
(defalias 'imenu--split-menu #[(menulist title) "��s���B�\"����	@!��	@\nB�	@\"	A��i����	\n	:��	�A	@\nB\n�m\n�\n	��*\"GV�����\"\"\n��*B�" [nil tail keep-at-top imenu--rescan-item menulist delq imenu--subalist-p imenu-sort-function sort oldlist res imenu-max-items mapcar #[(menu) "��\n@@\"\nB�" [format "From: %s" menu] 3] imenu--split title] 6])
(defalias 'imenu--split-submenus #[(alist) "��\n\"�" [mapcar #[(elt) ":��@;��A<���A@\"��" [elt imenu--split-menu] 3] alist] 3])
(defalias 'imenu--truncate-items #[(menulist) "��\n\"�" [mapcar #[(item) "A:���A!�\n���@G\nV���@�\nO��" [item imenu--truncate-items imenu-max-item-length 0] 4] menulist] 3])
#@488 Create an index-alist for the definitions in the current buffer.

Simple elements in the alist look like (INDEX-NAME . INDEX-POSITION).
Special elements look like (INDEX-NAME FUNCTION ARGUMENTS...).
A nested sub-alist element looks like (INDEX-NAME SUB-ALIST).
The function `imenu--subalist-p' tests an element and returns t
 if it is a sub-alist.

There is one simple element with negative POSITION; that's intended
as a way for the user to ask to recalculate the buffer's index alist.
(defalias 'imenu--make-index-alist #[(&optional noerror) "��	��	��� V����~� *�!�������!����C\nB�" [imenu--index-alist imenu-auto-rescan buffer-size imenu-auto-rescan-maxout imenu-create-index-function imenu--truncate-items noerror error "No items suitable for an index found in this buffer" nil imenu--rescan-item] 2 (#$ . 10282)])
(byte-code "�	B��" [imenu--cleanup-seen current-load-list] 2)
(defalias 'imenu--cleanup #[(&optional alist) "��	B��\n�C����\"��Ň" [alist imenu--cleanup-seen imenu--index-alist mapcar #[(item) "�	A!��	A��	As���	!���	A!�" [markerp item nil imenu--cleanup-seen imenu--subalist-p imenu--cleanup] 3] t] 4])
#@65 Check whether the string STR is contained in multi-level ALIST.
(defalias 'imenu--in-alist #[(str alist) "�������@�AA@\n<���\n\"��d���`��Y��S	,�" [nil res tail head elt alist imenu--in-alist str] 5 (#$ . 11433)])
#@223 *Wrapper for index searching functions.

Moves point to end of buffer and then repeatedly calls
`imenu-prev-index-position-function' and `imenu-extract-index-name-function'.
Their results are gathered into an index alist.
(defalias 'imenu-default-create-index-function #[nil "�	!���\n!��É�db����	�	\"�	)	 ������!�	�\\V���	\"�	)�\n );�X��� ��`BB�F���	�	\"�	)+����!���!�" [fboundp imenu-prev-index-position-function imenu-extract-index-name-function nil name prev-pos index-alist imenu-scanning-message 0 pos message imenu--relative-position t 5 imenu-use-markers point-marker 100 imenu-generic-expression imenu--generic-function error "This buffer cannot use `imenu-default-create-index-function'"] 4 (#$ . -11676)])
(defalias 'imenu--replace-spaces #[(name replacement) "��\n�#�" [mapconcat #[(ch) "�	�\"����	!�" [char-equal ch ?\  replacement char-to-string] 3] name ""] 4])
(defalias 'imenu--flatten-index-alist #[(index-alist &optional concat-names prefix) "��\n\"�" [mapcan #[(item) "@A����	Q��	�\n!��\n���\nBC���\n\"+�" [item name pos concat-names prefix imenu-level-separator new-prefix markerp imenu--flatten-index-alist] 3] index-alist] 3])
#@1184 Return an index of the current buffer as an alist.

PATTERN is an alist with elements that look like this: (MENU-TITLE
REGEXP INDEX).

MENU-TITLE is a string used as the title for the submenu or nil if the
entries are not nested.

REGEXP is a regexp that should match a construct in the buffer that is
to be displayed in the menu; i.e., function or variable definitions,
etc.  It contains a substring which is the name to appear in the
menu.  See the info section on Regexps for more information.

INDEX points to the substring in REGEXP that contains the name (of the
function, variable or type) that is to appear in the menu.

For emacs-lisp-mode for example PATTERN would look like:

'((nil "^\\s-*(def\\(un\\|subst\\|macro\\|advice\\)\\s-+\\([-A-Za-z0-9]+\\)" 2)
  ("*Vars*" "^\\s-*(def\\(var\\|const\\)\\s-+\\([-A-Za-z0-9]+\\)" 2)
  ("*Types*" "^\\s-*(def\\(type\\|struct\\|class\\|ine-condition\\)\\s-+\\([-A-Za-z0-9]+\\)" 2))'

Returns an index of the current buffer as an alist.  The elements in
the alist look like: (INDEX-NAME . INDEX-POSITION).  They may also be
nested index lists like (INDEX-NAME . INDEX-ALIST) depending on
pattern.

(imenu--generic-function PATTERN).
(defalias 'imenu--generic-function #[(patterns) "�C�����#�Q�	\ndb�����\"�)� Ҏ�	��#������!��\\V���\"�)�\n�͔b���\"�)�K*����\"�)�����\"\"A�-�" [dummy nil "\\(" mapconcat #[(pattern) "A@�" [pattern] 1] patterns "\\)\\|\\(" "\\)" prev-pos global-regexp found index-alist imenu-scanning-message 0 pos message match-data #1=#:match-data ((store-match-data #1#)) re-search-backward t imenu--relative-position 5 mapcar #[(pat) "@A@AA@AAA@AAAA?���!����	��	\"��� 	ϓ	\n��	\nE	���	B)�\"���BB�@�AB�,-�" [pat rest function index regexp menu-title found looking-at end beg t buffer-substring-no-properties name imenu-use-markers make-marker nil #2=#:--arg1--65902 assoc index-alist #3=#:G65900] 6] 100 main-element delq] 8 (#$ . 12893)])
#@129 Let the user select from INDEX-ALIST in a completion buffer with PROMPT.

Returns t for rescan and otherwise a position number.
(defalias 'imenu--completion-buffer #[(index-alist &optional prompt) "����\"�a���\n�������&��΋�;�����@���̪��\"�!���A\n\"��+�" [nil mapcar #[(item) "�	@\n\"	AB�" [imenu--replace-spaces item imenu-space-replacement] 3] index-alist prepared-index-alist choice name imenu-always-use-completion-buffer-p never completing-read prompt "Index item: " t imenu--history-list ((byte-code "�����\"!����������&ȇ" ["*Completions*" display-completion-list all-completions "" prepared-index-alist completing-read prompt "Index item: " nil t imenu--history-list name] 7)) imenu--rescan-item assoc imenu--subalist-p imenu--completion-buffer] 7 (#$ . 14920)])
(defalias 'imenu--create-menu-2 #[(list) "A<���@A\"��@�����DE����DD�#�" [list imenu--create-menu-1 vector imenu--popup-menu-p setq imenu--menu-callback-variable quote imenu--menubar-select t] 6])
(defalias 'imenu--create-menu-1 #[(title list) "����\"\"B�" [title mapcar imenu--create-menu-2 delq nil list] 6])
#@198 Let the user select from a buffer index from a mouse menu.

INDEX-ALIST is the buffer index and EVENT is a mouse event.

Returns t for rescan and otherwise an element or subelement of INDEX-ALIST.
(defalias 'imenu--mouse-menu #[(index-alist event &optional title) "�	!�	��� \"��@�AGW��A��A@A\"��\"���� �!���!��p�!�)*�" [imenu--split-submenus index-alist imenu--split-menu title buffer-name menu t imenu--popup-menu-p imenu--create-menu-1 1 nil imenu--menu-callback-variable popup-menu event misc-user-event-p next-event dispatch-event] 4 (#$ . 16051)])
#@619 Let the user select from a buffer index and return the chosen index.

If the user originally activated this function with the mouse, a mouse
menu is used.  Otherwise a completion buffer is used and the user is
prompted with PROMPT.

If you call this function with index alist ALIST, then it lets the user
select from ALIST.

With no index alist ALIST, it calls `imenu--make-index-alist' to
create the index alist.

If `imenu-always-use-completion-buffer-p' is non-nil, then the
completion buffer is always used, no matter if the mouse was used or
not.

The returned value is of the form (INDEX-NAME . INDEX-POSITION).
(defalias 'imenu-choose-buffer-index #[(&optional prompt alist) "��\n!���\n!���\n!����\n!���\n!�\n���\n!�)�a������� �����\n\"���\"��a�R� �N��I+�" [nil button-press-event-p last-command-event button-release-event-p misc-user-event-p t result mouse-triggered index-alist event-window window select-window alist imenu--make-index-alist imenu-always-use-completion-buffer-p imenu--mouse-menu imenu--completion-buffer prompt imenu--cleanup imenu--index-alist] 4 (#$ . 16636)])
#@158 Adds an `imenu' entry to the menu bar for the current buffer.
NAME is a string used to name the menu bar item.
See the command `imenu' for more information.
(defalias 'imenu-add-to-menubar #[(name) "�	!���\n!�������E!���\n\"�" [fboundp imenu-prev-index-position-function imenu-extract-index-name-function imenu-generic-expression easy-menu-add name :filter imenu-menu-filter error "The mode `%s' does not support Imenu" mode-name] 4 (#$ . 17767) "sImenu menu item name: "])
(defalias 'imenu-menu-filter #[(&rest ignore) "��!�k��\n����\n!�\n� \"�@�AGW��A��A@A\"A*�" [imenu--make-index-alist t index-alist imenu--last-menubar-index-alist imenu--split-submenus imenu--split-menu buffer-name menu imenu--create-menu-1 1] 5])
#@71 Use Imenu to select the function or variable named in this menu item.
(defalias 'imenu--menubar-select #[(item) "	k��� �É��!�" [item imenu--rescan-item imenu--cleanup nil imenu--index-alist imenu] 2 (#$ . 18511)])
#@213 This function is used for moving the point to POSITION. 
The NAME and REST parameters are not used, they are here just to make
this function have the same interface as a function placed in a special 
index-item.
(defalias 'imenu-default-goto-function #[(name position &optional rest) "eW��dV��~�b�" [position] 2 (#$ . 18736)])
#@127 Jump to a place in the buffer chosen using a buffer menu or mouse menu.
See `imenu-choose-buffer-index' for more information.
(defalias 'imenu #[(index-item) ";���� \"��� �A<���AA@����A@��A��AA�@$,�" [index-item assoc imenu--make-index-alist push-mark is-special-item imenu-default-goto-function function position rest apply] 6 (#$ . 19073) (list (imenu-choose-buffer-index))])
(provide 'imenu)

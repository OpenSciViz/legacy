;;; DO NOT MODIFY THIS FILE
(if (featurep 'ilisp-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "ilisp/_pkg.el")

(package-provide 'ilisp :version 1.08 :type 'regular)

;;;***

(provide 'ilisp-autoloads)

;;; DO NOT MODIFY THIS FILE
(if (featurep 'vm-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "vm/_pkg.el")

(package-provide 'vm :version 1.21 :type 'regular)

;;;***

;;;### (autoloads (vm-submit-bug-report vm-mail-other-window vm-mail-other-frame vm-mail vm-visit-virtual-folder-other-window vm-visit-virtual-folder-other-frame vm-visit-virtual-folder vm-visit-folder-other-window vm-visit-folder-other-frame vm-visit-folder vm-mode vm-other-window vm-other-frame vm) "vm-startup" "vm/vm-startup.el")

(autoload 'vm "vm-startup" "\
Read mail under Emacs.
Optional first arg FOLDER specifies the folder to visit.  It defaults
to the value of vm-primary-inbox.  The folder buffer is put into VM
mode, a major mode for reading mail.

Prefix arg or optional second arg READ-ONLY non-nil indicates
that the folder should be considered read only.  No attribute
changes, message additions or deletions will be allowed in the
visited folder.

Visiting the primary inbox causes any contents of the system mailbox to
be moved and appended to the resulting buffer.

All the messages can be read by repeatedly pressing SPC.  Use `n'ext and
`p'revious to move about in the folder.  Messages are marked for
deletion with `d', and saved to another folder with `s'.  Quitting VM
with `q' expunges deleted messages and saves the buffered folder to
disk.

See the documentation for vm-mode for more information." t nil)

(autoload 'vm-other-frame "vm-startup" "\
Like vm, but run in a newly created frame." t nil)

(autoload 'vm-other-window "vm-startup" "\
Like vm, but run in a different window." t nil)

(autoload 'vm-mode "vm-startup" "\
Major mode for reading mail.

This is VM 6.72.

Commands:
   h - summarize folder contents
 C-t - toggle threads display

   n - go to next message
   p - go to previous message
   N - like `n' but ignores skip-variable settings
   P - like `p' but ignores skip-variable settings
 M-n - go to next unread message
 M-p - go to previous unread message
 RET - go to numbered message (uses prefix arg or prompts in minibuffer)
 M-g - go to numbered message (uses prefix arg or prompts in minibuffer)
 TAB - go to last message seen
   ^ - go to parent of this message
 M-s - incremental search through the folder

   t - display hidden headers
 SPC - expose message body or scroll forward a page
   b - scroll backward a page
   < - go to beginning of current message
   > - go to end of current message

   d - delete message, prefix arg deletes messages forward
 C-d - delete message, prefix arg deletes messages backward
   u - undelete
   k - flag for deletion all messages with same subject as the current message

   r - reply (only to the sender of the message)
   R - reply with included text from the current message
 M-r - extract and resend bounced message
   f - followup (reply to all recipients of message)
   F - followup with included text from the current message
   z - forward the current message
   m - send a message
   B - resend the current message to another user.
   c - continue composing the most recent message you were composing

   @ - digestify and mail entire folder contents (the folder is not modified)
   * - burst a digest into individual messages, and append and assimilate these
       messages into the current folder.

   G - sort messages by various keys

   g - get any new mail that has arrived in the system mailbox
       (new mail is appended to the disk and buffer copies of the
       primary inbox.)
   v - visit another mail folder

   e - edit the current message
   j - discard cached information about the current message

   s - save current message in a folder (appends if folder already exists)
   w - write current message to a file without its headers (appends if exists)
   S - save entire folder to disk, does not expunge
   A - save unfiled messages to their vm-auto-folder-alist specified folders
   # - expunge deleted messages (without saving folder)
   q - quit VM, deleted messages are not expunged, folder is
       saved to disk if it is modified.  new messages are changed
       to be flagged as just unread.
   x - exit VM with no change to the folder

 M N - use marks; the next vm command will affect only marked messages
       if it makes sense for the command to do so.  These commands
       apply and remove marks to messages:

       M M - mark the current message
       M U - unmark the current message
       M m - mark all messages
       M u - unmark all messages
       M C - mark messages matched by a virtual folder selector
       M c - unmark messages matched by a virtual folder selector
       M T - mark thread tree rooted at the current message
       M t - unmark thread tree rooted at the current message
       M S - mark messages with the same subject as the current message
       M s - unmark messages with the same subject as the current message
       M A - mark messages with the same author as the current message
       M a - unmark messages with the same author as the current message
       M R - mark messages within the point/mark region in the summary
       M r - unmark messages within the point/mark region in the summary
       M V - toggle the marked-ness of all messages

       M ? - partial help for mark commands

 W S - save the current window configuration to a name
 W D - delete a window configuration
 W W - apply a configuration
 W ? - help for the window configuration commands

 V V - visit a virtual folder (must be defined in vm-virtual-folder-alist)
 V C - create a virtual folder composed of a subset of the
       current folder's messages.
 V A - create a virtual folder containing all the messages in the current
       folder with the same author as the current message.
 V S - create a virtual folder containing all the messages in the current
       folder with the same subject as the current message.
 V X - apply the selectors of a named virtual folder to the
       messages in the current folder and create a virtual folder
       containing the selected messages.
 V M - toggle whether this virtual folder's messages mirror the
       underlying real messages' attributes.
 V ? - help for virtual folder commands

 C-_ - undo, special undo that retracts the most recent
             changes in message attributes and labels.  Expunges,
             message edits, and saves cannot be undone.  C-x u is
             also bound to this command.

   a - set message attributes

 l a - add labels to message
 l d - delete labels from message

   L - reload your VM init file, ~/.vm

   % - change a folder to another type

   ? - help

   ! - run a shell command
   | - run a shell command with the current message as input

 M-C - view conditions under which you may redistribute VM
 M-W - view the details of VM's lack of a warranty

Use M-x vm-submit-bug-report to submit a bug report.

Variables:
   vm-arrived-message-hook
   vm-arrived-messages-hook
   vm-auto-center-summary
   vm-auto-decode-mime-messages
   vm-auto-displayed-mime-content-type-exceptions
   vm-auto-displayed-mime-content-types
   vm-auto-folder-alist
   vm-auto-folder-case-fold-search
   vm-auto-get-new-mail
   vm-auto-next-message
   vm-berkeley-mail-compatibility
   vm-burst-digest-messages-inherit-labels
   vm-check-folder-types
   vm-circular-folders
   vm-confirm-new-folders
   vm-confirm-quit
   vm-convert-folder-types
   vm-crash-box
   vm-crash-box-suffix
   vm-default-From_-folder-type
   vm-default-folder-type
   vm-delete-after-archiving
   vm-delete-after-bursting
   vm-delete-after-saving
   vm-delete-empty-folders
   vm-digest-burst-type
   vm-digest-center-preamble
   vm-digest-preamble-format
   vm-digest-send-type
   vm-display-buffer-hook
   vm-display-using-mime
   vm-edit-message-hook
   vm-fill-paragraphs-containing-long-lines
   vm-flush-interval
   vm-folder-directory
   vm-folder-read-only
   vm-follow-summary-cursor
   vm-forward-message-hook
   vm-forwarded-headers
   vm-forwarding-digest-type
   vm-forwarding-subject-format
   vm-frame-parameter-alist
   vm-frame-per-completion
   vm-frame-per-composition
   vm-frame-per-edit
   vm-frame-per-folder
   vm-frame-per-help
   vm-frame-per-summary
   vm-highlighted-header-face
   vm-highlighted-header-regexp
   vm-honor-page-delimiters
   vm-image-directory
   vm-imap-auto-expunge-alist
   vm-imap-bytes-per-session
   vm-imap-expunge-after-retrieving
   vm-imap-max-message-size
   vm-imap-messages-per-session
   vm-index-file-suffix
   vm-in-reply-to-format
   vm-included-text-attribution-format
   vm-included-text-discard-header-regexp
   vm-included-text-headers
   vm-included-text-prefix
   vm-infer-mime-types
   vm-invisible-header-regexp
   vm-jump-to-new-messages
   vm-jump-to-unread-messages
   vm-keep-crash-boxes
   vm-keep-sent-messages
   vm-mail-check-interval
   vm-mail-header-from
   vm-mail-header-insert-date
   vm-mail-header-insert-message-id
   vm-mail-mode-hook
   vm-make-crash-box-name
   vm-make-spool-file-name
   vm-mime-7bit-composition-charset
   vm-mime-8bit-composition-charset
   vm-mime-8bit-text-transfer-encoding
   vm-mime-alternative-select-method
   vm-mime-attachment-auto-type-alist
   vm-mime-attachment-save-directory
   vm-mime-avoid-folding-content-type
   vm-mime-base64-decoder-program
   vm-mime-base64-decoder-switches
   vm-mime-base64-encoder-program
   vm-mime-base64-encoder-switches
   vm-mime-button-format-alist
   vm-mime-button-face
   vm-mime-charset-font-alist
   vm-mime-default-face-charsets
   vm-mime-digest-discard-header-regexp
   vm-mime-digest-headers
   vm-mime-display-function
   vm-mime-external-content-types-alist
   vm-mime-ignore-mime-version
   vm-mime-internal-content-type-exceptions
   vm-mime-internal-content-types
   vm-mime-max-message-size
   vm-mime-type-converter-alist
   vm-mode-hook
   vm-mosaic-program
   vm-mosaic-program-switches
   vm-move-after-deleting
   vm-move-after-killing
   vm-move-after-undeleting
   vm-move-messages-physically
   vm-mutable-frames
   vm-mutable-windows
   vm-netscape-program
   vm-netscape-program-switches
   vm-pop-auto-expunge-alist
   vm-pop-bytes-per-session
   vm-pop-expunge-after-retrieving
   vm-pop-max-message-size
   vm-pop-md5-program
   vm-pop-messages-per-session
   vm-popup-menu-on-mouse-3
   vm-preferences-file
   vm-preview-lines
   vm-preview-read-messages
   vm-primary-inbox
   vm-quit-hook
   vm-recognize-imap-maildrops
   vm-recognize-pop-maildrops
   vm-reply-hook
   vm-reply-ignored-addresses
   vm-reply-ignored-reply-tos
   vm-reply-subject-prefix
   vm-resend-bounced-discard-header-regexp
   vm-resend-bounced-headers
   vm-resend-bounced-message-hook
   vm-resend-discard-header-regexp
   vm-resend-headers
   vm-resend-message-hook
   vm-retrieved-spooled-mail-hook
   vm-rfc1153-digest-discard-header-regexp
   vm-rfc1153-digest-headers
   vm-rfc934-digest-discard-header-regexp
   vm-rfc934-digest-headers
   vm-search-using-regexps
   vm-select-message-hook
   vm-select-new-message-hook
   vm-select-unread-message-hook
   vm-send-digest-hook
   vm-send-using-mime
   vm-skip-deleted-messages
   vm-skip-read-messages
   vm-spool-file-suffixes
   vm-spool-files
   vm-startup-with-summary
   vm-strip-reply-headers
   vm-summary-arrow
   vm-summary-format
   vm-summary-highlight-face
   vm-summary-mode-hook
   vm-summary-redo-hook
   vm-summary-show-threads
   vm-summary-thread-indent-level
   vm-tale-is-an-idiot
   vm-temp-file-directory
   vm-thread-using-subject
   vm-toolbar-pixmap-directory
   vm-trust-From_-with-Content-Length
   vm-undisplay-buffer-hook
   vm-unforwarded-header-regexp
   vm-url-browser
   vm-url-search-limit
   vm-use-menus
   vm-use-toolbar
   vm-virtual-folder-alist
   vm-virtual-mirror
   vm-visible-headers
   vm-visit-folder-hook
   vm-visit-when-saving
   vm-warp-mouse-to-new-frame
   vm-window-configuration-file
" t nil)

(autoload 'vm-visit-folder "vm-startup" "\
Visit a mail file.
VM will parse and present its messages to you in the usual way.

First arg FOLDER specifies the mail file to visit.  When this
command is called interactively the file name is read from the
minibuffer.

Prefix arg or optional second arg READ-ONLY non-nil indicates
that the folder should be considered read only.  No attribute
changes, messages additions or deletions will be allowed in the
visited folder." t nil)

(autoload 'vm-visit-folder-other-frame "vm-startup" "\
Like vm-visit-folder, but run in a newly created frame." t nil)

(autoload 'vm-visit-folder-other-window "vm-startup" "\
Like vm-visit-folder, but run in a different window." t nil)

(autoload 'vm-visit-virtual-folder "vm-startup" nil t nil)

(autoload 'vm-visit-virtual-folder-other-frame "vm-startup" "\
Like vm-visit-virtual-folder, but run in a newly created frame." t nil)

(autoload 'vm-visit-virtual-folder-other-window "vm-startup" "\
Like vm-visit-virtual-folder, but run in a different window." t nil)

(autoload 'vm-mail "vm-startup" "\
Send a mail message from within VM, or from without.
Optional argument TO is a string that should contain a comma separated
recipient list." t nil)

(autoload 'vm-mail-other-frame "vm-startup" "\
Like vm-mail, but run in a newly created frame.
Optional argument TO is a string that should contain a comma separated
recipient list." t nil)

(autoload 'vm-mail-other-window "vm-startup" "\
Like vm-mail, but run in a different window.
Optional argument TO is a string that should contain a comma separated
recipient list." t nil)

(autoload 'vm-submit-bug-report "vm-startup" "\
Submit a bug report, with pertinent information to the VM bug list." t nil)

;;;***

(provide 'vm-autoloads)

;;; gnus-mime.el --- MIME extensions for Gnus

;; Copyright (C) 1996,1997 Free Software Foundation, Inc.

;; Author: MORIOKA Tomohiko <morioka@jaist.ac.jp>
;; Created: 1996/8/6
;; Version: $Revision: 0.15 $
;; Keywords: news, MIME, multimedia, multilingual, encoded-word

;; This file is not part of GNU Emacs yet.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'tl-misc)


;;; @ version
;;;

(defconst gnus-mime-RCS-ID
  "$Id: gnus-mime.el,v 0.15 1997/03/07 15:59:31 morioka Exp $")

(defconst gnus-mime-version
  (get-version-string gnus-mime-RCS-ID))


;;; @ variables
;;;

(defvar gnus-show-mime t
  "*If non-nil, do mime processing of articles.
The articles will simply be fed to the function given by
`gnus-show-mime-method'.")

(defvar gnus-show-mime-method 'gnus-article-preview-mime-message
  "*Function to process a MIME message.
The function is called from the article buffer.")

(defvar gnus-decode-encoded-word-method 'gnus-article-decode-encoded-word
  "*Function to decode a MIME encoded-words.
The function is called from the article buffer.")

(defvar gnus-parse-headers-hook
  '(gnus-set-summary-default-charset gnus-decode-rfc1522)
  "*A hook called before parsing the headers.")


;;; @ load
;;;

(require 'gnus)
(require 'gnus-charset)


;;; @ end
;;;

(provide 'gnus-mime)

(or gnus-is-red-gnus-or-later
    (require 'gnus-mime-old)
    )

(call-after-loaded 'gnus-art (lambda ()
			       (require 'gnus-art-mime)
			       ))
(call-after-loaded 'gnus-sum (lambda ()
			       (require 'gnus-sum-mime)
			       ))

(run-hooks 'gnus-mime-load-hook)

;;; gnus-mime.el ends here

;;; Mouse and font support for PCL-CVS 1.3 running in XEmacs
;; @(#) Id: pcl-cvs-xemacs.el,v 1.2 1993/05/31 19:37:34 ceder Exp 
;; Copyright (C) 1992-1993 Free Software Foundation, Inc.

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.


;; This simply adds a menu of the common CVS commands to the menubar and to
;; the right mouse button.  Clicking right moves point, and then pops up a
;; menu from which commands can be executed.
;; 
;; This could stand to be a lot more clever: for example, the "Commit Changes"
;; command should only be active on files for which there is something to
;; commit.  Also, some indication of which files the command applies to
;; (especially in the presence of multiple marked files) would be nice.
;;
;; Middle-click runs find-file.


(require 'pcl-cvs)

;;;
;;; Menubar entry under "Tools", and a cvs-mode menu.
;;;

;;;###autoload
(if (and (string-match "XEmacs" emacs-version)
         (featurep 'menubar)
         (not (featurep 'pcl-cvs-xemacs))
         (not (featurep 'infodock)))
    (add-submenu '("Tools")
                 '("PCL CVS"
                   ["Update Directory"    cvs-update    t]
                   ["Examine Directory"   cvs-examine   t]
                   ["Status Directory"    cvs-status    t]
                   ["Checkout Module"     cvs-checkout  t])))

(defvar cvs-menu
  '("CVS"
    ["Find File"			cvs-mode-find-file		t]
    ["Find File Other Window"		cvs-mode-find-file-other-window	t]
    ["Interactively Merge (emerge)"	cvs-mode-emerge			t]
    ["Interactively Merge (ediff)"      cvs-mode-ediff                  (fboundp 'ediff)]
    ["Diff against Repository"		cvs-mode-diff-cvs		t]
    ["Diff against Backup Version"	cvs-mode-diff-backup		t]
    "----"
    ["Redo update/examine/status"	cvs-mode-redo	                t]
    ["Commit Changes to Repository"	cvs-mode-commit			t]
    ["ChangeLog Commit Changes" 	cvs-mode-changelog-commit	t]
    ["Revert File from Repository"	cvs-mode-undo-local-changes	t]
    ["Add File to Repository"		cvs-mode-add			t]
    ["Remove File from Repository"	cvs-mode-remove-file		t]
    ["Ignore File"			cvs-mode-ignore			t]
    ["Hide File"			cvs-mode-acknowledge		t]
    ["Hide Handled Files"		cvs-mode-remove-handled		t]
    "----"
    ["Add ChangeLog Entry"   cvs-mode-add-change-log-entry-other-window t]
    ["Show CVS Log"			cvs-mode-log			t]
    ["Show CVS Status"			cvs-mode-status			t]
    "----"
    ["Mark File"			cvs-mode-mark			t]
    ["Unmark File"			cvs-mode-unmark			t]
    ["Mark All Files"			cvs-mode-mark-all-files		t]
    ["Unmark All Files"			cvs-mode-unmark-all-files	t]
    "----"
    ["Update Directory"                 cvs-update                      t]
    ["Examine Directory"                cvs-examine                     t]
    ["Status Directory"                 cvs-status                      t]
    ["Checkout Module"                  cvs-checkout                    t]
    "----"
    ["Quit"				bury-buffer			t]
    ))


;;;
;;; Mouse bindings and mode motion
;;;

(defun cvs-menu (e)
  (interactive "e")
  (mouse-set-point e)
  (beginning-of-line)
  (or (looking-at "^[* ] ") (error "No CVS file line here"))
  (popup-menu cvs-menu))

(defun cvs-mouse-find-file (e)
  (interactive "e")
  (mouse-set-point e)
  (beginning-of-line)
  (or (looking-at "^[* ] ") (error "No CVS file line here"))
  (cvs-mode-find-file (point)))

(define-key cvs-mode-map 'button3 'cvs-menu)
(define-key cvs-mode-map 'button2 'cvs-mouse-find-file)

(defun pcl-mode-motion-highlight-line (event)
  (if (save-excursion
	(let* ((window (event-window event))
	       (buffer (and window (event-buffer event)))
	       (point (and buffer (event-point event))))
	  (and point
	       (progn
		 (set-buffer buffer)
		 (goto-char point)
		 (beginning-of-line)
		 (looking-at "^[* ] ")))))
      (mode-motion-highlight-line event)))

;;;
;;; Fontification
;;;

;;; #### these face defaults need serious work...
;;; #### add faces for head and base revision too?

(defface cvs-header-face
  '((((class color) (background dark))
     (:foreground "lightyellow" :bold t))
    (((class color) (background light))
     (:foreground "blue4" :bold t))
    (t (:bold t)))
  "PCL-CVS face used to highlight directory changes."
  :group 'pcl-cvs)

(defface cvs-filename-face
  '((((class color) (background dark))
     (:foreground "lightblue"))
    (((class color) (background light))
     (:foreground "blue4" :bold t))
    (t (:bold t)))
  "PCL-CVS face used to highlight file names."
  :group 'pcl-cvs)

(defface cvs-unknown-face
  '((((class color) (background dark))
     (:foreground "red"))
    (((class color) (background light))
     (:foreground "red" :bold t))
    (t (:italic t :bold t)))
  "PCL-CVS face used to highlight unknown file status."
  :group 'pcl-cvs)

(defface cvs-handled-face
  '((((class color) (background dark))
     (:foreground "pink"))
    (((class color) (background light))
     (:foreground "pink" :bold t))
    (t (:bold t)))
  "PCL-CVS face used to highlight handled file status."
  :group 'pcl-cvs)

(defface cvs-need-action-face
  '((((class color) (background dark))
     (:foreground "orange"))
    (((class color) (background light))
     (:foreground "orange" :bold t))
    (t (:italic t :bold t)))
  "PCL-CVS face used to highlight status of files needing action."
  :group 'pcl-cvs)

(defface cvs-marked-face
  '((((class color) (background dark))
     (:foreground "green" :bold t))
    (((class color) (background light))
     (:foreground "green" :bold t))
    (t (:bold t)))
  "PCL-CVS face used to highlight marked file indicator."
  :group 'pcl-cvs)

(defconst pcl-cvs-unknown-list '("Unknown")
  "List of status values to be highlighted as unknown in pcl-cvs")

(defconst pcl-cvs-handled-list
  '("Updated"
    "Patched"
    "Modified"
    "Merged"
    "Conflict"
    "Added"
    "Removed"
    ;; cvs-status adds these
    "Need-update"
    "Up-to-date"
    "Need-merge"
    "Merge"
    "Need-remove")
  "List of status values to be highlighted as handled in pcl-cvs")

(defconst pcl-cvs-need-action-list
  '("Updated[ ]+co"
    "Patched[ ]+co"
    "Modified[ ]+ci"
    "Merged[ ]+ci"
    "Conflict[ ]+ci"
    "Added[ ]+ci"
    "Removed[ ]+ci"
    ;; cvs-status adds these
    "Need-update[ ]+co"
    "Need-merge[ ]+ci"
    "Merge[ ]+co"
    "Unresolved-conflict"
    "Need-remove[ ]+ci")
  "List of status values to be highlighted as needing action in pcl-cvs")

(defun pcl-cvs-gen-keywords (status-list)
  (concat "^[ ]*\\(-?[0-9.]+\\)?[ ]*"  ; optional head revision
          "\\([* ]\\) "                ; optional marked indicator
          "\\("                        ; status and optional ci/co
          (mapconcat 'identity
                     status-list
                     "\\|")
          "\\)"
          "[ ]+\\(-?[0-9.]+\\)?[ ]+"   ; optional base revision
          "\\(.+\\)$"))                ; file name

(defconst pcl-cvs-font-lock-keywords
  (let ((file-line1 (pcl-cvs-gen-keywords pcl-cvs-unknown-list))
        (file-line2 (pcl-cvs-gen-keywords pcl-cvs-handled-list))
        (file-line3 (pcl-cvs-gen-keywords pcl-cvs-need-action-list)))
    `(("^In directory \\(.+\\)[:]$" 1 cvs-header-face)
      ("^[ ]*\\([* ]\\) \\(Conflict: Removed by you, changed in repository:\\) \\(.+\\)$"
       (1 cvs-marked-face) (2 cvs-need-action-face) (3 cvs-filename-face))
      (,file-line3 (2 cvs-marked-face) (3 cvs-need-action-face) (5 cvs-filename-face))
      (,file-line2 (2 cvs-marked-face) (3 cvs-handled-face) (5 cvs-filename-face))
      (,file-line1 (2 cvs-marked-face) (3 cvs-unknown-face) (5 cvs-filename-face))))
  "Patterns to highlight in the *cvs* buffer.")

;;;###autoload
(defun pcl-cvs-fontify ()
  ;;
  ;; set up line highlighting
  (require 'mode-motion)
  (setq mode-motion-hook 'pcl-mode-motion-highlight-line)
  ;;
  ;; set up menubar
  (if (and current-menubar (not (assoc "CVS" current-menubar)))
      (progn
	(set-buffer-menubar (copy-sequence current-menubar))
	(add-menu nil "CVS" (cdr cvs-menu))))
  ;;
  ;; fontify mousable lines
  (set (make-local-variable 'font-lock-keywords) pcl-cvs-font-lock-keywords)
  (font-lock-mode 1)
  )

(add-hook 'cvs-mode-hook 'pcl-cvs-fontify)


;;;
;;; Provide this package
;;;

(provide 'pcl-cvs-xemacs)

;;; pcl-cvs-xemacs.el ends here

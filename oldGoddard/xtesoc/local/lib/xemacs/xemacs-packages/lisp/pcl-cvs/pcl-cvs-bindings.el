
;; Bindings and autoloads for pcl-cvs-buffer.el

(defvar cvs:buffer-command-map nil
  "Map for commands that operate on the current buffer.")

(if cvs:buffer-command-map
    nil
  (define-prefix-command 'cvs:buffer-command-map)
  (setq cvs:buffer-command-map (symbol-function 'cvs:buffer-command-map))
  (define-key 'cvs:buffer-command-map "l" 'cvs:log))

(global-set-key "\C-c\C-v" 'cvs:buffer-command-map)

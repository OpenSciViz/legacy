;;; DO NOT MODIFY THIS FILE
(if (featurep 'pcl-cvs-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "pcl-cvs/_pkg.el")

(package-provide 'pcl-cvs :version 1.4 :type 'regular)

;;;***

;;;### (autoloads (sc-set-CCASE-mode sc-set-ATRIA-mode sc-set-CLEARCASE-mode sc-set-CVS-mode sc-set-RCS-mode sc-set-SCCS-mode sc-mode) "generic-sc" "pcl-cvs/generic-sc.el")

(autoload 'sc-mode "generic-sc" "\
Toggle sc-mode.
SYSTEM can be sccs, rcs or cvs.
Cvs requires the pcl-cvs package.

The following commands are available
\\[sc-next-operation]	perform next logical source control operation on current file
\\[sc-show-changes]	compare the version being edited with an older one
\\[sc-version-diff-file]	compare two older versions of a file
\\[sc-show-history]		display change history of current file
\\[sc-visit-previous-revision]	display an older revision of current file
\\[sc-revert-file]		revert buffer to last checked-in version
\\[sc-list-all-locked-files]		show all files locked in current directory
\\[sc-list-locked-files]		show all files locked by you in current directory
\\[sc-list-registered-files]		show all files under source control in current directory
\\[sc-update-directory]		get fresh copies of files checked-in by others in current directory
\\[sc-rename-file]		rename the current file and its source control file


While you are entering a change log message for a check in, sc-log-entry-mode
will be in effect.

Global user options:
    sc-diff-command	A list consisting of the command and flags
			to be used for generating context diffs.
    sc-mode-expert	suppresses some conformation prompts,
			notably for delta aborts and file saves.
    sc-max-log-size	specifies the maximum allowable size
			of a log message plus one.


When using SCCS you have additional commands and options

\\[sccs-insert-headers]		insert source control headers in current file

When you generate headers into a buffer using \\[sccs-insert-headers],
the value of sc-insert-headers-hook is called before insertion. If the
file is recognized a C or Lisp source, sc-insert-c-header-hook or
sc-insert-lisp-header-hook is called after insertion respectively.

    sccs-headers-wanted	which %-keywords to insert when adding
			headers with C-c h
    sccs-insert-static	if non-nil, keywords inserted in C files
			get stuffed in a static string area so that
			what(1) can see them in the compiled object code.

When using CVS you have additional commands

\\[sc-cvs-update-directory]	update the current directory using pcl-cvs
\\[sc-cvs-file-status]		show the CVS status of current file
" t nil)

(autoload 'sc-set-SCCS-mode "generic-sc" nil nil nil)

(autoload 'sc-set-RCS-mode "generic-sc" nil nil nil)

(autoload 'sc-set-CVS-mode "generic-sc" nil nil nil)

(autoload 'sc-set-CLEARCASE-mode "generic-sc" nil nil nil)

(autoload 'sc-set-ATRIA-mode "generic-sc" nil nil nil)

(autoload 'sc-set-CCASE-mode "generic-sc" nil nil nil)

;;;***

;;;### (autoloads (pcl-cvs-fontify) "pcl-cvs-xemacs" "pcl-cvs/pcl-cvs-xemacs.el")

(if (and (string-match "XEmacs" emacs-version) (featurep 'menubar) (not (featurep 'pcl-cvs-xemacs)) (not (featurep 'infodock))) (add-submenu '("Tools") '("PCL CVS" ["Update Directory" cvs-update t] ["Examine Directory" cvs-examine t] ["Status Directory" cvs-status t] ["Checkout Module" cvs-checkout t])))

(autoload 'pcl-cvs-fontify "pcl-cvs-xemacs" nil nil nil)

;;;***

;;;### (autoloads (cvs-change-cvsroot cvs-help cvs-status-other-window cvs-status cvs-checkout-other-window cvs-checkout cvs-update-other-window cvs-update cvs-examine-other-window cvs-examine) "pcl-cvs" "pcl-cvs/pcl-cvs.el")

(autoload 'cvs-examine "pcl-cvs" "\
Run a 'cvs -n update' in the current working directory.
That is, check what needs to be done, but don't change the disc.
Feed the output to a *cvs* buffer and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-examine-other-window "pcl-cvs" "\
Run a 'cvs -n update' in the current working directory.
That is, check what needs to be done, but don't change the disc.
Feed the output to a *cvs* buffer, display it in the other window,
and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-update "pcl-cvs" "\
Run a 'cvs update' in the current working directory.
Feed the output to a *cvs* buffer and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-update-other-window "pcl-cvs" "\
Run a 'cvs update' in the current working directory.
Feed the output to a *cvs* buffer, display it in the other window,
and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-checkout "pcl-cvs" "\
Run a 'cvs checkout MODULE' in DIRECTORY.
Feed the output to a *cvs* buffer, display it in the current window,
and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-checkout-other-window "pcl-cvs" "\
Run a 'cvs checkout MODULE' in DIRECTORY.
Feed the output to a *cvs* buffer, display it in the other window,
and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-status "pcl-cvs" "\
Run a 'cvs status' in the current working directory.
Feed the output to a *cvs* buffer and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-status-other-window "pcl-cvs" "\
Run a 'cvs status' in the current working directory.
Feed the output to a *cvs* buffer, display it in the other window,
and run cvs-mode on it.

With a prefix argument, prompt for cvs flags to use." t nil)

(autoload 'cvs-help "pcl-cvs" "\
Display help for various PCL-CVS commands." t nil)

(autoload 'cvs-change-cvsroot "pcl-cvs" "\
Change the cvsroot." t nil)

;;;***

(provide 'pcl-cvs-autoloads)

;;; pcl-cvs-buffer.el,v 1.2 1993/01/18 01:28:03 ceder Exp
;;; pcl-cvs-buffer.el -- Front-end to CVS 1.3 or later.  Release XXRELEASEXX.
;;; Copyright (C) 1991, 1992  Per Cederqvist
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

(require 'string)

(defvar cvs:log-args ""
  "*Flags to pass to 'cvs log'. Used by cvs:log.")

(defvar cvs:diff-args ""
  "*Flags to pass to 'cvs diff'. Used by cvs:diff.")


(defvar cvs:status-args ""
  "*Flags to pass to 'cvs status'. Used by cvs:status.")

(defun cvs:call-process-in-temp-buffer (program args message &optional noerrs)
  "Call PROGRAM, sending output to a temporary buffer.
ARGS is a list of strings to pass to the program. MESSAGE is
displayed in the echo area while the program is running.
Optional argument NOERRS should be an association list
of (errorcodes messages)."
  (cvs-use-temp-buffer)
  (message "%s..." message)
  (let ((code (apply (function call-process)
		     program nil t t args)))
    (cond
     ((memq code noerrs)
      (message "%s... %s" message (cdr (memq code noerrs))))
     ((stringp code)
      (message "%s received signal %s" program code))
     ((zerop code)
      (message "%s... Done." message))
     (t
      (message "%s exited with exit status %d" program code)))))
	 

(defun cvs:log (modify-args)
  "Run a 'cvs log' on the current buffer.
If optional prefix argument MODIFY-ARGS are given you are prompted for
flags to pass to 'cvs log'."
  (interactive "P")
  (if (or modify-args (not (string= "" cvs:log-args)))
      (setq cvs:log-args (read-string "Flags to pass to cvs log: "
				      cvs:log-args)))
  (cvs:call-process-in-temp-buffer
   cvs-program
   (cons "log"
	 (ncon
	  (cvs-filter (function (lambda (x) (not (string= "" x))))
		      (elib-string-split "[ \t]" cvs:log-args))
	  (cons (buffer-file-name) nil)))
   "Running cvs log"))



(defun cvs:diff (modify-args)
  "Run a 'cvs diff' on the current buffer.
If optional prefix argument MODIFY-ARGS are given you are prompted for
flags to pass to 'cvs diff'."
  (interactive "P")
  (if (or modify-args
	  (not (or (string= "" cvs:diff-args)
		   (string= "-u" cvs:diff-args)
		   (string= "-c" cvs:diff-args))))
      (setq cvs:diff-args (read-string "Flags to pass to cvs diff: "
				       cvs:diff-args)))
  (cvs:call-process-in-temp-buffer
   cvs-program
   (cons "diff"
	 (nconc
	  (elib-string-split "[ \t]" cvs:diff-args)
	  (cons (buffer-file-name) nil)))
   "Running cvs diff"
   '(1 "Differences found.")))

(defun cvs:status (modify-args)
  "Run a 'cvs status' on the current buffer.
If optional prefix argument MODIFY-ARGS are given you are prompted for
flags to pass to 'cvs status'."
  (interactive "P")
  (if (or modify-args (not (string= "" cvs:status-args)))
      (setq cvs:status-args (read-string "Flags to pass to cvs status: "
				      cvs:status-args)))
  (let ((name (buffer-file-name)))
    (cvs-use-temp-buffer)
    (apply (function call-process)
	   cvs-program nil t t "status"
	   (nconc
	    (elib-string-split "[ \t]" cvs:status-args)
	    (cons name nil)))))


(defun cvs:add (description)
  "Run a 'cvs add' on the current buffer.
DESCRIPTION is used as a description for the file."
  (interactive "sFile description: ")

  (let ((name (buffer-file-name)))
    (cvs-use-temp-buffer)
    (call-process cvs-program nil t t "add" "-m" description
		  (file-name-nondirectory name))))

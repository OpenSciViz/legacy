;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tools '("generic-sc"))
(custom-add-loads 'pcl-cvs '("pcl-cvs-xemacs"))
(custom-add-loads 'generic-sc '("generic-sc"))

;;; custom-load.el ends here

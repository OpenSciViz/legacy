;;; DO NOT MODIFY THIS FILE
(if (featurep 'time-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "time/_pkg.el")

(package-provide 'time :version 1.08 :type 'regular)

;;;***

;;;### (autoloads (display-time) "time" "time/time.el")

(defcustom display-time-day-and-date nil "*Non-nil means \\[display-time] should display day,date and time.\nThis affects the spec 'date in the variable display-time-form-list." :group 'display-time :type 'boolean)

(autoload 'display-time "time" "\
Display current time, load level, and mail flag in mode line of each buffer.
Updates automatically every minute.
If `display-time-day-and-date' is non-nil, the current day and date
are displayed as well.
After each update, `display-time-hook' is run with `run-hooks'.
If `display-time-echo-area' is non-nil, the time is displayed in the
echo area instead of in the mode-line." t nil)

;;;***

(provide 'time-autoloads)

;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'display-time-balloon '("time"))
(custom-add-loads 'display-time '("time"))
(custom-add-loads 'applications '("time"))

;;; custom-load.el ends here

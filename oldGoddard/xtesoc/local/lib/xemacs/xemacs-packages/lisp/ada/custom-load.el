;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'ada '("ada-mode"))
(custom-add-loads 'languages '("ada-mode"))

;;; custom-load.el ends here

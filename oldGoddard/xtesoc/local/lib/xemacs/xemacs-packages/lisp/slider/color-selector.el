;;; color-selector.el --- a graphical widget of three sliders to change faces. 

;; Copyright (C) 1997 Jens Lautenbacher <jens@tellux.de>

;; Author:     Jens Lautenbacher <jens@tellux.de>
;; Keywords:   extensions, faces
;; Version:    0.1

;; This file is part of XEmacs.

;; XEmacs and color-selector.el are free software; you can redistribute
;; and/or modify them under the terms of the GNU General Public
;; License as published by the Free Software Foundation; either
;; version 2, or (at your option) any later version.

;; XEmacs and color-selector.el are distributed in the hope that they
;; will be useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;; See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Synched up with: Not in FSF.

;;; Commentary:
;; ############## to test it: ##############################################
;;
;; NOTE: providing callback function is optional, but normally needed to
;;       integrate the selector into your program.
;;
;;(defun cs-message (selector)
;;  (let* ((face (color-selector-get-face selector))
;;         (mode (color-selector-get-mode selector))
;;         (clist (color-selector-get-color selector))
;;         (color (format "[#%02x%02x%02x]" (car clist) (cadr clist) (caddr clist)))) 
;;    (message "Changing %s of face `%s' to be %s" mode face color)))
;;
;;(color-selector-make-selector 'font-lock-function-name-face
;;				'foreground 'cs-message " ")
;;
;;(color-selector-make-selector 'font-lock-function-name-face
;;				'background 'cs-message " ")
;;

;;; Code:
;;


(require 'slider)

(defvar color-selector-ok     (make-glyph (concat slider-pixmap-dir
						  "cs-ok-up.xpm")))
(defvar color-selector-reset  (make-glyph (concat slider-pixmap-dir
						  "cs-reset-up.xpm")))
(defvar color-selector-cancel (make-glyph (concat slider-pixmap-dir
						 "cs-cancel-up.xpm")))
(defvar color-selector-ok-down     (make-glyph (concat slider-pixmap-dir
						       "cs-ok-down.xpm")))
(defvar color-selector-reset-down  (make-glyph (concat slider-pixmap-dir
						       "cs-reset-down.xpm")))
(defvar color-selector-cancel-down (make-glyph (concat slider-pixmap-dir
						       "cs-cancel-down.xpm")))


(defvar color-selector-button-keymap nil) 
(if color-selector-button-keymap ()
  (setq color-selector-button-keymap (make-keymap
				      'color-selector-button-keymap))
  (suppress-keymap color-selector-button-keymap)
  (define-key color-selector-button-keymap 'button1
    'color-selector-button-activate))

(defun color-selector-button-activate (event)
  (interactive "e")
  (let* ((extent (event-glyph-extent event))
	 (mouse-down t)
	 (action (extent-property extent 'button1-action))
	 up-glyph down-glyph)
    ;; make the glyph look pressed
    (cond  ((setq down-glyph (extent-property extent 'button-down))
	    (setq up-glyph (extent-begin-glyph extent))
	    (set-extent-begin-glyph extent down-glyph)))
    (while mouse-down
      (setq event (next-event event))
      (if (button-release-event-p event)
	  (setq mouse-down nil)))
    ;; make the glyph look released
    (if down-glyph (set-extent-begin-glyph extent up-glyph))
    (if (eq extent (event-glyph-extent event))
	(if action
	    (funcall action (extent-property extent 'button-selector))))))




(defun color-selector-repaint (ignore data)
  (let* ((selector (car data))
	 (a (slider-get (extent-property selector 'slider-red)))  
	 (b (slider-get (extent-property selector 'slider-green)))
	 (c (slider-get (extent-property selector 'slider-blue)))
	 (red-label   (extent-property selector 'extent-red))
	 (green-label (extent-property selector 'extent-green))
	 (blue-label  (extent-property selector 'extent-blue))
	 (red (make-glyph (format "%3s" a)))
	 (green (make-glyph (format "%3s" b)))
	 (blue (make-glyph (format "%3s" c)))
	 (face (extent-property selector 'mixed-face))
	 (face-to-set (extent-property selector 'selector-face))
	 (mode (extent-property selector 'selector-mode))
	 (callback (extent-property selector 'selector-callback))
	 face-r face-g face-b)
    (set-face-background (setq face-r (glyph-face
				    (extent-begin-glyph red-label)))
			 (format "#%02x0000" a))
    (set-face-background (setq face-g (glyph-face
				    (extent-begin-glyph green-label)))
			 (format "#00%02x00" b))
    (set-face-background (setq face-b (glyph-face
				    (extent-begin-glyph blue-label)))
			 (format "#0000%02x" c))
    (set-glyph-face red face-r)
    (set-glyph-face green face-g)
    (set-glyph-face blue face-b)
    (set-extent-begin-glyph red-label red)
    (set-extent-begin-glyph green-label green)
    (set-extent-begin-glyph blue-label blue)
    (set-face-background face (format "#%02x%02x%02x"
						      a b c))
    (cond ((eq mode 'foreground)
	   (set-face-foreground face-to-set (format "#%02x%02x%02x"
						    a b c)))
	  ((eq mode 'background)
	   (set-face-background face-to-set (format "#%02x%02x%02x"
						    a b c)))
	  (t (error "%s: None of `foreground' or `background'"  mode)))
    (if callback (funcall callback selector))))


;;;###autoload
(defun color-selector-make-selector (face mode &optional callback indent)
  "Creates a color-selector object (and returns it) at the next line
in the current buffer which is supposed to change face FACE's
foreground or background color depending on the setting of MODE
\('foreground or 'background) An optional CALLBACK function can be
provided which will be called with the selector as an
argument. Optional INDENT is a string which will be inserted just
before the three sliders."
  (let* ((color (map 'list '(lambda (x) (/ x 256))
		     (if (eq mode 'foreground)
			 (color-instance-rgb-components
			  (face-foreground-instance face))
		       (color-instance-rgb-components
			(face-background-instance face)))))
	 (saved-face (make-face (gensym "color-selector-") nil t))
	 (red-face (make-face (gensym "color-selector-") nil t))
	 (blue-face (make-face (gensym "color-selector-") nil t))
	 (green-face (make-face (gensym "color-selector-") nil t))
	 (label-face (make-face (gensym "color-selector-") nil t))
	 s-r s-g s-b red-label green-label blue-label glyph selector
	 tmp)
    (copy-face face saved-face)
    (set-face-foreground red-face "#ffffff")
    (set-face-foreground green-face "#ffffff")
    (set-face-foreground blue-face "#ffffff")
    (set-face-background red-face   (format "#%02x0000" (car color)))
    (set-face-background green-face (format "#00%02x00" (cadr color)))
    (set-face-background blue-face  (format "#0000%02x" (caddr color)))
    (make-face-bold red-face)
    (make-face-bold green-face)
    (make-face-bold blue-face)
    (if (not (= (point) (point-at-bol)))
	(insert-string "\n"))
    (setq selector (make-extent (point) (point)))
    (set-extent-property selector 'selector-is-selector t)
    (set-extent-property selector 'end-open nil)
    ;; red
    (if indent (insert-string indent))
    (setq red-label (make-extent (point) (point)))
    (set-extent-begin-glyph red-label
			    (setq glyph (make-glyph
					 (format "%3s" (car color)))))
    (set-glyph-face glyph red-face)
    (setq s-r (slider-new 100 0 255 20 'color-selector-repaint))
    (insert-string "      ")
    (set-extent-face (make-extent (- (point) 6) (point)) label-face)
    (insert-string " ")
    (setq tmp (make-extent (point) (point)))
    (set-extent-begin-glyph tmp color-selector-ok)
    (set-extent-property tmp 'button-down color-selector-ok-down)
    (set-extent-keymap tmp color-selector-button-keymap)
    (set-extent-property tmp 'button1-action 'color-selector-destroy-selector)
    (set-extent-property tmp 'button-selector selector)
    
    ;; green
    (insert-string "\n")
    (if indent (insert-string indent))
    (setq green-label (make-extent (point) (point)))
    (set-extent-begin-glyph green-label
			    (setq glyph (make-glyph
					 (format "%3s" (cadr color)))))
    (set-glyph-face glyph green-face)
    (setq s-g (slider-new 100 0 255 20 'color-selector-repaint))
    (insert-string "      ")
    (set-extent-face (make-extent (- (point) 6) (point)) label-face)
    (insert-string " ")
    (setq tmp (make-extent (point) (point)))
    (set-extent-begin-glyph tmp color-selector-reset)
    (set-extent-property tmp 'button-down color-selector-reset-down)
    (set-extent-keymap tmp color-selector-button-keymap)
    (set-extent-property tmp 'button1-action 'color-selector-reset-selector)
    (set-extent-property tmp 'button-selector selector)
    
    ;; blue
    (insert-string "\n")
    (if indent (insert-string indent))
    (setq blue-label (make-extent (point) (point)))
    (set-extent-begin-glyph blue-label
			    (setq glyph (make-glyph
					 (format "%3s" (caddr color)))))
    (set-glyph-face glyph blue-face)
    (setq s-b (slider-new 100 0 255 20 'color-selector-repaint))
    (insert-string "      ")
    (set-extent-face (make-extent (- (point) 6) (point)) label-face)
    (insert-string " ")
    (setq tmp (make-extent (point) (point)))
    (set-extent-begin-glyph tmp color-selector-cancel)
    (set-extent-property tmp 'button-down color-selector-cancel-down)
    (set-extent-keymap tmp color-selector-button-keymap)
    (set-extent-property tmp 'button1-action 'color-selector-cancel-selector)
    (set-extent-property tmp 'button-selector selector)
    
    (slider-set-data s-r (list selector))
    (slider-set-data s-g (list selector))
    (slider-set-data s-b (list selector))
    (insert-string "\n")
    (set-extent-property selector 'end-open t)
    (set-extent-property selector 'start-open nil)
    (set-extent-property selector 'read-only t)
    (set-extent-property selector 'slider-red s-r)
    (set-extent-property selector 'slider-green s-g)
    (set-extent-property selector 'slider-blue s-b)
    (set-extent-property selector 'extent-red   red-label)
    (set-extent-property selector 'extent-green green-label)
    (set-extent-property selector 'extent-blue  blue-label)
    (set-extent-property selector 'mixed-face label-face)
    (set-extent-property selector 'selector-face face)
    (set-extent-property selector 'selector-saved-face saved-face)
    (set-extent-property selector 'selector-mode mode)
    (set-extent-property selector 'selector-callback callback)
    (slider-set s-r (car color))
    (slider-set s-g (cadr color))
    (slider-set s-b (caddr color))
    selector))


(defun color-selector-destroy-selector (selector)
  "Destroys a color-selector SELECTOR"
  (color-selector-check selector)
  (set-extent-property selector 'read-only nil)
  (delete-region (extent-start-position selector)
		 (extent-end-position selector)
		 (extent-object selector))
  (delete-extent selector))

(defun color-selector-reset-selector (selector)
  "Resets the color selector SELECTOR and it's attached face
to the original value."
  (color-selector-check selector)
  (let* ((face  (extent-property selector 'selector-face))
	 (mode  (extent-property selector 'selector-mode))
	 (saved (extent-property selector 'selector-saved-face)))
    (cond ((eq mode 'foreground)
	   (set-face-foreground face (face-foreground saved))
	   (color-selector-adjust-selector
	    selector (map 'list '(lambda (x) (/ x 256))
			  (color-instance-rgb-components
			   (face-foreground-instance saved)))))
	  ((eq mode 'background)
	   (set-face-background face (face-background 'default))
	   (color-selector-adjust-selector
	    selector (map 'list '(lambda (x) (/ x 256))
			  (color-instance-rgb-components
			   (face-background-instance saved)))))
	  (t (error "%s: None of `foreground' or `background'"  mode)))))

(defun color-selector-cancel-selector (selector)
  "Resets and then destroys SELECTOR."
  (color-selector-check selector)
  (color-selector-reset-selector selector)
  (color-selector-destroy-selector selector))

(defun color-selector-adjust-selector (selector color)
  "Adjust color-selector SELECTOR to COLOR which is a list of three integers
in the range 0...255."
  (color-selector-check selector)
  (slider-set (extent-property selector 'slider-red) (car color))
  (slider-set (extent-property selector 'slider-green) (cadr color))
  (slider-set (extent-property selector 'slider-blue) (caddr color)))

(defun color-selector-get-color (selector)
  "Return the current color the color-selector SELECTOR is showing
as a list of 3 integers in the range 0...255."
  (color-selector-check selector)
  (list (slider-get (extent-property selector 'slider-red))
	(slider-get (extent-property selector 'slider-green))
	(slider-get (extent-property selector 'slider-blue))))

(defun color-selector-get-mode (selector)
  "Return the mode (background or foreground) color-selector SELECTOR is working in."
  (extent-property selector 'selector-mode))

(defun color-selector-get-face (selector)
  "Return the face color-selector SELECTOR is attached to."
  (color-selector-check selector)
  (extent-property selector 'selector-face))

;;;###autoload
(defun color-selector-p (obj)
  "Checks wether OBJ is a color-selector object"
  (and (extentp obj) (extent-property obj 'selector-is-selector)))

(defun color-selector-check (obj)
  (or (color-selector-p obj)
      (error "Object %s is no color-selector" obj)))

(provide 'color-selector)

;;; color-selector.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'slider-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "slider/_pkg.el")

(package-provide 'slider :version 1.1 :type 'regular)

;;;***

;;;### (autoloads (color-selector-p color-selector-make-selector) "color-selector" "slider/color-selector.el")

(autoload 'color-selector-make-selector "color-selector" "\
Creates a color-selector object (and returns it) at the next line
in the current buffer which is supposed to change face FACE's
foreground or background color depending on the setting of MODE
\('foreground or 'background) An optional CALLBACK function can be
provided which will be called with the selector as an
argument. Optional INDENT is a string which will be inserted just
before the three sliders." nil nil)

(autoload 'color-selector-p "color-selector" "\
Checks wether OBJ is a color-selector object" nil nil)

;;;***

(provide 'slider-autoloads)

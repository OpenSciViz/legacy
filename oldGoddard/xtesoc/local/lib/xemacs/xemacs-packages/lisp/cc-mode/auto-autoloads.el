;;; DO NOT MODIFY THIS FILE
(if (featurep 'cc-mode-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "cc-mode/_pkg.el")

(package-provide 'cc-mode :version 1.16 :type 'regular)

;;;***

;;;### (autoloads nil "cc-langs" "cc-mode/cc-langs.el")

(defvar c-mode-syntax-table nil "\
Syntax table used in c-mode buffers.")

(defvar c++-mode-syntax-table nil "\
Syntax table used in c++-mode buffers.")

(defvar objc-mode-syntax-table nil "\
Syntax table used in objc-mode buffers.")

(defvar java-mode-syntax-table nil "\
Syntax table used in java-mode buffers.")

(defvar idl-mode-syntax-table nil "\
Syntax table used in idl-mode buffers.")

(defvar pike-mode-syntax-table nil "\
Syntax table used in pike-mode buffers.")

;;;***

;;;### (autoloads (pike-mode idl-mode java-mode objc-mode c++-mode c-mode c-initialize-cc-mode) "cc-mode" "cc-mode/cc-mode.el")

(autoload 'c-initialize-cc-mode "cc-mode" nil nil nil)

(autoload 'c-mode "cc-mode" "\
Major mode for editing K&R and ANSI C code.
To submit a problem report, enter `\\[c-submit-bug-report]' from a
c-mode buffer.  This automatically sets up a mail buffer with version
information already added.  You just need to add a description of the
problem, including a reproducible test case and send the message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `c-mode-hook' is run with no args, if that value is
bound and has a non-nil value.  Also the hook `c-mode-common-hook' is
run first.

Key bindings:
\\{c-mode-map}" t nil)

(autoload 'c++-mode "cc-mode" "\
Major mode for editing C++ code.
To submit a problem report, enter `\\[c-submit-bug-report]' from a
c++-mode buffer.  This automatically sets up a mail buffer with
version information already added.  You just need to add a description
of the problem, including a reproducible test case, and send the
message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `c++-mode-hook' is run with no args, if that
variable is bound and has a non-nil value.  Also the hook
`c-mode-common-hook' is run first.

Key bindings:
\\{c++-mode-map}" t nil)

(autoload 'objc-mode "cc-mode" "\
Major mode for editing Objective C code.
To submit a problem report, enter `\\[c-submit-bug-report]' from an
objc-mode buffer.  This automatically sets up a mail buffer with
version information already added.  You just need to add a description
of the problem, including a reproducible test case, and send the
message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `objc-mode-hook' is run with no args, if that value
is bound and has a non-nil value.  Also the hook `c-mode-common-hook'
is run first.

Key bindings:
\\{objc-mode-map}" t nil)

(autoload 'java-mode "cc-mode" "\
Major mode for editing Java code.
To submit a problem report, enter `\\[c-submit-bug-report]' from a
java-mode buffer.  This automatically sets up a mail buffer with
version information already added.  You just need to add a description
of the problem, including a reproducible test case and send the
message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `java-mode-hook' is run with no args, if that value
is bound and has a non-nil value.  Also the common hook
`c-mode-common-hook' is run first.  Note that this mode automatically
sets the \"java\" style before calling any hooks so be careful if you
set styles in `c-mode-common-hook'.

Key bindings:
\\{java-mode-map}" t nil)

(autoload 'idl-mode "cc-mode" "\
Major mode for editing CORBA's IDL code.
To submit a problem report, enter `\\[c-submit-bug-report]' from an
idl-mode buffer.  This automatically sets up a mail buffer with
version information already added.  You just need to add a description
of the problem, including a reproducible test case, and send the
message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `idl-mode-hook' is run with no args, if that
variable is bound and has a non-nil value.  Also the hook
`c-mode-common-hook' is run first.

Key bindings:
\\{idl-mode-map}" t nil)

(autoload 'pike-mode "cc-mode" "\
Major mode for editing Pike code.
To submit a problem report, enter `\\[c-submit-bug-report]' from an
idl-mode buffer.  This automatically sets up a mail buffer with
version information already added.  You just need to add a description
of the problem, including a reproducible test case, and send the
message.

To see what version of CC Mode you are running, enter `\\[c-version]'.

The hook variable `pike-mode-hook' is run with no args, if that value
is bound and has a non-nil value.  Also the common hook
`c-mode-common-hook' is run first.

Key bindings:
\\{pike-mode-map}" t nil)

;;;***

;;;### (autoloads (c-set-offset c-add-style c-set-style) "cc-styles" "cc-mode/cc-styles.el")

(autoload 'c-set-style "cc-styles" "\
Set CC Mode variables to use one of several different indentation styles.
STYLENAME is a string representing the desired style from the list of
styles described in the variable `c-style-alist'.  See that variable
for details of setting up styles.

The variable `c-indentation-style' always contains the buffer's current
style name." t nil)

(autoload 'c-add-style "cc-styles" "\
Adds a style to `c-style-alist', or updates an existing one.
STYLE is a string identifying the style to add or update.  DESCRIP is
an association list describing the style and must be of the form:

  ([BASESTYLE] (VARIABLE . VALUE) [(VARIABLE . VALUE) ...])

See the variable `c-style-alist' for the semantics of BASESTYLE,
VARIABLE and VALUE.  This function also sets the current style to
STYLE using `c-set-style' if the optional SET-P flag is non-nil." t nil)

(autoload 'c-set-offset "cc-styles" "\
Change the value of a syntactic element symbol in `c-offsets-alist'.
SYMBOL is the syntactic element symbol to change and OFFSET is the new
offset for that syntactic element.  Optional ADD says to add SYMBOL to
`c-offsets-alist' if it doesn't already appear there." t nil)

;;;***

;;;### (autoloads nil "cc-vars" "cc-mode/cc-vars.el")

(defconst c-emacs-features (let ((infodock-p (boundp 'infodock-version)) (comments (let ((table (copy-syntax-table)) entry) (modify-syntax-entry ?a ". 12345678" table) (cond ((arrayp table) (setq entry (aref table ?a)) (if (consp entry) (setq entry (car entry)))) ((fboundp 'get-char-table) (setq entry (get-char-table ?a table))) ((and (fboundp 'char-table-p) (char-table-p table)) (setq entry (car (char-table-range table [?a])))) (t (error "CC Mode is incompatible with this version of Emacs"))) (if (= (logand (lsh entry -16) 255) 255) '8-bit '1-bit)))) (if infodock-p (list comments 'infodock) (list comments))) "\
A list of features extant in the Emacs you are using.
There are many flavors of Emacs out there, each with different
features supporting those needed by CC Mode.  Here's the current
supported list, along with the values for this variable:

 XEmacs 19:                  (8-bit)
 XEmacs 20:                  (8-bit)
 Emacs 19:                   (1-bit)

Infodock (based on XEmacs) has an additional symbol on this list:
`infodock'.")

;;;***

(provide 'cc-mode-autoloads)

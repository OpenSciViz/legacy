;ELC   
;;; compiled by steve@miho.etl.go.jp on Thu Jul 29 18:22:33 1999
;;; from file /project/xemacs/home/steve/devel/xemacs-packages/libs/tooltalk/tooltalk-util.el
;;; emacs version 21.1 (patch 4) "Arches" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`tooltalk-util.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


#@498 Initialize the Nth tooltalk message argument of MSG.
A new argument is created if necessary.  No attempt to distinguish
between strings that contain binary data and ordinary strings is made;
all non integer argument values are converted to a string (if not a
string already) and loaded with tt_message_arg_val_set().
Applications that need to put binary data into a ToolTalk message
argument should initialize the argument with:

   (set-tooltalk-message-attribute bin-string msg 'arg_bval arg-n)
(defalias 'initialize-tooltalk-message-arg #[(msg n mode value vtype) "T�\n�\"Z��V���\n#�S��n)	����	\n�$�	;���	\n�$����\n!�	!$�" [n get-tooltalk-message-attribute msg args_count n-args-needed 0 add-tooltalk-message-arg mode vtype value set-tooltalk-message-attribute arg_ival arg_val error "The value specified for msg %s argument %d, %s, must be a string or an integer" prin1-to-string] 7 (#$ . 611)])
(byte-code "���	\n��B��" [TT_IN TT_OUT TT_INOUT tooltalk-arg-mode-ids current-load-list] 6)
#@462 Apply INITFN to each the position mode value and type of
each argument in the list.  The value of INITFN should be either
'initialize-tooltalk-message-arg or 'initialize-tooltalk-pattern-arg.
See `make-tooltalk-message' for a description of how arguments are specified.
We distinguish the short form for arguments, e.g. "just-a-value", 
from the long form by checking to see if the argument is a list whose
car is one of the ToolTalk mode values like TT_INOUT.
(defalias 'initialize-tooltalk-message/pattern-args #[(initfn msg args) "�\n��\n@�:��@����@������A��A@���	��AA��AA@;��AA@��	���ʪ��		%�-\nA	T�)�" [0 n args arg tooltalk-arg-mode-ids long-form TT_IN mode "" value "int" "string" type initfn msg] 7 (#$ . 1631)])
#@318 Initialize the tooltalk message attributes.  The value of 
attributes must be a property list in the same form as for 
make-tooltalk-message.  This function can be used to reset
an existing message or to initialize a new one.  See 
initialize-tooltalk-message-args for a description of how
arguments are initialized.
(defalias 'initialize-tooltalk-message-attributes #[(msg attributes) "����A��@A@��a���\n#����#�*AA��X�*�" [attributes initialize-tooltalk-message-arg initfn args value indicator initialize-tooltalk-message/pattern-args msg set-tooltalk-message-attribute nil] 5 (#$ . 2393)])
#@1310 Create a tooltalk message and initialize its attributes.
The value of attributes must be a list of alternating keyword/values, 
where keywords are symbols that name valid message attributes.  
For example:

  (make-tooltalk-message 
    '(class TT_NOTICE
      scope TT_SESSION
      address TT_PROCEDURE
      op "do-something"
      args ("arg1" 12345 (TT_INOUT "arg3" "string"))))

Values must always be strings, integers, or symbols that
represent Tooltalk constants.  Attribute names are the same as 
those supported by set-tooltalk-message-attribute, plus 'args.

The value of args should be a list of message arguments where
each message argument has the following form:

   (mode [value [type]]) or just value

Where mode is one of TT_IN, TT_OUT, TT_INOUT and type is a string.  
If type isn't specified then "int" is used if the value is a 
number otherwise "string" is used.  If only a value is specified 
then mode defaults to TT_IN.  If mode is TT_OUT then value and 
type don't need to be specified.  You can find out more about the 
semantics and uses of ToolTalk message arguments in chapter 4 of the 
Tooltalk Programmer's Guide.

The no-callback arg is a hack to prevent the registration of the
C-level callback.  This hack is needed by the current SPARCworks
tool startup mechanism.  Yucko.
(defalias 'make-tooltalk-message #[(attributes &optional no-callback) "�	!�\n\"�\n)�" [create-tooltalk-message no-callback msg initialize-tooltalk-message-attributes attributes] 3 (#$ . 3010)])
#@102 Print tooltalk message MSG's attributes and arguments to STREAM.
This is often useful for debugging.
(defalias 'describe-tooltalk-message #[(msg &optional stream) "��!�	���	@\"���\"���	@\"\"��!�	A��a)��\"�W����\"��\"���\"���#����#�k����#����#E!\"�)�!�T��/*�" [(address class disposition file gid handler handler_ptype object op opnum otype scope sender sender_ptype session state status status_string uid callback) attrs terpri stream princ "  " prin1 get-tooltalk-message-attribute msg args_count 0 i n "Argument " arg_type type prin1-to-string arg_mode "int" arg_ival arg_val] 8 (#$ . 4522)])
#@139 Add one argument to tooltalk pattern PAT.
No support for specifying pattern arguments whose value is a vector
of binary data is provided.
(defalias 'initialize-tooltalk-pattern-arg #[(pat n mode value vtype) "���;�����!�\n$)�" [value prin1-to-string converted-value add-tooltalk-pattern-arg pat mode vtype] 5 (#$ . 5176)])
#@569 Initialize tooltalk pattern PAT's attributes.
ATTRIBUTES must be a property list in the same form as for
`make-tooltalk-pattern'.  The value of each attribute (except 'category)
can either be a single value or a list of values.  If a list of
values is provided then the pattern will match messages with
a corresponding attribute that matches any member of the list.

This function can be used to add attribute values to an existing
pattern or to initialize a new one.  See
`initialize-tooltalk-message/pattern-args' for a description of how
arguments are initialized.
(defalias 'initialize-tooltalk-pattern-attributes #[(pat attributes) "����A��@A@��a���\n#����a���	��	@	A@\n�\n#�*	AA�	�b)��:���	���	@#�	A�	�o)���#�*AA���*�" [attributes initialize-tooltalk-pattern-arg initfn args value indicator initialize-tooltalk-message/pattern-args pat plist values propval prop tooltalk-pattern-prop-set add-tooltalk-pattern-attribute nil] 5 (#$ . 5515)])
#@1284 Create a tooltalk pattern and initialize its attributes.
The value of attributes must be a list of alternating keyword/values, 
where keywords are symbols that name valid pattern attributes
or lists of valid attributes.  For example:

  (make-tooltalk-pattern 
    '(category TT_OBSERVE
      scope TT_SESSION
      op ("operation1" "operation2")
      args ("arg1" 12345 (TT_INOUT "arg3" "string"))))


Values must always be strings, integers, or symbols that
represent Tooltalk constants or lists of same.  When a list 
of values is provided all of the list elements are added to 
the attribute.  In the example above, messages whose op
attribute is "operation1" or "operation2" would match the pattern.

The value of args should be a list of pattern arguments where 
each pattern argument has the following form:

   (mode [value [type]]) or just value

Where mode is one of TT_IN, TT_OUT, TT_INOUT and type is a string.  
If type isn't specified then "int" is used if the value is a 
number otherwise "string" is used.  If only a value is specified 
then mode defaults to TT_IN.  If mode is TT_OUT then value and type 
don't need to be specified.  You can find out more about the semantics 
and uses of ToolTalk pattern arguments in chapter 3 of the Tooltalk
Programmers Guide.

(defalias 'make-tooltalk-pattern #[(attributes) "� �	\"�	)�" [create-tooltalk-pattern pat initialize-tooltalk-pattern-attributes attributes] 3 (#$ . 6511)])

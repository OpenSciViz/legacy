;;; DO NOT MODIFY THIS FILE
(if (featurep 'tooltalk-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "tooltalk/_pkg.el")

(package-provide 'tooltalk :version 1.1 :type 'regular)

;;;***

(provide 'tooltalk-autoloads)

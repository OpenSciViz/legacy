;;; custom-load.el --- automatically extracted custom dependencies

;;; Code:

(custom-add-loads 'tools '("ediff"))
(custom-add-loads 'ediff-hook '("ediff-init"))
(custom-add-loads 'ediff-diff '("ediff-diff"))
(custom-add-loads 'ediff-window '("ediff-wind"))
(custom-add-loads 'frames '("ediff-wind"))
(custom-add-loads 'ediff-ptch '("ediff-ptch"))
(custom-add-loads 'ediff-merge '("ediff-init" "ediff-merg"))
(custom-add-loads 'ediff-highlighting '("ediff-init"))
(custom-add-loads 'ediff-mult '("ediff-mult"))
(custom-add-loads 'ediff '("ediff-diff" "ediff-init" "ediff-mult" "ediff-ptch" "ediff-tbar" "ediff-wind" "ediff"))

;;; custom-load.el ends here

;;; DO NOT MODIFY THIS FILE
(if (featurep 'efs-autoloads) (error "Already loaded"))

;;;### (autoloads nil "_pkg" "efs/_pkg.el")

(package-provide 'efs :version 1.15 :type 'regular)

;;;***

;;;### (autoloads nil "default-dir" "efs/default-dir.el")

(defvar default-directory-function nil "\
A function to call to compute the default-directory for the current buffer.
If this is nil, the function default-directory will return the value of the
variable default-directory.
Buffer local.")

;;;***

;;;### (autoloads (efs-ftp-path) "efs-cu" "efs/efs-cu.el")

(defvar efs-path-root-regexp "^/[^/:]+:" "\
Regexp to match the `/user@host:' root of an efs full path.")

(autoload 'efs-ftp-path "efs-cu" "\
Parse PATH according to efs-path-regexp.
Returns a list (HOST USER PATH), or nil if PATH does not match the format." nil nil)

;;;***

;;;### (autoloads (remote-path-file-handler-function) "efs-dump" "efs/efs-dump.el")

(defvar efs-path-root-regexp "^/[^/:]+:" "\
Regexp to match the `/user@host:' root of an efs full path.")

(or (assoc efs-path-root-regexp file-name-handler-alist) (setq file-name-handler-alist (cons (cons efs-path-root-regexp 'remote-path-file-handler-function) file-name-handler-alist)))

(autoload 'remote-path-file-handler-function "efs-dump" "\
Function to call special file handlers for remote files." nil nil)

;;;***

;;;### (autoloads nil "efs-fnh" "efs/efs-fnh.el")

(defvar allow-remote-paths t "\
*Set this to nil if you don't want remote paths to access
remote files.")

;;;***

;;;### (autoloads (efs-root-file-name-completion efs-root-file-name-all-completions efs-set-passwd) "efs-netrc" "efs/efs-netrc.el")

(autoload 'efs-set-passwd "efs-netrc" "\
For a given HOST and USER, set or change the associated PASSWORD." t nil)

(autoload 'efs-root-file-name-all-completions "efs-netrc" nil nil nil)

(autoload 'efs-root-file-name-completion "efs-netrc" nil nil nil)

;;;***

;;;### (autoloads (efs-report-bug) "efs-report" "efs/efs-report.el")

(autoload 'efs-report-bug "efs-report" "\
Submit a bug report for efs." t nil)

;;;***

;;;### (autoloads (efs-file-handler-function efs-nslookup-host efs-display-ftp-activity) "efs" "efs/efs.el")

(autoload 'efs-display-ftp-activity "efs" "\
Displays the number of active background ftp sessions in the modeline.
Uses the variable `efs-mode-line-format' to determine how this will be
displayed." t nil)

(autoload 'efs-nslookup-host "efs" "\
Attempt to resolve the given HOSTNAME using nslookup if possible." t nil)

(autoload 'efs-file-handler-function "efs" "\
Function to call special file handlers for remote files." nil nil)

;;;***

(provide 'efs-autoloads)

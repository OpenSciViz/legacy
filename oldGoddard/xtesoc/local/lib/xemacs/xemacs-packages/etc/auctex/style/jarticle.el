;;; jarticle.el - Special code for jarticle style.

;; $Id: jarticle.el,v 1.1 1997/02/20 02:15:20 steve Exp $

;;; Code:

(TeX-add-style-hook "jarticle"
 (function (lambda () (setq LaTeX-largest-level 2))))

;;; jarticle.el ends here

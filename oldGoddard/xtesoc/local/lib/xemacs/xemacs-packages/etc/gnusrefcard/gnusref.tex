% -*- mode:LaTeX; eval:(setq LaTeX-version "2"); truncate-lines:t; -*-
% include file for the Gnus refcard and booklet
\def\version{5.4.56}
\def\date{12 June 1997}
\def\author{Vladimir Alexiev $<$vladimir@cs.ualberta.ca$>$}
\raggedbottom\raggedright
\newlength{\logowidth}\setlength{\logowidth}{6.861in}
\newlength{\logoheight}\setlength{\logoheight}{7.013in}
\newlength{\keycolwidth}
\newenvironment{keys}[1]% #1 is the widest key
  {\nopagebreak%\noindent%
   \settowidth{\keycolwidth}{#1}%
   \addtolength{\keycolwidth}{\tabcolsep}%
   \addtolength{\keycolwidth}{-\columnwidth}%
   \begin{tabular}{@{}l@{\hspace{\tabcolsep}}p{-\keycolwidth}@{}}}%
  {\end{tabular}\\}
\catcode`\^=12 % allow ^ to be typed literally
\catcode`\~=12 % allow ~ to be typed literally
\newcommand{\B}[1]{{\bf#1})}    % bold l)etter

\def\Title{
\begin{center}
{\bf\LARGE Gnus \version\ Reference \Guide\\}
%{\normalsize \Guide\ version \refver}
\end{center}
}

\newcommand\Logo[1]{\centerline{
\makebox[\logoscale\logowidth][l]{\vbox to \logoscale\logoheight
{\vfill\special{psfile=gnuslogo.#1}}\vspace{-\baselineskip}}}}

\def\CopyRight{
\begin{center}
Copyright \copyright\ 1995 Free Software Foundation, Inc.\\*
Copyright \copyright\ 1995-97 \author.\\*
Created from the Gnus manual Copyright \copyright\ 1994-97 Lars Magne
Ingebrigtsen.\\*
and the Emacs Help Bindings feature (C-h b).\\*
Gnus logo copyright \copyright\ 1995 Luis Fernandes.\\*
\end{center}

Permission is granted to make and distribute copies of this reference
\guide{} provided the copyright notice and this permission are preserved on
all copies.  Please send corrections, additions and suggestions to the
above email address. \Guide{} last edited on \date.
}

\def\Notes{\subsec{Notes}{\samepage
Gnus is big. Currently it has some 633 interactive (user-callable)
commands. Many of these commands have more than one binding. In order to save
space, every function is listed only once in this \guide, under the ``more
logical'' binding. Alternative bindings are given in parentheses at the
beginning of the description. This \guide{} describes only key-bindings, you
{\em should\/} at least browse the manual to discover many great features

Many Gnus commands use the numeric prefix. Normally you enter a prefix by
holding the Meta key and typing a number, but in most Gnus modes you don't
need to use Meta since the digits are not self-inserting. The prefixed
behavior of commands is given in [brackets]. Often the prefix is used to
specify:

\quad [distance] How many objects to move the point over.

\quad [scope] How many objects to operate on (including the current one).

\quad [p/p] The ``Process/Prefix Convention'': If a prefix is given then it
determines how many objects to operate on (negative means backwards). Else if
transient-mark-mode or zmacs-regions is set and the region is active, operate
on the region. Else if there are some objects marked with the process mark \#,
operated on them. Else operate only on the current object.

\quad [level] A group subscribedness level. Only groups with a lower or
equal level will be affected by the operation. If no prefix is given,
`gnus-group-default-list-level' is used.  If
`gnus-group-use-permanent-levels', then a prefix to the `g' and `l'
commands will also set the default level.

\quad [score] An article score. If no prefix is given,
`gnus-summary-default-score' is used.

\quad [GL rating] Give a GroupLens rating (1-5) to the current article before
moving to another article. 

%Some functions were not yet documented at the time of creating this
%\guide and are clearly indicated as such.
}}

\def\GroupLevels{\subsec{Group Subscribedness Levels}{\samepage
The table below assumes that you use the default Gnus levels.
Fill your user-specific levels in the blank cells.\\*
\begin{tabular}{|c|l|l|}
\hline
Level & Groups & Status \\
\hline
1 & mail groups                &              \\
2 & mail groups                &              \\
3 & default subscribed level   & subscribed   \\
4 &                            &              \\
5 & default list level         &              \\
\hline
6 & default unsubscribed level & unsubscribed \\
7 &                            &              \\
\hline             
8 &                            & zombies      \\
\hline             
9 &                            & killed       \\
\hline
\end{tabular}
}}

\def\Marks{\subsec{Mark Indication Characters}{\samepage
If a command directly sets a mark, it is shown in parentheses.\\*
\newlength{\markcolwidth}
\settowidth{\markcolwidth}{` '}% widest character
\addtolength{\markcolwidth}{4\tabcolsep}
\addtolength{\markcolwidth}{-\columnwidth}
\newlength{\markdblcolwidth}
\setlength{\markdblcolwidth}{\columnwidth}
\addtolength{\markdblcolwidth}{-2\tabcolsep}
\begin{tabular}{|c|p{-\markcolwidth}|}
\hline
\multicolumn{2}{|p{\markdblcolwidth}|}
{{\bf ``Read'' Marks.}
  All these marks appear in the first column of the summary line, and so
  are mutually exclusive.}\\ 
\hline
` ' & (M-u, M SPC, M c) Not read.\\
!   & (!, M t) Ticked (interesting).\\ %(M !)
?   & (?, M ?) Dormant (only followups are interesting).\\
E   & (E, M e) {\bf Expirable}. Only has effect in mail groups.\\ %(M x)
\hline\hline
\multicolumn{2}{|p{\markdblcolwidth}|}{The marks below mean that the article
  is read (killed, uninteresting), and have more or less the same effect.
  Some commands however explicitly differentiate between them (e.g.{} M
  M-C-r, adaptive scoring).}\\
\hline
r   & (d, M d, M r) Deleted (marked as {\bf read}).\\
C   & (M C; M C-c; M H; c, Z c; Z n; Z C) Killed by {\bf catch-up}.\\
F   & (O s; G s b) SOUPed.\\
G   & (S C, C) Canceled (only for your own articles).\\
O   & {\bf Old} (marked read in a previous session).\\
K   & (k, M k; C-k, M K) {\bf Killed}.\\
M   & Marked by duplicate elimination.\\
Q   & Divined through the building of sparse threads.\\
R   & {\bf Read} (actually viewed).\\
X   & Killed by a kill file.\\
Y   & Killed due to low score.\\
\hline
%\multicolumn{2}{c}{\vspace{0.2ex}}\\
\hline
\multicolumn{2}{|p{\markdblcolwidth}|}
{{\bf Other marks}}\\
\hline
\#  & (\#, M P p) Processable (will be affected by the next operation).\\ %(M \#)
*   & (*) Persistent (or cached for some other reason).\\
A   & {\bf Answered} (followed-up or replied).\\
S   & (O {\bf x}) {\bf Saved}.\\
+   & Over default score.\\
$-$ & Under default score.\\
=   & Has children (thread underneath it). Add `\%e' to `gnus-summary-line-format'.\\
\hline
\end{tabular}
}}

\def\General{\sec{(H) General and Help Commands}{\samepage
These commands work everywhere.
\begin{keys}{C-c C-i}
C-c C-i & Go to the Gnus on-line {\bf info}.\\
C-c C-b & Send a Gnus {\bf bug} report.\\
\end{keys}

These commands work in Summary mode, and most also work in Group mode.\\*
\begin{keys}{H d}
H d     & (C-c C-d) {\bf Describe} this group. [Prefix: re-read the description
from the server.]\\ 
H f     & Try to fetch the {\bf FAQ} for this group using ange-ftp.\\
H h     & Give a very short {\bf help} message.\\
H i     & (C-c C-i) Go to the Gnus online {\bf info}.\\
H v     & Display the Gnus {\bf version} number.\\
\end{keys}}}

\def\GroupMode{\sec{Group Mode}
\begin{keys}{C-c M-C-x}
RET     & (=) Select this group [Prefix: how many (read) articles to fetch.
          Positive: newest articles, negative: oldest ones, C-u: ask number]. 
          Also: fold/unfold a topic.\\ 
SPC     & Select this group and display the first unread article [Same
          prefix as above]. Also: fold/unfold a topic.\\ 
M-RET   & Select this group quickly (no scoring, expunging, etc).\\
M-SPC   & Select this group, and don't hide dormant articles.\\
M-C-RET & Select this group ephemerally: no processing and no permanent effect.\\
?       & Give a very short help message.\\
$<$     & Go to the beginning of the Group buffer.\\
$>$     & Go to the end of the Group buffer.\\
,       & Jump to the lowest-level group with unread articles.\\
.       & Jump to the first group with unread articles.\\
^       & Enter the Server buffer mode.\\
a       & Post an {\bf article} to a group.\\
b       & Find {\bf bogus} groups and delete them.\\
c       & Mark all unticked articles in this group as read ({\bf catch-up}).
[p/p]\\ 
g       & Check the server for new articles ({\bf get}). [level]\\
j       & {\bf Jump} to a specified group (may even be killed).\\
m       & {\bf Mail} a message to someone.\\
n       & Go to the {\bf next} group with unread articles. [distance]\\
p       & (DEL) Go to the {\bf previous} group with unread articles.
[distance]\\ 
q       & {\bf Quit} Gnus. M-x gnus-unload to remove it from memory.\\
r       & {\bf Re-read} the init file `.gnus'.\\
s       & {\bf Save} the `.newsrc.eld' file (and `.newsrc' if
`gnus-save-newsrc-file').\\ 
t       & Toggle {\bf Topic} mode. A topic is a hierarchy of groups.\\
z       & Suspend Gnus (kill all buffers except the Group buffer).\\
B       & {\bf Browse} a foreign server (specify method and name).\\
C       & Mark all articles in this group as read ({\bf Catch-up}). [p/p]\\
F       & {\bf Find} new groups and process them. [Prefix: ask-server]\\
N       & Go to the {\bf next} group. [distance]\\
P       & Go to the {\bf previous} group. [distance]\\
Q       & {\bf Quit} Gnus without saving any .newsrc files.\\
R       & {\bf Restart} Gnus.\\
V       & Display the Gnus {\bf version} number.\\
W f     & {\bf Flush} the score caches of all groups.\\
Z       & Clear the dribble buffer.\\
C-c C-s & {\bf Sort} the groups according to `gnus-group-sort-function'.\\
C-c C-x & Run all expirable articles in this group through the {\bf expiry} 
process.\\
C-c M-C-x & Run all articles in all groups through the {\bf expiry} process.\\
C-c M-g & Activate absolutely all groups.\\
C-x C-t & {\bf Transpose} two groups.\\
M-\&    & Execute a command on all process-marked groups.\\
M-c     & {\bf Clear} this group's data (read and other marks). [p/p]
Use M-x gnus-group-clear-data-on-native-groups to clear all data if you change
servers and article numbers don't match. Or use
M-x gnus-change-server to renumber the articles, but it is slow.\\
\end{keys}
\begin{keys}{C-c M-C-x}
\newlength{\keywidth}\settowidth{\keywidth}{C-c M-C-x}%
\makebox[\keywidth][l]%
{M-d}   & {\bf Describe} {\em all\/} groups. [Prefix: re-read the description
from the server]\\
M-f     & Fetch this group's {\bf FAQ} (using ange-ftp).\\
M-g     & Check the server for new articles in this group ({\bf get}). [p/p]\\
M-n     & Go to the {\bf previous} unread group on the same or lower level.
[distance]\\ 
M-p     & Go to the {\bf next} unread group on the same or lower level.
[distance]\\ 
\end{keys}}

\def\ListGroups{\subsec{(A) List Groups}{\samepage
\begin{keys}{A m}
A a     & (C-c C-a) List all groups whose names match a regexp ({\bf
apropos}).\\ 
A d     & List all groups whose names or {\bf descriptions} match a regexp.\\ 
A k     & List all {\bf killed} groups. [Prefix: all groups but [un]subscribed]\\ % (C-c C-l)
A l     & List unread groups on a specific {\bf level}. [Prefix: also read groups]\\
A m     & List groups that {\bf match} a regexp and have unread articles.
[level]\\ 
A s     & (l) List {\bf subscribed} groups with unread articles. [level]\\
A u     & (L) List all groups (including {\bf unsubscribed}). [level, default 7]\\ 
A z     & List the {\bf zombie} groups.\\
A A     & List all available {\bf active} groups on all servers (may be slow).\\
A M     & List groups that {\bf match} a regexp.\\
A T     & List all active groups arranged in {\bf topics}.\\
\end{keys}}}

\def\CreateGroups{\subsec{(G) Create/Edit Foreign Groups}{\samepage
The select methods are indicated in parentheses.\\*
\begin{keys}{G DEL}
G DEL   & {\bf Delete} this group. [Prefix: delete {\bf all} articles too]\\
G a     & Make the Gnus list {\bf archive} group. (nndir over ange-ftp)\\
G c     & {\bf Customize} this group's parameters.\\
G d     & Make a {\bf directory} group (every file a posting with numeric name). (nndir)\\
G e     & {\bf Edit} this group's select method.\\ % (M-e)
G f     & Make a group based on a {\bf file}. (nndoc: mbox, babyl, digest,
          mmdf, news, rnews, clari-briefs, rfc934, rfc822, forward) [Prefix: 
          don't guess the type]\\
G h     & Make the Gnus {\bf help} (documentation/tutorial) group. (nndoc)\\
G k     & Make a {\bf kiboze} group (specify name, groups, regexps). (nnkiboze)\\
G m     & {\bf Make} a new group (specify name, method, address).\\
G p     & Edit this group/topic's {\bf parameters}.\\
G r     & {\bf Rename} this group (only for mail groups).\\
G v     & Add this group to a {\bf virtual} group. [p/p]\\
G w     & Make a {\bf web}-based group. (nnweb: dejanews, altavista,
          reference) [Prefix: permanent group]\\
G D     & Enter a {\bf directory} as a temporary group. (nneething without
recording read marks.)\\
G E     & {\bf Edit} this group's info (select method, articles read, etc).\\
G V     & Make an empty {\bf virtual} group. (nnvirtual)\\
\end{keys}
You can also create mail-groups and read your mail with Gnus (very useful
if you are subscribed to any mailing lists), using one of the methods
nnmbox, nnbabyl, nnml, nnmh, or nnfolder. Read about it in the online info
(C-c C-i g Reading Mail RET).
}}

\def\SortGroups{\subsubsec{(G S) Sort Groups}{\samepage
To sort by multiple criteria, first apply the less significant ones, last the
most significant one.\\*
\begin{keys}{G P \bf x}
G S a     & Sort {\bf Alphabetically} by group name. [Prefix: reverse order]\\
G S l     & Sort by {\bf level}. [Prefix: reverse order]\\
G S m     & Sort by {\bf method} name. [Prefix: reverse order]\\
G S r     & Sort by rank (level and score). [Prefix: reverse order]\\
G S u     & Sort by number of {\bf unread} articles. [Prefix: reverse order]\\
G S v     & Sort by group score ({\bf value}). [Prefix: reverse order]\\
\end{keys}
Here {\bf x} can be any one of a,l,m,r,u,v:\\*
\begin{keys}{G P \bf x}
G P \bf x & Sort only the groups selected by [p/p].\\
T S \bf x & Sort only the current topic.\\
\end{keys}
}}

\def\SOUP{\subsubsec{(G s) SOUP Commands}{\samepage
SOUP is a protocol for putting many articles/replies in a packet, transferring
them in bulk and reading/composing them off-line. Use the following commands to
manipulate the packets, and use the nnsoup method to read.\\*
\begin{keys}{G s w}
G s b   & Pack all unread articles ({\bf brew} soup). [p/p]\\
G s p   & {\bf Pack} all SOUP data files into a SOUP packet.\\
G s r   & Pack all {\bf replies} into a replies packet.\\
G s s   & {\bf Send} all replies you put in the replies packet.\\
G s w   & {\bf Write} all SOUP data files.\\
\end{keys}}}

\def\MarkGroups{\subsec{(M) Mark Groups}{\samepage
\begin{keys}{M m}
M b     & Set the process mark on all groups in the {\bf buffer}.\\
M r     & Mark all groups matching a {\bf regexp}.\\
M m     & (\#) {\bf Mark} this group. [scope]\\
M u     & (M-\#) {\bf Unmark} this group. [scope]\\ 
M w     & Mark all groups in the current region.\\
M U     & {\bf Unmark} all groups in the buffer.\\
\end{keys}}}

\def\Unsubscribe{\subsec{(S) (Un)subscribe, Kill and Yank Groups}{\samepage
\begin{keys}{S C-k}
S C-k   & {\bf Kill} all groups on a certain level.\\
S k     & (C-k) {\bf Kill} this group/ topic (and all its groups).\\
S l     & Set the {\bf level} of this group. [p/p]\\
S s     & (U) Prompt for a group and toggle its {\bf subscription}.\\
S t     & (u) {\bf Toggle} subscription to this group. [p/p]\\
S w     & (C-w) Kill all groups in the region.\\
S y     & (C-y) {\bf Yank} the last killed group/topic.\\
S z     & Kill all {\bf zombie} groups.\\
\end{keys}}}

\def\GroupTopics{\subsec{(T) Group Topics}{\samepage
Topics are hierarchical arrangements of groups with the purpose of easier
reading and management.
\begin{keys}{T TAB}
T TAB   & Indent this topic to become a sub-topic of the previous one. 
          [Prefix: un-dent]\\
T DEL   & {\bf Delete} an empty topic.\\
T \#    & Mark all groups in this topic with the process mark.\\
T M-\#  & Unmark all groups in this topic with the process mark.\\
T c     & {\bf Copy} this group to another topic. [p/p]\\
T h     & Toggle the {\bf hiding} of empty topics.\\
T m     & {\bf Move} this group to another topic. [p/p]\\
T n     & Create a {\bf new} topic.\\
T r     & {\bf Rename} a topic.\\
T C     & {\bf Copy} all groups that match a regexp to a topic.\\
T D     & {\bf Delete} this group from this topic. [p/p]\\
T M     & {\bf Move} all groups that match a regexp to a topic.\\
\end{keys}}}

\def\SummaryMode{\sec{Summary Mode}{\samepage
\begin{keys}{M-C-d}
SPC     & Select an article, scroll it one page, move to the next one.\\  
%(A SPC, A n)
DEL     & (b) Scroll this article one page back.  [distance]\\ %(A DEL, A p)
RET     & Scroll this article one line forward. [distance]\\
=       & Expand the Summary window. [Prefix: shrink it to display the
Article window]\\
$<$     & (A $<$) Scroll to the beginning of this article.\\ %(A b)
$>$     & (A $>$) Scroll to the end of this article.\\ %(A e)
\&      & Execute a command on all articles matching a regexp. 
[Prefix: move backwards.]\\
k       & Give a GroupLens {\bf rating} (1-5) to this thread.\\ 
r       & Give a GroupLens {\bf rating} (1-5) to this article.\\ 
Y g     & Re{\bf generate} the summary buffer.\\
Y c     & Pull all {\bf cached} articles into the summary.\\
C-t     & Toggle {\bf truncation} of summary lines.\\
C-d     & Un-{\bf digestify} this article into a separate group.\\ %(A D)
M-C-d   & Un-{\bf digestify} all selected articles into one group. [p/p]\\
M-\&    & Execute a command on all articles having the process mark.\\
M-k     & Edit this group's {\bf kill} file.\\
M-r     & Search through previous articles for a regexp.\\
M-s     & {\bf Search} through subsequent articles for a regexp.\\
M-K     & Edit the general {\bf kill} file.\\
\end{keys}}}

\def\SortSummary{\subsec{(C-c C-s) Sort the Summary}{\samepage
In thread mode, these commands sort only the thread roots.\\*
\begin{keys}{C-c C-s C-a}
C-c C-s C-a & Sort the summary by {\bf author}.\\
C-c C-s C-d & Sort the summary by {\bf date}.\\
C-c C-s C-i & Sort the summary by article score.\\
C-c C-s C-l & Sort the summary by number of {\bf lines}.\\
C-c C-s C-n & Sort the summary by article {\bf number}.\\
C-c C-s C-s & Sort the summary by {\bf subject}.\\
\end{keys}}}

\def\Article{\subsec{(A) Article Commands}{\samepage
\begin{keys}{A m}
A g  & (g) (Re){\bf get} this article. [Prefix: don't do any processing]\\ 
A r  & (^) Go to the parent of this article ({\bf References} header).
[Prefix: how many ancestors back; negative: only the $n$-th ancestor]\\ %(A ^)
M-^  & Go to the article with a given Message-ID.\\
A s  & (s) Perform an i{\bf search} in the article buffer.\\
A P  & {\bf Print} the article as postscript.\\
A R  & Fetch all ancestors ({\bf References}) of this article.\\
\end{keys}}}

\def\MailGroup{\subsec{(B) Mail-Group Commands}{\samepage
These commands (except `B c') are only valid in a mail group.\\*
\begin{keys}{B M-C-e}
B DEL   & {\bf Delete} the mail article from disk. [p/p]\\
B c     & {\bf Copy} this article from any group to a mail group. [p/p]\\
B e     & {\bf Expire} all expirable articles in this group. [p/p]\\
B i     & {\bf Import} any file into this mail group (give From and Subject).\\
B m     & {\bf Move} the article from one mail group to another. [p/p]\\
B p     & Check if a courtesy copy of a message was also {\bf posted}.\\
B q     & {\bf Query} where would the article go if respooled.\\
B r     & {\bf Respool} this mail article. [p/p]\\
B w     & (e) Edit this mail article.\\
B M-C-e & {\bf Expunge} (from disk) all expirable articles in this group. [p/p]\\ 
B C     & {\bf Crosspost} this article to another group.\\
\end{keys}}}

\def\GotoArticle{\subsec{(G) Go To Article}{\samepage
These commands select the target article. They do not understand the prefix.\\*
\begin{keys}{G C-n}
G b     & (,) Go to the {\bf best} article (the one with highest score). [GL rating]\\
G f     & (.) Go to the {\bf first} unread article.\\
G g     & Ask for an article number and then {\bf go to} to that summary
line.\\ 
G j     & (j) Ask for an article number and then {\bf jump} to that article.\\ 
G l     & (l) Go to the {\bf last} article read.\\
G o     & Pop an article off the summary history and go to it.\\
G n     & (n) Go to the {\bf next} unread article. [GL rating]\\
G p     & (p) Go to the {\bf previous} unread article.\\
G N     & (N) Go to {\bf the} next article.\\
G P     & (P) Go to the {\bf previous} article.\\
G C-n   & (M-C-n) Go to the {\bf next} article with the same subject.\\
G C-p   & (M-C-p) Go to the {\bf previous} article with the same subject.\\
G M-n   & (M-n) Go to the {\bf next} summary line of an unread article.
[distance]\\ 
G M-p   & (M-p) Go to the {\bf previous} summary line of an unread article. 
[distance]\\ 
\end{keys}}}

\def\MarkArticles{\subsec{(M) Mark Articles}{\samepage
\begin{keys}{M M-C-r}
d       & (M d, M r) Mark this article as {\bf read} ({\bf delete} it) 
and move to the next one. [scope]\\ 
D       & Mark this article as read and move to the previous one. [scope]\\
u       & (!, M t) {\bf Tick} this article (mark it as interesting)
and move to the next one. [scope]\\ %(M !)
U       & Tick this article and move to the previous one. [scope]\\ 
M-u     & (M SPC, M c) {\bf Clear} all marks from this article 
and move to the next one. [scope]\\ 
M-U     & Clear all marks from this article and move to the previous one.
[scope]\\ 
M ?     & (?) Mark this article as dormant (only followups are
interesting). [scope]\\ 
*       & Make this article persistent. [p/p]\\
M-*     & Make this article non-persistent (and delete it). [p/p]\\
M b     & Set a {\bf bookmark} in this article.\\
M e     & (E) Mark this article as {\bf expirable}. [scope]\\ %(M x)
M k     & (k) {\bf Kill} all articles with the same subject then select the
next one.\\ 
M B     & Remove the {\bf bookmark} from this article.\\
M C     & {\bf Catch-up} (mark read) the articles that are not ticked.\\
M D     & Show all {\bf dormant} articles (normally they are hidden unless they
have any followups).\\
M H     & Catch-up (mark read) this group to point ({\bf here}).\\
M K     & (C-k) {\bf Kill} all articles with the same subject as this one.\\
C-w     & Mark all articles between point and mark as read.\\
M S     & (C-c M-C-s) {\bf Show} all expunged articles.\\
M C-c   & {\bf Catch-up} all articles in this group.\\
M M-r   & (x) Expunge all {\bf read} articles from this group.\\
M M-D   & Hide all {\bf dormant} articles.\\
M M-C-r & Expunge all articles having a given mark.\\
\end{keys}}}

\def\MarkScore{\subsubsec{(M V) Mark Based on Score (Value)}{\samepage
\begin{keys}{M s m}
M V c   & {\bf Clear} all marks from all high-scored articles. [score]\\
M V k   & {\bf Kill} all low-scored articles. [score]\\
M V m   & Mark all high-scored articles with a given {\bf mark}. [score]\\
M V u   & Mark all high-scored articles as interesting (tick them). [score]\\
\end{keys}}}

\def\ProcessMark{\subsubsec{(M P) The Process Mark}{\samepage 
These commands set and remove the process mark \#. You only need to use
it if the set of articles you want to operate on is non-contiguous. Else
use a numeric prefix.\\*
\begin{keys}{M P R}
M P a   & Mark {\bf all} articles (in series order).\\
M P b   & Mark all articles in the order they appear in the {\bf buffer}.\\
M P i   & {\bf Invert} all process marks.\\
M P k   & Push all marks on a stack then {\bf kill} them.\\
M P p   & (\#) Mark this article.\\  %(M \#)
M P r   & Mark all articles in the {\bf region}.\\
M P s   & Mark all articles in the current {\bf series}.\\
M P t   & Mark all articles in this (sub){\bf thread}.\\
M P u   & (M-\#) {\bf Unmark} this article.\\ %(M M-\#)
M P v   & Mark all high-scored articles ({\bf value}). [score]\\
M P w   & Push all marks on a stack.\\
M P y   & {\bf Yank} and restore a mark set from the stack after `M P y', 
`M P w' or an operation that cleared the marks.\\
M P R   & Mark all articles matching a {\bf regexp}.\\
M P S   & Mark all {\bf series} that already contain a marked article.\\
M P T   & Unmark all articles in this (sub){\bf thread}.\\
M P U   & {\bf Unmark} all articles.\\
\end{keys}}}

\def\OutputArticles{\subsec{(O) Output Articles}{\samepage
\begin{keys}{O m}
O b     & Save the article {\bf body} in a plain file. [p/p]\\
O f     & Save this article in a plain {\bf file}. [p/p]\\
O h     & Save this article in {\bf mh} folder format. [p/p]\\
O m     & Save this article in {\bf mail} format. [p/p]\\
O o     & (o, C-o) Save this article using the default article saver. [p/p]\\
O p     & ($\mid$) Pipe this article to a shell command. [p/p]\\ 
O r     & Save this article in {\bf rmail} format. [p/p]\\
O s     & Add this article to a {\bf SOUP} packet. [p/p]\\
O v     & Save this article in {\bf vm} format. [p/p]\\
O F     & Save this article as plain {\bf file} and overwrite any existing file. [p/p]\\
\end{keys}}}

\def\Send{\subsec{(S) Post, Followup, Reply, Forward, Cancel}{\samepage
These commands put you in a separate Message buffer. After editing the
article, send it by pressing C-c C-c.  If you are in a foreign group and want
to post the article using the foreign server, give a prefix to C-c C-c.
Set `gnus-post-method' to `nngateway' if your server cannot post [temporarily]
and you wan to post through a mail-to-news gateway.\\* 
\begin{keys}{S O m}
S b     & {\bf Both} post a followup to this article, and send a reply.\\
S c     & (C) {\bf Cancel} this article (only works if it is your own).\\
S f     & (f) Post a {\bf followup} to this article.\\
S m     & (m) Send a {\bf mail} to someone.\\
S n     & Followup in a {\bf newsgroup} to a message you got through mail.\\
S o m   & (C-c C-f) Forward this article by {\bf mail} to a person. 
[Prefix: include full headers]\\
S o p   & Forward this article as a {\bf post} to a newsgroup.\\
S p     & (a) {\bf Post} an article to this group.\\
S r     & (r) Mail a {\bf reply} to the author of this article.\\
S s     & {\bf Supersede} this article with a new one (only for own articles).\\ 
S u     & {\bf Uuencode} a file, split it into series and post it.\\
S w     & Mail a {\bf wide} reply to the author and the recipients of this
article.\\
S B     & {\bf Both} post a followup, send a reply, and include the
original. [p/p]\\ 
S D b   & Resend a {\bf bounced} mail after fixing the recipient address.
[Prefix: display the headers of the message this one is a reply to]\\
S D r   & {\bf Resend} this message to another of your addresses.\\
S F     & (F) Post a {\bf followup} and include the original. [p/p]\\
S N     & Followup in a {\bf newsgroup} to a mail message and include the
original [p/p].\\ 
S O m   & Digest these series and forward by {\bf mail}. [p/p]\\
S O p   & Digest these series and forward as a {\bf post} to a newsgroup.
[p/p]\\ 
S R     & (R) Mail a {\bf reply} and include the original. [p/p]\\
S W     & Mail a {\bf wide} reply and include the original. [p/p]\\
S M-c   & Send a {\bf complaint} about excessive {\bf crossposting}. [p/p]\\
\end{keys}
If you want to cancel or supersede an article you just posted (before it
has appeared on the server), go to the *sent \ldots* buffer, change
`Message-ID' to `Cancel' or `Supersedes' and send it again with C-c C-c.
}}

\def\Thread{\subsec{(T) Thread Commands}{\samepage
\begin{keys}{T M-\#}
T ^     & Make this article child of the previous or precess-marked one.\\
T \#    & Mark this (sub)thread with the process mark.\\
T M-\#  & Remove the process mark from this (sub)thread.\\
T d     & Move {\bf down} this thread. [distance]\\
T h     & {\bf Hide} this (sub)thread.\\
T i     & {\bf Increase} the score of this thread.\\
T k     & (M-C-k) {\bf Kill} the current (sub)thread. [Prefix:
negative--tick, positive--unmark]\\
T l     & (M-C-l) {\bf Lower} the score of this thread.\\
T n     & (M-C-f) Go to the {\bf next} thread. [distance]\\
T o     & Go to the {\bf top} of this thread.\\
T p     & (M-C-b) Go to the {\bf previous} thread. [distance]\\
T s     & {\bf Show} the thread hidden under this article.\\
T t     & Re-{\bf thread} this thread.\\
T u     & Move {\bf up} this thread. [distance]\\
T H     & {\bf Hide} all threads.\\
T S     & {\bf Show} all hidden threads.\\
T T     & (M-C-t) {\bf Toggle} threading.\\
\end{keys}}}

\def\Score{\subsec{(V) Score (Value) Commands}{\samepage
Read about Adaptive Scoring in the online info.\\*
\begin{keys}{\bf A p m l}
V a     & {\bf Add} a new score entry, specifying all elements.\\
V c     & Select a new score file as {\bf current}.\\
V e     & {\bf Edit} the current score file.\\
V f     & Edit a score {\bf file} and make it the current one.\\
V m     & {\bf Mark} all articles below a given score as read [score].\\
V s     & {\bf Set} the score of this article.\\
V t     & {\bf Trace} all score rules applied to this article.\\
V x     & {\bf Expunge} all low-scored articles. [score]\\
V F     & {\bf Flush} the score cache to put it in sync with files.\\
V C     & {\bf Customize} the current score file using a user-friendly
interface.\\ 
V S     & {\bf Show} the score of this article.\\
I C-i   & {\bf Increase} the score of this article.\\
L C-l   & {\bf Lower} the score of this article.\\
\bf A p m l& Make a scoring entry based on this article.\\
\end{keys}

The four letters stand for:\\*
\quad \B{A}ction: I)ncrease, L)ower;\\*
\quad \B{p}art: a)uthor (from), b)ody, d)ate, f)ollowups, h)ead (all headers),
message-i)d, l)ines, s)ubject, t)hread (references), x)refs (cross-posting);\\*
\quad \B{m}atch type:\\*
\qquad string: e)xact, f)uzzy, r)egexp, s)ubstring\\*
\qquad date: a)t, b)efore, n)this,\\*
\qquad number: $<$, =, $>$;\\*
\quad \B{l}ifetime: t)emporary, p)ermanent, i)mmediate.

If you type the second/third letter in uppercase, the remaining letters are
assumed to be s)ubstring and t)emporary.

\quad Extra keys for manual editing of a score file:\\*
\begin{keys}{C-c C-c}
C-c C-c & Finish editing the score file.\\
C-c C-d & Insert the current {\bf date} as number of days.\\
C-c C-p & {\bf Pretty-print} the [adaptive] score file.\\
\end{keys}}}

\def\Wash{\subsec{(W) Wash the Article Buffer}{\samepage
\begin{keys}{W C-c}
W b     & Make Message-IDs, URLs, citations and signature to mouse-clickable 
{\bf buttons}.\\  
W c     & Remove extra {\bf CR}s (^M).\\
W e     & Fontify {\bf emphasis}, e.g.{} *this* and \_that\_.\\
W f     & Display any X-{\bf Face} headers.\\
W l     & (w) Remove page breaks ({\bf^L}).\\
W m     & Toggle {\bf MIME} processing before displaying.\\
W o     & Treat {\bf overstrike} (a^Ha) or underline (a^H\_).\\
W q     & Treat {\bf quoted}-printable (=0D etc).\\
W r     & (C-c C-r) Caesar-{\bf rotate} (rot13 decode).\\
W t     & (t) {\bf Toggle} the displaying of all headers.\\
W v     & (v) Toggle permanent {\bf verbose} displaying of all headers.\\
W w     & Do word {\bf wrap}. [Prefix: width to use for filling]\\
W B     & Make headers to mouse-clickable {\bf buttons}.\\
W E a   & Expunge {\bf all} unwanted blank lines.\\
W E l   & Expunge {\bf leading} blank lines.\\
W E m   & Expunge {\bf multiple} blanks lines and replace them with a single
blank.\\
W E s   & Expunge leading {\bf spaces} from every line.\\
W E t   & Expunge {\bf trailing} blank lines.\\
W T e   & Display the date as time {\bf elapsed} since sent.\\
W T l   & Display the date in the {\bf local} timezone.\\
W T o   & Display the {\bf original} date.\\
W T s   & Display the date using `gnus-article-time-format'.\\
W T u   & (W T z) Display the date as {\bf UTC} (aka GMT, Zulu).\\
\end{keys}}}

\def\Hide{\subsubsec{(W W) Hide Parts of the Article}{\samepage
Without prefix, these commands toggle hiding; a positive prefix hides and a
negative prefix unhides.\\*
\begin{keys}{W W C}
W W a   & Hide {\bf all} unwanted parts.\\
W W c   & Hide {\bf cited} text.\\
W W h   & Hide the {\bf headers}.\\
W W s   & Hide the {\bf signature}.\\
W W p   & Hide {\bf PGP} signatures.\\
W W C   & Hide {\bf cited} text if the article is not a root of a thread.\\
W W P   & Hide {\bf PEM} gruft.\\
\end{keys}}}

\def\Highlight{\subsubsec{(W H) Highlight Parts of the Article}{\samepage
\begin{keys}{W H A}
W H a   & Highlight {\bf all} parts.\\
W H c   & Highlight {\bf cited} text.\\
W H h   & Highlight the {\bf headers}.\\
W H s   & Highlight the {\bf signature}.\\
\end{keys}}}

\def\Extract{\subsec{(X) Extract Series (Uudecode etc)}{\samepage
Gnus recognizes if the current article is part of a series (multipart
posting whose parts are identified by numbers in the subject, e.g.{}
1/10\dots10/10). You can mark and process more than one series at a time. If
the posting contains any archives, they are expanded and gathered in a new
group.\\* 
\begin{keys}{X p}
X b     & Un-{\bf binhex} these series. [p/p]\\
X o     & Simply {\bf output} these series (no decoding). [p/p]\\ 
X p     & Unpack these {\bf postscript} series. [p/p]\\
X s     & Un-{\bf shar} these series. [p/p]\\
X u     & {\bf Uudecode} these series. [p/p]\\
\end{keys}

Each one of these commands has four variants:\\*
\begin{keys}{X v \bf Z}
X   \bf z & Decode these series. [p/p]\\
X   \bf Z & Decode and save these series. [p/p]\\
X v \bf z & Decode and view these series. [p/p]\\
X v \bf Z & Decode, save and view these series. [p/p]\\
\end{keys}
where {\bf z} or {\bf Z} identifies the decoding method (b,o,p,s,u).

An alternative binding for the most-often used of these commands is\\*
\begin{keys}{C-c C-v C-v}
C-c C-v C-v & (X v u) Uudecode and view these series. [p/p]\\
\end{keys}
Use `M-x gnus-binary-mode' to do decoding automatically (then use `g' to see
the original text of the article).
}}

\def\Exit{\subsec{(Z) Exit the Current Group}{\samepage
\begin{keys}{Z G}
Z c     & (c) Mark all unticked articles as read ({\bf catch-up}) and exit.\\
Z n     & Mark all articles as read and go to the {\bf next} group.\\
Z s     & {\bf Save} this group's info in the dribble file. [Prefix: also save .newsrc]\\
Z C     & Mark all articles as read ({\bf catch-up}) and exit.\\
Z E     & (Q) {\bf Exit} without updating the group information.\\
Z G     & (M-g) Check for new articles in this group ({\bf get}).\\
Z N     & Exit and go to the {\bf next} group.\\
Z P     & Exit and go to the {\bf previous} group.\\
Z R     & Exit this group, then ({\bf reenter}) it.
[Prefix: select all articles, read and unread.]\\
Z Z     & (q, Z Q) Exit this group.\\
\end{keys}}}

\def\Limit{\subsec{(/) Limiting the Summary}{\samepage
\begin{keys}{/ D}
/ a  & Limit to a given {\bf author}.\\
/ c  & Hide dormant articles that have no {\bf children} (follow-ups).\\
/ d  & Hide all {\bf dormant} articles.\\
/ m  & Limit to articles not having a given {\bf mark}.\\
/ n  & Limit to the current article.\\
/ s  & (/ /) Limit to a given {\bf subject}.\\
/ t  & Limit by {\bf time} (older than a given number of days). [Prefix: newer]\\
/ u  & (x) Limit to {\bf unread} articles. [Prefix: also remove ! and ? articles]\\
/ v  & Limit to high-scored articles ({\bf value}). [score]\\
/ w  & Pop and restore the previous limit from a stack. [Prefix: pop all limits]\\
/ C  & {\bf Catch-up} all unread articles outside the limit. [Prefix: also ! and ? articles]\\
/ D  & Show all {\bf dormant} articles.\\
/ E  & (M S) {\bf Show} all {\bf expunged} articles.\\
\end{keys}}}

\def\PickAndRead{\subsec{Pick-and-Read Mode}{\samepage
Use `M-x gnus-pick-mode' to first choose the articles you like and
only read them after.\\*
\begin{keys}{SPC}
SPC  & Scroll the summary a page, if at the end start reading.\\
RET  & Start reading. [Prefix: catch-up all non-picked articles]\\
.    & Pick this article. [Prefix: pick the article with that number]\\
r    & Pick all articles in the {\bf region}.\\
R    & Unpick all articles in the {\bf region}.\\
t    & Pick this {\bf thread}.\\
u    & {\bf Unpick} this article.\\
T    & Unpick this {\bf thread}.\\
U    & {\bf Unpick} all articles.\\
e    & Pick articles that match a regexp.\\
b    & Pick all articles in the {\bf buffer}.\\
B    & Unpick all articles in the {\bf buffer}.\\
\end{keys}}}

\def\ArticleMode{\sec{Article Mode}{\samepage
Most keys in Summary mode also work in Article mode (of course, the normal
navigation keys operate on the Article buffer). Additional keys:\\*
\begin{keys}{C-c C-m}
RET     & (middle mouse button) Activate the button at point to follow
an URL or Message-ID, hide citation/signature, etc.\\
TAB     & Move point to the next button.\\
M-TAB   & Move point to the previous button.\\
?       & Give a brief help message.\\
h       & (s) Go to the {\bf header} line of the article in the {\bf
summary} buffer.\\
C-c ^   & (r) Get the article with Message-ID near point ({\bf refer}).\\
C-c C-m & {\bf Mail} reply to the address near point. [Prefix: cite the
article]\\ 
\end{keys}}}

\def\ServerMode{\sec{Server Mode}{\samepage
To enter this mode, press `^' while in Group mode.\\*
\begin{keys}{SPC}
SPC     & (RET) Browse this server.\\
a       & {\bf Add} a new server.\\
c       & {\bf Copy} this server.\\
e       & {\bf Edit} a server.\\
g       & Ask the server to re-{\bf generate} all its data structures.\\
k       & {\bf Kill} this server. [scope]\\
l       & {\bf List} all servers.\\
q       & Return to the group buffer ({\bf quit}).\\
s       & Ask the server to {\bf scan} for new messages.\\
y       & {\bf Yank} the previously killed server.\\
C       & {\bf Close} the connection to this server.\\
O       & (Re-){\bf Open} a connection to this server.\\
D       & Mark this server as unavailable ({\bf deny} it).\\
R       & {\bf Reset} all unavailability (denial) marks.\\
M-c     & {\bf Close} the connections to all servers.\\
M-o     & (Re-){\bf Open} connections to all servers.\\
\end{keys}}}

\def\BrowseServer{\sec{Browse Server Mode}{\samepage
To enter this mode, press `B' in Group mode.\\*
\begin{keys}{RET}
RET     & Enter the current group.\\
SPC     & Enter the current group and display the first article.\\
?       & Give a very short help message.\\
n       & Go to the {\bf next} group. [distance]\\
p       & Go to the {\bf previous} group. [distance]\\
q       & (l) {\bf Quit} browse mode.\\
u       & {\bf [Un]Subscribe} to the current group. [scope]\\
\end{keys}}}

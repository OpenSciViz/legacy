This is Info file cc-mode.info, produced by Makeinfo version 1.68 from
the input file cc-mode.texi.

   Copyright (C) 1995,96,97,98 Free Software Foundation, Inc.


File: cc-mode.info,  Node: Permanent Customization,  Next: Styles,  Prev: Interactive Customization,  Up: Customizing Indentation

Permanent Customization
=======================

   To make your changes permanent, you need to add some lisp code to
your `.emacs' file, but first you need to decide whether your styles
should be global and shared in all buffers, or local to each specific
buffer.

   If you edit primarily one style of code, you may want to make the CC
Mode style variables have global values so that every buffer will share
the same style settings.  This will allow you to set the CC Mode
variables at the top level of your `.emacs' file, and is the way CC
Mode works by default.

   If you edit many different styles of code at the same time, you
might want to make the CC Mode style variables have buffer local
values.  If you do this, then you will need to set any CC Mode style
variables in a hook function (e.g. off of `c-mode-common-hook' instead
of at the top level of your `.emacs' file).  The recommended way to do
this is to set the variable `c-style-variables-are-local-p' to `t'
*before* CC Mode is loaded into your Emacs session.  Note that once the
style variables are made buffer-local, they cannot be made global
again, without restarting Emacs.

   CC Mode provides several hooks that you can use to customize the
mode according to your coding style.  Each language mode has its own
hook, adhering to standard Emacs major mode conventions.  There is also
one general hook and one package initialization hook:

   * `c-mode-hook' -- for C buffers only

   * `c++-mode-hook' -- for C++ buffers only

   * `objc-mode-hook' -- for Objective-C buffers only

   * `java-mode-hook' -- for Java buffers only

   * `idl-mode-hook' -- for CORBA IDL buffers only

   * `pike-mode-hook' -- for Pike buffers only

   * `c-mode-common-hook' -- common across all languages

   * `c-initialization-hook' -- hook run only once per Emacs session,
     when CC Mode is initialized.

   The language hooks get run as the last thing when you enter that
language mode.  The `c-mode-common-hook' is run by all supported modes
*before* the language specific hook, and thus can contain
customizations that are common across all languages.  Most of the
examples in this section will assume you are using the common hook(1).

   Here's a simplified example of what you can add to your `.emacs'
file to make the changes described in the previous section more
permanent.  See the Emacs manuals for more information on customizing
Emacs via hooks.  *Note Sample .emacs File:: for a more complete sample
`.emacs' file.

     (defun my-c-mode-common-hook ()
       ;; my customizations for all of c-mode and related modes
       (c-set-offset 'substatement-open 0)
       ;; other customizations can go here
       )
     (add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

   If you are sharing a single style across all CC Mode buffers and want
to make these changes in the top level of your `.emacs' file, you can
add this instead:

     (setq c-default-style '((other . "user")))
     (c-set-offset 'substatement-open 0)

   Any time you make CC Mode customizations at the top-level of your
`.emacs' file, either through the use of `setq''s or as above, or when
you use the Custom interface, you will need to include the first line.
*Note Built-in Styles:: for details.

   For complex customizations, you will probably want to set up a
*style* that groups all your customizations under a single name.

   ---------- Footnotes ----------

   (1) `java-mode' and the hook variables interact in a slightly
different way than the other modes.  `java-mode' sets the style of the
buffer to `java' *before* running the `c-mode-common-hook' or
`java-mode-hook'.  You need to be aware of this so that style settings
in `c-mode-common-hook' don't clobber your Java style.


File: cc-mode.info,  Node: Styles,  Next: Advanced Customizations,  Prev: Permanent Customization,  Up: Customizing Indentation

Styles
======

   Most people only need to edit code formatted in just a few
well-defined and consistent styles.  For example, their organization
might impose a "blessed" style that all its programmers must conform
to.  Similarly, people who work on GNU software will have to use the
GNU coding style.  Some shops are more lenient, allowing a variety of
coding styles, and as programmers come and go, there could be a number
of styles in use.  For this reason, CC Mode makes it convenient for you
to set up logical groupings of customizations called "styles",
associate a single name for any particular style, and pretty easily
start editing new or existing code using these styles.

* Menu:

* Built-in Styles::
* Adding Styles::
* File Styles::


File: cc-mode.info,  Node: Built-in Styles,  Next: Adding Styles,  Up: Styles

Built-in Styles
---------------

   If you're lucky, one of CC Mode's built-in styles might be just what
you're looking for.  These include:

   * `gnu' -- coding style blessed by the Free Software Foundation for
     C code in GNU programs.

   * `k&r' -- The classic Kernighan and Ritchie style for C code.

   * `bsd' -- Also known as "Allman style" after Eric Allman.

   * `whitesmith' -- Popularized by the examples that came with
     Whitesmiths C, an early commercial C compiler.

   * `stroustrup' -- The classic Stroustrup style for C++ code.

   * `ellemtel' -- Popular C++ coding standards as defined by
     "Programming in C++, Rules and Recommendations", Erik Nyquist and
     Mats Henricson, Ellemtel (1).

   * `linux' -- C coding standard for Linux development.

   * `python' -- C coding standard for Python extension modules(2).

   * `java' -- The style for editing Java code.  Note that this style is
     automatically installed when you enter `java-mode'.

   * `user' -- This is a special style for several reasons.  First, if
     you customize CC Mode by using either the new Custom interface(3)
     or by doing `setq''s at the top level of your `.emacs' file, these
     settings will be captured in the `user' style.  Also, all other
     styles implicitly inherit their settings from `user' style.  This
     means that for any styles you add via `c-add-style' (*Note Adding
     Styles::) you need only define the differences between your new
     style and `user' style.

     Note however that `user' style is *not* the default style.  `gnu'
     is the default style for all newly created buffers, but you can
     change this by setting variable `c-default-style'.  Be careful if
     you customize CC Mode as described above; since your changes will
     be captured in the `user' style, you will also have to change
     `c-default-style' to "user" to see the effect of your
     customizations.

   `c-default-style' actually takes either a style name string, or an
association list of major mode symbols to style names.  Thus you can
control exactly which default style is used for which CC Mode language
mode.  Here are the rules:

  1. When `c-default-style' is a string, it must be an existing style
     name as found in `c-style-alist'.  This style is then used for all
     modes *except* `java-mode', where the style `java' is used by
     default(4).

  2. When `c-default-style' is an association list, the current major
     mode is looked up to find a style name string.  In this case, this
     style is always used exactly as specified and an error will occur
     if the named style does not exist.

  3. If `c-default-style' is an association list, but the current major
     mode isn't found, then the special symbol `other' is looked up.  If
     this value is found, the associated style is used.

  4. If `other' is not found, then the `gnu' style is used.

  5. In all cases, the style described in `c-default-style' is installed
     *before* the language hooks are run, so you can always override
     this setting by including an explicit call to `c-set-style' in your
     language mode hook, or in `c-mode-common-hook'.


   If you'd like to experiment with these built-in styles you can simply
type the following in a CC Mode buffer:

     C-c . STYLE-NAME RET

`C-c .' runs the command `c-set-style'.  Note that all style names are
case insensitive, even the ones you define.

   Setting a style in this way does *not* automatically re-indent your
file.  For commands that you can use to view the effect of your changes,
see *Note Commands::.

   Once you find a built-in style you like, you can make the change
permanent by adding some lisp to your `.emacs' file.  Let's say for
example that you want to use the `ellemtel' style in all your files.
You would add this:

     (defun my-c-mode-common-hook ()
       ;; use Ellemtel style for all C like languages
       (c-set-style "ellemtel")
       ;; other customizations can go here
       )
     (add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

   Note that for BOCM compatibility, `gnu' is the default style, and
any non-style based customizations you make (i.e. in
`c-mode-common-hook' in your `.emacs' file) will be based on `gnu'
style unless you do a `c-set-style' as the first thing in your hook.
The variable `c-indentation-style' always contains the buffer's current
style name, as a string.

   ---------- Footnotes ----------

   (1) This document is ftp'able from `euagate.eua.ericsson.se'

   (2) Python is a high level scripting language with a C/C++ foreign
function interface.  For more information, see
`<http://www.python.org/>'.

   (3) Available in Emacs 20, XEmacs 19.15 and XEmacs 20.

   (4) This is for backwards compatibility reasons.  The hard-coding of
`java-mode' style is admittedly bogus!


File: cc-mode.info,  Node: Adding Styles,  Next: File Styles,  Prev: Built-in Styles,  Up: Styles

Adding Styles
-------------

   If none of the built-in styles is appropriate, you'll probably want
to add a new "style definition".  Styles are kept in the
`c-style-alist' variable, but you should never modify this variable
directly.  Instead, CC Mode provides the function `c-add-style' that
you can use to easily add new styles or change existing styles.  This
function takes two arguments, a STYLENAME string, and an association
list DESCRIPTION of style customizations.  If STYLENAME is not already
in `c-style-alist', the new style is added, otherwise the style is
changed to the new DESCRIPTION.  This function also takes an optional
third argument, which if non-`nil', automatically applies the new style
to the current buffer.

   The sample `.emacs' file provides a concrete example of how a new
style can be added and automatically set.  *Note Sample .emacs File::.


File: cc-mode.info,  Node: File Styles,  Prev: Adding Styles,  Up: Styles

File Styles
-----------

   The Emacs manual describes how you can customize certain variables
on a per-file basis by including a "Local Variable" block at the end of
the file.  So far, you've only seen a functional interface to CC Mode
customization, which is highly inconvenient for use in a Local Variable
block.  CC Mode provides two variables that make it easier for you to
customize your style on a per-file basis(1)

   The variable `c-file-style' can be set to a style name string.  When
the file is visited, CC Mode will automatically set the file's style to
this style using `c-set-style'.

   Another variable, `c-file-offsets', takes an association list
similar to what is allowed in `c-offsets-alist'.  When the file is
visited, CC Mode will automatically institute these offsets using
`c-set-offset'.

   Note that file style settings (i.e. `c-file-style') are applied
before file offset settings (i.e. `c-file-offsets').  Also, if either
of these are set in a file's local variable section, all the style
variable values are made local to that buffer.

   ---------- Footnotes ----------

   (1) Note that this feature doesn't work with Emacs versions before
XEmacs 19.12 and Emacs 19.29.  It works via the standard Emacs hook
variable `hack-local-variables-hook'.


File: cc-mode.info,  Node: Advanced Customizations,  Prev: Styles,  Up: Customizing Indentation

Advanced Customizations
=======================

   For most users, CC Mode will support their coding styles with very
little need for more advanced customizations.  Usually, one of the
standard styles defined in `c-style-alist' will do the trick.  At most,
perhaps one of the syntactic symbol offsets will need to be tweaked
slightly, or maybe `c-basic-offset' will need to be changed.  However,
some styles require a more flexible framework for customization, and
one of the real strengths of CC Mode is that the syntactic analysis
model provides just such a framework. This allows you to implement
custom indentation calculations for situations not handled by the mode
directly.

* Menu:

* Custom Indentation Functions::
* Custom Brace and Colon Hanging::
* Customizing Semi-colons and Commas::
* Other Special Indentations::


File: cc-mode.info,  Node: Custom Indentation Functions,  Next: Custom Brace and Colon Hanging,  Up: Advanced Customizations

Custom Indentation Functions
----------------------------

   The most flexible way to customize CC Mode is by writing "custom
indentation functions" and associating them with specific syntactic
symbols (see *Note Syntactic Symbols::).  CC Mode itself uses custom
indentation functions to provide more sophisticated indentation, for
example when lining up C++ stream operator blocks:

     1: void main(int argc, char**)
     2: {
     3:   cout << "There were "
     4:     << argc
     5:     << "arguments passed to the program"
     6:     << endl;
     7: }

   In this example, lines 4 through 6 are assigned the `stream-op'
syntactic symbol.  Here, `stream-op' has an offset of `+', and with a
`c-basic-offset' of 2, you can see that lines 4 through 6 are simply
indented two spaces to the right of line 3.  But perhaps we'd like CC
Mode to be a little more intelligent so that it aligns all the `<<'
symbols in lines 3 through 6.  To do this, we have to write a custom
indentation function which finds the column of first stream operator on
the first line of the statement.  Here is sample lisp code implementing
this:

     (defun c-lineup-streamop (langelem)
       ;; lineup stream operators
       (save-excursion
         (let* ((relpos (cdr langelem))
                (curcol (progn (goto-char relpos)
                               (current-column))))
           (re-search-forward "<<\\|>>" (c-point 'eol) 'move)
           (goto-char (match-beginning 0))
           (- (current-column) curcol))))

Custom indent functions take a single argument, which is a syntactic
component cons cell (see *Note Syntactic Analysis::).  The function
returns an integer offset value that will be added to the running total
indentation for the line.  Note that what actually gets returned is the
difference between the column that the first stream operator is on, and
the column of the buffer relative position passed in the function's
argument.  Remember that CC Mode automatically adds in the column of
the component's relative buffer position and we don't the column offset
added in twice.

   Now, to associate the function `c-lineup-streamop' with the
`stream-op' syntactic symbol, we can add something like the following
to our `c++-mode-hook'(1):

     (c-set-offset 'stream-op 'c-lineup-streamop)

   Now the function looks like this after re-indenting (using `C-c
C-q'):

     1: void main(int argc, char**)
     2: {
     3:   cout << "There were "
     4:        << argc
     5:        << "arguments passed to the program"
     6:        << endl;
     7: }

   Custom indentation functions can be as simple or as complex as you
like, and any syntactic symbol that appears in `c-offsets-alist' can
have a custom indentation function associated with it.  CC Mode comes
with many standard custom indentation functions, not all of which are
used by the default styles.

   * `c-lineup-arglist' -- lines up function argument lines under the
     argument on the previous line.

   * `c-lineup-arglist-intro-after-paren' -- similar to
     `c-lineup-arglist', but works for argument lists that begin with an
     open parenthesis followed by a newline.

   * `c-lineup-arglist-close-under-paren' -- set your `arglist-close'
     syntactic symbol to this line-up function so that parentheses that
     close argument lists will line up under the parenthesis that
     opened the argument list.

   * `c-lineup-close-paren' -- lines up the closing parenthesis under
     its corresponding open parenthesis if that one is followed by code.
     Otherwise, if the open parenthesis ends its line, no indentation is
     added.  Works with any `...-close' symbol.

   * `c-lineup-streamop' -- lines up C++ stream operators (e.g. `<<'
     and `>>').

   * `c-lineup-multi-inher' -- lines up multiple inheritance lines.

   * `c-indent-one-line-block' -- adds `c-basic-offset' to the
     indentation if the line is a one line block, otherwise 0.
     Intended to be used with any opening brace symbol, e.g.
     `substatement-open'.

   * `c-lineup-C-comments' -- lines up C block comment continuation
     lines.

   * `c-lineup-comment' -- lines up comment only lines according to the
     variable `c-comment-only-line-offset'.

   * `c-lineup-runin-statements' -- lines up `statement's for coding
     standards which place the first statement in a block on the same
     line as the block opening brace(2).

   * `c-lineup-math' -- lines up math `statement-cont' lines under the
     previous line after the equals sign.

   * `c-lineup-ObjC-method-call' -- for Objective-C code, lines up
     selector arguments just after the message receiver.

   * `c-lineup-ObjC-method-args' -- for Objective-C code, aligns
     argument separating colons vertically.

   * `c-lineup-ObjC-method-args-2' -- similar to
     `c-lineup-ObjC-method-args' but lines up the colon on the current
     line with the colon on the previous line.

   * `c-lineup-inexpr-block' -- this can be used with the in-expression
     block symbols, i.e. `inexpr-statement', `inexpr-class' and
     `inlambda', to indent the whole block to the column where the
     construct is started.  E.g. for Java anonymous classes, this lines
     up the class under the `new' keyword.

   * `c-lineup-dont-change' -- this lineup function returns the
     indentation of the current line.  Think of it as an identity
     function for lineups; it is used for `cpp-macro-cont' lines.

   ---------- Footnotes ----------

   (1) It probably makes more sense to add this to `c++-mode-hook' than
`c-mode-common-hook' since stream operators are only relevant for C++.

   (2) Run-in style doesn't really work too well.  You might need to
write your own custom indentation functions to better support this
style.


File: cc-mode.info,  Node: Custom Brace and Colon Hanging,  Next: Customizing Semi-colons and Commas,  Prev: Custom Indentation Functions,  Up: Advanced Customizations

Custom Brace and Colon Hanging
------------------------------

   Syntactic symbols aren't the only place where you can customize CC
Mode with the lisp equivalent of callback functions.  Brace "hanginess"
can also be determined by custom functions associated with syntactic
symbols on the `c-hanging-braces-alist' variable.  Remember that
ACTION's are typically a list containing some combination of the
symbols `before' and `after' (see *Note Hanging Braces::).  However, an
ACTION can also be a function which gets called when a brace matching
that syntactic symbol is entered.

   These ACTION functions are called with two arguments: the syntactic
symbol for the brace, and the buffer position at which the brace was
inserted.  The ACTION function is expected to return a list containing
some combination of `before' and `after', including neither of them
(i.e. `nil').  This return value has the normal brace hanging semantics.

   As an example, CC Mode itself uses this feature to dynamically
determine the hanginess of braces which close "do-while" constructs:

     void do_list( int count, char** atleast_one_string )
     {
         int i=0;
         do {
             handle_string( atleast_one_string[i] );
             i++;
         } while( i < count );
     }

   CC Mode assigns the `block-close' syntactic symbol to the brace that
closes the `do' construct, and normally we'd like the line that follows
a `block-close' brace to begin on a separate line.  However, with
"do-while" constructs, we want the `while' clause to follow the closing
brace.  To do this, we associate the `block-close' symbol with the
ACTION function `c-snug-do-while':

     (defun c-snug-do-while (syntax pos)
       "Dynamically calculate brace hanginess for do-while statements.
     Using this function, `while' clauses that end a `do-while' block will
     remain on the same line as the brace that closes that block.
     
     See `c-hanging-braces-alist' for how to utilize this function as an
     ACTION associated with `block-close' syntax."
       (save-excursion
         (let (langelem)
           (if (and (eq syntax 'block-close)
                    (setq langelem (assq 'block-close c-syntactic-context))
                    (progn (goto-char (cdr langelem))
                           (if (= (following-char) ?{)
                               (forward-sexp -1))
                           (looking-at "\\<do\\>[^_]")))
               '(before)
             '(before after)))))

   This function simply looks to see if the brace closes a "do-while"
clause and if so, returns the list `(before)' indicating that a newline
should be inserted before the brace, but not after it.  In all other
cases, it returns the list `(before after)' so that the brace appears
on a line by itself.

   During the call to the brace hanging ACTION function, the variable
`c-syntactic-context' is bound to the full syntactic analysis list.

   Note that for symmetry, colon hanginess should be customizable by
allowing function symbols as ACTIONs on the `c-hanging-colon-alist'
variable.  Since no use has actually been found for this feature, it
isn't currently implemented!


File: cc-mode.info,  Node: Customizing Semi-colons and Commas,  Next: Other Special Indentations,  Prev: Custom Brace and Colon Hanging,  Up: Advanced Customizations

Customizing Semi-colons and Commas
----------------------------------

   You can also customize the insertion of newlines after semi-colons
and commas, when the auto-newline minor mode is enabled (see *Note
Minor Modes::).  This is controlled by the variable
`c-hanging-semi&comma-criteria', which contains a list of functions
that are called in the order they appear.  Each function is called with
zero arguments, and is expected to return one of the following values:

   * non-`nil' -- A newline is inserted, and no more functions from the
     list are called.

   * `stop' -- No more functions from the list are called, but no
     newline is inserted.

   * `nil' -- No determination is made, and the next function in the
     list is called.

   If every function in the list is called without a determination being
made, then no newline is added. The default value for this variable is a
list containing a single function which inserts newlines only after
semi-colons which do not appear inside parenthesis lists (i.e. those
that separate `for'-clause statements).

   Here's an example of a criteria function, provided by CC Mode, that
will prevent newlines from being inserted after semicolons when there is
a non-blank following line.  Otherwise, it makes no determination.  To
use, add this to the front of the `c-hanging-semi&comma-criteria' list.


     (defun c-semi&comma-no-newlines-before-nonblanks ()
       (save-excursion
         (if (and (eq last-command-char ?\;)
                  (zerop (forward-line 1))
                  (not (looking-at "^[ \t]*$")))
             'stop
           nil)))

   The function `c-semi&comma-inside-parenlist' is what prevents
newlines from being inserted inside the parenthesis list of `for'
statements.  In addition to `c-semi&comma-no-newlines-before-nonblanks'
described above, CC Mode also comes with the criteria function
`c-semi&comma-no-newlines-for-oneline-inliners', which suppresses
newlines after semicolons inside one-line inline method definitions
(i.e. in C++ or Java).


File: cc-mode.info,  Node: Other Special Indentations,  Prev: Customizing Semi-colons and Commas,  Up: Advanced Customizations

Other Special Indentations
--------------------------

   In `gnu' style (see *Note Built-in Styles::), a minimum indentation
is imposed on lines inside top-level constructs.  This minimum
indentation is controlled by the variable
`c-label-minimum-indentation'.  The default value for this variable is
1.

   One other customization variable is available in CC Mode:
`c-special-indent-hook'.  This is a standard hook variable that is
called after every line is indented by CC Mode.  You can use it to do
any special indentation or line adjustments your style dictates, such
as adding extra indentation to constructors or destructor declarations
in a class definition, etc.  Note however, that you should not change
point or mark inside your `c-special-indent-hook' functions (i.e.
you'll probably want to wrap your function in a `save-excursion').

   Setting `c-special-indent-hook' in your style definition is handled
slightly differently than other variables.  In your style definition,
you should set the value for `c-special-indent-hook' to a function or
list of functions, which will be appended to `c-special-indent-hook'
using `add-hook'.  That way, the current setting for the buffer local
value of `c-special-indent-hook' won't be overridden.

   Normally, the standard Emacs command `M-;' (`indent-for-comment')
will indent comment only lines to `comment-column'.  Some users
however, prefer that `M-;' act just like `TAB' for purposes of
indenting comment-only lines; i.e. they want the comments to always
indent as they would for normal code, regardless of whether `TAB' or
`M-;' were used.  This behavior is controlled by the variable
`c-indent-comments-syntactically-p'.  When `nil' (the default), `M-;'
indents comment-only lines to `comment-column', otherwise, they are
indented just as they would be if `TAB' were typed.

   Note that this has no effect for comment lines that are inserted with
`M-;' at the end of regular code lines.  These comments will always
start at `comment-column'.


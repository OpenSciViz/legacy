#!/usr/local/bin/perl
# ************************************************************ #
# NAME: page-configure
# PURPOSE: Routine to determine suitable layout of page for psmulti
# NOTES: 
# SCCS: @(#)page-configure.pl	2.4 5/26/92
# HISTORY:
#       murray - Mar 26, 1992: Created.
# ************************************************************ #

# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #

sub configure
  {
    local($pages, $pbbox, $ibbox, $rotate)=@_;
    local($iheight,$iwidth,$pheight,$pwidth);
    local($scale,$scalex,$scaley);
    local($rows,$cols,$rot);
    local($switch)=0;
    
    if ( $pbbox =~ /^\s*(-?\d+)\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)/ )
      { $pwidth= $3-$1; $pheight= $4-$2; }

    else
      { $StandardBBox{'a4'} =~ /^\s*(-?\d+)\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)/;
        $pwidth= $3-$1; $pheight= $4-$2; 
      }

    if ( $ibbox =~ /^\s*(-?\d+)\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)/ )
      { $iwidth= $3-$1; $iheight= $4-$2; }
    else
      { $StandardBBox{'a4'} =~ /^\s*(-?\d+)\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)/;
        $iwidth= $3-$1; $iheight= $4-$2; 
      }

    # If Grid Geometry is not defined work it out #
    
    if ( defined $Global{Rows} && defined $Global{Columns} )
      { $rows=$Global{Rows}; $cols=$Global{Columns}; }
    else
      { $rows=int( sqrt($pages) );
        $cols=int($pages / $rows); 
        if ( $rows * $cols < $pages ) { $rows++; }
        # Prefer More Rows to Columns by default #
        if ( $rows < $cols )
          { $pages=$rows; $rows=$cols; $cols=$pages; }
        $pages=$rows*$cols;
      }

    # If Rotation defined work out if its closer to landscape orientation #
    if ( defined $rotate )
      { $rot=$rotate; 
        if ( sin($rotate*0.017) > 0.707 || sin($rotate*0.017) < -0.707 )
          { $scale=$iwidth;
            $iwidth=$iheight;
            $iheight=$scale;
          }
      }
    else
      { $rot=0; }
    
    # Calculate the Scaling required in default orientation #
    $scalex=($pwidth)/($cols*$iwidth);
    $scaley=($pheight)/($rows*$iheight);

    if ( $scalex < $scaley )
       { $scale = $scalex; }
    else
       { $scale = $scaley; }

    # Compare with Scaling from printing picture in landscape #
    if ( ! defined $rotate )
      {
        $scalex=$pwidth/($cols*$iheight);     
        $scaley=$pheight/($rows*$iwidth);     

        if ( $scalex <= $scaley && $scalex > $scale )
          { $scale=$scalex; $rot=-90; }
        elsif ( $scalex > $scaley && $scaley > $scale ) 
          { $scale=$scaley; $rot=-90; }
      }

    # Compare with Switching round grid, ie nxm -> mxn, in portrait #
    if ( ! defined $Global{Rows} || ! defined $Global{Columns} )
      {
        $scalex=$pwidth/($rows*$iwidth);     
        $scaley=$pheight/($cols*$iheight);     

        if ( $scalex < $scaley && $scalex > $scale )
          { $scale=$scalex; $switch=1;
            if ( ! defined $rotate ) { $rot=0; }
          }

        elsif ( $scalex > $scaley && $scaley > $scale ) 
          { $scale=$scaley;  $switch=1;
            if ( ! defined $rotate ) { $rot=0; }
          }
      }
    
    # Compare with Scaling for switched grid and landscape #
    if ( ! defined $rotate )
      {
        $scalex=$pwidth/($rows*$iheight);     
        $scaley=$pheight/($cols*$iwidth);     

        if ( $scalex <= $scaley && $scalex > $scale )
          { $scale=$scalex; $rot=-90; $switch=1; }
        elsif ( $scalex > $scaley && $scaley > $scale ) 
          { $scale=$scaley; $rot=-90; $switch=1; }
      }

    if ( $switch )
      { $Global{Rows}=$cols; $Global{Columns}=$rows; }
    else
      { $Global{Rows}=$rows; $Global{Columns}=$cols; }
    
    $rot;
  }


# ---------------------------------------- #
# End of Package
# ---------------------------------------- #
1;

#!/usr/local/bin/perl
# ************************************************************ #
# NAME: file-util
# PURPOSE: Utilities for reading from files.
# FUNCTIONS:
#   &stdin_to_tmp: Create a Unique Tmp file for stdin.
#   &cat_region: Copy the specified region of a file from INPUT to
#          OUTPUT, optional flag enables DSC filtering.
#   &find_closing_rexp: Skip forward in file through pairs of rexp's.
#   &next_line: Read next line, plus of current offset.
#   &previous_line: read previous line from a file, plus current offset.
#   &strip_nl: Strip optional newline from end of line.
#         
# NOTES: 
# SCCS: @(#)file-util.pl	2.3 5/26/92
# HISTORY:
#       murray - Mar 25, 1992: Created.
# ************************************************************ #

# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #

# -------------------------------------------------- #
#   Routine to Copy Stdin to a temporary file
# -------------------------------------------------- #

sub main'stdin_to_tmp
  {
    local($prefix)=@_;
    local(*TMP,$num,$tmpstat);

    # Find a Free Filename in /tmp #
    if ( defined  $tmpfile )
      { if ( ! open(TMP,">$tmpfile") )
          { $@="Unable to open temporary file for Stdin";
            return "";
          }
      }
    else
      { for ( $num=0; $num<999; $num++)
          {
            $tmpfile="$prefix.$num";
            if ( ! -f $tmpfile && open(TMP,">$tmpfile") )
              { last; }

            undef $tmpfile;
          }
        if ( ! defined $tmpfile )
          { $@="Unable to temporary open file for stdin";
            return "";
          }
      }

    # Cat the Stdin to the tmpfile #
    while ( <STDIN> ) { print TMP $_; }
    close(TMP);
    
    # Check for Empty Stdin #
    @tmpstat=stat($tmpfile);
    if ( ! $tmpstat[7] > 0 )
      { $@="Standard input empty";
        unlink $tmpfile;
        return "";
      }
    
    # Return the Name of the Temporary File #
    $tmpfile;
  }

# --------------------------------------------------- #
# routine to print a selected area of text
# If flag is true filter lines matching /^%%/ to /^
# --------------------------------------------------- #

sub main'cat_region
  {
    local(*INFILE,*OUTFILE,$start_offset,$end_offset,$flag)=@_;
    local($offset, $line);

    if ( ! defined $start_offset )
      { while ( $line=<INFILE> ) { print OUTFILE $line; }
        return 1; 
      }

    if ( ! defined $flag ) { $flag=0; }
    
    if ( $start_offset < 0 ) { $offset=0; }
    elsif ( seek(INFILE, $start_offset,0) )
        { $offset=$start_offset; }
    else
      { $@="Seek Failed - $@"; return 0; }

    if ( defined $end_offset ) 
      {
        while ( $line=<INFILE> )
          { 
            $offset=$offset+length($line);

            if ( $flag ) { $line =~ s/^%%/%*%%/; }
            
            if ( $offset == $end_offset ) 
              { print  OUTFILE $line;
                return 1;
              }
            elsif ( $end_offset > 0 && $offset > $end_offset )
              { $line=substr($line, 0, length($line)+$end_offset-$offset);
                print OUTFILE $line;
                return 1;;
              }
            else
              { print OUTFILE $line; }
          }
      }
    else
      { while ( $line=<INFILE> )
          { if ( $flag ) { $line =~ s/^%%/%*%%/; }
            print OUTFILE $line;
          }
        return 1; 
      }

    $@="Unable to reach offset $end_offset";
    0;
  }

# ------------------------------------------------------- #
# Routine to skip round blocks such as Begin/EndDocument
# ------------------------------------------------------- #
sub main'find_closing_rexp
  {
    local(*FILE, *offset, $open_rexp, $close_rexp, $depth)=@_;
    local($line);

    if ( ! defined $depth )
      { 
        # Assume search starts after first match of open_rexp #
        $depth=1;
      }

    while( $depth > 0 && ($line=<FILE>) )
      {
        if ( $line =~ /$open_rexp/ ) { $depth++; }
        elsif ( $line =~ /$close_rexp/ ) { $depth--; }
        $offset=$offset+length($line);
      }

    if ( $depth > 0 ) { return 0; }
    else { return 1; }

  }

# -------------------------------------------------- #
# Routines to keep track of position while reading
# line from a file.
# -------------------------------------------------- #
sub main'next_line
  {
    local(*FHANDLE,*offset,*flag)= @_;
    local($line);

    $line="";

    if ( $offset < 0 ) 
      { $offset = 0; $flag=1; }

    if ( defined $flag && $flag )
      { if ( ! seek(FHANDLE,$offset,0) )
          { $@="Seek Failed - $@";
            $line="";
            return $line;
          }
        $flag=0;
      }

    if ( $line = <FHANDLE> )
      {
         $offset=$offset+length($line);
      }
    else
      { $@="Read Failed - $@";
        $line="";
      }

    $line;
  }

sub main'previous_line
  {
    local(*FHANDLE,*offset,*flag) = @_;
    local(@FSTAT, $char, $line, $newoffset);
    $line="";

    # Return null string when zero ofset reached #
    if ( $offset == 0 )
      { 
         $line=""; 
         return $line;
      }

    # Negative Offset is flag to reset to end of file #
    if ( $offset < 0 )
      { 
        @FSTAT=stat(FHANDLE);
        $offset=$FSTAT[7]; 
      }

    $flag=0;
    $newoffset=$offset;
    do
      {
        $newoffset=$newoffset-1;

        if ( ! seek(FHANDLE,$newoffset,0) )
          { $@="Seek Failed - $@";
            $line="";
            return $line;
          }

        if ( read(FHANDLE,$char,1,0) != 1 )
          { $@="Read Failed - $@";
            $line="";
            return $line;
          }

        $line="$char$line";
      }
    until ( ($char eq "\n" && $newoffset+1 < $offset ) || $newoffset == 0 );

    # If there is a leading newlines remove it and set offset #
    if( $newoffset > 0 )
      { $line=substr($line,1); $offset=$newoffset+1; }
    else
      { $offset=$newoffset; }

    $line;
  }

# ------------------------------------------------------------ #
# Quick routine that will remove a trailing newline if present
# ------------------------------------------------------------ #

sub main'strip_nl
  {
    local($line) = @_;
    if ( defined $line ) { $_=$line; }
    if ( /(.*)\n$/ ) { $_=$1; }
    $_;
  }


# ============================================================ #
# END of Package file-util.pl
# ============================================================ #
1;

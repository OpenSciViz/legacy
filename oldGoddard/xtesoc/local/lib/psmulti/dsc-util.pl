#!/usr/local/bin/perl
# ************************************************************ #
# NAME: dsc-util
# PURPOSE: Utilities for processing the document structuring
#    comments in postscript documents.
# NOTES: 
# SCCS: @(#)dsc-util.pl	2.4 5/26/92
# HISTORY:
#       murray - Mar 25, 1992: Created.
# ************************************************************ #


# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #
require "file-util.pl";

# ============================================================ #
# Routine to read DSC General Parameters
# ============================================================ #

sub read_dsc_param
  {
    local(*FILE,*DSC)=@_;
    local($flag, $lastdsc,$carry_back);
    local($offset) = -1;

    # Get The  (First) Line #

    $_ = &next_line(*FILE,*offset,*flag);
    if ( /^%\!(PS-Adobe.*)$/ ) { $DSC{Magic}= &strip_nl($1); }
    else { $@="Invalid Magic Code"; return 0; }

    # ---------------------------------------- #
    # Collect General Document Specifications
    # ---------------------------------------- #

    # First Collect from top of file #
    while( $_ = &next_line(*FILE,*offset,*flag) )
      {
        if ( /^%%EndComments/ )
            { $DSC{EndComments}=$offset-length($_);
              $DSC{BeginProlog}=$offset;
              last;
             }

        elsif ( /^%%Begin/ )
            { $DSC{BeginProlog}=$offset;
              $offset=$offset-length($_); $flag=1;
              $DSC{EndComments}=$offset; 
              last;
            }

        elsif ( /^%%([^:]+):(.*)$/ )
            { $DSC{$1}=&strip_nl($2); $lastdsc=$1; }
        elsif ( /^%%\+\s+(.*)$/ )
            { $DSC{$lastdsc}=$DSC{$lastdsc} . " " . &strip_nl($1); }

        else
            { $DSC{EndComments}=$offset-length($_);
              $DSC{BeginProlog}=$DSC{EndComments};
               last; 
            }
      }
    
    if ( ! defined $DSC{EndComments} ) 
      { $@="Could not find end of comments";  return 0; }     

    # The from end of file #
    $offset= -1;

    # Skip Trailing White Space #
    while( $_ = &previous_line(*FILE,*offset,*flag) )
      { 
        if (/^\s*$/ ) { next; }
        elsif ( /^%%EOF/ )
          { $DSC{EOF}=$offset; last; }
        else 
          { $offset=$offset+length($_); $flag=1;
            $DSC{EOF}=$offset; last; 
          }
      }

    while( $_ = &previous_line(*FILE,*offset,*flag) )
      {
        if    ( /^%%EOF/ )
            { $@="Encountered Second %%EOF marker."; return 0; } 

        elsif ( /^%%Trailer/ )
            { $DSC{Trailer} = $offset+length($_);
              $DSC{EndTrailer} = $DSC{Trailer};
              last;
            }

        elsif ( /^%%([^:]+):(.*)$/ )
            { $DSC{$1}=&strip_nl($2) . " " . $carry_back;
              $carry_back="";
            }
        elsif ( /^%%\+\s+(.*)$/ )
            { $carry_back=&strip_nl($1) . " " . $carry_back; }

        else
            { $DSC{EndTrailer}=$offset+length($_);
              last;
            }

      }

    # -------------------------------------------------- #
    # Now Check That Some of the entries are formatted ok
    # -------------------------------------------------- #

    if ( defined $DSC{Pages} && $DSC{Pages} =~ /(\d+)\s*(\d+)/ )
      { $DSC{Pages}=$1; $DSC{PageOrder}=$2;
        if ( $DSC{PageOrder} < 0 ) { $DSC{PageOrder}="Descend"; }
        elsif ( $DSC{PageOrder} > 0 ) { $DSC{PageOrder} = "Ascend"; }
        else {$DSC{PageOrder} = "Special"; }
      }

    if ( defined $DSC{BoundingBox} &&
         $DSC{BoundingBox} =~
            /(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)\s*/
       )
      { $DSC{BoundingBox}="$1 $2 $3 $4"; }

    1;
  }

# ============================================================ #
# Rotine for Parsing Complete DSC Structure
# ============================================================ #

sub main'read_dsc
  {
    local(*FILE,*DSC)=@_;
    local($flag, $lastdsc,$block,$carry_back);
    local($offset) = -1;
    local($pagecount)=0;

    # -------------------------------------------------- #
    # First parse in the general DSC parameters
    # -------------------------------------------------- #

    # If Magic is not defined than no params have not been read yet #
    if ( ! defined $DSC{Magic} )
      { &read_dsc_param(*FILE,*DSC) || return 0; }
    
    # ---------------------------------------- #
    # Optional Preview Section 
    # ---------------------------------------- #
    $offset=$DSC{BeginProlog}; $flag=1;

    $_ = &next_line(*FILE,*offset,*flag);
    if ( /^%%BeginPreview/ )
      { 
        $DSC{BeginPreview}=$offset;
        while( $_ = &next_line(*FILE,*offset,*flag) )
          {
            if ( /^%%EndPreview/ )
              { $DSC{EndPreview}=$offset-length($_); last; }
          }

        if ( ! defined $DSC{EndPreview} ) 
          { $@="Incomplete Begin/EndPreview Pair"; return 0;}
      }
    else
      { $offset=$offset-length($_); $flag=1; }
    
    # ---------------------------------------- #
    # Optional Defaults Section 
    # ---------------------------------------- #

    $_=&next_line(*FILE,*offset,*flag);
    if ( /^%%BeginDefaults/ )   
      {
        $DSC{BeginDefaults}=$offset;
        while( $_ = &next_line(*FILE,*offset,*flag) )
          {
            if ( /^%%EndDefaults/ )
              { $DSC{EndDefaults}=$offset; last; }
          }

        if ( ! defined $DSC{EndDefaults} ) 
          { $@="Incomplete Begin/EndDefaults pair"; return 0;}
      }
    else
      { $offset=$offset-length($_); $flag=1; }

    # ---------------------------------------- #
    # Start of Prolog 
    # ---------------------------------------- #

    $_=&next_line(*FILE,*offset,*flag);
    if ( /^%%BeginProlog/ ) 
      { $DSC{BeginProlog}=$offset; }
    else
      { $offset=$offset-length($_); $flag=1;
        $DSC{BeginProlog}=$offset;
      }
       
    # Scan Forwards to %%EndProlog #

    while ( $_ = &next_line(*FILE,*offset,*flag) )
      {
        if ( /^%%EndProlog/ )
          { $DSC{EndProlog}=$offset-length($_); last; }

        elsif ( /^%%Begin(Data|Binary|Document|Resource)/ )
          { $block=$1;
            if( ! &find_closing_rexp(*FILE, *offset,
                                  "^%%Begin$block", "^%%End$block") 
              )
              { $@="Incomplete Begin/End$block"; return 0; }
          }
      }

    if ( ! defined $DSC{EndProlog} ) 
      { $@="Could not find %%EndProlog"; return 0; }

    # Optional BeginSetUp #

    $_=&next_line(*FILE,*offset,*flag);
    if ( /^%%BeginSetup/ )
      { 
        $DSC{BeginSetup}=$offset;
        while ( $_ = &next_line(*FILE,*offset,*flag) )
          {
            if ( /^%%EndSetup/ )
              { $DSC{EndSetup}=$offset-length($_); last; }

            elsif ( /^%%Begin(Data|Binary|Document|Resource)/ )
              { $block=$1;
                if( ! &find_closing_rexp(*FILE, *offset,
                                      "^%%Begin$block", "^%%End$block") 
                  )
                  { $@="Incomplete Begin/End$block"; return 0; }
              }
          }

        if ( ! defined $DSC{EndSetup} )
          { $@="Incomplete Begin/EndSetup pair"; return 0; }
      }
    else
      { $offset=$offset-length($_); $flag=1; }

    # ------------------------------------------------------ #
    # Loose code before first %%Page: treated as additional
    # setup code.
    # ------------------------------------------------------ #

    if ( ! defined $DSC{BeginSetup} )
      { $DSC{TmpBeginSetup}=$offset; }
    
    while ( $_ = &next_line(*FILE,*offset,*flag) )
      {
        if    ( /^%%Page:/ )
          { $offset=$offset-length($_); $flag=1; last; }
        elsif ( /^\s*$/ || /^\s*%/ )
          { next; }
        else
          { if ( ! defined $DSC{BeginSetup} )
              { $DSC{BeginSetup} = $DSC{TmpBeginSetup}; }
            $DSC{EndSetup}=$offset;
          }
      }

    if ( defined $DSC{TmpBeginSetup} ) { delete $DSC{TmpBeginSetup}; }
    
    # ---------------------------------------- #
    # Now Search Through Marking Off Pages 
    # until %%Trailer
    # ---------------------------------------- #

    while ( $_ = &next_line(*FILE,*offset,*flag) )
      { 
        if   ( /^%%Trailer/ )
          { 
            $DSC{"EndPage:$pagecount"}=$offset-length($_);

            if ( defined $DSC{Trailer} && $DSC{Trailer} != $offset )
              { $@="Second %%Trailer ecountered."; return 0; }
            else
              { $DSC{Trailer}=$offset; }

            last;
          }

        elsif ( /^%%Page:\s*(\S+)\s+(\d+)/ )
          {
            $DSC{"EndPage:$pagecount"}=$offset-length($_);
            $pagecount++;
            $DSC{"StartPage:$pagecount"}=$offset;
            $DSC{"PageLabel:$1"}=$pagecount;
            $DSC{"PageOrdinal:$2"}=$pagecount;
          }

        
        elsif ( /^%%Begin(Data|Binary|Document|Resource)/ )
          { $block=$1;
            if( ! &find_closing_rexp(*FILE, *offset,
                                  "^%%Begin$block", "^%%End$block") 
              )
              { $@="Incomplete Begin/End$block"; return 0; }
          }

      }

    if ( ! defined $DSC{"StartPage:1"} )
      { $@="No %%Page Markers Found"; return 0; }

    if ( ! defined $DSC{Trailer} ) 
      { $@="No Document Trailer Found"; return 0; }

    if ( ! defined $DSC{Pages} )
      { $DSC{Pages}=$pagecount; }
    elsif ( $DSC{Pages} != $pagecount )
      { $@="Mismatch between pages counted and number declared in %%Pages.";
        return 0;
      }

    # ---------------------------------------- #
    # If we get to here the document is fully
    # conformant.
    # ---------------------------------------- #

    1;
  }


# ============================================================ #
# Routine that Generates a page order for use with
# DSC info to slect pages
# ============================================================ #

sub main'page_order
  {
    local(*DSC,$SelectPages,$SelectBy)=@_;
    local(@order,$start,$finish);

    if ( ! defined $SelectPages )
      { @order= 1 .. $DSC{Pages}; }
    else
      { foreach(split(/,/,$SelectPages))
          {
            if ( /^([^:]*):([^:]*)$/ )
              { $start=$1; $finish=$2;
                
                if ( $start ne "" )
                  { if ( ! defined ($start=$DSC{"$SelectBy:$start"}) )
                      { &warning("Invalid Page Reference: $start"); next; }
                  }
                else
                  { $start=1; }

                if ( $finish ne "" )
                  { if ( ! defined ($finish=$DSC{"$SelectBy:$finish"}) )
                      { &warning("Invalid Page Reference: $finish"); next; }
                  }
                else
                  { $finish=$DSC{Pages}; }

                if ( $start > $finish )
                  { push(@order, reverse( $finish .. $start) ); }
                else
                  { push(@order, $start .. $finish); }
              }
            else
              { if ( defined ($start=$DSC{"$SelectBy:$_"}) )
                  { push(@order, $start); }
                else
                  { &warning("Invalid page reference: $_."); }
              }
          }
      }

    @order;
  }

# ================================================== #
# END of package dsc-util.pl
# ================================================== #
1;


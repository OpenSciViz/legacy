#!/usr/local/bin/perl
# ************************************************************ #
# NAME: pspp-util.pl
# PURPOSE: Utilities to Process PS Documents, used by psmulti.
# NOTES: 
# SCCS: @(#)pspp-util.pl	2.6 5/26/92
# HISTORY:
#       murray - Mar 24, 1992: Created.
# ************************************************************ #

# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #

# ************************************************************ #
# PSPP Syntax:
#
#   /^%%IncludeDocument: <DocName>$/
#       Include document <DocName> at current point in text
#    surrounded by %%Begin/EndDocument comments.
#
#   /^%%RequireDocument: <DocName>$/
#       If <DocName> has not already been included then
#    include as per %%IncludeDocument.
#
#   /^%%PSMacro(Macro): <MacroExpansion>$/
#       Define a macro <Macro> having expansion <MacroExpansion>
#
#   /^%%+ <MacroContinuation>$/
#       If found directly following a psmacro declaration
#       taken as a continuation of that macro.
#
#   /@Macro\s+/ || /@{Macro}/
#       If macro <Macro> is know substitute its expansion
#     otherwise leave text as is.
# ************************************************************ #

package PSPP_Util;

if ( ! defined @main'PSPPIncludePath )
  { @main'PSPPIncludePath = ( "." ); }


# ============================================================ #
#        Routines Local to Package
# ============================================================ #

sub find_document
  {
     local($docname) = @_;
     local($doc);

     if ( -f $docname ) { return $docname; }
     
     foreach(@main'PSPPIncludePath)
       {  
         if ( -f "$_/$docname" ) 
           { $doc="$_/$docname"; last; }
       }

     if ( ! defined $doc )
       { $@="Can't locate document $docname on include path"; }
     
    $doc;
  }



sub expand_line
  {
    local($line)=@_;
    local($leading,$trailing,$macro);

    if ( $line =~ /@{([^\s}]+)}/ )
      { $leading=$`; $macro=$1; $trailing=$';
        $leading=&expand_line($leading);
        $trailing=&expand_line($trailing);
        
        if ( defined $PSMacro{"$macro"} )
          { $macro=&expand_line($PSMacro{"$macro"}); }
        else
          {$macro="@{$macro}"; }

        $line="$leading$macro$trailing";
      }

    elsif ( $line =~ /@([^\s]+)(\s+|$)/ )
      {
        $leading="$`"; $macro=$1; $trailing="$2$'";
        $leading=&expand_line($leading);
        $trailing=&expand_line($trailing);
        
        if ( defined $PSMacro{"$macro"} )
          { $macro=&expand_line($PSMacro{"$macro"}); }
        else
          {$macro="@$macro"; }

        $line="$leading$macro$trailing";
      }

    $line;
  }

# ============================================================ #
#         Routines Exported to Main
# ============================================================ #

sub main'reset_pspp
  { package PSPP_Util;
    @PSPPIncluded=();
    %PSMacros=();
  }

sub main'require_document
  { package PSPP_Util;

    local($docname,$input)=@_;
    
    if ( ! defined $input )
      { $input="du00"; }

    foreach( @PSPPIncluded )
      { if ( /$docname$/ ) { return 1; } }
        
    &main'include_document($docname, $input) || return 0;

    1;
  }


sub main'include_document
  { package PSPP_Util;
    
    local($docname,$input)=@_;
    local(@DocStack,%NextChar);
    local($cmacro);
    local($cmacro_count,$count)=( -1, 0);
    
    if ( ! defined $input )
      { $input="du00"; }
    else
      { $input++; }
    
    ($doc = &find_document($docname)) || return 0;

    unless ( open($input, $doc) )
      { $@="Can't open $doc: $!\n"; return 0; }
    
    push(@PSPPIncluded, $doc);

    print "%%BeginDocument: $doc\n";

    while( <$input> )
      {
        $_=&expand_line($_);
        $count++;
        
        if  ( /^%%IncludeDocument:\s*([^\s]+)\s*$/ )
          { &main'include_document($1,$input) || return 0; }

        elsif ( /^%%RequireDocument:\s*([^\s]+)\s*$/ )
          { &main'require_document($1,$input) || return 0; }

        elsif ( /^%%PSMacro\(\s*([^\s]+)\s*\):(.+)$/ )
          { $PSMacro{"$1"}="$2";
            $cmacro=$1; $cmacro_count=$count+1;
          }

        elsif ( /^%%\+(.*)$/ && $cmacro_count==$count )
          { $PSMacro{"$cmacro"}="$PSMacro{$cmacro}\n$1";
            $cmacro_count=$count+1;
          }
        else
          { print  $_; }
      }
    
    print "%%EndDocument\n";
    close($input);
    $input--;
    1;
  }

sub main'pspp_cat
  {
    package PSPP_Util;
    local($file,%InitMacros)=@_;

    if ( ! $InitMacros{'NoReset'} eq 'true' )
      { &main'reset_pspp; }

    foreach(keys(%InitMacros))
      { $PSMacro{"$_"}=$InitMacros{"$_"}; }

    &main'include_document($file) || return 0;
  }

# -------------------------------------------------- #
# End of Package PSMacro[216z_Util
# -------------------------------------------------- #
1;

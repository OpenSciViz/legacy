#!/usr/local/bin/perl
# ************************************************************ #
# NAME: bbox-util
# PURPOSE: Routines to perform transformation calculations
#      for Bounding Boxes.
# NOTES: 
# SCCS: @(#)bbox-util.pl	1.3 5/26/92
# HISTORY:
#       murray - Apr 21, 1992: Created.
# ************************************************************ #

# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #

@A4BBox=(20,20,570,820);

sub ConcatTM
  {
    local($a1,$b1,$c1,$d1,$tx1,$ty1,$a2,$b2,$c2,$d2,$tx2,$ty2)=@_;
    local($a,$b,$c,$d,$tx,$ty);
    
    $a  =$a1*$a2+$b1*$c2;            $b  =$a1*$b2+$b1*$d2;
    $c  =$c1*$a2+$d1*$c2;            $d  =$c1*$b2+$d1*$d2;
    $tx =($tx1*$a2+$ty1*$c2+$tx2);   $ty =(tx1*$b2+$ty1*$d2+$ty2);
    ($a,$b,$c,$d,$tx,$ty);
  }

sub RotateTM
  {
    local($r,$a,$b,$c,$d,$tx,$ty)=@_;
    local($a1,$b1,$c1,$d1,$tx1,$ty1);

    if( ! defined $ty2 )
      { ($a,$b,$c,$d,$tx,$ty) = (1,0,0,1,0,0); }

    $r=$r*3.142/180;
    
    $a1  =cos($r);       $b1  =sin($r);
    $c1  =-1*sin($r);    $d1  =cos($r); 
    $tx1 =0;             $ty1 =0;
    
    &ConcatTM($a1,$b1,$c1,$d1,$tx1,$ty1,$a,$b,$c,$d,$tx,$ty);
  }


sub ScaleTM
  {
    local($sx,$sy,$a,$b,$c,$d,$tx,$ty)=@_;
    local($a1,$b1,$c1,$d1,$tx1,$tx2);
    
    if( ! defined $ty )
      { ($a,$b,$c,$d,$tx,$ty) = (1,0,0,1,0,0); }

    $a1  =$sx;  $b1  =0;
    $c1  =0;    $d1  =$sy;
    $tx1 =0;    $tx2 =0;
    
    &ConcatTM($a1,$b1,$c1,$d1,$tx1,$ty1,$a,$b,$c,$d,$tx,$ty);
  }

sub TranslateTM
  {
    local($dx,$dy,$a,$b,$c,$d,$tx,$ty)=@_;
    local($a1,$b1,$c1,$d1,$tx1,$ty1);

    if( ! defined $ty )
      { ($a,$b,$c,$d,$tx,$ty) = (1,0,0,1,0,0); }
    
    $a1  =1;   $b1  =0;
    $c1  =0;   $d1  =1; 
    $tx1 =$dx; $ty1=$dy;
    
    &ConcatTM($a1,$b1,$c1,$d1,$tx1,$ty1,$a,$b,$c,$d,$tx,$ty);
  }


sub InvertTM
  { local($a,$b,$c,$d,$tx,$ty)=@_;
    local($det)=($a*$b-$c*$d);
    
    ( ($d/$det) ,                 (-$b/$det),
      (-$c/$det),               ($a/$det),
      (($d*$tx-$c&$ty)/$det),     ((-$b*$tx-$a*$ty)/$det)
    );

  }

sub CoorTransform
  {
    local($a,$b,$c,$d,$tx,$ty,@Coors)=@_;
    local(@NewCoors,$x,$y);
    
    while ($#Coors >= 1)
      { ($x,$y)=splice(@Coors,0,2);
        push(@NewCoors, (($a*$x+$c*$y+$tx), ($b*$x+$d*$y+$ty)) );
      }

    @NewCoors;
  }

sub BBoxTransform
  {
    local($a,$b,$c,$d,$tx,$ty,@BBox)=@_;
    local($llx,$lly,$urx,$ury)=(0,0,0,0);
    local(@Coors,$x,$y);

    @TM=($a,$b,$c,$d,$tx,$ty);
    
    while($#BBox >= 3)
      {
        ($llx,$lly,$urx,$ury)=splice(@BBox,0,4);

        @Coors=&CoorTransform(@TM, ($llx, $lly, $llx, $ury,
                                    $urx, $ury, $urx, $lly,
                                    )
                              );

        ($llx, $lly)=splice(@Coors,0,2);
        ($urx, $ury)=($llx, $lly);
        
        while($#Coors >=1)
          { ($x,$y)=splice(@Coors,0,2);
            if ( $x > $urx) { $urx = $x; }
            if ( $x < $llx) { $llx = $x; }
            if ( $y > $ury) { $ury = $y; }
            if ( $y < $lly) { $lly = $y; }
          }
        
        push(@NewBBox, ($llx,$lly,$urx,$ury));
      }

    @NewBBox;
  }

sub BBoxRotate
  {
    local($angle,@BBox)=@_;
    local(@TM);

    @TM=&RotateTM($angle);

    &BBoxTransform(@TM,@BBox);
  }

sub BBoxScale
  {
    local($sx,$sy,@BBox)=@_;
    local($x,$y,@NBBox);
    
    while($#BBox>=1)
      { ($x,$y)=splice(@BBox,0,2);
        $x=$x*$sx; $y=$y*$sy;
        push( @NBBox, ($x,$y) );
      }
    
    @NBBox;
  }

sub BBoxTranslate
  {
    local($tx,$ty,@BBox)=@_;
    local($x,$y,@NBBox);
    
    while($#BBox>=1)
      { ($x,$y)=splice(@BBox,0,2);
        $x=$x+$tx; $y=$y+$ty;
        push( @NBBox, ($x,$y) );
      }
    
    @NBBox;
  }



sub BBoxMargin
  { local($mx,$my,@BBox)=@_;
    local($llx,$lly,$urx,$ury,@NBBox);
    
    while( $#BBox >= 3 )
      { ($llx,$lly,$urx,$ury)=splice(@BBox,0,4);
        $llx=$llx-$mx; $lly=$lly-$my;
        $urx=$urx+$mx; $ury=$ury+$my;
        push(@NBBox,($llx,$lly,$urx,$ury));
      }
    @NBBox;
  }

# This routine is equivalent to the BBoxFit procedure in bbox-util.ps #
sub BBoxFit
  { local(*IBBox,*FBBox,*TM,
          $Rotate,$ScaleX,$ScaleY,$JustX,$JustY)=@_;
    local(@NBBox)=@IBBox;

    if ( ! defined $rotate) { $rotate=0; }
    if ( ! defined $ScaleX) { $ScaleX='/Max'; }
    if ( ! defined $ScaleY) { $ScaleY='/Max'; }
    if ( ! defined $JustX)  { $JustX='/Center'; }
    if ( ! defined $JustY)  { $JustY='/Center'; }
    
    @TM=(1,0,0,1,0,0);

    # Apply Rotation if Any #
    
    if ( $Rotate != 0 )
      { @TM=&RotateTM($Rotate);
        @NBBox=&BBoxTransform(@TM,@NBBox);
      }

    # Now The Scaling #

    if ( $ScaleX =~ /Max/ )
      { $ScaleX=($FBBox[2] - $FBBox[0]) / ($NBBox[2] - $NBBox[0]); }
    elsif ( $ScaleX =~ /Ratio/ )
      { $ScaleX=0; }
    
    if ( $ScaleY =~ /Max/ )
      { $ScaleY=($FBBox[3] - $FBBox[1]) / ($NBBox[3] - $NBBox[1]); }
    elsif ( $ScaleY =~ /Ratio/ )
      { $ScaleY=0; }

    if ( $ScaleX == 0 && $ScaleY == 0 )
      { $ScaleX=($FBBox[2] - $FBBox[0]) / ($NBBox[2] - $NBBox[0]);
        $ScaleY=($FBBox[3] - $FBBox[1]) / ($NBBox[3] - $NBBox[1]);

        if ( $ScaleX < $ScaleY )
          { $ScaleY=$ScaleX; }
        else
          { $ScaleX=$ScaleY; }
      }

    if ( $ScaleX == 0 )
      { $ScaleX=$ScaleY; }
    if ( $ScaleY == 0 )
      { $ScaleY=$ScaleX; }

    @TM=&ConcatTM( @TM, &ScaleTM($ScaleX,$ScaleY) );
    @NBBox=&BBoxScale($ScaleX,$ScaleY, @NBBox);

    # Now The Justification #
    if ( $JustX =~ /Center/ )
      { $JustX=(($FBBox[0] + $FBBox[2]) -
                ($NBBox[0] + $NBBox[2]))/2;
      }

    elsif ( $JustX =~ /Left/ )
      { $JustX=$FBBox[0]-$NBBox[0]; }
    
    elsif ( $JustX =~ /Bottom/ )
      { $JustX=$FBBox[2]-$NBBox[2]; }

    else
      { $JustX=( ($FBBox[2]-$NBBox[2])*( 1 + $JustX) +
                 ($FBBox[0]-$NBBox[0])*( 1 - $JustX)
                )/2;
      }

    if ( $JustY =~ /Center/ )
      { $JustY=(($FBBox[1] + $FBBox[3]) -
                ($NBBox[1] + $NBBox[3]))/2;
      }

    elsif ( $JustY =~ /Bottom/ )
      { $JustY=$FBBox[1]-$NBBox[1]; }
    
    elsif ( $JustY =~ /Bottom/ )
      { $JustY=$FBBox[3]-$NBBox[3]; }

    else
      { $JustY=( ($FBBox[3]-$NBBox[3])*( 1 + $JustY) +
                 ($FBBox[1]-$NBBox[1])*( 1 - $JustY)
                )/2;
      }

    @TM1= &TranslateTM($JustX,$JustY);
    @TM=&ConcatTM( @TM, @TM1 );
    @NBBox=&BBoxTranslate($JustX,$JustY, @NBBox);

    &info(sprintf("Using: %5.3f %5.3f translate %5.3f %5.3f scale %5.3f rotate",
                   $JustX,$JustY,$ScaleX,$ScaleY,$Rotate)
          );
    @NBBox;
  }


# ============================================================ #
# End of Package
# ============================================================ #
1;

#!/usr/local/bin/perl
# ************************************************************ #
# NAME: psmulti-standard
# PURPOSE: Basic perl routine for generating psmulti documents.
# NOTES: 
# SCCS: @(#)format-standard.pl	2.6 5/26/92
# HISTORY:
#       murray - Mar 26, 1992: Created.
# ************************************************************ #

# ##################################################################### #
# (C) 1992 D Murray Laing, D.M.Laing@uk.ac.edinburgh
#          c/o Department of Chemical Engineering,
#              University of Edinburgh,
#              Edinburgh,
#              Scotland
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2 as
#    published by the Free Software Foundation.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ###################################################################### #

# ============================================================ #
#             Load in Required Utilities
# ============================================================ #

require "pspp-util.pl";
require "file-util.pl";
require "dsc-util.pl";

# ============================================================ #
#              Set Up Undefined Defaults
# ============================================================ #

@GlobalExport = ( "Rows",  "Columns", "RowMajor", "FPageBBox",
                  "TtoB",  "LtoR"
                 );

@DocExport = (   "ImageBBox","EPSDoc", "NoDecor",
                 "MarginX", "MarginY",
                 "Rotate", "ScaleX", "ScaleY", "JustX", "JustY"
               );

# Set Up Global Defaults if Not Already Defined #

if ( ! defined $Global{Style} ) { $Global{Style}='shadow'; }

if ( ! defined $Rotate{Default} )
  { if ( $#FileList == 0 && defined $Rotate{$FileList[0]} ) 
      { $Rotate{Default}=$Rotate{$FileList[0]}; }
  }

if ( ! ( defined $Global{Rows} && defined $Global{Columns}
         && defined $Rotate{Default}
       )
   )
  {
    require "page-configure.pl";

    if (! defined $Global{Pages}) { $Global{Pages}=2; }

    # Get Full Page BBox #

    if ( defined $Global{FPageBBox} )
      { $pbbox=$Global{FPageBBox}; }
    else
      { $pbbox="null"; }

    # Get Default Image BBox #

    if ( defined $ImageBBox{Default} )
      { $ibbox=$ImageBBox{Default}; }
    elsif ( defined $ImageBBox{"FileList[0]"} )
      { $ibbox=$ImageBBox{"FileList[0]"}; }
    else
      { $ibbox='dsc'; }
    
    if ( $ibbox eq 'dsc' )
      {
        open(INPUT,$FileList[0]);
        &read_dsc_param(*INPUT,*DSC);
        if ( defined $DSC{BoundingBox} )
          { $ibbox=$DSC{BoundingBox}; }
        else
          { $ibbox='null'; }
        close(INPUT);
        %DSC=();
      }

    # Now use configure to calculate default rotation #
    $Rotate{Default}=
      &configure($Global{Pages},$pbbox,$ibbox,$Rotate{Default});

    &info("Using $Global{Columns}x$Global{Rows} geometry and $Rotate{Default} degrees rotation");
  }

if ( defined $OutFile ) 
  { open(OUTPUT,">$OutFile") ||
      &error("Could not open $outfile for writing");
    select OUTPUT;
  }
else
  { *OUTPUT=*STDOUT; }

# ============================================================ #
#            Generate the Postscript
# ============================================================ #

# -------------------------------------------------- #
#               Multi Prolog
# -------------------------------------------------- #

print "%!PS-Adobe\n";
print "%%Creator: psmulti\n";
print "%%Pages: (atend)\n";

if ( defined $Global{'FPageBBox'} && $Global{FPageBBox} ne '/Default' )
  { print "%%BoundingBox: " . $Global{FPageBBox}; }

print "%%EndComments\n";
print "%%BeginPrologue\n";

&pspp_cat("format-standard.ps", ("StyleFile", "border-$Global{Style}.ps"))
    || &error($@);

# -------------------------------------------------- #
#                   Multi Setup
# -------------------------------------------------- #

print "%%BeginSetup\n";
print "/StartParam\n";

foreach(@GlobalExport)
  { if ( defined $Global{$_} )
      { print "$Global{$_} /$_\n"; }
    else
      { print "/Default /$_\n"; }
  }

print "/EndParam\nBeginMulti\n";
print "%%EndSetup\n";

# -------------------------------------------------- #
#            Document Processing
# -------------------------------------------------- #

$LperP=$Global{Columns}*$Global{Rows};
$LPages=0;
$PPages=0;

foreach $File (@FileList)
  {

    # -------------------------------------------------- #
    #    Open the File for Random Access
    # -------------------------------------------------- #

    if ( ! open(INPUT,$File) )
      { &error("Unable to open $File"); }

    &info("Processing document: $File");

    # -------------------------------------------------- #
    #  Parse the Document Structure
    # -------------------------------------------------- #

    %DSC=();
    $dsc_ok=&read_dsc_param(*INPUT,*DSC);

    if ( ! $dsc_ok ) { &info("Document Non-conformant");}

    # -------------------------------------------------- #
    # Set Up Necessary Defaults that are still Undefined
    # -------------------------------------------------- #

    if ( ! defined $ImageBBox{"$File"} && defined $ImageBBox{Default} )
      { $ImageBBox{"$File"} = $ImageBBox{Default}; }

    if ( defined $DSC{BoundingBox} &&
         ( (! defined $ImageBBox{"$File"}) ||
           ($ImageBBox{"$File"} eq 'dsc')
         )
       )
      { $ImageBBox{"$File"}=$DSC{BoundingBox}; }

    if ( defined $ImageBBox{$File} )
      { &info("Using Bounding Box: $ImageBBox{$File}"); }
    else
      { &info("No Bounding Box Defined"); }

    if ( ! defined $EPSDoc{"$File"} )
      { if( defined $EPSDoc{Default} )
          { $EPSDoc{"$File"} = $EPSDoc{"$File"}; }
        elsif ( $DSC{Magic} =~ /EPSF/ )
          { $EPSDoc{$File} = "true"; }
      }

    if ( $EPSDoc{$File} eq "true" )
      { &info("Treating Document as Encapsulated PS"); }

    if( defined $ReversePages{"$File"} )
      { $reverse=$ReversePages{"$File"}; }
    elsif( defined $ReversePages{Default} )
      { $reverse=$ReversePages{Default}; }
    else
      { $reverse=0; }

    if( defined $SelectPages{"$File"} )
      { $select=$SelectPages{"$File"}; }
    elsif ( defined $SelectPages{Default} )
      { $select=$SelectPages{Default}; }
    else
      { undef $select; }

    if( defined $SelectBy{"$File"} )
      { $select_by=$SelectBy{"$File"}; }
    elsif( defined $SelectBy{Default} )
      { $select_by=$SelectBy{Default}; }
    else
      { $select_by='ordinal'; }

    # See whether it is necessary and possible to select pages #
    if ( $dsc_ok && ($reverse || defined $select) )
       { if ( &read_dsc(*INPUT,*DSC) )
           { $do_select=1;
             # Make sure we have a valid page selection #
             @order= &page_order(*DSC, $select, $select_by);
             if ( $reverse ) { reverse(@order); }

             if ( $#order < 0 )
              { &warning("Null page selection specified skipping document");
                next;
              }
             else
              { &info("Pages selected: " . join(',' , @order) ); }
              
           }
         else
           { &warning("Document non-conformant, cannot do page selection");
             $do_select=0;
           }
       }
    else
       { $do_select=0; }

    # -------------------------------------------------- #
    #     Setup the Document Specific Parameters
    # -------------------------------------------------- #

    print "/StartParam\n";

    foreach(@DocExport)
      { $value=eval "\$$_{\"$File\"}";
        if ( $value =~ /^\s*$/ )
          { $value=eval "\$$_{Default}";
             if ( $value =~ /^\s*$/ )
               { $value="/Default"; }
           }
        print "$value /$_\n"; 
      }
    print "/EndParam\nBeginDocument\n";


    # ---------------------------------------- #
    #        Document Prolog & Setup
    # ---------------------------------------- #
    print "%*%%BeginDocument: $File\n";

    if ( $do_select)
      {
          
        # Print the Document Prolog and Setup #

        print "%*%%BeginProlog\n";
        &cat_region(*INPUT,*OUTPUT,$DSC{BeginProlog},$DSC{EndProlog});
        print "%*%%EndProlog\n";

        if ( defined $DSC{BeginSetup} )
          { print "%*%%BeginSetup\n";
            &cat_region(*INPUT,*OUTPUT,$DSC{BeginSetup},$DSC{EndSetup});
            print "%*%%EndSetup\n";
          }

        foreach(@order)
         {
           $LPages++;
           if ( $LPages % $LperP == 1 || $LperP == 1 )
             { $PPages++;
               print "%%Page: ? $PPages\n";
             }

           print "%*%%Page: ? $_\n";
           &cat_region(*INPUT,*OUTPUT,
                       $DSC{"StartPage:$_"}, $DSC{"EndPage:$_"}
                      );

         }

        # Finally the Document Trailer

        print "%*%%Trailer\n";
        &cat_region(*INPUT,*OUTPUT,$DSC{Trailer},$DSC{"EndTrailer"});
      }
    else
      {
        $LPages++;
        if ( $LPages % $LperP == 1 || $LperP == 1 )
          { $PPages++; print "%%Page: ? $PPages\n"; }
        undef $FoundFirstPage;
        seek(INPUT,0,0);
        while ( <INPUT> )
          {
            if ( /^%%Page:/ )
              { if ( defined $FoundFirstPage )
                  { 
                    $LPages++;
                    if ( $LPages % $LperP == 1 || $LperP == 1 )
                      { $PPages++; print "%%Page: ? $PPages\n"; }
                  }
                else
                  { $FoundFirstPage=1; }
                print "%*$_";
              }

            elsif ( /^%%/ )
              { print "%*$_"; }
            else
              { print $_; }
          }
      }

    print "%*%%EndDocument\n";
    print "EndDocument\n";
    &info("Completed Document: $File");
    &info("Page Count Now - $LPages Logical, $PPages Physical");

    close(INPUT);
  }

# -------------------------------------------------- #
#                  Multi Trailer
# -------------------------------------------------- #

print "%%Trailer\n";
print "EndMulti\n";
print "%%Pages: $PPages\n";
print "%%EOF\n";

# ============================================================ #
#                  Clean Up
# ============================================================ #

close(OUTPUT);

# ============================================================ #
# END of Package psmulti-default.pl
# ============================================================ #
1;

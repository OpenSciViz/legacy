# NOTE: Derived from ./blib/lib/Tk/TextUndo.pm.  Changes made here will be lost.
package Tk::TextUndo;

sub FileName
{
 my $text = shift;
 if (@_)
  {
   $text->{'FILE'} = shift; 
  }
 return $text->{'FILE'};
}

1;


=head1 NAME

Tk::Animation - Display sequence of Tk::Photo images

=for pm Tk/Animation.pm

=for category Tk Image Classes

=head1 SYNOPSIS

  use Tk::Animation
  my $img = $widget->Animation('-format' => 'gif', -file => 'somefile.gif');

  $img->add_frames(@images);

  $img->start_animation($period);
  $img->stop_animation;


=head1 DESCRIPTION

In the simple case when C<Animation> is passed a GIF89 style GIF with
multiple 'frames', it will build an internal array of C<Photo> images.

The C<add_frames> method adds images to the sequence. It is provided
to allow animations to be constructed from separate images.
All images must be C<Photo>s and should all be the same size.

C<start_animation($period)> then initiates a C<repeat> with specified I<$period>
to sequence through these images. As for raw C<repeat> I<$period> is in milli-seconds,
for a 50Hz monitor it should be at least 20mS.

C<stop_animation> cancels the C<repeat> and resets the image to the first
image in the sequence.

=head1 BUGS

The 'period' should probably be a property of the Animation object
rather than specified at 'start' time. It may even be embedded
in the GIF.

=cut


# tk.tcl --
#
# Initialization script normally executed in the interpreter for each
# Tk-based application.  Arranges class bindings for widgets.
#
# @(#) tk.tcl 1.58 95/01/05 08:51:48
#
# Copyright (c) 1992-1994 The Regents of the University of California.
# Copyright (c) 1994 Sun Microsystems, Inc.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.

# Insist on running with compatible versions of Tcl and Tk.

scan [info tclversion] "%d.%d" a b
if {$a != 7} {
    error "wrong version of Tcl loaded ([info tclversion]): need 7.x"
}
scan $tk_version "%d.%d" a b
if {($a != 4) || ($b < 0)} {
    error "wrong version of Tk loaded ($tk_version): need 4.0 or later"
}
unset a b

# Add Tk's directory to the end of the auto-load search path:

lappend auto_path $tk_library

# Turn off strict Motif look and feel as a default.

set tk_strictMotif 0

# ----------------------------------------------------------------------
# Class bindings for various flavors of button widgets.
# ----------------------------------------------------------------------

bind Label <FocusIn> {
    if {"%d" != "NotifyPointer"} {
	tk_focusContinue %W
    }
}
bind Button <Enter> {tkButtonBind %W Enter}
bind Button <FocusIn> {tkButtonBind %W FocusIn}
bind Checkbutton <Enter> {tkButtonBind %W Enter}
bind Checkbutton <FocusIn> {tkButtonBind %W FocusIn}
bind Radiobutton <Enter> {tkButtonBind %W Enter}
bind Radiobutton <FocusIn> {tkButtonBind %W FocusIn}

# ----------------------------------------------------------------------
# Class bindings for entry widgets.
# ----------------------------------------------------------------------

bind Entry <Enter> {tkEntryBind Enter}
bind Entry <FocusIn> {tkEntryBind FocusIn}

# ----------------------------------------------------------------------
# Class bindings for listbox widgets.
# ----------------------------------------------------------------------

bind Listbox <Enter> {tkListboxBind Enter}
bind Listbox <FocusIn> {tkListboxBind FocusIn}

# ----------------------------------------------------------------------
# Class bindings for menubutton and menu widgets.
# ----------------------------------------------------------------------

bind Menubutton <Enter> {tkMenuBind %W Enter}
bind Menubutton <FocusIn> {tkMenuBind %W FocusIn}
bind Menu <FocusIn> {tkMenuBind %W FocusIn}
bind all <Alt-KeyPress> {
    tkTraverseToMenu %W %A
}
bind all <F10> {
    tkFirstMenu %W
}

# ----------------------------------------------------------------------
# Class bindings for message widgets: just ignore focus events.
# ----------------------------------------------------------------------

bind Message <FocusIn> {
    if {"%d" != "NotifyPointer"} {
	tk_focusContinue %W
    }
}

# ----------------------------------------------------------------------
# Class bindings for scrollbar widgets.
# ----------------------------------------------------------------------

bind Scrollbar <Enter> {tkScrollbarBind %W %x %y Enter}
bind Scrollbar <FocusIn> {tkScrollbarBind %W 0 0 FocusIn}

# ----------------------------------------------------------------------
# Class bindings for scale widgets.
# ----------------------------------------------------------------------

bind Scale <Enter> {tkScaleBind Enter}
bind Scale <FocusIn> {tkScaleBind FocusIn}

# ----------------------------------------------------------------------
# Class bindings for text widgets. $tkPriv(selectMode) holds one of
# "char", "word", or "line" to indicate which selection mode is active.
# ----------------------------------------------------------------------

bind Text <Enter> {tkTextBind Enter}
bind Text <FocusIn> {tkTextBind FocusIn}

# ----------------------------------------------------------------------
# Default bindings for keyboard traversal.
# ----------------------------------------------------------------------

bind all <Tab> {tk_focusNext %W}
bind all <Shift-Tab> {tk_focusPrev %W}

# tkScreenChanged --
# This procedure is invoked by the binding mechanism whenever the
# "current" screen is changing.  The procedure does two things.
# First, it uses "upvar" to make global variable "tkPriv" point at an
# array variable that holds state for the current display.  Second,
# it initializes the array if it didn't already exist.
#
# Arguments:
# screen -		The name of the new screen.

proc tkScreenChanged screen {
    set disp [file rootname $screen]
    uplevel #0 upvar #0 tkPriv.$disp tkPriv
    global tkPriv
    if [info exists tkPriv] {
	set tkPriv(screen) $screen
	return
    }
    set tkPriv(afterId) {}
    set tkPriv(buttons) 0
    set tkPriv(buttonWindow) {}
    set tkPriv(dragging) {}
    set tkPriv(focus) {}
    set tkPriv(focusContinue) 0
    set tkPriv(focusDir) next
    set tkPriv(focusOrigin) {}
    set tkPriv(grab) {}
    set tkPriv(inMenubutton) {}
    set tkPriv(initMouse) {}
    set tkPriv(mouseMoved) 0
    set tkPriv(popup) {}
    set tkPriv(postedMb) {}
    set tkPriv(screen) $screen
    set tkPriv(selectMode) char
    set tkPriv(window) {}
}

# Do initial setup for tkPriv, so that it is always bound to something
# (otherwise, if someone references it, it may get set to a non-upvar-ed
# value, which will cause trouble later).

tkScreenChanged [winfo screen .]

# tkCancelRepeat --
# This procedure is invoked to cancel an auto-repeat action described
# by tkPriv(afterId).
#
# Arguments:
# None.

proc tkCancelRepeat {} {
    global tkPriv
    after cancel $tkPriv(afterId)
    set tkPriv(afterId) {}
}


# scrollbar.tcl --
#
# This file defines the default bindings for Tk scrollbar widgets.
#
# @(#) scrollbar.tcl 1.7 95/01/05 08:51:48
#
# Copyright (c) 1994 The Regents of the University of California.
# Copyright (c) 1994 Sun Microsystems, Inc.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
#

# tkScrollbarBind --
# This procedure below invoked the first time the mouse enters a
# scrollbar widget or an scrollbar widget receives the input focus.  It
# creates all of the class bindings for scrollbars.
#
# Arguments:
# w -		The event in which the window occurred.
# x, y -	Coordinates of the mouse.
# event -	Indicates which event caused the procedure to be invoked
#		(Enter or FocusIn).  It is used so that we can carry out
#		the functions of that event in addition to setting up
#		bindings.

proc tkScrollbarBind {w x y {event {}}} {
    global tk_strictMotif tkPriv

    bind Scrollbar <FocusIn> {}

    # Standard Motif bindings:

    bind Scrollbar <Enter> {
	if $tk_strictMotif {
	    set tkPriv(activeBg) [%W cget -activebackground]
	    %W config -activebackground [%W cget -background]
	}
	%W activate [%W identify %x %y]
    }

    # If the event that triggered us was an Enter, execute the new
    # contents of the Enter binding directly, since the binding won't
    # be run automatically until the next Enter event.

    if {$event == "Enter"} {
	if $tk_strictMotif {
	    set tkPriv(activeBg) [$w cget -activebackground]
	    $w config -activebackground [$w cget -background]
	}
	$w activate [$w identify $x $y]
    }

    bind Scrollbar <Motion> {
	%W activate [%W identify %x %y]
    }
    bind Scrollbar <Leave> {
	if $tk_strictMotif {
	    %W config -activebackground $tkPriv(activeBg)
	}
	%W activate {}
    }
    bind Scrollbar <1> {
	tkScrollButtonDown %W %x %y
    }
    bind Scrollbar <B1-Motion> {
	tkScrollDrag %W %x %y
    }
    bind Scrollbar <ButtonRelease-1> {
	tkScrollButtonUp %W %x %y
    }
    bind Scrollbar <B1-Leave> {
	# Prevents <Leave> binding from being invoked.
    }
    bind Scrollbar <B1-Enter> {
	# Prevents <Enter> binding from being invoked.
    }
    bind Scrollbar <2> {
	tkScrollButtonDown %W %x %y
    }
    bind Scrollbar <B2-Motion> {
	tkScrollDrag %W %x %y
    }
    bind Scrollbar <ButtonRelease-2> {
	tkScrollButtonUp %W %x %y
    }
    bind Scrollbar <B2-Leave> {
	# Prevents <Leave> binding from being invoked.
    }
    bind Scrollbar <B2-Enter> {
	# Prevents <Enter> binding from being invoked.
    }
    bind Scrollbar <Control-1> {
	tkScrollTopBottom %W %x %y
    }
    bind Scrollbar <Control-2> {
	tkScrollTopBottom %W %x %y
    }

    bind Scrollbar <Up> {
	tkScrollByUnits %W v -1
    }
    bind Scrollbar <Down> {
	tkScrollByUnits %W v 1
    }
    bind Scrollbar <Control-Up> {
	tkScrollByPages %W v -1
    }
    bind Scrollbar <Control-Down> {
	tkScrollByPages %W v 1
    }
    bind Scrollbar <Left> {
	tkScrollByUnits %W h -1
    }
    bind Scrollbar <Right> {
	tkScrollByUnits %W h 1
    }
    bind Scrollbar <Control-Left> {
	tkScrollByPages %W h -1
    }
    bind Scrollbar <Control-Right> {
	tkScrollByPages %W h 1
    }
    bind Scrollbar <Prior> {
	tkScrollByPages %W hv -1
    }
    bind Scrollbar <Next> {
	tkScrollByPages %W hv 1
    }
    bind Scrollbar <Home> {
	tkScrollToPos %W 0
    }
    bind Scrollbar <End> {
	tkScrollToPos %W 1
    }
    rename tkScrollbarBind {}
}

# tkScrollButtonDown --
# This procedure is invoked when a button is pressed in a scrollbar.
# It changes the way the scrollbar is displayed and takes actions
# depending on where the mouse is.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	Mouse coordinates.

proc tkScrollButtonDown {w x y} {
    $w configure -activerelief sunken
    set element [$w identify $x $y]
    if {$element == "slider"} {
	tkScrollStartDrag $w $x $y
    } else {
	tkScrollSelect $w $element initial
    }
}

# tkScrollButtonUp --
# This procedure is invoked when a button is released in a scrollbar.
# It cancels scans and auto-repeats that were in progress, and restores
# the way the active element is displayed.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	Mouse coordinates.

proc tkScrollButtonUp {w x y} {
    tkCancelRepeat
    $w configure -activerelief raised
    tkScrollEndDrag $w $x $y
    $w activate [$w identify $x $y]
}

# tkScrollSelect --
# This procedure is invoked when button 1 is pressed over the scrollbar.
# It invokes one of several scrolling actions depending on where in
# the scrollbar the button was pressed.
#
# Arguments:
# w -		The scrollbar widget.
# element -	The element of the scrollbar that was selected, such
#		as "arrow1" or "trough2".  Shouldn't be "slider".
# repeat -	Whether and how to auto-repeat the action:  "noRepeat"
#		means don't auto-repeat, "initial" means this is the
#		first action in an auto-repeat sequence, and "again"
#		means this is the second repetition or later.

proc tkScrollSelect {w element repeat} {
    global tkPriv
    if {$element == "arrow1"} {
	tkScrollByUnits $w hv -1
    } elseif {$element == "trough1"} {
	tkScrollByPages $w hv -1
    } elseif {$element == "trough2"} {
	tkScrollByPages $w hv 1
    } elseif {$element == "arrow2"} {
	tkScrollByUnits $w hv 1
    } else {
	return
    }
    if {$repeat == "again"} {
	set tkPriv(afterId) [after [$w cget -repeatinterval] \
		tkScrollSelect $w $element again]
    } elseif {$repeat == "initial"} {
	set tkPriv(afterId) [after [$w cget -repeatdelay] \
		tkScrollSelect $w $element again]
    }
}

# tkScrollStartDrag --
# This procedure is called to initiate a drag of the slider.  It just
# remembers the starting position of the slider.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	The mouse position at the start of the drag operation.

proc tkScrollStartDrag {w x y} {
    global tkPriv

    if {[$w cget -command] == ""} {
	return
    }
    set tkPriv(initMouse) [$w fraction $x $y]
    set tkPriv(initValues) [$w get]
    if {[llength $tkPriv(initValues)] == 2} {
	set tkPriv(initPos) [lindex $tkPriv(initValues) 0]
    } else {
	set tkPriv(initPos) [expr (double([lindex $tkPriv(initValues) 2])) \
		/ [lindex $tkPriv(initValues) 0]]
    }
}

# tkScrollDrag --
# This procedure is called for each mouse motion even when the slider
# is being dragged.  It notifies the associated widget if we're not
# jump scrolling, and it just updates the scrollbar if we are jump
# scrolling.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	The current mouse position.

proc tkScrollDrag {w x y} {
    global tkPriv

    if {$tkPriv(initMouse) == ""} {
	return
    }
    set f [$w fraction $x $y]
    set delta [expr $f - $tkPriv(initMouse)]
    if [$w cget -jump] {
	if {[llength $tkPriv(initValues)] == 2} {
	    $w set [expr [lindex $tkPriv(initValues) 0] + $delta] \
		    [expr [lindex $tkPriv(initValues) 1] + $delta]
	} else {
	    set delta [expr round($delta * [lindex $tkPriv(initValues) 0])]
	    eval $w set [lreplace $tkPriv(initValues) 2 3 \
		    [expr [lindex $tkPriv(initValues) 2] + $delta] \
		    [expr [lindex $tkPriv(initValues) 3] + $delta]]
	}
    } else {
	tkScrollToPos $w [expr $tkPriv(initPos) + $delta]
    }
}

# tkScrollEndDrag --
# This procedure is called to end an interactive drag of the slider.
# If the mouse is far from the window then it just restores the position
# to what it was at the start of the drag;  otherwise it scrolls the
# window if we're in jump mode, otherwise it does nothing.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	The mouse position at the end of the drag operation.

proc tkScrollEndDrag {w x y} {
    global tkPriv

    if {$tkPriv(initMouse) == ""} {
	return
    }
    if {($x < -10) || ($y < -10) || ($x > ([winfo width $w] + 10))
	    || ($y > ([winfo height $w] + 10))} {
	if [$w cget -jump] {
	    eval $w set $tkPriv(initValues)
	} else {
	    tkScrollToPos $w $tkPriv(initPos)
	}
    } elseif [$w cget -jump] {
	tkScrollToPos $w [expr $tkPriv(initPos) + [$w fraction $x $y] \
		- $tkPriv(initMouse)]
    }
    set tkPriv(initMouse) ""
}

# tkScrollByUnits --
# This procedure tells the scrollbar's associated widget to scroll up
# or down by a given number of units.  It notifies the associated widget
# in different ways for old and new command syntaxes.
#
# Arguments:
# w -		The scrollbar widget.
# orient -	Which kinds of scrollbars this applies to:  "h" for
#		horizontal, "v" for vertical, "hv" for both.
# amount -	How many units to scroll:  typically 1 or -1.

proc tkScrollByUnits {w orient amount} {
    set cmd [$w cget -command]
    if {($cmd == "") || ([string first \
	    [string index [$w cget -orient] 0] $orient] < 0)} {
	return
    }
    set info [$w get]
    if {[llength $info] == 2} {
	eval $cmd scroll $amount units
    } else {
	eval $cmd [expr [lindex $info 2] + $amount]
    }
}

# tkScrollByPages --
# This procedure tells the scrollbar's associated widget to scroll up
# or down by a given number of screenfuls.  It notifies the associated
# widget in different ways for old and new command syntaxes.
#
# Arguments:
# w -		The scrollbar widget.
# orient -	Which kinds of scrollbars this applies to:  "h" for
#		horizontal, "v" for vertical, "hv" for both.
# amount -	How many screens to scroll:  typically 1 or -1.

proc tkScrollByPages {w orient amount} {
    set cmd [$w cget -command]
    if {($cmd == "") || ([string first \
	    [string index [$w cget -orient] 0] $orient] < 0)} {
	return
    }
    set info [$w get]
    if {[llength $info] == 2} {
	eval $cmd scroll $amount pages
    } else {
	eval $cmd [expr [lindex $info 2] + $amount*([lindex $info 1] - 1)]
    }
}

# tkScrollToPos --
# This procedure tells the scrollbar's associated widget to scroll to
# a particular location, given by a fraction between 0 and 1.  It notifies
# the associated widget in different ways for old and new command syntaxes.
#
# Arguments:
# w -		The scrollbar widget.
# pos -		A fraction between 0 and 1 indicating a desired position
#		in the document.

proc tkScrollToPos {w pos} {
    set cmd [$w cget -command]
    if {($cmd == "")} {
	return
    }
    set info [$w get]
    if {[llength $info] == 2} {
	eval $cmd moveto $pos
    } else {
	eval $cmd [expr round([lindex $info 0]*$pos)]
    }
}

# tkScrollTopBottom
# Scroll to the top or bottom of the document, depending on the mouse
# position.
#
# Arguments:
# w -		The scrollbar widget.
# x, y -	Mouse coordinates within the widget.

proc tkScrollTopBottom {w x y} {
    set element [$w identify $x $y]
    if [string match *1 $element] {
	tkScrollToPos $w 0
    } elseif [string match *2 $element] {
	tkScrollToPos $w 1
    }
}

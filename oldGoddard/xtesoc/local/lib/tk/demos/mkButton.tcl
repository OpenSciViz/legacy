# mkButton w
#
# Create a top-level window that displays a bunch of buttons.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkButton.tcl 1.2 94/12/23 15:51:55

proc mkButton {{w .b1}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Button Demonstration"
    wm iconname $w "Buttons"
    message $w.msg -font -Adobe-times-medium-r-normal--*-180-*-*-*-*-*-* \
            -aspect 300 \
	    -text "If you click on any of the top four buttons below, the background of the button area will change to the color indicated in the button.  Click the \"OK\" button when you've seen enough."
    pack $w.msg -side top -fill both

    button $w.b1 -text "Peach Puff" -width 10 \
	    -command "$w config -bg PeachPuff1"
    button $w.b2 -text "Light Blue" -width 10 \
	    -command "$w config -bg LightBlue1"
    button $w.b3 -text "Sea Green" -width 10 \
	    -command "$w config -bg SeaGreen2"
    button $w.b4 -text "Yellow" -width 10 \
	    -command "$w config -bg Yellow1"
    button $w.ok -text OK -width 10  -command "destroy $w"
    pack $w.b1 $w.b2 $w.b3 $w.b4 $w.ok -side top -expand yes -pady 2
}

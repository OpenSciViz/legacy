# mkEntry w
#
# Create a top-level window that displays a bunch of entries.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkEntry.tcl 1.2 94/12/23 15:51:57

proc mkEntry {{w .e1}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Entry Demonstration"
    wm iconname $w "Entries"
    label $w.msg -font -Adobe-times-medium-r-normal--*-180-*-*-*-*-*-* \
            -wraplength 5i -justify left \
	    -text "Three different entries are displayed below.  You can add characters by pointing, clicking and typing.  You can delete by selecting and typing Backspace, Delete, or Control-X.  Backspace and Control-h erase the character to the left of the insertion cursor, Delete and Control-d delete the chararacter to the right of the insertion cursor, Control-W erases the word to the left of the insertion cursor, and Meta-d deletes the word to the right of the insertion cursor.  For entries that are too large to fit in the window all at once, you can scan through the entries by dragging with mouse button 2 pressed.  Click the \"OK\" button when you've seen enough."
    frame $w.frame -borderwidth 10
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg $w.frame -side top -fill both
    pack $w.ok -side top

    entry $w.frame.e1 -relief sunken
    entry $w.frame.e2 -relief sunken
    entry $w.frame.e3 -relief sunken
    pack $w.frame.e1 $w.frame.e2 $w.frame.e3 -side top -pady 5 -fill x

    $w.frame.e1 insert 0 "Initial value"
    $w.frame.e2 insert end "This entry contains a long value, much too long "
    $w.frame.e2 insert end "to fit in the window at one time, so long in fact "
    $w.frame.e2 insert end "that you'll have to scan or scroll to see the end."
}

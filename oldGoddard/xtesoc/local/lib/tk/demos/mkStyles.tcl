# mkStyles w
#
# Create a top-level window with a text widget that demonstrates the
# various display styles that are available in texts.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkStyles.tcl 1.2 94/12/23 15:52:15

proc mkStyles {{w .styles}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Text Demonstration - Display Styles"
    wm iconname $w "Text Styles"

    button $w.ok -text OK -command "destroy $w" -width 8
    text $w.t -yscrollcommand "$w.s set" -setgrid true \
	    -width 70 -height 28 -wrap word
    scrollbar $w.s -command "$w.t yview"
    pack $w.ok -side bottom
    pack $w.s -side right -fill y
    pack $w.t -expand yes -fill both

    # Set up display styles

    $w.t tag configure bold -font -Adobe-Courier-Bold-O-Normal--*-120-*-*-*-*-*-*
    $w.t tag configure big -font -Adobe-Courier-Bold-R-Normal--*-140-*-*-*-*-*-*
    $w.t tag configure verybig -font -Adobe-Helvetica-Bold-R-Normal--*-240-*-*-*-*-*-*
    if {[tk colormodel $w] == "color"} {
	$w.t tag configure color1 -background #eed5b7
	$w.t tag configure color2 -foreground red
	$w.t tag configure raised -background #eed5b7 -relief raised \
		-borderwidth 1
	$w.t tag configure sunken -background #eed5b7 -relief sunken \
		-borderwidth 1
    } else {
	$w.t tag configure color1 -background black -foreground white
	$w.t tag configure color2 -background black -foreground white
	$w.t tag configure raised -background white -relief raised \
		-borderwidth 1
	$w.t tag configure sunken -background white -relief sunken \
		-borderwidth 1
    }
    $w.t tag configure bgstipple -background black -borderwidth 0 \
	    -bgstipple gray25
    $w.t tag configure fgstipple -fgstipple gray50
    $w.t tag configure underline -underline on
    $w.t tag configure right -justify right
    $w.t tag configure center -justify center
    $w.t tag configure super -offset 4p \
	    -font -Adobe-Courier-Medium-R-Normal--*-100-*-*-*-*-*-*
    $w.t tag configure sub -offset -2p \
	    -font -Adobe-Courier-Medium-R-Normal--*-100-*-*-*-*-*-*
    $w.t tag configure margins -lmargin1 12m -lmargin2 6m -rmargin 10m
    $w.t tag configure spacing -spacing1 10p -spacing2 2p \
	    -lmargin1 12m -lmargin2 6m -rmargin 10m

    $w.t insert end {Text widgets like this one allow you to display information in a
variety of styles.  Display styles are controlled using a mechanism
called }
    $w.t insert end tags bold
    $w.t insert end {. Tags are just textual names that you can apply to one
or more ranges of characters within a text widget.  You can configure
tags with various display styles.  If you do this, then the tagged
characters will be displayed with the styles you chose.  The
available display styles are:
}
    $w.t insert end "\n1. Font." big
    $w.t insert end "  You can choose any X font, "
    $w.t insert end large verybig
    $w.t insert end " or "
    $w.t insert end "small.\n"
    $w.t insert end "\n2. Color." big
    $w.t insert end "  You can change either the "
    $w.t insert end background color1
    $w.t insert end " or "
    $w.t insert end foreground color2
    $w.t insert end "\ncolor, or "
    $w.t insert end both {color1 color2}
    $w.t insert end ".\n"
    $w.t insert end "\n3. Stippling." big
    $w.t insert end "  You can cause either the "
    $w.t insert end background bgstipple
    $w.t insert end " or "
    $w.t insert end foreground fgstipple
    $w.t insert end {
information to be drawn with a stipple fill instead of a solid fill.
}
    $w.t insert end "\n4. Underlining." big
    $w.t insert end "  You can "
    $w.t insert end underline underline
    $w.t insert end " ranges of text.\n"
    $w.t insert end "\n5. 3-D effects." big
    $w.t insert end {  You can arrange for the background to be drawn
with a border that makes characters appear either }
    $w.t insert end raised raised
    $w.t insert end " or "
    $w.t insert end sunken sunken
    $w.t insert end ".\n"
    $w.t insert end "\n6. Justification." big
    $w.t insert end " You can arrange for lines to be displayed\n"
    $w.t insert end "left-justified,\n"
    $w.t insert end "right-justified, or\n" right
    $w.t insert end "centered.\n" center
    $w.t insert end "\n7. Superscripts and subscripts."  big
    $w.t insert end " You can control the vertical\n"
    $w.t insert end "position of text to generate superscript effects like 10"
    $w.t insert end "n" super
    $w.t insert end " or\nsubscript effects like X"
    $w.t insert end "i" sub
    $w.t insert end ".\n"
    $w.t insert end "\n8. Margins." big
    $w.t insert end " You can control the amount of extra space left"
    $w.t insert end " on\neach side of the text:\n"
    $w.t insert end "This paragraph is an example of the use of " margins
    $w.t insert end "margins.  It consists of a single line of text " margins
    $w.t insert end "that wraps around on the screen.  There are two " margins
    $w.t insert end "separate left margin values, one for the first " margins
    $w.t insert end "display line associated with the text line, " margins
    $w.t insert end "and one for the subsequent display lines, which " margins
    $w.t insert end "occur because of wrapping.  There is also a " margins
    $w.t insert end "separate specification for the right margin, " margins
    $w.t insert end "which is used to choose wrap points for lines.\n" margins
    $w.t insert end "\n9. Spacing." big
    $w.t insert end " You can control the spacing of lines with three\n"
    $w.t insert end "separate parameters.  \"Spacing1\" tells how much "
    $w.t insert end "extra space to leave\nabove a line, \"spacing3\" "
    $w.t insert end "tells how much space to leave below a line,\nand "
    $w.t insert end "if a text line wraps, \"spacing2\" tells how much "
    $w.t insert end "space to leave\nbetween the display lines that "
    $w.t insert end "make up the text line.\n"
    $w.t insert end "These indented paragraphs illustrate how spacing " spacing
    $w.t insert end "can be used.  Each paragraph is actually a " spacing
    $w.t insert end "single line in the text widget, which is " spacing
    $w.t insert end "word-wrapped by the widget.\n" spacing
    $w.t insert end "Spacing1 is set to 10 points for this text, " spacing
    $w.t insert end "which results in relatively large gaps between " spacing
    $w.t insert end "the paragraphs.  Spacing2 is set to 2 points, " spacing
    $w.t insert end "which results in just a bit of extra space " spacing
    $w.t insert end "within a pararaph.  Spacing3 isn't used " spacing
    $w.t insert end "in this example.\n" spacing
    $w.t insert end "To see where the space is, select ranges of " spacing
    $w.t insert end "text within these paragraphs.  The selection " spacing
    $w.t insert end "highlight will cover the extra space." spacing
}

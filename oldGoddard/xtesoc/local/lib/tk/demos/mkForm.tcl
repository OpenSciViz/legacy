# mkForm w
#
# Create a top-level window that displays a bunch of entries with
# tabs set up to move between them.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkForm.tcl 1.2 94/12/23 15:52:01

proc mkForm {{w .form}} {
    global tabList
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Form Demonstration"
    wm iconname $w "Form"
    label $w.msg -font -Adobe-times-medium-r-normal--*-180-*-*-*-*-*-* \
            -wraplength 4i -justify left -text "This window contains a simple form where you can type in the various entries and use tabs to move circularly between the entries.  Click the \"OK\" button or type return when you're done."
    foreach i {f1 f2 f3 f4 f5} {
	frame $w.$i -bd 1m
	entry $w.$i.entry -relief sunken -width 40
	bind $w.$i.entry <Return> "destroy $w"
	label $w.$i.label
	pack $w.$i.entry -side right
	pack $w.$i.label -side left
    }
    $w.f1.label config -text Name:
    $w.f2.label config -text Address:
    $w.f5.label config -text Phone:
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg $w.f1 $w.f2 $w.f3 $w.f4 $w.f5 -side top -fill x
    pack $w.ok -side top
}

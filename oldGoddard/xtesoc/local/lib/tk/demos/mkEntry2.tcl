# mkEntry2 -
#
# Create a top-level window that displays a bunch of entries with
# scrollbars.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkEntry2.tcl 1.3 94/12/23 15:51:58

proc mkEntry2 {{w .e2}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Entry Demonstration"
    wm iconname $w "Entries"
    label $w.msg -font -Adobe-times-medium-r-normal--*-180* -wraplength 5i \
	    -justify left \
	    -text "Three different entries are displayed below, with a scrollbar for each entry.  You can add characters by pointing, clicking, and typing.  You can delete by selecting and typing Backspace, Delete, or Control-X.  Backspace and Control-h erase the character to the left of the insertion cursor, Delete and Control-d delete the chararacter to the right of the insertion cursor, Control-W erases the word to the left of the insertion cursor, and Meta-d deletes the word to the right of the insertion cursor.  For entries that are too large to fit in the window all at once, you can scan through the entries using the scrollbars, or by dragging with mouse button 2 pressed.  Click the \"OK\" button when you've seen enough."
    frame $w.frame -borderwidth 10
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg $w.frame -side top -fill both
    pack $w.ok -side top

    entry $w.frame.e1 -relief sunken -xscrollcommand "$w.frame.s1 set"
    scrollbar $w.frame.s1 -relief sunken -orient horiz -command \
	    "$w.frame.e1 xview"
    frame $w.frame.f1 -width 20 -height 10
    entry $w.frame.e2 -relief sunken -xscrollcommand "$w.frame.s2 set"
    scrollbar $w.frame.s2 -relief sunken -orient horiz -command \
	    "$w.frame.e2 xview"
    frame $w.frame.f2 -width 20 -height 10
    entry $w.frame.e3 -relief sunken -xscrollcommand "$w.frame.s3 set"
    scrollbar $w.frame.s3 -relief sunken -orient horiz -command \
	    "$w.frame.e3 xview"
    pack $w.frame.e1 $w.frame.s1 $w.frame.f1 $w.frame.e2 $w.frame.s2 \
	    $w.frame.f2 $w.frame.e3 $w.frame.s3 -side top -fill x

    $w.frame.e1 insert 0 "Initial value"
    $w.frame.e2 insert end "This entry contains a long value, much too long "
    $w.frame.e2 insert end "to fit in the window at one time, so long in fact "
    $w.frame.e2 insert end "that you'll have to scan or scroll to see the end."
}

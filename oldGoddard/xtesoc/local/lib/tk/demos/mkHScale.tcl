# mkHScale w
#
# Create a top-level window that displays a horizontal scale.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkHScale.tcl 1.2 94/12/23 15:52:02

proc mkHScale {{w .scale2}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Horizontal Scale Demonstration"
    wm iconname $w "Scale"
    label $w.msg -font -Adobe-times-medium-r-normal--*-180-*-*-*-*-*-* \
            -wraplength 3i -justify left \
	    -text "An arrow and a horizontal scale are displayed below.  If you click or drag mouse button 1 in the scale, you can change the length of the arrow.  Click the \"OK\" button when you're finished."
    frame $w.frame -borderwidth 10
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg $w.frame -side top -fill x
    pack $w.ok -side top

    canvas $w.frame.canvas -width 50 -height 50 -bd 0 -highlightthickness 0
    $w.frame.canvas create polygon 0 0 1 1 2 2 -fill DeepSkyBlue3 -tags poly
    $w.frame.canvas create line 0 0 1 1 2 2 0 0 -fill black -tags line
    scale $w.frame.scale -orient horizontal -length 284 -from 0 -to 250 \
	    -command "setWidth $w.frame.canvas" -tickinterval 50
    pack $w.frame.canvas -side top -expand yes -anchor s -fill x
    pack $w.frame.scale -side bottom -expand yes -anchor n
    $w.frame.scale set 75
}

proc setWidth {w width} {
    incr width 21
    set x2 [expr $width - 30]
    if {$x2 < 21} {
	set x2 21
    }
    $w coords poly 20 15 20 35 $x2 35 $x2 45 $width 25 $x2 5 $x2 15 20 15
    $w coords line 20 15 20 35 $x2 35 $x2 45 $width 25 $x2 5 $x2 15 20 15
}

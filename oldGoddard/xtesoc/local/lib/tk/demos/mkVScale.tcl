# mkVScale w
#
# Create a top-level window that displays a vertical scale.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkVScale.tcl 1.2 94/12/23 15:52:18

proc mkVScale {{w .scale1}} {
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Vertical Scale Demonstration"
    wm iconname $w "Scale"
    label $w.msg -font -Adobe-times-medium-r-normal--*-180* -wraplength 4i \
	    -justify left -text "An arrow and a vertical scale are displayed below.  If you click or drag mouse button 1 in the scale, you can change the size of the arrow.  Click the \"OK\" button when you're finished."
    frame $w.frame -borderwidth 10
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg $w.frame $w.ok

    scale $w.frame.scale -orient vertical -length 284 -from 0 -to 250 \
	    -command "setHeight $w.frame.canvas" -tickinterval 50
    canvas $w.frame.canvas -width 50 -height 50 -bd 0 -highlightthickness 0
    $w.frame.canvas create polygon 0 0 1 1 2 2 -fill SeaGreen3 -tags poly
    $w.frame.canvas create line 0 0 1 1 2 2 0 0 -fill black -tags line
    frame $w.frame.right -borderwidth 15
    pack $w.frame.scale -side left -anchor ne
    pack $w.frame.canvas -side left -anchor nw -fill y
    $w.frame.scale set 75
}

proc setHeight {w height} {
    incr height 21
    set y2 [expr $height - 30]
    if {$y2 < 21} {
	set y2 21
    }
    $w coords poly 15 20 35 20 35 $y2 45 $y2 25 $height 5 $y2 15 $y2 15 20
    $w coords line 15 20 35 20 35 $y2 45 $y2 25 $height 5 $y2 15 $y2 15 20
}

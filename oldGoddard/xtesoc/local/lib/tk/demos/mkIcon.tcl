# mkIcon w
#
# Create a top-level window that displays a bunch of iconic
# buttons.
#
# Arguments:
#    w -	Name to use for new top-level window.
#
# @(#) mkIcon.tcl 1.3 94/12/23 15:52:03

proc mkIcon {{w .icon}} {
    global tk_library
    catch {destroy $w}
    toplevel $w
    dpos $w
    wm title $w "Iconic Button Demonstration"
    wm iconname $w "Icons"
    image create bitmap flagup -file $tk_library/demos/images/flagup \
	    -maskfile $tk_library/demos/images/flagup
    image create bitmap flagdown -file $tk_library/demos/images/flagdown \
	    -maskfile $tk_library/demos/images/flagdown
    label $w.msg -font -Adobe-times-medium-r-normal--*-180-*-*-*-*-*-* \
            -wraplength 4.5i -justify left \
	    -text "This window shows three ways of using bitmaps or images in radiobuttons and checkbuttons.  On the left are two radiobuttons, each of which displays a bitmap and an indicator.  In the middle is a checkbutton that displays a different image depending on whether it is selected or not.  On the right is a checkbutton that displays a single bitmap but changes its background color to indicate whether or not it is selected.  Click the \"OK\" button when you're done."
    frame $w.frame -borderwidth 10
    button $w.ok -text OK -command "destroy $w" -width 8
    pack $w.msg -side top
    pack $w.frame $w.ok -side top

    checkbutton $w.frame.b1 -image flagdown -selectimage flagup \
	    -indicatoron 0 -selectcolor bisque1 -activebackground bisque1
    checkbutton $w.frame.b2 -bitmap @$tk_library/demos/images/letters \
	    -indicatoron 0 -selectcolor #efbd9b
    frame $w.frame.left
    pack $w.frame.left $w.frame.b1 $w.frame.b2 -side left -expand yes -padx 5m

    radiobutton $w.frame.left.b3 -bitmap @$tk_library/demos/images/letters \
	    -variable letters -value full
    radiobutton $w.frame.left.b4 -bitmap @$tk_library/demos/images/noletters \
	    -variable letters -value empty
    pack $w.frame.left.b3 $w.frame.left.b4 -side top -expand yes
}

proc iconCmd {w} {
    global tk_library
    set bitmap [lindex [$w config -bitmap] 4]
    if {$bitmap == "@$tk_library/demos/images/flagdown"} {
	$w config -bitmap @$tk_library/demos/images/flagup
    } else {
	$w config -bitmap @$tk_library/demos/images/flagdown
    }
}

This is Info file ../info/rmail.info, produced by Makeinfo-1.55 from
the input file rmail.texi.

   This file documents the Rmail mail reader.

   Copyright (C) 1991 Free Software Foundation

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.


File: rmail.info,  Node: Top,  Next: Rmail Scrolling,  Up: (DIR)

   NOTE: The recommended mail reader in XEmacs is VM, which provides
more flexibility than Rmail and stores mail in standard Unix-mail-format
folders rather than in a special format.  VM comes with its own manual,
included standard with XEmacs.

   XEmacs also provides a sophisticated and comfortable front-end to the
MH mail-processing system, called `mh-e'.

   This manual documents the Rmail mail reader under Emacs.

   Rmail is an Emacs subsystem for reading and disposing of mail that
you receive.  Rmail stores mail messages in files called Rmail files.
You read the messages in an Rmail file in a special major mode, Rmail
mode, which redefines most letters to run commands for managing mail.
To enter Rmail, type `M-x rmail'.  This reads your primary mail file,
merges new mail in from your inboxes, displays the first new message,
and lets you begin reading.

   Using Rmail in the simplest fashion, you have one Rmail file,
`~/RMAIL', in which all of your mail is saved.  It is called your
"primary mail file".  You can also copy messages into other Rmail files
and then edit those files with Rmail.

   Rmail displays only one message at a time.  It is called the
"current message".  Rmail mode's special commands can move to another
message, delete the message, copy the message into another file, or
send a reply.

   Within the Rmail file, messages are arranged sequentially in order
of receipt.  They are also assigned consecutive integers as their
"message numbers".  The number of the current message is displayed in
Rmail's mode line, followed by the total number of messages in the
file.  You can move to a message by specifying its message number using
the `j' key (*note Rmail Motion::.).

   Following the usual conventions of Emacs, changes in an Rmail file
become permanent only when the file is saved.  You can do this with `s'
(`rmail-save'), which also expunges deleted messages from the file
first (*note Rmail Deletion::.).  To save the file without expunging,
use `C-x C-s'.  Rmail saves the Rmail file automatically when moving new
mail from an inbox file (*note Rmail Inbox::.).

   You can exit Rmail with `q' (`rmail-quit'); this expunges and saves
the Rmail file and then switches to another buffer.  However, there is
no need to `exit' formally.  If you switch from Rmail to editing in
other buffers, and never happen to switch back, you have exited.  Just
make sure to save the Rmail file eventually (like any other file you
have changed).  `C-x s' is a good enough way to do this.

* Menu:

* Scroll: Rmail Scrolling.   Scrolling through a message.
* Motion: Rmail Motion.      Moving to another message.
* Deletion: Rmail Deletion.  Deleting and expunging messages.
* Inbox: Rmail Inbox.        How mail gets into the Rmail file.
* Files: Rmail Files.        Using multiple Rmail files.
* Output: Rmail Output.	     Copying message out to files.
* Labels: Rmail Labels.      Classifying messages by labeling them.
* Summary: Rmail Summary.    Summaries show brief info on many messages.
* Reply: Rmail Reply.        Sending replies to messages you are viewing.
* Editing: Rmail Editing.    Editing message text and headers in Rmail.
* Digest: Rmail Digest.      Extracting the messages from a digest message.


File: rmail.info,  Node: Rmail Scrolling,  Next: Rmail Motion,  Prev: Top,  Up: Top

Scrolling Within a Message
**************************

   When Rmail displays a message that does not fit on the screen, you
have to scroll through it.  You could use `C-v', `M-v', and `M-<', but
scrolling is so frequent in Rmail that it deserves to be easier to type.

`SPC'
     Scroll forward (`scroll-up').

`DEL'
     Scroll backward (`scroll-down').

`.'
     Scroll to start of message (`rmail-beginning-of-message').

   Since the most common thing to do while reading a message is to
scroll through it by screenfuls, Rmail makes SPC and DEL synonyms of
`C-v' (`scroll-up') and `M-v' (`scroll-down')

   The command `.' (`rmail-beginning-of-message') scrolls back to the
beginning of a selected message.  This is not quite the same as `M-<':
first, it does not set the mark; secondly, it resets the buffer
boundaries to the current message if you have changed them.


File: rmail.info,  Node: Rmail Motion,  Next: Rmail Deletion,  Prev: Rmail Scrolling,  Up: Top

Moving Among Messages
*********************

   The most basic thing to do with a message is to read it.  The way to
do this in Rmail is to make the message current.  You can make any
message current, given its message number, by using the `j' command, but
people most often move sequentially through the file, since this is the
order of receipt of messages.  When you enter Rmail, you are positioned
at the first new message (new messages are those received after you last
used Rmail), or at the last message if there are no new messages this
time.  Move forward to see other new messages if there are any; move
backward to re-examine old messages.

`n'
     Move to the next non-deleted message, skipping any intervening
     deleted
     messages (`rmail-next-undeleted-message').

`p'
     Move to the previous non-deleted message
     (`rmail-previous-undeleted-message').

`M-n'
     Move to the next message, including deleted messages
     (`rmail-next-message').

`M-p'
     Move to the previous message, including deleted messages
     (`rmail-previous-message').

`j'
     Move to the first message.  With argument N, move to message
     number N (`rmail-show-message').

`>'
     Move to the last message (`rmail-last-message').

`M-s REGEXP RET'
     Move to the next message containing a match for REGEXP
     (`rmail-search').  If REGEXP is empty, the last regexp used is
     used again.

`- M-s REGEXP RET'
     Move to the previous message containing a match for REGEXP.  If
     REGEXP is empty, the last regexp used is used again.

   To move among messages in Rmail, you can use `n' and `p'.  These
keys move through the messages sequentially but skip over deleted
messages, which is usually what you want to do.  Their command
definitions are named `rmail-next-undeleted-message' and
`rmail-previous-undeleted-message'.  If you do not want to skip deleted
messages--for example, if you want to move to a message to undelete
it--use the variants `M-n' (`rmail-next-message') and `M-p'
(`rmail-previous-message').  A numeric argument to any of these
commands serves as a repeat count.

   In Rmail, you can specify a numeric argument by just typing the
digits.  It is not necessary to type `C-u' first.

   The `M-s' (`rmail-search') command is Rmail's version of search.
The usual incremental search command `C-s' works in Rmail, but searches
only within the current message.  The purpose of `M-s' is to search for
another message.  It reads a regular expression non-incrementally, then
starts searching at the beginning of the following message for a match.
The message containing the match is selected.

   To search backward in the file for another message, give `M-s' a
negative argument.  In Rmail you can do this with `- M-s'.

   It is also possible to search for a message based on labels.  *Note
Rmail Labels::.

   To move to a message specified by absolute message number, use `j'
(`rmail-show-message') with the message number as argument.  With no
argument, `j' selects the first message.  `>' (`rmail-last-message')
selects the last message.


File: rmail.info,  Node: Rmail Deletion,  Next: Rmail Inbox,  Prev: Rmail Motion,  Up: Top

Deleting Messages
*****************

   When you no longer need to keep a message, you can "delete" it.  This
flags it as ignorable, and some Rmail commands will pretend it is no
longer present, but it still has its place in the Rmail file and still
has its message number.

   "Expunging" the Rmail file actually removes the deleted messages.
The remaining messages are renumbered consecutively.  Expunging is the
only action that changes the message number of any message, except for
undigestifying (*note Rmail Digest::.).

`d'
     Delete the current message and move to the next non-deleted message
     (`rmail-delete-forward').

`C-d'
     Delete the current message and move to the previous non-deleted
     message (`rmail-delete-backward').

`u'
     Undelete the current message, or move back to a deleted message and
     undelete it (`rmail-undelete-previous-message').

`e'
`x'
     Expunge the Rmail file (`rmail-expunge').  These two commands are
     synonyms.

   There are two Rmail commands for deleting messages.  Both delete the
current message and select another message.  `d'
(`rmail-delete-forward') moves to the following message, skipping
messages already deleted, while `C-d' (`rmail-delete-backward') moves
to the previous non-deleted message.  If there is no non-deleted
message to move to in the specified direction, the message that was just
deleted remains current.

   To make all deleted messages disappear from the Rmail file, type `e'
(`rmail-expunge').  Until you do this, you can still "undelete" the
deleted messages.

   To undelete, type `u' (`rmail-undelete-previous-message'), which
cancels the effect of a `d' command (usually).  It undeletes the
current message if the current message is deleted.  Otherwise it moves
backward to previous messages until a deleted message is found, and
undeletes that message.

   You can usually undo a `d' with a `u' because the `u' moves back to
and undeletes the message that the `d' deleted.  This does not work
when the `d' skips a few already-deleted messages that follow the
message being deleted; in that case the `u' command undeletes the last
of the messages that were skipped.  There is no clean way to avoid this
problem.  However, by repeating the `u' command, you can eventually get
back to the message you intended to undelete.  You can also reach that
message with `M-p' commands and then type `u'.

   A deleted message has the `deleted' attribute, and as a result
`deleted' appears in the mode line when the current message is deleted.
In fact, deleting or undeleting a message is nothing more than adding
or removing this attribute.  *Note Rmail Labels::.


File: rmail.info,  Node: Rmail Inbox,  Next: Rmail Files,  Prev: Rmail Deletion,  Up: Top

Rmail Files and Inboxes
***********************

   Unix places your incoming mail in a file called your "inbox".  When
you start up Rmail, it copies the new messages from your inbox into
your primary mail file, an Rmail file which also contains other messages
saved from previous Rmail sessions.  In this file, you actually read
the mail with Rmail.  The operation is called "getting new mail".  You
can repeat it at any time using the `g' key in Rmail.  The inbox file
name is `/usr/spool/mail/USERNAME' in Berkeley Unix,
`/usr/mail/USERNAME' in system V.

   There are two reason for having separate Rmail files and inboxes.

  1. The format in which Unix delivers the mail in the inbox is not
     adequate for Rmail mail storage.  It has no way to record
     attributes (such as `deleted') or user-specified labels; it has no
     way to record old headers and reformatted headers; it has no way
     to record cached summary line information.

  2. It is very cumbersome to access an inbox file without danger of
     losing mail, because it is necessary to interlock with mail
     delivery.  Moreover, different Unix systems use different
     interlocking techniques.  The strategy of moving mail out of the
     inbox once and for all into a separate Rmail file avoids the need
     for interlocking in all the rest of Rmail, since only Rmail
     operates on the Rmail file.

   When getting new mail, Rmail first copies the new mail from the inbox
file to the Rmail file and saves the Rmail file.  It then deletes the
inbox file.  This way a system crash may cause duplication of mail
between the inbox and the Rmail file, but it cannot lose mail.

   Copying mail from an inbox in the system's mailer directory actually
puts it in an intermediate file, `~/.newmail'.  This is because the
interlocking is done by a C program that copies to another file.
`~/.newmail' is deleted after mail merging is successful.  If there is
a crash at the wrong time, this file will continue to exist and will be
used as an inbox the next time you get new mail.


File: rmail.info,  Node: Rmail Files,  Next: Rmail Output,  Prev: Rmail Inbox,  Up: Top

Multiple Mail Files
*******************

   Rmail operates by default on your "primary mail file", which is
named `~/RMAIL' and which receives your incoming mail from your system
inbox file. You can also have other mail files and edit them with
Rmail.  These files can receive mail through their own inboxes, or you
can move messages into them by explicit command in Rmail (*note Rmail
Output::.).

`i FILE RET'
     Read FILE into Emacs and run Rmail on it (`rmail-input').

`M-x set-rmail-inbox-list RET FILES RET'
     Specify inbox file names for current Rmail file to get mail from.

`g'
     Merge new mail from current Rmail file's inboxes
     (`rmail-get-new-mail').

`C-u g FILE'
     Merge new mail from inbox file FILE.

   To run Rmail on a file other than your primary mail file, you may use
the `i' (`rmail-input') command in Rmail.  This visits the file, puts
it in Rmail mode, and then gets new mail from the file's inboxes if
any.  You can also use `M-x rmail-input' even when not in Rmail.

   The file you read with `i' does not have to be in Rmail file format.
It could also be Unix mail format, mmdf format, or it could be a mixture
of all three, as long as each message has one of the three formats.
Rmail recognizes all three and converts all the messages to proper Rmail
format before showing you the file.

   Each Rmail file can contain a list of inbox file names; you can
specify this list with `M-x set-rmail-inbox-list RET FILES RET'.  The
argument can contain any number of file names, separated by commas.  It
can also be empty, which specifies that this file should have no
inboxes.  Once a list of inboxes is specified, the Rmail file remembers
it permanently until it is explicitly changed.

   If an Rmail file has inboxes, new mail is merged in from the inboxes
when you bring the Rmail file into Rmail, and when you use the `g'
(`rmail-get-new-mail') command.  If the Rmail file specifies no
inboxes, then no new mail is merged in at these times.  A special
exception is made for your primary mail file: Rmail uses the standard
system inbox for it if it does not specify an inbox.

   To merge mail from a file that is not the usual inbox, give the `g'
key a numeric argument, as in `C-u g'.  Rmail prompts you for a file
name and merges mail from that file.  The inbox file is not deleted or
changed at all when you use `g' with an argument, so this is a general
way of merging one file of messages into another.


File: rmail.info,  Node: Rmail Output,  Next: Rmail Labels,  Prev: Rmail Files,  Up: Top

Copying Messages Out to Files
*****************************

`o FILE RET'
     Append a copy of the current message to the file FILE, writing it
     in Rmail file format (`rmail-output-to-rmail-file').

`C-o FILE RET'
     Append a copy of the current message to the file FILE, writing it
     in Unix mail file format (`rmail-output').

   If an Rmail file has no inboxes, use explicit `o' commands to write
Rmail files.

   `o' (`rmail-output-to-rmail-file') appends the current message in
Rmail format to the end of a specified file.  This is the best command
to use to move messages between Rmail files.  If you are currently
visiting the other Rmail file, copying is done into the other file's
Emacs buffer instead.  You should eventually save the buffer on disk.

   The `C-o' (`rmail-output') command in Rmail appends a copy of the
current message to a specified file, in Unix mail file format.  This is
useful for moving messages into files to be read by other mail
processors that do not understand Rmail format.

   Copying a message with `o' or `C-o' gives the original copy of the
message the `filed' attribute. `filed' appears in the mode line when
such a message is current.

   Normally you should use only `o' to output messages to other Rmail
files, never `C-o'.  But it is also safe if you always use `C-o', never
`o'.  When a file is visited in Rmail, the last message is checked, and
if it is in Unix format, the entire file is scanned and all Unix-format
messages are converted to Rmail format.  (The reason for checking the
last message is that scanning the file is slow and most Rmail files
have only Rmail format messages.)  If you use `C-o' consistently, the
last message is guaranteed to be in Unix format, so Rmail will convert
all messages properly.

   When you and other users want to append mail to the same file, you
probably always want to use `C-o' instead of `o'.  Other mail
processors may not know Rmail format but will know Unix format.

   In any case, always use `o' to add to an Rmail file that is being
visited in Rmail.  Adding messages with `C-o' to the actual disk file
will trigger a "simultaneous editing" warning when you ask to save the
Emacs buffer, and the messages will be lost if you do save.


File: rmail.info,  Node: Rmail Labels,  Next: Rmail Summary,  Prev: Rmail Output,  Up: Top

Labels
******

   Each message can have various "labels" assigned to it as a means of
classification.  A label has a name; different names mean different
labels.  Any given label is either present or absent on a particular
message.  A few label names have standard meanings and are given to
messages automatically by Rmail when appropriate; these special labels
are called "attributes".  All other labels are assigned by the user.

`a LABEL RET'
     Assign the label LABEL to the current message (`rmail-add-label').

`k LABEL RET'
     Remove the label LABEL from the current message
     (`rmail-kill-label').

`C-M-n LABELS RET'
     Move to the next message that has one of the labels LABELS
     (`rmail-next-labeled-message').

`C-M-p LABELS RET'
     Move to the previous message that has one of the labels LABELS
     (`rmail-previous-labeled-message').

`C-M-l LABELS RET'
     Make a summary of all messages containing any of the labels LABELS
     (`rmail-summary-by-labels').

Specifying an empty string for one these commands means to use the last
label specified for any of these commands.

   The `a' (`rmail-add-label') and `k' (`rmail-kill-label') commands
allow you to assign or remove any label on the current message.  If the
LABEL argument is empty, it means to assign or remove the label most
recently assigned or removed.

   Once you have given messages labels to classify them as you wish,
there are two ways to use the labels: in moving and in summaries.

   The command `C-M-n LABELS RET' (`rmail-next-labeled-message') moves
to the next message that has one of the labels LABELS.  LABELS is one
or more label names, separated by commas.  `C-M-p'
(`rmail-previous-labeled-message') is similar, but moves backwards to
previous messages.  A preceding numeric argument to either one serves
as a repeat count.

   The command `C-M-l LABELS RET' (`rmail-summary-by-labels') displays
a summary containing only the messages that have at least one of a
specified set of messages.  The argument LABELS is one or more label
names, separated by commas.  *Note Rmail Summary::, for information on
summaries.

   If the LABELS argument to `C-M-n', `C-M-p' or `C-M-l' is empty, it
means to use the last set of labels specified for any of these commands.

   Some labels such as `deleted' and `filed' have built-in meanings and
are assigned to or removed from messages automatically at appropriate
times; these labels are called "attributes".  Here is a list of Rmail
attributes:

`unseen'
     Means the message has never been current.  Assigned to messages
     when they come from an inbox file, and removed when a message is
     made current.

`deleted'
     Means the message is deleted.  Assigned by deletion commands and
     removed by undeletion commands (*note Rmail Deletion::.).

`filed'
     Means the message has been copied to some other file.  Assigned by
     the file output commands (*note Rmail Files::.).

`answered'
     Means you have mailed an answer to the message.  Assigned by the
     `r' command (`rmail-reply').  *Note Rmail Reply::.

`forwarded'
     Means you have forwarded the message to other users.  Assigned by
     the `f' command (`rmail-forward').  *Note Rmail Reply::.

`edited'
     Means you have edited the text of the message within Rmail.  *Note
     Rmail Editing::.

   All other labels are assigned or removed only by the user, and it is
up to the user to decide what they mean.


File: rmail.info,  Node: Rmail Summary,  Next: Rmail Reply,  Prev: Rmail Labels,  Up: Top

Summaries
*********

   A "summary" is a buffer Rmail creates and displays to give you an
overview of the mail in an Rmail file.  It contains one line per
message; each line shows the message number, the sender, the labels,
and the subject.  When you select the summary buffer, you can use a
number of commands to select messages by moving in the summary buffer,
or to delete or undelete messages.

   A summary buffer applies to a single Rmail file only; if you are
editing multiple Rmail files, they have separate summary buffers.  The
summary buffer name is generated by appending `-summary' to the Rmail
buffer's name.  Only one summary buffer is displayed at a time unless
you make several windows and select the summary buffers by hand.

* Menu:

* Rmail Make Summary::  Making various sorts of summaries.
* Rmail Summary Edit::  Manipulating messages from the summary.


File: rmail.info,  Node: Rmail Make Summary,  Next: Rmail Summary Edit,  Prev: Rmail Summary,  Up: Rmail Summary

Making Summaries
================

   Here are the commands to create a summary for the current Rmail file.
Summaries do not update automatically; to make an updated summary, you
must use one of the commands again.

`h'
`C-M-h'
     Summarize all messages (`rmail-summary').

`l LABELS RET'
`C-M-l LABELS RET'
     Summarize message that have one or more of the specified labels
     (`rmail-summary-by-labels').

`C-M-r RCPTS RET'
     Summarize messages that have one or more of the specified
     recipients (`rmail-summary-by-recipients').

   The `h' or `C-M-h' (`rmail-summary') command fills the summary
buffer for the current Rmail file with a summary of all the messages in
the file.  It then displays and selects the summary buffer in another
window.

   The `l' or `C-M-l LABELS RET' (`rmail-summary-by-labels') makes a
partial summary mentioning only the messages that have one or more of
the labels LABELS.  LABELS should contain label names separated by
commas.

   `C-M-r RCPTS RET' (`rmail-summary-by-recipients') makes a partial
summary mentioning only the messages that have one or more of the
recipients RCPTS.  RCPTS should contain mailing addresses separated by
commas.

   Note that there is only one summary buffer for any Rmail file;
making one kind of summary discards any previously made summary.


File: rmail.info,  Node: Rmail Summary Edit,  Prev: Rmail Make Summary,  Up: Rmail Summary

Editing in Summaries
====================

   Summary buffers are given the major mode Rmail Summary mode, which
provides the following special commands:

`j'
     Select the message described by the line that point is on
     (`rmail-summary-goto-msg').

`C-n'
     Move to next line and select its message in Rmail
     (`rmail-summary-next-all').

`C-p'
     Move to previous line and select its message (`rmail-summary-
     previous-all').

`n'
     Move to next line, skipping lines saying `deleted', and select its
     message (`rmail-summary-next-msg').

`p'
     Move to previous line, skipping lines saying `deleted', and select
     its message (`rmail-summary-previous-msg').

`d'
     Delete the current line's message, then do like `n'
     (`rmail-summary-delete-forward').

`u'
     Undelete and select this message or the previous deleted message in
     the summary (`rmail-summary-undelete').

`SPC'
     Scroll the other window (presumably Rmail) forward
     (`rmail-summary-scroll-msg-up').

`DEL'
     Scroll the other window backward (`rmail-summary-scroll-msg-down').

`x'
     Kill the summary window (`rmail-summary-exit').

`q'
     Exit Rmail (`rmail-summary-quit').

   The keys `C-n'(`rmail-summary-next-all') and `C-p'
(`rmail-summary-previous-all') are modified in Rmail Summary mode.  In
addition to moving point in the summary buffer, they also cause the
line's message to become current in the associated Rmail buffer.  That
buffer is also made visible in another window if it is not currently
visible.

   `n' and `p' are similar to `C-n' and `C-p', but skip lines that say
`message deleted'.  They are like the `n' and `p' keys of Rmail itself.
Note, however, that in a partial summary these commands move only
among the message listed in the summary.

   The other Emacs cursor motion commands are not changed in Rmail
Summary mode, so it is easy to get the point on a line whose message is
not selected in Rmail.  This can also happen if you switch to the Rmail
window and switch messages there.  To get the Rmail buffer back in sync
with the summary, use the `j' (`rmail-summary-goto-msg') command, which
selects the message of the current summary line in Rmail.

   Deletion and undeletion can also be done from the summary buffer.
They always work based on where point is located in the summary buffer,
ignoring which message is selected in Rmail.  `d'
(`rmail-summary-delete-forward') deletes the current line's message,
then moves to the next line whose message is not deleted and selects
that message.  The inverse is `u' (`rmail-summary-undelete'), which
moves back (if necessary) to a line whose message is deleted, undeletes
that message, and selects it in Rmail.

   When moving through messages with the summary buffer, it is
convenient to be able to scroll the message while remaining in the
summary window.  The commands SPC (`rmail-summary-scroll-msg-up') and
DEL (`rmail-summary-scroll-msg-down') do this.  They scroll the message
just as they do when the Rmail buffer is selected.

   When you are finished using the summary, type `x'
(`rmail-summary-exit') to kill the summary buffer's window.

   You can also exit Rmail while in the summary.  `q'
(`rmail-summary-quit') kills the summary window, then saves the Rmail
file and switches to another buffer.


File: rmail.info,  Node: Rmail Reply,  Next: Rmail Editing,  Prev: Rmail Summary,  Up: Top

Sending Replies
***************

   Rmail has several commands that use Mail mode to send mail.  Only the
special commands of Rmail for entering Mail mode are documented here.
Note that the usual keys for sending mail, `C-x m' and `C-x 4 m', are
available in Rmail mode and work just as they usually do.

`m'
     Send a message (`rmail-mail').

`c'
     Continue editing already started outgoing message
     (`rmail-continue').

`r'
     Send a reply to the current Rmail message (`rmail-reply').

`f'
     Forward current message to other users (`rmail-forward').

   To reply to a the message you are reading in Rmail, type `r'
(`rmail-reply').  This displays the `*mail*' buffer in another window,
much like `C-x 4 m', but pre-initializes the `Subject', `To', `CC', and
`In-reply-to' header fields based on the message you reply to.  The
`To' field is given the sender of that message, and the `CC' gets all
the recipients of that message.  Recipients that match elements of the
list `rmail-dont-reply-to' are omitted; by default, this list contains
your own mailing address.

   Once you have initialized the `*mail*' buffer this way, sending the
mail goes as usual.  You can edit the presupplied header fields if they
are not what you want.

   One additional Mail mode command is available when you invoke mail
from Rmail: `C-c C-y' (`mail-yank-original') inserts into the outgoing
message a copy of the current Rmail message.  Normally this is the
message you are replying to, but you can also switch to the Rmail
buffer, select a different message, switch back, and yank the new
current message.  Normally the yanked message is indented four spaces
and has most header fields deleted from it; an argument to `C-c C-y'
specifies the amount to indent.  `C-u C-c C-y' neither indents the
message nor deletes any header fields.

   Another frequent reason to send mail in Rmail is to forward the
current message to other users.  `f' (`rmail-forward') makes this easy
by preinitializing the `*mail*' buffer with the current message as the
text and a subject designating a forwarded message.  All you have to do
is fill in the recipients and send.

   You can use the `m' (`rmail-mail') command to start editing an
outgoing message that is not a reply.  It leaves the header fields
empty.  Its only difference from `C-x 4 m' is that it makes the Rmail
buffer accessible for `C-c y', just as `r' does.  Thus `m' can be used
to reply to or forward a message; it can do anything `r' or `f' can do.

   The `c' (`rmail-continue') command resumes editing the `*mail*'
buffer, to finish editing an outgoing message you were already
composing, or to alter a message you have sent.


File: rmail.info,  Node: Rmail Editing,  Next: Rmail Digest,  Prev: Rmail Reply,  Up: Top

Editing Within a Message
************************

   Rmail mode provides a few special commands for moving within and
editing the current message.  In addition, the usual Emacs commands are
available (except for a few, such as `C-M-n' and `C-M-h', that are
redefined by Rmail for other purposes).  However, the Rmail buffer is
normally read-only, and to alter it you must use the Rmail command `w'
described below.

`t'
     Toggle display of original headers (`rmail-toggle-headers').

`w'
     Edit current message (`rmail-edit-current-message').

   Rmail reformats the header of each message before displaying it.
Normally this involves deleting most header fields, on the grounds that
they are not interesting.  The variable `rmail-ignored-headers' should
contain a regexp that matches the header fields to discard in this way.
The original headers are saved permanently; to see what they look
like, use the `t' (`rmail-toggle-headers') command.  This discards the
reformatted headers of the current message and displays it with the
original headers.  Repeating `t' reformats the message again.
Selecting the message again also reformats.

   The Rmail buffer is normally read-only, and most of the characters
you would type to modify it (including most letters) are redefined as
Rmail commands.  This is usually not a problem since people rarely want
to change the text of a message.  When you do want to do this, type `w'
(`rmail-edit-current-message'), which changes from Rmail mode to Rmail
Edit mode, another major mode which is nearly the same as Text mode.
The mode line indicates this change.

   In Rmail Edit mode, letters insert themselves as usual and the Rmail
commands are not available.  When you are finished editing the message
and are ready to go back to Rmail, type `C-c C-c', which switches back
to Rmail mode.  To return to Rmail mode but cancel all the editing you
have done, type `C-c C-]'.

   Entering Rmail Edit mode calls the value of the variable
`text-mode-hook' with no arguments, if that value exists and is not
`nil'.  It then does the same with the variable `rmail-edit-mode-hook'
and finally adds the attribute `edited' to the message.


File: rmail.info,  Node: Rmail Digest,  Prev: Rmail Editing,  Up: Top

Digest Messages
***************

   A "digest message" is a message which exists to contain and carry
several other messages.  Digests are used on moderated mailing lists.
All messages that arrive for the list during a period of time, such as
one day, are put inside a single digest which is then sent to the
subscribers.  Transmitting the single digest uses much less computer
time than transmitting the individual messages even though the total
size is the same, because the per-message overhead in network mail
transmission is considerable.

   When you receive a digest message, the most convenient way to read
it is to "undigestify" it: to turn it back into many individual
messages.  You can then read and delete the individual messages as it
suits you.

   To undigestify a message, select it and then type `M-x
undigestify-rmail-message'.  This copies each submessage as a separate
Rmail message and inserts them all following the digest.  The digest
message itself is flagged as deleted.



Tag Table:
Node: Top358
Node: Rmail Scrolling3678
Node: Rmail Motion4640
Node: Rmail Deletion7817
Node: Rmail Inbox10566
Node: Rmail Files12719
Node: Rmail Output15263
Node: Rmail Labels17603
Node: Rmail Summary21140
Node: Rmail Make Summary22110
Node: Rmail Summary Edit23550
Node: Rmail Reply26959
Node: Rmail Editing29732
Node: Rmail Digest32002

End Tag Table

This is Info file ../info/mh-e.info, produced by Makeinfo-1.55 from the
input file mh-e.texi.

   This is Edition 1.1, last updated 26 April 1995, of `mh-e, The Emacs
Interface to MH', for mh-e, Version 5.0.1.

   Copyright 1995 Free Software Foundation, Inc.

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided also
that the section entitled "Copying" is included exactly as in the
original, and provided that the entire resulting derived work is
distributed under the terms of a permission notice identical to this
one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Free Software Foundation.


File: mh-e.info,  Node: Variable Index,  Next: Concept Index,  Prev: Function Index,  Up: Top

Variable Index
**************

* Menu:

* mail-citation-hook:                   Customizing Inserting Letter.
* mh-auto-folder-collect:               Customizing Organizing.
* mh-before-quit-hook:                  Customizing Finishing Up.
* mh-before-send-letter-hook:           Customizing Sending Message.
* mh-before-send-letter-hook, example:  Customizing Sending Message.
* mh-bury-show-buffer:                  Customizing Reading.
* mh-bury-show-buffer, example:         Customizing mh-e.
* mh-clean-message-header:              Customizing Viewing.
* mh-cmd-note:                          Customizing Scan Line Formats.
* mh-comp-formfile:                     Customizing Sending.
* mh-compose-letter-function:           Customizing Sending.
* mh-cur-scan-msg-regexp:               Customizing Scan Line Formats.
* mh-default-folder-for-message-function: Customizing Organizing.
* mh-default-folder-for-message-function, example: Customizing Organizing.
* mh-delete-msg-hook:                   Customizing Deleting.
* mh-delete-yanked-msg-window:          Customizing Inserting Letter.
* mh-deleted-msg-regexp:                Customizing Scan Line Formats.
* mh-do-not-confirm:                    Customizing Reading.
* mh-folder-mode-hook:                  Customizing Reading.
* mh-folder-mode-hook, example:         Customizing Reading.
* mh-forward-subject-format:            Customizing Forwarding.
* mh-good-msg-regexp:                   Customizing Scan Line Formats.
* mh-inc-folder-hook:                   Customizing Incorporating.
* mh-inc-folder-hook, example:          Customizing Incorporating.
* mh-inc-prog:                          Customizing Incorporating.
* mh-ins-buf-prefix:                    Customizing Inserting Letter.
* mh-ins-buf-prefix:                    Customizing Inserting Letter.
* mh-invisible-headers:                 Customizing Viewing.
* mh-letter-mode-hook:                  Customizing Sending.
* mh-lib:                               Customizing Reading.
* mh-lib, example:                      Getting Started.
* mh-lpr-command-format:                Customizing Printing.
* mh-lpr-command-format, example:       Customizing mh-e.
* mh-mhn-args:                          Customizing Sending MIME.
* mh-mime-content-types:                Customizing Editing MIME.
* mh-mime-content-types, example:       Customizing Editing MIME.
* mh-msg-number-regexp:                 Customizing Scan Line Formats.
* mh-msg-search-regexp:                 Customizing Scan Line Formats.
* mh-new-draft-cleaned-headers:         Customizing Old Drafts.
* mh-new-draft-cleaned-headers, example: Customizing Old Drafts.
* mh-note-copied:                       Customizing Scan Line Formats.
* mh-note-cur:                          Customizing Scan Line Formats.
* mh-note-deleted:                      Customizing Scan Line Formats.
* mh-note-dist:                         Customizing Scan Line Formats.
* mh-note-forw:                         Customizing Scan Line Formats.
* mh-note-printed:                      Customizing Scan Line Formats.
* mh-note-refiled:                      Customizing Scan Line Formats.
* mh-note-repl:                         Customizing Scan Line Formats.
* mh-note-seq:                          Customizing Scan Line Formats.
* mh-partial-folder-mode-line-annotation: Customizing Searching.
* mh-pick-mode-hook:                    Customizing Searching.
* mh-print-background:                  Customizing Printing.
* mh-progs:                             Customizing Scan Line Formats.
* mh-progs:                             Customizing Incorporating.
* mh-progs:                             Customizing Reading.
* mh-progs, example:                    Getting Started.
* mh-quit-hook:                         Customizing Finishing Up.
* mh-recenter-summary-p:                Customizing Moving Around.
* mh-recursive-folders:                 Customizing Organizing.
* mh-redist-full-contents:              Customizing Redistributing.
* mh-refile-msg-hook:                   Customizing Organizing.
* mh-refiled-msg-regexp:                Customizing Scan Line Formats.
* mh-repl-formfile:                     Customizing Sending.
* mh-reply-default-reply-to:            Customizing Replying.
* mh-scan-prog:                         Customizing Scan Line Formats.
* mh-scan-prog:                         Customizing Incorporating.
* mh-send-prog:                         Customizing Sending Message.
* mh-show-buffer-mode-line-buffer-id:   Customizing Viewing.
* mh-show-hook:                         Customizing Viewing.
* mh-show-hook, example:                Customizing Viewing.
* mh-show-mode-hook:                    Customizing Viewing.
* mh-show-mode-hook, example:           Customizing Viewing.
* mh-signature-file-name:               Customizing Signature.
* mh-sortm-args:                        Customizing Organizing.
* mh-store-default-directory:           Customizing Files and Pipes.
* mh-store-default-directory, example:  Customizing Files and Pipes.
* mh-summary-height:                    Customizing Reading.
* mh-user-path, example:                Customizing Organizing.
* mh-valid-scan-line:                   Customizing Scan Line Formats.
* mh-visible-headers:                   Customizing Viewing.
* mh-yank-from-start-of-msg:            Customizing Inserting Letter.
* mhl-formfile:                         Customizing Viewing.


File: mh-e.info,  Node: Concept Index,  Prev: Variable Index,  Up: Top

Concept Index
*************

* Menu:

* burst:                                Reading Digests.
* comp:                                 Customizing Sending.
* dist:                                 Customizing Redistributing.
* folder:                               Organizing.
* forw:                                 Forwarding.
* ftp:                                  Tar.
* ftp:                                  FTP.
* inc:                                  Customizing Incorporating.
* inc:                                  Reading Mail Tour.
* inc:                                  Customizing Scan Line Formats.
* install-mh:                           Getting Started.
* lpr:                                  Printing.
* lpr:                                  Customizing Printing.
* mark:                                 Sequences.
* mh-e: comparison between versions:    Changes to mh-e.
* mhl:                                  Replying.
* mhl:                                  Printing.
* mhl:                                  Customizing Viewing.
* mhn:                                  Editing MIME.
* mhn:                                  Sending MIME.
* mhn:                                  Reading MIME.
* pick:                                 Sequences.
* pick:                                 Searching.
* refile:                               Organizing.
* repl:                                 Customizing Sending.
* repl:                                 Replying.
* repl:                                 Customizing Sending.
* scan:                                 Reading Mail Tour.
* scan:                                 Customizing Scan Line Formats.
* scan:                                 Customizing Scan Line Formats.
* send:                                 Customizing Redistributing.
* send:                                 Customizing Sending Message.
* shar:                                 Files and Pipes.
* shar:                                 Customizing Files and Pipes.
* show:                                 Reading MIME.
* sortm:                                Customizing Organizing.
* tar:                                  Tar.
* uuencode:                             Customizing Files and Pipes.
* uuencode:                             Files and Pipes.
* whom:                                 Recipients.
* xmh, in mh-e history:                 From Jim Larus.
* .emacs:                               Incorporating.
* .emacs:                               Getting mh-e.
* .emacs:                               Customizing mh-e.
* .emacs:                               Getting Started.
* .emacs:                               Customizing Organizing.
* .emacs:                               Customizing Reading.
* .mh_profile:                          Customizing Organizing.
* .signature:                           Signature.
* .signature:                           Customizing Signature.
* components:                           Customizing Sending.
* components:                           Customizing Sending.
* draft:                                Old Drafts.
* mhl.reply:                            Replying.
* replcomps:                            Customizing Sending.
* bugs:                                 Bug Reports.
* checking recipients:                  Recipients.
* content description:                  Editing MIME.
* content types:                        Editing MIME.
* deleting:                             Customizing Deleting.
* deleting:                             Deleting.
* digests:                              Reading Digests.
* editing draft:                        Draft Editing.
* editing draft:                        Customizing Draft Editing.
* editing header:                       Header.
* Emacs:                                Preface.
* Emacs:                                Conventions.
* Emacs, Emacs Lisp manual:             Customizing mh-e.
* Emacs, file completion:               Conventions.
* Emacs, functions; describe-mode:      Using mh-e.
* Emacs, info:                          Customizing mh-e.
* Emacs, interrupting:                  Conventions.
* Emacs, mark:                          Inserting Letter.
* Emacs, mark:                          Conventions.
* Emacs, minibuffer:                    Conventions.
* Emacs, notification of new mail:      Incorporating.
* Emacs, online help:                   Customizing mh-e.
* Emacs, online help:                   Using mh-e.
* Emacs, packages, supercite:           Customizing Inserting Letter.
* Emacs, point:                         Conventions.
* Emacs, point:                         Inserting Letter.
* Emacs, prefix argument:               Conventions.
* Emacs, quitting:                      Conventions.
* Emacs, quitting:                      Leaving mh-e.
* Emacs, region:                        Conventions.
* Emacs, region:                        Inserting Letter.
* Emacs, setting variables:             Customizing mh-e.
* Emacs, terms and conventions:         Conventions.
* expunging refiles and deletes:        Finishing Up.
* FAQ:                                  MH FAQ.
* file completion:                      Conventions.
* files, .emacs:                        Getting Started.
* files, .emacs:                        Getting mh-e.
* files, .emacs:                        Customizing mh-e.
* files, .emacs:                        Customizing Organizing.
* files, .emacs:                        Incorporating.
* files, .emacs:                        Customizing Reading.
* files, .mh_profile:                   Customizing Organizing.
* files, .signature:                    Customizing Signature.
* files, .signature:                    Signature.
* files, components:                    Customizing Sending.
* files, components:                    Customizing Sending.
* files, draft:                         Old Drafts.
* files, mhl.reply:                     Replying.
* files, replcomps:                     Customizing Sending.
* files, MH-E-NEWS:                     Getting mh-e.
* folder:                               Processing Mail Tour.
* forwarding:                           Customizing Forwarding.
* forwarding:                           Forwarding.
* Gildea, Stephen:                      Bug Reports.
* Gildea, Stephen:                      From Stephen Gildea.
* history:                              Preface.
* history of mh-e:                      History.
* images:                               Other MIME Objects.
* incorporating:                        Customizing Incorporating.
* incorporating:                        Incorporating.
* info:                                 Customizing mh-e.
* inserting messages:                   Customizing Inserting Letter.
* inserting messages:                   Inserting Letter.
* inserting messages:                   Inserting Messages.
* inserting signature:                  Signature.
* inserting signature:                  Customizing Signature.
* interrupting:                         Conventions.
* junk mail:                            Moving Around.
* killing draft:                        Killing Draft.
* Larus, Jim:                           From Jim Larus.
* Mailer-Daemon:                        Old Drafts.
* mailing list:                         Mailing List.
* mark:                                 Inserting Letter.
* mark:                                 Conventions.
* MH commands, burst:                   Reading Digests.
* MH commands, comp:                    Customizing Sending.
* MH commands, dist:                    Customizing Redistributing.
* MH commands, folder:                  Organizing.
* MH commands, forw:                    Forwarding.
* MH commands, inc:                     Customizing Incorporating.
* MH commands, inc:                     Reading Mail Tour.
* MH commands, inc:                     Customizing Scan Line Formats.
* MH commands, install-mh:              Getting Started.
* MH commands, mark:                    Sequences.
* MH commands, mhl:                     Replying.
* MH commands, mhl:                     Printing.
* MH commands, mhl:                     Customizing Viewing.
* MH commands, mhn:                     Reading MIME.
* MH commands, mhn:                     Sending MIME.
* MH commands, mhn:                     Editing MIME.
* MH commands, pick:                    Sequences.
* MH commands, pick:                    Searching.
* MH commands, refile:                  Organizing.
* MH commands, repl:                    Customizing Sending.
* MH commands, repl:                    Replying.
* MH commands, repl:                    Customizing Sending.
* MH commands, scan:                    Reading Mail Tour.
* MH commands, scan:                    Customizing Scan Line Formats.
* MH commands, scan:                    Customizing Scan Line Formats.
* MH commands, send:                    Customizing Sending Message.
* MH commands, send:                    Customizing Redistributing.
* MH commands, show:                    Reading MIME.
* MH commands, sortm:                   Customizing Organizing.
* MH commands, whom:                    Recipients.
* MH FAQ:                               MH FAQ.
* MH profile components, sortm:         Customizing Organizing.
* MH-Folder mode:                       Sequences.
* MH-Folder mode:                       Searching.
* MH-Folder mode:                       Customizing Reading.
* MH-Folder mode:                       Processing Mail Tour.
* MH-Folder mode:                       Customizing Reading.
* MH-Folder mode:                       Reading Mail Tour.
* MH-Folder mode:                       Reading Mail.
* MH-Folder mode:                       Moving Around.
* MH-Folder mode:                       Moving Around.
* MH-Folder mode:                       Customizing Moving Around.
* MH-Folder mode:                       Sending Mail.
* MH-Folder mode:                       Customizing Reading.
* MH-Folder Show mode:                  Reading Digests.
* MH-Folder Show mode:                  Moving Around.
* MH-Letter mode:                       Sending Mail Tour.
* MH-Letter mode:                       Sending Mail Tour.
* MH-Letter mode:                       Sending Mail.
* MH-Letter mode:                       Replying.
* MH-Letter mode:                       Draft Editing.
* MH-Show mode:                         Customizing Viewing.
* MH-Show mode:                         Customizing Moving Around.
* MIME:                                 Customizing Editing MIME.
* MIME:                                 Editing MIME.
* MIME:                                 Reading MIME.
* MIME, ftp:                            Tar.
* MIME, ftp:                            FTP.
* MIME, tar:                            Tar.
* MIME, content description:            Editing MIME.
* MIME, content types:                  Editing MIME.
* MIME, images:                         Other MIME Objects.
* MIME, sound:                          Other MIME Objects.
* MIME, video:                          Other MIME Objects.
* minibuffer:                           Conventions.
* mode:                                 Sending Mail Tour.
* modes, MH-Folder:                     Reading Mail.
* modes, MH-Folder:                     Searching.
* modes, MH-Folder:                     Reading Mail Tour.
* modes, MH-Folder:                     Customizing Reading.
* modes, MH-Folder:                     Processing Mail Tour.
* modes, MH-Folder:                     Customizing Moving Around.
* modes, MH-Folder:                     Sequences.
* modes, MH-Folder:                     Customizing Reading.
* modes, MH-Folder:                     Sending Mail.
* modes, MH-Folder:                     Customizing Reading.
* modes, MH-Folder:                     Moving Around.
* modes, MH-Folder:                     Moving Around.
* modes, MH-Folder Show:                Reading Digests.
* modes, MH-Folder Show:                Moving Around.
* modes, MH-Letter:                     Sending Mail.
* modes, MH-Letter:                     Replying.
* modes, MH-Letter:                     Draft Editing.
* modes, MH-Letter:                     Sending Mail Tour.
* modes, MH-Letter:                     Sending Mail Tour.
* modes, MH-Show:                       Customizing Moving Around.
* modes, MH-Show:                       Customizing Viewing.
* moving between messages:              Customizing Moving Around.
* moving between messages:              Moving Around.
* multimedia mail:                      Reading MIME.
* multimedia mail:                      Editing MIME.
* multimedia mail:                      Customizing Editing MIME.
* new mail:                             Incorporating.
* news:                                 Getting mh-e.
* notification of new mail:             Incorporating.
* obtaining mh-e:                       Getting mh-e.
* online help:                          Customizing mh-e.
* online help:                          Using mh-e.
* point:                                Conventions.
* point:                                Inserting Letter.
* prefix argument:                      Conventions.
* printing:                             Customizing Printing.
* printing:                             Printing.
* processing mail:                      Processing Mail Tour.
* processing mail:                      Moving Mail.
* processing mail:                      Customizing Moving Mail.
* quitting:                             Conventions.
* quitting:                             Leaving mh-e.
* quitting:                             Customizing Finishing Up.
* re-editing drafts:                    Customizing Old Drafts.
* re-editing drafts:                    Old Drafts.
* reading mail:                         Reading Mail Tour.
* reading mail:                         Reading Mail.
* reading mail:                         Customizing Reading.
* redistributing:                       Customizing Redistributing.
* redistributing:                       Redistributing.
* region:                               Inserting Letter.
* region:                               Conventions.
* regular expressions:                  Customizing Old Drafts.
* Reid, Brian:                          From Brian Reid.
* replying:                             Replying.
* replying:                             Customizing Replying.
* searching:                            Customizing Searching.
* searching:                            Searching.
* sending mail:                         Sending Mail.
* sending mail:                         Sending Message.
* sending mail:                         Sending Mail Tour.
* sending mail:                         Customizing Sending Message.
* sending mail:                         Customizing Sending.
* sequences:                            Sequences.
* setting variables:                    Customizing mh-e.
* signature:                            Customizing Signature.
* signature:                            Signature.
* sound:                                Other MIME Objects.
* spell check:                          Customizing Sending Message.
* starting from command line:           Sending Mail.
* Unix commands, ftp:                   Tar.
* Unix commands, ftp:                   FTP.
* Unix commands, lpr:                   Printing.
* Unix commands, lpr:                   Customizing Printing.
* Unix commands, shar:                  Files and Pipes.
* Unix commands, shar:                  Customizing Files and Pipes.
* Unix commands, tar:                   Tar.
* Unix commands, uuencode:              Customizing Files and Pipes.
* Unix commands, uuencode:              Files and Pipes.
* Unix commands, Emacs:                 Conventions.
* Unix commands, Emacs:                 Preface.
* using files:                          Customizing Files and Pipes.
* using files:                          Files and Pipes.
* using folders:                        Organizing.
* using folders:                        Customizing Organizing.
* using pipes:                          Files and Pipes.
* using pipes:                          Customizing Files and Pipes.
* video:                                Other MIME Objects.
* ~:                                    Getting Started.



How to setup VM:

0) Look at the Makefile and change the values of EMACS_VERSION,
   EMACS, INFODIR, LISPDIR, and PIXMAPDIR.  If they are not right for your
   system, change them.

   Note that version 18 of Emacs is no longer supported.  VM may
   or may not work under v18.

1) Do one of these:
     `make'.
     `make vm.info' to build the Info online help document.
     `make all' to make everything.
   Ignore the compiler warnings.

2) Put all the .elc files into a Lisp directory that Emacs knows
   about.  (see load-path).

3) If you're using XEmacs 19.12 and you want toolbar support,
   make a directory called `vm' in the XEmacs `etc' directory.
   Copy the files in pixmaps directory into the directory you
   just created.  VM will look for the pixmap there by default.

   Alternately you can put the pixmap files in any directory you
   want or just leave them where they are.  Be sure to point the
   variable vm-toolbar-pixmap-directory at the direrctory where
   you put the files.

4) If you built the Info document, copy the file vm.info file into
   the Emacs' info directory under the name "vm".  Edit the "dir"
   file in that directory and add a menu entry for VM, if it's
   not in there already.

5) Put these lines in your .emacs file if they aren't there
   already:

   (autoload 'vm "vm" "Start VM on your primary inbox." t)
   (autoload 'vm-other-frame "vm" "Like `vm' but starts in another frame." t)
   (autoload 'vm-visit-folder "vm" "Start VM on an arbitrary folder." t)
   (autoload 'vm-visit-virtual-folder "vm" "Visit a VM virtual folder." t)
   (autoload 'vm-mode "vm" "Run VM major mode on a buffer" t)
   (autoload 'vm-mail "vm" "Send a mail message using VM." t)
   (autoload 'vm-submit-bug-report "vm" "Send a bug report about VM." t)

You're now ready to use VM.  C-h i should start up the Emacs Info
system and if you've installed the Info document properly you can
use the online documentation to teach yourself how to use VM.
(Note: the Info document is currently only completely accurate for
version 4 of VM.)

Please use M-x vm-submit-bug-report to report bugs.  The bug report
will be sent to bug-vm@uunet.uu.net and be gatewayed from there to
gnu.emacs.vm.bug.

The latest version of VM can usually be found at ftp.uu.net in
the networking/mail/vm directory.

;;!emacs
;;
;; FILE:         kprop-em19.el
;; SUMMARY:      Koutline text property handling under Emacs 19.
;; USAGE:        GNU Emacs Lisp Library
;; KEYWORDS:     outlines, wp
;;
;; AUTHOR:       Bob Weiner
;;
;; ORIG-DATE:    7/27/93
;; LAST-MOD:      7-Jul-95 at 14:04:32 by Bob Weiner
;;
;; This file is part of Hyperbole.
;; Available for use and distribution under the same terms as GNU Emacs.
;;
;; Copyright (C) 1993-1995, Free Software Foundation, Inc.
;; Developed with support from Motorola Inc.
;;
;; DESCRIPTION:  
;; DESCRIP-END.

;;; ************************************************************************
;;; Other required Elisp libraries
;;; ************************************************************************

(require 'hversion)

;;; ************************************************************************
;;; Public functions
;;; ************************************************************************

(fset 'kproperty:get 'get-text-property)

(defun kproperty:map (function property value)
  "Apply FUNCTION to each PROPERTY `eq' to VALUE in the current buffer."
  (let ((result)
	(start (point-min)))
    (while (setq start
		 (text-property-any start (point-max) property value))
      (setq result (cons (funcall function (text-properties-at start))
			 result)))
    (nreverse result)))

(fset 'kproperty:next-single-change 'next-single-property-change)

(fset 'kproperty:previous-single-change 'previous-single-property-change)

(fset 'kproperty:properties 'text-properties-at)

(fset 'kproperty:put 'put-text-property)
(defun kproperty:put (start end prop value &optional object)
  "Set one property of the text from START to END.
The third and fourth arguments PROP and VALUE specify the property to add.
The optional fifth argument, OBJECT, is the string or buffer containing the
text.

Text inserted before or after this region does not inherit PROP."
  (add-text-properties
   start end (list prop value 'rear-nonsticky (list prop)) object))

(fset 'kproperty:remove 'remove-text-properties)

(defun kproperty:set (property value)
  "Set PROPERTY of character at point to VALUE."
  (kproperty:put (point) (min (+ 2 (point)) (point-max)) property value))

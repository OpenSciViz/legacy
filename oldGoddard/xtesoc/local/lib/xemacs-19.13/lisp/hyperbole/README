#
# FILE:		README
# SUMMARY:      Intro information on Hyperbole.  
#
# AUTHOR:       Bob Weiner
# ORG:          Brown U.
#
# ORIG-DATE:    19-Oct-91 at 03:27:47
# LAST-MOD:     29-Aug-95 at 11:07:53 by Bob Weiner

The author's work on this project has been sponsored by Motorola Inc.
under the Distinguished Student-Employee program.

We hope you enjoy using and developing with Hyperbole.  Suggestions and bug
reports are welcome, as described later in this document.  Feel free to
mail or post news containing this file wherever it may be of use.


           Outline
================================
* Hyperbole Overview
* What's New?
* How to Obtain
* Installation / Configuration
* Quick Reference
* Mail Lists
* User Quotes
* Why was Hyperbole developed?
* Copyright
================================


* Hyperbole Overview

Hyperbole is an open, efficient, programmable information management and
hypertext system.  It is intended for everyday work on any UNIX platform
supported by GNU Emacs.  It works well with the versions of Emacs that
support multiple X or NEXTSTEP windows: GNU Emacs V19, XEmacs (formerly
called Lucid Emacs) and Epoch.  Hyperbole allows hypertext buttons to be
embedded within unstructured and structured files, mail messages and news
articles.  It offers intuitive mouse-based control of information display
within multiple windows.  It also provides point-and-click access to Info
manuals, ftp archives, Wide-Area Information Servers (WAIS), and the
World-Wide Web (WWW) hypertext system through encapsulations of software that
support these protocols.

Hyperbole consists of four parts:

   1.  an interactive information management interface, including a powerful
       rolodex, which anyone can use.  It is easy to pick up and use since it
       introduces only a few new mechanisms and provides user-level
       facilities through a menu interface, which you control from the
       keyboard or the mouse;

   2.  an outliner with multi-level autonumbering and permanent ids attached
       to each outline node for use as hypertext link anchors;

   3.  A set of hyper-button-action types that provides core hypertext
       and other behaviors.  Users can make simple changes to button
       types and those familiar with Emacs Lisp can quickly prototype
       and deliver new types;

   4.  a set of programming library classes for system developers who
       want to integrate Hyperbole with another user interface or as a
       back-end to a distinct system.  (All of Hyperbole is written in
       Emacs Lisp for ease of modification.  Although Hyperbole was initially
       designed as a prototype, it has been engineered for real-world usage
       and is well structured.)

A Hyperbole user works with buttons; he may create, modify,
move or delete buttons.  Each button performs a specific action, such as
linking to a file or executing a shell command.

There are three categories of Hyperbole buttons:

   1.  explicit buttons  - created by Hyperbole, accessible from within a
       single document; 

   2.  global buttons    - created by Hyperbole, accessible anywhere within a
       user's network of documents; 

   3.  implicit buttons  - buttons created and managed by other programs or
       embedded within the structure of a document, accessible from within a
       single document.  Hyperbole recognizes implicit buttons by contextual
       patterns given in their type specifications.

Hyperbole buttons may be clicked upon with a mouse to activate them or
to describe their actions.  Thus, a user can always check how a button
will act before activating it.  Buttons may also be activated from a
keyboard.  (In fact, virtually all Hyperbole operations, including menu
usage, may be performed from any standard character terminal interface, so
one need not be anchored to a workstation all day).

Hyperbole does not enforce any particular hypertext or information
management model, but instead allows you to organize your information in
large or small chunks as you see fit.    The Hyperbole outliner organizes
information hierarchies which may also contain links to external
information sources.

Some of Hyperbole's most important features include:

    Buttons may link to information or may execute procedures, such as
    starting or communicating with external programs;

    One simply drags between a button source location and a link destination
    to create or to modify a link button.  The same result can be achieved
    from the keyboard.

    Buttons may be embedded within electronic mail messages;

    Outlines allow rapid browsing, editing and movement of chunks of
    information organized into trees (hierarchies);

    Other hypertext and information retrieval systems may be
    encapsulated under a Hyperbole user interface (a number of samples
    are provided).

Typical Hyperbole applications include:

    personal information management
       Overlapping link paths provide a variety of views into an
       information space.

       A search facility locates buttons in context and permits quick
       selection.

    documentation browsing
       Embed cross-references in your favorite documentation format.

       Add a point-and-click interface to existing documentation.

       Link code and design documents.  Jump to the definition of an
       identifier from its use within code or its reference within
       documentation.

    brainstorming
       Capture ideas and then quickly reorganize them with the Hyperbole
       outliner.  Link to related ideas, eliminating the need to copy
       and paste information into a single place.

    help/training systems
       Create tutorials with embedded buttons that show students how
       things work while explaining the concepts, e.g. an introduction
       to UNIX commands.  This technique can be much more effective than
       descriptions alone.

    archive managers
       Supplement programs that manage archives from incoming
       information streams by having them add topic-based buttons that
       link to the archive holdings.  Users can then search and create
       their own links to archive entries.


* What's New in V3.19.03 through V3.19.07?

  (See "ChangeLog" for more complete details of changes.)

  ACTION AND ASSIST KEYS

    - Action clicks on Fortran variables now jump to their definitions if an
      appropriate TAGS table has been built for the Fortran source code.

  BUTTONS

    - New patch-msg implicit button type jumps to the source line associated
      with output lines from the 'patch' program that begin with "Hunk" or
      "Patching".

    - Grep-msg implicib button type now jumps to source lines associated with
      Perl5 error messages.

    - Local world-wide-web file references of the form, file://localhost/ are
      now treated by Hyperbole as implicit pathname buttons and displayed as
      any other local file reference.

    - Improved recognition of URLs.

  EMACS VERSIONS

    - Setting hproperty:but-emphasize-p under Emacs 19 now highlights
      explicit buttons when the mouse moves over them.  This feature already
      existed for InfoDock and XEmacs.

  KOUTLINER

    - Added new outliner menu commands for InfoDock, XEmacs and Emacs 19:
        Hide-Levels, Hide-Subtree, and Show-Subtree.
    
    - Added 'Below' (same as {C-x $}) and 'Kill' (same as {C-c C-k}) menu
      items to Outliner minibuffer menu.

    - Enabled Copy-Before-Cell and Copy-After-Cell menu items.

    - {C-x $} prompts you for an outline level and hides any cells in the
      outline below that level.

    - {C-M-h} hides a subtree, ignoring the root cell.
      {M-x kotl-mode:show-subtree RET} is its inverse.

    - {M-s} now centers a line in the outliner.  {M-S} centers a paragraph.

    - Fixed problem that could leave internal outliner data exposed when a
      new final cell was added to an outline.

  MISCELLANEOUS

    - "Hyperbole mail buttons accepted" messages now appear in your outgoing
      mail only if you have loaded the full Hyperbole system.

    - Fixed a few minor bugs.


* How to Obtain

Hyperbole is actually part of an integrated tool framework that we have
developed called InfoDock.  InfoDock provides a modern user interface on top
of Emacs, information management, and powerful software development tools,
all in one package.  Get it via anonymous ftp from host ftp.cs.uiuc.edu in
the /pub/xemacs/infodock directory.

Hyperbole is also available as a standalone package via anonymous ftp across
the Internet.  Do not send requests to have it mailed to you since it won't
be.  Instead have another party who has Internet access obtain it for the
both of you.

Here is how to obtain Hyperbole as a standalone package on the Internet:

Move to a directory below which you want the 'hyperbole' directory to
be created.  Unpacking the Hyperbole archive will create this
directory and place all of the files below it.

   cd <LOCAL-LISP-DIR>

Ftp to ftp.cs.uiuc.edu  (Internet Host ID = 128.174.252.1):

   prompt> ftp ftp.cs.uiuc.edu

Login as 'anonymous' with your own <user-id>@<site-name> as a password.
   
   Name (ftp.cs.uiuc.edu): anonymous
   331 Guest login ok, send EMAIL address (e.g. user@host.domain) as password.
   Password:
   230 Guest login ok, access restrictions apply.

Move to the Hyperbole directory:

   ftp> cd pub/xemacs/infodock

Set your transfer mode to binary:

   ftp> bin
   200 Type set to I.

Turn off prompting:

   ftp> prompt
   Interactive mode off.

Retrieve just the Hyperbole archive and any diff-based patches (there may not
be any patches):

   ftp> mget hyperbole*

Close the ftp connection:

   ftp> quit
   221 Goodbye.

Unpack the tar archive using the GNU version of the 'zcat' program:

   zcat h*tar.gz | tar xvf -
or
   gunzip h*tar.gz; tar xvf h*tar

Apply any patches you retrieved, also:

   cd hyperbole; patch < <patch-file>


* Installation / Configuration

Edit the lines near the top of "Makefile" that set the actual executable to
use to compile the Hyperbole Lisp files.  Then choose one of the Emacs
version settings and set the EMACS variable to it.  You may also have to set
SITE-PRELOADS; follow the instructions that precede the 'SITE-PRELOADS ='
line.  Make these changes now and save the Makefile.

The following instructions use the term <HYP-DIR> to refer to the directory
in which you placed Hyperbole, so substitute your own value.

To install Hyperbole for use with XEmacs or GNU Emacs V19, from a shell:

   cd <HYP-DIR>; make

All of the .elc compiled Lisp files are already built for V19, so this build
will finish very quickly.  If you really want to rebuild all of the .elc
files under Emacs V19, use:

   cd <HYP-DIR>; make all-elc

To produce the Postscript version of the Hyperbole manual:

   cd <HYP-DIR>; make ps

To install Hyperbole for use with GNU Emacs V18 or Epoch, you must first
arrange for the Lisp variable `hyperb:dir' to be set to the full directory
path where you have placed Hyperbole.  It must be set before Hyperbole is
loaded into an Emacs session, e.g. in an Emacs initialization file add:

   (setq hyperb:dir "<HYP-DIR>/")
   (load (expand-file-name "hyperbole" hyperb:dir))

 Then from a shell:

   cd <HYP-DIR>
   make all-elc-v18

This will produce a complete set of Emacs V18 .elc files.

----

(Note that the section on 'Initializing' in the Hyperbole Manual duplicates
the information given below on how to load Hyperbole for use.  That
section is more extensive, however.)

To set up so that all Emacs users have Hyperbole loaded for them, make the
following changes in "lisp/default.el".  Otherwise, each user will have to
add these lines to his own "~/.emacs" initialization file.

   To autoload Hyperbole so that it is only loaded when needed:

      (load "<HYP-DIR>/hversion")
      (load "<HYP-DIR>/hyperbole")

   Alternatively, if you want Hyperbole to always completely load upon
   startup, add the following lines instead: 

      (load "<HYP-DIR>/hversion")
      (load "<HYP-DIR>/hyperbole")
      (require 'hsite)

That's all there is to the installation.

----

The Hyperbole Manual is included in two forms:
    "man/hyperbole.info"   - online version
    "man/hyperbole.texi"   - source form

Once Hyperbole has been installed for use at your site, you can manually
invoke it from within GNU Emacs or Epoch by loading the "hyperbole.el" code
library file: {M-x load-lib RTN hyperbole RTN}.  {C-h h} or {M-x hyperbole
RTN} then brings up the Hyperbole main menu.  

To add pointers to the Info version of the Hyperbole manual within your Info
directory, follow these instructions.  If 'Info-directory-list' is bound as a
variable within your Emacs, you can simply set it so that <HYP-DIR> is an
element in the list.  Otherwise, from a shell, cd to the directory
given by your 'Info-directory' variable and execute the following command:
      (rm hyperbole.info*; cp <HYP-DIR>/man/hyperbole.info* .)

Then add an Info menu entry for the Hyperbole manual in your Info "dir" file:
(the '*' should be placed in the first column of the file):

    * Hyperbole: (hyperbole.info).
        GNU Emacs-based everyday information management system.
	Use {C-h h d d} for a demo.  Includes context-sensitive
        mouse and keyboard support, a powerful rolodex, an autonumbered
        outliner with hyperlink anchors for each outline cell, and extensible
        hypertext facilities including hyper-links in mail and news messages.


* Quick Reference

"MANIFEST" summarizes most of the files in the distribution.

See "DEMO" for a demonstration of standard Hyperbole button
capabilities.

Naming conventions:
  All Hyperbole-specific code files begin with an 'h'.
  Hyperbole user-interface files begin with 'hui-' or 'hmous'.
  Files that define implicit button types begin with 'hib'.
  Encapsulations of foreign systems begin with 'hsys-'.

Most of the standard Emacs user interface for Hyperbole is located in
"hui.el".  Most of the Hyperbole application programming interface can be
found in "hbut.el".  "hbdata.el" encapsulates the button attribute storage
handling presently implemented by Hyperbole.  "hmail.el" provides a basic
abstract interface for folding mail readers other than Rmail into Hyperbole.

See the "(hyperbole.info)Questions and Answers" appendix in the
Hyperbole manual for information on how to alter the default
context-sensitive Hyperbole key bindings.


* Mail Lists

There are several Hyperbole-related mail addresses.  Learn what each is
for before you contemplate mailing to any of them.


hyperbole-request@hub.ucsb.edu
hyperbole-announce-request@hub.ucsb.edu

   ALL mail concerning administration of the Hyperbole mailing lists should
   be sent to the appropriate one of these addresses.  That includes
   addition, change, or deletion requests.  Don't EVER consider sending such
   a request to a Hyperbole mail list or people will think you can't read.

   Use the following formats on your subject line to execute requests,
   where you substitute your own values for the <> delimited items.
 
     Subject: Subscribe '<' <user@domain> '>' (<your name>).
     Subject: Unsubscribe '<' <user@domain> '>'.

   To change your address, you must unsubscribe your old address in one
   message and then subscribe your new address in another message.

   For example:

     To: hyperbole-announce-request@hub.ucsb.edu
     Subject: Unsubscribe <joe@any.com>.

     To: hyperbole-announce-request@hub.ucsb.edu
     Subject: Subscribe <joe@any.com> (Joe Williams).


hyperbole@hub.ucsb.edu

   Mail list for discussion of all Hyperbole issues.  Bug reports and
   suggestions may also be sent here.

   Always use your Subject and/or Summary: lines to state the position
   that your message takes on the topic that it addresses, e.g. send
   "Subject: Basic bug in top-level Hyperbole menu." rather than
   "Subject: Hyperbole bug".  Statements end with periods, questions
   with question marks (typically), and high energy, high impact
   declarations with exclamation points.  This simple rule makes all
   e-mail communication much easier for recipients to handle
   appropriately.

   If you ask a question, your subject line should include the word
   'question' or 'query' or should end with a '?', e.g. "Subject: How
   can man page SEE ALSOs be made implicit buttons?"  A "Subject: Re:
   How can ..." then indicates an answer to the question.  Question
   messages should normally include your Hyperbole and Emacs version
   numbers and clearly explain your problem and surrounding issues.
   Otherwise, you will simply waste the time of those who may want to
   help you.  (Your top-level Hyperbole menu shows its version number
   and {M-x emacs-version RTN} gives the other.)

   If you ask questions, you should consider adding to the discussion by
   telling people the kinds of work you are doing or contemplating doing
   with Hyperbole.  In this way, the list will not be overwhelmed by
   messages that ask for, but provide no information.

hyperbole-announce@hub.ucsb.edu

   Those who don't want to participate in the discussion but want to
   hear about bug fixes and new releases of Hyperbole should subscribe
   to this list.  Anyone on the 'hyperbole' list is automatically on
   this one too, so there is no need to subscribe to this one in that
   case.  This list is for official fixes and announcements so don't send
   your own fixes here.  Send them to 'hyperbole' instead.


* User Quotes


  *** MAN I love Hyperbole!!!  Wow! ***

				    	-- Ken Olstad

-------

  I *love* koutlines.

	   				-- Bob Glickstein

-------

 "In general, Hyperbole is an embeddable, highly extensible hypertext
  tool.  As such, I find it very useful. As it stands now, Hyperbole is
  particularly helpful for organizing ill-structured or loosely coupled
  information, in part because there are few tools geared for this purpose.
  Hyperbole also possesses a lot of potentials in supporting a wider
  spectrum of structuredness, ranging from unstructured to highly
  structured environments, as well as structural changes over time.

  Major Uses:

  * Menu interface to our own Epoch-based collaborative support environment
    called CoReView: This interface brings together all top-level user
    commands into a single partitioned screen, and allows the end user to
    interact with the system using simple mouse-clicking instead of the
    meta-x key.

  * Gateway to internet resources: this includes links to major Internet
    archive sites of various types of information. Links are made at both
    directory and file levels.

  * Alternative directory organizer: The hierarchical nature of the Unix
    file system sometimes makes it difficult to find things quickly and
    easily using directory navigational tools such as dired. Hyperbole
    enables me to create various "profile" views of my directory tree, with
    entries in these views referring to files anywhere in the hierarchy.

  * Organizing and viewing online documentation: using Hyperbole along with
    Hyper-man and Info makes it truly easy to look up online documentation.
      
  * Other desktop organization tasks: including links to various mail
    folders, saved newsgroup conversation threads, online note-taker,
    emacs-command invocations, etc."

				    	-- Dadong Wan

-------

 "Hyperbole is the first hyper-link system I've run across that is
  actually part of the environment I use regularly, namely Emacs. The
  complete flexibility of the links is both impressive and expected -- the
  idea of making the link itself programmable is clever, and given that one
  assumes the full power of emacs.  Being able to send email with buttons
  in it is a very powerful capability.  Using ange-ftp mode, one can make
  file references "across the world" as easily as normal file references."

				        -- Mark Eichin

-------

   I just wanted to say how much I enjoy using the Hyperbole outliner.
   It is a great way to quickly construct very readable technical documents
   that I can pass around to others.   Thanks for the great work.  

				        -- Jeff Fried

-------

   The Hyperbole system provides a nice interface to exploring corners of
   Unix that I didn't know existed before.

					-- Craig Smith

-------


* Why was Hyperbole developed?

Hyperbole has been designed to aid in research aimed at Personalized
Information production/retrieval Environments (PIEs).  Hyperbole is a
backend manager to be used in prototyping an initial PIE.

An examination of many hypertext environments as background research did
not turn up any that seemed suitable for the research envisioned, mainly
due to the lack of rich, portable programmer and user environments.  We also
tired of trying to manage our own distributed information pools with standard
UNIX tools.  And so Hyperbole was conceived and raved about until it
got its name.


* Copyright

The following copyright applies to the Hyperbole system as a whole.

Copyright (C) 1989-1995, Free Software Foundation, Inc.

Available for use and distribution under the terms of the GNU Public License,
version 2 or higher.

Hyperbole is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

Hyperbole is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.


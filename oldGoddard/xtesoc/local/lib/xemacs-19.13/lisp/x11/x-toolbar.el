;; Toolbar support for X.
;; Copyright (C) 1994 Andy Piper <andyp@parallax.demon.co.uk>
;; Copyright (C) 1995 Board of Trustees, University of Illinois

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;; DO NOT attempt to dump this file.  It will cause the dump to die a
;; horrible death.

;; x-init-toolbar-from-resources is defined in term/x-win.el

;;
;; toolbar ispell variables and defuns
;;

(defun toolbar-ispell ()
  "Intelligently spell the region or buffer."
  (interactive)
  (if (region-active-p)
      (ispell-region (region-beginning) (region-end))
    (ispell-buffer)))

;;
;; toolbar mail variables and defuns
;;

(defvar toolbar-use-separate-mail-frame nil
  "If non-nil run mail in a separate frame.")

(defvar toolbar-mail-frame nil
  "The frame in which mail is displayed.")

(defvar toolbar-mail-command 'vm
  "The mail reader to run.")

(defun toolbar-mail ()
  "Run mail in a separate frame."
  (interactive)
  (if (not toolbar-use-separate-mail-frame)
      (funcall toolbar-mail-command)
    (if (or (not toolbar-mail-frame)
	    (not (frame-live-p toolbar-mail-frame)))
	(progn
	  (setq toolbar-mail-frame (make-frame))
	  (add-hook 'vm-quit-hook
		    '(lambda ()
		       (save-excursion
			 (if (frame-live-p toolbar-mail-frame)
			     (delete-frame toolbar-mail-frame)))))
	  (select-frame toolbar-mail-frame)
	  (raise-frame toolbar-mail-frame)
	  (funcall toolbar-mail-command)))
    (if (frame-iconified-p toolbar-mail-frame)
	(deiconify-frame toolbar-mail-frame))
    (select-frame toolbar-mail-frame)
    (raise-frame toolbar-mail-frame)))

;;
;; toolbar info variables and defuns
;;

(defvar toolbar-info-frame nil
  "The frame in which info is displayed.")

(defun toolbar-info ()
  "Run info in a separate frame."
  (interactive)
  (if (or (not toolbar-info-frame)
	  (not (frame-live-p toolbar-info-frame)))
      (progn
	(setq toolbar-info-frame (make-frame))
	(select-frame toolbar-info-frame)
	(raise-frame toolbar-info-frame)))
  (if (frame-iconified-p toolbar-info-frame)
      (deiconify-frame toolbar-info-frame))
  (select-frame toolbar-info-frame)
  (raise-frame toolbar-info-frame)
  (info))

;;
;; toolbar debug variables and defuns
;;

(defun toolbar-debug ()
  (interactive)
  (require 'gdbsrc)
  (call-interactively 'gdbsrc)
  )

(defvar compile-command)

(defun toolbar-compile ()
  "Run compile without having to touch the keyboard."
  (interactive)
  (require 'compile)
  (popup-dialog-box
   `(,(concat "Compile:\n        " compile-command)
     ["Compile" (compile compile-command) t]
     ["Edit command" compile t]
     nil
     ["Cancel" (message "Quit") t])))

;;
;; toolbar news variables and defuns
;;

(defvar toolbar-news-frame nil
  "The frame in which news is displayed.")

(defun toolbar-news ()
  "Run GNUS in a separate frame."
  (interactive)
  (if (or (not toolbar-news-frame)
	  (not (frame-live-p toolbar-news-frame)))
      (progn
	(setq toolbar-news-frame (make-frame))
	(add-hook 'gnus-exit-gnus-hook
		  '(lambda ()
		     (if (frame-live-p toolbar-news-frame)
			 (delete-frame toolbar-news-frame))))
	(select-frame toolbar-news-frame)
	(raise-frame toolbar-news-frame)
	(gnus)))
  (if (frame-iconified-p toolbar-news-frame)
      (deiconify-frame toolbar-news-frame))
  (select-frame toolbar-news-frame)
  (raise-frame toolbar-news-frame))

(defvar toolbar-file-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "file-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "file-xx.xpm" toolbar-icon-directory)
       (expand-file-name "file-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "file-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "file-up.xbm" toolbar-icon-directory)
     (expand-file-name "file-dn.xbm" toolbar-icon-directory)
     (expand-file-name "file-xx.xbm" toolbar-icon-directory)))
  "A file icon pair.")

(defvar toolbar-folder-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "folder-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "folder-xx.xpm" toolbar-icon-directory)
       (expand-file-name "folder-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "folder-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "folder-up.xbm" toolbar-icon-directory)
     (expand-file-name "folder-dn.xbm" toolbar-icon-directory)
     (expand-file-name "folder-xx.xbm" toolbar-icon-directory)))
  "A folder icon pair")

(defvar toolbar-disk-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "disk-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "disk-xx.xpm" toolbar-icon-directory)
       (expand-file-name "disk-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "disk-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "disk-up.xbm" toolbar-icon-directory)
     (expand-file-name "disk-dn.xbm" toolbar-icon-directory)
     (expand-file-name "disk-xx.xbm" toolbar-icon-directory)))
  "A disk icon pair.")

(defvar toolbar-printer-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "printer-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "printer-xx.xpm" toolbar-icon-directory)
       (expand-file-name "printer-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "printer-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "printer-up.xbm" toolbar-icon-directory)
     (expand-file-name "printer-dn.xbm" toolbar-icon-directory)
     (expand-file-name "printer-xx.xbm" toolbar-icon-directory)))
  "A printer icon pair.")

(defvar toolbar-cut-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "cut-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "cut-xx.xpm" toolbar-icon-directory)
       (expand-file-name "cut-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "cut-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "cut-up.xbm" toolbar-icon-directory)
     (expand-file-name "cut-dn.xbm" toolbar-icon-directory)
     (expand-file-name "cut-xx.xbm" toolbar-icon-directory)))
  "A cut icon pair.")

(defvar toolbar-copy-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "copy-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "copy-xx.xpm" toolbar-icon-directory)
       (expand-file-name "copy-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "copy-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "copy-up.xbm" toolbar-icon-directory)
     (expand-file-name "copy-dn.xbm" toolbar-icon-directory)
     (expand-file-name "copy-xx.xbm" toolbar-icon-directory)))
  "A copy icon pair.")

(defvar toolbar-paste-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "paste-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "paste-xx.xpm" toolbar-icon-directory)
       (expand-file-name "paste-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "paste-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "paste-up.xbm" toolbar-icon-directory)
     (expand-file-name "paste-dn.xbm" toolbar-icon-directory)
     (expand-file-name "paste-xx.xbm" toolbar-icon-directory)))
  "A paste icon pair.")

(defvar toolbar-undo-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "undo-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "undo-xx.xpm" toolbar-icon-directory)
       (expand-file-name "undo-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "undo-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "undo-up.xbm" toolbar-icon-directory)
     (expand-file-name "undo-dn.xbm" toolbar-icon-directory)
     (expand-file-name "undo-xx.xbm" toolbar-icon-directory)))
  "An undo icon pair.")

(defvar toolbar-spell-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "spell-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "spell-xx.xpm" toolbar-icon-directory)
       (expand-file-name "spell-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "spell-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "spell-up.xbm" toolbar-icon-directory)
     (expand-file-name "spell-dn.xbm" toolbar-icon-directory)
     (expand-file-name "spell-xx.xbm" toolbar-icon-directory)))
  "A spell icon pair.")

(defvar toolbar-replace-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "replace-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "replace-xx.xpm" toolbar-icon-directory)
       (expand-file-name "replace-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "replace-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "replace-up.xbm" toolbar-icon-directory)
     (expand-file-name "replace-dn.xbm" toolbar-icon-directory)
     (expand-file-name "replace-xx.xbm" toolbar-icon-directory)))
  "A replace icon pair.")

(defvar toolbar-mail-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "mail-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "mail-xx.xpm" toolbar-icon-directory)
       (expand-file-name "mail-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "mail-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "mail-up.xbm" toolbar-icon-directory)
     (expand-file-name "mail-dn.xbm" toolbar-icon-directory)
     (expand-file-name "mail-xx.xbm" toolbar-icon-directory)))
  "A mail icon pair.")

(defvar toolbar-info-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "info-def-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "info-def-xx.xpm" toolbar-icon-directory)
       (expand-file-name "info-def-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "info-def-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "info-def-up.xbm" toolbar-icon-directory)
     (expand-file-name "info-def-dn.xbm" toolbar-icon-directory)
     (expand-file-name "info-def-xx.xbm" toolbar-icon-directory)))
  "An info icon pair.")

(defvar toolbar-compile-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "compile-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "compile-xx.xpm" toolbar-icon-directory)
       (expand-file-name "compile-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "compile-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "compile-up.xbm" toolbar-icon-directory)
     (expand-file-name "compile-dn.xbm" toolbar-icon-directory)
     (expand-file-name "compile-xx.xbm" toolbar-icon-directory)))
  "A compile icon.")

(defvar toolbar-debug-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "debug-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "debug-xx.xpm" toolbar-icon-directory)
       (expand-file-name "debug-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "debug-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "debug-up.xbm" toolbar-icon-directory)
     (expand-file-name "debug-dn.xbm" toolbar-icon-directory)
     (expand-file-name "debug-xx.xbm" toolbar-icon-directory)))
  "A debugger icon.")

(defvar toolbar-news-icon
  (if (featurep 'xpm)
      (toolbar-make-button-list
       (expand-file-name "news-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "news-xx.xpm" toolbar-icon-directory)
       (expand-file-name "news-cap-up.xpm" toolbar-icon-directory)
       nil
       (expand-file-name "news-cap-xx.xpm" toolbar-icon-directory))
    (toolbar-make-button-list
     (expand-file-name "news-up.xbm" toolbar-icon-directory)
     (expand-file-name "news-dn.xbm" toolbar-icon-directory)
     (expand-file-name "news-xx.xbm" toolbar-icon-directory)))
   "A news icons.")

(defvar initial-toolbar-spec
  '([toolbar-file-icon		find-file	t	"Open a file"	]
    [toolbar-folder-icon	dired		t	"View directory"]
    [toolbar-disk-icon		save-buffer	t	"Save buffer"	]
    [toolbar-printer-icon	lpr-buffer	t	"Print buffer"	]
    [toolbar-cut-icon		x-kill-primary-selection t "Kill region"]
    [toolbar-copy-icon		x-copy-primary-selection t "Copy region"]
    [toolbar-paste-icon		x-yank-clipboard-selection t
				"Paste from clipboard"]
    [toolbar-undo-icon		undo		t	"Undo edit"	]
    [toolbar-spell-icon		toolbar-ispell	t	"Spellcheck"	]
    [toolbar-replace-icon	query-replace	t	"Replace text"	]
    [toolbar-mail-icon		toolbar-mail	t	"Mail"		]
    [toolbar-info-icon		toolbar-info	t	"Information"	]
    [toolbar-compile-icon	toolbar-compile		t	"Compile"	]
    [toolbar-debug-icon		toolbar-debug	t	"Debug"		]
    [toolbar-news-icon		toolbar-news	t	"News"		])
  "The initial toolbar for a buffer.")

(set-specifier default-toolbar initial-toolbar-spec)


(provide 'x-toolbar)

**** EDIFF -- a comprehensive interface to diff for Emacs 19 and XEmacs 19

**** This version of Ediff requires *at least* Emacs 19.29 or XEmacs 19.12

This directory:

README	    	-- This file
Makefile	-- Automated installation file
ediff.texi	-- Manual in Texinfo format
ediff.info*	-- The Info files
ediff.dvi	-- Manual in DVI format

ediff.el    	-- Ediff Emacs Lisp code
ediff-init.el	-- Ediff Emacs Lisp code
ediff-wind.el	-- Ediff Emacs Lisp code
ediff-util.el	-- Ediff Emacs Lisp code
ediff-diff.el	-- Ediff Emacs Lisp code
ediff-merg.el	-- Ediff Emacs Lisp code
ediff-hook.el	-- Ediff Emacs Lisp code


To install Ediff do:

		uncompress ediff.shar.Z
		tar xf ediff.shar


Then do the following:

1. To compile Lisp:
		
      	    	make  (or 'make all')
   
2. You can also type

	       make install
	       
   to do what "make all" does plus it will copy the files ediff*.elc
   into a suitable Lisp directory.
   
   To make this happen, you will most likely have to change the PREFIX
   variable in Makefile and, possibly, one or more of these variables:
   
   	DATADIR, LISPDIR, INFODIR, ETCDIR, and VERSION
	
   if they don't point to the right directories in your installation.
   You also need to have a write permission for all directories
   mentioned in LISPDIR, INFODIR, and ETCDIR.
   
3. XEmacs users must invoke `make' with the parameter EMACS=xemacs
   or whatever name is used to invoke XEmacs (some sites still use xemacs
   for Emacs 18). An even better thing would be to edit Makefile directly
   as indicated in the comments there.

For manual installation, copy ediff*.elc into a directory on your load-path.

For more details, read documentation at the beginning of the file ediff.el

To install on-line documentation, you need to install the Info files
by copying 

ediff.info*

into your Info directory (which is emacs-root-dir/info,
if emacs-root-dir is the root directory of the installation).

Then edit the file 

emacs-root-dir/info/dir

to include the root menu item for Ediff (check how other menu
items look like in this file). 

In Emacs, this item should look like this:

*  Ediff: (ediff.info).  A Visual Interface to Unix Diff and Patch Utilities

In XEmacs, it looks like:

*  Ediff::        A Visual Interface to Unix Diff and Patch Utilities


Normally, all Ediff menus and autoloads are already defined in Emacs, so u
don't need to define anything in your .emacs to run Ediff.
However, if it was announced that this distribution of Ediff contains
new features, you may have to put 

(require 'ediff-hook)

in your .emacs to take advantage of these new features. This doesn't load
Ediff, but readies it for any taks u assign to it. When this version of
Ediff gets installed in the standard Emacs distribution, you may remove
the above require-statement (but leaving it in does no harm).

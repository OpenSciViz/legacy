;;!emacs
;;
;; LCD-ENTRY:    oobr|Bob Weiner|oo-browser@hub.ucsb.edu|Multi-Language Object-oriented Browser|29-Aug-95|2.9.8|ftp.cs.uiuc.edu:/pub/xemacs/infodock/
;;
;; FILE:         br-vers.el
;; SUMMARY:      OO-Browser revision number as 'br-version' variable.
;; USAGE:        GNU Emacs Lisp Library
;; KEYWORDS:     oop, tools
;;
;; AUTHOR:       Bob Weiner
;; ORG:          Motorola, Inc.
;;
;; ORIG-DATE:    16-Mar-95 at 12:31:05
;; LAST-MOD:     29-Aug-95 at 10:41:00 by Bob Weiner
;;
;; Copyright (C) 1995, Free Software Foundation, Inc.
;; See the file BR-COPY for license information.
;;
;; This file is part of the OO-Browser.
;;
;; DESCRIPTION:  
;; DESCRIP-END.

;;; ************************************************************************
;;; Public variables
;;; ************************************************************************

(defconst br-version "02.09.08" "The OO-Browser revision number.")


;;; obsolete.el --- obsoleteness support.

;;;; Copyright (C) 1985-1994 Free Software Foundation, Inc.
;;;; Copyright (C) 1994, 1995 Amdahl Corporation.
;;;; Copyright (C) 1995 Sun Microsystems.

;; Maintainer: XEmacs
;; Keywords: internal

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; The obsoleteness support used to be scattered throughout various
;;; source files.  We put the stuff in one place to remove the junkiness
;;; from other source files and to facilitate creating/updating things
;;; like sysdep.el.

(defsubst define-obsolete-function-alias (oldfun newfun)
  "Define OLDFUN as an obsolete alias for function NEWFUN.
This makes calling OLDFUN equivalent to calling NEWFUN and marks OLDFUN
as obsolete."
  (define-function oldfun newfun)
  (make-obsolete oldfun newfun))

(defsubst define-obsolete-variable-alias (oldvar newvar)
  "Define OLDVAR as an obsolete alias for varction NEWVAR.
This makes referencing or setting OLDVAR equivalent to referencing or
setting NEWVAR and marks OLDVAR as obsolete."
  (defvaralias oldvar newvar)
  (make-obsolete-variable oldvar newvar))

;;;;;;;;;;;;;;;;;;;;;;;;;;;; `point'

(define-obsolete-function-alias 'dot 'point)
(define-obsolete-function-alias 'dot-marker 'point-marker)
(define-obsolete-function-alias 'dot-min 'point-min)
(define-obsolete-function-alias 'dot-max 'point-max)
(define-obsolete-function-alias 'window-dot 'window-point)
(define-obsolete-function-alias 'set-window-dot 'set-window-point)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; extents

(defun extent-data (extent)
  "Obsolete.  Returns the `data' property of the given extent."
  (extent-property extent 'data))
(make-obsolete 'set-window-dot 'set-window-point)

(defun set-extent-data (extent data)
  "Obsolete.  Sets the `data' property of the given extent."
  (set-extent-property extent 'data data))
(make-obsolete 'set-extent-data 'set-extent-property)

(defun set-extent-attribute (extent attr &optional clearp)
  "" ;; obsoleteness info will be displayed, so no need for anything more.
  (cond ((eq attr 'write-protected)
         (set-extent-property extent 'read-only t))
        ((eq attr 'unhighlight)
         (set-extent-property extent 'highlight nil))
        ((eq attr 'writable)
         (set-extent-property extent 'read-only nil))
        ((eq attr 'visible)
         (set-extent-property extent 'invisible nil))
        (t
         (set-extent-property extent attr t))))
(make-obsolete 'set-extent-attribute 'set-extent-property)

(defun extent-glyph (extent)
  "" ;; obsoleteness info will be displayed, so no need for anything more.
  (or (extent-begin-glyph extent)
      (extent-end-glyph extent)))
(make-obsolete 'extent-glyph
	       "use `extent-begin-glyph' or `extent-end-glyph' instead.")

(defun extent-layout (extent)
  "" ;; obsoleteness info will be displayed, so no need for anything more.
  (extent-begin-glyph-layout extent))
(make-obsolete 'extent-layout
       "use `extent-begin-glyph-layout' or `extent-end-glyph-layout' instead.")

(defun set-extent-layout (extent layout)
  "" ;; obsoleteness info will be displayed, so no need for anything more.
  (set-extent-begin-glyph-layout extent layout))
(make-obsolete 'set-extent-layout
       "use `set-extent-begin-glyph-layout' or `set-extent-end-glyph-layout' instead.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;; frames

(define-obsolete-variable-alias 'select-screen-hook 'select-frame-hook)
(define-obsolete-variable-alias 'deselect-screen-hook 'deselect-frame-hook)
(define-obsolete-variable-alias 'auto-raise-screen 'auto-raise-frame)
(define-obsolete-variable-alias 'auto-lower-screen 'auto-lower-frame)
(define-obsolete-variable-alias 'get-screen-for-buffer-default-screen-name
  'get-frame-for-buffer-default-frame-name)

(define-obsolete-function-alias 'buffer-dedicated-screen
  'buffer-dedicated-frame)
(define-obsolete-function-alias 'deiconify-screen 'deiconify-frame)
(define-obsolete-function-alias 'delete-screen 'delete-frame)
(define-obsolete-function-alias 'event-screen 'event-frame)
(define-obsolete-function-alias 'find-file-other-screen 'find-file-other-frame)
(define-obsolete-function-alias 'find-file-read-only-other-screen
  'find-file-read-only-other-frame)
(define-obsolete-function-alias 'live-screen-p 'frame-live-p)
(define-obsolete-function-alias 'screen-height 'frame-height)
(define-obsolete-function-alias 'screen-iconified-p 'frame-iconified-p)
(define-obsolete-function-alias 'screen-list 'frame-list)
(define-obsolete-function-alias 'screen-live-p 'frame-live-p)
(define-obsolete-function-alias 'screen-name 'frame-name)
(define-obsolete-function-alias 'screen-parameters 'frame-parameters)
(define-obsolete-function-alias 'screen-pixel-height 'frame-pixel-height)
(define-obsolete-function-alias 'screen-pixel-width 'frame-pixel-width)
(define-obsolete-function-alias 'screen-root-window 'frame-root-window)
(define-obsolete-function-alias 'screen-selected-window 'frame-selected-window)
(define-obsolete-function-alias 'screen-totally-visible-p
  'frame-totally-visible-p)
(define-obsolete-function-alias 'screen-visible-p 'frame-visible-p)
(define-obsolete-function-alias 'screen-width 'frame-width)
(define-obsolete-function-alias 'screenp 'framep)
(define-obsolete-function-alias 'get-screen-for-buffer 'get-frame-for-buffer)
(define-obsolete-function-alias 'get-screen-for-buffer-noselect
  'get-frame-for-buffer-noselect)
(define-obsolete-function-alias 'get-other-screen 'get-other-frame)
(define-obsolete-function-alias 'iconify-screen 'iconify-frame)
(define-obsolete-function-alias 'lower-screen 'lower-frame)
(define-obsolete-function-alias 'mail-other-screen 'mail-other-frame)
(define-obsolete-function-alias 'make-screen 'make-frame)
(define-obsolete-function-alias 'make-screen-invisible 'make-frame-invisible)
(define-obsolete-function-alias 'make-screen-visible 'make-frame-visible)
(define-obsolete-function-alias 'modify-screen-parameters
  'modify-frame-parameters)
(define-obsolete-function-alias 'new-screen 'new-frame)
(define-obsolete-function-alias 'next-screen 'next-frame)
(define-obsolete-function-alias 'next-multiscreen-window
  'next-multiframe-window)
(define-obsolete-function-alias 'other-screen 'other-frame)
(define-obsolete-function-alias 'previous-screen 'previous-frame)
(define-obsolete-function-alias 'previous-multiscreen-window
  'previous-multiframe-window)
(define-obsolete-function-alias 'raise-screen 'raise-frame)
(define-obsolete-function-alias 'redraw-screen 'redraw-frame)
(define-obsolete-function-alias 'select-screen 'select-frame)
(define-obsolete-function-alias 'selected-screen 'selected-frame)
(define-obsolete-function-alias 'set-buffer-dedicated-screen
  'set-buffer-dedicated-frame)
(define-obsolete-function-alias 'set-screen-height 'set-frame-height)
(define-obsolete-function-alias 'set-screen-position 'set-frame-position)
(define-obsolete-function-alias 'set-screen-size 'set-frame-size)
(define-obsolete-function-alias 'set-screen-width 'set-frame-width)
(define-obsolete-function-alias 'show-temp-buffer-in-current-screen
  'show-temp-buffer-in-current-frame)
(define-obsolete-function-alias 'switch-to-buffer-other-screen
  'switch-to-buffer-other-frame)
(define-obsolete-function-alias 'visible-screen-list 'visible-frame-list)
(define-obsolete-function-alias 'window-screen 'window-frame)
(define-obsolete-function-alias 'x-set-screen-icon-pixmap
  'x-set-frame-icon-pixmap)
(define-obsolete-function-alias 'x-set-screen-pointer
  'x-set-frame-pointer)

(define-obsolete-variable-alias 'screen-title-format 'frame-title-format)
(define-obsolete-variable-alias 'screen-icon-title-format
  'frame-icon-title-format)
(define-obsolete-variable-alias 'terminal-screen 'terminal-frame)
(define-obsolete-variable-alias 'delete-screen-hook 'delete-frame-hook)
(define-obsolete-variable-alias 'create-screen-hook 'create-frame-hook)
(define-obsolete-variable-alias 'mouse-enter-screen-hook
  'mouse-enter-frame-hook)
(define-obsolete-variable-alias 'mouse-leave-screen-hook
  'mouse-leave-frame-hook)
(define-obsolete-variable-alias 'map-screen-hook 'map-frame-hook)
(define-obsolete-variable-alias 'unmap-screen-hook 'unmap-frame-hook)
(define-obsolete-variable-alias 'default-screen-alist 'default-frame-alist)
(define-obsolete-variable-alias 'default-screen-name 'default-frame-name)
(define-obsolete-variable-alias 'x-screen-defaults 'default-x-frame-alist)

(defun x-create-screen (parms window-id)
  ""
  (if (not (eq 'x (device-type (selected-device))))
      (error "Cannot create X frames on non-X device"))
  (make-frame (append parms (list (list 'window-id window-id)))
              (selected-device)))
(make-obsolete 'x-create-screen 'make-frame)

(defun frame-first-window (frame)
  "Returns the topmost, leftmost window of FRAME.
If omitted, FRAME defaults to the currently selected frame."
  (frame-highest-window frame 0))
(make-obsolete 'frame-first-window 'frame-highest-window)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; stuff replaced by specifiers

(defun screen-scrollbar-width (&optional screen)
  ;; specifier-specs is the inverse of set-specifier, but
  ;; the way this function was defined, specifier-instance
  ;; is closer.
  (specifier-instance scrollbar-width (or screen (selected-frame))))
(make-obsolete 'screen-scrollbar-width
	       "use (specifier-instance scrollbar-width ...).")

(defun set-screen-scrollbar-width (screen value)
  (set-specifier scrollbar-width (cons screen value)))
(make-obsolete 'set-screen-scrollbar-width
	       "use (set-specifier scrollbar-width ...).")

(defun set-screen-left-margin-width (value &optional screen)
  (set-specifier left-margin-width
		 (cons (or screen (selected-frame)) value)))
(make-obsolete 'set-screen-left-margin-width
	       "use (set-specifier left-margin-width ...).")

(defun set-screen-right-margin-width (value &optional screen)
  (set-specifier right-margin-width
		 (cons (or screen (selected-frame)) value)))
(make-obsolete 'set-screen-right-margin-width
	       "use (set-specifier right-margin-width ...).")

(defun set-buffer-left-margin-width (value &optional buffer)
  (set-specifier left-margin-width (cons (or buffer (current-buffer)) value)))
(make-obsolete 'set-buffer-left-margin-width
	       "use (set-specifier left-margin-width ...).")

(defun set-buffer-right-margin-width (value &optional buffer)
  (set-specifier right-margin-width (cons (or buffer (current-buffer)) value)))
(make-obsolete 'set-buffer-right-margin-width
	       "use (set-specifier right-margin-width ...).")

(defun screen-left-margin-width (&optional screen)
  (specifier-specs left-margin-width (or screen (selected-frame))))
(make-obsolete 'screen-left-margin-width
	       "use (specifier-specs left-margin-width ...).")

(defun screen-right-margin-width (&optional screen)
  (specifier-specs right-margin-width (or screen (selected-frame))))
(make-obsolete 'screen-right-margin-width
	       "use (specifier-specs right-margin-width ...).")

(defun buffer-left-margin-width (&optional buffer)
  (specifier-specs left-margin-width (or buffer (current-buffer))))
(make-obsolete 'buffer-left-margin-width
	       "use (specifier-specs left-margin-width ...).")

(defun buffer-right-margin-width (&optional buffer)
  (specifier-specs right-margin-width (or buffer (current-buffer))))
(make-obsolete 'buffer-right-margin-width
	       "use (specifier-specs right-margin-width ...).")

;;;;;;;;;;;;;;;;;;;;;;;;;;;; modeline

(define-obsolete-function-alias 'redraw-mode-line 'redraw-modeline)
(define-obsolete-function-alias 'force-mode-line-update
  'redraw-modeline) ;; FSF compatibility
(define-obsolete-variable-alias 'mode-line-map 'modeline-map)
(define-obsolete-variable-alias 'mode-line-buffer-identification
  'modeline-buffer-identification)
(define-obsolete-variable-alias 'mode-line-process 'modeline-process)
(define-obsolete-variable-alias 'mode-line-modified 'modeline-modified)
(make-obsolete-variable 'mode-line-inverse-video
			"use set-face-highlight-p and set-face-reverse-p")
(define-obsolete-variable-alias 'default-mode-line-format
  'default-modeline-format)
(define-obsolete-variable-alias 'mode-line-format 'modeline-format)
(define-obsolete-variable-alias 'mode-line-menu 'modeline-menu)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; device stuff

(make-obsolete-variable 'window-system "use (device-type)")
(make-obsolete-variable 'meta-flag
			"use the `set-input-mode' function instead.")

(defun x-display-color-p (&optional device)
  "Returns non-nil if DEVICE is a color device."
  (eq 'color (device-class device)))
(make-obsolete 'x-display-color-p 'device-class)

(define-function 'x-color-display-p 'x-display-color-p)
(make-obsolete 'x-display-color-p 'device-class)

(defun x-display-grayscale-p (&optional device)
  "Returns non-nil if DEVICE is a grayscale device."
  (eq 'grayscale (device-class device)))
(make-obsolete 'x-display-grayscale-p 'device-class)

(define-function 'x-grayscale-display-p 'x-display-grayscale-p)
(make-obsolete 'x-display-grayscale-p 'device-class)

(define-obsolete-function-alias 'x-display-pixel-width 'device-pixel-width)
(define-obsolete-function-alias 'x-display-pixel-height
  'device-pixel-height)
(define-obsolete-function-alias 'x-display-planes 'device-bitplanes)
(define-obsolete-function-alias 'x-display-color-cells 'device-color-cells)

(define-obsolete-function-alias 'baud-rate 'device-baud-rate)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; faces

(define-obsolete-function-alias 'list-faces-display 'edit-faces)
(define-obsolete-function-alias 'list-faces 'face-list)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; files

(make-obsolete-variable 'trim-versions-without-asking 'delete-old-versions)

;;; Old XEmacs name; kept around for compatibility.
(put 'after-write-file-hooks 'permanent-local t)
(make-obsolete-variable 'after-write-file-hooks 'after-save-hook)
(defvar after-write-file-hooks nil
  "")

;;;;;;;;;;;;;;;;;;;;;;;;;;;; keymaps

(defun keymap-parent (keymap)
  "Returns the first parent of the given keymap."
  (car (keymap-parents keymap)))
(make-obsolete 'keymap-parent 'keymap-parents)

(defun set-keymap-parent (keymap parent)
  "Makes the given keymap have (only) the given parent."
  (set-keymap-parents keymap (if parent (list parent) '()))
  parent)
(make-obsolete 'set-keymap-parent 'set-keymap-parents)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; menu stuff

(defun add-menu-item (menu-path item-name function enabled-p &optional before)
  "Obsolete.  See the function `add-menu-button'."
  (or item-name (error "must specify an item name"))
  (add-menu-button menu-path (vector item-name function enabled-p) before))
(make-obsolete 'add-menu-item 'add-menu-button)

(defun add-menu (menu-path menu-name menu-items &optional before)
  "Obsolete.  See the function `add-submenu'."
  (or menu-name (error (gettext "must specify a menu name")))
  (or menu-items (error (gettext "must specify some menu items")))
  (add-submenu menu-path (cons menu-name menu-items) before))
(make-obsolete 'add-menu 'add-submenu)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; minibuffer

(define-obsolete-function-alias 'read-minibuffer
  'read-expression) ; misleading name

;;;;;;;;;;;;;;;;;;;;;;;;;;;; misc

;; (defun user-original-login-name ()
;;   "Return user's login name from original login.
;; This tries to remain unaffected by `su', by looking in environment variables."
;;   (or (getenv "LOGNAME") (getenv "USER") (user-login-name)))
(define-obsolete-function-alias 'user-original-login-name 'user-login-name)

(define-obsolete-function-alias 'menu-event-p 'misc-user-event-p)

; old names
(define-obsolete-function-alias 'read-input 'read-string)
(define-obsolete-function-alias 'show-buffer 'set-window-buffer)
(define-obsolete-function-alias 'buffer-flush-undo 'buffer-disable-undo)
(define-obsolete-function-alias 'eval-current-buffer 'eval-buffer)
(define-obsolete-function-alias 'byte-code-function-p
  'compiled-function-p) ;FSFmacs
(define-obsolete-function-alias 'truename 'file-truename)

;;(make-obsolete 'mod '%)	; mod and % are different now
(make-obsolete 'read-no-blanks-input 'read-string) ; mocklisp crud

(make-obsolete 'ring-mod 'mod)
(make-obsolete-variable 'auto-fill-hook 'auto-fill-function)
(make-obsolete-variable 'blink-paren-hook 'blink-paren-function)
(make-obsolete-variable 'lisp-indent-hook 'lisp-indent-function)
(make-obsolete-variable 'comment-indent-hook 'comment-indent-function)
(make-obsolete-variable 'temp-buffer-show-hook
			'temp-buffer-show-function)
(make-obsolete-variable 'inhibit-local-variables
			"use `enable-local-variables' (with the reversed sense).")
(make-obsolete-variable 'suspend-hooks 'suspend-hook)
(make-obsolete-variable 'first-change-function 'first-change-hook)
(make-obsolete-variable 'before-change-function 'before-change-functions)
(make-obsolete-variable 'after-change-function 'after-change-functions)
(make-obsolete-variable 'after-write-file-hooks 'after-save-hook)

(make-obsolete-variable 'unread-command-char 'unread-command-events)
(make-obsolete 'current-time-seconds 'current-time)
(make-obsolete 'sleep-for-millisecs "use sleep-for with a float")
;; too bad there's not a way to check for aref, assq, and nconc
;; being called on the values of functions known to return keymaps,
;; or known to return vectors of events instead of strings...

;;;;;;;;;;;;;;;;;;;;;;;;;;;; mouse

;;; (defun mouse-eval-last-sexpr (event)
;;;   (interactive "@e")
;;;   (save-excursion
;;;     (mouse-set-point event)
;;;     (eval-last-sexp nil)))

(define-obsolete-function-alias 'mouse-eval-last-sexpr 'mouse-eval-sexp)

(defun read-mouse-position (frame)
  (cdr (mouse-position (frame-device frame))))
(make-obsolete 'read-mouse-position 'mouse-position)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; window-system objects

(define-obsolete-function-alias 'pixel-name 'color-name)

;; compatibility function -- a front-end to make-glyph
(defun make-pixmap (name &optional locale)
  "Create a glyph with NAME as a pixmap specifier and locale LOCALE.
The file should be in `XBM' or `XPM' format.
If the XBMLANGPATH environment variable is set, it will be searched for
 matching files.  Next, the directories listed in the `x-bitmap-file-path'
 variable will be searched (this variable is initialized from the
 \"*bitmapFilePath\" resource).  Finally, the XEmacs etc/ directory
 (the value of `data-directory') will be searched.
The file argument may also be a list of the form (width height data) where
 width and height are the size in pixels, and data is a string, containing
 the raw bits of the bitmap.  (Bitmaps specified this way can only be one bit
 deep.)
If compiled with support for XPM, the file argument may also be a string
 which is the contents of an XPM file (that is, a string beginning with the
 characters \"/* XPM */\"; see the XPM documentation).
The optional second argument is the specifier locale for this pixmap glyph.
The returned object is a glyph object.  To get the actual pixmap object for
a given frame, use the function `glyph-instance'."
  (if (consp name)
      (setq name (vector 'xbm :data name)))
  (make-glyph name))
(make-obsolete 'make-pixmap 'make-glyph)

(define-obsolete-function-alias 'pixmap-width 'glyph-width)
(define-obsolete-function-alias 'pixmap-contributes-to-line-height-p
  'glyph-contrib-p-instance)
(define-obsolete-function-alias 'set-pixmap-contributes-to-line-height
  'set-glyph-contrib-p)
(define-obsolete-function-alias 'event-glyph 'event-glyph-extent)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; processes

(define-obsolete-function-alias 'send-string 'process-send-string)
(define-obsolete-function-alias 'send-region 'process-send-region)


;;; process.el --- commands for subprocesses; split out of simple.el

;; Copyright (C) 1985, 1986, 1987, 1993, 1994 Free Software Foundation, Inc.

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defun start-process-shell-command (name buffer &rest args)
  "Start a program in a subprocess.  Return the process object for it.
Args are NAME BUFFER COMMAND &rest COMMAND-ARGS.
NAME is name for process.  It is modified if necessary to make it unique.
BUFFER is the buffer or (buffer-name) to associate with the process.
 Process output goes at end of that buffer, unless you specify
 an output stream or filter function to handle the output.
 BUFFER may be also nil, meaning that this process is not associated
 with any buffer
Third arg is command name, the name of a shell command.
Remaining arguments are the arguments for the command.
Wildcards and redirection are handled as usual in the shell."
  (if (eq system-type 'vax-vms)
      (apply 'start-process name buffer args)
    (start-process name buffer shell-file-name "-c"
		   (concat "exec " (mapconcat 'identity args " ")))))

(defun call-process (program &optional infile buffer display &rest args)
  "Call PROGRAM synchronously in separate process.
The program's input comes from file INFILE (nil means `/dev/null').
Insert output in BUFFER before point; t means current buffer;
 nil for BUFFER means discard it; 0 means discard and don't wait.
Fourth arg DISPLAY non-nil means redisplay buffer as output is inserted.
Remaining arguments are strings passed as command arguments to PROGRAM.
If BUFFER is 0, returns immediately with value nil.
Otherwise waits for PROGRAM to terminate
and returns a numeric exit status or a signal description string.
If you quit, the process is killed with SIGINT, or SIGKILL if you
quit again."
  (apply 'call-process-internal program infile buffer display args))

(defun call-process-region (start end program
                            &optional deletep buffer displayp
                            &rest args)
  "Send text from START to END to a synchronous process running PROGRAM.
Delete the text if fourth arg DELETE is non-nil.
Insert output in BUFFER before point; t means current buffer;
 nil for BUFFER means discard it; 0 means discard and don't wait.
Sixth arg DISPLAY non-nil means redisplay buffer as output is inserted.
Remaining args are passed to PROGRAM at startup as command args.
If BUFFER is 0, returns immediately with value nil.
Otherwise waits for PROGRAM to terminate
and returns a numeric exit status or a signal description string.
If you quit, the process is first killed with SIGINT, then with SIGKILL if
you quit again before the process exits."
  (let ((temp (cond ((eq system-type 'vax-vms)
                     (make-temp-name "tmp:emacs"))
                    ;;#### MS-DOS code needed
                    (t
                     (make-temp-name "/tmp/emacs")))))
    (unwind-protect
         (progn
           (write-region start end temp nil 'silent)
           (if deletep (delete-region start end))
           (apply #'call-process program temp buffer displayp args))
      (condition-case ()
          (delete-file temp)
        (file-error nil)))))


(defun shell-command (command &optional flag)
  "Execute string COMMAND in inferior shell; display output, if any.
If COMMAND ends in ampersand, execute it asynchronously.
 
Optional second arg non-nil (prefix arg, if interactive)
means insert output in current buffer after point (leave mark after it).
This cannot be done asynchronously."
  (interactive (list (read-shell-command "Shell command: ")
		     current-prefix-arg))
  (if flag
      (progn (barf-if-buffer-read-only)
	     (push-mark)
	     ;; We do not use -f for csh; we will not support broken use of
	     ;; .cshrcs.  Even the BSD csh manual says to use
	     ;; "if ($?prompt) exit" before things which are not useful
	     ;; non-interactively.  Besides, if someone wants their other
	     ;; aliases for shell commands then they can still have them.
	     (call-process shell-file-name nil t nil
			   "-c" command)
	     (exchange-point-and-mark t))
    ;; Preserve the match data in case called from a program.
    (let ((data (match-data)))
      (unwind-protect
	  (if (string-match "[ \t]*&[ \t]*$" command)
	      ;; Command ending with ampersand means asynchronous.
	      (progn
 		(require 'background) ; whizzy comint background code
 		(background (substring command 0 (match-beginning 0))))
	    (shell-command-on-region (point) (point) command nil))
	(store-match-data data)))))

(defun shell-command-on-region (start end command &optional flag interactive)
  "Execute string COMMAND in inferior shell with region as input.
Normally display output (if any) in temp buffer `*Shell Command Output*';
Prefix arg means replace the region with it.
Noninteractive args are START, END, COMMAND, FLAG.
Noninteractively FLAG means insert output in place of text from START to END,
and put point at the end, but don't alter the mark.

If the output is one line, it is displayed in the echo area,
but it is nonetheless available in buffer `*Shell Command Output*'
even though that buffer is not automatically displayed.  If there is no output
or output is inserted in the current buffer then `*Shell Command Output*' is
deleted." 
  (interactive (list (min (point) (mark)) (max (point) (mark))
		     (read-shell-command "Shell command on region: ")
		     current-prefix-arg
		     (prefix-numeric-value current-prefix-arg)))
  (if flag
      ;; Replace specified region with output from command.
      (let ((swap (and interactive (< (point) (mark)))))
	;; Don't muck with mark
	;; unless called interactively.
	(and interactive (push-mark))
	(call-process-region start end shell-file-name t t nil
			     "-c" command)
	(let ((shell-buffer (get-buffer "*Shell Command Output*")))
	  (and shell-buffer (not (eq shell-buffer (current-buffer)))
	       (kill-buffer shell-buffer)))
	(and interactive swap (exchange-point-and-mark t)))
      ;; No prefix argument: put the output in a temp buffer,
      ;; replacing its entire contents.
      (let ((buffer (get-buffer-create "*Shell Command Output*"))
            (success nil)
            (directory default-directory))
        (unwind-protect
             (if (eq buffer (current-buffer))
                 ;; If the input is the same buffer as the output,
                 ;; delete everything but the specified region,
                 ;; then replace that region with the output.
                 (progn
                   (delete-region end (point-max))
                   (delete-region (point-min) start)
                   (call-process-region (point-min) (point-max)
                                        shell-file-name t t nil
                                        "-c" command)
                   (setq success t))
                 (progn
                   ;; Clear the output buffer, 
                   ;; then run the command with output there.
                   (save-excursion
                     (set-buffer buffer)
                     ;; XEmacs change
                     (setq default-directory directory)
                     (erase-buffer))
                   (call-process-region start end shell-file-name
                                        nil buffer nil
                                        "-c" command)
                   (setq success t)))
          ;; Report the amount of output.
          (let ((lines (save-excursion
                         (set-buffer buffer)
                         (if (= (buffer-size) 0)
                             0
                             (count-lines (point-min) (point-max))))))
            (cond ((= lines 0)
                   (if success
                       (message
                        "(Shell command completed with no output)"))
                   (kill-buffer buffer))
                  ((and success (= lines 1))
                   (message "%s"
                            (save-excursion
                              (set-buffer buffer)
                              (goto-char (point-min))
                              (buffer-substring (point)
                                                (progn (end-of-line)
						       (point))))))
                  (t 
                   (set-window-start (display-buffer buffer) 1))))))))


(defun start-process (name buffer program &rest program-args)
  "Start a program in a subprocess.  Return the process object for it.
Args are NAME BUFFER PROGRAM &rest PROGRAM-ARGS
NAME is name for process.  It is modified if necessary to make it unique.
BUFFER is the buffer or (buffer-name) to associate with the process.
 Process output goes at end of that buffer, unless you specify
 an output stream or filter function to handle the output.
 BUFFER may be also nil, meaning that this process is not associated
 with any buffer
Third arg is program file name.  It is searched for as in the shell.
Remaining arguments are strings to give program as arguments.
INCODE and OUTCODE specify the coding-system objects used in input/output
 from/to the process."
  (apply 'start-process-internal name buffer program program-args))

(defun open-network-stream (name buffer host service)
  "Open a TCP connection for a service to a host.
Returns a subprocess-object to represent the connection.
Input and output work as for subprocesses; `delete-process' closes it.
Args are NAME BUFFER HOST SERVICE.
NAME is name for process.  It is modified if necessary to make it unique.
BUFFER is the buffer (or buffer-name) to associate with the process.
 Process output goes at end of that buffer, unless you specify
 an output stream or filter function to handle the output.
 BUFFER may be also nil, meaning that this process is not associated
 with any buffer
Third arg is name of the host to connect to, or its IP address.
Fourth arg SERVICE is name of the service desired, or an integer
 specifying a port number to connect to."
  (open-network-stream-internal name buffer host service))

(defun shell-quote-argument (argument)
  "Quote an argument for passing as argument to an inferior shell."
  ;; Quote everything except POSIX filename characters.
  ;; This should be safe enough even for really weird shells.
  (let ((result "") (start 0) end)
    (while (string-match "[^-0-9a-zA-Z_./]" argument start)
      (setq end (match-beginning 0)
	    result (concat result (substring argument start end)
			   "\\" (substring argument end (1+ end)))
	    start (1+ end)))
    (concat result (substring argument start))))

(defun exec-to-string (command)
  "Execute COMMAND as an external process and return the output of that
process as a string"
  ;; by "William G. Dubuque" <wgd@zurich.ai.mit.edu>
  (with-output-to-string
    (call-process shell-file-name nil t nil "-c" command)))

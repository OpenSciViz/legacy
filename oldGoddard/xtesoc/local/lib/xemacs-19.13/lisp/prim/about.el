;;; about.el --- the About The Authors page (shameless self promotion).
;;;

;; Copyright (c) 1995 XEmacs Advocacy Organization.

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; This is kind of a kludge.  We were going to use W3 to do this, but
;;; it's too slow to load, and HTML gives you too little control over
;;; the layout (especially indentation and inter-paragraph spacing).
;;; Maybe the text should have been written in limited HTML anyway,
;;; and I should have hacked up a simple and fast parser for it, but
;;; it's done now...
;;;
;;; Code: Jamie Zawinski <jwz@netscape.com>
;;; Text: Ben Wing <wing@netcom.com>, Jamie Zawinski <jwz@netscape.com>
;;; Hard: Amiga 1000, Progressive Peripherals Frame Grabber.
;;; Soft: FG 2.0, DigiPaint 3.0, pbmplus (dec 91), xv 3.0.
;;; Modified for 19.11 by Eduardo Pelegri-Llopart <pelegri@eng.sun.com>
;;;		      and Chuck Thompson <cthomp@cs.uiuc.edu>
;;; More hacking for 19.12 by Chuck Thompson and Ben Wing.
;;; 19.13 updating done by Chuck Thompson.

(defvar about-xref-map (let ((map (make-sparse-keymap)))
			 (define-key map 'button1 'about-xemacs-xref)
			 (define-key map 'button2 'about-xemacs-xref)
			 (define-key map '(return) 'about-xemacs-xref)
			 map))

;; This historically significant variable has been removed from service.
(defvar what-are-we-calling-it-today "XEmacs")

(defun about-face (text face)
  (let ((p (point))
	e)
    (insert text)
    (setq e (make-extent p (point)))
    ;;(set-extent-property e 'start-open t)
    (set-extent-face e face)
    e))

(defun about-xref (text xref help)
  (let ((e (about-face text 'bold)))
    (set-extent-property e 'keymap about-xref-map)
    (set-extent-property e 'highlight t)
    (set-extent-property e 'xref xref)
    (set-extent-property e 'help-echo help)
    e))

;;;###autoload
(defun about-xemacs ()
  (interactive)
  (delete-other-windows)
  (switch-to-buffer (get-buffer-create "About XEmacs"))
  (buffer-disable-undo (current-buffer))
  (widen)
  (set (make-local-variable 'tab-width) 8)
  (setq buffer-read-only t)
  (view-mode nil 'kill-buffer)		;; assume the new view-less
  (let* ((buffer-read-only nil)
	 (indent-tabs-mode t)
	 )
    (erase-buffer)
    (insert "\n")
    (indent-to (startup-center-spaces xemacs-logo))
    (let ((e (make-extent (point) (point))))
      (set-extent-begin-glyph e xemacs-logo))
    (insert "\n\n")
    (indent-to (startup-center-spaces "(formerly known as Lucid Emacs)"))
    (insert "(formerly known as Lucid Emacs)")
    (insert "\n\n")
    (indent-to (startup-center-spaces "version 19.13; September 1995"))
    (about-xref "version 19.13; September 1995" 'news "The latest NEWS of XEmacs")
    (insert "\n\n")

    (insert "\n\t")
    (about-face "XEmacs" 'italic)
    (insert " is a powerful, extensible text editor with full GUI
	support, initially based on an early version of GNU Emacs 19 from
	the Free Software Foundation and since kept up to date with recent
	versions of that product.  XEmacs stems from a ")
    (about-xref "collaboration" 'history "An XEmacs History Lesson")
    (insert "\n\tof Lucid, Inc. with Sun Microsystems, Inc. and the University
	of Illinois with additional support having been provided by
	Amdahl Corporation and INS Engineering Corporation.\n\n\t")

	(insert "In almost all circumstances, Emacs-Lisp code written for
	GNU Emacs versions 18 and 19 will run under XEmacs without
        requiring any modifications, or at most will require small
	changes to accommodate an improved functional interface.\n\n\t")

    (insert "XEmacs provides a great number of ")
    (about-xref "new features" 'features "See a list of the new features.")
    (insert ".  More details
	on XEmacs's functionality, including bundled packages can be
	obtained through the ")

    (about-xref "`info`" 'info "Look at the info pages")
    (insert " on-line information system.

	The WWW page for XEmacs can be browsed, using any WWW browser, at\n\t\t")
    (about-xref "http://xemacs.cs.uiuc.edu/" 'w3-xemacs "Go to the XEmacs World Wide Web page")
    (insert "\n\n\tNote that w3 (XEmacs's own browser), might need customization
	(due to firewalls) in order to work correctly.\n\n\t")

    (insert "XEmacs is the result of the time and effort of many people.
	The developers responsible for the 19.13 release are:

		 * ") (about-xref "Chuck Thompson" 'cthomp "Find out more about Chuck Thompson") (insert "  <cthomp@cs.uiuc.edu>
		 * ") (about-xref "Ben Wing" 'wing "Find out more about Ben Wing") (insert "  <wing@netcom.com>

		 * ") (about-xref "And many other contributors..." 'others "Read about the legion of XEmacs hackers") (insert "

	Jamie Zawinski was Mr. Lucid Emacs from 19.0 through 19.10,
	the last release actually named Lucid Emacs.  Richard Mlynarik
	was crucial to most of those releases.

		 * ") (about-xref "Jamie Zawinski" 'jwz "Find out more about Jamie Zawinski") (insert "  <jwz@netscape.com>
		 * ")  (about-xref "Richard Mlynarik" 'mly "Find out more about Richard Mlynarik")  (insert "  <mly@adoc.xerox.com>")
   (insert "\n\n\tClick ")
   (about-xref "here" 'kill-buffer "Exit the About page")
   (insert " to remove (kill) this buffer.")
   (goto-char (point-min)))
  )

(defun about-load-mosaic (&optional who-to-load)
  (save-excursion
    (set-buffer (get-buffer-create "About XEmacs"))
    (toggle-read-only 0)

    (let ((rest (if who-to-load (list who-to-load)
		  '(cthomp wing stig jwz mly vladimir baw piper bw wmperry)))
	  (got-error nil))
      (while rest
	(let* ((who (car rest))
	       (who-xpm (expand-file-name
			 (concat (symbol-name who)
				 (if (memq (device-class (selected-device))
					   '(color grayscale))
				     ""
				   "m")
				 ".xpm")
			 data-directory)))
	  (or (file-exists-p who-xpm) (setq who-xpm (concat who-xpm ".Z")))
	  (if (eq nil (assoc who (buffer-local-variables)))
	      (make-local-variable who))
	  (if (and (boundp who)
		   (glyphp (symbol-value who)))
	      nil
	    (message "One moment please...")
	    (condition-case c
		(save-restriction
		  (set who nil)
		  (narrow-to-region (point) (point))
		  (insert-file-contents who-xpm)
		  (if (looking-at "\037\235") ;may already be decompressed...
		      (call-process-region (point-min) (point-max)
					   "zcat" t t nil))
		  (set who (make-glyph
			    (prog1 (buffer-string)
			      (delete-region (point-min) (point-max)))))
		  )
	      (error
	       (setq got-error t)
	       (message nil)
	       (display-error c nil)
	       (sit-for 2)))))
	(setq rest (cdr rest)))
      (or got-error (message nil)))
    (toggle-read-only 1)
    ))

(defun about-add-mosaic ()
  (goto-char (point-min))
  (about-load-mosaic)

  ;; HERE TO PLACE ADDITIONAL MUGSHOTS

  (goto-char (point-max))
  (insert "\n   ")

  (let ((rest '(cthomp wing stig linebreak jwz mly vladimir linebreak baw piper bw linebreak wmperry))
	(got-error nil))
    (while rest
      (if (eq (car rest) 'linebreak)
	  (insert "\n\n  ")
	(let* ((who (car rest))
	       (b (get-buffer "About XEmacs"))
	       (p (symbol-value-in-buffer who b nil)))
	  (or (glyphp p) (setq p nil))
	  (and p
	       (let ((e (make-extent (point) (point))))
		 (set-extent-begin-glyph e p)
		 (set-extent-property e 'keymap about-xref-map)
		 (set-extent-property e 'xref who)))
	  (insert " ")
	  (sit-for 0)))
      (setq rest (cdr rest)))
    (insert "\n")
    (goto-char (point-min))
    (or got-error (message nil)))
  )

(defun about-xemacs-xref (e)
  (interactive "@e")
  ;; If e is null then we got here by a keypress over an extent.
  (let* ((extent (or (and (null e) (extent-at (point)))
		     (event-glyph-extent e)
 		     (extent-at (event-point e)
				(event-buffer e)
				'xref)))
	 (xref (extent-property extent 'xref))
	 prev-page)
    ;; prev-page is used for the core people's pages since they can be
    ;; reached from two different locations
    (if (equal (buffer-name) "About XEmacs")
	(setq prev-page 'about)
      ;; Kill the sub-buffers when going back to the top, so that we
      ;; don't hold pointers to the bitmaps longer than necessary.
      (if (not (eq xref 'w3-jamie))
	  (progn
	    (kill-buffer (current-buffer))
	    (setq prev-page 'others))))
    (cond
     ((eq xref 'about)
      (about-xemacs))
     ((eq xref 'info)
      (info))
     ((or (eq xref 'w3-xemacs) (eq xref 'w3-jamie))
      (if (boundp 'highlight-headers-follow-url-function)
	  (funcall highlight-headers-follow-url-function
		   (if (eq xref 'w3-xemacs)
		       "http://xemacs.cs.uiuc.edu/"
		     "http://www.netscape.com/people/jwz/"))
	(w3-fetch (if (eq xref 'w3-xemacs)
		       "http://xemacs.cs.uiuc.edu/"
		     "http://www.netscape.com/people/jwz/"))))
     ((eq xref 'kill-buffer)
      (kill-buffer (current-buffer)))
     ((eq xref 'news)
      (view-emacs-news)
      (view-mode nil 'kill-buffer)		;; assume the new view-less
      (save-excursion
	(goto-char (point-min))
	(let ((buffer-read-only nil))
	  (insert "\nClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n\n")
	  (set-buffer-modified-p nil)
	  )))
     (t
      (switch-to-buffer (get-buffer-create
			 (cond ((eq xref 'jwz) "About Jamie Zawinski")
			       ((eq xref 'cthomp) "About Chuck Thompson")
			       ((eq xref 'wing) "About Ben Wing")
			       ((eq xref 'mly) "About Richard Mlynarik")
			       ((eq xref 'vladimir) "About Vladimir Ivanovic")
			       ((eq xref 'baw) "About Barry Warsaw")
			       ((eq xref 'wmperry) "About William Perry")
			       ((eq xref 'bw) "About Bob Weiner")
			       ((eq xref 'piper) "About Andy Piper")
			       ((eq xref 'stig) "About Jonathan Stigelman")
			       ((eq xref 'others) "About Everyone")
			       ((eq xref 'features) "New XEmacs Features")
			       ((eq xref 'history) "XEmacs History")
			       )))
      (delete-other-windows)
      (buffer-disable-undo (current-buffer))
      (widen)
      (setq buffer-read-only t)
      (view-mode nil 'kill-buffer)		;; assume the new view-less
      (let ((buffer-read-only nil)
	    (case-fold-search nil)
	    )
	(if (and (not (eq xref 'others)) (not (eq xref 'history))
		 (not (eq xref 'features)))
	    (about-load-mosaic xref))
	(erase-buffer)
	(let* ((b (get-buffer "About XEmacs"))
	       (p (and b (symbol-value-in-buffer xref b nil))))
	  (or (glyphp p) (setq p nil))
	  (cond (p
		 (insert "\n\t")
		 (set-extent-begin-glyph (make-extent (point) (point)) p)
		 (insert "\n\t"))
		(t
		 (insert "\n\t"))))
	(cond
	 ((eq xref 'history)
	  (insert "Click ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n\n\t")

	  (about-face "XEmacs" 'bold)
	  (insert "\n\n\n\t")
	  (about-face "The Lucid, Inc. Point of View" 'italic)
	  (insert "

	At the time of the inception of Lucid Emacs (the former name
	of XEmacs), Lucid's latest product was Energize, a C/C++
	development environment.  Rather than invent (and force our
	users to learn) a new user interface, we chose to build part
	of our environment on top of the world's best editor, GNU
	Emacs.  (Though our product is commercial, the work we did on
	GNU Emacs is free software, and is useful in its own right.)

	We needed a version of Emacs with mouse-sensitive regions,
	multiple fonts, the ability to mark sections of a buffer as
	read-only, the ability to detect which parts of a buffer has
	been modified, and many other features.

	For our purposes, the existing version of Epoch was not
	sufficient; it did not allow us to put arbitrary pixmaps/icons
	in buffers, `undo' did not restore changes to regions, regions
	did not overlap and merge their attributes in the way we needed,
	and several other things.

	We could have devoted our time to making Epoch do what we needed
	(and, in fact, we spent some time doing that in 1990) but, since
	the FSF planned to include Epoch-like features in their version
	19, we decided that our efforts would be better spent improving
	Emacs 19 instead of Epoch.

	Our original hope was that our changes to Emacs would be
	incorporated into the \"official\" v19.  However, scheduling
	conflicts arose, and we found that, given the amount of work
	still remaining to be done, we didn't have the time or manpower
	to do the level of coordination that would be necessary to get
	our changes accepted by the FSF.  Consequently, we released our
	work as a forked branch of Emacs, instead of delaying any
	longer.

	Roughly a year after Lucid Emacs 19.0 was released, a beta
	version of the FSF branch of Emacs 19 was released.  The FSF
	version is better in some areas, and worse in others, as
	reflects the differing focus of our development efforts.

	We plan to continue developing and supporting Lucid Emacs, and
	merging in bug fixes and new features from the FSF branch as
	appropriate; we do not plan to discard any of the functionality
	that we implemented which RMS has chosen not to include in his
	version.

	Certain elements of Lucid Emacs, or derivatives of them, have
	been ported to the FSF version.  We have not been doing work in
	this direction, because we feel that Lucid Emacs has a cleaner
	and more extensible substrate, and that any kind of merger
	between the two branches would be far easier by merging the FSF
	changes into our version than the other way around.

	We have been working closely with the Epoch developers to merge
	in the remaining Epoch functionality which Lucid Emacs does not
	yet have.  Epoch and Lucid Emacs will soon be one and the same
	thing.  Work is being done on a compatibility package which will
	allow Epoch 4 code to run in XEmacs with little or no change.")

	  (insert "\n\n\n\t")
	  (about-face "The Sun Microsystems, Inc. Point of View" 'italic)
	  (insert "

	Emacs 18 has been around for a long, long time.  Version 19 was
	supposed to be the successor to v18 with X support.  It was
	going to be available \"real soon\" for a long time (some
	people remember hearing about v19 as early as 1984!), but it
	never came out.  v19 development was going very, very slowly,
	and from the outside it seemed that it was not moving at all.
	In the meantime other people gave up waiting for v19 and
	decided to build their own X-aware Emacsen.  The most important
	of these was probably Epoch, which came from University of
	Illinois (\"UofI\") and was based on v18.

	Around three or four years ago what was then called SunPro (a
	division of Sun Microsystems, Inc.) decided that it wanted an
	integrated editor.  They contracted with UofI to provide a
	number of basic enhancements to the functionality in Epoch.
	UofI initially was planning to deliver this on top of Epoch
	code.

	In the meantime (actually some time before they talked with UofI)
	Lucid had decided that it also wanted to provide an integrated
	environment with an integrated editor.  Lucid decided that the
	v19 basis was a better one than v18 and thus decided not to use
	Epoch but instead work with Richard Stallman, the head of the
	Free Software Foundation and principle author of Emacs, on
	getting v19 out.  At some point Stallman and Lucid parted ways.
	Lucid kept working and got a v19 out that they called Lucid
	Emacs 19.

	After Lucid's v19 came out it became clear to UofI and SunPro
	that the right thing to do was to push for an integration of
	both Lucid Emacs and Epoch, and to get the deliverables that
	SunPro was asking from UofI on top of this integrated
	platform.  Through the last three or four years, SunPro has
	been actively supporting this product and has been investing a
	comparable amount of effort into it as Lucid has.  Substantial
	portions of the current code have originated under the support
	of SunPro, either directly in SunPro, or in UofI but paid for
	by SunPro.  This code was kept away from Lucid for a while,
	but later was made available to them.  Initially Lucid didn't
	know that SunPro was supporting UofI, but later they were open
	about it.

	Around two years ago the SunPro-related code started showing up
	in Lucid Emacs.  This started with the infusion of the Epoch
	redisplay code.

	At this moment there is basically no difference in the source
	trees between what is at UofI and at Sun.  All the development
	sites are in sync.

	SunPro originally called the integrated product ERA, for
	\"Emacs Rewritten Again\".  SunPro and Lucid eventually came
	to an agreement to find a name for the product that was not
	specific to either company.  An additional constraint that
	Lucid placed on the name was that it must contain the word
	\"Emacs\" in it -- thus \"ERA\" was not acceptable.  The
	tentatively agreed-upon name was \"XEmacs\", and this has
	been the name of the product since version 19.11.)")

	  (insert "\n\n\n\t")
	  (about-face "SunPro is now DevPro (SunSoft Developer Products)\n" 'italic)
	  (insert "
	In mid-'94 Sun completed a reorganization where SunPro moved to
	SunSoft.  As a consequence, SunPro is now called SunSoft Developer
	Products, or DevPro for short. ")

	  (insert "\n\n\n\t")
	  (about-face "Lucid goes under\n" 'italic)
	  (insert "
	Around mid-'94, Lucid went out of business.  Development on
	XEmacs, however, has continued unabated under the auspices of
	Sun Microsystems and the University of Illinois, with help
	from Amdahl Corporation and INS Engineering Corporation.
	Sun plans to continue to support XEmacs into the future.")

	  (insert "\n\n\n\t")
	  (about-face "The Amdahl Corporation point of view" 'italic)
	  (insert "

	Amdahl Corporation's Storage Products Group (SPG) uses XEmacs
	as the focal point of a environment for development of the
	microcode used in Amdahl's large-scale disk arrays, or DASD's.
	SPG has joint ventures with Japanese companies, and decided
	in late 1994 to contract out for work on XEmacs in order
	to hasten the development of Mule support (i.e. support for
	Japanese, Chinese, etc.) in XEmacs and as a gesture of goodwill
	towards the XEmacs community for all the work they have done
	on making a powerful, modern, freely available text editor.
	Through this contract, Amdahl provided a large amount of work
	in XEmacs in the form of rewriting the basic text-processing
	mechanisms to allow for Mule support and writing a large
	amount of the support for multiple devices.
	
	Although Amdahl is no longer hiring a full-time contractor,
	they are still funding part-time work on XEmacs and providing
	resources for further XEmacs development.")

	  (insert "\n\n\n\t")
	  (about-face "The INS Engineering point of view" 'italic)
	  (insert "

        INS Engineering Corporation, based in Tokyo, bought rights
	to sell Energize when Lucid went out of business.  Unhappy
	with the performance of the Japanese support in XEmacs 19.11,
	INS also contributed to the XEmacs development from late 1994
	to early 1995.")

	  (insert "\n\n\n\t")
	  (insert "Click ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n\n\t")
	  )
	 ((eq xref 'jwz)
	  (about-face "Jamie Zawinski" 'bold)
	  (insert "\t\t\"")
	  (about-face "So much to do, so little time." 'italic)
	  (insert "\"\n")
	  (insert "\n
	Jamie Zawinski was primarily to blame for Lucid Emacs from its
	inception in 1991, to 1994 when Lucid Inc. finally died.  He is
	now to be found at Netscape Communications, hacking on the Unix
	version of Netscape Navigator, and engaging in extensive sleep
	deprivation experiments.

	Come visit his glorified .plan file at

		")
	  (about-xref "http://www.netscape.com/people/jwz/" 'w3-jamie "Visit Jamie's WWW page")
	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )
	 ((eq xref 'cthomp)
	  (about-face "Chuck Thompson" 'bold)
	  (insert " <cthomp@cs.uiuc.edu>

	Chuck, through being in the wrong place at the right time, has
	gotten stuck with being Jamie's replacement as the primary
	maintainer of XEmacs.  This has caused his hair to begin
	falling out and quadrupled his daily coffee dosage.  Though he
	works at and for the University of Illinois his funding
	actually comes from Sun Microsystems.

	He has worked on XEmacs since November 1992, which fact
	occasionally gives him nightmares.  His main contributions
	have been the greatly enhanced redisplay engine, scrollbar
	support, the toolbars, configure support and numerous other
	minor features and fixes.

	Rumors that Chuck is aka Black Francis aka Frank Black are
	completely unfounded.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )
	 ((eq xref 'wing)
	  (about-face "Ben Wing" 'bold)
	  (insert " <wing@netcom.com>

	I'm not a thug -- I just play one on video.
	My roommate says I'm a San Francisco \"Mission Critter\".\n\n\t")
	  (about-face "Gory stuff follows:" 'italic)
	  (insert "

	In 1992 I left a stuffy East-Coast university, set out into the
	real world, and ended up a co-founder of Pearl Software.  As
	part of this company, I am the principal architect of
	Win-Emacs, a port of Lucid Emacs to Microsoft Windows and
	Windows NT (for more info, e-mail to ")
	  (about-face "info@pearlsoft.com" 'italic)
	  (insert ").

	Since April 1993, I've worked on XEmacs as a contractor
	for various companies, changing hats faster than Ronald Reagan's
        hair color (oops, did I just show my age?).  My main contributions
        to XEmacs include adding the Mule support, rewriting large parts
        of the internals and the gory Xt/Xlib interfacing, implementing
        the external client widget, improving the documentation (especially
        the Emacs Lisp manual), and being a general nuisance ... er,
        brainstormer for many of the new features of XEmacs.")
	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )
	 ((eq xref 'mly)
	  (about-face "Richard Mlynarik" 'bold)
	  (insert " <mly@adoc.xerox.com>

	Cars are Evil.  Ride a bike.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )
	 ((eq xref 'vladimir)
	  (about-face "Vladimir Ivanovic" 'bold)
	  (insert " <vladimir@mri.com>

	Technical lead for XEmacs at DevPro (formerly SunPro), a
	division of Sun Microsystems.  He is now with Microtec
	Research, Inc. working on embedded systems development
	tools.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'baw)
	  (about-face "Barry Warsaw" 'bold)
	  (insert " <bwarsaw@cnri.reston.va.us>

	Author of cc-mode for C++, C, and Objective-C editing, and
	Supercite for mail and news citing.  Also various and sundry other
	Emacs utilities, fixes, enhancements and kludgery as whimsy,
	boredom, and ToT dictate (but not necessarily in that order).


	Daddy
	� 1994 Warsaw
	========
	Drive me Daddy, drive me quick
	Push my pedal, shift my stick
	Fill me up with golden gas
	My rubber squeals, I go real fast

	Milk me Daddy, milk me now
	Milk me like a big ol' cow
	I've got milk inside my udder
	Churn it up and make some butter")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'bw)
	  (about-face "Bob Weiner" 'bold)
	  (insert " <weiner@mot.com>

	Author of the Hyperbole everyday information management
	hypertext system and the OO-Browser multi-language code
	browser.  He also designed the InfoDock integrated tool
	framework for software engineers.  It runs atop XEmacs and is
	available from \"/anonymous@cs.uiuc.edu:pub/xemacs/infodock\".
	His interests include user interfaces, information management,
	CASE tools, communications and enterprise integration.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'piper)
	  (about-face "Andy Piper" 'bold)
	  (insert " <andyp@parallax.co.uk>

	Author of the original \"fake\" XEmacs toolbar, and outl-mouse for
	mouse gesture based outlining.  Accomplished kludge contributor.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'stig)
	  (about-face "Jonathan Stigelman" 'bold)
	  (insert " <stig@hackvan.com>

	Stig is sort of a tool fetishist.  He has a hate/love relationship
	with computers and he hacks on XEmacs because it's a good tool that
	makes computers somewhat less of a nuisance.  Besides XEmacs, Stig
	especially likes his Leatherman, his Makita, and his lockpicks.
	Stig wants a MIG welder and air tools.

	Stig likes to perch, hang from the ceiling, and climb on the walls.
	Stig has a cool van.  Stig would like to be able to telecommute
	from, say, the north rim of the Grand Canyon or the midst of Baja.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'wmperry)
	  (about-face "William Perry" 'bold)
	  (insert " <wmperry@spry.com>

 	Author of Emacs-w3, the builtin web browser that comes with XEmacs.

        He is currently working on adding really cool stylesheets to the
        web, which will stress the new capabilities of XEmacs to the limit.

        He only gets paid for working on an HTTP server for Spry, but will
        hack emacs for beer.")

	  (insert "\n\n\tClick ")
	  (about-xref "here" prev-page "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )

	 ((eq xref 'others)
	  (insert "Click ")
	  (about-xref "here" 'about "Return to previous page")
	  (insert " to go back to the previous page\n\n\t")

	  (about-face "Other Contributors to XEmacs" 'italic)

	  (insert "

	Like most free software, XEmacs is a collaborative effort.
	These are some of the contributors; we have no doubt forgotten
	someone; we apologize!  You can see some of our faces further below.

	") (about-xref "Vladimir Ivanovic" 'vladimir "Find out more about Vladimir Ivanovic") (insert " <vladimir@eng.sun.com>
	  Technical lead for XEmacs at DevPro (formerly SunPro), a division
          of Sun Microsystems.

	") (about-xref "Jonathan Stigelman" 'stig "Find out more about Jonathan Stigelman") (insert " <stig@hackvan.com>
          Peripatetic uninominal Emacs hacker.  Stig sometimes operates
	  out of a big white van set up for nomadic living and hacking.
	  Implemented the faster stay-up Lucid menus and hyper-apropos.
	  Contributor of many dispersed improvements in the core Lisp code,
	  and back-seat contributor for several of it's major packages.

	") (about-xref "Barry Warsaw" 'baw "Find out more about Barry Warsaw") (insert " <bwarsaw@cnri.reston.va.us>
	  Author of cc-mode for C++, C, and Objective-C editing, and
	  Supercite for mail and news citing.  Also various and sundry other
	  Emacs utilities, fixes, enhancements and kludgery as whimsy,
	  boredom, and ToT dictate (but not necessarily in that order).

	") (about-xref "Andy Piper" 'piper "Find out more about Andy Piper") (insert " <andyp@parallax.co.uk>
	  Created the prototype for the toolbars.  Has been the first to make
	  use of many of the new XEmacs graphics features.

	") (about-xref "Bob Weiner" 'bw "Find out more about Bob Weiner") (insert " <weiner@mot.com>
	  Author of the Hyperbole everyday information management
	  hypertext system and the OO-Browser multi-language code
	  browser.  He also designed the InfoDock integrated tool
	  framework for software engineers.  It runs atop XEmacs and is
	  available from \"/anonymous@cs.uiuc.edu:pub/xemacs/infodock\".
	  His interests include user interfaces, information management,
	  CASE tools, communications and enterprise integration.

	") (about-xref "William Perry" 'wmperry "Find out more about Bill Perry") (insert " <wmperry@spry.com>
	  Author of W3, a package for browsing the World Wide Web
	  which is included in the standard XEmacs distribution.
	  Although W3 runs on all versions of Emacs, Bill has been
	  quick to take advantage of the unique features of XEmacs
	  (such as embedded images and windows).  Thus, the XEmacs
	  version of W3 is significantly more powerful than versions
	  running in other Emacs variants.

	Kyle Jones <kyle@crystal.wonderworks.com>
	  Author of VM (View Mail), a mail-reading package that is
	  included in the standard XEmacs distribution, and
	  contributor of many improvements and bug fixes.  Unlike most
	  other mail-reading packages, VM uses the standard Unix-mail
	  format for its mailboxes; thus, you can use VM concurrently
	  with standard mail readers such as Unix mail and ELM.

	Darrell Kindred <Darrell.Kindred@cmu.edu>
	  Unofficial maintainer of the xemacs-beta list of extant
	  bugs and contributor of an extraordinary number of
	  important bug fixes, many of them in areas that neither
	  Chuck nor Ben was particularly enthusiastic about
	  investigating.

	Eduardo Pelegri-Llopart <pelegri@eng.sun.com>
	  Author of EOS, a package included in the standard XEmacs
	  distribution that integrates XEmacs with the SPARCworks
	  development environment from SunPro.  Past lead for XEmacs
	  at SunPro; advocated the validity of using Epoch, and later
	  Lemacs, at SunPro through several early prototypes.

	Matthieu Devin <devin@rs.com>
	  Part of the original (pre-19.0) Lucid Emacs development team.
	  Matthieu wrote the initial Energize interface, designed the
	  toolkit-independent Lucid Widget library, and fixed enough
	  redisplay bugs to last a lifetime.  The features in Lucid
	  Emacs were largely inspired by Matthieu's initial prototype
	  of an Energize interface using Epoch.

	Harlan Sexton <hbs@odi.com>
	  Part of the original (pre-19.0) Lucid Emacs development team.
	  Harlan designed and implemented many of the low level data
	  structures which are original to the Lucid version of Emacs,
	  including extents and hash tables.

	Eric Benson <eb@kaleida.com>
	  Part of the original (pre-19.0) Lucid Emacs development team.
	  Eric played a big part in the design of many aspects of the
	  system, including the new command loop and keymaps, fixed
	  numerous bugs, and has been a reliable beta tester ever
	  since.

	John Rose <john.rose@sun.com>
	  Author of many extensions to the `extents' code, including
	  the initial implementation of `duplicable' properties.

	Hans Muller <hmuller@eng.sun.com>
	 Author of the code used to connect XEmacs with ToolTalk,
	 and of an early client of the external Emacs widget.

	In addition to those just mentioned, the following people have
	spent a great deal of effort providing feedback, testing beta
	versions of XEmacs, providing patches to the source code,
	or doing all of the above.  We couldn't have done it without
	them.

	  Nagi M. Aboulenein <aboulene@cs.wisc.edu>
	  Gennady Agranov <agranov@csa.CS.Technion.Ac.IL>
	  Mark Allender <allender@vnet.IBM.COM>
	  Butch Anton <butch@taligent.com>
	  Tor Arntsen <tor@spacetec.no>
	  Mike Battaglia <mbattagl@dsccc.com>
	  Neal Becker <neal@ctd.comsat.com>
	  Paul Bibilo <peb@delcam.co.uk>
	  Jan Borchers <job@ira.uka.de>
	  Mark Borges <mdb@cdc.noaa.gov>
	  Tim Bradshaw <tfb@edinburgh.ac.uk>
	  Rick Braumoeller <rickb@mti.sgi.com>
	  Matthew J. Brown <mjb@doc.ic.ac.uk>
	  Martin Buchholz <mrb@almaden.ibm.com>
	  Alastair Burt <burt@dfki.uni-kl.de>
	  Rick Busdiecker <rfb@lehman.com>
	  Richard Caley <rjc@cstr.edinburgh.ac.uk>
	  Philippe Charton <charton@lmd.ens.fr>
	  Peter Cheng <phc@sun.com>
	  Jin S. Choi <jin@atype.com>
	  Serenella Ciongoli <czs00@ladybug.oes.amdahl.com>
	  Richard Cognot <cognot@ensg.u-nancy.fr>
	  Andy Cohen <cohen@andy.bu.edu>
	  Christopher Davis <ckd@kei.com>
	  Michael Diers <mdiers@logware.de>
	  William G. Dubuque <wgd@martigny.ai.mit.edu>
	  Samuel J. Eaton <samuele@cogs.susx.ac.uk>
	  Carl Edman <cedman@Princeton.EDU>
	  Eric Eide <eeide@asylum.cs.utah.edu>
	  David Fletcher <frodo@tsunami.com>
	  Paul Flinders <ptf@delcam.co.uk>
	  Jered J Floyd <jered@vorlon.mit.edu>
	  Jerry Frain <jerry@sneffels.tivoli.com>
	  Benjamin Fried <bf@morgan.com>
	  Barry Friedman <friedman@bnr.ca>
	  Tim Geisler <Tim.Geisler@informatik.uni-muenchen.de>
	  Dave Gillespie <daveg@synaptics.com>
	  Christian F. Goetze <c-goetze@akebono.u-aizu.ac.jp>
	  James Grinter <jrg@demon.net>
	  Ben Gross <bgross@uiuc.edu>
	  Dirk Grunwald <grunwald@foobar.cs.Colorado.EDU>
	  Dipankar Gupta <dg@hplb.hpl.hp.com>
	  Markus Gutschke <gutschk@GOEDEL.UNI-MUENSTER.DE>
	  Adam Hammer <hammer@cs.purdue.edu>
	  Magnus Hammerin <magnush@epact.se>
	  Derek Harding <dharding@lssec.bt.co.uk>
	  John Haxby <J.Haxby@isode.com>
	  Charles Hines <chuck_hines@VNET.IBM.COM>
	  David Hughes <djh@harston.cv.com>
	  Andrew Innes <andrewi@harlequin.co.uk>
	  Markku Jarvinen <Markku.Jarvinen@simpukka.funet.fi>
	  Robin Jeffries <robin.jeffries@sun.com>
	  Philip Johnson <johnson@uhics.ics.Hawaii.Edu>
	  Doug Keller <dkeller@vnet.ibm.com>
	  Gregor Kennedy <gregork@dadd.ti.com>
	  Michael Kifer <kifer@cs.sunysb.edu>
	  Simon Leinen <simon@lia.di.epfl.ch>
	  James LewisMoss <moss@cs.sc.edu>
	  Mats Lidell <mats.lidell@contactor.se>
	  Matt Liggett <mliggett@seven.ucs.indiana.edu>
	  Christian Limpach <Christian.Limpach@nice.ch>
	  Robert Lipe <robertl@arnet.com>
	  Damon Lipparelli <lipp@aa.net>
	  Hamish Macdonald <hamish@bnr.ca>
	  Patrick MacRoberts <macro@hpcobr30.cup.hp.com>
	  Steve March <smarch@quaver.urbana.mcd.mot.com>
	  Simon Marshall <Simon.Marshall@mail.esrin.esa.it>
	  Dave Mason <dmason@plg.uwaterloo.ca>
	  Jaye Mathisen <mrcpu@cdsnet.net>
	  Michael Meissner <meissner@osf.org>
	  David M. Meyer <meyer@ns.uoregon.edu>
	  John Morey <jmorey@crl.com>
	  Rob Mori <rob.mori@sun.com>
	  Heiko Muenkel <muenkel@tnt.uni-hannover.de>
	  Arup Mukherjee <arup+@cs.cmu.edu>
	  Colas Nahaboo <Colas.Nahaboo@sophia.inria.fr>
	  Lynn D. Newton <lynn@ives.phx.mcd.mot.com>
	  Georg Nikodym <Georg.Nikodym@canada.sun.com>
	  Andy Norman <ange@hplb.hpl.hp.com>
	  Kim Nyberg <kny@tekla.fi>
	  David Ofelt <ofelt@getalife.Stanford.EDU>
	  Greg Onufer <Greg.Onufer@eng.sun.com>
	  Marc Paquette <Marc.Paquette@Softimage.com>
	  Thomas A. Peterson <tap@src.honeywell.com>
	  Peter Pezaris <pez@atlantic2.sbi.com>
	  Tibor Polgar <tlp00@eng.amdahl.com>
	  E. Rehmi Post <rehmi@asylum.sf.ca.us>
	  Paul M Reilly <preilly@shr.dec.com>
	  Jack Repenning <jackr@sgi.com>
	  Daniel Rich <drich@sgi.com>
	  Roland Rieke <rol@darmstadt.gmd.de>
	  Russell Ritchie <russell@gssec.bt.co.uk>
	  Mike Russell <mjruss@rchland.vnet.ibm.com>
	  Jan Sandquist <etxquist@iqa.ericsson.se>
	  Marty Sasaki <sasaki@netop3.harvard.edu>
	  Mike Scheidler <c23mts@kocrsv01.delcoelect.com>
	  Darrel Schneider <darrel@slc.com>
	  Hayden Schultz <haydens@ll.mit.edu>
	  Cotton Seed <cottons@mit.edu>
	  John Shen <zfs60@cas.org>
	  Jeffrey Sparkes <jsparkes@bnr.ca>
	  Michael Sperber <sperber@informatik.uni-tuebingen.de>
	  Francois Staes <frans@kiwi.uia.ac.be>
	  Rick Tait <rickt@gnu.ai.mit.edu>
	  James Thompson <thompson@wg2.waii.com>
	  Raymond L. Toy <toy@rtp.ericsson.se>
	  John Turner <turner@xdiv.lanl.gov>
	  Vladimir Vukicevic <vladimir@intrepid.com>
	  Yoav Weiss <yoav@zeus.datasrv.co.il>
	  Rod Whitby <rwhitby@asc.corp.mot.com>
	  Rich Williams <rdw@hplb.hpl.hp.com>
	  Jason Yanowitz <yanowitz@eternity.cs.umass.edu>
	  La Monte Yarroll <piggy@hilbert.maths.utas.edu.au>
	  Blair Zajac <blair@olympia.gps.caltech.edu>
	  Daniel Zivkovic <daniel@canada.sun.com>
	  and the makers of Jolt Cola (tm)")
	  (goto-char (point-min))
	  (while (re-search-forward "^[ \t]*\\([^<>\n]+\\) <[^>\n]+>$"
				    nil t)
	    (set-extent-face (make-extent (match-beginning 1) (match-end 1))
			     'bold))
	  (goto-char (point-min))
	  (while (re-search-forward "^[ \t]*<\\([^>\n]+\\)>$" nil t)
	    (set-extent-face (make-extent (match-beginning 1) (match-end 1))
			     'bold))

	  (goto-char (point-max))
	  (insert "\n")
	  (about-add-mosaic)
	  (goto-char (point-max))
	  (insert "\n\n\tClick ")
	  (about-xref "here" 'about "Return to previous page")
	  (insert " to go back to the previous page.\n")
	  )
	 ((eq xref 'features)
	  (insert "Click ")
	  (about-xref "here" 'about "Return to previous page")
	  (insert " to go back to the previous page\n\n\t")

	  (about-face "New Features in XEmacs" 'bold-italic)

	  (insert "\n
	 * A real toolbar.
	 * Proper integration with Xt and Motif (including Motif menubars
	   and scrollbars).  Motif look-alike menubars and scrollbars
	   are provided for those systems without real Motif support.
	 * Face support on TTY's (color support coming in 19.14).
	 * Horizontal and vertical scrollbars in all windows.
	 * Support for variable-width and variable height fonts.
	 * Support for display on multiple simultaneous X and/or TTY devices.
	 * Support for arbitrary pixmaps in a buffer.
	 * Access to the ToolTalk API.
	 * Support for using XEmacs frames as Xt widgets.
	 * Support for overlapping regions (or extents) and efficient handling
	   of a large number of such extents in a single buffer.
	 * Powerful, flexible control over the display characteristics
	   of most of the visual aspects of XEmacs through the use
	   of specifiers, which allow separate values to be specified
	   for individual buffers, windows, frames, devices, device classes,
	   and device types.
	 * A clean interface to the menubar, window-system events, and key
	   combinations."))
	 ))
      (goto-char (point-min))
      ))))

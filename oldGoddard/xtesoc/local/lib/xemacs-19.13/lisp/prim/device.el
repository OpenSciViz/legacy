;;; extents.el --- miscellaneous device functions not written in C

;;;; Copyright (C) 1994, 1995 Board of Trustees, University of Illinois
;;;; Copyright (C) 1995 Ben Wing

;; Keywords: internal

;; This file is part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the Free
;; Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(defun quit-char (&optional device)
  "Return the character that causes a QUIT to happen.
This is normally C-g.  Optional arg DEVICE specifies the device
that the information is returned for; nil means the current device."
  (nth 3 (current-input-mode device)))

(defun make-tty-device (&optional tty terminal-type)
  "Create a new device on TTY.
  TTY should be the name of a tty device file (e.g. \"/dev/ttyp3\" under
SunOS et al.), as returned by the `tty' command.  A value of nil means
use the stdin and stdout as passed to XEmacs from the shell.
  If TERMINAL-TYPE is non-nil, it should be a string specifying the
type of the terminal attached to the specified tty.  If it is nil,
the terminal type will be inferred from the TERM environment variable."
  (make-device 'tty (list (cons 'tty tty)
			  (cons 'terminal-type terminal-type))))

(defun make-x-device (&optional display argv-list)
  "Create a new device connected to DISPLAY.
Optional argument ARGV-LIST is a list of strings describing command line
options."
  (make-device 'x (list (cons 'display display)
			(cons 'argv-list argv-list))))


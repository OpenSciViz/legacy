;;; generic-win.el --- generic window function initializtion
;; Copyright (C) 1990, 1993, 1994 Free Software Foundation, Inc.

;; Author: FSF
;; Keywords: terminals

;;; This file is part of XEmacs.
;;;
;;; XEmacs is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2, or (at your
;;; option) any later version.
;;;
;;; XEmacs is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with XEmacs; see the file COPYING.  If not, write to the
;;; Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

(require 'frame)

;; Keypad type things

;; this is so that where-is says beginning-of-buffer is M-< instead of f27.
(fset 'fkey-beginning-of-buffer 'beginning-of-buffer)
(fset 'fkey-end-of-buffer	'end-of-buffer)
(fset 'fkey-scroll-down		'scroll-down)
(fset 'fkey-scroll-up		'scroll-up)
(fset 'fkey-scroll-left		'scroll-left)
(fset 'fkey-scroll-right	'scroll-right)
(fset 'fkey-scroll-other-window 'scroll-other-window)
(fset 'fkey-backward-char	'backward-char)
(fset 'fkey-forward-char	'forward-char)
(fset 'fkey-backward-word	'backward-word)
(fset 'fkey-forward-word	'forward-word)
(fset 'fkey-backward-paragraph	'backward-paragraph)
(fset 'fkey-forward-paragraph	'forward-paragraph)
(fset 'fkey-backward-sexp	'backward-sexp)
(fset 'fkey-forward-sexp	'forward-sexp)
(fset 'fkey-other-window	'other-window)
(fset 'fkey-backward-other-window 'backward-other-window)
(fset 'fkey-beginning-of-line	'beginning-of-line)
(fset 'fkey-end-of-line		'end-of-line)
(fset 'fkey-repeat-complex-command 'repeat-complex-command)
(fset 'fkey-overwrite-mode	'overwrite-mode)

;; these two have to be defined in this more complicated way to make
;; current-column-tracking work, sigh...
(defun fkey-previous-line (p)
  (interactive "_p")
  (setq this-command 'previous-line)
  (previous-line p))
(defun fkey-next-line (p)
  (interactive "_p")
  (setq this-command 'next-line)
  (next-line p))

;;; These aren't bound to kbd macros like "\C-b" so that they have the
;; expected behavior even in, for example, vi-mode.

;; We use here symbolic names, assuming that the corresponding keys will
;; generate these keysyms.  This is not true on Suns, but x-win-sun.el 
;; fixes that.  If it turns out that the semantics of these keys should
;; differ from server to server, this should be moved into server-specific
;; files, but these appear to be the standard Motif and PC bindings.

;; movement by units
(define-key global-map 'left		'fkey-backward-char)
(define-key global-map 'up		'fkey-previous-line)
(define-key global-map 'right		'fkey-forward-char)
(define-key global-map 'down		'fkey-next-line)

;; movement by pages
(define-key global-map 'prior		'fkey-scroll-down)
(define-key global-map 'next		'fkey-scroll-up)

;; movement to the limits
(define-key global-map 'home		'fkey-beginning-of-line)
(define-key global-map 'end		'fkey-end-of-line)

;;; Miscellaneous key bindings
(define-key global-map 'again		'fkey-repeat-complex-command)
(define-key global-map 'insert		'fkey-overwrite-mode)

;;; generic-win.el ends here

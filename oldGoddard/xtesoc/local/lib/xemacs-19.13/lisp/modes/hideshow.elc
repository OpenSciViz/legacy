;ELC   
;;; compiled by cthomp@willow.cs.uiuc.edu on Tue Aug 29 14:07:45 1995
;;; from file /xemacs/xemacs-19.13-release/editor/lisp/modes/hideshow.el
;;; emacs version 19.13 XEmacs Lucid.
;;; bytecomp version 2.25; 1-Sep-94.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19")))
    (error "This file was compiled for Emacs 19."))

(defvar hs-unbalance-handler-method 'top-level "\
*Symbol representing how \"unbalanced parentheses\" should be handled.
This error is usually signalled by hs-show-block.  One of four values:
`top-level', `next-line', `signal' or `ignore'.  Default is `top-level'.

- `top-level' -- Show top-level block containing the currently troublesome
block.
- `next-line' -- Use the fact that, for an already hidden block, its end
will be on the next line.  Attempt to show this block.
- `signal' -- Pass the error through, stopping execution.
- `ignore' -- Ignore the error, continuing execution.

Values other than these four will be interpreted as `signal'.")
(defvar hs-special-modes-alist '((c-mode "{" "}") (c++-mode "{" "}")) "\
*Alist of the form (MODE START-RE END-RE FORWARD-SEXP-FUNC).
If present, hideshow will use these values for the start and end regexps,
respectively.  Since Algol-ish languages do not have single-character
block delimiters, the function `forward-sexp' which is used by hideshow
doesn't work.  In this case, if a similar function is provided, you can
register it and have hideshow use it instead of `forward-sexp'.  To add
more values, use

	(pushnew '(new-mode st-re end-re function-name)
		hs-special-modes-alist :test 'equal)

For example:

	(pushnew '(simula-mode \"begin\" \"end\" simula-next-statement)
		hs-special-modes-alist :test 'equal)

Note that the regexps should not contain leading or trailing whitespace.")
(defvar hs-hide-hooks nil "\
*Hooks called at the end of hs-hide-all and hs-hide-block.")
(defvar hs-show-hooks nil "\
*Hooks called at the end of hs-show-all, hs-show-block and hs-show-region.")
(defvar hs-minor-mode-prefix "" "\
*Prefix key to use for hideshow commands in hideshow minor mode.")
(defvar hs-minor-mode nil "\
Non-nil if using hideshow mode as a minor mode of some other mode.
Use the command `hs-minor-mode' to toggle this variable.")
(defvar hs-minor-mode-map nil "\
Mode map for hideshow minor mode.")
(defvar hs-menu-bar nil "\
Menu bar for hideshow minor mode (Xemacs only).")
(defvar hs-c-start-regexp nil "\
Regexp for beginning of comments.  Buffer-local.
Differs from mode-specific comment regexps in that surrounding
whitespace is stripped.")
(defvar hs-c-end-regexp nil "\
Regexp for end of comments.  Buffer-local.
See `hs-c-start-regexp'.")
(defvar hs-block-start-regexp nil "\
Regexp for beginning of block.  Buffer-local.")
(defvar hs-block-end-regexp nil "\
Regexp for end of block.  Buffer-local.")
(defvar hs-forward-sexp-func 'forward-sexp "\
Function used to do a forward-sexp.  Should change for Algol-ish modes.
For single-character block delimiters -- ie, the syntax table regexp for the
character is either `(' or `)' -- `hs-forward-sexp-func' would just be
`forward-sexp'.  For other modes such as simula, a more specialized function
is necessary.")
(defvar hs-emacs-type 'fsf "\
Used to support both FSF Emacs and Xemacs.")
(fset 'hs-flag-region #[(from to flag) "� �Ď��U��ʪ���%+�" [buffer-modified-p nil buffer-read-only modp ((set-buffer-modified-p modp)) subst-char-in-region from to flag 10 13 t] 6 "\
Hides or shows lines from FROM to TO, according to FLAG.
If FLAG is \\n (newline character) then text is shown, while if FLAG
is \\^M (control-M) the text is hidden."])
(fset 'hs-hide-block-at-point #[(&optional end) "�	!��`�!�`�y���\n`W���\n\"�V���\n`�#�����\nb*�" [looking-at hs-block-start-regexp p hs-forward-sexp-func 1 q -1 nil count-lines hs-flag-region 13 end] 4 "\
Hide block iff on block beginning, optional END means reposition at end."])
(fset 'hs-show-block-at-point #[(&optional end) "�	!��`��ŏ�\n�#�	��`T��\nb*�" [looking-at hs-block-start-regexp p error (byte-code "�!�`�" [hs-forward-sexp-func 1] 2) ((error (byte-code "�=��`��=��i�U��� �`��Pd��$�`��=���y�`��`��@A\"�" [hs-unbalance-handler-method ignore top-level 0 beginning-of-defun p re-search-forward "^" hs-block-start-regexp t 2 next-line signal error] 5))) q hs-flag-region 10 end] 4 "\
Show block iff on block beginning.  Optional END means reposition at end."])
(fset 'hs-safety-is-job-n #[nil "�\n�����	�P	G�U?����	\"���!)�" ["" str selective-display "selective-display nil " selective-display-ellipses "selective-display-ellipses nil" 0 message "warning: %s" sit-for 2] 3 "\
Warns if selective-display or selective-display-ellipses is nil."])
(fset 'hs-inside-comment-p #[nil "�`�Ę����y��\n�#�����\n�#?)���d�#�`��!��d�#�`GZ\nW��Ɣ	D+�" [nil q p comment-end "" found 0 re-search-forward hs-c-start-regexp t search-forward "\"" hs-c-end-regexp 1 forward-comment -1 comment-start] 5 "\
Returns non-nil if point is inside a comment, otherwise nil.
Actually, for multi-line-able comments, returns a list containing
the buffer position of the start and the end of the comment."])
(fset 'hs-grok-mode-type #[nil "��!����!���	!��\"��ǉ�SO\nȘ��ɪ��\n!\n��\n\"��\nǕ�O\n�\"�A@����8����8��׉)�" [boundp comment-start comment-end regexp-quote hs-c-start-regexp string-match " +$" 0 "" "\n" hs-c-end-regexp "^ +" nil assoc major-mode hs-special-modes-alist lookup "\\s(" hs-block-start-regexp 2 "\\s)" hs-block-end-regexp 3 forward-sexp hs-forward-sexp-func] 4 "\
Setup variables for new buffers where applicable."])
(fset 'hs-find-block-beginning #[nil "�`�\n�Ű���e�#��˔��˔��k̕b��!��^��b�+�" [nil "\\(" hs-block-start-regexp "\\)\\|\\(" hs-block-end-regexp "\\)" both-regexps here done re-search-backward t 1 2 hs-forward-sexp-func -1] 8 "\
Repositions point at block-start.  Return point, or nil if top-level."])
(fset 'hs-life-goes-on '(macro . #[(&rest body) "���BE�" [if hs-minor-mode progn body] 4 "\
Executes optional BODY iff variable `hs-minor-mode' is non-nil."]))
(fset 'hs-hide-all #[nil "����!���ed�#�eb���P	�� !��d�#��Ŕb���!���	T�	\"��_*� �)�y���!���!�" [hs-minor-mode message "hiding all blocks ..." hs-flag-region 10 0 "^" hs-block-start-regexp top-level-re count forward-comment buffer-size re-search-forward t hs-hide-block-at-point "hiding ... %d" hs-safety-is-job-n "hiding all blocks ... done" run-hooks hs-hide-hooks] 4 "\
Hides all top-level blocks, displaying only first and last lines.
When done, point is repositioned at the beginning of the line, and
hs-hide-hooks is called.  See documentation for `run-hooks'." nil])
(fset 'hs-show-all #[nil "����!��ed�#���!���!�" [hs-minor-mode message "showing all blocks ..." hs-flag-region 10 "showing all blocks ... done" run-hooks hs-show-hooks] 4 "\
Shows all top-level blocks.
When done, point is unchanged, and hs-show-hooks is called.  See
documentation for `run-hooks'." nil])
(fset 'hs-hide-block #[(&optional end) "��� ���Ę����!���\n@\nA@\"�W����!��\nA@b��y��\n@`�#���\nA@��\n@b�� ���!���!��� ���!�� ���!)�" [hs-minor-mode hs-inside-comment-p c-reg comment-end "" message "can't hide a single-line comment" count-lines 2 "not enougn comment lines to hide" -1 hs-flag-region 13 end hs-safety-is-job-n run-hooks hs-hide-hooks looking-at hs-block-start-regexp hs-find-block-beginning hs-hide-block-at-point] 5 "\
Selects a block and hides it.  With prefix arg, reposition at end.
Block is defined as a sexp for lispish modes, mode-specific otherwise.
Comments are blocks, too.  Upon completion, point is at repositioned and
hs-hide-hooks is called.  See documentation for `run-hooks'." "P"])
(fset 'hs-show-block #[(&optional end) "��� ���Ę����!���\n@\nA@�#�	��\nA@��\n@b���!��� ���	!�� ���!)�" [hs-minor-mode hs-inside-comment-p c-reg comment-end "" message "already looking at the entire comment" hs-flag-region 10 end looking-at hs-block-start-regexp hs-find-block-beginning hs-show-block-at-point hs-safety-is-job-n run-hooks hs-show-hooks] 5 "\
Selects a block and shows it.  With prefix arg, reposition at end.
Upon completion, point is repositioned hs-show-hooks are called.  See
documetation for `hs-hide-block' and `run-hooks'." "P"])
(fset 'hs-show-region #[(beg end) "���\n�#�� ���!�" [hs-minor-mode hs-flag-region beg end 10 hs-safety-is-job-n run-hooks hs-show-hooks] 4 "\
Shows all lines from BEG to END, without doing any block analysis.
Note: hs-show-region is intended for use when when hs-show-block signals
`unbalanced parentheses' and so is an emergency measure only.  You may
become very confused if you use this command indiscriminately." "r"])
(fset 'hs-minor-mode #[(&optional arg) "��	?���!�V����=����!!���@A#���� ���!��=����\"!���!���!�" [arg hs-minor-mode prefix-numeric-value 0 hs-emacs-type lucid set-buffer-menubar copy-sequence current-menubar add-menu nil hs-menu-bar t selective-display selective-display-ellipses hs-grok-mode-type run-hooks hs-minor-mode-hook delete kill-local-variable] 5 "\
Toggle hideshow minor mode.
With ARG, turn hideshow minor mode on if ARG is positive, off otherwise.
When hideshow minor mode is on, the menu bar is augmented with hideshow
commands and the hideshow commands are enabled.  The variables

	selective-display
	selective-display-ellipses

are set to t.  Lastly, the hooks set in hs-minor-mode-hook are called.
See documentation for `run-hooks'.

Turning hideshow minor mode off reverts the menu bar and the
variables to default values and disables the hideshow commands." "P"])
(byte-code "��\n\"��ê����� ���=���	�������!B#����#����#����#����#����#������BB������C\"��!���!���!���!���!���!�����#�����#�����#�����#�����#�����#���!�" [string-match "XEmacs\\|Lucid" emacs-version lucid fsf hs-emacs-type hs-minor-mode-map make-sparse-keymap ("hideshow" ["Hide Block" hs-hide-block t] ["Show Block" hs-show-block t] ["Hide All" hs-hide-all t] ["Show All" hs-show-all t] ["Show Region" hs-show-region t]) hs-menu-bar define-key [menu-bar hideshow] "hideshow" [menu-bar hideshow hs-show-region] ("Show Region" . hs-show-region) [menu-bar hideshow hs-show-all] ("Show All" . hs-show-all) [menu-bar hideshow hs-hide-all] ("Hide All" . hs-hide-all) [menu-bar hideshow hs-show-block] ("Show Block" . hs-show-block) [menu-bar hideshow hs-hide-block] ("Hide Block" . hs-hide-block) hs-minor-mode minor-mode-map-alist minor-mode-alist append (hs-minor-mode " hs") make-variable-buffer-local hs-c-start-regexp hs-c-end-regexp hs-block-start-regexp hs-block-end-regexp hs-forward-sexp-func put permanent-local t provide hideshow] 6)

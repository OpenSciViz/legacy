;ELC   
;;; compiled by cthomp@willow.cs.uiuc.edu on Tue Aug 29 14:26:07 1995
;;; from file /xemacs/xemacs-19.13-release/editor/lisp/utils/elp.el
;;; emacs version 19.13 XEmacs Lucid.
;;; bytecomp version 2.25; 1-Sep-94.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19")))
    (error "This file was compiled for Emacs 19."))

(defvar elp-function-list nil "\
*List of functions to profile.
Used by the command `elp-instrument-list'.")
(defvar elp-reset-after-results t "\
*Non-nil means reset all profiling info after results are displayed.
Results are displayed with the `elp-results' command.")
(defvar elp-sort-by-function 'elp-sort-by-total-time "\
*Non-nil specifies elp results sorting function.
These functions are currently available:

  elp-sort-by-call-count   -- sort by the highest call count
  elp-sort-by-total-time   -- sort by the highest total time
  elp-sort-by-average-time -- sort by the highest average times

You can write you're own sort function. It should adhere to the
interface specified by the PRED argument for the `sort' defun.  Each
\"element of LIST\" is really a 4 element vector where element 0 is
the call count, element 1 is the total time spent in the function,
element 2 is the average time spent in the function, and element 3 is
the symbol's name string.")
(defvar elp-report-limit 1 "\
*Prevents some functions from being displayed in the results buffer.
If a number, no function that has been called fewer than that number
of times will be displayed in the output buffer.  If nil, all
functions will be displayed.")
(defvar elp-use-standard-output nil "\
*Non-nil says to output to `standard-output' instead of a buffer.")
(defvar elp-recycle-buffers-p t "\
*Nil says to not recycle the `elp-results-buffer'.
In other words, a new unique buffer is create every time you run
\\[elp-results].")
(defconst elp-version "2.32" "\
ELP version number.")
(defconst elp-help-address "tools-help@merlin.cnri.reston.va.us" "\
Address accepting submissions of bug reports and questions.")
(defvar elp-results-buffer "*ELP Profiling Results*" "\
Buffer name for outputting profiling results.")
(defconst elp-timer-info-property 'elp-info "\
ELP information property name.")
(defvar elp-all-instrumented-list nil "\
List of all functions currently being instrumented.")
(defvar elp-record-p t "\
Controls whether functions should record times or not.
This variable is set by the master function.")
(defvar elp-master nil "\
Master function symbol.")
(fset 'elp-instrument-function #[(funsym) "	N����\"�K�Ɖ#�	��=����\"��!���	�\"	�	��D���!??E�FC\"	�	#�	M�>��B�+�" [funsym elp-timer-info-property error "Symbol `%s' is already instrumented for profiling." funguts vector 0 infovec (lambda (&rest args)) newguts macro "ELP cannot profile macro %s" commandp append ((interactive)) elp-wrapper quote and (interactive-p) args put elp-all-instrumented-list] 8 "\
Instrument FUNSYM for profiling.
FUNSYM must be a symbol of a defined function." "aFunction to instrument: "])
(fset 'elp-restore-function #[(funsym) "	N�\"=�����	�#�\n���K���\n�HM)�" [funsym elp-timer-info-property info delq elp-all-instrumented-list elp-master nil t elp-record-p put elp-wrapper 2] 4 "\
Restore an instrumented function to its original definition.
Argument FUNSYM is the symbol of a defined function." "aFunction to restore: "])
(fset 'elp-instrument-list #[(&optional list) "��	��\")�" [list elp-function-list mapcar #[(funsym) "���" [nil (elp-instrument-function funsym) ((error))] 3]] 3 "\
Instrument for profiling, all functions in `elp-function-list'.
Use optional LIST if provided instead." "PList of functions to instrument: "])
(fset 'elp-instrument-package #[(prefix) "�����#\"!�" [elp-instrument-list mapcar intern all-completions prefix obarray #[(sym) "�	!��	K��=?�" [fboundp sym macro] 2]] 7 "\
Instrument for profiling, all functions which start with PREFIX.
For example, to instrument all ELP functions, do the following:

    \\[elp-instrument-package] RET elp- RET" "sPrefix of package to instrument: "])
(fset 'elp-restore-list #[(&optional list) "��	��\")�" [list elp-function-list mapcar elp-restore-function] 3 "\
Restore the original definitions for all functions in `elp-function-list'.
Use optional LIST if provided instead." "PList of functions to restore: "])
(fset 'elp-restore-all #[nil "�	!�" [elp-restore-list elp-all-instrumented-list] 2 "\
Restores the original definitions of all functions being profiled." nil])
(fset 'elp-reset-function #[(funsym) "	N�����\"�\nŉI�\n��I)�" [funsym elp-timer-info-property info error "%s is not instrumented for profiling." 0 1 0.0] 4 "\
Reset the profiling information for FUNSYM." "aFunction to reset: "])
(fset 'elp-reset-list #[(&optional list) "��	��\")�" [list elp-function-list mapcar elp-reset-function] 3 "\
Reset the profiling information for all functions in `elp-function-list'.
Use optional LIST if provided instead." "PList of functions to reset: "])
(fset 'elp-reset-all #[nil "�	!�" [elp-reset-list elp-all-instrumented-list] 2 "\
Reset the profiling information for all functions being profiled." nil])
(fset 'elp-set-master #[(funsym) "�>���!�" [funsym elp-master nil elp-record-p elp-all-instrumented-list elp-instrument-function] 2 "\
Set the master function for profiling." "aMaster function: "])
(fset 'elp-unset-master #[nil "��" [nil elp-master t elp-record-p] 2 "\
Unsets the master function." nil])
(byte-code "��M���N�>�����\"�����#�" [elp-get-time #[nil "� �	A@!��	8!ĥ\\)�" [current-time now float 2 1000000.0] 4] byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand] 4)
(fset 'elp-wrapper #[(funsym interactive-p args) "��	=���	N��H�	����	\"������!���\"	��� �A@!��8!ӥ\\)��HTI����!���\"	��H� �A@!��8!ӥ\\)Z\\I�)��	=���	+�" [elp-master funsym t elp-record-p elp-timer-info-property info 2 func nil result error "%s is not instrumented for profiling." interactive-p call-interactively apply args current-time now float 1000000.0 enter-time 0 1] 8 "\
This function has been instrumented for profiling by the ELP.
ELP is the Emacs Lisp Profiler.  To restore the function to its
original definition, use \\[elp-restore-function] or \\[elp-restore-all]."])
(byte-code "��!�����!�����!�����!�����M���M���M���M���N�>�����\"�����#���M�" [boundp elp-field-len nil elp-cc-len elp-at-len elp-et-len elp-sort-by-call-count #[(vec1 vec2) "�H\n�HY�" [vec1 0 vec2] 3] elp-sort-by-total-time #[(vec1 vec2) "�H\n�HY�" [vec1 1 vec2] 3] elp-sort-by-average-time #[(vec1 vec2) "�H\n�HY�" [vec1 2 vec2] 3] elp-pack-number #[(number width) "G	X�����\"��ĔĕO�	ƕZƔ[Z�ZO�ƔƕOQ��	O��" [number width string-match "^\\(.*\\)\\(e[+-].*\\)$" 1 0 2 3 "..."] 5] byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand elp-output-result #[(resultvec) "�H�H�H�H�\n���\n!\n�!�!�����\nW?�� c���G[�\\\\\"�\nc���\nG[�\\\\\"��GX������\"��ÔÕO�ŕZŔ[Z�ZO�ŔŕOQ���O�*�GX������\"��ÔÕO�ŕZŔ[Z�ZO�ŔŕOQ���O�*�c���G[�\\\\\"�c�*�c.�" [resultvec 0 cc 1 tt 2 at 3 symname nil callcnt totaltime avetime number-to-string elp-report-limit insert-char 32 elp-field-len elp-cc-len elp-et-len width number string-match "^\\(.*\\)\\(e[+-].*\\)$" "..." elp-at-len atstr ttstr "\n"] 7]] 4)
(fset 'elp-results #[nil "p���\n!���\n!q�� �� ��	��G���G��G��G��\"c�	V����	Z\"�	���ޱ���\"��c���\"��c���\"��c���\"��c� ��� \"��\"�.q��!�$��%���ed{!�'��� *�" [elp-recycle-buffers-p get-buffer-create elp-results-buffer generate-new-buffer resultsbuf curbuf erase-buffer beginning-of-buffer 0 longest "Function Name" title titlelen elp-field-len "Call Count" cc-header elp-cc-len "Elapsed Time" et-header elp-et-len "Average Time" at-header elp-at-len mapcar #[(funsym) "	N��\"\n�H\n�H	\n������G]�	�U��ͪ��	!�!�$,�" [funsym elp-timer-info-property info format "%s" symname 0 cc 1 tt "No profiling information found for: " longest vector 0.0 float] 6] elp-all-instrumented-list resvec insert-char 32 "  " "\n" 61 elp-sort-by-function sort elp-output-result pop-to-buffer elp-use-standard-output noninteractive princ elp-reset-after-results elp-reset-all] 8 "\
Display current profiling results.
If `elp-reset-after-results' is non-nil, then current profiling
information for all instrumented functions are reset after results are
displayed." nil])
(fset 'elp-submit-bug-report #[nil "��!����!����P�#�" [y-or-n-p "Do you want to submit a report on elp? " require reporter reporter-submit-bug-report elp-help-address "elp " elp-version (elp-report-limit elp-reset-after-results elp-sort-by-function)] 4 "\
Submit via mail, a bug report on elp." nil])
(provide 'elp)

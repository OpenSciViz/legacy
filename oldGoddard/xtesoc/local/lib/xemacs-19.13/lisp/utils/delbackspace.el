;;; delbackspace.el --- rebind backspace and delete to be correct

;; Copyright (C) 1994 Free Software Foundation, Inc.
;; Copyright (C) 1995 Amdahl Corporation.
;;
;; Author: Tibor Polgar??
;; Maintainer: Ben Wing <wing@netcom.com>
;; Keywords: terminals
;;

;; this is a hack that will have to do until function-key-map is implemented
(defconst delbackspace-backspace
  (if (not (eq 'tty (device-type (selected-device))))
      'backspace
    "\C-h"))

(global-unset-key 'delete)
(global-unset-key delbackspace-backspace)
(local-unset-key 'delete)
(local-unset-key delbackspace-backspace)
(global-unset-key [(control delete)] )
(global-unset-key [(meta delete)] )
(global-unset-key [(meta shift delete)] )
(global-unset-key [(control backspace)] )
(global-unset-key [(meta backspace)] )
(global-unset-key [(meta shift backspace)] )

(if (eq 'tty (device-type (selected-device)))
    (progn
      (setq help-char '(meta h))
      (define-key global-map "\M-h" 'help-command)))

(define-key global-map 'delete 'delete-char)
(define-key lisp-mode-map 'delete 'delete-char)
(define-key emacs-lisp-mode-map 'delete 'delete-char)
(define-key c-mode-map 'delete 'delete-char)
(define-key c++-mode-map 'delete 'delete-char)
(define-key text-mode-map 'delete 'delete-char)
(define-key indented-text-mode-map 'delete 'delete-char)
(define-key minibuffer-local-map 'delete 'delete-char)

(define-key global-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key lisp-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key emacs-lisp-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key c-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key c++-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key isearch-mode-map delbackspace-backspace 'isearch-delete-char)
(define-key text-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key indented-text-mode-map delbackspace-backspace 'backward-delete-char-untabify)
(define-key minibuffer-local-map delbackspace-backspace 'backward-delete-char-untabify)
(and (boundp 'mh-folder-mode-map)
     (define-key mh-folder-mode-map delbackspace-backspace 'mh-previous-page))

(global-set-key [(control backspace)] 'backward-kill-line)     ; (ctrl) backspace to beginning of line
(global-set-key [(meta backspace)] 'backward-kill-word)         ; (meta) backspace word before cursor
(define-key c-mode-map [(meta backspace)] 'backward-kill-word)      
(define-key c++-mode-map [(meta backspace)] 'backward-kill-word)    
(global-set-key [(symbol backspace)] 'backward-kill-sentence)   ; (alt) backspace sentence before cursor
(define-key c-mode-map [(symbol backspace)] 'backward-kill-sexp)         
(define-key c++-mode-map [(symbol backspace)] 'backward-kill-sexp)       
(define-key emacs-lisp-mode-map [(symbol backspace)] 'backward-kill-sexp)
(define-key lisp-mode-map [(symbol backspace)] 'backward-kill-sexp)      
(define-key minibuffer-local-map [(symbol backspace)] 'backward-kill-sexp)
(global-set-key [(meta shift backspace)] 'backward-kill-paragraph)   ; (meta shift) backspace paragraph before cursor

(global-set-key [(control delete)] 'kill-line)       ; (alt) delete rest of line  (erase EOF)
(global-set-key [(meta delete)] 'kill-word)           ; (meta) delete next word
(define-key c-mode-map [(meta delete)] 'kill-word)  
(define-key c++-mode-map [(meta delete)] 'kill-word)
(global-set-key [(symbol delete)] 'kill-sentence)     ; (alt) delete next sentence
(define-key c-mode-map [(symbol delete)] 'kill-sexp)         ; (alt) delete expression forward
(define-key c++-mode-map [(symbol delete)] 'kill-sexp)        
(define-key emacs-lisp-mode-map [(symbol delete)] 'kill-sexp)
(define-key lisp-mode-map [(symbol delete)] 'kill-sexp)      
(define-key minibuffer-local-map [(symbol delete)] 'kill-sexp)
(global-set-key [(meta shift delete)] 'kill-paragraph)     ; (meta shift) delete next paragraph


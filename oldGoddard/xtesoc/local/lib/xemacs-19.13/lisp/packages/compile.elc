;ELC   
;;; compiled by cthomp@willow.cs.uiuc.edu on Tue Aug 29 14:11:52 1995
;;; from file /xemacs/xemacs-19.13-release/editor/lisp/packages/compile.el
;;; emacs version 19.13 XEmacs Lucid.
;;; bytecomp version 2.25; 1-Sep-94.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19")))
    (error "This file was compiled for Emacs 19."))

(defvar compilation-mode-hook nil "\
*List of hook functions run by `compilation-mode' (see `run-hooks').")
(defvar compilation-window-height nil "\
*Number of lines in a compilation window.  If nil, use Emacs default.")
(defvar compilation-error-list 'invalid "\
List of error message descriptors for visiting erring functions.
Each error descriptor is a cons (or nil).  Its car is a marker pointing to
an error message.  If its cdr is a marker, it points to the text of the
line the message is about.  If its cdr is a cons, it is a list
((DIRECTORY . FILE) LINE [COLUMN]).  Or its cdr may be nil if that
error is not interesting.

The value may be t instead of a list; this means that the buffer of
error messages should be reparsed the next time the list of errors is wanted.

Some other commands (like `diff') use this list to control the error
message tracking facilites; if you change its structure, you should make
sure you also change those packages.  Perhaps it is better not to change
it at all.")
(defvar compilation-old-error-list nil "\
Value of `compilation-error-list' after errors were parsed.")
(defvar compilation-parse-errors-function 'compilation-parse-errors "\
Function to call to parse error messages from a compilation.
It takes args LIMIT-SEARCH and FIND-AT-LEAST.
If LIMIT-SEARCH is non-nil, don't bother parsing past that location.
If FIND-AT-LEAST is non-nil, don't bother parsing after finding that 
many new errors.
It should read in the source files which have errors and set
`compilation-error-list' to a list with an element for each error message
found.  See that variable for more info.")
(defvar compilation-buffer-name-function nil "\
Function to compute the name of a compilation buffer.
The function receives one argument, the name of the major mode of the
compilation buffer.  It should return a string.
nil means compute the name with `(concat \"*\" (downcase major-mode) \"*\")'.")
(defvar compilation-finish-function nil "\
*Function to call when a compilation process finishes.
It is called with two arguments: the compilation buffer, and a string
describing how the process finished.")
(defvar compilation-last-buffer nil "\
The most recent compilation buffer.
A buffer becomes most recent when its compilation is started
or when it is used with \\[next-error] or \\[compile-goto-error].")
(defvar compilation-in-progress nil "\
List of compilation processes now running.")
(byte-code "�	����	B��" [compilation-in-progress minor-mode-alist (compilation-in-progress " Compiling")] 2)
(defvar compilation-parsing-end nil "\
Position of end of buffer when last error messages were parsed.")
(defvar compilation-error-message "No more errors" "\
Message to print when no more matches are found.")
(defvar compilation-error-regexp-alist '(("\n\\([^:( 	\n]+\\)[:(][ 	]*\\([0-9]+\\)\\([) 	]\\|:[^0-9\n]\\)" 1 2) ("\n\\([^:( 	\n]+\\):\\([^:( 	\n]+\\):[ 	]*\\([0-9]+\\)\\(:[^0-9\n]\\)" 2 3) ("\n\\(Error\\|Warning\\) \\([^:( 	\n]+\\) \\([0-9]+\\)\\([) 	]\\|:[^0-9\n]\\)" 2 3) ("[ 	:]\\([^:( 	\n]+\\)[:(](+[ 	]*\\([0-9]+\\))[:) 	]*$" 1 2) ("([ 	]*\\([^:( 	\n]+\\)[:(][ 	]*\\([0-9]+\\))" 1 2) ("\nfort: [^:\n]*: \\([^ \n]*\\), line \\([0-9]+\\):" 1 2) ("\\(\n\\|on \\)[Ll]ine[ 	]+\\([0-9]+\\)[ 	]+of[ 	]+\"?\\([^\":\n]+\\)\"?:" 3 2) ("\"\\([^,\" \n	]+\\)\", lines? \\([0-9]+\\)[:., (-]" 1 2) ("rror: \\([^,\" \n	]+\\)[,:] \\(line \\)?\\([0-9]+\\):" 1 3) ("in line \\([0-9]+\\) of file \\([^ \n]+[^. \n]\\)\\.? " 2 1) ("\n[EW], \\([^(\n]*\\)(\\([0-9]+\\),[ 	]*\\([0-9]+\\)" 1 2 3) ("\n\\([^ \n	:]+\\):\\([0-9]+\\):\\([0-9]+\\)[: 	]" 1 2 3) ("\n\\([^ \n	:]+\\):\\([^ \n	:]+\\):\\([0-9]+\\):\\([0-9]+\\)[: 	]" 2 3 4)) "\
Alist that specifies how to match errors in compiler output.
Each element has the form (REGEXP FILE-IDX LINE-IDX [COLUMN-IDX]).
If REGEXP matches, the FILE-IDX'th subexpression gives the file name, and
the LINE-IDX'th subexpression gives the line number. If COLUMN-IDX is
given, the COLUMN-IDX'th subexpression gives the column number on that line.")
(defvar compilation-read-command t "\
If not nil, M-x compile reads the compilation command to use.
Otherwise, M-x compile just uses the value of `compile-command'.")
(defvar compilation-ask-about-save t "\
If not nil, M-x compile asks which buffers to save before compiling.
Otherwise, it saves all modified buffers without asking.")
(defvar grep-regexp-alist '(("^\\([^:( 	\n]+\\)[:( 	]+\\([0-9]+\\)[:) 	]" 1 2)) "\
Regexp used to match grep hits.  See `compilation-error-regexp-alist'.")
(defvar grep-command "grep -n " "\
Last grep command used in \\[grep]; default for next grep.")
(defvar compilation-search-path '(nil) "\
*List of directories to search for source files named in error messages.
Elements should be directory names, not file names of directories.
nil as an element means to try the default directory.")
(defvar compile-command "make -k " "\
Last shell command used to do a compilation; default for next compilation.

Sometimes it is useful for files to supply local values for this variable.
You might also use mode hooks to specify it in certain modes, like this:

    (setq c-mode-hook
      '(lambda () (or (file-exists-p \"makefile\") (file-exists-p \"Makefile\")
		      (progn (make-local-variable 'compile-command)
			     (setq compile-command
				    (concat \"make -k \"
					    buffer-file-name))))))")
(defvar compilation-enter-directory-regexp ": Entering directory `\\(.*\\)'$" "\
Regular expression matching lines that indicate a new current directory.
This must contain one \\(, \\) pair around the directory name.

The default value matches lines printed by the `-w' option of GNU Make.")
(defvar compilation-leave-directory-regexp ": Leaving directory `\\(.*\\)'$" "\
Regular expression matching lines that indicate restoring current directory.
This may contain one \\(, \\) pair around the name of the directory
being moved from.  If it does not, the last directory entered (by a
line matching `compilation-enter-directory-regexp') is assumed.

The default value matches lines printed by the `-w' option of GNU Make.")
(defvar compilation-directory-stack nil "\
Stack of previous directories for `compilation-leave-directory-regexp'.
The head element is the directory the compilation was started in.")
(byte-code "��!�����!���" [boundp compile-history nil grep-history] 2)
(fset 'compile #[(command) "�?�\"��	�\"�" [command compile-command save-some-buffers compilation-ask-about-save nil compile-internal "No more errors"] 3 "\
Compile the program including the current buffer.  Default: run `make'.
Runs COMMAND, a shell command, in a separate process asynchronously
with output going to the buffer `*compilation*'.

You can then use the command \\[next-error] to find the next error message
and move to the source code that caused it.

To run more than one compilation at once, start one and rename the
`*compilation*' buffer to some other name with \\[rename-buffer].
Then start the next one.

The name used for the buffer is actually whatever is returned by
the function in `compilation-buffer-name-function', so you can set that
to a function that generates a unique name." (byte-code "����@���Ū��#C�C�" [compilation-read-command read-shell-command "Compile command: " compile-command compile-history (compile-history . 1)] 5)])
(fset 'recompile #[nil "�	?�\"���\"�" [save-some-buffers compilation-ask-about-save nil compile-internal compile-command "No more errors"] 3 "\
Re-compile the program including the current buffer." nil])
(fset 'grep #[(command-args) "�	�P���%�" [compile-internal command-args " /dev/null" "No more grep hits" "grep" nil grep-regexp-alist] 6 "\
Run grep, with user-specified args, and collect output in a buffer.
While grep runs asynchronously, you can use the \\[next-error] command
to find the text that grep hits refer to.

This command uses a special history list for its arguments, so you can
easily repeat a grep command." (byte-code "��\n�#C�" [read-shell-command "Run grep (like this): " grep-command grep-history] 4)])
(fset 'compile-internal #[(command error-message &optional name-of-mode parser regexp-alist name-function) "��\n���������\n!!�q��p!�	���	!�=�����\n\"!����Џ������ \"�)� �)����p��	!ގ	q��� ���#����!�勈� ��p!���!#L���!L���!+L���!L��C,�e\"�\n.�!���!���.�	##3�3�\"��3�\"��3!`	��39B9)�����#\"!��=�	��#&����#\"!�.	�@)�" [nil outbuf name-of-mode "Compilation" get-buffer-create name-function compilation-buffer-name-function #[(mode) "�	��Q�" ["*" mode] 3] get-buffer-process comp-proc process-status run yes-or-no-p format "A %s process is running; kill it? " (byte-code "�	!���!��	!�" [interrupt-process comp-proc sit-for 1 delete-process] 2) ((error)) error "Cannot have two processes in `%s' at once" buffer-name kill-all-local-variables regexp-alist compilation-error-regexp-alist parser compilation-parse-errors-function default-directory outwin buffer-save thisdir display-buffer ((set-buffer buffer-save)) buffer-read-only erase-buffer "cd " "\n" command set-buffer-modified-p ((byte-code "�	!�db�" [select-window outwin] 2)) compilation-mode buffer-disable-undo make-local-variable compile-command compilation-error-message error-message compilation-directory-stack set-window-start mode-name compilation-set-window-height fboundp start-process start-process-shell-command proc set-process-sentinel compilation-sentinel set-process-filter compilation-filter process-mark compilation-in-progress message "Executing `%s'..." call-process shell-file-name "-c" "Executing `%s'...done" compilation-last-buffer] 8 "\
Run compilation command COMMAND (low level interface).
ERROR-MESSAGE is a string to print if the user asks to see another error
and there are no more errors.  Third argument NAME-OF-MODE is the name
to display as the major mode in the compilation buffer.

Fourth arg PARSER is the error parser function (nil means the default).  Fifth
arg REGEXP-ALIST is the error message regexp alist to use (nil means the
default).  Sixth arg NAME-FUNCTION is a function called to name the buffer (nil
means the default).  The defaults for these variables are the global values of
`compilation-parse-errors-function', `compilation-error-regexp-alist', and
`compilation-buffer-name-function', respectively.

Returns the compilation buffer created."])
(fset 'compilation-set-window-height #[(window) "���\n!��\n!!U��\n��\n!!=?���� Ȏ�\n!��� Z!+�" [compilation-window-height window-width window frame-width window-frame frame-root-window selected-window w ((select-window w)) select-window enlarge-window window-height] 4])
(defvar compilation-minor-mode-map (byte-code "� �	�\"��	��#��	��#��	��#��	��#��	��#��	��#�	)�" [make-sparse-keymap map set-keymap-name compilation-minor-mode-map define-key "" compile-goto-error "" kill-compilation "�" compilation-next-error "�" compilation-previous-error "�" compilation-previous-file "�" compilation-next-file] 4) "\
Keymap for `compilation-minor-mode'.")
(defvar compilation-mode-map (byte-code "� �	\"��	�\"��	��#��	��#��	��#��	��#�	)�" [make-sparse-keymap map set-keymap-parent compilation-minor-mode-map set-keymap-name compilation-mode-map define-key " " scroll-up "" scroll-down button2 compile-mouse-goto-error button3 compile-popup-menu] 4) "\
Keymap for compilation log buffers.
`compilation-minor-mode-map' is a parent of this.")
(fset 'compilation-mode #[nil "� ��\n!���� ���!�" [fundamental-mode use-local-map compilation-mode-map compilation-mode major-mode "Compilation" mode-name compilation-setup run-hooks compilation-mode-hook] 2 "\
Major mode for compilation log buffers.
\\<compilation-mode-map>To visit the source for a line-numbered error,
move point to the error message line and type \\[compile-goto-error],
or click on the line with \\[compile-mouse-goto-error].
There is a menu of commands on \\[compile-popup-menu].
To kill the compilation, type \\[kill-compilation].

Runs `compilation-mode-hook' with `run-hooks' (which see)." nil])
(byte-code "��M���M�" [compilation-mode-motion-hook #[(event) "�	��#�" [mode-motion-highlight-internal event beginning-of-line #[nil "`��`�=��Y��Ƌ���@@X�����ʍ��\nb��b+�" [p nil e compilation-error-list t compilation-parsing-end ((compile-reinitialize-errors nil p)) compilation-old-error-list l found (byte-code "���@@!�W��A��\nX�����\"������\"�)�Yȇ" [l marker-position x p e throw found t nil] 4)] 2]] 4] compilation-setup #[nil "���!�L���!�L���!�L���!�L�p	��!����!����\"�" [(":%s") mode-line-process make-local-variable compilation-error-list nil compilation-old-error-list compilation-parsing-end 1 compilation-directory-stack compilation-last-buffer require mode-motion compilation-mode-motion-hook mode-motion-hook mouse-track-click-hook add-hook compile-mouse-maybe-goto-error] 3]] 2)
(defvar compilation-minor-mode nil "\
Non-nil when in compilation-minor-mode.
In this minor mode, all the error-parsing commands of the
Compilation major mode are available.")
(byte-code "��!��\n����\nB�����BB��" [make-variable-buffer-local compilation-minor-mode minor-mode-alist (compilation-minor-mode " Compilation") minor-mode-map-alist compilation-minor-mode-map] 2)
(fset 'compilation-minor-mode #[(&optional arg) "��	?���!�V���� �" [arg compilation-minor-mode prefix-numeric-value 0 compilation-setup] 2 "\
Toggle compilation minor mode.
With arg, turn compilation mode on if and only if arg is positive.
See `compilation-mode'.
! \\{compilation-mode-map}" "P"])
(fset 'compilation-sentinel #[(proc msg) "�	!�\n!�	!�>�� �\n!���	�\"��� pɉ�\nΎ\nq��d`b������u��� ��O���u����	!!�	!�U��ݪ����	!\"�\nQ ���d\"�����\"���\n���\"��	!�� �)��W��b�*��*\n\"�-�	,\"�,*�" [process-buffer proc buffer get-buffer-window window process-status (signal exit) buffer-name set-process-buffer nil estatus opoint omax obuf ((set-buffer obuf)) buffer-read-only 10 mode-name " " msg -1 " at " current-time-string 0 19 1 ":" symbol-name process-exit-status " OK" format " [exit-status %d]" mode-line-process pos-visible-in-window-p ding t ready message "Compilation process completed%s." " successfully" delete-process redraw-mode-line compilation-finish-function delq compilation-in-progress] 6 "\
Sentinel for compilation buffers."])
(fset 'compilation-filter #[(proc string) "��	!q����	!b��!��	!`+�" [process-buffer proc nil buffer-read-only process-mark insert-before-markers string] 3 "\
Process filter for compilation buffers.
Just inserts the text, but uses `insert-before-markers'."])
(byte-code "��M���M�" [compile-error-at-point #[nil "��`\"�\n���`@@V��A��s)�" [compile-reinitialize-errors nil compilation-old-error-list errors] 4] compilation-buffer-p #[(buffer) "��\n\"�" [local-variable-p compilation-error-list buffer] 3]] 2)
(fset 'compilation-next-error #[(n) "�p!����!�p� �W���	\n	=��\nT\n	A�	�o[\nV����!��\n\\8*��A�Ή#���S8����!)@b)�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer compile-error-at-point errors n 0 compilation-old-error-list e i "Moved back past first error" compilation-error-list compile-reinitialize-errors nil "Moved past last error"] 5 "\
Move point to the next error in the compilation buffer.
Does NOT find the source line like \\[next-error]." "p"])
(fset 'compilation-previous-error #[(n) "�	[!�" [compilation-next-error n] 2 "\
Move point to the previous error in the compilation buffer.
Does NOT find the source line like \\[next-error]." "p"])
(byte-code "��M���N�>�����\"�����#���M���N�>�����\"�����#�" [compilation-error-filedata #[(data) "A�!���!�@�" [data markerp marker-buffer] 2] byte-optimizer (nil byte-compile-inline-expand) error "%s already has a byte-optimizer, can't make it inline" put byte-compile-inline-expand compilation-error-filedata-file-name #[(filedata) "�	!���	!�	@�" [bufferp filedata buffer-file-name] 2]] 4)
(fset 'compilation-next-file #[(n) "�p!����!�p�WƉ�	��� ����!����`\"��![��`@@W��A��p�V�� @�A�!���!��@)��	�����!���!��@)\"����Ɖ�#��)�����!���!��@)\"@�A�!���!��@)���A�S��G ��	����!����Ɖ�#�)@@b+�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer n 0 nil filedata errors reversed compile-error-at-point "Moved past last error" compile-reinitialize-errors reverse compilation-old-error-list data markerp marker-buffer "%s the first erring file" bufferp buffer-file-name compilation-error-list 2 "%s is the last erring file" "This is the first erring file" 1] 6 "\
Move point to the next error for a different file than the current one." "p"])
(fset 'compilation-previous-file #[(n) "�	[!�" [compilation-next-file n] 2 "\
Move point to the previous error for a different file than the current one." "p"])
(fset 'kill-compilation #[nil "� �	!����	!!����!)�" [compilation-find-buffer buffer get-buffer-process interrupt-process error "The compilation process is not running."] 3 "\
Kill the process made by the \\[compile] command." nil])
(fset 'compile-reinitialize-errors #[(reparse &optional limit-search find-at-least) "���)�" [((byte-code "q�	�=����� �	����V��?��	GY?���!�	���	!����!�)��!�dW��	��	GZ\"�	����)�" [compilation-last-buffer compilation-error-list t reparse compilation-forget-errors limit-search compilation-parsing-end find-at-least get-buffer-window w select-window switch-to-buffer set-buffer-modified-p nil error-list-pos compilation-parse-errors-function compilation-old-error-list] 5))] 1])
(fset 'compile-goto-error #[(&optional argp) "�p!����!�p�:`\"��y����`@@V��A��p� ����!�q�� ���!�" [compilation-buffer-p error "Not in a compilation buffer." compilation-last-buffer compile-reinitialize-errors argp 0 compilation-old-error-list compilation-error-list one-window-p other-window -1 push-mark next-error 1] 4 "\
Visit the source for the error message point is on.
Use this command in a compilation log buffer.  Sets the mark at point there.
\\[universal-argument] as a prefix arg means to reparse the buffer's error messages first;
other kinds of prefix arguments are ignored." "P"])
(fset 'compile-mouse-goto-error #[(event) "�	!��y�� �" [mouse-set-point event 0 compile-goto-error] 2 "\
Visit the source for the error under the mouse.
Use this command in a compilation log buffer." "e"])
(byte-code "��M���M�" [compile-mouse-maybe-goto-error #[(event &optional click-count) "�	!��p`� ��ɏ+�" [event-button event 2 current-window-configuration config point buffer nil (byte-code "�	!�" [compile-mouse-goto-error event t] 2) ((error (byte-code "�	!�\nq�b�ć" [set-window-configuration config buffer point nil] 2)))] 3 nil "e"] compilation-find-buffer #[(&optional other-buffer) "���p!��p�\n���\n!����\np=��\n�� ����@!����@p=��A��k��@�����p!������!�p����!)�" [other-buffer compilation-buffer-p compilation-last-buffer buffer-name buffer-list buffers message "This is the only compilation buffer." error "No compilation started!"] 3]] 2)
(fset 'next-error #[(&optional argp) "� ��:?���!:\"!�" [compilation-find-buffer compilation-last-buffer compilation-goto-locus compilation-next-error-locus argp prefix-numeric-value] 4 "\
Visit next compilation error message and corresponding source code.
This operates on the output from the \\[compile] command.
If all preparsed error messages have been processed,
the error message buffer is checked for new ones.

A prefix arg specifies how many error messages to move;
negative means move back to previous error messages.
Just C-u as a prefix means reparse the error message buffer
and start at the first error.

\\[next-error] normally applies to the most recent compilation started,
but as long as you are in the middle of parsing errors from one compilation
output buffer, you stay with that compilation output buffer.

Use \\[next-error] in a compilation output buffer to switch to
processing errors from that compilation.

See variables `compilation-parse-errors-function' and
`compilation-error-regexp-alist' for customization ideas." "P"])
(fset 'previous-error #[(&optional argp) "�	��ª�	���	[��	!�" [next-error argp -1] 2 "\
Visit previous compilation error message and corresponding source code.
This operates on the output from the \\[compile] command." "P"])
(fset 'first-error #[nil "��!�" [next-error (1.1)] 2 "\
Reparse the error message buffer and start at the first error
Visit corresponding source code.
This operates on the output from the \\[compile] command." nil])
(fset 'compilation-next-error-locus #[(&optional move reparse silent) "�����?���W��Ū�S#�ĉ�ɍ�����@!!�����@!!!���!�*�" [move 1 compile-reinitialize-errors reparse nil 0 next-error next-errors no-next-error (byte-code "�q�	�V��	S���	S�=��TA��q[V����!��\\�+�\n@���	��	�U���	�V��ͪ��!������\"����p!����p!!�=���P!��\nAA���A!�|A@p�A@@#����\n�{\n@A:��\n@A@���\n@@щ��\n@ѡ�\nA�\n�VAA@�A8q���~��!����!����y�� ����@A:��@A@���@AA@#�@A8#Z$%�=��$�W������$[$�������$$���$y����!�#@� ��+A��*q�++�A!���A!?���\n@��D ��@AA���A��q)ч" [compilation-last-buffer move 0 compilation-error-list compilation-old-error-list e i n error "Moved back past first error" next-errors next-error 1 "Moved past last error" silent throw no-next-error nil compilation-error-message get-buffer-process process-status run " yet" markerp fileinfo cbuf compilation-find-file buffer 2 column last-line errors goto-line move-to-column point-marker this lines selective-display t re-search-backward "[\n]" end re-search-forward marker-buffer] 6) get-buffer-window marker-buffer select-window other-window -1] 5 "\
Visit next compilation error and return locus in corresponding source code.
This operates on the output from the \\[compile] command.
If all preparsed error messages have been processed,
the error message buffer is checked for new ones.

Returns a cons (ERROR . SOURCE) of two markers: ERROR is a marker at the
location of the error message in the compilation buffer, and SOURCE is a
marker at the location in the source code indicated by the error message.

Optional first arg MOVE says how many error messages to move forwards (or
backwards, if negative); default is 1.  Optional second arg REPARSE, if
non-nil, says to reparse the error message buffer and reset to the first
error (plus MOVE - 1).  If optional third argument SILENT is non-nil, return 
nil instead of raising an error if there are no more errors.

The current buffer should be the desired compilation output buffer."])
(fset 'compilation-goto-locus #[(next-error) "��@!�A!��\"���!	�	!��!��	@\"��	@\"��	!�q�Ab�`�A!U��~�Ab,�" [t pop-up-windows marker-buffer next-error compilation-buffer source-buffer get-buffer-window visible display-buffer compilation-window select-window pop-to-buffer set-window-point set-window-start compilation-set-window-height marker-position] 3 "\
Jump to an error locus returned by `compilation-next-error-locus'.
Takes one argument, a cons (ERROR . SOURCE) of two markers.
Selects a window with point at SOURCE, with another window displaying ERROR."])
(byte-code "��M���M�" [compilation-find-file #[(filename dir marker) "��������@��\"�\n!���\n!A��c�����!!�\"��\"�*����\"�$!�\n!���\n!P�\n!���\n!+�" [compilation-search-path nil name result dirs expand-file-name filename dir file-exists-p find-file-noselect t pop-up-windows display-buffer marker-buffer marker w set-window-point set-window-start read-file-name format "Find this error in: (default %s) " file-directory-p file-name-as-directory] 7] compilation-forget-errors #[nil "��@�@���	A!��	A��)A��d��Ɖ�" [compilation-old-error-list next-error nil markerp compilation-error-list compilation-directory-stack 1 compilation-parsing-end] 4]] 2)
(fset 'count-regexp-groupings #[(regexp) "�	G��W��	HT�U��W�j	H�U�cT��o�U�XW�S	HT�U�GT�@,�" [0 regexp nil c i len groupings 91 93 92 41] 5 "\
Return the number of \\( ... \\) groupings in REGEXP (a string)."])
(fset 'compilation-parse-errors #[(limit-search find-at-least) "���!�������������	\nb�o���y��u���������#ܰ	���!�\\\\��!�\\\\\n����!	\nT	���	@A@\\�	@8\\�	@8�\"��\"\\)FB�	@@!�\\\\	A�	�D#�!��\"�d���#�d������\\��\\�{!!*�*$�*,B,�*!��*#).�G`.Y�G��G����\\�,/�0����0�\\�{!!*�*$*/��/@*���/A�/�p)/A�,@�/��/#*.�G`.Y�G��G\n��C�	��	@@���	A�	�q	��	@	����!�Ĕ	A@�	A@�{��	8��	8�{!�	8����	8��	8�{!3456��!���5!��85P5#5B5��y�� 543EB�;A	@A���;	BT*<��<V��.��`.Y��	A��	@A@	A@A@���	A6b��,����!���`�_d�#�.�� `.Y�� ��� ��`��d.	�@ !�" [nil compilation-error-list message "Parsing error messages..." 0 compilation-num-errors-found found-desired error-regexp-groups subexpr alist error-group leave-group enter-group regexp parent-expanded orig-expanded orig compilation-parsing-end 2 -1 "\\(" compilation-enter-directory-regexp "\\)\\|" compilation-leave-directory-regexp mapconcat #[(elt) "�	@�Q�" ["\\(" elt "\\)"] 3] compilation-error-regexp-alist "\\|" "\\)" 1 count-regexp-groupings error "compilation-error-regexp-alist is empty!" 3 col default-directory file-truename expand-file-name "../" re-search-forward t file-name-as-directory dir compile-abbreviate-directory compilation-directory-stack file-directory-p limit-search stack beg "compilation-parse-errors: impossible regexp match!" string-to-int column linenum filename beginning-of-match boundp comint-file-name-prefix file-name-absolute-p point-marker this find-at-least "compilation-parse-errors: known groups didn't match!" "Parsing error messages...%d (%d%% of buffer)" 100 "Parsing error messages...done"] 13 "\
Parse the current buffer as grep, cc or lint error messages.
See variable `compilation-parse-errors-function' for the interface it uses."])
(fset 'compile-abbreviate-directory #[(dir orig orig-expanded parent-expanded) "��!��	\nP\nGGV��\n�GO���\nG�OP\nGGV��\n�GO�����!!\nG�OP\n�" [boundp comint-file-name-prefix dir orig-expanded 0 orig nil parent-expanded file-name-directory directory-file-name] 4])
(fset 'compilation-errors-exist-p #[(&optional buffer) "����Ï���!���q�??��dW)�" [buffer nil (compilation-find-buffer) ((error)) compilation-buffer-p compilation-error-list compilation-parsing-end] 3 "\
Whether we are in a state where the `next-error' command will work,
that is, whether there exist (or may exist) error targets in the *compile*
or *grep* buffers."])
(byte-code "��!�����!�����" [boundp compilation-mode-menu ("Compilation Mode Commands" ["Compile..." compile t] ["Kill Compilation" kill-compilation (get-buffer-process (current-buffer))] ["Goto Error" compile-goto-error (compilation-errors-exist-p)] ["Next Error" next-error (compilation-errors-exist-p)] ["Previous Error" previous-error (compilation-errors-exist-p)]) grep-mode-menu ("Grep Mode Commands" ["Grep..." grep t] ["Kill Grep" kill-compilation (get-buffer-process (current-buffer))] ["Goto Match" compile-goto-error (default-value 'compilation-error-list)] ["Next Match" next-error (default-value 'compilation-error-list)] ["Previous Match" previous-error (default-value 'compilation-error-list)])] 2)
(fset 'compile-popup-menu #[(e) "����\n����\"\n����	\n\n��˪���\n\"�������\nBC���\n!-�" [mode-name "grep" grep-p mapcar #[(string) "�	\n��ê��	D�#�" [vector string grep-p grep compile t] 4] grep-history compile-history submenu grep-mode-menu compilation-mode-menu menu "Grep History" "Compile History" name assoc existing popup-menu] 4 "\
Pops the compile menu up." "@e"])
(provide 'compile)

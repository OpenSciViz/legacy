;;; GNUS: an NNTP-based News Reader for GNU Emacs
;; Copyright (C) 1987,88,89,90,93,94 Free Software Foundation, Inc.

;; Author: Masanobu UMEDA <umerin@mse.kyutech.ac.jp>
;; Keywords: news

;; This file is part of GNU Emacs.

;; GNU Emacs is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; How to Install GNUS:
;; (0) First of all, remove GNUS related OLD *.elc files (at least
;;     nntp.elc).
;; (1) Unshar gnus.el, gnuspost.el, gnusmail.el, gnusmisc.el, and
;;     nntp.el.
;; (2) byte-compile-file nntp.el, gnus.el, gnuspost.el, gnusmail.el,
;;     and gnusmisc.el.  If you have a local news spool,
;;     byte-compile-file nnspool.el, too.
;; (3) Define three environment variables in .login file as follows:
;;
;;     setenv	NNTPSERVER	flab
;;     setenv	DOMAINNAME	"stars.flab.Fujitsu.CO.JP"
;;     setenv	ORGANIZATION	"Fujitsu Laboratories Ltd., Kawasaki, Japan."
;;
;;     Or instead, define lisp variables in your .emacs, site-init.el,
;;     or default.el as follows:
;;
;;     (setq gnus-nntp-server "flab")
;;     (setq gnus-local-domain "stars.flab.Fujitsu.CO.JP")
;;     (setq gnus-local-organization "Fujitsu Laboratories Ltd., ...")
;;
;;     If the function (system-name) returns the full internet name,
;;     you don't have to define the domain.
;;
;; (4) You may have to define NNTP service name as number 119.
;;
;;     (setq gnus-nntp-service 119)
;;
;;     Or, if you'd like to use a local news spool directly in stead
;;     of NNTP, install nnspool.el and set the variable to nil as
;;     follows:
;;
;;     (setq gnus-nntp-service nil)
;;
;; (5) If you'd like to use the GENERICFROM feature like the Bnews,
;;     define the variable as follows:
;;
;;     (setq gnus-use-generic-from t)
;;
;; (6) Define autoload entries in .emacs file as follows:
;;
;;     (autoload 'gnus "gnus" "Read network news." t)
;;     (autoload 'gnus-post-news "gnuspost" "Post a news." t)
;;
;; (7) Read nntp.el if you have problems with NNTP or kanji handling.
;;
;; (8) Install mhspool.el, tcp.el, and tcp.c if it is necessary.
;;
;;     mhspool.el is a package for reading articles or mail in your
;;     private directory using GNUS.
;;
;;     tcp.el and tcp.c are necessary if and only if your Emacs does
;;     not have the function `open-network-stream' which is used for
;;     communicating with NNTP server inside Emacs.
;;
;; (9) Install an Info file generated from the texinfo manual gnus.texinfo.
;;
;;     If you are not allowed to create the Info file to the standard
;;     Info-directory, create it in your private directory and set the
;;     variable gnus-info-directory to that directory.
;;
;; For getting more information about GNUS, consult USENET newsgorup
;; gnu.emacs.gnus.

;; TO DO:
;; (1) Incremental update of active info.
;; (2) Asynchronous transmission of large messages.

;;; Code:

(require 'nntp)
(require 'mail-utils)
(require 'timezone)

(defvar gnus-default-nntp-server nil
  "*Specify default NNTP server.
This variable should be defined in paths.el.")

(defvar gnus-nntp-server (or (getenv "NNTPSERVER") gnus-default-nntp-server)
  "*The name of the host running NNTP server.
If it is a string such as `:DIRECTORY', the user's private DIRECTORY
is used as a news spool.
Initialized from the NNTPSERVER environment variable.")

(defvar gnus-nntp-service "nntp"
  "*NNTP service name (\"nntp\" or 119).
Go to a local news spool if its value is nil.")

(defvar gnus-startup-file "~/.newsrc"
  "*Your `.newsrc' file.  Use `.newsrc-SERVER' instead if exists.")

(defvar gnus-signature-file "~/.signature"
  "*Your `.signature' file.  Use `.signature-DISTRIBUTION' instead if exists.")

(defvar gnus-use-cross-reference t
  "*Specifies what to do with cross references (Xref: field).
If nil, ignore cross references.  If t, mark articles as read in
subscribed newsgroups.  Otherwise, if not nil nor t, mark articles as
read in all newsgroups.")

(defvar gnus-use-followup-to t
  "*Specifies what to do with Followup-To: field.
If nil, ignore followup-to: field.  If t, use its value except for
`poster'.  Otherwise, if not nil nor t, always use its value.")

(defvar gnus-large-newsgroup 50
  "*The number of articles which indicates a large newsgroup.
If the number of articles in a newsgroup is greater than the value,
confirmation is required for selecting the newsgroup.")

(defvar gnus-author-copy (getenv "AUTHORCOPY")
  "*File name saving a copy of an article posted using FCC: field.
Initialized from the AUTHORCOPY environment variable.

Articles are saved using a function specified by the the variable
`gnus-author-copy-saver' (`rmail-output' is default) if a file name is
given.  Instead, if the first character of the name is `|', the
contents of the article is piped out to the named program. It is
possible to save an article in an MH folder as follows:

\(setq gnus-author-copy \"|/usr/local/lib/mh/rcvstore +Article\")")

(defvar gnus-author-copy-saver (function rmail-output)
  "*A function called with a file name to save an author copy to.
The default function is `rmail-output' which saves in Unix mailbox format.")

(defvar gnus-use-long-file-name
  (not (memq system-type '(usg-unix-v xenix)))
  "*Non-nil means that a newsgroup name is used as a default file name
to save articles to. If it's nil, the directory form of a newsgroup is
used instead.")

(defvar gnus-article-save-directory (getenv "SAVEDIR")
  "*A directory name to save articles to (default to ~/News).
Initialized from the SAVEDIR environment variable.")

(defvar gnus-kill-files-directory (getenv "SAVEDIR")
  "*A directory name to save kill files to (default to ~/News).
Initialized from the SAVEDIR environment variable.")

(defvar gnus-default-article-saver (function gnus-summary-save-in-rmail)
  "*A function to save articles in your favorite format.
The function must be interactively callable (in other words, it must
be an Emacs command).

GNUS provides the following functions:
	gnus-summary-save-in-rmail (in Rmail format)
	gnus-summary-save-in-mail (in Unix mail format)
	gnus-summary-save-in-folder (in an MH folder)
	gnus-summary-save-in-file (in article format).")

(defvar gnus-rmail-save-name (function gnus-plain-save-name)
  "*A function generating a file name to save articles in Rmail format.
The function is called with NEWSGROUP, HEADERS, and optional LAST-FILE.")

(defvar gnus-mail-save-name (function gnus-plain-save-name)
  "*A function generating a file name to save articles in Unix mail format.
The function is called with NEWSGROUP, HEADERS, and optional LAST-FILE.")

(defvar gnus-folder-save-name (function gnus-folder-save-name)
  "*A function generating a file name to save articles in MH folder.
The function is called with NEWSGROUP, HEADERS, and optional LAST-FOLDER.")

(defvar gnus-file-save-name (function gnus-numeric-save-name)
  "*A function generating a file name to save articles in article format.
The function is called with NEWSGROUP, HEADERS, and optional LAST-FILE.")

(defvar gnus-kill-file-name "KILL"
  "*File name of a KILL file.")

(defvar gnus-novice-user t
  "*Non-nil means that you are a novice to USENET.
If non-nil, verbose messages may be displayed
or your confirmations may be required.")

(defvar gnus-interactive-catchup t
  "*Require your confirmation when catching up a newsgroup if non-nil.")

(defvar gnus-interactive-post t
  "*Newsgroup, subject, and distribution will be asked for if non-nil.")

(defvar gnus-interactive-exit t
  "*Require your confirmation when exiting GNUS if non-nil.")

(defvar gnus-user-login-name nil
  "*The login name of the user.
Got from the function `user-login-name' if undefined.")

(defvar gnus-user-full-name nil
  "*The full name of the user.
Got from the NAME environment variable if undefined.")

(defvar gnus-show-mime nil
  "*Show MIME message if non-nil.")

(defvar gnus-show-threads t
  "*Show conversation threads in Summary Mode if non-nil.")

(defvar gnus-thread-hide-subject t
  "*Non-nil means hide subjects for thread subtrees.")

(defvar gnus-thread-hide-subtree nil
  "*Non-nil means hide thread subtrees initially.
If non-nil, you have to run the command `gnus-summary-show-thread' by
hand or by using `gnus-select-article-hook' to show hidden threads.")

(defvar gnus-thread-hide-killed t
  "*Non-nil means hide killed thread subtrees automatically.")

(defvar gnus-thread-ignore-subject nil
  "*Don't take care of subject differences, but only references if non-nil.
If it is non-nil, some commands work with subjects do not work properly.")

(defvar gnus-thread-indent-level 4
  "*Indentation of thread subtrees.")

(defvar gnus-ignored-newsgroups "^to\\..*$"
  "*A regexp to match uninteresting newsgroups in the active file.
Any lines in the active file matching this regular expression are
removed from the newsgroup list before anything else is done to it,
thus making them effectively invisible.")

(defvar gnus-ignored-headers
  "^Path:\\|^Posting-Version:\\|^Article-I.D.:\\|^Expires:\\|^Date-Received:\\|^References:\\|^Control:\\|^Xref:\\|^Lines:\\|^Posted:\\|^Relay-Version:\\|^Message-ID:\\|^Nf-ID:\\|^Nf-From:\\|^Approved:\\|^Sender:"
  "*All random fields within the header of a message.")

(defvar gnus-required-headers
  '(From Date Newsgroups Subject Message-ID Path Organization Distribution)
  "*All required fields for articles you post.
RFC977 and RFC1036 require From, Date, Newsgroups, Subject, Message-ID
and Path fields.  Organization, Distribution and Lines are optional.
If you want GNUS not to insert some field, remove it from the
variable.")

(defvar gnus-show-all-headers nil
  "*Show all headers of an article if non-nil.")

(defvar gnus-save-all-headers t
  "*Save all headers of an article if non-nil.")

(defvar gnus-optional-headers (function gnus-optional-lines-and-from)
  "*A function generating a optional string displayed in GNUS Summary
mode buffer.  The function is called with an article HEADER. The
result must be a string excluding `[' and `]'.")

(defvar gnus-auto-extend-newsgroup t
  "*Extend visible articles to forward and backward if non-nil.")

(defvar gnus-auto-select-first t
  "*Select the first unread article automagically if non-nil.
If you want to prevent automatic selection of the first unread article
in some newsgroups, set the variable to nil in `gnus-select-group-hook'
or `gnus-apply-kill-hook'.")

(defvar gnus-auto-select-next t
  "*Select the next newsgroup automagically if non-nil.
If the value is t and the next newsgroup is empty, GNUS will exit
Summary mode and go back to Group mode.  If the value is neither nil
nor t, GNUS will select the following unread newsgroup.  Especially, if
the value is the symbol `quietly', the next unread newsgroup will be
selected without any confirmations.")

(defvar gnus-auto-select-same nil
  "*Select the next article with the same subject automagically if non-nil.")

(defvar gnus-auto-center-summary t
  "*Always center the current summary in GNUS Summary window if non-nil.")

(defvar gnus-auto-mail-to-author nil
  "*Insert `To: author' of the article when following up if non-nil.
Mail is sent using the function specified by the variable
`gnus-mail-send-method'.")

(defvar gnus-break-pages t
  "*Break an article into pages if non-nil.
Page delimiter is specified by the variable `gnus-page-delimiter'.")

(defvar gnus-page-delimiter "^\^L"
  "*Regexp describing line-beginnings that separate pages of news article.")

(defvar gnus-digest-show-summary t
  "*Show a summary of undigestified messages if non-nil.")

(defvar gnus-digest-separator "^Subject:[ \t]"
  "*Regexp that separates messages in a digest article.")

(defvar gnus-use-full-window t
  "*Non-nil means to take up the entire screen of Emacs.")

(defvar gnus-window-configuration
  '((summary (0 1 0))
    (newsgroups   (1 0 0))
    (article   (0 3 10)))
  "*Specify window configurations for each action.
The format of the variable is a list of (ACTION (G S A)), where G, S,
and A are the relative height of Group, Summary, and Article windows,
respectively.  ACTION is `summary', `newsgroups', or `article'.")

(defvar gnus-show-mime-method (function metamail-buffer)
  "*Function to process a MIME message.
The function is expected to process current buffer as a MIME message.")

(defvar gnus-mail-reply-method
  (function gnus-mail-reply-using-mail)
  "*Function to compose reply mail.
The function `gnus-mail-reply-using-mail' uses usual sendmail mail
program.  The function `gnus-mail-reply-using-mhe' uses the MH-E mail
program.  You can use yet another program by customizing this variable.")

(defvar gnus-mail-forward-method
  (function gnus-mail-forward-using-mail)
  "*Function to forward current message to another user.
The function `gnus-mail-reply-using-mail' uses usual sendmail mail
program.  You can use yet another program by customizing this variable.")

(defvar gnus-mail-other-window-method
  (function gnus-mail-other-window-using-mail)
  "*Function to compose mail in other window.
The function `gnus-mail-other-window-using-mail' uses the usual sendmail
mail program.  The function `gnus-mail-other-window-using-mhe' uses the MH-E
mail program.  You can use yet another program by customizing this variable.")

(defvar gnus-mail-send-method send-mail-function
  "*Function to mail a message too which is being posted as an article.
The message must have To: or Cc: field.  The default is copied from
the variable `send-mail-function'.")

(defvar gnus-subscribe-newsgroup-method
  (function gnus-subscribe-alphabetically)
  "*Function called with a newsgroup name when new newsgroup is found.
The function `gnus-subscribe-randomly' inserts a new newsgroup a the
beginning of newsgroups.  The function `gnus-subscribe-alphabetically'
inserts it in strict alphabetic order.  The function
`gnus-subscribe-hierarchically' inserts it in hierarchical newsgroup
order.  The function `gnus-subscribe-interactively' asks for your decision.")

(defvar gnus-group-mode-hook nil
  "*A hook for GNUS Group Mode.")

(defvar gnus-summary-mode-hook nil
  "*A hook for GNUS Summary Mode.")

(defvar gnus-article-mode-hook nil
  "*A hook for GNUS Article Mode.")

(defvar gnus-kill-file-mode-hook nil
  "*A hook for GNUS KILL File Mode.")

(defvar gnus-open-server-hook nil
  "*A hook called just before opening connection to news server.")

(defvar gnus-startup-hook nil
  "*A hook called at start up time.
This hook is called after GNUS is connected to the NNTP server. So, it
is possible to change the behavior of GNUS according to the selected
NNTP server.")

(defvar gnus-group-prepare-hook nil
  "*A hook called after newsgroup list is created in the Newsgroup buffer.
If you want to modify the Newsgroup buffer, you can use this hook.")

(defvar gnus-summary-prepare-hook nil
  "*A hook called after summary list is created in the Summary buffer.
If you want to modify the Summary buffer, you can use this hook.")

(defvar gnus-article-prepare-hook nil
  "*A hook called after an article is prepared in the Article buffer.
If you want to run a special decoding program like nkf, use this hook.")

(defvar gnus-select-group-hook nil
  "*A hook called when a newsgroup is selected.
If you want to sort Summary buffer by date and then by subject, you
can use the following hook:

\(setq gnus-select-group-hook
      (list
       (function
	(lambda ()
	  ;; First of all, sort by date.
	  (gnus-keysort-headers
	   (function string-lessp)
	   (function
	    (lambda (a)
	      (gnus-sortable-date (gnus-header-date a)))))
	  ;; Then sort by subject string ignoring `Re:'.
	  ;; If case-fold-search is non-nil, case of letters is ignored.
	  (gnus-keysort-headers
	   (function string-lessp)
	   (function
	    (lambda (a)
	      (if case-fold-search
		  (downcase (gnus-simplify-subject (gnus-header-subject a) t))
		(gnus-simplify-subject (gnus-header-subject a) t)))))
	  ))))

If you'd like to simplify subjects like the
`gnus-summary-next-same-subject' command does, you can use the
following hook:

\(setq gnus-select-group-hook
      (list
       (function
	(lambda ()
	  (mapcar (function
		   (lambda (header)
		     (nntp-set-header-subject
		      header
		      (gnus-simplify-subject
		       (gnus-header-subject header) 're-only))))
		  gnus-newsgroup-headers)))))

In some newsgroups author name is meaningless. It is possible to
prevent listing author names in GNUS Summary buffer as follows:

\(setq gnus-select-group-hook
      (list
       (function
	(lambda ()
	  (cond ((string-equal \"comp.sources.unix\" gnus-newsgroup-name)
		 (setq gnus-optional-headers
		       (function gnus-optional-lines)))
		(t
		 (setq gnus-optional-headers
		       (function gnus-optional-lines-and-from))))))))")

(defvar gnus-select-article-hook
  '(gnus-summary-show-thread)
  "*A hook called when an article is selected.
The default hook shows conversation thread subtrees of the selected
article automatically using `gnus-summary-show-thread'.

If you'd like to run RMAIL on a digest article automagically, you can
use the following hook:

\(setq gnus-select-article-hook
      (list
       (function
	(lambda ()
	  (gnus-summary-show-thread)
	  (cond ((string-equal \"comp.sys.sun\" gnus-newsgroup-name)
		 (gnus-summary-rmail-digest))
		((and (string-equal \"comp.text\" gnus-newsgroup-name)
		      (string-match \"^TeXhax Digest\"
				    (gnus-header-subject gnus-current-headers)))
		 (gnus-summary-rmail-digest)
		 ))))))")

(defvar gnus-select-digest-hook
  (list
   (function
    (lambda ()
      ;; Reply-To: is required by `undigestify-rmail-message'.
      (or (mail-position-on-field "Reply-to" t)
	  (progn
	    (mail-position-on-field "Reply-to")
	    (insert (gnus-fetch-field "From")))))))
  "*A hook called when reading digest messages using Rmail.
This hook can be used to modify incomplete digest articles as follows
\(this is the default):

\(setq gnus-select-digest-hook
      (list
       (function
	(lambda ()
	  ;; Reply-To: is required by `undigestify-rmail-message'.
	  (or (mail-position-on-field \"Reply-to\" t)
	      (progn
		(mail-position-on-field \"Reply-to\")
		(insert (gnus-fetch-field \"From\"))))))))")

(defvar gnus-rmail-digest-hook nil
  "*A hook called when reading digest messages using Rmail.
This hook is intended to customize Rmail mode for reading digest articles.")

(defvar gnus-apply-kill-hook '(gnus-apply-kill-file)
  "*A hook called when a newsgroup is selected and summary list is prepared.
This hook is intended to apply a KILL file to the selected newsgroup.
The function `gnus-apply-kill-file' is called by default.

Since a general KILL file is too heavy to use only for a few
newsgroups, I recommend you to use a lighter hook function. For
example, if you'd like to apply a KILL file to articles which contains
a string `rmgroup' in subject in newsgroup `control', you can use the
following hook:

\(setq gnus-apply-kill-hook
      (list
       (function
	(lambda ()
	  (cond ((string-match \"control\" gnus-newsgroup-name)
		 (gnus-kill \"Subject\" \"rmgroup\")
		 (gnus-expunge \"X\")))))))")

(defvar gnus-mark-article-hook
  (list
   (function
    (lambda ()
      (or (memq gnus-current-article gnus-newsgroup-marked)
	  (gnus-summary-mark-as-read gnus-current-article))
      (gnus-summary-set-current-mark "+"))))
  "*A hook called when an article is selected at the first time.
The hook is intended to mark an article as read (or unread)
automatically when it is selected.

If you'd like to mark as unread (-) instead, use the following hook:

\(setq gnus-mark-article-hook
      (list
       (function
        (lambda ()
	  (gnus-summary-mark-as-unread gnus-current-article)
	  (gnus-summary-set-current-mark \"+\")))))")

(defvar gnus-prepare-article-hook (list (function gnus-inews-insert-signature))
  "*A hook called after preparing body, but before preparing header fields.
The default hook (`gnus-inews-insert-signature') inserts a signature
file specified by the variable `gnus-signature-file'.")

(defvar gnus-inews-article-hook (list (function gnus-inews-do-fcc))
  "*A hook called before finally posting an article.
The default hook (`gnus-inews-do-fcc') does FCC processing (save article
to a file).")

(defvar gnus-exit-group-hook nil
  "*A hook called when exiting (not quitting) Summary mode.
If your machine is so slow that exiting from Summary mode takes very
long time, set the variable `gnus-use-cross-reference' to nil. This
inhibits marking articles as read using cross-reference information.")

(defvar gnus-suspend-gnus-hook nil
  "*A hook called when suspending (not exiting) GNUS.")

(defvar gnus-exit-gnus-hook nil
  "*A hook called when exiting (not suspending) GNUS.")

(defvar gnus-save-newsrc-hook nil
  "*A hook called when saving the newsrc file.
This hook is called before saving the `.newsrc' file.")


;; Site dependent variables. You have to define these variables in
;;  site-init.el, default.el or your .emacs.

(defvar gnus-local-timezone nil
  "*Local time zone.
This value is used only if `current-time-zone' does not work in your Emacs.
It specifies the GMT offset, i.e. a decimal integer
of the form +-HHMM giving the hours and minutes ahead of (i.e. east of) GMT.
For example, +0900 should be used in Japan, since it is 9 hours ahead of GMT.

For backwards compatibility, it may also be a string like \"JST\",
but strings are obsolescent: you should use numeric offsets instead.")

(defvar gnus-local-domain nil
  "*Local domain name without a host name like: \"stars.flab.Fujitsu.CO.JP\"
The `DOMAINNAME' environment variable is used instead if defined.  If
the function (system-name) returns the full internet name, there is no
need to define the name.")

(defvar gnus-local-organization nil
  "*Local organization like: \"Fujitsu Laboratories Ltd., Kawasaki, Japan.\"
The `ORGANIZATION' environment variable is used instead if defined.")

(defvar gnus-local-distributions '("local" "world")
  "*List of distributions.
The first element in the list is used as default.  If distributions
file is available, its content is also used.")

(defvar gnus-use-generic-from nil
  "*If nil, prepend local host name to the defined domain in the From:
field; if stringp, use this; if non-nil, strip of the local host name.")

(defvar gnus-use-generic-path nil
  "*If nil, use the NNTP server name in the Path: field; if stringp,
use this; if non-nil, use no host name (user name only)")

;; Internal variables.

(defconst gnus-version "GNUS 4.1"
  "Version numbers of this version of GNUS.")

(defconst gnus-emacs-version
  (progn
    (string-match "[0-9]*" emacs-version)
    (string-to-int (substring emacs-version
			      (match-beginning 0) (match-end 0))))
  "Major version number of this emacs.")

(defvar gnus-info-nodes
  '((gnus-group-mode		"(gnus)Newsgroup Commands")
    (gnus-summary-mode		"(gnus)Summary Commands")
    (gnus-article-mode		"(gnus)Article Commands")
    (gnus-kill-file-mode	"(gnus)Kill File")
    (gnus-browse-killed-mode	"(gnus)Maintaining Subscriptions"))
  "Assoc list of major modes and related Info nodes.")

;; Alist syntax is different from that of 3.14.3.
(defvar gnus-access-methods
  '((nntp
     (gnus-retrieve-headers		nntp-retrieve-headers)
     (gnus-open-server			nntp-open-server)
     (gnus-close-server			nntp-close-server)
     (gnus-server-opened		nntp-server-opened)
     (gnus-status-message		nntp-status-message)
     (gnus-request-article		nntp-request-article)
     (gnus-request-group		nntp-request-group)
     (gnus-request-list			nntp-request-list)
     (gnus-request-list-newsgroups	nntp-request-list-newsgroups)
     (gnus-request-list-distributions	nntp-request-list-distributions)
     (gnus-request-post			nntp-request-post))
    (nnspool
     (gnus-retrieve-headers		nnspool-retrieve-headers)
     (gnus-open-server			nnspool-open-server)
     (gnus-close-server			nnspool-close-server)
     (gnus-server-opened		nnspool-server-opened)
     (gnus-status-message		nnspool-status-message)
     (gnus-request-article		nnspool-request-article)
     (gnus-request-group		nnspool-request-group)
     (gnus-request-list			nnspool-request-list)
     (gnus-request-list-newsgroups	nnspool-request-list-newsgroups)
     (gnus-request-list-distributions	nnspool-request-list-distributions)
     (gnus-request-post			nnspool-request-post))
    (mhspool
     (gnus-retrieve-headers		mhspool-retrieve-headers)
     (gnus-open-server			mhspool-open-server)
     (gnus-close-server			mhspool-close-server)
     (gnus-server-opened		mhspool-server-opened)
     (gnus-status-message		mhspool-status-message)
     (gnus-request-article		mhspool-request-article)
     (gnus-request-group		mhspool-request-group)
     (gnus-request-list			mhspool-request-list)
     (gnus-request-list-newsgroups	mhspool-request-list-newsgroups)
     (gnus-request-list-distributions	mhspool-request-list-distributions)
     (gnus-request-post			mhspool-request-post)))
  "Access method for NNTP, nnspool, and mhspool.")

(defvar gnus-group-buffer "*Newsgroup*")
(defvar gnus-summary-buffer "*Summary*")
(defvar gnus-article-buffer "*Article*")
(defvar gnus-digest-buffer "GNUS Digest")
(defvar gnus-digest-summary-buffer "GNUS Digest-summary")

(defvar gnus-buffer-list
  (list gnus-group-buffer gnus-summary-buffer gnus-article-buffer
	gnus-digest-buffer gnus-digest-summary-buffer)
  "GNUS buffer names which should be killed when exiting.")

(defvar gnus-variable-list
  '(gnus-newsrc-options
    gnus-newsrc-options-n-yes gnus-newsrc-options-n-no
    gnus-newsrc-assoc gnus-killed-assoc gnus-marked-assoc)
  "GNUS variables saved in the quick startup file.")

(defvar gnus-overload-functions
  '((news-inews gnus-inews-news "rnewspost")
    (caesar-region gnus-caesar-region "rnews"))
  "Functions overloaded by gnus.
It is a list of `(original overload &optional file)'.")

(defvar gnus-distribution-list nil)

(defvar gnus-newsrc-options nil
  "Options line in the .newsrc file.")

(defvar gnus-newsrc-options-n-yes nil
  "Regexp representing subscribed newsgroups.")

(defvar gnus-newsrc-options-n-no nil
  "Regexp representing unsubscribed newsgroups.")

(defvar gnus-newsrc-assoc nil
  "Assoc list of read articles.
gnus-newsrc-hashtb should be kept so that both hold the same information.")

(defvar gnus-newsrc-hashtb nil
  "Hashtable of gnus-newsrc-assoc.")

(defvar gnus-killed-assoc nil
  "Assoc list of newsgroups removed from gnus-newsrc-assoc.
gnus-killed-hashtb should be kept so that both hold the same information.")

(defvar gnus-killed-hashtb nil
  "Hashtable of gnus-killed-assoc.")

(defvar gnus-marked-assoc nil
  "Assoc list of articles marked as unread.
gnus-marked-hashtb should be kept so that both hold the same information.")

(defvar gnus-marked-hashtb nil
  "Hashtable of gnus-marked-assoc.")

(defvar gnus-unread-hashtb nil
  "Hashtable of unread articles.")

(defvar gnus-active-hashtb nil
  "Hashtable of active articles.")

(defvar gnus-octive-hashtb nil
  "Hashtable of OLD active articles.")

(defvar gnus-current-startup-file nil
  "Startup file for the current host.")

(defvar gnus-last-search-regexp nil
  "Default regexp for article search command.")

(defvar gnus-last-shell-command nil
  "Default shell command on article.")

(defvar gnus-have-all-newsgroups nil)

(defvar gnus-newsgroup-name nil)
(defvar gnus-newsgroup-begin nil)
(defvar gnus-newsgroup-end nil)
(defvar gnus-newsgroup-last-rmail nil)
(defvar gnus-newsgroup-last-mail nil)
(defvar gnus-newsgroup-last-folder nil)
(defvar gnus-newsgroup-last-file nil)

(defvar gnus-newsgroup-unreads nil
  "List of unread articles in the current newsgroup.")

(defvar gnus-newsgroup-unselected nil
  "List of unselected unread articles in the current newsgroup.")

(defvar gnus-newsgroup-marked nil
  "List of marked articles in the current newsgroup (a subset of unread art).")

(defvar gnus-newsgroup-headers nil
  "List of article headers in the current newsgroup.
If the variable is modified (added or deleted), the function
gnus-clear-hashtables-for-newsgroup-headers must be called to clear
the hash tables.")
(defvar gnus-newsgroup-headers-hashtb-by-id nil)
(defvar gnus-newsgroup-headers-hashtb-by-number nil)

(defvar gnus-current-article nil)
(defvar gnus-current-headers nil)
(defvar gnus-current-history nil)
(defvar gnus-have-all-headers nil "Must be either T or NIL.")
(defvar gnus-last-article nil)
(defvar gnus-current-kill-article nil)

;; Save window configuration.
(defvar gnus-winconf-kill-file nil)

(defvar gnus-group-mode-map nil)
(defvar gnus-summary-mode-map nil)
(defvar gnus-article-mode-map nil)
(defvar gnus-kill-file-mode-map nil)

(defvar rmail-default-file (expand-file-name "~/XMBOX"))
(defvar rmail-default-rmail-file (expand-file-name "~/XNEWS"))

;; Define GNUS Subsystems.
(autoload 'gnus-group-post-news "gnuspost"
	  "Post an article." t)
(autoload 'gnus-summary-post-news "gnuspost"
	  "Post an article." t)
(autoload 'gnus-summary-followup "gnuspost"
	  "Post a reply article." t)
(autoload 'gnus-summary-followup-with-original "gnuspost"
	  "Post a reply article with original article." t)
(autoload 'gnus-summary-cancel-article "gnuspost"
	  "Cancel an article you posted." t)

(autoload 'gnus-summary-reply "gnusmail"
	  "Reply mail to news author." t)
(autoload 'gnus-summary-reply-with-original "gnusmail"
	  "Reply mail to news author with original article." t)
(autoload 'gnus-summary-mail-forward "gnusmail"
	  "Forward the current message to another user." t)
(autoload 'gnus-summary-mail-other-window "gnusmail"
	  "Compose mail in other window." t)

(autoload 'gnus-group-kill-group "gnusmisc"
	  "Kill newsgroup on current line." t)
(autoload 'gnus-group-yank-group "gnusmisc"
	  "Yank the last killed newsgroup on current line." t)
(autoload 'gnus-group-kill-region "gnusmisc"
	  "Kill newsgroups in current region." t)
(autoload 'gnus-group-transpose-groups "gnusmisc"
	  "Exchange current newsgroup and previous newsgroup." t)
(autoload 'gnus-list-killed-groups "gnusmisc"
	  "List the killed newsgroups." t)
(autoload 'gnus-gmt-to-local "gnusmisc"
	  "Rewrite Date field in GMT to local in current buffer.")

(autoload 'metamail-buffer "metamail"
	  "Process current buffer through 'metamail'." t)

(autoload 'rmail-output "rmailout"
	  "Append this message to Unix mail file named FILE-NAME." t)
(autoload 'mail-position-on-field "sendmail")
(autoload 'mh-find-path "mh-e")
(autoload 'mh-prompt-for-folder "mh-e")

(put 'gnus-group-mode 'mode-class 'special)
(put 'gnus-summary-mode 'mode-class 'special)
(put 'gnus-article-mode 'mode-class 'special)

(autoload 'gnus-uu-ctl-map "gnus-uu" nil nil 'keymap)
(autoload 'gnus-uu-mark-article "gnus-uu" nil t)

;;(put 'gnus-eval-in-buffer-window 'lisp-indent-hook 1)

(defmacro gnus-eval-in-buffer-window (buffer &rest forms)
  "Pop to BUFFER, evaluate FORMS, and then returns to original window."
  (` (let ((GNUSStartBufferWindow (selected-window)))
       (unwind-protect
	   (progn
	     (pop-to-buffer (, buffer))
	     (,@ forms))
	 (select-window GNUSStartBufferWindow)))))

(defmacro gnus-make-hashtable (&optional hashsize)
  "Make a hash table (default and minimum size is 200).
Optional argument HASHSIZE specifies the table size."
  (` (make-vector (, (if hashsize (` (max (, hashsize) 200)) 200)) 0)))

(defmacro gnus-gethash (string hashtable)
  "Get hash value of STRING in HASHTABLE."
  ;;(` (symbol-value (abbrev-symbol (, string) (, hashtable))))
  ;;(` (abbrev-expansion (, string) (, hashtable)))
  (` (symbol-value (intern-soft (, string) (, hashtable)))))

(defmacro gnus-sethash (string value hashtable)
  "Set hash value. Arguments are STRING, VALUE, and HASHTABLE."
  ;; We cannot use define-abbrev since it only accepts string as value.
  (` (set (intern (, string) (, hashtable)) (, value))))

;; Note: Macros defined here are also defined in nntp.el. I don't like
;; to put them here, but many users got troubled with the old
;; definitions in nntp.elc. These codes are NNTP 3.10 version.

(defmacro nntp-header-number (header)
  "Return article number in HEADER."
  (` (aref (, header) 0)))

(defmacro nntp-set-header-number (header number)
  "Set article number of HEADER to NUMBER."
  (` (aset (, header) 0 (, number))))

(defmacro nntp-header-subject (header)
  "Return subject string in HEADER."
  (` (aref (, header) 1)))

(defmacro nntp-set-header-subject (header subject)
  "Set article subject of HEADER to SUBJECT."
  (` (aset (, header) 1 (, subject))))

(defmacro nntp-header-from (header)
  "Return author string in HEADER."
  (` (aref (, header) 2)))

(defmacro nntp-set-header-from (header from)
  "Set article author of HEADER to FROM."
  (` (aset (, header) 2 (, from))))

(defmacro nntp-header-xref (header)
  "Return xref string in HEADER."
  (` (aref (, header) 3)))

(defmacro nntp-set-header-xref (header xref)
  "Set article xref of HEADER to xref."
  (` (aset (, header) 3 (, xref))))

(defmacro nntp-header-lines (header)
  "Return lines in HEADER."
  (` (aref (, header) 4)))

(defmacro nntp-set-header-lines (header lines)
  "Set article lines of HEADER to LINES."
  (` (aset (, header) 4 (, lines))))

(defmacro nntp-header-date (header)
  "Return date in HEADER."
  (` (aref (, header) 5)))

(defmacro nntp-set-header-date (header date)
  "Set article date of HEADER to DATE."
  (` (aset (, header) 5 (, date))))

(defmacro nntp-header-id (header)
  "Return Id in HEADER."
  (` (aref (, header) 6)))

(defmacro nntp-set-header-id (header id)
  "Set article Id of HEADER to ID."
  (` (aset (, header) 6 (, id))))

(defmacro nntp-header-references (header)
  "Return references in HEADER."
  (` (aref (, header) 7)))

(defmacro nntp-set-header-references (header ref)
  "Set article references of HEADER to REF."
  (` (aset (, header) 7 (, ref))))


;;;
;;; GNUS Group Mode
;;;

(if gnus-group-mode-map
    nil
  (setq gnus-group-mode-map (make-keymap))
  (suppress-keymap gnus-group-mode-map)
  (define-key gnus-group-mode-map " " 'gnus-group-read-group)
  (define-key gnus-group-mode-map "=" 'gnus-group-select-group)
  (define-key gnus-group-mode-map "j" 'gnus-group-jump-to-group)
  (define-key gnus-group-mode-map "n" 'gnus-group-next-unread-group)
  (define-key gnus-group-mode-map "p" 'gnus-group-prev-unread-group)
  (define-key gnus-group-mode-map "\177" 'gnus-group-prev-unread-group)
  (define-key gnus-group-mode-map "N" 'gnus-group-next-group)
  (define-key gnus-group-mode-map "P" 'gnus-group-prev-group)
  (define-key gnus-group-mode-map "\C-n" 'gnus-group-next-group)
  (define-key gnus-group-mode-map "\C-p" 'gnus-group-prev-group)
  (define-key gnus-group-mode-map [down] 'gnus-group-next-group)
  (define-key gnus-group-mode-map [up] 'gnus-group-prev-group)
  (define-key gnus-group-mode-map "\r" 'next-line)
  ;;(define-key gnus-group-mode-map "/" 'isearch-forward)
  (define-key gnus-group-mode-map "<" 'beginning-of-buffer)
  (define-key gnus-group-mode-map ">" 'end-of-buffer)
  (define-key gnus-group-mode-map "u" 'gnus-group-unsubscribe-current-group)
  (define-key gnus-group-mode-map "U" 'gnus-group-unsubscribe-group)
  (define-key gnus-group-mode-map "c" 'gnus-group-catchup)
  (define-key gnus-group-mode-map "C" 'gnus-group-catchup-all)
  (define-key gnus-group-mode-map "l" 'gnus-group-list-groups)
  (define-key gnus-group-mode-map "L" 'gnus-group-list-all-groups)
  (define-key gnus-group-mode-map "g" 'gnus-group-get-new-news)
  (define-key gnus-group-mode-map "R" 'gnus-group-restart)
  (define-key gnus-group-mode-map "b" 'gnus-group-check-bogus-groups)
  (define-key gnus-group-mode-map "r" 'gnus-group-restrict-groups)
  (define-key gnus-group-mode-map "a" 'gnus-group-post-news)
  (define-key gnus-group-mode-map "\ek" 'gnus-group-edit-local-kill)
  (define-key gnus-group-mode-map "\eK" 'gnus-group-edit-global-kill)
  (define-key gnus-group-mode-map "\C-k" 'gnus-group-kill-group)
  (define-key gnus-group-mode-map "\C-y" 'gnus-group-yank-group)
  (define-key gnus-group-mode-map "\C-w" 'gnus-group-kill-region)
  (define-key gnus-group-mode-map "\C-x\C-t" 'gnus-group-transpose-groups)
  (define-key gnus-group-mode-map "\C-c\C-l" 'gnus-list-killed-groups)
  (define-key gnus-group-mode-map "V" 'gnus-version)
  ;;(define-key gnus-group-mode-map "x" 'gnus-group-force-update)
  (define-key gnus-group-mode-map "s" 'gnus-group-force-update)
  (define-key gnus-group-mode-map "z" 'gnus-group-suspend)
  (define-key gnus-group-mode-map "q" 'gnus-group-exit)
  (define-key gnus-group-mode-map "Q" 'gnus-group-quit)
  (define-key gnus-group-mode-map "?" 'gnus-group-describe-briefly)
  (define-key gnus-group-mode-map "\C-c\C-i" 'gnus-info-find-node)
  (define-key gnus-group-mode-map   [mouse-2] 'gnus-mouse-pick-group)

  ;; Make a menu bar item.
  (define-key gnus-group-mode-map [menu-bar GNUS]
	(cons "GNUS" (make-sparse-keymap "GNUS")))

  (define-key gnus-group-mode-map [menu-bar GNUS force-update]
	'("Force Update" . gnus-group-force-update))
  (define-key gnus-group-mode-map [menu-bar GNUS quit]
	'("Quit" . gnus-group-quit))
  (define-key gnus-group-mode-map [menu-bar GNUS exit]
	'("Exit" . gnus-group-exit))
  (define-key gnus-group-mode-map [menu-bar GNUS restart]
	'("Restart" . gnus-group-restart))
  (define-key gnus-group-mode-map [menu-bar GNUS suspend]
	'("Suspend" . gnus-group-suspend))
  (define-key gnus-group-mode-map [menu-bar GNUS get-new-news]
	'("Get New News" . gnus-group-get-new-news))

  ;; Make a menu bar item.
  (define-key gnus-group-mode-map [menu-bar groups]
	(cons "Groups" (make-sparse-keymap "Groups")))

  (define-key gnus-group-mode-map [menu-bar groups catchup]
	'("Catchup" . gnus-group-catchup))
  (define-key gnus-group-mode-map [menu-bar groups edit-global-kill]
	'("Edit Kill File" . gnus-group-edit-global-kill))

  (define-key gnus-group-mode-map [menu-bar groups separator-2]
	'("--"))

  (define-key gnus-group-mode-map [menu-bar groups yank-group]
	'("Yank Group" . gnus-group-yank-group))
  (define-key gnus-group-mode-map [menu-bar groups kill-group]
	'("Kill Group" . gnus-group-kill-group))

  (define-key gnus-group-mode-map [menu-bar groups separator-1]
	'("--"))

  (define-key gnus-group-mode-map [menu-bar groups jump-to-group]
	'("Jump to Group..." . gnus-group-jump-to-group))
  (define-key gnus-group-mode-map [menu-bar groups list-all-groups]
	'("List All Groups" . gnus-group-list-all-groups))
  (define-key gnus-group-mode-map [menu-bar groups list-groups]
	'("List Groups" . gnus-group-list-groups))
  (define-key gnus-group-mode-map [menu-bar groups unsub-current-group]
	'("Unsubscribe Group" . gnus-group-unsubscribe-current-group))
  )

(defun gnus-group-mode ()
  "Major mode for reading network news.
All normal editing commands are turned off.
Instead, these commands are available:

SPC	Read articles in this newsgroup.
=	Select this newsgroup.
j	Move to the specified newsgroup.
n	Move to the next unread newsgroup.
p	Move to the previous unread newsgroup.
C-n	Move to the next newsgroup.
C-p	Move to the previous newsgroup.
<	Move point to the beginning of this buffer.
>	Move point to the end of this buffer.
u	Unsubscribe from (subscribe to) this newsgroup.
U	Unsubscribe from (subscribe to) the specified newsgroup.
c	Mark all articles as read, preserving marked articles.
C	Mark all articles in this newsgroup as read.
l	Revert this buffer.
L	List all newsgroups.
g	Get new news.
R	Force to read the raw .newsrc file and get new news.
b	Check bogus newsgroups.
r	Restrict visible newsgroups to the current region.
a	Post a new article.
ESC k	Edit a local KILL file applied to this newsgroup.
ESC K	Edit a global KILL file applied to all newsgroups.
C-k	Kill this newsgroup.
C-y	Yank killed newsgroup here.
C-w	Kill newsgroups in current region (excluding current point).
C-x C-t	Exchange this newsgroup and previous newsgroup.
C-c C-l	list killed newsgroups.
s	Save .newsrc file.
z	Suspend reading news.
q	Quit reading news.
Q	Quit reading news without saving .newsrc file.
V	Show the version number of this GNUS.
?	Describe Group Mode commands briefly.
C-h m	Describe Group Mode.
C-c C-i	Read Info about Group Mode.

  The name of the host running NNTP server is asked for if no default
host is specified. It is also possible to choose another NNTP server
even when the default server is defined by giving a prefix argument to
the command `\\[gnus]'.

  If an NNTP server is preceded by a colon such as `:Mail', the user's
private directory `~/Mail' is used as a news spool. This makes it
possible to read mail stored in MH folders or articles saved by GNUS.
File names of mail or articles must consist of only numeric
characters. Otherwise, they are ignored.

  If there is a file named `~/.newsrc-SERVER', it is used as the
startup file instead of standard one when talking to SERVER.  It is
possible to talk to many hosts by using different startup files for
each.

  Option `-n' of the options line in the startup file is recognized
properly the same as the Bnews system. For example, if the options
line is `options -n !talk talk.rumors', newsgroups under the `talk'
hierarchy except for `talk.rumors' are ignored while checking new
newsgroups.

  If there is a file named `~/.signature-DISTRIBUTION', it is used as
signature file instead of standard one when posting a news in
DISTRIBUTION.

  If an Info file generated from `gnus.texinfo' is installed, you can
read an appropriate Info node of the Info file according to the
current major mode of GNUS by \\[gnus-info-find-node].

  The variable `gnus-version', `nntp-version', `nnspool-version', and
`mhspool-version' have the version numbers of this version of gnus.el,
nntp.el, nnspool.el, and mhspoo.el, respectively.

User customizable variables:
 gnus-nntp-server
    Specifies the name of the host running the NNTP server. If its
    value is a string such as `:DIRECTORY', the user's private
    DIRECTORY is used as a news spool.  The variable is initialized
    from the NNTPSERVER environment variable.

 gnus-nntp-service
    Specifies a NNTP service name.  It is usually \"nntp\" or 119.
    Nil forces GNUS to use a local news spool if the variable
    `gnus-nntp-server' is set to the local host name.

 gnus-startup-file
    Specifies a startup file (.newsrc).  If there is a file named
    `.newsrc-SERVER', it's used instead when talking to SERVER.  I
    recommend you to use the server specific file, if you'd like to
    talk to many servers.  Especially if you'd like to read your
    private directory, the name of the file must be
    `.newsrc-:DIRECTORY'.

 gnus-signature-file
    Specifies a signature file (.signature).  If there is a file named
    `.signature-DISTRIBUTION', it's used instead when posting an
    article in DISTRIBUTION.  Set the variable to nil to prevent
    appending the file automatically.  If you use an NNTP inews which
    comes with the NNTP package, you may have to set the variable to
    nil.

 gnus-use-cross-reference
    Specifies what to do with cross references (Xref: field).  If it
    is nil, cross references are ignored.  If it is t, articles in
    subscribed newsgroups are only marked as read.  Otherwise, if it
    is not nil nor t, articles in all newsgroups are marked as read.

 gnus-use-followup-to
    Specifies what to do with followup-to: field.  If it is nil, its
    value is ignored.  If it is non-nil, its value is used as followup
    newsgroups.  Especially, if it is t and field value is `poster',
    your confirmation is required.

 gnus-author-copy
    Specifies a file name to save a copy of article you posted using
    FCC: field.  If the first character of the value is `|', the
    contents of the article is piped out to a program specified by the
    rest of the value.  The variable is initialized from the
    AUTHORCOPY environment variable.

 gnus-author-copy-saver
    Specifies a function to save an author copy.  The function is
    called with a file name.  The default function `rmail-output'
    saves in Unix mail format.

 gnus-kill-file-name
    Use specified file name as a KILL file (default to `KILL').

 gnus-novice-user
    Non-nil means that you are a novice to USENET.  If non-nil,
    verbose messages may be displayed or your confirmations may be
    required.

 gnus-interactive-post
    Non-nil means that newsgroup, subject and distribution are asked
    for interactively when posting a new article.

 gnus-use-full-window
    Non-nil means to take up the entire screen of Emacs.

 gnus-window-configuration
    Specifies the configuration of Group, Summary, and Article
    windows.  It is a list of (ACTION (G S A)), where G, S, and A are
    the relative height of Group, Summary, and Article windows,
    respectively.  ACTION is `summary', `newsgroups', or `article'.

 gnus-subscribe-newsgroup-method
    Specifies a function called with a newsgroup name when new
    newsgroup is found.  The default definition adds new newsgroup at
    the beginning of other newsgroups.

  And more and more.  Please refer to texinfo documentation.

Various hooks for customization:
 gnus-group-mode-hook
    Entry to this mode calls the value with no arguments, if that
    value is non-nil. This hook is called before GNUS is connected to
    the NNTP server. So, you can change or define the NNTP server in
    this hook.

 gnus-startup-hook
    Called with no arguments after the NNTP server is selected. It is
    possible to change the behavior of GNUS or initialize the
    variables according to the selected NNTP server.

 gnus-group-prepare-hook
    Called with no arguments after a newsgroup list is created in the
    Newsgroup buffer, if that value is non-nil.

 gnus-save-newsrc-hook
    Called with no arguments when saving newsrc file if that value is
    non-nil.

 gnus-prepare-article-hook
    Called with no arguments after preparing message body, but before
    preparing header fields which is automatically generated if that
    value is non-nil.  The default hook (gnus-inews-insert-signature)
    inserts a signature file.

 gnus-inews-article-hook
    Called with no arguments when posting an article if that value is
    non-nil.  This hook is called just before posting an article.  The
    default hook does FCC (save an article to the specified file).

 gnus-suspend-gnus-hook
    Called with no arguments when suspending (not exiting) GNUS, if
    that value is non-nil.

 gnus-exit-gnus-hook
    Called with no arguments when exiting (not suspending) GNUS, if
    that value is non-nil."
  (interactive)
  (kill-all-local-variables)
  ;; Gee.  Why don't you upgrade?
  (cond ((boundp 'mode-line-modified)
	 (setq mode-line-modified "--- "))
	((listp (default-value 'mode-line-format))
	 (setq mode-line-format
	       (cons "--- " (cdr (default-value 'mode-line-format)))))
	(t
	 (setq mode-line-format
	       "--- GNUS: List of Newsgroups  %[(%m)%]----%3p-%-")))
  (setq major-mode 'gnus-group-mode)
  (setq mode-name "Newsgroup")
  (setq mode-line-buffer-identification	"GNUS: List of Newsgroups")
  (setq mode-line-process nil)
  (use-local-map gnus-group-mode-map)
  (buffer-flush-undo (current-buffer))
  (setq buffer-read-only t)		;Disable modification
  (run-hooks 'gnus-group-mode-hook))

(defun gnus-mouse-pick-group (e)
  (interactive "e")
  (mouse-set-point e)
  (gnus-group-read-group nil))

;;;###autoload
(defun gnus (&optional confirm)
  "Read network news.
If optional argument CONFIRM is non-nil, ask NNTP server."
  (interactive "P")
  (unwind-protect
      (progn
	(switch-to-buffer (get-buffer-create gnus-group-buffer))
	(gnus-group-mode)
	(gnus-start-news-server confirm))
    (if (not (gnus-server-opened))
	(gnus-group-quit)
      ;; NNTP server is successfully open. 
      (setq mode-line-process (format " {%s}" gnus-nntp-server))
      (let ((buffer-read-only nil))
	(erase-buffer)
	(gnus-group-startup-message)
	(sit-for 0))
      (run-hooks 'gnus-startup-hook)
      (gnus-setup-news)
      (if gnus-novice-user
	  (gnus-group-describe-briefly)) ;Show brief help message.
      (gnus-group-list-groups nil)
      )))

(defun gnus-group-startup-message ()
  "Insert startup message in current buffer."
  ;; Insert the message.
  (insert
   (format "
                   %s

         NNTP-based News Reader for GNU Emacs


If you have any trouble with this software, please let me
know. I will fix your problems in the next release.

Comments, suggestions, and bug fixes are welcome.

Masanobu UMEDA
umerin@mse.kyutech.ac.jp" gnus-version))
  ;; And then hack it.
  ;; 57 is the longest line.
  (indent-rigidly (point-min) (point-max) (/ (max (- (window-width) 57) 0) 2))
  (goto-char (point-min))
  ;; +4 is fuzzy factor.
  (insert-char ?\n (/ (max (- (window-height) 18) 0) 2)))

(defun gnus-group-list-groups (show-all)
  "List newsgroups in the Newsgroup buffer.
If argument SHOW-ALL is non-nil, unsubscribed groups are also listed."
  (interactive "P")
  (let ((case-fold-search nil)
	(last-group			;Current newsgroup.
	 (gnus-group-group-name))
	(next-group			;Next possible newsgroup.
	 (progn
	   (gnus-group-search-forward nil nil)
	   (gnus-group-group-name)))
	(prev-group			;Previous possible newsgroup.
	 (progn
	   (gnus-group-search-forward t nil)
	   (gnus-group-group-name))))
    (set-buffer gnus-group-buffer)	;May call from out of Group buffer
    (gnus-group-prepare show-all)
    (if (zerop (buffer-size))
	(message "No news is good news")
      ;; Go to last newsgroup if possible.  If cannot, try next and
      ;; previous.  If all fail, go to first unread newsgroup.
      (goto-char (point-min))
      (or (and last-group
	       (re-search-forward (gnus-group-make-regexp last-group) nil t))
	  (and next-group
	       (re-search-forward (gnus-group-make-regexp next-group) nil t))
	  (and prev-group
	       (re-search-forward (gnus-group-make-regexp prev-group) nil t))
	  (gnus-group-search-forward nil nil t))
      ;; Adjust cursor point.
      (beginning-of-line)
      (search-forward ":" nil t)
      )))

(defun gnus-group-prepare (&optional all)
  "Prepare list of newsgroups in current buffer.
If optional argument ALL is non-nil, unsubscribed groups are also listed."
  (let ((buffer-read-only nil)
	(newsrc gnus-newsrc-assoc)
	(group-info nil)
	(group-name nil)
	(unread-count 0)
	;; This specifies the format of Group buffer.
	(cntl "%s%s%5d: %s\n"))
    (erase-buffer)
    ;; List newsgroups.
    (while newsrc
      (setq group-info (car newsrc))
      (setq group-name (car group-info))
      (setq unread-count (nth 1 (gnus-gethash group-name gnus-unread-hashtb)))
      (if (or all
	      (and (nth 1 group-info)	;Subscribed.
		   (> unread-count 0)))	;There are unread articles.
	  ;; Yes, I can use gnus-group-prepare-line, but this is faster.
	  (insert
	   (format cntl
		   ;; Subscribed or not.
		   (if (nth 1 group-info) " " "U")
		   ;; Has new news?
		   (if (and (> unread-count 0)
			    (>= 0
				(- unread-count
				   (length
				    (cdr (gnus-gethash group-name
						       gnus-marked-hashtb))))))
		       "*" " ")
		   ;; Number of unread articles.
		   unread-count
		   ;; Newsgroup name.
		   group-name))
	)
      (setq newsrc (cdr newsrc))
      )
    (setq gnus-have-all-newsgroups all)
    (goto-char (point-min))
    (run-hooks 'gnus-group-prepare-hook)
    ))

(defun gnus-group-prepare-line (info)
  "Return a string for the Newsgroup buffer from INFO.
INFO is an element of gnus-newsrc-assoc or gnus-killed-assoc."
  (let* ((group-name (car info))
	 (unread-count
	  (or (nth 1 (gnus-gethash group-name gnus-unread-hashtb))
	      ;; Not in hash table, so compute it now.
	      (gnus-number-of-articles
	       (gnus-difference-of-range
		(nth 2 (gnus-gethash group-name gnus-active-hashtb))
		(nthcdr 2 info)))))
	 ;; This specifies the format of Group buffer.
	 (cntl "%s%s%5d: %s\n"))
    (format cntl
	    ;; Subscribed or not.
	    (if (nth 1 info) " " "U")
	    ;; Has new news?
	    (if (and (> unread-count 0)
		     (>= 0
			 (- unread-count
			    (length
			     (cdr (gnus-gethash group-name
						gnus-marked-hashtb))))))
		"*" " ")
	    ;; Number of unread articles.
	    unread-count
	    ;; Newsgroup name.
	    group-name
	    )))

(defun gnus-group-update-group (group &optional visible-only)
  "Update newsgroup info of GROUP.
If optional argument VISIBLE-ONLY is non-nil, non displayed group is ignored."
  (let ((buffer-read-only nil)
	(case-fold-search nil)		;appleIIgs vs. appleiigs
	(regexp (gnus-group-make-regexp group))
	(visible nil))
    ;; Buffer may be narrowed.
    (save-restriction
      (widen)
      ;; Search a line to modify.  If the buffer is large, the search
      ;; takes long time.  In most cases, current point is on the line
      ;; we are looking for.  So, first of all, check current line. 
      ;; And then if current point is in the first half, search from
      ;; the beginning.  Otherwise, search from the end.
      (if (cond ((progn
		   (beginning-of-line)
		   (looking-at regexp)))
		((and (> (/ (buffer-size) 2) (point)) ;In the first half.
		      (progn
			(goto-char (point-min))
			(re-search-forward regexp nil t))))
		((progn
		   (goto-char (point-max))
		   (re-search-backward regexp nil t))))
	  ;; GROUP is listed in current buffer. So, delete old line.
	  (progn
	    (setq visible t)
	    (beginning-of-line)
	    (delete-region (point) (progn (forward-line 1) (point)))
	    )
	;; No such line in the buffer, so insert it at the top.
	(goto-char (point-min)))
      (if (or visible (not visible-only))
	  (progn
	    (insert (gnus-group-prepare-line
		     (gnus-gethash group gnus-newsrc-hashtb)))
	    (forward-line -1)		;Move point on that line.
	    ))
      )))

(defun gnus-group-group-name ()
  "Get newsgroup name around point."
  (save-excursion
    (beginning-of-line)
    (if (looking-at "^.+:[ \t]+\\([^ \t\n]+\\)\\([ \t].*\\|$\\)")
	(buffer-substring (match-beginning 1) (match-end 1))
      )))

(defun gnus-group-make-regexp (newsgroup)
  "Return regexp that matches for a line of NEWSGROUP."
  (concat "^.+: " (regexp-quote newsgroup) "\\([ \t].*\\|$\\)"))

(defun gnus-group-search-forward (backward norest &optional heretoo)
  "Search for the next (or previous) newsgroup.
If 1st argument BACKWARD is non-nil, search backward instead.
If 2nd argument NOREST is non-nil, don't care about newsgroup property.
If optional argument HERETOO is non-nil, current line is searched for, too."
  (let ((case-fold-search nil)
	(func
	 (if backward
	     (function re-search-backward) (function re-search-forward)))
	(regexp
	 (format "^%s[ \t]*\\(%s\\):"
		 (if norest ".." " [ \t]")
		 (if norest "[0-9]+" "[1-9][0-9]*")))
	(found nil))
    (if backward
	(if heretoo
	    (end-of-line)
	  (beginning-of-line))
      (if heretoo
	  (beginning-of-line)
	(end-of-line)))
    (setq found (funcall func regexp nil t))
    ;; Adjust cursor point.
    (beginning-of-line)
    (search-forward ":" nil t)
    ;; Return T if found.
    found
    ))

;; GNUS Group mode command

(defun gnus-group-read-group (all &optional no-article)
  "Read news in this newsgroup.
If argument ALL is non-nil, already read articles become readable.
If optional argument NO-ARTICLE is non-nil, no article body is displayed."
  (interactive "P")
  (let ((group (gnus-group-group-name))) ;Newsgroup name to read.
    (if group
	(gnus-summary-read-group
	 group
	 (or all
	     ;;(not (nth 1 (gnus-gethash group gnus-newsrc-hashtb))) ;Unsubscribed
	     (zerop
	      (nth 1 (gnus-gethash group gnus-unread-hashtb))))	;No unread
	 no-article
	 ))
    ))

(defun gnus-group-select-group (all)
  "Select this newsgroup.
No article is selected automatically.
If argument ALL is non-nil, already read articles become readable."
  (interactive "P")
  (gnus-group-read-group all t))

(defun gnus-group-jump-to-group (group)
  "Jump to newsgroup GROUP."
  (interactive
   (list (completing-read "Newsgroup: " gnus-newsrc-assoc nil 'require-match)))
  (let ((case-fold-search nil))
    (goto-char (point-min))
    (or (re-search-forward (gnus-group-make-regexp group) nil t)
	(if (gnus-gethash group gnus-newsrc-hashtb)
	    ;; Add GROUP entry, then seach again.
	    (gnus-group-update-group group)))
    ;; Adjust cursor point.
    (beginning-of-line)
    (search-forward ":" nil t)
    ))

(defun gnus-group-next-group (n)
  "Go to next N'th newsgroup."
  (interactive "p")
  (while (and (> n 1)
	      (gnus-group-search-forward nil t))
    (setq n (1- n)))
  (or (gnus-group-search-forward nil t)
      (message "No more newsgroups")))

(defun gnus-group-next-unread-group (n)
  "Go to next N'th unread newsgroup."
  (interactive "p")
  (while (and (> n 1)
	      (gnus-group-search-forward nil nil))
    (setq n (1- n)))
  (or (gnus-group-search-forward nil nil)
      (message "No more unread newsgroups")))

(defun gnus-group-prev-group (n)
  "Go to previous N'th newsgroup."
  (interactive "p")
  (while (and (> n 1)
	      (gnus-group-search-forward t t))
    (setq n (1- n)))
  (or (gnus-group-search-forward t t)
      (message "No more newsgroups")))

(defun gnus-group-prev-unread-group (n)
  "Go to previous N'th unread newsgroup."
  (interactive "p")
  (while (and (> n 1)
	      (gnus-group-search-forward t nil))	      
    (setq n (1- n)))
  (or (gnus-group-search-forward t nil)
      (message "No more unread newsgroups")))

(defun gnus-group-catchup (all)
  "Mark all articles not marked as unread in current newsgroup as read.
If prefix argument ALL is non-nil, all articles are marked as read.
Cross references (Xref: field) of articles are ignored."
  (interactive "P")
  (let* ((group (gnus-group-group-name))
         (marked (if (not all)
		     (cdr (gnus-gethash group gnus-marked-hashtb)))))
    (and group
	 (or (not gnus-interactive-catchup) ;Without confirmation?
	     (y-or-n-p
	      (if all
		  "Do you really want to mark everything as read? "
		"Delete all articles not marked as read? ")))
	 (progn
	   (message "")			;Clear "Yes or No" question.
	   ;; Any marked articles will be preserved.
	   (gnus-update-unread-articles group marked marked)
	   (gnus-group-update-group group)
	   (gnus-group-next-group 1)))
    ))

(defun gnus-group-catchup-all ()
  "Mark all articles in current newsgroup as read.
Cross references (Xref: field) of articles are ignored."
  (interactive)
  (gnus-group-catchup t))

(defun gnus-group-unsubscribe-current-group ()
  "Toggle subscribe from/to unsubscribe current group."
  (interactive)
  (let ((group (gnus-group-group-name)))
    (if group
	 (progn
	   (gnus-group-unsubscribe-group group)
	   (gnus-group-next-group 1))
      (message "No Newsgroup found to \(un\)subscribe"))))

(defun gnus-group-unsubscribe-group (group)
  "Toggle subscribe from/to unsubscribe GROUP.
New newsgroup is added to .newsrc automatically."
  (interactive
   (list (completing-read "Newsgroup: "
			  gnus-active-hashtb nil 'require-match)))
  (let ((newsrc (gnus-gethash group gnus-newsrc-hashtb)))
    (cond ((not (null newsrc))
	   ;; Toggle subscription flag.
	   (setcar (nthcdr 1 newsrc) (not (nth 1 newsrc)))
	   (gnus-update-newsrc-buffer group)
	   (gnus-group-update-group group)
	   ;; Adjust cursor point.
	   (beginning-of-line)
	   (search-forward ":" nil t))
	  ((and (stringp group)
		(gnus-gethash group gnus-active-hashtb))
	   ;; Add new newsgroup.
	   (gnus-add-newsgroup group)
	   (gnus-group-update-group group)
	   ;; Adjust cursor point.
	   (beginning-of-line)
	   (search-forward ":" nil t))
	  (t (error "No such newsgroup: %s" group)))
    ))

(defun gnus-group-list-all-groups ()
  "List all of newsgroups in the Newsgroup buffer."
  (interactive)
  (message "Listing all groups...")
  (gnus-group-list-groups t)
  (message "Listing all groups...done"))

(defun gnus-group-get-new-news ()
  "Get newly arrived articles. In fact, read the active file again."
  (interactive)
  (gnus-setup-news)
  (gnus-group-list-groups gnus-have-all-newsgroups))

(defun gnus-group-restart ()
  "Force GNUS to read the raw startup file."
  (interactive)
  (gnus-save-newsrc-file)
  (gnus-setup-news t)			;Force to read the raw startup file.
  (gnus-group-list-groups gnus-have-all-newsgroups))

(defun gnus-group-check-bogus-groups ()
  "Check bogus newsgroups."
  (interactive)
  (gnus-check-bogus-newsgroups t)	;Require confirmation.
  (gnus-group-list-groups gnus-have-all-newsgroups))

(defun gnus-group-restrict-groups (start end)
  "Restrict visible newsgroups to the current region (START and END).
Type \\[widen] to remove restriction."
  (interactive "r")
  (save-excursion
    (narrow-to-region (progn
			(goto-char start)
			(beginning-of-line)
			(point))
		      (progn
			(goto-char end)
			(forward-line 1)
			(point))))
  (message (substitute-command-keys "Type \\[widen] to remove restriction")))

(defun gnus-group-edit-global-kill ()
  "Edit a global KILL file."
  (interactive)
  (setq gnus-current-kill-article nil)	;No articles selected.
  (gnus-kill-file-edit-file nil) 	;Nil stands for global KILL file.
  (message
   (substitute-command-keys
    "Editing a global KILL file (Type \\[gnus-kill-file-exit] to exit)")))

(defun gnus-group-edit-local-kill ()
  "Edit a local KILL file."
  (interactive)
  (setq gnus-current-kill-article nil)	;No articles selected.
  (gnus-kill-file-edit-file (gnus-group-group-name))
  (message
   (substitute-command-keys
    "Editing a local KILL file (Type \\[gnus-kill-file-exit] to exit)")))

(defun gnus-group-force-update ()
  "Update `.newsrc' file."
  (interactive)
  (gnus-save-newsrc-file))

(defun gnus-group-suspend ()
  "Suspend the current GNUS session.
In fact, cleanup buffers except for Group Mode buffer.
The hook gnus-suspend-gnus-hook is called before actually suspending."
  (interactive)
  (run-hooks 'gnus-suspend-gnus-hook)
  ;; Kill GNUS buffers except for Group Mode buffer.
  (let ((buffers gnus-buffer-list)
	(group-buf (get-buffer gnus-group-buffer)))
    (while buffers
      (and (not (eq (car buffers) gnus-group-buffer))
	   (get-buffer (car buffers))
	   (kill-buffer (car buffers)))
      (setq buffers (cdr buffers))
      )
    (bury-buffer group-buf)
    (delete-windows-on group-buf t)))

(defun gnus-group-exit ()
  "Quit reading news after updating .newsrc.
The hook gnus-exit-gnus-hook is called before actually quitting."
  (interactive)
  (if (or noninteractive		;For gnus-batch-kill
	  (zerop (buffer-size))		;No news is good news.
	  (not (gnus-server-opened))	;NNTP connection closed.
	  (not gnus-interactive-exit)	;Without confirmation
	  (y-or-n-p "Are you sure you want to quit reading news? "))
      (progn
	(message "")			;Erase "Yes or No" question.
	(run-hooks 'gnus-exit-gnus-hook)
	(gnus-save-newsrc-file)
	(gnus-clear-system)
	(gnus-close-server))
    ))

(defun gnus-group-quit ()
  "Quit reading news without updating .newsrc.
The hook gnus-exit-gnus-hook is called before actually quitting."
  (interactive)
  (if (or noninteractive		;For gnus-batch-kill
	  (zerop (buffer-size))
	  (not (gnus-server-opened))
	  (yes-or-no-p
	   (format "Quit reading news without saving %s? "
		   (file-name-nondirectory gnus-current-startup-file))))
      (progn
	(message "")			;Erase "Yes or No" question.
	(run-hooks 'gnus-exit-gnus-hook)
	(gnus-clear-system)
	(gnus-close-server))
    ))

(defun gnus-group-describe-briefly ()
  "Describe Group mode commands briefly."
  (interactive)
  (message
   (concat
    (substitute-command-keys "\\[gnus-group-read-group]:Select  ")
    (substitute-command-keys "\\[gnus-group-next-unread-group]:Forward  ")
    (substitute-command-keys "\\[gnus-group-prev-unread-group]:Backward  ")
    (substitute-command-keys "\\[gnus-group-exit]:Exit  ")
    (substitute-command-keys "\\[gnus-info-find-node]:Run Info  ")
    (substitute-command-keys "\\[gnus-group-describe-briefly]:This help")
    )))


;;;
;;; GNUS Summary Mode
;;;

(if gnus-summary-mode-map
    nil
  (setq gnus-summary-mode-map (make-keymap))
  (suppress-keymap gnus-summary-mode-map)
  (define-key gnus-summary-mode-map "\C-c\C-v" 'gnus-uu-ctl-map)
  (define-key gnus-summary-mode-map "#" 'gnus-uu-mark-article)
  (define-key gnus-summary-mode-map " " 'gnus-summary-next-page)
  (define-key gnus-summary-mode-map "\177" 'gnus-summary-prev-page)
  (define-key gnus-summary-mode-map "\r" 'gnus-summary-scroll-up)
  (define-key gnus-summary-mode-map "n" 'gnus-summary-next-unread-article)
  (define-key gnus-summary-mode-map "p" 'gnus-summary-prev-unread-article)
  (define-key gnus-summary-mode-map "N" 'gnus-summary-next-article)
  (define-key gnus-summary-mode-map "P" 'gnus-summary-prev-article)
  (define-key gnus-summary-mode-map "\e\C-n" 'gnus-summary-next-same-subject)
  (define-key gnus-summary-mode-map "\e\C-p" 'gnus-summary-prev-same-subject)
  ;;(define-key gnus-summary-mode-map "\e\C-n" 'gnus-summary-next-unread-same-subject)
  ;;(define-key gnus-summary-mode-map "\e\C-p" 'gnus-summary-prev-unread-same-subject)
  (define-key gnus-summary-mode-map "\C-c\C-n" 'gnus-summary-next-digest)
  (define-key gnus-summary-mode-map "\C-c\C-p" 'gnus-summary-prev-digest)
  (define-key gnus-summary-mode-map "\C-n" 'gnus-summary-next-subject)
  (define-key gnus-summary-mode-map "\C-p" 'gnus-summary-prev-subject)
  (define-key gnus-summary-mode-map [down] 'gnus-summary-next-subject)
  (define-key gnus-summary-mode-map [up] 'gnus-summary-prev-subject)
  (define-key gnus-summary-mode-map "\en" 'gnus-summary-next-unread-subject)
  (define-key gnus-summary-mode-map "\ep" 'gnus-summary-prev-unread-subject)
  ;;(define-key gnus-summary-mode-map "\C-cn" 'gnus-summary-next-group)
  ;;(define-key gnus-summary-mode-map "\C-cp" 'gnus-summary-prev-group)
  (define-key gnus-summary-mode-map "." 'gnus-summary-first-unread-article)
  ;;(define-key gnus-summary-mode-map "/" 'isearch-forward)
  (define-key gnus-summary-mode-map "s" 'gnus-summary-isearch-article)
  (define-key gnus-summary-mode-map "\es" 'gnus-summary-search-article-forward)
  ;;(define-key gnus-summary-mode-map "\eS" 'gnus-summary-search-article-backward)
  (define-key gnus-summary-mode-map "\er" 'gnus-summary-search-article-backward)
  (define-key gnus-summary-mode-map "<" 'gnus-summary-beginning-of-article)
  (define-key gnus-summary-mode-map ">" 'gnus-summary-end-of-article)
  (define-key gnus-summary-mode-map "j" 'gnus-summary-goto-subject)
  ;;(define-key gnus-summary-mode-map "J" 'gnus-summary-goto-article)
  (define-key gnus-summary-mode-map "l" 'gnus-summary-goto-last-article)
  (define-key gnus-summary-mode-map "^" 'gnus-summary-refer-parent-article)
  ;;(define-key gnus-summary-mode-map "\er" 'gnus-summary-refer-article)
  (define-key gnus-summary-mode-map "\e^" 'gnus-summary-refer-article)
  (define-key gnus-summary-mode-map "u" 'gnus-summary-mark-as-unread-forward)
  (define-key gnus-summary-mode-map "U" 'gnus-summary-mark-as-unread-backward)
  (define-key gnus-summary-mode-map "d" 'gnus-summary-mark-as-read-forward)
  (define-key gnus-summary-mode-map "D" 'gnus-summary-mark-as-read-backward)
  (define-key gnus-summary-mode-map "\eu" 'gnus-summary-clear-mark-forward)
  (define-key gnus-summary-mode-map "\eU" 'gnus-summary-clear-mark-backward)
  (define-key gnus-summary-mode-map "k" 'gnus-summary-kill-same-subject-and-select)
  (define-key gnus-summary-mode-map "\C-k" 'gnus-summary-kill-same-subject)
  (define-key gnus-summary-mode-map "\e\C-t" 'gnus-summary-toggle-threads)
  (define-key gnus-summary-mode-map "\e\C-s" 'gnus-summary-show-thread)
  (define-key gnus-summary-mode-map "\e\C-h" 'gnus-summary-hide-thread)
  (define-key gnus-summary-mode-map "\e\C-f" 'gnus-summary-next-thread)
  (define-key gnus-summary-mode-map "\e\C-b" 'gnus-summary-prev-thread)
  (define-key gnus-summary-mode-map "\e\C-u" 'gnus-summary-up-thread)
  (define-key gnus-summary-mode-map "\e\C-d" 'gnus-summary-down-thread)
  (define-key gnus-summary-mode-map "\e\C-k" 'gnus-summary-kill-thread)
  (define-key gnus-summary-mode-map "&" 'gnus-summary-execute-command)
  ;;(define-key gnus-summary-mode-map "c" 'gnus-summary-catchup)
  ;;(define-key gnus-summary-mode-map "c" 'gnus-summary-catchup-all)
  (define-key gnus-summary-mode-map "c" 'gnus-summary-catchup-and-exit)
  ;;(define-key gnus-summary-mode-map "c" 'gnus-summary-catchup-all-and-exit)
  (define-key gnus-summary-mode-map "\C-t" 'gnus-summary-toggle-truncation)
  (define-key gnus-summary-mode-map "x" 'gnus-summary-delete-marked-as-read)
  (define-key gnus-summary-mode-map "X" 'gnus-summary-delete-marked-with)
  (define-key gnus-summary-mode-map "\C-c\C-sn" 'gnus-summary-sort-by-number)
  (define-key gnus-summary-mode-map "\C-c\C-sa" 'gnus-summary-sort-by-author)
  (define-key gnus-summary-mode-map "\C-c\C-ss" 'gnus-summary-sort-by-subject)
  (define-key gnus-summary-mode-map "\C-c\C-sd" 'gnus-summary-sort-by-date)
  (define-key gnus-summary-mode-map "\C-c\C-s\C-n" 'gnus-summary-sort-by-number)
  (define-key gnus-summary-mode-map "\C-c\C-s\C-a" 'gnus-summary-sort-by-author)
  (define-key gnus-summary-mode-map "\C-c\C-s\C-s" 'gnus-summary-sort-by-subject)
  (define-key gnus-summary-mode-map "\C-c\C-s\C-d" 'gnus-summary-sort-by-date)
  (define-key gnus-summary-mode-map "=" 'gnus-summary-expand-window)
  ;;(define-key gnus-summary-mode-map "G" 'gnus-summary-reselect-current-group)
  (define-key gnus-summary-mode-map "\C-x\C-s" 'gnus-summary-reselect-current-group)
  (define-key gnus-summary-mode-map "w" 'gnus-summary-stop-page-breaking)
  (define-key gnus-summary-mode-map "\C-c\C-r" 'gnus-summary-caesar-message)
  (define-key gnus-summary-mode-map "g" 'gnus-summary-show-article)
  (define-key gnus-summary-mode-map "t" 'gnus-summary-toggle-header)
  ;;(define-key gnus-summary-mode-map "v" 'gnus-summary-show-all-headers)
  (define-key gnus-summary-mode-map "\et" 'gnus-summary-toggle-mime)
  (define-key gnus-summary-mode-map "\C-d" 'gnus-summary-rmail-digest)
  (define-key gnus-summary-mode-map "a" 'gnus-summary-post-news)
  (define-key gnus-summary-mode-map "f" 'gnus-summary-followup)
  (define-key gnus-summary-mode-map "F" 'gnus-summary-followup-with-original)
  (define-key gnus-summary-mode-map "C" 'gnus-summary-cancel-article)
  (define-key gnus-summary-mode-map "r" 'gnus-summary-reply)
  (define-key gnus-summary-mode-map "R" 'gnus-summary-reply-with-original)
  (define-key gnus-summary-mode-map "\C-c\C-f" 'gnus-summary-mail-forward)
  (define-key gnus-summary-mode-map "m" 'gnus-summary-mail-other-window)
  (define-key gnus-summary-mode-map "o" 'gnus-summary-save-article)
  (define-key gnus-summary-mode-map "\C-o" 'gnus-summary-save-in-mail)
  (define-key gnus-summary-mode-map "|" 'gnus-summary-pipe-output)
  (define-key gnus-summary-mode-map "\ek" 'gnus-summary-edit-local-kill)
  (define-key gnus-summary-mode-map "\eK" 'gnus-summary-edit-global-kill)
  (define-key gnus-summary-mode-map "V" 'gnus-version)
  (define-key gnus-summary-mode-map "q" 'gnus-summary-exit)
  (define-key gnus-summary-mode-map "Q" 'gnus-summary-quit)
  (define-key gnus-summary-mode-map "?" 'gnus-summary-describe-briefly)
  (define-key gnus-summary-mode-map "\C-c\C-i" 'gnus-info-find-node)
  (define-key gnus-summary-mode-map [mouse-2] 'gnus-mouse-pick-article)

  (define-key gnus-summary-mode-map [menu-bar misc]
	(cons "Misc" (make-sparse-keymap "misc")))

  (define-key gnus-summary-mode-map [menu-bar misc caesar-message]
	'("Caesar Message" . gnus-summary-caesar-message))
  (define-key gnus-summary-mode-map [menu-bar misc cancel-article]
	'("Cancel Article" . gnus-summary-cancel-article))
  (define-key gnus-summary-mode-map [menu-bar misc edit-local-kill]
	'("Edit Kill File" . gnus-summary-edit-local-kill))

  (define-key gnus-summary-mode-map [menu-bar misc mark-as-unread]
	'("Mark as Unread" . gnus-summary-mark-as-unread-forward))
  (define-key gnus-summary-mode-map [menu-bar misc mark-as-read]
	'("Mark as Read" . gnus-summary-mark-as-read))

  (define-key gnus-summary-mode-map [menu-bar misc quit]
	'("Quit Group" . gnus-summary-quit))
  (define-key gnus-summary-mode-map [menu-bar misc exit]
	'("Exit Group" . gnus-summary-exit))

  (define-key gnus-summary-mode-map [menu-bar sort]
	(cons "Sort" (make-sparse-keymap "sort")))

  (define-key gnus-summary-mode-map [menu-bar sort sort-by-author]
	'("Sort by Author" . gnus-summary-sort-by-author))
  (define-key gnus-summary-mode-map [menu-bar sort sort-by-date]
	'("Sort by Date" . gnus-summary-sort-by-date))
  (define-key gnus-summary-mode-map [menu-bar sort sort-by-number]
	'("Sort by Number" . gnus-summary-sort-by-number))
  (define-key gnus-summary-mode-map [menu-bar sort sort-by-subject]
	'("Sort by Subject" . gnus-summary-sort-by-subject))

  (define-key gnus-summary-mode-map [menu-bar show/hide]
	(cons "Show/Hide" (make-sparse-keymap "show/hide")))

  (define-key gnus-summary-mode-map [menu-bar show/hide hide-all-threads]
	'("Hide All Threads" . gnus-summary-hide-all-threads))
  (define-key gnus-summary-mode-map [menu-bar show/hide hide-thread]
	'("Hide Thread" . gnus-summary-hide-thread))
  (define-key gnus-summary-mode-map [menu-bar show/hide show-all-threads]
	'("Show All Threads" . gnus-summary-show-all-threads))
  (define-key gnus-summary-mode-map [menu-bar show/hide show-all-headers]
	'("Show All Headers" . gnus-summary-show-all-headers))
  (define-key gnus-summary-mode-map [menu-bar show/hide show-thread]
	'("Show Thread" . gnus-summary-show-thread))
  (define-key gnus-summary-mode-map [menu-bar show/hide show-article]
	'("Show Article" . gnus-summary-show-article))
  (define-key gnus-summary-mode-map [menu-bar show/hide toggle-truncation]
	'("Toggle Truncation" . gnus-summary-toggle-truncation))
  (define-key gnus-summary-mode-map [menu-bar show/hide toggle-mime]
	'("Toggle Mime" . gnus-summary-toggle-mime))
  (define-key gnus-summary-mode-map [menu-bar show/hide toggle-header]
	'("Toggle Header" . gnus-summary-toggle-header))

  (define-key gnus-summary-mode-map [menu-bar action]
	(cons "Action" (make-sparse-keymap "action")))

  (define-key gnus-summary-mode-map [menu-bar action kill-same-subject]
	'("Kill Same Subject" . gnus-summary-kill-same-subject))
  (define-key gnus-summary-mode-map [menu-bar action kill-thread]
	'("Kill Thread" . gnus-summary-kill-thread))
  (define-key gnus-summary-mode-map [menu-bar action delete-marked-with]
	'("Delete Marked With" . gnus-summary-delete-marked-with))
  (define-key gnus-summary-mode-map [menu-bar action delete-marked-as-read]
	'("Delete Marked As Read" . gnus-summary-delete-marked-as-read))
  (define-key gnus-summary-mode-map [menu-bar action catchup-and-exit]
	'("Catchup And Exit" . gnus-summary-catchup-and-exit))
  (define-key gnus-summary-mode-map [menu-bar action catchup-to-here]
	'("Catchup to Here" . gnus-summary-catchup-to-here))

  (define-key gnus-summary-mode-map [menu-bar action ignore]
    '("---"))

  (define-key gnus-summary-mode-map [menu-bar action save-in-file]
	'("Save in File" . gnus-summary-save-in-file))
  (define-key gnus-summary-mode-map [menu-bar action save-article]
	'("Save Article" . gnus-summary-save-article))

  (define-key gnus-summary-mode-map [menu-bar action lambda]
    '("---"))

  (define-key gnus-summary-mode-map [menu-bar action forward]
	'("Forward" . gnus-summary-mail-forward))
  (define-key gnus-summary-mode-map [menu-bar action followup-with-original]
	'("Followup with Original" . gnus-summary-followup-with-original))
  (define-key gnus-summary-mode-map [menu-bar action followup]
	'("Followup" . gnus-summary-followup))
  (define-key gnus-summary-mode-map [menu-bar action reply-with-original]
	'("Reply with Original" . gnus-summary-reply-with-original))
  (define-key gnus-summary-mode-map [menu-bar action reply]
	'("Reply" . gnus-summary-reply))
  (define-key gnus-summary-mode-map [menu-bar action post]
	'("Post News" . gnus-summary-post-news))

  (define-key gnus-summary-mode-map [menu-bar move]
	(cons "Move" (make-sparse-keymap "move")))

  (define-key gnus-summary-mode-map [menu-bar move isearch-article]
	'("Search in Article" . gnus-summary-isearch-article))
  (define-key gnus-summary-mode-map [menu-bar move search-through-articles]
	'("Search through Articles" . gnus-summary-search-article-forward))
  (define-key gnus-summary-mode-map [menu-bar move down-thread]
	'("Down Thread" . gnus-summary-down-thread))
  (define-key gnus-summary-mode-map [menu-bar move prev-same-subject]
	'("Prev Same Subject" . gnus-summary-prev-same-subject))
  (define-key gnus-summary-mode-map [menu-bar move prev-group]
	'("Prev Group" . gnus-summary-prev-group))
  (define-key gnus-summary-mode-map [menu-bar move next-unread-same-subject]
	'("Next Unread Same Subject" . gnus-summary-next-unread-same-subject))
  (define-key gnus-summary-mode-map [menu-bar move next-unread-article]
	'("Next Unread Article" . gnus-summary-next-unread-article))
  (define-key gnus-summary-mode-map [menu-bar move next-thread]
	'("Next Thread" . gnus-summary-next-thread))
  (define-key gnus-summary-mode-map [menu-bar move next-group]
	'("Next Group" . gnus-summary-next-group))
  (define-key gnus-summary-mode-map [menu-bar move first-unread-article]
	'("First Unread Article" . gnus-summary-first-unread-article))
  )


(defun gnus-summary-mode ()
  "Major mode for reading articles in this newsgroup.
All normal editing commands are turned off.
Instead, these commands are available:

SPC	Scroll to the next page of the current article.  The next unread
	article is selected automatically at the end of the message.
DEL	Scroll to the previous page of the current article.
RET	Scroll up (or down) one line the current article.
n	Move to the next unread article.
p	Move to the previous unread article.
N	Move to the next article.
P	Move to the previous article.
ESC C-n	Move to the next article which has the same subject as the
	current article.
ESC C-p	Move to the previous article which has the same subject as the
	current article.
\\[gnus-summary-next-unread-same-subject]
	Move to the next unread article which has the same subject as the
	current article.
\\[gnus-summary-prev-unread-same-subject]
	Move to the previous unread article which has the same subject as
	the current article.
C-c C-n	Scroll to the next digested message of the current article.
C-c C-p	Scroll to the previous digested message of the current article.
C-n	Move to the next subject.
C-p	Move to the previous subject.
ESC n	Move to the next unread subject.
ESC p	Move to the previous unread subject.
\\[gnus-summary-next-group]
	Exit the current newsgroup and select the next unread newsgroup.
\\[gnus-summary-prev-group]
	Exit the current newsgroup and select the previous unread newsgroup.
.	Jump to the first unread article in the current newsgroup.
s	Do an incremental search forward on the current article.
ESC s	Search for an article containing a regexp forward.
ESC r	Search for an article containing a regexp backward.
<	Move point to the beginning of the current article.
>	Move point to the end of the current article.
j	Jump to the article specified by the numeric article ID.
l	Jump to the article you read last.
^	Refer to parent of the current article.
ESC ^	Refer to the article specified by the Message-ID.
u	Mark the current article as unread, and go forward.
U	Mark the current article as unread, and go backward.
d	Mark the current article as read, and go forward.
D	Mark the current article as read, and go backward.
ESC u	Clear the current article's mark, and go forward.
ESC U	Clear the current article's mark, and go backward.
k	Mark articles which has the same subject as the current article as
	read, and then select the next unread article.
C-k	Mark articles which has the same subject as the current article as
	read.
ESC k	Edit a local KILL file applied to the current newsgroup.
ESC K	Edit a global KILL file applied to all newsgroups.
ESC C-t	Toggle showing conversation threads.
ESC C-s	Show thread subtrees.
ESC C-h	Hide thread subtrees.
\\[gnus-summary-show-all-threads]	Show all thread subtrees.
\\[gnus-summary-hide-all-threads]	Hide all thread subtrees.
ESC C-f	Go to the same level next thread.
ESC C-b	Go to the same level previous thread.
ESC C-d	Go downward current thread.
ESC C-u	Go upward current thread.
ESC C-k	Mark articles under current thread as read.
&	Execute a command for each article conditionally.
\\[gnus-summary-catchup]
	Mark all articles as read in the current newsgroup, preserving
	articles marked as unread.
\\[gnus-summary-catchup-all]
	Mark all articles as read in the current newsgroup.
\\[gnus-summary-catchup-and-exit]
	Catch up all articles not marked as unread, and then exit the
	current newsgroup.
\\[gnus-summary-catchup-all-and-exit]
	Catch up all articles, and then exit the current newsgroup.
C-t	Toggle truncations of subject lines.
x	Delete subject lines marked as read.
X	Delete subject lines with the specific marks.
C-c C-s C-n	Sort subjects by article number.
C-c C-s C-a	Sort subjects by article author.
C-c C-s C-s	Sort subjects alphabetically.
C-c C-s C-d	Sort subjects by date.
=	Expand Summary window to show headers full window.
C-x C-s	Reselect the current newsgroup. Prefix argument means to select all.
w	Stop page breaking by linefeed.
C-c C-r	Caesar rotates letters by 13/47 places.
g	Force to show the current article.
t	Show original article header if pruned header currently shown, or
	vice versa.
ESC-t	Toggle MIME processing.
C-d	Run RMAIL on the current digest article.
a	Post a new article.
f	Post a reply article.
F	Post a reply article with original article.
C	Cancel the current article.
r	Mail a message to the author.
R	Mail a message to the author with original author.
C-c C-f	Forward the current message to another user.
m	Mail a message in other window.
o	Save the current article in your favorite format.
C-o	Append the current article to a file in Unix mail format.
|	Pipe the contents of the current article to a subprocess.
q	Quit reading news in the current newsgroup.
Q	Quit reading news without recording unread articles information.
V	Show the version number of this GNUS.
?	Describe Summary mode commands briefly.
C-h m	Describe Summary mode.
C-c C-i	Read Info about Summary mode.

User customizable variables:
 gnus-large-newsgroup
    The number of articles which indicates a large newsgroup. If the
    number of articles in a newsgroup is greater than the value, the
    number of articles to be selected is asked for. If the given value
    N is positive, the last N articles is selected. If N is negative,
    the first N articles are selected. An empty string means to select
    all articles.

 gnus-use-long-file-name
    Non-nil means that a newsgroup name is used as a default file name
    to save articles to. If it's nil, the directory form of a
    newsgroup is used instead.

 gnus-default-article-saver
    Specifies your favorite article saver which is interactively
    funcallable. Following functions are available:

	gnus-summary-save-in-rmail (in Rmail format)
	gnus-summary-save-in-mail (in Unix mail format)
	gnus-summary-save-in-folder (in MH folder)
	gnus-summary-save-in-file (in article format).

 gnus-rmail-save-name
 gnus-mail-save-name
 gnus-folder-save-name
 gnus-file-save-name
    Specifies a function generating a file name to save articles in
    specified format.  The function is called with NEWSGROUP, HEADERS,
    and optional LAST-FILE.  Access macros to the headers are defined
    as nntp-header-FIELD, and functions are defined as
    gnus-header-FIELD.

 gnus-article-save-directory
    Specifies a directory name to save articles to using the commands
    gnus-summary-save-in-rmail, gnus-summary-save-in-mail and
    gnus-summary-save-in-file. The variable is initialized from the
    SAVEDIR environment variable.

 gnus-kill-files-directory
    Specifies a directory name to save KILL files to using the commands
    gnus-edit-global-kill, and gnus-edit-local-kill. The variable is
    initialized from the SAVEDIR environment variable.

 gnus-show-all-headers
    Non-nil means that all headers of an article are shown.

 gnus-save-all-headers
    Non-nil means that all headers of an article are saved in a file.

 gnus-show-mime
    Non-nil means that show a MIME message.

 gnus-show-threads
    Non-nil means that conversation threads are shown in tree structure.

 gnus-thread-hide-subject
    Non-nil means that subjects for thread subtrees are hidden.

 gnus-thread-hide-subtree
    Non-nil means that thread subtrees are hidden initially.

 gnus-thread-hide-killed
    Non-nil means that killed thread subtrees are hidden automatically.

 gnus-thread-ignore-subject
    Non-nil means that subject differences are ignored in constructing
    thread trees.

 gnus-thread-indent-level
    Indentation of thread subtrees.

 gnus-optional-headers
    Specifies a function which generates an optional string displayed
    in the Summary buffer. The function is called with an article
    HEADERS.  The result must be a string excluding `[' and `]'.  The
    default function returns a string like NNN:AUTHOR, where NNN is
    the number of lines in an article and AUTHOR is the name of the
    author.

 gnus-auto-extend-newsgroup
    Non-nil means visible articles are extended to forward and
    backward automatically if possible.

 gnus-auto-select-first
    Non-nil means the first unread article is selected automagically
    when a newsgroup is selected normally (by gnus-group-read-group).
    If you'd like to prevent automatic selection of the first unread
    article in some newsgroups, set the variable to nil in
    gnus-select-group-hook or gnus-apply-kill-hook.

 gnus-auto-select-next
    Non-nil means the next newsgroup is selected automagically at the
    end of the newsgroup. If the value is t and the next newsgroup is
    empty (no unread articles), GNUS will exit Summary mode and go
    back to Group mode. If the value is neither nil nor t, GNUS won't
    exit Summary mode but select the following unread newsgroup.
    Especially, if the value is the symbol `quietly', the next unread
    newsgroup will be selected without any confirmations.

 gnus-auto-select-same
    Non-nil means an article with the same subject as the current
    article is selected automagically like `rn -S'.

 gnus-auto-center-summary
    Non-nil means the point of Summary Mode window is always kept
    centered.

 gnus-break-pages
    Non-nil means an article is broken into pages at page delimiters.
    This may not work with some versions of GNU Emacs earlier than
    version 18.50.

 gnus-page-delimiter
    Specifies a regexp describing line-beginnings that separate pages
    of news article.

 [gnus-more-message is obsolete.  overlay-arrow-string interfares
    with other subsystems, such as dbx mode.]

 gnus-digest-show-summary
    Non-nil means that a summary of digest messages is shown when
    reading a digest article using `gnus-summary-rmail-digest'
    command.

 gnus-digest-separator
    Specifies a regexp separating messages in a digest article.

 gnus-mail-reply-method
 gnus-mail-other-window-method
    Specifies a function to begin composing mail message using
    commands gnus-summary-reply and gnus-summary-mail-other-window.
    Functions gnus-mail-reply-using-mail and gnus-mail-reply-using-mhe
    are available for the value of gnus-mail-reply-method.  And
    functions gnus-mail-other-window-using-mail and
    gnus-mail-other-window-using-mhe are available for the value of
    gnus-mail-other-window-method.

 gnus-mail-send-method
    Specifies a function to mail a message too which is being posted
    as an article.  The message must have To: or Cc: field.  The value
    of the variable send-mail-function is the default function which
    uses sendmail mail program.

Various hooks for customization:
 gnus-summary-mode-hook
    Entry to this mode calls the value with no arguments, if that
    value is non-nil.

 gnus-select-group-hook
    Called with no arguments when newsgroup is selected, if that value
    is non-nil. It is possible to sort subjects in this hook. See the
    documentation of this variable for more information.

 gnus-summary-prepare-hook
    Called with no arguments after a summary list is created in the
    Summary buffer, if that value is non-nil. If you'd like to modify
    the buffer, you can use this hook.

 gnus-select-article-hook
    Called with no arguments when an article is selected, if that
    value is non-nil. See the documentation of this variable for more
    information.

 gnus-select-digest-hook
    Called with no arguments when reading digest messages using Rmail,
    if that value is non-nil. This hook can be used to modify an
    article so that Rmail can work with it. See the documentation of
    the variable for more information.

 gnus-rmail-digest-hook
    Called with no arguments when reading digest messages using Rmail,
    if that value is non-nil. This hook is intended to customize Rmail
    mode.

 gnus-apply-kill-hook
    Called with no arguments when a newsgroup is selected and the
    Summary buffer is prepared. This hook is intended to apply a KILL
    file to the selected newsgroup. The format of KILL file is
    completely different from that of version 3.8. You have to rewrite
    them in the new format. See the documentation of Kill file mode
    for more information.

 gnus-mark-article-hook
    Called with no arguments when an article is selected at the first
    time. The hook is intended to mark an article as read (or unread)
    automatically when it is selected.  See the documentation of the
    variable for more information.

 gnus-exit-group-hook
    Called with no arguments when exiting the current newsgroup, if
    that value is non-nil. If your machine is so slow that exiting
    from Summary mode takes very long time, inhibit marking articles
    as read using cross-references by setting the variable
    gnus-use-cross-reference to nil in this hook."
  (interactive)
  (kill-all-local-variables)
  ;; Gee.  Why don't you upgrade?
  (cond ((boundp 'mode-line-modified)
	 (setq mode-line-modified "--- "))
	((listp (default-value 'mode-line-format))
	 (setq mode-line-format
	       (cons "--- " (cdr (default-value 'mode-line-format))))))
  ;; To disable display-time facility.
  ;;(make-local-variable 'global-mode-string)
  ;;(setq global-mode-string nil)
  (setq major-mode 'gnus-summary-mode)
  (setq mode-name "Summary")
  ;;(setq mode-line-process '(" " gnus-newsgroup-name))
  (make-local-variable 'minor-mode-alist)
  (or (assq 'gnus-show-threads minor-mode-alist)
      (setq minor-mode-alist
	    (cons (list 'gnus-show-threads " Thread") minor-mode-alist)))
  (gnus-summary-set-mode-line)
  (use-local-map gnus-summary-mode-map)
  (buffer-flush-undo (current-buffer))
  (setq buffer-read-only t)		;Disable modification
  (setq truncate-lines t)		;Stop line folding
  (setq selective-display t)
  (setq selective-display-ellipses t)	;Display `...'
  ;;(setq case-fold-search t)
  (run-hooks 'gnus-summary-mode-hook))

(defun gnus-mouse-pick-article (e)
  (interactive "e")
  (mouse-set-point e)
  (gnus-summary-next-page nil))

(defun gnus-summary-setup-buffer ()
  "Initialize Summary buffer."
  (if (get-buffer gnus-summary-buffer)
      (set-buffer gnus-summary-buffer
% lib/pssun.pro -- prolog for pssun (raster screen dump) files
% Copyright (C) 1985 Adobe Systems, Inc.
%%Page: ? 1
save /pssunsav exch def
/inch {72 mul} def
gsave initmatrix
% precompute some values in case we want to scale for "fast" imaging
/mxTrans matrix currentmatrix def
% number of device units per 72nd of an inch
/devit {dtransform 2 copy dup mul exch dup mul exch add sqrt
	dup 3 1 roll div 3 1 roll div exch idtransform} def
/xDev 1 0 devit pop def
/yDev 0 1 devit exch pop def
/thisfile currentfile def
/doit
  {/picstr cbitWidth 7 add 8 idiv string def
   dxWidInch inch dyHtInch inch scale
   {1.0 exch sub} settransfer
   cbitWidth cbitHt cbitDepth [cbitWidth 0 0 cbitHt neg 0 cbitHt]
   {thisfile picstr readhexstring pop} image
  } def

##################
# Build menus

#############################################
# Menu trees

proc MkMainMenu {} {
	frame .menuframe -relief raised -bd 2
	pack .menuframe -side top -fill both
	menubutton .menuframe.file -text "File" -underline 0 \
		-menu .menuframe.file.m
	menubutton .menuframe.edit -text "Edit" -underline 0 \
		-menu .menuframe.edit.m
	menubutton .menuframe.font -text "Font" -underline 3 \
		-menu .menuframe.font.m
	menubutton .menuframe.func -text "Functions" -underline 2 \
		-menu .menuframe.func.m
	menubutton .menuframe.forms -text "Forms" -underline 2 \
		-menu .menuframe.forms.m
	menubutton .menuframe.anchors -text "Anchors" -underline 0 \
		-menu .menuframe.anchors.m
	menubutton .menuframe.entities -text "Entities" -underline 0 \
		-menu .menuframe.entities.m
	
	menubutton .menuframe.help -text "Help" -underline 0 \
		-menu .menuframe.help.m

	MkMainMenuFile
	MkMainMenuEdit
	MkMainMenuFont
	MkMainMenuFunctions
	MkMainMenuForms
	MkMainMenuAnchors
	MkMainMenuEntities
	MkMainMenuHelp


	pack .menuframe.file .menuframe.edit .menuframe.font \
		.menuframe.func .menuframe.forms .menuframe.anchors .menuframe.entities\
		-side left -padx 3
	pack .menuframe.help -side right -padx 3

		tk_menuBar .menuframe .menuframe.file .menuframe.edit \
		.menuframe.font .menuframe.func .menuframe.forms .menuframe.anchors\
		.menuframe.entities .menuframe.help
}

proc MkMainMenuFile {} {
	menu .menuframe.file.m
	.menuframe.file.m add command -label "New" -underline 0 \
		-command {NewFileDlg}
	.menuframe.file.m add command -label "Open" -underline 0 \
		-command {OpenFileDlg}
	.menuframe.file.m add command -label "Save" -underline 0 \
		-command {SaveFileDlg SaveCmd} -accelerator "  Ctrl-s"
	.menuframe.file.m add command -label "Save as" -underline 3 \
		-command {SaveFileAs}
	.menuframe.file.m add separator
	.menuframe.file.m add command -label "About" -underline 1 \
		-command {AboutDlg}
	.menuframe.file.m add command -label "Quit" -underline 1 \
		-command {QuitDlg} -accelerator "  Ctrl-q"
}

proc MkMainMenuEdit {} {
	menu .menuframe.edit.m
	.menuframe.edit.m add command -label "Cut" -underline 0 \
		-command {
			global CUTBUFFER
			set CUTBUFFER [selection get]
			.textframe.vp.text delete sel.first sel.last
		} -accelerator "  Ctrl-X"
	.menuframe.edit.m add command -label "Copy" -underline 1 \
		-command {
			global CUTBUFFER
			set CUTBUFFER [selection get]
		} -accelerator "  Ctrl-C"
	.menuframe.edit.m add command -label "Paste" -underline 0 \
		-command {
			global CUTBUFFER
			.textframe.vp.text insert insert $CUTBUFFER
		} -accelerator "  Ctrl-V"
	.menuframe.edit.m add separator
	.menuframe.edit.m add command -label "Find ..." -underline 0 \
		-command {
			SearchDlg
		} -accelerator "  Ctrl-S"
	.menuframe.edit.m add command -label "Find again" -underline 5 \
		-command {
			Search
		} -accelerator "  Ctrl-F"
	.menuframe.edit.m add command -label "Replace" -underline 0 \
		-command {
			SearchReplaceDlg
		} -accelerator "  Ctrl-R"
}

proc MkMainMenuFont {} {
	global netscape 

	menu .menuframe.font.m
	.menuframe.font.m add command -label "Bold <B>" \
		-underline 0 -command {DoFormat <B> </B>} -accelerator "  Alt-b"
	.menuframe.font.m add command \
		-label "Italic <I>" \
		-underline 0 -command {DoFormat <I> </I>} -accelerator "  Alt-i"
	.menuframe.font.m add command -label "Underline <U>" \
		-underline 0 -command {DoFormat <U> </U>} \
		-accelerator "  Alt-u"
	.menuframe.font.m add separator
	if {$netscape == 1} {
		.menuframe.font.m add command \
			-label "Center  <CENTER>" \
			-accelerator "  Alt-C" \
			-command {DoFormat <CENTER>\n </CENTER>\n}
		.menuframe.font.m add cascade -label "Font <FONT>"\
			 -underline 0 \
			-menu .menuframe.font.m.size 
		MkMainMenuFontSize
	}
	.menuframe.font.m add command -label "Typewriter <TT>" \
		-underline 0 -command {DoFormat <TT>  </TT>}
	.menuframe.font.m add command -label "Citation <CITE>" \
		-underline 0 -command {DoFormat <CITE> </CITE>} 
	.menuframe.font.m add command -label "Code <CODE>" \
		-underline 3 -command {DoFormat <CODE> </CODE>} \
		-accelerator "  Alt-c"
	.menuframe.font.m add command -label "Emphasis <EM>" \
		-underline 0 -command {DoFormat <EM> </EM>} \
		-accelerator "  Alt-e"
	.menuframe.font.m add command -label "Strong <STRONG>" \
		-underline 0 -command {DoFormat <STRONG> </STRONG>} \
		-accelerator "  Alt-s"
	.menuframe.font.m add command -label "Sample <SAMP>" \
		-underline 1 -command {DoFormat <SAMP> </SAMP>}
	.menuframe.font.m add command -label "Keyboard <KBD>" \
		-underline 0 -command {DoFormat <KBD> </KBD>}
	.menuframe.font.m add command -label "Variable <VAR>" \
		-underline 0 -command {DoFormat <VAR> </VAR>}
	.menuframe.font.m add command -label "Definition <DFN>" \
		-underline 0 -command {DoFormat <DFN> </DFN>} 
	.menuframe.font.m add command -label "Address <ADDRESS>" \
		-underline 0 -command {DoFormat <ADDRESS> </ADDRESS>} \
		-accelerator "  Alt-a"
	if {$netscape == 1} {
		.menuframe.font.m add command \
			-label "Blink  <BLINK>" \
			-accelerator "  Alt-B" \
			-command {DoFormat <BLINK> </BLINK>}
	}	
}

proc MkMainMenuFontSize {} {
	menu .menuframe.font.m.size
	for {set i 1} {$i < 8} {incr i +1} {
		set foo "DoFormat \{<FONT SIZE=$i>\} <FONT>"
		.menuframe.font.m.size add command \
		-label " $i " \
		-command $foo
	}
}
	


proc MkMainMenuFunctions {} {
	global netscape
	menu .menuframe.func.m
	.menuframe.func.m add cascade -label "Headers" -underline 0 \
		-menu .menuframe.func.m.headers 
	MkMainMenuFuncHeaders

	.menuframe.func.m add cascade -label "Lists" -underline 0 \
		-menu .menuframe.func.m.lists
	MkMainMenuFuncLists

	.menuframe.func.m add separator
	.menuframe.func.m add command \
		-label "Title <TITLE>" \
		-command {DoFormat <TITLE> </TITLE>} \
		-accelerator "  Alt-t"
	if {$netscape != 1} {
		.menuframe.func.m add command \
			-label "Horizontal line <HR>" \
			-command {
				.textframe.vp.text insert insert "<HR>\n"
			} -accelerator "  Alt-h"
	} else {
		.menuframe.func.m add cascade \
			-label "Horiz Rule <HR>" -underline 0 \
			-accelerator "  Alt-h" \
			-menu .menuframe.func.m.hr
		MkMainMenuFuncHR
	}
	.menuframe.func.m add command \
		-label "Preformatted <PRE>" -underline 0 \
		-command {DoFormat <PRE> </PRE>} -accelerator "  Alt-P"
	.menuframe.func.m add command \
		-label "List item <LI>" \
		-command {
			.textframe.vp.text insert insert "<LI>"
		} -accelerator "  Alt-l"
	.menuframe.func.m add command \
		-label "Paragraph <P>" \
		-command {
			.textframe.vp.text insert insert "<P>"
		} -accelerator "  Alt-Enter"
	if {$netscape == 1} {
	.menuframe.func.m add command \
		-label "Wordbreak <WBR>" \
		-command {
			.textframe.vp.text insert insert "<WBR>"
		} 
	.menuframe.func.m add command \
		-label "No break <NOBR>" \
		-command {
			.textframe.vp.text insert insert "<NOBR>"
		} 
	}
	.menuframe.func.m add command -label "Comment <!-- ..>" \
		-command {DoFormat "<!--" ">"}
}

proc MkMainMenuFuncHeaders {} {
	menu .menuframe.func.m.headers
	.menuframe.func.m.headers add command \
		-label "Header 1" \
		-command {DoFormat <H1> </H1>} \
		-accelerator "  Alt-1"
	.menuframe.func.m.headers add command \
		-label "Header 2" \
		-command {DoFormat <H2> </H2>} \
		-accelerator "  Alt-2"
	.menuframe.func.m.headers add command \
		-label "Header 3" \
		-command {DoFormat <H3> </H3>} \
		-accelerator "  Alt-3"
	.menuframe.func.m.headers add command \
		-label "Header 4" \
		-command {DoFormat <H4> </H4>} \
		-accelerator "  Alt-5"
	.menuframe.func.m.headers add command \
		-label "Header 5" \
		-command {DoFormat <H5> </H5>} \
		-accelerator "  Alt-5"
	.menuframe.func.m.headers add command \
		-label "Header 6" \
		-command {DoFormat <H6> </H6>} \
		-accelerator "  Alt-6"
}

proc MkMainMenuFuncHR {} {
	menu .menuframe.func.m.hr
	.menuframe.func.m.hr add command \
		-label "Default <HR>" \
		-accelerator "  Alt-h" \
		-command {
			.textframe.vp.text insert insert "<HR>\n" \
		} 
	.menuframe.func.m.hr add command \
		-label "NOSHADE" \
		-command {
			.textframe.vp.text insert insert "<HR NOSHADE>\n"
		}
	.menuframe.func.m.hr add command \
		-label "SIZE" \
		-command {DoFormat "<HR SIZE=" >\n}
	.menuframe.func.m.hr add command \
		-label "WIDTH" \
		-command {DoFormat "<HR WIDTH=" >\n}
	.menuframe.func.m.hr add cascade\
		-label "ALIGN" \
		-menu .menuframe.func.m.hr.align
		MkMainMenuFuncHRAlign
	.menuframe.func.m.hr add command \
		-label "All options" \
		-command {
			.textframe.vp.text insert insert "<HR SIZE= WIDTH= ALIGN= >\n"
		}
}

proc MkMainMenuFuncHRAlign {} {
	menu .menuframe.func.m.hr.align
	.menuframe.func.m.hr.align add command \
		-label " middle" \
		-command {
			.textframe.vp.text insert insert "<HR ALIGN=middle>\n"
		}
	.menuframe.func.m.hr.align add command \
		-label " left" \
		-command {
			.textframe.vp.text insert insert "<HR ALIGN=left>\n"
		}
	.menuframe.func.m.hr.align add command \
		-label " right" \
		-command {
			.textframe.vp.text insert insert "<HR ALIGN=right>\n"
		}
}


proc MkMainMenuFuncLists {} {	
	global netscape
	menu .menuframe.func.m.lists
	.menuframe.func.m.lists add command -label "Definition list <DL>" \
		-command {DoFormat <DL>\n </DL>\n}
	if {$netscape == 1} {
		.menuframe.func.m.lists add cascade \
			-label "Ordered List <OL>" \
			-menu .menuframe.func.m.lists.oltype
		MkMainMenuFuncListsOLType
	} else {
		.menuframe.func.m.lists add command \
			-label "Ordered List <OL>" \
			-command {DoFormat <OL>\n </OL>\n}
	}
	.menuframe.func.m.lists add command \
		-label "Directory list <DIR>" \
		-command {DoFormat <DIR>\n </DIR>\n}
	if {$netscape == 1} {
		.menuframe.func.m.lists add cascade \
			-label "Unordered List <UL>" \
			-menu .menuframe.func.m.lists.ultype
		MkMainMenuFuncListsULType
	} else {
		.menuframe.func.m.lists add command \
			-label "Unordered List <UL>" \
			-command {DoFormat <UL>\n </UL>\n}
	}
	.menuframe.func.m.lists add separator
	.menuframe.func.m.lists add command -label "List item <LI>"\
		-command {.textframe.vp.text insert insert "<LI>"} \
		-accelerator "  Alt-l"
	.menuframe.func.m.lists add command -label "List item <DT>"\
                -command {.textframe.vp.text insert insert "<LI>"}
        .menuframe.func.m.lists add command -label "List item <DD>"\
                -command {.textframe.vp.text insert insert "<LI>"} 
}

proc MkMainMenuFuncListsOLType {} {
        menu .menuframe.func.m.lists.oltype
	.menuframe.func.m.lists.oltype add command \
		-label "Default <OL>" \
		-command {DoFormat <OL>\n </OL>\n}
        .menuframe.func.m.lists.oltype add command \
                -label "Capital letters" \
                -command {DoFormat "<OL TYPE=A>\n" </OL>\n"}
        .menuframe.func.m.lists.oltype add command \
                -label "Small letters" \
                -command {DoFormat "<OL TYPE=s>\n" </OL>\n"}
        .menuframe.func.m.lists.oltype add command \
                -label "Large Roman numerals" \
                -command {DoFormat "<OL TYPE=I>\n" </OL>\n"}
        .menuframe.func.m.lists.oltype add command \
                -label "Small Roman numerals" \
                -command {DoFormat "<OL TYPE=i>\n" </OL>\n"}
        .menuframe.func.m.lists.oltype add command \
                -label "Default numbers" \
                -command {DoFormat "<OL TYPE=1>\n" </OL>\n"}
}

proc MkMainMenuFuncListsULType {} {
        menu .menuframe.func.m.lists.ultype
	.menuframe.func.m.lists.ultype add command \
		-label "Default <UL>" \
		-command {DoFormat <UL>\n </UL>\n}
        .menuframe.func.m.lists.ultype add command \
                -label "Disc" \
                -command {DoFormat "<UL TYPE=disc>\n" </UL>\n"}
        .menuframe.func.m.lists.ultype add command \
                -label "Circle" \
                -command {DoFormat "<UL TYPE=circle>\n" </UL>\n"}
        .menuframe.func.m.lists.ultype add command \
                -label "Square" \
                -command {DoFormat "<UL TYPE=square>\n" </UL>\n"}
}


proc MkMainMenuForms {} {
	menu .menuframe.forms.m
	.menuframe.forms.m add command \
		-label "Form Method Post" \
		-command {DoFormat "<FORM METHOD=\"POST\" ACTION=\"" "\">"} \
		-accelerator "  "
	.menuframe.forms.m add command \
		-label "Form Method Get" \
		-command {DoFormat "<FORM METHOD=\"GET\" ACTION\"" "\">"}
	.menuframe.forms.m add cascade -label "Input Type" -underline 0 \
		-menu .menuframe.forms.m.input
	MkMainMenuFormsInput

	.menuframe.forms.m add cascade -label "Select type" -underline 0 \
	-menu .menuframe.forms.m.select
	MkMainMenuFormsSelect

	.menuframe.forms.m add separator
	.menuframe.forms.m add command \
		-label "Textarea <TEXTAREA...>" \
		-command {DoFormat "<TEXTAREA NAME=\"" "\" ROWS= COLS=>\n</TEXTAREA>"}
	.menuframe.forms.m add command \
                -label "Option <OPTION>" \
                -command {.textframe.vp.text insert insert "<OPTION>"}
	.menuframe.forms.m add command \
		-label "Size SIZE=" \
		-command {.textframe.vp.text insert insert "SIZE="}
        .menuframe.forms.m add command \
                -label "MaxLength MAXLENGTH=" \
                -command {.textframe.vp.text insert insert  "MAXLENGTH="}
	.menuframe.forms.m add command \
                -label "Name NAME=" \
                -command {.textframe.vp.text insert insert "NAME="} 
}

proc MkMainMenuFormsInput {} {
        menu .menuframe.forms.m.input
        .menuframe.forms.m.input add command \
                -label "Input Name <...NAME=\"\">" \
                -command {DoFormat "<INPUT NAME=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Text <...TYPE=\"text\">" \
		-command {DoFormat "<INPUT TYPE=\"text\" VALUE=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Password <...TYPE=\"password\">" \
		-command {DoFormat "<INPUT TYPE=\"password\" VALUE=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Checkbox <...TYPE\"checkbox\">" \
		-command {DoFormat "<INPUT TYPE=\"checkbox\" VALUE=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Radio <...TYPE\"radio\">" \
		-command {DoFormat "<INPUT TYPE=\"radio\" VALUE=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Submit <...TYPE\"submit\">" \
		-command {DoFormat "<INPUT TYPE=\"submit\" VALUE=\"" "\">"}
	.menuframe.forms.m.input add command \
		-label "Reset <...TYPE\"reset\">" \
		-command {DoFormat "<INPUT TYPE=\"reset\" VALUE=\"" "\">"}
        .menuframe.forms.m.input add command \
                -label "Hidden <...TYPE\"hidden\">" \
                -command {DoFormat "<INPUT TYPE=\"hidden\" VALUE=\"" "\">"}
}

proc MkMainMenuFormsSelect {} {
	menu .menuframe.forms.m.select
	.menuframe.forms.m.select add command \
		-label "Select <SELECT NAME=>" \
		-command {DoFormat "<SELECT NAME=\"" "\">\n</SELECT>"}
	.menuframe.forms.m.select add command \
		-label "Select <SELECT NAME= SIZE=>" \
		-command {DoFormat "<SELECT NAME=\"" "\" SIZE=>\n</SELECT>"}
}

proc MkMainMenuAnchors {} {
	menu .menuframe.anchors.m
	.menuframe.anchors.m add command -label "Hyperlink" \
		-command {
			if {[IsSelected] == 1} {
				set temp [selection get]
				set temp2 [GetUrl hyperlink]
				if {[string length $temp2] != 0 } {
					.textframe.vp.text insert sel.first "<A HREF=\"$temp2\">$temp</A>"
					.textframe.vp.text delete sel.first sel.last
				}
			} else {
				DoFormat "<A HREF=\"" "\">CHANGE_ME</A>"
			}
		} -accelerator "  Alt-H"
	.menuframe.anchors.m add command -label "Inline image" \
		-command {
			if {[IsSelected] == 1} {
				set temp [selection get]
				.textframe.vp.text insert sel.first "<IMG SRC=\"$temp\">"
				.textframe.vp.text delete sel.first sel.last
			} else {
				DoFormat "<IMG SRC=\"" "\">"
			}		
		} -accelerator "  Alt-I"
}

proc MkMainMenuEntities {} {
	global insertspec insertesc entlist entities esclist
	menu .menuframe.entities.m
	.menuframe.entities.m config -disabledforeground black

	.menuframe.entities.m add checkbutton \
		-label "insert entities for special (iso) characters (&\344\337..)"\
		-variable insertspec
	.menuframe.entities.m add checkbutton \
		-label "insert entities for escaped chars (\"a\"s'e...)" \
		-variable insertesc
	.menuframe.entities.m add separator
	.menuframe.entities.m add command \
		-state disabled \
		-label "insert entity by selecting from list:"
	.menuframe.entities.m add command \
		-state disabled \
		-font "-adobe-courier-medium-r-normal-*-14-*-*-*-*-*-*-*" \
		-label "name                  iso esc   entity"
	.menuframe.entities.m add separator
	foreach ent $entlist {
		set labeltext \
		[ string range "$entities($ent,text)                   " 0 20 ]
		append labeltext "/ "
		if { $entities($ent,spon) } {
			append labeltext "$entities($ent,iso) / "
		} else {
			append labeltext "  / "
		}
		if { $entities($ent,eson) } {
			append labeltext \
				[ lindex $esclist $entities($ent,esc1)]
			append labeltext "$entities($ent,esc2) / "
		} else {
			append labeltext "  / "
		}
		append labeltext $entities($ent,ent)
		.menuframe.entities.m add command \
			-label $labeltext \
			-font "-adobe-courier-medium-r-normal-*-14-*-*-*-*-*-*-*" \
			-command \
			".textframe.vp.text insert insert 			\"$entities($ent,ent)\""
	}
}

#############################################
# dialog to enter a URL (used by Hyperlink)

proc GetUrl {type} {
	global urlchoice
	set urlchoice ""
	set foo ""
	toplevel .geturl
	wm title .geturl "Get $type"
	set x [expr 230 + [winfo x .]]
	set y [expr 100 + [winfo y .]]
	wm geometry .geturl +$x+$y

	frame .geturl.top -relief raised -bd 1
	label .geturl.top.lbl -text "
Enter the URL of this $type:"
	entry .geturl.top.entry -textvariable foo
	tixDlgBtns .geturl.btns
	.geturl.btns add ok -text "Ok" -width 8
	.geturl.btns add cancel -text "Cancel" -width 8
	
	pack .geturl.top.lbl -expand yes -fill x -padx 10 -pady 10
	pack .geturl.top.entry -expand yes -fill x -padx 10 -pady 10
	pack .geturl.top -expand yes -fill both
	pack .geturl.btns -fill x

	bind .geturl <Enter> {
		focus .geturl.top.entry
	}

	bind .geturl.top.entry <Key-Return> { 
		set urlchoice $foo
		destroy .geturl
	}
	
	.geturl.btns button ok config -command {
		set urlchoice $foo
		destroy .geturl
	}
	.geturl.btns button cancel config -command {
		destroy .geturl
		return
	}

	tkwait window .geturl
	return $urlchoice
}

proc MkMainMenuHelp {} {
	menu .menuframe.help.m
	.menuframe.help.m add command -label "Formatting text" \
		-command {DoHelp HelpFormatting}
	.menuframe.help.m add command -label "Cutting and Pasting" \
		-command {DoHelp HelpCutting}
	.menuframe.help.m add command -label "Inserting Hyperlinks" \
		-command {DoHelp HelpHyper}
	.menuframe.help.m add command -label "Inserting Inline Images" \
		-command {DoHelp HelpInline}
	.menuframe.help.m add command -label "Entities" \
		-command {DoHelp HelpEntities}
	.menuframe.help.m add command -label "Short-cut Keys" \
		-command {DoHelp HelpShort}
	.menuframe.help.m add command -label "HTML Quick Reference" \
		-command {catch {Preview "http://www.ncsa.uiuc.edu/General/Internet/WWW/HTMLQuickRef.html"}}
	.menuframe.help.m add command -label "WYSIWYG Previewing" \
		-command {DoHelp HelpPreview}
	.menuframe.help.m add command -label "Help Browser" \
		-command {DoHelp HelpMain}
	.menuframe.help.m add separator
	 .menuframe.help.m add command -label "tkHTML Home Page" \
		-command {catch {Preview "http://alfred1.u.washington.edu:8080/~roland/tkHTML/tkHTML.html"}}
}



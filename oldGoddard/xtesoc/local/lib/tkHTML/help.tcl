##################
# Help dialogs

proc DoHelp {type} {

####################3
# help window
# ok let's create the dialog for HELP

####### 
# global variables

	global w
	global helpprev
	global helpnext
	global xpos
	global ypos

#######
# next and previous help pages
	set helpprev "HelpMain"
	set helpnext "HelpFormatting"
	set w ".help.mf.vs.text"
#######
# Configure and create the actual dialog

	ClearEvent "Calling help"

	toplevel .help
	GetXY
	set tmpx [expr $xpos+350]
	set tmpy [expr $ypos+60]

	wm geometry .help +$tmpx+$tmpy

	wm title .help "tkHTML Help"

	frame .help.mf -relief raised -borderwidth 2 

	pack .help.mf -fill both -expand true

	frame .help.mf.vs -relief flat -borderwidth 2 

	frame .help.mf.bb -relief flat -borderwidth 2 

	pack .help.mf.vs -side top -fill both -expand true
	pack .help.mf.bb -side bottom -fill both -expand true

	button .help.mf.bb.quit -text "Quit Help" \
		-command "
	if {\"\[info commands XFDestroy\]\" != \"\"} {
		catch {XFDestroy .help}
	} else {
		catch {destroy .help}
	}"
	button .help.mf.bb.main -text "Main Menu" \
		-command HelpMain
	button .help.mf.bb.next -text "Next Document" \
		-command $helpnext
	button .help.mf.bb.previous -text "Previous Document" \
		-command $helpprev

	pack .help.mf.bb.quit .help.mf.bb.main .help.mf.bb.next \
	.help.mf.bb.previous -side left -fill both -expand true -padx 2 -pady 2

	scrollbar .help.mf.vs.scroll -relief sunken \
		-command {.help.mf.vs.text yview} 
	pack .help.mf.vs.scroll -side left -fill both -expand true

	text .help.mf.vs.text -relief sunken -borderwidth 1 \
		-background #bfbfbfbfbfbf \
		-yscrollcommand {.help.mf.vs.scroll set} \
		-wrap word -width 50 -height 18
	pack .help.mf.vs.text -side right -padx 5

	ConfigFonts $w

	$w tag bind h1 <1> HelpFormatting
	$w tag bind h2 <1> HelpCutting
	$w tag bind h3 <1> HelpHyper
	$w tag bind h4 <1> HelpInline
	$w tag bind h6 <1> HelpEntities
	$w tag bind h5 <1> HelpShort
	$w tag bind hh <1> HelpPrimer
	$w tag bind hp <1> HelpPreview

#######
# when called, we go to the procedure for the selected help function

	$type
}

proc ConfigFonts {w} {
#######
#now, lets define colors and fonts
	$w tag configure 100L -font -b&h-lucida-medium-r-*-*-12-*-*-*-*-*-*-*
	$w tag configure 140H -font -adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*
	$w tag configure 180H -font -adobe-helvetica-bold-r-normal-*-18-*-*-*-*-*-*-*
	$w tag configure 180L -font -b&h-lucida-bold-r-normal-*-18-*-*-*-*-*-*-*
	$w tag configure 240H -font -adobe-helvetica-bold-r-normal-*-24-*-*-*-*-*-*-*
	$w tag configure normal -font -adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*
	$w tag configure normalb -font -adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-*-*
	$w tag configure normali -font -adobe-helvetica-medium-o-normal-*-14-*-*-*-*-*-*-*
	$w tag configure code -font -adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*

#######
# these are the hyperlink configurations

	$w tag configure blue -foreground blue
	$w tag configure red -foreground red

}

# Procedure: HelpCutting
proc HelpCutting {} {
###################
# Cut and PAste Hyperhelp

# global variables
global w
global helpnext
global helpprev

# set previous/next documents

set helpnext HelpHyper
set helpprev HelpFormatting

.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Cutting and Pasting"

insertWithTags $w "CUTTING AND PASTING TEXT\n\n" 180H
insertWithTags $w "Cut and paste works like contemporary text editors. Highlight the text you would like to cut or copy,  select the '" nnor" normal
insertWithTags $w "Edit" normali
insertWithTags $w "' menu option, followed by '" normal
insertWithTags $w "cut" normali
insertWithTags $w "' or '" normal
insertWithTags $w "copy" normali
insertWithTags $w "'. To paste the text, locate the cursor where you would like to  paste the text, then select '" normal
insertWithTags $w "Edit" normali
insertWithTags $w "' followed by '" normalb
insertWithTags $w "paste" normali
insertWithTags $w "'. \n\nYou can also use the short-cut keys:\n\n To " normal
insertWithTags $w "COPY" normalb
insertWithTags $w " the selection to the buffer:	Control-c\n
To " normal
insertWithTags $w "CUT" normalb 
insertWithTags $w " the selection to the buffer:	Control-x\n
To " normal
insertWithTags $w "PASTE" normalb
insertWithTags $w " the selection to the text: 	Control-v\n" normal

# disable editing
$w config -state disabled
}


# Procedure: HelpFormatting
proc HelpFormatting {} {
####################
# Formatting Hyperhelp

# global variables
global w
global helpnext
global helpprev

# set previous/next documents

set helpnext HelpCutting
set helpprev HelpMain
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev
# enable and clear help window

$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Formatting Text"

insertWithTags $w "FORMATTING TEXT\n\n" 180H
insertWithTags $w "To format text with html-functions, such as emphasis, headlines or list blocks, there are two methods of inserting them in the document: using the pull-down menus or short-cut keys.\n\n" normal
insertWithTags $w "Pull-down Menus\n" 140H 
insertWithTags $w "Select the text you would like to format with the mouse, then select the menu option, either '" normal
insertWithTags $w "Font" normali 
insertWithTags $w "'  or '" normal
insertWithTags $w "Function" normali
insertWithTags $w "', that has the format option you want, then select it. The text will automatically be placed between the formatting devices for that style.\n\n" normal
insertWithTags $w "Short-cut Keys\n\n" h5 blue 140H 
insertWithTags $w "This is the fastest and easiest method to use. There are two modes of operation: highlighting with the mouse or 'on the fly' formatting. On the fly simply implies that there is no mouse use involved, just straight typing. As you are typ
ing, you may insert a new format by typing the corresponding " normal
insertWithTags $w "short-cut key" h5 normal blue 
insertWithTags $w ". The cursor will be placed between the starting and closing marks of that type of format automatically. You may also highlight a region of text with the mouse and apply the short-cut key sequence and have that region formatted in that 
style." normal

# disable editting
$w config -state disabled
}

# Procedure: HelpHyper
proc HelpHyper {} {
#########################
# Hyperlink Hyperhelp

# global variables
global w
global helpnext
global helpprev

# set previous/next documents
set helpnext HelpInline
set helpprev HelpCutting
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Hyperlinking"

insertWithTags $w "HYPERLINKING DOCUMENTS\n\n" 180H
insertWithTags $w "Select the text you would like to be the 'anchor' of the link (the words that will be underlined in the  document as the link). Select the '" normal
insertWithTags $w "Anchors" normali
insertWithTags $w "' menu option, followed by '" normal
insertWithTags $w "Hyper link" normali
insertWithTags $w "'. You will be promtped for the URL of the link which is the document or item that you are creating  a link to. If you do not highlight any words before selecting '" normal
insertWithTags $w "Hyper link" normali 
insertWithTags $w "', your cursor will be placed between the anchor functions for inserting a hyperlink manually.\n\nYou may also use the short-cut key " normal
insertWithTags $w "Alt-H" normalb
insertWithTags $w ", which works the same way as selecting '" normal
insertWithTags $w "Anchor | Hyper link" normali
insertWithTags $w "' on the menubar. See " normal
insertWithTags $w "Formatting Text" h1 blue normalb
insertWithTags $w " or " normal
insertWithTags $w "Short-cut Keys" h5 blue normalb
insertWithTags $w " for more information." normal

# disable editting
$w config -state disabled
}


# Procedure: HelpInline
proc HelpInline {} {
###########################
# Inline Image hyperhelp
# global variables
global w
global helpnext
global helpprev

# set previous/next documents
set helpnext HelpShort
set helpprev HelpEntities
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Inline Images"
insertWithTags $w "Inline Images\n\n" 180H
insertWithTags $w "To insert an inline image, select the '" normal
insertWithTags $w "Anchor" normali
insertWithTags $w "' menu option, followed by the '" normal
insertWithTags $w "Inline Image" normali
insertWithTags $w "' option.The cursor will be placed between the " normal
insertWithTags $w "<IMG SRC=\"" code
insertWithTags $w " and " normal
insertWithTags $w "\">" code
insertWithTags $w " marks automatically, where you can type in the filename or URL of the image. IF a region of  text has been highlighted before you select '" normal
insertWithTags $w "Anchor | Inline image" normali
insertWithTags $w "', this will be assumed to be the filename/URL of the image  you are inlining, and be placed into the \n" normal
insertWithTags $w "<IMG SRC=\" \">" code
insertWithTags $w " anchor automatically.\n\n" normal
insertWithTags $w "You may also use the shortcut key, " normal
insertWithTags $w "Alt-I" normalb
insertWithTags $w ", to do the same operation as indicated above." normal


# disable editting
$w config -state disabled
}
# Procedure: HelpEntities
proc HelpEntities {} {
###########################
# Inline Image hyperhelp
# global variables
global w
global helpnext
global helpprev

# set previous/next documents
set helpnext HelpInline
set helpprev HelpShort
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Entities"
insertWithTags $w "Entities\n\n" 180H
insertWithTags $w "To include special characters like accented characters in an HTML-document, the correct way is to use " normal
insertWithTags $w "entities" normalb
insertWithTags $w ". To get an \"Umlaut a (\344)\", for example, you have to include the entity " normal
insertWithTags $w "\"&auml;\"" code
insertWithTags $w "Typing this by hand is both tedious and boring, so tkHTML has three ways of avoiding this:\n\n" normal
insertWithTags $w "Selecting from menu\n\n" 140H
insertWithTags $w "The menu \"" normal
insertWithTags $w "Entities" code
insertWithTags $w "\" has a list of all the entities known by tkHTML. Selecting them places that entity in the text.\n\n" normal
insertWithTags $w "Typing on special keyboards\n\n" 140H
insertWithTags $w "In countries with languages using such characters, you often have keyboards with these accented characters on them. Typing an umlaut \"a\" will include the entity \"" normal
insertWithTags $w "&auml;" code
insertWithTags $w "\" in the text if the checkbutton \"" normal
insertWithTags $w "insert entities for special (iso) characters\"" code
insertWithTags $w " is on. If you don't have such a keyboard, you may define some unused keys to enter special characters using the command \"" normal
insertWithTags $w "xmodmap -e ..." code
insertWithTags $w "\".\n\n" normal 
insertWithTags $w "Using escape sequences\n\n" 140H
insertWithTags $w "In the German version of " normal
insertWithTags $w "LaTeX" code
insertWithTags $w " you may type \"a (quotation mark follow by \"a\") to get an umlaut \"a\" in the formatted text. Alsso typing \'e (single quote follows by \"e\") on some keyboards will send an \"e acute (\351)\". If the " normal
insertWithTags $w "insert entities for escaped characxters" code
insertWithTags $w " checkbutton is enabled, tkHTML will replace these sequences with their entities.\n\n" normal
insertWithTags $w "Special entities, undefined escape sequences\n\n" 140H
insertWithTags $w "Some special things are actually defined in the following manner: Typing > will output a real >, typing \"> will output it's entity " normal
insertWithTags $w "&gt;" code
insertWithTags $w " (same for < and &). Typing \"\" will output the entity " normal
insertWithTags $w "&quot;" code
insertWithTags $w ". Typing \"<space> will output the normal \", as with '<pace> and `<space>. Typing two different escape characters will delete the first one. Typing undefined escape sequences will output the escape character and the following character
 (\"m -> \"m ...). Typing an escape character followed by a special character will result in a lonely special character (\"\344 -> \344)\n\n" normal
insertWithTags $w "Defining entities\n\n" 140H
insertWithTags $w "The list may be incomplete for your personal preferences, so you may define your own entities in your config.tcl or ~/.tkhtmlrc. See entities.tcl and the README.entities file for more information." normal
 
# disable editting
$w config -state disabled
}

# Procedure: HelpMain
proc HelpMain {} {
############################
# Main help index

# global variables
global w
global helpprev
global helpnext

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

# set previous/next documents
set helpnext HelpFormatting
set helpprev HelpMain
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

wm title .help "tkHTML Help Browser - Main Index"

insertWithTags $w "HELP MAIN MENU - tkHTML\n\n" 180H
insertWithTags $w "tkHTML is a simple html editor with many commands to simplify editing and converting html documents." normal
insertWithTags $w "Most html functions are available by using the pull-dowm menus, and most essential text formatting" normal
insertWithTags $w "commands are available with shortcut keys.\n\n" normal
insertWithTags $w "\nFor more help, click on one of the following topics:\n\n" 140H

insertWithTags $w "Formatting" h1 140H blue
insertWithTags $w " - using html functions\n\n" 140H
insertWithTags $w "Cutting and Pasting" h2 140H blue
insertWithTags $w " - manipulating text\n\n" 140H
insertWithTags $w "Inserting Hyperlinks" h3 blue 140H
insertWithTags $w " - linking to other documents\n\n" 140H 
insertWithTags $w "Inline Images" h4 140H blue
insertWithTags $w " - adding pictures and icons\n\n" 140H 
insertWithTags $w "Entities" h6 140H blue
insertWithTags $w " - interting special characters\n\n" 140H
insertWithTags $w "Short-cut Keys" h5 140H blue
insertWithTags $w " - on the fly editing\n\n" 140H
insertWithTags $w "WYSIWYG Previewing" hp 140H blue
insertWithTags $w " - seeing as you work\n\n" 140H

insertWithTags $w "HTML primer" blue 140H hh
insertWithTags $w "- A quick study on html\n" 140H

# disable editting
$w config -state disabled
}

# Procedure: HelpPreview
proc HelpPreview {} {
####################
# Previewer hyper document

# global produres
global w
global helpnext
global helpprev
global version

# previous/next documents
set helpnext HelpMain
set helpprev HelpMain

.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable update of help window
$w config -state normal

# clear current document
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML HELP Browser - Previewer"

insertWithTags $w "WYSIWYG PREVIEWER\n\n" 180H
insertWithTags $w "tkHTML $version has been equipped with a simple WYSIWYG (what you see is what you get) previewer to help  you design your pages. The previewer only works after you have saved the file. To start the previewer, either select the " normal
insertWithTags $w "Preview" normali
insertWithTags $w "' menu option folllowed by '" normal
insertWithTags $w "Activate Previewer" normali
insertWithTags $w "' or use the shortcut key, " normal
insertWithTags $w "Ctrl-p" normalb
insertWithTags $w ".\n\nYou will be presented with the previewer window, which may stay open as long as you work on your document. To  update the display, save your document first (" normal
insertWithTags $w "Ctrl-S" normalb
insertWithTags $w ") then select the " normal
insertWithTags $w "Reload" code
insertWithTags $w "button." normal

# disable editing of help window
$w config -state disabled
}

# Procedure: HelpPrimer
proc HelpPrimer {} {
####################3
# HTML PRimer hyper document

# global produres
global w
global helpnext
global helpprev

# previous/next documents
set helpnext HelpMain
set helpprev HelpMain
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable update of help window
$w config -state normal

# clear current document
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML HELP Browser - HTML Primer"

insertWithTags $w "HTML PRIMER\n\n" 180H
insertWithTags $w "To be added - Liem\n" normalb

# disable editing of help window
$w config -state disabled
}


# Procedure: HelpShort
proc HelpShort {} {
#########################3
# Shortcut keys hyperhelp

# global veriables
global w
global helpnext
global helpprev

# set previuos/next document
set helpnext HelpMain
set helpprev HelpEntities
.help.mf.bb.next config -command $helpnext
.help.mf.bb.previous config -command $helpprev

# enable and clear help window
$w config -state normal
$w mark set insert 0.0
$w delete insert end

wm title .help "tkHTML Help Browser - Shortcut Keys"

insertWithTags $w "SHORT-CUT KEYS\n\n" 180H
insertWithTags $w "Short-cut keys allow you to format the document without using the mouse. Almost all major formatting  functions are available in short form.\n\nThe format of the short cut keys is as follows: \n\n" normal
insertWithTags $w "Alt  = Alternate (Meta1)\nCtrl = Control (right or left)\n\n" normalb
insertWithTags $w "File Options\n" 140H red
insertWithTags $w "Save			Ctrl-s\n" normalb 
insertWithTags $w "Quit			Ctrl-q\n\n" normalb

insertWithTags $w "Directional Movement\n" 140H red
insertWithTags $w "Arrow Keys\n" normalb
insertWithTags $w "Left			Ctrl-b\n" normalb
insertWithTags $w "Right			Ctrl-f\n" normalb
insertWithTags $w "Up			Ctrl-p\n" normalb
insertWithTags $w "Down			Ctrl-n\n" normalb
insertWithTags $w "Start of line		Ctrl-e\n" normalb
insertWithTags $w "End of line		Ctrl-a\n" normalb
insertWithTags $w "Page up			Ctrl-y\n" normalb
insertWithTags $w "Page down		Ctrl-v\n\n" normalb

insertWithTags $w "Text Formatting\n" 140H red
insertWithTags $w "Centering		Alt-C (netscape mode)\n" normalb
insertWithTags $w "Logical Styles:\n" normalb
insertWithTags $w "Emphasis		Alt-e\n" normalb
insertWithTags $w "Strong Emphasis		Alt-s\n" normalb
insertWithTags $w "Code			Alt-c\n" normalb
insertWithTags $w "Address 			Alt-a\n\n" normalb

insertWithTags $w "Physical Styles: \n" normalb
insertWithTags $w "Bold			Alt-b\n" normalb
insertWithTags $w "Underline		Alt-b\n" normalb
insertWithTags $w "Italic			Alt-i\n" normalb
insertWithTags $w "Headline			Alt-x (x = 1-6)\n" normalb
insertWithTags $w "Preformatted text		Alt-P\n\n" normalb

insertWithTags $w "Anchors\n" red 140H
insertWithTags $w "Hyperlink		Alt-H\n" normalb
insertWithTags $w "Inline Image		Alt-I\n\n" normalb

insertWithTags $w "Entities\n" 140H red
insertWithTags $w "Greater than (>)		Alt->\n" normalb
insertWithTags $w "Less than (<)		Alt-<\n" normalb
insertWithTags $w "Ampersand (&)		Alt-&\n" normalb
insertWithTags $w "Quote (\")		Alt-\"\n" normalb
insertWithTags $w "Non-breaking space	Alt-space\n" normalb
insertWithTags $w "Additional Entities info\n\n" h6 blue normalb

insertWithTags $w "Other\n" 140H red
insertWithTags $w "Title			Alt-t\n" normalb
insertWithTags $w "Horizontal Line		Alt-h\n" normalb
insertWithTags $w "List Item			Alt-l\n" normalb
insertWithTags $w "Paragraph		Alt-return\n" normalb
insertWithTags $w "Newline			Alt-b\n" normalb
insertWithTags $w "Comment		Alt-C\n\n" normalb

insertWithTags $w "Selection Options\n" red 140H
insertWithTags $w "highlight left		Shift-Left Arrow\n" normalb
insertWithTags $w "highlight right		Shift-Right Arrow\n" normalb
insertWithTags $w "Cut			Ctrl-X\n" normalb
insertWithTags $w "Paste			Ctrl-Y\n" normalb
insertWithTags $w "Copy			Ctrl-C\n" normalb
insertWithTags $w "Find			Ctrl-S\n" normalb
insertWithTags $w "Find again		Ctrl-F\n" normalb 
insertWithTags $w "Delete line		Ctrl-k\n" normalb
insertWithTags $w "Delete prev word		Shift-delete\n\n" normalb
# disable editting
$w config -state disabled
}


proc GetXY {} {
################
# get the current X and Y coords of toplevel

global xpos
global ypos

	set position [wm geometry .]
	set position2 [split $position +]
	set xpos [lindex $position2 1]
	set ypos [lindex $position2 2]
}




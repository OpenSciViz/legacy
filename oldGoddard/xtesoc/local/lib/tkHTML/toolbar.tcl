#############################################
# toolbar procedures

#############################################
# this does everything for the toolbar

proc MkToolbar {} {
	frame .toolbarframe -relief flat 
	pack .toolbarframe -side top -fill both
	MkToolbarPreview
	MkToolbarFont
	MkToolbarSize
	MkToolbarWrap
}

#############################################
# preview button

proc MkToolbarPreview {} {
	button .toolbarframe.preview -text "Preview" \
		-font "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*" \
		-command { SaveForPreview } -foreground Red
	pack .toolbarframe.preview -side left -padx 8 -pady 2
}

#############################################
# linewrap widget
proc MkToolbarWrap {} {
	label .toolbarframe.wraplabel -text "Word wrap:" \
		-font "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
	tixSelect .toolbarframe.sel -command {SetMainWrap} \
		-font "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
	.toolbarframe.sel add word -text "Word" -width 6
	.toolbarframe.sel add char -text "Char" -width 6
	.toolbarframe.sel config -value {char}
#	.toolbarframe.sel config -command {SetMainWrap}

	pack .toolbarframe.wraplabel -side left -padx 3 -pady 2
	pack .toolbarframe.sel -side left -pady 2

}

#############################################
# main window font widget

proc MkToolbarFont {} {
	global textfont
#	label .toolbarframe.fontlabel -text "Display font:" \
#		-font "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
	tixCombobox .toolbarframe.font -type static \
		-command SetMainFont -width 22\
		-entryfont "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"

	.toolbarframe.font appendhistory "Courier"
	.toolbarframe.font appendhistory "Helvetica"
	.toolbarframe.font appendhistory "New Century Schoolbook"
	.toolbarframe.font appendhistory "Times"
	.toolbarframe.font appendhistory "Lucida"
	.toolbarframe.font pick $textfont

#	pack .toolbarframe.fontlabel -side left -padx 3 -pady 2
	pack .toolbarframe.font -side left -padx 2 

}	

#############################################
# main window point size widget

proc MkToolbarSize {} {
	global textsize
	label .toolbarframe.sizelabel -text "Display font size:" \
		-font "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
	tixCombobox .toolbarframe.size -type static \
		-command SetMainSize \
		-width 3 \
		-entryfont "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"

	.toolbarframe.size appendhistory "8"
	.toolbarframe.size appendhistory "10"
	.toolbarframe.size appendhistory "12"
	.toolbarframe.size appendhistory "14"
	.toolbarframe.size appendhistory "18"
	.toolbarframe.size appendhistory "24"
	.toolbarframe.size pick $textsize

#	pack .toolbarframe.sizelabel -side left -pady 2 -padx 3 
	pack .toolbarframe.size -side left -pady 2
}

#############################################
# set word or character wrapping for main window

proc SetMainWrap {widget status} {
	if {$widget == "word"} {
		if {$status == 0} {
			.textframe.vp.text config -wrap char
		} else {
			.textframe.vp.text config -wrap word
		}
	}
}

#############################################
# actually set the point size of the main window

proc SetMainSize {sizechoice} {
	global textfont
	global textsize
	set textsize $sizechoice 
	SetMainFontAndSize
}


#############################################
# set the font size

proc SetMainFont {fontchoice} {
	global textsize
	global textfont
	set textfont $fontchoice
	SetMainFontAndSize
}

#############################################
# use values to set font and point

proc SetMainFontAndSize {} {
	global textsize
	global textfont
	if {$textfont == "Times" } {
		.textframe.vp.text config -font \
		"-adobe-times-bold-r-normal-*-$textsize-*-*-*-*-*-*"
	} elseif {$textfont == "Lucida" } {
		.textframe.vp.text config -font \
		"-b&h-lucida-medium-r-normal-*-$textsize-*-*-*-*-*-*-*"
	} elseif {$textfont == "Courier" } {
		.textframe.vp.text config -font \
		"-adobe-courier-medium-r-normal-*-$textsize-*-*-*-*-*-*-*"
	} elseif {$textfont == "Helvetica" } {
		.textframe.vp.text config -font \
		"-adobe-helvetica-medium-r-normal-*-$textsize-*-*-*-*-*-*-*"
	} elseif {$textfont == "New Century Schoolbook"} { 
		.textframe.vp.text config -font \
		"-adobe-new century schoolbook-medium-r-normal-*-$textsize-*-*-*-*-*-*-*"
	}
}







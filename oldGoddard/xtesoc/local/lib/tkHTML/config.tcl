#############################################################
# user-definable, use caution, especially with fonts
#############################################################
# 1 = yes
# 0 = no

# main window font (Courier,Helvetica,Times,Lucida,New Century Schoolbook)
set textfont "New Century Schoolbook"

# main window point size (8,10,12,14,18,24)
set textsize "14"

# previewer typeface (times,helvetica,lucida,century)
set typeface "helvetica"

# previewer temp file (must end in .html!)
# set tempfile "/tmp/.tkhtml_preview.html"
# this will save with pid as part of the filename, nice for multiple users
set tempfile "/tmp/.tkhtml[pid].html"

# do you want a default <HEAD>/<BODY> template when starting a new document?
# 1 = yes, 0 = no
set newheader 1

# do you want special entities enabled on startup (can be toggled in the Entities menu)
set insertspec 1
set insertesc 1

# do you want NetScape tags in menus?
set netscape 1


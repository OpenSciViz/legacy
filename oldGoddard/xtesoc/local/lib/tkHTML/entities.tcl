##################
# Build entities
# by Heiko Jacobs <jacobs@ipf.bau-verm.uni-karlsruhe.de>

global insertspec insertesc insertescape entlist esclist escesclist 
global entities escapei nesc
# Defaults for menus
set insertspec 1
set insertesc 1

set entlist ""

# all escape characters
set esclist    [ list "\""     "'" "`" ]
set nesc [ llength $esclist ]
# what to do for doubled escape characters ( typing "" -> &quot; )
set escesclist [ list "&quot;" "'" "`" ]

for { set i 0 } { $i < $nesc } { incr i } {
	set insertescape($i) 0
	set escapei([lindex $esclist $i]) $i
}

# store values from list (file???) to array
proc defentities {name iso key ent esc1 esc2 text {spon 1} {eson 1} } {
# name : any name you like ...
# iso  : this character you want to get finally on screen
# key  : its name in X11
# ent  : its HTML-entity
# esc1 : number of escape character
# esc2 : the character typed after escape character
# text : any text for menu 
# spon : change typed special char to entity if on
# eson : change escaped char to entity if on
	global entities entlist
	set entities($name,iso) $iso
	set entities($name,key) $key
	set entities($name,ent) $ent
	set entities($name,esc1) $esc1
	set entities($name,esc2) $esc2
	set entities($name,text) $text
	set entities($name,spon) $spon
	set entities($name,eson) $eson
	lappend entlist $name
}

# all entities you want to use...

defentities "am" "&"    "Key-ampersand"   "&amp;"    0 "&"  "Ampersand" 0 1
defentities "gt" ">"    "Key-greater"     "&gt;"     0 ">"  "greater"   0 1
defentities "lt" "<"    "Key-less"        "&lt;"     0 "<"  "less"      0 1
defentities "qt" "\""   "Key-quotedbl"    "&quot;"   0 "\"" "quote"     0 1
defentities "ae" "\344" "Key-adiaeresis"  "&auml;"   0 "a"  "a-Umlaut"
defentities "Ae" "\304" "Key-Adiaeresis"  "&Auml;"   0 "A"  "A-Umlaut"
defentities "oe" "\366" "Key-odiaeresis"  "&ouml;"   0 "o"  "o-Umlaut"
defentities "Oe" "\326" "Key-Odiaeresis"  "&Ouml;"   0 "O"  "O-Umlaut"
defentities "ue" "\374" "Key-udiaeresis"  "&uuml;"   0 "u"  "u-Umlaut"
defentities "Ue" "\334" "Key-Udiaeresis"  "&Uuml;"   0 "U"  "U-Umlaut"
defentities "ss" "\337" "Key-ssharp"      "&szlig;"  0 "s"  "scharfes s"
defentities "ea" "\351" "Key-eacute"      "&eacute;" 1 "e"  "e-acute"
defentities "aa" "\341" "Key-aacute"      "&aacute;" 1 "a"  "a-acute"
defentities "eg" "\350" "Key-egrave"      "&egrave;" 2 "e"  "e-grave"
defentities "ag" "\340" "Key-agrave"      "&agrave;" 2 "a"  "a-grave"
defentities "ec" "\352" "Key-ecircumflex" "&ecirc;"  1 "x"  "e-circumflex"

# what to insert, if a special key is hit
proc insertspecial {ch1 ch2} {
	global insertspec insertescape nesc
# checkbutton is on, insert its entity
	if { $insertspec } then {
		set insertfound 0
		for { set i 0 } { $i < $nesc } { incr i } {
# any escape character is hit before, don't insert entity, but original char
			if { $insertescape($i) } then {
				.textframe.vp.text insert insert $ch2
				set insertescape($i) 0
				set insertfound 1
			}
		}
# insert entity
		if { $insertfound == 0 } then {
			.textframe.vp.text insert insert $ch1
		}
# insert original iso character
	} else {
		.textframe.vp.text insert insert $ch2
	}
}

#####################
# define bindings

proc MkEntities {} {
	global entlist entities esclist escesclist nesc

# define bindings for iso chars etc. if there are any on your keyboard...
	foreach ent $entlist {
		if { $entities($ent,spon) } {
			bind .textframe.vp.text <$entities($ent,key)> \
	"insertspecial \"$entities($ent,ent)\" \"$entities($ent,iso)\""
		}
	}

# define bindings for escape characters
	for { set i 0 } { $i < $nesc } { incr i } {
		bind .textframe.vp.text "[lindex $esclist $i]" {
			global insertesc insertescape nesc escapei
			set k $escapei(%A)
			if { $insertescape($k) } then {
				.textframe.vp.text insert insert \
					"[lindex $escesclist $k]"
				for { set j 0 } { $j < $nesc } { incr j } {
					set insertescape($j) 0
				}
			} else {
				for { set j 0 } { $j < $nesc } { incr j } {
					set insertescape($j) 0
				}
				if { $insertesc } then {
					set insertescape($k) 1
				} else {
					.textframe.vp.text insert insert "%A"
				}
			}
		}
	}
# redefine ALL other characters to check if they are preceeded 
# by a escape character
	bind .textframe.vp.text <Any-KeyPress> {
		global insertescape nesc esclist
		if { "%A" != "" } then {
			set insertfound 0
			for { set i 0 } { $i < $nesc } { incr i } {
				# this escape character is typed before
				if { $insertescape($i) && !$insertfound } then {
					foreach ent $entlist {
					   # right esc char and right entity?
					   if { $entities($ent,esc1) == $i && \
					      $entities($ent,eson) && \
					      "%A" == $entities($ent,esc2) } {
						%W insert insert \
						   "$entities($ent,ent)"
						set insertfound 1
					   }
					}
					# no legal combination, blank ->
					# escape character itself...
					if { !$insertfound && "%A" == " " } {
						%W insert insert \
							"[lindex $esclist $i]"
						set insertfound 1
					} elseif { !$insertfound } {
					# any other non-legal combination ->
					# insert esc char and next char...
						%W insert insert \
							"[lindex $esclist $i]%A"
						set insertfound 1
					}
					set insertescape($i) 0
				}
			}
			# there is no escape char, insert it...
			if { $insertfound == 0 } then {
				%W insert insert %A
			}
			%W yview -pickplace insert
		}
	}
}


#############################################
# Main text window, status window, and bindings

#############################################
# build main text window

proc MkMainText {} {

	frame .textframe 
	frame .textframe.vp
	text .textframe.vp.text -relief sunken -bd 2 \
		-yscrollcommand {.textframe.vp.mainscroll set} \
		-wrap char -setgrid true

	scrollbar .textframe.vp.mainscroll -orient vertical \
		-command mainscrollProc
	pack .textframe -fill both -expand true
	pack .textframe.vp -fill both -expand true
	pack .textframe.vp.text -side right -fill both -expand true 
	pack .textframe.vp.mainscroll -side left -pady 10 -fill y 

	MkMainTextBindings
}

#############################################
# build status line (filename/event)

proc MkMainStatus {} {
	frame .textframe.stati
	label .textframe.stati.statusl -text "Filename:" -width 9
	label .textframe.stati.status -textvariable {filename} \
		-relief groove -bd 2 -width 50
	label .textframe.stati.eventl -text "Event:" -width 6
	label .textframe.stati.event -textvariable {event} \
		-relief groove -bd 2 -width 25

	pack .textframe.stati -side bottom -fill x -expand 0 -anchor s


	pack .textframe.stati.statusl -side left -in .textframe.stati
	pack .textframe.stati.status -side left -in .textframe.stati
	pack .textframe.stati.eventl -side left -in .textframe.stati
	pack .textframe.stati.event -side left -in .textframe.stati
}

#############################################
# main window scrolling procedure

proc mainscrollProc {index} {
	.textframe.vp.mainscroll  config \
		-command {.textframe.vp.text  yview }
}

#############################################
# keyboard selection

proc MoveSelect {w X Y x y} {
        if { ![ IsSelected ] } {
                tk_textSelectTo $w insert
	}
	set sx [ $w index @$X,$Y ]
	set sf [ $w index sel.first ]
	set sl [ $w index sel.last ]
	set i [ tk_textIndexCloser $w $sx $sf $sl ]
	set j [ expr 1-$i ]
	set x1 [ expr "$x*$i" ]
	set x2 [ expr "$x*$j" ]
	set y1 [ expr "$y*$i" ]
	set y2 [ expr "$y*$j" ]
	if { $x1 > 0 } { set sf [ $w index "$sf +$x1 c" ] }
	if { $x1 < 0 } { set sf [ $w index "$sf  $x1 c" ] }
	if { $x2 > 0 } { set sl [ $w index "$sl +$x2 c" ] }
	if { $x2 < 0 } { set sl [ $w index "$sl  $x2 c" ] }
	if { $y1 > 0 } { set sf [ $w index "$sf +$y1 l" ] }
	if { $y1 < 0 } { set sf [ $w index "$sf  $y1 l" ] }
	if { $y2 > 0 } { set sl [ $w index "$sl +$y2 l" ] }
	if { $y2 < 0 } { set sl [ $w index "$sl  $y2 l" ] }
	$w tag remove sel 0.0 end
	$w tag add sel $sf $sl
	
}



#############################################
# main window bindings

proc MkMainTextBindings {} {
	global netscape
	bind .textframe.vp.text <Button-3> {
		selection clear .textframe.vp.text
	}
	bind .textframe.vp.text <Control-Key-C> {
		global CUTBUFFER
		set CUTBUFFER [selection get]
	}
	bind .textframe.vp.text <Control-Key-V> {
		global CUTBUFFER
		.textframe.vp.text insert insert $CUTBUFFER
	}
	bind .textframe.vp.text <Control-Key-X> {
		global CUTBUFFER
		set CUTBUFFER [selection get]
		.textframe.vp.text delete sel.first sel.last
	}
	bind .textframe.vp.text <Control-Key-a> {
		.textframe.vp.text mark set insert "insert linestart"
	}
	bind .textframe.vp.text <Control-Key-b> {
		%W mark set insert insert-1c
	}
	bind .textframe.vp.text <Control-Key-c> {
		global CUTBUFFER
		set CUTBUFFER [selection get]
	}
	bind .textframe.vp.text <Control-Key-e> {
		.textframe.vp.text mark set insert "insert lineend"
	}
	bind .textframe.vp.text <Control-Key-f> {
		%W mark set insert insert+1c
	}
	bind .textframe.vp.text <Control-Key-k> {
		if {[%W compare insert == "insert lineend"]} {
			.textframe.vp.text delete insert "insert lineend +1c"
		} else {
			.textframe.vp.text delete insert "insert lineend"
		}
	}
	bind .textframe.vp.text <Control-Key-n> {
		%W mark set insert  "insert+1l"
	}
	bind .textframe.vp.text <Control-Key-p> {
		%W mark set insert "insert-1l"
	}
	bind .textframe.vp.text <Control-Key-P> {
		SaveForPreview
	}
	bind .textframe.vp.text <Control-Key-q> {
		global filename
		QuitDlg
	}
	bind .textframe.vp.text <Control-Key-s> {
		global filename
		SaveFileDlg SaveCmd
	}
	bind .textframe.vp.text <Control-Key-v> {
#		global CUTBUFFER
#		.textframe.vp.text insert insert $CUTBUFFER
		.textframe.vp.text mark set insert "insert +24 lines"
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Control-Key-y> {
		.textframe.vp.text mark set insert "insert -24 lines"
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Control-Key-x> {
		global CUTBUFFER
		if {[IsSelected] == 1} {
			set CUTBUFFER [selection get]
			.textframe.vp.text delete sel.first sel.last
		}
	}
	bind .textframe.vp.text <Control-Key-S> {
		SearchDlg
	}
	bind .textframe.vp.text <Control-Key-F> {
		Search
	}
	bind .textframe.vp.text <Enter> {
		focus .textframe.vp.text
	}
	bind .textframe.vp.text <Key-Delete> {
		if {[IsSelected] == 0} {
			tk_textBackspace %W
			%W yview -pickplace insert
		} else {
			.textframe.vp.text delete sel.first sel.last
		}
	}
	# thanks Heiko Jacobs <jacobs@ipf.bau-verm.uni-karlsruhe.de>
	bind .textframe.vp.text <Key-BackSpace> {
		if {[IsSelected] == 0} {
			tk_textBackspace %W
			%W yview -pickplace insert
		} else {
			.textframe.vp.text delete sel.first sel.last
		}
	}

	# delete whole entities, functions, strings, words...
	bind .textframe.vp.text <Shift-Key-BackSpace> {
		global CUTBUFFER
		set x [ %W get insert-1c ]
		if { $x == ";" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "&" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} elseif { $x == ">" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "<" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} elseif { $x == "\"" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "\"" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} else {
			set j [ %W index "insert-1c wordstart" ]
			set CUTBUFFER [ %W get $j insert ]
			%W delete $j insert
		}
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Shift-Key-Delete> {
		global CUTBUFFER
		set x [ %W get insert-1c ]
		if { $x == ";" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "&" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} elseif { $x == ">" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "<" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} elseif { $x == "\"" } then {
			set j [ %W index "insert linestart" ]
			set l [ %W get $j insert-1c ]
			set i [ string last "\"" $l ]
			if { $i < 0 } { tk_textBackspace %W } else {
				set j [ %W index "insert linestart +$i c" ]
				set CUTBUFFER [ %W get $j insert ]
				%W delete $j insert
			}
		} else {
			set j [ %W index "insert-1c wordstart" ]
			set CUTBUFFER [ %W get $j insert ]
			%W delete $j insert
		}
	}
	# split line and move to start of next line (removed for now)
	bind .textframe.vp.text <Key-Return> {
		%W insert insert "\n"
#		%W mark set insert "insert +1l linestart"
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Key-Down> {
		%W mark set insert insert+1l
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Key-End> {
		%W mark set insert "insert lineend"
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Control-End> {
		%W mark set insert end
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Key-Home> {
		%W mark set insert "insert linestart"
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Control-Home> {
		%W mark set insert 1.0
		%W yview -pickplace insert
	} 
	bind .textframe.vp.text <Key-Left> {
		%W mark set insert insert-1c
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Key-Right> {
		%W mark set insert insert+1c
		%W yview -pickplace insert
	}
	bind .textframe.vp.text <Key-Up> {
		%W mark set insert insert-1l
		%W yview -pickplace insert
	}
	if {$netscape == 1} {
		bind .textframe.vp.text <Mod1-Key-C> {
			DoFormat <CENTER>\n </CENTER>\n
		}
	}
	bind .textframe.vp.text <Mod1-Key-1> {DoFormat <H1> </H1>}
	bind .textframe.vp.text <Mod1-Key-2> {DoFormat <H2> </H2>}
	bind .textframe.vp.text <Mod1-Key-3> {DoFormat <H3> </H3>}
	bind .textframe.vp.text <Mod1-Key-4> {DoFormat <H4> </H4>}
	bind .textframe.vp.text <Mod1-Key-5> {DoFormat <H5> </H5>}
	bind .textframe.vp.text <Mod1-Key-6> {DoFormat <H6> </H6>}
	bind .textframe.vp.text <Mod1-Key-C> {DoFormat "<!--" ">"}
	bind .textframe.vp.text <Mod1-Key-H> {
		if {[IsSelected] == 1} {
			set temp [selection get]
			set temp2 [GetUrl hyperlink]
			if {[string length $temp2] != 0 } {
				.textframe.vp.text insert sel.first "<A HREF=\"$temp2\">$temp</A>"
				.textframe.vp.text delete sel.first sel.last
			}
		} else {
			DoFormat "<A HREF=\"" "\">CHANGE_ME</A>"
		}
	}
	bind .textframe.vp.text <Mod1-Key-I> {
		if {[IsSelected] == 1} {
			set temp [selection get]
			.textframe.vp.text insert sel.first "<IMG SRC=\"$temp\">"
			.textframe.vp.text delete sel.first sel.last
		} else {
			DoFormat "<IMG SRC=\"" "\">"
		}
	}
	bind .textframe.vp.text <Mod1-Key-P> {DoFormat <PRE> </PRE>}
	bind .textframe.vp.text <Mod1-Key-Return> {
		.textframe.vp.text insert insert "<P>"
	}
	bind .textframe.vp.text <Mod1-Key-a> {DoFormat <ADDRESS> </ADDRESS>}
	bind .textframe.vp.text <Mod1-Key-ampersand> {
		.textframe.vp.text insert insert "&amp;"
	}
	bind .textframe.vp.text <Mod1-Key-b> {DoFormat "<B>" "</B>"}
	bind .textframe.vp.text <Mod1-Key-c> {DoFormat <CODE> </CODE>}
	bind .textframe.vp.text <Mod1-Key-e> {DoFormat <EM> </EM>}
	bind .textframe.vp.text <Mod1-Key-greater> {
		.textframe.vp.text insert insert "&gt;"
	}
	bind .textframe.vp.text <Mod1-Key-h> {
		.textframe.vp.text insert insert "<HR>\n"
	}
	bind .textframe.vp.text <Mod1-Key-i> {DoFormat <I> </I>}
	bind .textframe.vp.text <Mod1-Key-l> {
		.textframe.vp.text insert insert "<LI>"
	}
	bind .textframe.vp.text <Mod1-Key-less> {
		.textframe.vp.text insert insert "&lt;"
	}
	bind .textframe.vp.text <Mod1-Key-p> {
		.textframe.vp.text insert insert "<BR>"
	}
	bind .textframe.vp.text <Mod1-Key-quotedbl> {
		.textframe.vp.text insert insert "&quot;"
	}
	bind .textframe.vp.text <Mod1-Key-s> {DoFormat <STRONG> </STRONG>}
	bind .textframe.vp.text <Mod1-Key-space> {
		.textframe.vp.text insert insert "&nbsp;"
	}
	bind .textframe.vp.text <Mod1-Key-t> {DoFormat <TITLE> </TITLE>}
	bind .textframe.vp.text <Mod1-Key-u> {DoFormat <U> </U>}
	bind .textframe.vp.text <Shift-Left> {
		MoveSelect %W %x %y -1 0
	}
	bind .textframe.vp.text <Shift-Right> {
		MoveSelect %W %x %y 1 1
	}
	bind .textframe.vp.text <Shift-Down> {
		MoveSelect %W %x %y 0 1
	}
	bind .textframe.vp.text <Shift-Up> {
		MoveSelect %W %x %y 0 -1
	}
}

#############################################
# check if there is something in selection
# returns 1 if there is, 0 if selection is empty

proc IsSelected {} {
	if {[selection own] != ""} {
		if [catch {selection get} result] {
			return 0
		} else {
			if {[string match *tagged* $result] == 0} {
				return 1
			} else {
				return 1
			}
		}
	} else {
		return 0
	}
}


#############################################
# Create formatting tags in main window
# if there is a selection, place it between the tags
# if there is no selection, place the cursor between them
#
# open is the opening format command <X>
# close is the clincher </X>

proc DoFormat {open close} {
	if {[IsSelected] == 0} {
		.textframe.vp.text insert insert "$open$close"
		set length [string length $close]
		.textframe.vp.text mark set insert "insert -$length c"
	} else {
		set temp [selection get]
		.textframe.vp.text insert sel.first "$open$temp$close"
		.textframe.vp.text delete sel.first sel.last
	}
}


proc Search {} {

	global searchstring
	global direction

	if {[winfo exists .search] == 1} {
		destroy .search
	}

	if {[string length $searchstring] == 0} {
		return 0
	}

	if {$direction == "for"} {
		set lastfirst first
		set textpart [.textframe.vp.text get insert end]
		set countfrom insert
	} else {
		set lastfirst last
		set textpart [.textframe.vp.text get 0.0 {insert -1char}]
		set countfrom 0.0
	}

	set foundpos [string $lastfirst $searchstring $textpart]

	if {$foundpos == -1} then {
		GenericDialog "\"$searchstring\" not found."
		return
	}

	if {[IsSelected] == 1} {
		if {[selection own] != ""} {
			selection clear .textframe.vp.text
		}
	} 
	
	set lastpos [expr {$foundpos + [string length $searchstring]}]
	.textframe.vp.text tag add sel \
		"$countfrom + $foundpos chars" "$countfrom + $lastpos chars"

	.textframe.vp.text mark set insert "$countfrom + $lastpos chars"
	.textframe.vp.text yview -pickplace insert
}

proc SearchDlg {} {

	global searchstring direction searchstate

	ClearEvent "Search..."

	set foo ""
	set string ""

	toplevel .search
	wm title .search "Search"
	set x [expr 275 + [winfo x .]]
	set y [expr 140 + [winfo y .]]
	wm geometry .search +$x+$y

	frame .search.top -relief raised -bd 1
	label .search.top.lbl -text "Enter the search string"
	entry .search.top.entry -textvariable foo

	radiobutton .search.top.forward -text "Forward" -variable direction \
		-value for -relief flat -command {set direction "for"}
	radiobutton .search.top.backward -text "Backward" \
		-variable direction \
		-value back -relief flat -command {set direction "back"}

	tixDlgBtns .search.btns
	.search.btns add ok -text "Ok" -width 8
	.search.btns add cancel -text "Cancel" -width 8

	bind .search <Enter> {
		focus .search.top.entry
	}

	bind .search.top.entry <Key-Return> { 
		set searchstring $foo
		Search
	}
	pack .search.top.lbl -expand yes -fill x -padx 10 -pady 10
	pack .search.top.entry -expand yes -fill x -padx 10 -pady 10
	pack .search.top.forward -side left
	pack .search.top.backward -side right
	pack .search.top -expand yes -fill both
	pack .search.btns -fill x

	.search.btns button ok config -command {
		set searchstring $foo
		Search
	}

	.search.btns button cancel config -command {
		destroy .search
	}

}

proc GenericDialog {message} {

	if {[winfo exists .message] == 1} {
		destroy .message
	}
	toplevel .message
	wm title .message "Notice"

#	tk_dialog .message "Notice" "$message" warning 0 "Ok"

	set x [expr 255 + [winfo x .]]
	set y [expr 150 + [winfo y .]]
	wm geometry .message +$x+$y
	
	frame .message.top -relief raised -bd 1
	pack .message.top -side top -fill both -expand true
	frame .message.bot -relief raised -bd 1
	pack .message.bot -side bottom -fill both -expand true
	
	message .message.top.msg -text $message -width 200 \
		-font "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"
	pack .message.top.msg -side right -expand 1 -fill both \
		-padx 5m -pady 5m 
	
	button .message.bot.ok -text "Close" -command {destroy .message} 
	pack .message.bot.ok -padx 5m -pady 5m
}

proc SearchReplaceDlg {} {

	global searchstring direction searchstate confirm

	ClearEvent "Search and Replace..."

	set foo ""
	set string ""

	toplevel .searchr
	wm title .searchr "Search"
	set x [expr 250 + [winfo x .]]
	set y [expr 140 + [winfo y .]]
	wm geometry .searchr +$x+$y

	frame .searchr.top -relief raised -bd 1
	label .searchr.top.lbl -text "Search string"
	entry .searchr.top.entry -textvariable foo
	label .searchr.top.lbl2 -text "Replacement string"
	entry .searchr.top.entry2 -textvariable foo2

	frame .searchr.top.dir -relief groove -bd 1
	radiobutton .searchr.top.dir.forward -text "Forward" \
		-variable direction \
		-value for -relief flat -command {set direction "for"}
	radiobutton .searchr.top.dir.backward -text "Backward" \
		-variable direction \
		-value back -relief flat -command {set direction "back"}

	checkbutton .searchr.top.query -text "Confirm?" -variable confirm \
		-relief flat

	tixDlgBtns .searchr.btns
	.searchr.btns add ok -text "Ok" -width 8
	.searchr.btns add cancel -text "Cancel" -width 8

	bind .searchr <Enter> {
		focus .searchr.top.entry
	}

	bind .searchr.top.entry <Key-Tab> {
		focus .searchr.top.entry2
	}
	bind .searchr.top.entry2 <Key-Tab> {
		focus .searchr.top.entry
	}

	bind .searchr.top.entry <Key-Return> { 
		set searchstring $foo
		set replacestring $foo2
		SearchReplace
	}
	bind .searchr.top.entry2 <Key-Return> {
		set searchstring $foo
		set replacestring $foo2
		SearchReplace
	}

	pack .searchr.top.lbl -expand yes -fill x -padx 10
	pack .searchr.top.entry -expand yes -fill x -padx 10 -pady 10
	pack .searchr.top.lbl2 -expand yes -fill x -padx 10
	pack .searchr.top.entry2 -expand yes -fill x -padx 10 -pady 10
	pack .searchr.top.query -side bottom
	pack .searchr.top.dir -side bottom -pady 10 -padx 10
	pack .searchr.top.dir.forward -side left
	pack .searchr.top.dir.backward -side right
	pack .searchr.top -expand yes -fill both
	pack .searchr.btns -fill x

	.searchr.btns button ok config -command {
		set searchstring $foo
		set replacestring $foo2
		SearchReplace
	}

	.searchr.btns button cancel config -command {
		destroy .searchr
		return
	}

}

proc SearchReplace {} {

	global searchstring direction searchstate confirm replacestring
	global confirmopt

	destroy .searchr

	ClearEvent "Search and replace..."

	if {[string length $searchstring] == 0} {
		return 0
	}

	set foundpos 1

	while {$foundpos != -1} {

	if {[IsSelected] == 1} {
		if {[selection own] != ""} {
			selection clear .textframe.vp.text
		}
	} 


	if {$direction == "for"} {
		set lastfirst first
		set textpart [.textframe.vp.text get insert end]
		set countfrom insert
	} else {
		set lastfirst last
		set textpart [.textframe.vp.text get 0.0 {insert -1char}]
		set countfrom 0.0
	}

	set foundpos [string $lastfirst $searchstring $textpart]


	if {$foundpos != -1} then {
		set lastpos [expr {$foundpos + [string length $searchstring]}]
		.textframe.vp.text tag add sel \
			"$countfrom + $foundpos chars" "$countfrom + $lastpos chars"	

		.textframe.vp.text mark set insert "$countfrom + $lastpos chars"
		.textframe.vp.text yview -pickplace insert

		if {$confirm != 1} {
			.textframe.vp.text del sel.first sel.last
			.textframe.vp.text insert insert $replacestring
		} else {
			ConfirmDlg
			tkwait window .confirm
			if {$confirmopt == 1} {
				.textframe.vp.text del sel.first sel.last
				.textframe.vp.text insert insert $replacestring
			} elseif {$confirmopt == 0} {
				# skip
			} else {
				if {[IsSelected] == 1} {
					selection clear .textframe.vp.text
				}
				return
			}
		
		}
	}

	}
}

proc ConfirmDlg {} {
	global searchstring replacestring confirmoptt

	if [winfo exists .confirm] {
		return
	}

	toplevel .confirm

	wm title .confirm "Confirm"

	set x [expr 420 + [winfo x .]]
	set y [expr 50 + [winfo y .]]

	wm geometry .confirm +$x+$y

	frame .confirm.top -relief raised -border 1
	message .confirm.top.msg \
		-font -*-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-* \
		-relief sunken -bd 1 -anchor n -padx 40 -width 370\
		-text "Replace \"$searchstring\" with \"$replacestring\"?"

	tixDlgBtns .confirm.btns
	.confirm.btns add replace -text "Replace"   -width 8
	.confirm.btns add ignore  -text "Ignore" -width 8
	.confirm.btns add cancel -text "Cancel" -width 8

	pack .confirm.top.msg -expand yes -fill both -padx 10 -pady 10
	pack .confirm.top -expand yes -fill both 
	pack .confirm.btns -fill x

	.confirm.btns button replace config -command {
		set confirmopt 1
		destroy .confirm
	}
	.confirm.btns button ignore  config -command {
		set confirmopt 0
		destroy .confirm
	}
	.confirm.btns button cancel config -command {
		set confirmopt -1
		destroy .confirm
	}

}




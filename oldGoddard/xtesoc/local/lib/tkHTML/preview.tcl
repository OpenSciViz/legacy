##################
# Preview-mode stuff
# most of this was taken from tkWWW, salted-to-taste


proc Preview {pagename} {
	global filename
	global current
	set current $pagename

	ClearEvent "Creating preview window"

	toplevel .preview

	frame .preview.t -relief raised -borderwidth 2
	label .preview.t.label -text "Document Title: "  
	pack .preview.t.label -side left -expand false -padx 20
	label .preview.t.t 
	pack .preview.t.t -side left -expand true -fill x

	frame .preview.vp 
	
	text .preview.vp.txt -yscrollcommand {.preview.vp.sc set} \
		-relief sunken -borderwidth 2  \
		-wrap word -background #bfbfbfbfbfbf -height 24 -width 80


	scrollbar .preview.vp.sc -orient vertical -borderwidth 2 \
		-relief sunken -command {.preview.vp.txt yview} 

	pack .preview.t -side top -fill x -expand false
	pack .preview.vp -fill both -expand true
	pack .preview.vp.txt -side right -fill both -expand true
	pack .preview.vp.sc -side left -fill both

	button .preview.reload -text "Reload" \
		-command {
			SaveForPreview
		}
	button .preview.ok -text "Close" \
		-command {
			if {[info commands XFDestroy] != ""} {
				catch {HtUncache "$pagename"}
				catch {XFDestroy .preview}
			} else {
				catch {HtUncache "$pagename"}
			catch {destroy .preview}
			}
		}

	tixCombobox .preview.font  -type static -width 1 \
		-fancy yes -command {ChangePreviewFont}
	.preview.font appendhistory "Times"
	.preview.font appendhistory "Helvetica"
	.preview.font appendhistory "New Century Schoolbook"
	.preview.font appendhistory "Lucida"
	SetFontStartup

	pack .preview.ok .preview.reload .preview.font -fill both \
		-expand true -side left -padx 5 -pady 2

DoPreview $pagename
#tkW3FontInitialize
}

#######################################
# set the fotn when you start previwer

proc SetFontStartup {} {
	global typeface
	if {$typeface == "times"} {
		.preview.font pick "Times"
	} elseif {$typeface == "helvetica"} {
		.preview.font pick "Helvetica"
	} elseif {$typeface == "century"} {
		.preview.font pick "New Century Schoolbook"
	} elseif {$typeface == "lucida"} {
		.preview.font pick "Lucida"
	}
}

######################
# change the previewer font
#

proc ChangePreviewFont {string} {
	global current
	global typeface
	if {$string == "Times"} {
		set typeface "times"
		DoPreview $current		
	} elseif {$string == "Helvetica"} {
		set typeface "helvetica"
		DoPreview $current
	} elseif {$string == "New Century Schoolbook"} {
		set typeface "century"
		DoPreview $current
	} elseif {$string == "Lucida"} {
		set typeface "lucida"
		DoPreview $current
	}

}

############################################
# this is nasty, but it works
# I should procedurize the font-building

proc DoPreview {pagename} {
	global typeface
	global filename

	ClearEvent "Previewing..."

	tkW3OutputCursorWait
	catch {HtUncache "$pagename"}
	#tkW3HtBeginDoc
	.preview.vp.txt delete 1.0 end

	if {$typeface == "times"} {
		tkW3FontSetTextFonts .preview.vp.txt {
			{"" "-adobe-times-medium-r-*-*-12-*-*-*-*-*-*-*"}
			{"H1" "-adobe-times-bold-r-normal-*-24-*-*-*-*-*-*-*"}
			{"H2" "-adobe-times-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H3" "-adobe-times-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H4" "-adobe-times-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"H5" "-adobe-times-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"H6" "-adobe-times-bold-r-normal-*-10-*-*-*-*-*-*-*"}
			{"ADDRESS" "-adobe-times-medium-i-normal-*-14-*-*-*-*-*-*-*"}
			{"XMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"PRE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"TT" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"CODE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"SAMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"EM" "-adobe-times-bold-r-*-*-12-*-*-*-*-*-*-*"}
			{"STRONG" "-adobe-times-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"B" "-adobe-times-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"I" "-adobe-times-medium-i-normal-*-14-*-*-*-*-*-*-*"}  
		}
	} elseif {$typeface == "century" } {
		tkW3FontSetTextFonts .preview.vp.txt {
			{"" "-adobe-new century schoolbook-medium-r-*-*-12-*-*-*-*-*-*-*"}
			{"H1" "-adobe-new century schoolbook-bold-r-normal-*-24-*-*-*-*-*-*-*"}
			{"H2" "-adobe-new century schoolbook-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H3" "-adobe-new century schoolbook-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H4" "-adobe-new century schoolbook-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"H5" "-adobe-new century schoolbook-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"H6" "-adobe-new century schoolbook-bold-r-normal-*-10-*-*-*-*-*-*-*"}
			{"ADDRESS" "-adobe-new century schoolbook-medium-r-normal-*-14-*-*-*-*-*-*-*"}
			{"XMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"PRE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"TT" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"CODE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"SAMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"B" "-adobe-new century schoolbook-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"EM" "-adobe-new century schoolbook-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"STRONG" "-adobe-new century schoolbook-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"I" "-adobe-new century schoolbook-medium-i-normal-*-14-*-*-*-*-*-*-*"}
		}
	} elseif {$typeface == "lucida" } {
		tkW3FontSetTextFonts .preview.vp.txt {
			{"" "-b&h-lucida-medium-r-*-*-12-*-*-*-*-*-*-*"}
			{"H1" "-b&h-lucida-bold-r-normal-*-24-*-*-*-*-*-*-*"}
			{"H2" "-b&h-lucida-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H3" "-b&h-lucida-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H4" "-b&h-lucida-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"H5" "-b&h-lucida-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"H6" "-b&h-lucida-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"ADDRESS" "-b&h-lucida-medium-i-normal-*-14-*-*-*-*-*-*-*"}
			{"XMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"PRE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"TT" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"CODE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"SAMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"B" "-b&h-lucida-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"EM" "-b&h-lucida-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"STRONG" "-b&h-lucida-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"I" "-b&h-lucida-medium-i-normal-*-14-*-*-*-*-*-*-*"}
		}
	} elseif {$typeface == "helvetica"} {
		tkW3FontSetTextFonts .preview.vp.txt {
			{"" "-adobe-helvetica-medium-r-*-*-14-*-*-*-*-*-*-*"}
			{"H1" "-adobe-helvetica-bold-r-normal-*-24-*-*-*-*-*-*-*"}
			{"H2" "-adobe-helvetica-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H3" "-adobe-helvetica-bold-r-normal-*-18-*-*-*-*-*-*-*"}
			{"H4" "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"H5" "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"H6" "-adobe-helvetica-bold-r-normal-*-10-*-*-*-*-*-*-*"}
			{"ADDRESS" "-adobe-helvetica-medium-r-normal-*-14-*-*-*-*-*-*-*"}
			{"XMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"PRE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"TT" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"CODE" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"SAMP" "-adobe-courier-medium-r-normal-*-12-*-*-*-*-*-*-*"}
			{"B" "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"}
			{"EM" "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*"}
			{"STRONG" "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"}
			{"I" "-adobe-helvetica-medium-o-normal-*-14-*-*-*-*-*-*-*"}
		}
	}
	catch {HtLoad $pagename}
	tkW3OutputCursorNormal
}	


# Procedure: insertWithTags
proc insertWithTags { w text args} {
	set start [$w index insert]
	$w insert insert $text
	foreach tag [$w tag names $start] {
		$w tag remove $tag $start insert
	}
	foreach i $args {
		$w tag add $i $start insert
	}
}


# Procedure: tkW3FontInitialize
proc tkW3FontInitialize {} {
	global tkW3ConfigFont tkW3ConfigFontList tkW3ConfigFontDefault
#	puts "tkW3FontInitialize"

#	foreach item $tkW3ConfigFontList {
#		pack append .font_dialog.f  [radiobutton .font_dialog.f.[lindex $item 0]  \
			-text [lindex $item 1] -relief flat  \
			-variable font_name -anchor w ] {top fillx}
#	}

	tkW3FontSetTextFonts .preview.vp.txt $tkW3ConfigFont(times)
#	$tkW3ConfigFont($tkW3ConfigFontDefault)
}


# Procedure: tkW3FontSetTextFonts
proc tkW3FontSetTextFonts { w list} {
#	puts "tkW3FontSetTextFonts"
	foreach font $list {
		set style [lindex $font 0]
		if {$style == ""} {
			$w configure -font [lindex $font 1]
		} else {
			$w tag configure $style -font [lindex $font 1]
		}
	}
	$w tag configure U -underline true
}


# Procedure: tkW3HtAdd
proc tkW3HtAdd { string {styles ""}} {
	global tkW3HtText tkW3HtPage
#	puts "tkW3HtAdd: styles->$styles"

	if $tkW3HtText(in_anchor) {
		lappend styles Anchor
		lappend styles h$tkW3HtPage(anchor.index)
	}

	lappend styles [tkW3HtListLast tkW3HtText(para.stack)]
  
	if [llength $tkW3HtText(char.stack)] {
		lappend styles [tkW3HtListLast tkW3HtText(char.stack)]
	}
	tkW3HtInsert $string $styles
}


# Procedure: tkW3HtAddBul
proc tkW3HtAddBul { type} {
	global tkW3HtText
#	puts "tkW3HtAddBul: type->$type"

	switch -exact $type {
		BR {
			tkW3HtInsert "\n" BR
			return
		}

		HR {
			tkW3HtInsert "\n_______________________________________________________________________________\n" HR
		return
		}

		P {
			tkW3HtInsert "\n\n" P
		return
		}
	
		DT {
			tkW3HtBlankLines 1
		}
    	}

	tkW3HtInsert "\n" blank
	for {set i 1} {$i < [llength $tkW3HtText(list.stack)]} {incr i} {
		if {$type == "DD"} {
			tkW3HtInsert "\t" "DD"
		} {
			tkW3HtInsert "\t" "LI"
		}
	}

	case $type {
		"LI" {
			tkW3HtInsert "   * " "LI type.[tkW3HtListLast tkW3HtText(list.stack)]"
		}
		"DD" {
			tkW3HtInsert "     * " "DD"
		}
		"DT" {
			tkW3HtInsert "     " "LI type.DL"
		}
	}
}


# Procedure: tkW3HtBegin
proc tkW3HtBegin { stack elem} {
	global tkW3HtText
#	puts "tkW3HtBegin: stack->$stack elem->$elem"
 
	switch -regexp $elem {
		{^H[1-9]+$} -
		{^ADDRESS$} {
			tkW3HtBlankLines 2
		}
		{^XMP$} -
		{^PRE$} {
			tkW3HtBlankLines 1
		}
		{^Q$} {
			tkW3HtInsert "``" blank
		}
	}
	lappend tkW3HtText($stack.stack) $elem
}


# Procedure: tkW3HtBeginAnc
proc tkW3HtBeginAnc { name href} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtBeginAnc: name->$name href->$href"

	set i $tkW3HtPage(anchor.index)
	set tkW3HtPage(anchor.name.$i) $name
	set tkW3HtPage(anchor.href.$i) $href
	set tkW3HtPage(id.$name) h$i
	set tkW3HtText(in_anchor) 1

#	if {$i >= $tkW3HtText(anchor.max)} {
		.preview.vp.txt tag configure h$i -foreground blue  \
			-background $tkW3HtText(background)
		incr tkW3HtText(anchor.max)
#	}
}


# Procedure: tkW3HtBeginDoc
proc tkW3HtBeginDoc {} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtBeginDoc"

	unset tkW3HtPage
	set tkW3HtPage(anchor.index) 1
	set tkW3HtPage(image.index) 1
	set tkW3HtPage(is_index) 0
	set tkW3HtPage(modified) 0
	set tkW3HtPage(base) ""
	set tkW3HtPage(base.use) 0
	set tkW3HtPage(next_id) ""
	set tkW3HtPage(index.href) ""
	set tkW3HtPage(link) ""

	set tkW3HtText(in_anchor) 0
	set tkW3HtText(list.stack) {}
	set tkW3HtText(para.stack) {}
	set tkW3HtText(char.stack) {}
	set tkW3HtText(background) [lindex [.preview.vp.txt configure \
		-background] 4]
#	set i $tkW3HtPage(anchor.index)
#	set tkW3HtPage(anchor.name.$i) ""
#	set tkW3HtPage(anchor.href.$i) ""
	tkW3OutputClearBody
}


# Procedure: tkW3HtBlankLines
proc tkW3HtBlankLines { n} {
#	puts "tkW3HtBlankLines: n->$n"
	set text [regexp {^[ ]*$}  [.preview.vp.txt get {insert linestart} \
		{insert - 1 char lineend}]]
	if {!$text} {
		for {set i 0} {$i<$n} {incr i} {
			tkW3HtInsert "\n" blank
		}
	}
}


# Procedure: tkW3HtButtonPress
proc tkW3HtButtonPress { w loc b} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtButtonPress: w->$w loc->$loc b->$b"
	set tag_list [.preview.vp.txt tag names $loc]
	set index [lsearch -regexp $tag_list  {^i[0-9]+$}]
 
	if {$index == -1} {
		set index [lsearch -regexp $tag_list  {^h[0-9]+$}]
	}
	if {$index != -1} {
		$w tag configure [lindex $tag_list $index] -relief sunken
		update idletasks
	}
}


# Procedure: tkW3HtButtonRelease
proc tkW3HtButtonRelease { w loc b} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtButtonRelease"
	set tag_list [.preview.vp.txt tag names $loc]

	# Search for active tag
	# image tags override hypertext anchor tags 
	set index [lsearch -regexp $tag_list  {^i[0-9]+$}]
	if {$index == -1} {
		set index [lsearch -regexp $tag_list  {^h[0-9]+$}]
	}

	if {$index != -1} {
		set tag [lindex $tag_list $index]
		$w tag configure $tag -relief raised
		update idletasks
		regexp {([hi])([0-9]+)} $tag {} tag_type i
		switch $tag_type {
			"h" {
				switch $b {
					"1" {
						tkW3NavigateRecordAndGoto $tkW3HtPage(anchor.href.$i)
					}
					"2" {
						tkW3NavigateClone $tkW3HtPage(anchor.href.$i)
					}
					"3" {
						tkW3EditSetupAnchorDialog $w $i
					}
				}
			}
			"i" {
				switch $b {
					"1" {
						tkW3NavigateRecordAndGoto  $tkW3HtPage(image.$i) {}  $tkW3HtPage(image.anchor.$i)  $tkW3HtPage(image.ismap.$i)
					}
					"2" {
						tkW3NavigateClone $tkW3HtPage(image.$i) 
					}
					"3" {
						tkW3EditChangeImage $i
					}
				}
			}
		}
	}
}


# Procedure: tkW3HtEnd
proc tkW3HtEnd { stack elem} {
	global tkW3HtText
#	puts "tkW3HtEnd: stack->$stack elem->$elem"

	tkW3HtListPop tkW3HtText($stack.stack)

	switch -regexp $elem {
		{^H[1-9]+$} -
		{^ADDRESS$} {
			tkW3HtBlankLines 1
		}
		{^Q$} {
			tkW3HtInsert "''" blank
		}
	}

	if {$stack == "list" && $tkW3HtText(list.stack) == {}} {
		tkW3HtBlankLines 2
	}
}


# Procedure: tkW3HtEndAnc
proc tkW3HtEndAnc {} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtEndAnc"
	set tkW3HtText(in_anchor) 0
	incr tkW3HtPage(anchor.index)
}


# Procedure: tkW3HtEndDoc
proc tkW3HtEndDoc {} {
	global tkW3HtPage
#	puts "tkW3HtEndDoc"
	if {$tkW3HtPage(base) == ""} {
		set tkW3HtPage(base) $tkW3HtPage(address)
	}
}


# Procedure: tkW3HtInsert
proc tkW3HtInsert { text {list ""}} {
	set start [.preview.vp.txt index insert]
#	puts "tkW3HtInsert: text->$text list->$list"
	.preview.vp.txt insert insert $text
	foreach tag [.preview.vp.txt tag names $start] {
		.preview.vp.txt tag remove $tag $start insert
	}
	foreach tag $list {
		.preview.vp.txt tag add $tag $start insert
	}
}


# Procedure: tkW3HtListLast
proc tkW3HtListLast { in_list} {
#	puts "tkW3HtListLast: in_list->$in_list"
	upvar $in_list list
	set index [llength $list]
	incr index -1
	lindex $list $index
}


# Procedure: tkW3HtListPop
proc tkW3HtListPop { in_list} {
#	puts "tkW3HtListPop: in_list->$in_list"
	upvar $in_list list
	set index [llength $list]
	incr index -1
	set item [lindex $list $index]
	incr index -1
	set list [lrange $list 0 $index]
	return $item
}


# Procedure: tkW3HtProgress
proc tkW3HtProgress { msg} {
#	puts "tkW3HtProgress: msg->$msg"
	tkW3OutputSetMessage $msg
	update idletasks
}


# Procedure: tkW3HtSetBase
proc tkW3HtSetBase { href} {
	global tkW3HtPage
#	puts "tkW3HtSetBase: href->$href"
	set tkW3HtPage(base.use) 1
	set tkW3HtPage(base) $href
}


# Procedure: tkW3HtSetImg
proc tkW3HtSetImg { source {ismap "0"}} {
	global tkW3HtPage tkW3HtText
#	puts "tkW3HtSetImg: source->$source ismap->$ismap"

	set image_string "<IMAGE"
	set i $tkW3HtPage(image.index)

	set tkW3HtPage(image.ismap.$i) $ismap   
	if $tkW3HtText(in_anchor) {
		set anchor_num $tkW3HtPage(anchor.index)
		set tkW3HtPage(image.anchor.$i) $tkW3HtPage(anchor.href.$anchor_num)
		append image_string "-ANCHOR"
		if {$ismap} {
			append image_string "-ISMAP"
		}
	} {
		set tkW3HtPage(image.anchor.$i) ""
	}
  
	append image_string ">"

	tkW3HtAdd $image_string "Image i$i"
#	if {$i > $tkW3HtText(image.max)} {
		.preview.vp.txt tag configure i$i -foreground blue \
			-background $tkW3HtText(background)
		incr tkW3HtText(image.max)
#	}
	set tkW3HtPage(image.$i) $source
	incr tkW3HtPage(image.index)
}


# Procedure: tkW3HtSetInd
proc tkW3HtSetInd { href} {
	global tkW3HtPage
#	puts "tkW3HtSetInd: href->$href"
	set tkW3HtPage(is_index) 1
	set tkW3HtPage(index.href) $href
}


# Procedure: tkW3HtSetLink
proc tkW3HtSetLink { relation href} {
	global tkW3HtPage
#	puts "tkW3HtSetLink relation->$relation href->$href"
	lappend tkW3HtPage(link) $relation
	set tkW3HtPage(link.$relation) $href
}


# Procedure: tkW3HtSetName
proc tkW3HtSetName { address title} {
	global tkW3HtPage
#	puts "tkW3HtSetName: address->$address title->$title">
	set tkW3HtPage(address) $address
	set tkW3HtPage(title) $title
	tkW3OutputSetAddress $address $title
}


# Procedure: tkW3HtSetNextId
proc tkW3HtSetNextId { id} {
	global tkW3HtPage
#	puts "tkW3HtSetNextId: id->$id"
	set tkW3HtPage(next_id) $id
}


# Procedure: tkW3OutputButtonSetSensitive
proc tkW3OutputButtonSetSensitive { w var} {
#	puts "tkW3OutputButtonSetSensitive: w->$w var->$var"
	if {$var == 0} {
		$w configure -state disabled
	} {
		$w configure -state normal
	}
}


# Procedure: tkW3OutputClearBody
proc tkW3OutputClearBody {} {
#	puts "tkW3OutputClearBody"
	.preview.vp.txt delete 1.0 end
}


# Procedure: tkW3OutputCursorNormal
proc tkW3OutputCursorNormal {} {
	.preview.vp.txt configure -cursor xterm
}


# Procedure: tkW3OutputCursorWait
proc tkW3OutputCursorWait {} {
	.preview.vp.txt configure -cursor watch
}


# Procedure: tkW3OutputDoNothing
proc tkW3OutputDoNothing {} {

}


# Procedure: tkW3OutputEntryPrint
proc tkW3OutputEntryPrint { w message} {
#	puts "tkW3OutputEntryPrint: w->$w message->$message"
	$w configure -state normal
	$w delete 0 end
	$w insert 0 $message
	$w configure -state disabled
}


# Procedure: tkW3OutputError
proc tkW3OutputError { message} {
	#DLG:msg . .error_dialog $message error "OK"
	#puts stdout "Some error: $message"
}


# Procedure: tkW3OutputGetScrollPosition
proc tkW3OutputGetScrollPosition {} {
#	puts "tkW3OutputGetScrollPosition"
	lindex [.preview.vp.sc get] 2
}


# Procedure: tkW3OutputMenuSetSensitive
proc tkW3OutputMenuSetSensitive { w entry var} {
	if {$var == 0} {
		$w entryconfigure $entry -state disabled
	} {
		$w entryconfigure $entry -state normal
	}
}


# Procedure: tkW3OutputSetAddress
proc tkW3OutputSetAddress { address title} {
#	tkW3OutputEntryPrint .titles.address_entry $address
#	tkW3OutputEntryPrint .titles.title_entry $title
	.preview.t.t configure -text "$title"

#puts stdout "uhhh: $address $title"
}


# Procedure: tkW3OutputSetMessage
proc tkW3OutputSetMessage { message {duration ""}} {
#	tkW3OutputEntryPrint .message $message
#	if {$duration != ""} {
#	after $duration {tkW3OutputSetMessage {}}
#	}
#	puts stdout "message: $message"
}


# Procedure: tkW3OutputSetScrollPosition
proc tkW3OutputSetScrollPosition { value} {
	.preview.vp.txt  yview $value
}


# Procedure: tkW3OutputToggleSet
proc tkW3OutputToggleSet { w var} {
	if {$var == 0} {
		$w deselect
	} {
		$w select
	}
}




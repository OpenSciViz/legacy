#############################################
# file manipulation

#############################################
# this sets and eventually clears the eetn window

proc ClearEvent {message} {
	global event
	set event $message
	after 2500 {set event ""}
}

#############################################
# When a user wants a new file, verify that they are sure

proc NewFileDlg {} {
	global filename event

	ClearEvent "Calling fileselector"

	toplevel .new
	wm title .new "New document"

	set x [expr 230 + [winfo x .]]
	set y [expr 100 + [winfo y .]]
	wm geometry .new +$x+$y


	frame .new.top -relief raised -border 1
	message .new.top.msg -relief sunken -bd 1 -anchor n -padx 20 \
		-width 300 -text "Save current?"
	tixDlgBtns .new.btns
	.new.btns add yes -text "Yes, Save" -width 10
	.new.btns add no -text "No Save" -width 10
	.new.btns add cancel -text "Cancel" -width 10
	
	pack .new.top.msg -expand yes -fill both -padx 10 -pady 10
	pack .new.top -expand yes -fill both 
	pack .new.btns -fill x

	.new.btns button yes config -command {
		if {[string length $filename] == 0 } {
			SaveFileDlg SaveCmd4
		} else {
			SaveFile
			.textframe.vp.text delete "1.0" "end"
			catch {HtUncache "file://localhost/$filename"}
			set filename ""
			if {$newheader == 1} {
				Header
			}
			destroy .new
		}
	}
	.new.btns button no config -command {
		destroy .new
		.textframe.vp.text delete "1.0" "end"
		catch {HtUncache "file://localhost/$filename"}
		set filename ""
		if {$newheader == 1} {
			Header
		}
	}
	.new.btns button cancel config -command "destroy .new"	
}

#############################################
# Save as dialog

proc SaveFileAs {} {
	global filename
	global event

	ClearEvent "Calling fileselector"

	if {![winfo exists .save]} {
		tixMkFileDialog .save SaveCmd
		set x [expr 230 + [winfo x .]]
		set y [expr 60 + [winfo y .]]
		wm geometry .save +$x+$y
		wm title .save "Save As..."
	} else {
		wm deiconify .save
	}
}

#############################################
# File | Save option

proc SaveFileDlg {savecommand} {
	global filename

	ClearEvent "Calling fileselector"

	if {[string length $filename] == 0} {
		if {![winfo exists .save]} {
			tixMkFileDialog .save $savecommand
			set x [expr 230 + [winfo x .]]
			set y [expr 60 + [winfo y .]]
			wm geometry .save +$x+$y
			wm title .save "Save file"
		} else { 
			wm deiconify .save
		}
	} else {
		SaveFile
	}
}

#############################################
# Save file

proc SaveCmd {filenametmp} {
	global filename

	if {[string length $filenametmp] != 0} {
		set filename $filenametmp
		SaveFile
	}
}

#############################################
# save file and exit

proc SaveCmd2 {filenametmp} {
	global filename

	if {[string length $filenametmp] != 0} {
		set filename $filenametmp
		SaveFile
		if {[file exists $tempfile] == 1} {
			catch {exec rm $tempfile}
		}
		destroy .
	}
}

#############################################
# save file and open new one

proc SaveCmd4 {filenametmp} {
	global filename
	if {[string length $filenametmp] != 0} {
		set filename $filenametmp
		SaveFile
		destroy .new
		.textframe.vp.text delete "1.0" "end"
		catch {HtUncache "file://localhost/$filename"}
		set filename ""
	}
}

#############################################
# write the file, called by all SaveCmd's

proc SaveFile {} {
	global filename

	ClearEvent "Saving file..."

	set f [open $filename w]
	set lines [.textframe.vp.text get "1.0" "end"]
	puts $f $lines
	close $f

	ClearEvent "File saved."
	
}

#############################################
# about tkHTML

proc AboutDlg {} {
	global version
	global tkhtmllibdir

	ClearEvent "About..."

	toplevel .about	
	wm title .about "About tkHTML $version"

	set x [expr 230 + [winfo x .]]
	set y [expr 100 + [winfo y .]]

	wm geometry .about +$x+$y
	wm title .about "About tkHTML-$version"

	frame .about.top -relief raised -bd 1
	# a couple fonts, for fun

	label .about.top.msgl \
		-bitmap @$tkhtmllibdir/icon
	text .about.top.msg -relief groove -bd 2 -width 28 -height 8 \
		-background lightgrey -wrap word

	ConfigFonts .about.top.msg

	insertWithTags .about.top.msg "\n tkHTML $version\n" 180H 
	insertWithTags .about.top.msg " (C)1994 Liem Bahneman\n" normal
	insertWithTags .about.top.msg " roland@cac.washington.edu\n" normali
	insertWithTags .about.top.msg " If you have any questions, gripes or\n comments, please send them to me!\n" normal
	tixDlgBtns .about.btns
	.about.btns add close -text "Close" -width 10
	pack .about.top.msgl -expand yes -fill both -side left \
		-padx 10 -pady 10
	pack .about.top.msg -expand yes -fill both -padx 10 -pady 10 \
		-side right
	pack .about.top -expand yes -fill both
	pack .about.btns -fill x
	.about.top.msg configure -state disabled

	.about.btns button close config -command "destroy .about"
}

#############################################
# open an existing file

proc OpenFileDlg {} {
	global filename

	ClearEvent "Calling fileselector"

	if {![winfo exists .open]} {
		tixMkFileDialog .open OpenCmd
		set x [expr 230 + [winfo x .]]
		set y [expr 60 + [winfo x .]]
		wm geometry .open +$x+$y
		wm title .open "Open file"
	} else {
		wm deiconify .open
	}

}	

#############################################
# open the file 

proc OpenCmd {filenametmp} {
	global filename

	if {[string length $filenametmp] != 0} {
		OpenFile $filenametmp
	}
}

#############################################
# open the file from file

proc OpenFile {newfile} {
	global filename

	ClearEvent "Opening file..."

	#catch {HtUncache "file://localhost/$filename"}
	
	set f [open $newfile r]
	set lines [read $f]

	.textframe.vp.text delete "1.0" "end"
	.textframe.vp.text insert "1.0" $lines

	set filename $newfile
	close $f
	
	ClearEvent "File opened."
	.textframe.vp.text mark set insert 1.0
}

#############################################
# if there was a file on the command line, open it

proc InitFile {argv} {
	global filename newheader

	set newfile1 [lindex $argv 0]

	# check if absolute or relative path
	if {[string index $$newfile1 1] == "/"} {
		set newfile2 $newfile1
	} else {
		set pwd [pwd]
		set newfile2 "$pwd/$newfile1"
	}

	if {[file exists $newfile2] == 1} {
		OpenFile $newfile2
	} else {
		set filename $newfile2
		if {$newheader == 1} {
			Header
			return
		}
	}
	.textframe.vp.text mark set insert 0.0
}

#############################################
# quit, verify save or cancel

proc QuitDlg {} {
	global filename tempfile

	toplevel .quit
	wm title .quit "Quit tkHTML"

	set x [expr 230 + [winfo x .]]
	set y [expr 100 + [winfo y .]]
	wm geometry .quit +$x+$y


	frame .quit.top -relief raised -border 1
	message .quit.top.msg -relief sunken -bd 1 -anchor n -padx 20 \
		-width 300 -text "Save current?" \
		-font "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"
	tixDlgBtns .quit.btns
	.quit.btns add yes -text "Yes, Save" -width 10
	.quit.btns add no -text "No Save" -width 10
	.quit.btns add cancel -text "Cancel" -width 10
	
	pack .quit.top.msg -expand yes -fill both -padx 10 -pady 10
	pack .quit.top -expand yes -fill both 
	pack .quit.btns -fill x

	.quit.btns button yes config -command {
		if {[string length $filename] == 0 } {
			SaveFileDlg SaveCmd2
		} else {
			SaveFile
			exit
		}
	}
	.quit.btns button no config -command {
		destroy .
		if {[file exists $tempfile] == 1} {
			catch {exec rm $tempfile}
		}
	}
	.quit.btns button cancel config -command "destroy .quit"	
}

#############################################
# save file for previewing

proc SaveForPreview {} {
	global tempfile

	ClearEvent "Saving temp file..."

	set f [open $tempfile w]
	set lines [.textframe.vp.text get "1.0" "end"]
	puts $f $lines
	close $f

	ClearEvent "File saved."

	if {[winfo exists .preview] == 0} {
		Preview "file://localhost/$tempfile"
	} else {
		DoPreview "file://localhost/$tempfile"
	}
}



proc Header {} {
	.textframe.vp.text insert insert \
"<HEAD>
<TITLE></TITLE>
</HEAD>
<BODY>"

	#set the cursor in the middle of the <TITLE> tags
	.textframe.vp.text mark set insert 2.7
}






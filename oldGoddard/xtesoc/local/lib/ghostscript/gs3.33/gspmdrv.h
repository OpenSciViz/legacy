/* Copyright (C) 1992, 1993, 1994 Aladdin Enterprises.  All rights reserved.
  
  This file is part of GNU Ghostscript.
  
  GNU Ghostscript is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility to
  anyone for the consequences of using it or for whether it serves any
  particular purpose or works at all, unless he says so in writing.  Refer
  to the GNU Ghostscript General Public License for full details.
  
*/

/* gspmdrv.h */
/* Definitions common to gspmdrv.c and gspmdrv.rc */
#define GSPMDRV_VERSION "1994-02-09"

#define IDM_ABOUT 5
#define IDM_COPY  6

#define IDD_ABOUT IDM_ABOUT

#define ID_GSPMDRV 1000

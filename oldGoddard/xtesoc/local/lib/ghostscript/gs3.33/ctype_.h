/* Copyright (C) 1993 Aladdin Enterprises.  All rights reserved.
  
  This file is part of GNU Ghostscript.
  
  GNU Ghostscript is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility to
  anyone for the consequences of using it or for whether it serves any
  particular purpose or works at all, unless he says so in writing.  Refer
  to the GNU Ghostscript General Public License for full details.
  
*/

/* ctype_.h */
/* Wrapper for ctype.h */

/* We must include std.h before any file that includes sys/types.h. */
#include "std.h"

/* ... and that's the only reason for having this file at all. */
#include <ctype.h>

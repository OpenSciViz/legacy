/* Copyright (C) 1993, 1995 Aladdin Enterprises.  All rights reserved.
  
  This file is part of GNU Ghostscript.
  
  GNU Ghostscript is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility to
  anyone for the consequences of using it or for whether it serves any
  particular purpose or works at all, unless he says so in writing.  Refer
  to the GNU Ghostscript General Public License for full details.
  
*/

/* gsdparam.c */
/* Default device parameters for Ghostscript library */
#include "memory_.h"			/* for memcpy */
#include "gx.h"
#include "gserrors.h"
#include "gsparam.h"
#include "gxdevice.h"
#include "gxfixed.h"

/* ================ Getting parameters ================ */

/* Forward references */
private bool param_HWColorMap(P2(gx_device *, byte *));

/* Get the device parameters. */
int
gs_getdeviceparams(gx_device *dev, gs_param_list *plist)
{	gx_device_set_procs(dev);
	fill_dev_proc(dev, get_params, gx_default_get_params);
	fill_dev_proc(dev, get_page_device, gx_default_get_page_device);
	return (*dev_proc(dev, get_params))(dev, plist);
}

/* Standard ProcessColorModel values. */
static const char *pcmsa[] = {
  "", "DeviceGray", "", "DeviceRGB", "DeviceCMYK"
};

/* Get standard parameters. */
int
gx_default_get_params(gx_device *dev, gs_param_list *plist)
{
	int code;

	/* Standard page device parameters: */

	gs_param_string dns, pcms;
	gs_param_float_array psa, ibba, hwra, ma;
#define set_param_array(a, d, s)\
  (a.data = d, a.size = s, a.persistent = false);

	/* Non-standard parameters: */

	int colors = dev->color_info.num_components;
	int depth = dev->color_info.depth;
	int GrayValues = dev->color_info.max_gray + 1;
	bool IsPageDevice = (*dev_proc(dev, get_page_device))(dev) != 0;
	int HWSize[2];
	  gs_param_int_array hwsa;
	gs_param_float_array hwma;

	/* Fill in page device parameters. */

	param_string_from_string(dns, dev->dname);
	{	const char *cms = pcmsa[colors];
		/* We might have an uninitialized device with */
		/* color_info.num_components = 0.... */
		if ( *cms != 0 )
		  param_string_from_string(pcms, cms);
		else
		  pcms.data = 0;
	}
	set_param_array(hwra, dev->HWResolution, 2);
	set_param_array(psa, dev->PageSize, 2);
	set_param_array(ibba, dev->ImagingBBox, 4);
	set_param_array(ma, dev->Margins, 2);

	/* Fill in non-standard parameters. */

	HWSize[0] = dev->width;
	HWSize[1] = dev->height;
	set_param_array(hwsa, HWSize, 2);
	set_param_array(hwma, dev->HWMargins, 4);

	/* Transmit the values. */

	if (
			/* Standard parameters */

	     (code = param_write_name(plist, "OutputDevice", &dns)) < 0 ||
	     (code = (pcms.data == 0 ? 0 :
		      param_write_name(plist, "ProcessColorModel", &pcms))) < 0 ||
	     (code = param_write_float_array(plist, "HWResolution", &hwra)) < 0 ||
	     (code = param_write_float_array(plist, "PageSize", &psa)) < 0 ||
	     (code = (dev->ImagingBBox_set ?
		      param_write_float_array(plist, "ImagingBBox", &ibba) :
		      param_write_null(plist, "ImagingBBox"))) < 0 ||
	     (code = param_write_float_array(plist, "Margins", &ma)) < 0 ||
	     (code = (dev->Orientation_set > 0 ?
		      param_write_int(plist, "Orientation", &dev->Orientation) :
		      dev->Orientation_set == 0 ?
		      param_write_null(plist, "Orientation") :
		      0)) < 0 ||
	     (code = (dev->Duplex_set > 0 ?
		      param_write_bool(plist, "Duplex", &dev->Duplex) :
		      dev->Duplex_set == 0 ?
		      param_write_null(plist, "Duplex") :
		      0)) < 0 ||

			/* Non-standard parameters */

	     (code = param_write_int_array(plist, "HWSize", &hwsa)) < 0 ||
	     (code = param_write_float_array(plist, ".HWMargins", &hwma)) < 0 ||
	     (code = param_write_string(plist, "Name", &dns)) < 0 ||
	     (code = param_write_int(plist, "Colors", &colors)) < 0 ||
	     (code = param_write_int(plist, "BitsPerPixel", &depth)) < 0 ||
	     (code = param_write_int(plist, "GrayValues", &GrayValues)) < 0 ||
	     (code = param_write_long(plist, "PageCount", &dev->PageCount)) < 0 ||
	     (code = param_write_bool(plist, ".IsPageDevice", &IsPageDevice)) < 0

	   )
		return code;

	/* Fill in color information. */

	if ( colors > 1 )
	{	int RGBValues = dev->color_info.max_color + 1;
		long ColorValues = 1L << depth;
		if ( (code = param_write_int(plist, "RedValues", &RGBValues)) < 0 ||
		     (code = param_write_int(plist, "GreenValues", &RGBValues)) < 0 ||
		     (code = param_write_int(plist, "BlueValues", &RGBValues)) < 0 ||
		     (code = param_write_long(plist, "ColorValues", &ColorValues)) < 0
		   )
			return code;
	}

	if ( param_requested(plist, "HWColorMap") )
	{	byte palette[3 << 8];
		if ( param_HWColorMap(dev, palette) )
		  {	gs_param_string hwcms;
			hwcms.data = palette, hwcms.size = colors << depth,
			  hwcms.persistent = false;
			if ( (code = param_write_string(plist, "HWColorMap", &hwcms)) < 0 )
			  return code;
		  }
	}

	return 0;
}

/* Get the color map for a device.  Return true if there is one. */
private bool
param_HWColorMap(gx_device *dev, byte *palette /* 3 << 8 */)
{	int depth = dev->color_info.depth;
	int colors = dev->color_info.num_components;
	
	if ( depth <= 8 && colors <= 3 )
	  {	byte *p = palette;
		gx_color_value rgb[3];
		gx_color_index i;
		fill_dev_proc(dev, map_color_rgb, gx_default_map_color_rgb);
		for ( i = 0; (i >> depth) == 0; i++ )
		  {	int j;
			(*dev_proc(dev, map_color_rgb))(dev, i, rgb);
			for ( j = 0; j < colors; j++ )
			  *p++ = gx_color_value_to_byte(rgb[j]);
		  }
		return true;
	  }
	return false;
}

/* ================ Putting parameters ================ */

/* Forward references */
private int param_check_bool(P4(gs_param_list *, gs_param_name, bool, bool));
private int param_check_long(P4(gs_param_list *, gs_param_name, long, bool));
#define param_check_int(plist, pname, ival, defined)\
  param_check_long(plist, pname, (long)(ival), defined)
private int param_check_bytes(P5(gs_param_list *, gs_param_name, const byte *, uint, bool));
#define param_check_string(plist, pname, str, defined)\
  param_check_bytes(plist, pname, (const byte *)str, strlen(str), defined)

/* Set the device parameters. */
/* If the device was open and the put_params procedure closed it, */
/* return 1; otherwise, return 0 or an error code as usual. */
int
gs_putdeviceparams(gx_device *dev, gs_param_list *plist)
{	bool was_open = dev->is_open;
	int code;
	fill_dev_proc(dev, put_params, gx_default_put_params);
	code = (*dev_proc(dev, put_params))(dev, plist);
	return (code < 0 ? code : was_open && !dev->is_open ? 1 : code);
}

/* Set standard parameters. */
/* Note that setting the size, resolution, or (if allowed) orientation */
/*  closes the device. */
/* Window devices that don't want this to happen must temporarily */
/* set is_open to false before calling gx_default_put_params, */
/* and then taking appropriate action afterwards. */
int
gx_default_put_params(gx_device *dev, gs_param_list *plist)
{	int ecode = 0;
	int code;
	const char _ds *param_name;
	gs_param_float_array hwra;
	gs_param_int_array hwsa;
	gs_param_float_array psa;
	gs_param_float_array ma;
	gs_param_float_array hwma;
	gs_param_float_array ibba;
	bool ibbnull = false;
	int orientation;
	  int orientation_set = -1;
	bool duplex;
	  int duplex_set = -1;
	int colors = dev->color_info.num_components;
	int depth = dev->color_info.depth;
	int GrayValues = dev->color_info.max_gray + 1;
	int RGBValues = dev->color_info.max_color + 1;
	long ColorValues = 1L << depth;
	bool IsPageDevice = (*dev_proc(dev, get_page_device))(dev) != 0;
	gs_param_string cms;

#define BEGIN_ARRAY_PARAM(pread, pname, pa, psize, e)\
  switch ( code = pread(plist, (param_name = pname), &pa) )\
  {\
  case 0:\
	if ( pa.size != psize )\
	  ecode = gs_error_rangecheck;\
	else { 
/* The body of the processing code goes here. */
/* If it succeeds, it should do a 'break'; */
/* if it fails, it should set ecode and fall through. */
#define END_PARAM(pa, e)\
	}\
	goto e;\
  default:\
	ecode = code;\
e:	param_signal_error(plist, param_name, ecode);\
  case 1:\
	pa.data = 0;		/* mark as not filled */\
  }

	/*
	 * The HWResolution, HWSize, and PageSize parameters interact in
	 * the following way:
	 *	1. Setting HWResolution recomputes HWSize from PageSize.
	 *	2. Setting HWSize recomputes PageSize from HWResolution.
	 *	3. Setting PageSize recomputes HWSize from HWResolution.
	 * If more than one parameter is being set, we apply these rules
	 * in the order 1, 2, 3.  This does the right thing in the most
	 * common case of setting more than one parameter, namely,
	 * setting both HWResolution and HWSize.
	 */

	BEGIN_ARRAY_PARAM(param_read_float_array, "HWResolution", hwra, 2, hwre)
		if ( hwra.data[0] <= 0 || hwra.data[1] <= 0 )
		  ecode = gs_error_rangecheck;
		else
		  break;
	END_PARAM(hwra, hwre)

	BEGIN_ARRAY_PARAM(param_read_int_array, "HWSize", hwsa, 2, hwsa)
		if ( hwsa.data[0] <= 0 || hwsa.data[1] <= 0 )
		  ecode = gs_error_rangecheck;
#define max_coord (max_fixed / fixed_1)
#if max_coord < max_int
		else if ( hwsa.data[0] > max_coord || hwsa.data[1] > max_coord )
		  ecode = gs_error_limitcheck;
#endif
#undef max_coord
		else
		  break;
	END_PARAM(hwsa, hwse)

	BEGIN_ARRAY_PARAM(param_read_float_array, "PageSize", psa, 2, pse)
		const float *res =
		  (hwra.data == 0 ? dev->HWResolution : hwra.data);
		float width_new, height_new;
		if ( (width_new = psa.data[0] * res[0] / 72) <= 0 ||
		     (height_new = psa.data[1] * res[1] / 72) <= 0
		   )
		  ecode = gs_error_rangecheck;
#define max_coord (max_fixed / fixed_1)
#if max_coord < max_int
		else if ( width_new > max_coord || height_new > max_coord )
		  ecode = gs_error_limitcheck;
#endif
#undef max_coord
		else
		  break;
	END_PARAM(psa, pse)

	BEGIN_ARRAY_PARAM(param_read_float_array, "Margins", ma, 2, me)
		break;
	END_PARAM(ma, me)

	BEGIN_ARRAY_PARAM(param_read_float_array, ".HWMargins", hwma, 4, hwme)
		break;
	END_PARAM(hwma, hwme)

	switch ( code = param_read_float_array(plist, (param_name = "ImagingBBox"), &ibba) )
	{
	case 0:
		if ( ibba.size != 4 || ibba.data[0] < 0 || ibba.data[1] < 0 ||
		     ibba.data[2] < ibba.data[0] || ibba.data[3] < ibba.data[1]
		   )
		  ecode = gs_error_rangecheck;
		else
		  break;
		goto ibbe;
	default:
		if ( (code = param_read_null(plist, param_name)) == 0 )
		{	ibbnull = true;
			ibba.data = 0;
			break;
		}
		ecode = code;	/* can't be 1 */
ibbe:		param_signal_error(plist, param_name, ecode);
	case 1:
		ibba.data = 0;
		break;
	}

	if ( dev->Orientation_set >= 0 ) /* i.e., Orientation is supported */
	  switch ( code = param_read_int(plist, (param_name = "Orientation"),
					 &orientation) )
	    {
	    case 0:
		if ( orientation >= 0 && orientation <= 3 )
		  {	orientation_set = 1;
			break;
		  }
		ecode = gs_error_rangecheck;
		goto oe;
	    default:
		if ( (code = param_read_null(plist, param_name)) == 0 )
		{	orientation_set = 0;
			orientation = dev->Orientation;	/* preserve */
			break;
		}
		ecode = code;
oe:		param_signal_error(plist, param_name, ecode);
	    case 1:
		;
	    }

	if ( dev->Duplex_set >= 0 )	/* i.e., Duplex is supported */
	  switch ( code = param_read_bool(plist, (param_name = "Duplex"),
					  &duplex) )
	    {
	    case 0:
		duplex_set = 1;
		break;
	    default:
		if ( (code = param_read_null(plist, param_name)) == 0 )
		{	duplex_set = 0;
			break;
		}
		ecode = code;
		param_signal_error(plist, param_name, ecode);
	    case 1:
		;
	    }

	/* Now check nominally read-only parameters. */
	if ( (code = param_check_string(plist, "OutputDevice", dev->dname, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_string(plist, "ProcessColorModel", pcmsa[colors], colors != 0)) < 0 )
	  ecode = code;
	if ( (code = param_check_string(plist, "Name", dev->dname, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "Colors", colors, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "BitsPerPixel", depth, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "GrayValues", GrayValues, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_long(plist, "PageCount", dev->PageCount, true)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "RedValues", RGBValues, colors > 1)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "GreenValues", RGBValues, colors > 1)) < 0 )
	  ecode = code;
	if ( (code = param_check_int(plist, "BlueValues", RGBValues, colors > 1)) < 0 )
	  ecode = code;
	if ( (code = param_check_long(plist, "ColorValues", ColorValues, colors > 1)) < 0 )
	  ecode = code;
	if ( (code = param_check_bool(plist, ".IsPageDevice", IsPageDevice, true)) < 0 )
	  ecode = code;
	if ( param_read_string(plist, "HWColorMap", &cms) != 1 )
	  {	byte palette[3 << 8];
		if ( param_HWColorMap(dev, palette) )
		  code = param_check_bytes(plist, "HWColorMap", palette,
					   colors << depth, true);
		else
		  code = param_check_bytes(plist, "HWColorMap", 0, 0, false);
		if ( code < 0 )
		  ecode = code;
	  }

	if ( ecode < 0 )
	  return ecode;
	code = param_commit(plist);
	if ( code < 0 )
	  return code;

	/* Now actually make the changes. */
	/* Changing resolution or page size requires closing the device, */
	/* but changing margins or ImagingBBox does not. */
	/* In order not to close and reopen the device unnecessarily, */
	/* we check for replacing the values with the same ones. */

	if ( hwra.data != 0 &&
	      (dev->HWResolution[0] != hwra.data[0] ||
	       dev->HWResolution[1] != hwra.data[1])
	   )
	  {	if ( dev->is_open )
		  gs_closedevice(dev);
		gx_device_set_resolution(dev, hwra.data[0], hwra.data[1]);
	  }
	if ( hwsa.data != 0 &&
	      (dev->width != hwsa.data[0] ||
	       dev->height != hwsa.data[1])
	   )
	  {	if ( dev->is_open )
		  gs_closedevice(dev);
		gx_device_set_width_height(dev, hwsa.data[0], hwsa.data[1]);
	  }
	if ( psa.data != 0 &&
	      (dev->PageSize[0] != psa.data[0] ||
	       dev->PageSize[1] != psa.data[1])
	   )
	  {	if ( dev->is_open )
		  gs_closedevice(dev);
		gx_device_set_page_size(dev, psa.data[0], psa.data[1]);
	  }
	if ( ma.data != 0 )
	  {	dev->Margins[0] = ma.data[0];
		dev->Margins[1] = ma.data[1];
	  }
	if ( hwma.data != 0 )
	  {	dev->HWMargins[0] = hwma.data[0];
		dev->HWMargins[1] = hwma.data[1];
		dev->HWMargins[2] = hwma.data[2];
		dev->HWMargins[3] = hwma.data[3];
	  }
	if ( ibba.data != 0 )
	  {	dev->ImagingBBox[0] = ibba.data[0];
		dev->ImagingBBox[1] = ibba.data[1];
		dev->ImagingBBox[2] = ibba.data[2];
		dev->ImagingBBox[3] = ibba.data[3];
		dev->ImagingBBox_set = true;
	  }
	else if ( ibbnull )
	  {	dev->ImagingBBox_set = false;
	  }
	if ( orientation_set >= 0 &&
	      dev->Orientation != orientation
	   )
	  {	dev->Orientation = orientation;
		dev->Orientation_set = orientation_set;
	  }
	if ( duplex_set >= 0 )
	  {	dev->Duplex = duplex;
		dev->Duplex_set = duplex_set;
	  }
	return 0;
}

/* Check that a nominally read-only parameter is being set to */
/* its existing value. */
private int
param_check_bool(gs_param_list *plist, gs_param_name pname, bool value,
  bool defined)
{	int code;
	bool new_value;
	switch ( code = param_read_bool(plist, pname, &new_value) )
	  {
	  case 0:
		if ( defined && new_value == value )
		  break;
		code = gs_note_error(gs_error_rangecheck);
		goto e;
	  default:
		if ( param_read_null(plist, pname) == 0 )
		  return 1;
e:		param_signal_error(plist, pname, code);
	  case 1:
		;
	  }
	return code;
}
private int
param_check_long(gs_param_list *plist, gs_param_name pname, long value,
  bool defined)
{	int code;
	long new_value;
	switch ( code = param_read_long(plist, pname, &new_value) )
	  {
	  case 0:
		if ( defined && new_value == value )
		  break;
		code = gs_note_error(gs_error_rangecheck);
		goto e;
	  default:
		if ( param_read_null(plist, pname) == 0 )
		  return 1;
e:		param_signal_error(plist, pname, code);
	  case 1:
		;
	  }
	return code;
}
private int
param_check_bytes(gs_param_list *plist, gs_param_name pname, const byte *str,
  uint size, bool defined)
{	int code;
	gs_param_string new_value;
	switch ( code = param_read_string(plist, pname, &new_value) )
	  {
	  case 0:
		if ( defined && new_value.size == size &&
		     !memcmp((const char *)str, (const char *)new_value.data,
			     size)
		   )
		  break;
		code = gs_note_error(gs_error_rangecheck);
		goto e;
	  default:
		if ( param_read_null(plist, pname) == 0 )
		  return 1;
e:		param_signal_error(plist, pname, code);
	  case 1:
		;
	  }
	return code;
}

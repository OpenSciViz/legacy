   /   `  X  �  �����           ��������   *   *          Q   Q����      x   x��������   �   �         �   �����      �  ��������   �  *      	  %  s����   
  /  }��������  Z  �        �  �����     �  g��������  �  �        �  �����       ���������  #  �        M  ,����     |  Z��������  �  �        �  �����     �  ���������    H      #  "  �����     H  ���������  v          �  S����       ���������  6  �         W  �����     x  ��������  �        !  �  <����   "  �  V��������  �  �      )    �����   %  9  ��������  P  0   $   '  v  [����   (  �  ���������  �  �   &   ,  �  �����   +  	  ���������  +  %   *   -  Y  m����   .    ���������  �  �      G  �  �����   1  �  	?��������  #  	�   0   3  L  	�����   4  k  
��������  �  
T   2   8  �  
�����   7  �  
���������    
�   6   9  @  
�����   :  e  
���������  |     5   A  �  ����   =  �  ��������  E     <   ?  j  U����   @  �  ���������  �  �   >   D  �  !����   C  !  `��������  K  �   B   E  d  �����   F  �  	��������  �  J   ;   S  �  s����   I  �  ���������    �   H   K  I  �����   L  o  #��������  �  C   J   P  �  t����   O  �  ���������  �  �   N   Q  2  ����   R  O  7��������  a  M   M   Y  }  o����   U  �  ���������  �  �   T   W  �  �����   X  '  1��������  Q  o   V   \  y  p����   [  �  ��������  �  �   Z   ^  �  ���������  �  �   ]   _    ���������  W  �xrealloc(): couldn't reallocate %d bytes
 xmalloc(): couldn't allocate %d bytes
 xcalloc(): couldn't allocate %d bytes
 wrapped warning: didn't process following options from environment variable %s:
 unknown special escape: %s unknown encoding: %s unexpected EOF while scanning ^@epsf escape truncated too long file name for ^@epsf escape:
%.*s too long argument for %s escape:
%.*s syntax error in option string %s="%s":
missing end of quotation: %c reading AFM info for font "%s"
 processing file "%s"...
 printer passing through %s file "%s"
 non-printable character codes (decimal):
 must print at least one line per each page: %s missing character codes (decimal):
 malformed underlay position: %s malformed font spec: %s malformed font spec for ^@font escape: %s malformed float dimension: "%s" malformed ^@epsf escape: no '{' found malformed ^@epsf escape: no ']' after options malformed %s escape: no '{' found known media:
name             width	height	llx	lly	urx	ury
------------------------------------------------------------
 invalid value for ^@shade escape: %s invalid value "%s" for option %s illegal value "%s" for option %s illegal underlay style: %s illegal page label format "%s" illegal option: %s illegal option %c for ^@epsf escape illegal non-printable format "%s" epsf: couldn't open pipe to command "%s": %s
 downloading font "%s"
 do not know anything about media "%s" couldn't stat input file "%s": %s couldn't rewind divert file: %s couldn't open printer `%s': %s couldn't open input filter "%s" for file "%s": %s couldn't open input file "%s": %s couldn't open font description file "%s": %s
 couldn't open config file "%s/%s": %s couldn't open EPS file "%s": %s
 couldn't open AFM library: %s couldn't open AFM file for the default font: %s couldn't open AFM file for font "%s", using default
 couldn't get passwd entry for uid=%d: %s couldn't find prolog "%s": %s
 couldn't find header definition file "%s.hdr": %s
 couldn't find header definition file "%s.hdr" couldn't find encoding file "%s.enc": %s
 couldn't create output file "%s": %s couldn't create divert file name: %s couldn't create divert file "%s": %s [ %d pages * %d copy ] Usage: %s [OPTION]... [FILE]...
Mandatory arguments to long options are mandatory for short options too.
  -1                         same as --columns=1
  -2                         same as --columns=2
      --columns=NUM          specify the number of columns per page
  -a, --pages=PAGES          specify which pages are printed
  -b, --header=HEADER        set page header
  -B, --no-header            no page headers
  -c, --truncate-lines       cut long lines (default is to wrap)
  -C, --line-numbers         precede each line with its line number
  -d                         an alias for option --printer
  -D, --setpagedevice=KEY[:VALUE]
                             pass a page device definition to output
  -e, --escapes[=CHAR]       enable special escape interpretation
 Try `%s --help' for more information.
 Long-only options:
  --download-font=NAME       download font NAME
  --filter-stdin=NAME        specify how stdin is shown to the input filter
  --help                     print this help and exit
  --highlight-bar-gray=NUM   print highlight bars with gray NUM (0 - 1)
  --list-media               list names of all known media
  --list-options             list all options and their values
  --non-printable-format=FMT specify how non-printable chars are printed
  --page-label-format=FMT    set page label format to FMT
  --printer-options=OPTIONS  pass extra options to the printer command
  --ul-angle=ANGLE           set underlay text's angle to ANGLE
  --ul-font=NAME             print underlays with font NAME
  --ul-gray=NUM              print underlays with gray value NUM
  --ul-position=POS          set underlay's starting position to POS
  --ul-style=STYLE           print underlays with style STYLE
 EPS file "%s" is too large for page
 EPS file "%s" is not a valid EPS file
 EPS file "%s" does not start with "%%!" magic
 EPS file "%s" contains malformed %%%%BoundingBox row:
"%.*s"
 %s:%d: %%HeaderHeight: no argument %s:%d: %%Format: too long name, maxlen=%d %s:%d: %%Format: no name %s:%d: %%Format: name "%s" is already defined %s:%d: %%FooterHeight: no argument %s: unrecognized option `--%s'
 %s: unrecognized option `%c%s'
 %s: option requires an argument -- %c
 %s: option `--%s' doesn't allow an argument
 %s: option `%s' requires an argument
 %s: option `%s' is ambiguous
 %s: option `%c%s' doesn't allow an argument
 %s: invalid option -- %c
 %s: illegal option -- %c
 %s: illegal newline character specifier: '%s': expected 'n' or 'r'
 %d non-printable characters
 %d lines were %s
 %d characters were missing
 %%Format: unknown `%%' escape `%c' (%d) %%Format: unknown `$' escape `%c' (%d) %%Format: too long variable name for $() escape %%Format: too long format for %%D{} escape %%Format: too long format for $D{} escape %%Format: no closing ')' for $() escape  sent to %s
  left in %s
   option %d = "%s"
   -s, --baselineskip=NUM     set baselineskip to NUM
  -S, --statusdict=KEY[:VALUE]
                             pass a statusdict definition to the output
  -t, --title=TITLE          set banner page's job title to TITLE.  Option
                             sets also the name of the input file stdin.
  -T, --tabsize=NUM          set tabulator size to NUM
  -u, --underlay[=TEXT]      print TEXT under every page
  -v, --verbose              tell what we are doing
  -V, --version              print version number
  -X, --encoding=NAME        use input encoding NAME
  -z, --no-formfeed          do not interpret form feed characters
  -Z, --pass-through         pass through PostScript and PCL files
                             without any modifications
   -f, --font=NAME            use font NAME for body text
  -F, --header-font=NAME     use font NAME for header texts
  -g, --print-anyway         nothing (compatibility option)
  -G                         same as --fancy-header
      --fancy-header[=NAME]  select fancy page header
  -h, --no-job-header        suppress the job header page
  -H, --highlight-bars=NUM   specify how high highlight bars are
  -i, --indent=NUM           set line indent to NUM characters
  -I, --filter=CMD           read input files through input filter CMD
  -j, --borders              print borders around columns
  -k, --page-prefeed         enable page prefeed
  -K, --no-page-prefeed      disable page prefeed
  -l, --lineprinter          simulate lineprinter, this is an alias for:
                               --lines-per-page=66, --no-header, --portrait,
                               --columns=1
   -L, --lines-per-page=NUM   specify how many lines are printed on each page
  -m, --mail                 send mail upon completion
  -M, --media=NAME           use output media NAME
  -n, --copies=NUM           print NUM copies of each page
  -N, --newline=NL           select the newline character.  Possible
                             values for NL are: n (`\n') and r (`\r').
  -o                         an alias for option --output
  -O, --missing-characters   list missing characters
  -p, --output=FILE          leave output to file FILE.  If FILE is `-',
                             leave output to stdout.
  -P, --printer=NAME         print output to printer NAME
  -q, --quiet, --silent      be really quiet
  -r, --landscape            print in landscape mode
  -R, --portrait             print in portrait mode
  xrealloc(): ne peut r�allouer %d octets.
 xmalloc(): ne peut allouer %d octets.
 xcalloc(): ne peut allouer %d octets.
 boucl�es AVERTISSEMENT: ne peut traiter les options suivantes � partir
des variables d'environnement %s:
 Le caract�re sp�cial d'�chappement est inconnu: %s Encodage inconnu: %s Fin de fichier (EOF) inattendue lors de l'examen des �chappements ^@epsf tronqu�es Le nom de fichier est trop long pour un �chappement ^@epsf:
%.*s Le nom de fichier est trop long pour un �chappement ^@epsf:
%.*s Erreur de syntaxe rencontr�e dans l'option %s="%s":
L'apostrophe terminant la cha�ne  %c est manquante. Lecture du fichier d'informations AFM pour le jeu de caract�res  "%s"
 Traitement du fichier "%s"...
 l'imprimante Traitement du fichier %s "%s".
 codes de caract�re non imprimables (en d�cimal):
 Doit imprimer au moins une ligne par page: %s codes de caract�re manquants (en d�cimal):
 La position sous-jacente de %s est mal compos�e. La sp�cification du jeu de caract�res est mal compos�e: %s La sp�cification de jeu de caract�res est mal compos�e pour un �chappement ^@font: %s La d�finition de dimension en point flottant est mal compos�e: "%s" L'�chappement ^@epsf est mal compos�: '}' n'a pas �t� retrouv�. L'�chappement ^@epsf mal compos�e: ']' non retrouv� apr�s les options. L'�chappement ^@epsf est mal compos�: '}' n'a pas �t� retrouv�. Liste des m�dias connus:
nom              largeur	hauteur	llx	lly	urx	ury
------------------------------------------------------------
   La valeur  "%s" est ill�gale pour l'option %s  Le format d'�tiquette de page "%s" est ill�gal. L'option est ill�gale: %s L'option %c est ill�gale pour un �chappement ^@epsf. Le format de la cha�ne non imprimable "%s" est ill�gal. epsf: ne peut ouvrir un "pipe" pour ex�cuter la commande "%s": %s
 T�l�chargement du jeu de caract�res "%s"
 Ne peut reconna�tre ce type de m�dium "%s" Ne peut �valuer � l'aide de stat() le fichier fourni � l'entr�e "%s": %s  Ne peut acc�der � l'imprimante `%s': %s Ne peut ouvrir le fichier d'entr�e "%s": %s Ne peut ouvrir le fichier d'entr�e "%s": %s Ne peut ouvrir le fichier de description du jeu de caract�res "%s": %s
 Ne peut ouvrir le fichier de configuration "%s/%s": %s Ne peut ouvrir le fichier EPS "%s": %s
 Ne peut ouvrir la librairie "AFM": %s Ne peut ouvrir le fichier AFM contenant les jeu de caract�res par d�faut: %s Ne peut ouvrir le fichier AFM pour le jeu de caract�res "%s", utilisera la jeu par d�faut
 Ne peut retrouver une entr�e dans le fichier 'passwd'
correspondant au Uid=%d: %s Ne peut trouver le pr�ambule "%s": %s
 Ne peut trouver le fichier de d�finition d'en-t�te "%s.hdr": %s
 Ne peut le fichier de d�finition de l'en-t�te "%s.hdr" Ne peut trouver le fichier d'encodage "%s.enc": %s
 Ne peut cr�er un fichier de sortie "%s": %s   [ nombre de pages %d * nombre de copies %d ]    Le fichier EPS "%s" est trop volumineux pour une page.
 Le fichier EPS "%s" est un fichier EPS invalide.
 Le fichier EPS "%s" ne d�bute pas par "%%!" magic
 Le fichier EPS "%s" contient une d�finition d'encadrement mal compos�e
de %%%%BoundingBox row:
"%.*s"
 %s:%d: %%Erreur sur la hauteur de l'en-t�te: pas de param�tre. %s:%d: %%Erreur de format: nom trop long d�passant la longueur maximale=%d %s:%d: %%Erreur de format: pas de nom %s:%d: %%Erreur de format: le nom "%s" est d�j� d�fini. %s:%d: %%Erreur sur la hauteur du bas de page: pas de param�tre. %s: l'option `--%s' n'est pas reconnue.
 %s: l'option `%c%s' n'est pas reconnue.
 %s: l'option --%c requiert un param�tre.
 %s: l'option `--%s' ne permet pas de param�tre.
 %s: l'option `--%s' requiert un param�tre.
 %s: l'option `%s' est ambigu�.
 %s: l'option `%c%s' ne permet pas de param�tre.
 %s: l'option -- %c est invalide.
 %s: l'option -- %c est ill�gale.
 %s: la sp�cification d'un caract�re de saut de ligne est ill�gale:
 '%s': attendait 'n' ou 'r'
 %d caract�res non-imprimables
 %d lignes �taient %s
 %d caract�res �taient manquants.
 %%Erreur de format: s�quence `%%' inconnue pour l'�chappement `%c' (%d) %%Erreur de format: s�quence  `$' pour l'�chappement  `%c' (%d)  %%Erreur de format: format trop long l'�cahppement %%D{} %%Erreur de format: format trop long pour un �chappement $D{}  exp�di� � `%s
 laiss� dans %s
  option %d = "%s"
    Project-Id-Version: GNU enscript 1.3.0
POT-Creation-Date: 1996-06-30 10:16+0300
PO-Revision-Date: 1996-04-01 19:11 EST
Last-Translator: Michel Robitaille <robitail@IRO.UMontreal.CA>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8-bit
 
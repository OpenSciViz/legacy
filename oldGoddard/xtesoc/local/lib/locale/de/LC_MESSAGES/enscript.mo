   /   `  X  �  �����           ��������   *   5          Q   b����      x   ���������   �   �         �   �����      �  ��������   �  7      	  %  �����   
  /  ���������  Z  �        �  ����     �  _��������  �  �        �  �����       ���������  #  �        M  �����     |  %��������  �  G        �  o����     �  ���������    �      #  "  
����     H  E��������  v  �        �  �����       O��������  6  P         W  Q����     x  t��������  �  u      !  �  �����   "  �  ���������  �  �      )    *����   %  9  \��������  P  l   $   '  v  �����   (  �  ���������  �  	   &   ,  �  3����   +  	  [��������  +  �   *   -  Y  �����   .    ���������  �        G  �  8����   1  �  m��������  #  �   0   3  L  �����   4  k  	��������  �  	M   2   8  �  	�����   7  �  	���������    	�   6   9  @  
"����   :  e  
c��������  |  
|   5   A  �  
}����   =  �  
~��������  E  
   <   ?  j  
�����   @  �  
���������  �  	   >   D  �  M����   C  !  s��������  K  �   B   E  d  �����   F  �  ���������  �     ;   S  �  1����   I  �  S��������    z   H   K  I  �����   L  o  ���������  �  �   J   P  �  ����   O  �  6��������  �  P   N   Q  2  �����   R  O  ���������  a  �   M   Y  }  �����   U  �  ��������  �  H   T   W  �  �����   X  '  ���������  Q  �   V   \  y  ,����   [  �  ?��������  �  Q   Z   ^  �  e��������  �  f   ]   _    g��������  W  hxrealloc(): couldn't reallocate %d bytes
 xmalloc(): couldn't allocate %d bytes
 xcalloc(): couldn't allocate %d bytes
 wrapped warning: didn't process following options from environment variable %s:
 unknown special escape: %s unknown encoding: %s unexpected EOF while scanning ^@epsf escape truncated too long file name for ^@epsf escape:
%.*s too long argument for %s escape:
%.*s syntax error in option string %s="%s":
missing end of quotation: %c reading AFM info for font "%s"
 processing file "%s"...
 printer passing through %s file "%s"
 non-printable character codes (decimal):
 must print at least one line per each page: %s missing character codes (decimal):
 malformed underlay position: %s malformed font spec: %s malformed font spec for ^@font escape: %s malformed float dimension: "%s" malformed ^@epsf escape: no '{' found malformed ^@epsf escape: no ']' after options malformed %s escape: no '{' found known media:
name             width	height	llx	lly	urx	ury
------------------------------------------------------------
 invalid value for ^@shade escape: %s invalid value "%s" for option %s illegal value "%s" for option %s illegal underlay style: %s illegal page label format "%s" illegal option: %s illegal option %c for ^@epsf escape illegal non-printable format "%s" epsf: couldn't open pipe to command "%s": %s
 downloading font "%s"
 do not know anything about media "%s" couldn't stat input file "%s": %s couldn't rewind divert file: %s couldn't open printer `%s': %s couldn't open input filter "%s" for file "%s": %s couldn't open input file "%s": %s couldn't open font description file "%s": %s
 couldn't open config file "%s/%s": %s couldn't open EPS file "%s": %s
 couldn't open AFM library: %s couldn't open AFM file for the default font: %s couldn't open AFM file for font "%s", using default
 couldn't get passwd entry for uid=%d: %s couldn't find prolog "%s": %s
 couldn't find header definition file "%s.hdr": %s
 couldn't find header definition file "%s.hdr" couldn't find encoding file "%s.enc": %s
 couldn't create output file "%s": %s couldn't create divert file name: %s couldn't create divert file "%s": %s [ %d pages * %d copy ] Usage: %s [OPTION]... [FILE]...
Mandatory arguments to long options are mandatory for short options too.
  -1                         same as --columns=1
  -2                         same as --columns=2
      --columns=NUM          specify the number of columns per page
  -a, --pages=PAGES          specify which pages are printed
  -b, --header=HEADER        set page header
  -B, --no-header            no page headers
  -c, --truncate-lines       cut long lines (default is to wrap)
  -C, --line-numbers         precede each line with its line number
  -d                         an alias for option --printer
  -D, --setpagedevice=KEY[:VALUE]
                             pass a page device definition to output
  -e, --escapes[=CHAR]       enable special escape interpretation
 Try `%s --help' for more information.
 Long-only options:
  --download-font=NAME       download font NAME
  --filter-stdin=NAME        specify how stdin is shown to the input filter
  --help                     print this help and exit
  --highlight-bar-gray=NUM   print highlight bars with gray NUM (0 - 1)
  --list-media               list names of all known media
  --list-options             list all options and their values
  --non-printable-format=FMT specify how non-printable chars are printed
  --page-label-format=FMT    set page label format to FMT
  --printer-options=OPTIONS  pass extra options to the printer command
  --ul-angle=ANGLE           set underlay text's angle to ANGLE
  --ul-font=NAME             print underlays with font NAME
  --ul-gray=NUM              print underlays with gray value NUM
  --ul-position=POS          set underlay's starting position to POS
  --ul-style=STYLE           print underlays with style STYLE
 EPS file "%s" is too large for page
 EPS file "%s" is not a valid EPS file
 EPS file "%s" does not start with "%%!" magic
 EPS file "%s" contains malformed %%%%BoundingBox row:
"%.*s"
 %s:%d: %%HeaderHeight: no argument %s:%d: %%Format: too long name, maxlen=%d %s:%d: %%Format: no name %s:%d: %%Format: name "%s" is already defined %s:%d: %%FooterHeight: no argument %s: unrecognized option `--%s'
 %s: unrecognized option `%c%s'
 %s: option requires an argument -- %c
 %s: option `--%s' doesn't allow an argument
 %s: option `%s' requires an argument
 %s: option `%s' is ambiguous
 %s: option `%c%s' doesn't allow an argument
 %s: invalid option -- %c
 %s: illegal option -- %c
 %s: illegal newline character specifier: '%s': expected 'n' or 'r'
 %d non-printable characters
 %d lines were %s
 %d characters were missing
 %%Format: unknown `%%' escape `%c' (%d) %%Format: unknown `$' escape `%c' (%d) %%Format: too long variable name for $() escape %%Format: too long format for %%D{} escape %%Format: too long format for $D{} escape %%Format: no closing ')' for $() escape  sent to %s
  left in %s
   option %d = "%s"
   -s, --baselineskip=NUM     set baselineskip to NUM
  -S, --statusdict=KEY[:VALUE]
                             pass a statusdict definition to the output
  -t, --title=TITLE          set banner page's job title to TITLE.  Option
                             sets also the name of the input file stdin.
  -T, --tabsize=NUM          set tabulator size to NUM
  -u, --underlay[=TEXT]      print TEXT under every page
  -v, --verbose              tell what we are doing
  -V, --version              print version number
  -X, --encoding=NAME        use input encoding NAME
  -z, --no-formfeed          do not interpret form feed characters
  -Z, --pass-through         pass through PostScript and PCL files
                             without any modifications
   -f, --font=NAME            use font NAME for body text
  -F, --header-font=NAME     use font NAME for header texts
  -g, --print-anyway         nothing (compatibility option)
  -G                         same as --fancy-header
      --fancy-header[=NAME]  select fancy page header
  -h, --no-job-header        suppress the job header page
  -H, --highlight-bars=NUM   specify how high highlight bars are
  -i, --indent=NUM           set line indent to NUM characters
  -I, --filter=CMD           read input files through input filter CMD
  -j, --borders              print borders around columns
  -k, --page-prefeed         enable page prefeed
  -K, --no-page-prefeed      disable page prefeed
  -l, --lineprinter          simulate lineprinter, this is an alias for:
                               --lines-per-page=66, --no-header, --portrait,
                               --columns=1
   -L, --lines-per-page=NUM   specify how many lines are printed on each page
  -m, --mail                 send mail upon completion
  -M, --media=NAME           use output media NAME
  -n, --copies=NUM           print NUM copies of each page
  -N, --newline=NL           select the newline character.  Possible
                             values for NL are: n (`\n') and r (`\r').
  -o                         an alias for option --output
  -O, --missing-characters   list missing characters
  -p, --output=FILE          leave output to file FILE.  If FILE is `-',
                             leave output to stdout.
  -P, --printer=NAME         print output to printer NAME
  -q, --quiet, --silent      be really quiet
  -r, --landscape            print in landscape mode
  -R, --portrait             print in portrait mode
  xrealloc(): kann %d Bytes nicht erneut unterbringen
 xmalloc(): kann %d Bytes nicht unterbringen
 xmalloc(): kann %d Bytes nicht unterbringen
 zusammengedr�ngt (`wrapped') ACHTUNG: folgende Optionen von der Umgebungsvariablen %s nicht ausgef�hrt:
 unbekanntes Spezil-escape-Zeichen: %s unbekannte Kodierung: %s unerwartetes EOF w�hrend Durchsicht (`scanning') des escape-Zeichens ^@epsf abgeschnitten (`truncated') zu langer Dateiname f�r das escape-Zeichen ^@epsf:
%.*s zu langer Dateiname f�r das escape-Zeichen ^@epsf:
%.*s Syntaxfehler in der Option %s="%s":
fehlendes Ende des Zitats (`quotation'): %c lese AFM-Information f�r Font "%s"
 bearbeite Datei "%s"...
 Drucker durch %s die Datei "%s" weiterreichen
 nicht druckbare Zeichencodes (dezimal):
 muss wenigstens eine Zeile pro Seite drucken: %s fehlende Zeichencodes (dezimal):
 fehlformatierte `underlay' Position: %s unpassende (`malformed') Fontspezifikation: %s fehlformatierte Fontspezifikation f�r das escape-Zeichen ^@font: %s fehlformatierte `float dimension': "%s" fehlformatiertes escape-Zeichen ^@epsf: keine '{' gefunden fehlgebildetes escape-Zeichen ^@epsf: kein ']' nach den Optionen fehlformatiertes escape-Zeichen ^@epsf: keine '{' gefunden unbekanntes Medium:
Name             Weite  H�he    llx     lly     urx     ury
------------------------------------------------------------
   ung�ltiger Wert "%s" f�r Option %s  ung�ltiges Format f�r das Seiten-Label (?) "%s" ung�ltige Option: %s ung�ltige Option %c f�r das escape-Zeichen ^@epsf ung�ltiges, nicht auszugebendes Format (`non-printable') "%s" epsf: kann Pipe zum Befehl "%s" nicht �ffnen: %s
 lade Font "%s"
 Nichts von dem Medium "%s" bekannt kann Eingabedatei "%s" nicht feststellen (`stat'): %s Datei mit abgeleitetem Namen kann nicht "zur�ckgespuhlt" werden: %s kann nicht auf Drucker '%s' zugreifen: %s kann Eingabedatei "%s" nicht �ffnen: %s kann Eingabedatei "%s" nicht �ffnen: %s kann Datei f�r Font-Beschreibung "%s" nicht �ffnen: %s
 kann Konfigurationsdatei "%s/%s" nicht �ffnen: %s kann EPS-Datei "%s" nicht �ffnen: %s
 kann AFM-Bibliothek nicht �ffnen: %s kann AFM-Datei f�r den Vorgabe-Font nicht �ffnen: %s kann AFM-Datei f�r Font "%s" nicht �ffnen, nehme Vorgabe (`default')
 kein Passwort-Eintrag f�r UID=%d zu bekommen: %s kann Vorspann (`prolog') "%s" nicht finden: %s
 kann Datei "%s.hdr" f�r die Kopfangabe nicht finden: %s
 kann Definitionsdatei "%s.hdr" f�r die Kopfangaben nicht finden kann Kodierungsdatei "%s.enc" nicht finden: %s
 kann Ausgabedatei "%s" nicht anlegen: %s Datei mit abgeleitetem Namen kann nicht angelegt werden: %s Datei mit abgeleitetem Namen kann nicht angelegt werden "%s": %s [ %d Seiten * %d Kopie ]    EPS-Datei "%s" ist zu gro� f�r Seite
 EPS-Datei "%s" ist keine g�ltige EPS-Datei
 EPS-Datei "%s" beginnt nicht mit Kennung (`magic') %%!
 EPS-Datei "%s" enth�lt fehlgebildete %%%%BoundingBox-Zeile:
"%.*s"
 %s:%d: %%HeaderHeight: kein Parameter %s:%d: %%Format: zu langer Name, maxlen=%d %s:%d: %%Format: keine Name %s:%d: %%Format: Name "%s" ist schon definiert %s:%d: %%FooterHeight: kein Parameter %s: Nicht erkannte Option '--%s'
 %s: Nicht erkannte Option '%c%s'
 %s: option requires an argument -- %c
 %s: Option '--%s' erlaubt keinen Parameter
 %s: Option '%s' verlangt einen Parameter
 %s: Option '%s' ist zweideutig
 %s: Option '%c%s' erlaubt keinen Parameter
 %s: invalid option -- %c
 %s: illegal option -- %c
 %s: ung�ltiger Bezeichner f�r "Neue Zeile": '%s': erwartet 'n' oder 'r'
 %d nicht druckbare Zeichen
 %d Zeile(n) wurde(n) %s
 %d Zeichen fehlen
 %%Format: unbekanntes `%%' escape-Zeichen '%c' (%d) %%Format: unbekanntes `$' escape-Zeichen '%c' (%d) %%Format: zu langer Variablen-Name f�r das escape-Zeichen $() %%Format: zu langes Format f�r escape-Zeichen %%D{} %%Format: zu langes Format f�r das escape-Zeichen $D{} %%Format: Keine schlie�ende ')' f�r das escape-Zeichen $()  geschickt zu: %s
  abgelegt in: %s
   Option %d = "%s"
    Project-Id-Version: GNU enscript 1.3.2
POT-Creation-Date: 1996-06-30 10:16+0300
PO-Revision-Date: 1996-04-29 17:56 MET DST
Last-Translator: Karl Eichwalder <ke@ke.Central.DE>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8-bit
 
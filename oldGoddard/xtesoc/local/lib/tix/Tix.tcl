#----------------------------------------------------------------------
# The Tix Package stub file. It loads in the Tix Package.
#
#----------------------------------------------------------------------
proc tixInit {args} {
    # The default options

    set option(-scheme)   TK
    set option(-fontset)  TK
    set option(-binding)  TK
    set option(-libdir)   /usr/local/lib/tix

    set validOptions {-scheme -binding -libdir -fontset}

    tixHandleOptions option $validOptions $args

    #-------------------------------
    # Initialization of the package
    #-------------------------------
    global TIX_SOURCE_DIR auto_path

    set TIX_SOURCE_DIR $option(-libdir)

    lappend auto_path $TIX_SOURCE_DIR/schemes
    lappend auto_path $TIX_SOURCE_DIR/bindings

    #-----------------------------------
    # Initialization of options database
    #-----------------------------------
    if {$option(-fontset) == {}} {
	set option(-fontset) TK
    }
    if {$option(-scheme) == {}} {
	set option(-scheme) TK
    }

    # Load the fontset
    #
    if [auto_load tixSetFontset::$option(-fontset)] {
	tixSetFontset::$option(-fontset)
    } else {
	return -code error "unknown fontset option \"$option(-fontset)\""
    }

    # Load the bindings
    #
    if [auto_load tixSetColorScheme::$option(-scheme)] {
	tixSetColorScheme::$option(-scheme)
    } else {
	return -code error "unknown color scheme option \"$option(-scheme)\""
    }

    tixInitOptionDatabase

    #----------------------------------
    # Initialization of binding package
    #----------------------------------
    if {$option(-binding) == {}} {
	set option(-binding) TK
    }

    if [auto_load tixSetBinding::$option(-binding)] {
	tixSetBinding::$option(-binding)
    } else {
	return -code error "unknown binding option \"$option(-binding)\""
    }
}

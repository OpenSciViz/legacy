;ELC   
;;; compiled by root@xpert on Wed Sep 22 17:47:02 1999
;;; from file /tmp/xemacs-21.1.6/lisp/glyphs.el
;;; emacs version 21.1 (patch 6) "Big Bend" XEmacs Lucid.
;;; bytecomp version 2.25 XEmacs; 22-Mar-96.
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 19.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "20")))
    (error "`glyphs.el' was compiled for Emacs 20"))

(or (boundp 'current-load-list) (setq current-load-list nil))


#@299 Return a new `image' specifier object with the specification list SPEC-LIST.
SPEC-LIST can be a list of specifications (each of which is a cons of a
locale and a list of instantiators), a single instantiator, or a list
of instantiators.  See `make-specifier' for more information about
specifiers.
(defalias 'make-image-specifier #[(spec-list) "��\n\"�" [make-specifier-and-init image spec-list] 3 (#$ . 550)])
#@61 A list of the built-in face properties that are specifiers.
(defconst built-in-glyph-specifiers '(image contrib-p baseline) (#$ . 967))
#@2557 Return GLYPH's value of PROPERTY in LOCALE.

If LOCALE is omitted, the GLYPH's actual value for PROPERTY will be
  returned.  For built-in properties, this will be a specifier object
  of a type appropriate to the property (e.g. a font or color
  specifier).  For other properties, this could be anything.

If LOCALE is supplied, then instead of returning the actual value,
  the specification(s) for the given locale or locale type will
  be returned.  This will only work if the actual value of
  PROPERTY is a specifier (this will always be the case for built-in
  properties, but not or not may apply to user-defined properties).
  If the actual value of PROPERTY is not a specifier, this value
  will simply be returned regardless of LOCALE.

The return value will be a list of instantiators (e.g. strings
  specifying a font or color name), or a list of specifications, each
  of which is a cons of a locale and a list of instantiators.
  Specifically, if LOCALE is a particular locale (a buffer, window,
  frame, device, or 'global), a list of instantiators for that locale
  will be returned.  Otherwise, if LOCALE is a locale type (one of
  the symbols 'buffer, 'window, 'frame, 'device, 'device-class, or
  'device-type), the specifications for all locales of that type will
  be returned.  Finally, if LOCALE is 'all, the specifications for all
  locales of all types will be returned.

The specifications in a specifier determine what the value of
  PROPERTY will be in a particular "domain" or set of circumstances,
  which is typically a particular Emacs window along with the buffer
  it contains and the frame and device it lies within.  The value
  is derived from the instantiator associated with the most specific
  locale (in the order buffer, window, frame, device, and 'global)
  that matches the domain in question.  In other words, given a domain
  (i.e. an Emacs window, usually), the specifier for PROPERTY will first
  be searched for a specification whose locale is the buffer contained
  within that window; then for a specification whose locale is the window
  itself; then for a specification whose locale is the frame that the
  window is contained within; etc.  The first instantiator that is
  valid for the domain (usually this means that the instantiator is
  recognized by the device [i.e. the X server or TTY device] that the
  domain is on.  The function `glyph-property-instance' actually does
  all this, and is used to determine how to display the glyph.

See `set-glyph-property' for the built-in property-names.
(defalias 'glyph-property #[(glyph property &optional locale) "�	!����	\"	N��s���!���\")�" [glyphp glyph wrong-type-argument property value locale built-in-glyph-specifiers specifierp specifier-specs] 3 (#$ . 1111)])
#@66 Convert PROPERTY on GLYPH into a specifier, if it's not already.
(defalias 'convert-glyph-property-into-specifier #[(glyph property) "�	!����	\"	N�!?����!���	�#���\"��	#))�" [glyphp glyph wrong-type-argument property specifier specifierp make-specifier generic new-specifier get t add-spec-to-specifier put] 4 (#$ . 3902)])
#@1381 Return the instance of GLYPH's PROPERTY in the specified DOMAIN.

Under most circumstances, DOMAIN will be a particular window,
  and the returned instance describes how the specified property
  actually is displayed for that window and the particular buffer
  in it.  Note that this may not be the same as how the property
  appears when the buffer is displayed in a different window or
  frame, or how the property appears in the same window if you
  switch to another buffer in that window; and in those cases,
  the returned instance would be different.

DOMAIN defaults to the selected window if omitted.

DOMAIN can be a frame or device, instead of a window.  The value
  returned for a such a domain is used in special circumstances
  when a more specific domain does not apply; for example, a frame
  value might be used for coloring a toolbar, which is conceptually
  attached to a frame rather than a particular window.  The value
  is also useful in determining what the value would be for a
  particular window within the frame or device, if it is not
  overridden by a more specific specification.

If PROPERTY does not name a built-in property, its value will
  simply be returned unless it is a specifier object, in which case
  it will be instanced using `specifier-instance'.

Optional arguments DEFAULT and NO-FALLBACK are the same as in
  `specifier-instance'.
(defalias 'glyph-property-instance #[(glyph property &optional domain default no-fallback) "�	!����	\"	N�!���	$)�" [glyphp glyph wrong-type-argument property value specifierp specifier-instance domain default no-fallback] 5 (#$ . 4250)])
#@3370 Change a property of a GLYPH.

NOTE: If you want to remove a property from a glyph, use
  `remove-glyph-property' rather than attempting to set a value of nil
   for the property.

For built-in properties, the actual value of the property is a
  specifier and you cannot change this; but you can change the
  specifications within the specifier, and that is what this function
  will do.  For user-defined properties, you can use this function
  to either change the actual value of the property or, if this value
  is a specifier, change the specifications within it.

If PROPERTY is a built-in property, the specifications to be added to
  this property can be supplied in many different ways:

  -- If VALUE is a simple instantiator (e.g. a string naming a font or
     color) or a list of instantiators, then the instantiator(s) will
     be added as a specification of the property for the given LOCALE
     (which defaults to 'global if omitted).
  -- If VALUE is a list of specifications (each of which is a cons of
     a locale and a list of instantiators), then LOCALE must be nil
     (it does not make sense to explicitly specify a locale in this
     case), and specifications will be added as given.
  -- If VALUE is a specifier (as would be returned by `glyph-property'
     if no LOCALE argument is given), then some or all of the
     specifications in the specifier will be added to the property.
     In this case, the function is really equivalent to
     `copy-specifier' and LOCALE has the same semantics (if it is
     a particular locale, the specification for the locale will be
     copied; if a locale type, specifications for all locales of
     that type will be copied; if nil or 'all, then all
     specifications will be copied).

HOW-TO-ADD should be either nil or one of the symbols 'prepend,
  'append, 'remove-tag-set-prepend, 'remove-tag-set-append, 'remove-locale,
  'remove-locale-type, or 'remove-all.  See `copy-specifier' and
  `add-spec-to-specifier' for a description of what each of
  these means.  Most of the time, you do not need to worry about
  this argument; the default behavior usually is fine.

In general, it is OK to pass an instance object (e.g. as returned
  by `glyph-property-instance') as an instantiator in place of
  an actual instantiator.  In such a case, the instantiator used
  to create that instance object will be used (for example, if
  you set a font-instance object as the value of the 'font
  property, then the font name used to create that object will
  be used instead).  If some cases, however, doing this
  conversion does not make sense, and this will be noted in
  the documentation for particular types of instance objects.

If PROPERTY is not a built-in property, then this function will
  simply set its value if LOCALE is nil.  However, if LOCALE is
  given, then this function will attempt to add VALUE as the
  instantiator for the given LOCALE, using `add-spec-to-specifier'.
  If the value of the property is not a specifier, it will
  automatically be converted into a 'generic specifier.


The following symbols have predefined meanings:

 image		The image used to display the glyph.

 baseline	Percent above baseline that glyph is to be
		displayed.

 contrib-p	Whether the glyph contributes to the
		height of the line it's on.

 face		Face of this glyph (*not* a specifier).
(defalias 'set-glyph-property #[(glyph property value &optional locale tag-set how-to-add) "�	!����	\"s���	N	%������	#����	\"��	N	%��" [glyphp glyph wrong-type-argument property built-in-glyph-specifiers set-specifier value locale tag-set how-to-add put convert-glyph-property-into-specifier add-spec-to-specifier] 6 (#$ . 5888)])
#@187 Remove a property from a glyph.
For built-in properties, this is analogous to `remove-specifier'.
See `remove-specifier' for the meaning of the LOCALE, TAG-SET, and EXACT-P
  arguments.
(defalias 'remove-glyph-property #[(glyph property &optional locale tag-set exact-p) "���\ns����\n\"$��a���\n\"��\n\"���\n\"$�" [locale all property built-in-glyph-specifiers remove-specifier glyph-property glyph tag-set exact-p remprop convert-glyph-property-into-specifier] 5 (#$ . 9622)])
#@27 Return the face of GLYPH.
(defalias 'glyph-face #[(glyph) "�	�\"�" [glyph-property glyph face] 3 (#$ . 10128)])
#@35 Change the face of GLYPH to FACE.
(defalias 'set-glyph-face #[(glyph face) "�	�\n#�" [set-glyph-property glyph face] 4 (#$ . 10246)])
#@387 Return the image of GLYPH in LOCALE, or nil if it is unspecified.

LOCALE may be a locale (the instantiators for that particular locale
  will be returned), a locale type (the specifications for all locales
  of that type will be returned), 'all (all specifications will be
  returned), or nil (the actual specifier object will be returned).

See `glyph-property' for more information.
(defalias 'glyph-image #[(glyph &optional locale) "�	�#�" [glyph-property glyph image locale] 4 (#$ . 10387)])
#@289 Return the instance of GLYPH's image in DOMAIN.

Normally DOMAIN will be a window or nil (meaning the selected window),
  and an instance object describing how the image appears in that
  particular window and buffer will be returned.

See `glyph-property-instance' for more information.
(defalias 'glyph-image-instance #[(glyph &optional domain default no-fallback) "�	�%�" [glyph-property-instance glyph image domain default no-fallback] 6 (#$ . 10892)])
#@664 Change the image of GLYPH in LOCALE.

SPEC should be an instantiator (a string or vector; see
  `image-specifier-p' for a description of possible values here),
  a list of (possibly tagged) instantiators, an alist of specifications
  (each mapping a locale to an instantiator list), or an image specifier
  object.

If SPEC is an alist, LOCALE must be omitted.  If SPEC is a
  specifier object, LOCALE can be a locale, a locale type, 'all,
  or nil; see `copy-specifier' for its semantics.  Otherwise LOCALE
  specifies the locale under which the specified instantiator(s)
  will be added, and defaults to 'global.

See `set-glyph-property' for more information.
(defalias 'set-glyph-image #[(glyph spec &optional locale tag-set how-to-add) "�	�&�" [set-glyph-property glyph image spec locale tag-set how-to-add] 7 (#$ . 11359)])
#@374 Return whether GLYPH contributes to its line height.

LOCALE may be a locale (the instantiators for that particular locale
  will be returned), a locale type (the specifications for all locales
  of that type will be returned), 'all (all specifications will be
  returned), or nil (the actual specifier object will be returned).

See `glyph-property' for more information.
(defalias 'glyph-contrib-p #[(glyph &optional locale) "�	�#�" [glyph-property glyph contrib-p locale] 4 (#$ . 12202)])
#@313 Return the instance of GLYPH's 'contrib-p property in DOMAIN.

Normally DOMAIN will be a window or nil (meaning the selected window),
  and an instance object describing what the 'contrib-p property is in
  that particular window and buffer will be returned.

See `glyph-property-instance' for more information.
(defalias 'glyph-contrib-p-instance #[(glyph &optional domain default no-fallback) "�	�%�" [glyph-property-instance glyph contrib-p domain default no-fallback] 6 (#$ . 12702)])
#@597 Change the contrib-p property of GLYPH in LOCALE.

SPEC should be an instantiator (t or nil), a list of (possibly
  tagged) instantiators, an alist of specifications (each mapping a
  locale to an instantiator list), or a boolean specifier object.

If SPEC is an alist, LOCALE must be omitted.  If SPEC is a
  specifier object, LOCALE can be a locale, a locale type, 'all,
  or nil; see `copy-specifier' for its semantics.  Otherwise LOCALE
  specifies the locale under which the specified instantiator(s)
  will be added, and defaults to 'global.

See `set-glyph-property' for more information.
(defalias 'set-glyph-contrib-p #[(glyph spec &optional locale tag-set how-to-add) "�	�&�" [set-glyph-property glyph contrib-p spec locale tag-set how-to-add] 7 (#$ . 13201)])
#@390 Return the baseline of GLYPH in LOCALE, or nil if it is unspecified.

LOCALE may be a locale (the instantiators for that particular locale
  will be returned), a locale type (the specifications for all locales
  of that type will be returned), 'all (all specifications will be
  returned), or nil (the actual specifier object will be returned).

See `glyph-property' for more information.
(defalias 'glyph-baseline #[(glyph &optional locale) "�	�#�" [glyph-property glyph baseline locale] 4 (#$ . 13985)])
#@284 Return the instance of GLYPH's baseline in DOMAIN.

Normally DOMAIN will be a window or nil (meaning the selected window),
  and an integer or nil (specifying the baseline in that particular
  window and buffer) will be returned.

See `glyph-property-instance' for more information.
(defalias 'glyph-baseline-instance #[(glyph &optional domain default no-fallback) "�	�%�" [glyph-property-instance glyph baseline domain default no-fallback] 6 (#$ . 14499)])
#@668 Change the baseline of GLYPH to SPEC in LOCALE.

SPEC should be an instantiator (an integer [a percentage above the
  baseline of the line the glyph is on] or nil), a list of (possibly
  tagged) instantiators, an alist of specifications (each mapping a
  locale to an instantiator list), or a generic specifier object.

If SPEC is an alist, LOCALE must be omitted.  If SPEC is a
  specifier object, LOCALE can be a locale, a locale type, 'all,
  or nil; see `copy-specifier' for its semantics.  Otherwise LOCALE
  specifies the locale under which the specified instantiator(s)
  will be added, and defaults to 'global.

See `set-glyph-property' for more information.
(defalias 'set-glyph-baseline #[(glyph spec &optional locale tag-set how-to-add) "�	�&�" [set-glyph-property glyph baseline spec locale tag-set how-to-add] 7 (#$ . 14967)])
#@779 Return a new `glyph' object of type TYPE.

TYPE should be one of `buffer' (used for glyphs in an extent, the modeline,
the toolbar, or elsewhere in a buffer), `pointer' (used for the mouse-pointer),
or `icon' (used for a frame's icon), and defaults to `buffer'.

SPEC-LIST is used to initialize the glyph's image.  It is typically an
image instantiator (a string or a vector; see `image-specifier-p' for
a detailed description of the valid image instantiators), but can also
be a list of such instantiators (each one in turn is tried until an
image is successfully produced), a cons of a locale (frame, buffer, etc.)
and an instantiator, a list of such conses, or any other form accepted
by `canonicalize-spec-list'.  See `make-specifier' for more information
about specifiers.
(defalias 'make-glyph #[(&optional spec-list type) "�	!���\n\"�\n)�" [make-glyph-internal type glyph spec-list set-glyph-image] 3 (#$ . 15820)])
#@49 Return t if OBJECT is a glyph of type `buffer'.
(defalias 'buffer-glyph-p #[(object) "�	!���	!�a�" [glyphp object glyph-type buffer] 2 (#$ . 16752)])
#@50 Return t if OBJECT is a glyph of type `pointer'.
(defalias 'pointer-glyph-p #[(object) "�	!���	!�a�" [glyphp object glyph-type pointer] 2 (#$ . 16908)])
#@47 Return t if OBJECT is a glyph of type `icon'.
(defalias 'icon-glyph-p #[(object) "�	!���	!�a�" [glyphp object glyph-type icon] 2 (#$ . 17067)])
#@786 Return a new `pointer-glyph' object with the specification list SPEC-LIST.

This is equivalent to calling `make-glyph', specifying a type of `pointer'.

SPEC-LIST is used to initialize the glyph's image.  It is typically an
image instantiator (a string or a vector; see `image-specifier-p' for
a detailed description of the valid image instantiators), but can also
be a list of such instantiators (each one in turn is tried until an
image is successfully produced), a cons of a locale (frame, buffer, etc.)
and an instantiator, a list of such conses, or any other form accepted
by `canonicalize-spec-list'.  See `make-specifier' for more information
about specifiers.

You can also create a glyph with an empty SPEC-LIST and add image
instantiators afterwards using `set-glyph-image'.
(defalias 'make-pointer-glyph #[(&optional spec-list) "�	�\"�" [make-glyph spec-list pointer] 3 (#$ . 17218)])
#@780 Return a new `icon-glyph' object with the specification list SPEC-LIST.

This is equivalent to calling `make-glyph', specifying a type of `icon'.

SPEC-LIST is used to initialize the glyph's image.  It is typically an
image instantiator (a string or a vector; see `image-specifier-p' for
a detailed description of the valid image instantiators), but can also
be a list of such instantiators (each one in turn is tried until an
image is successfully produced), a cons of a locale (frame, buffer, etc.)
and an instantiator, a list of such conses, or any other form accepted
by `canonicalize-spec-list'.  See `make-specifier' for more information
about specifiers.

You can also create a glyph with an empty SPEC-LIST and add image
instantiators afterwards using `set-glyph-image'.
(defalias 'make-icon-glyph #[(&optional spec-list) "�	�\"�" [make-glyph spec-list icon] 3 (#$ . 18121)])
#@60 Return t if OBJECT is an image instance of type `nothing'.
(defalias 'nothing-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type nothing] 2 (#$ . 19011)])
#@57 Return t if OBJECT is an image instance of type `text'.
(defalias 'text-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type text] 2 (#$ . 19208)])
#@64 Return t if OBJECT is an image instance of type `mono-pixmap'.
(defalias 'mono-pixmap-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type mono-pixmap] 2 (#$ . 19396)])
#@65 Return t if OBJECT is an image instance of type `color-pixmap'.
(defalias 'color-pixmap-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type color-pixmap] 2 (#$ . 19605)])
#@60 Return t if OBJECT is an image instance of type `pointer'.
(defalias 'pointer-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type pointer] 2 (#$ . 19817)])
#@120 Return t if OBJECT is an image instance of type `subwindow'.
Subwindows are not implemented in this version of XEmacs.
(defalias 'subwindow-image-instance-p #[(object) "�	!���	!�a�" [image-instance-p object image-instance-type subwindow] 2 (#$ . 20015)])
#@102 *The shape of the mouse-pointer when over text.
This is a glyph; use `set-glyph-image' to change it.
(defvar text-pointer-glyph (make-pointer-glyph) (#$ . -20277))
(set-glyph-face text-pointer-glyph 'pointer)
#@194 *The shape of the mouse-pointer when over a buffer, but not over text.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, `text-pointer-glyph' is used.
(defvar nontext-pointer-glyph (make-pointer-glyph) (#$ . -20493))
(set-glyph-face nontext-pointer-glyph 'pointer)
#@182 *The shape of the mouse-pointer when over the modeline.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, `nontext-pointer-glyph' is used.
(defvar modeline-pointer-glyph (make-pointer-glyph) (#$ . -20807))
(set-glyph-face modeline-pointer-glyph 'pointer)
#@191 *The shape of the mouse-pointer when over a selectable text region.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, `text-pointer-glyph' is used.
(defvar selection-pointer-glyph (make-pointer-glyph) (#$ . -21111))
(set-glyph-face selection-pointer-glyph 'pointer)
#@194 *The shape of the mouse-pointer when XEmacs is busy.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, the pointer is not changed
when XEmacs is busy.
(defvar busy-pointer-glyph (make-pointer-glyph) (#$ . -21426))
(set-glyph-face busy-pointer-glyph 'pointer)
#@179 *The shape of the mouse-pointer when over a toolbar.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, `nontext-pointer-glyph' is used.
(defvar toolbar-pointer-glyph (make-pointer-glyph) (#$ . -21734))
(set-glyph-face toolbar-pointer-glyph 'pointer)
#@186 *The shape of the mouse-pointer when over a window divider.
This is a glyph; use `set-glyph-image' to change it.
If unspecified in a particular domain, `nontext-pointer-glyph' is used.
(defvar divider-pointer-glyph (make-pointer-glyph) (#$ . -22033))
(byte-code "�	�\"���!����\"���!����\"���\"�" [set-glyph-face divider-pointer-glyph pointer featurep menubar menubar-pointer-glyph scrollbar scrollbar-pointer-glyph gc-pointer-glyph] 3)
(defalias 'dontusethis-set-value-glyph-handler #[(sym args fun harg handler) "��\n\"�" [error "Use `set-glyph-image' to set `%s'" sym] 3])
(defalias 'dontusethis-make-unbound-glyph-handler #[(sym args fun harg handler) "��\n\"�" [error "Can't `makunbound' `%s'" sym] 3])
(defalias 'dontusethis-make-local-glyph-handler #[(sym args fun harg handler) "��\n\"�" [error "Use `set-glyph-image' to make local values for `%s'" sym] 3])
(defalias 'define-constant-glyph #[(sym) "�	��#��	��#��	��#��	��#�" [dontusethis-set-symbol-value-handler sym set-value dontusethis-set-value-glyph-handler make-unbound dontusethis-make-unbound-glyph-handler make-local dontusethis-make-local-glyph-handler put const-glyph-variable t] 4])
(byte-code "��!���!���!���!���!���!���!���!���!���!���!���!���!���!���!���!���!�" [define-constant-glyph text-pointer-glyph nontext-pointer-glyph modeline-pointer-glyph selection-pointer-glyph busy-pointer-glyph gc-pointer-glyph divider-pointer-glyph toolbar-pointer-glyph menubar-pointer-glyph scrollbar-pointer-glyph octal-escape-glyph control-arrow-glyph invisible-text-glyph hscroll-glyph truncation-glyph continuation-glyph frame-icon-glyph] 2)
(defalias 'dontusethis-old-pointer-shape-handler #[(sym args fun harg handler) "@�����\"���J	\")�" [args value remove-specifier harg global set-glyph-image] 4])
(defalias 'define-obsolete-pointer-glyph #[(old new) "	�\n\"��\n\"�*���	$�" [old new newvar oldvar defvaralias make-compatible-variable dontusethis-set-symbol-value-handler set-value dontusethis-old-pointer-shape-handler] 5])
(byte-code "���\"����\"����\"����\"����\"����\"����\"�" [define-obsolete-pointer-glyph x-pointer-shape text-pointer-glyph x-nontext-pointer-shape nontext-pointer-glyph x-mode-pointer-shape modeline-pointer-glyph x-selection-pointer-shape selection-pointer-glyph x-busy-pointer-shape busy-pointer-glyph x-gc-pointer-shape gc-pointer-glyph x-toolbar-pointer-shape toolbar-pointer-glyph] 3)
(defalias 'init-glyphs #[nil "��!�������!��������#�BB���!�����!�����!�����!�����!�����!�����!��������#�BBC&B\"\"���!�����\"�����$��!���$��#���$��$�\"��%�\"��'�\"����\",�!,\"��,\"��#,\"�)��!���.�0����P��$���!���3��P��$�����!���3��P��$���!���.���$�" [featurep x set-console-type-image-conversion-list append xpm (("\\.xpm\\'" [xpm :file nil] 2)) "\\.xbm\\'" vector xbm :file nil (2) (("\\`/\\* XPM \\*/" [xpm :data nil] 2)) xface (("\\`X-Face:" [xface :data nil] 2)) gif (("\\.gif\\'" [gif :file nil] 2) ("\\`GIF8[79]" [gif :data nil] 2)) jpeg (("\\.jpe?g\\'" [jpeg :file nil] 2)) (("\\`���� JFIF" [jpeg :data nil] 2)) png (("\\.png\\'" [png :file nil] 2)) (("\\`�PNG" [png :data nil] 2)) "" autodetect :data (2) tty (("^#define" [string :data "[xpm]"]) ("\\`X-Face:" [string :data "[xface]"]) ("\\`/\\* XPM \\*/" [string :data "[xpm]"]) ("\\`GIF87" [string :data "[gif]"]) ("\\`��� JFIF" [string :data "[jpeg]"]) ("" [string :data nil] 2) ("" [nothing])) set-glyph-image truncation-glyph "$" global continuation-glyph "\\" hscroll-glyph octal-escape-glyph control-arrow-glyph "^" invisible-text-glyph " ..." make-face border-glyph "Truncation and continuation glyphs face" face set-glyph-face xemacs-logo "../etc/" emacs-beta-version "xemacs-beta.xpm" "xemacs.xpm" frame-icon-glyph "xemacs-icon.xpm" "xemacs-icon2.xbm" "XEmacs <insert spiffy graphic logo here>"] 18])
(init-glyphs)

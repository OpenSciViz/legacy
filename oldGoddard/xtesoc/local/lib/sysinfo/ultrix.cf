#
# $Id: ultrix.cf,v 1.5 1994/11/15 23:25:53 mcooper Exp $
#
# SYSINFO configuration file for Ultrix
#
include		local.cf
#
# Device definetions.
#
# 	Format is
#
#		KEY |NAME |TYPE |NumID |MODEL INFO |DESCRIPTION |FLAGS |FILE
#
# 	The NAME field can have multiple names, each seperated by 
#	a space (` ').
#
# 	Some devices are marked `Zombie' because they usually are
# 	not indicated as ALIVE by the kernel, but should only
# 	be found in the device table if they exists.
#
# Network Interfaces
#
#	 Driver		Model				 Description
device	|ln	|netif||AMD Lance Am7990		|10Mb/sec Ethernet
device	|de	|netif||DEC Ethernet			|10Mb/sec Ethernet
device	|ni	|netif||DEC Ethernet			|10Mb/sec Ethernet
device	|ne	|netif||Second Generation Ethernet	|10Mb/sec Ethernet
device	|qe	|netif||DEQNA/DELQA 			|10Mb/sec Ethernet
device	|xna	|netif||DEBNI/DEMNA 			|10Mb/sec Ethernet
device	|fza	|netif||DEFZA FDDI			|100Mb/sec DAS FDDI
#
# Disk Drive Drivers
#
# 	These values are used to determine what devices are disk drives.
#	Info about specific devices is read from each device.
#
device	|fd	|diskdrive
device	|ra	|diskdrive
device	|rz	|diskdrive
#
# Tape Drive Drivers
#
# 	These values are used to determine what devices are tape drives.
#
device	|mu	|tapedrive
device	|stc	|tapedrive
device	|tms	|tapedrive
device	|ts	|tapedrive
device	|tu	|tapedrive
device	|tz	|tapedrive
#
# Frame Buffers
#
#	I don't know anything about DEC frame buffers
#
#      Driver Name		     
device|cfb	|generic||Color Frame Buffer
device|pm	|generic||Graphics Device
device|px	|generic||Graphics Device
device|ga	|generic||Graphics Device
device|gq	|generic||Graphics Device
device|fb	|generic||Graphics Device
#
#	Bus Info
#
device	|dti		|bus		|	|Desktop Interconnect
#
# 	Miscellaneous devices
#
device	|pp		|generic|TC-100		|parallel card
device	|presto		|generic|PrestoServe	|NFS accelerator
device	|dc		|generic|		|4-port serial line controller
device	|mdc		|generic|DS5100		|4-port serial line controller
device	|scc		|generic|SCC		|2-port serial line controller
device	|cxa		|generic|CXA16		|16-port serial line controller
device	|cxy		|generic|CXY08		|8-port serial line controller
device	|dhv		|generic|DHV11		|8-port serial line controller
device	|dmb		|generic|DMB32		|8-port serial line controller
device	|dhq		|generic|DHQ11		|8-port serial line controller
#
# Definetions
# 	Format of Definetions is:
#
#define	|DefineName	|StringKey	|NumericKey	|StrValue1|StrValue2
#
# System Model definetions
#
#	Values are from "/usr/include/machine/cpuconf.h".
#
define|SysModel||1	|VAX-11/78x		# 780, 785
define|SysModel||2	|VAX-11/750
define|SysModel||3	|VAX-11/730
define|SysModel||4	|VAX-86x0		# 8600, 8650
define|SysModel||5	|VAX-8[23]x0		# 8200, 8250, 8300, 8350
define|SysModel||6	|VAX-8[578]x0		# 8500, 8550, 8700, 8800
define|SysModel||7	|MicroVAX-I
define|SysModel||8	|MicroVAX-II
define|SysModel||9	|Virtual VAX		# This can't be real
define|SysModel||10	|VAX-3[26]00		# Mayfair I
define|SysModel||11	|VAX-6[23]00		# CVAX/Calypso
define|SysModel||12	|VAX-3[34]00		# Mayfair II
define|SysModel||13	|VAX-3100		# PVAX
define|SysModel||14	|VAX-35x0		# Firefox
define|SysModel||15	|VAX-3[89]00		# Mayfair III
define|SysModel||16	|DECsystem-[23]100	# PMAX
define|SysModel||17	|VAX-88[234]0		# SID for Polarstar
define|SysModel||18	|DECsystem-5400		# MIPSfair
define|SysModel||19	|DECsystem-5800		# ISIS
define|SysModel||20	|DECsystem-5000/200	# 3MAX
define|SysModel||21	|DECsystem-CMAX		# RIGEL/Calypso
define|SysModel||23	|VAXstation-2000	# MIPSFAIR-2
define|SysModel||25	|DECsystem-5100		# MIPSMATE
define|SysModel||26	|VAX-9000		# VAX9000
define|SysModel||27	|DECsystem-5000/100	# 3MIN
define|SysModel||28	|DECsystem-5000/240	# 3MAX+
define|SysModel||29	|DECsystem-5000/20	# MAXine
#define|SysModel||DS_R4_5000_100|DECsystem-5000/150	# R4000 3MIN
#define|SysModel||DS_R4_5000_300|DECsystem-5000/260	# R4000 3MAX+
#define|SysModel||DS_R4_MAXINE	|DECsystem-5000/50	# R4000 MAXine
#
# Category definetions
#
# 	These are from DEV_* in `/usr/include/sys/devio.h'.
#
define	|Category	||0x00		|Tape drive
define	|Category	||0x01		|Disk drive
define	|Category	||0x02		|Terminal
define	|Category	||0x03		|Printer
define	|Category	||0x04		|Special
#
# Network Types
#
#	These are from ENDT_* in `/usr/include/net/pfilt.h'.
#
define	|NetType	||3		|3Mb/sec Ethernet
define	|NetType	||1		|Byte Swapped 3Mb/sec Ethernet
define	|NetType	||2		|10Mb/sec Ethernet
define	|NetType	||4		|100Mb/sec FDDI
#
# Tape Info
#
#	These are from DEV_* in `/usr/include/devio.h'.
#
define	|TapeInfo	||0x10		|800 bpi density
define	|TapeInfo	||0x20		|1600 bpi density
define	|TapeInfo	||0x40		|6250 bpi density
define	|TapeInfo	||0x80		|6666 bpi density
define	|TapeInfo	||0x100		|10240 bpi density
define	|TapeInfo	||0x200		|38000 bpi density
define	|TapeInfo	||0x400		|media loader present
define	|TapeInfo	||0x800		|38000 bpi compacted density
define	|TapeInfo	||0x1000	|76000 bpi density
define	|TapeInfo	||0x2000	|76000 bpi compacted density
define	|TapeInfo	||0x4000	|QIC-24 9 track
define	|TapeInfo	||0x8000	|QIC-120 15trk and QIC-150 18trk
define	|TapeInfo	||0x10000	|QIC-320/525 26 track
define	|TapeInfo	||0x20000	|4mm tape cartridge
define	|TapeInfo	||0x40000	|8mm tape cartridge
define	|TapeInfo	||0x80000	|TZ85
#
# Kernel Variables
#
#		 SYMBOL		  TYPE	 DESCRIPTION
define|Kernel	|maxusers	||int	|Maximum number of users (for system tables)
define|Kernel	|maxuprc	||int	|Maximum number of processes per user
define|Kernel	|nproc		||int	|Maximum number of processes for system
define|Kernel	|npty		||int	|Maximum number of BSD (/dev/ptyXX) pty's
define|Kernel	|nfile		||int	|Maximum number of open files for system
define|Kernel	|max_nofile	||int	|Maximum number of open files per process
define|Kernel	|ndquot		||int	|Size of the quotas table
define|Kernel	|ngnode		||int	|Size of the gnode table
define|Kernel	|nchsize	||int	|Size of the gnode cache table
define|Kernel	|bufcache	||int	|Percent of memory to use for buffer cache
define|Kernel	|ncsize		||int	|Size of the directory name lookup cache
define|Kernel	|ncallout	||int	|Size of the callout table
#
# System Configuration
#
#	NAME and ID are from <unistd.h> and sysconf(2)
#
#	       NAME		 ID	 TYPE DESCRIPTION
define|SysConf|_SC_ARG_MAX	|1	|long|Max combined size of argv[] and envp[]
define|SysConf|_SC_CHILD_MAX	|2	|long|Max processes allowed to any UID
define|SysConf|_SC_CLK_TCK	|3	|long|Clock ticks per second
define|SysConf|_SC_NGROUPS_MAX	|4	|long|Max simultaneous groups per user
define|SysConf|_SC_OPEN_MAX	|5	|long|Max open files per process
define|SysConf|_SC_JOB_CONTROL	|6	|bool|Job control supported
define|SysConf|_SC_SAVED_IDS	|7	|bool|Savid ids (seteuid()) supported
define|SysConf|_SC_VERSION	|8	|long|Version of POSIX.1 standard supported
define|SysConf|_SC_XOPEN_VERSION|9	|long|X/Open Portability Guide version
define|SysConf|_SC_PASS_MAX	|10	|long|Max password length

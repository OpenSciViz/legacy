<title>Figures</title>
<H1>Figures</H1>

<B>Permitted Context:</B> %body.content, %flow, %block<BR>
<B>Content Model:</B> Optional <a href="overlays.html">OVERLAY</a>s
followed by an optional <a href="captions.html">CAPTION</a>, then
%body.content and finally an optional <a href="credits.html">CREDIT</a>

<P>The FIG element is used for figures. Subsequent elements will be
flowed around the figure if there is sufficient room. This behaviour is
disabled when the align attribute is <i>center</i> (the default) or
<i>justify</i>.

<P>Figure overlays provide for more effective use of caching as small
changes to a figure in a subsequent document incur only the penalty of
downloading the overlays and not the larger base figure, as the latter is
already in the cache.

<P>The figure description text is intended to convey the content of the
figure for people with non-graphical user agents, while the figure
caption and credit are rendered on both graphical and non-graphical
user agents. The FIG element improves on the IMG element by allowing
authors to use markup for the description text. The content model
allows authors to include headers, which is appropriate when the
headers are part of the image data. It also allows graphical hypertext
links to be specified in the markup and interpreted by the user agent
rather than the server.

<P>The anchor elements in the figure description text play a dual role:
Non-graphical user agents show conventional hypertext links, while for
graphical user agents, the same anchor elements specify graphical
hypertext links, with the SHAPE attribute designating the hotzones. This
is designed to simplify the task of authors writing for both audiences.
Hopefully, the FIG element will help to combat the tendency for authors
to forget about people limited to terminal access or the visually
impaired relying on text to speech, as the new element forces you to
write description text to define the graphical hypertext links.

<P>For some applications the hotzones are dynamically defined by
programs running on the server. HTML 3.0 allows clicks and drags to be
passed to the server with the IMAGEMAP attribute. Hotzones may also be
specified as part of the graphics data format e.g. as in VRML. Hotzones
in the FIG element take precedence over hotzones in the graphics data,
which in turn take precedence over passing events to a server imagemap
program.


<P>Hotzones in overlay graphics data take precedence over hotzones in
figure data. Similarly, the imagemap attribute in overlays takes
precedence over the imagemap attribute for the figure. For a group of
overlapping overlays the precedence is determined by the order the
OVERLAY elements appear within the FIG element. Later overlays take
precedence over earlier ones.

<h3>Examples</h3>

<P>Photographic image with caption and credits:

<pre>&lt;FIG SRC="nicodamus.jpeg"&gt;
  &lt;CAPTION&gt;Ground dweller: &lt;I&gt;Nicodamus bicolor&lt;/I&gt;
  builds silk snares&lt;/CAPTION&gt;
  &lt;P&gt;A small hairy spider light fleshy red in color with a brown abdomen.
  &lt;CREDIT&gt;J. A. L. Cooke/OSF&lt;/CREDIT&gt;
&lt;/FIG&gt;</pre>

<P>Company home page:

<pre>&lt;FIG SRC="mainmenu.gif"&gt;
 &lt;H1&gt;Access HP from Hewlett Packard&lt;/H1&gt;
 &lt;P&gt;Select between:
 &lt;UL&gt;
  &lt;LI&gt;&lt;A HREF="guide.html" SHAPE="rect 30,200,60,16"&gt;Access Guide&lt;/A&gt;
  &lt;LI&gt;&lt;A HREF="about.html" SHAPE="rect 100,200,50,16"&gt;About HP&lt;/A&gt;
  &lt;LI&gt;&lt;A HREF="guide.html" SHAPE="rect 160,200,30,16"&gt;News&lt;/A&gt;
  &lt;LI&gt;&lt;A HREF="guide.html" SHAPE="rect 200,200,50,16"&gt;Products&lt;/A&gt;
  &lt;LI&gt;&lt;A HREF="guide.html" SHAPE="rect 260,200,80,16"&gt;Worldwide Contacts&lt;/A&gt;
 &lt;/UL&gt;
&lt;/FIG&gt;</pre>

<P>Aerial photograph with map overlay:

<pre>&lt;FIG SRC="newyork.jpeg"&gt;
  &lt;OVERLAY SRC="map.gif"&gt;
  &lt;P&gt;New York from the air!
&lt;/FIG&gt;</pre>

<H3>Permitted Attributes</h3>

<DL> <DT>ID <DD>An SGML identifier used as the target for hypertext
links or for naming particular elements in associated style sheets.
Identifiers are NAME tokens and must be unique within the scope of the
current document.

<DT>LANG <DD>This is one of the ISO standard language abbreviations,
e.g. "en.uk" for the variation of English spoken in the United Kingdom.
It can be used by parsers to select language specific choices for
quotation marks, ligatures and hypenation rules etc. The language
attribute is composed from the two letter language code from ISO 639,
optionally followed by a period and a two letter country code from ISO
3166.

<DT>CLASS <DD>This a space separated list of SGML NAME tokens and is
used to subclass tag names. By convention, the class names are
interpreted hierarchically, with the most general class on the left
and the most specific on the right, where classes are separated by a
period. The CLASS attribute is most commonly used to attach a
different style to some element, but it is recommended that where
practical class names should be picked on the basis of the element's
semantics, as this will permit other uses, such as restricting search
through documents by matching on element class names. The  conventions
for choosing class names are outside the scope of this specification.

<DT>CLEAR <DD>When there is already a figure or table in the margin, you
sometimes want to position another figure below the figure in the margin
rather than alongside it. The CLEAR attribute allows you to move down
unconditionally:<BR><BR>

<DL>
<DT>clear=left<DD>move down until left margin is clear
<DT>clear=right<DD>move down until right margin is clear
<DT>clear=all<DD>move down until both margins are clear
</DL>

<P>Alternatively, you can decide to place the figure alongside the
figure in the margin just so long as there is enough room. The minimum
width needed is specified as:<BR><BR>

<DL>
<DT>clear="40 en"<DD>move down until there is at least 40 en units free
<DT>clear="100 pixels"<DD>move down until there is at least 100 pixels
free
</DL>

<P>The style sheet (or browser defaults) may provide default minimum
widths for each class of block-like elements.

<DT>NOFLOW <DD>The presence of this attribute disables text flow around
the figure. It avoids the need to use the CLEAR or NEEDS attributes
on the following element.

<DT>SRC <DD>Specifies the figure's graphical content. The image is
specified as a URI. This attribute may appear together with the MD
attribute.

<DT>MD <DD>Specifies a message digest or cryptographic checksum for the
associated graphic specified by the SRC attribute. It is used when you
want to be sure that a linked object is indeed the same one that the
author intended, and hasn't been modified in any way. For instance,
MD="md5:jV2OfH+nnXHU8bnkPAad/mSQlTDZ", which specifies an MD5 checksum
encoded as a base64 character string. The MD attribute is generally
allowed for all elements which support URI based links.

<DT>ALIGN <DD>Specifies horizontal alignment of the figure:<BR><BR>
<DL>
<DT>BLEEDLEFT <DD>Flush left with the left (window) border.
<DT>LEFT <DD>Flush left with the left text margin.
<DT>CENTER <DD>The figure is centered between the text margins and text
flow around the figure is disabled. This is the default setting for
ALIGN.
<DT>RIGHT <DD>Flush right with the right text margin.
<DT>BLEEDRIGHT <DD>Flush right with the right (window) border
<DT>JUSTIFY <DD>When applicable the figure should be magnified or reduced
to fill the space between the left and right text margins. Text flow
around the figure is disabled for align=justify.
</DL>

<DT>WIDTH <DD>Specifies the desired width in pixels or en units
(according to the value of the UNITS attribute). User agents may scale
the figure image to match this width.

<DT>HEIGHT <DD>Specifies the desired height in pixels or en units
(according to the value of the UNITS attribute). User agents may scale
the figure image to match this height.

<DT>UNITS <DD>Specifies the choice of units for width and height.
<tt>units=pixels</tt> (the default) specifies pixels, while
<tt>units=en</tt> specifies en units. The en unit is a typographical unit
equal to half the point size.

<DT>IMAGEMAP <DD>Specifies a URI for processing image clicks and drags.

</DL>
<!--NewPage-->


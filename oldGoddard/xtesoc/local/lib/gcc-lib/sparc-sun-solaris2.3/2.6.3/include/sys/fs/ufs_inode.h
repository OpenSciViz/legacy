/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T	*/
/*	  All Rights Reserved  	*/

/*	THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF AT&T	*/
/*	The copyright notice above does not evidence any   	*/
/*	actual or intended publication of such source code.	*/

/*
 * Copyright (c) 1993 by Sun Microsystems, Inc.
 */

#ifndef	_SYS_FS_UFS_INODE_H
#define	_SYS_FS_UFS_INODE_H

#pragma ident	"@(#)ufs_inode.h	2.81	93/07/21 SMI"
/* SVr4.0 1.16	*/

#include <sys/fbuf.h>
#include <sys/fcntl.h>
#include <sys/uio.h>
#include <sys/t_lock.h>
#include <sys/thread.h>
#include <sys/fs/ufs_fs.h>
#include <sys/fs/ufs_lockfs.h>
#include <sys/kstat.h>

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * The I node is the focus of all local file activity in UNIX.
 * There is a unique inode allocated for each active file,
 * each current directory, each mounted-on file, each mapping,
 * and the root.  An inode is `named' by its dev/inumber pair.
 * Data in icommon is read in from permanent inode on volume.
 *
 * Each inode has 5 locks associated with it:
 *	i_rwlock:	Serializes ufs_write and ufs_setattr request
 *			and allows ufs_read requests to proceed in parallel.
 *			Serializes reads/updates to directories.
 *	i_contents:	Protects almost all of the fields in the inode
 *			except for those list below.
 *	i_tlock:	Protects just the i_atime, i_mtime, i_ctime,
 *			i_delayoff, i_delaylen, i_nextrio, i_writes
 *			and i_flag fields.
 *	ih_lock:	Protects inode hash chain buckets
 *	ifree_lock:	Protects inode freelist
 *
 * Lock ordering:
 *	i_rwlock > i_contents > ih_lock > i_tlock > ifree_lock
 *
 * SVR4 Extended Fundamental Type (EFT) support:
 * 	The inode structure has been enhanced to support
 *	32-bit user-id, 32-bit group-id, and 32-bit device number.
 *	Standard SVR4 ufs also supports 32-bit mode field.  For the reason
 *	of backward compatibility with the previous ufs disk format,
 *	32-bit mode field is not supported.
 *
 *	The current inode structure is 100% backward compatible with
 *	the previous inode structure if no user-id or group-id exceeds
 *	MAXUID (param.h), and no major or minor number of a device number
 *	stored in an inode exceeds 255.
 *
 */

#define	UID_LONG  (o_uid_t)65535
				/* flag value to indicate uid is 32-bit long */
#define	GID_LONG  (o_uid_t)65535
				/* flag value to indicate gid is 32-bit long */

#define	NDADDR	12		/* direct addresses in inode */
#define	NIADDR	3		/* indirect addresses in inode */
#define	FSL_SIZE (NDADDR+NIADDR-1) * sizeof (daddr_t)
				/* max fast symbolic name length is 56 */

#define	i_fs	i_ufsvfs->vfs_bufp->b_un.b_fs

struct 	icommon {
	o_mode_t ic_smode;	/*  0: mode and type of file */
	short	ic_nlink;	/*  2: number of links to file */
	o_uid_t	ic_suid;	/*  4: owner's user id */
	o_gid_t	ic_sgid;	/*  6: owner's group id */
	quad	ic_size;	/*  8: number of bytes in file */
#ifdef _KERNEL
	struct timeval ic_atime; /* 16: time last accessed */
	struct timeval ic_mtime; /* 24: time last modified */
	struct timeval ic_ctime; /* 32: last time inode changed */
#else
	time_t	ic_atime;	/* 16: time last accessed */
	long	ic_atspare;
	time_t	ic_mtime;	/* 24: time last modified */
	long	ic_mtspare;
	time_t	ic_ctime;	/* 32: last time inode changed */
	long	ic_ctspare;
#endif
	daddr_t	ic_db[NDADDR];	/* 40: disk block addresses */
	daddr_t	ic_ib[NIADDR];	/* 88: indirect blocks */
	long	ic_flags;	/* 100: status, currently unused */
	long	ic_blocks;	/* 104: blocks actually held */
	long	ic_gen;		/* 108: generation number */
	long	ic_mode_reserv; /* 112: reserved */
	uid_t	ic_uid;		/* 116: long EFT version of uid */
	gid_t	ic_gid;		/* 120: long EFT version of gid */
	ulong	ic_oeftflag;	/* 124: reserved */
};

struct inode {
	struct	inode *i_chain[2];	/* must be first */
	struct	vnode i_vnode;	/* vnode associated with this inode */
	struct	vnode *i_devvp;	/* vnode for block I/O */
	u_short	i_flag;
	dev_t	i_dev;		/* device where inode resides */
	ino_t	i_number;	/* i number, 1-to-1 with device address */
	off_t	i_diroff;	/* offset in dir, where we found last entry */
	struct	ufsvfs *i_ufsvfs; /* incore fs associated with inode */
	struct	dquot *i_dquot;	/* quota structure controlling this file */
	krwlock_t i_rwlock;	/* serializes write/setattr requests */
	krwlock_t i_contents;	/* protects (most of) inode contents */
	kmutex_t i_tlock;	/* protects time fields, i_flag */
	daddr_t	i_nextr;	/*
				 * next byte read offset (read-ahead)
				 *   No lock required
				 */
	struct inode  *i_freef;	/* free list forward */
	struct inode **i_freeb;	/* free list back */
	ulong	i_vcode;	/* version code attribute */
	long	i_mapcnt;	/* mappings to file pages */
	int	*i_map;		/* block list for the corresponding file */
	long	i_rdev;		/* INCORE rdev from i_oldrdev by ufs_iget */
	long	i_delaylen;	/* delayed writes, units=bytes */
	long	i_delayoff;	/* where we started delaying */
	long	i_nextrio;	/* where to start the next clust */
	long	i_writes;	/* number of outstanding bytes in write q */
	kcondvar_t i_wrcv;	/* sleep/wakeup for write throttle */
	kthread_id_t i_owner;	/* prevent races in bmap_write/ufs_itrunc */
	struct 	icommon	i_ic;
};

struct dinode {
	union {
		struct	icommon di_icom;
		char	di_size[128];
	} di_un;
};

#define	i_mode		i_ic.ic_smode
#define	i_nlink		i_ic.ic_nlink
#define	i_uid		i_ic.ic_uid
#define	i_gid		i_ic.ic_gid
#define	i_smode		i_ic.ic_smode
#define	i_suid		i_ic.ic_suid
#define	i_sgid		i_ic.ic_sgid

/* ugh! -- must be fixed */
#if defined(__vax__) || defined(__i386__)
#define	i_size		i_ic.ic_size.val[0]
#define	i_size_o	i_ic.ic_size.val[1]
#endif
#if defined(__mc68000__) || defined(__sparc__) || defined(u3b2) || defined(u3b15)
#define	i_size		i_ic.ic_size.val[1]
#define	i_size_o	i_ic.ic_size.val[0]
#endif
#define	i_db		i_ic.ic_db
#define	i_ib		i_ic.ic_ib

#define	i_atime		i_ic.ic_atime
#define	i_mtime		i_ic.ic_mtime
#define	i_ctime		i_ic.ic_ctime

#define	i_blocks	i_ic.ic_blocks
#define	i_ordev		i_ic.ic_db[0]	/* was i_oldrdev */
#define	i_gen		i_ic.ic_gen
#define	i_forw		i_chain[0]
#define	i_back		i_chain[1]

#if defined(DEBUG)
#define	i_cksum		i_ic.ic_oeftflag
#define	di_cksum	di_ic.ic_oeftflag
#endif /* DEBUG */

/* EFT transition aids - obsolete */
#define	oEFT_MAGIC	0x90909090
#define	di_mode_reserved di_ic.ic_mode_reserved
#define	di_oeftflag	di_ic.ic_oeftflag

#define	di_ic		di_un.di_icom
#define	di_mode		di_ic.ic_smode
#define	di_nlink	di_ic.ic_nlink
#define	di_uid		di_ic.ic_uid
#define	di_gid		di_ic.ic_gid
#define	di_smode	di_ic.ic_smode
#define	di_suid		di_ic.ic_suid
#define	di_sgid		di_ic.ic_sgid

#if defined(__vax__) || defined(__i386__)
#define	di_size		di_ic.ic_size.val[0]
#endif
#if defined(__mc68000__) || defined(__sparc__) || defined(u3b2) || defined(u3b15)
#define	di_size		di_ic.ic_size.val[1]
#endif
#define	di_db		di_ic.ic_db
#define	di_ib		di_ic.ic_ib

#define	di_atime	di_ic.ic_atime
#define	di_mtime	di_ic.ic_mtime
#define	di_ctime	di_ic.ic_ctime

#define	di_ordev	di_ic.ic_db[0]
#define	di_blocks	di_ic.ic_blocks
#define	di_gen		di_ic.ic_gen

/* flags */
#define	IUPD		0x0001		/* file has been modified */
#define	IACC		0x0002		/* inode access time to be updated */
#define	IMOD		0x0004		/* inode has been modified */
#define	ICHG		0x0008		/* inode has been changed */
#define	INOACC		0x0010		/* no access time update in getpage */
#define	IMODTIME	0x0020		/* mod time already set */
#define	IREF		0x0040		/* inode is being referenced */
#define	ISYNC		0x0080		/* do all allocation synchronously */
#define	IFASTSYMLNK	0x0100		/* fast symbolic link */
#define	IMODACC		0x0200		/* only access time changed; */
					/*   filesystem won't become active */
#define	IATTCHG		0x0400		/* only size/blocks have changed */
#define	IBDWRITE	0x0800		/* the inode has been scheduled for */
					/* write operation asynchrously */
#define	IBAD		0x1000		/* invalid inode contents */

/* modes */
#define	IFMT		0170000		/* type of file */
#define	IFIFO		0010000		/* named pipe (fifo) */
#define	IFCHR		0020000		/* character special */
#define	IFDIR		0040000		/* directory */
#define	IFBLK		0060000		/* block special */
#define	IFREG		0100000		/* regular */
#define	IFLNK		0120000		/* symbolic link */
#define	IFSOCK		0140000		/* socket */

#define	ISUID		04000		/* set user id on execution */
#define	ISGID		02000		/* set group id on execution */
#define	ISVTX		01000		/* save swapped text even after use */
#define	IREAD		0400		/* read, write, execute permissions */
#define	IWRITE		0200
#define	IEXEC		0100

/* specify how the inode info is written in ufs_syncip() */
#define	I_SYNC		1		/* wait for the inode written to disk */
#define	I_DSYNC		2		/* wait for the inode written to disk */
					/* only if IATTCHG is set */
#define	I_ASYNC		0		/* don't wait for the inode written */

/*
 * Statistics on inodes
 * Not protected by locks
 */
struct instats {
	kstat_named_t in_size;		/* current cache size */
	kstat_named_t in_maxsize;	/* maximum cache size */
	kstat_named_t in_hits;		/* cache hits */
	kstat_named_t in_misses;	/* cache misses */
	kstat_named_t in_malloc;	/* kmem_alloce'd */
	kstat_named_t in_mfree;		/* kmem_free'd */
	kstat_named_t in_maxreached;	/* Largest size reached by cache */
	kstat_named_t in_frfront;	/* # put at front of freelist */
	kstat_named_t in_frback;	/* # put at back of freelist */
	kstat_named_t in_dnlclook;	/* # examined in dnlc */
	kstat_named_t in_dnlcpurge;	/* # purged from dnlc */
};

#ifdef _KERNEL
extern int ufs_ninode;			/* high-water mark for inode cache */
struct inode *ufs_inode;		/* the inode table itself */
extern int ufs_allocinode;		/* # inodes actually allocated */
extern int ufs_iincr;			/* number of inodes to alloc at once */

extern struct vnodeops ufs_vnodeops;	/* vnode operations for ufs */

/*
 * Convert between inode pointers and vnode pointers
 */
#define	VTOI(VP)	((struct inode *)(VP)->v_data)
#define	ITOV(IP)	((struct vnode *)&(IP)->i_vnode)

/*
 * convert to fs
 */
#define	ITOF(IP)	((struct fs *)(IP)->i_fs)

/*
 * Convert between vnode types and inode formats
 */
extern enum vtype	iftovt_tab[];

#ifdef notneeded

/* Look at sys/mode.h and os/vnode.c */

extern int		vttoif_tab[];

#endif

/*
 * Mark an inode with the current (unique) timestamp.
 */
struct timeval iuniqtime;

#define	IMARK(ip) ufs_imark(ip)
#define	ITIMES_NOLOCK(ip) ufs_itimes_nolock(ip)

#define	ITIMES(ip) { \
	mutex_enter(&(ip)->i_tlock); \
	ITIMES_NOLOCK(ip); \
	mutex_exit(&(ip)->i_tlock); \
}

/*
 * Allocate the specified block in the inode
 * and make sure any in-core pages are initialized.
 */
#define	BMAPALLOC(ip, off, size, cr) \
	bmap_write((ip), (off), (daddr_t *)NULL, (int *)0, (size), 0, cr)

#define	ESAME	(-1)		/* trying to rename linked files (special) */

/*
 * Check that file is owned by current user or user is su.
 */
#define	OWNER(CR, IP) (((CR)->cr_uid == (IP)->i_uid)? 0: (suser(CR)? 0: EPERM))

#define	UFS_HOLE	(daddr_t)-1	/* value used when no block allocated */

/*
 * enums
 */
enum de_op   { DE_CREATE, DE_MKDIR, DE_LINK, DE_RENAME };  /* direnter ops */
enum dr_op   { DR_REMOVE, DR_RMDIR, DR_RENAME };	   /* dirremove ops */

/*
 * This overlays the fid structure (see vfs.h)
 */
struct ufid {
	u_short	ufid_len;
	ino_t	ufid_ino;
	long	ufid_gen;
};

/*
 * UFS VFS private data.
 */
struct ufsvfs {
	struct vnode	*vfs_root;	/* root vnode			*/
	struct buf	*vfs_bufp;	/* buffer containing superblock */
	struct vnode	*vfs_devvp;	/* block device vnode		*/
	struct inode	*vfs_qinod;	/* QUOTA: pointer to quota file */
	u_short		vfs_qflags;	/* QUOTA: filesystem flags	*/
	u_long		vfs_btimelimit;	/* QUOTA: block time limit	*/
	u_long		vfs_ftimelimit;	/* QUOTA: file time limit	*/
	/*
	 * These are copied from the super block at mount time.
	 */
	long	vfs_nrpos;		/* # rotational positions */
	long	vfs_npsect;		/* # sectors/track including spares */
	long	vfs_interleave;		/* hardware sector interleave */
	long	vfs_tracksew;		/* sector 0 skew, per track */
	/*
	 * This lock protects cg's and super block pointed at by
	 * vfs_bufp->b_fs.
	 */
	kmutex_t vfs_lock;		/*
					 * Locks contents of fs and cg's
					 * and contents of vfs_dio
					 */
	struct	ulockfs	vfs_ulockfs;	/* ufs lockfs support */
	u_long	vfs_dio;		/* delayed io (_FIODIO) */
	u_long	vfs_nointr;		/* disallow lockfs interrupts */
	/*
	 * Some useful constants
	 */
	int	vfs_nindirshift;	/* calc. from fs_nindir */
	int	vfs_nindiroffset;	/* calc. from fs_ninidr */
	int	vfs_rdclustsz;		/* bytes in read cluster */
	int	vfs_wrclustsz;		/* bytes in write cluster */

	/*
	 * for sanity checking
	 */
	long	*vfs_cylseq;		/* ptr to array of cyl seq. nums */
};

#define	vfs_fs	vfs_bufp->b_un.b_fs

/* inohsz is guaranteed to be a power of 2 */
#define	INOHASH(dev, ino)	(hash2ints((int)dev, (int)ino) & (inohsz - 1))

union ihead {
	union	ihead	*ih_head[2];
	struct	inode	*ih_chain[2];
};

extern	union	ihead	*ihead;
extern  kmutex_t	*ih_lock;
extern	int	inohsz;

#endif /* _KERNEL */

/*
 * ufs function prototypes
 */
#if defined(_KERNEL)

extern	int	ufs_iget(struct vfs *, struct fs *, ino_t,
    struct inode **, struct cred *);
extern	void	ufs_iinactive(struct inode *, struct cred *);
extern	void	ufs_iupdat(struct inode *, int);
extern	void	ufs_iput(struct inode *);
extern	void	irele(struct inode *);
extern	void	idrop(struct inode *);
extern	void	ufs_iaddfree(struct inode *);
extern	void	ufs_irmfree(struct inode *);
extern	void	ufs_delcache(struct inode *);
extern	int	ufs_itrunc(struct inode *, u_long, struct cred *);
extern	int	ufs_iaccess(struct inode *, int, struct cred *);
#ifdef QUOTA
extern	int	ufs_iflush(struct vfs *, struct inode *);
#else
extern	int	ufs_iflush(struct vfs *);
#endif

extern void	ufs_imark(struct inode *);
extern void	ufs_itimes_nolock(struct inode *);

extern	int	ufs_dirlook(struct inode *, char *, struct inode **,
    struct cred *);
extern	int	ufs_direnter(struct inode *, char *, enum de_op, struct inode *,
    struct inode *, struct vattr *, struct inode **, struct cred *);
extern	int	ufs_dirremove(struct inode *, char *, struct inode *,
    struct vnode *, enum dr_op, struct cred *);

extern	void	sbupdate(struct vfs *);

extern	int	ufs_ialloc(struct inode *, ino_t, mode_t, struct inode **,
    struct cred *);
extern	void	ufs_ifree(struct inode *, ino_t, mode_t);
extern	void	free(struct inode *, daddr_t, off_t);
extern	int	alloc(struct inode *, daddr_t, int, daddr_t *, struct cred *);
extern	int	realloccg(struct inode *, daddr_t, daddr_t, int, int,
    daddr_t *, struct cred*);
extern	int	ufs_freesp(struct vnode *, struct flock *, int, struct cred *);
extern	ino_t	dirpref(struct ufsvfs *);
extern	daddr_t	blkpref(struct inode *, daddr_t, int, daddr_t *);

extern	int	ufs_rdwri(enum uio_rw, struct inode *, caddr_t, int, off_t,
    enum uio_seg, int *, struct cred *);
extern	int	bmap_read(struct inode *, u_int, daddr_t *, int *);
extern	int	bmap_write(struct inode *, u_int, daddr_t *, int *, int,
    int, struct cred *);
extern	int	bmap_has_holes(struct inode *);

extern	void	ufs_sbwrite(struct ufsvfs *);
extern	void	ufs_update(int);
extern	void	ufs_flushi(int);
extern	int	ufs_getsummaryinfo(dev_t, struct fs *);
extern	int	ufs_syncip(struct inode *, int, int);
extern	int	ufs_badblock(struct inode *, daddr_t);
extern	void	ufs_notclean(struct ufsvfs *);
extern	void	ufs_checkclean(struct vfs *, struct ufsvfs *, dev_t,
    timestruc_t *);
extern	int	isblock(struct fs *, unsigned char *, daddr_t);
extern	void	setblock(struct fs *, unsigned char *, daddr_t);
extern	void	clrblock(struct fs *, u_char *, daddr_t);
extern	int	isclrblock(struct fs *, u_char *, daddr_t);
extern	void	fragacct(struct fs *, int, long *, int);
extern	int	skpc(char, u_int, char *);
extern	int	scanc(u_int, u_char *, u_char *, u_char);
extern	int	ufs_fbwrite(struct fbuf *, struct inode *);
extern	int	ufs_fbiwrite(struct fbuf *, struct inode *, daddr_t, long);
extern	int	ufs_indirblk_sync(struct inode *, u_int);
extern	int	ufs_sync_indir(struct inode *);
extern	int	ufs_putapage(struct vnode *, struct page *, u_int *, u_int *,
    int, struct cred *);

#if defined(DEBUG)
extern	int	ufs_indir_badblock(struct inode *, daddr_t *);
extern	int	ufs_ivalid(struct inode *);
extern	int	ufs_cksum(char *, int, void *, struct fs *);
extern	ulong	ufs_icksum(struct inode *);
extern	ulong	ufs_cgcksum(struct cg *, long);
extern	int	ufs_cgseq(char *, struct cg *, int, long *, struct fs *);
extern	int	ufs_cgsane(char *, struct cg *, int, struct fs *);
extern	int	ufs_reconcile_cgseq(struct ufsvfs *);

#define	CK_CG	1	/* do cylinder group checksums */
#define	CK_INO	2	/* do inode checksums */

#define	SET	0	/* for sequence check/set routines */
#define	CHK	1

#define	set_cgcksum(cgp, size)	(cgp)->cg_cksum = ufs_cgcksum(cgp, size)
#define	set_icksum(ip)		(ip)->i_cksum = ufs_icksum(ip)

#define	UDBG_OFF	0x000	/* no ufs debugging */
#define	UDBG_PANIC	0x001	/* if any problem found, panic */
#define	UDBG_MNTFAIL	0x002	/* (debug) mount errors cause it to fail */
#define	UDBG_ICKSUM	0x010	/* do inode checksumming */
#define	UDBG_IVALID	0x020	/* do inode validity checking */
#define	UDBG_CGCKSUM	0x100	/* do cylinder group checksumming */
#define	UDBG_CGSEQ	0x200	/* do cylinder group seq. no. checking */
#define	UDBG_CGSANE	0x400	/* do cylinder group sanity checking */

extern int ufs_debug;

#endif /* DEBUG */

#endif	/* defined(_KERNEL) */

#ifdef	__cplusplus
}
#endif

#endif	/* _SYS_FS_UFS_INODE_H */

/*	Copyright (c) 1988 AT&T	*/
/*	  All Rights Reserved  	*/

/*	THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF AT&T	*/
/*	The copyright notice above does not evidence any   	*/
/*	actual or intended publication of such source code.	*/

#ifndef _SETJMP_H
#define	_SETJMP_H

#pragma ident	"@(#)setjmp.h	1.13	93/03/10 SMI"	/* SVr4.0 1.9.2.9 */

#include <sys/feature_tests.h>

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef _JBLEN

#if defined(__STDC__)

#if defined(__i386__)
#define	_SIGJBLEN 128	/* (sizeof(ucontext_t) / sizeof (int)) */
#elif defined(__sparc__)
#define	_SIGJBLEN 19	/* ABI value */
#else
#define	_SIGJBLEN 64	/* (sizeof(ucontext_t) / sizeof (int)) */
#endif

#if defined(__pdp11__)
#define	_JBLEN  3
#elif defined(__sparc__)
#define	_JBLEN  12	/* ABI value */
#elif defined(__u370__)
#define	_JBLEN  4
#elif defined(__u3b__)
#define	_JBLEN  11
#else
#define	_JBLEN  10
#endif	/* #machine */

#else

#if __i386__
#define	_SIGJBLEN 128	/* (sizeof(ucontext_t) / sizeof (int)) */
#elif __sparc__
#define	_SIGJBLEN 19	/* ABI value */
#else
#define	_SIGJBLEN 64	/* (sizeof(ucontext_t) / sizeof (int)) */
#endif

#if pdp11
#define	_JBLEN  3
#elif __sparc__
#define	_JBLEN  12	/* ABI value */
#elif u370
#define	_JBLEN  4
#elif u3b
#define	_JBLEN  11
#else
#define	_JBLEN  10
#endif

#endif	/* __STDC__ */

typedef int jmp_buf[_JBLEN];

#if defined(__STDC__)
extern int setjmp(jmp_buf);
#pragma unknown_control_flow(setjmp)
extern void longjmp(jmp_buf, int);

#if !defined (__STRICT_ANSI__) || defined(_POSIX_C_SOURCE) || defined(_XOPEN_SOURCE)
/* non-ANSI standard compilation */

typedef int sigjmp_buf[_SIGJBLEN];

extern int sigsetjmp(sigjmp_buf, int);
#pragma unknown_control_flow(sigsetjmp)
extern void siglongjmp(sigjmp_buf, int);
#endif

#if defined (__STRICT_ANSI__)
#define	setjmp(env)	setjmp(env)
#endif

#else
typedef int sigjmp_buf[_SIGJBLEN];

extern int setjmp();
#pragma unknown_control_flow(setjmp)
extern void longjmp();
extern int sigsetjmp();
#pragma unknown_control_flow(sigsetjmp)
extern void siglongjmp();

#endif  /* __STDC__ */

#endif  /* _JBLEN */

#ifdef	__cplusplus
}
#endif

#endif	/* _SETJMP_H */

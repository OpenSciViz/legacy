/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T	*/
/*	  All Rights Reserved  	*/

/*	THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF AT&T	*/
/*	The copyright notice above does not evidence any   	*/
/*	actual or intended publication of such source code.	*/

/*
 *		PROPRIETARY NOTICE (Combined)
 *
 *  This source code is unpublished proprietary information
 *  constituting, or derived under license from AT&T's Unix(r) System V.
 *
 *
 *
 *		Copyright Notice
 *
 *  Notice of copyright on this source code product does not indicate
 *  publication.
 *
 *	(c) 1986,1987,1988,1989,1990,1991  Sun Microsystems, Inc.
 *	(c) 1983,1984,1985,1986,1987,1988,1989  AT&T.
 *		All rights reserved.
 */

#ifndef	_NFS_NFS_H
#define	_NFS_NFS_H

#pragma ident	"@(#)nfs.h	1.23	93/07/21 SMI"	/* SVr4.0 1.5	*/
/*	nfs.h 2.38 88/08/19 SMI 	*/

#include <sys/vfs.h>
#include <sys/stream.h>
#include <sys/kstat.h>
#include <rpc/types.h>
#include <rpc/xdr.h>
#include <nfs/export.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* Maximum size of data portion of a remote request */
#define	NFS_MAXDATA	8192
#define	NFS_MAXNAMLEN	255
#define	NFS_MAXPATHLEN	1024

/*
 * Rpc retransmission parameters
 */
#define	NFS_TIMEO	11	/* initial timeout in tenths of a sec. */
#define	NFS_RETRIES	5	/* times to retry request */

/*
 * maximum transfer size for different interfaces
 */
#define	ECTSIZE	2048
#define	IETSIZE	8192

/*
 * Error status
 * Should include all possible net errors.
 * For now we just cast errno into an enum nfsstat.
 */
enum nfsstat {
	NFS_OK = 0,			/* no error */
	NFSERR_PERM = 1,		/* Not owner */
	NFSERR_NOENT = 2,		/* No such file or directory */
	NFSERR_IO = 5,			/* I/O error */
	NFSERR_NXIO = 6,		/* No such device or address */
	NFSERR_ACCES = 13,		/* Permission denied */
	NFSERR_EXIST = 17,		/* File exists */
	NFSERR_NODEV = 19,		/* No such device */
	NFSERR_NOTDIR = 20,		/* Not a directory */
	NFSERR_ISDIR = 21,		/* Is a directory */
	NFSERR_FBIG = 27,		/* File too large */
	NFSERR_NOSPC = 28,		/* No space left on device */
	NFSERR_ROFS = 30,		/* Read-only file system */
	NFSERR_OPNOTSUPP = 45,		/* Operation not supported */
	NFSERR_NAMETOOLONG = 63,	/* File name too long */
	NFSERR_NOTEMPTY = 66,		/* Directory not empty */
	NFSERR_DQUOT = 69,		/* Disc quota exceeded */
	NFSERR_STALE = 70,		/* Stale NFS file handle */
	NFSERR_WFLUSH			/* write cache flushed */
};

/*
 * File types
 */
enum nfsftype {
	NFNON,
	NFREG,		/* regular file */
	NFDIR,		/* directory */
	NFBLK,		/* block special */
	NFCHR,		/* character special */
	NFLNK		/* symbolic link */
};

/*
 * Special kludge for fifos (named pipes)  [to adhere to NFS Protocol Spec]
 *
 * VFIFO is not in the protocol spec (VNON will be replaced by VFIFO)
 * so the over-the-wire representation is VCHR with a '-1' device number.
 *
 * NOTE: This kludge becomes unnecessary with the Protocol Revision,
 *	 but it may be necessary to support it (backwards compatibility).
 */
#define	NFS_FIFO_TYPE	NFCHR
#define	NFS_FIFO_MODE	S_IFCHR
/* #define	NFS_FIFO_DEV	(~0) */
#define	NFS_FIFO_DEV	((u_long)-1)

/* identify fifo in nfs attributes */
#define	NA_ISFIFO(NA)	(((NA)->na_type == NFS_FIFO_TYPE) && \
			    ((NA)->na_rdev == NFS_FIFO_DEV))

/* set fifo in nfs attributes */
#define	NA_SETFIFO(NA)	{ \
			(NA)->na_type = NFS_FIFO_TYPE; \
			(NA)->na_rdev = NFS_FIFO_DEV; \
			(NA)->na_mode = ((NA)->na_mode&~S_IFMT)|NFS_FIFO_MODE; \
			}


/*
 * Size of an fhandle in bytes
 */
#define	NFS_FHSIZE	32

struct nfs_fid {
	u_short	nf_len;
	u_short	nf_pad;
	char	nf_data[NFS_FHSIZE];
};

/*
 * File access handle
 * This structure is the Sun server representation of a file.
 * It is handed out by a server for the client to use in further
 * file transactions.
 */

/*
 * This struct is only used to find the size of the data field in the
 * fhandle structure below.
 */
struct fhsize {
	fsid_t	f1;
	u_short	f2;
	char	f3[4];
	u_short	f4;
	char	f5[4];
};
#define	NFS_FHMAXDATA	((NFS_FHSIZE - sizeof (struct fhsize) + 8) / 2)

struct svcfh {
	fsid_t	fh_fsid;			/* filesystem id */
	u_short	fh_len;				/* file number length */
	char	fh_data[NFS_FHMAXDATA];		/* and data */
	u_short	fh_xlen;			/* export file number length */
	char	fh_xdata[NFS_FHMAXDATA];	/* and data */
};

typedef struct svcfh fhandle_t;

/*
 * Arguments to remote write and writecache
 */
/*
 * The `over the wire' representation of the first four arguments.
 */
struct otw_nfswriteargs {
	fhandle_t	otw_wa_fhandle;
	u_long		otw_wa_begoff;
	u_long		otw_wa_offset;
	u_long		otw_wa_totcount;
};

struct nfswriteargs {
	struct otw_nfswriteargs *wa_args;	/* ptr to the otw arguments */
	struct otw_nfswriteargs wa_args_buf;	/* space for otw arguments */
	u_long		wa_count;
	char		*wa_data;	/* data to write (up to NFS_MAXDATA) */
	mblk_t		*wa_mblk;	/* pointer to mblks containing data */
};
#define	wa_fhandle	wa_args->otw_wa_fhandle
#define	wa_begoff	wa_args->otw_wa_begoff
#define	wa_offset	wa_args->otw_wa_offset
#define	wa_totcount	wa_args->otw_wa_totcount


/*
 * File attributes
 */
struct nfsfattr {
	enum nfsftype	na_type;	/* file type */
	u_long		na_mode;	/* protection mode bits */
	u_long		na_nlink;	/* # hard links */
	u_long		na_uid;		/* owner user id */
	u_long		na_gid;		/* owner group id */
	u_long		na_size;	/* file size in bytes */
	u_long		na_blocksize;	/* prefered block size */
	u_long		na_rdev;	/* special device # */
	u_long		na_blocks;	/* Kb of disk used by file */
	u_long		na_fsid;	/* device # */
	u_long		na_nodeid;	/* inode # */
	struct timeval	na_atime;	/* time of last access */
	struct timeval	na_mtime;	/* time of last modification */
	struct timeval	na_ctime;	/* time of last change */
};

#define	n2v_type(x)	(NA_ISFIFO(x) ? VFIFO : (enum vtype)((x)->na_type))
#define	n2v_rdev(x)	(NA_ISFIFO(x) ? 0 : (x)->na_rdev)

/*
 * Arguments to remote read
 */
struct nfsreadargs {
	fhandle_t	ra_fhandle;	/* handle for file */
	u_long		ra_offset;	/* byte offset in file */
	u_long		ra_count;	/* immediate read count */
	u_long		ra_totcount;	/* total read cnt (from this offset) */
};

/*
 * Status OK portion of remote read reply
 */
struct nfsrrok {
	struct nfsfattr	rrok_attr;	/* attributes, need for pagin */
	u_long		rrok_count;	/* bytes of data */
	char		*rrok_data;	/* data (up to NFS_MAXDATA bytes) */
	char		*rrok_map;	/* pointer to mapped data */
	struct vnode	*rrok_vp;	/* vnode assoc. with mapping */
	krwlock_t	*rrok_lockp;	/* wrap lock assoc. with mapping */
};

/*
 * Reply from remote read
 */
struct nfsrdresult {
	enum nfsstat	rr_status;		/* status of read */
	union {
		struct nfsrrok	rr_ok_u;	/* attributes, need for pagin */
	} rr_u;
};
#define	rr_ok		rr_u.rr_ok_u
#define	rr_attr		rr_u.rr_ok_u.rrok_attr
#define	rr_count	rr_u.rr_ok_u.rrok_count
#define	rr_data		rr_u.rr_ok_u.rrok_data
#define	rr_map		rr_u.rr_ok_u.rrok_map
#define	rr_vp		rr_u.rr_ok_u.rrok_vp
#define	rr_lockp	rr_u.rr_ok_u.rrok_lockp


/*
 * File attributes which can be set
 */
struct nfssattr {
	u_long		sa_mode;	/* protection mode bits */
	u_long		sa_uid;		/* owner user id */
	u_long		sa_gid;		/* owner group id */
	u_long		sa_size;	/* file size in bytes */
	struct timeval	sa_atime;	/* time of last access */
	struct timeval	sa_mtime;	/* time of last modification */
};


/*
 * Reply status with file attributes
 */
struct nfsattrstat {
	enum nfsstat	ns_status;		/* reply status */
	union {
		struct nfsfattr ns_attr_u;	/* NFS_OK: file attributes */
	} ns_u;
};
#define	ns_attr	ns_u.ns_attr_u


/*
 * NFS_OK part of read sym link reply union
 */
struct nfssrok {
	u_long	srok_count;	/* size of string */
	char	*srok_data;	/* string (up to NFS_MAXPATHLEN bytes) */
};

/*
 * Result of reading symbolic link
 */
struct nfsrdlnres {
	enum nfsstat	rl_status;		/* status of symlink read */
	union {
		struct nfssrok	rl_srok_u;	/* name of linked to */
	} rl_u;
};
#define	rl_srok		rl_u.rl_srok_u
#define	rl_count	rl_u.rl_srok_u.srok_count
#define	rl_data		rl_u.rl_srok_u.srok_data


/*
 * Arguments to readdir
 */
struct nfsrddirargs {
	fhandle_t rda_fh;	/* directory handle */
	u_long rda_offset;	/* offset in directory (opaque) */
	u_long rda_count;	/* number of directory bytes to read */
};

/*
 * NFS_OK part of readdir result
 */
struct nfsrdok {
	u_long	rdok_offset;		/* next offset (opaque) */
	u_long	rdok_size;		/* size in bytes of entries */
	bool_t	rdok_eof;		/* true if last entry is in result */
	struct dirent *rdok_entries;	/* variable number of entries */
};

/*
 * Readdir result
 */
struct nfsrddirres {
	enum nfsstat	rd_status;
	u_long		rd_bufsize;	/* client request size (not xdr'ed) */
	union {
		struct nfsrdok rd_rdok_u;
	} rd_u;
};
#define	rd_rdok		rd_u.rd_rdok_u
#define	rd_offset	rd_u.rd_rdok_u.rdok_offset
#define	rd_size		rd_u.rd_rdok_u.rdok_size
#define	rd_eof		rd_u.rd_rdok_u.rdok_eof
#define	rd_entries	rd_u.rd_rdok_u.rdok_entries


/*
 * Arguments for directory operations
 */
struct nfsdiropargs {
	fhandle_t	*da_fhandle;	/* pointer to directory file handle */
	char		*da_name;	/* name (up to NFS_MAXNAMLEN bytes) */
	fhandle_t	da_fhandle_buf;	/* directory file handle */
	int		da_flags;	/* flags, see below */
};
#define	DA_FREENAME	1

/*
 * NFS_OK part of directory operation result
 */
struct  nfsdrok {
	fhandle_t	drok_fhandle;	/* result file handle */
	struct nfsfattr	drok_attr;	/* result file attributes */
};

/*
 * Results from directory operation
 */
struct  nfsdiropres {
	enum nfsstat	dr_status;	/* result status */
	union {
		struct  nfsdrok	dr_drok_u;	/* NFS_OK result */
	} dr_u;
};
#define	dr_drok		dr_u.dr_drok_u
#define	dr_fhandle	dr_u.dr_drok_u.drok_fhandle
#define	dr_attr		dr_u.dr_drok_u.drok_attr

/*
 * arguments to setattr
 */
struct nfssaargs {
	fhandle_t	saa_fh;		/* fhandle of file to be set */
	struct nfssattr	saa_sa;		/* new attributes */
};

/*
 * arguments to create and mkdir
 */
struct nfscreatargs {
	struct nfsdiropargs	ca_da;	/* file name to create and parent dir */
	struct nfssattr		*ca_sa;	/* initial attributes */
	struct nfssattr		ca_sa_buf;	/* space to store attributes */
};

/*
 * arguments to link
 */
struct nfslinkargs {
	fhandle_t		*la_from;	/* old file */
	fhandle_t		la_from_buf;	/* old file */
	struct nfsdiropargs	la_to;		/* new file and parent dir */
};

/*
 * arguments to rename
 */
struct nfsrnmargs {
	struct nfsdiropargs rna_from;	/* old file and parent dir */
	struct nfsdiropargs rna_to;	/* new file and parent dir */
};

/*
 * arguments to symlink
 */
struct nfsslargs {
	struct nfsdiropargs	sla_from;	/* old file and parent dir */
	char			*sla_tnm;	/* new name */
	int			sla_tnm_flags;	/* flags for name */
	struct nfssattr		*sla_sa;	/* attributes */
	struct nfssattr		sla_sa_buf;	/* attributes buffer */
};
#define	SLA_FREETNM	1

/*
 * NFS_OK part of statfs operation
 */
struct nfsstatfsok {
	u_long fsok_tsize;	/* preferred transfer size in bytes */
	u_long fsok_bsize;	/* fundamental file system block size */
	u_long fsok_blocks;	/* total blocks in file system */
	u_long fsok_bfree;	/* free blocks in fs */
	u_long fsok_bavail;	/* free blocks avail to non-superuser */
};

/*
 * Results of statfs operation
 */
struct nfsstatfs {
	enum nfsstat	fs_status;	/* result status */
	union {
		struct	nfsstatfsok fs_fsok_u;	/* NFS_OK result */
	} fs_u;
};
#define	fs_fsok		fs_u.fs_fsok_u
#define	fs_tsize	fs_u.fs_fsok_u.fsok_tsize
#define	fs_bsize	fs_u.fs_fsok_u.fsok_bsize
#define	fs_blocks	fs_u.fs_fsok_u.fsok_blocks
#define	fs_bfree	fs_u.fs_fsok_u.fsok_bfree
#define	fs_bavail	fs_u.fs_fsok_u.fsok_bavail

/*
 * XDR routines for handling structures defined above
 */
#ifdef __STDC__
extern bool_t	xdr_attrstat(XDR *, struct nfsattrstat *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastattrstat(XDR *, struct nfsattrstat *);
#endif
extern bool_t	xdr_creatargs(XDR *, struct nfscreatargs *);
extern bool_t	xdr_fastcreatargs(XDR *, struct nfscreatargs **);
extern bool_t	xdr_diropargs(XDR *, struct nfsdiropargs *);
extern bool_t	xdr_fastdiropargs(XDR *, struct nfsdiropargs **);
extern bool_t	xdr_diropres(XDR *, struct nfsdiropres *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastdiropres(XDR *, struct nfsdiropres *);
#endif
extern bool_t	xdr_drok(XDR *, struct nfsdrok *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastdrok(XDR *, struct nfsdrok *);
#endif
extern bool_t	xdr_fattr(XDR *, struct nfsfattr *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastfattr(XDR *, struct nfsfattr *);
#endif
extern bool_t	xdr_fhandle(XDR *, fhandle_t *);
extern bool_t	xdr_fastfhandle(XDR *, fhandle_t **);
extern bool_t	xdr_linkargs(XDR *, struct nfslinkargs *);
extern bool_t	xdr_fastlinkargs(XDR *, struct nfslinkargs **);
extern bool_t	xdr_rddirargs(XDR *, struct nfsrddirargs *);
extern bool_t	xdr_fastrddirargs(XDR *, struct nfsrddirargs **);
extern bool_t	xdr_putrddirres(XDR *, struct nfsrddirres *);
extern bool_t	xdr_getrddirres(XDR *, struct nfsrddirres *);
extern bool_t	xdr_rdlnres(XDR *, struct nfsrdlnres *);
extern bool_t	xdr_rdresult(XDR *, struct nfsrdresult *);
extern bool_t	xdr_readargs(XDR *, struct nfsreadargs *);
extern bool_t	xdr_fastreadargs(XDR *, struct nfsreadargs **);
extern bool_t	xdr_rnmargs(XDR *, struct nfsrnmargs *);
extern bool_t	xdr_fastrnmargs(XDR *, struct nfsrnmargs **);
extern bool_t	xdr_rrok(XDR *, struct nfsrrok *);
extern bool_t	xdr_saargs(XDR *, struct nfssaargs *);
extern bool_t	xdr_fastsaargs(XDR *, struct nfssaargs **);
extern bool_t	xdr_sattr(XDR *, struct nfssattr *);
extern bool_t	xdr_slargs(XDR *, struct nfsslargs *);
extern bool_t	xdr_fastslargs(XDR *, struct nfsslargs **);
extern bool_t	xdr_srok(XDR *, struct nfssrok *);
extern bool_t	xdr_timeval(XDR *, struct timeval *);
extern bool_t	xdr_writeargs(XDR *, struct nfswriteargs *);
extern bool_t	xdr_fastwriteargs(XDR *, struct nfswriteargs **);
extern bool_t	xdr_fsok(XDR *, struct nfsstatfsok *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastfsok(XDR *, struct nfsstatfsok *);
#endif
extern bool_t	xdr_statfs(XDR *, struct nfsstatfs *);
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_faststatfs(XDR *, struct nfsstatfs *);
extern bool_t	xdr_fastenum(XDR *, enum_t *);
#endif
#else
extern bool_t	xdr_attrstat();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastattrstat();
#endif
extern bool_t	xdr_creatargs();
extern bool_t	xdr_fastcreatargs();
extern bool_t	xdr_diropargs();
extern bool_t	xdr_fastdiropargs();
extern bool_t	xdr_diropres();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastdiropres();
#endif
extern bool_t	xdr_drok();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastdrok();
#endif
extern bool_t	xdr_fattr();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastfattr();
#endif
extern bool_t	xdr_fhandle();
extern bool_t	xdr_fastfhandle();
extern bool_t	xdr_linkargs();
extern bool_t	xdr_fastlinkargs();
extern bool_t	xdr_rddirargs();
extern bool_t	xdr_fastrddirargs();
extern bool_t	xdr_putrddirres();
extern bool_t	xdr_getrddirres();
extern bool_t	xdr_rdlnres();
extern bool_t	xdr_rdresult();
extern bool_t	xdr_readargs();
extern bool_t	xdr_fastreadargs();
extern bool_t	xdr_rnmargs();
extern bool_t	xdr_fastrnmargs();
extern bool_t	xdr_rrok();
extern bool_t	xdr_saargs();
extern bool_t	xdr_fastsaargs();
extern bool_t	xdr_sattr();
extern bool_t	xdr_slargs();
extern bool_t	xdr_fastslargs();
extern bool_t	xdr_srok();
extern bool_t	xdr_timeval();
extern bool_t	xdr_writeargs();
extern bool_t	xdr_fastwriteargs();
extern bool_t	xdr_fsok();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_fastfsok();
#endif
extern bool_t	xdr_statfs();
#if defined(__vax__) || defined(__i386__)
extern bool_t	xdr_faststatfs();
extern bool_t	xdr_fastenum();
#endif
#endif

/*
 * Remote file service routines
 */
#define	RFS_NULL	0
#define	RFS_GETATTR	1
#define	RFS_SETATTR	2
#define	RFS_ROOT	3
#define	RFS_LOOKUP	4
#define	RFS_READLINK	5
#define	RFS_READ	6
#define	RFS_WRITECACHE	7
#define	RFS_WRITE	8
#define	RFS_CREATE	9
#define	RFS_REMOVE	10
#define	RFS_RENAME	11
#define	RFS_LINK	12
#define	RFS_SYMLINK	13
#define	RFS_MKDIR	14
#define	RFS_RMDIR	15
#define	RFS_READDIR	16
#define	RFS_STATFS	17
#define	RFS_NPROC	18

/*
 * remote file service numbers
 */
#define	NFS_PROGRAM	((u_long)100003)
#define	NFS_VERSION	((u_long)2)
#define	NFS_PORT	2049

#ifdef _KERNEL
/*	function defs for NFS kernel */
extern int	nfs_validate_caches(struct vnode *, cred_t *);
extern void	nfs_purge_caches(struct vnode *);
extern void	nfs_cache_check(struct vnode *, timestruc_t, u_long);
extern void	nfs_attrcache(struct vnode *, struct nfsfattr *);
extern void	nfs_attrcache_va(struct vnode *, struct vattr *);
extern int	nfs_getattr_otw(struct vnode *, struct vattr *, cred_t *);
extern int	nfs_fhhash(fsid_t *, fid_t *);
extern int	nfsgetattr(struct vnode *, struct vattr *, cred_t *);
extern void	nattr_to_vattr(struct vnode *, struct nfsfattr *,
				struct vattr *);
extern int	nfs_clntinit(void);
extern int	nfstsize(void);
extern void	vattr_to_nattr(struct vattr *, struct nfsfattr *);
extern int	nfs_exportinit(void);
extern int	makefh(fhandle_t *, struct vnode *, struct exportinfo *);
extern struct exportinfo *findexport(fsid_t *, struct fid *);
extern void	export_rw_exit(void);
extern int	nfs_srvinit(void);
extern void	vattr_to_sattr(struct vattr *, struct nfssattr *);
extern void	setdiropargs(struct nfsdiropargs *, char *, struct vnode *);
extern int	setdirgid(struct vnode *, cred_t *);
extern u_int	setdirmode(struct vnode *, u_int);
extern char *	newname(void);
extern int	nfs_subrinit(void);
extern enum nfsstat puterrno(int);
extern int	geterrno(enum nfsstat);
extern int	nfsinit(struct vfssw *, int);
extern int	nfs_vfsinit(void);
extern int	nfs_putpage(struct vnode *, offset_t, u_int, int, cred_t *);
extern void	nfs_async_stop(struct vfs *);
extern int	nfs_dump(vnode_t *, caddr_t, int, int);

void nfs_perror(int error, char *fmt, ...);
void nfs_cmn_err(int error, int level, char *fmt, ...);

extern struct vfsops	nfs_vfsops;
extern struct vnodeops	nfs_vnodeops;
extern kmutex_t		nfs_rtable_lock;
extern kmutex_t		nfs_minor_lock;
extern int 		nfsdump_maxcount;
extern int 		module_keepcnt;
extern fhandle_t 	nfsdump_fhandle;
extern struct netbuf 	nfsdump_addr;
extern struct knetconfig nfsdump_cf;
extern int		nfs_major;
extern kstat_named_t	*clstat_ptr;
extern kstat_named_t	*svstat_ptr;
extern ulong_t		clstat_ndata;
extern ulong_t		svstat_ndata;
extern kmutex_t		nfs_flock_lock;
#endif	/* _KERNEL */

#ifdef	__cplusplus
}
#endif

#endif	/* _NFS_NFS_H */

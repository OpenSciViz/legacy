/*
 * Copyright (c) 1992 by Sun Microsystems, Inc.
 */

#ifndef _cl_system_parms_h
#define _cl_system_parms_h

#pragma	ident	"@(#)cl_system_parms.h	1.48	93/01/27 SMI"

#include <cl_base_system_parms.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define	SYS_CLASS	"system"
#define	CL_SYSTEM	SYS_CLASS

/*
 * Methods
 */
#define SYS_SETUP_SERVER_INFO_MTHD	"setup_server_info"
#define SYS_CUSTOMIZE_CLIENT_ENV_MTHD	"customize_client_env"
#define SYS_CUSTOMIZE_4XCLIENT_ENV_MTHD	"customize_4xclient_env"	/* Multi-os */
#define SYS_CUSTOMIZE_INSTALL_CLIENT_ENV_MTHD	"customize_install_client_env"
#define SYS_CONFIG_SYSTEM_MTHD		"config_system"
#define SYS_UNCONFIG_SYSTEM_MTHD	"unconfig_system"
#define SYS_CHECK_SERVER_STATUS_MTHD	"check_server_status"
#define SYS_CHECK_INSTALL_HIERARCHY_MTHD	"check_install_hierarchy"
#define SYS_CREATE_CLIENT_ENV_MTHD	"create_client_env"
#define SYS_CREATE_INSTALL_CLIENT_ENV_MTHD	"create_install_client_env"
#define SYS_ADDCLIENT_MTHD		"addclient"
#define SYS_ADD_REMOTEINSTALL_PRIVS_MTHD	"add_remoteinstall_privs"
#define SYS_SHOW_CLIENT_PROPS_MTHD	"show_client_props"
#define SYS_SHOW_ALL_CLIENTS_MTHD	"show_all_clients"
#define SYS_RMCLIENT_MTHD		"rmclient"
#define SYS_RM_CLIENT_ENV_MTHD		"rm_client_env"
#define SYS_RM_SERVER_INFO_MTHD		"rm_server_info"
#define SYS_GET_BEST_NET_IF_MTHD	"get_best_net_if"
#define SYS_GET_NET_IF_NAMES_MTHD	BSYS_GET_NET_IF_NAMES_MTHD
#define SYS_SET_NET_IF_IP_ADDR_MTHD	BSYS_SET_NET_IF_IP_ADDR_MTHD
#define SYS_GET_NET_IF_IP_ADDR_MTHD	BSYS_GET_NET_IF_IP_ADDR_MTHD
#define SYS_GET_NET_IF_IP_NETMASK_MTHD	BSYS_GET_NET_IF_IP_NETMASK_MTHD
#define SYS_SET_NET_IF_IP_NETMASK_MTHD	BSYS_SET_NET_IF_IP_NETMASK_MTHD
#define SYS_SET_DEFAULT_IP_ROUTE_MTHD	BSYS_SET_DEFAULT_IP_ROUTE_MTHD
#define SYS_GET_DEFAULT_IP_ROUTE_MTHD	BSYS_GET_DEFAULT_IP_ROUTE_MTHD
#define SYS_SET_NODENAME_MTHD		BSYS_SET_NODENAME_MTHD
#define SYS_GET_NODENAME_MTHD		BSYS_GET_NODENAME_MTHD
#define SYS_SET_DOMAIN_MTHD		BSYS_SET_DOMAIN_MTHD
#define SYS_GET_DOMAIN_MTHD		BSYS_GET_DOMAIN_MTHD
#define SYS_GET_DATE_MTHD		BSYS_GET_DATE_MTHD
#define SYS_SET_DATE_MTHD		BSYS_SET_DATE_MTHD
#define SYS_SYNC_DATE_MTHD		BSYS_SYNC_DATE_MTHD
#define SYS_SET_TIMEZONE_MTHD		BSYS_SET_TIMEZONE_MTHD
#define SYS_GET_TIMEZONE_MTHD		BSYS_GET_TIMEZONE_MTHD
#define SYS_GET_NET_IF_ETHER_ADDR_MTHD	BSYS_GET_NET_IF_ETHER_ADDR_MTHD
#define SYS_GET_RMINSTALL_MOUNTPTS_MTHD	"get_rminstall_mountpts"
#define SYS_GET_SYSINFO_MTHD		BSYS_GET_SYSINFO_MTHD
#define SYS_GET_KERNEL_REV_MTHD		"get_kernel_rev"
#define SYS_FIND_FILE_IN_PATH_MTHD	"find_file_in_path"
#define SYS_GET_FILE_TYPE_MTHD		"get_file_type"
#define SYS_GET_ELF_LIBS_MTHD		"get_elf_libs"
#define SYS_GET_COMMAND_REV_MTHD	"get_command_rev"
#define SYS_GET_FILE_PERMISSIONS_MTHD	"get_file_permissions"
#define SYS_GET_FILE_CHECKSUM_MTHD	"get_file_checksum"
#define SYS_GET_OPENWIN_REV_MTHD	"get_openwin_rev"
#define SYS_SHOWREV_MTHD		"showrev"
#define SYS_SET_RUN_LEVEL_MTHD		BSYS_SET_RUN_LEVEL_MTHD
#define SYS_UNCONFIG_FILES_MTHD		"unconfig_files"
#define SYS_MOUNTFS_MTHD		"mountfs"
#define SYS_UNMOUNTFS_MTHD		"unmountfs"	/* not currently implemented */
#define SYS_SHOW_MOUNTFS_MTHD		"show_mountfs"
#define SYS_SHAREFS_MTHD		"sharefs"
#define SYS_UNSHAREFS_MTHD		"unsharefs"
#define SYS_SHOW_SHAREFS_MTHD		"show_sharefs"	/* not currently implemented */
#define SYS_SET_LB_NTOA_MTHD		"set_lb_ntoa_entry"
#define	SYS_SET_NET_IF_STATUS_MTHD	BSYS_SET_NET_IF_STATUS_MTHD
#define	SYS_REMOVE_LB_NTOA_MTHD		"remove_lb_ntoa_entry"
#define SYS_YP_INIT_ALIASES_MTHD	"yp_init_aliases"
#define SYS_YP_INIT_BINDING_MTHD	"yp_init_binding"
#define SYS_NIS_INIT_MTHD		"nisinit"
#define SYS_CONFIG_NSSWITCH_MTHD	"config_nsswitch"
#define	SYS_LIST_PATCHES_MTHD		"list_patches"

/*
 * Parameters
 */
#define SYS_ADDFLAG_PAR			"addflag"
#define SYS_ARCHITECTURE_PAR		BSYS_ARCHITECTURE_PAR
#define	SYS_ARP_PAR			BSYS_ARP_PAR
#define SYS_AUTOMNT_PAR			"automount"
#define SYS_BLOCKS_PAR			"blocks"
#define SYS_BOOTPARAMSRECORD_PAR	"bootparamsrecord"
#define SYS_CD_DEVICE_PAR		"cd_device"
#define SYS_CHECKSUM_PAR		"checksum"
#define SYS_CLIENTDBDEL_PAR		"clientdbdel"
#define SYS_CLIENTTYPE_PAR		"clienttype"
#define SYS_COMMAND_REV_PAR		"command_rev"
#define SYS_COMMENT_PAR			"comment"
#define SYS_CURR_DOMAIN_PAR		BSYS_CURR_DOMAIN_PAR
#define SYS_CURR_NODENAME_PAR		BSYS_CURR_NODENAME_PAR
#define	SYS_CURR_PATH_PAR		"curr_path"
#define SYS_CURR_ROUTE_PAR		BSYS_CURR_ROUTE_PAR
#define SYS_CURR_TZ_PAR			BSYS_CURR_TZ_PAR
#define SYS_DATE_PAR			BSYS_DATE_PAR
#define SYS_DB_HOSTNAME_PAR		"db_"SYS_HOSTNAME_PAR
#define SYS_DESCRIPTION_PAR		"description"
#define SYS_DFSTYPE_PAR			"fstype"
#define SYS_DOMAIN_PAR			BSYS_DOMAIN_PAR
#define SYS_DUMPPATH_PAR		"dumppath"
#define SYS_DUMPSIZE_PAR		"dumpsize"
#define SYS_ELF_LIB_PAR			"elf_lib"
#define SYS_ETHERADDR_PAR		BSYS_ETHERADDR_PAR
#define SYS_ETHERSRECORD_PAR		"ethersrecord"
#define SYS_EXECPATH_PAR		"execpath"
#define SYS_FILE_PAR			"file"
#define SYS_FILE_TYPE_PAR		"file_type"
#define SYS_FSCKDEV_PAR			"fsckdev"
#define SYS_FSCKPASS_PAR		"fsckpass"
#define SYS_FSTYPE_PAR			SYS_DFSTYPE_PAR
#define SYS_FULL_PATH_PAR		"full_path"
#define SYS_GROUP_PAR			"group"
#define SYS_HOMEPATH_PAR		"homepath"
#define SYS_HOSTNAME_PAR		BSYS_HOSTNAME_PAR
#define SYS_HOSTSRECORD_PAR		"hostsrecord"
#define SYS_HW_PROVIDER_PAR		BSYS_HW_PROVIDER_PAR
#define SYS_HW_SERIAL_PAR		BSYS_HW_SERIAL_PAR
#define SYS_IMPLEMENTATION_ARCH_PAR	"implementation_arch"
#define SYS_INSTALL_IPADDR_PAR		"install_"SYS_IPADDR_PAR
#define SYS_INSTALL_HIERARCHY_PAR	"install_"SYS_HIERARCHY_PAR
#define SYS_INSTALL_HOSTNAME_PAR	"install_"SYS_HOSTNAME_PAR
#define SYS_INSTALL_NETIF_PAR		"install_"SYS_NETIF_PAR
#define SYS_INSTRUCTION_SET_PAR		"instruction_set"
#define SYS_HIERARCHY_PAR		"hierarchy"
#define SYS_IPADDR_PAR			BSYS_IPADDR_PAR
#define SYS_KERNEL_REV_PAR		"kernel_rev"
#define SYS_KVMPATH_PAR			"kvmpath"
#define SYS_LD_LIBRARY_PATH_PAR		"ld_library_path"
#define SYS_LOCALE_PAR			"locale"
#define SYS_MACHINE_PAR			BSYS_MACHINE_PAR
#define SYS_MAILPATH_PAR		"mailpath"
#define SYS_MESSAGE_PAR			"message"
#define SYS_MODE_PAR			"mode"
#define SYS_MOUNTPT_PAR			"mountpt"
#define SYS_NAMESERVICE_PAR		"nameservice"
#define SYS_NETIF_PAR			BSYS_NETIF_PAR
#define SYS_NETMASK_PAR			BSYS_NETMASK_PAR
#define SYS_NODENAME_PAR		BSYS_NODENAME_PAR
#define	SYS_OBSOLETES_PAR		"obsoletes"
#define SYS_OPTIONS_PAR			"options"
#define SYS_OS_NAME_PAR			"os_name"
#define SYS_OS_VERSION_PAR		"os_version"
#define SYS_OWNER_PAR			"owner"
#define SYS_OPENWIN_REV_PAR		"openwin_rev"
#define	SYS_PATCHID_PAR			"patchid"
#define SYS_PATHNAME_PAR		"pathname"
#define SYS_PATH_PAR			"path"
#define SYS_PERM_DOMAIN_PAR		BSYS_PERM_DOMAIN_PAR
#define SYS_PERM_NODENAME_PAR		BSYS_PERM_NODENAME_PAR
#define SYS_PERM_ROUTE_PAR		BSYS_PERM_ROUTE_PAR
#define SYS_PERM_TZ_PAR			BSYS_PERM_TZ_PAR
#define	SYS_PKGS_PAR			"pkgs"
#define	SYS_PRIVATE_PAR			BSYS_PRIVATE_PAR
#define SYS_RELEASE_PAR			BSYS_RELEASE_PAR
#define SYS_REMOTEFLAG_PAR		"remoteflag"
#define SYS_RESOURCE_PAR		"resource"
#define SYS_ROOTPATH_PAR		"rootpath"
#define SYS_ROUTE_PAR			BSYS_ROUTE_PAR
#define SYS_RUN_LEVEL_PAR		BSYS_RUN_LEVEL_PAR
#define SYS_RUN_IPADDR_PAR		"run_"SYS_IPADDR_PAR
#define SYS_RUN_HIERARCHY_PAR		"run_"SYS_HIERARCHY_PAR
#define SYS_RUN_HOSTNAME_PAR		"run_"SYS_HOSTNAME_PAR
#define SYS_RUN_NETIF_PAR		"run_"SYS_NETIF_PAR
#define SYS_SERVICETYPE_PAR		"servicetype"
#define	SYS_SHAREPATH_PAR		"sharepath"		/* Multi-os */
#define SYS_SRCH_RES_PAR		"searchresult"
#define	SYS_STATUS_PAR			BSYS_STATUS_PAR
#define SYS_SWAPPATH_PAR		"swappath"
#define SYS_SWAPSIZE_PAR		"swapsize"
#define SYS_SYSNAME_PAR			BSYS_SYSNAME_PAR
#define SYS_TAKEEFFECT_PAR		BSYS_TAKEEFFECT_PAR
#define SYS_TERM_PAR			"term"
#define SYS_TE_PAR			SYS_TAKEEFFECT_PAR
#define SYS_TIMEHOST_PAR		BSYS_TIMEHOST_PAR
#define SYS_TIMEZONE_PAR		BSYS_TIMEZONE_PAR
#define SYS_TIMEZONERECORD_PAR		"timezonerecord"
#define	SYS_TRAILERS_PAR		BSYS_TRAILERS_PAR
#define SYS_VERSION_PAR			BSYS_VERSION_PAR
#define SYS_VFSTABNAME_PAR		"vfstab"

/*
 * Well-known values for some parameters
 */
#define SYS_VAL_NS_NIS			"nis"
#define SYS_VAL_NS_NIS_PLUS		"nis_plus"
#define SYS_VAL_NS_UFS			"ufs"

#define SYS_TE_NOW			BSYS_TE_NOW
#define SYS_TE_BOOT			BSYS_TE_BOOT
#define SYS_TE_NOWANDBOOT		BSYS_TE_NOWANDBOOT

#define	SYS_VAL_CLIENTTYPE_DISKLESS	"DISKLESS"
#define SYS_VAL_CLIENTTYPE_DATALESS	"DATALESS"
#define SYS_VAL_CLIENTTYPE_DISKFULL	"DISKFULL"
#define SYS_VAL_CLIENTTYPE_DATALESS_OR_DISKFULL	\
    SYS_VAL_CLIENTTYPE_DATALESS"/"SYS_VAL_CLIENTTYPE_DISKFULL
#define SYS_VAL_SERVICETYPE_RUN		"RUN"
#define SYS_VAL_SERVICETYPE_INSTALL	"INSTALL"
#define SYS_VAL_CLIENTDBDEL_REMOVE	"REMOVE"
#define SYS_VAL_CLIENTDBDEL_PRESERVE	"PRESERVE"

#define	SYS_VAL_UNKNOWN			"Unknown"
#define SYS_VAL_NOT_DETERMINED		"NotDetermined"

#define SYS_VAL_ADDFLAG_ADDONLY		"addonly"
    /*
     * "strict" add:
     * if any client files already exist exit with error,
     * if any nameservice entries already exist exit with error,
     * if removing nameservice entries name<->IPaddr entry must exist
     */
#define SYS_VAL_ADDFLAG_ADD_OVERWRITE	"add_overwrite"
    /*
     * non-destructive restart: 
     * reuse existing files where possible and create new if not,
     * reuse nameservice entries where possible and create new if not
     */
#define SYS_VAL_ADDFLAG_OVERWRITEONLY	"overwrite"
    /*
     * force destructive restart:
     * overwrite all existing files even if they seem reusable,
     * reuse nameservice entries where possible and create new if not
     */
#define SYS_VAL_ADDFLAG_REUSE		"reuse"
    /*
     * "safe" upgrade of an existing system:
     * do not overwrite any existing file under any circumstances,
     * reuse nameservice entries where possible and create new if not
     */
    /*
     * Note that last three values behave differently wrt. files,
     * yet currently behave identically wrt. nameservice entries.  
     * The fine distinctions in behavior are not currently implemented
     * for nameservice entries -- there may never be a good reason
     * to implement them.   As a result, the value names
     * and descriptions make good sense for file resources
     * but seem a bit confusing for nameservice entries.  
     */

#define SYS_VAL_REMOTEFLAG_YES		SYS_VAL_YES
#define SYS_VAL_REMOTEFLAG_NO		SYS_VAL_NO

#define SYS_VAL_AUTOMNT_YES		SYS_VAL_YES
#define SYS_VAL_AUTOMNT_NO		SYS_VAL_NO

#define	SYS_VAL_STATUS_UP	BSYS_VAL_STATUS_UP
#define	SYS_VAL_STATUS_DOWN	BSYS_VAL_STATUS_DOWN

#define	SYS_VAL_YES		BSYS_VAL_YES
#define	SYS_VAL_NO		BSYS_VAL_NO

#define SYS_VAL_FS_TYPE_NFS	"nfs"
#define SYS_VAL_FS_TYPE_RFS	"rfs"
#define SYS_VAL_FS_TYPE_UFS	"ufs"
#define SYS_VAL_FS_TYPE_HSFS	"hsfs"

/*
 * Default values for some parameters
 */
#define SYS_DEF_ADDFLAG		SYS_VAL_ADDFLAG_ADDONLY
#define SYS_DEF_AUTOMNT		SYS_VAL_AUTOMNT_YES
#define SYS_DEF_DFS_TYPE	SYS_VAL_FS_TYPE_NFS
#define SYS_DEF_FS_TYPE		SYS_VAL_FS_TYPE_UFS
#define SYS_DEF_FSCKPASS	"2"
#define SYS_DEF_MNT_OPT		"-"
#define SYS_DEF_NFSOPTION	"bg,hard,rw"
#define SYS_DEF_OPTION		"rw"
#define SYS_DEF_RWOPTION	SYS_DEF_OPTION		/* Multi-os */
#define	SYS_DEF_ROOPTION	"ro"			/* Multi-os */
#define	SYS_DEF_4XFSCKPASS	"0"			/* Multi-os */
#define SYS_DEF_VFSTAB		"/etc/vfstab"
#define SYS_DEF_FSTAB		"/etc/fstab"		/* Multi-os */
#define SYS_DEF_HOSTSFILE	"/etc/inet/hosts"
#define SYS_DEF_4XHOSTSFILE	"/etc/hosts"		/* Multi-os */
#define SYS_DEF_NIS_PLUS_TREE	"/var/nis/NIS_COLD_START"
#define SYS_DEF_NIS_TREE	"/var/yp/binding"
#define SYS_DEF_REMOTEFLAG	SYS_VAL_REMOTEFLAG_NO
#define SYS_VAL_DUMPSIZE_DEF	"24"
#define SYS_VAL_SWAPSIZE_DEF	"24"
#ifdef __i386__
#define SYS_VAL_CD_DEVICE_DEF	"/dev/c0t6d0p0"
#else
#define SYS_VAL_CD_DEVICE_DEF	"/dev/c0t6d0s0"
#endif
#define	SYS_VAL_CLIENTTYPE_DEF	SYS_VAL_CLIENTTYPE_DISKLESS
#define SYS_VAL_SERVICETYPE_DEF	SYS_VAL_SERVICETYPE_RUN

/*
 * Compatibility with other style of symbol name
 * which doesn't exlicitly include the class
 * as part of the symbol name
 */
#define GET_KERNEL_REV_MTHD		SYS_GET_KERNEL_REV_MTHD
#define FIND_FILE_IN_PATH_MTHD		SYS_FIND_FILE_IN_PATH_MTHD
#define GET_FILE_TYPE_MTHD		SYS_GET_FILE_TYPE_MTHD
#define GET_ELF_LIBS_MTHD		SYS_GET_ELF_LIBS_MTHD
#define GET_COMMAND_REV_MTHD		SYS_GET_COMMAND_REV_MTHD
#define GET_FILE_PERMISSIONS_MTHD	SYS_GET_FILE_PERMISSIONS_MTHD
#define GET_FILE_CHECKSUM_MTHD		SYS_GET_FILE_CHECKSUM_MTHD
#define MOUNTFS_MTHD			SYS_MOUNTFS_MTHD
#define SHAREFS_MTHD			SYS_SHAREFS_MTHD
#define UNSHAREFS_MTHD			SYS_UNSHAREFS_MTHD
#define UNMOUNTFS_MTHD			SYS_UNMOUNTFS_MTHD
#define SET_LB_NTOA_MTHD		SYS_SET_LB_NTOA_MTHD

#define AUTOMNT_PAR		SYS_AUTOMNT_PAR
#define BLOCKS_PAR		SYS_BLOCKS_PAR
#define CHECKSUM_PAR		SYS_CHECKSUM_PAR
#define COMMAND_REV_PAR		SYS_COMMAND_REV_PAR
#define DESCRIPTION_PAR		SYS_DESCRIPTION_PAR
#define DFSTYPE_PAR		SYS_DFSTYPE_PAR
#define ELF_LIB_PAR		SYS_ELF_LIB_PAR
#define FILE_PAR		SYS_FILE_PAR
#define FSCKDEV_PAR		SYS_FSCKDEV_PAR
#define FSCKPASS_PAR		SYS_FSCKPASS_PAR
#define FSTYPE_PAR		SYS_FSTYPE_PAR
#define FULL_PATH_PAR		SYS_FULL_PATH_PAR
#define GROUP_PAR		SYS_GROUP_PAR
#define KERNEL_REV_PAR		SYS_KERNEL_REV_PAR
#define LD_LIBRARY_PATH_PAR	SYS_LD_LIBRARY_PATH_PAR
#define LOCALE_PAR		SYS_LOCALE_PAR
#define MODE_PAR		SYS_MODE_PAR
#define MOUNTPT_PAR		SYS_MOUNTPT_PAR
#define OPTIONS_PAR		SYS_OPTIONS_PAR
#define OWNER_PAR		SYS_OWNER_PAR
#define PATCH_DESC_PAR		SYS_PATCH_DESC_PAR
#define PATCH_NAME_PAR		SYS_PATCH_NAME_PAR
#define PATH_PAR		SYS_PATH_PAR
#define PATHNAME_PAR		SYS_PATHNAME_PAR
#define RESOURCE_PAR		SYS_RESOURCE_PAR
#define SRCH_RES_PAR		SYS_SRCH_RES_PAR
#define TERM_PAR		SYS_TERM_PAR
#define VFSTABNAME_PAR		SYS_VFSTABNAME_PAR

#define DEF_AUTOMNT		SYS_DEF_AUTOMNT
#define DEF_DFS_TYPE		SYS_DEF_DFS_TYPE
#define DEF_FS_TYPE		SYS_DEF_FS_TYPE
#define DEF_FSCKPASS		SYS_DEF_FSCKPASS
#define DEF_MNT_OPT		SYS_DEF_MNT_OPT
#define DEF_NFSOPTION		SYS_DEF_NFSOPTION
#define DEF_OPTION		SYS_DEF_OPTION
#define DEF_VFSTAB		SYS_DEF_VFSTAB

#ifdef  __cplusplus
}
#endif

#endif /* !_cl_system_parms_h */

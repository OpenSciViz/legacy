/****************************************************************************
 *      Copyright (c) 1993
 *      Century Computing, Inc.
 *      ALL RIGHTS RESERVED
 *
 *      The software (programs, data bases and/or documentation) on or in
 *      any media can not be reproduced, disclosed, or used except under
 *      the terms of the TAE Plus Software License Agreement.
 *
 ***************************************************************************/



/*	
 *	XtaeResolveUilIdentifier.
 *
 *	unravel environment variables in fileName string
 *	and define the string to Mrm.
 *
 */

/*	Change Log:
 * 
 * 25-may-93	Initial..palm
 * 03-jun-93	Removed taeconf, make depend complained...kbs
 */

#include <Mrm/MrmPublic.h>
#include <Xtae/XtaeUtils.h>	/* constants and _xtae_f_ functions */
#include <string.h>

static char *whatString = "@(#)XtaeResolveUilIdentifier.c	29.1 12/2/93";

/*	substitute environment variables in identName
 *	and define the identifier to Mrm.
 */

int XtaeResolveUilIdentifier (identName, fileName)
	char *identName;		/* input */
	char *fileName;			/* input */
{
MrmRegisterArg identList;
char newFileName[500];
int mrmcode, subcode;

/* expand the filespec if necessary */
subcode = _xtae_f_subst (fileName, newFileName);
if (subcode != SUCCESS)
    strcpy (newFileName, fileName);	/* define the un-substituted form */

/* 	Technically, the following is a leak, but
 *	it only happens upon XtaeResolveUilIdentifier
 *	which should be limited to a fixed number of
 *	uil identifiers.  The strings must be made
 *	permanent before transmission to Mrm.
 */
identList.name =  strdup(identName);
identList.value = strdup(newFileName);

mrmcode = MrmRegisterNames (&identList, 1);
if (subcode != SUCCESS)
    return subcode;			/* if substitution difficulties */
else
    return mrmcode != MrmSUCCESS;	/* if registration difficulties */
}


% Prolog definitions for genscript's PostScript code.
% Copyright (c) 1995 Markku Rossi.
%
% Author: Markku Rossi <mtr@iki.fi>
%
%
% This file is part of genscript.
%
% Genscript is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% Genscript is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with genscript; see the file COPYING.  If not, write to
% the Free Software Foundation, 59 Temple Place - Suite 330,
% Boston, MA 02111-1307, USA.
%

% -- code follows this line --

% ReEncodeSmall from Adobe Postscript Tutorial and Cookbook
/reencsmalldict 12 dict def
/ReEncodeSmall
 {reencsmalldict begin
  /newcodesandnames exch def
  /newfontname exch def
  /basefontname exch def

  /basefontdict basefontname findfont def
  /newfont basefontdict maxlength dict def

  basefontdict
   {exch dup /FID ne
     {dup /Encoding eq
       {exch dup length array copy
	 newfont 3 1 roll put}
       {exch newfont 3 1 roll put}
       ifelse
     }
     {pop pop}
     ifelse
   }forall

  newfont /FontName newfontname put
  newcodesandnames aload pop

  newcodesandnames length 2 idiv
   {newfont /Encoding get 3 1 roll put}
   repeat

  newfontname newfont definefont pop
  end
}def

%
% Macros.
%

/_S {	% save current state
  /_s save def
} def
/_R {	% restore from saved state
  _s restore
} def

/S {	% showpage protecting gstate
  gsave
  showpage
  grestore
} bind def

/MF {	% fontname newfontname -> -	make a new encoded font
  encode_vector ReEncodeSmall
} def

/M {moveto} bind def
/s {show} bind def

/Box {	% x y w h -> -			define box path
  /d_h exch def /d_w exch def /d_y exch def /d_x exch def
  d_x d_y  moveto 
  d_w 0 rlineto 
  0 d_h rlineto 
  d_w neg 0 rlineto 
  closepath
} def

% Column separator lines.
/column_lines {
  gsave
    0 setlinewidth
    /cw d_output_w cols div def
    1 1 cols 1 sub {
      cw mul 0 moveto
      0 d_output_h rlineto stroke
    } for
  grestore
} def

% Underlay
/underlay {	% - -> -
  gsave
    0 d_page_h translate
    d_page_h neg d_page_w atan rotate

    ul_gray setgray
    ul_font setfont
    /dw d_page_h dup mul d_page_w dup mul add sqrt def
    ul_str stringwidth pop dw exch sub 2 div ul_ptsize -2 div moveto 
    ul_str true charpath stroke
  grestore
} def

% Check PostScript language level.
/languagelevel where {
  pop /gs_languagelevel languagelevel def
} {
  /gs_languagelevel 1 def
} ifelse

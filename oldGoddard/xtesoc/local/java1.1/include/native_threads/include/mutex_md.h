/*
 * @(#)mutex_md.h	1.7 98/07/01
 *
 * Copyright 1995-1998 by Sun Microsystems, Inc.,
 * 901 San Antonio Road, Palo Alto, California, 94303, U.S.A.
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of Sun Microsystems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Sun.
 */

/*
 * Interface to mutex HPI implementation for Solaris
 */

#ifndef _MUTEX_MD_H_
#define _MUTEX_MD_H_

#include <synch.h>

/*
 * Generally, we would typedef mutex_t to be whatever the system
 * supplies.  But Solaris gives us mutex_t directly.
 */

int    mutexInit(mutex_t *);
int    mutexDestroy(mutex_t *);
int    mutexLock(mutex_t *);
bool_t mutexLocked(mutex_t *);
int    mutexUnlock(mutex_t *);

#endif /* !_MUTEX_MD_H_ */

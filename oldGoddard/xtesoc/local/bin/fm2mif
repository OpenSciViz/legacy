#!/usr/local/bin/perl
# fm2mif.pl
#
# THIS FILE IS COPYRIGHT 1994 BY TELENOR RESEARCH, NORWAY
#
#                    ALL RIGHTS RESERVED.
#
# For rights granted to the public see the COPYRIGHT file
#
# Programming : Jon Stephenson von Tetzchner

# This script is part of the fm2html package.

$\ = "\n";		# set output record separator
$, = " ";		# set output field separator
$* = 0;			# set single line pattern matching

select( STDERR );	# all error messages go to stderr
$| = 1;			# make stderr unbuffered

$FALSE = 0;
$TRUE  = 1;

$FM_MAGIC	= "<MakerFile";
$FM_BOOK_MAGIC	= "<Bookfile";
$MIF_MAGIC	= "<MIFFile";
$MIF_BOOK_MAGIC	= "<Book ";

$TMPNAME	= "fm2mif.tmp";


sub escape	# take string, insert backslash before special characters
{
  local( $_ ) = @_;
  s|([\.\+\?\*\'\"\(\)])|\\$1|g;
  $_;
}

sub dirname	# take pathname, return leading directories
{
  local( $_ ) = @_;
  s|^(.+)/.*$|$1| || s|^/[^/]*$|/| || s|^[^/]*$|.|;
  $_;
}

sub basename	# take pathname, strip leading directories and optional suffix
{
  local( $_, $suffix ) = @_;
  m|/|o && s|^.*/([^/]*)$|$1|;
  $suffix = &escape( $suffix );
  s|(.*)$suffix|$1|;
  $_;
}


local( $prog ) = &basename( $0 );
local( $usage ) = "Usage: $prog [-q] [-s] filename";

local( $infile, $indir, $cwd, $tmpfile, $outfile, $script, $fmbatch, 
       $version, $execpath, @docs, $doc, @errs );

local( $quiet ) = $FALSE;
local( $strip_doc_extension ) = $FALSE;
local( $redirect ) = "1>&2";

while ( $ARGV[0] =~ m|^-| )
{
  $_ = shift;
  last if m|^--|;
  
  if ( m|^-q$| )
  {
    $quiet = $TRUE; next;
  }
  elsif ( m|^-s$| )
  {
    $strip_doc_extension = $TRUE; next;
  }

  print "$prog: ERROR: Invalid flag: \"$_\"";
  print "$usage";
  exit( 1 );
}

if ( $#ARGV != 0 )
{
  print "";
  print "$usage";
  print "";
  print "\tThe parameter specifies the name of a FrameMaker file to";
  print "\ttranslate to Maker Interchange Format (MIF).  If a";
  print "\tFrameMaker book file is specified then all files in the";
  print "\tbook will be translated, as will the book itself.";
  print "";
  print "\tThe name of each output file is formed by appending";
  print "\t\".mif\" to the name of the input file.  If the name of";
  print "\tan input file ends in a \".doc\" extension and the \"-s\"";
  print "\t(for \"strip\") option has been specified, the extension";
  print "\twill be removed prior to \".mif\" being appended.";
  print "";
  print "\tSpecifying \"-q\" causes the program to operate in \"quiet\"";
  print "\tmode, in which messages are printed only if errors occur.";
  print "";
  exit( 1 );
}

$quiet == $FALSE && print "";

$infile = "$ARGV[0]";

if ( ! -e "$infile" )
  { die "$prog: ERROR: Input file \"$infile\" does not exist... exiting"; }
if ( ! open( INFILE, "<$infile" ) )
  { die "$prog: ERROR: Could not open input file \"$infile\"... exiting"; }

$idline = <INFILE>;

if ( $idline =~ m|^$MIF_MAGIC|i || $idline =~ m|^$MIF_BOOK_MAGIC|i )
  { die "$prog: ERROR: File \"$infile\" is already in MIF format... exiting";}
if ( $idline !~ m|^$FM_MAGIC|i && $idline !~ m|^$FM_BOOK_MAGIC|i )
  { die "$prog: ERROR: File \"$infile\" is not a FrameMaker document... exiting"; }

close( INFILE );

# djf: Docs and xrefs may have relative pathnames so it is necessary to chdir.
# Unfortunately, however, FrameMaker sometimes has problems figuring out
# what the current directory is and defaults to the user's home directory,
# in which case it won't find the files anyway.
$indir = &dirname( "$infile" );
chdir( "$indir" ) ||
  die "mif2mif: ERROR: Can't change directory to \"$indir\"... exiting";
chop( $cwd = `pwd` );
$infile = &basename( "$infile" );
$infile = "$cwd/$infile";

if ( $quiet == $TRUE )
{
  local( $tmpdir ) = $ENV{'TMPDIR'};

  if ( ! $tmpdir ) { $tmpdir = "."; }
  if ( ! $tmpdir ) { $tmpdir = "$cwd"; }
  $tmpdir =~ s|/$||;
  $tmpfile = "$tmpdir/$TMPNAME.$$";
  $redirect = "1>$tmpfile 2>&1";
}

$idline =~ m|^\S+\s+(.)|;
$version = "$1";

if ( ! "/usr/frame4/bin/fmbatch -makername maker -i" )
{
  $fmbatch = "$ENV{FMHOME}/bin/fmbatch";

  if ( $version > 3 )
    { die "$prog: ERROR: FrameMaker batch mode program \"$fmbatch\" can not process version $version FrameMaker documents... exiting"; }
}
elsif ( ! "$ENV{FMHOME}/bin/fmbatch" )
{
  $fmbatch = "/usr/frame4/bin/fmbatch -makername maker -i";

  if ( $version < 4 )
    { $quiet == $FALSE && print "$prog: WARNING: FrameMaker batch mode program \"$fmbatch\" will convert version $version FrameMaker documents to version 4 MIF files"; }
}
elsif ( $version < 4 )
{
  $fmbatch = "$ENV{FMHOME}/bin/fmbatch";
}
else
{
  $fmbatch = "/usr/frame4/bin/fmbatch -makername maker -i";
}

$execpath = "$fmbatch";
$execpath =~ s|^(\S+)\s*.*|$1|;

if ( ! -x "$execpath" )
  { die "$prog: ERROR: FrameMaker batch mode program \"$execpath\" is not executable... exiting"; }

if ( $idline =~ m|^$FM_BOOK_MAGIC|i )
{
  $outfile = $infile;
  $strip_doc_extension == $TRUE && ($outfile =~ s|\.doc$||);
  $outfile = "$outfile.mif";

  $quiet == $FALSE && print
    "$prog: Converting book file \"$infile\" to MIF file \"$outfile\"...\n";
  
  $script = <<END;
Open $infile
SaveAs m $infile $outfile
Quit
END

  eval open( FMBATCH, "| $fmbatch $redirect" );
  print FMBATCH "$script";
  eval close( FMBATCH );

  if ( $? != 0 )
  {
    if ( $quiet == $TRUE )
    {
      # djf: It would be nice to eliminate the need for $tmpfile by piping
      # fmbatch output directly to grep.  However, if you do that then the
      # status value returned by "close( FMBATCH )" is for the grep, not the
      # fmbatch job.
      eval system( "grep -i error $tmpfile 1>&2" );
      eval system( "rm -f $tmpfile 1>&2" );
    }
    die "$prog: ERROR: FrameMaker batch program \"$fmbatch\" failed... exiting";
  }
  eval system( "rm -f $tmpfile 1>&2" );

  if ( ! -e "$outfile" )
    {die "$prog: ERROR: Failed to convert FrameMaker book to MIF... exiting";}
  if ( ! open( BOOKMIF, "<$outfile" ) )
    { die "$prog: ERROR: Could not open MIF file \"$outfile\"... exiting"; }

  while ( <BOOKMIF> )
  {
    if ( m|<FileName \s*`<c\\>[^<]*<U\\>([^']*)'>| )
    {
      $docs[++$#docs] = "$1";	# djf: version 3 FrameMaker documents
    }
    elsif ( m|<FileName \s*`<c\\>([^']*)| )
    {
      $docs[++$#docs] = "$1";	# djf: version 4 FrameMaker documents
    }			    
  }

  close( BOOKMIF );

  @ARGV = @docs;

  $quiet == $FALSE && print "";
}
else
{
  $ARGV[0] = "$infile";
  @docs = @ARGV;
}

$script = "";
while ( $doc = shift )
{
  if ( ! -e "$doc" )
  {
    $errs[++$#errs] = 
      "$prog: ERROR: Can't convert file \"$doc\" (does not exist)";
    next;
  }

  if ( ! open( DOC, "<$doc" ) )
  {
    $errs[++$#errs] = "$prog: ERROR: Can't open file \"$doc\"";
    next;
  }

  $idline = <DOC>;
  close( DOC );

  if ( $idline =~ m|^$MIF_MAGIC|i || $idline =~ m|^$MIF_BOOK_MAGIC|i )
  {
    $quiet == $FALSE &&
      print "$prog: ATTENTION: File \"$doc\" is already in MIF format";
    next;
  }

  if ( $idline !~ m|^$FM_MAGIC|i && $idline !~ m|^$FM_BOOK_MAGIC|i )
  {
    $errs[++$#errs] = 
      "$prog: ERROR: File \"$doc\" is not a FrameMaker document";
    next;
  }

  $mif = $doc;
  if ( $strip_doc_extension == $TRUE )
    { $mif =~ s|\.doc$||; }
  $mif = "$mif.mif";
  
  # djf: Remove any backup file, and remove any existing MIF file so that
  # no new backup file will be created
  eval system( "rm -f $mif $mif.backup 1>/dev/null 2>&1" );

  $script .= <<END;
Open $doc
SaveAs m $doc $mif
END
}

$script .= <<END;
Quit
END

if ( $quiet == $FALSE )
{
  print "$prog: Converting the following FrameMaker document files to MIF:";
  while ( $doc = shift( @docs ) )
    { print "  $doc"; }
  print "";
}

eval open( FMBATCH, "| $fmbatch $redirect" );
print FMBATCH "$script";
close( FMBATCH );

if ( $? != 0 )
{
  if ( $quiet == $TRUE )
  {
    # djf: It would be nice to eliminate the need for $tmpfile by piping
    # fmbatch output directly to grep.  However, if you do that then the
    # status value returned by "close( FMBATCH )" is for the grep, not the
    # fmbatch job.
    eval system( "grep -i error $tmpfile 1>&2" );
    eval system( "rm -f $tmpfile 1>&2" );
  }
  die "$prog: ERROR: FrameMaker batch program \"$fmbatch\" failed... exiting";
}
eval system( "rm -f $tmpfile 1>&2" );

$quiet == $FALSE && print "";

if ( @errs )
{
  local( $err );
  while ( $err = shift( @errs ) )
    { print "$err"; }
  exit( 1 );
}

$quiet == $FALSE && print "$prog: Done\n";

exit( 0 );

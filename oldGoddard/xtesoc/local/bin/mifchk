#!/usr/local/bin/perl
# File:		mifchk.pl
# Version:	1.0
# Copyright:	MacDonald Dettwiler and Associates, 1994
# Author:	Duncan Fraser (djf@mda.ca)
# This file may be modified provided this header is kept intact
# and all modifications are documented here.
# This code may not be included in a commercial product without
# the permission of the author.

# This script is part of the fm2html package.

$\ = "\n";		# set output record separator
$, = " ";		# set output field separator
$* = 0;			# set single line pattern matching

select( STDERR );	# all error messages go to stderr
$| = 1;			# make stderr unbuffered

$FALSE = 0;
$TRUE  = 1;

$MIF_MAGIC		= "<MIFFile";
$MIF_BOOK_MAGIC		= "<Book ";
$FM_MAGIC		= "<MakerFile";
$FM_BOOK_MAGIC		= "<Bookfile";


sub escape	# take string, insert backslash before special characters
{
  local( $_ ) = @_;
  s|([\.\+\?\*\'\"\(\)])|\\$1|g;
  $_;
}

sub dirname	# take pathname, return leading directories
{
  local( $_ ) = @_;
  s|^(.+)/.*$|$1| || s|^/[^/]*$|/| || s|^[^/]*$|.|;
  $_;
}

sub basename	# take pathname, strip leading directories and optional suffix
{
  local( $_, $suffix ) = @_;
  m|/|o && s|^.*/([^/]*)$|$1|;
  $suffix = &escape( $suffix );
  s|(.*)$suffix|$1|;
  $_;
}


local( $prog ) = &basename( $0 );
local( $usage ) = "Usage: $prog [-nx] [-ni] [-q] [-r] filename";

local( $indir, $infile, $cwd, @mifs, @missing, %xrefs, %imports );

local( $do_xrefs ) = $TRUE;
local( $do_imports ) = $TRUE;
local( $quiet ) = $FALSE;
local( $remove ) = $FALSE;
local( $exitval ) = 0;

while ( $ARGV[0] =~ m|^-| )
{
  $_ = shift;
  last if m|^--|;
  
  if ( m|^-nx$| )
  {
    $do_xrefs = $FALSE; next;
  }
  elsif ( m|^-ni$| )
  {
    $do_imports = $FALSE; next;
  }
  elsif ( m|^-q$| )
  {
    $quiet = $TRUE; next;
  }
  elsif ( m|^-r$| )
  {
    $remove = $TRUE; next;
  }

  print "$prog: ERROR: Invalid flag: \"$_\"";
  print "$usage";
  exit( 1 );
}

if ( $#ARGV != 0 )
{
  print "$prog: ERROR: You must specify exactly one MIF book or document as input";
  print "$usage";
  exit( 1 );
}

$quiet == $FALSE && print "";

$infile = "$ARGV[0]";

if ( ! -e "$infile" )
  { die "$prog: Input file \"$infile\" does not exist... exiting"; }
if ( ! open( INFILE, "<$infile" ) )
  { die "$prog: Can't open input file \"$infile\"... exiting"; }

$magic = <INFILE>;

if ( $magic =~ m|^$FM_MAGIC|i || $magic =~ m|^$FM_BOOK_MAGIC|i )
  { die "$prog: FrameMaker file \"$infile\" is not a MIF file... exiting"; }
if ( $magic !~ m|^$MIF_MAGIC|i && $magic !~ m|^$MIF_BOOK_MAGIC|i )
  { die "$prog: Input file \"$infile\" is not a FrameMaker file... exiting";}

close( INFILE );

# Docs and xrefs may have relative pathnames so it is necessary to chdir.
# Unfortunately, however, FrameMaker sometimes has problems figuring out
# what the current directory is and defaults to the user's home directory,
# so it won't find the files anyway.
$indir = &dirname( "$infile" );
chdir( "$indir" ) ||
  die "mif2mif: ERROR: Can't change directory to \"$indir\"... exiting";
chop( $cwd = `pwd` );
$infile = &basename( "$infile" );
$infile = "$cwd/$infile";

if ( $magic =~ m|^$MIF_BOOK_MAGIC|i )
{
  open( INFILE, "<$infile" )
    || die "$prog: Can't open input file \"$infile\"... exiting";
  $mifs[++$#mifs] = "$infile";

  while ( <INFILE> )
  {
    if ( m|<FileName | )
    {
      local( $docname, $docpath, $mifpath, $suffix );

      if ( m|<FileName \s*`<c\\>([^<]*)<U\\>([^']*)'>| )
      {
        # FrameMaker 3.1
        $docname = "$1";
        $docpath = "$2";
      }
      elsif ( m|<FileName \s*`<c\\>([^<]*)'>| )
      {
        # FrameMaker 4.0
        $docname = "$1";
        $docpath = "$1";
      }
      
      $mifpath = "$docpath.mif";

      if ( ! -e "$mifpath" )
      {
print "docpath is $docpath, mifpath is $mifpath, cwd is", `pwd`;
	local( $unstripped ) = "$mifpath";

	$mifpath =~ s|\.doc||;

	if ( $mifpath eq $unstripped || ! -e $mifpath )
	{
	  $missing[++$#missing] = "$docpath";
	  next;
	}
      }

      $mifs[++$#mifs] = "$mifpath";
    }
  }

  close( INFILE );

  @ARGV = @mifs;

  if ( $#missing > -1 )
  {
    local( $name );

    print "$prog: ERROR: Corresponding MIF files don't exist for these documents:";
    $exitval = 1;

    while ( $name = shift( @missing ) )
      { print "  $name"; }

    $quiet == $FALSE && print "";
  }
}
else
{
  $ARGV[0] = "$infile"; 
}

$quiet == $FALSE && print "$prog: examining the following files:";

while ( $infile = shift )
{
  $quiet == $FALSE && print "  $infile";

  if ( ! open( INFILE, "<$infile" ) )
  {
    print "$prog: ERROR: Can't open file \"$infile\"";
    $exitval = 1;
  }
  
  while ( <INFILE> )
  {
    if ( m|<XRefSrcFile \s*`<c\\>([^<]*)'>| )
    {
      local( $path ) = "$1";

      if ( $do_xrefs == $TRUE && "$path" && ! -e "$path" )
        { $xrefs{$path} = "$path"; }
    }
    elsif ( m|<ImportObFile \s*`([^<]*)'>| )
    {
      local( $path ) = "$1";

      if ( $do_imports == $TRUE && "$path" &&
           "$path" !~ m|internal inset| && ! -e "$path" )
        { $imports{$path} = "$path"; }
    }
    elsif ( m|<ImportObFileDI \s*`.*<U\\>([^<]*)'>| )
    {
      local( $path ) = "$1";

      if ( $do_imports == $TRUE && "$path" &&
	   "$path" !~ m|internal inset| && ! -e "$path" )
        { $imports{$path} = "$path"; }
    }
    elsif ( m|<ImportObFileDI \s*`(.*)'>| )
    {
      local( $path ) = "$1";

      $path =~ s|<r\\><c\\>|/|g;
      $path =~ s|<u\\><c\\>|../|g;
      $path =~ s|<u\\>|../|g;
      $path =~ s|<c\\>|/|g;

      if ( $do_imports == $TRUE && "$path" &&
	   "$path" !~ m|internal inset| && ! -e "$path" )
        { $imports{$path} = "$path"; }
    }
  }

  if ( $remove == $TRUE )
    { eval system( "rm -f $infile 1>&2" ); }
}

$quiet == $FALSE && print "";

if ( %xrefs )
{
  local( $path );

  print "$prog: ERROR: The following files are cross-referenced but don't exist:";
  $exitval = 1;

  while ( ($path, $path) = each( %xrefs ) )
    { print "  $path"; }

  $quiet == $FALSE && print "";
}

if ( %imports )
{
  local( $path );

  print "$prog: ERROR: The following files are imported but don't exist:";
  $exitval = 1;

  while ( ($path, $path) = each( %imports ) )
    { print "  $path"; }

  $quiet == $FALSE && print "";
}

if ( $quiet == $FALSE )
{
  if ( $exitval == 0 )
    { print "$prog: No errors... done\n"; }
  else
    { print "$prog: Exiting with errors\n"; }    
}

exit( $exitval );

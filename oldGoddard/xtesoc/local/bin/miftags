#!/usr/local/bin/perl
# File:		miftags.pl
# Version:	1.3
# Copyright:	MacDonald Dettwiler and Associates, 1994
# Programming:	Duncan Fraser (djf@mda.ca)
# This file may be modified provided this header is kept intact
# and all modifications are documented here.
# This code may not be included in a commercial product without
# the permission of the author.

# This script is part of the fm2html package.

# CHANGES
#
# Version 1.3
#   - "-q" option added
#
# Version 1.2
#   - FrameMaker 4.0 book files are now supported
#
# Version 1.1
#   - assume first that MIF file in a book has same name as document 
#     but with ".mif" appended; however, if file with that name is not
#     found, look for file with ".doc" stripped, e.g. given "foo.doc" 
#     look first for "foo.doc.mif", then for "foo.mif"

$\ = "\n";		# set output record separator
$, = " ";		# set output field separator
$* = 0;			# set single line pattern matching

select( STDERR );	# all error messages go to stderr
$| = 1;			# make stderr unbuffered

$FALSE = 0;
$TRUE  = 1;

$MIF_MAGIC		= "<MIFFile";
$MIF_BOOK_MAGIC		= "<Book ";
$FM_MAGIC		= "<MakerFile";
$FM_BOOK_MAGIC		= "<Bookfile";

$PTAGS_EXTENSION	= "ptags";
$CTAGS_EXTENSION	= "ctags";
$HTAGS_EXTENSION	= "htags";

$GLOBAL_PTAGS_FILE	= "/usr/local/lib/fm2html/ptags";
$GLOBAL_CTAGS_FILE	= "/usr/local/lib/fm2html/ctags";
$GLOBAL_HTAGS_FILE	= "/usr/local/lib/fm2html/htags";


sub escape	# take string, insert backslash before special characters
{
  local( $_ ) = @_;
  s|([\.\+\?\*\'\"\(\)])|\\$1|g;
  $_;
}

sub basename	# take pathname, strip leading directories and optional suffix
{
  local( $_, $suffix ) = @_;
  m|/|o && s|^.*/([^/]*)$|$1|;
  $suffix = &escape( $suffix );
  s|(.*)$suffix|$1|;
  $_;
}

sub toupper	# take string, convert to upper case
{
  local( $_ ) = @_;
  s|(.*)|\U$1\E|;
  $_;
}


local( $prog ) = &basename( $0 );
local( $usage ) = "USAGE: $prog [-h] [-q] filename";

local( $base, %g_ptags, %g_ctags, %ptags, %ctags, %ptag_files, %ctag_files,
       $ptags_infile, $ctags_infile, $hmode, $counter );

local( $ptags_infile ) = "$GLOBAL_PTAGS_FILE";
local( $ctags_infile ) = "$GLOBAL_CTAGS_FILE";
local( $do_ptags ) = $TRUE;
local( $do_ctags ) = $TRUE;
local( $hmode ) = $FALSE;
local( $quiet ) = $FALSE;
local( $counter ) = 1;

while ( $ARGV[0] =~ m|^-| )
{
  $_ = shift;
  last if m|^--|;
  
  if ( m|^-h$| )
  {
    $ptags_infile = "$GLOBAL_HTAGS_FILE"; $hmode = $TRUE; next;
  }
  elsif ( m|^-q$| )
  {
    $quiet = $TRUE; next;
  }

  print "$prog: ERROR: Invalid flag: \"$_\"";
  print "$usage";
  exit( 1 );
}

if ( $#ARGV != 0 )
{
  print "$prog: ERROR: You must specify exactly one MIF book or document as input";
  print "$usage";
  exit( 1 );
}
  
$infile = "$ARGV[0]";

if ( ! -e "$infile" )
  { die "$prog: ERROR: Input file \"$infile\" does not exist... exiting"; }
if ( ! open( INFILE, "<$infile" ) )
  { die "$prog: ERROR: Could not open input file \"$infile\"... exiting"; }

$magic = <INFILE>;

if ( $magic =~ m|^$FM_MAGIC|i || $magic =~ m|^$FM_BOOK_MAGIC|i )
  { die "$prog: ERROR: \"$infile\" is a FrameMaker file, not MIF... exiting";}
if ( $magic !~ m|^$MIF_MAGIC|i && $magic !~ m|^$MIF_BOOK_MAGIC|i )
  { die "$prog: ERROR: \"$infile\" is not a FrameMaker file... exiting";}


$base = $ARGV[0];
$base =~ s|\.mif$||;

if ( $hmode == $TRUE )
{
  $ptags_outfile = "$base.$HTAGS_EXTENSION";
  $do_ctags = $FALSE;
}
else
{
  $ctags_outfile = "$base.$CTAGS_EXTENSION";

  if ( open( CTAGSF, ">$ctags_outfile" ) )
  {
    close( CTAGSF );
    &read_global_tags( "CTAGS" );
  }
  else
  {
    print "$prog: ERROR: Could not create output file \"$ctags_outfile\"";
    $do_ctags = $FALSE;
  }

  $ptags_outfile = "$base.$PTAGS_EXTENSION";
}

if ( open( PTAGSF, ">$ptags_outfile" ) )
{
  close( PTAGSF );
  &read_global_tags( "PTAGS" );
}
else
{
  print "$prog: ERROR: Could not create output file \"$ptags_outfile\"";
  $do_ptags = $FALSE;
}

if ( $do_ctags == $FALSE && $do_ptags == $FALSE )
  { exit( 1 ); }

if ( $magic =~ m|^$MIF_BOOK_MAGIC|i )
{
  local( $docpath, @mifs );

  while ( <INFILE> )
  {
    if ( m|<FileName[^`]*`<c\\>[^<]*<U\\>([^']*)'>| )
    {
      # FrameMaker 3.1
      $docpath = "$1";
      $mifs[++$#mifs] = "$docpath.mif";
    }
    elsif ( m|<FileName \s*`<c\\>([^<]*)'>| )
    {
      # FrameMaker 4.0
      $docpath = "$1";
      $mifs[++$#mifs] = "$docpath.mif";
    }
  }

  close( INFILE );
  @ARGV = @mifs;
}

while ( $file = shift )
{
  local( $unstripped, $magic, $base, $in_p_catalog, $in_t_catalog,
	 $in_c_catalog, $in_table, $in_textflow, $in_generated_textflow );

  $in_p_catalog		 = $FALSE; 
  $in_t_catalog		 = $FALSE;
  $in_c_catalog		 = $FALSE; 
  $in_table		 = $FALSE; 
  $in_textflow		 = $FALSE; 
  $in_generated_textflow = $FALSE;

  if ( ! -e $file )
  {
    $unstripped = "$file";
    $file =~ s|\.doc||;

    if ( $file eq $unstripped )
    {
      print "$prog: ERROR: Can't scan \"$unstripped\" (does not exist)";
      next;
    }
    elsif ( ! -e $file )
    {
      print "$prog: ERROR: Can't scan \"$unstripped\" or \"$file\" (neither exist)";
      next;
    }
  }

  if ( ! open( INF, "<$file" ) )
  {
    print "$prog: ERROR: Could not open file \"$file\"";
    next;
  }

  $base = &basename( $file );

  $magic = <INF>;

  if ( $magic =~ m|$MIF_BOOK_MAGIC|i )
  {
    $quiet == $FALSE && 
      print "$prog: ATTENTION: Ignoring \"$base\" (file is MIF book)";
    next;
  }

  if ( $magic !~ m|$MIF_MAGIC|i )
  {
    print "$prog: ERROR \"$base\" is not a MIF file";
    next;
  }

  $quiet == $FALSE && print "$prog: Scanning file \"$base\"...";

  while ( <INF> )
  {
    m|<PgfCatalog|o		&& ($in_p_catalog = $TRUE);
    m|\> # end of PgfCatalog|o	&& ($in_p_catalog = $FALSE);
    m|<TblCatalog|o		&& ($in_t_catalog = $TRUE);
    m|\> # end of TblCatalog|o	&& ($in_t_catalog = $FALSE);
    m|<FontCatalog|o		&& ($in_c_catalog = $TRUE);
    m|\> # end of FontCatalog|o	&& ($in_c_catalog = $FALSE);
    m|<Tbls|o			&& ($in_table     = $TRUE);
    m|\> # end of Tbls|o	&& ($in_table     = $FALSE);
    m|<TextFlow|o		&& ($in_textflow  = $TRUE);
    m|<TFTag `TOC'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `IX'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `LOT'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `LOF'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `AIX'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `AML'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `LOP'|o		&& ($in_generated_textflow = $TRUE);
    m|<TFTag `SIX'|o		&& ($in_generated_textflow = $TRUE);
    m|\> # end of TextFlow|o	&& ($in_textflow = $FALSE,
				    $in_generated_textflow = $FALSE);

    if ( m|<PgfTag[^`]*`([^']+)'>| )
    {
      local( $tag ); $tag = $1;

      $ptag_files{$tag} = "$ptag_files{$tag} $base";

      if ( $in_p_catalog == $TRUE || $in_t_catalog == $TRUE )
      {
	if ( ! $ptags{$tag} )
	  { $ptags{$tag} = $g_ptags{$tag} ? $g_ptags{$tag} : "-CATALOG-"; }
      }
      elsif ( $in_table == $TRUE )
      {
	if ( ! $ptags{$tag} )
	  { $ptags{$tag} = $g_ptags{$tag} ? $g_ptags{$tag} : "-TABLE-";	}
	elsif ( $ptags{$tag} =~ m|-CATALOG-|o && $ptags{$tag} !~ m|-TABLE-|o )
	  { $ptags{$tag} = "$ptags{$tag} & -TABLE-"; }
      }
      elsif ( $in_generated_textflow == $TRUE )
      {
	local( $is_generated_tag ); $is_generated_tag = $FALSE;

	$tag =~ m|TOC$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|IX$|o  && ($is_generated_tag = $TRUE);
	$tag =~ m|LOT$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|LOF$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|AIX$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|AML$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|LOP$|o && ($is_generated_tag = $TRUE);
	$tag =~ m|SIX$|o && ($is_generated_tag = $TRUE);
	    
	if ( $is_generated_tag == $FALSE )
	{
	  if ( ! $ptags{$tag} )
	    { $ptags{$tag} = $g_ptags{$tag} ? $g_ptags{$tag} : "-GENFLOW-"; }
	  elsif ( ($ptags{$tag} =~ m|-CATALOG-|o ||
		   $ptags{$tag} =~ m|-TABLE-|o) &&
		   $ptags{$tag} !~ m|-GENFLOW-|o )
            { $ptags{$tag} = "$ptags{$tag} & -GENFLOW-"; }
	}
      }
      elsif ( $in_textflow == $TRUE )
      {
	if ( ! $ptags{$tag} )
	  { $ptags{$tag} = $g_ptags{$tag} ? $g_ptags{$tag} : "-TEXTFLOW-"; }
	elsif ( ($ptags{$tag} =~ m|-CATALOG-|o ||
		 $ptags{$tag} =~ m|-TABLE-|o ||
		 $ptags{$tag} =~ m|-GENFLOW-|o) &&
	         $ptags{$tag} !~ m|-TEXTFLOW-|o )
          { $ptags{$tag} = "$ptags{$tag} & -TEXTFLOW-";	}
      }
    }
    elsif ( $do_ctags == $TRUE && m|<FTag[^`]*`([^']+)'>| )
    {
      local( $tag ); $tag = $1;

      $ctag_files{$tag} = "$ctag_files{$tag} $base";

      if ( $in_c_catalog == $TRUE && ! $ctags{$tag} )
      {
	$ctags{$tag} = $g_ctags{$tag} ? $g_ctags{$tag} : "-CATALOG-";
      }
      elsif ( $in_textflow == $TRUE )
      {
	if ( ! $ctags{$tag} )
	{
          if ( $in_index == $TRUE )
            { $ctags{$tag} = $tag; }
	  else
            { $ctags{$tag} = $g_ctags{$tag} ? $g_ctags{$tag} : "-TEXTFLOW-"; }
	}
	elsif ( $ctags{$tag} eq "-CATALOG-" )
	{
          $ctags{$tag} = "$ctags{$tag} & -TEXTFLOW-";
        }
      }
    }
    elsif ( m|<(XRefSrcText) `[^:]*: ([^:]*): [^']*'>| ||
            m|<(MText) `[^:]*: ([^:]*): [^']*'>| )
    {
      local( $tag_type, $tag, $word ); $tag_type = $1; $tag = $2;

      $ptag_files{$tag} = "$ptag_files{$tag} $base";

      $word = $tag_type eq "MText" ? "MARKER" : "XREF";
      $to_tag = $g_ptags{$tag} ? $g_ptags{$tag} : $word;

      if ( ! $ptags{$tag} || $ptags{$tag} !~ m|$to_tag| )
        { $ptags{$tag} = $to_tag eq $word ? "$ptags{$tag} & $word" : $to_tag; }
    }
  }

  close( INF );
}	

if ( $do_ptags == $TRUE )
  { &write_output_tags( "PTAGS" ); }

if ( $do_ctags == $TRUE )
  { &write_output_tags( "CTAGS" ); }

$quiet == $FALSE && print "$prog: Done";


sub read_global_tags
{
  local( $type ) = @_;
  local( $globalfile );

  $globalfile = ($type eq "PTAGS" ? $ptags_infile : $ctags_infile );
  -e $globalfile || die
    "$prog: ERROR: Global tags file \"$globalfile\" does not exist... exiting";
  open( GLOBALF, "<$globalfile" ) || die
    "$prog: ERROR: Could not open global tags file \"$globalfile\"... exiting";
  $quiet == $FALSE &&
    print "$prog: Reading global tags file \"$globalfile\"...";
  
  while ( <GLOBALF> )
  {
    m|^([^\t]+)\t+([^\t\n]+)| && 
      ($type eq "PTAGS" ? ($g_ptags{$1} = "$2") : ($g_ctags{$1} = "$2"));
  }

  close( GLOBALF );
}


sub write_output_tags
{
  local( $type ) = @_;
  local( $outputfile, @from_tags, @sorted_from_tags, $from_tag );

  $outputfile = ($type eq "PTAGS" ? $ptags_outfile : $ctags_outfile);

  if ( ! open( OUTF, ">$outputfile" ) )
  {
    "$prog: ERROR: Could not create output file \"$outputfile\"";
    return;
  }

  if ( $type eq "PTAGS" )
    { @from_tags = keys( %ptags ); }
  else
    { @from_tags = keys( %ctags ); }

  @sorted_from_tags = sort( {&toupper( $a ) cmp &toupper( $b );} @from_tags );

  $quiet == $FALSE && 
    print "$prog: Creating output tags file \"$outputfile\"...";

  foreach $from_tag ( @sorted_from_tags )
  {
    local( $ntabs, $to_tag, $files, @sorted_files, @unique_files, $file,
	   $previous_file );

    $to_tag = $type eq "PTAGS" ? $ptags{$from_tag} : $ctags{$from_tag};

    if ( $hmode && $to_tag eq "-CATALOG-" )
      { next; }

    $files = $type eq "PTAGS" ? $ptag_files{$from_tag} : $ctag_files{$from_tag};
    @sorted_files = sort( {&toupper( $a ) cmp &toupper( $b );}
			  split( m|[ \t\n]+|, $files ) );

    foreach $file ( @sorted_files )
    {
      if ( $file ne $previous_file )
      {
	$unique_files[++$#unique_files] = $file;
	$previous_file = $file;
      }
    }

    printf( OUTF "$from_tag" );

    $ntabs = 4 - length( $from_tag ) / 8;
    for ( $ntabs = $ntabs > 0 ? $ntabs : 1; $ntabs > 0; $ntabs-- )
      { printf( OUTF "\t" ); }

    if ( $to_tag =~ m|^-CATALOG-|o || $to_tag =~ m|^-TABLE-|o ||
	 $to_tag =~ m|^-GENFLOW-|o || $to_tag =~ m|^-TEXTFLOW-|o )
    {
      local( $ntabs, $i );

      printf( OUTF "$from_tag" );

      $ntabs = 3 - length( $from_tag ) / 8;
      for ( $ntabs = $ntabs > 0 ? $ntabs : 1; $ntabs > 0; $ntabs-- )
        { printf( OUTF "\t" ); }

      print OUTF "# $to_tag";

      for ( $i = 0; $i <= $#unique_files; $i++ )
        { print OUTF "\t\t\t\t\t\t\t# $unique_files[$i]"; }
    }
    else
    {
      print OUTF "$to_tag";
    }
  }
	
  close( OUTF );
}



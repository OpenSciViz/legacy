#!/usr/local/bin/perl
# mif2gif.pl
#
# THIS FILE IS COPYRIGHT 1994 BY TELENOR RESEARCH, NORWAY
#
#                    ALL RIGHTS RESERVED.
#
# For rights granted to the public see the COPYRIGHT file
#
# Programming : Jon Stephenson von Tetzchner

# This script is part of the fm2html package.

$\ = "\n";		# set output record separator
$, = " ";		# set output field separator
$* = 0;			# set single line pattern matching

select( STDERR );	# all error messages go to stderr
$| = 1;			# make stderr unbuffered

$FALSE = 0;
$TRUE  = 1;


sub escape	# take string, insert backslash before special characters
{
  local( $_ ) = @_;
  s|([\.\+\?\*\'\"\(\)])|\\$1|g;
  $_;
}

sub basename	# take pathname, strip leading directories and optional suffix
{
  local( $_, $suffix ) = @_;
  m|/|o && s|^.*/([^/]*)$|$1|;
  $suffix = &escape( $suffix );
  s|(.*)$suffix|$1|;
  $_;
}


local( $prog ) = &basename( $0 );
local( $usage ) = "Usage: $prog [-c] [-r] [-q] filename...";

local( $crop ) = "";
local( $remove ) = $FALSE;
local( $quiet ) = $FALSE;
local( $qflag ) = "";

while ( $ARGV[0] =~ m|^-| )
{
  $_ = shift;
  last if m|^--|;

  if ( m|^-c$| )
  {
    $crop = "-crop"; next;
  }
  elsif ( m|^-r$| )
  {
    $remove = $TRUE; next;
  }
  elsif ( m|^-q$| )
  {
    $quiet = $TRUE; $qflag = "-q"; next;
  }

  print "$prog: ERROR: Invalid flag: \"$_\"";
  print "$usage";
  exit( 1 );
}

if ( $#ARGV == -1 )
{
  print "";
  print "$usage";
  print "";
  print "\tThe \"-c\" flag causes graphics to be cropped.";
  print "\tThe \"-r\" flag causes input MIF files to be removed";
  print "\tafter they are translated to PostScript.  The \"-q\" flag";
  print "\tcauses the program to operate in \"quiet\" mode.  The remaining";
  print "\tparameters specify the names of MIF files to translate to GIF.";
  print "";
  exit( 1 );
}

$quiet == $FALSE && print
  "\n$prog: Translating graphics files from MIF to PostScript";
system( "/usr/local/bin/mif2ps $qflag @ARGV" );

$quiet == $FALSE &&
  print "\n$prog: Translating graphics files from PostScript to GIF";

while ( @ARGV )
{
  local( $mif ) = shift;
  local( $ps ) = $mif;

  $ps =~ s|\.mif|.ps|; 
  system( "/usr/local/bin/ps2gif $qflag $crop -colors 50 -margin 2 $ps");

  if ( $remove == $TRUE )
    { system( "rm -f $mif" ); }
}

$quiet == $FALSE && print "\n$prog: Done";

exit( 0 );

<html><head><title> Fortran Programmer's Guide: 9 - Performance and Optimization 
</title></head>
<body bgcolor=#ffffff>
 
<center>
<a href="8_profiling.doc.html">
<img src="shared/previous_motif.gif" alt="Previous"></a>

<a href="10_parallel.doc.html">
<img src="shared/next_motif.gif" alt="Next"></a>
<a href="index.html">
<img src="shared/contents_motif.gif" alt="Contents"></a>
<a href="IX.html">
<img src="shared/index.gif" alt="Index"></a>
<a href="../index.html">
<img src="shared/up_motif.gif" alt="Doc Set"></a>
<a href="../../index.html">
<img src="shared/home.gif" alt="Home"></a>
</center>
<br><br>
 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr valign="top">
<td align="left">
<a name="367">
<h1><font size=7>
Performance and Optimization 
</font></h1>
</a></td>
<td><a name="1343">
<h1 align="right"><font size=7>
9 
</font></h1>
</a></td>
<td align="right"> <img src="chars/chapnumbar.gif"></td>
</tr></table>
<hr size="3" width="100%" align="left" noshade><a name="379">
This chapter considers some optimization techniques that may improve the performance of numerically intense Fortran programs. Proper use of algorithms, compiler options, library routines, and coding practices can bring significant performance gains. This discussion does not discuss cache, I/O, or system environment tuning. Parallelization issues are treated in the next chapter.  <p>
</a>
<a name="1972">
Some of the issues considered here are:<p>
</a>
<ul><a name="1973">
<p><li>Compiler options that may improve performance
</a>
<a name="1974">
<li>Compiling with feedback from runtime performance profiles
</a>
<a name="1975">
<li>Use of optimized library routines for common procedures
</a>
<a name="2828">
<li>Coding strategies to improve performance of key loops
</a>
</ul><a name="1991">
The subject of optimization and performance tuning is much too complex to be treated exhaustively here. However, this discussion should provide the reader with a useful introduction to these issues. A list of books that cover the subject much more deeply appears at the end of the chapter. <p>
</a>
<a name="2006">
Optimization and performance tuning is an art that depends heavily on being able to determine <em>what</em> to optimize or tune.  <p>
</a>
<a name="2011">
<p><hr size="8" align="left" width="30%" noshade>
<h2><font size=6><i></i> Choice of Compiler Options
</font></h2>
</a>
<a name="2022">
Choice of the proper compiler options is the first step in improving performance. Sun compilers offer a wide range of options that affect the object code. In the default case, where no options are explicitly stated on the compile command line, most options are <em>off</em>. To improve performance, these options must be explicitly selected.<p>
</a>
<a name="2023">
Performance options are normally off by default because most optimizations force the compiler to make assumptions about a user&#39;s source code. Programs that conform to standard coding practices and do not introduce hidden side effects should optimize correctly. However, programs that take liberties with standard practices may run afoul of some of the compiler&#39;s assumptions. The resulting code may run faster, but the computational results may not be correct.<p>
</a>
<a name="2024">
Recommended practice is to first compile with all options off, verify that the computational results are correct and accurate, and use these results and performance profile as a baseline. Then, proceed in steps&#45;&#45; recompiling with additional options and comparing execution results and performance against the baseline. If numerical results change, the program may have questionable code, which needs careful analysis to locate and reprogram. <p>
</a>
<a name="2046">
If performance does not improve significantly, or degrades, as a result of adding optimization options, the coding may not provide the compiler with opportunities for further performance improvements. The next step would then be to analyze and restructure the program at the source code level to achieve better performance.<p>
</a>
<a name="2067">
<h3><font size=5><i></i> Performance Option Reference
</font></h3>
</a>
<a name="2068">
The compiler options listed in <a href="9_perform.doc.html#2068">Table&#160;9-1</a> provide the user with a repertoire of strategies to improve the performance of a program over default compilation. Only some of the compilers&#39; more potent performance options appear below. A more complete list can be found in the <em>Fortran User&#39;s Guide</em>.<br><br><Table Border="1" Cellpadding="5">
<caption><a name="2071">
<p><center><h4><font size=3>Table &#160;9-1	 Some Effective Performance Options
</font></h4></center>
</a></caption>

<tr align="left" valign="baseline"><td><a name="2075">
<b> Action</b><br>
</a>
<td><a name="2077">
<b>  Option</b><br>
</a>

<tr valign="baseline"><td><a name="2079">
<p> Use various optimization options together
</a>
<td><a name="2081">
<p>  <code>-fast</code>
</a>

<tr valign="baseline"><td><a name="2083">
<p> Set compiler optimization level to <em>n</em>
</a>
<td><a name="2085">
<p>  <code>-O</code><em>n     </em>(<code>-O</code> <code>=</code> <code>-O3</code>)
</a>

<tr valign="baseline"><td><a name="2087">
<p> Specify target hardware
</a>
<td><a name="2089">
<p>  <code>-xtarget=</code><em>sys</em>
</a>

<tr valign="baseline"><td><a name="2091">
<p> Optimize using performance profile data (with <code>-O5</code>)
</a>
<td><a name="2093">
<p>  <code>-xprofile=use</code>
</a>

<tr valign="baseline"><td><a name="2115">
<p> Unroll loops by <em>n</em>
</a>
<td><a name="2117">
<p>  <code>-unroll=</code><em>n</em>
</a>

<tr valign="baseline"><td><a name="2121">
<p> Permit simplifications and optimization of floating-point 
</a>
<td><a name="2123">
<p>  <code>-fsimple=1|2</code>
</a>

<tr valign="baseline"><td><a name="2130">
<p> Perform dependency analysis to optimize loops
</a>
<td><a name="2132">
<p>  <code>-depend</code>
</a>


</Table>
 
<Table>
<tr><td>
</Table><p>
<p>
</a>
<a name="2146">
Some of these options will increase compilation time because they invoke a deeper analysis of the program. Some options work best when routines are collected into files along with the routines that call them (rather than splitting each routine into its own file); this allows the analysis to be global.       <p>
</a>
<a name="2171">
<h4><font size=4><i> <code>-fast</code>
</i></font></h4>
</a>
<a name="2172">
This single option selects a number of performance options that, working together, produce object code optimized for execution speed without an excessive increase in compilation time.<p>
</a>
<a name="2183">
The options selected by <code>-fast</code> are subject to change from one release to another, and not all are available on each platform:<p>
</a>
<ul><a name="2187">
<p><li><code>-xtarget=native</code> &#45; generates code optimized for the host architecture
</a>
<a name="2188">
<li><code>-O4</code> &#45; sets optimization level
</a>
<a name="2191">
<li><code>-libmil</code> &#45; inlines calls to some simple library functions
</a>
<a name="2194">
<li><code>-fsimple=1</code> &#45;  simplifies floating-point code (<em>SPARC only</em>)
</a>
<a name="2209">
<li><code>-dalign</code> &#45; uses faster, double word loads and stores (<em>SPARC only</em>)
</a>
<a name="2200">
<li><code>-xlibmopt</code> &#45; use optimized <kbd>libm</kbd> math library (<em>SPARC, PowerPC only</em>)
</a>
<a name="2865">
<li><code>-fns -ftrap=%none</code> &#45; turns off all trapping
</a>
<a name="2866">
<li><code>-depend</code> &#45; analyze loops for data dependencies (<em>SPARC only</em>)
</a>
<a name="3150">
<li><code>&#45;nofstore</code> &#45; disables forcing precision on expressions (<em>Intel only</em>)
</a>
</ul><a name="2220">
<code>-fast</code> provides a quick way to engage much of the optimizing power of the compilers. Each of the composite options may be specified individually, and each may have side effects to be aware of (discussed in the<em> Fortran User&#39;s Guide)</em>. Following <code>-fast</code> with additional options adds further optimizations. For example:<p>
</a>
<dl>
<a name="2257">
<p><dd><code>f77 -fast -O5</code>  ...
</a><p>
</dl>
<a name="2258">
sets the optimization to level 5 instead of 4.<p>
</a>
<a name="2812">
<blockquote><hr noshade size="1"><b>Note - </b> <code>-fast</code>  includes <code>-dalign</code> and <code>-native</code>.  These options may have unexpected side-effects for some programs. 
<hr noshade size="1"></blockquote>
</a><a name="2227">
<h4><font size=4><i> <code>-O</code>n
</i></font></h4>
</a>
<a name="2228">
No compiler optimizations are performed by the compilers unless a <code>-O</code> option is specified explicitly (or implicitly with macro options like <code>-fast</code>). In nearly all cases, specifying an optimization level for compilation improves program execution performance. On the other hand, higher levels of optimization increase compilation time and may significantly increase code size. <p>
</a>
<a name="2263">
For most cases, level <code>-O3</code> is a good balance between performance gain, code size, and compilation time. Level <code>-O4</code> adds automatic inlining of calls to routines contained in the same source file as the caller routine, among other things. Level <code>-O5</code> adds more aggressive optimization techniques that would not be applied at lower leves. In general, levels above <code>-O3</code> should be specified only to those routines that make up the most compute-intensive parts of the program and thereby have a high certainty of improving performance. (There is no problem linking together parts of a program compiled with different optimization levels.)<p>
</a>
<a name="2293">
<h4><font size=4><i> SPARC: Optimization With Runtime Profile Feedback
</i></font></h4>
</a>
<a name="2287">
The compiler applies its optimization strategies at level <code>O3</code> and above much more efficiently if combined with <code>-xprofile=use</code>. With this option (available only on SPARC processors), the optimizer is directed by a runtime execution profile produced by the program (compiled with <code>-xprofile=collect</code>) with typical input data. The feedback profile indicates to the compiler where optimization will have the greatest effect. This may be particularly important with <code>-O5</code>. Here&#39;s a typical example of profile collection with higher optimization levels: <br><br><Table Border="1" Cellpadding="5">
<caption></caption>

<tr valign="baseline"><td><a name="2314">
<pre>demo% <kbd><b>f77 -o prg -fast -xprofile=collect prg.f ...
</b></kbd></pre>
</a>
<a name="2315">
<pre>demo% <kbd><b>prg</b></kbd>   <em> 
</em></pre>
</a>
<a name="2801">
<pre>demo% <kbd><b>f77 -o prgx -fast -O5 -xprofile=use:prg.profile prg.f ...
</b></kbd></pre>
</a>
<a name="2342">
<pre>demo% <kbd><b>prgx</b></kbd>  
</pre>
</a>


</Table>
 
<Table>
<tr><td>
</Table><p>
<p>
</a>
<a name="2311">
The first compilation above generates an executable that produces statement coverage statistics when run. The second compilation uses this performance data to guide the optimization of the program.<p>
</a>
<a name="2805">
(See<em> Fortran User&#39;s Guide </em>and<em> Performance Profiling Tools</em> for details on  <code>-xprofile</code> options.)<p>
</a>
<a name="2357">
<h4><font size=4><i> <code>-dalign</code>  
</i></font></h4>
</a>
<a name="2362">
With <code>-dalign</code> the compiler is able to generate double-word load/store instructions whenever possible. Programs that do much data motion may benefit significantly when compiled with this option. (It is one of the options selected by <code>-fast</code>.) The double-word instructions are almost twice as fast as the equivalent single word operations. <p>
</a>
<a name="2369">
However, users should be aware that using <code>-dalign</code> (and therefore <code>-fast</code>) may cause problems with some programs that have been coded expecting a specific alignment of data in <code>COMMON</code> blocks. With -<code>dalign</code>, the compiler may add padding to ensure that all double (and quad) precision data (either REAL or <code>COMPLEX</code>) are aligned on double word boundaries, with the result that:<p>
</a>
<ul><ul type="disc">
<a name="2374">
<p><li><code>COMMON</code> blocks may be larger than expected due to added padding
</a>
<a name="2375">
<li>All program units sharing <code>COMMON</code> must be compiled with <code>-dalign</code> if any one of them is compiled with <code>-dalign</code> 
</a>
</ul>
</ul><a name="2378">
For example, a program that writes data by aliasing an entire <code>COMMON</code> block of mixed data types as a single array may not work properly with <code>-dalign</code> because the block will be larger (due to padding of double and quad precision variables) than the program expects. <p>
</a>
<a name="2280">
<h4><font size=4><i>SPARC:   <code>-depend  </code><i><b>(</b></i><kbd>f77</kbd><i><b> only)</b></i>
</i></font></h4>
</a>
<a name="2395">
Adding <code>-depend</code> to optimization levels <code>-O3</code> and higher (on SPARC processors) extends the compiler&#39;s ability to optimize <code>DO</code> loops and loop nests. With this option, the optimizer analyzes inter-iteration loop dependencies to determine whether or not certain transformations of the loop structure can be performed. Only loops without dependencies can be restructured. However, the added analysis may increase compilation time.<p>
</a>
<a name="2398">
<h4><font size=4><i> <code>-fsimple=2</code>  (<kbd>f77</kbd> only)
</i></font></h4>
</a>
<a name="2399">
Unless directed to, the compiler does not attempt to simplify floating-point computations (this is the default, <code>-fsimple=0</code>). With the <code>-fast</code> option, <br><code>-fsimple=1 </code>is used and some conservative assumptions are made. Adding <br><code>-fsimple=2 </code>enables the optimizer to make further simplifications with the understanding that this may cause some programs to produce slightly different results due to rounding effects. If <code>-fsimple</code> level 1 or 2 is used, all program units should be similarly compiled to insure consistent numerical accuracy, <p>
</a>
<a name="2409">
<h4><font size=4><i> <code>-unroll=</code>n
</i></font></h4>
</a>
<a name="2410">
Unrolling short loops with long iteration counts can be profitable for some routines. However, unrolling can also increase program size and may even degrade performance of other loops. With <em>n</em>=1, the default, no loops are unrolled automatically by the optimizer. With <em>n</em> greater than 1, the optimizer  attempts to unroll loops up to a depth of <em>n</em>. If a <code>DO</code> loop with a variable loop limit can be unrolled, both an unrolled version and the original loop are compiled. A runtime test on iteration count determines whether or not executing the unrolled loop is inappropriate. Loop unrolling, especially with simple one or two statement loops, increases the amount of computation done per iteration and provides the optimizer better opportunities to schedule registers and simplify operations. The tradeoff between number of iterations, loop complexity, and choice of unrolling depth is not easy to determine, and some experimentation may be needed. <p>
</a>
<a name="2421">
The example that follows shows how a simple loop might be unrolled to a depth of four with <code>-unroll=4</code> (the source code is not changed with this option):<br><br><Table Border="1" Cellpadding="5">
<caption></caption>

<tr valign="baseline"><td><a name="2426">
<p>Original Loop:
</a>
<a name="2430">
<pre>    DO I=1,20000
</pre>
</a>
<a name="2431">
<pre>       X(I) = X(I) + Y(I)*A(I)
</pre>
</a>
<a name="2432">
<pre>    END DO
</pre>
</a>
<a name="2433">
<pre>
</pre>
</a>
<a name="2434">
<p>Unrolled by 4 <em>compiles as</em>:
</a>
<a name="2435">
<pre>  <em>  DO I=1, 19997,4
</em></pre>
</a>
<a name="2436">
<pre><em>       TEMP1 = X(I) + Y(I)*A(I)
</em></pre>
</a>
<a name="2441">
<pre><em>       TEMP2 = X(I+1) + Y(I+1)*A(I+1)
</em></pre>
</a>
<a name="2446">
<pre><em>       TEMP3 = X(I+2) + Y(I+2)*A(I+2)
</em></pre>
</a>
<a name="2451">
<pre><em>       X(I+3) = X(I+3) + Y(I+3)*A(I+3)
</em></pre>
</a>
<a name="2456">
<pre><em>       X(I) = TEMP1
</em></pre>
</a>
<a name="2457">
<pre><em>       X(I+1) = TEMP2
</em></pre>
</a>
<a name="2458">
<pre><em>       X(I+2) = TEMP3
</em></pre>
</a>
<a name="2459">
<pre><em>    END DO
</em></pre>
</a>


</Table>
 
<Table>
<tr><td>
</Table><p>
<p>
</a>
<a name="2494">
This example shows a simple loop with a fixed loop count. The restructuring is more complex with variable loop counts. <p>
</a>
<a name="2502">
<h4><font size=4><i> <code>-xtarget=</code>system 
</i></font></h4>
</a>
<a name="2503">
The performance of some programs may benefit if the compiler has an accurate description of the target computer hardware. When program performance is critical, the proper specification of the target hardware could be very important. This is especially true when running on the newer SPARC processors. However, for most programs and older SPARC processors, the performance gain may be negligible and a generic specification may be sufficient.<p>
</a>
<a name="2504">
The<em> Fortran User&#39;s Guide</em> lists all the system names recognized by <code>-xtarget=</code>. For any given system name (for example, <code>ss1000</code>, for SPARC Server 1000), -<code>xtarget</code> expands into a specific combination of <code>-xarch</code>, <code>-xcache</code>, and <code>-xchip</code> that properly matches that system. The optimizer uses these specifications to determine strategies to follow and instructions to generate.<p>
</a>
<a name="2508">
The special setting <code>-xtarget=native</code> enables the optimizer to compile code targeted at the host system (the system doing the compilation). This is obviously useful when compilation and execution are done on the same system. When the execution system is not known, it is desirable to compile for a <em>generic</em> architecture, therefore <code>-xtarget=generic</code> is the default, although this may produce suboptimal performance.<p>
</a>
<a name="2422">
<p><hr size="8" align="left" width="30%" noshade>
<h2><font size=6><i></i> Other Performance Strategies
</font></h2>
</a>
<a name="2498">
Assuming that you have experimented with using a variety of optimization options, compiling your program and measuring actual runtime performance, the next step might be to look closely at the Fortran source program to see what further tuning can be tried.<p>
</a>
<a name="2516">
Focusing on just those parts of the program that use most of the compute time, you might consider the following strategies:<p>
</a>
<ul><a name="2517">
<p><li>Replace handwritten procedures with calls to equivalent optimized libraries
</a>
<a name="2518">
<li>Remove I/O, calls, and unnecessary conditional operations from key loops
</a>
<a name="2520">
<li>Eliminate aliasing that might inhibit optimization
</a>
<a name="2523">
<li>Rationalize tangled, spaghetti-like code to use block <code>IF</code>
</a>
</ul><a name="2527">
These are some of the good programming practices that tend to lead to better performance. It is possible to go further, hand-tuning the source code for a specific hardware configuration. However, these attempts may only further obscure the code and make it even more difficult for the compiler&#39;s optimizer to achieve significant performance improvements. Excessive hand-tuning of the source code may hide the original intent of the procedure and could have a significantly detrimental effect on performance for different architectures.<p>
</a>
<a name="2953">
<h3><font size=5><i></i> Use Optimized Libraries
</font></h3>
</a>
<a name="2547">
In most situations, optimized commercial or shareware libraries perform standard computational procedures far more efficiently than you could by coding them by hand. <p>
</a>
<a name="2629">
For example, the Sun Performance Library<sup>TM</sup> is a suite of highly optimized mathematical subroutines based on the standard LAPACK, BLAS, FFTPACK, VFFTPACK, and LINPACK libraries. Performance improvement using these routines can be significant when compared with hand coding. <p>
</a>
<a name="2550">
<h3><font size=5><i></i> Eliminate Performance Inhibitors
</font></h3>
</a>
<a name="2551">
Use the profiling techniques described in the previous chapter to identify the key computational parts of the program. Then, carefully analyze the loop or loop nest to eliminate coding that might either inhibit the optimizer from generating optimal code or otherwise degrade performance. See also the chapter on Porting. Many of the nonstandard coding practices that make portability difficult may also inhibit optimization by the compiler. <p>
</a>
<a name="2751">
Reprogramming techniques that improve performance are dealt with in more detail in some of the reference books listed at the end of the chapter. Three major approaches are worth mentioning here: <p>
</a>
<a name="2725">
<h4><font size=4><i> Remove I/O From Key Loops
</i></font></h4>
</a>
<a name="2729">
I/O within a loop or loop nest enclosing the significant computational work of a program will seriously degrade performance. The amount of CPU time spent in the I/O library may be a major portion of the time spent in the loop. (I/O also causes process interrupts, thereby degrading program throughput.) By moving I/O out of the computation loop wherever possible, the number of calls to the I/O library can be greatly reduced. <p>
</a>
<a name="2730">
<h4><font size=4><i> Eliminate Subprogram Calls
</i></font></h4>
</a>
<a name="2731">
Subroutines called deep within a loop nest may get called thousands of times. Even if the time spent in each routine per call is small, the total effect may be substantial. Also, subprogram calls inhibit optimization of the loop that contains them because the compiler cannot make assumptions about the state of registers over the call.<p>
</a>
<a name="2768">
Automatic inlining of subprogram calls (using <code>-inline=</code><em>x,y,..z</em>, or <code>-O4</code>) is one way to let the compiler replace the actual call with the subprogram itself (<em>pulling</em> the subprogram into the loop). The subprogram source code for the routines that are to be inlined be must be found in the same file as the calling routine.<p>
</a>
<a name="2766">
There are other ways to eliminate subprogram calls:<p>
</a>
<ul><a name="2767">
<p><li>Use statement functions. If the external function being called is a simple math function, it may be possible to rewrite the function as a statement function or set of statement functions. Statement functions are compiled in-line and can be optimized. 
</a>
<a name="2775">
<p><li>Push the loop into the subprogram. That is, rewrite the subprogram so that it can be called fewer times (outside the loop) and operate on a vector or array of values per call. 
</a>
</ul><a name="2552">
<h4><font size=4><i> Rationalize Tangled Code
</i></font></h4>
</a>
<a name="2650">
Complicated conditional operations within a computationally intensive loop can dramatically inhibit the compiler&#39;s attempt at optimization. In general, a good rule to follow is to eliminate all arithmetic and logical <code>IF</code>&#39;s, replacing them with block <code>IF</code>&#39;s:<br><br><Table Border="1" Cellpadding="5">
<caption></caption>

<tr valign="baseline"><td><a name="2656">
<pre>Original Code:
</pre>
</a>
<a name="2657">
<pre>    IF(A(I)-DELTA) 10,10,11
</pre>
</a>
<a name="2658">
<pre>10  XA(I) = XB(I)*B(I,I)
</pre>
</a>
<a name="2659">
<pre>    XY(I) = XA(I) - A(I)
</pre>
</a>
<a name="2665">
<pre>    GOTO 13
</pre>
</a>
<a name="2666">
<pre>11  XA(I) = Z(I)
</pre>
</a>
<a name="2667">
<pre>    XY(I) = Z(I)
</pre>
</a>
<a name="2668">
<pre>    IF(QZDATA.LT.0.) GOTO 12
</pre>
</a>
<a name="2669">
<pre>    ICNT = ICNT + 1
</pre>
</a>
<a name="2673">
<pre>    ROX(ICNT) = XA(I)-DELTA/2.
</pre>
</a>
<a name="2670">
<pre>12  SUM = SUM + X(I)
</pre>
</a>
<a name="2674">
<pre>13  SUM = SUM + XA(I)
</pre>
</a>
<a name="2681">
<pre>
</pre>
</a>
<a name="2682">
<pre>Untangled Code:
</pre>
</a>
<a name="2683">
<pre>    IF(A(I).LE.DELTA) THEN
</pre>
</a>
<a name="2686">
<pre>      XA(I) = XB(I)*B(I,I)
</pre>
</a>
<a name="2687">
<pre>      XY(I) = XA(I) - A(I)
</pre>
</a>
<a name="2684">
<pre>    ELSE
</pre>
</a>
<a name="2697">
<pre>      XA(I) = Z(I)
</pre>
</a>
<a name="2698">
<pre>      XY(I) = Z(I)
</pre>
</a>
<a name="2699">
<pre>      IF(QZDATA.GE.0.) THEN
</pre>
</a>
<a name="2700">
<pre>        ICNT = ICNT + 1
</pre>
</a>
<a name="2701">
<pre>        ROX(ICNT) = XA(I)-DELTA/2.
</pre>
</a>
<a name="2710">
<pre>      ENDIF
</pre>
</a>
<a name="2702">
<pre>      SUM = SUM + X(I)
</pre>
</a>
<a name="2718">
<pre>    ENDIF
</pre>
</a>
<a name="2703">
<pre>    SUM = SUM + XA(I)
</pre>
</a>


</Table>
 
<Table>
<tr><td>
</Table><p>
<p>
</a>
<a name="2553">
Using block <code>IF</code> not only improves the opportunities for the compiler to generate optimal code, it also improves readability and assures portability.<p>
</a>
<a name="2155">
<p><hr size="8" align="left" width="30%" noshade>
<h2><font size=6><i></i> Further Reading                    
</font></h2>
</a>
<a name="916">
The following reference books provide more details:<p>
</a>
<ul><a name="446">
<p><li><em>Fortran 77 Language Reference</em>, Sun Microsystems, Inc.
</a>
<a name="447">
<p><li><em>Numerical Computation Guide</em>, Sun Microsystems, Inc.
</a>
<a name="362">
<p><li><em>Performance Profiling Tools</em>, Sun Microsystems, Inc.
</a>
<a name="448">
<p><li><em>Programming Pearls</em>, by Jon Louis Bentley, Addison Wesley
</a>
<a name="449">
<p><li><em>More Programming Pearls</em>, by Jon Louis Bentley, Addison Wesley
</a>
<a name="450">
<p><li><em>Writing Efficient Programs</em>, by Jon Louis Bentley, Prentice Hall
</a>
<a name="451">
<p><li><em>FORTRAN Optimization</em>, by Michael Metcalf, Academic Press 1982
</a>
<a name="452">
<p><li><em>Optimizing FORTRAN Programs, </em>by C. F. Schofield Ellis Horwood Ltd., 1989
</a>
<a name="453">
<p><li><em>A Guidebook to Fortran on Supercomputers</em>, John Levesque, Joel Williamson, Academic Press, 1989
</a>
<a name="3204">
<p><li><em>High Performance Computing, </em>Kevin Dowd, O&#39;Reilly &amp; Associates, 1993<em></em>
</a>
</ul><a name="3030">
<p>
</a>

<br>
 
<center>
<a href="8_profiling.doc.html">
<img src="shared/previous_motif.gif" alt="Previous"></a>

<a href="10_parallel.doc.html">
<img src="shared/next_motif.gif" alt="Next"></a>
<a href="index.html">
<img src="shared/contents_motif.gif" alt="Contents"></a>
<a href="IX.html">
<img src="shared/index.gif" alt="Index"></a>
<a href="../index.html">
<img src="shared/up_motif.gif" alt="Doc Set"></a>
<a href="../../index.html">
<img src="shared/home.gif" alt="Home"></a>
</center>
<hr noshade> <br>
 


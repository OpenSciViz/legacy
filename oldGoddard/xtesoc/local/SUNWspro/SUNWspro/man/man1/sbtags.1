.TH SBTAGS 1 "05 Apr 1994"
.\"
.\" This text is in the public domain.  You are welcome to use it for
.\" any purpose.  It is not warranted to accurately describe anything.
.\" It was derived by Sam Kendall from the original ctags man page.
.\" It was further derived by Eduardo Pelegri-Llopart for sbtags
.\"
.\"
.UC 4
.SH NAME
etags, ctags \- create tags files for GNU Emacs and ex/vi
.br
sbtags \- create tags files for Source Browser
.SH SYNOPSIS
.B sbtags
file ...
.br
.B etags
[ 
.B \-aDS
] [
.B \-o
tagsfile ] file ...
.br
.B ctags
[ 
.B \-aBdFStTuwvx
] [
.B \-o
tagsfile ] file ...
.SH AVAILABILITY
Available in release SW3.0 and later.
.SH DESCRIPTION
.I Sbtags
creates .bd data for the Source Browser component of SPARCworks,
.I etags
makes a tags file for GNU Emacs Version 18, and
.I ctags
makes a tags file for
.IR ex (1)/ vi (1),
from the specified source files.
A tags file gives the locations of the definitions of named objects
(functions, variables, macros, types) in a group of source files.
Each name recorded is called a `tag'.
In etags and ctags, given a tag by the user, the editor uses a tags
file to map
the name into a location in a source file, and takes the user there.
In sbtags, the editor uses the .bd information via the source browser
engine, available using ToolTalk.
.PP
SBtags collects the same definition data as etags except for storing it in
a different format.  In addition, it collects some basic call graph data
(currently only for the C languages).
.PP
The following filename extensions are understood:
.TP 10
.B "\.c  \.h  \.y  \.C  \.H  \.cxx  \.hxx  \.ixx  \.hc  \.cc  \.hpp  \.cpp"
C or C++ source files.
.IR Etags :
by default,
files are searched for function, member function, macro function,
macro constant, and type definitions.
.IR Ctags :
by default, files are searched for function and macro function definitions;
use the
.B \-Tdw
options to also search for the rest and to suppress annoying warnings.
.\" .TP 10
.\" .B "\.cs  \.hs"
.\" Dataparallel C source files.
.\" (Dataparallel C, formerly known as C*, is a parallel cousin of C++.)
.\" NOTE: Dataparallel C support is still in the source; I'll document
.\" it if someone tells me they are actually using it.
.\" -kendall@centerline.com 6/4/92
.TP 10
.B "\.tex  \.aux  \.bbl"
LaTex source files.
.TP 10
.B "\.s  \.a"
Assembly language source files.
.TP 10
.B "\.l  \.el  \.lisp  \.cl  \.clisp"
Emacs Lisp or Common Lisp source files.
.TP 10
.B "\.scm  \.sm  \.scheme  \.SM  \.SCM  \.scm\.\fIn\fP  \.SCM\.\fIn\fP"
Scheme source files.
.PP
Other files are first examined to see if they contain any
Pascal or Fortran routine definitions; if not, they are processed again
looking for C or C++ definitions.
.SH "OPTIONS"
.TP 5
.B \-a
append to tags file.  This option is meaningless for sbtags
(which generates one .bd file per each source file).
.TP 5
.B \-s
forces sbtags behavior (creation of .bd files).
.TP 5
.B \-e
forces etags behavior (creation of TAGS file).
.TP 5
.BR \-o " tagsfile"
specify tags file to output.
Default is
.B TAGS
for
.I etags
and
.B tags
for
.IR ctags .
A tagsfile of
.B \-
means standard output.
.SH "OPTIONS FOR C AND C++"
.TP 5
.B \-d
create tags for macro constant definitions
(default for
.BR etags ).
.TP 5
.B \-D
do not create tags for macro constant definitions (default for
.BR ctags ).
This can halve the size of the TAGS file.
.TP 5
.B \-S
do
.I not
assume that a `}' in column 0 indicates the end of a function body;
the default is to make this assumption.
Use this option with code that is poorly indented.
Do not use this option with code containing unbalanced curly braces,
as can happen with some usage patterns of conditional compilation.
.TP 5
.B \-t
create tags for typedefs.
.TP 5
.B \-T
create tags for type definitions (including typedefs)
and member function definitions (default for
.BR etags ).
.SH "CTAGS ONLY"
.TP 5
.B \-F
use forward searching patterns (/.../) (default).
.TP 5
.B \-B
use backward searching patterns (?...?).
.TP 5
.B \-w
suppress warning diagnostics.
.TP 5
.B \-u
causes the specified files to be
.I updated
in tags. All references to them are deleted,
and the new values are appended to the file.
(note: this option is rather slow;
it is usually faster to simply rebuild the
.I tags
file.)
.TP 5
.B \-x
produce a list of object names and include the line number and file
name on which each is defined as well as the text of that line.
Print it on the standard output.  This is a simple index
which can be printed as an off-line readable function index.
.TP 5
.B \-v
produce an index of the form expected by
.IR vgrind (1).
Print it on the standard output.
This listing contains the function name,
file name, and page number
(assuming 64 line pages).
Since the output will be sorted into lexicographic order,
you might want to run the output through
.BR "sort \-f" .
Sample use:
.nf
		ctags \-v files | sort \-f > index
		vgrind \-x index
.fi
.PP
.I Ctags
treats the tag
.I main
specially in C programs.
The tag formed is created by prepending
.I M
to the name of the file, remove any trailing .c,
and remove any leading pathname components.
This makes use of
.I ctags
practical in directories with more than one program.
.SH "HOW TO USE WITH GNU EMACS"
First, insure that
.B "tags.el"
from this distribution is loaded, or is in a directory in your
load-path.
Then you can use these commands:
.TP 12
.B "Meta-x visit-tags-table"
Visit a TAGS file.
.TP 12
.B "Meta-."
Find a definition for a tag.
The default tag is the identifier under the cursor.
There is name completion in the minibuffer;
typing
.B "foo TAB"
completes the identifier starting with `foo' (`foobar',
for example) or lists the alternatives.
Typing
.B "Classname::?"
lists all member functions of `Classname'.
.TP 12
.B "Meta-,"
Find the next definition for the tag.
Exact matches are found first, followed
by less exact matches.
.PP
For more commands, see the
.I Tags
topic in the Emacs info tree.
.SH "HOW TO USE WITH VI"
Once you have created a tags file, any use of
.I vi
in the same directory uses that tags file.
Available commands are:
.TP 12
.B "vi -t \fItag\fR"
Starts up vi at the definition of tag.
.TP 12
.B "Control-]"
Find the tag under the cursor.
.TP 12
.B ":ta \fItag\fR"
Find a tag.
.PP
These commands finds the first definition of tag.
There is no way to find other definitions;
this is a limitation of
.IR vi .
For more information, see the
.I vi
documentation.
.SH "ENVIRONMENT VARIABLES"
.DT
TEXTAGS	additional control sequences for LaTex
.SH FILES
TAGS		default \fIetags\fP output tags file
.br
tags		default \fIctags\fP output tags file
.SH SEE ALSO
emacs(1) (GNU Emacs), ex(1), vi(1)
.br
.BR sbcleanup (1),
.BR sbquery (1),
.BR sbrowser (1),
.BR sbinit (4).
.br
.IR Tags ,
a topic under
.I Emacs
in the GNU Emacs info tree; type
.B "C-H i"
to read info.
.br
.IR Completion ,
a topic under
.I Emacs
in the GNU Emacs info tree.
.br
sbinit
.SH AUTHOR
Ken Arnold wrote ctags.
Contributors include Jim Kleckner, Bill Joy, Eduardo Pelegri-Llopart, and
Richard Stallman.
Sam Kendall (kendall@centerline.com) added C++ support and maintains
this version.
Eduardo Pelegri-Llopart added sbtags.
.SH BUGS
.IR Ex (1)/ vi (1),
and thus
.IR ctags ,
cannot handle more than one tag with the same name;
all but the first are discarded and a warning is generated.
This is a problem for many languages, but particularly for C++,
which has overloaded functions,
and for Pascal, which can have procedures in
different blocks with the same name.
This is why the
.B \-T
and
.B \-d
options are not the default for
.IR ctags .
.I Emacs
and
.I etags
do not share this limitation.
Neither does
.IR sbtags .
.PP
Tricky macros in C and C++ (for example, macros that implement
parameterized types) can lead to spurious or missing tag entries.
.PP
Does not record C/C++ macros defined to nothing.
.PP
Does not understand C++ nested classes, templates, or exception
specifications.
.PP
Recognition of functions, subroutines and procedures
for Fortran and Pascal
is performed is a very simple manner.
.PP
The method of deciding whether to look for C or Pascal
and FORTRAN functions is heuristic.
.PP
Should know about Pascal types.
.PP
.B \-x
seems broken.
On platforms that have it, use
.B "/usr/ucb/ctags \-x"
instead.
.PP
Use of
.B \-tx
shows only the last line of each typedef.


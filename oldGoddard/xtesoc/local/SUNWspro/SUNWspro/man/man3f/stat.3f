.\" @(#)stat.3f 1.4 86/01/02 SMI; from UCB 4.2
.TH STAT 3F "12 June 1991"
.SH NAME
stat, lstat, fstat \- get file status
.SH SYNOPSIS
.\"
.IX stat "" \fLstat\fP
.IX lstat "" \fLlstat\fP
.IX fstat "" \fLfstat\fP
.IX file status
.IX status "of file"
.\"
.B integer function stat(name,statb)
.br
.B character*(*) name
.br
.B integer statb(13)
.sp 1
.B integer function lstat(name,statb)
.br
.B character*(*) name
.br
.B integer statb(13)
.sp 1
.B integer function fstat(lunit,statb)
.br
.B integer statb(13)
.\"
.SH DESCRIPTION
These routines return detailed information about a file.
.br
The functions
.B stat
and
.B lstat
do the query by 
.IR filename .
.br
The function
.B fstat
does the query by FORTRAN logical unit
.IR lunit .
.br
The value of each function is zero if successful, and an error code otherwise.
.br
The variable 
.I statb 
receives the status structure for the file.
.sp 1
.B Calling Sequences:
.sp 1
.I stat :
.br
.B integer stat, statb(13)
.br
.B character name*(*)
.br
.B "ierr = stat ( name, statb )"
.sp 1
.I fstat :
.br
.B integer fstat, logunit, statb(13)
.br
.B "ierr = fstat ( logunit, statb )"
.sp
.I lstat :
.br
.B integer lstat, statb(13)
.br
.B character name*(*)
.br
.B "ierr = lstat ( name, statb )"
.sp
.\".in -8
The meaning of the information returned in array
.I statb
is as described for the structure
.I stat
under
.IR stat (2).
.br
Spare values are not included. The order is shown below:
.sp
.nf
.ta 1i 1.7i 2.5i
	statb(1)	 device inode resides on 
	statb(2)	 this inode's number 
	statb(3)	 protection 
	statb(4)	 number of hard links to the file 
	statb(5)	 user-id of owner 
	statb(6)	 group-id of owner 
	statb(7)	 the device type, for inode that is device 
	statb(8)	 total size of file 
	statb(9)	 file last access time 
	statb(10)	 file last modify time 
	statb(11)	 file last status change time 
	statb(12)	 optimal blocksize for file system i/o ops 
	statb(13)	 actual number of blocks allocated 
.fi
.SH FILES
libF77.a
.SH "SEE ALSO"
stat(2), access(3F), perror(3F), time(3F)
.SH BUGS
Pathnames can be no longer than MAXPATHLEN as defined in <sys/param.h>.

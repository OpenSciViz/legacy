.\" @(#)f77_floatingpoint.3f 1.2 88/03/19 SMI;
.TH F77_FLOATINGPOINT 3F "28 October 1991"
.SH NAME
f77_floatingpoint \- FORTRAN IEEE floating-point definitions
.SH SYNOPSIS
.B #include <f77/f77_floatingpoint.h>
.SH DESCRIPTION
.\"
.IX "floating-point" "ieee definitions" "" "\s-1IEEE\s0 definitions" 
.IX "f77_floatingpoint ieee definitions" "" "\fLf77_floatingpoint\fP \s-1IEEE\s0 definitions" 
.IX ieee "floating-point" \s-1IEEE\s0
.\"
This file defines constants and types
used to implement standard floating-point
according to ANSI/IEEE Std 754-1985.
Use these constants and types to write more easily understood 
.B .F 
source files
that will undergo automatic preprocessing prior to compilation.
.PP
IEEE Rounding Modes:
.TP 20
.SM
fp_direction_type
The type of the IEEE rounding direction mode.
Note that the order of enumeration varies according to hardware.
.TP 20
.SM
fp_precision_type
The type of the IEEE rounding precision mode.
Only for systems with extended precision such
as Sun-3's with 68881's.
.PP
SIGFPE handling:
.TP 20
.SM
sigfpe_code_type
The type of a SIGFPE code.
.TP 20
.SM
sigfpe_handler_type
The type of a user-definable SIGFPE exception handler called
to handle a particular SIGFPE code.
.TP 20
.SM
SIGFPE_DEFAULT
A macro indicating the default SIGFPE exception handling, namely
for IEEE exceptions to continue with a default result,
and to abort for other SIGFPE codes.
.TP 20
.SM
SIGFPE_IGNORE
A macro indicating an alternate SIGFPE exception handling, namely
to ignore and continue execution.
.TP 20
.SM
SIGFPE_ABORT
A macro indicating an alternate SIGFPE exception handling, namely
to abort with a core dump.
.PP
IEEE Exception Handling:
.TP 20
.SM
N_IEEE_EXCEPTION
The number of distinct IEEE floating-point exceptions.
.TP 20
.SM
fp_exception_type
The type of the N_IEEE_EXCEPTION exceptions.
Each exception is given a bit number.
.TP 20
.SM
fp_exception_field_type
The type intended to hold at least N_IEEE_EXCEPTION
bits corresponding to the IEEE exceptions
numbered by
.IR fp_exception_type.
Thus 
.IR fp_inexact 
corresponds to the least significant bit
and 
.IR fp_invalid 
to the fifth least significant bit.
Some operations may set more than one exception.
.PP
IEEE Classification:
.TP 20
.SM
fp_class_type
A list of the classes of IEEE floating-point values and symbols.
.SH FILES
include/f77_floatingpoint.h
.SH "SEE ALSO"
f77_ieee_environment(3F)

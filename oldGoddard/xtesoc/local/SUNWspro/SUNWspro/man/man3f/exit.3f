.\" @(#)exit.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH EXIT 3F  "20 July 1992"
.SH NAME
exit \- terminate process with status
.SH SYNOPSIS
.\"
.IX exit "" \fLexit\fP
.IX "terminate with status"
.IX status "at terminate"
.\"
.B subroutine exit (status)
.br
.B integer status
.SH DESCRIPTION
.B exit
flushes and closes all the process's files, and notifies the parent process
if it is executing a
.BR wait .
The low-order 8 bits of 
.I status
are available to the parent process.
These 8 bits are shifted left 8 bits,
and all other bits are zero.
(Therefore
.B status
should be in the range 256 \- 65280)
.PP
This call will never return.
.PP
The C function
.B exit
may cause cleanup actions before the
final `sys exit'.
.PP
If you call
.B exit
without an argument, you will get a warning message,
and a zero will be automatically provided as an argument.
.SH FILES
libF77.a
.SH "SEE ALSO"
exit(2), fork(2), fork(3f), wait(2), wait(3f)

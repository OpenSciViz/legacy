'\" t
.\" @(#)convert_external.3m 1.8 93/09/01 SMI; 
.TH convert_external 3M "1 Sep 1993"
.SH NAME
convert_external \- convert external binary data formats
.SH SYNOPSIS
.LP
.B cc
.RI "[ " "flag" " \|.\|.\|. ] " "file" " \|.\|.\|."
.B \-lsunmath -lm
.RI "[ " "library" " \|.\|.\|. ]"
.LP
.B #include <sunmath.h>
.LP
.ft B
.nf
enum convert_external_arch_t = {
	convert_external_sparc, convert_external_pc,
	convert_external_vax, convert_external_vaxg,
	convert_external_s370, convert_external_cray };

enum convert_external_type_t = {
	convert_external_signed,
	convert_external_unsigned,
	convert_external_float };

enum convert_external_rounding_t = {
	convert_external_common = \-1,
	convert_external_biased = \-2,
	convert_external_away = \-3 };

typedef struct {
	enum convert_external_arch_t arch ;	/* format architecture */
	enum convert_external_type_t type ;	/* format type */	
	int size ; 				/* format size in 8-bit bytes */
	} convert_external_t ;
.fi
.ft R
.LP
.BI "fp_exception_field_type convert_external(const char *" "src_p" ,
.BI "convert_external_t " "src_format" ,
.BI "char *" "dst_p" ,
.BI "convert_external_t " "dst_format" ,
.BI "int " "rounding" ,
.BI "int " "n" );
.LP
.BI "fp_exception_field_type convert_external_(const char *" "src_p" ,
.BI "const convert_external_t *" "src_format_p" ,
.BI "char *" "dst_p" ,
.BI "const convert_external_t *" "dst_format_p" ,
.BI "const int *" "rounding_p" ,
.BI "const int *" "n_p" );
.LP
.SH DESCRIPTION
.IX "convert_external" "convert external binary data formats"
.LP
.B convert_external(\|)
is used in C programs to convert between binary data on non-SPARC
systems and SPARC binary data.  The data may be signed integers, unsigned
integers, or floating-point data.
.B convert_external_(\|)
is used in FORTRAN programs (CALL CONVERT_EXTERNAL(...))
for the same purpose, with all parameters call-by-reference.
.br
.ne 6
.LP
The
.B convert_external_t
type describes the supported formats; for each
architecture, signed and unsigned integers of sizes 1, 2, 4, 8, or 16
bytes, and floating-point types of sizes 4, 8, 10, 12, or 16 bytes in size
are supported.  If an improper size is specified for the source or destination,
no conversion occurs and (1<<fp_division) is returned, an exception
that can't arise otherwise.
.ne 15
.LP
Supported architectures and formats include 
.sp
.TS
center ;
l c c c c c c .
external_t	endian-	float*4	float*8	float*10	float*12	float*16
architecture	ness	format	format	format	format	format

sparc	big	IEEE	IEEE	none	MC68881	IEEE
		single	double		extended	quad
pc	little	IEEE	IEEE	i80x87	i80960KB	IEEE
		single	double	extended	extended	quad
vax	little	VAX F	VAX D	none	none	VAX H
vaxg	little	VAX F	VAX G	none	none	VAX H
s370	big	S/370	S/370	none	none	S/370
		single	double			extended
cray	big	none	Cray-1	none	none	Cray-1
			single			double
.TE
.sp
Note that MC68000 and SPARC formats are identical big-endian formats; 
the Intel PC formats for 8086/8087, 80x86/7, and 80486 are identical little-endian.
Table entries ``none'' represent improper sizes; no conversion occurs.
``IEEE quad'' refers to the natural big- or little-endian
extension of IEEE single and double precision
to a format with 15 exponent bits, one implicit significand bit, and 112
explicit significand bits.
.SH USAGE
.nf
\fIexcep = \fBconvert_external(\fPsrc_p, src_format, dst_p, dst_format, rounding, n\fB)\fP;\fP
.fi
.RS
.TP
.I excep
The function value is an accumulation of all the exceptions encountered during the conversion.
If detecting individual exceptions is important, 
.I n 
should be 1 so that only one data item is converted
at a time.
.RE
.RS
.TP
.I src_p
A pointer to the data to be converted stored as a contiguous array.
The pointer is declared \fBchar *\fP to emphasize that there are no alignment requirements,
although data being converted to SPARC format for subsequent processing by the
same program should be appropriately aligned.
.RE
.RS
.TP
.I dst_p
A pointer to the converted data stored as a contiguous array.
.RE
.RS
.TP
.I src_format
Description of the format of the source data.
.RE
.RS
.TP
.I dst_format
Description of the format of the destination data.
.RE
.RS
.TP
.I rounding
The rounding mode to apply to the destination data.
.RE
.RS
.TP
.I n
Number of data items to be converted.
.RE
.br
.ne 6
.PP 
Rounding choices; the first is intended to be satisfactory for almost all
applications:
.IP "(int) convert_external_common"
integer and unsigned destination formats round toward zero (2.9 rounds to 2, \-2.9 rounds to \-2), 
.br
IEEE floating-point destinations round to nearest unbiased,
.br
VAX floating-point destinations round to nearest biased,
.br
IBM 370 and Cray floating-point destinations round toward zero.
.IP "(int) convert_external_biased"
to round to nearest biased (for integer destinations, 1.5 rounds to 2, 2.5 rounds to 3).
.IP "(int) convert_external_away"
to round away from zero (for integer destinations, 2.1 rounds to 3, \-2.1 rounds to \-3). 
.br
.ne 3
.IP "(int) fp_nearest, fp_tozero, fp_positive, fp_negative"
to obtain rounding in one of the modes specified in IEEE 754.
.br
.ne 12
.PP
After the conversion, if 
.IR "(excep & (1 << fp_invalid)) != 0" , 
then one or more invalid
exceptions occurred; similarly for the other exceptions, which are defined this way:
.IP fp_invalid
An input argument was an IEEE signaling NaN or a VAX reserved operand,
or an input argument was an infinity and the destination was an integer or unsigned type,
or a floating-point type with no infinity representation.
.IP fp_overflow
An input argument was finite and large but could not be represented in the destination
format with only normal rounding error; if the destination format is integer or unsigned
then fp_overflow represents integer overflow.
.IP fp_underflow
An input argument was finite and small and could not be represented in the destination
format with only normal rounding error.  This can only arise with floating-point destinations.
.IP fp_inexact
An input argument was finite but could not be represented exactly in the destination format.
.IP fp_division
The source or destination formats aren't supported.  No conversions are done.
.PP
Note that converting external data to SPARC format, processing on a SPARC, then
converting back to external format will almost never yield the same results as
processing the data entirely on the external system, even if the conversions
and processing yield no exceptions on the SPARC.
.SH EXAMPLES
.SS "Generating data on an external system"
.LP
The following code demonstrates typical FORTRAN generation of binary floating-point data
on an external system:
.RS
.ft B
.nf
        REAL A(100)
        WRITE(9) A
.fi
.ft
.RE
.SS "Converting external data to SPARC internal form in C"
.LP
Suppose the previous binary data file was created on a tape on a Cray and
read on a SPARC system.
To convert to IEEE double-precision floating-point format, but using biased rounding,
.RS
.ft B
.nf
#include <sunmath.h>

char datasrc[800];
double datadest[100];
fp_exception_field_type excep;
int rounding;
int i;
convert_external_t src, dest;

/*    read the Cray data into the array datasrc somehow, then ...  */

src.arch = convert_external_cray;
src.type = convert_external_float;
src.size = 8;
dest.arch = convert_external_sparc;
dest.type = convert_external_float;
dest.size = 8;
rounding = convert_external_biased;

excep = convert_external((char *) datasrc, src, (char *) datadest, dest, rounding, 100);

/*
 * usually you aren't interested in inexact exceptions
 * and you'd do this
 */
excep &= ~(1 << fp_inexact);

/*
 * other exceptions are possible in this example - after the fact,
 * you can find out where they happened this way
 */
if (excep != 0) for (i = 0 ; i < 100 ; i++) {
    excep = convert_external((char *)&datasrc[8 * i], src, (char *)&datadest[i], dest, rounding, 1);
    if (excep != 0) { 
        /* do something specific about datadest[i] */ 
    }
}	

.fi
.ft
.RE
.SS "Converting SPARC internal data to external form in FORTRAN"
.LP
Suppose data created in a SPARC program is to be read into an IBM PC.
To convert,
.RS
.ft B
.nf
#include <f77_floatingpoint.h>	

        REAL*4 datasrc(100)
        REAL*4 datadest(100)
        INTEGER excep, convert_external, round
        INTEGER src(4), dest(4)

c        create datasrc array somehow, then ...

        src(1) = convert_external_sparc
        src(2) = convert_external_float
        src(3) = 4
        dest(1) = convert_external_pc
        dest(2) = convert_external_float
        dest(3) = 4
        round = convert_external_common

        excep = convert_external(datasrc, src, datadest, dest, round, 100)
c        The only exception that can arise is fp_invalid, 
c        by converting a signaling NaN.

c        Now write the data out in a file that an IBM PC can read
.fi
.ft
.RE
.SH "SEE ALSO"
.BR dd (1M),
for reading tapes from foreign systems;
.BR xdr (3N),
for formatting arbitrary
data structures in a machine-independent fashion.
.SH NOTES
.LP
Conversions are performed one at a time by converting a source datum into
an internal format large enough to hold any input exactly, then by
converting the internal data to the destination form.  Thus any
pair of source and destination formats is permitted, but efficiency is
not as high as would be the case for programs written to convert
from one specific format to another.

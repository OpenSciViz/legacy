#ifndef etags_h_INCLUDED
#define etags_h_INCLUDED

#ident "etags.h 16.1 93/07/09"

/* Tags include file
   Copyright (C) 1984, 1987, 1988 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
 * Etags include file.
 */

/*
 * Typedefs
 */

#define streq(s, t)	(strcmp (s, t) == 0)
#define strneq(s, t, n)	(strncmp (s, t, n) == 0)
#define	reg	register
#define	logical	char

#define	TRUE	1
#define	FALSE	0

struct	nd_st {			/* sorting structure		*/
	char	*name;		/* function or type name	*/
	char	*file;		/* file name			*/
	logical is_func;	/* use pattern or line no	*/
	logical is_call;	/* is this a function call? -sb */
	logical rewritten;	/* list name separately		*/
	logical	been_warned;	/* set if noticed dup		*/
	int	lno;		/* line number tag is on	*/
	long    cno;		/* character number line starts on */
	char	*pat;		/* search pattern		*/
	struct	nd_st	*left,*right; /* left and right sons	*/
	struct  nd_st   *next;	/* next node; only used for -sb */
	struct  nd_st   *caller;/* who called this node         */
};

typedef struct nd_st NODE;


/*
 * Flag Variables
 */

extern int	aflag;		/* -a: append to tags */
extern int	eflag;		/* emacs style output: no -e option any more */

extern int	sflag;		/* sbrowser data generation  */
extern int	outfflag;	/* -o: define output file */

/* The following three default to 1 for etags, but to 0 for ctags.  */
extern int	tflag;		/* -t: create tags for typedefs */
extern int	strflag;	/* -T: create tags for typedefs, level */
				/* 0 struct/enum/union decls, and C++ */
				/* member functions */
extern int	constantflag;	/* -d: create tags for C #define and enum */
				/* constants. Default under etags.  Enum */
				/* constants not implemented. */
				/* -D: opposite of -d.  Default under ctags. */
extern int	uflag;		/* -u: update tags */
extern int	vflag;		/* -v: create vgrind style index output */
extern int	wflag;		/* -w: suppress warnings */
extern int	xflag;		/* -x: create cxref style output */
extern int	Cflag;		/* .[hc] means C++, not C (default) */
extern int	noindentflag;	/* -S: ignore indentation in C */
extern int	iflag;		/* -i: treat all spec'd files as
				   included sub-tag-tables.  */

/*
 * Other used global variables
 */

extern char *progname;		/* name this program was invoked with.  */
extern char *base_progname;	/* base of it */

extern char searchar;		/* what character to use in searches */

extern FILE *outf;		/* ioptr for tags file */
extern char *outfile;		/* output file	*/

extern NODE *head;		/* the head of the binary tree of tags	*/

typedef int LINENO;

extern LINENO	lineno;		/* line number of current line */
extern long	charno;		/* current character number */


/*
 * Procedures
 */

extern char *malloc (), *realloc ();
extern char *getenv ();
extern char *strchr (), *strrchr ();
extern char *strcpy (), *strncpy ();
extern int   strcmp ();

extern char *savestr (char *);
extern char *savenstr (char *, int);
extern char *concat (char *, char *, char *);
extern void fatal ();

extern logical find_entries (char *);
extern void free_tree (NODE *);
extern void put_entries (NODE *);
extern int total_size_of_entries (NODE*);
extern void process_file (char *);
extern void note_language (int, char *);
extern FILE *sb_open_pipe (void);
extern void sb_print_line_id ();
extern void sb_enter_line_id (int, unsigned int);


/*
 * Valid arguments for note_language
 */

#define LANG_NONE	-1
#define LANG_TEX	0
#define LANG_LISP	1
#define LANG_SCHEME	2
#define LANG_AS		3
#define LANG_C		4
#define LANG_C_PLPL	5
#define LANG_C_STAR	6
#define LANG_FORTRAN	7
#define LANG_PASCAL	8	/* currently using FORTRAN instead!! */

#define LANG_FIRST	LANG_TEX
#define LANG_LAST	LANG_PASCAL

#endif /* etags_h_INCLUDED */

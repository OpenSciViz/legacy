GFLAG = -g -sb
CC = acc
# OFLAG = -O
CFLAGS = $(OFLAG) $(GFLAG)
SRCFILES = COPYING Makefile README README.2 etags.c etags_out.c etags.h \
	etags.1 tags.el
VERSION=1.0

# Install binaries 'etags', 'ctags', and 'sbtags' (a symlink) here.
BIN_DIR = /usr/local/bin
# Install man pages here.
MAN_DIR = /usr/local/man/man1
# Man page source filenames get this extension.
MAN_EXT = .1

etags: etags.o etags_out.o
	$(CC) $(CFLAGS) -o etags etags.o etags_out.o

ctags: etags
	rm -f ctags
	cp etags ctags

sbtags: etags
	rm -f sbtags
	cp etags sbtags

etags.o: etags.c etags.h
etags_out.o: etags_out.c etags.h

# Install etags, ctags and sbtags in $(BIN_DIR).  Don't use a symlink;
# it messes up rdist.
# Note: this does not install tags.el in your local Emacs area;
# you must do that manually.
install: etags
	cp etags $(BIN_DIR)/etags
	chmod 775 $(BIN_DIR)/etags
	cp etags $(BIN_DIR)/ctags
	chmod 775 $(BIN_DIR)/ctags
	cp etags $(BIN_DIR)/sbtags
	chmod 775 $(BIN_DIR)/sbtags

# Install etags and ctags man pages (copies of the same page).
install_man: etags.1
	cp etags.1 $(MAN_DIR)/etags$(MAN_EXT)
	cp etags.1 $(MAN_DIR)/ctags$(MAN_EXT)
	cp etags.1 $(MAN_DIR)/sbtags$(MAN_EXT)

cl_etags:
	##load -DDEBUG etags.c

clean:
	rm -f etags ctags sbtags *.o

# -s70k is needed because etags.c is so big.
kit:
	makekit -s70k $(SRCFILES)

# Make a tar file with fictional directory 'tags-VERSION'.
tar tags-$(VERSION).tar.Z: $(SRCFILES)
	rm -f tags-$(VERSION)
	ln -s . tags-$(VERSION)
	tar cf - `ls $(SRCFILES) | sed 's|^|tags-$(VERSION)/|'` | \
		compress > tags-$(VERSION).tar.Z

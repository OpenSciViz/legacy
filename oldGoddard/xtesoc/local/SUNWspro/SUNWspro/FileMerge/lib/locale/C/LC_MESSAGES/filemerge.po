domain "filemerge"
#
# ../bin/filemerge/fm.cc: line number: 264
msgid "Unable to set HELPPATH environment variable. Spot help may not work\n"
msgstr "Unable to set HELPPATH environment variable. Spot help may not work\n�դ���"
#
# ../bin/filemerge/fm.cc: line number: 376
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm.cc: line number: 419
msgid "When the -l option is used, both leftfile and\nrightfile must be directories"
msgstr "When the -l option is used, both leftfile and\nrightfile must be directories�դ���"
#
# ../bin/filemerge/fm.cc: line number: 507
msgid "Load Next From List"
msgstr "Load Next From List�դ���"
#
# ../bin/filemerge/fm.cc: line number: 509
msgid "Load..."
msgstr "Load...�դ���"
#
# ../bin/filemerge/fm.cc: line number: 575
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 576
msgid "Save"
msgstr "Save�դ���"
#
# ../bin/filemerge/fm.cc: line number: 577
msgid "Save As..."
msgstr "Save As...�դ���"
#
# ../bin/filemerge/fm.cc: line number: 634
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 635
msgid "Save"
msgstr "Save�դ���"
#
# ../bin/filemerge/fm.cc: line number: 636
msgid "Save As..."
msgstr "Save As...�դ���"
#
# ../bin/filemerge/fm.cc: line number: 759
msgid "You have UNSAVED edits!\nDo you really want to load?"
msgstr "You have UNSAVED edits!\nDo you really want to load?�դ���"
#
# ../bin/filemerge/fm.cc: line number: 760
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm.cc: line number: 760
msgid "Load"
msgstr "Load�դ���"
#
# ../bin/filemerge/fm.cc: line number: 774
msgid "No more files"
msgstr "No more files�դ���"
#
# ../bin/filemerge/fm.cc: line number: 839
msgid "Can't have input file as a directory"
msgstr "Can't have input file as a directory�դ���"
#
# ../bin/filemerge/fm.cc: line number: 889
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 896
msgid "Save"
msgstr "Save�դ���"
#
# ../bin/filemerge/fm.cc: line number: 897
msgid "Save As..."
msgstr "Save As...�դ���"
#
# ../bin/filemerge/fm.cc: line number: 906
msgid "Stay After Apply"
msgstr "Stay After Apply�դ���"
#
# ../bin/filemerge/fm.cc: line number: 908
msgid "Stay After Apply"
msgstr "Stay After Apply�դ���"
#
# ../bin/filemerge/fm.cc: line number: 910
msgid "Stay After Resolve"
msgstr "Stay After Resolve�դ���"
#
# ../bin/filemerge/fm.cc: line number: 993
msgid "You have UNSAVED edits!\nDo you really want to quit?"
msgstr "You have UNSAVED edits!\nDo you really want to quit?�դ���"
#
# ../bin/filemerge/fm.cc: line number: 994
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm.cc: line number: 995
msgid "Quit"
msgstr "Quit�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1047
msgid "Must specify a directory name\nfor the Directory field"
msgstr "Must specify a directory name\nfor the Directory field�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1057
msgid "Must specify a file name\nfor the File field"
msgstr "Must specify a file name\nfor the File field�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1241
msgid "Must specify a file name\nfor the Left File"
msgstr "Must specify a file name\nfor the Left File�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1251
msgid "Must specify a file name\nfor the Right File"
msgstr "Must specify a file name\nfor the Right File�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1310
msgid "You have UNSAVED edits!\nDo you really want to unload?"
msgstr "You have UNSAVED edits!\nDo you really want to unload?�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1311
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1311
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1320
msgid "There are more files in listfile.\nDo you really want to unload?"
msgstr "There are more files in listfile.\nDo you really want to unload?�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1321
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1321
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1330
msgid "FileMerge"
msgstr "FileMerge�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1333
msgid "(no files loaded)"
msgstr "(no files loaded)�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1336
msgid "(no files loaded)"
msgstr "(no files loaded)�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1338
msgid "(no files loaded)"
msgstr "(no files loaded)�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1353
msgid "Unload"
msgstr "Unload�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1354
msgid "Save"
msgstr "Save�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1355
msgid "Save As..."
msgstr "Save As...�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1367
msgid "0 of 0 Diffs Unresolved"
msgstr "0 of 0 Diffs Unresolved�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1371
msgid "0 Diffs"
msgstr "0 Diffs�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1382
msgid "FileMerge"
msgstr "FileMerge�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1388
msgid " - Ancestor File: "
msgstr " - Ancestor File: �դ���"
#
# ../bin/filemerge/fm.cc: line number: 1424
msgid "Usage: %s [-b] [-r] [-a ancestor] [-f1 name] [-f2 name] \n\t[-l listfile] [leftfile rightfile [outfile]]\n"
msgstr "Usage: %s [-b] [-r] [-a ancestor] [-f1 name] [-f2 name] \n\t[-l listfile] [leftfile rightfile [outfile]]\n�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1488
msgid "Directory must be the name of an existing directory"
msgstr "Directory must be the name of an existing directory�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1508
msgid "Ancestor File cannot be a directory"
msgstr "Ancestor File cannot be a directory�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1509
msgid "Ancestor File not found"
msgstr "Ancestor File not found�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1519
msgid "Left File cannot be a directory"
msgstr "Left File cannot be a directory�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1520
msgid "Left File not found"
msgstr "Left File not found�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1530
msgid "Right File cannot be a directory"
msgstr "Right File cannot be a directory�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1531
msgid "Right File not found"
msgstr "Right File not found�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1584
msgid "File"
msgstr "File�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1593
msgid "Dir"
msgstr "Dir�դ���"
#
# ../bin/filemerge/fm.cc: line number: 1604
msgid "%s \"%s\" does not exist"
msgstr "%s \"%s\" does not exist�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 265
msgid "No changes to undo"
msgstr "No changes to undo�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 283
msgid "No current differences to undo"
msgstr "No current differences to undo�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 296
msgid "Do you really want to\nUndo all your changes?"
msgstr "Do you really want to\nUndo all your changes?�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 297
msgid "Undo All"
msgstr "Undo All�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 297
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 324
msgid "unresolved "
msgstr "unresolved �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 359
msgid "unresolved "
msgstr "unresolved �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 375
msgid "resolved "
msgstr "resolved �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 395
msgid "left-hand side "
msgstr "left-hand side �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 415
msgid "right-hand side "
msgstr "right-hand side �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 449
msgid "Reached the bottom of the file"
msgstr "Reached the bottom of the file�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 450
msgid "Wrap"
msgstr "Wrap�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 450
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 456
msgid "There are no more %sdiffs"
msgstr "There are no more %sdiffs�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 515
msgid "unresolved "
msgstr "unresolved �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 531
msgid "resolved "
msgstr "resolved �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 551
msgid "left-hand side "
msgstr "left-hand side �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 571
msgid "right-hand side "
msgstr "right-hand side �դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 606
msgid "Reached the top of the file"
msgstr "Reached the top of the file�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 607
msgid "Wrap"
msgstr "Wrap�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 607
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 613
msgid "There are no more %sdiffs"
msgstr "There are no more %sdiffs�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 697
msgid "No differences."
msgstr "No differences.�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 706
msgid "No selection to go to."
msgstr "No selection to go to.�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 715
msgid "Can't get selection."
msgstr "Can't get selection.�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 776
msgid "%d Diff"
msgstr "%d Diff�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 778
msgid "%d Diffs"
msgstr "%d Diffs�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 782
msgid "%d of %d Diff Resolved, %d remaining"
msgstr "%d of %d Diff Resolved, %d remaining�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 785
msgid "%d of %d Diffs Resolved, %d remaining"
msgstr "%d of %d Diffs Resolved, %d remaining�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 820
msgid "No current differences"
msgstr "No current differences�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 865
msgid "You have %d unresolved difference\nDo you really want to save?"
msgstr "You have %d unresolved difference\nDo you really want to save?�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 868
msgid "You have %d unresolved differences\nDo you really want to save?"
msgstr "You have %d unresolved differences\nDo you really want to save?�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 871
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 872
msgid "Save"
msgstr "Save�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 879
msgid "Saving %s..."
msgstr "Saving %s...�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 882
msgid "%s not saved."
msgstr "%s not saved.�դ���"
#
# ../bin/filemerge/fm_panel.cc: line number: 886
msgid "%s saved."
msgstr "%s saved.�դ���"
#
# ../bin/filemerge/fm_frame.cc: line number: 218
msgid "You have UNSAVED edits!\nDo you really want to quit?"
msgstr "You have UNSAVED edits!\nDo you really want to quit?�դ���"
#
# ../bin/filemerge/fm_frame.cc: line number: 219
msgid "Cancel"
msgstr "Cancel�դ���"
#
# ../bin/filemerge/fm_frame.cc: line number: 219
msgid "Quit"
msgstr "Quit�դ���"
#
# ../bin/filemerge/fm_sdiff.cc: line number: 94
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_sdiff.cc: line number: 103
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_sdiff.cc: line number: 120
msgid "%s: can't popen\n"
msgstr "%s: can't popen\n�դ���"
#
# ../bin/filemerge/fm_sdiff.cc: line number: 207
msgid "\nfilemerge: trouble performing diff.  Perhaps you are diffing a binary file?\n"
msgstr "\nfilemerge: trouble performing diff.  Perhaps you are diffing a binary file?\n�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 292
msgid "Next After Apply"
msgstr "Next After Apply�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 294
msgid "Next After Apply"
msgstr "Next After Apply�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 296
msgid "Next After Resolve"
msgstr "Next After Resolve�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 299
msgid "Stay After Apply"
msgstr "Stay After Apply�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 301
msgid "Stay After Apply"
msgstr "Stay After Apply�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 303
msgid "Stay After Resolve"
msgstr "Stay After Resolve�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 384
msgid "You have made changes that have not been applied."
msgstr "You have made changes that have not been applied.�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 385
msgid "Cancel Save"
msgstr "Cancel Save�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 385
msgid "Apply, then Save"
msgstr "Apply, then Save�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 480
msgid "%s: can't find home directory\n"
msgstr "%s: can't find home directory\n�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 591
msgid "Cannot find home directory to read \"%s\""
msgstr "Cannot find home directory to read \"%s\"�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 603
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 618
msgid "Cannot parse line\n\t%sin file \"%s\""
msgstr "Cannot parse line\n\t%sin file \"%s\"�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 645
msgid "Cannot parse line\n\t%sin file \"%s\""
msgstr "Cannot parse line\n\t%sin file \"%s\"�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 674
msgid "Cannot parse line\n\t%sin file \"%s\""
msgstr "Cannot parse line\n\t%sin file \"%s\"�դ���"
#
# ../bin/filemerge/fm_popup.cc: line number: 1116
msgid "Colors palette is not supported."
msgstr "Colors palette is not supported.�դ���"
#
# ../bin/filemerge/fm_glyph.cc: line number: 111
msgid "%s: too many differences\n"
msgstr "%s: too many differences\n�դ���"
#
# ../bin/filemerge/fm_group.cc: line number: 197
msgid "linetogroup: negative line number: %d"
msgstr "linetogroup: negative line number: %d�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 594
msgid "Accept parent"
msgstr "Accept parent�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 599
msgid "Accept child"
msgstr "Accept child�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 725
msgid "filemerge: Must specify a file name for the Left File."
msgstr "filemerge: Must specify a file name for the Left File.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 728
msgid "filemerge: Must specify a file name for the Right File."
msgstr "filemerge: Must specify a file name for the Right File.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 731
msgid "filemerge: You have UNSAVED edits!"
msgstr "filemerge: You have UNSAVED edits!�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 734
msgid "filemerge: There are more files in listfile."
msgstr "filemerge: There are more files in listfile.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 737
msgid "filemerge: Cannot open listfile."
msgstr "filemerge: Cannot open listfile.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 740
msgid "filemerge: Can't use listfile unless both arguments are directories."
msgstr "filemerge: Can't use listfile unless both arguments are directories.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 743
msgid "filemerge: ToolTalk Load failed."
msgstr "filemerge: ToolTalk Load failed.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 746
msgid "filemerge: ToolTalk Load was successful."
msgstr "filemerge: ToolTalk Load was successful.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 749
msgid "filemerge: Currently has files loaded."
msgstr "filemerge: Currently has files loaded.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 752
msgid "filemerge: Permission denied on force load."
msgstr "filemerge: Permission denied on force load.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 756
msgid "filemerge: ToolTalk Unload was successful."
msgstr "filemerge: ToolTalk Unload was successful.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 759
msgid "filemerge: Permission denied on force unload."
msgstr "filemerge: Permission denied on force unload.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 763
msgid "filemerge: You still have unresolved differences."
msgstr "filemerge: You still have unresolved differences.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 766
msgid "filemerge: ToolTalk Save failed."
msgstr "filemerge: ToolTalk Save failed.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 769
msgid "filemerge: ToolTalk Save was successful."
msgstr "filemerge: ToolTalk Save was successful.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 772
msgid "filemerge: Permission denied on save."
msgstr "filemerge: Permission denied on save.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 775
msgid "filemerge: Permission denied on force save."
msgstr "filemerge: Permission denied on force save.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 779
msgid "filemerge: ToolTalk quit was successful."
msgstr "filemerge: ToolTalk quit was successful.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 782
msgid "filemerge: Permission denied on quit."
msgstr "filemerge: Permission denied on quit.�դ���"
#
# ../bin/filemerge/fm_tt.cc: line number: 786
msgid "filemerge: Permission denied on force quit."
msgstr "filemerge: Permission denied on force quit.�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 182
msgid "%s: fatal error: "
msgstr "%s: fatal error: �դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 212
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 470
msgid "Can't initialize licensing.\n"
msgstr "Can't initialize licensing.\n�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 501
msgid "Cannot get a valid license for %s.  Trying %s..."
msgstr "Cannot get a valid license for %s.  Trying %s...�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 507
msgid "Cannot get a valid license to run!"
msgstr "Cannot get a valid license to run!�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 560
msgid "FileMerge: None of these licenses exist: "
msgstr "FileMerge: None of these licenses exist: �դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 631
msgid "Read of \"%s\" failed"
msgstr "Read of \"%s\" failed�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 676
msgid "Write to \"%s\" failed"
msgstr "Write to \"%s\" failed�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 691
msgid "Open of \"%s\" for writing failed"
msgstr "Open of \"%s\" for writing failed�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 702
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_util.cc: line number: 744
msgid "Write to \"%s\" failed"
msgstr "Write to \"%s\" failed�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 123
msgid "Doing diffs for %s"
msgstr "Doing diffs for %s�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 171
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 179
msgid "Open of \"%s\" for reading failed"
msgstr "Open of \"%s\" for reading failed�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 263
msgid "Sorry, textedit Undo is not supported in %s\n"
msgstr "Sorry, textedit Undo is not supported in %s\n�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 290
msgid "%s: fwrite failed on %s\n"
msgstr "%s: fwrite failed on %s\n�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 310
msgid "%s: fwrite failed on %s\n"
msgstr "%s: fwrite failed on %s\n�դ���"
#
# ../bin/filemerge/fm_text.cc: line number: 337
msgid "%s: fwrite failed on %s\n"
msgstr "%s: fwrite failed on %s\n�դ���"
#
# ../bin/filemerge/fm_canvas.cc: line number: 886
msgid "Open of \"%s\" for writing failed"
msgstr "Open of \"%s\" for writing failed�դ���"
#
# ../bin/filemerge/fm_canvas.cc: line number: 910
msgid "Please make a primary selection first.\nPress \"Continue\" to proceed."
msgstr "Please make a primary selection first.\nPress \"Continue\" to proceed.�դ���"
#
# ../bin/filemerge/fm_serv.c: line number: 76
msgid "%s: Can't create rpc server for -R option."
msgstr "%s: Can't create rpc server for -R option.�դ���"
#
# ../bin/filemerge/fm_serv.c: line number: 85
msgid "%s: Can't register rpc server for -R option."
msgstr "%s: Can't register rpc server for -R option.�դ���"
#
# ../bin/filemerge/fm_serv.c: line number: 165
msgid "%s: Couldn't reply to FM_LOAD_FILES rpc call.\n"
msgstr "%s: Couldn't reply to FM_LOAD_FILES rpc call.\n�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 154
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 161
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 168
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 176
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 184
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 193
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 202
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 212
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 222
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 233
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 244
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 256
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 268
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 281
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 294
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 308
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 322
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 337
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 352
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/ow_util.c: line number: 369
msgid "Continue"
msgstr "Continue�դ���"
#
# ../bin/filemerge/fm_history.cc: line number: 110
msgid "del_from_history(): NULL history"
msgstr "del_from_history(): NULL history�դ���"
#
# ../bin/filemerge/fm_canvas_menu.cc: line number: 117
msgid "Selection starts in line %d.\nPress \"Continue\" to proceed."
msgstr "Selection starts in line %d.\nPress \"Continue\" to proceed.�դ���"
#
# ../bin/filemerge/fm_canvas_menu.cc: line number: 225
msgid "Must specify a file name\nto store into."
msgstr "Must specify a file name\nto store into.�դ���"
#
# ../bin/filemerge/fm_canvas_menu.cc: line number: 379
msgid "\"%s\" not found"
msgstr "\"%s\" not found�դ���"

08 Aug 1994
         

        	SPARCWorks - debugger 3.0.1 and dbx 3.0.1
		For Solaris (TM) 1.0 or later
                For Solaris (TM) 2.2 or later
=============================================================================

INTRODUCTION:
        SPARCWorks debugger 3.0.1 and dbx 3.0.1 run under SunOS(TM) 5.2 
        or later (Solaris 2.x), and SunOS 4.1.x operating environments 
        (Solaris 1.x). 

CONTENTS:
	A. New Features List of (selected) Known Bugs
	B. Changes to Features 
	C. Incompatibilities/Limitations
	D. Current Software Bugs
        E. Directory Structure
        F. Additional Help
_____________________________________________________________________________
A. New Features

   There are many new features in this version of the debugger including
   fix & continue, Run Time Checking, C++ template support, etc. Customer
   feedback is very important to us, please use the About... button to 
   let us know how we are doing. The Custom menu no longer exists. The
   following new features have been added to the Debugger for 3.0.1:

   (Solaris 2.x)
   Visual Data Inspector
   Allows you to graphically examine program variables, including complex 
   structures, and to monitor values during program execution.

   (Solaris 2.x)
   Process/Thread Inspector
   Allows you to find information about threads. The Debugger recognizes a
   multithreaded program and automatically enables the Debugger multithreaded 
   features.

   Stack Inspector
   Performs operations on the stack and dynamically updates the stack when 
   you visit another process.

   Program Input/Output (PIO)
   A PIO window has been added to provide an I/O command interface for 
   applications that is separate from the debugging command I/O interface.

   Debugger Window - About Box
   An About button has been added to the window. Click on the About button 
   to open the About box. The About box contains the Debugger version number,
   copyright information, and a brief description of the Debugger.

   Event Management
   Allows you to use the general capability of the Debugger to perform certain
   actions when certain events take place in the program being debugged. Event
   Management is an enhancement to setting breakpoints and traces.

   (Solaris 2.x)
   Fast Watchpoints
   The "stop modify" command utilizes page protection to notify the user when 
   a particular piece of memory has been written to. Previously the Debugger 
   used automatic single-stepping to accomplish the same, albeit much slower.
	
   (Solaris 2.x)
   Debugging Child Processes
   Adds the ability to follow an exec'd or forked process.

   Runtime Checking (RTC)
   Allows you to automatically detect run-time errors in an application during
   the development phase. As an integral feature of the Debugger, Runtime
   Checking uses the full capabilities of the Debugger including setting
   breakpoints and examining variables.

   Fix and Continue
   Allows you to fix (change) a file, recompile it with the same options,
   install new code in the program, and continue the program without 
   restarting the application/program.

   Handling Exceptions in the Debugger
   Exception handling describes how to get the Debugger to catch any 
   exceptions that may be raised by "throw" expressions in C++ code.

   Debugging with Templates
   Debugging with C++ Templates shows you how to load programs containing 
   class and function templates into the Debugger and invoke any of the 
   debugging commands on a template that you would use on a class or function.

   Korn Shell 
   The Debugger command language is based on the syntax of the Korn shell, 
   including I/O redirection, loops, built-in arithmetic, history, and 
   command-line editing. (Command history and editing are available only 
   in command-line mode, not in the Debugger.)  See "The Korn Shell 
   Command and Programming Language", Morris I. Bolsky and David G. Korn, 
   Prentice Hall, 1989.

_____________________________________________________________________________
B. Changes to Features

   The following minor changes have been made to Debugger for 3.0.1:

   * In the previous 2.0.1 Debugger the use of "sh stty sane" was recommended
     to get around an OpenLook(R) bug. This is no longer necessary with this 
     release.

   * The Debugger Property sheet has been enhanced and now includes Debugger 
     Events, Run Time Check, and Miscellaneous options to control
     Debugger parameters.

   * Replaced the Make option with the MakeTool program.

   * Added and enhanced Debugger Commands.

_____________________________________________________________________________
C. Incompatibilities/Limitations

   (Solaris 1.x)
   Solaris 1.x does not support cc 1.0.

   (Solaris 1.x)
   On 4.x systems, if an application overflows its stack, the overflowing 
   stack might run into data areas reserved for shared libraries and corrupt 
   names of shared libraries that are extracted by dbx. Normally, the stack 
   "redzone" keeps this from happening but a particularly large frame size 
   (large function local array) can circumvent the stack redzone. The symptom
   is an error message:
 	      
     fatal error: could not mmap "" (file descriptor %d) -- No such device"
	
   Reducing the shell cordumpsize limit should help this problem.

   (Solaris 1.x and 2.x) 
   If you run the Debugger and redisplay the Debugger window on a remote 
   machine, you must have ttsession available from your $PATH. 

   (Solaris 1.x and 2.x)
   Undefined or Duplicate Types in Stabs 

   Applicable to those running on Solaris 1.x (or Solaris 2.2 or 2.3 if 
   your program was compiled with the -xs option).

   When debugging some programs in a Solaris 1.x operating environment, you
   might receive warnings similar to the following examples that resulted
   from reading symbol table (stab) information:

     undefined type number (312,17) at a.out: file12.cc stab #...

     duplicate type definition (101,25) at a.out: file8.cc stab #...

   This problem can cause errors when printing or accessing functions,
   variables, and types from the indicated modules. In the context of
   Runtime Checking, such "bad stabs" can cause RTC initialization to
   terminate.

   If you encounter such warnings and the attempted dbx command fails, 
   the problem can be remedied sometimes by trying one of the following
   workarounds:
 
   o Recompile the source files that produced the bad stabs without the
     -g option if you do not need the debugging information for those
     files, and then relink the program. The warning messages specify the
     source files that gave rise to the bad stabs. This is probably the
     best approach when bad stabs interfere with RTC.

   o In your link line, reposition the .o files corresponding to the 
     "bad stab" source files and relink the program. Files that need
     debugging should be placed at the beginning of the link line; the
     others should be placed at the end of the link line.
 
   o Starting with the list of source files specified in the warning 
     messages, locate "include" files, a.h and b.h, where both of the 
     following conditions exist:

     -- a.h has a forward reference to a symbol defined in b.h
 
     -- various source files include a.h and b.h in different order
  
     Edit all the source files that include a.h and b.h so as to always
     include those .h files in the same order (e.g., in alphabetical
     order) and rebuild the program.

   (Solaris 2.x)
   The following features are supported on Solaris 2.x only:
   *  MT 
   *  Stop modify 
   *  Follow-fork child
   *  Collector 
   *  Visual Data Inspector

   (Solaris 2.x)
   * Support for dynamic and static types in C++ are supported with the 
     "-d" options of the "print" and "whatis" commands, and through the 
     "dbxenv" variable "dynamic_type".

   (Solaris 2.x)
   *  The mangling of C++ names was changed after the SPARCompiler 3.0 beta
      release. This may cause the dbx from this release to complain about
      older binaries.  To fix, recompile your code with the SPARCompiler 3.0 
      or 3.0.1 fcs compiler.

   (Solaris 2.x) 
   *  The mangling scheme of C++ templates with integral constant argument(s) 
      was changed after SPARCompiler 3.0 alpha release. This may cause the
      dbx from this release to complain about older binaries. To fix,
      recompile your code with the SPARCompiler 3.0.1 FCS compiler.

   (Solaris 2.x) 
   *  Use of libC.so.5 (or libC.so.4) may cause problems for dbx in the
      area of C++ exceptions. Misc. warning messages about bad stabs and
      unhandled exceptions may result. A workaround is to link libC
      statically or intall the latest LibC.so patch (101242-06).
    
   *  Dbx may fail to find files that are accessed through non-generic
      automounter paths. See the "pathmap" command for help in dealing
      with these unusual directory mappings.
    
   *  Fix & Continue uses file timestamps to determine when files need
      to be fixed. The timestamp handling can occasionally become confused.
      Use of the -f option (i.e. "fix -f") will force the "fix" to happen.
     
   *  Runtime Checking (RTC)

	- Does not support MT programs. (Solaris 2.x)
       
	- An exec() call (in any of its forms) disables checking.

        - Has an 8Mb limitation. A workaround is described through the help 
          package, see (help rtc8M).

	(Solaris 2.x)
	- if "follow_fork_mode" is set to "parent", checking is turned off 
	  for child processes.  If "follow_fork_mode" is set to "child",
	  checking is turned off for the parent process.

	- if a large number of errors are produced while using RTC with the
	  debugger (GUI) without redirection to an error log file, a
	  disproportionate amount of swap space may be consumed by the XView
	  library, and the following message may appear:
	
		Tooltalk: out of memory

	  possibly followed by a core dump.

	  To reduce the consumption of swap space in this manner, redirect
	  RTC errors to a log file by entering the following commands:
	  
		dbxenv autocontinue on
		dbxenv errlogfile <filename>
		
	  Alternatively, you can increase the amount of swap space available
	  by starting the debugger with the -M option and a size argument
	  greater than 200,000.  E.g.:

		debugger -M 1000000 

	- FORTRAN users should compile with the -stackvar option to take
	  full advantage of RTC.  
	  
	  Note: Some programs may not work properly with -stackvar; see
		the FORTRAN User's Guide and Reference Manual for more
		information.  In such cases, users may wish to try the -C
		compiler option, which will turn on array subscript checking
		without RTC.

	(Solaris 2.x)	
	- RTC + watchpoints (stop modify) should not be used together. 
	  There is a multiplicative slowdown effect.

	(Solaris 2.x)
	- Use of RTC with the Collector is not allowed. The code
	  inserted by RTC would seriously perturb any performance 
	  measurements.

   (Solaris 2.x)
   * MT applications
	
        - Use of Solaris 2.3 or newer is necessary for MT features to
          operate correctly.

        - Use of the Debugger and dbx on MT requires the "sunpro.mpmt" 
          license feature line.

	- Collector/Analyzer is not supported.
        
	- RTC is not supported.
               
	- Follow fork may be unreliable.

        - Watchpoints or "stop modify" requires the dbxenv variable 
 	  "mt_watchpoints" set to 'On'. Use of watchpoints with MT may 
	  cause the program to hang.

	- Use of "call" or printing function calls may cause deadlock
	  situations. Keep in mind that a call will resume a single LWP 
	  which might cause a deadlock.

	- A known Solaris 2.2 and 2.3 kernel bug may cause an MT a.out to
	  core dump during startup (or after using the "debug" command).
	  Besides the actual "core" file and the fact that it might use up
	  considerable disk space, this core file can be ignored. The user
	  will not see any error messages when this happens, just a delay
	  shortly after the "Reading" messages. This bug has been fixed in 
          Solaris 2.4, one workaround is to limit the coredump size to 0 
          while debugging MT applications.

   (Solaris 2.x)
   * Visual Data Inspector
	
	- Does not annotate arrays of pointers properly.

        - Data Inspector performance problem with 24-bit display. In 24-bit 
          mode on machines with an SX frame buffer, the Data Inspector can be
          extremely slow in drawing or updating nodes. We recommend that the 
          Data Inspector be used with 8-bit display. Alternatively, in 24-bit
          mode, you can use the Data Display window with no such performance
          problem.

   (Differences in Sparcworks 3.0 from 2.0.1:)
   * The following changes were made to the dbx help messages in version 3.0
     from version 2.0.1:
  
     (1) The `address/format' command is now `x address/format'.
     (2) The `address=format' command is now `x address=format'.
     (3) The `/pattern' command is now `search pattern'.
     (4) The `?pattern' command is now `bsearch pattern'.
     (5) The `set' command is now the Korn shell `set' command.  Use `assign'.
     (6) The syntax of the `modules' command has changed.  See `help module' 
         and `help modules'.
     (7) The corefile is used only if explicitly requested.
     (8) Redirected output from `status' is no longer usable as input. See
         `help status'.

_____________________________________________________________________________
D. Current Software Bugs

    BugId   Synopsis 
    ------- -----------------------------------------------------
    1124948 The command I/O window doesn't support ksh command-line editing.

    1138484 ^C in the program I/O window will be ignored.  Type ^C in the
            command I/O window instead to stop the debugged program.

    1152995 There is a bug in Solaris 5.3 (fixed in 5.4) such that if an 
            application mmap(2) is a file and the mapped region excceeds the
            size of the file, and the application coredumps, dbx will
            have some trouble reading the corefile.

    1162677 When more than one Visual Data Inspector (VDI) buffer has been 
            created, deleting a buffer (via the View button menu) gives a
            warning message in the shell/command tool from which the Debugger
            was invoked. The warning message "OlListItemPointer - bad token..."
            does not cause further problems.

    1171193 In a session where the Data Inspector has been brought up, loading 
            a file with the Program Loader sometimes gives a message "Xlib: 
            unexpected async reply (sequence 0x...)" in the shell/command tool
            from which the Debugger was invoked. The only problem known to 
            accompany this is that a Stack Inspector arrow glyph covered by 
            the File Loader may not be redisplayed after the File Loader is 
            dismissed; the glyph can be restored by a left click on the button
            that normally displays it.

    1173969/1174135 
            If you have a multi-threaded process that forks on a 
            multiprocessor system, dbx might hang if you do not have patch 
            101945-01 (for SPARC) or patch 101946-01 (for x86) installed. 

_____________________________________________________________________________
E. Directory Structure

   The following table shows the directory structure of these tools. In the 
   following table, ... is /opt/SUNWspro. This base is the default value; 
   it can be changed at installation time.

   DIRECTORY                  DEFINITION
   _________                  __________

   .../SW3.0.1/READMEs        readme files
   .../SW3.0.1/bin            binaries
   .../SW3.0.1/man            manual pages
   .../SW3.0.1/lib	      support files, message catalogs and icons
   .../bin                    relative symbolic links to user
                              visible programs in SW3.0/bin
   .../man                    relative symbolic links to manual
                              pages in SW3.0/man
   .../READMEs                relative symbolic links to individual
                              readme files

_____________________________________________________________________________
F. Additional Help

   Type 'help <command name>' in the Debugger command pane for additional
   help on each command or topic. See 'help changes' for new and changed
   features or use the 'help -k <topic>' command to find which 'help 
   <command name>' commands are relevant to <topic>.

   See 'help FAQ' for answers to frequently asked questions about dbx 3.0.1.



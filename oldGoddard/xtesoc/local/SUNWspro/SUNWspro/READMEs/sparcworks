08 Aug 1994

                           SPARCworks(TM) 3.0.1 Toolset
                           For Solaris(TM) 1.0 or later
                           For Solaris(TM) 2.2 or later
                             
==========================================================================


Introduction: SPARCworks 3.0.1, for Solaris 1.0 (or later) or Solaris 2.2 
              (or later), is a suite of tools for software application 
              developers. SPARCworks tools make the development of 
              applications software easier with the use of an integrated 
              toolset.

Table of Contents :
         A. Operating Environment
         B. Prerequisites for Running SPARCworks 3.0.1
         C. SPARCworks Registration
         D. SPARCworks 3.0.1 Directory Structure
         E. Individual Usage
         F. SPARCworks README Files 
         G. Patches Information 
         H. FileMerge Errata
         I. Sample Programs Used in the SPARCworks Manuals

____________________________________________________________________________
A. Operating Environment

   The 3.0.1 release of SPARCworks runs with either Solaris 1.0 (or later) 
   or Solaris 2.2 (or later). Note that use of Solaris 2.3 or newer is
   necessary for MT features to operate correctly.
 
   Solaris 2.2 (or later) implies:
     - SunOS 5.2 (or later) operating system
     - A SPARC(R) computer, either a server or workstation
     - OpenWindows(TM) 3.x or later

   Solaris 1.0 (or later) implies:
     - SunOS 4.1.1 (or later) operating system
     - A SPARC(R) computer, either a server or workstation
     - OpeNwindows(TM) 3.0 or later

   SPARCworks is also operable with the window manager from 
   Motif(R) 1.2.2. Motif is a registered trademark of Open Software 
   Foundation, Inc. 

   If you are running window systems other than OpenWindows, you must 
   still have Tooltalk and the XView shared libraries installed.

__________________________________________________________________________
B. Prerequisites for Running SPARCworks 3.0.1 

   You must also have the corresponding version of one of the 
   following compilers running on your system:

   If on Solaris 1.x or 2.2 (or later)

     - SPARCompiler C (ANSI C) version 3.0.1
     - SPARCompiler C++ version 4.0.1
     - SPARCompiler FORTRAN version 3.0.1
     - SPARCompiler Pascal version 3.0.3

_________________________________________________________________________    
C. SPARCworks Registration

   You have the option of registering with SunSoft through the registration 
   tool available from the SPARCworks Manager palette. Select the registration
   icon to open the tool window. Fill out the self-explanatory form and mail 
   or fax to SunSoft to receive information updates on SPARCworks.

_________________________________________________________________________
D. SPARCworks 3.0.1 Directory Structure

   The following table shows the directory structure for SPARCworks
   3.0.1 on Solaris 1.x and 2.x. In the following table, ... is
     
     for 1.x: /usr/lang
     for 2.x: /opt/SUNWspro  

   This base is the default value; it can be changed at installation time.
	 
   DIRECTORY                    DEFINITION
   _________                    __________

   .../SW3.0.1/bin              binaries
   .../SW3.0.1/man              manual pages
   .../SW3.0.1/lib              libraries, help files, icons, and 
                                configuration files
                                (Note: icons cannot be modified.)
   .../SW3.0.1/READMEs	        readme files for SPARCworks tools
   .../SW3.0.1/examples	        files for Blocks and Freeway sample 
                                applications                          
   .../bin                      relative symbolic links to user    
			        visible programs in SW3.0.1/bin
   .../man                      relative symbolic links to manual
			        pages in SW3.0.1/man
   .../lib                      shared libraries explicitly referenced
			        by users
   .../READMEs	                relative symbolic links to individual
				readme files for each tool
   .../examples		        relative symbolic links to example files

_______________________________________________________________________ 
E. Individual Usage

 
    To use SPARCworks 3.0.1:

    1.  Put /usr/lang (for 1.x) or /opt/SUNWspro/bin (for 2.x) in PATH, if
        not already included.

        (in csh)	setenv PATH /opt/SUNWspro/bin:$PATH

	(in ksh)	export PATH=/opt/SUNWspro/bin:$PATH

    2.  Put /usr/lang (for 1.x) /opt/SUNWspro/man (for 2.x) in MANPATH, if 
        not already included.

        (in csh)	setenv MANPATH /opt/SUNWspro/man:$MANPATH

	(in ksh)	export MANPATH=/opt/SUNWspro/man:$MANPATH

    ** If you have installed SPARCworks in a directory other than 
       /usr or /opt (the default), replace /usr or /opt in the steps above
       with the directory path where SPARCworks is installed.

   NOTE: SPARCworks is not supported on machines without floating-point
         hardware. To verify if floating-point hardware is supported on your 
         machine, type: fpversion.
     
__________________________________________________________________________
F. SPARCworks README Files
 
   For information on individual SPARCworks tools, see the READMEs for a
   list of new features, enhancements, and  features that have changed.
   READMEs are located in the default directory /opt/SUNWspro/READMEs
   for 2.x and in /usr/lang/READMEs for 1.x.
	
__________________________________________________________________________
G. Patches Information
 
   After you have installed your software product, check the Patch
   directory on the CD for any of the latest patches that you might 
   need to install on your system.

__________________________________________________________________________
H. FileMerge Errata
 
   If you are using a 24-bit frame buffer and running FileMerge, you cannot 
   change FileMerge's default color palette. If you select the Colors 
   category from the FileMerge Properties window, FileMerge will display the
   message: "Colors palette is not supported." in the Properties window 
   footer. The FileMerge window closes to a black and white icon if you are 
   using a 24-bit frame buffer. 
__________________________________________________________________________
I. Sample Programs Used in the SPARCworks Manuals
 
   The SPARCworks document set uses the Blocks* and Freeway applications to 
   show the features of the SPARCworks tools. Blocks is a C++ implementation
   of a Lisp application called Blocks World. Freeway is a C++ program 
   developed at Sun Microsystems, Inc. 
 
   The Blocks and Freeway programs install automatically when you install
   SPARCworks. 

   The programs are located in either:
        
	/your_directory/SW3.0.1/examples/<Blocks, Freeway> for 1.x
   and  
	/your_directory/SUNWspro/SW3.0.1/examples/<Blocks, Freeway> for 2.x
 
   or the default directories:
 
        /usr/lang/SW3.0.1/examples/<Blocks, Freeway> for 1.x  
   and
	/opt/SUNWspro/SW3.0.1/examples/<Blocks, Freeway> for 2.x
 
   The Blocks and Freeway programs are write-protected. You should copy the 
   complete contents of both directories to your personal workspace.
 
   To run both programs, you must have Solaris 1.x or later and OpenWindows 3.x
   or later running on your system. You must also have access to SPARCompilers 
   4.0.1 C++ and the make(1) command. Note: make sure that you have 
   /usr/ccs/bin in your path.

   If you are running on 5.x and do not have access to a C++ SPARCompiler, 
   use the executable Freeway application provided in the sample directory to 
   perform the debugging exercises. The only exercise you will not be able to 
   do is the "Building a Freeway" exercise. Note the executable has a long 
   load time.
  
   To use the Blocks and the Freeway programs with SPARCworks, you must
   compile them with the SourceBrowser (-sb) and Debugger (-g) option. Both
   programs come with a Makefile that performs the compilation for you.
 
   To compile each program:
 
   1. Change to the directory where you made your copy of the program.
 
   2. Enter the following command at the system prompt: make debug
 
   When the program is finished compiling, bring up the SPARCworks
   tools in the directory in which you compiled the program. You
   are encouraged to enter and execute the examples of the Blocks program
   presented in the SPARCworks manuals, and to compare your results
   with those shown in the books. As an alternative, try out the features of 
   the SPARCworks tools using the Freeway program.
 
 
   * The Blocks application used in the SPARCworks manuals is derived
     from "Blocks World CLOS demo" from Chapter 21 of "Lisp", third
     edition, by Patrick Henry Winston and Berthold K.P. Horn,
     Copyright(c) 1989, 1984, 1981 by Addison-Wesley Publishing
     Company, Inc. and San Marco Associates.

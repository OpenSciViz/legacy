			SPARCworks/TeamWare 1.0.3


This README provides information about release 1.0.3 of SPARCworks/TeamWare.
The only changes contained in the 1.0.3 release (since the 1.0.2 release) are
minor bug fixes to the SPARCworks Manager and the addition of the
SPARCworks/TeamWare Solutions Guide.

Note:   * Releases 1.0, 1.0.1, 1.0.2 (Japan release only), and 1.0.3 of
	  TeamWare are compatible
	* Solaris 1 and 2 versions of TeamWare are compatible
	* A TeamWare 1.0 license can be used with 1.0.x executables


Table of Contents:

	A. New Features in release 1.0.3
	B. Features added previously in release 1.0.2
	C. Features added previously in release 1.0.1
	D. Release Notes
	E. Software Bugs
	F. Documentation Errata


A. New Features
_____________________________________________________________________________

The following features are new in release 1.0.3.

* The CheckPoint program (and its related programs, files and documentation)
  has been renamed to "FreezePoint".

* Solutions Guide

  A SPARCworks/TeamWare Solutions Guide is included with this release.  For
  this initial release, the guide is not available online through the
  AnswerBook system.

  The SPARCworks/TeamWare Solutions Guide is an evolving document - it is
  anticipated that this guide will be updated periodically to include new
  topics.

  This release is comprised of five scenario based topics and an in-depth case
  study, designed to help users take full advantage of SPARCworks/TeamWare
  features.  The topics are:

	o Workspace Hierarchy Strategies for Software Development and Release
	o Deleting Files From CodeManager Workspaces
	o Remote Software Development Using CodeManager
	o Using Makefiles With SPARCworks/TeamWare
	o Strategies for Deploying CodeManager in Software Development
	  Organizations

  A case study is included that describes how the SPARCworks/TeamWare tools are
  used by SunSoft in the development of the SunOS operating system:

	o Managing SunOS Development With SPARCworks/TeamWare

* QuickStart Guide

  A SPARCworks/TeamWare QuickStart Guide is included with SPARCworks/TeamWare
  1.0.3.

  This guide is a brief introduction to the SPARCworks/TeamWare code management
  tools.  This guide is intended as a means to quickly get software developers
  up, running, and productive using these tools.

* Color Icons
  
  In order to display TeamWare tool icons in color on the SPARCworks Manager
  palette (Solaris 2.x only), you must install patches 101771-01, 101773-01,
  and 101774-01.  These patches can be found on the CD under the directory
  "Solaris_2.x/Patch".


B. Features Added Previously In Release 1.0.2
_____________________________________________________________________________

The following features were new in release 1.0.2 and are not reflected in
the current product documentation.

* FileMerge Release 2.1

  Version 2.1 of FileMerge was new for SPARCworks/TeamWare release 1.0.1.
  Version 2.1 of FileMerge is completely compatible with the
  SPARCworks/TeamWare 1.0 release.  The following are new features of FileMerge
  2.1:

  - Horizontal scroll bars have been added to help you view long lines of
    text.

  - In addition to glyphs, differences in the input files are now indicated
    with highlighting and strikethrough.

  - If you have a color monitor, you now can assign different colors to
    highlight differences using the color palette. To find the color palette,
    select the Colors category from the Properties window.

  - Glyphs are no longer displayed next to differences in the output window.
    This makes it easier to edit the text in the output window.

  - An "About" button has been added. The About button displays a pop-up
    window that provides a brief description of FileMerge including its
    version number and copyright information. You can use the Comments button
    to send comments about FileMerge to SunSoft.

  Note:  The FileMerge program is also distributed with products other
	 than TeamWare. As a result, future releases of these other products
	 may include newer versions of FileMerge. SPARCworks/TeamWare release
	 1.0.x is compatible with versions of FileMerge 2.0.2 and higher.


C. Features Added Previously In Release 1.0.1
_____________________________________________________________________________

The following features were new in release 1.0.1 and are not reflected in
the current product documentation.

* ParallelMake Release 1.0.2

  The following change was made to the ParallelMake program.  This change
  affects some Release 1.0 users.

  ParallelMake Release 1.0, conditional macro targets, such as:

	foo := x += y

  and their dependencies are built in parallel with other conditional and
  non-conditional macro targets.  Under some conditions this creates
  "overloading" of the environment and causes some compile and link lines to
  include the same macro value multiple times.

  In ParallelMake Release 1.0.2, conditional macro targets are built in a
  different order.  They are no longer built in parallel with other targets -
  they are now built in "isolation" after all other targets have been built.

  Due to these new semantics for processing conditional macro targets, some
  Release 1.0 ParallelMake users may need to edit their makefiles and add
  appropriate .WAIT targets to their dependency lines.  See the section on
  .WAIT in the man page or the ParallelMake User's Guide for more information.


D. Release Notes
_____________________________________________________________________________

The following sections contain information about the individual TeamWare
products that you should know.  This information has not changed since Release
1.0 of TeamWare.

  Search Paths
  ------------
  All TeamWare executables search for other TeamWare programs and files (for
  example license files) that they execute and reference in known installation
  areas that are relative to where they (the running executables) are
  installed.  If files are not found by that method, the executables revert to
  the standard operating system search path method.  

  It is recommended that you add the TeamWare installation directory to your
  search path (PATH variable).

  ParallelMake
  ------------
  Unlike the other TeamWare executables, a link is not automatically made from
  the common access area to the ParallelMake package directory. The link is not
  automatically made because the executable is named make and is executed in
  place of the standard version of make.

  Note that some Makefiles may require modification in order to work correctly
  with ParallelMake (see ParallelMake User's Guide for more information).

  The ParallelMake executable is named make and is installed in the following
  package directories:

	o /opt/SUNWspro/ParallelMake/bin

  When you are ready to use ParallelMake, you must create a symlink or adjust
  your search path so that the distributed version is found ahead of the
  standard version of make. The following methods can be used:

  1. Make a symlink to the package directory from:

	/opt/SUNWspro/bin

     Be sure that the directory is in your search path ahead of the standard
     version of make which is usually located in:

	/usr/ccs/bin
	
  2. Add the package directory to your search path:

	/opt/SUNWspro/ParallelMake/bin

     Be sure that the directory is in your search path ahead of the
     standard version of make.

  
  Note: Method number 1 is recommended.


  CodeManager
  -----------
 * CodeManager SCCS Restrictions: rmdel, fix, comb and cdc

  Avoid using the SCCS rmdel, fix, comb and cdc commands in CodeManager
  workspaces. Use of these commands can change SCCS history files in ways that
  make them impossible for CodeManager track. These commands have undesirable
  side effects when used on files that exist in multiple workspaces that may
  eventually be merged.

    rmdel

    SCCS restricts the use of the rmdel command to remove only the most recent
    (leaf) delta on a branch. If you remove a delta that also exists in another
    workspace, it is possible that another user will add a delta in the other
    workspace. The delta that was removed in your workspace will no longer be a
    leaf delta when the files are merged.

    fix

    A front-end for rmdel.

    comb

    Completely rebuilds the SCCS history file. 

    cdc

    Can cause unnecessary branching and confusing histories.

* Differentiation

  CodeManager uses information in the root delta of SCCS history files to
  identify files and to track them when they are renamed. If SCCS history files
  are copied (using cp) within workspaces, multiple files contain identical
  root delta information and it becomes difficult for CodeManager to identify
  files.  When CodeManager detects this situation during Bringover and Putback
  transactions, it takes steps to differentiate between the different copies of
  the file by adding a line to the root delta of each copy of the file (the
  original file is not modified).

  In some rare, complicated cases you are notified to execute the workspace
  updatenames command; in this case you are prompted by updatenames to supply
  information to aid in the differentiation process.

  When CodeManager differentiates files, you are so notified in the Bringover
  or Putback transaction output.

* codemgrtool (CodeManager GUI)

  Conflicted Workspace Icon 

  A special icon is used in the CodeManager Workspace Graph pane to
  depict workspaces that contain unresolved conflicts. This icon was added
  after the TeamWare document set went to press and is therefore not
  documented.

  If you double-click SELECT on this icon, CodeManager automatically activates
  the Resolve transaction window.

* resolve

  The resolve CLI command can only be used on systems on which OpenWindows
  Version 3.1 is installed. The resolve command requires that the ToolTalk
  libraries be present.


E. Software Bugs
_____________________________________________________________________________

Please be aware of the following software bugs:

CodeManager
-----------

* If the GUI Load  Children/Parent commands encounter workspaces that
  they cannot access, they are ignored and no notice is given.

* If the Autoadvance property is not selected (selection is the default),
  you must manually select each file to be loaded into FileMerge from the
  resolve transaction window. Between selections, FileMerge remains in a "busy"
  state awaiting the next selection and cannot be used except by selecting
  another file in the Resolve transaction window, or by quitting.


F. Documentation Errata
_____________________________________________________________________________

Please be aware of the following documentation bugs:

Incorrect References in man Pages
---------------------------------

* A number of CodeManager man pages incorrectly refer to a fileresolve(1)
  man page. There is no fileresolve(1) man page.

* The freezepttool(1) man page incorrectly refers to the freezepointfile(5) man
  page as freezeptfile(5).


Additional CodeManager Error Messages
-------------------------------------

The following error messages were added to CodeManager after the documentation
went to press.

2623:	This warning can be issued in either of the following forms:

	o Read-lock left in workspace "workspace_name"
	o Write-lock left in workspace "workspace_name"

Meaning: A CodeManager command was unable to remove locks in
	 "workspace_name". May indicate that there is insufficient disk space,
	 or that permissions on the file Codemgr_wsdata/locks were changed
	 since the lock was originally written.

Remedy: Remove the locks using the CodeManager GUI Props  Workspace command or
	the CLI workspace locks command.


2627:	Directory "directory_name" is mounted read-only

Meaning: Before beginning Bringover and Putback transactions,
	 CodeManager checks to determine whether the destination workspace root
	 (top-level) directory is accessible for writing. This is not treated
	 as an error condition because lower level directories within the
	 workspace could be mounted from different areas and they may
	 accessible for writing. This warning is issued as an early warning
	 that directory permission might be set incorrectly.

Remedy: If write access is not intentionally denied, change the root directory
	permissions.

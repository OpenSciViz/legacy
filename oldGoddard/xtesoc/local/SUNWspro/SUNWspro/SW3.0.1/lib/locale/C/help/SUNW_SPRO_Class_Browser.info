#
# SUNW_SPRO_Class_Browser.info 16.2 93/08/10
#

#
#	Copyright (c) 10 Aug 1993 Sun Microsystems, Inc.  All Rights Reserved.
#

#
# This file contains all the help messages used by the
# Xview help mechanism from the ClassBrowser
#

:Main
ClassBrowser

ClassBrowser helps you effectively browse through
C++ source code and libraries.  ClassBrowser 
provides the following capabilities:

-  List and select from available classes
-  Navigate from class to class
-  Display class data and member functions
-  Show access protection of class members
-  Identify friend classes
-  Display the source of a class or member 
   function
-  Issue a query on a class or other symbol
-  Interact with ClassGrapher

:ClassButton
Browse button

Use the Browse button to browse a class or to list
all classes in your program.

You can browse a class in either the existing
ClassBrowser window or in a new ClassBrowser
window.  Select the class name anywhere on the 
screen, then choose either In Current Window
or In New Window from the Browse menu.

To bring up a pop-up window with an alphabetical
list of all classes in your program, choose
Class List from the class menu.

:GraphButton
Graph button

Use the Graph menu to view the class hierarchy in
graphical form.  ClassBrowser graphs the class
inheritance relationships in the ClassGrapher
window.

The Graph menu has the following options:

- Show Current Class option clears the 
  ClassGrapher window before graphing the class.
- Add Current Class option adds teh class to an
  existing graph.

You can also automatically graph the current class
by setting Graph Mode to one of the Auto settings
in the Property window.

:QueryButton
Query button

To issue a query on a class or a member defined in
a class, select the class or member name, then
click on the Query button.

The name can be a name you have typed in the Text
field, a name in a ClassBrowser pane, or a name in
another window. 

ClassBrowser displays the results of the query
in the SourceBrowser base window.

:SourceButton
Source button

To display the source code of a class or its
function members, select the class or function
name, then click on the Source button.

The name can be a name you have typed in the Text
field, a name in a ClassBrowser pane, or a name in
another window. 

By default, ClassBrowser displays the source code
in the SourceBrowser base window.  If you want to
display the code in the Debugger, set
Show Source In to Debugger in the ClassBrowser
Property window.

:PropsButton
Props button

Click on the Props button to open the Properties
window. The Properties window settings let you
customize ClassBrowser in the following ways:

-  Change the default display options
-  Turn on the glyphs for virtual and inline
   functions
-  Turn on automatic graphing
-  Choose to show source in the Debugger
-  Set a maximum for the Class history menu

:ClassHistMenu
Class menu

The Class menu maintains a history of the classes
you have browsed during the current session.

To revisit a previously browsed class, choose the
desired class from the menu.

By default, ClassBrowser includes up to 10
classes in the history menu.  You can change
this maximum in the Property window.

:ClassTextField
Class Text field

The Class text field indicates the class you are
currently browsing.

To browse a different class, select the class
name, then click on the Browse button.

You can also type the new class name in the text
field and press the Return key.

:BaseClassesMenu
Base Classes menu

The Base Classes menu lists the classes 
immediately inherited from the current class.

To browse a class in this menu, simply choose the
class name. 

If the current class has no base classes, this
item is inactive.

:DerivedClassesMenu
Derived Classes menu

The Derived Classes menu lists the classes
immediately derived from the current class.

To browse a class in this menu, simply choose the
class name. 

If the current class has no derived classes, this
item is inactive.

:FriendClassesMenu
Friend Classes menu

The Friend Classes menu lists the friend classes 
of the current class.

To browse a class in this menu, simply choose the
class name.

If the current class has no friend classes, this
item is inactive.

:DataMembersPane
Data Members pane

The Data Members pane lists the data members
defined in the current class.

ClassBrowser groups the members according to their
access protection: private, protected, and public.

You can also display inherited members and friend
functions by clicking on the boxes in the Display
Options pane. 

:MemberFunctionsPane
Function Members pane

The Function Members pane lists the function 
members defined in the current class.

ClassBrowser groups the function members
according to their access protection:  private,
protected, and public.

You can also display inherited members and friend
functions by clicking on the boxes in the Display
Options pane. 

The Function Members display pane may contain
glyphs that identify the virtual and inline 
functions in the current class.

A  hollow box indicates a virtual function.
A solid box indicates an inline function. 
A hollow box with a line through it indicates
both a virtual and inline function.

You can turn on and off the display of virtual
and inline functions in the Properties window.

:DisplayOptionsPane
Display Options pane

The Display Options pane controls the
information displayed in the Data and Function
Members panes.

To change your display, click on the box next
to the desired option.

The display options are non-exclusive.  If an 
option is inactive, then it does not apply to
the current class.

:ListSearchPattern
Search Pattern 

To list a subset of classes in the Class List
window, type the pattern that represents the
subset in the text field and press Return.

ClassBrowser uses shell-style wildcards when
making a match.

For example, if you enter the pattern *stream,
ClassBrowser returns all names that end with
the word "stream".

:ListSourceTypes
Types attribute

The Types attribute tells ClassBrowser what
to list in the Class List window:  classes,
templates, structures, unions, or any
combination thereof.

To change the current setting, click on the
desired attribute.  ClassBrowser updates the
List window immediately.

The Class List window includes only the C++-style
structs and unions and only instantiated template
classes.

:ListMatches
List Matches

By default, the Class List window contains an
alphabetical list of all available classes in your
program. 

By clicking on the settings in the Types field,
you can also list the instantiated template
classes, structs, and unions in your program.
These settings are nonexclusive.

:ListBrowse
Browse List

To browse an item in the Class List window,
first select an item from the class list.

To display the item in the existing
ClassBrowser window, choose In Current
Window from the Browse menu.  

Choose In New Window to activate a new
ClassBrowser.

:PropDisplayOptions
Property Display options

The Display Defaults property controls the 
information displayed in the data and function
members panes.

To change your display, click on the box next
to the desired option.  ClassBrowser applies
the changes when you click on Apply.

Use the Property window when you want to retain
the changes in the .SBdefaults file in your home
directory.  Subsequent ClassBrowser sessions use
these default property settings.

:PropShowGlyphs
Show Glyphs property

The Virtual setting tells ClassBrowser to mark
virtual functions in the Function Members pane
with a hollow box.  This setting is the default.

The Inline setting tells ClassBrowser to mark
inline functions with a solid box.

The Virtual and Inline settings are nonexclusive.
When both settings are on, ClassBrowser marks
virtual inline functions with a hollow box with
a line through it.

:PropGraphMode
Graph Mode property

With the Graph Mode property, you can instruct
ClassBrowser to automatically graph the current
class in the ClassGrapher window. 

If you set Auto: Show Current Class, ClassBrowser
clears the ClassGrapher before graphing the class.

If you set Auto: Add Current Class, ClassBrowser
adds the class to the existing graph.

If you set Manual, you must use the Graph menu in
the ClassBrowser base window to graph a class. 
This setting is the default.

:PropShowSource
Show Source property

The Browser setting instructs ClassBrowser to
display source code in the SourceBrowser window
when you click on the Source button.  This
setting is the default.

The Debugger setting instructs ClassBrowser
to display source code in the Debugger.

:PropMaximumHistory
Maximum History field

The Maximum History field tells ClassBrowser the 
number of classes to record in the Class Menu.  
Click the up and down arrows to the desired 
value.

The minimum value is one and the maximum value
is 30.  By default, ClassBrowser records the
ten most recently browsed classes, including 
the current class.

:PropApplyButton
Properties Apply button

Click on the Apply button to apply the changes you
have made in the Properties window.  

ClassBrowser dismisses the Properties window if
it is unpinned.

:PropResetButton
Properties Reset button

Click on the Reset button to discard any changes
you have made, but have not yet applied, in the
Properties window.  

The Properties window remains open so that you may
make further changes.

:ClassChoicesFrame
Class Choices frame

Your program has a nested class, and its name is
ambiguous with another regular class name.

Select the name of the class you want to browse,
then click on the Apply button.

:ClassChoicesList
Class Choices list

This list includes the nested class names that
are ambiguous with regular class names in
your program.

Select the class you want to browse, then
click o the Apply button.

:ClassChoicesButton
Class Choices Apply button

Click on the Apply button to browse the
class you selected.

#
# SUNW_SPRO_Class_Grapher.info 16.2 93/08/10
#

#
#	Copyright (c) 10 Aug 1993 Sun Microsystems, Inc.  All Rights Reserved.
#

#
# This file contains all the help messages used by the
# Xview help mechanism from the call grapher
#

:Main
ClassGrapher

ClassGrapher produces a graph for C++ class
hierarchies.  The class graph consists of nodes
and edges.  A node represents a class type.  An
edge represents class inheritance.  You can:

-  Add, find, or remove a node from the graph
-  Display the source of node 
-  Issue a query on a node
-  Browse a node

:ExpandButton
Expand button

Use the Expand menu to extend the class graph N
levels from the selected node(s), where N is the
expansion depth set in the Properties window.

The Expand menu has the following options:

- Derived Classes option expands the graph in the
  forward or derived class direction.
- Base Classes option expands the graph in the
  backward or base class direction.

To expand the graph in both the forward and
backward direction, choose Both Derived and 
Base classes.

:ViewButton
View button

Use the View menu to either redo the graph layout
or find a node in the graph.

To reposition the graph's nodes and edges, choose
Redo Layout.  Choose this item after you have 
edited the graph.

To find a node in the graph, select the name of
the node you want to find, then choose Find Node.

If ClassGrapher finds the node, it moves the node
near the center of the window.  If the Selection
property is set to Move to Show Node, ClassGrapher
highlights the node.

:QueryButton
Query button

To issue a query on a node, select the node name,
then click on the Query button. 

ClassGrapher displays the results of the query
in the SourceBrowser base window.

If you select more than one node, SourceBrowser
performs successive queries.

:SourceButton
Source button

To display the source code of a node, select the
node name, then click on the Source button.

By default, ClassGrapher displays the source code
in the SourceBrowser base window.

If you want to display the source in the Debugger,
set Show Source In to Debugger in the Properties
window.

:EditButton
Edit button

Use the Edit menu to remove nodes and edges from
the graph. The Edit menu has the following 
options:

- Clear Selected Nodes option removes the selected
  nodes. ClassGrapher also removes the edges that 
  connect the nodes with the rest of the graph.
- Clear Unselected Nodes option removes the nodes
  that are not selected. The selected nodes and
  their connecting edges remain.
- Contract option trims the graph around the selected
  nodes. ClassGrapher removes the nodes more than N
  levels away from the selected nodes, where N is the
  contraction radius set in the Properties window.
- Clear Graph removes all nodes and edges. 
  ClassGrapher leaves an empty window.

:BrowseButton
Browse button

To browse a node, select the name of the node you
want to browse, then click on the Browse button.

ClassGrapher opens a ClassBrowser window with
information about the selected node.

If you select more than one node, ClassBrowser
browses the last node you selected.

:PropsButton
Props button

Click on the Props button to open the Properties
window. Use the settings in this window to:

-  Change the expansion depth
-  Specify actions when you double-click on a node
-  Change the graph orientation
-  Move the selection during a graph operation
-  Show a node's source in the Debugger window
-  Specify whether or not edges have arrowheads
-  Change the contraction radius

:AddNodeButton
Add Node button

To add a node to the graph, select the node name,
then click on the Add Node button.

The node name can be a name you have selected in 
the ClassGrapher Text field, SourceBrowser
window, or ClassBrowser window.

ClassGrapher adds the node to the graph and
expands the graph around the added node.
ClassGrapher also adds all edges between new and
old nodes.

:TextItem
Text field

The text field is an area of ClassGrapher into
which you can type text from the keyboard.

To add a node to the graph, type the node name in
the Text field and press Return.

To find, issue a query on, or show the source of a
node, type and select the node name in the Text
field, then press the appropriate Grapher button.

:PropMain
Apply button

To set a property, click on the setting(s) of
interest, then click on the Apply button.

ClassGrapher dismisses the Properties window if
it is unpinned.

All changes you make in the Properties window are
retained in the .SBdefaults file in your home
directory.

:PropExpansionDepth
Expansion Depth

The Expansion Depth represents the distance from
the selected node(s) within which ClassGrapher
adds nodes and edges to the graph.

ClassGrapher applies the expansion depth when you
use either the Expand or Add Node buttons.

Click on the up or down arrows to the desired
value.  The minimum expansion is zero.

:PropDoubleClick
Double-click

Double-click specifies the action when you
double-click on a node. You can set Double-click
to:

-  Issue a query on the node
-  Display the node definition
-  Expand upon the node's derived classes
-  Expand upon the node's base classes
-  Contract the graph around the node

These settings are not mutually exclusive.

:PropGraphOrientation
Graph Orientation

Graph Orientation determines the direction of the
graph layout.

If you choose Horizontal, ClassGrapher displays
nodes and edges from left to right.

If you choose Vertical, ClassGrapher displays
nodes and edges from top to bottom.

:PropMoveSelection
Move Selection property

Selection determines whether or not ClassGrapher
moves the selection during a find node, add node,
or show source operation.

If you choose Moved to Show Node, ClassGrapher
moves the selection to the node you just found,
added, or showed the source of.

If you choose Not Moved, ClassGrapher does not
move the selection to the node you operated on.

:PropShowSource
Show Source In property

Show Source In tells ClassGrapher where to display
the source of a node when you click on the Source
button.

If you choose Browser, ClassGrapher displays the 
source in the SourceBrowser window.

If you choose Debugger, ClassGrapher displays the
source in the Debugger window.

:PropArrowhead
Arrowhead property

Arrowheads determines the appearance of the edges
in the graph.

If you choose Off, ClassGrapher draws edges as
lines.

If you choose On, ClassGrapher puts an arrowhead
at the endpoint of the line.

:PropTrackingDepth
Contraction Radius property

The Contraction Radius represents the distance
from the selected node(s) beyond which
ClassGrapher removes nodes and edges when you
contract the graph.

For example, if you set the contraction radius
to two, the Grapher removes all nodes and edges
at radius three or more from the selected node
when you contract the graph.

Click the up or down arrows to the desired value. 
The minimum radius is one.

:PropApplyButton
Properties Apply button

Click on the Apply button to apply the changes
you have made in the ClassGrapher Properties
window.

The changes you make in the Properties window
are retained in the .SBdefaults file in your
home directory.

All subsequent ClassGrapher sessions will use
the property settings in this file.

:PropResetButton
Properties Reset button

Click on the Reset button to discard any changes
you have made, but have not yet applied, in the
Properties window.

The Properties window remains open so you can
make additional changes.


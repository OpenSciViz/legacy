#include <xview/xview.h>
#include <X11/Xlib.h>

extern "C" Xv_opaque xv_pf_default ();
extern "C"           xv_pf_textbound (...);

extern "C" int xv_col (Xv_opaque, int);
extern "C" int xv_row (Xv_opaque, int);


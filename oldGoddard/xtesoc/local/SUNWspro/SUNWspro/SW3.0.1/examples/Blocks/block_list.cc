#include "block_list.h"

list* remove(list& c, list& list)
{ return (remove(*c.value(), list)); }

list* remove(block& c, list& block_list)
{ list start(0, block_list);
  for (list *i = &block_list, *j = &start; i != 0; j = i, i = i->cdr)
    { if (!strcmp((i->value())->name(), c.name()))
	{ j->cdr = i->cdr;
	  delete i;
	  break; }}
  return start.cdr;
}

ostream& operator<<(ostream& o, list c)
{ o << "(";
  for (list *i = &c; i != 0; i = i->next())
    { o << (*i->value());
      if (i->next() != 0)
	o << " "; }
  return (o << ")"); }



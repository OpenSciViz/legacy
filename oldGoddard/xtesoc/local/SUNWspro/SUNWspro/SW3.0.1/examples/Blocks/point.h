#ifndef POINT_H
#define POINT_H

#include <stream.h>

class point {
 private:
  int xval, yval;
 public:
  point(int x = 0, int y = 0) { xval = x; yval = y; }
  int& x() { return xval; }
  int& y() { return yval; }
  point convert(int x_offset = 0, int y_offset = 0); 
  friend int operator==(const point&, int);
  friend ostream& operator<<(ostream&, const point&);
};

inline int operator!=(const point& p, int i) { return !(p == i); }

#endif

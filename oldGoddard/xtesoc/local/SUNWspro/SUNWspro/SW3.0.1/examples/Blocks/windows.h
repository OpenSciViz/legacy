#ifndef WINDOW_H
#define WINDOW_H

#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/svrimage.h>
#include <xview/fullscreen.h>
#include <xview/cms.h>
#include <xview/icon.h>
#include <xview/font.h>
#include <xview/wmgr.h>
#include <X11/Xlib.h>

#include "point.h"
#include "block.h"

const int color_ct = 64;
const int bg_color = 0;

const int world_width  = 26;
const int world_height = 24;
const int world_margin = 5;
const int world_y_offset = 2;

extern int monochrome, label_blocks, in_auto_mode;
extern int fg_color;
extern int world_factor;
extern Frame frame;
extern Xv_opaque world;
extern Xv_opaque pw;
extern Xv_font block_font;
extern GC gc;
extern unsigned long *colors;

void make_window(int argc, char **argv, char **envp);
block* find_block(Panel_item pi);
int convert_dim(int);
void draw_cylinder (Xv_opaque pw, point nw, point se);
void draw_cone     (Xv_opaque pw, point sw, point nc, point se);

#endif

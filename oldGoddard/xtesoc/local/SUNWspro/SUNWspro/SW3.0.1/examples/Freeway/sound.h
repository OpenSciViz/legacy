#ifndef SOUND_H
#define SOUND_H

//
// sound.H
//
// Header file for sound portions of "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

void sound_off();		// turn off sound

void sound_on();		// turn on sound (default is on)

void play(char *filename);	// play the given audio file

#endif

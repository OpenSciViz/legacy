#ifndef	window_HEADER
#define	window_HEADER

//
// window_ui.h - User interface object and function declarations.
// This file was generated by `gxv++' from `window.G'.
// DO NOT EDIT BY HAND.
//

extern Attr_attribute	INSTANCE;

Xv_opaque	window_menu_file_create(caddr_t, Xv_opaque);
Xv_opaque	window_menu_reset_create(caddr_t, Xv_opaque);

class window_traffic_objects {

public:
	Xv_opaque	traffic;
	Xv_opaque	controls1;
	Xv_opaque	gap;
	Xv_opaque	clock_label;
	Xv_opaque	clock;
	Xv_opaque	button_file;
	Xv_opaque	button_start;
	Xv_opaque	button_stop;
	Xv_opaque	button_reset;
	Xv_opaque	choice_randomize;
	Xv_opaque	time;
	Xv_opaque	controls2;
	Xv_opaque	speed1;
	Xv_opaque	ave1u;
	Xv_opaque	speed2;
	Xv_opaque	ave2u;
	Xv_opaque	speed3;
	Xv_opaque	ave3u;
	Xv_opaque	speed4;
	Xv_opaque	ave4u;
	Xv_opaque	speed5;
	Xv_opaque	ave5u;
	Xv_opaque	ave1l;
	Xv_opaque	ave2l;
	Xv_opaque	ave3l;
	Xv_opaque	ave4l;
	Xv_opaque	ave5l;
	Xv_opaque	canvas1;

	virtual void	objects_initialize(Xv_opaque);

	virtual Xv_opaque	traffic_create(Xv_opaque);
	virtual Xv_opaque	controls1_create(Xv_opaque);
	virtual Xv_opaque	gap_create(Xv_opaque);
	virtual Xv_opaque	clock_label_create(Xv_opaque);
	virtual Xv_opaque	clock_create(Xv_opaque);
	virtual Xv_opaque	button_file_create(Xv_opaque);
	virtual Xv_opaque	button_start_create(Xv_opaque);
	virtual Xv_opaque	button_stop_create(Xv_opaque);
	virtual Xv_opaque	button_reset_create(Xv_opaque);
	virtual Xv_opaque	choice_randomize_create(Xv_opaque);
	virtual Xv_opaque	time_create(Xv_opaque);
	virtual Xv_opaque	controls2_create(Xv_opaque);
	virtual Xv_opaque	speed1_create(Xv_opaque);
	virtual Xv_opaque	ave1u_create(Xv_opaque);
	virtual Xv_opaque	speed2_create(Xv_opaque);
	virtual Xv_opaque	ave2u_create(Xv_opaque);
	virtual Xv_opaque	speed3_create(Xv_opaque);
	virtual Xv_opaque	ave3u_create(Xv_opaque);
	virtual Xv_opaque	speed4_create(Xv_opaque);
	virtual Xv_opaque	ave4u_create(Xv_opaque);
	virtual Xv_opaque	speed5_create(Xv_opaque);
	virtual Xv_opaque	ave5u_create(Xv_opaque);
	virtual Xv_opaque	ave1l_create(Xv_opaque);
	virtual Xv_opaque	ave2l_create(Xv_opaque);
	virtual Xv_opaque	ave3l_create(Xv_opaque);
	virtual Xv_opaque	ave4l_create(Xv_opaque);
	virtual Xv_opaque	ave5l_create(Xv_opaque);
	virtual Xv_opaque	canvas1_create(Xv_opaque);
};

class window_popup1_objects {

public:
	Xv_opaque	popup1;
	Xv_opaque	canvas2;
	Xv_opaque	controls3;
	Xv_opaque	textfield_vehicle;
	Xv_opaque	textfield_position;
	Xv_opaque	choice_class;
	Xv_opaque	textfield_front_gap;
	Xv_opaque	choice_state;
	Xv_opaque	textfield_behind_gap;
	Xv_opaque	textfield_velocity;
	Xv_opaque	button_step;
	Xv_opaque	butten_remove;
	Xv_opaque	textfield_max_speed;

	virtual void	objects_initialize(Xv_opaque);

	virtual Xv_opaque	popup1_create(Xv_opaque);
	virtual Xv_opaque	canvas2_create(Xv_opaque);
	virtual Xv_opaque	controls3_create(Xv_opaque);
	virtual Xv_opaque	textfield_vehicle_create(Xv_opaque);
	virtual Xv_opaque	textfield_position_create(Xv_opaque);
	virtual Xv_opaque	choice_class_create(Xv_opaque);
	virtual Xv_opaque	textfield_front_gap_create(Xv_opaque);
	virtual Xv_opaque	choice_state_create(Xv_opaque);
	virtual Xv_opaque	textfield_behind_gap_create(Xv_opaque);
	virtual Xv_opaque	textfield_velocity_create(Xv_opaque);
	virtual Xv_opaque	button_step_create(Xv_opaque);
	virtual Xv_opaque	butten_remove_create(Xv_opaque);
	virtual Xv_opaque	textfield_max_speed_create(Xv_opaque);
};

class window_popup_name_objects {

public:
	Xv_opaque	popup_name;
	Xv_opaque	controls_name;
	Xv_opaque	textfield_directory;
	Xv_opaque	textfield_filename;
	Xv_opaque	button_save;
	Xv_opaque	button_cancel;

	virtual void	objects_initialize(Xv_opaque);

	virtual Xv_opaque	popup_name_create(Xv_opaque);
	virtual Xv_opaque	controls_name_create(Xv_opaque);
	virtual Xv_opaque	textfield_directory_create(Xv_opaque);
	virtual Xv_opaque	textfield_filename_create(Xv_opaque);
	virtual Xv_opaque	button_save_create(Xv_opaque);
	virtual Xv_opaque	button_cancel_create(Xv_opaque);
};

class window_popup_story_objects {

public:
	Xv_opaque	popup_story;
	Xv_opaque	textpane1;

	virtual void	objects_initialize(Xv_opaque);

	virtual Xv_opaque	popup_story_create(Xv_opaque);
	virtual Xv_opaque	textpane1_create(Xv_opaque);
};
#endif

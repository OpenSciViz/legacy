#ifndef VEHICLE_LIST_H
#define VEHICLE_LIST_H

#include <stream.h>
#include <string.h>

#include "vehicle.h"

typedef vehicle * vehicleP;

class list {
 protected:
  list *n, *p;			// next and previous
  vehicleP v;			// this vehicle

 public:
  list()              { n = p = this; v = 0; }
  vehicleP value()    { return v; }
  int      hasValue() { return (this && v); }
  int      isEmpty()  { return (n == this); }
  list *   first()    { return n; }
  list *   last()     { return p; }
  list *   next()     { return n; }
  list *   prev()     { return p; }
  list *   next   (vehicleP);
  list *   prev   (vehicleP);
  list *   find   (vehicleP);
  void     remove (vehicleP);
  void     append (vehicleP);
  void     prepend(vehicleP);
  void     insert (vehicleP);
};

#define insertAfter  prepend
#define insertBefore append

ostream & operator<< (ostream &, list &);

#endif

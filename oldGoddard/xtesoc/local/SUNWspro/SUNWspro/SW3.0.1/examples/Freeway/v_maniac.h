#ifndef V_MANIAC_H
#define V_MANIAC_H

//
// v_maniac.H
//
// Header file for maniac class for "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <stream.h>

#include "vehicle.h"
#include "v_sports.h"

#define CLASS_MANIAC 3

class maniac : public sports_car {
 protected:
  
 public:
  maniac (int = 0, int = 0, double = 0.0, double = 0.0);

  virtual char   *classname()       { return "maniac"; }
  virtual int     classnum()        { return CLASS_MANIAC; }

  virtual void    check_lane_change(vehicle *in_front, void *neighbors);
  virtual double  optimal_dist (vehicle *in_front);
  virtual int     limit_speed (int limit);
};

#endif

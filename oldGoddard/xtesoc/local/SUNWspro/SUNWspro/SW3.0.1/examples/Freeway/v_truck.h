#ifndef V_TRUCK_H
#define V_TRUCK_H

//
// v_truck.H
//
// Header file for truck class for "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <stream.h>

#include "vehicle.h"

#define CLASS_TRUCK 1

class truck : public vehicle {
 protected:
  
 public:
  truck (int = 0, int = 0, double = 0.0, double = 0.0);

  virtual char         *classname()       { return "truck"; }
  virtual int           classnum()        { return CLASS_TRUCK; }
  virtual double        vehicle_length();
  virtual void          recalc_velocity();
  virtual double        optimal_dist(vehicle *in_front);
  virtual void          draw (Display *display, Drawable pix, GC gc, 
			      int x, int y, int direction_right, int scale, 
			      int xorg, int yorg, int selected);
};

#endif

#ifndef V_SPORTS_H
#define V_SPORTS_H

//
// v_sports.H
//
// Header file for sports_car class for "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <stream.h>

#include "vehicle.h"

#define CLASS_SPORTS_CAR 2

class sports_car : public vehicle {
 protected:
  
 public:
  sports_car (int = 0, int = 0, double = 0.0, double = 0.0);

  virtual char         *classname()       { return "sports car"; }
  virtual int           classnum()        { return CLASS_SPORTS_CAR; }
  virtual double        vehicle_length();
  virtual void          recalc_velocity();
  virtual double        optimal_dist(vehicle *in_front);
  virtual void          draw (Display *display, Drawable pix, GC gc, 
			      int x, int y, int direction_right, int scale, 
			      int xorg, int yorg, int selected);
};

#endif

//
// sound.C
//
// Implementation of sound portions of "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <xview/notify.h>
#include <stream.h>
#include <unistd.h>

// extern "C" int vfork();

extern Notify_client nclient;	// from traffic.C

static int sound_pending = FALSE;
static int sound_is_off  = FALSE;


void
sound_off()
{
  sound_is_off = TRUE;
}


void
sound_on()
{
  sound_is_off = FALSE;
}


//
// Local handler.  Gets called when child process dies.
//
static Notify_value
wait3_handler(Notify_client, int, union wait *, struct rusage *)
{
  sound_pending = FALSE;
  return NOTIFY_DONE;
}


//
// Forks a "play" command to play the given audio file.
// The only unusual part is that the parent process uses the notifier's
// default wait3() function to reap the child process when it's done.
// This is documented in the XView Programming Manual p. 368.
//
void 
play(char *audiofile)
{
  char *argv[5];
  char *play = "./play";
  int   pid;

  argv[0] = play;
  argv[1] = "-v";
  argv[2] = "100";
  argv[3] = audiofile;
  argv[4] = NULL;

  if (sound_is_off || sound_pending) 
    {
      return;			// one sound at a time
    }

  if ((pid = vfork()) == 0)	// child process gets 0
    {
      execv(play, argv);
      exit(1);
    }
  else				// parent process gets child PID
    {
      sound_pending = TRUE;
      notify_set_wait3_func(nclient, (Notify_func) wait3_handler, pid);
    }
}


#include "vehicle_list.h"


list* list::prev(vehicleP veh)
{
  list *i = this->find(veh);

  if (i->hasValue()) return i->p;
  else               return NULL;    // couldn't find vehicle
}


list* list::next(vehicleP veh)
{
  list *i = this->find(veh);

  if (i->hasValue()) return i->n;
  else               return NULL;    // couldn't find vehicle
}


list* list::find(vehicleP veh)
{
  for (list *i = this->first(); i->hasValue(); i = i->n) 
    {
      if (i->v->name() == veh->name()) 
	{
	  return i;
	}
    }
  return NULL; // couldn't find vehicle
}


void list::remove(vehicleP veh)
{
  list *i = this->find(veh);

  if (i->hasValue())
    {
      i->p->n = i->n;
      i->n->p = i->p;
//      delete i;
    }
}


void list::append(vehicleP veh)
{
  list *i = new list();
  
  i->v = veh;
  i->n = this;
  i->p = p;
  p->n = i;
  p    = i;
}


void list::prepend(vehicleP veh)
{
  list *i = new list();

  i->v = veh;
  i->p = this;
  i->n = n;
  n->p = i;
  n    = i;
}


void list::insert(vehicleP veh)
{
  //
  // Scan over list looking for element which is smaller than v and then insert v after it.
  //
  for (list *i = this->last(); i->hasValue(); i = i->p) 
    {
      if (i->v->pos() < veh->pos())
	{
	  i->insertAfter(veh);
	  return;
	}
    }
  //
  // If there is no element smaller than v, insert it at the beginning.
  //
  this->insertAfter(veh);
}


ostream& operator<< (ostream & o, list & l)
{ 
  o << "{ ";

  for (list *i = l.first(); i->hasValue(); i = i->next())
    {
      o << i->value();
      if (i != l.last()) o << " , ";
    }

  o << " }" << endl;

  return o;
}


#ifdef  TESTING

main()
{
  list *l = new list();

  cout << "is empty list? " << l->isEmpty() << " : " << *l << endl;

  l->insert(3);

  l->insert(1);

  l->insert(2);

  cout << "full list: " << *l << endl;

  l->remove(2);

  l->remove(1);

  cout << "should have 3: " << *l << endl;

  l->append(17);
  l->append(10);

  cout << "append 17 then 10: " << *l << endl;

  l->remove(17);

  l->remove(3);

  l->remove(10);

  l->remove(4);

  l->prepend(2);

  cout << *l << endl;
}


#endif


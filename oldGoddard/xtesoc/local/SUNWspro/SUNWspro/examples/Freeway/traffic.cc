//
// traffic.C
//
// Most of the application logic for "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <X11/Xos.h>
#include <xview/notify.h>
#include <stream.h>
#include <fstream.h>
#include "sound.h"
#include "vehicle.h"
#include "vehicle_list.h"
#include "v_truck.h"
#include "v_sports.h"
#include "v_maniac.h"
#include "v_police.h"


#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/notice.h>
#include <xview/textsw.h>
#include <xview/xv_xrect.h>
#include "window_ui.h"

#include <X11/Xlib.h>

#include <math.h>

//
// Forward declarations
//
void traffic_start();
void traffic_display();
void traffic_reset();
void traffic_stop();
void traffic_popup_display(int);
void traffic_popup_done();
void traffic_generate();
void traffic_popup(vehicle *);
void traffic_gap(int);
void traffic_time(int);
void traffic_state(int);
void traffic_class(int);
void traffic_randomize(int);
void traffic_position(char *);
void traffic_velocity(int);
void traffic_max_speed(int);
void traffic_force_popup_done();
void traffic_set_popup_values(vehicle *);
void traffic_popup_scroll(vehicle *);
void traffic_save();
void traffic_cancel();
void traffic_do_load();
static unsigned long state_to_color(int);


//
// Constants for drawing on window.
//
const int           N_LANES        = 4;
const int           LANES_EACH_DIR = 2;
const unsigned long WHITE          = 0;
const unsigned long BLACK          = 1;
const int           PIX_PER_MILE   = 2000;
const int           PIX_PER_SIGN   = 256;
const int           CYC_PER_STAT   = 5;
const int           LANE_WIDTH     = 12;
const int           DASH_PERIOD    = 20;  
const int           DASH_LENGTH    = 5;  
const int           CAR_LENGTH     = 7;	  
const int           CAR_WIDTH      = 4;	  
const int           TWENTIETH_SEC  = 50000;   // microseconds
const int           TENTH_SEC      = 100000;  // microseconds
const int           ONE_SEC        = 1000000; // microseconds


//
// Global variables.  If they are not static, chances are that they are
// employed outside this file.
//
static int name_counter = 0;  // Generate unique names for vehicles

       int              pixel_depth    = 8;
static char            *filename       = NULL;
       Notify_client    nclient        = NULL;
static GC               gc             = NULL;
static Display         *display        = NULL;
static Drawable         drawable       = NULL;
static Drawable         popup_xwin     = NULL;
static Pixmap           pix_1          = NULL;
static Pixmap           pix_2          = NULL;
static Pixmap           pop_pix        = NULL;
       vehicle         *popup_current  = NULL;
static int              running        = FALSE;
static int              was_running    = FALSE;
       int              randomize      = 0;
static list            *lanes[4];
static int              will_do_load;
static struct itimerval timer;
static int              yloc[4];
       int              width;
       double           width_in_miles;
static int              height, middle;
static int              popup_width, popup_height, popup_top;
static int              limits[5];
static double           generate_gap;
static int              update_time;
static int              lanes_top, lanes_height;
       unsigned long    color_black, color_white, color_blue, color_violet,
                        color_red,   color_green, color_orange, color_yellow,
			color_cadet;
static window_popup1_objects      *popup_obj  = NULL;
static window_traffic_objects     *window_obj = NULL;
static window_popup_story_objects *story_obj  = NULL;
static window_popup_name_objects  *name_obj   = NULL;

static char *state_names[] = {
  "Maintain", "Accelerate", "Coast", "Brake", "Crash", "Top Speed", 
  "Lane Change" };

//
// Macros deal with lanes in their pre-configured arrangement:
// (Okay, so it's a silly arrangement; it has to do with history.)
//
//  -------------------------------
//  -  -  -  -  Lane 3  -  -  -  - 
//  -------------------------------  <== traffic going left  <==
//  -  -  -  -  Lane 1  -  -  -  - 
//  -------------------------------
//  -------------------------------
//  -  -  -  -  Lane 0  -  -  -  - 
//  -------------------------------  ==> traffic going right ==>
//  -  -  -  -  Lane 2  -  -  -  - 
//  -------------------------------
//
#define IsUpperLane(n)      ((n) & 1)
#define IsLowerLane(n)      (!IsUpperLane(n))
#define IsInsideLane(n)     ((n) < 2)
#define LowerToUpperLane(n) ((n)+1)
#define UpperToLowerLane(n) ((n)-1)
#define GetNeighborLane(n)  ( (IsLowerLane(n)) ? 2 - (n) : 4 - (n) )

#define ChangingLanes(s)    ( (s) == VSTATE_CHANGE_LANE  || \
			      (s) == VSTATE_CHANGE_LEFT  || \
			      (s) == VSTATE_CHANGE_RIGHT )

//
// Used to decice whether mouse click is on particular car
//
int
is_between(int n, int lo, int hi)
{
  return (n >= (lo) && n <= (hi));
}


//
// Given a vehicle, this function finds out which speed zone (segment) it's in.
//
int
get_segment(vehicle *v)
{
  int seg = (int) (v->xloc() / PIX_PER_SIGN);

  if      (seg > 4) seg = 4;
  else if (seg < 0) seg = 0;

  return seg;
}


//
// Given a segment (speed zone) and a lane number, this function returns
// the message item which displays average velocity there.
//
Xv_opaque
seg_to_message(int seg, int lane)
{
  switch(lane) 
    {
    case 0:
    case 2:
      switch(seg)
	{
	case 0: return window_obj->ave1l;
	case 1: return window_obj->ave2l;
	case 2: return window_obj->ave3l;
	case 3: return window_obj->ave4l;
	case 4: return window_obj->ave5l;
	}
      break;
    case 1:
    case 3:
      switch(seg)
	{
	case 0: return window_obj->ave1u;
	case 1: return window_obj->ave2u;
	case 2: return window_obj->ave3u;
	case 3: return window_obj->ave4u;
	case 4: return window_obj->ave5u;
	}
      break;
    }
  return NULL; // should never happen
}


//
// Show the current filename and status in the application frame.
//
void
traffic_frame()
{
  char *name   = filename ? filename  : "";
  char *status = running  ? "Running" : "Stopped";

  if (popup_current && was_running) status = "Suspended";

  xv_set(window_obj->traffic,
	 FRAME_LEFT_FOOTER,  name, 
	 FRAME_RIGHT_FOOTER, status, 
	 NULL);
}


//
// Do statistics for average velocities.  Most times, don't display anything
// for performance reasons.  Only do it every CYC_PER_STAT times.  If the
// boolean argument delay is FALSE, display it now regardless.
//
void
traffic_stats(int delay)
{
  static int    stats_counter = 0;
  static int    segment_cnt[2][5];
  static double segment_vel[2][5];
  static char   buff[10];

  // 
  // Most times, do nothing.
  //
  if (delay && ((stats_counter++ % CYC_PER_STAT) != 0)) return;

  //
  // Initialize counters
  //
  for (int j = 0; j < 2; j++) 
    {
      for (int i = 0; i < 5; i++) 
	{
	  segment_vel[j][i] = 0.0;
	  segment_cnt[j][i] = 0;
	}
    }

  //
  // Find average velocity in each segment
  //
  for (j = 0; j < N_LANES; j++) 
    {
      for (list *l = lanes[j]->first(); l->hasValue(); l = l->next())
	{ 
	  vehicle *v = l->value(); 
	  int    seg = get_segment(v);
	  int    top = (j == 1 || j == 3) ? 1 : 0;
	  
	  segment_cnt[top][seg]++;
	  segment_vel[top][seg] += v->vel();
	}
    }
  
  // 
  // Display average velocities
  //
  for (j = 0; j < 2; j++) 
    {
      for (int i = 0; i < 5; i++) 
	{
	  Xv_opaque message;
	  double    count = (double) segment_cnt[j][i];
	  
	  if (count == 0) sprintf(buff, " ");
	  else            sprintf(buff, "%2d", 
				  (int) (segment_vel[j][i] / count));
	  
	  message = seg_to_message(i, j);
	  xv_set(message, PANEL_LABEL_STRING, buff, NULL);
	}
    }
}


//
// Deal with the clock.  Argument says whether to advance or reset time.
///
void
clock_advance(int advance)
{
  static char buffer[30];
  static int  hours;
  static int  mins;
  static int  secs;
  static int  tenths;
  
  if (advance)
    tenths += 2;		      // advance 1/5 second
  else
    hours = mins = secs = tenths = 0; // zero the clock

  if (tenths >= 10)
    {
      tenths = 0;
      secs += 1;
      if (secs >= 60) 
	{
	  secs = 0;
	  mins += 1;
	  if (mins >= 60)
	    {
	      mins = 0;
	      hours += 1;
	    }
	}
    }

  sprintf(buffer, "%d:%02d:%02d.%d", hours, mins, secs, tenths);

  // Set clock message to show simulation clock.
  xv_set(window_obj->clock, PANEL_LABEL_STRING, buffer, NULL);
}


//
// Advance all of the vehicles in their lanes.  Basically, this loops over
// all the vehicles in all the lanes and tells them to recalculate.
//
void
traffic_advance()
{
  for (int j = 0; j < N_LANES; j++)
    {
      list *next = NULL;

      for (list *i = lanes[j]->first(); i->hasValue(); i = next)
	{ 
	  vehicle *current, *in_front;
	  int      index, limit;
	  
	  current = i->value();
	  next    = i->next(); 
	  index   = get_segment(current);
	  limit   = limits[index];

	  //
	  // Vehicles in the inside lane get to go 5 mph over limit.
	  //
	  if (IsInsideLane(j)) limit += 5;
	  
	  //
	  // Tell vehicle to recalculate 
	  //
	  list *neighbors = lanes[GetNeighborLane(j)];
	  list *upperlane = IsLowerLane(j) ? lanes[LowerToUpperLane(j)] : NULL;

	  if (next->hasValue())
	    {
	      in_front = next->value();
	    }
	  else 
	    {
	      if (upperlane && !upperlane->isEmpty())
		   in_front = upperlane->first()->value();
	      else in_front = NULL;
	    }
	  current->recalc(in_front, limit, (void *) neighbors);

	  //
	  // See if it has gone off the edge
	  //
	  if (IsLowerLane(j) && current->xloc() > width)
	    {
	      //
	      // Gone off the right side of bottom lane; turn it around.
	      //
	      double p  = current->pos() - width_in_miles;
	      int    up = LowerToUpperLane(j);
	      current->lane(up);
	      current->pos(p);
	      lanes[j] ->remove (current);
	      lanes[up]->prepend(current);
	      break;
	    }
	  else if (IsUpperLane(j) && current->xloc() < -10)
	    {
	      //
	      // Gone off the left side of upper lane; remove it.
	      //
	      if (current == popup_current) traffic_force_popup_done();
	      lanes[j]->remove(current);
	      delete current;
	    }
	  //
	  // Deal with lane changes
	  //
	  else if (ChangingLanes(current->vstate()))
	    {
	      int delta = current->lane_change();
	      
	      if (delta >= CAR_WIDTH)
		{
		  int n = GetNeighborLane(j);
		  lanes[j]->remove(current);
		  lanes[n]->insert(current);
                  current ->lane  (n);
		  current ->vstate(VSTATE_MAINTAIN);
		  current ->lane_change(LANE_WIDTH - delta - 2);
		}
	      else 
		{
		  current->lane_change(delta + 1);
		}
	    }
	  else if (current->lane_change())
	    {
	      current->lane_change(current->lane_change() - 1);
	    }
	}
    }
}


//
// Advances simulation.  Internal function called every so often by a timer.
//
Notify_value
traffic_simulate(Notify_client, int)
{
  clock_advance(TRUE);		// advance simulation clock

  traffic_advance();		// advance time for all vehicles
  
  traffic_generate();		// potentially generate new vehicles
  
  traffic_stats(TRUE);		// do statistics on vehicles in lanes
  
  traffic_display();		// display the vehicles in new states
  
  return NOTIFY_DONE;
}


//
// Give me a new vehicle of the given class (passed by number).
//
vehicle *
new_vehicle(int classnum, int name, int lane, double pos, double vel)
{
  vehicle *v;

  switch (classnum)
    {
    case CLASS_TRUCK: 
      v = (vehicle *) new truck(name, lane, pos, vel); break; 

    case CLASS_SPORTS_CAR: 
      v = (vehicle *) new sports_car(name, lane, pos, vel); break;

    case CLASS_MANIAC:
      v = (vehicle *) new maniac(name, lane, pos, vel); break;

    case CLASS_POLICE: 
      v = (vehicle *) new police(name, lane, pos, vel); break;

    case CLASS_VEHICLE:
    default:
      v = new vehicle(name, lane, pos, vel);
    }

  return v;
}


//
// Called for every simulation update.  May generate new cars if there is room.
//
void
traffic_generate()
{
  //
  // If the lane is empty or if the last car in lane is beyond "generate_gap"
  // then generate a new car in that lane.  "generate_gap" is set by slider.
  // The new vehicle takes a position of 0 and velocity of car in front.
  //
  for (int i = 0; i < N_LANES; i += 2) 
    {
      vehicle *last = (lanes[i]->isEmpty()) ? NULL:lanes[i]->first()->value();
  
      if ((!last || last->rear_pos() > generate_gap) &&
	  (!randomize || !name_counter || (rand() % 4 == 0))) 
	{
	  vehicle *v;
	  int      classnum;
	  int      limit = IsInsideLane(i) ? limits[0] : limits[0] + 5;
	  double   vel   = (last) ? last->vel() : (double) limit;
	  
	  if      (rand() % 10 == 0) classnum = CLASS_SPORTS_CAR;
	  else if (rand() % 15 == 0) classnum = CLASS_TRUCK;
	  else                         classnum = CLASS_VEHICLE;

	  v = new_vehicle(classnum, name_counter++, i, 0.0, vel);
	  lanes[i]->prepend(v);
	}
    }
}


//
// Draw box outline in given gc
//
void
draw_box(Drawable win, GC gc, int l, int t, int r, int b)
{
#define DrawThickLine(dy, de, gc, x1, y1, x2, y2)     \
      XFillRectangle(dy, de, gc, x1, y1, (x2 - x1 + 2), (y2 - y1 + 2));

  DrawThickLine(display, win, gc, l, t, r, t);
  DrawThickLine(display, win, gc, r, t, r, b);
  DrawThickLine(display, win, gc, l, b, r, b);
  DrawThickLine(display, win, gc, l, t, l, b);
}


static int box_l, box_r, box_t, box_b;

//
// Draw box around selected area
//
void
BoxPopupArea()
{
  int x = popup_current->xloc();
  int y = middle;

  box_l = x - popup_width  / 6;	// left
  box_r = x + popup_width  / 6;	// right
//box_t = y - popup_height / 6;	// top   -- these 2 are set in traffic_repaint
//box_b = y + popup_height / 6;	// bottom--   rather than here.

  XSetForeground(display, gc, (pixel_depth > 1) ? color_red : color_white);
  draw_box(drawable, gc, box_l, box_t, box_r, box_b);
}


//
// Tell all the vehicles to redraw themselves
//
static void
draw_vehicles(Drawable pix, int scale, int xoff, int yoff)
{
  for (int j = 0; j < N_LANES; j++)
    {
      for (list *i = lanes[j]->first(); i->hasValue(); i = i->next())
	{ 
	  vehicle *current  = i->value();
	  int      x        = (int) (current->pos() * PIX_PER_MILE);
	  int      go_right = IsLowerLane(j);
	  //
	  // Decide whether vehicle is moving to the right or left.
	  //
	  x = go_right ? x : width - x;
	  
	  XSetForeground(display, gc, state_to_color(current->vstate()));
	  
	  int y     = yloc[j];
	  int state = current->vstate();

	  if (ChangingLanes(state) || current->lane_change())
	    {
	      switch (j) 
		{
		case 0: y += current->lane_change(); break;
		case 1: y -= current->lane_change(); break;
		case 2: y -= current->lane_change(); break;
		case 3: y += current->lane_change(); break;
		}
	    }

	  //
	  // Tell the vehicle to draw itself.
	  //
	  int selected  = (current == popup_current);

	  current->draw(display, pix, gc, x, y, go_right, scale, xoff, yoff, 
			selected);
	}
    }
}


//
// Draw the whole roadway.
//
void
traffic_display()
{
  static   int  boxed = FALSE;
  
  //
  // If I'm not ready to draw, bail out.
  //
  if (!display || !drawable || !gc ) return;

  // Erase roadway by copying empty version from pix_1.
  XCopyArea(display, pix_1, pix_2, gc, 0, 0, width, lanes_height, 0, 0);
      
  draw_vehicles(pix_2, 1, 0, 0);
  
  XCopyArea(display, pix_2, drawable, gc, 0, 0, width, lanes_height, 0, 
	    lanes_top);

  if (popup_current) BoxPopupArea();

  XFlush(display);
}


//
// Get requested pixel value (color) by name.
//
static unsigned long
get_color(Display *dpy, Colormap cmap, char *name)
{
  XColor cdef;
  
  if (XParseColor(dpy, cmap, name, &cdef) &&
      XAllocColor(dpy, cmap, &cdef))
    return cdef.pixel;
  else
    return WHITE;
}


//
// Return color for a given state.
//
static unsigned long
state_to_color(int s)
{
  switch(s) 
    {
    case VSTATE_COAST:        return color_yellow;
    case VSTATE_BRAKE:        return color_red;   
    case VSTATE_ACCELERATE:   return color_green; 
    case VSTATE_CRASH:        return color_orange;
    case VSTATE_MAINTAIN:     return color_white; 
    case VSTATE_MAX_SPEED:    return color_blue;  
    case VSTATE_CHANGE_LANE:
    case VSTATE_CHANGE_LEFT:
    case VSTATE_CHANGE_RIGHT: return color_violet;
    default:                  return color_black; // shouldn't happen
    }
}


//
// Set speed limit menus based on limits[] array.
//
static void
traffic_set_limits()
{
#define speed_to_set(s) ((55-(s)) / 10)
  
  xv_set(window_obj->speed1, PANEL_VALUE, speed_to_set(limits[0]), NULL);
  xv_set(window_obj->speed2, PANEL_VALUE, speed_to_set(limits[1]), NULL);
  xv_set(window_obj->speed3, PANEL_VALUE, speed_to_set(limits[2]), NULL);
  xv_set(window_obj->speed4, PANEL_VALUE, speed_to_set(limits[3]), NULL);
  xv_set(window_obj->speed5, PANEL_VALUE, speed_to_set(limits[4]), NULL);
}


//
// Draw colored box for legend.
//
static void
draw_color_box(int x, int y, unsigned long c)
{
  int l = x;
  int t = y - 14;
  int r = x + 14;
  int b = y;
  
  // Fill rectangle in color
  XSetForeground(display, gc, c);
  XFillRectangle(display, drawable, gc, l, t, (r - l), (b - t));
  
  // Draw border in black
  XSetForeground(display, gc, color_black);
  draw_box(drawable, gc, l, t, r, b);
}


static const int border = 20;

//
// Display a legend indicating which colors belong to which state
//
static void
display_legend()
{
  int y = height - border / 2;
  
  for (int i = VSTATE_MAINTAIN; i <= VSTATE_CHANGE_LANE; i++)
    {
      int           x = border + (i * width / (VSTATE_CHANGE_LANE + 1));
      unsigned long c = state_to_color(i);
      
      draw_color_box(x, y, c);
      
      XSetForeground(display, gc, color_white);
      XDrawString(display, drawable, gc, x + border, y,
		  state_names[i], strlen(state_names[i]));
    }
}


//
// Mark the different speed zones above the roadway.
//
static void
display_zones()
{
  int y = border;
  
  for (int i = 0; i < 5; i++)
    {
      char buf[20];
      int x_line = i * PIX_PER_SIGN;
      int x_text = x_line + border;
      
      XSetForeground(display, gc, (pixel_depth > 1)? color_white : color_black);
      if (i) XDrawLine(display, drawable, gc, 
		       x_line, y - 15,
		       x_line, y +  5);
      
      sprintf(buf, "Speed Zone %d", i+1);
      XDrawString(display, drawable, gc, x_text, y, buf, strlen(buf));
    }
}


//
// Display the turn sign.
//
static void
display_signs()
{
#include "arrow_up.bitmap"
#include "arrow_over.bitmap"
#include "arrow_right.bitmap"
#include "arrow_left.bitmap"

  static Pixmap pix_over  = NULL;
  static Pixmap pix_up    = NULL;
  static Pixmap pix_right = NULL;
  static Pixmap pix_left  = NULL;

  int w = arrow_over_width;
  int h = arrow_over_height;
  int l = border / 4;
  int r = width - w - border / 4;
  int t = lanes_top - h - border / 4;
  int b = lanes_top + lanes_height + border / 4;

  if (!pix_over)
    pix_over = XCreatePixmapFromBitmapData
      (display, drawable, (char *)arrow_over_bits, w, h,
       (pixel_depth > 1) ? color_white : color_black, color_cadet, 
        pixel_depth);
  if (!pix_up)
    pix_up = XCreatePixmapFromBitmapData
      (display, drawable, (char *)arrow_up_bits, w, h,
       (pixel_depth > 1) ? color_white : color_black, color_cadet, 
        pixel_depth);
  if (!pix_right)
    pix_right = XCreatePixmapFromBitmapData
      (display, drawable, (char *)arrow_right_bits, w, h,
       (pixel_depth > 1) ? color_white : color_black, color_cadet, 
        pixel_depth);
  if (!pix_left)
    pix_left = XCreatePixmapFromBitmapData
      (display, drawable, (char *)arrow_left_bits, w, h,
       (pixel_depth > 1) ? color_white : color_black, color_cadet, 
        pixel_depth);

  XCopyArea(display, pix_up,    drawable, gc, 0, 0, w, h, r, b);
  XCopyArea(display, pix_over,  drawable, gc, 0, 0, w, h, r, t);
  XCopyArea(display, pix_left,  drawable, gc, 0, 0, w, h, l, t);
  XCopyArea(display, pix_right, drawable, gc, 0, 0, w, h, l, b);
}


//
// Get graphics context, color map, etc.
//
static void
setup_graphics(Display *dpy, Window xwin)
{
  XWindowAttributes window_attributes;
  Colormap cmap;
  
  // get cmap
  XGetWindowAttributes(dpy, xwin, &window_attributes);
  cmap = window_attributes.colormap;
  
  // make gc
  gc   = XCreateGC(dpy, xwin, 0, NULL);
  
  // get some colors
  color_green  = get_color(dpy, cmap, "green");
  color_red    = get_color(dpy, cmap, "red");
  color_yellow = get_color(dpy, cmap, "yellow");
  color_orange = get_color(dpy, cmap, "orange");
  color_blue   = get_color(dpy, cmap, "light blue");
  color_violet = get_color(dpy, cmap, "violet");
  color_cadet  = get_color(dpy, cmap, "Cadet Blue");
  color_white  = get_color(dpy, cmap, "white");
  color_black  = get_color(dpy, cmap, "black");
  if (color_black == WHITE) color_black = BLACK; // shouldn't happen
  
  pix_1     = XCreatePixmap(dpy, xwin, width, lanes_height, pixel_depth);
  pix_2     = XCreatePixmap(dpy, xwin, width, lanes_height, pixel_depth);
  pop_pix   = XCreatePixmap(dpy, xwin, width, lanes_height * 3, pixel_depth);
}


//
// Draws black road with yellow and white lines for roadway.
//
void
draw_street(Pixmap pix, int scale, int xoff)
{
  int mid = lanes_height / 2;

  // Draw street in black
  XSetForeground(display, gc, color_black);
  XFillRectangle(display, pix, gc, 0, 0, width * scale, lanes_height * scale);
  
  // Draw lane lines -- center divider
  XSetForeground(display, gc, color_yellow);
  XFillRectangle(display, pix, gc, 0, mid * scale, width * scale, scale);
  
  // Draw lane lines -- dashed lanes *** SWITCH TO XDRAWSEGMENTS HERE
  XSetForeground(display, gc, color_white);
  for (int l = 1; l < LANES_EACH_DIR; l++)
    {
      int yoff  = l * LANE_WIDTH;
      int yup   = (mid - yoff) * scale;
      int ydown = (mid + yoff) * scale;
      for (int i = 0; i < width; i += DASH_PERIOD) 
	{
	  XFillRectangle(display, pix, gc, i * scale + xoff, yup,   
			 DASH_LENGTH * scale, scale);
	  XFillRectangle(display, pix, gc, i * scale + xoff, ydown, 
			 DASH_LENGTH * scale, scale);
	}
    }
  
  // Draw lane lines -- edges
  int bot = (lanes_height - 1) * scale;
  XFillRectangle(display, pix, gc, 0, 0,   width * scale, scale);
  XFillRectangle(display, pix, gc, 0, bot, width * scale, scale);
}


//
// Repaint the canvas.  Called for expose events, like when the windows are
// first created.
//
void
traffic_repaint(Canvas, Xv_Window paint_window, 
		Display *dpy, Window xwin, Xv_xrectlist *)
{
  // Remember stuff for drawing later
  width          = (int) xv_get(paint_window, XV_WIDTH);
  height         = (int) xv_get(paint_window, XV_HEIGHT);
  middle         = height / 2;
  width_in_miles = (double) width / (double) PIX_PER_MILE;

  lanes_top    = middle - LANE_WIDTH * LANES_EACH_DIR;
  lanes_height = 2 * LANE_WIDTH * LANES_EACH_DIR + 1;

  yloc[0]   = middle + ((LANE_WIDTH-CAR_WIDTH) / 2) - lanes_top;
  yloc[1]   = middle - ((LANE_WIDTH+CAR_WIDTH) / 2) - lanes_top;
  yloc[2]   = yloc[0] + LANE_WIDTH;
  yloc[3]   = yloc[1] - LANE_WIDTH;
  box_t     = lanes_top + 1;
  box_b     = lanes_top + lanes_height - 3;
  display   = dpy;
  drawable  = xwin;

  pixel_depth = (int)xv_get (paint_window, WIN_DEPTH);

  if (!gc) setup_graphics(dpy, xwin);

  draw_street(pix_1, 1, 0);

  if (pixel_depth > 1)
    display_legend();

  display_zones();

  display_signs();
  
  traffic_display();
}


//
// Repaint the popup canvas.  Doesn't actually draw yet.  Just remember stuff.
//
void
traffic_popup_repaint(Canvas, Xv_Window paint_window, 
		      Display *, Window xwin, Xv_xrectlist *)
{
  if (!gc) return;
  
  // Remember stuff for drawing later
  popup_xwin   = xwin;
  popup_width  = (int) xv_get(paint_window, XV_WIDTH);
  popup_height = (int) xv_get(paint_window, XV_HEIGHT);
  popup_top    = (popup_height / 2) - LANE_WIDTH * LANES_EACH_DIR * 3;
}


//
// Draw vehicles on popup.  They are magnified (x3) from the regular roadway.
//
void
traffic_popup_display(int xoff)
{
  //
  // If I'm not ready to draw, bail out.
  //
  if (!popup_xwin) return;

  // Erase roadway.
  XSetForeground(display, gc, color_black);
  XFillRectangle(display, pop_pix, gc, 0, 0, popup_width, lanes_height * 3);

  // Draw vehicles over street lines.
  int xadj = (xoff - popup_current->xloc()) * 3 + popup_width / 2;
  draw_street  (pop_pix, 3, xadj);
  draw_vehicles(pop_pix, 3, xadj, 0);

  // Erase parts of roadway which are off edge of screen
  int xleft      = popup_current->xloc() - popup_width / 6 - xoff;
  int xright     = popup_current->xloc() + popup_width / 6 - xoff;
  int left_edge  = (xleft  < 0)     ? -3 * xleft : 0;
  int right_edge = (xright > width) ? 
    popup_width - 3 * (xright - width) : popup_width;
  int show_width = right_edge - left_edge;
  if (left_edge) XClearArea(display, popup_xwin, 0, 0, left_edge, 0, 0);
  else           XClearArea(display, popup_xwin, right_edge, 0, 0, 0, 0);

  // Copy to the display
  XCopyArea(display, pop_pix, popup_xwin, gc, left_edge, 0, show_width, 
	    lanes_height * 3, left_edge, popup_top);

  XFlush(display);
}


//
// Handle mouse events in the canvas.  It was probably the user selecting a car
//
void
traffic_mouse_event(Xv_window xvwin, Event *event, Notify_arg)
{
  //
  // If this is not a "mouse selection", bail out.
  //
  if (event_action(event) != ACTION_SELECT || !event_is_down(event)) return;

  //
  // See if this mouse click happened in the main window or in the popup
  //
  int in_popup   = (xvwin == canvas_paint_window(popup_obj->canvas2));
  int scale      = in_popup ? 3 : 1;
  int lane_match = -1;

  //
  // Try to figure out which vehicle was selected.
  //
  for (int j = 0; j < N_LANES; j++)
    {
      int y;

      if (in_popup) y = yloc[j] * scale + popup_top;
      else          y = yloc[j] + lanes_top;

      // If y matches for a particular lane...
      if (is_between(event->ie_locy, 
		     y - 4*scale, y + (CAR_WIDTH+4)*scale))
	{
	  lane_match = j;
	  break;
	}
    }

  if (lane_match == -1) return;	// mouse click was not in traffic

  vehicle *closest  = NULL;
  int      distance = 2000;

  // 
  // Find the vehicle in this lane which is closest to the mouse click.
  //
  for (list *i = lanes[lane_match]->first(); i->hasValue(); i = i->next())
    { 
      vehicle *current = i->value();
      int            x = current->xloc();
      
      if (in_popup)  x = scale * (x - popup_current->xloc() + CAR_LENGTH/2) 
	                 + popup_width/2;

      int dx = abs(event->ie_locx - x);

      if (distance > dx) 
	{
	  distance = dx;
	  closest  = current;
	}
    }

  if (closest == NULL) return;	// nobody nearby

  if (in_popup)	// scroll the popup to center the selection
    {
      traffic_popup_scroll(closest);
    }
  else		// display the popup with the selected vehicle
    {
      if (popup_current)
	{
	  if (abs(popup_current->xloc() - closest->xloc()) < popup_width / 6)
	    traffic_popup_scroll(closest);
	  else
	    traffic_popup(closest);
	}
      else 
	{
	  was_running = running;
	  traffic_stop();
	  traffic_popup(closest);
	}
    }
}


//
// Called by traffic_init() and traffic_reset().  Set default values
// for buttons, sliders, etc.
//
void
traffic_default_settings()
{
  // gap between new cars slider
  xv_set(window_obj->gap,  PANEL_VALUE, 20, NULL);
  traffic_gap(20);
  
  // time between updates slider
  xv_set(window_obj->time, PANEL_VALUE, 40, NULL);
  traffic_time(40);
  
  // randomize
  randomize = TRUE;
  xv_set(window_obj->choice_randomize, PANEL_VALUE, randomize, NULL);
  
  // speed limits
  for (int i = 0; i < 5; i++) limits[i] = 55;
  traffic_set_limits();
}


//
// Called when application first starts from modified window_stubs.C
// Gives us an opportunity to jot down some global variables for window stuff.
//
void 
traffic_init(int argc, char **argv, 
	     window_traffic_objects     *window_traffic,
	     window_popup1_objects      *window_popup1,
	     window_popup_name_objects  *window_name,
	     window_popup_story_objects *window_story)
{
  // Initialize some local variables
  for (int i = 0; i < N_LANES; i++)
    lanes[i] = new list();
  
  // Remember window stuff so we can use it later.
  popup_obj  = window_popup1;
  window_obj = window_traffic;
  story_obj  = window_story;
  name_obj   = window_name;
  nclient    = window_obj->traffic;
  
  // Set up traffic_repaint() to be called whenever window is exposed.
  xv_set(window_obj->canvas1,
	 CANVAS_REPAINT_PROC,   traffic_repaint,
	 CANVAS_X_PAINT_WINDOW, TRUE,
	 NULL);
  
  // Set up traffic_mouse_event() to intercept events for mouse clicks.
  xv_set(canvas_paint_window(window_obj->canvas1),
	 WIN_EVENT_PROC,     traffic_mouse_event,
	 WIN_CONSUME_EVENTS, WIN_MOUSE_BUTTONS, NULL,
	 NULL);
  xv_set(canvas_paint_window(popup_obj->canvas2),
	 WIN_EVENT_PROC,     traffic_mouse_event,
	 WIN_CONSUME_EVENTS, WIN_MOUSE_BUTTONS, NULL,
	 NULL);
  
  // Set up traffic_popup_repaint() to be called whenever popup is exposed.
  xv_set(popup_obj->canvas2,
	 CANVAS_REPAINT_PROC,   traffic_popup_repaint,
	 CANVAS_X_PAINT_WINDOW, TRUE,
	 NULL);
  
  // Set the directory in the file popup to be current working directory
  char *current_dir = getenv("PWD");
  if (!current_dir) 
        current_dir = getenv("HOME");
  if (current_dir)
    xv_set(name_obj->textfield_directory, PANEL_VALUE, current_dir,    NULL);
  xv_set(name_obj->textfield_filename,    PANEL_VALUE, "traffic.save", NULL);

  // Position popups so that they don't appear over application
  xv_set(popup_obj->popup1,      XV_X,  10, XV_Y, 330, NULL);
  xv_set(story_obj->popup_story, XV_X, 575, XV_Y, 330, NULL);
  xv_set( name_obj->popup_name,  XV_X,  50, XV_Y,  70, NULL);

  // Set textfields that deal with velocities to jump 5 MPH per mouse click.
  // (This doesn't seem to have the desired effect; so it's commented out.)
  // xv_set(popup_obj->textfield_velocity,  PANEL_JUMP_DELTA, 5, NULL);
  // xv_set(popup_obj->textfield_max_speed, PANEL_JUMP_DELTA, 5, NULL);

  // Put story in story popup
  xv_set(story_obj->textpane1, TEXTSW_FILE, "story.text", NULL);

  // Set speeds, buttons, etc to default.
  traffic_default_settings();

  // Reset randomness for reproducibility.
  /* srandom(1); */

  // Reset clock to time of big bang.
  clock_advance(FALSE);

  // Show status in frame
  traffic_frame();

  // Parse command line.
  for (i = 1; i < argc; i++)
    if (argv[i][0] != '-')
      {
	filename = argv[i];	// name of file to load
      }
    else if (strcmp(argv[i], "-q") == 0)
      {
	sound_off();		// run quiet
      }

  // If a filename was on the command line, load that file.
  if (filename) traffic_do_load();
}


//////////////////////////////////////////////////////////////////////
//                                                                  //
//                                                                  //
//           FUNCTIONS CALLED IN RESPONSE TO USER ACTIONS           //
//                                                                  //
//                                                                  //
//////////////////////////////////////////////////////////////////////


//
// Called when user moves "gap" slider.  "gap" arg ranges from 0 to 100.
//
void
traffic_gap(int gap)
{
  generate_gap = (double) (20.0 + 4.0 * gap) / 5280;
}


//
// Called when user moves "time" slider.  "time" arg ranges from 0 to 100.
//
void
traffic_time(int time)
{
  double frac = (double) time / 100.0;

  // Set update time to range from 1/20 to 19/20 second by a quadratic scale
  update_time  = (int) ((double) (ONE_SEC - TENTH_SEC) * (frac * frac));
  update_time += TWENTIETH_SEC;

  if (running)  // Set up new timer with correct update time
    {
      traffic_stop();
      traffic_start();
    }
}


//
// Called when user clicks mouse button on a particular vehicle.
// Causes "vehicle info" popup to appear.
//
void
traffic_popup(vehicle *v)
{
  popup_current = v;

  traffic_set_popup_values(v);

  xv_set(popup_obj->popup1, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
  xv_set(popup_obj->popup1, XV_SHOW, TRUE, NULL);
  
  traffic_display();		// puts dot in current vehicle on main

  traffic_popup_display(0);
  
  traffic_frame();		// indicate state "suspended" if appropriate
}


//
// Scroll popup until given vehicle is centered and selected.
// This function was called in response to a mouse click by the user.
//
void
traffic_popup_scroll(vehicle *v)
{
  int dx  = v->xloc() - popup_current->xloc();
  int dir = (dx > 0) ? -1 : 1;

  popup_current = v;

  traffic_set_popup_values(v);

  traffic_display();		// puts dot in current vehicle on main


#define round_to(m, i) ( ((int) ((i) / (m))) * m )

  const int step = 5;

  // Scroll to the correct place such that the current vehicle is centered
  for (int i = round_to(step, dx); (i*dir) <= 0; i += (dir * step))
    {
      traffic_popup_display(i);
    }
}


//
// Called to put values from vehicle into popup
//
void
traffic_set_popup_values(vehicle *v)
{
  static char buffer[20];
  
  if (!v) return;
  
  xv_set(popup_obj->choice_state, PANEL_VALUE, v->vstate(), NULL);
  
  xv_set(popup_obj->choice_class, PANEL_VALUE, v->classnum(), NULL);
  
  sprintf(buffer, "%d", v->name());
  xv_set(popup_obj->textfield_vehicle, PANEL_VALUE, buffer, NULL);
  
  sprintf(buffer, "%d", (int) (v->pos() * 5280.0));
  xv_set(popup_obj->textfield_position, PANEL_VALUE, buffer, NULL);
  
  xv_set(popup_obj->textfield_velocity, PANEL_VALUE, (int) v->vel(), NULL);
  
  xv_set(popup_obj->textfield_max_speed, PANEL_VALUE, v->top_speed(), NULL);

  // Get gap in front and back
  list  *front     = lanes[v->lane()]->next(v);
  list  *back      = lanes[v->lane()]->prev(v);
  double front_gap = front->hasValue() ? front->value()->rear_pos()-v->pos():-1;
  double  back_gap = back ->hasValue() ? v->rear_pos()-back->value()->pos() :-1;

#define round(f) (int) ((f) + 0.5)

  if (front_gap < 0) sprintf(buffer, "(lead car)");
  else               sprintf(buffer, "%d", round(front_gap * 5280.0));
  xv_set(popup_obj->textfield_front_gap,  PANEL_VALUE, buffer, NULL);

  if (back_gap < 0)  sprintf(buffer, "(rear car)");
  else               sprintf(buffer, "%d", round(back_gap * 5280.0));
  xv_set(popup_obj->textfield_behind_gap, PANEL_VALUE, buffer, NULL);
}


//
// Called to put values from popup into vehicle
//
void
traffic_get_popup_values()
{
  traffic_state((int) xv_get(popup_obj->choice_state, PANEL_VALUE));
  
  traffic_class((int) xv_get(popup_obj->choice_class, PANEL_VALUE));
  
  traffic_position((char *)
		   xv_get(popup_obj->textfield_position, PANEL_VALUE));
  
  traffic_velocity((int) 
		   xv_get(popup_obj->textfield_velocity, PANEL_VALUE));

  traffic_max_speed((int) 
		   xv_get(popup_obj->textfield_max_speed, PANEL_VALUE));
}


//
// Called when user presses start button
//
void
traffic_start()
{
  running = 1;

  traffic_force_popup_done();

  // Start simulation timer (goes 5 times per sec)
  timer.it_value.tv_usec    = update_time; // INITIAL value for timer
  timer.it_value.tv_sec     = 0;
  timer.it_interval.tv_usec = update_time; // RELOAD value for timer
  timer.it_interval.tv_sec  = 0;
  notify_set_itimer_func(nclient, 
			 (Notify_func) traffic_simulate, 
			 ITIMER_REAL, 
			 &timer, NULL);		// vol. 7 p. 372

  traffic_frame();		// update frame with file name and status
}


//
// Called when user presses stop button
//
void
traffic_stop()
{
  // Turn off timer
  notify_set_itimer_func(nclient, NOTIFY_FUNC_NULL, ITIMER_REAL, NULL, NULL);
  
  running = 0;

  traffic_frame();		// update frame with file name and status
}


//
// Called when user presses reset button
//
void
traffic_reset()
{
  traffic_force_popup_done();

  traffic_stop();

  traffic_default_settings();

  //
  // Remove existing vehicles
  //
  for (int j = 0; j < N_LANES; j++)
    {
      while (!lanes[j]->isEmpty()) 
	{
	  vehicle *v = lanes[j]->first()->value();
	  lanes[j]->remove(v);
	  delete v;
	}
    }
  name_counter = 0;		// start counting vehicles at 0 again

  /* srandom(1); */		// reset randomness for reproducibility

  traffic_display();		// display empty lanes

  traffic_stats(FALSE);		// erase averages velocities

  clock_advance(FALSE);		// reset clock to time of big bang
}


//
// Called when user chooses "Clear Wrecks" menu item.
//
void
traffic_clear()
{
  traffic_stop();

  for (int j = 0; j < N_LANES; j++)
    {
      list *next = NULL;

      for (list *i = lanes[j]->first(); i->hasValue(); i = next)
	{ 
	  vehicle *current;
	  
	  current = i->value();
	  next    = i->next(); 

	  if (current->vstate() == VSTATE_CRASH) lanes[j]->remove(current);
	}
    }
  traffic_display();		// display lanes without wrecked cars

  traffic_stats(FALSE);		// do statistics to erase averages velocities
}


//
// Called when user presses story button
//
void
traffic_story()
{
  xv_set(story_obj->popup_story, XV_SHOW, TRUE, NULL);
}


//
// Called when user chooses "load" from file menu
//
void
traffic_load()
{
  will_do_load = TRUE;

  xv_set(name_obj->button_save, PANEL_LABEL_STRING, " Load ", NULL);
  xv_set(name_obj->popup_name, XV_LABEL, "Name of File to Load", NULL);
  xv_set(name_obj->popup_name, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
  xv_set(name_obj->popup_name, XV_SHOW, TRUE, NULL);
}


//
// Called when user chooses "load" from filename popup
//
void
traffic_do_load()
{
  if (!filename) 
    {				// do notice here
      return;
    }

  traffic_reset();

  ifstream mystream(filename, ios::in);

  // Get application state...
  for (int i = 0; i < 5; i++) mystream >> limits[i];    // limits
  traffic_set_limits();
  int gap, time;
  mystream >> gap;
  xv_set(window_obj->gap,  PANEL_VALUE, gap,  NULL);	// gap
  traffic_gap(gap);
  mystream >> time;
  xv_set(window_obj->time, PANEL_VALUE, time, NULL);	// time
  traffic_time(time);
  mystream >> randomize;				// randomize
  xv_set(window_obj->choice_randomize, PANEL_VALUE, randomize, NULL);
  mystream >> name_counter;

  vehicle *current = NULL;
  while (mystream.good())
    { 
      int classnum;

      mystream >> classnum;

      if (mystream.good()) 
	{
	  vehicle *v = new_vehicle(classnum, 0, 0, 0.0, 0.0);

	  mystream >> *v;

	  if (mystream.fail()) 
	    delete v;
	  else lanes[v->lane()]->append(v);
	}
    }

  mystream.close();

  traffic_frame();		// update frame with file name and status

  traffic_stats(FALSE);		// do statistics on vehicles in lanes
  
  traffic_display();		// display the vehicles in new states
}


//
// Called when user chooses "saveAs" from file menu.
//
void
traffic_saveAs()
{
  will_do_load = FALSE;

  xv_set(name_obj->button_save, PANEL_LABEL_STRING, " Save ", NULL);
  xv_set(name_obj->popup_name, XV_LABEL, "Name of File to Save", NULL);
  xv_set(name_obj->popup_name, FRAME_CMD_PUSHPIN_IN, TRUE, NULL);
  xv_set(name_obj->popup_name, XV_SHOW, TRUE, NULL);
}


//
// Called when user chooses "save" from file menu
//
void
traffic_do_save()
{
  if (!filename) 
    {				// do notice here
      return;
    }

  ofstream mystream(filename, ios::out);

  // Dump application state...
  for (int i = 0; i < 5; i++) mystream << limits[i] << "\t";       // limits
  mystream << (int) xv_get(window_obj->gap,  PANEL_VALUE) << "\t"; // gap
  mystream << (int) xv_get(window_obj->time, PANEL_VALUE) << "\t"; // time
  mystream << randomize << " " << name_counter << endl;		   // random,etc

  // Dump vehicle states...
  for (int j = 0; j < N_LANES; j++)
    {
      for (list *l = lanes[j]->first(); l->hasValue(); l = l->next())
	{ 
	  vehicle *current = l->value();
	  mystream << current->classnum() << "\t" << *current << endl;
	}
    }

  mystream.close();

  traffic_frame();		// update frame with file name and status
}


//
// Called when user chooses "save" from file menu
//
void
traffic_save()
{
  if (filename) traffic_do_save();
  else          traffic_saveAs();  // If filename is unspecified, show popup
}


//
// Called when user chooses "close" from file menu
//
void
traffic_close()
{
  filename = NULL;

  traffic_frame();		// update frame with file name and status
}


//
// Called when user changes a speed limit.
//
void
traffic_speed(int zone, int limit)
{
  limits[zone-1] = limit;
}


//
// Called when user presses save / load in file name popup
//
void
traffic_load_save()
{
  static char buff[200];

  // Get directory and filename from popup
  char *dir  = (char *) xv_get(name_obj->textfield_directory, PANEL_VALUE);
  char *file = (char *) xv_get(name_obj->textfield_filename,  PANEL_VALUE);

  // Construct full pathname
  if (dir[strlen(dir)-1] == '/') sprintf(buff, "%s%s",  dir, file);
  else                           sprintf(buff, "%s/%s", dir, file);
  filename = buff;

  // Do the right thing... either load or save
  if (will_do_load) traffic_do_load();
  else              traffic_do_save();

  // Hide the popup
  traffic_cancel();
}


//
// Called when user presses cancel in file name popup
//
void
traffic_cancel()
{
  xv_set(name_obj->popup_name, FRAME_CMD_PUSHPIN_IN, FALSE, NULL);
  xv_set(name_obj->popup_name, XV_SHOW, FALSE, NULL);
}


//
// Called when user changes max_speed in popup
//
void
traffic_max_speed(int m)
{
  popup_current->top_speed(m);
}


//
// Called when user presses step button in popup
//
void
traffic_step()
{
  traffic_simulate(NULL, 0);

  if (popup_current) // current may have been removed during _simulate()
    {
      traffic_popup_display(0);

      traffic_set_popup_values(popup_current);
    }
  else
    { // ????
    }
}


//
// Called when user presses remove button in popup
//
void
traffic_remove()
{
  int         result;

  result = notice_prompt(popup_obj->popup1, NULL,
//      NOTICE_FOCUS_XY,        event_x(event), event_y(event),
        NOTICE_MESSAGE_STRINGS, "Do you really want to remove this vehicle?",
			 NULL,
        NOTICE_BUTTON_YES,      "Yes",
        NOTICE_BUTTON_NO,       "No",
        NULL);
  
  if (result == NOTICE_YES) 
    {
      int lane = popup_current->lane();
      int oldx = popup_current->xloc(); 

      // find car to be showing next (either previous or next car in list)
      list               *l = lanes[lane]->prev(popup_current);
      if (!l->hasValue()) l = lanes[lane]->next(popup_current);

      // remove car from the list
      lanes[lane]->remove(popup_current);
      delete popup_current;
      
      // show next one or hide popup
      if (l->hasValue()) 
	{
	  vehicle *next = l->value();
	  if ( abs(oldx - next->xloc()) < popup_width / 6)
	    traffic_popup_scroll(next);
	  else
	    traffic_popup(next);
	}
      else 
	{
	  traffic_force_popup_done();
	}
    }
}


//
// Force popup to hide.  (idempotent)
//
void
traffic_force_popup_done()
{
  if (popup_current) 
    {
      xv_set(popup_obj->popup1, FRAME_CMD_PUSHPIN_IN, FALSE, NULL);
      xv_set(popup_obj->popup1, XV_SHOW, FALSE, NULL);
      was_running = 0;		// so _popup_done() doesn't call start again
      traffic_popup_done();
    }
}


//
// Called when user removes pushpin on popup
//
void
traffic_popup_done()
{
  traffic_get_popup_values();

  popup_current = NULL;

  traffic_display();		// draw traffic without "current" vehicle

  if (was_running) traffic_start();
}


//
// Called when user removes pushpin on name popup
//
int
traffic_name_done()
{
  return TRUE;  // just let the popup go
}


//
// Called when user types in position textfield
//
void
traffic_position(char *text)
{
  double val;

  sscanf(text, "%lf", &val);

  val /= 5280.0;

  popup_current->pos(val);
}


//
// Called when user types in velocity textfield
//
void
traffic_velocity(int val)
{
  if (( (int) popup_current->vel() ) != val) // If it has changed...
    popup_current->vel((double) val);
}


//
// Called when user changes "state" choice item
//
void
traffic_state(int newstate)
{
  if (newstate == popup_current->vstate()) return;
  
  popup_current->vstate(newstate);
  
  popup_current->sound(newstate);

  traffic_popup_display(0);
  traffic_display();		// display on main window
}


//
// Called when user changes "class" choice item
//
void
traffic_class(int newclass)
{
  //
  // Don't do anything if it's the same class.
  //
  if (newclass == popup_current->classnum()) return;

  int    lanenum = popup_current->lane();
  double pos     = popup_current->pos();
  double vel     = popup_current->vel();

  // 
  // Create a new vehicle in the chosen class with the same characteristics
  // as the current vehicle.
  //
  vehicle *v = new_vehicle(newclass, name_counter++, lanenum, pos, vel);

  v->vstate     (popup_current->vstate());
  v->xloc       (popup_current->xloc());
  v->top_speed  (popup_current->top_speed());
  v->lane_change(popup_current->lane_change());

  //
  // Make the switch. 
  //
  list *lane = lanes[popup_current->lane()];
  lane->remove(popup_current);
  delete popup_current;
  popup_current = v;
  lane->insert(popup_current);
  traffic_set_popup_values(popup_current);
  traffic_popup_display(0);
  traffic_display();		// display on main window
}


//
// Called when user changes "randomize" choice item
//
void
traffic_randomize(int choice)
{
  randomize = choice;
}




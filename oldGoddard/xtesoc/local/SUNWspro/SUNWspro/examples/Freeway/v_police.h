#ifndef V_POLICE_H
#define V_POLICE_H

//
// v_police.H
//
// Header file for police class for "highway".
//
// Copyright 16 May 1994 by Sun Microsystems, Inc.
//

#include <stream.h>

#include "v_maniac.h"

#define CLASS_POLICE 4

class police : public maniac {
 protected:
  int flash_state;		// state of flashing lights
  
 public:
  police (int = 0, int = 0, double = 0.0, double = 0.0);

  virtual char   *classname()       { return "police"; }
  virtual int     classnum()        { return CLASS_POLICE; }
  virtual double  vehicle_length();
  virtual void    recalc_pos();
  virtual void    draw (Display *display, Drawable pix, GC gc, 
			int x, int y, int direction_right, int scale, 
			int xorg, int yorg, int selected);
};

#endif

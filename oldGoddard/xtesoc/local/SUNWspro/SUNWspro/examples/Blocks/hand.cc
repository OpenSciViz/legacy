#include "cfns.h"
#include "hand.h"

int hand_width;
int hand_height;

hand::hand(char* name, const point& pos)
{ nm = name; position = pos; grasping = 0; }
  
void hand::draw(Xv_opaque pw)
{ hand_width = world_factor + 3;
  hand_height = world_factor * 3 / 2;
  point cs = position.convert();
  draw_cylinder(pw, point(cs.x() - hand_width / 2 + 1,
			  cs.y() - hand_height / 5 - 1),
		 point(cs.x() + hand_width / 2 - 1, cs.y()));
  draw_cone(pw, point(cs.x() - hand_width / 2 + hand_width / 5,
		      cs.y() - hand_height / 5),
	    point(cs.x(), cs.y() - hand_height + 2),
	    point(cs.x() + hand_width / 2 - hand_width / 5,
		  cs.y() - hand_height / 5));
  draw_cylinder(pw, point(cs.x() - hand_width / 5, -2),
		point(cs.x() + hand_width / 5, cs.y() - hand_height / 2)); }

void hand::animate(point d)
{ point src = position.convert();
  point dst = d.convert();
  int s_x = src.x() - hand_width / 2,
      s_y = src.y() - hand_height,
      d_x = dst.x() - hand_width / 2,
      d_y = dst.y() - hand_height,
      x_dir = (d_x - s_x),
      wd = hand_width,
      ht = hand_height;
  if (grasping)
    { wd  = convert_dim(grasping->w()) + 1;
      ht += convert_dim(grasping->h());
      s_x = src.x() - wd / 2;
      d_x = dst.x() - wd / 2; }
  if (x_dir != 0)
    x_dir /= abs(x_dir);
  if ((s_x != d_x) || (s_y != d_y))
    { wmgr_top (frame);
      for (int i = s_y; i > 0; i--)
	pw_copy(pw, s_x, i, wd, ht, PIX_SRC, pw, s_x, i + 1);
      wmgr_top (frame);
      if (s_x != d_x)
	for (i = s_x; i != d_x; i += x_dir)
	  pw_copy(pw, i, 0, wd, ht, PIX_SRC, pw, i - x_dir, 0);
      wmgr_top (frame);
      for (i = 0; i < d_y; i++)
	pw_copy(pw, d_x, i + 1, wd, ht, PIX_SRC, pw, d_x, i); }

  /* Force X to apply all our changes to the display */
  XSync ((Display *) xv_get (pw, XV_DISPLAY), FALSE);
  pos() = d; }

void hand::grasp(block& object)
{ if (grasping != &object)
    { if ((&object)->supported_blocks() != 0)
	(&object)->clear_top();
      if (grasping != 0)
	get_rid_of(*grasping);
      cout << "Moving hand to pick up " << object.name() << " at location "
	<< object.top_location() << '.' << endl;
      animate(object.top_location());
      cout << "Grasping " << object.name() << '.' << endl;
      grasping = &object; }}

void hand::ungrasp(block& object)
{ if (object.support() != 0)
    { cout << "Releasing " << object.name() << '.' << endl;
      grasping = 0; }}

void hand::move(block& object, load_bearing_block& support)
{ load_bearing_block* old_support = object.support();
  if (old_support != 0)
    { cout << "Removing support relationship between " << object.name()
	<< " and " << old_support->name() << '.' << endl;
      object.set_support(0); }
  point new_place = support.get_space(object);
  cout << "Moving " << object.name() << " to top of " << support.name()
    << " at location " << new_place << '.' << endl;
  object.pos() = new_place;
  animate(object.top_location());
  cout << "Adding support relationship between " << object.name() << " and "
    << support.name() << '.' << endl;
  object.set_support(&support); }

ostream& operator<<(ostream& o, hand& h)
{ o << "#<HAND " << h.name() << ' ' << h.position;
  if (h.grasping != 0)
    o << " grasping " << h.grasping->name();
  return (o << '>'); }


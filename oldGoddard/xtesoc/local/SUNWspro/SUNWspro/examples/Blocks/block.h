#ifndef BLOCK_H
#define BLOCK_H

#include <xview/xview.h>
#include <xview/panel.h>

#include "point.h"

const int block_count = 8;

class list;
class load_bearing_block;

class block {
 protected:
  char* nm;
  int movable;
  int width, height;
  point position;
  load_bearing_block* supported_by;
  Panel_item panel_item;
 public:
  block();
  block(char*, int, int, const point&, load_bearing_block*);
  virtual char* type() { return "BLOCK"; }
  char* name() { return nm; }
  int is_movable() { return movable; }
  int w()      { return width; }
  int h()      { return height; }
  point& pos() { return position; }
  int& x()   { return position.x(); }
  int& y()   { return position.y(); }
  point convert(int x_offset = 0, int y_offset = 0)
    { return position.convert(x_offset, y_offset); }
  Panel_item& button() { return panel_item; }
  point top_location();
  load_bearing_block* support() { return supported_by; }
  void set_support(load_bearing_block*);
  virtual int is_load_bearing() { return 0; }
  virtual list* supported_blocks() { return (list*)0; }
  virtual void draw(Xv_opaque);
  virtual void print_supported_blocks() { cout << name() << ':' << endl; }
  virtual void clear_top() {}
  virtual void put_on(block&); };

int operator==(block&, block&);
inline int operator!=(block& a, block& b) { return !(a == b); }
ostream& operator<<(ostream& o, block& b);

class load_bearing_block : public block {
 protected:
  list* support_for;
 public:
  load_bearing_block();
  load_bearing_block(char*, int, int, const point&, load_bearing_block*);
  int is_load_bearing() { return 1; }
  list* supported_blocks();
  void add_supported_block(block&);
  void remove_supported_block(block&);
  void print_supported_blocks();
  void clear_top();
  void put_on(block&);
  point get_space(block&);
  point find_space(block&);
  point make_space(block&); };

void get_rid_of(block&);

class table : public load_bearing_block {
 public:
  table(char*, int, int, const point&);
  char* type() { return "TABLE"; }
  void draw(Xv_opaque); };

class brick : public load_bearing_block {
 public:
  brick(char*, int, int, const point&, load_bearing_block*);
  char* type() { return "BRICK"; }
  void draw(Xv_opaque); };

class wedge : public block {
 public:
  wedge(char*, int, int, const point&, load_bearing_block*);
  char* type() { return "WEDGE"; }
  void draw(Xv_opaque); };

class ball : public block {
 public:
  ball(char*, int, int, const point&, load_bearing_block*);
  char* type() { return "BALL"; }
  void draw(Xv_opaque); };

extern table* the_table;
extern block* blocks[];

#endif

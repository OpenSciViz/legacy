/* Note: be careful not to insert or remove lines before the definition 
   of repaint_proc without adjusting the line number on the "stop at"
   statement in file D.in */

#include <unistd.h>
#include <string.h>

#include "cfns.h"
#include "point.h"
#include "block.h"
#include "windows.h"
#include "hand.h"

Display *display;
Frame frame;
Panel buttons;
Xv_opaque auto_button;
Xv_opaque world;
Xv_opaque pw;
Xv_font block_font;
GC gc;
unsigned long *colors;

int fg_color = color_ct - 1;
int world_factor = 20;
int button_width = 0;
int monochrome = FALSE;
int label_blocks = TRUE;

int in_auto_mode = FALSE;

char frame_footer_msg[80];

Xv_singlecolor world_colors[color_ct];

unsigned short icon_bits[] = {
#include "Blocks.icon"
};

unsigned short mask_bits[] = {
#include "Blocks.mask"
};

void button_proc(Panel_item);
void resize_proc(Xv_opaque, int, int);
void auto_proc(), auto_mode(), quit_proc(), destroy_proc();

Notify_value event_filter (Xv_opaque, Event*, Notify_arg, Notify_event_type);
Notify_value stop_proc();

static int call_count = 0;

void repaint_proc(Xv_opaque, Xv_opaque pw, Rectlist*)
{ if (call_count < 3) {
    call_count++;
    return; }
  for (int i = 0; i <= block_count; i++)
    blocks[i] -> draw (pw);
  the_hand -> draw (pw); }

block* find_block(Panel_item button)
{ for (int i = 0; i <= block_count; i++)
    if (button == blocks[i]->button())
      return blocks[i];
  return 0; }

inline int min (int x, int y)
{ return (x < y) ? x : y; }

inline int max (int x, int y)
{ return (x > y) ? x : y; }

void move_frame_onto_screen (Frame frame)
{ Xv_opaque screen = xv_get (frame, XV_OWNER);
  int current_x = int(xv_get (frame,  XV_X)),
      current_y = int(xv_get (frame,  XV_Y)),
      new_x = max (0, min (current_x, int(xv_get (screen, XV_WIDTH)) -
			   int(xv_get (frame,  XV_WIDTH)) - 5)),
      new_y = max (0, min (current_y, int(xv_get (screen, XV_HEIGHT)) -
			   int(xv_get (frame,  XV_HEIGHT)) - 20));
		      
  xv_set (frame, XV_X, new_x, XV_Y, new_y, NULL);
  XSync (display, TRUE);
  }


void make_window(int argc, char **argv, char **)
{ 
  /* Disables X grabs.  If you're in auto mode and accidentally click on
     a button, you won't be able to get any response from any window. */
  fullscreendebug = TRUE;
  

  xv_init (XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);

  char *program_name = argv[0];
  if (strrchr(program_name, '/'))
    program_name = strrchr(program_name, '/') + 1;

  for (int arg = 1; arg < argc; arg++)
    if (!strcmp (argv[arg], "-m"))
      monochrome = !monochrome;
    else
    if (!strcmp (argv[arg], "-l"))
      label_blocks = !label_blocks;

  frame_footer_msg[0] = '\0';

  frame = xv_create (NULL, FRAME,
		     XV_LABEL, "Blocks World",
		     FRAME_ARGC_PTR_ARGV, &argc, argv,
		     FRAME_LEFT_FOOTER, frame_footer_msg,
		     FRAME_SHOW_FOOTER, TRUE,
		     WIN_ERROR_MSG, "Error creating window frame.", 0);

  xv_set (frame, WIN_CONSUME_EVENTS, WIN_MAP_NOTIFY, NULL, NULL);

  Server_image icon_image
    = (Server_image) xv_create (NULL, SERVER_IMAGE,
				XV_WIDTH, 64,
				XV_HEIGHT, 64,
				SERVER_IMAGE_BITS, icon_bits,
				NULL);
  
  Server_image mask_image
    = (Server_image) xv_create (NULL, SERVER_IMAGE,
				XV_WIDTH, 64,
				XV_HEIGHT, 64,
				SERVER_IMAGE_BITS, mask_bits,
				NULL);
  
  world_factor = (int)xv_get (xv_pf_default(), FONT_SIZE) * 5 / 3;

  buttons = xv_create
    (frame, PANEL,
     PANEL_LAYOUT, PANEL_VERTICAL,
     WIN_RETAINED, TRUE,
     PANEL_ITEM_Y_GAP, 5,
     PANEL_ITEM_X_GAP, 5,
     XV_HEIGHT, world_height * world_factor + 2 * world_margin, NULL);

  button_width = (int)xv_get (xv_pf_default(), FONT_SIZE) * 4;

  for (int i = 0; i <= block_count; i++)
    { char* name = blocks[i]->name();
      Panel_item button =
	xv_create
	  (buttons, PANEL_BUTTON,
	   PANEL_NOTIFY_PROC, button_proc,
	   PANEL_LABEL_STRING, name,
	   PANEL_LABEL_WIDTH, button_width,
	   XV_X, xv_col(buttons, 1),
	   XV_Y, xv_col(buttons, 1) + xv_row(buttons, i == 0 ?
					     block_count : i - 1),
	   NULL);

      blocks[i]->button() = button; }

  auto_button =
    xv_create (buttons, PANEL_BUTTON,
	       PANEL_NOTIFY_PROC, auto_proc,
	       PANEL_LABEL_STRING, "Auto",
	       PANEL_LABEL_WIDTH, button_width,
	       XV_X, xv_col(buttons, 1),
	       XV_Y, xv_col(buttons, 3) + xv_row(buttons, block_count + 1),
	       NULL);

  xv_create (buttons, PANEL_BUTTON,
	     PANEL_NOTIFY_PROC, quit_proc,
	     PANEL_LABEL_STRING, "Quit",
	     PANEL_LABEL_WIDTH, button_width,	
	     XV_X, xv_col(buttons, 1),
	     XV_Y, xv_col(buttons, 3) + xv_row(buttons, block_count + 2),
	     NULL);

  xv_set (buttons, XV_WIDTH, xv_col(buttons, 1) * 5 / 2
	  + (int)xv_get (auto_button, XV_WIDTH), NULL);

  world_colors[bg_color].red   = 255;
  world_colors[bg_color].green = 245;
  world_colors[bg_color].blue  = 245;

  world_colors[fg_color].red   =   0;
  world_colors[fg_color].green =   0;
  world_colors[fg_color].blue  =   0;

  for (i = 1; i < color_ct - 1; i++)
    world_colors[i].blue =
      world_colors[i].green =
	32 + (world_colors[i].red = 32 + i * 3);

  Cms cms = xv_create (XV_NULL, CMS,
		       CMS_NAME, "Blocks Colors",
		       CMS_SIZE, color_ct,
		       CMS_COLORS, world_colors,
		       NULL); 

  world = xv_create
    (frame, CANVAS,
     XV_WIDTH,  world_width  * world_factor + 2 * world_margin,
     XV_HEIGHT, world_height * world_factor + 2 * world_margin,
     WIN_DYNAMIC_VISUAL, TRUE,
     WIN_RIGHT_OF, buttons,
     WIN_Y, 0,
     WIN_CMS, cms,
     CANVAS_RETAINED, TRUE,
     CANVAS_FIXED_IMAGE, FALSE,
     CANVAS_REPAINT_PROC, repaint_proc,
     CANVAS_RESIZE_PROC,  resize_proc, NULL);

  Icon icon = (Icon) xv_create (frame, ICON,
				ICON_IMAGE,      icon_image,
				ICON_MASK_IMAGE, mask_image,
				ICON_TRANSPARENT, TRUE,
				ICON_TRANSPARENT_LABEL, program_name,
				NULL);

  xv_set (frame, FRAME_ICON, icon, NULL);
  
  window_fit (frame);

  pw = canvas_paint_window (world);
  colors = (unsigned long *) xv_get (pw, WIN_X_COLOR_INDICES);
  display = (Display *)xv_get (pw, XV_DISPLAY);

  if (xv_get (pw, WIN_DEPTH) == 1)
    monochrome = TRUE;

  if (monochrome)
    fg_color = 1;

  notify_interpose_event_func (world, (Notify_func)event_filter, NOTIFY_SAFE);
  notify_set_signal_func(frame, (Notify_func)stop_proc, SIGINT, NOTIFY_ASYNC);
  notify_set_destroy_func(frame, (Notify_func)destroy_proc);

  block_font =
    (Xv_font)xv_find(frame, FONT,
		     FONT_STYLE, FONT_STYLE_BOLD,
		     FONT_SCALE, xv_get(xv_pf_default(), FONT_SCALE),
		     NULL);
  if (!block_font)
    { cout << "Can't get the requested font" << endl; 
      block_font = xv_pf_default(); }

  XGCValues gc_val;
  gc_val.font = (Font) xv_get (block_font, XV_XID);
  gc_val.foreground = colors[0];
  gc_val.arc_mode = ArcPieSlice;
  Display *display  = (Display*) xv_get (pw, XV_DISPLAY);
  Drawable drawable = (Drawable) xv_get (pw, XV_XID);
  gc = XCreateGC (display, drawable, GCForeground | GCFont | GCArcMode ,
		  &gc_val);

  xv_main_loop (frame);
  while (TRUE)
    { if (in_auto_mode)
	auto_mode(); 

      notify_start (); }
}

static block* first_block = 0;

void button_proc(Panel_item button)
{ block* the_block = find_block(button);
  if (first_block)
    { Panel_item first_button = first_block->button();
      strcat (frame_footer_msg, the_block->name());
      strcat (frame_footer_msg, ".");
      xv_set (frame, FRAME_LEFT_FOOTER, frame_footer_msg, NULL);
      xv_set (frame, FRAME_BUSY, TRUE, NULL);
      move_frame_onto_screen (frame);  
      cout << endl << frame_footer_msg << endl;
      the_block->put_on (*first_block);      
      xv_set (frame, FRAME_BUSY, FALSE, NULL);
      frame_footer_msg[0] = '\0';
      xv_set (frame, FRAME_LEFT_FOOTER, frame_footer_msg, NULL);
      first_block = 0; }
  else
    { strcpy (frame_footer_msg, "Move block ");
      strcat (frame_footer_msg, the_block->name());
      strcat (frame_footer_msg, " on top of block ");
      xv_set (frame, FRAME_LEFT_FOOTER, frame_footer_msg, NULL);
      first_block = the_block; }}

void resize_proc(Xv_opaque, int width, int height)
{ int w_fac = (width  - world_margin) / world_width,
      h_fac = (height - world_margin) / world_height;
  Xv_font font;
  world_factor = (w_fac < h_fac) ? w_fac : h_fac; 
  font =
    (Xv_font)xv_find(frame, FONT,
		     FONT_STYLE, FONT_STYLE_BOLD,
		     FONT_SIZE, world_factor * 3 / 5,
		     NULL);
  if (font != NULL)
    block_font = font;
}

void auto_mode()
{ struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec = 250000;

  move_frame_onto_screen (frame);
  xv_set (frame, FRAME_RIGHT_FOOTER, "Auto Mode", NULL);

  while (TRUE)
    { int src = int((rand() >> 3) % block_count + 1),
	  dst = int((rand() >> 2) % (block_count + 1));
      if (src != dst &&
	  blocks[src] -> support() != blocks[dst] &&
	  blocks[dst] -> is_load_bearing())
	{ sprintf (frame_footer_msg, "Move block %s on top of block %s.",
		   blocks[src]->name(), blocks[dst]->name());	
	  xv_set (frame, FRAME_LEFT_FOOTER, frame_footer_msg, NULL);
	  cout << endl << frame_footer_msg << endl;
	  blocks[dst]->put_on (*blocks[src]);
	  frame_footer_msg[0] = '\0';
	  xv_set (frame, FRAME_LEFT_FOOTER, frame_footer_msg, NULL);
	  if (in_auto_mode)
	    { notify_dispatch();
	      if (in_auto_mode)
		for (int i = 0; i < 8; i++)
		  { select (0, NULL, NULL, NULL, &timeout);
		    notify_dispatch(); }}
	  if (!in_auto_mode)
	    { xv_set (frame, FRAME_RIGHT_FOOTER, "", NULL);
	      return; }}}}

void auto_proc()
{ in_auto_mode = !in_auto_mode;
  xv_set (auto_button,
	  PANEL_LABEL_STRING, in_auto_mode ? "Stop" : "Auto",
	  NULL);
  
  if (in_auto_mode)
    notify_stop();

  for (int i = 0; i <= block_count; i++)
    xv_set (blocks[i]->button(), PANEL_INACTIVE, in_auto_mode, NULL); 

  /* Flush incoming events, like extra presses on this button. */
  XSync (display, TRUE);

}


Notify_value event_filter (Xv_opaque window, Event *event, Notify_arg arg,
			   Notify_event_type type)
{ Notify_value value =
    notify_next_event_func (window, (Notify_event)event, arg, type);
  return value; }

Notify_value stop_proc()
{ in_auto_mode = FALSE;
  return NOTIFY_DONE; }

void destroy_proc()
{ exit(1); }

void quit_proc()
{ xv_set (frame, FRAME_NO_CONFIRM, TRUE, 0);
  xv_destroy_safe (frame);

  exit (1); }

#include <stream.h>
#include "block.h"
#include "hand.h"
#include "windows.h"

block* blocks[block_count + 1];
hand*  the_hand; 
table* the_table;

void main(int argc, char **argv, char **envp)
{ the_hand  = new hand("Hand", point(1, world_height - world_y_offset - 5));
  the_table = new table("Table", world_width, 0, point(0, 0));

  int x = 0;
  blocks[0] = the_table;
  blocks[1] = new brick("B1", 2, 2, point(x, 0), the_table); x += 2;
  blocks[2] = new brick("B2", 2, 2, point(x, 0), the_table); x += 2;
  blocks[3] = new brick("B3", 4, 4, point(x, 0), the_table); x += 4;
  blocks[4] = new brick("B4", 2, 2, point(x, 0), the_table); x += 2;
  blocks[5] = new wedge("W5", 2, 4, point(x, 0), the_table); x += 2;
  blocks[6] = new brick("B6", 4, 2, point(x, 0), the_table); x += 4;
  blocks[7] = new wedge("W7", 2, 2, point(x, 0), the_table); x += 2;
  blocks[8] = new ball ("L8", 2, 2, point(x, 0), the_table); x += 2;

  make_window (argc, argv, envp); }

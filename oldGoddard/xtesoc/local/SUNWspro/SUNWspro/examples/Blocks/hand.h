#ifndef HAND_H
#define HAND_H

#include <stream.h>
#include "point.h"
#include "block.h"
#include "windows.h"

extern "C" abs(int);

class hand {
 private:
  char* nm;
  point position;
  block* grasping;
  void animate(point);
  point& pos() { return position; }
 public:
  hand(char*, const point&);
  char* name() { return nm; }
  void draw(Xv_opaque);
  void grasp(block&);
  void move(block&, load_bearing_block&);
  void ungrasp(block&);
  friend ostream& operator<<(ostream&, hand&); };

extern hand *the_hand;

#endif

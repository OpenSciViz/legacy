#include <stream.h>
#include <string.h>
#include "block.h"
#include "block_list.h"
#include "hand.h"

block::block()
{ nm = "???"; }

block::block
  (char* name, int w, int h, const point& pos, load_bearing_block* blk)
{ nm = name;
  width = w;
  height = h;
  position = pos;
  supported_by = blk;
  movable = 1;
  if (blk != 0)
    blk->add_supported_block(*this); }

ostream& operator<<(ostream& o, block& b)
{ return (o << "#<" << b.type() << ' ' << b.name() << ' ' << b.pos() << '>'); }

int operator==(block& a, block& b)
{ if ((&a != &b) && !strcmp(a.name(), b.name()))
    cout << "Warning: different objects have same name!" << endl;
  return (&a == &b); }

point block::top_location()
{ return point(x() + width / 2, y() + height); }

load_bearing_block::load_bearing_block()
{ support_for = 0; }

load_bearing_block::load_bearing_block
  (char* name, int w, int h, const point& pos, load_bearing_block* blk)
     : block(name, w, h, pos, blk) { support_for = 0; }

list* load_bearing_block::supported_blocks()
{ return support_for; }

void load_bearing_block::add_supported_block(block& b)
{ support_for = new list(b, *support_for); }

void load_bearing_block::remove_supported_block(block& b)
{ support_for = remove(b, *support_for); }

void load_bearing_block::print_supported_blocks()
{ cout << name() << ": ";
  if (support_for != 0)
    cout << (*support_for);
  cout << endl; }

void block::set_support(load_bearing_block* new_support)
{ if (supported_by != 0)
    supported_by->remove_supported_block(*this); 
  supported_by = new_support;
  if (supported_by != 0)
    supported_by->add_supported_block(*this); }

table::table(char* name, int w, int h, const point& pos)
     : load_bearing_block(name, w, h, pos, 0) { movable = 0; }

brick::brick
  (char* name, int w, int h, const point& pos, load_bearing_block* blk)
     : load_bearing_block(name, w, h, pos, blk) {}

wedge::wedge
  (char* name, int w, int h, const point& pos, load_bearing_block* blk)
     : block(name, w, h, pos, blk) {}

ball::ball(char* name, int w, int h, const point& pos, load_bearing_block* blk)
     : block(name, w, h, pos, blk) {}

int intersections(block& object, int offset, int base, list& obstacles)
{ int ls_proposed = offset + base;
  int rs_proposed = ls_proposed + object.w();
  for (list* link = &obstacles; link != 0; link = link->next())
    { block* obstacle = link->value();
      int ls_obstacle = obstacle->x();
      int rs_obstacle = ls_obstacle + obstacle->w();
      if (!(ls_proposed >= rs_obstacle || rs_proposed <= ls_obstacle))
	return 1; }
  return 0; }

point load_bearing_block::find_space(block& object)
{ for (int offset = 0; offset < width - object.w() + 1; offset++)
    if (!(intersections(object, offset, x(), *support_for)))
      return point (offset + x(), y() + height);
  return point(-1, -1); }

void get_rid_of (block& object)
{ cout << "First I must get " << object.name() << " out of the way." << endl;
  the_table->put_on (object); }

point load_bearing_block::make_space(block& object)
{ list* link = supported_blocks();
  while ((link = supported_blocks()) != 0)
    { block* obstruction = link->value();
      get_rid_of (*obstruction);
      point space = find_space(object);
      if (space != -1)
	return space; }
  return point(-1, -1); }

void load_bearing_block::clear_top()
{ list* link;
  while ((link = supported_blocks()) != 0)
    get_rid_of (*link->value()); }

point load_bearing_block::get_space (block& object)
{ point temp = find_space(object);
  if (temp != -1)
    return temp;
  else
    return (make_space(object)); }

void load_bearing_block::put_on(block& object)
{ if (!object.is_movable())
    cout << "How do you expect me to put block " << object.name()
      << " on top of another block?" << endl;
  else
    if (*this == object)
      cout << "How do you expect me to put " << name() << " on top of itself?"
	<< endl;
    else
      if (get_space(object) != -1)
	{ the_hand->grasp(object);
	  the_hand->move(object, *this);
	  the_hand->ungrasp(object); }
      else
	cout << "Sorry, but there is no room for " << object.name() << " on "
	  << name() << '.' << endl; }

void block::put_on(block& object)
{ if (!object.is_movable())
    cout << "How do you expect me to put block " << object.name()
      << " on top of another block?" << endl;
  else
    cout << "Sorry, but there is no room for " << object.name() << " on "
      << name() << '.' << endl; }

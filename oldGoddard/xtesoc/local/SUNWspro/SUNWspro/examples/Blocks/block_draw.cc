#include <math.h>
#include <string.h>

#include <xview/xview.h>
#include <xview/panel.h>
#include <xview/canvas.h>
#include <X11/Xlib.h>

#include "cfns.h"
#include "point.h"
#include "block.h"
#include "windows.h"

point point::convert(int x_offset, int y_offset)
{ return point((x() + x_offset) * world_factor + world_margin,
	       (world_height - y() - y_offset - world_y_offset) * world_factor
	       + world_margin); }

int convert_dim(int dimension)
{ return dimension * world_factor; }

void fill_rectangle (Xv_opaque pw, int x, int y, int w, int h, int color)
{ Display *display  = (Display*) xv_get (pw, XV_DISPLAY);
  Drawable drawable = (Drawable) xv_get (pw, XV_XID);
  XSetForeground (display, gc, colors[monochrome ? bg_color : color]);
  XFillRectangle (display, drawable, gc, x, y, w, h); }

void fill_circle (Xv_opaque pw, int x, int y, int rad, int color)
{ Display *display  = (Display*) xv_get (pw, XV_DISPLAY);
  Drawable drawable = (Drawable) xv_get (pw, XV_XID);
  XSetForeground (display, gc, colors[color]);
  XFillArc (display, drawable, gc, x, y, rad, rad, 0, 360 * 64); }

void draw_circle (Xv_opaque pw, int x, int y, int rad, int color)
{ Display *display  = (Display*) xv_get (pw, XV_DISPLAY);
  Drawable drawable = (Drawable) xv_get (pw, XV_XID);
  XSetForeground (display, gc, colors[color]);
  XDrawArc (display, drawable, gc, x, y, rad - 1, rad - 1, 0, 360 * 64); }

void draw_text (Xv_opaque pw, int x, int y, char* str)
{ Display *display  = (Display*) xv_get (pw, XV_DISPLAY);
  Drawable drawable = (Drawable) xv_get (pw, XV_XID);
  XSetFont (display, gc, (Font) xv_get (block_font, XV_XID));
  if (monochrome || world_factor < 15)
    XSetForeground (display, gc, colors[1]);
  else {
    XSetForeground (display, gc, colors[fg_color - 1]);
    XDrawString (display, drawable, gc, x-1, y-1, str, strlen(str));
    XSetForeground (display, gc, colors[fg_color / 8]);
    if (world_factor > 20) {
      XSetForeground (display, gc, colors[2]);
      XDrawString (display, drawable, gc, x+1, y+1, str, strlen(str));
      XSetForeground (display, gc, colors[fg_color / 2]); }}
  XDrawString (display, drawable, gc, x, y, str, strlen(str));
}

void draw_cylinder(Xv_opaque pw, point nw, point se)
{ int center = (nw.x() + se.x()) / 2;
  int r = center - nw.x();
  pw_batch_on (pw);
  for (int x = nw.x()+1; x <= se.x()-1; x++)
    { double color_index = double(center - x) * (center - x) / r / r;
      if (color_index > 1)
	color_index = 1;
      int c = 1 + int((color_ct - 3) * (1 - color_index));
      pw_vector (pw, x, nw.y()+1, x, se.y()-1, PIX_SRC,
		 monochrome ? bg_color : c); }
  pw_vector (pw, nw.x()+1, nw.y()+1, nw.x()+1, se.y()-1, PIX_SRC, fg_color);
  pw_vector (pw, se.x()-1, se.y()-1, se.x()-1, nw.y()+1, PIX_SRC, fg_color);
  pw_vector (pw, nw.x()+1, se.y()-1, se.x()-1, se.y()-1, PIX_SRC, fg_color);
  pw_vector (pw, nw.x()+1, nw.y()+1, se.x()-1, nw.y()+1, PIX_SRC, fg_color);
  pw_batch_off (pw); }

void draw_cone (Xv_opaque pw, point sw, point nc, point se)
{ int r = nc.x() - sw.x(),
      ht = sw.y() - nc.y();
  pw_batch_on (pw);
  if (!monochrome)
    for (int x = 0; x <= r; x++)
      { double color_index = 1 - (double(x)*x) / r / r;
	if (color_index < 0)
	  color_index = 0;
	int c = 1 + int((color_ct - 3) * color_index);
	pw_vector (pw, nc.x()-x, se.y(), nc.x(), nc.y(), PIX_SRC, c);
	pw_vector (pw, nc.x()+x, se.y(), nc.x(), nc.y(), PIX_SRC, c); }
  pw_vector (pw, se.x(), se.y(), nc.x(), nc.y(), PIX_SRC, fg_color);
  pw_vector (pw, sw.x(), sw.y(), nc.x(), nc.y(), PIX_SRC, fg_color);
  pw_vector (pw, se.x(), se.y(), sw.x(), sw.y(), PIX_SRC, fg_color);
  pw_batch_off (pw); }

void draw_cube (Xv_opaque pw, point nw, point se)
{ point sc = point(nw.x() + world_factor, se.y()),
        nc = point(se.x() - world_factor, nw.y());

  draw_cylinder (pw, nw, sc);
  draw_cylinder (pw, nc, se);

  int width  = (se.x() + nc.x()) / 2 - (sc.x() + nw.x()) / 2;
  int height = se.y() - nw.y() - 2;
  fill_rectangle
    (pw, (sc.x() + nw.x()) / 2, nw.y() + 1, width, height, fg_color - 1);
  pw_vector (pw, nw.x()+1, se.y()-1, se.x()-1, se.y()-1, PIX_SRC, fg_color);
  pw_vector (pw, nw.x()+1, nw.y()+1, se.x()-1, nw.y()+1, PIX_SRC, fg_color); }

void block::draw(Xv_opaque pw)
{ point c = convert(w() / 2, h() / 2);
  Font_string_dims dims;

  xv_get (block_font, FONT_STRING_DIMS, name(), &dims);
  c.x() -= dims.width / 2;
  c.y() += dims.height / 2;
  if (label_blocks)
    draw_text (pw, c.x(), c.y(), name()); }

void table::draw(Xv_opaque pw)
{ point leg(x() + 3, y() - 1), nw = leg.convert(); nw.y() -= 2;
  draw_cylinder (pw, nw, leg.convert(1, -5));
  leg.x() = w() - 4;
  nw = leg.convert(); nw.y() -= 2;
  draw_cylinder (pw, nw, leg.convert(1, -5));
  draw_cylinder (pw, convert(), convert(w(), -1)); }

void brick::draw(Xv_opaque pw)
{ draw_cube (pw, convert(0, h()), convert(w()));
  block::draw(pw); }

void wedge::draw(Xv_opaque pw)
{ point sw = convert(),
        nc = convert(w() / 2, h()),
        se = convert(w());
  sw.x()++; sw.y()--; nc.y()++; se.x()--; se.y()--;
  draw_cone (pw, sw, nc, se);
  block::draw(pw); }
  
void ball::draw(Xv_opaque pw)
{ point center = convert(w() / 2, w() / 2);
  int r = convert_dim(w() / 2);
  const double sphere_pwr = 3.5;
  if (monochrome)
    draw_circle (pw, center.x() - r + 1, center.y() - r + 1, r * 2 - 1,
		 fg_color);
  else
    for (int x = r; x >= 1; x--) {
      double color_index = pow (double(x) / r, sphere_pwr);
      if (color_index > 1)
	color_index = 1;
      int c = 1 + int((color_ct - 3) * (1 - color_index));
      fill_circle (pw, center.x() - x + 1, center.y() - x + 1, x * 2 - 1, c); }
 block::draw(pw); }


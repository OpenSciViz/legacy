#ifndef BLOCK_LIST_H
#define BLOCK_LIST_H

#include <stream.h>
#include <string.h>

#include "block.h"

class list {
 protected:
  block* car;
  list* cdr;
 public:
  list(int dummy, list& next) // dummy list element for list head
    { car = (block*)dummy;
      cdr = &next; }
  list(block& value)
    { car = &value;
      cdr = (list*)0; }
  list(block& value, list& next)
    { car = &value;
      cdr = &next; }
  block* value() { return car; }
  list* next()  { return cdr; }
  friend list* remove(block&, list&);
};

list* remove(list&, list&);

ostream& operator<<(ostream&, list);

#endif

#include "point.h"

int operator==(const point& p, int i)
{ return (p.xval == i && p.yval == i); }

ostream& operator<<(ostream& o, const point& p)
{ return (o << p.xval << ":" << p.yval); }

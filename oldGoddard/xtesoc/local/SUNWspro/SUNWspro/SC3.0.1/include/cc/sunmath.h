/*
 * Copyright 14 Nov 1993 Sun Microsystems, Inc.  All Rights Reserved.
 */

#ifndef _SUNMATH_H
#define	_SUNMATH_H

#ifdef __cplusplus
extern "C" {
#endif

#pragma ident	"@(#)sunmath.h	1.10	93/11/14"

#ifdef __STDC__
#define	__P(p)	p
#else
#define	__P(p)	()
#endif

#ifdef __STDC__
#include <stdio.h>			/* for the definition of FILE */
#endif
#include <floatingpoint.h>

/*
 * Definitions for convert_external.
 */
enum convert_external_arch_t {
	convert_external_sparc,
	convert_external_pc,
	convert_external_vax,
	convert_external_vaxg,
	convert_external_s370,
	convert_external_cray
};

enum convert_external_type_t {
	convert_external_signed,
	convert_external_unsigned,
	convert_external_float
};

enum convert_external_rounding_t {
	convert_external_common = -1,
	convert_external_biased = -2,
	convert_external_away = -3
};

typedef struct {
	enum convert_external_arch_t arch;	/* format architecture */
	enum convert_external_type_t type;	/* format type */	
	int size; 				/* format size in 8-bit bytes */
} convert_external_t;

#define	FLOATFUNCTIONTYPE	float
#define	RETURNFLOAT(x)		{return (x);}
#define	ASSIGNFLOAT(x,y)	{x = (y);}

#define	FLOATPARAMETER		float
#define	FLOATPARAMETERVALUE(x)	(x)

/*
 * Size of additive random number generator tables.
 */
#define	ADDRAN_SIZE 55

/*
 * Lower and upper bounds of additive random number generator tables.
 */
#define	I_ADDRAN_LB	(-2147483647-1)
#define	U_ADDRAN_LB	0
#define	R_ADDRAN_LB	0
#define	D_ADDRAN_LB	0
#define	I_ADDRAN_UB	2147483647
#define	U_ADDRAN_UB	((unsigned)4294967295)
#define	R_ADDRAN_UB	0.9999999403953552246
#define	D_ADDRAN_UB	0.9999999999999998890

#define	LCRAN_MULTIPLIER	16807		 /* default multiplier */
#define	LCRAN_FIRST	1			 /* first LCRAN variate */
#define	LCRAN_MODULUS	2147483647		 /* 2**31-1 */
#define	ILCRAN_MODULUS	4.656612875245796923E-10 /* 1/(2**31-1) */

#define	I_LCRAN_LB	1
#define	R_LCRAN_LB	4.656612873077392578E-10 /* I_LCRAN_LB/LCRAN_MODULUS */
#define	D_LCRAN_LB	4.656612875245796923E-10
#define	I_LCRAN_UB	(LCRAN_MODULUS-1)
#define	R_LCRAN_UB	1			 /* I_LCRAN_UB/LCRAN_MODULUS */
#define	D_LCRAN_UB	0.9999999995343387127

extern int signgamf;	/* for use with gammaf() and lgammaf() */
extern int signgaml;	/* for use with gammal() and lgammal() */

/*
 * ANSI C Future Library Directions {4.13.4}
 * Note that frexpf, ldexpf, modff, frexpl, ldexpl and modfl are _not_ provided.
 */
extern float acosf __P((float));
extern float asinf __P((float));
extern float atanf __P((float));
extern float atan2f __P((float, float));
extern float cosf __P((float));
extern float sinf __P((float));
extern float tanf __P((float));

extern float coshf __P((float));
extern float sinhf __P((float));
extern float tanhf __P((float));

extern float expf __P((float));
extern float logf __P((float));
extern float log10f __P((float));

extern float powf __P((float, float));
extern float sqrtf __P((float));

extern float ceilf __P((float));
extern float fabsf __P((float));
extern float floorf __P((float));
extern float fmodf __P((float, float));

extern long double acosl __P((long double));
extern long double asinl __P((long double));
extern long double atanl __P((long double));
extern long double atan2l __P((long double, long double));
extern long double cosl __P((long double));
extern long double sinl __P((long double));
extern long double tanl __P((long double));

extern long double coshl __P((long double));
extern long double sinhl __P((long double));
extern long double tanhl __P((long double));

extern long double expl __P((long double));
extern long double logl __P((long double));
extern long double log10l __P((long double));

extern long double powl __P((long double, long double));
extern long double sqrtl __P((long double));

extern long double ceill __P((long double));
extern long double fabsl __P((long double));
extern long double floorl __P((long double));
extern long double fmodl __P((long double, long double));

/*
 * SVID & X/Open float and long double counterparts
 */
extern float erff __P((float));
extern float erfcf __P((float));
extern float gammaf __P((float));
extern float hypotf __P((float, float));
extern int isnanf __P((float));
extern float j0f __P((float));
extern float j1f __P((float));
extern float jnf __P((int, float));
extern float lgammaf __P((float));
extern float y0f __P((float));
extern float y1f __P((float));
extern float ynf __P((int, float));

extern long double erfl __P((long double));
extern long double erfcl __P((long double));
extern long double gammal __P((long double));
extern long double hypotl __P((long double, long double));
extern int isnanl __P((long double));
extern long double j0l __P((long double));
extern long double j1l __P((long double));
extern long double jnl __P((int, long double));
extern long double lgammal __P((long double));
extern long double y0l __P((long double));
extern long double y1l __P((long double));
extern long double ynl __P((int, long double));

/*
 * SVID float and long double counterparts
 */
extern float acoshf __P((float));
extern float asinhf __P((float));
extern float atanhf __P((float));
extern float cbrtf __P((float));
extern float logbf __P((float));
extern float nextafterf __P((float, float));
extern float remainderf __P((float, float));
extern float scalbf __P((float, float));

extern long double acoshl __P((long double));
extern long double asinhl __P((long double));
extern long double atanhl __P((long double));
extern long double cbrtl __P((long double));
extern long double logbl __P((long double));
extern long double nextafterl __P((long double, long double));
extern long double remainderl __P((long double, long double));
extern long double scalbl __P((long double, long double));

/*
 * IEEE Test Vector float and long double counterparts
 */
extern float significandf __P((float));
extern long double significandl __P((long double));

/*
 * Functions callable from C, intended to support IEEE arithmetic.
 *
 * Functions declared elsewhere:
 * <ieeefp.h>:			finite & isnanf
 * <math.h> X/Open area:	isnan
 * <math.h> SVID area:		logb, nextafter, remainder & scalb
 * <math.h> IEEE support area:	copysign, ilogb, rint, scalbn
 * <sunmath.h> X/Open area:	isnanl
 * <sunmath.h> SVID area:	logbf, nextafterf, remainderf & scalbf;
 *				logbl, nextafterl, remainderl & scalbl.
 */
extern enum fp_class_type fp_class __P((double));
extern double infinity __P((void));
extern int irint __P((double));
extern int isinf __P((double));
extern int isnormal __P((double));
extern int issubnormal __P((double));
extern int iszero __P((double));
extern double max_normal __P((void));
extern double max_subnormal __P((void));
extern double min_normal __P((void));
extern double min_subnormal __P((void));
extern double quiet_nan __P((long));
extern double signaling_nan __P((long));
extern int signbit __P((double));

extern float copysignf __P((float, float));
extern int finitef __P((float));
extern enum fp_class_type fp_classf __P((float));
extern int ilogbf __P((float));
extern float infinityf __P((void));
extern int irintf __P((float));
extern int isinff __P((float));
extern int isnormalf __P((float));
extern int issubnormalf __P((float));
extern int iszerof __P((float));
extern float max_normalf __P((void));
extern float max_subnormalf __P((void));
extern float min_normalf __P((void));
extern float min_subnormalf __P((void));
extern float quiet_nanf __P((long));
extern float rintf __P((float));
extern float scalbnf __P((float, int));
extern float signaling_nanf __P((long));
extern int signbitf __P((float));

extern long double copysignl __P((long double, long double));
extern int finitel __P((long double));
extern enum fp_class_type fp_classl __P((long double));
extern int ilogbl __P((long double));
extern long double infinityl __P((void));
extern int irintl __P((long double));
extern int isinfl __P((long double));
extern int isnormall __P((long double));
extern int issubnormall __P((long double));
extern int iszerol __P((long double));
extern long double max_normall __P((void));
extern long double max_subnormall __P((void));
extern long double min_normall __P((void));
extern long double min_subnormall __P((void));
extern long double quiet_nanl __P((long));
extern long double rintl __P((long double));
extern long double scalbnl __P((long double, int));
extern long double signaling_nanl __P((long));
extern int signbitl __P((long double));

/*
 * IEEE exceptions handling.
 *
 * Function(s) and typedef(s) declared elsewhere:
 * <floatingpoint.h>:	sigfpe, sigfpe_code_type, sigfpe_handler_type
 */
extern int ieee_flags __P((const char *, const char *, const char *, char **));
extern int ieee_handler __P((const char *, const char *, sigfpe_handler_type));
extern void ieee_retrospective __P((FILE *));
extern void nonstandard_arithmetic __P((void));
extern void standard_arithmetic __P((void));

/*
 * BSD math library entry points' float and long double counterparts.
 * Note that drem will disappear in a future release; use remainder instead.
 */
extern double drem __P((double, double));

extern float expm1f __P((float));
extern float log1pf __P((float));

extern long double expm1l __P((long double));
extern long double log1pl __P((long double));

/* 
 * Reentrant version of gamma and lgamma - float and long double counterparts.
 */
extern float gammaf_r __P((float, int *));
extern float lgammaf_r __P((float, int *));

extern long double gammal_r __P((long double, int *));
extern long double lgammal_r __P((long double, int *));

/*
 * Some commonly used functions.
 */
extern double aint __P((double));
extern double anint __P((double));
extern double annuity __P((double, double));
extern double compound __P((double, double));
extern double exp10 __P((double));
extern double exp2 __P((double));
extern double log2 __P((double));
extern int nint __P((double));
extern void sincos __P((double, double *, double *));

extern float aintf __P((float));
extern float anintf __P((float));
extern float annuityf __P((float, float));
extern float compoundf __P((float, float));
extern float exp10f __P((float));
extern float exp2f __P((float));
extern float log2f __P((float));
extern int nintf __P((float));
extern void sincosf __P((float, float *, float *));

extern long double aintl __P((long double));
extern long double anintl __P((long double));
extern long double annuityl __P((long double, long double));
extern long double compoundl __P((long double, long double));
extern long double exp10l __P((long double));
extern long double exp2l __P((long double));
extern long double log2l __P((long double));
extern int nintl __P((long double));
extern void sincosl __P((long double, long double *, long double *));

/*
 * trig{true_pi*x} and inverses in radians.
 */
extern double acospi __P((double));
extern double asinpi __P((double));
extern double atan2pi __P((double, double));
extern double atanpi __P((double));
extern double cospi __P((double));
extern double sinpi __P((double));
extern double tanpi __P((double));
extern void sincospi __P((double, double *, double *));

extern float acospif __P((float));
extern float asinpif __P((float));
extern float atan2pif __P((float, float));
extern float atanpif __P((float));
extern float cospif __P((float));
extern float sinpif __P((float));
extern float tanpif __P((float));
extern void sincospif __P((float, float *, float *));

extern long double acospil __P((long double));
extern long double asinpil __P((long double));
extern long double atan2pil __P((long double, long double));
extern long double atanpil __P((long double));
extern long double cospil __P((long double));
extern long double sinpil __P((long double));
extern long double tanpil __P((long double));
extern void sincospil __P((long double, long double *, long double *));

/*
 * trig{x} and inverses in degrees.
 */
extern double acosd __P((double));
extern double asind __P((double));
extern double atan2d __P((double, double));
extern double atand __P((double));
extern double cosd __P((double));
extern double sind __P((double));
extern double tand __P((double));
extern void sincosd __P((double, double *, double *));

extern float acosdf __P((float));
extern float asindf __P((float));
extern float atan2df __P((float, float));
extern float atandf __P((float));
extern float cosdf __P((float));
extern float sindf __P((float));
extern float tandf __P((float));
extern void sincosdf __P((float, float *, float *));

extern long double acosdl __P((long double));
extern long double asindl __P((long double));
extern long double atandl __P((long double));
extern long double atan2dl __P((long double, long double));
extern long double cosdl __P((long double));
extern long double sindl __P((long double));
extern long double tandl __P((long double));
extern void sincosdl __P((long double, long double *, long double *));

/*
 * trig{{true_pi/machine_pi}*x} and inverses in radians.
 */
extern double acosp __P((double));
extern double asinp __P((double));
extern double atanp __P((double));
extern double cosp __P((double));
extern double sinp __P((double));
extern double tanp __P((double));
extern void sincosp __P((double, double *, double *));

extern float acospf __P((float));
extern float asinpf __P((float));
extern float atanpf __P((float));
extern float cospf __P((float));
extern float sinpf __P((float));
extern float tanpf __P((float));
extern void sincospf __P((float, float *, float *));

extern long double acospl __P((long double));
extern long double asinpl __P((long double));
extern long double atanpl __P((long double));
extern long double cospl __P((long double));
extern long double sinpl __P((long double));
extern long double tanpl __P((long double));
extern void sincospl __P((long double, long double *, long double *));

/*
 * Convert external binary data formats - see convert_external{3M}
 */
extern fp_exception_field_type
	convert_external __P((const char *, convert_external_t, char *,
			convert_external_t, int, int));

/*
 * Additive Random Number Generators - see addrans{3M}
 */
extern int i_addran_ __P((void));
extern float r_addran_ __P((void));
extern double d_addran_ __P((void));

extern void i_addrans_ __P((int *, int *, int *, int *));
extern void u_addrans_ __P((unsigned *, int *, unsigned *, unsigned *));
extern void r_addrans_ __P((float *, int *, float *, float *));
extern void d_addrans_ __P((double *, int *, double *, double *));

extern void i_get_addrans_ __P((int *));
extern void r_get_addrans_ __P((float *));
extern void d_get_addrans_ __P((double *));

extern void i_set_addrans_ __P((int *));
extern void r_set_addrans_ __P((float *));
extern void d_set_addrans_ __P((double *));

extern void i_init_addrans_ __P((void));
extern void r_init_addrans_ __P((void));
extern void d_init_addrans_ __P((void));

/*
 * Linear Congruential Random Number Generators - see lcrans{3M}
 */
extern int i_lcran_ __P((void));
extern float r_lcran_ __P((void));
extern double d_lcran_ __P((void));

extern void i_lcrans_ __P((int *, int *, int *, int *));
extern void u_lcrans_ __P((unsigned *, int *, unsigned *, unsigned *));
extern void r_lcrans_ __P((float *, int *, float *, float *));
extern void d_lcrans_ __P((double *, int *, double *, double *));

extern void i_get_lcrans_ __P((int *));
extern void i_set_lcrans_ __P((int *));
extern void i_init_lcrans_ __P((void));

/*
 * Random Number Shufflers - see shufrans{3M}
 */
extern void i_shufrans_ __P((int *, int *, int *, int *));
extern void u_shufrans_ __P((unsigned *, int *, unsigned *, unsigned *));
extern void r_shufrans_ __P((float *, int *, float *, float *));
extern void d_shufrans_ __P((double *, int *, double *, double *));

/*
 * Functions callable from FORTRAN.
 */
extern void abrupt_underflow_ __P((void));
extern fp_exception_field_type
	convert_external_ __P((const char *, const convert_external_t *, char *,
			const convert_external_t *, const int *, const int *));
extern void gradual_underflow_ __P((void));
extern int ieee_flags_ __P((const char *, const char *, const char *, char *,
			int, int, int, int));
extern int ieee_handler_ __P((const char *, const char *, sigfpe_handler_type,
			int, int));
extern void ieee_retrospective_ __P((void));
extern void nonstandard_arithmetic_ __P((void));
extern sigfpe_handler_type sigfpe_ __P((sigfpe_code_type *,
					sigfpe_handler_type));
extern void standard_arithmetic_ __P((void));

extern double d_acos_ __P((double *));
extern double d_acosd_ __P((double *));
extern double d_acosh_ __P((double *));
extern double d_acosp_ __P((double *));
extern double d_acospi_ __P((double *));
extern double d_aint_ __P((double *));
extern double d_anint_ __P((double *));
extern double d_annuity_ __P((double *, double *));
extern double d_asin_ __P((double *));
extern double d_asind_ __P((double *));
extern double d_asinh_ __P((double *));
extern double d_asinp_ __P((double *));
extern double d_asinpi_ __P((double *));
extern double d_atan_ __P((double *));
extern double d_atan2_ __P((double *, double *));
extern double d_atan2d_ __P((double *, double *));
extern double d_atan2pi_ __P((double *, double *));
extern double d_atand_ __P((double *));
extern double d_atanh_ __P((double *));
extern double d_atanp_ __P((double *));
extern double d_atanpi_ __P((double *));
extern double d_cbrt_ __P((double *));
extern double d_ceil_ __P((double *));
extern double d_compound_ __P((double *, double *));
extern double d_copysign_ __P((double *, double *));
extern double d_cos_ __P((double *));
extern double d_cosd_ __P((double *));
extern double d_cosh_ __P((double *));
extern double d_cosp_ __P((double *));
extern double d_cospi_ __P((double *));
extern double d_erf_ __P((double *));
extern double d_erfc_ __P((double *));
extern double d_exp_ __P((double *));
extern double d_exp10_ __P((double *));
extern double d_exp2_ __P((double *));
extern double d_expm1_ __P((double *));
extern double d_fabs_ __P((double *));
extern int id_finite_ __P((double *));
extern double d_floor_ __P((double *));
extern double d_fmod_ __P((double *, double *));
extern enum fp_class_type id_fp_class_ __P((double *));
extern double d_hypot_ __P((double *, double *));
extern int id_ilogb_ __P((double *));
extern double d_infinity_ __P((void));
extern int id_irint_ __P((double *));
extern int id_isinf_ __P((double *));
extern int id_isnan_ __P((double *));
extern int id_isnormal_ __P((double *));
extern int id_issubnormal_ __P((double *));
extern int id_iszero_ __P((double *));
extern double d_j0_ __P((double *));
extern double d_j1_ __P((double *));
extern double d_jn_ __P((int *, double *));
extern double d_lgamma_ __P((double *));
extern double d_lgamma_r_ __P((double *, int *));
extern double d_log_ __P((double *));
extern double d_log10_ __P((double *));
extern double d_log1p_ __P((double *));
extern double d_log2_ __P((double *));
extern double d_logb_ __P((double *));
extern double d_max_normal_ __P((void));
extern double d_max_subnormal_ __P((void));
extern double d_min_normal_ __P((void));
extern double d_min_subnormal_ __P((void));
extern double d_nextafter_ __P((double *, double *));
extern int id_nint_ __P((double *));
extern double d_pow_ __P((double *, double *));
extern double d_quiet_nan_ __P((long *));
extern double d_remainder_ __P((double *, double *));
extern double d_rint_ __P((double *));
extern double d_scalb_ __P((double *, double *));
extern double d_scalbn_ __P((double *, int *));
extern double d_signaling_nan_ __P((long *));
extern int id_signbit_ __P((double *));
extern double d_significand_ __P((double *));
extern double d_sin_ __P((double *));
extern void d_sincos_ __P((double *, double *, double *));
extern void d_sincosd_ __P((double *, double *, double *));
extern void d_sincosp_ __P((double *, double *, double *));
extern void d_sincospi_ __P((double *, double *, double *));
extern double d_sind_ __P((double *));
extern double d_sinh_ __P((double *));
extern double d_sinp_ __P((double *));
extern double d_sinpi_ __P((double *));
extern double d_sqrt_ __P((double *));
extern double d_tan_ __P((double *));
extern double d_tand_ __P((double *));
extern double d_tanh_ __P((double *));
extern double d_tanp_ __P((double *));
extern double d_tanpi_ __P((double *));
extern double d_y0_ __P((double *));
extern double d_y1_ __P((double *));
extern double d_yn_ __P((int *, double *));

extern float r_acos_ __P((float *));
extern float r_acosd_ __P((float *));
extern float r_acosh_ __P((float *));
extern float r_acosp_ __P((float *));
extern float r_acospi_ __P((float *));
extern float r_aint_ __P((float *));
extern float r_anint_ __P((float *));
extern float r_annuity_ __P((float *, float *));
extern float r_asin_ __P((float *));
extern float r_asind_ __P((float *));
extern float r_asinh_ __P((float *));
extern float r_asinp_ __P((float *));
extern float r_asinpi_ __P((float *));
extern float r_atan_ __P((float *));
extern float r_atan2_ __P((float *, float *));
extern float r_atan2d_ __P((float *, float *));
extern float r_atan2pi_ __P((float *, float *));
extern float r_atand_ __P((float *));
extern float r_atanh_ __P((float *));
extern float r_atanp_ __P((float *));
extern float r_atanpi_ __P((float *));
extern float r_cbrt_ __P((float *));
extern float r_ceil_ __P((float *));
extern float r_compound_ __P((float *, float *));
extern float r_copysign_ __P((float *, float *));
extern float r_cos_ __P((float *));
extern float r_cosd_ __P((float *));
extern float r_cosh_ __P((float *));
extern float r_cosp_ __P((float *));
extern float r_cospi_ __P((float *));
extern float r_erf_ __P((float *));
extern float r_erfc_ __P((float *));
extern float r_exp_ __P((float *));
extern float r_exp10_ __P((float *));
extern float r_exp2_ __P((float *));
extern float r_expm1_ __P((float *));
extern float r_fabs_ __P((float *));
extern int ir_finite_ __P((float *));
extern float r_floor_ __P((float *));
extern float r_fmod_ __P((float *, float *));
extern enum fp_class_type ir_fp_class_ __P((float *));
extern float r_hypot_ __P((float *, float *));
extern int ir_ilogb_ __P((float *));
extern float r_infinity_ __P((void));
extern int ir_irint_ __P((float *));
extern int ir_isinf_ __P((float *));
extern int ir_isnan_ __P((float *));
extern int ir_isnormal_ __P((float *));
extern int ir_issubnormal_ __P((float *));
extern int ir_iszero_ __P((float *));
extern float r_j0_ __P((float *));
extern float r_j1_ __P((float *));
extern float r_jn_ __P((int *, float *));
extern float r_lgamma_ __P((float *));
extern float r_lgamma_r_ __P((float *, int *));
extern float r_log_ __P((float *));
extern float r_log10_ __P((float *));
extern float r_log1p_ __P((float *));
extern float r_log2_ __P((float *));
extern float r_logb_ __P((float *));
extern float r_max_normal_ __P((void));
extern float r_max_subnormal_ __P((void));
extern float r_min_normal_ __P((void));
extern float r_min_subnormal_ __P((void));
extern float r_nextafter_ __P((float *, float *));
extern int ir_nint_ __P((float *));
extern float r_pow_ __P((float *, float *));
extern float r_quiet_nan_ __P((long *));
extern float r_remainder_ __P((float *, float *));
extern float r_rint_ __P((float *));
extern float r_scalb_ __P((float *, float *));
extern float r_scalbn_ __P((float *, int *));
extern float r_signaling_nan_ __P((long *));
extern int ir_signbit_ __P((float *));
extern float r_significand_ __P((float *));
extern float r_sin_ __P((float *));
extern void r_sincos_ __P((float *, float *, float *));
extern void r_sincosd_ __P((float *, float *, float *));
extern void r_sincosp_ __P((float *, float *, float *));
extern void r_sincospi_ __P((float *, float *, float *));
extern float r_sind_ __P((float *));
extern float r_sinh_ __P((float *));
extern float r_sinp_ __P((float *));
extern float r_sinpi_ __P((float *));
extern float r_sqrt_ __P((float *));
extern float r_tan_ __P((float *));
extern float r_tand_ __P((float *));
extern float r_tanh_ __P((float *));
extern float r_tanp_ __P((float *));
extern float r_tanpi_ __P((float *));
extern float r_y0_ __P((float *));
extern float r_y1_ __P((float *));
extern float r_yn_ __P((int *, float *));

extern long double q_copysign_ __P((long double *, long double *));
extern long double q_fabs_ __P((long double *));
extern int iq_finite_ __P((long double *));
extern long double q_fmod_ __P((long double *, long double *));
extern enum fp_class_type iq_fp_class_ __P((long double *));
extern int iq_ilogb_ __P((long double *));
extern long double q_infinity_ __P((void));
extern int iq_isinf_ __P((long double *));
extern int iq_isnan_ __P((long double *));
extern int iq_isnormal_ __P((long double *));
extern int iq_issubnormal_ __P((long double *));
extern int iq_iszero_ __P((long double *));
extern long double q_max_normal_ __P((void));
extern long double q_max_subnormal_ __P((void));
extern long double q_min_normal_ __P((void));
extern long double q_min_subnormal_ __P((void));
extern long double q_nextafter_ __P((long double *, long double *));
extern long double q_quiet_nan_ __P((long *));
extern long double q_remainder_ __P((long double *, long double *));
extern long double q_scalbn_ __P((long double *, int *));
extern long double q_signaling_nan_ __P((long *));
extern int iq_signbit_ __P((long double *));

#ifdef __cplusplus
}
#endif

#endif	/* !defined(_SUNMATH_H) */

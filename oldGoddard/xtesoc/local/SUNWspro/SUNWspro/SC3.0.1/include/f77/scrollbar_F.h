#ifndef scrollbar_F_DEFINED
#define scrollbar_F_DEFINED

C  derived from @(#)scrollbar.h 1.35 91/03/19  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Module:	scrollbar.h
C  * Library:	libxview.a
C  *
C  * Level:	public
C  *
C  * Description:
C  *
C  *	Describes attributes for scrollbar
C  *
C  
#include "attr_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "window_F.h"
      Xv_pkg_ptr SCROLLBAR
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
#define Scrollbar Xv_opaque
      STRUCTURE /Xv_scrollbar/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
C  Public Attributes 
      INTEGER SCROLLBAR_COMPUTE_SCROLL_PROC
      PARAMETER (SCROLLBAR_COMPUTE_SCROLL_PROC = 1627785825)
      INTEGER SCROLLBAR_DIRECTION
      PARAMETER (SCROLLBAR_DIRECTION = 1627916577)
      INTEGER SCROLLBAR_PERCENT_OF_DRAG_REPAIN
      PARAMETER (SCROLLBAR_PERCENT_OF_DRAG_REPAIN = 1628375041)
      INTEGER SCROLLBAR_INACTIVE
      PARAMETER (SCROLLBAR_INACTIVE = 1628244225)
      INTEGER SCROLLBAR_LAST_VIEW_START
      PARAMETER (SCROLLBAR_LAST_VIEW_START = 1628047361)
      INTEGER SCROLLBAR_MENU
      PARAMETER (SCROLLBAR_MENU = 1628113409)
      INTEGER SCROLLBAR_MOTION
      PARAMETER (SCROLLBAR_MOTION = 1628309505)
      INTEGER SCROLLBAR_NORMALIZE_PROC
      PARAMETER (SCROLLBAR_NORMALIZE_PROC = 1627720289)
      INTEGER SCROLLBAR_NOTIFY_CLIENT
      PARAMETER (SCROLLBAR_NOTIFY_CLIENT = 1627982337)
      INTEGER SCROLLBAR_OBJECT_LENGTH
      PARAMETER (SCROLLBAR_OBJECT_LENGTH = 1627457537)
      INTEGER SCROLLBAR_OVERSCROLL
      PARAMETER (SCROLLBAR_OVERSCROLL = 1628178433)
      INTEGER SCROLLBAR_PAGE_LENGTH
      PARAMETER (SCROLLBAR_PAGE_LENGTH = 1627654145)
      INTEGER SCROLLBAR_PIXELS_PER_UNIT
      PARAMETER (SCROLLBAR_PIXELS_PER_UNIT = 1627392001)
      INTEGER SCROLLBAR_SPLITTABLE
      PARAMETER (SCROLLBAR_SPLITTABLE = 1627851009)
      INTEGER SCROLLBAR_VIEW_START
      PARAMETER (SCROLLBAR_VIEW_START = 1627523073)
      INTEGER SCROLLBAR_VIEW_LENGTH
      PARAMETER (SCROLLBAR_VIEW_LENGTH = 1627588609)
#define Scrollbar_attribute Attr_attribute
      INTEGER SCROLLBAR_ABSOLUTE,SCROLLBAR_POINT_TO_MIN
     &,SCROLLBAR_PAGE_FORWARD,SCROLLBAR_LINE_FORWARD
     &,SCROLLBAR_MIN_TO_POINT,SCROLLBAR_PAGE_BACKWARD
     &,SCROLLBAR_LINE_BACKWARD,SCROLLBAR_TO_END,SCROLLBAR_TO_START
     &,SCROLLBAR_PAGE_ALIGNED,SCROLLBAR_NONE
      PARAMETER ( SCROLLBAR_ABSOLUTE=0,  SCROLLBAR_POINT_TO_MIN=1, 
     & SCROLLBAR_PAGE_FORWARD=2,  SCROLLBAR_LINE_FORWARD=3, 
     & SCROLLBAR_MIN_TO_POINT=4,  SCROLLBAR_PAGE_BACKWARD=5, 
     & SCROLLBAR_LINE_BACKWARD=6,  SCROLLBAR_TO_END=7, 
     & SCROLLBAR_TO_START=8,  SCROLLBAR_PAGE_ALIGNED=9, 
     & SCROLLBAR_NONE=10)
#define Scroll_motion INTEGER*4
      INTEGER SCROLLBAR_VERTICAL,SCROLLBAR_HORIZONTAL
      PARAMETER ( SCROLLBAR_VERTICAL=0,  SCROLLBAR_HORIZONTAL=1)
#define Scrollbar_setting INTEGER*4
      INTEGER SCROLLBAR_FIRST
      PARAMETER (SCROLLBAR_FIRST = 32256)
      INTEGER SCROLLBAR_REQUEST
      PARAMETER (SCROLLBAR_REQUEST = SCROLLBAR_FIRST+0)
C  32256 
C  SunView1 Compatiblity 
      INTEGER SCROLL_LINE_HEIGHT
      PARAMETER (SCROLL_LINE_HEIGHT = SCROLLBAR_PIXELS_PER_UNIT)
#define Scrollbar_compute_proc INTEGER
#define Scrollbar_compute_procp INTEGER*4
#define Scrollbar_normalize_proc INTEGER
#define Scrollbar_normalize_procp INTEGER*4
C 
C  * Public functions 
C  
      EXTERNAL scrollbar_default_compute_scroll
C       Scrollbar sb
C       INTEGER pos
C       INTEGER length
C       Scroll_motion motion
C       INTEGER offset
C       INTEGER object_length

      EXTERNAL scrollbar_paint
C       Scrollbar scrollbar

      COMMON /scrollbar_globals/   SCROLLBAR
#endif scrollbar_F.

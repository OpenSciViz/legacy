#ifndef rect_F_DEFINED
#define rect_F_DEFINED

C   derived from @(#)rect.h 20.18 91/05/28 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Defines the interface to the geometric object
C  * called a Rect which is a rectangle.
C  
C 
C  * PUBLIC #defines
C  
      INTEGER RECT_NULL
      PARAMETER (RECT_NULL = 0)
#define coord INTEGER*2
C 
C  * Rectangle sort ordering.
C  
      INTEGER RECTS_TOPTOBOTTOM
      PARAMETER (RECTS_TOPTOBOTTOM = 0)
      INTEGER RECTS_BOTTOMTOTOP
      PARAMETER (RECTS_BOTTOMTOTOP = 1)
      INTEGER RECTS_LEFTTORIGHT
      PARAMETER (RECTS_LEFTTORIGHT = 2)
      INTEGER RECTS_RIGHTTOLEFT
      PARAMETER (RECTS_RIGHTTOLEFT = 3)
      INTEGER RECTS_UNSORTED
      PARAMETER (RECTS_UNSORTED = -1)
      INTEGER RECTS_SORTS
      PARAMETER (RECTS_SORTS = 4)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C 
C  * PUBLIC structures 
C  
      STRUCTURE /Rect/
          coord r_left, r_top
          INTEGER*2  r_width, r_height
          END STRUCTURE
#define Rect_ptr INTEGER*4
C 
C  * Rect Geometry macros
C  
      coord  rect_right
      EXTERNAL rect_right
C       Rect_ptr rect

      coord  rect_bottom
      EXTERNAL rect_bottom
C       Rect_ptr rect

      EXTERNAL rect_print
C       Rect_ptr rect

      EXTERNAL rect_marginadjust
C       RECORD /Rect/ r
C       INTEGER*2 m

      EXTERNAL rect_borderadjust
C       RECORD /Rect/ r
C       INTEGER*2 m

      EXTERNAL rect_construct
C       RECORD /Rect/ r
C       coord x
C       coord y
C       INTEGER*2 w
C       INTEGER*2 h

C 
C  * Rect Predicate macros
C  
      LOGICAL  rect_equal
      EXTERNAL rect_equal
C       Rect_ptr r1
C       Rect_ptr r2

      LOGICAL  rect_sizes_differ
      EXTERNAL rect_sizes_differ
C       Rect_ptr r1
C       Rect_ptr r2

      LOGICAL  rect_isnull
      EXTERNAL rect_isnull
C       Rect_ptr r

      LOGICAL  rect_includespoint
      EXTERNAL rect_includespoint
C       Rect_ptr r
C       coord x
C       coord y

      LOGICAL  rect_includesrect
      EXTERNAL rect_includesrect
C       Rect_ptr r1
C       Rect_ptr r2

      LOGICAL  rect_intersectsrect
      EXTERNAL rect_intersectsrect
C       Rect_ptr r1
C       Rect_ptr r2

C 
C  * Rect Transformation macros used for passing rects up/down embedded
C  * coordinate systems.
C  
      EXTERNAL rect_passtoparent
C       coord x
C       coord y
C       RECORD /Rect/ rect

      EXTERNAL rect_passtochild
C       coord x
C       coord y
C       RECORD /Rect/ rect

C 
C  * PUBLIC variables 
C  
C 
C  * PUBLIC functions 
C  
      EXTERNAL rect_bounding
C       RECORD /Rect/  returnval__
C       RECORD /Rect/ r1
C       RECORD /Rect/ r2

      INTEGER  rect_clipvector
      EXTERNAL rect_clipvector
C       RECORD /Rect/ r
C       INTEGER x1arg
C       INTEGER y1arg
C       INTEGER x2arg
C       INTEGER y2arg

      INTEGER  rect_order
      EXTERNAL rect_order
C       RECORD /Rect/ r1
C       RECORD /Rect/ r2
C       INTEGER sortorder

      INTEGER  rect_right_of
      EXTERNAL rect_right_of
C       RECORD /Rect/ rect1
C       RECORD /Rect/ rect2

      INTEGER  rect_below
      EXTERNAL rect_below
C       RECORD /Rect/ rect1
C       RECORD /Rect/ rect2

      EXTERNAL rect_intersection
C       RECORD /Rect/ r1
C       RECORD /Rect/ r2
C       RECORD /Rect/ r

      INTEGER  rect_distance
      EXTERNAL rect_distance
C       RECORD /Rect/ rect
C       INTEGER x
C       INTEGER y
C       INTEGER x_used
C       INTEGER y_used

#endif rect_F.

#ifndef cms_rgb_F_DEFINED
#define cms_rgb_F_DEFINED

C   derived from @(#)cms_rgb.h 20.10 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Definition of the colormap segment CMS_RGB,
C  * the collection of rgb (red-green-blue) binary combinations.
C  
      CHARACTER*(3) CMS_RGB
      PARAMETER (CMS_RGB = "rgb")
      INTEGER CMS_RGBSIZE
      PARAMETER (CMS_RGBSIZE = 8)
      INTEGER BLACK
      PARAMETER (BLACK = 0)
      INTEGER RED
      PARAMETER (RED = 1)
      INTEGER YELLOW
      PARAMETER (YELLOW = 2)
      INTEGER GREEN
      PARAMETER (GREEN = 3)
      INTEGER CYAN
      PARAMETER (CYAN = 4)
      INTEGER BLUE
      PARAMETER (BLUE = 5)
      INTEGER MAGENTA
      PARAMETER (MAGENTA = 6)
      INTEGER WHITE
      PARAMETER (WHITE = 7)
#define Cms_rgb(__v) Byte __v ( 0 : 7 )
      EXTERNAL cms_rgbsetup
C       Cms_rgb (r)
C       Cms_rgb (g)
C       Cms_rgb (b)

#endif cms_rgb_F.

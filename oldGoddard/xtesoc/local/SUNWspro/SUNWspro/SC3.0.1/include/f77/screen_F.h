#ifndef screen_F_DEFINED
#define screen_F_DEFINED

C   derived from @(#)screen.h 20.35 91/10/17 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "base_F.h"
#include "generic_F.h"
#include "attr_F.h"
#include "pkg_public_F.h"
C 
C  * PUBLIC definitions
C  
      Xv_pkg_ptr SCREEN
#define Xv_Screen Xv_opaque
#define Xv_screen Xv_opaque
C   Public attributes  
      INTEGER SCREEN_NUMBER
      PARAMETER (SCREEN_NUMBER = 1175062529)
      INTEGER SCREEN_SERVER
      PARAMETER (SCREEN_SERVER = 1175390721)
#define Screen_attr Attr_attribute
C Define the different types of GC available in the GC list 
      INTEGER SCREEN_SET_GC
      PARAMETER (SCREEN_SET_GC = 0)
      INTEGER SCREEN_CLR_GC
      PARAMETER (SCREEN_CLR_GC = 1)
      INTEGER SCREEN_TEXT_GC
      PARAMETER (SCREEN_TEXT_GC = 2)
      INTEGER SCREEN_BOLD_GC
      PARAMETER (SCREEN_BOLD_GC = 3)
      INTEGER SCREEN_GLYPH_GC
      PARAMETER (SCREEN_GLYPH_GC = 4)
      INTEGER SCREEN_INACTIVE_GC
      PARAMETER (SCREEN_INACTIVE_GC = 5)
      INTEGER SCREEN_DIM_GC
      PARAMETER (SCREEN_DIM_GC = 6)
      INTEGER SCREEN_INVERT_GC
      PARAMETER (SCREEN_INVERT_GC = 7)
      INTEGER SCREEN_NONSTD_GC
      PARAMETER (SCREEN_NONSTD_GC = 8)
C  Color or non-standard font 
      INTEGER SCREEN_RUBBERBAND_GC
      PARAMETER (SCREEN_RUBBERBAND_GC = 9)
      INTEGER SCREEN_OLGC_LIST_SIZE
      PARAMETER (SCREEN_OLGC_LIST_SIZE = 10)
      STRUCTURE /Xv_screen_struct/
          RECORD /Xv_generic_struct/ parent
          Xv_opaque private_data
          END STRUCTURE
      COMMON /screen_globals/   SCREEN
#endif screen_F.

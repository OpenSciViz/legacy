#ifndef seln_F_DEFINED
#define seln_F_DEFINED

C   derived from @(#)seln.h 20.11 91/05/28  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  ***********************************************************************
C  *			Include Files
C  ***********************************************************************
C  
#include "sel_svc_F.h"
#include "sel_attrs_F.h"
#endif seln_F.

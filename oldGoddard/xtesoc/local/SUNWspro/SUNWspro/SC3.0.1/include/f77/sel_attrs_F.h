#ifndef sel_attrs_F_DEFINED
#define sel_attrs_F_DEFINED

C   derived from @(#)sel_attrs.h 20.20 92/09/14		
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C 
C  * Public Enumerations 
C  
C 
C  *	Attributes of selections
C  * 	The numbering scheme has to match the scheme of Sunview selection_attributes.h
C  
C   Public Attributes  
      INTEGER SELN_REQ_BYTESIZE
      PARAMETER (SELN_REQ_BYTESIZE = 1191249921)
      INTEGER SELN_REQ_COMMIT_PENDING_DELETE
      PARAMETER (SELN_REQ_COMMIT_PENDING_DELETE = 1195444768)
      INTEGER SELN_REQ_CONTENTS_ASCII
      PARAMETER (SELN_REQ_CONTENTS_ASCII = 1191348545)
      INTEGER SELN_REQ_CONTENTS_PIECES
      PARAMETER (SELN_REQ_CONTENTS_PIECES = 1191414081)
      INTEGER SELN_REQ_DELETE
      PARAMETER (SELN_REQ_DELETE = 1195510304)
      INTEGER SELN_REQ_END_REQUEST
      PARAMETER (SELN_REQ_END_REQUEST = 1207765536)
      INTEGER SELN_REQ_FAILED
      PARAMETER (SELN_REQ_FAILED = 1207896065)
      INTEGER SELN_REQ_FAKE_LEVEL
      PARAMETER (SELN_REQ_FAKE_LEVEL = 1197606913)
      INTEGER SELN_REQ_FILE_NAME
      PARAMETER (SELN_REQ_FILE_NAME = 1191807297)
      INTEGER SELN_REQ_FIRST
      PARAMETER (SELN_REQ_FIRST = 1191446529)
      INTEGER SELN_REQ_FIRST_UNIT
      PARAMETER (SELN_REQ_FIRST_UNIT = 1191512065)
      INTEGER SELN_REQ_LAST
      PARAMETER (SELN_REQ_LAST = 1191577601)
      INTEGER SELN_REQ_LAST_UNIT
      PARAMETER (SELN_REQ_LAST_UNIT = 1191643137)
      INTEGER SELN_REQ_LEVEL
      PARAMETER (SELN_REQ_LEVEL = 1191708673)
      INTEGER SELN_REQ_RESTORE
      PARAMETER (SELN_REQ_RESTORE = 1195575840)
      INTEGER SELN_REQ_SET_LEVEL
      PARAMETER (SELN_REQ_SET_LEVEL = 1197672449)
      INTEGER SELN_REQ_UNKNOWN
      PARAMETER (SELN_REQ_UNKNOWN = 1207830529)
      INTEGER SELN_REQ_YIELD
      PARAMETER (SELN_REQ_YIELD = 1197541665)
#define Seln_attribute Attr_attribute
C  Meta-levels available for use with SELN_REQ_FAKE/SET_LEVEL.
C  *	SELN_LEVEL_LINE is "text line bounded by newline characters,
C  *			    including only the terminating newline"
C  
      INTEGER SELN_LEVEL_FIRST
      PARAMETER (SELN_LEVEL_FIRST = X'40000001')
      INTEGER SELN_LEVEL_LINE
      PARAMETER (SELN_LEVEL_LINE = X'40000101')
      INTEGER SELN_LEVEL_ALL
      PARAMETER (SELN_LEVEL_ALL = X'40008001')
      INTEGER SELN_LEVEL_NEXT
      PARAMETER (SELN_LEVEL_NEXT = X'4000F001')
      INTEGER SELN_LEVEL_PREVIOUS
      PARAMETER (SELN_LEVEL_PREVIOUS = X'4000F002')
#define Seln_level INTEGER*4
#endif sel_attrs_F.

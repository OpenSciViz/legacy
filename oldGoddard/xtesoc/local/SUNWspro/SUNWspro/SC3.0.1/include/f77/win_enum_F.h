#ifndef win_enum_F_DEFINED
#define win_enum_F_DEFINED

C  derived from @(#)win_enum.h 20.11 91/05/28 SMI 
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "rect_F.h"
C 
C  * Values for 'flags' field in struct Win_enum_node 
C  
      INTEGER WIN_NODE_INSERTED
      PARAMETER (WIN_NODE_INSERTED = 1)
      INTEGER WIN_NODE_OPEN
      PARAMETER (WIN_NODE_OPEN = 2)
      INTEGER WIN_NODE_IS_ROOT
      PARAMETER (WIN_NODE_IS_ROOT = 4)
#define Window_handle INTEGER
      INTEGER Enum_Normal,Enum_Succeed,Enum_Fail
      PARAMETER ( Enum_Normal=0,  Enum_Succeed=1,  Enum_Fail=2)
#define Win_enum_result INTEGER*4
#define Enumerator INTEGER
C 
C  * For a fast window enumerator in user-space 
C  
      STRUCTURE /Win_enum_node/
          Byte me
          Byte parent
          Byte upper_sib
          Byte lowest_kid
          INTEGER  flags
          RECORD /Rect/ open_rect
          RECORD /Rect/ icon_rect
          END STRUCTURE
      STRUCTURE /Win_tree_layer/
          INTEGER  bytecount
          INTEGER*4 buffer
          END STRUCTURE
#endif win_enum_F.

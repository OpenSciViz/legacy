#ifndef canvas_F_DEFINED
#define canvas_F_DEFINED

C       Derived from @(#)canvas.h 20.36 92/08/25 SMI      
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "attrol_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "openwin_F.h"
#include "pixwin_F.h"
#include "rectlist_F.h"
#include "window_F.h"
#include "win_input_F.h"
C 
C  * PUBLIC definition
C  
      Xv_pkg_ptr CANVAS
      Xv_pkg_ptr CANVAS_VIEW
      Xv_pkg_ptr CANVAS_PAINT_WINDOW
      INTEGER CANVAS_AUTO_CLEAR
      PARAMETER (CANVAS_AUTO_CLEAR = OPENWIN_AUTO_CLEAR)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structs
C  ***********************************************************************
C  
#define Canvas Xv_opaque
#define Canvas_view Xv_opaque
#define Canvas_paint_window Xv_opaque
C 
C  * Enumerations 
C  
C   Public attributes  
      INTEGER CANVAS_AUTO_EXPAND
      PARAMETER (CANVAS_AUTO_EXPAND = 1325467905)
      INTEGER CANVAS_AUTO_SHRINK
      PARAMETER (CANVAS_AUTO_SHRINK = 1325730049)
      INTEGER CANVAS_FIXED_IMAGE
      PARAMETER (CANVAS_FIXED_IMAGE = 1326057729)
      INTEGER CANVAS_HEIGHT
      PARAMETER (CANVAS_HEIGHT = 1326385281)
      INTEGER CANVAS_MIN_PAINT_HEIGHT
      PARAMETER (CANVAS_MIN_PAINT_HEIGHT = 1326712961)
      INTEGER CANVAS_MIN_PAINT_WIDTH
      PARAMETER (CANVAS_MIN_PAINT_WIDTH = 1327040577)
      INTEGER CANVAS_NTH_PAINT_WINDOW
      PARAMETER (CANVAS_NTH_PAINT_WINDOW = 1327368705)
      INTEGER CANVAS_REPAINT_PROC
      PARAMETER (CANVAS_REPAINT_PROC = 1327696481)
      INTEGER CANVAS_RESIZE_PROC
      PARAMETER (CANVAS_RESIZE_PROC = 1328024161)
      INTEGER CANVAS_RETAINED
      PARAMETER (CANVAS_RETAINED = 1328351489)
      INTEGER CANVAS_VIEW_MARGIN
      PARAMETER (CANVAS_VIEW_MARGIN = 1328678913)
      INTEGER CANVAS_VIEWABLE_RECT
      PARAMETER (CANVAS_VIEWABLE_RECT = 1329007073)
      INTEGER CANVAS_WIDTH
      PARAMETER (CANVAS_WIDTH = 1329334337)
      INTEGER CANVAS_X_PAINT_WINDOW
      PARAMETER (CANVAS_X_PAINT_WINDOW = 1329662209)
      INTEGER CANVAS_PAINTWINDOW_ATTRS
      PARAMETER (CANVAS_PAINTWINDOW_ATTRS = 1330006593)
      INTEGER CANVAS_NO_CLIPPING
      PARAMETER (CANVAS_NO_CLIPPING = 1330317569)
      INTEGER CANVAS_CMS_REPAINT
      PARAMETER (CANVAS_CMS_REPAINT = 1330645249)
C  takes on the values CANVAS_AUTO_EXPAND .. CANVAS_CMS_REPAINT 
#define Canvas_attribute Attr_attribute
      INTEGER CANVAS_VIEW_PAINT_WINDOW
      PARAMETER (CANVAS_VIEW_PAINT_WINDOW = 1560349185)
      INTEGER CANVAS_VIEW_CANVAS_WINDOW
      PARAMETER (CANVAS_VIEW_CANVAS_WINDOW = 1560414721)
C  takes on the values  CANVAS_VIEW_PAINT_WINDOW .. CANVAS_VIEW_CANVAS_WINDOW 
#define Canvas_view_attribute Attr_attribute
      INTEGER CANVAS_PAINT_CANVAS_WINDOW
      PARAMETER (CANVAS_PAINT_CANVAS_WINDOW = 1577126401)
      INTEGER CANVAS_PAINT_VIEW_WINDOW
      PARAMETER (CANVAS_PAINT_VIEW_WINDOW = 1577191937)
C  takes on the values  CANVAS_PAINT_CANVAS_WINDOW .. CANVAS_PAINT_VIEW_WINDOW 
#define Canvas_paint_attribute Attr_attribute
C 
C  * For SunView 1 compatibility 
C  
      INTEGER CANVAS_TYPE
      PARAMETER (CANVAS_TYPE = ATTR_PKG_CANVAS)
      INTEGER CANVAS_MARGIN
      PARAMETER (CANVAS_MARGIN = CANVAS_VIEW_MARGIN)
C 
C  * Structures 
C  
      STRUCTURE /Xv_canvas/
          RECORD /Xv_openwin/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_canvas_view/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_canvas_pw/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Canvas_repaint_proc INTEGER
#define Canvas_repaint_procp INTEGER*4
#define Canvas_resize_proc INTEGER
#define Canvas_resize_procp INTEGER*4
C 
C  * Functions
C  
      Event_ptr  canvas_event
      EXTERNAL canvas_event
C       Canvas canvas_obj
C       Event_ptr eventp

      Event_ptr  canvas_window_event
      EXTERNAL canvas_window_event
C       Canvas canvas_obj
C       Event_ptr eventp

      Pixwin_ptr  canvas_pixwin
      EXTERNAL canvas_pixwin
C       Canvas canvas

      Xv_Window  canvas_paint_window
      EXTERNAL canvas_paint_window
C       Canvas canvas

      COMMON /canvas_globals/   CANVAS_PAINT_WINDOW, CANVAS_VIEW, CANVAS
#endif canvas_F.

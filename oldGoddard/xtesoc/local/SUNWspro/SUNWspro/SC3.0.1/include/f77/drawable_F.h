#ifndef drawable_F_DEFINED
#define drawable_F_DEFINED

C       derived from @(#)drawable.h 20.13 91/05/28 SMI      
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Interface to generic attributes of Drawable objects, where "drawable"
C  * is defined by X server.  This is currently an implementation concept.
C  
C 
C  ***********************************************************************
C  *			Include files
C  ***********************************************************************
C  
#include "attr_F.h"
#include "base_F.h"
#include "generic_F.h"
#include "pkg_public_F.h"
C 
C  ***********************************************************************
C  *			Definitions and Macros
C  ***********************************************************************
C  
C 
C  * PRIVATE #defines 
C  
      INTEGER*4 XV_DRAWABLE_OBJECT
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
#define Xv_Drawable Xv_opaque
#define Xv_drawable Xv_opaque
C 
C 	 * Private Attributes
C 	 
      INTEGER DRAWABLE_INFO
      PARAMETER (DRAWABLE_INFO = 1113852417)
#define Drawable_attr Attr_attribute
      STRUCTURE /Xv_drawable_struct/
          RECORD /Xv_generic_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      COMMON /drawable_globals/   XV_DRAWABLE_OBJECT
#endif drawable_F.

#ifndef cms_F_DEFINED
#define cms_F_DEFINED

C  derived from @(#)cms.h 20.12  89/08/17 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "pkg_public_F.h"
#include "generic_F.h"
#include "attr_F.h"
#include "base_F.h"
C 
C  * PUBLIC #defines
C  
      INTEGER CMS_NAMESIZE
      PARAMETER (CMS_NAMESIZE = 25)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
      INTEGER XV_DEFAULT_CMS_SIZE
      PARAMETER (XV_DEFAULT_CMS_SIZE = 2)
      INTEGER CMS_CONTROL_BG1
      PARAMETER (CMS_CONTROL_BG1 = 0)
      INTEGER CMS_CONTROL_BG2
      PARAMETER (CMS_CONTROL_BG2 = 1)
      INTEGER CMS_CONTROL_BG3
      PARAMETER (CMS_CONTROL_BG3 = 2)
      INTEGER CMS_CONTROL_HIGHLIGHT
      PARAMETER (CMS_CONTROL_HIGHLIGHT = 3)
      INTEGER CMS_CONTROL_COLORS
      PARAMETER (CMS_CONTROL_COLORS = 4)
#define Cms Xv_opaque
      INTEGER XV_STATIC_CMS
      PARAMETER (XV_STATIC_CMS = 1)
      INTEGER XV_DYNAMIC_CMS
      PARAMETER (XV_DYNAMIC_CMS = 2)
C  takes on the values XV_STATIC_CMS ..XV_DYNAMIC_CMS 
#define Cms_type INTEGER
      INTEGER CMS_TYPE
      PARAMETER (CMS_TYPE = 1292175649)
      INTEGER CMS_SIZE
      PARAMETER (CMS_SIZE = 1292503041)
      INTEGER CMS_COLORS
      PARAMETER (CMS_COLORS = 1292831393)
      INTEGER CMS_NAMED_COLORS
      PARAMETER (CMS_NAMED_COLORS = 1292929377)
      INTEGER CMS_X_COLORS
      PARAMETER (CMS_X_COLORS = 1292962305)
      INTEGER CMS_INDEX
      PARAMETER (CMS_INDEX = 1293158401)
      INTEGER CMS_COLOR_COUNT
      PARAMETER (CMS_COLOR_COUNT = 1293486081)
      INTEGER CMS_SCREEN
      PARAMETER (CMS_SCREEN = 1293814273)
      INTEGER CMS_INDEX_TABLE
      PARAMETER (CMS_INDEX_TABLE = 1294141953)
      INTEGER CMS_FOREGROUND_PIXEL
      PARAMETER (CMS_FOREGROUND_PIXEL = 1294469121)
      INTEGER CMS_BACKGROUND_PIXEL
      PARAMETER (CMS_BACKGROUND_PIXEL = 1294796801)
      INTEGER CMS_PIXEL
      PARAMETER (CMS_PIXEL = 1295124481)
      INTEGER CMS_CMS_DATA
      PARAMETER (CMS_CMS_DATA = 1295452673)
      INTEGER CMS_CONTROL_CMS
      PARAMETER (CMS_CONTROL_CMS = 1295780097)
#define Cms_attribute Attr_attribute
      INTEGER CMS_NAME
      PARAMETER (CMS_NAME = XV_NAME)
      STRUCTURE /Xv_cms_struct/
          RECORD /Xv_generic_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_cms_struct_ptr INTEGER*4
      STRUCTURE /Xv_cmsdata/
          Cms_type type
          INTEGER*2  size
          INTEGER*2  index
          INTEGER*2  rgb_count
          INTEGER*4 red
          INTEGER*4 green
          INTEGER*4 blue
          END STRUCTURE
#define Xv_cmsdata_ptr INTEGER*4
      STRUCTURE /Xv_singlecolor/
          Byte red, green, blue
          END STRUCTURE
#define Xv_singlecolor_ptr INTEGER*4
#define Xv_Singlecolor RECORD /Xv_singlecolor/
C 
C  * do we really need this ? check and remove.
C  
      STRUCTURE /Xv_Color_info/
          Cms cms
          INTEGER  cms_fg
          INTEGER  cms_bg
          END STRUCTURE
C  Takes on the values XV_STATIC_CMAP .. XV_DYNAMIC_CMAP 
#define Cmap_type INTEGER
      Xv_pkg_ptr CMS
      COMMON /cms_globals/   CMS
#endif cms_F.

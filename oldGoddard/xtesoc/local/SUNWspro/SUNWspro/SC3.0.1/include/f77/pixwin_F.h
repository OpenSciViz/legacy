#ifndef pixwin_F_DEFINED
#define pixwin_F_DEFINED

C  derived from @(#)pixwin.h 20.28 91/10/30 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "base_F.h"
#include "pixrect_F.h"
#include "rect_F.h"
C 
C  ***********************************************************************
C  *		Constants, Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
      INTEGER PIX_MAX_PLANE_GROUPS
      PARAMETER (PIX_MAX_PLANE_GROUPS = 1)
      INTEGER PW_PIXEL_CACHE_NULL
      PARAMETER (PW_PIXEL_CACHE_NULL = 0)
C  Pw_pixel_cache_ptr 
      INTEGER XV_DEFAULT_FG_BG
      PARAMETER (XV_DEFAULT_FG_BG = 0)
      INTEGER XV_INVERTED_FG_BG
      PARAMETER (XV_INVERTED_FG_BG = 1)
      INTEGER XV_USE_OP_FG
      PARAMETER (XV_USE_OP_FG = 0)
      INTEGER XV_USE_CMS_FG
      PARAMETER (XV_USE_CMS_FG = 1)
      INTEGER PW_ROP_NULL_SRC
      PARAMETER (PW_ROP_NULL_SRC = 0)
      INTEGER PW_VECTOR
      PARAMETER (PW_VECTOR = 1)
      INTEGER PW_LINE
      PARAMETER (PW_LINE = 2)
      INTEGER PW_POLYLINE
      PARAMETER (PW_POLYLINE = 3)
      INTEGER PW_TEXT
      PARAMETER (PW_TEXT = 4)
      INTEGER PW_STENCIL
      PARAMETER (PW_STENCIL = 5)
      INTEGER PW_POLYGON2
      PARAMETER (PW_POLYGON2 = 6)
      INTEGER PW_POLYPOINT
      PARAMETER (PW_POLYPOINT = 7)
      INTEGER PW_ROP
      PARAMETER (PW_ROP = 8)
      INTEGER PW_REPLROP
      PARAMETER (PW_REPLROP = 9)
      INTEGER PW_NUM_OPS
      PARAMETER (PW_NUM_OPS = 10)
#define Pw_pixel_cache_ptr INTEGER*4
      STRUCTURE /Pw_pixel_cache/
          RECORD /Rect/ r
          Pixrect_ptr plane_group (0 : PIX_MAX_PLANE_GROUPS)
      
          END STRUCTURE
      STRUCTURE /Pixwin/
          CHARACTER  dummy
          END STRUCTURE
#define Pixwin_ptr INTEGER*4
C 
C  ***********************************************************************
C  *		Procedure
C  ***********************************************************************
C  
C 
C  *    SV1 compatibility definitions.
C  
      EXTERNAL pw_copy
C       Xv_opaque window
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixwin_ptr spw
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pw_read
C       Pixrect_ptr dpr
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Xv_opaque window
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pw_replrop
C       Xv_opaque window
C       INTEGER xw
C       INTEGER yw
C       INTEGER width
C       INTEGER height
C       INTEGER op
C       Pixrect_ptr pr
C       INTEGER xr
C       INTEGER yr

      EXTERNAL pw_setcmsname
C       Xv_opaque window
C       CHARACTER name

      EXTERNAL pw_rop
C       Xv_opaque window
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixrect_ptr sp
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pw_stencil
C       Xv_opaque window
C       INTEGER x
C       INTEGER y
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixrect_ptr stpr
C       INTEGER stx
C       INTEGER sty
C       Pixrect_ptr spr
C       INTEGER sy
C       INTEGER sx

      EXTERNAL pw_text
C       Xv_opaque window
C       INTEGER xbasew
C       INTEGER ybasew
C       INTEGER op
C       Xv_opaque font
C       CHARACTER s

      EXTERNAL pw_ttext
C       Xv_opaque window
C       INTEGER xbasew
C       INTEGER ybasew
C       INTEGER op
C       Xv_opaque font
C       CHARACTER s

      EXTERNAL pw_vector
C       Xv_opaque window
C       INTEGER x0
C       INTEGER y0
C       INTEGER x1
C       INTEGER y1
C       INTEGER op
C       INTEGER cms_index

      EXTERNAL pw_write
C       Xv_opaque window
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pw_writebackground
C       Xv_opaque window
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op

C 
C  * PUBLIC functions 
C  
      Pw_pixel_cache_ptr  pw_save_pixels
      EXTERNAL pw_save_pixels
C       Xv_opaque pw
C       Rect_ptr rect

      EXTERNAL pw_restore_pixels
C       Xv_opaque ps
C       Pw_pixel_cache_ptr pc

C 
C  * For SunView Pixrect/Pixwin Graphics Compatibility
C  
      INTEGER  xv_read
      EXTERNAL xv_read
C       Pixrect_ptr pr
C       INTEGER op
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       Xv_opaque window
C       INTEGER sx
C       INTEGER sy

      INTEGER  xv_replrop
      EXTERNAL xv_replrop
C       Xv_opaque window
C       INTEGER op
C       INTEGER xw
C       INTEGER yw
C       INTEGER width
C       INTEGER height
C       Pixrect_ptr pr
C       INTEGER xr
C       INTEGER yr

      INTEGER  xv_rop
      EXTERNAL xv_rop
C       Xv_opaque window
C       INTEGER op
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       Pixrect_ptr pr
C       INTEGER xr
C       INTEGER yr

      INTEGER  xv_stencil
      EXTERNAL xv_stencil
C       Xv_opaque window
C       INTEGER op
C       INTEGER dx
C       INTEGER dy
C       INTEGER width
C       INTEGER height
C       Pixrect_ptr stpr
C       INTEGER stx
C       INTEGER sty
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

      INTEGER  xv_text
      EXTERNAL xv_text
C       Xv_opaque window
C       INTEGER op
C       INTEGER xbasew
C       INTEGER ybasew
C       Xv_opaque font
C       CHARACTER str

      INTEGER  xv_ttext
      EXTERNAL xv_ttext
C       Xv_opaque window
C       INTEGER xbasew
C       INTEGER ybasew
C       INTEGER op
C       Xv_opaque font
C       CHARACTER str

      INTEGER  xv_vector
      EXTERNAL xv_vector
C       Xv_opaque window
C       INTEGER x0
C       INTEGER y0
C       INTEGER x1
C       INTEGER y1
C       INTEGER op
C       INTEGER cms_index

      INTEGER  pw_batchrop
      EXTERNAL pw_batchrop
C       Pixwin_ptr pw
C       INTEGER x
C       INTEGER y
C       INTEGER op
C       RECORD /Pr_prpos/ sbp
C       INTEGER m

      INTEGER  pw_get
      EXTERNAL pw_get
C       Xv_opaque xv_drawable
C       INTEGER x
C       INTEGER y

      EXTERNAL pw_put
C       Xv_opaque pw
C       INTEGER x
C       INTEGER y
C       INTEGER val

      EXTERNAL pw_putattributes
C       Xv_opaque pw
C       INTEGER planes

      EXTERNAL pw_getattributes
C       Xv_opaque pw
C       INTEGER planes

      EXTERNAL pw_putcolormap
C       Xv_opaque pw
C       INTEGER index
C       INTEGER count
C       Byte red
C       Byte grn
C       Byte blu

      EXTERNAL pw_getcolormap
C       Xv_opaque pw
C       INTEGER ind
C       INTEGER cnt
C       Byte red
C       Byte grn
C       Byte blu

      Pixfont_ptr  pw_pfsysopen
      EXTERNAL pw_pfsysopen
      INTEGER  pw_pfsysclose
      EXTERNAL pw_pfsysclose
      INTEGER  pw_char
      EXTERNAL pw_char
C       Xv_opaque pw
C       INTEGER xw
C       INTEGER yw
C       INTEGER op
C       Pixfont_ptr pixfont
C       INTEGER c

      INTEGER  xv_glyph_char
      EXTERNAL xv_glyph_char
C       Xv_opaque window
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       Pixfont_ptr pixfont
C       INTEGER c
C       INTEGER color_index

      INTEGER  pw_getcmsname
      EXTERNAL pw_getcmsname
C       Xv_opaque pw
C       Char_ptr name

      INTEGER  pw_line
      EXTERNAL pw_line
C       Pixwin_ptr pw
C       INTEGER x0
C       INTEGER y0
C       INTEGER x1
C       INTEGER y1
C       Pr_brush_ptr bursh
C       Pr_texture_ptr tex
C       INTEGER op

      INTEGER  pw_polygon_2
      EXTERNAL pw_polygon_2
C       Pixwin_ptr pw
C       INTEGER dx
C       INTEGER dy
C       INTEGER nbds
C       INTEGER npts
C       Pr_pos_ptr vlist
C       INTEGER op
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

      INTEGER  pw_polyline
      EXTERNAL pw_polyline
C       Xv_opaque pw
C       INTEGER dx
C       INTEGER dy
C       INTEGER npts
C       Pr_pos_ptr ptlist
C       Char_ptr mvlist
C       Pr_brush_ptr brush
C       Pr_texture_ptr tex
C       INTEGER op

      INTEGER  pw_polypoint
      EXTERNAL pw_polypoint
C       Xv_opaque pw
C       INTEGER dx
C       INTEGER dy
C       INTEGER npts
C       Pr_pos_ptr ptlist
C       INTEGER op

      INTEGER  pw_traprop
      EXTERNAL pw_traprop
C       Xv_opaque pw
C       INTEGER dx
C       INTEGER dy
C       RECORD /Pr_trap/ trap
C       INTEGER op
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

#endif pixwin_F.

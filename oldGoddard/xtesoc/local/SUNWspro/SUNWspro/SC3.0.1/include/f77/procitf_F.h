/*  @(#)procitf.h	1.3     14 Dec 1993  */

#ifndef procitf_F_DEFINED
#define procitf_F_DEFINED

C
C     (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C     pending in the U.S. and foreign countries. See LEGAL NOTICE
C     file for terms of the license.
C
C     This header file contains list of routines which will
C     generate interface routines for all procedure types which
C     are passed to Xview.
C     The input argument is a Fortran subroutine.
C     The output is the addres of an interface routine that will
C     call the Fortran routine with the arguments properly mapped.
C
C     NOTE. All routines are linked to one general routine 'proc_itf'.
C
#include "stddefs_F.h"

C
C     General routine which will generate interface routines
C     for all procedure types.
C
      ProcPointer proc_itf
      EXTERNAL proc_itf
C       ProcPointer proc

#define canvas_repaint_itf proc_itf
#define canvas_resize_itf proc_itf
#define frame_done_itf proc_itf
#define notify_copy_itf proc_itf
#define notify_release_itf proc_itf
#define notify_itimer_itf proc_itf
#define notify_destroy_itf proc_itf
#define notify_wait3_itf proc_itf
#define notify_input_itf proc_itf
#define notify_output_itf proc_itf
#define notify_exception_itf proc_itf
#define notify_signal_itf proc_itf
#define notify_event_itf proc_itf
#define notify_interposer_itf proc_itf
#define notify_scheduler_itf proc_itf
#define notify_prioritizer_itf proc_itf
#define menu_done_itf proc_itf
#define menu_gen_itf proc_itf
#define menu_notify_itf proc_itf
#define menu_pin_itf proc_itf
#define menu_action_itf proc_itf
#define openwin_split_destroy_itf proc_itf
#define panel_background_itf proc_itf
#define panel_event_itf proc_itf
#define panel_repaint_itf proc_itf
#define panel_notify_itf proc_itf
#define textsw_notify_itf proc_itf
#define win_event_itf proc_itf

#endif procitf_F.

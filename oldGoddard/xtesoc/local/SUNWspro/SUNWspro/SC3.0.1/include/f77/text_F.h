#ifndef text_F_DEFINED
#define text_F_DEFINED

C   derived from @(#)text.h 20.11 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license
C  
C 
C  * The entire contents of this file are for
C  * SunView 1 compatibility only -- THIS IS GOING AWAY 
C  
#include "attrol_F.h"
#include "textsw_F.h"
      INTEGER TEXT_TYPE
      PARAMETER (TEXT_TYPE = ATTR_PKG_TEXTSW)
#define Text Textsw
#endif text_F.

#ifndef tty_F_DEFINED
#define tty_F_DEFINED

C 
C  * derived from @(#)tty.h 20.16 91/05/28 SMI
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "attrol_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "openwin_F.h"
#include "window_F.h"
      Xv_pkg_ptr TTY
C 
C  * Data type declaration for ttysw view 
C  
      INTEGER TTY_VIEW_TYPE
      PARAMETER (TTY_VIEW_TYPE = ATTR_PKG_TTY_VIEW)
      Xv_pkg_ptr TTY_VIEW
C 
C  * Data type declaration for ttysw folio 
C  
      INTEGER TTY_TYPE
      PARAMETER (TTY_TYPE = ATTR_PKG_TTY)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures 
C  ***********************************************************************
C  
#define Tty Xv_opaque
#define Tty_view Xv_opaque
      STRUCTURE /Xv_tty/
          RECORD /Xv_openwin/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_tty_view/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
C   Public attributes  
      INTEGER TTY_ARGV
      PARAMETER (TTY_ARGV = 1476463105)
      INTEGER TTY_CONSOLE
      PARAMETER (TTY_CONSOLE = 1476724993)
      INTEGER TTY_INPUT
      PARAMETER (TTY_INPUT = 1477052931)
      INTEGER TTY_OUTPUT
      PARAMETER (TTY_OUTPUT = 1477380611)
      INTEGER TTY_PAGE_MODE
      PARAMETER (TTY_PAGE_MODE = 1477708033)
      INTEGER TTY_QUIT_ON_CHILD_DEATH
      PARAMETER (TTY_QUIT_ON_CHILD_DEATH = 1478035713)
#define Tty_attribute Attr_attribute
C 
C  * 		Escape sequences recognized by TTY subwindows
C  *
C  *      \E[1t           - open
C  *      \E[2t           - close (become iconic)
C  *      \E[3t           - move, with interactive feedback
C  *      \E[3;TOP;LEFTt  - move, TOP LEFT in pixels
C  *      \E[4t           - stretch, with interactive feedback
C  *      \E[4;ROWS;COLSt - stretch, ROWS COLS in pixels
C  *      \E[5t           - top (expose)
C  *      \E[6t           - bottom (hide)
C  *      \E[7t           - refresh
C  *      \E[8;ROWS;COLSt - stretch, ROWS COLS in characters
C  *      \E[11t          - report open or iconic, sends \E[1t or \E[2t
C  *      \E[13t          - report position, sends \E[3;TOP;LEFTt
C  *      \E[14t          - report size in pixels, sends \E[8;ROWS;COLSt
C  *      \E[18t          - report size in chars, sends \E[4;ROWS;COLSt
C  *      \E[20t          - report icon label, sends \E]Llabel\E\
C  *      \E[21t          - report tool label, sends \E]llabel\E\
C  *      \E]l<text>\E\   - set tool label to <text>
C  *      \E]I<file>\E\   - set icon file to <file>
C  *      \E]L<label>\E\  - set icon label to <label>
C  
      COMMON /tty_globals/   TTY_VIEW, TTY
#endif tty_F.

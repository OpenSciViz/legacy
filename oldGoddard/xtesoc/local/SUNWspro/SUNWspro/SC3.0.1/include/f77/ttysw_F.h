#ifndef ttysw_F_DEFINED
#define ttysw_F_DEFINED

C 	derived from @(#)ttysw.h 20.14 91/07/08 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * A tty subwindow is a subwindow type that is used to provide a
C  * terminal emulation for teletype based programs.
C  *
C  * The caller of ttysw_start typically waits for the child process to die
C  * before exiting.
C  *
C  
#include "tty_F.h"
C  ???? this is not needed ? 
C  
C  * options - controlled by ttysw_getopt(), ttysw_setopt
C  * The values of the #define's are wired into aliases, shell-scripts,
C  * etc. and should not be changed!
C  
      INTEGER TTYOPT_PAGEMODE
      PARAMETER (TTYOPT_PAGEMODE = 1)
      INTEGER TTYOPT_SELSVC
      PARAMETER (TTYOPT_SELSVC = 3)
      INTEGER TTYOPT_TEXT
      PARAMETER (TTYOPT_TEXT = 4)
C  TERMSW 
C 
C  * styles for rendering boldface characters 
C  
      INTEGER TTYSW_BOLD_NONE
      PARAMETER (TTYSW_BOLD_NONE = X'0')
      INTEGER TTYSW_BOLD_OFFSET_X
      PARAMETER (TTYSW_BOLD_OFFSET_X = X'1')
      INTEGER TTYSW_BOLD_OFFSET_Y
      PARAMETER (TTYSW_BOLD_OFFSET_Y = X'2')
      INTEGER TTYSW_BOLD_OFFSET_XY
      PARAMETER (TTYSW_BOLD_OFFSET_XY = X'4')
      INTEGER TTYSW_BOLD_INVERT
      PARAMETER (TTYSW_BOLD_INVERT = X'8')
      INTEGER TTYSW_BOLD_MAX
      PARAMETER (TTYSW_BOLD_MAX = X'8')
C 
C  * Modes for invert and underline 
C  
      INTEGER TTYSW_ENABLE
      PARAMETER (TTYSW_ENABLE = X'0')
      INTEGER TTYSW_DISABLE
      PARAMETER (TTYSW_DISABLE = X'1')
      INTEGER TTYSW_SAME_AS_BOLD
      PARAMETER (TTYSW_SAME_AS_BOLD = X'2')
#define Ttysubwindow INTEGER
      INTEGER  ttysw_input
      EXTERNAL ttysw_input
C       Tty tty
C       CHARACTER buf
C       INTEGER len

      INTEGER  ttysw_output
      EXTERNAL ttysw_output
C       Tty tty
C       CHARACTER buf
C       INTEGER len

C 
C  * for compatibility with pre-SunView 1 code
C  
      EXTERNAL ttysw_becomeconsole
C       Ttysubwindow ttysw

#endif ttysw_F.

#ifndef sel_svc_F_DEFINED
#define sel_svc_F_DEFINED

C       derived from @(#)sel_svc.h 20.33 91/05/30  SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "win_input_F.h"
#include "server_F.h"
#include "sel_attrs_F.h"
      INTEGER SELN_FUNCTION_WORD_COUNT
      PARAMETER (SELN_FUNCTION_WORD_COUNT = 8)
C   256 bits should last a while  
      INTEGER SELN_BUFSIZE
      PARAMETER (SELN_BUFSIZE = 1892)
C 
C  * PUBLIC Definitions
C  
      EXTERNAL SELN_REPORT
C       RECORD /Event/ event

C **
C  ***  Definitions taken from /usr/include/netinet/in.h
C  **
      STRUCTURE /in_addr/
          INTEGER  S_addr
          END STRUCTURE
      STRUCTURE /sockaddr_in/
          INTEGER*2  sin_family
          INTEGER*2  sin_port
          RECORD /in_addr/ sin_addr
          CHARACTER sin_zero (0 : 7)
      
          END STRUCTURE
C **
C  ***  End of definitions taken from /usr/include/netinet/in.h
C  **
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C 
C  * Seln_client:	opaque handle returned to client from create    
C  
#define Seln_client INTEGER
C 
C  * PUBLIC enums 
C  
C 
C  * Seln_result:	Standard return codes	
C  
C  Values of type Seln_result 
      INTEGER SELN_FAILED
      PARAMETER (SELN_FAILED = 0)
      INTEGER SELN_SUCCESS
      PARAMETER (SELN_SUCCESS = 1)
      INTEGER SELN_NON_EXIST
      PARAMETER (SELN_NON_EXIST = 2)
      INTEGER SELN_DIDNT_HAVE
      PARAMETER (SELN_DIDNT_HAVE = 3)
      INTEGER SELN_WRONG_RANK
      PARAMETER (SELN_WRONG_RANK = 4)
      INTEGER SELN_CONTINUED
      PARAMETER (SELN_CONTINUED = 5)
      INTEGER SELN_CANCEL
      PARAMETER (SELN_CANCEL = 6)
      INTEGER SELN_UNRECOGNIZED
      PARAMETER (SELN_UNRECOGNIZED = 7)
      INTEGER SELN_OVER
      PARAMETER (SELN_OVER = 8)
#define Seln_result INTEGER
C 
C  * Seln_rank:	Selection identifiers	
C  
C  Values of type Seln_rank 
      INTEGER SELN_UNKNOWN
      PARAMETER (SELN_UNKNOWN = 0)
      INTEGER SELN_CARET
      PARAMETER (SELN_CARET = 1)
      INTEGER SELN_PRIMARY
      PARAMETER (SELN_PRIMARY = 2)
      INTEGER SELN_SECONDARY
      PARAMETER (SELN_SECONDARY = 3)
      INTEGER SELN_SHELF
      PARAMETER (SELN_SHELF = 4)
      INTEGER SELN_UNSPECIFIED
      PARAMETER (SELN_UNSPECIFIED = 5)
#define Seln_rank INTEGER
C 
C  *	Seln_function:	Modes which affect rank of selection,
C  *	controlled by function-keys or their programmatic equivalents
C  
C  Values of type Seln_function 
      INTEGER SELN_FN_ERROR
      PARAMETER (SELN_FN_ERROR = 0)
      INTEGER SELN_FN_STOP
      PARAMETER (SELN_FN_STOP = 1)
      INTEGER SELN_FN_AGAIN
      PARAMETER (SELN_FN_AGAIN = 2)
      INTEGER SELN_FN_PROPS
      PARAMETER (SELN_FN_PROPS = 3)
      INTEGER SELN_FN_UNDO
      PARAMETER (SELN_FN_UNDO = 4)
      INTEGER SELN_FN_FRONT
      PARAMETER (SELN_FN_FRONT = 5)
      INTEGER SELN_FN_PUT
      PARAMETER (SELN_FN_PUT = 6)
      INTEGER SELN_FN_OPEN
      PARAMETER (SELN_FN_OPEN = 7)
      INTEGER SELN_FN_GET
      PARAMETER (SELN_FN_GET = 8)
      INTEGER SELN_FN_FIND
      PARAMETER (SELN_FN_FIND = 9)
      INTEGER SELN_FN_DELETE
      PARAMETER (SELN_FN_DELETE = 10)
#define Seln_function INTEGER
      INTEGER SELN_FN_COPY
      PARAMETER (SELN_FN_COPY = SELN_FN_PUT)
      INTEGER SELN_FN_PASTE
      PARAMETER (SELN_FN_PASTE = SELN_FN_GET)
      INTEGER SELN_FN_CUT
      PARAMETER (SELN_FN_CUT = SELN_FN_DELETE)
C 
C  *	Seln_state:	States a selection (or its holder) may be in
C  
C  Values of type Seln_function 
      INTEGER SELN_NONE
      PARAMETER (SELN_NONE = 0)
      INTEGER SELN_EXISTS
      PARAMETER (SELN_EXISTS = 1)
      INTEGER SELN_FILE
      PARAMETER (SELN_FILE = 2)
#define Seln_state INTEGER
C 
C  *	Seln_response:	possible appropriate responses to a Seln_function_buffer
C  
C  Values of type Seln_response 
      INTEGER SELN_IGNORE
      PARAMETER (SELN_IGNORE = 0)
      INTEGER SELN_REQUEST
      PARAMETER (SELN_REQUEST = 1)
      INTEGER SELN_FIND
      PARAMETER (SELN_FIND = 2)
      INTEGER SELN_SHELVE
      PARAMETER (SELN_SHELVE = 3)
      INTEGER SELN_DELETE
      PARAMETER (SELN_DELETE = 4)
#define Seln_response INTEGER
C 
C  * PUBLIC structs 
C  
C 
C  * Seln_access for SunView 1 compatibility
C  * (cannot be moved below because Seln_holder depends upon it)
C  
      STRUCTURE /Seln_access/
          INTEGER  pid
          INTEGER  program
          RECORD /sockaddr_in/ tcp_address
          RECORD /sockaddr_in/ udp_address
          Char_ptr client
          END STRUCTURE
      STRUCTURE /Seln_holder/
          Seln_rank rank
          Seln_state state
          RECORD /Seln_access/ access
          END STRUCTURE
#define Seln_holder_ptr INTEGER*4
      STRUCTURE /Seln_holders_all/
          RECORD /Seln_holder/ caret
          RECORD /Seln_holder/ primary
          RECORD /Seln_holder/ secondary
          RECORD /Seln_holder/ shelf
          END STRUCTURE
      STRUCTURE /Seln_function_buffer/
          Seln_function function
          Seln_rank addressee_rank
          RECORD /Seln_holder/ caret
          RECORD /Seln_holder/ primary
          RECORD /Seln_holder/ secondary
          RECORD /Seln_holder/ shelf
          END STRUCTURE
#define Seln_function_buffer_ptr INTEGER*4
      STRUCTURE /Seln_replier_data/
          Char_ptr client_data
          Seln_rank rank
          Char_ptr context
          INTEGER*4 request_pointer
          INTEGER*4 response_pointer
          END STRUCTURE
#define Seln_replier_data_ptr INTEGER*4
      STRUCTURE /Seln_requester/
          ProcPointer consume
          Char_ptr context
          END STRUCTURE
#define Seln_requester_ptr INTEGER*4
      STRUCTURE /Seln_request/
          Seln_replier_data_ptr replier
          RECORD /Seln_requester/ requester
          Char_ptr addressee
          Seln_rank rank
          Seln_result status
          INTEGER  buf_size
          CHARACTER data (0 : SELN_BUFSIZE-1)
      
          END STRUCTURE
#define Seln_request_ptr INTEGER*4
C 
C  * PUBLIC structs 
C  * For SunView 1 compatibility 
C  
      STRUCTURE /Seln_file_info/
          Seln_rank rank
          Char_ptr pathname
          END STRUCTURE
      STRUCTURE /Seln_inform_args/
          RECORD /Seln_holder/ holder
          Seln_function function
          INTEGER  down
          END STRUCTURE
      STRUCTURE /Seln_functions_state/
          INTEGER data (0 : SELN_FUNCTION_WORD_COUNT-1)
      
          END STRUCTURE
#define SelnRequestProc INTEGER
#define SelectionRequestProc INTEGER
#define SelnReaderProc INTEGER
#define SelectionReaderProc INTEGER
#define FunctionProc INTEGER
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * PUBLIC variables 
C  
C 
C  * null structs for initialization & easy reference	
C  
C 
C  * PUBLIC functions 
C  
      Seln_rank  seln_acquire
      EXTERNAL seln_acquire
C       Seln_client client
C       Seln_rank asked

      Seln_request_ptr  selection_ask
      EXTERNAL selection_ask
C       Xv_Server server
C       RECORD /Seln_holder/ holder

      EXTERNAL selection_clear_functions
C       Xv_Server server

      Seln_client  selection_create
      EXTERNAL selection_create
C       Xv_Server server
C       FunctionProc function_proc
C       SelectionRequestProc reply_proc
C       caddr_t client_data

      EXTERNAL selection_destroy
C       Xv_Server server
C       Seln_client client

      Seln_result  selection_done
      EXTERNAL selection_done
C       Xv_Server server
C       Seln_client client
C       Seln_rank rank

      Seln_result  selection_figure_response
      EXTERNAL selection_figure_response
C       Xv_Server server
C       Seln_function_buffer_ptr buffer
C       Seln_holder_ptr holder

      Seln_result  selection_hold_file
      EXTERNAL selection_hold_file
C       Xv_Server server
C       Seln_rank rank
C       Char_ptr path

      EXTERNAL selection_inform
C       RECORD /Seln_function_buffer/  returnval__
C       Xv_Server server
C       Seln_client client
C       Seln_function which
C       INTEGER down

      EXTERNAL selection_init_request
C       Xv_Server server
C       RECORD /Seln_request/ buffer
C       RECORD /Seln_holder/ holder

      EXTERNAL selection_init_request_l
C       Xv_Server server
C       RECORD /Seln_request/ buffer
C       Seln_holder_ptr holder
C       Attr_avlist avlist

      EXTERNAL selection_inquire
C       RECORD /Seln_holder/  returnval__
C       Xv_Server server
C       Seln_rank rank

      EXTERNAL selection_inquire_all
C       RECORD /Seln_holders_all/  returnval__
C       Xv_Server server

      Seln_result  selection_query
      EXTERNAL selection_query
C       Xv_Server server
C       RECORD /Seln_holder/ holder
C       SelectionReaderProc reader
C       Char_ptr context

      Seln_result  selection_query_l
      EXTERNAL selection_query_l
C       Xv_Server server
C       RECORD /Seln_holder/ holder
C       ProcPointer reader
C       Char_ptr context
C       Attr_avlist avlist

      EXTERNAL selection_report_event
C       Xv_Server server
C       Seln_client client
C       Event_ptr event

      Seln_result  selection_request
      EXTERNAL selection_request
C       Xv_Server server
C       Seln_holder_ptr holder
C       Seln_request_ptr buffer

      EXTERNAL selection_use_timeout
C       Xv_Server server
C       INTEGER seconds

      EXTERNAL selection_yield_all
C       Xv_Server server

      Seln_client  seln_create
      EXTERNAL seln_create
C       ProcPointer function_proc
C       SelnRequestProc request_proc
C       caddr_t client_data

      EXTERNAL seln_inform
C       RECORD /Seln_function_buffer/  returnval__
C       Seln_client client
C       Seln_function which
C       INTEGER down

      EXTERNAL seln_inquire
C       RECORD /Seln_holder/  returnval__
C       Seln_rank rank

      EXTERNAL seln_inquire_all
C       RECORD /Seln_holders_all/  returnval__

      Seln_rank  selection_acquire
      EXTERNAL selection_acquire
C       Xv_Server server
C       Seln_client client
C       Seln_rank asked

      Seln_request_ptr  seln_ask
      EXTERNAL seln_ask
C       RECORD /Seln_holder/ holder

      Seln_request_ptr  selection_ask_l
      EXTERNAL selection_ask_l
C       Xv_Server server
C       RECORD /Seln_holder/ holder
C       Attr_avlist avlist

      Seln_response  seln_figure_response
      EXTERNAL seln_figure_response
C       Seln_function_buffer_ptr buffer
C       Seln_holder_ptr holder

      Seln_result  seln_done
      EXTERNAL seln_done
C       Seln_client seln_client
C       Seln_rank rank

      Seln_result  seln_hold_file
      EXTERNAL seln_hold_file
C       Seln_rank rank
C       Char_ptr path

      Seln_result  seln_query
      EXTERNAL seln_query
C       RECORD /Seln_holder/ holder
C       SelnReaderProc reader
C       Char_ptr context

      Seln_result  seln_request
      EXTERNAL seln_request
C       Seln_holder_ptr holder
C       Seln_request_ptr buffer

      EXTERNAL seln_report_event
C       Seln_client client
C       Event_ptr event

      EXTERNAL seln_yield_all
      EXTERNAL seln_destroy
C       Seln_client client

      LOGICAL  seln_holder_same_client
      EXTERNAL seln_holder_same_client
C       Seln_holder_ptr holder
C       caddr_t client_data

      LOGICAL  seln_holder_same_process
      EXTERNAL seln_holder_same_process
C       Seln_holder_ptr holder

      LOGICAL  seln_secondary_made
      EXTERNAL seln_secondary_made
C       Seln_function_buffer_ptr buffer

      LOGICAL  seln_secondary_exists
      EXTERNAL seln_secondary_exists
C       Seln_function_buffer_ptr buffer

      EXTERNAL seln_init_request
C       RECORD /Seln_request/ buffer
C       RECORD /Seln_holder/ holder

      EXTERNAL seln_clear_functions
      EXTERNAL seln_use_timeout
C       INTEGER seconds

#endif sel_svc_F.

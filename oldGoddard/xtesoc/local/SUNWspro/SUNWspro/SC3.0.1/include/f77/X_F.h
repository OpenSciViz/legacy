#ifndef X_F_DEFINED
#define X_F_DEFINED

C 
C  *	Derived from:
C  *	$XConsortium: X.h,v 1.66 88/09/06 15:55:56 jim Exp $
C  
C  Definitions for the X window system likely to be used by applications 
C **********************************************************
C Copyright 1987 by Digital Equipment Corporation, Maynard, Massachusetts,
C and the Massachusetts Institute of Technology, Cambridge, Massachusetts.
C 
C                         All Rights Reserved
C 
C Permission to use, copy, modify, and distribute this software and its 
C documentation for any purpose and without fee is hereby granted, 
C provided that the above copyright notice appear in all copies and that
C both that copyright notice and this permission notice appear in 
C supporting documentation, and that the names of Digital or MIT not be
C used in advertising or publicity pertaining to distribution of the
C software without specific, written prior permission.  
C 
C DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
C ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
C DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
C ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
C WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
C ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
C SOFTWARE.
C 
C *****************************************************************
      INTEGER X_PROTOCOL
      PARAMETER (X_PROTOCOL = 11)
C  current protocol version 
      INTEGER X_PROTOCOL_REVISION
      PARAMETER (X_PROTOCOL_REVISION = 0)
C  current minor version 
C  Resources 
#define XID INTEGER
#define Window XID
#define Drawable XID
#define Font XID
#define Pixmap XID
#define Cursor XID
#define Colormap XID
#define Colormap_ptr INTEGER*4
#define GContext XID
#define KeySym XID
#define KeySym_ptr INTEGER*4
#define Mask INTEGER
#define Atom INTEGER
#define Atom_ptr INTEGER*4
#define VisualID INTEGER
#define Time INTEGER
#define KeyCode Byte
C ****************************************************************
C  * RESERVED RESOURCE AND CONSTANT DEFINITIONS
C  ****************************************************************
      INTEGER None
      PARAMETER (None = 0)
C  universal null resource or null atom 
      INTEGER ParentRelative
      PARAMETER (ParentRelative = 1)
C  background pixmap in CreateWindow
C 				    and ChangeWindowAttributes 
      INTEGER CopyFromParent
      PARAMETER (CopyFromParent = 0)
C  border pixmap in CreateWindow
C 				       and ChangeWindowAttributes
C 				   special VisualID and special window
C 				       class passed to CreateWindow 
      INTEGER PointerWindow
      PARAMETER (PointerWindow = 0)
C  destination window in SendEvent 
      INTEGER InputFocus
      PARAMETER (InputFocus = 1)
C  destination window in SendEvent 
      INTEGER PointerRoot
      PARAMETER (PointerRoot = 1)
C  focus window in SetInputFocus 
      INTEGER AnyPropertyType
      PARAMETER (AnyPropertyType = 0)
C  special Atom, passed to GetProperty 
      INTEGER AnyKey
      PARAMETER (AnyKey = 0)
C  special Key Code, passed to GrabKey 
      INTEGER AnyButton
      PARAMETER (AnyButton = 0)
C  special Button Code, passed to GrabButton 
      INTEGER AllTemporary
      PARAMETER (AllTemporary = 0)
C  special Resource ID passed to KillClient 
      INTEGER CurrentTime
      PARAMETER (CurrentTime = 0)
C  special Time 
      INTEGER NoSymbol
      PARAMETER (NoSymbol = 0)
C  special KeySym 
C **************************************************************** 
C  * EVENT DEFINITIONS 
C  ****************************************************************
C  Input Event Masks. Used as event-mask window attribute and as arguments
C    to Grab requests.  Not to be confused with event names.  
      INTEGER NoEventMask
      PARAMETER (NoEventMask = X'0')
      INTEGER KeyPressMask
      PARAMETER (KeyPressMask = X'1')
      INTEGER KeyReleaseMask
      PARAMETER (KeyReleaseMask = X'2')
      INTEGER ButtonPressMask
      PARAMETER (ButtonPressMask = X'4')
      INTEGER ButtonReleaseMask
      PARAMETER (ButtonReleaseMask = X'8')
      INTEGER EnterWindowMask
      PARAMETER (EnterWindowMask = X'10')
      INTEGER LeaveWindowMask
      PARAMETER (LeaveWindowMask = X'20')
      INTEGER PointerMotionMask
      PARAMETER (PointerMotionMask = X'40')
      INTEGER PointerMotionHintMask
      PARAMETER (PointerMotionHintMask = X'80')
      INTEGER Button1MotionMask
      PARAMETER (Button1MotionMask = X'100')
      INTEGER Button2MotionMask
      PARAMETER (Button2MotionMask = X'200')
      INTEGER Button3MotionMask
      PARAMETER (Button3MotionMask = X'400')
      INTEGER Button4MotionMask
      PARAMETER (Button4MotionMask = X'800')
      INTEGER Button5MotionMask
      PARAMETER (Button5MotionMask = X'1000')
      INTEGER ButtonMotionMask
      PARAMETER (ButtonMotionMask = X'2000')
      INTEGER KeymapStateMask
      PARAMETER (KeymapStateMask = X'4000')
      INTEGER ExposureMask
      PARAMETER (ExposureMask = X'8000')
      INTEGER VisibilityChangeMask
      PARAMETER (VisibilityChangeMask = X'10000')
      INTEGER StructureNotifyMask
      PARAMETER (StructureNotifyMask = X'20000')
      INTEGER ResizeRedirectMask
      PARAMETER (ResizeRedirectMask = X'40000')
      INTEGER SubstructureNotifyMask
      PARAMETER (SubstructureNotifyMask = X'80000')
      INTEGER SubstructureRedirectMask
      PARAMETER (SubstructureRedirectMask = X'100000')
      INTEGER FocusChangeMask
      PARAMETER (FocusChangeMask = X'200000')
      INTEGER PropertyChangeMask
      PARAMETER (PropertyChangeMask = X'400000')
      INTEGER ColormapChangeMask
      PARAMETER (ColormapChangeMask = X'800000')
      INTEGER OwnerGrabButtonMask
      PARAMETER (OwnerGrabButtonMask = X'1000000')
C  Event names.  Used in "type" field in XEvent structures.  Not to be
C confused with event masks above.  They start from 2 because 0 and 1
C are reserved in the protocol for errors and replies. 
      INTEGER KeyPress
      PARAMETER (KeyPress = 2)
      INTEGER KeyRelease
      PARAMETER (KeyRelease = 3)
      INTEGER ButtonPress
      PARAMETER (ButtonPress = 4)
      INTEGER ButtonRelease
      PARAMETER (ButtonRelease = 5)
      INTEGER MotionNotify
      PARAMETER (MotionNotify = 6)
      INTEGER EnterNotify
      PARAMETER (EnterNotify = 7)
      INTEGER LeaveNotify
      PARAMETER (LeaveNotify = 8)
      INTEGER FocusIn
      PARAMETER (FocusIn = 9)
      INTEGER FocusOut
      PARAMETER (FocusOut = 10)
      INTEGER KeymapNotify
      PARAMETER (KeymapNotify = 11)
      INTEGER Expose
      PARAMETER (Expose = 12)
      INTEGER GraphicsExpose
      PARAMETER (GraphicsExpose = 13)
      INTEGER NoExpose
      PARAMETER (NoExpose = 14)
      INTEGER VisibilityNotify
      PARAMETER (VisibilityNotify = 15)
      INTEGER CreateNotify
      PARAMETER (CreateNotify = 16)
      INTEGER DestroyNotify
      PARAMETER (DestroyNotify = 17)
      INTEGER UnmapNotify
      PARAMETER (UnmapNotify = 18)
      INTEGER MapNotify
      PARAMETER (MapNotify = 19)
      INTEGER MapRequest
      PARAMETER (MapRequest = 20)
      INTEGER ReparentNotify
      PARAMETER (ReparentNotify = 21)
      INTEGER ConfigureNotify
      PARAMETER (ConfigureNotify = 22)
      INTEGER ConfigureRequest
      PARAMETER (ConfigureRequest = 23)
      INTEGER GravityNotify
      PARAMETER (GravityNotify = 24)
      INTEGER ResizeRequest
      PARAMETER (ResizeRequest = 25)
      INTEGER CirculateNotify
      PARAMETER (CirculateNotify = 26)
      INTEGER CirculateRequest
      PARAMETER (CirculateRequest = 27)
      INTEGER PropertyNotify
      PARAMETER (PropertyNotify = 28)
      INTEGER SelectionClear
      PARAMETER (SelectionClear = 29)
      INTEGER SelectionRequest
      PARAMETER (SelectionRequest = 30)
      INTEGER SelectionNotify
      PARAMETER (SelectionNotify = 31)
      INTEGER ColormapNotify
      PARAMETER (ColormapNotify = 32)
      INTEGER ClientMessage
      PARAMETER (ClientMessage = 33)
      INTEGER MappingNotify
      PARAMETER (MappingNotify = 34)
      INTEGER LASTEvent
      PARAMETER (LASTEvent = 35)
C  must be bigger than any event # 
C  Key masks. Used as modifiers to GrabButton and GrabKey, results of QueryPointer,
C    state in various key-, mouse-, and button-related events. 
      INTEGER ShiftMask
      PARAMETER (ShiftMask = X'1')
      INTEGER LockMask
      PARAMETER (LockMask = X'2')
      INTEGER ControlMask
      PARAMETER (ControlMask = X'4')
      INTEGER Mod1Mask
      PARAMETER (Mod1Mask = X'8')
      INTEGER Mod2Mask
      PARAMETER (Mod2Mask = X'10')
      INTEGER Mod3Mask
      PARAMETER (Mod3Mask = X'20')
      INTEGER Mod4Mask
      PARAMETER (Mod4Mask = X'40')
      INTEGER Mod5Mask
      PARAMETER (Mod5Mask = X'80')
C  modifier names.  Used to build a SetModifierMapping request or
C    to read a GetModifierMapping request.  These correspond to the
C    masks defined above. 
      INTEGER ShiftMapIndex
      PARAMETER (ShiftMapIndex = 0)
      INTEGER LockMapIndex
      PARAMETER (LockMapIndex = 1)
      INTEGER ControlMapIndex
      PARAMETER (ControlMapIndex = 2)
      INTEGER Mod1MapIndex
      PARAMETER (Mod1MapIndex = 3)
      INTEGER Mod2MapIndex
      PARAMETER (Mod2MapIndex = 4)
      INTEGER Mod3MapIndex
      PARAMETER (Mod3MapIndex = 5)
      INTEGER Mod4MapIndex
      PARAMETER (Mod4MapIndex = 6)
      INTEGER Mod5MapIndex
      PARAMETER (Mod5MapIndex = 7)
C  button masks.  Used in same manner as Key masks above. Not to be confused
C    with button names below. 
      INTEGER Button1Mask
      PARAMETER (Button1Mask = X'100')
      INTEGER Button2Mask
      PARAMETER (Button2Mask = X'200')
      INTEGER Button3Mask
      PARAMETER (Button3Mask = X'400')
      INTEGER Button4Mask
      PARAMETER (Button4Mask = X'800')
      INTEGER Button5Mask
      PARAMETER (Button5Mask = X'1000')
      INTEGER AnyModifier
      PARAMETER (AnyModifier = X'8000')
C  used in GrabButton, GrabKey 
C  button names. Used as arguments to GrabButton and as detail in ButtonPress
C    and ButtonRelease events.  Not to be confused with button masks above.
C    Note that 0 is already defined above as "AnyButton".  
      INTEGER Button1
      PARAMETER (Button1 = 1)
      INTEGER Button2
      PARAMETER (Button2 = 2)
      INTEGER Button3
      PARAMETER (Button3 = 3)
      INTEGER Button4
      PARAMETER (Button4 = 4)
      INTEGER Button5
      PARAMETER (Button5 = 5)
C  Notify modes 
      INTEGER NotifyNormal
      PARAMETER (NotifyNormal = 0)
      INTEGER NotifyGrab
      PARAMETER (NotifyGrab = 1)
      INTEGER NotifyUngrab
      PARAMETER (NotifyUngrab = 2)
      INTEGER NotifyWhileGrabbed
      PARAMETER (NotifyWhileGrabbed = 3)
      INTEGER NotifyHint
      PARAMETER (NotifyHint = 1)
C  for MotionNotify events 
C  Notify detail 
      INTEGER NotifyAncestor
      PARAMETER (NotifyAncestor = 0)
      INTEGER NotifyVirtual
      PARAMETER (NotifyVirtual = 1)
      INTEGER NotifyInferior
      PARAMETER (NotifyInferior = 2)
      INTEGER NotifyNonlinear
      PARAMETER (NotifyNonlinear = 3)
      INTEGER NotifyNonlinearVirtual
      PARAMETER (NotifyNonlinearVirtual = 4)
      INTEGER NotifyPointer
      PARAMETER (NotifyPointer = 5)
      INTEGER NotifyPointerRoot
      PARAMETER (NotifyPointerRoot = 6)
      INTEGER NotifyDetailNone
      PARAMETER (NotifyDetailNone = 7)
C  Visibility notify 
      INTEGER VisibilityUnobscured
      PARAMETER (VisibilityUnobscured = 0)
      INTEGER VisibilityPartiallyObscured
      PARAMETER (VisibilityPartiallyObscured = 1)
      INTEGER VisibilityFullyObscured
      PARAMETER (VisibilityFullyObscured = 2)
C  Circulation request 
      INTEGER PlaceOnTop
      PARAMETER (PlaceOnTop = 0)
      INTEGER PlaceOnBottom
      PARAMETER (PlaceOnBottom = 1)
C  protocol families 
      INTEGER FamilyInternet
      PARAMETER (FamilyInternet = 0)
      INTEGER FamilyDECnet
      PARAMETER (FamilyDECnet = 1)
      INTEGER FamilyChaos
      PARAMETER (FamilyChaos = 2)
C  Property notification 
      INTEGER PropertyNewValue
      PARAMETER (PropertyNewValue = 0)
      INTEGER PropertyDelete
      PARAMETER (PropertyDelete = 1)
C  Color Map notification 
      INTEGER ColormapUninstalled
      PARAMETER (ColormapUninstalled = 0)
      INTEGER ColormapInstalled
      PARAMETER (ColormapInstalled = 1)
C  GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes 
      INTEGER GrabModeSync
      PARAMETER (GrabModeSync = 0)
      INTEGER GrabModeAsync
      PARAMETER (GrabModeAsync = 1)
C  GrabPointer, GrabKeyboard reply status 
      INTEGER GrabSuccess
      PARAMETER (GrabSuccess = 0)
      INTEGER AlreadyGrabbed
      PARAMETER (AlreadyGrabbed = 1)
      INTEGER GrabInvalidTime
      PARAMETER (GrabInvalidTime = 2)
      INTEGER GrabNotViewable
      PARAMETER (GrabNotViewable = 3)
      INTEGER GrabFrozen
      PARAMETER (GrabFrozen = 4)
C  AllowEvents modes 
      INTEGER AsyncPointer
      PARAMETER (AsyncPointer = 0)
      INTEGER SyncPointer
      PARAMETER (SyncPointer = 1)
      INTEGER ReplayPointer
      PARAMETER (ReplayPointer = 2)
      INTEGER AsyncKeyboard
      PARAMETER (AsyncKeyboard = 3)
      INTEGER SyncKeyboard
      PARAMETER (SyncKeyboard = 4)
      INTEGER ReplayKeyboard
      PARAMETER (ReplayKeyboard = 5)
      INTEGER AsyncBoth
      PARAMETER (AsyncBoth = 6)
      INTEGER SyncBoth
      PARAMETER (SyncBoth = 7)
C  Used in SetInputFocus, GetInputFocus 
      INTEGER RevertToNone
      PARAMETER (RevertToNone = None)
      INTEGER RevertToPointerRoot
      PARAMETER (RevertToPointerRoot = PointerRoot)
      INTEGER RevertToParent
      PARAMETER (RevertToParent = 2)
C ****************************************************************
C  * ERROR CODES 
C  ****************************************************************
      INTEGER Success
      PARAMETER (Success = 0)
C  everything's okay 
      INTEGER BadRequest
      PARAMETER (BadRequest = 1)
C  bad request code 
      INTEGER BadValue
      PARAMETER (BadValue = 2)
C  int parameter out of range 
      INTEGER BadWindow
      PARAMETER (BadWindow = 3)
C  parameter not a Window 
      INTEGER BadPixmap
      PARAMETER (BadPixmap = 4)
C  parameter not a Pixmap 
      INTEGER BadAtom
      PARAMETER (BadAtom = 5)
C  parameter not an Atom 
      INTEGER BadCursor
      PARAMETER (BadCursor = 6)
C  parameter not a Cursor 
      INTEGER BadFont
      PARAMETER (BadFont = 7)
C  parameter not a Font 
      INTEGER BadMatch
      PARAMETER (BadMatch = 8)
C  parameter mismatch 
      INTEGER BadDrawable
      PARAMETER (BadDrawable = 9)
C  parameter not a Pixmap or Window 
      INTEGER BadAccess
      PARAMETER (BadAccess = 10)
C  depending on context:
C 				 - key/button already grabbed
C 				 - attempt to free an illegal 
C 				   cmap entry 
C 				- attempt to store into a read-only 
C 				   color map entry.
C  				- attempt to modify the access control
C 				   list from other than the local host.
C 				
      INTEGER BadAlloc
      PARAMETER (BadAlloc = 11)
C  insufficient resources 
      INTEGER BadColor
      PARAMETER (BadColor = 12)
C  no such colormap 
      INTEGER BadGC
      PARAMETER (BadGC = 13)
C  parameter not a GC 
      INTEGER BadIDChoice
      PARAMETER (BadIDChoice = 14)
C  choice not in range or already used 
      INTEGER BadName
      PARAMETER (BadName = 15)
C  font or color name doesn't exist 
      INTEGER BadLength
      PARAMETER (BadLength = 16)
C  Request length incorrect 
      INTEGER BadImplementation
      PARAMETER (BadImplementation = 17)
C  server is defective 
      INTEGER FirstExtensionError
      PARAMETER (FirstExtensionError = 128)
      INTEGER LastExtensionError
      PARAMETER (LastExtensionError = 255)
C ****************************************************************
C  * WINDOW DEFINITIONS 
C  ****************************************************************
C  Window classes used by CreateWindow 
C  Note that CopyFromParent is already defined as 0 above 
      INTEGER InputOutput
      PARAMETER (InputOutput = 1)
      INTEGER InputOnly
      PARAMETER (InputOnly = 2)
C  Window attributes for CreateWindow and ChangeWindowAttributes 
      INTEGER CWBackPixmap
      PARAMETER (CWBackPixmap = X'1')
      INTEGER CWBackPixel
      PARAMETER (CWBackPixel = X'2')
      INTEGER CWBorderPixmap
      PARAMETER (CWBorderPixmap = X'4')
      INTEGER CWBorderPixel
      PARAMETER (CWBorderPixel = X'8')
      INTEGER CWBitGravity
      PARAMETER (CWBitGravity = X'10')
      INTEGER CWWinGravity
      PARAMETER (CWWinGravity = X'20')
      INTEGER CWBackingStore
      PARAMETER (CWBackingStore = X'40')
      INTEGER CWBackingPlanes
      PARAMETER (CWBackingPlanes = X'80')
      INTEGER CWBackingPixel
      PARAMETER (CWBackingPixel = X'100')
      INTEGER CWOverrideRedirect
      PARAMETER (CWOverrideRedirect = X'200')
      INTEGER CWSaveUnder
      PARAMETER (CWSaveUnder = X'400')
      INTEGER CWEventMask
      PARAMETER (CWEventMask = X'800')
      INTEGER CWDontPropagate
      PARAMETER (CWDontPropagate = X'1000')
      INTEGER CWColormap
      PARAMETER (CWColormap = X'2000')
      INTEGER CWCursor
      PARAMETER (CWCursor = X'4000')
C  ConfigureWindow structure 
      INTEGER CWX
      PARAMETER (CWX = X'1')
      INTEGER CWY
      PARAMETER (CWY = X'2')
      INTEGER CWWidth
      PARAMETER (CWWidth = X'4')
      INTEGER CWHeight
      PARAMETER (CWHeight = X'8')
      INTEGER CWBorderWidth
      PARAMETER (CWBorderWidth = X'10')
      INTEGER CWSibling
      PARAMETER (CWSibling = X'20')
      INTEGER CWStackMode
      PARAMETER (CWStackMode = X'40')
C  Bit Gravity 
      INTEGER ForgetGravity
      PARAMETER (ForgetGravity = 0)
      INTEGER NorthWestGravity
      PARAMETER (NorthWestGravity = 1)
      INTEGER NorthGravity
      PARAMETER (NorthGravity = 2)
      INTEGER NorthEastGravity
      PARAMETER (NorthEastGravity = 3)
      INTEGER WestGravity
      PARAMETER (WestGravity = 4)
      INTEGER CenterGravity
      PARAMETER (CenterGravity = 5)
      INTEGER EastGravity
      PARAMETER (EastGravity = 6)
      INTEGER SouthWestGravity
      PARAMETER (SouthWestGravity = 7)
      INTEGER SouthGravity
      PARAMETER (SouthGravity = 8)
      INTEGER SouthEastGravity
      PARAMETER (SouthEastGravity = 9)
      INTEGER StaticGravity
      PARAMETER (StaticGravity = 10)
C  Window gravity + bit gravity above 
      INTEGER UnmapGravity
      PARAMETER (UnmapGravity = 0)
C  Used in CreateWindow for backing-store hint 
      INTEGER NotUseful
      PARAMETER (NotUseful = 0)
      INTEGER WhenMapped
      PARAMETER (WhenMapped = 1)
      INTEGER Always
      PARAMETER (Always = 2)
C  Used in GetWindowAttributes reply 
      INTEGER IsUnmapped
      PARAMETER (IsUnmapped = 0)
      INTEGER IsUnviewable
      PARAMETER (IsUnviewable = 1)
      INTEGER IsViewable
      PARAMETER (IsViewable = 2)
C  Used in ChangeSaveSet 
      INTEGER SetModeInsert
      PARAMETER (SetModeInsert = 0)
      INTEGER SetModeDelete
      PARAMETER (SetModeDelete = 1)
C  Used in ChangeCloseDownMode 
      INTEGER DestroyAll
      PARAMETER (DestroyAll = 0)
      INTEGER RetainPermanent
      PARAMETER (RetainPermanent = 1)
      INTEGER RetainTemporary
      PARAMETER (RetainTemporary = 2)
C  Window stacking method (in configureWindow) 
      INTEGER Above
      PARAMETER (Above = 0)
      INTEGER Below
      PARAMETER (Below = 1)
      INTEGER TopIf
      PARAMETER (TopIf = 2)
      INTEGER BottomIf
      PARAMETER (BottomIf = 3)
      INTEGER Opposite
      PARAMETER (Opposite = 4)
C  Circulation direction 
      INTEGER RaiseLowest
      PARAMETER (RaiseLowest = 0)
      INTEGER LowerHighest
      PARAMETER (LowerHighest = 1)
C  Property modes 
      INTEGER PropModeReplace
      PARAMETER (PropModeReplace = 0)
      INTEGER PropModePrepend
      PARAMETER (PropModePrepend = 1)
      INTEGER PropModeAppend
      PARAMETER (PropModeAppend = 2)
C ****************************************************************
C  * GRAPHICS DEFINITIONS
C  ****************************************************************
C  graphics functions, as in GC.alu 
      INTEGER GXclear
      PARAMETER (GXclear = X'0')
C  0 
      INTEGER GXand
      PARAMETER (GXand = X'1')
C  src AND dst 
      INTEGER GXandReverse
      PARAMETER (GXandReverse = X'2')
C  src AND NOT dst 
      INTEGER GXcopy
      PARAMETER (GXcopy = X'3')
C  src 
      INTEGER GXandInverted
      PARAMETER (GXandInverted = X'4')
C  NOT src AND dst 
      INTEGER GXnoop
      PARAMETER (GXnoop = X'5')
C  dst 
      INTEGER GXxor
      PARAMETER (GXxor = X'6')
C  src XOR dst 
      INTEGER GXor
      PARAMETER (GXor = X'7')
C  src OR dst 
      INTEGER GXnor
      PARAMETER (GXnor = X'8')
C  NOT src AND NOT dst 
      INTEGER GXequiv
      PARAMETER (GXequiv = X'9')
C  NOT src XOR dst 
      INTEGER GXinvert
      PARAMETER (GXinvert = X'A')
C  NOT dst 
      INTEGER GXorReverse
      PARAMETER (GXorReverse = X'B')
C  src OR NOT dst 
      INTEGER GXcopyInverted
      PARAMETER (GXcopyInverted = X'C')
C  NOT src 
      INTEGER GXorInverted
      PARAMETER (GXorInverted = X'D')
C  NOT src OR dst 
      INTEGER GXnand
      PARAMETER (GXnand = X'E')
C  NOT src OR NOT dst 
      INTEGER GXset
      PARAMETER (GXset = X'F')
C  1 
C  LineStyle 
      INTEGER LineSolid
      PARAMETER (LineSolid = 0)
      INTEGER LineOnOffDash
      PARAMETER (LineOnOffDash = 1)
      INTEGER LineDoubleDash
      PARAMETER (LineDoubleDash = 2)
C  capStyle 
      INTEGER CapNotLast
      PARAMETER (CapNotLast = 0)
      INTEGER CapButt
      PARAMETER (CapButt = 1)
      INTEGER CapRound
      PARAMETER (CapRound = 2)
      INTEGER CapProjecting
      PARAMETER (CapProjecting = 3)
C  joinStyle 
      INTEGER JoinMiter
      PARAMETER (JoinMiter = 0)
      INTEGER JoinRound
      PARAMETER (JoinRound = 1)
      INTEGER JoinBevel
      PARAMETER (JoinBevel = 2)
C  fillStyle 
      INTEGER FillSolid
      PARAMETER (FillSolid = 0)
      INTEGER FillTiled
      PARAMETER (FillTiled = 1)
      INTEGER FillStippled
      PARAMETER (FillStippled = 2)
      INTEGER FillOpaqueStippled
      PARAMETER (FillOpaqueStippled = 3)
C  fillRule 
      INTEGER EvenOddRule
      PARAMETER (EvenOddRule = 0)
      INTEGER WindingRule
      PARAMETER (WindingRule = 1)
C  subwindow mode 
      INTEGER ClipByChildren
      PARAMETER (ClipByChildren = 0)
      INTEGER IncludeInferiors
      PARAMETER (IncludeInferiors = 1)
C  SetClipRectangles ordering 
      INTEGER Unsorted
      PARAMETER (Unsorted = 0)
      INTEGER YSorted
      PARAMETER (YSorted = 1)
      INTEGER YXSorted
      PARAMETER (YXSorted = 2)
      INTEGER YXBanded
      PARAMETER (YXBanded = 3)
C  CoordinateMode for drawing routines 
      INTEGER CoordModeOrigin
      PARAMETER (CoordModeOrigin = 0)
C  relative to the origin 
      INTEGER CoordModePrevious
      PARAMETER (CoordModePrevious = 1)
C  relative to previous point 
C  Polygon shapes 
      INTEGER Complex
      PARAMETER (Complex = 0)
C  paths may intersect 
      INTEGER Nonconvex
      PARAMETER (Nonconvex = 1)
C  no paths intersect, but not convex 
      INTEGER Convex
      PARAMETER (Convex = 2)
C  wholly convex 
C  Arc modes for PolyFillArc 
      INTEGER ArcChord
      PARAMETER (ArcChord = 0)
C  join endpoints of arc 
      INTEGER ArcPieSlice
      PARAMETER (ArcPieSlice = 1)
C  join endpoints to center of arc 
C  GC components: masks used in CreateGC, CopyGC, ChangeGC, OR'ed into
C    GC.stateChanges 
      INTEGER GCFunction
      PARAMETER (GCFunction = X'1')
      INTEGER GCPlaneMask
      PARAMETER (GCPlaneMask = X'2')
      INTEGER GCForeground
      PARAMETER (GCForeground = X'4')
      INTEGER GCBackground
      PARAMETER (GCBackground = X'8')
      INTEGER GCLineWidth
      PARAMETER (GCLineWidth = X'10')
      INTEGER GCLineStyle
      PARAMETER (GCLineStyle = X'20')
      INTEGER GCCapStyle
      PARAMETER (GCCapStyle = X'40')
      INTEGER GCJoinStyle
      PARAMETER (GCJoinStyle = X'80')
      INTEGER GCFillStyle
      PARAMETER (GCFillStyle = X'100')
      INTEGER GCFillRule
      PARAMETER (GCFillRule = X'200')
      INTEGER GCTile
      PARAMETER (GCTile = X'400')
      INTEGER GCStipple
      PARAMETER (GCStipple = X'800')
      INTEGER GCTileStipXOrigin
      PARAMETER (GCTileStipXOrigin = X'1000')
      INTEGER GCTileStipYOrigin
      PARAMETER (GCTileStipYOrigin = X'2000')
      INTEGER GCFont
      PARAMETER (GCFont = X'4000')
      INTEGER GCSubwindowMode
      PARAMETER (GCSubwindowMode = X'8000')
      INTEGER GCGraphicsExposures
      PARAMETER (GCGraphicsExposures = X'10000')
      INTEGER GCClipXOrigin
      PARAMETER (GCClipXOrigin = X'20000')
      INTEGER GCClipYOrigin
      PARAMETER (GCClipYOrigin = X'40000')
      INTEGER GCClipMask
      PARAMETER (GCClipMask = X'80000')
      INTEGER GCDashOffset
      PARAMETER (GCDashOffset = X'100000')
      INTEGER GCDashList
      PARAMETER (GCDashList = X'200000')
      INTEGER GCArcMode
      PARAMETER (GCArcMode = X'400000')
      INTEGER GCLastBit
      PARAMETER (GCLastBit = 22)
C ****************************************************************
C  * FONTS 
C  ****************************************************************
C  used in QueryFont -- draw direction 
      INTEGER FontLeftToRight
      PARAMETER (FontLeftToRight = 0)
      INTEGER FontRightToLeft
      PARAMETER (FontRightToLeft = 1)
      INTEGER FontChange
      PARAMETER (FontChange = 255)
C ****************************************************************
C  *  IMAGING 
C  ****************************************************************
C  ImageFormat -- PutImage, GetImage 
      INTEGER XYBitmap
      PARAMETER (XYBitmap = 0)
C  depth 1, XYFormat 
      INTEGER XYPixmap
      PARAMETER (XYPixmap = 1)
C  depth == drawable depth 
      INTEGER ZPixmap
      PARAMETER (ZPixmap = 2)
C  depth == drawable depth 
C ****************************************************************
C  *  COLOR MAP STUFF 
C  ****************************************************************
C  For CreateColormap 
      INTEGER AllocNone
      PARAMETER (AllocNone = 0)
C  create map with no entries 
      INTEGER AllocAll
      PARAMETER (AllocAll = 1)
C  allocate entire map writeable 
C  Flags used in StoreNamedColor, StoreColors 
      INTEGER DoRed
      PARAMETER (DoRed = X'1')
      INTEGER DoGreen
      PARAMETER (DoGreen = X'2')
      INTEGER DoBlue
      PARAMETER (DoBlue = X'4')
C ****************************************************************
C  * CURSOR STUFF
C  ****************************************************************
C  QueryBestSize Class 
      INTEGER CursorShape
      PARAMETER (CursorShape = 0)
C  largest size that can be displayed 
      INTEGER TileShape
      PARAMETER (TileShape = 1)
C  size tiled fastest 
      INTEGER StippleShape
      PARAMETER (StippleShape = 2)
C  size stippled fastest 
C **************************************************************** 
C  * KEYBOARD/POINTER STUFF
C  ****************************************************************
      INTEGER AutoRepeatModeOff
      PARAMETER (AutoRepeatModeOff = 0)
      INTEGER AutoRepeatModeOn
      PARAMETER (AutoRepeatModeOn = 1)
      INTEGER AutoRepeatModeDefault
      PARAMETER (AutoRepeatModeDefault = 2)
      INTEGER LedModeOff
      PARAMETER (LedModeOff = 0)
      INTEGER LedModeOn
      PARAMETER (LedModeOn = 1)
C  masks for ChangeKeyboardControl 
      INTEGER KBKeyClickPercent
      PARAMETER (KBKeyClickPercent = X'1')
      INTEGER KBBellPercent
      PARAMETER (KBBellPercent = X'2')
      INTEGER KBBellPitch
      PARAMETER (KBBellPitch = X'4')
      INTEGER KBBellDuration
      PARAMETER (KBBellDuration = X'8')
      INTEGER KBLed
      PARAMETER (KBLed = X'10')
      INTEGER KBLedMode
      PARAMETER (KBLedMode = X'20')
      INTEGER KBKey
      PARAMETER (KBKey = X'40')
      INTEGER KBAutoRepeatMode
      PARAMETER (KBAutoRepeatMode = X'80')
      INTEGER MappingSuccess
      PARAMETER (MappingSuccess = 0)
      INTEGER MappingBusy
      PARAMETER (MappingBusy = 1)
      INTEGER MappingFailed
      PARAMETER (MappingFailed = 2)
      INTEGER MappingModifier
      PARAMETER (MappingModifier = 0)
      INTEGER MappingKeyboard
      PARAMETER (MappingKeyboard = 1)
      INTEGER MappingPointer
      PARAMETER (MappingPointer = 2)
C ****************************************************************
C  * SCREEN SAVER STUFF 
C  ****************************************************************
      INTEGER DontPreferBlanking
      PARAMETER (DontPreferBlanking = 0)
      INTEGER PreferBlanking
      PARAMETER (PreferBlanking = 1)
      INTEGER DefaultBlanking
      PARAMETER (DefaultBlanking = 2)
      INTEGER DisableScreenSaver
      PARAMETER (DisableScreenSaver = 0)
      INTEGER DisableScreenInterval
      PARAMETER (DisableScreenInterval = 0)
      INTEGER DontAllowExposures
      PARAMETER (DontAllowExposures = 0)
      INTEGER AllowExposures
      PARAMETER (AllowExposures = 1)
      INTEGER DefaultExposures
      PARAMETER (DefaultExposures = 2)
C  for ForceScreenSaver 
      INTEGER ScreenSaverReset
      PARAMETER (ScreenSaverReset = 0)
      INTEGER ScreenSaverActive
      PARAMETER (ScreenSaverActive = 1)
C ****************************************************************
C  * HOSTS AND CONNECTIONS
C  ****************************************************************
C  for ChangeHosts 
      INTEGER HostInsert
      PARAMETER (HostInsert = 0)
      INTEGER HostDelete
      PARAMETER (HostDelete = 1)
C  for ChangeAccessControl 
      INTEGER EnableAccess
      PARAMETER (EnableAccess = 1)
      INTEGER DisableAccess
      PARAMETER (DisableAccess = 0)
C  Display classes  used in opening the connection 
C  * Note that the statically allocated ones are even numbered and the
C  * dynamically changeable ones are odd numbered 
      INTEGER StaticGray
      PARAMETER (StaticGray = 0)
      INTEGER GrayScale
      PARAMETER (GrayScale = 1)
      INTEGER StaticColor
      PARAMETER (StaticColor = 2)
      INTEGER PseudoColor
      PARAMETER (PseudoColor = 3)
      INTEGER TrueColor
      PARAMETER (TrueColor = 4)
      INTEGER DirectColor
      PARAMETER (DirectColor = 5)
C  Byte order  used in imageByteOrder and bitmapBitOrder 
      INTEGER LSBFirst
      PARAMETER (LSBFirst = 0)
      INTEGER MSBFirst
      PARAMETER (MSBFirst = 1)
#endif X_F.

#ifndef server_F_DEFINED
#define server_F_DEFINED

C   derived from @(#)server.h 20.59 92/12/18 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
#include "generic_F.h"
#include "pkg_public_F.h"
C 
C  * PUBLIC definitions 
C  
      Xv_pkg_ptr SERVER
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
      INTEGER KBD_CMDS_SUNVIEW1,KBD_CMDS_BASIC,KBD_CMDS_FULL
      PARAMETER ( KBD_CMDS_SUNVIEW1=0,  KBD_CMDS_BASIC=1, 
     & KBD_CMDS_FULL=2)
#define Kbd_cmds_value INTEGER*4
#define Xv_Server Xv_opaque
#define Xv_server Xv_opaque
      STRUCTURE /Xv_server_struct/
          RECORD /Xv_generic_struct/ parent
          Xv_opaque private_data
          END STRUCTURE
C 
C  ******************************
C  *		Attributes
C  ******************************
C  
C   Public attributes  
      INTEGER SERVER_ATOM
      PARAMETER (SERVER_ATOM = 1215040257)
      INTEGER SERVER_ATOM_NAME
      PARAMETER (SERVER_ATOM_NAME = 1215105537)
      INTEGER SERVER_ATOM_DATA
      PARAMETER (SERVER_ATOM_DATA = 1215367681)
      INTEGER SERVER_CHORDING_TIMEOUT
      PARAMETER (SERVER_CHORDING_TIMEOUT = 1223821313)
      INTEGER SERVER_CHORD_MENU
      PARAMETER (SERVER_CHORD_MENU = 1223952385)
      INTEGER SERVER_EXTENSION_PROC
      PARAMETER (SERVER_EXTENSION_PROC = 1208224353)
      INTEGER SERVER_NTH_SCREEN
      PARAMETER (SERVER_NTH_SCREEN = 1208092674)
      INTEGER SERVER_RESOURCE_DB
      PARAMETER (SERVER_RESOURCE_DB = 1224083969)
      INTEGER SERVER_SYNC
      PARAMETER (SERVER_SYNC = 1210059009)
      INTEGER SERVER_SYNC_AND_PROCESS_EVENTS
      PARAMETER (SERVER_SYNC_AND_PROCESS_EVENTS = 1210124832)
      INTEGER SERVER_EXTERNAL_XEVENT_PROC
      PARAMETER (SERVER_EXTERNAL_XEVENT_PROC = 1209272834)
      INTEGER SERVER_EXTERNAL_XEVENT_MASK
      PARAMETER (SERVER_EXTERNAL_XEVENT_MASK = 1209403907)
#define Server_attr Attr_attribute
C 
C  * PUBLIC variables 
C  
      COMMON /server_globals/   SERVER
#endif server_F.

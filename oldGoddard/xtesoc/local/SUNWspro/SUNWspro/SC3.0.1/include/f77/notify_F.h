#ifndef notify_F_DEFINED
#define notify_F_DEFINED

C 	derived from @(#)notify.h 20.33 92/08/26 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "time_F.h"
#include "win_input_F.h"
#include "base_F.h"
C 
C  ***********************************************************************
C  *			Definitions and Macros
C  ***********************************************************************
C  
C 
C  * PUBLIC definitions
C  
      INTEGER NOTIFY_FUNC_NULL
      PARAMETER (NOTIFY_FUNC_NULL = 0)
C 
C  *       System types needed for this package
C  
C *** From sys/resource.h ***
      STRUCTURE /Rusage/
          RECORD /Timeval/ ru_utime
          RECORD /Timeval/ ru_stime
          INTEGER  ru_maxrss
          INTEGER  ru_ixrss
          INTEGER  ru_idrss
          INTEGER  ru_isrss
          INTEGER  ru_minflt
          INTEGER  ru_majflt
          INTEGER  ru_nswap
          INTEGER  ru_inblock
          INTEGER  ru_oublock
          INTEGER  ru_msgsnd
          INTEGER  ru_msgrcv
          INTEGER  ru_nsignals
          INTEGER  ru_nvcsw
          INTEGER  ru_nivcsw
          END STRUCTURE
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structures
C  ***********************************************************************
C  
C 
C  * Opaque client handle.
C  
#define Notify_client Xv_opaque
C 
C  * Opaque client event.
C  
#define Notify_event Xv_opaque
#define Notify_event_ptr INTEGER*4
C 
C  * Opaque client event optional argument.
C  
#define Notify_arg Xv_opaque
C 
C  * A pointer to a function returning a Notify_arg (used for client
C  * event additional argument copying).
C  
#define Notify_copy INTEGER
C 
C  * A pointer to a function returning void (used for client
C  * event additional argument storage releasing).
C  
#define Notify_release INTEGER
C 
C  * For Debugging utility:
C  
      INTEGER NOTIFY_ALL,NOTIFY_DETECT,NOTIFY_DISPATCH
      PARAMETER ( NOTIFY_ALL=0,  NOTIFY_DETECT=1,  NOTIFY_DISPATCH=2)
#define Notify_dump_type INTEGER*4
C 
C  * Client notification function return values for notifier to client calls.
C  
      INTEGER NOTIFY_DONE,NOTIFY_IGNORED,NOTIFY_UNEXPECTED
      PARAMETER ( NOTIFY_DONE=0,  NOTIFY_IGNORED=1, 
     & NOTIFY_UNEXPECTED=2)
#define Notify_value INTEGER*4
C 
C  * Argument types
C  
      INTEGER NOTIFY_SYNC,NOTIFY_ASYNC
      PARAMETER ( NOTIFY_SYNC=0,  NOTIFY_ASYNC=1)
#define Notify_signal_mode INTEGER*4
      INTEGER NOTIFY_SAFE,NOTIFY_IMMEDIATE
      PARAMETER ( NOTIFY_SAFE=0,  NOTIFY_IMMEDIATE=1)
#define Notify_event_type INTEGER*4
      INTEGER DESTROY_PROCESS_DEATH,DESTROY_CHECKING,DESTROY_CLEANUP
     &,DESTROY_SAVE_YOURSELF
      PARAMETER ( DESTROY_PROCESS_DEATH=0,  DESTROY_CHECKING=1, 
     & DESTROY_CLEANUP=2,  DESTROY_SAVE_YOURSELF=3)
#define Destroy_status INTEGER*4
C 
C  * A pointer to a function returning a Notify_value.
C  
#define Notify_func INTEGER
C   This is a function type, which takes different arguments 
C   depending on the type of of object we are being notified 
C   about.                                                   
#define Notify_itimer_func INTEGER
#define Notify_destroy_func INTEGER
#define Notify_wait3_func INTEGER
#define Notify_input_func INTEGER
#define Notify_output_func INTEGER
#define Notify_exception_func INTEGER
#define Notify_signal_func INTEGER
#define Notify_event_func INTEGER
#define Notify_interposer_func INTEGER
#define Notify_scheduler_func INTEGER
#define Notify_prioritizer_func INTEGER
#define Notify_copy_func INTEGER
#define Notify_release_func INTEGER
C 
C  * Error codes for client to notifier calls (returned when no other
C  * return value or stored in notify_errno when other return value
C  * indicates error condition).
C  
      INTEGER NOTIFY_OK,NOTIFY_UNKNOWN_CLIENT,NOTIFY_NO_CONDITION
     &,NOTIFY_BAD_ITIMER,NOTIFY_BAD_SIGNAL,NOTIFY_NOT_STARTED
     &,NOTIFY_DESTROY_VETOED,NOTIFY_INTERNAL_ERROR,NOTIFY_SRCH
     &,NOTIFY_BADF,NOTIFY_NOMEM,NOTIFY_INVAL,NOTIFY_FUNC_LIMIT
      PARAMETER ( NOTIFY_OK=0,  NOTIFY_UNKNOWN_CLIENT=1, 
     & NOTIFY_NO_CONDITION=2,  NOTIFY_BAD_ITIMER=3,  NOTIFY_BAD_SIGNAL=4
     &,  NOTIFY_NOT_STARTED=5,  NOTIFY_DESTROY_VETOED=6, 
     & NOTIFY_INTERNAL_ERROR=7,  NOTIFY_SRCH=8,  NOTIFY_BADF=9, 
     & NOTIFY_NOMEM=10,  NOTIFY_INVAL=11,  NOTIFY_FUNC_LIMIT=12)
#define Notify_error INTEGER*4
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * PUBLIC variables 
C  
C 
C  * PUBLIC functions
C  
      Notify_value  notify_default_wait3
      EXTERNAL notify_default_wait3
C       Notify_client client
C       INTEGER pid
C       INTEGER status
C       RECORD /Rusage/ rusage

      Notify_value  notify_default_waitpid
      EXTERNAL notify_default_waitpid
C       Notify_client client
C       INTEGER pid
C       INTEGER status
C       RECORD /Rusage/ rusage

      Notify_error  notify_dispatch
      EXTERNAL notify_dispatch
      Notify_error  notify_do_dispatch
      EXTERNAL notify_do_dispatch
      Notify_error  notify_itimer_value
      EXTERNAL notify_itimer_value
C       Notify_client client
C       INTEGER which
C       Itimerval_ptr value

      Notify_value  notify_next_destroy_func
      EXTERNAL notify_next_destroy_func
C       Notify_client client
C       Destroy_status status

      Notify_value  notify_next_event_func
      EXTERNAL notify_next_event_func
C       Notify_client client
C       Notify_event event
C       Notify_arg arg
C       Notify_event_type etype

      Notify_error  notify_no_dispatch
      EXTERNAL notify_no_dispatch
      Notify_destroy_func  notify_set_destroy_func
      EXTERNAL notify_set_destroy_func
C       Notify_client client
C       Notify_destroy_func destroy_func

      Notify_exception_func  notify_set_exception_func
      EXTERNAL notify_set_exception_func
C       Notify_client client
C       Notify_exception_func exception_func
C       INTEGER fd

      Notify_input_func  notify_set_input_func
      EXTERNAL notify_set_input_func
C       Notify_client client
C       Notify_input_func input_func
C       INTEGER fd

      Notify_itimer_func  notify_set_itimer_func
      EXTERNAL notify_set_itimer_func
C       Notify_client client
C       Notify_itimer_func itimer_func
C       INTEGER which
C       Itimerval_ptr value
C       Itimerval_ptr ovalue

      Notify_output_func  notify_set_output_func
      EXTERNAL notify_set_output_func
C       Notify_client client
C       Notify_output_func output_func
C       INTEGER fd

      Notify_signal_func  notify_set_signal_func
      EXTERNAL notify_set_signal_func
C       Notify_client client
C       Notify_signal_func signal_func
C       INTEGER signal
C       Notify_signal_mode when

      Notify_wait3_func  notify_set_wait3_func
      EXTERNAL notify_set_wait3_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_wait3_func  notify_set_waitpid_func
      EXTERNAL notify_set_waitpid_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_error  notify_start
      EXTERNAL notify_start
      Notify_error  notify_stop
      EXTERNAL notify_stop
      Notify_error  notify_veto_destroy
      EXTERNAL notify_veto_destroy
C       Notify_client client

      EXTERNAL notify_perror
C       CHARACTER s

      EXTERNAL notify_enable_rpc_svc
C       INTEGER flag

C 
C  * PRIVATE functions
C  
      Notify_error  notify_client
      EXTERNAL notify_client
C       Notify_client client

      Notify_error  notify_destroy
      EXTERNAL notify_destroy
C       Notify_client nclient
C       Destroy_status status

      Notify_error  notify_die
      EXTERNAL notify_die
C       Destroy_status status

      Notify_destroy_func  notify_get_destroy_func
      EXTERNAL notify_get_destroy_func
C       Notify_client client

      Notify_exception_func  notify_get_exception_func
      EXTERNAL notify_get_exception_func
C       Notify_client client
C       INTEGER fd

      Notify_event_func  notify_get_event_func
      EXTERNAL notify_get_event_func
C       Notify_client client
C       Notify_event_type when

      Notify_input_func  notify_get_input_func
      EXTERNAL notify_get_input_func
C       Notify_client client
C       INTEGER fd

      Notify_itimer_func  notify_get_itimer_func
      EXTERNAL notify_get_itimer_func
C       Notify_client client
C       INTEGER which

      Notify_output_func  notify_get_output_func
      EXTERNAL notify_get_output_func
C       Notify_client client
C       INTEGER fd

      Notify_prioritizer_func  notify_get_prioritizer_func
      EXTERNAL notify_get_prioritizer_func
C       Notify_client client

      Notify_signal_func  notify_get_signal_func
      EXTERNAL notify_get_signal_func
C       Notify_client client
C       INTEGER signal
C       Notify_signal_mode when

      Notify_scheduler_func  notify_get_scheduler_func
      EXTERNAL notify_get_scheduler_func
      Notify_wait3_func  notify_get_wait3_func
      EXTERNAL notify_get_wait3_func
C       Notify_client client
C       INTEGER pid

      Notify_wait3_func  notify_get_waitpid_func
      EXTERNAL notify_get_waitpid_func
C       Notify_client client
C       INTEGER pid

      Notify_error  notify_interpose_destroy_func
      EXTERNAL notify_interpose_destroy_func
C       Notify_client client
C       Notify_destroy_func destroy_func

      Notify_error  notify_interpose_event_func
      EXTERNAL notify_interpose_event_func
C       Notify_client client
C       Notify_event_func event_func
C       Notify_event_type etype

      Notify_error  notify_interpose_exception_func
      EXTERNAL notify_interpose_exception_func
C       Notify_client client
C       Notify_exception_func exception_func
C       INTEGER fd

      Notify_error  notify_interpose_input_func
      EXTERNAL notify_interpose_input_func
C       Notify_client client
C       Notify_input_func input_func
C       INTEGER fd

      Notify_error  notify_interpose_itimer_func
      EXTERNAL notify_interpose_itimer_func
C       Notify_client client
C       Notify_itimer_func itimer_func
C       INTEGER which

      Notify_error  notify_interpose_output_func
      EXTERNAL notify_interpose_output_func
C       Notify_client client
C       Notify_output_func output_func
C       INTEGER fd

      Notify_error  notify_interpose_signal_func
      EXTERNAL notify_interpose_signal_func
C       Notify_client client
C       Notify_output_func signal_func
C       INTEGER signal
C       Notify_signal_mode mode

      Notify_error  notify_interpose_wait3_func
      EXTERNAL notify_interpose_wait3_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_error  notify_interpose_waitpid_func
      EXTERNAL notify_interpose_waitpid_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_value  notify_next_exception_func
      EXTERNAL notify_next_exception_func
C       Notify_client client
C       INTEGER fd

      Notify_value  notify_next_input_func
      EXTERNAL notify_next_input_func
C       Notify_client client
C       INTEGER fd

      Notify_value  notify_next_itimer_func
      EXTERNAL notify_next_itimer_func
C       Notify_client client
C       INTEGER which

      Notify_value  notify_next_output_func
      EXTERNAL notify_next_output_func
C       Notify_client client
C       INTEGER fd

      Notify_value  notify_next_signal_func
      EXTERNAL notify_next_signal_func
C       Notify_client client
C       INTEGER signal
C       Notify_signal_mode mode

      Notify_value  notify_next_wait3_func
      EXTERNAL notify_next_wait3_func
C       Notify_client client
C       INTEGER pid
C       INTEGER status
C       RECORD /Rusage/ rusage

      Notify_value  notify_next_waitpid_func
      EXTERNAL notify_next_waitpid_func
C       Notify_client client
C       INTEGER pid
C       INTEGER status
C       RECORD /Rusage/ rusage

      Notify_error  notify_post_event
      EXTERNAL notify_post_event
C       Notify_client client
C       Notify_event event
C       Notify_event_type when_hint

      Notify_error  notify_post_destroy
      EXTERNAL notify_post_destroy
C       Notify_client client
C       Destroy_status status
C       Notify_event_type when

      Notify_error  notify_post_event_and_arg
      EXTERNAL notify_post_event_and_arg
C       Notify_client client
C       Notify_event event
C       Notify_event_type when_hint
C       Notify_arg arg
C       Notify_copy copy_func
C       Notify_release release_func

      Notify_error  notify_remove
      EXTERNAL notify_remove
C       Notify_client client

      Notify_error  notify_remove_destroy_func
      EXTERNAL notify_remove_destroy_func
C       Notify_client client
C       Notify_destroy_func destroy_func

      Notify_error  notify_remove_exception_func
      EXTERNAL notify_remove_exception_func
C       Notify_client client
C       Notify_exception_func exception_func
C       INTEGER fd

      Notify_error  notify_remove_event_func
      EXTERNAL notify_remove_event_func
C       Notify_client client
C       Notify_event_func event_func
C       Notify_event_type when

      Notify_error  notify_remove_input_func
      EXTERNAL notify_remove_input_func
C       Notify_client client
C       Notify_input_func input_func
C       INTEGER fd

      Notify_error  notify_remove_itimer_func
      EXTERNAL notify_remove_itimer_func
C       Notify_client client
C       Notify_itimer_func itimer_func
C       INTEGER which

      Notify_error  notify_remove_output_func
      EXTERNAL notify_remove_output_func
C       Notify_client client
C       Notify_output_func output_func
C       INTEGER fd

      Notify_error  notify_remove_signal_func
      EXTERNAL notify_remove_signal_func
C       Notify_client client
C       Notify_signal_func signal_func
C       INTEGER signal
C       Notify_signal_mode when

      Notify_error  notify_remove_wait3_func
      EXTERNAL notify_remove_wait3_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_error  notify_remove_waitpid_func
      EXTERNAL notify_remove_waitpid_func
C       Notify_client client
C       Notify_wait3_func wait3_func
C       INTEGER pid

      Notify_event_func  notify_set_event_func
      EXTERNAL notify_set_event_func
C       Notify_client client
C       Notify_event_func event_func
C       Notify_event_type when

      Notify_prioritizer_func  notify_set_prioritizer_func
      EXTERNAL notify_set_prioritizer_func
C       Notify_client client
C       Notify_prioritizer_func prioritizer_func

      Notify_scheduler_func  notify_set_scheduler_func
      EXTERNAL notify_set_scheduler_func
C       Notify_scheduler_func scheduler_func

      Notify_error  notify_event
      EXTERNAL notify_event
C       Notify_client client
C       Notify_event event
C       Notify_arg arg

      Notify_error  notify_exception
      EXTERNAL notify_exception
C       Notify_client client
C       INTEGER fd

      Notify_error  notify_input
      EXTERNAL notify_input
C       Notify_client client
C       INTEGER fd

      Notify_error  notify_itimer
      EXTERNAL notify_itimer
C       Notify_client client
C       INTEGER which

      Notify_error  notify_output
      EXTERNAL notify_output
C       Notify_client client
C       INTEGER fd

      Notify_error  notify_signal
      EXTERNAL notify_signal
C       Notify_client client
C       INTEGER signal

      Notify_error  notify_wait3
      EXTERNAL notify_wait3
C       Notify_client client

      Notify_error  notify_waitpid
      EXTERNAL notify_waitpid
C       Notify_client client

#endif notify_F.

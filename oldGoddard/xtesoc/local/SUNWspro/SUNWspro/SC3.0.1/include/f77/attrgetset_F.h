#ifndef attrgetset_F_DEFINED
#define attrgetset_F_DEFINED

#include "attr_F.h"
#include "base_F.h"
#include "canvas_F.h"
#include "cursor_F.h"
#include "cms_F.h"
#include "dragdrop_F.h"
#include "drawable_F.h"
#include "font_F.h"
#include "frame_F.h"
#include "fullscreen_F.h"
#include "icon_F.h"
#include "openmenu_F.h"
#include "openwin_F.h"
#include "panel_F.h"
#include "pixrect_F.h"
#include "pkg_public_F.h"
#include "rect_F.h"
#include "screen_F.h"
#include "scrollbar_F.h"
#include "sel_svc_F.h"
#include "server_F.h"
#include "svrimage_F.h"
#include "termsw_F.h"
#include "textsw_F.h"
#include "tty_F.h"
#include "window_F.h"
#include "win_input_F.h"
C  Temporary 
#define Seln RECORD /Seln_holder/
      LOGICAL  get_CANVAS_AUTO_EXPAND
      EXTERNAL get_CANVAS_AUTO_EXPAND
C       Canvas obj

      EXTERNAL set_CANVAS_AUTO_EXPAND
C       Canvas obj
C       LOGICAL v1

      LOGICAL  get_CANVAS_AUTO_SHRINK
      EXTERNAL get_CANVAS_AUTO_SHRINK
C       Canvas obj

      EXTERNAL set_CANVAS_AUTO_SHRINK
C       Canvas obj
C       LOGICAL v1

      LOGICAL  get_CANVAS_AUTO_CLEAR
      EXTERNAL get_CANVAS_AUTO_CLEAR
C       Canvas obj

      EXTERNAL set_CANVAS_AUTO_CLEAR
C       Canvas obj
C       LOGICAL v1

      LOGICAL  get_CANVAS_CMS_REPAINT
      EXTERNAL get_CANVAS_CMS_REPAINT
C       Canvas obj

      EXTERNAL set_CANVAS_CMS_REPAINT
C       Canvas obj
C       LOGICAL v1

      LOGICAL  get_CANVAS_FIXED_IMAGE
      EXTERNAL get_CANVAS_FIXED_IMAGE
C       Canvas obj

      EXTERNAL set_CANVAS_FIXED_IMAGE
C       Canvas obj
C       LOGICAL v1

      INTEGER  get_CANVAS_HEIGHT
      EXTERNAL get_CANVAS_HEIGHT
C       Canvas obj

      EXTERNAL set_CANVAS_HEIGHT
C       Canvas obj
C       INTEGER v1

      INTEGER  get_CANVAS_MIN_PAINT_HEIGHT
      EXTERNAL get_CANVAS_MIN_PAINT_HEIGHT
C       Canvas obj

      EXTERNAL set_CANVAS_MIN_PAINT_HEIGHT
C       Canvas obj
C       INTEGER v1

      INTEGER  get_CANVAS_MIN_PAINT_WIDTH
      EXTERNAL get_CANVAS_MIN_PAINT_WIDTH
C       Canvas obj

      EXTERNAL set_CANVAS_MIN_PAINT_WIDTH
C       Canvas obj
C       INTEGER v1

      LOGICAL  get_CANVAS_NO_CLIPPING
      EXTERNAL get_CANVAS_NO_CLIPPING
C       Canvas obj

      EXTERNAL set_CANVAS_NO_CLIPPING
C       Canvas obj
C       LOGICAL v1

      Xv_opaque  get_CANVAS_NTH_PAINT_WINDOW
      EXTERNAL get_CANVAS_NTH_PAINT_WINDOW
C       Canvas obj
C       INTEGER v1

      Xv_opaque  get_CANVAS_PAINT_CANVAS_WINDOW
      EXTERNAL get_CANVAS_PAINT_CANVAS_WINDOW
C       Canvas obj

      Xv_opaque  get_CANVAS_PAINT_VIEW_WINDOW
      EXTERNAL get_CANVAS_PAINT_VIEW_WINDOW
C       Canvas obj

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_1
C       Canvas obj
C       Attr_attribute v1

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_2
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_3
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_4
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_5
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_6
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_7
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7

      EXTERNAL set_CANVAS_PAINTWINDOW_ATTRS_8
C       Canvas obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7
C       Attr_attribute v8

      Canvas_repaint_procp  get_CANVAS_REPAINT_PROC
      EXTERNAL get_CANVAS_REPAINT_PROC
C       Canvas obj

      EXTERNAL set_CANVAS_REPAINT_PROC
C       Canvas obj
C       Canvas_repaint_procp v1

      Canvas_resize_procp  get_CANVAS_RESIZE_PROC
      EXTERNAL get_CANVAS_RESIZE_PROC
C       Canvas obj

      EXTERNAL set_CANVAS_RESIZE_PROC
C       Canvas obj
C       Canvas_resize_procp v1

      LOGICAL  get_CANVAS_RETAINED
      EXTERNAL get_CANVAS_RETAINED
C       Canvas obj

      EXTERNAL set_CANVAS_RETAINED
C       Canvas obj
C       LOGICAL v1

      Xv_opaque  get_CANVAS_VIEW_CANVAS_WINDOW
      EXTERNAL get_CANVAS_VIEW_CANVAS_WINDOW
C       Canvas obj

      INTEGER  get_CANVAS_VIEW_MARGIN
      EXTERNAL get_CANVAS_VIEW_MARGIN
C       Canvas obj

      EXTERNAL set_CANVAS_VIEW_MARGIN
C       Canvas obj
C       INTEGER v1

      Xv_opaque  get_CANVAS_VIEW_PAINT_WINDOW
      EXTERNAL get_CANVAS_VIEW_PAINT_WINDOW
C       Canvas obj

      EXTERNAL get_CANVAS_VIEWABLE_RECT
C       RECORD /Rect/  returnval__
C       Canvas obj

      INTEGER  get_CANVAS_WIDTH
      EXTERNAL get_CANVAS_WIDTH
C       Canvas obj

      EXTERNAL set_CANVAS_WIDTH
C       Canvas obj
C       INTEGER v1

      LOGICAL  get_CANVAS_X_PAINT_WINDOW
      EXTERNAL get_CANVAS_X_PAINT_WINDOW
C       Canvas obj

      EXTERNAL set_CANVAS_X_PAINT_WINDOW
C       Canvas obj
C       LOGICAL v1

      INTEGER  get_CMS_BACKGROUND_PIXEL
      EXTERNAL get_CMS_BACKGROUND_PIXEL
C       Cms obj

      INTEGER  get_CMS_COLORS
      EXTERNAL get_CMS_COLORS
C       Cms obj
C       RECORD /Xv_singlecolor/ v1

      EXTERNAL set_CMS_COLORS
C       Cms obj
C       RECORD /Xv_singlecolor/ v1

      EXTERNAL set_CMS_COLOR_COUNT
C       Cms obj
C       INTEGER v1

      LOGICAL  get_CMS_CONTROL_CMS
      EXTERNAL get_CMS_CONTROL_CMS
C       Cms obj

      EXTERNAL set_CMS_CONTROL_CMS
C       Cms obj
C       LOGICAL v1

      INTEGER  get_CMS_FOREGROUND_PIXEL
      EXTERNAL get_CMS_FOREGROUND_PIXEL
C       Cms obj

      EXTERNAL set_CMS_INDEX
C       Cms obj
C       INTEGER v1

      Xv_opaque  get_CMS_INDEX_TABLE
      EXTERNAL get_CMS_INDEX_TABLE
C       Cms obj

      Cstringp  get_CMS_NAME
      EXTERNAL get_CMS_NAME
C       Cms obj

      EXTERNAL set_CMS_NAME
C       Cms obj
C       CHARACTER v1

      EXTERNAL set_CMS_NAMED_COLORS_1
C       Cms obj
C       CHARACTER v1

      EXTERNAL set_CMS_NAMED_COLORS_2
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_CMS_NAMED_COLORS_3
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_CMS_NAMED_COLORS_4
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_CMS_NAMED_COLORS_5
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_CMS_NAMED_COLORS_6
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_CMS_NAMED_COLORS_7
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_CMS_NAMED_COLORS_8
C       Cms obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      INTEGER  get_CMS_PIXEL
      EXTERNAL get_CMS_PIXEL
C       Cms obj
C       INTEGER v1

      Xv_opaque  get_CMS_SCREEN
      EXTERNAL get_CMS_SCREEN
C       Cms obj

      INTEGER  get_CMS_SIZE
      EXTERNAL get_CMS_SIZE
C       Cms obj

      Xv_opaque  get_CMS_TYPE
      EXTERNAL get_CMS_TYPE
C       Cms obj

      INTEGER  get_CMS_X_COLORS
      EXTERNAL get_CMS_X_COLORS
C       Cms obj
C       RECORD /XColor/ v1

      EXTERNAL set_CMS_X_COLORS
C       Cms obj
C       RECORD /XColor/ v1

      EXTERNAL get_CURSOR_BACKGROUND_COLOR
C       RECORD /Xv_singlecolor/  returnval__
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_BACKGROUND_COLOR
C       Xv_Cursor obj
C       RECORD /Xv_singlecolor/ v1

      Xv_opaque  get_CURSOR_BASIC_PTR
      EXTERNAL get_CURSOR_BASIC_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_BASIC_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      Xv_opaque  get_CURSOR_BUSY_PTR
      EXTERNAL get_CURSOR_BUSY_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_BUSY_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      Xv_opaque  get_CURSOR_COPY_PTR
      EXTERNAL get_CURSOR_COPY_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_COPY_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      EXTERNAL get_CURSOR_FOREGROUND_COLOR
C       RECORD /Xv_singlecolor/  returnval__
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_FOREGROUND_COLOR
C       Xv_Cursor obj
C       RECORD /Xv_singlecolor/ v1

      Server_image  get_CURSOR_IMAGE
      EXTERNAL get_CURSOR_IMAGE
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_IMAGE
C       Xv_Cursor obj
C       Server_image v1

      CHARACTER  get_CURSOR_MASK_CHAR
      EXTERNAL get_CURSOR_MASK_CHAR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_MASK_CHAR
C       Xv_Cursor obj
C       CHARACTER v1

      Xv_opaque  get_CURSOR_MOVE_PTR
      EXTERNAL get_CURSOR_MOVE_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_MOVE_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      Xv_opaque  get_CURSOR_NAVIGATION_LEVEL_PTR
      EXTERNAL get_CURSOR_NAVIGATION_LEVEL_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_NAVIGATION_LEVEL_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      INTEGER  get_CURSOR_OP
      EXTERNAL get_CURSOR_OP
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_OP
C       Xv_Cursor obj
C       INTEGER v1

      Xv_opaque  get_CURSOR_PANNING_PTR
      EXTERNAL get_CURSOR_PANNING_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_PANNING_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      LOGICAL  get_CURSOR_SHOW_CURSOR
      EXTERNAL get_CURSOR_SHOW_CURSOR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_SHOW_CURSOR
C       Xv_Cursor obj
C       LOGICAL v1

      CHARACTER  get_CURSOR_SRC_CHAR
      EXTERNAL get_CURSOR_SRC_CHAR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_SRC_CHAR
C       Xv_Cursor obj
C       CHARACTER v1

      INTEGER  get_CURSOR_XHOT
      EXTERNAL get_CURSOR_XHOT
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_XHOT
C       Xv_Cursor obj
C       INTEGER v1

      INTEGER  get_CURSOR_YHOT
      EXTERNAL get_CURSOR_YHOT
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_YHOT
C       Xv_Cursor obj
C       INTEGER v1

      Xv_opaque  get_CURSOR_STOP_PTR
      EXTERNAL get_CURSOR_STOP_PTR
C       Xv_Cursor obj

      EXTERNAL set_CURSOR_STOP_PTR
C       Xv_Cursor obj
C       Xv_opaque v1

      Xv_opaque  get_DRAWABLE_INFO
      EXTERNAL get_DRAWABLE_INFO
C       Xv_Drawable obj

      EXTERNAL set_DRAWABLE_INFO
C       Xv_Drawable obj
C       Xv_opaque v1

      Xv_opaque  get_DND_ACCEPT_CURSOR
      EXTERNAL get_DND_ACCEPT_CURSOR
C       Dnd obj

      EXTERNAL set_DND_ACCEPT_CURSOR
C       Dnd obj
C       Xv_opaque v1

      INTEGER  get_DND_ACCEPT_X_CURSOR
      EXTERNAL get_DND_ACCEPT_X_CURSOR
C       Dnd obj

      EXTERNAL set_DND_ACCEPT_X_CURSOR
C       Dnd obj
C       INTEGER v1

      Xv_opaque  get_DND_CURSOR
      EXTERNAL get_DND_CURSOR
C       Dnd obj

      EXTERNAL set_DND_CURSOR
C       Dnd obj
C       Xv_opaque v1

      INTEGER  get_DND_TYPE
      EXTERNAL get_DND_TYPE
C       Dnd obj

      EXTERNAL set_DND_TYPE
C       Dnd obj
C       INTEGER v1

      INTEGER  get_DND_X_CURSOR
      EXTERNAL get_DND_X_CURSOR
C       Dnd obj

      EXTERNAL set_DND_X_CURSOR
C       Dnd obj
C       INTEGER v1

      LOGICAL  get_DROP_SITE_DEFAULT
      EXTERNAL get_DROP_SITE_DEFAULT
C       Xv_drop_site obj

      EXTERNAL set_DROP_SITE_DEFAULT
C       Xv_drop_site obj
C       LOGICAL v1

      Xv_opaque  get_DROP_SITE_DELETE_REGION
      EXTERNAL get_DROP_SITE_DELETE_REGION
C       Xv_drop_site obj

      EXTERNAL set_DROP_SITE_DELETE_REGION
C       Xv_drop_site obj
C       Xv_opaque v1

      Xv_opaque  get_DROP_SITE_REGION
      EXTERNAL get_DROP_SITE_REGION
C       Xv_drop_site obj

      EXTERNAL set_DROP_SITE_REGION
C       Xv_drop_site obj
C       Xv_opaque v1

      INTEGER  get_FONT_CHAR_HEIGHT
      EXTERNAL get_FONT_CHAR_HEIGHT
C       Xv_Font obj
C       CHARACTER v1

      INTEGER  get_FONT_CHAR_WIDTH
      EXTERNAL get_FONT_CHAR_WIDTH
C       Xv_Font obj
C       CHARACTER v1

      INTEGER  get_FONT_DEFAULT_CHAR_HEIGHT
      EXTERNAL get_FONT_DEFAULT_CHAR_HEIGHT
C       Xv_Font obj

      INTEGER  get_FONT_DEFAULT_CHAR_WIDTH
      EXTERNAL get_FONT_DEFAULT_CHAR_WIDTH
C       Xv_Font obj

      Cstringp  get_FONT_FAMILY
      EXTERNAL get_FONT_FAMILY
C       Xv_Font obj

      INTEGER  get_FONT_HEAD
      EXTERNAL get_FONT_HEAD
C       Xv_Font obj

      EXTERNAL set_FONT_HEAD
C       Xv_Font obj
C       INTEGER v1

      EXTERNAL get_FONT_INFO
C       RECORD /XFontStruct/  returnval__
C       Xv_Font obj

      Cstringp  get_FONT_NAME
      EXTERNAL get_FONT_NAME
C       Xv_Font obj

      EXTERNAL get_FONT_PIXFONT
C       RECORD /Pixfont/  returnval__
C       Xv_Font obj

      Frame_rescale_state  get_FONT_SCALE
      EXTERNAL get_FONT_SCALE
C       Xv_Font obj

      INTEGER  get_FONT_SIZE
      EXTERNAL get_FONT_SIZE
C       Xv_Font obj

      INTEGER  get_FONT_STRING_DIMS
      EXTERNAL get_FONT_STRING_DIMS
C       Xv_Font obj
C       CHARACTER v1
C       RECORD /Font_string_dims/ v2

      Cstringp  get_FONT_STYLE
      EXTERNAL get_FONT_STYLE
C       Xv_Font obj

      INTEGER  get_FONT_TYPE
      EXTERNAL get_FONT_TYPE
C       Xv_Font obj

      INTEGER  get_FONT_UNKNOWN_HEAD
      EXTERNAL get_FONT_UNKNOWN_HEAD
C       Xv_Font obj

      EXTERNAL set_FONT_UNKNOWN_HEAD
C       Xv_Font obj
C       INTEGER v1

      EXTERNAL set_FRAME_ARGC_PTR_ARGV
C       Frame obj
C       INTEGER v1
C       Cstringp_arrayp v2

      EXTERNAL set_FRAME_ARGS
C       Frame obj
C       Cstringp_arrayp v1

      EXTERNAL get_FRAME_BACKGROUND_COLOR
C       RECORD /Xv_singlecolor/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_BACKGROUND_COLOR
C       Frame obj
C       RECORD /Xv_singlecolor/ v1

      LOGICAL  get_FRAME_BUSY
      EXTERNAL get_FRAME_BUSY
C       Frame obj

      EXTERNAL set_FRAME_BUSY
C       Frame obj
C       LOGICAL v1

      LOGICAL  get_FRAME_CLOSED
      EXTERNAL get_FRAME_CLOSED
C       Frame obj

      EXTERNAL set_FRAME_CLOSED
C       Frame obj
C       LOGICAL v1

      EXTERNAL get_FRAME_CLOSED_RECT
C       RECORD /Rect/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_CLOSED_RECT
C       Frame obj
C       RECORD /Rect/ v1

      Frame_cmdline_help_procp  get_FRAME_CMDLINE_HELP_PROC
      EXTERNAL get_FRAME_CMDLINE_HELP_PROC
C       Frame obj

      EXTERNAL set_FRAME_CMDLINE_HELP_PROC
C       Frame obj
C       Frame_cmdline_help_procp v1

      Xv_opaque  get_FRAME_CMD_PANEL
      EXTERNAL get_FRAME_CMD_PANEL
C       Frame obj

      EXTERNAL set_FRAME_CMD_PANEL
C       Frame obj
C       Xv_opaque v1

      LOGICAL  get_FRAME_CMD_PUSHPIN_IN
      EXTERNAL get_FRAME_CMD_PUSHPIN_IN
C       Frame obj

      EXTERNAL set_FRAME_CMD_PUSHPIN_IN
C       Frame obj
C       LOGICAL v1

      EXTERNAL get_FRAME_CURRENT_RECT
C       RECORD /Rect/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_CURRENT_RECT
C       Frame obj
C       RECORD /Rect/ v1

      Frame_done_procp  get_FRAME_DEFAULT_DONE_PROC
      EXTERNAL get_FRAME_DEFAULT_DONE_PROC
C       Frame obj

      EXTERNAL set_FRAME_DEFAULT_DONE_PROC
C       Frame obj
C       Frame_done_procp v1

      Frame_done_procp  get_FRAME_DONE_PROC
      EXTERNAL get_FRAME_DONE_PROC
C       Frame obj

      EXTERNAL set_FRAME_DONE_PROC
C       Frame obj
C       Frame_done_procp v1

      EXTERNAL set_FRAME_FOCUS_DIRECTION
C       Frame obj
C       INTEGER v1

      Xv_Window  get_FRAME_FOCUS_WIN
      EXTERNAL get_FRAME_FOCUS_WIN
C       Frame obj

      EXTERNAL get_FRAME_FOREGROUND_COLOR
C       RECORD /Xv_singlecolor/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_FOREGROUND_COLOR
C       Frame obj
C       RECORD /Xv_singlecolor/ v1

      LOGICAL  get_FRAME_GROUP_LEADER
      EXTERNAL get_FRAME_GROUP_LEADER
C       Frame obj

      EXTERNAL set_FRAME_GROUP_LEADER
C       Frame obj
C       LOGICAL v1

      Icon  get_FRAME_ICON
      EXTERNAL get_FRAME_ICON
C       Frame obj

      EXTERNAL set_FRAME_ICON
C       Frame obj
C       Icon v1

      LOGICAL  get_FRAME_INHERIT_COLORS
      EXTERNAL get_FRAME_INHERIT_COLORS
C       Frame obj

      EXTERNAL set_FRAME_INHERIT_COLORS
C       Frame obj
C       LOGICAL v1

      Cstringp  get_FRAME_LABEL
      EXTERNAL get_FRAME_LABEL
C       Frame obj

      EXTERNAL set_FRAME_LABEL
C       Frame obj
C       CHARACTER v1

      Cstringp  get_FRAME_LEFT_FOOTER
      EXTERNAL get_FRAME_LEFT_FOOTER
C       Frame obj

      EXTERNAL set_FRAME_LEFT_FOOTER
C       Frame obj
C       CHARACTER v1

      LOGICAL  get_FRAME_NO_CONFIRM
      EXTERNAL get_FRAME_NO_CONFIRM
C       Frame obj

      EXTERNAL set_FRAME_NO_CONFIRM
C       Frame obj
C       LOGICAL v1

      Frame  get_FRAME_NTH_SUBFRAME
      EXTERNAL get_FRAME_NTH_SUBFRAME
C       Frame obj
C       INTEGER v1

      Xv_Window  get_FRAME_NTH_SUBWINDOW
      EXTERNAL get_FRAME_NTH_SUBWINDOW
C       Frame obj
C       INTEGER v1

      EXTERNAL get_FRAME_OLD_RECT
C       RECORD /Rect/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_OLD_RECT
C       Frame obj
C       RECORD /Rect/ v1

      EXTERNAL get_FRAME_OPEN_RECT
C       RECORD /Rect/  returnval__
C       Frame obj

      EXTERNAL set_FRAME_OPEN_RECT
C       Frame obj
C       RECORD /Rect/ v1

      ProcPointer  get_FRAME_PROPERTIES_PROC
      EXTERNAL get_FRAME_PROPERTIES_PROC
C       Frame obj

      EXTERNAL set_FRAME_PROPERTIES_PROC
C       Frame obj
C       ProcPointer v1

      LOGICAL  get_FRAME_PROPS_PUSHPIN_IN
      EXTERNAL get_FRAME_PROPS_PUSHPIN_IN
C       Frame obj

      EXTERNAL set_FRAME_PROPS_PUSHPIN_IN
C       Frame obj
C       LOGICAL v1

      Xv_opaque  get_FRAME_PROPS_PANEL
      EXTERNAL get_FRAME_PROPS_PANEL
C       Frame obj

      EXTERNAL set_FRAME_PROPS_PANEL
C       Frame obj
C       Xv_opaque v1

      Cstringp  get_FRAME_RIGHT_FOOTER
      EXTERNAL get_FRAME_RIGHT_FOOTER
C       Frame obj

      EXTERNAL set_FRAME_RIGHT_FOOTER
C       Frame obj
C       CHARACTER v1

      LOGICAL  get_FRAME_SHOW_FOOTER
      EXTERNAL get_FRAME_SHOW_FOOTER
C       Frame obj

      EXTERNAL set_FRAME_SHOW_FOOTER
C       Frame obj
C       LOGICAL v1

      LOGICAL  get_FRAME_SHOW_LABEL
      EXTERNAL get_FRAME_SHOW_LABEL
C       Frame obj

      EXTERNAL set_FRAME_SHOW_LABEL
C       Frame obj
C       LOGICAL v1

      LOGICAL  get_FRAME_SHOW_RESIZE_CORNER
      EXTERNAL get_FRAME_SHOW_RESIZE_CORNER
C       Frame obj

      EXTERNAL set_FRAME_SHOW_RESIZE_CORNER
C       Frame obj
C       LOGICAL v1

      INTEGER  get_XV_BOTTOM_MARGIN
      EXTERNAL get_XV_BOTTOM_MARGIN
C       Xv_object obj

      EXTERNAL set_XV_BOTTOM_MARGIN
C       Xv_object obj
C       INTEGER v1

      Xv_opaque  get_XV_DEPTH
      EXTERNAL get_XV_DEPTH
C       Xv_object obj

      INTEGER  get_XV_DEVICE_NUMBER
      EXTERNAL get_XV_DEVICE_NUMBER
C       Xv_object obj

      EXTERNAL set_XV_DEVICE_NUMBER
C       Xv_object obj
C       INTEGER v1

      Xv_opaque  get_XV_DISPLAY
      EXTERNAL get_XV_DISPLAY
C       Xv_object obj

      EXTERNAL set_XV_DISPLAY
C       Xv_object obj
C       Xv_opaque v1

      LOGICAL  get_XV_FOCUS_ELEMENT
      EXTERNAL get_XV_FOCUS_ELEMENT
C       Xv_object obj

      EXTERNAL set_XV_FOCUS_ELEMENT
C       Xv_object obj
C       LOGICAL v1

      Xv_Font  get_XV_FONT
      EXTERNAL get_XV_FONT
C       Xv_object obj

      EXTERNAL set_XV_FONT
C       Xv_object obj
C       Xv_Font v1

      Xv_opaque  get_XV_GC
      EXTERNAL get_XV_GC
C       Xv_object obj

      EXTERNAL set_XV_GC
C       Xv_object obj
C       Xv_opaque v1

      INTEGER  get_XV_HEIGHT
      EXTERNAL get_XV_HEIGHT
C       Xv_object obj

      EXTERNAL set_XV_HEIGHT
C       Xv_object obj
C       INTEGER v1

      Cstringp  get_XV_HELP
      EXTERNAL get_XV_HELP
C       Xv_object obj

      EXTERNAL set_XV_HELP
C       Xv_object obj
C       CHARACTER v1

      LOGICAL  get_XV_IS_SUBTYPE_OF
      EXTERNAL get_XV_IS_SUBTYPE_OF
C       Xv_object obj
C       Xv_opaque v1

      Xv_opaque  get_XV_KEY_DATA
      EXTERNAL get_XV_KEY_DATA
C       Xv_object obj
C       INTEGER v1

      EXTERNAL set_XV_KEY_DATA
C       Xv_object obj
C       INTEGER v1
C       Xv_opaque v2

      ProcPointer  get_XV_KEY_DATA_COPY_PROC
      EXTERNAL get_XV_KEY_DATA_COPY_PROC
C       Xv_object obj

      EXTERNAL set_XV_KEY_DATA_COPY_PROC
C       Xv_object obj
C       ProcPointer v1

      EXTERNAL set_XV_KEY_DATA_REMOVE
C       Xv_object obj
C       INTEGER v1

      ProcPointer  get_XV_KEY_DATA_REMOVE_PROC
      EXTERNAL get_XV_KEY_DATA_REMOVE_PROC
C       Xv_object obj

      EXTERNAL set_XV_KEY_DATA_REMOVE_PROC
C       Xv_object obj
C       ProcPointer v1

      Cstringp  get_XV_LABEL
      EXTERNAL get_XV_LABEL
C       Xv_object obj

      EXTERNAL set_XV_LABEL
C       Xv_object obj
C       CHARACTER v1

      Cstringp  get_XV_LC_BASIC_LOCALE
      EXTERNAL get_XV_LC_BASIC_LOCALE
C       Xv_object obj

      EXTERNAL set_XV_LC_BASIC_LOCALE
C       Xv_object obj
C       CHARACTER v1

      Cstringp  get_XV_LC_DISPLAY_LANG
      EXTERNAL get_XV_LC_DISPLAY_LANG
C       Xv_object obj

      EXTERNAL set_XV_LC_DISPLAY_LANG
C       Xv_object obj
C       CHARACTER v1

      Cstringp  get_XV_LC_INPUT_LANG
      EXTERNAL get_XV_LC_INPUT_LANG
C       Xv_object obj

      EXTERNAL set_XV_LC_INPUT_LANG
C       Xv_object obj
C       CHARACTER v1

      Cstringp  get_XV_LC_NUMERIC
      EXTERNAL get_XV_LC_NUMERIC
C       Xv_object obj

      EXTERNAL set_XV_LC_NUMERIC
C       Xv_object obj
C       CHARACTER v1

      Cstringp  get_XV_LC_TIME_FORMAT
      EXTERNAL get_XV_LC_TIME_FORMAT
C       Xv_object obj

      EXTERNAL set_XV_LC_TIME_FORMAT
C       Xv_object obj
C       CHARACTER v1

      INTEGER  get_XV_LEFT_MARGIN
      EXTERNAL get_XV_LEFT_MARGIN
C       Xv_object obj

      EXTERNAL set_XV_LEFT_MARGIN
C       Xv_object obj
C       INTEGER v1

      Cstringp  get_XV_LOCALE_DIR
      EXTERNAL get_XV_LOCALE_DIR
C       Xv_object obj

      EXTERNAL set_XV_LOCALE_DIR
C       Xv_object obj
C       CHARACTER v1

      INTEGER  get_XV_MARGIN
      EXTERNAL get_XV_MARGIN
C       Xv_object obj

      EXTERNAL set_XV_MARGIN
C       Xv_object obj
C       INTEGER v1

      Cstringp  get_XV_NAME
      EXTERNAL get_XV_NAME
C       Xv_object obj

      EXTERNAL set_XV_NAME
C       Xv_object obj
C       CHARACTER v1

      Xv_opaque  get_XV_OWNER
      EXTERNAL get_XV_OWNER
C       Xv_object obj

      EXTERNAL get_XV_RECT
C       RECORD /Rect/  returnval__
C       Xv_object obj

      EXTERNAL set_XV_RECT
C       Xv_object obj
C       RECORD /Rect/ v1

      INTEGER  get_XV_REF_COUNT
      EXTERNAL get_XV_REF_COUNT
C       Xv_object obj

      EXTERNAL set_XV_REF_COUNT
C       Xv_object obj
C       INTEGER v1

      INTEGER  get_XV_RIGHT_MARGIN
      EXTERNAL get_XV_RIGHT_MARGIN
C       Xv_object obj

      EXTERNAL set_XV_RIGHT_MARGIN
C       Xv_object obj
C       INTEGER v1

      Xv_object  get_XV_ROOT
      EXTERNAL get_XV_ROOT
C       Xv_object obj

      Xv_opaque  get_XV_SCREEN
      EXTERNAL get_XV_SCREEN
C       Xv_object obj

      Xv_opaque  get_XV_SELF
      EXTERNAL get_XV_SELF
C       Xv_object obj

      LOGICAL  get_XV_SHOW
      EXTERNAL get_XV_SHOW
C       Xv_object obj

      EXTERNAL set_XV_SHOW
C       Xv_object obj
C       LOGICAL v1

      INTEGER  get_XV_TOP_MARGIN
      EXTERNAL get_XV_TOP_MARGIN
C       Xv_object obj

      EXTERNAL set_XV_TOP_MARGIN
C       Xv_object obj
C       INTEGER v1

      Xv_pkg_ptr  get_XV_TYPE
      EXTERNAL get_XV_TYPE
C       Xv_object obj

      Xv_opaque  get_XV_VISUAL
      EXTERNAL get_XV_VISUAL
C       Xv_object obj

      Xv_opaque  get_XV_VISUAL_CLASS
      EXTERNAL get_XV_VISUAL_CLASS
C       Xv_object obj

      INTEGER  get_XV_WIDTH
      EXTERNAL get_XV_WIDTH
C       Xv_object obj

      EXTERNAL set_XV_WIDTH
C       Xv_object obj
C       INTEGER v1

      LOGICAL  get_XV_USE_LOCALE
      EXTERNAL get_XV_USE_LOCALE
C       Xv_object obj

      EXTERNAL set_XV_USE_LOCALE
C       Xv_object obj
C       LOGICAL v1

      INTEGER  get_XV_X
      EXTERNAL get_XV_X
C       Xv_object obj

      EXTERNAL set_XV_X
C       Xv_object obj
C       INTEGER v1

      Cstringp  get_XV_XNAME
      EXTERNAL get_XV_XNAME
C       Xv_object obj

      INTEGER  get_XV_XID
      EXTERNAL get_XV_XID
C       Xv_object obj

      INTEGER  get_XV_Y
      EXTERNAL get_XV_Y
C       Xv_object obj

      EXTERNAL set_XV_Y
C       Xv_object obj
C       INTEGER v1

      EXTERNAL set_FULLSCREEN_ALLOW_EVENTS
C       Fullscreen obj
C       Xv_opaque v1

      EXTERNAL set_FULLSCREEN_ALLOW_SYNC_EVENT
C       Fullscreen obj

      Xv_opaque  get_FULLSCREEN_CURSOR_WINDOW
      EXTERNAL get_FULLSCREEN_CURSOR_WINDOW
C       Fullscreen obj

      LOGICAL  get_FULLSCREEN_GRAB_KEYBOARD
      EXTERNAL get_FULLSCREEN_GRAB_KEYBOARD
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_GRAB_KEYBOARD
C       Fullscreen obj
C       LOGICAL v1

      LOGICAL  get_FULLSCREEN_GRAB_POINTER
      EXTERNAL get_FULLSCREEN_GRAB_POINTER
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_GRAB_POINTER
C       Fullscreen obj
C       LOGICAL v1

      LOGICAL  get_FULLSCREEN_GRAB_SERVER
      EXTERNAL get_FULLSCREEN_GRAB_SERVER
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_GRAB_SERVER
C       Fullscreen obj
C       LOGICAL v1

      Xv_opaque  get_FULLSCREEN_INPUT_WINDOW
      EXTERNAL get_FULLSCREEN_INPUT_WINDOW
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_INPUT_WINDOW
C       Fullscreen obj
C       Xv_opaque v1

      Fullscreen_grab_mode  get_FULLSCREEN_KEYBOARD_GRAB_PTR
      EXTERNAL get_FULLSCREEN_KEYBOARD_GRAB_PTR
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_KEYBOARD_GRAB_PTR
C       Fullscreen obj
C       Fullscreen_grab_mode v1

      Fullscreen_grab_mode  get_FULLSCREEN_KEYBOARD_GRAB_KBD
      EXTERNAL get_FULLSCREEN_KEYBOARD_GRAB_KBD
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_KEYBOARD_GRAB_KBD
C       Fullscreen obj
C       Fullscreen_grab_mode v1

      LOGICAL  get_FULLSCREEN_OWNER_EVENTS
      EXTERNAL get_FULLSCREEN_OWNER_EVENTS
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_OWNER_EVENTS
C       Fullscreen obj
C       LOGICAL v1

      Xv_opaque  get_FULLSCREEN_PAINT_WINDOW
      EXTERNAL get_FULLSCREEN_PAINT_WINDOW
C       Fullscreen obj

      Fullscreen_grab_mode  get_FULLSCREEN_POINTER_GRAB_PTR_
      EXTERNAL get_FULLSCREEN_POINTER_GRAB_PTR_
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_POINTER_GRAB_PTR_
C       Fullscreen obj
C       Fullscreen_grab_mode v1

      Fullscreen_grab_mode  get_FULLSCREEN_POINTER_GRAB_KBD_
      EXTERNAL get_FULLSCREEN_POINTER_GRAB_KBD_
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_POINTER_GRAB_KBD_
C       Fullscreen obj
C       Fullscreen_grab_mode v1

      EXTERNAL get_FULLSCREEN_RECT
C       RECORD /Rect/  returnval__
C       Fullscreen obj

      LOGICAL  get_FULLSCREEN_SYNC
      EXTERNAL get_FULLSCREEN_SYNC
C       Fullscreen obj

      EXTERNAL set_FULLSCREEN_SYNC
C       Fullscreen obj
C       LOGICAL v1

      Xv_Font  get_ICON_FONT
      EXTERNAL get_ICON_FONT
C       Icon obj

      EXTERNAL set_ICON_FONT
C       Icon obj
C       Xv_Font v1

      INTEGER  get_ICON_HEIGHT
      EXTERNAL get_ICON_HEIGHT
C       Icon obj

      EXTERNAL set_ICON_HEIGHT
C       Icon obj
C       INTEGER v1

      Server_image  get_ICON_IMAGE
      EXTERNAL get_ICON_IMAGE
C       Icon obj

      EXTERNAL set_ICON_IMAGE
C       Icon obj
C       Server_image v1

      EXTERNAL get_ICON_IMAGE_RECT
C       RECORD /Rect/  returnval__
C       Icon obj

      EXTERNAL set_ICON_IMAGE_RECT
C       Icon obj
C       RECORD /Rect/ v1

      Cstringp  get_ICON_LABEL
      EXTERNAL get_ICON_LABEL
C       Icon obj

      EXTERNAL set_ICON_LABEL
C       Icon obj
C       CHARACTER v1

      EXTERNAL get_ICON_LABEL_RECT
C       RECORD /Rect/  returnval__
C       Icon obj

      EXTERNAL set_ICON_LABEL_RECT
C       Icon obj
C       RECORD /Rect/ v1

      Server_image  get_ICON_MASK_IMAGE
      EXTERNAL get_ICON_MASK_IMAGE
C       Icon obj

      EXTERNAL set_ICON_MASK_IMAGE
C       Icon obj
C       Server_image v1

      LOGICAL  get_ICON_TRANSPARENT
      EXTERNAL get_ICON_TRANSPARENT
C       Icon obj

      EXTERNAL set_ICON_TRANSPARENT
C       Icon obj
C       LOGICAL v1

      Cstringp  get_ICON_TRANSPARENT_LABEL
      EXTERNAL get_ICON_TRANSPARENT_LABEL
C       Icon obj

      EXTERNAL set_ICON_TRANSPARENT_LABEL
C       Icon obj
C       CHARACTER v1

      INTEGER  get_ICON_WIDTH
      EXTERNAL get_ICON_WIDTH
C       Icon obj

      EXTERNAL set_ICON_WIDTH
C       Icon obj
C       INTEGER v1

      Menu_action_procp  get_MENU_ACTION
      EXTERNAL get_MENU_ACTION
C       Menu obj

      EXTERNAL set_MENU_ACTION
C       Menu obj
C       Menu_action_procp v1

      EXTERNAL set_MENU_ACTION_IMAGE
C       Menu obj
C       Server_image v1
C       Menu_action_procp v2

      EXTERNAL set_MENU_ACTION_ITEM
C       Menu obj
C       CHARACTER v1
C       Menu_action_procp v2

      Menu_action_procp  get_MENU_ACTION_PROC
      EXTERNAL get_MENU_ACTION_PROC
C       Menu obj

      EXTERNAL set_MENU_ACTION_PROC
C       Menu obj
C       Menu_action_procp v1

      EXTERNAL set_MENU_APPEND_ITEM
C       Menu obj
C       Menu_item v1

      Menu_class  get_MENU_CLASS
      EXTERNAL get_MENU_CLASS
C       Menu obj

      EXTERNAL set_MENU_CLASS
C       Menu obj
C       Menu_class v1

      Xv_opaque  get_MENU_CLIENT_DATA
      EXTERNAL get_MENU_CLIENT_DATA
C       Menu obj

      EXTERNAL set_MENU_CLIENT_DATA
C       Menu obj
C       Xv_opaque v1

      INTEGER  get_MENU_COLOR
      EXTERNAL get_MENU_COLOR
C       Menu obj

      EXTERNAL set_MENU_COLOR
C       Menu obj
C       INTEGER v1

      LOGICAL  get_MENU_COL_MAJOR
      EXTERNAL get_MENU_COL_MAJOR
C       Menu obj

      EXTERNAL set_MENU_COL_MAJOR
C       Menu obj
C       LOGICAL v1

      INTEGER  get_MENU_DEFAULT
      EXTERNAL get_MENU_DEFAULT
C       Menu obj

      EXTERNAL set_MENU_DEFAULT
C       Menu obj
C       INTEGER v1

      Menu_item  get_MENU_DEFAULT_ITEM
      EXTERNAL get_MENU_DEFAULT_ITEM
C       Menu obj

      EXTERNAL set_MENU_DEFAULT_ITEM
C       Menu obj
C       Menu_item v1

      INTEGER  get_MENU_DISABLE_ITEM
      EXTERNAL get_MENU_DISABLE_ITEM
C       Menu obj

      EXTERNAL set_MENU_DISABLE_ITEM
C       Menu obj
C       INTEGER v1

      Menu_done_procp  get_MENU_DONE_PROC
      EXTERNAL get_MENU_DONE_PROC
C       Menu obj

      EXTERNAL set_MENU_DONE_PROC
C       Menu obj
C       Menu_done_procp v1

      LOGICAL  get_MENU_FEEDBACK
      EXTERNAL get_MENU_FEEDBACK
C       Menu obj

      EXTERNAL set_MENU_FEEDBACK
C       Menu obj
C       LOGICAL v1

      Event_ptr  get_MENU_FIRST_EVENT
      EXTERNAL get_MENU_FIRST_EVENT
C       Menu obj

      EXTERNAL set_MENU_GEN_PIN_WINDOW
C       Menu obj
C       Frame v1
C       CHARACTER v2

      Menu_gen_procp  get_MENU_GEN_PROC
      EXTERNAL get_MENU_GEN_PROC
C       Menu obj

      EXTERNAL set_MENU_GEN_PROC
C       Menu obj
C       Menu_gen_procp v1

      EXTERNAL set_MENU_GEN_PROC_IMAGE
C       Menu obj
C       RECORD /Pixrect/ v1
C       Menu_gen_procp v2

      EXTERNAL set_MENU_GEN_PROC_ITEM
C       Menu obj
C       CHARACTER v1
C       Menu_gen_procp v2

      Menu_gen_procp  get_MENU_GEN_PULLRIGHT
      EXTERNAL get_MENU_GEN_PULLRIGHT
C       Menu obj

      EXTERNAL set_MENU_GEN_PULLRIGHT
C       Menu obj
C       Menu_gen_procp v1

      EXTERNAL set_MENU_GEN_PULLRIGHT_IMAGE
C       Menu obj
C       RECORD /Pixrect/ v1
C       Menu_gen_procp v2

      EXTERNAL set_MENU_GEN_PULLRIGHT_ITEM
C       Menu obj
C       CHARACTER v1
C       Menu_gen_procp v2

      Server_image  get_MENU_IMAGE
      EXTERNAL get_MENU_IMAGE
C       Menu obj

      EXTERNAL set_MENU_IMAGE
C       Menu obj
C       Server_image v1

      EXTERNAL set_MENU_IMAGES_1
C       Menu obj
C       Server_image v1

      EXTERNAL set_MENU_IMAGES_2
C       Menu obj
C       Server_image v1
C       Server_image v2

      EXTERNAL set_MENU_IMAGES_3
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3

      EXTERNAL set_MENU_IMAGES_4
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4

      EXTERNAL set_MENU_IMAGES_5
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5

      EXTERNAL set_MENU_IMAGES_6
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6

      EXTERNAL set_MENU_IMAGES_7
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7

      EXTERNAL set_MENU_IMAGES_8
C       Menu obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7
C       Server_image v8

      Xv_opaque  get_MENU_IMAGE_ITEM
      EXTERNAL get_MENU_IMAGE_ITEM
C       Menu obj

      EXTERNAL set_MENU_IMAGE_ITEM
C       Menu obj
C       Server_image v1

      LOGICAL  get_MENU_INACTIVE
      EXTERNAL get_MENU_INACTIVE
C       Menu obj

      EXTERNAL set_MENU_INACTIVE
C       Menu obj
C       LOGICAL v1

      Menu_item  get_MENU_INSERT
      EXTERNAL get_MENU_INSERT
C       Menu obj

      EXTERNAL set_MENU_INSERT
C       Menu obj
C       INTEGER v1

      EXTERNAL set_MENU_INSERT_ITEM
C       Menu obj
C       Menu_item v1

      LOGICAL  get_MENU_INVERT
      EXTERNAL get_MENU_INVERT
C       Menu obj

      EXTERNAL set_MENU_INVERT
C       Menu obj
C       LOGICAL v1

      EXTERNAL set_MENU_ITEM_1
C       Menu obj
C       Attr_attribute v1

      EXTERNAL set_MENU_ITEM_2
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2

      EXTERNAL set_MENU_ITEM_3
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3

      EXTERNAL set_MENU_ITEM_4
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4

      EXTERNAL set_MENU_ITEM_5
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5

      EXTERNAL set_MENU_ITEM_6
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6

      EXTERNAL set_MENU_ITEM_7
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7

      EXTERNAL set_MENU_ITEM_8
C       Menu obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7
C       Attr_attribute v8

      INTEGER  get_MENU_LAST_EVENT
      EXTERNAL get_MENU_LAST_EVENT
C       Menu obj

      EXTERNAL set_MENU_LAST_EVENT
C       Menu obj

      INTEGER  get_MENU_MENU
      EXTERNAL get_MENU_MENU
C       Menu obj

      EXTERNAL set_MENU_MENU
C       Menu obj

      INTEGER  get_MENU_NCOLS
      EXTERNAL get_MENU_NCOLS
C       Menu obj

      EXTERNAL set_MENU_NCOLS
C       Menu obj
C       INTEGER v1

      INTEGER  get_MENU_NITEMS
      EXTERNAL get_MENU_NITEMS
C       Menu obj

      EXTERNAL set_MENU_NITEMS
C       Menu obj

      Menu_notify_procp  get_MENU_NOTIFY_PROC
      EXTERNAL get_MENU_NOTIFY_PROC
C       Menu obj

      EXTERNAL set_MENU_NOTIFY_PROC
C       Menu obj
C       Menu_notify_procp v1

      INTEGER  get_MENU_NOTIFY_STATUS
      EXTERNAL get_MENU_NOTIFY_STATUS
C       Menu obj

      EXTERNAL set_MENU_NOTIFY_STATUS
C       Menu obj
C       INTEGER v1

      INTEGER  get_MENU_NROWS
      EXTERNAL get_MENU_NROWS
C       Menu obj

      EXTERNAL set_MENU_NROWS
C       Menu obj
C       INTEGER v1

      Menu_item  get_MENU_NTH_ITEM
      EXTERNAL get_MENU_NTH_ITEM
C       Menu obj
C       INTEGER v1

      Xv_opaque  get_MENU_PARENT
      EXTERNAL get_MENU_PARENT
C       Menu obj

      EXTERNAL set_MENU_PARENT
C       Menu obj
C       Xv_opaque v1

      LOGICAL  get_MENU_PIN
      EXTERNAL get_MENU_PIN
C       Menu obj

      EXTERNAL set_MENU_PIN
C       Menu obj
C       LOGICAL v1

      Menu_pin_procp  get_MENU_PIN_PROC
      EXTERNAL get_MENU_PIN_PROC
C       Menu obj

      EXTERNAL set_MENU_PIN_PROC
C       Menu obj
C       Menu_pin_procp v1

      Frame_cmd  get_MENU_PIN_WINDOW
      EXTERNAL get_MENU_PIN_WINDOW
C       Menu obj

      EXTERNAL set_MENU_PIN_WINDOW
C       Menu obj
C       Frame_cmd v1

      Menu  get_MENU_PULLRIGHT
      EXTERNAL get_MENU_PULLRIGHT
C       Menu obj

      EXTERNAL set_MENU_PULLRIGHT
C       Menu obj
C       Menu v1

      INTEGER  get_MENU_PULLRIGHT_DELTA
      EXTERNAL get_MENU_PULLRIGHT_DELTA
C       Menu obj

      EXTERNAL set_MENU_PULLRIGHT_DELTA
C       Menu obj
C       INTEGER v1

      EXTERNAL set_MENU_PULLRIGHT_IMAGE
C       Menu obj
C       Server_image v1
C       Menu v2

      EXTERNAL set_MENU_PULLRIGHT_ITEM
C       Menu obj
C       CHARACTER v1
C       Menu v2

      INTEGER  get_MENU_RELEASE
      EXTERNAL get_MENU_RELEASE
C       Menu obj

      EXTERNAL set_MENU_RELEASE
C       Menu obj

      INTEGER  get_MENU_RELEASE_IMAGE
      EXTERNAL get_MENU_RELEASE_IMAGE
C       Menu obj

      EXTERNAL set_MENU_RELEASE_IMAGE
C       Menu obj

      EXTERNAL set_MENU_REMOVE
C       Menu obj
C       INTEGER v1

      EXTERNAL set_MENU_REMOVE_ITEM
C       Menu obj
C       Menu_item v1

      EXTERNAL set_MENU_REPLACE
C       Menu obj
C       INTEGER v1
C       Menu_item v2

      EXTERNAL set_MENU_REPLACE_ITEM
C       Menu obj
C       Menu_item v1
C       Menu_item v2

      INTEGER  get_MENU_SELECTED
      EXTERNAL get_MENU_SELECTED
C       Menu obj

      EXTERNAL set_MENU_SELECTED
C       Menu obj
C       INTEGER v1

      Menu_item  get_MENU_SELECTED_ITEM
      EXTERNAL get_MENU_SELECTED_ITEM
C       Menu obj

      EXTERNAL set_MENU_SELECTED_ITEM
C       Menu obj
C       Menu_item v1

      Cstringp  get_MENU_STRING
      EXTERNAL get_MENU_STRING
C       Menu obj

      EXTERNAL set_MENU_STRING
C       Menu obj
C       CHARACTER v1

      EXTERNAL set_MENU_STRINGS_1
C       Menu obj
C       CHARACTER v1

      EXTERNAL set_MENU_STRINGS_2
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_MENU_STRINGS_3
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_MENU_STRINGS_4
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_MENU_STRINGS_5
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_MENU_STRINGS_6
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_MENU_STRINGS_7
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_MENU_STRINGS_8
C       Menu obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      EXTERNAL set_MENU_STRING_ITEM
C       Menu obj
C       CHARACTER v1
C       Xv_opaque v2

      LOGICAL  get_MENU_TITLE
      EXTERNAL get_MENU_TITLE
C       Menu obj

      EXTERNAL set_MENU_TITLE
C       Menu obj
C       LOGICAL v1

      EXTERNAL set_MENU_TITLE_IMAGE
C       Menu obj
C       RECORD /Pixrect/ v1

      EXTERNAL set_MENU_TITLE_ITEM
C       Menu obj
C       CHARACTER v1

      Menu_attribute  get_MENU_TYPE
      EXTERNAL get_MENU_TYPE
C       Menu obj

      LOGICAL  get_MENU_VALID_RESULT
      EXTERNAL get_MENU_VALID_RESULT
C       Menu obj

      EXTERNAL set_MENU_VALID_RESULT
C       Menu obj
C       LOGICAL v1

      Xv_opaque  get_MENU_VALUE
      EXTERNAL get_MENU_VALUE
C       Menu obj

      EXTERNAL set_MENU_VALUE
C       Menu obj
C       Xv_opaque v1

      LOGICAL  get_OPENWIN_ADJUST_FOR_HORIZONTA
      EXTERNAL get_OPENWIN_ADJUST_FOR_HORIZONTA
C       Openwin obj

      EXTERNAL set_OPENWIN_ADJUST_FOR_HORIZONTA
C       Openwin obj
C       LOGICAL v1

      LOGICAL  get_OPENWIN_ADJUST_FOR_VERTICAL_
      EXTERNAL get_OPENWIN_ADJUST_FOR_VERTICAL_
C       Openwin obj

      EXTERNAL set_OPENWIN_ADJUST_FOR_VERTICAL_
C       Openwin obj
C       LOGICAL v1

      LOGICAL  get_OPENWIN_AUTO_CLEAR
      EXTERNAL get_OPENWIN_AUTO_CLEAR
C       Openwin obj

      EXTERNAL set_OPENWIN_AUTO_CLEAR
C       Openwin obj
C       LOGICAL v1

      Xv_opaque  get_OPENWIN_HORIZONTAL_SCROLLBAR
      EXTERNAL get_OPENWIN_HORIZONTAL_SCROLLBAR
C       Openwin obj
C       Xv_opaque v1

      EXTERNAL set_OPENWIN_HORIZONTAL_SCROLLBAR
C       Openwin obj
C       Xv_opaque v1
C       Xv_opaque v2

      LOGICAL  get_OPENWIN_NO_MARGIN
      EXTERNAL get_OPENWIN_NO_MARGIN
C       Openwin obj

      EXTERNAL set_OPENWIN_NO_MARGIN
C       Openwin obj
C       LOGICAL v1

      Xv_opaque  get_OPENWIN_NTH_VIEW
      EXTERNAL get_OPENWIN_NTH_VIEW
C       Openwin obj
C       INTEGER v1

      INTEGER  get_OPENWIN_NVIEWS
      EXTERNAL get_OPENWIN_NVIEWS
C       Openwin obj

      Xv_opaque  get_OPENWIN_SELECTED_VIEW
      EXTERNAL get_OPENWIN_SELECTED_VIEW
C       Openwin obj

      EXTERNAL set_OPENWIN_SELECTED_VIEW
C       Openwin obj
C       Xv_opaque v1

      LOGICAL  get_OPENWIN_SHOW_BORDERS
      EXTERNAL get_OPENWIN_SHOW_BORDERS
C       Openwin obj

      EXTERNAL set_OPENWIN_SPLIT_1
C       Openwin obj
C       Attr_attribute v1

      EXTERNAL set_OPENWIN_SPLIT_2
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2

      EXTERNAL set_OPENWIN_SPLIT_3
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3

      EXTERNAL set_OPENWIN_SPLIT_4
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4

      EXTERNAL set_OPENWIN_SPLIT_5
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5

      EXTERNAL set_OPENWIN_SPLIT_6
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6

      EXTERNAL set_OPENWIN_SPLIT_7
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7

      EXTERNAL set_OPENWIN_SPLIT_8
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7
C       Attr_attribute v8

      Openwin_split_destroy_procp  get_OPENWIN_SPLIT_DESTROY_PROC
      EXTERNAL get_OPENWIN_SPLIT_DESTROY_PROC
C       Openwin obj

      EXTERNAL set_OPENWIN_SPLIT_DESTROY_PROC
C       Openwin obj
C       Openwin_split_destroy_procp v1

      EXTERNAL set_OPENWIN_SPLIT_DIRECTION
C       Openwin obj
C       Openwin_split_direction v1

      Openwin_split_init_procp  get_OPENWIN_SPLIT_INIT_PROC
      EXTERNAL get_OPENWIN_SPLIT_INIT_PROC
C       Openwin obj

      EXTERNAL set_OPENWIN_SPLIT_INIT_PROC
C       Openwin obj
C       Openwin_split_init_procp v1

      EXTERNAL set_OPENWIN_SPLIT_POSITION
C       Openwin obj
C       INTEGER v1

      EXTERNAL set_OPENWIN_SPLIT_VIEW
C       Openwin obj
C       Xv_opaque v1

      INTEGER  get_OPENWIN_SPLIT_VIEW_START
      EXTERNAL get_OPENWIN_SPLIT_VIEW_START
C       Openwin obj

      EXTERNAL set_OPENWIN_SPLIT_VIEW_START
C       Openwin obj
C       INTEGER v1

      Xv_opaque  get_OPENWIN_VERTICAL_SCROLLBAR
      EXTERNAL get_OPENWIN_VERTICAL_SCROLLBAR
C       Openwin obj
C       Xv_opaque v1

      EXTERNAL set_OPENWIN_VERTICAL_SCROLLBAR
C       Openwin obj
C       Xv_opaque v1
C       Xv_opaque v2

      EXTERNAL set_OPENWIN_VIEW_ATTRS_1
C       Openwin obj
C       Attr_attribute v1

      EXTERNAL set_OPENWIN_VIEW_ATTRS_2
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2

      EXTERNAL set_OPENWIN_VIEW_ATTRS_3
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3

      EXTERNAL set_OPENWIN_VIEW_ATTRS_4
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4

      EXTERNAL set_OPENWIN_VIEW_ATTRS_5
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5

      EXTERNAL set_OPENWIN_VIEW_ATTRS_6
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6

      EXTERNAL set_OPENWIN_VIEW_ATTRS_7
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7

      EXTERNAL set_OPENWIN_VIEW_ATTRS_8
C       Openwin obj
C       Attr_attribute v1
C       Attr_attribute v2
C       Attr_attribute v3
C       Attr_attribute v4
C       Attr_attribute v5
C       Attr_attribute v6
C       Attr_attribute v7
C       Attr_attribute v8

      Xv_opaque  get_OPENWIN_VIEW_CLASS
      EXTERNAL get_OPENWIN_VIEW_CLASS
C       Openwin obj

      LOGICAL  get_PANEL_ACCEPT_KEYSTROKE
      EXTERNAL get_PANEL_ACCEPT_KEYSTROKE
C       Panel obj

      EXTERNAL set_PANEL_ACCEPT_KEYSTROKE
C       Panel obj
C       LOGICAL v1

      Panel_background_procp  get_PANEL_BACKGROUND_PROC
      EXTERNAL get_PANEL_BACKGROUND_PROC
C       Panel obj

      EXTERNAL set_PANEL_BACKGROUND_PROC
C       Panel obj
C       Panel_background_procp v1

      LOGICAL  get_PANEL_BLINK_CARET
      EXTERNAL get_PANEL_BLINK_CARET
C       Panel obj

      EXTERNAL set_PANEL_BLINK_CARET
C       Panel obj
C       LOGICAL v1

      Xv_Font  get_PANEL_BOLD_FONT
      EXTERNAL get_PANEL_BOLD_FONT
C       Panel obj

      EXTERNAL set_PANEL_BOLD_FONT
C       Panel obj
C       Xv_Font v1

      LOGICAL  get_PANEL_BUSY
      EXTERNAL get_PANEL_BUSY
C       Panel obj

      EXTERNAL set_PANEL_BUSY
C       Panel obj
C       LOGICAL v1

      Panel_item  get_PANEL_CARET_ITEM
      EXTERNAL get_PANEL_CARET_ITEM
C       Panel obj

      EXTERNAL set_PANEL_CARET_ITEM
C       Panel obj
C       Panel_item v1

      Panel_item  get_PANEL_CHILD_CARET_ITEM
      EXTERNAL get_PANEL_CHILD_CARET_ITEM
C       Panel obj

      EXTERNAL set_PANEL_CHILD_CARET_ITEM
C       Panel obj
C       Panel_item v1

      LOGICAL  get_PANEL_CHOICES_BOLD
      EXTERNAL get_PANEL_CHOICES_BOLD
C       Panel obj

      EXTERNAL set_PANEL_CHOICES_BOLD
C       Panel obj
C       LOGICAL v1

      EXTERNAL get_PANEL_CHOICE_FONT
C       RECORD /Pixfont/  returnval__
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_FONT
C       Panel obj
C       INTEGER v1
C       RECORD /Pixfont/ v2

      EXTERNAL set_PANEL_CHOICE_FONTS_1
C       Panel obj
C       RECORD /Pixfont/ v1

      EXTERNAL set_PANEL_CHOICE_FONTS_2
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2

      EXTERNAL set_PANEL_CHOICE_FONTS_3
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3

      EXTERNAL set_PANEL_CHOICE_FONTS_4
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3
C       RECORD /Pixfont/ v4

      EXTERNAL set_PANEL_CHOICE_FONTS_5
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3
C       RECORD /Pixfont/ v4
C       RECORD /Pixfont/ v5

      EXTERNAL set_PANEL_CHOICE_FONTS_6
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3
C       RECORD /Pixfont/ v4
C       RECORD /Pixfont/ v5
C       RECORD /Pixfont/ v6

      EXTERNAL set_PANEL_CHOICE_FONTS_7
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3
C       RECORD /Pixfont/ v4
C       RECORD /Pixfont/ v5
C       RECORD /Pixfont/ v6
C       RECORD /Pixfont/ v7

      EXTERNAL set_PANEL_CHOICE_FONTS_8
C       Panel obj
C       RECORD /Pixfont/ v1
C       RECORD /Pixfont/ v2
C       RECORD /Pixfont/ v3
C       RECORD /Pixfont/ v4
C       RECORD /Pixfont/ v5
C       RECORD /Pixfont/ v6
C       RECORD /Pixfont/ v7
C       RECORD /Pixfont/ v8

      Server_image  get_PANEL_CHOICE_IMAGE
      EXTERNAL get_PANEL_CHOICE_IMAGE
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_IMAGE
C       Panel obj
C       INTEGER v1
C       Server_image v2

      EXTERNAL set_PANEL_CHOICE_IMAGES_1
C       Panel obj
C       Server_image v1

      EXTERNAL set_PANEL_CHOICE_IMAGES_2
C       Panel obj
C       Server_image v1
C       Server_image v2

      EXTERNAL set_PANEL_CHOICE_IMAGES_3
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3

      EXTERNAL set_PANEL_CHOICE_IMAGES_4
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4

      EXTERNAL set_PANEL_CHOICE_IMAGES_5
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5

      EXTERNAL set_PANEL_CHOICE_IMAGES_6
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6

      EXTERNAL set_PANEL_CHOICE_IMAGES_7
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7

      EXTERNAL set_PANEL_CHOICE_IMAGES_8
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7
C       Server_image v8

      INTEGER  get_PANEL_CHOICE_NCOLS
      EXTERNAL get_PANEL_CHOICE_NCOLS
C       Panel obj

      EXTERNAL set_PANEL_CHOICE_NCOLS
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_CHOICE_NROWS
      EXTERNAL get_PANEL_CHOICE_NROWS
C       Panel obj

      EXTERNAL set_PANEL_CHOICE_NROWS
C       Panel obj
C       INTEGER v1

      EXTERNAL get_PANEL_CHOICE_RECT
C       RECORD /Rect/  returnval__
C       Panel obj
C       INTEGER v1

      Cstringp  get_PANEL_CHOICE_STRING
      EXTERNAL get_PANEL_CHOICE_STRING
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_STRING
C       Panel obj
C       INTEGER v1
C       CHARACTER v2

      EXTERNAL set_PANEL_CHOICE_STRINGS_1
C       Panel obj
C       CHARACTER v1

      EXTERNAL set_PANEL_CHOICE_STRINGS_2
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_PANEL_CHOICE_STRINGS_3
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_PANEL_CHOICE_STRINGS_4
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_PANEL_CHOICE_STRINGS_5
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_PANEL_CHOICE_STRINGS_6
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_PANEL_CHOICE_STRINGS_7
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_PANEL_CHOICE_STRINGS_8
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      INTEGER  get_PANEL_CHOICE_X
      EXTERNAL get_PANEL_CHOICE_X
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_X
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_CHOICE_XS_1
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_XS_2
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_CHOICE_XS_3
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_PANEL_CHOICE_XS_4
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_PANEL_CHOICE_XS_5
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_PANEL_CHOICE_XS_6
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_PANEL_CHOICE_XS_7
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_PANEL_CHOICE_XS_8
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_PANEL_CHOICE_Y
      EXTERNAL get_PANEL_CHOICE_Y
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_Y
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_CHOICE_YS_1
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_CHOICE_YS_2
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_CHOICE_YS_3
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_PANEL_CHOICE_YS_4
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_PANEL_CHOICE_YS_5
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_PANEL_CHOICE_YS_6
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_PANEL_CHOICE_YS_7
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_PANEL_CHOICE_YS_8
C       Panel obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      LOGICAL  get_PANEL_CHOOSE_NONE
      EXTERNAL get_PANEL_CHOOSE_NONE
C       Panel obj

      EXTERNAL set_PANEL_CHOOSE_NONE
C       Panel obj
C       LOGICAL v1

      LOGICAL  get_PANEL_CHOOSE_ONE
      EXTERNAL get_PANEL_CHOOSE_ONE
C       Panel obj

      EXTERNAL set_PANEL_CHOOSE_ONE
C       Panel obj
C       LOGICAL v1

      Xv_opaque  get_PANEL_CLIENT_DATA
      EXTERNAL get_PANEL_CLIENT_DATA
C       Panel obj

      EXTERNAL set_PANEL_CLIENT_DATA
C       Panel obj
C       Xv_opaque v1

      Xv_opaque  get_PANEL_CURRENT_ITEM
      EXTERNAL get_PANEL_CURRENT_ITEM
C       Panel obj

      Panel_item  get_PANEL_DEFAULT_ITEM
      EXTERNAL get_PANEL_DEFAULT_ITEM
C       Panel obj

      EXTERNAL set_PANEL_DEFAULT_ITEM
C       Panel obj
C       Panel_item v1

      INTEGER  get_PANEL_DEFAULT_VALUE
      EXTERNAL get_PANEL_DEFAULT_VALUE
C       Panel obj

      EXTERNAL set_PANEL_DEFAULT_VALUE
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_DIRECTION
      EXTERNAL get_PANEL_DIRECTION
C       Panel obj

      EXTERNAL set_PANEL_DIRECTION
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_DISPLAY_LEVEL
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_DROP_BUSY_GLYPH
      EXTERNAL get_PANEL_DROP_BUSY_GLYPH
C       Panel obj

      EXTERNAL set_PANEL_DROP_BUSY_GLYPH
C       Panel obj
C       Xv_opaque v1

      Xv_opaque  get_PANEL_DROP_DND
      EXTERNAL get_PANEL_DROP_DND
C       Panel obj

      EXTERNAL set_PANEL_DROP_DND
C       Panel obj
C       Xv_opaque v1

      LOGICAL  get_PANEL_DROP_FULL
      EXTERNAL get_PANEL_DROP_FULL
C       Panel obj

      EXTERNAL set_PANEL_DROP_FULL
C       Panel obj
C       LOGICAL v1

      Xv_opaque  get_PANEL_DROP_GLYPH
      EXTERNAL get_PANEL_DROP_GLYPH
C       Panel obj

      EXTERNAL set_PANEL_DROP_GLYPH
C       Panel obj
C       Xv_opaque v1

      INTEGER  get_PANEL_DROP_HEIGHT
      EXTERNAL get_PANEL_DROP_HEIGHT
C       Panel obj

      EXTERNAL set_PANEL_DROP_HEIGHT
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_DROP_SEL_REQ
      EXTERNAL get_PANEL_DROP_SEL_REQ
C       Panel obj

      LOGICAL  get_PANEL_DROP_SITE_DEFAULT
      EXTERNAL get_PANEL_DROP_SITE_DEFAULT
C       Panel obj

      EXTERNAL set_PANEL_DROP_SITE_DEFAULT
C       Panel obj
C       LOGICAL v1

      INTEGER  get_PANEL_DROP_WIDTH
      EXTERNAL get_PANEL_DROP_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_DROP_WIDTH
C       Panel obj
C       INTEGER v1

      Panel_event_procp  get_PANEL_EVENT_PROC
      EXTERNAL get_PANEL_EVENT_PROC
C       Panel obj

      EXTERNAL set_PANEL_EVENT_PROC
C       Panel obj
C       Panel_event_procp v1

      INTEGER  get_PANEL_EXTRA_PAINT_HEIGHT
      EXTERNAL get_PANEL_EXTRA_PAINT_HEIGHT
C       Panel obj

      EXTERNAL set_PANEL_EXTRA_PAINT_HEIGHT
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_EXTRA_PAINT_WIDTH
      EXTERNAL get_PANEL_EXTRA_PAINT_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_EXTRA_PAINT_WIDTH
C       Panel obj
C       INTEGER v1

      Panel_setting  get_PANEL_FEEDBACK
      EXTERNAL get_PANEL_FEEDBACK
C       Panel obj

      EXTERNAL set_PANEL_FEEDBACK
C       Panel obj
C       Panel_setting v1

      Panel_item  get_PANEL_FIRST_ITEM
      EXTERNAL get_PANEL_FIRST_ITEM
C       Panel obj

      Xv_opaque  get_PANEL_FIRST_PAINT_WINDOW
      EXTERNAL get_PANEL_FIRST_PAINT_WINDOW
C       Panel obj

      Xv_opaque  get_PANEL_FOCUS_PW
      EXTERNAL get_PANEL_FOCUS_PW
C       Panel obj

      INTEGER  get_PANEL_GAUGE_WIDTH
      EXTERNAL get_PANEL_GAUGE_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_GAUGE_WIDTH
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_GINFO
      EXTERNAL get_PANEL_GINFO
C       Panel obj

      LOGICAL  get_PANEL_INACTIVE
      EXTERNAL get_PANEL_INACTIVE
C       Panel obj

      EXTERNAL set_PANEL_INACTIVE
C       Panel obj
C       LOGICAL v1

      Panel_item_type  get_PANEL_ITEM_CLASS
      EXTERNAL get_PANEL_ITEM_CLASS
C       Panel obj

      EXTERNAL set_PANEL_ITEM_CLASS
C       Panel obj
C       Panel_item_type v1

      INTEGER  get_PANEL_ITEM_COLOR
      EXTERNAL get_PANEL_ITEM_COLOR
C       Panel obj

      EXTERNAL set_PANEL_ITEM_COLOR
C       Panel obj
C       INTEGER v1

      LOGICAL  get_PANEL_ITEM_CREATED
      EXTERNAL get_PANEL_ITEM_CREATED
C       Panel obj

      LOGICAL  get_PANEL_ITEM_DEAF
      EXTERNAL get_PANEL_ITEM_DEAF
C       Panel obj

      EXTERNAL set_PANEL_ITEM_DEAF
C       Panel obj
C       LOGICAL v1

      EXTERNAL get_PANEL_ITEM_LABEL_RECT
C       RECORD /Rect/  returnval__
C       Panel obj

      EXTERNAL set_PANEL_ITEM_LABEL_RECT
C       Panel obj
C       RECORD /Rect/ v1

      Menu  get_PANEL_ITEM_MENU
      EXTERNAL get_PANEL_ITEM_MENU
C       Panel obj

      EXTERNAL set_PANEL_ITEM_MENU
C       Panel obj
C       Menu v1

      Xv_opaque  get_PANEL_ITEM_NTH_WINDOW
      EXTERNAL get_PANEL_ITEM_NTH_WINDOW
C       Panel obj

      INTEGER  get_PANEL_ITEM_NWINDOWS
      EXTERNAL get_PANEL_ITEM_NWINDOWS
C       Panel obj

      EXTERNAL get_PANEL_ITEM_RECT
C       RECORD /Rect/  returnval__
C       Panel obj

      EXTERNAL get_PANEL_ITEM_VALUE_RECT
C       RECORD /Rect/  returnval__
C       Panel obj

      EXTERNAL set_PANEL_ITEM_VALUE_RECT
C       Panel obj
C       RECORD /Rect/ v1

      LOGICAL  get_PANEL_ITEM_WANTS_ADJUST
      EXTERNAL get_PANEL_ITEM_WANTS_ADJUST
C       Panel obj

      EXTERNAL set_PANEL_ITEM_WANTS_ADJUST
C       Panel obj
C       LOGICAL v1

      LOGICAL  get_PANEL_ITEM_WANTS_ISO
      EXTERNAL get_PANEL_ITEM_WANTS_ISO
C       Panel obj

      EXTERNAL set_PANEL_ITEM_WANTS_ISO
C       Panel obj
C       LOGICAL v1

      INTEGER  get_PANEL_ITEM_X
      EXTERNAL get_PANEL_ITEM_X
C       Panel obj

      EXTERNAL set_PANEL_ITEM_X
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_ITEM_X_GAP
      EXTERNAL get_PANEL_ITEM_X_GAP
C       Panel obj

      EXTERNAL set_PANEL_ITEM_X_GAP
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_ITEM_X_POSITION
      EXTERNAL get_PANEL_ITEM_X_POSITION
C       Panel obj

      EXTERNAL set_PANEL_ITEM_X_POSITION
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_ITEM_Y
      EXTERNAL get_PANEL_ITEM_Y
C       Panel obj

      EXTERNAL set_PANEL_ITEM_Y
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_ITEM_Y_GAP
      EXTERNAL get_PANEL_ITEM_Y_GAP
C       Panel obj

      EXTERNAL set_PANEL_ITEM_Y_GAP
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_ITEM_Y_POSITION
      EXTERNAL get_PANEL_ITEM_Y_POSITION
C       Panel obj

      EXTERNAL set_PANEL_ITEM_Y_POSITION
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_JUMP_DELTA
      EXTERNAL get_PANEL_JUMP_DELTA
C       Panel obj

      EXTERNAL set_PANEL_JUMP_DELTA
C       Panel obj
C       INTEGER v1

      LOGICAL  get_PANEL_LABEL_BOLD
      EXTERNAL get_PANEL_LABEL_BOLD
C       Panel obj

      EXTERNAL set_PANEL_LABEL_BOLD
C       Panel obj
C       LOGICAL v1

      EXTERNAL get_PANEL_LABEL_FONT
C       RECORD /Pixfont/  returnval__
C       Panel obj

      EXTERNAL set_PANEL_LABEL_FONT
C       Panel obj
C       RECORD /Pixfont/ v1

      Server_image  get_PANEL_LABEL_IMAGE
      EXTERNAL get_PANEL_LABEL_IMAGE
C       Panel obj

      EXTERNAL set_PANEL_LABEL_IMAGE
C       Panel obj
C       Server_image v1

      Cstringp  get_PANEL_LABEL_STRING
      EXTERNAL get_PANEL_LABEL_STRING
C       Panel obj

      EXTERNAL set_PANEL_LABEL_STRING
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_LABEL_WIDTH
      EXTERNAL get_PANEL_LABEL_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_LABEL_WIDTH
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LABEL_X
      EXTERNAL get_PANEL_LABEL_X
C       Panel obj

      EXTERNAL set_PANEL_LABEL_X
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LABEL_Y
      EXTERNAL get_PANEL_LABEL_Y
C       Panel obj

      EXTERNAL set_PANEL_LABEL_Y
C       Panel obj
C       INTEGER v1

      Panel_setting  get_PANEL_LAYOUT
      EXTERNAL get_PANEL_LAYOUT
C       Panel obj

      EXTERNAL set_PANEL_LAYOUT
C       Panel obj
C       Panel_setting v1

      INTEGER  get_PANEL_LIST_CLIENT_DATA
      EXTERNAL get_PANEL_LIST_CLIENT_DATA
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_CLIENT_DATA
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_LIST_DELETE
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_DELETE_ROWS
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_PANEL_LIST_DELETE_SELECTED_R
C       Panel obj

      INTEGER  get_PANEL_LIST_DISPLAY_ROWS
      EXTERNAL get_PANEL_LIST_DISPLAY_ROWS
C       Panel obj

      EXTERNAL set_PANEL_LIST_DISPLAY_ROWS
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LIST_FIRST_SELECTED
      EXTERNAL get_PANEL_LIST_FIRST_SELECTED
C       Panel obj

      Xv_Font  get_PANEL_LIST_FONT
      EXTERNAL get_PANEL_LIST_FONT
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_FONT
C       Panel obj
C       INTEGER v1
C       Xv_Font v2

      EXTERNAL set_PANEL_LIST_FONTS_1
C       Panel obj
C       Xv_Font v1

      EXTERNAL set_PANEL_LIST_FONTS_2
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2

      EXTERNAL set_PANEL_LIST_FONTS_3
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3

      EXTERNAL set_PANEL_LIST_FONTS_4
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3
C       Xv_Font v4

      EXTERNAL set_PANEL_LIST_FONTS_5
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3
C       Xv_Font v4
C       Xv_Font v5

      EXTERNAL set_PANEL_LIST_FONTS_6
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3
C       Xv_Font v4
C       Xv_Font v5
C       Xv_Font v6

      EXTERNAL set_PANEL_LIST_FONTS_7
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3
C       Xv_Font v4
C       Xv_Font v5
C       Xv_Font v6
C       Xv_Font v7

      EXTERNAL set_PANEL_LIST_FONTS_8
C       Panel obj
C       Xv_Font v1
C       Xv_Font v2
C       Xv_Font v3
C       Xv_Font v4
C       Xv_Font v5
C       Xv_Font v6
C       Xv_Font v7
C       Xv_Font v8

      Server_image  get_PANEL_LIST_GLYPH
      EXTERNAL get_PANEL_LIST_GLYPH
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_GLYPH
C       Panel obj
C       INTEGER v1
C       Server_image v2

      EXTERNAL set_PANEL_LIST_GLYPHS_1
C       Panel obj
C       Server_image v1

      EXTERNAL set_PANEL_LIST_GLYPHS_2
C       Panel obj
C       Server_image v1
C       Server_image v2

      EXTERNAL set_PANEL_LIST_GLYPHS_3
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3

      EXTERNAL set_PANEL_LIST_GLYPHS_4
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4

      EXTERNAL set_PANEL_LIST_GLYPHS_5
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5

      EXTERNAL set_PANEL_LIST_GLYPHS_6
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6

      EXTERNAL set_PANEL_LIST_GLYPHS_7
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7

      EXTERNAL set_PANEL_LIST_GLYPHS_8
C       Panel obj
C       Server_image v1
C       Server_image v2
C       Server_image v3
C       Server_image v4
C       Server_image v5
C       Server_image v6
C       Server_image v7
C       Server_image v8

      EXTERNAL set_PANEL_LIST_INSERT
C       Panel obj
C       INTEGER v1

      LOGICAL  get_PANEL_LIST_INSERT_DUPLICATE
      EXTERNAL get_PANEL_LIST_INSERT_DUPLICATE
C       Panel obj

      EXTERNAL set_PANEL_LIST_INSERT_GLYPHS
C       Panel obj
C       INTEGER v1
C       Xv_opaque v2

      EXTERNAL set_PANEL_LIST_INSERT_STRINGS
C       Panel obj
C       INTEGER v1
C       CHARACTER v2

      INTEGER  get_PANEL_LIST_MODE
      EXTERNAL get_PANEL_LIST_MODE
C       Panel obj

      EXTERNAL set_PANEL_LIST_MODE
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LIST_NEXT_SELECTED
      EXTERNAL get_PANEL_LIST_NEXT_SELECTED
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LIST_NROWS
      EXTERNAL get_PANEL_LIST_NROWS
C       Panel obj

      EXTERNAL set_PANEL_LIST_NROWS
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_LIST_ROW_HEIGHT
      EXTERNAL get_PANEL_LIST_ROW_HEIGHT
C       Panel obj

      EXTERNAL set_PANEL_LIST_ROW_HEIGHT
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_LIST_SCROLLBAR
      EXTERNAL get_PANEL_LIST_SCROLLBAR
C       Panel obj

      EXTERNAL set_PANEL_LIST_SELECT
C       Panel obj
C       INTEGER v1
C       LOGICAL v2

      LOGICAL  get_PANEL_LIST_SELECTED
      EXTERNAL get_PANEL_LIST_SELECTED
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_SORT
C       Panel obj
C       Panel_setting v1

      Cstringp  get_PANEL_LIST_STRING
      EXTERNAL get_PANEL_LIST_STRING
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_LIST_STRING
C       Panel obj
C       INTEGER v1
C       CHARACTER v2

      EXTERNAL set_PANEL_LIST_STRINGS_1
C       Panel obj
C       CHARACTER v1

      EXTERNAL set_PANEL_LIST_STRINGS_2
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_PANEL_LIST_STRINGS_3
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_PANEL_LIST_STRINGS_4
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_PANEL_LIST_STRINGS_5
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_PANEL_LIST_STRINGS_6
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_PANEL_LIST_STRINGS_7
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_PANEL_LIST_STRINGS_8
C       Panel obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      Cstringp  get_PANEL_LIST_TITLE
      EXTERNAL get_PANEL_LIST_TITLE
C       Panel obj

      EXTERNAL set_PANEL_LIST_TITLE
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_LIST_WIDTH
      EXTERNAL get_PANEL_LIST_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_LIST_WIDTH
C       Panel obj
C       INTEGER v1

      EXTERNAL get_PANEL_MARK_IMAGE
C       RECORD /Pixrect/  returnval__
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_MARK_IMAGE
C       Panel obj
C       INTEGER v1
C       RECORD /Pixrect/ v2

      EXTERNAL set_PANEL_MARK_IMAGES_1
C       Panel obj
C       RECORD /Pixrect/ v1

      EXTERNAL set_PANEL_MARK_IMAGES_2
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2

      EXTERNAL set_PANEL_MARK_IMAGES_3
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3

      EXTERNAL set_PANEL_MARK_IMAGES_4
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4

      EXTERNAL set_PANEL_MARK_IMAGES_5
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5

      EXTERNAL set_PANEL_MARK_IMAGES_6
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6

      EXTERNAL set_PANEL_MARK_IMAGES_7
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6
C       RECORD /Pixrect/ v7

      EXTERNAL set_PANEL_MARK_IMAGES_8
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6
C       RECORD /Pixrect/ v7
C       RECORD /Pixrect/ v8

      INTEGER  get_PANEL_MARK_X
      EXTERNAL get_PANEL_MARK_X
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_MARK_Y
      EXTERNAL get_PANEL_MARK_Y
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_MASK_CHAR
C       Panel obj
C       CHARACTER v1

      Cstringp  get_PANEL_MAX_TICK_STRING
      EXTERNAL get_PANEL_MAX_TICK_STRING
C       Panel obj

      EXTERNAL set_PANEL_MAX_TICK_STRING
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_MAX_VALUE
      EXTERNAL get_PANEL_MAX_VALUE
C       Panel obj

      EXTERNAL set_PANEL_MAX_VALUE
C       Panel obj
C       INTEGER v1

      Cstringp  get_PANEL_MAX_VALUE_STRING
      EXTERNAL get_PANEL_MAX_VALUE_STRING
C       Panel obj

      EXTERNAL set_PANEL_MAX_VALUE_STRING
C       Panel obj
C       CHARACTER v1

      Cstringp  get_PANEL_MIN_TICK_STRING
      EXTERNAL get_PANEL_MIN_TICK_STRING
C       Panel obj

      EXTERNAL set_PANEL_MIN_TICK_STRING
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_MIN_VALUE
      EXTERNAL get_PANEL_MIN_VALUE
C       Panel obj

      EXTERNAL set_PANEL_MIN_VALUE
C       Panel obj
C       INTEGER v1

      Cstringp  get_PANEL_MIN_VALUE_STRING
      EXTERNAL get_PANEL_MIN_VALUE_STRING
C       Panel obj

      EXTERNAL set_PANEL_MIN_VALUE_STRING
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_NCHOICES
      EXTERNAL get_PANEL_NCHOICES
C       Panel obj

      EXTERNAL set_PANEL_NCHOICES
C       Panel obj
C       INTEGER v1

      Panel_item  get_PANEL_NEXT_COL
      EXTERNAL get_PANEL_NEXT_COL
C       Panel obj

      EXTERNAL set_PANEL_NEXT_COL
C       Panel obj
C       Panel_item v1

      Panel_item  get_PANEL_NEXT_ITEM
      EXTERNAL get_PANEL_NEXT_ITEM
C       Panel obj

      Panel_item  get_PANEL_NEXT_ROW
      EXTERNAL get_PANEL_NEXT_ROW
C       Panel obj

      EXTERNAL set_PANEL_NEXT_ROW
C       Panel obj
C       Panel_item v1

      EXTERNAL get_PANEL_NOMARK_IMAGE
C       RECORD /Pixrect/  returnval__
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_NOMARK_IMAGE
C       Panel obj
C       INTEGER v1
C       RECORD /Pixrect/ v2

      EXTERNAL set_PANEL_NOMARK_IMAGES_1
C       Panel obj
C       RECORD /Pixrect/ v1

      EXTERNAL set_PANEL_NOMARK_IMAGES_2
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2

      EXTERNAL set_PANEL_NOMARK_IMAGES_3
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3

      EXTERNAL set_PANEL_NOMARK_IMAGES_4
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4

      EXTERNAL set_PANEL_NOMARK_IMAGES_5
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5

      EXTERNAL set_PANEL_NOMARK_IMAGES_6
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6

      EXTERNAL set_PANEL_NOMARK_IMAGES_7
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6
C       RECORD /Pixrect/ v7

      EXTERNAL set_PANEL_NOMARK_IMAGES_8
C       Panel obj
C       RECORD /Pixrect/ v1
C       RECORD /Pixrect/ v2
C       RECORD /Pixrect/ v3
C       RECORD /Pixrect/ v4
C       RECORD /Pixrect/ v5
C       RECORD /Pixrect/ v6
C       RECORD /Pixrect/ v7
C       RECORD /Pixrect/ v8

      Panel_setting  get_PANEL_NOTIFY_LEVEL
      EXTERNAL get_PANEL_NOTIFY_LEVEL
C       Panel obj

      EXTERNAL set_PANEL_NOTIFY_LEVEL
C       Panel obj
C       Panel_setting v1

      Panel_notify_procp  get_PANEL_NOTIFY_PROC
      EXTERNAL get_PANEL_NOTIFY_PROC
C       Panel obj

      EXTERNAL set_PANEL_NOTIFY_PROC
C       Panel obj
C       Panel_notify_procp v1

      INTEGER  get_PANEL_NOTIFY_STATUS
      EXTERNAL get_PANEL_NOTIFY_STATUS
C       Panel obj

      EXTERNAL set_PANEL_NOTIFY_STATUS
C       Panel obj
C       INTEGER v1

      Cstringp  get_PANEL_NOTIFY_STRING
      EXTERNAL get_PANEL_NOTIFY_STRING
C       Panel obj

      EXTERNAL set_PANEL_NOTIFY_STRING
C       Panel obj
C       CHARACTER v1

      INTEGER  get_PANEL_NO_REDISPLAY_ITEM
      EXTERNAL get_PANEL_NO_REDISPLAY_ITEM
C       Panel obj

      EXTERNAL set_PANEL_NO_REDISPLAY_ITEM
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_OPS_VECTOR
      EXTERNAL get_PANEL_OPS_VECTOR
C       Panel obj

      EXTERNAL set_PANEL_OPS_VECTOR
C       Panel obj
C       Xv_opaque v1

      EXTERNAL set_PANEL_PAINT
C       Panel obj
C       Panel_setting v1

      Panel  get_PANEL_PARENT_PANEL
      EXTERNAL get_PANEL_PARENT_PANEL
C       Panel obj

      Panel_item  get_PANEL_PRIMARY_FOCUS_ITEM
      EXTERNAL get_PANEL_PRIMARY_FOCUS_ITEM
C       Panel obj

      EXTERNAL set_PANEL_PRIMARY_FOCUS_ITEM
C       Panel obj
C       Panel_item v1

      LOGICAL  get_PANEL_READ_ONLY
      EXTERNAL get_PANEL_READ_ONLY
C       Panel obj

      EXTERNAL set_PANEL_READ_ONLY
C       Panel obj
C       LOGICAL v1

      Panel_repaint_procp  get_PANEL_REPAINT_PROC
      EXTERNAL get_PANEL_REPAINT_PROC
C       Panel obj

      EXTERNAL set_PANEL_REPAINT_PROC
C       Panel obj
C       Panel_repaint_procp v1

      LOGICAL  get_PANEL_SHOW_ITEM
      EXTERNAL get_PANEL_SHOW_ITEM
C       Panel obj

      EXTERNAL set_PANEL_SHOW_ITEM
C       Panel obj
C       LOGICAL v1

      LOGICAL  get_PANEL_SHOW_RANGE
      EXTERNAL get_PANEL_SHOW_RANGE
C       Panel obj

      EXTERNAL set_PANEL_SHOW_RANGE
C       Panel obj
C       LOGICAL v1

      LOGICAL  get_PANEL_SHOW_VALUE
      EXTERNAL get_PANEL_SHOW_VALUE
C       Panel obj

      EXTERNAL set_PANEL_SHOW_VALUE
C       Panel obj
C       LOGICAL v1

      LOGICAL  get_PANEL_SLIDER_END_BOXES
      EXTERNAL get_PANEL_SLIDER_END_BOXES
C       Panel obj

      EXTERNAL set_PANEL_SLIDER_END_BOXES
C       Panel obj
C       LOGICAL v1

      INTEGER  get_PANEL_SLIDER_WIDTH
      EXTERNAL get_PANEL_SLIDER_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_SLIDER_WIDTH
C       Panel obj
C       INTEGER v1

      Xv_opaque  get_PANEL_STATUS
      EXTERNAL get_PANEL_STATUS
C       Panel obj

      EXTERNAL set_PANEL_TEXT_SELECT_LINE
C       Panel obj

      INTEGER  get_PANEL_TICKS
      EXTERNAL get_PANEL_TICKS
C       Panel obj

      EXTERNAL set_PANEL_TICKS
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_TOGGLE_VALUE
      EXTERNAL get_PANEL_TOGGLE_VALUE
C       Panel obj
C       INTEGER v1

      EXTERNAL set_PANEL_TOGGLE_VALUE
C       Panel obj
C       INTEGER v1
C       INTEGER v2

      INTEGER  get_PANEL_VALUE
      EXTERNAL get_PANEL_VALUE
C       Panel obj

      EXTERNAL set_PANEL_VALUE
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_VALUE_DISPLAY_LENGTH
      EXTERNAL get_PANEL_VALUE_DISPLAY_LENGTH
C       Panel obj

      EXTERNAL set_PANEL_VALUE_DISPLAY_LENGTH
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_VALUE_DISPLAY_WIDTH
      EXTERNAL get_PANEL_VALUE_DISPLAY_WIDTH
C       Panel obj

      EXTERNAL set_PANEL_VALUE_DISPLAY_WIDTH
C       Panel obj
C       INTEGER v1

      EXTERNAL get_PANEL_VALUE_FONT
C       RECORD /Pixfont/  returnval__
C       Panel obj

      EXTERNAL set_PANEL_VALUE_FONT
C       Panel obj
C       RECORD /Pixfont/ v1

      INTEGER  get_PANEL_VALUE_STORED_LENGTH
      EXTERNAL get_PANEL_VALUE_STORED_LENGTH
C       Panel obj

      EXTERNAL set_PANEL_VALUE_STORED_LENGTH
C       Panel obj
C       INTEGER v1

      LOGICAL  get_PANEL_VALUE_UNDERLINED
      EXTERNAL get_PANEL_VALUE_UNDERLINED
C       Panel obj

      EXTERNAL set_PANEL_VALUE_UNDERLINED
C       Panel obj
C       LOGICAL v1

      INTEGER  get_PANEL_VALUE_X
      EXTERNAL get_PANEL_VALUE_X
C       Panel obj

      EXTERNAL set_PANEL_VALUE_X
C       Panel obj
C       INTEGER v1

      INTEGER  get_PANEL_VALUE_Y
      EXTERNAL get_PANEL_VALUE_Y
C       Panel obj

      EXTERNAL set_PANEL_VALUE_Y
C       Panel obj
C       INTEGER v1

      INTEGER  get_SCREEN_NUMBER
      EXTERNAL get_SCREEN_NUMBER
C       Xv_Screen obj

      EXTERNAL set_SCREEN_NUMBER
C       Xv_Screen obj
C       INTEGER v1

      Xv_opaque  get_SCREEN_SERVER
      EXTERNAL get_SCREEN_SERVER
C       Xv_Screen obj

      EXTERNAL set_SCREEN_SERVER
C       Xv_Screen obj
C       Xv_opaque v1

      Scrollbar_compute_procp  get_SCROLLBAR_COMPUTE_SCROLL_PRO
      EXTERNAL get_SCROLLBAR_COMPUTE_SCROLL_PRO
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_COMPUTE_SCROLL_PRO
C       Scrollbar obj
C       Scrollbar_compute_procp v1

      Scrollbar_setting  get_SCROLLBAR_DIRECTION
      EXTERNAL get_SCROLLBAR_DIRECTION
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_DIRECTION
C       Scrollbar obj
C       Scrollbar_setting v1

      INTEGER  get_SCROLLBAR_LAST_VIEW_START
      EXTERNAL get_SCROLLBAR_LAST_VIEW_START
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_LAST_VIEW_START
C       Scrollbar obj
C       INTEGER v1

      Xv_opaque  get_SCROLLBAR_MENU
      EXTERNAL get_SCROLLBAR_MENU
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_MENU
C       Scrollbar obj
C       Xv_opaque v1

      Scrollbar_normalize_procp  get_SCROLLBAR_NORMALIZE_PROC
      EXTERNAL get_SCROLLBAR_NORMALIZE_PROC
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_NORMALIZE_PROC
C       Scrollbar obj
C       Scrollbar_normalize_procp v1

      Xv_opaque  get_SCROLLBAR_NOTIFY_CLIENT
      EXTERNAL get_SCROLLBAR_NOTIFY_CLIENT
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_NOTIFY_CLIENT
C       Scrollbar obj
C       Xv_opaque v1

      INTEGER  get_SCROLLBAR_OBJECT_LENGTH
      EXTERNAL get_SCROLLBAR_OBJECT_LENGTH
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_OBJECT_LENGTH
C       Scrollbar obj
C       INTEGER v1

      INTEGER  get_SCROLLBAR_PAGE_LENGTH
      EXTERNAL get_SCROLLBAR_PAGE_LENGTH
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_PAGE_LENGTH
C       Scrollbar obj
C       INTEGER v1

      INTEGER  get_SCROLLBAR_PIXELS_PER_UNIT
      EXTERNAL get_SCROLLBAR_PIXELS_PER_UNIT
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_PIXELS_PER_UNIT
C       Scrollbar obj
C       INTEGER v1

      LOGICAL  get_SCROLLBAR_SPLITTABLE
      EXTERNAL get_SCROLLBAR_SPLITTABLE
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_SPLITTABLE
C       Scrollbar obj
C       LOGICAL v1

      INTEGER  get_SCROLLBAR_VIEW_LENGTH
      EXTERNAL get_SCROLLBAR_VIEW_LENGTH
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_VIEW_LENGTH
C       Scrollbar obj
C       INTEGER v1

      INTEGER  get_SCROLLBAR_VIEW_START
      EXTERNAL get_SCROLLBAR_VIEW_START
C       Scrollbar obj

      EXTERNAL set_SCROLLBAR_VIEW_START
C       Scrollbar obj
C       INTEGER v1

      EXTERNAL set_SEL_APPEND_TYPES_1
C       Selection obj
C       Atom v1

      EXTERNAL set_SEL_APPEND_TYPES_2
C       Selection obj
C       Atom v1
C       Atom v2

      EXTERNAL set_SEL_APPEND_TYPES_3
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3

      EXTERNAL set_SEL_APPEND_TYPES_4
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4

      EXTERNAL set_SEL_APPEND_TYPES_5
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5

      EXTERNAL set_SEL_APPEND_TYPES_6
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6

      EXTERNAL set_SEL_APPEND_TYPES_7
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6
C       Atom v7

      EXTERNAL set_SEL_APPEND_TYPES_8
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6
C       Atom v7
C       Atom v8

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_1
C       Selection obj
C       CHARACTER v1

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_2
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_3
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_4
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_5
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_6
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_7
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_SEL_APPEND_TYPE_NAMES_8
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      ProcPointer  get_SEL_CONVERT_PROC
      EXTERNAL get_SEL_CONVERT_PROC
C       Selection obj

      EXTERNAL set_SEL_CONVERT_PROC
C       Selection obj
C       ProcPointer v1

      LOGICAL  get_SEL_COPY
      EXTERNAL get_SEL_COPY
C       Selection obj

      EXTERNAL set_SEL_COPY
C       Selection obj
C       LOGICAL v1

      Xv_opaque  get_SEL_DATA
      EXTERNAL get_SEL_DATA
C       Selection obj
C       INTEGER v1

      EXTERNAL set_SEL_DATA
C       Selection obj
C       INTEGER v1
C       Xv_opaque v2

      ProcPointer  get_SEL_DONE_PROC
      EXTERNAL get_SEL_DONE_PROC
C       Selection obj

      EXTERNAL set_SEL_DONE_PROC
C       Selection obj
C       ProcPointer v1

      Xv_opaque  get_SEL_FIRST_ITEM
      EXTERNAL get_SEL_FIRST_ITEM
C       Selection obj

      INTEGER  get_SEL_FORMAT
      EXTERNAL get_SEL_FORMAT
C       Selection obj

      EXTERNAL set_SEL_FORMAT
C       Selection obj
C       INTEGER v1

      INTEGER  get_SEL_LENGTH
      EXTERNAL get_SEL_LENGTH
C       Selection obj

      EXTERNAL set_SEL_LENGTH
C       Selection obj
C       INTEGER v1

      ProcPointer  get_SEL_LOSE_PROC
      EXTERNAL get_SEL_LOSE_PROC
C       Selection obj

      EXTERNAL set_SEL_LOSE_PROC
C       Selection obj
C       ProcPointer v1

      Xv_opaque  get_SEL_NEXT_ITEM
      EXTERNAL get_SEL_NEXT_ITEM
C       Selection obj

      LOGICAL  get_SEL_OWN
      EXTERNAL get_SEL_OWN
C       Selection obj

      EXTERNAL set_SEL_OWN
C       Selection obj
C       LOGICAL v1

      INTEGER  get_SEL_PROPERTY
      EXTERNAL get_SEL_PROPERTY
C       Selection obj

      EXTERNAL set_SEL_PROPERTY
C       Selection obj
C       INTEGER v1

      EXTERNAL set_SEL_PROP_DATA
C       Selection obj
C       Xv_opaque v1

      EXTERNAL set_SEL_PROP_FORMAT
C       Selection obj
C       INTEGER v1

      Xv_opaque  get_SEL_PROP_INFO
      EXTERNAL get_SEL_PROP_INFO
C       Selection obj

      EXTERNAL set_SEL_PROP_LENGTH
C       Selection obj
C       INTEGER v1

      EXTERNAL set_SEL_PROP_TYPE
C       Selection obj
C       Atom v1

      EXTERNAL set_SEL_PROP_TYPE_NAME
C       Selection obj
C       CHARACTER v1

      Atom  get_SEL_RANK
      EXTERNAL get_SEL_RANK
C       Selection obj

      EXTERNAL set_SEL_RANK
C       Selection obj
C       Atom v1

      Cstringp  get_SEL_RANK_NAME
      EXTERNAL get_SEL_RANK_NAME
C       Selection obj

      EXTERNAL set_SEL_RANK_NAME
C       Selection obj
C       CHARACTER v1

      ProcPointer  get_SEL_REPLY_PROC
      EXTERNAL get_SEL_REPLY_PROC
C       Selection obj

      EXTERNAL set_SEL_REPLY_PROC
C       Selection obj
C       ProcPointer v1

      Xv_opaque  get_SEL_TIME
      EXTERNAL get_SEL_TIME
C       Selection obj

      EXTERNAL set_SEL_TIME
C       Selection obj
C       Xv_opaque v1

      INTEGER  get_SEL_TIMEOUT_VALUE
      EXTERNAL get_SEL_TIMEOUT_VALUE
C       Selection obj

      EXTERNAL set_SEL_TIMEOUT_VALUE
C       Selection obj
C       INTEGER v1

      Atom  get_SEL_TYPE
      EXTERNAL get_SEL_TYPE
C       Selection obj

      EXTERNAL set_SEL_TYPE
C       Selection obj
C       Atom v1

      Data_arrayp  get_SEL_TYPES
      EXTERNAL get_SEL_TYPES
C       Selection obj

      EXTERNAL set_SEL_TYPES_1
C       Selection obj
C       Atom v1

      EXTERNAL set_SEL_TYPES_2
C       Selection obj
C       Atom v1
C       Atom v2

      EXTERNAL set_SEL_TYPES_3
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3

      EXTERNAL set_SEL_TYPES_4
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4

      EXTERNAL set_SEL_TYPES_5
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5

      EXTERNAL set_SEL_TYPES_6
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6

      EXTERNAL set_SEL_TYPES_7
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6
C       Atom v7

      EXTERNAL set_SEL_TYPES_8
C       Selection obj
C       Atom v1
C       Atom v2
C       Atom v3
C       Atom v4
C       Atom v5
C       Atom v6
C       Atom v7
C       Atom v8

      EXTERNAL set_SEL_TYPE_INDEX
C       Selection obj
C       INTEGER v1

      Cstringp  get_SEL_TYPE_NAME
      EXTERNAL get_SEL_TYPE_NAME
C       Selection obj

      EXTERNAL set_SEL_TYPE_NAME
C       Selection obj
C       CHARACTER v1

      Cstringp_arrayp  get_SEL_TYPE_NAMES
      EXTERNAL get_SEL_TYPE_NAMES
C       Selection obj

      EXTERNAL set_SEL_TYPE_NAMES_1
C       Selection obj
C       CHARACTER v1

      EXTERNAL set_SEL_TYPE_NAMES_2
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2

      EXTERNAL set_SEL_TYPE_NAMES_3
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3

      EXTERNAL set_SEL_TYPE_NAMES_4
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4

      EXTERNAL set_SEL_TYPE_NAMES_5
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5

      EXTERNAL set_SEL_TYPE_NAMES_6
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6

      EXTERNAL set_SEL_TYPE_NAMES_7
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7

      EXTERNAL set_SEL_TYPE_NAMES_8
C       Selection obj
C       CHARACTER v1
C       CHARACTER v2
C       CHARACTER v3
C       CHARACTER v4
C       CHARACTER v5
C       CHARACTER v6
C       CHARACTER v7
C       CHARACTER v8

      Xv_opaque  get_SERVER_ASCII_KEYSTATE
      EXTERNAL get_SERVER_ASCII_KEYSTATE
C       Xv_Server obj

      EXTERNAL set_SERVER_ASCII_KEYSTATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_ASCII_TO_KEYCODE_MAP
      EXTERNAL get_SERVER_ASCII_TO_KEYCODE_MAP
C       Xv_Server obj

      EXTERNAL set_SERVER_ASCII_TO_KEYCODE_MAP
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_ATOM
      EXTERNAL get_SERVER_ATOM
C       Xv_Server obj
C       CHARACTER v1

      Xv_opaque  get_SERVER_ATOM_DATA
      EXTERNAL get_SERVER_ATOM_DATA
C       Xv_Server obj
C       Xv_opaque v1

      EXTERNAL set_SERVER_ATOM_DATA
C       Xv_Server obj
C       Xv_opaque v1
C       Xv_opaque v2

      Xv_opaque  get_SERVER_ATOM_NAME
      EXTERNAL get_SERVER_ATOM_NAME
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_CHORDING_TIMEOUT
      EXTERNAL get_SERVER_CHORDING_TIMEOUT
C       Xv_Server obj

      EXTERNAL set_SERVER_CHORDING_TIMEOUT
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_CHORD_MENU
      EXTERNAL get_SERVER_CHORD_MENU
C       Xv_Server obj

      EXTERNAL set_SERVER_CHORD_MENU
C       Xv_Server obj
C       Xv_opaque v1

      ProcPointer  get_SERVER_EXTENSION_PROC
      EXTERNAL get_SERVER_EXTENSION_PROC
C       Xv_Server obj

      EXTERNAL set_SERVER_EXTENSION_PROC
C       Xv_Server obj
C       ProcPointer v1

      Xv_opaque  get_SERVER_EXTERNAL_XEVENT_MASK
      EXTERNAL get_SERVER_EXTERNAL_XEVENT_MASK
C       Xv_Server obj
C       Xv_opaque v1
C       Xv_opaque v2

      EXTERNAL set_SERVER_EXTERNAL_XEVENT_MASK
C       Xv_Server obj
C       Xv_opaque v1
C       Xv_opaque v2
C       Xv_opaque v3

      ProcPointer  get_SERVER_EXTERNAL_XEVENT_PROC
      EXTERNAL get_SERVER_EXTERNAL_XEVENT_PROC
C       Xv_Server obj
C       Xv_opaque v1

      EXTERNAL set_SERVER_EXTERNAL_XEVENT_PROC
C       Xv_Server obj
C       Xv_opaque v1
C       ProcPointer v2

      Xv_opaque  get_SERVER_DO_DRAG_COPY
      EXTERNAL get_SERVER_DO_DRAG_COPY
C       Xv_Server obj

      EXTERNAL set_SERVER_DO_DRAG_COPY
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_DO_DRAG_LOAD
      EXTERNAL get_SERVER_DO_DRAG_LOAD
C       Xv_Server obj

      EXTERNAL set_SERVER_DO_DRAG_LOAD
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_DO_DRAG_MOVE
      EXTERNAL get_SERVER_DO_DRAG_MOVE
C       Xv_Server obj

      EXTERNAL set_SERVER_DO_DRAG_MOVE
C       Xv_Server obj
C       Xv_opaque v1

      Cstringp  get_SERVER_FONT_WITH_NAME
      EXTERNAL get_SERVER_FONT_WITH_NAME
C       Xv_Server obj

      EXTERNAL set_SERVER_FONT_WITH_NAME
C       Xv_Server obj
C       CHARACTER v1

      EXTERNAL set_SERVER_IMAGE_X_BITS
C       Xv_Server obj
C       CHARACTER v1

      Pixmap  get_SERVER_IMAGE_PIXMAP
      EXTERNAL get_SERVER_IMAGE_PIXMAP
C       Xv_Server obj

      EXTERNAL set_SERVER_IMAGE_PIXMAP
C       Xv_Server obj
C       Pixmap v1

      LOGICAL  get_SERVER_IMAGE_SAVE_PIXMAP
      EXTERNAL get_SERVER_IMAGE_SAVE_PIXMAP
C       Xv_Server obj

      EXTERNAL set_SERVER_IMAGE_SAVE_PIXMAP
C       Xv_Server obj
C       LOGICAL v1

      LOGICAL  get_SERVER_JOURNALLING
      EXTERNAL get_SERVER_JOURNALLING
C       Xv_Server obj

      EXTERNAL set_SERVER_JOURNALLING
C       Xv_Server obj
C       LOGICAL v1

      Xv_opaque  get_SERVER_JOURNAL_SYNC_ATOM
      EXTERNAL get_SERVER_JOURNAL_SYNC_ATOM
C       Xv_Server obj

      EXTERNAL set_SERVER_JOURNAL_SYNC_ATOM
C       Xv_Server obj
C       Xv_opaque v1

      INTEGER  get_SERVER_JOURNAL_SYNC_EVENT
      EXTERNAL get_SERVER_JOURNAL_SYNC_EVENT
C       Xv_Server obj

      EXTERNAL set_SERVER_JOURNAL_SYNC_EVENT
C       Xv_Server obj
C       INTEGER v1

      Xv_opaque  get_SERVER_KEY_EVENTS_MAP
      EXTERNAL get_SERVER_KEY_EVENTS_MAP
C       Xv_Server obj

      EXTERNAL set_SERVER_KEY_EVENTS_MAP
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_NONASCII_KEYSTATE
      EXTERNAL get_SERVER_NONASCII_KEYSTATE
C       Xv_Server obj

      EXTERNAL set_SERVER_NONASCII_KEYSTATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_Screen  get_SERVER_NTH_SCREEN
      EXTERNAL get_SERVER_NTH_SCREEN
C       Xv_Server obj
C       INTEGER v1

      EXTERNAL set_SERVER_NTH_SCREEN
C       Xv_Server obj
C       INTEGER v1
C       Xv_Screen v2

      Xv_opaque  get_SERVER_RESOURCE_DB
      EXTERNAL get_SERVER_RESOURCE_DB
C       Xv_Server obj

      Xv_opaque  get_SERVER_SEMANTIC_MAP
      EXTERNAL get_SERVER_SEMANTIC_MAP
C       Xv_Server obj

      EXTERNAL set_SERVER_SEMANTIC_MAP
C       Xv_Server obj
C       Xv_opaque v1

      LOGICAL  get_SERVER_SYNC
      EXTERNAL get_SERVER_SYNC
C       Xv_Server obj

      EXTERNAL set_SERVER_SYNC
C       Xv_Server obj
C       LOGICAL v1

      EXTERNAL set_SERVER_SYNC_AND_PROCESS_EVEN
C       Xv_Server obj

      Xv_opaque  get_SERVER_WM_ADD_DECOR
      EXTERNAL get_SERVER_WM_ADD_DECOR
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_ADD_DECOR
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_CHANGE_STATE
      EXTERNAL get_SERVER_WM_CHANGE_STATE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_CHANGE_STATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_COMMAND
      EXTERNAL get_SERVER_WM_COMMAND
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_COMMAND
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_CLOSE
      EXTERNAL get_SERVER_WM_DECOR_CLOSE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_CLOSE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_FOOTER
      EXTERNAL get_SERVER_WM_DECOR_FOOTER
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_FOOTER
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_HEADER
      EXTERNAL get_SERVER_WM_DECOR_HEADER
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_HEADER
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_OK
      EXTERNAL get_SERVER_WM_DECOR_OK
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_OK
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_PIN
      EXTERNAL get_SERVER_WM_DECOR_PIN
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_PIN
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DECOR_RESIZE
      EXTERNAL get_SERVER_WM_DECOR_RESIZE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DECOR_RESIZE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DEFAULT_BUTTON
      EXTERNAL get_SERVER_WM_DEFAULT_BUTTON
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DEFAULT_BUTTON
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DELETE_DECOR
      EXTERNAL get_SERVER_WM_DELETE_DECOR
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DELETE_DECOR
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DELETE_WINDOW
      EXTERNAL get_SERVER_WM_DELETE_WINDOW
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DELETE_WINDOW
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_DISMISS
      EXTERNAL get_SERVER_WM_DISMISS
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_DISMISS
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_MENU_FULL
      EXTERNAL get_SERVER_WM_MENU_FULL
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_MENU_FULL
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_MENU_LIMITED
      EXTERNAL get_SERVER_WM_MENU_LIMITED
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_MENU_LIMITED
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_NONE
      EXTERNAL get_SERVER_WM_NONE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_NONE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_PIN_IN
      EXTERNAL get_SERVER_WM_PIN_IN
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_PIN_IN
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_PIN_OUT
      EXTERNAL get_SERVER_WM_PIN_OUT
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_PIN_OUT
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_PIN_STATE
      EXTERNAL get_SERVER_WM_PIN_STATE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_PIN_STATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_PROTOCOLS
      EXTERNAL get_SERVER_WM_PROTOCOLS
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_PROTOCOLS
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_RESCALE
      EXTERNAL get_SERVER_WM_RESCALE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_RESCALE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_RESCALE_STATE
      EXTERNAL get_SERVER_WM_RESCALE_STATE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_RESCALE_STATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_SAVE_YOURSELF
      EXTERNAL get_SERVER_WM_SAVE_YOURSELF
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_SAVE_YOURSELF
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_SCALE_LARGE
      EXTERNAL get_SERVER_WM_SCALE_LARGE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_SCALE_LARGE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_SCALE_MEDIUM
      EXTERNAL get_SERVER_WM_SCALE_MEDIUM
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_SCALE_MEDIUM
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_SCALE_SMALL
      EXTERNAL get_SERVER_WM_SCALE_SMALL
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_SCALE_SMALL
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_SCALE_XLARGE
      EXTERNAL get_SERVER_WM_SCALE_XLARGE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_SCALE_XLARGE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_STATE
      EXTERNAL get_SERVER_WM_STATE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_STATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_TAKE_FOCUS
      EXTERNAL get_SERVER_WM_TAKE_FOCUS
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_TAKE_FOCUS
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WINDOW_MOVED
      EXTERNAL get_SERVER_WM_WINDOW_MOVED
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WINDOW_MOVED
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WINMSG_ERROR
      EXTERNAL get_SERVER_WM_WINMSG_ERROR
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WINMSG_ERROR
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WINMSG_STATE
      EXTERNAL get_SERVER_WM_WINMSG_STATE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WINMSG_STATE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WIN_ATTR
      EXTERNAL get_SERVER_WM_WIN_ATTR
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WIN_ATTR
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WIN_BUSY
      EXTERNAL get_SERVER_WM_WIN_BUSY
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WIN_BUSY
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_BASE
      EXTERNAL get_SERVER_WM_WT_BASE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_BASE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_CMD
      EXTERNAL get_SERVER_WM_WT_CMD
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_CMD
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_HELP
      EXTERNAL get_SERVER_WM_WT_HELP
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_HELP
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_NOTICE
      EXTERNAL get_SERVER_WM_WT_NOTICE
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_NOTICE
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_OTHER
      EXTERNAL get_SERVER_WM_WT_OTHER
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_OTHER
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_WM_WT_PROP
      EXTERNAL get_SERVER_WM_WT_PROP
C       Xv_Server obj

      EXTERNAL set_SERVER_WM_WT_PROP
C       Xv_Server obj
C       Xv_opaque v1

      Xv_opaque  get_SERVER_XV_MAP
      EXTERNAL get_SERVER_XV_MAP
C       Xv_Server obj

      EXTERNAL set_SERVER_XV_MAP
C       Xv_Server obj
C       Xv_opaque v1

      INTEGER  get_SERVER_IMAGE_DEPTH
      EXTERNAL get_SERVER_IMAGE_DEPTH
C       Xv_Server obj

      EXTERNAL set_SERVER_IMAGE_DEPTH
C       Xv_Server obj
C       INTEGER v1

      INTEGER  get_SERVER_IMAGE_BITS
      EXTERNAL get_SERVER_IMAGE_BITS
C       Xv_Server obj

      EXTERNAL set_SERVER_IMAGE_BITS
C       Xv_Server obj
C       INTEGER v1

      Cstringp  get_SERVER_IMAGE_COLORMAP
      EXTERNAL get_SERVER_IMAGE_COLORMAP
C       Xv_Server obj

      EXTERNAL set_SERVER_IMAGE_COLORMAP
C       Xv_Server obj
C       CHARACTER v1

      Termsw_mode  get_TERMSW_MODE
      EXTERNAL get_TERMSW_MODE
C       Termsw obj

      EXTERNAL set_TERMSW_MODE
C       Termsw obj
C       Termsw_mode v1

      LOGICAL  get_TEXTSW_ADJUST_IS_PENDING_DEL
      EXTERNAL get_TEXTSW_ADJUST_IS_PENDING_DEL
C       Textsw obj

      EXTERNAL set_TEXTSW_ADJUST_IS_PENDING_DEL
C       Textsw obj
C       LOGICAL v1

      INTEGER  get_TEXTSW_AGAIN_LIMIT
      EXTERNAL get_TEXTSW_AGAIN_LIMIT
C       Textsw obj

      EXTERNAL set_TEXTSW_AGAIN_LIMIT
C       Textsw obj
C       INTEGER v1

      LOGICAL  get_TEXTSW_AGAIN_RECORDING
      EXTERNAL get_TEXTSW_AGAIN_RECORDING
C       Textsw obj

      EXTERNAL set_TEXTSW_AGAIN_RECORDING
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_AUTO_INDENT
      EXTERNAL get_TEXTSW_AUTO_INDENT
C       Textsw obj

      EXTERNAL set_TEXTSW_AUTO_INDENT
C       Textsw obj
C       LOGICAL v1

      INTEGER  get_TEXTSW_AUTO_SCROLL_BY
      EXTERNAL get_TEXTSW_AUTO_SCROLL_BY
C       Textsw obj

      EXTERNAL set_TEXTSW_AUTO_SCROLL_BY
C       Textsw obj
C       INTEGER v1

      LOGICAL  get_TEXTSW_BLINK_CARET
      EXTERNAL get_TEXTSW_BLINK_CARET
C       Textsw obj

      EXTERNAL set_TEXTSW_BLINK_CARET
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_BROWSING
      EXTERNAL get_TEXTSW_BROWSING
C       Textsw obj

      EXTERNAL set_TEXTSW_BROWSING
C       Textsw obj
C       LOGICAL v1

      INTEGER  get_TEXTSW_CHECKPOINT_FREQUENCY
      EXTERNAL get_TEXTSW_CHECKPOINT_FREQUENCY
C       Textsw obj

      EXTERNAL set_TEXTSW_CHECKPOINT_FREQUENCY
C       Textsw obj
C       INTEGER v1

      Xv_opaque  get_TEXTSW_CLIENT_DATA
      EXTERNAL get_TEXTSW_CLIENT_DATA
C       Textsw obj

      EXTERNAL set_TEXTSW_CLIENT_DATA
C       Textsw obj
C       Xv_opaque v1

      Xv_opaque  get_TEXTSW_COALESCE_WITH
      EXTERNAL get_TEXTSW_COALESCE_WITH
C       Textsw obj

      EXTERNAL set_TEXTSW_COALESCE_WITH
C       Textsw obj
C       Xv_opaque v1

      LOGICAL  get_TEXTSW_CONFIRM_OVERWRITE
      EXTERNAL get_TEXTSW_CONFIRM_OVERWRITE
C       Textsw obj

      EXTERNAL set_TEXTSW_CONFIRM_OVERWRITE
C       Textsw obj
C       LOGICAL v1

      Textsw_index  get_TEXTSW_CONTENTS
      EXTERNAL get_TEXTSW_CONTENTS
C       Textsw obj
C       Textsw_index v1
C       CHARACTER v2
C       INTEGER v3

      LOGICAL  get_TEXTSW_CONTROL_CHARS_USE_FON
      EXTERNAL get_TEXTSW_CONTROL_CHARS_USE_FON
C       Textsw obj

      EXTERNAL set_TEXTSW_CONTROL_CHARS_USE_FON
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_DESTROY_ALL_VIEWS
      EXTERNAL get_TEXTSW_DESTROY_ALL_VIEWS
C       Textsw obj

      EXTERNAL set_TEXTSW_DESTROY_ALL_VIEWS
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_DISABLE_CD
      EXTERNAL get_TEXTSW_DISABLE_CD
C       Textsw obj

      EXTERNAL set_TEXTSW_DISABLE_CD
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_DISABLE_LOAD
      EXTERNAL get_TEXTSW_DISABLE_LOAD
C       Textsw obj

      EXTERNAL set_TEXTSW_DISABLE_LOAD
C       Textsw obj
C       LOGICAL v1

      CHARACTER  get_TEXTSW_EDIT_BACK_CHAR
      EXTERNAL get_TEXTSW_EDIT_BACK_CHAR
C       Textsw obj

      EXTERNAL set_TEXTSW_EDIT_BACK_CHAR
C       Textsw obj
C       CHARACTER v1

      CHARACTER  get_TEXTSW_EDIT_BACK_LINE
      EXTERNAL get_TEXTSW_EDIT_BACK_LINE
C       Textsw obj

      EXTERNAL set_TEXTSW_EDIT_BACK_LINE
C       Textsw obj
C       CHARACTER v1

      CHARACTER  get_TEXTSW_EDIT_BACK_WORD
      EXTERNAL get_TEXTSW_EDIT_BACK_WORD
C       Textsw obj

      EXTERNAL set_TEXTSW_EDIT_BACK_WORD
C       Textsw obj
C       CHARACTER v1

      INTEGER  get_TEXTSW_EDIT_COUNT
      EXTERNAL get_TEXTSW_EDIT_COUNT
C       Textsw obj

      EXTERNAL set_TEXTSW_EDIT_COUNT
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TEXTSW_END_ALL_VIEWS
C       Textsw obj

      Cstringp  get_TEXTSW_ERROR_MSG
      EXTERNAL get_TEXTSW_ERROR_MSG
C       Textsw obj

      EXTERNAL set_TEXTSW_ERROR_MSG
C       Textsw obj
C       CHARACTER v1

      INTEGER  get_TEXTSW_EXTRAS_CMD_MENU
      EXTERNAL get_TEXTSW_EXTRAS_CMD_MENU
C       Textsw obj

      EXTERNAL set_TEXTSW_EXTRAS_CMD_MENU
C       Textsw obj
C       INTEGER v1

      Cstringp  get_TEXTSW_FILE
      EXTERNAL get_TEXTSW_FILE
C       Textsw obj

      EXTERNAL set_TEXTSW_FILE
C       Textsw obj
C       CHARACTER v1

      EXTERNAL set_TEXTSW_FILE_CONTENTS
C       Textsw obj
C       CHARACTER v1

      INTEGER  get_TEXTSW_FIRST
      EXTERNAL get_TEXTSW_FIRST
C       Textsw obj

      EXTERNAL set_TEXTSW_FIRST
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_FIRST_LINE
      EXTERNAL get_TEXTSW_FIRST_LINE
C       Textsw obj

      EXTERNAL set_TEXTSW_FIRST_LINE
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_FOR_ALL_VIEWS
      EXTERNAL get_TEXTSW_FOR_ALL_VIEWS
C       Textsw obj

      EXTERNAL set_TEXTSW_FOR_ALL_VIEWS
C       Textsw obj

      Xv_Font  get_TEXTSW_FONT
      EXTERNAL get_TEXTSW_FONT
C       Textsw obj

      EXTERNAL set_TEXTSW_FONT
C       Textsw obj
C       Xv_Font v1

      INTEGER  get_TEXTSW_HISTORY_LIMIT
      EXTERNAL get_TEXTSW_HISTORY_LIMIT
C       Textsw obj

      EXTERNAL set_TEXTSW_HISTORY_LIMIT
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_IGNORE_LIMIT
      EXTERNAL get_TEXTSW_IGNORE_LIMIT
C       Textsw obj

      EXTERNAL set_TEXTSW_IGNORE_LIMIT
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_INSERTION_POINT
      EXTERNAL get_TEXTSW_INSERTION_POINT
C       Textsw obj

      EXTERNAL set_TEXTSW_INSERTION_POINT
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TEXTSW_INSERT_FROM_FILE
C       Textsw obj
C       CHARACTER v1

      Textsw_enum  get_TEXTSW_INSERT_MAKES_VISIBLE
      EXTERNAL get_TEXTSW_INSERT_MAKES_VISIBLE
C       Textsw obj

      EXTERNAL set_TEXTSW_INSERT_MAKES_VISIBLE
C       Textsw obj
C       Textsw_enum v1

      LOGICAL  get_TEXTSW_INSERT_ONLY
      EXTERNAL get_TEXTSW_INSERT_ONLY
C       Textsw obj

      EXTERNAL set_TEXTSW_INSERT_ONLY
C       Textsw obj
C       LOGICAL v1

      INTEGER  get_TEXTSW_LEFT_MARGIN
      EXTERNAL get_TEXTSW_LEFT_MARGIN
C       Textsw obj

      EXTERNAL set_TEXTSW_LEFT_MARGIN
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_LENGTH
      EXTERNAL get_TEXTSW_LENGTH
C       Textsw obj

      Textsw_enum  get_TEXTSW_LINE_BREAK_ACTION
      EXTERNAL get_TEXTSW_LINE_BREAK_ACTION
C       Textsw obj

      EXTERNAL set_TEXTSW_LINE_BREAK_ACTION
C       Textsw obj
C       Textsw_enum v1

      INTEGER  get_TEXTSW_LOWER_CONTEXT
      EXTERNAL get_TEXTSW_LOWER_CONTEXT
C       Textsw obj

      EXTERNAL set_TEXTSW_LOWER_CONTEXT
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_MEMORY_MAXIMUM
      EXTERNAL get_TEXTSW_MEMORY_MAXIMUM
C       Textsw obj

      EXTERNAL set_TEXTSW_MEMORY_MAXIMUM
C       Textsw obj
C       INTEGER v1

      Menu  get_TEXTSW_MENU
      EXTERNAL get_TEXTSW_MENU
C       Textsw obj

      EXTERNAL set_TEXTSW_MENU
C       Textsw obj
C       Menu v1

      LOGICAL  get_TEXTSW_MODIFIED
      EXTERNAL get_TEXTSW_MODIFIED
C       Textsw obj

      INTEGER  get_TEXTSW_MULTI_CLICK_SPACE
      EXTERNAL get_TEXTSW_MULTI_CLICK_SPACE
C       Textsw obj

      EXTERNAL set_TEXTSW_MULTI_CLICK_SPACE
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_MULTI_CLICK_TIMEOUT
      EXTERNAL get_TEXTSW_MULTI_CLICK_TIMEOUT
C       Textsw obj

      EXTERNAL set_TEXTSW_MULTI_CLICK_TIMEOUT
C       Textsw obj
C       INTEGER v1

      LOGICAL  get_TEXTSW_MUST_SHOW_CARET
      EXTERNAL get_TEXTSW_MUST_SHOW_CARET
C       Textsw obj

      EXTERNAL set_TEXTSW_MUST_SHOW_CARET
C       Textsw obj
C       LOGICAL v1

      Cstringp  get_TEXTSW_NAME_TO_USE
      EXTERNAL get_TEXTSW_NAME_TO_USE
C       Textsw obj

      EXTERNAL set_TEXTSW_NAME_TO_USE
C       Textsw obj
C       CHARACTER v1

      INTEGER  get_TEXTSW_NOTIFY_LEVEL
      EXTERNAL get_TEXTSW_NOTIFY_LEVEL
C       Textsw obj

      EXTERNAL set_TEXTSW_NOTIFY_LEVEL
C       Textsw obj
C       INTEGER v1

      Textsw_notify_procp  get_TEXTSW_NOTIFY_PROC
      EXTERNAL get_TEXTSW_NOTIFY_PROC
C       Textsw obj

      EXTERNAL set_TEXTSW_NOTIFY_PROC
C       Textsw obj
C       Textsw_notify_procp v1

      LOGICAL  get_TEXTSW_NO_CD
      EXTERNAL get_TEXTSW_NO_CD
C       Textsw obj

      EXTERNAL set_TEXTSW_NO_CD
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_NO_REPAINT_TIL_EVENT
      EXTERNAL get_TEXTSW_NO_REPAINT_TIL_EVENT
C       Textsw obj

      EXTERNAL set_TEXTSW_NO_REPAINT_TIL_EVENT
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_NO_RESET_TO_SCRATCH
      EXTERNAL get_TEXTSW_NO_RESET_TO_SCRATCH
C       Textsw obj

      EXTERNAL set_TEXTSW_NO_RESET_TO_SCRATCH
C       Textsw obj
C       LOGICAL v1

      LOGICAL  get_TEXTSW_NO_SELECTION_SERVICE
      EXTERNAL get_TEXTSW_NO_SELECTION_SERVICE
C       Textsw obj

      EXTERNAL set_TEXTSW_NO_SELECTION_SERVICE
C       Textsw obj
C       LOGICAL v1

      EXTERNAL get_TEXTSW_PIXWIN
C       RECORD /Pixwin/  returnval__
C       Textsw obj

      LOGICAL  get_TEXTSW_READ_ONLY
      EXTERNAL get_TEXTSW_READ_ONLY
C       Textsw obj

      EXTERNAL set_TEXTSW_READ_ONLY
C       Textsw obj
C       LOGICAL v1

      INTEGER  get_TEXTSW_RESET_MODE
      EXTERNAL get_TEXTSW_RESET_MODE
C       Textsw obj

      EXTERNAL set_TEXTSW_RESET_MODE
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TEXTSW_RESET_TO_CONTENTS
C       Textsw obj

      INTEGER  get_TEXTSW_RIGHT_MARGIN
      EXTERNAL get_TEXTSW_RIGHT_MARGIN
C       Textsw obj

      EXTERNAL set_TEXTSW_RIGHT_MARGIN
C       Textsw obj
C       INTEGER v1

      Scrollbar  get_TEXTSW_SCROLLBAR
      EXTERNAL get_TEXTSW_SCROLLBAR
C       Textsw obj

      EXTERNAL set_TEXTSW_SCROLLBAR
C       Textsw obj
C       Scrollbar v1

      LOGICAL  get_TEXTSW_STORE_CHANGES_FILE
      EXTERNAL get_TEXTSW_STORE_CHANGES_FILE
C       Textsw obj

      EXTERNAL set_TEXTSW_STORE_CHANGES_FILE
C       Textsw obj
C       LOGICAL v1

      Menu  get_TEXTSW_SUBMENU_EDIT
      EXTERNAL get_TEXTSW_SUBMENU_EDIT
C       Textsw obj

      Menu  get_TEXTSW_SUBMENU_FILE
      EXTERNAL get_TEXTSW_SUBMENU_FILE
C       Textsw obj

      Menu  get_TEXTSW_SUBMENU_FIND
      EXTERNAL get_TEXTSW_SUBMENU_FIND
C       Textsw obj

      Menu  get_TEXTSW_SUBMENU_VIEW
      EXTERNAL get_TEXTSW_SUBMENU_VIEW
C       Textsw obj

      INTEGER  get_TEXTSW_TAB_WIDTH
      EXTERNAL get_TEXTSW_TAB_WIDTH
C       Textsw obj

      EXTERNAL set_TEXTSW_TAB_WIDTH
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TEXTSW_TAB_WIDTHS_1
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TEXTSW_TAB_WIDTHS_2
C       Textsw obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_TEXTSW_TAB_WIDTHS_3
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_TEXTSW_TAB_WIDTHS_4
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_TEXTSW_TAB_WIDTHS_5
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_TEXTSW_TAB_WIDTHS_6
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_TEXTSW_TAB_WIDTHS_7
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_TEXTSW_TAB_WIDTHS_8
C       Textsw obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      Cstringp  get_TEXTSW_TEMP_FILENAME
      EXTERNAL get_TEXTSW_TEMP_FILENAME
C       Textsw obj

      EXTERNAL set_TEXTSW_TEMP_FILENAME
C       Textsw obj
C       CHARACTER v1

      Frame  get_TEXTSW_TOOL
      EXTERNAL get_TEXTSW_TOOL
C       Textsw obj

      EXTERNAL set_TEXTSW_TOOL
C       Textsw obj
C       Frame v1

      EXTERNAL set_TEXTSW_UPDATE_SCROLLBAR
C       Textsw obj

      INTEGER  get_TEXTSW_UPPER_CONTEXT
      EXTERNAL get_TEXTSW_UPPER_CONTEXT
C       Textsw obj

      EXTERNAL set_TEXTSW_UPPER_CONTEXT
C       Textsw obj
C       INTEGER v1

      INTEGER  get_TEXTSW_WRAPAROUND_SIZE
      EXTERNAL get_TEXTSW_WRAPAROUND_SIZE
C       Textsw obj

      EXTERNAL set_TEXTSW_WRAPAROUND_SIZE
C       Textsw obj
C       INTEGER v1

      EXTERNAL set_TTY_ARGV
C       Tty obj
C       Cstringp_arrayp v1

      INTEGER  get_TTY_BOLDSTYLE
      EXTERNAL get_TTY_BOLDSTYLE
C       Tty obj

      EXTERNAL set_TTY_BOLDSTYLE
C       Tty obj
C       INTEGER v1

      Cstringp  get_TTY_BOLDSTYLE_NAME
      EXTERNAL get_TTY_BOLDSTYLE_NAME
C       Tty obj

      EXTERNAL set_TTY_BOLDSTYLE_NAME
C       Tty obj
C       CHARACTER v1

      EXTERNAL set_TTY_CONSOLE
C       Tty obj
C       LOGICAL v1

      EXTERNAL set_TTY_INPUT
C       Tty obj
C       CHARACTER v1
C       INTEGER v2
C       INTEGER v3

      INTEGER  get_TTY_INVERSE_MODE
      EXTERNAL get_TTY_INVERSE_MODE
C       Tty obj

      EXTERNAL set_TTY_INVERSE_MODE
C       Tty obj
C       INTEGER v1

      Xv_opaque  get_TTY_OUTPUT
      EXTERNAL get_TTY_OUTPUT
C       Tty obj

      EXTERNAL set_TTY_OUTPUT
C       Tty obj
C       Xv_opaque v1

      LOGICAL  get_TTY_PAGE_MODE
      EXTERNAL get_TTY_PAGE_MODE
C       Tty obj

      EXTERNAL set_TTY_PAGE_MODE
C       Tty obj
C       LOGICAL v1

      INTEGER  get_TTY_PID
      EXTERNAL get_TTY_PID
C       Tty obj

      EXTERNAL set_TTY_PID
C       Tty obj
C       INTEGER v1

      INTEGER  get_TTY_PTY_FD
      EXTERNAL get_TTY_PTY_FD
C       Tty obj

      EXTERNAL set_TTY_QUIT_ON_CHILD_DEATH
C       Tty obj
C       LOGICAL v1

      INTEGER  get_TTY_TTY_FD
      EXTERNAL get_TTY_TTY_FD
C       Tty obj

      INTEGER  get_TTY_UNDERLINE_MODE
      EXTERNAL get_TTY_UNDERLINE_MODE
C       Tty obj

      EXTERNAL set_TTY_UNDERLINE_MODE
C       Tty obj
C       INTEGER v1

      EXTERNAL set_WIN_ALARM
C       Xv_window obj

      Xv_opaque  get_WIN_ALARM_DATA
      EXTERNAL get_WIN_ALARM_DATA
C       Xv_window obj

      EXTERNAL set_WIN_BACK
C       Xv_window obj

      INTEGER  get_WIN_BACKGROUND_COLOR
      EXTERNAL get_WIN_BACKGROUND_COLOR
C       Xv_window obj

      EXTERNAL set_WIN_BACKGROUND_COLOR
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_BELOW
C       Xv_window obj
C       Xv_Window v1

      EXTERNAL set_WIN_BIT_GRAVITY
C       Xv_window obj
C       INTEGER v1

      LOGICAL  get_WIN_BORDER
      EXTERNAL get_WIN_BORDER
C       Xv_window obj

      EXTERNAL set_WIN_BORDER
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_BOTTOM_MARGIN
      EXTERNAL get_WIN_BOTTOM_MARGIN
C       Xv_window obj

      EXTERNAL set_WIN_BOTTOM_MARGIN
C       Xv_window obj
C       INTEGER v1

      Xv_opaque  get_WIN_CLIENT_DATA
      EXTERNAL get_WIN_CLIENT_DATA
C       Xv_window obj

      EXTERNAL set_WIN_CLIENT_DATA
C       Xv_window obj
C       Xv_opaque v1

      Cstringp  get_WIN_CMD_LINE
      EXTERNAL get_WIN_CMD_LINE
C       Xv_window obj

      EXTERNAL set_WIN_CMD_LINE
C       Xv_window obj
C       CHARACTER v1

      Xv_opaque  get_WIN_CMS
      EXTERNAL get_WIN_CMS
C       Xv_window obj

      EXTERNAL set_WIN_CMS
C       Xv_window obj
C       Xv_opaque v1

      EXTERNAL set_WIN_CMS_CHANGE
C       Xv_window obj

      Xv_cmsdata_ptr  get_WIN_CMS_DATA
      EXTERNAL get_WIN_CMS_DATA
C       Xv_window obj

      EXTERNAL set_WIN_CMS_DATA
C       Xv_window obj
C       Xv_cmsdata_ptr v1

      Cstringp  get_WIN_CMS_NAME
      EXTERNAL get_WIN_CMS_NAME
C       Xv_window obj

      EXTERNAL set_WIN_CMS_NAME
C       Xv_window obj
C       CHARACTER v1

      LOGICAL  get_WIN_COLLAPSE_EXPOSURES
      EXTERNAL get_WIN_COLLAPSE_EXPOSURES
C       Xv_window obj

      EXTERNAL set_WIN_COLLAPSE_EXPOSURES
C       Xv_window obj
C       LOGICAL v1

      Xv_opaque  get_WIN_COLOR_INFO
      EXTERNAL get_WIN_COLOR_INFO
C       Xv_window obj

      EXTERNAL set_WIN_COLOR_INFO
C       Xv_window obj
C       Xv_opaque v1

      INTEGER  get_WIN_COLUMN_GAP
      EXTERNAL get_WIN_COLUMN_GAP
C       Xv_window obj

      EXTERNAL set_WIN_COLUMN_GAP
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_COLUMN_WIDTH
      EXTERNAL get_WIN_COLUMN_WIDTH
C       Xv_window obj

      EXTERNAL set_WIN_COLUMN_WIDTH
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_COLUMNS
      EXTERNAL get_WIN_COLUMNS
C       Xv_window obj

      EXTERNAL set_WIN_COLUMNS
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_CONSUME_EVENT
      EXTERNAL get_WIN_CONSUME_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_CONSUME_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_CONSUME_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_CONSUME_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_CONSUME_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_CONSUME_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_CONSUME_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_CONSUME_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_CONSUME_KBD_EVENT
      EXTERNAL get_WIN_CONSUME_KBD_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_CONSUME_KBD_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_CONSUME_KBD_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_CONSUME_PICK_EVENT
      EXTERNAL get_WIN_CONSUME_PICK_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_CONSUME_PICK_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_CONSUME_PICK_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_CONSUME_X_EVENT_MASK
      EXTERNAL get_WIN_CONSUME_X_EVENT_MASK
C       Xv_window obj

      EXTERNAL set_WIN_CONSUME_X_EVENT_MASK
C       Xv_window obj
C       INTEGER v1

      Xv_Cursor  get_WIN_CURSOR
      EXTERNAL get_WIN_CURSOR
C       Xv_window obj

      EXTERNAL set_WIN_CURSOR
C       Xv_window obj
C       Xv_Cursor v1

      INTEGER  get_WIN_DEPTH
      EXTERNAL get_WIN_DEPTH
C       Xv_window obj

      INTEGER  get_WIN_DESIRED_HEIGHT
      EXTERNAL get_WIN_DESIRED_HEIGHT
C       Xv_window obj

      EXTERNAL set_WIN_DESIRED_HEIGHT
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_DESIRED_WIDTH
      EXTERNAL get_WIN_DESIRED_WIDTH
C       Xv_window obj

      EXTERNAL set_WIN_DESIRED_WIDTH
C       Xv_window obj
C       INTEGER v1

      Cstringp  get_WIN_DEVICE_NAME
      EXTERNAL get_WIN_DEVICE_NAME
C       Xv_window obj

      INTEGER  get_WIN_DEVICE_NUMBER
      EXTERNAL get_WIN_DEVICE_NUMBER
C       Xv_window obj

      Cstringp  get_WIN_ERROR_MSG
      EXTERNAL get_WIN_ERROR_MSG
C       Xv_window obj

      EXTERNAL set_WIN_ERROR_MSG
C       Xv_window obj
C       CHARACTER v1

      Win_event_procp  get_WIN_EVENT_PROC
      EXTERNAL get_WIN_EVENT_PROC
C       Xv_window obj

      EXTERNAL set_WIN_EVENT_PROC
C       Xv_window obj
C       Win_event_procp v1

      Xv_opaque  get_WIN_FD
      EXTERNAL get_WIN_FD
C       Xv_window obj

      EXTERNAL set_WIN_FINDINTERSECT
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      INTEGER  get_WIN_FIT_HEIGHT
      EXTERNAL get_WIN_FIT_HEIGHT
C       Xv_window obj

      EXTERNAL set_WIN_FIT_HEIGHT
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_FIT_WIDTH
      EXTERNAL get_WIN_FIT_WIDTH
C       Xv_window obj

      EXTERNAL set_WIN_FIT_WIDTH
C       Xv_window obj
C       INTEGER v1

      Xv_Font  get_WIN_FONT
      EXTERNAL get_WIN_FONT
C       Xv_window obj

      EXTERNAL set_WIN_FONT
C       Xv_window obj
C       Xv_Font v1

      INTEGER  get_WIN_FOREGROUND_COLOR
      EXTERNAL get_WIN_FOREGROUND_COLOR
C       Xv_window obj

      EXTERNAL set_WIN_FOREGROUND_COLOR
C       Xv_window obj
C       INTEGER v1

      Frame  get_WIN_FRAME
      EXTERNAL get_WIN_FRAME
C       Xv_window obj

      EXTERNAL set_WIN_FRAME
C       Xv_window obj
C       Frame v1

      EXTERNAL set_WIN_FRONT
C       Xv_window obj

      EXTERNAL set_WIN_GRAB_ALL_INPUT
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_HEIGHT
      EXTERNAL get_WIN_HEIGHT
C       Xv_window obj

      EXTERNAL set_WIN_HEIGHT
C       Xv_window obj
C       INTEGER v1

      Scrollbar  get_WIN_HORIZONTAL_SCROLLBAR
      EXTERNAL get_WIN_HORIZONTAL_SCROLLBAR
C       Xv_window obj

      EXTERNAL set_WIN_HORIZONTAL_SCROLLBAR
C       Xv_window obj
C       Scrollbar v1

      INTEGER  get_WIN_IGNORE_EVENT
      EXTERNAL get_WIN_IGNORE_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_IGNORE_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_IGNORE_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_IGNORE_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_IGNORE_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_IGNORE_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_IGNORE_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_IGNORE_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_IGNORE_KBD_EVENT
      EXTERNAL get_WIN_IGNORE_KBD_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_IGNORE_KBD_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_IGNORE_KBD_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_IGNORE_PICK_EVENT
      EXTERNAL get_WIN_IGNORE_PICK_EVENT
C       Xv_window obj

      EXTERNAL set_WIN_IGNORE_PICK_EVENT
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_1
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_2
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_3
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_4
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_5
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_6
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_7
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7

      EXTERNAL set_WIN_IGNORE_PICK_EVENTS_8
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2
C       INTEGER v3
C       INTEGER v4
C       INTEGER v5
C       INTEGER v6
C       INTEGER v7
C       INTEGER v8

      INTEGER  get_WIN_IGNORE_X_EVENT_MASK
      EXTERNAL get_WIN_IGNORE_X_EVENT_MASK
C       Xv_window obj

      EXTERNAL set_WIN_IGNORE_X_EVENT_MASK
C       Xv_window obj
C       INTEGER v1

      EXTERNAL get_WIN_INPUT_MASK
C       RECORD /Inputmask/  returnval__
C       Xv_window obj

      EXTERNAL set_WIN_INPUT_MASK
C       Xv_window obj
C       RECORD /Inputmask/ v1

      LOGICAL  get_WIN_INHERIT_COLORS
      EXTERNAL get_WIN_INHERIT_COLORS
C       Xv_window obj

      EXTERNAL set_WIN_INHERIT_COLORS
C       Xv_window obj
C       LOGICAL v1

      LOGICAL  get_WIN_IS_CLIENT_PANE
      EXTERNAL get_WIN_IS_CLIENT_PANE
C       Xv_window obj

      EXTERNAL set_WIN_IS_CLIENT_PANE
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_IS_IN_FULLSCREEN_MODE
      EXTERNAL get_WIN_IS_IN_FULLSCREEN_MODE
C       Xv_window obj

      EXTERNAL set_WIN_IS_IN_FULLSCREEN_MODE
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_IS_ROOT
C       Xv_window obj

      LOGICAL  get_WIN_KBD_FOCUS
      EXTERNAL get_WIN_KBD_FOCUS
C       Xv_window obj

      EXTERNAL set_WIN_KBD_FOCUS
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_LEFT_MARGIN
      EXTERNAL get_WIN_LEFT_MARGIN
C       Xv_window obj

      EXTERNAL set_WIN_LEFT_MARGIN
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_LOCKDATA
C       Xv_window obj

      LOGICAL  get_WIN_MAP
      EXTERNAL get_WIN_MAP
C       Xv_window obj

      EXTERNAL set_WIN_MAP
C       Xv_window obj
C       LOGICAL v1

      Menu  get_WIN_MENU
      EXTERNAL get_WIN_MENU
C       Xv_window obj

      EXTERNAL set_WIN_MENU
C       Xv_window obj
C       Menu v1

      Xv_opaque  get_WIN_MESSAGE_DATA
      EXTERNAL get_WIN_MESSAGE_DATA
C       Xv_window obj

      EXTERNAL set_WIN_MESSAGE_DATA
C       Xv_window obj
C       Xv_opaque v1

      INTEGER  get_WIN_MESSAGE_FORMAT
      EXTERNAL get_WIN_MESSAGE_FORMAT
C       Xv_window obj

      EXTERNAL set_WIN_MESSAGE_FORMAT
C       Xv_window obj
C       INTEGER v1

      Xv_opaque  get_WIN_MESSAGE_TYPE
      EXTERNAL get_WIN_MESSAGE_TYPE
C       Xv_window obj

      EXTERNAL set_WIN_MESSAGE_TYPE
C       Xv_window obj
C       Xv_opaque v1

      EXTERNAL get_WIN_MOUSE_XY
C       RECORD /Rect/  returnval__
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      EXTERNAL set_WIN_MOUSE_XY
C       Xv_window obj
C       INTEGER v1
C       INTEGER v2

      Cstringp  get_WIN_NAME
      EXTERNAL get_WIN_NAME
C       Xv_window obj

      EXTERNAL set_WIN_NAME
C       Xv_window obj
C       CHARACTER v1

      Win_event_procp  get_WIN_NOTIFY_EVENT_PROC
      EXTERNAL get_WIN_NOTIFY_EVENT_PROC
C       Xv_window obj

      EXTERNAL set_WIN_NOTIFY_EVENT_PROC
C       Xv_window obj
C       Win_event_procp v1

      Win_event_procp  get_WIN_NOTIFY_IMMEDIATE_EVENT_P
      EXTERNAL get_WIN_NOTIFY_IMMEDIATE_EVENT_P
C       Xv_window obj

      EXTERNAL set_WIN_NOTIFY_IMMEDIATE_EVENT_P
C       Xv_window obj
C       Win_event_procp v1

      Win_event_procp  get_WIN_NOTIFY_SAFE_EVENT_PROC
      EXTERNAL get_WIN_NOTIFY_SAFE_EVENT_PROC
C       Xv_window obj

      EXTERNAL set_WIN_NOTIFY_SAFE_EVENT_PROC
C       Xv_window obj
C       Win_event_procp v1

      LOGICAL  get_WIN_NO_CLIPPING
      EXTERNAL get_WIN_NO_CLIPPING
C       Xv_window obj

      EXTERNAL set_WIN_NO_CLIPPING
C       Xv_window obj
C       LOGICAL v1

      EXTERNAL get_WIN_OUTER_RECT
C       RECORD /Rect/  returnval__
C       Xv_window obj

      EXTERNAL set_WIN_OUTER_RECT
C       Xv_window obj
C       RECORD /Rect/ v1

      Xv_Window  get_WIN_OWNER
      EXTERNAL get_WIN_OWNER
C       Xv_window obj

      Xv_Window  get_WIN_PARENT
      EXTERNAL get_WIN_PARENT
C       Xv_window obj

      EXTERNAL set_WIN_PARENT
C       Xv_window obj
C       Xv_Window v1

      INTEGER  get_WIN_PERCENT_HEIGHT
      EXTERNAL get_WIN_PERCENT_HEIGHT
C       Xv_window obj

      EXTERNAL set_WIN_PERCENT_HEIGHT
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_PERCENT_WIDTH
      EXTERNAL get_WIN_PERCENT_WIDTH
C       Xv_window obj

      EXTERNAL set_WIN_PERCENT_WIDTH
C       Xv_window obj
C       INTEGER v1

      EXTERNAL get_WIN_PICK_INPUT_MASK
C       RECORD /Inputmask/  returnval__
C       Xv_window obj

      EXTERNAL set_WIN_PICK_INPUT_MASK
C       Xv_window obj
C       RECORD /Inputmask/ v1

      EXTERNAL get_WIN_PIXWIN
C       RECORD /Pixwin/  returnval__
C       Xv_window obj

      Rect_ptr  get_WIN_RECT
      EXTERNAL get_WIN_RECT
C       Xv_window obj

      EXTERNAL set_WIN_RECT
C       Xv_window obj
C       Rect_ptr v1

      INTEGER  get_WIN_RECT_INFO
      EXTERNAL get_WIN_RECT_INFO
C       Xv_window obj

      EXTERNAL set_WIN_RECT_INFO
C       Xv_window obj
C       INTEGER v1

      LOGICAL  get_WIN_RETAINED
      EXTERNAL get_WIN_RETAINED
C       Xv_window obj

      EXTERNAL set_WIN_RETAINED
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_RIGHT_MARGIN
      EXTERNAL get_WIN_RIGHT_MARGIN
C       Xv_window obj

      EXTERNAL set_WIN_RIGHT_MARGIN
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_WIN_RIGHT_OF
C       Xv_window obj
C       Xv_Window v1

      INTEGER  get_WIN_ROWS
      EXTERNAL get_WIN_ROWS
C       Xv_window obj

      EXTERNAL set_WIN_ROWS
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_ROW_GAP
      EXTERNAL get_WIN_ROW_GAP
C       Xv_window obj

      EXTERNAL set_WIN_ROW_GAP
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_ROW_HEIGHT
      EXTERNAL get_WIN_ROW_HEIGHT
C       Xv_window obj

      EXTERNAL set_WIN_ROW_HEIGHT
C       Xv_window obj
C       INTEGER v1

      LOGICAL  get_WIN_SAVE_UNDER
      EXTERNAL get_WIN_SAVE_UNDER
C       Xv_window obj

      EXTERNAL set_WIN_SAVE_UNDER
C       Xv_window obj
C       LOGICAL v1

      EXTERNAL get_WIN_SCREEN_RECT
C       RECORD /Rect/  returnval__
C       Xv_window obj

      EXTERNAL set_WIN_SCREEN_RECT
C       Xv_window obj
C       RECORD /Rect/ v1

      EXTERNAL set_WIN_SELECTION_WINDOW
C       Xv_window obj

      EXTERNAL set_WIN_SET_FOCUS
C       Xv_window obj

      LOGICAL  get_WIN_SHOW
      EXTERNAL get_WIN_SHOW
C       Xv_window obj

      EXTERNAL set_WIN_SHOW
C       Xv_window obj
C       LOGICAL v1

      LOGICAL  get_WIN_TOP_LEVEL
      EXTERNAL get_WIN_TOP_LEVEL
C       Xv_window obj

      EXTERNAL set_WIN_TOP_LEVEL
C       Xv_window obj
C       LOGICAL v1

      LOGICAL  get_WIN_TOP_LEVEL_NO_DECOR
      EXTERNAL get_WIN_TOP_LEVEL_NO_DECOR
C       Xv_window obj

      EXTERNAL set_WIN_TOP_LEVEL_NO_DECOR
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_TOP_MARGIN
      EXTERNAL get_WIN_TOP_MARGIN
C       Xv_window obj

      EXTERNAL set_WIN_TOP_MARGIN
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_TYPE
      EXTERNAL get_WIN_TYPE
C       Xv_window obj

      EXTERNAL set_WIN_TYPE
C       Xv_window obj
C       INTEGER v1

      Scrollbar  get_WIN_VERTICAL_SCROLLBAR
      EXTERNAL get_WIN_VERTICAL_SCROLLBAR
C       Xv_window obj

      EXTERNAL set_WIN_VERTICAL_SCROLLBAR
C       Xv_window obj
C       Scrollbar v1

      INTEGER  get_WIN_WIDTH
      EXTERNAL get_WIN_WIDTH
C       Xv_window obj

      EXTERNAL set_WIN_WIDTH
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_WINDOW_GRAVITY
      EXTERNAL get_WIN_WINDOW_GRAVITY
C       Xv_window obj

      EXTERNAL set_WIN_WINDOW_GRAVITY
C       Xv_window obj
C       INTEGER v1

      INTEGER  get_WIN_X
      EXTERNAL get_WIN_X
C       Xv_window obj

      EXTERNAL set_WIN_X
C       Xv_window obj
C       INTEGER v1

      Xv_opaque  get_WIN_X_CLIP_RECTS
      EXTERNAL get_WIN_X_CLIP_RECTS
C       Xv_window obj

      EXTERNAL set_WIN_X_CLIP_RECTS
C       Xv_window obj
C       Xv_opaque v1

      INTEGER  get_WIN_X_COLOR_INDICES
      EXTERNAL get_WIN_X_COLOR_INDICES
C       Xv_window obj

      LOGICAL  get_WIN_X_PAINT_WINDOW
      EXTERNAL get_WIN_X_PAINT_WINDOW
C       Xv_window obj

      EXTERNAL set_WIN_X_PAINT_WINDOW
C       Xv_window obj
C       LOGICAL v1

      INTEGER  get_WIN_Y
      EXTERNAL get_WIN_Y
C       Xv_window obj

      EXTERNAL set_WIN_Y
C       Xv_window obj
C       INTEGER v1

      EXTERNAL set_XV_INIT_ARGS
C       Xv_object obj
C       INTEGER v1
C       Cstringp_arrayp v2

      EXTERNAL set_XV_INIT_ARGC_PTR_ARGV
C       Xv_object obj
C       INTEGER v1
C       Cstringp_arrayp v2

#endif attrgetset_F.

#ifndef panel_F_DEFINED
#define panel_F_DEFINED

C 	derived from @(#)panel.h 20.100 93/01/21 SMI  
C 
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents 
C 
C  *	pending in the U.S. and foreign countries. See LEGAL_NOTICE 
C 
C  *	file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
#include "canvas_F.h"
#include "font_F.h"
#include "frame_F.h"
#include "generic_F.h"
#include "pkg_public_F.h"
#include "pixrect_F.h"
#include "win_input_F.h"
#include "svrimage_F.h"
C 
C  * PUBLIC definitions
C  
      Xv_pkg_ptr PANEL
      Xv_pkg_ptr PANEL_ABBREV_MENU_BUTTON
      Xv_pkg_ptr PANEL_BUTTON
      Xv_pkg_ptr PANEL_CHOICE
      Xv_pkg_ptr PANEL_DROP_TARGET
      Xv_pkg_ptr PANEL_GAUGE
      Xv_pkg_ptr PANEL_ITEM
      Xv_pkg_ptr PANEL_LIST
      Xv_pkg_ptr PANEL_MESSAGE
      Xv_pkg_ptr PANEL_MULTILINE_TEXT
      Xv_pkg_ptr PANEL_NUMERIC_TEXT
      Xv_pkg_ptr PANEL_SLIDER
      Xv_pkg_ptr PANEL_TEXT
      Xv_pkg_ptr PANEL_VIEW
      Xv_pkg_ptr SCROLLABLE_PANEL
C 
C  * Miscellaneous constants 
C  
      INTEGER PANEL_ITEM_X_START
      PARAMETER (PANEL_ITEM_X_START = 4)
C  offset from left edge 
      INTEGER PANEL_ITEM_Y_START
      PARAMETER (PANEL_ITEM_Y_START = 4)
C  offset from top edge 
C  Panel defined events.
C  * These are given to the Panel's or Panel_item's event proc
C  
      INTEGER PANEL_EVENT_FIRST
      PARAMETER (PANEL_EVENT_FIRST = 32000)
      INTEGER PANEL_EVENT_CANCEL
      PARAMETER (PANEL_EVENT_CANCEL = PANEL_EVENT_FIRST+0)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
      STRUCTURE /Xv_panel_or_item/
          RECORD /Xv_canvas/ parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_or_item_ptr INTEGER*4
#define Panel Xv_opaque
#define Scrollable_panel Xv_opaque
#define Panel_item Xv_opaque
#define Panel_attribute_value Xv_opaque
#define Xv_panel RECORD /Xv_panel_or_item/
#define Xv_item RECORD /Xv_panel_or_item/
#define Panel_view Xv_opaque
#define Xv_panel_message Xv_item
#define Panel_abbrev_menu_button_item Panel_item
#define Panel_button_item Panel_item
#define Panel_choice_item Panel_item
#define Panel_drop_target_item Panel_item
#define Panel_gauge_item Panel_item
#define Panel_list_item Panel_item
#define Panel_message_item Panel_item
#define Panel_slider_item Panel_item
#define Panel_text_item Panel_item
#define Panel_numeric_text_item Panel_item
#define Panel_multiline_text_item Panel_item
#define Panel_background_proc INTEGER
#define Panel_background_procp INTEGER*4
#define Panel_event_proc INTEGER
#define Panel_event_procp INTEGER*4
#define Panel_repaint_proc Canvas_repaint_proc
#define Panel_repaint_procp INTEGER*4
#define Panel_notify_proc Xv_opaque
#define Panel_notify_procp INTEGER*4
C   For button, text and numeric text items this is: 
C       PROC(Panel_item, Event)                      
C   For choice and slider items, this is:            
C       PROC(Panel_item, INTEGER, Event)            
C   For panel list items: this is:                   
C       PROC(Panel_item, Panel_list_op, Event): INTEGER 
C   For choice items, this is:                       
C       PROC(Panel_item, CARDINAL, Event)           
C 
C  * Enumerations 
C  
C   Public Attributes  
C   Panel and Panel_item attributes  
      INTEGER PANEL_ACCEPT_KEYSTROKE
      PARAMETER (PANEL_ACCEPT_KEYSTROKE = 1426131201)
      INTEGER PANEL_CLIENT_DATA
      PARAMETER (PANEL_CLIENT_DATA = 1428425217)
      INTEGER PANEL_EVENT_PROC
      PARAMETER (PANEL_EVENT_PROC = 1428949601)
      INTEGER PANEL_GINFO
      PARAMETER (PANEL_GINFO = 1429015041)
C   PANEL_ITEM_X and PANEL_ITEM_Y must still be supported in addition  
C   to XV_X and XV_Y because they can be used on the panel. When used  
C   on the panel, PANEL_ITEM_X != XV_X.  
      INTEGER PANEL_ITEM_X
      PARAMETER (PANEL_ITEM_X = 1430194241)
      INTEGER PANEL_ITEM_Y
      PARAMETER (PANEL_ITEM_Y = 1430259841)
      INTEGER PANEL_ITEM_X_GAP
      PARAMETER (PANEL_ITEM_X_GAP = 1430325313)
      INTEGER PANEL_ITEM_Y_GAP
      PARAMETER (PANEL_ITEM_Y_GAP = 1430390913)
      INTEGER PANEL_LAYOUT
      PARAMETER (PANEL_LAYOUT = 1431439649)
      INTEGER PANEL_PAINT
      PARAMETER (PANEL_PAINT = 1436551457)
C   Panel attributes  
      INTEGER PANEL_BACKGROUND_PROC
      PARAMETER (PANEL_BACKGROUND_PROC = 1426197089)
      INTEGER PANEL_BLINK_CARET
      PARAMETER (PANEL_BLINK_CARET = 1426327809)
      INTEGER PANEL_BOLD_FONT
      PARAMETER (PANEL_BOLD_FONT = 1426459041)
      INTEGER PANEL_CARET_ITEM
      PARAMETER (PANEL_CARET_ITEM = 1426590209)
      INTEGER PANEL_CURRENT_ITEM
      PARAMETER (PANEL_CURRENT_ITEM = 1426655745)
      INTEGER PANEL_DEFAULT_ITEM
      PARAMETER (PANEL_DEFAULT_ITEM = 1428556289)
      INTEGER PANEL_FOCUS_PW
      PARAMETER (PANEL_FOCUS_PW = 1428621825)
      INTEGER PANEL_EXTRA_PAINT_HEIGHT
      PARAMETER (PANEL_EXTRA_PAINT_HEIGHT = 1429080193)
      INTEGER PANEL_EXTRA_PAINT_WIDTH
      PARAMETER (PANEL_EXTRA_PAINT_WIDTH = 1429211201)
      INTEGER PANEL_FIRST_ITEM
      PARAMETER (PANEL_FIRST_ITEM = 1429473793)
      INTEGER PANEL_FIRST_PAINT_WINDOW
      PARAMETER (PANEL_FIRST_PAINT_WINDOW = 1428359681)
      INTEGER PANEL_ITEM_X_POSITION
      PARAMETER (PANEL_ITEM_X_POSITION = 1429145601)
      INTEGER PANEL_ITEM_Y_POSITION
      PARAMETER (PANEL_ITEM_Y_POSITION = 1429276673)
      INTEGER PANEL_NO_REDISPLAY_ITEM
      PARAMETER (PANEL_NO_REDISPLAY_ITEM = 1428883713)
      INTEGER PANEL_PRIMARY_FOCUS_ITEM
      PARAMETER (PANEL_PRIMARY_FOCUS_ITEM = 1429408257)
      INTEGER PANEL_REPAINT_PROC
      PARAMETER (PANEL_REPAINT_PROC = 1436813921)
      INTEGER PANEL_STATUS
      PARAMETER (PANEL_STATUS = 1436944897)
      INTEGER PANEL_BORDER
      PARAMETER (PANEL_BORDER = 1440352513)
C   Panel_item attributes  
      INTEGER PANEL_BUSY
      PARAMETER (PANEL_BUSY = 1426524417)
      INTEGER PANEL_CHILD_CARET_ITEM
      PARAMETER (PANEL_CHILD_CARET_ITEM = 1431374337)
      INTEGER PANEL_INACTIVE
      PARAMETER (PANEL_INACTIVE = 1429604609)
      INTEGER PANEL_ITEM_CLASS
      PARAMETER (PANEL_ITEM_CLASS = 1429735937)
      INTEGER PANEL_ITEM_COLOR
      PARAMETER (PANEL_ITEM_COLOR = 1429866497)
      INTEGER PANEL_ITEM_CREATED
      PARAMETER (PANEL_ITEM_CREATED = 1429932289)
      INTEGER PANEL_ITEM_DEAF
      PARAMETER (PANEL_ITEM_DEAF = 1429801217)
      INTEGER PANEL_ITEM_LABEL_RECT
      PARAMETER (PANEL_ITEM_LABEL_RECT = 1429670369)
      INTEGER PANEL_ITEM_MENU
      PARAMETER (PANEL_ITEM_MENU = 1429998081)
      INTEGER PANEL_ITEM_NTH_WINDOW
      PARAMETER (PANEL_ITEM_NTH_WINDOW = 1431112193)
      INTEGER PANEL_ITEM_NWINDOWS
      PARAMETER (PANEL_ITEM_NWINDOWS = 1431242753)
      INTEGER PANEL_ITEM_RECT
      PARAMETER (PANEL_ITEM_RECT = 1430129121)
      INTEGER PANEL_ITEM_VALUE_RECT
      PARAMETER (PANEL_ITEM_VALUE_RECT = 1430718945)
      INTEGER PANEL_ITEM_WANTS_ADJUST
      PARAMETER (PANEL_ITEM_WANTS_ADJUST = 1430063361)
      INTEGER PANEL_ITEM_WANTS_ISO
      PARAMETER (PANEL_ITEM_WANTS_ISO = 1430587649)
      INTEGER PANEL_LABEL_BOLD
      PARAMETER (PANEL_LABEL_BOLD = 1430522113)
      INTEGER PANEL_LABEL_FONT
      PARAMETER (PANEL_LABEL_FONT = 1430653345)
      INTEGER PANEL_LABEL_IMAGE
      PARAMETER (PANEL_LABEL_IMAGE = 1430784385)
      INTEGER PANEL_LABEL_STRING
      PARAMETER (PANEL_LABEL_STRING = 1430915425)
      INTEGER PANEL_LABEL_WIDTH
      PARAMETER (PANEL_LABEL_WIDTH = 1431046145)
      INTEGER PANEL_LABEL_X
      PARAMETER (PANEL_LABEL_X = 1431177281)
      INTEGER PANEL_LABEL_Y
      PARAMETER (PANEL_LABEL_Y = 1431308417)
      INTEGER PANEL_NEXT_COL
      PARAMETER (PANEL_NEXT_COL = 1435568129)
      INTEGER PANEL_NEXT_ITEM
      PARAMETER (PANEL_NEXT_ITEM = 1435634177)
      INTEGER PANEL_NEXT_ROW
      PARAMETER (PANEL_NEXT_ROW = 1435699201)
      INTEGER PANEL_NOTIFY_PROC
      PARAMETER (PANEL_NOTIFY_PROC = 1436158561)
      INTEGER PANEL_NOTIFY_STATUS
      PARAMETER (PANEL_NOTIFY_STATUS = 1436289025)
      INTEGER PANEL_OPS_VECTOR
      PARAMETER (PANEL_OPS_VECTOR = 1436355073)
      INTEGER PANEL_VALUE_X
      PARAMETER (PANEL_VALUE_X = 1438517313)
      INTEGER PANEL_VALUE_Y
      PARAMETER (PANEL_VALUE_Y = 1438648449)
      INTEGER PANEL_POST_EVENTS
      PARAMETER (PANEL_POST_EVENTS = 1439893761)
C   Panel_choice_item attributes  
      INTEGER PANEL_CHOICES_BOLD
      PARAMETER (PANEL_CHOICES_BOLD = 1426721025)
      INTEGER PANEL_CHOICE_COLOR
      PARAMETER (PANEL_CHOICE_COLOR = 1426786306)
      INTEGER PANEL_CHOICE_FONT
      PARAMETER (PANEL_CHOICE_FONT = 1426849890)
      INTEGER PANEL_CHOICE_FONTS
      PARAMETER (PANEL_CHOICE_FONTS = 1427016097)
      INTEGER PANEL_CHOICE_IMAGE
      PARAMETER (PANEL_CHOICE_IMAGE = 1427111970)
      INTEGER PANEL_CHOICE_IMAGES
      PARAMETER (PANEL_CHOICE_IMAGES = 1427278209)
      INTEGER PANEL_CHOICE_NCOLS
      PARAMETER (PANEL_CHOICE_NCOLS = 1427310593)
      INTEGER PANEL_CHOICE_NROWS
      PARAMETER (PANEL_CHOICE_NROWS = 1427441665)
      INTEGER PANEL_CHOICE_RECT
      PARAMETER (PANEL_CHOICE_RECT = 1430849537)
      INTEGER PANEL_CHOICE_STRING
      PARAMETER (PANEL_CHOICE_STRING = 1427374082)
      INTEGER PANEL_CHOICE_STRINGS
      PARAMETER (PANEL_CHOICE_STRINGS = 1427540321)
      INTEGER PANEL_CHOICE_X
      PARAMETER (PANEL_CHOICE_X = 1427769442)
      INTEGER PANEL_CHOICE_XS
      PARAMETER (PANEL_CHOICE_XS = 1427933249)
      INTEGER PANEL_CHOICE_Y
      PARAMETER (PANEL_CHOICE_Y = 1428031650)
      INTEGER PANEL_CHOICE_YS
      PARAMETER (PANEL_CHOICE_YS = 1428195457)
      INTEGER PANEL_DEFAULT_VALUE
      PARAMETER (PANEL_DEFAULT_VALUE = 1428687361)
      INTEGER PANEL_DISPLAY_LEVEL
      PARAMETER (PANEL_DISPLAY_LEVEL = 1428818209)
      INTEGER PANEL_FEEDBACK
      PARAMETER (PANEL_FEEDBACK = 1429342497)
      INTEGER PANEL_MARK_IMAGE
      PARAMETER (PANEL_MARK_IMAGE = 1434189858)
      INTEGER PANEL_MARK_IMAGES
      PARAMETER (PANEL_MARK_IMAGES = 1434356097)
      INTEGER PANEL_MARK_X
      PARAMETER (PANEL_MARK_X = 1434454114)
      INTEGER PANEL_MARK_XS
      PARAMETER (PANEL_MARK_XS = 1434617921)
      INTEGER PANEL_MARK_Y
      PARAMETER (PANEL_MARK_Y = 1434716322)
      INTEGER PANEL_MARK_YS
      PARAMETER (PANEL_MARK_YS = 1434880129)
      INTEGER PANEL_NCHOICES
      PARAMETER (PANEL_NCHOICES = 1435437057)
      INTEGER PANEL_NOMARK_IMAGE
      PARAMETER (PANEL_NOMARK_IMAGE = 1435762722)
      INTEGER PANEL_NOMARK_IMAGES
      PARAMETER (PANEL_NOMARK_IMAGES = 1435928961)
      INTEGER PANEL_TOGGLE_VALUE
      PARAMETER (PANEL_TOGGLE_VALUE = 1437728834)
C   Panel_choice_item, Panel_gauge_item, Panel_multiline_text_item,   
C   Panel_numeric_text_item, Panel_slider_item and Panel_text_item    
C   attributes.                                                       
      INTEGER PANEL_VALUE
      PARAMETER (PANEL_VALUE = 1437861889)
C   Panel_choice_item and Panel_list_item attributes  
      INTEGER PANEL_CHOOSE_NONE
      PARAMETER (PANEL_CHOOSE_NONE = 1428228353)
      INTEGER PANEL_CHOOSE_ONE
      PARAMETER (PANEL_CHOOSE_ONE = 1428293889)
C   Panel_drop_target_item attributes  
      INTEGER PANEL_DROP_BUSY_GLYPH
      PARAMETER (PANEL_DROP_BUSY_GLYPH = 1426262529)
      INTEGER PANEL_DROP_DND
      PARAMETER (PANEL_DROP_DND = 1426393601)
      INTEGER PANEL_DROP_FULL
      PARAMETER (PANEL_DROP_FULL = 1426917633)
      INTEGER PANEL_DROP_GLYPH
      PARAMETER (PANEL_DROP_GLYPH = 1427048961)
      INTEGER PANEL_DROP_HEIGHT
      PARAMETER (PANEL_DROP_HEIGHT = 1427179521)
      INTEGER PANEL_DROP_SEL_REQ
      PARAMETER (PANEL_DROP_SEL_REQ = 1427704321)
      INTEGER PANEL_DROP_SITE_DEFAULT
      PARAMETER (PANEL_DROP_SITE_DEFAULT = 1431505153)
      INTEGER PANEL_DROP_WIDTH
      PARAMETER (PANEL_DROP_WIDTH = 1427834881)
      INTEGER PANEL_DROP_DELETE
      PARAMETER (PANEL_DROP_DELETE = 1428490497)
      INTEGER PANEL_DROP_DND_TYPE
      PARAMETER (PANEL_DROP_DND_TYPE = 1431635969)
C   Panel_gauge_item attributes  
      INTEGER PANEL_GAUGE_WIDTH
      PARAMETER (PANEL_GAUGE_WIDTH = 1429538817)
C   Panel_gauge_item, Panel_numeric_text_item and    
C   Panel_slider_item attributes                     
      INTEGER PANEL_MAX_VALUE
      PARAMETER (PANEL_MAX_VALUE = 1435109377)
      INTEGER PANEL_MIN_VALUE
      PARAMETER (PANEL_MIN_VALUE = 1435502593)
C   Panel_gauge_item and Panel_slider_item attributes  
      INTEGER PANEL_DIRECTION
      PARAMETER (PANEL_DIRECTION = 1428752673)
      INTEGER PANEL_MAX_TICK_STRING
      PARAMETER (PANEL_MAX_TICK_STRING = 1435044193)
      INTEGER PANEL_MIN_TICK_STRING
      PARAMETER (PANEL_MIN_TICK_STRING = 1435240801)
      INTEGER PANEL_SHOW_RANGE
      PARAMETER (PANEL_SHOW_RANGE = 1437337857)
      INTEGER PANEL_TICKS
      PARAMETER (PANEL_TICKS = 1437665281)
C   Panel_list_item attributes  
      INTEGER PANEL_LIST_CLIENT_DATA
      PARAMETER (PANEL_LIST_CLIENT_DATA = 1431830658)
      INTEGER PANEL_LIST_CLIENT_DATAS
      PARAMETER (PANEL_LIST_CLIENT_DATAS = 1431996929)
      INTEGER PANEL_LIST_DELETE
      PARAMETER (PANEL_LIST_DELETE = 1432094721)
      INTEGER PANEL_LIST_DELETE_ROWS
      PARAMETER (PANEL_LIST_DELETE_ROWS = 1432160258)
      INTEGER PANEL_LIST_DELETE_SELECTED_ROWS
      PARAMETER (PANEL_LIST_DELETE_SELECTED_ROWS = 1432422944)
      INTEGER PANEL_LIST_DISPLAY_ROWS
      PARAMETER (PANEL_LIST_DISPLAY_ROWS = 1432225793)
      INTEGER PANEL_LIST_FIRST_SELECTED
      PARAMETER (PANEL_LIST_FIRST_SELECTED = 1432291872)
      INTEGER PANEL_LIST_FONT
      PARAMETER (PANEL_LIST_FONT = 1432354914)
      INTEGER PANEL_LIST_FONTS
      PARAMETER (PANEL_LIST_FONTS = 1432521121)
      INTEGER PANEL_LIST_GLYPH
      PARAMETER (PANEL_LIST_GLYPH = 1432616994)
      INTEGER PANEL_LIST_GLYPHS
      PARAMETER (PANEL_LIST_GLYPHS = 1432783233)
      INTEGER PANEL_LIST_INSERT
      PARAMETER (PANEL_LIST_INSERT = 1433012225)
      INTEGER PANEL_LIST_INSERT_DUPLICATE
      PARAMETER (PANEL_LIST_INSERT_DUPLICATE = 1432946945)
      INTEGER PANEL_LIST_INSERT_GLYPHS
      PARAMETER (PANEL_LIST_INSERT_GLYPHS = 1433078274)
      INTEGER PANEL_LIST_INSERT_STRINGS
      PARAMETER (PANEL_LIST_INSERT_STRINGS = 1433209346)
      INTEGER PANEL_LIST_MODE
      PARAMETER (PANEL_LIST_MODE = 1433864481)
      INTEGER PANEL_LIST_NEXT_SELECTED
      PARAMETER (PANEL_LIST_NEXT_SELECTED = 1427965953)
      INTEGER PANEL_LIST_NROWS
      PARAMETER (PANEL_LIST_NROWS = 1433143297)
      INTEGER PANEL_LIST_ROW_HEIGHT
      PARAMETER (PANEL_LIST_ROW_HEIGHT = 1433274369)
      INTEGER PANEL_LIST_SCROLLBAR
      PARAMETER (PANEL_LIST_SCROLLBAR = 1433340417)
      INTEGER PANEL_LIST_SELECT
      PARAMETER (PANEL_LIST_SELECT = 1433405442)
      INTEGER PANEL_LIST_SELECTED
      PARAMETER (PANEL_LIST_SELECTED = 1433470977)
      INTEGER PANEL_LIST_SORT
      PARAMETER (PANEL_LIST_SORT = 1433733409)
      INTEGER PANEL_LIST_STRING
      PARAMETER (PANEL_LIST_STRING = 1433534466)
      INTEGER PANEL_LIST_STRINGS
      PARAMETER (PANEL_LIST_STRINGS = 1433700705)
      INTEGER PANEL_LIST_TITLE
      PARAMETER (PANEL_LIST_TITLE = 1428097377)
      INTEGER PANEL_LIST_WIDTH
      PARAMETER (PANEL_LIST_WIDTH = 1434060801)
      INTEGER PANEL_LIST_INACTIVE
      PARAMETER (PANEL_LIST_INACTIVE = 1439631362)
      INTEGER PANEL_LIST_DELETE_INACTIVE_ROWS
      PARAMETER (PANEL_LIST_DELETE_INACTIVE_ROWS = 1439697440)
      INTEGER PANEL_LIST_DO_DBL_CLICK
      PARAMETER (PANEL_LIST_DO_DBL_CLICK = 1439762689)
      INTEGER PANEL_LIST_ROW_VALUES
      PARAMETER (PANEL_LIST_ROW_VALUES = 1440090627)
      INTEGER PANEL_LIST_EXTENSION_DATA
      PARAMETER (PANEL_LIST_EXTENSION_DATA = 1439957122)
      INTEGER PANEL_LIST_EXTENSION_DATAS
      PARAMETER (PANEL_LIST_EXTENSION_DATAS = 1440057857)
      INTEGER PANEL_LIST_MASK_GLYPH
      PARAMETER (PANEL_LIST_MASK_GLYPH = 1440219170)
      INTEGER PANEL_LIST_MASK_GLYPHS
      PARAMETER (PANEL_LIST_MASK_GLYPHS = 1440319873)
C   Panel_list_item, Panel_multiline_text_item,       
C   Panel_numeric_text_item, Panel_slider_item and    
C   Panel_text_item attributes                        
      INTEGER PANEL_READ_ONLY
      PARAMETER (PANEL_READ_ONLY = 1436682497)
C   Panel_multiline_text_item attributes  
      INTEGER PANEL_LINE_BREAK_ACTION
      PARAMETER (PANEL_LINE_BREAK_ACTION = 1431570721)
C   Panel_multiline_text_item, Panel_numeric_text_item,       
C   Panel_slider_item and Panel_text_item attributes          
      INTEGER PANEL_NOTIFY_LEVEL
      PARAMETER (PANEL_NOTIFY_LEVEL = 1436027169)
      INTEGER PANEL_VALUE_DISPLAY_LENGTH
      PARAMETER (PANEL_VALUE_DISPLAY_LENGTH = 1437992961)
C   Panel_multiline_text_item, Panel_numeric_text_item and    
C   Panel_text_item attributes                                
      INTEGER PANEL_NOTIFY_STRING
      PARAMETER (PANEL_NOTIFY_STRING = 1436420449)
      INTEGER PANEL_VALUE_DISPLAY_WIDTH
      PARAMETER (PANEL_VALUE_DISPLAY_WIDTH = 1438058497)
      INTEGER PANEL_VALUE_STORED_LENGTH
      PARAMETER (PANEL_VALUE_STORED_LENGTH = 1438255105)
C   Panel_numeric_text_item and Panel_slider_item attributes  
      INTEGER PANEL_JUMP_DELTA
      PARAMETER (PANEL_JUMP_DELTA = 1430456321)
      INTEGER PANEL_VALUE_FONT
      PARAMETER (PANEL_VALUE_FONT = 1438124449)
C   Panel_numeric_text_item and Panel_text_item attributes  
      INTEGER PANEL_MASK_CHAR
      PARAMETER (PANEL_MASK_CHAR = 1434978625)
      INTEGER PANEL_VALUE_UNDERLINED
      PARAMETER (PANEL_VALUE_UNDERLINED = 1438386433)
C   Panel_slider_item attributes  
      INTEGER PANEL_MAX_VALUE_STRING
      PARAMETER (PANEL_MAX_VALUE_STRING = 1435175265)
      INTEGER PANEL_MIN_VALUE_STRING
      PARAMETER (PANEL_MIN_VALUE_STRING = 1435371873)
      INTEGER PANEL_SHOW_VALUE
      PARAMETER (PANEL_SHOW_VALUE = 1437468929)
      INTEGER PANEL_SLIDER_END_BOXES
      PARAMETER (PANEL_SLIDER_END_BOXES = 1437534465)
      INTEGER PANEL_SLIDER_WIDTH
      PARAMETER (PANEL_SLIDER_WIDTH = 1437599809)
C   Panel_text_item attributes  
      INTEGER PANEL_TEXT_SELECT_LINE
      PARAMETER (PANEL_TEXT_SELECT_LINE = 1439828512)
#define Panel_attr Attr_attribute
      INTEGER PANEL_PARENT_PANEL
      PARAMETER (PANEL_PARENT_PANEL = XV_OWNER)
      INTEGER PANEL_SHOW_ITEM
      PARAMETER (PANEL_SHOW_ITEM = XV_SHOW)
      INTEGER PANEL_DISPLAY_ROWS
      PARAMETER (PANEL_DISPLAY_ROWS = PANEL_LIST_DISPLAY_ROWS)
      INTEGER PANEL_LIST_OP_DESELECT,PANEL_LIST_OP_SELECT
     &,PANEL_LIST_OP_VALIDATE,PANEL_LIST_OP_DELETE
     &,PANEL_LIST_OP_DBL_CLICK
      PARAMETER ( PANEL_LIST_OP_DESELECT=0,  PANEL_LIST_OP_SELECT=1, 
     & PANEL_LIST_OP_VALIDATE=2,  PANEL_LIST_OP_DELETE=3, 
     & PANEL_LIST_OP_DBL_CLICK=4)
#define Panel_list_op INTEGER*4
      INTEGER PANEL_LIST_READ,PANEL_LIST_EDIT
      PARAMETER ( PANEL_LIST_READ=0,  PANEL_LIST_EDIT=1)
#define Panel_list_mode INTEGER*4
#define Panel_paint_window_ptr INTEGER*4
      STRUCTURE /Panel_paint_window/
          Xv_Window pw
          Xv_Window view
          Panel_paint_window_ptr next
          END STRUCTURE
C 
C  * Values for LEVEL attributes 
C  
      INTEGER PANEL_CLEAR,PANEL_NO_CLEAR,PANEL_NONE,PANEL_ALL
     &,PANEL_NON_PRINTABLE,PANEL_SPECIFIED,PANEL_CURRENT,PANEL_DONE
     &,PANEL_MARKED,PANEL_VERTICAL,PANEL_HORIZONTAL,PANEL_INVERTED
     &,PANEL_INSERT,PANEL_NEXT,PANEL_PREVIOUS,PANEL_NONE_DOWN
     &,PANEL_LEFT_DOWN,PANEL_MIDDLE_DOWN,PANEL_RIGHT_DOWN
     &,PANEL_CHORD_DOWN,PANEL_FORWARD,PANEL_REVERSE,PANEL_WRAP_AT_CHAR
     &,PANEL_WRAP_AT_WORD
      PARAMETER ( PANEL_CLEAR=0,  PANEL_NO_CLEAR=1,  PANEL_NONE=2, 
     & PANEL_ALL=3,  PANEL_NON_PRINTABLE=4,  PANEL_SPECIFIED=5, 
     & PANEL_CURRENT=6,  PANEL_DONE=7,  PANEL_MARKED=8, 
     & PANEL_VERTICAL=9,  PANEL_HORIZONTAL=10,  PANEL_INVERTED=11, 
     & PANEL_INSERT=12,  PANEL_NEXT=13,  PANEL_PREVIOUS=14, 
     & PANEL_NONE_DOWN=15,  PANEL_LEFT_DOWN=16,  PANEL_MIDDLE_DOWN=17, 
     & PANEL_RIGHT_DOWN=18,  PANEL_CHORD_DOWN=19,  PANEL_FORWARD=20, 
     & PANEL_REVERSE=21,  PANEL_WRAP_AT_CHAR=22,  PANEL_WRAP_AT_WORD=23)
#define Panel_setting INTEGER*4
C 
C 
C  * Values for PANEL_DROP_DND_TYPE
C  
      INTEGER PANEL_DROP_USERDEF,PANEL_DROP_COPY_ONLY
     &,PANEL_DROP_MOVE_ONLY
      PARAMETER ( PANEL_DROP_USERDEF=0,  PANEL_DROP_COPY_ONLY=1, 
     & PANEL_DROP_MOVE_ONLY=2)
#define Panel_drop_dnd_type INTEGER*4
C 
C  * Used by PANEL_LIST_ROW_VALUES attribute
C  
      STRUCTURE /Panel_list_row_values/
          Char_ptr string
          Server_image glyph
          Server_image mask_glyph
          Xv_font font
          Xv_opaque client_data
          Xv_opaque extension_data
          INTEGER  inactive
          Xv_opaque reserved
          END STRUCTURE
#define Panel_list_row_values_ptr INTEGER*4
C 
C 
C  * Types of items
C  
      INTEGER PANEL_MESSAGE_ITEM,PANEL_BUTTON_ITEM,PANEL_CHOICE_ITEM
     &,PANEL_TOGGLE_ITEM,PANEL_TEXT_ITEM,PANEL_NUMERIC_TEXT_ITEM
     &,PANEL_SLIDER_ITEM,PANEL_LIST_ITEM,PANEL_GAUGE_ITEM
     &,PANEL_ABBREV_MENU_BUTTON_ITEM,PANEL_EXTENSION_ITEM
     &,PANEL_MULTILINE_TEXT_ITEM,PANEL_DROP_TARGET_ITEM
      PARAMETER ( PANEL_MESSAGE_ITEM=0,  PANEL_BUTTON_ITEM=1, 
     & PANEL_CHOICE_ITEM=2,  PANEL_TOGGLE_ITEM=3,  PANEL_TEXT_ITEM=4, 
     & PANEL_NUMERIC_TEXT_ITEM=5,  PANEL_SLIDER_ITEM=6, 
     & PANEL_LIST_ITEM=7,  PANEL_GAUGE_ITEM=8, 
     & PANEL_ABBREV_MENU_BUTTON_ITEM=9,  PANEL_EXTENSION_ITEM=10, 
     & PANEL_MULTILINE_TEXT_ITEM=11,  PANEL_DROP_TARGET_ITEM=12)
#define Panel_item_type INTEGER*4
      STRUCTURE /Xv_panel_view/
          RECORD /Xv_canvas_view/ parent_data
          Xv_opaque private_data
          END STRUCTURE
C 
C  * Structures 
C  
      STRUCTURE /Xv_panel_choice/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_panel_gauge/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_gauge_ptr INTEGER*4
      STRUCTURE /Xv_panel_slider/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_slider_ptr INTEGER*4
      STRUCTURE /Xv_panel_text/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_text_ptr INTEGER*4
      STRUCTURE /Xv_panel_num_text/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_num_text_ptr INTEGER*4
      STRUCTURE /Xv_panel_multiline_text/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_multiline_text_ptr INTEGER*4
      STRUCTURE /Xv_panel_ambtn/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_ambtn_ptr INTEGER*4
      STRUCTURE /Xv_panel_button/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_button_ptr INTEGER*4
      STRUCTURE /Xv_panel_drop/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_drop_ptr INTEGER*4
      STRUCTURE /Xv_panel_list/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_list_ptr INTEGER*4
      STRUCTURE /Xv_panel_extension_item/
          Xv_item parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_panel_extension_item_ptr INTEGER*4
C 
C  * For SunView 1 compatibility 
C  
#define Panel_attribute Panel_attr
C 
C  ***********************************************************************
C  *		Globals
C  ***********************************************************************
C  
C 
C  * for PANEL_CYCLE choice item 
C  
C 
C  * 	Public Functions 
C  
C 
C  * Panel routines 
C  
      Panel_item  panel_advance_caret
      EXTERNAL panel_advance_caret
C       Panel panel

      Panel_item  panel_backup_caret
      EXTERNAL panel_backup_caret
C       Panel panel

      EXTERNAL panel_show_focus_win
C       Panel_item item
C       Frame frame
C       INTEGER x
C       INTEGER y

C 
C  * event mapping routines 
C  
      INTEGER  panel_handle_event
      EXTERNAL panel_handle_event
C       Panel_item item
C       Event_ptr event

      EXTERNAL panel_default_handle_event
C       Panel_item item
C       Event_ptr event

      INTEGER  panel_cancel
      EXTERNAL panel_cancel
C       Panel_item item
C       Event_ptr event

C 
C  * Panel_item action routines 
C  
      EXTERNAL panel_begin_preview
C       Panel_item panel
C       Event_ptr event

      EXTERNAL panel_update_preview
C       Panel_item panel
C       Event_ptr event

      EXTERNAL panel_accept_preview
C       Panel_item panel
C       Event_ptr event

      EXTERNAL panel_cancel_preview
C       Panel_item panel
C       Event_ptr event

      EXTERNAL panel_accept_menu
C       Panel_item panel
C       Event_ptr event

      EXTERNAL panel_accept_key
C       Panel_item panel
C       Event_ptr event

C 
C  * utilities 
C  
      Panel_setting  panel_text_notify
      EXTERNAL panel_text_notify
C       Panel_item item
C       Event_ptr eventp

      EXTERNAL panel_paint_label
C       Panel_item item

      Pixrect_ptr  panel_button_image
      EXTERNAL panel_button_image
C       Panel panel
C       Cstringp string
C       INTEGER width
C       Xv_font font

      EXTERNAL panel_default_clear_item
C       Panel_item item

C  routines to translate event coordinates
C  
      Event_ptr  panel_window_event
      EXTERNAL panel_window_event
C       Panel panel
C       Event_ptr event

      Event_ptr  panel_event
      EXTERNAL panel_event
C       Panel panel
C       Event_ptr event

      COMMON /panel_globals/   SCROLLABLE_PANEL, PANEL_VIEW, PANEL_TEXT,
     & PANEL_SLIDER, PANEL_NUMERIC_TEXT, PANEL_MULTILINE_TEXT,
     & PANEL_MESSAGE, PANEL_LIST, PANEL_ITEM, PANEL_GAUGE,
     & PANEL_DROP_TARGET, PANEL_CHOICE, PANEL_BUTTON,
     & PANEL_ABBREV_MENU_BUTTON, PANEL
#endif panel_F.

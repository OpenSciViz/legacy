#ifndef win_notify_F_DEFINED
#define win_notify_F_DEFINED

C 
C  * derived from @(#)win_notify.h 20.14 91/05/28 SMI
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * SunView related notification definitions (see also notify.h).
C  
#include "notify_F.h"
#include "win_input_F.h"
C 
C  * PUBLIC Functions 
C  
C 
C  * Posting of client events to window notifier clients 
C  
      Notify_error  win_post_id
      EXTERNAL win_post_id
C       Notify_client client
C       INTEGER id
C       Notify_event_type when

      Notify_error  win_post_id_and_arg
      EXTERNAL win_post_id_and_arg
C       Notify_client client
C       INTEGER id
C       Notify_event_type when
C       Notify_arg arg
C       Notify_copy copy_func
C       Notify_release release_func

      Notify_error  win_post_event
      EXTERNAL win_post_event
C       Notify_client client
C       Event_ptr event
C       Notify_event_type when

      Notify_error  win_post_event_arg
      EXTERNAL win_post_event_arg
C       Notify_client client
C       Event_ptr event
C       Notify_event_type when
C       Notify_arg arg
C       Notify_copy copy_func
C       Notify_release release_func

C 
C  * Utilities to call if posting with win_post_id_and_arg or win_post_event_arg
C  
      Notify_arg  win_copy_event
      EXTERNAL win_copy_event
C       Notify_client client
C       Notify_arg arg
C       Event_ptr event_ptr

      EXTERNAL win_free_event
C       Notify_client client
C       Notify_arg arg
C       Event_ptr event

#endif win_notify_F.

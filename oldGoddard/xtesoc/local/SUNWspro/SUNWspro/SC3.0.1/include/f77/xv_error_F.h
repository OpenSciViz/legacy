#ifndef xv_error_F_DEFINED
#define xv_error_F_DEFINED

C 
C 
C  * derived from @(#)xv_error.h 1.24 91/05/28 SMI
C 
C  *
C 
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents 
C  *	pending in the U.S. and foreign countries. See LEGAL NOTICE 
C  *	file for terms of the license.
C 
C  
#include "attr_F.h"
#include "attr_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C 
C  * PRIVATE enumerations
C  
      INTEGER ERROR_RECOVERABLE,ERROR_NON_RECOVERABLE
      PARAMETER ( ERROR_RECOVERABLE=0,  ERROR_NON_RECOVERABLE=1)
#define Error_severity INTEGER*4
      INTEGER ERROR_SYSTEM,ERROR_SERVER,ERROR_TOOLKIT,ERROR_PROGRAM
      PARAMETER ( ERROR_SYSTEM=0,  ERROR_SERVER=1,  ERROR_TOOLKIT=2, 
     & ERROR_PROGRAM=3)
#define Error_layer INTEGER*4
      INTEGER ERROR_BAD_ATTR
      PARAMETER (ERROR_BAD_ATTR = 1275136513)
      INTEGER ERROR_BAD_VALUE
      PARAMETER (ERROR_BAD_VALUE = 1275267586)
      INTEGER ERROR_CANNOT_GET
      PARAMETER (ERROR_CANNOT_GET = 1275464193)
      INTEGER ERROR_CANNOT_SET
      PARAMETER (ERROR_CANNOT_SET = 1275660801)
      INTEGER ERROR_CREATE_ONLY
      PARAMETER (ERROR_CREATE_ONLY = 1275857409)
      INTEGER ERROR_INVALID_OBJECT
      PARAMETER (ERROR_INVALID_OBJECT = 1276053857)
      INTEGER ERROR_LAYER
      PARAMETER (ERROR_LAYER = 1276250401)
      INTEGER ERROR_PKG
      PARAMETER (ERROR_PKG = 1276447233)
      INTEGER ERROR_SERVER_ERROR
      PARAMETER (ERROR_SERVER_ERROR = 1276578305)
      INTEGER ERROR_SEVERITY
      PARAMETER (ERROR_SEVERITY = 1276643617)
      INTEGER ERROR_STRING
      PARAMETER (ERROR_STRING = 1276840289)
#define Error_attr Attr_attribute
      INTEGER  xv_error
      EXTERNAL xv_error
C       Xv_object object

      Char_ptr  xv_error_format
      EXTERNAL xv_error_format
C       Xv_object object
C       Attr_avlist avlist

      INTEGER  xv_error_default
      EXTERNAL xv_error_default
C       Xv_object object
C       Attr_avlist avlist

#endif xv_error_F.

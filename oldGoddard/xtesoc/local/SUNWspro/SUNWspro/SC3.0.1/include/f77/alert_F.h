#ifndef alert_F_DEFINED
#define alert_F_DEFINED

C     derived from  @(#)alert.h 20.14 91/05/28
C  * 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C  SunView 1.x "alerts" are now called "notices" in SunXView.x.  Below 
C  * is for backwards compatibility only.  All usages of the alert package
C  *  should be discontinued as further releases may not support this
C  *  interface.  Not all "alert" attributes apply in SunXView.x, so
C  *  full compatiblity  does not exist.
C  * Use the notice package definitions and attributes instead.
C  
#include "notice_F.h"
#include "window_F.h"
#include "win_input_F.h"
C 
C  ***********************************************************************
C  *			Attributes
C  ***********************************************************************
C  
#define Alert_attribute Notice_attribute
      INTEGER ALERT_NO_BEEPING
      PARAMETER (ALERT_NO_BEEPING = NOTICE_NO_BEEPING)
      INTEGER ALERT_MESSAGE_STRINGS
      PARAMETER (ALERT_MESSAGE_STRINGS = NOTICE_MESSAGE_STRINGS)
      INTEGER ALERT_MESSAGE_STRINGS_ARRAY_PTR
      PARAMETER (ALERT_MESSAGE_STRINGS_ARRAY_PTR = 
     &  NOTICE_MESSAGE_STRINGS_ARRAY_PTR)
      INTEGER ALERT_MESSAGE_FONT
      PARAMETER (ALERT_MESSAGE_FONT = NOTICE_FONT)
      INTEGER ALERT_BUTTON_YES
      PARAMETER (ALERT_BUTTON_YES = NOTICE_BUTTON_YES)
      INTEGER ALERT_BUTTON_NO
      PARAMETER (ALERT_BUTTON_NO = NOTICE_BUTTON_NO)
      INTEGER ALERT_BUTTON
      PARAMETER (ALERT_BUTTON = NOTICE_BUTTON)
      INTEGER ALERT_TRIGGER
      PARAMETER (ALERT_TRIGGER = NOTICE_TRIGGER)
C 
C  * Useful constants 
C  
      INTEGER ALERT_YES
      PARAMETER (ALERT_YES = 1)
      INTEGER ALERT_NO
      PARAMETER (ALERT_NO = 0)
      INTEGER ALERT_FAILED
      PARAMETER (ALERT_FAILED = -1)
      INTEGER ALERT_TRIGGERED
      PARAMETER (ALERT_TRIGGERED = -2)
      INTEGER  alert_prompt
      EXTERNAL alert_prompt
C       Xv_window client_frame
C       Event_ptr event

#endif alert_F.

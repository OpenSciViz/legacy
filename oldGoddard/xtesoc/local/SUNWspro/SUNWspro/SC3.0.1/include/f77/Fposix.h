
/*
 *  This file contains predefinitions of external functions
 *  for POSIX.9 binding library.  
 *  In POSIX.1 those functions are macro.
 */

C*****    3.2.1.1

      external pxfwifexited
      logical  pxfwifexited

      external ipxfwexitstatus
      integer  ipxfwexitstatus

      external pxfwifsignaled
      logical  pxfwifsignaled

      external ipxfwtermsig
      integer  ipxfwtermsig

      external pxfwifstopped
      logical  pxfwifstopped

      external ipxfwstopsig
      integer  ipxfwstopsig


C*****    5.6.1.1

      external pxfisdir
      logical  pxfisdir

      external pxfischr
      logical  pxfischr

      external pxfisblk
      logical  pxfisblk

      external pxfisreg
      logical  pxfisreg

      external pxfisfifo
      logical  pxfisfifo


C*****    8.1.1.1

      external ipxfconst
      integer  ipxfconst

      logical  pxfisconst
      external pxfisconst
	
C....!...1.........2.........3.........4.........5.........6.........7..

#ifndef selection_F_DEFINED
#define selection_F_DEFINED

C   derived from @(#)selection.h 20.16 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "base_F.h"
C 
C  * PUBLIC definitions
C  
C 
C  * sel_type values
C  
      INTEGER SELTYPE_NULL
      PARAMETER (SELTYPE_NULL = 0)
      INTEGER SELTYPE_CHAR
      PARAMETER (SELTYPE_CHAR = 1)
C 
C  * sel_itembytes values
C  
      INTEGER SEL_UNKNOWNITEMS
      PARAMETER (SEL_UNKNOWNITEMS = -1)
C  Don't know how many items 
C 
C  * sel_pubflags values
C  
      INTEGER SEL_PRIMARY
      PARAMETER (SEL_PRIMARY = X'1')
C  Primary selection 
      INTEGER SEL_SECONDARY
      PARAMETER (SEL_SECONDARY = X'2')
C  Secondary selection 
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
      STRUCTURE /Selection/
          INTEGER  sel_type, sel_items, sel_itembytes, sel_pubflags
          caddr_t sel_privdata
          END STRUCTURE
#define Selection_ptr INTEGER*4
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * Public variables 
C  
C 
C  * Public Functions 
C  
C 
C  * Create the selection
C  
      EXTERNAL selection_set
C       RECORD /Selection/ sel
C       ProcPointer sel_write
C       ProcPointer sel_clear
C       Xv_opaque window

C 
C  * Fetch the selection
C  
      EXTERNAL selection_get
C       ProcPointer sel_read
C       Xv_opaque window

C 
C  * Clear the selection
C  
      EXTERNAL selection_clear
C       Xv_opaque window

#endif selection_F.

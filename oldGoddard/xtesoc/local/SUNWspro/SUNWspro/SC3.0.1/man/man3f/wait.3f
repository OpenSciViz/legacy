.\" @(#)wait.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH WAIT 3F "12 June 1990"
.SH NAME
wait \- wait for a process to terminate
.SH SYNOPSIS
.\"
.IX wait "" \fLwait\fP
.IX process  "wait for terminatation"
.IX "terminate - wait for process to terminate"
.\"
.B integer function wait (status)
.br
.B integer status
.SH DESCRIPTION
.I Wait
causes its caller to be suspended until a signal is received or one of
its child processes terminates.
If any child has terminated since the last
.I wait,
return is immediate; if there are no children, return is immediate
with an error code.
.PP
If the returned value is positive, it is the process ID of the child and
.I status
is its termination status (see
.IR wait (2)).
If the returned value is negative, it is the negation of a system error code.
.\"
.SH FILES
.\"
libF77.a
.SH "SEE ALSO"
wait(2), signal(3F), kill(3F), perror(3F)

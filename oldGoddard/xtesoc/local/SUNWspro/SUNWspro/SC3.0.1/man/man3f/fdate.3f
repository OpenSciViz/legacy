.\" @(#)fdate.3f 1.4 86/01/02 SMI; from UCB 4.2
.TH FDATE 3F "28 October 1991"
.SH NAME
fdate \- return date and time in an ASCII string
.SH SYNOPSIS
.\"
.IX fdate "" \fLfdate\fP
.IX "date and time - fdate" "" "date and time - \fLfdate\fP"
.IX "time and date - fdate" "" "time and date - \fLfdate\fP"
.\"
.B subroutine fdate (string)
.br
.B character*24 string
.sp 1
.B character*24 function fdate()
.\"
.SH DESCRIPTION
.\"
.B fdate
returns the current date and time as a 24 character string
in the format described under
.BR ctime (3).
.LP
.B fdate
can be called either as a function or as a subroutine.
If called as a function, the calling routine must define
its type and length. For example
.RS
.nf

character*24   fdate
write(*,*) fdate()

.fi
.RE
.SH FILES
libF77.a
.SH "SEE ALSO"
ctime(3), time(3F), idate(3F)

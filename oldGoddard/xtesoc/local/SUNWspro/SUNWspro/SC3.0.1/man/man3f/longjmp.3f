.TH LONGJMP 3F "19 April 1994"
.SH NAME
longjmp, isetjmp \- longjmp returns to the location set by isetjmp
.SH SYNOPSIS
.PP
Usage:
.B isetjmp
.sp
.nf
.ft 3
integer  env(12)
common /jmpblk/ env
j = isetjmp( env )
.ft 1
.fi
.sp
Usage:
.B longjmp
.sp
.nf
.ft 3
integer  env(12)
common /jmpblk/ env
call longjmp(env,ival)
.ft 1
.fi
.\"
.SH DESCRIPTION
.\"
.PP
The
.B isetjmp
and
.B longjmp
routines are used to deal with errors and 
interrupts encountered in a low-level
routine of a program.
These routines should be used only as a last resort.
They require discipline.
They are not portable.
Read the man page
.B setjmp (3V)
for bugs and other details.
.sp
.B isetjmp
saves the stack environment in
.BR env .
It also saves the register environment.
.sp
.B longjmp
restores the environment saved by the last
call to
.B isetjmp
and returns in such a way that execution continues
as if the call to
.B isetjmp
had just returned the value
.BR ival .
.sp
The integer expression
.B ival
returned from
.B isetjmp
is zero if
.B longjmp
is not called, and it is nonzero if
.B longjmp
is called.
.sp
Example: Code fragment using
.B isetjmp
and
.BR longjmp .
.sp
.nf
.ft 3
	integer  env(12)
	common /jmpblk/ env
	j = isetjmp( env )                   !\f2      <-- isetjmp\f3
	if ( j .eq. 0 ) then
		call  sbrtnA
	else
		call error_processor
	end if
	end
	subroutine sbrtnA
	integer  env(12)
	common /jmpblk/ env
	call longjmp( env, ival )                 !\f2      <-- longjmp\f3
	return
	end
.ft 1
.fi
.\"
.SH "NOTE"
.\"
You must invoke
.B isetjmp
before calling
.BR longjmp() .
.sp
The argument to 
.B isetjmp
must be a 12 integer array.
.sp
You must pass the
.B env 
variable from the routine that calls
.B isetjmp
to the routine that calls
.BR longjmp ,
either by common or as an argument.
.sp
.B longjmp()
attempts to clean up the stack.
.br
.B longjmp()
must be called from a lower call-level than
.BR isetjmp() .
.sp
Passing
.B isetjmp
as an argument that is a procedure name does not work.
.\"
.SH BUGS
.\"
See
.BR setjmp (3V).
.\"
.SH FILES
.\"
.BR libF77.a
.\"
.SH SEE ALSO
.\"
.BR setjmp (3C) ,
Solaris 2.x
.br
.BR setjmp (3V) ,
Solaris 1.x
.br
For the C version of
.BR longjmp ,
use:
.B "man\ -M\ /usr/man\ longjmp"
.br
For the C version of
.BR isetjmp ,
use:
.B "man\ -M\ /usr/man\ isetjmp"

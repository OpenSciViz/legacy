'\" t
.\" @(#)libm_single.3f 1.4 88/03/19 SMI;
.\"
.\"    For screen: tbl libm_single.3f | nroff -man | more
.\"    For paper:  tbl libm_single.3f | nroff -man | lp
.\"
.TH LIBM_SINGLE 3F "3 May 1994"
.SH NAME
libm_single - FORTRAN access to single-precision libm functions and subroutines
.SH SYNOPSIS
.sp
.IR "Example for non-intrinsics" :
.sp
.nf
.na
REAL c, s, x, y, z
 \.\.\.
z = r_acosh( x )
i = ir_finite( x )
z = r_hypot( x, y )
z = r_infinity()
CALL r_sincos( x, s, c )
.fi
.ad
.sp
.I Note:
The REAL functions
used are not in a REAL statement.
Type is determined by the default
typing rules for the letter "r".
.\"
.SH DESCRIPTION
.\"
These subprograms provide access to single-precision
.B libm
functions and subroutines.
.sp
.BR asind(x) ,
.BR sind(x) ,
etc. involve
.I degrees
rather than
.IR radians .
.\"
.SH INTRINSICS
.\"
.PP
The following FORTRAN intrinsic functions return single-precision
values if their arguments are single-precision.
.sp
You need not put them in a type statement. If the function needed is 
available as an intrinsic function, it is simpler to use an intrinsic
than a non-intrinsic function.
.sp
The variables
.B x
and
.B y
are of type real.
.sp
.TS
tab(;) ; 
lfL lfL lfL .
sqrt(x);asin(x);acosd(x)*
log(x);acos(x);asind(x)*
log10(x);atan(x);acosd(x)*
exp(x);atan2(x,y);atand(x)*
x**y;sinh(x);atan2d(x,y)* 
sin(x);cosh(x)*;aint(x) 
cos(x);tanh(x)*;anint(x)
tan(x);sind(x)*;nint(x)
.TE
.\"
.sp
* = nonstandard: it is an extension that this is intrinsic
.\"
.bp
.\"
.SH "NON-INTRINSIC FUNCTIONS AND SUBROUTINES"
.\"
In general, these functions do 
.I not 
correspond to standard FORTRAN
generic intrinsic functions,
so their data types are determined by the usual FORTRAN
data typing rules.
.sp
The variables
.BR c ,
.BR l ,
.BR p ,
.BR s ,
.BR u ,
.BR x ,
and
.B y
are of type real.
.sp
.\"
.TS
tab(;) ;
lfLB lfL lfR .
r_acos( x ); real; function;
r_acosd( x ); real; function;
r_acosh( x ); real; function;
r_acosp( x ); real; function;
r_acospi( x ); real; function;

r_atan( x ); real; function;
r_atand( x ); real; function;
r_atanh( x ); real; function;
r_atanp( x ) ; real; function;
r_atanpi( x ); real; function;

r_asin( x ); real; function;
r_asind( x ); real; function;
r_asinh( x ) ; real; function;
r_asinp( x ); real; function;
r_asinpi( x ); real; function;

r_atan2(( y, x ); real; function;
r_atan2d( y, x ); real; function;
r_atan2pi( y, x ); real; function;

r_cbrt( x ) ; real; function;
r_ceil( x ) ; real; function;
r_copysign( x, y ); real; function;

r_cos( x ); real; function;
r_cosd( x ); real; function;
r_cosh( x ); real; function;
r_cosp( x ); real; function;
r_cospi( x ); real; function;

r_erf( x ) ; real; function;
r_erfc( x ); real; function;

r_expm1( x ) ; real; function;
r_floor( x ) ; real; function;
r_hypot( x, y ); real; function;
r_infinity( ); real; function;

r_j0( x ) ; real; function;
r_j1( x ); real; function;
r_jn( x ); real; function;

ir_finite( x ); integer; function;
ir_fp_class( x ); integer; function;
ir_ilogb( x ) ; integer; function;
ir_irint( x ) ; integer; function;
ir_isinf( x ) ; integer; function;
ir_isnan( x ) ; integer; function;
ir_isnormal( x ) ; integer; function;
ir_issubnormal( x ) ; integer; function;
ir_iszero( x ); integer; function;
ir_signbit( x  ) ; integer; function;

r_addran(); real; function;
r_addrans( x, p, l, u ); n/a; subroutine;
r_lcran(); real; function;
r_lcrans( x, p, l, u ); n/a; subroutine;
r_shufrans(x, p, l, u); n/a; subroutine;

r_lgamma( x ) ; real; function;
r_logb( x ) ; real; function;
r_log1p( x ) ; real; function;
r_log2( x ) ; real; function;

r_max_normal() ; real; function;
r_max_subnormal() ; real; function;
r_min_normal() ; real; function;
r_min_subnormal() ; real; function;
r_nextafter( x, y ) ; real; function;
r_quiet_nan( n ) ; real; function;
r_remainder( x, y ) ; real; function;
r_rint( x ) ; real; function;
r_scalb( x, y ) ; real; function;
r_scalbn( x, n ) ; real; function;
r_signaling_nan( n ) ; real; function;
r_significand( x ); real; function;

r_sin( x ); real; function;
r_sind( x ); real; function;
r_sinh( x ); real; function;
r_sinp( x ); real; function;
r_sinpi( x ); real; function;

r_sincos( x, s, c ); n/a; subroutine;
r_sincosd( x, s, c ) ; n/a; subroutine;
r_sincosp( x, s, c ); n/a; subroutine;
r_sincospi( x, s, c ); n/a; subroutine;

r_tan( x ); real; function;
r_tand( x ); real; function;
r_tanh( x ); real; function;
r_tanp( x ); real; function;
r_tanpi( x ); real; function;

r_y0( x ) ; real; function;
r_y1( x ) ; real; function;
r_yn( n,x ) ; real; function;
.TE
.\"
.SH FILES
.\"
libm.a
.\"
.SH "SEE ALSO"
.\"
intro(3M)
.br
.I "FORTRAN Reference Manual"
.br
.I "Numerical Computation Guide"

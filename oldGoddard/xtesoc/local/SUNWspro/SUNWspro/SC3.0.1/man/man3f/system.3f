.\" @(#)system.3f 1.3 89/01/27 SMI; from UCB 4.2
.TH SYSTEM 3F "28 October 1990"
.SH NAME
system \- execute operating system command
.SH SYNOPSIS
.\"
.IX system "" \fLsystem\fP
.IX "execute a SunOS command"
.IX "command, execute a SunOS command"
.IX "sunos, execute a sunos command" "" "SunOS, execute a SunOS command"
.\"
.B integer function system (string)
.br
.B character*(*) string
.SH DESCRIPTION
The function
.B system
gives
.I string
to your shell
as input, as if the string had been typed as a command.
If the environment variable
.B SHELL
is found, its value will be used as the command interpreter (shell);
otherwise
.IR sh (1)
is used.
.PP
The current process waits until the command terminates.
The returned value will be the exit status of the shell.
See
.IR wait (2)
for an explanation of this value.
.PP
Historically
.B cc
and
.B f77
developed with different assumptions:
.in +2
.br
If
.B cc
calls
.B system 
the shell is always the Bourne shell.
.br
If
.B f77
calls
.B system
which shell gets called depends on environment variable
.B SHELL .
.in -2
.PP
The
.I system
function flushes all open files.
.br
For output files, the buffer is flushed to the actual file.
.br
For input files, the position of the pointer is unpredictable.
.SH FILES
libF77.a
.SH "SEE ALSO"
execve(2), wait(2), system(3)
.SH BUGS
.I string
can not be longer than 1024 characters.
.SH SEE ALSO
.B sh (3F)

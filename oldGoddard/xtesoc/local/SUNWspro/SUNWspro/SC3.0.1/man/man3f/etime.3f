.\" @(#)etime.3f 1.6 86/01/02 SMI; from UCB 4.2
.TH ETIME 3F "12 November 1993"
.SH NAME
etime, dtime \- return elapsed time
.SH SYNOPSIS
.B real function etime (time)
.br
.B real time(2)
.sp 1
.B real function dtime (time)
.br
.B real time(2)
.\"
.SH DESCRIPTION
These functions return elapsed time.
.br
Time is in number of seconds.
The resolution is to a nanosecond in Solaris 2.x.
.br
If there is an error,
.br
o   Argument elements
.B time(1)
and
.B time(2)
are
.IR undefined .
.br
o   Function return value = -1.0
.br
If no error,
.br
o   Argument:
.I "User time" 
in 
.B time(1)
and 
.I "system time"
in
.B time(2)
.br
o   Function return value: Sum of
.B time(1)
and
.B time(2)
.sp
.B dtime
returns the elapsed time since the last call to
.BR dtime .
.br
For
.BR dtime ,
elapsed time is:
.br
o   On the first call,
elapsed time since the start of execution
.br
o   On second and subsequent calls,
elapsed time since
last call to
.B dtime
.br
o   For 
.IR "single processor" : 
Time used by the CPU
.br
o   For 
.IR "multiple processors" :
Sum of times for all the CPUs
(not useful, use 
.BR etime )
.br
.in +1
.IR \ Note: 
.IR Note :
Do not call
.B dtime
from within a parallelized loop.
.in -1
.sp
.B etime
returns the elapsed time since the start of execution.
.br
For
.BR etime ,
elapsed time is:
.br
o   For 
.IR "single processor" :
CPU time for the calling process
.br
o   For 
.IR "multiple processors" :
Wall-clock time while running your program
.br
.in +1
.IR \ Note: 
.B time(1)
contains the wall clock time and
.B time(2)
is 0.0
.in -1
.sp
.B "How f77 Decides Single/Multiple User"
.sp
.in +1
For a FORTRAN MP program
(uses an MP option: ultimately, linked with
.BR libF77_mt ),
if the environment variable 
.B PARALLEL
is:
.in +1
.br
.IR Undefined ,
then the current run is
.IR "Single Processor" .
.br
.IR Defined ,
and in the range 1, 2, 3, ...,
then the current run is
.IR "Multiple Processor" .
.br
.IR Defined ,
but some value other than 1, 2, 3, ...,
then the results are unpredictable.
.in -2
.\"
.SH FILES
.\"
.BR libF77.a ,
.BR libF77_mt.a ,
.BR libF77.so ,
.BR libF77_mt.so
.\"
.SH "SEE ALSO"
.\"
.BR times(2) ,
.BR f77(1) ,
.I "FORTRAN User's Guide"

.\" @(#)mvbits.3f 1.5 92/01/28 SMI; from UCB 4.2
.TH MVBITS 3F "5 January 1994"
.SH NAME
mvbits \- move specified bits
.SH SYNOPSIS
.\"
.IX mvbits "" \fLmvbits\fP
.IX move bits
.\"
.nf
.ft B
subroutine mvbits (src, ini1, nbits, des, ini2 )
integer src, ini1, nbits, des, ini2
.fi
.ft 1
.\"
.SH DESCRIPTION
.\"
.LP
This routine moves bits from
.B src
starting with bit
.BR ini1 ,
number of bits moved
.BR nbits , 
to
.BR des ,
starting with bit
.BR ini2 .
.\"
.SH NOTES
.\"
.PP
To use this VMS routine, you need the
.B \-lV77
option.
If you use
.BR \-lV77 ,
then invoking
.BR idate()
or
.B time()
gets the VMS version.
.sp
Usage:
.in +2
.br
.nf
.ft B
INTEGER  src, ini1, nbits, des, ini2
	...
CALL mvbits(src, ini1, nbits, des, ini2)
.sp
.in -2
.ft 1
.fi
Restrictions:
.in +2
.br
.B
ini1
+
.B
nbits
\(<=
32
.br
.B
ini2
+
.B
nbits
\(<=
32
.sp
.in -2
Example:
.br
.ft B
.nf
.in +2
demo$ cat  mvb1.f
* mvb1.f -- From src, initial bit 0, move 3 bits to   des, initial bit 3. 
*    src       des 
* 543210 543210 <-- Bit numbers (VMS convention) 
* 000111 000001 <-- Values before move 
* 000111 111001 <-- Values after move 
	INTEGER src, ini1, nbits, des, ini2 
	DATA src, ini1, nbits, des, ini2 
& 	         / 7,    0,      3,     1,     3 / 
	CALL mvbits ( src, ini1, nbits, des, ini2 ) 
	WRITE (*,"(5O3)") src, ini1, nbits, des, ini2 
	END 
demo$ f77\  \-silent \ mvb1.f \ \-lV77 
demo$ a.out 
\ \ 7\ \ 0 3 71\ \ 3 
demo$
.ft R
.fi
.in -2
.\"
.SH FILES
.\"
.BR libF77.a , 
.B libV77.a
.\"
.SH "SEE ALSO"
.\"
The
.IR "FORTRAN Reference Manual" .

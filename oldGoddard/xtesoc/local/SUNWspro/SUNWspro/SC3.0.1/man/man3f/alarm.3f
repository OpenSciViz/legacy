.\" @(#)alarm.3f 1.4 86/01/02 SMI; from UCB 4.2
.TH ALARM 3F "12 June 1991"
.SH NAME
alarm \- execute a subroutine after a specified time
.SH SYNOPSIS
.\"
.IX alarm "" \fLalarm\fP
.IX "delay execution"
.\"
.B integer function alarm (time, sbrtn)
.br
.B integer time
.br
.B external sbrtn
.SH USAGE
.B integer time /8/
.br
.B external sbrtn
.br
.B nseconds =  alarm(time,sbrtn)
.SH DESCRIPTION
This routine arranges for subroutine
.I sbrtn
to be called after
.I time
seconds. If
.I time
is 0, the alarm is turned off and no routine will be called.
The returned value will be the time remaining on the last alarm.
.SH FILES
libF77.a
.SH "SEE ALSO"
alarm(3C), sleep(3F), signal(3F)
.SH BUGS
.LP
A subroutine cannot pass its own name to
.I alarm
because of restrictions in the standard.

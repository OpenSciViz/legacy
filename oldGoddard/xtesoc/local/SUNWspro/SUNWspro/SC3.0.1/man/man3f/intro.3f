.\" @(#)intro.3f 1.1 93/11/10 SMI;
.TH INTRO 3F "17 September 1993"
.SH NAME
intro \- introduction to FORTRAN library functions and subroutines.
.SH DESCRIPTION
.PP
This section describes those functions and subroutines that are in 
the FORTRAN runtime library.  The functions and subroutines  listed 
here provide an interface from 
.B f77
programs to the system in the same manner as the C library does for C
programs.  
(You can also access the C library directly if you use pragmas.)
These functions and subroutines are automatically loaded as needed 
by the FORTRAN compiler
.B f77 (1).
.\"
.PP
Most of these routines are in 
.B libF77.a \|. 
Some of these routines are in 
.B libV77.a \|. 
The descriptions are tagged as VMS.
.sp
A few intrinsic functions are described for the sake of completeness.
.PP
For efficiency, the SCCS ID strings are not normally included in the
.B a.out
file. To include them, simply declare
.nf

	EXTERNAL f77lid

.fi
in any
.B f77
module.
.SH "LIST OF FUNCTIONS"
.sp 2
.nf
.ta \w'system'u+2n +\w'access.3f'u+10n
\fIName\fP	\fIAppears on Page\fP	\fIDescription\fP
.ta \w'system'u+4n +\w'access.3f'u+4n

abort	 abort(3F)	 terminate abruptly; write memory image to core file
access	 access(3F)	 return access mode (r,w,x) or existence of a file
alarm	 alarm(3F)	 execute a subroutine after a specified time
bit	 bit(3F)	 and, or, xor, not, rshift, lshift, bic, bis, bit, setbit functions
chdir	 chdir(3F)	 change default directory
chmod	 chmod(3F)	 change mode of a file
ctime	 time(3F)	 return system time
date	 date(3F)	 return date in character form
drand	 rand(3F)	 random values (See Numerical Computation Guide: lcrans, ... )
dtime	 etime(3F)	 return elapsed execution time
etime	 etime(3F)	 return elapsed execution time
exit	 exit(3F)	 terminate process with status
f77_floatingpoint f77_floatingpoint(3F)
	 	 	IEEE floating-point definitions
f77_ieee_environment f77_ieee_environment(3F)
	 	 	IEEE floating-point mode, status, and signals
fdate	 fdate(3F)	 return date and time in an ASCII string
fgetc	 getc(3F)	 get a character from a logical unit
flush	 flush(3F)	 flush output to a logical unit
fork	 fork(3F)	 create a copy of this process
fputc	 putc(3F)	 write a character to a FORTRAN logical unit
free 	 free(3F)	 memory deallocator
fseek	 fseek(3F)	 reposition a file on a logical unit
fstat	 stat(3F)	 get file status
ftell	 fseek(3F)	 reposition a file on a logical unit
gerror	 perror(3F)	 get system error messages
getarg	 getarg(3F)	 get the k\fIth\fP command-line argument
getc	 getc(3F)	 get a character from a logical unit
getcwd	 getcwd(3F)	 get pathname of current working directory
getenv	 getenv(3F)	 get value of environment variables
getfd	 getfd(3F)	 get file descriptor of external unit number
getfilep	 getfilep(3F)	 get file pointer of external unit number
getgid	 getuid(3F)	 get user or group ID of the caller
getlog	 getlog(3F)	 get user's login name
getpid	 getpid(3F)	 get process id
getuid	 getuid(3F)	 get user or group ID of the caller
gmtime	 time(3F)	 return system time
hostnm	 hostnm(3F)	 get name of current host
iargc	 getarg(3F)	 return the number of arguments after command name
idate	 idate(3F)	 return date in numerical form
ierrno	 perror(3F)	 get system error messages
index	 index(3F)	 return index of first occurrence of string a2 in a1
inmax	 inmax(3F)	 return the maximum positive integer
ioinit	 ioinit(3F)	 initialize IO carriage control, blanks, append, names
irand	 rand(3F)	 random values (See Numerical Computation Guide: lcrans, ... )
isatty	 ttynam(3F)	 returns true if unit is a terminal device
isetjmp	 longjmp(3F)	 save the stack and register envirnoment for longjmp
itime	 itime(3F)	 return time in numerical form
kill	 kill(3F)	 send a signal to a process
len	 index(3F)	 return declared length of character string
libm_double 	 double-precision FORTRAN entries for libm (math) functions
libm_quadruple	 quadruple-precision FORTRAN entries for libm (math) functions
libm_single	 single-precision FORTRAN entries for libm (math) functions
link	 link(3F)	 make a link to an existing file
lnblnk	 index(3F)	 return index of last non-blank in character string
loc	 loc(3F)	 return the address of an object
long	 long(3F)	 integer object conversion
longjmp	 long(3F)	 return to the location set by isetjmp
lstat	 stat(3F)	 get file status
ltime	 time(3F)	 return system time
malloc	 malloc(3F)	 memory allocator
mvbits	 mvbits(3F)	 move bits, VMS
perror	 perror(3F)	 get system error messages
putc	 putc(3F)	 write a character to a FORTRAN logical unit
qsort	 qsort(3F)	 quick sort
ran	 ran(3F)	 random values, VMS
rand	 rand(3F)	 random values (See Numerical Computation Guide: lcrans, ... )
rename	 rename(3F)	 rename a file
rindex	 index(3F)	 return index of last occurrence of string a2 in a1
secnds	 secnds(3F)	 return system time, VMS
sh	 sh(3F)	 	 fast execute of sh command
short	 long(3F)	 integer object conversion
signal	 signal(3F)	 change the action for a signal
sleep	 sleep(3F)	 suspend execution for an interval
stat	 stat(3F)	 get file status
symlnk	 link(3F)	 make a link to an existing file
system	 system(3F)	 execute operating system command
tclose	 topen(3F)	 f77 tape close
time	 time(3F)	 return system time
topen	 topen(3F)	 f77 tape open
tread	 topen(3F)	 f77 tape read
trewin	 topen(3F)	 f77 tape rewind
tskipf	 topen(3F)	 f77 tape skip files or records, reset eof & eot status
tstate	 topen(3F)	 logical state of f77 tape I/O channel
ttynam	 ttynam(3F)	 find name of a terminal port
twrite	 topen(3F)	 f77 tape write
unlink	 unlink(3F)	 remove a file
wait	 wait(3F)	 wait for a process to terminate
.fi

.\" @(#)link.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH LINK 3F "12 June 1991"
.SH NAME
link, symlnk \- make a link to an existing file
.SH SYNOPSIS
.\"
.IX link "" \fLlink\fR
.IX symlnk "" \fLsymlnk\fR
.IX link "to an existing file"
.\"
.B function link ( name1, name2 )
.br
.B character*(*) name1, name2
.sp 1
.B integer function symlnk(name1,name2)
.br
.B character*(*) name1, name2
.SH DESCRIPTION
The argument
.I name1
must be the pathname of an existing file.
.I name2
is a pathname to be linked to file
.IR name1 .
.I name2
must not already exist.
The returned value will be 0 if successful; 
a system error code otherwise.
.PP
.I Symlnk
creates a symbolic link to
.IR name1 .
.SH FILES
libF77.a
.SH "SEE ALSO"
link(2), symlink(2), perror(3F), unlink(3F)
.SH BUGS
Pathnames can be no longer than MAXPATHLEN as defined in <sys/param.h>.

.\" @(#)getc.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH GETC 3F "12 June 1990"
.SH NAME
getc, fgetc \- get a character from a logical unit
.SH SYNOPSIS
.\"
.IX getc "" \fLgetc\fP
.IX fgetc "" \fLfgetc\fP
.IX get "a character getc, fgetc" "" "a character: \&\fLgetc\fR, \&\fLfgetc\fR"
.IX read "a character getc, fgetc" "" "a character: \&\fLgetc\fR, \&\fLfgetc\fR"
.IX character "get a character getc, fgetc" "" "get a character: \&\fLgetc\fR, \&\fLfgetc\fP"
.\"
.B integer function getc (char)
.br
.B character char
.sp 1
.B integer function fgetc (lunit, char)
.br
.B character char
.\"
.SH DESCRIPTION
These functions return the next character from a file associated with a
FORTRAN logical unit, bypassing normal FORTRAN I/O.
.PP
.B getc
reads from logical unit 5, normally connected to the control terminal input.
.PP
The value of each function is a system status code. Zero indicates no
error occured on the read; \-1 indicates end of file was detected.
A positive value will be either a SunOS system error code or an f77 I/O error
code. See 
.BR perror (3F).
.\"
.SH FILES
libF77.a
.SH "SEE ALSO"
getc(3S), intro(2), perror(3F)

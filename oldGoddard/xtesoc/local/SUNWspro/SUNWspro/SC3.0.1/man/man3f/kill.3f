.\" @(#)kill.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH KILL 3F "12 June 1990"
.SH NAME
kill \- send a signal to a process
.SH SYNOPSIS
.\"
.IX kill "" \fLkill\fP
.IX "send signal to process, kill" "" "send signal to process, \fLkill\fP"
.IX "signal a process, kill" "" "signal a process, \fLkill\fP"
.IX process "send signal to, kill" "" "send signal to, \fLkill\fP"
.\"
.B function kill (pid, signum)
.br
.B integer pid, signum
.SH DESCRIPTION
The argument
.B pid
must be the process id of one of the user's processes.
.sp
The argument
.B signum
must be a valid signal number (see signal(3)).
.sp
The returned value will be 0 if successful; an error code otherwise.
.PP
Note that this function just sends a message;
it does not necessarily kill the process.
Some users have been known to consider this a UNIX misnomer, 
but what do they know?
If you really mean to kill a process, you do:

.B "kill( pid, SIGKILL )"

.SH FILES
libF77.a
.SH "SEE ALSO"
kill(2), signal(3), signal(3F), fork(3F), perror(3F)

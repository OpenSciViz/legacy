.\" @(#)getarg.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH GETARG 3F "26 July 1992"
.SH NAME
getarg, iargc \- get the k\fIth\fP command line argument
.SH SYNOPSIS
.\"
.IX getarg "" \fLgetarg\fP
.IX iargc "" \fLiargc\fP
.IX "command line argument"
.IX arguments "of command line"
.\"
.B subroutine getarg ( k, arg )
.br
.B character*(*) arg
.sp 1
.B function iargc ()
.\"
.SH DESCRIPTION
.\"
The statement
.B "call getarg( k , arg )"
will get the \fBk\fP\fIth\fR command-line argument
and put it into
.B arg .
.PP
The 0\fIth\fR argument is the command name.
.PP
The function
.B iargc
returns the argument count:
the index of the last command-line argument,
.br
and therefore the number of arguments after ]
the command name.
.\"
.SH EXAMPLE
\fB
.nf
demo% cat tesargs.f
	character argv*10
	integer i, iargc, m
	m = iargc()
	do i = 1, m
		call getarg ( i, argv )
 		write( *, '( i2, 1x, a )' ) i, argv
	end do
	end
demo % a.out first second last
 1 first     
 2 second    
 3 last      
demo%
\fP
.fi
.SH FILES
libF77.a
.SH "SEE ALSO"
execve(2), getenv(3F)

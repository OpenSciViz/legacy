#ifndef icon_load_F_DEFINED
#define icon_load_F_DEFINED

C   derived from @(#)icon_load.h 20.16 92/02/20 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "icon_F.h"
#include "pixrect_F.h"
#include "svrimage_F.h"
C 
C  * The format of an ASCII representation of an icon file is defined by:
C  *	<icon_file>	::= <header> <icon_image_bits>
C  *	<header>	::= <header_start> <header_body> <header_end>
C 
C  *	<header_start>	::=  Format_version=1;
C  *	<header_end>	::= 
C 
C  * 	<header_body>	::= <header_options> <header_misc> 
C  *	<header_options>::= | <header_option> <header_options> 
C  * 	<header_option> ::= Depth=1
C  *			  | Height=<Number>
C  *			  | Width=<Multiple_of_16>
C  *			  | Valid_bits_per_item=<Sixteen_or_ThirtyTwo>
C  *	<header_misc>	::= <Anything_except_header_option_or_header_end>
C  *	<icon_image_bits>
C  *			::= <icon_image_value>
C  * 			  | <icon_image_bits> , <icon_image_value>
C  *
C  * Default values for header parameters are:
C  *	Depth			 1
C  *	Height			64
C  * 	Width			64
C  *	Valid_bits_per_item	16
C  * 
C  * A sample icon file follows:
C  *
C  * Format_version=1, Width=16, Height=16, Depth=1, Valid_bits_per_item=16
C  * This file is the template for all images in the cursor/icon library.
C  * The first line contains the information needed to properly interpret the
C  *   actual bits, which are expected to be used directly by software that
C  *   wants to do compile-time binding to an image via a #include.
C  * The actual bits must be specified in hex.
C  * The default interpretation of the bits below is specified by the
C  *   behavior of mpr_static.
C  * Note that Valid_bits_per_item uses the least-significant bits.
C  * Description: An empty (clear) cursor
C  * Background: White
C  *
C  *	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
C  *	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
C  *
C  *
C  * The utilities available to read icon files are:
C  *
C  *	int
C  *	icon_load(icon, from_file, error_msg);
C  *		register Icon	icon;
C  *		char			*from_file, *error_msg;
C  * Loads the pixrect for the icon, and then copies information from the pixrect
C  * to initialize the icon.  Information which cannot be specified in the
C  * current format is set to default values.
C  * See the following description of icon_load_mpr for details about the
C  * arguments other than icon.
C  * Returns 0 iff it managed to successfully initialize the icon.
C  *
C  * 	int
C  *	  icon_init_from_pr(icon, pr)
C  *		register Icon	icon;
C  *		register struct pixrect	*pr;
C  * Initializes the icon struct from the specified pr.
C  * Information, such as the font for any icon text, which cannot be ascertained
C  * from a pixrect, is set to default values.
C  *
C  * The return value is meaningless.
C  *
C  *	struct pixrect *
C  *	icon_load_mpr(from_file, error_msg)
C  *		char		*from_file, *error_msg;
C  * Loads the icon named by from_file into a dynamically allocated memory
C  *   pixrect.
C  * If NULL is returned, which happens if the file cannot be accessed or if the
C  *   file is not in a valid format, an appropriate error message is placed
C  *   in error_msg, which should be at least IL_ERRORMSG_SIZE long.
C  *
C  *	FILE *
C  *	icon_open_header(from_file, error_msg, info)
C  *		char				*from_file, *error_msg;
C  *		register icon_header_handle	 info;
C  * icon_open_header() allows a client to preserve extra descriptive material
C  *   when rewriting an icon file.  It is also the front-end routine used by
C  *   icon_load_mpr (and thus icon_load).
C  * If NULL is returned, which happens if the file cannot be accessed or if the
C  *   file is not in a valid format, an appropriate error message is placed
C  *   in error_msg, which should be at least IL_ERRORMSG_SIZE long.
C  * info is mostly filled in with the information from the header options.  The
C  * exception is that info->last_param_pos is filled in with the position
C  * immediately after the last header option that was read.
C  * The returned FILE * is left positioned at the end of the header, thus
C  * ftell(icon_open_header()) indicates where the actual bits of the image
C  * are expected to begin.  Hence, the characters in the range
C  * [info->last_param_pos..ftell(icon_open_header())) should contain all extra
C  * descriptive material contained in the icon file's header.
C  *
C  * icon_read_pr is not a public routine
C  *	int
C  *	icon_read_pr(fd, header, pr)
C  *		register FILE			*fd;
C  * 		register icon_header_handle	 header;
C  * 		register struct pixrect		*pr;
C  * Reads the actual bit pattern for the pixrect's image data from the specified
C  *  fd using the parameters in the specified header.
C  * The fd is expected to be positioned to the beginning of the image data
C  *  (usually by a prior call to icon_open_header).
C  * The fd is left positioned at the end of the image data, so that by doing an
C  *  ftell(fd) before and after calling icon_read_pr, a caller can write out a
C  *  modified version of the image data while keeping intact the surrounding
C  *  commentary, declarations, etc.
C  * The return value is meaningless.
C  * CAVEAT: currently pr must be a memory pixrect.
C  *
C  
C 
C  ***********************************************************************
C  *			Definitions and Macros
C  ***********************************************************************
C  
      INTEGER IL_ERRORMSG_SIZE
      PARAMETER (IL_ERRORMSG_SIZE = 256)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
#define Icon_errmsg_buf(__v) CHARACTER __v ( 0 : 255 )
      STRUCTURE /Xv_icon_header_info/
          INTEGER  depth, height, format_version, valid_bits_per_item, 
     &  width
          INTEGER  last_param_pos
          END STRUCTURE
#define icon_header_handle INTEGER*4
#define icon_header_object RECORD /Xv_icon_header_info/
C 
C  ***********************************************************************
C  *			Globals
C  ***********************************************************************
C  
C 
C  * Public Functions
C  
      INTEGER  icon_load
      EXTERNAL icon_load
C       Icon icon
C       CHARACTER from_file
C       CHARACTER error_msg

      INTEGER  icon_init_from_pr
      EXTERNAL icon_init_from_pr
C       Icon icon
C       Pixrect_ptr pr

      Pixrect_ptr  icon_load_mpr
      EXTERNAL icon_load_mpr
C       CHARACTER file
C       CHARACTER error_msg

      Server_image  icon_load_svrim
      EXTERNAL icon_load_svrim
C       CHARACTER file
C       CHARACTER error_msg

      FILE_ptr  icon_open_header
      EXTERNAL icon_open_header
C       CHARACTER file
C       CHARACTER error_msg
C       icon_header_handle info

#endif icon_load_F.

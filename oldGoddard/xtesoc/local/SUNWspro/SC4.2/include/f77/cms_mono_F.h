#ifndef cms_mono_F_DEFINED
#define cms_mono_F_DEFINED

C   derived from @(#)cms_mono.h 20.10 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Definition of the colormap segment CMS_MONOCHROME,
C  * a black and white color map segment.
C  
C 
C  * PUBLIC defines 
C  
      CHARACTER*(10) CMS_MONOCHROME
      PARAMETER (CMS_MONOCHROME = "monochrome")
      INTEGER CMS_MONOCHROMESIZE
      PARAMETER (CMS_MONOCHROMESIZE = 2)
      INTEGER WHITE
      PARAMETER (WHITE = 0)
      INTEGER BLACK
      PARAMETER (BLACK = 1)
#define Cms_monochrome(__v) Byte __v ( 0 : 1 )
      EXTERNAL cms_monochromeload
C       Cms_monochrome (r)
C       Cms_monochrome (g)
C       Cms_monochrome (b)

#endif cms_mono_F.

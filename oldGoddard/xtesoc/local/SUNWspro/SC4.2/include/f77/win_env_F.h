#ifndef win_env_F_DEFINED
#define win_env_F_DEFINED

C   derived from @(#)win_env.h 20.13 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "rect_F.h"
C 
C  * PRIVATE declarations
C  
C 
C  * The window library uses the ENVIRONMENT to pass a limited amount of
C  * window information to newly created child processes.
C  * A collection of conventions dictate the use of these values.
C  
      CHARACTER*(13) WE_PARENT
      PARAMETER (WE_PARENT = "WINDOW_PARENT")
      CHARACTER*(18) WE_INITIALDATA
      PARAMETER (WE_INITIALDATA = "WINDOW_INITIALDATA")
      CHARACTER*(10) WE_GFX
      PARAMETER (WE_GFX = "WINDOW_GFX")
      CHARACTER*(9) WE_ME
      PARAMETER (WE_ME = "WINDOW_ME")
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * PUBLIC functions 
C  
C 
C  * Get/set window that can be taken over by graphics programs.
C  
      EXTERNAL we_setgfxwindow
C       CHARACTER windevname

      INTEGER  we_getgfxwindow
      EXTERNAL we_getgfxwindow
C       CHARACTER windevname

C  
C  * PUBLIC functions provided only for
C  * compatibility with pre-SunView code 
C  
C 
C  * Get/set parent of window that is being created.
C  
      EXTERNAL we_setparentwindow
C       CHARACTER windevname

      INTEGER  we_getparentwindow
      EXTERNAL we_getparentwindow
C       CHARACTER windevname

#endif win_env_F.

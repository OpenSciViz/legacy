#ifndef sel_pkg_F_DEFINED
#define sel_pkg_F_DEFINED

C   derived from @(#)sel_pkg.h 1.5 90/11/13 SMI	
C 
C  *	(c) Copyright 1990 Sun Microsystems, Inc. Sun design patents 
C  *	pending in the U.S. and foreign countries. See LEGAL NOTICE 
C  *	file for terms of the license.
C  
C FROM stddefs 	IMPORT ;
#include "attr_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "generic_F.h"
#include "Xlib_F.h"
C 
C  ***********************************************************************
C  *		Definitions and Macros
C  ***********************************************************************
C  
C 
C  * Package definitions
C  
      Xv_pkg_ptr SELECTION
      Xv_pkg_ptr SELECTION_OWNER
      Xv_pkg_ptr SELECTION_REQUESTOR
      Xv_pkg_ptr SELECTION_ITEM
C 
C  * Various utility macros 
C  
C 
C  * Public constants 
C  
C  Errors 
      INTEGER SEL_ERROR
      PARAMETER (SEL_ERROR = -1)
      INTEGER SEL_BEGIN_MULTIPLE
      PARAMETER (SEL_BEGIN_MULTIPLE = 2)
      INTEGER SEL_END_MULTIPLE
      PARAMETER (SEL_END_MULTIPLE = 4)
      INTEGER SEL_MULTIPLE
      PARAMETER (SEL_MULTIPLE = 8)
      INTEGER SEL_BAD_TIME
      PARAMETER (SEL_BAD_TIME = 0)
      INTEGER SEL_BAD_WIN_ID
      PARAMETER (SEL_BAD_WIN_ID = 1)
      INTEGER SEL_BAD_PROPERTY
      PARAMETER (SEL_BAD_PROPERTY = 2)
      INTEGER SEL_BAD_CONVERSION
      PARAMETER (SEL_BAD_CONVERSION = 3)
      INTEGER SEL_TIMEDOUT
      PARAMETER (SEL_TIMEDOUT = 4)
      INTEGER SEL_PROPERTY_DELETED
      PARAMETER (SEL_PROPERTY_DELETED = 5)
      INTEGER SEL_BAD_PROPERTY_EVENT
      PARAMETER (SEL_BAD_PROPERTY_EVENT = 6)
      INTEGER SEL_INCREMENT
      PARAMETER (SEL_INCREMENT = 2)
      INTEGER SEL_EMPTY
      PARAMETER (SEL_EMPTY = 1)
      INTEGER SEL_PROPERTY_DATA
      PARAMETER (SEL_PROPERTY_DATA = 1)
      INTEGER SEL_BUSY
      PARAMETER (SEL_BUSY = 1)
      INTEGER SEL_LOSE
      PARAMETER (SEL_LOSE = 2)
      INTEGER SEL_LOCAL_PROCESS
      PARAMETER (SEL_LOCAL_PROCESS = 4)
      INTEGER SEL_ADD_PROP_NOTIFY
      PARAMETER (SEL_ADD_PROP_NOTIFY = 8)
      INTEGER SEL_INTERNAL_ERROR
      PARAMETER (SEL_INTERNAL_ERROR = 16)
      INTEGER OLD_SEL_CLIENT
      PARAMETER (OLD_SEL_CLIENT = 2)
      INTEGER NEW_SEL_CLIENT
      PARAMETER (NEW_SEL_CLIENT = 4)
      INTEGER MAX_NUM_INCR
      PARAMETER (MAX_NUM_INCR = 20)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
C 
C  * Typedefs 
C  
#define Selection Xv_opaque
#define Selection_owner Selection
#define Selection_requestor Selection
#define Selection_item Xv_opaque
C 
C  * Enumerations 
C  
C   Public Attributes  
C   Common Selection package attributes  
      INTEGER SEL_DATA
      PARAMETER (SEL_DATA = 1644497409)
      INTEGER SEL_TYPE
      PARAMETER (SEL_TYPE = 1644825345)
      INTEGER SEL_TYPE_NAME
      PARAMETER (SEL_TYPE_NAME = 1645152609)
C   Selection object attributes  
      INTEGER SEL_RANK
      PARAMETER (SEL_RANK = 1645480705)
      INTEGER SEL_RANK_NAME
      PARAMETER (SEL_RANK_NAME = 1645807969)
      INTEGER SEL_TIME
      PARAMETER (SEL_TIME = 1646135809)
      INTEGER SEL_TIMEOUT_VALUE
      PARAMETER (SEL_TIMEOUT_VALUE = 1646462977)
C   Selection_owner object attributes  
      INTEGER SEL_CONVERT_PROC
      PARAMETER (SEL_CONVERT_PROC = 1646791265)
      INTEGER SEL_DONE_PROC
      PARAMETER (SEL_DONE_PROC = 1647118945)
      INTEGER SEL_FIRST_ITEM
      PARAMETER (SEL_FIRST_ITEM = 1647446529)
      INTEGER SEL_LOSE_PROC
      PARAMETER (SEL_LOSE_PROC = 1647774305)
      INTEGER SEL_NEXT_ITEM
      PARAMETER (SEL_NEXT_ITEM = 1648101889)
      INTEGER SEL_OWN
      PARAMETER (SEL_OWN = 1648429313)
      INTEGER SEL_PROP_INFO
      PARAMETER (SEL_PROP_INFO = 1648757249)
      INTEGER SEL_PROPERTY
      PARAMETER (SEL_PROPERTY = 1649085185)
C   Selection_requestor object attributes  
      INTEGER SEL_REPLY_PROC
      PARAMETER (SEL_REPLY_PROC = 1649412705)
      INTEGER SEL_TYPES
      PARAMETER (SEL_TYPES = 1649773313)
      INTEGER SEL_TYPE_NAMES
      PARAMETER (SEL_TYPE_NAMES = 1650100577)
      INTEGER SEL_PROP_DATA
      PARAMETER (SEL_PROP_DATA = 1650395649)
      INTEGER SEL_PROP_FORMAT
      PARAMETER (SEL_PROP_FORMAT = 1650722817)
      INTEGER SEL_PROP_LENGTH
      PARAMETER (SEL_PROP_LENGTH = 1651051265)
      INTEGER SEL_PROP_TYPE
      PARAMETER (SEL_PROP_TYPE = 1651378945)
      INTEGER SEL_PROP_TYPE_NAME
      PARAMETER (SEL_PROP_TYPE_NAME = 1651706209)
      INTEGER SEL_TYPE_INDEX
      PARAMETER (SEL_TYPE_INDEX = 1652033537)
      INTEGER SEL_APPEND_TYPES
      PARAMETER (SEL_APPEND_TYPES = 1652394753)
      INTEGER SEL_APPEND_TYPE_NAMES
      PARAMETER (SEL_APPEND_TYPE_NAMES = 1652722017)
C   Selection_item object attributes  
      INTEGER SEL_COPY
      PARAMETER (SEL_COPY = 1653016833)
      INTEGER SEL_FORMAT
      PARAMETER (SEL_FORMAT = 1653344257)
      INTEGER SEL_LENGTH
      PARAMETER (SEL_LENGTH = 1653672705)
C   Private Attributes  
#define Selection_attr Attr_attribute
C 
C  * Structures 
C  
      STRUCTURE /Xv_sel/
          RECORD /Xv_generic_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_sel_owner/
          RECORD /Xv_sel/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_sel_requestor/
          RECORD /Xv_sel/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Xv_sel_item/
          RECORD /Xv_sel_owner/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      INTEGER  MAX_SEL_BUFF_SIZE
      EXTERNAL MAX_SEL_BUFF_SIZE
C       Display_ptr dpy

      INTEGER  BYTE_SIZE
      EXTERNAL BYTE_SIZE
C       INTEGER len
C       INTEGER format

C 
C  * 	Public Functions 
C  
      Bool  sel_convert_proc
      EXTERNAL sel_convert_proc
C       Selection_owner sel_owner
C       Atom type
C       Xv_opaque data
C       INTEGER length
C       INTEGER format

      EXTERNAL sel_post_req
C       Selection_requestor sel_req

      STRUCTURE /sel_prop_info/
          Xv_opaque data
          INTEGER  format
          INTEGER  length
          Atom type
          Char_ptr typeName
          END STRUCTURE
#define sel_prop_info_ptr INTEGER*4
#define sel_compat_info_ptr INTEGER*4
      STRUCTURE /sel_compat_info/
          Window owner
          Atom selection
          INTEGER  clientType
          sel_compat_info_ptr next
          END STRUCTURE
      COMMON /sel_pkg_globals/   SELECTION_ITEM, SELECTION_REQUESTOR,
     & SELECTION_OWNER, SELECTION
#endif sel_pkg_F.

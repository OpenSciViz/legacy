#ifndef win_event_F_DEFINED
#define win_event_F_DEFINED

C 
C  * derived from  @(#)win_event.h 1.17 91/05/28
C  * (formally sundev/vuid_event.h)
C  *
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "time_F.h"
C 
C This file describes a virtual user input device (vuid) interface.  This
C is an interface between input devices and their clients.  The interface
C defines an idealized user input device that may not correspond to any
C existing physical collection of input devices.
C 
C It is targeted to input devices that gather command data from humans,
C e.g., mice, keyboards, tablets, joysticks, light pens, knobs, sliders,
C buttons, ascii terminals, etc.  The vuid interface is specifically not
C designed to support input devices that produce voluminous amounts of
C data, e.g., input scanners, disk drives, voice packets.
C 
C Here are some of the properties that are expected of a typical client
C of vuid:
C 
C 	The client has a richer user interface than can be supported by
C 	a simple ascii terminal.
C 
C 	The client serializes multiple input devices being used
C 	by the user into a single stream of events.
C 
C 	The client preserves the entire state of its input so that
C 	it may query this state.
C 
C Here are some features that viud provides to its clients:
C 
C 	A client may extend the capabilities of the predefined vuid by
C 	adding input devices.  A client wants to be able to do this in
C 	a way that fits smoothly with its existing input paradigm.
C 
C 	A client can write its code to be input device independent.  A
C 	client can replace the underlaying physical devices and not
C 	have to be concerned.  In fact, the vuid interface doesn't
C 	really care about physical devices.  One physical device can
C 	masquerade a many logical devices and many physical devices can
C 	look like a single logical device.
C 
C This file defines the protocol that makes up the virtual user input
C device.  This includes:
C 
C 	The vuid station codes and there meanings.
C 
C 	The form by which changes to vuid stations, i.e., firm events,
C 	are communicated to clients (typically via the read system
C 	call).
C 
C 	The form by which clients send commands to input devices that
C 	support the vuid (typically via an ioctl system call to send
C 	vuid instead of a native byte stream).
C 	
C Explicitly, this file does not define:
C 
C 	How to store the state of the vuid
C 	(see ../sunwindowdev/vuid_state.h).
C 	
C 	How to dynamically allocate additional vuid segments in order
C 	to extend the vuid (one could statically allocate additional
C 	vuid segments by treating this file as the central registry
C 	of vuid segments).
C 	
C 
C 
C  * VUID_SEG_SIZE is the size of a virtual user input "device" address space
C  * segment.
C  
      INTEGER VUID_SEG_SIZE
      PARAMETER (VUID_SEG_SIZE = 256)
C 
C  * This is the central registry of vuitual user input devices.
C  * To allocate a new vuid:
C  * 
C  *	o Choose an unused portion of the address space.
C  *	  Vuids from 0x00 to 0x7F are reserved for Sun implementers.
C  *	  Vuids from 0x80 to 0xFF are reserved for Sun customers.
C  *
C  *	o Note the new device with a *_DEVID define.  Breifly describe
C  *	  the purpose/usage of the device.  Point to the place where
C  *	  more information can be found.
C  *
C  *	o Note the new device with a VUID_* entry in the Vuid_device
C  *	  enumeration.
C  *
C  *	o List the specific event codes in another header file that is
C  *	  specific to the new device (ASCII_DEVID, TOP_DEVID &
C  *	  WORKSTATION_DEVID events are listing here for historical
C  *	  reasons).
C  
      INTEGER ASCII_DEVID
      PARAMETER (ASCII_DEVID = X'0')
C  Ascii codes, which include META codes (see below) 
      INTEGER TOP_DEVID
      PARAMETER (TOP_DEVID = X'1')
C  Top codes, which is ASCII with the 9th bit on (see below) 
      INTEGER ISO_DEVID
      PARAMETER (ISO_DEVID = X'2')
C  ISO characters 0x80 - 0xFF 
C  ... Sun implementers add new device ids here ... 
      INTEGER XVIEW_DEVID
      PARAMETER (XVIEW_DEVID = X'7C')
C  XView semantic actions 
      INTEGER PANEL_DEVID
      PARAMETER (PANEL_DEVID = X'7D')
C  Panel subwindow package event codes passed around internal
C 	   to the panel package (see <suntool/panel.h>) 
      INTEGER SCROLL_DEVID
      PARAMETER (SCROLL_DEVID = X'7E')
C  Scrollbar package event codes passed to scrollbar clients on
C 	   interesting scrollbar activity (see <suntool/scrollbar.h>) 
      INTEGER WORKSTATION_DEVID
      PARAMETER (WORKSTATION_DEVID = X'7F')
C  Virtual keyboard and locator (mouse) related event codes
C 	   that describe a basic "workstation" device collection (see below).
C 	   This device is a bit of a hodge podge for historical reasons;
C 	   the middle of the address space has SunWindows related events
C 	   in it (see <sunwindow/win_input.h>), and the virtual keyboard
C 	   and virtual locator are thrown together. 
C  ... Sun customers add new device ids here ... 
      INTEGER LAST_DEVID
      PARAMETER (LAST_DEVID = X'FF')
C  No more device ids beyond LAST_DEVID 
      INTEGER VUID_ASCII
      PARAMETER (VUID_ASCII = ASCII_DEVID)
      INTEGER VUID_TOP
      PARAMETER (VUID_TOP = TOP_DEVID)
      INTEGER VUID_PANEL
      PARAMETER (VUID_PANEL = PANEL_DEVID)
      INTEGER VUID_SCROLL
      PARAMETER (VUID_SCROLL = SCROLL_DEVID)
      INTEGER VUID_WORKSTATION
      PARAMETER (VUID_WORKSTATION = WORKSTATION_DEVID)
      INTEGER VUID_LAST
      PARAMETER (VUID_LAST = LAST_DEVID)
#define Vuid_device BYTE
      INTEGER  vuid_first
      EXTERNAL vuid_first
C       Vuid_device devid

      INTEGER  vuid_last
      EXTERNAL vuid_last
C       Vuid_device devid

      LOGICAL  vuid_in_range
      EXTERNAL vuid_in_range
C       Vuid_device devid
C       Vuid_device id

C 
C  * Ascii device related definitions:
C  
      INTEGER ASCII_FIRST
      PARAMETER (ASCII_FIRST = 0)
      INTEGER ISO_FIRST
      PARAMETER (ISO_FIRST = 0)
      INTEGER ASCII_LAST
      PARAMETER (ASCII_LAST = 127)
      INTEGER ISO_LAST
      PARAMETER (ISO_LAST = 255)
C 
C  * SunView1 compatibility definitions
C  
      INTEGER META_FIRST
      PARAMETER (META_FIRST = 0)
      INTEGER META_LAST
      PARAMETER (META_LAST = 255)
C 
C  * Top device related definitions:
C  
      INTEGER TOP_FIRST
      PARAMETER (TOP_FIRST = 256)
      INTEGER TOP_LAST
      PARAMETER (TOP_LAST = 511)
C 
C  * Workstation device related definitions.  First are virtual keyboard
C  * assignments.  All events for the virtual keyboard have 0 (went up) or
C  * 1 (went down) values.
C  
      INTEGER VKEY_FIRST
      PARAMETER (VKEY_FIRST = 32512)
      INTEGER VKEY_UP
      PARAMETER (VKEY_UP = 0)
      INTEGER VKEY_DOWN
      PARAMETER (VKEY_DOWN = 1)
      INTEGER VKEY_KBD_CODES
      PARAMETER (VKEY_KBD_CODES = 128)
C  The number of event codes in a subset of the
C 				   workstation device's address space
C 				   that belong to the virtual keyboard 
      INTEGER VKEY_FIRSTPSEUDO
      PARAMETER (VKEY_FIRSTPSEUDO = VKEY_FIRST)
C 
C  * VKEY_FIRSTPSEUDO thru VKEY_LASTPSEUDO are taken (for historical
C  * reasons) by SunWindows related codes (see <sunwindow/win_input.h>).
C  
      INTEGER VKEY_LASTPSEUDO
      PARAMETER (VKEY_LASTPSEUDO = VKEY_FIRSTPSEUDO+34)
      INTEGER VKEY_FIRSTSHIFT
      PARAMETER (VKEY_FIRSTSHIFT = VKEY_LASTPSEUDO+1)
      INTEGER SHIFT_CAPSLOCK
      PARAMETER (SHIFT_CAPSLOCK = VKEY_FIRSTSHIFT+0)
      INTEGER SHIFT_LOCK
      PARAMETER (SHIFT_LOCK = VKEY_FIRSTSHIFT+1)
      INTEGER SHIFT_LEFT
      PARAMETER (SHIFT_LEFT = VKEY_FIRSTSHIFT+2)
      INTEGER SHIFT_RIGHT
      PARAMETER (SHIFT_RIGHT = VKEY_FIRSTSHIFT+3)
      INTEGER SHIFT_LEFTCTRL
      PARAMETER (SHIFT_LEFTCTRL = VKEY_FIRSTSHIFT+4)
C  SHIFT_CTRL is for compatability with previous releases 
      INTEGER SHIFT_CTRL
      PARAMETER (SHIFT_CTRL = SHIFT_LEFTCTRL)
      INTEGER SHIFT_RIGHTCTRL
      PARAMETER (SHIFT_RIGHTCTRL = VKEY_FIRSTSHIFT+5)
      INTEGER SHIFT_META
      PARAMETER (SHIFT_META = VKEY_FIRSTSHIFT+6)
      INTEGER SHIFT_TOP
      PARAMETER (SHIFT_TOP = VKEY_FIRSTSHIFT+7)
      INTEGER SHIFT_CMD
      PARAMETER (SHIFT_CMD = VKEY_FIRSTSHIFT+8)
      INTEGER SHIFT_ALTG
      PARAMETER (SHIFT_ALTG = VKEY_FIRSTSHIFT+9)
      INTEGER SHIFT_ALT
      PARAMETER (SHIFT_ALT = VKEY_FIRSTSHIFT+10)
      INTEGER SHIFT_NUMLOCK
      PARAMETER (SHIFT_NUMLOCK = VKEY_FIRSTSHIFT+11)
      INTEGER SHIFT_BREAK
      PARAMETER (SHIFT_BREAK = VKEY_FIRSTSHIFT+12)
      INTEGER VKEY_LASTSHIFT
      PARAMETER (VKEY_LASTSHIFT = VKEY_FIRSTSHIFT+15)
      INTEGER VKEY_FIRSTFUNC
      PARAMETER (VKEY_FIRSTFUNC = VKEY_LASTSHIFT+1)
      INTEGER BUT_FIRST
      PARAMETER (BUT_FIRST = VKEY_FIRSTFUNC)
      INTEGER BUT_LAST
      PARAMETER (BUT_LAST = BUT_FIRST+9)
      INTEGER KEY_LEFTFIRST
      PARAMETER (KEY_LEFTFIRST = BUT_LAST+1)
      INTEGER KEY_LEFTLAST
      PARAMETER (KEY_LEFTLAST = KEY_LEFTFIRST+15)
      INTEGER KEY_RIGHTFIRST
      PARAMETER (KEY_RIGHTFIRST = KEY_LEFTLAST+1)
      INTEGER KEY_RIGHTLAST
      PARAMETER (KEY_RIGHTLAST = KEY_RIGHTFIRST+15)
      INTEGER KEY_TOPFIRST
      PARAMETER (KEY_TOPFIRST = KEY_RIGHTLAST+1)
      INTEGER KEY_TOPLAST
      PARAMETER (KEY_TOPLAST = KEY_TOPFIRST+15)
      INTEGER KEY_BOTTOMLEFT
      PARAMETER (KEY_BOTTOMLEFT = KEY_TOPLAST+1)
      INTEGER KEY_BOTTOMRIGHT
      PARAMETER (KEY_BOTTOMRIGHT = KEY_BOTTOMLEFT+1)
      INTEGER KEY_BOTTOMFIRST
      PARAMETER (KEY_BOTTOMFIRST = KEY_TOPLAST+1)
      INTEGER KEY_BOTTOMLAST
      PARAMETER (KEY_BOTTOMLAST = KEY_BOTTOMFIRST+15)
      INTEGER VKEY_LASTFUNC
      PARAMETER (VKEY_LASTFUNC = 32664)
      INTEGER VKEY_LAST
      PARAMETER (VKEY_LAST = VKEY_FIRST+VKEY_KBD_CODES-1)
      INTEGER  BUT
      EXTERNAL BUT
C       INTEGER i

      INTEGER  KEY_LEFT
      EXTERNAL KEY_LEFT
C       INTEGER i

      INTEGER  KEY_RIGHT
      EXTERNAL KEY_RIGHT
C       INTEGER i

      INTEGER  KEY_TOP
      EXTERNAL KEY_TOP
C       INTEGER i

      INTEGER  KEY_BOTTOM
      EXTERNAL KEY_BOTTOM
C       INTEGER i

C 
C  * More workstation device definitions.  These are virtual locator
C  * related event code assignments.  Values for these events are int.
C  * VLOC_BATCH's value is a u_int that describes the number of events
C  * that follow that should be treated as a batch.
C  
      INTEGER MOUSE_DEVID
      PARAMETER (MOUSE_DEVID = WORKSTATION_DEVID)
C  Backward compatibility 
      INTEGER VLOC_FIRST
      PARAMETER (VLOC_FIRST = VKEY_LAST+1)
      INTEGER LOC_FIRST_DELTA
      PARAMETER (LOC_FIRST_DELTA = VLOC_FIRST+0)
      INTEGER LOC_X_DELTA
      PARAMETER (LOC_X_DELTA = VLOC_FIRST+0)
      INTEGER LOC_Y_DELTA
      PARAMETER (LOC_Y_DELTA = VLOC_FIRST+1)
      INTEGER LOC_LAST_DELTA
      PARAMETER (LOC_LAST_DELTA = VLOC_FIRST+1)
      INTEGER LOC_FIRST_ABSOLUTE
      PARAMETER (LOC_FIRST_ABSOLUTE = VLOC_FIRST+2)
      INTEGER LOC_X_ABSOLUTE
      PARAMETER (LOC_X_ABSOLUTE = VLOC_FIRST+2)
      INTEGER LOC_Y_ABSOLUTE
      PARAMETER (LOC_Y_ABSOLUTE = VLOC_FIRST+3)
      INTEGER LOC_LAST_ABSOLUTE
      PARAMETER (LOC_LAST_ABSOLUTE = VLOC_FIRST+3)
      INTEGER VLOC_BATCH
      PARAMETER (VLOC_BATCH = VLOC_FIRST+4)
      INTEGER VLOC_LAST
      PARAMETER (VLOC_LAST = VLOC_BATCH+1)
C 
C  * Common names for certain input codes.  The buttons on the physical
C  * mouse are thought to actually belong to the virtual keyboard.
C  
      INTEGER MS_LEFT
      PARAMETER (MS_LEFT = 32563)
      INTEGER MS_MIDDLE
      PARAMETER (MS_MIDDLE = 32564)
      INTEGER MS_RIGHT
      PARAMETER (MS_RIGHT = 32565)
C 
C  * A firm_event structure is encoded in the byte stream of a device
C  * when the device has been asked to format its byte stream so.
C  * The time stamp is not defined to be meaningful except to compare
C  * with other Firm_event time stamps.
C  *
C  * The pair field is critical for a state maintainence package
C  * (such as vuid_state.h), one that is designed to not know anything
C  * about the semantics of particular events, to maintain correct data
C  * for corresponding absolute, delta and paired state variables.
C  *
C  * pair, when defined (as indicated by pair_type), is the associated
C  * state variable that should be updated due to this events generation.
C  * This is used to maintain a correspondence between an event that is a
C  * delta and a state that is an absolute value (with a known delta event
C  * defined) and visa versa, e.g., LOC_X_DELTA & LOC_X_ABSOLUTE.
C  * pair is also used to indicate another state variable that
C  * should be updated with the occurrence of this event, e.g., if id is
C  * '^G' then pair could be 'g' or 'G' depending on the state of the shift
C  * key.
C  
      INTEGER FE_PAIR_NONE
      PARAMETER (FE_PAIR_NONE = 0)
C  pair is not defined 
      INTEGER FE_PAIR_SET
      PARAMETER (FE_PAIR_SET = 1)
C  pair is accompanying id to set to
C 				   this events value 
      INTEGER FE_PAIR_DELTA
      PARAMETER (FE_PAIR_DELTA = 2)
C  pair is accompanying id that should
C 				   be set to the delta of id's current
C 				   value and the new value indicated by
C 				   this event 
      INTEGER FE_PAIR_ABSOLUTE
      PARAMETER (FE_PAIR_ABSOLUTE = 3)
C  pair is accompanying id that should
C 				   be set to the sum of its current
C 				   value and the delta indicated by
C 				   this event's value 
      STRUCTURE /Firm_event/
          INTEGER*2  id
          Byte pair_type
          Byte pair
          INTEGER  value
          RECORD /Timeval/ time
          END STRUCTURE
      INTEGER FIRM_EVENT_NULL
      PARAMETER (FIRM_EVENT_NULL = 0)
#endif win_event_F.

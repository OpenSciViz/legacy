#ifndef base_F_DEFINED
#define base_F_DEFINED

C       Derived from @(#)base.h 20.31 92/03/23 SMI      
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
      INTEGER XV_OK
      PARAMETER (XV_OK = 0)
      INTEGER XV_ERROR
      PARAMETER (XV_ERROR = 1)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structs
C  ***********************************************************************
C  
#define Xv_opaque INTEGER
#define Xv_object Xv_opaque
      INTEGER XV_NULL
      PARAMETER (XV_NULL = 0)
#endif base_F.

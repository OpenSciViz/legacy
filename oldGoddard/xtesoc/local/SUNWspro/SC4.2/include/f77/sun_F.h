#ifndef sun_F_DEFINED
#define sun_F_DEFINED

C   derived from @(#)sun.h 20.19 91/05/28 SMI 
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C  The definition of the const TRUE, FALSE, True, False
C    removed to the stddefs_F.h file.
C 
C *%FORTRAN       LOGICAL TRUE, FALSE *
C *%FORTRAN       PARAMETER (TRUE=.TRUE., FALSE=.FALSE.) *
C 
C CONST
C  *	True = 1;
C  *	False = 0;
C  
#define Bool INTEGER
      LOGICAL  strequal
      EXTERNAL strequal
C       Char_ptr s1
C       Char_ptr s2

      Char_ptr  STRDUP
      EXTERNAL STRDUP
C       Char_ptr s

C 
C  * Get some storage and copy a string.
C  
#endif sun_F.

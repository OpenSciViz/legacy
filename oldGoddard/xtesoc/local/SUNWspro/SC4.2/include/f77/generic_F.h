#ifndef generic_F_DEFINED
#define generic_F_DEFINED

C       Derived from @(#)generic.h 20.43 93/01/15 SMI
C 
C  *
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *	pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *	file for terms of the license.
C  
C 
C  * Generic attributes fall into two classes:
C  *	(1) Truly generic, implemented by attr_xxx.o or generic.o, use the
C  * package ATTR_PKG_GENERIC, shared with attr.h.
C  *	(2) Common but not truly generic, implemented by .o's spread
C  * across many sub-systems, use the package ATTR_PKG_SV, shared with xview.h.
C  * Many of these common attributes pertain to server properties and thus only
C  * apply to objects with a window server component.
C  *
C  * Implementation dependent notes on generic X attributes:
C  *	XV_XNAME has the format
C  * "<host name>:<display number in decimal>:<xid in decimal>".
C  *	XV_DEVICE_NUMBER is the XID of the underlying X object.  XV_XID is
C  * provided when a piece of code wants to emphasize that the "X id" is what
C  * is needed, rather than an abstract "tree link".
C  * 	Most of these attributes are only supported on Drawable objects,
C  * but some, like XV_XID, are supported by all objects that have direct
C  * underlying X components, e.g. Fonts.
C  
#include "attr_F.h"
#include "pkg_public_F.h"
#include "base_F.h"
      Xv_pkg_ptr XV_GENERIC_OBJECT
C  XV_COPY is "magic" package xv_create checks for to distinguish
C  * creation of a new object from creation of a copy of an existing object.
C  
C Xv_pkg_ptr
      INTEGER XV_COPY
      PARAMETER (XV_COPY = 1)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
      INTEGER XV_FOCUS_SECONDARY
      PARAMETER (XV_FOCUS_SECONDARY = 0)
      INTEGER XV_FOCUS_PRIMARY
      PARAMETER (XV_FOCUS_PRIMARY = 1)
#define Xv_focus_rank INTEGER
C 
C  * WARNING: GENERIC_ATTR shared with attr.h (which claims [0..64)) 
C  
C 
C 	 * PUBLIC and Generic 
C 	 
C  For "contexts": key & data (& optional destroy for data) 
      INTEGER XV_KEY_DATA
      PARAMETER (XV_KEY_DATA = 1077938178)
      INTEGER XV_KEY_DATA_COPY_PROC
      PARAMETER (XV_KEY_DATA_COPY_PROC = 1078004226)
      INTEGER XV_KEY_DATA_REMOVE
      PARAMETER (XV_KEY_DATA_REMOVE = 1078069249)
      INTEGER XV_KEY_DATA_REMOVE_PROC
      PARAMETER (XV_KEY_DATA_REMOVE_PROC = 1078135298)
C   For reference count on shared objects, e.g. fonts & menus  
      INTEGER XV_REF_COUNT
      PARAMETER (XV_REF_COUNT = 1078200321)
C   Type of object  
      INTEGER XV_TYPE
      PARAMETER (XV_TYPE = 1078266369)
      INTEGER XV_IS_SUBTYPE_OF
      PARAMETER (XV_IS_SUBTYPE_OF = 1078331905)
C   Miscellaneous  
      INTEGER XV_LABEL
      PARAMETER (XV_LABEL = 1078397281)
      INTEGER XV_NAME
      PARAMETER (XV_NAME = 1078462817)
      INTEGER XV_STATUS
      PARAMETER (XV_STATUS = 1078528001)
      INTEGER XV_STATUS_PTR
      PARAMETER (XV_STATUS_PTR = 1078594049)
      INTEGER XV_HELP
      PARAMETER (XV_HELP = 1078987105)
      INTEGER XV_HELP_STRING_FILENAME
      PARAMETER (XV_HELP_STRING_FILENAME = 1079118177)
      INTEGER XV_SHOW
      PARAMETER (XV_SHOW = 1079052545)
C   Required by package implementations, used only by xv_create  
      INTEGER XV_COPY_OF
      PARAMETER (XV_COPY_OF = 1078659585)
      INTEGER XV_END_CREATE
      PARAMETER (XV_END_CREATE = 1078725152)
C   To simplify SunView1.X compatibility  
      INTEGER XV_SELF
      PARAMETER (XV_SELF = 1078790657)
C   Managing (usually containing) object  
      INTEGER XV_OWNER
      PARAMETER (XV_OWNER = 1078856193)
C   Required by package implementations, used only by xv_find  
      INTEGER XV_AUTO_CREATE
      PARAMETER (XV_AUTO_CREATE = 1078921217)
C   PUBLIC but only Common  
C   For layout  
      INTEGER XV_FONT
      PARAMETER (XV_FONT = 1245710849)
      INTEGER XV_MARGIN
      PARAMETER (XV_MARGIN = 1245775873)
      INTEGER XV_LEFT_MARGIN
      PARAMETER (XV_LEFT_MARGIN = 1245841409)
      INTEGER XV_TOP_MARGIN
      PARAMETER (XV_TOP_MARGIN = 1245906945)
      INTEGER XV_RIGHT_MARGIN
      PARAMETER (XV_RIGHT_MARGIN = 1245972481)
      INTEGER XV_BOTTOM_MARGIN
      PARAMETER (XV_BOTTOM_MARGIN = 1246038017)
C   Origin is usually parent's most upper-left coord inside margins  
      INTEGER XV_X
      PARAMETER (XV_X = 1246103617)
      INTEGER XV_Y
      PARAMETER (XV_Y = 1246169217)
      INTEGER XV_WIDTH
      PARAMETER (XV_WIDTH = 1246234689)
      INTEGER XV_HEIGHT
      PARAMETER (XV_HEIGHT = 1246300289)
      INTEGER XV_RECT
      PARAMETER (XV_RECT = 1246366177)
C   Server specific or dependent  
      INTEGER XV_XNAME
      PARAMETER (XV_XNAME = 1247807841)
      INTEGER XV_DEVICE_NUMBER
      PARAMETER (XV_DEVICE_NUMBER = 1247873793)
      INTEGER XV_ROOT
      PARAMETER (XV_ROOT = 1247939073)
      INTEGER XV_VISUAL
      PARAMETER (XV_VISUAL = 1249708545)
      INTEGER XV_VISUAL_CLASS
      PARAMETER (XV_VISUAL_CLASS = 1249183745)
      INTEGER XV_DEPTH
      PARAMETER (XV_DEPTH = 1249773569)
      INTEGER XV_DISPLAY
      PARAMETER (XV_DISPLAY = 1248725505)
      INTEGER XV_SCREEN
      PARAMETER (XV_SCREEN = 1249118721)
      INTEGER XV_APP_NAME
      PARAMETER (XV_APP_NAME = 1248856417)
C   Mouseless Model support  
      INTEGER XV_FOCUS_ELEMENT
      PARAMETER (XV_FOCUS_ELEMENT = 1249249281)
      INTEGER XV_FOCUS_RANK_KEY
      PARAMETER (XV_FOCUS_RANK_KEY = 1249315105)
C   Added to support the Xrm resource database  
      INTEGER XV_USE_DB
      PARAMETER (XV_USE_DB = 1249397313)
      INTEGER XV_INSTANCE_NAME
      PARAMETER (XV_INSTANCE_NAME = 1249708385)
      INTEGER XV_INSTANCE_QLIST
      PARAMETER (XV_INSTANCE_QLIST = 1250036225)
      INTEGER XV_QUARK
      PARAMETER (XV_QUARK = 1250363905)
      INTEGER XV_USE_INSTANCE_RESOURCES
      PARAMETER (XV_USE_INSTANCE_RESOURCES = 1250691585)
C   Added to support locale announcement  
      INTEGER XV_LC_BASIC_LOCALE
      PARAMETER (XV_LC_BASIC_LOCALE = 1251674465)
      INTEGER XV_LC_DISPLAY_LANG
      PARAMETER (XV_LC_DISPLAY_LANG = 1252002145)
      INTEGER XV_LC_INPUT_LANG
      PARAMETER (XV_LC_INPUT_LANG = 1252329825)
      INTEGER XV_LC_NUMERIC
      PARAMETER (XV_LC_NUMERIC = 1252657505)
      INTEGER XV_LC_TIME_FORMAT
      PARAMETER (XV_LC_TIME_FORMAT = 1252985185)
      INTEGER XV_LOCALE_DIR
      PARAMETER (XV_LOCALE_DIR = 1253312865)
      INTEGER XV_USE_LOCALE
      PARAMETER (XV_USE_LOCALE = 1253640449)
C   PRIVATE now, but ...   
      INTEGER XV_GC
      PARAMETER (XV_GC = 1248922113)
#define Xv_generic_attr Attr_attribute
      INTEGER XV_XID
      PARAMETER (XV_XID = XV_DEVICE_NUMBER)
      INTEGER XV_DUMMY_WINDOW
      PARAMETER (XV_DUMMY_WINDOW = X'77777777')
      INTEGER XV_RC_SPECIAL
      PARAMETER (XV_RC_SPECIAL = X'20000')
C 
C  * Generic package definition	
C  
      STRUCTURE /Xv_generic_struct/
          RECORD /Xv_base/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      INTEGER XV_INIT_ARGS
      PARAMETER (XV_INIT_ARGS = 1241778178)
      INTEGER XV_INIT_ARGC_PTR_ARGV
      PARAMETER (XV_INIT_ARGC_PTR_ARGV = 1241974786)
      INTEGER XV_USAGE_PROC
      PARAMETER (XV_USAGE_PROC = 1242106465)
      INTEGER XV_ERROR_PROC
      PARAMETER (XV_ERROR_PROC = 1242303073)
      INTEGER XV_X_ERROR_PROC
      PARAMETER (XV_X_ERROR_PROC = 1242499681)
#define Xv_attr Attr_attribute
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * PUBLIC functions 
C  
      EXTERNAL xv_init

      Xv_opaque  xv_init_f
      EXTERNAL xv_init_f

      EXTERNAL xv_init_l
C       Attr_avlist attrs

      Xv_opaque  xv_init_f_l
      EXTERNAL xv_init_f_l
C       Attr_avlist attrs

      Attr_attribute  xv_unique_key
      EXTERNAL xv_unique_key
      COMMON /generic_globals/   XV_GENERIC_OBJECT
#endif generic_F.

#ifndef Xlib_F_DEFINED
#define Xlib_F_DEFINED

C  
C 
C  * Copyright 1985, 1986, 1987, 1991 by the Massachusetts Institute of Technology
C 
C  *
C  * Permission to use, copy, modify, and distribute this software and its
C  * documentation for any purpose and without fee is hereby granted, provided 
C  * that the above copyright notice appear in all copies and that both that 
C  * copyright notice and this permission notice appear in supporting 
C  * documentation, and that the name of M.I.T. not be used in advertising
C  * or publicity pertaining to distribution of the software without specific, 
C  * written prior permission. M.I.T. makes no representations about the 
C  * suitability of this software for any purpose.  It is provided "as is"
C  * without express or implied warranty.
C  *
C 
C  * X Window System is a Trademark of MIT.
C 
C  *
C  
C 
C  *	Xlib.h - Header definition and support file for the C subroutine
C  *	interface library (Xlib) to the X Window System Protocol (V11).
C  *	Structures and symbols starting with "_" are private to the library.
C  
#include "X_F.h"
#define XPointer INTEGER*4
#define Bool INTEGER
#define Bool_ptr INTEGER*4
#define Status INTEGER
C 	True = 1;
C 	False = 0; 
      INTEGER QueuedAlready
      PARAMETER (QueuedAlready = 0)
      INTEGER QueuedAfterReading
      PARAMETER (QueuedAfterReading = 1)
      INTEGER QueuedAfterFlush
      PARAMETER (QueuedAfterFlush = 2)
      INTEGER AllPlanes
      PARAMETER (AllPlanes = X'FFFFFFFF')
C 
C  * Extensions need a way to hang private data on some structures.
C  
#define XExtData_ptr INTEGER*4
      STRUCTURE /XExtData/
          INTEGER  number
          XExtData_ptr next
          ProcPointer free_private
          XPointer private_data
          END STRUCTURE
C 
C  * This file contains structures used by the extension mechanism.
C  
      STRUCTURE /XExtCodes/
          INTEGER  extension
          INTEGER  major_opcode
          INTEGER  first_event
          INTEGER  first_error
          END STRUCTURE
#define XExtCodes_ptr INTEGER*4
C 
C  * This structure is private to the library.
C  
#define _XExtension_ptr INTEGER
C 
C  * Data structure for retrieving info about pixmap formats.
C  
      STRUCTURE /XPixmapFormatValues/
          INTEGER  depth
          INTEGER  bits_per_pixel
          INTEGER  scanline_pad
          END STRUCTURE
#define XPixmapFormatValues_ptr INTEGER*4
C 
C  * Data structure for setting graphics context.
C  
      STRUCTURE /XGCValues/
          INTEGER  function
          INTEGER  plane_mask
          INTEGER  foreground
          INTEGER  background
          INTEGER  line_width
          INTEGER  line_style
          INTEGER  cap_style
          INTEGER  join_style
          INTEGER  fill_style
          INTEGER  fill_rule
          INTEGER  arc_mode
          Pixmap tile
          Pixmap stipple
          INTEGER  ts_x_origin
          INTEGER  ts_y_origin
          Font font
          INTEGER  subwindow_mode
          Bool graphics_exposures
          INTEGER  clip_x_origin
          INTEGER  clip_y_origin
          Pixmap clip_mask
          INTEGER  dash_offset
          CHARACTER  dashes
          END STRUCTURE
#define XGCValues_ptr INTEGER*4
C 
C 
C  * Graphics context.  The contents of this structure are implementation
C  * dependent.  A GC should be treated as opaque by application code.
C 
C  
      STRUCTURE /GC_rec/
          XExtData_ptr ext_data
          GContext gid
          Bool rects
          Bool dashes
          INTEGER  dirty
          RECORD /XGCValues/ values
          END STRUCTURE
#define GC INTEGER*4
C 
C  * Visual structure; contains information about colormapping possible.
C  
      STRUCTURE /Visual/
          XExtData_ptr ext_data
          VisualID visualid
          INTEGER  class
          INTEGER  red_mask, green_mask, blue_mask
          INTEGER  bits_per_rgb
          INTEGER  map_entries
          END STRUCTURE
#define Visual_ptr INTEGER*4
C 
C  * Depth structure; contains information for each possible depth.
C  
      STRUCTURE /Depth/
          INTEGER  depth
          INTEGER  nvisuals
          Visual_ptr visuals
          END STRUCTURE
#define Depth_ptr INTEGER*4
C 
C 
C  * Information about the screen.  The contents of this structure are
C  * implementation dependent.  A Screen should be treated as opaque
C  * by application code.
C 
C  
#define Display_ptr INTEGER*4
      STRUCTURE /Screen/
          XExtData_ptr ext_data
          Display_ptr display
          Window root
          INTEGER  width, height
          INTEGER  mwidth, mheight
          INTEGER  ndepths
          Depth_ptr depths
          INTEGER  root_depth
          Visual_ptr root_visual
          GC default_gc
          Colormap cmap
          INTEGER  white_pixel
          INTEGER  black_pixel
          INTEGER  max_maps, min_maps
          INTEGER  backing_store
          Bool save_unders
          INTEGER  root_input_mask
          END STRUCTURE
#define Screen_ptr INTEGER*4
C 
C  * Format structure; describes ZFormat data the screen will understand.
C  
#define ScreenFormat_ptr INTEGER*4
      STRUCTURE /ScreenFormat/
          XExtData_ptr ext_data
          INTEGER  depth
          INTEGER  bits_per_pixel
          INTEGER  scanline_pad
          END STRUCTURE
C 
C  * Data structure for setting window attributes.
C  
      STRUCTURE /XSetWindowAttributes/
          Pixmap background_pixmap
          INTEGER  background_pixel
          Pixmap border_pixmap
          INTEGER  border_pixel
          INTEGER  bit_gravity
          INTEGER  win_gravity
          INTEGER  backing_store
          INTEGER  backing_planes
          INTEGER  backing_pixel
          Bool save_under
          INTEGER  event_mask
          INTEGER  do_not_propagate_mask
          Bool override_redirect
          Colormap colormap
          Cursor cursor
          END STRUCTURE
      STRUCTURE /XWindowAttributes/
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  border_width
          INTEGER  depth
          Visual_ptr visual
          Window root
          INTEGER  class
          INTEGER  bit_gravity
          INTEGER  win_gravity
          INTEGER  backing_store
          INTEGER  backing_planes
          INTEGER  backing_pixel
          Bool save_under
          Colormap colormap
          Bool map_installed
          INTEGER  map_state
          INTEGER  all_event_masks
          INTEGER  your_event_mask
          INTEGER  do_not_propagate_mask
          Bool override_redirect
          Screen_ptr screen
          END STRUCTURE
C 
C  * Data structure for host setting; getting routines.
C  *
C  
      STRUCTURE /XHostAddress/
          INTEGER  family
          INTEGER  length
          Char_ptr address
          END STRUCTURE
#define XHostAddress_ptr INTEGER*4
C 
C  * Data structure for "image" data, used by image manipulation routines.
C  
#define XImage_ptr INTEGER*4
#define XImage_create_image_proc INTEGER
#define XImage_destroy_image_proc INTEGER
#define XImage_get_pixel_proc INTEGER
#define XImage_put_pixel_proc INTEGER
#define XImage_sub_image_proc INTEGER
#define XImage_add_pixel_proc INTEGER
      STRUCTURE /XImage/
          INTEGER  width, height
          INTEGER  xoffset
          INTEGER  format
          Char_ptr data
          INTEGER  byte_order
          INTEGER  bitmap_unit
          INTEGER  bitmap_bit_order
          INTEGER  bitmap_pad
          INTEGER  depth
          INTEGER  bytes_per_line
          INTEGER  bits_per_pixel
          INTEGER  red_mask
          INTEGER  green_mask
          INTEGER  blue_mask
          XPointer obdata
          STRUCTURE /x__dummyXlib0/
              XImage_create_image_proc create_image
              XImage_destroy_image_proc destroy_image
              XImage_get_pixel_proc get_pixel
              XImage_put_pixel_proc put_pixel
              XImage_sub_image_proc sub_image
              XImage_add_pixel_proc add_pixel
              END STRUCTURE
          RECORD /x__dummyXlib0/ f 
          END STRUCTURE
C  
C  * Data structure for XReconfigureWindow
C  
      STRUCTURE /XWindowChanges/
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  border_width
          Window sibling
          INTEGER  stack_mode
          END STRUCTURE
C 
C  * Data structure used by color operations
C  
      STRUCTURE /XColor/
          INTEGER  pixel
          INTEGER*2  red, green, blue
          CHARACTER  flags
          CHARACTER  pad
          END STRUCTURE
C  
C  * Data structures for graphics operations.  On most machines, these are
C  * congruent with the wire protocol structures, so reformatting the data
C  * can be avoided on these architectures.
C  
      STRUCTURE /XSegment/
          INTEGER*2  x1, y1, x2, y2
          END STRUCTURE
      STRUCTURE /XPoint/
          INTEGER*2  x, y
          END STRUCTURE
      STRUCTURE /XRectangle/
          INTEGER*2  x, y
          INTEGER*2  width, height
          END STRUCTURE
      STRUCTURE /XArc/
          INTEGER*2  x, y
          INTEGER*2  width, height
          INTEGER*2  angle1, angle2
          END STRUCTURE
C  Data structure for XChangeKeyboardControl 
      STRUCTURE /XKeyboardControl/
          INTEGER  key_click_percent
          INTEGER  bell_percent
          INTEGER  bell_pitch
          INTEGER  bell_duration
          INTEGER  led
          INTEGER  led_mode
          INTEGER  key
          INTEGER  auto_repeat_mode
          END STRUCTURE
C  Data structure for XGetKeyboardControl 
      STRUCTURE /XKeyboardState/
          INTEGER  key_click_percent
          INTEGER  bell_percent
          INTEGER  bell_pitch, bell_duration
          INTEGER  led_mask
          INTEGER  global_auto_repeat
          CHARACTER auto_repeats (0 : 31)
      
          END STRUCTURE
C  Data structure for XGetMotionEvents.  
      STRUCTURE /XTimeCoord/
          Time time
          INTEGER*2  x, y
          END STRUCTURE
#define XTimeCoord_ptr INTEGER*4
C  Data structure for X{Set,Get}ModifierMapping 
      STRUCTURE /XModifierKeymap/
          INTEGER  max_keypermod
          INTEGER*4 modifiermap
          END STRUCTURE
#define XModifierKeymap_ptr INTEGER*4
C 
C  * Display datatype maintaining display specific data.
C 
C  * The contents of this structure are implementation dependent.
C  * A Display should be treated as opaque by application code.
C 
C  
#define Display_resource_alloc_proc INTEGER
#define Display_synchandler_proc INTEGER
#define Display_event_vec_proc INTEGER
#define Display_wire_vec_proc INTEGER
#define XQEvent_ptr INTEGER*4
#define XFreeFuncs_ptr INTEGER
#define XKeytrans_ptr INTEGER
#define XDisplayAtoms_ptr INTEGER
#define XContextDB_ptr INTEGER
#define XIMFilter_ptr INTEGER
      STRUCTURE /Display/
          XExtData_ptr ext_data
          XFreeFuncs_ptr free_funcs
          INTEGER  fd
          INTEGER  conn_checker
          INTEGER  proto_major_version
          INTEGER  proto_minor_version
          Char_ptr vendor
          XID resource_base
          XID resource_mask
          XID resource_id
          INTEGER  resource_shift
          Display_resource_alloc_proc resource_alloc
          INTEGER  byte_order
          INTEGER  bitmap_unit
          INTEGER  bitmap_pad
          INTEGER  bitmap_bit_order
          INTEGER  nformats
          ScreenFormat_ptr pixmap_format
          INTEGER  vnumber
          INTEGER  release
          XQEvent_ptr head, tail
          INTEGER  qlen
          INTEGER  last_request_read
          INTEGER  request
          Char_ptr last_req
          Char_ptr buffer
          Char_ptr bufptr
          Char_ptr bufmax
          INTEGER  max_request_size
          INTEGER  db
          Display_synchandler_proc synchandler
          Char_ptr display_name
          INTEGER  default_screen
          INTEGER  nscreens
          Screen_ptr screens
          INTEGER  motion_buffer
          INTEGER  flags
          INTEGER  min_keycode
          INTEGER  max_keycode
          INTEGER*4 keysyms
          XModifierKeymap_ptr modifiermap
          INTEGER  keysyms_per_keycode
          Char_ptr xdefaults
          Char_ptr scratch_buffer
          INTEGER  scratch_length
          INTEGER  ext_number
          _XExtension_ptr ext_procs
          Display_event_vec_proc event_vec (0 : 127)
      
          Display_event_vec_proc wire_vec (0 : 127)
      
          KeySym lock_meaning
          XPointer lock
          XPointer async_handlers
          INTEGER  bigreq_size
          XPointer lock_fns
          XKeytrans_ptr key_bindings
          Font cursor_font
          XDisplayAtoms_ptr atoms
          INTEGER  mode_switch
          XContextDB_ptr context_db
          ProcPointer error_vec
          STRUCTURE /x__dummyXlib1/
              XPointer defaultCCCs
              XPointer clientCmaps
              XPointer perVisualIntensityMaps
              END STRUCTURE
          RECORD /x__dummyXlib1/ cms 
          XIMFilter_ptr im_filters
          XPointer qfree
          INTEGER  next_event_serial_num
          ProcPointer savedsynchandler
          END STRUCTURE
C 
C  * A "XEvent" structure always  has type as the first entry.  This 
C  * uniquely identifies what  kind of event it is.  The second entry
C  * is always a pointer to the display the event was read from.
C  * The third entry is always a window of one type or another,
C  * carefully selected to be useful to toolkit dispatchers.  (Except
C  * for keymap events, which have no window.) You
C  * must not change the order of the three elements or toolkits will
C  * break! The pointer to the generic event must be cast before use to 
C  * access any other information in the structure.
C  
C 
C  * Definitions of specific events.
C  
      STRUCTURE /XKeyEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Window root
          Window subwindow
          Time time
          INTEGER  x, y
          INTEGER  x_root, y_root
          INTEGER  state
          INTEGER  keycode
          Bool same_screen
          END STRUCTURE
#define XKeyEvent_ptr INTEGER*4
#define XKeyPressedEvent RECORD /XKeyEvent/
      STRUCTURE /XButtonEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Window root
          Window subwindow
          Time time
          INTEGER  x, y
          INTEGER  x_root, y_root
          INTEGER  state
          INTEGER  button
          Bool same_screen
          END STRUCTURE
      STRUCTURE /XMotionEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Window root
          Window subwindow
          Time time
          INTEGER  x, y
          INTEGER  x_root, y_root
          INTEGER  state
          CHARACTER  is_hint
          Bool same_screen
          END STRUCTURE
      STRUCTURE /XCrossingEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Window root
          Window subwindow
          Time time
          INTEGER  x, y
          INTEGER  x_root, y_root
          INTEGER  mode
          INTEGER  detail
          Bool same_screen
          Bool focus
          INTEGER  state
          END STRUCTURE
      STRUCTURE /XFocusChangeEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          INTEGER  mode
          INTEGER  detail
          END STRUCTURE
C  generated on EnterWindow and FocusIn  when KeyMapState selected 
      STRUCTURE /XKeymapEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          CHARACTER key_vector (0 : 31)
      
          END STRUCTURE
      STRUCTURE /XExposeEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  count
          END STRUCTURE
      STRUCTURE /XGraphicsExposeEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Drawable drawable
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  count
          INTEGER  major_code
          INTEGER  minor_code
          END STRUCTURE
      STRUCTURE /XNoExposeEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Drawable drawable
          INTEGER  major_code
          INTEGER  minor_code
          END STRUCTURE
      STRUCTURE /XVisibilityEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          INTEGER  state
          END STRUCTURE
      STRUCTURE /XCreateWindowEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window parent
          Window window
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  border_width
          Bool override_redirect
          END STRUCTURE
      STRUCTURE /XDestroyWindowEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          END STRUCTURE
      STRUCTURE /XUnmapEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          Bool from_configure
          END STRUCTURE
      STRUCTURE /XMapEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          Bool override_redirect
          END STRUCTURE
      STRUCTURE /XMapRequestEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window parent
          Window window
          END STRUCTURE
      STRUCTURE /XReparentEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          Window parent
          INTEGER  x, y
          Bool override_redirect
          END STRUCTURE
      STRUCTURE /XConfigureEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  border_width
          Window above
          Bool override_redirect
          END STRUCTURE
      STRUCTURE /XGravityEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          INTEGER  x, y
          END STRUCTURE
      STRUCTURE /XResizeRequestEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          INTEGER  width, height
          END STRUCTURE
      STRUCTURE /XConfigureRequestEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window parent
          Window window
          INTEGER  x, y
          INTEGER  width, height
          INTEGER  border_width
          Window above
          INTEGER  detail
          INTEGER  value_mask
          END STRUCTURE
      STRUCTURE /XCirculateEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window event
          Window window
          INTEGER  place
          END STRUCTURE
      STRUCTURE /XCirculateRequestEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window parent
          Window window
          INTEGER  place
          END STRUCTURE
      STRUCTURE /XPropertyEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Atom atom
          Time time
          INTEGER  state
          END STRUCTURE
      STRUCTURE /XSelectionClearEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Atom selection
          Time time
          END STRUCTURE
      STRUCTURE /XSelectionRequestEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window owner
          Window requestor
          Atom selection
          Atom target
          Atom property
          Time time
          END STRUCTURE
      STRUCTURE /XSelectionEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window requestor
          Atom selection
          Atom target
          Atom property
          Time time
          END STRUCTURE
      STRUCTURE /XColormapEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Colormap colormap
          Bool new
          INTEGER  state
          END STRUCTURE
      STRUCTURE /XClientMessageEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          Atom message_type
          INTEGER  format
          STRUCTURE /x__dummyXlib2/
              UNION
              MAP
                  CHARACTER b (0 : 19)
      
              END MAP
              MAP
                  INTEGER*2 s (0 : 9)
      
              END MAP
              MAP
                  INTEGER l (0 : 4)
      
              END MAP
              END UNION
              END STRUCTURE
          RECORD /x__dummyXlib2/ data 
          END STRUCTURE
      STRUCTURE /XMappingEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          INTEGER  request
          INTEGER  first_keycode
          INTEGER  count
          END STRUCTURE
      STRUCTURE /XErrorEvent/
          INTEGER  type
          Display_ptr display
          XID resourceid
          INTEGER  serial
          Byte error_code
          Byte request_code
          Byte minor_code
          END STRUCTURE
#define XErrorEvent_ptr INTEGER*4
      STRUCTURE /XAnyEvent/
          INTEGER  type
          INTEGER  serial
          Bool send_event
          Display_ptr display
          Window window
          END STRUCTURE
C 
C  * this union is defined so Xlib can always use the same sized
C  * event structure internally, to avoid memory fragmentation.
C  
      STRUCTURE /XEvent/
          UNION
          MAP
              INTEGER  type
          END MAP
          MAP
              RECORD /XAnyEvent/ xany
          END MAP
          MAP
              RECORD /XKeyEvent/ xkey
          END MAP
          MAP
              RECORD /XButtonEvent/ xbutton
          END MAP
          MAP
              RECORD /XMotionEvent/ xmotion
          END MAP
          MAP
              RECORD /XCrossingEvent/ xcrossing
          END MAP
          MAP
              RECORD /XFocusChangeEvent/ xfocus
          END MAP
          MAP
              RECORD /XExposeEvent/ xexpose
          END MAP
          MAP
              RECORD /XGraphicsExposeEvent/ xgraphicsexpose
          END MAP
          MAP
              RECORD /XNoExposeEvent/ xnoexpose
          END MAP
          MAP
              RECORD /XVisibilityEvent/ xvisibility
          END MAP
          MAP
              RECORD /XCreateWindowEvent/ xcreatewindow
          END MAP
          MAP
              RECORD /XDestroyWindowEvent/ xdestroywindow
          END MAP
          MAP
              RECORD /XUnmapEvent/ xunmap
          END MAP
          MAP
              RECORD /XMapEvent/ xmap
          END MAP
          MAP
              RECORD /XMapRequestEvent/ xmaprequest
          END MAP
          MAP
              RECORD /XReparentEvent/ xreparent
          END MAP
          MAP
              RECORD /XConfigureEvent/ xconfigure
          END MAP
          MAP
              RECORD /XGravityEvent/ xgravity
          END MAP
          MAP
              RECORD /XResizeRequestEvent/ xresizerequest
          END MAP
          MAP
              RECORD /XConfigureRequestEvent/ xconfigurerequest
          END MAP
          MAP
              RECORD /XCirculateEvent/ xcirculate
          END MAP
          MAP
              RECORD /XCirculateRequestEvent/ xcirculaterequest
          END MAP
          MAP
              RECORD /XPropertyEvent/ xproperty
          END MAP
          MAP
              RECORD /XSelectionClearEvent/ xselectionclear
          END MAP
          MAP
              RECORD /XSelectionRequestEvent/ xselectionrequest
          END MAP
          MAP
              RECORD /XSelectionEvent/ xselection
          END MAP
          MAP
              RECORD /XColormapEvent/ xcolormap
          END MAP
          MAP
              RECORD /XClientMessageEvent/ xclient
          END MAP
          MAP
              RECORD /XMappingEvent/ xmapping
          END MAP
          MAP
              RECORD /XErrorEvent/ xerror
          END MAP
          MAP
              RECORD /XKeymapEvent/ xkeymap
          END MAP
          MAP
              INTEGER pad (0 : 23)
      
          END MAP
          END UNION
          END STRUCTURE
      STRUCTURE /XQEvent/
          XQEvent_ptr next
          RECORD /XEvent/ event
          END STRUCTURE
      XID  XAllocID
      EXTERNAL XAllocID
C       Display_ptr dpy

C 
C  * per character font metric information.
C  
      STRUCTURE /XCharStruct/
          INTEGER*2  lbearing
          INTEGER*2  rbearing
          INTEGER*2  width
          INTEGER*2  ascent
          INTEGER*2  descent
          INTEGER*2  attributes
          END STRUCTURE
#define XCharStruct_ptr INTEGER*4
C 
C  * To allow arbitrary information with fonts, there are additional properties
C  * returned.
C  
      STRUCTURE /XFontProp/
          Atom name
          INTEGER  card32
          END STRUCTURE
#define XFontProp_ptr INTEGER*4
      STRUCTURE /XFontStruct/
          XExtData_ptr ext_data
          Font fid
          INTEGER  direction
          INTEGER  min_char_or_byte2
          INTEGER  max_char_or_byte2
          INTEGER  min_byte1
          INTEGER  max_byte1
          Bool all_chars_exist
          INTEGER  default_char
          INTEGER  n_properties
          XFontProp_ptr properties
          RECORD /XCharStruct/ min_bounds
          RECORD /XCharStruct/ max_bounds
          XCharStruct_ptr per_char
          INTEGER  ascent
          INTEGER  descent
          END STRUCTURE
#define XFontStruct_ptr INTEGER*4
C 
C  * PolyText routines take these as arguments.
C  
      STRUCTURE /XTextItem/
          Char_ptr chars
          INTEGER  nchars
          INTEGER  delta
          Font font
          END STRUCTURE
      STRUCTURE /XChar2b/
          Byte byte1
          Byte byte2
          END STRUCTURE
#define XChar2b_ptr INTEGER*4
      STRUCTURE /XTextItem16/
          XChar2b_ptr chars
          INTEGER  nchars
          INTEGER  delta
          Font font
          END STRUCTURE
      STRUCTURE /XEDataObject/
          UNION
          MAP
              Display_ptr display
          END MAP
          MAP
              GC gc
          END MAP
          MAP
              Visual_ptr visual
          END MAP
          MAP
              Screen_ptr screen
          END MAP
          MAP
              ScreenFormat_ptr pixmap_format
          END MAP
          MAP
              XFontStruct_ptr font
          END MAP
          END UNION
          END STRUCTURE
      STRUCTURE /XFontSetExtents/
          RECORD /XRectangle/ max_ink_extent
          RECORD /XRectangle/ max_logical_extent
          END STRUCTURE
#define XFontSetExtents_ptr INTEGER*4
#define XFontSet INTEGER
      STRUCTURE /XmbTextItem/
          Char_ptr chars
          INTEGER  nchars
          INTEGER  delta
          XFontSet font_set
          END STRUCTURE
#define XmbTextItem_ptr INTEGER*4
      STRUCTURE /XwcTextItem/
          Wchar_ptr chars
          INTEGER  nchars
          INTEGER  delta
          XFontSet font_set
          END STRUCTURE
#define XwcTextItem_ptr INTEGER*4
#define XIMProc ProcPointer
#define XIM INTEGER
#define XIC INTEGER
#define XIMStyle INTEGER
      STRUCTURE /XIMStyles/
          INTEGER*2  count_styles
          XIMStyle supported_styles
          END STRUCTURE
      INTEGER XIMPreeditArea
      PARAMETER (XIMPreeditArea = X'1')
      INTEGER XIMPreeditCallbacks
      PARAMETER (XIMPreeditCallbacks = X'2')
      INTEGER XIMPreeditPosition
      PARAMETER (XIMPreeditPosition = X'4')
      INTEGER XIMPreeditNothing
      PARAMETER (XIMPreeditNothing = X'8')
      INTEGER XIMPreeditNone
      PARAMETER (XIMPreeditNone = X'10')
      INTEGER XIMStatusArea
      PARAMETER (XIMStatusArea = X'100')
      INTEGER XIMStatusCallbacks
      PARAMETER (XIMStatusCallbacks = X'200')
      INTEGER XIMStatusNothing
      PARAMETER (XIMStatusNothing = X'400')
      INTEGER XIMStatusNone
      PARAMETER (XIMStatusNone = X'800')
      CHARACTER*(14) XNVaNestedList
      PARAMETER (XNVaNestedList = "XNVaNestedList")
      CHARACTER*(15) XNQueryInputStyle
      PARAMETER (XNQueryInputStyle = "queryInputStyle")
      CHARACTER*(12) XNClientWindow
      PARAMETER (XNClientWindow = "clientWindow")
      CHARACTER*(10) XNInputStyle
      PARAMETER (XNInputStyle = "inputStyle")
      CHARACTER*(11) XNFocusWindow
      PARAMETER (XNFocusWindow = "focusWindow")
      CHARACTER*(12) XNResourceName
      PARAMETER (XNResourceName = "resourceName")
      CHARACTER*(13) XNResourceClass
      PARAMETER (XNResourceClass = "resourceClass")
      CHARACTER*(16) XNGeometryCallback
      PARAMETER (XNGeometryCallback = "geometryCallback")
      CHARACTER*(12) XNFilterEvents
      PARAMETER (XNFilterEvents = "filterEvents")
      CHARACTER*(20) XNPreeditStartCallback
      PARAMETER (XNPreeditStartCallback = "preeditStartCallback")
      CHARACTER*(19) XNPreeditDoneCallback
      PARAMETER (XNPreeditDoneCallback = "preeditDoneCallback")
      CHARACTER*(19) XNPreeditDrawCallback
      PARAMETER (XNPreeditDrawCallback = "preeditDrawCallback")
      CHARACTER*(20) XNPreeditCaretCallback
      PARAMETER (XNPreeditCaretCallback = "preeditCaretCallback")
      CHARACTER*(17) XNPreeditAttributes
      PARAMETER (XNPreeditAttributes = "preeditAttributes")
      CHARACTER*(19) XNStatusStartCallback
      PARAMETER (XNStatusStartCallback = "statusStartCallback")
      CHARACTER*(18) XNStatusDoneCallback
      PARAMETER (XNStatusDoneCallback = "statusDoneCallback")
      CHARACTER*(18) XNStatusDrawCallback
      PARAMETER (XNStatusDrawCallback = "statusDrawCallback")
      CHARACTER*(16) XNStatusAttributes
      PARAMETER (XNStatusAttributes = "statusAttributes")
      CHARACTER*(4) XNArea
      PARAMETER (XNArea = "area")
      CHARACTER*(10) XNAreaNeeded
      PARAMETER (XNAreaNeeded = "areaNeeded")
      CHARACTER*(12) XNSpotLocation
      PARAMETER (XNSpotLocation = "spotLocation")
      CHARACTER*(8) XNColormap
      PARAMETER (XNColormap = "colorMap")
      CHARACTER*(11) XNStdColormap
      PARAMETER (XNStdColormap = "stdColorMap")
      CHARACTER*(10) XNForeground
      PARAMETER (XNForeground = "foreground")
      CHARACTER*(10) XNBackground
      PARAMETER (XNBackground = "background")
      CHARACTER*(16) XNBackgroundPixmap
      PARAMETER (XNBackgroundPixmap = "backgroundPixmap")
      CHARACTER*(7) XNFontSet
      PARAMETER (XNFontSet = "fontSet")
      CHARACTER*(9) XNLineSpace
      PARAMETER (XNLineSpace = "lineSpace")
      CHARACTER*(6) XNCursor
      PARAMETER (XNCursor = "cursor")
      INTEGER XBufferOverflow
      PARAMETER (XBufferOverflow = -1)
      INTEGER XLookupNone
      PARAMETER (XLookupNone = 1)
      INTEGER XLookupChars
      PARAMETER (XLookupChars = 2)
      INTEGER XLookupKeySym
      PARAMETER (XLookupKeySym = 3)
      INTEGER XLookupBoth
      PARAMETER (XLookupBoth = 4)
#define XVaNestedList XPointer
      STRUCTURE /XIMCallback/
          XPointer client_data
          XIMProc callback
          END STRUCTURE
#define XIMCallbackm_ptr INTEGER*4
#define XIMFeedback INTEGER
#define XIMFeedback_ptr INTEGER*4
      INTEGER XIMReverse
      PARAMETER (XIMReverse = 1)
      INTEGER XIMUnderline
      PARAMETER (XIMUnderline = X'2')
      INTEGER XIMHighlight
      PARAMETER (XIMHighlight = X'4')
      INTEGER XIMPrimary
      PARAMETER (XIMPrimary = X'20')
      INTEGER XIMSecondary
      PARAMETER (XIMSecondary = X'40')
      INTEGER XIMTertiary
      PARAMETER (XIMTertiary = X'80')
      STRUCTURE /XIMText/
          INTEGER*2  length
          XIMFeedback_ptr feedback
          Bool encoding_is_wchar
          STRUCTURE /x__dummyXlib3/
              UNION
              MAP
                  Char_ptr multi_byte
              END MAP
              MAP
                  Wchar_ptr wide_char
              END MAP
              END UNION
              END STRUCTURE
          RECORD /x__dummyXlib3/ string 
          END STRUCTURE
#define XIMText_ptr INTEGER*4
      STRUCTURE /XIMPreeditDrawCallbackStruct/
          INTEGER  caret
          INTEGER  chg_first
          INTEGER  chg_length
          XIMText_ptr text
          END STRUCTURE
#define XIMPreeditDrawCallbackStruct_ptr INTEGER*4
      INTEGER XIMForwardChar,XIMBackwardChar,XIMForwardWord
     &,XIMBackwardWord,XIMCaretUp,XIMCaretDown,XIMNextLine
     &,XIMPreviousLine,XIMLineStart,XIMLineEnd,XIMAbsolutePosition
     &,XIMDontChange
      PARAMETER ( XIMForwardChar=0,  XIMBackwardChar=1, 
     & XIMForwardWord=2,  XIMBackwardWord=3,  XIMCaretUp=4, 
     & XIMCaretDown=5,  XIMNextLine=6,  XIMPreviousLine=7, 
     & XIMLineStart=8,  XIMLineEnd=9,  XIMAbsolutePosition=10, 
     & XIMDontChange=11)
#define XIMCaretDirection INTEGER*4
      INTEGER XIMIsInvisible,XIMIsPrimary,XIMIsSecondary
      PARAMETER ( XIMIsInvisible=0,  XIMIsPrimary=1,  XIMIsSecondary=2)
#define XIMCaretStyle INTEGER*4
      STRUCTURE /XIMPreeditCaretCallbackStruct/
          INTEGER  position
          XIMCaretDirection direction
          XIMCaretStyle style
          END STRUCTURE
#define XIMPreeditCaretCallbackStruct_ptr INTEGER*4
      INTEGER XIMTextType,XIMBitmapType
      PARAMETER ( XIMTextType=0,  XIMBitmapType=1)
#define XIMStatusDataType INTEGER*4
      STRUCTURE /XIMStatusDrawCallbackStruct/
          XIMStatusDataType type
          STRUCTURE /x__dummyXlib4/
              UNION
              MAP
                  XIMText_ptr text
              END MAP
              MAP
                  Pixmap bitmap
              END MAP
              END UNION
              END STRUCTURE
          RECORD /x__dummyXlib4/ data 
          END STRUCTURE
#define XIMStatusDrawCallbackStruct_ptr INTEGER*4
      INTEGER  ConnectionNumber
      EXTERNAL ConnectionNumber
C       Display_ptr dpy

      Window  RootWindow
      EXTERNAL RootWindow
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DefaultScreen
      EXTERNAL DefaultScreen
C       Display_ptr dpy

      Window  DefaultRootWindow
      EXTERNAL DefaultRootWindow
C       Display_ptr dpy

      Visual_ptr  DefaultVisual
      EXTERNAL DefaultVisual
C       Display_ptr dpy
C       INTEGER scr

      GC  DefaultGC
      EXTERNAL DefaultGC
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  BlackPixel
      EXTERNAL BlackPixel
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  WhitePixel
      EXTERNAL WhitePixel
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  QLength
      EXTERNAL QLength
C       Display_ptr dpy

      INTEGER  DisplayWidth
      EXTERNAL DisplayWidth
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DisplayHeight
      EXTERNAL DisplayHeight
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DisplayWidthMM
      EXTERNAL DisplayWidthMM
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DisplayHeightMM
      EXTERNAL DisplayHeightMM
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DisplayPlanes
      EXTERNAL DisplayPlanes
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  DisplayCells
      EXTERNAL DisplayCells
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  ScreenCount
      EXTERNAL ScreenCount
C       Display_ptr dpy

      Char_ptr  ServerVendor
      EXTERNAL ServerVendor
C       Display_ptr dpy

      INTEGER  ProtocolVersion
      EXTERNAL ProtocolVersion
C       Display_ptr dpy

      INTEGER  ProtocolRevision
      EXTERNAL ProtocolRevision
C       Display_ptr dpy

      INTEGER  VendorRelease
      EXTERNAL VendorRelease
C       Display_ptr dpy

      Char_ptr  DisplayString
      EXTERNAL DisplayString
C       Display_ptr dpy

      INTEGER  DefaultDepth
      EXTERNAL DefaultDepth
C       Display_ptr dpy
C       INTEGER scr

      Colormap  DefaultColormap
      EXTERNAL DefaultColormap
C       Display_ptr dpy
C       INTEGER scr

      INTEGER  BitmapUnit
      EXTERNAL BitmapUnit
C       Display_ptr dpy

      INTEGER  BitmapBitOrder
      EXTERNAL BitmapBitOrder
C       Display_ptr dpy

      INTEGER  BitmapPad
      EXTERNAL BitmapPad
C       Display_ptr dpy

      INTEGER  ImageByteOrder
      EXTERNAL ImageByteOrder
C       Display_ptr dpy

      INTEGER  NextRequest
      EXTERNAL NextRequest
C       Display_ptr dpy

      INTEGER  LastKnownRequestProcessed
      EXTERNAL LastKnownRequestProcessed
C       Display_ptr dpy

C  macros for screen oriented applications (toolkit) 
      Screen_ptr  ScreenOfDisplay
      EXTERNAL ScreenOfDisplay
C       Display_ptr dpy
C       INTEGER scr

      Screen_ptr  DefaultScreenOfDisplay
      EXTERNAL DefaultScreenOfDisplay
C       Display_ptr dpy

      Display_ptr  DisplayOfScreen
      EXTERNAL DisplayOfScreen
C       Screen_ptr s

      Window  RootWindowOfScreen
      EXTERNAL RootWindowOfScreen
C       Screen_ptr s

      INTEGER  BlackPixelOfScreen
      EXTERNAL BlackPixelOfScreen
C       Screen_ptr s

      INTEGER  WhitePixelOfScreen
      EXTERNAL WhitePixelOfScreen
C       Screen_ptr s

      Colormap  DefaultColormapOfScreen
      EXTERNAL DefaultColormapOfScreen
C       Screen_ptr s

      INTEGER  DefaultDepthOfScreen
      EXTERNAL DefaultDepthOfScreen
C       Screen_ptr s

      GC  DefaultGCOfScreen
      EXTERNAL DefaultGCOfScreen
C       Screen_ptr s

      Visual_ptr  DefaultVisualOfScreen
      EXTERNAL DefaultVisualOfScreen
C       Screen_ptr s

      INTEGER  WidthOfScreen
      EXTERNAL WidthOfScreen
C       Screen_ptr s

      INTEGER  HeightOfScreen
      EXTERNAL HeightOfScreen
C       Screen_ptr s

      INTEGER  WidthMMOfScreen
      EXTERNAL WidthMMOfScreen
C       Screen_ptr s

      INTEGER  HeightMMOfScreen
      EXTERNAL HeightMMOfScreen
C       Screen_ptr s

      INTEGER  PlanesOfScreen
      EXTERNAL PlanesOfScreen
C       Screen_ptr s

      INTEGER  CellsOfScreen
      EXTERNAL CellsOfScreen
C       Screen_ptr s

      INTEGER  MinCmapsOfScreen
      EXTERNAL MinCmapsOfScreen
C       Screen_ptr s

      INTEGER  MaxCmapsOfScreen
      EXTERNAL MaxCmapsOfScreen
C       Screen_ptr s

      Bool  DoesSaveUnders
      EXTERNAL DoesSaveUnders
C       Screen_ptr s

      INTEGER  DoesBackingStore
      EXTERNAL DoesBackingStore
C       Screen_ptr s

      INTEGER  EventMaskOfScreen
      EXTERNAL EventMaskOfScreen
C       Screen_ptr s

      XFontStruct_ptr  XLoadQueryFont
      EXTERNAL XLoadQueryFont
C       Display_ptr display
C       CHARACTER name

      XFontStruct_ptr  XQueryFont
      EXTERNAL XQueryFont
C       Display_ptr display
C       XID font_ID

      XTimeCoord_ptr  XGetMotionEvents
      EXTERNAL XGetMotionEvents
C       Display_ptr display
C       Window w
C       Time start
C       Time stop
C       INTEGER nevents_return

      XModifierKeymap_ptr  XDeleteModifiermapEntry
      EXTERNAL XDeleteModifiermapEntry
C       XModifierKeymap_ptr modmap
C       KeyCode keycode_entry
C       INTEGER modifier

      XModifierKeymap_ptr  XGetModifierMapping
      EXTERNAL XGetModifierMapping
C       Display_ptr display

      XModifierKeymap_ptr  XInsertModifiermapEntry
      EXTERNAL XInsertModifiermapEntry
C       XModifierKeymap_ptr modmap
C       KeyCode keycode_entry
C       INTEGER modifier

      XModifierKeymap_ptr  XNewModifiermap
      EXTERNAL XNewModifiermap
C       INTEGER max_keys_per_mod

      XImage_ptr  XCreateImage
      EXTERNAL XCreateImage
C       Display_ptr display
C       Visual_ptr visual
C       INTEGER depth
C       INTEGER format
C       INTEGER offset
C       Char_ptr data
C       INTEGER width
C       INTEGER height
C       INTEGER bitmap_pad
C       INTEGER bytes_per_line

      XImage_ptr  XGetImage
      EXTERNAL XGetImage
C       Display_ptr display
C       Drawable d
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER plane_mask
C       INTEGER format

      XImage_ptr  XGetSubImage
      EXTERNAL XGetSubImage
C       Display_ptr display
C       Drawable d
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER plane_mask
C       INTEGER format
C       XImage_ptr dest_image
C       INTEGER dest_x
C       INTEGER dest_y

C 
C  * X function declarations.
C  
      Display_ptr  XOpenDisplay
      EXTERNAL XOpenDisplay
C       CHARACTER display_name

      EXTERNAL XrmInitialize
      Char_ptr  XFetchBytes
      EXTERNAL XFetchBytes
C       Display_ptr display
C       INTEGER nbytes_return

      Char_ptr  XFetchBuffer
      EXTERNAL XFetchBuffer
C       Display_ptr display
C       INTEGER nbytes_return
C       INTEGER buffer

      Char_ptr  XGetAtomName
      EXTERNAL XGetAtomName
C       Display_ptr display
C       Atom atom

      Char_ptr  XGetDefault
      EXTERNAL XGetDefault
C       Display_ptr display
C       CHARACTER program
C       CHARACTER option

      Char_ptr  XDisplayName
      EXTERNAL XDisplayName
C       CHARACTER string

      Char_ptr  XKeysymToString
      EXTERNAL XKeysymToString
C       KeySym keysym

#define AfterFunction INTEGER
      AfterFunction  XSynchronize
      EXTERNAL XSynchronize
C       Display_ptr display
C       Bool onoff

      AfterFunction  XSetAfterFunction
      EXTERNAL XSetAfterFunction
C       Display_ptr display
C       AfterFunction func

      Atom  XInternAtom
      EXTERNAL XInternAtom
C       Display_ptr display
C       CHARACTER atom_name
C       Bool only_if_exists

      Colormap  XCopyColormapAndFree
      EXTERNAL XCopyColormapAndFree
C       Display_ptr display
C       Colormap colormap

      Colormap  XCreateColormap
      EXTERNAL XCreateColormap
C       Display_ptr display
C       Window w
C       Visual_ptr visual
C       INTEGER alloc

      Cursor  XCreatePixmapCursor
      EXTERNAL XCreatePixmapCursor
C       Display_ptr display
C       Pixmap source
C       Pixmap mask
C       RECORD /XColor/ foreground_color
C       RECORD /XColor/ background_color
C       INTEGER x_hot
C       INTEGER y_hot

      Cursor  XCreateGlyphCursor
      EXTERNAL XCreateGlyphCursor
C       Display_ptr display
C       Font source_font
C       Font mask_font
C       INTEGER source_char
C       INTEGER mask_char
C       RECORD /XColor/ foreground_color
C       RECORD /XColor/ background_color

      Cursor  XCreateFontCursor
      EXTERNAL XCreateFontCursor
C       Display_ptr display
C       INTEGER shape

      Font  XLoadFont
      EXTERNAL XLoadFont
C       Display_ptr display
C       CHARACTER name

      GC  XCreateGC
      EXTERNAL XCreateGC
C       Display_ptr display
C       Drawable d
C       INTEGER valuemask
C       XGCValues_ptr values

      GContext  XGContextFromGC
      EXTERNAL XGContextFromGC
C       GC gc

      EXTERNAL XFlushGC
C       Display_ptr display
C       GC gc

      Pixmap  XCreatePixmap
      EXTERNAL XCreatePixmap
C       Display_ptr display
C       Drawable d
C       INTEGER width
C       INTEGER height
C       INTEGER depth

      Pixmap  XCreateBitmapFromData
      EXTERNAL XCreateBitmapFromData
C       Display_ptr display
C       Drawable d
C       CHARACTER data
C       INTEGER width
C       INTEGER height

      Pixmap  XCreatePixmapFromBitmapData
      EXTERNAL XCreatePixmapFromBitmapData
C       Display_ptr display
C       Drawable d
C       CHARACTER data
C       INTEGER width
C       INTEGER height
C       INTEGER fg
C       INTEGER bg
C       INTEGER depth

      Window  XCreateSimpleWindow
      EXTERNAL XCreateSimpleWindow
C       Display_ptr display
C       Window parent
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER border_width
C       INTEGER border
C       INTEGER background

      Window  XGetSelectionOwner
      EXTERNAL XGetSelectionOwner
C       Display_ptr display
C       Atom selection

      Window  XCreateWindow
      EXTERNAL XCreateWindow
C       Display_ptr display
C       Window parent
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER border_width
C       INTEGER depth
C       INTEGER class
C       Visual_ptr visual
C       INTEGER valuemask
C       RECORD /XSetWindowAttributes/ attributes

      Colormap_ptr  XListInstalledColormaps
      EXTERNAL XListInstalledColormaps
C       Display_ptr display
C       Window w
C       INTEGER num_return

      INTEGER MaxFontListSize
      PARAMETER (MaxFontListSize = 100)
#define FontList(__v) Char_ptr __v ( 0 : 99 )
#define FontList_ptr INTEGER*4
#define FontStructList(__v) RECORD /XFontStruct/ __v ( 0 : 99 )
#define FontStructList_ptr INTEGER*4
      FontList_ptr  XListFonts
      EXTERNAL XListFonts
C       Display_ptr display
C       CHARACTER pattern
C       INTEGER maxnames
C       INTEGER actual_count_return

      FontList_ptr  XListFontsWithInfo
      EXTERNAL XListFontsWithInfo
C       Display_ptr display
C       CHARACTER pattern
C       INTEGER maxnames
C       INTEGER count_return
C       XFontStruct_ptr info_return

      INTEGER MaxFontPathListSize
      PARAMETER (MaxFontPathListSize = 100)
#define FontPathList(__v) Char_ptr __v ( 0 : 99 )
#define FontPathList_ptr INTEGER*4
      FontPathList_ptr  XGetFontPath
      EXTERNAL XGetFontPath
C       Display_ptr display
C       INTEGER npaths_return

      INTEGER MaxExtensionListSize
      PARAMETER (MaxExtensionListSize = 100)
#define ExtensionList(__v) Char_ptr __v ( 0 : 99 )
#define ExtensionList_ptr INTEGER*4
      ExtensionList_ptr  XListExtensions
      EXTERNAL XListExtensions
C       Display_ptr display
C       INTEGER nextensions_return

      Atom_ptr  XListProperties
      EXTERNAL XListProperties
C       Display_ptr display
C       Window w
C       INTEGER num_prop_return

      XHostAddress_ptr  XListHosts
      EXTERNAL XListHosts
C       Display_ptr display
C       INTEGER nhosts_return
C       Bool state_return

      KeySym  XKeycodeToKeysym
      EXTERNAL XKeycodeToKeysym
C       Display_ptr display
C       KeyCode keycode
C       INTEGER index

      KeySym  XLookupKeysym
      EXTERNAL XLookupKeysym
C       XKeyEvent_ptr key_event
C       INTEGER index

      KeySym_ptr  XGetKeyboardMapping
      EXTERNAL XGetKeyboardMapping
C       Display_ptr display
C       KeyCode first_keycode
C       INTEGER keycode_count
C       INTEGER keysyms_per_keycode_return

      KeySym  XStringToKeysym
      EXTERNAL XStringToKeysym
C       CHARACTER string

      INTEGER  XMaxRequestSize
      EXTERNAL XMaxRequestSize
C       Display_ptr display

      Char_ptr  XResourceManagerString
      EXTERNAL XResourceManagerString
C       Display_ptr display

      Char_ptr  XScreenResourceString
      EXTERNAL XScreenResourceString
C       Screen_ptr screen

      INTEGER  XDisplayMotionBufferSize
      EXTERNAL XDisplayMotionBufferSize
C       Display_ptr display

      VisualID  XVisualIDFromVisual
      EXTERNAL XVisualIDFromVisual
C       Visual_ptr visual

C  routines for dealing with extensions 
      XExtCodes_ptr  XInitExtension
      EXTERNAL XInitExtension
C       Display_ptr display
C       CHARACTER name

      XExtCodes_ptr  XAddExtension
      EXTERNAL XAddExtension
C       Display_ptr display

      INTEGER MaxXExtDataListSize
      PARAMETER (MaxXExtDataListSize = 100)
#define XExtDataList(__v) RECORD /XExtData/ __v ( 0 : 99 )
#define XExtDataList_ptr INTEGER*4
      XExtData_ptr  XFindOnExtensionList
      EXTERNAL XFindOnExtensionList
C       XExtDataList_ptr structure
C       INTEGER number

      XExtDataList_ptr  XEHeadOfExtensionList
      EXTERNAL XEHeadOfExtensionList
C       RECORD /XEDataObject/ object

C  these are routines for which there are also macros 
      Window  XRootWindow
      EXTERNAL XRootWindow
C       Display_ptr display
C       INTEGER screen_number

      Window  XDefaultRootWindow
      EXTERNAL XDefaultRootWindow
C       Display_ptr display

      Window  XRootWindowOfScreen
      EXTERNAL XRootWindowOfScreen
C       Screen_ptr screen

      Visual_ptr  XDefaultVisual
      EXTERNAL XDefaultVisual
C       Display_ptr display
C       INTEGER screen_number

      Visual_ptr  XDefaultVisualOfScreen
      EXTERNAL XDefaultVisualOfScreen
C       Screen_ptr screen

      GC  XDefaultGC
      EXTERNAL XDefaultGC
C       Display_ptr display
C       INTEGER screen_number

      GC  XDefaultGCOfScreen
      EXTERNAL XDefaultGCOfScreen
C       Screen_ptr screen

      INTEGER  XBlackPixel
      EXTERNAL XBlackPixel
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XWhitePixel
      EXTERNAL XWhitePixel
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XAllPlanes
      EXTERNAL XAllPlanes
      INTEGER  XBlackPixelOfScreen
      EXTERNAL XBlackPixelOfScreen
C       Screen_ptr screen

      INTEGER  XWhitePixelOfScreen
      EXTERNAL XWhitePixelOfScreen
C       Screen_ptr screen

      INTEGER  XNextRequest
      EXTERNAL XNextRequest
C       Display_ptr display

      INTEGER  XLastKnownRequestProcessed
      EXTERNAL XLastKnownRequestProcessed
C       Display_ptr display

      Char_ptr  XServerVendor
      EXTERNAL XServerVendor
C       Display_ptr display

      Char_ptr  XDisplayString
      EXTERNAL XDisplayString
C       Display_ptr display

      Colormap  XDefaultColormap
      EXTERNAL XDefaultColormap
C       Display_ptr display
C       INTEGER screen_number

      Colormap  XDefaultColormapOfScreen
      EXTERNAL XDefaultColormapOfScreen
C       Screen_ptr screen

      Display_ptr  XDisplayOfScreen
      EXTERNAL XDisplayOfScreen
C       Screen_ptr screen

      Screen_ptr  XScreenOfDisplay
      EXTERNAL XScreenOfDisplay
C       Display_ptr display
C       INTEGER screen_number

      Screen_ptr  XDefaultScreenOfDisplay
      EXTERNAL XDefaultScreenOfDisplay
C       Display_ptr display

      INTEGER  XEventMaskOfScreen
      EXTERNAL XEventMaskOfScreen
C       Screen_ptr screen

      INTEGER  XScreenNumberOfScreen
      EXTERNAL XScreenNumberOfScreen
C       Screen_ptr screen

C  WARNING, this type not in Xlib spec 
#define XErrorHandler INTEGER
      XErrorHandler  XSetErrorHandler
      EXTERNAL XSetErrorHandler
C       XErrorHandler handler

C  WARNING, this type not in Xlib spec 
#define XIOErrorHandler INTEGER
      XIOErrorHandler  XSetIOErrorHandler
      EXTERNAL XSetIOErrorHandler
C       XIOErrorHandler handler

      XPixmapFormatValues_ptr  XListPixmapFormats
      EXTERNAL XListPixmapFormats
C       Display_ptr display
C       INTEGER count_return

      INTEGER MaxDepthListSize
      PARAMETER (MaxDepthListSize = 100)
#define XDepthList(__v) INTEGER __v ( 0 : 99 )
#define XDepthList_ptr INTEGER*4
      XDepthList_ptr  XListDepths
      EXTERNAL XListDepths
C       Display_ptr display
C       INTEGER screen_number
C       INTEGER count_return

C  ICCCM routines for things that don't require special include files; 
C  other declarations are given in Xutil.h                             
      Status  XReconfigureWMWindow
      EXTERNAL XReconfigureWMWindow
C       Display_ptr display
C       Window w
C       INTEGER screen_number
C       INTEGER mask
C       RECORD /XWindowChanges/ changes

      INTEGER MaxAtomListSize
      PARAMETER (MaxAtomListSize = 100)
#define AtomList(__v) Atom __v ( 0 : 99 )
#define AtomList_ptr INTEGER*4
      Status  XGetWMProtocols
      EXTERNAL XGetWMProtocols
C       Display_ptr display
C       Window w
C       AtomList_ptr protocols_return
C       INTEGER count_return

      Status  XSetWMProtocols
      EXTERNAL XSetWMProtocols
C       Display_ptr display
C       Window w
C       AtomList (protocols)
C       INTEGER count

      Status  XIconifyWindow
      EXTERNAL XIconifyWindow
C       Display_ptr display
C       Window w
C       INTEGER screen_number

      Status  XWithdrawWindow
      EXTERNAL XWithdrawWindow
C       Display_ptr display
C       Window w
C       INTEGER screen_number

      INTEGER MaxArgListSize
      PARAMETER (MaxArgListSize = 100)
#define ArgList(__v) Char_ptr __v ( 0 : 99 )
#define ArgList_ptr INTEGER*4
      Status  XGetCommand
      EXTERNAL XGetCommand
C       Display_ptr display
C       Window w
C       ArgList_ptr argv_return
C       INTEGER argc_return

      INTEGER MaxWindowList
      PARAMETER (MaxWindowList = 100)
#define WindowList(__v) Window __v ( 0 : 100 )
#define WindowList_ptr INTEGER*4
      Status  XGetWMColormapWindows
      EXTERNAL XGetWMColormapWindows
C       Display_ptr display
C       Window w
C       WindowList_ptr windows_return
C       INTEGER count_return

      Status  XSetWMColormapWindows
      EXTERNAL XSetWMColormapWindows
C       Display_ptr display
C       Window w
C       WindowList (colormap_windows)
C       INTEGER count

      EXTERNAL XFreeStringList
C       Char_ptr list

      EXTERNAL XSetTransientForHint
C       Display_ptr display
C       Window w
C       Window prop_window

C  The following are given in alphabetical order 
      EXTERNAL XActivateScreenSaver
C       Display_ptr display

      EXTERNAL XAddHost
C       Display_ptr display
C       XHostAddress_ptr host

      EXTERNAL XAddHosts
C       Display_ptr display
C       XHostAddress_ptr hosts
C       INTEGER num_hosts

      EXTERNAL XAddToExtensionList
C       XExtData_ptr structure
C       XExtData_ptr ext_data

      EXTERNAL XAddToSaveSet
C       Display_ptr display
C       Window w

      Status  XAllocColor
      EXTERNAL XAllocColor
C       Display_ptr display
C       Colormap colormap
C       RECORD /XColor/ screen_in_out

      Status  XAllocColorCells
      EXTERNAL XAllocColorCells
C       Display_ptr display
C       Colormap colormap
C       Bool contig
C       INTEGER plane_masks_return
C       INTEGER nplanes
C       INTEGER pixels_return
C       INTEGER npixels

      Status  XAllocColorPlanes
      EXTERNAL XAllocColorPlanes
C       Display_ptr display
C       Colormap colormap
C       Bool contig
C       INTEGER pixels_return
C       INTEGER ncolors
C       INTEGER nreds
C       INTEGER ngreens
C       INTEGER nblues
C       INTEGER rmask_return
C       INTEGER gmask_return
C       INTEGER bmask_return

      Status  XAllocNamedColor
      EXTERNAL XAllocNamedColor
C       Display_ptr display
C       Colormap colormap
C       CHARACTER color_name
C       RECORD /XColor/ screen_def_return
C       RECORD /XColor/ exact_def_return

      EXTERNAL XAllowEvents
C       Display_ptr display
C       INTEGER event_mode
C       Time time

      EXTERNAL XAutoRepeatOff
C       Display_ptr display

      EXTERNAL XAutoRepeatOn
C       Display_ptr display

      EXTERNAL XBell
C       Display_ptr display
C       INTEGER percent

      INTEGER  XBitmapBitOrder
      EXTERNAL XBitmapBitOrder
C       Display_ptr display

      INTEGER  XBitmapPad
      EXTERNAL XBitmapPad
C       Display_ptr display

      INTEGER  XBitmapUnit
      EXTERNAL XBitmapUnit
C       Display_ptr display

      INTEGER  XCellsOfScreen
      EXTERNAL XCellsOfScreen
C       Screen_ptr screen

      EXTERNAL XChangeActivePointerGrab
C       Display_ptr display
C       INTEGER event_mask
C       Cursor cursor
C       Time time

      EXTERNAL XChangeGC
C       Display_ptr display
C       GC gc
C       INTEGER valuemask
C       XGCValues_ptr values

      EXTERNAL XChangeKeyboardControl
C       Display_ptr display
C       INTEGER value_mask
C       RECORD /XKeyboardControl/ values

      EXTERNAL XChangeKeyboardMapping
C       Display_ptr display
C       INTEGER first_keycode
C       INTEGER keysyms_per_keycode
C       KeySym keysyms
C       INTEGER num_codes

      EXTERNAL XChangePointerControl
C       Display_ptr display
C       Bool do_accel
C       Bool do_threshold
C       INTEGER accel_numerator
C       INTEGER accel_denominator
C       INTEGER threshold

      EXTERNAL XChangeProperty
C       Display_ptr display
C       Window w
C       Atom property
C       Atom type
C       INTEGER format
C       INTEGER mode
C       Byte data
C       INTEGER nelements

      EXTERNAL XChangeSaveSet
C       Display_ptr display
C       Window w
C       INTEGER change_mode

      EXTERNAL XChangeWindowAttributes
C       Display_ptr display
C       Window w
C       INTEGER valuemask
C       RECORD /XSetWindowAttributes/ attributes

#define XCheckIfEventPredicate INTEGER
      Bool  XCheckIfEvent
      EXTERNAL XCheckIfEvent
C       Display_ptr display
C       RECORD /XEvent/ event_return
C       XCheckIfEventPredicate predicate
C       XPointer arg

      Bool  XCheckMaskEvent
      EXTERNAL XCheckMaskEvent
C       Display_ptr display
C       INTEGER event_mask
C       RECORD /XEvent/ event_return

      Bool  XCheckTypedEvent
      EXTERNAL XCheckTypedEvent
C       Display_ptr display
C       INTEGER event_type
C       RECORD /XEvent/ event_return

      Bool  XCheckTypedWindowEvent
      EXTERNAL XCheckTypedWindowEvent
C       Display_ptr display
C       Window w
C       INTEGER event_type
C       RECORD /XEvent/ event_return

      Bool  XCheckWindowEvent
      EXTERNAL XCheckWindowEvent
C       Display_ptr display
C       Window w
C       INTEGER event_mask
C       RECORD /XEvent/ event_return

      EXTERNAL XCirculateSubwindows
C       Display_ptr display
C       Window w
C       INTEGER direction

      EXTERNAL XCirculateSubwindowsDown
C       Display_ptr display
C       Window w

      EXTERNAL XCirculateSubwindowsUp
C       Display_ptr display
C       Window w

      EXTERNAL XClearArea
C       Display_ptr display
C       Window w
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       Bool exposures

      EXTERNAL XClearWindow
C       Display_ptr display
C       Window w

      EXTERNAL XCloseDisplay
C       Display_ptr display

      EXTERNAL XConfigureWindow
C       Display_ptr display
C       Window w
C       INTEGER value_mask
C       RECORD /XWindowChanges/ values

      INTEGER  XConnectionNumber
      EXTERNAL XConnectionNumber
C       Display_ptr display

      EXTERNAL XConvertSelection
C       Display_ptr display
C       Atom selection
C       Atom target
C       Atom property
C       Window requestor
C       Time time

      EXTERNAL XCopyArea
C       Display_ptr display
C       Drawable src
C       Drawable dest
C       GC gc
C       INTEGER src_x
C       INTEGER src_y
C       INTEGER width
C       INTEGER height
C       INTEGER dest_x
C       INTEGER dest_y

      EXTERNAL XCopyGC
C       Display_ptr display
C       GC src
C       INTEGER valuemask
C       GC dest

      EXTERNAL XCopyPlane
C       Display_ptr display
C       Drawable src
C       Drawable dest
C       GC gc
C       INTEGER src_x
C       INTEGER src_y
C       INTEGER width
C       INTEGER height
C       INTEGER dest_x
C       INTEGER dest_y
C       INTEGER plane

      INTEGER  XDefaultDepth
      EXTERNAL XDefaultDepth
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XDefaultDepthOfScreen
      EXTERNAL XDefaultDepthOfScreen
C       Screen_ptr screen

      INTEGER  XDefaultScreen
      EXTERNAL XDefaultScreen
C       Display_ptr display

      EXTERNAL XDefineCursor
C       Display_ptr display
C       Window w
C       Cursor cursor

      EXTERNAL XDeleteProperty
C       Display_ptr display
C       Window w
C       Atom property

      EXTERNAL XDestroyWindow
C       Display_ptr display
C       Window w

      EXTERNAL XDestroySubwindows
C       Display_ptr display
C       Window w

      INTEGER  XDoesBackingStore
      EXTERNAL XDoesBackingStore
C       Screen_ptr screen

      Bool  XDoesSaveUnders
      EXTERNAL XDoesSaveUnders
C       Screen_ptr screen

      EXTERNAL XDisableAccessControl
C       Display_ptr display

      INTEGER  XDisplayCells
      EXTERNAL XDisplayCells
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XDisplayHeight
      EXTERNAL XDisplayHeight
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XDisplayHeightMM
      EXTERNAL XDisplayHeightMM
C       Display_ptr display
C       INTEGER screen_number

      EXTERNAL XDisplayKeycodes
C       Display_ptr display
C       INTEGER min_keycodes_return
C       INTEGER max_keycodes_return

      INTEGER  XDisplayPlanes
      EXTERNAL XDisplayPlanes
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XDisplayWidth
      EXTERNAL XDisplayWidth
C       Display_ptr display
C       INTEGER screen_number

      INTEGER  XDisplayWidthMM
      EXTERNAL XDisplayWidthMM
C       Display_ptr display
C       INTEGER screen_number

      EXTERNAL XDrawArc
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER angle1
C       INTEGER angle2

      EXTERNAL XDrawArcs
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XArc/ arcs
C       INTEGER narcs

      EXTERNAL XDrawImageString
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       CHARACTER string
C       INTEGER length

      EXTERNAL XDrawImageString16
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XChar2b/ string
C       INTEGER length

      EXTERNAL XDrawLine
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x1
C       INTEGER y1
C       INTEGER x2
C       INTEGER y2

      EXTERNAL XDrawLines
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XPoint/ points
C       INTEGER npoints
C       INTEGER mode

      EXTERNAL XDrawPoint
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y

      EXTERNAL XDrawPoints
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XPoint/ points
C       INTEGER npoints
C       INTEGER mode

      EXTERNAL XDrawRectangle
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height

      EXTERNAL XDrawRectangles
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XRectangle/ rectangles
C       INTEGER nrectangles

      EXTERNAL XDrawSegments
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XSegment/ segments
C       INTEGER nsegments

      EXTERNAL XDrawString
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       CHARACTER string
C       INTEGER length

      EXTERNAL XDrawString16
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XChar2b/ string
C       INTEGER length

      EXTERNAL XDrawText
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XTextItem/ items
C       INTEGER nitems

      EXTERNAL XDrawText16
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XTextItem16/ items
C       INTEGER nitems

      EXTERNAL XEnableAccessControl
C       Display_ptr display

      INTEGER  XEventsQueued
      EXTERNAL XEventsQueued
C       Display_ptr display
C       INTEGER mode

      Status  XFetchName
      EXTERNAL XFetchName
C       Display_ptr display
C       Window w
C       Char_ptr window_name_return

      EXTERNAL XFillArc
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height
C       INTEGER angle1
C       INTEGER angle2

      EXTERNAL XFillArcs
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XArc/ arcs
C       INTEGER narcs

      EXTERNAL XFillPolygon
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XPoint/ points
C       INTEGER npoints
C       INTEGER shape
C       INTEGER mode

      EXTERNAL XFillRectangle
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height

      EXTERNAL XFillRectangles
C       Display_ptr display
C       Drawable d
C       GC gc
C       RECORD /XRectangle/ rectangles
C       INTEGER nrectangles

      EXTERNAL XFlush
C       Display_ptr display

      EXTERNAL XForceScreenSaver
C       Display_ptr display
C       INTEGER mode

      EXTERNAL XFree
C       INTEGER data

      EXTERNAL XFreeColormap
C       Display_ptr display
C       Colormap colormap

      EXTERNAL XFreeColors
C       Display_ptr display
C       Colormap colormap
C       INTEGER pixels
C       INTEGER npixels
C       INTEGER planes

      EXTERNAL XFreeCursor
C       Display_ptr display
C       Cursor cursor

      EXTERNAL XFreeExtensionList
C       ExtensionList_ptr list

      EXTERNAL XFreeFont
C       Display_ptr display
C       XFontStruct_ptr font_struct

      EXTERNAL XFreeFontInfo
C       FontList_ptr names
C       XFontStruct_ptr free_info
C       INTEGER actual_count

      EXTERNAL XFreeFontNames
C       FontList_ptr list

      EXTERNAL XFreeFontPath
C       FontPathList_ptr list

      EXTERNAL XFreeGC
C       Display_ptr display
C       GC gc

      EXTERNAL XFreeModifiermap
C       XModifierKeymap_ptr modmap

      EXTERNAL XFreePixmap
C       Display_ptr display
C       Pixmap pixmap

      INTEGER  XGeometry
      EXTERNAL XGeometry
C       Display_ptr display
C       INTEGER screen
C       CHARACTER user_geom
C       CHARACTER default_geom
C       INTEGER bwidth
C       INTEGER fwidth
C       INTEGER fheight
C       INTEGER xadder
C       INTEGER yadder
C       INTEGER x_return
C       INTEGER y_return
C       INTEGER width_return
C       INTEGER height_return

      EXTERNAL XGetErrorDatabaseText
C       Display_ptr display
C       CHARACTER name
C       CHARACTER message
C       CHARACTER default_string
C       CHARACTER buffer_return
C       INTEGER length

      EXTERNAL XGetErrorText
C       Display_ptr display
C       INTEGER code
C       CHARACTER buffer_return
C       INTEGER length

      Bool  XGetFontProperty
      EXTERNAL XGetFontProperty
C       XFontStruct_ptr font_struct
C       Atom atom
C       INTEGER value_return

      Status  XGetGCValues
      EXTERNAL XGetGCValues
C       Display_ptr display
C       GC gc
C       INTEGER valuemask
C       RECORD /XGCValues/ values_return

      Status  XGetGeometry
      EXTERNAL XGetGeometry
C       Display_ptr display
C       Drawable d
C       Window root_return
C       INTEGER x_return
C       INTEGER y_return
C       INTEGER width_return
C       INTEGER height_return
C       INTEGER border_width_return
C       INTEGER depth_return

      Status  XGetIconName
      EXTERNAL XGetIconName
C       Display_ptr display
C       Window w
C       Char_ptr icon_name_return

      EXTERNAL XGetInputFocus
C       Display_ptr display
C       Window focus_return
C       INTEGER revert_to_return

      EXTERNAL XGetKeyboardControl
C       Display_ptr display
C       RECORD /XKeyboardState/ values_return

      EXTERNAL XGetPointerControl
C       Display_ptr display
C       INTEGER accel_numerator_return
C       INTEGER accel_denominator_return
C       INTEGER threshold_return

      INTEGER  XGetPointerMapping
      EXTERNAL XGetPointerMapping
C       Display_ptr display
C       Byte map_return
C       INTEGER nmap

      EXTERNAL XGetScreenSaver
C       Display_ptr display
C       INTEGER timeout_return
C       INTEGER interval_return
C       INTEGER prefer_blanking_return
C       INTEGER allow_exposures_return

      Status  XGetTransientForHint
      EXTERNAL XGetTransientForHint
C       Display_ptr display
C       Window w
C       Window prop_window_return

      INTEGER PropDataMax
      PARAMETER (PropDataMax = 1000)
#define PropData(__v) Byte __v ( 0 : 1000 )
#define PropData_ptr INTEGER*4
      INTEGER  XGetWindowProperty
      EXTERNAL XGetWindowProperty
C       Display_ptr display
C       Window w
C       Atom property
C       INTEGER long_offset
C       INTEGER long_length
C       Bool delete
C       Atom req_type
C       Atom actual_type_return
C       INTEGER actual_format_return
C       INTEGER nitems_return
C       INTEGER bytes_after_return
C       PropData_ptr prop_return

      Status  XGetWindowAttributes
      EXTERNAL XGetWindowAttributes
C       Display_ptr display
C       Window w
C       RECORD /XWindowAttributes/ window_attributes_return

      EXTERNAL XGrabButton
C       Display_ptr display
C       INTEGER button
C       INTEGER modifiers
C       Window grab_window
C       Bool owner_events
C       INTEGER event_mask
C       INTEGER pointer_mode
C       INTEGER keyboard_mode
C       Window confine_to
C       Cursor cursor

      EXTERNAL XGrabKey
C       Display_ptr display
C       INTEGER keycode
C       INTEGER modifiers
C       Window grab_window
C       Bool owner_events
C       INTEGER pointer_mode
C       INTEGER keyboard_mode

      INTEGER  XGrabKeyboard
      EXTERNAL XGrabKeyboard
C       Display_ptr display
C       Window grab_window
C       Bool owner_events
C       INTEGER pointer_mode
C       INTEGER keyboard_mode
C       Time time

      INTEGER  XGrabPointer
      EXTERNAL XGrabPointer
C       Display_ptr display
C       Window grab_window
C       Bool owner_events
C       INTEGER event_mask
C       INTEGER pointer_mode
C       INTEGER keyboard_mode
C       Window confine_to
C       Cursor cursor
C       Time time

      EXTERNAL XGrabServer
C       Display_ptr display

C ** undocumented routines
C PROCEDURE XHeightMMOfScreen(
C     screen : Screen_ptr
C ) : INTEGER;
C 
C PROCEDURE XHeightOfScreen(
C     screen : Screen_ptr
C ) : INTEGER;
C **
#define XIfEventPredicate INTEGER
      EXTERNAL XIfEvent
C       Display_ptr display
C       RECORD /XEvent/ event_return
C       XIfEventPredicate predicate
C       XPointer arg

      INTEGER  XImageByteOrder
      EXTERNAL XImageByteOrder
C       Display_ptr display

      EXTERNAL XInstallColormap
C       Display_ptr display
C       Colormap colormap

      KeyCode  XKeysymToKeycode
      EXTERNAL XKeysymToKeycode
C       Display_ptr display
C       KeySym keysym

      EXTERNAL XKillClient
C       Display_ptr display
C       XID resource

C ** undocumented routine
C PROCEDURE XLastKnownRequestProcessed(
C     display : Display_ptr
C ) : CARDINAL;
C **
      Status  XLookupColor
      EXTERNAL XLookupColor
C       Display_ptr display
C       Colormap colormap
C       CHARACTER color_name
C       RECORD /XColor/ exact_def_return
C       RECORD /XColor/ screen_def_return

      EXTERNAL XLowerWindow
C       Display_ptr display
C       Window w

      EXTERNAL XMapRaised
C       Display_ptr display
C       Window w

      EXTERNAL XMapSubwindows
C       Display_ptr display
C       Window w

      EXTERNAL XMapWindow
C       Display_ptr display
C       Window w

      EXTERNAL XMaskEvent
C       Display_ptr display
C       INTEGER event_mask
C       RECORD /XEvent/ event_return

C ** Undocumented routines
C PROCEDURE XMaxCmapsOfScreen(
C     screen : Screen_ptr
C ) : INTEGER;
C 
C PROCEDURE XMinCmapsOfScreen(
C     screen : Screen_ptr
C ) : INTEGER;
C **
      EXTERNAL XMoveResizeWindow
C       Display_ptr display
C       Window w
C       INTEGER x
C       INTEGER y
C       INTEGER width
C       INTEGER height

      EXTERNAL XMoveWindow
C       Display_ptr display
C       Window w
C       INTEGER x
C       INTEGER y

      EXTERNAL XNextEvent
C       Display_ptr display
C       RECORD /XEvent/ event_return

      EXTERNAL XNoOp
C       Display_ptr display

      Status  XParseColor
      EXTERNAL XParseColor
C       Display_ptr display
C       Colormap colormap
C       CHARACTER spec
C       RECORD /XColor/ exact_def_return

      INTEGER  XParseGeometry
      EXTERNAL XParseGeometry
C       CHARACTER parsestring
C       INTEGER x_return
C       INTEGER y_return
C       INTEGER width_return
C       INTEGER height_return

      EXTERNAL XPeekEvent
C       Display_ptr display
C       RECORD /XEvent/ event_return

#define XPeekIfEventPredicate INTEGER
      EXTERNAL XPeekIfEvent
C       Display_ptr display
C       RECORD /XEvent/ event_return
C       XPeekIfEventPredicate predicate
C       XPointer arg

      INTEGER  XPending
      EXTERNAL XPending
C       Display_ptr display

C ** undocumented routines
C PROCEDURE XPlanesOfScreen(
C     screen : Screen_ptr
C ) : INTEGER;
C 
C PROCEDURE XProtocolRevision(
C     display : Display_ptr;
C ) : INTEGER;
C 
C PROCEDURE XProtocolVersion(
C     display : Display_ptr;
C ) : INTEGER;
C 
C **
      EXTERNAL XPutBackEvent
C       Display_ptr display
C       RECORD /XEvent/ event

      EXTERNAL XPutImage
C       Display_ptr display
C       Drawable d
C       GC gc
C       XImage_ptr image
C       INTEGER src_x
C       INTEGER src_y
C       INTEGER dest_x
C       INTEGER dest_y
C       INTEGER width
C       INTEGER height

      INTEGER  XQLength
      EXTERNAL XQLength
C       Display_ptr display

      Status  XQueryBestCursor
      EXTERNAL XQueryBestCursor
C       Display_ptr display
C       Drawable d
C       INTEGER width
C       INTEGER height
C       INTEGER width_return
C       INTEGER height_return

      Status  XQueryBestSize
      EXTERNAL XQueryBestSize
C       Display_ptr display
C       INTEGER class
C       Drawable which_screen
C       INTEGER width
C       INTEGER height
C       INTEGER width_return
C       INTEGER height_return

      Status  XQueryBestStipple
      EXTERNAL XQueryBestStipple
C       Display_ptr display
C       Drawable which_screen
C       INTEGER width
C       INTEGER height
C       INTEGER width_return
C       INTEGER height_return

      Status  XQueryBestTile
      EXTERNAL XQueryBestTile
C       Display_ptr display
C       Drawable which_screen
C       INTEGER width
C       INTEGER height
C       INTEGER width_return
C       INTEGER height_return

      EXTERNAL XQueryColor
C       Display_ptr display
C       Colormap colormap
C       RECORD /XColor/ def_in_out

      EXTERNAL XQueryColors
C       Display_ptr display
C       Colormap colormap
C       RECORD /XColor/ defs_in_out
C       INTEGER ncolors

      Bool  XQueryExtension
      EXTERNAL XQueryExtension
C       Display_ptr display
C       CHARACTER name
C       INTEGER major_opcode_return
C       INTEGER first_event_return
C       INTEGER first_error_return

#define XQueryKeymapKeys(__v) Byte __v ( 1 : 32 )
      EXTERNAL XQueryKeymap
C       Display_ptr display
C       XQueryKeymapKeys (keys_return)

      Bool  XQueryPointer
      EXTERNAL XQueryPointer
C       Display_ptr display
C       Window w
C       Window root_return
C       Window child_return
C       INTEGER root_x_return
C       INTEGER root_y_return
C       INTEGER win_x_return
C       INTEGER win_y_return
C       INTEGER mask_return

      EXTERNAL XQueryTextExtents
C       Display_ptr display
C       XID font_ID
C       CHARACTER string
C       INTEGER nchars
C       INTEGER direction_return
C       INTEGER font_ascent_return
C       INTEGER font_descent_return
C       RECORD /XCharStruct/ overall_return

      EXTERNAL XQueryTextExtents16
C       Display_ptr display
C       XID font_ID
C       RECORD /XChar2b/ string
C       INTEGER nchars
C       INTEGER direction_return
C       INTEGER font_ascent_return
C       INTEGER font_descent_return
C       RECORD /XCharStruct/ overall_return

      INTEGER Max_children
      PARAMETER (Max_children = 1000)
#define Children_list(__v) Window __v ( 1 : 1000 )
#define Children_list_ptr INTEGER*4
      Status  XQueryTree
      EXTERNAL XQueryTree
C       Display_ptr display
C       Window w
C       Window root_return
C       Window parent_return
C       Children_list_ptr children_return
C       INTEGER nchildren_return

      EXTERNAL XRaiseWindow
C       Display_ptr display
C       Window w

      INTEGER  XReadBitmapFile
      EXTERNAL XReadBitmapFile
C       Display_ptr display
C       Drawable d
C       CHARACTER filename
C       INTEGER width_return
C       INTEGER height_return
C       Pixmap bitmap_return
C       INTEGER x_hot_return
C       INTEGER y_hot_return

      EXTERNAL XRebindKeysym
C       Display_ptr display
C       KeySym keysym
C       KeySym list
C       INTEGER mod_count
C       CHARACTER string
C       INTEGER bytes_string

      EXTERNAL XRecolorCursor
C       Display_ptr display
C       Cursor cursor
C       RECORD /XColor/ foreground_color
C       RECORD /XColor/ background_color

      EXTERNAL XRefreshKeyboardMapping
C       RECORD /XMappingEvent/ event_map

      EXTERNAL XRemoveFromSaveSet
C       Display_ptr display
C       Window w

      EXTERNAL XRemoveHost
C       Display_ptr display
C       XHostAddress_ptr host

      EXTERNAL XRemoveHosts
C       Display_ptr display
C       RECORD /XHostAddress/ hosts
C       INTEGER num_hosts

      EXTERNAL XReparentWindow
C       Display_ptr display
C       Window w
C       Window parent
C       INTEGER x
C       INTEGER y

      EXTERNAL XResetScreenSaver
C       Display_ptr display

      EXTERNAL XResizeWindow
C       Display_ptr display
C       Window w
C       INTEGER width
C       INTEGER height

      EXTERNAL XRestackWindows
C       Display_ptr display
C       Window windows
C       INTEGER nwindows

      EXTERNAL XRotateBuffers
C       Display_ptr display
C       INTEGER rotate

      EXTERNAL XRotateWindowProperties
C       Display_ptr display
C       Window w
C       Atom properties
C       INTEGER num_prop
C       INTEGER npositions

C *** undocumented routine
C PROCEDURE INTEGER XScreenCount(
C     display : Display_ptr;
C );
C ***
      EXTERNAL XSelectInput
C       Display_ptr display
C       Window w
C       INTEGER event_mask

      Status  XSendEvent
      EXTERNAL XSendEvent
C       Display_ptr display
C       Window w
C       Bool propagate
C       INTEGER event_mask
C       RECORD /XEvent/ event_send

      EXTERNAL XSetAccessControl
C       Display_ptr display
C       INTEGER mode

      EXTERNAL XSetArcMode
C       Display_ptr display
C       GC gc
C       INTEGER arc_mode

      EXTERNAL XSetBackground
C       Display_ptr display
C       GC gc
C       INTEGER background

      EXTERNAL XSetClipMask
C       Display_ptr display
C       GC gc
C       Pixmap pixmap

      EXTERNAL XSetClipOrigin
C       Display_ptr display
C       GC gc
C       INTEGER clip_x_origin
C       INTEGER clip_y_origin

      EXTERNAL XSetClipRectangles
C       Display_ptr display
C       GC gc
C       INTEGER clip_x_origin
C       INTEGER clip_y_origin
C       RECORD /XRectangle/ rectangles
C       INTEGER nrect
C       INTEGER ordering

      EXTERNAL XSetCloseDownMode
C       Display_ptr display
C       INTEGER close_mode

      EXTERNAL XSetCommand
C       Display_ptr display
C       Window w
C       ArgList (argv)
C       INTEGER argc

      EXTERNAL XSetDashes
C       Display_ptr display
C       GC gc
C       INTEGER dash_offset
C       CHARACTER dash_list
C       INTEGER n

      EXTERNAL XSetFillRule
C       Display_ptr display
C       GC gc
C       INTEGER fill_rule

      EXTERNAL XSetFillStyle
C       Display_ptr display
C       GC gc
C       INTEGER fill_style

      EXTERNAL XSetFont
C       Display_ptr display
C       GC gc
C       Font font

      EXTERNAL XSetFontPath
C       Display_ptr display
C       FontPathList_ptr list
C       INTEGER ndirs

      EXTERNAL XSetForeground
C       Display_ptr display
C       GC gc
C       INTEGER foreground

      EXTERNAL XSetFunction
C       Display_ptr display
C       GC gc
C       INTEGER function

      EXTERNAL XSetGraphicsExposures
C       Display_ptr display
C       GC gc
C       Bool graphics_exposures

      EXTERNAL XSetIconName
C       Display_ptr display
C       Window w
C       CHARACTER icon_name

      EXTERNAL XSetInputFocus
C       Display_ptr display
C       Window focus
C       INTEGER revert_to
C       Time time

      EXTERNAL XSetLineAttributes
C       Display_ptr display
C       GC gc
C       INTEGER line_width
C       INTEGER line_style
C       INTEGER cap_style
C       INTEGER join_style

      INTEGER  XSetModifierMapping
      EXTERNAL XSetModifierMapping
C       Display_ptr display
C       XModifierKeymap_ptr modmap

      EXTERNAL XSetPlaneMask
C       Display_ptr display
C       GC gc
C       INTEGER plane_mask

      INTEGER  XSetPointerMapping
      EXTERNAL XSetPointerMapping
C       Display_ptr display
C       Byte map
C       INTEGER nmap

      EXTERNAL XSetScreenSaver
C       Display_ptr display
C       INTEGER timeout
C       INTEGER interval
C       INTEGER prefer_blanking
C       INTEGER allow_exposures

      EXTERNAL XSetSelectionOwner
C       Display_ptr display
C       Atom selection
C       Window owner
C       Time time

      EXTERNAL XSetState
C       Display_ptr display
C       GC gc
C       INTEGER foreground
C       INTEGER background
C       INTEGER function
C       INTEGER plane_mask

      EXTERNAL XSetStipple
C       Display_ptr display
C       GC gc
C       Pixmap stipple

      EXTERNAL XSetSubwindowMode
C       Display_ptr display
C       GC gc
C       INTEGER subwindow_mode

      EXTERNAL XSetTSOrigin
C       Display_ptr display
C       GC gc
C       INTEGER ts_x_origin
C       INTEGER ts_y_origin

      EXTERNAL XSetTile
C       Display_ptr display
C       GC gc
C       Pixmap tile

      EXTERNAL XSetWindowBackground
C       Display_ptr display
C       Window w
C       INTEGER background_pixel

      EXTERNAL XSetWindowBackgroundPixmap
C       Display_ptr display
C       Window w
C       Pixmap background_pixmap

      EXTERNAL XSetWindowBorder
C       Display_ptr display
C       Window w
C       INTEGER border_pixel

      EXTERNAL XSetWindowBorderPixmap
C       Display_ptr display
C       Window w
C       Pixmap border_pixmap

      EXTERNAL XSetWindowBorderWidth
C       Display_ptr display
C       Window w
C       INTEGER width

      EXTERNAL XSetWindowColormap
C       Display_ptr display
C       Window w
C       Colormap colormap

      EXTERNAL XStoreBuffer
C       Display_ptr display
C       CHARACTER bytes
C       INTEGER nbytes
C       INTEGER buffer

      EXTERNAL XStoreBytes
C       Display_ptr display
C       CHARACTER bytes
C       INTEGER nbytes

      EXTERNAL XStoreColor
C       Display_ptr display
C       Colormap colormap
C       RECORD /XColor/ color

      EXTERNAL XStoreColors
C       Display_ptr display
C       Colormap colormap
C       RECORD /XColor/ color
C       INTEGER ncolors

      EXTERNAL XStoreName
C       Display_ptr display
C       Window w
C       CHARACTER window_name

      EXTERNAL XStoreNamedColor
C       Display_ptr display
C       Colormap colormap
C       CHARACTER colorname
C       INTEGER pixel
C       INTEGER flags

      EXTERNAL XSync
C       Display_ptr display
C       Bool discard

      EXTERNAL XTextExtents
C       XFontStruct_ptr font_struct
C       CHARACTER string
C       INTEGER nchars
C       INTEGER direction_return
C       INTEGER font_ascent_return
C       INTEGER font_descent_return
C       RECORD /XCharStruct/ overall_return

      EXTERNAL XTextExtents16
C       XFontStruct_ptr font_struct
C       RECORD /XChar2b/ string
C       INTEGER nchars
C       INTEGER direction_return
C       INTEGER font_ascent_return
C       INTEGER font_descent_return
C       RECORD /XCharStruct/ overall_return

      INTEGER  XTextWidth
      EXTERNAL XTextWidth
C       XFontStruct_ptr font_struct
C       CHARACTER string
C       INTEGER count

      INTEGER  XTextWidth16
      EXTERNAL XTextWidth16
C       XFontStruct_ptr font_struct
C       RECORD /XChar2b/ string
C       INTEGER count

      Bool  XTranslateCoordinates
      EXTERNAL XTranslateCoordinates
C       Display_ptr display
C       Window src_w
C       Window dest_w
C       INTEGER src_x
C       INTEGER src_y
C       INTEGER dest_x_return
C       INTEGER dest_y_return
C       Window child_return

      EXTERNAL XUndefineCursor
C       Display_ptr display
C       Window w

      EXTERNAL XUngrabButton
C       Display_ptr display
C       INTEGER button
C       INTEGER modifiers
C       Window grab_window

      EXTERNAL XUngrabKey
C       Display_ptr display
C       INTEGER keycode
C       INTEGER modifiers
C       Window grab_window

      EXTERNAL XUngrabKeyboard
C       Display_ptr display
C       Time time

      EXTERNAL XUngrabPointer
C       Display_ptr display
C       Time time

      EXTERNAL XUngrabServer
C       Display_ptr display

      EXTERNAL XUninstallColormap
C       Display_ptr display
C       Colormap colormap

      EXTERNAL XUnloadFont
C       Display_ptr display
C       Font font

      EXTERNAL XUnmapSubwindows
C       Display_ptr display
C       Window w

      EXTERNAL XUnmapWindow
C       Display_ptr display
C       Window w

      INTEGER  XVendorRelease
      EXTERNAL XVendorRelease
C       Display_ptr display

      EXTERNAL XWarpPointer
C       Display_ptr display
C       Window src_w
C       Window dest_w
C       INTEGER src_x
C       INTEGER src_y
C       INTEGER src_width
C       INTEGER src_height
C       INTEGER dest_x
C       INTEGER dest_y

      INTEGER  XWidthMMOfScreen
      EXTERNAL XWidthMMOfScreen
C       Screen_ptr screen

      INTEGER  XWidthOfScreen
      EXTERNAL XWidthOfScreen
C       Screen_ptr screen

      EXTERNAL XWindowEvent
C       Display_ptr display
C       Window w
C       INTEGER event_mask
C       RECORD /XEvent/ event_return

      INTEGER  XWriteBitmapFile
      EXTERNAL XWriteBitmapFile
C       Display_ptr display
C       CHARACTER filename
C       Pixmap bitmap
C       INTEGER width
C       INTEGER height
C       INTEGER x_hot
C       INTEGER y_hot

      Bool  XSupportsLocale
      EXTERNAL XSupportsLocale
      Char_ptr  XSetLocaleModifiers
      EXTERNAL XSetLocaleModifiers
C       Char_ptr modifier_list

      INTEGER MaxCharsetListSize
      PARAMETER (MaxCharsetListSize = 100)
#define CharsetList(__v) Char_ptr __v ( 0 : 99 )
#define CharsetList_ptr INTEGER*4
      XFontSet  XCreateFontSet
      EXTERNAL XCreateFontSet
C       Display_ptr display
C       CHARACTER base_font_name_list
C       CharsetList_ptr missing_charset_list_return
C       INTEGER missing_charset_count_return
C       Char_ptr def_string_return

      EXTERNAL XFreeFontSet
C       Display_ptr display
C       XFontSet font_set

      INTEGER MaxXFontStructList
      PARAMETER (MaxXFontStructList = 100)
#define XFontStruct_ptrList(__v) XFontStruct_ptr __v ( 0 : 100 )
#define XFontStruct_ptrList_ptr INTEGER*4
      INTEGER MaxFontNameListSize
      PARAMETER (MaxFontNameListSize = 100)
#define FontNameList(__v) Char_ptr __v ( 0 : 99 )
#define FontNameList_ptr INTEGER*4
      INTEGER  XFontsOfFontSet
      EXTERNAL XFontsOfFontSet
C       XFontSet font_set
C       XFontStruct_ptrList_ptr font_struct_list
C       FontNameList_ptr font_name_list

      Char_ptr  XBaseFontNameListOfFontSet
      EXTERNAL XBaseFontNameListOfFontSet
C       XFontSet font_set

      Char_ptr  XLocaleOfFontSet
      EXTERNAL XLocaleOfFontSet
C       XFontSet font_set

      Bool  XContextDependentDrawing
      EXTERNAL XContextDependentDrawing
C       XFontSet font_set

      XFontSetExtents_ptr  XExtentsOfFontSet
      EXTERNAL XExtentsOfFontSet
C       XFontSet font_set

      INTEGER  XmbTextEscapement
      EXTERNAL XmbTextEscapement
C       XFontSet font_set
C       CHARACTER text
C       INTEGER bytes_text

      INTEGER  XwcTextEscapement
      EXTERNAL XwcTextEscapement
C       XFontSet font_set
C       wchar_t text
C       INTEGER num_wchars

      INTEGER  XmbTextExtents
      EXTERNAL XmbTextExtents
C       XFontSet font_set
C       CHARACTER text
C       INTEGER bytes_text
C       RECORD /XRectangle/ overall_ink_return
C       RECORD /XRectangle/ overall_logical_return

      INTEGER  XwcTextExtents
      EXTERNAL XwcTextExtents
C       XFontSet font_set
C       wchar_t text
C       INTEGER num_wchars
C       RECORD /XRectangle/ overall_ink_return
C       RECORD /XRectangle/ overall_logical_return

      Status  XmbTextPerCharExtents
      EXTERNAL XmbTextPerCharExtents
C       XFontSet font_set
C       CHARACTER text
C       INTEGER bytes_text
C       RECORD /XRectangle/ ink_extents_buffer
C       RECORD /XRectangle/ logical_extents_buffer
C       INTEGER buffer_size
C       INTEGER num_chars
C       RECORD /XRectangle/ overall_ink_return
C       RECORD /XRectangle/ overall_logical_return

      Status  XwcTextPerCharExtents
      EXTERNAL XwcTextPerCharExtents
C       XFontSet font_set
C       wchar_t text
C       INTEGER num_wchars
C       RECORD /XRectangle/ ink_extents_buffer
C       RECORD /XRectangle/ logical_extents_buffer
C       INTEGER buffer_size
C       INTEGER num_chars
C       RECORD /XRectangle/ overall_ink_return
C       RECORD /XRectangle/ overall_logical_return

      EXTERNAL XmbDrawText
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XmbTextItem/ text_items
C       INTEGER nitems

      EXTERNAL XwcDrawText
C       Display_ptr display
C       Drawable d
C       GC gc
C       INTEGER x
C       INTEGER y
C       RECORD /XmbTextItem/ text_items
C       INTEGER nitems

      EXTERNAL XmbDrawString
C       Display_ptr display
C       Drawable d
C       XFontSet font_set
C       GC gc
C       INTEGER x
C       INTEGER y
C       CHARACTER text
C       INTEGER bytes_text

      EXTERNAL XwcDrawString
C       Display_ptr display
C       Drawable d
C       XFontSet font_set
C       GC gc
C       INTEGER x
C       INTEGER y
C       wchar_t text
C       INTEGER num_wchars

      EXTERNAL XmbDrawImageString
C       Display_ptr display
C       Drawable d
C       XFontSet font_set
C       GC gc
C       INTEGER x
C       INTEGER y
C       CHARACTER text
C       INTEGER bytes_text

      EXTERNAL XwcDrawImageString
C       Display_ptr display
C       Drawable d
C       XFontSet font_set
C       GC gc
C       INTEGER x
C       INTEGER y
C       wchar_t text
C       INTEGER num_wchars

#define XrmHashBucketRec_ptr INTEGER
      XIM  XOpenIM
      EXTERNAL XOpenIM
C       Display_ptr dpy
C       XrmHashBucketRec_ptr rdb
C       CHARACTER res_name
C       CHARACTER res_class

      Status  XCloseIM
      EXTERNAL XCloseIM
C       XIM im

      Char_ptr  XGetIMValues
      EXTERNAL XGetIMValues
C       XIM im
C       CHARACTER argument
C       INTEGER value

      Display_ptr  XDisplayOfIM
      EXTERNAL XDisplayOfIM
C       XIM im

      Char_ptr  XLocaleOfIM
      EXTERNAL XLocaleOfIM
C       XIM im

      XIC  XCreateIC
      EXTERNAL XCreateIC
C       XIM im
C       CHARACTER argument
C       INTEGER value

      EXTERNAL XDestroyIC
C       XIC ic

      EXTERNAL XSetICFocus
C       XIC ic

      EXTERNAL XUnsetICFocus
C       XIC ic

      Wchar_ptr  XwcResetIC
      EXTERNAL XwcResetIC
C       XIC ic

      Char_ptr  XmbResetIC
      EXTERNAL XmbResetIC
C       XIC ic

      Char_ptr  XSetICValues
      EXTERNAL XSetICValues
C       XIC ic
C       CHARACTER argument
C       INTEGER value

      Char_ptr  XGetICValues
      EXTERNAL XGetICValues
C       XIC ic
C       CHARACTER argument
C       INTEGER value

      XIM  XIMOfIC
      EXTERNAL XIMOfIC
C       XIC ic

      Bool  XFilterEvent
      EXTERNAL XFilterEvent
C       RECORD /XEvent/ event
C       Window window

      INTEGER  XmbLookupString
      EXTERNAL XmbLookupString
C       XIC ic
C       XKeyPressedEvent event
C       CHARACTER buffer_return
C       INTEGER bytes_buffer
C       KeySym keysym_return
C       Status status_return

      INTEGER  XwcLookupString
      EXTERNAL XwcLookupString
C       XIC ic
C       XKeyPressedEvent event
C       wchar_t buffer_return
C       INTEGER wchars_buffer
C       KeySym keysym_return
C       Status status_return

      XVaNestedList  XVaCreateNestedList1
      EXTERNAL XVaCreateNestedList1
C       CHARACTER arg1
C       INTEGER val1

      XVaNestedList  XVaCreateNestedList2
      EXTERNAL XVaCreateNestedList2
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2

      XVaNestedList  XVaCreateNestedList3
      EXTERNAL XVaCreateNestedList3
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3

      XVaNestedList  XVaCreateNestedList4
      EXTERNAL XVaCreateNestedList4
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4

      XVaNestedList  XVaCreateNestedList5
      EXTERNAL XVaCreateNestedList5
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5

      XVaNestedList  XVaCreateNestedList6
      EXTERNAL XVaCreateNestedList6
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5
C       CHARACTER arg6
C       INTEGER val6

      XVaNestedList  XVaCreateNestedList7
      EXTERNAL XVaCreateNestedList7
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5
C       CHARACTER arg6
C       INTEGER val6
C       CHARACTER arg7
C       INTEGER val7

      XVaNestedList  XVaCreateNestedList8
      EXTERNAL XVaCreateNestedList8
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5
C       CHARACTER arg6
C       INTEGER val6
C       CHARACTER arg7
C       INTEGER val7
C       CHARACTER arg8
C       INTEGER val8

      XVaNestedList  XVaCreateNestedList9
      EXTERNAL XVaCreateNestedList9
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5
C       CHARACTER arg6
C       INTEGER val6
C       CHARACTER arg7
C       INTEGER val7
C       CHARACTER arg8
C       INTEGER val8
C       CHARACTER arg9
C       INTEGER val9

      XVaNestedList  XVaCreateNestedList10
      EXTERNAL XVaCreateNestedList10
C       CHARACTER arg1
C       INTEGER val1
C       CHARACTER arg2
C       INTEGER val2
C       CHARACTER arg3
C       INTEGER val3
C       CHARACTER arg4
C       INTEGER val4
C       CHARACTER arg5
C       INTEGER val5
C       CHARACTER arg6
C       INTEGER val6
C       CHARACTER arg7
C       INTEGER val7
C       CHARACTER arg8
C       INTEGER val8
C       CHARACTER arg9
C       INTEGER val9
C       CHARACTER arg10
C       INTEGER val10

#endif Xlib_F.

#ifndef expandname_F_DEFINED
#define expandname_F_DEFINED

C   derived from @(#)expandname.h 20.12 91/05/28 SMI 
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
      INTEGER NONAMES
      PARAMETER (NONAMES = 0)
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structs
C  ***********************************************************************
C  
      STRUCTURE /Namelist/
          INTEGER  count
          Char_ptr names (0 : 1)
      
          END STRUCTURE
C  LIE --it's actually names[count]
C 				 * followed by the strings themselves
C 			 
#define Namelist_ptr INTEGER*4
C 
C  *
C  *  xv_expand_name returns a pointer to a struct namelist containing
C  *	the client's shell's idea of what its argument expands to.
C  *	If str contains no shell metacharacters, the shell is never
C  *	consulted, and the argument string is simply returned
C  *	in a singleton namelist.
C  * 
C  *	In case of errors, xv_expand_name() writes an error message to
C  *	stderr, and returns NONAMES.
C  *
C  *  free_namelist
C  *	The struct namelist is dynamically allocated, and should be
C  *	freed after use by a call to free_namelist().
C  
      Namelist_ptr  xv_expand_name
      EXTERNAL xv_expand_name
C       Char_ptr str

      EXTERNAL free_namelist
C       Namelist_ptr nl

#endif expandname_F.

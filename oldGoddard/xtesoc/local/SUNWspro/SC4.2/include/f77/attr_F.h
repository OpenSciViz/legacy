#ifndef attr_F_DEFINED
#define attr_F_DEFINED

C 	Derived from @(#)attr.h 20.42 91/06/17
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "base_F.h"
C 
C  *  The base type space potentially runs from 0 to 127 inclusive.  This is
C  * subdivided as follows:
C  *	[0..32)	[ATTR_BASE_UNUSED_FIRST..ATTR_BASE_UNUSED_LAST]
C  *		Reserved for non-Sun packages.
C  *     [32..64)	Reserved for future use.
C  *    [64..128)	[ATTR_BASE_FIRST..ATTR_BASE_LAST] is currently used by
C  *		Sun packages
C  *		(ATTR_BASE_LAST..128) is for future use by Sun packages.
C  
      INTEGER ATTR_BASE_UNUSED_FIRST
      PARAMETER (ATTR_BASE_UNUSED_FIRST = 0)
      INTEGER ATTR_BASE_UNUSED_LAST
      PARAMETER (ATTR_BASE_UNUSED_LAST = 31)
      INTEGER ATTR_BASE_FIRST
      PARAMETER (ATTR_BASE_FIRST = 64)
      INTEGER ATTR_PKG_UNUSED_FIRST
      PARAMETER (ATTR_PKG_UNUSED_FIRST = 1)
      INTEGER ATTR_PKG_UNUSED_LAST
      PARAMETER (ATTR_PKG_UNUSED_LAST = 31)
      INTEGER ATTR_PKG_FIRST
      PARAMETER (ATTR_PKG_FIRST = 64)
C 
C  * Values from the Attr_pkg enumeration which are required for some
C  * of the following #defines. This allows those defines to remain in this
C  * section rather than below the enumeration.
C  
      INTEGER ATTR_PKG_ZERO_VALUE
      PARAMETER (ATTR_PKG_ZERO_VALUE = 0)
      INTEGER ATTR_PKG_GENERIC_VALUE
      PARAMETER (ATTR_PKG_GENERIC_VALUE = ATTR_PKG_FIRST)
      INTEGER ATTR_PKG_LAST_VALUE
      PARAMETER (ATTR_PKG_LAST_VALUE = ATTR_PKG_FIRST+14)
C 
C  * ATTR_STANDARD_SIZE is large enough to allow for 
C  * most attribute-value lists.
C  
      INTEGER ATTR_STANDARD_SIZE
      PARAMETER (ATTR_STANDARD_SIZE = 250)
      INTEGER ATTR_CU_TAG
      PARAMETER (ATTR_CU_TAG = X'80000000')
      INTEGER ATTR_PIXEL_OFFSET
      PARAMETER (ATTR_PIXEL_OFFSET = X'8000')
      INTEGER ATTR_CU_MASK
      PARAMETER (ATTR_CU_MASK = X'C0000000')
C 
C  ***********************************************************************
C  *			Typedefs, enumerations, and structs
C  ***********************************************************************
C  
C 
C  * Attr_avlist is not an array of Attr_attributes, because it is an array
C  * of intermixed attributes and values.
C  
#define Attr_avlist INTEGER*4
#define Attr_attribute INTEGER
C 
C  * Enumerations 
C  
      INTEGER ATTR_CU_POSITION
      PARAMETER (ATTR_CU_POSITION = 0)
C  bit 29 is off 
      INTEGER ATTR_CU_LENGTH
      PARAMETER (ATTR_CU_LENGTH = X'20000000')
C  bit 29 is on 
#define Attr_cu_type INTEGER
      INTEGER ATTR_LIST_IS_INLINE,ATTR_LIST_IS_PTR
      PARAMETER ( ATTR_LIST_IS_INLINE=0,  ATTR_LIST_IS_PTR=1)
#define Attr_list_ptr_type INTEGER*4
      INTEGER ATTR_NONE,ATTR_RECURSIVE,ATTR_NULL,ATTR_COUNTED
      PARAMETER ( ATTR_NONE=0,  ATTR_RECURSIVE=1,  ATTR_NULL=2, 
     & ATTR_COUNTED=3)
#define Attr_list_type INTEGER*4
C 
C  * NOTE: The base type numbers have to be EXACTLY the same as SunView1 in order
C  *	  to support cut and paste between SunView1 and XView windows.
C  *	  Nothing changes!
C  
      INTEGER ATTR_BASE_NO_VALUE
      PARAMETER (ATTR_BASE_NO_VALUE = ATTR_BASE_FIRST+17)
C 
C 	 * Fundamental C types. 
C 	 
      INTEGER ATTR_BASE_INT
      PARAMETER (ATTR_BASE_INT = ATTR_BASE_FIRST)
      INTEGER ATTR_BASE_LONG
      PARAMETER (ATTR_BASE_LONG = ATTR_BASE_FIRST+24)
      INTEGER ATTR_BASE_SHORT
      PARAMETER (ATTR_BASE_SHORT = ATTR_BASE_FIRST+25)
      INTEGER ATTR_BASE_ENUM
      PARAMETER (ATTR_BASE_ENUM = ATTR_BASE_FIRST+9)
      INTEGER ATTR_BASE_CHAR
      PARAMETER (ATTR_BASE_CHAR = ATTR_BASE_FIRST+10)
      INTEGER ATTR_BASE_STRING
      PARAMETER (ATTR_BASE_STRING = ATTR_BASE_FIRST+11)
      INTEGER ATTR_BASE_FUNCTION_PTR
      PARAMETER (ATTR_BASE_FUNCTION_PTR = ATTR_BASE_FIRST+19)
C 
C 	 * Derivative C types. 
C 	 
      INTEGER ATTR_BASE_BOOLEAN
      PARAMETER (ATTR_BASE_BOOLEAN = ATTR_BASE_FIRST+8)
      INTEGER ATTR_BASE_OPAQUE
      PARAMETER (ATTR_BASE_OPAQUE = ATTR_BASE_FIRST+16)
C 
C 	 * Special coordinate types; look in attr_cu.c for the details. 
C 	 
      INTEGER ATTR_BASE_X
      PARAMETER (ATTR_BASE_X = ATTR_BASE_FIRST+2)
      INTEGER ATTR_BASE_INDEX_X
      PARAMETER (ATTR_BASE_INDEX_X = ATTR_BASE_FIRST+3)
      INTEGER ATTR_BASE_Y
      PARAMETER (ATTR_BASE_Y = ATTR_BASE_FIRST+4)
      INTEGER ATTR_BASE_INDEX_Y
      PARAMETER (ATTR_BASE_INDEX_Y = ATTR_BASE_FIRST+5)
      INTEGER ATTR_BASE_XY
      PARAMETER (ATTR_BASE_XY = ATTR_BASE_FIRST+6)
      INTEGER ATTR_BASE_INDEX_XY
      PARAMETER (ATTR_BASE_INDEX_XY = ATTR_BASE_FIRST+7)
C 
C 	 * Pointer types. 
C 	 
      INTEGER ATTR_BASE_PIXRECT_PTR
      PARAMETER (ATTR_BASE_PIXRECT_PTR = ATTR_BASE_FIRST+12)
      INTEGER ATTR_BASE_PIXFONT_PTR
      PARAMETER (ATTR_BASE_PIXFONT_PTR = ATTR_BASE_FIRST+13)
      INTEGER ATTR_BASE_RECT_PTR
      PARAMETER (ATTR_BASE_RECT_PTR = ATTR_BASE_FIRST+15)
      INTEGER ATTR_BASE_AV
      PARAMETER (ATTR_BASE_AV = ATTR_BASE_FIRST+18)
      INTEGER ATTR_BASE_ICON_PTR
      PARAMETER (ATTR_BASE_ICON_PTR = ATTR_BASE_FIRST+20)
      INTEGER ATTR_BASE_SINGLE_COLOR_PTR
      PARAMETER (ATTR_BASE_SINGLE_COLOR_PTR = ATTR_BASE_FIRST+21)
      INTEGER ATTR_BASE_CURSOR_PTR
      PARAMETER (ATTR_BASE_CURSOR_PTR = ATTR_BASE_FIRST+22)
      INTEGER ATTR_BASE_LAST
      PARAMETER (ATTR_BASE_LAST = ATTR_BASE_FIRST+25)
#define Attr_base_type Attr_attribute
C  Clients of the attribute value package should use
C  * Attr_base_cardinality elements to define the base type
C  * and cardinality of their attributes.
C  
      INTEGER ATTR_NO_VALUE
      PARAMETER (ATTR_NO_VALUE = 2592)
      INTEGER ATTR_INT
      PARAMETER (ATTR_INT = 2049)
      INTEGER ATTR_INT_PAIR
      PARAMETER (ATTR_INT_PAIR = 2050)
      INTEGER ATTR_INT_TRIPLE
      PARAMETER (ATTR_INT_TRIPLE = 2051)
      INTEGER ATTR_LONG
      PARAMETER (ATTR_LONG = 2817)
      INTEGER ATTR_SHORT
      PARAMETER (ATTR_SHORT = 2849)
      INTEGER ATTR_ENUM
      PARAMETER (ATTR_ENUM = 2337)
      INTEGER ATTR_CHAR
      PARAMETER (ATTR_CHAR = 2369)
      INTEGER ATTR_STRING
      PARAMETER (ATTR_STRING = 2401)
      INTEGER ATTR_FUNCTION_PTR
      PARAMETER (ATTR_FUNCTION_PTR = 2657)
      INTEGER ATTR_BOOLEAN
      PARAMETER (ATTR_BOOLEAN = 2305)
      INTEGER ATTR_OPAQUE
      PARAMETER (ATTR_OPAQUE = 2561)
      INTEGER ATTR_OPAQUE_PAIR
      PARAMETER (ATTR_OPAQUE_PAIR = 2562)
      INTEGER ATTR_OPAQUE_TRIPLE
      PARAMETER (ATTR_OPAQUE_TRIPLE = 2563)
      INTEGER ATTR_X
      PARAMETER (ATTR_X = 2113)
      INTEGER ATTR_INDEX_X
      PARAMETER (ATTR_INDEX_X = 2146)
      INTEGER ATTR_Y
      PARAMETER (ATTR_Y = 2177)
      INTEGER ATTR_INDEX_Y
      PARAMETER (ATTR_INDEX_Y = 2210)
      INTEGER ATTR_XY
      PARAMETER (ATTR_XY = 2242)
      INTEGER ATTR_INDEX_XY
      PARAMETER (ATTR_INDEX_XY = 2275)
      INTEGER ATTR_PIXRECT_PTR
      PARAMETER (ATTR_PIXRECT_PTR = 2433)
      INTEGER ATTR_PIXFONT_PTR
      PARAMETER (ATTR_PIXFONT_PTR = 2465)
      INTEGER ATTR_RECT_PTR
      PARAMETER (ATTR_RECT_PTR = 2529)
      INTEGER ATTR_AV
      PARAMETER (ATTR_AV = 2625)
      INTEGER ATTR_ICON_PTR
      PARAMETER (ATTR_ICON_PTR = 2689)
      INTEGER ATTR_SINGLE_COLOR_PTR
      PARAMETER (ATTR_SINGLE_COLOR_PTR = 2721)
      INTEGER ATTR_CURSOR_PTR
      PARAMETER (ATTR_CURSOR_PTR = 2753)
#define Attr_base_cardinality Attr_attribute
C 
C  * For Sunview 1 compatibility
C  
      INTEGER ATTR_PIXWIN_PTR
      PARAMETER (ATTR_PIXWIN_PTR = ATTR_OPAQUE)
C 
C  *  The package id space potentially runs from 0 to 255 inclusive.  This is
C  * subdivided as follows:
C  *	     0	NEVER a valid package id.
C  *	[1..32)	[ATTR_PKG_UNUSED_FIRST..ATTR_PKG_UNUSED_LAST]
C  *		Reserved for non-Sun packages.
C  *     [32..64)	Reserved for future use.
C  *    [64..128)	[ATTR_PKG_FIRST..ATTR_PKG_LAST] is currently used by
C  *		Sun packages
C  *		(ATTR_PKG_LAST..128) is for future use by Sun packages.
C  *   [128..256)	Reserved for future use.
C  
      INTEGER ATTR_PKG_ZERO
      PARAMETER (ATTR_PKG_ZERO = ATTR_PKG_ZERO_VALUE)
      INTEGER ATTR_PKG_GENERIC
      PARAMETER (ATTR_PKG_GENERIC = ATTR_PKG_GENERIC_VALUE)
      INTEGER ATTR_PKG_CURSOR
      PARAMETER (ATTR_PKG_CURSOR = ATTR_PKG_FIRST+1)
      INTEGER ATTR_PKG_DRAWABLE
      PARAMETER (ATTR_PKG_DRAWABLE = ATTR_PKG_FIRST+2)
      INTEGER ATTR_PKG_FONT
      PARAMETER (ATTR_PKG_FONT = ATTR_PKG_FIRST+3)
      INTEGER ATTR_PKG_IMAGE
      PARAMETER (ATTR_PKG_IMAGE = ATTR_PKG_FIRST+4)
      INTEGER ATTR_PKG_SERVER_IMAGE
      PARAMETER (ATTR_PKG_SERVER_IMAGE = ATTR_PKG_FIRST+5)
      INTEGER ATTR_PKG_SCREEN
      PARAMETER (ATTR_PKG_SCREEN = ATTR_PKG_FIRST+6)
      INTEGER ATTR_PKG_SELN_BASE
      PARAMETER (ATTR_PKG_SELN_BASE = 71)
C  ATTR_PKG_FIRST +  7 
C  ATTR_PKG_SELN_BASE must be 71, as it is known to 3.X and 4.X code.
C 	 * In fact, the layout of the bits in an attribute is known, and also
C 	 * cannot change without breaking communication between SunView 1 and
C 	 * SunView 2 selections.
C 	 
      INTEGER ATTR_PKG_SERVER
      PARAMETER (ATTR_PKG_SERVER = ATTR_PKG_FIRST+8)
      INTEGER ATTR_PKG_WIN
      PARAMETER (ATTR_PKG_WIN = ATTR_PKG_FIRST+9)
      INTEGER ATTR_PKG_SV
      PARAMETER (ATTR_PKG_SV = ATTR_PKG_FIRST+10)
      INTEGER ATTR_PKG_FULLSCREEN
      PARAMETER (ATTR_PKG_FULLSCREEN = ATTR_PKG_FIRST+11)
      INTEGER ATTR_PKG_ERROR
      PARAMETER (ATTR_PKG_ERROR = ATTR_PKG_FIRST+12)
      INTEGER ATTR_PKG_CMS
      PARAMETER (ATTR_PKG_CMS = ATTR_PKG_FIRST+13)
      INTEGER ATTR_PKG_DND
      PARAMETER (ATTR_PKG_DND = ATTR_PKG_FIRST+14)
      INTEGER ATTR_PKG_LAST
      PARAMETER (ATTR_PKG_LAST = ATTR_PKG_LAST_VALUE)
C  
C 	 * Change ATTR_PKG_LAST_VALUE to be EQUAL to the last legal pkg id.
C 	 * The procedure counter(), called by attr_make, aborts if 
C 	 * PKG_ID > ATTR_PKG_LAST 
C 	 * PKG name should also be added to attr_names[] in attr.c 
C 	 
C  REMIND: ATTR_PKG_SELECTION should be ATTR_PKG_FIRST+15 put this
C                will cause the pkg values for the OL pkgs to change.  This
C                would break binary compatibility.  So we put the new intrinsic
C                pkg after the OL pkgs.  When we can again break binary
C                compatibility, we should change this and add some space
C                between the intrinsic pkgs and the OL pkgs.
C                Remove comment in attrol.h when this is fixed.
C      
      INTEGER ATTR_PKG_SELECTION
      PARAMETER (ATTR_PKG_SELECTION = ATTR_PKG_LAST_VALUE+20)
#define Attr_pkg INTEGER
C 
C  * Generic attributes: ATTR_PKG_GENERIC is shared with
C  * generic.h [64..128).
C  
      INTEGER ATTR_LIST
      PARAMETER (ATTR_LIST = 1073768992)
      INTEGER ATTR_NOP0
      PARAMETER (ATTR_NOP0 = 1074792992)
      INTEGER ATTR_NOP1
      PARAMETER (ATTR_NOP1 = 1074858497)
      INTEGER ATTR_NOP2
      PARAMETER (ATTR_NOP2 = 1074924034)
      INTEGER ATTR_NOP3
      PARAMETER (ATTR_NOP3 = 1074989571)
      INTEGER ATTR_NOP4
      PARAMETER (ATTR_NOP4 = 1075055108)
#define Attr_generic Attr_attribute
C 
C  * Structs 
C  
C ** bit-fields NOT IMPLEMENTED,  use ATTR_ procedures to get values
C 		struct {
C 			Attr_pkg		pkg	: 8;
C 			unsigned		ordinal	: 8;
C 			Attr_list_type		list_type: 2;
C 			Attr_list_ptr_type	list_ptr_type: 1;
C 			unsigned		consumed: 1;
C 			Attr_base_type		base_type: 7;
C 			unsigned		spare1	: 1;	
C 			unsigned		cardinality: 4;
C     		} 			info;
C 	**
      STRUCTURE /Attr_union/
          Attr_attribute code
          END STRUCTURE
#define Attr_union_ptr INTEGER*4
C 
C  ***********************************************************************
C  *			Globals
C  ***********************************************************************
C  
C 
C  * Public Functions
C  
      Attr_avlist  attr_create_list_1
      EXTERNAL attr_create_list_1
C       Attr_attribute av1

      Attr_avlist  attr_create_list_2
      EXTERNAL attr_create_list_2
C       Attr_attribute av1
C       Attr_attribute av2

      Attr_avlist  attr_create_list_3
      EXTERNAL attr_create_list_3
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3

      Attr_avlist  attr_create_list_4
      EXTERNAL attr_create_list_4
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4

      Attr_avlist  attr_create_list_5
      EXTERNAL attr_create_list_5
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5

      Attr_avlist  attr_create_list_6
      EXTERNAL attr_create_list_6
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6

      Attr_avlist  attr_create_list_7
      EXTERNAL attr_create_list_7
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7

      Attr_avlist  attr_create_list_8
      EXTERNAL attr_create_list_8
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8

      Attr_avlist  attr_create_list_9
      EXTERNAL attr_create_list_9
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9

      Attr_avlist  attr_create_list_10
      EXTERNAL attr_create_list_10
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10

      Attr_avlist  attr_create_list_11
      EXTERNAL attr_create_list_11
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11

      Attr_avlist  attr_create_list_12
      EXTERNAL attr_create_list_12
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12

      Attr_avlist  attr_create_list_13
      EXTERNAL attr_create_list_13
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13

      Attr_avlist  attr_create_list_14
      EXTERNAL attr_create_list_14
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14

      Attr_avlist  attr_create_list_15
      EXTERNAL attr_create_list_15
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14
C       Attr_attribute av15

      Attr_avlist  attr_create_list_16
      EXTERNAL attr_create_list_16
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14
C       Attr_attribute av15
C       Attr_attribute av16

      Attr_avlist  attr_create_list_1s
      EXTERNAL attr_create_list_1s
C       Attr_attribute av1

      Attr_avlist  attr_create_list_2s
      EXTERNAL attr_create_list_2s
C       Attr_attribute av1
C       Attr_attribute av2

      Attr_avlist  attr_create_list_3s
      EXTERNAL attr_create_list_3s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3

      Attr_avlist  attr_create_list_4s
      EXTERNAL attr_create_list_4s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4

      Attr_avlist  attr_create_list_5s
      EXTERNAL attr_create_list_5s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5

      Attr_avlist  attr_create_list_6s
      EXTERNAL attr_create_list_6s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6

      Attr_avlist  attr_create_list_7s
      EXTERNAL attr_create_list_7s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7

      Attr_avlist  attr_create_list_8s
      EXTERNAL attr_create_list_8s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8

      Attr_avlist  attr_create_list_9s
      EXTERNAL attr_create_list_9s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9

      Attr_avlist  attr_create_list_10s
      EXTERNAL attr_create_list_10s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10

      Attr_avlist  attr_create_list_11s
      EXTERNAL attr_create_list_11s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11

      Attr_avlist  attr_create_list_12s
      EXTERNAL attr_create_list_12s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12

      Attr_avlist  attr_create_list_13s
      EXTERNAL attr_create_list_13s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13

      Attr_avlist  attr_create_list_14s
      EXTERNAL attr_create_list_14s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14

      Attr_avlist  attr_create_list_15s
      EXTERNAL attr_create_list_15s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14
C       Attr_attribute av15

      Attr_avlist  attr_create_list_16s
      EXTERNAL attr_create_list_16s
C       Attr_attribute av1
C       Attr_attribute av2
C       Attr_attribute av3
C       Attr_attribute av4
C       Attr_attribute av5
C       Attr_attribute av6
C       Attr_attribute av7
C       Attr_attribute av8
C       Attr_attribute av9
C       Attr_attribute av10
C       Attr_attribute av11
C       Attr_attribute av12
C       Attr_attribute av13
C       Attr_attribute av14
C       Attr_attribute av15
C       Attr_attribute av16

#endif attr_F.

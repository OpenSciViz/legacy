#ifndef pixrect_F_DEFINED
#define pixrect_F_DEFINED

C  derived from @(#)pixrect.h 1.50 89/07/10 SMI 
C 
C  * Copyright 1986, 1987 by Sun Microsystems, Inc.
C  
C 
C  * This file defines the programmer interface to the pixrect abstraction.
C  * A pixrect is a rectangular array of pixels on which a number of
C  * operations are defined.
C  *
C  * Each pixrect has as visible attributes its height and width in
C  * pixels and the number of bits stored for each pixel.  It also supports
C  * several operations.  The proper way to think of the operations is
C  * that they are messages sent to the pixrect.  The operations are:
C  *
C  *	pr_destroy	Destroy a pixrect.
C  *	pr_rop		Raster operation from another pixrect to the
C  *			destination pixrect.  The case where the source
C  *			and destination overlap is properly handled.
C  *	pr_stencil	Raster operation from source pixrect to the
C  *			dest pixrect using a stencil pixrect as a 'cookie
C  *			cutter' to perform a spatial write enable.
C  *	pr_batchrop	Like pr_rop, but source is an array of pixrects,
C  *			and an offset to be applied before each pixrect.
C  *			This is specifically designed for operations like
C  *			putting up text, which consists of a number of
C  *			characters from a font, each given by a pixrect.
C  *	pr_get		Get the value of a single pixel from a pixrect.
C  *	pr_put		Change a single pixel value in a pixrect.
C  *	pr_vector	Draw a vector in a pixrect
C  *	pr_region	Create a new pixrect which describes a rectangular
C  *			sub-region of an existing pixrect.
C  *	pr_putcolormap	Write a portion of the colormap.
C  *	pr_getcolormap	Read a portion of the colormap.
C  *	pr_putattributes Set the plane mask.
C  *	pr_getattributes Get the plane mask.
C  
C 
C  * There are a number of structures used in the arguments to pixrects:
C  *
C  *	struct pr_pos		A position within a pixrect is a pair of
C  *				integers giving the offset from the upper
C  *				left corner.  The pixels within a pixrect
C  *				are numbered with (0,0) at the upper left
C  *				and (width-1,height-1) at the lower right.
C  *	struct pr_prpos		A pixrect and a position within it.
C  *	struct pr_size		A pair of integers representing the
C  *				size of a rectangle within a pixrect.
C  *	struct pr_subregion	A pixrect, a position and a size,
C  *				specifying a rectangular sub-region.
C  
#define Pixrect_ptr INTEGER*4
#define Pixrectops_ptr INTEGER*4
      STRUCTURE /Pixrectops/
          ProcPointer pro_rop
          ProcPointer pro_stencil
          ProcPointer pro_batchrop
          ProcPointer pro_nop
          ProcPointer pro_destroy
          ProcPointer pro_get
          ProcPointer pro_put
          ProcPointer pro_vector
          ProcPointer pro_region
          ProcPointer pro_putcolormap
          ProcPointer pro_getcolormap
          ProcPointer pro_putattributes
          ProcPointer pro_getattributes
          END STRUCTURE
      STRUCTURE /Pr_size/
          INTEGER  x, y
          END STRUCTURE
      STRUCTURE /Pr_pos/
          INTEGER  x, y
          END STRUCTURE
#define Pr_pos_ptr INTEGER*4
      STRUCTURE /Pixrect/
          Pixrectops_ptr pr_ops
          RECORD /Pr_size/ pr_size
          INTEGER  pr_depth
          caddr_t pr_data
          END STRUCTURE
      STRUCTURE /Pr_prpos/
          Pixrect_ptr pr
          RECORD /Pr_pos/ pos
          END STRUCTURE
      STRUCTURE /Pr_subregion/
          Pixrect_ptr pr
          RECORD /Pr_pos/ pos
          RECORD /Pr_size/ size
          END STRUCTURE
C  structure used to specify a single color 
      STRUCTURE /singlecolor/
          Byte red, green, blue
          END STRUCTURE
C 
C  * Pr_product is used when doing multiplications involving pixrects,
C  * and casts its arguments to that the compiler will use 16 by 16 multiplies.
C  
      INTEGER*2  pr_product
      EXTERNAL pr_product
C       INTEGER*2 a
C       INTEGER*2 b

      EXTERNAL pr_rop
C       Pixrect_ptr dpr
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pr_putcolormap
C       Pixrect_ptr pr
C       INTEGER ind
C       INTEGER cnt
C       Byte red
C       Byte grn
C       Byte blu

      EXTERNAL pr_putattributes
C       Pixrect_ptr pr
C       INTEGER planes

      EXTERNAL pr_stencil
C       Pixrect_ptr dpr
C       INTEGER dx
C       INTEGER dy
C       INTEGER w
C       INTEGER h
C       INTEGER op
C       Pixrect_ptr stpr
C       INTEGER stx
C       INTEGER sty
C       Pixrect_ptr spr
C       INTEGER sx
C       INTEGER sy

      EXTERNAL pr_batchrop
C       Pixrect_ptr dpr
C       INTEGER x
C       INTEGER y
C       INTEGER op
C       RECORD /Pr_prpos/ sbp
C       INTEGER n

      EXTERNAL pr_destroy
C       Pixrect_ptr pr

      EXTERNAL pr_get
C       Pixrect_ptr pr
C       INTEGER x
C       INTEGER y

      EXTERNAL pr_put
C       Pixrect_ptr pr
C       INTEGER x
C       INTEGER y
C       INTEGER val

      EXTERNAL pr_vector
C       Pixrect_ptr pr
C       INTEGER x0
C       INTEGER y0
C       INTEGER x1
C       INTEGER y1
C       INTEGER op
C       INTEGER color

      Pixrect_ptr  pr_region
      EXTERNAL pr_region
C       Pixrect_ptr pr
C       INTEGER x
C       INTEGER y
C       INTEGER w
C       INTEGER h

      EXTERNAL pr_getcolormap
C       Pixrect_ptr pr
C       INTEGER ind
C       INTEGER cnt
C       Byte red
C       Byte grn
C       Byte blu

      EXTERNAL pr_getattributes
C       Pixrect_ptr pr
C       INTEGER planes

C 
C  * Several of the above operations return a common, distinguished value when
C  * an error arises.  That value is defined as follows:
C  
      INTEGER PIX_ERR
      PARAMETER (PIX_ERR = -1)
C 
C  * Operations.  The 'op' in 'rasterop' may be any binary Boolean function, 
C  * encoded as an integer from 0 to 15 (the op code) shifted left by one bit.  
C  * The function is applied per-pixel.  
C  *
C  * The following permit the op to be expressed as Boolean combinations
C  * of the two inputs 'src' and 'dst'.  Thus oring the source and destination
C  * together is written as PIX_SRC|PIX_DST, while xoring the source with the
C  * destination is PIX_SRC^PIX_DST.  Since ~op would set the color and clip
C  * bits, the macro PIX_NOT is provided for use in place of ~.
C  
      INTEGER PIX_SRC
      PARAMETER (PIX_SRC = 24)
      INTEGER PIX_DST
      PARAMETER (PIX_DST = 20)
      INTEGER PIX_CLR
      PARAMETER (PIX_CLR = 0)
      INTEGER PIX_SET
      PARAMETER (PIX_SET = 30)
      Byte  PIX_NOT
      EXTERNAL PIX_NOT
C       Byte op

C  macros which tell whether a rasterop needs SRC or DST values 
      LOGICAL  PIXOP_NEEDS_DST
      EXTERNAL PIXOP_NEEDS_DST
C       Byte op

      LOGICAL  PIXOP_NEEDS_SRC
      EXTERNAL PIXOP_NEEDS_SRC
C       Byte op

C  macros for encoding and extracting color field 
      Byte  PIX_COLOR
      EXTERNAL PIX_COLOR
C       Byte c

      Byte  PIX_OPCOLOR
      EXTERNAL PIX_OPCOLOR
C       Byte op

C 
C  * The pseudo-operation PIX_DONTCLIP specifies that clipping should not
C  * be performed.  PIX_CLIP is also provided, although unnecessary.
C  
      INTEGER PIX_DONTCLIP
      PARAMETER (PIX_DONTCLIP = 1)
      INTEGER PIX_CLIP
      PARAMETER (PIX_CLIP = 0)
C 
C  * The following structured definitions, all prefixed with prs_, correspond
C  * to the unstructured definitions above prefixed with pr_.
C  
      EXTERNAL prs_rop
C       RECORD /Pr_subregion/ dstreg
C       INTEGER op
C       RECORD /Pr_prpos/ srcprpos

      EXTERNAL prs_putcolormap
C       Pixrect_ptr pr
C       INTEGER ind
C       INTEGER cnt
C       Byte red
C       Byte grn
C       Byte blu

      EXTERNAL prs_putattributes
C       Pixrect_ptr pr
C       INTEGER planes

      EXTERNAL prs_stencil
C       RECORD /Pr_subregion/ dstreg
C       INTEGER op
C       RECORD /Pr_prpos/ stenprpos
C       RECORD /Pr_prpos/ srcprpos

      EXTERNAL prs_batchrop
C       RECORD /Pr_prpos/ dstprpos
C       INTEGER op
C       RECORD /Pr_prpos/ items
C       INTEGER n

      EXTERNAL prs_destroy
C       Pixrect_ptr pr

      EXTERNAL prs_get
C       RECORD /Pr_prpos/ srcprpos

      EXTERNAL prs_put
C       RECORD /Pr_prpos/ dstprpos
C       INTEGER val

      EXTERNAL prs_vector
C       Pixrect_ptr pr
C       RECORD /Pr_pos/ pos0
C       RECORD /Pr_pos/ pos1
C       INTEGER op
C       INTEGER color

      EXTERNAL prs_region
C       RECORD /Pr_subregion/ dstreg

      EXTERNAL prs_getcolormap
C       Pixrect_ptr pr
C       INTEGER ind
C       INTEGER cnt
C       Byte red
C       Byte grn
C       Byte blu

      EXTERNAL prs_getattributes
C       Pixrect_ptr pr
C       INTEGER planes

C  pr_close is a synonym for pr_destroy 
      EXTERNAL pr_close
C       Pixrect_ptr pr

C  these definitions come from pixfont.h 
      STRUCTURE /Pixchar/
          Pixrect_ptr pc_pr
          RECORD /Pr_pos/ pc_home
          RECORD /Pr_pos/ pc_adv
          END STRUCTURE
#define Pixfont_ptr INTEGER*4
      STRUCTURE /Pixfont/
          RECORD /Pr_size/ pf_defaultsize
          RECORD /Pixchar/ pf_char (0 : 255)
      
          END STRUCTURE
C  these definitions come from pixrect/memvar.h 
C  these definitions come from pixrect/pr_line.h 
      STRUCTURE /Pr_texture/
          INTEGER  pattern
          INTEGER*2  offset
          INTEGER  options
          INTEGER*2  res_polyoff
          INTEGER*2  res_oldpatln
          INTEGER*2  res_fatoff
          INTEGER  res_patfat
          INTEGER*2  res_numsegs
          END STRUCTURE
#define Pr_texture_ptr INTEGER*4
      STRUCTURE /Pr_brush/
          INTEGER  width
          END STRUCTURE
#define Pr_brush_ptr INTEGER*4
C  these definitions come from pixrect/traprop.h 
#define Pr_chain_ptr INTEGER*4
      STRUCTURE /Pr_chain/
          Pr_chain_ptr next
          RECORD /Pr_size/ size
          INTEGER  bits
          END STRUCTURE
      STRUCTURE /Pr_fall/
          RECORD /Pr_pos/ pos
          Pr_chain_ptr chain
          END STRUCTURE
#define Pr_fall_ptr INTEGER*4
      STRUCTURE /Pr_trap/
          Pr_fall_ptr left
          Pr_fall_ptr right
          INTEGER  y0, y1
          END STRUCTURE
#define pr_trap_ptr INTEGER*4
#endif pixrect_F.

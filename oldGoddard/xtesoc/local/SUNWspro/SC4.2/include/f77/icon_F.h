#ifndef icon_F_DEFINED
#define icon_F_DEFINED

C   derived from  @(#)icon.h 20.13 90/03/13 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "base_F.h"
#include "attr_F.h"
#include "attrol_F.h"
#include "generic_F.h"
#include "pkg_public_F.h"
#include "window_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, enumerations, and structures
C  ***********************************************************************
C  
#define Icon Xv_opaque
      INTEGER ICON_IMAGE
      PARAMETER (ICON_IMAGE = 1392839041)
      INTEGER ICON_IMAGE_RECT
      PARAMETER (ICON_IMAGE_RECT = 1393166817)
      INTEGER ICON_LABEL_RECT
      PARAMETER (ICON_LABEL_RECT = 1393494497)
      INTEGER ICON_TRANSPARENT
      PARAMETER (ICON_TRANSPARENT = 1393821953)
      INTEGER ICON_MASK_IMAGE
      PARAMETER (ICON_MASK_IMAGE = 1394149761)
      INTEGER ICON_TRANSPARENT_LABEL
      PARAMETER (ICON_TRANSPARENT_LABEL = 1394477409)
#define Icon_attribute Attr_attribute
#define Icon_attribute_ptr INTEGER*4
      STRUCTURE /Xv_icon/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      Xv_pkg_ptr ICON
      INTEGER ICON_TYPE
      PARAMETER (ICON_TYPE = ATTR_PKG_ICON)
      INTEGER ICON_WIDTH
      PARAMETER (ICON_WIDTH = XV_WIDTH)
      INTEGER ICON_HEIGHT
      PARAMETER (ICON_HEIGHT = XV_HEIGHT)
      INTEGER ICON_LABEL
      PARAMETER (ICON_LABEL = XV_LABEL)
      INTEGER ICON_FONT
      PARAMETER (ICON_FONT = XV_FONT)
      Icon_attribute_ptr  icon_attr_next
      EXTERNAL icon_attr_next
C       Xv_opaque attr

      INTEGER ICON_DEFAULT_WIDTH
      PARAMETER (ICON_DEFAULT_WIDTH = 64)
      INTEGER ICON_DEFAULT_HEIGHT
      PARAMETER (ICON_DEFAULT_HEIGHT = 64)
C 
C  * PUBLIC Functions 
C  * For Sunview 1 Compatibility
C  
      Xv_opaque  icon_create
      EXTERNAL icon_create
C       Attr_attribute attr1

      Xv_opaque  icon_get
      EXTERNAL icon_get
C       Icon icon_public
C       Attr_attribute attr

      INTEGER  icon_set
      EXTERNAL icon_set
C       Icon icon

      INTEGER  icon_destroy
      EXTERNAL icon_destroy
C       Icon icon

      EXTERNAL icon_display
C       Icon icon
C       INTEGER x
C       INTEGER y

      COMMON /icon_globals/   ICON
#endif icon_F.

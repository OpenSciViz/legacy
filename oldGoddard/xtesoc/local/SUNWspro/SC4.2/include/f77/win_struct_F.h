#ifndef win_struct_F_DEFINED
#define win_struct_F_DEFINED

C   derived from @(#)win_struct.h 20.10 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C  Host name maximum size is 31 + 1 for colon + 2 for display number
C  * + 10 for xid + 1 for null.
C  
      INTEGER WIN_NAMESIZE
      PARAMETER (WIN_NAMESIZE = 45)
C 
C  * Link names.
C  
      INTEGER WL_PARENT
      PARAMETER (WL_PARENT = 0)
      INTEGER WL_OLDERSIB
      PARAMETER (WL_OLDERSIB = 1)
      INTEGER WL_YOUNGERSIB
      PARAMETER (WL_YOUNGERSIB = 2)
      INTEGER WL_OLDESTCHILD
      PARAMETER (WL_OLDESTCHILD = 3)
      INTEGER WL_YOUNGESTCHILD
      PARAMETER (WL_YOUNGESTCHILD = 4)
      INTEGER WL_ENCLOSING
      PARAMETER (WL_ENCLOSING = WL_PARENT)
      INTEGER WL_COVERED
      PARAMETER (WL_COVERED = WL_OLDERSIB)
      INTEGER WL_COVERING
      PARAMETER (WL_COVERING = WL_YOUNGERSIB)
      INTEGER WL_BOTTOMCHILD
      PARAMETER (WL_BOTTOMCHILD = WL_OLDESTCHILD)
      INTEGER WL_TOPCHILD
      PARAMETER (WL_TOPCHILD = WL_YOUNGESTCHILD)
      INTEGER WIN_NULLLINK
      PARAMETER (WIN_NULLLINK = -1)
#endif win_struct_F.

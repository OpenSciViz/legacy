#ifndef defaults_F_DEFINED
#define defaults_F_DEFINED

C   derived from  @(#)defaults.h 20.19 91/03/27 SMI
C  *
C 
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents 
C  *	pending in the U.S. and foreign countries. See LEGAL_NOTICE 
C  *	file for terms of the license.
C 
C  
      INTEGER DEFAULTS_MAX_VALUE_SIZE
      PARAMETER (DEFAULTS_MAX_VALUE_SIZE = 128)
      STRUCTURE /Defaults_pairs/
          Char_ptr name
          INTEGER  value
          END STRUCTURE
C 
C  * Public Functions
C  *
C  *
C  * NOTE: Any returned string pointers should be considered temporary at best.
C  * If you want to hang onto the data, make your own private copy of the string!
C  
C 
C  * defaults_exists(name, class_name) will return TRUE if a values exists in
C  * the database for name, and class_name.
C  
      LOGICAL  defaults_exists
      EXTERNAL defaults_exists
C       Char_ptr path_name
C       Char_ptr class_name

C 
C  * defaults_get_boolean(name, class, deflt) will lookup name and class in
C  * the defaults database and return TRUE if the value is "True", "Yes", "On",
C  * "Enabled", "Set", "Activated", or "1".  FALSE will be returned if the
C  * value is "False", "No", "Off", "Disabled", "Reset", "Cleared",
C  * "Deactivated", or "0".  If the value is none of the above, a warning
C  * message will be displayed and Default will be returned.
C  
      LOGICAL  defaults_get_boolean
      EXTERNAL defaults_get_boolean
C       Char_ptr name
C       Char_ptr class
C       LOGICAL deflt

C 
C  * defaults_get_character(name, class, deflt) will lookup name and class in
C  * the defaults database and return the resulting character value.  Default
C  * will be returned if any error occurs.
C  
      CHARACTER  defaults_get_character
      EXTERNAL defaults_get_character
C       Char_ptr name
C       Char_ptr class
C       CHARACTER deflt

C 
C  * defaults_get_enum(name, class, pairs) will lookup the value associated
C  * with name and class, scan the Pairs table and return the associated value.
C  * If no match is found, an error is generated and the value associated with
C  * last entry (i.e. the NULL entry) is returned.
C  
      INTEGER  defaults_get_enum
      EXTERNAL defaults_get_enum
C       Char_ptr name
C       Char_ptr class
C       RECORD /Defaults_pairs/ pairs

C 
C  * defaults_get_integer(name, class, deflt) will lookup name and class in
C  * the defaults database and return the resulting integer value. Default will
C  * be returned if any error occurs.
C  
      INTEGER  defaults_get_integer
      EXTERNAL defaults_get_integer
C       Char_ptr name
C       Char_ptr class
C       INTEGER deflt

C 
C  * defaults_get_integer_check(name, class, deflt, mininum, maximum) will
C  * lookup name and class in the defaults database and return the resulting
C  * integer value. If the value in the database is not between Minimum and
C  * Maximum (inclusive), an error message will be printed.  Default will be
C  * returned if any error occurs.
C  
      INTEGER  defaults_get_integer_check
      EXTERNAL defaults_get_integer_check
C       Char_ptr name
C       Char_ptr class
C       INTEGER deflt
C       INTEGER minimum
C       INTEGER maximum

C 
C  * defaults_get_string(name, class, deflt) will lookup and return the
C  * null-terminated string value assocatied with name and class in the
C  * defaults database.  Default will be returned if any error occurs.
C  
      Char_ptr  defaults_get_string
      EXTERNAL defaults_get_string
C       Char_ptr name
C       Char_ptr class
C       Char_ptr deflt

C 
C  * defaults_init_db initializes the X11 Resource Manager.
C  
      EXTERNAL defaults_init_db
C 
C  * defaults_load_db(filename) will load the server database if filename is
C  * NULL, or the database residing in the specified filename.
C  
      EXTERNAL defaults_load_db
C       Char_ptr filename

C 
C  * defaults_store_db(filename) will write the defaults database to the
C  * specified file, and update the server Resource Manager property.
C  
      EXTERNAL defaults_store_db
C       Char_ptr filename

C 
C  * defaults_lookup(name, pairs) will linearly scan the Pairs data structure
C  * looking for Name.  The value associated with Name will be returned.
C  * If Name can not be found in Pairs, the value assoicated with NULL will
C  * be returned.  (The Pairs data structure must be terminated with NULL.)
C  
      INTEGER  defaults_lookup
      EXTERNAL defaults_lookup
C       Char_ptr name
C       RECORD /Defaults_pairs/ pairs

C 
C  * defaults_set_character(resource, value) will set the resource to
C  * value.  value is an character. resource is a string.
C  
      EXTERNAL defaults_set_character
C       Char_ptr resource
C       CHARACTER value

C 
C  * defaults_set_character(resource, value) will set the resource to
C  * value.  value is a integer. resource is a string.
C  
      EXTERNAL defaults_set_integer
C       Char_ptr resource
C       INTEGER value

C 
C  * defaults_set_character(resource, value) will set the resource to
C  * value.  value is a Boolean. resource is a string.
C  
      EXTERNAL defaults_set_boolean
C       Char_ptr resource
C       LOGICAL value

C 
C  * defaults_set_character(resource, value) will set the resource to
C  * value.  value is a string. resource is a string.
C  
      EXTERNAL defaults_set_string
C       Char_ptr resource
C       Char_ptr value

#endif defaults_F.

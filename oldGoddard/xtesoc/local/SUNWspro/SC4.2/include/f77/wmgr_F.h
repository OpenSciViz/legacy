#ifndef wmgr_F_DEFINED
#define wmgr_F_DEFINED

C   derived from  @(#)wmgr.h 20.22 91/11/04 SMI
C 
C  *
C 
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents 
C  *	pending in the U.S. and foreign countries. See LEGAL_NOTICE 
C  *	file for terms of the license.
C 
C  
C 
C  * This header file describes the interface to a window management mechanism.
C  * A menu interface to these functions is also provided.
C  * Typically, a tool window is responsible for window management.
C  
#include "frame_F.h"
#include "rect_F.h"
#include "window_F.h"
      INTEGER WMGR_ICONIC
      PARAMETER (WMGR_ICONIC = X'10')
C  Indicates window is iconic
C 					   in user flags of window 
      INTEGER WMGR_SUBFRAME
      PARAMETER (WMGR_SUBFRAME = X'40')
C  Indicates window is a sub-frame
C 					   in user flags of window 
      INTEGER WMGR_SETPOS
      PARAMETER (WMGR_SETPOS = -1)
C  Indicates "use default" in
C 					   wmgr_figure*rect calls	
      LOGICAL WMGR_NO_DELETE
      PARAMETER (WMGR_NO_DELETE = .FALSE.)
C  for XGetProperty call, indicate no deleting the existing property after
C  * a get
C  
      INTEGER WM_None,WM_N,WM_NE,WM_E,WM_SE,WM_S,WM_SW,WM_W,WM_NW
      PARAMETER ( WM_None=0,  WM_N=1,  WM_NE=2,  WM_E=3,  WM_SE=4, 
     & WM_S=5,  WM_SW=6,  WM_W=7,  WM_NW=8)
#define WM_Direction INTEGER*4
C 
C  * Basic window management operations.
C  * Move and stretch require user interaction.
C  
      EXTERNAL wmgr_open
C       Frame frame

      EXTERNAL wmgr_close
C       Frame frame

      EXTERNAL wmgr_top
C       Frame frame

      EXTERNAL wmgr_bottom
C       Frame frame

C 
C  * Exported by wmgr_rect.c:
C  
      EXTERNAL wmgr_completechangerect
C       Xv_window window
C       Rect_ptr newrect
C       Rect_ptr oldrect
C       INTEGER parentprleft
C       INTEGER parentprtop

      EXTERNAL wmgr_refreshwindow
C       Xv_window window

C 
C  * The following routines are exported by wmgr_policy.c;  they implement
C  * the default positioning of icons and open windows for tools.
C  * Init_xxx_posiiton sets the initial position from which a sequence
C  *	of icons / open windows will propagate.
C  * Figure_xxx_rect establishes a new rect for one tool, without
C  *	requiring the tool's windowfd;  this implies the window should
C  *	not yet be in the display tree when these routines are called.
C  * Acquire_xxx_rect does the same job, but takes care of removing / 
C  *	reinserting the window in the display tree.
C  * Inquire_xxx_rect returns the same information Aquire would,
C  *	without advancing the global positioning information.
C  * Set_xxx_rect handle the rect changes for operations Front / Back
C  *	and Full / Open / Close
C  
C 
C  * Exported by wmgr_state.c:
C  
      EXTERNAL wmgr_changelevel
C       Xv_window window
C       INTEGER parent
C       INTEGER top

C 
C  * Fork programname with otherargs.  Place its normal rect at normalrect.
C  * Place its icon rect at iconrect.  Iconicflag indicates the original
C  * state of the tool.  Positioning/state information are only hints.
C  * The tool can ignore them.
C 
C  *
C  * NOTE: This function prototype exists only for backwards compatibility reasons.
C  * In wmgr_menu.c, it is marked Pkg_private. It's use is highly discouraged.
C 
C  
      INTEGER  wmgr_forktool
      EXTERNAL wmgr_forktool
C       Char_ptr programname
C       Char_ptr otherargs
C       Rect_ptr normalrect
C       Rect_ptr iconrect
C       LOGICAL iconicflag

#endif wmgr_F.

#ifndef stddefs_F_DEFINED
#define stddefs_F_DEFINED

      INTEGER True
      PARAMETER (True = 1)
      INTEGER False
      PARAMETER (False = 0)
      INTEGER TRUE, FALSE
      PARAMETER (TRUE=1, FALSE=0)
      IMPLICIT INTEGER (A-Z)
      IMPLICIT INTEGER (a-z)
#define Byte BYTE
#define Char_ptr INTEGER*4
#define ProcPointer INTEGER*4
C  TEMPORARY 
#define caddr_t Char_ptr
#define Cstring100 CHARACTER*(100)
#define Cstringp INTEGER*4
#define FILE_ptr INTEGER
#define Cstringp_array(__v) Cstringp __v ( 1 : 100 )
#define Cstringp_arrayp INTEGER*4
#define Data_array(__v) INTEGER __v ( 1 : 100 )
#define Data_arrayp INTEGER*4
#define wchar_t INTEGER
#define Wchar_ptr INTEGER*4
#endif stddefs_F.

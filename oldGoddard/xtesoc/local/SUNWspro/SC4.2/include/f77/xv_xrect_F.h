#ifndef xv_xrect_F_DEFINED
#define xv_xrect_F_DEFINED

C   derived from @(#)xv_xrect.h 1.12 91/05/28 SMI 
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * To enable applications to get the current clipping list
C  * for direct X graphics.
C  
#include "Xlib_F.h"
      INTEGER XV_MAX_XRECTS
      PARAMETER (XV_MAX_XRECTS = 32)
      STRUCTURE /Xv_xrectlist/
          RECORD /XRectangle/ rect_array (0 : XV_MAX_XRECTS-1)
      
          INTEGER  count
          END STRUCTURE
#endif xv_xrect_F.

#ifndef rectlist_F_DEFINED
#define rectlist_F_DEFINED

C   derived from @(#)rectlist.h 20.14 91/05/28 SMI 
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Defines the interface to the data structure called 
C  * a rectlist which is a list of rectangles.
C  *
C  * A rectlist has an offset (rl_x, rl_y) assoicated with it that is used
C  * to efficiently pass this data structure from one embedded coordinate
C  * system to another without changing the offsets of all the rectangles in
C  * the list.
C  *
C  * Rl_bound is the rectangle that bounds all the rectangles in the list
C  * and is maintained to facilitate efficient rectangle algebra.
C  
#include "rect_F.h"
C 
C  * PUBLIC definitions
C  
      INTEGER RECTNODE_NULL
      PARAMETER (RECTNODE_NULL = 0)
      INTEGER RECTLIST_NULL
      PARAMETER (RECTLIST_NULL = 0)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C 
C  * PUBLIC structures 
C  
#define Rectnode_ptr INTEGER*4
      STRUCTURE /Rectnode/
          Rectnode_ptr rn_next
          RECORD /Rect/ rn_rect
          END STRUCTURE
#define Rectlist_ptr INTEGER*4
      STRUCTURE /Rectlist/
          coord rl_x, rl_y
          Rectnode_ptr rl_head
          Rectnode_ptr rl_tail
          RECORD /Rect/ rl_bound
          END STRUCTURE
C 
C  * Rectlist Transformation macros used for passing rectlists up/down embedded
C  * coordinate systems.
C  
      EXTERNAL rl_passtoparent
C       coord x
C       coord y
C       RECORD /Rectlist/ rl

      EXTERNAL rl_passtochild
C       coord x
C       coord y
C       RECORD /Rectlist/ rl

C 
C  * Rectlist Offset Adjustment macros
C  
      EXTERNAL rl_rectoffset
C       RECORD /Rectlist/ rl
C       RECORD /Rect/ r1
C       RECORD /Rect/ r

      EXTERNAL rl_coordoffset
C       RECORD /Rectlist/ rl
C       coord x
C       coord y

C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
      INTEGER  rl_empty
      EXTERNAL rl_empty
C       RECORD /Rectlist/ rl

      INTEGER  rl_equal
      EXTERNAL rl_equal
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl2

      INTEGER  rl_boundintersectsrect
      EXTERNAL rl_boundintersectsrect
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl

      INTEGER  rl_rectintersects
      EXTERNAL rl_rectintersects
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl

      INTEGER  rl_equalrect
      EXTERNAL rl_equalrect
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl

      INTEGER  rl_includespoint
      EXTERNAL rl_includespoint
C       RECORD /Rectlist/ rl
C       INTEGER x
C       INTEGER y

      EXTERNAL rl_rectintersection
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl

      EXTERNAL rl_rectunion
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl

      EXTERNAL rl_rectdifference
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl

      EXTERNAL rl_intersection
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl2
C       RECORD /Rectlist/ rl

      EXTERNAL rl_sort
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl
C       INTEGER sortorder

      EXTERNAL rl_union
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl2
C       RECORD /Rectlist/ rl

      EXTERNAL rl_difference
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl2
C       RECORD /Rectlist/ rl

      EXTERNAL rl_initwithrect
C       RECORD /Rect/ r
C       RECORD /Rectlist/ rl

      EXTERNAL rl_copy
C       RECORD /Rectlist/ rl1
C       RECORD /Rectlist/ rl

      EXTERNAL rl_free
C       RECORD /Rectlist/ rl

      EXTERNAL rl_coalesce
C       RECORD /Rectlist/ rl

      EXTERNAL rl_normalize
C       RECORD /Rectlist/ rl

      EXTERNAL rl_print
C       Rectlist_ptr rl
C       Char_ptr tag

#endif rectlist_F.

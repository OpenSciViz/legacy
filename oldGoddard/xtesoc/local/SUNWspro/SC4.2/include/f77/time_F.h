#ifndef time_F_DEFINED
#define time_F_DEFINED

C   derived from @(#)time.h 2.10 88/02/08 SMI; from UCB 7.1 6/4/86	
C 
C  * Copyright (c) 1982, 1986 Regents of the University of California.
C  * All rights reserved.  The Berkeley software License Agreement
C  * specifies the terms and conditions for redistribution.
C  
C 
C  * Structure returned by gettimeofday(2) system call,
C  * and used in other calls.
C  
      STRUCTURE /Timeval/
          INTEGER  tv_sec
          INTEGER  tv_usec
          END STRUCTURE
C 
C  * Names of the interval timers, and structure
C  * defining a timer setting.
C  
      INTEGER ITIMER_REAL
      PARAMETER (ITIMER_REAL = 0)
      INTEGER ITIMER_VIRTUAL
      PARAMETER (ITIMER_VIRTUAL = 1)
      INTEGER ITIMER_PROF
      PARAMETER (ITIMER_PROF = 2)
      STRUCTURE /Itimerval/
          RECORD /Timeval/ it_interval
          RECORD /Timeval/ it_value
          END STRUCTURE
#define Itimerval_ptr INTEGER*4
#endif time_F.

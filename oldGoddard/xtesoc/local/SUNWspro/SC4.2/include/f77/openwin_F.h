#ifndef openwin_F_DEFINED
#define openwin_F_DEFINED

C       derived from @(#)openwin.h ?.?? ??/??/??   
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  *
C  * Description:
C  *
C  *	Defines attributes for the window that implements a LAF
C  *  subwindow.
C  *
C  
#include "attr_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "window_F.h"
      Xv_pkg_ptr OPENWIN
      INTEGER OPENWIN_SPLIT_NEWVIEW_IN_PLACE
      PARAMETER (OPENWIN_SPLIT_NEWVIEW_IN_PLACE = -1)
      INTEGER OPENWIN_CANNOT_EXPAND
      PARAMETER (OPENWIN_CANNOT_EXPAND = -1)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
#define Openwin Xv_opaque
      STRUCTURE /Xv_openwin/
          RECORD /Xv_window_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      INTEGER OPENWIN_SPLIT_HORIZONTAL,OPENWIN_SPLIT_VERTICAL
      PARAMETER ( OPENWIN_SPLIT_HORIZONTAL=0,  OPENWIN_SPLIT_VERTICAL=1)
#define Openwin_split_direction INTEGER*4
#define Openwin_split_destroy_proc INTEGER
#define Openwin_split_destroy_procp INTEGER*4
#define Openwin_split_init_proc INTEGER
#define Openwin_split_init_procp INTEGER*4
C 
C     * Public Attributes 
C     
      INTEGER OPENWIN_ADJUST_FOR_HORIZONTAL_SCROLLBAR
      PARAMETER (OPENWIN_ADJUST_FOR_HORIZONTAL_SCROLLBAR = 1442908417)
      INTEGER OPENWIN_ADJUST_FOR_VERTICAL_SCROLLBAR
      PARAMETER (OPENWIN_ADJUST_FOR_VERTICAL_SCROLLBAR = 1443170561)
      INTEGER OPENWIN_AUTO_CLEAR
      PARAMETER (OPENWIN_AUTO_CLEAR = 1443498241)
      INTEGER OPENWIN_HORIZONTAL_SCROLLBAR
      PARAMETER (OPENWIN_HORIZONTAL_SCROLLBAR = 1443826177)
      INTEGER OPENWIN_NVIEWS
      PARAMETER (OPENWIN_NVIEWS = 1444153345)
      INTEGER OPENWIN_NO_MARGIN
      PARAMETER (OPENWIN_NO_MARGIN = 1444481281)
      INTEGER OPENWIN_NTH_VIEW
      PARAMETER (OPENWIN_NTH_VIEW = 1444809217)
      INTEGER OPENWIN_SELECTED_VIEW
      PARAMETER (OPENWIN_SELECTED_VIEW = 1445136897)
      INTEGER OPENWIN_SHOW_BORDERS
      PARAMETER (OPENWIN_SHOW_BORDERS = 1445464321)
      INTEGER OPENWIN_SPLIT
      PARAMETER (OPENWIN_SPLIT = 1445808705)
      INTEGER OPENWIN_SPLIT_DIRECTION
      PARAMETER (OPENWIN_SPLIT_DIRECTION = 1446119713)
      INTEGER OPENWIN_SPLIT_POSITION
      PARAMETER (OPENWIN_SPLIT_POSITION = 1446447169)
      INTEGER OPENWIN_SPLIT_VIEW
      PARAMETER (OPENWIN_SPLIT_VIEW = 1446775297)
      INTEGER OPENWIN_SPLIT_VIEW_START
      PARAMETER (OPENWIN_SPLIT_VIEW_START = 1447102529)
      INTEGER OPENWIN_VERTICAL_SCROLLBAR
      PARAMETER (OPENWIN_VERTICAL_SCROLLBAR = 1447430657)
      INTEGER OPENWIN_VIEW_ATTRS
      PARAMETER (OPENWIN_VIEW_ATTRS = 1447774785)
      INTEGER OPENWIN_SPLIT_INIT_PROC
      PARAMETER (OPENWIN_SPLIT_INIT_PROC = 1447823969)
      INTEGER OPENWIN_SPLIT_DESTROY_PROC
      PARAMETER (OPENWIN_SPLIT_DESTROY_PROC = 1447889505)
#define Openwin_attribute Attr_attribute
      COMMON /openwin_globals/   OPENWIN
#endif openwin_F.

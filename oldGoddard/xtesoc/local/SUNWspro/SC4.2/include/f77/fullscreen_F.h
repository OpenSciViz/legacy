#ifndef fullscreen_F_DEFINED
#define fullscreen_F_DEFINED

C  derived from   @(#)fullscreen.h 20.27 91/05/28 
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "cursor_F.h"
#include "base_F.h"
#include "generic_F.h"
#include "pkg_public_F.h"
#include "rect_F.h"
#include "win_input_F.h"
#include "window_F.h"
      Xv_pkg_ptr FULLSCREEN
C 
C  * Public typedefs
C  
#define Fullscreen Xv_opaque
C 
C  * Public structures
C  
C 
C  * type fullscreen is For SunView 1 fullscreen compatibility only
C  
      STRUCTURE /fullscreen/
          INTEGER  fs_windowfd
          RECORD /Rect/ fs_screenrect
          Xv_Window fs_rootwindow
          RECORD /Inputmask/ inputmask
          Xv_Cursor cursor
          END STRUCTURE
#define fullscreen_ptr INTEGER*4
      STRUCTURE /Xv_fullscreen/
          RECORD /Xv_generic_struct/ parent
          Xv_opaque private_data
          Xv_embedding embedding_data
          RECORD /fullscreen/ fullscreen_struct
          END STRUCTURE
      INTEGER FULLSCREEN_SYNCHRONOUS,FULLSCREEN_ASYNCHRONOUS
      PARAMETER ( FULLSCREEN_SYNCHRONOUS=0,  FULLSCREEN_ASYNCHRONOUS=1)
#define Fullscreen_grab_mode INTEGER*4
C   Public Attributes  
      INTEGER FULLSCREEN_CURSOR_WINDOW
      PARAMETER (FULLSCREEN_CURSOR_WINDOW = 1258621441)
      INTEGER FULLSCREEN_INPUT_WINDOW
      PARAMETER (FULLSCREEN_INPUT_WINDOW = 1258949121)
      INTEGER FULLSCREEN_PAINT_WINDOW
      PARAMETER (FULLSCREEN_PAINT_WINDOW = 1259276801)
      INTEGER FULLSCREEN_RECT
      PARAMETER (FULLSCREEN_RECT = 1259604512)
      INTEGER FULLSCREEN_SYNC
      PARAMETER (FULLSCREEN_SYNC = 1259931649)
      INTEGER FULLSCREEN_ALLOW_SYNC_EVENT
      PARAMETER (FULLSCREEN_ALLOW_SYNC_EVENT = 1260259872)
      INTEGER FULLSCREEN_ALLOW_EVENTS
      PARAMETER (FULLSCREEN_ALLOW_EVENTS = 1260390913)
      INTEGER FULLSCREEN_GRAB_KEYBOARD
      PARAMETER (FULLSCREEN_GRAB_KEYBOARD = 1260587265)
      INTEGER FULLSCREEN_GRAB_POINTER
      PARAMETER (FULLSCREEN_GRAB_POINTER = 1260914945)
      INTEGER FULLSCREEN_GRAB_SERVER
      PARAMETER (FULLSCREEN_GRAB_SERVER = 1261242625)
      INTEGER FULLSCREEN_KEYBOARD_GRAB_PTR_MODE
      PARAMETER (FULLSCREEN_KEYBOARD_GRAB_PTR_MODE = 1261570561)
      INTEGER FULLSCREEN_KEYBOARD_GRAB_KBD_MODE
      PARAMETER (FULLSCREEN_KEYBOARD_GRAB_KBD_MODE = 1261898241)
      INTEGER FULLSCREEN_POINTER_GRAB_PTR_MODE
      PARAMETER (FULLSCREEN_POINTER_GRAB_PTR_MODE = 1262225921)
      INTEGER FULLSCREEN_POINTER_GRAB_KBD_MODE
      PARAMETER (FULLSCREEN_POINTER_GRAB_KBD_MODE = 1262553601)
      INTEGER FULLSCREEN_OWNER_EVENTS
      PARAMETER (FULLSCREEN_OWNER_EVENTS = 1262225665)
#define Fullscreen_attr Attr_attribute
C 
C  * PUBLIC functions
C  
      fullscreen_ptr  fullscreen_init
      EXTERNAL fullscreen_init
C       Xv_Window window

      EXTERNAL fullscreen_destroy
C       fullscreen_ptr fs

      INTEGER  fullscreen_set_cursor
      EXTERNAL fullscreen_set_cursor
C       fullscreen_ptr fs
C       Xv_Cursor cursor

      EXTERNAL fullscreen_set_inputmask
C       fullscreen_ptr fs
C       RECORD /Inputmask/ im

      COMMON /fullscreen_globals/   FULLSCREEN
#endif fullscreen_F.

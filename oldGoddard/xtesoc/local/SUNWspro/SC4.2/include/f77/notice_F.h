#ifndef notice_F_DEFINED
#define notice_F_DEFINED

C      derived from  @(#)notice.h 20.23 91/01/11  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "window_F.h"
#include "win_input_F.h"
#include "pkg_public_F.h"
#include "base_F.h"
#include "generic_F.h"
      Xv_pkg_ptr NOTICE
C 
C  * the following constant, NOTICE_FAILED is returned if notice_prompt() 
C  *  failed for an unspecified reason.
C  
      INTEGER NOTICE_YES
      PARAMETER (NOTICE_YES = 1)
      INTEGER NOTICE_NO
      PARAMETER (NOTICE_NO = 0)
      INTEGER NOTICE_FAILED
      PARAMETER (NOTICE_FAILED = -1)
      INTEGER NOTICE_TRIGGERED
      PARAMETER (NOTICE_TRIGGERED = -2)
#define Xv_Notice Xv_opaque
#define Xv_notice Xv_opaque
C  Values for Notice_attributes 
      INTEGER NOTICE_BUTTON
      PARAMETER (NOTICE_BUTTON = 1493239810)
      INTEGER NOTICE_BUTTON_NO
      PARAMETER (NOTICE_BUTTON_NO = 1493502305)
      INTEGER NOTICE_BUTTON_YES
      PARAMETER (NOTICE_BUTTON_YES = 1493829985)
      INTEGER NOTICE_FOCUS_XY
      PARAMETER (NOTICE_FOCUS_XY = 1494157506)
      INTEGER NOTICE_FONT
      PARAMETER (NOTICE_FONT = 1494485409)
      INTEGER NOTICE_MESSAGE_STRINGS
      PARAMETER (NOTICE_MESSAGE_STRINGS = 1494845793)
      INTEGER NOTICE_MESSAGE_STRINGS_ARRAY_PTR
      PARAMETER (NOTICE_MESSAGE_STRINGS_ARRAY_PTR = 1495140705)
      INTEGER NOTICE_NO_BEEPING
      PARAMETER (NOTICE_NO_BEEPING = 1495468289)
      INTEGER NOTICE_TRIGGER
      PARAMETER (NOTICE_TRIGGER = 1495795713)
      INTEGER NOTICE_MESSAGE_STRING
      PARAMETER (NOTICE_MESSAGE_STRING = 1496123745)
      INTEGER NOTICE_LOCK_SCREEN
      PARAMETER (NOTICE_LOCK_SCREEN = 1498417409)
      INTEGER NOTICE_TRIGGER_EVENT
      PARAMETER (NOTICE_TRIGGER_EVENT = 1498744833)
      INTEGER NOTICE_STATUS
      PARAMETER (NOTICE_STATUS = 1499400705)
      INTEGER NOTICE_EVENT_PROC
      PARAMETER (NOTICE_EVENT_PROC = 1499728481)
      INTEGER NOTICE_BUSY_FRAMES
      PARAMETER (NOTICE_BUSY_FRAMES = 1500088833)
      INTEGER NOTICE_BLOCK_THREAD
      PARAMETER (NOTICE_BLOCK_THREAD = 1500383489)
#define Notice_attribute Attr_attribute
      STRUCTURE /Xv_notice_struct/
          RECORD /Xv_generic_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      INTEGER  notice_prompt
      EXTERNAL notice_prompt
C       Xv_window client_frame
C       Event_ptr event

      INTEGER  notice_prompt_l
      EXTERNAL notice_prompt_l
C       Xv_window client_frame
C       Event_ptr event
C       Attr_avlist avlist

      COMMON /notice_globals/   NOTICE
#endif notice_F.

#ifndef cms_grays_F_DEFINED
#define cms_grays_F_DEFINED

C   derived from @(#)cms_grays.h 20.10 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Definition of the colormap segment CMS_GRAYS, a collection of grays.
C  
      CHARACTER*(5) CMS_GRAYS
      PARAMETER (CMS_GRAYS = "grays")
      INTEGER CMS_GRAYSSIZE
      PARAMETER (CMS_GRAYSSIZE = 8)
      INTEGER WHITE
      PARAMETER (WHITE = 0)
      INTEGER BLACK
      PARAMETER (BLACK = 7)
#define Cms_gray(__v) Byte __v ( 0 : 7 )
      Byte  GRAY
      EXTERNAL GRAY
C       Byte i

      EXTERNAL cms_grayssetup
C       Cms_gray (r)
C       Cms_gray (g)
C       Cms_gray (b)

#endif cms_grays_F.

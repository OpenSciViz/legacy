#ifndef pkg_F_DEFINED
#define pkg_F_DEFINED

C   derived from @(#)pkg.h 20.22 91/03/19 SMI 
C 
C  *
C  * Copyright (c) 1988 by Sun Microsystems, Inc.
C  
C  
C  * 	Of interest to package implementors only.
C  
#include "base_F.h"
#include "attr_F.h"
C  set procs can return XV_SET_DONE if xv_super_set_avlist()
C  * has been called.  This will end the set.  Note that
C  * other possible set proc return values are XV_OK or an attribute.
C  
      INTEGER XV_SET_DONE
      PARAMETER (XV_SET_DONE = 2)
#endif pkg_F.

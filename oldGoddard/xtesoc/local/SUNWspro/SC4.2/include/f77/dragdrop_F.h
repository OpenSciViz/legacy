#ifndef dragdrop_F_DEFINED
#define dragdrop_F_DEFINED

C   derived from @(#)dragdrop.h 1.17 91/07/03 SMI	
C 
C  *	(c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *	pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *	file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
#include "pkg_public_F.h"
#include "win_input_F.h"
#include "sel_pkg_F.h"
      Xv_pkg_ptr DRAGDROP
      Xv_pkg_ptr DROP_SITE_ITEM
#define Xv_drag_drop Xv_opaque
#define Drag_drop Xv_opaque
#define Dnd Xv_opaque
#define Xv_drop_site Xv_opaque
#define Drop_site_item Xv_opaque
C  Error codes 
      INTEGER DND_SUCCEEDED
      PARAMETER (DND_SUCCEEDED = XV_OK)
      INTEGER DND_ERROR
      PARAMETER (DND_ERROR = XV_ERROR)
      INTEGER DND_ILLEGAL_TARGET
      PARAMETER (DND_ILLEGAL_TARGET = 2)
      INTEGER DND_TIMEOUT
      PARAMETER (DND_TIMEOUT = 3)
      INTEGER DND_SELECTION
      PARAMETER (DND_SELECTION = 4)
      INTEGER DND_ROOT
      PARAMETER (DND_ROOT = 5)
      INTEGER DND_ABORTED
      PARAMETER (DND_ABORTED = 6)
C  STOP key pressed 
C  Drag type 
      INTEGER DND_MOVE,DND_COPY
      PARAMETER ( DND_MOVE=0,  DND_COPY=1)
#define DndDragType INTEGER*4
      INTEGER DND_ENTERLEAVE
      PARAMETER (DND_ENTERLEAVE = X'1')
      INTEGER DND_MOTION
      PARAMETER (DND_MOTION = X'2')
      INTEGER DND_DEFAULT_SITE
      PARAMETER (DND_DEFAULT_SITE = X'4')
      INTEGER DND_RECT_SITE
      PARAMETER (DND_RECT_SITE = 0)
C  XXX: Currently, only rect sites are supported. 
      INTEGER DND_WINDOW_SITE
      PARAMETER (DND_WINDOW_SITE = 1)
      INTEGER DND_VERSION
      PARAMETER (DND_VERSION = 0)
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
C  Private defines 
      INTEGER DND_MOVE_FLAG
      PARAMETER (DND_MOVE_FLAG = X'1')
      INTEGER DND_ACK_FLAG
      PARAMETER (DND_ACK_FLAG = X'2')
      INTEGER DND_TRANSIENT_FLAG
      PARAMETER (DND_TRANSIENT_FLAG = X'4')
      INTEGER DND_FORWARDED_FLAG
      PARAMETER (DND_FORWARDED_FLAG = X'8')
C  ie_flags 
      INTEGER DND_LOCAL
      PARAMETER (DND_LOCAL = X'1')
      INTEGER DND_FORWARDED
      PARAMETER (DND_FORWARDED = X'2')
C   Public Drag & Drop pkg attrs  
      INTEGER DND_TYPE
      PARAMETER (DND_TYPE = 1308691233)
      INTEGER DND_CURSOR
      PARAMETER (DND_CURSOR = 1308953089)
      INTEGER DND_X_CURSOR
      PARAMETER (DND_X_CURSOR = 1309281025)
      INTEGER DND_ACCEPT_CURSOR
      PARAMETER (DND_ACCEPT_CURSOR = 1309608449)
      INTEGER DND_ACCEPT_X_CURSOR
      PARAMETER (DND_ACCEPT_X_CURSOR = 1309936385)
      INTEGER DND_TIMEOUT_VALUE
      PARAMETER (DND_TIMEOUT_VALUE = 1310264065)
C   Private Drop Site Item attrs  
      INTEGER DROP_SITE_SIZE
      PARAMETER (DROP_SITE_SIZE = 1314850817)
C   Public Drop Site Item attrs  
C   DROP_SITE_TYPE		 
      INTEGER DROP_SITE_ID
      PARAMETER (DROP_SITE_ID = 1315506945)
      INTEGER DROP_SITE_EVENT_MASK
      PARAMETER (DROP_SITE_EVENT_MASK = 1315833857)
      INTEGER DROP_SITE_REGION
      PARAMETER (DROP_SITE_REGION = 1316162049)
      INTEGER DROP_SITE_REGION_PTR
      PARAMETER (DROP_SITE_REGION_PTR = 1316489729)
      INTEGER DROP_SITE_DELETE_REGION
      PARAMETER (DROP_SITE_DELETE_REGION = 1316817409)
      INTEGER DROP_SITE_DELETE_REGION_PTR
      PARAMETER (DROP_SITE_DELETE_REGION_PTR = 1317145089)
      INTEGER DROP_SITE_DEFAULT
      PARAMETER (DROP_SITE_DEFAULT = 1317472513)
#define Dnd_attribute Attr_attribute
      STRUCTURE /Xv_dnd_struct/
          RECORD /Xv_sel_owner/ parent_data
          Xv_opaque private_data
          END STRUCTURE
#define Xv_drop_site_struct RECORD /Xv_dnd_struct/
      Bool  dnd_is_local
      EXTERNAL dnd_is_local
C       Event_ptr event

      Bool  dnd_is_forwarded
      EXTERNAL dnd_is_forwarded
C       Event_ptr event

C  Due to the unclear future for a standard drag and drop protocol, do not
C  * rely on being able to access the drop site id in the X event.   It may
C  * not always be there.  You've been warned!
C  
C MACRO PROCEDURE  dnd_site_id(event : Event_ptr):INTEGER; 
C 
C  * is not converted becose its body is inaccurancly coded 
C  * (macro parameter occurence isn't parethesized)
C  
C 
C  * Public Functions
C  
      INTEGER  dnd_send_drop
      EXTERNAL dnd_send_drop
C       Xv_opaque object

      Xv_opaque  dnd_decode_drop
      EXTERNAL dnd_decode_drop
C       Xv_opaque object
C       Event_ptr event

      EXTERNAL dnd_done
C       Xv_opaque object

C  The function xv_decode_drop is obsolete, please use the new V3
C    drap and drop interface. The function is still provided for backwards
C    compatibility for V2 programs.
C 
      INTEGER  xv_decode_drop
      EXTERNAL xv_decode_drop
C       Event_ptr ev
C       Cstringp buffer
C       INTEGER bsize

      COMMON /dragdrop_globals/   DROP_SITE_ITEM, DRAGDROP
#endif dragdrop_F.

#ifndef pkg_public_F_DEFINED
#define pkg_public_F_DEFINED

C   derived from @(#)pkg_public.h 20.23 91/07/18 SMI      
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  *
C  * SunView pkg. definition	
C  
C 
C  * PRIVATE structures for pkg implementors only  
C  
C 
C  * Last field before "embedded" struct in an "embedding object". 
C  
#define Xv_embedding INTEGER
#define Xv_pkg_ptr INTEGER*4
      STRUCTURE /Xv_pkg/
          Char_ptr name
          Attr_pkg attr_id
          INTEGER  size_of_object
          Xv_pkg_ptr parent_pkg
          ProcPointer init
          ProcPointer set
          ProcPointer get
          ProcPointer destroy
          ProcPointer find
          END STRUCTURE
C 
C  * Base instance for all objects	
C  
      STRUCTURE /Xv_base/
          INTEGER  seal
          Xv_pkg_ptr pkg
          END STRUCTURE
C 
C  ***********************************************************************
C  *				Globals
C  ***********************************************************************
C  
C 
C  * PUBLIC General interface functions	
C  
      Xv_object  xv_create
      EXTERNAL xv_create
C       Xv_object parent
C       Xv_pkg_ptr pkg

      Xv_object  xv_create_l
      EXTERNAL xv_create_l
C       Xv_object parent
C       Xv_pkg_ptr pkg
C       Attr_avlist attrs

      Xv_object  xv_find
      EXTERNAL xv_find
C       Xv_object parent
C       Xv_pkg_ptr pkg

      Xv_object  xv_find_l
      EXTERNAL xv_find_l
C       Xv_object parent
C       Xv_pkg_ptr pkg
C       Attr_avlist attrs

      EXTERNAL xv_destroy
C       Xv_object object

      INTEGER  xv_destroy_f
      EXTERNAL xv_destroy_f
C       Xv_object object

      INTEGER  xv_destroy_check
      EXTERNAL xv_destroy_check
C       Xv_object object

      INTEGER  xv_destroy_immediate
      EXTERNAL xv_destroy_immediate
C       Xv_object object

      INTEGER  xv_destroy_safe
      EXTERNAL xv_destroy_safe
C       Xv_object object

      Xv_opaque  xv_set
      EXTERNAL xv_set
C       Xv_object object
C       Attr_attribute attr
C       INTEGER data

      Xv_opaque  xv_set_l
      EXTERNAL xv_set_l
C       Xv_object object
C       Attr_avlist attrs

      Xv_opaque  xv_get
      EXTERNAL xv_get
C       Xv_object object
C       Attr_attribute attr

      Xv_opaque  xv_get_1
      EXTERNAL xv_get_1
C       Xv_object object
C       Attr_avlist attr
C       INTEGER par1

      Xv_opaque  xv_get_2
      EXTERNAL xv_get_2
C       Xv_object object
C       Attr_avlist attr
C       INTEGER par1
C       INTEGER par2

      Xv_opaque  xv_get_3
      EXTERNAL xv_get_3
C       Xv_object object
C       Attr_avlist attr
C       INTEGER par1
C       INTEGER par2
C       INTEGER par3

      INTEGER XV_OBJECT_SEAL
      PARAMETER (XV_OBJECT_SEAL = X'F0A58142')
#endif pkg_public_F.

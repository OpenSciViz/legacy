#ifndef window_F_DEFINED
#define window_F_DEFINED

C  derived from @(#)window.h 20.97 92/12/14 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
#include "notify_F.h"
#include "pkg_public_F.h"
#include "drawable_F.h"
#include "screen_F.h"
#include "server_F.h"
#include "generic_F.h"
#include "win_input_F.h"
#include "Xlib_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
#define Xv_Window Xv_opaque
#define Xv_window Xv_opaque
#define Window_type Attr_pkg
#define Win_event_proc INTEGER
#define Win_event_procp INTEGER*4
C 
C  * PUBLIC definitions 
C  
      Xv_pkg_ptr WINDOW
      INTEGER WIN_EXTEND_TO_EDGE
      PARAMETER (WIN_EXTEND_TO_EDGE = -1)
      INTEGER WIN_DEFAULT_BORDER_WIDTH
      PARAMETER (WIN_DEFAULT_BORDER_WIDTH = 1)
      INTEGER WIN_MESSAGE_DATA_SIZE
      PARAMETER (WIN_MESSAGE_DATA_SIZE = 20)
C  values for WIN_SCALE_STATE 
      INTEGER WIN_SCALE_SMALL,WIN_SCALE_MEDIUM,WIN_SCALE_LARGE
     &,WIN_SCALE_EXTRALARGE
      PARAMETER ( WIN_SCALE_SMALL=0,  WIN_SCALE_MEDIUM=1, 
     & WIN_SCALE_LARGE=2,  WIN_SCALE_EXTRALARGE=3)
#define Window_rescale_state INTEGER*4
C 
C  *  The following constants are defined in the X header file, so we don't
C  *  redefine them here.
C  * * Bit Gravity *
C  *	ForgetGravity = 0;
C  *	NorthWestGravity = 1;
C  *	NorthGravity = 2;
C  *	NorthEastGravity = 3;
C  *	WestGravity = 4;
C  *	CenterGravity = 5;
C  *	EastGravity = 6;
C  *	SouthWestGravity = 7;
C  *	SouthGravity = 8;
C  *	SouthEastGravity = 9;
C  *	StaticGravity = 10;
C  *
C  * * Window gravity + bit gravity above *
C  *
C  *	UnmapGravity = 0;
C  
C 
C  * Useful conversion macros
C  
      Xv_Screen  XV_SCREEN_FROM_WINDOW
      EXTERNAL XV_SCREEN_FROM_WINDOW
C       Xv_window window

      Xv_Server  XV_SERVER_FROM_WINDOW
      EXTERNAL XV_SERVER_FROM_WINDOW
C       Xv_window window

      Display_ptr  XV_DISPLAY_FROM_WINDOW
      EXTERNAL XV_DISPLAY_FROM_WINDOW
C       Xv_window window

C 
C  * Window-fitting macros 
C  
      EXTERNAL window_fit
C       Xv_window win

      EXTERNAL window_fit_height
C       Xv_window win

      EXTERNAL window_fit_width
C       Xv_window win

C 
C  * WIN_RECT_INFO flags for package implementors 
C  
      INTEGER WIN_X_SET
      PARAMETER (WIN_X_SET = 1)
      INTEGER WIN_Y_SET
      PARAMETER (WIN_Y_SET = 2)
      INTEGER WIN_WIDTH_SET
      PARAMETER (WIN_WIDTH_SET = 4)
      INTEGER WIN_HEIGHT_SET
      PARAMETER (WIN_HEIGHT_SET = 8)
C 
C 	 * Public Attributes 
C 	 
      INTEGER WIN_ALARM
      PARAMETER (WIN_ALARM = 1224804896)
      INTEGER WIN_BACK
      PARAMETER (WIN_BACK = 1224870432)
      INTEGER WIN_BACKGROUND_PIXMAP
      PARAMETER (WIN_BACKGROUND_PIXMAP = 1224935937)
      INTEGER WIN_BELOW
      PARAMETER (WIN_BELOW = 1225001473)
      INTEGER WIN_CLIENT_DATA
      PARAMETER (WIN_CLIENT_DATA = 1225263617)
      INTEGER WIN_COLUMNS
      PARAMETER (WIN_COLUMNS = 1225525249)
      INTEGER WIN_COLUMN_GAP
      PARAMETER (WIN_COLUMN_GAP = 1225787393)
      INTEGER WIN_COLUMN_WIDTH
      PARAMETER (WIN_COLUMN_WIDTH = 1226049537)
      INTEGER WIN_CONSUME_EVENT
      PARAMETER (WIN_CONSUME_EVENT = 1226311969)
      INTEGER WIN_CONSUME_EVENTS
      PARAMETER (WIN_CONSUME_EVENTS = 1226606881)
      INTEGER WIN_CURSOR
      PARAMETER (WIN_CURSOR = 1226836673)
      INTEGER WIN_DEPTH
      PARAMETER (WIN_DEPTH = 1231423489)
C   WIN_DEPTH_V2 is needed to keep backwards compatibility with   
C   V2.  WIN_DEPTH was mistakenly defined as and ATTR_NO_VALUE,   
C   which meant that despite the documentation you couldn't   
C   use it in the creation of a window.  Thus we created   
C   a new WIN_DEPTH that is of the correct type, and moved   
C   the old WIN_DEPTH to WIN_DEPTH_V2.  This can be removed   
C   when it has sufficiently aged.   
      INTEGER WIN_DEPTH_V2
      PARAMETER (WIN_DEPTH_V2 = 1227098656)
      INTEGER WIN_DESIRED_HEIGHT
      PARAMETER (WIN_DESIRED_HEIGHT = 1227360257)
      INTEGER WIN_DESIRED_WIDTH
      PARAMETER (WIN_DESIRED_WIDTH = 1227622401)
      INTEGER WIN_ERROR_MSG
      PARAMETER (WIN_ERROR_MSG = 1227884897)
      INTEGER WIN_EVENT_PROC
      PARAMETER (WIN_EVENT_PROC = 1228147297)
      INTEGER WIN_FIT_HEIGHT
      PARAMETER (WIN_FIT_HEIGHT = 1228671105)
      INTEGER WIN_FIT_WIDTH
      PARAMETER (WIN_FIT_WIDTH = 1228933185)
      INTEGER WIN_FRONT
      PARAMETER (WIN_FRONT = 1229064736)
      INTEGER WIN_GLYPH_FONT
      PARAMETER (WIN_GLYPH_FONT = 1229130241)
      INTEGER WIN_GRAB_ALL_INPUT
      PARAMETER (WIN_GRAB_ALL_INPUT = 1229195521)
      INTEGER WIN_HORIZONTAL_SCROLLBAR
      PARAMETER (WIN_HORIZONTAL_SCROLLBAR = 1229457921)
      INTEGER WIN_IGNORE_EVENT
      PARAMETER (WIN_IGNORE_EVENT = 1229719841)
      INTEGER WIN_IGNORE_EVENTS
      PARAMETER (WIN_IGNORE_EVENTS = 1230014753)
      INTEGER WIN_INPUT_MASK
      PARAMETER (WIN_INPUT_MASK = 1230244353)
      INTEGER WIN_IS_CLIENT_PANE
      PARAMETER (WIN_IS_CLIENT_PANE = 1230506528)
      INTEGER WIN_MENU
      PARAMETER (WIN_MENU = 1230768641)
      INTEGER WIN_MOUSE_XY
      PARAMETER (WIN_MOUSE_XY = 1231030466)
      INTEGER WIN_PARENT
      PARAMETER (WIN_PARENT = 1231292929)
      INTEGER WIN_PERCENT_HEIGHT
      PARAMETER (WIN_PERCENT_HEIGHT = 1231554561)
      INTEGER WIN_PERCENT_WIDTH
      PARAMETER (WIN_PERCENT_WIDTH = 1231816705)
      INTEGER WIN_RIGHT_OF
      PARAMETER (WIN_RIGHT_OF = 1232341505)
      INTEGER WIN_ROWS
      PARAMETER (WIN_ROWS = 1232603137)
      INTEGER WIN_ROW_GAP
      PARAMETER (WIN_ROW_GAP = 1232865281)
      INTEGER WIN_ROW_HEIGHT
      PARAMETER (WIN_ROW_HEIGHT = 1233127425)
      INTEGER WIN_SCREEN_RECT
      PARAMETER (WIN_SCREEN_RECT = 1233390112)
      INTEGER WIN_SET_FOCUS
      PARAMETER (WIN_SET_FOCUS = 1239681568)
      INTEGER WIN_VERTICAL_SCROLLBAR
      PARAMETER (WIN_VERTICAL_SCROLLBAR = 1233914369)
      INTEGER WIN_MESSAGE_TYPE
      PARAMETER (WIN_MESSAGE_TYPE = 1233979905)
      INTEGER WIN_MESSAGE_FORMAT
      PARAMETER (WIN_MESSAGE_FORMAT = 1234044929)
      INTEGER WIN_MESSAGE_DATA
      PARAMETER (WIN_MESSAGE_DATA = 1234110977)
      INTEGER WIN_BORDER
      PARAMETER (WIN_BORDER = 1234438401)
      INTEGER WIN_CMS
      PARAMETER (WIN_CMS = 1239878145)
C   Public Attributes  
      INTEGER WIN_X_CLIP_RECTS
      PARAMETER (WIN_X_CLIP_RECTS = 1240009217)
      INTEGER WIN_CMS_DATA
      PARAMETER (WIN_CMS_DATA = 1240140289)
      INTEGER WIN_CMS_NAME
      PARAMETER (WIN_CMS_NAME = 1240205665)
      INTEGER WIN_BIT_GRAVITY
      PARAMETER (WIN_BIT_GRAVITY = 1240270849)
      INTEGER WIN_WINDOW_GRAVITY
      PARAMETER (WIN_WINDOW_GRAVITY = 1240336385)
      INTEGER WIN_FOREGROUND_COLOR
      PARAMETER (WIN_FOREGROUND_COLOR = 1240401921)
      INTEGER WIN_BACKGROUND_COLOR
      PARAMETER (WIN_BACKGROUND_COLOR = 1240467457)
      INTEGER WIN_X_COLOR_INDICES
      PARAMETER (WIN_X_COLOR_INDICES = 1240664577)
      INTEGER WIN_CONSUME_X_EVENT_MASK
      PARAMETER (WIN_CONSUME_X_EVENT_MASK = 1240992513)
      INTEGER WIN_IGNORE_X_EVENT_MASK
      PARAMETER (WIN_IGNORE_X_EVENT_MASK = 1241058049)
      INTEGER WIN_X_EVENT_MASK
      PARAMETER (WIN_X_EVENT_MASK = 1241123585)
      INTEGER WIN_COLLAPSE_EXPOSURES
      PARAMETER (WIN_COLLAPSE_EXPOSURES = 1241188609)
      INTEGER WIN_SOFT_FNKEY_LABELS
      PARAMETER (WIN_SOFT_FNKEY_LABELS = 1238042977)
C   Private Attributes  
      INTEGER WIN_FRAME
      PARAMETER (WIN_FRAME = 1234962945)
      INTEGER WIN_NOTIFY_SAFE_EVENT_PROC
      PARAMETER (WIN_NOTIFY_SAFE_EVENT_PROC = 1237322337)
C   Public Attributes For SunView 1 Compatibility  
      INTEGER WIN_TYPE
      PARAMETER (WIN_TYPE = 1239419169)
#define Window_attribute Attr_attribute
      INTEGER WIN_SHOW
      PARAMETER (WIN_SHOW = XV_SHOW)
C 
C  * For SunView 1 Compatibility Only 
C  
      INTEGER WIN_X
      PARAMETER (WIN_X = XV_X)
      INTEGER WIN_Y
      PARAMETER (WIN_Y = XV_Y)
      INTEGER WIN_WIDTH
      PARAMETER (WIN_WIDTH = XV_WIDTH)
      INTEGER WIN_HEIGHT
      PARAMETER (WIN_HEIGHT = XV_HEIGHT)
      INTEGER WIN_FONT
      PARAMETER (WIN_FONT = XV_FONT)
      INTEGER WIN_DEVICE_NAME
      PARAMETER (WIN_DEVICE_NAME = XV_XNAME)
      INTEGER WIN_DEVICE_NUMBER
      PARAMETER (WIN_DEVICE_NUMBER = XV_DEVICE_NUMBER)
      INTEGER WIN_TOP_MARGIN
      PARAMETER (WIN_TOP_MARGIN = XV_TOP_MARGIN)
      INTEGER WIN_BOTTOM_MARGIN
      PARAMETER (WIN_BOTTOM_MARGIN = XV_BOTTOM_MARGIN)
      INTEGER WIN_LEFT_MARGIN
      PARAMETER (WIN_LEFT_MARGIN = XV_LEFT_MARGIN)
      INTEGER WIN_RIGHT_MARGIN
      PARAMETER (WIN_RIGHT_MARGIN = XV_RIGHT_MARGIN)
      INTEGER WIN_NAME
      PARAMETER (WIN_NAME = XV_NAME)
      INTEGER WIN_OWNER
      PARAMETER (WIN_OWNER = XV_OWNER)
      INTEGER WIN_FD
      PARAMETER (WIN_FD = XV_SELF)
      INTEGER WIN_PIXWIN
      PARAMETER (WIN_PIXWIN = XV_SELF)
      INTEGER WIN_RECT
      PARAMETER (WIN_RECT = XV_RECT)
      INTEGER WINDOW_TYPE
      PARAMETER (WINDOW_TYPE = ATTR_PKG_WIN)
      INTEGER WIN_CONSUME_KBD_EVENT
      PARAMETER (WIN_CONSUME_KBD_EVENT = WIN_CONSUME_EVENT)
      INTEGER WIN_IGNORE_KBD_EVENT
      PARAMETER (WIN_IGNORE_KBD_EVENT = WIN_IGNORE_EVENT)
      INTEGER WIN_CONSUME_KBD_EVENTS
      PARAMETER (WIN_CONSUME_KBD_EVENTS = WIN_CONSUME_EVENTS)
      INTEGER WIN_IGNORE_KBD_EVENTS
      PARAMETER (WIN_IGNORE_KBD_EVENTS = WIN_IGNORE_EVENTS)
      INTEGER WIN_PICK_INPUT_MASK
      PARAMETER (WIN_PICK_INPUT_MASK = WIN_INPUT_MASK)
      INTEGER WIN_CONSUME_PICK_EVENT
      PARAMETER (WIN_CONSUME_PICK_EVENT = WIN_CONSUME_EVENT)
      INTEGER WIN_IGNORE_PICK_EVENT
      PARAMETER (WIN_IGNORE_PICK_EVENT = WIN_IGNORE_EVENT)
      INTEGER WIN_CONSUME_PICK_EVENTS
      PARAMETER (WIN_CONSUME_PICK_EVENTS = WIN_CONSUME_EVENTS)
      INTEGER WIN_IGNORE_PICK_EVENTS
      PARAMETER (WIN_IGNORE_PICK_EVENTS = WIN_IGNORE_EVENTS)
      INTEGER WIN_NOTIFY_EVENT_PROC
      PARAMETER (WIN_NOTIFY_EVENT_PROC = WIN_NOTIFY_SAFE_EVENT_PROC)
      INTEGER WIN_NULL_VALUE,WIN_NO_EVENTS,WIN_UP_EVENTS
     &,WIN_ASCII_EVENTS,WIN_UP_ASCII_EVENTS,WIN_MOUSE_BUTTONS
     &,WIN_IN_TRANSIT_EVENTS,WIN_LEFT_KEYS,WIN_TOP_KEYS,WIN_RIGHT_KEYS
     &,WIN_META_EVENTS,WIN_UP_META_EVENTS,WIN_SUNVIEW_FUNCTION_KEYS
     &,WIN_EDIT_KEYS,WIN_MOTION_KEYS,WIN_TEXT_KEYS
      PARAMETER ( WIN_NULL_VALUE=0,  WIN_NO_EVENTS=1,  WIN_UP_EVENTS=2, 
     & WIN_ASCII_EVENTS=3,  WIN_UP_ASCII_EVENTS=4,  WIN_MOUSE_BUTTONS=5,
     &  WIN_IN_TRANSIT_EVENTS=6,  WIN_LEFT_KEYS=7,  WIN_TOP_KEYS=8, 
     & WIN_RIGHT_KEYS=9,  WIN_META_EVENTS=10,  WIN_UP_META_EVENTS=11, 
     & WIN_SUNVIEW_FUNCTION_KEYS=12,  WIN_EDIT_KEYS=13, 
     & WIN_MOTION_KEYS=14,  WIN_TEXT_KEYS=15)
#define Window_input_event INTEGER*4
      INTEGER WIN_CREATE,WIN_INSERT,WIN_REMOVE,WIN_DESTROY
     &,WIN_GET_RIGHT_OF,WIN_GET_BELOW,WIN_ADJUST_RECT,WIN_GET_X
     &,WIN_GET_Y,WIN_GET_WIDTH,WIN_GET_HEIGHT,WIN_GET_RECT,WIN_LAYOUT
     &,WIN_INSTALL
      PARAMETER ( WIN_CREATE=0,  WIN_INSERT=1,  WIN_REMOVE=2, 
     & WIN_DESTROY=3,  WIN_GET_RIGHT_OF=4,  WIN_GET_BELOW=5, 
     & WIN_ADJUST_RECT=6,  WIN_GET_X=7,  WIN_GET_Y=8,  WIN_GET_WIDTH=9, 
     & WIN_GET_HEIGHT=10,  WIN_GET_RECT=11,  WIN_LAYOUT=12, 
     & WIN_INSTALL=13)
#define Window_layout_op INTEGER*4
      STRUCTURE /Xv_window_struct/
          RECORD /Xv_drawable_struct/ parent_data
          Xv_opaque private_data
          END STRUCTURE
      STRUCTURE /Window_rescale_rect_obj/
          RECORD /Rect/ old_rect
          RECORD /Rect/ new_rect
          INTEGER  width_change, height_change, x_change, y_change
          INTEGER  adjusted
          Xv_Window sw
          END STRUCTURE
C 
C  * PUBLIC functions 
C  
      INTEGER  window_done
      EXTERNAL window_done
C       Xv_window window

      EXTERNAL xv_main_loop
C       Xv_object window

      INTEGER  window_read_event
      EXTERNAL window_read_event
C       Xv_window window
C       Event_ptr event

      EXTERNAL window_bell
C       Xv_window window

      INTEGER  xv_rows
      EXTERNAL xv_rows
C       Xv_Window window
C       INTEGER rows

      INTEGER  xv_row
      EXTERNAL xv_row
C       Xv_window window
C       INTEGER row

      INTEGER  xv_cols
      EXTERNAL xv_cols
C       Xv_window window
C       INTEGER cols

      INTEGER  xv_col
      EXTERNAL xv_col
C       Xv_window window
C       INTEGER col

      INTEGER  xv_send_message
      EXTERNAL xv_send_message
C       Xv_window window
C       Xv_opaque addresse
C       Char_ptr msg_type
C       INTEGER format
C       Xv_opaque data
C       INTEGER len

C   For Sunview compatibility 
      EXTERNAL window_main_loop
C       Xv_object win

C 
C  * PUBLIC functions 
C  * For SunView 1 Compatibility
C  
      Xv_Window  window_create
      EXTERNAL window_create
C       Xv_Window window
C       Xv_pkg_ptr pkg

      Xv_opaque  window_get
      EXTERNAL window_get
C       Xv_Window window
C       Window_attribute attr

      INTEGER  window_set
      EXTERNAL window_set
C       Xv_Window window

      INTEGER  window_destroy
      EXTERNAL window_destroy
C       Xv_Window window

C 
C  * Some wmgr stuff that needs to be here for the split libs.
C  * This should be moved out as soon as all the pushpin stuff in moved
C  * out of the intrensic layer.  [csk 3/23/89]
C  
C  value for pushpin_initial_state 
      INTEGER WMPushpinIsOut
      PARAMETER (WMPushpinIsOut = 0)
      INTEGER WMPushpinIsIn
      PARAMETER (WMPushpinIsIn = 1)
      COMMON /window_globals/   WINDOW
#endif window_F.

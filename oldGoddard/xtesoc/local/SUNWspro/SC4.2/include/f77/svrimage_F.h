#ifndef svrimage_F_DEFINED
#define svrimage_F_DEFINED

C   derived from @(#)svrimage.h 20.28 92/06/19 SMI  
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C 
C  *      pending in the U.S. and foreign countries. See LEGAL_NOTICE
C 
C  *      file for terms of the license.
C  
#include "attr_F.h"
#include "base_F.h"
#include "drawable_F.h"
#include "pixrect_F.h"
#include "pkg_public_F.h"
      Xv_pkg_ptr SERVER_IMAGE
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
#define Server_image Xv_opaque
C   Public attributes  
      INTEGER SERVER_IMAGE_DEPTH
      PARAMETER (SERVER_IMAGE_DEPTH = 1157695489)
      INTEGER SERVER_IMAGE_BITS
      PARAMETER (SERVER_IMAGE_BITS = 1157761537)
      INTEGER SERVER_IMAGE_X_BITS
      PARAMETER (SERVER_IMAGE_X_BITS = 1157827073)
      INTEGER SERVER_IMAGE_COLORMAP
      PARAMETER (SERVER_IMAGE_COLORMAP = 1157892449)
      INTEGER SERVER_IMAGE_BITMAP_FILE
      PARAMETER (SERVER_IMAGE_BITMAP_FILE = 1157957985)
      INTEGER SERVER_IMAGE_PIXMAP
      PARAMETER (SERVER_IMAGE_PIXMAP = 1158023681)
      INTEGER SERVER_IMAGE_SAVE_PIXMAP
      PARAMETER (SERVER_IMAGE_SAVE_PIXMAP = 1158088961)
      INTEGER SERVER_IMAGE_CMS
      PARAMETER (SERVER_IMAGE_CMS = 1158220289)
#define Server_image_attribute Attr_attribute
      STRUCTURE /Xv_server_image/
          RECORD /Xv_drawable_struct/ parent_data
          Xv_opaque private_data
          Xv_embedding embedding_data
          RECORD /Pixrect/ pixrect
          END STRUCTURE
C 
C  * PUBLIC functions 
C  
      INTEGER  server_image_rop
      EXTERNAL server_image_rop
C       Xv_opaque dest
C       INTEGER dx
C       INTEGER dy
C       INTEGER dw
C       INTEGER dh
C       INTEGER op
C       Xv_opaque src
C       INTEGER sx
C       INTEGER sy

      INTEGER  server_image_stencil
      EXTERNAL server_image_stencil
C       Xv_opaque dest
C       INTEGER dx
C       INTEGER dy
C       INTEGER dw
C       INTEGER dh
C       INTEGER op
C       Xv_opaque st
C       INTEGER stx
C       INTEGER dty
C       Xv_opaque src
C       INTEGER sx
C       INTEGER sy

      INTEGER  server_image_destroy
      EXTERNAL server_image_destroy
C       Pixrect_ptr pr

      INTEGER  server_image_get
      EXTERNAL server_image_get
C       Xv_opaque dest
C       INTEGER x
C       INTEGER y

      INTEGER  server_image_put
      EXTERNAL server_image_put
C       Xv_opaque dest
C       INTEGER x
C       INTEGER y
C       INTEGER value

      INTEGER  server_image_vector
      EXTERNAL server_image_vector
C       Xv_opaque dest
C       INTEGER x0
C       INTEGER y0
C       INTEGER x1
C       INTEGER y1
C       INTEGER op
C       INTEGER value

      Pixrect_ptr  server_image_region
      EXTERNAL server_image_region
C       Xv_opaque dest
C       INTEGER x
C       INTEGER y
C       INTEGER w
C       INTEGER h

      INTEGER  server_image_colormap
      EXTERNAL server_image_colormap
C       Xv_opaque dest
C       INTEGER index
C       INTEGER count
C       Byte red
C       Byte green
C       Byte blue

      INTEGER  server_image_replrop
      EXTERNAL server_image_replrop
C       Xv_opaque dest
C       INTEGER dx
C       INTEGER dy
C       INTEGER dw
C       INTEGER dh
C       INTEGER op
C       Xv_opaque src
C       INTEGER sx
C       INTEGER sy

      INTEGER  server_image_pf_text
      EXTERNAL server_image_pf_text
C       RECORD /Pr_prpos/ rpr
C       INTEGER op
C       Pixfont_ptr font
C       Char_ptr string

      COMMON /svrimage_globals/   SERVER_IMAGE
#endif svrimage_F.

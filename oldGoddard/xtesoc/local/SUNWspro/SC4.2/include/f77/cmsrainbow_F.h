#ifndef cmsrainbow_F_DEFINED
#define cmsrainbow_F_DEFINED

C   derived from @(#)cmsrainbow.h 20.10 91/05/28 SMI	
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
C 
C  * Definition of the colormap segment CMS_RAINBOW,
C  * a small collection of colors of the rainbow.
C  
      CHARACTER*(7) CMS_RAINBOW
      PARAMETER (CMS_RAINBOW = "rainbow")
      INTEGER CMS_RAINBOWSIZE
      PARAMETER (CMS_RAINBOWSIZE = 8)
      INTEGER WHITE
      PARAMETER (WHITE = 0)
      INTEGER RED
      PARAMETER (RED = 1)
      INTEGER ORANGE
      PARAMETER (ORANGE = 2)
      INTEGER YELLOW
      PARAMETER (YELLOW = 3)
      INTEGER GREEN
      PARAMETER (GREEN = 4)
      INTEGER BLUE
      PARAMETER (BLUE = 5)
      INTEGER INDIGO
      PARAMETER (INDIGO = 6)
      INTEGER VIOLET
      PARAMETER (VIOLET = 7)
#define Cms_rainbow(__v) Byte __v ( 0 : 7 )
      EXTERNAL cms_rainbowsetup
C       Cms_rainbow (r)
C       Cms_rainbow (g)
C       Cms_rainbow (b)

#endif cmsrainbow_F.

#ifndef termsw_F_DEFINED
#define termsw_F_DEFINED

C  derived from @(#)termsw.h 20.16 91/05/28 SMI 
C 
C  *
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "base_F.h"
#include "attr_F.h"
#include "pkg_public_F.h"
#include "textsw_F.h"
      Xv_pkg_ptr TERMSW
      Xv_pkg_ptr TERMSW_VIEW
#define Termsw Xv_opaque
#define Termsw_view Xv_opaque
C  the parent_data field isn't really a textsw view, only shares few attrs 
      STRUCTURE /Xv_termsw/
          RECORD /Xv_textsw/ parent_data
          Xv_opaque private_data
          Xv_opaque private_text
          Xv_opaque private_tty
          END STRUCTURE
C  the parent_data field isn't really a textsw view, only shares few attrs 
      STRUCTURE /Xv_termsw_view/
          RECORD /Xv_textsw_view/ parent_data
          Xv_opaque private_data
          Xv_opaque private_text
          Xv_opaque private_tty
          END STRUCTURE
      INTEGER TERMSW_MODE_TYPE,TTYSW_MODE_TYPE
      PARAMETER ( TERMSW_MODE_TYPE=0,  TTYSW_MODE_TYPE=1)
#define Termsw_mode INTEGER*4
C  Public attributes. 
      INTEGER TERMSW_MODE
      PARAMETER (TERMSW_MODE = 1359022081)
#define Termsw_attribute Attr_attribute
      COMMON /termsw_globals/   TERMSW_VIEW, TERMSW
#endif termsw_F.

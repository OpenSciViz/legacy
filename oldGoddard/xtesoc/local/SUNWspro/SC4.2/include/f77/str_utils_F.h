#ifndef str_utils_F_DEFINED
#define str_utils_F_DEFINED

C   derived from @(#)str_utils.h 20.15 91/05/28 SMI 
C 
C  *      (c) Copyright 1989 Sun Microsystems, Inc. Sun design patents
C  *      pending in the U.S. and foreign countries. See LEGAL NOTICE
C  *      file for terms of the license.
C  
#include "sun_F.h"
C 
C  ***********************************************************************
C  *		Typedefs, Enumerations, and Structures
C  ***********************************************************************
C  
      INTEGER Break,Sepr,Other
      PARAMETER ( Break=0,  Sepr=1,  Other=2)
#define CharClass INTEGER*4
      STRUCTURE /CharAction/
          Bool stop
          Bool include
          END STRUCTURE
#define CharClassProc INTEGER
#define CharActionProc INTEGER
C 
C  * PUBLIC Functions 
C  
      CharClass  xv_white_space
      EXTERNAL xv_white_space
C       CHARACTER ch

C 
C  * returns sepr for blanks, newlines, and tabs, other for everything else 
C  
      INTEGER  string_find
      EXTERNAL string_find
C       Char_ptr s
C       Char_ptr target
C       LOGICAL case_matters

C  
C  * strfind searches one instance of a string for another.
C  * If successful, returns the position in the string where the match began,
C  * otherwise -1.
C  * If case_matters = FALSE, 'a' will match with 'a' or 'A'.
C  
      LOGICAL  string_equal
      EXTERNAL string_equal
C       Char_ptr s1
C       Char_ptr s2
C       LOGICAL case_matters

C 
C  * strequal compares two strings. 
C  * If case_matters = FALSE, 'a' will match with 'a' or 'A'.
C  * either s1 or s2 can be NULL without harm.
C  
      LOGICAL  xv_substrequal
      EXTERNAL xv_substrequal
C       Char_ptr s1
C       INTEGER start1
C       Char_ptr s2
C       INTEGER start2
C       INTEGER n
C       LOGICAL case_matters

C 
C  * xv_substrequal compares two substrings without having to construct them.
C  * If case_matters = FALSE, 'a' will match with 'a' or 'A'.
C  
      Char_ptr  string_get_token
      EXTERNAL string_get_token
C       Char_ptr s
C       INTEGER index
C       Char_ptr dest
C       CharClassProc charproc

C 
C  * string_get_token is used for tokenizing input, where more degree of
C  * flexibility is required than simply delimiting tokens by white spaces
C  * characters are divided into three classes, Break, Sepr, and Other.
C  * separators (Sepr) serve to delimit a token. Leading separators are skipped.
C  * think of separators as white space. Break characters delimit tokens, and
C  * are themselves tokens. Thus, if a break character is the first thing seen
C  * it is returned as the token. If any non-separator characters have been seen,
C  * then they are returned as the token, and the break character will be the
C  * returned as the result of the next call to get_token.
C  * for example, if charproc returns Sepr for space, and Break for '(' and ')'
C  * and Other for all alphabetic characters, then the string "now (is) the"
C  * will yield five tokens consisting of "now" "(" "is" ")" and "the"
C 
C  * get_token stores the token that it constructs into dest,
C  * which is also returned as its value.
C  * index marks the current position in the string to "begin reading from"
C  * it is updated so that the client program does not have to keep track of
C  * how many characters have been read.
C 
C  * get_token returns NULL, rather than the empty string, corresponding to
C  * the case where the token is empty
C   
      Char_ptr  string_get_sequence
      EXTERNAL string_get_sequence
C       Char_ptr s
C       INTEGER index
C       Char_ptr dest
C       CharActionProc charproc

C 
C  * string_get_sequence is a more primitive tokenizer than get_token.
C  * it takes a procedure which for each character specifies whether the
C  * character is to terminate the sequence, and whether or not the
C  * character is to be included in the sequence.
C  * (If the character terminates the sequence, but is not included, then
C  * it will be seen again on the next call.)
C  * For example, having seen a \"\, to read to the matching \"\, call 
C  * get_sequence with an action procedure that returns {TRUE, TRUE} for \"\
C  * and  {FALSE, TRUE} for everything else. (If you want to detect the
C  * case where a " is preceded by a \\, simply save the last character
C  * and modify the procedure accordingly.
C 
C  * Note that gettoken can be defined in terms of get_sequence by
C  * having Other characters return {FALSE, TRUE}, and also noticing whether
C  * any have been seen yet, having Seprs return
C  * {(seen_some_others ? TRUE : FALSE), FALSE}
C  * and Break characters return {TRUE, (seen_some_others ? FALSE : TRUE)}
C  * returns NULL for the empty sequence
C  
#endif str_utils_F.

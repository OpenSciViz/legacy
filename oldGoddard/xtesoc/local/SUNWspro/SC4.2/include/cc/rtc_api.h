/*
 * Copyright 06/10/96 Sun Microsystems, Inc. All Rights Reserved
 *
 * rtc_api.h 1.13 96/06/10
 */

#ifndef _RTC_API_H
#define _RTC_API_H

/* 
 * The RTC API is the interface between the runtime memory access
 * checking (RTC) utility and private memory allocators.
 * See "man rtc_api" for details.
 *
 */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void * RTC_Result;  
#define RTC_SUCCESS (RTC_Result)0

extern RTC_Result
__RTC_hide_region(void *ptr,
    size_t size);

extern RTC_Result
__RTC_check_free(void *ptr);

extern RTC_Result
__RTC_check_malloc(size_t size);

extern RTC_Result
__RTC_check_malloc_result(void *ptr, size_t size);

extern RTC_Result
__RTC_check_realloc(void *ptr,
    size_t new_size);

extern RTC_Result
__RTC_check_realloc_result(void *old_ptr, void *new_ptr, size_t new_size);
 
extern void
__RTC_off(void);

extern void
__RTC_on(void);

extern RTC_Result
__RTC_record_free(void *ptr);

extern RTC_Result
__RTC_record_malloc(void *ptr, 
    size_t size);

extern RTC_Result
__RTC_record_realloc(void *old_ptr, 
    void *new_ptr, 
    size_t new_size);

extern void
__RTC_report_error(RTC_Result err);

#ifdef __SVR4

#pragma weak __RTC_hide_region
#pragma weak __RTC_check_free
#pragma weak __RTC_check_malloc
#pragma weak __RTC_check_malloc_result
#pragma weak __RTC_check_realloc
#pragma weak __RTC_check_realloc_result
#pragma weak __RTC_off
#pragma weak __RTC_on
#pragma weak __RTC_record_free
#pragma weak __RTC_record_malloc
#pragma weak __RTC_record_realloc
#pragma weak __RTC_report_error

#define _rtc_hide_region(p,s) \
        ((&__RTC_hide_region != 0) ? __RTC_hide_region(p,s) : RTC_SUCCESS)
#define _rtc_check_free(p) \
        ((&__RTC_check_free != 0) ? __RTC_check_free(p) : RTC_SUCCESS)
#define _rtc_check_malloc(s) \
        ((&__RTC_check_malloc != 0) ? __RTC_check_malloc(s) : RTC_SUCCESS)
#define _rtc_check_malloc_result(p,s) \
        ((&__RTC_check_malloc_result != 0) ? __RTC_check_malloc_result(p,s) : RTC_SUCCESS)
#define _rtc_check_realloc(p,s) \
        ((&__RTC_check_realloc != 0) ? __RTC_check_realloc(p,s) : RTC_SUCCESS)
#define _rtc_check_realloc_result(op, np, s) \
        ((&__RTC_check_realloc_result != 0) ? __RTC_check_realloc_result(op,np,s) : RTC_SUCCESS)

#define _rtc_off() \
        if (&__RTC_off != 0)  __RTC_off();
#define _rtc_on() \
        if (&__RTC_on != 0)  __RTC_on();
#define _rtc_record_free(p) \
        ((&__RTC_record_free != 0) ? __RTC_record_free(p) : RTC_SUCCESS)
#define _rtc_record_malloc(p,s) \
        ((&__RTC_record_malloc != 0) ? __RTC_record_malloc(p,s) : RTC_SUCCESS)
#define _rtc_record_realloc(op,np,s) \
        ((&__RTC_record_realloc != 0) ? __RTC_record_realloc(op,np,s) : RTC_SUCCESS)
#define _rtc_report_error(e) \
        if (&__RTC_report_error != 0)  __RTC_report_error(e);

#endif /* __SVR4 */

#ifdef __cplusplus
}
#endif

#endif /* #ifndef _RTC_API_H */
